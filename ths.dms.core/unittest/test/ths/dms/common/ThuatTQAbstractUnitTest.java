package test.ths.dms.common;

import java.text.SimpleDateFormat;
import java.util.Scanner;

import org.apache.log4j.PropertyConfigurator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.vo.LogInfoVO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public abstract class ThuatTQAbstractUnitTest {
	protected static SimpleDateFormat dtFormat = new SimpleDateFormat(
			"yyyy-MM-dd");
	protected static SimpleDateFormat dtimeFormat = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss");
	
	protected static LogInfoVO logInfo;
	protected static boolean stop = true;

	@BeforeClass
	public static void init() {
		// Khoi tao log4j
		PropertyConfigurator.configure("conf/log4j.properties");

		if (null == logInfo) {
			logInfo = new LogInfoVO();
			logInfo.setStaffCode("THUATTQ");
		}
	}

	@AfterClass
	public static void end() {
		if (stop) {
			System.out.println("\n	END PROGRAME");

			Scanner sc = new Scanner(System.in);
			while (!sc.nextLine().equals(""))
				;
		}
	}
}

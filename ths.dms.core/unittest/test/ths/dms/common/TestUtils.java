package test.ths.dms.common;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TestUtils {
	public static void getValueOfListObject(List<Object> listObjs) {
		if (null != listObjs && listObjs.size() > 0) {
			Object inputObj = listObjs.get(0);
			Class clazz = inputObj.getClass();
			Field[] fields = clazz.getDeclaredFields();

			for (Field field : fields) {
				String fieldName = field.getName().substring(0, 1)
						.toUpperCase()
						+ field.getName().substring(1);

				if (!fieldName.equals("serialVersionUID")) {
					System.out.print(fieldName + "\t");
				}
			}

			for (Object object : listObjs) {
				TestUtils.getValueOfObject(object);
			}
		} else {
			System.out.println("Data is empty ...");
		}
	}

	public static void printlnFieldOfObject(Object inputObj) {
		if (null != inputObj) {
			Class clazz = inputObj.getClass();
			Field[] fields = clazz.getDeclaredFields();

			for (int i = 0; i < fields.length; i++) {
				Field field = fields[i];
				String fieldName = field.getName().substring(0, 1)
						.toUpperCase()
						+ field.getName().substring(1);

				if (!fieldName.equals("SerialVersionUID")) {
					System.out.print("\"" + fieldName + "\"");

					if (i < fields.length - 1) {
						System.out.print(",");
					}
				}
			}

			System.out.println();
		} else {
			System.out.println("Data is empty ...");
		}
	}

	public static void getValueOfObject(Object inputObj) {
		Class clazz = inputObj.getClass();

		Field[] fields = clazz.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			String fieldName = field.getName().substring(0, 1).toUpperCase()
					+ field.getName().substring(1);

			if (!fieldName.equals("SerialVersionUID")) {
				try {
					String groupByGetter = "get" + fieldName;

					@SuppressWarnings("unchecked")
					Method m = clazz.getMethod(groupByGetter);
					Object result = m.invoke(inputObj);
					String value = "";

					if (null != result) {
						value = result.toString();
					} else {
						value = "null";
					}

					// System.out.print("" + field.getName());
					System.out.print("\"" + value + "\"");
					if (i < fields.length - 1) {
						System.out.print(",");
					}

				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

	static public String addParamToQuery(String query, Object[] params) {
		for (Object obj : params) {
			String temp = obj.toString();
			try {
				Integer.parseInt(temp);
			} catch (Exception ex) {
				try {
					DateFormat df = new SimpleDateFormat(
							"EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH);
					Date date = df.parse(temp);
					df = new SimpleDateFormat("EEE MMM dd kk:mm:ss yyyy",
							Locale.ENGLISH);
					temp = "to_date('" + df.format(date)
							+ "', 'DY MON DD HH24:MI:SS YYYY')";
				} catch (Exception e) {
					temp = "'" + temp + "'";
				}
			}

			query = query.replaceFirst("\\?", temp);
		}
		return query;
	}
}
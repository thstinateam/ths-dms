package test.ths.dms.crm;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import test.ths.dms.common.TestUtils;
import test.ths.dms.common.ThuatTQAbstractUnitTest;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.SuperviserMgr;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class TestCustomer extends ThuatTQAbstractUnitTest {

	@Autowired
	CommonMgr commonMgr;

	@Autowired
	CustomerMgr customerMgr;
	
	@Autowired
	SuperviserMgr superviserMgr; 
	
	@Test
	public void test() throws BusinessException {
		SaleOrder so = new SaleOrder();
		Date a = new Date();
		so.setAccountDate(a);
		System.out.println(so.getAccountDate());
	}

//	@Test
//	public void updateCustomer() throws BusinessException {
//		try {
//			Customer cus = this.customerMgr.getCustomerById(147l);
//
//			if (null == cus) {
//				System.out.println("Customer is null");
//				return;
//			}
//
//			cus.setStatus(ActiveType.STOPPED);
//
//			this.customerMgr.updateCustomer(cus, logInfo);
//			System.out.println("update success ....");
//		} catch (Exception e) {
//			System.out.println("failure ...");
//			e.printStackTrace();
//		} finally {
//		}
//	}

//	@Test
//	public void createCustomer() throws BusinessException {
//		try {
//			Customer cus = this.customerMgr.getCustomerById(147l);
//			Customer newCus = cus.clone();
//
//			newCus.setCustomerName("thuattq");
//			newCus.setId(null);
//
//			this.customerMgr.createCustomer(newCus, logInfo);
//			System.out.println("create success ....");
//		} catch (Exception e) {
//			System.out.println("failure ...");
//			e.printStackTrace();
//		} finally {
//		}
//	}

//	@Test
//	public void getListCustomerByStaffRouting() throws BusinessException {
//		KPaging<Customer> kPaging = new KPaging<Customer>();
//		Long shopId = 21l;
//		String customerCode = null; // "01L_BC"; // "0CL";
//		String customerName = null; // "Nguyen Tam Anh"; // "Huỳnh Tú";
//		Long staffId = 28l;
//		ActiveType status = ActiveType.RUNNING;
//
//		kPaging.setPage(0);
//		kPaging.setPageSize(10);
//		try {
//			ObjectVO<Customer> result = this.customerMgr
//					.getListCustomerByStaffRouting(kPaging, shopId,
//							customerCode, customerName, staffId, status);
//
//			if (null != result && result.getLstObject() != null
//					&& result.getLstObject().size() > 0) {
//				for (Customer item : result.getLstObject()) {
//					TestUtils.getValueOfObject(item);
//					System.out.println();
//				}
//			} else {
//				System.out.println("list data is empty");
//			}
//		} catch (Exception e) {
//			System.out.println("failure ...");
//		} finally {
//			stop = false;
//		}
//	}
//	
//	@Test
//	public void test1() throws BusinessException {
//		superviserMgr.getListCustomerBySaleStaffHasVisitPlan(49L);
//	}
	
//	@Test
//	public void searchCustomerVOForDSKH() throws BusinessException {
//		try {
//			KPaging<CustomerVO> paging = new KPaging<CustomerVO>(10);
//			ObjectVO<CustomerVO> vo = customerMgr.searchCustomerVOForDSKH(null, null, "149", null, null);
//			System.out.println("searchCustomerVOForDSKH success ...." + vo);
//		} catch (Exception e) {
//			System.out.println("failure ...");
//			e.printStackTrace();
//		} finally {
//		}
//	}
}

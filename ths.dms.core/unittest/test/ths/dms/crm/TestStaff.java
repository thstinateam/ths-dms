package test.ths.dms.crm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import test.ths.dms.common.TestUtils;
import test.ths.dms.common.ThuatTQAbstractUnitTest;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.ShopLockMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.dao.SaleOrderDAO;

import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.SaleOrderVOEx;
import ths.dms.core.entities.vo.TreeExVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class TestStaff extends ThuatTQAbstractUnitTest {

	@Autowired
	CommonMgr commonMgr;

	@Autowired
	StaffMgr staffMgr;

	@Autowired
	ShopMgr shopMgr;
	

	@Autowired
	SaleOrderDAO saleOrderDAO;
	
	@Autowired
	SaleOrderMgr saleOrderMgr;

	@Autowired
	ShopLockMgr shopLockMgr;
	
	@Test
	public void getApplicationDate() throws BusinessException {
		Date d = shopLockMgr.getNextLockedDay(6l);
		System.out.println(DateUtility.toDateString(d, "dd/MM/YYYY hh:mm:ss"));
	}
	
	@Test
	public void getListSaleOrderWithNoInvoiceEx1() throws BusinessException, DataAccessException {
		/*List<SaleOrderDetail> lst = saleOrderDAO.getListSaleOrderWithNoInvoiceEx1SOD
		(null, 6l, null,
					null, null, DateUtility.parse("27/05/2014","dd/MM/yyyy"),  DateUtility.parse("27/05/2014","dd/MM/yyyy"), null, DateUtility.parse("27/05/2014","dd/MM/yyyy"), null);
		System.out.println("0K");*/
	}
	
	@Test
	public void getListStaffByShop() throws BusinessException {
		KPaging<Staff> kPaging = null;
		Long shopId = 15l;
		ActiveType activeType = null;
		List<StaffObjectType> objectType = new ArrayList<StaffObjectType>();
		objectType.add(StaffObjectType.NVBH);

		try {
			ObjectVO<Staff> result = this.staffMgr.getListStaffByShop(kPaging,
					shopId, activeType, objectType);

			if (null != result && result.getLstObject() != null
					&& result.getLstObject().size() > 0) {
				for (Staff item : result.getLstObject()) {
					TestUtils.getValueOfObject(item);
					System.out.println();
				}
			} else {
				System.out.println("list data is empty");
			}
		} catch (Exception e) {
			System.out.println("failure ...");
		} finally {
			stop = false;
		}
	}

	@Test
	public void getShop() throws BusinessException {
		try {
			Shop shop = this.shopMgr.getShopById(21l);
			System.out.println("" + shop.getShopCode());

			Shop parentShop01 = shop.getParentShop();
			System.out.println("" + parentShop01.getShopCode());

			Shop parentShop02 = shop.getParentShop();
			System.out.println("" + parentShop02.getShopCode());
		} catch (Exception e) {
			System.out.println("failure ...");
		} finally {
			stop = false;
		}
	}

	// @Test
	// public void getListNVGS() throws BusinessException {
	// KPaging<Staff> kPaging = null;
	// String staffCode = null;
	// String staffName = null;
	// Long shopId = null;
	//
	// try {
	// shopId = 15l;
	//
	// ObjectVO<Staff> result = this.staffMgr.getListNVGS(kPaging,
	// staffCode, staffName, shopId);
	//
	// if (null != result && result.getLstObject() != null
	// && result.getLstObject().size() > 0) {
	// for (Staff item : result.getLstObject()) {
	// TestUtils.getValueOfObject(item);
	// System.out.println();
	// }
	// } else {
	// System.out.println("list data is empty");
	// }
	// } catch (Exception e) {
	// System.out.println("failure ...");
	// } finally {
	// stop = false;
	// }
	// }

	@Test
	public void getListNVBH() throws BusinessException {
		KPaging<Staff> kPaging = null;
		String staffCode = null;
		String staffName = null;
		List<String> lstStaffOwnerCode = null;
		Long shopId = null;
		Boolean isGetChildShop = null;

		try {
			lstStaffOwnerCode = new ArrayList<String>();
			lstStaffOwnerCode.add("VNM_GSNPP");
			lstStaffOwnerCode.add("KTNPP0002");

			shopId = 15l;
			isGetChildShop = true;

			// ObjectVO<Staff> result = this.staffMgr.getListNVBH(kPaging,
			// staffCode, staffName, lstStaffOwnerCode, shopId,
			// isGetChildShop);
			ObjectVO<Staff> result = null;
			if (null != result && result.getLstObject() != null
					&& result.getLstObject().size() > 0) {
				for (Staff item : result.getLstObject()) {
					TestUtils.getValueOfObject(item);
					System.out.println();
				}
			} else {
				System.out.println("list data is empty");
			}
		} catch (Exception e) {
			System.out.println("failure ...");
		} finally {
			stop = false;
		}
	}

	@Test
	public void getTreeStaffNotInDisplayProgramEx() throws BusinessException {
		Long parentShopId = 1l;
		Long displayProgramId = 172l;
		String staffCode = "TBHV000002";
		String staffName = null;
		Integer limit = null;

		try {
		} catch (Exception e) {
			System.out.println("failure ...");
			e.printStackTrace();
		} finally {
			stop = false;
		}
	}

	@Test
	public void getChildTreeStaffNotInDisplayProgram() throws BusinessException {
		Long parentShopId = 1l;
		Long displayProgramId = 179l;
		String staffCode = null;
		String staffName = null; // "Huỳnh trọng Trí";
		Integer limit = null;

		try {
		} catch (Exception e) {
			System.out.println("failure ...");
		} finally {
			stop = false;
		}
	}

	private void printTreeExVO(TreeExVO<Shop, Staff> input) {
		if (null != input) {
			// TestUtils.getValueOfObject(input.getObject());
			System.out.println(String.format("ShopId: %s\t ShopCode: %s", input
					.getObject().getId(), input.getObject().getShopCode()));
			// System.out.println();
			for (Staff item : input.getListLeaves()) {
				// TestUtils.getValueOfObject(item);
				// System.out.println();

				System.out.println(String.format("StaffId: %s\t StaffCode: %s",
						item.getId(), item.getStaffCode()));
			}

			if (null != input.getListChildren()) {
				for (TreeExVO<Shop, Staff> left : input.getListChildren()) {
					this.printTreeExVO(left);
					// System.out.println();
				}
			}
		} else {
			System.out.println("input is empty ...");
		}
	}
}
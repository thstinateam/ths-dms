package test.ths.dms.crm;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import test.ths.dms.common.TestUtils;
import test.ths.dms.common.ThuatTQAbstractUnitTest;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.ExceptionDayMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.StockReportMgr;

import ths.dms.core.entities.ExceptionDay;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class TestProduct extends ThuatTQAbstractUnitTest {
	@Autowired
	StockReportMgr stockReportMgr;

	@Autowired
	CommonMgr commonMgr;

	@Autowired
	ProductMgr productMgr;

	@Autowired
	ExceptionDayMgr exceptionDayMgr;

	@Test
	public void getListPrice() throws BusinessException {
		KPaging<Price> kPaging = null;
		String productCode = "02aa";
		BigDecimal price = null;
		Date fromDate = null;
		Date toDate = null;
		ActiveType status = null;
		BigDecimal priceNotVat = null;
		BigDecimal vat = null;

		try {
			ObjectVO<Price> listVO = this.productMgr.getListPrice(kPaging,
					productCode, price, fromDate, toDate, status, priceNotVat,
					vat, true);

			if (null == listVO || null == listVO.getLstObject()
					|| listVO.getLstObject().size() <= 0) {
				System.out.println("List attribute is empty");
			} else {
				System.out.println(String.format("list size %s", listVO
						.getLstObject().size()));
				for (Price item : listVO.getLstObject()) {
					System.out.println(String.format(
							"%s\t%s\t%s",
							item.getProduct().getProductCode(),
							null != item.getFromDate() ? dtFormat.format(item
									.getFromDate()) : "",
							null != item.getToDate() ? dtFormat.format(item
									.getToDate()) : ""));
				}
			}
		} catch (Exception e) {
			System.out.println("failure ...");
		} finally {
			stop = false;
		}
	}
}

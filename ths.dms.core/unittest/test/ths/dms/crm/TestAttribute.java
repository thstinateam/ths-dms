package test.ths.dms.crm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import test.ths.dms.common.TestUtils;
import test.ths.dms.common.ThuatTQAbstractUnitTest;
import ths.dms.core.business.AttributeMgr;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.StockReportMgr;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.enumtype.AttributeFilter;
import ths.dms.core.entities.enumtype.AttributeVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class TestAttribute extends ThuatTQAbstractUnitTest {
	@Autowired
	StockReportMgr stockReportMgr;

	@Autowired
	CommonMgr commonMgr;

	@Autowired
	AttributeMgr attributeMgr;

	@Test
	public void getListAttributeVO() throws BusinessException {
		KPaging<AttributeVO> kPaging = new KPaging<AttributeVO>();
		AttributeFilter filter = new AttributeFilter();

		kPaging.setPage(0);
		kPaging.setPageSize(10);

		try {
			ObjectVO<AttributeVO> result = attributeMgr.getListAttributeVO(
					filter, kPaging);

			if (null != result && result.getLstObject() != null
					&& result.getLstObject().size() > 0) {
			} else {
				System.out.println("list data is empty");
			}
		} catch (Exception e) {
			System.out.println("failure ...");
		} finally {
			stop = false;
		}
	}
}

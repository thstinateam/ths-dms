package test.ths.dms.crm;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import test.ths.dms.common.TestUtils;
import test.ths.dms.common.ThuatTQAbstractUnitTest;
import test.ths.dms.core.ThuatTQUnitTest;
import ths.dms.core.business.DebitMgr;

import ths.dms.core.entities.Debit;
import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.vo.CustomerDebitVO;
import ths.dms.core.entities.vo.DebitCustomerVO;
import ths.core.entities.vo.rpt.RptDebtOfCustomerDataVO;
import ths.core.entities.vo.rpt.RptDebtOfCustomerLv01VO;
import ths.dms.core.exceptions.BusinessException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class TestDebit extends ThuatTQAbstractUnitTest {
	@Autowired
	DebitMgr debitMgr;

	@Test
	public void testGetDataForDebtOfCustomer() throws BusinessException,
			ParseException {
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date reportDate = dtFormat.parse("2012-01-01");

		List<Long> customerIds = new ArrayList<Long>();
		Long shopId = 72l;
		customerIds.add(3l);
		// customerIds.add(5l);

		List<RptDebtOfCustomerLv01VO> listData = debitMgr
				.getDataForReportDebtOfCustomer(shopId, customerIds, reportDate);

		if (null != listData && listData.size() >= 0) {
			for (RptDebtOfCustomerLv01VO item : listData) {
				System.out.println(item.getCustomerCode() + " - "
						+ item.getCustomerName());

				ThuatTQUnitTest.printlnFieldOfObject(item.getListData().get(0));
				for (RptDebtOfCustomerDataVO data : item.getListData()) {
					ThuatTQUnitTest.getValueOfObject(data);
					System.out.println();
				}
			}
		}

		System.out.println("Success ...");
	}

	@Test
	public void getDebitCustomer() throws BusinessException, ParseException {
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDate = dtFormat.parse("2013-01-01");
		Date toDate = dtFormat.parse("2013-01-30");

		Long customerId = null;
		Long shopId = 15l;
		String customerCode = "0AH";// "0L1_BC";
		String customerName = null;

//		List<DebitCustomerVO> listData = debitMgr.getDebitCustomer(shopId,
//				customerId, customerCode, customerName, fromDate, toDate);
//
//		if (null != listData && listData.size() >= 0) {
//			for (DebitCustomerVO item : listData) {
//				ThuatTQUnitTest.getValueOfObject(item);
//				System.out.println();
//			}
//		}

		System.out.println("Success ...");
	}

	@Test
	public void testGetDebitCustomerRemain() throws BusinessException,
			ParseException {
		Long customerId = 78l;

		DebitDetail data = debitMgr.getDebitCustomerRemain(customerId);

		if (null != data) {
			System.out.println("ObjectId:  " + data.getDebit().getObjectId());
		} else {
			System.out.println("Data is empty");
		}

		System.out.println("Success ...");
	}

	@Test
	public void testGetDebit() throws BusinessException, ParseException {
		Long customerId = 72l;

		try {
			Debit data = debitMgr.getDebit(customerId, DebitOwnerType.CUSTOMER);

			System.out.println("Success ...");
		} catch (Exception ex) {

		}
	}

	@Test
	public void getListCustomerDebitVO() throws BusinessException,
			ParseException {
		Long shopId = 15l;
		String staffCode = "0000004109";
		String custName = null;
		String shortCode = "0AH";
		String orderNumber = null;
		BigDecimal amount = null; //new BigDecimal(1200000);
		BigDecimal fromAmount = null;
		BigDecimal toAmount = null;
		Boolean isReveived = true;
		Date fromDate = null;
		Date toDate = null;
		Long nvghId = null;
		Long nvttId = null;

		// 15, 0000004109, null, 0000004109, null, 1200000, null, null, true
		try {
			List<CustomerDebitVO> listData = debitMgr.getListCustomerDebitVO(shopId, shortCode, orderNumber, amount,
					fromAmount, toAmount, isReveived, fromDate, toDate, nvghId, nvttId, null);
			
			if (null != listData && listData.size() > 0) {
				for (CustomerDebitVO item : listData) {
					TestUtils.getValueOfObject(item);
				}
			} else {
				System.out.println("data is empty ...");
			}
		} catch (Exception ex) {
			System.out.println("failure ...");
		}
	}
}
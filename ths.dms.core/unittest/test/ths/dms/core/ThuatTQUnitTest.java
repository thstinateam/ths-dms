package test.ths.dms.core;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import test.ths.dms.common.ThuatTQAbstractUnitTest;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DebitMgr;
import ths.dms.core.business.ExceptionDayMgr;
import ths.dms.core.business.PoMgr;
import ths.dms.core.business.PromotionProgramMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.SalePlanMgr;
import ths.dms.core.business.StockReportMgr;
import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.StringUtility;

import ths.dms.core.entities.ExceptionDay;
import ths.dms.core.entities.PoAuto;
import ths.dms.core.entities.PoAutoDetail;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.enumtype.InvoiceStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PlanType;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.vo.DebitCustomerVO;
import ths.dms.core.entities.vo.DistributeSalePlanVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PoAuto01VO;
import ths.dms.core.entities.vo.PoCSVO;
import ths.dms.core.entities.vo.PoDVKHVO;
import ths.core.entities.vo.rpt.RptAggegateVATInvoiceDataVO;
import ths.core.entities.vo.rpt.RptCompareImExStF1DataVO;
import ths.core.entities.vo.rpt.RptCompareImExStF1VO;
import ths.core.entities.vo.rpt.RptCustomerByProductCatDataVO;
import ths.core.entities.vo.rpt.RptCustomerByProductCatLv01VO;
import ths.core.entities.vo.rpt.RptCustomerByProductCatLv02VO;
import ths.core.entities.vo.rpt.RptDebtOfCustomerDataVO;
import ths.core.entities.vo.rpt.RptDebtOfCustomerLv01VO;
import ths.core.entities.vo.rpt.RptF1DataVO;
import ths.core.entities.vo.rpt.RptF1Lv01VO;
import ths.core.entities.vo.rpt.RptImExStDataVO;
import ths.core.entities.vo.rpt.RptOrderByPromotionDataVO;
import ths.core.entities.vo.rpt.RptPoAutoDataVO;
import ths.core.entities.vo.rpt.RptPoAutoLV01VO;
import ths.core.entities.vo.rpt.RptPoAutoLV02VO;
import ths.core.entities.vo.rpt.RptPoAutoLV03VO;
import ths.core.entities.vo.rpt.RptPromotionDetailDataVO;
import ths.core.entities.vo.rpt.RptPromotionDetailLv01VO;
import ths.core.entities.vo.rpt.RptSaleOrderByProductDataVO;
import ths.core.entities.vo.rpt.RptSaleOrderByProductLv01VO;
import ths.core.entities.vo.rpt.RptSaleOrderByProductLv02VO;
import ths.dms.core.exceptions.BusinessException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class ThuatTQUnitTest extends ThuatTQAbstractUnitTest {
	@Autowired
	StockReportMgr stockReportMgr;

	@Autowired
	CommonMgr commonMgr;

	@Autowired
	PoMgr poMrg;

	@Autowired
	SaleOrderMgr saleOrderMgr;

	@Autowired
	ExceptionDayMgr exceptionDayMgr;

	@Autowired
	CustomerMgr customerMgr;

	@Autowired
	DebitMgr debitMgr;

	@Autowired
	SalePlanMgr salePlanMgr;

	@Autowired
	PromotionProgramMgr promotionProgramMgr;

	public static void getValueOfListObject(List<Object> listObjs) {
		if (null != listObjs && listObjs.size() > 0) {
			Object inputObj = listObjs.get(0);
			Class clazz = inputObj.getClass();
			Field[] fields = clazz.getDeclaredFields();

			for (Field field : fields) {
				String fieldName = field.getName().substring(0, 1)
						.toUpperCase()
						+ field.getName().substring(1);

				if (!fieldName.equals("serialVersionUID")) {
					System.out.print(fieldName + "\t");
				}
			}

			for (Object object : listObjs) {
				ThuatTQUnitTest.getValueOfObject(object);
			}
		} else {
			System.out.println("Data is empty ...");
		}
	}

	public static void printlnFieldOfObject(Object inputObj) {
		if (null != inputObj) {
			Class clazz = inputObj.getClass();
			Field[] fields = clazz.getDeclaredFields();

			for (int i = 0; i < fields.length; i++) {
				Field field = fields[i];
				String fieldName = field.getName().substring(0, 1)
						.toUpperCase()
						+ field.getName().substring(1);

				if (!fieldName.equals("SerialVersionUID")) {
					System.out.print("\"" + fieldName + "\"");

					if (i < fields.length - 1) {
						System.out.print(",");
					}
				}
			}

			System.out.println();
		} else {
			System.out.println("Data is empty ...");
		}
	}

	public static void getValueOfObject(Object inputObj) {
		Class clazz = inputObj.getClass();

		Field[] fields = clazz.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			String fieldName = field.getName().substring(0, 1).toUpperCase()
					+ field.getName().substring(1);

			if (!fieldName.equals("SerialVersionUID")) {
				try {
					String groupByGetter = "get" + fieldName;

					@SuppressWarnings("unchecked")
					Method m = clazz.getMethod(groupByGetter);
					Object result = m.invoke(inputObj);
					String value = "";

					if (null != result) {
						value = result.toString();
					} else {
						value = "null";
					}

					// System.out.print("" + field.getName());
					System.out.print("\"" + value + "\"");
					if (i < fields.length - 1) {
						System.out.print(",");
					}

				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// @Test
	public void stockMgr() throws BusinessException {
		PoVnm poConfirm = new PoVnm();
		Date currentDate = new Date();

		currentDate = DateUtility.addDate(currentDate, -10);
		poConfirm.setImportDate(currentDate);

		/*if (poMrg.checkValidateDatePoReturn(poConfirm)) {
			System.out.println("Validate ... ");
		} else {
			System.out.println("Not validate ... ");
		}*/
	}

	// @Test
	public void testUpdatePoAuto() throws BusinessException {
		List<PoAuto> listPoAuto = poMrg.getListPoAuto(null, null, null, null,
				null, null);

		int index = 0;
		for (PoAuto poAuto : listPoAuto) {
			System.out.println("" + index++ + ". Id : " + poAuto.getId());
		}

		try {
			poMrg.approvedPoAuto(listPoAuto);

			System.out.println("Update success ... ");
		} catch (Exception ex) {
			System.out.println("Update failure ... ");
		}
	}

	// @Test
	public void testPoAutoVo01() throws BusinessException {
		List<PoAuto01VO> listPoAuto = poMrg.getListPoAuto01(null, null, null,
				null, null, false);
		int index = 0;
		for (PoAuto01VO poAuto : listPoAuto) {
			System.out.println("" + index++ + ". Id : " + poAuto.getPoAutoId());
		}

		System.out.println("Sucess ...");
	}

	// @Test
	public void test() throws BusinessException {
		PoAuto poAuto = poMrg.getPoAutoById(1l);

		System.out.println("Sucess ..." + poAuto.getPoAutoNumber());
	}

	// @Test
	public void testReportPoConfirm() throws BusinessException {
		Date fromDate = DateUtility.addDate(new Date(), -20);
		Date toDate = DateUtility.addDate(new Date(), 2);

		List<PoDVKHVO> listPoDVKHVo = poMrg.getPoDVKHforReport(10l, fromDate,
				toDate, 1L);

		for (PoDVKHVO vo : listPoDVKHVo) {
			System.out.println("" + vo.getSaleOrderNumber());
		}

		System.out.println("Success ...");
	}

	// @Test
	// public void testgetDataForCompareImExStF1Report() throws
	// BusinessException {
	// Date currentDate = new Date();
	//
	// List<RptCompareImExStF1VO> listVo = poMrg
	// .getDataForCompareImExStF1Report(10l, currentDate, currentDate);
	//
	// for (RptCompareImExStF1VO vo : listVo) {
	// System.out.println("" + vo.getSubCat());
	// }
	//
	// System.out.println("Success ...");
	// }

	/*@Test
	public void testgetDataForComparePoAutoReport() throws BusinessException {

		List<RptCompareImExStF1VO> listData = poMrg
				.getDataForComparePOAutoReport(21L, 110L);

		if (null != listData && listData.size() >= 0) {
			ThuatTQUnitTest.printlnFieldOfObject(listData.get(0));

			for (RptCompareImExStF1VO item : listData) {
				System.out.println("Cat: " + item.getSubCat());

				ThuatTQUnitTest.printlnFieldOfObject(item.getData().get(0));
				for (RptCompareImExStF1DataVO data : item.getData()) {
					ThuatTQUnitTest.getValueOfObject(data);
					System.out.println();
				}
				System.out.println();
			}
		}

		System.out.println("Success ...");
	}*/

	// @Test
	/*public void testGetListPoCSVO() throws BusinessException {

		Date xx = new Date();
		Date yy = DateUtility.addDate(xx, -30);
		// List<PoCSVO> listVo = poMrg.getListPoCSVO(null, null, xx, yy, null,
		// false);
		List<PoCSVO> listVo = poMrg.getListPoCSVO(null, null, null, null, null,
				false);

		for (PoCSVO vo : listVo) {
			System.out.print("PoVNMId: " + vo.getPoVNMId());
			System.out.print("\tQuantity: " + vo.getQuantity());
			System.out.print("\tQuantityReceived: " + vo.getQuantityReceived());
			System.out.print("\tStatus: " + vo.getStatus());
			System.out.print("\tAmount: " + vo.getAmount());
			System.out.print("\tAmountReceived: " + vo.getAmountReceived());
			System.out.print("\tPoVNMDate: " + vo.getPoVNMDate());

			System.out.println();
		}

		System.out.println("Success ...");
	}*/

	// @Test
	public void testGetListSaleOrderFormTable() throws BusinessException {
		List<SaleOrder> result = saleOrderMgr.getListSaleOrderFromTablet();

		for (SaleOrder item : result) {
			System.out.println("OrderNumber: " + item.getOrderNumber());
		}

		System.out.println("Success ...");
	}

	// @Test
	public void testGetListSaleOrderFormTableByCondition()
			throws BusinessException {
		List<SaleOrder> result = saleOrderMgr
				.getListSaleOrderFromTabletByCondition("ORDER_NUMBER10",
						SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM,
						"C00001", "Tran", 1l, 1, new Date(), new Date());

		for (SaleOrder item : result) {
			System.out.println("OrderNumber: " + item.getOrderNumber());
		}

		System.out.println("Success ...");
	}

	// @Test
	public void testGetListPoAutoDetail() throws BusinessException {
		List<PoAutoDetail> result = poMrg.getListPoAutoDetail(null, 67l);

		for (PoAutoDetail item : result) {
			System.out.println("OrderNumber: " + item.getId());
		}

		System.out.println("Success ...");
	}

	// @Test
	public void testAcceptSaleOrderFromTable() throws BusinessException {
		List<Long> result = new ArrayList<Long>();
		result.add(12l);
		List<SaleOrder> listSaleOrder = saleOrderMgr.acceptSaleOrderFromTable(
				result, null);
		for (SaleOrder item : listSaleOrder) {
			System.out.println("OrderNumber: " + item.getId());
		}

		System.out.println("Success ...");
	}

	// @Test
	public void testCancelSaleOrderFromTable() throws BusinessException {
		List<Long> result = new ArrayList<Long>();
		result.add(12l);
		result.add(10l);
		result.add(13l);
		saleOrderMgr.cancelSaleOrderFromTable(result,
				"test cancel saleorder from table");

		System.out.println("Success ...");
	}

	// @Test
	public void testExceptionDayMgr() throws BusinessException {
//		ExceptionDay date = exceptionDayMgr.getExceptionDayById(1l);
//		List<ExceptionDay> listdateinMonth = exceptionDayMgr
//				.getExceptionDayByMonth(1, 2012);
//		List<ExceptionDay> listdateinyear = exceptionDayMgr
//				.getExceptionDayByYear(2012);
//
//		System.out.println("Success ..." + listdateinMonth.size());
	}

	// @Test
	public void testImExStReport() throws BusinessException {
		Date currentDate = commonMgr.getSysDate();
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDate = null;

		try {
			fromDate = dtFormat.parse("2012-01-01");
		} catch (ParseException e) {
			e.printStackTrace();
		}

		currentDate = DateUtility.addDate(currentDate, -1);

		List<RptImExStDataVO> listData = stockReportMgr.getImExStReportReport(
				2l, fromDate, currentDate);

		if (null != listData) {
			for (RptImExStDataVO item : listData) {
//				System.out.println("Cat: "
//						+ item.getProductCat().getProductInfoName());
//				List<RptImExStDataDetailVO> listDataDetail = item.getListData();
//				for (RptImExStDataDetailVO itemDetail : listDataDetail) {
//					System.out.println("\t Product name: "
//							+ itemDetail.getProductName());
//				}
			}
		}

		System.out.println("Success ...");
	}

	// @Test
	public void testPoAutoReport() throws BusinessException {
		Date currentDate = commonMgr.getSysDate();
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDate = null;

		try {
			fromDate = dtFormat.parse("2012-08-01");
		} catch (ParseException e) {
			e.printStackTrace();
		}

		currentDate = DateUtility.addDate(currentDate, -1);
		List<Long> listShopId = new ArrayList<Long>();
		listShopId.add(16l);
		listShopId.add(1l);
		listShopId.add(2l);

		List<RptPoAutoLV01VO> listData = poMrg.getDataForReportPoAutoShop(
				listShopId, fromDate, currentDate);

		if (null != listData) {
			for (RptPoAutoLV01VO vo01 : listData) {
				System.out.println("Shop id: " + vo01.getShopCode());
				for (RptPoAutoLV02VO vo02 : vo01.getListData()) {
					System.out.println("\tPo date: " + vo02.getStrPoDate());

					for (RptPoAutoLV03VO vo03 : vo02.getListData()) {
						System.out.println("\t\tPo number: "
								+ vo03.getPoNumber());

						for (RptPoAutoDataVO data : vo03.getListData()) {
							System.out.println("\t\t\t" + data.getProductCode()
									+ "\t" + data.getProductName());
						}
					}
				}

			}
		}

		System.out.println("Success ...");
	}

	// @Test
	public void testSaleOrderByProductReport() throws BusinessException {
		Date currentDate = commonMgr.getSysDate();
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDate = null;

		try {
			fromDate = dtFormat.parse("2012-01-01");
		} catch (ParseException e) {
			e.printStackTrace();
		}

		List<Long> staffIds = new ArrayList<Long>();
		Long shopId = 1l;
		// staffIds.add(3l);
		staffIds.add(4l);

		List<RptSaleOrderByProductLv01VO> listData = saleOrderMgr
				.getDataForSaleByProductReport(shopId, staffIds, fromDate,
						currentDate);

		if (null != listData) {
			for (RptSaleOrderByProductLv01VO vo01 : listData) {
				System.out.println("NVBH: " + vo01.getStaffCode() + "\t"
						+ vo01.getStaffName());
				for (RptSaleOrderByProductLv02VO vo02 : vo01.getListData()) {
					System.out.println("\tNganh: " + vo02.getCat());

					for (RptSaleOrderByProductDataVO data : vo02.getListData()) {
						System.out.println("\t\t" + data.getProductCode()
								+ "\t" + data.getProductName());
					}
				}

			}
		}
	}

	// @Test
	public void testCustomerByProductCatReport() throws BusinessException {
		Date currentDate = commonMgr.getSysDate();
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDate = null;

		try {
			fromDate = dtFormat.parse("2012-01-01");
		} catch (ParseException e) {
			e.printStackTrace();
		}

		List<Long> cusIds = new ArrayList<Long>();
		Long shopId = 1l;
		cusIds.add(3l);
		cusIds.add(5l);

		List<RptCustomerByProductCatLv01VO> listData = customerMgr
				.getDataForCusByProReport(shopId, cusIds, fromDate, currentDate);

		if (null != listData) {
			for (RptCustomerByProductCatLv01VO vo01 : listData) {
				System.out.println("Khach hang: " + vo01.getCustomerCode()
						+ " - " + vo01.getCustomerName());
				for (RptCustomerByProductCatLv02VO vo02 : vo01.getListData()) {
					System.out.println("\tNganh: " + vo02.getProductCat()
							+ " - " + vo02.getProductCatDes());

					for (RptCustomerByProductCatDataVO data : vo02
							.getListData()) {
						System.out.println("\t\t" + data.getProductCode()
								+ "\t" + data.getProductName());
					}
				}

			}
		}
	}

	// @Test
	public void testGetDataForOrderByPromotion() throws BusinessException {
		Date currentDate = commonMgr.getSysDate();
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDate = null;

		try {
			fromDate = dtFormat.parse("2012-01-01");
		} catch (ParseException e) {
			e.printStackTrace();
		}

		List<Long> staffIds = new ArrayList<Long>();
		Long shopId = 1l;
		// staffIds.add(3l);
		// staffIds.add(5l);

		List<RptOrderByPromotionDataVO> listData = saleOrderMgr
				.getDataForOrderByPromotionReport(shopId, staffIds, fromDate,
						currentDate);

		if (null != listData && listData.size() >= 0) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

			for (RptOrderByPromotionDataVO item : listData) {
				System.out.print("CustomerCode: " + item.getCustomerCode());
				System.out.print("\tCustomerName: " + item.getCustomerName());
				System.out.print("\tOrderDate: "
						+ format.format(item.getOrderDate()));
				System.out.print("\tPrice: " + item.getPrice());
				System.out.print("\tProductCode: " + item.getProductCode());
				System.out.print("\tProductName: " + item.getProductName());
				System.out.print("\tPromotionCode: " + item.getPromotionCode());
				System.out.print("\tPromotionName: " + item.getPromotionName());
				System.out.print("\tQuantity: " + item.getQuantity());
				System.out.print("\tStaffCode: " + item.getStaffCode());
				System.out.print("\tStaffName: " + item.getStaffName());
				System.out.print("\tAmount: " + item.getAmount());
				System.out.println("");
			}
		}

		System.out.println("Success ...");
	}

	// @Test
	public void testGetDataForDebtOfCustomer() throws BusinessException,
			ParseException {
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date reportDate = dtFormat.parse("2012-01-01");

		List<Long> customerIds = new ArrayList<Long>();
		Long shopId = 1l;
		// customerIds.add(3l);
		// customerIds.add(5l);

		List<RptDebtOfCustomerLv01VO> listData = debitMgr
				.getDataForReportDebtOfCustomer(shopId, customerIds, reportDate);

		if (null != listData && listData.size() >= 0) {
			for (RptDebtOfCustomerLv01VO item : listData) {
				System.out.println(item.getCustomerCode() + " - "
						+ item.getCustomerName());

				ThuatTQUnitTest.printlnFieldOfObject(item.getListData().get(0));
				for (RptDebtOfCustomerDataVO data : item.getListData()) {
					ThuatTQUnitTest.getValueOfObject(data);
					System.out.println();
				}
			}
		}

		System.out.println("Success ...");
	}

	@Test
	public void testGetDebitCustomer() throws BusinessException, ParseException {
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDate = dtFormat.parse("2012-12-16");
		Date toDate = dtFormat.parse("2012-12-18");

		Long customerId = null;
		Long shopId = 16l;
		String customerCode = null;
		String customerName = "quan TU toan";

//		List<DebitCustomerVO> listData = debitMgr.getDebitCustomer(shopId,
//				customerId, customerCode, customerName, fromDate, toDate);
//
//		if (null != listData && listData.size() >= 0) {
//			for (DebitCustomerVO item : listData) {
//				ThuatTQUnitTest.getValueOfObject(item);
//				System.out.println();
//			}
//		}

		System.out.println("Success ...");
	}

	// @Test
	public void testGetDataForAggegateVATInvoiceReport()
			throws BusinessException, ParseException {
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDate = dtFormat.parse("2012-01-01");
		Date toDate = dtFormat.parse("2012-09-30");

		List<Long> customerIds = new ArrayList<Long>();
		Long shopId = 1l;
		Long staffId = 1L;
		Integer VAT = 10;
		InvoiceStatus status = null;
		// customerIds.add(3l);
		// customerIds.add(5l);

		List<RptAggegateVATInvoiceDataVO> listData = saleOrderMgr
				.getDataForAggegateVATInvoiceReport(shopId, customerIds,
						staffId, VAT, status, fromDate, toDate);

		if (null != listData && listData.size() >= 0) {
			ThuatTQUnitTest.printlnFieldOfObject(listData.get(0));

			for (RptAggegateVATInvoiceDataVO item : listData) {
				ThuatTQUnitTest.getValueOfObject(item);
				System.out.println();
			}
		}

		System.out.println("Success ...");
	}

	 @Test
	public void testGetListPoConfirmByCondition() throws BusinessException {
		Date fromDate = null;
		Date toDate = null;

//		List<PoVnm> abc = poMrg.getListPoConfirmByCondition(1l, null, null,
//				fromDate, toDate, PoVNMStatus.IMPORTED, true);

		System.out.println("Success ...");
	}

	// @Test
	public void testCheckEnoughProductQuantityInListOrder()
			throws BusinessException {
		Long shopId = 16l;
		List<Long> listOrderId = new ArrayList<Long>();
		int size = 0;

		listOrderId.add(25l);
		listOrderId.add(100l);
		listOrderId.add(123l);
		listOrderId.add(121l);
		listOrderId.add(120l);
		listOrderId.add(88l);
		listOrderId.add(26l);
		listOrderId.add(108l);
		listOrderId.add(133l);
		listOrderId.add(102l);
		listOrderId.add(107l);
		listOrderId.add(114l);
		listOrderId.add(101l);
		listOrderId.add(104l);
		listOrderId.add(12l);
		listOrderId.add(98l);
		listOrderId.add(106l);
		listOrderId.add(122l);
		listOrderId.add(103l);
		listOrderId.add(118l);

		List<Product> listProduct = saleOrderMgr
				.checkEnoughProductQuantityInListOrder(shopId, listOrderId);

		if (null != listProduct && listProduct.size() > 0) {
			size = listProduct.size();
		}

		System.out.println("List size: " + size);
		System.out.println("Success ...");

		System.out.println("Success ...");
	}

	// @Test
	public void testGetListDataForPromotionDetailReport()
			throws BusinessException {
		SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDate = null;
		Date toDate = null;
		Long shopId = 16l;
		List<Long> staffIds = new ArrayList<Long>();

		staffIds.add(11l);

		try {
			fromDate = dtFormat.parse("2012-01-01");
			toDate = new Date();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		List<RptPromotionDetailLv01VO> listData = promotionProgramMgr
				.getDataForPromotionDetailReport(shopId, staffIds, fromDate,
						toDate);
		String template = "Ngay HD: %s. Ma NV: %s. Ma Kh: %s. Loai CT: %s. SP Ban: %s. SL Ban: %s. SP KM: %s. SL KM: %s. Tien KM: %s. Ma KH: %s.";
		if (null != listData && listData.size() >= 0) {
			for (RptPromotionDetailLv01VO item : listData) {
				System.out.println(item.getPromotionCode() + " - "
						+ item.getPromotionName());

				for (RptPromotionDetailDataVO data : item.getListData()) {

					System.out.println(String.format(template,
							data.getOrderDate(), data.getStaffCode() + " - "
									+ data.getStaffName(),
							data.getCustomerCode(), data.getPromotionType(),
							data.getSaleProduct(), data.getSaleQuantity(),
							data.getFreeProduct(), data.getFreeQuantity(),
							data.getFreeAmount(), data.getPromotionCode()
									+ " - " + data.getProductName()));
				}
			}
		}

		System.out.println("Success ...");
	}

	// @Test
	public void testGetDataForF1Report() throws BusinessException {
		Long shopId = 16l;
		Date fromDate, toDate;
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

		try {
			fromDate = format.parse("2012/09/01");
			// toDate = format.parse("2012/09/30");
			toDate = format.parse("2012/10/31");
		} catch (Exception ex) {
			return;
		}

		List<RptF1Lv01VO> listData = poMrg.getDataForF1Report(shopId, fromDate,
				toDate);

		if (null != listData && listData.size() >= 0) {
			for (RptF1Lv01VO item : listData) {
				System.out.println("Cat: " + item.getProductInfoName());

				for (RptF1DataVO data : item.getListData()) {
					System.out.print(data.getProductCode() + "\t");
					System.out.print("" + data.getProductName() + "\t");
					System.out.print("" + data.getPrice() + "\t");
					System.out.print("" + data.getOpenStockTotal() + "\t");
					System.out.print("" + data.getImportQty() + "\t");
					System.out.print("" + data.getExportQty() + "\t");
					System.out.print("" + data.getCloseStockTotal() + "\t");
					System.out.print("" + data.getMonthCumulate() + "\t");
					System.out.print("" + data.getMonthPlan() + "\t");
					System.out.print("" + data.getDayPlan() + "\t");
					System.out.print("" + data.getDayReservePlan() + "\t");
					System.out.print("" + data.getDayReserveReal() + "\t");
					System.out.print("" + data.getMin() + "\t");
					System.out.print("" + data.getNext() + "\t");
					System.out.print("" + data.getLead() + "\t");
					System.out.print("" + data.getRequimentStock() + "\t");
					System.out.print("" + data.getPoCfQty() + "\t");
					System.out.print("" + data.getPoCsQty() + "\t");
					System.out.print("" + data.getConvfact() + "\t");
					System.out.print("" + data.getBoxQuantity() + "\t");
					System.out.print("" + data.getAmount() + "\t");
					System.out.print("" + data.getWarning() + "\t");
					System.out.println();
				}
				System.out.println();
			}
		}

		System.out.println("Success ...");
	}

	 @Test
	public void testgetListSalePlanWithCondition() throws BusinessException {
		List<DistributeSalePlanVO> listData = salePlanMgr
				.getListSalePlanWithCondition(21l, null, null, 5, 2013, null,
						PlanType.MONTH);

		System.out.println("Success ...");
	}

	 @Test
		public void testgetListSalePlanWithNoCondition() throws BusinessException {
		 KPaging<DistributeSalePlanVO> kPaging = new KPaging<DistributeSalePlanVO>(10);
		 kPaging.setPage(0);
			ObjectVO<DistributeSalePlanVO> listData = salePlanMgr.getListSalePlanWithNoCondition(kPaging,null,null);

			System.out.println("Success ...");
		}
	 
	// @Test
	public void testModifierExceptionDayMgr() throws BusinessException,
			ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = format.parse("2012-10-14");
		ExceptionDay excDay = new ExceptionDay();

		LogInfoVO info = new LogInfoVO();
		info.setIp("127.0.0.1");
		info.setStaffCode("16");

		excDay.setDayOff(date);
		// excDay.setId(18l);

		exceptionDayMgr.deleteExceptionDay(excDay, info);

		System.out.println("Success ...");

		Scanner scan = new Scanner(System.in);
		while (!scan.nextLine().equals("")) {
		}
	}

	// @Test
	public void testCreateExceptionDay() throws BusinessException,
			ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = format.parse("2013-05-23");
		ExceptionDay excDay = new ExceptionDay();

		LogInfoVO info = new LogInfoVO();
		info.setIp("127.0.0.1");
		info.setStaffCode("16");

		excDay.setDayOff(date);
		// excDay.setId(18l);
		//excDay.setType("NL");

		exceptionDayMgr.createExceptionDay(excDay, info);

		System.out.println("Success ...");

		Scanner scan = new Scanner(System.in);
		while (!scan.nextLine().equals("")) {
		}
	}

	// @Test
	public void testStringUtil() throws BusinessException {
		String input = "nhanvien_donvi";
		System.out.println(StringUtility.toOracleSearchLike(input));
	}
}

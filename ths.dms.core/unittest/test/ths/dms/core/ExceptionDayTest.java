package test.ths.dms.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.ExceptionDay;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.BusinessException;

import test.ths.dms.common.ThuatTQAbstractUnitTest;
import ths.dms.core.business.ExceptionDayMgr;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class ExceptionDayTest extends ThuatTQAbstractUnitTest {
	@Autowired
	ExceptionDayMgr exceptionDayMgr;

	@Test
	public void testCreateExceptionDay() throws BusinessException,
			ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = format.parse("2013-05-23");
		ExceptionDay excDay = new ExceptionDay();

		LogInfoVO info = new LogInfoVO();
		info.setIp("127.0.0.1");
		info.setStaffCode("16");

		excDay.setDayOff(date);
		// excDay.setId(18l);
		//excDay.setType("NL");

		exceptionDayMgr.createExceptionDay(excDay, info);

		System.out.println("Success ...");

		Scanner scan = new Scanner(System.in);
		while (!scan.nextLine().equals("")) {
		}
	}

	// 226026

	@Test
	public void testModifierExceptionDayMgr() throws BusinessException,
			ParseException {
		ExceptionDay excDay = this.exceptionDayMgr.getExceptionDayById(226026l);

		LogInfoVO info = new LogInfoVO();
		info.setIp("127.0.0.1");
		info.setStaffCode("16");

		//excDay.setType("THUATTQ");
		this.exceptionDayMgr.updateExceptionDay(excDay, info);

		System.out.println("Success ...");

		Scanner scan = new Scanner(System.in);
		while (!scan.nextLine().equals("")) {
		}
	}
}

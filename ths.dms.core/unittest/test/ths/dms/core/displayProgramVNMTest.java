package test.ths.dms.core;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.DisplayProgramVNM;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.core.business.DisplayProgramVNMMgr;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class displayProgramVNMTest {
	@Autowired DisplayProgramVNMMgr displayProgramVNMMgr;
	
	@Test
	public void getListDisplayProgramVNM() throws BusinessException{
		ObjectVO<DisplayProgramVNM> vo = displayProgramVNMMgr.getListDisplayProgramVNM(null, 15l, null, null, ActiveType.RUNNING, null, null);
		System.out.print("vuong mo cu: "+ vo.getLstObject().size()+"\n");
	}
}

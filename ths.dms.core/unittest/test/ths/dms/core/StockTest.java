package test.ths.dms.core;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.CycleCountResult;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StockLock;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductAmountVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransDetailVO;
import ths.dms.core.entities.vo.StockTransVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_Stock;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.business.CycleCountMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.StockMgr;
import ths.dms.core.dao.StockTotalDAO;
import ths.dms.core.dao.StockTransDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class StockTest  extends AbstractUnitTest {
	
	@Autowired
	StockTotalDAO stockTotalDAO;
	
	@Autowired
	StockTransDAO stockTransDAO;
	
	@Autowired
	CycleCountMgr cycleCountMgr;
	
	@Autowired 
	ProductMgr productMgr;
	
	@Autowired
	ShopMgr shopMgr;
	
	@Autowired
	StockMgr stockMgr;
	
	@Autowired
	StaffMgr staffMgr;
	
	@Test
	public void createBeginningData() throws BusinessException, ParseException {
		//shop: 61
		//product: 81
		Shop s = shopMgr.getShopById(Long.valueOf(61));
		Product p = productMgr.getProductById(Long.valueOf(81));
		
		StockTotal st = new StockTotal();
		st.setAvailableQuantity(100);
		st.setQuantity(100);
		st.setObjectId(s.getId());
		st.setObjectType(StockObjectType.SHOP);
		st.setProduct(p);
		st = stockMgr.createStockTotal(st);
		
		DateFormat f = new SimpleDateFormat("dd-MM-yyyy");
		
		ProductLot lot = new ProductLot();
		lot.setExpiryDate(f.parse("01-10-2013"));
		lot.setLot("CODE7");
		lot.setProduct(p);
		lot.setObjectId(s.getId());
		lot.setObjectType(StockObjectType.SHOP);
		lot.setQuantity(10);
		stockMgr.createProductLot(lot);
		
		lot = new ProductLot();
		lot.setExpiryDate(f.parse("01-12-2013"));
		lot.setLot("CODE8");
		lot.setProduct(p);
		lot.setObjectId(s.getId());
		lot.setObjectType(StockObjectType.SHOP);
		lot.setQuantity(20);
		stockMgr.createProductLot(lot);
		
		lot = new ProductLot();
		lot.setExpiryDate(f.parse("12-11-2013"));
		lot.setLot("CODE9");
		lot.setProduct(p);
		lot.setObjectId(s.getId());
		lot.setObjectType(StockObjectType.SHOP);
		lot.setQuantity(70);
		stockMgr.createProductLot(lot);
		
	}
	
	@Test
	public void exportVansale() throws BusinessException {
		//staff: 1
		
	}
	
	@Test
	public void importVansale() throws BusinessException {
		//staff: 1
	}	
	
	@Test
	public void createCycleCount() throws DataAccessException, BusinessException {
		Shop s = shopMgr.getShopById(Long.valueOf(20));
//		Product p = productMgr.getProductById(81);
		
		CycleCount cc = new CycleCount();
		cc.setCycleCountCode("CCC26611");
		cc.setFirstNumber(2);
		cc.setShop(s);
		cc.setCreateUser("12312333");
		cc = cycleCountMgr.createCycleCount(cc);
		LogInfoVO log = new LogInfoVO();
		log.setStaffCode("VNM_ADMIN");
		cycleCountMgr.createListCycleCountMapProduct(cc, null, true,log);
	}
	
	@Test
	public void editCycleCountMapProduct() throws BusinessException {
//		CycleCountMapProduct ccd = cycleCountMgr.getCycleCountMapProductById(1);
//		CycleCountResult ccr = new CycleCountResult();
//		ccr.setCycleCountMapProduct(ccd);
//		ccr.setLot("CODE9");
//		ccr.setQuantityCounted(15);
//		cycleCountMgr.createCycleCountResult(ccr);
		
		CycleCountResult ccr = cycleCountMgr.getCycleCountResultById(Long.valueOf(2));
//		ccr.setQuantityCounted(9);
		cycleCountMgr.deleteCycleCountResult(ccr);
		
	}
	
	@Test
	public void getStockTotal() throws DataAccessException {
		StockTotal s =stockTotalDAO.getStockTotalById(Long.valueOf(1));
		System.out.println(s);
	}
	
	@Test
	public void generateStockTransCode() throws DataAccessException {
		//System.out.println(stockTransDAO.generateStockTransCode(getCurrentShop().getId()));
	}
	
	@Test
	public void deleteCycleCount() throws BusinessException {
		CycleCount c = new CycleCount();
		c = cycleCountMgr.createCycleCount(c);
		cycleCountMgr.deleteCycleCount(c);
	}
	
	@Test
	public void checkExists() throws BusinessException {
		cycleCountMgr.checkIfCycleCountMapProductExist(Long.valueOf(1), "123", Long.valueOf(1));
	}
	
	@Test
	public void getListStockTotal() throws BusinessException {
		KPaging<StockTotal> kPaging = new KPaging<StockTotal>();
		stockMgr.getListStockTotal(kPaging, "productCode", "productName", "promotionProgramCode", Long.valueOf(1), StockObjectType.CUSTOMER, Long.valueOf(1), Long.valueOf(2), 1,2, Long.valueOf(1));
		List<ProductAmountVO> lstProductAmountVO = new ArrayList<ProductAmountVO>();
		ProductAmountVO vo = new ProductAmountVO();
		vo.setOwnerId(1);
		vo.setOwnerType(StockObjectType.CUSTOMER);
		vo.setProductId(1);
		lstProductAmountVO.add(vo);
		stockMgr.checkIfProductHasEnough(lstProductAmountVO);
	}
	
	@Test
	public void stockMgr() throws BusinessException {
		KPaging<StockTransVO> kPaging = new KPaging<StockTransVO>();
		kPaging.setPageSize(10);
		stockMgr.getListStockTransVO(kPaging, null, null, null, null);
	}
	
	@Test
	public void getListStockTotalVO() throws BusinessException {
//		KPaging<StockTotalVO> kPaging = new KPaging<StockTotalVO>();
//		kPaging.setPageSize(10);
//		ObjectVO<StockTotalVO> lst = stockMgr.getListStockTotalVO(kPaging, null, null, null, Long.valueOf(15), StockObjectType.SHOP , null, null, null, null, null,182L);
//		System.out.println(lst);
	}
	
	@Test
	public void createListStockTransDetail() throws BusinessException {
		
//		stockMgr.createListStockTransDetail(stockTrans, lstStockTransDetailVO);
	}
	
	@Test
	public void testStockLock() throws BusinessException {
		StockLock st = stockMgr.getStockLock(6l, 709l);
		int a = 0;
		int b = 0;
	}
	
	@Test
	public void testPrinPXKKVCNB() throws BusinessException{
		RptPXKKVCNB_Stock dataStock = stockMgr.getPXKKVCNB(6l, "", 884l, "a", "b");
		System.out.print("");
	}
}
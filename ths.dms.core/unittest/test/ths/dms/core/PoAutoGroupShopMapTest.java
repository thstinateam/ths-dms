package test.ths.dms.core;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.enumtype.PoAutoGroupDetailVO;

import ths.dms.core.business.PoMgr;
import ths.dms.core.dao.PoAutoGroupShopMapDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class PoAutoGroupShopMapTest {
	@Autowired
	PoAutoGroupShopMapDAO poAutoGroupShopMapMgr;
	
	@Autowired
	PoMgr poMgr;

	@Test
	public void checkExistsShopInGroup() {
		Long shopId = 1l;
		try {
			Boolean result = poAutoGroupShopMapMgr.checkExistsShopInGroup(shopId);
			
			System.out.println("" + result);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	
	
	@Test
	public void createShopPoAuto() {
		try {
			poMgr.createShopPoAuto(226505l, 1541008l, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package test.ths.dms.core;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import test.ths.dms.common.TestUtils;
import ths.dms.core.business.PoMgr;
import ths.dms.core.dao.PoVnmDAO;
import ths.dms.core.dao.PoVnmDetailDAO;
import ths.dms.core.dao.PoVnmLotDAO;

import ths.dms.core.entities.PoAutoGroup;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.PoVnmDetail;
import ths.dms.core.entities.PoVnmLot;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailVO;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.vo.CreatePoVnmDetailVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PoAutoGroupByCatVO;
import ths.dms.core.entities.vo.PoAutoVO;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.entities.vo.ProductInfoVOEx;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class PoMgrTest {

	@Autowired
	private PoMgr poMgr;

	@Autowired
	private PoVnmDAO poVnmDAO;

	@Autowired
	private PoVnmDetailDAO poVnmDetailDAO;

	@Autowired
	private PoVnmLotDAO poVnmLotDAO;
	
	@Test
	public void testUpdateInputPoVnm() throws BusinessException {
		//poMgr.updateInputPoVnm(6l, 3456l, "84200123_3452036", "014109-558579", 28800, BigDecimal.valueOf(155520000l), BigDecimal.ZERO, "TL40041");
		System.out.println("=========================");
	}
	
	@Test
	public void testGetListPoConfirmBySaleOrderNumber() throws DataAccessException {
		//List<PoVnm> listPoVnm = poVnmDAO.getListPoConfirmBySaleOrderNumber("10001377586", PoVNMStatus.IMPORTED);
		//System.out.println(listPoVnm.size());
		System.out.println("=================================");
	}

	@Test
	public void getDataForComparePOAutoReport() throws BusinessException,
			DataAccessException {
		List<PoAutoGroupByCatVO> lst = poMgr.getDataForComparePOAutoReport(
				13l, 153l);
		System.out.println("con ga");
	}

	@Test
	public void getPoConfirmDetail() throws BusinessException,
			DataAccessException {
		ObjectVO<PoVnmDetailLotVO> vo = poMgr.getPoConfirmDetail(null, 60202l,
				600024l);
		System.out.println("con ga");
	}

	@Test
	public void updatePoConfirm() throws BusinessException, DataAccessException {
		PoVnm poConfirm = poMgr.getPoVnmById(71l);
		List<CreatePoVnmDetailVO> lstPoConfirmDetail = new ArrayList<CreatePoVnmDetailVO>();
		List<PoVnmDetail> listDetail = poVnmDetailDAO
				.getListPoVnmDetailByPoVnmId(poConfirm.getId());
		for (PoVnmDetail poVnmDetail : listDetail) {
			List<PoVnmLot> listLot = poVnmLotDAO
					.getListPoVnmLotByPoVnmId(poConfirm.getId());
			CreatePoVnmDetailVO child = new CreatePoVnmDetailVO();
			child.setPoVnmDetail(poVnmDetail);
			child.setListPoVnmLot(listLot);
			lstPoConfirmDetail.add(child);
		}
		poMgr.createPoConfirm(poConfirm, lstPoConfirmDetail);
		System.out.println("con ga");
	}


	@Test
	public void getListPoAuto() throws BusinessException,
			DataAccessException {
		Long shopId = 13l;
		Boolean checkAll = true;
		
		try {
			List<PoAutoVO> listData = poMgr.getListPoAuto(shopId,
					checkAll);

			if (null != listData && listData.size() > 0) {
				for(PoAutoVO item : listData){
					TestUtils.getValueOfObject(item);
					System.out.println("\n");
				}
			} else {
				System.out.println("not have data ...");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Test
	public void createShopPoAuto() throws BusinessException,
			DataAccessException {
		Long shopId = 15l;
		Long staffId = 1000000l;
		Boolean checkAllProductType = true;
		
		try {
			poMgr.createShopPoAuto(shopId, staffId, checkAllProductType);
			
			System.out.println("success ...");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Test
	public void getListPoAutoGroupDetailVO() throws BusinessException,
			DataAccessException {
		Long shopId = 16l;
		Long staffId = 9l;
		Boolean checkAllProductType = true;
		try {
			PoAutoGroupDetailFilter filter = new PoAutoGroupDetailFilter();
			filter.setPoAutoGroupId(201L);
			filter.setObjectType(1);
			
			ObjectVO<PoAutoGroupDetailVO> lst = poMgr.getListPoAutoGroupDetailVO(filter, null);
			
			System.out.println("success ...");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Test
	public void isExistsInfoOfPoAutoGroup() throws BusinessException,
			DataAccessException {
		PoAutoGroup po = new PoAutoGroup();
		po.setId(100l);
		try {
			Boolean res = poMgr.isExistsInfoOfPoAutoGroup(po);
			
			System.out.println(res);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Test
	public void getListPoVnmDetailLotVOByPoVnmId() throws BusinessException,
			BusinessException {
		KPaging<PoVnmDetailLotVO> kPaging = null;
		Long poVnmId = 600109l;
//		try {
//			poMgr.getListPoVnmDetailLotVOByPoVnmId(kPaging, poVnmId);

//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
	}
	@Test
	public void getListProductInfoCanBeAdded() throws BusinessException {
		KPaging<ProductInfoVOEx> kPaging = new KPaging<ProductInfoVOEx>();
		kPaging.setPageSize(10);
		ObjectVO<ProductInfoVOEx> lst = poMgr.getListProductInfoCanBeAdded(Integer.valueOf(261), null, null, true, kPaging);
		System.out.println(lst);
	}
}

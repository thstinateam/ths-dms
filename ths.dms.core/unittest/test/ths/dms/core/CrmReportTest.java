package test.ths.dms.core;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTotalVOFilter;
import ths.core.entities.vo.rpt.RptBCKD11_2VO;
import ths.core.entities.vo.rpt.RptBCKD13VO;
import ths.core.entities.vo.rpt.RptBCKD18VO;
import ths.core.entities.vo.rpt.RptBCKD6;
import ths.core.entities.vo.rpt.RptBCKD6VO;
import ths.core.entities.vo.rpt.RptDSBHMienVO;
import ths.core.entities.vo.rpt.RptDSPPNHVO;
import ths.core.entities.vo.rpt.RptDiemNVBHVO;
import ths.core.entities.vo.rpt.Rpt_HO_Kho_F1;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.core.business.StockMgr;
import ths.dms.core.report.CrmReportMgr;
import ths.dms.core.report.HoReportMgr;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class CrmReportTest {

	
	@Autowired
	private CrmReportMgr crmReportMgr;
	
	@Autowired
	private StockMgr stockMgr;
	
	@Autowired
	private HoReportMgr hoReportMgr;
	
	
	@Test
	public void testStock() throws BusinessException {
		long a = System.currentTimeMillis();
		
		
		StockTotalVOFilter filter = new StockTotalVOFilter();
		filter.setOwnerId(1l);
		filter.setOwnerType(StockObjectType.SHOP);
		List<StockTotal> lst = (List<StockTotal>) stockMgr.getListStockTotalNew(null, filter);
		
		System.out.println(lst.size());
		
		long b = System.currentTimeMillis();
		System.out.println(b -a);
	}
	
	@Test
	public void getDSPPNHReport() {
		try {
			Calendar calFrom = Calendar.getInstance();
			calFrom.set(Calendar.YEAR, 2013);
			calFrom.set(Calendar.MONTH, 0);
			calFrom.set(Calendar.DATE, 1);
			Calendar calTo = Calendar.getInstance();
			calTo.set(Calendar.YEAR, 2013);
			calTo.set(Calendar.MONTH, 0);
			calTo.set(Calendar.DATE, 7);
			RptDSPPNHVO res = crmReportMgr.getDSPPNHReport( 15l,null, calTo.getTime());
			System.out.print(res);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void getDSBHMienReport() {
		try {
			Calendar calFrom = Calendar.getInstance();
			calFrom.set(Calendar.YEAR, 2013);
			calFrom.set(Calendar.MONTH, 0);
			calFrom.set(Calendar.DATE, 1);
			Calendar calTo = Calendar.getInstance();
			calTo.set(Calendar.YEAR, 2013);
			calTo.set(Calendar.MONTH, 2);
			calTo.set(Calendar.DATE, 15);
			RptDSBHMienVO res = crmReportMgr.getDSBHMienReport(calTo.getTime());
			System.out.print(res);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void getRptBCKD11_2() {
		try {
			Calendar calFrom = Calendar.getInstance();
			calFrom.set(2013,1,10);
			Calendar calTo = Calendar.getInstance();
			calTo.set(2013,1,20);
			RptBCKD11_2VO res = crmReportMgr.getRptBCKD11_2( calFrom.getTime(), calTo.getTime());
			System.out.print(res);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void getRptBCKD18() {
		try {
			Calendar calFrom = Calendar.getInstance();
			calFrom.set(2013,0,10);//20/02/2013
			Calendar calTo = Calendar.getInstance();
			calTo.set(2013,2,18);//25/02/2013
			RptBCKD18VO res = crmReportMgr.getRptBCKD18(calFrom.getTime(), calTo.getTime());
			System.out.println(res.getLstDetail().size());
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void getDiemNVBHTheoTuyenReport() {
		try {
			Calendar calFrom = Calendar.getInstance();
			calFrom.set(2013,1,20);//20/02/2013
			Calendar calTo = Calendar.getInstance();
			calTo.set(2013,1,25);//25/02/2013
			RptDiemNVBHVO res = crmReportMgr.getDiemNVBHTheoTuyenReport( 15l, null,calFrom.getTime(), calTo.getTime());
			System.out.println(res.getLstDetail().size());
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void getBCKD6() throws BusinessException {
		Calendar calFrom = Calendar.getInstance();
		calFrom.set(Calendar.YEAR, 2013);
		calFrom.set(Calendar.MONTH, 1);
		calFrom.set(Calendar.DATE, 1);
		Calendar calTo = Calendar.getInstance();
		calTo.set(Calendar.YEAR, 2013);
		calTo.set(Calendar.MONTH, 1);
		calTo.set(Calendar.DATE, 18);
		RptBCKD6 res = crmReportMgr.getRptBCKD6(calFrom.getTime(), calTo.getTime());
		for (RptBCKD6VO vo: res.getLstDetail()) {
			System.out.println("______________________________");
			System.out.println("TyLeTHKHNhapHang" + vo.getTyLeTHKHNhapHang());
			System.out.println("TyLeTHKHBanHang" + vo.getTyLeTHKHBanHang());
			System.out.println("DoanhSoChenhLech" + vo.getDoanhSoChenhLech());
			System.out.println("ChenhLechNgayBan" + vo.getChenhLechNgayBan());
			System.out.println("PhanTramTangTruong" + vo.getPhanTramTangTruong());
		}
		
	}
	
	@Test
	public void getRptBCKD1_1() throws BusinessException {
		crmReportMgr.getRptBCKD1_1(Arrays.asList(1L,2L), Arrays.asList(1L,2L), new Date());
	}
	
	@Test
	public void getRptBCKD13() throws BusinessException {
		Calendar c = Calendar.getInstance();
		c.set(2013, 1, 1);
		
		Calendar cc = Calendar.getInstance();
		cc.set(2013, 4, 1);
		
		List<RptBCKD13VO> lst = crmReportMgr.getRptBCKD13(21L, null, c.getTime(), cc.getTime());
		System.out.println(lst.size());
	}
}

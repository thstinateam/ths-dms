package test.ths.dms.core;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.PayReceived;
import ths.dms.core.entities.PaymentDetail;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.enumtype.DebtPaymentType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ReceiptType;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.DebitMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.SaleOrderDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class DebitTest extends AbstractUnitTest {

	@Autowired
	SaleOrderDAO saleOrderDAO;
	
	@Autowired
	ShopMgr shopMgr;
	
	@Autowired
	CustomerMgr customerMgr;
	
	@Autowired
	DebitMgr debitMgr;
	
	@Autowired
	StaffMgr staffMgr;
	
	@Autowired
	SaleOrderMgr saleOrderMgr;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Test
	public void updateSaleOrder() throws BusinessException, DataAccessException {
		List<Long> listId = new ArrayList<Long>();
		listId.add(166L);
		listId.add(160L);
//		saleOrderMgr.approveListSaleOrderByListSaleOrderId(listId, "laksjdlaksd", null);
//		System.out.println("fasdfjaskljkfldsa");
	}
	
	
	@Test
	public void createSaleOrder() throws DataAccessException, BusinessException, ParseException {
//		SaleOrder so = new SaleOrder();
//		
//		so.setShop(shopMgr.getShopById(Long.valueOf(18)));
//		so.setCustomer(customerMgr.getCustomerById(Long.valueOf(12)));
//		so.setStaff(staffMgr.getStaffById(5L));
//		
//		so.setAmount(new BigDecimal(1500));
//		so.setDiscount(new BigDecimal(1500));
//		so.setTotal(new BigDecimal(1000));
//		so.setCreateUser("123");
//		SaleOrder so1 = saleOrderDAO.getSaleOrderById(12L);
//		so1.setFromSaleOrder(so1);
//		so1.setId(null);
//		saleOrderDAO.createSaleOrder(so1);
		
//		SaleOrder so1 = saleOrderDAO.getSaleOrderById(427L);
//		saleOrderMgr.updateReturnSaleOrder(427L, "Minh Hung");
		
//		SaleOrder tempsaleOrder = saleOrderDAO.getSaleOrderById(25L);
//		SaleOrder fromsaleOrder = saleOrderDAO.getSaleOrderById(25L);
//		tempsaleOrder.setFromSaleOrder(fromsaleOrder);
//		tempsaleOrder.setType(SaleOrderType.RETURNED_ORDER);
//		tempsaleOrder.setOrderType(OrderType.CM);
//		tempsaleOrder.setApproved(SaleOrderStatus.APPROVED);
//		tempsaleOrder.setId(null);
//		SaleOrder saleOrder = saleOrderDAO.createSaleOrder(tempsaleOrder);
//		System.out.println(saleOrder.getId());
		
//		so.setOrderNumber("ON123");
//		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
//		so.setOrderDate(formatter.parse("02/02/2002"));
//		saleOrderDAO.createSaleOrder(so);
	}
	
	@Test
	public void getListPositiveDebit() throws BusinessException {
		KPaging<DebitDetail> kPaging = new KPaging<DebitDetail>();
		kPaging.setPageSize(10);
		kPaging.setPage(0);
		kPaging.setTotalPages(10);
//		ObjectVO<Debit> vo = debitMgr.getListPositiveDebit(kPaging, null, null, 
//				DebtType.CUSTOMER, new BigDecimal(0), new BigDecimal(2000), 1L);
//		System.out.println("SIZE=" + vo.getLstObject().size());
	}
	
	@Test
	public void createPayReceived() throws BusinessException {
		PayReceived p = new PayReceived();
		p.setAmount(new BigDecimal(700));
		p.setCustomer(customerMgr.getCustomerById(Long.valueOf(41)));
		p.setShop(shopMgr.getShopById(Long.valueOf(122)));
		p.setReceiptType(ReceiptType.RECEIVED);
		
		PaymentDetail dd = new PaymentDetail();
		dd.setAmount(new BigDecimal(200));
		dd.setPaymentType(DebtPaymentType.MANUAL);
		dd.setDebit(debitMgr.getDebitDetailById(Long.valueOf(21)));
		
		List<PaymentDetail> lstPaymentDetail = new ArrayList<PaymentDetail>();
		lstPaymentDetail.add(dd);
		
		debitMgr.createPayReceived(p, lstPaymentDetail);
	}
}
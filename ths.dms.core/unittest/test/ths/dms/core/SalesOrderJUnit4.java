package test.ths.dms.core;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.PlanType;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SalePlanOwnerType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.RptDSKHTMHCustomerAndListInfoVO;
import ths.dms.core.entities.vo.RptDSKHTMHRecordProductVO;
import ths.dms.core.entities.vo.SumSalePlanVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.SalePlanMgr;
import ths.dms.core.dao.SaleOrderDAO;
import ths.dms.core.report.ShopReportMgr;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class SalesOrderJUnit4 {

	@Autowired
	private SalePlanMgr salePlanMgr;
	
	@Autowired
	private SaleOrderMgr saleOrderMgr;
	
	@Autowired
	private SaleOrderDAO saleOrderDAO;
	@Autowired
	private ShopReportMgr shopReportMgr;
	
	@Test
	public void testLastOrder() throws Exception {
		String str = saleOrderDAO.getLastestOrderNumberByShop(6l);
		System.out.println(str);
	}
	
	@Test
	public void test() {
		try {
			SumSalePlanVO tmp = salePlanMgr.getSumSalePlan(21l, null, SalePlanOwnerType.SHOP, PlanType.MONTH, 1, 2013);
			System.out.print(tmp);
		} catch (Exception e) {

			e.printStackTrace();
		}

	}
	
	@Test
	public void getListSaleOrder() {
		try {
			ObjectVO<SaleOrder> res;
			SaleOrderFilter filter = new SaleOrderFilter();
			filter.setShopCode("TL40041");
			filter.setApproved(SaleOrderStatus.APPROVED);
			filter.setIsCompareEqual(true);
			filter.setOrderType(OrderType.IN);
			KPaging<SaleOrder> kPaging = new KPaging<SaleOrder>(5);
			kPaging.setPage(0);
			res = saleOrderMgr.getListSaleOrder(filter, kPaging);
			System.out.print("con ga");
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	@Test
	public void getListRptDSKHTMHRecordProductVO() {
		try {
			List<RptDSKHTMHRecordProductVO> res = saleOrderDAO.getListRptDSKHTMHRecordProductVO(16L, null,
					null, null);
			System.out.println("con ga size = " + res.size());
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
}
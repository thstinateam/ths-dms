package test.ths.dms.core;

import org.apache.log4j.PropertyConfigurator;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public abstract class AbstractUnitTest {
	@BeforeClass
	public static void init() {
		// Khoi tao log4j
		PropertyConfigurator.configure("conf/log4j.properties");

	}
}

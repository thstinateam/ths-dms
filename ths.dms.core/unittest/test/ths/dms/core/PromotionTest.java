package test.ths.dms.core;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.PromotionShopMapFilter;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PromotionCustAttrVO;
import ths.dms.core.entities.vo.PromotionShopMapVO;
import ths.dms.core.entities.vo.ZV03View;
import ths.dms.core.entities.vo.ZV07View;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.dao.PromotionProgramDAO;
import ths.dms.core.dao.PromotionShopMapDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class PromotionTest  extends AbstractUnitTest{

	@Autowired
	PromotionProgramDAO promotionProgramDAO;
	
	@Autowired
	PromotionShopMapDAO promotionShopMapDAO;
	
	@Autowired
	SaleOrderMgr saleOrderMgr;
	
	@Test
	public void getListPromotionCustAttrVOCanBeSet() throws BusinessException{
		List<PromotionCustAttrVO> res = null;
	}
	
	//lay danh sach cac thuoc tinh khach hang da set cho CTKM
	@Test
	public void getListPromotionCustAttrVOAlreadySet() throws BusinessException{
		List<PromotionCustAttrVO> res = null;
		try {
			res = promotionProgramDAO.getListPromotionCustAttrVOCanBeSet(null, 22);
			System.out.println(res.size());
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void checkCTHTTMShopMap() throws Exception {
		boolean a = promotionShopMapDAO.checkCTHMShopMap("HUONGNTDOI", 6l, ActiveType.RUNNING, new Date(2014, 3, 20));
		System.out.println("=========================");
	}
	
	@Test
	public void zv03() throws DataAccessException {
		KPaging<ZV03View> paging = new KPaging<ZV03View>();
		paging.setPageSize(50);
		List<ZV03View> lst = promotionProgramDAO.getZV03ViewHeader(paging, 112l);
		lst = promotionProgramDAO.getZV03ViewDetail(paging, 112l, 441l, 5);
		lst = promotionProgramDAO.getZV06ViewHeader(paging, 112l);
		lst = promotionProgramDAO.getZV06ViewDetail(paging, 112l, 441l, 5l);
		System.out.println(lst.size());
		
		KPaging<ZV07View> paging7 = new KPaging<ZV07View>();
		paging7.setPageSize(50);
		List<ZV07View> abc = promotionProgramDAO.getZV07ViewHeader(paging7, 204l);
		abc = promotionProgramDAO.getZV07ViewDetail(paging7, 204l, 5f, 50);
		abc = promotionProgramDAO.getZV08ViewHeader(paging7, 204l);
		abc = promotionProgramDAO.getZV08ViewDetail(paging7, 204l, new BigDecimal(10), 50);
		abc = promotionProgramDAO.getZV09ViewHeader(paging7, 139l);
		abc = promotionProgramDAO.getZV09ViewSaleProductDetail(paging7, 139l, 10);
		abc = promotionProgramDAO.getZV09ViewFreeProductDetail(paging7, 139l, 10);
		abc = promotionProgramDAO.getZV10ViewHeader(paging7, 662l);
		abc = promotionProgramDAO.getZV10ViewDetail(paging7, 662l, new BigDecimal(100000l));
		abc = promotionProgramDAO.getZV11ViewHeader(paging7, 662l);
		abc = promotionProgramDAO.getZV11ViewDetail(paging7, 662l, new BigDecimal(100000l));
		abc = promotionProgramDAO.getZV12ViewHeader(paging7, 139l);
		abc = promotionProgramDAO.getZV12ViewSaleProductDetail(paging7, 139l, new BigDecimal(10));
		abc = promotionProgramDAO.getZV12ViewFreeProductDetail(paging7, 139l, new BigDecimal(10));
		abc = promotionProgramDAO.getZV13ViewHeader(paging7, 662l);
		abc = promotionProgramDAO.getZV13ViewDetail(paging7, 662l, 5f);
		abc = promotionProgramDAO.getZV14ViewHeader(paging7, 662l);
		abc = promotionProgramDAO.getZV14ViewDetail(paging7, 662l, new BigDecimal(1000));
		
		abc = promotionProgramDAO.getZV15ViewHeader(paging7, 139l);
		abc = promotionProgramDAO.getZV15ViewSaleProductDetail(paging7, 139l, "123");
		abc = promotionProgramDAO.getZV15ViewFreeProductDetail(paging7, 139l, "123");
		
		abc = promotionProgramDAO.getZV16ViewHeader(paging7, 662l);
		abc = promotionProgramDAO.getZV16ViewDetail(paging7, 662l, 4.5f);
		
		abc = promotionProgramDAO.getZV17ViewHeader(paging7, 662l);
		abc = promotionProgramDAO.getZV17ViewDetail(paging7, 662l, new BigDecimal(12));
		
		abc = promotionProgramDAO.getZV18ViewHeader(paging7, 139l);
		abc = promotionProgramDAO.getZV18ViewSaleProductDetail(paging7, 139l, "123");
		abc = promotionProgramDAO.getZV18ViewFreeProductDetail(paging7, 139l, "123");
		
		abc = promotionProgramDAO.getZV21ViewHeader(paging7, 112l);
		abc = promotionProgramDAO.getZV21ViewDetail(paging7, 112l, new BigDecimal(100));
		
		System.out.println(abc.size());
	}
	
	@Test
	public void saleOrder() throws BusinessException {
		SoFilter filter = new SoFilter();
		filter.setSaleOrderType(SaleOrderType.NOT_YET_RETURNED);
		filter.setShopId(21l);
		filter.setApproval(SaleOrderStatus.APPROVED);
		filter.setListOrderType(Arrays.asList(OrderType.IN));
		ObjectVO<SaleOrder> vo = saleOrderMgr.getListSaleOrderToReturn(filter);
		System.out.println(vo);
	}
	
	@Test
	public void getListPromotionShopMapVO() throws DataAccessException {
//		PromotionShopMapFilter filter = new PromotionShopMapFilter();
//		filter.setProgramId(19l);
//		List<PromotionShopMapVO> vo = promotionShopMapDAO.getListPromotionShopMapVO(filter, null);
//		System.out.println("Successs");
	}
}

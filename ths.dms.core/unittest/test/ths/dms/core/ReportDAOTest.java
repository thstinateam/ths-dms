package test.ths.dms.core;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.enumtype.InvoiceCustPayment;
import ths.dms.core.entities.enumtype.KPaging;
import ths.core.entities.vo.rpt.RptSaleOrderLotVO;
import ths.core.entities.vo.rpt.RptSaleOrderVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.InvoiceDAO;
import ths.dms.core.dao.PayReceivedDAO;
import ths.dms.core.dao.SaleOrderDAO;
import ths.dms.core.dao.SaleOrderDetailDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class ReportDAOTest extends AbstractUnitTest {

	@Autowired
	SaleOrderDetailDAO saleOrderDetailDAO;
	
	@Autowired
	SaleOrderDAO saleOrderDAO;
	
	@Autowired
	InvoiceDAO invoiceDAO;
	
	@Autowired
	PayReceivedDAO payReceivedDAO;
	
	@Test
	public void getListSaleOrderLotVO() throws DataAccessException {
		KPaging<RptSaleOrderLotVO> kPaging = new KPaging<RptSaleOrderLotVO>();
		kPaging.setPageSize(10);
		List<RptSaleOrderLotVO> lst = saleOrderDetailDAO.getListSaleOrderLotVO(kPaging, 1L, 1L, new Date(), new Date(), 1, true, 1L);
		System.out.println(lst.size());
		lst = saleOrderDetailDAO.getListSaleOrderLotVO(kPaging, null, null, null, null, 1, true, null);
		System.out.println(lst.size());
	}
	
	@Test 
	public void getListRptSaleOrderVO() throws DataAccessException {
		KPaging<RptSaleOrderVO> kPaging = new KPaging<RptSaleOrderVO>();
		kPaging.setPageSize(10);
		saleOrderDAO.getListRptSaleOrderVO(kPaging, 1L, 1L, new Date(), new Date(), true);
		saleOrderDAO.getListRptSaleOrderVO(kPaging, null, null, null, null, null);
//		invoiceDAO.getListInvoice(null, 1.1F, "123", new Date(), new Date(), "123", "123", InvoiceCustPayment.BANK_TRANSFER);
//		invoiceDAO.getListInvoice(null, null, null, null, null, null, null, null);
		payReceivedDAO.getListRptCustomerPayReceivedVO(null, 1L, new Date(), new Date(), 1L);
		payReceivedDAO.getListRptCustomerPayReceivedVO(null, null, null, null, null);
	}
	
	
}
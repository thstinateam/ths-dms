package test.ths.dms.core;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class TestUtil {
	public static void main(String[] args) throws Exception {
		String query = " SELECT pa.po_auto_date AS poAutoDate, pa.po_auto_number AS poAutoNumber, pa.status      AS status, pad.quantity   AS quantity, pad.price      AS priceValue, pad.amount     AS amount, p.product_code AS productCode, p.product_name AS productName, p.uom1         AS uom1, p.gross_weight AS grossWeight FROM po_auto pa, po_auto_detail pad, product p WHERE pa.po_auto_id = pad.po_auto_id AND pad.product_id  = p.product_id AND (pa.status      = ? OR pa.status        = ?) and pa.shop_id = ? and pa.po_auto_date >= trunc(?) and pa.po_auto_date < (trunc(?) + 1) order by pa.po_auto_number, p.product_code";
		Object[] params = {0, 1, 15, "Sat Sep 01 10:00:37 ICT 2012", "Tue Oct 01 10:00:37 ICT 2013"};
//		System.out.println(addParamToQuery(query, params));
	}
	
	static public String addParamToQuery(String query, List<Object> params) {
		for (Object obj: params) {
			String temp = obj.toString();
			try {
				Integer.parseInt(temp);
			}
			catch (Exception ex) {
				try {
					DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH);
					Date date = df.parse(temp);
					df = new SimpleDateFormat("EEE MMM dd kk:mm:ss yyyy", Locale.ENGLISH);
					temp = "to_date('" + df.format(date) + "', 'DY MON DD HH24:MI:SS YYYY')";
				}
				catch (Exception e) {
					temp = "'" + temp + "'";
				}
			}
			
			query = query.replaceFirst("\\?", temp);
		}
		return query;
	}
	
	static public void getMD5(){
		String password = "123456";
        MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
		
	        md.update(password.getBytes());
	 
	        byte byteData[] = md.digest();
	 
	        //convert the byte to hex format method 1
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < byteData.length; i++) {
	         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }
	 
	        System.out.println("Digest(in hex format):: " + sb.toString());
	 
	        //convert the byte to hex format method 2
	        StringBuffer hexString = new StringBuffer();
	    	for (int i=0;i<byteData.length;i++) {
	    		String hex=Integer.toHexString(0xff & byteData[i]);
	   	     	if(hex.length()==1) hexString.append('0');
	   	     	hexString.append(hex);
	    	}
	    	System.out.println("Digest(in hex format):: " + hexString.toString());
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static public void addFormReportByCMS() {
		/**
		 * VNM: 2223
		 * */
		
	}
	
}

package test.ths.dms.core;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.Price;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.enumtype.ProgramObjectType;
import ths.dms.core.entities.vo.ChannelTypeVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.entities.vo.ProductInfoVO;
import ths.dms.core.entities.vo.ProductTreeVO;
import ths.dms.core.entities.vo.SaleCatLevelVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.PromotionProgramMgr;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class ProductTest {

	@Autowired
	PromotionProgramMgr promotionProgramMgr;
	
	@Autowired
	ProductMgr productMgr;

	@Test
	public void test() throws BusinessException, DataAccessException {
		//			List<SaleCatLevelVO> ls = promotionProgramMgr
		//					.getListSaleCatLevelVOByIdProAlreadySet(null, 2);
					Pattern p = Pattern.compile("^\\d+$");
					Matcher m = p.matcher("ewewewew21323213");
		
					boolean matchFound = m.matches();
					System.out.println(matchFound);
	}

	@Test
	public void getListProductTreeVO() throws BusinessException {
		String productCode = null;
		String productName = null;
		Long incentiveProgramId = null;
		Long focusProgramId = 34l;
		Long displayProgramId = null;
		try {
			List<ProductTreeVO> listVO = productMgr.getListProductTreeVO(productCode, productName, focusProgramId, ProgramObjectType.FOCUS_PROGRAM, true, DisplayProductGroupType.DS);
			if (null == listVO) {
				System.out.println("List attribute is empty");
			} else {
				System.out.println("ss ...");
			}
		} catch (Exception e) {
			System.out.println("failure ...");
		} finally {
		}
	}
}

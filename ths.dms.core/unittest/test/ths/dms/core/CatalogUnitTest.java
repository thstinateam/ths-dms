package test.ths.dms.core;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import test.ths.dms.common.TestUtils;
import ths.dms.core.business.SaleLevelCatMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.dao.CustomerDAO;
import ths.dms.core.dao.PromotionProgramDAO;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.ProductGroup;
import ths.dms.core.entities.SaleLevelCat;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ParamsDAOGetListCustomer;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class CatalogUnitTest {

	@Autowired
	SaleLevelCatMgr saleLevelCatMgr;
	
	@Autowired
	PromotionProgramDAO promotionProgramDAO;
	
	@Autowired
	ShopMgr shopMgr;
	
	@Autowired
	CustomerDAO customerDAO;
	
	@Test
	public void testGetListCustomer(){
		try {
			ParamsDAOGetListCustomer funcParams = new ParamsDAOGetListCustomer();
			funcParams.setShopId(2660L);
			funcParams.setStatus(ActiveType.RUNNING);
			List<Customer> lst = customerDAO.getListCustomer(funcParams);
			System.out.println(lst);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Test
	public void testGetProductGroup() throws Exception {
		//ProductGroup productGroup = promotionProgramDAO.getProductGroupByCode("NKM1", 92849l);
		System.out.println("fuck");
	}
	
	@Test
	public void hauPV3Test() throws Exception{
		try {
			Shop sh = shopMgr.getShopByCodeNEW("MB");
			System.out.println(sh.getShopCode());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

	@Test
	public void getListSaleLevelCatByProductInfo() throws BusinessException,
			DataAccessException {
		KPaging<SaleLevelCat> kPaging = new KPaging<SaleLevelCat>(5);
		List<String> listProInfoCode = new ArrayList<String>();
		String saleLevelCode = "M9";
		String saleLevelName = null;
		BigDecimal fromAmount = new BigDecimal(100000);
		BigDecimal toAmount = new BigDecimal(500);
		ActiveType status = ActiveType.RUNNING;

		listProInfoCode.add("A1");
		kPaging.setPage(0);

		try {
			/*ObjectVO<SaleLevelCat> result = saleLevelCatMgr
					.getListSaleLevelCatByProductInfo(kPaging, listProInfoCode,
							saleLevelCode, saleLevelName, fromAmount, toAmount,
							status);

			if (null != result && result.getLstObject() != null
					&& result.getLstObject().size() > 0) {
				for (SaleLevelCat object : result.getLstObject()) {
					TestUtils.getValueOfObject(object);
				}
			} else {
				System.out.println("Resutl is null");
			}*/
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

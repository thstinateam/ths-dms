package test.ths.dms.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import test.ths.dms.common.ThuatTQAbstractUnitTest;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.FocusProgramMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.dao.FocusProgramDAO;

import ths.dms.core.entities.FocusProgram;
import ths.dms.core.entities.FocusShopMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.FocusProgramFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.NewTreeVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class FocusProgramTest extends ThuatTQAbstractUnitTest {
	@Autowired
	FocusProgramMgr focusProgramMgr;
	
	@Autowired
	FocusProgramDAO focusProgramDAO;
	
	@Autowired
	CommonMgr commonMgr;
	
	@Autowired
	ShopMgr shopMgr;
	
	private SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

	@Test
	public void getShopTreeInFocusProgram() throws BusinessException,
			ParseException {
		Long focusProgramId = 118l;
		String shopCode = null;
		String shopName = null;
		System.out.println("Success ...");
	}
	
	@Test
	public void hasChildShopJoinFocusProgram() throws DataAccessException {
		Long focusProgramId = null;
		Long shopId = null;

		Boolean result = this.focusProgramDAO.hasChildShopJoinFocusProgram(
				focusProgramId, shopId);

		if (Boolean.TRUE.equals(result)) {
			System.out.println("");
		} else {
			System.out.println("");
		}
	}
	
	@Test
	public void getShopTreeVOForFocusProgramEx() throws BusinessException,
			ParseException {
		Long shopId = null;
		List<Long> exceptionalShopId = null;
		String shopCode = null;
		String shopName = null;
		ActiveType status = null;
		Long focusProgramId = null;

		System.out.println("Success ...");
	}
	
	@Test
	public void copyFocusProgram() throws BusinessException {
		Long focusProgramId = 41l;
		LogInfoVO logInfo = new LogInfoVO();
		String fpCode = "thuattq";
		String fpName = "thuattq";
		
		logInfo.setIp("127.0.0.1");
		logInfo.setStaffCode("VNM_DVKH");

		try {
			FocusProgram result = this.focusProgramMgr.copyFocusProgram(
					focusProgramId, logInfo, fpCode, fpName);
			
			System.out.println("" + result.getFocusProgramCode());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Test
	public void createFocusShopMap() throws BusinessException {
		FocusShopMap focusShopMap = new FocusShopMap();
		FocusProgram focusProgram = this.focusProgramMgr.getFocusProgramById(78l);
		Shop shop = this.shopMgr.getShopById(12l);
		LogInfoVO logInfo = new LogInfoVO();

		logInfo.setIp("127.0.0.1");
		logInfo.setStaffCode("VNM_DVKH");
		
		focusShopMap.setFocusProgram(focusProgram);
		focusShopMap.setShop(shop);
		focusShopMap.setStatus(ActiveType.RUNNING);
		focusShopMap.setCreateDate(this.commonMgr.getSysDate());
		focusShopMap.setCreateUser(logInfo.getStaffCode());

		try {
			FocusShopMap result = this.focusProgramMgr.createFocusShopMap(
					focusShopMap, logInfo);

			System.out.println("" + result.getId());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Test
	public void createListFocusShopMap() throws BusinessException {
		FocusProgram focusProgram = this.focusProgramMgr
				.getFocusProgramById(78l);
		LogInfoVO logInfo = new LogInfoVO();

		logInfo.setIp("127.0.0.1");
		logInfo.setStaffCode("VNM_DVKH");

		List<FocusShopMap> list = new ArrayList<FocusShopMap>();
		
		FocusShopMap focusShopMap1 = new FocusShopMap();
		focusShopMap1.setFocusProgram(focusProgram);
		focusShopMap1.setShop(this.shopMgr.getShopById(127l));
		focusShopMap1.setStatus(ActiveType.RUNNING);
		focusShopMap1.setCreateDate(this.commonMgr.getSysDate());
		focusShopMap1.setCreateUser(logInfo.getStaffCode());
		list.add(focusShopMap1);
		
		FocusShopMap focusShopMap2 = new FocusShopMap();
		focusShopMap2.setFocusProgram(focusProgram);
		focusShopMap2.setShop(this.shopMgr.getShopById(21l));
		focusShopMap2.setStatus(ActiveType.RUNNING);
		focusShopMap2.setCreateDate(this.commonMgr.getSysDate());
		focusShopMap2.setCreateUser(logInfo.getStaffCode());
		list.add(focusShopMap2);

		try {
			this.focusProgramMgr.createListFocusShopMap(list, logInfo);

			System.out.println("success ...");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Test
	public void getListFocusProgram() throws BusinessException, ParseException {
		KPaging<FocusProgram> kPaging = null;
		FocusProgramFilter filter = new FocusProgramFilter();
		filter.setShopCode("MN");
		filter.setStatus(ActiveType.RUNNING);
		try {
			ObjectVO<FocusProgram> result = focusProgramMgr.getListFocusProgram(kPaging, filter, StaffRole.VNM_SALESONLINE_HO);
			System.out.println("Success ...");

			if (null != result && result.getLstObject() != null&& result.getLstObject().size() > 0) {
				for (FocusProgram focus : result.getLstObject()) {
					if (null != focus.getFromDate()) {
						/*System.out.println(""
								+ format.format(focus.getFromDate()));*/
						System.out.println(""+ focus.getFocusProgramCode());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

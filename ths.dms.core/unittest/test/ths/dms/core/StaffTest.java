package test.ths.dms.core;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.filter.FeedbackFilter;
import ths.dms.core.entities.vo.FeedbackVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.business.FeedbackMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.common.utils.Constant;
import ths.dms.core.dao.StaffDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class StaffTest {

	@Autowired
	private StaffDAO staffDAO;
	
	@Autowired
	private StaffMgr staffMgr;
	
	@Autowired
	private FeedbackMgr feedbackMgr;
	
	@Test
	public void getListProductForPoConfirm() throws BusinessException, DataAccessException {
		List<Staff> l = staffDAO.getListStaffForAbsentReport(null, null, null, 15l);
		System.out.println("con ga");
	}
	
	
	@Test
	public void testGet() throws BusinessException, DataAccessException {
		List<StaffPositionVO> lstStaff = staffMgr.getListStaffPosition1(1L, 1L, StaffObjectType.TBHM, true);
		System.out.println(lstStaff);
		
		List<StaffPositionVO> __lstStaff = staffMgr.getListStaffPosition1(null, 1L, StaffObjectType.NHVNM, true);
		System.out.println(__lstStaff);
	}
	
	@Test
	public void testGetFeed() throws BusinessException, DataAccessException {
		FeedbackFilter filter = new FeedbackFilter();
		filter.setCreateUserId(1L);
		filter.setFlagGetStaffId(Constant.FEEDBACK_CREATE_AND_OWNER_STAFF_ID);
		List<FeedbackVO> lstFeedbackIdOfStaff = feedbackMgr.getListFeedbackIdCheckByFilter(filter);
		System.out.println(lstFeedbackIdOfStaff);
		System.out.println(lstFeedbackIdOfStaff.toArray());
		System.out.println(lstFeedbackIdOfStaff.size());
	}
}

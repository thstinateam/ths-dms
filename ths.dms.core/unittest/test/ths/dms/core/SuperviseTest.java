/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
 /**
 * @author vuongmq
 * @date 20/08/2015
 * @description Unit test chuc nang Giam sat ban hang
 */                 
 
package test.ths.dms.core;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActionLogObjectType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.filter.RptStaffSaleFilter;
import ths.dms.core.entities.vo.ActionLogCustomerVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.business.StaffMgr;
import ths.dms.core.dao.ActionLogDAO;
import ths.dms.core.dao.RptStaffSaleDAO;
import ths.dms.core.dao.StaffDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class SuperviseTest {

	@Autowired
	private RptStaffSaleDAO rptStaffSaleDAO;
	
	@Autowired
	private ActionLogDAO actionLogDAO;
	
	@Test
	public void getListActionLogNVBHofCustomer() throws BusinessException, DataAccessException {
		RptStaffSaleFilter filter = new RptStaffSaleFilter();
		filter.setStaffId(127L);
		filter.setCustomerId(658L);
		List<Integer> lstObjetType = new ArrayList<Integer>();
		lstObjetType.add(ActionLogObjectType.VISIT.getValue());
		lstObjetType.add(ActionLogObjectType.END_VISIT.getValue());
		filter.setLstObjetType(lstObjetType);
		filter.setObjetType(null);
		List<ActionLogCustomerVO> lstActionLogCustomerVOs = actionLogDAO.getActionLogCustomerOfStaff(filter);
		if (lstActionLogCustomerVOs != null && lstActionLogCustomerVOs.size() > 0) {
			ActionLogCustomerVO item = lstActionLogCustomerVOs.get(0);
			System.out.println("bd: " + item.getStartTime());
			System.out.println("kt: " + item.getEndTime());
			System.out.println("bdHHMM: " + item.getStartTimeHHMM());
			System.out.println("ktHHMM: " + item.getEndTimeHHMM());
		}
	}
	
}

package test.ths.dms.core;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.ShopVO;
import ths.core.entities.vo.rpt.RptAbsentVO;
import ths.core.entities.vo.rpt.RptBookProductVinamilkVO;
import ths.core.entities.vo.rpt.RptCTKMTCK739VO;
import ths.core.entities.vo.rpt.RptCycleCountDiffVO;
import ths.core.entities.vo.rpt.RptDMVSVO;
import ths.core.entities.vo.rpt.RptExSaleOrder2VO;
import ths.core.entities.vo.rpt.RptF1DataVO;
import ths.core.entities.vo.rpt.RptF1Lv01VO;
import ths.core.entities.vo.rpt.RptImExStDataVO;
import ths.core.entities.vo.rpt.RptPDTHVO;
import ths.core.entities.vo.rpt.RptPoCfrmFromDvkhVO;
import ths.core.entities.vo.rpt.RptTotalAmountParentVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.business.StockReportMgr;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.ShopDAOImpl;
import ths.dms.core.dao.repo.IRepository;
import ths.dms.core.report.ShopReportMgr;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class ShopReportTest {

	
	@Autowired
	private ShopReportMgr shopReportMgr;
	
	@Autowired
	private StockReportMgr stockReportMgr;
	
	@Autowired
	private ShopDAO shopDAO;
	
	@Autowired
	private IRepository repo;
	
	@Test
	public void getListTestShop() throws DataAccessException {
		BasicFilter<ShopVO> filter = new BasicFilter<ShopVO>();
		filter.setUserId(1l);
		filter.setId(1l);
		filter.setRoleId(5l);
		List<ShopVO> lst = shopDAO.getListChildShopVOByFilter(filter);
		System.out.println("fuck");
		System.out.println(lst.size());
	}
	
	@Test
	public void testF1() throws BusinessException, DataAccessException {//ham dung de test báo cáo F1
		Calendar calF = Calendar.getInstance();
		calF.setTimeInMillis(1395680400000l);
		
		Calendar calT = Calendar.getInstance();
		calT.setTimeInMillis(1395680400000l);
		
		Date fDate = calF.getTime();
		Date tDate = calT.getTime();
		
		
		
		List<RptF1DataVO> lstF1Data = new ArrayList<RptF1DataVO>();
		List<RptF1Lv01VO> lstF1 = shopReportMgr.getDataForF1Report(6L, fDate, tDate, 26, 20);
		if(lstF1!= null && lstF1.size() > 0){					
			Long openStockTotalSum = Long.valueOf(0);
			Long importQtySum = Long.valueOf(0);
			Long exportQtySum = Long.valueOf(0);
			Long closeStockTotalSum = Long.valueOf(0);
			Long monthCumulateSum = Long.valueOf(0);
			Long monthPlanSum = Long.valueOf(0);
			Long dayPlanSum = Long.valueOf(0);					
			Long requimentStockSum = Long.valueOf(0);
			Long poCfQtySum = Long.valueOf(0);
			Long poCsQtySum = Long.valueOf(0);					
			Long boxQuantitySum = Long.valueOf(0);
			BigDecimal amountSum =BigDecimal.ZERO;
			for(int i=0;i<lstF1.size();i++){
				if(lstF1.get(i)!= null && lstF1.get(0).getListData()!= null && lstF1.get(0).getListData().size()>0){
					Long openStockTotal = Long.valueOf(0);
					Long importQty = Long.valueOf(0);
					Long exportQty = Long.valueOf(0);
					Long closeStockTotal = Long.valueOf(0);
					Long monthCumulate = Long.valueOf(0);
					Long monthPlan = Long.valueOf(0);
					Long dayPlan = Long.valueOf(0);							
					Long requimentStock = Long.valueOf(0);
					Long poCfQty = Long.valueOf(0);
					Long poCsQty = Long.valueOf(0);							
					Long boxQuantity = Long.valueOf(0);
					BigDecimal amount =BigDecimal.ZERO;
					for(int j=0;j<lstF1.get(i).getListData().size();j++){
						RptF1DataVO data = lstF1.get(i).getListData().get(j);
						data.setType(0);
						lstF1Data.add(data);								
						openStockTotal += data.getOpenStockTotal();								
						importQty += data.getImportQty();
						exportQty +=data.getExportQty();
						closeStockTotal += data.getCloseStockTotal();
						monthCumulate += data.getMonthCumulate();
						monthPlan += data.getMonthPlan();
						dayPlan += data.getDayPlan();								
						requimentStock += data.getRequimentStock();
						poCsQty += data.getPoCsQty();
						poCfQty += data.getPoCfQty();
						boxQuantity += data.getBoxQuantity();
						amount = amount.add(data.getAmount());								
					}
					RptF1DataVO sumVO = new RptF1DataVO();
					//sumVO.setProductName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.stock.f1.sum.cat", lstF1.get(i).getProductInfoName()).toUpperCase());														
					sumVO.setOpenStockTotal(BigDecimal.valueOf(openStockTotal));
					sumVO.setImportQty(BigDecimal.valueOf(importQty));
					sumVO.setExportQty(BigDecimal.valueOf(exportQty));
					sumVO.setCloseStockTotal(BigDecimal.valueOf(closeStockTotal));
					sumVO.setMonthCumulate(BigDecimal.valueOf(monthCumulate));
					sumVO.setMonthPlan(BigDecimal.valueOf(monthPlan));
					sumVO.setDayPlan(BigDecimal.valueOf(dayPlan));							
					sumVO.setRequimentStock(BigDecimal.valueOf(requimentStock));
					sumVO.setPoCsQty(BigDecimal.valueOf(poCsQty));
					sumVO.setPoCfQty(BigDecimal.valueOf(poCfQty));
					sumVO.setBoxQuantity(BigDecimal.valueOf(boxQuantity));
					sumVO.setAmount(amount);							
					//sumVO.setType(TOTAL_CAT);
					sumVO.setWarning("");
					lstF1Data.add(sumVO);
					openStockTotalSum += openStockTotal;								
					importQtySum += importQty;
					exportQtySum += exportQty;
					closeStockTotalSum += closeStockTotal;
					monthCumulateSum += monthCumulate;
					monthPlanSum += monthPlan;
					dayPlanSum += dayPlan;							
					requimentStockSum += requimentStock;
					poCsQtySum += poCsQty;
					poCfQtySum += poCfQty;
					boxQuantitySum += boxQuantity;
					amountSum = amountSum.add(amount);							
				}
			}
			RptF1DataVO sumVOTotal = new RptF1DataVO();
			//sumVOTotal.setProductName(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "report.stock.f1.total").toUpperCase());					
			sumVOTotal.setOpenStockTotal(BigDecimal.valueOf(openStockTotalSum));
			sumVOTotal.setImportQty(BigDecimal.valueOf(importQtySum));
			sumVOTotal.setExportQty(BigDecimal.valueOf(exportQtySum));
			sumVOTotal.setCloseStockTotal(BigDecimal.valueOf(closeStockTotalSum));
			sumVOTotal.setMonthCumulate(BigDecimal.valueOf(monthCumulateSum));
			sumVOTotal.setMonthPlan(BigDecimal.valueOf(monthPlanSum));
			sumVOTotal.setDayPlan(BigDecimal.valueOf(dayPlanSum));					
			sumVOTotal.setRequimentStock(BigDecimal.valueOf(requimentStockSum));
			sumVOTotal.setPoCsQty(BigDecimal.valueOf(poCsQtySum));
			sumVOTotal.setPoCfQty(BigDecimal.valueOf(poCfQtySum));
			sumVOTotal.setBoxQuantity(BigDecimal.valueOf(boxQuantitySum));
			sumVOTotal.setAmount(amountSum);					
			//sumVOTotal.setType(TOTAL_STOCK);
			sumVOTotal.setWarning("");
			lstF1Data.add(sumVOTotal);
		}
		
		for(RptF1DataVO vo : lstF1Data) {
			if(vo.getProductCode().equals("02BA22")) {
				System.out.println(vo.getProductCode() + "        " + vo.getDayReserveReal());
			}
		}
			
	}
	
	@Test
	public void getXNTCTReport() throws BusinessException, DataAccessException {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DATE, 10);
		Date f = c.getTime();
//		c.set(Calendar.DATE, 16);
		c = Calendar.getInstance();
		Date t = c.getTime();
		List<RptImExStDataVO> res = shopReportMgr.getXNTCTReport(15L, c.getTime(), c.getTime());
		if(res != null && res.size() > 0){
			for (RptImExStDataVO child : res) {
				System.out.print(child.toString());
			}
		}
		System.out.println("con ga");
	}
	
	@Test
	public void getPTHReport() throws BusinessException, DataAccessException {
//		Calendar f = Calendar.getInstance();
//		f.set(2013, 11, 1);
//		
//		Calendar t = Calendar.getInstance();
//		t.set(2014, 3, 8);
//		List<RptCTKMTCK739VO> res = shopReportMgr.rpt7_2_7_CTKMTCK(6l, f.getTime(), t.getTime());
//		System.out.println("con ga");
	}
	
	@Test
	public void getListSaleOrderBySaleStaffGroupByProductCode() throws BusinessException, DataAccessException {
		Calendar f = Calendar.getInstance();
		f.set(2014, 8, 1);
		
		Calendar t = Calendar.getInstance();
		t.set(2014, 9, 30);
		long current = System.currentTimeMillis();
		System.out.println(current);
		List<RptExSaleOrder2VO> res = shopReportMgr.getListSaleOrderBySaleStaffGroupByProductCode(6l, null, null,
				null, f.getTime(), t.getTime(), 645l, 0);
		
		System.out.println("TIME RUN" + String.valueOf((System.currentTimeMillis() - current)));
		System.out.println("conga");
	}
	
	@Test
	public void getDataForImExStReport() throws BusinessException, DataAccessException {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DATE, 10);
		Date f = c.getTime();
//		c.set(Calendar.DATE, 16);
		c = Calendar.getInstance();
		Date t = c.getTime();
		List<RptImExStDataVO> res = stockReportMgr.getImExStReportReport(15L, f, t);
		if(res != null && res.size() > 0){
			for (RptImExStDataVO child : res) {
				System.out.print(child.toString());
			}
		}
		System.out.println("con ga");
	}
	
	@Test
	public void getCLTKTTReport() {
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(2012, 8, 1);
			Date fromDate = cal.getTime();
			cal.set(2012, 21, 1);
			Date toDate = cal.getTime();
			Long shopId = 1L;
			RptCycleCountDiffVO tmp = shopReportMgr.getCLTKTTReport(shopId, fromDate, toDate, null);
			System.out.print(tmp.getLstRptCycleCountDiff1VO().size());
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void getPOConfirmDVKHReport() {
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(2013, 2, 12);
			Date fromDate = cal.getTime();
			cal.set(2013, 2, 12);
			Date toDate = cal.getTime();
			Long shopId = 15L;
			RptPoCfrmFromDvkhVO tmp = shopReportMgr.getPOConfirmDVKHReport(shopId, fromDate, toDate);
			System.out.print(tmp.getLstRptPoCfrmFromDvkh1VO().size());
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void getListRptBookProduct() {
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(2012, 8, 1);
			Date fromDate = cal.getTime();
			cal.set(2012, 21, 1);
			Date toDate = cal.getTime();
			Long shopId = 15L;
			List<RptBookProductVinamilkVO> tmp = shopReportMgr.getListProductBookFromVNM(shopId, fromDate, toDate);
			System.out.print(tmp.size());
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void getPDTHReport() {
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(2013, 1, 20);
			Date fromDate = cal.getTime();
			cal.set(2013, 1, 26);
			//Date toDate = cal.getTime();
			Long shopId = 15L;
			List<RptPDTHVO> tmp = shopReportMgr.getPDTHReport(shopId, fromDate, null, null, null);
			System.out.print(tmp);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
//	@Test
//	public void getBKCTHDGTGTReport() {
//		try {
//			Calendar cal = Calendar.getInstance();
//			cal.set(2013, 2, 8);
//			Date fromDate = cal.getTime();
//			cal.set(2013, 2, 8);
//			Date toDate = cal.getTime();
//			Long shopId = 6L;
//			RptInvDetailStatVO tmp = shopReportMgr.getBKCTHDGTGTReport(shopId, null, null, null, fromDate, toDate, null);
//			System.out.print(tmp);
//		} catch (Exception e) {
//			e.printStackTrace();
//		} 
//	}
	
	@Test
	public void getSummaryStaffTurnoverReport() {
		try {
			Calendar cal = Calendar.getInstance();
			cal.set(2012, 8, 1);
			Date fromDate = cal.getTime();
			cal.set(2012, 21, 1);
			Date toDate = cal.getTime();
			Long shopId = 15L;
			List<RptTotalAmountParentVO> tmp = shopReportMgr.getSummaryStaffTurnoverReport(shopId, fromDate, toDate, null);
			System.out.print(tmp.size());
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	/*
	@Test
	public void getDSKHReport() {
		try {
			List<RptCustomerVO> res = shopReportMgr.getDSKHReport("BC", null, null, null, false);
			System.out.print(res.size());
			res = shopReportMgr.getDSKHReport("BC", null, null, null, true);
			System.out.print(res.size());
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}*/
	@Test
	public void getAbsentReport() {
		try {
			Calendar calFrom = Calendar.getInstance();
			calFrom.set(2013, 0, 1);
			Calendar calTo = Calendar.getInstance();
			calTo.set(2013, 1, 1);
			List<RptAbsentVO> res = shopReportMgr.getAbsentReport2(15L, null, calFrom.getTime(), calTo.getTime());
			System.out.print(res);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void testPro() {
		try {
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void getDMVSReport() throws BusinessException, DataAccessException {
//		Calendar f = Calendar.getInstance();
//		f.set(2013,1,6);
//		
//		
//		Calendar t = Calendar.getInstance();
//		t.set(2013,1,7);
//		List<RptDMVSVO> res = shopReportMgr.getDMVSReport("100l", null, null, f.getTime(), t.getTime(), 9, 30, 16, 0);
//		
//		System.out.println("con ga");
	}
	
	@Test
	public void getDataToListPaginated() throws BusinessException, DataAccessException {
		String sql = "select staff_id, staff_code, shop_id, staff_name from staff where rownum < 3";
		List<Object> t = repo.getDataToListPaginated(sql, null, 100, 0);
		for (Object object : t) {
			HashMap<String, Object> tmp =(HashMap<String, Object>)(object); 
			for (String child : tmp.keySet()) {
				System.out.println(child + " " + tmp.get(child));
			}
		}
		System.out.println("con ga");
	}
	
	public static void main(String[] args) {
		String tmp = "abc > ";
		Long t = 1000000000000000000l;
		tmp += t.longValue();
		System.out.println(tmp);
	}
}

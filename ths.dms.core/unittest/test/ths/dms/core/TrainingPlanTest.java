package test.ths.dms.core;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.httpclient.util.DateParseException;
import org.apache.commons.httpclient.util.DateUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.TrainingPlanDetail;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.StaffTrainingPlanVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.business.ShopLockMgr;
import ths.dms.core.business.StaffMgr;
import ths.dms.core.business.SuperviserMgr;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class TrainingPlanTest {

	@Autowired
	SuperviserMgr superviserMgr;
	
	@Autowired
	StaffMgr staffMgr;
	
	@Autowired
	ShopLockMgr shopLockMgr;

	
	@Test
	public void getListTrainingForAreaManager() throws BusinessException, DataAccessException, ParseException {
		Long tbhvId = 1540000l;
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

		try {
			Date trainingDate = format.parse("2013/06/26");
			
			List<StaffTrainingPlanVO> result = this.superviserMgr
					.getListTrainingForAreaManager(tbhvId, trainingDate);
			if (null != result && result.size() > 0) {
				System.out.println("size " + result.size());
			} else {
				System.out.println("empty ...");
			}
		} catch (BusinessException e) {
			e.printStackTrace();
		}
	}
	@Test 
	public void getLockDate() throws BusinessException, DateParseException{
		Date lockDate = shopLockMgr.getApplicationDate(new Long(12));
		System.out.println("ngay: ");
		System.out.println(DateUtil.formatDate(lockDate,"dd/MM/yyyy"));
		System.out.println("END ngay: ");
		
	}
	/*@Test
	public void getListTrainingPlanDetailBySuperId() throws BusinessException,
			DataAccessException, ParseException {
		Long tbhvId = 1540000l;
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

		try {
			Date trainingDate = format.parse("2013/06/26");

			List<TrainingPlanDetail> result = this.superviserMgr
					.getListTrainingPlanDetailBySuperId(tbhvId, trainingDate);

			if (result != null && result.size() > 0) {
				for (TrainingPlanDetail item : result) {
					if (item.getManager() != null
							&& item.getManager().getId().compareTo(tbhvId) == 0
							&& item.getStatusManager() != null
							&& item.getStatusManager().getValue() == 1) {
						System.out.println("Edit: " + item.getId());
					} else {
						System.out.println("Add new: " + item.getId());
					}
				}
			}
		} catch (BusinessException e) {
			e.printStackTrace();
		}
	}*/
	
	/*@Test
	public void replaceTrainingPlanDetail() throws BusinessException,
			DataAccessException, ParseException {
		Long oldId = 10285l;
		TrainingPlanDetail trainingPlanDetail = this.superviserMgr.getTrainingPlanDetailById(10077l);
		Staff staff = this.staffMgr.getStaffById(43l);
		LogInfoVO logVO = new LogInfoVO();
		
		logVO.setIp("127.0.0.1");
		logVO.setStaffCode("thuattq");

		try {
			trainingPlanDetail.setManager(staff);
			this.superviserMgr.replaceTrainingPlanDetail(oldId,
					trainingPlanDetail, logVO);

			System.out.println("empty ...");
		} catch (BusinessException e) {
			e.printStackTrace();
		}
	}
	*/
	
	/*@Test
	public void getTrainingPlanDetailForAreaManager() throws BusinessException,
			DataAccessException, ParseException {
		Long tbhvId = 1540000l;
		Long gsnppId = 173l;
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		Date trainingDate = null;

		try {
			trainingDate = format.parse("2013/06/26");
			
			TrainingPlanDetail result = this.superviserMgr
					.getTrainingPlanDetailForAreaManager(tbhvId, gsnppId,
							trainingDate);

			System.out.println("empty ...");
		} catch (BusinessException e) {
			e.printStackTrace();
		}
	}*/
}

package test.ths.dms.core;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.vo.CommercialSupportVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleProductVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.business.CarMgr;
import ths.dms.core.business.CommonMgr;
import ths.dms.core.business.CustomerMgr;
import ths.dms.core.business.ProductMgr;
import ths.dms.core.business.PromotionProgramMgr;
import ths.dms.core.business.SaleOrderMgr;
import ths.dms.core.business.ShopMgr;
import ths.dms.core.business.StaffMgr;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class SalesTest  extends AbstractUnitTest {
	
	@Autowired
	SaleOrderMgr saleOrderMgr;
	
	@Autowired
	ProductMgr productMgr;
	
	@Autowired
	PromotionProgramMgr promotionProgramMgr;
	
	@Autowired
	CustomerMgr customerMgr;
	
	@Autowired
	ShopMgr shopMgr;
	
	@Autowired
	StaffMgr staffMgr;
	
	@Autowired
	CommonMgr commonMgr;
	
	@Autowired
	CarMgr carMgr;
	
	
	@Test
	public void getListCS() throws BusinessException {
		ObjectVO<CommercialSupportVO> commercials = saleOrderMgr.getListCommercialSupport(null, 16L, 5L, commonMgr.getSysDate(), 1L);
		commercials = saleOrderMgr.getListCommercialSupport(null, 16L, 5L, commonMgr.getSysDate(), null);
		System.out.println("Hellofdsafdsfdsfas");
	}
	
	
	//ban hang, huy hang, khuyen mai
	//duyet don hang
	//xoa don hang - xoa don hang tu tablet
	
	@Test
	public void createSaleOrder() throws BusinessException {
		//16:
		//02AA10: 862 - 807
		//02AA12: 108 - 99 --> Lot: 1-1 & 62-62
		//04FA21: 954-456 --> Lot: 754-654 
		
		//5:
		//02AA10: 55 - 55
		//02AA12: 32 - 32 --> Lot: 20-20 & 90-90
		//04Px07: 60-60
		
		//ban hang: 02AA10
		SaleOrder so = new SaleOrder();
		so.setShop(shopMgr.getShopById(16L));
		so.setStaff(staffMgr.getStaffById(10L));
		so.setCustomer(customerMgr.getCustomerById(5L));
		so.setOrderDate(new Date());
		so.setCreateUser("NMHUNG");
		so.setIsVisitPlan(1);
		
		List<SaleProductVO> lstSaleProductVO = new ArrayList<SaleProductVO>();
		List<SaleOrderDetail> lstPromoProduct = new ArrayList<SaleOrderDetail>();
		
		SaleProductVO soVO = new SaleProductVO();
		
		SaleOrderDetail sod = new SaleOrderDetail();
		sod.setAmount(new BigDecimal(100D));
		sod.setCreateUser("NMHUNG");
		sod.setDiscountAmount(new BigDecimal(0));
		sod.setIsFreeItem(0);
		sod.setOrderDate(new Date());
		sod.setPrice(productMgr.getPriceById(22L));
		sod.setProduct(productMgr.getProductByCode("02AA10"));
		sod.setQuantity(7);
		sod.setShop(shopMgr.getShopById(16L));
		sod.setStaff(staffMgr.getStaffById(10L));
		sod.setVat(10F);
		soVO.setSaleOrderDetail(sod);
		
		lstSaleProductVO.add(soVO);
		
		//huy hang: 02AA12
		soVO = new SaleProductVO();
		sod = new SaleOrderDetail();
		sod.setAmount(new BigDecimal(100D));
		sod.setCreateUser("NMHUNG");
		sod.setDiscountAmount(new BigDecimal(0));
		sod.setIsFreeItem(0);
		sod.setOrderDate(new Date());
		sod.setPrice(productMgr.getPriceById(22L));
		sod.setProduct(productMgr.getProductByCode("02AA12"));
		sod.setProgramCode("LAM01");
		sod.setProgramType(ProgramType.DESTROY);
		sod.setQuantity(1);
		sod.setShop(shopMgr.getShopById(16L));
		sod.setStaff(staffMgr.getStaffById(10L));
		sod.setVat(10F);
		
		soVO.setSaleOrderDetail(sod);
		
		SaleOrderLot lot = new SaleOrderLot();
		lot.setCreateUser("nmhung");
		lot.setLot("LOT1");
		lot.setShop(shopMgr.getShopById(16L));
		lot.setQuantity(1);
		lot.setProduct(productMgr.getProductByCode("02AA12"));
		soVO.setLstSaleOrderLot(Arrays.asList(lot));
		
		lstSaleProductVO.add(soVO);
		
		//khuyen mai: 04FA21
		sod = new SaleOrderDetail();
		sod.setAmount(new BigDecimal(100D));
		sod.setCreateUser("NMHUNG");
		sod.setDiscountAmount(new BigDecimal(0));
		sod.setIsFreeItem(0);
		sod.setOrderDate(new Date());
		sod.setPrice(productMgr.getPriceById(22L));
		sod.setProduct(productMgr.getProductByCode("04FA21"));
		sod.setProgramCode("NGANLTT09");
		sod.setProgramType(ProgramType.AUTO_PROM);
		sod.setQuantity(1);
		sod.setShop(shopMgr.getShopById(16L));
		sod.setStaff(staffMgr.getStaffById(10L));
		sod.setVat(10F);
		lstPromoProduct.add(sod);
		
//		saleOrderMgr.createSaleOrder(so, lstSaleProductVO, lstPromoProduct);
	}
	
	
	@Test
	public void modifySaleOrder() throws BusinessException {
		SaleOrder so = saleOrderMgr.getSaleOrderById(199L);
		
		List<SaleProductVO> lstSaleProductVO = new ArrayList<SaleProductVO>();
		List<SaleOrderDetail> lstPromoProduct = new ArrayList<SaleOrderDetail>();
		
		SaleProductVO soVO = new SaleProductVO();
		
		SaleOrderDetail sod = new SaleOrderDetail();
		sod.setAmount(new BigDecimal(100D));
		sod.setCreateUser("NMHUNG");
		sod.setDiscountAmount(new BigDecimal(0));
		sod.setIsFreeItem(0);
		sod.setOrderDate(new Date());
		sod.setPrice(productMgr.getPriceById(22L));
		sod.setProduct(productMgr.getProductByCode("02AA10"));
		sod.setQuantity(6);
		sod.setShop(shopMgr.getShopById(16L));
		sod.setStaff(staffMgr.getStaffById(10L));
		sod.setVat(10F);
		soVO.setSaleOrderDetail(sod);
		
		lstSaleProductVO.add(soVO);
		
		//huy hang: 02AA12
		soVO = new SaleProductVO();
		sod = new SaleOrderDetail();
		sod.setAmount(new BigDecimal(100D));
		sod.setCreateUser("NMHUNG");
		sod.setDiscountAmount(new BigDecimal(0));
		sod.setIsFreeItem(0);
		sod.setOrderDate(new Date());
		sod.setPrice(productMgr.getPriceById(22L));
		sod.setProduct(productMgr.getProductByCode("02AA12"));
		sod.setProgramCode("LAM01");
		sod.setProgramType(ProgramType.DESTROY);
		sod.setQuantity(1);
		sod.setShop(shopMgr.getShopById(16L));
		sod.setStaff(staffMgr.getStaffById(10L));
		sod.setVat(10F);
		
		soVO.setSaleOrderDetail(sod);
		
		SaleOrderLot lot = new SaleOrderLot();
		lot.setCreateUser("nmhung");
		lot.setLot("LOT1");
		lot.setShop(shopMgr.getShopById(16L));
		lot.setQuantity(1);
		lot.setProduct(productMgr.getProductByCode("02AA12"));
		soVO.setLstSaleOrderLot(Arrays.asList(lot));
		
		lstSaleProductVO.add(soVO);
		
		//khuyen mai: 04FA21
		sod = new SaleOrderDetail();
		sod.setAmount(new BigDecimal(100D));
		sod.setCreateUser("NMHUNG");
		sod.setDiscountAmount(new BigDecimal(0));
		sod.setIsFreeItem(0);
		sod.setOrderDate(new Date());
		sod.setPrice(productMgr.getPriceById(22L));
		sod.setProduct(productMgr.getProductByCode("04FA21"));
		sod.setProgramCode("NGANLTT09");
		sod.setProgramType(ProgramType.AUTO_PROM);
		sod.setQuantity(1);
		sod.setShop(shopMgr.getShopById(16L));
		sod.setStaff(staffMgr.getStaffById(10L));
		sod.setVat(10F);
		lstPromoProduct.add(sod);
		
//		saleOrderMgr.modifySaleOrder(so, lstSaleProductVO, lstPromoProduct);
	}
	
	@Test
	public void returnSaleOrder() throws BusinessException {
//		saleOrderMgr.updateReturnSaleOrder(199L, "hungnm");
	}
	

//	@Test
//	public void approveSaleOrder() throws BusinessException {
//		saleOrderMgr.approveSaleOrder(null);
//	}
	
	@Test
	public void deleteSaleOrder() throws BusinessException {
		saleOrderMgr.checkDebitInSaleOrder(5L, 5L, new BigDecimal(100));
	}
	
}
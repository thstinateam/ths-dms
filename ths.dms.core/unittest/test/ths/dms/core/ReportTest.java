package test.ths.dms.core;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.RptTDBHTMHProductAndListInfoVO;
import ths.core.entities.vo.rpt.Rpt10_1_2;
import ths.core.entities.vo.rpt.Rpt7_2_5_PTVO;
import ths.core.entities.vo.rpt.Rpt7_2_5_PT_Detail;
import ths.core.entities.vo.rpt.RptBCKD10_3;
import ths.core.entities.vo.rpt.RptBCKD11;
import ths.core.entities.vo.rpt.RptBCKD19_1;
import ths.core.entities.vo.rpt.RptBCKD2;
import ths.core.entities.vo.rpt.RptBCKD9VO;
import ths.core.entities.vo.rpt.RptBCTCNVO;
import ths.core.entities.vo.rpt.RptExSaleOrder2VO;
import ths.core.entities.vo.rpt.RptPGNVTTGVO;
import ths.core.entities.vo.rpt.RptPoStatusTracking2VO;
import ths.core.entities.vo.rpt.Rpt_HO_KHO_1_2;
import ths.dms.core.entities.vo.rpt.ho.RptCTKMVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.business.PoReportMgr;
import ths.dms.core.business.RptPayDebitMgr;
import ths.dms.core.business.SaleOrderReportMgr;
import ths.dms.core.report.CrmReportMgr;
import ths.dms.core.report.HoReportMgr;
import ths.dms.core.report.ShopReportMgr;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class ReportTest extends AbstractUnitTest {

	@Autowired
	SaleOrderReportMgr saleOrderReportMgr;
	
	@Autowired
	RptPayDebitMgr rptPayDebitMgr;
	
	@Autowired
	HoReportMgr hoReportMgr;
	
	@Test
	public void getListSaleOrderLotVO() throws DataAccessException, BusinessException {
		
//		List<RptSaleOrderLot1VO> lst = saleOrderReportMgr.getListSaleOrderByDeliveryStaffGroupByProductCode(null, null, null, null);
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, 2012);
		c.set(Calendar.MONTH, 11);
		c.set(Calendar.DATE, 1);
		
//		List<RptTDBHTMHProductAndListInfoVO> lst1 = shopReportMgr.getTDBHTMHReport(13l, null, c.getTime(), new Date());
//		rptPayDebitMgr.getListRptCustomerPayReceived1VO(null, null, null, null);
//		saleOrderReportMgr.getListRptProductExchangeVO(new Date(), new Date(), 1L, "123", 1L);
//		saleOrderReportMgr.getListRptProductExchangeVO(null, null, null, null, null);
//		System.out.println(lst.size());
//		System.out.println(lst1.size());
		
	}
	
	@Autowired
	PoReportMgr poReportMgr;
	
	@Test
	public void getRptPoStatusTrackingVO() throws BusinessException {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, 2012);
		c.set(Calendar.MONTH, 0);
		c.set(Calendar.DATE, 1);
		
//		List<RptPoStatusTracking2VO> lst = shopReportMgr.getRptPoStatusTrackingVO(c.getTime(), new Date(), 13L);
//		lst = shopReportMgr.getRptPoStatusTrackingVO(c.getTime(), new Date(), 13L);
//		System.out.print(lst);
		List<RptPoStatusTracking2VO> lst = shopReportMgr.getRptPoStatusTrackingVO(null, null, null);
		System.out.print(lst);
		System.out.print("123");
	}
	
	@Autowired
	ShopReportMgr shopReportMgr;
	
	@Test
	public void shopReport() throws BusinessException {
		List<Rpt10_1_2> lst ;//= shopReportMgr.getListSaleOrderByDeliveryStaffGroupByProductCode(15L, null, null, null);
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, 2013);
		c.set(Calendar.MONTH, 2);
		c.set(Calendar.DATE, 25);
		
		Calendar ct = Calendar.getInstance();
		ct.set(Calendar.YEAR, 2013);
		ct.set(Calendar.MONTH, 3);
		ct.set(Calendar.DATE, 1);
//		lst = shopReportMgr.getListRpt10_1_2(6L,null,new Date(),new Date());
//		System.out.print("adsasdsad");
//		lst = shopReportMgr.getListSaleOrderByDeliveryStaffGroupByProductCode(15L, null, null, c.getTime(), ct.getTime());
//		shopReportMgr.getListSaleOrderByDeliveryStaff(1L, 1L, new Date(), new Date(), 1, true, 1L);
//		shopReportMgr.getListSaleOrderByDeliveryStaffGroupByProductCode(1L, 1L, new Date(), new Date());
	}
	
	@Test
	public void pgnvttg() throws BusinessException {
//		shopReportMgr.getListRptPGNVTTGVOs(15L, 12L, new Date(), new Date());
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, 2012);
		c.set(Calendar.MONTH, 0);
		c.set(Calendar.DATE, 30);
		
		Calendar ct = Calendar.getInstance();
		ct.set(Calendar.YEAR, 2013);
		ct.set(Calendar.MONTH, 3);
		ct.set(Calendar.DATE, 30);
		
		List<RptPGNVTTGVO> cbc = shopReportMgr.getListRptPGNVTTGVOs(6L, null, c.getTime(), ct.getTime());
		System.out.println(cbc);
	}
	
	@Test
	public void bcxnknvbh() throws BusinessException {
		List<Long> lstStaff = new ArrayList<Long>();
		lstStaff.add(57L);
		lstStaff.add(51L);
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, 2012);
		c.set(Calendar.MONTH, 11);
		c.set(Calendar.DATE, 1);
		
		Calendar ct = Calendar.getInstance();
		ct.set(Calendar.YEAR, 2013);
		ct.set(Calendar.MONTH, 0);
		ct.set(Calendar.DATE, 31);
		//shopReportMgr.getListRptBCXNKNVBHVOs(15L, lstStaff, c.getTime(), ct.getTime());
		//shopReportMgr.getListRptBCXNKNVBHVOs(15L, null, new Date(), new Date());
		//shopReportMgr.getListRptBCXNKNVBHVOs(15L, null, null, null);
		//List<RptBCXNKNVBHVO> cbc = shopReportMgr.getListRptBCXNKNVBHVOs(null, null, null, null);
		//System.out.println(cbc);
	}
	
	@Test
	public void getListRptBCTCNVO()throws BusinessException {
		List<RptBCTCNVO> cbc = shopReportMgr.getListRptBCTCNVO(15L, null, StockObjectType.CUSTOMER);
		List<Long> lstId = new ArrayList<Long>();
		lstId.add(123L);
		lstId.add(12L);
		cbc = shopReportMgr.getListRptBCTCNVO(15L, lstId, StockObjectType.STAFF);
		cbc = shopReportMgr.getListRptBCTCNVO(15L, lstId, StockObjectType.CUSTOMER);
		System.out.println(cbc);
	}
	
	@Autowired
	CrmReportMgr crmReportMgr;
	
	@Test
	public void alksjdlaksd() throws BusinessException {
		Calendar d = Calendar.getInstance();
		d.set(Calendar.YEAR, 2012);
		d.set(Calendar.MONTH, 0);
		d.set(Calendar.DATE, 29);
		
		Calendar dp = Calendar.getInstance();
		dp.set(Calendar.YEAR, 2014);
		dp.set(Calendar.MONTH, 0);
		dp.set(Calendar.DATE, 29);
//		RptBCKD1_1 alk = crmReportMgr.getRptBCKD1_1(Arrays.asList(1L), Arrays.asList(1L), new Date());
//		alk = crmReportMgr.getRptBCKD1_1(Arrays.asList(1L), null, new Date());
//		System.out.println(alk.getLstDetail().size());
		RptBCKD11 r = crmReportMgr.getRptBCKD11(224L, d.getTime(), dp.getTime());
		System.out.println(r.getLstDetail().size());
	}
	
	
	@Test
	public void alksjdlkajsd() throws BusinessException {
		Calendar d = Calendar.getInstance();
		d.set(Calendar.YEAR, 2013);
		d.set(Calendar.MONTH, 2);
		d.set(Calendar.DATE, 27);
//		crmReportMgr.getRptBCKD10_3(1L, 1L, Arrays.asList(1L, 2L, 3L), new Date());
		RptBCKD10_3 alk = crmReportMgr.getRptBCKD10_3(349L, 303L, Arrays.asList(1L), d.getTime());
		System.out.println(alk.getLstDetail().size());
	}
	
	@Test
	public void alskjdlaks() throws BusinessException {
		Calendar d = Calendar.getInstance();
		d.set(Calendar.YEAR, 2012);
		d.set(Calendar.MONTH, 11);
		d.set(Calendar.DATE, 29);
		RptBCKD2 alk ;//= crmReportMgr.getRptBCKD2(null, null, null, null, null, new Date());
		alk = crmReportMgr.getRptBCKD2(Arrays.asList(1L), Arrays.asList(1L), Arrays.asList(1L), Arrays.asList(1L), Arrays.asList(1L), new Date()); 
//		alk = crmReportMgr.getRptBCKD2(139L, 21L, 320L, 16L, null, null, null, d.getTime());
		System.out.println(alk.getLstDetail().size());
	}
	
	@Test
	public void asdjlaksdj() throws BusinessException {
		List<Long> listIdNpp = new ArrayList<Long>();
		listIdNpp.add(12L);
//		listIdNpp.add(2L);
		List<Long> listIdCat = new ArrayList<Long>();
//		listIdCat.add(1L);
//		listIdCat.add(2L);
		List<Long> listIdSp = new ArrayList<Long>();
//		listIdSp.add(1L);
//		listIdSp.add(2L);
		
		Calendar d = Calendar.getInstance();
		d.set(Calendar.YEAR, 2013);
		d.set(Calendar.MONTH, 2);
		d.set(Calendar.DATE, 3);
		
		Calendar c = Calendar.getInstance();
		c.set(Calendar.YEAR, 2013);
		c.set(Calendar.MONTH, 2);
		c.set(Calendar.DATE, 18);
		
		List<RptBCKD9VO> lst = crmReportMgr.getRptBCKD9(listIdNpp, listIdCat, listIdSp, d.getTime(), c.getTime());
//		List<RptBCKD9VO> lst = crmReportMgr.getRptBCKD9(null, null, null, new Date(), new Date());
		List<RptBCKD9VO> lst1 = new ArrayList<RptBCKD9VO>();
		System.out.println(lst.size());
		for (RptBCKD9VO bc: lst) {
			if (bc.getItemNo() != null) {
				lst1.add(bc);
			}
		}
		System.out.println(lst1.size());
	}
	
	@Test
	public void crmGiaiPhap() throws BusinessException {
		Calendar d = Calendar.getInstance();
		d.set(Calendar.YEAR, 2013);
		d.set(Calendar.MONTH, 2);
		d.set(Calendar.DATE, 1);
		
		Calendar dd = Calendar.getInstance();
		dd.set(Calendar.YEAR, 2013);
		dd.set(Calendar.MONTH, 2);
		dd.set(Calendar.DATE, 23);
		//SangTN - comment
//		crmReportMgr.getRptBCKD11(1L, new Date(), new Date());
//		crmReportMgr.getRptBCKD11(null, d.getTime(), dd.getTime());
//		crmReportMgr.getRptBCKD16_1(d.getTime(), dd.getTime());
		RptBCKD19_1 v = crmReportMgr.getRptBCKD19_1(1L, 1, d.getTime(), dd.getTime());
		System.out.println(v);
//		crmReportMgr.getRptBCKD13(15L, "123", new Date(), new Date());
	}

	@Test
	public void getListRpt7_2_5_PT() throws BusinessException, Exception{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date fDate = format.parse("2014-04-5");
		Date tDate = format.parse("2014-04-8");
		//List<Rpt7_2_5_PT_Detail> result = shopReportMgr.getRpt7_2_5PT(6l, "", fDate, tDate);
		//System.out.println("Success ..." + (result == null ? 0:result.size()));
	}
	
	@Test
	public void exportBCCTKM() throws BusinessException, Exception{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date fDate = format.parse("2015-10-01");
		Date tDate = format.parse("2015-11-01");
		List<RptCTKMVO> result = hoReportMgr.exportBCCTKM(1l, 5l, 1l, fDate, tDate);
		System.out.println("Success ...: " + (result == null ? 0:result.size()));
	}
}
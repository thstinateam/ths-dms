package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.KS;
import ths.dms.core.entities.KSCusProductReward;
import ths.dms.core.entities.KSCustomer;
import ths.dms.core.entities.KSCustomerLevel;
import ths.dms.core.entities.KSLevel;
import ths.dms.core.entities.KSProduct;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.KeyShopFilter;
import ths.dms.core.entities.vo.AllocateShopVO;
import ths.dms.core.entities.vo.KeyShopCustomerHistoryVO;
import ths.dms.core.entities.vo.KeyShopVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.KeyShopDAO;


public class KeyShopMgrImpl implements KeyShopMgr {
	@Autowired
	KeyShopDAO keyShopDAO;
	
	@Override
	public KS createKS(KS ks, LogInfoVO logInfo) throws BusinessException {
		try {
			return keyShopDAO.createKS(ks, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateKS(KS ks, LogInfoVO logInfo) throws BusinessException {
		try {
			keyShopDAO.updateKS(ks, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteKS(KS ks, LogInfoVO logInfo) throws BusinessException{
		try {
			keyShopDAO.deleteKS(ks, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public KS getKSById(Long id) throws BusinessException{
		try {
			return id==null?null:keyShopDAO.getKSById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public KSLevel getKSLevelById(Long id) throws BusinessException{
		try {
			return id==null?null:keyShopDAO.getKSLevelById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public KSCustomer getKSCustomerById(Long id) throws BusinessException {
		try {
			return id == null ? null : keyShopDAO.getKSCustomerById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public KS getKSByCode(String code) throws BusinessException {
		try {
			return keyShopDAO.getKSByCode(code);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public KSLevel getKSLevelByCode(String levelCode, String ksCode) throws BusinessException {
		try {
			return keyShopDAO.getKSLevelByCode(levelCode, ksCode);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<KeyShopVO> getListKeyShopVOByFilter(KeyShopFilter filter)
			throws BusinessException {
		try {
			List<KeyShopVO> lst = keyShopDAO.getListKeyShopVOByFilter(filter);
			ObjectVO<KeyShopVO> vo = new ObjectVO<KeyShopVO>();
			vo.setkPaging(filter.getkPagingVO());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkExistsCustomerNotInKSCycle(KeyShopFilter filter) throws BusinessException {
		try {
			return keyShopDAO.checkExistsCustomerNotInKSCycle(filter);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkExistsCustomerNotInKSLevel(KeyShopFilter filter) throws BusinessException {
		try {
			return keyShopDAO.checkExistsCustomerNotInKSLevel(filter);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkExistsRewardInKS(KeyShopFilter filter) throws BusinessException {
		try {
			return keyShopDAO.checkExistsRewardInKS(filter);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<KSProduct> getListKSProductByFilter(KeyShopFilter filter) throws BusinessException {
		try {
			List<KSProduct> lst = keyShopDAO.getListKSProductByFilter(filter);
			ObjectVO<KSProduct> vo = new ObjectVO<KSProduct>();
			vo.setkPaging(filter.getkPagingP());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<KSCustomer> getListKSCustomerByFilter(KeyShopFilter filter) throws BusinessException {
		try {
			List<KSCustomer> lst = keyShopDAO.getListKSCustomerByFilter(filter);
			ObjectVO<KSCustomer> vo = new ObjectVO<KSCustomer>();
			vo.setkPaging(filter.getkPagingC());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<KSCustomerLevel> getListKSCustomerLevelByFilter(KeyShopFilter filter) throws BusinessException {
		try {
			List<KSCustomerLevel> lst = keyShopDAO.getListKSCustomerLevelByFilter(filter);
			ObjectVO<KSCustomerLevel> vo = new ObjectVO<KSCustomerLevel>();
			vo.setkPaging(filter.getkPagingCL());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<KSCusProductReward> getListKSCusProductRewardByFilter(KeyShopFilter filter) throws BusinessException {
		try {
			List<KSCusProductReward> lst = keyShopDAO.getListKSCusProductRewardByFilter(filter);
			ObjectVO<KSCusProductReward> vo = new ObjectVO<KSCusProductReward>();
			vo.setkPaging(filter.getkPagingCPR());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Shop> getListShopForKSByFilter(KeyShopFilter filter) throws BusinessException {
		try {
			return keyShopDAO.getListShopForKSByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void removeShopMapByShop(KeyShopFilter filter) throws BusinessException{
		try {
			keyShopDAO.removeShopMapByShop(filter);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkKSCustomerExistsShop(KeyShopFilter filter) throws BusinessException {
		try {
			return keyShopDAO.checkKSCustomerExistsShop(filter);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkKSCustomerExistsShopDel(KeyShopFilter filter) throws BusinessException {
		try {
			return keyShopDAO.checkKSCustomerExistsShopDel(filter);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<KeyShopVO> getListKSProductVOByFilter(KeyShopFilter filter) throws BusinessException {
		try {
			List<KeyShopVO> lst = keyShopDAO.getListKSProductVOByFilter(filter);
			ObjectVO<KeyShopVO> vo = new ObjectVO<KeyShopVO>();
			vo.setkPaging(filter.getkPagingVO());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<KeyShopVO> getListKSLevelVOByFilter(KeyShopFilter filter) throws BusinessException {
		try {
			List<KeyShopVO> lst = keyShopDAO.getListKSLevelVOByFilter(filter);
			ObjectVO<KeyShopVO> vo = new ObjectVO<KeyShopVO>();
			vo.setkPaging(filter.getkPagingVO());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<KeyShopVO> getListKSCustomerLevelVOByFilter(KeyShopFilter filter) throws BusinessException {
		try {
			List<KeyShopVO> lst = keyShopDAO.getListKSCustomerLevelVOByFilter(filter);
			ObjectVO<KeyShopVO> vo = new ObjectVO<KeyShopVO>();
			vo.setkPaging(filter.getkPagingVO());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<KeyShopVO> getListKSCustomerVOByFilter(KeyShopFilter filter) throws BusinessException {
		try {
			List<KeyShopVO> lst = keyShopDAO.getListKSCustomerVOByFilter(filter);
			ObjectVO<KeyShopVO> vo = new ObjectVO<KeyShopVO>();
			vo.setkPaging(filter.getkPagingVO());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<KeyShopVO> getListKSRewardVOByFilter(KeyShopFilter filter) throws BusinessException {
		try {
			List<KeyShopVO> lst = keyShopDAO.getListKSRewardVOByFilter(filter);
			ObjectVO<KeyShopVO> vo = new ObjectVO<KeyShopVO>();
			vo.setkPaging(filter.getkPagingVO());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<KeyShopVO> getListKSVOForRewardOrderByFilter(KeyShopFilter filter) throws BusinessException {
		try {
			return keyShopDAO.getListKSVOForRewardOrderByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<KeyShopVO> getListKeyShopVOForViewOrderByFilter(KeyShopFilter filter) throws BusinessException {
		try {
			return keyShopDAO.getListKeyShopVOForViewOrderByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<AllocateShopVO> getListAllocateShop(KeyShopFilter filter) throws BusinessException {
		try {
			return keyShopDAO.getListAllocateShop(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<KeyShopVO> getInfoKSForOrder(KeyShopFilter filter)
			throws BusinessException {
		try {
			return keyShopDAO.getInfoKSForOrder(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<KeyShopCustomerHistoryVO> getKSRegistedHistory(KPaging<KeyShopCustomerHistoryVO> kPaging, Long ksId, Long cycleId, Long ksLevelId, Long customerId, Date updateDate) throws BusinessException {
		try {
			List<KeyShopCustomerHistoryVO> rows = keyShopDAO.getKSRegistedHistory(kPaging, ksId, cycleId, ksLevelId, customerId, updateDate);
			ObjectVO<KeyShopCustomerHistoryVO> result = new ObjectVO<KeyShopCustomerHistoryVO>();
			result.setkPaging(kPaging);
			result.setLstObject(rows);
			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

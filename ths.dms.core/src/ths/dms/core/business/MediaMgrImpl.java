package ths.dms.core.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Media;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.MediaMap;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.filter.MediaFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.MediaDAO;
import ths.dms.core.dao.MediaItemDAO;
import ths.dms.core.dao.MediaMapDAO;

public class MediaMgrImpl implements MediaMgr {

	@Autowired
	MediaDAO mediaDAO;
	
	@Autowired
	MediaItemDAO mediaItemDAO;
	
	@Autowired
	MediaMapDAO mediaMapDAO;
	

	@Override
	public Media createMedia(Media media) throws BusinessException {
		try {
			return mediaDAO.createMedia(media);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}		
	}

	@Override
	public void deleteMedia(Media media) throws BusinessException {
		try {
			mediaDAO.deleteMedia(media);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
	}

	@Override
	public Media getMediaById(long id) throws BusinessException {
		try {
			return mediaDAO.getMediaById(id);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
	}

	@Override
	public Media updateMedia(Media media) throws BusinessException {
		try {
			return mediaDAO.updateMedia(media);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
	}	
	
	@Override
	public Media getMediaByCode(String mediaCode) throws BusinessException{
		try{
			return mediaDAO.getMediaByCode(mediaCode);
		}catch(DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
	@Override
	public MediaItem createMediaItem(MediaItem mediaItem)throws BusinessException {		
		try {
			return mediaItemDAO.createMediaItem(mediaItem);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public MediaItem updateMediaItem(MediaItem mediaItem)throws BusinessException {
		
		try {
			return mediaItemDAO.updateMediaItem(mediaItem);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}	
	}
	
	@Override
	public MediaItem getMediaItemById(Long mediaItemId)
			throws BusinessException {
		try {
			return mediaItemDAO.getMediaItemById(mediaItemId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<MediaItem> getListMediaItemByMedia(KPaging<MediaItem> kPaging,
			Long mediaId) throws BusinessException {
		try {
			return mediaItemDAO.getListMediaItemByObjectIdAndObjectType(kPaging, mediaId, MediaObjectType.VIDEO_UPLOAD);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Media> getListMedia(MediaFilter filter) throws BusinessException{
		try {
			ObjectVO<Media> vo = new ObjectVO<Media>();  
			List<Media> lstObject = mediaDAO.getListMedia(filter);
			if(null != filter && null != filter.getkPaging()){
				vo.setkPaging(filter.getkPaging());				
			}else{
				vo.setkPaging(null);				
			}
			vo.setLstObject(lstObject);
			return vo;
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Product> getListProductByMedia(KPaging<Product> kPaging, Long mediaId, Integer status) throws BusinessException{
		try{
			ObjectVO<Product> vo = new ObjectVO<Product>();
			List<Product> lstObject = mediaDAO.getListProductByMedia(kPaging, mediaId, status);			
			vo.setkPaging(kPaging);				
			vo.setLstObject(lstObject);
			return vo;
		}catch(DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public MediaMap createMediaMap(MediaMap mediaMap) throws BusinessException {
		try {
			return mediaMapDAO.createMediaMap(mediaMap);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}		
	}

	@Override
	public void deleteMediaMap(MediaMap mediaMap) throws BusinessException {
		try {
			mediaMapDAO.deleteMediaMap(mediaMap);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
	}

	@Override
	public MediaMap getMediaMapById(long id) throws BusinessException {
		try {
			return mediaMapDAO.getMediaMapById(id);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
	}

	@Override
	public MediaMap updateMediaMap(MediaMap mediaMap) throws BusinessException {
		try {
			return mediaMapDAO.updateMediaMap(mediaMap);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
	}	

	@Override
	public MediaMap getMediaMapByMediaAndObject(Long mediaId, Long objectId, Long objectType)  throws BusinessException {
		try {
			return mediaMapDAO.getMediaMapByMediaAndObject(mediaId, objectId, objectType);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
	}
}

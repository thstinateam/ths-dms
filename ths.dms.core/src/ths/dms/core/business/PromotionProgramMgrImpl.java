package ths.dms.core.business;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.NoSuchAlgorithmException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.GroupLevel;
import ths.dms.core.entities.GroupLevelDetail;
import ths.dms.core.entities.GroupMapping;
import ths.dms.core.entities.PPConvert;
import ths.dms.core.entities.PPConvertDetail;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductGroup;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.PromotionCustAttr;
import ths.dms.core.entities.PromotionCustAttrDetail;
import ths.dms.core.entities.PromotionCustomerMap;
import ths.dms.core.entities.PromotionProductOpen;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.PromotionStaffMap;
import ths.dms.core.entities.RptAccumulativePromotionProgram;
import ths.dms.core.entities.RptAccumulativePromotionProgramDetail;
import ths.dms.core.entities.RptCTTLPay;
import ths.dms.core.entities.RptCTTLPayDetail;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.CmsFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductGroupType;
import ths.dms.core.entities.enumtype.PromotionProgramFilter;
import ths.dms.core.entities.enumtype.PromotionShopMapFilter;
import ths.dms.core.entities.enumtype.PromotionType;
import ths.dms.core.entities.enumtype.SaleOrderDetailFilter;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.ValueType;
import ths.dms.core.entities.filter.CalculatePromotionFilter;
import ths.dms.core.entities.filter.PromotionCustomerFilter;
import ths.dms.core.entities.filter.PromotionStaffFilter;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.entities.vo.ChannelTypeVO;
import ths.dms.core.entities.vo.ExMapping;
import ths.dms.core.entities.vo.ExcelPromotionHeader;
import ths.dms.core.entities.vo.GroupKM;
import ths.dms.core.entities.vo.GroupLevelDetailVO;
import ths.dms.core.entities.vo.GroupLevelVO;
import ths.dms.core.entities.vo.GroupMua;
import ths.dms.core.entities.vo.GroupSP;
import ths.dms.core.entities.vo.LevelMappingVO;
import ths.dms.core.entities.vo.ListGroupKM;
import ths.dms.core.entities.vo.ListGroupMua;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.MapMuaKM;
import ths.dms.core.entities.vo.NewLevelMapping;
import ths.dms.core.entities.vo.NewProductGroupVO;
import ths.dms.core.entities.vo.Node;
import ths.dms.core.entities.vo.NodeAmount;
import ths.dms.core.entities.vo.NodePercent;
import ths.dms.core.entities.vo.NodeSP;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PPConvertDetailVO;
import ths.dms.core.entities.vo.PPConvertVO;
import ths.dms.core.entities.vo.ProductCatVO;
import ths.dms.core.entities.vo.ProductGroupVO;
import ths.dms.core.entities.vo.ProductInfoVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.entities.vo.PromoProductConvVO;
import ths.dms.core.entities.vo.PromotionCustAttUpdateVO;
import ths.dms.core.entities.vo.PromotionCustAttVO2;
import ths.dms.core.entities.vo.PromotionCustAttrVO;
import ths.dms.core.entities.vo.PromotionCustomerVO;
import ths.dms.core.entities.vo.PromotionItem;
import ths.dms.core.entities.vo.PromotionProductOpenVO;
import ths.dms.core.entities.vo.PromotionProductsVO;
import ths.dms.core.entities.vo.PromotionShopMapVO;
import ths.dms.core.entities.vo.PromotionShopVO;
import ths.dms.core.entities.vo.PromotionStaffVO;
import ths.dms.core.entities.vo.RptAccumulativePromotionProgramVO;
import ths.dms.core.entities.vo.SaleCatLevelVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderLotVO;
import ths.dms.core.entities.vo.SubLevelMapping;
import ths.dms.core.entities.vo.ZV03View;
import ths.dms.core.entities.vo.ZV07View;
import ths.core.entities.vo.rpt.RptPromotionDetailDataVO;
import ths.core.entities.vo.rpt.RptPromotionDetailLv01VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.LogUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.AttributeDetailDAO;
import ths.dms.core.dao.ChannelTypeDAO;
import ths.dms.core.dao.CmsDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.ProductInfoDAO;
import ths.dms.core.dao.PromotionCustAttrDAO;
import ths.dms.core.dao.PromotionCustAttrDetailDAO;
import ths.dms.core.dao.PromotionCustomerMapDAO;
import ths.dms.core.dao.PromotionProgramDAO;
import ths.dms.core.dao.PromotionShopMapDAO;
import ths.dms.core.dao.PromotionStaffMapDAO;
import ths.dms.core.dao.SaleLevelCatDAO;
import ths.dms.core.dao.SaleOrderDAO;
import ths.dms.core.dao.SaleOrderDetailDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.StockTotalDAO;

public class PromotionProgramMgrImpl implements PromotionProgramMgr {
	
	private final String QUANTITY = "QUANTITY";
	public static final String DUPLICATE_PRODUCT_IN_PRODUCT_GROUPS = "DUPLICATE_PRODUCT_IN_PRODUCT_GROUPS";
	public static final String DUPLICATE_LEVELS = "DUPLICATE_LEVELS";
	/*
	 * [Dung de xu ly tinh KM cho CTKM tich luy]
	 * Phan bo so luong tich luy cho cac san pham trong nhom nhu binh thuong (bat ke group co define so luong toi thieu hay ko) 
	 */
	private final String ZV23_DISTRIBUTE_MODE_NORMAL = "NORMAL";
	/*
	 * [Dung de xu ly tinh KM cho CTKM tich luy]
	 * Truong hop group co define so luong toi thieu phai dat, 
	 * va lan phan bo dau tien van chua dam bao so luong san pham tich luy da phan bo >= so luong toi thieu da define,
	 * thi tiep tuc phan bo them de dam bao dieu kien so luong toi thieu cua group.
	 * Neu nhu phan bo thanh cong thi xem nhu dat 1 lan muc KM.
	 * Nguoc lai, muc KM nay ko dat.
	 */
	private final String ZV23_DISTRIBUTE_MODE_ACCUMULATE = "ACCUMULATE";
	
	@Autowired
	PromotionProgramDAO promotionProgramDAO;

	@Autowired
	PromotionShopMapDAO promotionShopMapDAO;
	
	@Autowired
	PromotionCustomerMapDAO promotionCustomerMapDAO;
	
	@Autowired
	PromotionStaffMapDAO promotionStaffMapDAO;
	
	@Autowired
	ShopDAO shopDAO;
	
	@Autowired
	CmsDAO cmsDAO;
	
	@Autowired
	SaleOrderDAO saleOrderDAO;
	
	@Autowired
	ProductInfoDAO productInfoDAO;
	
	@Autowired
	SaleLevelCatDAO saleLevelCatDAO;
	
	@Autowired
	ChannelTypeDAO channelTypeDAO;
	
	@Autowired
	PromotionCustAttrDAO promotionCustAttrDAO;
	
	@Autowired
	AttributeDetailDAO attributeDetailDAO;
	
	@Autowired
	PromotionCustAttrDetailDAO promotionCustAttrDetailDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	SaleOrderDetailDAO saleOrderDetailDAO;
	
	@Autowired
	StockTotalDAO stockTotalDAO;
	
	@Override
	public PromotionProgram createPromotionProgram(
			PromotionProgram promotionProgram, LogInfoVO logInfo)
			throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			promotionProgram.setCreateDate(currentDate);
			promotionProgram.setNameText(Unicode2English.codau2khongdau(promotionProgram.getPromotionProgramName()));
			return promotionProgramDAO.createPromotionProgram(promotionProgram,logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updatePromotionProgram(PromotionProgram promotionProgram,
			LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			promotionProgram.setUpdateDate(currentDate);
			promotionProgram.setUpdateUser(logInfo.getStaffCode());
			promotionProgram.setNameText(Unicode2English.codau2khongdau(promotionProgram.getPromotionProgramName()));
			promotionProgramDAO.updatePromotionProgram(promotionProgram,
					logInfo);
			if(promotionProgram != null && promotionProgram.getId() != null && promotionProgram.getId() > 0){
				List<PromotionShopMap> lstPSM = promotionShopMapDAO.getListPromotionShopMap(null, promotionProgram.getId(), null, null, null, null, null);
				if(!lstPSM.isEmpty()){
					for (PromotionShopMap promotionShopMap : lstPSM) {
						
						if( (promotionShopMap.getToDate() == null && promotionProgram.getToDate() != null)   ||
								(promotionShopMap.getToDate() != null && !promotionShopMap.getToDate().equals(promotionProgram.getToDate()))){
							promotionShopMap.setToDate(promotionProgram.getToDate());
							promotionShopMap.setUpdateDate(currentDate);
							if (logInfo != null)
								promotionShopMap.setUpdateUser(logInfo.getStaffCode());
							promotionShopMapDAO.updatePromotionShopMap(promotionShopMap, logInfo);
						}
					}
				}
			}			
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void deletePromotionProgram(PromotionProgram promotionProgram,
			LogInfoVO logInfo) throws BusinessException {
		try {
			promotionProgramDAO.deletePromotionProgram(promotionProgram,
					logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public PromotionProgram getPromotionProgramById(Long id)
			throws BusinessException {
		try {
			return id == null ? null : promotionProgramDAO
					.getPromotionProgramById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public PromotionProgram getPromotionProgramByCode(String code)
			throws BusinessException {
		try {
			return promotionProgramDAO.getPromotionProgramByCode(code);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public PromotionProgram getPromotionProgramByCodeByType(String code,ApParamType type)
			throws BusinessException {
		try {
			return promotionProgramDAO.getPromotionProgramByCodeByType(code,type);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ProductGroup> getListProductGroupByPromotionId(Long promotionId, ProductGroupType groupType) throws BusinessException {
		try {
			List<ProductGroup> lst = promotionProgramDAO.getListProductGroupByPromotionId(promotionId, groupType);
			return lst;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<NewProductGroupVO> getListNewProductGroupByPromotionId(Long promotionId) throws BusinessException {
		try {
			List<NewProductGroupVO> lst = promotionProgramDAO.getListNewProductGroupByPromotionId(promotionId);
			return lst;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ProductGroupVO> getListProductGroupVO(Long promotionId, ProductGroupType groupType) throws BusinessException {
		try {
			return promotionProgramDAO.getListProductGroupVO(promotionId, groupType);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ProductGroup getProductGroupByCode(String productGroup, ProductGroupType groupType, Long promotionProgramId) throws BusinessException {
		try {
			return promotionProgramDAO.getProductGroupByCode(productGroup, groupType, promotionProgramId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public ProductGroup createProductGroup(Long promotionId, String groupCode, String groupName, ProductGroupType groupType, Integer minQuantity, Integer maxQuantity, BigDecimal minAmount, BigDecimal maxAmount, Boolean multiple, Boolean recursive, Integer stt, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			ProductGroup productGroup = promotionProgramDAO.getProductGroupByCode(groupCode, groupType, promotionId);
			if(productGroup != null) {
				throw new BusinessException(ExceptionCode.ERR_PRODUCT_GROUP_EXIST);
			}
			PromotionProgram promotionProgram = promotionProgramDAO.getPromotionProgramById(promotionId);
			if(promotionProgram == null) {
				throw new BusinessException(ExceptionCode.PROMOTIONPROGRAM_INSTANCE_IS_NULL);
			}
			productGroup = new ProductGroup();
			if(groupCode == null || groupCode.isEmpty()) {
				throw new BusinessException("group_code_null");
			}
			productGroup.setProductGroupCode(groupCode.toUpperCase());
			productGroup.setProductGroupName(groupName);
			productGroup.setPromotionProgram(promotionProgram);
			productGroup.setGroupType(groupType);
			productGroup.setMinQuantity(minQuantity);
			productGroup.setMinAmount(minAmount);
			productGroup.setMaxQuantity(maxQuantity);
			productGroup.setMaxAmount(maxAmount);
			productGroup.setMultiple((multiple != null && multiple == true) ? 1 : 0);
			productGroup.setRecursive((recursive != null && recursive == true) ? 1 : 0);
			productGroup.setOrder(stt);
			productGroup.setCreateDate(currentDate);
			productGroup.setCreateUser(logInfo.getStaffCode());
			return promotionProgramDAO.createProductGroup(productGroup);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateProductGroup(ProductGroup productGroup, LogInfoVO logInfo) throws BusinessException {
		try {
			productGroup.setUpdateUser(logInfo.getStaffCode());
			productGroup.setUpdateDate(commonDAO.getSysDate());
			promotionProgramDAO.updateProductGroup(productGroup);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteLevelDetail(Long levelDetailId, LogInfoVO logInfo) throws BusinessException {
		try {
		GroupLevelDetail detail = promotionProgramDAO.getGroupLevelDetailById(levelDetailId);
		if(detail == null) {
			throw new BusinessException("detail_not_found");
		}
		detail.setStatus(ActiveType.DELETED);
		detail.setUpdateDate(commonDAO.getSysDate());
		detail.setUpdateUser(logInfo.getStaffCode());
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	} 
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void addLevel2SaleProductGroup(ProductGroup productGroup, List<Long> listLevelId, List<Integer> listMinQuantity, List<BigDecimal> listMinAmount, List<Integer> listOrder, List<String> listProductDetail, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			productGroup.setUpdateUser(logInfo.getStaffCode());
			productGroup.setUpdateDate(currentDate);
			promotionProgramDAO.updateProductGroup(productGroup);
			//List<GroupLevel> listGroupLevel = promotionProgramDAO.getListGroupLevelByGroupId(productGroup.getId());
			//List<GroupMapping> lstMapping = promotionProgramDAO.getListGroupMappingByPromotionGroupId(productGroup.getId());
			/*for(GroupLevel level : listGroupLevel) {
				List<GroupLevelDetail> lstDetail = promotionProgramDAO.getListGroupLevelDetailByLevelId(level.getId());
				for(GroupLevelDetail detail : lstDetail) {
					detail.setStatus(ActiveType.DELETED);
					detail.setUpdateDate(currentDate);
					detail.setUpdateUser(logInfo.getStaffCode());
					promotionProgramDAO.updateGroupLevelDetail(detail);
				}
				level.setStatus(ActiveType.DELETED);
				level.setUpdateDate(currentDate);
				level.setUpdateUser(logInfo.getStaffCode());
				promotionProgramDAO.updateGroupLevel(level);
			}*/
			/*for(GroupMapping m : lstMapping) {
				m.setUpdateDate(currentDate);
				m.setUpdateUser(logInfo.getStaffCode());
				m.setStatus(ActiveType.DELETED);
				promotionProgramDAO.updateGroupMapping(m);
			}*/
			String promotionType = productGroup.getPromotionProgram().getType();
			if(PromotionType.ZV19.getValue().equals(promotionType) || PromotionType.ZV20.getValue().equals(promotionType) || PromotionType.ZV21.getValue().equals(promotionType)) {
				for(int i = 0; i < listMinQuantity.size(); i++) {
					Integer qty = listMinQuantity.get(i);
					BigDecimal amount = listMinAmount.get(i);
					Long levelId = listLevelId.get(i);
					Integer stt = listOrder.get(i);
					GroupLevel groupLevel = null;
					if(levelId == 0 || levelId == null) {
						groupLevel = new GroupLevel();
					} else {
						groupLevel = promotionProgramDAO.getGroupLevelById(levelId);
					}
					groupLevel.setMinQuantity((qty == null || qty == 0) ? null : qty);
					groupLevel.setMinAmount((amount == null || BigDecimal.ZERO.compareTo(amount) == 0) ? null : amount);
					groupLevel.setOrder((stt == null || stt == 0) ? null : stt);
					groupLevel.setCreateDate(currentDate);
					groupLevel.setCreateUser(logInfo.getStaffCode());
					groupLevel.setHasProduct(0);
					groupLevel.setProductGroup(productGroup);
					groupLevel.setStatus(ActiveType.RUNNING);
					groupLevel = promotionProgramDAO.createGroupLevel(groupLevel);
				}
			} else {
				for(int i = 0; i < listMinQuantity.size(); i++) {
					Integer qty = listMinQuantity.get(i);
					BigDecimal amount = listMinAmount.get(i);
					Long levelId = listLevelId.get(i);
					Integer stt = listOrder.get(i);
					GroupLevel groupLevel = null;
					if(levelId == 0 || levelId == null) {
						groupLevel = new GroupLevel();
						groupLevel.setCreateDate(currentDate);
						groupLevel.setCreateUser(logInfo.getStaffCode());
					} else {
						groupLevel = promotionProgramDAO.getGroupLevelById(levelId);
					}
					if(listProductDetail != null && !listProductDetail.isEmpty()) {
						String groupText = "(";
						String strDetail = listProductDetail.get(i);
						String __valueType = strDetail.split(";")[0];
						Integer valueType = Integer.valueOf(__valueType);
						List<GroupLevelDetail> listDetail = new ArrayList<GroupLevelDetail>();
						List<GroupLevelDetail> listNonDetail = new ArrayList<GroupLevelDetail>();
						String[] __arrDetail = strDetail.split(";");
						
						for(int ii = 1; ii < __arrDetail.length; ii++) {
							//productCode-value,1
							Integer required = Integer.valueOf(__arrDetail[ii].split(",")[1]);
							String productCode = __arrDetail[ii].split(",")[0].split("-")[0];
							BigDecimal value = BigDecimal.valueOf(Long.valueOf(__arrDetail[ii].split(",")[0].split("-")[1]));
							Product product = productDAO.getProductByCode(productCode);
							if(product == null) {
								throw new BusinessException("product_not_fount");
							}
							GroupLevelDetail detail = new GroupLevelDetail();
							detail.setCreateUser(logInfo.getStaffCode());
							detail.setCreateDate(currentDate);
							detail.setIsRequired(required);
							detail.setProduct(product);
							detail.setStatus(ActiveType.RUNNING);
							detail.setUom(product.getUom1());
							detail.setValue((value == null || BigDecimal.ZERO.compareTo(value) == 0) ? null : value);
							detail.setValueType((valueType == null || valueType == 0) ? null :ValueType.parseValue(valueType));
							if(PromotionType.ZV07.getValue().equals(productGroup.getPromotionProgram().getType()) || PromotionType.ZV08.getValue().equals(productGroup.getPromotionProgram().getType()) || PromotionType.ZV09.getValue().equals(productGroup.getPromotionProgram().getType()) ) {
								if("(".equals(groupText)) {
									groupText += productCode + (required == 1 ? "*" : "");
								} else {
									groupText += ", " + productCode + (required == 1 ? "*" : "");
								}
								detail.setValueType(ValueType.QUANTITY);
								detail.setValue(null);
							} else if(PromotionType.ZV10.getValue().equals(productGroup.getPromotionProgram().getType()) || PromotionType.ZV11.getValue().equals(productGroup.getPromotionProgram().getType()) || PromotionType.ZV12.getValue().equals(productGroup.getPromotionProgram().getType())) {
								if("(".equals(groupText)) {
									groupText += productCode + (required == 1 ? "*" : "");
								} else {
									groupText += ", " + productCode + (required == 1 ? "*" : "");
								}
								detail.setValueType(ValueType.AMOUNT);
								detail.setValue(null);
							} else if(PromotionType.ZV01.getValue().equals(productGroup.getPromotionProgram().getType()) || PromotionType.ZV02.getValue().equals(productGroup.getPromotionProgram().getType()) || PromotionType.ZV03.getValue().equals(productGroup.getPromotionProgram().getType())) {
								if("(".equals(groupText)) {
									groupText += productCode + (required == 1 ? "*" : "") + "(" + qty + ")";
								} else {
									groupText += ", " + productCode + (required == 1 ? "*" : "") + "(" + qty + ")";
								}
							} else if(PromotionType.ZV04.getValue().equals(productGroup.getPromotionProgram().getType()) || PromotionType.ZV05.getValue().equals(productGroup.getPromotionProgram().getType()) || PromotionType.ZV06.getValue().equals(productGroup.getPromotionProgram().getType())) {
								if("(".equals(groupText)) {
									groupText += productCode + (required == 1 ? "*" : "") + "(" + StringUtility.convertMoney(amount) + ")";
								} else {
									groupText += ", " + productCode + (required == 1 ? "*" : "") + "(" + StringUtility.convertMoney(amount) + ")";
								}
							} else {
								if("(".equals(groupText)) {
									groupText += productCode + (required == 1 ? "*" : "") + "(" + StringUtility.convertMoney(value) + ")";
								} else {
									groupText += ", " + productCode + (required == 1 ? "*" : "") + "(" + StringUtility.convertMoney(value) + ")";
								}
							}
							if(groupLevel.getId() != null) {
								GroupLevelDetail __detail = promotionProgramDAO.getGroupLevelDetailByLevelIdProductId(groupLevel.getId(), detail.getProduct().getId());
								if(__detail == null) {
									//create new
									detail.setGroupLevel(groupLevel);
									detail = promotionProgramDAO.createGroupLevelDetail(detail);
								} else if(__detail != null && __detail.getValue() != null && __detail.getValueType() != null && 
										__detail.getValue().equals(detail.getValue()) && 
										__detail.getValueType().equals(detail.getValueType()) &&
										!ActiveType.RUNNING.equals(__detail.getStatus())) {
									//update
									__detail.setStatus(ActiveType.RUNNING);
									__detail.setIsRequired(detail.getIsRequired());
									__detail.setUpdateDate(currentDate);
									__detail.setUpdateUser(logInfo.getStaffCode());
									promotionProgramDAO.updateGroupLevelDetail(__detail);
								} else if(__detail != null && __detail.getIsRequired() != null && detail.getIsRequired() != null &&  !__detail.getIsRequired().equals(detail.getIsRequired())) {
									__detail.setIsRequired(detail.getIsRequired());
									__detail.setUpdateDate(currentDate);
									__detail.setUpdateUser(logInfo.getStaffCode());
									promotionProgramDAO.updateGroupLevelDetail(__detail);
								} else if(__detail != null && __detail.getValue() != null && __detail.getValueType() != null && 
										!__detail.getValue().equals(detail.getValue())) {
									//update
									__detail.setValue(detail.getValue());
									__detail.setValueType(detail.getValueType());
									__detail.setIsRequired(detail.getIsRequired());
									__detail.setUpdateDate(currentDate);
									__detail.setUpdateUser(logInfo.getStaffCode());
									promotionProgramDAO.updateGroupLevelDetail(__detail);
								} else {
									__detail.setStatus(ActiveType.RUNNING);
									__detail.setIsRequired(detail.getIsRequired());
									__detail.setValue(detail.getValue());
									__detail.setValueType(detail.getValueType());
									__detail.setUpdateDate(currentDate);
									__detail.setUpdateUser(logInfo.getStaffCode());
									promotionProgramDAO.updateGroupLevelDetail(__detail);
								}
								listNonDetail.add(__detail);
							} else {
								listDetail.add(detail);
							}
						}
						groupText += ")";
						groupLevel.setHasProduct((!listDetail.isEmpty() || !listNonDetail.isEmpty()) ? 1 : 0);
						groupLevel.setGroupText(groupText);
						groupLevel.setMinQuantity((qty == null || qty == 0) ? null : qty);
						groupLevel.setMinAmount((amount == null || BigDecimal.ZERO.compareTo(amount) == 0) ? null : amount);
						groupLevel.setOrder((stt == null || stt == 0) ? null : stt);
						groupLevel.setProductGroup(productGroup);
						groupLevel.setStatus(ActiveType.RUNNING);
						if(groupLevel.getId() != null) {
							groupLevel.setUpdateDate(currentDate);
							groupLevel.setUpdateUser(logInfo.getStaffCode());
							promotionProgramDAO.updateGroupLevel(groupLevel);
						} else {
							groupLevel = promotionProgramDAO.createGroupLevel(groupLevel);
							for(GroupLevelDetail levelDetail : listDetail) {
								levelDetail.setGroupLevel(groupLevel);
								levelDetail = promotionProgramDAO.createGroupLevelDetail(levelDetail);
							}
						}
					} else {
						groupLevel.setHasProduct(0);
						groupLevel.setMinQuantity((qty == null || qty == 0) ? null : qty);
						groupLevel.setMinAmount((amount == null || BigDecimal.ZERO.compareTo(amount) == 0) ? null : amount);
						groupLevel.setOrder((stt == null || stt == 0) ? null : stt);
						groupLevel.setProductGroup(productGroup);
						groupLevel.setStatus(ActiveType.RUNNING);
						if(groupLevel.getId() != null) {
							groupLevel.setUpdateDate(currentDate);
							groupLevel.setUpdateUser(logInfo.getStaffCode());
							promotionProgramDAO.updateGroupLevel(groupLevel);
						} else {
							groupLevel = promotionProgramDAO.createGroupLevel(groupLevel);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void addLevel2FreeProductGroup(ProductGroup productGroup, List<Long> listLevelId, List<Integer> listMaxQuantity, List<Integer> listOrder, List<BigDecimal> listMaxAmount, List<Float> listPercent, List<String> listProductDetail, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			productGroup.setUpdateUser(logInfo.getStaffCode());
			productGroup.setUpdateDate(currentDate);
			promotionProgramDAO.updateProductGroup(productGroup);
			for (int i = 0, sizei = listMaxQuantity.size(); i < sizei; i++) {
				Integer qty = listMaxQuantity.get(i);
				BigDecimal amt = listMaxAmount.get(i);
				Float percent = listPercent.get(i);
				Integer stt = listOrder.get(i);
				Long levelId = listLevelId.get(i);
				GroupLevel groupLevel = null;
				if(levelId == 0 || levelId == null) {
					groupLevel = new GroupLevel();
					groupLevel.setCreateDate(currentDate);
					groupLevel.setCreateUser(logInfo.getStaffCode());
				} else {
					groupLevel = promotionProgramDAO.getGroupLevelById(levelId);
				}
				if(listProductDetail != null && !listProductDetail.isEmpty()) {
					String groupText = "(";
					String strDetail = listProductDetail.get(i);
					String __valueType = strDetail.split(";")[0];
					Integer valueType = Integer.valueOf(__valueType);
					List<GroupLevelDetail> listDetail = new ArrayList<GroupLevelDetail>();
					ValueType valueTypeGlobal = null;
					Integer maxQuantityGlobal = null;
					BigDecimal maxAmountGlobal = null;
					String[] __arrDetail = strDetail.split(";");
					
					for (int ii = 1, sizeii = __arrDetail.length; ii < sizeii; ii++) {
						//productCode-value,1
						Integer required = Integer.valueOf(__arrDetail[ii].split(",")[1]);
						String productCode = __arrDetail[ii].split(",")[0].split("-")[0];
						BigDecimal value = BigDecimal.valueOf(Long.valueOf(__arrDetail[ii].split(",")[0].split("-")[1]));
						Product product = productDAO.getProductByCode(productCode);
						if (product == null) {
							throw new BusinessException("product_not_fount");
						}
						GroupLevelDetail detail = new GroupLevelDetail();
						detail.setCreateUser(logInfo.getStaffCode());
						detail.setCreateDate(currentDate);
						detail.setIsRequired(required);
						detail.setProduct(product);
						detail.setStatus(ActiveType.RUNNING);
						detail.setUom(product.getUom1());
						detail.setValue(value);
						detail.setValueType(ValueType.parseValue(valueType));
						if ("(".equals(groupText)) {
							groupText += productCode + (required == 1 ? "*" : "") + "(" + StringUtility.convertMoney(value) + ")";
						} else {
							groupText += ", " + productCode + (required == 1 ? "*" : "") + "(" + StringUtility.convertMoney(value) + ")";
						}
						if (groupLevel.getId() != null) {
							GroupLevelDetail __detail = promotionProgramDAO.getGroupLevelDetailByLevelIdProductId(groupLevel.getId(), detail.getProduct().getId());
							if (__detail == null) {
								//create new
								detail.setGroupLevel(groupLevel);
								detail = promotionProgramDAO.createGroupLevelDetail(detail);
							} else if (__detail != null && __detail.getValue() != null && __detail.getValueType() != null && __detail.getValue().equals(detail.getValue()) && __detail.getValueType().equals(detail.getValueType())
									&& !ActiveType.RUNNING.equals(__detail.getStatus())) {
								//update
								__detail.setStatus(ActiveType.RUNNING);
								__detail.setIsRequired(detail.getIsRequired());
								__detail.setUpdateDate(currentDate);
								__detail.setUpdateUser(logInfo.getStaffCode());
								promotionProgramDAO.updateGroupLevelDetail(__detail);
							} else if (__detail != null && __detail.getValue() != null && __detail.getValueType() != null && !__detail.getValue().equals(detail.getValue())) {
								//update
								__detail.setValue(detail.getValue());
								__detail.setValueType(detail.getValueType());
								__detail.setIsRequired(detail.getIsRequired());
								__detail.setUpdateDate(currentDate);
								__detail.setUpdateUser(logInfo.getStaffCode());
								promotionProgramDAO.updateGroupLevelDetail(__detail);
							} else {
								__detail.setValue(detail.getValue());
								__detail.setValueType(detail.getValueType());
								__detail.setStatus(ActiveType.RUNNING);
								__detail.setIsRequired(detail.getIsRequired());
								__detail.setUpdateDate(currentDate);
								__detail.setUpdateUser(logInfo.getStaffCode());
								promotionProgramDAO.updateGroupLevelDetail(__detail);
							}
						} else {
							listDetail.add(detail);
						}
					}
					groupText += ")";
					groupLevel.setHasProduct(1);
					groupLevel.setGroupText(groupText);
					if(ValueType.QUANTITY.equals(valueTypeGlobal)) {
						groupLevel.setMaxQuantity(maxQuantityGlobal);
					} else if(ValueType.AMOUNT.equals(valueTypeGlobal)) {
						groupLevel.setMaxAmount(maxAmountGlobal);
					}
					groupLevel.setMaxQuantity((qty == null || qty == 0) ? null : qty);
					groupLevel.setMaxAmount((amt == null || BigDecimal.ZERO.compareTo(amt) == 0) ? null : amt);
					groupLevel.setPercent((percent == null || percent == 0.0) ? null : percent);
					groupLevel.setOrder((stt == null || stt == 0) ? null : stt);
					groupLevel.setProductGroup(productGroup);
					groupLevel.setStatus(ActiveType.RUNNING);
					if(groupLevel.getId() != null) {
						groupLevel.setUpdateDate(currentDate);
						groupLevel.setUpdateUser(logInfo.getStaffCode());
						promotionProgramDAO.updateGroupLevel(groupLevel);
					} else {
						groupLevel = promotionProgramDAO.createGroupLevel(groupLevel);
						for(GroupLevelDetail levelDetail : listDetail) {
							levelDetail.setGroupLevel(groupLevel);
							levelDetail = promotionProgramDAO.createGroupLevelDetail(levelDetail);
						}
					}
				} else {
					groupLevel.setHasProduct(0);
					groupLevel.setMaxQuantity((qty == null || qty == 0) ? null : qty);
					groupLevel.setMaxAmount((amt == null || BigDecimal.ZERO.compareTo(amt) == 0) ? null : amt);
					groupLevel.setPercent((percent == null || percent == 0.0) ? null : percent);
					groupLevel.setOrder((stt == null || stt == 0) ? null : stt);
					groupLevel.setProductGroup(productGroup);
					groupLevel.setStatus(ActiveType.RUNNING);
					if(groupLevel.getId() != null) {
						groupLevel.setUpdateDate(currentDate);
						groupLevel.setUpdateUser(logInfo.getStaffCode());
						promotionProgramDAO.updateGroupLevel(groupLevel);
					} else {
						groupLevel = promotionProgramDAO.createGroupLevel(groupLevel);
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<GroupLevel> getListLevelNotInMapping(Long promotionId) throws BusinessException {
		try {
			return promotionProgramDAO.getListLevelNotInMapping(promotionId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public PromotionProgram checkProductExistInOrPromotion(Long promotionId) throws BusinessException {
		try {
			return promotionProgramDAO.checkProductExistInOrPromotion(promotionId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ProductGroup getProductGroupById(Long groupId) throws BusinessException {
		try {
			return promotionProgramDAO.getProductGroup(groupId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override 
	public ObjectVO<NewLevelMapping> getListMappingLevel(Long groupMuaId, Long groupKMId, Integer fromLevel, Integer toLevel) throws BusinessException {
		try {
			return promotionProgramDAO.getListMappingLevel(groupMuaId, groupKMId, fromLevel, toLevel);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	class TempClass {
		String productCode;
		BigDecimal value;
	}
	
	@Override
	public Boolean checkCoCau(Long promotionId) throws BusinessException {
		try {
			List<NewProductGroupVO> lstGroup = promotionProgramDAO.getListNewProductGroupByPromotionId(promotionId);
			for(NewProductGroupVO group : lstGroup) {
				List<NewLevelMapping> listMapping = promotionProgramDAO.getListMappingLevel(group.getGroupMuaId(), group.getGroupKMId(), null, null).getLstObject();
				for(NewLevelMapping level : listMapping) {
					Integer __check = checkLevel(level.getMapId(), level.getListExLevelMua(), level.getListExLevelKM());
					boolean check = (__check == 0) ? true : false;
					if(!check) {
						return false;
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		return true;
	}
	
	@Override
	public Integer checkLevel(Long mappingId, List<ExMapping> listSubLevelMua, List<ExMapping> listSubLevelKM) throws BusinessException {
		try {
			GroupMapping mapping = promotionProgramDAO.getGroupMappingById(mappingId);
			ProductGroup groupMua = mapping.getSaleGroup();
			ProductGroup groupKM = mapping.getPromotionGroup();

			List<GroupMapping> listMap = promotionProgramDAO.getlistMappingLevel(groupMua.getId(), groupKM.getId());

			Integer checkQuantityOrAmount = 0;

			List<TempClass> tempList = new ArrayList<TempClass>();
			for (ExMapping ex : listSubLevelMua) {
				if (ex.getListSubLevel() != null && ex.getListSubLevel().size() > 0) {
					for (SubLevelMapping sub : ex.getListSubLevel()) {
						TempClass tempClass = new TempClass();
						tempClass.productCode = sub.getProductCode();
						if (sub.getQuantity() != null && sub.getQuantity() != 0) {
							tempClass.value = new BigDecimal(sub.getQuantity());
							if (checkQuantityOrAmount == 0) {
								checkQuantityOrAmount = 1;
							} else if (checkQuantityOrAmount == 1) {

							} else if (checkQuantityOrAmount == 2) {
								//return 3;
							}
						} else if (sub.getAmount() != null && !BigDecimal.ZERO.equals(sub.getAmount())) {
							tempClass.value = sub.getAmount();
							if (checkQuantityOrAmount == 0) {
								checkQuantityOrAmount = 2;
							} else if (checkQuantityOrAmount == 1) {
								//return 3;
							} else if (checkQuantityOrAmount == 2) {

							}
						} else if (ex.getMinQuantityMua() != null && ex.getMinQuantityMua() != 0) {
							tempClass.value = new BigDecimal(ex.getMinQuantityMua());
							if (checkQuantityOrAmount == 0) {
								checkQuantityOrAmount = 1;
							} else if (checkQuantityOrAmount == 1) {

							} else if (checkQuantityOrAmount == 2) {
								//return 3;
							}
						} else if (ex.getMinAmountMua() != null && !BigDecimal.ZERO.equals(ex.getMinAmountMua())) {
							tempClass.value = ex.getMinAmountMua();
							if (checkQuantityOrAmount == 0) {
								checkQuantityOrAmount = 2;
							} else if (checkQuantityOrAmount == 1) {
								//return 3;
							} else if (checkQuantityOrAmount == 2) {

							}
						}
						tempList.add(tempClass);
					}
				}
			}
			Collections.sort(tempList, new Comparator<TempClass>() {
				@Override
				public int compare(TempClass o1, TempClass o2) {
					return o1.productCode.compareTo(o2.productCode);
				}
			});

			for (GroupMapping map : listMap) {
				if (!map.getId().equals(mappingId)) {
					Map<Long, List<SubLevelMapping>> __map = promotionProgramDAO.getListMapDetail(map.getSaleGroupLevel().getId(), ProductGroupType.MUA);
					List<TempClass> __tempList = new ArrayList<TempClass>();
					for (Long key : __map.keySet()) {
						List<SubLevelMapping> __listSubLevelMua = __map.get(key);
						for (SubLevelMapping sub : __listSubLevelMua) {
							TempClass tempClass = new TempClass();
							tempClass.productCode = sub.getProductCode();
							tempClass.value = (sub.getQuantity() == null && sub.getAmount() == null) ? null : (sub.getQuantity() == null ? sub.getAmount() : new BigDecimal(sub.getQuantity()));
							__tempList.add(tempClass);
						}
					}
					Collections.sort(__tempList, new Comparator<TempClass>() {
						@Override
						public int compare(TempClass o1, TempClass o2) {
							return o1.productCode.compareTo(o2.productCode);
						}
					});

					if (tempList.size() > 0 && __tempList.size() > 0) {
						if (tempList.size() != __tempList.size()) {
							return 2;
						} else {
							for (int i = 0; i < tempList.size(); i++) {
								if (!tempList.get(i).productCode.equals(__tempList.get(i).productCode)) {
									return 2;
								} else if (tempList.get(i).productCode.equals(__tempList.get(i).productCode) && tempList.get(i).value.compareTo(__tempList.get(i).value) == 0) {
									return 1;
								}
							}
						}
					}
				}
			}
			return 0;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Map newSaveSublevel(Long mappingId, Integer stt, List<ExMapping> listSubLevelMua, List<ExMapping> listSubLevelKM, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			GroupMapping mapping = promotionProgramDAO.getGroupMappingById(mappingId);
			if (mapping == null) {
				return null;
			}
			GroupLevel groupLevelMuaParent = mapping.getSaleGroupLevel();
			//			boolean isMuaParentHasProduct = false;
			GroupLevel groupLevelKMParent = mapping.getPromotionGroupLevel();
			//			boolean isKMParentHasProduct = false;
			if (mapping.getSaleGroup() == null) {
				return null;
			}
			PromotionProgram pp = mapping.getSaleGroup().getPromotionProgram();
			if (pp == null) {
				return null;
			}
			String ZVType = pp.getType();
			final int REQUIRE = 1;
			final int HAS_PRODUCT = 1;
			final int HASNT_PRODUCT = 0;

			List<Long> lstProductId = new ArrayList<Long>();
			if (listSubLevelMua != null && listSubLevelMua.size() > 0) {
				StringBuilder lstCheckProductIds = null;
				StringBuilder lstCheckValues = null;
				for (ExMapping __subLevel : listSubLevelMua) {
					lstCheckProductIds = new StringBuilder();
					lstCheckValues = new StringBuilder();
					//ExMapping __subLevel = listSubLevelMua.get(i);
					GroupLevel subLevel = null;
					if (__subLevel.getLevelId() != null && __subLevel.getLevelId() > 0) {
						subLevel = promotionProgramDAO.getGroupLevelById(__subLevel.getLevelId());
						if (subLevel == null) {
							throw new BusinessException("sub_level_id_not_exists");
						}
					} else {
						subLevel = new GroupLevel();
					}
					subLevel.setParentLevel(groupLevelMuaParent);
					subLevel.setPromotionProgramId(groupLevelMuaParent.getPromotionProgramId());
					subLevel.setGroupText(__subLevel.getTextCode());
					subLevel.setMinQuantity((__subLevel.getMinQuantityMua() == null || __subLevel.getMinQuantityMua() == 0) ? null : __subLevel.getMinQuantityMua());
					subLevel.setMinAmount((__subLevel.getMinAmountMua() == null || BigDecimal.ZERO.equals(__subLevel.getMinAmountMua())) ? null : __subLevel.getMinAmountMua());
					subLevel.setStatus(ActiveType.RUNNING);
					subLevel.setProductGroup(mapping.getSaleGroup());
					if (__subLevel.getListSubLevel() != null && __subLevel.getListSubLevel().size() > 0) {
						subLevel.setHasProduct(HAS_PRODUCT);
						groupLevelMuaParent.setHasProduct(HAS_PRODUCT);
					} else {
						subLevel.setHasProduct(HASNT_PRODUCT);
						groupLevelMuaParent.setHasProduct(HASNT_PRODUCT);
					}
					if (subLevel.getId() != null && subLevel.getId() > 0) {
						subLevel.setUpdateDate(currentDate);
						subLevel.setUpdateUser(logInfo.getStaffCode());
						promotionProgramDAO.updateGroupLevel(subLevel);
					} else {
						subLevel.setCreateDate(currentDate);
						subLevel.setCreateUser(logInfo.getStaffCode());
						subLevel = promotionProgramDAO.createGroupLevel(subLevel);
					}
					__subLevel.setLevelId(subLevel.getId());
					__subLevel.setParentId(groupLevelMuaParent.getId());
					StringBuilder groupText = new StringBuilder("(");
					if (__subLevel.getListSubLevel() != null && __subLevel.getListSubLevel().size() > 0) {
						for (SubLevelMapping __detail : __subLevel.getListSubLevel()) {
							//							SubLevelMapping __detail = __subLevel.getListSubLevel().get(j);
							GroupLevelDetail detail = null;
							if (__detail.getLevelDetailId() != null && __detail.getLevelDetailId() > 0) {
								detail = promotionProgramDAO.getGroupLevelDetailById(__detail.getLevelDetailId());
							} else {
								detail = new GroupLevelDetail();
							}
							Product product = productDAO.getProductByCode(__detail.getProductCode());
							if (product == null) {
								continue;
							}
							if (!lstProductId.contains(product.getId())) {
								lstProductId.add(product.getId());
							}

							detail.setProduct(product);
							detail.setUom(product.getUom1());
							if (__detail.getQuantity() != null && __detail.getQuantity() > 0) {
								if (!PromotionType.ZV01.getValue().equalsIgnoreCase(ZVType) && !PromotionType.ZV04.getValue().equalsIgnoreCase(ZVType)) {
									detail.setValueType(ValueType.QUANTITY);
									detail.setValue(new BigDecimal(__detail.getQuantity()));
								}
							} else if (__detail.getAmount() != null && !BigDecimal.ZERO.equals(__detail.getAmount())) {
								if (!PromotionType.ZV01.getValue().equalsIgnoreCase(ZVType) && !PromotionType.ZV04.getValue().equalsIgnoreCase(ZVType)) {
									detail.setValueType(ValueType.AMOUNT);
									detail.setValue(__detail.getAmount());
								}
							} else if (PromotionType.ZV07.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV08.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV09.getValue().equalsIgnoreCase(ZVType)) {
								detail.setValueType(ValueType.QUANTITY);
							} else if (PromotionType.ZV10.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV11.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV12.getValue().equalsIgnoreCase(ZVType)) {
								detail.setValueType(ValueType.AMOUNT);
							}

							lstCheckProductIds.append(",").append(product.getId());
							lstCheckValues.append(",").append(detail.getValue() != null ? detail.getValue() : "");

							if (PromotionType.ZV01.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV02.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV03.getValue().equalsIgnoreCase(ZVType)
									|| PromotionType.ZV04.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV05.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV06.getValue().equalsIgnoreCase(ZVType)
									|| PromotionType.ZV13.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV14.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV15.getValue().equalsIgnoreCase(ZVType)
									|| PromotionType.ZV16.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV17.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV18.getValue().equalsIgnoreCase(ZVType)) {
								detail.setIsRequired(REQUIRE);
								// cap nhat group-text
								if (PromotionType.ZV01.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV02.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV03.getValue().equalsIgnoreCase(ZVType)) {
									groupText.append(product.getProductCode()).append("*(").append(__detail.getQuantity()).append(")");
									if (detail.getValue() != null) {
										subLevel.setMinQuantity(detail.getValue().intValue());
									}
								} else if (PromotionType.ZV04.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV05.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV06.getValue().equalsIgnoreCase(ZVType)) {
									groupText.append(product.getProductCode()).append("*(").append(StringUtility.convertMoney(__detail.getAmount())).append(")");
									subLevel.setMinAmount(detail.getValue());
								}
							} else {
								detail.setIsRequired(__detail.getIsRequired());
							}
							detail.setGroupLevel(subLevel);
							detail.setPromotionProgramId(groupLevelMuaParent.getPromotionProgramId());
							if (detail.getId() != null && detail.getId() > 0) {
								detail.setUpdateDate(currentDate);
								detail.setUpdateUser(logInfo.getStaffCode());
								promotionProgramDAO.updateGroupLevelDetail(detail);
							} else {
								detail.setCreateDate(currentDate);
								detail.setCreateUser(logInfo.getStaffCode());
								detail = promotionProgramDAO.createGroupLevelDetail(detail);
							}
							__detail.setLevelDetailId(detail.getId());
							__detail.setLevelId(__subLevel.getLevelId());
						}
					}
					groupText.append(")");
					if (PromotionType.ZV01.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV02.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV03.getValue().equalsIgnoreCase(ZVType)
							|| PromotionType.ZV04.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV05.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV06.getValue().equalsIgnoreCase(ZVType)) {
						subLevel.setGroupText(groupText.toString());
						groupLevelMuaParent.setGroupText(subLevel.getGroupText());
						subLevel.setOrder(groupLevelMuaParent.getOrder());
					}
					promotionProgramDAO.updateGroupLevel(subLevel);

					if (PromotionType.ZV01.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV02.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV03.getValue().equalsIgnoreCase(ZVType)
							|| PromotionType.ZV04.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV05.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV06.getValue().equalsIgnoreCase(ZVType)
							|| PromotionType.ZV13.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV14.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV15.getValue().equalsIgnoreCase(ZVType)
							|| PromotionType.ZV16.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV17.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV18.getValue().equalsIgnoreCase(ZVType)) {
						String lstCheckProducts = lstCheckProductIds.toString().replaceFirst(",", "");
						String lstCheckQtt = lstCheckValues.toString().replaceFirst(",", "");
						List<GroupLevelDetail> lstCheck = promotionProgramDAO.getListDuplicateLevelDetail(mapping.getSaleGroup().getId(), lstCheckProducts, lstCheckQtt, subLevel.getId());
						if (lstCheck != null && lstCheck.size() > 0) {
							throw new IllegalArgumentException(DUPLICATE_LEVELS);
						}
					} else if (PromotionType.ZV07.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV08.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV09.getValue().equalsIgnoreCase(ZVType)) {
						List<GroupLevel> lstCheck = promotionProgramDAO.getListDuplicateLevelWithMinValue(mapping.getSaleGroup().getId(), subLevel.getMinQuantity(), null, subLevel.getId());
						if (lstCheck != null && lstCheck.size() > 0) {
							throw new IllegalArgumentException(DUPLICATE_LEVELS);
						}
					} else if (PromotionType.ZV10.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV11.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV12.getValue().equalsIgnoreCase(ZVType)
							|| PromotionType.ZV19.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV20.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV21.getValue().equalsIgnoreCase(ZVType)) {
						List<GroupLevel> lstCheck = promotionProgramDAO.getListDuplicateLevelWithMinValue(mapping.getSaleGroup().getId(), null, subLevel.getMinAmount(), subLevel.getId());
						if (lstCheck != null && lstCheck.size() > 0) {
							throw new IllegalArgumentException(DUPLICATE_LEVELS);
						}
					}
				}
			}
			if (listSubLevelKM != null && listSubLevelKM.size() > 0) {
				for (ExMapping __subLevel : listSubLevelKM) {
					GroupLevel subLevel = null;
					if (__subLevel.getLevelId() != null && __subLevel.getLevelId() > 0) {
						subLevel = promotionProgramDAO.getGroupLevelById(__subLevel.getLevelId());
						if (subLevel == null) {
							throw new BusinessException("sub_level_id_not_exists");
						}
					} else {
						subLevel = new GroupLevel();
					}
					subLevel.setParentLevel(groupLevelKMParent);
					subLevel.setPromotionProgramId(groupLevelKMParent.getPromotionProgramId());
					subLevel.setGroupText(__subLevel.getTextCode());
					subLevel.setPercent((__subLevel.getPercent() == null || __subLevel.getPercent().floatValue() == 0) ? null : __subLevel.getPercent().floatValue());
					subLevel.setMaxQuantity((__subLevel.getMaxQuantityKM() == null || __subLevel.getMaxQuantityKM() == 0) ? null : __subLevel.getMaxQuantityKM());
					subLevel.setMaxAmount((__subLevel.getMaxAmountKM() == null || BigDecimal.ZERO.equals(__subLevel.getMaxAmountKM())) ? null : __subLevel.getMaxAmountKM());
					subLevel.setStatus(ActiveType.RUNNING);
					subLevel.setProductGroup(mapping.getPromotionGroup());
					if (__subLevel.getListSubLevel() != null && __subLevel.getListSubLevel().size() > 0) {
						subLevel.setHasProduct(HAS_PRODUCT);
						groupLevelKMParent.setHasProduct(HAS_PRODUCT);
					} else {
						subLevel.setHasProduct(HASNT_PRODUCT);
						groupLevelKMParent.setHasProduct(HASNT_PRODUCT);
					}
					if (subLevel.getId() != null && subLevel.getId() > 0) {
						subLevel.setUpdateDate(currentDate);
						subLevel.setUpdateUser(logInfo.getStaffCode());
					} else {
						subLevel.setCreateDate(currentDate);
						subLevel.setCreateUser(logInfo.getStaffCode());
						subLevel = promotionProgramDAO.createGroupLevel(subLevel);
					}
					__subLevel.setLevelId(subLevel.getId());
					__subLevel.setParentId(groupLevelKMParent.getId());
					StringBuilder groupText = new StringBuilder("(");
					if (__subLevel.getListSubLevel() != null && __subLevel.getListSubLevel().size() > 0) {
						for (SubLevelMapping __detail : __subLevel.getListSubLevel()) {
							GroupLevelDetail detail = null;
							if (__detail.getLevelDetailId() != null && __detail.getLevelDetailId() > 0) {
								detail = promotionProgramDAO.getGroupLevelDetailById(__detail.getLevelDetailId());
							} else {
								detail = new GroupLevelDetail();
							}
							Product product = productDAO.getProductByCode(__detail.getProductCode());
							detail.setProduct(product);
							detail.setUom(product.getUom1());
							if (__detail.getQuantity() != null && __detail.getQuantity() > 0) {
								detail.setValueType(ValueType.QUANTITY);
								detail.setValue(new BigDecimal(__detail.getQuantity()));
							} else if (__detail.getAmount() != null) {
								detail.setValueType(ValueType.AMOUNT);
								detail.setValue(__detail.getAmount());
							}
							detail.setIsRequired(__detail.getIsRequired());
							detail.setGroupLevel(subLevel);
							detail.setPromotionProgramId(groupLevelKMParent.getPromotionProgramId());
							if (detail.getId() != null && detail.getId() > 0) {
								detail.setUpdateDate(currentDate);
								detail.setUpdateUser(logInfo.getStaffCode());
								promotionProgramDAO.updateGroupLevelDetail(detail);
							} else {
								detail.setCreateDate(currentDate);
								detail.setCreateUser(logInfo.getStaffCode());
								detail = promotionProgramDAO.createGroupLevelDetail(detail);
							}
							__detail.setLevelDetailId(detail.getId());
							__detail.setLevelId(__subLevel.getLevelId());

							// cap nhat group-text
							if (PromotionType.ZV03.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV06.getValue().equalsIgnoreCase(ZVType)) {
								groupText.append(product.getProductCode()).append("*(").append(__detail.getQuantity()).append(")");
							}
						}
					}
					groupText.append(")");

					if (PromotionType.ZV01.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV02.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV03.getValue().equalsIgnoreCase(ZVType)
							|| PromotionType.ZV04.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV05.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV06.getValue().equalsIgnoreCase(ZVType)) {
						if (PromotionType.ZV01.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV04.getValue().equalsIgnoreCase(ZVType)) {
							subLevel.setGroupText(subLevel.getPercent() + "%");
						} else if (PromotionType.ZV03.getValue().equalsIgnoreCase(ZVType) || PromotionType.ZV06.getValue().equalsIgnoreCase(ZVType)) {
							subLevel.setGroupText(groupText.toString());
						} else {
							subLevel.setGroupText(StringUtility.convertMoney(subLevel.getMinAmount()));
						}
						groupLevelKMParent.setGroupText(subLevel.getGroupText());
						subLevel.setOrder(groupLevelKMParent.getOrder());
					}
					promotionProgramDAO.updateGroupLevel(subLevel);
				}
			}

			groupLevelMuaParent.setUpdateDate(currentDate);
			groupLevelMuaParent.setUpdateUser(logInfo.getStaffCode());
			promotionProgramDAO.updateGroupLevel(groupLevelMuaParent);
			groupLevelKMParent.setUpdateDate(currentDate);
			groupLevelKMParent.setUpdateUser(logInfo.getStaffCode());
			promotionProgramDAO.updateGroupLevel(groupLevelKMParent);

			if (lstProductId != null && lstProductId.size() > 0) {
				StringBuilder productIds = new StringBuilder();
				int i = 0;
				for (Long prodId : lstProductId) {
					if (i == 0) {
						productIds.append(prodId);
						i++;
					} else {
						productIds.append(",").append(prodId);
					}
				}
				List<Product> lstChecking = promotionProgramDAO.getListDuplicateProductInProgram(pp.getId(), mapping.getSaleGroup().getId(), productIds.toString());
				if (lstChecking != null && lstChecking.size() > 0) {
					StringBuilder s = new StringBuilder();
					i = 0;
					for (Product prod : lstChecking) {
						if (i == 0) {
							s.append(prod.getProductCode());
							i++;
						} else {
							s.append(", ").append(prod.getProductCode());
						}
					}
					throw new IllegalArgumentException(DUPLICATE_PRODUCT_IN_PRODUCT_GROUPS + s.toString());
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		Map<String, Object> result = new HashMap<String, Object>();
		if (listSubLevelMua != null && listSubLevelMua.size() > 0) {
			result.put("listSubLevelMua", listSubLevelMua);
		}
		if (listSubLevelKM != null && listSubLevelKM.size() > 0) {
			result.put("listSubLevelKM", listSubLevelKM);
		}
		return result;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<NewLevelMapping> newCopyLevel(Long mappingId, Integer copyNum, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			GroupMapping __copyMapping = promotionProgramDAO.getGroupMappingById(mappingId);
			List<NewLevelMapping> result = new ArrayList<NewLevelMapping>();
			Integer maxOrderMua = promotionProgramDAO.getMaxOrderNumberOfGroupLevel(__copyMapping.getSaleGroupLevel().getProductGroup().getId());
			Integer maxOrderKM = promotionProgramDAO.getMaxOrderNumberOfGroupLevel(__copyMapping.getPromotionGroupLevel().getProductGroup().getId());
			for (int i = 0; i < copyNum; i++) {
				NewLevelMapping mapping = new NewLevelMapping();

				ProductGroup groupMua = __copyMapping.getSaleGroup();
				ProductGroup groupKM = __copyMapping.getPromotionGroup();

				GroupLevel __parentLevelMua = __copyMapping.getSaleGroupLevel();
				GroupLevel __parentLevelKM = __copyMapping.getPromotionGroupLevel();
				//copy GroupMapping
				GroupLevel parentLevelMua = __parentLevelMua.clone();
				if (parentLevelMua.getOrder() != null) {
					parentLevelMua.setOrder(maxOrderMua + (i + 1));
				}
				parentLevelMua.setCreateDate(currentDate);
				parentLevelMua.setCreateUser(logInfo.getStaffCode());
				parentLevelMua.setLevelCode(__parentLevelMua.getLevelCode());
				parentLevelMua = promotionProgramDAO.createGroupLevel(parentLevelMua);
				GroupLevel parentLevelKM = __parentLevelKM.clone();
				if (parentLevelKM.getOrder() != null) {
					parentLevelKM.setOrder(maxOrderKM + (i + 1));
				}
				parentLevelKM.setCreateDate(currentDate);
				parentLevelKM.setCreateUser(logInfo.getStaffCode());
				parentLevelKM.setLevelCode(__parentLevelKM.getLevelCode());
				parentLevelKM = promotionProgramDAO.createGroupLevel(parentLevelKM);
				//create map
				GroupMapping copyMapping = new GroupMapping();
				copyMapping.setSaleGroup(groupMua);
				copyMapping.setSaleGroupLevel(parentLevelMua);
				copyMapping.setPromotionGroup(groupKM);
				copyMapping.setPromotionGroupLevel(parentLevelKM);
				copyMapping.setStatus(__copyMapping.getStatus());
				copyMapping.setCreateDate(currentDate);
				copyMapping.setCreateUser(logInfo.getStaffCode());
				copyMapping = promotionProgramDAO.createGroupMapping(copyMapping);
				//set value to mapping
				mapping.setMapId(copyMapping.getId());
				mapping.setLevelCode(parentLevelMua.getLevelCode());
				mapping.setStt(parentLevelMua.getOrder());
				mapping.setLevelMuaId(parentLevelMua.getId());
				mapping.setLevelKMId(parentLevelKM.getId());
				mapping.setGroupMuaId(groupMua.getId());
				mapping.setGroupKMId(groupKM.getId());
				mapping.setMinQuantity(parentLevelMua.getMinQuantity());
				mapping.setMinAmount(parentLevelMua.getMinAmount());
				mapping.setPercent(parentLevelKM.getPercent());
				mapping.setMaxQuantity(parentLevelKM.getMaxQuantity());
				mapping.setMaxAmount(parentLevelKM.getMaxAmount());

				List<GroupLevel> __listSubLevelMua = promotionProgramDAO.getListSubLevelByParentId(__parentLevelMua.getId());
				List<ExMapping> listExLevelMua = new ArrayList<ExMapping>();
				//copy sub level group level mua
				for (GroupLevel __subLevelMua : __listSubLevelMua) {
					GroupLevel subLevelMua = __subLevelMua.clone();
					ExMapping exLevelMua = new ExMapping();
					subLevelMua.setParentLevel(parentLevelMua);
					subLevelMua.setCreateDate(currentDate);
					subLevelMua.setCreateUser(logInfo.getStaffCode());
					subLevelMua = promotionProgramDAO.createGroupLevel(subLevelMua);
					exLevelMua.setLevelId(subLevelMua.getId());
					exLevelMua.setParentId(__parentLevelMua.getId());
					exLevelMua.setTextCode(subLevelMua.getGroupText());
					exLevelMua.setMinQuantityMua(subLevelMua.getMinQuantity());
					exLevelMua.setMinAmountMua(subLevelMua.getMinAmount());
					List<GroupLevelDetail> __listGroupLevelDetail = promotionProgramDAO.getListGroupLevelDetailByLevelId(__subLevelMua.getId());
					List<SubLevelMapping> listSubLevel = new ArrayList<SubLevelMapping>();
					for (GroupLevelDetail __detail : __listGroupLevelDetail) {
						GroupLevelDetail detail = __detail.clone();
						SubLevelMapping subLevel = new SubLevelMapping();
						detail.setCreateDate(currentDate);
						detail.setCreateUser(logInfo.getStaffCode());
						detail.setGroupLevel(subLevelMua);
						detail.setPromotionProgramId(subLevelMua.getPromotionProgramId());
						detail = promotionProgramDAO.createGroupLevelDetail(detail);
						subLevel.setLevelId(subLevelMua.getId());
						subLevel.setLevelDetailId(detail.getId());
						subLevel.setProductId(detail.getProduct().getId());
						subLevel.setProductCode(detail.getProduct().getProductCode());
						subLevel.setProductName(detail.getProduct().getProductName());
						if (ValueType.QUANTITY.equals(detail.getValueType())) {
							subLevel.setQuantity(detail.getValue() != null ? detail.getValue().intValue() : null);
						} else if (ValueType.AMOUNT.equals(detail.getValueType())) {
							subLevel.setAmount(detail.getValue());
						}
						subLevel.setIsRequired(detail.getIsRequired());
						listSubLevel.add(subLevel);
					}
					exLevelMua.setListSubLevel(listSubLevel);
					listExLevelMua.add(exLevelMua);
				}
				mapping.setListExLevelMua(listExLevelMua);

				List<GroupLevel> __listSubLevelKM = promotionProgramDAO.getListSubLevelByParentId(__parentLevelKM.getId());
				List<ExMapping> listExLevelKM = new ArrayList<ExMapping>();
				//copy sub group level km
				for (GroupLevel __subLevelKM : __listSubLevelKM) {
					GroupLevel subLevelKM = __subLevelKM.clone();
					ExMapping exLevelKM = new ExMapping();
					subLevelKM.setParentLevel(parentLevelKM);
					subLevelKM.setCreateDate(currentDate);
					subLevelKM.setCreateUser(logInfo.getStaffCode());
					subLevelKM = promotionProgramDAO.createGroupLevel(subLevelKM);
					exLevelKM.setLevelId(subLevelKM.getId());
					exLevelKM.setParentId(__parentLevelKM.getId());
					exLevelKM.setTextCode(subLevelKM.getGroupText());
					if (subLevelKM.getPercent() != null) {
						exLevelKM.setPercent(BigDecimal.valueOf(subLevelKM.getPercent()));						
					}
					exLevelKM.setMaxQuantityKM(subLevelKM.getMaxQuantity());
					exLevelKM.setMaxAmountKM(subLevelKM.getMaxAmount());
					List<GroupLevelDetail> __listGroupLevelDetail = promotionProgramDAO.getListGroupLevelDetailByLevelId(__subLevelKM.getId());
					List<SubLevelMapping> listSubLevel = new ArrayList<SubLevelMapping>();
					for (GroupLevelDetail __detail : __listGroupLevelDetail) {
						GroupLevelDetail detail = __detail.clone();
						SubLevelMapping subLevel = new SubLevelMapping();
						detail.setCreateDate(currentDate);
						detail.setCreateUser(logInfo.getStaffCode());
						detail.setGroupLevel(subLevelKM);
						detail.setPromotionProgramId(subLevelKM.getPromotionProgramId());
						detail = promotionProgramDAO.createGroupLevelDetail(detail);
						subLevel.setLevelId(subLevelKM.getId());
						subLevel.setLevelDetailId(detail.getId());
						subLevel.setProductId(detail.getProduct().getId());
						subLevel.setProductCode(detail.getProduct().getProductCode());
						subLevel.setProductName(detail.getProduct().getProductName());
						if (ValueType.QUANTITY.equals(detail.getValueType())) {
							subLevel.setQuantity(detail.getValue() != null ? detail.getValue().intValue() : null);
						} else if (ValueType.AMOUNT.equals(detail.getValueType())) {
							subLevel.setAmount(detail.getValue());
						}
						subLevel.setIsRequired(detail.getIsRequired());
						listSubLevel.add(subLevel);
					}
					exLevelKM.setListSubLevel(listSubLevel);
					listExLevelKM.add(exLevelKM);
				}
				mapping.setListExLevelKM(listExLevelKM);
				result.add(mapping);
			}
			return result;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	} 
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public NewLevelMapping newUpdateLevel(Long groupMuaId, Long groupKMId, Long levelMuaId, Long levelKMId, String levelCode, Integer stt, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			
			GroupLevel groupLevelMua = promotionProgramDAO.getGroupLevelById(levelMuaId);
			GroupLevel groupLevelKM = promotionProgramDAO.getGroupLevelById(levelKMId);
			GroupMapping mapping = promotionProgramDAO.getGroupMappingByProductGroupAndLevel(groupMuaId, levelMuaId);
			groupLevelMua.setOrder(stt);
			groupLevelMua.setUpdateDate(currentDate);
			groupLevelMua.setUpdateUser(logInfo.getStaffCode());
			groupLevelMua.setLevelCode(levelCode);
			promotionProgramDAO.updateGroupLevel(groupLevelMua);
			groupLevelKM.setOrder(stt);
			groupLevelKM.setUpdateDate(currentDate);
			groupLevelKM.setUpdateUser(logInfo.getStaffCode());
			promotionProgramDAO.updateGroupLevel(groupLevelKM);
			
			List<ExMapping> listExMua = promotionProgramDAO.getListSubLevelByMapping(levelMuaId);
			List<ExMapping> listExKM = promotionProgramDAO.getListSubLevelByMapping(levelKMId);
			
			NewLevelMapping result = new NewLevelMapping();
			result.setGroupKMId(groupKMId);
			result.setGroupMuaId(groupMuaId);
			result.setLevelCode(levelCode);
			result.setMapId(mapping.getId());
			result.setLevelMuaId(groupLevelMua.getId());
			result.setLevelKMId(groupLevelKM.getId());
			result.setListExLevelKM(listExKM);
			result.setListExLevelMua(listExMua);
			result.setStt(stt);
			return result;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void newDeleteSubLevel(Long levelId, LogInfoVO logInfo)  throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			GroupLevel subLevel = promotionProgramDAO.getGroupLevelById(levelId);
			subLevel.setStatus(ActiveType.DELETED);
			subLevel.setUpdateDate(currentDate);
			subLevel.setUpdateUser(logInfo.getStaffCode());
			List<GroupLevelDetail> listDetail = promotionProgramDAO.getListGroupLevelDetailByLevelId(levelId);
			for(GroupLevelDetail dt : listDetail) {
				dt.setStatus(ActiveType.DELETED);
				promotionProgramDAO.updateGroupLevelDetail(dt);
			}
			promotionProgramDAO.updateGroupLevel(subLevel);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void newDeleteLevel(Long mapId, Long levelMuaId, Long levelKMId, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			GroupMapping mapping = promotionProgramDAO.getGroupMappingById(mapId);
			GroupLevel levelMua = promotionProgramDAO.getGroupLevelById(levelMuaId);
			GroupLevel levelKM = promotionProgramDAO.getGroupLevelById(levelKMId);
			List<GroupLevel> subLevelMua = promotionProgramDAO.getListSubLevelByParentId(levelMuaId);
			List<GroupLevel> subLevelKM = promotionProgramDAO.getListSubLevelByParentId(levelKMId);
			for (GroupLevel levelMuaObj : subLevelMua) {
				List<GroupLevelDetail> listDetail = promotionProgramDAO.getListGroupLevelDetailByLevelId(levelMuaObj.getId());
				for (GroupLevelDetail dt : listDetail) {
					dt.setStatus(ActiveType.DELETED);
					dt.setUpdateDate(currentDate);
					dt.setUpdateUser(logInfo.getStaffCode());
					promotionProgramDAO.updateGroupLevelDetail(dt);
				}
				levelMuaObj.setStatus(ActiveType.DELETED);
				levelMuaObj.setUpdateDate(currentDate);
				levelMuaObj.setUpdateUser(logInfo.getStaffCode());
				promotionProgramDAO.updateGroupLevel(levelMuaObj);
			}
			for (GroupLevel levelKMObj : subLevelKM) {
				List<GroupLevelDetail> listDetail = promotionProgramDAO.getListGroupLevelDetailByLevelId(levelKMObj.getId());
				for (GroupLevelDetail dt : listDetail) {
					dt.setStatus(ActiveType.DELETED);
					dt.setUpdateDate(currentDate);
					dt.setUpdateUser(logInfo.getStaffCode());
					promotionProgramDAO.updateGroupLevelDetail(dt);
				}
				levelKMObj.setStatus(ActiveType.DELETED);
				levelKMObj.setUpdateDate(currentDate);
				levelKMObj.setUpdateUser(logInfo.getStaffCode());
				promotionProgramDAO.updateGroupLevel(levelKMObj);
			}
			levelMua.setStatus(ActiveType.DELETED);
			levelMua.setUpdateDate(currentDate);
			levelMua.setUpdateUser(logInfo.getStaffCode());
			promotionProgramDAO.updateGroupLevel(levelMua);
			levelKM.setStatus(ActiveType.DELETED);
			levelKM.setUpdateDate(currentDate);
			levelKM.setUpdateUser(logInfo.getStaffCode());
			promotionProgramDAO.updateGroupLevel(levelKM);
			mapping.setStatus(ActiveType.DELETED);
			mapping.setUpdateDate(currentDate);
			mapping.setUpdateUser(logInfo.getStaffCode());
			promotionProgramDAO.updateGroupMapping(mapping);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Integer getMaxOrderNumberOfGroupLevel(Long groupProductId) throws BusinessException {
		try {
			return promotionProgramDAO.getMaxOrderNumberOfGroupLevel(groupProductId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public NewLevelMapping newAddLevel(Long groupMuaId, Long groupKMId, String levelCode, Integer stt, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			ProductGroup groupMua = promotionProgramDAO.getProductGroup(groupMuaId);
			ProductGroup groupKM = promotionProgramDAO.getProductGroup(groupKMId);

			GroupLevel levelMua = new GroupLevel();
			levelMua.setCreateDate(currentDate);
			levelMua.setCreateUser(logInfo.getStaffCode());
			levelMua.setOrder(stt);
			levelMua.setProductGroup(groupMua);
			levelMua.setPromotionProgramId(groupMua.getPromotionProgram().getId());
			levelMua.setStatus(ActiveType.RUNNING);
			levelMua.setLevelCode(levelCode);

			GroupLevel levelKM = new GroupLevel();
			levelKM.setCreateDate(currentDate);
			levelKM.setCreateUser(logInfo.getStaffCode());
			levelKM.setOrder(stt);
			levelKM.setProductGroup(groupKM);
			levelKM.setPromotionProgramId(groupKM.getPromotionProgram().getId());
			levelKM.setStatus(ActiveType.RUNNING);
			levelKM.setLevelCode(levelCode);

			levelMua = promotionProgramDAO.createGroupLevel(levelMua);
			levelKM = promotionProgramDAO.createGroupLevel(levelKM);

			GroupMapping mapping = new GroupMapping();
			mapping.setCreateDate(currentDate);
			mapping.setCreateUser(levelCode);
			mapping.setPromotionGroup(groupKM);
			mapping.setSaleGroup(groupMua);
			mapping.setSaleGroupLevel(levelMua);
			mapping.setPromotionGroupLevel(levelKM);
			mapping.setStatus(ActiveType.RUNNING);
			mapping = promotionProgramDAO.createGroupMapping(mapping);

			NewLevelMapping result = new NewLevelMapping();
			result.setGroupKMId(groupKMId);
			result.setGroupMuaId(groupMuaId);
			result.setLevelCode(levelCode);
			result.setMapId(mapping.getId());
			result.setLevelMuaId(levelMua.getId());
			result.setLevelKMId(levelKM.getId());
			result.setListExLevelKM(new ArrayList<ExMapping>());
			result.setListExLevelMua(new ArrayList<ExMapping>());
			result.setStt(stt);
			return result;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Product> getListProductInSaleLevel(Long promotionId) throws BusinessException {
		try {
			return promotionProgramDAO.getListProductInSaleLevel(promotionId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteProductGroup(ProductGroup productGroup, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			List<GroupLevel> listGroupLevel = promotionProgramDAO.getListGroupLevelByGroupId(productGroup.getId());
			List<GroupMapping> lstMapping = promotionProgramDAO.getListGroupMappingByPromotionGroupId(productGroup.getId());
			for (GroupLevel __level : listGroupLevel) {
				List<GroupLevel> subLevel = promotionProgramDAO.getListSubLevelByParentId(__level.getId());
				for (GroupLevel level : subLevel) {
					List<GroupLevelDetail> lstDetail = promotionProgramDAO.getListGroupLevelDetailByLevelId(level.getId());
					for (GroupLevelDetail detail : lstDetail) {
						detail.setStatus(ActiveType.DELETED);
						detail.setUpdateDate(currentDate);
						detail.setUpdateUser(logInfo.getStaffCode());
						promotionProgramDAO.updateGroupLevelDetail(detail);
					}
					level.setStatus(ActiveType.DELETED);
					level.setUpdateDate(currentDate);
					level.setUpdateUser(logInfo.getStaffCode());
					promotionProgramDAO.updateGroupLevel(level);
				}

				__level.setStatus(ActiveType.DELETED);
				__level.setUpdateDate(currentDate);
				__level.setUpdateUser(logInfo.getStaffCode());
				promotionProgramDAO.updateGroupLevel(__level);
			}
			for (GroupMapping m : lstMapping) {
				m.setUpdateDate(currentDate);
				m.setUpdateUser(logInfo.getStaffCode());
				m.setStatus(ActiveType.DELETED);
				promotionProgramDAO.updateGroupMapping(m);
			}
			productGroup.setUpdateDate(currentDate);
			productGroup.setUpdateUser(logInfo.getStaffCode());
			productGroup.setStatus(ActiveType.DELETED);
			promotionProgramDAO.updateProductGroup(productGroup);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<GroupLevelVO> getListGroupLevelVO(Long groupId) throws BusinessException {
		try {
			return promotionProgramDAO.getListGroupLevelVO(groupId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public GroupLevel getGroupLevelByLevelCode(Long groupId, String levelCode, Integer orderNumber) throws BusinessException {
		try {
			return promotionProgramDAO.getGroupLevelByLeveCode(groupId, levelCode, orderNumber);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteGroupLevel(Long levelId, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			GroupLevel groupLevel = promotionProgramDAO.getGroupLevelById(levelId);
			groupLevel.setStatus(ActiveType.DELETED);
			groupLevel.setUpdateDate(currentDate);
			groupLevel.setUpdateUser(logInfo.getStaffCode());
			promotionProgramDAO.updateGroupLevel(groupLevel);
			List<GroupLevelDetail> listDetail = promotionProgramDAO.getListGroupLevelDetailByLevelId(levelId);
			for(GroupLevelDetail detail : listDetail) {
				detail.setStatus(ActiveType.DELETED);
				detail.setUpdateDate(currentDate);
				detail.setUpdateUser(logInfo.getStaffCode());
				promotionProgramDAO.updateGroupLevelDetail(detail);
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public GroupLevel getGroupLevelByLevelId(Long levelId) throws BusinessException {
		try {
			return promotionProgramDAO.getGroupLevelById(levelId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<GroupMapping> getListGroupMappingByLevelId(Long levelMuaId, Long levelKMId) throws BusinessException {
		try {
			return promotionProgramDAO.getListGroupMappingByLevelId(levelMuaId, levelKMId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	private GroupLevel setValueGroupLevel(GroupLevel groupLevel, String typeGLM, Node node, Boolean isSale, Integer quantity, BigDecimal amount, Date currentDate, String staffCode) throws DataAccessException {//GroupLevelDetail
		groupLevel.setStatus(ActiveType.RUNNING);
		groupLevel.setCreateUser(staffCode);
		groupLevel.setCreateDate(currentDate);
		if (isSale) {
			//setMinQuantity
			if (PromotionType.ZV01.getValue().equals(typeGLM) || PromotionType.ZV02.getValue().equals(typeGLM) || PromotionType.ZV03.getValue().equals(typeGLM) || PromotionType.ZV07.getValue().equals(typeGLM)
					|| PromotionType.ZV08.getValue().equals(typeGLM) || PromotionType.ZV09.getValue().equals(typeGLM)) {
				groupLevel.setMinQuantity(quantity);
			}
			//setMinAmount
			if (PromotionType.ZV04.getValue().equals(typeGLM) || PromotionType.ZV05.getValue().equals(typeGLM) || PromotionType.ZV06.getValue().equals(typeGLM) || PromotionType.ZV10.getValue().equals(typeGLM)
					|| PromotionType.ZV11.getValue().equals(typeGLM) || PromotionType.ZV12.getValue().equals(typeGLM) || PromotionType.ZV19.getValue().equals(typeGLM) || PromotionType.ZV20.getValue().equals(typeGLM)
					|| PromotionType.ZV21.getValue().equals(typeGLM)) {
				groupLevel.setMinAmount(amount);
			}
			//set HasProduct
			if (PromotionType.ZV19.getValue().equals(typeGLM) || PromotionType.ZV20.getValue().equals(typeGLM) || PromotionType.ZV21.getValue().equals(typeGLM)) {
				groupLevel.setHasProduct(0);
			} else {
				groupLevel.setHasProduct(1);
			}
		} else {
			//setMaxAmount
			if (PromotionType.ZV02.getValue().equals(typeGLM) || PromotionType.ZV05.getValue().equals(typeGLM) || PromotionType.ZV08.getValue().equals(typeGLM) || PromotionType.ZV11.getValue().equals(typeGLM)
					|| PromotionType.ZV14.getValue().equals(typeGLM) || PromotionType.ZV17.getValue().equals(typeGLM) || PromotionType.ZV20.getValue().equals(typeGLM)) {
				groupLevel.setMaxAmount(amount);
			}
			//MaxQuantity ko set

			//set Hasproduct
			if (PromotionType.ZV03.getValue().equals(typeGLM) || PromotionType.ZV06.getValue().equals(typeGLM) || PromotionType.ZV09.getValue().equals(typeGLM) || PromotionType.ZV12.getValue().equals(typeGLM)
					|| PromotionType.ZV15.getValue().equals(typeGLM) || PromotionType.ZV18.getValue().equals(typeGLM) || PromotionType.ZV21.getValue().equals(typeGLM)) {
				groupLevel.setHasProduct(1);
			} else {
				groupLevel.setHasProduct(0);
			}
		}

		// them group cha
		GroupLevel parent = new GroupLevel();

		parent.setProductGroup(groupLevel.getProductGroup());
		parent.setStatus(groupLevel.getStatus());
		parent.setOrder(groupLevel.getOrder());
		//parent.setMinQuantity(groupLevel.getMinQuantity());
		//parent.setMaxQuantity(groupLevel.getMaxQuantity());
		//parent.setMinAmount(groupLevel.getMinAmount());
		//parent.setMaxAmount(groupLevel.getMaxAmount());
		//parent.setPercent(groupLevel.getPercent());
		parent.setHasProduct(groupLevel.getHasProduct());
		parent.setCreateDate(currentDate);
		parent.setCreateUser(staffCode);
		parent.setGroupText(groupLevel.getGroupText());
		parent.setPromotionProgramId(groupLevel.getPromotionProgramId());

		parent = promotionProgramDAO.createGroupLevel(parent);

		groupLevel.setParentLevel(parent);
		return groupLevel;
	}

	private GroupLevelDetail setValueTypeGLD(GroupLevelDetail groupLevelDetail, String type, NodeSP nodeSP, Boolean isSale, Date currentDate, String staffCode) {//GroupLevelDetail
		groupLevelDetail.setStatus(ActiveType.RUNNING);
		groupLevelDetail.setCreateDate(currentDate);
		groupLevelDetail.setCreateUser(staffCode);
		if (nodeSP.isRequired != null) {
			groupLevelDetail.setIsRequired(nodeSP.isRequired ? 1 : 0);
		}
		if (Boolean.TRUE.equals(isSale)) {//Mua
			//set ValueType
			if (PromotionType.ZV01.getValue().equals(type) || PromotionType.ZV02.getValue().equals(type) || PromotionType.ZV03.getValue().equals(type) || PromotionType.ZV07.getValue().equals(type) || PromotionType.ZV08.getValue().equals(type)
					|| PromotionType.ZV09.getValue().equals(type) || PromotionType.ZV13.getValue().equals(type) || PromotionType.ZV14.getValue().equals(type) || PromotionType.ZV15.getValue().equals(type)) {
				groupLevelDetail.setValueType(ValueType.QUANTITY);
			} else if (PromotionType.ZV04.getValue().equals(type) || PromotionType.ZV05.getValue().equals(type) || PromotionType.ZV06.getValue().equals(type) || PromotionType.ZV10.getValue().equals(type) || PromotionType.ZV11.getValue().equals(type)
					|| PromotionType.ZV12.getValue().equals(type) || PromotionType.ZV16.getValue().equals(type) || PromotionType.ZV17.getValue().equals(type) || PromotionType.ZV18.getValue().equals(type) || PromotionType.ZV19.getValue().equals(type)
					|| PromotionType.ZV20.getValue().equals(type) || PromotionType.ZV21.getValue().equals(type)) {
				groupLevelDetail.setValueType(ValueType.AMOUNT);
			}

			//Set value
			if (PromotionType.ZV13.getValue().equals(type) || PromotionType.ZV14.getValue().equals(type) || PromotionType.ZV15.getValue().equals(type)) {
				groupLevelDetail.setValue(BigDecimal.valueOf(nodeSP.quantity));
			} else if (PromotionType.ZV16.getValue().equals(type) || PromotionType.ZV17.getValue().equals(type) || PromotionType.ZV18.getValue().equals(type)) {
				groupLevelDetail.setValue(nodeSP.amount);
			}
		} else {//KM
				//set ValueType KM dang san pham thi moi co GroupLevelDetail
			if (PromotionType.ZV03.getValue().equals(type) || PromotionType.ZV06.getValue().equals(type) || PromotionType.ZV09.getValue().equals(type) || PromotionType.ZV12.getValue().equals(type) || PromotionType.ZV15.getValue().equals(type)
					|| PromotionType.ZV18.getValue().equals(type) || PromotionType.ZV21.getValue().equals(type)) {
				groupLevelDetail.setValueType(ValueType.QUANTITY);
				groupLevelDetail.setValue(BigDecimal.valueOf(nodeSP.quantity));
			}
		}
		return groupLevelDetail;
	}

	private GroupMapping setGroupMapping(ProductGroup productGroupMua, GroupLevel groupLevelMua, ProductGroup productGroupKM, GroupLevel groupLevelKM, Date currentDate, String staffCode) {
		GroupMapping groupMapping = new GroupMapping();
		groupMapping.setSaleGroup(productGroupMua);
		if (groupLevelMua.getParentLevel() == null) {
			groupMapping.setSaleGroupLevel(groupLevelMua);
		} else {
			groupLevelMua.getParentLevel().setGroupText(groupLevelMua.getGroupText());
			groupMapping.setSaleGroupLevel(groupLevelMua.getParentLevel());
		}
		groupMapping.setPromotionGroup(productGroupKM);
		if (groupLevelKM.getParentLevel() == null) {
			groupMapping.setPromotionGroupLevel(groupLevelKM);
		} else {
			groupLevelKM.getParentLevel().setGroupText(groupLevelKM.getGroupText());
			groupMapping.setPromotionGroupLevel(groupLevelKM.getParentLevel());
		}
		groupMapping.setStatus(ActiveType.RUNNING);
		groupMapping.setCreateUser(staffCode);
		groupMapping.setCreateDate(currentDate);
		return groupMapping;
	}

	private ProductGroup setProductGroup(String type, Boolean isSale, String groupCode, Integer order, PromotionProgram __promotionProgram, Date currentDate, String staffCode) {
		ProductGroup productGroup = new ProductGroup();
		//productGroup.setOrder(order);
		productGroup.setProductGroupCode(groupCode);
		productGroup.setProductGroupName(productGroup.getProductGroupCode());
		productGroup.setPromotionProgram(__promotionProgram);
		productGroup.setStatus(ActiveType.RUNNING);
		productGroup.setCreateUser(staffCode);
		productGroup.setCreateDate(currentDate);
		if (isSale) {
			productGroup.setGroupType(ProductGroupType.MUA);
			if (PromotionType.ZV01.getValue().equals(type) || PromotionType.ZV04.getValue().equals(type) || PromotionType.ZV07.getValue().equals(type) || PromotionType.ZV10.getValue().equals(type) || PromotionType.ZV19.getValue().equals(type)) {
				productGroup.setMultiple(0);
			} else {
				productGroup.setMultiple(1);
			}
			productGroup.setRecursive(0);
		} else {
			productGroup.setGroupType(ProductGroupType.KM);
		}
		return productGroup;
	}
	
	@Override
	public List<LevelMappingVO> getListLevelMappingByGroupId(Long promotionId, Long groupMuaId, Long groupKMId) throws BusinessException {
		try {
			return promotionProgramDAO.getListLevelMappingByGroupId(promotionId, groupMuaId, groupKMId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteGroupMapping(GroupMapping groupMapping) throws BusinessException {
		try {
			groupMapping.setStatus(ActiveType.DELETED);
			promotionProgramDAO.updateGroupMapping(groupMapping);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public GroupMapping getGroupMappingByGroupCodeAndOrder(Long promotionId, String groupMuaCode, Integer orderLevelMua, String groupKMCode, Integer orderLevelKM) throws BusinessException {
		try {
			return promotionProgramDAO.getGroupMappingByGroupCodeAndOrder(promotionId, groupMuaCode, orderLevelMua, groupKMCode, orderLevelKM);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<GroupMapping> getGroupMappingByGroupCodeAndOrderMua(Long promotionId, String groupMuaCode, Integer orderLevelMua) throws BusinessException {
		try {
			return promotionProgramDAO.getGroupMappingByGroupCodeAndOrderMua(promotionId, groupMuaCode, orderLevelMua);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public GroupMapping getGroupMappingById(Long id) throws BusinessException {
		try {
			return promotionProgramDAO.getGroupMappingById(id);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public GroupMapping createGroupMappingByGroupCodeAndOrder(Long promotionId, String groupMuaCode, Integer orderLevelMua, String groupKMCode, Integer orderLevelKM, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			ProductGroup productGroupMua = promotionProgramDAO.getProductGroupByCode(groupMuaCode, ProductGroupType.MUA, promotionId);
			if(productGroupMua == null) {
				throw new BusinessException("product_group_mua_not_exist");
			}
			ProductGroup productGroupKM = promotionProgramDAO.getProductGroupByCode(groupKMCode, ProductGroupType.KM, promotionId);
			if(productGroupKM == null) {
				throw new BusinessException("product_group_km_not_exist");
			}
			GroupLevel groupLevelMua = promotionProgramDAO.getGroupLevelByOrder(orderLevelMua, productGroupMua.getId());
			if(groupLevelMua == null) {
				throw new BusinessException("product_group_mua_level_not_exist");
			}
			GroupLevel groupLevelKM = promotionProgramDAO.getGroupLevelByOrder(orderLevelKM, productGroupKM.getId());
			if(groupLevelKM == null) {
				throw new BusinessException("product_group_km_level_not_exist");
			}
			GroupMapping groupMapping = new GroupMapping();
			groupMapping.setCreateDate(currentDate);
			groupMapping.setCreateUser(logInfo.getStaffCode());
			groupMapping.setStatus(ActiveType.RUNNING);
			groupMapping.setSaleGroup(productGroupMua);
			groupMapping.setSaleGroupLevel(groupLevelMua);
			groupMapping.setPromotionGroup(productGroupKM);
			groupMapping.setPromotionGroupLevel(groupLevelKM);
			return promotionProgramDAO.createGroupMapping(groupMapping);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Import CTKM
	 * 
	 * @modify hunglm16
	 * @since 10/09/2015
	 * @description mager soure
	 * */
	public void importPromotionVNM(List<ExcelPromotionHeader> listHeader, Map<String, ListGroupMua> mapPromotionMua, Map<String, ListGroupKM> mapPromotionKM, MapMuaKM mapMuaKM, List<PromotionShopVO> lstPromotionShop, LogInfoVO logInfo)
			throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			String s = null;
			Map<String, PromotionProgram> mapPP = new HashMap<String, PromotionProgram>();
			for (ExcelPromotionHeader header : listHeader) {
				PromotionProgram newPromotionProgram = promotionProgramDAO.getPromotionProgramByCode(header.promotionCode);
				if (newPromotionProgram == null) {//tao moi
					newPromotionProgram = new PromotionProgram();
					newPromotionProgram.setPromotionProgramCode(header.promotionCode);
					s = header.description.toUpperCase();
					newPromotionProgram.setPromotionProgramName(s);
					s = Unicode2English.codau2khongdau(s);
					newPromotionProgram.setNameText(s);
					newPromotionProgram.setIsEdited(1);
					newPromotionProgram.setType(header.type);
					newPromotionProgram.setProFormat(header.format);
					newPromotionProgram.setDescription(header.description);
					newPromotionProgram.setStatus(ActiveType.WAITING);
					newPromotionProgram.setFromDate(header.fromDate);
					newPromotionProgram.setToDate(header.toDate);
					newPromotionProgram.setCreateUser(logInfo.getStaffCode());
					newPromotionProgram.setCreateDate(currentDate);
					newPromotionProgram = promotionProgramDAO.createPromotionProgram(newPromotionProgram, logInfo);
				} else if (ActiveType.RUNNING.equals(newPromotionProgram.getStatus())) {
					return;//neu CTKM dang hoat dong thi ko cho sua
				} else {
					promotionProgramDAO.clearPromotionProgram(newPromotionProgram, logInfo);// co promotion roi thi clear cac thong tin lien quan
					newPromotionProgram.setPromotionProgramCode(header.promotionCode);
					s = header.description.toUpperCase();
					newPromotionProgram.setPromotionProgramName(s);
					s = Unicode2English.codau2khongdau(s);
					newPromotionProgram.setNameText(s);
					newPromotionProgram.setIsEdited(1);
					newPromotionProgram.setType(header.type);
					newPromotionProgram.setProFormat(header.format);
					newPromotionProgram.setDescription(header.description);
					newPromotionProgram.setStatus(ActiveType.WAITING);
					newPromotionProgram.setFromDate(header.fromDate);
					newPromotionProgram.setToDate(header.toDate);
					newPromotionProgram.setUpdateUser(logInfo.getStaffCode());
					newPromotionProgram.setUpdateDate(currentDate);
					promotionProgramDAO.updatePromotionProgram(newPromotionProgram, logInfo);
				}
				if (newPromotionProgram != null) {
					mapPP.put(newPromotionProgram.getPromotionProgramCode(), newPromotionProgram);
				}
			}
			for (String promotionCode : mapPromotionMua.keySet()) {
				ListGroupMua lstGroupMua = mapPromotionMua.get(promotionCode);
				ListGroupKM lstGroupKM = mapPromotionKM.get(promotionCode);
				PromotionProgram __promotionProgram = promotionProgramDAO.getPromotionProgramByCode(promotionCode);
				if (__promotionProgram == null) {
					throw new BusinessException(ExceptionCode.ERR_PROMOTION_IMPORT_PROMOTION_NOT_EXIST);
				}

				/**
				 * delete all relate data ProductGroup, GroupLevel,
				 * GroupMapping, GroupLevelDetail
				 */
				List<ProductGroup> listDelProductGroup = promotionProgramDAO.getListProductGroupByPromotionId(__promotionProgram.getId(), null);
				for (ProductGroup productGroup : listDelProductGroup) {
					if (productGroup.getGroupType() == ProductGroupType.MUA) {
						List<GroupLevel> listDelGroupLevel = promotionProgramDAO.getListGroupLevelByGroupId(productGroup.getId());
						for (GroupLevel groupLevel : listDelGroupLevel) {
							List<GroupLevelDetail> listDelGroupLevelDetail = promotionProgramDAO.getListGroupLevelDetailByLevelId(groupLevel.getId());
							for (GroupLevelDetail groupLevelDetail : listDelGroupLevelDetail) {
								groupLevelDetail.setStatus(ActiveType.DELETED);
								promotionProgramDAO.updateGroupLevelDetail(groupLevelDetail);
							}
							groupLevel.setStatus(ActiveType.DELETED);
							promotionProgramDAO.updateGroupLevel(groupLevel);
						}
						List<GroupMapping> listDelGroupMapping = promotionProgramDAO.getListGroupMappingBySaleGroupId(productGroup.getId());
						for (GroupMapping groupMapping : listDelGroupMapping) {
							groupMapping.setStatus(ActiveType.DELETED);
							promotionProgramDAO.updateGroupMapping(groupMapping);
						}
					} else {
						List<GroupLevel> listDelGroupLevel = promotionProgramDAO.getListGroupLevelByGroupId(productGroup.getId());
						for (GroupLevel groupLevel : listDelGroupLevel) {
							List<GroupLevelDetail> listDelGroupLevelDetail = promotionProgramDAO.getListGroupLevelDetailByLevelId(groupLevel.getId());
							for (GroupLevelDetail groupLevelDetail : listDelGroupLevelDetail) {
								groupLevelDetail.setStatus(ActiveType.DELETED);
								promotionProgramDAO.updateGroupLevelDetail(groupLevelDetail);
							}
							groupLevel.setStatus(ActiveType.DELETED);
							promotionProgramDAO.updateGroupLevel(groupLevel);
						}
						List<GroupMapping> listDelGroupMapping = promotionProgramDAO.getListGroupMappingByPromotionGroupId(productGroup.getId());
						for (GroupMapping groupMapping : listDelGroupMapping) {
							groupMapping.setStatus(ActiveType.DELETED);
							promotionProgramDAO.updateGroupMapping(groupMapping);
						}
					}
				}
				/**************************/

				BigDecimal am = null;
				for (int i = 0; i < lstGroupMua.size(); i++) {
					GroupMua groupMua = lstGroupMua.get(i);
					/**
					 * mua A(10), B(10)...
					 */
					String typeGLM = __promotionProgram.getType();//typeGroupLevelMua
					if (groupMua.lstLevel.get(0).lstSP.get(0) instanceof NodeSP && !StringUtility.isNullOrEmpty(groupMua.lstLevel.get(0).lstSP.get(0).productCode) && groupMua.lstLevel.get(0).lstSP.get(0).quantity != null
							&& groupMua.lstLevel.get(0).lstSP.get(0).quantity > 0) {
						ProductGroup productGroupMua = null;
						ProductGroup __productGroupMua = promotionProgramDAO.getProductGroupByCode(groupMua.groupCode, ProductGroupType.MUA, __promotionProgram.getId());
						if (__productGroupMua == null) {
							productGroupMua = setProductGroup(typeGLM, true, groupMua.groupCode, groupMua.order, __promotionProgram, currentDate, logInfo.getStaffCode());
							productGroupMua = promotionProgramDAO.createProductGroup(productGroupMua);
						} else {
							productGroupMua = __productGroupMua;
						}
						for (int j = 0; j < groupMua.lstLevel.size(); j++) {
							GroupSP levelMua = groupMua.lstLevel.get(j);//create level
							GroupLevel groupLevelMua = new GroupLevel();
							groupLevelMua.setOrder(levelMua.order);
							groupLevelMua.setProductGroup(productGroupMua);
							groupLevelMua.setPromotionProgramId(productGroupMua.getPromotionProgram().getId());
							//set quantity cho tung loai
							List<GroupLevelDetail> listGroupLevelMuaDetail = new ArrayList<GroupLevelDetail>();
							groupLevelMua = setValueGroupLevel(groupLevelMua, typeGLM, levelMua.lstSP.get(0), true, levelMua.lstSP.get(0).quantity, levelMua.lstSP.get(0).amount, currentDate, logInfo.getStaffCode());
							if (PromotionType.ZV07.getValue().equals(typeGLM) || PromotionType.ZV08.getValue().equals(typeGLM) || PromotionType.ZV09.getValue().equals(typeGLM)) {
								NodeSP nodeSP = (NodeSP) levelMua.lstSP.get(0);
								String groupLevelMuaText = nodeSP.quantity + "(";
								for (int k = 0, szk = levelMua.lstSP.size(); k < szk; k++) {
									nodeSP = (NodeSP) levelMua.lstSP.get(k);
									//if("(".equals(groupLevelMuaText)) {
									if (k == 0) {
										//groupLevelMuaText += nodeSP.productCode + (nodeSP.isRequired!=null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
										groupLevelMuaText += nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*" : "");
									} else {
										//groupLevelMuaText += ", " + nodeSP.productCode + (nodeSP.isRequired!=null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
										groupLevelMuaText += ", " + nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*" : "");
									}
									GroupLevelDetail groupLevelDetail = new GroupLevelDetail();
									Product product = productDAO.getProductByCode(nodeSP.productCode);
									groupLevelDetail.setProduct(product);
									groupLevelDetail.setUom(product.getUom1());
									groupLevelDetail = setValueTypeGLD(groupLevelDetail, typeGLM, nodeSP, true, currentDate, logInfo.getStaffCode());
									listGroupLevelMuaDetail.add(groupLevelDetail);
								}
								groupLevelMuaText += ")";
								groupLevelMua.setGroupText(groupLevelMuaText);
								groupLevelMua = promotionProgramDAO.createGroupLevel(groupLevelMua);
							} else {
								String groupLevelMuaText = "(";
								for (int k = 0, szk = levelMua.lstSP.size(); k < szk; k++) {
									NodeSP nodeSP = (NodeSP) levelMua.lstSP.get(k);
									if ("(".equals(groupLevelMuaText)) {
										groupLevelMuaText += nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
									} else {
										groupLevelMuaText += ", " + nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
									}
									GroupLevelDetail groupLevelDetail = new GroupLevelDetail();
									Product product = productDAO.getProductByCode(nodeSP.productCode);
									groupLevelDetail.setProduct(product);
									groupLevelDetail.setUom(product.getUom1());
									groupLevelDetail = setValueTypeGLD(groupLevelDetail, typeGLM, nodeSP, true, currentDate, logInfo.getStaffCode());
									listGroupLevelMuaDetail.add(groupLevelDetail);
								}
								groupLevelMuaText += ")";
								groupLevelMua.setGroupText(groupLevelMuaText);
								groupLevelMua = promotionProgramDAO.createGroupLevel(groupLevelMua);
							}
							//create group level detail
							for (GroupLevelDetail grDetail : listGroupLevelMuaDetail) {
//								if (PromotionType.ZV01.getValue().equals(typeGLM) || PromotionType.ZV02.getValue().equals(typeGLM) || PromotionType.ZV03.getValue().equals(typeGLM)) {
								if (PromotionType.ZV02.getValue().equals(typeGLM) || PromotionType.ZV03.getValue().equals(typeGLM)) {
									grDetail.setValue(new BigDecimal(groupLevelMua.getMinQuantity()));
								}
								grDetail.setGroupLevel(groupLevelMua);
								grDetail.setPromotionProgramId(productGroupMua.getPromotionProgram().getId());
								promotionProgramDAO.createGroupLevelDetail(grDetail);
							}

							List<Long> listIndexLevelKM = mapMuaKM.get(levelMua.index);
							for (int ii = 0; ii < listIndexLevelKM.size(); ii++) {
								long indexLevelKM = listIndexLevelKM.get(ii);
								GroupKM groupKM = lstGroupKM.get(lstGroupKM.indexOf(indexLevelKM));//find level and create level
								ProductGroup productGroupKM = null;
								ProductGroup __productGroupKM = promotionProgramDAO.getProductGroupByCode(groupKM.groupCode, ProductGroupType.KM, __promotionProgram.getId());
								if (__productGroupKM == null) {
									productGroupKM = setProductGroup(typeGLM, false, groupKM.groupCode, groupKM.order, __promotionProgram, currentDate, logInfo.getStaffCode());
									productGroupKM = promotionProgramDAO.createProductGroup(productGroupKM);
								} else {
									productGroupKM = __productGroupKM;
								}
								GroupSP levelKM = null;
								for (GroupSP __levelKM : groupKM.lstLevel) {
									if (__levelKM.index == indexLevelKM) {
										levelKM = __levelKM;
										break;
									}
								}
								if (levelKM.lstSP.get(0) instanceof NodeSP && !StringUtility.isNullOrEmpty(levelKM.lstSP.get(0).productCode) && levelKM.lstSP.get(0).quantity != null && levelKM.lstSP.get(0).quantity >= 0) {
									/**
									 * km sp A(10), B(10)
									 */
									GroupLevel groupLevelKM = new GroupLevel();
									groupLevelKM.setOrder(levelKM.order);
									groupLevelKM.setProductGroup(productGroupKM);
									groupLevelKM.setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
									groupLevelKM = setValueGroupLevel(groupLevelKM, typeGLM, levelKM.lstSP.get(0), false, levelKM.lstSP.get(0).quantity, levelKM.lstSP.get(0).amount, currentDate, logInfo.getStaffCode());
									if (PromotionType.ZV09.getValue().equals(__promotionProgram.getType())) {
										String groupLevelKMText = "(";
										List<GroupLevelDetail> listGroupLevelKMDetail = new ArrayList<GroupLevelDetail>();
										for (int k = 0; k < levelKM.lstSP.size(); k++) {
											NodeSP nodeSP = (NodeSP) levelKM.lstSP.get(k);
											if ("(".equals(groupLevelKMText)) {
												groupLevelKMText += nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
											} else {
												groupLevelKMText += ", " + nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
											}
											GroupLevelDetail groupLevelDetail = new GroupLevelDetail();
											Product product = productDAO.getProductByCode(nodeSP.productCode);
											groupLevelDetail.setProduct(product);
											groupLevelDetail.setUom(product.getUom1());
											groupLevelDetail = setValueTypeGLD(groupLevelDetail, typeGLM, nodeSP, false, currentDate, logInfo.getStaffCode());
											listGroupLevelKMDetail.add(groupLevelDetail);
										}
										groupLevelKMText += ")";
										groupLevelKM.setGroupText(groupLevelKMText);
										//										groupLevelKM.setMaxQuantity(levelKM.lstSP.get(0).quantity);
										groupLevelKM = promotionProgramDAO.createGroupLevel(groupLevelKM);
										//create group level detail
										for (int k = 0; k < listGroupLevelKMDetail.size(); k++) {
											listGroupLevelKMDetail.get(k).setGroupLevel(groupLevelKM);
											listGroupLevelKMDetail.get(k).setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
											promotionProgramDAO.createGroupLevelDetail(listGroupLevelKMDetail.get(k));
										}
									} else {
										String groupLevelKMText = "(";
										List<GroupLevelDetail> listGroupLevelKMDetail = new ArrayList<GroupLevelDetail>();
										for (int k = 0; k < levelKM.lstSP.size(); k++) {
											NodeSP nodeSP = (NodeSP) levelKM.lstSP.get(k);
											if ("(".equals(groupLevelKMText)) {
												groupLevelKMText += nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
											} else {
												groupLevelKMText += ", " + nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
											}
											GroupLevelDetail groupLevelDetail = new GroupLevelDetail();
											Product product = productDAO.getProductByCode(nodeSP.productCode);
											groupLevelDetail.setProduct(product);
											groupLevelDetail.setUom(product.getUom1());
											groupLevelDetail = setValueTypeGLD(groupLevelDetail, typeGLM, nodeSP, false, currentDate, logInfo.getStaffCode());
											listGroupLevelKMDetail.add(groupLevelDetail);
										}
										groupLevelKMText += ")";
										groupLevelKM.setGroupText(groupLevelKMText);
										groupLevelKM = promotionProgramDAO.createGroupLevel(groupLevelKM);
										//create group level detail
										for (int k = 0; k < listGroupLevelKMDetail.size(); k++) {
											listGroupLevelKMDetail.get(k).setGroupLevel(groupLevelKM);
											listGroupLevelKMDetail.get(k).setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
											promotionProgramDAO.createGroupLevelDetail(listGroupLevelKMDetail.get(k));
										}
									}

									// map groupLevelMua and groupLevelKM
									GroupMapping groupMapping = setGroupMapping(productGroupMua, groupLevelMua, productGroupKM, groupLevelKM, currentDate, logInfo.getStaffCode());
									groupMapping = promotionProgramDAO.createGroupMapping(groupMapping);
								} else if (levelKM.lstSP.get(0) instanceof NodeAmount && levelKM.lstSP.get(0).amount != null && levelKM.lstSP.get(0).amount.compareTo(BigDecimal.ZERO) >= 0) {
									/**
									 * levelKM chi chua 1 lstSP la NodeAmount =>
									 * kiem tra nhom sp co amount do chua? chua
									 * co => update lai productGroupKM
									 */
									//									GroupLevel groupLevelKM = promotionProgramDAO.getGroupLevelByMaxAmount(productGroupKM.getId(), levelKM.lstSP.get(0).amount);
									//									if(groupLevelKM == null) {
									GroupLevel groupLevelKM = new GroupLevel();
									groupLevelKM.setOrder(levelKM.order);
									groupLevelKM.setProductGroup(productGroupKM);
									groupLevelKM.setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
									am = levelKM.lstSP.get(0).amount;
									am = am.setScale(0, RoundingMode.UP);
									groupLevelKM.setGroupText("" + am);
									groupLevelKM = setValueGroupLevel(groupLevelKM, typeGLM, levelKM.lstSP.get(0), false, levelKM.lstSP.get(0).quantity, levelKM.lstSP.get(0).amount, currentDate, logInfo.getStaffCode());
									groupLevelKM = promotionProgramDAO.createGroupLevel(groupLevelKM);
									//									}

									//map groupLevelMua and productGroupKM
									GroupMapping groupMapping = setGroupMapping(productGroupMua, groupLevelMua, productGroupKM, groupLevelKM, currentDate, logInfo.getStaffCode());
									groupMapping = promotionProgramDAO.createGroupMapping(groupMapping);
								} else if (levelKM.lstSP.get(0) instanceof NodePercent && levelKM.lstSP.get(0).percent != null && levelKM.lstSP.get(0).percent >= 0) {
									/**
									 * levelKM la danh sach chi chua 1 lstSP la
									 * NodePercent => kiem tra nhom sp co
									 * percent do chua? chua co => update lia
									 * productGroupKM
									 */
									//									GroupLevel groupLevelKM = promotionProgramDAO.getGroupLevelByPercent(productGroupKM.getId(), levelKM.lstSP.get(0).percent);
									//									if(groupLevelKM == null) {
									GroupLevel groupLevelKM = new GroupLevel();
									groupLevelKM.setOrder(levelKM.order);
									groupLevelKM.setProductGroup(productGroupKM);
									groupLevelKM.setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
									groupLevelKM.setPercent(levelKM.lstSP.get(0).percent);
									groupLevelKM.setGroupText(levelKM.lstSP.get(0).percent + "%");
									groupLevelKM = setValueGroupLevel(groupLevelKM, typeGLM, levelKM.lstSP.get(0), false, levelKM.lstSP.get(0).quantity, levelKM.lstSP.get(0).amount, currentDate, logInfo.getStaffCode());
									groupLevelKM = promotionProgramDAO.createGroupLevel(groupLevelKM);
									//									}

									GroupMapping groupMapping = setGroupMapping(productGroupMua, groupLevelMua, productGroupKM, groupLevelKM, currentDate, logInfo.getStaffCode());
									groupMapping = promotionProgramDAO.createGroupMapping(groupMapping);
								}
							}
						}
					} else if (groupMua.lstLevel.get(0).lstSP.get(0) instanceof NodeSP && !StringUtility.isNullOrEmpty(groupMua.lstLevel.get(0).lstSP.get(0).productCode) && groupMua.lstLevel.get(0).lstSP.get(0).amount != null
							&& groupMua.lstLevel.get(0).lstSP.get(0).amount.compareTo(BigDecimal.ZERO) > 0) {
						/**
						 * mua A(10.000), B(20.000),...
						 */
						ProductGroup productGroupMua = null;
						ProductGroup __productGroupMua = promotionProgramDAO.getProductGroupByCode(groupMua.groupCode, ProductGroupType.MUA, __promotionProgram.getId());
						if (__productGroupMua == null) {
							productGroupMua = setProductGroup(typeGLM, true, groupMua.groupCode, groupMua.order, __promotionProgram, currentDate, logInfo.getStaffCode());
							productGroupMua = promotionProgramDAO.createProductGroup(productGroupMua);
						} else {
							productGroupMua = __productGroupMua;
						}
						for (int j = 0; j < groupMua.lstLevel.size(); j++) {
							GroupSP levelMua = groupMua.lstLevel.get(j);//create level
							GroupLevel groupLevelMua = new GroupLevel();
							groupLevelMua.setOrder(levelMua.order);
							groupLevelMua.setProductGroup(productGroupMua);
							groupLevelMua.setPromotionProgramId(productGroupMua.getPromotionProgram().getId());
							groupLevelMua = setValueGroupLevel(groupLevelMua, typeGLM, levelMua.lstSP.get(0), true, levelMua.lstSP.get(0).quantity, levelMua.lstSP.get(0).amount, currentDate, logInfo.getStaffCode());
							String groupLevelMuaText = null;
							List<GroupLevelDetail> listGroupLevelMuaDetail = new ArrayList<GroupLevelDetail>();
							if (PromotionType.ZV10.getValue().equals(typeGLM) || PromotionType.ZV11.getValue().equals(typeGLM) || PromotionType.ZV12.getValue().equals(typeGLM)) {
								NodeSP nodeSP = (NodeSP) levelMua.lstSP.get(0);
								groupLevelMuaText = StringUtility.convertMoney(nodeSP.amount) + "(";
								for (int k = 0; k < levelMua.lstSP.size(); k++) {
									nodeSP = (NodeSP) levelMua.lstSP.get(k);
									if (k == 0) {
										groupLevelMuaText += nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*" : "");
									} else {
										groupLevelMuaText += ", " + nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*" : "");
									}
									GroupLevelDetail groupLevelDetail = new GroupLevelDetail();
									Product product = productDAO.getProductByCode(nodeSP.productCode);
									groupLevelDetail.setProduct(product);
									groupLevelDetail.setUom(product.getUom1());
									groupLevelDetail = setValueTypeGLD(groupLevelDetail, typeGLM, nodeSP, true, currentDate, logInfo.getStaffCode());
									listGroupLevelMuaDetail.add(groupLevelDetail);
								}
							} else {
								groupLevelMuaText = "(";
								for (int k = 0; k < levelMua.lstSP.size(); k++) {
									NodeSP nodeSP = (NodeSP) levelMua.lstSP.get(k);
									if ("(".equals(groupLevelMuaText)) {
										groupLevelMuaText += nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (StringUtility.convertMoney(nodeSP.amount)) + ")";
									} else {
										groupLevelMuaText += ", " + nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (StringUtility.convertMoney(nodeSP.amount)) + ")";
									}
									GroupLevelDetail groupLevelDetail = new GroupLevelDetail();
									Product product = productDAO.getProductByCode(nodeSP.productCode);
									groupLevelDetail.setProduct(product);
									groupLevelDetail.setUom(product.getUom1());
									groupLevelDetail = setValueTypeGLD(groupLevelDetail, typeGLM, nodeSP, true, currentDate, logInfo.getStaffCode());
									listGroupLevelMuaDetail.add(groupLevelDetail);
								}
							}
							groupLevelMuaText += ")";
							groupLevelMua.setGroupText(groupLevelMuaText);
							groupLevelMua = promotionProgramDAO.createGroupLevel(groupLevelMua);
							//create group level detail
							for (GroupLevelDetail grDetail : listGroupLevelMuaDetail) {
//								if (PromotionType.ZV04.getValue().equals(typeGLM) || PromotionType.ZV05.getValue().equals(typeGLM) || PromotionType.ZV06.getValue().equals(typeGLM)) {
								if (PromotionType.ZV05.getValue().equals(typeGLM) || PromotionType.ZV06.getValue().equals(typeGLM)) {
									grDetail.setValue(groupLevelMua.getMinAmount());
								}
								grDetail.setGroupLevel(groupLevelMua);
								grDetail.setPromotionProgramId(productGroupMua.getPromotionProgram().getId());
								promotionProgramDAO.createGroupLevelDetail(grDetail);
							}

							List<Long> listIndexLevelKM = mapMuaKM.get(levelMua.index);
							for (int ii = 0; ii < listIndexLevelKM.size(); ii++) {
								long indexLevelKM = listIndexLevelKM.get(ii);
								GroupKM groupKM = lstGroupKM.get(lstGroupKM.indexOf(indexLevelKM));//find level and create level
								ProductGroup productGroupKM = null;
								ProductGroup __productGroupKM = promotionProgramDAO.getProductGroupByCode(groupKM.groupCode, ProductGroupType.KM, __promotionProgram.getId());
								if (__productGroupKM == null) {
									productGroupKM = setProductGroup(typeGLM, false, groupKM.groupCode, groupKM.order, __promotionProgram, currentDate, logInfo.getStaffCode());
									productGroupKM = promotionProgramDAO.createProductGroup(productGroupKM);
								} else {
									productGroupKM = __productGroupKM;
								}
								GroupSP levelKM = null;
								for (GroupSP __levelKM : groupKM.lstLevel) {
									if (__levelKM.index == indexLevelKM) {
										levelKM = __levelKM;
										break;
									}
								}
								if (levelKM.lstSP.get(0) instanceof NodeSP && !StringUtility.isNullOrEmpty(levelKM.lstSP.get(0).productCode) && levelKM.lstSP.get(0).quantity != null && levelKM.lstSP.get(0).quantity >= 0) {
									/**
									 * km sp A(10), B(10)...
									 */
									GroupLevel groupLevelKM = new GroupLevel();
									groupLevelKM.setOrder(levelKM.order);
									groupLevelKM.setProductGroup(productGroupKM);
									groupLevelKM.setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
									groupLevelKM = setValueGroupLevel(groupLevelKM, typeGLM, levelKM.lstSP.get(0), false, levelKM.lstSP.get(0).quantity, levelKM.lstSP.get(0).amount, currentDate, logInfo.getStaffCode());
									if (PromotionType.ZV09.equals(__promotionProgram.getType())) {
										String groupLevelKMText = "(";
										List<GroupLevelDetail> listGroupLevelKMDetail = new ArrayList<GroupLevelDetail>();
										for (int k = 0; k < levelKM.lstSP.size(); k++) {
											NodeSP nodeSP = (NodeSP) levelKM.lstSP.get(k);
											if ("(".equals(groupLevelKMText)) {
												groupLevelKMText += nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
											} else {
												groupLevelKMText += ", " + nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
											}
											GroupLevelDetail groupLevelDetail = new GroupLevelDetail();
											Product product = productDAO.getProductByCode(nodeSP.productCode);
											groupLevelDetail.setProduct(product);
											groupLevelDetail.setUom(product.getUom1());
											groupLevelDetail = setValueTypeGLD(groupLevelDetail, typeGLM, nodeSP, false, currentDate, logInfo.getStaffCode());
											listGroupLevelKMDetail.add(groupLevelDetail);
										}
										groupLevelKMText += ")";
										groupLevelKM.setGroupText(groupLevelKMText);
										groupLevelKM = promotionProgramDAO.createGroupLevel(groupLevelKM);
										//create group level detail
										for (int k = 0; k < listGroupLevelKMDetail.size(); k++) {
											listGroupLevelKMDetail.get(k).setGroupLevel(groupLevelKM);
											listGroupLevelKMDetail.get(k).setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
											promotionProgramDAO.createGroupLevelDetail(listGroupLevelKMDetail.get(k));
										}
									} else {
										String groupLevelKMText = "(";
										List<GroupLevelDetail> listGroupLevelKMDetail = new ArrayList<GroupLevelDetail>();
										for (int k = 0; k < levelKM.lstSP.size(); k++) {
											NodeSP nodeSP = (NodeSP) levelKM.lstSP.get(k);
											if ("(".equals(groupLevelKMText)) {
												groupLevelKMText += nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
											} else {
												groupLevelKMText += ", " + nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
											}
											GroupLevelDetail groupLevelDetail = new GroupLevelDetail();
											Product product = productDAO.getProductByCode(nodeSP.productCode);
											groupLevelDetail.setProduct(product);
											groupLevelDetail.setUom(product.getUom1());
											groupLevelDetail = setValueTypeGLD(groupLevelDetail, typeGLM, nodeSP, false, currentDate, logInfo.getStaffCode());
											listGroupLevelKMDetail.add(groupLevelDetail);
										}
										groupLevelKMText += ")";
										groupLevelKM.setGroupText(groupLevelKMText);
										groupLevelKM = promotionProgramDAO.createGroupLevel(groupLevelKM);
										//create group level detail
										for (int k = 0; k < listGroupLevelKMDetail.size(); k++) {
											listGroupLevelKMDetail.get(k).setGroupLevel(groupLevelKM);
											listGroupLevelKMDetail.get(k).setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
											promotionProgramDAO.createGroupLevelDetail(listGroupLevelKMDetail.get(k));
										}
									}

									// map groupLevelMua and groupLevelKM
									GroupMapping groupMapping = setGroupMapping(productGroupMua, groupLevelMua, productGroupKM, groupLevelKM, currentDate, logInfo.getStaffCode());
									groupMapping = promotionProgramDAO.createGroupMapping(groupMapping);
								} else if (levelKM.lstSP.get(0) instanceof NodeAmount && levelKM.lstSP.get(0).amount != null && levelKM.lstSP.get(0).amount.compareTo(BigDecimal.ZERO) >= 0) {
									/**
									 * levelKM chi chua 1 lstSP la NodeAmount =>
									 * kiem tra nhom sp co amount do chua? chua
									 * co => update lai productGroupKM
									 */
									//									GroupLevel groupLevelKM = promotionProgramDAO.getGroupLevelByMaxAmount(productGroupKM.getId(), levelKM.lstSP.get(0).amount);
									//									if(groupLevelKM == null) {
									GroupLevel groupLevelKM = new GroupLevel();
									groupLevelKM.setOrder(levelKM.order);
									groupLevelKM.setProductGroup(productGroupKM);
									groupLevelKM.setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
									am = levelKM.lstSP.get(0).amount;
									am = am.setScale(0, RoundingMode.UP);
									groupLevelKM.setGroupText("" + am);
									groupLevelKM = setValueGroupLevel(groupLevelKM, typeGLM, levelKM.lstSP.get(0), false, levelKM.lstSP.get(0).quantity, levelKM.lstSP.get(0).amount, currentDate, logInfo.getStaffCode());
									groupLevelKM = promotionProgramDAO.createGroupLevel(groupLevelKM);
									//									}

									//map groupLevelMua and productGroupKM
									GroupMapping groupMapping = setGroupMapping(productGroupMua, groupLevelMua, productGroupKM, groupLevelKM, currentDate, logInfo.getStaffCode());
									groupMapping = promotionProgramDAO.createGroupMapping(groupMapping);
								} else if (levelKM.lstSP.get(0) instanceof NodePercent && levelKM.lstSP.get(0).percent != null && levelKM.lstSP.get(0).percent >= 0) {
									/**
									 * levelKM la danh sach chi chua 1 lstSP la
									 * NodePercent => kiem tra nhom sp co
									 * percent do chua? chua co => update lia
									 * productGroupKM
									 */
									//									GroupLevel groupLevelKM = promotionProgramDAO.getGroupLevelByPercent(productGroupKM.getId(), levelKM.lstSP.get(0).percent);
									//									if(groupLevelKM == null) {
									GroupLevel groupLevelKM = new GroupLevel();
									groupLevelKM.setOrder(levelKM.order);
									groupLevelKM.setProductGroup(productGroupKM);
									groupLevelKM.setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
									groupLevelKM.setPercent(levelKM.lstSP.get(0).percent);
									groupLevelKM.setGroupText(levelKM.lstSP.get(0).percent + "%");
									groupLevelKM = setValueGroupLevel(groupLevelKM, typeGLM, levelKM.lstSP.get(0), false, levelKM.lstSP.get(0).quantity, levelKM.lstSP.get(0).amount, currentDate, logInfo.getStaffCode());
									groupLevelKM = promotionProgramDAO.createGroupLevel(groupLevelKM);
									//									}

									GroupMapping groupMapping = setGroupMapping(productGroupMua, groupLevelMua, productGroupKM, groupLevelKM, currentDate, logInfo.getStaffCode());
									groupMapping = promotionProgramDAO.createGroupMapping(groupMapping);
								}
							}
						}
					} else if (groupMua.lstLevel.get(0).lstSP.get(0) instanceof NodeAmount) {
						/**
						 * mua don hang voi tong tien 100.000
						 */
						ProductGroup productGroupMua = null;
						ProductGroup __productGroupMua = promotionProgramDAO.getProductGroupByCode(groupMua.groupCode, ProductGroupType.MUA, __promotionProgram.getId());
						if (__productGroupMua == null) {
							productGroupMua = setProductGroup(typeGLM, true, groupMua.groupCode, groupMua.order, __promotionProgram, currentDate, logInfo.getStaffCode());
							productGroupMua = promotionProgramDAO.createProductGroup(productGroupMua);
						} else {
							productGroupMua = __productGroupMua;
						}

						for (int j = 0; j < groupMua.lstLevel.size(); j++) {
							GroupSP levelMua = groupMua.lstLevel.get(j);//create level
							GroupLevel groupLevelMua = new GroupLevel();
							groupLevelMua.setOrder(levelMua.order);
							groupLevelMua.setProductGroup(productGroupMua);
							groupLevelMua.setPromotionProgramId(productGroupMua.getPromotionProgram().getId());
							am = levelMua.lstSP.get(0).amount;
							if (am != null) {
								am = am.setScale(0, RoundingMode.UP);
								groupLevelMua.setGroupText(am.toString());
							}
							groupLevelMua = setValueGroupLevel(groupLevelMua, typeGLM, levelMua.lstSP.get(0), true, levelMua.lstSP.get(0).quantity, levelMua.lstSP.get(0).amount, currentDate, logInfo.getStaffCode());
							groupLevelMua = promotionProgramDAO.createGroupLevel(groupLevelMua);
							List<Long> listIndexLevelKM = mapMuaKM.get(levelMua.index);
							for (int ii = 0; ii < listIndexLevelKM.size(); ii++) {
								long indexLevelKM = listIndexLevelKM.get(ii);
								GroupKM groupKM = lstGroupKM.get(lstGroupKM.indexOf(indexLevelKM));//find level and create level
								ProductGroup productGroupKM = null;
								ProductGroup __productGroupKM = promotionProgramDAO.getProductGroupByCode(groupKM.groupCode, ProductGroupType.KM, __promotionProgram.getId());
								if (__productGroupKM == null) {
									productGroupKM = setProductGroup(typeGLM, false, groupKM.groupCode, groupKM.order, __promotionProgram, currentDate, logInfo.getStaffCode());
									productGroupKM = promotionProgramDAO.createProductGroup(productGroupKM);
								} else {
									productGroupKM = __productGroupKM;
								}
								GroupSP levelKM = null;
								for (GroupSP __levelKM : groupKM.lstLevel) {
									if (__levelKM.index == indexLevelKM) {
										levelKM = __levelKM;
										break;
									}
								}
								if (levelKM.lstSP.get(0) instanceof NodeSP && !StringUtility.isNullOrEmpty(levelKM.lstSP.get(0).productCode) && levelKM.lstSP.get(0).quantity != null && levelKM.lstSP.get(0).quantity >= 0) {
									/**
									 * km sp A(10), B(10)
									 */
									GroupLevel groupLevelKM = new GroupLevel();
									groupLevelKM.setOrder(levelKM.order);
									groupLevelKM.setProductGroup(productGroupKM);
									groupLevelKM.setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
									groupLevelKM = setValueGroupLevel(groupLevelKM, typeGLM, levelKM.lstSP.get(0), false, levelKM.lstSP.get(0).quantity, levelKM.lstSP.get(0).amount, currentDate, logInfo.getStaffCode());
									if (PromotionType.ZV09.getValue().equals(__promotionProgram.getType())) {
										String groupLevelKMText = "(";
										List<GroupLevelDetail> listGroupLevelKMDetail = new ArrayList<GroupLevelDetail>();
										for (int k = 0; k < levelKM.lstSP.size(); k++) {
											NodeSP nodeSP = (NodeSP) levelKM.lstSP.get(k);
											if ("(".equals(groupLevelKMText)) {
												groupLevelKMText += nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
											} else {
												groupLevelKMText += ", " + nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
											}
											GroupLevelDetail groupLevelDetail = new GroupLevelDetail();
											Product product = productDAO.getProductByCode(nodeSP.productCode);
											groupLevelDetail.setProduct(product);
											groupLevelDetail.setUom(product.getUom1());
											groupLevelDetail = setValueTypeGLD(groupLevelDetail, typeGLM, nodeSP, false, currentDate, logInfo.getStaffCode());
											listGroupLevelKMDetail.add(groupLevelDetail);
										}
										groupLevelKMText += ")";
										groupLevelKM.setGroupText(groupLevelKMText);
										groupLevelKM = promotionProgramDAO.createGroupLevel(groupLevelKM);
										//create group level detail
										for (int k = 0; k < listGroupLevelKMDetail.size(); k++) {
											listGroupLevelKMDetail.get(k).setGroupLevel(groupLevelKM);
											listGroupLevelKMDetail.get(k).setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
											promotionProgramDAO.createGroupLevelDetail(listGroupLevelKMDetail.get(k));
										}
									} else {
										String groupLevelKMText = "(";
										List<GroupLevelDetail> listGroupLevelKMDetail = new ArrayList<GroupLevelDetail>();
										for (int k = 0; k < levelKM.lstSP.size(); k++) {
											NodeSP nodeSP = (NodeSP) levelKM.lstSP.get(k);
											if ("(".equals(groupLevelKMText)) {
												groupLevelKMText += nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
											} else {
												groupLevelKMText += ", " + nodeSP.productCode + (nodeSP.isRequired != null && nodeSP.isRequired ? "*(" : "(") + (nodeSP.quantity.toString()) + ")";
											}
											GroupLevelDetail groupLevelDetail = new GroupLevelDetail();
											Product product = productDAO.getProductByCode(nodeSP.productCode);
											groupLevelDetail.setProduct(product);
											groupLevelDetail.setUom(product.getUom1());
											groupLevelDetail = setValueTypeGLD(groupLevelDetail, typeGLM, nodeSP, false, currentDate, logInfo.getStaffCode());
											listGroupLevelKMDetail.add(groupLevelDetail);
										}
										groupLevelKMText += ")";
										groupLevelKM.setGroupText(groupLevelKMText);
										groupLevelKM = promotionProgramDAO.createGroupLevel(groupLevelKM);
										//create group level detail
										for (int k = 0; k < listGroupLevelKMDetail.size(); k++) {
											listGroupLevelKMDetail.get(k).setGroupLevel(groupLevelKM);
											listGroupLevelKMDetail.get(k).setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
											promotionProgramDAO.createGroupLevelDetail(listGroupLevelKMDetail.get(k));
										}
									}

									// map groupLevelMua and groupLevelKM
									GroupMapping groupMapping = setGroupMapping(productGroupMua, groupLevelMua, productGroupKM, groupLevelKM, currentDate, logInfo.getStaffCode());
									groupMapping = promotionProgramDAO.createGroupMapping(groupMapping);
								} else if (levelKM.lstSP.get(0) instanceof NodeAmount && levelKM.lstSP.get(0).amount != null && levelKM.lstSP.get(0).amount.compareTo(BigDecimal.ZERO) >= 0) {
									/**
									 * levelKM chi chua 1 lstSP la NodeAmount =>
									 * kiem tra nhom sp co amount do chua? chua
									 * co => update lai productGroupKM
									 */
									//									GroupLevel groupLevelKM = promotionProgramDAO.getGroupLevelByMaxAmount(productGroupKM.getId(), levelKM.lstSP.get(0).amount);
									//									if(groupLevelKM == null) {
									GroupLevel groupLevelKM = new GroupLevel();
									groupLevelKM.setOrder(levelKM.order);
									groupLevelKM.setProductGroup(productGroupKM);
									groupLevelKM.setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
									am = levelKM.lstSP.get(0).amount;
									am = am.setScale(0, RoundingMode.UP);
									groupLevelKM.setGroupText("" + am);
									groupLevelKM = setValueGroupLevel(groupLevelKM, typeGLM, levelKM.lstSP.get(0), false, levelKM.lstSP.get(0).quantity, levelKM.lstSP.get(0).amount, currentDate, logInfo.getStaffCode());
									groupLevelKM = promotionProgramDAO.createGroupLevel(groupLevelKM);
									//									}

									//map groupLevelMua and productGroupKM
									GroupMapping groupMapping = setGroupMapping(productGroupMua, groupLevelMua, productGroupKM, groupLevelKM, currentDate, logInfo.getStaffCode());
									groupMapping = promotionProgramDAO.createGroupMapping(groupMapping);
								} else if (levelKM.lstSP.get(0) instanceof NodePercent && levelKM.lstSP.get(0).percent != null && levelKM.lstSP.get(0).percent >= 0) {
									/**
									 * levelKM la danh sach chi chua 1 lstSP la
									 * NodePercent => kiem tra nhom sp co
									 * percent do chua? chua co => update lia
									 * productGroupKM
									 */
									//									GroupLevel groupLevelKM = promotionProgramDAO.getGroupLevelByPercent(productGroupKM.getId(), levelKM.lstSP.get(0).percent);
									//									if(groupLevelKM == null) {
									GroupLevel groupLevelKM = new GroupLevel();
									groupLevelKM.setOrder(levelKM.order);
									groupLevelKM.setProductGroup(productGroupKM);
									groupLevelKM.setPromotionProgramId(productGroupKM.getPromotionProgram().getId());
									groupLevelKM.setPercent(levelKM.lstSP.get(0).percent);
									groupLevelKM.setGroupText(levelKM.lstSP.get(0).percent + "%");
									groupLevelKM = setValueGroupLevel(groupLevelKM, typeGLM, levelKM.lstSP.get(0), false, levelKM.lstSP.get(0).quantity, levelKM.lstSP.get(0).amount, currentDate, logInfo.getStaffCode());
									groupLevelKM = promotionProgramDAO.createGroupLevel(groupLevelKM);
									//									}

									GroupMapping groupMapping = setGroupMapping(productGroupMua, groupLevelMua, productGroupKM, groupLevelKM, currentDate, logInfo.getStaffCode());
									groupMapping = promotionProgramDAO.createGroupMapping(groupMapping);
								}
							}
						}
					}
				}
			}

			// import promotion_shop_map
			//			importPromotionShopMap(mapPP, lstPromotionShop, logInfo);
			//			
			//			importPromotionCustomerType(mapPP, promotionCustomerTypes, currentDate, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * import du lieu cho promotion_shop_map
	 * 
	 * @param mapPP - map luu CTKM
	 * @param lstPromotionShop - lst vo chua thong tin de import promotion_shop_map
	 * @param logInfo
	 * 
	 * @throws BusinessException
	 * 
	 * @author lacnv1
	 * @since Apr 13, 2015
	 */
	private void importPromotionShopMap(Map<String, PromotionProgram> mapPP, List<PromotionShopVO> lstPromotionShop, LogInfoVO logInfo)
			throws DataAccessException, BusinessException {
		if (mapPP == null || mapPP.size() == 0 || lstPromotionShop == null || lstPromotionShop.size() == 0) {
			return;
		}
		PromotionProgram pp = null;
		PromotionShopVO vo = null;
		PromotionShopMap promotionShopMap = null;
		Shop sh = null;
		Date sysdate = commonDAO.getSysDate();
		String userName = logInfo != null ? logInfo.getStaffCode() : "";
		for (int i = 0, sz = lstPromotionShop.size(); i < sz; i++) {
			vo = lstPromotionShop.get(i);
			pp = mapPP.get(vo.getPromotionCode());
			sh = shopDAO.getShopByCode(vo.getShopCode());
			if (pp != null && sh != null) {
				promotionShopMap = promotionShopMapDAO.getPromotionShopMap(sh.getId(), pp.getId());
				if (promotionShopMap != null) {
					promotionShopMap.setFromDate(pp.getFromDate());
					promotionShopMap.setToDate(pp.getToDate());
					promotionShopMap.setUpdateDate(sysdate);
					promotionShopMap.setCreateUser(userName);
					if (vo.getQuantity() != null) {
						promotionShopMap.setQuantityMax(vo.getQuantity().intValue());
					}
					if (!ActiveType.RUNNING.equals(promotionShopMap.getStatus())) {
						promotionShopMap.setStatus(ActiveType.RUNNING);
						promotionShopMap.setQuantityReceived(0);
					}
					promotionShopMapDAO.updatePromotionShopMap(promotionShopMap, logInfo);
				} else if (ActiveType.WAITING.equals(pp.getStatus())) {
					promotionShopMap = new PromotionShopMap();
					promotionShopMap.setShop(sh);
					promotionShopMap.setPromotionProgram(pp);
					promotionShopMap.setFromDate(pp.getFromDate());
					promotionShopMap.setToDate(pp.getToDate());
					promotionShopMap.setCreateDate(sysdate);
					promotionShopMap.setCreateUser(userName);
					if (vo.getQuantity() != null) {
						promotionShopMap.setQuantityMax(vo.getQuantity().intValue());
					}
					promotionShopMap.setQuantityReceived(0);
					promotionShopMap.setStatus(ActiveType.RUNNING);
					this.createPromotionShopMap2(promotionShopMap, logInfo);
				}
			}
		}
	}
	
	/**
	 * Tao promotion_shop_map, xoa record cua shop cha con neu co
	 * 
	 * @param promotionShopMap - promotion_shop_map can tao
	 * @param logInfo
	 * 
	 * @author lacnv1
	 * @since Apr 13, 2015
	 */
	private PromotionShopMap createPromotionShopMap2(PromotionShopMap promotionShopMap, LogInfoVO logInfo)
			throws BusinessException {
		try {
			PromotionProgram pp = promotionShopMap.getPromotionProgram();
			promotionShopMap.setFromDate(pp.getFromDate());
			promotionShopMap.setToDate(pp.getToDate());
			PromotionShopMap psm = promotionShopMapDAO.getPromotionParentShopMap(pp.getId(), promotionShopMap.getShop().getId(), ActiveType.RUNNING.getValue());
			if (psm != null) {
				psm.setStatus(ActiveType.DELETED);
				promotionShopMapDAO.updatePromotionShopMap(psm, logInfo);
			}
			List<PromotionShopMap> lst = promotionShopMapDAO.getPromotionChildrenShopMap(pp.getId(), promotionShopMap.getShop().getId(), ActiveType.RUNNING.getValue());
			if (lst != null) {
				for (PromotionShopMap psm1 : lst) {
					psm1.setStatus(ActiveType.DELETED);
					promotionShopMapDAO.updatePromotionShopMap(psm1, logInfo);
				}
			}
			return promotionShopMapDAO.createPromotionShopMap(promotionShopMap, logInfo);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	

	@Override
	public PromotionCustAttrDetail getPromotionCustAttrDetail(Long promotionCustAttrId, Long objectType, Long objectId)
			throws BusinessException {
		try{
			return promotionCustAttrDetailDAO.getPromotionCustAttrDetailbyObjectType_and_ObjectId(promotionCustAttrId, objectType, objectId);
		}catch (DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<PromotionProgram> getListPromotionProgram(PromotionProgramFilter filter) throws BusinessException {
		try {
			List<PromotionProgram> lst = promotionProgramDAO.getListPromotionProgram(filter);
			ObjectVO<PromotionProgram> vo = new ObjectVO<PromotionProgram>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<PromotionProgram> getListDefaultPromotionProgram(PromotionProgramFilter filter) throws BusinessException {
		try {
			List<PromotionProgram> lst = promotionProgramDAO.getListDefaultPromotionProgram(filter);
			ObjectVO<PromotionProgram> vo = new ObjectVO<PromotionProgram>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional (rollbackFor = Exception.class)
	public PromotionShopMap createPromotionShopMap(PromotionShopMap promotionShopMap, LogInfoVO logInfo)
			throws BusinessException {
		try {
			PromotionProgram pp = promotionShopMap.getPromotionProgram();
			promotionShopMap.setFromDate(pp.getFromDate());
			promotionShopMap.setToDate(pp.getToDate());
			PromotionShopMap psm = promotionShopMapDAO.getPromotionParentShopMap(pp.getId(),
					promotionShopMap.getShop().getId(), ActiveType.RUNNING.getValue());
			if (psm != null) {
				List<PromotionStaffMap> lstPstm = promotionStaffMapDAO.getListPromotionStaffMapByShopMap(psm.getId(), ActiveType.RUNNING.getValue());
				if (lstPstm != null && lstPstm.size() > 0) {
					for (PromotionStaffMap pstm : lstPstm) {
						pstm.setStatus(ActiveType.DELETED);
						promotionStaffMapDAO.updatePromotionStaffMap(pstm, logInfo);
					}
				}
			}
			return promotionShopMapDAO.createOnePromotionShopMap(promotionShopMap, logInfo);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	@Transactional (rollbackFor = Exception.class)
	public Boolean createListPromotionShopMap(List<PromotionShopMap> lstPromotionShopMap, LogInfoVO logInfo)
			throws BusinessException {
		try {
			PromotionShopMap psm1 = null;
			for (PromotionShopMap psm: lstPromotionShopMap) {
				psm1 = promotionShopMapDAO.getPromotionParentShopMap(psm.getPromotionProgram().getId(),
						psm.getShop().getId(), ActiveType.RUNNING.getValue());
				if (psm1 != null) {
					List<PromotionStaffMap> lstPstm = promotionStaffMapDAO.getListPromotionStaffMapByShopMap(psm1.getId(), ActiveType.RUNNING.getValue());
					if (lstPstm != null && lstPstm.size() > 0) {
						for (PromotionStaffMap pstm : lstPstm) {
							pstm.setStatus(ActiveType.DELETED);
							promotionStaffMapDAO.updatePromotionStaffMap(pstm, logInfo);
						}
					}
				}
				promotionShopMapDAO.createOnePromotionShopMap(psm, logInfo);
			}
			return true;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updatePromotionShopMap(PromotionShopMap promotionShopMap,
			LogInfoVO logInfo) throws BusinessException {
		try {
			//TODO:
//			PromotionShopMap psm = this.getPromotionShopMapById(promotionShopMap.getId());
//			if (!psm.getObjectApply().equals(promotionShopMap.getObjectApply()))
//				promotionCustomerMapDAO.deleteRelevantCustomerMap(promotionShopMap.getId());
			
			promotionShopMapDAO.updatePromotionShopMap(promotionShopMap, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	private void deleteOnePromotionShopMap(long promotionId, long shopId, LogInfoVO logInfo) throws BusinessException {
		try {
			Shop sh = shopDAO.getShopById(shopId);
			if (sh == null) {
				throw new BusinessException("shopId is not exist");
			}
			PromotionProgram pp = promotionProgramDAO.getPromotionProgramById(promotionId);
			if (pp == null) {
				throw new BusinessException("promotionId is not exist");
			}
			
			List<PromotionCustomerMap> listPromotionCustomerMap = promotionCustomerMapDAO.getListPromotionCustomerMapWithShopAndPromotionProgram(shopId, promotionId,false);
			List<PromotionShopMap> listPromotionShopMap = promotionShopMapDAO.getListPromotionChildShopMapWithShopAndPromotionProgram(shopId, promotionId);
			
			for (PromotionCustomerMap pcm: listPromotionCustomerMap) {
				pcm.setStatus(ActiveType.DELETED);
				promotionCustomerMapDAO.updatePromotionCustomerMap(pcm, logInfo);
			}
			for (PromotionShopMap psm: listPromotionShopMap) {
				psm.setStatus(ActiveType.DELETED);
				promotionShopMapDAO.updatePromotionShopMap(psm, logInfo);
			}
			
			List<PromotionStaffMap> lstPstm = promotionStaffMapDAO.getListPromotionStaffMap(promotionId, shopId, ActiveType.RUNNING.getValue());
			if (lstPstm != null && lstPstm.size() > 0) {
				for (PromotionStaffMap pstm : lstPstm) {
					pstm.setStatus(ActiveType.DELETED);
					promotionStaffMapDAO.updatePromotionStaffMap(pstm, logInfo);
				}
			}
			
			//kiem tra cha no coi co con chau gi tham gia khong, neu khong thi them cha no vao
			
			if (sh.getParentShop() != null) {
				CmsFilter filter = new CmsFilter();
				filter.setShopId(sh.getParentShop().getId());
				filter.setShopRootId(logInfo.getShopRootId());
				if (cmsDAO.checkShopInCMSByFilter(filter) == 1) {
					listPromotionShopMap = promotionShopMapDAO.getListPromotionChildShopMapWithShopAndPromotionProgram(sh.getParentShop().getId(), promotionId);
					if (listPromotionShopMap.size() == 0) {
						PromotionShopMap psm = new PromotionShopMap();
						psm.setShop(sh.getParentShop());
						psm.setPromotionProgram(pp);
						psm.setFromDate(pp.getFromDate());
						psm.setToDate(pp.getToDate());
						psm.setStatus(ActiveType.RUNNING);
						psm.setCreateUser(logInfo.getStaffCode());
						psm.setCreateDate(commonDAO.getSysDate());
						promotionShopMapDAO.createOnePromotionShopMap(psm, logInfo);
					}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void deletePromotionShopMap(long promotionId, long shopId, LogInfoVO logInfo) throws BusinessException {
		this.deleteOnePromotionShopMap(promotionId, shopId, logInfo);
	}

	@Override
	public PromotionShopMap getPromotionShopMapById(Long id)
			throws BusinessException {
		try {
			return id == null ? null : promotionShopMapDAO
					.getPromotionShopMapById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public PromotionShopMap checkPromotionShopMap(Long promotionProgramId,
			String shopCode, ActiveType status,Date lockDay) throws BusinessException {
		try {
			return promotionShopMapDAO.checkPromotionShopMap(promotionProgramId, shopCode, status,lockDay);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkCTHMShopMap(String promotionProgramCode,
			Long shopId, ActiveType status,Date lockDay) throws BusinessException {
		try {
			return promotionShopMapDAO.checkCTHMShopMap(promotionProgramCode, shopId, status, lockDay);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<PromotionShopMap> getListPromotionShopMap(
			KPaging<PromotionShopMap> kPaging, Long promotionProgramId,
			String shopCode, String shopName, Integer shopQuantity,
			ActiveType status, Long parentShopId) throws BusinessException {
		try {
			List<PromotionShopMap> lst = promotionShopMapDAO
					.getListPromotionShopMap(kPaging, promotionProgramId,
							shopCode, shopName, shopQuantity, status, parentShopId);
			ObjectVO<PromotionShopMap> vo = new ObjectVO<PromotionShopMap>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public PromotionCustomerMap createPromotionCustomerMap(
			PromotionCustomerMap promotionCustomerMap, LogInfoVO logInfo)
			throws BusinessException {
		try {
			
			PromotionCustomerMap result = promotionCustomerMapDAO.createPromotionCustomerMap(promotionCustomerMap, logInfo);
			
//			PromotionShopMap promotionShopMap = null;
//			Integer qtShop = null;
//			Integer qtSumCustomer;
//			
//			if(promotionCustomerMap != null){
//				promotionShopMap = promotionCustomerMap.getPromotionShopMap();
//				if(promotionShopMap != null){
//					qtShop = promotionShopMap.getQuantityMax();
//					qtSumCustomer = promotionCustomerMapDAO.getSumQuantityMaxOfShopMap(promotionShopMap.getId(), null);
//					
//					if(qtShop == null){
//						promotionShopMap.setQuantityMax(qtSumCustomer);
//					}else{
//						if(qtShop < qtSumCustomer){
//							promotionShopMap.setQuantityMax(qtSumCustomer);
//						}
//					}
//					promotionShopMapDAO.updatePromotionShopMap(promotionShopMap, logInfo);
//				}
//			}	 
			
			
			return result;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updatePromotionCustomerMap(PromotionCustomerMap promotionCustomerMap, LogInfoVO logInfo) throws BusinessException {
		try {
			promotionCustomerMapDAO.updatePromotionCustomerMap(promotionCustomerMap, logInfo);
//			PromotionShopMap psm = promotionCustomerMap.getPromotionShopMap();
//			if (psm != null) {
//				Integer quantityMax = psm.getQuantityMax();
//				List<Long> lstExcept = new ArrayList<Long>();
//				lstExcept.add(promotionCustomerMap.getId());
//				Integer qt = promotionCustomerMapDAO.getSumQuantityMaxOfShopMap(psm.getId(), lstExcept);
//				if (qt == null) {
//					qt = promotionCustomerMap.getQuantityMax();
//				} else {
//					qt += promotionCustomerMap.getQuantityMax();
//				}
//				if (quantityMax == null || quantityMax < qt) {
//					psm.setQuantityMax(qt);
//				}
//				promotionShopMapDAO.updatePromotionShopMap(psm, logInfo);
//			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createListPromotionCustomerMap(PromotionShopMap psm, List<PromotionCustomerMap> lstPCM, LogInfoVO logInfo) throws BusinessException {
		try {
			if (lstPCM == null || lstPCM.size() == 0) {
				throw new IllegalArgumentException("invalid list customers");
			}
			promotionCustomerMapDAO.createPromotionCustomerMap(lstPCM, logInfo);
			
			if (psm != null) {
				Integer qtMax = psm.getQuantityMax();
				List<Long> lstExcept = new ArrayList<Long>();
				Integer qtNew = 0;
				for (PromotionCustomerMap pcm : lstPCM) {
					lstExcept.add(pcm.getId());
					if (pcm.getQuantityMax() != null) {
						qtNew += pcm.getQuantityMax();
					}
				}
				Integer qt = promotionCustomerMapDAO.getSumQuantityMaxOfShopMap(psm.getId(), lstExcept);
				if (qt == null) {
					qt = qtNew;
				} else {
					qt += qtNew;
				}
				if (qtMax == null || qtMax < qt) {
					psm.setQuantityMax(qt);
				}
				promotionShopMapDAO.updatePromotionShopMap(psm, logInfo);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createListPromotionCustomerMapEx(List<PromotionShopMap> promotionShopMaps, List<PromotionCustomerMap> lstPCM, LogInfoVO logInfo) throws BusinessException {
		try {
			if (lstPCM == null || lstPCM.size() == 0) {
				throw new IllegalArgumentException("invalid list customers");
			}
			promotionCustomerMapDAO.createPromotionCustomerMap(lstPCM, logInfo);
			
			for (int i = 0, n = promotionShopMaps.size(); i < n; i++) {
				PromotionShopMap psm = promotionShopMaps.get(i);
				Integer qtMax = psm.getQuantityMax();
				List<Long> lstExcept = new ArrayList<Long>();
				Integer qtNew = 0;
				for (PromotionCustomerMap pcm : lstPCM) {
					if (psm.getShop().getId().equals(pcm.getShop().getId())) {
						lstExcept.add(pcm.getId());
						if (pcm.getQuantityMax() != null) {
							qtNew += pcm.getQuantityMax();
						}
					}
				}
				Integer qt = promotionCustomerMapDAO.getSumQuantityMaxOfShopMap(psm.getId(), lstExcept);
				if (qt == null) {
					qt = qtNew;
				} else {
					qt += qtNew;
				}
				if (qtMax == null || qtMax < qt) {
					psm.setQuantityMax(qt);
				}
				promotionShopMapDAO.updatePromotionShopMap(psm, logInfo);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void deletePromotionCustomerMap(
			PromotionCustomerMap promotionCustomerMap, LogInfoVO logInfo)
			throws BusinessException {
		try {
			promotionCustomerMapDAO.deletePromotionCustomerMap(
					promotionCustomerMap, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public PromotionCustomerMap getPromotionCustomerMapById(Long id)
			throws BusinessException {
		try {
			return id == null ? null : promotionCustomerMapDAO
					.getPromotionCustomerMapById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public PromotionCustomerMap getPromotionCustomerMap(Long promotionShopMapId, Long customerId)
			throws BusinessException {
		try {
			return promotionCustomerMapDAO.getPromotionCustomerMap(promotionShopMapId, customerId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<PromotionCustomerMap> getListPromotionCustomerMap(
			KPaging<PromotionCustomerMap> kPaging, Long promotionProgramId,
			String shopCode, Long channelTypeId, String shortCode,
			Integer quantityMax, ActiveType status, String channelTypeName, 
			Integer quantityReceived, String customerName, 
			String channelTypeCode) throws BusinessException {
		try {
			List<PromotionCustomerMap> lst = promotionCustomerMapDAO
					.getListPromotionCustomerMap(kPaging, promotionProgramId,
							shopCode, channelTypeId, shortCode, quantityMax,
							status, channelTypeName, quantityReceived, customerName, channelTypeCode);
			ObjectVO<PromotionCustomerMap> vo = new ObjectVO<PromotionCustomerMap>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkIfPromotionCustomerMapExist(long promotionProgramId,
			String customerCode, Long id) throws BusinessException {
		try {
			return promotionCustomerMapDAO.checkIfRecordExist(
					promotionProgramId, customerCode, id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkIfPromotionCustomerMapExistEx(long promotionProgramId,
			String channelTypeCode, String shopCode, Long id) throws BusinessException {
		try {
			return promotionCustomerMapDAO.checkIfRecordExistEx(
					promotionProgramId, channelTypeCode, shopCode, id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	// -----------------------------------------------------------------
	
	@Override
	public Boolean checkIfPromotionShopMapExist(long promotionProgramId,
			String shopCode, Long id) throws BusinessException {
		try {
			return promotionShopMapDAO.checkIfRecordExist(promotionProgramId,
					shopCode, id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<PromotionCustAttrVO> getListPromotionCustAttrVOCanBeSet(
			KPaging<PromotionCustAttrVO> kPaging, long promotionProgramId) throws BusinessException{
		try {
			return promotionProgramDAO
					.getListPromotionCustAttrVOCanBeSet(kPaging, promotionProgramId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<PromotionCustAttrVO> getListPromotionCustAttrVOAlreadySet(
			KPaging<PromotionCustAttrVO> kPaging, long promotionProgramId) throws BusinessException{
		try {
			return promotionProgramDAO
					.getListPromotionCustAttrVOAlreadySet(kPaging, promotionProgramId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	

	@Override
	public List<RptPromotionDetailLv01VO> getDataForPromotionDetailReport(
			Long shopId, List<Long> staffIds, Date fromDate, Date toDate)
			throws BusinessException {
		try {
			List<RptPromotionDetailDataVO> listData = promotionProgramDAO
					.getDataForPromotionDetailReport(shopId, staffIds,
							fromDate, toDate);

			if (null != listData && listData.size() > 0) {
				List<RptPromotionDetailLv01VO> resutl = RptPromotionDetailDataVO
						.groupByPromotion(listData);

				return resutl;
			} else {
				return null;
			}

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkProductInOtherPromotionProgram(String productCode,
			Long promotionProgramId, Long promotionProgramDetailId) throws BusinessException {
		try {
			return promotionProgramDAO.checkProductInOtherPromotionProgram(productCode,
					promotionProgramId, promotionProgramDetailId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkProductInOtherPromotionProgramEx(String productCode,
			Date fromDate, Date toDate, Long promotionProgramId) throws BusinessException {
		try {
			return promotionProgramDAO.checkProductInOtherPromotionProgram(productCode,
					fromDate, toDate, promotionProgramId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkPromotionShopMapInCustomerMap(Long promotionShopMapId) throws BusinessException {
		try {
			return promotionCustomerMapDAO.checkPromotionShopMapInCustomerMap(promotionShopMapId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public PromotionShopMap getPromotionShopMap(Long shopId, Long promotionProgramId) throws BusinessException {
		try {
			return promotionShopMapDAO.getPromotionShopMap(shopId, promotionProgramId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public boolean checkExistPromotionShopMapByListShop(String strListShopId, Long promotionProgramId) throws BusinessException {
		try {
			return promotionShopMapDAO.checkExistPromotionShopMapByListShop(strListShopId, promotionProgramId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PromotionProgramMgr#getListPromotionShopMapWithListPromotionId(java.util.List)
	 */
	@Override
	public List<PromotionShopMap> getListPromotionShopMapWithListPromotionId(
			List<Long> listPromotionProgramId) throws BusinessException {
		
		try {
			return promotionShopMapDAO.getListPromotionShopMapWithListPromotionId(listPromotionProgramId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PromotionProgramMgr#checkExitsPromotionWithParentShop(java.lang.Long, java.lang.String)
	 */
	@Override
	public Boolean checkExitsPromotionWithParentShop(Long promotionProgramid,
			String shopCode) throws BusinessException {
		
		try {
			return promotionShopMapDAO.checkExitsPromotionWithParentShop(promotionProgramid, shopCode);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public PromotionCustomerMap checkPromotionCustomerMap(Long promotionProgramId,
			String shopCode, String shortCode, String channelTypeCode) throws BusinessException {
		try {
			return promotionCustomerMapDAO.checkPromotionCustomerMap(promotionProgramId, shopCode, shortCode, channelTypeCode);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ProductCatVO> getListProductCatVOEx(String productCode,
			String productName, Long exPpId) throws BusinessException {
		
		return null;
	}

	@Override
	public List<ProductInfo> getListSubCat(String productCode,
			String productName, Long exPpId, Long catId)
			throws BusinessException {
		
		return null;
	}

	@Override
	public List<Product> getListProductBySubCat(String productCode,
			String productName, Long exPpId, Long catId, Long subCatId)
			throws BusinessException {
		
		return null;
	}
	
	@Override
	public ObjectVO<ZV03View> getZV03ViewHeader(KPaging<ZV03View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV03View> lst = promotionProgramDAO.getZV03ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV03View> vo = new ObjectVO<ZV03View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV03View> getZV03ViewDetail(KPaging<ZV03View> paging, Long promotionProgramId, Long productId, Integer quant)
			throws BusinessException {
		try {
			List<ZV03View> lst = promotionProgramDAO.getZV03ViewDetail(paging, promotionProgramId, productId, quant);
			ObjectVO<ZV03View> vo = new ObjectVO<ZV03View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV03View> getZV06ViewHeader(KPaging<ZV03View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV03View> lst = promotionProgramDAO.getZV06ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV03View> vo = new ObjectVO<ZV03View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV03View> getZV06ViewDetail(KPaging<ZV03View> paging, Long promotionProgramId, Long productId, Long amount)
			throws BusinessException {
		try {
			List<ZV03View> lst = promotionProgramDAO.getZV06ViewDetail(paging, promotionProgramId, productId, amount);
			ObjectVO<ZV03View> vo = new ObjectVO<ZV03View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV07ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV07ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV07ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			Float discountPercent, Integer quantity) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV07ViewDetail(paging, promotionProgramId, discountPercent, quantity);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV08ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV08ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV08ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			BigDecimal discountAmount, Integer quantity) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV08ViewDetail(paging, promotionProgramId, discountAmount, quantity);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV09ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV09ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV09ViewFreeProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			Integer quantity) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV09ViewFreeProductDetail(paging, promotionProgramId, quantity);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV09ViewSaleProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			Integer quantity) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV09ViewSaleProductDetail(paging, promotionProgramId, quantity);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV10ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV10ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV10ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			BigDecimal amount) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV10ViewDetail(paging, promotionProgramId, amount);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV11ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV11ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV11ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			BigDecimal amount) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV11ViewDetail(paging, promotionProgramId, amount);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV12ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV12ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV12ViewFreeProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			BigDecimal amount) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV12ViewFreeProductDetail(paging, promotionProgramId, amount);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV12ViewSaleProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			BigDecimal amount) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV12ViewSaleProductDetail(paging, promotionProgramId, amount);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV13ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV13ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV13ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			Float discountPercent) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV13ViewDetail(paging, promotionProgramId, discountPercent);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV14ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV14ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV14ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			BigDecimal discountAmount) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV14ViewDetail(paging, promotionProgramId, discountAmount);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV15ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV15ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV15ViewFreeProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			String productCodeStr) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV15ViewFreeProductDetail(paging, promotionProgramId, productCodeStr);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV15ViewSaleProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			String freeProductCodeStr) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV15ViewSaleProductDetail(paging, promotionProgramId, freeProductCodeStr);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV16ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV16ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV16ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			Float discountPercent) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV16ViewDetail(paging, promotionProgramId, discountPercent);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV17ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV17ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV17ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			BigDecimal discountAmount) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV17ViewDetail(paging, promotionProgramId, discountAmount);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV21ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV21ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV21ViewDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			BigDecimal amount) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV21ViewDetail(paging, promotionProgramId, amount);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public List<ProductInfoVO> getListProductInfoVO() throws BusinessException {
		
		try {
			return productInfoDAO.getListProductInfoVO();
		} catch (Exception e) {
			
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public List<ChannelTypeVO> getListChannelTypeVO()
			throws BusinessException {
		
		try {
			return channelTypeDAO.getListChannelTypeVO();
		} catch (Exception e) {
			
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public List<SaleCatLevelVO> getListSaleCatLevelVOByIdPro(Long id)
			throws BusinessException {
		
		try {
			return saleLevelCatDAO.getListSaleCatLevelVOByIdPro(id);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV18ViewHeader(KPaging<ZV07View> paging, Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV18ViewHeader(paging, promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV18ViewFreeProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			String productCodeStr) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV18ViewFreeProductDetail(paging, promotionProgramId, productCodeStr);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV18ViewSaleProductDetail(KPaging<ZV07View> paging, Long promotionProgramId, 
			String freeProductCodeStr) throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV18ViewSaleProductDetail(paging, promotionProgramId, freeProductCodeStr);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV01ViewHeader(Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV01ViewHeader(promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV02ViewHeader(Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV02ViewHeader(promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV04ViewHeader(Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV04ViewHeader(promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV05ViewHeader(Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV05ViewHeader(promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV19ViewHeader(Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV19ViewHeader(promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ZV07View> getZV20ViewHeader(Long promotionProgramId) 
			throws BusinessException {
		try {
			List<ZV07View> lst = promotionProgramDAO.getZV20ViewHeader(promotionProgramId);
			ObjectVO<ZV07View> vo = new ObjectVO<ZV07View>();
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<ChannelTypeVO> getListChannelTypeVOAlreadySet(
			KPaging<ChannelTypeVO> kPaging, long promotionProgramId)
			throws BusinessException {
		
		try {
			return channelTypeDAO.getListChannelTypeVOAlreadySet(kPaging, promotionProgramId);
		} catch (Exception e) {
			
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public List<SaleCatLevelVO> getListSaleCatLevelVOByIdProAlreadySet(
			KPaging<SaleCatLevelVO> kPaging, long promotionProgramId)
			throws BusinessException {
		
		try {
			return saleLevelCatDAO.getListSaleCatLevelVOByIdProAlreadySet(kPaging, promotionProgramId);
		} catch (Exception e) {
			
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public List<SaleCatLevelVO> getListSaleCatLevelVOByIdProAlreadySetSO(
			KPaging<SaleCatLevelVO> kPaging, long promotionProgramId)
			throws BusinessException {
		
		try {
			return saleLevelCatDAO.getListSaleCatLevelVOByIdProAlreadySetSO(kPaging, promotionProgramId);
		} catch (Exception e) {
			
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public ObjectVO<PromotionCustomerMap> getListPromotionCustomerMapByShopAndPP(
			KPaging<PromotionCustomerMap> paging, Long shopId,
			Long promotionProgramId, String customerCode, String customerName, String address) throws BusinessException {
		try {
			ObjectVO<PromotionCustomerMap> vo = new ObjectVO<PromotionCustomerMap>();
			vo.setLstObject(promotionCustomerMapDAO.getListPromotionCustomerMapByShopAndPP(paging, shopId, promotionProgramId,
					customerCode, customerName, address));
			vo.setkPaging(paging);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	@Override
	public List<AttributeDetailVO> getListPromotionCustAttVOCanBeSet(Long id)
			throws BusinessException {
		try {
			return attributeDetailDAO.getListAttributeDetailByAttributeId(null, id, ActiveType.RUNNING);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	@Override
	public List<PromotionCustAttVO2> getListPromotionCustAttVOValue(
			long promotionProgramId) throws BusinessException {
		
		try {
			return promotionCustAttrDAO.getListPromotionCustAttVOValue(promotionProgramId);
		} catch (Exception e) {
			
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public List<PromotionCustAttVO2> getListPromotionCustAttVOValueDetail(
			long promotionProgramId) throws BusinessException {
		
		try{
			return promotionCustAttrDAO.getListPromotionCustAttVOValueDetail(promotionProgramId);
		}catch (Exception e) {
			
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createOrUpdatePromotionCustAttVO(Long promotionProgramId,
			List<PromotionCustAttUpdateVO> lst, LogInfoVO logInfo)
			throws BusinessException {
		try {
			List<Long> lstExceptAttrId = new ArrayList<Long>();
			if(lst!=null){
				for (PromotionCustAttUpdateVO item:lst) {
					if(item.getPromotionCustAttr()!=null){
						if(item.getPromotionCustAttr().getId() != null){
							lstExceptAttrId.add(item.getPromotionCustAttr().getId());
						}
						
					}
				}
				if(promotionProgramId != null){
					promotionCustAttrDAO.deletePromotionCustAttrInfo(promotionProgramId, lstExceptAttrId, logInfo);
				}
				for (PromotionCustAttUpdateVO item:lst) {
					if(item.getPromotionCustAttr()!=null){
						if(item.getLstPromotionCustAttrDetail()!=null&&!item.getLstPromotionCustAttrDetail().isEmpty()){
							promotionCustAttrDAO.createOrUpdatePromotionCustAttDynamicVO(item.getPromotionCustAttr(), item.getLstPromotionCustAttrDetail(), logInfo);
						}else{
							promotionCustAttrDAO.createOrUpdatePromotionCustAttDynamicVO(item.getPromotionCustAttr());
						}
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}


	@Override
	public PromotionCustAttr getPromotionCustAttrByPromotion(long promotionId,
			Integer objectType, Long objectId) throws BusinessException {
		try {
			return promotionCustAttrDAO.getPromotionCustAttrByPromotion(promotionId, objectType, objectId);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public List<PromotionProgram> getListPromotionProgramByCustomerType(Long customerTypeId)
			throws BusinessException {
		try {
			return promotionProgramDAO.getListPromotionProgramByCustomerType(customerTypeId);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	
	@Override
	public Boolean isExistChildJoinProgram(String shopCode, Long promotionProgramId)
			throws BusinessException{
		try {
			return promotionShopMapDAO.isExistChildJoinProgram(shopCode, promotionProgramId, true);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public List<PromotionShopMapVO> getListPromotionShopMapVO(PromotionShopMapFilter filter, KPaging<PromotionShopMapVO> kPaging)
			throws BusinessException{
		try {
			return promotionShopMapDAO.getListPromotionShopMapVO(filter, kPaging);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public Boolean checkExistCustomerMap(Long id)throws BusinessException {
		try {
			if (id == null) {
				throw new IllegalArgumentException("promotionShopMapId is null");
			}
			return promotionCustomerMapDAO.checkExistCustomerMap(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkExistProductTimeShopInActive(Long promotionProgramId, String startDate, String endDate) throws BusinessException {
		if (promotionProgramId == null || startDate == null || startDate.length() == 0){
			throw new IllegalArgumentException("promotionProgramId or startDate is null");
		}
		try {
			return promotionProgramDAO.checkExistProductTimeShopInActive(promotionProgramId, startDate, endDate);
		} catch (DataAccessException e) {
			throw  new BusinessException(e);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionShopVO> getShopTreeInPromotionProgram(long shopId, long promotionId, String shopCode, String shopName, 
			Integer quantity) throws BusinessException {
		try {
			return promotionShopMapDAO.getShopTreeInPromotionProgram(shopId, promotionId, shopCode, shopName, quantity);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author hunglm16
	 */
	@Override
	public List<PromotionShopVO> searchShopTreeInPromotionProgramImpr(PromotionShopMapFilter filter) throws BusinessException {
		try {
			return promotionShopMapDAO.searchShopTreeInPromotionProgramImpr(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<PromotionCustomerVO> getCustomerInPromotionProgram(PromotionCustomerFilter filter) throws BusinessException {
		try {
			List<PromotionCustomerVO> lst = promotionCustomerMapDAO.getCustomerInPromotionProgram(filter);
			ObjectVO<PromotionCustomerVO> objVO = new ObjectVO<PromotionCustomerVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionStaffVO> getSalerInPromotionProgram(long promotionId, String shopCode, String shopName, 
			Integer quantity) throws BusinessException {
		try {
			return promotionStaffMapDAO.getSalerInPromotionProgram(promotionId, shopCode, shopName, quantity);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author trietptm
	 */
	@Override
	public ObjectVO<PromotionCustomerVO> searchCustomerForPromotionProgram(PromotionCustomerFilter filter) throws BusinessException {
		try {
			List<PromotionCustomerVO> lst = promotionCustomerMapDAO.searchCustomerForPromotionProgram(filter);
			ObjectVO<PromotionCustomerVO> objVO = new ObjectVO<PromotionCustomerVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionCustomerVO> getListCustomerInPromotion(long promotionId, List<Long> lstCustId) throws BusinessException {
		try {
			List<PromotionCustomerVO> lst = promotionCustomerMapDAO.getListCustomerInPromotion(promotionId, lstCustId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<PromotionShopVO> searchShopForPromotionProgram(long promotionId, long pShopId, String shopCode, String shopName, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		try {
			List<PromotionShopVO> lst = promotionShopMapDAO.searchShopForPromotionProgram(promotionId, pShopId, shopCode, shopName, staffRootId, roleId, shopRootId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionShopVO> getListShopInPromotion(long promotionId, List<Long> lstShopId, boolean shopMapOnly) throws BusinessException {
		try {
			List<PromotionShopVO> lst = promotionShopMapDAO.getListShopInPromotion(promotionId, lstShopId, shopMapOnly);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public PromotionStaffMap getPromotionStaffMapById(long id) throws BusinessException {
		try {
			return promotionStaffMapDAO.getPromotionStaffMapById(id);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public void updatePromotionStaffMap(PromotionStaffMap promotionStaffMap, LogInfoVO logInfo) throws BusinessException {
		try {
			promotionStaffMapDAO.updatePromotionStaffMap(promotionStaffMap, logInfo);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public PromotionStaffMap createPromotionStaffMap(PromotionStaffMap promotionStaffMap, LogInfoVO logInfo) throws BusinessException {
		try {
			return promotionStaffMapDAO.createPromotionStaffMap(promotionStaffMap, logInfo);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void removePromotionStaffMap(long promotionId, long shopId, LogInfoVO logInfo) throws BusinessException {
		try {
			List<PromotionStaffMap> lst = promotionStaffMapDAO.getListPromotionStaffMap(promotionId, shopId, ActiveType.RUNNING.getValue());
			if (lst == null || lst.size() == 0) {
				return;
			}
			for (PromotionStaffMap psm : lst) {
				psm.setStatus(ActiveType.DELETED);
				promotionStaffMapDAO.updatePromotionStaffMap(psm, logInfo);
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionStaffVO> searchStaffForPromotion(PromotionStaffFilter filter) throws BusinessException {
		try {
			return promotionStaffMapDAO.searchStaffForPromotion(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionStaffVO> getListStaffInPromotion(long promotionId, List<Long> lstStaffId) throws BusinessException {
		try {
			return promotionStaffMapDAO.getListStaffInPromotion(promotionId, lstStaffId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public PromotionShopMap getPromotionParentShopMap(long promotionId, long shopId, Integer status) throws BusinessException {
		try {
			return promotionShopMapDAO.getPromotionParentShopMap(promotionId, shopId, status);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public void createListPromotionStaffMap(List<PromotionStaffMap> lst, LogInfoVO logInfo) throws BusinessException {
		try {
			promotionStaffMapDAO.createListPromotionStaffMap(lst, logInfo);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionShopMapVO> getListPromotionShopMapVO2(PromotionShopMapFilter filter) throws BusinessException {
		try {
			return promotionStaffMapDAO.getListPromotionShopMapVO(filter);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionShopMapVO> getListPromotionShopMapVO3(PromotionShopMapFilter filter) throws BusinessException {
		try {
			return promotionCustomerMapDAO.getListPromotionShopMapVO(filter);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public PromotionStaffMap getPromotionStaffMapByShopMapAndStaff(long shopMapId, long staffId) throws BusinessException {
		try {
			return promotionStaffMapDAO.getPromotionStaffMapByShopMapAndStaff(shopMapId, staffId);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public PromotionProductsVO calculatePromotionProducts2(CalculatePromotionFilter calculatePromotionFilter) throws BusinessException {
		try {
			return promotionProgramDAO.calculatePromotionProducts2(calculatePromotionFilter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<PromotionProgram> getPromotionProgramByProductAndShopAndCustomer(Long productId, long shopId, long customerId,
			Date orderDate, Long saleOrderId, Long staffId) throws BusinessException {
		try {
			return promotionProgramDAO.getPromotionProgramByProductAndShopAndCustomer(productId,shopId,customerId, orderDate, saleOrderId, staffId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<SaleOrderDetailVO> getChangePromotionProductForEditingSaleOrder(Long shopId, 
			Long saleOrderId, String productCode, String programCode,
			Integer gotPromoLevel) throws BusinessException {
		try {
			List<SaleOrderDetailVO> result = new ArrayList<SaleOrderDetailVO>();
			
			SaleOrder so = saleOrderDAO.getSaleOrderById(saleOrderId);
			SaleOrderDetailFilter<SaleOrderDetail> filter = new SaleOrderDetailFilter<SaleOrderDetail>();
			filter.setSaleOrderId(saleOrderId);
			filter.setProductCode(productCode);
			filter.setProgramCode(programCode);
			filter.setPromoLevel(gotPromoLevel);
			List<SaleOrderDetail> saleOrderDetails = saleOrderDetailDAO.getSaleOrderDetails(filter);
			
			if (saleOrderDetails != null && saleOrderDetails.size() > 0) {
				SaleOrderDetail sod = saleOrderDetails.get(0);
				/*
				 * get received promotive quota
				 */
				int basePromotiveProductQuantity = promotionProgramDAO.getBasePromotiveProductQuantityForEditingSaleOrder(saleOrderId, sod.getProduct().getProductCode(), programCode, gotPromoLevel);
				if (basePromotiveProductQuantity == 0) {
					return result;
				}
				Integer gotPromotiveQuota = sod.getMaxQuantityFree()/basePromotiveProductQuantity;
				
				List<SaleOrderDetailVOEx> sodExs = promotionProgramDAO.getPromotiveProductForEditingSaleOrder(shopId, saleOrderId, sod.getProduct().getProductCode(), programCode, gotPromoLevel);
				if (sodExs != null && sodExs.size() > 0) {
					for (SaleOrderDetailVOEx sodEx : sodExs) {
						SaleOrderDetailVO sodVO = new SaleOrderDetailVO();
						sodVO.setLevelPromo(gotPromoLevel);
						sodVO.setType(1);
						sodVO.setPromoProduct(productDAO.getProductById(sodEx.getProductId()));
						//sodVO.setPromotionType(null);
						sodVO.setAvailableQuantity(sodEx.getAvailableQuantity());
						sodVO.setChangeProduct(1);
						List<PromotionProgram> pp = promotionProgramDAO.getPromotionProgramByProductAndShopAndCustomer(sodVO.getPromoProduct().getId(), so.getShop().getId(),
								so.getCustomer().getId(), so.getOrderDate(), saleOrderId, so.getStaff().getId());
						if (pp != null && pp.size() > 0) {
							sodVO.setIsEdited(pp.get(0).getIsEdited());// lay tam pp dau tien (chua ra soat toi)
						}
						SaleOrderDetail tmpSod = new SaleOrderDetail();
						sodVO.setSaleOrderDetail(tmpSod);
						tmpSod.setIsFreeItem(1);
						tmpSod.setLevelPromo(gotPromoLevel);
						tmpSod.setMaxQuantityFree(gotPromotiveQuota * sodEx.getQuantity());
//						tmpSod.setMaxQuantityFreeCk(tmpSod.getMaxQuantityFree());
						tmpSod.setOrderNumber(gotPromoLevel);
						tmpSod.setProduct(sodVO.getPromoProduct());
						tmpSod.setProgramCode(programCode);
						tmpSod.setQuantity(tmpSod.getMaxQuantityFree());
						for (SaleOrderDetail detail : saleOrderDetails) {
							if (detail.getProduct() != null 
//									&& detail.getProduct().getId().equals(sodVO.getPromoProduct().getId())
									) {
								if (detail.getProductGroupId() != null) {
									try {
										ProductGroup productGroup = promotionProgramDAO.getProductGroup(detail.getProductGroupId());
										tmpSod.setProductGroup(productGroup);
									} catch (Exception e) {
										// pass through
									}
								}
								
								if (detail.getGroupLevelId() != null) {
									try {
										GroupLevel groupLevel = promotionProgramDAO.getGroupLevelById(detail.getGroupLevelId());
										tmpSod.setGroupLevel(groupLevel);
									} catch (Exception e) {
										// pass through
									}
								}
								break;
							}
						}
						
						List<SaleOrderLot> lstSaleOrderLot = new ArrayList<SaleOrderLot>();
						sodVO.setSaleOrderLot(lstSaleOrderLot);
						
						SaleOrderLot sol = new SaleOrderLot();
						lstSaleOrderLot.add(sol);
						StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwner(sodEx.getProductId(), StockObjectType.SHOP, shopId, sodEx.getWarehouseId());
						if (stockTotal != null) {
							stockTotal.setAvailableQuantity(sodEx.getAvailableQuantity());
						}
						sol.setStockTotal(stockTotal);
						sol.setWarehouse(commonDAO.getEntityById(Warehouse.class, sodEx.getWarehouseId()));
						sol.setQuantity(tmpSod.getMaxQuantityFree());
						
						result.add(sodVO);
					}					
				}
			}
			return result;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptAccumulativePromotionProgramVO getRptAccumulativePPForCustomer(Long shopId, Long customerId, Long promotionId, Date orderDate) throws BusinessException {
		return this.getRptAccumulativePPVO(shopId, promotionId, customerId, orderDate, null , null, null,null);
	}

	@Override
	public Boolean isSaleOrderPromotionProgramStructureHasChanged(Long saleOrderId) throws BusinessException {
		try {
			int countSaleOrderDetailHasInvalidPromotionGroupLevel = promotionProgramDAO.countSaleOrderDetailHasInvalidPromotionGroupLevel(saleOrderId);
			return countSaleOrderDetailHasInvalidPromotionGroupLevel != 0;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<String> getChangedStructurePromotionProgramInSaleOrder(
			Long saleOrderId) throws BusinessException {
		try {
			return promotionProgramDAO.getChangedStructurePromotionProgramInSaleOrder(saleOrderId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<OrderProductVO> getSaleOrdersHasChangedStructurePromotionProgram(List<Long> saleOrderIds) throws BusinessException {
		try {
			return promotionProgramDAO.getSaleOrdersHasChangedStructurePromotionProgram(saleOrderIds);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<OrderProductVO> getSaleOrdersHasChangedStructurePromotionProgramWithFilter(SoFilter soFilter) throws BusinessException {
		try {
			return promotionProgramDAO.getSaleOrdersHasChangedStructurePromotionProgram(soFilter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<PromotionItem> calculatePromotionZV23(Long shopId, Long customerId, Long staffId,
			Date orderDate, Long exceptOrderId) throws BusinessException {
		List<PromotionItem> receivablePromotionItems = null;
		try {
			/*
			 * lay danh sach tat ca cac CTKM tich luy ma KH co tham gia
			 */
			List<PromotionProgram> participatingPromotionPrograms = promotionProgramDAO.getAccumulativePromotionProgramByProductAndShopAndCustomer(shopId, customerId, staffId, orderDate);
			if (participatingPromotionPrograms != null && participatingPromotionPrograms.size() > 0) {
				receivablePromotionItems = new ArrayList<PromotionItem>();
				for (int i = 0, iLoopSize = participatingPromotionPrograms.size(); i < iLoopSize; i++) {
					PromotionProgram promotionProgram = participatingPromotionPrograms.get(i);
					if (promotionProgram == null) {
						continue;
					}
					/*
					 * lay thong tin tich luy san pham/tien cua KH
					 */
					List<PromoProductConvVO> lstConvert = new ArrayList<PromoProductConvVO>();
					//list de tru quy doi, luu san pham tich luy da tru pay 
					Map<Long, RptAccumulativePromotionProgramDetail> mapAccumulativePPDetailSubConvert = new HashMap<Long, RptAccumulativePromotionProgramDetail>();
					Map<Long, Object> mapAccumulativePPDetailFinal = new HashMap<Long, Object>();
					RptAccumulativePromotionProgramVO accumulativePPVO = this.getRptAccumulativePPVO(shopId, promotionProgram.getId(), customerId, 
																											orderDate,lstConvert,mapAccumulativePPDetailSubConvert, exceptOrderId, mapAccumulativePPDetailFinal);
					if (accumulativePPVO == null) {
						continue;
					}
					// Map<san pham, chi tiet tich luy cua san pham>
					Map<Long, Object> mapAccumulativePPDetail = this.getAccumulativePromotionProgramDetailMap(accumulativePPVO);
					
					if (mapAccumulativePPDetail == null) {
						continue;
					}
					
					/*
					 * lay thong tin co cau san pham mua cua CTKM
					 * NM1[PL1(L1, L2), PL2(L3, L4)], NM2[PL3(L5, L6), PL4(L7, L8)]
					 */
					List<ProductGroup> ppBuyStructures = promotionProgramDAO.getPromotionProgramStructure(promotionProgram.getId(), ProductGroupType.MUA);
					if (ppBuyStructures != null && ppBuyStructures.size() > 0) {
						for (int j = 0, jLoopSize = ppBuyStructures.size(); j < jLoopSize; j++) {
							ProductGroup buyGroup = ppBuyStructures.get(j);
							// tinh KM cho tung nhom SP mua khai bao trong co cau
							List<PromotionItem> promotionItems = this.getZV23PromotionItemsOfBuyGroup(accumulativePPVO.getRptAccumulativePP(), 
																									mapAccumulativePPDetail, mapAccumulativePPDetailFinal,
																									promotionProgram.getPromotionProgramCode(), buyGroup);
							
							if (promotionItems != null && promotionItems.size() > 0) {
								subConvert(promotionItems,mapAccumulativePPDetailSubConvert,lstConvert);
								for(PromotionItem temp:promotionItems){
									if(temp.getBuyProducts().size()>0){
										if(temp.getSubGroupLevel().getPercent()!=null){
											List<BigDecimal> lstAmountPercent = this.convertProductAmount(temp.getBuyProducts(), mapAccumulativePPDetailFinal,temp.getSubGroupLevel().getPercent());
											temp.setLstAmountPercent(lstAmountPercent);
										}
									}
								}
								receivablePromotionItems.addAll(promotionItems);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		return receivablePromotionItems;
	}
	
	public void subConvert(final List<PromotionItem> promotionItems,Map<Long, RptAccumulativePromotionProgramDetail> mapAccumulativePPDetailSubConvert,
			List<PromoProductConvVO> lstConvert){
		//tru quy doi
		if(promotionItems!=null && promotionItems.size()>0){
			for(PromotionItem item : promotionItems){
				if(item.getBuyProducts()!=null && item.getBuyProducts().size()>0){
					List<List<GroupLevelDetailVO>> lstBuyProduct = item.getBuyProducts();
					for(int k = 0, n=lstBuyProduct.size() ; k < n ; k++){
						Boolean flagRemain = true;//neu quy doi ma het phan bo thi xoa cac xuat sau do
						for(int m = 0, l=lstBuyProduct.get(k).size()  ; m < l ; m++){
							GroupLevelDetailVO vo = lstBuyProduct.get(k).get(m);
							RptAccumulativePromotionProgramDetail detailSub = mapAccumulativePPDetailSubConvert.get(vo.getProductId());
							if(detailSub == null){
								detailSub = new RptAccumulativePromotionProgramDetail();
								detailSub.setQuantity(BigDecimal.ZERO);
								detailSub.setAmount(BigDecimal.ZERO);
							}
							//tru so luong den khi het thi lay trong map convert
							if(ValueType.QUANTITY.getValue().equals(vo.getValueType())){
								BigDecimal quantity = detailSub.getQuantity().subtract(vo.getValue());
								Boolean overValue = detailSub.getQuantity().compareTo(BigDecimal.ZERO) <= 0? true:false;//neu het phan bo thi xoa dong di
								if(BigDecimal.ZERO.compareTo(quantity) > 0 ){
									BigDecimal remain = vo.getValue().subtract(detailSub.getQuantity());
									vo.setValue(detailSub.getQuantity());
									detailSub.setQuantity(BigDecimal.ZERO);
									//sau khi phan bo het sp chinh thi tru tiep wa SP quy doi
									for(PromoProductConvVO promo : lstConvert){
										if(promo.getProductId1().equals(vo.getProductId()) && promo.getQuantity1().compareTo(BigDecimal.ZERO) > 0
												&& promo.getQuantity0().compareTo(BigDecimal.ZERO) > 0){
											GroupLevelDetailVO newDetail = new GroupLevelDetailVO();
											newDetail.setProductId(promo.getProductId0());
											newDetail.setValueType(vo.getValueType());
											if(promo.getQuantity1().compareTo(remain) >= 0){// SP quy doi du sl phan bo
												newDetail.setValue(remain);
												remain = BigDecimal.ZERO;
											}else{//sp quy doi ko du sl phan bo, -het so luong va tiep tuc phan bo sp quy doi tiep theo
												newDetail.setValue(promo.getQuantity1());
												remain = remain.subtract(promo.getQuantity1());
												promo.setQuantity1(BigDecimal.ZERO);
											}
											newDetail.setValue(newDetail.getValue().multiply(promo.getFactor0()).divide(promo.getFactor1(), 2 , RoundingMode.DOWN));
											if(newDetail.getValue().compareTo(BigDecimal.ZERO) == 0){
												newDetail.setValue(new BigDecimal("0.01"));
											}
											lstBuyProduct.get(k).add(newDetail);
											promo.setQuantity0(promo.getQuantity0().subtract(newDetail.getValue()));
											promo.setQuantity1(promo.getQuantity0().multiply(promo.getFactor1()).divide(promo.getFactor0(), 2 , RoundingMode.DOWN));
											if(BigDecimal.ZERO.compareTo(remain)>=0){
												break;
											}
										}
									}
									if(BigDecimal.ZERO.compareTo(remain) < 0){
										flagRemain=false;
									}
								}else{//sp chinh phan bo chua het tich luy
									detailSub.setQuantity(quantity);
								}
								if(overValue){//neu het phan bo thi xoa dong di
									lstBuyProduct.get(k).remove(m);
									m--;l--;
								}
							}else{
								BigDecimal amount = detailSub.getAmount().subtract(vo.getValue());
								Boolean overValue = detailSub.getAmount().compareTo(BigDecimal.ZERO) <= 0? true:false;//neu het phan bo thi xoa dong di
								if(BigDecimal.ZERO.compareTo(amount) > 0 ){
									BigDecimal remain = vo.getValue().subtract(detailSub.getAmount());
									vo.setValue(detailSub.getAmount());
									detailSub.setAmount(BigDecimal.ZERO);
									//sau khi phan bo het sp chinh thi tru tiep wa SP quy doi
									for(PromoProductConvVO promo : lstConvert){
										if(promo.getProductId1().equals(vo.getProductId()) && promo.getAmount1().compareTo(BigDecimal.ZERO) > 0
												&& promo.getAmount0().compareTo(BigDecimal.ZERO) > 0){
											GroupLevelDetailVO newDetail = new GroupLevelDetailVO();
											newDetail.setProductId(promo.getProductId0());
											newDetail.setValueType(vo.getValueType());
											if(promo.getAmount1().compareTo(remain) >= 0){// SP quy doi du sl phan bo
												newDetail.setValue(remain);
												remain = BigDecimal.ZERO;
											}else{//sp quy doi ko du sl phan bo, -het so luong va tiep tuc phan bo sp quy doi tiep theo
												newDetail.setValue(promo.getAmount1());
												remain = remain.subtract(promo.getAmount1());
												promo.setAmount1(BigDecimal.ZERO);
											}
											newDetail.setValue(newDetail.getValue().multiply(promo.getFactor0()).divide(promo.getFactor1(), 0 , RoundingMode.DOWN));
											if(newDetail.getValue().compareTo(BigDecimal.ZERO) == 0){
												newDetail.setValue(new BigDecimal("1"));
											}
											lstBuyProduct.get(k).add(newDetail);
											promo.setAmount0(promo.getAmount0().subtract(newDetail.getValue()));
											promo.setAmount1(promo.getAmount0().multiply(promo.getFactor1()).divide(promo.getFactor0(), 0 , RoundingMode.DOWN));
											if(BigDecimal.ZERO.compareTo(remain)>=0){
												break;
											}
										}
									}
									if(BigDecimal.ZERO.compareTo(remain) < 0){
										flagRemain=false;
									}
								}else{//sp chinh phan bo chua het tich luy
									detailSub.setAmount(amount);
								}
								if(overValue){//neu het phan bo thi xoa dong di
									lstBuyProduct.get(k).remove(m);
									m--;l--;
								}
							}
						}
						if(!flagRemain){
							lstBuyProduct.remove(k);
							k--;n--;
						}
					}
				}
			}
		}
	}
	
	/**
	 * convert thong tin tong hop CTKM sang kieu du lieu Map<san pham, chi tiet> de xu ly
	 * @author tuannd20
	 * @param accumulativePPVO
	 * @return
	 * @date 10/12/2014
	 */
	private Map<Long, Object> getAccumulativePromotionProgramDetailMap(RptAccumulativePromotionProgramVO accumulativePPVO) {
		if (accumulativePPVO != null
			&& accumulativePPVO.getRptAccumulativePPDetail() != null
			&& accumulativePPVO.getRptAccumulativePPDetail().size() > 0) {
			List<RptAccumulativePromotionProgramDetail> rptAccumulativePPDetails = accumulativePPVO.getRptAccumulativePPDetail();
			Map<Long, Object> mapRptDetail = new HashMap<Long, Object>();
			for (RptAccumulativePromotionProgramDetail rptDetail : rptAccumulativePPDetails) {
				mapRptDetail.put(rptDetail.getProduct().getId(), rptDetail.clone());
			}
			return mapRptDetail;
		}
		return null;
	}

	/**
	 * lay thong tin tong hop cua CTKM tich luy
	 * @author tuannd20
	 * @param shopId
	 * @param promotionProgramId
	 * @param customerId
	 * @param orderDate
	 * @return
	 * @date 10/12/2014
	 */
	private RptAccumulativePromotionProgramVO getRptAccumulativePPVO(Long shopId, Long promotionProgramId, Long customerId, Date orderDate,final List<PromoProductConvVO> lstConvert,
			final Map<Long, RptAccumulativePromotionProgramDetail> lstRptDetailSubConvert, Long exceptOrderId,
			final Map<Long, Object> mapAccumulativePPDetailFinal) {
		if (exceptOrderId == null) {
			exceptOrderId = -1L;
		}
		RptAccumulativePromotionProgramVO rptAccumulativePPVO = null;
		try {
			// get rpt
			RptAccumulativePromotionProgram rptAccumulativePP = promotionProgramDAO.getRptAccumulativePP(shopId, promotionProgramId, customerId, orderDate);
			if (rptAccumulativePP == null) {
				return null;
			}
			// get rpt detail
			List<RptAccumulativePromotionProgramDetail> rptAccumulativePPDetailsEx = promotionProgramDAO.getRptAccumulativePPDetail(rptAccumulativePP.getId());
			List<RptAccumulativePromotionProgramDetail> rptAccumulativePPDetails = new ArrayList<RptAccumulativePromotionProgramDetail>();
			List<RptAccumulativePromotionProgramDetail> rptAccumulativePPDetailsFinal = new ArrayList<RptAccumulativePromotionProgramDetail>();
			for(RptAccumulativePromotionProgramDetail temp : rptAccumulativePPDetailsEx){
				rptAccumulativePPDetails.add(temp.clone());
				rptAccumulativePPDetailsFinal.add(temp.clone());
			}
			if(mapAccumulativePPDetailFinal != null){
				for(RptAccumulativePromotionProgramDetail detail:rptAccumulativePPDetails){
					mapAccumulativePPDetailFinal.put(detail.getProduct().getId(),detail.clone());
				}
			}
			
			//get rpt pay de tru` so luong da KM
			List<RptCTTLPayDetail> rptPayDetail = promotionProgramDAO.getListRptCTTLPayDetail(null,shopId, promotionProgramId, customerId, orderDate);
			if (rptAccumulativePPDetails != null && rptAccumulativePPDetails.size() > 0 && rptPayDetail != null && rptPayDetail.size() > 0){
				for (RptAccumulativePromotionProgramDetail detail : rptAccumulativePPDetails) {
					for (RptCTTLPayDetail pay : rptPayDetail) {
						if((pay.getRptCTTLPay() ==  null || pay.getRptCTTLPay().getSaleOrder() == null
								|| !exceptOrderId.equals(pay.getRptCTTLPay().getSaleOrder().getId())) // lacnv1 - khong tru don hang hien tai dang duoc chinh sua
									&& detail.getProduct().getId().equals(pay.getProduct().getId())){
							detail.setAmount(detail.getAmount().subtract(pay.getAmountPromotion()));
							detail.setQuantity(detail.getQuantity().subtract(pay.getQuantityPromotion()));
						}
					}
					if(detail.getAmount().compareTo(BigDecimal.ZERO) < 0){
						detail.setAmount(BigDecimal.ZERO);
					}
					if(detail.getQuantity().compareTo(BigDecimal.ZERO) < 0){
						detail.setQuantity(BigDecimal.ZERO);
					}
				}
			}
			if(lstRptDetailSubConvert!=null){
				for(RptAccumulativePromotionProgramDetail detail:rptAccumulativePPDetails){
					lstRptDetailSubConvert.put(detail.getProduct().getId(),detail.clone());
				}
			}
			
			//lay san pham quy doi
			if(lstConvert!=null){
				List<PromoProductConvVO> lstConvertTemp = promotionProgramDAO.getListConvertPromotionProgram(promotionProgramId);
				if(lstConvertTemp!=null && lstConvertTemp.size()>0){
					//put vao map
					LinkedHashMap<Long , PromoProductConvVO> mapConvert = new LinkedHashMap<Long , PromoProductConvVO>(); 
					for(PromoProductConvVO vo:lstConvertTemp){
						vo.setQuantity0(BigDecimal.ZERO);
						vo.setQuantity1(BigDecimal.ZERO);
						vo.setAmount0(BigDecimal.ZERO);
						vo.setAmount1(BigDecimal.ZERO);
						mapConvert.put(vo.getProductId0(), vo);
						lstConvert.add(vo);
					}
					
					//+ vao theo factor
					for(int i = 0 ; i < rptAccumulativePPDetails.size() ; i++){
						RptAccumulativePromotionProgramDetail detail = rptAccumulativePPDetails.get(i);
						if(detail.getProduct()!=null){
							PromoProductConvVO vo = mapConvert.get(detail.getProduct().getId());
							if(vo!=null){
								BigDecimal quantity = detail.getQuantity().multiply(vo.getFactor1()).divide(vo.getFactor0(), 2 , RoundingMode.DOWN);
								BigDecimal amount = detail.getAmount().multiply(vo.getFactor1()).divide(vo.getFactor0(), 0 , RoundingMode.DOWN);
								vo.setQuantity1(quantity);
								vo.setAmount1(amount);
								vo.setQuantity0(detail.getQuantity());
								vo.setAmount0(detail.getAmount());
								Boolean existsProductSource = false;
								for(RptAccumulativePromotionProgramDetail d : rptAccumulativePPDetails){
									if(d.getProduct()!=null && d.getProduct().getId().equals(vo.getProductId1())){
										existsProductSource = true;
										d.setAmount(d.getAmount().add(amount));
										d.setQuantity(d.getQuantity().add(quantity));
									}
								}
								if(!existsProductSource){// khong co ban ghi san pham source nao trong tich luy
									RptAccumulativePromotionProgramDetail d = new RptAccumulativePromotionProgramDetail();
									d.setProduct(productDAO.getProductById(vo.getProductId1()));
									d.setQuantity(quantity);
									d.setAmount(amount);
									rptAccumulativePPDetails.add(d);
								}
							}
						}
					}
				}
			}
			
			// set to a VO
			rptAccumulativePPVO = new RptAccumulativePromotionProgramVO();
			rptAccumulativePPVO.setRptAccumulativePP(rptAccumulativePP);
			rptAccumulativePPVO.setRptAccumulativePPDetail(rptAccumulativePPDetails);
			rptAccumulativePPVO.setRptAccumulativePPDetailFinal(rptAccumulativePPDetailsFinal);
		} catch (Exception e) {
			LogUtility.logError(e, "Calculate accumutive promotion program, "
								+ "fail to initialize rptAccumulativePromotionProgramVO. "
								+ "[promotion_program_id = " + promotionProgramId + ", customer_id = " + customerId + ", shop_id = " + shopId + "]");
		}
		return rptAccumulativePPVO;
	}

	private List<PromotionItem> getZV23PromotionItemsOfBuyGroup(final RptAccumulativePromotionProgram rptAccumulativePP, 
																final Map<Long, Object> mapAccumulativePPDetail,final Map<Long, Object> mapAccumulativePPDetailFinal,
																final String promotionProgramCode,
																final ProductGroup buyGroup) throws Exception {
		//NM1[PL1(L1, L2), PL2(L3, L4)]
		List<PromotionItem> promotionItems = null;
		if (buyGroup != null) {
			promotionItems = new ArrayList<PromotionItem>();
			//final Boolean IS_MULTIPLE = buyGroup.getMultiple() != null ? buyGroup.getMultiple() == 1 : false;	// so lan phan bo so luong tinh KM
			final Boolean IS_MULTIPLE = true;
			final Boolean IS_RECURSIVE = buyGroup.getRecursive() != null ? buyGroup.getRecursive() == 1 : false;	// co tinh TOI UU KM hay ko, lap 1 muc nhieu lan
			try {
				final int ONE = 1;
				/*
				 * lay danh sach cac muc cua nhom duoc mapping voi 1 nhom KM nao do
				 * PL1(L1, L2), PL2(L3, L4)
				 */
				List<GroupMapping> parentLevelSaleStructures = promotionProgramDAO.getPromotionProgramParentLevelStructures(buyGroup.getPromotionProgram().getId(), buyGroup.getId());
				int iLoopSize = IS_RECURSIVE ? parentLevelSaleStructures.size() : ONE;				
				for (int i = 0; i < iLoopSize; i++) {
					GroupMapping groupMapping = parentLevelSaleStructures.get(i);
					GroupLevel parentSaleGroupLevel = groupMapping.getSaleGroupLevel();
					GroupLevel parentPromoSaleGroupLevel = groupMapping.getPromotionGroupLevel();
					/*
					 * get sub-sale group levels to check if it can get a promotive ration
					 * AND(L1{detail: product, product}, L2{detail: amount, amount})
					 */
					List<GroupLevel> lstSubSaleGroupLevel = promotionProgramDAO.getListSubLevelByParentId(parentSaleGroupLevel.getId());
					Integer receivablePromotiveRation = 0;
					List<List<GroupLevelDetailVO>> lstConvert = new ArrayList<List<GroupLevelDetailVO>>();
					if (parentSaleGroupLevel.getHasProduct() != null && parentSaleGroupLevel.getHasProduct() == 1) {	// phan bo theo san pham
						receivablePromotiveRation = this.getReceivablePromotiveRationByProduct(mapAccumulativePPDetail, mapAccumulativePPDetailFinal,
																								parentSaleGroupLevel, lstSubSaleGroupLevel, 
																								lstConvert,IS_MULTIPLE);
					} else {	// phan bo theo tien
						receivablePromotiveRation = this.getReceivablePromotiveRationByAmount(rptAccumulativePP, lstSubSaleGroupLevel, IS_MULTIPLE);
					}
					
					/*
					 * neu nhu dat duoc KM thi tinh toan phan KM duoc nhan: KM sp, (KM tien, KM phan tram)
					 */
					if (receivablePromotiveRation > 0) {
						List<PromotionItem> promotionItemsOfSaleGroup = this.getPromotionItems(promotionProgramCode, receivablePromotiveRation, parentSaleGroupLevel, parentPromoSaleGroupLevel);
						if (promotionItemsOfSaleGroup != null && promotionItemsOfSaleGroup.size() > 0) {
							for(PromotionItem temp:promotionItemsOfSaleGroup){
								if(lstConvert.size()>0){
									temp.setBuyProducts(lstConvert);
								}
							}
							promotionItems.addAll(promotionItemsOfSaleGroup);
						}
					}
				}
			} catch (Exception e) {
				throw e;
			}
		}
		return promotionItems;
	}
	
	/**
	 * lay thong tin co cau khuyen mai dat duoc
	 * @author tungmt
	 * @return so tien cac lan dat
	 * @throws DataAccessException
	 * @date 12/12/2014
	 */
	private List<BigDecimal> convertProductAmount(List<List<GroupLevelDetailVO>> lstConvert, Map<Long, Object> mapAccumulativePPDetail, Float percent){
		List<BigDecimal> lstResult = new ArrayList<BigDecimal>();
		for(List<GroupLevelDetailVO> lst: lstConvert){
			BigDecimal result = BigDecimal.ZERO;
			for(GroupLevelDetailVO vo: lst){
				if(vo.getProductId()!=null && mapAccumulativePPDetail.get(vo.getProductId())!=null){
					RptAccumulativePromotionProgramDetail detail = (RptAccumulativePromotionProgramDetail) mapAccumulativePPDetail.get(vo.getProductId());
					if(detail!=null){
						if(ValueType.QUANTITY.getValue().equals(vo.getValueType())){
							BigDecimal temp = vo.getValue().multiply(detail.getAmount()).divide(detail.getQuantity(), 0, RoundingMode.DOWN);
							result = result.add(temp);
						}else{
							result = result.add(BigDecimal.valueOf(vo.getValue().floatValue()));
						}
					}
				}
			}
			result = result.multiply(new BigDecimal(percent.toString())).divide(BigDecimal.valueOf(100)).setScale(0,RoundingMode.DOWN);
			lstResult.add(result);
		}
		
		return lstResult;
	}
	

	/**
	 * lay thong tin co cau khuyen mai dat duoc
	 * @author tuannd20
	 * @param receivablePromotiveRation
	 * @param parentPromoGroupLevel
	 * @return co cau KM cung voi so suat tuong ung voi co cau do
	 * @throws DataAccessException
	 * @date 12/12/2014
	 */
	private List<PromotionItem> getPromotionItems(	final String promotionProgramCode,
													final Integer receivablePromotiveRation, 
													final GroupLevel parentSaleGroupLevel,
													final GroupLevel parentPromoGroupLevel) throws DataAccessException {
		final int GROUP_LEVEL_HAS_PRODUCT = 1;
		List<PromotionItem> promotionItems = null;
		
		List<GroupLevel> lstSubPromoGroupLevel = promotionProgramDAO.getListSubLevelByParentId(parentPromoGroupLevel.getId());
		if (lstSubPromoGroupLevel != null && lstSubPromoGroupLevel.size() > 0) {
			promotionItems = new ArrayList<PromotionItem>();
			for (int i = 0, size = lstSubPromoGroupLevel.size(); i < size; i++) {
				GroupLevel subGroupLevel = lstSubPromoGroupLevel.get(i);
				
				PromotionItem promotionItem = new PromotionItem();
				promotionItem.setPromotionProgramCode(promotionProgramCode);
				promotionItem.setRatiton(receivablePromotiveRation);
				promotionItem.setParentGroupId(parentPromoGroupLevel.getId());
				promotionItem.setSubGroupId(subGroupLevel.getId());
				promotionItem.setSubGroupLevel(subGroupLevel);
				
				if (subGroupLevel.getHasProduct() == GROUP_LEVEL_HAS_PRODUCT) {
					List<GroupLevelDetailVO> groupLevelDetailVOs = promotionProgramDAO.getListGroupLevelDetailVO(subGroupLevel.getId());
					promotionItem.setPromotionProducts(groupLevelDetailVOs);					
				}
//				else if(subGroupLevel.getPercent()!=null){
//					List<GroupLevel> lstSubSaleGroupLevel = promotionProgramDAO.getListSubLevelByParentId(parentSaleGroupLevel.getId());
//					if(lstSubSaleGroupLevel!=null && lstSubSaleGroupLevel.size()>0){
//						List<GroupLevelDetailVO> groupLevelDetailVOs = promotionProgramDAO.getListGroupLevelDetailVO(lstSubSaleGroupLevel.get(0).getId());
//						promotionItem.setBuyProducts(groupLevelDetailVOs);
//					}
//				}
				
				promotionItems.add(promotionItem);
			}
		}
		return promotionItems;
	}
	
	/**
	 * tinh so lan dat duoc 1 muc KM, phan bo theo tien tich luy
	 * Cach tinh: phan bo tien tich luy cua san pham/don hang vao co cau khai bao, den khi ko the phan bo them duoc nua thi dung
	 * Moi lan phan bo thanh cong thi tinh thanh 1 lan dat muc KM.
	 * 
	 * @author tuannd20
	 * @param rptAccumulativePP
	 * @param lstSubSaleGroupLevel
	 * @param iS_MULTIPLE
	 * @return
	 * @date 10/12/2014
	 */
	private Integer getReceivablePromotiveRationByAmount(final RptAccumulativePromotionProgram rptAccumulativePP, 
														final List<GroupLevel> lstSubSaleGroupLevel, 
														final Boolean iS_MULTIPLE) {
		Integer gotPromotiveRation = 0;		// tong so lan dat muc KM
		boolean canGetOneMorePromotiveRation = false;	// cho biet co the dat them 1 lan KM cua muc hay ko
		Map<Long, Object> mapDistributeInfoCache = new HashMap<Long, Object>();	// luu lai thong tin phan bo theo san pham de rollback local du lieu khi khong the dat duoc KM
		Map<Long, Object> mapGroupLevelDetailCache = new HashMap<Long, Object>();	// cache lai group level detail de khi tinh KM dang multiple ko phai request DB nua
		
		/*
		 * thuc hien phan bo de tinh KM
		 */
		while (canGetOneMorePromotiveRation) {// vong lap vo tan
			
		}
		return 0;
	}

	/**
	 * tinh so lan dat duoc 1 muc KM, phan bo theo san pham.
	 * Cach tinh:
	 * Phan bo so luong SP da tich luy vao co cau nhom mua cho den khi nao khong the phan bo them duoc nua thi dung.
	 * Moi lan phan bo thanh cong thi tinh thanh 1 lan dat muc KM.
	 * 
	 * @author tuannd20
	 * @param mapAccumulativePPDetail
	 * @param parentSaleGroupLevel
	 * @param lstSubSaleGroupLevel
	 * @param IS_MULTIPLE
	 * @return
	 * @throws DataAccessException
	 * @date 10/12/2014
	 */
	private Integer getReceivablePromotiveRationByProduct(
			final Map<Long, Object> mapAccumulativePPDetail,
			final Map<Long, Object> mapAccumulativePPDetailFinal,
			final GroupLevel parentSaleGroupLevel,
			final List<GroupLevel> lstSubSaleGroupLevel, 
			final List<List<GroupLevelDetailVO>> lstConvert,
			final Boolean IS_MULTIPLE) throws DataAccessException {
		// AND(L1{detail: product, product}, L2{detail: amount, amount})
		Integer gotPromotiveRation = 0;		// tong so lan dat muc KM
		boolean canGetOneMorePromotiveRation = true;	// cho biet co the dat them 1 lan KM cua muc hay ko
		Map<Long, Object> mapDistributeInfoCache = new HashMap<Long, Object>();	// luu lai thong tin phan bo theo san pham de rollback local du lieu khi khong the dat duoc KM
		Map<Long, Object> mapGroupLevelDetailCache = new HashMap<Long, Object>();	// cache lai group level detail de khi tinh KM dang multiple ko phai request DB nua
		
		if (lstSubSaleGroupLevel == null || lstSubSaleGroupLevel.size() == 0) {
			return 0;
		}
		/*
		 * thuc hien phan bo tinh KM
		 */
		while (canGetOneMorePromotiveRation) {
			/*
			 * neu nhu tat ca cac level con deu thoa man (dieu kien AND) thi dat 1 suat KM
			 */
			for (int i = 0, size = lstSubSaleGroupLevel.size(); i < size && canGetOneMorePromotiveRation; i++) {
				GroupLevel groupLevel = lstSubSaleGroupLevel.get(i);
				BigDecimal groupMinQuantity = groupLevel.getRawMinQuantity()!=null ? BigDecimal.valueOf(groupLevel.getRawMinQuantity()) : null;
				BigDecimal groupMinAmount = groupLevel.getRawMinAmount();
				/*
				 * neu nhu trong cache da co thi lay trong cache, con khong thi request DB
				 */
				List<GroupLevelDetail> lstGroupLevelDetail = null;
				if (mapGroupLevelDetailCache.containsKey(groupLevel.getId())) {
					lstGroupLevelDetail = (List<GroupLevelDetail>) mapGroupLevelDetailCache.get(groupLevel.getId());
				} else {
					lstGroupLevelDetail = promotionProgramDAO.getListGroupLevelDetailByLevelId(groupLevel.getId());	// order by: is_required, product_code
					// luu cache
					mapGroupLevelDetailCache.put(groupLevel.getId(), lstGroupLevelDetail);
				}
				
				/*
				 * Phan bo so luong/so tien da tich luy vao tung nhom san pham da khai bao sao cho level hien tai thoa man dieu kien khai bao.
				 * Neu nhu ko the phan bo duoc (da phan bo het san pham, het tien tich luy) thi tuc la level hien tai ko du dieu kien dat 1 suat KM,
				 * do do, muc cha cung ko dat them suat KM nao
				 */
				canGetOneMorePromotiveRation = this.tryDistributingQuantityAmountIntoGroupLevelDetail(groupMinQuantity, groupMinAmount,
																									lstGroupLevelDetail, 
																									mapAccumulativePPDetail,mapAccumulativePPDetailFinal,
																									mapDistributeInfoCache);
			}
			
			/*
			 * Neu nhu khong the phan bo them de huong 1 suat KM thi rollback lai cac buoc da phan bo de tinh KM lai trong truong hop TOI UU (2).
			 * Nguoc lai, tinh 1 suat KM (1)
			 */
			if (canGetOneMorePromotiveRation) {	// (1)
				gotPromotiveRation++;
				List<GroupLevelDetailVO> lstTemp = new ArrayList<GroupLevelDetailVO>();
				for (Entry<Long, Object> entry : mapDistributeInfoCache.entrySet()) {
					GroupLevelDetailVO temp = new GroupLevelDetailVO();
					temp.setProductId(entry.getKey());
					List<Object> lstObject = (List<Object>) entry.getValue();
					temp.setValue((BigDecimal)lstObject.get(1));
					if(String.valueOf(lstObject.get(0)).equals("QUANTITY")){
						temp.setValueType(ValueType.QUANTITY.getValue());
					}else{
						temp.setValueType(ValueType.AMOUNT.getValue());
					}
					lstTemp.add(temp);
				}
				if(lstTemp.size()>0){
					lstConvert.add(lstTemp);
				}
				mapDistributeInfoCache = new HashMap<Long, Object>();	// reset cache, moi lan dat 1 muc KM thi chot lai so lieu, tranh restore nham
			} else {	// (2)
				this.restoreDistributedQuantityAmountIntoGroupLevelDetail(mapAccumulativePPDetail, mapDistributeInfoCache);
			}
			
			/*
			 * neu ko tinh boi so thi xem nhu da tinh xong muc KM nay
			 */
			if (!IS_MULTIPLE) {
				break;
			}
		}
				
		return gotPromotiveRation;
	}

	/**
	 * truong hop ko dat duoc muc KM thi restore lai so lieu da phan bo, de phan bo cho cac muc tiep theo
	 * @author tuannd20
	 * @param mapAccumulativePPDetail
	 * @param mapDistributeInfoCache
	 * @date 10/12/2014
	 */
	private void restoreDistributedQuantityAmountIntoGroupLevelDetail(
			Map<Long, Object> mapAccumulativePPDetail,
			Map<Long, Object> mapDistributeInfoCache) {
		
		Iterator<Entry<Long, Object>> iterator = mapDistributeInfoCache.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Long, Object> entry = iterator.next();
			RptAccumulativePromotionProgramDetail rptAccumulativePPDetail = (RptAccumulativePromotionProgramDetail) 
																							mapAccumulativePPDetail.get(entry.getKey());
			List<Object> changeObject = (List<Object>) entry.getValue();
			if (changeObject != null && changeObject.size() == 2) {
				if (((ValueType)changeObject.get(0)) == ValueType.QUANTITY) {
					BigDecimal currentQuantity = rptAccumulativePPDetail.getQuantity();
					rptAccumulativePPDetail.setQuantity(currentQuantity.add((BigDecimal)changeObject.get(1)));
				} else if (((ValueType)changeObject.get(0)) == ValueType.AMOUNT) {
					BigDecimal currentAmount = rptAccumulativePPDetail.getAmount();
					rptAccumulativePPDetail.setAmount(currentAmount.add((BigDecimal)changeObject.get(1)));
				}
			}			
		}
	}
	
	/**
	 * phan bo so luong tich luy vao danh sach san pham cua group_level
	 * @author tuannd20
	 * @param groupMinQuantity
	 * @param groupMinAmount 
	 * @param lstGroupLevelDetail
	 * @param mapAccumulativePPDetail
	 * @param mapDistributeInfoCache
	 * @return
	 * @date 10/12/2014
	 */
	private Boolean tryDistributingQuantityAmountIntoGroupLevelDetail(	final BigDecimal groupMinQuantity,
																		final BigDecimal groupMinAmount, 
																		final List<GroupLevelDetail> lstGroupLevelDetail,
																		final Map<Long, Object> mapAccumulativePPDetail,
																		final Map<Long, Object> mapAccumulativePPDetailFinal, 
																		final Map<Long, Object> mapDistributeInfoCache) {
		BigDecimal accumulativeDistributedValue = BigDecimal.ZERO;		// tong so luong/so tien tich luy cua tat ca san pham, dung de so sanh voi so luong/so tien toi thieu cua level
		Boolean distributable = true;			// co the phan bo tren toan bo group hay ko
		String distributeMode = ZV23_DISTRIBUTE_MODE_NORMAL;
		
		while (distributable) {
			Boolean IS_DISTRIBUTE_BY_QUANTITY = false,
					IS_DISTRIBUTE_BY_AMOUNT = false;
			/*
			 * phan bo so luong/tien tich luy vao cac san pham trong group
			 */
			Boolean flag = false;// kiem tra tat ca san het phan bo
			for (int i = 0, size = lstGroupLevelDetail.size(); i < size && distributable; i++) {
				GroupLevelDetail groupLevelDetail = lstGroupLevelDetail.get(i);
				/*
				 * phan bo theo so luong (1) hay theo tien (2)
				 */
				if (groupLevelDetail.getValueType() == ValueType.QUANTITY) {	// (1): QUANTITY
					IS_DISTRIBUTE_BY_QUANTITY = true;
				} else {	// (2): AMOUNT
					IS_DISTRIBUTE_BY_AMOUNT = true;
				}
				
				Map.Entry<String, BigDecimal> distributedValueEntry = new AbstractMap.SimpleEntry<String, BigDecimal>("dummy", null);
				distributable = this.distributingProductQuantityAmount(lstGroupLevelDetail, groupLevelDetail,
																		groupMinQuantity, groupMinAmount,
																		mapAccumulativePPDetail,mapAccumulativePPDetailFinal,
																		accumulativeDistributedValue,
																		distributeMode,
																		distributedValueEntry);
				if(distributable){
					flag=true;
				}
				if(groupLevelDetail.getIsRequired()==0 || distributeMode.equalsIgnoreCase(ZV23_DISTRIBUTE_MODE_ACCUMULATE)){
					distributable = true;
				}
				BigDecimal distributedValue = distributedValueEntry.getValue() != null ? distributedValueEntry.getValue() : BigDecimal.ZERO;
				accumulativeDistributedValue = accumulativeDistributedValue.add(distributedValue);
				// luu cache
				if (distributable && distributedValue != null && distributedValue.compareTo(BigDecimal.ZERO) > 0) {
					//phan bo qua so luong can thiet cua group thi` tru` bot đi
					if(IS_DISTRIBUTE_BY_QUANTITY && groupMinQuantity != null && accumulativeDistributedValue.compareTo(groupMinQuantity) > 0){
						BigDecimal remain = accumulativeDistributedValue.subtract(groupMinQuantity);
						distributedValue = distributedValue.subtract(remain);
						RptAccumulativePromotionProgramDetail rptAccDetail = null;
						if (mapAccumulativePPDetail.containsKey(groupLevelDetail.getProduct().getId())) {	
							rptAccDetail = (RptAccumulativePromotionProgramDetail)mapAccumulativePPDetail.get(groupLevelDetail.getProduct().getId());
							if(rptAccDetail != null){
								rptAccDetail.setQuantity(rptAccDetail.getQuantity().add(remain));
							}
						}
					}else if(IS_DISTRIBUTE_BY_AMOUNT && groupMinAmount != null && accumulativeDistributedValue.compareTo(groupMinAmount) > 0){
						BigDecimal remain = accumulativeDistributedValue.subtract(groupMinAmount);
						distributedValue = distributedValue.subtract(remain);
						RptAccumulativePromotionProgramDetail rptAccDetail = null;
						if (mapAccumulativePPDetail.containsKey(groupLevelDetail.getProduct().getId())) {	
							rptAccDetail = (RptAccumulativePromotionProgramDetail)mapAccumulativePPDetail.get(groupLevelDetail.getProduct().getId());
							if(rptAccDetail != null){
								rptAccDetail.setAmount(rptAccDetail.getAmount().add(remain));
							}
						}
					}
					this.saveDistributedQuantityIntoCache(groupLevelDetail.getProduct().getId(), distributedValue, groupLevelDetail.getValueType(), mapDistributeInfoCache);
				}
			}
			
			/*
			 * kiem tra xem co tiep tuc phan bo de thoa dieu kien so luong toi thieu cua group (neu co define) hay khong
			 */
			if (distributable && flag) {
				if ((IS_DISTRIBUTE_BY_QUANTITY && groupMinQuantity == null) ||
					(IS_DISTRIBUTE_BY_AMOUNT && groupMinAmount == null)) {		// phan bo thanh cong => 1 lan dat muc
					break;
				} else if ((IS_DISTRIBUTE_BY_QUANTITY && accumulativeDistributedValue.compareTo(groupMinQuantity) < 0) ||
							(IS_DISTRIBUTE_BY_AMOUNT && accumulativeDistributedValue.compareTo(groupMinAmount) < 0)) {
					/*
					 * truong hop co define so luong toi thieu cua group, ma lan phan bo truoc van chua dam bao dieu kien so luong toi thieu,
					 * thi tiep tuc phan bo cho du dieu kien so luong toi thieu de dat 1 lan KM.
					 */
					distributeMode = ZV23_DISTRIBUTE_MODE_ACCUMULATE;
				}else if((IS_DISTRIBUTE_BY_QUANTITY && accumulativeDistributedValue.compareTo(groupMinQuantity) >= 0) ||
						(IS_DISTRIBUTE_BY_AMOUNT && accumulativeDistributedValue.compareTo(groupMinAmount) >= 0)){
					break;
				}
			} else {
				/*
				 * neu nhu lan phan bo nay ko thanh cong, thi kiem tra xem day co phai la lan phan bo de dam bao dieu kien so luong toi thieu cua group hay ko.
				 * Neu dung va da dam bao dieu kien so luong toi thieu, thi xem nhu 1 dat muc KM.
				 * Nguoc lai, ko dat muc KM nay
				 */
				if (distributeMode.equalsIgnoreCase(ZV23_DISTRIBUTE_MODE_ACCUMULATE)) {
					if ((IS_DISTRIBUTE_BY_QUANTITY && groupMinQuantity != null && accumulativeDistributedValue.compareTo(groupMinQuantity) >= 0) ||
						(IS_DISTRIBUTE_BY_AMOUNT && groupMinAmount != null && accumulativeDistributedValue.compareTo(groupMinAmount) >= 0)) {
						distributable = true;						
						break;
					}else{
						distributable = false;						
						break;
					}
				}
			}
		}
		
		return distributable;
	}
	
	@SuppressWarnings("unchecked")
	private void saveDistributedQuantityIntoCache(final Long distributingProductId, 
													final BigDecimal distributedValue,
													final ValueType valueType,
													final Map<Long, Object> mapDistributeInfoCache) {
		/*
		 * cong don so luong da phan bo trong truong hop dang apply BOI SO
		 */
		List<Object> changeObject = null;
		if (mapDistributeInfoCache.containsKey(distributingProductId)) {
			changeObject = (List<Object>) mapDistributeInfoCache.get(distributingProductId);
			if (changeObject != null && changeObject.size() == 2) {
				int index = 1;
				changeObject.set(index, ((BigDecimal)changeObject.get(index)).add(distributedValue));
			}
		} else {
			changeObject = Arrays.asList(valueType, (Object)distributedValue);
		}
		mapDistributeInfoCache.put(distributingProductId, changeObject);
	}
	
	/**
	 * phan bo so luong tich luy vao san pham dang xet
	 * @author tuannd20
	 * @param lstGroupLevelDetail
	 * @param groupLevelDetail
	 * @param groupMinQuantity
	 * @param groupMinAmount 
	 * @param mapAccumulativePPDetail
	 * @param currentAccumulativeValue
	 * @param DISTRIBUTE_MODE
	 * @param distributedValueEntry
	 * @return
	 * @date 10/12/2014
	 */
	private Boolean distributingProductQuantityAmount(	final List<GroupLevelDetail> lstGroupLevelDetail,
														final GroupLevelDetail groupLevelDetail,
														final BigDecimal groupMinQuantity, final BigDecimal groupMinAmount, 
														final Map<Long, Object> mapAccumulativePPDetail,
														final Map<Long, Object> mapAccumulativePPDetailFinal,
														final BigDecimal currentAccumulativeValue,
														final String DISTRIBUTE_MODE,
														final Entry<String, BigDecimal> distributedValueEntry) {
		if (groupLevelDetail == null) {
			return false;
		}
		final Boolean IS_DISTRIBUTE_BY_QUANTITY = groupLevelDetail.getValueType() == ValueType.QUANTITY;
		final Boolean IS_DISTRIBUTE_BY_AMOUNT = groupLevelDetail.getValueType() == ValueType.AMOUNT;
		
		Boolean distributable = true;	// co the phan bo so luong tich luy cho san pham dang xu ly hay ko
		Product distributingProduct = groupLevelDetail.getProduct();
		Boolean isProductRequired = groupLevelDetail.getIsRequired() != null ? groupLevelDetail.getIsRequired() == 1 : false;
		BigDecimal declareProductValue = null;
		if(IS_DISTRIBUTE_BY_AMOUNT){
			BigDecimal amountOneProduct = new BigDecimal(1);
			if(distributingProduct!=null){
				RptAccumulativePromotionProgramDetail rptTemp = (RptAccumulativePromotionProgramDetail)mapAccumulativePPDetailFinal.get(distributingProduct.getId());
				if(rptTemp!=null && rptTemp.getAmount()!=null && rptTemp.getQuantity()!=null && BigDecimal.ZERO.compareTo(rptTemp.getQuantity())!=0){
					amountOneProduct = rptTemp.getAmount().divide(rptTemp.getQuantity(), 0, RoundingMode.UP);
				}
			}
 			declareProductValue = groupLevelDetail.getValue() != null ? groupLevelDetail.getValue() : amountOneProduct;	// so luong/so tien san pham trong co cau khai bao
		}else{
			declareProductValue = groupLevelDetail.getValue() != null ? groupLevelDetail.getValue() : new BigDecimal(1);	// so luong/so tien san pham trong co cau khai bao
		}
		RptAccumulativePromotionProgramDetail rptAccumulativeProductDetail = null;
		if (mapAccumulativePPDetail.containsKey(distributingProduct.getId())) {	
			rptAccumulativeProductDetail = (RptAccumulativePromotionProgramDetail)mapAccumulativePPDetail.get(distributingProduct.getId());
		}
		// so luong da tich luy cua san pham dang xet
		BigDecimal rptAccumulativeProductQuantity = rptAccumulativeProductDetail != null && rptAccumulativeProductDetail.getQuantity() != null 
												? rptAccumulativeProductDetail.getQuantity() : BigDecimal.ZERO;
		BigDecimal distributedQuantity = null;	// so luong da phan bo cua san pham
		
		// so tien da tich luy cua san pham dang xet
		BigDecimal rptAccumulativeProductAmount = rptAccumulativeProductDetail != null && rptAccumulativeProductDetail.getAmount() != null 
												? rptAccumulativeProductDetail.getAmount() : BigDecimal.ZERO;
		BigDecimal distributedAmount = null;	// so tien da phan bo cua san pham
		
		//Neu so tien/so luong tich luy nho hon gia tri 1 SP thi gan lai declareProductValue = tich luy
		if(groupLevelDetail.getValue() == null){
			if(IS_DISTRIBUTE_BY_QUANTITY && rptAccumulativeProductQuantity.compareTo(BigDecimal.ZERO) > 0 && rptAccumulativeProductQuantity.compareTo(declareProductValue) < 0){
				declareProductValue = rptAccumulativeProductQuantity;
			}else if(IS_DISTRIBUTE_BY_AMOUNT && rptAccumulativeProductAmount.compareTo(BigDecimal.ZERO) > 0 && rptAccumulativeProductAmount.compareTo(declareProductValue) < 0){
				declareProductValue = rptAccumulativeProductAmount;
			}
		}
		
		if (DISTRIBUTE_MODE.equalsIgnoreCase(ZV23_DISTRIBUTE_MODE_NORMAL)) {
			if ((IS_DISTRIBUTE_BY_QUANTITY && groupMinQuantity != null) 
				|| (IS_DISTRIBUTE_BY_AMOUNT && groupMinAmount != null)) {
				if (isProductRequired) {
					/*
					 * Neu so luong/so tien tich luy >= gia tri (so luong hoac so tien) khai bao thi phan bo theo so luong khai bao
					 * Nguoc lai, xem nhu ko the phan bo (vi san pham la bat buoc)
					 */
					if (IS_DISTRIBUTE_BY_QUANTITY && declareProductValue != null && rptAccumulativeProductQuantity.compareTo(declareProductValue) >= 0){
						distributedQuantity = declareProductValue;
						rptAccumulativeProductDetail.setQuantity(rptAccumulativeProductQuantity.subtract(distributedQuantity));							
					} else if (IS_DISTRIBUTE_BY_AMOUNT && declareProductValue != null && rptAccumulativeProductAmount.compareTo(declareProductValue) >= 0){
						distributedAmount = declareProductValue;
						rptAccumulativeProductDetail.setAmount(rptAccumulativeProductAmount.subtract(distributedAmount));
					} else {
						distributable = false;
					}
				} else {
					if (IS_DISTRIBUTE_BY_QUANTITY && rptAccumulativeProductQuantity.compareTo(BigDecimal.ZERO) > 0) {
						BigDecimal maxDistributedQuantity = groupMinQuantity.subtract(currentAccumulativeValue);
						distributedQuantity = rptAccumulativeProductQuantity.min(maxDistributedQuantity != null ? maxDistributedQuantity : new BigDecimal(1)) ;
						rptAccumulativeProductDetail.setQuantity(rptAccumulativeProductQuantity.subtract(distributedQuantity));
					} else if (IS_DISTRIBUTE_BY_AMOUNT && rptAccumulativeProductAmount.compareTo(BigDecimal.ZERO) > 0) {
						BigDecimal maxDistributedAmount = groupMinAmount.subtract(currentAccumulativeValue);
						distributedAmount = rptAccumulativeProductAmount.compareTo(maxDistributedAmount) <= 0 ? rptAccumulativeProductAmount : maxDistributedAmount;
						rptAccumulativeProductDetail.setAmount(rptAccumulativeProductAmount.subtract(distributedAmount));
					}
				}
			} else {	// khong define groupMinQuantity/groupMinAmount
				/*
				 * Truong hop khong define groupMinQuantity/groupMinAmount,
				 * San pham bat buoc thi check nhu binh thuong (1)
				 * San pham tuy chon (optional) thi check nhu san pham bat buoc neu nhu co tong hop cho san pham do, truong hop nay xem nhu
				 * so luong/so tien nhap cho san pham tuy chon la so luong/so tien toi thieu phai co. (2)
				 */
				if (isProductRequired ||
					(!isProductRequired && IS_DISTRIBUTE_BY_QUANTITY && rptAccumulativeProductQuantity.compareTo(BigDecimal.ZERO) > 0) ||
					(!isProductRequired && IS_DISTRIBUTE_BY_AMOUNT && rptAccumulativeProductAmount.compareTo(BigDecimal.ZERO) > 0)) {
					if (IS_DISTRIBUTE_BY_QUANTITY && rptAccumulativeProductQuantity.compareTo(declareProductValue) >= 0) {
						distributedQuantity = declareProductValue;
						rptAccumulativeProductDetail.setQuantity(rptAccumulativeProductQuantity.subtract(distributedQuantity));
					} else if (IS_DISTRIBUTE_BY_AMOUNT && rptAccumulativeProductAmount.compareTo(declareProductValue) >= 0){
						distributedAmount = declareProductValue;
						rptAccumulativeProductDetail.setAmount(rptAccumulativeProductAmount.subtract(distributedAmount));
					} else {
						distributable = false;
					}
				}
			}
		} else if (DISTRIBUTE_MODE.equalsIgnoreCase(ZV23_DISTRIBUTE_MODE_ACCUMULATE)) {
			/*
			 * mode nay chi xay ra khi groupMinQuantity/groupMinAmount co gia tri
			 */
			if (IS_DISTRIBUTE_BY_QUANTITY && groupMinQuantity.compareTo(currentAccumulativeValue) > 0) {
				distributedQuantity = rptAccumulativeProductQuantity.min(declareProductValue);
				if (distributedQuantity.compareTo(BigDecimal.ZERO) > 0) {
					rptAccumulativeProductDetail.setQuantity(rptAccumulativeProductQuantity.subtract(distributedQuantity));				
				}else{
					distributable = false;
				}
			} else if (IS_DISTRIBUTE_BY_AMOUNT && groupMinAmount.compareTo(currentAccumulativeValue) == 1) {
				distributedAmount = rptAccumulativeProductAmount.compareTo(declareProductValue) <= 0 ? rptAccumulativeProductAmount : declareProductValue;
				if (distributedAmount.compareTo(BigDecimal.ZERO) > 0) {
					rptAccumulativeProductDetail.setAmount(rptAccumulativeProductAmount.subtract(distributedAmount));				
				}else{
					distributable = false;
				}
			} else {
				distributable = false;
			}
		}
		
		if (IS_DISTRIBUTE_BY_QUANTITY) {
			distributedValueEntry.setValue(distributedQuantity);			
		} else if (IS_DISTRIBUTE_BY_AMOUNT) {
			distributedValueEntry.setValue(distributedAmount);
		}
		return distributable;
	}

	@Override
	public List<SaleOrderDetailVO> getChangePromotionProductForEditingSaleOrder2(Long shopId, 
			Long saleOrderId, String productCode, String programCode,
			Long promoLevelId, Integer gotPromoLevel) throws BusinessException {
		try {
			List<SaleOrderDetailVO> result = new ArrayList<SaleOrderDetailVO>();
			
			SaleOrder so = saleOrderDAO.getSaleOrderById(saleOrderId);
			SaleOrderDetailFilter<SaleOrderDetail> filter = new SaleOrderDetailFilter<SaleOrderDetail>();
			filter.setSaleOrderId(saleOrderId);
			filter.setProductCode(productCode);
			filter.setProgramCode(programCode);
			filter.setPromoLevel(gotPromoLevel);
			filter.setLevelId(promoLevelId);
			List<SaleOrderDetail> saleOrderDetails = saleOrderDetailDAO.getSaleOrderDetails(filter);

			if (saleOrderDetails != null && saleOrderDetails.size() > 0) {
				SaleOrderDetail sod = saleOrderDetails.get(0);
				if (sod.getProductGroupId() != null) {
					ProductGroup group = commonDAO.getEntityById(ProductGroup.class, sod.getProductGroupId());
					sod.setProductGroup(group);
				}
				if (sod.getGroupLevelId() != null) {
					GroupLevel level = commonDAO.getEntityById(GroupLevel.class, sod.getGroupLevelId());
					sod.setGroupLevel(level);
				}
				/*
				 * get received promotive quota
				 */
				int basePromotiveProductQuantity = promotionProgramDAO.getBasePromotiveProductQuantityForEditingSaleOrder2(saleOrderId, sod.getProduct().getProductCode(), programCode, promoLevelId, gotPromoLevel);
				Integer gotPromotiveQuota = sod.getMaxQuantityFree()/basePromotiveProductQuantity;
				
				PromotionProgram pp = promotionProgramDAO.getPromotionProgramByCode(programCode);
				boolean hasPortion = promotionProgramDAO.checkPromotionProgramHasPortion(pp.getId(), shopId, so.getCustomer().getId(), so.getStaff().getId(), so.getOrderDate());
				if (hasPortion) {
					pp.setHasPortion(1);
				} else {
					pp.setHasPortion(0);
				}
				
				List<SaleOrderDetailVOEx> sodExs = promotionProgramDAO.getPromotiveProductForEditingSaleOrder2(shopId, saleOrderId, sod.getProduct().getProductCode(), programCode, promoLevelId, gotPromoLevel);
				if (sodExs != null && sodExs.size() > 0) {
					List<StockTotal> lstStock = null;
					for (SaleOrderDetailVOEx sodEx : sodExs) {
						StockTotal stockTotal = null;
						if (sodEx.getWarehouseId() == null) {
							StockTotalFilter stockTotalFilter = new StockTotalFilter();
							stockTotalFilter.setProductId(sodEx.getProductId());
							stockTotalFilter.setOwnerType(StockObjectType.SHOP);
							stockTotalFilter.setOwnerId(shopId);
							lstStock = stockTotalDAO.getListStockTotalByProductAndOwner(stockTotalFilter);
							if (lstStock != null && lstStock.size() > 0) {
								stockTotal = lstStock.get(0);
								sodEx.setWarehouseId(stockTotal.getWarehouse().getId());
								sodEx.setAvailableQuantity(stockTotal.getAvailableQuantity());
								sodEx.setStockQuantity(stockTotal.getQuantity());
							}
						} else {
							stockTotal = stockTotalDAO.getStockTotalByProductAndOwner(sodEx.getProductId(), StockObjectType.SHOP, shopId, sodEx.getWarehouseId());
						}
						if (sodEx.getWarehouseId() == null) {
							continue;
						}
						
						SaleOrderDetailVO sodVO = new SaleOrderDetailVO();
						sodVO.setLevelPromo(gotPromoLevel);
						sodVO.setType(1);
						sodVO.setPromoProduct(productDAO.getProductById(sodEx.getProductId()));
						//sodVO.setPromotionType(null);
						sodVO.setAvailableQuantity(sodEx.getAvailableQuantity());
						sodVO.setChangeProduct(1);
						/*PromotionProgram pp = promotionProgramDAO.getPromotionProgramByProductAndShopAndCustomer(sodVO.getPromoProduct().getId(), so.getShop().getId(),
								so.getCustomer().getId(), so.getOrderDate(), saleOrderId, so.getStaff().getId());*/
						sodVO.setPromotionType(SaleOrderDetailVO.PROMOTION_FOR_PRODUCT);
						if (pp != null) {
							sodVO.setIsEdited(pp.getIsEdited());
							if (PromotionType.ZV24.getValue().equalsIgnoreCase(pp.getType())) {
								sodVO.setOpenPromo(1);
							} else if (PromotionType.ZV21.getValue().equalsIgnoreCase(pp.getType())) {
								sodVO.setPromotionType(SaleOrderDetailVO.PROMOTION_FOR_ORDER_TYPE_PRODUCT);
							}
						}
						sodVO.setHasPortion(pp.getHasPortion());
						SaleOrderDetail tmpSod = new SaleOrderDetail();
						sodVO.setSaleOrderDetail(tmpSod);
						tmpSod.setIsFreeItem(1);
						tmpSod.setLevelPromo(gotPromoLevel);
						tmpSod.setMaxQuantityFree(gotPromotiveQuota * sodEx.getQuantity());
//						tmpSod.setMaxQuantityFreeCk(tmpSod.getMaxQuantityFree());
						tmpSod.setOrderNumber(gotPromoLevel);
						tmpSod.setProduct(sodVO.getPromoProduct());
						tmpSod.setProgramCode(programCode);
						tmpSod.setQuantity(tmpSod.getMaxQuantityFree());
						/*for (SaleOrderDetail detail : saleOrderDetails) {
							if (detail.getProduct() != null 
//									&& detail.getProduct().getId().equals(sodVO.getPromoProduct().getId())
									) {
								group = commonDAO.getEntityById(ProductGroup.class, detail.getProductGroupId());
								tmpSod.setProductGroup(group);
								level = commonDAO.getEntityById(GroupLevel.class, detail.getGroupLevelId());
								tmpSod.setGroupLevel(level);
								break;
							}
						}*/
						tmpSod.setProductGroup(sod.getProductGroup());
						tmpSod.setGroupLevel(sod.getGroupLevel());
						sodVO.setProductGroupId(sod.getProductGroupId());
						
						List<SaleOrderLot> lstSaleOrderLot = new ArrayList<SaleOrderLot>();
						sodVO.setSaleOrderLot(lstSaleOrderLot);
						
						SaleOrderLot sol = new SaleOrderLot();
						lstSaleOrderLot.add(sol);
						
						if (stockTotal != null) {
							stockTotal.setAvailableQuantity(sodEx.getAvailableQuantity());
						}
						
						sol.setStockTotal(stockTotal);
						sol.setWarehouse(commonDAO.getEntityById(Warehouse.class, sodEx.getWarehouseId()));
						sol.setQuantity(tmpSod.getMaxQuantityFree());
						
						result.add(sodVO);
					}					
				}
			}
			return result;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<PromotionProductOpenVO> listPromotionProductOpenVO(Long promotionId) throws BusinessException {
		try {
			return promotionProgramDAO.listPromotionProductOpenVO(promotionId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deletePromotionProductOpen(Long id) throws BusinessException {
		try {
			PromotionProductOpen entity = promotionProgramDAO.getPromotionProductOpen(id);
			if(entity == null) {
				throw new IllegalAccessException("entity is null");
			}
			entity.setStatus(ActiveType.DELETED);
			promotionProgramDAO.updatePromotionProductOpen(entity);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveListPromotionProductOpen(Long promotionId, List<PromotionProductOpenVO> listProductOpen, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			PromotionProgram promotionProgram = promotionProgramDAO.getPromotionProgramById(promotionId);
			if(promotionProgram == null) {
				throw new IllegalAccessException("promotionProgram is null");
			}
			for(PromotionProductOpenVO vo : listProductOpen) {
				if(vo.getId() != null) {
					PromotionProductOpen promotionProductOpen = promotionProgramDAO.getPromotionProductOpen(vo.getId());
					if(promotionProductOpen == null) {
						throw new IllegalAccessException("promotionProductOpen is null");
					}
					boolean isUpdate = false;
					if(promotionProductOpen.getProduct() != null && !promotionProductOpen.getProduct().getProductCode().equals(vo.getProductCode())) {
						isUpdate = true;
						promotionProductOpen.setProduct(productDAO.getProductByCode(vo.getProductCode()));
					}
					if(promotionProductOpen.getAmount() != null && !promotionProductOpen.getAmount().equals(vo.getAmount())) {
						isUpdate = true;
						promotionProductOpen.setAmount(vo.getAmount());
					}
					if(promotionProductOpen.getQuantity() != null && !promotionProductOpen.getQuantity().equals(vo.getQuantity())) {
						isUpdate = true;
						promotionProductOpen.setQuantity(vo.getQuantity());
					}
					if(isUpdate) {
						promotionProductOpen.setUpdateDate(currentDate);
						promotionProductOpen.setUpdateUser(logInfo.getStaffCode());
						promotionProgramDAO.updatePromotionProductOpen(promotionProductOpen);
					}
				} else {
					PromotionProductOpen promotionProductOpen = new PromotionProductOpen();
					promotionProductOpen.setPromotionProgram(promotionProgram);
					promotionProductOpen.setQuantity(vo.getQuantity());
					promotionProductOpen.setAmount(vo.getAmount());
					promotionProductOpen.setProduct(productDAO.getProductByCode(vo.getProductCode()));
					promotionProductOpen.setCreateDate(currentDate);
					promotionProductOpen.setCreateUser(logInfo.getStaffCode());
					promotionProgramDAO.createPromotionProductOpen(promotionProductOpen);
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<PPConvertVO> listPromotionProductConvertVO(Long promotionId, String name) throws BusinessException {
		try {
			return promotionProgramDAO.listPromotionProductConvertVO(promotionId, name);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void savePPConvert(Long promotionId, List<PPConvertVO> listVO, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			PromotionProgram promotionProgram = promotionProgramDAO.getPromotionProgramById(promotionId);
			for(PPConvertVO vo : listVO) {
				if(vo.getId() != null) {
					PPConvert ppConvert = promotionProgramDAO.getPPConvertById(vo.getId());
					if(ppConvert == null) {
						throw new IllegalAccessException("vo.getId() is not null -> ppConvert is null");
					}
					for(PPConvertDetailVO detail : vo.getListDetail()) {
						if(detail.getId() != null) {
							PPConvertDetail __detail = promotionProgramDAO.getPPConvertDetailById(detail.getId());
							boolean isUpdate = false;
							if(__detail.getIsSourceProduct() != null && !__detail.getIsSourceProduct().equals(detail.getIsSourceProduct())) {
								isUpdate = true;
								__detail.setIsSourceProduct(detail.getIsSourceProduct());
							}
							if(__detail.getFactor() != null && !__detail.getFactor().equals(detail.getFactor())) {
								isUpdate = true;
								__detail.setFactor(detail.getFactor());
							}
							if(__detail.getProduct() != null && !__detail.getProduct().getProductCode().equals(detail.getProductCode())) {
								isUpdate = true;
								__detail.setProduct(productDAO.getProductByCode(detail.getProductCode()));
							}
							if(isUpdate) {
								__detail.setUpdateDate(currentDate);
								__detail.setUpdateUser(logInfo.getStaffCode());
								promotionProgramDAO.updatePPConvertDetail(__detail);
							}
						} else {
							PPConvertDetail __detail = new PPConvertDetail();
							__detail.setIsSourceProduct(detail.getIsSourceProduct());
							__detail.setFactor(detail.getFactor());
							__detail.setProduct(productDAO.getProductByCode(detail.getProductCode()));
							__detail.setPromotionProductConvert(ppConvert);
							__detail.setStatus(ActiveType.RUNNING);
							__detail.setCreateDate(currentDate);
							__detail.setCreateUser(logInfo.getStaffCode());
							__detail = promotionProgramDAO.createPPConvertDetail(__detail);
						}
					}
				} else {
					PPConvert ppConvert = new PPConvert();
					ppConvert.setName(vo.getName());
					ppConvert.setPromotionProgram(promotionProgram);
					ppConvert.setStatus(ActiveType.RUNNING);
					ppConvert.setCreateDate(currentDate);
					ppConvert.setCreateUser(logInfo.getStaffCode());
					ppConvert = promotionProgramDAO.createPPConvert(ppConvert);
					
					for(PPConvertDetailVO detail : vo.getListDetail()) {
						PPConvertDetail __detail = new PPConvertDetail();
						__detail.setIsSourceProduct(detail.getIsSourceProduct());
						__detail.setFactor(detail.getFactor());
						__detail.setProduct(productDAO.getProductByCode(detail.getProductCode()));
						__detail.setPromotionProductConvert(ppConvert);
						__detail.setStatus(ActiveType.RUNNING);
						__detail.setCreateDate(currentDate);
						__detail.setCreateUser(logInfo.getStaffCode());
						__detail = promotionProgramDAO.createPPConvertDetail(__detail);
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deletePPConvert(Long id, LogInfoVO logInfo) throws BusinessException {
		try {
			PPConvert ppConvert = promotionProgramDAO.getPPConvertById(id);
			if(ppConvert == null) {
				throw new IllegalAccessException("ppConvert is null");
			}
			Date currentDate = commonDAO.getSysDate();
			ppConvert.setStatus(ActiveType.DELETED);
			ppConvert.setUpdateDate(currentDate);
			ppConvert.setUpdateUser(logInfo.getStaffCode());
			promotionProgramDAO.updatePPConvert(ppConvert);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deletePPConvertDetail(Long id, LogInfoVO logInfo) throws BusinessException {
		try {
			PPConvertDetail detail = promotionProgramDAO.getPPConvertDetailById(id);
			if(detail == null) {
				throw new IllegalAccessException("detail is null");
			}
			Date currentDate = commonDAO.getSysDate();
			detail.setStatus(ActiveType.DELETED);
			detail.setUpdateDate(currentDate);
			detail.setUpdateUser(logInfo.getStaffCode());
			promotionProgramDAO.updatePPConvertDetail(detail);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptCTTLPay createRptCTTLPay(
			RptCTTLPay rptCTTLPay, LogInfoVO logInfo)
			throws BusinessException {
		try {
//			Date currentDate = commonDAO.getSysDate();
//			rptCTTLPay.setCreateDate(currentDate);
			return promotionProgramDAO.createRptCTTLPay(rptCTTLPay,logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}


	@Override
	public RptCTTLPay getRptCTTLPayById(Long id)
			throws BusinessException {
		try {
			return id == null ? null : promotionProgramDAO.getRptCTTLPayById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptCTTLPayDetail createRptCTTLPayDetail(
			RptCTTLPayDetail rptCTTLPayDetail, LogInfoVO logInfo)
			throws BusinessException {
		try {
//			Date currentDate = commonDAO.getSysDate();
//			rptCTTLPayDetail.setCreateDate(currentDate);
			return promotionProgramDAO.createRptCTTLPayDetail(rptCTTLPayDetail,logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}


	@Override
	public RptCTTLPayDetail getRptCTTLPayDetailById(Long id)
			throws BusinessException {
		try {
			return id == null ? null : promotionProgramDAO.getRptCTTLPayDetailById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionItem> getAccumulationInfoOfOrder(long saleOrderId) throws BusinessException {
		List<PromotionItem> lstPromotions = null;
		try {
			SaleOrder so = saleOrderDAO.getSaleOrderById(saleOrderId);
			if (so == null) {
				return new ArrayList<PromotionItem>();
			}
			
			/*
			 * lay danh sach tat ca cac CTKM tich luy dang co trong don hang
			 */
			List<PromotionProgram> lstPrograms = saleOrderDAO.getAccumulativeProgramOfOrder(saleOrderId);
			if (lstPrograms != null && lstPrograms.size() > 0) {
				// khai bao bien cho vong lap
				PromotionProgram promotionProgram = null;
				/*RptAccumulativePromotionProgramVO accumulativePPVO = null;
				RptAccumulativePromotionProgram rpp = null;
				BigDecimal debit = null;*/
				List<RptCTTLPay> lstPays = null;
				List<RptCTTLPayDetail> lstPayDetails = null;
				RptCTTLPayDetail payDetail = null;
				SaleOrderDetail sod = null;
				SaleOrderPromoDetail sopd = null;
				GroupLevelDetailVO detail = null;
				GroupLevelDetailVO detailChange = null;
				Product p = null;
				List<GroupLevel> lstGroupLevels = null;
				List<GroupLevelDetail> lstGroupDetails = null;
				GroupLevelDetail groupDetail = null;
				PromotionItem pItem = null;
				Long productGroupId = null;
				Map<String, PromotionItem> mapTmp = new HashMap<String, PromotionItem>();
				SaleOrderFilter<RptCTTLPay> filter = new SaleOrderFilter<RptCTTLPay>();
				filter.setSaleOrderId(saleOrderId);
				// ---
				List<SaleOrderDetail> lstSOD = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(saleOrderId, null);
				List<SaleOrderLotVO> lstLot = saleOrderDetailDAO.getSaleOrderLotVOOfOrder(saleOrderId);
				List<SaleOrderPromoDetail> lstPromoDetails = null;
				//lstPromotions = new ArrayList<PromotionItem>();
				for (int i = 0, iLoopSize = lstPrograms.size(); i < iLoopSize; i++) {
					promotionProgram = lstPrograms.get(i);
					if (promotionProgram == null) {
						continue;
					}
					
					filter.setProgramId(promotionProgram.getId());
					lstPays = promotionProgramDAO.getListRptCTTLPay(filter);
					if (lstPays == null || lstPays.size() == 0) {
						continue;
					}
					
					lstPromoDetails = saleOrderDetailDAO.getListSaleOrderPromoDetailOfOrder(saleOrderId, promotionProgram.getPromotionProgramCode());
					
					for (int t = 0, szt = lstPays.size(); t < szt; t++) {
						if (lstSOD != null && lstSOD.size() > 0) {
							for (int j = 0, szj = lstSOD.size(); j < szj; j++) {
								sod = lstSOD.get(j);
								if (sod.getPayingOrder() == null) {
									sod.setPayingOrder(1);
								}
								if (sod.getIsFreeItem() == 1 && PromotionType.ZV23.getValue().equals(sod.getProgrameTypeCode())
										&& promotionProgram.getPromotionProgramCode().equalsIgnoreCase(sod.getProgramCode())
										&& sod.getPayingOrder() == (t+1)) {
									if (mapTmp.get(sod.getProgramCode()) == null) {
										pItem = new PromotionItem();
										pItem.setId(sod.getId());
										pItem.setPromotionProgramCode(sod.getProgramCode());
										pItem.setParentGroupId(sod.getGroupLevelId());
										pItem.setRatiton(szt);
										productGroupId = sod.getProductGroupId();
										
										lstGroupLevels = promotionProgramDAO.getListSubLevelByParentId(sod.getGroupLevelId());
										if (lstGroupLevels != null && lstGroupLevels.size() > 0) { // neu co nhieu con se khong biet lay thang con nao cho dung
											pItem.setSubGroupId(lstGroupLevels.get(0).getId());
											pItem.setSubGroupLevel(lstGroupLevels.get(0));
										}
										lstGroupDetails = promotionProgramDAO.getListGroupLevelDetailByLevelId(pItem.getSubGroupId());
										pItem.setLstPromoProducts(new ArrayList<GroupLevelDetailVO>());
										if (lstGroupDetails != null && lstGroupDetails.size() > 0) {
											for (int k = 0, szk = lstGroupDetails.size(); k < szk; k++) {
												groupDetail = lstGroupDetails.get(k);
												if (groupDetail.getProduct() != null) {
													// luu danh sach doi sp khuyen mai
													detailChange = new GroupLevelDetailVO();
													detailChange.setProductId(groupDetail.getProduct().getId());
													detailChange.setProductCode(groupDetail.getProduct().getProductCode());
													detailChange.setProductName(groupDetail.getProduct().getProductName());
													detailChange.setConvfact(groupDetail.getProduct().getConvfact());
													detailChange.setGroupLevelId(sod.getGroupLevelId());
													detailChange.setProductGroupId(sod.getProductGroupId());
													if (pItem.getSubGroupLevel() != null) {
														detailChange.setMinQuantity(pItem.getSubGroupLevel().getMinQuantity());
														detailChange.setMaxQuantity(pItem.getSubGroupLevel().getMaxQuantity());
													}
													detailChange.setValueType(groupDetail.getValueType().getValue());
													detailChange.setValue(groupDetail.getValue());
													detailChange.setIsRequired(groupDetail.getIsRequired());
													
													pItem.getLstPromoProducts().add(detailChange);
												}
											}
										}
										
										pItem.setBuyProducts(new ArrayList<List<GroupLevelDetailVO>>());
										
										mapTmp.put(sod.getProgramCode(), pItem);
										lstGroupLevels = null;
										lstGroupDetails = null;
									} else {
										pItem = mapTmp.get(sod.getProgramCode());
									}
									
									detail = new GroupLevelDetailVO();
									if (pItem.getPromotionProducts() == null) {
										pItem.setPromotionProducts(new ArrayList<GroupLevelDetailVO>());
									}
									
									p = sod.getProduct();
									detail.setProductId(p.getId());
									detail.setProductCode(p.getProductCode());
									detail.setProductName(p.getProductName());
									detail.setConvfact(p.getConvfact());
									detail.setGroupLevelId(sod.getGroupLevelId());
									detail.setProductGroupId(sod.getProductGroupId());
									if (pItem.getSubGroupLevel() != null) {
										detail.setMinQuantity(pItem.getSubGroupLevel().getMinQuantity());
										detail.setMaxQuantity(pItem.getSubGroupLevel().getMaxQuantity());
									}
									detail.setMaxQuantityFree(sod.getMaxQuantityFree());
									detail.setValueActually(new BigDecimal(sod.getQuantity()));
									
									if (pItem.getLstPromoProducts() != null && pItem.getLstPromoProducts().size() > 0) {
										for (int k = 0, szk = pItem.getLstPromoProducts().size(); k < szk; k++) {
											detailChange = pItem.getLstPromoProducts().get(k);
											if (detailChange.getProductId() != null && detailChange.getProductId().equals(p.getId())) {
												detail.setValueType(detailChange.getValueType());
												detail.setValue(detailChange.getValue());
												detail.setIsRequired(detailChange.getIsRequired());
												break;
											}
										}
									}
									detail.setLstSaleOrderLot(new ArrayList<SaleOrderLotVO>());
									if (lstLot != null && lstLot.size() > 0) {
										for (int k = 0, szk = lstLot.size(); k < szk; k++) {
											if (sod.getId().equals(lstLot.get(k).getSaleOrderDetailId())) {
												detail.getLstSaleOrderLot().add(lstLot.get(k));
											}
										}
									}
									detail.setPayingOrder(sod.getPayingOrder());
									pItem.getPromotionProducts().add(detail);
								}
							}
						}
						
						pItem = mapTmp.get(promotionProgram.getPromotionProgramCode());
						if (pItem == null) {
							pItem = new PromotionItem();
							pItem.setPromotionProgramCode(promotionProgram.getPromotionProgramCode());
							pItem.setRatiton(szt);
							pItem.setAmount(BigDecimal.ZERO);
							
							if (lstPromoDetails != null && lstPromoDetails.size() > 0) {
								for (int j = 0, szj = lstPromoDetails.size(); j < szj; j++) {
									sopd = lstPromoDetails.get(j);
									
									productGroupId = sopd.getProductGroupId();
									pItem.setParentGroupId(sopd.getGroupLevelId());
									if (pItem.getSubGroupId() == null && sopd.getGroupLevelId() != null) {
										lstGroupLevels = promotionProgramDAO.getListSubLevelByParentId(sopd.getGroupLevelId());
										if (lstGroupLevels != null && lstGroupLevels.size() > 0) { // neu co nhieu con se khong biet lay thang con nao cho dung
											pItem.setSubGroupId(lstGroupLevels.get(0).getId());
											pItem.setSubGroupLevel(lstGroupLevels.get(0));
										}
										lstGroupLevels = null;
										lstGroupDetails = null;
									}
									
									if (sopd.getDiscountAmount() != null) {
										pItem.setAmount(pItem.getAmount().add(sopd.getDiscountAmount()));
									}
								}
							}
							
							pItem.setBuyProducts(new ArrayList<List<GroupLevelDetailVO>>());
							
							mapTmp.put(promotionProgram.getPromotionProgramCode(), pItem);
						}
						
						//
						pItem = mapTmp.get(promotionProgram.getPromotionProgramCode());
						if (pItem != null) {
							lstPayDetails = promotionProgramDAO.getRptCTTLPayDetailByRptPayId(lstPays.get(t).getId());
							if (lstPayDetails != null && lstPayDetails.size() > 0) {
								if (productGroupId != null && pItem.getParentGroupId() != null) {
									lstGroupDetails = promotionProgramDAO.getListGroupLevelDetailByPromoGroupAndPromoGroupLevel(productGroupId, pItem.getParentGroupId());
								}
								for (int k = 0, szk = lstPayDetails.size(); k < szk; k++) {
									payDetail = lstPayDetails.get(k);
									detail = new GroupLevelDetailVO();
									
									p = payDetail.getProduct();
									detail.setProductId(p.getId());
									detail.setProductCode(p.getProductCode());
									detail.setProductName(p.getProductName());
									detail.setConvfact(p.getConvfact());
									if (lstGroupDetails != null && lstGroupDetails.size() > 0) {
										detail.setValueType(lstGroupDetails.get(0).getValueType().getValue());
									} else {
										detail.setValueType(1);
									}
									if (detail.getValueType() == 1) {
										detail.setValue(payDetail.getQuantityPromotion());
									} else {
										detail.setValue(payDetail.getAmountPromotion());
									}
									//detail.setValueAmount(payDetail.getAmountPromotion());
									while (t >= pItem.getBuyProducts().size()) {
										pItem.getBuyProducts().add(new ArrayList<GroupLevelDetailVO>());
									}
									pItem.getBuyProducts().get(t).add(detail);
								}
								lstGroupDetails = null;
							}
						}
					}
				}
				lstPromotions = new ArrayList<PromotionItem>(mapTmp.values());
			}
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
		return lstPromotions;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public boolean checkPromotionProgramHasPortion(String programCode, long shopId, long customerId, long staffId, Date lockDate) throws BusinessException {
		try {
			if (StringUtility.isNullOrEmpty(programCode)) {
				return false;
			}
			PromotionProgram pp = promotionProgramDAO.getPromotionProgramByCode(programCode);
			if (pp == null) {
				return false;
			}
			return promotionProgramDAO.checkPromotionProgramHasPortion(pp.getId(), shopId, customerId, staffId, lockDate);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public Integer getQuantityRemainOfPromotionProgram(long promotionId,
			long shopId, long staffId, long customerId, Date lockDate)
			throws BusinessException {
		try {
			return promotionProgramDAO.getQuantityRemainOfPromotionProgram(promotionId, shopId, staffId, customerId, lockDate);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public BigDecimal getAmountRemainOfPromotionProgram(String promotionCode,
			long shopId, long staffId, long customerId, Date lockDate)
			throws BusinessException {
		try {
			return promotionProgramDAO.getAmountRemainOfPromotionProgram(promotionCode, shopId, staffId, customerId, lockDate);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public BigDecimal getAmountRemainApprovedOfPromotionProgram(String promotionCode,
			long shopId, long staffId, long customerId, Date lockDate)
			throws BusinessException {
		try {
			return promotionProgramDAO.getAmountRemainApprovedOfPromotionProgram(promotionCode, shopId, staffId, customerId, lockDate);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Integer getNumRemainOfPromotionProgram(String promotionCode,
			long shopId, long staffId, long customerId, Date lockDate)
			throws BusinessException {
		try {
			return promotionProgramDAO.getNumRemainOfPromotionProgram(promotionCode, shopId, staffId, customerId, lockDate);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	
	@Override
	public Integer getNumRemainApprovedOfPromotionProgram(String promotionCode,
			long shopId, long staffId, long customerId, Date lockDate)
			throws BusinessException {
		try {
			return promotionProgramDAO.getNumRemainApprovedOfPromotionProgram(promotionCode, shopId, staffId, customerId, lockDate);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author tungmt
	 */
	@Override
	public void updateMD5ValidCode(PromotionProgram pp, LogInfoVO logInfo) throws BusinessException {
		try {
			if (pp == null || pp.getId() == null) {
				throw new BusinessException("PromotionProgram is not null");
			}
			List<ProgrameExtentVO> lst = promotionProgramDAO.getIdAllTableProgram(pp.getId());
			if (lst != null && lst.size() > 0) {
				String md5 = "";
				for (ProgrameExtentVO vo : lst) {
					md5 += vo.getId().toString();
					if (!StringUtility.isNullOrEmpty(vo.getUpdateDate())) {
						md5 += vo.getUpdateDate();
					}
				}
				try {
					md5 = StringUtility.generateHash(md5, pp.getPromotionProgramCode().trim().toLowerCase());
				} catch (NoSuchAlgorithmException e) {
					throw new BusinessException(e);
				} catch (UnsupportedEncodingException e) {
					throw new BusinessException(e);
				}
				pp.setMd5ValidCode(md5);
				promotionProgramDAO.updatePromotionProgram(pp, logInfo);
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<PromotionShopMap> getPromotionChildrenShopMap(PromotionShopMapFilter filter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return promotionShopMapDAO.getPromotionChildrenShopMap(filter.getPromotionId(), filter.getShopRootId(), filter.getStatus().getValue());
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<PromotionShopMap> getPromotionShopMapByCustomer(List<Long> lstId, Long promotionId) throws BusinessException{
		try {
			return promotionShopMapDAO.getPromotionShopMapByCustomer(lstId, promotionId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateGroupLevelOrderNumber(long promotionId, long productGroupId, LogInfoVO logInfo) throws BusinessException {
		try {
			promotionProgramDAO.updateGroupLevelOrderNumber(promotionId, productGroupId, logInfo);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<PromotionShopMapVO> getPromotionShopMapVOByFilter(PromotionShopMapFilter filter, KPaging<PromotionShopMapVO> kPaging) throws BusinessException {
		try {
			return promotionShopMapDAO.getPromotionShopMapVOByFilter(filter, kPaging);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public List<PromotionCustomerMap> getListPromotionCustomerMapEntity(Long customerID, PromotionShopMap promotionShopMap) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return promotionCustomerMapDAO.getListPromotionCustomerMapEntity(customerID, promotionShopMap);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public List<PromotionStaffMap> getListPromotionStaffMapAddPromotionStaff(Long promotionShopMapId, Long staffId, Long shopId) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return promotionStaffMapDAO.getListPromotionStaffMapAddPromotionStaff(promotionShopMapId, staffId, shopId);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
}
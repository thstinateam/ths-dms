package ths.dms.core.business;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.vo.ApParamVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.ApParamDAO;

public class ApParamMgrImpl implements ApParamMgr {
	@Autowired
	ApParamDAO apParamDAO;
	
	@Override
	public ApParam createApParam(ApParam apParam, LogInfoVO logInfo) throws BusinessException {
		try {
			return apParamDAO.createApParam(apParam, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateApParam(ApParam apParam, LogInfoVO logInfo) throws BusinessException {
		try {
			apParamDAO.updateApParam(apParam, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteApParam(ApParam apParam, LogInfoVO logInfo) throws BusinessException{
		try {
			apParamDAO.deleteApParam(apParam, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ApParam getApParamById(Long id) throws BusinessException{
		try {
			return id==null?null:apParamDAO.getApParamById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public List<ApParam> getListApParam(ApParamType type, ActiveType status) throws BusinessException {
		try {
			List<ApParam> lst = apParamDAO.getListApParamByType(type, status);
			return lst;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ApParam getApParamByCode(String code, ApParamType type) throws BusinessException {
		try {
			return apParamDAO.getApParamByCode(code, type);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ApParam getApParamByCodeX(String code, ApParamType type, ActiveType status) throws BusinessException {
		try {
			return apParamDAO.getApParamByCodeX(code, type, status);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ApParam getApParamByCodeEx(String code, ActiveType status) throws BusinessException {
		try {
			return apParamDAO.getApParamByCode(code, status);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ApParam> getOffDateReason()
			throws BusinessException {
		try {
			return apParamDAO.getListApParamByType(ApParamType.OFFDATE,
					ActiveType.RUNNING);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Map<String, String> getMapPromotionType() throws BusinessException {
		try {
			List<ApParam> list = apParamDAO.getListApParamByType(ApParamType.PROMOTION, ActiveType.RUNNING);
			Map<String, String> result = new HashMap<String, String>();
			for(ApParam ap : list) {
				if (ap.getApParamCode() != null) {
					ap.setApParamCode(ap.getApParamCode().toUpperCase());
				}
				if (ap.getValue() != null) {
					ap.setValue(ap.getValue().toUpperCase());
				}
				result.put(ap.getApParamCode(), ap.getValue());
			}
			return result;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ApParam> getListApParamByFilter(ApParamFilter filter)  throws BusinessException {
		try {
			return apParamDAO.getListApParamByFilter(filter);
		} catch (DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ApParamVO> getListApParamByFilterEx(ApParamFilter filter)  throws BusinessException {
		try {
			//List<ApParam> list = apParamDAO.getListApParamByFilter(filter);
			List<ApParamVO> lst = apParamDAO.getListApParamByFilter2(filter); // lacnv1 - tim kiem like
			ObjectVO<ApParamVO> vo = new ObjectVO<ApParamVO>();
			vo.setkPaging(filter.getPagingVO());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public ApParam createApParamEx(ApParam apParam, LogInfoVO logInfo) throws BusinessException {
		try {
			return apParamDAO.createApParamEx(apParam, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public ApParam updateApParamEx(ApParam apParam, LogInfoVO logInfo) throws BusinessException {
		try {
			return apParamDAO.updateApParamEx(apParam, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ApParam> getListUoms(ApParamType type) throws BusinessException {
		try {
			return apParamDAO.getListUoms(type);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ApParam> getListActiveType(ApParamType type, ActiveType... activeTypes) throws BusinessException {
		try {
			return apParamDAO.getListActiveType(type, activeTypes);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ApParam> getListApParamByType(ApParamType type,
			ActiveType status) throws BusinessException {
		try {
			return apParamDAO.getListApParamByType(type, status);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
}

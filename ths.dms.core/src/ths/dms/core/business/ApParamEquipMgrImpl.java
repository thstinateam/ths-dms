package ths.dms.core.business;
/**
 * @author vuongmq
 * @date 15/04/2015
 *  equip_param ; ApParamEquipMgrImpl cho thiet bi
 */
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ApParamEquip;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamEquipType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.vo.ApParamEquipVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.ApParamEquipDAO;

public class ApParamEquipMgrImpl implements ApParamEquipMgr {
	@Autowired
	ApParamEquipDAO apParamEquipDAO;
	
	@Override
	public ApParamEquip createApParamEquip(ApParamEquip apParamEquip, LogInfoVO logInfo) throws BusinessException {
		try {
			return apParamEquipDAO.createApParamEquip(apParamEquip, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public void deleteApParamEquip(ApParamEquip apParamEquip, LogInfoVO logInfo) throws BusinessException{
		try {
			apParamEquipDAO.deleteApParamEquip(apParamEquip, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateApParamEquip(ApParamEquip apParamEquip, LogInfoVO logInfo) throws BusinessException {
		try {
			apParamEquipDAO.updateApParamEquip(apParamEquip, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ApParamEquip getApParamEquipById(Long id) throws BusinessException{
		try {
			return id==null?null:apParamEquipDAO.getApParamEquipById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	

	@Override
	public ApParamEquip getApParamEquipByCode(String code, ApParamEquipType type) throws BusinessException {
		try {
			return apParamEquipDAO.getApParamEquipByCode(code, type);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ApParamEquip getApParamEquipByCodeX(String code, ApParamEquipType type, ActiveType status) throws BusinessException {
		try {
			return apParamEquipDAO.getApParamEquipByCodeX(code, type, status);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public ApParamEquip getApParamEquipByCodeActive(String code, ActiveType status) throws BusinessException {
		try {
			return apParamEquipDAO.getApParamEquipByCodeActive(code, status);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public List<ApParamEquip> getListApParamEquipByType(ApParamEquipType type, ActiveType status) throws BusinessException {
		try {
			List<ApParamEquip> lst = apParamEquipDAO.getListApParamEquipByType(type, status);
			return lst;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ApParamEquip> getListApParamEquipByFilter(ApParamFilter filter)  throws BusinessException {
		try {
			return apParamEquipDAO.getListApParamEquipByFilter(filter);
		} catch (DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ApParamEquipVO> getListEmailToRepairByFilter(ApParamFilter filter)  throws BusinessException {
		try {
			return apParamEquipDAO.getListEmailToRepairByFilter(filter);
		} catch (DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ApParamEquipVO> getListEmailToParentShopByFilter(ApParamFilter filter)  throws BusinessException {
		try {
			return apParamEquipDAO.getListEmailToParentShopByFilter(filter);
		} catch (DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
}

package ths.dms.core.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.CycleCountMapProduct;
import ths.dms.core.entities.CycleCountResult;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.CycleCountFilter;
import ths.dms.core.entities.enumtype.CycleCountSearchFilter;
import ths.dms.core.entities.enumtype.CycleCountType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.MapProductFilter;
import ths.dms.core.entities.vo.CycleCountDiffVO;
import ths.dms.core.entities.vo.CycleCountMapProductVOEx;
import ths.dms.core.entities.vo.CycleCountResultVO;
import ths.dms.core.entities.vo.CycleCountSearchVO;
import ths.dms.core.entities.vo.CycleCountVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.MapProductVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.CycleCountDAO;
import ths.dms.core.dao.CycleCountMapProductDAO;
import ths.dms.core.dao.CycleCountResultDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.StockTotalDAO;

/**
 * 
 * @author hungnm
 *
 */
public class CycleCountMgrImpl implements CycleCountMgr {

	@Autowired
	CycleCountDAO cycleCountDAO;
	
	@Autowired
	CycleCountMapProductDAO cycleCountMapProductDAO;
	
	@Autowired
	CycleCountResultDAO cycleCountResultDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	StockTotalDAO stockTotalDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	@Override
	public CycleCount createCycleCount(CycleCount cycleCount) throws BusinessException {
		try {
			return cycleCountDAO.createCycleCount(cycleCount);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateCycleCount(CycleCount cycleCount) throws BusinessException {
		try {
			cycleCountDAO.updateCycleCount(cycleCount);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteCycleCount(CycleCount cycleCount) throws BusinessException{
		try {
			cycleCountDAO.deleteCycleCount(cycleCount);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public CycleCount getCycleCountById(Long id) throws BusinessException{
		try {
			return id==null?null:cycleCountDAO.getCycleCountById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	/*@Override
	public CycleCount getCycleCountByCode(String code) throws BusinessException {
		try {
			return cycleCountDAO.getCycleCountByCode(code);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}*/
	
	@Override
	public CycleCount getCycleCountByCodeAndShop(String code, Long shopId) throws BusinessException {
		try {
			return cycleCountDAO.getCycleCountByCodeAndShop(code, shopId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CycleCount> getListCycleCountEx(KPaging<CycleCount> kPaging,
			String cycleCountCode, CycleCountType cycleCountStatus,
			String desc, Long shopId, Date fromDate, Date toDate, Boolean isCompleteOrOngoing, Long wareHouseId, String strListShopId) 
					throws BusinessException {
		try {
			List<CycleCount> lst = cycleCountDAO.getListCycleCount(kPaging, cycleCountCode, cycleCountStatus, desc, shopId, fromDate, toDate, isCompleteOrOngoing, wareHouseId, strListShopId);
			ObjectVO<CycleCount> vo = new ObjectVO<CycleCount>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CycleCount> getListCycleCount(KPaging<CycleCount> kPaging,
			String cycleCountCode, CycleCountType cycleCountStatus,
			String desc, Long shopId, Date fromDate, Date toDate, Long wareHouseId, String strListShopId) 
					throws BusinessException {
		try {
			List<CycleCount> lst = cycleCountDAO.getListCycleCount(kPaging, cycleCountCode, cycleCountStatus, desc, shopId, fromDate, toDate, null, wareHouseId, strListShopId);
			ObjectVO<CycleCount> vo = new ObjectVO<CycleCount>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CycleCount> getListCycleCount(CycleCountSearchFilter filter)	throws BusinessException {
		try {
			List<CycleCount> lst = cycleCountDAO.getListCycleCount(filter);
			ObjectVO<CycleCount> vo = new ObjectVO<CycleCount>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CycleCountVO> getListCycleCountVO(KPaging<CycleCountVO> kPaging,
			String cycleCountVOCode, CycleCountType cycleCountVOStatus,
			String desc, Long shopId, Date fromDate, Date toDate,Date fromCreate,Date toCreate, Long wareHouseId, String strListShopId) 
					throws BusinessException {
		try {
			List<CycleCountVO> lst = cycleCountDAO.getListCycleCountVO(kPaging, cycleCountVOCode, cycleCountVOStatus, desc, shopId, fromDate, toDate,fromCreate,toCreate, wareHouseId, strListShopId);
			ObjectVO<CycleCountVO> vo = new ObjectVO<CycleCountVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CycleCountSearchVO> getListCycleCountSearchVO(CycleCountFilter filter) throws BusinessException {
		try {
			List<CycleCountSearchVO> lst = cycleCountDAO.getListCycleCountSearchVO(filter);
			ObjectVO<CycleCountSearchVO> vo = new ObjectVO<CycleCountSearchVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkOngoingCycleCount(long shopId, long wareHouseId) throws BusinessException {
		try {
			return cycleCountDAO.checkOngoingCycleCount(shopId, wareHouseId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	//------------------------------------------------------
	
	@Override
	public CycleCountMapProduct createCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct,
			Boolean isUpdateQuantityBfrCount, LogInfoVO log) throws BusinessException {
		try {
			return cycleCountMapProductDAO.createCycleCountMapProduct(cycleCountMapProduct, isUpdateQuantityBfrCount, log);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct, Boolean isUpdateQuantityBfrCount) throws BusinessException {
		try {
			cycleCountMapProductDAO.updateCycleCountMapProduct(cycleCountMapProduct, isUpdateQuantityBfrCount, false);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct) throws BusinessException{
		try {
			List<CycleCountResult> lstCcr = cycleCountResultDAO.getListCycleCount(null, cycleCountMapProduct.getId());
			for (CycleCountResult ccr: lstCcr) {
				cycleCountResultDAO.deleteCycleCountResult(ccr);
			}
			cycleCountMapProductDAO.deleteCycleCountMapProduct(cycleCountMapProduct);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public CycleCountMapProduct getCycleCountMapProductById(Long id) throws BusinessException{
		try {
			return id==null?null:cycleCountMapProductDAO.getCycleCountMapProductById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CycleCountMapProduct> getListCycleCountMapProductByCycleCountId(
			KPaging<CycleCountMapProduct> kPaging, long cycleCountId) throws BusinessException {
		try {
			List<CycleCountMapProduct> lst = cycleCountMapProductDAO.getListCycleCountMapProductByCycleCountId(kPaging, cycleCountId);
			ObjectVO<CycleCountMapProduct> vo = new ObjectVO<CycleCountMapProduct>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CycleCountMapProduct> getListCycleCountMapProduct(KPaging<CycleCountMapProduct> kPaging, Long cycleCountId, String productCode, String productName, Long catId, Long subCatId, Integer minQuantity, Integer maxQuantity,
			Boolean isOnlyGetDifferentQuantity) throws BusinessException {
		try {
			List<CycleCountMapProduct> lst = cycleCountMapProductDAO.getListCycleCountMapProduct(kPaging, cycleCountId, productCode, productName, catId, subCatId, minQuantity, maxQuantity, isOnlyGetDifferentQuantity);
			ObjectVO<CycleCountMapProduct> vo = new ObjectVO<CycleCountMapProduct>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public Boolean createListCycleCountMapProduct(CycleCount cycleCount, 
			List<CycleCountMapProduct> lstCycleCountMapProduct, Boolean isCreatingAll, LogInfoVO log) throws BusinessException {
		try {
			return cycleCountMapProductDAO.createListCycleCountMapProduct(cycleCount, lstCycleCountMapProduct, isCreatingAll,log);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkIfCycleCountMapProductExist(long cycleCountId, String productCode, Long id) 
			throws BusinessException {
		try {
			return cycleCountMapProductDAO.checkIfRecordExist(cycleCountId, productCode, id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	
	//------------------------------------------------------
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public CycleCountResult createCycleCountResult(CycleCountResult cycleCountResult) throws BusinessException {
		try {
			return cycleCountResultDAO.createCycleCountResult(cycleCountResult);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateCycleCountResult(CycleCountResult cycleCountResult, Boolean isUpdateQuantityBfrCount) throws BusinessException {
		try {
			cycleCountResultDAO.updateCycleCountResult(cycleCountResult, isUpdateQuantityBfrCount);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void createListCycleCountResult(List<CycleCountResult> lstCycleCountResult, LogInfoVO log) throws BusinessException {
		try {
			cycleCountResultDAO.createListCycleCountResult(lstCycleCountResult, log, false);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateListCycleCountResult(List<CycleCountResultVO> lstCycleCountResultVO) throws BusinessException {
		try {
			cycleCountResultDAO.updateListCycleCountResult(lstCycleCountResultVO);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateListCycleCountMapProduct(List<CycleCountMapProduct> lstCycleCountMapProduct, LogInfoVO log) throws BusinessException {
		try {
			cycleCountMapProductDAO.updateListCycleCountMapProduct(lstCycleCountMapProduct, log);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	
	//nhap kiem kho, duyet kiem kho
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateListCycleCountDetailAndCreateListCycleCountResult(CycleCount cycleCount, List<CycleCountMapProduct> lstUpdatedCycleCountMapProduct, List<CycleCountResult> lstCycleCountResult,
			List<CycleCountMapProductVOEx> lstCycleCountMapProductVOEx, List<CycleCountResult> lstDeletedCycleCountResult, Map<Long, Integer> mapProductInsert, LogInfoVO log) throws BusinessException {
		try {
			if (cycleCount != null){
				cycleCount.setUpdateUser(log.getStaffCode());
				this.updateCycleCount(cycleCount);
			}

			if (lstDeletedCycleCountResult != null && lstDeletedCycleCountResult.size() > 0) {
				cycleCountResultDAO.deleteListCycleCountResult(lstCycleCountResult,log);
			}
			if (lstUpdatedCycleCountMapProduct != null && lstUpdatedCycleCountMapProduct.size() > 0) {
				List<CycleCountMapProduct> lstCreatedCcm = new ArrayList<CycleCountMapProduct>();
				List<CycleCountMapProduct> lstUpdatedCcm = new ArrayList<CycleCountMapProduct>();
				for (CycleCountMapProduct ccm: lstUpdatedCycleCountMapProduct) {
					if (ccm.getId() != null) {
						lstUpdatedCcm.add(ccm);
					} else {
						lstCreatedCcm.add(ccm);
					}
				}

				//update & tao moi
				if(lstUpdatedCcm.size() > 0){
					cycleCountMapProductDAO.updateListCycleCountMapProduct(lstUpdatedCcm, log);
				}
				if(lstCreatedCcm.size() > 0) {
					cycleCountMapProductDAO.createListCycleCountMapProduct(cycleCount, lstCreatedCcm, false, log);
				}
			}
			
			if (lstCycleCountResult != null && lstCycleCountResult.size() > 0) {
				//update hoac tao moi
				cycleCountResultDAO.createListCycleCountResult(lstCycleCountResult, log, false);
			}
			
			//tao moi map product, tao moi result
			if (lstCycleCountMapProductVOEx != null && lstCycleCountMapProductVOEx.size() > 0) {
				for (CycleCountMapProductVOEx vo: lstCycleCountMapProductVOEx) {
					CycleCountMapProduct ccm = cycleCountMapProductDAO.createCycleCountMapProduct(vo.getCycleCountMapProduct(), false, log);
					List<CycleCountResult> lstCcr = vo.getLstCycleCountResult();
					if (lstCcr == null || lstCcr.size() == 0)
						continue;
					for (CycleCountResult ccr: lstCcr) {
						ccr.setCycleCountMapProduct(ccm);
					}
					cycleCountResultDAO.createListCycleCountResult(lstCcr, log, true);
				}
			}
			//Xu ly cho danh sach san pham them moi
			if (mapProductInsert != null && cycleCount != null) {
				Date sysCurentDate = commonDAO.getSysDate();
				for (Entry<Long, Integer> entry : mapProductInsert.entrySet()) {
					CycleCountMapProduct obj = new CycleCountMapProduct();
					obj.setCycleCount(cycleCount);
					obj.setProduct(commonDAO.getEntityById(Product.class, entry.getKey()));
					obj.setQuantityCounted(entry.getValue());
					obj.setQuantityBeforeCount(0);
					obj.setCreateDate(sysCurentDate);
					obj.setCreateUser(log.getStaffCode());
					commonDAO.createEntity(obj);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteCycleCountResult(CycleCountResult cycleCountResult) throws BusinessException{
		try {
			cycleCountResultDAO.deleteCycleCountResult(cycleCountResult);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public CycleCountResult getCycleCountResultById(Long id) throws BusinessException{
		try {
			return id==null?null:cycleCountResultDAO.getCycleCountResultById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CycleCountResult> getListCycleCountResult(KPaging<CycleCountResult> kPaging, 
			Long cycleCountMapProductId) 
			throws BusinessException {
		try {
			List<CycleCountResult> lst = cycleCountResultDAO.getListCycleCount(kPaging, cycleCountMapProductId);
			ObjectVO<CycleCountResult> vo = new ObjectVO<CycleCountResult>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkIfCycleCountResultExist(long cycleCountMapId, String lotCode, Long id) throws BusinessException {
		try {
			return cycleCountResultDAO.checkIfRecordExist(cycleCountMapId, lotCode, id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Integer getMaxStockCardNumber(Long cycleCountId) throws BusinessException {
		try {
			return cycleCountMapProductDAO.getMaxStockCardNumber(cycleCountId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Integer getTotalStockCardNumber(Long cycleCountId) throws BusinessException {
		try {
			return cycleCountMapProductDAO.getTotalStockCardNumber(cycleCountId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkIfAnyProductCounted(Long cycleCountId)
			throws BusinessException {
		try {
			return cycleCountMapProductDAO.checkIfAnyProductCounted(cycleCountId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public CycleCountMapProduct getCycleCountMapProduct(Long cycleCountId, Long productId) 
			throws BusinessException {
		try {
			return cycleCountMapProductDAO.getCycleCountMapProduct(cycleCountId, productId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public CycleCountResult getCycleCountResult(Long cycleCountId, Long productId, String lot) 
			throws BusinessException {
		try {
			return cycleCountResultDAO.getCycleCountResult(cycleCountId, productId, lot);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CycleCountResult> getListCycleCountResultEx(
			KPaging<CycleCountResult> kPaging, Long cycleCountMapProductId,
			Long ownerId, StockObjectType ownerType, Long productId)
			throws BusinessException {
		try {
			return cycleCountResultDAO.getListCycleCountResultEx(kPaging, cycleCountMapProductId, ownerId, ownerType, productId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<CycleCountDiffVO> getListCycleCountDiff(long cycleCountId) throws BusinessException {
		try {
			return cycleCountMapProductDAO.getListCycleCountDiff(cycleCountId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author trietptm
	 */
	@Override
	public Boolean completeCheckStock(CycleCount cycleCount, LogInfoVO logInfo) throws BusinessException {
		try {
			Date curDate = commonDAO.getSysDate();
			if (cycleCountDAO.checkCompleteCheckStock(cycleCount.getId())) {
				cycleCount.setStatus(CycleCountType.WAIT_APPROVED);
				cycleCount.setUpdateUser(logInfo.getStaffCode());
				cycleCount.setUpdateDate(curDate);
			} else {
				List<CycleCountResult> lstCycleCountResult = new ArrayList<CycleCountResult>();
				cycleCount.setApprovedDate(curDate);
				cycleCount.setStatus(CycleCountType.COMPLETED);
				cycleCount.setUpdateUser(logInfo.getStaffCode());
				cycleCount.setUpdateDate(curDate);
				List<CycleCountMapProduct>  lstCCMapProduct = cycleCountMapProductDAO.getListCycleCountMapProductByCycleCountId(null, cycleCount.getId());
				if (lstCCMapProduct != null && lstCCMapProduct.size() > 0) {
					for (int i = 0, sz = lstCCMapProduct.size(); i < sz; ++i) {
						CycleCountMapProduct cycleCountMapProduct = lstCCMapProduct.get(i);
						if (cycleCountMapProduct != null) {
							CycleCountResult cycleCountResult = new CycleCountResult();
							cycleCountResult.setCountDate(cycleCountMapProduct.getCountDate());
							cycleCountResult.setCreateDate(curDate);
							cycleCountResult.setCreateUser(logInfo.getStaffCode());
							cycleCountResult.setCycleCount(cycleCount);
							cycleCountResult.setCycleCountMapProduct(cycleCountMapProduct);
							cycleCountResult.setProduct(cycleCountMapProduct.getProduct());
							cycleCountResult.setQuantityBeforeCount(cycleCountMapProduct.getQuantityBeforeCount());
							cycleCountResult.setQuantityCounted(cycleCountMapProduct.getQuantityCounted());
							cycleCountResult.setUpdateDate(curDate);
							cycleCountResult.setUpdateUser(logInfo.getStaffCode());
							lstCycleCountResult.add(cycleCountResult);
						}
					}
				}
				cycleCountResultDAO.createListCycleCountResult(lstCycleCountResult);
			}
			cycleCountDAO.updateCycleCount(cycleCount);
			
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
		return true;
	}

	
	/**
	 * @author trietptm
	 */
	@Override
	public List<String> approveStockCount(CycleCount cycleCount, List<CycleCountMapProduct> lstCCMapProductTest, LogInfoVO logInfo) throws BusinessException {
		try {
			List<String> lstProductFail = new ArrayList<String>();
			Date curDate = commonDAO.getSysDate();
			List<CycleCountResult> lstCycleCountResult = new ArrayList<CycleCountResult>();
			List<StockTotal> stockTotals = stockTotalDAO.getListStockTotalByCycleCount(cycleCount);
			if (stockTotals != null && lstCCMapProductTest != null) {
				List<StockTotal> lstStockTotalUpdate = new ArrayList<StockTotal>();
				List<StockTotal> lstStockTotalcreate = new ArrayList<StockTotal>();
				for (int i = 0; i < lstCCMapProductTest.size(); ++i) {
					CycleCountMapProduct cycleCountMapProduct = lstCCMapProductTest.get(i);
					if (cycleCountMapProduct != null && cycleCountMapProduct.getProduct() != null) {
						StockTotal stockTotal = null;
						for (int j = 0, m = stockTotals.size(); j < m; j++) {
							StockTotal st = stockTotals.get(j);
							if (st.getProduct() != null && cycleCountMapProduct.getProduct().getId().equals(st.getProduct().getId())) {
								stockTotal = st;
								break;
							}
						}
						if (stockTotal == null) {
							stockTotal = new StockTotal();
							stockTotal.setWarehouse(cycleCount.getWarehouse());
							stockTotal.setObjectType(StockObjectType.SHOP);
							stockTotal.setProduct(cycleCountMapProduct.getProduct());
							//stockTotal.setDescr();
							stockTotal.setQuantity(cycleCountMapProduct.getQuantityCounted());
							stockTotal.setAvailableQuantity(cycleCountMapProduct.getQuantityCounted());
							stockTotal.setApprovedQuantity(cycleCountMapProduct.getQuantityCounted());
							stockTotal.setCreateUser(logInfo.getStaffCode());
							stockTotal.setCreateDate(curDate);
							stockTotal.setStatus(ActiveType.RUNNING);
							if (cycleCount.getShop() != null) {
								stockTotal.setShop(cycleCount.getShop());
								stockTotal.setObjectId(cycleCount.getShop().getId());
							}
							if (cycleCount.getWarehouse() != null) {
								stockTotal.setWarehouse(cycleCount.getWarehouse());
								stockTotal.setSeqWarehouse(cycleCount.getWarehouse().getSeq());
							}
							lstStockTotalcreate.add(stockTotal);
						} else {
							int diffCount = cycleCountMapProduct.getQuantityBeforeCount() - cycleCountMapProduct.getQuantityCounted();
							stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() - diffCount);
							int quantity = stockTotal.getQuantity() - diffCount;
							if (quantity < 0) {
								lstProductFail.add(cycleCountMapProduct.getProduct().getProductName());
							}
							stockTotal.setQuantity(quantity);
							stockTotal.setUpdateDate(curDate);
							stockTotal.setUpdateUser(logInfo.getStaffCode());
							lstStockTotalUpdate.add(stockTotal);
						}
						CycleCountResult cycleCountResult = new CycleCountResult();
						cycleCountResult.setCountDate(cycleCountMapProduct.getCountDate());
						cycleCountResult.setCreateDate(curDate);
						cycleCountResult.setCreateUser(logInfo.getStaffCode());
						cycleCountResult.setCycleCount(cycleCount);
						cycleCountResult.setCycleCountMapProduct(cycleCountMapProduct);
						cycleCountResult.setProduct(cycleCountMapProduct.getProduct());
						cycleCountResult.setQuantityBeforeCount(cycleCountMapProduct.getQuantityBeforeCount());
						cycleCountResult.setQuantityCounted(cycleCountMapProduct.getQuantityCounted());
						cycleCountResult.setUpdateDate(curDate);
						cycleCountResult.setUpdateUser(logInfo.getStaffCode());
						lstCycleCountResult.add(cycleCountResult);
					}
				}
				
				if (lstProductFail.isEmpty()) {
					if (lstStockTotalUpdate != null && !lstStockTotalUpdate.isEmpty()) {
						stockTotalDAO.updateListStockTotal(lstStockTotalUpdate);
					}
					
					if (lstStockTotalcreate != null && !lstStockTotalcreate.isEmpty()) {
						stockTotalDAO.createListStockTotal(lstStockTotalcreate);
					}
				}
			}
			cycleCount.setApprovedDate(curDate);
			cycleCount.setStatus(CycleCountType.COMPLETED);
			cycleCount.setUpdateUser(logInfo.getStaffCode());
			cycleCount.setUpdateDate(curDate);
			if (lstProductFail.isEmpty()) {
				cycleCountDAO.updateCycleCount(cycleCount);
				cycleCountResultDAO.createListCycleCountResult(lstCycleCountResult);
			}
			return lstProductFail;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ProductVO> searchProductNotInCycleProductMap(KPaging<ProductVO> kPaging, Integer status, Long cycleCountId, List<Long> lstProductId, List<Long> lstProductIdExcep, String productCode, String productName) throws BusinessException {
		try {
			List<ProductVO> lst = cycleCountMapProductDAO.searchProductNotInCycleProductMap(kPaging, status, cycleCountId, lstProductId, lstProductIdExcep, productCode, productName);
			ObjectVO<ProductVO> vo = new ObjectVO<ProductVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<MapProductVO> searchCycleCountMapProductByFilter (MapProductFilter<MapProductVO> filter) throws BusinessException {
		try {
			List<MapProductVO> lst = cycleCountMapProductDAO.searchCycleCountMapProductByFilter(filter);
			ObjectVO<MapProductVO> vo = new ObjectVO<MapProductVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<MapProductVO> getCycleCountMapProductByProductInsertTemp(MapProductFilter<MapProductVO> filter, List<Long> lstProductId) throws BusinessException {
		try {
			List<MapProductVO> lst = cycleCountMapProductDAO.getCycleCountMapProductByProductInsertTemp(filter, lstProductId);
			ObjectVO<MapProductVO> vo = new ObjectVO<MapProductVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

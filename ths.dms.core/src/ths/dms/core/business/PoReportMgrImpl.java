package ths.dms.core.business;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.RptDebitMonth;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.RptDebitMonthObjectType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.core.entities.vo.rpt.RptCompareBookAndDeliveryProductVO;
import ths.core.entities.vo.rpt.RptInputProductVinamilkVO;
import ths.core.entities.vo.rpt.RptPoVnmDebitComparison1VO;
import ths.core.entities.vo.rpt.RptPoVnmDebitComparison2VO;
import ths.core.entities.vo.rpt.RptPoVnmDebitComparisonVO;
import ths.core.entities.vo.rpt.RptPoVnmVO;
import ths.core.entities.vo.rpt.RptReturnSaleOrderParentVO;
import ths.core.entities.vo.rpt.RptReturnSaleOrderVO;
import ths.core.entities.vo.rpt.Rpt_PO_CTTTVO;
import ths.core.entities.vo.rpt.Rpt_PO_CTTT_MAPPINGVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.PoAutoDetailDAO;
import ths.dms.core.dao.PoVnmDAO;
import ths.dms.core.dao.PoVnmDetailDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.RptDebitMonthDAO;
import ths.dms.core.dao.SaleOrderDAO;
import ths.dms.core.dao.SaleOrderDetailDAO;
import ths.dms.core.dao.ShopDAO;

public class PoReportMgrImpl implements PoReportMgr {

	@Autowired
	PoVnmDetailDAO poVnmDetailDAO;
	
	@Autowired
	PoVnmDAO poVnmDAO;
	
	@Autowired
	ShopDAO shopDAO;
	
	@Autowired
	RptDebitMonthDAO rptDebitMonthDAO;
	
	@Autowired
	SaleOrderDAO saleOrderDAO;
	
	@Autowired
	SaleOrderDetailDAO saleOrderDetailDAO;
	
	@Autowired
	PoAutoDetailDAO poAutoDetailDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	
	@Override
	public RptPoVnmDebitComparisonVO getRptPoVnmDebitComparisonVO(
			Long shopId, Date fromDate, Date toDate) throws BusinessException {
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			if (fromDate == null) {
				throw new IllegalArgumentException("fromDate is null");
			}
			if (toDate == null) {
				throw new IllegalArgumentException("toDate is null");
			}
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			RptPoVnmDebitComparisonVO rptPoVnmDebitComparisonVO = new RptPoVnmDebitComparisonVO();
			rptPoVnmDebitComparisonVO.shop = shop;
			rptPoVnmDebitComparisonVO.rptDebitMonth = rptDebitMonthDAO
					.getRptDebitMonthByOwner(shopId,
							RptDebitMonthObjectType.SHOP, toDate);
			List<RptPoVnmDebitComparison2VO> lst = poVnmDAO
					.getListRptPoVnmDebitComparison2VO(shopId, fromDate, toDate);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			RptPoVnmDebitComparison1VO rptPoVnmDebitComparison1VO = new RptPoVnmDebitComparison1VO();
			String poVnmDate = "";
			for (RptPoVnmDebitComparison2VO rptPoVnmDebitComparison2VO : lst) {
				String date = sdf.format(rptPoVnmDebitComparison2VO
						.getPoVnmDate());
				if (!poVnmDate.endsWith(date)) {
					poVnmDate = date;
					//
					rptPoVnmDebitComparison1VO = new RptPoVnmDebitComparison1VO();
					rptPoVnmDebitComparison1VO.setPoVnmDate(poVnmDate);
					//
					rptPoVnmDebitComparisonVO
							.getLstRptPoVnmDebitComparison1VO().add(
									rptPoVnmDebitComparison1VO);
				}
				rptPoVnmDebitComparison1VO.getLstRptPoVnmDebitComparison2VO()
						.add(rptPoVnmDebitComparison2VO);
			}
			return rptPoVnmDebitComparisonVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptDebitMonth getRptDebitMonthByOwner(Long ownerId, RptDebitMonthObjectType ownerType, Date date)
			throws BusinessException {
		try {
			if (ownerId == null || ownerId <= 0) {
				throw new IllegalArgumentException("ownerId is null or <= 0");
			}
			if (ownerType == null) {
				throw new IllegalArgumentException("ownerType is null");
			}
			if (date == null) {
				throw new IllegalArgumentException("date is null");
			}
			return rptDebitMonthDAO.getRptDebitMonthByOwner(ownerId, ownerType, date);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author hungnm
	 */
	@Override
	public ObjectVO<RptPoVnmVO> getListRptPoVnmVO(KPaging<RptPoVnmVO> kPaging,
			Date fromDate, Date toDate, Long shopId) throws BusinessException {
		try {
			ObjectVO<RptPoVnmVO> vo = new ObjectVO<RptPoVnmVO>();
			List<RptPoVnmVO> lst = poVnmDAO.getListRptPoVnmVO(kPaging, fromDate, toDate, shopId);
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}


	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoReportMgr#getListChangeReturnSaleOrder(java.lang.Long, java.util.Date, java.util.Date, java.lang.Long)
	 */
	@Override
	public List<RptReturnSaleOrderParentVO> getListChangeReturnSaleOrder(
			Long shopId, Date fromDate, Date toDate, Long deliveryId)
			throws BusinessException {
		// TODO Auto-generated method stub
		List<RptReturnSaleOrderParentVO> listReturn = new ArrayList<RptReturnSaleOrderParentVO>();
		try {
			String orderNumber = "";
			List<RptReturnSaleOrderVO> listTemp = new ArrayList<RptReturnSaleOrderVO>();
			List<RptReturnSaleOrderVO> list = saleOrderDetailDAO.getListChangeReturnSaleOrder(shopId, fromDate, toDate, deliveryId);
			for (int i = 0, length = list.size(); i < length; i++) {
				RptReturnSaleOrderVO item = list.get(i);
				if (i == 0) {
					orderNumber = item.getOrderNumber();
					listTemp.add(item);
				} else {
					if (item.getOrderNumber().equals(orderNumber)) {
						listTemp.add(item);
					} else {
						//add mot item cha
						RptReturnSaleOrderParentVO parent = new RptReturnSaleOrderParentVO();
						parent.setOrderNumber(orderNumber);
						parent.setListRptReturnVO(listTemp);
						listReturn.add(parent);
						//reset du lieu
						listTemp.clear();
						orderNumber = item.getOrderNumber();
						listTemp.add(item);
					}
					if (i == length - 1) {
						RptReturnSaleOrderParentVO parent = new RptReturnSaleOrderParentVO();
						parent.setOrderNumber(orderNumber);
						parent.setListRptReturnVO(listTemp);
						listReturn.add(parent);
					}
				}
			}
		} catch (DataAccessException e) {
			// TODO: handle exception
			throw new BusinessException(e);
		}
		return listReturn;
	}


	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoReportMgr#getListRptImportProduct(java.lang.Long, java.util.Date, java.util.Date)
	 */
	@Override
	public List<RptInputProductVinamilkVO> getListRptImportProduct(Long shopId,
			Date fromDate, Date toDate) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return poVnmDetailDAO.getListRptImportProduct(shopId, fromDate, toDate);
		} catch (DataAccessException e) {
			// TODO: handle exception
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoReportMgr#getListCompareBookAndDeliveryProduct(java.lang.Long, java.util.Date, java.util.Date)
	 */
	@Override
	public List<RptCompareBookAndDeliveryProductVO> getListCompareBookAndDeliveryProduct(
			Long shopId, Date fromDate, Date toDate) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return productDAO.getListCompareBookAndDeliveryProduct(shopId, fromDate, toDate);
		} catch (DataAccessException e) {
			// TODO: handle exception
			throw new BusinessException(e);
		}
	}
	

}

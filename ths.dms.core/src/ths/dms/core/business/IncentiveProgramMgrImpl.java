package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.IncentiveProgram;
import ths.dms.core.entities.IncentiveProgramDetail;
import ths.dms.core.entities.IncentiveProgramLevel;
import ths.dms.core.entities.IncentiveShopMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.IncentiveProgramDetailFilter;
import ths.dms.core.entities.enumtype.IncentiveProgramDetailVO;
import ths.dms.core.entities.enumtype.IncentiveProgramFilter;
import ths.dms.core.entities.enumtype.IncentiveProgramLevelFilter;
import ths.dms.core.entities.enumtype.IncentiveProgramLevelVO;
import ths.dms.core.entities.enumtype.IncentiveProgramVO;
import ths.dms.core.entities.enumtype.IncentiveShopMapFilter;
import ths.dms.core.entities.enumtype.IncentiveShopMapVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.IncentiveProgramDAO;
import ths.dms.core.dao.IncentiveProgramDetailDAO;
import ths.dms.core.dao.IncentiveProgramLevelDAO;
import ths.dms.core.dao.IncentiveShopMapDAO;
import ths.dms.core.dao.ShopDAO;

public class IncentiveProgramMgrImpl implements IncentiveProgramMgr {
/*
	@Autowired
	IncentiveProgramDAO incentiveProgramDAO;
	
	@Autowired
	IncentiveProgramDetailDAO incentiveProgramDetailDAO;
	
	@Autowired
	IncentiveProgramLevelDAO incentiveProgramLevelDAO;
	
	@Autowired
	IncentiveShopMapDAO incentiveShopMapDAO;
	
	@Autowired
	ShopDAO shopDAO;
	
	@Autowired
	CommonMgr commonMgr;
	
	@Override
	public IncentiveProgram createIncentiveProgram(
			IncentiveProgram incentiveProgram, LogInfoVO logInfo)
			throws BusinessException {
		try {
			return incentiveProgramDAO.createIncentiveProgram(incentiveProgram, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteIncentiveProgram(IncentiveProgram incentiveProgram,
			LogInfoVO logInfo) throws BusinessException {
		try {
			incentiveProgramDAO.deleteIncentiveProgram(incentiveProgram, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
		
	}

	@Override
	public void updateIncentiveProgram(IncentiveProgram incentiveProgram,
			LogInfoVO logInfo) throws BusinessException {
		try {
//			if(!this.checkUpdateIncentiveProgramForActive(incentiveProgram.getId()).equals(ExceptionCode.ERR_ACTIVE_PROGRAM_OK)){
//				throw new IllegalArgumentException("CTKT khong co sp, don vi, muc");
//			}
			incentiveProgramDAO.updateIncentiveProgram(incentiveProgram, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	*//**
	 * Kiem tra khi cap nhat CTKT sang hoat dong
	 * @author thongnm
	 *//*
	@Override
	public String checkUpdateIncentiveProgramForActive(Long incentiveProgramId)
			throws BusinessException {
		try {
			List<IncentiveShopMap> listIncentiveShopMap = null;
			List<IncentiveProgramDetail> listIncentiveProgramDetail = null;
			List<IncentiveProgramLevel> listIncentiveProgramLevel = null;
			
			listIncentiveShopMap = incentiveShopMapDAO.getListIncentiveShopMap(incentiveProgramId, ActiveType.RUNNING, null);
			listIncentiveProgramDetail = incentiveProgramDetailDAO.getListIncentiveProgramDetail(incentiveProgramId, ActiveType.RUNNING, null);
			listIncentiveProgramLevel = incentiveProgramLevelDAO.getListIncentiveProgramLevel(incentiveProgramId, ActiveType.RUNNING, null);
			
			if(listIncentiveShopMap == null || listIncentiveShopMap.size() <= 0){
				return ExceptionCode.ERR_ACTIVE_INCENTIVE_PROGRAM_DVTG;
			}
			if(listIncentiveProgramDetail == null || listIncentiveProgramDetail.size() <= 0){
				return ExceptionCode.ERR_ACTIVE_INCENTIVE_PROGRAM_SPKT;
			}
			if(listIncentiveProgramLevel == null || listIncentiveProgramLevel.size() <= 0){
				return ExceptionCode.ERR_ACTIVE_INCENTIVE_PROGRAM_MUC;
			}
			return ExceptionCode.ERR_ACTIVE_PROGRAM_OK;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public IncentiveProgram getIncentiveProgramById(long id)
			throws BusinessException {
		try {
			return incentiveProgramDAO.getIncentiveProgramById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public IncentiveProgramDetail createIncentiveProgramDetail(
			IncentiveProgramDetail incentiveProgramDetail, LogInfoVO logInfo)
			throws BusinessException {
		try {
			return incentiveProgramDetailDAO.createIncentiveProgramDetail(incentiveProgramDetail, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteIncentiveProgramDetail(
			IncentiveProgramDetail incentiveProgramDetail, LogInfoVO logInfo)
			throws BusinessException {
		try {
			incentiveProgramDetailDAO.deleteIncentiveProgramDetail(incentiveProgramDetail, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateIncentiveProgramDetail(
			IncentiveProgramDetail incentiveProgramDetail, LogInfoVO logInfo)
			throws BusinessException {
		try {
			incentiveProgramDetailDAO.updateIncentiveProgramDetail(incentiveProgramDetail, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public IncentiveProgramDetail getIncentiveProgramDetailById(long id)
			throws BusinessException {
		try {
			return incentiveProgramDetailDAO.getIncentiveProgramDetailById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public IncentiveProgramLevel createIncentiveProgramLevel(
			IncentiveProgramLevel incentiveProgramLevel, LogInfoVO logInfo)
			throws BusinessException {
		try {
			return incentiveProgramLevelDAO.createIncentiveProgramLevel(incentiveProgramLevel, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteIncentiveProgramLevel(
			IncentiveProgramLevel incentiveProgramLevel, LogInfoVO logInfo)
			throws BusinessException {
		try {
			incentiveProgramLevelDAO.deleteIncentiveProgramLevel(incentiveProgramLevel, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateIncentiveProgramLevel(
			IncentiveProgramLevel incentiveProgramLevel, LogInfoVO logInfo)
			throws BusinessException {
		try {
			incentiveProgramLevelDAO.updateIncentiveProgramLevel(incentiveProgramLevel, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public IncentiveProgramLevel getIncentiveProgramLevelById(long id)
			throws BusinessException {
		try {
			return incentiveProgramLevelDAO.getIncentiveProgramLevelById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public IncentiveShopMap createIncentiveShopMap(
			IncentiveShopMap incentiveShopMap, LogInfoVO logInfo)
			throws BusinessException {
		try {
			return incentiveShopMapDAO.createIncentiveShopMap(incentiveShopMap, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void deleteIncentiveShopMap(Long shopId, Long incentiveProgramId,
			LogInfoVO logInfo) throws BusinessException {
		this.deleteOneIncentiveShopMap(shopId, incentiveProgramId, logInfo);
	}
	

	private void deleteOneIncentiveShopMap(Long shopId, Long incentiveProgramId, LogInfoVO logInfo) throws BusinessException {
		try {
			Shop sh = shopDAO.getShopById(shopId);
			if (sh == null) throw new BusinessException("shopId is not exist");
			
			List<IncentiveShopMap> listIncentiveShopMap = incentiveShopMapDAO.getListIncentiveChildShopMapWithShopAndIncentiveProgram(shopId, incentiveProgramId);
			for (IncentiveShopMap psm: listIncentiveShopMap) {
				psm.setStatus(ActiveType.DELETED);
				incentiveShopMapDAO.updateIncentiveShopMap(psm, logInfo);
			}
			//kiem tra cha no coi co con chau gi tham gia khong, neu khong thi them cha no vao
			if (sh.getParentShop() != null) {
				listIncentiveShopMap = incentiveShopMapDAO.getListIncentiveChildShopMapWithShopAndIncentiveProgram(sh.getParentShop().getId(), incentiveProgramId);
				if (listIncentiveShopMap.size() == 0) {
					IncentiveShopMap psm = new IncentiveShopMap();
					psm.setShop(sh.getParentShop());
					psm.setIncentiveProgram(incentiveProgramDAO.getIncentiveProgramById(incentiveProgramId));
					psm.setStatus(ActiveType.RUNNING);
					psm.setCreateUser(logInfo.getStaffCode());
					psm.setCreateDate(commonMgr.getSysDate());
					incentiveShopMapDAO.createIncentiveShopMap(psm, logInfo);
				}
			}
			
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateIncentiveShopMap(IncentiveShopMap incentiveShopMap,
			LogInfoVO logInfo) throws BusinessException {
		try {
			incentiveShopMapDAO.updateIncentiveShopMap(incentiveShopMap, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public IncentiveShopMap getIncentiveShopMapById(long id)
			throws BusinessException {
		try {
			return incentiveShopMapDAO.getIncentiveShopMapById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<IncentiveProgramDetail> getListIncentiveProgramDetail(
			Long incentiveProgramId, ActiveType status,
			KPaging<IncentiveProgramDetail> kPaging) throws BusinessException {
		try {
			ObjectVO<IncentiveProgramDetail> res = new ObjectVO<IncentiveProgramDetail>();

			List<IncentiveProgramDetail> lst = incentiveProgramDetailDAO.getListIncentiveProgramDetail(
					incentiveProgramId, status, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<IncentiveProgramLevel> getListIncentiveProgramLevel(Long incentiveProgramId,
			ActiveType status, KPaging<IncentiveProgramLevel> kPaging)
			throws BusinessException {
		try {
			ObjectVO<IncentiveProgramLevel> res = new ObjectVO<IncentiveProgramLevel>();

			List<IncentiveProgramLevel> lst = incentiveProgramLevelDAO.getListIncentiveProgramLevel(
					incentiveProgramId, status, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
			
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<IncentiveShopMap> getListIncentiveShopMap(Long incentiveProgramId,
			ActiveType status, KPaging<IncentiveShopMap>  kPaging)
			throws BusinessException {
		try {
			ObjectVO<IncentiveShopMap> res = new ObjectVO<IncentiveShopMap>();
			
			List<IncentiveShopMap> lst = incentiveShopMapDAO.getListIncentiveShopMap(incentiveProgramId, status, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
			
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<IncentiveProgramVO> getListIncentiveProgramVO(
			IncentiveProgramFilter filter, KPaging<IncentiveProgramVO> kPaging)
			throws BusinessException {
		try {
			return incentiveProgramDAO.getListIncentiveProgramVO(filter, kPaging);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<IncentiveProgramLevelVO> getListIncentiveProgramLevelVO(
			IncentiveProgramLevelFilter filter,
			KPaging<IncentiveProgramLevelVO> kPaging) throws BusinessException {
		try {
			ObjectVO<IncentiveProgramLevelVO> res = new ObjectVO<IncentiveProgramLevelVO>();

			List<IncentiveProgramLevelVO> lst = incentiveProgramLevelDAO.getListIncentiveProgramLevelVO(filter, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<IncentiveProgramDetailVO> getListIncentiveProgramDetailVO(
			IncentiveProgramDetailFilter filter,
			KPaging<IncentiveProgramDetailVO> kPaging) throws BusinessException {
		try {
			ObjectVO<IncentiveProgramDetailVO> res = new ObjectVO<IncentiveProgramDetailVO>();

			List<IncentiveProgramDetailVO> lst = incentiveProgramDetailDAO.getListIncentiveProgramDetailVO(filter, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<IncentiveShopMapVO> getListIncentiveShopMapVO(
			IncentiveShopMapFilter filter, KPaging<IncentiveShopMapVO> kPaging)
			throws BusinessException {
		try {
			ObjectVO<IncentiveShopMapVO> res = new ObjectVO<IncentiveShopMapVO>();

			List<IncentiveShopMapVO> lst = incentiveShopMapDAO.getListIncentiveShopMapVO(filter, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Shop> getListShopNotJoinIncentiveProgram(Long incentiveProgramId,
			KPaging<Shop>  kPaging) throws BusinessException {
		try {
			ObjectVO<Shop> res = new ObjectVO<Shop>();

			List<Shop> lst = incentiveShopMapDAO.getListShopNotJoinIncentiveProgram(incentiveProgramId, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
			
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean isExistProduct(Long incentiveProgramId, Long productId, Date fromDate, Date toDate, Long exceptedId)
			throws BusinessException {
		try {
			return incentiveProgramDetailDAO.isExistProduct(incentiveProgramId, productId, fromDate, toDate, exceptedId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean isExistShop(Long incentiveProgramId, Long shopId) throws BusinessException {
		try {
			return incentiveShopMapDAO.isExistShop(incentiveProgramId, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public IncentiveProgram getIncentiveProgramByCode(String code)
			throws BusinessException {
		try {
			return incentiveProgramDAO.getIncentiveProgramByCode(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<IncentiveShopMap> getListAncestorIncentiveShopMap(Long incentiveProgramId,
			Long shopId) throws BusinessException {
		try {
			return incentiveShopMapDAO.getListAncestorIncentiveShopMap(incentiveProgramId, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean isExistCategory(Long incentiveProgramId, Long catId,
			Date fromDate, Date toDate) throws BusinessException {
		try {
			return incentiveProgramDetailDAO.isExistCategory(incentiveProgramId, catId, fromDate, toDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkIfAncestorInIp(Long shopId, Long incentiveProgramId) throws BusinessException {
		try {
			return incentiveShopMapDAO.checkIfAncestorInIp(shopId, incentiveProgramId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional (rollbackFor = Exception.class)
	public Boolean createListIncentiveShopMap(List<IncentiveShopMap> lstIncentiveShopMap, LogInfoVO logInfo) throws BusinessException {
		for (IncentiveShopMap child: lstIncentiveShopMap) {
			try {
				incentiveShopMapDAO.createIncentiveShopMap(child, logInfo);
			} catch (DataAccessException e) {
				throw new BusinessException(e);
			}
		}
		return true;
	}
	
	@Override
	public IncentiveProgramLevel getIncentiveProgramLevelByIncentiveProCode(
			Long incentiveProgramId, String incentiveProCode, String levelCode,
			ActiveType activeType) throws BusinessException {
		try {
			return incentiveProgramLevelDAO.getIncentiveProgramLevelByIncentiveProCode(incentiveProgramId, incentiveProCode, levelCode, activeType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public IncentiveProgram copyIncentiveProgram(Long incentiveProgramId,
			LogInfoVO logInfo, String ipCode, String ipName)
			throws BusinessException {
		if (StringUtility.isNullOrEmpty(ipCode)) {
			throw new IllegalArgumentException("incentive programe code is empty");
		}
		if (StringUtility.isNullOrEmpty(ipName)) {
			throw new IllegalArgumentException("incentive programe name is empty");
		}

		IncentiveProgram originalIP = this.getIncentiveProgramById(incentiveProgramId);
		IncentiveProgram newIP = new IncentiveProgram();

		// incentive_program_id
		newIP.setCreateDate(DateUtility.now());
		newIP.setFromDate(originalIP.getFromDate());
		newIP.setMultiple(originalIP.getMultiple());
		newIP.setObjectType(originalIP.getObjectType());
		newIP.setRecursive(originalIP.getRecursive());
		newIP.setIncentiveProgramCode(ipCode.toUpperCase());
		newIP.setIncentiveProgramName(ipName);
		newIP.setStatus(ActiveType.WAITING);
		newIP.setType(originalIP.getType());
		newIP.setToDate(originalIP.getToDate());
		newIP.setCreateUser(logInfo.getStaffCode());


		List<IncentiveShopMap> listIncentiveShopMap = null;
		List<IncentiveProgramDetail> listIncentiveProgramDetail = null;
		List<IncentiveProgramLevel> listIncentiveProgramLevel = null;
		
		try {
			listIncentiveShopMap = incentiveShopMapDAO.getListIncentiveShopMap(incentiveProgramId, ActiveType.RUNNING, null);
			listIncentiveProgramDetail = incentiveProgramDetailDAO.getListIncentiveProgramDetail(incentiveProgramId, ActiveType.RUNNING, null);
			listIncentiveProgramLevel = incentiveProgramLevelDAO.getListIncentiveProgramLevel(incentiveProgramId, ActiveType.RUNNING, null);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		
		newIP = this.createIncentiveProgram(newIP, logInfo);

		if(listIncentiveShopMap != null && listIncentiveShopMap.size() > 0){
			for (IncentiveShopMap child : listIncentiveShopMap) {
				IncentiveShopMap newIncentiveShopMap = new IncentiveShopMap();
				newIncentiveShopMap.setCreateDate(DateUtility.now());
				newIncentiveShopMap.setShop(child.getShop());
				newIncentiveShopMap.setStatus(child.getStatus());
	
				newIncentiveShopMap.setIncentiveProgram(newIP);
				newIncentiveShopMap.setCreateUser(logInfo.getStaffCode());
	
				try {
					newIncentiveShopMap = incentiveShopMapDAO.createIncentiveShopMap(newIncentiveShopMap, logInfo);
				} catch (DataAccessException ex) {
					throw new BusinessException(ex);
				}
			}
		}

		if(listIncentiveProgramDetail != null && listIncentiveProgramDetail.size() > 0){
			for (IncentiveProgramDetail child : listIncentiveProgramDetail) {
				IncentiveProgramDetail newObj = new IncentiveProgramDetail();
				newObj.setCreateDate(DateUtility.now());
				newObj.setCreateUser(logInfo.getStaffCode());
				newObj.setIncentiveProgram(newIP);
				newObj.setProduct(child.getProduct());
				newObj.setStatus(child.getStatus());
				
				try {
					newObj = incentiveProgramDetailDAO.createIncentiveProgramDetail(
							newObj, logInfo);
				} catch (DataAccessException ex) {
					throw new BusinessException(ex);
				}
			}
		}
		
		if(listIncentiveProgramLevel != null && listIncentiveProgramLevel.size() > 0){
			for (IncentiveProgramLevel child : listIncentiveProgramLevel) {
				IncentiveProgramLevel newObj = new IncentiveProgramLevel();
				newObj.setCreateDate(DateUtility.now());
				newObj.setIncentiveProgram(newIP);
				newObj.setAmount(child.getAmount());
				newObj.setFreeAmount(child.getFreeAmount());
				newObj.setFreeProduct(child.getFreeProduct());
				newObj.setFreeQuantity(child.getFreeQuantity());
				newObj.setIncentiveProgram(newIP);
				newObj.setStatus(child.getStatus());
				newObj.setCreateUser(logInfo.getStaffCode());
				try {
					newObj = incentiveProgramLevelDAO.createIncentiveProgramLevel(
							newObj, logInfo);
				} catch (DataAccessException ex) {
					throw new BusinessException(ex);
				}
			}
		}

		return newIP;
	}

	@Override
	public Boolean isDuplicateProductByIncentiveProgram(
			IncentiveProgram incentiveProgram) throws BusinessException {
		try {
			return incentiveProgramDAO.isDuplicateProductByIncentiveProgram(incentiveProgram);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public Boolean isExistChildJoinProgram(String shopCode, Long incentiveProgramId)
			throws BusinessException{
		try {
			return incentiveShopMapDAO.isExistChildJoinProgram(shopCode, incentiveProgramId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}*/
	
}

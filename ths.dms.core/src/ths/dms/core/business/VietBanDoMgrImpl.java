package ths.dms.core.business;

import org.springframework.beans.factory.annotation.Autowired;

import vn.kunkun.vietbando.LatLng;
import vn.kunkun.vietbando.VietBanDoWs;

import ths.dms.core.entities.RequestLog;
import ths.dms.core.entities.enumtype.RequestAPI;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.core.dao.RequestLogDAO;

/**
 * The Class AuthenticationMgrImpl.
 */
public class VietBanDoMgrImpl implements VietBanDoMgr {

	@Autowired
	RequestLogDAO requestLogDAO;
	
	@Override
	public LatLng fromStreetToLatLng(String sStreetName, RequestLog requestLog)
			throws BusinessException {
		try {
			VietBanDoWs.init("127-35-93-135-68-177-86-224-157-147-124-146-174-233-128-35-91-163-251-31-4-96-134-252-171-251-158-254-202-38-177-135-107-196-129-157-238-127-62-33-14-124-206-161-245-250-35-142-82-233-223-209-179-94-129-141-102-45-58-60-86-225-207-121-134-123-212-31-114-138-151-221", 60000);
			requestLog.setApi(RequestAPI.FROM_STREET_TO_LAT_LONG);
			requestLogDAO.createRequestLog(requestLog);
			return VietBanDoWs.fromStreetToLatLng(sStreetName);
		} catch (Exception e) {
			throw new BusinessException("Failed to fromStreetToLatLng: "
					+ e.getMessage(), e);
		}
	}
}

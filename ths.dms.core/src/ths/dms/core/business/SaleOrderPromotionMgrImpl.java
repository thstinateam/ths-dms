package ths.dms.core.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SaleOrderPromotion;
import ths.dms.core.entities.filter.SaleOrderPromotionFilter;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.SaleOrderPromotionDAO;

public class SaleOrderPromotionMgrImpl implements SaleOrderPromotionMgr {
	@Autowired
	SaleOrderPromotionDAO saleOrderPromotionDAO;

	@Override
	public List<SaleOrderPromotion> getSaleOrderPromotions(
			SaleOrderPromotionFilter filter) throws BusinessException {
		try {
			return saleOrderPromotionDAO.getSaleOrderPromotions(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

}

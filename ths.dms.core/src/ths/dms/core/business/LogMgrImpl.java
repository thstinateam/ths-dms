package ths.dms.core.business;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.ls.LSException;

import ths.dms.core.entities.ActionAudit;
import ths.dms.core.entities.ActionAuditDetail;
import ths.dms.core.entities.enumtype.ActionAuditType;
import ths.dms.core.entities.enumtype.ActionType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.core.entities.vo.rpt.RptLogDiff1VO;
import ths.core.entities.vo.rpt.RptLogDiffVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.core.common.utils.ColumnName2VN;
import ths.dms.core.dao.ActionGeneralLogDAO;
import ths.dms.core.dao.ActionGeneralLogDetailDAO;

public class LogMgrImpl implements LogMgr {
	
	@Autowired
	ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Autowired
	ActionGeneralLogDetailDAO actionGeneralLogDetailDAO;
	
	@Override
	public ObjectVO<ActionAudit> getListActionGeneralLog(KPaging<ActionAudit> kPaging, String staffCode,
			Long customerId, Long staffId, Date fromDate, Date toDate) throws BusinessException {
		try {
			ObjectVO<ActionAudit> vo = new ObjectVO<ActionAudit>();
			vo.setkPaging(kPaging);
			vo.setLstObject(actionGeneralLogDAO.getListActionGeneralLog(kPaging, staffCode, customerId, staffId, fromDate, toDate));
			return vo;
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<ActionAuditDetail> getListActionGeneralLogDetail(KPaging<ActionAuditDetail> kPaging, 
			Long actionGeneralLogId) throws BusinessException {
		try {
			ObjectVO<ActionAuditDetail> vo = new ObjectVO<ActionAuditDetail>();
			vo.setkPaging(kPaging);
			List<ActionAuditDetail> lstActionAuditDetails = actionGeneralLogDetailDAO.getListActionGeneralLogDetail(kPaging, actionGeneralLogId);
			if(lstActionAuditDetails != null){
	   			for(int j = 0 ; j < lstActionAuditDetails.size(); ++ j){
	   				String columnNameVN = ColumnName2VN.getVNName(lstActionAuditDetails.get(j).getColumnName());
	   				lstActionAuditDetails.get(j).setColumnName(columnNameVN);
	   			}
	   		}
			vo.setLstObject(lstActionAuditDetails);
			return vo;
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.LogMgr#getListActionAudit(ths.dms.core.entities.enumtype.KPaging, java.lang.String, ths.dms.core.entities.enumtype.ActionAuditType, java.util.Date, java.util.Date)
	 */
	@Override
	public ObjectVO<ActionAudit> getListActionAudit(KPaging<ActionAudit> kPaging,
			String staffCode, Long objectId, ActionAuditType type, Date fromDate, Date toDate)
			throws BusinessException {
		// TODO Auto-generated method stub
		try {
			ObjectVO<ActionAudit> vo = new ObjectVO<ActionAudit>();
			vo.setkPaging(kPaging);
			vo.setLstObject(actionGeneralLogDAO.getListActionAudit(kPaging, staffCode, objectId, type, fromDate, toDate));
			return vo;
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<ActionAuditDetail> getListActionAuditDetail(KPaging<ActionAuditDetail> kPaging, 
			Long actionAuditId) throws BusinessException {
		try {
			ObjectVO<ActionAuditDetail> vo = new ObjectVO<ActionAuditDetail>();
			vo.setkPaging(kPaging);
			List<ActionAuditDetail> lstActionAuditDetails = actionGeneralLogDetailDAO.getListActionGeneralLogDetail(kPaging, actionAuditId);
			if(lstActionAuditDetails != null){
	   			for(int j = 0 ; j < lstActionAuditDetails.size(); ++ j){
	   				String columnNameVN = ColumnName2VN.getVNName(lstActionAuditDetails.get(j).getColumnName());
	   				lstActionAuditDetails.get(j).setColumnName(columnNameVN);
	   			}
	   		}
			vo.setLstObject(lstActionAuditDetails);
			return vo;
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public RptLogDiffVO getRptLogDiffVO(Long objectId, ActionAuditType type, Date fromDate, Date toDate)throws BusinessException {
		try {
			if (objectId == null) {
				throw new IllegalArgumentException("objectId is null");
			}
			if (type == null) {
				throw new IllegalArgumentException("type is null");
			}
			RptLogDiffVO rptLogDiffVO = new RptLogDiffVO();
			RptLogDiff1VO rptLogDiff1VO = null;
			List<ActionAudit> lstActionAudit = null;
			if(type.equals(ActionAuditType.STAFF)){
				lstActionAudit = actionGeneralLogDAO.getListActionGeneralLog(null, null, null, objectId, fromDate, toDate);
			} else if(type.equals(ActionAuditType.CUSTOMER)) {
				lstActionAudit = actionGeneralLogDAO.getListActionGeneralLog(null, null, objectId, null, fromDate, toDate);
			} else{
				lstActionAudit = actionGeneralLogDAO.getListActionAudit(null,null,objectId,type,fromDate,toDate);
			}
			if(lstActionAudit!= null){
			   	for(int i = 0 ; i< lstActionAudit.size() ;++i){
			   		rptLogDiff1VO = new RptLogDiff1VO();
			   		rptLogDiff1VO.setActionUser(lstActionAudit.get(i).getActionUser());
			   		rptLogDiff1VO.setActionIp(lstActionAudit.get(i).getActionIp());
			   		rptLogDiff1VO.setActionType(convertActionType(lstActionAudit.get(i).getActionType()));
			   		rptLogDiff1VO.setActionDate(convertActionDate(lstActionAudit.get(i).getActionDate()));
			   		List<ActionAuditDetail> lstActionAuditDetails = actionGeneralLogDetailDAO.getListActionGeneralLogDetail(null, lstActionAudit.get(i).getId());
			   		if(lstActionAuditDetails != null){
			   			for(int j = 0 ; j < lstActionAuditDetails.size(); ++ j){
			   				String columnNameVN = ColumnName2VN.getVNName(lstActionAuditDetails.get(j).getColumnName());
			   				lstActionAuditDetails.get(j).setColumnName(columnNameVN);
			   			}
			   			rptLogDiff1VO.setLstActionAuditDetail(lstActionAuditDetails);
			   		}
			   		rptLogDiffVO.getLstRptLogDiff1VO().add(rptLogDiff1VO);
			   	}
		    }
			return rptLogDiffVO;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	private static String convertActionType(ActionType type){
		if(type.getValue().equals(ActionType.INSERT.getValue())){
			return "INSERT";
		} 
		if(type.getValue().equals(ActionType.UPDATE.getValue())){
			return "UPDATE";
		}
		if(type.getValue().equals(ActionType.DELETE.getValue())){
			return "DELETE";
		}
		return null;
	}
	private static String convertActionDate(Date actionDate){
		String dateString = "";
		if (actionDate == null) return dateString;
		Object[] params = new Object[] { actionDate };
		try {
			dateString = MessageFormat.format("{0,date," + "dd/MM/yyyy" + "}", params);
		} catch (Exception e) {
		}
		return dateString;
	}
}

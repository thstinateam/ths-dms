package ths.dms.core.business;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.CustomerAttribute;
import ths.dms.core.entities.CustomerAttributeDetail;
import ths.dms.core.entities.CustomerAttributeEnum;
import ths.dms.core.entities.CustomerCatLevel;
import ths.dms.core.entities.CustomerPositionLog;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActionType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ParamDAOGetCustomerCatLevelImportVO;
import ths.dms.core.entities.enumtype.ParamsDAOGetListCustomer;
import ths.dms.core.entities.filter.CustomerFilter;
import ths.dms.core.entities.vo.Customer4SaleVO;
import ths.dms.core.entities.vo.CustomerCatLevelImportVO;
import ths.dms.core.entities.vo.CustomerExportVO;
import ths.dms.core.entities.vo.CustomerGTVO;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.DuplicatedCustomerVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.TreeExVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.core.entities.vo.rpt.RptCustomerByProductCatDataVO;
import ths.core.entities.vo.rpt.RptCustomerByProductCatLv01VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.Constant;
import ths.dms.core.common.utils.MathUtil;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.ApParamDAO;
import ths.dms.core.dao.AttributeDAO;
import ths.dms.core.dao.AttributeDetailDAO;
import ths.dms.core.dao.AttributeValueDAO;
import ths.dms.core.dao.AttributeValueDetailDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.CustomerAttributeDAO;
import ths.dms.core.dao.CustomerCatLevelDAO;
import ths.dms.core.dao.CustomerDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.PromotionProgramDAO;
import ths.dms.core.dao.RoutingCustomerDAO;
import ths.dms.core.dao.StaffDAO;

public class CustomerMgrImpl implements CustomerMgr {
	
	@Autowired
	CustomerDAO customerDAO;
		
	@Autowired
	ShopMgr shopMgr;
	
	@Autowired
	StaffDAO staffDAO;
	
	@Autowired
	AttributeValueDAO attributeValueDAO;
	
	@Autowired
	AttributeDAO attributeDAO;
	
	@Autowired
	AttributeDetailDAO attributeDetailDAO;
	
	@Autowired
	AttributeValueDetailDAO attributeValueDetailDAO;
	
	@Autowired
	RoutingCustomerDAO routingCustomerDAO;
	
	@Autowired
	CommonDAO commonDAO; 
	
	@Autowired
	PromotionProgramDAO promotionProgramDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	CustomerAttributeDAO customerAttributeDAO;
	
	@Autowired
	ApParamDAO apParamDAO;
	
	@Autowired
	PlatformTransactionManager transactionManager;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Customer createCustomer(Customer customer, String username, List<Long> lstAttributeId, List<String> lstAttributeValue, List<Integer> lstAttributeColumnValueType, LogInfoVO logInfo) throws BusinessException {
		try {
			customer = createCustomer(customer, logInfo);
			// tao log position customer
			this.saveCustomerPositionLog(customer, logInfo, ActionType.INSERT.getValue());
			updateCurrentCustomerCode();
			this.saveCustomerAttribute(customer, lstAttributeId, lstAttributeValue, lstAttributeColumnValueType, logInfo);
			return customer;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	/*
	 * Tao khach hang
	 */
	private Customer createCustomer(Customer customer, LogInfoVO logInfo) throws DataAccessException, BusinessException {
		String nameText = null;
		if (!StringUtility.isNullOrEmpty(customer.getCustomerName())) {
			nameText = customer.getCustomerName().toUpperCase();
		}
		if (!StringUtility.isNullOrEmpty(customer.getAddress())) {
			nameText = nameText + " " + customer.getAddress().toUpperCase();
		}
		if (!StringUtility.isNullOrEmpty(nameText)) {
			nameText = Unicode2English.codau2khongdau(nameText);
			customer.setNameText(nameText);
		}
		
		Date sysDate = commonDAO.getSysDate();
		String customerShortCode = generateCustomerShortCode();
		String shopCode = customer.getShop() != null ? customer.getShop().getShopCode() : "";
		String customerCode = generateCustomerCode(customerShortCode, shopCode);
		
		customer.setShortCode(customerShortCode);
		customer.setCustomerCode(customerCode);
		customer.setCreateUser(logInfo.getStaffCode());
		customer.setCreateDate(sysDate);
		return commonDAO.createEntity(customer);
	}
	
	public void saveCustomerAttribute(Customer customer, List<Long> lstAttributeId, List<String> lstAttributeValue, List<Integer> lstAttributeColumnValueType, LogInfoVO logInfo) throws BusinessException {
		try {
			CustomerAttributeDetail customerAttributeDetail = null;
			CustomerAttributeEnum customerAttributeEnum = null;
			Date sysDate = commonDAO.getSysDate();
			int index = 0;
			if (lstAttributeId != null) {
				for(Long attrId : lstAttributeId){
					CustomerAttribute customerAttribute = commonDAO.getEntityById(CustomerAttribute.class, attrId);
					if(customerAttribute == null){
						continue;
					}
					if(AttributeColumnType.CHARACTER.equals(customerAttribute.getValueType())
						|| AttributeColumnType.NUMBER.equals(customerAttribute.getValueType())
						|| AttributeColumnType.LOCATION.equals(customerAttribute.getValueType())
						|| AttributeColumnType.CHOICE.equals(customerAttribute.getValueType())
						|| AttributeColumnType.DATE_TIME.equals(customerAttribute.getValueType())){
						
						customerAttributeDetail = customerAttributeDAO.getCustomerAttributeDetailByFilter(attrId, customer.getId(), null);
						
						if(AttributeColumnType.CHOICE.equals(customerAttribute.getValueType()) && customerAttributeDetail != null){
							customerAttributeDetail.setStatus(ActiveType.DELETED);
							customerAttributeDetail.setUpdateDate(sysDate);
							customerAttributeDetail.setUpdateUser(logInfo.getStaffCode());
							commonDAO.updateEntity(customerAttributeDetail);
							customerAttributeDetail = null;
						}
						
						if(customerAttributeDetail == null){
							customerAttributeDetail = new CustomerAttributeDetail();
							customerAttributeDetail.setCreateDate(sysDate);
							customerAttributeDetail.setCreateUser(logInfo.getStaffCode());
							
							
							customerAttributeDetail.setCustomer(customer);
							customerAttributeDetail.setStatus(ActiveType.RUNNING);
							customerAttributeDetail.setCustomerAttribute(customerAttribute);
							
							if(AttributeColumnType.CHOICE.equals(customerAttribute.getValueType())) {
								if(!StringUtility.isNullOrEmpty(lstAttributeValue.get(index))){
									customerAttributeEnum = commonDAO.getEntityById(CustomerAttributeEnum.class, Long.valueOf(lstAttributeValue.get(index)));
									if(customerAttributeEnum != null){
										customerAttributeDetail.setCustomerAttributeEnum(customerAttributeEnum);
										commonDAO.createEntity(customerAttributeDetail);
									}
								}							
							}else{
								customerAttributeDetail.setValue(lstAttributeValue.get(index));
								commonDAO.createEntity(customerAttributeDetail);
							}
						}else{
							customerAttributeDetail.setUpdateDate(sysDate);
							customerAttributeDetail.setUpdateUser(logInfo.getStaffCode());
							customerAttributeDetail.setValue(lstAttributeValue.get(index));
							commonDAO.updateEntity(customerAttributeDetail);
						}
					}else if(AttributeColumnType.MULTI_CHOICE.equals(customerAttribute.getValueType())){					
						String valueStr = lstAttributeValue.get(index); //format x;y;z
						String[] valueStrArray = valueStr.split(";");
						
						List<CustomerAttributeDetail> attributeDetails = customerAttributeDAO.getCustomerAttributeDetailByFilter(attrId, customer.getId());
						
						for(CustomerAttributeDetail detail : attributeDetails ){
							detail.setStatus(ActiveType.DELETED);
							detail.setUpdateDate(sysDate);
							detail.setUpdateUser(logInfo.getStaffCode());
							commonDAO.updateEntity(detail);
						}
						if(!StringUtility.isNullOrEmpty(valueStr)){
							for(String str : valueStrArray){
								customerAttributeDetail = new CustomerAttributeDetail();
								customerAttributeDetail.setCreateDate(sysDate);
								customerAttributeDetail.setCreateUser(logInfo.getStaffCode());
								
								
								customerAttributeDetail.setCustomer(customer);
								customerAttributeDetail.setStatus(ActiveType.RUNNING);
								customerAttributeDetail.setCustomerAttribute(customerAttribute);						
								
								customerAttributeEnum = commonDAO.getEntityById(CustomerAttributeEnum.class, Long.valueOf(str));
								if(customerAttributeEnum != null){
									customerAttributeDetail.setCustomerAttributeEnum(customerAttributeEnum);
								}
													
								commonDAO.createEntity(customerAttributeDetail);
							}
						}
					}
					++index;
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateCustomer(Customer customer , String username, List<Long> lstAttributeId, List<String> lstAttributeValue, List<Integer> lstAttributeColumnValueType, LogInfoVO logInfo)
			throws BusinessException {
		try {
			// neu chuyen sang tam ngung thi xoa seq trong ROUTING_CUSTOMER.
			if (customer.getStatus().getValue()
					.equals(ActiveType.STOPPED.getValue())) {
				this.routingCustomerDAO.updateSeqOfDisableCustomer(customer
						.getId());
			}
			Customer customerOld = customerDAO.getCustomerById(customer.getId());
			// tao log position customer
			if ((customer.getLng() != null && !customer.getLng().equals(customerOld.getLng()))
					|| (customer.getLat() != null && !customer.getLat().equals(customerOld.getLat()))
					|| (customerOld.getLng() != null && !customerOld.getLng().equals(customer.getLng()))
					|| (customerOld.getLat() != null && !customerOld.getLat().equals(customer.getLat()))) {
				this.saveCustomerPositionLog(customer, logInfo, ActionType.UPDATE.getValue());
			}
			customerDAO.updateCustomer(customer, logInfo);
			this.saveCustomerAttribute(customer, lstAttributeId, lstAttributeValue, lstAttributeColumnValueType, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteCustomerPosition(Customer customer, LogInfoVO logInfoVO) throws BusinessException {
		try {
			customerDAO.updateCustomer(customer, logInfoVO);
			this.saveCustomerPositionLog(customer, logInfoVO, ActionType.UPDATE.getValue());
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * Luu Customerpositionlog KH 
	 * @author vuongmq
	 * @param customer
	 * @param logInfo
	 * @throws BusinessException
	 * @since 10/09/2015
	 */
	private void saveCustomerPositionLog(Customer customer, LogInfoVO logInfo, Integer typeChange) throws BusinessException {
		try {
			// tao log position customer
			Staff staff = staffDAO.getStaffByCode(logInfo.getStaffCode());
			CustomerPositionLog customerPositionLog = new CustomerPositionLog();
			customerPositionLog.setCustomer(customer);
			customerPositionLog.setStaff(staff);
			customerPositionLog.setLat(customer.getLat());
			customerPositionLog.setLng(customer.getLng());
			if (ActionType.INSERT.getValue().equals(typeChange)) {
				customerPositionLog.setCreateDate(customer.getCreateDate());
			} else {
				customerPositionLog.setCreateDate(customer.getUpdateDate());
			}
			commonDAO.createEntity(customerPositionLog);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteCustomer(Customer customer, LogInfoVO logInfo) throws BusinessException{
		try {
			customerDAO.deleteCustomer(customer, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Customer getCustomerById(Long id) throws BusinessException{
		try {
			return id==null?null:customerDAO.getCustomerById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Customer getCustomerByCode(String code) throws BusinessException {
		try {
			if(!StringUtility.isNullOrEmpty(code)){
				code = code.trim();
			}
			return customerDAO.getCustomerByCode(code);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	

	@Override
	public Customer getCustomerByShortCode(String code)
			throws BusinessException {
		try {
			if(!StringUtility.isNullOrEmpty(code)){
				code = code.trim();
			}
			return customerDAO.getCustomerByShortCode(code);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Customer getCustomerByCodeAndShop(String shortCode, Long shopId) throws BusinessException {
		try {
			if(!StringUtility.isNullOrEmpty(shortCode)){
				shortCode = shortCode.trim();
			}
			return customerDAO.getCustomerByCodeAndShop(shortCode, shopId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	/**
	 * @author sangtn
	 * @since 08-05-2014
	 * @description Get customer by full code and status 
	 * @note Extend from getCustomerByCode
	 */
	@Override
	public Customer getCustomerByCodeEx(String code, ActiveType status) throws BusinessException {
		try {
			return customerDAO.getCustomerByCodeEx(code, status);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Customer> getListCustomerEx(KPaging<Customer> kPaging,
			String shortCode, String customerName, 
			String parentShopCode, Long shopId, ActiveType status)	throws BusinessException {
		try {
			ParamsDAOGetListCustomer param = new ParamsDAOGetListCustomer();
			param.setkPaging(kPaging);
			param.setShortCode(shortCode);
			param.setCustomerName(customerName);
			param.setDirectShopCode(parentShopCode);
			param.setShopId(shopId);
			param.setStatus(status);
			List<Customer> lst = customerDAO.getListCustomer(param);
			ObjectVO<Customer> vo = new ObjectVO<Customer>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Customer> getListCustomerEx1(KPaging<Customer> kPaging,
			String shortCode, String customerName, 
			String parentShopCode, Long shopId, String address, ActiveType status)	throws BusinessException {
		try {
			ParamsDAOGetListCustomer param = new ParamsDAOGetListCustomer();
			param.setkPaging(kPaging);
			param.setShortCode(shortCode);
			param.setCustomerName(customerName);
			param.setDirectShopCode(parentShopCode);
			param.setShopId(shopId);
			param.setAddress(address);
			param.setStatus(status);
			List<Customer> lst = customerDAO.getListCustomer(param);
			ObjectVO<Customer> vo = new ObjectVO<Customer>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Customer> getListCustomerEx1ByShopId(KPaging<Customer> kPaging,
			String shortCode, String customerName, 
			String parentShopCode, Long shopId, String address, ActiveType status)	throws BusinessException {
		try {
			ParamsDAOGetListCustomer param = new ParamsDAOGetListCustomer();
			param.setkPaging(kPaging);
			param.setShortCode(shortCode);
			param.setCustomerName(customerName);
			param.setDirectShopCode(parentShopCode);
			param.setAddress(address);
			param.setStatus(status);
			List<Customer> lst = customerDAO.getListCustomerByShopId(shopId, param);
			ObjectVO<Customer> vo = new ObjectVO<Customer>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CustomerVO> getListCustomerFilterByShopId(KPaging<CustomerVO> kPaging,
			String shortCode, String customerName, 
			String parentShopCode, Long shopId, String address, ActiveType status, 
			String strListShopId, Long cycleId, Long ksId)	throws BusinessException {
		try {
			CustomerFilter param = new CustomerFilter();
			param.setkPaging(kPaging);
			param.setShortCode(shortCode);
			param.setCustomerName(customerName);
			param.setShopCode(parentShopCode);
			param.setAddress(address);
			param.setStatus(status.getValue());
			param.setStrShopId(strListShopId);
			param.setCycleId(cycleId);
			param.setKsId(ksId);
			List<CustomerVO> lst = customerDAO.getListCustomerByShopFilter(param);
			ObjectVO<CustomerVO> vo = new ObjectVO<CustomerVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Customer4SaleVO> getListCustomer4CreateOrder(Long shopId) throws BusinessException {
		try {
			List<Customer4SaleVO> listCustomer = customerDAO.getListCustomer4CreateOrder(shopId);
			return listCustomer;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Customer> getListCustomerForGroupTransfer(KPaging<Customer> kPaging,
			String shortCode, String customerName, String parentShopCode, 
			Long shopId, ActiveType status, Long notInDeliverId) throws BusinessException {
		try {
			ParamsDAOGetListCustomer param = new ParamsDAOGetListCustomer();
			param.setkPaging(kPaging);
			param.setShortCode(shortCode);
			param.setCustomerName(customerName);
			param.setDirectShopCode(parentShopCode);
			param.setShopId(shopId);
			param.setStatus(status);
			param.setNotInDeliverId(notInDeliverId);
			List<Customer> lst = customerDAO.getListCustomer(param);
			ObjectVO<Customer> vo = new ObjectVO<Customer>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Customer> getListCustomerForGroupTransferByShopId(KPaging<Customer> kPaging,
			String shortCode, String customerName, String parentShopCode, 
			Long shopId, ActiveType status, Long notInDeliverId) throws BusinessException {
		try {
			ParamsDAOGetListCustomer param = new ParamsDAOGetListCustomer();
			param.setkPaging(kPaging);
			param.setShortCode(shortCode);
			param.setCustomerName(customerName);
			param.setDirectShopCode(parentShopCode);
			param.setStatus(status);
			param.setNotInDeliverId(notInDeliverId);
			List<Customer> lst = customerDAO.getListCustomerByShopId(shopId, param);
			ObjectVO<Customer> vo = new ObjectVO<Customer>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Customer> getListCustomer(ParamsDAOGetListCustomer params) 
					throws BusinessException {
		try {
			Long shopId = null;
			ObjectVO<Customer> vo = new ObjectVO<Customer>();
			if (!StringUtility.isNullOrEmpty(params.getShopCode())) {
				Shop s = shopMgr.getShopByCode(params.getShopCode());
				if(s!= null){
					shopId = s.getId();
				}
				else
					return vo;
			}
			
			List<Customer> lst = new ArrayList<Customer>();
			
			if (params.getCashierId() != null) {
				Staff cashier = staffDAO.getStaffById(params.getCashierId());
				if (ActiveType.RUNNING.equals(cashier.getStatus())) {
					lst = customerDAO.getListCustomer(params);	
				}
			}
			else
				lst = customerDAO.getListCustomer(params);
			setAdditionalCustomerInfo(lst);
			vo.setkPaging(params.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	/*
	 * set thong tin ten nguoi tao, cac tuyen cua khach hang, ...
	 */
	private void setAdditionalCustomerInfo(List<Customer> lst) throws DataAccessException {
		if (lst == null || lst.isEmpty()) {
			return;
		}
		List<CustomerVO> customerRoutingInfo = getCustomerRoutingInfo(lst);
		for (Customer c : lst) {
			if (c != null) {
				setCustomerRoutingInfo(c, customerRoutingInfo);				
			}
		}
	}

	private List<CustomerVO> getCustomerRoutingInfo(List<Customer> lst) throws DataAccessException {
		List<String> customerCodes = getCustomerCodes(lst);
		List<CustomerVO> customerRoutingsInfo = customerDAO.getCustomerRoutingsInfo(customerCodes);
		return customerRoutingsInfo;
	}

	private List<String> getCustomerCodes(List<Customer> lst) {
		List<String> customerCodes = new ArrayList<String>(lst.size());
		for (Customer c : lst) {
			if (c != null) {
				customerCodes.add(c.getCustomerCode());				
			}
		}
		return customerCodes;
	}
	
	private void setCustomerRoutingInfo(Customer customer, List<CustomerVO> customerRoutingInfo) {
		for (CustomerVO customerVO : customerRoutingInfo) {
			if (customer.getCustomerCode().equals(customerVO.getCustomerCode())) {
				customer.setRoutes(customerVO.getRoutes());
				customer.setCreateUserName(customerVO.getCreateUserName());
			}
		}
	}

	@Override
	public Boolean isUsingByOthers(long customerId) throws BusinessException {
		try{
			return customerDAO.isUsingByOthers(customerId);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	
	//------------------------------------------------------
	@Autowired
	CustomerCatLevelDAO customerCatLevelDAO;
	
	@Override
	public CustomerCatLevel createCustomerCatLevel(CustomerCatLevel customerCatLevel, LogInfoVO logInfo) throws BusinessException {
		try {
			return customerCatLevelDAO.createCustomerCatLevel(customerCatLevel, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateCustomerCatLevel(CustomerCatLevel customerCatLevel, LogInfoVO logInfo) throws BusinessException {
		try {
			customerCatLevelDAO.updateCustomerCatLevel(customerCatLevel, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteCustomerCatLevel(CustomerCatLevel customerCatLevel, LogInfoVO logInfo) throws BusinessException{
		try {
			customerCatLevelDAO.deleteCustomerCatLevel(customerCatLevel, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public CustomerCatLevel getCustomerCatLevelById(Long id) throws BusinessException{
		try {
			return id==null?null:customerCatLevelDAO.getCustomerCatLevelById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CustomerCatLevel> getListCustomerCatLevel(KPaging<CustomerCatLevel> kPaging, Long customerId,
			Long saleLevelCatId, Boolean isOrderByName) throws BusinessException {
		try {
			List<CustomerCatLevel> lst = customerCatLevelDAO.getListCustomerCatLevel(kPaging, customerId, saleLevelCatId, isOrderByName);
			ObjectVO<CustomerCatLevel> vo = new ObjectVO<CustomerCatLevel>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkIfCustomerCatLevelRecordExist(long customerId, 
			long saleLevelCatId, Long id) throws BusinessException {
		try {
			return customerCatLevelDAO.checkIfRecordExist(customerId, saleLevelCatId, id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public CustomerCatLevel getCustomerCatLevel(Long customerId, Long catId) throws BusinessException {
		try {
			return customerCatLevelDAO.getCustomerCatLevel(customerId, catId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public Boolean checkIfCustomerExists(String email, String mobiphone,String phone, String numberIdentity,
			Long id, String fax) throws BusinessException {
		try {
			return customerDAO.checkIfCustomerExists(email, mobiphone,phone, numberIdentity, id, fax);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Customer getCustomerMultiChoine(Long customerId, Long shopId, String shortCode, Integer status) throws BusinessException {
		try {
			return customerDAO.getCustomerMultiChoine(customerId, shopId, shortCode, status);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptCustomerByProductCatLv01VO> getDataForCusByProReport(
			Long shopId, List<Long> cusIds, Date fromDate, Date toDate)
			throws BusinessException {
		try {
			List<RptCustomerByProductCatDataVO> listData = customerDAO
					.getDataForCusByProCatReport(shopId, cusIds, fromDate,
							toDate);

			return RptCustomerByProductCatDataVO.groupByCusAndCat(listData);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public CustomerVO getCustomerVO(Long customerId)
			throws BusinessException {
		try {
			return customerDAO.getCustomerVO(customerId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	private TreeExVO<Shop, Customer> getListTreeEx(TreeVO<Shop> shopTree, List<Customer> lstCustomer) {
		TreeExVO<Shop, Customer> treeEx = new TreeExVO<Shop, Customer>();
		treeEx.setObject(shopTree.getObject());
		List<TreeExVO<Shop, Customer>> lstChildShopTreeEx = new ArrayList<TreeExVO<Shop, Customer>>();
		List<Customer> lstLeaves = new ArrayList<Customer>();
		
		List<Customer> lstTempCustomer = new ArrayList<Customer>();
		for(Customer st: lstCustomer) {
			if (st.getShop().getId().equals(shopTree.getObject().getId())) {
				lstLeaves.add(st);
			}
			else 
				lstTempCustomer.add(st);
		}
		lstCustomer.removeAll(lstCustomer);
		lstCustomer.addAll(lstTempCustomer);
		
		for (TreeVO<Shop> childTree: shopTree.getListChildren()) {
			TreeExVO<Shop, Customer> childTreeEx = new TreeExVO<Shop, Customer>();
			childTreeEx = this.getListTreeEx(childTree, lstCustomer);
			if (childTreeEx.getListChildren().size() + childTreeEx.getListLeaves().size() > 0) {
				lstChildShopTreeEx.add(childTreeEx);
			}
		}
		treeEx.setListChildren(lstChildShopTreeEx);
		treeEx.setListLeaves(lstLeaves);
		
		return treeEx;
	}
	
	@Override
	public ObjectVO<Customer> getListCustomerNotInPromotionProgram(KPaging<Customer> kPaging, 
			Long parentShopId, Long promotionShopMapId, String shortCode, String customerName, String address)
			throws BusinessException {
		try {
			List<Customer> lst = customerDAO.getListCustomerNotInPromotionProgram(kPaging, parentShopId, promotionShopMapId, shortCode, customerName, address);
			ObjectVO<Customer> vo = new ObjectVO<Customer>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		}
		catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<Customer> getListCustomerByShopCode(KPaging<Customer> kPaging, String shopCode)
			throws BusinessException {
		try {
			List<Customer> lst = customerDAO.getListCustomerByShopCode(kPaging, shopCode);
			ObjectVO<Customer> vo = new ObjectVO<Customer>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<CustomerCatLevelImportVO> getCustomerCatLevelImportVO(
			KPaging<CustomerCatLevelImportVO> kPaging, String shortCode,
			String customerName, List<String> lstAreaCode, String phone,
			String mobiPhone, Long customerTypeId, ActiveType status,
			String region, String loyalty, Long groupTransferId,
			Long cashierId, String address, String street, String shopCode,
			String shopCodeLike, String display, String location)
			throws BusinessException {
		try {
			Long shopId = null;
			ObjectVO<CustomerCatLevelImportVO> vo = new ObjectVO<CustomerCatLevelImportVO>();

			if (!StringUtility.isNullOrEmpty(shopCode)) {
				Shop s = shopMgr.getShopByCode(shopCode);

				if (s != null) {
					shopId = s.getId();
				} else
					return vo;
			}

			ParamDAOGetCustomerCatLevelImportVO param = new ParamDAOGetCustomerCatLevelImportVO();

			param.setkPaging(kPaging);
			param.setShortCode(shortCode);
			param.setCustomerName(customerName);
			param.setShopId(shopId);
			param.setStatus(status);
			param.setLstAreaCode(lstAreaCode);
			param.setPhone(phone);
			param.setMobiPhone(mobiPhone);
			param.setCustomerTypeId(customerTypeId);
			param.setRegion(region);

			param.setLoyalty(loyalty);
			param.setGroupTransferId(groupTransferId);
			param.setCashierId(cashierId);
			param.setAddress(address);
			param.setStreet(street);
			param.setShopCodeLike(shopCodeLike);
			param.setDisplay(display);
			param.setLocation(location);

			List<CustomerCatLevelImportVO> lst = new ArrayList<CustomerCatLevelImportVO>();

			if (cashierId != null) {
				Staff cashier = staffDAO.getStaffById(cashierId);
				if (ActiveType.RUNNING.equals(cashier.getStatus())) {
					lst = this.customerCatLevelDAO
							.getCustomerCatLevelImportVO(param);
				}
			} else {
				lst = this.customerCatLevelDAO
						.getCustomerCatLevelImportVO(param);
			}

			vo.setkPaging(kPaging);
			vo.setLstObject(lst);

			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Customer> getListCustomerByStaffRouting(
			KPaging<Customer> kPaging, Long shopId, String customerCode,
			String customerName, Long staffId, 
			ActiveType status) throws BusinessException {
		try {
			List<Customer> lst = customerDAO.getListCustomerByStaffRouting(
					kPaging, shopId, customerCode, customerName, staffId,status);
			
			ObjectVO<Customer> vo = new ObjectVO<Customer>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CustomerGTVO> getListCustomerGTVOByGroupTransfer(
			KPaging<CustomerGTVO> kPaging, Long shopId,
			String groupTransferCode, String groupTransferName,
			String staffTransferCode, String shortCode, ActiveType status,
			String shopCode, Boolean check, String customerName) throws BusinessException {
		try {
			List<CustomerGTVO> lst = customerDAO.getListCustomerGTVOByGroupTransfer(kPaging, shopId, groupTransferCode, groupTransferName, staffTransferCode, shortCode, status, shopCode, check, customerName);
			
			ObjectVO<CustomerGTVO> vo = new ObjectVO<CustomerGTVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Customer> searchCustomerForDSKH(
			KPaging<Customer> kPaging, String shopCode, String staffCode,
			String customerCode, String customerName) throws BusinessException {
		try {
			List<Customer> lst = customerDAO.searchCustomerVOForDSKH(kPaging, shopCode, staffCode, customerCode, customerName);
			
			ObjectVO<Customer> vo = new ObjectVO<Customer>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Customer> getListCustomerByShortCode(Long shopId, String shortCode)
			throws BusinessException {
		try {
			return customerDAO.getListCustomerByShortCode(shopId, shortCode);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void removeTypeOfCustomer(long typeId) throws BusinessException {
		try {
			customerDAO.removeTypeOfCustomer(typeId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<CustomerVO> searchListCustomerVOByFilter(CustomerFilter<CustomerVO> filter)	throws BusinessException {
		try {
			List<CustomerVO> lst = customerDAO.searchListCustomerVOByFilter(filter);
			ObjectVO<CustomerVO> vo = new ObjectVO<CustomerVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CustomerVO> searchListCustomerVOForRoutingByFilter(CustomerFilter<CustomerVO> filter)	throws BusinessException {
		try {
			List<CustomerVO> lst = customerDAO.searchListCustomerVOForRoutingByFilter(filter);
			ObjectVO<CustomerVO> vo = new ObjectVO<CustomerVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public boolean existsCode(Long cusID, Long shopId, String code)
			throws BusinessException {	
		try {
			return customerDAO.existsCode( cusID,  shopId,  code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<DuplicatedCustomerVO> approveNewCustomer(final Long customerId, final LogInfoVO logInfo) throws BusinessException {
		if (customerId == null) {
			throw new IllegalArgumentException("customerId is null");
		}
		
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		List<DuplicatedCustomerVO> duplicatedCustomer = transactionTemplate.execute(new TransactionCallback<List<DuplicatedCustomerVO>>() {

			@Override
			public List<DuplicatedCustomerVO> doInTransaction(TransactionStatus transactionStatus) {
				List<DuplicatedCustomerVO> duplicateCustomers = null;
				try {
					Customer customer = commonDAO.getEntityById(Customer.class, customerId);
					if (customer.getLat() == null || customer.getLng() == null) {
						throw new IllegalArgumentException("customer's GPS coordinate is invalid.");
					}
					duplicateCustomers = getDuplicateCustomers(customer);
					if (duplicateCustomers == null || duplicateCustomers.isEmpty()) {
						createNewCustomerWithNewCode(customer, logInfo);
					} else {
						transactionStatus.setRollbackOnly();
					}
					return duplicateCustomers;
				} catch (Exception e) {
					transactionStatus.setRollbackOnly();
				}
				return duplicateCustomers;
			}

			
		});
		
		return duplicatedCustomer;
	}
	
	/*
	 * Lay danh sach khach hang trung (dia chi, khoang cach) voi khach hang dang duyet
	 */
	private List<DuplicatedCustomerVO> getDuplicateCustomers(Customer customer) throws DataAccessException {
		return customerDAO.getDuplicatedCustomers(customer.getId());
	}
	
	/*
	 * Lay danh sach khach hang trung dia chi voi khach hang co id = customerId
	 */
	private List<DuplicatedCustomerVO> getDuplicatedAddressCustomers(Long customerId) throws DataAccessException {
		return customerDAO.getDuplicatedCustomers(customerId);
	}

	/*
	 * Lay cau hinh khoang cach kiem tra khach hang trung
	 */
	private Integer getCheckingDuplicateCustomerDistance() throws DataAccessException, NumberFormatException {
		final String checkingDuplicateCustomerDistanceConfigCode = "SYS_CHECK_DUPLICATE_CUSTOMER_DISTANCE";
		ApParamFilter filter = new ApParamFilter();
		filter.setApParamCode(checkingDuplicateCustomerDistanceConfigCode);
		filter.setType(ApParamType.SYS_CONFIG);
		filter.setStatus(ActiveType.RUNNING);
		List<ApParam> configs = apParamDAO.getListApParamByFilter(filter);
		ApParam distanceConfig = configs != null && configs.size() > 0 ? configs.get(0) : null;
		Integer checkingDuplicateCustomerDistance = distanceConfig != null ? Integer.valueOf(distanceConfig.getValue()) : null;
		return checkingDuplicateCustomerDistance;
	}

	/*
	 * Lay danh sach khach hang trung dia chi trong khoang cach cau hinh
	 */
	private List<DuplicatedCustomerVO> getDuplicatedCustomerInValidDistance(Customer customer, List<DuplicatedCustomerVO> duplicatedAddressCustomers, Integer checkingDuplicateCustomerDistance) {
		List<DuplicatedCustomerVO> duplicatedCustomer = new ArrayList<DuplicatedCustomerVO>();
		for (DuplicatedCustomerVO duplicateCustomer : duplicatedAddressCustomers) {
			if (duplicateCustomer.getLat() == null || duplicateCustomer.getLng() == null) {
				continue;
			}
			Point2D.Double customerPosition = new Point2D.Double(customer.getLat(), customer.getLng());
			Point2D.Double duplicateCustomerPosition = new Point2D.Double(duplicateCustomer.getLat(), duplicateCustomer.getLng());
			double distance = calculateGPSDistance(customerPosition, duplicateCustomerPosition);
			if (distance <= checkingDuplicateCustomerDistance) {
				duplicateCustomer.setDistanceFromApprovingCustomer(distance);
				duplicatedCustomer.add(duplicateCustomer);
			}
		}
		return duplicatedCustomer;
	}

	/*
	 * Tinh khoang cach GPS giua 2 diem (don vi: met)
	 */
	private double calculateGPSDistance(Point2D.Double customerPosition, Point2D.Double duplicateCustomerPosition) {
		return MathUtil.calculateGPSDistance(customerPosition, duplicateCustomerPosition);
	}
	
	/*
	 * Phat sinh ma khach hang. Ma khach hang hien tai luu trong cau hinh ap_param
	 */
	private String generateCustomerShortCode() throws DataAccessException {
		ApParam currentCustomerCodeConfig = getCurrentCustomerCodeConfig();
		if (currentCustomerCodeConfig == null) {
			throw new IllegalArgumentException("customer_code config not set.");
		}
		
		Long currentCustomerCode = Long.valueOf(currentCustomerCodeConfig.getValue());
		return currentCustomerCode.toString();
	}

	/*
	 * Cap nhat ma khach hang sau khi duyet khach hang xong
	 */
	private void updateCurrentCustomerCode() throws DataAccessException {
		ApParam currentCustomerCodeConfig = getCurrentCustomerCodeConfig();
		if (currentCustomerCodeConfig != null) {
			Long currentCustomerCode = Long.valueOf(currentCustomerCodeConfig.getValue());
			currentCustomerCodeConfig.setValue(String.valueOf(++currentCustomerCode));
			commonDAO.updateEntity(currentCustomerCodeConfig);
		}
	}
	
	/*
	 * Lay cau hinh ma khach hang hien tai
	 */
	private ApParam getCurrentCustomerCodeConfig() throws DataAccessException {
		final String currentCustomerCodeConfigCode = "CURRENT_CUSTOMER_CODE";
		ApParamFilter filter = new ApParamFilter();
		filter.setApParamCode(currentCustomerCodeConfigCode);
		filter.setType(ApParamType.CUSTOMER_CONFIG);
		filter.setStatus(ActiveType.RUNNING);
		List<ApParam> configs = apParamDAO.getListApParamByFilter(filter);
		ApParam currentCustomerCodeConfig = configs != null && configs.size() > 0 ? configs.get(0) : null;
		return currentCustomerCodeConfig;
	}
	
	/*
	 * Tao moi khach hang voi ma khach hang moi tu sinh
	 */
	private Customer createNewCustomerWithNewCode(Customer customer, final LogInfoVO logInfo) throws DataAccessException {
		Date sysDate = commonDAO.getSysDate();
		String customerShortCode = generateCustomerShortCode();
		String shopCode = customer.getShop() != null ? customer.getShop().getShopCode() : "";
		String customerCode = generateCustomerCode(customerShortCode, shopCode);
		
		customer.setStatus(ActiveType.RUNNING);
		customer.setCustomerCode(customerCode);
		customer.setShortCode(customerShortCode);
		customer.setUpdateDate(sysDate);
		customer.setUpdateUser(logInfo.getStaffCode());
		commonDAO.updateEntity(customer);
		
		updateCurrentCustomerCode();
		
		return customer;
	}
	
	/*
	 * sinh ma khach hang dua tren ma shortCode & ma shopCode
	 */
	private String generateCustomerCode(String customerShortCode, String shopCode) {
		final String DELIMITER = "_";
		return customerShortCode + DELIMITER + shopCode;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Customer createNewCustomer(Long customerId, LogInfoVO logInfo) throws BusinessException {
		if (customerId == null) {
			throw new IllegalArgumentException("customerId is null");
		}
		try {
			Customer customer = commonDAO.getEntityById(Customer.class, customerId);
			customer = createNewCustomerWithNewCode(customer, logInfo);
			return customer;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<CustomerExportVO> getCustomerWithDynamicAttributeInfo(ParamsDAOGetListCustomer param) throws BusinessException {
		try {
			return customerDAO.getCustomerWithDynamicAttributeInfo(param);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CustomerVO> getListCustomerBySaleStaffHasVisitPlan(Long shopId, Long staffId, Date checkDate) throws BusinessException {
		if (null == staffId) {
			throw new IllegalArgumentException("staff is null");
		}
		try {
			ApParam config = apParamDAO.getApParamByCode(Constant.CF_IS_CUSTOMER_WAITING, ApParamType.SYS_CONFIG);
			return customerDAO.getListCustomerBySaleStaffHasVisitPlanWithFirstSaleOrder(shopId, staffId, config != null ? Integer.parseInt(config.getValue()) : null, checkDate);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CustomerVO> getCustomerVOFeedback(CustomerFilter<CustomerVO> filter) throws BusinessException {
		try {
			List<CustomerVO> lst = customerDAO.getCustomerVOFeedback(filter);
			ObjectVO<CustomerVO> vo = new ObjectVO<CustomerVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CustomerVO> getCustomerVORoutingFeedback(CustomerFilter<CustomerVO> filter) throws BusinessException {
		try {
			List<CustomerVO> lst = customerDAO.getCustomerVORoutingFeedback(filter);
			ObjectVO<CustomerVO> vo = new ObjectVO<CustomerVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.CustomerMgr#searchListCustomerFullVOByFilter(ths.dms.core.entities.filter.CustomerFilter)
	 */
	@Override
	public ObjectVO<CustomerVO> searchListCustomerFullVOByFilter(CustomerFilter<CustomerVO> filter) throws BusinessException {
		try {
			List<CustomerVO> lst = customerDAO.getListCustomerFullVOByFilter(filter);
			ObjectVO<CustomerVO> vo = new ObjectVO<CustomerVO>();
			vo.setkPaging(filter.getkPaging());

			for (int i = 0, sz = lst.size(); i < sz; i++) {
				CustomerVO customerVO = lst.get(i);
				// set kenh mien cho KH kenh ST
				if (customerVO != null && StringUtility.isNullOrEmpty(customerVO.getShopKenhCode())) {
					customerVO.setShopKenhCode(customerVO.getShopVungCode());
					customerVO.setShopKenhName(customerVO.getShopVungName());
					customerVO.setShopMienCode(customerVO.getShopCode());
					customerVO.setShopMienName(customerVO.getShopName());
				}
			}
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

}
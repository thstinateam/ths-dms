package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import ths.dms.core.entities.ApParamEquip;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.EquipDeliveryRecDtl;
import ths.dms.core.entities.EquipDeliveryRecord;
import ths.dms.core.entities.EquipEvictionForm;
import ths.dms.core.entities.EquipEvictionFormDtl;
import ths.dms.core.entities.EquipFormHistory;
import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipGroupProduct;
import ths.dms.core.entities.EquipHistory;
import ths.dms.core.entities.EquipImportRecord;
import ths.dms.core.entities.EquipItem;
import ths.dms.core.entities.EquipItemConfig;
import ths.dms.core.entities.EquipItemConfigDtl;
import ths.dms.core.entities.EquipLend;
import ths.dms.core.entities.EquipLendDetail;
import ths.dms.core.entities.EquipLender;
import ths.dms.core.entities.EquipLiquidationForm;
import ths.dms.core.entities.EquipLostRecord;
import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.EquipProvider;
import ths.dms.core.entities.EquipRepairForm;
import ths.dms.core.entities.EquipRepairFormDtl;
import ths.dms.core.entities.EquipRepairPayForm;
import ths.dms.core.entities.EquipRepairPayFormDtl;
import ths.dms.core.entities.EquipRole;
import ths.dms.core.entities.EquipRoleUser;
import ths.dms.core.entities.EquipSalePlan;
import ths.dms.core.entities.EquipStatisticCustomer;
import ths.dms.core.entities.EquipStatisticGroup;
import ths.dms.core.entities.EquipStatisticRecord;
import ths.dms.core.entities.EquipStatisticUnit;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.EquipStockTotal;
import ths.dms.core.entities.EquipStockTransForm;
import ths.dms.core.entities.EquipStockTransFormDtl;
import ths.dms.core.entities.EquipSuggestEviction;
import ths.dms.core.entities.EquipSuggestEvictionDTL;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamEquipType;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.DeliveryType;
import ths.dms.core.entities.enumtype.EquipGFilter;
import ths.dms.core.entities.enumtype.EquipPeriodType;
import ths.dms.core.entities.enumtype.EquipStockEquipFilter;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeStatus;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUsageStatus;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.EquipmentRepairFormFilter;
import ths.dms.core.entities.enumtype.EquipmentStatisticRecordStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PerFormStatus;
import ths.dms.core.entities.enumtype.StatusDeliveryEquip;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.enumtype.TableName;
import ths.dms.core.entities.enumtype.TreeVOType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.EquipItemConfigDtlFilter;
import ths.dms.core.entities.filter.EquipItemConfigFilter;
import ths.dms.core.entities.filter.EquipItemFilter;
import ths.dms.core.entities.filter.EquipPermissionFilter;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipRepairFilter;
import ths.dms.core.entities.filter.EquipRoleFilter;
import ths.dms.core.entities.filter.EquipStaffFilter;
import ths.dms.core.entities.filter.EquipStatisticFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.filter.EquipSuggestEvictionFilter;
import ths.dms.core.entities.filter.EquipmentEvictionFilter;
import ths.dms.core.entities.filter.EquipmentGroupProductFilter;
import ths.dms.core.entities.filter.EquipmentSalePlaneFilter;
import ths.dms.core.entities.vo.EquipBorrowVO;
import ths.dms.core.entities.vo.EquipItemConfigDtlVO;
import ths.dms.core.entities.vo.EquipItemConfigVO;
import ths.dms.core.entities.vo.EquipItemVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipRepairFormVO;
import ths.dms.core.entities.vo.EquipRepairFormVOList;
import ths.dms.core.entities.vo.EquipSalePlanVO;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentDeliveryPrintVO;
import ths.dms.core.entities.vo.EquipmentDeliveryReturnVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentEvictionVO;
import ths.dms.core.entities.vo.EquipmentEvictionVOs;
import ths.dms.core.entities.vo.EquipmentExVO;
import ths.dms.core.entities.vo.EquipmentGroupProductVO;
import ths.dms.core.entities.vo.EquipmentGroupVO;
import ths.dms.core.entities.vo.EquipmentLiquidationFormVO;
import ths.dms.core.entities.vo.EquipmentManagerVO;
import ths.dms.core.entities.vo.EquipmentPermissionVO;
import ths.dms.core.entities.vo.EquipmentRecordDeliveryVO;
import ths.dms.core.entities.vo.EquipmentRepairFormDtlVO;
import ths.dms.core.entities.vo.EquipmentRepairFormVO;
import ths.dms.core.entities.vo.EquipmentRepairPaymentRecord;
import ths.dms.core.entities.vo.EquipmentStaffVO;
import ths.dms.core.entities.vo.EquipmentStatisticCheckingRecordExportVO;
import ths.dms.core.entities.vo.EquipmentStockDeliveryVO;
import ths.dms.core.entities.vo.EquipmentSuggestEvictionVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.FormErrVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductTreeVO;
import ths.dms.core.entities.vo.RepairItemPaymentVO;
import ths.dms.core.entities.vo.ShopViewParentVO;
import ths.dms.core.entities.vo.StatisticCheckingVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.LogUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.ApParamEquipDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.CustomerDAO;
import ths.dms.core.dao.EquipProposalBorrowDAO;
import ths.dms.core.dao.EquipRecordDAO;
import ths.dms.core.dao.EquipStatisticGroupDAO;
import ths.dms.core.dao.EquipStatisticRecordDAO;
import ths.dms.core.dao.EquipSuggestEvictionDAO;
import ths.dms.core.dao.EquipmentLiquidationFormDAO;
import ths.dms.core.dao.EquipmentManagerDAO;
import ths.dms.core.dao.EquipmentPeriodDAO;
import ths.dms.core.dao.EquipmentRecordDeliveryDAO;
import ths.dms.core.dao.EquipmentStockTotalDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.StaffDAO;

public class EquipmentManagerMgrImpl implements EquipmentManagerMgr {
	@Autowired
	CommonDAO commonDAO;

	@Autowired
	PlatformTransactionManager transactionManager;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	StaffDAO staffDAO;
	
	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	EquipmentManagerDAO equipMngDAO;

	@Autowired
	EquipmentRecordDeliveryDAO equipmentRecordDeliveryDAO;

	@Autowired
	EquipmentLiquidationFormDAO equipmentLiquidationFormDAO;

	@Autowired
	EquipmentPeriodDAO equipmentPeriodDAO;

	@Autowired
	EquipmentStockTotalDAO equipmentStockTotalDAO;

	@Autowired
	EquipStatisticGroupDAO equipStatisticGroupDAO;

	@Autowired
	private ProductDAO productDAO;

	@Autowired
	EquipRecordDAO equipRecordDAO;

	@Autowired
	EquipStatisticRecordDAO equipStatisticRecordDAO;
	
	@Autowired
	EquipProposalBorrowDAO equipProposalBorrowDAO;

	@Autowired
	EquipSuggestEvictionDAO equipSuggestEvictionDAO;
	
	@Autowired
	ApParamEquipDAO apParamEquipDAO;
	
	@Override
	public EquipCategory getEquipCategoryById(Long id) throws BusinessException {
		try {
			return id == null ? null : equipMngDAO.getEquipCategoryById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public EquipItem getEquipItemById(Long id) throws BusinessException {
		try {
			return id == null ? null : equipMngDAO.getEquipItemById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public String getEquipImportRecordCodeGenetated() throws BusinessException {
		try {
			return equipMngDAO.getEquipImportRecordCodeGenetated();
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public EquipProvider getEquipProviderById(Long id) throws BusinessException {
		try {
			return id == null ? null : equipMngDAO.getEquipProviderById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipProvider getEquipProviderByCode(String code) throws BusinessException {
		try {
			return code == null ? null : equipMngDAO.getEquipProviderByCode(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipGroup getEquipGroupByCode(String code) throws BusinessException {
		try {
			if (StringUtility.isNullOrEmpty(code)) {
				return null;
			}
			return equipMngDAO.getEquipGroupByCode(code);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipPeriod> getListEquipPeriodByFilter(EquipmentFilter<EquipPeriod> filter) throws BusinessException {
		try {
			List<EquipPeriod> lst = equipMngDAO.getListEquipPeriodByFilter(filter);
			ObjectVO<EquipPeriod> objVO = new ObjectVO<EquipPeriod>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipPeriod> getListEquipPeriodClosedByFromDate(EquipmentFilter<EquipPeriod> filter) throws BusinessException {
		try {
			return equipMngDAO.getListEquipPeriodClosedByFromDate(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Equipment createEquipment(Equipment equipment) throws BusinessException {
		try {
			equipment.setCreateDate(commonDAO.getSysDate());
			return equipMngDAO.createEquipment(equipment);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEquipment(Equipment equipment) throws BusinessException {
		try {
			equipment.setUpdateDate(commonDAO.getSysDate());
			equipMngDAO.updateEquipment(equipment);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> getListEquipmentGeneral(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getListEquipmentGeneral(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipCategory createEquipCategoryManagerCatalog(EquipCategory equipcategory, LogInfoVO logInfoVO) throws BusinessException {
		try {
			return equipMngDAO.createEquipCategoryManagerCatalog(equipcategory, logInfoVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipProvider createEquipProviderManagerCatalog(EquipProvider equipprovider, LogInfoVO logInfoVO) throws BusinessException {
		try {
			return equipMngDAO.createEquipProviderManagerCatalog(equipprovider, logInfoVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipItem createEquipItemManagerCatalog(EquipItem equipItem, LogInfoVO logInfoVO) throws BusinessException {

		try {
			return equipMngDAO.createEquipItemManagerCatalog(equipItem, logInfoVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipCategory updateEquipCategoryManagerCatalog(EquipCategory equipCategory, LogInfoVO logInfoVO) throws BusinessException {

		try {
			return equipMngDAO.updateEquipCategoryManagerCatalog(equipCategory, logInfoVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipProvider updateEquipProviderManagerCatalog(EquipProvider equipProvider, LogInfoVO logInfoVO) throws BusinessException {

		try {
			return equipMngDAO.updateEquipProviderManagerCatalog(equipProvider, logInfoVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipItem updateEquipItemManagerCatalog(EquipItem equipItem, LogInfoVO logInfoVO) throws BusinessException {
		try {
			return equipMngDAO.updateEquipItemManagerCatalog(equipItem, logInfoVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipmentEvictionVO> getListEquipmentEviction(EquipmentEvictionFilter filter) throws BusinessException {
		try {
			List<EquipmentEvictionVO> lst = equipMngDAO.getListEquipmentEviction(filter);
			ObjectVO<EquipmentEvictionVO> objVO = new ObjectVO<EquipmentEvictionVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipEvictionForm createEquipEvictionForm(EquipEvictionForm equipEvictionForm) throws BusinessException {
		try {
			return equipMngDAO.createEquipEvictionForm(equipEvictionForm);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipEvictionForm createEquipEvictionFormX(EquipEvictionForm equipEvictionForm, List<Equipment> lstEquipment, EquipRecordFilter<EquipRepairForm> filter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			String username = equipEvictionForm.getCreateUser();
			Date date = equipEvictionForm.getCreateDate();
			if (filter != null) {
				filter.setChangeDate(date);
				filter.setUserName(username);
			}
			equipEvictionForm = equipMngDAO.createEquipEvictionForm(equipEvictionForm);
//			String id = "";
//			id = "00000000" + equipEvictionForm.getId().toString();
//			id = id.substring(id.length() - 8);
//			String code = "TH" + id;
			String code = equipmentRecordDeliveryDAO.getCodeEviction();
			equipEvictionForm.setCode(code);
			equipMngDAO.updateEquipEvictionForm(equipEvictionForm);
			//Xu ly File
			if (filter != null) {
				//Xu ly them hinh anh
				if (filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
					for (FileVO voFile : filter.getLstFileVo()) {
						EquipAttachFile equipAttFile = new EquipAttachFile();
						equipAttFile.setUrl(voFile.getUrl());
						equipAttFile.setObjectId(equipEvictionForm.getId());
						equipAttFile.setFileName(voFile.getFileName());
						equipAttFile.setThumbUrl(voFile.getThumbUrl());
						equipAttFile.setObjectType(EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue());
						equipAttFile.setCreateDate(filter.getChangeDate());
						equipAttFile.setCreateUser(filter.getUserName());
						equipRecordDAO.createEquipAttachFile(equipAttFile);
					}
				}
			}
			// luu lich su bien ban
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(equipEvictionForm.getCreateDate());
			equipFormHistory.setCreateDate(date);
			equipFormHistory.setCreateUser(equipEvictionForm.getCreateUser());
			equipFormHistory.setDeliveryStatus(equipEvictionForm.getActionStatus());
			equipFormHistory.setRecordId(equipEvictionForm.getId());
			equipFormHistory.setRecordStatus(equipEvictionForm.getFormStatus());
			equipFormHistory.setRecordType(EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue());
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
			EquipStock stock = null;
			String stockCode = null;
			String stockName = null;
			if (equipEvictionForm.getToStockId() != null) {
				stock = commonDAO.getEntityById(EquipStock.class,equipEvictionForm.getToStockId());
				if (stock != null) {
					stockCode = stock.getCode();
					stockName = stock.getName();
				}
			}
			
			// luu chi tiet bien ban
			for (int i = 0; i < lstEquipment.size(); i++) {
				Equipment equipment = lstEquipment.get(i);
				EquipEvictionFormDtl detail = new EquipEvictionFormDtl();
				detail.setCreateDate(date);
				detail.setCreateUser(equipEvictionForm.getCreateUser());
				detail.setEquip(equipment);
				/** tamvnm: luu id bien ban giao nhan*/
				if (equipEvictionForm.getCustomer() != null) {
					List<Long> lstEquipId = new ArrayList<Long>();
					lstEquipId.add(equipment.getId());
					List<EquipmentExVO> lstequipVO = equipMngDAO.getListAllEquipmentDeliveryByCustomerId(equipEvictionForm.getCustomer().getId(), lstEquipId);
					if (lstequipVO != null && lstequipVO.size() > 0) {
						Long deliveryId = null;
						EquipDeliveryRecord deliveryRecord = null;
						for (int c = 0, csize = lstequipVO.size(); c < csize; c++) {
							if (lstequipVO.get(c).getId().equals(equipment.getId())) {
								deliveryId = lstequipVO.get(c).getEquipDeliveryId();
								break;
							}
						}
						if (deliveryId != null) {
							deliveryRecord = commonDAO.getEntityById(EquipDeliveryRecord.class, deliveryId);
						}
						
						if (deliveryRecord != null) {
							detail.setEquipDeliveryRecord(deliveryRecord);
						}
					}
				}
				
				detail.setSerial(equipment.getSerial());
				detail.setManufacturingYear(equipment.getManufacturingYear());
				detail.setHealthStatus(equipment.getHealthStatus() != null ? equipment.getHealthStatus() : null);
				if (equipment.getEquipGroup() != null) {
					detail.setEquipCategory(equipment.getEquipGroup().getEquipCategory());
					if (equipment.getEquipGroup().getEquipCategory() != null) {
						detail.setEquipCategoryName(equipment.getEquipGroup().getEquipCategory().getName());
					}
				}
				detail.setEquipGroup(equipment.getEquipGroup());
				if (equipment.getEquipGroup() != null) {
					detail.setEquipGroupName(equipment.getEquipGroup().getName());
					detail.setCapacityFrom(equipment.getEquipGroup().getCapacityFrom());
					detail.setCapacityTo(equipment.getEquipGroup().getCapacityTo());
				}
				detail.setEquipEvictionForm(equipEvictionForm);
				detail.setTotal(1);//mac dinh la 1

				equipMngDAO.createEquipEvictionFormDtl(detail);
				if (equipEvictionForm.getFormStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {//trang thai bien ban la du thao
					equipment.setTradeType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
					equipment.setTradeStatus(1);//dang giao dich
					equipment.setUpdateDate(date);
					equipment.setUpdateUser(equipEvictionForm.getCreateUser());
					equipMngDAO.updateEquipment(equipment);
				} else if (equipEvictionForm.getFormStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {//trang thai bien ban la duyet
					equipEvictionForm.setApproveDate(date);
					equipMngDAO.updateEquipEvictionForm(equipEvictionForm);
					
					equipment.setTradeType(null);
					equipment.setTradeStatus(0);//ko giao dich
					equipment.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE); //trang thai : dang o kho
					
					equipment.setStockId(equipEvictionForm.getToStockId());
					equipment.setStockType(EquipStockTotalType.parseValue(equipEvictionForm.getToStockType()));
					equipment.setStockCode(stockCode);
					equipment.setStockName(stockName);
					equipment.setUpdateDate(date);
					equipment.setUpdateUser(equipEvictionForm.getCreateUser());
					equipMngDAO.updateEquipment(equipment);

					EquipStockTotal khoThuHoi = equipmentStockTotalDAO.getEquipStockTotalById(equipEvictionForm.getToStockId(), EquipStockTotalType.parseValue(equipEvictionForm.getToStockType()), equipment.getEquipGroup().getId());
					if (khoThuHoi == null) {
						khoThuHoi = new EquipStockTotal();
						khoThuHoi.setCreateDate(date);
						khoThuHoi.setCreateUser(username);
						khoThuHoi.setEquipGroup(equipment.getEquipGroup());
						khoThuHoi.setQuantity(0);
						khoThuHoi.setStockId(equipEvictionForm.getToStockId());
						khoThuHoi.setStockType(EquipStockTotalType.parseValue(equipEvictionForm.getToStockType()));
						khoThuHoi = equipmentStockTotalDAO.createEquipStockTotal(khoThuHoi);
					}
					EquipStockTotal khoKH = equipmentStockTotalDAO.getEquipStockTotalById(equipEvictionForm.getCustomer().getId(), EquipStockTotalType.KHO_KH, equipment.getEquipGroup().getId());
					if (khoKH == null) {
						khoKH = new EquipStockTotal();
						khoKH.setCreateDate(date);
						khoKH.setCreateUser(username);
						khoKH.setEquipGroup(equipment.getEquipGroup());
						khoKH.setQuantity(1);
						khoKH.setStockId(equipEvictionForm.getCustomer().getId());
						khoKH.setStockType(EquipStockTotalType.KHO_KH);
						khoKH = equipmentStockTotalDAO.createEquipStockTotal(khoKH);
					}
					if (khoKH != null) {
						khoKH.setQuantity(khoKH.getQuantity() - detail.getTotal());
						khoKH.setUpdateDate(date);
						khoKH.setUpdateUser(username);
						khoThuHoi.setQuantity(khoThuHoi.getQuantity() + detail.getTotal());
						khoThuHoi.setUpdateDate(date);
						khoThuHoi.setUpdateUser(username);
						equipmentStockTotalDAO.updateEquipmentStockTotal(khoKH);
						equipmentStockTotalDAO.updateEquipmentStockTotal(khoThuHoi);
					}
				}
				EquipHistory qHis = new EquipHistory();
//				qHis.setCreateDate(date);
//				qHis.setCreateUser(username);
//				qHis.setEquip(equipment);
//				if (equipment.getHealthStatus() != null) {
//					qHis.setHealthStatus(equipment.getHealthStatus());
//				}
//				if (equipment.getStockCode() != null) {
//					//qHis.setEquipWarehouseCode(equipment.getStockCode());
//				}
//				qHis.setObjectId(equipment.getStockId());
//				qHis.setObjectType(equipment.getStockType());
//				qHis.setUsageStatus(equipment.getUsageStatus());
//				qHis.setStatus(equipment.getStatus().getValue());
//				qHis.setTradeType(equipment.getTradeType());
//				qHis.setTradeStatus(equipment.getTradeStatus());
				qHis.setCreateDate(date);
				qHis.setCreateUser(username);
				qHis.setEquip(equipment);
				if (!StringUtility.isNullOrEmpty(equipment.getStockCode())) {
					//equipHistory.setEquipWarehouseCode(eq.getStockCode());
				}
				qHis.setHealthStatus(equipment.getHealthStatus());
				if (equipment.getStockId() != null) {
					qHis.setObjectId(equipment.getStockId());
				}
				if (equipment.getStockType() != null) {
					qHis.setObjectType(equipment.getStockType());
				}
				qHis.setFormId(equipEvictionForm.getId());
				qHis.setFormType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
				qHis.setObjectCode(equipment.getStockCode());
				qHis.setStockName(equipment.getStockName());
				qHis.setTableName(TableName.EQUIP_EVICTION_FORM_TABLE.getValue());
				qHis.setPriceActually(equipment.getPriceActually());
				qHis.setStatus(equipment.getStatus().getValue());
				qHis.setTradeStatus(equipment.getTradeStatus());
				qHis.setTradeType(equipment.getTradeType());
				qHis.setUsageStatus(equipment.getUsageStatus());
				equipMngDAO.createEquipHistory(qHis);
			}

			return equipEvictionForm;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipEvictionForm updateEquipEvictionForm(EquipEvictionForm equipEvictionForm, List<Equipment> lstEquipment, EquipRecordFilter<EquipRepairForm> filter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			Date date = equipEvictionForm.getUpdateDate();
			String username = equipEvictionForm.getUpdateUser();
			if (filter != null) {
				filter.setChangeDate(date);
				filter.setUserName(username);
			}
			if (equipEvictionForm.getFormStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {//trang thai bien ban la duyet
				 equipEvictionForm.setApproveDate(date);
			}
			equipMngDAO.updateEquipEvictionForm(equipEvictionForm);
			//Xu ly File
			if (filter != null) {
				if (filter.getLstEquipAttachFileId() != null && !filter.getLstEquipAttachFileId().isEmpty()) {
					//Xu ly xoa file
					BasicFilter<EquipAttachFile> filterB = new BasicFilter<EquipAttachFile>();
					filterB.setLstId(filter.getLstEquipAttachFileId());
					filterB.setObjectId(equipEvictionForm.getId());
					filterB.setObjectType(EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue());
					List<EquipAttachFile> lstFile = equipRecordDAO.searchEquipAttachFileByFilter(filterB);
					if (lstFile != null && !lstFile.isEmpty()) {
						for (EquipAttachFile fileVItem : lstFile) {
							equipRecordDAO.deletEquipAttachFile(fileVItem);
						}
					}
				}
				//Xu ly them hinh anh
				if (filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
					for (FileVO voFile : filter.getLstFileVo()) {
						EquipAttachFile equipAttFile = new EquipAttachFile();
						equipAttFile.setUrl(voFile.getUrl());
						equipAttFile.setObjectId(equipEvictionForm.getId());
						equipAttFile.setFileName(voFile.getFileName());
						equipAttFile.setThumbUrl(voFile.getThumbUrl());
						equipAttFile.setObjectType(EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue());
						equipAttFile.setCreateDate(filter.getChangeDate());
						equipAttFile.setCreateUser(filter.getUserName());
						equipRecordDAO.createEquipAttachFile(equipAttFile);
					}
				}
			}
			List<EquipEvictionFormDtl> lstDetail = equipMngDAO.getEquipEvictionFormDtlByEquipEvictionFormId(equipEvictionForm.getId());
//			List<EquipSuggestEvictionDTL> esEvictionDTL = new ArrayList<EquipSuggestEvictionDTL>();
//			if (equipEvictionForm.getShop() != null) {
//				esEvictionDTL = equipMngDAO.getListEquipSugEvictionDTL(equipEvictionForm.getId(), equipEvictionForm.getShop().getId());
//			}
			EquipStock stock = commonDAO.getEntityById(EquipStock.class, equipEvictionForm.getToStockId());
			String stockCode = null;
			String stockName = null;
			if (stock != null) {
				stockCode = stock.getCode();
				stockName = stock.getName();
			}
			if (StatusRecordsEquip.CANCELLATION.getValue().equals(equipEvictionForm.getFormStatus())) {
//				if (esEvictionDTL != null) {
//					for (int j = 0, size = esEvictionDTL.size(); j < size; j++) {
//						esEvictionDTL.get(j).setEquipment(null);
//						commonDAO.updateEntity(esEvictionDTL.get(j));
//					}
//				}
				for (int j = 0; j < lstDetail.size(); j++) {
					Equipment equip = lstDetail.get(j).getEquip();
					equip.setTradeType(null);
					equip.setTradeStatus(0);//ko giao dich
					equip.setUpdateDate(date);
					equip.setUpdateUser(username);
					equipMngDAO.updateEquipment(equip);

					EquipHistory qHis = new EquipHistory();
//					qHis.setCreateDate(date);
//					qHis.setCreateUser(username);
//					qHis.setEquip(equip);
//					if (equip.getHealthStatus() != null) {
//						qHis.setHealthStatus(equip.getHealthStatus());
//					}
//					if (equip.getStockCode() != null) {
//						//qHis.setEquipWarehouseCode(equip.getStockCode());
//					}
//					qHis.setObjectId(equip.getStockId());
//					qHis.setObjectType(equip.getStockType());
//					qHis.setUsageStatus(equip.getUsageStatus());
//					qHis.setStatus(equip.getStatus().getValue());
//					qHis.setTradeType(equip.getTradeType());
//					qHis.setTradeStatus(equip.getTradeStatus());
					qHis.setCreateDate(date);
					qHis.setCreateUser(username);
					qHis.setEquip(equip);
					if (!StringUtility.isNullOrEmpty(equip.getStockCode())) {
						//equipHistory.setEquipWarehouseCode(eq.getStockCode());
					}
					qHis.setHealthStatus(equip.getHealthStatus());
					if (equip.getStockId() != null) {
						qHis.setObjectId(equip.getStockId());
					}
					if (equip.getStockType() != null) {
						qHis.setObjectType(equip.getStockType());
					}
					qHis.setFormId(equipEvictionForm.getId());
					qHis.setFormType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
					qHis.setObjectCode(equip.getStockCode());
					qHis.setStockName(equip.getStockName());
					qHis.setTableName(TableName.EQUIP_EVICTION_FORM_TABLE.getValue());
					qHis.setPriceActually(equip.getPriceActually());
					qHis.setStatus(equip.getStatus().getValue());
					qHis.setTradeStatus(equip.getTradeStatus());
					qHis.setTradeType(equip.getTradeType());
					qHis.setUsageStatus(equip.getUsageStatus());
					equipMngDAO.createEquipHistory(qHis);
				}
				// luu lich su bien ban
				EquipFormHistory equipFormHistory = new EquipFormHistory();
				equipFormHistory.setActDate(date);
				equipFormHistory.setCreateDate(date);
				equipFormHistory.setCreateUser(username);
				equipFormHistory.setDeliveryStatus(equipEvictionForm.getActionStatus());
				equipFormHistory.setRecordId(equipEvictionForm.getId());
				equipFormHistory.setRecordStatus(equipEvictionForm.getFormStatus());
				equipFormHistory.setRecordType(EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue());
				equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
				return equipEvictionForm;
			}
			
			List<Equipment> lstEquipmentTemp = new ArrayList<Equipment>();
			for (int t = 0, tsize = lstEquipment.size(); t < tsize; t++) {
				lstEquipmentTemp.add(lstEquipment.get(t));
			}
			List<EquipEvictionFormDtl> lstDetailTemp = new ArrayList<EquipEvictionFormDtl>();
			List<EquipEvictionFormDtl> lstDetailBD = new ArrayList<EquipEvictionFormDtl>();
			for (int n = 0, nsize = lstDetail.size(); n < nsize; n++) {
				lstDetailTemp.add(lstDetail.get(n));
				lstDetailBD.add(lstDetail.get(n));
			}
			
			for (int i = 0; i < lstEquipment.size(); i++) {
				if (lstEquipment.size() > 0) {
					for (int j = 0; j < lstDetail.size(); j++) {
						if (lstDetail.get(j).getEquip() != null &&  lstEquipment.get(i).getId().equals(lstDetail.get(j).getEquip().getId())) {
							lstEquipment.remove(lstEquipment.get(i));
							lstDetail.remove(lstDetail.get(j));
							i = -1;
							j = -1;
							break;
						}
					}
				}
			}
			Boolean isNotChange = false;
			if (lstEquipmentTemp.size() > 0 && lstDetailTemp.size() > 0 && lstEquipment.size() == 0 && lstDetail.size() == 0) {
				lstEquipment = lstEquipmentTemp;
				lstDetail = lstDetailTemp;
				isNotChange = true;
			}
			
			
			for (int j = 0; j < lstDetail.size(); j++) {
				if (StatusRecordsEquip.APPROVED.getValue().equals(equipEvictionForm.getFormStatus()) || StatusRecordsEquip.DRAFT.getValue().equals(equipEvictionForm.getFormStatus())) {
					Equipment equip = lstDetail.get(j).getEquip();
//					equip.setTradeStatus(0);
//					equip.setTradeType(null);
//					equip.setUpdateDate(date);
//					equip.setUpdateUser(username);
//					equipMngDAO.updateEquipment(equip);
					equip.setTradeType(null);
					equip.setTradeStatus(0);//ko giao dich
					equip.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE); //trang thai : dang o kho	
					equip.setStockId(equipEvictionForm.getToStockId());
					equip.setStockType(EquipStockTotalType.parseValue(equipEvictionForm.getToStockType()));
					equip.setStockCode(stockCode);
					//set stock name
					equip.setStockName(stockName);
					equip.setUpdateDate(date);
					equip.setUpdateUser(equipEvictionForm.getCreateUser());
					equipMngDAO.updateEquipment(equip);
					EquipHistory qHis = new EquipHistory();
	//				qHis.setCreateDate(date);
	//				qHis.setCreateUser(username);
	//				qHis.setEquip(equip);
	//				if (equip.getHealthStatus() != null) {
	//					qHis.setHealthStatus(equip.getHealthStatus());
	//				}
	//				if (equip.getStockCode() != null) {
	//					//qHis.setEquipWarehouseCode(equip.getStockCode());
	//				}
	//				qHis.setObjectId(equip.getStockId());
	//				qHis.setObjectType(equip.getStockType());
	//				qHis.setUsageStatus(equip.getUsageStatus());
	//				qHis.setStatus(equip.getStatus().getValue());
	//				qHis.setTradeType(equip.getTradeType());
	//				qHis.setTradeStatus(equip.getTradeStatus());
					qHis.setCreateDate(date);
					qHis.setCreateUser(username);
					qHis.setEquip(equip);
					if (!StringUtility.isNullOrEmpty(equip.getStockCode())) {
						//equipHistory.setEquipWarehouseCode(eq.getStockCode());
					}
					qHis.setHealthStatus(equip.getHealthStatus());
					if (equip.getStockId() != null) {
						qHis.setObjectId(equip.getStockId());
					}
					if (equip.getStockType() != null) {
						qHis.setObjectType(equip.getStockType());
					}
					qHis.setFormId(equipEvictionForm.getId());
					qHis.setFormType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
					qHis.setObjectCode(equip.getStockCode());
					qHis.setStockName(equip.getStockName());
					qHis.setTableName(TableName.EQUIP_EVICTION_FORM_TABLE.getValue());
					qHis.setPriceActually(equip.getPriceActually());
					qHis.setStatus(equip.getStatus().getValue());
					qHis.setTradeStatus(equip.getTradeStatus());
					qHis.setTradeType(equip.getTradeType());
					qHis.setUsageStatus(equip.getUsageStatus());
					equipMngDAO.createEquipHistory(qHis);
				}
				equipMngDAO.deleteEquipEvictionFormDtl(lstDetail.get(j));
			}

			if (isNotChange) {
				// luu chi tiet bien ban
				for (int i = 0; i < lstEquipment.size(); i++) {
					Equipment equipment = lstEquipment.get(i);
					EquipEvictionFormDtl detail = new EquipEvictionFormDtl();
					detail.setCreateDate(date);
					detail.setCreateUser(username);
					detail.setEquip(equipment);
					/**tamvnm: luu ID bien ban giao nhan*/
					if (equipEvictionForm.getCustomer() != null) {
						List<Long> lstEquipId = new ArrayList<Long>();
						lstEquipId.add(equipment.getId());
						List<EquipmentExVO> lstequipVO = equipMngDAO.getListAllEquipmentDeliveryByCustomerId(equipEvictionForm.getCustomer().getId(), lstEquipId);
						if (lstequipVO != null && lstequipVO.size() > 0) {
							Long deliveryId = null;
							EquipDeliveryRecord deliveryRecord = null;
							for (int c = 0, csize = lstequipVO.size(); c < csize; c++) {
								if (lstequipVO.get(c).getId().equals(equipment.getId())) {
									deliveryId = lstequipVO.get(c).getEquipDeliveryId();
									break;
								}
							}
							if (deliveryId != null) {
								deliveryRecord = commonDAO.getEntityById(EquipDeliveryRecord.class, deliveryId);
							}
							
							if (deliveryRecord != null) {
								detail.setEquipDeliveryRecord(deliveryRecord);
							}
						}
					}
					detail.setSerial(equipment.getSerial());
					detail.setManufacturingYear(equipment.getManufacturingYear());
					detail.setHealthStatus(equipment.getHealthStatus() != null ? equipment.getHealthStatus() : null);
					if (equipment.getEquipGroup() != null) {
						detail.setEquipCategory(equipment.getEquipGroup().getEquipCategory());
						if (equipment.getEquipGroup().getEquipCategory() != null) {
							detail.setEquipCategoryName(equipment.getEquipGroup().getEquipCategory().getName());
						}
					}
					detail.setEquipGroup(equipment.getEquipGroup());
					if (equipment.getEquipGroup() != null) {
						detail.setEquipGroupName(equipment.getEquipGroup().getName());
						detail.setCapacityFrom(equipment.getEquipGroup().getCapacityFrom());
						detail.setCapacityTo(equipment.getEquipGroup().getCapacityTo());
					}
					
					detail.setEquipEvictionForm(equipEvictionForm);
					detail.setTotal(1);//mac dinh la 1
					
					equipMngDAO.createEquipEvictionFormDtl(detail);
					
					Boolean isExist = false;
					for (int c = 0, csize = lstDetailBD.size(); c < csize; c++) {
						if (equipment.getId().equals(lstDetailBD.get(c).getEquip().getId())) {
							isExist = true;
							break;
						}
					}
					
					
					if (equipEvictionForm.getFormStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {//trang thai bien ban la du thao
						equipment.setTradeType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
						equipment.setTradeStatus(1);//dang giao dich
						equipment.setUpdateDate(date);
						equipment.setUpdateUser(username);
						equipMngDAO.updateEquipment(equipment);
						if (!isExist) {
							EquipHistory qHis = new EquipHistory();
							qHis.setCreateDate(date);
							qHis.setCreateUser(username);
							qHis.setEquip(equipment);
							if (!StringUtility.isNullOrEmpty(equipment.getStockCode())) {
								//equipHistory.setEquipWarehouseCode(eq.getStockCode());
							}
							qHis.setHealthStatus(equipment.getHealthStatus());
							if (equipment.getStockId() != null) {
								qHis.setObjectId(equipment.getStockId());
							}
							if (equipment.getStockType() != null) {
								qHis.setObjectType(equipment.getStockType());
							}
							qHis.setFormId(equipEvictionForm.getId());
							qHis.setFormType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
							qHis.setObjectCode(equipment.getStockCode());
							qHis.setStockName(equipment.getStockName());
							qHis.setTableName(TableName.EQUIP_EVICTION_FORM_TABLE.getValue());
							qHis.setPriceActually(equipment.getPriceActually());
							qHis.setStatus(equipment.getStatus().getValue());
							qHis.setTradeStatus(equipment.getTradeStatus());
							qHis.setTradeType(equipment.getTradeType());
							qHis.setUsageStatus(equipment.getUsageStatus());
							equipMngDAO.createEquipHistory(qHis);
						}
						
					} else if (equipEvictionForm.getFormStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {//trang thai bien ban la duyet	
						equipEvictionForm.setApproveDate(date);
						equipMngDAO.updateEquipEvictionForm(equipEvictionForm);
						
						// luu lich su bien ban
						EquipFormHistory equipFormHistory = new EquipFormHistory();
						equipFormHistory.setActDate(date);
						equipFormHistory.setCreateDate(date);
						equipFormHistory.setCreateUser(username);
						equipFormHistory.setDeliveryStatus(equipEvictionForm.getActionStatus());
						equipFormHistory.setRecordId(equipEvictionForm.getId());
						equipFormHistory.setRecordStatus(equipEvictionForm.getFormStatus());
						equipFormHistory.setRecordType(EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue());
						equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
						
						equipment.setTradeType(null);
						equipment.setTradeStatus(0);//ko giao dich
						equipment.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE); //trang thai : dang o kho	
						equipment.setStockId(equipEvictionForm.getToStockId());
						equipment.setStockType(EquipStockTotalType.parseValue(equipEvictionForm.getToStockType()));
						equipment.setStockCode(stockCode);
						//set stock name
						equipment.setStockName(stockName);
						equipment.setUpdateDate(date);
						equipment.setUpdateUser(equipEvictionForm.getCreateUser());
						equipMngDAO.updateEquipment(equipment);
//						if (esEvictionDTL != null) {
//							for (int j = 0, size = esEvictionDTL.size(); j < size; j++) {
//								esEvictionDTL.get(j).setDeliveryStatus(1);
//								commonDAO.updateEntity(esEvictionDTL.get(j));
//							}
//							EquipSuggestEviction es = equipEvictionForm.getEquipSugEviction();
//							if (es != null) {
//								es.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
//								commonDAO.updateEntity(es);
//							}
//						}

						EquipStockTotal khoThuHoi = equipmentStockTotalDAO.getEquipStockTotalById(equipEvictionForm.getToStockId(), EquipStockTotalType.parseValue(equipEvictionForm.getToStockType()), equipment.getEquipGroup().getId());
						if (khoThuHoi == null) {
							khoThuHoi = new EquipStockTotal();
							khoThuHoi.setCreateDate(date);
							khoThuHoi.setCreateUser(username);
							khoThuHoi.setEquipGroup(equipment.getEquipGroup());
							khoThuHoi.setQuantity(0);
							khoThuHoi.setStockId(equipEvictionForm.getToStockId());
							khoThuHoi.setStockType(EquipStockTotalType.parseValue(equipEvictionForm.getToStockType()));
							khoThuHoi = equipmentStockTotalDAO.createEquipStockTotal(khoThuHoi);
						}
						EquipStockTotal khoKH = equipmentStockTotalDAO.getEquipStockTotalById(equipEvictionForm.getCustomer().getId(), EquipStockTotalType.KHO_KH, equipment.getEquipGroup().getId());
						if (khoKH == null) {
							khoKH = new EquipStockTotal();
							khoKH.setCreateDate(date);
							khoKH.setCreateUser(username);
							khoKH.setEquipGroup(equipment.getEquipGroup());
							khoKH.setQuantity(1);
							khoKH.setStockId(equipEvictionForm.getCustomer().getId());
							khoKH.setStockType(EquipStockTotalType.KHO_KH);
							khoKH = equipmentStockTotalDAO.createEquipStockTotal(khoKH);
						}
						if (khoKH != null) {
							khoKH.setQuantity(khoKH.getQuantity() - detail.getTotal());
							khoThuHoi.setQuantity(khoThuHoi.getQuantity() + detail.getTotal());
							/* hoanv25 - fix defect 220841 */
							khoKH.setUpdateDate(date);
							khoKH.setUpdateUser(username);
							khoThuHoi.setUpdateDate(date);
							khoThuHoi.setUpdateUser(username);
							/* end */
							equipmentStockTotalDAO.updateEquipmentStockTotal(khoKH);
							equipmentStockTotalDAO.updateEquipmentStockTotal(khoThuHoi);
						}
					} else if (equipEvictionForm.getFormStatus().intValue() == StatusRecordsEquip.CANCELLATION.getValue().intValue()) {//trang thai bien ban la huy
						equipment.setTradeType(null);
						equipment.setTradeStatus(0);//ko giao dich
						equipment.setUpdateDate(date);
						equipment.setUpdateUser(username);
						equipMngDAO.updateEquipment(equipment);
					}
				}
			} else {
				// luu chi tiet bien ban
				for (int i = 0; i < lstEquipment.size(); i++) {
					Equipment equipment = lstEquipment.get(i);
					EquipEvictionFormDtl detail = new EquipEvictionFormDtl();
					detail.setCreateDate(date);
					detail.setCreateUser(username);
					detail.setEquip(equipment);
					/**tamvnm: luu ID bien ban giao nhan*/
					if (equipEvictionForm.getCustomer() != null) {
						List<Long> lstEquipId = new ArrayList<Long>();
						lstEquipId.add(equipment.getId());
						List<EquipmentExVO> lstequipVO = equipMngDAO.getListAllEquipmentDeliveryByCustomerId(equipEvictionForm.getCustomer().getId(), lstEquipId);
						if (lstequipVO != null && lstequipVO.size() > 0) {
							Long deliveryId = null;
							EquipDeliveryRecord deliveryRecord = null;
							for (int c = 0, csize = lstequipVO.size(); c < csize; c++) {
								if (lstequipVO.get(c).getId().equals(equipment.getId())) {
									deliveryId = lstequipVO.get(c).getEquipDeliveryId();
									break;
								}
							}
							if (deliveryId != null) {
								deliveryRecord = commonDAO.getEntityById(EquipDeliveryRecord.class, deliveryId);
							}
							
							if (deliveryRecord != null) {
								detail.setEquipDeliveryRecord(deliveryRecord);
							}
						}
					}
					detail.setSerial(equipment.getSerial());
					detail.setManufacturingYear(equipment.getManufacturingYear());
					detail.setHealthStatus(equipment.getHealthStatus() != null ? equipment.getHealthStatus() : null);
					if (equipment.getEquipGroup() != null) {
						detail.setEquipCategory(equipment.getEquipGroup().getEquipCategory());
						if (equipment.getEquipGroup().getEquipCategory() != null) {
							detail.setEquipCategoryName(equipment.getEquipGroup().getEquipCategory().getName());
						}
					}
					detail.setEquipGroup(equipment.getEquipGroup());
					if (equipment.getEquipGroup() != null) {
						detail.setEquipGroupName(equipment.getEquipGroup().getName());
						detail.setCapacityFrom(equipment.getEquipGroup().getCapacityFrom());
						detail.setCapacityTo(equipment.getEquipGroup().getCapacityTo());
					}
					
					detail.setEquipEvictionForm(equipEvictionForm);
					detail.setTotal(1);//mac dinh la 1
					
					equipMngDAO.createEquipEvictionFormDtl(detail);
					if (equipEvictionForm.getFormStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {//trang thai bien ban la du thao
						equipment.setTradeType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
						equipment.setTradeStatus(1);//dang giao dich
						equipment.setUpdateDate(date);
						equipment.setUpdateUser(username);
						equipMngDAO.updateEquipment(equipment);
					} else if (equipEvictionForm.getFormStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {//trang thai bien ban la duyet	
						equipEvictionForm.setApproveDate(date);
						equipMngDAO.updateEquipEvictionForm(equipEvictionForm);
						
						// luu lich su bien ban
						EquipFormHistory equipFormHistory = new EquipFormHistory();
						equipFormHistory.setActDate(date);
						equipFormHistory.setCreateDate(date);
						equipFormHistory.setCreateUser(username);
						equipFormHistory.setDeliveryStatus(equipEvictionForm.getActionStatus());
						equipFormHistory.setRecordId(equipEvictionForm.getId());
						equipFormHistory.setRecordStatus(equipEvictionForm.getFormStatus());
						equipFormHistory.setRecordType(EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue());
						equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
						
						equipment.setTradeType(null);
						equipment.setTradeStatus(0);//ko giao dich
						equipment.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE); //trang thai : dang o kho	
						equipment.setStockId(equipEvictionForm.getToStockId());
						equipment.setStockType(EquipStockTotalType.parseValue(equipEvictionForm.getToStockType()));
						equipment.setStockCode(stockCode);
						//set stock name
						equipment.setStockName(stockName);
						equipment.setUpdateDate(date);
						equipment.setUpdateUser(equipEvictionForm.getCreateUser());
						equipMngDAO.updateEquipment(equipment);
//						if (esEvictionDTL != null) {
//							for (int j = 0, size = esEvictionDTL.size(); j < size; j++) {
//								esEvictionDTL.get(j).setDeliveryStatus(1);
//								commonDAO.updateEntity(esEvictionDTL.get(j));
//							}
//							EquipSuggestEviction es = equipEvictionForm.getEquipSugEviction();
//							if (es != null) {
//								es.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
//								commonDAO.updateEntity(es);
//							}
//						}
	
						EquipStockTotal khoThuHoi = equipmentStockTotalDAO.getEquipStockTotalById(equipEvictionForm.getToStockId(), EquipStockTotalType.parseValue(equipEvictionForm.getToStockType()), equipment.getEquipGroup().getId());
						if (khoThuHoi == null) {
							khoThuHoi = new EquipStockTotal();
							khoThuHoi.setCreateDate(date);
							khoThuHoi.setCreateUser(username);
							khoThuHoi.setEquipGroup(equipment.getEquipGroup());
							khoThuHoi.setQuantity(0);
							khoThuHoi.setStockId(equipEvictionForm.getToStockId());
							khoThuHoi.setStockType(EquipStockTotalType.parseValue(equipEvictionForm.getToStockType()));
							khoThuHoi = equipmentStockTotalDAO.createEquipStockTotal(khoThuHoi);
						}
						EquipStockTotal khoKH = equipmentStockTotalDAO.getEquipStockTotalById(equipEvictionForm.getCustomer().getId(), EquipStockTotalType.KHO_KH, equipment.getEquipGroup().getId());
						if (khoKH == null) {
							khoKH = new EquipStockTotal();
							khoKH.setCreateDate(date);
							khoKH.setCreateUser(username);
							khoKH.setEquipGroup(equipment.getEquipGroup());
							khoKH.setQuantity(1);
							khoKH.setStockId(equipEvictionForm.getCustomer().getId());
							khoKH.setStockType(EquipStockTotalType.KHO_KH);
							khoKH = equipmentStockTotalDAO.createEquipStockTotal(khoKH);
						}
						if (khoKH != null) {
							khoKH.setQuantity(khoKH.getQuantity() - detail.getTotal());
							khoThuHoi.setQuantity(khoThuHoi.getQuantity() + detail.getTotal());
							/* hoanv25 - fix defect 220841 */
							khoKH.setUpdateDate(date);
							khoKH.setUpdateUser(username);
							khoThuHoi.setUpdateDate(date);
							khoThuHoi.setUpdateUser(username);
							/* end */
							equipmentStockTotalDAO.updateEquipmentStockTotal(khoKH);
							equipmentStockTotalDAO.updateEquipmentStockTotal(khoThuHoi);
						}
					} else if (equipEvictionForm.getFormStatus().intValue() == StatusRecordsEquip.CANCELLATION.getValue().intValue()) {//trang thai bien ban la huy
						equipment.setTradeType(null);
						equipment.setTradeStatus(0);//ko giao dich
						equipment.setUpdateDate(date);
						equipment.setUpdateUser(username);
						equipMngDAO.updateEquipment(equipment);
					}
				}
			}
			return equipEvictionForm;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEquipEvictionFormStatus(EquipEvictionForm equipEvictionForm, String username, int status, List<EquipSuggestEvictionDTL> lstequipSug) throws BusinessException {
		try {
			Date now = commonDAO.getSysDate();
//			if (equipEvictionForm.getEquipSugEviction() != null && lstequipSug != null && lstequipSug.size() > 0) {
//				for (int i = 0, isize = lstequipSug.size(); i < isize; i++) {
//					commonDAO.updateEntity(lstequipSug.get(i));
//				}
//			}
			String stockCode = null;
			if (status == 2 || status == 1) {// trang thai tu du thao -> duyet : xoa ton kho 
				List<EquipEvictionFormDtl> lstDetail = equipMngDAO.getEquipEvictionFormDtlByEquipEvictionFormId(equipEvictionForm.getId());
				if (equipEvictionForm.getToStockId() != null ) {
//					Shop stock = shopDAO.getShopById(equipEvictionForm.getToStockId());
					EquipStock stock = commonDAO.getEntityById(EquipStock.class, equipEvictionForm.getToStockId());
					if (stock != null) {
						stockCode = stock.getCode();
					}
				}
				
				//cap nhat approvedDate
				equipEvictionForm.setApproveDate(now);
				if (lstDetail != null && lstDetail.size() > 0) {
					for (int i = 0; i < lstDetail.size(); i++) {
						EquipStockTotalType stockType = EquipStockTotalType.parseValue(equipEvictionForm.getToStockType());
						Equipment equip = lstDetail.get(i).getEquip();
						Long equipGroupId = equip.getEquipGroup().getId();
						if (status == 2) {
							EquipStockTotal khoKH = equipmentStockTotalDAO.getEquipStockTotalById(equipEvictionForm.getCustomer().getId(), EquipStockTotalType.KHO_KH, equipGroupId);
							EquipStockTotal khoThuHoi = equipmentStockTotalDAO.getEquipStockTotalById(equipEvictionForm.getToStockId(), stockType, equipGroupId);
							if (khoThuHoi == null) {
								khoThuHoi = new EquipStockTotal();
								khoThuHoi.setCreateDate(now);
								khoThuHoi.setCreateUser(equipEvictionForm.getCreateUser());
								khoThuHoi.setEquipGroup(equip.getEquipGroup());
								khoThuHoi.setQuantity(0);
								khoThuHoi.setStockId(equipEvictionForm.getToStockId());
								khoThuHoi.setStockType(EquipStockTotalType.parseValue(equipEvictionForm.getToStockType()));
								khoThuHoi = equipmentStockTotalDAO.createEquipStockTotal(khoThuHoi);
							}
							if (khoKH == null) {
								khoKH = new EquipStockTotal();
								khoKH.setCreateDate(now);
								khoKH.setCreateUser(username);
								khoKH.setEquipGroup(equip.getEquipGroup());
								khoKH.setQuantity(1);
								khoKH.setStockId(equipEvictionForm.getCustomer().getId());
								khoKH.setStockType(EquipStockTotalType.KHO_KH);
								khoKH = equipmentStockTotalDAO.createEquipStockTotal(khoKH);
							}
							if (khoKH != null) {
								khoKH.setQuantity(khoKH.getQuantity() - lstDetail.get(i).getTotal());
								khoThuHoi.setQuantity(khoThuHoi.getQuantity() + lstDetail.get(i).getTotal());
								/* hoanv25 - fix defect 220841 */
								khoKH.setUpdateDate(now);
								khoKH.setUpdateUser(username);
								khoThuHoi.setUpdateDate(now);
								khoThuHoi.setUpdateUser(username);
								/* end */
								equipmentStockTotalDAO.updateEquipmentStockTotal(khoKH);
								equipmentStockTotalDAO.updateEquipmentStockTotal(khoThuHoi);
							}
							equip.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE); //trang thai thiet bi : dang o kho
							equip.setStockId(equipEvictionForm.getToStockId());
							equip.setStockType(EquipStockTotalType.parseValue(equipEvictionForm.getToStockType()));
							equip.setStockCode(stockCode);
							//set stock name
							equip.setStockName(equipEvictionForm.getToStockName());
							
//							if (equipEvictionForm.getShop() != null) {
//								List<EquipSuggestEvictionDTL> esEvictionDTL = equipMngDAO.getListEquipSugEvictionDTL(equipEvictionForm.getId(), equipEvictionForm.getShop().getId());
//								if (esEvictionDTL != null) {
//									for (int j = 0, size = esEvictionDTL.size(); j < size; j++) {
//										esEvictionDTL.get(j).setDeliveryStatus(1);
//										commonDAO.updateEntity(esEvictionDTL.get(j));
//									}
//									
//								}
//								EquipSuggestEviction es = equipEvictionForm.getEquipSugEviction();
//								if (es != null) {
//									es.setStatus(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
//									commonDAO.updateEntity(es);
//								}
//							}
//						} else if (status == 1) {
						} else {
//							if (equipEvictionForm.getShop() != null) {
//								List<EquipSuggestEvictionDTL> esEvictionDTL = equipMngDAO.getListEquipSugEvictionDTL(equipEvictionForm.getId(), equipEvictionForm.getShop().getId());
//								if (esEvictionDTL != null) {
//									for (int j = 0, size = esEvictionDTL.size(); j < size; j++) {
//										esEvictionDTL.get(j).setEquipment(null);
//										commonDAO.updateEntity(esEvictionDTL.get(j));
//									}
//								}
//							}
						}
						equip.setTradeType(null);
						equip.setTradeStatus(0);//trang thai giao dich : khong giao dich
						equip.setUpdateDate(now);
						equip.setUpdateUser(username);
						equipMngDAO.updateEquipment(equip);
						
						
						//luu lich su thiet bi
						EquipHistory qHis = new EquipHistory();
						qHis.setCreateDate(now);
						qHis.setCreateUser(username);
						qHis.setEquip(equip);
						if (!StringUtility.isNullOrEmpty(equip.getStockCode())) {
							//equipHistory.setEquipWarehouseCode(eq.getStockCode());
						}
						qHis.setHealthStatus(equip.getHealthStatus());
						if (equip.getStockId() != null) {
							qHis.setObjectId(equip.getStockId());
						}
						if (equip.getStockType() != null) {
							qHis.setObjectType(equip.getStockType());
						}
						qHis.setFormId(equipEvictionForm.getId());
						qHis.setFormType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
						qHis.setObjectCode(equip.getStockCode());
						qHis.setStockName(equip.getStockName());
						qHis.setTableName(TableName.EQUIP_EVICTION_FORM_TABLE.getValue());
						qHis.setPriceActually(equip.getPriceActually());
						qHis.setStatus(equip.getStatus().getValue());
						qHis.setTradeStatus(equip.getTradeStatus());
						qHis.setTradeType(equip.getTradeType());
						qHis.setUsageStatus(equip.getUsageStatus());
						equipMngDAO.createEquipHistory(qHis);
					}
				}
			}
			// ghi lich su
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setDeliveryStatus(equipEvictionForm.getActionStatus());
			equipFormHistory.setRecordStatus(equipEvictionForm.getFormStatus());
			equipFormHistory.setRecordId(equipEvictionForm.getId());
			equipFormHistory.setRecordType(EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue());
			equipFormHistory.setActDate(now);
			equipFormHistory.setCreateDate(now);
			equipFormHistory.setCreateUser(username);
			equipMngDAO.updateEquipEvictionForm(equipEvictionForm);
			equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipEvictionForm getEquipEvictionFormById(long id) throws BusinessException {
		try {
			return equipMngDAO.getEquipEvictionFormById(id);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipRecordVO> getHistoryEquipRecordByFilter(EquipRecordFilter<EquipRecordVO> filter) throws BusinessException {
		try {
			List<EquipRecordVO> lst = equipMngDAO.getHistoryEquipRecordByFilter(filter);
			ObjectVO<EquipRecordVO> objVO = new ObjectVO<EquipRecordVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipmentEvictionVO> getHistory(Long idRecord, EquipTradeType recordType) throws BusinessException {
		try {
			List<EquipmentEvictionVO> lst = equipMngDAO.getHistory(idRecord, recordType);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipEvictionFormDtl> getEquipEvictionFormDtlByEquipEvictionFormId(Long equipEvictionFormId) throws BusinessException {
		try {
			List<EquipEvictionFormDtl> lst = equipMngDAO.getEquipEvictionFormDtlByEquipEvictionFormId(equipEvictionFormId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentRecordDeliveryVO> getListEquipmentRecordDeliveryVOByFilter(EquipmentFilter<EquipmentRecordDeliveryVO> equipmentFilter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<EquipmentRecordDeliveryVO> lst = equipmentRecordDeliveryDAO.getListEquipmentRecordDeliveryVOByFilter(equipmentFilter);
			ObjectVO<EquipmentRecordDeliveryVO> objVO = new ObjectVO<EquipmentRecordDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentDeliveryVO> getListEquipmentDeliveryVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<EquipmentDeliveryVO> lst = equipMngDAO.getListEquipmentDeliveryVOByFilter(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> objVO = new ObjectVO<EquipmentDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentDeliveryVO> getListEquipmentDeliveryVOByFilterEx(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<EquipmentDeliveryVO> lst = equipMngDAO.getListEquipmentDeliveryVOByFilterEx(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> objVO = new ObjectVO<EquipmentDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<EquipmentDeliveryVO> getListEquipmentPopUp(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException {
		try {
			List<EquipmentDeliveryVO> lst = equipMngDAO.getListEquipmentPopUp(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> objVO = new ObjectVO<EquipmentDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Equipment getEquipmentEntityByCode(String code) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return equipMngDAO.getEquipmentByCode(code);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Equipment getEquipmentByCodeHasPermission(String code,Long staffRootId,Long shopRootId) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return equipMngDAO.getEquipmentByCodeHasPermission(code, staffRootId, shopRootId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Equipment getEquipmentById(Long id) throws BusinessException {
		try {
			return equipMngDAO.getEquipmentById(id);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Equipment getEquipmentById(Long equipmentId, Long shopId) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return equipmentId == null ? null : equipMngDAO.getEquipmentById(equipmentId, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipPeriod getEquipPeriodCurrent() throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return equipmentPeriodDAO.getEquipPeriodCurrent();
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipmentDeliveryReturnVO createRecordDeliveryByImportExcel(EquipDeliveryRecord eqDeliveryRecord, List<EquipDeliveryRecDtl> lstDeliveryRecDtls, EquipRecordFilter<EquipmentDeliveryVO> filter) throws BusinessException {
		int maxLengthCode = 8;
		try {
			// luu bien ban
			Date sysDateIns = commonDAO.getSysDate();
			String code = equipmentRecordDeliveryDAO.getCodeRecordDelivery();
			//			String code = "00000000" + (Long.parseLong(object.toString()) + 1);
			//			code = code.substring(code.length() - 8);
			//			code = "GN" + code;
			eqDeliveryRecord.setCode(code);
			eqDeliveryRecord = equipmentRecordDeliveryDAO.createEquipmentRecordDelivery(eqDeliveryRecord);

			// luu lich su bien ban
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(sysDateIns);
			equipFormHistory.setCreateDate(sysDateIns);
			equipFormHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
			equipFormHistory.setDeliveryStatus(eqDeliveryRecord.getDeliveryStatus());
			equipFormHistory.setRecordId(eqDeliveryRecord.getId());
			equipFormHistory.setRecordStatus(eqDeliveryRecord.getRecordStatus());
			equipFormHistory.setRecordType(EquipTradeType.DELIVERY.getValue());
			Staff staff = staffDAO.getStaffByCode(eqDeliveryRecord.getCreateUser());
			equipFormHistory.setStaff(staff);
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);

			EquipItemFilter filterVO = new EquipItemFilter();
			filterVO.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
			filterVO.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
			filterVO.setStatus(ActiveType.RUNNING);
			EquipmentDeliveryReturnVO equipDeliveryReturnVO = new EquipmentDeliveryReturnVO();
			List<Equipment> lstEquipment = new ArrayList<Equipment>();
			Map<Long, String> mapMaxCode = new HashMap<Long, String>();
			// luu chi tiet bien ban
			for (int i = 0; i < lstDeliveryRecDtls.size(); i++) {
				//neu la cap moi
				if (lstDeliveryRecDtls.get(i).getContent().equals(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue())) {
					//tao moi thiet bi
					Equipment e = lstDeliveryRecDtls.get(i).getEquipment();
					String maxEquipmentCode = null;
					if (e.getEquipGroup() != null && e.getEquipGroup().getEquipCategory() != null) {
						EquipCategory ec = e.getEquipGroup().getEquipCategory();
						
						if (mapMaxCode.containsKey(ec.getId()) == true) {
							maxEquipmentCode = this.getEquipmentCodeByMaxCodeAndPrefixMaxLength(mapMaxCode.get(ec.getId()), ec.getCode(), maxLengthCode);
							mapMaxCode.put(ec.getId(), maxEquipmentCode);
						} else {
							maxEquipmentCode = equipMngDAO.getMaxEquipmentCodeByEquipCategoryId(ec.getId());
							maxEquipmentCode = this.getEquipmentCodeByMaxCodeAndPrefixMaxLength(maxEquipmentCode, ec.getCode(), maxLengthCode);
							mapMaxCode.put(ec.getId(), maxEquipmentCode);
						}
					}
					e.setCode(maxEquipmentCode);
					commonDAO.createEntity(e);
					EquipDeliveryRecDtl ed = lstDeliveryRecDtls.get(i);
					if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
//						e.setTradeType(EquipTradeType.DELIVERY);
//						e.setTradeStatus(EquipTradeStatus.TRADING.getValue());
//						e.setUpdateDate(sysDateIns);
//						e.setUpdateUser(eqDeliveryRecord.getCreateUser());
//						equipMngDAO.updateEquipment(e);
						
						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
						equipHistory.setEquip(e);
						
						equipHistory.setHealthStatus(e.getHealthStatus());
						if (e.getStockId() != null) {
							equipHistory.setObjectId(e.getStockId());
						}
						if (e.getStockType() != null) {
							equipHistory.setObjectType(e.getStockType());
						}
						
						equipHistory.setFormId(eqDeliveryRecord.getId());
						equipHistory.setFormType(EquipTradeType.DELIVERY);
						equipHistory.setObjectCode(e.getStockCode());
						equipHistory.setStockName(e.getStockName());
						equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
						equipHistory.setPriceActually(e.getPriceActually());
						equipHistory.setStatus(e.getStatus().getValue());
						equipHistory.setTradeStatus(e.getTradeStatus());
						equipHistory.setTradeType(e.getTradeType());
						equipHistory.setUsageStatus(e.getUsageStatus());
						equipMngDAO.createEquipHistory(equipHistory);
	
					} else if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
						EquipStockTotal equipStockTotal = null;
						
						if (ed.getContent().equals(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue())) {
							EquipStockEquipFilter<EquipStockTotal> filterSt = new EquipStockEquipFilter<EquipStockTotal>();
							filterSt.setEquipGroupId(e.getEquipGroup().getId());
							filterSt.setStockType(EquipStockTotalType.KHO_TONG.getValue());  // kho hien tai chon
							EquipStockTotal total = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
							/**kiem tra kho tong co ton tai ko*/
							if(total!=null){ /**TH1: Ton tai kho tong*/
								/**Cap nhat kho tong*/
								if (total.getQuantity() != null) {
									total.setQuantity(total.getQuantity() + 1);
								} else {
									total.setQuantity(1);
								}
								total.setStockType(EquipStockTotalType.KHO_TONG); // Thay kho hien tai chon
								total.setUpdateUser(eqDeliveryRecord.getCreateUser());
								total.setUpdateDate(sysDateIns);
								equipMngDAO.updateEquipStockTotal(total);
								
							}else{ /**TH2: Chua ton tai kho tong*/
								/**Them moi kho tong*/
								total = new EquipStockTotal();
								total.setStockId(null);
								total.setEquipGroup(e.getEquipGroup());
								total.setStockType(EquipStockTotalType.KHO_TONG);
								total.setQuantity(1);
								total.setCreateUser(e.getCreateUser());
								total.setCreateDate(sysDateIns);
								total = equipMngDAO.createEquipStockTotal(total);
							}
						} else {
							// xoa kho VNM hoac kho NPP
							equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(e.getStockId(), EquipStockTotalType.parseValue(e.getStockType().getValue()), e.getEquipGroup().getId());
							equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
							equipStockTotal.setUpdateDate(sysDateIns);
							equipStockTotal.setUpdateUser(eqDeliveryRecord.getUpdateUser());
							equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
						}
	
						//cap nhat approvedDate
						eqDeliveryRecord.setApproveDate(sysDateIns);
						commonDAO.updateEntity(eqDeliveryRecord);
						
						// cap nhat kho kh
						equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eqDeliveryRecord.getCustomer().getId(), EquipStockTotalType.KHO_KH, e.getEquipGroup().getId());
						if (equipStockTotal != null) {
							// cap nhat
							if (equipStockTotal.getQuantity() == null) {
								equipStockTotal.setQuantity(1);
							} else {
								equipStockTotal.setQuantity(equipStockTotal.getQuantity() + 1);
							}
							equipStockTotal.setUpdateDate(sysDateIns);
							equipStockTotal.setUpdateUser(eqDeliveryRecord.getCreateUser());
							equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
						} else {
							// tao moi
							equipStockTotal = new EquipStockTotal();
							equipStockTotal.setCreateDate(sysDateIns);
							equipStockTotal.setCreateUser(eqDeliveryRecord.getCreateUser());
							equipStockTotal.setEquipGroup(e.getEquipGroup());
							equipStockTotal.setQuantity(1);
							equipStockTotal.setStockId(eqDeliveryRecord.getCustomer().getId());
							equipStockTotal.setStockType(EquipStockTotalType.KHO_KH);
							equipStockTotal = equipmentStockTotalDAO.createEquipStockTotal(equipStockTotal);
	
						}
	//					eq.setStockCode(eqDeliveryRecord.getCustomer().getShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
						e.setStockCode(eqDeliveryRecord.getToShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
						e.setStockId(eqDeliveryRecord.getCustomer().getId());
						e.setStockName(eqDeliveryRecord.getCustomer().getCustomerName());
						e.setStockType(EquipStockTotalType.KHO_KH);
						e.setUsageStatus(EquipUsageStatus.IS_USED);
						e.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						e.setTradeType(null);
						if (e.getFirstDateInUse() == null) {
							e.setFirstDateInUse(sysDateIns);
						}
						e.setUpdateDate(sysDateIns);
						e.setUpdateUser(eqDeliveryRecord.getCreateUser());
						equipMngDAO.updateEquipment(e);
	
						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
						equipHistory.setEquip(e);
						if (!StringUtility.isNullOrEmpty(e.getStockCode())) {
							//equipHistory.setEquipWarehouseCode(eq.getStockCode());
						}
						equipHistory.setHealthStatus(e.getHealthStatus());
						if (e.getStockId() != null) {
							equipHistory.setObjectId(e.getStockId());
						}
						if (e.getStockType() != null) {
							equipHistory.setObjectType(e.getStockType());
						}
						equipHistory.setFormId(eqDeliveryRecord.getId());
						equipHistory.setFormType(EquipTradeType.DELIVERY);
						equipHistory.setObjectCode(e.getStockCode());
						equipHistory.setStockName(e.getStockName());
						equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
						equipHistory.setPriceActually(e.getPriceActually());
						equipHistory.setStatus(e.getStatus().getValue());
						equipHistory.setTradeStatus(e.getTradeStatus());
						equipHistory.setTradeType(e.getTradeType());
						equipHistory.setUsageStatus(e.getUsageStatus());
						equipMngDAO.createEquipHistory(equipHistory);
					}
					// luu chi tiet bien ban
					ed.setEquipDeliveryRecord(eqDeliveryRecord);
					ed.setHealthStatus(ed.getEquipment().getHealthStatus());
					ed.setPrice(ed.getEquipment().getPrice());
					ed = equipmentRecordDeliveryDAO.createEquipmentRecordDeliveryDetail(ed);
				} else {// khong phai cap moi
					EquipDeliveryRecDtl ed = lstDeliveryRecDtls.get(i);
					Equipment eq = ed.getEquipment();
					filterVO.setEquipId(eq.getId());
					Equipment eqInDB = equipMngDAO.getEquipmentByIdAndBlock(filterVO);
					if (eqInDB != null) {
						// luu thay doi thiet bi trong chi tiet bien ban
						eqInDB.setSerial(eq.getSerial());//set so seri lai khi lay equipment tu DB len
						if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
							eqInDB.setTradeType(EquipTradeType.DELIVERY);
							eqInDB.setTradeStatus(EquipTradeStatus.TRADING.getValue());
							eqInDB.setUpdateDate(sysDateIns);
							eqInDB.setUpdateUser(eqDeliveryRecord.getCreateUser());
							equipMngDAO.updateEquipment(eqInDB);
							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
							equipHistory.setEquip(eqInDB);
							if (!StringUtility.isNullOrEmpty(eqInDB.getStockCode())) {
								//equipHistory.setEquipWarehouseCode(eq.getStockCode());
							}
							equipHistory.setHealthStatus(eqInDB.getHealthStatus());
							if (eqInDB.getStockId() != null) {
								equipHistory.setObjectId(eqInDB.getStockId());
							}
							if (eqInDB.getStockType() != null) {
								equipHistory.setObjectType(eqInDB.getStockType());
							}
							equipHistory.setFormId(eqDeliveryRecord.getId());
							equipHistory.setFormType(EquipTradeType.DELIVERY);
							equipHistory.setObjectCode(eqInDB.getStockCode());
							equipHistory.setStockName(eqInDB.getStockName());
							equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
							equipHistory.setPriceActually(eqInDB.getPriceActually());
							equipHistory.setStatus(eqInDB.getStatus().getValue());
							equipHistory.setTradeStatus(eqInDB.getTradeStatus());
							equipHistory.setTradeType(eqInDB.getTradeType());
							equipHistory.setUsageStatus(eqInDB.getUsageStatus());
							equipMngDAO.createEquipHistory(equipHistory);
		
						} else if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
							// xoa kho vnm, npp
							EquipStockTotal equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eqInDB.getStockId(), EquipStockTotalType.parseValue(eqInDB.getStockType().getValue()), eqInDB.getEquipGroup().getId());
							equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
							equipStockTotal.setUpdateDate(sysDateIns);
							equipStockTotal.setUpdateUser(eqDeliveryRecord.getCreateUser());
							equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
		
							//cap nhat approvedDate
							eqDeliveryRecord.setApproveDate(sysDateIns);
							commonDAO.updateEntity(eqDeliveryRecord);
							
							// cap nhat kho kh
							equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eqDeliveryRecord.getCustomer().getId(), EquipStockTotalType.KHO_KH, eqInDB.getEquipGroup().getId());
							if (equipStockTotal != null) {
								// cap nhat
								if (equipStockTotal.getQuantity() == null) {
									equipStockTotal.setQuantity(1);
								} else {
									equipStockTotal.setQuantity(equipStockTotal.getQuantity() + 1);
								}
								equipStockTotal.setUpdateDate(sysDateIns);
								equipStockTotal.setUpdateUser(eqDeliveryRecord.getCreateUser());
								equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
							} else {
								// tao moi
								equipStockTotal = new EquipStockTotal();
								equipStockTotal.setCreateDate(sysDateIns);
								equipStockTotal.setCreateUser(eqDeliveryRecord.getCreateUser());
								equipStockTotal.setEquipGroup(eqInDB.getEquipGroup());
								equipStockTotal.setQuantity(1);
								equipStockTotal.setStockId(eqDeliveryRecord.getCustomer().getId());
								equipStockTotal.setStockType(EquipStockTotalType.KHO_KH);
								equipStockTotal = equipmentStockTotalDAO.createEquipStockTotal(equipStockTotal);
		
							}
		//					eq.setStockCode(eqDeliveryRecord.getCustomer().getShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
							eqInDB.setStockCode(eqDeliveryRecord.getToShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
							eqInDB.setStockId(eqDeliveryRecord.getCustomer().getId());
							eqInDB.setStockName(eqDeliveryRecord.getCustomer().getCustomerName());
							eqInDB.setStockType(EquipStockTotalType.KHO_KH);
							eqInDB.setUsageStatus(EquipUsageStatus.IS_USED);
							eqInDB.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
							eqInDB.setTradeType(null);
							if (eqInDB.getFirstDateInUse() == null) {
								eqInDB.setFirstDateInUse(sysDateIns);
							}
							eqInDB.setUpdateDate(sysDateIns);
							eqInDB.setUpdateUser(eqDeliveryRecord.getCreateUser());
							equipMngDAO.updateEquipment(eqInDB);
		
							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
							equipHistory.setEquip(eqInDB);
							if (!StringUtility.isNullOrEmpty(eqInDB.getStockCode())) {
								//equipHistory.setEquipWarehouseCode(eq.getStockCode());
							}
							equipHistory.setHealthStatus(eqInDB.getHealthStatus());
							if (eqInDB.getStockId() != null) {
								equipHistory.setObjectId(eqInDB.getStockId());
							}
							if (eqInDB.getStockType() != null) {
								equipHistory.setObjectType(eqInDB.getStockType());
							}
							equipHistory.setFormId(eqDeliveryRecord.getId());
							equipHistory.setFormType(EquipTradeType.DELIVERY);
							equipHistory.setObjectCode(eqInDB.getStockCode());
							equipHistory.setStockName(eqInDB.getStockName());
							equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
							equipHistory.setPriceActually(eqInDB.getPriceActually());
							equipHistory.setStatus(eqInDB.getStatus().getValue());
							equipHistory.setTradeStatus(eqInDB.getTradeStatus());
							equipHistory.setTradeType(eqInDB.getTradeType());
							equipHistory.setUsageStatus(eqInDB.getUsageStatus());
							equipMngDAO.createEquipHistory(equipHistory);
						}
						// luu chi tiet bien ban
						ed.setEquipDeliveryRecord(eqDeliveryRecord);
						ed.setHealthStatus(ed.getEquipment().getHealthStatus());
						ed.setPrice(ed.getEquipment().getPrice());
//						ed.setPrice(Integer.parseInt(ed.getEquipment().getPrice()+""));
						ed = equipmentRecordDeliveryDAO.createEquipmentRecordDeliveryDetail(ed);
					} else {
						lstEquipment.add(eq);
					}
				}
				
			} //end for

			//Xu ly them file
			if (filter != null) {
				if (filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
					for (FileVO voFile : filter.getLstFileVo()) {
						EquipAttachFile equipAttFile = new EquipAttachFile();
						equipAttFile.setUrl(voFile.getUrl());
						equipAttFile.setObjectId(eqDeliveryRecord.getId());
						equipAttFile.setFileName(voFile.getFileName());
						equipAttFile.setThumbUrl(voFile.getThumbUrl());
						equipAttFile.setObjectType(EquipTradeType.DELIVERY.getValue());
						equipAttFile.setCreateDate(sysDateIns);
						equipAttFile.setCreateUser(eqDeliveryRecord.getCreateUser());
						equipRecordDAO.createEquipAttachFile(equipAttFile);
					}
				}
			}
			equipDeliveryReturnVO.setEquipDeliveryRevord(eqDeliveryRecord);
			return equipDeliveryReturnVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

//	@Override
//	@Transactional(rollbackFor = Exception.class)
//	public EquipmentDeliveryReturnVO createRecordDeliveryByImportExcel(EquipDeliveryRecord eqDeliveryRecord, List<EquipDeliveryRecDtl> lstDeliveryRecDtls, EquipRecordFilter<EquipmentDeliveryVO> filter) throws BusinessException {
//		// TODO Auto-generated method stub
//		try {
//			// luu bien ban
//			Date sysDateIns = commonDAO.getSysDate();
//			String code = equipmentRecordDeliveryDAO.getCodeRecordDelivery();
//			//			String code = "00000000" + (Long.parseLong(object.toString()) + 1);
//			//			code = code.substring(code.length() - 8);
//			//			code = "GN" + code;
//			eqDeliveryRecord.setCode(code);
//			eqDeliveryRecord = equipmentRecordDeliveryDAO.createEquipmentRecordDelivery(eqDeliveryRecord);
//
//			// luu lich su bien ban
//			EquipFormHistory equipFormHistory = new EquipFormHistory();
//			equipFormHistory.setActDate(sysDateIns);
//			equipFormHistory.setCreateDate(sysDateIns);
//			equipFormHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
//			equipFormHistory.setDeliveryStatus(eqDeliveryRecord.getDeliveryStatus());
//			equipFormHistory.setRecordId(eqDeliveryRecord.getId());
//			equipFormHistory.setRecordStatus(eqDeliveryRecord.getRecordStatus());
//			equipFormHistory.setRecordType(EquipTradeType.DELIVERY.getValue());
//			Staff staff = staffDAO.getStaffByCode(eqDeliveryRecord.getCreateUser());
//			equipFormHistory.setStaff(staff);
//			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
//
//			EquipItemFilter filterVO = new EquipItemFilter();
//			filterVO.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
//			filterVO.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
//			filterVO.setStatus(ActiveType.RUNNING);
//			EquipmentDeliveryReturnVO equipDeliveryReturnVO = new EquipmentDeliveryReturnVO();
//			List<Equipment> lstEquipment = new ArrayList<Equipment>();
//			// luu chi tiet bien ban
//			for (int i = 0; i < lstDeliveryRecDtls.size(); i++) {
//				EquipDeliveryRecDtl ed = lstDeliveryRecDtls.get(i);
//				Equipment eq = ed.getEquipment();
//				filterVO.setEquipId(eq.getId());
//				Equipment eqInDB = equipMngDAO.getEquipmentByIdAndBlock(filterVO);
//				if (eqInDB != null) {
//					// luu thay doi thiet bi trong chi tiet bien ban
//					eqInDB.setSerial(eq.getSerial());//set so seri lai khi lay equipment tu DB len
//					if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
//						eqInDB.setTradeType(EquipTradeType.DELIVERY);
//						eqInDB.setTradeStatus(EquipTradeStatus.TRADING.getValue());
//						eqInDB.setUpdateDate(sysDateIns);
//						eqInDB.setUpdateUser(eqDeliveryRecord.getCreateUser());
//						equipMngDAO.updateEquipment(eqInDB);
//						// luu lich su thiet bi
//						EquipHistory equipHistory = new EquipHistory();
//						equipHistory.setCreateDate(sysDateIns);
//						equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
//						equipHistory.setEquip(eqInDB);
//						if (!StringUtility.isNullOrEmpty(eqInDB.getStockCode())) {
//							//equipHistory.setEquipWarehouseCode(eq.getStockCode());
//						}
//						equipHistory.setHealthStatus(eqInDB.getHealthStatus());
//						if (eqInDB.getStockId() != null) {
//							equipHistory.setObjectId(eqInDB.getStockId());
//						}
//						if (eqInDB.getStockType() != null) {
//							equipHistory.setObjectType(eqInDB.getStockType());
//						}
//						equipHistory.setFormId(eqDeliveryRecord.getId());
//						equipHistory.setFormType(EquipTradeType.DELIVERY);
//						equipHistory.setObjectCode(eqInDB.getStockCode());
//						equipHistory.setStockName(eqInDB.getStockName());
//						equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
//						equipHistory.setPriceActually(eqInDB.getPriceActually());
//						equipHistory.setStatus(eqInDB.getStatus().getValue());
//						equipHistory.setTradeStatus(eqInDB.getTradeStatus());
//						equipHistory.setTradeType(eqInDB.getTradeType());
//						equipHistory.setUsageStatus(eqInDB.getUsageStatus());
//						equipMngDAO.createEquipHistory(equipHistory);
//	
//					} else if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
//						// xoa kho vnm, npp
//						EquipStockTotal equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eqInDB.getStockId(), EquipStockTotalType.parseValue(eqInDB.getStockType().getValue()), eqInDB.getEquipGroup().getId());
//						equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
//						equipStockTotal.setUpdateDate(sysDateIns);
//						equipStockTotal.setUpdateUser(eqDeliveryRecord.getCreateUser());
//						equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
//	
//						//cap nhat approvedDate
//						eqDeliveryRecord.setApproveDate(sysDateIns);
//						commonDAO.updateEntity(eqDeliveryRecord);
//						
//						// cap nhat kho kh
//						equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eqDeliveryRecord.getCustomer().getId(), EquipStockTotalType.KHO_KH, eqInDB.getEquipGroup().getId());
//						if (equipStockTotal != null) {
//							// cap nhat
//							if (equipStockTotal.getQuantity() == null) {
//								equipStockTotal.setQuantity(1);
//							} else {
//								equipStockTotal.setQuantity(equipStockTotal.getQuantity() + 1);
//							}
//							equipStockTotal.setUpdateDate(sysDateIns);
//							equipStockTotal.setUpdateUser(eqDeliveryRecord.getCreateUser());
//							equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
//						} else {
//							// tao moi
//							equipStockTotal = new EquipStockTotal();
//							equipStockTotal.setCreateDate(sysDateIns);
//							equipStockTotal.setCreateUser(eqDeliveryRecord.getCreateUser());
//							equipStockTotal.setEquipGroup(eqInDB.getEquipGroup());
//							equipStockTotal.setQuantity(1);
//							equipStockTotal.setStockId(eqDeliveryRecord.getCustomer().getId());
//							equipStockTotal.setStockType(EquipStockTotalType.KHO_KH);
//							equipStockTotal = equipmentStockTotalDAO.createEquipStockTotal(equipStockTotal);
//	
//						}
//	//					eq.setStockCode(eqDeliveryRecord.getCustomer().getShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
//						eqInDB.setStockCode(eqDeliveryRecord.getToShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
//						eqInDB.setStockId(eqDeliveryRecord.getCustomer().getId());
//						eqInDB.setStockName(eqDeliveryRecord.getCustomer().getCustomerName());
//						eqInDB.setStockType(EquipStockTotalType.KHO_KH);
//						eqInDB.setUsageStatus(EquipUsageStatus.IS_USED);
//						eqInDB.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
//						eqInDB.setTradeType(null);
//						if (eqInDB.getFirstDateInUse() == null) {
//							eqInDB.setFirstDateInUse(sysDateIns);
//						}
//						eqInDB.setUpdateDate(sysDateIns);
//						eqInDB.setUpdateUser(eqDeliveryRecord.getCreateUser());
//						equipMngDAO.updateEquipment(eqInDB);
//	
//						// luu lich su thiet bi
//						EquipHistory equipHistory = new EquipHistory();
//						equipHistory.setCreateDate(sysDateIns);
//						equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
//						equipHistory.setEquip(eqInDB);
//						if (!StringUtility.isNullOrEmpty(eqInDB.getStockCode())) {
//							//equipHistory.setEquipWarehouseCode(eq.getStockCode());
//						}
//						equipHistory.setHealthStatus(eqInDB.getHealthStatus());
//						if (eqInDB.getStockId() != null) {
//							equipHistory.setObjectId(eqInDB.getStockId());
//						}
//						if (eqInDB.getStockType() != null) {
//							equipHistory.setObjectType(eqInDB.getStockType());
//						}
//						equipHistory.setFormId(eqDeliveryRecord.getId());
//						equipHistory.setFormType(EquipTradeType.DELIVERY);
//						equipHistory.setObjectCode(eqInDB.getStockCode());
//						equipHistory.setStockName(eqInDB.getStockName());
//						equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
//						equipHistory.setPriceActually(eqInDB.getPriceActually());
//						equipHistory.setStatus(eqInDB.getStatus().getValue());
//						equipHistory.setTradeStatus(eqInDB.getTradeStatus());
//						equipHistory.setTradeType(eqInDB.getTradeType());
//						equipHistory.setUsageStatus(eqInDB.getUsageStatus());
//						equipMngDAO.createEquipHistory(equipHistory);
//					}
//					// luu chi tiet bien ban
//					ed.setEquipDeliveryRecord(eqDeliveryRecord);
//					ed.setHealthStatus(ed.getEquipment().getHealthStatus());
//					ed.setPrice(ed.getEquipment().getPrice());
////					ed.setPrice(Integer.parseInt(ed.getEquipment().getPrice()+""));
//					ed = equipmentRecordDeliveryDAO.createEquipmentRecordDeliveryDetail(ed);
//				} else {
//					lstEquipment.add(eq);
//				}
//			} //end for
//
//			//Xu ly them file
//			if (filter != null) {
//				if (filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
//					for (FileVO voFile : filter.getLstFileVo()) {
//						EquipAttachFile equipAttFile = new EquipAttachFile();
//						equipAttFile.setUrl(voFile.getUrl());
//						equipAttFile.setObjectId(eqDeliveryRecord.getId());
//						equipAttFile.setFileName(voFile.getFileName());
//						equipAttFile.setThumbUrl(voFile.getThumbUrl());
//						equipAttFile.setObjectType(EquipTradeType.DELIVERY.getValue());
//						equipAttFile.setCreateDate(sysDateIns);
//						equipAttFile.setCreateUser(eqDeliveryRecord.getCreateUser());
//						equipRecordDAO.createEquipAttachFile(equipAttFile);
//					}
//				}
//			}
//			equipDeliveryReturnVO.setEquipDeliveryRevord(eqDeliveryRecord);
//			return equipDeliveryReturnVO;
//		} catch (DataAccessException ex) {
//			throw new BusinessException(ex);
//		}
//	}
	
	@Override
	public EquipStockTotal getEquipStockTotalById(Long stockId, EquipStockTotalType stockType, Long equipGroupId) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return equipmentStockTotalDAO.getEquipStockTotalById(stockId, stockType, equipGroupId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEquipmentStockTotal(EquipStockTotal eqDeliveryRecord) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			equipmentStockTotalDAO.updateEquipmentStockTotal(eqDeliveryRecord);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipGroup getEquipGroupById(Long id) throws BusinessException {
		try {
			return id == null ? null : equipMngDAO.getEquipGroupById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * @Override public List<EquipmentVO>
	 * getListEquipmentBrand(EquipmentFilter<EquipmentVO> filter) throws
	 * BusinessException { try { return
	 * equipMngDAO.getListEquipmentBrand(filter); } catch (DataAccessException
	 * e) { throw new BusinessException(e); } }
	 */

	@Override
	public List<EquipmentVO> getListEquipmentCategory(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			return equipMngDAO.getListEquipmentCategory(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipFormHistory createEquipmentHistoryRecord(EquipFormHistory equipFormHistory) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}

	}

	@Override
	public List<EquipCategory> getListEquipmentCategoryByFilter(EquipmentFilter<EquipCategory> filter) throws BusinessException {
		try {
			return equipMngDAO.getListEquipmentCategoryByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<EquipProvider> getListEquipProviderByFilter(EquipmentFilter<EquipProvider> filter) throws BusinessException {
		try {
			return equipMngDAO.getListEquipProviderByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipmentRepairFormVO> searchListEquipmentRepairFormVOByFilter(EquipmentRepairFormFilter<EquipmentRepairFormVO> filter) throws BusinessException {
		try {
			List<EquipmentRepairFormVO> lst = equipMngDAO.searchListEquipmentRepairFormVOByFilter(filter);
			ObjectVO<EquipmentRepairFormVO> objVO = new ObjectVO<EquipmentRepairFormVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<EquipStockTotal> getListEquipStockTotal(Long stockId, EquipStockTotalType stockType, Long equipGroupId) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return equipmentStockTotalDAO.getListEquipStockTotal(stockId, stockType, equipGroupId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipmentRecordDeliveryVO> getListEquipmentRecordDeliveryForExport(EquipmentFilter<EquipmentRecordDeliveryVO> equipmentFilter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<EquipmentRecordDeliveryVO> lstRecordDeliveryVOs = new ArrayList<EquipmentRecordDeliveryVO>();
			for (int i = 0; i < equipmentFilter.getLstIdRecord().size(); i++) {
				EquipmentRecordDeliveryVO equipmentRecordDeliveryVO = equipmentRecordDeliveryDAO.getEquipmentRecordDeliveryVOById(equipmentFilter.getLstIdRecord().get(i));
				if (equipmentRecordDeliveryVO != null) {
					// lay danh sach thiet bi
					equipmentRecordDeliveryVO.setToAddress(equipmentRecordDeliveryVO.getToPermanentAddress());
					EquipmentFilter<EquipmentDeliveryVO> filter = new EquipmentFilter<EquipmentDeliveryVO>();
					filter.setkPaging(null);
					filter.setIdRecordDelivery(equipmentRecordDeliveryVO.getIdRecord());
					filter.setStatusEquip(StatusType.ACTIVE.getValue());
					List<EquipmentDeliveryVO> lstEquipmentDeliveryVOs = equipMngDAO.getListEquipmentDeliveryVOByFilter(filter);
					if (lstEquipmentDeliveryVOs != null) {
						equipmentRecordDeliveryVO.setLstEquipmentDeliveryVOs(lstEquipmentDeliveryVOs);
						equipmentRecordDeliveryVO.setNumberEquip(lstEquipmentDeliveryVOs.size());
					}

					lstRecordDeliveryVOs.add(equipmentRecordDeliveryVO);
				}
			}

			return lstRecordDeliveryVOs;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> searchEquipmentGroup(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.searchEquipmentGroup(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipmentVO> getListEquipmentProvider(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			return equipMngDAO.getListEquipmentProvider(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> getListEquipmentByFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<EquipmentVO> lst = equipMngDAO.getListEquipmentByFilter(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<EquipmentVO> getListEquipmentByFilterNew(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getListEquipmentByFilterNew(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipStockVO> getListEquipStockVOByFilter(EquipStockFilter filter) throws BusinessException {
		try {
			List<EquipStockVO> lst = equipMngDAO.getListEquipStockVOByFilter(filter);
			ObjectVO<EquipStockVO> objVO = new ObjectVO<EquipStockVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<EquipStockVO> getListEquipStockVOByFilterForStockTrans(EquipStockFilter filter) throws BusinessException {
		try {
			List<EquipStockVO> lst = equipMngDAO.getListEquipStockVOByFilterForStockTrans(filter);
			ObjectVO<EquipStockVO> objVO = new ObjectVO<EquipStockVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipGroup createEquipGroup(EquipGroup equipGroup, LogInfoVO logInfoVO) throws BusinessException {
		try {
			equipGroup.setNameText(Unicode2English.codau2khongdau(equipGroup.getName()).trim().toUpperCase());
			return equipMngDAO.createEquipmentGroup(equipGroup);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipSalePlan createEquipSalePlan(EquipSalePlan equipSalePlan, LogInfoVO logInfoVO) throws BusinessException {
		try {
			return equipMngDAO.createEquipSalePlan(equipSalePlan);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipSalePlan getEquipSalePlanById(Long id) throws BusinessException {
		try {
			return id == null ? null : equipMngDAO.getEquipSalePlanById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipGroup updateEquipGroup(EquipGroup equipGroup, LogInfoVO logInfoVO) throws BusinessException {
		try {
			return equipMngDAO.updateEquipGroup(equipGroup, logInfoVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipSalePlan updateEquipSalePlan(EquipSalePlan equipSalePlan, LogInfoVO logInfoVO) throws BusinessException {
		try {
			//equipMngDAO.deleteEquipSalePlan(equipSalePlan,logInfoVO );
			return equipMngDAO.updateEquipSalePlan(equipSalePlan, logInfoVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> getEquipFlan(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getEquipFlan(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> searchEquipmentGroupVObyFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.searchEquipmentGroupVObyFilter(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}

	}

	/*
	 * @Override public ObjectVO<EquipmentVO>
	 * getListEquipmentByFilter(EquipmentFilter<EquipmentVO> filter) throws
	 * BusinessException { // TODO Auto-generated method stub return null; }
	 */

	@Override
	public EquipDeliveryRecord getEquipDeliveryRecordById(Long idRecord) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return idRecord == null ? null : equipmentRecordDeliveryDAO.getEquipDeliveryRecordById(idRecord);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveEquipRecordDelivery(EquipDeliveryRecord equipDeliveryRecord, Integer statusRecord) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			Date sysDateIns = commonDAO.getSysDate();
			if (statusRecord != 0) {
				if (statusRecord == -2 || statusRecord.equals(StatusRecordsEquip.APPROVED.getValue())) {
					equipDeliveryRecord.setApproveDate(sysDateIns);
				}
				equipmentRecordDeliveryDAO.updateEquipmentRecordDelivery(equipDeliveryRecord);
				// luu lich su bien ban
				EquipFormHistory equipFormHistory = new EquipFormHistory();
				equipFormHistory.setActDate(sysDateIns);
				equipFormHistory.setCreateDate(sysDateIns);
				equipFormHistory.setCreateUser(equipDeliveryRecord.getUpdateUser());
				equipFormHistory.setDeliveryStatus(equipDeliveryRecord.getDeliveryStatus());
				equipFormHistory.setRecordId(equipDeliveryRecord.getId());
				equipFormHistory.setRecordStatus(equipDeliveryRecord.getRecordStatus());
				equipFormHistory.setRecordType(EquipTradeType.DELIVERY.getValue());
				Staff staff = staffDAO.getStaffByCode(equipDeliveryRecord.getUpdateUser());
				equipFormHistory.setStaff(staff);
				equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
			}
			// thay doi trang thai giao nhan
			//			if (statusRecord == -2 || statusRecord == -1) {
			//				
			//			}
			List<EquipDeliveryRecDtl> lstDetail = new ArrayList<EquipDeliveryRecDtl>();
			List<Equipment> lstEquipment = new ArrayList<Equipment>();
			List<EquipHistory> lstHistory = new ArrayList<EquipHistory>();
			if (statusRecord == -2 || statusRecord.equals(StatusRecordsEquip.CANCELLATION.getValue()) || statusRecord.equals(StatusRecordsEquip.APPROVED.getValue())) {
				// thay doi trang thai giao nhan va trang thai bien ban
				List<EquipDeliveryRecDtl> deliveryRecDtls = equipmentRecordDeliveryDAO.getListEquipDeliveryRecDtlByEquipDeliveryRecId(equipDeliveryRecord.getId());
				int size = deliveryRecDtls.size();
				for (int i = 0; i < size; i++) {
					Equipment eq = deliveryRecDtls.get(i).getEquipment();
					if (statusRecord.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
						if (deliveryRecDtls.get(i).getContent().equals(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue())) {
							lstDetail.add(deliveryRecDtls.get(i));
							lstEquipment.add(eq);
							List<EquipHistory> lstEquipHistory = equipMngDAO.getListEquipHistoryByEquipId(eq.getId());
							for (int c = 0, csize = lstEquipHistory.size(); c < csize; c++) {
								lstHistory.add(lstEquipHistory.get(c));
							}
						} else {
							eq.setTradeType(null);
							eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
							eq.setUpdateUser(equipDeliveryRecord.getUpdateUser());
							eq.setUpdateDate(sysDateIns);
							equipMngDAO.updateEquipment(eq);

							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(equipDeliveryRecord.getCreateUser());
							equipHistory.setEquip(eq);
							if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
								//equipHistory.setEquipWarehouseCode(equipment.getStockCode());
							}
							equipHistory.setHealthStatus(eq.getHealthStatus());
							if (eq.getStockId() != null) {
								equipHistory.setObjectId(eq.getStockId());
							}
							if (eq.getStockType() != null) {
								equipHistory.setObjectType(eq.getStockType());
							}
							equipHistory.setFormId(equipDeliveryRecord.getId());
							equipHistory.setFormType(EquipTradeType.DELIVERY);
							equipHistory.setObjectCode(eq.getStockCode());
							equipHistory.setStockName(eq.getStockName());
							equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
							equipHistory.setPriceActually(eq.getPriceActually());
							equipHistory.setStatus(eq.getStatus().getValue());
							equipHistory.setTradeStatus(eq.getTradeStatus());
							equipHistory.setTradeType(eq.getTradeType());
							equipHistory.setUsageStatus(eq.getUsageStatus());
							equipMngDAO.createEquipHistory(equipHistory);
						}
					} else {
						EquipStockTotal equipStockTotal = null;
						if (deliveryRecDtls.get(i).getContent().equals(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue())) {
							EquipStockEquipFilter<EquipStockTotal> filterSt = new EquipStockEquipFilter<EquipStockTotal>();
							filterSt.setEquipGroupId(eq.getEquipGroup().getId());
							filterSt.setStockType(EquipStockTotalType.KHO_TONG.getValue());  // kho hien tai chon
							EquipStockTotal total = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
							/**kiem tra kho tong co ton tai ko*/
							if(total!=null){ /**TH1: Ton tai kho tong*/
								/**Cap nhat kho tong*/
								if (total.getQuantity() != null) {
									total.setQuantity(total.getQuantity() + 1);
								} else {
									total.setQuantity(1);
								}
								total.setStockType(EquipStockTotalType.KHO_TONG); // Thay kho hien tai chon
								total.setUpdateUser(equipDeliveryRecord.getCreateUser());
								total.setUpdateDate(sysDateIns);
								equipMngDAO.updateEquipStockTotal(total);
								
							}else{ /**TH2: Chua ton tai kho tong*/
								/**Them moi kho tong*/
								total = new EquipStockTotal();
								total.setStockId(null);
								total.setEquipGroup(eq.getEquipGroup());
								total.setStockType(EquipStockTotalType.KHO_TONG);
								total.setQuantity(1);
								total.setCreateUser(eq.getCreateUser());
								total.setCreateDate(sysDateIns);
								total = equipMngDAO.createEquipStockTotal(total);
							}
						} else {
							// xoa kho VNM hoac kho NPP
							equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.parseValue(eq.getStockType().getValue()), eq.getEquipGroup().getId());
							equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
							equipStockTotal.setUpdateDate(sysDateIns);
							equipStockTotal.setUpdateUser(equipDeliveryRecord.getUpdateUser());
							equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
						}
						
						// Cap nhat kho khach hang
						equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(equipDeliveryRecord.getCustomer().getId(), EquipStockTotalType.KHO_KH, eq.getEquipGroup().getId());
						if (equipStockTotal != null) {
							
							if (equipStockTotal.getQuantity() == null) {
								equipStockTotal.setQuantity(1);
							} else {
								equipStockTotal.setQuantity(equipStockTotal.getQuantity() + 1);
							}
							equipStockTotal.setUpdateDate(sysDateIns);
							equipStockTotal.setUpdateUser(equipDeliveryRecord.getUpdateUser());
							equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
						} else {
							// tao moi
							equipStockTotal = new EquipStockTotal();
							equipStockTotal.setCreateDate(sysDateIns);
							equipStockTotal.setCreateUser(equipDeliveryRecord.getCreateUser());
							equipStockTotal.setEquipGroup(eq.getEquipGroup());
							equipStockTotal.setQuantity(1);
							equipStockTotal.setStockId(equipDeliveryRecord.getCustomer().getId());
							equipStockTotal.setStockType(EquipStockTotalType.KHO_KH);
							equipStockTotal = equipmentStockTotalDAO.createEquipStockTotal(equipStockTotal);
						}
						eq.setStockCode(equipDeliveryRecord.getCustomer().getShop().getShopCode() + equipDeliveryRecord.getCustomer().getShortCode());
						eq.setStockId(equipDeliveryRecord.getCustomer().getId());
						eq.setStockType(EquipStockTotalType.KHO_KH);
						eq.setStockName(equipDeliveryRecord.getCustomer().getCustomerName());
						eq.setUsageStatus(EquipUsageStatus.IS_USED);
						eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						eq.setTradeType(null);
						if (eq.getFirstDateInUse() == null) {
							eq.setFirstDateInUse(sysDateIns);
						}
						eq.setUpdateUser(equipDeliveryRecord.getUpdateUser());
						eq.setUpdateDate(sysDateIns);
						equipMngDAO.updateEquipment(eq);
						
						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(equipDeliveryRecord.getCreateUser());
						equipHistory.setEquip(eq);
						if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
							//equipHistory.setEquipWarehouseCode(equipment.getStockCode());
						}
						equipHistory.setHealthStatus(eq.getHealthStatus());
						if (eq.getStockId() != null) {
							equipHistory.setObjectId(eq.getStockId());
						}
						if (eq.getStockType() != null) {
							equipHistory.setObjectType(eq.getStockType());
						}
						equipHistory.setFormId(equipDeliveryRecord.getId());
						equipHistory.setFormType(EquipTradeType.DELIVERY);
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
						equipHistory.setPriceActually(eq.getPriceActually());
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipMngDAO.createEquipHistory(equipHistory);
						
					}
				}
				//xoa chi tiet detail cap moi
				for (int i = 0, isize = lstDetail.size(); i < isize; i++) {
					commonDAO.deleteEntity(lstDetail.get(i));
				}
				for (int i = 0, isize = lstEquipment.size(); i < isize; i++) {
					commonDAO.deleteEntity(lstEquipment.get(i));
				}
				for (int i = 0, isize = lstHistory.size(); i < isize; i++) {
					commonDAO.deleteEntity(lstHistory.get(i));
				}
				
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentStockDeliveryVO> getListEquipStockTotalVOByfilter(EquipmentFilter<EquipmentStockDeliveryVO> filter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<EquipmentStockDeliveryVO> lst = equipmentStockTotalDAO.getListEquipStockTotalVOByfilter(filter);
			ObjectVO<EquipmentStockDeliveryVO> objVO = new ObjectVO<EquipmentStockDeliveryVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createEquipHistoryByEquipment(Equipment equipment, String userName) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			EquipHistory equipHistory = new EquipHistory();
			equipHistory.setEquip(equipment);
			equipHistory.setStatus(StatusType.ACTIVE.getValue());
			equipHistory.setHealthStatus(equipment.getHealthStatus());
			equipHistory.setUsageStatus(equipment.getUsageStatus());
			equipHistory.setTradeStatus(equipment.getTradeStatus());
			equipHistory.setObjectId(equipment.getStockId());
			//equipHistory.setEquipWarehouseCode(equipment.getStockCode());
			equipHistory.setObjectType(equipment.getStockType());
			equipHistory.setTradeType(equipment.getTradeType());
			equipHistory.setCreateUser(userName);
			equipHistory.setCreateDate(commonDAO.getSysDate());
			equipMngDAO.createEquipHistory(equipHistory);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createEquipmentInListEquip(Equipment equipment, EquipmentFilter<BasicVO> filter) throws BusinessException {
		try {
			Date createDateSys = commonDAO.getSysDate();
			//Kho
			Long stockId = null;
			String stockCode = null;
			String stockName = null;
			if(equipment.getStockId()!=null){
				stockId = equipment.getStockId();
				stockCode = equipment.getStockCode();
				stockName = equipment.getStockName();
			}
			//TODO Thuc hien them moi vao EquipImportRecord (Bien ban)
			if (equipment.getEquipGroup() != null) {
				String maxImportRecordCode = equipMngDAO.getMaxEquipImportRecordCode();
				maxImportRecordCode = this.getEquipImportRecordCodeByMaxCode(maxImportRecordCode);
				EquipImportRecord equipImportRecord = new EquipImportRecord();
				equipImportRecord.setQuantity(filter.getQuantity());
				equipImportRecord.setEquipPriod(equipmentPeriodDAO.getEquipPeriodCurrent());
				equipImportRecord.setCode(maxImportRecordCode);
				equipImportRecord.setPrice(filter.getPrice());
				equipImportRecord.setEquipGroup(equipment.getEquipGroup());
				equipImportRecord.setHealthStatus(equipment.getHealthStatus());
				equipImportRecord.setEquipProvider(equipment.getEquipProvider());
				equipImportRecord.setWarrantyExpiredDate(equipment.getWarrantyExpiredDate());
				equipImportRecord.setApproveDate(createDateSys);
				equipImportRecord.setCreateUser(equipment.getCreateUser());
				equipImportRecord.setCreateDate(createDateSys);
				equipImportRecord.setManuFacturingYear(equipment.getManufacturingYear());
				equipImportRecord.setStockId(equipment.getStockId());
				equipImportRecord = equipMngDAO.createEquipImportRecord(equipImportRecord);
				//Xu ly them hinh anh
				if (filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
					for (FileVO voFile : filter.getLstFileVo()) {
						EquipAttachFile equipAttFile = new EquipAttachFile();
						equipAttFile.setUrl(voFile.getUrl());
						equipAttFile.setObjectId(equipImportRecord.getId());
						equipAttFile.setFileName(voFile.getFileName());
						equipAttFile.setThumbUrl(voFile.getThumbUrl());
						equipAttFile.setObjectType(EquipTradeType.EQUIP_ALLOCATION.getValue());
						equipAttFile.setCreateDate(createDateSys);
						equipAttFile.setCreateUser(equipment.getCreateUser());
						equipAttFile = equipRecordDAO.createEquipAttachFile(equipAttFile);
					}
				}
				String maxEquipmentCode = equipMngDAO.getMaxEquipmentCodeByEquipGroupId(equipment.getEquipGroup().getId());
				for (int i = 0; i < filter.getQuantity(); i++) {
					/**Thuc hien them moi vao Equipment (Thiet bi)*/
					maxEquipmentCode = this.getEquipmentCodeByMaxCodeAndPrefix(maxEquipmentCode, equipment.getEquipGroup().getCode());
					Equipment equipmentIst = equipment.clone();
					equipmentIst.setEquipImportRecord(equipImportRecord);
					equipmentIst.setStockId(stockId); // Kho chon
					equipmentIst.setStockCode(stockCode);
					equipmentIst.setStockName(stockName);
					equipmentIst.setStockType(EquipStockTotalType.KHO);
					equipmentIst.setStatus(StatusType.ACTIVE);
					equipmentIst.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);
					equipmentIst.setTradeType(null);
					equipmentIst.setTradeStatus(StatusType.INACTIVE.getValue());
					equipmentIst.setCode(maxEquipmentCode);
					equipmentIst.setCreateDate(createDateSys);
					equipmentIst = equipMngDAO.createEquipment(equipmentIst);
					/**Luu lich su thiet bi*/
					EquipHistory equipHistory = new EquipHistory();
					equipHistory.setEquip(equipmentIst);
					equipHistory.setStatus(StatusType.ACTIVE.getValue());
					equipHistory.setHealthStatus(equipmentIst.getHealthStatus());
					equipHistory.setUsageStatus(equipmentIst.getUsageStatus());
					equipHistory.setTradeType(null);
					equipHistory.setTradeStatus(StatusType.INACTIVE.getValue());
					equipHistory.setObjectId(equipment.getStockId());//kho chon
					equipHistory.setStockName(equipment.getStockName());
					equipHistory.setObjectCode(equipment.getStockCode());
					equipHistory.setObjectType(EquipStockTotalType.KHO);
					equipHistory.setCreateUser(equipmentIst.getCreateUser());
					equipHistory.setCreateDate(createDateSys);
					//Luu bien ban
					equipHistory.setFormId(equipImportRecord.getId());
					equipHistory.setFormType(EquipTradeType.EQUIP_ALLOCATION);
					equipHistory.setTableName("EQUIP_IMPORT_RECORD");
					equipHistory = equipMngDAO.createEquipHistory(equipHistory);
				}
				
				EquipStockEquipFilter<EquipStockTotal> filterSt = new EquipStockEquipFilter<EquipStockTotal>();
				filterSt.setEquipGroupId(equipment.getEquipGroup().getId());
				filterSt.setStockType(EquipStockTotalType.KHO_TONG.getValue());  // kho hien tai chon
				EquipStockTotal equipStockTotal = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
				filterSt = new EquipStockEquipFilter<EquipStockTotal>();
				filterSt.setEquipGroupId(equipment.getEquipGroup().getId());
				filterSt.setStockId(stockId);
				filterSt.setStockType(EquipStockTotalType.KHO.getValue());
				EquipStockTotal stock = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
				/**kiem tra kho tong co ton tai ko*/
				if(equipStockTotal!=null){ /**TH1: Ton tai kho tong*/
					/**Cap nhat kho tong*/
					if (equipStockTotal.getQuantity() != null) {
						equipStockTotal.setQuantity(equipStockTotal.getQuantity() + filter.getQuantity());
					} else {
						equipStockTotal.setQuantity(filter.getQuantity());
					}
					equipStockTotal.setStockType(EquipStockTotalType.KHO_TONG); // Thay kho hien tai chon
					equipStockTotal.setUpdateUser(equipment.getCreateUser());
					equipStockTotal.setUpdateDate(createDateSys);
					equipMngDAO.updateEquipStockTotal(equipStockTotal);
					if(stock!=null){
						/**TH1.1: Cap nhat kho thiet bi*/
						if (stock.getQuantity() != null) {
							stock.setQuantity(stock.getQuantity() + filter.getQuantity());
						} else {
							stock.setQuantity(filter.getQuantity());
						}
						stock.setStockType(EquipStockTotalType.KHO); // Thay kho hien tai chon
						stock.setUpdateUser(equipment.getCreateUser());
						stock.setUpdateDate(createDateSys);
						equipMngDAO.updateEquipStockTotal(stock);
					}else{
						/**TH1.2: Them moi kho thiet bi*/
						stock = new EquipStockTotal();
						stock.setStockId(stockId);
						stock.setEquipGroup(equipment.getEquipGroup());
						stock.setQuantity(filter.getQuantity());
						stock.setCreateUser(equipment.getCreateUser());
						stock.setCreateDate(createDateSys);
						stock.setStockType(EquipStockTotalType.KHO);
						stock = equipMngDAO.createEquipStockTotal(stock);
					}
				}else{ /**TH2: Chua ton tai kho tong*/
					/**Them moi kho tong*/
					equipStockTotal = new EquipStockTotal();
					equipStockTotal.setStockId(null);
					equipStockTotal.setEquipGroup(equipment.getEquipGroup());
					equipStockTotal.setStockType(EquipStockTotalType.KHO_TONG);
					equipStockTotal.setQuantity(filter.getQuantity());
					equipStockTotal.setCreateUser(equipment.getCreateUser());
					equipStockTotal.setCreateDate(createDateSys);
					equipStockTotal = equipMngDAO.createEquipStockTotal(equipStockTotal);
					/**Them moi kho thiet bi*/
					stock = new EquipStockTotal();
					stock.setStockId(stockId);
					stock.setEquipGroup(equipment.getEquipGroup());
					stock.setQuantity(filter.getQuantity());
					stock.setCreateUser(equipment.getCreateUser());
					stock.setCreateDate(createDateSys);
					stock.setStockType(EquipStockTotalType.KHO);
					stock = equipMngDAO.createEquipStockTotal(stock);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEquipmentInListEquip(Equipment equipmentUdt, EquipmentFilter<BasicVO> filter) throws BusinessException {
		try {
			Date sysDateDB = commonDAO.getSysDate();
			equipmentUdt.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);
			equipmentUdt.setUpdateDate(sysDateDB);
			equipMngDAO.updateEquipment(equipmentUdt);
			//TODO Luu lich su thiet bi
			if (!StringUtility.isNullOrEmpty(filter.getHealthStatusNew())) {
				EquipHistory equipHistory = new EquipHistory();
				equipHistory.setEquip(equipmentUdt);
				equipHistory.setStatus(StatusType.ACTIVE.getValue());
				equipHistory.setHealthStatus(equipmentUdt.getHealthStatus());
				equipHistory.setUsageStatus(equipmentUdt.getUsageStatus());
				equipHistory.setTradeStatus(equipmentUdt.getTradeStatus());
				equipHistory.setObjectId(equipmentUdt.getStockId());
				equipHistory.setObjectCode(equipmentUdt.getStockCode());
				equipHistory.setStockName(equipmentUdt.getStockName());
				equipHistory.setObjectType(equipmentUdt.getStockType());
				equipHistory.setTradeType(equipmentUdt.getTradeType());
				equipHistory.setCreateUser(equipmentUdt.getCreateUser());
				equipHistory.setCreateDate(sysDateDB);
				equipMngDAO.createEquipHistory(equipHistory);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public String getEquipImportRecordCodeByMaxCode(String maxCode) {
		String maxCodeNew = maxCode.replaceAll("CM", "").trim();
		try {
			Integer numberMaxCode = Integer.valueOf(maxCodeNew);
			numberMaxCode++;
			String kq = "00000000" + String.valueOf(numberMaxCode);
			kq = kq.substring(kq.length() - 8).trim();
			kq = "CM" + kq.trim();
			return kq.trim();
		} catch (Exception e) {
			throw new IllegalArgumentException("error data in by maxCode not format");
		}
	}

	@Override
	public String getEquipmentCodeByMaxCodeAndPrefix(String maxCode, String prefix) {
		try {
			String kq = "";
			if (StringUtility.isNullOrEmpty(maxCode) || maxCode.length() < 6) {
				kq = prefix.trim() + "000001";
				return kq.trim();
			}
			String maxCodeNew = maxCode.substring(maxCode.length() - 6).trim();
			if (!StringUtility.isNumber(maxCodeNew)) {
				kq = prefix.trim() + "000001";
			} else {
				Integer numberMaxCode = Integer.valueOf(maxCodeNew);
				numberMaxCode++;
				kq = "000000" + String.valueOf(numberMaxCode);
				kq = kq.substring(kq.length() - 6).trim();
				kq = prefix.trim() + kq.trim();
			}
			return kq.trim();
		} catch (Exception e) {
			throw new IllegalArgumentException("error data in by prefix not format");
		}
	}
	
	@Override
	public String getEquipmentCodeByMaxCodeAndPrefixMaxLength(String maxCode, String prefix, int length) {
		try {
			String kq = "";
			String numberZero = "";
			String numberOne = "";
			for (int i = 0; i < length - 1; i++) {
				numberZero += "0";
			}
			numberOne = numberZero + "1";
			numberZero += "0";
			
			if (StringUtility.isNullOrEmpty(maxCode) || maxCode.length() < length) {
				kq = prefix.trim() + numberOne;
				return kq.trim();
			}
			String maxCodeNew = maxCode.substring(maxCode.length() - length).trim();
			if (!StringUtility.isNumber(maxCodeNew)) {
				kq = prefix.trim() + numberOne;
			} else {
				Integer numberMaxCode = Integer.valueOf(maxCodeNew);
				numberMaxCode++;
				kq = numberZero + String.valueOf(numberMaxCode);
				kq = kq.substring(kq.length() - length).trim();
				kq = prefix.trim() + kq.trim();
			}
			return kq.trim();
		} catch (Exception e) {
			throw new IllegalArgumentException("error data in by prefix not format");
		}
	}

	@Override
	public List<EquipmentExVO> getListEquipmentDeliveryByCustomerId(Long customerId) throws BusinessException {
		try {
			List<EquipmentExVO> lst = equipMngDAO.getListEquipmentDeliveryByCustomerId(customerId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipmentRecordDeliveryVO getEquipmentRecordDeliveryVOById(Long id) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return id == null ? null : equipmentRecordDeliveryDAO.getEquipmentRecordDeliveryVOById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateRecordDelivery(EquipDeliveryRecord eqDeliveryRecord, List<EquipDeliveryRecDtl> lstDeliveryRecDtls, List<String> lstEquipDelete, Integer status, EquipRecordFilter<EquipmentDeliveryVO> filterFile) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			Date sysDateIns = commonDAO.getSysDate();
			if (eqDeliveryRecord != null && eqDeliveryRecord.getId() != null) {
				EquipDeliveryRecord eqDeliveryRecordDB = commonDAO.getEntityById(EquipDeliveryRecord.class, eqDeliveryRecord.getId());
				if (eqDeliveryRecordDB.getDeliveryStatus() != eqDeliveryRecord.getDeliveryStatus() 
						|| eqDeliveryRecordDB.getRecordStatus() != eqDeliveryRecord.getRecordStatus()) {
					EquipFormHistory equipFormHistory = new EquipFormHistory();
					equipFormHistory.setActDate(sysDateIns);
					equipFormHistory.setCreateDate(sysDateIns);
					equipFormHistory.setCreateUser(eqDeliveryRecord.getUpdateUser());
					equipFormHistory.setDeliveryStatus(eqDeliveryRecord.getDeliveryStatus());
					equipFormHistory.setRecordId(eqDeliveryRecord.getId());
					equipFormHistory.setRecordStatus(eqDeliveryRecord.getRecordStatus());
					equipFormHistory.setRecordType(EquipTradeType.DELIVERY.getValue());
					Staff staff = staffDAO.getStaffByCode(eqDeliveryRecord.getUpdateUser());
					equipFormHistory.setStaff(staff);
					equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
				} else if (eqDeliveryRecordDB.getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue()) 
					&& status.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
					EquipFormHistory equipFormHistory = new EquipFormHistory();
					equipFormHistory.setActDate(sysDateIns);
					equipFormHistory.setCreateDate(sysDateIns);
					equipFormHistory.setCreateUser(eqDeliveryRecord.getUpdateUser());
					equipFormHistory.setDeliveryStatus(eqDeliveryRecord.getDeliveryStatus());
					equipFormHistory.setRecordId(eqDeliveryRecord.getId());
					equipFormHistory.setRecordStatus(status);
					equipFormHistory.setRecordType(EquipTradeType.DELIVERY.getValue());
					Staff staff = staffDAO.getStaffByCode(eqDeliveryRecord.getUpdateUser());
					equipFormHistory.setStaff(staff);
					equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
				}
			
			}
			eqDeliveryRecord.setUpdateDate(sysDateIns);
			
			Map<Long, String> mapEquip = new HashMap<Long, String>();
			
			//cap nhat approvedDate
			if (status == -2 || status.equals(StatusRecordsEquip.APPROVED.getValue())) {
				eqDeliveryRecord.setApproveDate(sysDateIns);
			}
			if (status.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
				eqDeliveryRecord.setRecordStatus(StatusRecordsEquip.CANCELLATION.getValue());
			}
			
			equipmentRecordDeliveryDAO.updateEquipmentRecordDelivery(eqDeliveryRecord);
			

			if (!status.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
				// thay doi trang thai giao nhan
				//				if (status == -2 || status == -1) {
				//					// equipmentRecordDeliveryDAO.updateEquipmentRecordDelivery(equipDeliveryRecord);
				//					// luu lich su bien ban
				//					
				//				}
				// xoa chi tiet
				if (lstEquipDelete != null) {
					for (int i = 0; i < lstEquipDelete.size(); i++) {
						EquipmentFilter<EquipDeliveryRecDtl> filter = new EquipmentFilter<EquipDeliveryRecDtl>();
						filter.setIdRecordDelivery(eqDeliveryRecord.getId());
						filter.setCode(lstEquipDelete.get(i));
						EquipDeliveryRecDtl equipDeliveryRecDtl = equipmentRecordDeliveryDAO.getEquipDeliveryRecDtlByFilter(filter);

						Equipment equipment = equipDeliveryRecDtl.getEquipment();
						equipment.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						equipment.setTradeType(null);
						equipMngDAO.updateEquipment(equipment);

						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
						equipHistory.setEquip(equipment);
						if (!StringUtility.isNullOrEmpty(equipment.getStockCode())) {
							//equipHistory.setEquipWarehouseCode(equipment.getStockCode());
						}
						equipHistory.setHealthStatus(equipment.getHealthStatus());
						if (equipment.getStockId() != null) {
							equipHistory.setObjectId(equipment.getStockId());
						}
						if (equipment.getStockType() != null) {
							equipHistory.setObjectType(equipment.getStockType());
						}
						equipHistory.setFormId(eqDeliveryRecord.getId());
						equipHistory.setFormType(EquipTradeType.DELIVERY);
						equipHistory.setObjectCode(equipment.getStockCode());
						equipHistory.setStockName(equipment.getStockName());
						equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
						equipHistory.setPriceActually(equipment.getPriceActually());
						equipHistory.setStatus(equipment.getStatus().getValue());
						equipHistory.setTradeStatus(equipment.getTradeStatus());
						equipHistory.setTradeType(equipment.getTradeType());
						equipHistory.setUsageStatus(equipment.getUsageStatus());
						equipMngDAO.createEquipHistory(equipHistory);

						equipmentRecordDeliveryDAO.deleteEquipmentRecordDeliveryDetail(equipDeliveryRecDtl);
					}
				}
				// cap nhat chi tiet khi doi trang thai
				if (status == -2 || status.equals(StatusRecordsEquip.APPROVED.getValue())) {
					// thay doi trang thai giao nhan va trang thai bien ban
					List<EquipDeliveryRecDtl> deliveryRecDtls = equipmentRecordDeliveryDAO.getListEquipDeliveryRecDtlByEquipDeliveryRecId(eqDeliveryRecord.getId());
					int size = deliveryRecDtls.size();
					for (int i = 0; i < size; i++) {
						Equipment eq = deliveryRecDtls.get(i).getEquipment();
						if (mapEquip.containsKey(eq.getId()) == false) {
							// xoa kho VNM hoac kho NPP
							EquipStockTotal equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.parseValue(eq.getStockType().getValue()), eq.getEquipGroup().getId());
							equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
							equipStockTotal.setUpdateDate(sysDateIns);
							equipStockTotal.setUpdateUser(eqDeliveryRecord.getUpdateUser());
							equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
							// Cap nhat kho khach hang
							equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eqDeliveryRecord.getCustomer().getId(), EquipStockTotalType.KHO_KH, eq.getEquipGroup().getId());
							if (equipStockTotal != null) {
								if (equipStockTotal.getQuantity() == null) {
									equipStockTotal.setQuantity(1);
								} else {
									equipStockTotal.setQuantity(equipStockTotal.getQuantity() + 1);
								}
								equipStockTotal.setUpdateDate(sysDateIns);
								equipStockTotal.setUpdateUser(eqDeliveryRecord.getUpdateUser());
								equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
							} else {
								// tao moi
								equipStockTotal = new EquipStockTotal();
								equipStockTotal.setCreateDate(sysDateIns);
								equipStockTotal.setCreateUser(eqDeliveryRecord.getCreateUser());
								equipStockTotal.setEquipGroup(eq.getEquipGroup());
								equipStockTotal.setQuantity(1);
								equipStockTotal.setStockId(eqDeliveryRecord.getCustomer().getId());
								equipStockTotal.setStockType(EquipStockTotalType.KHO_KH);
								equipStockTotal = equipmentStockTotalDAO.createEquipStockTotal(equipStockTotal);

							}
							eq.setStockCode(eqDeliveryRecord.getCustomer().getShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
							eq.setStockId(eqDeliveryRecord.getCustomer().getId());
							eq.setStockName(eqDeliveryRecord.getCustomer().getCustomerName());
							eq.setStockType(EquipStockTotalType.KHO_KH);
							eq.setUsageStatus(EquipUsageStatus.IS_USED);
							eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
							eq.setTradeType(null);
							if (eq.getFirstDateInUse() == null) {
								eq.setFirstDateInUse(sysDateIns);
							}
							eq.setUpdateUser(eqDeliveryRecord.getUpdateUser());
							eq.setUpdateDate(sysDateIns);
							equipMngDAO.updateEquipment(eq);

							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
							equipHistory.setEquip(eq);
							if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
								//equipHistory.setEquipWarehouseCode(eq.getStockCode());
							}
							equipHistory.setHealthStatus(eq.getHealthStatus());
							if (eq.getStockId() != null) {
								equipHistory.setObjectId(eq.getStockId());
							}
							if (eq.getStockType() != null) {
								equipHistory.setObjectType(eq.getStockType());
							}
							equipHistory.setFormId(eqDeliveryRecord.getId());
							equipHistory.setFormType(EquipTradeType.DELIVERY);
							equipHistory.setObjectCode(eq.getStockCode());
							equipHistory.setStockName(eq.getStockName());
							equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
							equipHistory.setPriceActually(eq.getPriceActually());
							equipHistory.setStatus(eq.getStatus().getValue());
							equipHistory.setTradeStatus(eq.getTradeStatus());
							equipHistory.setTradeType(eq.getTradeType());
							equipHistory.setUsageStatus(eq.getUsageStatus());
							equipMngDAO.createEquipHistory(equipHistory);
							
							//cap nhat approvedDate
							eqDeliveryRecord.setApproveDate(sysDateIns);
							commonDAO.updateEntity(eqDeliveryRecord);
							mapEquip.put(eq.getId(), eq.getCode());
						}
						
					}
				}
				// them moi chi tiet
				if (lstDeliveryRecDtls != null) {
					for (int i = 0; i < lstDeliveryRecDtls.size(); i++) {
						EquipDeliveryRecDtl ed = lstDeliveryRecDtls.get(i);
						Equipment eq = ed.getEquipment();
						EquipmentFilter<EquipDeliveryRecDtl> filter = new EquipmentFilter<EquipDeliveryRecDtl>();
						filter.setIdRecordDelivery(eqDeliveryRecord.getId());
						filter.setId(eq.getId());
						EquipDeliveryRecDtl equipDeliveryDtlDB = equipmentRecordDeliveryDAO.getEquipDeliveryRecDtlByFilter(filter);
						
						if(equipDeliveryDtlDB == null){
							// cap nhĂ¢t khi doi trang thai giao nhan
							if (status == -2 || status.equals(StatusRecordsEquip.APPROVED.getValue()) || status == 0 || status == -1) {
								// luu thay doi thiet bi trong chi tiet bien ban
								if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
									eq.setTradeType(EquipTradeType.DELIVERY);
									eq.setTradeStatus(EquipTradeStatus.TRADING.getValue());
									eq.setUpdateDate(sysDateIns);
									eq.setUpdateUser(eqDeliveryRecord.getCreateUser());
									equipMngDAO.updateEquipment(eq);
	
									// luu lich su thiet bi
									EquipHistory equipHistory = new EquipHistory();
									equipHistory.setCreateDate(sysDateIns);
									equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
									equipHistory.setEquip(eq);
									if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
										//equipHistory.setEquipWarehouseCode(eq.getStockCode());
									}
									equipHistory.setHealthStatus(eq.getHealthStatus());
									if (eq.getStockId() != null) {
										equipHistory.setObjectId(eq.getStockId());
									}
									if (eq.getStockType() != null) {
										equipHistory.setObjectType(eq.getStockType());
									}
									equipHistory.setFormId(eqDeliveryRecord.getId());
									equipHistory.setFormType(EquipTradeType.DELIVERY);
									equipHistory.setObjectCode(eq.getStockCode());
									equipHistory.setStockName(eq.getStockName());
									equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
									equipHistory.setPriceActually(eq.getPriceActually());
									equipHistory.setStatus(eq.getStatus().getValue());
									equipHistory.setTradeStatus(eq.getTradeStatus());
									equipHistory.setTradeType(eq.getTradeType());
									equipHistory.setUsageStatus(eq.getUsageStatus());
									equipMngDAO.createEquipHistory(equipHistory);
								} else if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {//duyet
									// xoa kho vnm, npp
									EquipStockTotal equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.parseValue(eq.getStockType().getValue()), eq.getEquipGroup().getId());
									equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
									equipStockTotal.setUpdateDate(sysDateIns);
									equipStockTotal.setUpdateUser(eqDeliveryRecord.getCreateUser());
									equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
	
									// cap nhat kho kh
									equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eqDeliveryRecord.getCustomer().getId(), EquipStockTotalType.KHO_KH, eq.getEquipGroup().getId());
									if (equipStockTotal != null) {
										// cap nhat
										if (equipStockTotal.getQuantity() == null) {
											equipStockTotal.setQuantity(1);
										} else {
											equipStockTotal.setQuantity(equipStockTotal.getQuantity() + 1);
										}
										equipStockTotal.setUpdateDate(sysDateIns);
										equipStockTotal.setUpdateUser(eqDeliveryRecord.getCreateUser());
										equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
									} else {
										// tao moi
										equipStockTotal = new EquipStockTotal();
										equipStockTotal.setCreateDate(sysDateIns);
										equipStockTotal.setCreateUser(eqDeliveryRecord.getCreateUser());
										equipStockTotal.setEquipGroup(eq.getEquipGroup());
										equipStockTotal.setQuantity(1);
										equipStockTotal.setStockId(eqDeliveryRecord.getCustomer().getId());
										equipStockTotal.setStockType(EquipStockTotalType.KHO_KH);
										equipStockTotal = equipmentStockTotalDAO.createEquipStockTotal(equipStockTotal);
	
									}
									eq.setStockCode(eqDeliveryRecord.getCustomer().getShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
									eq.setStockId(eqDeliveryRecord.getCustomer().getId());
									eq.setStockName(eqDeliveryRecord.getCustomer().getCustomerName());
									eq.setStockType(EquipStockTotalType.KHO_KH);
									eq.setUsageStatus(EquipUsageStatus.IS_USED);
									eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
									eq.setTradeType(null);
									if (eq.getFirstDateInUse() == null) {
										eq.setFirstDateInUse(sysDateIns);
									}
									eq.setUpdateDate(sysDateIns);
									eq.setUpdateUser(eqDeliveryRecord.getCreateUser());
									equipMngDAO.updateEquipment(eq);
									// luu lich su thiet bi
									EquipHistory equipHistory = new EquipHistory();
									equipHistory.setCreateDate(sysDateIns);
									equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
									equipHistory.setEquip(eq);
									if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
										//equipHistory.setEquipWarehouseCode(eq.getStockCode());
									}
									equipHistory.setHealthStatus(eq.getHealthStatus());
									if (eq.getStockId() != null) {
										equipHistory.setObjectId(eq.getStockId());
									}
									if (eq.getStockType() != null) {
										equipHistory.setObjectType(eq.getStockType());
									}
									equipHistory.setFormId(eqDeliveryRecord.getId());
									equipHistory.setFormType(EquipTradeType.DELIVERY);
									equipHistory.setObjectCode(eq.getStockCode());
									equipHistory.setStockName(eq.getStockName());
									equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
									equipHistory.setPriceActually(eq.getPriceActually());
									equipHistory.setStatus(eq.getStatus().getValue());
									equipHistory.setTradeStatus(eq.getTradeStatus());
									equipHistory.setTradeType(eq.getTradeType());
									equipHistory.setUsageStatus(eq.getUsageStatus());
									equipMngDAO.createEquipHistory(equipHistory);
								}
							}
							
							// luu chi tiet bien ban
							ed.setEquipDeliveryRecord(eqDeliveryRecord);
							ed.setHealthStatus(ed.getEquipment().getHealthStatus());
							ed.setPrice(ed.getEquipment().getPrice());
//							ed.setPrice(Integer.parseInt(ed.getEquipment().getPrice()+""));
							ed = equipmentRecordDeliveryDAO.createEquipmentRecordDeliveryDetail(ed);
						} else if((equipDeliveryDtlDB.getEquipment().getSerial() == null && eq.getSerial() != null ) 
								|| ( eq.getSerial() != null  && equipDeliveryDtlDB.getEquipment().getSerial() != null && !equipDeliveryDtlDB.getEquipment().getSerial().equals(eq.getSerial()))) {
							
							Boolean isChangedSerial = false;
							if (equipDeliveryDtlDB.getEquipment().getSerial() == null && eq.getSerial() != null) {
								isChangedSerial = true;
							}
							// luu thay doi thiet bi trong chi tiet bien ban
							if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
								eq.setTradeType(EquipTradeType.DELIVERY);
								eq.setTradeStatus(EquipTradeStatus.TRADING.getValue());
								eq.setUpdateDate(sysDateIns);
								eq.setUpdateUser(eqDeliveryRecord.getCreateUser());
								equipMngDAO.updateEquipment(eq);
								if (!isChangedSerial) {
									// luu lich su thiet bi
									EquipHistory equipHistory = new EquipHistory();
									equipHistory.setCreateDate(sysDateIns);
									equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
									equipHistory.setEquip(eq);
									if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
										//equipHistory.setEquipWarehouseCode(eq.getStockCode());
									}
									equipHistory.setHealthStatus(eq.getHealthStatus());
									if (eq.getStockId() != null) {
										equipHistory.setObjectId(eq.getStockId());
									}
									if (eq.getStockType() != null) {
										equipHistory.setObjectType(eq.getStockType());
									}
									equipHistory.setFormId(eqDeliveryRecord.getId());
									equipHistory.setFormType(EquipTradeType.DELIVERY);
									equipHistory.setObjectCode(eq.getStockCode());
									equipHistory.setStockName(eq.getStockName());
									equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
									equipHistory.setPriceActually(eq.getPriceActually());
									equipHistory.setStatus(eq.getStatus().getValue());
									equipHistory.setTradeStatus(eq.getTradeStatus());
									equipHistory.setTradeType(eq.getTradeType());
									equipHistory.setUsageStatus(eq.getUsageStatus());
									equipMngDAO.createEquipHistory(equipHistory);
								}
								
							} else if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
								if (mapEquip.containsKey(eq.getId()) == false) {
									// xoa kho vnm, npp
									EquipStockTotal equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.parseValue(eq.getStockType().getValue()), eq.getEquipGroup().getId());
									equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
									equipStockTotal.setUpdateDate(sysDateIns);
									equipStockTotal.setUpdateUser(eqDeliveryRecord.getCreateUser());
									equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);

									// cap nhat kho kh
									equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eqDeliveryRecord.getCustomer().getId(), EquipStockTotalType.KHO_KH, eq.getEquipGroup().getId());
									if (equipStockTotal != null) {
										// cap nhat
										if (equipStockTotal.getQuantity() == null) {
											equipStockTotal.setQuantity(1);
										} else {
											equipStockTotal.setQuantity(equipStockTotal.getQuantity() + 1);
										}
										equipStockTotal.setUpdateDate(sysDateIns);
										equipStockTotal.setUpdateUser(eqDeliveryRecord.getCreateUser());
										equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
									} else {
										// tao moi
										equipStockTotal = new EquipStockTotal();
										equipStockTotal.setCreateDate(sysDateIns);
										equipStockTotal.setCreateUser(eqDeliveryRecord.getCreateUser());
										equipStockTotal.setEquipGroup(eq.getEquipGroup());
										equipStockTotal.setQuantity(1);
										equipStockTotal.setStockId(eqDeliveryRecord.getCustomer().getId());
										equipStockTotal.setStockType(EquipStockTotalType.KHO_KH);
										equipStockTotal = equipmentStockTotalDAO.createEquipStockTotal(equipStockTotal);

									}
									eq.setStockCode(eqDeliveryRecord.getCustomer().getShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
									eq.setStockId(eqDeliveryRecord.getCustomer().getId());
									eq.setStockName(eqDeliveryRecord.getCustomer().getCustomerName());
									eq.setStockType(EquipStockTotalType.KHO_KH);
									eq.setUsageStatus(EquipUsageStatus.IS_USED);
									eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
									eq.setTradeType(null);
									if (eq.getFirstDateInUse() == null) {
										eq.setFirstDateInUse(sysDateIns);
									}
									eq.setUpdateDate(sysDateIns);
									eq.setUpdateUser(eqDeliveryRecord.getCreateUser());
									equipMngDAO.updateEquipment(eq);
									// luu lich su thiet bi
									EquipHistory equipHistory = new EquipHistory();
									equipHistory.setCreateDate(sysDateIns);
									equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
									equipHistory.setEquip(eq);
									if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
										//equipHistory.setEquipWarehouseCode(eq.getStockCode());
									}
									equipHistory.setHealthStatus(eq.getHealthStatus());
									if (eq.getStockId() != null) {
										equipHistory.setObjectId(eq.getStockId());
									}
									if (eq.getStockType() != null) {
										equipHistory.setObjectType(eq.getStockType());
									}
									equipHistory.setFormId(eqDeliveryRecord.getId());
									equipHistory.setFormType(EquipTradeType.DELIVERY);
									equipHistory.setObjectCode(eq.getStockCode());
									equipHistory.setStockName(eq.getStockName());
									equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
									equipHistory.setPriceActually(eq.getPriceActually());
									equipHistory.setStatus(eq.getStatus().getValue());
									equipHistory.setTradeStatus(eq.getTradeStatus());
									equipHistory.setTradeType(eq.getTradeType());
									equipHistory.setUsageStatus(eq.getUsageStatus());
									equipMngDAO.createEquipHistory(equipHistory);
									mapEquip.put(eq.getId(), eq.getCode());
								} 
							}
						} 
						if (equipDeliveryDtlDB != null) {
							if (equipDeliveryDtlDB.getDepreciation() == null && ed.getDepreciation() != null) {
								equipDeliveryDtlDB.setDepreciation(ed.getDepreciation());
								commonDAO.updateEntity(equipDeliveryDtlDB);
							} else if (equipDeliveryDtlDB.getDepreciation() != null && ed.getDepreciation() != null &&
									equipDeliveryDtlDB.getDepreciation() != ed.getDepreciation()) {
								equipDeliveryDtlDB.setDepreciation(ed.getDepreciation());
								commonDAO.updateEntity(equipDeliveryDtlDB);
							}
						}
					}
				}
			} else {//huy
				List<EquipDeliveryRecDtl> deliveryRecDtls = equipmentRecordDeliveryDAO.getListEquipDeliveryRecDtlByEquipDeliveryRecId(eqDeliveryRecord.getId());
				int size = deliveryRecDtls.size();
				for (int i = 0; i < size; i++) {
					Equipment eq = deliveryRecDtls.get(i).getEquipment();
					eq.setTradeType(null);
					eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
					eq.setUpdateUser(eqDeliveryRecord.getUpdateUser());
					eq.setUpdateDate(sysDateIns);
					equipMngDAO.updateEquipment(eq);

					// luu lich su thiet bi
					EquipHistory equipHistory = new EquipHistory();
					equipHistory.setCreateDate(sysDateIns);
					equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
					equipHistory.setEquip(eq);
					if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
						//equipHistory.setEquipWarehouseCode(eq.getStockCode());
					}
					equipHistory.setHealthStatus(eq.getHealthStatus());
					if (eq.getStockId() != null) {
						equipHistory.setObjectId(eq.getStockId());
					}
					if (eq.getStockType() != null) {
						equipHistory.setObjectType(eq.getStockType());
					}
					equipHistory.setFormId(eqDeliveryRecord.getId());
					equipHistory.setFormType(EquipTradeType.DELIVERY);
					equipHistory.setObjectCode(eq.getStockCode());
					equipHistory.setStockName(eq.getStockName());
					equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
					equipHistory.setPriceActually(eq.getPriceActually());
					equipHistory.setStatus(eq.getStatus().getValue());
					equipHistory.setTradeStatus(eq.getTradeStatus());
					equipHistory.setTradeType(eq.getTradeType());
					equipHistory.setUsageStatus(eq.getUsageStatus());
					equipMngDAO.createEquipHistory(equipHistory);
//					equipmentRecordDeliveryDAO.deleteEquipmentRecordDeliveryDetail(deliveryRecDtls.get(i));
				}
			}
			//Xu ly xoa file
			if (filterFile.getLstEquipAttachFileId() != null && !filterFile.getLstEquipAttachFileId().isEmpty()) {
				BasicFilter<EquipAttachFile> filterB = new BasicFilter<EquipAttachFile>();
				filterB.setLstId(filterFile.getLstEquipAttachFileId());
				List<EquipAttachFile> lstFile = equipRecordDAO.searchEquipAttachFileByFilter(filterB);
				if (lstFile != null && !lstFile.isEmpty()) {
					for (EquipAttachFile fileVItem : lstFile) {
						equipRecordDAO.deletEquipAttachFile(fileVItem);
					}
				}
			}
			//Xu ly them file
			if (filterFile.getLstFileVo() != null && !filterFile.getLstFileVo().isEmpty()) {
				for (FileVO voFile : filterFile.getLstFileVo()) {
					EquipAttachFile equipAttFile = new EquipAttachFile();
					equipAttFile.setUrl(voFile.getUrl());
					equipAttFile.setObjectId(eqDeliveryRecord.getId());
					equipAttFile.setFileName(voFile.getFileName());
					equipAttFile.setThumbUrl(voFile.getThumbUrl());
					equipAttFile.setObjectType(EquipTradeType.DELIVERY.getValue());
					equipAttFile.setCreateDate(sysDateIns);
					equipAttFile.setCreateUser(eqDeliveryRecord.getUpdateUser());
					equipRecordDAO.createEquipAttachFile(equipAttFile);
				}
			}
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateRecordDeliveryX(EquipDeliveryRecord eqDeliveryRecord, List<EquipDeliveryRecDtl> lstDeliveryRecDtls, List<EquipDeliveryRecDtl> lstEquipDelete, Integer status, EquipRecordFilter<EquipmentDeliveryVO> filterFile) throws BusinessException {
		int maxLengthCode = 8;
		try {
			Date sysDateIns = commonDAO.getSysDate();
			if (eqDeliveryRecord != null && eqDeliveryRecord.getId() != null) {
				EquipDeliveryRecord eqDeliveryRecordDB = commonDAO.getEntityById(EquipDeliveryRecord.class, eqDeliveryRecord.getId());
				if (eqDeliveryRecordDB.getDeliveryStatus() != eqDeliveryRecord.getDeliveryStatus() 
						|| eqDeliveryRecordDB.getRecordStatus() != eqDeliveryRecord.getRecordStatus()) {
					EquipFormHistory equipFormHistory = new EquipFormHistory();
					equipFormHistory.setActDate(sysDateIns);
					equipFormHistory.setCreateDate(sysDateIns);
					equipFormHistory.setCreateUser(eqDeliveryRecord.getUpdateUser());
					equipFormHistory.setDeliveryStatus(eqDeliveryRecord.getDeliveryStatus());
					equipFormHistory.setRecordId(eqDeliveryRecord.getId());
					equipFormHistory.setRecordStatus(eqDeliveryRecord.getRecordStatus());
					equipFormHistory.setRecordType(EquipTradeType.DELIVERY.getValue());
					Staff staff = staffDAO.getStaffByCode(eqDeliveryRecord.getUpdateUser());
					equipFormHistory.setStaff(staff);
					equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
				} else if (eqDeliveryRecordDB.getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue()) 
					&& status.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
					EquipFormHistory equipFormHistory = new EquipFormHistory();
					equipFormHistory.setActDate(sysDateIns);
					equipFormHistory.setCreateDate(sysDateIns);
					equipFormHistory.setCreateUser(eqDeliveryRecord.getUpdateUser());
					equipFormHistory.setDeliveryStatus(eqDeliveryRecord.getDeliveryStatus());
					equipFormHistory.setRecordId(eqDeliveryRecord.getId());
					equipFormHistory.setRecordStatus(status);
					equipFormHistory.setRecordType(EquipTradeType.DELIVERY.getValue());
					Staff staff = staffDAO.getStaffByCode(eqDeliveryRecord.getUpdateUser());
					equipFormHistory.setStaff(staff);
					equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
				}
			
			}
			eqDeliveryRecord.setUpdateDate(sysDateIns);
			
			Map<Long, String> mapEquip = new HashMap<Long, String>();
			
			//cap nhat approvedDate
			if (status == -2 || status.equals(StatusRecordsEquip.APPROVED.getValue())) {
				eqDeliveryRecord.setApproveDate(sysDateIns);
			}
			if (status.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
				eqDeliveryRecord.setRecordStatus(StatusRecordsEquip.CANCELLATION.getValue());
			}
			
			equipmentRecordDeliveryDAO.updateEquipmentRecordDelivery(eqDeliveryRecord);
			
			List<EquipDeliveryRecDtl> deliveryRecDtlsBD = equipmentRecordDeliveryDAO.getListEquipDeliveryRecDtlByEquipDeliveryRecId(eqDeliveryRecord.getId());
			List<EquipDeliveryRecDtl> deliveryRecDtlDelete = new ArrayList<EquipDeliveryRecDtl>();
			List<Equipment> equipmentDelete = new ArrayList<Equipment>();
			List<EquipHistory> equipmentHistoryDelete = new ArrayList<EquipHistory>();
			if (!status.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
				// xoa chi tiet
				if (lstEquipDelete != null) {
					for (int i = 0; i < lstEquipDelete.size(); i++) {
						EquipmentFilter<EquipDeliveryRecDtl> filter = new EquipmentFilter<EquipDeliveryRecDtl>();
						filter.setIdRecordDelivery(eqDeliveryRecord.getId());
						filter.setCode(lstEquipDelete.get(i).getEquipment().getCode());
						EquipDeliveryRecDtl equipDeliveryRecDtl = equipmentRecordDeliveryDAO.getEquipDeliveryRecDtlByFilter(filter);

						Equipment equipment = equipDeliveryRecDtl.getEquipment();
						//Truong hop cap moi thiet bi
						if (equipment.getStockId() == null && equipDeliveryRecDtl.getContent().equals(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue())) {
							List<EquipHistory> lstEquipHistory = equipMngDAO.getListEquipHistoryByEquipId(equipment.getId());
							for (int c = 0, csize = lstEquipHistory.size(); c < csize; c++) {
//								commonDAO.deleteEntity(lstEquipHistory.get(c));
								equipmentHistoryDelete.add(lstEquipHistory.get(c));
							}
//							commonDAO.deleteEntity(equipment);
							equipmentDelete.add(equipment);
						} else {
							equipment.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
							equipment.setTradeType(null);
							equipMngDAO.updateEquipment(equipment);
							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
							equipHistory.setEquip(equipment);
							if (!StringUtility.isNullOrEmpty(equipment.getStockCode())) {
								//equipHistory.setEquipWarehouseCode(equipment.getStockCode());
							}
							equipHistory.setHealthStatus(equipment.getHealthStatus());
							if (equipment.getStockId() != null) {
								equipHistory.setObjectId(equipment.getStockId());
							}
							if (equipment.getStockType() != null) {
								equipHistory.setObjectType(equipment.getStockType());
							}
							equipHistory.setFormId(eqDeliveryRecord.getId());
							equipHistory.setFormType(EquipTradeType.DELIVERY);
							equipHistory.setObjectCode(equipment.getStockCode());
							equipHistory.setStockName(equipment.getStockName());
							equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
//							equipHistory.setPriceActually(equipment.getPriceActually());
							equipHistory.setStatus(equipment.getStatus().getValue());
							equipHistory.setTradeStatus(equipment.getTradeStatus());
							equipHistory.setTradeType(equipment.getTradeType());
							equipHistory.setUsageStatus(equipment.getUsageStatus());
							equipMngDAO.createEquipHistory(equipHistory);
						}
						deliveryRecDtlDelete.add(equipDeliveryRecDtl);
//						equipmentRecordDeliveryDAO.deleteEquipmentRecordDeliveryDetail(equipDeliveryRecDtl);
						deliveryRecDtlsBD.remove(equipDeliveryRecDtl);
					}
				}
				// cap nhat chi tiet khi doi trang thai
				if (status == -2 || status.equals(StatusRecordsEquip.APPROVED.getValue())) {
					// thay doi trang thai giao nhan va trang thai bien ban
					int size = deliveryRecDtlsBD.size();
					for (int i = 0; i < size; i++) {
						EquipDeliveryRecDtl deliveryDtl = deliveryRecDtlsBD.get(i);
						Equipment eq = deliveryRecDtlsBD.get(i).getEquipment();
						Equipment eWeb = null;
						for (int j = 0; j < lstDeliveryRecDtls.size(); j++) {
							if (eq.getCode().equals(lstDeliveryRecDtls.get(j).getEquipment().getCode())) {
								eWeb = lstDeliveryRecDtls.get(j).getEquipment();
								lstDeliveryRecDtls.remove(lstDeliveryRecDtls.get(j));
								break;
							}
						}
						
						if (eWeb.getSerial() != null && eq.getSerial() == null) {
							eq.setSerial(eWeb.getSerial());
							deliveryDtl.setEquipment(eq);
							commonDAO.updateEntity(deliveryDtl);
						}
						
						if (mapEquip.containsKey(eq.getId()) == false) {
							EquipStockTotal equipStockTotal = null;
							
							if (deliveryDtl.getContent().equals(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue())) {
								EquipStockEquipFilter<EquipStockTotal> filterSt = new EquipStockEquipFilter<EquipStockTotal>();
								filterSt.setEquipGroupId(eq.getEquipGroup().getId());
								filterSt.setStockType(EquipStockTotalType.KHO_TONG.getValue());  // kho hien tai chon
								EquipStockTotal total = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
								/**kiem tra kho tong co ton tai ko*/
								if(total!=null){ /**TH1: Ton tai kho tong*/
									/**Cap nhat kho tong*/
									if (total.getQuantity() != null) {
										total.setQuantity(total.getQuantity() + 1);
									} else {
										total.setQuantity(1);
									}
									total.setStockType(EquipStockTotalType.KHO_TONG); // Thay kho hien tai chon
									total.setUpdateUser(eqDeliveryRecord.getCreateUser());
									total.setUpdateDate(sysDateIns);
									equipMngDAO.updateEquipStockTotal(total);
									
								}else{ /**TH2: Chua ton tai kho tong*/
									/**Them moi kho tong*/
									total = new EquipStockTotal();
									total.setStockId(null);
									total.setEquipGroup(eq.getEquipGroup());
									total.setStockType(EquipStockTotalType.KHO_TONG);
									total.setQuantity(1);
									total.setCreateUser(eq.getCreateUser());
									total.setCreateDate(sysDateIns);
									total = equipMngDAO.createEquipStockTotal(total);
								}
							} else {
								// xoa kho VNM hoac kho NPP
								equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.parseValue(eq.getStockType().getValue()), eq.getEquipGroup().getId());
								equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
								equipStockTotal.setUpdateDate(sysDateIns);
								equipStockTotal.setUpdateUser(eqDeliveryRecord.getUpdateUser());
								equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
							}
							
							
							// Cap nhat kho khach hang
							equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eqDeliveryRecord.getCustomer().getId(), EquipStockTotalType.KHO_KH, eq.getEquipGroup().getId());
							if (equipStockTotal != null) {
								if (equipStockTotal.getQuantity() == null) {
									equipStockTotal.setQuantity(1);
								} else {
									equipStockTotal.setQuantity(equipStockTotal.getQuantity() + 1);
								}
								equipStockTotal.setUpdateDate(sysDateIns);
								equipStockTotal.setUpdateUser(eqDeliveryRecord.getUpdateUser());
								equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
							} else {
								// tao moi
								equipStockTotal = new EquipStockTotal();
								equipStockTotal.setCreateDate(sysDateIns);
								equipStockTotal.setCreateUser(eqDeliveryRecord.getCreateUser());
								equipStockTotal.setEquipGroup(eq.getEquipGroup());
								equipStockTotal.setQuantity(1);
								equipStockTotal.setStockId(eqDeliveryRecord.getCustomer().getId());
								equipStockTotal.setStockType(EquipStockTotalType.KHO_KH);
								equipStockTotal = equipmentStockTotalDAO.createEquipStockTotal(equipStockTotal);

							}
							eq.setStockCode(eqDeliveryRecord.getCustomer().getShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
							eq.setStockId(eqDeliveryRecord.getCustomer().getId());
							eq.setStockName(eqDeliveryRecord.getCustomer().getCustomerName());
							eq.setStockType(EquipStockTotalType.KHO_KH);
							eq.setUsageStatus(EquipUsageStatus.IS_USED);
							eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
							eq.setTradeType(null);
							if (eq.getFirstDateInUse() == null) {
								eq.setFirstDateInUse(sysDateIns);
							}
							eq.setUpdateUser(eqDeliveryRecord.getUpdateUser());
							eq.setUpdateDate(sysDateIns);
							equipMngDAO.updateEquipment(eq);

							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
							equipHistory.setEquip(eq);
							if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
								//equipHistory.setEquipWarehouseCode(eq.getStockCode());
							}
							equipHistory.setHealthStatus(eq.getHealthStatus());
							if (eq.getStockId() != null) {
								equipHistory.setObjectId(eq.getStockId());
							}
							if (eq.getStockType() != null) {
								equipHistory.setObjectType(eq.getStockType());
							}
							equipHistory.setFormId(eqDeliveryRecord.getId());
							equipHistory.setFormType(EquipTradeType.DELIVERY);
							equipHistory.setObjectCode(eq.getStockCode());
							equipHistory.setStockName(eq.getStockName());
							equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
//							equipHistory.setPriceActually(eq.getPriceActually());
							equipHistory.setStatus(eq.getStatus().getValue());
							equipHistory.setTradeStatus(eq.getTradeStatus());
							equipHistory.setTradeType(eq.getTradeType());
							equipHistory.setUsageStatus(eq.getUsageStatus());
							equipMngDAO.createEquipHistory(equipHistory);
							
							//cap nhat approvedDate
							eqDeliveryRecord.setApproveDate(sysDateIns);
							commonDAO.updateEntity(eqDeliveryRecord);
							mapEquip.put(eq.getId(), eq.getCode());
						}
					}
				} else if (status.equals(StatusRecordsEquip.DRAFT.getValue())) {
					int size = deliveryRecDtlsBD.size();
					for (int i = 0; i < size; i++) {
						EquipDeliveryRecDtl deliveryDtl = deliveryRecDtlsBD.get(i);
						Equipment eq = deliveryRecDtlsBD.get(i).getEquipment();
						Equipment eWeb = null;
						for (int j = 0, jsize = lstDeliveryRecDtls.size(); j < jsize; j++) {
							if (eq.getCode().equals(lstDeliveryRecDtls.get(j).getEquipment().getCode())) {
								eWeb = lstDeliveryRecDtls.get(j).getEquipment();
								lstDeliveryRecDtls.remove(lstDeliveryRecDtls.get(j));
								break;
							}
						}
						//update so seri
						if (eWeb.getSerial() != null && eq.getSerial() == null) {
							eq.setSerial(eWeb.getSerial());
							deliveryDtl.setEquipment(eq);
							commonDAO.updateEntity(deliveryDtl);
						}
					}
				}
				
				
				Map<Long, String> mapMaxCode = new HashMap<Long, String>();
				// them moi chi tiet
				if (lstDeliveryRecDtls != null) {
					for (int i = 0, isize = lstDeliveryRecDtls.size(); i < isize; i++ ) {
						//neu la cap moi
						if (lstDeliveryRecDtls.get(i).getContent().equals(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue())) {
							//tao moi thiet bi
							Equipment e = lstDeliveryRecDtls.get(i).getEquipment();
							String maxEquipmentCode = null;
							if (e.getEquipGroup() != null && e.getEquipGroup().getEquipCategory() != null) {
								EquipCategory ec = e.getEquipGroup().getEquipCategory();
								
								if (mapMaxCode.containsKey(ec.getId()) == true) {
									maxEquipmentCode = this.getEquipmentCodeByMaxCodeAndPrefixMaxLength(mapMaxCode.get(ec.getId()), ec.getCode(), maxLengthCode);
									mapMaxCode.put(ec.getId(), maxEquipmentCode);
								} else {
									maxEquipmentCode = equipMngDAO.getMaxEquipmentCodeByEquipCategoryId(ec.getId());
									maxEquipmentCode = this.getEquipmentCodeByMaxCodeAndPrefixMaxLength(maxEquipmentCode, ec.getCode(), maxLengthCode);
									mapMaxCode.put(ec.getId(), maxEquipmentCode);
								}
							}
							e.setCode(maxEquipmentCode);
							
							
							commonDAO.createEntity(e);
							
							EquipDeliveryRecDtl ed = lstDeliveryRecDtls.get(i);
							if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
//								e.setTradeType(EquipTradeType.DELIVERY);
//								e.setTradeStatus(EquipTradeStatus.TRADING.getValue());
//								e.setUpdateDate(sysDateIns);
//								e.setUpdateUser(eqDeliveryRecord.getCreateUser());
//								equipMngDAO.updateEquipment(e);
								
								// luu lich su thiet bi
								EquipHistory equipHistory = new EquipHistory();
								equipHistory.setCreateDate(sysDateIns);
								equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
								equipHistory.setEquip(e);
								
								equipHistory.setHealthStatus(e.getHealthStatus());
								if (e.getStockId() != null) {
									equipHistory.setObjectId(e.getStockId());
								}
								if (e.getStockType() != null) {
									equipHistory.setObjectType(e.getStockType());
								}
								
								equipHistory.setFormId(eqDeliveryRecord.getId());
								equipHistory.setFormType(EquipTradeType.DELIVERY);
								equipHistory.setObjectCode(e.getStockCode());
								equipHistory.setStockName(e.getStockName());
								equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
//								equipHistory.setPriceActually(e.getPriceActually());
								equipHistory.setStatus(e.getStatus().getValue());
								equipHistory.setTradeStatus(e.getTradeStatus());
								equipHistory.setTradeType(e.getTradeType());
								equipHistory.setUsageStatus(e.getUsageStatus());
								equipMngDAO.createEquipHistory(equipHistory);
			
							} else if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
								EquipStockTotal equipStockTotal = null;
								
								if (ed.getContent().equals(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue())) {
									EquipStockEquipFilter<EquipStockTotal> filterSt = new EquipStockEquipFilter<EquipStockTotal>();
									filterSt.setEquipGroupId(e.getEquipGroup().getId());
									filterSt.setStockType(EquipStockTotalType.KHO_TONG.getValue());  // kho hien tai chon
									EquipStockTotal total = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
									/**kiem tra kho tong co ton tai ko*/
									if(total!=null){ /**TH1: Ton tai kho tong*/
										/**Cap nhat kho tong*/
										if (total.getQuantity() != null) {
											total.setQuantity(total.getQuantity() + 1);
										} else {
											total.setQuantity(1);
										}
										total.setStockType(EquipStockTotalType.KHO_TONG); // Thay kho hien tai chon
										total.setUpdateUser(eqDeliveryRecord.getCreateUser());
										total.setUpdateDate(sysDateIns);
										equipMngDAO.updateEquipStockTotal(total);
										
									}else{ /**TH2: Chua ton tai kho tong*/
										/**Them moi kho tong*/
										total = new EquipStockTotal();
										total.setStockId(null);
										total.setEquipGroup(e.getEquipGroup());
										total.setStockType(EquipStockTotalType.KHO_TONG);
										total.setQuantity(1);
										total.setCreateUser(e.getCreateUser());
										total.setCreateDate(sysDateIns);
										total = equipMngDAO.createEquipStockTotal(total);
									}
								} else {
									// xoa kho VNM hoac kho NPP
//									equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.parseValue(eq.getStockType().getValue()), eq.getEquipGroup().getId());
//									equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
//									equipStockTotal.setUpdateDate(sysDateIns);
//									equipStockTotal.setUpdateUser(eqDeliveryRecord.getUpdateUser());
//									equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
								}
			
								//cap nhat approvedDate
								eqDeliveryRecord.setApproveDate(sysDateIns);
								commonDAO.updateEntity(eqDeliveryRecord);
								
								// cap nhat kho kh
								equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eqDeliveryRecord.getCustomer().getId(), EquipStockTotalType.KHO_KH, e.getEquipGroup().getId());
								if (equipStockTotal != null) {
									// cap nhat
									if (equipStockTotal.getQuantity() == null) {
										equipStockTotal.setQuantity(1);
									} else {
										equipStockTotal.setQuantity(equipStockTotal.getQuantity() + 1);
									}
									equipStockTotal.setUpdateDate(sysDateIns);
									equipStockTotal.setUpdateUser(eqDeliveryRecord.getCreateUser());
									equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
								} else {
									// tao moi
									equipStockTotal = new EquipStockTotal();
									equipStockTotal.setCreateDate(sysDateIns);
									equipStockTotal.setCreateUser(eqDeliveryRecord.getCreateUser());
									equipStockTotal.setEquipGroup(e.getEquipGroup());
									equipStockTotal.setQuantity(1);
									equipStockTotal.setStockId(eqDeliveryRecord.getCustomer().getId());
									equipStockTotal.setStockType(EquipStockTotalType.KHO_KH);
									equipStockTotal = equipmentStockTotalDAO.createEquipStockTotal(equipStockTotal);
			
								}
			//					eq.setStockCode(eqDeliveryRecord.getCustomer().getShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
								e.setStockCode(eqDeliveryRecord.getToShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
								e.setStockId(eqDeliveryRecord.getCustomer().getId());
								e.setStockName(eqDeliveryRecord.getCustomer().getCustomerName());
								e.setStockType(EquipStockTotalType.KHO_KH);
								e.setUsageStatus(EquipUsageStatus.IS_USED);
								e.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
								e.setTradeType(null);
								if (e.getFirstDateInUse() == null) {
									e.setFirstDateInUse(sysDateIns);
								}
								e.setUpdateDate(sysDateIns);
								e.setUpdateUser(eqDeliveryRecord.getCreateUser());
								equipMngDAO.updateEquipment(e);
			
								// luu lich su thiet bi
								EquipHistory equipHistory = new EquipHistory();
								equipHistory.setCreateDate(sysDateIns);
								equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
								equipHistory.setEquip(e);
								if (!StringUtility.isNullOrEmpty(e.getStockCode())) {
									//equipHistory.setEquipWarehouseCode(eq.getStockCode());
								}
								equipHistory.setHealthStatus(e.getHealthStatus());
								if (e.getStockId() != null) {
									equipHistory.setObjectId(e.getStockId());
								}
								if (e.getStockType() != null) {
									equipHistory.setObjectType(e.getStockType());
								}
								equipHistory.setFormId(eqDeliveryRecord.getId());
								equipHistory.setFormType(EquipTradeType.DELIVERY);
								equipHistory.setObjectCode(e.getStockCode());
								equipHistory.setStockName(e.getStockName());
								equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
//								equipHistory.setPriceActually(e.getPriceActually());
								equipHistory.setStatus(e.getStatus().getValue());
								equipHistory.setTradeStatus(e.getTradeStatus());
								equipHistory.setTradeType(e.getTradeType());
								equipHistory.setUsageStatus(e.getUsageStatus());
								equipMngDAO.createEquipHistory(equipHistory);
							}
							// luu chi tiet bien ban
							ed.setEquipDeliveryRecord(eqDeliveryRecord);
							ed.setHealthStatus(ed.getEquipment().getHealthStatus());
							ed.setPrice(ed.getEquipment().getPrice());
							ed = equipmentRecordDeliveryDAO.createEquipmentRecordDeliveryDetail(ed);
						} else {// khong phai cap moi
							EquipDeliveryRecDtl ed = lstDeliveryRecDtls.get(i);
							Equipment eq = ed.getEquipment();
							Equipment eqInDB = equipMngDAO.getEquipmentById(eq.getId());
							if (eqInDB != null) {
								// luu thay doi thiet bi trong chi tiet bien ban
								eqInDB.setSerial(eq.getSerial());//set so seri lai khi lay equipment tu DB len
								if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
									eqInDB.setTradeType(EquipTradeType.DELIVERY);
									eqInDB.setTradeStatus(EquipTradeStatus.TRADING.getValue());
									eqInDB.setUpdateDate(sysDateIns);
									eqInDB.setUpdateUser(eqDeliveryRecord.getCreateUser());
									equipMngDAO.updateEquipment(eqInDB);
									// luu lich su thiet bi
									EquipHistory equipHistory = new EquipHistory();
									equipHistory.setCreateDate(sysDateIns);
									equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
									equipHistory.setEquip(eqInDB);
									if (!StringUtility.isNullOrEmpty(eqInDB.getStockCode())) {
										//equipHistory.setEquipWarehouseCode(eq.getStockCode());
									}
									equipHistory.setHealthStatus(eqInDB.getHealthStatus());
									if (eqInDB.getStockId() != null) {
										equipHistory.setObjectId(eqInDB.getStockId());
									}
									if (eqInDB.getStockType() != null) {
										equipHistory.setObjectType(eqInDB.getStockType());
									}
									equipHistory.setFormId(eqDeliveryRecord.getId());
									equipHistory.setFormType(EquipTradeType.DELIVERY);
									equipHistory.setObjectCode(eqInDB.getStockCode());
									equipHistory.setStockName(eqInDB.getStockName());
									equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
//									equipHistory.setPriceActually(eqInDB.getPriceActually());
									equipHistory.setStatus(eqInDB.getStatus().getValue());
									equipHistory.setTradeStatus(eqInDB.getTradeStatus());
									equipHistory.setTradeType(eqInDB.getTradeType());
									equipHistory.setUsageStatus(eqInDB.getUsageStatus());
									equipMngDAO.createEquipHistory(equipHistory);
				
								} else if (eqDeliveryRecord.getRecordStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
									EquipStockTotal equipStockTotal = null;
									
									if (ed.getContent().equals(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue())) {
										EquipStockEquipFilter<EquipStockTotal> filterSt = new EquipStockEquipFilter<EquipStockTotal>();
										filterSt.setEquipGroupId(eq.getEquipGroup().getId());
										filterSt.setStockType(EquipStockTotalType.KHO_TONG.getValue());  // kho hien tai chon
										EquipStockTotal total = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
										/**kiem tra kho tong co ton tai ko*/
										if(total!=null){ /**TH1: Ton tai kho tong*/
											/**Cap nhat kho tong*/
											if (total.getQuantity() != null) {
												total.setQuantity(total.getQuantity() + 1);
											} else {
												total.setQuantity(1);
											}
											total.setStockType(EquipStockTotalType.KHO_TONG); // Thay kho hien tai chon
											total.setUpdateUser(eqDeliveryRecord.getCreateUser());
											total.setUpdateDate(sysDateIns);
											equipMngDAO.updateEquipStockTotal(total);
											
										}else{ /**TH2: Chua ton tai kho tong*/
											/**Them moi kho tong*/
											total = new EquipStockTotal();
											total.setStockId(null);
											total.setEquipGroup(eq.getEquipGroup());
											total.setStockType(EquipStockTotalType.KHO_TONG);
											total.setQuantity(1);
											total.setCreateUser(eq.getCreateUser());
											total.setCreateDate(sysDateIns);
											total = equipMngDAO.createEquipStockTotal(total);
										}
									} else {
										// xoa kho VNM hoac kho NPP
										equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.parseValue(eq.getStockType().getValue()), eq.getEquipGroup().getId());
										equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
										equipStockTotal.setUpdateDate(sysDateIns);
										equipStockTotal.setUpdateUser(eqDeliveryRecord.getUpdateUser());
										equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
									}
				
									//cap nhat approvedDate
									eqDeliveryRecord.setApproveDate(sysDateIns);
									commonDAO.updateEntity(eqDeliveryRecord);
									
									// cap nhat kho kh
									equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eqDeliveryRecord.getCustomer().getId(), EquipStockTotalType.KHO_KH, eqInDB.getEquipGroup().getId());
									if (equipStockTotal != null) {
										// cap nhat
										if (equipStockTotal.getQuantity() == null) {
											equipStockTotal.setQuantity(1);
										} else {
											equipStockTotal.setQuantity(equipStockTotal.getQuantity() + 1);
										}
										equipStockTotal.setUpdateDate(sysDateIns);
										equipStockTotal.setUpdateUser(eqDeliveryRecord.getCreateUser());
										equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
									} else {
										// tao moi
										equipStockTotal = new EquipStockTotal();
										equipStockTotal.setCreateDate(sysDateIns);
										equipStockTotal.setCreateUser(eqDeliveryRecord.getCreateUser());
										equipStockTotal.setEquipGroup(eqInDB.getEquipGroup());
										equipStockTotal.setQuantity(1);
										equipStockTotal.setStockId(eqDeliveryRecord.getCustomer().getId());
										equipStockTotal.setStockType(EquipStockTotalType.KHO_KH);
										equipStockTotal = equipmentStockTotalDAO.createEquipStockTotal(equipStockTotal);
				
									}
				//					eq.setStockCode(eqDeliveryRecord.getCustomer().getShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
									eqInDB.setStockCode(eqDeliveryRecord.getToShop().getShopCode() + eqDeliveryRecord.getCustomer().getShortCode());
									eqInDB.setStockId(eqDeliveryRecord.getCustomer().getId());
									eqInDB.setStockName(eqDeliveryRecord.getCustomer().getCustomerName());
									eqInDB.setStockType(EquipStockTotalType.KHO_KH);
									eqInDB.setUsageStatus(EquipUsageStatus.IS_USED);
									eqInDB.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
									eqInDB.setTradeType(null);
									if (eqInDB.getFirstDateInUse() == null) {
										eqInDB.setFirstDateInUse(sysDateIns);
									}
									eqInDB.setUpdateDate(sysDateIns);
									eqInDB.setUpdateUser(eqDeliveryRecord.getCreateUser());
									equipMngDAO.updateEquipment(eqInDB);
				
									// luu lich su thiet bi
									EquipHistory equipHistory = new EquipHistory();
									equipHistory.setCreateDate(sysDateIns);
									equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
									equipHistory.setEquip(eqInDB);
									if (!StringUtility.isNullOrEmpty(eqInDB.getStockCode())) {
										//equipHistory.setEquipWarehouseCode(eq.getStockCode());
									}
									equipHistory.setHealthStatus(eqInDB.getHealthStatus());
									if (eqInDB.getStockId() != null) {
										equipHistory.setObjectId(eqInDB.getStockId());
									}
									if (eqInDB.getStockType() != null) {
										equipHistory.setObjectType(eqInDB.getStockType());
									}
									equipHistory.setFormId(eqDeliveryRecord.getId());
									equipHistory.setFormType(EquipTradeType.DELIVERY);
									equipHistory.setObjectCode(eqInDB.getStockCode());
									equipHistory.setStockName(eqInDB.getStockName());
									equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
//									equipHistory.setPriceActually(eqInDB.getPriceActually());
									equipHistory.setStatus(eqInDB.getStatus().getValue());
									equipHistory.setTradeStatus(eqInDB.getTradeStatus());
									equipHistory.setTradeType(eqInDB.getTradeType());
									equipHistory.setUsageStatus(eqInDB.getUsageStatus());
									equipMngDAO.createEquipHistory(equipHistory);
								}
								// luu chi tiet bien ban
								ed.setEquipDeliveryRecord(eqDeliveryRecord);
								ed.setHealthStatus(ed.getEquipment().getHealthStatus());
								ed.setPrice(ed.getEquipment().getPrice());
//								ed.setPrice(Integer.parseInt(ed.getEquipment().getPrice()+""));
								ed = equipmentRecordDeliveryDAO.createEquipmentRecordDeliveryDetail(ed);
							}
						}
					}
				}
			} else {//huy
				List<EquipDeliveryRecDtl> deliveryRecDtlsDB = equipmentRecordDeliveryDAO.getListEquipDeliveryRecDtlByEquipDeliveryRecId(eqDeliveryRecord.getId());
				int size = deliveryRecDtlsDB.size();
				for (int i = 0; i < size; i++) {
					Equipment eq = deliveryRecDtlsDB.get(i).getEquipment();
					//truyen xuong 1 list deleted, va xoa no.
					//neu la cap moi thi xoa het
					if (deliveryRecDtlsDB.get(i).getContent().equals(ApParamEquipType.DELIVERY_CONTENT_NEW.getValue())) {
//						Boolean isDeteled = false;
//						for (int j = 0, jsize = lstEquipDelete.size(); j < jsize; j++) {
//							if (eq.getId().equals(lstEquipDelete.get(j).getEquipment().getId())) {
//								isDeteled = true;
//								break;
//							}
//						}
//						if (!isDeteled) {
							//xoa dong lich su
							List<EquipHistory> lstEquipHistory = equipMngDAO.getListEquipHistoryByEquipId(eq.getId());
							for (int c = 0, csize = lstEquipHistory.size(); c < csize; c++) {
								equipmentHistoryDelete.add(lstEquipHistory.get(c));
							}
							//xoa dong chi tiet
							deliveryRecDtlDelete.add(deliveryRecDtlsDB.get(i));
							//xoa dong thiet bi
							equipmentDelete.add(deliveryRecDtlsDB.get(i).getEquipment());
//						}

					} else {
						Boolean isDeteled = false;
						for (int j = 0, jsize = lstEquipDelete.size(); j < jsize; j++) {
							if (eq.getId().equals(lstEquipDelete.get(j).getEquipment().getId())) {
								isDeteled = true;
								break;
							}
						}
						if (isDeteled) {
							deliveryRecDtlDelete.add(deliveryRecDtlsDB.get(i));
						}
						
						
						eq.setTradeType(null);
						eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						eq.setUpdateUser(eqDeliveryRecord.getUpdateUser());
						eq.setUpdateDate(sysDateIns);
						equipMngDAO.updateEquipment(eq);

						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
						equipHistory.setEquip(eq);
						if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
							//equipHistory.setEquipWarehouseCode(eq.getStockCode());
						}
						equipHistory.setHealthStatus(eq.getHealthStatus());
						if (eq.getStockId() != null) {
							equipHistory.setObjectId(eq.getStockId());
						}
						if (eq.getStockType() != null) {
							equipHistory.setObjectType(eq.getStockType());
						}
						equipHistory.setFormId(eqDeliveryRecord.getId());
						equipHistory.setFormType(EquipTradeType.DELIVERY);
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
						equipHistory.setPriceActually(eq.getPriceActually());
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipMngDAO.createEquipHistory(equipHistory);
					}

//					equipmentRecordDeliveryDAO.deleteEquipmentRecordDeliveryDetail(deliveryRecDtls.get(i));
				}
			}
			
			//xoa dong chi tiet
			for (int i = 0, isize = deliveryRecDtlDelete.size(); i < isize; i++) {
				commonDAO.deleteEntity(deliveryRecDtlDelete.get(i));
			}
			//xoa dong thiet bi
			for (int i = 0, isize = equipmentDelete.size(); i < isize; i++) {
				commonDAO.deleteEntity(equipmentDelete.get(i));
			}
			//xoa dong lich su
			for (int i = 0, isize = equipmentHistoryDelete.size(); i < isize; i++) {
				commonDAO.deleteEntity(equipmentHistoryDelete.get(i));
			}
			
			//Xu ly xoa file
			if (filterFile.getLstEquipAttachFileId() != null && !filterFile.getLstEquipAttachFileId().isEmpty()) {
				BasicFilter<EquipAttachFile> filterB = new BasicFilter<EquipAttachFile>();
				filterB.setLstId(filterFile.getLstEquipAttachFileId());
				List<EquipAttachFile> lstFile = equipRecordDAO.searchEquipAttachFileByFilter(filterB);
				if (lstFile != null && !lstFile.isEmpty()) {
					for (EquipAttachFile fileVItem : lstFile) {
						equipRecordDAO.deletEquipAttachFile(fileVItem);
					}
				}
			}
			//Xu ly them file
			if (filterFile.getLstFileVo() != null && !filterFile.getLstFileVo().isEmpty()) {
				for (FileVO voFile : filterFile.getLstFileVo()) {
					EquipAttachFile equipAttFile = new EquipAttachFile();
					equipAttFile.setUrl(voFile.getUrl());
					equipAttFile.setObjectId(eqDeliveryRecord.getId());
					equipAttFile.setFileName(voFile.getFileName());
					equipAttFile.setThumbUrl(voFile.getThumbUrl());
					equipAttFile.setObjectType(EquipTradeType.DELIVERY.getValue());
					equipAttFile.setCreateDate(sysDateIns);
					equipAttFile.setCreateUser(eqDeliveryRecord.getUpdateUser());
					equipRecordDAO.createEquipAttachFile(equipAttFile);
				}
			}
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<EquipmentExVO> getListAllEquipmentDeliveryByCustomerId(Long customerId, List<Long> lstEquipId) throws BusinessException {
		try {
			List<EquipmentExVO> lst = equipMngDAO.getListAllEquipmentDeliveryByCustomerId(customerId, lstEquipId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> getListEquipStock(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getListEquipStock(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> searchEquipmentStockChange(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.searchEquipmentStockChange(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipStockTransForm getEquipStockTransFormById(Long idRecord) throws BusinessException {
		try {
			return idRecord == null ? null : equipmentStockTotalDAO.getEquipStockTransFormById(idRecord);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipStockTransForm getEquipStockTransFormById(Long idRecord, Long shopId) throws BusinessException {
		try {
			return idRecord == null ? null : equipmentStockTotalDAO.getEquipStockTransFormById(idRecord, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveStatusEquipStockRecord(EquipStockTransForm e, Integer statusRecord) throws BusinessException {
		try {
			Date sysDateIns = commonDAO.getSysDate();
			final Integer EQUIP_STOCK_TRANS_FORM_STATUS_APPROVED = 2;
			final Integer EQUIP_STOCK_TRANS_FORM_STATUS_CANCELED = 4;
			if (e.getStatus() != null) {
				if (e.getStatus() == EQUIP_STOCK_TRANS_FORM_STATUS_APPROVED) {
					e.setApproveDate(sysDateIns);
				} else if (e.getStatus() == EQUIP_STOCK_TRANS_FORM_STATUS_CANCELED) {
					e.setRefuseDate(sysDateIns);
				}
			}
			equipMngDAO.updateEquipStockTransForm(e);
			if (EQUIP_STOCK_TRANS_FORM_STATUS_APPROVED.equals(statusRecord)) {
				// luu lich su chuyen kho
				EquipFormHistory equipFormHistory = new EquipFormHistory();
				equipFormHistory.setActDate(sysDateIns);
				equipFormHistory.setCreateDate(sysDateIns);
				equipFormHistory.setCreateUser(e.getUpdateUser());
				equipFormHistory.setRecordStatus(e.getStatus());
				equipFormHistory.setRecordId(e.getId());
				equipFormHistory.setRecordType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
				equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
				//thuc hien thao tac voi thiet bi
//				List<EquipStockTransFormDtl> deliveryRecDtls = equipmentRecordDeliveryDAO.getListEquipStockTransFormDtlById(e.getId());
//				int size = deliveryRecDtls.size();
//				for (int i = 0; i < size; i++) {
//					Equipment eq = deliveryRecDtls.get(i).getEquip();
//					eq.setTradeType(null);
//					eq.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);
//					eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
//					/*Shop shop = shopDAO.getShopById(e.getToStockId());
//					if (ShopObjectType.NPP.getValue().equals(shop.getType().getObjectType())) {
//						//eq.setStockType(EquipStockTotalType.KHO_NPP);
//					} else {
//						//eq.setStockType(EquipStockTotalType.KHO_VNM);
//					}*/
//					//eq.setStockId(e.getToStockId());
//					//eq.setStockCode(shop.getShopCode());
//					eq.setUpdateUser(e.getUpdateUser());
//					eq.setUpdateDate(sysDateIns);
//					equipMngDAO.updateEquipment(eq);
//
//					EquipHistory qHis = new EquipHistory();
//					qHis.setCreateDate(sysDateIns);
//					qHis.setCreateUser(e.getUpdateUser());
//					qHis.setEquip(eq);
//					qHis.setHealthStatus(eq.getHealthStatus());
//					//qHis.setEquipWarehouseCode(eq.getStockCode());
//					qHis.setObjectId(eq.getStockId());
//					qHis.setObjectType(eq.getStockType());
//					qHis.setUsageStatus(eq.getUsageStatus());
//					qHis.setStatus(eq.getStatus().getValue());
//					qHis.setTradeType(eq.getTradeType());
//					qHis.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
//					equipMngDAO.createEquipHistory(qHis);
//
////					//Xoa thiet bi kho nguon
////					EquipStockEquipFilter<EquipStockTotal> filterTt = new EquipStockEquipFilter<EquipStockTotal>();
////					//filterTt.setStockId(e.getFromStockId());
////					//filterTt.setStockType(e.getFromStockType());
////					filterTt.setEquipGroupId(eq.getEquipGroup().getId());
////					EquipStockTotal equipStockTotal = equipMngDAO.getFirtEquipStockTotalByFilter(filterTt);
////					//equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.parseValue(eq.getStockType().getValue()), eq.getEquipGroup().getId());
////					if (equipStockTotal != null) {
////						if (equipStockTotal.getQuantity() != null && equipStockTotal.getQuantity() > 0) {
////							equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
////						} else {
////							equipStockTotal.setQuantity(0);
////						}
////						equipStockTotal.setUpdateDate(sysDateIns);
////						equipStockTotal.setUpdateUser(e.getCreateUser());
////						equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
////					}
////
////					// cap nhat kho dich
////					filterTt = new EquipStockEquipFilter<EquipStockTotal>();
////					//filterTt.setStockId(e.getToStockId());
////					//filterTt.setStockType(e.getToStockType());
////					filterTt.setEquipGroupId(eq.getEquipGroup().getId());
////					equipStockTotal = equipMngDAO.getFirtEquipStockTotalByFilter(filterTt);
////					//equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(e.getToStockId(), EquipStockTotalType.parseValue(e.getToStockType()), eq.getEquipGroup().getId());
////					if (equipStockTotal != null) {
////						// cap nhat
////						if (equipStockTotal.getQuantity() == null) {
////							equipStockTotal.setQuantity(1);
////						} else {
////							equipStockTotal.setQuantity(equipStockTotal.getQuantity() + 1);
////						}
////						equipStockTotal.setUpdateDate(sysDateIns);
////						equipStockTotal.setUpdateUser(e.getCreateUser());
////						equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
////					} else {
////						// tao moi
////						equipStockTotal = new EquipStockTotal();
////						equipStockTotal.setCreateDate(sysDateIns);
////						equipStockTotal.setCreateUser(e.getCreateUser());
////						equipStockTotal.setEquipGroup(eq.getEquipGroup());
////						equipStockTotal.setQuantity(1);
////						//equipStockTotal.setStockId(e.getToStockId());
////						//equipStockTotal.setStockType(EquipStockTotalType.parseValue(e.getToStockType()));
////						equipStockTotal = equipmentStockTotalDAO.createEquipStockTotal(equipStockTotal);
////					}
//				}
			}
			if (EQUIP_STOCK_TRANS_FORM_STATUS_CANCELED.equals(statusRecord)) {
				List<EquipStockTransFormDtl> deliveryRecDtls = equipmentRecordDeliveryDAO.getListEquipStockTransFormDtlById(e.getId());
				int size = deliveryRecDtls.size();
				// luu lich su chuyen kho
				EquipFormHistory equipFormHistory = new EquipFormHistory();
				equipFormHistory.setActDate(sysDateIns);
				equipFormHistory.setCreateDate(sysDateIns);
				equipFormHistory.setCreateUser(e.getUpdateUser());
				equipFormHistory.setRecordStatus(e.getStatus());
				equipFormHistory.setRecordId(e.getId());
				equipFormHistory.setRecordType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
				equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
				for (int i = 0; i < size; i++) {
					Equipment eq = deliveryRecDtls.get(i).getEquip();
					eq.setTradeType(null);
					eq.setTradeStatus(0);
					eq.setUpdateUser(e.getUpdateUser());
					eq.setUpdateDate(sysDateIns);
					equipMngDAO.updateEquipment(eq);

					EquipHistory qHis = new EquipHistory();
					qHis.setCreateDate(sysDateIns);
					qHis.setCreateUser(e.getUpdateUser());
					qHis.setEquip(eq);
					if (eq.getHealthStatus() != null) {
						qHis.setHealthStatus(eq.getHealthStatus());
					}
					qHis.setObjectId(eq.getStockId());
					qHis.setObjectType(eq.getStockType());
					qHis.setUsageStatus(eq.getUsageStatus());
					qHis.setStatus(eq.getStatus().getValue());
					qHis.setTradeType(eq.getTradeType());
					qHis.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
					qHis.setObjectCode(eq.getStockCode());
					qHis.setFormId(e.getId());
					qHis.setFormType(EquipTradeType.WAREHOUSE_TRANSFER);
					qHis.setStockName(eq.getStockName());
					qHis.setTableName("EQUIP_STOCK_TRANS_FORM");
					equipMngDAO.createEquipHistory(qHis);
				}
			}
		} catch (Exception e2) {
			throw new BusinessException(e2);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveDonePerformStatusRecord(EquipStockTransForm e, Integer performStatusRecord) throws BusinessException {
		try {
			equipMngDAO.updateEquipStockTransForm(e);
		} catch (Exception e2) {
			throw new BusinessException(e2);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipStockTransForm updateEquipStockTransForm(EquipStockTransForm e, LogInfoVO logInfoVO) throws BusinessException {
		// TODO Auto-generated method stub
		try {

		} catch (Exception e2) {
			// TODO: handle exception
		}
		return null;
	}

	@Override
	public ObjectVO<EquipmentDeliveryVO> getListEquipmentStockVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException {
		try {
			List<EquipmentDeliveryVO> lst = equipMngDAO.getListEquipmentStockVOByFilter(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> objVO = new ObjectVO<EquipmentDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentLiquidationFormVO> getListEquipmentLiquidationFormVOByFilter(EquipmentFilter<EquipmentLiquidationFormVO> equipmentFilter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<EquipmentLiquidationFormVO> lst = equipmentLiquidationFormDAO.getListEquipmentLiquidationFormVOByFilter(equipmentFilter);
			ObjectVO<EquipmentLiquidationFormVO> objVO = new ObjectVO<EquipmentLiquidationFormVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipLiquidationForm getEquipLiquidationFormById(Long idForm) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return idForm == null ? null : equipmentLiquidationFormDAO.getEquipmentLiquidationFormById(idForm);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipLiquidationForm getEquipLiquidationFormById(Long idForm, Long shopId) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return idForm == null ? null : equipmentLiquidationFormDAO.getEquipmentLiquidationFormById(idForm, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> getListEquipToStock(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getListEquipToStock(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> getListEquipGroupById(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getListEquipGroupById(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> listEquipCategoryCode(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.listEquipCategoryCode(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> listEquipEquipProvider(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.listEquipEquipProvider(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> listEquipBrand(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.listEquipBrand(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> listEquipItem(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.listEquipItem(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> getListEquipGroupByIdEquipProvider(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getListEquipGroupByIdEquipProvider(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> listEquipItemById(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.listEquipItemById(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	
	@Override
	public EquipItemConfigDtl getEquipItemConfigDtl(EquipItemFilter filter) throws BusinessException {
		try {
			return equipMngDAO.getEquipItemConfigDtl(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Object getDoanhSoEquipRepair(EquipRepairFilter filter) throws BusinessException {
		try {
			return equipMngDAO.getDoanhSoEquipRepair(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<EquipRepairFormVO> getListEquipRepair(EquipRepairFilter filter) throws BusinessException {
		try {
			List<EquipRepairFormVO> lst = equipMngDAO.getListEquipRepair(filter);
			ObjectVO<EquipRepairFormVO> objVO = new ObjectVO<EquipRepairFormVO>();
			objVO.setLstObject(lst);
			objVO.setkPaging(filter.getkPaging());
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipRepairFormDtl> getListEquipRepairDtlByEquipRepairId(EquipRepairFilter filter) throws BusinessException {
		try {
			List<EquipRepairFormDtl> lst = equipMngDAO.getListEquipRepairDtlByEquipRepairId(filter);

			/*
			 * lay thong tin thanh toan cho tung hang muc
			 */
			// khong lay thong tin phieu thanh toan nua
			/*if (filter.getEquipRepairId() != null) {
				List<EquipRepairPayFormDtl> equipmentRepairPaymentDetails = equipMngDAO.retrieveEquipmentRepairPaymentDetails(filter.getEquipRepairId());
				if (lst != null && lst.size() > 0 && equipmentRepairPaymentDetails != null && equipmentRepairPaymentDetails.size() > 0) {
					for (EquipRepairPayFormDtl equipmentRepairPaymentDetail : equipmentRepairPaymentDetails) {
						for (int i = 0, size = lst.size(); i < size; i++) {
							if (lst.get(i).getId().compareTo(equipmentRepairPaymentDetail.getEquipRepairFormDtl().getId()) == 0) {
								lst.get(i).setTotalActualAmount(equipmentRepairPaymentDetail.getTotalActualAmount().toString());
								break;
							}
						}
					}
				}
			}*/

			ObjectVO<EquipRepairFormDtl> objVO = new ObjectVO<EquipRepairFormDtl>();
			objVO.setLstObject(lst);
			objVO.setkPaging(filter.getkPagingDetail());
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * Lay danh sach phieu sua chua; dung cho tao moi ds chi tiet cua phieu thanh toan
	 * @author vuongmq
	 * @since 24/06/2015
	 * @return
	 */
	@Override
	public List<EquipRepairForm> getListEquipRepairFormFilter(EquipRepairFilter filter) throws BusinessException {
		try {
			return equipMngDAO.getListEquipRepairFormFilter(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/***
	 * @author vuongmq
	 * @date 11/05/2015
	 * lay danh sách kho cap tren cua Thiet bi ung voi tung loai 1: NPP; 2: KH
	 */
	@Override
	public List<EquipStock> getListEquipStockParentByFilter(EquipStockEquipFilter<EquipStock> filter) throws BusinessException {
		try {
			return equipMngDAO.getListEquipStockParentByFilter(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/***
	 * @author vuongmq
	 * @date 11/05/2015
	 * lay danh sách kho cap tren cua Thiet bi ung voi tung loai 1: NPP; 2: KH
	 */
	@Override
	public List<EquipStock> getListEquipStockParentSameLevelByFilter(EquipStockEquipFilter<EquipStock> filter) throws BusinessException {
		try {
			return equipMngDAO.getListEquipStockParentSameLevelByFilter(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<EquipCategory> getListCategory() throws BusinessException {
		try {
			return equipMngDAO.getListCategory();
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<EquipmentVO> getlistEquipGroup(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getlistEquipGroup(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipRepairFormVO> getListEquipRepairEx(EquipRepairFilter filter) throws BusinessException {
		try {
			List<EquipRepairFormVO> lst = equipMngDAO.getListEquipRepairEx(filter);
			ObjectVO<EquipRepairFormVO> objVO = new ObjectVO<EquipRepairFormVO>();
			objVO.setLstObject(lst);
			objVO.setkPaging(filter.getkPaging());
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipRepairForm createEquipRepairForm(EquipRepairForm equipRepairForm) throws BusinessException {
		try {
			if (equipRepairForm != null && equipRepairForm.getEquip() != null) {
				if (equipRepairForm.getEquip().getStockType() != null && equipRepairForm.getEquip().getStockId() != null && EquipStockTotalType.KHO_KH.getValue().equals(equipRepairForm.getEquip().getStockType().getValue())) {
					Staff gsgn = equipmentRecordDeliveryDAO.getStaffInEquipDelivRecordApproved(equipRepairForm.getEquip().getId(), equipRepairForm.getEquip().getStockId());
					if (gsgn != null) {
						equipRepairForm.setStaff(gsgn);
					}
				}
			}
			return equipMngDAO.createEquipRepairForm(equipRepairForm);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipRepairForm updateEquipRepairForm(EquipRepairForm equipRepairForm) throws BusinessException {
		try {
			return equipMngDAO.updateEquipRepairForm(equipRepairForm);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipRepairForm getEquipRepairFormById(long id) throws BusinessException {
		try {
			return equipMngDAO.getEquipRepairFormById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author vuongmq
	 * @date 22/04/2015 
	 * in phieu phieu yeu cau sua chua*/
	@Override
	public List<EquipRepairFormVOList> getEquipRepairPrint(EquipRepairFilter filter) throws BusinessException {
		try {
			//List<EquipRepairFormVOList> lstEquipRepairFormVO = equipMngDAO.getListEquipRepair(filter);
			List<EquipRepairFormVOList> lstEquipRepairFormVO = equipMngDAO.getListEquipRepairPrint(filter);
			if (lstEquipRepairFormVO != null) {
				for (int i = 0, sz = lstEquipRepairFormVO.size(); i < sz; i++) {
					if (lstEquipRepairFormVO.get(i) != null && lstEquipRepairFormVO.get(i).getId() != null) {
						filter = new EquipRepairFilter();
						filter.setEquipRepairId(lstEquipRepairFormVO.get(i).getId());;
						List<EquipmentRepairFormDtlVO> lstEquipDetail = equipMngDAO.getListEquipRepairDtlVOByEquipRepairId(filter);
						if (lstEquipDetail != null) {
							lstEquipRepairFormVO.get(i).setLstDetail(lstEquipDetail);
						}
					}
				}
			}
			return lstEquipRepairFormVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author vuongmq
	 * cap nhat @date 17/04/2015 
	 * cap nhat phieu yeu cau sua chua*/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEquipRepairFormStatus(EquipRepairForm equipRepairForm, List<EquipRepairFormDtl> lstEquipRepairFormDtl, int status, Equipment equipOld, EquipRecordFilter<EquipRepairForm> filter) throws BusinessException {
		try {
			Date date = commonDAO.getSysDate();
			String username = equipRepairForm.getUpdateUser();
			/** truong hop cap nhat trang thai phieu updateStatus(): param 1: equipRepairForm != null va 2: status != null   */
			Integer VALUE_UPDATE_2 = 2;
			if (filter != null) {
				filter.setChangeDate(date);
				filter.setUserName(username);
			}
			Equipment equip = equipRepairForm.getEquip();
			equip.setUpdateDate(date);
			equip.setUpdateUser(username);
			/**cap nhat lai thiet bi moi vao phieu sua chua*/
			if (equipOld != null && !equipOld.getId().equals(equip.getId())) {
				equip.setTradeStatus(EquipTradeStatus.TRADING.getValue());
				equip.setTradeType(EquipTradeType.CORRECTED);
				equipMngDAO.updateEquipment(equip);
				/** ghi lai lich su thiet bi moi */
				EquipHistory qHis = new EquipHistory();
				qHis.setCreateDate(date);
				qHis.setCreateUser(username);
				qHis.setEquip(equip);
				qHis.setHealthStatus(equip.getHealthStatus());
				qHis.setObjectId(equip.getStockId());
				qHis.setObjectType(equip.getStockType());
				qHis.setUsageStatus(equip.getUsageStatus());
				qHis.setStatus(equip.getStatus().getValue());
				qHis.setTradeType(equip.getTradeType());
				qHis.setTradeStatus(equip.getTradeStatus());
				
				/*** vuongmq; 26/05/2015; cap nhat them cac truong equipment_history*/
				qHis.setObjectCode(equip.getStockCode());
				qHis.setFormId(equipRepairForm.getId());
				qHis.setFormType(EquipTradeType.CORRECTED); // sua chua
				qHis.setStockName(equip.getStockName());
				qHis.setTableName(TableName.EQUIP_REPAIR_FORM.getValue());
				equipMngDAO.createEquipHistory(qHis);
				/** update lai thiet bi cu */
				equipOld.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
				equipOld.setTradeType(null);
				equipMngDAO.updateEquipment(equipOld);
				/** ghi lai lich su thiet bi cu */
				qHis = new EquipHistory();
				qHis.setCreateDate(date);
				qHis.setCreateUser(username);
				qHis.setEquip(equipOld);
				qHis.setHealthStatus(equipOld.getHealthStatus());
				qHis.setObjectId(equipOld.getStockId());
				qHis.setObjectType(equipOld.getStockType());
				qHis.setUsageStatus(equipOld.getUsageStatus());
				qHis.setStatus(equipOld.getStatus().getValue());
				qHis.setTradeType(equipOld.getTradeType());
				qHis.setTradeStatus(equipOld.getTradeStatus());
				
				/*** vuongmq; 26/05/2015; cap nhat them cac truong equipment_history*/
				qHis.setObjectCode(equipOld.getStockCode());
				qHis.setFormId(equipRepairForm.getId());
				qHis.setFormType(EquipTradeType.CORRECTED); // sua chua
				qHis.setStockName(equipOld.getStockName());
				qHis.setTableName(TableName.EQUIP_REPAIR_FORM.getValue());
				equipMngDAO.createEquipHistory(qHis);
			}
			/** Trang thai Sua chua: Dang sua chua; hoan tat; khong duyet; duyet */
			if (StatusRecordsEquip.ARE_CORRECTED.equals(equipRepairForm.getStatus()) 
					|| StatusRecordsEquip.COMPLETION_REPAIRS.equals(equipRepairForm.getStatus())
					|| StatusRecordsEquip.NO_APPROVAL.equals(equipRepairForm.getStatus())
					|| StatusRecordsEquip.APPROVED.equals(equipRepairForm.getStatus())) {
				/** Trang thai Sua chua: Dang sua chua */
				if (StatusRecordsEquip.ARE_CORRECTED.equals(equipRepairForm.getStatus())) {
					equip.setUsageStatus(EquipUsageStatus.SHOWING_REPAIR);//dang sua chua
				} else if (StatusRecordsEquip.COMPLETION_REPAIRS.equals(equipRepairForm.getStatus())) {
					/** Trang thai Sua chua: Hoan tat */
					if (EquipStockTotalType.KHO_KH.equals(equip.getStockType())) { //kho kh
						equip.setUsageStatus(EquipUsageStatus.IS_USED);//dang su dung
					} else {
						equip.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);// dang o kho
					}
					equip.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
					equip.setTradeType(null);
				} else if (StatusRecordsEquip.NO_APPROVAL.equals(equipRepairForm.getStatus())) {
					/** Trang thai Sua chua: Khong duyet */
					equip.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());//ko giao dich(binh thuong)
					equip.setTradeType(null);
				} else if (StatusRecordsEquip.APPROVED.equals(equipRepairForm.getStatus())) {
					/** Trang thai Sua chua: Duyet */; // vuongmq; 26/05/2015; cap nhat; APPROVE_DATE
					// vuongmq; 02/07/2015; cap nhat luc nay duyet la chuyen qua trang thai sua chua luon
					equip.setUsageStatus(EquipUsageStatus.SHOWING_REPAIR);//dang sua chua
					equipRepairForm.setStatus(StatusRecordsEquip.ARE_CORRECTED); // trang thai la sua chua
					equipRepairForm.setApproveDate(date);
				}
				equipMngDAO.updateEquipment(equip);
				/** ghi lai lich su thiet bi moi */
				EquipHistory qHis = new EquipHistory();
				qHis.setCreateDate(date);
				qHis.setCreateUser(username);
				qHis.setEquip(equip);
				qHis.setHealthStatus(equip.getHealthStatus());
				qHis.setObjectId(equip.getStockId());
				qHis.setObjectType(equip.getStockType());
				qHis.setUsageStatus(equip.getUsageStatus());
				qHis.setStatus(equip.getStatus().getValue());
				qHis.setTradeType(equip.getTradeType());
				qHis.setTradeStatus(equip.getTradeStatus());
				
				/*** vuongmq; 26/05/2015; cap nhat them cac truong equipment_history*/
				qHis.setObjectCode(equip.getStockCode());
				qHis.setFormId(equipRepairForm.getId());
				qHis.setFormType(EquipTradeType.CORRECTED); // sua chua
				qHis.setStockName(equip.getStockName());
				qHis.setTableName(TableName.EQUIP_REPAIR_FORM.getValue());
				equipMngDAO.createEquipHistory(qHis);
			}
			/** Trang thai Sua chua: huy; Cho duyet */
			if (status == VALUE_UPDATE_2 && (StatusRecordsEquip.CANCELLATION.equals(equipRepairForm.getStatus()) 
					|| StatusRecordsEquip.WAITING_APPROVAL.equals(equipRepairForm.getStatus()))
					|| StatusRecordsEquip.COMPLETION_REPAIRS.equals(equipRepairForm.getStatus())) {
				if (StatusRecordsEquip.CANCELLATION.equals(equipRepairForm.getStatus())) {
					/** du thao -> huy; */ /** vuongmq; 19/06/2015 va co cho duyet -> huy */
					equip.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());//ko giao dich(binh thuong)
					equip.setTradeType(null);
				} else if (StatusRecordsEquip.WAITING_APPROVAL.equals(equipRepairForm.getStatus())) {
					/** Khong duyet -> Cho duyet; */
					equip.setTradeStatus(EquipTradeStatus.TRADING.getValue());//dang giao dich
					equip.setTradeType(EquipTradeType.CORRECTED); // sua chua
				} else if (StatusRecordsEquip.COMPLETION_REPAIRS.equals(equipRepairForm.getStatus())) {
					/** sua chua -> Hoan tat; */
					equip.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());//ko giao dich(binh thuong)
					equip.setTradeType(null);
					equipRepairForm.setCompleteDate(date);
				} 
				equipMngDAO.updateEquipment(equip);
				/** ghi lai lich su thiet bi moi */
				EquipHistory qHis = new EquipHistory();
				qHis.setCreateDate(date);
				qHis.setCreateUser(username);
				qHis.setEquip(equip);
				qHis.setHealthStatus(equip.getHealthStatus());
				qHis.setObjectId(equip.getStockId());
				qHis.setObjectType(equip.getStockType());
				qHis.setUsageStatus(equip.getUsageStatus());
				qHis.setStatus(equip.getStatus().getValue());
				qHis.setTradeType(equip.getTradeType());
				qHis.setTradeStatus(equip.getTradeStatus());
				
				/*** vuongmq; 26/05/2015; cap nhat them cac truong equipment_history*/
				qHis.setObjectCode(equip.getStockCode());
				qHis.setFormId(equipRepairForm.getId());
				qHis.setFormType(EquipTradeType.CORRECTED); // sua chua
				qHis.setStockName(equip.getStockName());
				qHis.setTableName(TableName.EQUIP_REPAIR_FORM.getValue());
				equipMngDAO.createEquipHistory(qHis);
			}

			// ghi lich su bien ban
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setRecordStatus(equipRepairForm.getStatus().getValue());
			equipFormHistory.setRecordId(equipRepairForm.getId());
			equipFormHistory.setRecordType(EquipTradeType.CORRECTED.getValue());// sua chua
			equipFormHistory.setActDate(date);
			equipFormHistory.setCreateDate(date);
			equipFormHistory.setCreateUser(username);
			/**
			 * Cap nhat giam sat
			 * 
			 * @author hunglm16
			 * @since August 19,2015
			 * */
			if (equipRepairForm != null && equipRepairForm.getEquip() != null) {
				if (equipRepairForm.getEquip().getStockType() != null && equipRepairForm.getEquip().getStockId() != null && EquipStockTotalType.KHO_KH.getValue().equals(equipRepairForm.getEquip().getStockType().getValue())) {
					Staff gsgn = equipmentRecordDeliveryDAO.getStaffInEquipDelivRecordApproved(equipRepairForm.getEquip().getId(), equipRepairForm.getEquip().getStockId());
					if (gsgn != null) {
						equipRepairForm.setStaff(gsgn);
					}
				}
			}
			equipRepairForm.setUpdateDate(date);
			equipMngDAO.updateEquipRepairForm(equipRepairForm); /** cap nhat lai phieu sua chua*/

			if (filter != null) {
				if (filter.getLstEquipAttachFileId() != null && !filter.getLstEquipAttachFileId().isEmpty()) {
					//Xu ly xoa file
					BasicFilter<EquipAttachFile> filterB = new BasicFilter<EquipAttachFile>();
					filterB.setLstId(filter.getLstEquipAttachFileId());
					filterB.setObjectId(equipRepairForm.getId());
					filterB.setObjectType(EquipTradeType.CORRECTED.getValue());
					List<EquipAttachFile> lstFile = equipRecordDAO.searchEquipAttachFileByFilter(filterB);
					if (lstFile != null && !lstFile.isEmpty()) {
						for (EquipAttachFile fileVItem : lstFile) {
							equipRecordDAO.deletEquipAttachFile(fileVItem);
						}
					}
				}
				//Xu ly them hinh anh
				if (filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
					for (FileVO voFile : filter.getLstFileVo()) {
						EquipAttachFile equipAttFile = new EquipAttachFile();
						equipAttFile.setUrl(voFile.getUrl());
						equipAttFile.setObjectId(equipRepairForm.getId());
						equipAttFile.setFileName(voFile.getFileName());
						equipAttFile.setThumbUrl(voFile.getThumbUrl());
						equipAttFile.setObjectType(EquipTradeType.CORRECTED.getValue());
						equipAttFile.setCreateDate(filter.getChangeDate());
						equipAttFile.setCreateUser(filter.getUserName());
						equipRecordDAO.createEquipAttachFile(equipAttFile);
					}
				}
			}
			equipMngDAO.createEquipmentHistoryRecord(equipFormHistory); /** ghi lich su lai phieu sua chua*/
			if (lstEquipRepairFormDtl != null && lstEquipRepairFormDtl.size() > 0) {
				//tim cac dong chi tiet phieu sua chua
				Map<Long, Date> mapCreateDetail = new HashMap<Long, Date>(); // Mong muon; neu cap nhat co detailId Item hang muc trung voi Id Item hang muc delete thi giu lai ngay tao
				List<EquipRepairFormDtl> lstDetail = equipMngDAO.getEquipRepairFormDtlByEquipRepairFormId(equipRepairForm.getId());
				for (int j = 0, sz = lstDetail.size();  j < sz; j++) {
					EquipRepairFormDtl itemDetail = lstDetail.get(j);
					if (itemDetail.getEquipItem() != null && itemDetail.getEquipItem().getId() != null) {
						mapCreateDetail.put(itemDetail.getEquipItem().getId(), itemDetail.getCreateDate()); //Id CuaItem Hang muc; value: ngay tao cua ItemDetail
					}
					equipMngDAO.deleteEquipRepairFormDtl(itemDetail);
				}
				// luu chi tiet bien ban
				for (int i = 0, sz = lstEquipRepairFormDtl.size(); i < sz; i++) {
					EquipRepairFormDtl detail = lstEquipRepairFormDtl.get(i);
					detail.setCreateDate(date);
					detail.setUpdateDate(date);
					detail.setEquipRepairForm(equipRepairForm);
					if (mapCreateDetail != null && !mapCreateDetail.isEmpty()) {
						// Mong muon; neu cap nhat co detailId Item hang muc trung voi Id Item hang muc delete thi giu lai ngay tao
						if (detail.getEquipItem() != null && detail.getEquipItem().getId() != null 
							&& mapCreateDetail.containsKey(detail.getEquipItem().getId())) {
							detail.setCreateDate(mapCreateDetail.get(detail.getEquipItem().getId()));
						}
					}
					equipMngDAO.createEquipRepairFormDtl(detail);
				}
			}

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> getListEquipSalePlan(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getListEquipSalePlan(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipSalePlan> getListEquipSalePlanByFilter(EquipmentSalePlaneFilter<EquipSalePlan> filter) throws BusinessException {
		try {
			return equipMngDAO.getListEquipSalePlanByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void changeEquipSalePlanByFilter(EquipmentSalePlaneFilter<EquipGroup> filter) throws BusinessException {
		try {
			if (filter.getAttribute() == null) {
				throw new BusinessException("EquipGroup is null");
			}
			/**
			 * Xu ly cac tham so cong chuoi
			 * 
			 * mapFromMonthStr: Tu thang
			 * mapToMonthStr: Den thang
			 * mapCustomerTypeStr: Loai khach hang
			 * mapAmountStr: Doanh so trong thoi ky
			 * */
			EquipGroup equipGroup = filter.getAttribute();
			Map<Integer, Integer> mapFromMonthStr = new HashMap<Integer, Integer>();
			Map<Integer, Integer> mapToMonthStr = new HashMap<Integer, Integer>();
			Map<Integer, Long> mapCustomerTypeStr = new HashMap<Integer, Long>();
			Map<Integer, Double> mapAmountStr = new HashMap<Integer, Double>();
			//Lay cac tham so Tu ngay
			if (!StringUtility.isNullOrEmpty(filter.getArrFromMonthStr())) {
				String[] arrStrFromMonth = filter.getArrFromMonthStr().split(",");
				for (String value : arrStrFromMonth) {
					String[] arrValue = value.split("_");
					if (arrValue.length > 1 && StringUtility.isNumber(arrValue[0]) && StringUtility.isNumber(arrValue[1])) {
						mapFromMonthStr.put(Integer.valueOf(arrValue[0]), Integer.valueOf(arrValue[1]));
					}
				}
			}
			//Lay cac tham so Den ngay
			if (!StringUtility.isNullOrEmpty(filter.getArrToMonthStr())) {
				String[] arrStrToMonth = filter.getArrToMonthStr().split(",");
				for (String value : arrStrToMonth) {
					String[] arrValue = value.split("_");
					if (arrValue.length > 1 && StringUtility.isNumber(arrValue[0]) && StringUtility.isNumber(arrValue[1])) {
						mapToMonthStr.put(Integer.valueOf(arrValue[0]), Integer.valueOf(arrValue[1]));
					}
				}
			}
			//Lay cac tham so Loai khach hang
			if (!StringUtility.isNullOrEmpty(filter.getArrCustomerTypeStr())) {
				String[] arrStrCustomerType = filter.getArrCustomerTypeStr().split(",");
				for (String value : arrStrCustomerType) {
					String[] arrValue = value.split("_");
					if (arrValue.length > 1 && StringUtility.isNumber(arrValue[0]) && StringUtility.isNumber(arrValue[1])) {
						mapCustomerTypeStr.put(Integer.valueOf(arrValue[0]), Long.valueOf(arrValue[1]));
					}
				}
			}
			//Lay cac tham so Doanh so
			if (!StringUtility.isNullOrEmpty(filter.getArrAmountStr())) {
				String[] arrStrAmount = filter.getArrAmountStr().split(",");
				for (String value : arrStrAmount) {
					String[] arrValue = value.split("_");
					if (arrValue.length > 1 && StringUtility.isNumber(arrValue[0]) && StringUtility.isNumber(arrValue[1])) {
						mapAmountStr.put(Integer.valueOf(arrValue[0]), Double.valueOf(arrValue[1]));
					}
				}
			}
			//Ngay hien tai cua he thong
			Date sysDateU = commonDAO.getSysDate();
			
			if (filter.getEquipGroupId() != null && filter.getEquipGroupId() > 0) {
				/** Xu ly cap nhat nhom thiet bi */
				equipGroup.setUpdateDate(sysDateU);
				equipMngDAO.updateEquipGroup(equipGroup, filter.getLogInfoVO());
				//Xu ly xoa toan bo detail
				List<EquipSalePlan> lstDatail = equipMngDAO.getListEquipSalePlanByEquipGroup(filter.getEquipGroupId());
				for (EquipSalePlan delItem:lstDatail) {
					equipMngDAO.deleteEquipmentSalePlan(delItem);
				}
			} else {
				/** Xu ly them moi nhom thiet bi */
				equipGroup.setCreateDate(sysDateU);
				equipGroup.setCreateUser(filter.getLogInfoVO().getStaffCode());
				equipGroup = equipMngDAO.createEquipmentGroup(equipGroup);
			}
			if (mapFromMonthStr != null && mapFromMonthStr.size() > 0) {
				for (Entry<Integer, Integer> entry : mapFromMonthStr.entrySet()) {
					Integer keyMap = entry.getKey();
					Integer fromMonth = entry.getValue();
					Integer toMonth = mapToMonthStr.get(keyMap);
					Long customerTypeId = mapCustomerTypeStr.get(keyMap);
					BigDecimal amount = new BigDecimal(mapAmountStr.get(keyMap));
					//Them Equip Sale Plan
					EquipSalePlan equipSalePlan = new EquipSalePlan();
					equipSalePlan.setEquipGroup(equipGroup);
					if (toMonth == null || toMonth > 0) {
						equipSalePlan.setToMonth(toMonth);
					}
					equipSalePlan.setFromMonth(fromMonth);
					if (customerTypeId > 0) {
						equipSalePlan.setCustomerTypeId(customerTypeId);
					}
					equipSalePlan.setAmount(amount);
					equipSalePlan.setCreateUser(filter.getUserName());
					equipSalePlan.setCreateDate(sysDateU);
					equipMngDAO.createEquipmentSalePlane(equipSalePlan);
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipItemVO> getListEquipItem(EquipItemFilter filter) throws BusinessException {
		try {
			List<EquipItemVO> lst = equipMngDAO.getListEquipItem(filter);
			ObjectVO<EquipItemVO> objVO = new ObjectVO<EquipItemVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/***
	 * @author vuongmq
	 * @date 06/04/2015
	 * lay danh sach hang muc popup lap phieu yeu cau sua chua
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public ObjectVO<EquipItemVO> getListEquipItemPopUpRepair(EquipItemFilter filter) throws BusinessException {
		try {
			List<EquipItemVO> lst = equipMngDAO.getListEquipItemPopUpRepair(filter);
			ObjectVO<EquipItemVO> objVO = new ObjectVO<EquipItemVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<EquipmentVO> searchEquipByEquipDeliveryRecordNew(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.searchEquipByEquipDeliveryRecordNew(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createEquipSalePlanByFilter(EquipmentSalePlaneFilter<EquipmentVO> create) throws BusinessException {
		try {
			if (create.getLstInsert() != null && !create.getLstInsert().isEmpty()) {
				List<EquipSalePlan> getLstInsert = create.getLstInsert();
				int size = getLstInsert.size();
				for (int i = 0; i < size; i++) {
					EquipSalePlan eqs = getLstInsert.get(i);
					eqs.setEquipGroup(getLstInsert.get(i).getEquipGroup());
					eqs.setFromMonth(getLstInsert.get(i).getFromMonth());
					eqs.setToMonth(getLstInsert.get(i).getToMonth());
					eqs.setCustomerTypeId(getLstInsert.get(i).getCustomerTypeId());
					eqs.setAmount(getLstInsert.get(i).getAmount());
					equipMngDAO.createEquipmentSalePlane(eqs);
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}

	}

	@Override
	public EquipStockTransForm getEquipStockTransForm(Long id) throws BusinessException {
		try {
			return equipMngDAO.getEquipStockTransForm(id);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Shop getListEquipStockTransForm(Long fromStockId) throws BusinessException {
		try {
			return equipMngDAO.getListEquipStockTransForm(fromStockId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Shop getListToStockTransForm(Long toStockId) throws BusinessException {
		try {
			return equipMngDAO.getListToStockTransForm(toStockId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipStockTransForm createStockTransFormByImportExcel(EquipStockTransForm equipStockTransForm, List<EquipStockTransFormDtl> equipmentStockTransFormDtl, EquipRecordFilter<EquipLostRecord> filter, LogInfoVO logInfo) throws BusinessException {
		try {
			// luu bien ban
			Date sysDateIns = commonDAO.getSysDate();
			String code = equipMngDAO.getCodeStockTransForm();
			equipStockTransForm.setCode(code);
			equipStockTransForm = equipMngDAO.createEquipmentStockTransForm(equipStockTransForm);
			//Xu ly luu update file
			if (filter != null) {
				filter.setChangeDate(sysDateIns);
				filter.setUserName(equipStockTransForm.getCreateUser());
				if (filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
					for (FileVO voFile : filter.getLstFileVo()) {
						EquipAttachFile equipAttFile = new EquipAttachFile();
						equipAttFile.setUrl(voFile.getUrl());
						equipAttFile.setObjectId(equipStockTransForm.getId());
						equipAttFile.setFileName(voFile.getFileName());
						equipAttFile.setThumbUrl(voFile.getThumbUrl());
						equipAttFile.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
						equipAttFile.setCreateDate(sysDateIns);
						equipAttFile.setCreateUser(equipStockTransForm.getCreateUser());
						equipRecordDAO.createEquipAttachFile(equipAttFile);
					}
				}
			}
			// luu lich su bien ban
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(sysDateIns);
			equipFormHistory.setCreateDate(sysDateIns);
			equipFormHistory.setRecordType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
			equipFormHistory.setCreateUser(equipStockTransForm.getCreateUser());
			equipFormHistory.setRecordStatus(equipStockTransForm.getStatus());
			equipFormHistory.setRecordId(equipStockTransForm.getId());
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);

			// luu chi tiet bien ban
			for (int i = 0; i < equipmentStockTransFormDtl.size(); i++) {

				EquipStockTransFormDtl ed = equipmentStockTransFormDtl.get(i);
				Equipment eq = ed.getEquip();
				if (eq == null) {
					continue;
				}

				// luu thay doi thiet bi trong chi tiet bien ban
				if (equipStockTransForm.getStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
					//		Shop shop = shopDAO.getShopById(equipStockTransForm.getToStockId());
					//		eq.setStockCode(shop.getShopCode());
					eq.setTradeType(EquipTradeType.WAREHOUSE_TRANSFER);
					eq.setTradeStatus(EquipTradeStatus.TRADING.getValue());
					eq.setUpdateDate(sysDateIns);
					eq.setUpdateUser(equipStockTransForm.getCreateUser());
					equipMngDAO.updateEquipment(eq);
					// luu lich su thiet bi
					EquipHistory equipHistory = new EquipHistory();
					equipHistory.setCreateDate(sysDateIns);
					equipHistory.setCreateUser(equipStockTransForm.getCreateUser());
					equipHistory.setEquip(eq);
					if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
						//equipHistory.setEquipWarehouseCode(eq.getStockCode());
					}
					equipHistory.setHealthStatus(eq.getHealthStatus());
					if (eq.getStockId() != null) {
						equipHistory.setObjectId(eq.getStockId());
					}
					if (eq.getStockType() != null) {
						equipHistory.setObjectType(eq.getStockType());
					}
					equipHistory.setStatus(eq.getStatus().getValue());
					equipHistory.setTradeStatus(eq.getTradeStatus());
					equipHistory.setTradeType(eq.getTradeType());
					equipHistory.setUsageStatus(eq.getUsageStatus());
					equipHistory.setObjectCode(eq.getStockCode());
					equipHistory.setFormId(equipStockTransForm.getId());
					equipHistory.setFormType(EquipTradeType.WAREHOUSE_TRANSFER);
					equipHistory.setStockName(eq.getStockName());
					equipHistory.setTableName("EQUIP_STOCK_TRANS_FORM");
					equipMngDAO.createEquipHistory(equipHistory);

				} else if (equipStockTransForm.getStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
					equipStockTransForm.setApproveDate(sysDateIns);
					equipStockTransForm = equipMngDAO.createEquipmentStockTransForm(equipStockTransForm);
					if (PerFormStatus.SUCCESS.getValue().equals(equipStockTransForm.getPerformStatus().getValue())) {
						ed.setCompleteDate(sysDateIns);
						EquipStock equipStock = equipMngDAO.getEquipStockById(ed.getToStockId());
						if (equipStock != null) {
							eq.setStockCode(equipStock.getCode());
							eq.setStockName(equipStock.getName());
							eq.setStockType(EquipStockTotalType.KHO);
							eq.setStockId(equipStock.getId());
						}
						eq.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);
						eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						eq.setTradeType(null);
						/*
						 * if (eq.getFirstDateInUse() == null) {
						 * eq.setFirstDateInUse(sysDateIns); }
						 */
						eq.setUpdateDate(sysDateIns);
						eq.setUpdateUser(equipStockTransForm.getCreateUser());
						equipMngDAO.updateEquipment(eq);

						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(equipStockTransForm.getCreateUser());
						equipHistory.setEquip(eq);
						//equipHistory.setEquipWarehouseCode(eq.getStockCode());
						equipHistory.setHealthStatus(eq.getHealthStatus());
						equipHistory.setObjectId(eq.getStockId());
						equipHistory.setObjectType(eq.getStockType());
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(null);
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setFormId(equipStockTransForm.getId());
						equipHistory.setFormType(EquipTradeType.WAREHOUSE_TRANSFER);
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName("EQUIP_STOCK_TRANS_FORM");
						equipMngDAO.createEquipHistory(equipHistory);
						//tru kho
						EquipStockTotal khoDich = equipmentStockTotalDAO.getEquipStockTotalById(ed.getToStockId(), EquipStockTotalType.KHO, eq.getEquipGroup().getId());
						EquipStockTotal khoNguon = equipmentStockTotalDAO.getEquipStockTotalById(ed.getFromStockId(), EquipStockTotalType.KHO, eq.getEquipGroup().getId());
						if (khoNguon != null) {
							khoNguon.setQuantity(khoNguon.getQuantity() - 1);
							equipmentStockTotalDAO.updateEquipmentStockTotal(khoNguon);

						}
						if (khoDich != null) {
							khoDich.setQuantity(khoDich.getQuantity() + 1);
							equipmentStockTotalDAO.updateEquipmentStockTotal(khoDich);
						} else {
							// tao moi
							khoDich = new EquipStockTotal();
							khoDich.setCreateDate(sysDateIns);
							khoDich.setCreateUser(equipStockTransForm.getCreateUser());
							khoDich.setEquipGroup(eq.getEquipGroup());
							khoDich.setQuantity(1);
							khoDich.setStockId(ed.getToStockId());
							khoDich.setStockType(EquipStockTotalType.KHO);
							khoDich = equipmentStockTotalDAO.createEquipStockTotal(khoDich);
						}
					} else if (PerFormStatus.ONGOING.getValue().equals(equipStockTransForm.getPerformStatus().getValue())) {
						eq.setTradeType(EquipTradeType.WAREHOUSE_TRANSFER);
						eq.setTradeStatus(EquipTradeStatus.TRADING.getValue());
						eq.setUpdateDate(sysDateIns);
						eq.setUpdateUser(equipStockTransForm.getCreateUser());
						equipMngDAO.updateEquipment(eq);
						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(equipStockTransForm.getCreateUser());
						equipHistory.setEquip(eq);
						if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
							//equipHistory.setEquipWarehouseCode(eq.getStockCode());
						}
						equipHistory.setHealthStatus(eq.getHealthStatus());
						if (eq.getStockId() != null) {
							equipHistory.setObjectId(eq.getStockId());
						}
						if (eq.getStockType() != null) {
							equipHistory.setObjectType(eq.getStockType());
						}
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setFormId(equipStockTransForm.getId());
						equipHistory.setFormType(EquipTradeType.WAREHOUSE_TRANSFER);
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName("EQUIP_STOCK_TRANS_FORM");
						equipMngDAO.createEquipHistory(equipHistory);
					}
				}
				// luu chi tiet bien ban
				ed.setEquipStockTransForm(equipStockTransForm);
				ed.setHealthStatus(ed.getEquip().getHealthStatus());
				ed.setCreateUser(logInfo.getStaffCode());
				ed = equipMngDAO.createEquipStockTransFormDtl(ed);
			}
			return equipStockTransForm;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * cap nhat Luu phieu sua chua; saveRepair: tao moi
	 * @author vuongmq
	 * @since 14/04/2015
	 * @return
	 * create  chi cho chon trang thai du thao va cho duyet; ben ham action.saveRepair
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipRepairForm createEquipRepairForm(EquipRepairForm equipRepairForm, List<EquipRepairFormDtl> lstEquipRepairFormDtl, EquipRecordFilter<EquipRepairForm> filter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			Date date = commonDAO.getSysDate();
			equipRepairForm.setCreateDate(date);
			String username = equipRepairForm.getCreateUser();
			if (filter != null) {
				filter.setChangeDate(date);
				filter.setUserName(username);
			}
			if (equipRepairForm != null && equipRepairForm.getEquip() != null) {
				if (equipRepairForm.getEquip().getStockType() != null && equipRepairForm.getEquip().getStockId() != null && EquipStockTotalType.KHO_KH.getValue().equals(equipRepairForm.getEquip().getStockType().getValue())) {
					Staff gsgn = equipmentRecordDeliveryDAO.getStaffInEquipDelivRecordApproved(equipRepairForm.getEquip().getId(), equipRepairForm.getEquip().getStockId());
					if (gsgn != null) {
						equipRepairForm.setStaff(gsgn);
					}
				}
			}
			equipRepairForm = equipMngDAO.createEquipRepairForm(equipRepairForm);
			String id = "";
			id = "00000000" + equipRepairForm.getId().toString();
			id = id.substring(id.length() - 8);
			String code = "SC" + id;
			equipRepairForm.setEquipRepairFormCode(code);
			if (equipRepairForm != null && equipRepairForm.getEquip() != null) {
				if (equipRepairForm.getEquip().getStockType() != null && equipRepairForm.getEquip().getStockId() != null && EquipStockTotalType.KHO_KH.getValue().equals(equipRepairForm.getEquip().getStockType().getValue())) {
					Staff gsgn = equipmentRecordDeliveryDAO.getStaffInEquipDelivRecordApproved(equipRepairForm.getEquip().getId(), equipRepairForm.getEquip().getStockId());
					if (gsgn != null) {
						equipRepairForm.setStaff(gsgn);
					}
				}
			}
			equipMngDAO.updateEquipRepairForm(equipRepairForm);

			if (filter != null && filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
				//Xu ly them hinh anh
				for (FileVO voFile : filter.getLstFileVo()) {
					EquipAttachFile equipAttFile = new EquipAttachFile();
					equipAttFile.setUrl(voFile.getUrl());
					equipAttFile.setObjectId(equipRepairForm.getId());
					equipAttFile.setFileName(voFile.getFileName());
					equipAttFile.setThumbUrl(voFile.getThumbUrl());
					equipAttFile.setObjectType(EquipTradeType.CORRECTED.getValue());
					equipAttFile.setCreateDate(filter.getChangeDate());
					equipAttFile.setCreateUser(filter.getUserName());
					equipRecordDAO.createEquipAttachFile(equipAttFile);
				}
			}
			// luu lich su bien ban equip_form_history
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(date);
			equipFormHistory.setCreateDate(date);
			equipFormHistory.setCreateUser(username);
			equipFormHistory.setRecordId(equipRepairForm.getId());
			equipFormHistory.setRecordStatus(equipRepairForm.getStatus().getValue());
			equipFormHistory.setRecordType(EquipTradeType.CORRECTED.getValue());
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);

			//cap nhat thiet bi
			Equipment equipment = equipRepairForm.getEquip();
			equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
			equipment.setTradeType(EquipTradeType.CORRECTED);
			equipment.setUpdateDate(date);
			equipment.setUpdateUser(username);
			equipMngDAO.updateEquipment(equipment);

			// luu lich su thiet bi equip_history
			EquipHistory qHis = new EquipHistory();
			qHis.setCreateDate(date);
			qHis.setCreateUser(username);
			qHis.setEquip(equipment);
			qHis.setHealthStatus(equipment.getHealthStatus());
			qHis.setObjectId(equipment.getStockId());
			qHis.setObjectType(equipment.getStockType());
			qHis.setUsageStatus(equipment.getUsageStatus());
			qHis.setStatus(equipment.getStatus().getValue());
			qHis.setTradeType(equipment.getTradeType());
			qHis.setTradeStatus(equipment.getTradeStatus());
			
			/*** vuongmq; 26/05/2015; cap nhat them cac truong equipment_history*/
			qHis.setObjectCode(equipment.getStockCode());
			qHis.setFormId(equipRepairForm.getId());
			qHis.setFormType(EquipTradeType.CORRECTED); // sua chua
			qHis.setStockName(equipment.getStockName());
			qHis.setTableName(TableName.EQUIP_REPAIR_FORM.getValue());
			equipMngDAO.createEquipHistory(qHis);

			// luu chi tiet bien ban
			for (int i = 0, sz = lstEquipRepairFormDtl.size(); i < sz ; i++) {
				EquipRepairFormDtl detail = lstEquipRepairFormDtl.get(i);
				detail.setEquipRepairForm(equipRepairForm);
				detail.setCreateDate(date);
				equipMngDAO.createEquipRepairFormDtl(detail);
			}

			return equipRepairForm;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipRepairForm createEquipRepairFormByExcel(EquipRepairForm equipRepairForm, List<EquipRepairFormDtl> lstEquipRepairFormDtl) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			Date date = commonDAO.getSysDate();
			equipRepairForm.setCreateDate(date);

			String username = equipRepairForm.getCreateUser();
			BigDecimal tongTien = BigDecimal.ZERO;
			BigDecimal tongTienChiTiet = BigDecimal.ZERO;
			BigDecimal maxWorkPrice = BigDecimal.ZERO;
			// tinh tien
			for (int i = 0, sz = lstEquipRepairFormDtl.size(); i < sz; i++) {
				EquipRepairFormDtl detail = lstEquipRepairFormDtl.get(i);
				BigDecimal soLanSuaChuaHangMuc = BigDecimal.ONE;
				if (detail != null) {
					if (detail.getEquipItem() != null) {
						soLanSuaChuaHangMuc = equipMngDAO.getRepairCountEquipItem(detail.getEquipItem().getId(), equipRepairForm.getEquip().getId());
						/** vuongmq; 10/06/2015; cap nhat cac gia tri cho detail sua chua: "fromWorkerPrice", "toWorkerPrice", "fromMaterialPrice","toMaterialPrice",*/
						EquipItemFilter filter2 = new EquipItemFilter();
						// vao day thi equipRepairForm luong != null
						if (equipRepairForm.getEquip() != null) {
							filter2.setEquipId(equipRepairForm.getEquip().getId());
						}
						filter2.setEpuipItemId(detail.getEquipItem().getId());
						List<EquipItemVO> lstEquipItemVOs = equipMngDAO.getListEquipItemPopUpRepair(filter2);
						if (lstEquipItemVOs != null && lstEquipItemVOs.size() > 0) {
							// don gia vat tu dinh muc
							lstEquipRepairFormDtl.get(i).setFromMaterialPrice(lstEquipItemVOs.get(0).getFromMaterialPrice());
							lstEquipRepairFormDtl.get(i).setToMaterialPrice(lstEquipItemVOs.get(0).getToMaterialPrice());
							// don gia nhan dinh muc
							lstEquipRepairFormDtl.get(i).setFromWorkerPrice(lstEquipItemVOs.get(0).getFromWorkerPrice());
							lstEquipRepairFormDtl.get(i).setToWorkerPrice(lstEquipItemVOs.get(0).getToWorkerPrice());
						}
					}
					lstEquipRepairFormDtl.get(i).setRepairCount(soLanSuaChuaHangMuc.intValue());
					tongTienChiTiet = BigDecimal.ZERO;
					/** vuongmq; 13/06/2015; tong tien chi tiet se bang = so luong ct * don gia vat tu ct; ko nhan voi gia nhan cong nua */ 
					if (detail.getQuantity() != null && detail.getMaterialPrice() != null) {
						//tongTienChiTiet = detail.getQuantity() * detail.getMaterialPrice();
						BigDecimal sl = BigDecimal.valueOf(detail.getQuantity());
						tongTienChiTiet = sl.multiply(detail.getMaterialPrice());
					}
					if (detail.getWorkerPrice() != null) {
						//tongTienChiTiet += detail.getWorkerPrice();
						//tongTienChiTiet = tongTienChiTiet.add(detail.getWorkerPrice());
						if (detail.getWorkerPrice().compareTo(maxWorkPrice) == 1) {
							maxWorkPrice = detail.getWorkerPrice();
						}
					}
					lstEquipRepairFormDtl.get(i).setTotalAmount(tongTienChiTiet);
					//tongTien += tongTienChiTiet;
					tongTien = tongTien.add(tongTienChiTiet);
				}
			}
			equipRepairForm.setWorkerPrice(maxWorkPrice);
			/** vuongmq; 13/06/2015; tongtien chinh = tongtien ct + maxWorkPrice*/
			tongTien = tongTien.add(maxWorkPrice);
			equipRepairForm.setTotalAmount(tongTien);
			BigDecimal soLanSuaChuaThietbi = BigDecimal.ONE;
			if (equipRepairForm != null && equipRepairForm.getEquip() != null) {
				soLanSuaChuaThietbi = equipMngDAO.getRepairCountEquip(equipRepairForm.getEquip().getId());
			}
			equipRepairForm.setRepairCount(soLanSuaChuaThietbi.intValue());
			equipRepairForm = equipMngDAO.createEquipRepairForm(equipRepairForm);
			String id = "";
			id = "00000000" + equipRepairForm.getId().toString();
			id = id.substring(id.length() - 8);
			String code = "SC" + id;
			equipRepairForm.setEquipRepairFormCode(code);
			if (equipRepairForm != null && equipRepairForm.getEquip() != null) {
				if (equipRepairForm.getEquip().getStockType() != null && equipRepairForm.getEquip().getStockId() != null && EquipStockTotalType.KHO_KH.getValue().equals(equipRepairForm.getEquip().getStockType().getValue())) {
					Staff gsgn = equipmentRecordDeliveryDAO.getStaffInEquipDelivRecordApproved(equipRepairForm.getEquip().getId(), equipRepairForm.getEquip().getStockId());
					if (gsgn != null) {
						equipRepairForm.setStaff(gsgn);
					}
				}
			}
			equipMngDAO.updateEquipRepairForm(equipRepairForm);

			// luu lich su bien ban
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(date);
			equipFormHistory.setCreateDate(date);
			equipFormHistory.setCreateUser(username);
			equipFormHistory.setRecordId(equipRepairForm.getId());
			equipFormHistory.setRecordStatus(equipRepairForm.getStatus().getValue());
			equipFormHistory.setRecordType(EquipTradeType.CORRECTED.getValue());
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);

			//cap nhat thiet bi
			Equipment equipment = equipRepairForm.getEquip();
			equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
			equipment.setTradeType(EquipTradeType.CORRECTED);
			equipment.setUpdateDate(date);
			equipment.setUpdateUser(username);
			equipMngDAO.updateEquipment(equipment);

			// luu lich su thiet bi
			EquipHistory qHis = new EquipHistory();
			qHis.setCreateDate(date);
			qHis.setCreateUser(username);
			qHis.setEquip(equipment);
			qHis.setHealthStatus(equipment.getHealthStatus());
			qHis.setObjectId(equipment.getStockId());
			qHis.setObjectType(equipment.getStockType());
			qHis.setUsageStatus(equipment.getUsageStatus());
			qHis.setStatus(equipment.getStatus().getValue());
			qHis.setTradeType(equipment.getTradeType());
			qHis.setTradeStatus(equipment.getTradeStatus());
			
			/*** vuongmq; 26/05/2015; cap nhat them cac truong equipment_history */
			qHis.setObjectCode(equipment.getStockCode());
			qHis.setFormId(equipRepairForm.getId());
			qHis.setFormType(EquipTradeType.CORRECTED); // sua chua
			qHis.setStockName(equipment.getStockName());
			qHis.setTableName(TableName.EQUIP_REPAIR_FORM.getValue());
			equipMngDAO.createEquipHistory(qHis);

			// luu chi tiet bien ban
			for (int i = 0, size = lstEquipRepairFormDtl.size(); i < size; i++) {
				EquipRepairFormDtl detail = lstEquipRepairFormDtl.get(i);
				detail.setCreateDate(date);
				detail.setEquipRepairForm(equipRepairForm);
				equipMngDAO.createEquipRepairFormDtl(detail);
			}
			return equipRepairForm;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentGroupVO> searchEquipmentGroupVO(EquipmentGroupProductFilter<EquipmentGroupVO> equipmentGroupFilter) throws BusinessException {
		try {
			List<EquipmentGroupVO> equipmentGroupVOs = equipMngDAO.searchEquipmentGroupVO(equipmentGroupFilter);
			ObjectVO<EquipmentGroupVO> objectVO = new ObjectVO<EquipmentGroupVO>();
			objectVO.setkPaging(equipmentGroupFilter.getkPaging());
			objectVO.setLstObject(equipmentGroupVOs);
			return objectVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipmentGroupVO> getListEquipmentGroupVOForExport(EquipmentGroupProductFilter<EquipmentGroupVO> equipmentGroupFilter) throws BusinessException {
		try {
			List<EquipmentGroupVO> equipmentGroupVOs = equipMngDAO.getListEquipmentGroupVOForExport(equipmentGroupFilter);
			return equipmentGroupVOs;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*@Override
	@Transactional(rollbackFor = Exception.class)
	public void createEquipGroupByExcel(EquipmentVO vo) throws BusinessException {
		try {
			EquipGroup equipGroup = new EquipGroup();
			if (vo == null) {
				throw new BusinessException("EquipmentVO ");
			}
			// xu ly luu lai Nhom thiet bi
			equipGroup.setCreateDate(vo.getCreateDate());
			equipGroup.setCreateUser(vo.getCreateUser());
			equipGroup.setCode(vo.getCode());
			equipGroup.setName(vo.getName());
			equipGroup.setNameText(Unicode2English.codau2khongdau(vo.getName().trim().toUpperCase()));
			// nho cap nhat loai
			EquipCategory category = equipMngDAO.getEquipCategoryByCode(vo.getTypeGroup(), ActiveType.RUNNING);
			if (category != null) {
				equipGroup.setEquipCategory(category);
			}
			if (vo.getCapacityFrom() != null) {
				equipGroup.setCapacityFrom(new BigDecimal(vo.getCapacityFrom()));
			}
			if (vo.getCapacityTo() != null) {
				equipGroup.setCapacityTo(new BigDecimal(vo.getCapacityTo()));
			}
			equipGroup.setEquipBrandName(vo.getBrandName());
			equipGroup.setStatus(ActiveType.parseValue(vo.getStatus()));
			equipGroup = equipMngDAO.createEquipment(equipGroup);
			// luu chi tiet equip sale plan voi du lieu co muc doanh so
			List<EquipSalePlan> lstSP = vo.getLstEquipSalePlan();
			if (lstSP != null && lstSP.size() > 0) {
				for (EquipSalePlan sp : lstSP) {
					sp.setEquipGroup(equipGroup);
					sp.setCreateDate(vo.getCreateDate());
					sp.setCreateUser(vo.getCreateUser());
					equipMngDAO.createEquipSalePlan(sp);
				}
			}

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}*/
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createEquipGroupByExcel(EquipmentGroupVO vo) throws BusinessException {
		try {
			if (vo == null || StringUtility.isNullOrEmpty(vo.getTypeGroup()) || StringUtility.isNullOrEmpty(vo.getCode())) {
				throw new BusinessException("EquipmentGroupVO is null ");
			}
			String codeGroup = vo.getTypeGroup() + vo.getCode();
			// kiem tra ma nhom co ton tai trong he thong\
			EquipGroup equipGroup = equipMngDAO.getEquipGroupByCode(codeGroup);
			if (equipGroup != null && equipGroup.getId() > 0 ) {
				/** vuongmq; 22/07/2015; Khi cap nhat thi vao day */
				// Xu ly cap nhat Nhom TB; update lai equipGroup; luc nay vo.getCreateUser(), vo.getCreateDate() la luu cot update
				equipGroup.setUpdateUser(vo.getCreateUser());
				equipGroup.setUpdateDate(vo.getCreateDate());
				equipGroup.setName(vo.getName());
				equipGroup.setNameText(Unicode2English.codau2khongdau(vo.getName().trim().toUpperCase()));
				// nho cap nhat loai
				EquipCategory category = equipMngDAO.getEquipCategoryByCode(vo.getTypeGroup(), ActiveType.RUNNING);
				if (category != null) {
					equipGroup.setEquipCategory(category);
				}
				if (!StringUtility.isNullOrEmpty(vo.getCapacityFrom())) {
					equipGroup.setCapacityFrom(new BigDecimal(vo.getCapacityFrom()));
				}
				if (!StringUtility.isNullOrEmpty(vo.getCapacityTo())) {
					equipGroup.setCapacityTo(new BigDecimal(vo.getCapacityTo()));
				}
				equipGroup.setEquipBrandName(vo.getBrandName());
				equipGroup.setStatus(ActiveType.parseValue(vo.getStatus()));
				LogInfoVO logInfoVO = new LogInfoVO();
				equipGroup = equipMngDAO.updateEquipGroup(equipGroup, logInfoVO);
				// xoa het cac equip_sale_plan; tao lai nhung dong moi
				List<EquipSalePlan> lsEquipSalePlans = equipMngDAO.getListEquipSalePlanByEquipGroup(equipGroup.getId());
				for (EquipSalePlan equipSalePlan : lsEquipSalePlans) {
					commonDAO.deleteEntity(equipSalePlan);
				}
				// luu chi tiet equip sale plan voi du lieu co muc doanh so
				List<EquipSalePlanVO> lstSP = vo.getLstEquipSalePlanVO();
				if (lstSP != null && lstSP.size() > 0 && vo.getFlagCheckExistSP()) {
					for (EquipSalePlanVO spVO : lstSP) {
						EquipSalePlan sp = new EquipSalePlan(); 
						sp.setEquipGroup(equipGroup);
						sp.setCreateDate(vo.getCreateDate());
						sp.setCreateUser(vo.getCreateUser());
						
						if(!StringUtility.isNullOrEmpty(spVO.getFromMonth())){
							sp.setFromMonth(Integer.valueOf(spVO.getFromMonth()));
						}
						if(!StringUtility.isNullOrEmpty(spVO.getToMonth())){
							sp.setToMonth(Integer.valueOf(spVO.getToMonth()));
						}
						if(spVO.getCustomerTypeId()!=null){
							sp.setCustomerTypeId(spVO.getCustomerTypeId());
						}
						if(!StringUtility.isNullOrEmpty(spVO.getAmount())){
							sp.setAmount(new BigDecimal(spVO.getAmount()));
						}
						equipMngDAO.createEquipSalePlan(sp);
					}
				}
			} else {
				/** Khi them moi thi vao day */
				equipGroup = new EquipGroup();
				// xu ly luu lai Nhom thiet bi
				equipGroup.setCreateDate(vo.getCreateDate());
				equipGroup.setCreateUser(vo.getCreateUser());
				equipGroup.setCode(vo.getTypeGroup()+vo.getCode());
				equipGroup.setName(vo.getName());
				equipGroup.setNameText(Unicode2English.codau2khongdau(vo.getName().trim().toUpperCase()));
				// nho cap nhat loai
				EquipCategory category = equipMngDAO.getEquipCategoryByCode(vo.getTypeGroup(), ActiveType.RUNNING);
				if (category != null) {
					equipGroup.setEquipCategory(category);
				}
				if (!StringUtility.isNullOrEmpty(vo.getCapacityFrom())) {
					equipGroup.setCapacityFrom(new BigDecimal(vo.getCapacityFrom()));
				}
				if (!StringUtility.isNullOrEmpty(vo.getCapacityTo())) {
					equipGroup.setCapacityTo(new BigDecimal(vo.getCapacityTo()));
				}
				equipGroup.setEquipBrandName(vo.getBrandName());
				equipGroup.setStatus(ActiveType.parseValue(vo.getStatus()));
				equipGroup = equipMngDAO.createEquipmentGroup(equipGroup);
				// luu chi tiet equip sale plan voi du lieu co muc doanh so
				List<EquipSalePlanVO> lstSP = vo.getLstEquipSalePlanVO();
				if (lstSP != null && lstSP.size() > 0 && vo.getFlagCheckExistSP()) {
					for (EquipSalePlanVO spVO : lstSP) {
						EquipSalePlan sp = new EquipSalePlan(); 
						sp.setEquipGroup(equipGroup);
						sp.setCreateDate(vo.getCreateDate());
						sp.setCreateUser(vo.getCreateUser());
						
						if(!StringUtility.isNullOrEmpty(spVO.getFromMonth())){
							sp.setFromMonth(Integer.valueOf(spVO.getFromMonth()));
						}
						if(!StringUtility.isNullOrEmpty(spVO.getToMonth())){
							sp.setToMonth(Integer.valueOf(spVO.getToMonth()));
						}
						if(spVO.getCustomerTypeId()!=null){
							sp.setCustomerTypeId(spVO.getCustomerTypeId());
						}
						if(!StringUtility.isNullOrEmpty(spVO.getAmount())){
							sp.setAmount(new BigDecimal(spVO.getAmount()));
						}
						equipMngDAO.createEquipSalePlan(sp);
					}
				}
			}

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public ObjectVO<EquipmentGroupProductVO> retrieveProductsInEquipmentGroup(String equipmentGroupCode, ActiveType equipmentGroupProductStatus, KPaging<EquipmentGroupProductVO> kPaging) throws BusinessException {
		try {
			List<EquipmentGroupProductVO> productsInEquipmentGroup = equipMngDAO.retrieveProductsInEquipmentGroup(equipmentGroupCode, equipmentGroupProductStatus, kPaging);
			ObjectVO<EquipmentGroupProductVO> objectVO = new ObjectVO<EquipmentGroupProductVO>();
			objectVO.setkPaging(kPaging);
			objectVO.setLstObject(productsInEquipmentGroup);
			return objectVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void removeProductFromEquipmentGroup(String equipmentGroupCode, List<Long> equipGroupProductIds, LogInfoVO logInfo) throws BusinessException {
		if (StringUtility.isNullOrEmpty(equipmentGroupCode) || equipGroupProductIds == null || equipGroupProductIds.isEmpty()) {
			throw new IllegalArgumentException("equipmentGroupCode and equipGroupProductIds must not be null.");
		}
		try {
			EquipGroup equipmentGroup = equipMngDAO.getEquipGroupByCode(equipmentGroupCode);
			if (equipmentGroup == null) {
				throw new DataAccessException("No equipmentGroup with equipmentGroupCode = " + equipmentGroupCode);
			}
			if (equipmentGroup.getStatus() != ActiveType.RUNNING) {
				throw new DataAccessException("equipmentGroup with equipmentGroupCode = " + equipmentGroupCode + " is not running.");
			}

			Date sysDate = commonDAO.getSysDate();
			
			EquipmentGroupProductFilter<EquipGroupProduct> filter = new EquipmentGroupProductFilter<EquipGroupProduct>();
			filter.setEquipGroupCodes(Arrays.asList(new String []{equipmentGroupCode}));
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setEquipGroupProductIds(equipGroupProductIds);
			List<EquipGroupProduct> lstData = equipMngDAO.getListEquipGroupProductByFilter(filter);
			
			for (EquipGroupProduct equipGroupProduct : lstData) {
				if (equipGroupProduct.getFromDate() == null) {
					equipGroupProduct.setFromDate(sysDate);
				}
				if (commonDAO.compareDateWithoutTime(equipGroupProduct.getFromDate(), sysDate) == 1) {
					equipGroupProduct.setStatus(ActiveType.DELETED);
					equipGroupProduct.setFromDate(sysDate);
				}
				equipGroupProduct.setToDate(sysDate);
				equipGroupProduct.setUpdateDate(sysDate);
				equipGroupProduct.setUpdateUser(logInfo.getStaffCode());
				commonDAO.updateEntity(equipGroupProduct);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void addProductToEquipmentGroup(String equipmentGroupCode, List<String> productCodes, LogInfoVO logInfo) throws BusinessException {
		if (StringUtility.isNullOrEmpty(equipmentGroupCode) || productCodes == null || productCodes.size() == 0) {
			throw new IllegalArgumentException("equipmentGroupCode and productCode must not be null.");
		}
		try {
			EquipGroup equipmentGroup = equipMngDAO.getEquipGroupByCode(equipmentGroupCode);
			if (equipmentGroup == null) {
				throw new DataAccessException("No equipmentGroup with equipmentGroupCode = " + equipmentGroupCode);
			}
			if (equipmentGroup.getStatus() != ActiveType.RUNNING) {
				throw new DataAccessException("equipmentGroup with equipmentGroupCode = " + equipmentGroupCode + " is not running.");
			}

			Date sysDate = commonDAO.getSysDate();

			for (String productCode : productCodes) {
				Product product = productDAO.getProductByCode(productCode);
				if (product == null) {
					throw new DataAccessException("No product with productCode = " + productCode);
				}
				if (product.getStatus() != ActiveType.RUNNING) {
					throw new DataAccessException("Product " + equipmentGroupCode + " is not running.");
				}

				EquipGroupProduct equipmentGroupProduct = equipMngDAO.retrieveEquipmentGroupProduct(equipmentGroupCode, productCode, ActiveType.RUNNING);
				if (equipmentGroupProduct != null) {
					throw new DataAccessException("equipmentGroupProduct exists. equipmentGroupCode=" + equipmentGroupCode + ", productCode=" + productCode);
				}

				equipmentGroupProduct = new EquipGroupProduct();
				equipmentGroupProduct.setEquipGroup(equipmentGroup);
				equipmentGroupProduct.setProduct(product);
				equipmentGroupProduct.setStatus(ActiveType.RUNNING);
				equipmentGroupProduct.setCreateUser(logInfo.getStaffCode());
				equipmentGroupProduct.setCreateDate(sysDate);
				commonDAO.createEntity(equipmentGroupProduct);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ProductTreeVO> buildProductTreeVOForEquipmentGroup(String productCode, String productName, Long equipmentGroupId, Boolean exceptProductsInZCategory) throws BusinessException {
		try {
			List<ProductTreeVO> products = new ArrayList<ProductTreeVO>(), categories = new ArrayList<ProductTreeVO>(), subCategories = new ArrayList<ProductTreeVO>();
			equipMngDAO.retrieveProductsAndCategories(productCode, productName, equipmentGroupId, exceptProductsInZCategory, products, categories, subCategories);

			for (ProductTreeVO subCategory : subCategories) {
				for (ProductTreeVO product : products) {
					if (subCategory.getId().equals(product.getParentId())) {
						product.setType(TreeVOType.PRODUCT);
						subCategory.getListChildren().add(product);
					}
				}
			}

			for (ProductTreeVO category : categories) {
				category.setType(TreeVOType.CATEGORY);
				for (ProductTreeVO subCategory : subCategories) {
					if (category.getId().equals(subCategory.getParentId())) {
						subCategory.setType(TreeVOType.SUB_CATEGORY);
						category.getListChildren().add(subCategory);
					}
				}
			}

			return categories;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipStockTransForm updateStockTransForm(EquipStockTransForm equipStockTransForm, List<EquipStockTransFormDtl> equipmentStockTransFormDtl, List<Long> addEquipmentIds, EquipRecordFilter<EquipStockTransForm> filter, LogInfoVO logInfo)
			throws BusinessException {
		try {
			Date sysDateIns = commonDAO.getSysDate();
			//xoa het cac dong detail cu
			List<EquipStockTransFormDtl> lstDetail = equipMngDAO.getListEquipStockTransFormDtlById(equipStockTransForm.getId());
			if (lstDetail != null && lstDetail.size() > 0) {
				List<Long> lstId = new ArrayList<Long>();
				boolean isDelete = true;
				for (EquipStockTransFormDtl detail : lstDetail) {
					for (int i = equipmentStockTransFormDtl.size() - 1; i >= 0; i--) {
						if (detail.getEquip() != null && equipmentStockTransFormDtl.get(i).getEquip() != null && detail.getEquip().getId().equals(equipmentStockTransFormDtl.get(i).getEquip().getId())) {
							if(equipStockTransForm.getStatus().equals(StatusRecordsEquip.APPROVED.getValue()) && PerFormStatus.SUCCESS.getValue().equals(equipStockTransForm.getPerformStatus().getValue()) ){
								Equipment eq = detail.getEquip();
								EquipStock equipStock = equipMngDAO.getEquipStockById(detail.getToStockId());
								detail.setCompleteDate(sysDateIns);
								detail.setPerformStatus(PerFormStatus.SUCCESS);
								if(equipStock!=null){
									eq.setStockCode(equipStock.getCode());
									eq.setStockName(equipStock.getName());
									eq.setStockType(EquipStockTotalType.KHO);
									eq.setStockId(equipStock.getId());
								}
								eq.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);
								eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
								eq.setTradeType(null);
								eq.setUpdateDate(sysDateIns);
								eq.setUpdateUser(equipStockTransForm.getCreateUser());
								equipMngDAO.updateEquipment(eq);
								
								// luu lich su thiet bi
								EquipHistory equipHistory = new EquipHistory();
								equipHistory.setCreateDate(sysDateIns);
								equipHistory.setCreateUser(equipStockTransForm.getCreateUser());
								equipHistory.setEquip(eq);
								if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
									//equipHistory.setEquipWarehouseCode(eq.getStockCode());
								}
								equipHistory.setHealthStatus(eq.getHealthStatus());
								if (eq.getStockId() != null) {
									equipHistory.setObjectId(eq.getStockId());
								}
								if (eq.getStockType() != null) {
									equipHistory.setObjectType(eq.getStockType());
								}
								equipHistory.setStatus(eq.getStatus().getValue());
								equipHistory.setTradeStatus(eq.getTradeStatus());
								equipHistory.setTradeType(eq.getTradeType());
								equipHistory.setUsageStatus(eq.getUsageStatus());
								equipHistory.setObjectCode(eq.getStockCode());
								equipHistory.setFormId(equipStockTransForm.getId());
								equipHistory.setFormType(EquipTradeType.WAREHOUSE_TRANSFER);
								equipHistory.setStockName(eq.getStockName());
								equipHistory.setTableName("EQUIP_STOCK_TRANS_FORM");
								equipMngDAO.createEquipHistory(equipHistory);
								//tru kho
								EquipStockTotal khoDich = equipmentStockTotalDAO.getEquipStockTotalById(detail.getToStockId(), EquipStockTotalType.KHO, eq.getEquipGroup().getId());
								EquipStockTotal khoNguon = equipmentStockTotalDAO.getEquipStockTotalById(detail.getFromStockId(), EquipStockTotalType.KHO, eq.getEquipGroup().getId());
								if (khoNguon != null) {
									khoNguon.setQuantity(khoNguon.getQuantity() - 1);
									equipmentStockTotalDAO.updateEquipmentStockTotal(khoNguon);

								}
								if (khoDich != null) {
									khoDich.setQuantity(khoDich.getQuantity() + 1);
									equipmentStockTotalDAO.updateEquipmentStockTotal(khoDich);
								} else {
									// tao moi
									khoDich = new EquipStockTotal();
									khoDich.setCreateDate(sysDateIns);
									khoDich.setCreateUser(equipStockTransForm.getCreateUser());
									khoDich.setEquipGroup(eq.getEquipGroup());
									khoDich.setQuantity(1);
									khoDich.setStockId(detail.getToStockId());
									khoDich.setStockType(EquipStockTotalType.KHO);
									khoDich = equipmentStockTotalDAO.createEquipStockTotal(khoDich);
								}
								detail.setUpdateDate(sysDateIns);
								detail.setHealthStatus(detail.getEquip().getHealthStatus());
								detail = equipMngDAO.updateEquipStockTransFormDtl(detail);
							}
							equipmentStockTransFormDtl.remove(i);
							isDelete = false;
						}
					}
					if (isDelete) {
						lstId.add(detail.getId());
					}
					isDelete = true;
				}
				if (!lstId.isEmpty()) {
					this.deleteStockTransFormDtl(lstId);
				}
			}

			// luu bien ban		
			if (equipStockTransForm.getStatus() != null) {
				if (equipStockTransForm.getStatus().equals(StatusRecordsEquip.CANCELLATION.getValue())) {
					equipStockTransForm.setRefuseDate(sysDateIns);
				} else if (equipStockTransForm.getStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
					equipStockTransForm.setApproveDate(sysDateIns);
				}
			}
			equipStockTransForm = equipMngDAO.updateEquipmentStockTransForm(equipStockTransForm);

			// luu lich su bien ban
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(sysDateIns);
			equipFormHistory.setRecordType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
			equipFormHistory.setCreateDate(sysDateIns);
			equipFormHistory.setCreateUser(equipStockTransForm.getCreateUser());
			equipFormHistory.setRecordStatus(equipStockTransForm.getStatus());
			equipFormHistory.setRecordId(equipStockTransForm.getId());
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
			//Lay gia tri kho nguon de luu lich su
			//Shop shopFrom = shopDAO.getShopById(equipStockTransForm.getFromStockId());
			//Truong hop khong thay doi gird thiet bi
			if (equipmentStockTransFormDtl != null && equipmentStockTransFormDtl.size() > 0) {
				// luu chi tiet bien ban
				for (int i = 0; i < equipmentStockTransFormDtl.size(); i++) {
					EquipStockTransFormDtl ed = equipmentStockTransFormDtl.get(i);
					Equipment eq = ed.getEquip();
					// luu thay doi thiet bi trong chi tiet bien ban
					if (equipStockTransForm.getStatus().equals(StatusRecordsEquip.CANCELLATION.getValue())) {
						if(eq.getTradeType()!=null){
							eq.setTradeType(null);
							eq.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);
							eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
							eq.setUpdateDate(sysDateIns);
							eq.setUpdateUser(equipStockTransForm.getCreateUser());
							equipMngDAO.updateEquipment(eq);
							
							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(equipStockTransForm.getCreateUser());
							equipHistory.setEquip(eq);
							if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
								//equipHistory.setEquipWarehouseCode(eq.getStockCode());
							}
							equipHistory.setHealthStatus(eq.getHealthStatus());
							if (eq.getStockId() != null) {
								equipHistory.setObjectId(eq.getStockId());
							}
							if (eq.getStockType() != null) {
								equipHistory.setObjectType(eq.getStockType());
							}
							equipHistory.setStatus(eq.getStatus().getValue());
							equipHistory.setTradeStatus(eq.getTradeStatus());
							equipHistory.setTradeType(eq.getTradeType());
							equipHistory.setUsageStatus(eq.getUsageStatus());
							equipHistory.setObjectCode(eq.getStockCode());
							equipHistory.setFormId(equipStockTransForm.getId());
							equipHistory.setFormType(EquipTradeType.WAREHOUSE_TRANSFER);
							equipHistory.setStockName(eq.getStockName());
							equipHistory.setTableName("EQUIP_STOCK_TRANS_FORM");
							equipMngDAO.createEquipHistory(equipHistory);
						}

					} else if (equipStockTransForm.getStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
						if(PerFormStatus.SUCCESS.getValue().equals(equipStockTransForm.getPerformStatus().getValue())){
							EquipStock equipStock = equipMngDAO.getEquipStockById(ed.getToStockId());
							ed.setCompleteDate(sysDateIns);
							ed.setPerformStatus(PerFormStatus.SUCCESS);
							if(equipStock!=null){
								eq.setStockCode(equipStock.getCode());
								eq.setStockName(equipStock.getName());
								eq.setStockType(EquipStockTotalType.KHO);
								eq.setStockId(equipStock.getId());
							}
							eq.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);
							eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
							eq.setTradeType(null);
							eq.setUpdateDate(sysDateIns);
							eq.setUpdateUser(equipStockTransForm.getCreateUser());
							equipMngDAO.updateEquipment(eq);
							
							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(equipStockTransForm.getCreateUser());
							equipHistory.setEquip(eq);
							if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
								//equipHistory.setEquipWarehouseCode(eq.getStockCode());
							}
							equipHistory.setHealthStatus(eq.getHealthStatus());
							if (eq.getStockId() != null) {
								equipHistory.setObjectId(eq.getStockId());
							}
							if (eq.getStockType() != null) {
								equipHistory.setObjectType(eq.getStockType());
							}
							equipHistory.setStatus(eq.getStatus().getValue());
							equipHistory.setTradeStatus(eq.getTradeStatus());
							equipHistory.setTradeType(eq.getTradeType());
							equipHistory.setUsageStatus(eq.getUsageStatus());
							equipHistory.setObjectCode(eq.getStockCode());
							equipHistory.setFormId(equipStockTransForm.getId());
							equipHistory.setFormType(EquipTradeType.WAREHOUSE_TRANSFER);
							equipHistory.setStockName(eq.getStockName());
							equipHistory.setTableName("EQUIP_STOCK_TRANS_FORM");
							equipMngDAO.createEquipHistory(equipHistory);
							//tru kho
							EquipStockTotal khoDich = equipmentStockTotalDAO.getEquipStockTotalById(ed.getToStockId(), EquipStockTotalType.KHO, eq.getEquipGroup().getId());
							EquipStockTotal khoNguon = equipmentStockTotalDAO.getEquipStockTotalById(ed.getFromStockId(), EquipStockTotalType.KHO, eq.getEquipGroup().getId());
							if (khoNguon != null) {
								khoNguon.setQuantity(khoNguon.getQuantity() - 1);
								equipmentStockTotalDAO.updateEquipmentStockTotal(khoNguon);

							}
							if (khoDich != null) {
								khoDich.setQuantity(khoDich.getQuantity() + 1);
								equipmentStockTotalDAO.updateEquipmentStockTotal(khoDich);
							} else {
								// tao moi
								khoDich = new EquipStockTotal();
								khoDich.setCreateDate(sysDateIns);
								khoDich.setCreateUser(equipStockTransForm.getCreateUser());
								khoDich.setEquipGroup(eq.getEquipGroup());
								khoDich.setQuantity(1);
								khoDich.setStockId(ed.getToStockId());
								khoDich.setStockType(EquipStockTotalType.KHO);
								khoDich = equipmentStockTotalDAO.createEquipStockTotal(khoDich);
							}
						}else{
							if(eq.getTradeType()==null){
								eq.setTradeType(EquipTradeType.WAREHOUSE_TRANSFER);
								eq.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);
								eq.setTradeStatus(EquipTradeStatus.TRADING.getValue());
								eq.setUpdateDate(sysDateIns);
								eq.setUpdateUser(equipStockTransForm.getCreateUser());
								equipMngDAO.updateEquipment(eq);
								
								// luu lich su thiet bi
								EquipHistory equipHistory = new EquipHistory();
								equipHistory.setCreateDate(sysDateIns);
								equipHistory.setCreateUser(equipStockTransForm.getCreateUser());
								equipHistory.setEquip(eq);
								if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
									//equipHistory.setEquipWarehouseCode(eq.getStockCode());
								}
								equipHistory.setHealthStatus(eq.getHealthStatus());
								if (eq.getStockId() != null) {
									equipHistory.setObjectId(eq.getStockId());
								}
								if (eq.getStockType() != null) {
									equipHistory.setObjectType(eq.getStockType());
								}
								equipHistory.setStatus(eq.getStatus().getValue());
								equipHistory.setTradeStatus(eq.getTradeStatus());
								equipHistory.setTradeType(eq.getTradeType());
								equipHistory.setUsageStatus(eq.getUsageStatus());
								equipHistory.setObjectCode(eq.getStockCode());
								equipHistory.setFormId(equipStockTransForm.getId());
								equipHistory.setFormType(EquipTradeType.WAREHOUSE_TRANSFER);
								equipHistory.setStockName(eq.getStockName());
								equipHistory.setTableName("EQUIP_STOCK_TRANS_FORM");
								equipMngDAO.createEquipHistory(equipHistory);
							}
						}
					} else if (equipStockTransForm.getStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
						eq.setTradeStatus(EquipTradeStatus.TRADING.getValue());
						eq.setTradeType(EquipTradeType.WAREHOUSE_TRANSFER);
						eq.setUpdateDate(sysDateIns);
						eq.setUpdateUser(equipStockTransForm.getCreateUser());
						equipMngDAO.updateEquipment(eq);
					}
					// luu chi tiet bien ban
					ed.setEquipStockTransForm(equipStockTransForm);
					ed.setHealthStatus(ed.getEquip().getHealthStatus());
//					if (!equipStockTransForm.getStatus().equals(StatusRecordsEquip.CANCELLATION.getValue())) {
						ed = equipMngDAO.createEquipStockTransFormDtl(ed);
//					}
				}
			} else {
//				for (int i = 0; i < addEquipmentIds.size(); i++) {
//					Equipment eq = equipMngDAO.getEquipmentById(addEquipmentIds.get(i));
//					if (equipStockTransForm.getStatus().equals(StatusRecordsEquip.CANCELLATION.getValue())) {
//						eq.setTradeType(null);
//						eq.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);
//						eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
//						eq.setUpdateDate(sysDateIns);
//						eq.setUpdateUser(equipStockTransForm.getCreateUser());
//						equipMngDAO.updateEquipment(eq);
//						// luu lich su thiet bi
//						EquipHistory equipHistory = new EquipHistory();
//						equipHistory.setCreateDate(sysDateIns);
//						equipHistory.setCreateUser(equipStockTransForm.getCreateUser());
//						equipHistory.setEquip(eq);
//						if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
//							//equipHistory.setEquipWarehouseCode(eq.getStockCode());
//						}
//						equipHistory.setHealthStatus(eq.getHealthStatus());
//						if (eq.getStockId() != null) {
//							equipHistory.setObjectId(eq.getStockId());
//						}
//						if (eq.getStockType() != null) {
//							equipHistory.setObjectType(eq.getStockType());
//						}
//						equipHistory.setStatus(eq.getStatus().getValue());
//						equipHistory.setTradeStatus(eq.getTradeStatus());
//						equipHistory.setTradeType(eq.getTradeType());
//						equipHistory.setUsageStatus(eq.getUsageStatus());
//						equipMngDAO.createEquipHistory(equipHistory);
//					} else if (equipStockTransForm.getStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
//						if(PerFormStatus.SUCCESS.getValue().equals(equipStockTransForm.getPerformStatus().getValue())){
//							EquipStock equipStock = equipMngDAO.getEquipStockById(ed.getToStockId());
//							if(equipStock!=null){
//								eq.setStockCode(equipStock.getCode());
//								eq.setStockName(equipStock.getName());
//								eq.setStockType(EquipStockTotalType.KHO);
//								eq.setStockId(equipStock.getId());
//							}
//							eq.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);
//							eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
//							eq.setTradeType(null);
//							eq.setUpdateDate(sysDateIns);
//							eq.setUpdateUser(equipStockTransForm.getCreateUser());
//							equipMngDAO.updateEquipment(eq);
//							
//							// luu lich su thiet bi
//							EquipHistory equipHistory = new EquipHistory();
//							equipHistory.setCreateDate(sysDateIns);
//							equipHistory.setCreateUser(equipStockTransForm.getCreateUser());
//							equipHistory.setEquip(eq);
//							if (!StringUtility.isNullOrEmpty(eq.getStockCode())) {
//								//equipHistory.setEquipWarehouseCode(eq.getStockCode());
//							}
//							equipHistory.setHealthStatus(eq.getHealthStatus());
//							if (eq.getStockId() != null) {
//								equipHistory.setObjectId(eq.getStockId());
//							}
//							if (eq.getStockType() != null) {
//								equipHistory.setObjectType(eq.getStockType());
//							}
//							equipHistory.setStatus(eq.getStatus().getValue());
//							equipHistory.setTradeStatus(eq.getTradeStatus());
//							equipHistory.setTradeType(eq.getTradeType());
//							equipHistory.setUsageStatus(eq.getUsageStatus());
//							equipMngDAO.createEquipHistory(equipHistory);
//						}
//					} else if (equipStockTransForm.getStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
//						eq.setUpdateDate(sysDateIns);
//						eq.setUpdateUser(equipStockTransForm.getCreateUser());
//						equipMngDAO.updateEquipment(eq);
//					}
//				}
			}
			//Xu ly xoa file
			if (filter != null && filter.getLstEquipAttachFileId() != null && !filter.getLstEquipAttachFileId().isEmpty()) {
				BasicFilter<EquipAttachFile> filterB = new BasicFilter<EquipAttachFile>();
				filterB.setLstId(filter.getLstEquipAttachFileId());
				filterB.setObjectId(equipStockTransForm.getId());
				filterB.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
				List<EquipAttachFile> lstFile = equipRecordDAO.searchEquipAttachFileByFilter(filterB);
				if (lstFile != null && !lstFile.isEmpty()) {
					for (EquipAttachFile fileVItem : lstFile) {
						equipRecordDAO.deletEquipAttachFile(fileVItem);
					}
				}
			}
			//Xu ly them file
			if (filter != null && filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
				for (FileVO voFile : filter.getLstFileVo()) {
					EquipAttachFile equipAttFile = new EquipAttachFile();
					equipAttFile.setUrl(voFile.getUrl());
					equipAttFile.setObjectId(equipStockTransForm.getId());
					equipAttFile.setFileName(voFile.getFileName());
					equipAttFile.setThumbUrl(voFile.getThumbUrl());
					equipAttFile.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
					equipAttFile.setCreateDate(sysDateIns);
					equipAttFile.setCreateUser(equipStockTransForm.getCreateUser());
					equipRecordDAO.createEquipAttachFile(equipAttFile);
				}
			}
			return equipStockTransForm;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipStockTransForm updateStockTransFormApproved(EquipStockTransForm equipStockTransForm, List<EquipStockTransFormDtl> equipmentStockTransFormDtl, List<Long> addEquipmentIds, EquipRecordFilter<EquipStockTransForm> filter, LogInfoVO logInfo)
			throws BusinessException {
		try {
			Date sysDateIns = commonDAO.getSysDate();
			String usename = equipStockTransForm.getCreateUser();
			if (equipmentStockTransFormDtl != null && equipmentStockTransFormDtl.size() > 0) {
				for (int i = 0; i < equipmentStockTransFormDtl.size(); i++) {
					EquipStockTransFormDtl ed = equipmentStockTransFormDtl.get(i);
					if (PerFormStatus.SUCCESS.equals(ed.getPerformStatus())) {
						ed.setCompleteDate(sysDateIns);
						Equipment eq = ed.getEquip();
						eq.setStockId(ed.getToStockId());
						EquipStock stock = equipMngDAO.getEquipStockById(ed.getToStockId());
						if (stock != null) {
							eq.setStockCode(stock.getCode());
						}
						eq.setStockName(ed.getToStockName());
						eq.setStockType(EquipStockTotalType.KHO);
						eq.setUpdateDate(sysDateIns);
						eq.setUpdateUser(usename);
						eq.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);
						eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						eq.setTradeType(null);
						equipMngDAO.updateEquipment(eq);

						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(usename);
						equipHistory.setEquip(eq);
						equipHistory.setHealthStatus(eq.getHealthStatus());
						if (eq.getStockId() != null) {
							equipHistory.setObjectId(eq.getStockId());
						}
						if (eq.getStockType() != null) {
							equipHistory.setObjectType(eq.getStockType());
						}
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setFormId(equipStockTransForm.getId());
						equipHistory.setFormType(EquipTradeType.WAREHOUSE_TRANSFER);
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName("EQUIP_STOCK_TRANS_FORM");
						equipMngDAO.createEquipHistory(equipHistory);

						//tru kho
						EquipStockTotal khoDich = equipmentStockTotalDAO.getEquipStockTotalById(ed.getToStockId(), EquipStockTotalType.KHO, eq.getEquipGroup().getId());
						EquipStockTotal khoNguon = equipmentStockTotalDAO.getEquipStockTotalById(ed.getFromStockId(), EquipStockTotalType.KHO, eq.getEquipGroup().getId());
						if (khoNguon != null) {
							khoNguon.setQuantity(khoNguon.getQuantity() - 1);
							equipmentStockTotalDAO.updateEquipmentStockTotal(khoNguon);

						}
						if (khoDich != null) {
							khoDich.setQuantity(khoDich.getQuantity() + 1);
							equipmentStockTotalDAO.updateEquipmentStockTotal(khoDich);
						} else {
							// tao moi
							khoDich = new EquipStockTotal();
							khoDich.setCreateDate(sysDateIns);
							khoDich.setCreateUser(equipStockTransForm.getCreateUser());
							khoDich.setEquipGroup(eq.getEquipGroup());
							khoDich.setQuantity(1);
							khoDich.setStockId(ed.getToStockId());
							khoDich.setStockType(EquipStockTotalType.KHO);
							khoDich = equipmentStockTotalDAO.createEquipStockTotal(khoDich);
						}
					} else if (PerFormStatus.CANCEL.equals(ed.getPerformStatus())) {
						Equipment eq = ed.getEquip();
						eq.setUpdateDate(sysDateIns);
						eq.setUpdateUser(usename);
						eq.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE);
						eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						eq.setTradeType(null);
						equipMngDAO.updateEquipment(eq);

						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(usename);
						equipHistory.setEquip(eq);
						equipHistory.setHealthStatus(eq.getHealthStatus());
						if (eq.getStockId() != null) {
							equipHistory.setObjectId(eq.getStockId());
						}
						if (eq.getStockType() != null) {
							equipHistory.setObjectType(eq.getStockType());
						}
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setFormId(equipStockTransForm.getId());
						equipHistory.setFormType(EquipTradeType.WAREHOUSE_TRANSFER);
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName("EQUIP_STOCK_TRANS_FORM");
						equipMngDAO.createEquipHistory(equipHistory);
					}
					equipMngDAO.updateEquipStockTransFormDtl(ed);
				}
				List<EquipStockTransFormDtl> lstDetail = equipMngDAO.getListEquipStockTransFormDtlOnGoing(equipStockTransForm.getId());
				if (lstDetail == null || lstDetail.isEmpty()) {
					equipStockTransForm.setPerformStatus(PerFormStatus.SUCCESS);
				}
				EquipStockTransForm equipStockTransFormOld = equipMngDAO.getEquipStockTransForm(equipStockTransForm.getId());
				if (equipStockTransFormOld.getPerformStatus() != equipStockTransForm.getPerformStatus()) {
					equipMngDAO.updateEquipStockTransForm(equipStockTransForm);
					// luu lich su bien ban
					EquipFormHistory equipFormHistory = new EquipFormHistory();
					equipFormHistory.setActDate(sysDateIns);
					equipFormHistory.setRecordType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
					equipFormHistory.setCreateDate(sysDateIns);
					equipFormHistory.setCreateUser(equipStockTransForm.getCreateUser());
					equipFormHistory.setRecordStatus(equipStockTransForm.getStatus());
					equipFormHistory.setRecordId(equipStockTransForm.getId());
					equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
				}
			}

			//Xu ly xoa file
			if (filter != null && filter.getLstEquipAttachFileId() != null && !filter.getLstEquipAttachFileId().isEmpty()) {
				BasicFilter<EquipAttachFile> filterB = new BasicFilter<EquipAttachFile>();
				filterB.setLstId(filter.getLstEquipAttachFileId());
				filterB.setObjectId(equipStockTransForm.getId());
				filterB.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
				List<EquipAttachFile> lstFile = equipRecordDAO.searchEquipAttachFileByFilter(filterB);
				if (lstFile != null && !lstFile.isEmpty()) {
					for (EquipAttachFile fileVItem : lstFile) {
						equipRecordDAO.deletEquipAttachFile(fileVItem);
					}
				}
			}
			//Xu ly them file
			if (filter != null && filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
				for (FileVO voFile : filter.getLstFileVo()) {
					EquipAttachFile equipAttFile = new EquipAttachFile();
					equipAttFile.setUrl(voFile.getUrl());
					equipAttFile.setObjectId(equipStockTransForm.getId());
					equipAttFile.setFileName(voFile.getFileName());
					equipAttFile.setThumbUrl(voFile.getThumbUrl());
					equipAttFile.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
					equipAttFile.setCreateDate(sysDateIns);
					equipAttFile.setCreateUser(equipStockTransForm.getCreateUser());
					equipRecordDAO.createEquipAttachFile(equipAttFile);
				}
			}
			return equipStockTransForm;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public void deleteStockTransFormDtl(List<Long> lstDelete) throws BusinessException {
		try {
			if (lstDelete != null && !lstDelete.isEmpty()) {
				int size = lstDelete.size();
				for (int i = 0; i < size; i++) {
					EquipStockTransFormDtl eqs = equipMngDAO.getEquipStockTransFormDtlById(lstDelete.get(i));
					if (eqs != null) {
						//Tra lai trang thai thiet bi
						equipMngDAO.deleteEquipStockTransFormDtl(eqs);
						Equipment equipment = null;
						equipment = equipMngDAO.getEquipmentById(eqs.getEquip().getId());
						equipment.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						equipment.setTradeType(null);
						equipment.setUpdateUser(eqs.getCreateUser());
						equipMngDAO.updateEquipment(equipment);
						// luu lich su thiet bi
						Date sysDateIns = commonDAO.getSysDate();
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(eqs.getCreateUser());
						equipHistory.setEquip(equipment);
						if (!StringUtility.isNullOrEmpty(equipment.getStockCode())) {
							//equipHistory.setEquipWarehouseCode(equipment.getStockCode());
						}
						equipHistory.setHealthStatus(equipment.getHealthStatus());
						if (equipment.getStockId() != null) {
							equipHistory.setObjectId(equipment.getStockId());
						}
						if (equipment.getStockType() != null) {
							equipHistory.setObjectType(equipment.getStockType());
						}
						equipHistory.setStatus(equipment.getStatus().getValue());
						equipHistory.setTradeStatus(equipment.getTradeStatus());
						equipHistory.setTradeType(equipment.getTradeType());
						equipHistory.setUsageStatus(equipment.getUsageStatus());
						equipHistory.setObjectCode(equipment.getStockCode());
						equipHistory.setFormId(eqs.getEquipStockTransForm().getId());
						equipHistory.setFormType(EquipTradeType.WAREHOUSE_TRANSFER);
						equipHistory.setStockName(equipment.getStockName());
						equipHistory.setTableName("EQUIP_STOCK_TRANS_FORM");
						equipMngDAO.createEquipHistory(equipHistory);
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipmentDeliveryVO> getListEquipmentequipmentStockTransFormVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException {
		try {
			List<EquipmentDeliveryVO> lst = equipMngDAO.getListEquipmentequipmentStockTransFormVOByFilter(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> objVO = new ObjectVO<EquipmentDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipStockTransForm updateEquipmentStockTransForm(EquipStockTransForm equipStockTransForm, List<Long> addEquipmentIds, EquipRecordFilter<EquipStockTransForm> filter, LogInfoVO logInfo) throws BusinessException {
		try {
			Date sysDateIns = commonDAO.getSysDate();
			/*
			 * reset trang thai cac thiet bi bi xoa khoi bien ban o trang thai
			 * du thao
			 */
			this.updateRemovedEquipmentStatusInStockTransRecord(equipStockTransForm, addEquipmentIds, sysDateIns, logInfo);

			/*
			 * String code = equipMngDAO.getCodeStockTransForm();
			 * equipStockTransForm.setCode(code);
			 */
			//Xu ly xoa file
			if (filter != null && filter.getLstEquipAttachFileId() != null && !filter.getLstEquipAttachFileId().isEmpty()) {
				BasicFilter<EquipAttachFile> filterB = new BasicFilter<EquipAttachFile>();
				filterB.setLstId(filter.getLstEquipAttachFileId());
				filterB.setObjectId(equipStockTransForm.getId());
				filterB.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
				List<EquipAttachFile> lstFile = equipRecordDAO.searchEquipAttachFileByFilter(filterB);
				if (lstFile != null && !lstFile.isEmpty()) {
					for (EquipAttachFile fileVItem : lstFile) {
						equipRecordDAO.deletEquipAttachFile(fileVItem);
					}
				}
			}
			//Xu ly them file
			if (filter != null && filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
				for (FileVO voFile : filter.getLstFileVo()) {
					EquipAttachFile equipAttFile = new EquipAttachFile();
					equipAttFile.setUrl(voFile.getUrl());
					equipAttFile.setObjectId(equipStockTransForm.getId());
					equipAttFile.setFileName(voFile.getFileName());
					equipAttFile.setThumbUrl(voFile.getThumbUrl());
					equipAttFile.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
					equipAttFile.setCreateDate(sysDateIns);
					equipAttFile.setCreateUser(equipStockTransForm.getCreateUser());
					equipRecordDAO.createEquipAttachFile(equipAttFile);
				}
			}
			return equipStockTransForm = equipMngDAO.updateEquipmentStockTransForm(equipStockTransForm);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEquipItemConfigByStatus(EquipItemConfig equipItemConfig, Integer status, String userName) throws BusinessException {
		try {
			if (equipItemConfig == null || equipItemConfig.getId() == null) {
				throw new BusinessException("equipItemConfig.getId is null ");
			}
			if (status == null || !ActiveType.isValidValue(status)) {
				throw new BusinessException("status not in ActiveType ");
			}
			Date sysDateIns = commonDAO.getSysDate();
			equipItemConfig.setUpdateDate(sysDateIns);
			equipItemConfig.setUpdateUser(userName);
			equipItemConfig.setStatus(ActiveType.parseValue(status));
			commonDAO.updateEntity(equipItemConfig);
			List<EquipItemConfigDtl> lstDatail = equipMngDAO.getListEquipItemConfigDtlByEquipItemConfig(equipItemConfig.getId(), ActiveType.RUNNING.getValue());
			if (lstDatail != null && !lstDatail.isEmpty()) {
				for (EquipItemConfigDtl rowJ: lstDatail) {
					rowJ.setUpdateDate(sysDateIns);
					rowJ.setUpdateUser(userName);
					rowJ.setStatus(ActiveType.parseValue(status));
					commonDAO.updateEntity(rowJ);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<EquipmentDeliveryVO> getListEquipmentStockTransByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException {
		try {
			List<EquipmentDeliveryVO> lst = equipMngDAO.getListEquipmentStockTransByFilter(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> objVO = new ObjectVO<EquipmentDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipRepairPayForm getEquipRepairPayFormByCode(String code) throws BusinessException {
		try {
			return equipMngDAO.getEquipRepairPayFormByCode(code);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public String updateEquipmentRepairPaymentRecord(final Long equipmentRepairItemId, final EquipmentRepairPaymentRecord equipmentRepairPaymentRecord, final LogInfoVO logInfo) throws BusinessException {
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		final StringBuilder errorMessage = new StringBuilder();
		Boolean isUpdateSuccess = transactionTemplate.execute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus transactionStatus) {
				Boolean isUpdateSuccess = true;
				try {
					EquipPeriod currentEquipmentPeriod = equipmentPeriodDAO.getEquipPeriodCurrent();
					if (currentEquipmentPeriod == null || currentEquipmentPeriod.getStatus() != EquipPeriodType.OPENED) {
						errorMessage.append("equipment.repair.payment.record.equipment.period.not.open");
						isUpdateSuccess = false;
						return isUpdateSuccess;
					}
					EquipRepairForm equipmentRepairForm = equipMngDAO.getEquipRepairFormById(equipmentRepairItemId);
					if (equipmentRepairForm == null) {
						errorMessage.append("equipment.repair.payment.record.equipment.repair.form.not.exists");
						isUpdateSuccess = false;
						return isUpdateSuccess;
					}
					Date sysdate = commonDAO.getSysDate();
					/*
					 * tao equip_repair_pay_form
					 */
					if (StringUtility.isNullOrEmpty(equipmentRepairPaymentRecord.getPaymentRecordCode())) {
						errorMessage.append("equipment.repair.payment.record.payment.record.code.must.not.empty");
						isUpdateSuccess = false;
						return isUpdateSuccess;
					}
					EquipRepairPayForm equipRepairPayForm = equipMngDAO.retrieveEquipmentRepairPayment(equipmentRepairItemId);
					if (equipRepairPayForm == null) { // tao moi phieu thanh toan
						//equipRepairPayForm = equipMngDAO.retrieveEquipmentRepairPaymentRecord(equipmentRepairPaymentRecord.getPaymentRecordCode().trim().toUpperCase());
						equipRepairPayForm = equipMngDAO.getEquipRepairPayFormByCode(equipmentRepairPaymentRecord.getPaymentRecordCode().trim().toUpperCase());
						if (equipRepairPayForm != null) {
							errorMessage.append("equipment.repair.payment.record.payment.record.code.exists");
							isUpdateSuccess = false;
							return isUpdateSuccess;
						}

						equipRepairPayForm = new EquipRepairPayForm();
						equipRepairPayForm.setCode(equipmentRepairPaymentRecord.getPaymentRecordCode().trim().toUpperCase());
						//equipRepairPayForm.setEquipRepairForm(equipmentRepairForm);
						equipRepairPayForm.setEquipPeriod(currentEquipmentPeriod);
						equipRepairPayForm.setPaymentDate(equipmentRepairPaymentRecord.getPaymentDate());
						equipRepairPayForm.setCreateUser(logInfo.getStaffCode());
						equipRepairPayForm.setCreateDate(sysdate);
						//equipRepairPayForm.setMaterialAmount(equipmentRepairPaymentRecord.getMaterialPrice()); //
						//equipRepairPayForm.setWorkerAmount(equipmentRepairPaymentRecord.getWorkerPrice()); //
						equipRepairPayForm.setTotalAmount(equipmentRepairPaymentRecord.getTotalAmount()); //
						/** Trang thai thanh toan */; // vuongmq; 26/05/2015; cap nhat; APPROVE_DATE
						equipRepairPayForm.setApproveDate(sysdate);
						equipRepairPayForm = commonDAO.createEntity(equipRepairPayForm);
					} else {
						equipRepairPayForm.setUpdateDate(sysdate);
						equipRepairPayForm.setUpdateUser(logInfo.getStaffCode());
						/** Trang thai thanh toan */; // vuongmq; 26/05/2015; cap nhat; APPROVE_DATE
						equipRepairPayForm.setApproveDate(sysdate);
						commonDAO.updateEntity(equipRepairPayForm);
						/*
						 * xoa cac chi tiet cua phieu thanh toan cu trong truong
						 * hop cap nhat
						 */
						List<EquipRepairPayFormDtl> equipmentRepairPaymentDetails = equipMngDAO.retrieveEquipmentRepairPaymentDetails(equipmentRepairItemId);
						if (equipmentRepairPaymentDetails != null && equipmentRepairPaymentDetails.size() > 0) {
							for (EquipRepairPayFormDtl equipRepairPayFormDtl : equipmentRepairPaymentDetails) {
								commonDAO.deleteEntity(equipRepairPayFormDtl);
							}
						}
					}
					/*
					 * tao equip_repair_pay_form_dtl
					 */
					// kiem tra so luong tien thanh toan nhap vao co bang so luong hang muc sua chua hay ko
					EquipRepairFilter equipmentRepairFilter = new EquipRepairFilter();
					equipmentRepairFilter.setEquipRepairId(equipmentRepairForm.getId());
					List<EquipRepairFormDtl> equipmentRepairFormDetails = equipMngDAO.getListEquipRepairDtlByEquipRepairId(equipmentRepairFilter);
					if (equipmentRepairFormDetails != null && equipmentRepairPaymentRecord.getPaymentRecordDetails() != null) {
						if (equipmentRepairFormDetails.size() != equipmentRepairPaymentRecord.getPaymentRecordDetails().size()) {
							errorMessage.append("equipment.repair.payment.record.payment.record.count.must.equal.to.repair.item.count");
							isUpdateSuccess = false;
						}
						if (isUpdateSuccess) {
							for (RepairItemPaymentVO repairItemPaymentVO : equipmentRepairPaymentRecord.getPaymentRecordDetails()) {
								EquipRepairFormDtl equipRepairFormDtl = commonDAO.getEntityById(EquipRepairFormDtl.class, repairItemPaymentVO.getItemId());
								if (equipRepairFormDtl == null) {
									errorMessage.append("equipment.repair.payment.record.equipment.repair.form.detail.not.exists");
									isUpdateSuccess = false;
									break;
								}
								if (repairItemPaymentVO.getTotalRepairAmount() == null || repairItemPaymentVO.getTotalRepairAmount().compareTo(BigDecimal.ZERO) <= 0) {
									errorMessage.append("equipment.repair.payment.record.repair.amount.must.be.greater.than.zero");
									isUpdateSuccess = false;
									break;
								}

								EquipRepairPayFormDtl equipRepairPayFormDtl = new EquipRepairPayFormDtl();
								equipRepairPayFormDtl.setEquipRepairPayForm(equipRepairPayForm);
								//equipRepairPayFormDtl.setEquipRepairFormDtl(equipRepairFormDtl);
								equipRepairPayFormDtl.setTotalActualAmount(repairItemPaymentVO.getTotalRepairAmount());
								commonDAO.createEntity(equipRepairPayFormDtl);
							}
						}
					}

				} catch (DataAccessException e) {
					isUpdateSuccess = false;
					errorMessage.append("system.error");
					transactionStatus.setRollbackOnly();
				}
				if (!isUpdateSuccess) {
					transactionStatus.setRollbackOnly();
				}
				return isUpdateSuccess;
			}
		});
		if (isUpdateSuccess) {
			return null;
		}
		return errorMessage.toString();
	}

	@Override
	public List<EquipRepairPayFormDtl> retrieveEquipmentRepairPaymentDetails(Long equipmentRepairFormId) throws BusinessException {
		try {
			return equipMngDAO.retrieveEquipmentRepairPaymentDetails(equipmentRepairFormId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipRepairPayForm retrieveEquipmentRepairPayment(Long equipmentRepairFormId) throws BusinessException {
		try {
			return equipMngDAO.retrieveEquipmentRepairPayment(equipmentRepairFormId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * Lay danh sach chi tiet cua phieu thanh toan;
	 * @author vuongmq
	 * @since 24/06/2015
	 * @return
	 */
	@Override
	public List<EquipRepairPayFormDtl> getEquipmentRepairPaymentDtlFilter(	EquipRepairFilter filter) throws BusinessException {
		try {
			return equipMngDAO.getEquipmentRepairPaymentDtlFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * kiem tra trang thai moi cua bien ban kiem ke co hop le hay ko
	 * 
	 * @author tuannd20
	 * @param oldStatus
	 *            trang thai cu
	 * @param newStatus
	 *            trang thai moi
	 * @return true neu nhu trang thai moi hop le, nguoc lai, false
	 */
	private Boolean isNewEquipmentStatisticRecordStatusValid(EquipmentStatisticRecordStatus oldStatus, EquipmentStatisticRecordStatus newStatus) {
		Boolean isNewStatusValid = false;
		isNewStatusValid |= oldStatus == EquipmentStatisticRecordStatus.DRAFT && newStatus == EquipmentStatisticRecordStatus.RUNNING;
		isNewStatusValid |= oldStatus == EquipmentStatisticRecordStatus.DRAFT && newStatus == EquipmentStatisticRecordStatus.DELETED;
		isNewStatusValid |= oldStatus == EquipmentStatisticRecordStatus.RUNNING && newStatus == EquipmentStatisticRecordStatus.DONE;
		return isNewStatusValid;
	}


	/**
	 * cap nhat trang thai bien ban kiem ke
	 * 
	 * @author tuannd20
	 * @param equipmentStatisticRecordId
	 *            id cua bien ban kiem ke can cap nhat
	 * @param equipmentStatisticRecordStatus
	 *            trang thai moi cua bien ban kiem ke
	 * @param sysdate
	 *            ngay he thong tu DB
	 * @param logInfo
	 *            thong tin ghi log
	 * @param equipmentStatisticRecordCode
	 *            [out] ma cua bien ban kiem ke trong truong hop cap nhat thanh
	 *            cong
	 * @return true neu nhu cap nhat thanh cong, nguoc lai, false
	 */
	@Transactional(rollbackFor = Exception.class)
	private Boolean updateEquipmentStatisticRecordStatus(final Long equipmentStatisticRecordId, final EquipmentStatisticRecordStatus equipmentStatisticRecordStatus, final Date sysdate, final LogInfoVO logInfo,
			final StringBuilder equipmentStatisticRecordCode) {
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		Boolean isUpdateSuccess = transactionTemplate.execute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus transactionStatus) {
				try {
					EquipStatisticRecord equipStatisticRecord = commonDAO.getEntityById(EquipStatisticRecord.class, equipmentStatisticRecordId);
					if (equipStatisticRecord != null) {
						equipmentStatisticRecordCode.append(equipStatisticRecord.getCode());
						EquipmentStatisticRecordStatus currentEquipmentStatisticRecordStatus = EquipmentStatisticRecordStatus.parseValue(equipStatisticRecord.getRecordStatus());
						if (EquipmentManagerMgrImpl.this.isNewEquipmentStatisticRecordStatusValid(currentEquipmentStatisticRecordStatus, equipmentStatisticRecordStatus)) {
							if (EquipmentStatisticRecordStatus.DONE.getValue().equals(equipmentStatisticRecordStatus.getValue())) { 
								equipStatisticRecord.setApproveDate(sysdate); 			
							}
							equipStatisticRecord.setRecordStatus(equipmentStatisticRecordStatus.getValue());
							equipStatisticRecord.setUpdateDate(sysdate);
							equipStatisticRecord.setUpdateUser(logInfo.getStaffCode());
							commonDAO.updateEntity(equipStatisticRecord);
						} else {
							return false;
						}
					}
				} catch (Exception e) {
					transactionStatus.setRollbackOnly();
					return false;
				}
				return true;
			}
		});
		return isUpdateSuccess;
	}

	@Override
	public List<FormErrVO> updateEquipmentStatisticStatus(final List<Long> equipmentStatisticIds, final EquipmentStatisticRecordStatus equipmentStatisticRecordStatus, final LogInfoVO logInfo) throws BusinessException {
		if (equipmentStatisticIds != null && equipmentStatisticIds.size() > 0) {
			try {
				Date sysdate = commonDAO.getSysDate();
				List<FormErrVO> updateFailedRecords = new ArrayList<FormErrVO>();
				for (Long equipmentStatisticRecordId : equipmentStatisticIds) {
					FormErrVO formErrorVO = new FormErrVO();
					formErrorVO.setShopInActive("");
					formErrorVO.setGroupInActive("");
					formErrorVO.setGroupNotEquip("");
					StringBuilder equipmentStatisticRecordCode = new StringBuilder();
					// lay thong tin bien ban
					EquipStatisticRecord equipStatisticRecord = commonDAO.getEntityById(EquipStatisticRecord.class, equipmentStatisticRecordId);
					if (equipmentStatisticRecordStatus.getValue().equals(EquipmentStatisticRecordStatus.RUNNING.getValue()) && equipStatisticRecord.getRecordStatus().equals(EquipmentStatisticRecordStatus.DRAFT.getValue())) {
						// lay danh sach nhom
						List<EquipGroup> lstEquipGroup = equipStatisticGroupDAO.getListEquipGroupByRecordId(equipmentStatisticRecordId);
						List<Product> listShelf = equipStatisticGroupDAO.getListShelfByRecordId(equipmentStatisticRecordId);
						List<Equipment> lstEquip = equipStatisticGroupDAO.getListEquipByRecordId(equipmentStatisticRecordId);
						
						
						// lay danh sach don vi thuoc bien ban kiem ke
						List<EquipStatisticUnit> lstUnit = equipStatisticRecordDAO.getListStatisticUnitChild(equipmentStatisticRecordId, null);
//						List<ShopObjectType> lstShopObjectType = new ArrayList<ShopObjectType>();
//						lstShopObjectType.add(ShopObjectType.NPP);
//						lstShopObjectType.add(ShopObjectType.NPP_KA);
//						List<EquipRecordShopVO> lstShop = equipStatisticRecordDAO.getListShopOfRecord(equipmentStatisticRecordId, lstShopObjectType, null, null);
						// kiem tra co nhom hay shop ko
						if ((lstEquipGroup.isEmpty() && listShelf.isEmpty() && lstEquip.isEmpty()) || lstUnit.isEmpty()) {
							formErrorVO.setFormCode(equipStatisticRecord.getCode());
							formErrorVO.setNotGroupOrShop(1);
						}
						// kiem tra shop co hoat dong ko
//						for (EquipRecordShopVO shop : lstShop) {
//							if (!ActiveType.RUNNING.getValue().equals(shop.getStatus())) {
//								formErrorVO.setShopInActive(formErrorVO.getShopInActive() + ", " + shop.getShopCode());
//							}
//						}
//						if (!StringUtility.isNullOrEmpty(formErrorVO.getShopInActive())) {
//							formErrorVO.setFormCode(equipStatisticRecord.getCode());
//							String t = formErrorVO.getShopInActive().substring(2);
//							formErrorVO.setShopInActive(t);
//						}
						// kiem tra nhom co hoat dong ko
//						for (EquipGroup eqGroup : lstEquipGroup) {
//							if (!ActiveType.RUNNING.getValue().equals(eqGroup.getStatus().getValue())) {
//								formErrorVO.setGroupInActive(formErrorVO.getGroupInActive() + ", " + eqGroup.getCode());
//							} else {
//								// check nhom thiet bi
//								EquipmentFilter<EquipmentVO> filter = new EquipmentFilter<EquipmentVO>();
//								filter.setEquipGroupId(eqGroup.getId());
//								filter.setStockType(EquipStockTotalType.KHO_KH.getValue());
//								filter.setStatus(StatusType.ACTIVE.getValue());
//								filter.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
//								// lay danh sach thiet bi trong mot nhom
//								List<EquipmentVO> equipVO = equipMngDAO.getListEquipmentByFilter(filter);
//								if (equipVO != null && equipVO.size() > 0) {
//									Integer sizeEquip = equipVO.size();
//									for (int i = 0, size = equipVO.size(); i < size; i++) {
//										EquipmentVO equipmentVO = equipVO.get(i);
//										for (int j = 0, sizej = lstShop.size(); j < sizej; j++) {
//											if (EquipStockTotalType.KHO_KH.getValue().equals(equipmentVO.getStockType())
//													&& (EquipUsageStatus.IS_USED.getValue().equals(equipmentVO.getUsageStatus()) || EquipUsageStatus.SHOWING_REPAIR.getValue().equals(equipmentVO.getUsageStatus()))
//													&& equipmentVO.getStockCode().indexOf(lstShop.get(j).getShopCode()) == 0) {
//												sizeEquip--;
//												break;
//											}
//										}
//									}
//									if (sizeEquip == equipVO.size()) {
//										formErrorVO.setGroupNotEquip(formErrorVO.getGroupNotEquip() + ", " + eqGroup.getCode());
//									}
//								} else {
//									formErrorVO.setGroupNotEquip(formErrorVO.getGroupNotEquip() + ", " + eqGroup.getCode());
//								}
//							}
//						}
						if (!StringUtility.isNullOrEmpty(formErrorVO.getGroupInActive())) {
							formErrorVO.setFormCode(equipStatisticRecord.getCode());
							String t = formErrorVO.getGroupInActive().substring(2);
							formErrorVO.setGroupInActive(t);
						}
						if (!StringUtility.isNullOrEmpty(formErrorVO.getGroupNotEquip())) {
							formErrorVO.setFormCode(equipStatisticRecord.getCode());
							String t = formErrorVO.getGroupNotEquip().substring(2);
							formErrorVO.setGroupNotEquip(t);
						}
						// kiem tra va chuyen trang thai
						if (StringUtility.isNullOrEmpty(formErrorVO.getShopInActive()) && StringUtility.isNullOrEmpty(formErrorVO.getGroupInActive()) && StringUtility.isNullOrEmpty(formErrorVO.getGroupNotEquip())
								&& formErrorVO.getNotGroupOrShop() == null) {
							Boolean isUpdateSuccess = this.updateEquipmentStatisticRecordStatus(equipmentStatisticRecordId, equipmentStatisticRecordStatus, sysdate, logInfo, equipmentStatisticRecordCode);
							if (!isUpdateSuccess) {
								formErrorVO.setFormCode(equipmentStatisticRecordCode.toString());
								formErrorVO.setFormStatusErr(1);
							}
						}
					} else {
						// kiem tra va chuyen trang thai
						Boolean isUpdateSuccess = this.updateEquipmentStatisticRecordStatus(equipmentStatisticRecordId, equipmentStatisticRecordStatus, sysdate, logInfo, equipmentStatisticRecordCode);
						if (!isUpdateSuccess) {
							formErrorVO.setFormCode(equipmentStatisticRecordCode.toString());
							formErrorVO.setFormStatusErr(1);
						}
					}

					//					Boolean isUpdateSuccess = this.updateEquipmentStatisticRecordStatus(equipmentStatisticRecordId, equipmentStatisticRecordStatus, sysdate, logInfo, equipmentStatisticRecordCode);
					//					if (!isUpdateSuccess) {		
					//						formErrorVO.setFormCode(equipmentStatisticRecordCode.toString());
					//						formErrorVO.setFormStatusErr(1);						
					//					}

					if (!StringUtility.isNullOrEmpty(formErrorVO.getShopInActive()) || !StringUtility.isNullOrEmpty(formErrorVO.getGroupInActive()) || formErrorVO.getNotGroupOrShop() != null || formErrorVO.getFormStatusErr() != null) {
						updateFailedRecords.add(formErrorVO);
					}
				}
				return updateFailedRecords;
			} catch (DataAccessException e) {
				throw new BusinessException(e);
			}
		}
		return null;
	}

	@Override
	public ObjectVO<EquipmentDeliveryVO> getCheckFromStockEquipment(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException {
		try {
			List<EquipmentDeliveryVO> lst = equipMngDAO.getCheckFromStockEquipment(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> objVO = new ObjectVO<EquipmentDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord, Integer status, Integer type, LogInfoVO logInfo) throws BusinessException {
		try {
			Date sysdate = commonDAO.getSysDate();
			equipStatisticRecord.setUpdateDate(sysdate);
			equipStatisticRecord.setUpdateUser(logInfo.getStaffCode());
			if (equipStatisticRecord.getRecordStatus() != status) {
				equipStatisticRecord.setRecordStatus(status);
			}
			if (EquipmentStatisticRecordStatus.DONE.getValue().equals(status)) {
				equipStatisticRecord.setApproveDate(sysdate);
			}
			if (type != null && !type.equals(equipStatisticRecord.getType())) {
				EquipStatisticFilter filter = new EquipStatisticFilter();
				filter.setRecordId(equipStatisticRecord.getId());
				filter.setObjectType(equipStatisticRecord.getType());
				List<EquipStatisticGroup> lstEntity = equipStatisticRecordDAO.getListEquipStatisticGroup(filter);
				equipStatisticRecordDAO.deleteEquipStatisticGroup(lstEntity);
				equipStatisticRecord.setType(type);
			}
			equipStatisticRecordDAO.updateEquipStatisticRecord(equipStatisticRecord);
			
			// luu lich su
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(sysdate);
			equipFormHistory.setCreateDate(sysdate);
			equipFormHistory.setCreateUser(equipStatisticRecord.getUpdateUser());
			equipFormHistory.setRecordId(equipStatisticRecord.getId());
			equipFormHistory.setRecordStatus(equipStatisticRecord.getRecordStatus());
			equipFormHistory.setRecordType(EquipTradeType.INVENTORY.getValue());
			Staff staff = staffDAO.getStaffByCode(equipStatisticRecord.getUpdateUser());
			equipFormHistory.setStaff(staff);
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
			
			/*load customer to insert to equip_statistic_customer*/
			if(EquipmentStatisticRecordStatus.RUNNING.getValue() == status) {
				if(equipStatisticRecord.getTypeCustomer() != null) {
					if(equipStatisticRecord.getTypeCustomer() == 0) {//don vi tham gia
//						List<Customer> listCustomer = equipMngDAO.getListCustomerByShop(equipStatisticRecord.getId());
//						Map<Long, EquipStatisticCustomer> map = equipMngDAO.getListExistsCustomer(equipStatisticRecord.getId(), listCustomer);
						List<Customer> listCustomer = equipMngDAO.getListCustomerByEquipStatisticRecordId(equipStatisticRecord.getId());
						List<EquipStatisticCustomer> listInsert = new ArrayList<EquipStatisticCustomer>();
						if (listCustomer != null && !listCustomer.isEmpty()) {
							for(Customer cus : listCustomer) {
								EquipStatisticCustomer equipCus = new EquipStatisticCustomer();
								equipCus.setEquipStatisticRecord(equipStatisticRecord);
								equipCus.setCustomer(cus);
								equipCus.setType(0);//Don vi tham gia
								equipCus.setStatus(ActiveType.RUNNING.getValue());
								equipCus.setCreateDate(sysdate);
								equipCus.setCreateUser(logInfo.getStaffCode());
								listInsert.add(equipCus);
							}
							equipStatisticRecordDAO.insertListEquipStatisticCustomer(listInsert);
						}
					} else if(equipStatisticRecord.getTypeCustomer() == 1) {//ct httm
//						List<Customer> listCustomer = equipMngDAO.getListCustomerBuTCHTTM(equipStatisticRecord.getId());
//						Map<Long, EquipStatisticCustomer> map = equipMngDAO.getListExistsCustomer(equipStatisticRecord.getId(), listCustomer);
						List<Customer> listCustomer = equipMngDAO.getListCustomerByEquipSttRecIdbyCTHTTM(equipStatisticRecord.getId());
						List<EquipStatisticCustomer> listInsert = new ArrayList<EquipStatisticCustomer>();
						if (listCustomer != null && !listCustomer.isEmpty()) {
							for(Customer cus : listCustomer) {
	//							if(map.get(cus.getId()) != null) {
	//								continue;
	//							}
								EquipStatisticCustomer equipCus = new EquipStatisticCustomer();
								equipCus.setEquipStatisticRecord(equipStatisticRecord);
								equipCus.setCustomer(cus);
								equipCus.setType(1);//CTHT
								equipCus.setStatus(ActiveType.RUNNING.getValue());
								equipCus.setCreateDate(sysdate);
								equipCus.setCreateUser(logInfo.getStaffCode());
								listInsert.add(equipCus);
							}
							equipStatisticRecordDAO.insertListEquipStatisticCustomer(listInsert);
						}
					}
				}
				List<EquipStatisticCustomer> listEqSttCustomer = equipMngDAO.getListCusInImportEquipStatisticCus(equipStatisticRecord.getId(), ActiveType.STOPPED.getValue());
				if (listEqSttCustomer != null && !listEqSttCustomer.isEmpty()) {
					for (EquipStatisticCustomer item : listEqSttCustomer) {
						item.setStatus(ActiveType.RUNNING.getValue());
						item.setUpdateDate(sysdate);
						item.setUpdateUser(logInfo.getStaffCode());
						commonDAO.updateEntity(item);
					}
				}
			}
			
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Lay equipment Entity by code theo filter
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	@Override
	public Equipment getEquipmentByCodeFilter(EquipmentFilter<Equipment> filter)  throws BusinessException {
		try {
			return equipMngDAO.getEquipmentByCodeFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * Lay equipment Entity by code an status
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	@Override
	public Equipment getEquipmentByCodeAndStatus(String code, ActiveType status) throws BusinessException {
		try {
			return equipMngDAO.getEquipmentByCodeAndStatus(code, status);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipItem getEquipItemByCode(String code) throws BusinessException {
		try {
			return equipMngDAO.getEquipItemByCode(code);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	@Override
	public EquipItem getEquipItemByCodeEx(String code,ActiveType status) throws BusinessException {
		try {
			return equipMngDAO.getEquipItemByCode(code, status);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	@Override
	public ObjectVO<EquipmentDeliveryVO> searchRecordStockChangeByEquipment(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException {
		try {
			List<EquipmentDeliveryVO> lst = equipMngDAO.searchRecordStockChangeByEquipment(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> objVO = new ObjectVO<EquipmentDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Equipment getEquipmentByCode(String code) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return equipMngDAO.getEquipmentByCode(code);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipStockTransFormDtl getEquipStockTransFormDtlById(Long id) throws BusinessException {
		try {
			return equipMngDAO.getEquipStockTransFormDtlById(id);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * Lay danh sach bien ban thanh ly cho export
	 * 
	 * @author tamvnm
	 * @since 12/01/2015
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	@Override
	public List<EquipmentLiquidationFormVO> getListEquipmentLiquidationFormVOForExport(List<Long> equipmentFilter) throws BusinessException {
		try {
			/*for (int i = 0; i < equipmentFilter.size(); i++) {

				List<EquipmentLiquidationFormVO> equipmentliquidation = equipmentLiquidationFormDAO.getListEquipmentLiquidationFormVOForExport(equipmentFilter.get(i));
				if (equipmentliquidation != null) {
					for (int count = 0; count < equipmentliquidation.size(); count++) {
						lsLiquidation.add(equipmentliquidation.get(count));
					}
					//add them 1 dong rong ko co du lieu.
					if (i != equipmentFilter.size() - 1)
						lsLiquidation.add(new EquipmentLiquidationFormVO());
				}
			}*/
			EquipmentFilter<EquipmentLiquidationFormVO> filter = new EquipmentFilter<EquipmentLiquidationFormVO>();
			filter.setLstId(equipmentFilter);
			return equipmentLiquidationFormDAO.getListEquipmentLiquidationFormVOForExport(filter);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			LogUtility.logError(e, e.getMessage());
			throw new BusinessException(e);
		}
	}

	/**
	 * Lay danh sach bien ban thu hoi va thanh ly cho export
	 * 
	 * @author tamvnm
	 * @since 12/01/2015
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	@Override
	public List<EquipmentEvictionVO> getListEquipmentEvictionVOForExport(List<Long> lstIdRecord) throws BusinessException {
		List<EquipmentEvictionVO> lsEviction = new ArrayList<EquipmentEvictionVO>();
		try {
			for (int i = 0; i < lstIdRecord.size(); i++) {

				List<EquipmentEvictionVO> equipmentliquidation = equipMngDAO.getListEquipmentEvictionVOForExport(lstIdRecord.get(i));
				if (equipmentliquidation != null) {
					for (int count = 0; count < equipmentliquidation.size(); count++) {
						lsEviction.add(equipmentliquidation.get(count));
					}
					//add them 1 dong rong ko co du lieu.
					if (i != lstIdRecord.size() - 1)
						lsEviction.add(new EquipmentEvictionVO());
				}
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			LogUtility.logError(e, e.getMessage());
		}
		return lsEviction;
	}

	/**
	 * Lay danh sach bien ban thu hoi va thanh ly cho in
	 * 
	 * @author phuongvm
	 * @since 15/01/2015
	 * @param lstIdEviction
	 * @throws BusinessException
	 */
	@Override
	public List<EquipmentEvictionVOs> getListEquipmentEvictionVOForPrint(List<Long> lstIdRecord) throws BusinessException {
		List<EquipmentEvictionVOs> lsEviction = new ArrayList<EquipmentEvictionVOs>();
		try {
			for (int i = 0; i < lstIdRecord.size(); i++) {
				EquipmentEvictionVOs vos = new EquipmentEvictionVOs();
				List<EquipmentEvictionVO> equipmentliquidation = equipMngDAO.getListEquipmentEvictionVOForPrint(lstIdRecord.get(i));
				if (equipmentliquidation != null) {
					vos.setLstEquipEvictionVO(equipmentliquidation);
					vos.setDiaChi(equipmentliquidation.get(0).getDiaChi());
					vos.setLyDo(equipmentliquidation.get(0).getLyDo());
					vos.setMaKH(equipmentliquidation.get(0).getMaKH());
					vos.setTenKH(equipmentliquidation.get(0).getTenKH());
					vos.setDiaChi(equipmentliquidation.get(0).getDiaChi());
					vos.setDienThoai(equipmentliquidation.get(0).getDienThoai());
//					vos.setMaKHNew(equipmentliquidation.get(0).getMaKHNew());
//					vos.setTenKHNew(equipmentliquidation.get(0).getTenKHNew());
//					vos.setDiaChiNew(equipmentliquidation.get(0).getDiaChiNew());
					vos.setMaKHNew(null);
					vos.setTenKHNew(null);
					vos.setDiaChiNew(null);
					vos.setMaNPP(equipmentliquidation.get(0).getMaNPP());
					vos.setMaPhieu(equipmentliquidation.get(0).getMaPhieu());
					vos.setMaThietBi(equipmentliquidation.get(0).getMaThietBi());
					vos.setTenTB(equipmentliquidation.get(0).getTenTB());
					vos.setLoaiTB(equipmentliquidation.get(0).getLoaiTB());
					vos.setTinhTrang(equipmentliquidation.get(0).getTinhTrang());
					vos.setSoSeri(equipmentliquidation.get(0).getSoSeri());
					vos.setSoLuong(equipmentliquidation.get(0).getSoLuong());
					vos.setNgay(equipmentliquidation.get(0).getNgay());
					vos.setNgDaiDienKhachHang(equipmentliquidation.get(0).getNgDaiDienKhachHang());
					vos.setNgDaiDienThuHoi(equipmentliquidation.get(0).getNgDaiDienThuHoi());
					vos.setShopCustomerStockcode(equipmentliquidation.get(0).getCustomerStockCode());
					vos.setShopStockcode(equipmentliquidation.get(0).getShopStockcode());
//					vos.setViTri(equipmentliquidation.get(0).getViTri());
					vos.setViTri(null);
					vos.setCustomerId(equipmentliquidation.get(0).getCustomerId());
					lsEviction.add(vos);
				}
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			LogUtility.logError(e, e.getMessage());
		}
		return lsEviction;
	}

	@Override
	public ObjectVO<EquipmentVO> getEquipStockTransFormDtlByFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getEquipStockTransFormDtlByIdEquip(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Equipment getEquipmentByEquipId(Long equipId) throws BusinessException {
		try {
			return equipMngDAO.getEquipmentByEquipId(equipId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * cap nhat trang thai cac thiet bi bi xoa ra khoi bien ban chuyen kho o
	 * trang thai du thao
	 * 
	 * @author tuannd20
	 * @param equipStockTransForm
	 *            bien ban chuyen kho
	 * @param addEquipmentIds
	 *            id cac thiet bi se them vao bien ban
	 * @param sysdate
	 *            ngay he thong
	 * @param logInfo
	 *            thong tin ghi log
	 * @throws DataAccessException
	 * @since 15/01/2015
	 */
	private void updateRemovedEquipmentStatusInStockTransRecord(EquipStockTransForm equipStockTransForm, List<Long> addEquipmentIds, Date sysdate, LogInfoVO logInfo) throws DataAccessException {
		if (equipStockTransForm != null && equipStockTransForm.getId() != null) {
			List<Equipment> removedEquipmentFromStockTransRecords = equipMngDAO.retrieveRemovedEquipmentFromStockTransRecord(equipStockTransForm.getId(), addEquipmentIds);
			if (removedEquipmentFromStockTransRecords != null && removedEquipmentFromStockTransRecords.size() > 0) {
				for (Equipment equipment : removedEquipmentFromStockTransRecords) {
					/*
					 * cap nhat thong tin thiet bi
					 */
					equipment.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
					equipment.setTradeType(null);
					equipment.setUpdateDate(sysdate);
					equipment.setUpdateUser(logInfo.getStaffCode());
					commonDAO.updateEntity(equipment);

					/*
					 * tao moi lich su
					 */
					EquipHistory equipmentHistory = new EquipHistory();
					equipmentHistory.setEquip(equipment);
					equipmentHistory.setStatus(equipment.getStatus().getValue());
					equipmentHistory.setUsageStatus(equipment.getUsageStatus());
					equipmentHistory.setHealthStatus(equipment.getHealthStatus());
					equipmentHistory.setTradeStatus(equipment.getTradeStatus());
					equipmentHistory.setTradeType(equipment.getTradeType());
					equipmentHistory.setObjectId(equipment.getStockId());
					equipmentHistory.setObjectType(equipment.getStockType());
					//equipmentHistory.setEquipWarehouseCode(equipment.getStockCode());
					equipmentHistory.setCreateDate(sysdate);
					equipmentHistory.setCreateUser(logInfo.getStaffCode());
					equipmentHistory.setObjectCode(equipment.getStockCode());
					equipmentHistory.setFormId(equipStockTransForm.getId());
					equipmentHistory.setFormType(EquipTradeType.WAREHOUSE_TRANSFER);
					equipmentHistory.setStockName(equipment.getStockName());
					equipmentHistory.setTableName("EQUIP_STOCK_TRANS_FORM");
					commonDAO.createEntity(equipmentHistory);
				}
			}
		}
	}

	/**
	 * Lay danh sach kiem ke de xuat file excel
	 * 
	 * @author tamvnm
	 * @since 16/01/2015
	 * @param lstIdRecord
	 * @throws DataAccessException
	 */
	@Override
	public List<EquipmentStatisticCheckingRecordExportVO> getListEquipmentStatisticCheckingVOForExport(List<Long> lstIdRecord) {
		List<EquipmentStatisticCheckingRecordExportVO> lstStatisticCheckingDetailExport = new ArrayList<EquipmentStatisticCheckingRecordExportVO>();
		for (int i = 0; i < lstIdRecord.size(); i++) {
			EquipStatisticRecord equipStatic = new EquipStatisticRecord();
			List<StatisticCheckingVO> lstEquipStatisticDetail = new ArrayList<StatisticCheckingVO>();
			EquipmentStatisticCheckingRecordExportVO StatisticCheckingDetailExport = new EquipmentStatisticCheckingRecordExportVO();
			try {
				equipStatic = equipStatisticRecordDAO.getEquipStatisticRecordById(lstIdRecord.get(i));
				lstEquipStatisticDetail = equipStatisticRecordDAO.getListEquipmentStatisticCheckingVOForExport(lstIdRecord.get(i));
			} catch (DataAccessException e) {
				LogUtility.logError(e, e.getMessage());
			}
			if (equipStatic != null) {
				StatisticCheckingDetailExport.setEquipStatisticRecord(equipStatic);
			}
			if (lstEquipStatisticDetail != null) {
				StatisticCheckingDetailExport.setLstStatictisCheckingDetails(lstEquipStatisticDetail);
			}
			lstStatisticCheckingDetailExport.add(StatisticCheckingDetailExport);
		}
		return lstStatisticCheckingDetailExport;
	}

	/**
	 * Lay stock code - name ben EquipStock voi stock_id; cua kho don vi
	 * @author vuongmq
	 * @since 23/04/2015
	 * @return
	 */
	public EquipStock getEquipStockByFilter(EquipStockEquipFilter<EquipStock> filter)  throws BusinessException {
		try {
			return equipMngDAO.getEquipStockByFilter(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/***
	 * @author vuongmq
	 * @date 20/04/2015
	 * lay equip_repair_form_dtl theo filter, 
	 * lay hang muc con thoi gian bao hanh
	 */
	public EquipRepairFormDtl getEquipRepairFormDtlByFilter(EquipItemFilter filter) throws BusinessException {
		try {
			return equipMngDAO.getEquipRepairFormDtlByFilter(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<EquipmentRepairFormDtlVO> getListEquipRepairDtlVOByEquipRepairId(EquipRepairFilter filter) throws BusinessException {
		try {
			List<EquipmentRepairFormDtlVO> lst = equipMngDAO.getListEquipRepairDtlVOByEquipRepairId(filter);
			ObjectVO<EquipmentRepairFormDtlVO> objVO = new ObjectVO<EquipmentRepairFormDtlVO>();
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipStockVO> getListStockByFilter(EquipStockFilter filter) throws BusinessException {
		try {
			List<EquipStockVO> lst = equipMngDAO.getListStockByFilter(filter);
			ObjectVO<EquipStockVO> objVO = new ObjectVO<EquipStockVO>();
			objVO.setLstObject(lst);
			objVO.setkPaging(filter.getkPaging());
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/*** vuongmq; 16/03/2015; lay theo code EquipStock */

	@Override
	public Integer getEquipStockOrdinalMaxByShop(Long id) throws BusinessException {
		try {
			return equipMngDAO.getEquipStockOrdinalMaxByShop(id);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipStock> getEquipStockByShopAndOrdinal(Long id, Integer stt) throws BusinessException {
		try {
			return equipMngDAO.getEquipStockByShopAndOrdinal(id, stt);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipStock getEquipStockbyCode(String code) throws BusinessException {
		try {
			return equipMngDAO.getEquipStockbyCode(code);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<ShopViewParentVO> searchShopTreeView(Long shopId, String code, String name, EquipmentFilter<ShopViewParentVO> filter) throws BusinessException {
		try {
			return equipMngDAO.searchShopTreeView(shopId, code, name, filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<ShopViewParentVO> searchShopTreeCustomer(Long shopId, String code, String name) throws BusinessException {
		try {
			return equipMngDAO.searchShopTreeCustomer(shopId, code, name);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	/*** vuongmq; 16/03/2015; search EquipStock */
	@Override
	public ObjectVO<EquipStock> getListEquipmentStockByFilter(EquipmentFilter<EquipStock> filter) throws BusinessException {
		try {
			List<EquipStock> lst = equipMngDAO.getListEquipmentStockByFilter(filter);
			ObjectVO<EquipStock> objVO = new ObjectVO<EquipStock>();
			objVO.setLstObject(lst);
			objVO.setkPaging(filter.getkPaging());
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> searchContractNumberByFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.searchContractNumberByFilter(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setLstObject(lst);
			objVO.setkPaging(filter.getkPaging());
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> getListCheckEquipGroupById(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getListCheckEquipGroupById(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> getListCheckEquipGroupProductByIdEquipGroup(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getListCheckEquipGroupProductByIdEquipGroup(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * Tao moi Quyen nguoi dung
	 * 
	 * @author tamvnm
	 * @since 24/03/2015
	 * @param 
	 * @throws DataAccessException
	 */
	@Override
	public EquipRoleUser createEquipRoleUser(EquipRoleUser roleUser) throws BusinessException {
		try {
			return equipMngDAO.createEquipRoleUser(roleUser);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Lay danh sach staff cho phan quyen - Thiet bi
	 * 
	 * @author tamvnm
	 * @since 24/03/2015
	 * @param 
	 * @throws BusinessException
	 */
	@Override
	public ObjectVO<EquipmentStaffVO> getStaffForPermission(EquipStaffFilter filter) throws BusinessException {
		try {
			List<EquipmentStaffVO> lst = equipMngDAO.getStaffForPermission(filter);
			ObjectVO<EquipmentStaffVO> objVO = new ObjectVO<EquipmentStaffVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	
	/**
	 * Lay danh sach staff cho phan quyen - Thiet bi
	 * Note: lay danh sach staff giong nhu getStaffForPermission o tren; cong them roleCode quyen vao cho nhan vien nua
	 * @author vuongmq
	 * @date 26/03/2015
	 * @param 
	 * @throws DataAccessException
	 */
	@Override
	public ObjectVO<EquipmentStaffVO> getStaffForPermissionExportExcel(EquipStaffFilter filter) throws BusinessException {
		try {
			List<EquipmentStaffVO> lst = equipMngDAO.getStaffForPermissionExportExcel(filter);
			ObjectVO<EquipmentStaffVO> objVO = new ObjectVO<EquipmentStaffVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * Lay danh sach quyen - Gan Quyen Cho Nguoi Dung
	 * 
	 * @author tamvnm
	 * @since 24/03/2015
	 * @param 
	 * @throws BusinessException
	 */
	@Override
	public ObjectVO<EquipmentPermissionVO> getPermissionByFilter(EquipPermissionFilter filter) throws BusinessException {
		try {
			List<EquipmentPermissionVO> lst = equipMngDAO.getPermissionByFilter(filter);
			ObjectVO<EquipmentPermissionVO> objVO = new ObjectVO<EquipmentPermissionVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Lay danh sach quyen - popup search
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param 
	 * @throws BusinessException
	 */
	@Override
	public ObjectVO<EquipRole> searchListEquipRoleByFilter(
			EquipRoleFilter filter) throws BusinessException {
		try {
			List<EquipRole> lst = equipMngDAO.searchListEquipRoleByFilter(filter);
			ObjectVO<EquipRole> objVO = new ObjectVO<EquipRole>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Lay EquipRole
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param  id
	 * @throws BusinessException
	 */
	@Override
	public EquipRole getEquipRoleById(Long id) throws BusinessException {
		try {
			return equipMngDAO.getEquipRoleById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Lay Entities EquipRoleUser ung voi filter
	 * @author vuongmq
	 * @since 27/03/2015
	 */
	@Override
	public EquipRoleUser getEquipEquipRoleUserByFilter(EquipPermissionFilter filter) throws BusinessException {
		try {
			return equipMngDAO.getEquipEquipRoleUserByFilter(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public EquipStock getEquipStockById(Long id) throws BusinessException {
		try {
			return id == null ? null : equipMngDAO.getEquipStockById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipmentVO> searchEquipmentStockChangeExcel(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.searchEquipmentStockChangeExcel(filter);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipLendDetail> getListEquipmentLendDetailByLendId(
			Long equipLendId) throws BusinessException {
		List<EquipLendDetail> lstEquipLendDetail = new ArrayList<EquipLendDetail>();
		try {
		lstEquipLendDetail = equipMngDAO.getListEquipmentLendDetailByLendId(equipLendId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		return lstEquipLendDetail;
	}
	@Override
	public String getCodeEquipItemConfig() throws BusinessException {
		try {
			return equipMngDAO.getCodeEquipItemConfig();
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipItemConfigVO> getListEquipItemConfigByFilter(EquipItemConfigFilter filter) throws BusinessException {
		try {
			List<EquipItemConfigVO> lst = equipMngDAO.getListEquipItemConfigByFilter(filter);
			ObjectVO<EquipItemConfigVO> objVO = new ObjectVO<EquipItemConfigVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipItemConfigVO> exportListEquipItemConfigByFilter(EquipItemConfigFilter filter) throws BusinessException {
		try {
			return equipMngDAO.exportListEquipItemConfigByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipItemConfigDtlVO> getListEquipItemConfigDetailByFilter(EquipItemConfigDtlFilter filter) throws BusinessException {
		try {
			List<EquipItemConfigDtlVO> lst = equipMngDAO.getListEquipItemConfigDetailByFilter(filter);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipItemConfigDtlVO> getEquipItemInDetailByEquipItemConfig(Long equipItemConfigId) throws BusinessException {
		try {
			List<EquipItemConfigDtlVO> lst = equipMngDAO.getEquipItemInDetailByEquipItemConfig(equipItemConfigId);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<EquipDeliveryRecord> getListDeliveryRecordByFilter(EquipmentFilter<EquipmentVO> filter)
			throws BusinessException {
		List<EquipDeliveryRecord> lstEquipDeliveryRecord = new ArrayList<EquipDeliveryRecord>();
		try {
		lstEquipDeliveryRecord = equipMngDAO.getListDeliveryRecordByFilter(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		return lstEquipDeliveryRecord;
	}

	@Override
	public List<EquipmentRecordDeliveryVO> getListDeliveryRecordForExport(EquipmentFilter<EquipmentRecordDeliveryVO> equipmentFilter) throws BusinessException {
		List<EquipmentRecordDeliveryVO> lstEquipDeliveryRecord = new ArrayList<EquipmentRecordDeliveryVO>();
		try {
		lstEquipDeliveryRecord = equipMngDAO.getListDeliveryRecordForExport(equipmentFilter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		return lstEquipDeliveryRecord;
	}

	@Override
	public EquipLend getEquipLendById(Long id) throws BusinessException {
		try {
			return equipMngDAO.getEquipLendById(id);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<EquipItemConfigDtl> getListEquipItemConfigDtlByFilter(EquipItemConfigDtlFilter filter) throws BusinessException {
		try {
			List<EquipItemConfigDtl> lst = equipMngDAO.getListEquipItemConfigDtlByFilter(filter);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipItemConfig> getListCheckExistCrossing(EquipItemConfigFilter filter) throws BusinessException {
		try {
			List<EquipItemConfig> lst = equipMngDAO.getListCheckExistCrossing(filter);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Integer countIntersectionCapacity(Integer fCapacity, Integer tCapacity, Integer status, Long[] longsException) throws BusinessException {
		try {
			return equipMngDAO.countIntersectionCapacity(fCapacity, tCapacity, status, longsException);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipItemConfig getEquipItemConfigByFromAndToCapacity(Integer fCapacity, Integer tCapacity, Integer status) throws BusinessException {
		try {
			return equipMngDAO.getEquipItemConfigByFromAndToCapacity(fCapacity, tCapacity, status);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createOrUpdateEquipItemConfigByExcel(EquipItemConfig equipItemConfig, List<EquipItemConfigDtl> lstEquipItemConfigDtl) throws BusinessException {
		try {
			if (equipItemConfig == null) {
				throw new BusinessException("equipItemConfig is null");
			}
			if (lstEquipItemConfigDtl == null) {
				throw new BusinessException("lstEquipItemConfigDtl is null");
			}
			Date changDate = commonDAO.getSysDate();
			if (equipItemConfig.getId() !=  null) {
				//Cap nhat
				commonDAO.updateEntity(equipItemConfig);
				//Xu ly voi danh sach chi tiet
				List<EquipItemConfigDtl> lstDatail = equipMngDAO.getListEquipItemConfigDtlByEquipItemConfig(equipItemConfig.getId(), ActiveType.RUNNING.getValue());
				for (EquipItemConfigDtl rowI: lstEquipItemConfigDtl) {
					rowI.setEquipItemConfig(equipItemConfig);
					rowI.setCreateDate(changDate);
					rowI.setCreateUser(equipItemConfig.getUpdateUser());
					if (lstDatail == null || lstDatail.isEmpty()) {
						commonDAO.createEntity(rowI);
					} else {
						boolean flag = false;
						for (EquipItemConfigDtl rowJ: lstDatail) {
							if (rowI.getEquipItem() != null && rowI.getEquipItem().getId() != null && rowJ.getEquipItem() != null && rowJ.getEquipItem().getId() != null && rowI.getEquipItem().getId().equals(rowJ.getEquipItem().getId())) {
								rowJ.setFromMaterialPrice(rowI.getFromMaterialPrice());
								rowJ.setFromWorkerPrice(rowI.getFromWorkerPrice());
								rowJ.setToMaterialPrice(rowI.getToMaterialPrice());
								rowJ.setToWorkerPrice(rowI.getToWorkerPrice());
								rowJ.setUpdateDate(changDate);
								rowJ.setUpdateUser(equipItemConfig.getUpdateUser());
								commonDAO.updateEntity(rowJ);
								flag = true;
								break;
							}
						}
						if (!flag) {
							//Thuc hien them moi
							commonDAO.createEntity(rowI);
						}
					}
				}
			} else {
				//Them moi
				equipItemConfig.setCode(equipMngDAO.getCodeEquipItemConfig());
				equipItemConfig = commonDAO.createEntity(equipItemConfig);
				//Them chi tiet
				for (EquipItemConfigDtl equipItemConfigDtl: lstEquipItemConfigDtl) {
					equipItemConfigDtl.setEquipItemConfig(equipItemConfig);
					equipItemConfigDtl.setCreateDate(changDate);
					equipItemConfigDtl.setCreateUser(equipItemConfig.getCreateUser());
					commonDAO.createEntity(equipItemConfigDtl);
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipmentDeliveryPrintVO> getListEquipDeliveryRecordForPrint(
			List<Long> lstId) throws BusinessException {
		try {
			List<EquipmentDeliveryPrintVO> lst = equipMngDAO.getListEquipDeliveryRecordForPrint(lstId);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipStockVO> getEquipStockCMSbyCode(EquipStockFilter filter)
			throws BusinessException {
		try {
			List<EquipStockVO> lst = equipMngDAO.getEquipStockCMSbyCode(filter);
			ObjectVO<EquipStockVO> objVO = new ObjectVO<EquipStockVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipmentVO> getListEquipmentPeriod(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			return equipMngDAO.getListEquipmentPeriod(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipPeriod getListEquipPeriodById(Long id) throws BusinessException {
		try {
			return id == null ? null : equipMngDAO.getListEquipPeriodById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> getListEquipmentPeriodChangeYear(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getListEquipmentPeriodChangeYear(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
	//		objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Equipment getEquipmentByIdAndBlock(EquipItemFilter filter)
			throws BusinessException {
		try {
			return equipMngDAO.getEquipmentByIdAndBlock(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipmentVO> getListCategoryCount(Long statisticRecord) throws BusinessException {
		try {
			return equipMngDAO.getListCategoryCount(statisticRecord);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkExistStockTranFormDtlByEquip(Long stockTranFormId, Long equipId) throws BusinessException {
		try {
			return equipMngDAO.checkExistStockTranFormDtlByEquip(stockTranFormId, equipId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipmentGroupVO> getListEquipGroupForFilter(EquipGFilter<EquipmentGroupVO> filter) throws BusinessException {
		try {
			return equipMngDAO.getListEquipGroupForFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipmentGroupVO> getListEquipGroupForFilterByRPT(EquipGFilter<EquipmentGroupVO> filter) throws BusinessException {
		try {
			return equipMngDAO.getListEquipGroupForFilterByRPT(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<EquipDeliveryRecDtl> getListDeliveryDetailByRecordId(
			Long equipDeliveryId) throws BusinessException {
		try {
			return equipMngDAO.getListDeliveryDetailByRecordId(equipDeliveryId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<EquipGroupProduct> getListEquipGroupProductByFilter(EquipmentGroupProductFilter<EquipGroupProduct> filter) throws BusinessException {
		try {
			List<EquipGroupProduct> lst = equipMngDAO.getListEquipGroupProductByFilter(filter);
			ObjectVO<EquipGroupProduct> objVO = new ObjectVO<EquipGroupProduct>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipmentVO> getListEquipmentStatisticPeriod(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipMngDAO.getListEquipmentStatisticPeriod(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public int checkFromDateToDateInEquipGroupProduct(Long equipGroupId, String productCode, String fDateStr, String tDateStr, int flag) throws BusinessException {
		try {
			return equipMngDAO.checkFromDateToDateInEquipGroupProduct(equipGroupId, productCode, fDateStr, tDateStr, flag);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void addProductForEqupGroupProduct(Long equipGroupId, String productCode, Date fromDate, Date toDate, Boolean flagUpdate, LogInfoVO logInfo) throws BusinessException {
		try {
			if (productCode == null) {
				throw new BusinessException("productCode is null");
			}
			if (fromDate == null) {
				throw new BusinessException("fromDate is null");
			}
			if (flagUpdate == null) {
				throw new BusinessException("flagUpdate is null");
			}
			EquipGroup equipGroup = commonDAO.getEntityById(EquipGroup.class, equipGroupId);
			if (equipGroup == null) {
				throw new BusinessException("equipGroup is null");
			}
			Product product = productDAO.getProductByCodeNotByStatus(productCode);
			if (product == null) {
				throw new BusinessException("product is null");
			} else if (product.getStatus() == null || (!ActiveType.RUNNING.getValue().equals(product.getStatus().getValue()) && !ActiveType.STOPPED.getValue().equals(product.getStatus().getValue()))){
				throw new BusinessException("product.status not in (0, 1)");
			}
			Date changDate = commonDAO.getSysDate();
			if (toDate == null) {
				//Xoa het cac dong san pham thiet bi co trong tuong lai co from_date > fromDate
				List<EquipGroupProduct> lstPrdGroupEquipNextSysDate = equipMngDAO.getPrdInEquipGroupPrdByFdateForDel(equipGroupId, productCode, fromDate, true);
				if (lstPrdGroupEquipNextSysDate != null && !lstPrdGroupEquipNextSysDate.isEmpty()) {
					for (EquipGroupProduct equipGroupProduct: lstPrdGroupEquipNextSysDate) {
						equipGroupProduct.setStatus(ActiveType.DELETED);
						equipGroupProduct.setUpdateDate(changDate);
						equipGroupProduct.setUpdateUser(logInfo.getStaffCode());
						commonDAO.updateEntity(equipGroupProduct);
					}
				}
			}
			
			if (flagUpdate) {
				EquipGroupProduct productGroupEquip = equipMngDAO.getPrdInEquipGroupPrdByFlagToDate(equipGroupId, productCode, fromDate, true);
				if (productGroupEquip == null) {
					throw new BusinessException("productGroupEquip is null");
				}
				productGroupEquip.setToDate(commonDAO.getDateByDateForNumberDay(fromDate, -1, true));
				productGroupEquip.setUpdateDate(changDate);
				productGroupEquip.setUpdateUser(logInfo.getStaffCode());
				commonDAO.updateEntity(productGroupEquip);
				
			}
			//Cap nhat lai danh sach giao va co from_date < fdate
			List<EquipGroupProduct> lstPrdGroupEquipUpdateTDate = equipMngDAO.getPrdInEquipGroupPrdByFdateForUpdateToDate(equipGroupId, productCode, fromDate);
			if (lstPrdGroupEquipUpdateTDate != null && !lstPrdGroupEquipUpdateTDate.isEmpty()) {
				for (EquipGroupProduct equipGroupProductUdp: lstPrdGroupEquipUpdateTDate) {
					equipGroupProductUdp.setToDate(commonDAO.getDateByDateForNumberDay(fromDate, -1, true));
					equipGroupProductUdp.setUpdateDate(changDate);
					equipGroupProductUdp.setUpdateUser(logInfo.getStaffCode());
					commonDAO.updateEntity(equipGroupProductUdp);
				}
			}
			
			//Them moi du lieu
			EquipGroupProduct productGroupEquipIst = new EquipGroupProduct();
			productGroupEquipIst.setEquipGroup(equipGroup);
			productGroupEquipIst.setFromDate(fromDate);
			productGroupEquipIst.setToDate(toDate);
			productGroupEquipIst.setProduct(product);
			productGroupEquipIst.setStatus(ActiveType.RUNNING);
			productGroupEquipIst.setCreateUser(logInfo.getStaffCode());
			productGroupEquipIst.setCreateDate(changDate);
			commonDAO.createEntity(productGroupEquipIst);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipmentDeliveryVO> getListEquipmentByRole(
			EquipmentFilter<EquipmentDeliveryVO> equipmentFilter)
			throws BusinessException {
		try {
			List<EquipmentDeliveryVO> lst = equipMngDAO.getListEquipmentByRole(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> objVO = new ObjectVO<EquipmentDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipStockVO> getListEquipStockVOByRole(
			EquipStockFilter filter) throws BusinessException {
		try {
			List<EquipStockVO> lst = equipMngDAO.getListEquipStockVOByRole(filter);
			ObjectVO<EquipStockVO> objVO = new ObjectVO<EquipStockVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public int countStockEquipForPermisionCSM(Long staffRootId, Long shopRootId, String stockCode) throws BusinessException {
		try {
			return equipMngDAO.countStockEquipForPermisionCSM(staffRootId, shopRootId, stockCode);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipSuggestEvictionDTL> getListSuggestDetailByRecordId(Long equipSuggestId, Integer status) throws BusinessException {
		try {
			return equipMngDAO.getListSuggestDetailByRecordId(equipSuggestId, status);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<EquipmentManagerVO> getListEquipmentVOByShopInCMS(EquipmentFilter<EquipmentManagerVO> filter) throws BusinessException {
		try {
			List<EquipmentManagerVO> lst = equipMngDAO.getListEquipmentVOByShopInCMS(filter);
			ObjectVO<EquipmentManagerVO> objVO = new ObjectVO<EquipmentManagerVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<EquipmentManagerVO> getListEquipmentVOByShopInCMSEx(EquipmentFilter<EquipmentManagerVO> filter) throws BusinessException {
		try {
			List<EquipmentManagerVO> lst = equipMngDAO.getListEquipmentVOByShopInCMSEx(filter);
			ObjectVO<EquipmentManagerVO> objVO = new ObjectVO<EquipmentManagerVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipPeriod> getListEquipPeriodByDate(Date d)
			throws BusinessException {
		try {
			return equipMngDAO.getListEquipPeriodByDate(d);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipCategory getEquipCategoryByCode(String code, ActiveType activeType) throws BusinessException {
		// TODO Auto-generated method stub		
		try {
			return equipMngDAO.getEquipCategoryByCode(code, activeType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateStatusEquipSugEvicDTL(EquipSuggestEvictionDTL equipSug) throws BusinessException {
		try {
			commonDAO.updateEntity(equipSug);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		
	}
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<EquipDeliveryRecord> exportListEquipDeliveryRecordExportByEquipLend(EquipProposalBorrowFilter<EquipBorrowVO> filter) throws BusinessException {
		// TODO Auto-generated method stub
		if (filter == null) {
			throw new IllegalArgumentException("filter must not be null");
		}
		try {
			List<EquipDeliveryRecord> lstEquipDeliveryRecords = null;
			Date sysdate = commonDAO.getSysDate();
			List<EquipLend> lstEquipLends = equipProposalBorrowDAO.getListEquipLendByFilter(filter);
			// list id bien ban de nghi muon da duyet
			List<Long> lstId = new ArrayList<Long>();
			// them thong tin danh sach bien ban de nghi muon		
			if (lstEquipLends != null) {
				for (int i = 0, sz = lstEquipLends.size(); i < sz; i++) {
					EquipLend equipLend = lstEquipLends.get(i);
					if (filter.getLstCustomerCode() == null) {						
						equipLend.setDeliveryStatus(StatusDeliveryEquip.EXPORTED.getValue());
						equipLend.setUpdateDate(sysdate);
						equipLend.setUpdateUser(filter.getUserName());
					}
					lstId.add(equipLend.getEquipLendId());
				}
			}
			filter.setLstId(null);
			filter.setLstObjectId(lstId);
			
			// danh sach chi tiet cua danh sach bien ban de nghi muon da duyet		
			List<EquipLendDetail> lstEquipLendDetails = equipProposalBorrowDAO.getListEquipLendDetailByFilter(filter);
			if(lstEquipLendDetails != null && lstEquipLendDetails.size() > 0){
				// loc ra theo tung bien ban de nghi
				List<EquipBorrowVO> lstEquipBorrowVOs = new ArrayList<EquipBorrowVO>();
				EquipBorrowVO equipBorrowVO = new EquipBorrowVO();
				equipBorrowVO.setLstEquipLendDetails(new ArrayList<EquipLendDetail>());
				equipBorrowVO.setId(lstEquipLendDetails.get(0).getEquipLend().getEquipLendId());				
				equipBorrowVO.getLstEquipLendDetails().add(lstEquipLendDetails.get(0));
				for (int i=1, sz = lstEquipLendDetails.size(); i < sz; i++) {
					EquipLendDetail equipLendDetail = lstEquipLendDetails.get(i);
					if(equipLendDetail.getEquipLend().getEquipLendId().equals(equipBorrowVO.getId())){
						equipBorrowVO.getLstEquipLendDetails().add(equipLendDetail);
					}else{
						lstEquipBorrowVOs.add(equipBorrowVO);
						// them moi
						equipBorrowVO = new EquipBorrowVO();
						equipBorrowVO.setLstEquipLendDetails(new ArrayList<EquipLendDetail>());
						equipBorrowVO.setId(equipLendDetail.getEquipLend().getEquipLendId());
						equipBorrowVO.getLstEquipLendDetails().add(equipLendDetail);
					}
				}
				lstEquipBorrowVOs.add(equipBorrowVO);
				// xuat bien ban giao nhan cho tung bien ban de nghi
				String code = "";
				for (int j = 0, szj = lstEquipBorrowVOs.size(); j < szj; j++) {
					Long cusTemp = -1L;					
					lstEquipLendDetails = lstEquipBorrowVOs.get(j).getLstEquipLendDetails();
					for (int i = 0, sz = lstEquipLendDetails.size(); i < sz; i++) {
						EquipLendDetail equipLendDetail = lstEquipLendDetails.get(i);
						if (!cusTemp.equals(equipLendDetail.getCustomer().getId())) {
							cusTemp = equipLendDetail.getCustomer().getId();
							if (filter.getLstCustomerId() == null || (filter.getLstCustomerId() != null && filter.getLstCustomerId().indexOf(cusTemp) != -1)) {
								EquipLend equipLend = equipLendDetail.getEquipLend();

								EquipDeliveryRecord equipDeliveryRecord = new EquipDeliveryRecord();
								equipDeliveryRecord.setCreateDate(sysdate);
								// ngay lap
								equipDeliveryRecord.setCreateFormDate(equipLendDetail.getEquipLend().getCreateFormDate());
								equipDeliveryRecord.setCreateUser(filter.getUserName());
								equipDeliveryRecord.setCustomer(equipLendDetail.getCustomer());
								equipDeliveryRecord.setDeliveryStatus(DeliveryType.NOTSEND.getValue());
								equipDeliveryRecord.setEquipLend(equipLend);
								// ki
								equipDeliveryRecord.setEquipPeriod(equipLend.getEquipPeriod());
								
								/**
								 * @author nhutnn
								 * @since 21/08/2015
								 * */
								EquipLender equipLender = commonDAO.getFirtInformationEquipLender(equipLendDetail.getCustomer().getShop().getId());
								if (equipLender != null) {									
									equipDeliveryRecord.setFromObjectName(equipLender.getName());
									equipDeliveryRecord.setFromObjectAddress(equipLender.getAddress());
									equipDeliveryRecord.setFromObjectTax(equipLender.getTaxNumber());
									equipDeliveryRecord.setFromObjectPhone(equipLender.getPhone());
									equipDeliveryRecord.setFromRepresentative(equipLender.getRepressentative());
									equipDeliveryRecord.setFromPosition(equipLender.getPosition());
									equipDeliveryRecord.setFromPage(equipLender.getBusinessLicense());
									equipDeliveryRecord.setFromPagePlace(equipLender.getBusinessPlace());
									equipDeliveryRecord.setFromPageDate(equipLender.getBusinessDate());
									equipDeliveryRecord.setFromFax(equipLender.getFax());
								}								
								
								equipDeliveryRecord.setRecordStatus(StatusRecordsEquip.DRAFT.getValue());
								equipDeliveryRecord.setStaff(equipLend.getStaff());
								equipDeliveryRecord.setToAddress(equipLendDetail.getCustomerAddress());
								equipDeliveryRecord.setToBusinessDate(equipLendDetail.getBusinessNoDate());
								equipDeliveryRecord.setToBusinessLincense(equipLendDetail.getBusinessNo());
								equipDeliveryRecord.setToBusinessPlace(equipLendDetail.getBusinessNoPlace());
								equipDeliveryRecord.setToIdNO(equipLendDetail.getIdNo());
								equipDeliveryRecord.setToIdNODate(equipLendDetail.getIdNoDate());
								equipDeliveryRecord.setToIdNOPlace(equipLendDetail.getIdNoPlace());
//								
								equipDeliveryRecord.setRelation(equipLendDetail.getRelation());
								equipDeliveryRecord.setAddress(equipLendDetail.getAddress());
								equipDeliveryRecord.setStreet(equipLendDetail.getStreet());
								equipDeliveryRecord.setWardName(equipLendDetail.getWardName());
								equipDeliveryRecord.setDistrictName(equipLendDetail.getDistrictName());
								equipDeliveryRecord.setProvinceName(equipLendDetail.getProvinceName());
								String t = equipLendDetail.getAddress();
								t += ", " + equipLendDetail.getStreet();
								t += ", " + equipLendDetail.getWardName();
								t += ", " + equipLendDetail.getDistrictName();
								t += ", " + equipLendDetail.getProvinceName();
								equipDeliveryRecord.setToObjectAddress(t);
								equipDeliveryRecord.setToObjectName(equipLendDetail.getCustomer().getShop().getShopName());
								equipDeliveryRecord.setToPhone(equipLendDetail.getPhone());
								//				equipDeliveryRecord.setToPosition(toPosition);
								equipDeliveryRecord.setToRepresentative(equipLendDetail.getRepresentative());
								equipDeliveryRecord.setToShop(equipLendDetail.getCustomer().getShop());

								if (StringUtility.isNullOrEmpty(code)) {
									code = equipmentRecordDeliveryDAO.getCodeRecordDelivery();
								} else {
									code = code.substring(code.length() - 8);
									code = "00000000" + (Long.parseLong(code.toString()) + 1);
									code = code.substring(code.length() - 8);
									code = "GN" + code;
								}
								equipDeliveryRecord.setCode(code);
								equipDeliveryRecord = this.createRecordDeliveryByEquipLend(equipDeliveryRecord);
								code = equipDeliveryRecord.getCode();
							}
						}
					}
				}
				// cap nhat danh sach bien ban de nghi muon
				for(int i=0, sz = lstEquipLends.size(); i < sz; i++){
					EquipLend equipLend = lstEquipLends.get(i);
					equipProposalBorrowDAO.updateEquipLend(equipLend);
				}
			}
			return lstEquipDeliveryRecords;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipmentRecordDeliveryVO> getListDeliveryRecordVOForEquipLendByFilter(EquipmentFilter<EquipmentRecordDeliveryVO> filter)throws BusinessException {		
		try {
			return equipmentRecordDeliveryDAO.getListRecordDeliveryVOForEquipLendByFilter(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipDeliveryRecord createRecordDeliveryByEquipLend(EquipDeliveryRecord eqDeliveryRecord) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			// luu bien ban
			Date sysDateIns = commonDAO.getSysDate();
			eqDeliveryRecord = equipmentRecordDeliveryDAO.createEquipmentRecordDelivery(eqDeliveryRecord);

			// luu lich su bien ban
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(sysDateIns);
			equipFormHistory.setCreateDate(sysDateIns);
			equipFormHistory.setCreateUser(eqDeliveryRecord.getCreateUser());
			equipFormHistory.setDeliveryStatus(eqDeliveryRecord.getDeliveryStatus());
			equipFormHistory.setRecordId(eqDeliveryRecord.getId());
			equipFormHistory.setRecordStatus(eqDeliveryRecord.getRecordStatus());
			equipFormHistory.setRecordType(EquipTradeType.DELIVERY.getValue());
			Staff staff = staffDAO.getStaffByCode(eqDeliveryRecord.getCreateUser());
			equipFormHistory.setStaff(staff);
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
			
			return eqDeliveryRecord;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipmentRecordDeliveryVO getEquipmentRecordDeliveryVOByFilter(EquipmentFilter<EquipmentRecordDeliveryVO> filter) throws BusinessException {
		try {
			List<EquipmentRecordDeliveryVO> lstEquipmentRecordDeliveryVOs = equipMngDAO.getEquipmentRecordDeliveryVOByFilter(filter);
			if (lstEquipmentRecordDeliveryVOs != null && lstEquipmentRecordDeliveryVOs.size() > 0) {
				return lstEquipmentRecordDeliveryVOs.get(0);
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		return null;
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<EquipEvictionForm> exportListEquipEvictionExportByEquipSuggestEviction(EquipSuggestEvictionFilter<EquipSuggestEviction> filter) throws BusinessException {
		// TODO Auto-generated method stub
		if (filter == null) {
			throw new IllegalArgumentException("filter must not be null");
		}
		try {
			List<EquipEvictionForm> lstEquipEvictionForms = new ArrayList<EquipEvictionForm>();
			Date sysdate = commonDAO.getSysDate();
			List<EquipSuggestEviction> lstEquipSuggestEvictions = equipSuggestEvictionDAO.getListEquipSuggestEvictionByFilter(filter);
			// list id bien ban de nghi muon da duyet
			List<Long> lstId = new ArrayList<Long>();
			// them thong tin danh sach bien ban de nghi muon		
			if (lstEquipSuggestEvictions != null) {
				for (int i = 0, sz = lstEquipSuggestEvictions.size(); i < sz; i++) {
					EquipSuggestEviction equipSuggestEviction = lstEquipSuggestEvictions.get(i);
					if (filter.getLstCustomerCode() == null) {						
						equipSuggestEviction.setDeliveryStatus(StatusDeliveryEquip.EXPORTED.getValue());
						equipSuggestEviction.setUpdateDate(sysdate);
						equipSuggestEviction.setUpdateUser(filter.getUserName());
					}
					lstId.add(equipSuggestEviction.getId());
				}
			}	
			
			// danh sach chi tiet cua danh sach bien ban de nghi thu hoi da duyet		
			EquipSuggestEvictionFilter<EquipSuggestEvictionDTL> filterDetail = new EquipSuggestEvictionFilter<EquipSuggestEvictionDTL>();
			filterDetail.setLstObjectId(lstId);
			List<EquipSuggestEvictionDTL> lstEquipSuggestEvictionDTLs = equipSuggestEvictionDAO.getListEquipSuggestEvictionDetailByFilter(filterDetail);
			if(lstEquipSuggestEvictionDTLs != null && lstEquipSuggestEvictionDTLs.size() > 0){
				// loc ra theo tung bien ban de nghi
				List<EquipmentSuggestEvictionVO> lstEquipmentSuggestEvictionVOs = new ArrayList<EquipmentSuggestEvictionVO>();
				EquipmentSuggestEvictionVO equipmentSuggestEvictionVO = new EquipmentSuggestEvictionVO();
				equipmentSuggestEvictionVO.setLstEquipSuggestEvictionDTLs(new ArrayList<EquipSuggestEvictionDTL>());
				equipmentSuggestEvictionVO.setId(lstEquipSuggestEvictionDTLs.get(0).getEquipSugEviction().getId());				
				equipmentSuggestEvictionVO.getLstEquipSuggestEvictionDTLs().add(lstEquipSuggestEvictionDTLs.get(0));
				for (int i=1, sz = lstEquipSuggestEvictionDTLs.size(); i < sz; i++) {
					EquipSuggestEvictionDTL equipSuggestEvictionDTL = lstEquipSuggestEvictionDTLs.get(i);
					if(equipSuggestEvictionDTL.getEquipSugEviction().getId().equals(equipmentSuggestEvictionVO.getId())){
						equipmentSuggestEvictionVO.getLstEquipSuggestEvictionDTLs().add(equipSuggestEvictionDTL);
					}else{
						lstEquipmentSuggestEvictionVOs.add(equipmentSuggestEvictionVO);
						// them moi
						equipmentSuggestEvictionVO = new EquipmentSuggestEvictionVO();
						equipmentSuggestEvictionVO.setLstEquipSuggestEvictionDTLs(new ArrayList<EquipSuggestEvictionDTL>());
						equipmentSuggestEvictionVO.setId(equipSuggestEvictionDTL.getEquipSugEviction().getId());				
						equipmentSuggestEvictionVO.getLstEquipSuggestEvictionDTLs().add(equipSuggestEvictionDTL);
					}
				}
				lstEquipmentSuggestEvictionVOs.add(equipmentSuggestEvictionVO);
				
				// xuat bien ban giao nhan cho tung bien ban de nghi
				String code = "";
				String fromShop = "";
				ApParamEquip fromShopLend = apParamEquipDAO.getApParamEquipByCodeX(ApParamEquipType.FROM_SHOP.getValue(), ApParamEquipType.EQUIP_EVICTION_FROM_SHOP_TYPE, ActiveType.RUNNING);
				if (fromShopLend != null) {
					fromShop = fromShopLend.getValue();
				}
				for (int j = 0, szj = lstEquipmentSuggestEvictionVOs.size(); j < szj; j++) {
					Long cusTemp = -1L;					
					List<Equipment> lstEquipments = new ArrayList<Equipment>();
					EquipEvictionForm equipEvictionForm = null;
					// xet chi tiet de luu bien ban thu hoi
					lstEquipSuggestEvictionDTLs = lstEquipmentSuggestEvictionVOs.get(j).getLstEquipSuggestEvictionDTLs();
					for (int i = 0, sz = lstEquipSuggestEvictionDTLs.size(); i < sz; i++) {
						EquipSuggestEvictionDTL equipSuggestEvictionDTL = lstEquipSuggestEvictionDTLs.get(i);
						if (filter.getLstCustomerId() == null || (filter.getLstCustomerId() != null && filter.getLstCustomerId().indexOf(cusTemp) != -1)) {
							if (!cusTemp.equals(equipSuggestEvictionDTL.getCustomer().getId())) {
								cusTemp = equipSuggestEvictionDTL.getCustomer().getId();

								// tao moi neu khach hang ke tiep khac khach hang ban dau
								if (equipEvictionForm != null && lstEquipments.size() > 0) {
									equipEvictionForm = this.createFormEvictionByEquipSuggestEviction(equipEvictionForm, lstEquipments);
									lstEquipEvictionForms.add(equipEvictionForm);
									lstEquipments = new ArrayList<Equipment>();
									equipEvictionForm = null;
								}
								equipEvictionForm = new EquipEvictionForm();
								EquipSuggestEviction equipSuggestEviction = equipSuggestEvictionDTL.getEquipSugEviction();
								equipEvictionForm.setEquipSugEviction(equipSuggestEviction);
								
								Customer customer = equipSuggestEvictionDTL.getCustomer();
								equipEvictionForm.setCustomer(customer);
								equipEvictionForm.setCustomerCode(customer.getShortCode());
								equipEvictionForm.setCustomerName(equipSuggestEvictionDTL.getCustomerName());
								equipEvictionForm.setPhone(equipSuggestEvictionDTL.getMobile());
															
								equipEvictionForm.setAddress(customer.getAddress());																
								equipEvictionForm.setShop(customer.getShop());
								equipEvictionForm.setCreateUser(filter.getUserName());
								equipEvictionForm.setFormStatus(StatusRecordsEquip.DRAFT.getValue());
								equipEvictionForm.setActionStatus(DeliveryType.NOTSEND.getValue());
								
								//equipEvictionForm.setReason(equipSuggestEvictionDTL.getSuggestReason());
								equipEvictionForm.setToRepresentator(fromShop);
								equipEvictionForm.setFromRepresentor(equipSuggestEvictionDTL.getRefresentative());
								equipEvictionForm.setCreateDate(sysdate);
								equipEvictionForm.setCreateFormDate(equipSuggestEviction.getCreateFormDate());
								//equipEvictionForm.setToStockId(kho.getId());
								//equipEvictionForm.setToStockType(EquipStockTotalType.KHO.getValue());
								//equipEvictionForm.setToStockName(kho.getName());
								equipEvictionForm.setEquipPriod(equipSuggestEviction.getEquipPriod());
								lstEquipments.add(equipSuggestEvictionDTL.getEquipment());

								if (StringUtility.isNullOrEmpty(code)) {
									code = equipmentRecordDeliveryDAO.getCodeEviction();
								} else {
									code = code.substring(code.length() - 8);
									code = "00000000" + (Long.parseLong(code.toString()) + 1);
									code = code.substring(code.length() - 8);
									code = "TH" + code;
								}
								equipEvictionForm.setCode(code);
								code = equipEvictionForm.getCode();
							} else {
								lstEquipments.add(equipSuggestEvictionDTL.getEquipment());
							}
							equipSuggestEvictionDTL.setDeliveryStatus(StatusDeliveryEquip.NOT_EXPORT.getValue());
							commonDAO.updateEntity(equipSuggestEvictionDTL);
						}
					}
					if (equipEvictionForm != null && lstEquipments.size() > 0) {
						equipEvictionForm = this.createFormEvictionByEquipSuggestEviction(equipEvictionForm, lstEquipments);
						lstEquipEvictionForms.add(equipEvictionForm);
					}
				}
				// cap nhat danh sach bien ban de nghi muon
				for(int i=0, sz = lstEquipSuggestEvictions.size(); i < sz; i++){
					EquipSuggestEviction equipSuggestEviction = lstEquipSuggestEvictions.get(i);
					commonDAO.updateEntity(equipSuggestEviction);;
				}
			}
			return lstEquipEvictionForms;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipmentEvictionVO> getListFormEvictionVOForEquipSuggestEvictionByFilter(EquipmentFilter<EquipmentEvictionVO> filter)throws BusinessException {		
		try {
			return equipRecordDAO.getListFormEvictionVOForEquipSuggestEvictionByFilter(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipEvictionForm createFormEvictionByEquipSuggestEviction(EquipEvictionForm equipEvictionForm, List<Equipment> lstEquipments) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			// luu bien ban
			Date sysDateIns = commonDAO.getSysDate();
			equipEvictionForm = equipMngDAO.createEquipEvictionForm(equipEvictionForm);
			
			// luu lich su bien ban
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(equipEvictionForm.getCreateDate());
			equipFormHistory.setCreateDate(sysDateIns);
			equipFormHistory.setCreateUser(equipEvictionForm.getCreateUser());
			equipFormHistory.setDeliveryStatus(equipEvictionForm.getActionStatus());
			equipFormHistory.setRecordId(equipEvictionForm.getId());
			equipFormHistory.setRecordStatus(equipEvictionForm.getFormStatus());
			equipFormHistory.setRecordType(EquipTradeType.WITHDRAWAL_LIQUIDATION.getValue());
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
					
			// luu chi tiet bien ban
			for (int i = 0; i < lstEquipments.size(); i++) {
				Equipment equipment = lstEquipments.get(i);
				EquipEvictionFormDtl detail = new EquipEvictionFormDtl();
				detail.setCreateDate(sysDateIns);
				detail.setCreateUser(equipEvictionForm.getCreateUser());
				detail.setEquip(equipment);
				/** tamvnm: luu id bien ban giao nhan */
				if (equipEvictionForm.getCustomer() != null) {
					List<Long> lstEquipId = new ArrayList<Long>();
					lstEquipId.add(equipment.getId());
					List<EquipmentExVO> lstequipVO = equipMngDAO.getListAllEquipmentDeliveryByCustomerId(equipEvictionForm.getCustomer().getId(), lstEquipId);
					if (lstequipVO != null && lstequipVO.size() > 0) {
						Long deliveryId = null;
						EquipDeliveryRecord deliveryRecord = null;
						for (int c = 0, csize = lstequipVO.size(); c < csize; c++) {
							if (lstequipVO.get(c).getId().equals(equipment.getId())) {
								deliveryId = lstequipVO.get(c).getEquipDeliveryId();
								break;
							}
						}
						if (deliveryId != null) {
							deliveryRecord = commonDAO.getEntityById(EquipDeliveryRecord.class, deliveryId);
						}

						if (deliveryRecord != null) {
							detail.setEquipDeliveryRecord(deliveryRecord);
						}
					}
				}

				detail.setSerial(equipment.getSerial());
				detail.setManufacturingYear(equipment.getManufacturingYear());
				detail.setHealthStatus(equipment.getHealthStatus() != null ? equipment.getHealthStatus() : null);
				if (equipment.getEquipGroup() != null) {
					detail.setEquipCategory(equipment.getEquipGroup().getEquipCategory());
					if (equipment.getEquipGroup().getEquipCategory() != null) {
						detail.setEquipCategoryName(equipment.getEquipGroup().getEquipCategory().getName());
					}
				}
				detail.setEquipGroup(equipment.getEquipGroup());
				if (equipment.getEquipGroup() != null) {
					detail.setEquipGroupName(equipment.getEquipGroup().getName());
					detail.setCapacityFrom(equipment.getEquipGroup().getCapacityFrom());
					detail.setCapacityTo(equipment.getEquipGroup().getCapacityTo());
				}
				detail.setEquipEvictionForm(equipEvictionForm);
				detail.setTotal(1);//mac dinh la 1
				equipMngDAO.createEquipEvictionFormDtl(detail);

				// cap nhat thiet bi
				equipment.setTradeType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
				equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());//dang giao dich
				equipment.setUpdateDate(sysDateIns);
				equipment.setUpdateUser(equipEvictionForm.getCreateUser());
				equipMngDAO.updateEquipment(equipment);

				// lich su thiet bi
				EquipHistory qHis = new EquipHistory();
				qHis.setCreateDate(sysDateIns);
				qHis.setCreateUser(equipEvictionForm.getCreateUser());
				qHis.setEquip(equipment);
				if (!StringUtility.isNullOrEmpty(equipment.getStockCode())) {
					//equipHistory.setEquipWarehouseCode(eq.getStockCode());
				}
				qHis.setHealthStatus(equipment.getHealthStatus());
				if (equipment.getStockId() != null) {
					qHis.setObjectId(equipment.getStockId());
				}
				if (equipment.getStockType() != null) {
					qHis.setObjectType(equipment.getStockType());
				}
				qHis.setFormId(equipEvictionForm.getId());
				qHis.setFormType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
				qHis.setObjectCode(equipment.getStockCode());
				qHis.setStockName(equipment.getStockName());
				qHis.setTableName(TableName.EQUIP_EVICTION_FORM_TABLE.getValue());
				qHis.setPriceActually(equipment.getPriceActually());
				qHis.setStatus(equipment.getStatus().getValue());
				qHis.setTradeStatus(equipment.getTradeStatus());
				qHis.setTradeType(equipment.getTradeType());
				qHis.setUsageStatus(equipment.getUsageStatus());
				equipMngDAO.createEquipHistory(qHis);
			}

			return equipEvictionForm;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
}

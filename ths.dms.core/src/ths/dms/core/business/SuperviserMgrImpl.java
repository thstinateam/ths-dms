package ths.dms.core.business;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ActionLog;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.Routing;
import ths.dms.core.entities.RoutingCustomer;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffPositionLog;
import ths.dms.core.entities.TrainingPlan;
import ths.dms.core.entities.TrainingPlanDetail;
import ths.dms.core.entities.VisitPlan;
import ths.dms.core.entities.enumtype.ActionLogObjectType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.TrainningPlanDetailStatus;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.filter.RoutingGeneralFilter;
import ths.dms.core.entities.filter.RptStaffSaleFilter;
import ths.dms.core.entities.filter.StaffFilter;
import ths.dms.core.entities.filter.SupFilter;
import ths.dms.core.entities.filter.VisitPlanFilter;
import ths.dms.core.entities.vo.ActionLogCustomerVO;
import ths.dms.core.entities.vo.ActionLogVO;
import ths.dms.core.entities.vo.AmountAndAmountPlanVO;
import ths.dms.core.entities.vo.AmountPlanCustomerVO;
import ths.dms.core.entities.vo.AmountPlanStaffVO;
import ths.dms.core.entities.vo.CustomerPositionVO;
import ths.dms.core.entities.vo.CustomerSaleVO;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.NVBHSaleVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.RoutingCustomerVO;
import ths.dms.core.entities.vo.RoutingTreeVO;
import ths.dms.core.entities.vo.RoutingVO;
import ths.dms.core.entities.vo.ShopSaleVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.entities.vo.StaffTrainingPlanVO;
import ths.dms.core.entities.vo.SuperVisorInfomationVO;
import ths.dms.core.entities.vo.SupervisorSaleVO;
import ths.dms.core.entities.vo.TrainingPlanVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.ConstantManager;
import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.MathUtil;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.ActionLogDAO;
import ths.dms.core.dao.ApParamDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.CustomerDAO;
import ths.dms.core.dao.MediaItemDAO;
import ths.dms.core.dao.RoutingCustomerDAO;
import ths.dms.core.dao.RoutingDAO;
import ths.dms.core.dao.RptStaffSaleDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.StaffDAO;
import ths.dms.core.dao.StaffPositionLogDAO;
import ths.dms.core.dao.TrainingPlanDAO;
import ths.dms.core.dao.TrainingPlanDetailDAO;
import ths.dms.core.dao.VisitPlanDAO;
import ths.dms.core.dao.repo.IRepository;

public class SuperviserMgrImpl implements SuperviserMgr {

	@Autowired
	StaffDAO staffDAO;

	@Autowired
	VisitPlanDAO visitPlanDAO;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	RoutingDAO routingDAO;

	@Autowired
	RoutingCustomerDAO routingCustomerDAO;

	@Autowired
	MediaItemDAO mediaItemDAO;

	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	TrainingPlanDAO trainingPlanDAO;

	@Autowired
	TrainingPlanDetailDAO trainingPlanDetailDAO;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	ActionLogDAO actionLogDAO;

	@Autowired
	StaffPositionLogDAO staffPositionLogDAO;

	@Autowired
	RptStaffSaleDAO rptStaffSaleDAO;
	
	@Autowired
	ApParamDAO apParamDAO;

	@Autowired
	private IRepository repo;
	
	@Override
	public List<Staff> getListStaffByOwnerId(Long ownerId) throws BusinessException {
		try {
			return staffDAO.getListStaffByOwnerId(ownerId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#getListVisitPlanByShopId(java
	 * .lang.Long)
	 */
	@Override
	public ObjectVO<RoutingVO> getListVisitPlanByCondition(KPaging<RoutingVO> kPaging, Long shopId, Long ownerId, ActiveType status, String customerCode, String staffCode, String routingCode, String routingName) throws BusinessException {

		ObjectVO<RoutingVO> objectVo = new ObjectVO<RoutingVO>();
		try {
			List<RoutingVO> listVisitPlan = visitPlanDAO.getListVisitPlanByShopId(kPaging, shopId, ownerId, status, customerCode, staffCode, routingCode, routingName);
			objectVo.setLstObject(listVisitPlan);
			objectVo.setkPaging(kPaging);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return objectVo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#getListVisitPlanByShopId(java
	 * .lang.Long)
	 */
	@Override
	public List<Routing> getListRoutingShopId(Long shopId) throws BusinessException {
		try {
			return visitPlanDAO.getListRoutingShopId(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RoutingTreeVO> getListRoutingShopIdNew(Long shopId) throws BusinessException {
		try {
			return visitPlanDAO.getListRoutingShopIdVO(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkRoutingErrDelete(Long routingId, Long shopId) throws BusinessException {
		try {
			return visitPlanDAO.checkRoutingErrDelete(routingId, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RoutingVO> exportTuyenBySelect(Long shopId, String routingCode, String routingName, String staffCode, Date sysdate, List<Long> lstRoutingId) throws BusinessException {
		try {
			return visitPlanDAO.exportTuyenBySelect(shopId, routingCode, routingName, staffCode, sysdate, lstRoutingId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#createRouting(java.lang.Long,
	 * java.lang.String, java.lang.String,
	 * ths.dms.core.entities.enumtype.ActiveType, java.util.List)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createRouting(Long shopId, String routingCode, String routingName, ActiveType status, List<RoutingCustomer> listRoutingCustomer) throws BusinessException {

		try {
			Shop shop = shopDAO.getShopById(shopId);
			Routing routing = new Routing();
			routing.setRoutingCode(routingCode);
			routing.setRoutingName(routingName);
			routing.setShop(shop);
			routing.setStatus(status);
			Routing insertRouting = routingDAO.createRouting(routing);
			for (RoutingCustomer routingCustomer : listRoutingCustomer) {
				routingCustomer.setRouting(insertRouting);
			}
			routingCustomerDAO.createRoutingCustomer(listRoutingCustomer);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#getListRoutingCustomer(com.viettel
	 * .core.entities.enumtype.KPaging, java.lang.Long,
	 * ths.dms.core.entities.enumtype.ActiveType)
	 */
	@Override
	public ObjectVO<RoutingCustomer> getListRoutingCustomer(KPaging<RoutingCustomer> kPaging, Long routingId, ActiveType status, Integer saleDate, Boolean hasOrderBySeq, Long shopId, Boolean hasChildShop, Long superId) throws BusinessException {

		ObjectVO<RoutingCustomer> objectVO = new ObjectVO<RoutingCustomer>();
		try {
			List<RoutingCustomer> listRoutingCustomer = routingCustomerDAO.getListRoutingCustomer(kPaging, routingId, status, saleDate, hasOrderBySeq, shopId, hasChildShop, superId);
			objectVO.setLstObject(listRoutingCustomer);
			objectVO.setkPaging(kPaging);
		} catch (Exception e) {

			throw new BusinessException(e);
		}
		return objectVO;
	}

	@Override
	public List<RoutingCustomerVO> getListRoutingCustomerNew(KPaging<RoutingCustomer> kPaging, Long routingId, ActiveType status, Integer saleDate, Boolean hasOrderBySeq, Long shopId, Boolean hasChildShop, Long superId) throws BusinessException {
		try {
			return routingCustomerDAO.getListRoutingCustomerNew(kPaging, routingId, status, saleDate, hasOrderBySeq, shopId, hasChildShop, superId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<RoutingCustomerVO> getListRoutingCustomerByCusOrRouting(Long routingId, Long shopId, Long customerId, String shortCode, String customerName, String address, Date startDate, Date endDate, Integer status) throws BusinessException {

		ObjectVO<RoutingCustomerVO> objectVO = new ObjectVO<RoutingCustomerVO>();
		try {
			List<RoutingCustomerVO> listRoutingCustomerVO = routingCustomerDAO.getListRoutingCustomerByCusOrRouting(routingId, shopId, customerId, shortCode, customerName, address, startDate, endDate, status);
			objectVO.setLstObject(listRoutingCustomerVO);
			objectVO.setkPaging(null);
		} catch (Exception e) {

			throw new BusinessException(e);
		}
		return objectVO;
	}

	@Override
	public List<RoutingCustomerVO> getListRouCusWitFDateMinByListCustomerID(List<Long> lstCustomerId, Long routeId) throws BusinessException {
		try {
			return routingCustomerDAO.getListRouCusWitFDateMinByListCustomerID(lstCustomerId, routeId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void insertListCusInRouCus(List<Long> lstCustomerId, RoutingCustomerFilter filter) throws BusinessException {
		try {
			boolean flag = false;
			Date sysDate = commonDAO.getDateBySysdateForNumberDay(0, true);
			//			Date backSysDate = commonDAO.getDateBySysdateForNumberDay(-1, true);
			Date nextSysDate = commonDAO.getDateBySysdateForNumberDay(1, true);
			Routing rou = new Routing();
			rou = routingDAO.getRoutingById(filter.getRoutingId());
			List<RoutingCustomerVO> getListRouCusNextDate = new ArrayList<RoutingCustomerVO>();
			List<Customer> lstCus = customerDAO.getListCustomerByListID(lstCustomerId);
			List<RoutingCustomer> lstData1 = new ArrayList<RoutingCustomer>();
			filter.setFlag(false);
			//Cap nhat endDate = sysDate voi tuyen va khach hang o thoi diem hien tai 
			filter.setFlag(true);
			lstData1 = routingCustomerDAO.getRoutingCustomerByFilterAndListID(lstCustomerId, null, filter);
			for (int i = 0; i < lstData1.size(); i++) {
				lstData1.get(i).setEndDate(sysDate);
				lstData1.get(i).setUpdateDate(new Date());
				lstData1.get(i).setUpdateUser(filter.getUserName());
				lstData1.get(i).setStatus(ActiveType.RUNNING);
				routingCustomerDAO.updateRoutingCustomer(lstData1.get(i));
			}

			lstData1 = new ArrayList<RoutingCustomer>();

			//Lay danh sach tuyen trong tuong lai voi danh sach khach hang
			getListRouCusNextDate = routingCustomerDAO.getListRouCusWitFDateMinByFlag(lstCustomerId, filter.getRoutingId(), filter.getShopId(), false);
			//			if(getListRouCusNextDate!=null && getListRouCusNextDate.size()>0){
			//Them khach hang vao tuyen
			for (int i = 0; i < lstCus.size(); i++) {
				flag = false;
				for (RoutingCustomerVO rouCus : getListRouCusNextDate) {
					if (lstCus.get(i).getId().equals(rouCus.getCustomerId())) {
						//Update 
						RoutingCustomer item = new RoutingCustomer();
						item.setStartDate(nextSysDate);
						item.setEndDate(commonDAO.getDateByDateForNumberDay(rouCus.getStartDate(), -1, true));
						item.setCustomer(lstCus.get(i));
						item.setRouting(rou);
						item.setCreateDate(new Date());
						item.setCreateUser(filter.getUserName());
						item.setStatus(ActiveType.RUNNING);
						item.setWeekInterval(1);
						routingCustomerDAO.createRoutingCustomer(item);
						flag = true;
						break;
					}
				}
				if (!flag) {
					RoutingCustomer item = new RoutingCustomer();
					item.setStartDate(nextSysDate);
					item.setCustomer(lstCus.get(i));
					item.setRouting(rou);
					item.setCreateDate(DateUtility.now(DateUtility.DATE_FORMAT_STR));
					item.setCreateUser(filter.getUserName());
					item.setStatus(ActiveType.RUNNING);
					item.setWeekInterval(1);
					routingCustomerDAO.createRoutingCustomer(item);
				}
			}

			//			}

		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<RoutingCustomer> getListRoutingCustomerListRouCusID(List<Long> lstID) throws BusinessException {
		try {
			return routingCustomerDAO.getListRoutingCustomerListRouCusID(lstID);

		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateRoutingCustomerByImport(RoutingCustomer rouCus, RoutingCustomerFilter filter) throws BusinessException {
		try {
			boolean flag = false;
			List<RoutingCustomer> lstData1 = new ArrayList<RoutingCustomer>();
			List<RoutingCustomerVO> getListRouCusNextDate = new ArrayList<RoutingCustomerVO>();
			filter.setFlag(false);
			List<Long> lstId = new ArrayList<Long>();
			lstId.add(rouCus.getCustomer().getId());
			//Cap nhat endDate = sysDate voi tuyen va khach hang o thoi diem hien tai 
			filter.setFlag(true);
			lstData1 = routingCustomerDAO.getRoutingCustomerByFilterAndListID(lstId, null, filter);
			for (int i = 0; i < lstData1.size(); i++) {
				lstData1.get(i).setEndDate(commonDAO.getDateBySysdateForNumberDay(0, true));
				lstData1.get(i).setUpdateDate(new Date());
				lstData1.get(i).setUpdateUser(filter.getUserName());
				lstData1.get(i).setStatus(ActiveType.RUNNING);
				routingCustomerDAO.updateRoutingCustomer(lstData1.get(i));
			}

			//Lay danh sach tuyen trong tuong lai voi danh sach khach hang
			getListRouCusNextDate = routingCustomerDAO.getListRouCusWitFDateMinByFlag(lstId, filter.getRoutingId(), filter.getShopId(), false);
			//			if(getListRouCusNextDate!=null && getListRouCusNextDate.size()>0){
			//Them khach hang vao tuyen
			flag = false;
			for (RoutingCustomerVO rouCusItem : getListRouCusNextDate) {
				if (rouCus.getCustomer().getId().equals(rouCusItem.getCustomerId())) {
					//Update 
					RoutingCustomer item = new RoutingCustomer();
					item.setStartDate(commonDAO.getDateBySysdateForNumberDay(1, true));
					item.setEndDate(commonDAO.getDateByDateForNumberDay(rouCus.getStartDate(), -1, true));
					item.setCustomer(rouCus.getCustomer());
					item.setRouting(rouCus.getRouting());
					item.setCreateDate(new Date());
					item.setCreateUser(filter.getUserName());
					item.setStatus(ActiveType.RUNNING);
					item.setWeekInterval(1);
					routingCustomerDAO.createRoutingCustomer(item);
					flag = true;
					break;
				}
			}
			if (!flag) {
				routingCustomerDAO.createRoutingCustomer(rouCus);
			}

		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createVisplanByStaffAndRouting(VisitPlan visitPlan) throws BusinessException {
		try {
			boolean flag = false;
			List<VisitPlan> lstData1 = new ArrayList<VisitPlan>();
			//Cap nhat endDate = sysDate voi tuyen va khach hang o thoi diem hien tai
			lstData1 = visitPlanDAO.getListVisitPlanInCurrent(null, visitPlan.getRouting().getId(), visitPlan.getShop().getId(), null);
			for (int i = 0; i < lstData1.size(); i++) {
				lstData1.get(i).setToDate(commonDAO.getDateBySysdateForNumberDay(0, true));
				lstData1.get(i).setUpdateDate(new Date());
				lstData1.get(i).setUpdateUser(visitPlan.getCreateUser());
				lstData1.get(i).setStatus(ActiveType.RUNNING);
				visitPlanDAO.updateVisitPlan(lstData1.get(i));
			}
			if (visitPlan.getToDate() != null) {
				//lay ngay startDate_min  so voi to Date
				VisitPlan visitPlanCurrent = visitPlanDAO.getVisitPlanWitFDateMin(visitPlan.getShop().getId());
				if (visitPlanCurrent != null && visitPlanCurrent.getFromDate() != null) {
					visitPlan.setToDate(visitPlanCurrent.getFromDate());
				}
			}
			//tao moi khach hang
			visitPlanDAO.createVisitPlan(visitPlan);

		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RoutingCustomer> getListRouCusErrByStartDate(Long customerId, Long routingId, Date fDate, Long shopId, Long rouCusId) throws BusinessException {
		try {
			return routingCustomerDAO.getListRouCusErrByStartDate(customerId, routingId, fDate, shopId, rouCusId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<VisitPlan> getListVisitPlanErrByStartDate(Long staffId, Long routingId, Date fDate, Long shopId) throws BusinessException {
		try {
			return visitPlanDAO.getListVisitPlanErrByStartDate(staffId, routingId, fDate, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RoutingCustomerVO> getListRusCusByEndateNextSysDate() throws BusinessException {
		try {
			return routingCustomerDAO.getListRusCusByEndateNextSysDate();
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Date getMinStartDateByFilter(RoutingCustomerFilter filter) throws BusinessException {
		try {
			List<RoutingCustomerVO> getListRouCusNextDate = new ArrayList<RoutingCustomerVO>();
			getListRouCusNextDate = routingCustomerDAO.getListRouCusWitFDateMinByFlag(null, null, filter.getShopId(), null);
			if (getListRouCusNextDate != null && getListRouCusNextDate.size() > 0) {
				return getListRouCusNextDate.get(0).getStartDate();
			}

		} catch (Exception e) {
			throw new BusinessException(e);
		}
		return filter.getToDate();
	}

	@Override
	public MediaItem getMediaItemById(Long id) throws BusinessException {
		try {
			if (id == null) {
				throw new IllegalArgumentException("id is null");
			}
			return mediaItemDAO.getMediaItemById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<MediaItem> getListMediaItemByObjectIdAndObjectType(Long objectId, MediaObjectType objectType) throws BusinessException {
		try {
			if (objectId == null) {
				throw new IllegalArgumentException("objectId is null");
			}
			return mediaItemDAO.getListMediaItemByObjectIdAndObjectType(null, objectId, objectType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#createRouting(ths.dms.core
	 * .entities.Routing)
	 */
	@Override
	public Routing createRouting(Routing routing) throws BusinessException {

		if (routing == null) {
			throw new IllegalArgumentException("routing is null");
		}
		try {
			return routingDAO.createRouting(routing);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Routing createRoutingImport(Routing routing) throws BusinessException {

		if (routing == null) {
			throw new IllegalArgumentException("routing is null");
		}
		try {
			return routingDAO.createRouting(routing);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#deleteRouting(ths.dms.core
	 * .entities.Routing)
	 */
	@Override
	public void deleteRouting(Routing routing) throws BusinessException {

		if (routing == null) {
			throw new IllegalArgumentException("routing is null");
		}
		try {
			routingDAO.deleteRouting(routing);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#updateRouting(ths.dms.core
	 * .entities.Routing)
	 */
	@Override
	public void updateRouting(Routing routing) throws BusinessException {

		if (routing == null) {
			throw new IllegalArgumentException("routing is null");
		}
		try {
			routingDAO.updateRouting(routing);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#getRoutingById(java.lang.Long)
	 */
	@Override
	public Routing getRoutingById(Long id) throws BusinessException {

		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		try {
			return routingDAO.getRoutingById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#createRoutingCustomer(com.viettel
	 * .core.entities.RoutingCustomer)
	 */
	@Override
	public RoutingCustomer createRoutingCustomer(RoutingCustomer routingCustomer) throws BusinessException {

		if (routingCustomer == null) {
			throw new IllegalArgumentException("routingCustomer is null");
		}
		try {
			return routingCustomerDAO.createRoutingCustomer(routingCustomer);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public RoutingCustomer createRoutingCustomerImport(RoutingCustomer routingCustomer) throws BusinessException {
		if (routingCustomer == null) {
			throw new IllegalArgumentException("routingCustomer is null");
		}
		try {
			return routingCustomerDAO.createRoutingCustomer(routingCustomer);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#deleteRoutingCustomer(com.viettel
	 * .core.entities.RoutingCustomer)
	 */
	@Override
	public void deleteRoutingCustomer(RoutingCustomer routingCustomer) throws BusinessException {

		if (routingCustomer == null) {
			throw new IllegalArgumentException("routingCustomer is null");
		}
		try {
			routingCustomerDAO.deleteRoutingCustomer(routingCustomer);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#updateRoutingCustomer(com.viettel
	 * .core.entities.RoutingCustomer)
	 */
	@Override
	public void updateRoutingCustomer(RoutingCustomer routingCustomer) throws BusinessException {

		if (routingCustomer == null) {
			throw new IllegalArgumentException("routingCustomer is null");
		}
		try {
			routingCustomerDAO.updateRoutingCustomer(routingCustomer);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#getRoutingCustomerById(java.lang
	 * .Long)
	 */
	@Override
	public RoutingCustomer getRoutingCustomerById(Long id) throws BusinessException {

		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		try {
			return routingCustomerDAO.getRoutingCustomerById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#createRoutingCustomer(java.util
	 * .List)
	 */
	@Override
	public void createRoutingCustomer(List<RoutingCustomer> listRoutingCustomer) throws BusinessException {

		if (listRoutingCustomer == null) {
			throw new IllegalArgumentException("listRoutingCustomer is null");
		}
		try {
			routingCustomerDAO.createRoutingCustomer(listRoutingCustomer);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#updateRounting(java.lang.Long,
	 * java.util.List, java.util.List, java.util.List)
	 */
	@Override
	@Transactional
	public void updateRouting(Routing routing, List<RoutingCustomer> listDelete, List<RoutingCustomer> listUpdate, List<RoutingCustomer> listInsert) throws BusinessException {

		try {
			if (routing == null) {
				throw new IllegalArgumentException("get routing from id is null");
			}
			routingDAO.updateRouting(routing);
			if (listDelete != null && listDelete.size() > 0) {
				for (RoutingCustomer routingCustomer : listDelete)
					routingCustomerDAO.deleteRoutingCustomer(routingCustomer);
			}
			if (listUpdate != null && listUpdate.size() > 0) {
				routingCustomerDAO.updateRoutingCustomer(listUpdate);
			}
			if (listInsert != null && listInsert.size() > 0) {
				StringBuilder listCustomer = new StringBuilder();
				listCustomer.append("(");
				for (int i = 0, length = listInsert.size(); i < length; i++) {
					RoutingCustomer routingCustomer = listInsert.get(i);
					listCustomer.append(routingCustomer.getCustomer().getId());
					if (i < length - 1) {
						listCustomer.append(",");
					}
					routingCustomer.setRouting(routing);
				}
				listCustomer.append(")");
				Boolean check = routingCustomerDAO.checkExit(routing.getId(), listCustomer.toString());
				if (check == true) {
					throw new IllegalArgumentException("exits rountingCustomer for rounting, not insert");
				}
				routingCustomerDAO.createRoutingCustomer(listInsert);
			}
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional
	public void updateRoutingCustomer(List<RoutingCustomer> listDelete, List<RoutingCustomer> listUpdate, List<RoutingCustomer> listInsert) throws BusinessException {
		try {
			if (listDelete != null && listDelete.size() > 0) {
				for (RoutingCustomer routingCustomer : listDelete) {
					routingCustomerDAO.deleteRoutingCustomer(routingCustomer);
				}
			}
			if (listUpdate != null && listUpdate.size() > 0) {
				for (RoutingCustomer routingCustomer : listUpdate) {
					routingCustomerDAO.updateRoutingCustomer(routingCustomer);
				}
			}
			if (listInsert != null && listInsert.size() > 0) {
				for (RoutingCustomer routingCustomer : listInsert) {
					routingCustomerDAO.createRoutingCustomer(routingCustomer);
				}
			}
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#getListVisitPlanByRoutingId(com
	 * .viettel.core.entities.enumtype.KPaging, java.lang.Long)
	 */
	@Override
	public ObjectVO<VisitPlan> getListVisitPlanByRoutingId(SupFilter<VisitPlan> filter) throws BusinessException {

		ObjectVO<VisitPlan> result = new ObjectVO<VisitPlan>();
		try {
			List<VisitPlan> listVisitPlan = visitPlanDAO.getListVisitPlanByRoutingId(filter);
			result.setkPaging(filter.getkPaging());
			result.setLstObject(listVisitPlan);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#createVisitPlan(ths.dms.core
	 * .entities.VisitPlan)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public VisitPlan createVisitPlan(VisitPlan visitPlan) throws BusinessException {

		try {
			if (visitPlan == null) {
				throw new IllegalArgumentException("visitPlan is null");
			}
			Date fromDate = visitPlan.getFromDate();
			Date toDate = visitPlan.getToDate();
			Long routingId = visitPlan.getRouting().getId();
			Long staffId = visitPlan.getStaff().getId();
			if (ActiveType.RUNNING.equals(visitPlan.getStatus())) {
				Boolean checkExit = visitPlanDAO.checkExitVisitPlan(routingId, 0L, staffId, fromDate, toDate);
				if (checkExit == true) {
					throw new IllegalArgumentException("update visitPlan is exits");
				}
			}
			VisitPlan result = visitPlanDAO.createVisitPlan(visitPlan);
			/*
			 * Boolean checkSP = visitPlanDAO.checkVisitPlanWithSP(routingId,
			 * fromDate, toDate); if (checkSP == true) { throw new
			 * IllegalArgumentException("update visitPlan is exits"); }
			 */
			return result;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public VisitPlan createVisitPlanSingle(VisitPlan visitPlan) throws BusinessException {
		try {
			return visitPlanDAO.createVisitPlan(visitPlan);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateVisitplanImportFile(VisitPlan visitPlan) throws BusinessException {
		try {
			//Xu ly du lieu ban dau != This
			List<VisitPlan> lstVisitPlan = visitPlanDAO.getListVisitPlanByImport(visitPlan, false);
			if (lstVisitPlan != null && lstVisitPlan.size() > 0) {
				for (VisitPlan item : lstVisitPlan) {
					//Kiem tra co nam trong hien tai
					if (item.getFromDate() != null && DateUtility.compareDateWithoutTime(DateUtility.parse(DateUtility.format(item.getFromDate())), DateUtility.now()) == -1) {
						if (item.getToDate() != null && DateUtility.compareDateWithoutTime(DateUtility.parse(DateUtility.format(item.getToDate())), visitPlan.getFromDate()) != -1) {
							item.setToDate(commonDAO.getDateByDateForNumberDay(visitPlan.getFromDate(), -1, true));
							item.setUpdateDate(visitPlan.getCreateDate());
							item.setUpdateUser(visitPlan.getCreateUser());
							visitPlanDAO.updateVisitPlan(item);
						} else if (item.getToDate() == null) {
							item.setToDate(commonDAO.getDateByDateForNumberDay(visitPlan.getFromDate(), -1, true));
							item.setUpdateDate(visitPlan.getCreateDate());
							item.setUpdateUser(visitPlan.getCreateUser());
							visitPlanDAO.updateVisitPlan(item);
						}
					} else if (item.getFromDate() != null) {
						item.setStatus(ActiveType.DELETED);
						item.setUpdateDate(visitPlan.getCreateDate());
						item.setUpdateUser(visitPlan.getCreateUser());
						visitPlanDAO.updateVisitPlan(item);
					}
				}
			}
			//Kiem tra Is This
			lstVisitPlan = visitPlanDAO.getListVisitPlanByImport(visitPlan, true);
			if (lstVisitPlan != null && lstVisitPlan.size() > 0) {
				for (VisitPlan item : lstVisitPlan) {
					//Kiem tra co nam trong hien tai
					if (item.getFromDate() != null && DateUtility.compareDateWithoutTime(DateUtility.parse(DateUtility.format(item.getFromDate())), DateUtility.now()) == -1) {
						if (item.getToDate() == null || 
							(item.getToDate() != null && DateUtility.compareDateWithoutTime(DateUtility.parse(DateUtility.format(item.getToDate())), visitPlan.getFromDate()) != -1)) {
							item.setToDate(commonDAO.getDateByDateForNumberDay(visitPlan.getFromDate(), -1, true));
							item.setUpdateDate(visitPlan.getCreateDate());
							item.setUpdateUser(visitPlan.getCreateUser());
							visitPlanDAO.updateVisitPlan(item);
						}
					} else if (item.getFromDate() != null) {
						item.setStatus(ActiveType.DELETED);
						item.setUpdateDate(visitPlan.getCreateDate());
						item.setUpdateUser(visitPlan.getCreateUser());
						visitPlanDAO.updateVisitPlan(item);
					}
				}
			}
			//Them moi visitplan
			visitPlanDAO.createVisitPlan(visitPlan);

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	private final String FROM_DATE_LESS_THAN = "FROM_DATE_LESS_THAN";
	private final String FROM_DATE_EQUAL = "FROM_DATE_EQUAL";

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateVisitplanByChangeTime(VisitPlan visitPlan, boolean flag, Boolean isAllowDelete) throws BusinessException {
		try {
			//Xu ly du lieu ban dau != This
			List<VisitPlan> lstVisitPlan = visitPlanDAO.getListVisitPlanByForm(visitPlan, false);
			if (lstVisitPlan != null && lstVisitPlan.size() > 0) {
				Boolean isShowConfirmDelete = false;
				for (VisitPlan item : lstVisitPlan) {
					//Kiem tra co nam trong hien tai
					if (!item.getId().equals(visitPlan.getId()) && item.getFromDate() != null) {
						if (DateUtility.compareDateWithoutTime(visitPlan.getFromDate(), DateUtility.parse(DateUtility.format(item.getFromDate()))) == 1) {
							if (item.getToDate() == null || DateUtility.compareDateWithoutTime(visitPlan.getFromDate(), DateUtility.parse(DateUtility.format(item.getToDate()))) == -1) {
								item.setToDate(commonDAO.getDateByDateForNumberDay(visitPlan.getFromDate(), -1, true));
							}
						} else if (DateUtility.compareDateWithoutTime(visitPlan.getFromDate(), DateUtility.parse(DateUtility.format(item.getFromDate()))) == -1) {
							if (visitPlan.getToDate() == null || DateUtility.compareDateWithoutTime(visitPlan.getToDate(), DateUtility.parse(DateUtility.format(item.getFromDate()))) > -1) {
								throw new BusinessException(FROM_DATE_LESS_THAN);
							}
						} else {
							if (Boolean.TRUE.equals(isAllowDelete)) {
								item.setStatus(ActiveType.DELETED);
							} else {
								isShowConfirmDelete = true;
								continue;
							}
						}
						
						item.setUpdateDate(visitPlan.getCreateDate());
						item.setUpdateUser(visitPlan.getCreateUser());
						visitPlanDAO.updateVisitPlan(item);
					}
				}
				if (isShowConfirmDelete) {
					throw new BusinessException(FROM_DATE_EQUAL);
				}
			}
			
			if (!flag) {//Cap nhat
				visitPlanDAO.updateVisitPlan(visitPlan);
			} else {//Them moi
				visitPlanDAO.createVisitPlan(visitPlan);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateRoutingCustomerImportFile(RoutingCustomer routingCus) throws BusinessException {
		try {
			//Update list Customer
			List<RoutingCustomer> lstRoucus = routingCustomerDAO.getListRoutingCustomerByImport(routingCus);
			if (lstRoucus != null && lstRoucus.size() > 0) {
				for (RoutingCustomer item : lstRoucus) {
					//Kiem tra co nam trong hien tai
					if (item.getStartDate() != null && DateUtility.compareDateWithoutTime(DateUtility.parse(DateUtility.format(item.getStartDate())), DateUtility.now()) == -1) {
						item.setEndDate(commonDAO.getDateByDateForNumberDay(routingCus.getStartDate(), -1, true));
						item.setUpdateDate(routingCus.getCreateDate());
						item.setUpdateUser(routingCus.getCreateUser());
						routingCustomerDAO.updateRoutingCustomer(item);
					} else if (item.getStartDate() != null) {
						item.setStatus(ActiveType.DELETED);
						item.setUpdateDate(routingCus.getCreateDate());
						item.setUpdateUser(routingCus.getCreateUser());
						routingCustomerDAO.updateRoutingCustomer(item);
					}
				}
			}
			//Them moi routing_custonmer
			routingCustomerDAO.createRoutingCustomer(routingCus);

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.business.SuperviserMgr#getVisitPlan(java.lang.Long,
	 * java.util.Date, java.util.Date)
	 */
	@Override
	public VisitPlan getVisitPlan(Long routingId, Date fromDate, Date toDate) throws BusinessException {

		try {
			return visitPlanDAO.getVisitPlan(routingId, fromDate, toDate);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public VisitPlan getVisitPlanMultiChoce(VisitPlanFilter filter) throws BusinessException {
		try {
			return visitPlanDAO.getVisitPlanMultiChoce(filter);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.business.SuperviserMgr#getListRouting()
	 */
	@Override
	public ObjectVO<Routing> getListRouting(KPaging<Routing> kPaging, Long parentShopId, Long shopId, Long superId, String routingCode, String routingName) throws BusinessException {

		try {
			ObjectVO<Routing> object = new ObjectVO<Routing>();
			List<Routing> list = routingDAO.getListRouting(kPaging, parentShopId, shopId, superId, routingCode, routingName);
			object.setLstObject(list);
			object.setkPaging(kPaging);
			return object;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#updateVisitPlan(ths.dms.core
	 * .entities.VisitPlan)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateVisitPlan(VisitPlan visitPlan) throws BusinessException {

		try {
			Date fromDate = visitPlan.getFromDate();
			Date toDate = visitPlan.getToDate();
			Long routingId = visitPlan.getRouting().getId();
			Long visitPlanId = visitPlan.getId();
			Long staffId = visitPlan.getStaff().getId();
			if (ActiveType.RUNNING.equals(visitPlan.getStatus())) {
				Boolean checkExit = visitPlanDAO.checkExitVisitPlan(routingId, visitPlanId, staffId, fromDate, toDate);
				if (checkExit == true) {
					throw new IllegalArgumentException("update visitPlan is exits");
				}
			}
			visitPlanDAO.updateVisitPlan(visitPlan);
			/*
			 * Boolean checkSP = visitPlanDAO.checkVisitPlanWithSP(routingId,
			 * fromDate, toDate); if (checkSP == true) { throw new
			 * IllegalArgumentException("update visitPlan is exits"); }
			 */
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateVisitPlanSingle(VisitPlan visitPlan) throws BusinessException {
		try {
			visitPlanDAO.updateVisitPlan(visitPlan);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#getVisitPlanById(java.lang.Long)
	 */
	@Override
	public VisitPlan getVisitPlanById(Long id) throws BusinessException {

		try {
			return visitPlanDAO.getVisitPlanById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#updateListRoutingCustomer(java
	 * .util.List)
	 */
	@Override
	public void updateListRoutingCustomer(List<RoutingCustomer> lstRoutingCustomer) throws BusinessException {

		try {
			routingCustomerDAO.updateRoutingCustomer(lstRoutingCustomer);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#updateListRoutingCustomer(java
	 * .util.List)
	 */
	@Override
	public void updateListRoutingCustomerByListShortCode(List<String> lstShortCode, List<Integer> lstSeq, Shop shop, Integer saleDate, String userName, Long routingId) throws BusinessException {
		try {
			List<RoutingCustomer> lstRoucus = routingCustomerDAO.getListRouCusByListShortCodeAndShopId(lstShortCode, shop.getId(), saleDate, routingId);
			if (lstRoucus != null && lstRoucus.size() > 0) {
				for (int i = 0; i < lstShortCode.size(); i++) {
					for (RoutingCustomer routingCustomer : lstRoucus) {
						if (lstShortCode.get(i).trim().toUpperCase().equals(routingCustomer.getCustomer().getShortCode())) {
							switch (saleDate) {
							case 1:
								routingCustomer.setSeq8(lstSeq.get(i));
								break;
							case 2:
								routingCustomer.setSeq2(lstSeq.get(i));
								break;
							case 3:
								routingCustomer.setSeq3(lstSeq.get(i));
								break;
							case 4:
								routingCustomer.setSeq4(lstSeq.get(i));
								break;
							case 5:
								routingCustomer.setSeq5(lstSeq.get(i));
								break;
							case 6:
								routingCustomer.setSeq6(lstSeq.get(i));
								break;
							case 7:
								routingCustomer.setSeq7(lstSeq.get(i));
								break;
							}
							routingCustomer.setUpdateDate(new Date());
							routingCustomer.setUpdateUser(userName);
							routingCustomerDAO.updateRoutingCustomer(routingCustomer);
						}
					}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#checkExitVisitPlan(java.lang.
	 * Long, java.lang.Long, java.lang.Long, java.util.Date, java.util.Date)
	 */
	@Override
	public Boolean checkExitVisitPlan(Long routingId, Long visitPlanId, Long staffId, Date fromDate, Date toDate) throws BusinessException {

		try {
			return visitPlanDAO.checkExitVisitPlan(routingId, visitPlanId, staffId, fromDate, toDate);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<VisitPlan> checkExitVisitPlanInShop(Long shopId, Long routingId, Long visitPlanId, Long staffId, Date fromDate, Date toDate) throws BusinessException {

		try {
			return visitPlanDAO.checkExitVisitPlanInShop(shopId, routingId, visitPlanId, staffId, fromDate, toDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#getRoutingByCode(java.lang.String
	 * )
	 */
	@Override
	public Routing getRoutingByCode(Long shopId, String code) throws BusinessException {

		try {
			return routingDAO.getRoutingByCode(shopId, code);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Routing getRoutingMultiChoice(Long routingId, Long shopId, String routingCode, Integer status) throws BusinessException {
		try {
			return routingDAO.getRoutingMultiChoice(routingId, shopId, routingCode, status);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#getListCustomerForOnlyShop(com
	 * .viettel.core.entities.enumtype.KPaging, java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public ObjectVO<Customer> getListCustomerForOnlyShop(KPaging<Customer> kPaging, String shortCode, String customerName, String shopCode, List<Long> lstExceptionCus) throws BusinessException {

		try {
			List<Customer> lst = customerDAO.getListCustomerForOnlyShop(kPaging, shortCode, customerName, shopCode, lstExceptionCus);
			ObjectVO<Customer> vo = new ObjectVO<Customer>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#createRoutingWithEntity(com.viettel
	 * .core.entities.Routing, java.util.List)
	 */
	@Override
	public void createRoutingWithEntity(Routing routing, List<RoutingCustomer> listRoutingCustomer) throws BusinessException {

		try {
			Routing insertRouting = routingDAO.createRouting(routing);
			for (RoutingCustomer routingCustomer : listRoutingCustomer) {
				routingCustomer.setRouting(insertRouting);
			}
			routingCustomerDAO.createRoutingCustomer(listRoutingCustomer);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Routing createRoutingWithEntityNew(Routing routing) throws BusinessException {
		try {
			return routingDAO.createRouting(routing);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#getListStaffByOwnerAndShop(java
	 * .lang.Long, java.lang.String)
	 */
	@Override
	public List<Staff> getListStaffByOwnerAndShop(Long ownerId, String shopCode) throws BusinessException {

		try {
			return staffDAO.getListStaffByOwnerId(null, ownerId, shopCode, null, null, null);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<Staff> getListNVBHInTuyen(Long gsnppId, Long shopId) throws BusinessException {
		try {
			return staffDAO.getListNVBHInTuyen(null, gsnppId, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Staff> getListNVBHInTuyenNew(Long gsnppId, Long shopId) throws BusinessException {
		try {
			return staffDAO.getListNVBHInTuyenNew(null, gsnppId, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SuperviserMgr#getListVisitPlanForManagerPlan
	 * (java.lang.Long, java.lang.Long, java.lang.Integer, java.lang.Integer,
	 * ths.dms.core.entities.enumtype.ActiveType)
	 */
	@Override
	public ObjectVO<VisitPlan> getListVisitPlanForManagerPlan(KPaging<VisitPlan> kPaging, Long shopId, Long staffId, Integer month, Integer year, ActiveType type) throws BusinessException {

		Date date = new Date();
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String stringDate = "";
			if (month < 10) {
				stringDate = year + "0" + month + "02";
			} else {
				stringDate = "" + year + month + "02";
			}
			date = sdf.parse(stringDate);
		} catch (ParseException e) {
			throw new BusinessException(e);
		}
		try {
			ObjectVO<VisitPlan> object = new ObjectVO<VisitPlan>();
			List<VisitPlan> list = visitPlanDAO.getListVisitPlanForManagerPlan(kPaging, shopId, staffId, date, type);
			object.setkPaging(kPaging);
			object.setLstObject(list);
			return object;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkExitStaffManagerBy(String shopCode, String superCode) throws BusinessException {

		try {
			return staffDAO.checkExitStaffManagerBy(shopCode, superCode);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Routing> getListRoutingBySuper(KPaging<Routing> kPaging, Long superId, String routingCode, String routingName) throws BusinessException {

		try {
			ObjectVO<Routing> object = new ObjectVO<Routing>();
			List<Routing> list = routingDAO.getListRoutingBySuper(kPaging, superId, routingCode, routingName);
			object.setLstObject(list);
			object.setkPaging(kPaging);
			return object;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ActionLog createActionLog(ActionLog actionLog, LogInfoVO logVO) throws BusinessException {
		try {
			return this.actionLogDAO.createActionLog(actionLog, logVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ActionLogVO> getListVisitPlanBySuperVisor(KPaging<ActionLogVO> kPaging, Long shopId, String staffCode, Date visitPlanDate, Boolean isHasChild) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			//			List<ActionLogVO> list = new ArrayList<ActionLogVO>();
			List<ActionLogVO> listVO = actionLogDAO.getListVisitPlanBySuperVisor(kPaging, shopId, staffCode, visitPlanDate, isHasChild);
			//			if (listVO != null && listVO.size() > 0) {
			//				ActionLogVO temp = null;
			//				String sCode = "";
			//				String sfCode = "";
			//				Integer numVisit = 0;
			//				Integer numLess2Minute = 0;
			//				Integer numOver30Minute = 0;
			//				Integer numCorrectRouting = 0;
			//				for (ActionLogVO alVO: listVO) {
			//					if (alVO.getShopCode().equals(sCode) && (alVO.getStaffCode().equals(sfCode))) {
			//						if (alVO.getObjectType() != null) {
			//							if(alVO.getObjectType().intValue() == 0 || alVO.getObjectType().intValue() == 1) {
			//								numVisit++;
			//								if(alVO.getVisitTime() != null) {
			//									if (alVO.getVisitTime().floatValue() < 2.0) {
			//										numLess2Minute++;
			//									} else if (alVO.getVisitTime().floatValue() > 30.0) {
			//										numOver30Minute++;
			//									}
			//								}
			//								if (alVO.getIsOr().intValue() == 0) {
			//									numCorrectRouting ++;
			//								}
			//							}
			//						}
			//					} else { //kiem tra khong trung nhau
			//						//check object_type
			//						if (temp != null) {
			//							temp.setNumVisit(numVisit);
			//							temp.setNumLess2Minute(numLess2Minute);
			//							temp.setNumOver30Minute(numOver30Minute);
			//							temp.setNumBetween2And30Minute(numVisit - numLess2Minute - numOver30Minute);
			//							temp.setNumCorrectRouting(numCorrectRouting);
			//							temp.setNumWrongRouting(numVisit - numCorrectRouting);
			//							list.add(temp);
			//							numVisit = 0;
			//							numLess2Minute = 0;
			//							numOver30Minute = 0;
			//							numCorrectRouting = 0;
			//						}
			//						temp = alVO;
			//						sCode = alVO.getShopCode();
			//						sfCode = alVO.getStaffCode();
			//						if (alVO.getObjectType() != null) {
			//							if(alVO.getObjectType().intValue() == 0 || alVO.getObjectType().intValue() == 1) {
			//								numVisit++;
			//								if(alVO.getVisitTime() != null) {
			//									if (alVO.getVisitTime().floatValue() < 2.0) {
			//										numLess2Minute++;
			//									} else if (alVO.getVisitTime().floatValue() > 30.0) {
			//										numOver30Minute++;
			//									}
			//								}
			//								if (alVO.getIsOr().intValue() == 0) {
			//									numCorrectRouting ++;
			//								}
			//							}
			//						}
			//					}
			//				}
			//				if (temp != null) {
			//					temp.setNumVisit(numVisit);
			//					temp.setNumLess2Minute(numLess2Minute);
			//					temp.setNumOver30Minute(numOver30Minute);
			//					temp.setNumBetween2And30Minute(numVisit - numLess2Minute - numOver30Minute);
			//					temp.setNumCorrectRouting(numCorrectRouting);
			//					temp.setNumWrongRouting(numVisit - numCorrectRouting);
			//					list.add(temp);
			//					numVisit = 0;
			//					numLess2Minute = 0;
			//					numOver30Minute = 0;
			//					numCorrectRouting = 0;
			//				}
			//			}
			ObjectVO<ActionLogVO> objectVO = new ObjectVO<ActionLogVO>();
			objectVO.setLstObject(listVO);
			objectVO.setkPaging(kPaging);
			return objectVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<CustomerPositionVO> getListCustomerPosition(KPaging<CustomerPositionVO> kPaging, Long staffId, Date visitPlanDate) throws BusinessException {
		try {
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(visitPlanDate);
			Integer day = cal1.get(Calendar.DAY_OF_WEEK);
			StaffPositionLog spl = staffPositionLogDAO.getLastStaffPositionLog(staffId, visitPlanDate, null, null);
			Double lat = Double.valueOf(0);
			Double lng = Double.valueOf(0);
			if (spl != null) {
				if (spl.getLat() != null) {
					lat = spl.getLat();
				}
				if (spl.getLng() != null) {
					lng = spl.getLng();
				}
			}
			List<CustomerPositionVO> listTmp = actionLogDAO.getListCustomerPosition(kPaging, staffId, visitPlanDate, day);
			List<CustomerPositionVO> list = new ArrayList<CustomerPositionVO>();
			String customerCode = "";
			for (CustomerPositionVO customerPositionVO : listTmp) {
				customerPositionVO.setLastStaffLat(lat);
				customerPositionVO.setLastStaffLng(lng);
				if (customerCode.equals(customerPositionVO.getCustomerCode())) {
					customerPositionVO.setType(3);
				} else {
					customerCode = customerPositionVO.getCustomerCode();
					if (customerPositionVO.getIsOr().equals(1)) {
						customerPositionVO.setType(0);
					} else {
						if (customerPositionVO.getObjectType() != null) {
							if (customerPositionVO.getObjectType().equals(0) || customerPositionVO.getObjectType().equals(1)) {
								if (customerPositionVO.getEndtime() != null) {
									customerPositionVO.setType(2);
								} else {
									customerPositionVO.setType(1);
								}
							}
						} else {
							customerPositionVO.setType(4);
						}
					}
				}
			}
			ObjectVO<CustomerPositionVO> objectVO = new ObjectVO<CustomerPositionVO>();
			objectVO.setLstObject(list);
			objectVO.setkPaging(kPaging);
			return objectVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<CustomerPositionVO> getListCustomerPositionEx(Long staffId, Date visitPlanDate) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(visitPlanDate);
			Integer day = cal1.get(Calendar.DAY_OF_WEEK);
			StaffPositionLog spl = staffPositionLogDAO.getLastStaffPositionLog(staffId, visitPlanDate, null, null);
			Double lat = Double.valueOf(0);
			Double lng = Double.valueOf(0);
			if (spl != null) {
				if (spl.getLat() != null) {
					lat = spl.getLat();
				}
				if (spl.getLng() != null) {
					lng = spl.getLng();
				}
			}
			List<CustomerPositionVO> listTmp = actionLogDAO.getListCustomerPosition(null, staffId, visitPlanDate, day);
			List<CustomerPositionVO> list = new ArrayList<CustomerPositionVO>();
			String customerCode = "";
			for (int i = 0; i < listTmp.size(); ++i) {
				CustomerPositionVO customerPositionVO = listTmp.get(i);
				customerPositionVO.setLastStaffLat(lat);
				customerPositionVO.setLastStaffLng(lng);
				if (customerCode.equals(customerPositionVO.getCustomerCode())) {
					//Ngoai tuyen hoac ghe tham co don hang
					if (customerPositionVO.getIsOr().equals(1)) {
						//						customerPositionVO.setType(0);
					} else {
						if (customerPositionVO.getObjectType().equals(1) || (customerPositionVO.getObjectType().equals(0) && (customerPositionVO.getEndtime() != null))) {
							//							customerPositionVO.setType(3);
							list.get(list.size() - 1).setType(3);
						} else if (customerPositionVO.getObjectType().equals(4) && list.get(list.size() - 1).getType().equals(2)) {
							list.get(list.size() - 1).setType(3);
						}
					}
				} else {
					customerCode = customerPositionVO.getCustomerCode();
					if (customerPositionVO.getIsOr().equals(1)) {
						customerPositionVO.setType(0);
					} else {
						if (customerPositionVO.getObjectType() != null) {
							if (customerPositionVO.getObjectType().equals(1) || (customerPositionVO.getObjectType().equals(0) && (customerPositionVO.getEndtime() != null))) {
								customerPositionVO.setType(2);
							}
							if (customerPositionVO.getObjectType().equals(0) && (customerPositionVO.getEndtime() == null)) {
								customerPositionVO.setType(1);
							}
						} else {
							customerPositionVO.setType(4);
						}
					}
					list.add(customerPositionVO);
				}
			}
			return list;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<SuperVisorInfomationVO> getInfomationOfParentSuper(Long parentStaffId) throws BusinessException {
		try {
			return staffPositionLogDAO.getInfomationOfParentSuper(parentStaffId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<StaffPositionVO> getListStaffPosition(Long ownerId, StaffObjectType objectType) throws BusinessException {
		try {
			return staffPositionLogDAO.getListStaffPosition(ownerId, objectType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<CustomerPositionVO> getListCustomerForStaff(Long staffId) throws BusinessException {
		try {
			return routingCustomerDAO.getListCustomerForStaff(staffId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<SuperVisorInfomationVO> getInfomationOfSuper(Long superStaffId) throws BusinessException {
		try {
			return staffPositionLogDAO.getInfomationOfSuper(superStaffId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<SuperVisorInfomationVO> getInfomationOfStaff(Long staffId) throws BusinessException {
		try {
			return staffPositionLogDAO.getInfomationOfStaff(staffId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<StaffPositionVO> getListSuperPosition(Long staffId) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return staffPositionLogDAO.getListSuperPosition(staffId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void offRouting(Long routingId, String updateUser) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			Date updateDate = commonDAO.getSysDate();
			//Lay tat ca cac Routing_Customer theo Routing_Id
			//			RoutingCustomerFilter filter = new RoutingCustomerFilter();
			//			filter.setRoutingId(routingId);
			//			List<RoutingCustomer> lstRoutingCus = routingCustomerDAO.getRoutingCustomerByFilter(filter);
			//			if (lstRoutingCus != null && !lstRoutingCus.isEmpty()) {
			//				for (RoutingCustomer value: lstRoutingCus){
			//					value.setUpdateUser(updateUser);
			//					value.setStatus(ActiveType.DELETED);
			//					routingCustomerDAO.updateRoutingCustomer(value);
			//				}
			//			}
			//routingCustomerDAO.offRoutingCustomerWithRoutingId(routingId, updateUser, updateDate);
			visitPlanDAO.offVisitPlanWithRoutingId(routingId, updateUser, updateDate);
			Routing routing = routingDAO.getRoutingById(routingId);
			routing.setUpdateDate(updateDate);
			routing.setStatus(ActiveType.DELETED);
			routing.setUpdateUser(updateUser);
			routingDAO.updateRouting(routing);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public AmountAndAmountPlanVO getAmountAndAmountPlanByStaff(Staff staff) throws BusinessException {
		try {
			if (null == staff) {
				throw new IllegalArgumentException("staff can not null");
			}
			//trungtm6 bo sung cau hinh
			ApParam ap = apParamDAO.getApParamByCodeX(ConstantManager.SYS_SALE_ROUTE, ApParamType.SYS_CONFIG, ActiveType.RUNNING);
			String type = ConstantManager.ONE_TEXT;
			if (ap != null && !StringUtility.isNullOrEmpty(ap.getValue())) {
				if (ConstantManager.TWO_TEXT.equals(ap.getValue().trim())) {
					type = ConstantManager.TWO_TEXT;
				}
			}
			return rptStaffSaleDAO.getAmountAndAmountPlanByStaff(staff, type);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public StaffPositionLog getNewestStaffPositionLog(Long staffId, Long shopId) throws BusinessException {
		try {
			return staffPositionLogDAO.getNewestStaffPositionLog(staffId, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RoutingCustomer getRoutingCustomerByRoutingIdAndCustomerId(Long routingId, Long customerId) throws BusinessException {
		try {
			return routingCustomerDAO.getRoutingCustomerByRoutingIdAndCustomerId(routingId, customerId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public CustomerVO getCustomerVisitting(Long staffId) throws BusinessException {
		if (null == staffId) {
			throw new IllegalArgumentException("staff is null");
		}
		try {
			return customerDAO.getCustomerVisitting(staffId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<SupervisorSaleVO> getListSupervisorSaleVO(KPaging<SupervisorSaleVO> kPaging, Long tbhvId, Long shopId) throws BusinessException {
		if (null == tbhvId) {
			throw new IllegalArgumentException("tbhvId can not null");
		}
		try {
			List<SupervisorSaleVO> lstSupervisorSaleVO = staffDAO.getListSupervisorSaleVO(kPaging, tbhvId, shopId);
			ObjectVO<SupervisorSaleVO> vo = new ObjectVO<SupervisorSaleVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lstSupervisorSaleVO);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<NVBHSaleVO> getListNVBHSaleVO(KPaging<NVBHSaleVO> kPaging, RptStaffSaleFilter filter) throws BusinessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter can not null");
		}
		try {
			List<NVBHSaleVO> lstNVBHSaleVO = rptStaffSaleDAO.getListNVBHSaleVO(kPaging, filter);
			ObjectVO<NVBHSaleVO> vo = new ObjectVO<NVBHSaleVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lstNVBHSaleVO);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ShopSaleVO> getListShopSaleVO(KPaging<ShopSaleVO> kPaging, RptStaffSaleFilter filter) throws BusinessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter can not null");
		}
		try {
			List<ShopSaleVO> lstResult = new ArrayList<ShopSaleVO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_REPORT_GS.GET_LIST_SHOP_SALE(?, :userId, :roleId, :shopId)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(filter.getStaffRootId());
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(filter.getRoleId());
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(filter.getShopId());
			params.add(java.sql.Types.NUMERIC);
			
			lstResult = repo.getListByQueryDynamicFromPackage(ShopSaleVO.class, sql.toString(), params, null);
			ObjectVO<ShopSaleVO> vo = new ObjectVO<ShopSaleVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lstResult);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CustomerSaleVO> getListCustomerSaleVO(KPaging<CustomerSaleVO> kPaging, Long nvbhId) throws BusinessException {
		try {
			if (null == nvbhId) {
				throw new IllegalArgumentException("nvbhId can not null");
			}
			//trungtm6 bo sung cau hinh
			ApParam ap = apParamDAO.getApParamByCodeX(ConstantManager.SYS_SALE_ROUTE, ApParamType.SYS_CONFIG, ActiveType.RUNNING);
			String type = ConstantManager.ONE_TEXT;
			if (ap != null && !StringUtility.isNullOrEmpty(ap.getValue())) {
				if (ConstantManager.TWO_TEXT.equals(ap.getValue().trim())) {
					type = ConstantManager.TWO_TEXT;
				}
			}
			List<CustomerSaleVO> listCustomerSaleVO = rptStaffSaleDAO.getListCustomerSaleVO(kPaging, nvbhId, type);
			ObjectVO<CustomerSaleVO> vo = new ObjectVO<CustomerSaleVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(listCustomerSaleVO);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public AmountPlanStaffVO getAmountPlanOfStaff(StaffObjectType type, Long staffId, Long shopId) throws BusinessException {
		if (staffId == null) {
			throw new IllegalArgumentException("STAFF_ID is null");
		}
		AmountPlanStaffVO obj = null;
		try {
			Staff staff = staffDAO.getStaffById(staffId);
			if (staff == null) {
				throw new IllegalArgumentException("STAFF is null");
			}
			obj = staffDAO.getAmountPlanOfStaff(type, staff, shopId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		return obj;
	}

	@Override
	public List<CustomerVO> getListCustomerBySaleStaffHasVisitPlan(Long staffId, Date checkDate) throws BusinessException {
		if (null == staffId) {
			throw new IllegalArgumentException("staff is null");
		}
		try {
			return customerDAO.getListCustomerBySaleStaffHasVisitPlan(staffId, checkDate);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/***************************** QUAN LY LICH HUAN LUYEN TBHV ****************************************/

	@Override
	public ObjectVO<NVBHSaleVO> getListTBHMSaleVO(KPaging<NVBHSaleVO> kPaging, Long gdmId, Long shopId) throws BusinessException {
		if (null == gdmId) {
			throw new IllegalArgumentException("gdmId can not null");
		}
		try {
			List<NVBHSaleVO> lstNVBHSaleVO = staffDAO.getListTBHMSaleVO(kPaging, gdmId, shopId);
			if (lstNVBHSaleVO != null && lstNVBHSaleVO.size() > 0) {
				for (NVBHSaleVO child : lstNVBHSaleVO) {
					if (child.getDhKH() == null) {
						child.setDhKH(0);
					}
					if (child.getDhTH() == null) {
						child.setDhKH(0);
					}
					if (child.getDsKH() == null) {
						child.setDsKH(BigDecimal.ZERO);
					}
					if (child.getDsTH() == null) {
						child.setDsTH(BigDecimal.ZERO);
					}
					if (child.getDsDuyet() == null) {
						child.setDsDuyet(BigDecimal.ZERO);
					}
					child.setDsConLai(child.getDsKH().subtract(child.getDsTH()));
					if (child.getDsConLai().compareTo(BigDecimal.ZERO) < 0) {
						child.setDsConLai(BigDecimal.ZERO);
					}
				}
			}
			ObjectVO<NVBHSaleVO> vo = new ObjectVO<NVBHSaleVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lstNVBHSaleVO);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Kiem tra shop co duoc quan ly boi NVGS
	 * 
	 * @author thongnm
	 */
	@Override
	public Boolean checkShopManagedByNVGS(Long gsnppId, Long shopId) throws BusinessException {
		try {
			return staffDAO.checkShopManagedByNVGS(gsnppId, shopId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<StaffPositionLog> showDirectionStaff(Long staffId) throws BusinessException {
		try {
			return staffDAO.showDirectionStaff(staffId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void changeTrainingPlanDetailById(Long id, Long staffId, LogInfoVO logInfo) throws BusinessException {
		// TODO Auto-generated method stub

	}

	@Override
	public void cancelTrainingPlanDetailBySuperCodeStaffCode(String superCode, String staffCode, Date date, String note, LogInfoVO logInfo) throws BusinessException {
		// TODO Auto-generated method stub

	}

	@Override
	public void createTrainingPlanDetailByOwnerIdAndStaffId(Long ownerId, Shop chooseShop, Long staffId, Date date, LogInfoVO logInfo) throws BusinessException {
		// TODO Auto-generated method stub

	}

	@Override
	public ObjectVO<Staff> getListSuperForTrainning(KPaging<Staff> kPaging, Long parentShopId, Date date) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<StaffTrainingPlanVO> getListTrainingForAreaManager(Long tbhvId, Date trainingDate) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer countAllTrainingPlanNVGSByTBHVId(Long tbhvId) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectVO<TrainingPlanVO> getListTrainingPlan(KPaging<TrainingPlanVO> kPaging, Long shopId, Long nvgsId, Date date, Long shopRootId, Long staffRootId, boolean flagChildCMS) throws BusinessException {
		try {
			if (kPaging != null && kPaging.getPageSize() == 0) {
				kPaging.setPageSize(10);
			}
			List<TrainingPlanVO> list = trainingPlanDAO.getListTrainingPlan(kPaging, shopId, nvgsId, date, shopRootId, staffRootId, flagChildCMS);
			ObjectVO<TrainingPlanVO> vo = new ObjectVO<TrainingPlanVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(list);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public TrainingPlan getTrainingPlan(String staffCode, Long shopId, Date date) throws BusinessException {

		try {
			return trainingPlanDAO.getTrainingPlanByOwnerCodeAndDate(staffCode, shopId, date);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createTrainingPlanDetailByOwnerIdAndStaffId(Shop shopNPP, Long ownerId, Long staffId, Date date) throws BusinessException {
		try {
			if (ownerId == null) {
				throw new IllegalArgumentException("ownerId is null");
			}
			if (staffId == null) {
				throw new IllegalArgumentException("staffId is null");
			}
			if (date == null) {
				throw new IllegalArgumentException("date is null");
			}
			// check staff is existed
			Staff owner = staffDAO.getStaffById(ownerId);
			if (owner == null) {
				throw new IllegalArgumentException("owner is null");
			}
			// check staff is existed
			Staff staff = staffDAO.getStaffById(staffId);
			if (staff == null) {
				throw new IllegalArgumentException("staff is null");
			}
			// get training plan
			TrainingPlan tp = trainingPlanDAO.getTrainingPlanByOwnerIdAndDate(ownerId, date);
			if (tp == null) {
				tp = new TrainingPlan();
				tp.setStaff(owner);
				//tp.setShop(owner.getShop());
				tp.setShop(shopNPP);
				Date tpDate = (Date) date.clone();
				tpDate.setDate(1);
				tp.setMonth(tpDate);
				tp.setStatus(ActiveType.RUNNING);
				tp.setCreateDate(DateUtility.now());
				tp = trainingPlanDAO.createTrainingPlan(tp);
			}
			// new training plan detail
			TrainingPlanDetail tpd = new TrainingPlanDetail();
			tpd.setShop(staff.getShop());
			tpd.setStaff(staff);
			tpd.setTrainingDate(date);
			//tpd.setStatus(TrainningPlanDetailStatus.NEW);
			tpd.setStatus(TrainningPlanDetailStatus.TRAINED);
			tpd.setCreateDate(DateUtility.now());
			tpd.setTrainingPlan(tp);
			trainingPlanDetailDAO.createTrainingPlanDetail(tpd);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public TrainingPlanDetail getTrainingPlanDetail(String superCode, Date date) throws BusinessException {
		try {
			return trainingPlanDetailDAO.getTrainingPlanDetailBy(superCode, null, date);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public TrainingPlanDetail createTrainingPlanDetail(TrainingPlanDetail trainingPlanDetail, LogInfoVO logInfo) throws BusinessException {
		try {
			return trainingPlanDetailDAO.createTrainingPlanDetail(trainingPlanDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteListTrainingPlanDetail(List<Long> lstTraningPlanDetailId, List<Long> lstTraningPlans) throws BusinessException {
		//check 
		try {
			lstTraningPlans = new ArrayList<Long>();
			if (lstTraningPlanDetailId == null) {
				throw new IllegalArgumentException("lstTraningPlanDetailId can not null");
			}
			for (int i = 0; i < lstTraningPlanDetailId.size(); i++) {
				Long id = lstTraningPlanDetailId.get(i);
				TrainingPlanDetail trainingPlanDetail = trainingPlanDetailDAO.getTrainingPlanDetailById(id);
				if (trainingPlanDetail != null) {
					if (!lstTraningPlans.contains(trainingPlanDetail.getTrainingPlan().getId()))
						lstTraningPlans.add(trainingPlanDetail.getTrainingPlan().getId());
					trainingPlanDetailDAO.deleteTrainingPlanDetail(trainingPlanDetail);
				}
				//Phải -1 đơn vị khi xoá thành công do transaction
				if (trainingPlanDAO.getCountTrainingPlanDetailById(id) - 1 == 0) {//trainingPlanDetail.getTrainingPlan().getId())==0){
					trainingPlanDAO.deleteTrainingPlanByIdDetail(id);
				}
			}
			for (int zindex = 0; zindex < lstTraningPlans.size(); ++zindex) {
				TrainingPlan trainingPlan = trainingPlanDAO.getTrainingPlanById(lstTraningPlans.get(zindex));
				if (trainingPlan != null && trainingPlanDetailDAO.countTrainingPlanDetail(trainingPlan.getId()) == 0) {
					trainingPlanDAO.deleteTrainingPlan(trainingPlan);
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public int countSEQBySeqAndRoutingId(Long routingId, Integer seq) throws BusinessException {
		try {
			return routingCustomerDAO.countSEQBySeqAndRoutingId(routingId, seq);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RoutingCustomer> getListRoutingCustomersBySeq(Long routingId, Integer seq) throws BusinessException {
		try {
			return routingCustomerDAO.getListRoutingCustomersBySeq(routingId, seq);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean countCustomerInRouCusValidImport(String shortCode, String routingCode, Long shopId) throws BusinessException {
		try {
			return routingCustomerDAO.countCustomerInRouCusValidImport(shortCode, routingCode, shopId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<RoutingVO> searchListRoutingVOByFilter(RoutingGeneralFilter<RoutingVO> filter) throws BusinessException {
		ObjectVO<RoutingVO> objectVo = new ObjectVO<RoutingVO>();
		try {
			List<RoutingVO> listVisitPlan = visitPlanDAO.searchListRoutingVOByFilter(filter);
			objectVo.setLstObject(listVisitPlan);
			objectVo.setkPaging(filter.getkPaging());
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return objectVo;
	}
	
	@Override
	public AmountAndAmountPlanVO getAmountAndAmountPlanByStaffSupervise(Staff staff, RptStaffSaleFilter filter) throws BusinessException {
		try {
			return rptStaffSaleDAO.getAmountAndAmountPlanByStaffSupervise(staff, filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public AmountPlanCustomerVO getAmountPlanCustomerByStaff(RptStaffSaleFilter filter) throws BusinessException {
		try {
			AmountPlanCustomerVO amountPlanCustomerVO = new AmountPlanCustomerVO();
			List<AmountPlanCustomerVO> lstAmountCustomerVO = rptStaffSaleDAO.getAmountPlanCustomerByStaff(filter);
			if (lstAmountCustomerVO != null && lstAmountCustomerVO.size() > 0) {
				amountPlanCustomerVO = lstAmountCustomerVO.get(0); 
				List<Integer> lstObjetType = new ArrayList<Integer>();
				lstObjetType.add(ActionLogObjectType.VISIT.getValue());
				lstObjetType.add(ActionLogObjectType.END_VISIT.getValue());
				filter.setLstObjetType(lstObjetType);
				filter.setObjetType(null);
				List<ActionLogCustomerVO> lstActionLogCustomerVOs = actionLogDAO.getActionLogCustomerOfStaff(filter);
				if (lstActionLogCustomerVOs != null && lstActionLogCustomerVOs.size() > 0) {
					amountPlanCustomerVO.setLstDetailActionLog(lstActionLogCustomerVOs);
				}
			}
			return amountPlanCustomerVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Double getDistanceOfStaff(StaffFilter filter) throws BusinessException {
		Double result = 0d;
		try {
			List<StaffPositionLog> list = staffDAO.showDirectionStaff(filter.getStaffId(), filter.getCheckDate(), 2000, null, null);
			result = this.calcNumKm(list);
		} catch (DataAccessException e) {

		}
		return result;
	}
	
	@Override
	public Double calcNumKm(List<StaffPositionLog> list) throws BusinessException {
		Double result = 0d;
		if (list != null && list.size() > 1) {
			for (int i = 0, size = list.size(); i < size - 1; i++) {
				StaffPositionLog row = list.get(i);
				StaffPositionLog nextRow = list.get(i + 1);
				try {
					result += MathUtil.distFrom(row.getLat(), row.getLng(), nextRow.getLat(), nextRow.getLng());
				} catch (Exception e) {

				}
			}
		}
		return result;
	}
}

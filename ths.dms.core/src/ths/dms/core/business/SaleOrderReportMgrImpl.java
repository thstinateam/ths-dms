package ths.dms.core.business;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.vo.ObjectVO;
import ths.core.entities.vo.rpt.RptCustomerProductSaleOrder1VO;
import ths.core.entities.vo.rpt.RptCustomerProductSaleOrderVO;
import ths.core.entities.vo.rpt.RptOrderDetailProduct1VO;
import ths.core.entities.vo.rpt.RptOrderDetailProduct2VO;
import ths.core.entities.vo.rpt.RptOrderDetailProduct3VO;
import ths.core.entities.vo.rpt.RptOrderDetailProduct4VO;
import ths.core.entities.vo.rpt.RptOrderDetailProductVO;
import ths.core.entities.vo.rpt.RptPayDetailDisplayProgr1ParentVO;
import ths.core.entities.vo.rpt.RptPayDetailDisplayProgr1VO;
import ths.core.entities.vo.rpt.RptPayDetailDisplayProgrVO;
import ths.core.entities.vo.rpt.RptProductDistrResult1VO;
import ths.core.entities.vo.rpt.RptProductDistrResult2VO;
import ths.core.entities.vo.rpt.RptProductDistrResultVO;
import ths.core.entities.vo.rpt.RptProductExchangeVO;
import ths.core.entities.vo.rpt.RptPromotionDetailStaff1VO;
import ths.core.entities.vo.rpt.RptPromotionDetailStaff2VO;
import ths.core.entities.vo.rpt.RptPromotionDetailStaffVO;
import ths.core.entities.vo.rpt.RptPromotionProgramDetail1VO;
import ths.core.entities.vo.rpt.RptPromotionProgramDetail2VO;
import ths.core.entities.vo.rpt.RptPromotionProgramDetailVO;
import ths.core.entities.vo.rpt.RptRealTotalSalePlanParentVO;
import ths.core.entities.vo.rpt.RptRealTotalSalePlanVO;
import ths.core.entities.vo.rpt.RptRevenueByDisplayProgramLevel;
import ths.core.entities.vo.rpt.RptRevenueInDate1VO;
import ths.core.entities.vo.rpt.RptRevenueInDate2VO;
import ths.core.entities.vo.rpt.RptRevenueInDateVO;
import ths.core.entities.vo.rpt.RptSaleOrderVO;
import ths.core.entities.vo.rpt.RptStaffProgramPromotionVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.PromotionProgramDAO;
import ths.dms.core.dao.SaleOrderDAO;
import ths.dms.core.dao.SaleOrderDetailDAO;
import ths.dms.core.dao.SalePlanDAO;
import ths.dms.core.dao.ShopDAO;

public class SaleOrderReportMgrImpl implements SaleOrderReportMgr {

	@Autowired
	SaleOrderDetailDAO saleOrderDetailDAO;
	
	@Autowired
	ShopDAO shopDAO;
	
	@Autowired
	SaleOrderDAO saleOrderDAO;
	
	@Autowired
	SalePlanDAO salePlanDAO;
		
	@Autowired
	PromotionProgramDAO promotionProgramDAO;

	@Override
	public RptOrderDetailProductVO getRptOrderDetailProductVO(Long shopId, String staffCode,
			Date fromDate, Date toDate) throws BusinessException {
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			if (fromDate == null) {
				throw new IllegalArgumentException("fromDate is null");
			}
			if (toDate == null) {
				throw new IllegalArgumentException("toDate is null");
			}
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			RptOrderDetailProductVO rptOrderDetailProductVO = new RptOrderDetailProductVO();
			rptOrderDetailProductVO.setShop(shop);
			List<RptOrderDetailProduct4VO> lst = saleOrderDetailDAO
					.getListRptOrderDetailProduct4VO(shopId, staffCode, fromDate,
							toDate);
			RptOrderDetailProduct1VO rptOrderDetailProduct1VO = null;
			RptOrderDetailProduct2VO rptOrderDetailProduct2VO = null;
			RptOrderDetailProduct3VO rptOrderDetailProduct3VO = null;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String productCode = "";
			String productName = "";
			BigDecimal price = new BigDecimal(-1);
			String orderDate = "";
			staffCode = "";
			String staffName = "";
			for (RptOrderDetailProduct4VO rptOrderDetailProduct4VO : lst) {
				if (!productCode.endsWith(rptOrderDetailProduct4VO.getProductCode())
						|| !(price.compareTo(rptOrderDetailProduct4VO.getPrice()) == 0)) {
					productCode = rptOrderDetailProduct4VO.getProductCode();
					productName = rptOrderDetailProduct4VO.getProductName();
					price = rptOrderDetailProduct4VO.getPrice();
					//
					rptOrderDetailProduct1VO = new RptOrderDetailProduct1VO();
					rptOrderDetailProduct1VO.setProductCode(productCode);
					rptOrderDetailProduct1VO.setProductName(productName);
					rptOrderDetailProduct1VO.setPrice(price);
					//
					rptOrderDetailProductVO.getLstRptOrderDetailProduct1VO()
							.add(rptOrderDetailProduct1VO);
					//
					orderDate = "";
				}
				String date = sdf.format(rptOrderDetailProduct4VO.getOrderDate());
				if (!orderDate.endsWith(date)) {
					orderDate = date;
					//
					rptOrderDetailProduct2VO = new RptOrderDetailProduct2VO();
					rptOrderDetailProduct2VO.setProductCode(productCode);
					rptOrderDetailProduct2VO.setProductName(productName);
					rptOrderDetailProduct2VO.setPrice(price);
					rptOrderDetailProduct2VO.setOrderDate(orderDate);
					//
					rptOrderDetailProduct1VO.getLstRptOrderDetailProduct1VO()
							.add(rptOrderDetailProduct2VO);
					//
					staffCode = "";
					staffName = "";
				}
				if (!staffCode.equals(rptOrderDetailProduct4VO.getStaffCode())) {
					staffCode = rptOrderDetailProduct4VO.getStaffCode();
					staffName = rptOrderDetailProduct4VO.getStaffName();
					//
					rptOrderDetailProduct3VO = new RptOrderDetailProduct3VO();
					rptOrderDetailProduct3VO.setProductCode(productCode);
					rptOrderDetailProduct3VO.setProductName(productName);
					rptOrderDetailProduct3VO.setPrice(price);
					rptOrderDetailProduct3VO.setOrderDate(orderDate);
					rptOrderDetailProduct3VO.setStaffCode(staffCode);
					rptOrderDetailProduct3VO.setStaffName(staffName);
					//
					rptOrderDetailProduct2VO.getLstRptOrderDetailProduct3VO()
							.add(rptOrderDetailProduct3VO);
					//
				}
				rptOrderDetailProduct3VO.getLstRptOrderDetailProduct4VO().add(
						rptOrderDetailProduct4VO);
			}
			return rptOrderDetailProductVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptCustomerProductSaleOrderVO getListRptCustomerProductSaleOrderVO(Long shopId, Long staffId,
			Date fromDate, Date toDate) throws BusinessException {
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			if (fromDate == null) {
				throw new IllegalArgumentException("fromDate is null");
			}
			if (toDate == null) {
				throw new IllegalArgumentException("toDate is null");
			}
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			RptCustomerProductSaleOrderVO rptCustomerProductSaleOrderVO = new RptCustomerProductSaleOrderVO();
			rptCustomerProductSaleOrderVO.setShop(shop);
			List<RptCustomerProductSaleOrder1VO> lst = saleOrderDetailDAO.getListRptCustomerProductSaleOrderVO(shopId, staffId,
					fromDate, toDate);
			for (RptCustomerProductSaleOrder1VO rptCustomerProductSaleOrder1VO : lst) {
				rptCustomerProductSaleOrderVO.getRptCustomerProductSaleOrder1VO().add(rptCustomerProductSaleOrder1VO);
			}
			return rptCustomerProductSaleOrderVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptPayDetailDisplayProgrVO getRptPayDetailDisplayProgrVO(Long shopId, String ppCode,
			Date fromDate, Date toDate) throws BusinessException {
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			if (ppCode == null) {
				throw new IllegalArgumentException("ppCode is null");
			}
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			RptPayDetailDisplayProgrVO rptPayDetailDisplayProgrVO = new RptPayDetailDisplayProgrVO();
			/*DisplayProgram dpProgram = displayProgramDAO.getDisplayProgramByCode(ppCode);
			rptPayDetailDisplayProgrVO.setDpProgram(dpProgram);
			rptPayDetailDisplayProgrVO.setShop(shop);
			List<RptPayDetailDisplayProgr1VO> lst = saleOrderDetailDAO.getListRptPayDetailDisplayProgrVO(shopId, ppCode, fromDate, toDate);
			for (RptPayDetailDisplayProgr1VO rptPayDetailDisplayProgr1VO : lst) {
				rptPayDetailDisplayProgrVO.getLstRptPayDetailDisplayProgr1VO().add(rptPayDetailDisplayProgr1VO);
			}*/
			return rptPayDetailDisplayProgrVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptPromotionDetailStaffVO getRptPromotionDetailStaffVO(Long shopId, String staffCode,
			Date fromDate, Date toDate) throws BusinessException {
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			if (fromDate == null) {
				throw new IllegalArgumentException("fromDate is null");
			}
			if (toDate == null) {
				throw new IllegalArgumentException("toDate is null");
			}
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			RptPromotionDetailStaffVO rptPromotionDetailStaffVO = new RptPromotionDetailStaffVO();
			rptPromotionDetailStaffVO.setShop(shop);
			List<RptPromotionDetailStaff2VO> lst = saleOrderDetailDAO
					.getListRptPromotionDetailStaffV2O(shopId, staffCode,
							fromDate, toDate, true);
			RptPromotionDetailStaff1VO rptPromotionDetailStaff1VO = null;
			String promotionProgramCode = "";
			String promotionProgramName = "";
			for (RptPromotionDetailStaff2VO rptPromotionDetailStaff2VO : lst) {
				if (!promotionProgramCode.equals(rptPromotionDetailStaff2VO.getPromotionProgramCode())) {
					promotionProgramCode = rptPromotionDetailStaff2VO.getPromotionProgramCode();
					promotionProgramName = rptPromotionDetailStaff2VO.getPromotionProgramName();
					//
					rptPromotionDetailStaff1VO = new RptPromotionDetailStaff1VO();
					rptPromotionDetailStaff1VO.setPromotionProgramCode(promotionProgramCode);
					rptPromotionDetailStaff1VO.setPromotionProgramName(promotionProgramName);
					//
					rptPromotionDetailStaffVO.getLstRptPromotionDetailStaff1VO().add(rptPromotionDetailStaff1VO);
				}
				if (rptPromotionDetailStaff2VO.getStaffCode() != null) {
					rptPromotionDetailStaff1VO.getLstRptPromotionDetailStaff2VO().add(rptPromotionDetailStaff2VO);
				}
			}
			return rptPromotionDetailStaffVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	
	/** Lay danh sach sale_order
	 * @author hungnm
	 */
	@Override
	public ObjectVO<RptSaleOrderVO> getListRptSaleOrderVO(KPaging<RptSaleOrderVO> kPaging,
			Long shopId, Long deliveryStaffId, Date fromDate, Date toDate, Boolean getDeliveryStaff)
				throws BusinessException {
		try {
			ObjectVO<RptSaleOrderVO> vo = new ObjectVO<RptSaleOrderVO>();
			List<RptSaleOrderVO> lst = saleOrderDAO.getListRptSaleOrderVO(kPaging, shopId, deliveryStaffId, fromDate, toDate, getDeliveryStaff);
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		}
		catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	
	
	/**
	 * 
	 * @author hungnm
	 */
	@Override
	public List<RptRevenueInDate2VO> getListRptRptRevenueInDate2VO(Date fromDate, Date toDate, 
			Long staffId, Long shopId) throws BusinessException {
		try {
			List<RptRevenueInDateVO> lst = saleOrderDAO.getListRptRevenueInDateVO(null, fromDate, toDate, staffId, shopId);
			List<RptRevenueInDate2VO> lst2 = new ArrayList<RptRevenueInDate2VO>();
			
			if (lst.size() > 0) {
				RptRevenueInDateVO item = lst.get(0);
				
				RptRevenueInDate2VO item2 = new RptRevenueInDate2VO();
				RptRevenueInDate1VO item1 = new RptRevenueInDate1VO();
				item1.setOrderDate(item.getOrderDate());
				item1.setLstRptRevenueInDateVO(Arrays.asList(item));
				
				item2.setStaffCode(item.getStaffCode());
				item2.setLstRptRevenueInDate1VO(Arrays.asList(item1));
				
				lst2.add(item2);
				lst.remove(0);
			}
			
			for (RptRevenueInDateVO item: lst) {
				
				RptRevenueInDate2VO item2 = lst2.get(lst2.size() - 1);
				if (item2.getStaffCode().equals(item.getStaffCode())) {
					//dua vao item2
					List<RptRevenueInDate1VO> lstItem1 = item2.getLstRptRevenueInDate1VO();
					RptRevenueInDate1VO item1 = lstItem1.get(lstItem1.size() - 1);
					if (item1.getOrderDate().equals(item.getOrderDate())) {
						List<RptRevenueInDateVO> lstItem = item1.getLstRptRevenueInDateVO();
						List<RptRevenueInDateVO> lstNewItem = new ArrayList<RptRevenueInDateVO>();
						lstNewItem.addAll(lstItem);
						lstNewItem.add(item);
						item1.setLstRptRevenueInDateVO(lstNewItem);
//						List<RptPoStatusTracking1VO> lstNewItem1 = new ArrayList<RptPoStatusTracking1VO>();
//						lstNewItem1.addAll(lstItem1);
//						lstNewItem1.add(item1);
//						item2.setLstRptPoStatusTracking1VO(lstNewItem1);
					}
					else {
						item1 = new RptRevenueInDate1VO();
						item1.setOrderDate(item.getOrderDate());
						item1.setLstRptRevenueInDateVO(Arrays.asList(item));
								
						List<RptRevenueInDate1VO> lstNewItem1 = new ArrayList<RptRevenueInDate1VO>();
						lstNewItem1.addAll(lstItem1);
						lstNewItem1.add(item1);
						item2.setLstRptRevenueInDate1VO(lstNewItem1);
					}
				}
				else {
					RptRevenueInDate1VO newItem1 = new RptRevenueInDate1VO();
					newItem1.setOrderDate(item.getOrderDate());
					newItem1.setLstRptRevenueInDateVO(Arrays.asList(item));
					
					RptRevenueInDate2VO newItem2 = new RptRevenueInDate2VO();
					newItem2.setStaffCode(item.getStaffCode());
					newItem2.setLstRptRevenueInDate1VO(Arrays.asList(newItem1));
					lst2.add(newItem2);
				}
				
			}
			
			return lst2;
		}
		catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptProductDistrResultVO getRptProductDistrResultVO(Long shopId, String staffCode,
			Date fromDate, Date toDate) throws BusinessException {
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			if (fromDate == null) {
				throw new IllegalArgumentException("fromDate is null");
			}
			if (toDate == null) {
				throw new IllegalArgumentException("toDate is null");
			}
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			RptProductDistrResultVO rptProductDistrResultVO = new RptProductDistrResultVO();
			rptProductDistrResultVO.setShop(shop);
			List<RptProductDistrResult2VO> lst = saleOrderDetailDAO.getListRptProductDistrResult2VO(shopId, staffCode, fromDate, toDate);
			RptProductDistrResult1VO rptProductDistrResult1VO = null;
			staffCode = "";
			String staffName = "";
			for (RptProductDistrResult2VO rptProductDistrResult2VO : lst) {
				if (!staffCode.equals(rptProductDistrResult2VO.getStaffCode())) {
					staffCode = rptProductDistrResult2VO.getStaffCode();
					staffName = rptProductDistrResult2VO.getStaffName();
					//
					rptProductDistrResult1VO = new RptProductDistrResult1VO();
					rptProductDistrResult1VO.setStaffCode(staffCode);
					rptProductDistrResult1VO.setStaffName(staffName);
					//
					rptProductDistrResultVO.getLstRptProductDistrResult2VO().add(rptProductDistrResult1VO);
				}
				rptProductDistrResult1VO.getLstRptProductDistrResult2VO().add(rptProductDistrResult2VO);
			}
			return rptProductDistrResultVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}


	/* (non-Javadoc)
	 * @see ths.dms.core.business.SaleOrderReportMgr#getListRptReturnSaleOrder(java.lang.Long, ths.dms.core.entities.enumtype.OrderType, java.util.Date, java.util.Date, java.lang.Long)
	 */
	@Override
	public List<SaleOrder> getListRptReturnSaleOrder(Long shopId,
			OrderType orderType, Date fromDate, Date toDate, Long deliveryId)
			throws BusinessException {

		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		try {
			return saleOrderDAO.getListRptReturnSaleOrder(null, shopId,
					orderType, null, null, fromDate, toDate, deliveryId);
		} catch (Exception e) {

			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.SaleOrderReportMgr#getListRealTotalSalePlan(java.lang.Long, java.util.Date, java.util.Date, java.lang.String)
	 */
	@Override
	public List<RptRealTotalSalePlanParentVO> getListRealTotalSalePlan(
			Long shopId, Integer month, Integer year, String staffCode)
			throws BusinessException {
		// TODO Auto-generated method stub
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (month == null) {
			throw new IllegalArgumentException("month is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}
		List<RptRealTotalSalePlanParentVO> listResult = new ArrayList<RptRealTotalSalePlanParentVO>();
		try {
			List<RptRealTotalSalePlanVO> list = salePlanDAO.getListRptRealTotalSalePlan(shopId, month, year, staffCode);
			//thuc hien tao parent VO
			String staff_code = "";
			RptRealTotalSalePlanParentVO parent = new RptRealTotalSalePlanParentVO();
			for (RptRealTotalSalePlanVO item: list) {
				if (item.getStaffCode().equals(staff_code)) {
					if (parent.getListRealPlan() != null) {
						parent.getListRealPlan().add(item);
					}
				} else {
					if (!staff_code.equals("")) {
						listResult.add(parent);
						parent = new RptRealTotalSalePlanParentVO();
					}
					staff_code = item.getStaffCode();
					parent.setStaffCode(staff_code);
					parent.setStaffName(item.getStaffName());
					parent.getListRealPlan().add(item);
				}
			}
			if (!staff_code.equals("")) {
				listResult.add(parent);
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e);
		}
		return listResult;
	}
	
	@Override
	public List<RptRevenueByDisplayProgramLevel> getListRptRevenueByDisplayProgramLevel(
			KPaging<RptRevenueByDisplayProgramLevel> kPaging,
			Long displayProgramId, Long customerId, Long shopId, Integer year)
			throws BusinessException {
		try {
			return null;//displayProgramDAO.getListRptRevenueByDisplayProgramLevel(kPaging, displayProgramId, customerId, shopId, year);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	public List<RptPromotionProgramDetail2VO> getListRptPromotionProgramDetail(
			KPaging<RptPromotionProgramDetailVO> kPaging, Date fromDate,
			Date toDate, Long shopId, Long staffId)
			throws BusinessException {
		try {
			List<RptPromotionProgramDetailVO> lst = promotionProgramDAO.getListRptPromotionProgramDetail(kPaging, fromDate, toDate, shopId, staffId);
			List<RptPromotionProgramDetail2VO> lst2 = new ArrayList<RptPromotionProgramDetail2VO>();
			
			if (lst.size() > 0) {
				RptPromotionProgramDetailVO item = lst.get(0);
				
				RptPromotionProgramDetail2VO item2 = new RptPromotionProgramDetail2VO();
				RptPromotionProgramDetail1VO item1 = new RptPromotionProgramDetail1VO();
				item1.setStaffCode(item.getStaffCode());
				item1.setLstRptPromotionProgramDetailVO(Arrays.asList(item));
				
				item2.setPromotionCode(item.getPromotionProgramCode());
				item2.setLstRptPromotionProgramDetail1VO(Arrays.asList(item1));
				
				lst2.add(item2);
				lst.remove(0);
			}
			
			for (RptPromotionProgramDetailVO item: lst) {
				
				RptPromotionProgramDetail2VO item2 = lst2.get(lst2.size() - 1);
				if (item2.getPromotionCode().equals(item.getPromotionProgramCode())) {
					//dua vao item2
					List<RptPromotionProgramDetail1VO> lstItem1 = item2.getLstRptPromotionProgramDetail1VO();
					RptPromotionProgramDetail1VO item1 = lstItem1.get(lstItem1.size() - 1);
					if (item1.getStaffCode().equals(item.getStaffCode())) {
						List<RptPromotionProgramDetailVO> lstItem = item1.getLstRptPromotionProgramDetailVO();
						List<RptPromotionProgramDetailVO> lstNewItem = new ArrayList<RptPromotionProgramDetailVO>();
						lstNewItem.addAll(lstItem);
						lstNewItem.add(item);
						item1.setLstRptPromotionProgramDetailVO(lstNewItem);
//						List<RptPoStatusTracking1VO> lstNewItem1 = new ArrayList<RptPoStatusTracking1VO>();
//						lstNewItem1.addAll(lstItem1);
//						lstNewItem1.add(item1);
//						item2.setLstRptPoStatusTracking1VO(lstNewItem1);
					}
					else {
						item1 = new RptPromotionProgramDetail1VO();
						item1.setStaffCode(item.getStaffCode());
						item1.setLstRptPromotionProgramDetailVO(Arrays.asList(item));
								
						List<RptPromotionProgramDetail1VO> lstNewItem1 = new ArrayList<RptPromotionProgramDetail1VO>();
						lstNewItem1.addAll(lstItem1);
						lstNewItem1.add(item1);
						item2.setLstRptPromotionProgramDetail1VO(lstNewItem1);
					}
				}
				else {
					RptPromotionProgramDetail1VO newItem1 = new RptPromotionProgramDetail1VO();
					newItem1.setStaffCode(item.getStaffCode());
					newItem1.setLstRptPromotionProgramDetailVO(Arrays.asList(item));
					
					RptPromotionProgramDetail2VO newItem2 = new RptPromotionProgramDetail2VO();
					newItem2.setPromotionCode(item.getPromotionProgramCode());
					newItem2.setLstRptPromotionProgramDetail1VO(Arrays.asList(newItem1));
					lst2.add(newItem2);
				}
				
			}
			
			return lst2;
		}
		catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.SaleOrderReportMgr#RptStaffProgramPromotion(java.lang.Long, java.util.Date, java.util.Date)
	 */
	@Override
	public List<RptStaffProgramPromotionVO> getListRptStaffProgramPromotion(Long shopId, String staffCode,
			Date fromDate, Date toDate) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			if (fromDate == null) {
				throw new IllegalArgumentException("fromDate is null");
			}
			if (toDate == null) {
				throw new IllegalArgumentException("toDate is null");
			}
			List<RptStaffProgramPromotionVO> listResutl = new ArrayList<RptStaffProgramPromotionVO>();
			List<RptPromotionDetailStaff2VO> lst = saleOrderDetailDAO.getListRptPromotionDetailStaffV2O(shopId, staffCode, fromDate, toDate, false);
			RptStaffProgramPromotionVO rptStaffProgramPromotionVO = null;
			staffCode = "";
			String staffName = "";
			for (RptPromotionDetailStaff2VO rptPromotionDetailStaff2VO : lst) {
				if (!staffCode.equals(rptPromotionDetailStaff2VO.getStaffCode())) {
					if (staffCode != "") {
						listResutl.add(rptStaffProgramPromotionVO);
					}
					staffCode = rptPromotionDetailStaff2VO.getStaffCode();
					staffName = rptPromotionDetailStaff2VO.getStaffName();
					//
					rptStaffProgramPromotionVO = new RptStaffProgramPromotionVO();
					rptStaffProgramPromotionVO.setStaffCode(staffCode);
					rptStaffProgramPromotionVO.setStaffName(staffName);
					//
					rptStaffProgramPromotionVO.getLstRptPromotionDetailStaff2VO().add(rptPromotionDetailStaff2VO);
				}
				if (rptPromotionDetailStaff2VO.getStaffCode() != null) {
					rptStaffProgramPromotionVO.getLstRptPromotionDetailStaff2VO().add(rptPromotionDetailStaff2VO);
				}
			}
			if (rptStaffProgramPromotionVO != null) {
				listResutl.add(rptStaffProgramPromotionVO);
			}
			return listResutl;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.SaleOrderReportMgr#getRptGenaralDetailDisplayProgram(java.lang.Long, java.lang.String)
	 */
	@Override
	public RptPayDetailDisplayProgr1ParentVO getRptGenaralDetailDisplayProgram(
			Long shopId, String displayProgramCode) throws BusinessException {
		// TODO Auto-generated method stub
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (displayProgramCode == null) {
			throw new IllegalArgumentException("displayProgramCode is null");
		}
		RptPayDetailDisplayProgr1ParentVO result = new RptPayDetailDisplayProgr1ParentVO();
		try {
			/*DisplayProgram displayProgram = displayProgramDAO.getDisplayProgramByCode(displayProgramCode);
			if (displayProgram == null) {
				throw new IllegalArgumentException("get displayProgram is null");
			}
			result.setDisplayProgram(displayProgram);*/
			List<RptPayDetailDisplayProgr1VO> listRptGenaralDetailDis = saleOrderDetailDAO.getListRptGenaralDetailDisplayProgram(shopId, displayProgramCode);
			if (listRptGenaralDetailDis != null) {
				result.setLstRptPayDetailDisplayProgr1VO(listRptGenaralDetailDis);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return result;
	}
	
	@Override
	public List<RptProductExchangeVO> getListRptProductExchangeVO(
			Date fromDate, Date toDate,
			Long shopId, String programCode, Long catId)
			throws BusinessException {
		try {
			return saleOrderDAO.getListRptProductExchangeVO(null, fromDate, toDate, shopId, programCode, catId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

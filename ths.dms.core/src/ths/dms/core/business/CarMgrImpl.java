package ths.dms.core.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Car;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.CarSOFilter;
import ths.dms.core.entities.vo.CarSOVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.CarDAO;

public class CarMgrImpl implements CarMgr {
	
	@Autowired
	CarDAO carDAO;
	
	@Override
	public Car createCar(Car car, LogInfoVO logInfo) throws BusinessException {
		try {
			return carDAO.createCar(car, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateCar(Car car, LogInfoVO logInfo) throws BusinessException {
		try {
			carDAO.updateCar(car, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteCar(Car car, LogInfoVO logInfo) throws BusinessException{
		try {
			carDAO.deleteCar(car, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Car getCarById(Long id) throws BusinessException{
		try {
			return id==null?null:carDAO.getCarById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Car> getListCar(KPaging<Car> kPaging, Long shopId,
			String type, String carNumber, Long labelId, Float gross,
			String category, String origin, ActiveType status, Boolean getCarInChildShop)
					throws BusinessException {
		try {
			List<Car> lst = carDAO.getListCar(kPaging, shopId, type, carNumber, labelId, gross, category, origin, status, getCarInChildShop);
			ObjectVO<Car> vo = new ObjectVO<Car>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean isUsingByOthers(long carId) throws BusinessException {
		try {
			return carDAO.isUsingByOthers(carId);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkIfRecordExists(String carNumber, Long id) 
			throws BusinessException {
		try {
			return carDAO.checkIfRecordExists(carNumber, id);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Car getCarByNumberAndShopId(String carNumber, Long shopId)
			throws BusinessException {
		try {
			return carDAO.getCarByNumberAndShopId(carNumber, shopId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Car getCarByNumberAndListShop(String carNumber, String lstShopId)
			throws BusinessException {
		try {
			return carDAO.getCarByNumberAndListShop(carNumber, lstShopId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public ObjectVO<CarSOVO> getListCar(CarSOFilter filter) throws BusinessException {
		try {
			List<CarSOVO> lst = carDAO.getListCar(filter);
			ObjectVO<CarSOVO> vo = new ObjectVO<CarSOVO>();
			vo.setkPaging(filter.getPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
}

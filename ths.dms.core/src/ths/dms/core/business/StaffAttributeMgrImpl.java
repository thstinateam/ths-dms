package ths.dms.core.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.StaffAttribute;
import ths.dms.core.entities.StaffAttributeDetail;
import ths.dms.core.entities.StaffAttributeEnum;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductAttributeReqType;
import ths.dms.core.entities.filter.StaffAttributeFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductAttributeVO;
import ths.dms.core.entities.vo.StaffAttributeVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.StaffAttributeDAO;
/**
 * @author liemtpt
 * @since 21/01/2015
 */
public class StaffAttributeMgrImpl implements StaffAttributeMgr {

	@Autowired
	StaffAttributeDAO staffAttributeDAO;
	
//	@Autowired
//	StaffAttributeDetailDAO staffAttributeDetailDAO;
	

	@Override
	public StaffAttribute createStaffAttribute(StaffAttribute attribute,LogInfoVO logInfoVO) throws BusinessException {
		try {
			return staffAttributeDAO.createStaffAttribute(attribute,logInfoVO);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteStaffAttribute(StaffAttribute attribute) throws BusinessException {
		try {
			staffAttributeDAO.deleteStaffAttribute(attribute);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteStaffAttributeEnum(StaffAttributeEnum attribute) throws BusinessException {
		try {
			staffAttributeDAO.deleteStaffAttributeEnum(attribute);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StaffAttribute getStaffAttributeById(long id) throws BusinessException {
		try {
			return staffAttributeDAO.getStaffAttributeById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StaffAttributeEnum getStaffAttributeEnumById(long id) throws BusinessException {
		try {
			return staffAttributeDAO.getStaffAttributeEnumById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateStaffAttribute(StaffAttribute attribute,LogInfoVO logInfo) throws BusinessException {
		try {
			staffAttributeDAO.updateStaffAttribute(attribute,logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateStaffAttributeDetail(StaffAttributeDetail attribute,LogInfoVO logInfo) throws BusinessException {
		try {
			staffAttributeDAO.updateStaffAttributeDetail(attribute,logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateStaffAttributeEnum(StaffAttributeEnum attribute,LogInfoVO logInfo) throws BusinessException {
		try {
			staffAttributeDAO.updateStaffAttributeEnum(attribute,logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<AttributeDynamicVO> getListStaffAttributeVO(AttributeDynamicFilter filter, KPaging<AttributeDynamicVO> kPaging)
			throws BusinessException{
		try {
			ObjectVO<AttributeDynamicVO> res = new ObjectVO<AttributeDynamicVO>();
			List<AttributeDynamicVO> lst = staffAttributeDAO.getListStaffAttributeVO(filter, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveOrUpdateStaffAttribute(AttributeDynamicVO vo, LogInfoVO logInfo ) throws BusinessException{
		if(vo == null){
			throw new IllegalArgumentException("attributeDynamicVO is null");
		}
		try{			
			if(vo.getAttributeId()!=null && vo.getAttributeId()>0L){
				StaffAttribute att = staffAttributeDAO.getStaffAttributeById(vo.getAttributeId());
				if(att == null){
					throw new IllegalArgumentException("StaffAttribute id is null");
				}
				att.setName(vo.getAttributeName());
				att.setMandatory(ProductAttributeReqType.parseValue(vo.getMandatory()));
				att.setDescription(vo.getDescription());
				att.setDisplayOrder(vo.getDisplayOrder());
				att.setStatus(ActiveType.parseValue(vo.getStatus()));
				if(vo.getType()!=null){
					if(AttributeColumnType.CHARACTER.getValue().equals(vo.getType())){
						att.setDataLength(vo.getDataLength());
					}else if(AttributeColumnType.NUMBER.getValue().equals(vo.getType())){
						att.setDataLength(vo.getDataLength());
						att.setMinValue(vo.getMinValue());
						att.setMaxValue(vo.getMaxValue());
					}
				}
				staffAttributeDAO.updateStaffAttribute(att,logInfo);
			}else{
				StaffAttribute att = new StaffAttribute();
				att.setCode(vo.getAttributeCode());
				att.setName(vo.getAttributeName());
				att.setType(AttributeColumnType.parseValue(vo.getType()));
				att.setMandatory(ProductAttributeReqType.parseValue(vo.getMandatory()));
				att.setDescription(vo.getDescription());
				att.setDisplayOrder(vo.getDisplayOrder());
				att.setStatus(ActiveType.RUNNING);
				if(vo.getType()!=null){
					if(AttributeColumnType.CHARACTER.getValue().equals(vo.getType())){
						att.setDataLength(vo.getDataLength());
					}else if(AttributeColumnType.NUMBER.getValue().equals(vo.getType())){
						att.setDataLength(vo.getDataLength());
						att.setMinValue(vo.getMinValue());
						att.setMaxValue(vo.getMaxValue());
					}
				}
				att = staffAttributeDAO.createStaffAttribute(att,logInfo);
			}
		}catch(DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveOrUpdateStaffAttributeEnum(AttributeEnumVO vo, LogInfoVO logInfo ) throws BusinessException{
		if(vo == null){
			throw new IllegalArgumentException("attributeEnumVO is null");
		}
		try{
			StaffAttribute att = staffAttributeDAO.getStaffAttributeById(vo.getAttributeId());
			StaffAttributeEnum attEnum = staffAttributeDAO.getStaffAttributeEnumByCode(vo.getEnumCode());
			if(attEnum!=null){
				attEnum.setValue(vo.getEnumValue());
				staffAttributeDAO.updateStaffAttributeEnum(attEnum,logInfo);
			}else{
				attEnum = new StaffAttributeEnum();
				attEnum.setCode(vo.getEnumCode().toUpperCase());
				attEnum.setValue(vo.getEnumValue());
				attEnum.setStaffAttribute(att);
				attEnum.setStatus(ActiveType.RUNNING);
				attEnum = staffAttributeDAO.createStaffAttributeEnum(attEnum,logInfo);
			}
		}catch(DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<StaffAttributeDetail> getListStaffAttributeByEnumId(long id) throws BusinessException {
		try {
			return staffAttributeDAO.getListStaffAttributeDetailByEnumId(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<StaffAttributeVO> getListStaffAttributeVOFilter(StaffAttributeFilter filter) throws BusinessException {
		try {
			List<StaffAttributeVO> attributeVOs = new ArrayList<StaffAttributeVO>();
			
			attributeVOs = staffAttributeDAO.getListStaffAttributeVOFilter(filter);
			
			for(StaffAttributeVO vo : attributeVOs){
				filter.setStaffAttId(vo.getId());
				if(AttributeColumnType.CHOICE.getValue().equals(vo.getType())
					|| AttributeColumnType.MULTI_CHOICE.getValue().equals(vo.getType())){					
					vo.setAttributeEnumVOs(staffAttributeDAO.getListStaffAttributeEnumVOFilter(filter));										
				}				
				if(filter.getStaffId() != null){
					vo.setAttributeDetailVOs(staffAttributeDAO.getListStaffAttributeDetailVOFilter(filter));
					//vo.setAttributeDetailVOs(staffAttributeDAO.getListStaffAttributeDetailVOFilter(filter));
				}
			}
			return attributeVOs;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}
package ths.dms.core.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Shop;
import ths.core.entities.vo.rpt.RptCustomerPayReceived1VO;
import ths.core.entities.vo.rpt.RptCustomerPayReceivedVO;
import ths.core.entities.vo.rpt.RptLimitDebitParentVO;
import ths.core.entities.vo.rpt.RptLimitDebitVO;
import ths.core.entities.vo.rpt.RptPayReceived1VO;
import ths.core.entities.vo.rpt.RptPayReceivedVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.DebitDetailDAO;
import ths.dms.core.dao.PayReceivedDAO;
import ths.dms.core.dao.PaymentDetailDAO;
import ths.dms.core.dao.ShopDAO;

public class RptPayDebitMgrImpl implements RptPayDebitMgr {

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	PaymentDetailDAO paymentDetailDAO;
	
	@Autowired
	PayReceivedDAO payReceivedDAO;
	
	@Autowired
	DebitDetailDAO debitDetailDAO;

	@Override
	public RptPayReceivedVO getRptPayReceivedVO(Long shopId, String shortCode,
			Date fromDate, Date toDate) throws BusinessException {
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			List<RptPayReceived1VO> lstRptPayReceived1VO = paymentDetailDAO
					.getListRptPayReceived1VO(shopId, shortCode, fromDate, toDate);
			RptPayReceivedVO rptPayReceivedVO = new RptPayReceivedVO();
			rptPayReceivedVO.setShop(shop);
			rptPayReceivedVO.setLstRptPayReceived1VO(lstRptPayReceived1VO);
			return rptPayReceivedVO;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<RptCustomerPayReceived1VO> getListRptCustomerPayReceived1VO(Long shopId, 
			Date fromPayDate, Date toPayDate, Long customerId)
			throws BusinessException {
		try {
			List<RptCustomerPayReceivedVO> lst = payReceivedDAO.getListRptCustomerPayReceivedVO(null, shopId, fromPayDate, toPayDate, customerId);
			List<RptCustomerPayReceived1VO> lstRs = new ArrayList<RptCustomerPayReceived1VO>();
			if (lst.size() != 0) {
				RptCustomerPayReceived1VO subVo = new RptCustomerPayReceived1VO();
				subVo.setCustomerCode(lst.get(0).getCustomerCode());
				List<RptCustomerPayReceivedVO> subLst = Arrays.asList(lst.get(0));
				subVo.setLstRptCustomerPayReceivedVO(subLst);
				lstRs.add(subVo);
				lst.remove(0);
			}
				
			for (RptCustomerPayReceivedVO vo: lst) {
				Integer maxLstRsIndex = lstRs.size()-1;
				if (vo.getCustomerCode().equals(lstRs.get(maxLstRsIndex).getCustomerCode())) {
					List<RptCustomerPayReceivedVO> subLst = lstRs.get(maxLstRsIndex).getLstRptCustomerPayReceivedVO();
					List<RptCustomerPayReceivedVO> subLst1 = new ArrayList<RptCustomerPayReceivedVO>();
					subLst1.addAll(subLst);
					subLst1.add(vo);
					lstRs.get(maxLstRsIndex).setLstRptCustomerPayReceivedVO(subLst1);
				}
				else {
					RptCustomerPayReceived1VO lot1VO = new RptCustomerPayReceived1VO();
					lot1VO.setCustomerCode(vo.getCustomerCode());
					List<RptCustomerPayReceivedVO> subLst = Arrays.asList(vo);
					lot1VO.setLstRptCustomerPayReceivedVO(subLst);
					lstRs.add(lot1VO);
				}
				
			}
			return lstRs;
		}
		catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.RptPayDebitMgr#getListLimitDebit(java.lang.Long, java.util.Date, java.lang.String)
	 */
	@Override
	public List<RptLimitDebitParentVO> getListLimitDebit(Long shopId,
			Date date, String customerCode) throws BusinessException {
		List<RptLimitDebitParentVO> result = new ArrayList<RptLimitDebitParentVO>();
		try {
			RptLimitDebitParentVO parent = null;
			String shortCode = "";
			List<RptLimitDebitVO> listLimit = debitDetailDAO.getListLimitDebitDetail(shopId, date, customerCode);
			if (listLimit != null) {
				for (RptLimitDebitVO item: listLimit) {
					if (!shortCode.equals(item.getShortCode())) {
						if (!shortCode.equals("")) {
							result.add(parent);
						}
						parent = new RptLimitDebitParentVO();
						shortCode = item.getShortCode();
						parent.setShortCode(shortCode);
						parent.setCustomerCode(item.getCustomerCode());
						parent.setCustomerName(item.getCustomerName());
						parent.getListLimitDebit().add(item);
					} else {
						parent.getListLimitDebit().add(item);
					}
				}
				if (parent != null) {
					result.add(parent);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return result;
	}
}

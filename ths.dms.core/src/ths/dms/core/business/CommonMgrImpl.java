package ths.dms.core.business;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.EquipLender;
import ths.dms.core.entities.ExceptionDay;
import ths.dms.core.entities.ImportFileData;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ArithmeticEnum;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PromotionProgramVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.core.entities.vo.rpt.RptKD_General_VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.ChannelTypeDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.ExceptionDayDAO;
import ths.dms.core.dao.ProductInfoDAO;
import ths.dms.core.dao.PromotionProgramDAO;
import ths.dms.core.dao.SaleDayDAO;
import ths.dms.core.dao.StaffDAONew;
import ths.dms.core.dao.repo.IRepository;

public class CommonMgrImpl implements CommonMgr {
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	PromotionProgramDAO promotionProgramDAO;
	
	@Autowired
	SaleDayMgr saleDayMgr;

	@Autowired
	SaleDayDAO saleDayDAO;

	@Autowired
	ProductInfoDAO productInfoDAO;

	@Autowired
	ExceptionDayDAO exceptionDayDAO;

	@Autowired
	StaffDAONew staffDAONew;
	
	@Autowired
	ChannelTypeDAO channelTypeDAO;

	@Autowired
	private IRepository repo;

	@Override
	public Date getSysDate() throws BusinessException {
		try {
			return commonDAO.getSysDate();
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Integer getIslevel(Long shopId) throws BusinessException {
		try {
			return commonDAO.getIslevel(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * General
	 * 
	 * @author hunglm16
	 * @since January 23,2014
	 * */
	@Override
	public Date getYesterDay() throws BusinessException {
		try {
			return commonDAO.getYesterday();
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author hunglm16
	 * @since March 24, 2014
	 * @description Lay ngay so voi ngay hien tai qua so khoang cach ngay
	 * 
	 * */
	@Override
	public Date getDateBySysdateForNumberDay(Integer numberDay, boolean trunc) throws BusinessException {
		try {
			return commonDAO.getDateBySysdateForNumberDay(numberDay, trunc);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * General get Sale Date HO by fromDate, toDate
	 * 
	 * @author hunglm16
	 * @since January 23,2014
	 * */
	@Override
	public RptKD_General_VO generalDateBySalesHO(Date fromDate, Date toDate) throws BusinessException {
		RptKD_General_VO kq = new RptKD_General_VO();
		try {
			Double soNgayBanHang = 0.0;
			Double soNgayThucHien = 0.0;
			Double soNgayNghiLe = 0.0;
			Double tienDoChuan = 0.0;
			Double tienThucHien = 0.0;
			// So ngay ban hang trong thang
			if (toDate != null) {
				soNgayBanHang = Double.valueOf(saleDayDAO.getSaleDayByDate(toDate));
			}
			if (fromDate != null && toDate != null) {
				// So ngay thuc hien
				soNgayThucHien = Double.valueOf(saleDayMgr.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate));
				//so ngay nghi le
				List<ExceptionDay> lstExcDay = exceptionDayDAO.getLstExceptionDay(fromDate, toDate);
				if (lstExcDay != null) {
					soNgayNghiLe = Double.valueOf(lstExcDay.size());
				}
			}

			//Tien do chuan
			if (soNgayBanHang > 0 || soNgayBanHang < 0) {
				tienDoChuan = soNgayThucHien / soNgayBanHang;
			}
			tienThucHien = tienDoChuan;
			kq.setSoNgayBanHangTinhNgayLe(soNgayThucHien + soNgayNghiLe);
			kq.setSoNgayBanHang(soNgayBanHang);
			kq.setSoNgayThucHien(soNgayThucHien);
			kq.setTienDoChuan(tienDoChuan);
			kq.setTienThucHien(tienThucHien);
			kq.setSoNgayNghiLe(soNgayNghiLe);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		return kq;
	}

	/**
	 * @author hunglm16
	 * @since April 04, 2014
	 * @description Kiem tra co phai la mot ngay hay khong
	 * 
	 * */
	@Override
	public BigDecimal checkValidDateDate(String dateStr) throws BusinessException {
		try {
			return commonDAO.checkValidDateDate(dateStr);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ShopParam getShopParamByType(Long shopId, String type) throws BusinessException {
		try {
			return commonDAO.getShopParamByType(shopId, type);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateShopParam(ShopParam shopParam) throws BusinessException {
		try {
			repo.update(shopParam);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ShopParam createShopParam(ShopParam shopParam) throws BusinessException {
		try {
			return repo.create(shopParam);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public <T> T createEntity(T object) throws BusinessException {
		try {
			return commonDAO.createEntity(object);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public <T> List<T> creatListEntity(List<T> lstObjects) throws BusinessException {
		try {
			return commonDAO.creatListEntity(lstObjects);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEntity(Object object) throws BusinessException {
		try {
			commonDAO.updateEntity(object);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public <T> List<T> updateListEntity(List<T> lstObjects) throws BusinessException {
		try {
			return commonDAO.updateListEntity(lstObjects);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ImportFileData> getListImportFileData(KPaging<ImportFileData> kPaging, Long staffId, List<Integer> listStatus, String serverId) throws BusinessException {
		try {
			ObjectVO<ImportFileData> result = new ObjectVO<ImportFileData>();
			result.setkPaging(kPaging);
			result.setLstObject(commonDAO.getListImportFileData(kPaging, staffId, listStatus, serverId));
			return result;
		} catch (Exception e) {
			throw new BusinessException();
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public <T> T getEntityById(Class<T> clazz, Serializable id) throws BusinessException {
		try {
			return commonDAO.getEntityById(clazz, id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteEntity(Object object) throws BusinessException {
		try {
			commonDAO.deleteEntity(object);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

	}
	
	@Override
	public ImportFileData getImportFileData(String fileName, Long staffId) throws BusinessException {
		try {
			return commonDAO.getImportFileData(fileName, staffId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Staff> getListStaffImportFile(KPaging<Staff> kPaging, List<Integer> listStatus, String serverId) throws BusinessException {
		try {
			ObjectVO<Staff> result = new ObjectVO<Staff>();
			result.setkPaging(kPaging);
			result.setLstObject(commonDAO.getListStaffImportFile(kPaging, listStatus, serverId));
			return result;
		} catch (Exception e) {
			throw new BusinessException();
		}
	}

	@Override
	public boolean checkStaffInCMSByFilterWithCMS(BasicFilter<BasicVO> filter) throws BusinessException {
		try {
			return staffDAONew.checkStaffInCMSByFilterWithCMS(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<StaffVO> getListStaffVOFullChilrentByCMS(StaffPrsmFilter<StaffVO> filter) throws BusinessException {
		try {
			return staffDAONew.getListStaffVOFullChilrentByCMS(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ChannelType> getListChannelTypeByTypeAndArrObjType(Integer type, Integer... objectType) throws BusinessException {
		try {
			return channelTypeDAO.getListChannelTypeByTypeAndArrObjType(type, objectType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public ShopParam getShopParamEx(long shopId, String type) throws BusinessException {
		try {
			ShopParam sp = commonDAO.getShopParamByType(shopId, type);
			if (sp == null) {
				Shop sh = commonDAO.getEntityById(Shop.class, shopId);
				while (sp == null && (sh != null && sh.getParentShop() != null)) {
					sh = sh.getParentShop();
					sp = commonDAO.getShopParamByType(sh.getId(), type);
				}
			}
			return sp;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ImportFileData createImportFileData(ImportFileData importFileData, LogInfoVO logInfo) throws BusinessException {
		try {
			Date sysDate = commonDAO.getSysDate();
			importFileData.setCreateDate(sysDate);
			importFileData.setCreateUser(logInfo.getStaffCode());
			return commonDAO.createImportFileData(importFileData);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateImportFileData(ImportFileData importFileData, LogInfoVO logInfo) throws BusinessException {
		try {
			Date sysDate = commonDAO.getSysDate();
			importFileData.setUpdateDate(sysDate);
			importFileData.setUpdateUser(logInfo.getStaffCode());
			commonDAO.updateEntity(importFileData);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<PromotionProgramVO> getListVOPromotionProgramByShop(Integer status, Long shopId, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		try {
			return promotionProgramDAO.getListVOPromotionProgramByShop(status, shopId, staffRootId, roleId, shopRootId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Integer compareDateWithoutTime(Date date1, Date date2) throws BusinessException {
		try {
			return commonDAO.compareDateWithoutTime(date1, date2);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public int arithmeticForDateByDateWithTrunc(Date date1, Date date2, ArithmeticEnum arithmetic) throws BusinessException {
		try {
			return commonDAO.arithmeticForDateByDateWithTrunc(date1, date2, arithmetic);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<BasicVO> getListYearSysConfig() throws BusinessException {
		try {
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_for_function_g.GET_LIST_YEAR_SYS_CF(?)");
			return repo.getListByQueryDynamicFromPackage(BasicVO.class, sql.toString(), params, null);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Long getEquipPeriodIdByCloseForMaxToDate() throws BusinessException {
		try {
			return commonDAO.getEquipPeriodIdByCloseForMaxToDate();
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipLender getFirtInformationEquipLender(Long shopId) throws BusinessException {
		try {
			return commonDAO.getFirtInformationEquipLender(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

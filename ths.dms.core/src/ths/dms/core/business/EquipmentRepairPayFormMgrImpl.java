package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.EquipRepairForm;
import ths.dms.core.entities.EquipRepairFormDtl;
import ths.dms.core.entities.EquipRepairPayForm;
import ths.dms.core.entities.EquipRepairPayFormDtl;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.EquipPeriodType;
import ths.dms.core.entities.enumtype.PaymentStatusRepair;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.filter.EquipRepairFilter;
import ths.dms.core.entities.vo.EquipRepairFormVO;
import ths.dms.core.entities.vo.EquipRepairPayFormVO;
import ths.dms.core.entities.vo.EquipmentRepairPaymentRecord;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.EquipmentManagerDAO;
import ths.dms.core.dao.EquipmentPeriodDAO;
import ths.dms.core.dao.EquipmentRepairPayFormDAO;

public class EquipmentRepairPayFormMgrImpl implements EquipmentRepairPayFormMgr {
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	PlatformTransactionManager transactionManager;
	
	@Autowired
	EquipmentPeriodDAO equipmentPeriodDAO;
	
	@Autowired
	EquipmentManagerDAO equipMngDAO;
	
	@Autowired
	EquipmentRepairPayFormDAO equipmentRepairPayFormDAO;
	
	@Override
	public ObjectVO<EquipRepairPayFormVO> getListEquipRepairPayForm(EquipRepairFilter filter) throws BusinessException {
		try {
			List<EquipRepairPayFormVO> lstObject = equipmentRepairPayFormDAO.getListEquipRepairPayForm(filter);
			ObjectVO<EquipRepairPayFormVO> vo = new ObjectVO<EquipRepairPayFormVO>();
			vo.setLstObject(lstObject);
			vo.setkPaging(filter.getkPagingPayFormVO());
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<EquipRepairPayFormVO> getListEquipRepairPayFormDtlById(EquipRepairFilter filter) throws BusinessException {
		try {
			List<EquipRepairPayFormVO> lstObject = equipmentRepairPayFormDAO.getListEquipRepairPayFormDtlById(filter);
			ObjectVO<EquipRepairPayFormVO> vo = new ObjectVO<EquipRepairPayFormVO>();
			vo.setLstObject(lstObject);
			vo.setkPaging(filter.getkPagingPayFormVO());
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<EquipRepairFormVO> getListEquipRepairPopup(EquipRepairFilter filter) throws BusinessException {
		try {
			List<EquipRepairFormVO> lst = equipmentRepairPayFormDAO.getListEquipRepairPopup(filter);
			ObjectVO<EquipRepairFormVO> objVO = new ObjectVO<EquipRepairFormVO>();
			objVO.setLstObject(lst);
			objVO.setkPaging(filter.getkPaging());
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author vuongmq
	 * @date 24/06/2015
	 * @description xu ly them moi va cap nhat phieu thanh toan sua chua, o MH detail
	 */
	@Override
	public String updateEquipmentRepairPaymentRecord(final Long id, final List<Long> lstId , final EquipmentRepairPaymentRecord equipmentRepairPaymentRecord, final LogInfoVO logInfo) throws BusinessException {
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		final StringBuilder errorMessage = new StringBuilder();
		final StringBuilder maPhieuVaId = new StringBuilder();
		Boolean isUpdateSuccess = transactionTemplate.execute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus transactionStatus) {
				Boolean isUpdateSuccess = true;
				try {
					Date sysdate = commonDAO.getSysDate();
					EquipPeriod currentEquipmentPeriod = equipmentPeriodDAO.getEquipPeriodCurrent();
					if (currentEquipmentPeriod == null || currentEquipmentPeriod.getStatus() != EquipPeriodType.OPENED) {
						errorMessage.append("equipment.repair.payment.record.equipment.period.not.open");
						isUpdateSuccess = false;
						return isUpdateSuccess;
					}
					if (id == null) {
						/** tao moi phieu thanh toán*/
						EquipRepairPayForm equipRepairPayForm = new EquipRepairPayForm();
						equipRepairPayForm.setEquipPeriod(currentEquipmentPeriod);
						equipRepairPayForm.setPaymentDate(equipmentRepairPaymentRecord.getPaymentDate());
						equipRepairPayForm.setStatus(equipmentRepairPaymentRecord.getStatus());
						// add shop root de khi search, hay phe duyet lay duoc danh sach phan quyen theo shop
						Shop shop = commonDAO.getEntityById(Shop.class, equipmentRepairPaymentRecord.getShopRoot());
						equipRepairPayForm.setShop(shop);
						equipRepairPayForm.setCreateUser(logInfo.getStaffCode());
						equipRepairPayForm.setCreateDate(sysdate);
						/** Trang thai thanh toan */; // vuongmq; 26/05/2015; cap nhat; APPROVE_DATE
						//equipRepairPayForm.setApproveDate(sysdate);
						equipRepairPayForm = commonDAO.createEntity(equipRepairPayForm);
						/** TINH TONG TIEN CUA ALL CAC PHIEU SUA CHUA**/
						BigDecimal tongTien = BigDecimal.ZERO;
						EquipRepairFilter filter = new EquipRepairFilter();
						filter.setLstId(lstId);
						List<EquipRepairForm> lstRepair = equipMngDAO.getListEquipRepairFormFilter(filter);
						if (lstRepair != null && lstRepair.size() > 0) {
							for (int i = 0, sz = lstRepair.size(); i < sz; i++) {
								EquipRepairForm equipRepairForm = lstRepair.get(i);
								if (equipRepairForm.getTotalAmount() != null) {
									tongTien = tongTien.add(equipRepairForm.getTotalAmount());
								}
								// tao danh sach chi tiet cua phieu thanh toan
								EquipRepairPayFormDtl equipRepairPayFormDtl = new EquipRepairPayFormDtl();
								equipRepairPayFormDtl.setEquipRepairPayForm(equipRepairPayForm);
								equipRepairPayFormDtl.setEquipRepairForm(equipRepairForm);
								equipRepairPayFormDtl.setTotalActualAmount(equipRepairForm.getTotalAmount());
								equipRepairPayFormDtl.setCreateUser(logInfo.getStaffCode());
								equipRepairPayFormDtl.setCreateDate(sysdate);
								commonDAO.createEntity(equipRepairPayFormDtl);
								// tao moi thanh toan: cap nhat cac phieu sua chua phai la dang tham gia thanh toan
								if (StatusRecordsEquip.DRAFT.equals(equipmentRepairPaymentRecord.getStatus()) || StatusRecordsEquip.WAITING_APPROVAL.equals(equipmentRepairPaymentRecord.getStatus()) ) {
									equipRepairForm.setStatusPayment(PaymentStatusRepair.PAID_TRANSACTION);
								}
								equipRepairForm.setUpdateDate(sysdate);
								equipRepairForm.setUpdateUser(logInfo.getStaffCode());;
								commonDAO.updateEntity(equipRepairForm);
							}
						}
						/** cap nhat lai ma phieu thanh toan tang tu dong va add tong tien vao**/
						String id = "";
						id = "00000000" + equipRepairPayForm.getId().toString();
						id = id.substring(id.length() - 8);
						String code = "TT" + id;
						equipRepairPayForm.setCode(code);
						equipRepairPayForm.setTotalAmount(tongTien);
						commonDAO.updateEntity(equipRepairPayForm);
						maPhieuVaId.append(code).append(";").append(equipRepairPayForm.getId()); // truong hop tao thanh cong tra ma code ve de view cho nguoi dung
						
					} else {
						/** cap nhat lai phieu thanh toan */
						EquipRepairPayForm equipRepairPayForm = equipmentRepairPayFormDAO.retrieveEquipmentRepairPayment(id);
						if (equipRepairPayForm != null) {
							// xoa danh sach cac phieu sua chua
							EquipRepairFilter filterD = new EquipRepairFilter();
							filterD.setId(id);
							List<EquipRepairPayFormDtl> lstDetail = equipmentRepairPayFormDAO.getEquipmentRepairPaymentDtlFilter(filterD);
							for (EquipRepairPayFormDtl item : lstDetail) {
								commonDAO.deleteEntity(item);
							}
							// tao moi lai danh sach chi tiet cua thanh toan
							/** TINH TONG TIEN CUA ALL CAC PHIEU SUA CHUA**/
							BigDecimal tongTien = BigDecimal.ZERO;
							EquipRepairFilter filter = new EquipRepairFilter();
							filter.setLstId(lstId);
							List<EquipRepairForm> lstRepair = equipMngDAO.getListEquipRepairFormFilter(filter);
							if (lstRepair != null && lstRepair.size() > 0) {
								for (int i = 0, sz = lstRepair.size(); i < sz; i++) {
									EquipRepairForm equipRepairForm = lstRepair.get(i);
									if (equipRepairForm.getTotalAmount() != null) {
										tongTien = tongTien.add(equipRepairForm.getTotalAmount());
									}
									// tao danh sach chi tiet cua phieu thanh toan
									EquipRepairPayFormDtl equipRepairPayFormDtl = new EquipRepairPayFormDtl();
									equipRepairPayFormDtl.setEquipRepairPayForm(equipRepairPayForm);
									equipRepairPayFormDtl.setEquipRepairForm(equipRepairForm);
									equipRepairPayFormDtl.setTotalActualAmount(equipRepairForm.getTotalAmount());
									equipRepairPayFormDtl.setCreateUser(logInfo.getStaffCode());
									equipRepairPayFormDtl.setCreateDate(sysdate);
									commonDAO.createEntity(equipRepairPayFormDtl);
									//cap nhat thanh toan: cap nhat cac phieu sua chua phai la dang tham gia thanh toan
									if (StatusRecordsEquip.DRAFT.equals(equipmentRepairPaymentRecord.getStatus())) {
										// van la su thao thi nhung thiet bi moi thi van them vao la cho thanh toan
										equipRepairForm.setStatusPayment(PaymentStatusRepair.PAID_TRANSACTION);
									} else if (StatusRecordsEquip.WAITING_APPROVAL.equals(equipmentRepairPaymentRecord.getStatus())) {
										// du thao, khong duyet -> cho duyet
										if (StatusRecordsEquip.DRAFT.equals(equipRepairPayForm.getStatus()) || StatusRecordsEquip.NO_APPROVAL.equals(equipRepairPayForm.getStatus())) {
											equipRepairForm.setStatusPayment(PaymentStatusRepair.PAID_TRANSACTION);
										}
									} else if (StatusRecordsEquip.CANCELLATION.equals(equipmentRepairPaymentRecord.getStatus()) || StatusRecordsEquip.NO_APPROVAL.equals(equipmentRepairPaymentRecord.getStatus())) {
										// chon: Huy, khong duyet
										equipRepairForm.setStatusPayment(PaymentStatusRepair.NOT_PAID_YET);
									} else if (StatusRecordsEquip.APPROVED.equals(equipmentRepairPaymentRecord.getStatus())) {
										// chon: Duyet
										equipRepairForm.setStatusPayment(PaymentStatusRepair.PAID);
									}
									equipRepairForm.setUpdateDate(sysdate);
									equipRepairForm.setUpdateUser(logInfo.getStaffCode());;
									commonDAO.updateEntity(equipRepairForm);
								}
							}
							// cap nhat lai phieu thanh toan
							equipRepairPayForm.setStatus(equipmentRepairPaymentRecord.getStatus());
							equipRepairPayForm.setTotalAmount(tongTien);
							equipRepairPayForm.setUpdateDate(sysdate);
							equipRepairPayForm.setUpdateUser(logInfo.getStaffCode());
							commonDAO.updateEntity(equipRepairPayForm);
						} else {
							errorMessage.append("equipment.repair.payment.record.payment.record.code.not.exists");
							isUpdateSuccess = false;
							return isUpdateSuccess;
						}
					}

				} catch (DataAccessException e) {
					isUpdateSuccess = false;
					errorMessage.append("system.error");
					transactionStatus.setRollbackOnly();
				}
				if (!isUpdateSuccess) {
					transactionStatus.setRollbackOnly();
				}
				return isUpdateSuccess;
			}
		});
		if (isUpdateSuccess) {
			if (!StringUtility.isNullOrEmpty(maPhieuVaId.toString())) {
				// truong hop tao thanh cong tra ma code ve de view cho nguoi dung
				return maPhieuVaId.toString();
			}
			return null;
		}
		return errorMessage.toString();
	}
	
	/***
	 * @author vuongmq
	 * @date 23/06/2015
	 * danh sach detai cua phieu thanh toan theo entities
	 */
	@Override
	public List<EquipRepairPayFormDtl> getEquipmentRepairPaymentDtlFilter(EquipRepairFilter filter) throws BusinessException {
		try {
			return equipmentRepairPayFormDAO.getEquipmentRepairPaymentDtlFilter(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/***
	 * @author vuongmq
	 * @date 23/06/2015
	 * phieu thanh toan theo entities
	 */
	@Override
	public EquipRepairPayForm retrieveEquipmentRepairPayment(Long equipmentRepairPayFormId) throws BusinessException {
		try {
			return equipmentRepairPayFormDAO.retrieveEquipmentRepairPayment(equipmentRepairPayFormId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/***
	 * @author vuongmq
	 * @date 23/06/2015
	 * danh sach cua phieu thanh toan theo entities
	 */
	@Override
	public List<EquipRepairPayForm> retrieveListEquipmentRepairPayment(EquipRepairFilter filter) throws BusinessException {
		try {
			return equipmentRepairPayFormDAO.retrieveListEquipmentRepairPayment(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/***
	 * @author vuongmq
	 * @throws BusinessException 
	 * @date 26/06/2015
	 * Xu ly cap nhat trang thai quan ly thanh toan 
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateStatus(EquipRepairFilter filterAdd, LogInfoVO logInfoVO) throws BusinessException {
		try {
			Date sysdate = commonDAO.getSysDate();
			if (filterAdd != null && filterAdd.getLstId() != null && filterAdd.getLstId().size() > 0) {
				EquipRepairFilter filter = new EquipRepairFilter();
				filter.setLstId(filterAdd.getLstId());
				List<EquipRepairPayForm> lstEquipRepairPayForms = equipmentRepairPayFormDAO.retrieveListEquipmentRepairPayment(filter);
				if (lstEquipRepairPayForms != null && lstEquipRepairPayForms.size() > 0) {
					for (int i = 0, sz = lstEquipRepairPayForms.size(); i < sz; i++) {
						EquipRepairPayForm equipRepairPayForm = lstEquipRepairPayForms.get(i);
						equipRepairPayForm.setStatus(filterAdd.getStatusRecordsEquip());
						equipRepairPayForm.setUpdateDate(sysdate);
						equipRepairPayForm.setUpdateUser(logInfoVO.getStaffCode());
						if (StatusRecordsEquip.APPROVED.equals(filterAdd.getStatusRecordsEquip())) {
							equipRepairPayForm.setApproveDate(sysdate);
						}
						commonDAO.updateEntity(equipRepairPayForm);
						// cap nhat danh sach detail cua phieu sua chua 
						filter = new EquipRepairFilter();
						filter.setId(equipRepairPayForm.getId());
						List<EquipRepairPayFormDtl> lstEquipRepairFormDtls = equipmentRepairPayFormDAO.getEquipmentRepairPaymentDtlFilter(filter);
						if (lstEquipRepairFormDtls != null && lstEquipRepairFormDtls.size() > 0) {
							for (int j = 0, size = lstEquipRepairFormDtls.size(); j < size; j++) {
								if (lstEquipRepairFormDtls.get(j) != null && lstEquipRepairFormDtls.get(j).getEquipRepairForm() != null) {
									EquipRepairForm equipRepairForm = lstEquipRepairFormDtls.get(j).getEquipRepairForm();
									equipRepairForm.setStatusPayment(filterAdd.getPaymentStatusRepair());
									equipRepairForm.setUpdateDate(sysdate);
									equipRepairForm.setUpdateUser(logInfoVO.getStaffCode());
									commonDAO.updateEntity(equipRepairForm);
								}
							}
							
						}
						
					} // end for (int i = 0, sz = lstEquipRepairPayForms.size(); i < sz; i++) {}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipRepairPayFormVO> exportRepairPayForm(EquipRepairFilter filter) throws BusinessException {
		try {
			return equipmentRepairPayFormDAO.exportRepairPayForm(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
}

package ths.dms.core.business;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.Kpi;
import ths.dms.core.entities.KpiImplement;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.SalePlan;
import ths.dms.core.entities.SalePlanVersion;
import ths.dms.core.entities.SalesPlan;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PlanType;
import ths.dms.core.entities.enumtype.SalePlanOwnerType;
import ths.dms.core.entities.vo.DistributeSalePlanVO;
import ths.dms.core.entities.vo.DynamicBigVO;
import ths.dms.core.entities.vo.DynamicStaffKpiSaveVO;
import ths.dms.core.entities.vo.DynamicStaffKpiVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.SalePlanVO;
import ths.dms.core.entities.vo.SumSalePlanVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.CommonFilter;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.CycleDAO;
import ths.dms.core.dao.KpiDAO;
import ths.dms.core.dao.KpiImplementDAO;
import ths.dms.core.dao.PriceDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.SalePlanDAO;
import ths.dms.core.dao.SalePlanVersionDAO;
import ths.dms.core.dao.SalesPlanDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.StaffDAO;
import ths.dms.core.dao.repo.IRepository;

public class SalePlanMgrImpl implements SalePlanMgr {

	@Autowired
	SalePlanDAO salePlanDAO;

	@Autowired
	SalePlanVersionDAO salePlanVersionDAO;

	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	PriceDAO priceDAO;

	@Autowired
	StaffDAO staffDAO;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	SalesPlanDAO salesPlanDAO;
	
	@Autowired
	CommonDAO commonDAO;

	@Autowired
	CycleDAO cycleDAO;
	
	@Autowired
	KpiDAO kpiDAO;
	
	@Autowired
	KpiImplementDAO kpiImplementDAO;
	
	@Autowired
	private IRepository repo;
	
	@Override
	public SalePlan createSalePlan(SalePlan salePlan) throws BusinessException {
		try {
			return salePlanDAO.createSalePlan(salePlan);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public SalePlanVersion createSalePlanVersion(SalePlanVersion salePlanVersion) throws BusinessException {
		try {
			return salePlanVersionDAO.createSalePlanVersion(salePlanVersion);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateSalePlan(SalePlan salePlan) throws BusinessException {
		try {
			salePlanDAO.updateSalePlan(salePlan);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<SalePlanVO> getListSalePlanVO(Long shopId, String staffCode, List<String> lstCatCode, String productCode, Integer num, Integer year, Long gsnppId) throws BusinessException {
		try {
			if (num == null) {
				throw new IllegalArgumentException("num of period is null");
			}
			if (year == null) {
				throw new IllegalArgumentException("year is null");
			}
			//fix bug 6868 - thanhnn
			if (!StringUtility.isNullOrEmpty(staffCode) && shopId == null) {
				Staff s = staffDAO.getStaffByCode(staffCode);
				if (s != null) {
					shopId = s.getShop().getId();
				} else {
					return null;
				}
			}
			return salePlanDAO.getListSalePlanVO(shopId, staffCode, lstCatCode, productCode, num, year, gsnppId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<SalePlanVO> getListSalePlanVO4KT(Long shopId, String staffCode, List<String> lstCatCode, String productCode, Integer month, Integer year) throws BusinessException {
		try {
			if (month == null) {
				throw new IllegalArgumentException("month is null");
			}
			if (year == null) {
				throw new IllegalArgumentException("year is null");
			}
			//fix bug 6868 - thanhnn
			if (!StringUtility.isNullOrEmpty(staffCode) && shopId == null) {
				Staff s = staffDAO.getStaffByCode(staffCode);
				if (s != null) {
					shopId = s.getShop().getId();
				} else {
					return null;
				}
			}
			return salePlanDAO.getListSalePlanVO4KT(shopId, staffCode, lstCatCode, productCode, month, year);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Integer getMaxVersionBySalePlanSersionId(long id) throws BusinessException {
		try {
			return salePlanVersionDAO.getMaxVersionBySalePlanSersionId(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SalePlanMgr#getListSalePlanWithCondition(java
	 * .lang.Long, java.lang.Long, java.lang.Integer, java.lang.Integer,
	 * ths.dms.core.entities.enumtype.PlanType)
	 */
	@Override
	public List<DistributeSalePlanVO> getListSalePlanWithCondition(Long shopId, Long ownerId, String productCode, Integer num, Integer year, List<String> lstCateCode, PlanType type) throws BusinessException {
		try {
			List<DistributeSalePlanVO> list = salePlanDAO.getListSalePlanWithCondition(shopId, ownerId, productCode, num, year, lstCateCode, type, null);
			return list;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<SalePlanVO> getListSalePlanVOForProduct(KPaging<SalePlanVO> kPaging, Long shopId, String staffCode, String catCode, String productCode, Integer month, Integer year) throws BusinessException {
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (month == null) {
				throw new IllegalArgumentException("month is null");
			}
			if (year == null) {
				throw new IllegalArgumentException("year is null");
			}
			ObjectVO<SalePlanVO> ret = new ObjectVO<SalePlanVO>();
			List<SalePlanVO> lstObject = productDAO.getListSalePlanVOForProduct(kPaging, shopId, staffCode, catCode, productCode, month, year);
			ret.setLstObject(lstObject);
			ret.setkPaging(kPaging);
			return ret;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createSalePlanForStaff(Long staffId, Integer num, Integer year, List<Long> lstProductId, List<Integer> lstQty, List<BigDecimal> lstAmt, String createUser,Long shopRootId) throws BusinessException {
		try {
			if (staffId == null || staffId < 0) {
				throw new IllegalArgumentException("staffId is null or staffId < 0");
			}
			if (num == null || num < 0) {
				throw new IllegalArgumentException("month is null or month < 0");
			}
			if (year == null || year < 0) {
				throw new IllegalArgumentException("year is null or year < 0");
			}			
			if (lstProductId == null) {
				throw new IllegalArgumentException("lstProductId is null");
			}
			if (lstQty == null && lstAmt == null) {
				throw new IllegalArgumentException("lstQty and lstAmt is null");
			}
			if (lstProductId.size() != lstQty.size()) {
				throw new IllegalArgumentException("lstProductId.size() != lstQty.size()");
			}
			if (shopRootId == null || shopRootId <= 0) {
				throw new IllegalArgumentException("shopRootId is null or shopRootId < = 0");
			}
			
			Staff staff = staffDAO.getStaffById(staffId);
			Shop shopRoot = shopDAO.getShopById(shopRootId);
			if (staff == null) {
				throw new IllegalArgumentException("staff is null");
			}
			for (int i = 0; i < lstProductId.size(); i++) {
				Integer qty = lstQty.get(i);
				BigDecimal amt = lstAmt.get(i);
				if (qty == null || qty < 0) {
					throw new IllegalArgumentException("qty is null or qty < 0");
				}
				Long productId = lstProductId.get(i);
				if (productId == null || productId < 0) {
					throw new IllegalArgumentException("productId is null or productId < 0");
				}
				Product product = productDAO.getProductById(productId);
				if (product == null) {
					throw new IllegalArgumentException("product is null");
				}
				//Price price = priceDAO.getPriceByProductId(productId);
				/*Price price = priceDAO.getPriceByProductAndShopId(productId, shopRootId);
				if (price == null) {
					throw new IllegalArgumentException("price is null");
				}*/
				SalePlan oldSalePlan = salePlanDAO.getSalePlan(staffDAO.getStaffById(staffId).getStaffCode(), productDAO.getProductById(productId).getProductCode(), num, year);
				if (oldSalePlan != null) {
					//cap nhat sale plan
					SalePlan salePlan = oldSalePlan;
					//Integer oldQty = salePlan.getQuantity();
					//salePlan.setVersion(salePlan.getVersion() + 1);
					//BigDecimal oldAmt = salePlan.getAmount();
					salePlan.setQuantity(qty);
					salePlan.setAmount(amt);
					salePlanDAO.updateSalePlan(salePlan);
					// /////////////////////
					SalePlanVersion salePlanVersion = new SalePlanVersion();
					salePlanVersion.setSalePlan(salePlan);
					salePlanVersion.setVersion(salePlanVersionDAO.getMaxVersionBySalePlanSersionId(salePlan.getId())+1);
//					salePlanVersion.setMonthDate(salePlan.getMonthDate());
					salePlanVersion.setCycle(salePlan.getCycle());
					//salePlanVersion.setProduct(salePlan.getProduct());
					salePlanVersion.setQuantity(salePlan.getQuantity());
					salePlanVersion.setCreateUser(createUser);
					salePlanVersion.setAmount(amt);
					salePlanVersion.setCreateDate(commonDAO.getSysDate());
					salePlanVersionDAO.createSalePlanVersion(salePlanVersion);
					// ////////////////////
					/*SalePlan shopSalePlan = salePlanDAO.getSalePlanOfVNM(staff.getShop().getId(), productId, SalePlanOwnerType.SHOP, PlanType.MONTH, month, year);
					if (shopSalePlan != null) {
						Integer quantityAssign = shopSalePlan.getQuantityAssign();
						if (quantityAssign == null) {
							quantityAssign = new Integer(0);
						}
						shopSalePlan.setQuantityAssign(quantityAssign + salePlan.getQuantity() - oldQty);
						BigDecimal amountAssign = shopSalePlan.getAmountAssign();
						if (amountAssign == null) {
							amountAssign = new BigDecimal(0);
						}
						shopSalePlan.setAmountAssign(amountAssign.add(salePlan.getAmount()).subtract(oldAmt));
						salePlanDAO.updateSalePlan(shopSalePlan);
					}*/
				} else {
					/*if (qty == 0) {
						continue;
					}*/
					//tao moi sale plan
					SalePlan salePlan = new SalePlan();
					salePlan.setObjectId(staffId);
					salePlan.setObjectType(SalePlanOwnerType.SALEMAN);
					//salePlan.setParent(staff.getShop());
					salePlan.setShop(shopRoot);
					salePlan.setType(PlanType.MONTH);
					salePlan.setProduct(product);
					salePlan.setQuantity(qty);
					salePlan.setAmount(amt);
					//salePlan.setMonth(month);
					//salePlan.setYear(year);
					
					
					Cycle cycle = cycleDAO.getCycleByYearAndNum(year, num);
					salePlan.setCycle(cycle);
					//salePlan.setPrice(price.getPrice());
					//salePlan.setVersion(1);
					salePlan.setCreateUser(createUser);
					salePlan.setCat(product.getCat());
					salePlan = salePlanDAO.createSalePlan(salePlan);
					// ///////////////
					SalePlanVersion salePlanVersion = new SalePlanVersion();
					salePlanVersion.setSalePlan(salePlan);
					salePlanVersion.setVersion(1);
//					salePlanVersion.setMonthDate(monthDate);
					salePlanVersion.setCycle(salePlan.getCycle());
					salePlanVersion.setAmount(amt);
					salePlanVersion.setCreateDate(commonDAO.getSysDate());
					//salePlanVersion.setProduct(salePlan.getProduct());
					salePlanVersion.setQuantity(salePlan.getQuantity());
					salePlanVersion.setCreateUser(createUser);
					salePlanVersionDAO.createSalePlanVersion(salePlanVersion);
					// ////////////////////
					/*SalePlan shopSalePlan = salePlanDAO.getSalePlanOfVNM(staff.getShop().getId(), productId, SalePlanOwnerType.SHOP, PlanType.MONTH, month, year);

					if (shopSalePlan != null) {
						Integer quantityAssign = shopSalePlan.getQuantityAssign();
						if (quantityAssign == null) {
							quantityAssign = new Integer(0);
						}
						shopSalePlan.setQuantityAssign(quantityAssign + salePlan.getQuantity());
						BigDecimal amountAssign = shopSalePlan.getAmountAssign();
						if (amountAssign == null) {
							amountAssign = new BigDecimal(0);
						}
						shopSalePlan.setAmountAssign(amountAssign.add(salePlan.getAmount()));
						salePlanDAO.updateSalePlan(shopSalePlan);
					} else {
						//neu san pham duoc phan bo cho nhan vien ma chua duoc phan bo 
						//cho NPP thi tao moi saleplan cho NPP voi quantity = 0
						Long shopId = staffDAO.getStaffById(staffId).getShop().getId();
						Shop shop = shopDAO.getShopById(shopId);
						SalePlan newShopSalePlan = new SalePlan();
						newShopSalePlan.setQuantityAssign(qty);
						newShopSalePlan.setQuantity(0);
						newShopSalePlan.setAmount(new BigDecimal(0));
						newShopSalePlan.setAmountAssign((price.getPrice().multiply(new BigDecimal(qty))));
						newShopSalePlan.setObjectId(shop.getId());
						newShopSalePlan.setObjectType(SalePlanOwnerType.SHOP);
						newShopSalePlan.setType(PlanType.MONTH);
						newShopSalePlan.setProduct(product);
						newShopSalePlan.setCreateUser(createUser);
						newShopSalePlan.setParent(shop.getParentShop());
						newShopSalePlan.setShop(shopRoot);
						newShopSalePlan.setMonth(month);
						newShopSalePlan.setYear(year);
						newShopSalePlan.setPrice(price.getPrice());
						newShopSalePlan.setVersion(1);

						newShopSalePlan = salePlanDAO.createSalePlan(newShopSalePlan);
						// ///////////////
						SalePlanVersion shopSalePlanVersion = new SalePlanVersion();
						shopSalePlanVersion.setSalePlan(newShopSalePlan);
						shopSalePlanVersion.setVersion(newShopSalePlan.getVersion());
						shopSalePlanVersion.setProduct(newShopSalePlan.getProduct());
						shopSalePlanVersion.setQuantity(newShopSalePlan.getQuantity());
						shopSalePlanVersion.setCreateUser(createUser);
						salePlanVersionDAO.createSalePlanVersion(shopSalePlanVersion);
					}*/
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public SumSalePlanVO getSumSalePlan(Long ownerId, Long productId, SalePlanOwnerType shopType, PlanType planType, int num, int year) throws BusinessException {
		try {
			if (ownerId == null) {
				throw new IllegalArgumentException("ownerId is null");
			}
			return salePlanDAO.getSumSalePlan(ownerId, productId, shopType, planType, num, year);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SalePlanMgr#insertSalePlanAndSalePlanVersion
	 * (ths.dms.core.entities.SalePlan, long)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void insertSalePlanAndSalePlanVersion(SalePlan salePlan) throws BusinessException {

		try {
			/*SalePlan oldSalePlan = salePlanDAO.getSalePlanOfVNM(salePlan.getObjectId(), salePlan.getProduct().getId(), SalePlanOwnerType.SHOP, PlanType.MONTH, salePlan.getMonth(), salePlan.getYear());
			if (oldSalePlan != null) {
				throw new IllegalArgumentException("sale plan is exits, only update not insert");
			}*/
			SalePlan slePlan = salePlanDAO.createSalePlan(salePlan);
			//DuyNQ
			/*
			 * Long shopId = slePlan.getObjectId(); Shop shop =
			 * shopDAO.getShopById(shopId); Shop parentShop =
			 * shop.getParentShop(); SalePlan parentSalePlan =
			 * salePlanDAO.getSalePlanOfVNM( parentShop.getId(),
			 * slePlan.getProduct().getId(), SalePlanOwnerType.SHOP,
			 * PlanType.MONTH, slePlan.getMonth(), slePlan.getYear()); if
			 * (parentSalePlan != null) { Integer quantityAssign =
			 * parentSalePlan .getQuantityAssign(); if (quantityAssign == null)
			 * { quantityAssign = 0; }
			 * parentSalePlan.setQuantityAssign(quantityAssign +
			 * slePlan.getQuantity()); BigDecimal amountAssign = parentSalePlan
			 * .getAmountAssign(); if (amountAssign == null) { amountAssign =
			 * new BigDecimal(0); }
			 * parentSalePlan.setAmountAssign(amountAssign.add
			 * (slePlan.getAmount()));
			 * salePlanDAO.updateSalePlan(parentSalePlan); }
			 */
			SalePlanVersion salePlanVersion = new SalePlanVersion();
			salePlanVersion.setSalePlan(slePlan);
			salePlanVersion.setVersion(1);
			//salePlanVersion.setProduct(salePlan.getProduct());
			salePlanVersion.setQuantity(slePlan.getQuantity());
			salePlanVersionDAO.createSalePlanVersion(salePlanVersion);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SalePlanMgr#updateSalePlanAndSalePlanVesion
	 * (ths.dms.core.entities.SalePlan, long)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateSalePlanAndSalePlanVesion(SalePlan salePlan, String currentUser) throws BusinessException {
		try {
			if (salePlan == null) {
				throw new IllegalArgumentException("sale plan is null");
			}
			SalePlan oldSalePlan = null;
			if (SalePlanOwnerType.SHOP.equals(salePlan.getObjectType())) {
				/*
				 * oldSalePlan =
				 * salePlanDAO.getSalePlan(shopDAO.getShopById(salePlan
				 * .getObjectId()).getShopCode(),
				 * salePlan.getProduct().getProductCode(), salePlan.getMonth(),
				 * salePlan.getYear());
				 */
				//oldSalePlan = salePlanDAO.getSalePlanOfVNM(salePlan.getObjectId(), salePlan.getProduct().getId(), SalePlanOwnerType.SHOP, PlanType.MONTH, salePlan.getMonth(), salePlan.getYear());
			} else {
				//oldSalePlan = salePlanDAO.getSalePlan(staffDAO.getStaffById(salePlan.getObjectId()).getStaffCode(), salePlan.getProduct().getProductCode(), salePlan.getMonth(), salePlan.getYear());
			}
			if (oldSalePlan == null) {
				throw new IllegalArgumentException("sale plan is not exits");
			}
			Integer oldQuantity = oldSalePlan.getQuantity();
			if (oldQuantity == null) {
				oldQuantity = 0;
			}
			BigDecimal oldAmount = oldSalePlan.getAmount();
			if (oldAmount == null) {
				oldAmount = new BigDecimal(0);
			}
			salePlanDAO.updateSalePlan(salePlan);
			//DuyNQ
			//update parent
			/*
			 * Long shopId = salePlan.getObjectId(); Shop shop =
			 * shopDAO.getShopById(shopId); Shop parentShop =
			 * shop.getParentShop(); if (parentShop != null) { //chi phan bo cha
			 * truc tiep SalePlan parentSalePlan = salePlanDAO.getSalePlanOfVNM(
			 * parentShop.getId(), salePlan.getProduct().getId(),
			 * SalePlanOwnerType.SHOP, PlanType.MONTH, salePlan.getMonth(),
			 * salePlan.getYear()); if (parentSalePlan != null &&
			 * parentSalePlan.getQuantity() != null &&
			 * parentSalePlan.getQuantity() > 0) { Integer quantityAssign =
			 * parentSalePlan .getQuantityAssign(); if (quantityAssign == null)
			 * { quantityAssign = 0; }
			 * parentSalePlan.setQuantityAssign(salePlan.getQuantity() +
			 * quantityAssign - oldQuantity); BigDecimal amountAssign =
			 * parentSalePlan .getAmountAssign(); if (amountAssign == null) {
			 * amountAssign = new BigDecimal(0); }
			 * parentSalePlan.setAmountAssign
			 * ((amountAssign.add(salePlan.getAmount()).subtract(oldAmount)));
			 * salePlanDAO.updateSalePlan(parentSalePlan); } }
			 */
			SalePlanVersion salePlanVersion = new SalePlanVersion();
			salePlanVersion.setSalePlan(salePlan);
			salePlanVersion.setVersion(salePlanVersion.getVersion());
			//salePlanVersion.setProduct(salePlan.getProduct());
			salePlanVersion.setQuantity(salePlan.getQuantity());
			salePlanVersion.setCreateUser(currentUser);
			salePlanVersionDAO.createSalePlanVersion(salePlanVersion);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createSalePlanForShop(Long shopId, Integer num, Integer year, List<Long> lstProductId, List<Integer> lstQty, List<BigDecimal> lstAmount, String createUser) throws BusinessException {
		try {
			if (shopId == null || shopId < 0) {
				throw new IllegalArgumentException("shopId is null or shopId < 0");
			}
			if (num == null || num < 0) {
				throw new IllegalArgumentException("month is null or month < 0");
			}
			if (year == null || year < 0) {
				throw new IllegalArgumentException("year is null or year < 0");
			}
			if (lstProductId == null) {
				throw new IllegalArgumentException("lstProductId is null");
			}
			if (lstQty == null && lstAmount==null) {
				throw new IllegalArgumentException("lstQty, lstAmount is null");
			}
			if (lstProductId.size() != lstQty.size()) {
				throw new IllegalArgumentException("lstProductId.size() != lstQty.size()");
			}
			for (int i = 0; i < lstProductId.size(); i++) {
				Integer qty = lstQty.get(i);
				BigDecimal amt =lstAmount.get(i);
				if (qty == null || qty < 0) {
					throw new IllegalArgumentException("qty is null or qty < 0");
				}
				Long productId = lstProductId.get(i);
				if (productId == null || productId < 0) {
					throw new IllegalArgumentException("productId is null or productId < 0");
				}
				Price price = priceDAO.getPriceByProductId(productId);
				if (price == null) {
					throw new IllegalArgumentException("price is null");
				}
				SalePlan oldSalePlan = salePlanDAO.getSalePlanOfVNM(shopId, productId, SalePlanOwnerType.SHOP, PlanType.MONTH, num, year);
//				int oldQuantity = 0;
//				BigDecimal oldAmount = new BigDecimal(0);
				if (oldSalePlan != null) {
					/** @author tientv */
					//cap nhat sale plan
					SalePlan salePlan = oldSalePlan;
//						Integer oldQty = salePlan.getQuantity();
					//salePlan.setVersion(salePlan.getVersion() + 1);
//						BigDecimal oldAmt = salePlan.getAmount();
					salePlan.setQuantity(qty);
					salePlan.setAmount(amt);
					salePlanDAO.updateSalePlan(salePlan);
					// /////////////////////
					SalePlanVersion salePlanVersion = new SalePlanVersion();
					salePlanVersion.setSalePlan(salePlan);
					salePlanVersion.setVersion(salePlanVersionDAO.getMaxVersionBySalePlanSersionId(salePlan.getId())+1);
//						salePlanVersion.setMonthDate(salePlan.getMonthDate());
					salePlanVersion.setCycle(salePlan.getCycle());
					//salePlanVersion.setProduct(salePlan.getProduct());
					salePlanVersion.setQuantity(salePlan.getQuantity());
					salePlanVersion.setCreateUser(createUser);
					salePlanVersion.setAmount(amt);
					salePlanVersion.setCreateDate(commonDAO.getSysDate());
					salePlanVersionDAO.createSalePlanVersion(salePlanVersion);
				} else { // if (qty > 0) {
					//tao moi sale plan
					Product product = productDAO.getProductById(productId);
					SalePlan salePlan = new SalePlan();
					salePlan.setObjectType(SalePlanOwnerType.SHOP);
					salePlan.setObjectId(shopId);
					salePlan.setType(PlanType.MONTH);
					salePlan.setProduct(product);
					salePlan.setQuantity(qty);
					salePlan.setCreateUser(createUser);
					salePlan.setCreateDate(commonDAO.getSysDate());
					Shop shop = shopDAO.getShopById(shopId);
					//salePlan.setParent(shop.getParentShop());
					//add cat - bo di do update db
					//						ProductLevel productLevel = product.getProductLevel();
					//						if (productLevel != null) {
					//							ProductInfo productInfo = productLevel.getCat();
					//							salePlan.setCat(productInfo);
					//						}
					salePlan.setAmount(amt);
					
//						String dateInString = "01-"+month.toString()+"-"+year.toString();
//						Date monthDate  = DateUtility.parse(dateInString, "dd-MM-yyyy");
//						salePlan.setMonthDate(monthDate);
					
					Cycle cycle = cycleDAO.getCycleByYearAndNum(year, num);
					salePlan.setCycle(cycle);
					//salePlan.setPrice(price.getPrice());
					//salePlan.setVersion(1);
					salePlan.setShop(shop);
					salePlan.setCat(product.getCat());
					salePlan = salePlanDAO.createSalePlan(salePlan);
					// ///////////////
					SalePlanVersion salePlanVersion = new SalePlanVersion();
					salePlanVersion.setSalePlan(salePlan);
					salePlanVersion.setVersion(1);
					//salePlanVersion.setProduct(salePlan.getProduct());
					salePlanVersion.setQuantity(salePlan.getQuantity());
					salePlanVersion.setCreateUser(createUser);
					salePlanVersion.setCreateDate(commonDAO.getSysDate());
//						salePlanVersion.setMonthDate(monthDate);
					salePlanVersion.setCycle(salePlan.getCycle());
					salePlanVersion.setAmount(amt);
					salePlanVersionDAO.createSalePlanVersion(salePlanVersion);
				}
				//update salePlan parent
				/*
				 * DuyNQ Shop shop = shopDAO.getShopById(shopId); Shop
				 * parentShop = shop.getParentShop(); if (parentShop !=
				 * null) { //chi phan bo cha truc tiep SalePlan
				 * parentSalePlan = salePlanDAO.getSalePlanOfVNM(
				 * parentShop.getId(), productId, SalePlanOwnerType.SHOP,
				 * PlanType.MONTH, month, year); if (parentSalePlan != null)
				 * { Integer quantityAssign =
				 * parentSalePlan.getQuantityAssign(); if
				 * (parentSalePlan.getQuantity() != null &&
				 * parentSalePlan.getQuantity() > 0) {
				 * parentSalePlan.setQuantityAssign(qty + quantityAssign -
				 * oldQuantity); BigDecimal amountAssign = parentSalePlan
				 * .getAmountAssign(); if (amountAssign == null) {
				 * amountAssign = new BigDecimal(0); }
				 * parentSalePlan.setAmountAssign((amountAssign.add(price
				 * .getPrice().multiply(new
				 * BigDecimal(qty)))).subtract(oldAmount));
				 * salePlanDAO.updateSalePlan(parentSalePlan); } } //
				 * parentShop = parentShop.getParentShop(); }
				 */
				}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * THUATTQ
	 */
	//	@Override
	//	public List<Staff> getListStaffByShopIdSR(Long shopId) throws BusinessException {
	//		if(null == shopId){
	//			throw new IllegalArgumentException("shopId is null");
	//		}
	//		try {
	//			return staffDAO.getListStaffByShopId(shopId, null, ChannelTypeType.SR);
	//		} catch (DataAccessException e) {
	//			throw new BusinessException(e);
	//		}
	//	}

	@Override
	public ObjectVO<SalePlanVO> getListSalePlanVOProductOfShop(KPaging<SalePlanVO> kPaging, Long shopId, String staffCode, String catCode, String productCode, String productName, Integer month, Integer year, Integer fromQty, Integer toQty)
			throws BusinessException {
		try {

			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (month == null) {
				throw new IllegalArgumentException("month is null");
			}
			if (year == null) {
				throw new IllegalArgumentException("year is null");
			}
			ObjectVO<SalePlanVO> ret = new ObjectVO<SalePlanVO>();
			List<SalePlanVO> lst = salePlanDAO.getListSalePlanVOProductOfShop(kPaging, shopId, staffCode, catCode, productCode, productName, month, year, fromQty, toQty);
			ret.setLstObject(lst);
			ret.setkPaging(kPaging);
			return ret;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SalePlanMgr#getListSalePlanVOProductOfParentShop
	 * (ths.dms.core.entities.enumtype.KPaging, java.lang.Long,
	 * java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public ObjectVO<SalePlanVO> getListSalePlanVOProductOfParentShop(KPaging<SalePlanVO> kPaging, Long shopId, String productCode, String productName, Integer month, Integer year, Integer fromQty, Integer toQty) throws BusinessException {

		try {

			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (month == null) {
				throw new IllegalArgumentException("month is null");
			}
			if (year == null) {
				throw new IllegalArgumentException("year is null");
			}
			ObjectVO<SalePlanVO> ret = new ObjectVO<SalePlanVO>();
			List<SalePlanVO> lst = salePlanDAO.getListSalePlanVOProductOfParentShop(kPaging, shopId, productCode, productName, month, year, fromQty, toQty);
			ret.setLstObject(lst);
			ret.setkPaging(kPaging);
			return ret;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.business.SalePlanMgr#getSalePlanById(long)
	 */
	@Override
	public SalePlan getSalePlanById(long id) throws BusinessException {

		try {
			return salePlanDAO.getSalePlanById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SalePlanMgr#getSalePlanByCondition(java.lang
	 * .Long, java.lang.String, java.lang.Integer, java.lang.Integer,
	 * ths.dms.core.entities.enumtype.PlanType)
	 */
	@Override
	public SalePlan getSalePlanByCondition(Long ownerId, String catCode, Integer month, Integer year, PlanType type) throws BusinessException {

		try {
			return salePlanDAO.getSalePlanByCondition(ownerId, catCode, month, year, type);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SalePlanMgr#getListSalePlanForManagerPlan(com
	 * .viettel.core.entities.enumtype.KPaging, java.lang.Long, java.lang.Long,
	 * java.lang.Long, java.util.Date)
	 */
	@Override
	public ObjectVO<SalesPlan> getListSalePlanForManagerPlan(KPaging<SalesPlan> kPaging, Long shopId, Long staffId, Long productId, Integer month, Integer year) throws BusinessException {

		try {
			Date date = new Date();
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String stringDate = "";
				if (month < 10) {
					stringDate = year + "0" + month + "01";
				} else {
					stringDate = "" + year + month + "01";
				}
				date = sdf.parse(stringDate);
			} catch (ParseException e) {
				throw new BusinessException(e);
			}
			ObjectVO<SalesPlan> object = new ObjectVO<SalesPlan>();
			List<SalesPlan> list = salesPlanDAO.getListSalePlanForManagerPlan(kPaging, shopId, staffId, productId, date);
			object.setkPaging(kPaging);
			object.setLstObject(list);
			return object;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SalePlanMgr#getListSalePlanForManagerPlanSearchLike
	 * (ths.dms.core.entities.enumtype.KPaging, java.lang.Long,
	 * java.lang.Long, java.lang.String, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public ObjectVO<SalesPlan> getListSalePlanForManagerPlanSearchLike(KPaging<SalesPlan> kPaging, Long shopId, Long staffId, String productCode, Integer month, Integer year) throws BusinessException {

		try {
			Date date = new Date();
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
				String stringDate = "";
				if (month < 10) {
					stringDate = year + "0" + month + "01";
				} else {
					stringDate = "" + year + month + "01";
				}
				date = sdf.parse(stringDate);
			} catch (ParseException e) {
				throw new BusinessException(e);
			}
			ObjectVO<SalesPlan> object = new ObjectVO<SalesPlan>();
			List<SalesPlan> list = salesPlanDAO.getListSalePlanForManagerPlanSearchLike(kPaging, shopId, staffId, productCode, date);
			object.setkPaging(kPaging);
			object.setLstObject(list);
			return object;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SalePlanMgr#getSalePlanOfVNM(java.lang.Long,
	 * java.lang.Long, ths.dms.core.entities.enumtype.SalePlanOwnerType,
	 * ths.dms.core.entities.enumtype.PlanType, java.lang.Integer,
	 * java.lang.Integer)
	 */
	@Override
	public SalePlan getSalePlanOfVNM(Long ownerId, Long productId, SalePlanOwnerType ownerType, PlanType planType, Integer month, Integer year) throws BusinessException {
		try {
			return salePlanDAO.getSalePlanOfVNM(ownerId, productId, ownerType, planType, month, year);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkSalePlan(String objectCode, SalePlanOwnerType objectType, String productCode, Integer month, Integer year) throws BusinessException {
		try {
			return salePlanDAO.checkSalePlan(objectCode, objectType, productCode, month, year);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<DistributeSalePlanVO> getListSalePlanWithNoCondition(KPaging<DistributeSalePlanVO> kPaging, String productCode, String productName) throws BusinessException {
		try {
			ObjectVO<DistributeSalePlanVO> object = new ObjectVO<DistributeSalePlanVO>();
			List<DistributeSalePlanVO> list = salePlanDAO.getListSalePlanWithNoCondition(kPaging, productCode, productName);
			object.setkPaging(kPaging);
			object.setLstObject(list);
			return object;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<SalePlan> getListSalePlan(KPaging<SalePlan> paging, String staffCode, List<String> lstCatCode, String productCode, Integer month, Integer year) throws BusinessException {
		try {
			ObjectVO<SalePlan> object = new ObjectVO<SalePlan>();
			List<SalePlan> list = salePlanDAO.getListSalePlan(paging, staffCode, lstCatCode, productCode, month, year);
			object.setkPaging(paging);
			object.setLstObject(list);
			return object;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public SalePlan getSalePlan(String staffCode, String productCode, Integer num, Integer year) throws BusinessException {
		try {
			return salePlanDAO.getSalePlan(staffCode, productCode, num, year);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<SalePlanVO> getListSalePlanVOForLoadPage(KPaging<SalePlanVO> paging, Long shopId, Integer month, Integer year) throws BusinessException {

		try {
			ObjectVO<SalePlanVO> ret = new ObjectVO<SalePlanVO>();
			List<SalePlanVO> lst = salePlanDAO.getListSalePlanVOForLoadPage(paging, shopId, month, year);
			ret.setLstObject(lst);
			ret.setkPaging(paging);
			return ret;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Integer getVersion(Long ownerId, Long productId, SalePlanOwnerType ownerType, PlanType planType, Integer month, Integer year) throws BusinessException {
		try {
			return salePlanDAO.getVersion(ownerId, productId, ownerType, planType, month, year);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<DynamicStaffKpiVO> getListDynamicStaffKpi(Long shopId,
			Long cycleId) throws BusinessException {
		try{
			List<Object> inParams = new ArrayList<Object>();
			inParams.add(2);
			inParams.add(shopId);
			inParams.add(java.sql.Types.NUMERIC);
			inParams.add(3);
			inParams.add(cycleId);
			inParams.add(java.sql.Types.NUMERIC);
			String sql  ="";
			sql = "call PKG_KPI.GET_LIST_STAFF_KPI(?, ? , ?)";
			return repo.getListByQueryDynamicFromPackage(DynamicStaffKpiVO.class, sql, inParams, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		
	}

	@Override
	public ObjectVO<Kpi> getListKpiByFilter(KPaging<Kpi> kPaging,
			CommonFilter filter) throws BusinessException {
		try {
			ObjectVO<Kpi> ret = new ObjectVO<Kpi>();
			List<Kpi> lst = kpiDAO.getListKpiByFilter(kPaging, filter);
			ret.setLstObject(lst);
			ret.setkPaging(kPaging);
			return ret;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void createOrUpdateListKi(List<DynamicStaffKpiSaveVO> lstKpiInfo, LogInfoVO log)
			throws BusinessException {
		List<KpiImplement> lstKiCreate = new ArrayList<KpiImplement>();
		List<KpiImplement> lstKiUpdate = new ArrayList<KpiImplement>();
		try{
			for (int i = 0, n = lstKpiInfo.size(); i < n; i++){
				DynamicStaffKpiSaveVO row = lstKpiInfo.get(i); 
				if (row.getLstKpiData() != null && row.getLstKpiData().size() > 0){
					Staff st = staffDAO.getStaffById(row.getStaffId());
					Shop sh = st.getShop();
					Cycle c = cycleDAO.getCycleByYearAndNum(row.getYear(), row.getPeriod());
					Date sysDate = commonDAO.getSysDate();
					for (DynamicBigVO vo: row.getLstKpiData()){
						KpiImplement ki = null;
						Kpi kpi = commonDAO.getEntityById(Kpi.class, Long.parseLong(vo.getKey()));
						if (st != null && sh != null && c != null && kpi != null){
							ki = kpiImplementDAO.getKpiImplement(st.getId(), kpi.getId(), c.getId());
							if (ki != null){//update
								ki.setPlan(vo.getValue());
//								ki.setPlan(new BigDecimal(vo.getValue().toString()));
								ki.setUpdateDate(sysDate);
								ki.setUpdateUser(log.getStaffCode());
								lstKiUpdate.add(ki);
							} else {//create
								ki = new KpiImplement();
								ki.setKpi(kpi);
								ki.setStaff(st);
								ki.setShop(sh);
								ki.setCycle(c);
								ki.setPlan(vo.getValue());
//								ki.setPlan(new BigDecimal(vo.getValue().toString()));
								ki.setCreateUser(log.getStaffCode());
								ki.setCreateDate(sysDate);
								lstKiCreate.add(ki);
							}
							
							
						}
					}
						
				}
			}
			if (lstKiCreate != null && lstKiCreate.size() > 0) {
				commonDAO.creatListEntity(lstKiCreate);
			}
			
			if (lstKiUpdate != null && lstKiUpdate.size() > 0) {
				commonDAO.updateListEntity(lstKiUpdate);
			}
			
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		
		
	}

	@Override
	public KpiImplement getKpiImplement(Long staffId, Long kpiId, Long cycleId)
			throws BusinessException {
		try {
			return kpiImplementDAO.getKpiImplement(staffId, kpiId, cycleId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

}

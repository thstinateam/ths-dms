package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.filter.CycleFilter;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.CycleDAO;

public class CycleMgrImpl implements CycleMgr {
	
	@Autowired
	CycleDAO cycleDAO;

	@Override
	public Cycle createCycle(Cycle cycle, LogInfoVO logInfo)
			throws BusinessException {
		try {
			return cycleDAO.createCycle(cycle, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateCycle(Cycle cycle, LogInfoVO logInfo)
			throws BusinessException {
		try {
			cycleDAO.updateCycle(cycle, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteCycle(Cycle cycle, LogInfoVO logInfo)
			throws BusinessException {
		try {
			cycleDAO.deleteCycle(cycle, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Cycle getCycleById(Long id) throws BusinessException {
		try {
			return id == null ? null : cycleDAO.getCycleById(id);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Cycle getCycleByCurrentSysdate() throws BusinessException {
		try {
			return cycleDAO.getCycleByCurrentSysdate();
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Cycle getCycleByDate(Date date) throws BusinessException {
		try {
			return cycleDAO.getCycleByDate(date);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Cycle getCycleByYearAndNum(int year, int num) throws BusinessException {
		try {
			return cycleDAO.getCycleByYearAndNum(year, num);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Integer> getListYearOfAllCycle() throws BusinessException {
		try {
			return cycleDAO.getListYearOfAllCycle();
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CycleVO> getListCycleByFilter(CycleFilter filter) throws BusinessException {
		try {
			return cycleDAO.getListCycleByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<CycleVO> getListKPIByCycle(Long cycleId) throws BusinessException {
		try {
			return cycleDAO.getListKPIByCycle(cycleId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public List<CycleVO> getListKeyShopByCycle(Long cycleId, List<Long> lstKeyShopId, String fromDate, String toDate) throws BusinessException {
		try {
			return cycleDAO.getListKeyShopByCycle(cycleId, lstKeyShopId, fromDate, toDate);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Cycle getCycleByFilter(CycleFilter filter) throws BusinessException {
		try {
			return cycleDAO.getCycleByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Cycle getCycleLast(Long cycleId) throws BusinessException {
		try {
			return cycleDAO.getCycleLast(cycleId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

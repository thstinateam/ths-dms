package ths.dms.core.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.EquipEvictionFormDtl;
import ths.dms.core.entities.EquipRole;
import ths.dms.core.entities.EquipRoleDetail;
import ths.dms.core.entities.EquipSalePlan;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.filter.EquipmentRoleDetailFilter;
import ths.dms.core.entities.vo.EquipProposalBorrowVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentEvictionVO;
import ths.dms.core.entities.vo.EquipmentRoleStockTempVO;
import ths.dms.core.entities.vo.EquipmentRoleVO;
import ths.dms.core.entities.vo.EquipmentRoleVOTempImport;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StockTransLotVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.EquipProposalBorrowDAO;
import ths.dms.core.dao.EquipStockPermissionDAO;

public class EquipStockPermissionMgrImpl implements EquipStockPermissionMgr {
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	EquipStockPermissionDAO equipStockPermissionDAO;

	@Override
	public ObjectVO<EquipProposalBorrowVO> searchListStockPermissionVOByFilter(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws BusinessException {
		try {
			List<EquipProposalBorrowVO> lst = equipStockPermissionDAO.searchListStockPermissionVOByFilter(filter);
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/***
	 * @author vuongmq
	 * @date Mar 23,2015
	 * @description lay danh sach them cho equip_role left join equip_role_detail de xuat excel quan lý quyen kho thiet bi
	 */
	@Override
	public ObjectVO<EquipProposalBorrowVO> searchListStockPermissionVOByFilterExport(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws BusinessException {
		try {
			List<EquipProposalBorrowVO> lst = equipStockPermissionDAO.searchListStockPermissionVOByFilterExport(filter);
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipProposalBorrowVO> searchStockByFilter(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws BusinessException {
		try {
			List<EquipProposalBorrowVO> lst = equipStockPermissionDAO.searchStockByFilter(filter);
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipRole getEquipRoleByCodeEx(String code, ActiveType status) throws BusinessException {
		try {
			return equipStockPermissionDAO.getEquipRoleByCodeEx(code, status);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipRole createStockPermission(EquipRole eq, LogInfoVO logInfoVO) throws BusinessException {
		try {
			return equipStockPermissionDAO.createStockPermission(eq, logInfoVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipRole getStockPermissionById(Long id) throws BusinessException {
		try {
			return id==null?null:equipStockPermissionDAO.getStockPermissionById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveStockPermission(EquipRole eq, LogInfoVO logInfoVO) throws BusinessException {
		try {			
			eq = equipStockPermissionDAO.saveStockPermission(eq, logInfoVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteStockPermission(EquipRole eq) throws BusinessException {
		try {			
			List<EquipRoleDetail> lstDetail = equipStockPermissionDAO.getListEquipRoleDetailByEquipRoleId(eq.getId());
			if (lstDetail != null && lstDetail.size() > 0) {
				for (int i = 0; i < lstDetail.size(); i++) {
					EquipRoleDetail eqr = equipStockPermissionDAO.getEquipRoleDetailById(lstDetail.get(i).getId());
					equipStockPermissionDAO.deleteEquipRoleDetail(eqr);
				}
			}
			equipStockPermissionDAO.deleteStockPermission(eq);
			
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}		
	}

	@Override
	public ObjectVO<EquipProposalBorrowVO> getListtEquipStockByEquipRoleId(KPaging<EquipProposalBorrowVO> kPaging, Long id) throws BusinessException {
		try {
			List<EquipProposalBorrowVO> lst=equipStockPermissionDAO.getListtEquipStockByEquipRoleId(kPaging, id);
			ObjectVO<EquipProposalBorrowVO> vo = new ObjectVO<EquipProposalBorrowVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		}
		catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<EquipProposalBorrowVO> searchListStockPermissionVOAdd(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws BusinessException {
		try {
			List<EquipProposalBorrowVO> lst = equipStockPermissionDAO.searchListStockPermissionVOAdd(filter);
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void createListStockRoleDetail(EquipmentRoleDetailFilter<EquipmentRoleVO> filter, EquipRole eq) throws BusinessException {
		try {
			//danh sach kho da check
			if (filter.getListCheckStock() != null && !filter.getListCheckStock().isEmpty()) {
				int size = filter.getListCheckStock().size();
				for (int i = 0; i < size; i++) {
					EquipStock eqs = equipStockPermissionDAO.getEquipStockById(filter.getListCheckStock().get(i));
					if (eqs != null) {
						EquipRoleDetail eqr = new EquipRoleDetail();
						eqr.setEquipStock(eqs);
						eqr.setCreateDate(commonDAO.getSysDate());
						eqr.setCreateUser(eq.getCreateUser());
						eqr.setEquipRole(eq);
						eqr.setIsUnder(1);
						equipStockPermissionDAO.createStockPermission(eqr);
					}
				}
			}
			//			danh sach kho chua check
			if (filter.getListNoCheckStock() != null && !filter.getListNoCheckStock().isEmpty()) {
				int size = filter.getListNoCheckStock().size();
				for (int i = 0; i < size; i++) {
					EquipStock eqs = equipStockPermissionDAO.getEquipStockById(filter.getListNoCheckStock().get(i));
					if (eqs != null) {
						EquipRoleDetail eqr = new EquipRoleDetail();
						eqr.setEquipStock(eqs);
						eqr.setCreateDate(commonDAO.getSysDate());
						eqr.setCreateUser(eq.getCreateUser());
						eqr.setEquipRole(eq);
						eqr.setIsUnder(0);
						equipStockPermissionDAO.createStockPermission(eqr);
					}
				}
			}
			if (filter.getListmapIdStock() != null && !filter.getListmapIdStock().isEmpty()) {
				int size = filter.getListmapIdStock().size();
				for (int i = 0; i < size; i++) {
					EquipStock eqs = equipStockPermissionDAO.getEquipStockById(filter.getListmapIdStock().get(i));
					if (eqs != null) {
						EquipRoleDetail eqr = new EquipRoleDetail();
						eqr.setEquipStock(eqs);
						eqr.setCreateDate(commonDAO.getSysDate());
						eqr.setCreateUser(eq.getCreateUser());
						eqr.setEquipRole(eq);
						eqr.setIsUnder(0);
						equipStockPermissionDAO.createStockPermission(eqr);
					}
				}
			}

		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipRoleDetail getListEquipRoleDetailById(Long id) throws BusinessException {
		try {
			return id==null?null:equipStockPermissionDAO.getListEquipRoleDetailById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteEquipRoleDetail(EquipRoleDetail edt) throws BusinessException {
		try {
			equipStockPermissionDAO.deleteEquipRoleDetail(edt);
		} catch (Exception e) {

			throw new BusinessException(e);
		}	
		
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateStockPermissionVODetail(Long idRole, List<Long> lstStockNow,  List<Long> listMapEditIdStock, List<EquipmentRoleStockTempVO> checkListStockVO, List<EquipmentRoleStockTempVO> noCheckListStockVO, LogInfoVO logInfoVO)throws BusinessException {
		try {
			Date sysDate = commonDAO.getSysDate();
			/** lay list checkListStock */
			if(listMapEditIdStock != null && listMapEditIdStock.size() >0){
				for (Long voTemp : listMapEditIdStock) {
					Long id = voTemp;
					if(lstStockNow.contains(id)){
						// ton tai thi khong update					
					} else {
						/** create check;  them moi id: voTemp.getIdDetail() la mac dinh bang 0 duoi js**/ 
						EquipRoleDetail eRDetailNew = new EquipRoleDetail();
						EquipRole equipRole = commonDAO.getEntityById(EquipRole.class, idRole);
						eRDetailNew.setEquipRole(equipRole);
						if(voTemp != null){
							EquipStock equipStock = commonDAO.getEntityById(EquipStock.class, voTemp);
							if(equipStock != null){
								eRDetailNew.setEquipStock(equipStock);
							}
						}
						eRDetailNew.setIsUnder(ActiveType.STOPPED.getValue());
						eRDetailNew.setCreateDate(sysDate);
						eRDetailNew.setCreateUser(logInfoVO.getStaffCode());
						
						commonDAO.createEntity(eRDetailNew);
						
					}
				}
			}
			if (checkListStockVO != null && checkListStockVO.size() > 0) {
				for (EquipmentRoleStockTempVO voTemp : checkListStockVO) {
					Long id = voTemp.getIdDetail();
					if(lstStockNow.contains(id)){
						// ton tai update check
						EquipRoleDetail eRDetail = commonDAO.getEntityById(EquipRoleDetail.class, id);
						if(eRDetail != null){
							if(eRDetail.getEquipStock() != null 
								&& eRDetail.getEquipStock().getId().equals(voTemp.getIdStock())
								&& ActiveType.STOPPED.getValue().equals(eRDetail.getIsUnder())){
									eRDetail.setIsUnder(ActiveType.RUNNING.getValue());
									eRDetail.setUpdateUser(logInfoVO.getStaffCode());
									eRDetail.setUpdateDate(sysDate);
									commonDAO.updateEntity(eRDetail);
							}
						}
					} else {
						/** create check;  them moi id: voTemp.getIdDetail() la mac dinh bang 0 duoi js**/ 
						EquipRoleDetail eRDetailNew = new EquipRoleDetail();
						EquipRole equipRole = commonDAO.getEntityById(EquipRole.class, idRole);
						eRDetailNew.setEquipRole(equipRole);
						if(voTemp.getIdStock() != null){
							EquipStock equipStock = commonDAO.getEntityById(EquipStock.class, voTemp.getIdStock());
							if(equipStock != null){
								eRDetailNew.setEquipStock(equipStock);
							}
						}
						eRDetailNew.setIsUnder(ActiveType.RUNNING.getValue());
						eRDetailNew.setCreateDate(sysDate);
						eRDetailNew.setCreateUser(logInfoVO.getStaffCode());
						
						commonDAO.createEntity(eRDetailNew);
						
					}
				}
			}
			/** lay list noCheckListStock */
			if (noCheckListStockVO != null && noCheckListStockVO.size() > 0) {
				for (EquipmentRoleStockTempVO voTemp : noCheckListStockVO) {
					Long id = voTemp.getIdDetail();
					if(lstStockNow.contains(id)){
						// ton tai update NO check
						EquipRoleDetail eRDetail = commonDAO.getEntityById(EquipRoleDetail.class, id);
						if(eRDetail != null){
							if(eRDetail.getEquipStock() != null 
								&& eRDetail.getEquipStock().getId().equals(voTemp.getIdStock())
								&& ActiveType.RUNNING.getValue().equals(eRDetail.getIsUnder())){
									eRDetail.setIsUnder(ActiveType.STOPPED.getValue());
									eRDetail.setUpdateUser(logInfoVO.getStaffCode());
									eRDetail.setUpdateDate(sysDate);
									commonDAO.updateEntity(eRDetail);
							}
						}
						
					} else {
						/** create NO check;  them moi id: voTemp.getIdDetail() la mac dinh bang 0 duoi js**/ 
						EquipRoleDetail eRDetailNew = new EquipRoleDetail();
						EquipRole equipRole = commonDAO.getEntityById(EquipRole.class, idRole);
						eRDetailNew.setEquipRole(equipRole);
						if(voTemp.getIdStock() != null){
							EquipStock equipStock = commonDAO.getEntityById(EquipStock.class, voTemp.getIdStock());
							if(equipStock != null){
								eRDetailNew.setEquipStock(equipStock);
							}
						}
						eRDetailNew.setIsUnder(ActiveType.STOPPED.getValue());
						eRDetailNew.setCreateDate(sysDate);
						eRDetailNew.setCreateUser(logInfoVO.getStaffCode());
						
						commonDAO.createEntity(eRDetailNew);
					}
				}
			}
		} catch (Exception e) {			
			throw new BusinessException(e);
		}
		
	}

	@Override
	public ObjectVO<EquipProposalBorrowVO> getListEquipRoleDetailByStockIdAndRoleId(Long idcheck, Long id) throws BusinessException {
		try {
			List<EquipProposalBorrowVO> lst = equipStockPermissionDAO.getListEquipRoleDetailByStockIdAndRoleId(idcheck, id);
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();		
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEquipRoleDetail(EquipRoleDetail eqrd, LogInfoVO logInfoVO) throws BusinessException {
		try {			
			eqrd = equipStockPermissionDAO.updateEquipRoleDetail(eqrd, logInfoVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipStock getEquipStockById(Long idcheck) throws BusinessException {
		try {
			return idcheck==null?null:equipStockPermissionDAO.getEquipStockById(idcheck);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public void createEquipRoleDetail(EquipRoleDetail eRDetailNew) throws BusinessException {
		try {
			 equipStockPermissionDAO.createEquipRoleDetail(eRDetailNew);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipProposalBorrowVO> searchListStockChildPermissionEdit(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws BusinessException {
		try {
			if (filter.getLstInsertId() != null) {
				for (int i = 0, sz = filter.getLstInsertId().size(); i < sz; i++) {
					
				}
			}	
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();
			return objVO;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}	
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipRole createEquipRoleAndDetail(List<EquipmentRoleVOTempImport> lstRoleExcel, LogInfoVO logInfoVO) throws BusinessException {
		try {
			EquipRole equipRole = null;
			Date sys = commonDAO.getSysDate();
			if(lstRoleExcel != null && lstRoleExcel.size() > 0){
				/** lay dong dau tien; co ma quyen code them EquipRole*/
				if(lstRoleExcel.get(0) != null && lstRoleExcel.get(0).getCode() != null && lstRoleExcel.get(0).getName() != null){
					equipRole = new EquipRole();
					EquipmentRoleVOTempImport vo = lstRoleExcel.get(0);
					equipRole.setCode(vo.getCode().toUpperCase());
					equipRole.setName(vo.getName());
					equipRole.setStatus(vo.getStatus());
					equipRole.setCreateDate(sys);
					equipRole.setCreateUser(logInfoVO.getStaffCode());
					equipRole = commonDAO.createEntity(equipRole);
				}
				for (EquipmentRoleVOTempImport equipmentRoleVOTempImport : lstRoleExcel) {
					if (equipmentRoleVOTempImport != null && equipmentRoleVOTempImport.getStockCode() != null) {
						/** lay cac dong co ma kho stockCode != null them EquipRoleDetail cho EquipRole */
						EquipRoleDetail equipRoleDetail = new EquipRoleDetail();
						equipRoleDetail.setEquipRole(equipRole);
						equipRoleDetail.setEquipStock(equipmentRoleVOTempImport.getEquipStock());
						equipRoleDetail.setCreateDate(sys);
						equipRoleDetail.setCreateUser(logInfoVO.getStaffCode());
						if (equipmentRoleVOTempImport.getViewUnder() != null && ActiveType.RUNNING.getValue().equals(equipmentRoleVOTempImport.getViewUnder())) {
							equipRoleDetail.setIsUnder(ActiveType.RUNNING.getValue());
						} else {
							equipRoleDetail.setIsUnder(ActiveType.STOPPED.getValue());
						}
						commonDAO.createEntity(equipRoleDetail);
					}
				}
			}
			return equipRole;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

	}
	@Override
	public ObjectVO<EquipProposalBorrowVO> searchLstAddStockId(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws BusinessException {
		try {
			List<Long> lstStockNow = new ArrayList<Long>();
			List<EquipProposalBorrowVO> lstAddShopId = new ArrayList<EquipProposalBorrowVO>();
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();
			if (filter.getLstAddStockId() != null && !filter.getLstAddStockId().isEmpty()) {
				int size = filter.getLstAddStockId().size();
				for (int i = 0; i < size; i++) {
					EquipStock lstShopId = equipStockPermissionDAO.getEquipStockById(filter.getLstAddStockId().get(i));
					if (lstShopId != null) {
						if (lstShopId != null) {
							lstStockNow.add(lstShopId.getShop().getId());
						}
					}
				}
				for (int i = 0; i < size; i++) {
					EquipStock lstShopId = equipStockPermissionDAO.getEquipStockById(filter.getLstAddStockId().get(i));
					if (lstShopId != null) {
						List<EquipProposalBorrowVO> lst = equipStockPermissionDAO.searchLstAddStockId(lstShopId.getShop().getId());
						if (lst != null && lst.size() > 0) {
							for (EquipProposalBorrowVO voTemp : lst) {
								Long id = voTemp.getId();
								if (lstStockNow.contains(id)) {
									lstAddShopId.add(lst.get(i));
								}
							}
						}
					}
				}
			}
			objVO.setLstObject(lstAddShopId);
			return objVO;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public ObjectVO<EquipProposalBorrowVO> searchCheckAddStockChild(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws BusinessException {
		try {
			List<Long> lstShopIdOld = new ArrayList<Long>();
			List<EquipProposalBorrowVO> lstAddShopId = new ArrayList<EquipProposalBorrowVO>();
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();
			if (filter.getLstAddStockIdInsert() != null && !filter.getLstAddStockIdInsert().isEmpty()) {
				int size = filter.getLstAddStockIdInsert().size();
				for (int i = 0; i < size; i++) {
					EquipStock lstShopId = equipStockPermissionDAO.getEquipStockById(filter.getLstAddStockIdInsert().get(i));
					if (lstShopId != null) {
						if (lstShopId != null) {
							lstShopIdOld.add(lstShopId.getShop().getId());
						}
					}
				}
			}
			if (filter.getLstAddStockId() != null && !filter.getLstAddStockId().isEmpty()) {
				int size = filter.getLstAddStockId().size();
				for (int i = 0; i < size; i++) {
					EquipStock lstShopId = equipStockPermissionDAO.getEquipStockById(filter.getLstAddStockId().get(i));
					if (lstShopId != null) {
						List<EquipProposalBorrowVO> lst = equipStockPermissionDAO.searchLstAddStockId(lstShopId.getShop().getId());
						if (lst != null && lst.size() > 0) {
							for (EquipProposalBorrowVO voTemp : lst) {
								Long id = voTemp.getId();
								if (lstShopIdOld.contains(id)) {
									lstAddShopId.add(lst.get(i));
								}
							}
						}
						List<EquipProposalBorrowVO> lstParenId = equipStockPermissionDAO.searchLstParenIdAddStockId(lstShopId.getShop().getId());
						if (lstParenId != null && lstParenId.size() > 0) {
							for (EquipProposalBorrowVO voTemp : lstParenId) {
								Long id = voTemp.getId();
								if (lstShopIdOld.contains(id)) {
									lstAddShopId.add(lstParenId.get(i));
								}
							}
						}
					}
				}
			}
			objVO.setLstObject(lstAddShopId);
			return objVO;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public ObjectVO<EquipProposalBorrowVO> checkParenIdStock(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws BusinessException {
		try {
			List<EquipProposalBorrowVO> lstAddShopId = new ArrayList<EquipProposalBorrowVO>();
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();
			if (filter.getLstAddStockId() != null && !filter.getLstAddStockId().isEmpty()) {
				int size = filter.getLstAddStockId().size();
				for (int i = 0; i < size; i++) {
					EquipStock lstShopId = equipStockPermissionDAO.getEquipStockById(filter.getLstAddStockId().get(i));
					if (lstShopId != null) {
						List<EquipProposalBorrowVO> lst = equipStockPermissionDAO.searchLstAddStockId(lstShopId.getShop().getId());
						if (lst != null && lst.size() > 0) {
							lstAddShopId.add(lst.get(i));
						}
					}
				}
			}
			objVO.setLstObject(lstAddShopId);
			return objVO;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipProposalBorrowVO> getListEquipRoleDetailByEquipRoleCode(Long id) throws BusinessException {
		try {
			List<EquipProposalBorrowVO> lst = equipStockPermissionDAO.getListEquipRoleDetailByEquipRoleCode(id);
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();		
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public ObjectVO<EquipProposalBorrowVO> checkStockPermissionAdd(List<Long> listCheckStock, List<Long> listNoCheckStock, List<Long> listmapIdStock) throws BusinessException {
		try {
			List<Long> lstStockNow = new ArrayList<Long>();
			List<EquipProposalBorrowVO> lstAddShopId = new ArrayList<EquipProposalBorrowVO>();
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();
			if (listCheckStock != null && !listCheckStock.isEmpty()) {
				int size = listCheckStock.size();
				for (int i = 0; i < size; i++) {
					EquipStock lstShopId = equipStockPermissionDAO.getEquipStockById(listCheckStock.get(i));
					if (lstShopId != null) {
						if (lstShopId != null) {
							lstStockNow.add(lstShopId.getShop().getId());
						}
					}
				}
			}
			if (listNoCheckStock != null && !listNoCheckStock.isEmpty()) {
				int size = listNoCheckStock.size();
				for (int i = 0; i < size; i++) {
					EquipStock lstShopId = equipStockPermissionDAO.getEquipStockById(listNoCheckStock.get(i));
					if (lstShopId != null) {
						if (lstShopId != null) {
							lstStockNow.add(lstShopId.getShop().getId());
						}
					}
				}
			}
			if (listmapIdStock != null && !listmapIdStock.isEmpty() && listCheckStock != null && !listCheckStock.isEmpty()) {
				int size = listmapIdStock.size();
				for (int i = 0; i < size; i++) {
					EquipStock lstShopId = equipStockPermissionDAO.getEquipStockById(listmapIdStock.get(i));
					if (lstShopId != null) {
						List<EquipProposalBorrowVO> lst = equipStockPermissionDAO.searchLstAddStockId(lstShopId.getShop().getId());
						if (lst != null && lst.size() > 0) {
							for (EquipProposalBorrowVO voTemp : lst) {
								Long id = voTemp.getId();
								if (lstStockNow.contains(id)) {
									lstAddShopId.add(lst.get(i));
								}
							}
						}
						List<EquipProposalBorrowVO> lstParenId = equipStockPermissionDAO.searchLstParenIdAddStockId(lstShopId.getShop().getId());
						if (lstParenId != null && lstParenId.size() > 0) {
							for (EquipProposalBorrowVO voTemp : lstParenId) {
								Long id = voTemp.getId();
								if (lstStockNow.contains(id)) {
									lstAddShopId.add(lstParenId.get(i));
								}
							}
						}
					}
				}
			}
			objVO.setLstObject(lstAddShopId);
			return objVO;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public ObjectVO<EquipProposalBorrowVO> checkStockPermissionEdit(List<Long> listMapEditIdStock, List<Long> listEditIdRoleStock, List<EquipmentRoleStockTempVO> checkListStockVO, List<EquipmentRoleStockTempVO> noCheckListStockVO)
			throws BusinessException {
		try {
			List<Long> lstCheckStockNow = new ArrayList<Long>();
			List<Long> lstNoCheckStockNow = new ArrayList<Long>();
			List<EquipProposalBorrowVO> lstAddShopId = new ArrayList<EquipProposalBorrowVO>();
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();
			if (listMapEditIdStock != null && !listMapEditIdStock.isEmpty()) {
				int size = listMapEditIdStock.size();
				for (int i = 0; i < size; i++) {
					EquipStock lstShopId = equipStockPermissionDAO.getEquipStockById(listMapEditIdStock.get(i));
					if (lstShopId != null) {
						if (lstShopId != null) {
							lstNoCheckStockNow.add(lstShopId.getShop().getId());
						}
					}
				}
			}			
			if (noCheckListStockVO != null && !noCheckListStockVO.isEmpty()) {
				int size = noCheckListStockVO.size();
				for (int i = 0; i < size; i++) {
					EquipStock lstShopId = equipStockPermissionDAO.getEquipStockById(noCheckListStockVO.get(i).getIdStock());
					if (lstShopId != null) {
						if (lstShopId != null) {
							lstNoCheckStockNow.add(lstShopId.getShop().getId());
						}
					}
				}
			}
			if (checkListStockVO != null && !checkListStockVO.isEmpty()) {
				int size = checkListStockVO.size();
				for (int i = 0; i < size; i++) {
					EquipStock lstShopId = equipStockPermissionDAO.getEquipStockById(checkListStockVO.get(i).getIdStock());
					if (lstShopId != null) {
						if (lstShopId != null) {
							lstCheckStockNow.add(lstShopId.getShop().getId());
						}
					}
				}
			}
			if (listEditIdRoleStock != null && !listEditIdRoleStock.isEmpty()) {
				int size = listEditIdRoleStock.size();
				for (int i = 0; i < size; i++) {
					EquipStock lstShopId = equipStockPermissionDAO.getEquipStockById(listEditIdRoleStock.get(i));
					if (lstShopId != null) {
						if (lstCheckStockNow.contains(lstShopId.getShop().getId())) {
						} else {
							lstNoCheckStockNow.add(lstShopId.getShop().getId());
						}
					}
				}
			}

			if (lstCheckStockNow != null && !lstCheckStockNow.isEmpty()) {
				int size = lstCheckStockNow.size();
				for (int i = 0; i < size; i++) {			
						List<EquipProposalBorrowVO> lst = equipStockPermissionDAO.searchLstAddStockId(lstCheckStockNow.get(i));
						if (lst != null && lst.size() > 0) {
							for (EquipProposalBorrowVO voTemp : lst) {
								Long id = voTemp.getId();
								if (lstNoCheckStockNow.contains(id)) {
									lstAddShopId.add(lst.get(i));
								}
							}
						}
						List<EquipProposalBorrowVO> lstParenId = equipStockPermissionDAO.searchLstParenIdAddStockId(lstCheckStockNow.get(i));
						if (lstParenId != null && lstParenId.size() > 0) {
							for (EquipProposalBorrowVO voTemp : lstParenId) {
								Long id = voTemp.getId();
								if (lstCheckStockNow.contains(id)) {
									lstAddShopId.add(lstParenId.get(i));
								}
							}
						}					
				}
			}
			objVO.setLstObject(lstAddShopId);
			return objVO;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public ObjectVO<EquipProposalBorrowVO> listEquipRoleById(Long id)throws BusinessException {
		try {
			List<EquipProposalBorrowVO> lst = equipStockPermissionDAO.listEquipRoleById(id);
			ObjectVO<EquipProposalBorrowVO> objVO = new ObjectVO<EquipProposalBorrowVO>();		
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public List<EquipProposalBorrowVO> searchCheckShopId(Long id) throws BusinessException {
		try {
			return equipStockPermissionDAO.searchCheckShopId(id);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

}

package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.Car;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockIssue;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.StockTransLot;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ProductFilter;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTransLotOwnerType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.StockGeneralFilter;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductOrderVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StockGeneralVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransVO;
import ths.dms.core.entities.vo.WarehouseVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.ShopLockDAO;
import ths.dms.core.dao.StaffDAO;
import ths.dms.core.dao.StaffDAONew;
import ths.dms.core.dao.StockManagerDAO;
import ths.dms.core.dao.StockTotalDAO;
import ths.dms.core.dao.StockTransDAO;
import ths.dms.core.dao.StockTransDetailDAO;
import ths.dms.core.dao.StockTransLotDAO;

public class StockManagerMgrImpl implements StockManagerMgr {

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	StockManagerDAO stockManagerDAO;

	@Autowired
	StockTotalDAO stockTotalDAO;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	StaffDAO staffDAO;

	@Autowired
	ShopLockDAO shopLockDAO;

	@Autowired
	ProductDAO productDAO;

	@Autowired
	StockTransDAO stockTransDAO;

	@Autowired
	StockTransDetailDAO stockTransDetailDAO;

	@Autowired
	StockTransLotDAO stockTransLotDAO;

	@Autowired
	StaffDAONew staffDAONew;
	
	@Autowired
	CommonBusinessHelper commonBusinessHelper;

	/**
	 * Tao moi kho
	 * 
	 * @author hunglm16
	 * @return Warehouse
	 * */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Warehouse createWarehouse(Warehouse warehouse, LogInfoVO logInfo) throws BusinessException {
		try {
			Date sysDate = commonDAO.getSysDate();
			warehouse = stockManagerDAO.createWarehouse(warehouse, sysDate);
			createStockTotalForAllProduct(warehouse, sysDate, logInfo);
			return warehouse;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	private void createStockTotalForAllProduct(Warehouse warehouse, Date sysdate, LogInfoVO logInfo) throws DataAccessException {
		List<Product> allProducts = getAllProducts();
		Shop shop = warehouse.getShop();
		for (Product product : allProducts) {
			StockTotal stockTotal = commonBusinessHelper.createStockTotalEntity(shop, product, warehouse, sysdate, logInfo);
			commonDAO.createEntity(stockTotal);
		}
	}

	private List<Product> getAllProducts() throws DataAccessException {
		ProductFilter filter = new ProductFilter();
		return productDAO.getListProduct(filter);
	}

	/**
	 * Cap nhat thong tin kho
	 * 
	 * @author hunglm16
	 * @return Void
	 * */
	@Override
	public void updateWarehouse(Warehouse warehouse) throws BusinessException {
		try {
			stockManagerDAO.updateWarehouse(warehouse);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Lay thong tin kho voi tham so la Id
	 * 
	 * @author hunglm16
	 * @return Warehouse
	 * */
	@Override
	public Warehouse getWarehouseById(Long id) throws BusinessException {
		try {
			return id == null ? null : stockManagerDAO.getWarehouseById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Lay danh sach kho theo cac dieu kien tim kiem
	 * 
	 * @author hunglm16
	 * @return ObjectVO WarehouseVO
	 * @description get List WarehouseVO By Filter
	 * */
	@Override
	public ObjectVO<WarehouseVO> getListWarehouseVOByFilter(BasicFilter<WarehouseVO> filter) throws BusinessException {
		try {
			List<WarehouseVO> lst = stockManagerDAO.getListWarehouseVOByFilter(filter);
			ObjectVO<WarehouseVO> vo = new ObjectVO<WarehouseVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Kiem tra kho co ton tai hay khong theo nhieu tham so
	 * 
	 * @author hunglm16
	 * @return ObjectVO WarehouseVO
	 * @description Co them cac tham so khac dap ung voi muc dich chuc nang,
	 *              nhung luu y khong truyen du tham so khi goi ham
	 * */
	@Override
	public Boolean checkIfRecordWarehouseExist(BasicFilter<WarehouseVO> filter) throws BusinessException {
		try {
			return stockManagerDAO.checkIfRecordWarehouseExist(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkIfRecordWarehouseInStockTotalExist(BasicFilter<WarehouseVO> filter) throws BusinessException {
		try {
			return stockManagerDAO.checkIfRecordWarehouseInStockTotalExist(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Lay danh sach kho theo Filter
	 * 
	 * @author hunglm16
	 * @since August 25,2014
	 * @return List Warehouse
	 * */
	@Override
	public List<Warehouse> getListWarehouseByFilter(BasicFilter<Warehouse> filter) throws BusinessException {
		try {
			return stockManagerDAO.getListWarehouseByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Sao chep kho
	 * 
	 * @author hunglm16
	 * @return void
	 * @description Ham chinh
	 * */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Integer copyWarehouseByFilter(BasicFilter<ShopVO> filter) throws BusinessException {
		try {
			Date sysdate = commonDAO.getSysDate();
			LogInfoVO logInfo = new LogInfoVO();
			if (filter != null) {
				logInfo.setStaffCode(filter.getUserName());				
			}
			
			//Lay danh sach Kho theo Npp nguon
			BasicFilter<Warehouse> filterW = new BasicFilter<Warehouse>();
			filterW.setStatus(ActiveType.RUNNING.getValue());
			filterW.setLongG(filter.getLongG());
			//			filterW.setTextG(filter.getTextG());
			filterW.setStatus(filter.getStatus());
			// lay du lieu phan quyen duoi DB
			filterW.setStaffRootId(filter.getStaffRootId());
			filterW.setRoleId(filter.getRoleId());
			filterW.setShopRootId(filter.getShopRootId());
			List<Warehouse> lstWareHouse = new ArrayList<Warehouse>();
			lstWareHouse = stockManagerDAO.getListWarehouseByFilter(filterW);
			if (lstWareHouse.isEmpty()) {
				return 0;
			}
			//Lay danh sach NPP theo Danh sach ma don vi
			BasicFilter<Shop> filterG = new BasicFilter<Shop>();
			filterG = new BasicFilter<Shop>();
			filterG.setStatus(ActiveType.RUNNING.getValue());
			filterG.setArrlongG(filter.getArrlongG());
			List<Shop> lstShop = shopDAO.getListShopByFilter(filterG);
			if (lstShop != null && !lstShop.isEmpty()) {
				List<Long> arrId = new ArrayList<Long>();
				for (Shop shop : lstShop) {
					arrId.add(shop.getId());
				}
				lstShop = new ArrayList<Shop>();
				//Lay the tat ca con chau cua don vi de tuong tac
				lstShop = shopDAO.getAllShopChildrenByLstId(arrId);
				List<Warehouse> lstWarehoseEnd = null;
				boolean flagRt = false;
				for (Shop shop : lstShop) {
					if (ShopSpecificType.NPP.equals(shop.getType().getSpecificType())) {
						flagRt = true;
						lstWarehoseEnd = new ArrayList<Warehouse>();
						//Thuc hien kiem tra va sao chep
						filterW = new BasicFilter<Warehouse>();
						filterW.setStatus(ActiveType.RUNNING.getValue());
						filterW.setLongG(shop.getId());
						// lay du lieu phan quyen duoi DB
						filterW.setStaffRootId(filter.getStaffRootId());
						filterW.setRoleId(filter.getRoleId());
						filterW.setShopRootId(filter.getShopRootId());
						lstWarehoseEnd = stockManagerDAO.getListWarehouseByFilter(filterW);
						boolean flag = false;
						Integer maxSeq = 1;
						for (Warehouse whEnd : lstWarehoseEnd) {
							if (whEnd.getSeq() != null && maxSeq <= whEnd.getSeq()) {
								maxSeq = whEnd.getSeq() + 1;
							}
						}
						for (Warehouse whBegin : lstWareHouse) {
							for (Warehouse whEnd : lstWarehoseEnd) {
								if (whBegin.getWarehouseCode().trim().equals(whEnd.getWarehouseCode().trim())) {
									flag = true;
									break;
								}
							}
							if (!flag) {
								//Sao chep kho
								Warehouse whNew = new Warehouse();
								whNew.setWarehouseCode(whBegin.getWarehouseCode());
								whNew.setWarehouseName(whBegin.getWarehouseName());
								whNew.setShop(shop);
								whNew.setSeq(maxSeq);
								whNew.setWarehouseType(whBegin.getWarehouseType());
								whNew.setStatus(whBegin.getStatus());
								whNew.setCreateDate(new Date());
								whNew.setCreateUser(filter.getUserName());
								stockManagerDAO.createWarehouse(whNew);
								createStockTotalForAllProduct(whNew, sysdate, logInfo);
								maxSeq++;
							}
						}
					}
				}
				if (!flagRt) {
					return 2;
				}
			}
			return 1;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	public ObjectVO<StockTotalVO> getListStockWarehouseByProductId(BasicFilter<StockTotalVO> filter) throws BusinessException {
		try {
			List<StockTotalVO> lst = stockManagerDAO.getListStockWarehouseByProductId(filter);
			ObjectVO<StockTotalVO> vo = new ObjectVO<StockTotalVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	public ObjectVO<StockTotalVO> getListStockWarehouseByListProduct(BasicFilter<StockTotalVO> filter) throws BusinessException {
		try {
			List<StockTotalVO> lst = stockManagerDAO.getListStockWarehouseByListProduct(filter);
			ObjectVO<StockTotalVO> vo = new ObjectVO<StockTotalVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo; 
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public ObjectVO<StockTotalVO> getListStockTotalVOBySaler(BasicFilter<StockTotalVO> filter) throws BusinessException {
		try {
			if (filter == null || filter.getShopId() == null || filter.getLongG() == null) {
				throw new IllegalArgumentException("invalid parameters");
			}
			Date lockDate = shopLockDAO.getApplicationDate(filter.getShopId());
			filter.setDateG(lockDate);
			List<StockTotalVO> lst = stockManagerDAO.getListStockTotalVOBySaler(filter);
			ObjectVO<StockTotalVO> vo = new ObjectVO<StockTotalVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public StockTrans createStockTransDP(Staff st, Car car, List<ProductOrderVO> lstProducts, LogInfoVO logInfo) throws BusinessException {
		try {
			if (st == null || car == null || lstProducts == null || logInfo == null) {
				throw new IllegalArgumentException("invalid parameters");
			}
			Date sysdate = commonDAO.getSysDate();
			Staff user = staffDAO.getStaffByCode(logInfo.getStaffCode());

			Shop sh = st.getShop();

			// Luu thong tin stock_trans
			StockTrans stockTrans = new StockTrans();
			String code = stockTransDAO.generateStockTransCodeVansale(sh.getId(), "DP");
			stockTrans.setStockTransCode(code);
			stockTrans.setStaffId(user);
			stockTrans.setShop(sh);
			stockTrans.setCar(car);
			stockTrans.setCreateDate(sysdate);
			stockTrans.setCreateUser(logInfo.getStaffCode());
			stockTrans.setStockTransDate(sysdate); // [ngay chot = ngay hien tai] moi duoc phep xuat, nen lay luon ngay hien tai
			stockTrans.setTransType("DP");
			stockTrans.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
			stockTrans.setApprovedStep(SaleOrderStep.CONFIRMED);

			stockTrans = stockTransDAO.createStockTrans(stockTrans);

			BigDecimal weight = BigDecimal.ZERO;
			BigDecimal amount = BigDecimal.ZERO;

			ProductOrderVO vo = null;
			Product product = null;
			Warehouse warehouse = null;
			StockTransDetail detail = null;
			List<StockTransDetail> lstDetail = new ArrayList<StockTransDetail>();
			StockTransLot lot = null;
			StockTotal stockTotal = null;
			BigDecimal amTmp = BigDecimal.ZERO;
			BigDecimal qtt = null;
			Integer availableQtt = null;
			List<Long> lstPTmp = new ArrayList<Long>();
			int idx = -1;
			for (int i = 0, sz = lstProducts.size(); i < sz; i++) {
				vo = lstProducts.get(i);
				if (vo == null) {
					continue;
				}

				// kiem tra ds sp
				product = productDAO.getProductById(vo.getProductId());
				if (product == null) {
					throw new IllegalArgumentException(ProductOrderVO.ERR_PRODUCT_NULL);
				}
				warehouse = stockManagerDAO.getWarehouseById(vo.getWarehouseId());
				if (warehouse == null) {
					throw new IllegalArgumentException(ProductOrderVO.ERR_WAREHOUSE_NULL);
				}
				if (warehouse.getShop() == null || !sh.getId().equals(warehouse.getShop().getId())) {
					throw new IllegalArgumentException(ProductOrderVO.ERR_WAREHOUSE_NOT_BELONG_SHOP);
				}
				stockTotal = stockTotalDAO.getStockTotalByProductAndOwner(product.getId(), StockObjectType.SHOP, sh.getId(), warehouse.getId());
				if (stockTotal == null) {
					throw new IllegalArgumentException(ProductOrderVO.ERR_WAREHOUSE_NOT_BELONG_SHOP);
				}
				availableQtt = stockTotal.getAvailableQuantity();
				if (availableQtt < vo.getQuantity()) {
					throw new IllegalArgumentException(ProductOrderVO.ERR_NOT_ENOUGH + product.getProductCode());
				}
				availableQtt = availableQtt - vo.getQuantity();

				idx = lstPTmp.indexOf(product.getId());
				// Luu thong tin stock_trans_detail
				if (idx < 0) {
					detail = new StockTransDetail();

					detail.setStockTrans(stockTrans);
					detail.setProduct(product);
					detail.setCreateDate(sysdate);
					detail.setCreateUser(logInfo.getStaffCode());
					detail.setStockTransDate(sysdate); //
					detail.setQuantity(vo.getQuantity());
					detail.setPrice(vo.getPrice());

					detail = stockTransDetailDAO.createStockTransDetail(detail);
					lstDetail.add(detail);
					lstPTmp.add(product.getId());
				} else {
					detail = lstDetail.get(idx);
					detail.setQuantity(detail.getQuantity() + vo.getQuantity());
				}

				// Luu thong tin stock_trans_lot (tach theo kho)
				lot = new StockTransLot();

				lot.setStockTrans(stockTrans);
				lot.setStockTransDetail(detail);
				lot.setProduct(product);
				lot.setCreateDate(sysdate);
				lot.setCreateUser(logInfo.getStaffCode());
				lot.setStockTransDate(sysdate); //
				lot.setQuantity(vo.getQuantity());
				lot.setToOwnerType(StockTransLotOwnerType.STAFF);
				lot.setToOwnerId(st.getId());
				lot.setFromOwnerType(StockTransLotOwnerType.WAREHOUSE);
				lot.setFromOwnerId(warehouse.getId());
				lot.setShop(sh);
				lot = stockTransLotDAO.createStockTransLot(lot);

				qtt = new BigDecimal(vo.getQuantity());
				amTmp = vo.getPrice().multiply(qtt);

				weight = weight.add(product.getGrossWeight().multiply(qtt));
				amount = amount.add(amTmp);

				// Cap nhat ton kho dap ung
				stockTotal.setAvailableQuantity(availableQtt);
				stockTotalDAO.updateStockTotal(stockTotal);
			}

			stockTrans.setTotalWeight(weight);
			stockTrans.setTotalAmount(amount);

			stockTransDAO.updateStockTrans(stockTrans);
			return stockTrans;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<StockTotalVO> getListProductDPByStaff(long shopId, long staffId) throws BusinessException {
		try {
			List<StockTotalVO> lst = stockTransDAO.getListProductDPByStaff(shopId, staffId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author tungmt
	 */
	@Override
	public List<StockTransVO> getListDPByStaff(StockStransFilter filter) throws BusinessException {
		try {
			List<StockTransVO> lst = stockTransDAO.getListDPByStaff(filter);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public StockTrans createStockTransGO(Staff st, List<ProductOrderVO> lstProducts, LogInfoVO logInfo) throws BusinessException {
		try {
			if (st == null || lstProducts == null || logInfo == null) {
				throw new IllegalArgumentException("invalid parameters");
			}
			Date sysdate = commonDAO.getSysDate();
			Staff user = staffDAO.getStaffByCode(logInfo.getStaffCode());

			Shop sh = st.getShop();

			StockTrans stockTrans = new StockTrans();
			String code = stockTransDAO.generateStockTransCodeVansale(sh.getId(), "GO");
			stockTrans.setStockTransCode(code);
			stockTrans.setStaffId(user);
			stockTrans.setShop(sh);
			stockTrans.setCreateDate(sysdate);
			stockTrans.setCreateUser(logInfo.getStaffCode());
			stockTrans.setStockTransDate(sysdate); // [ngay chot = ngay hien tai] moi duoc nhap kho, nen lay luon sysdate
			stockTrans.setTransType("GO");
			stockTrans.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
			stockTrans.setApprovedStep(SaleOrderStep.CONFIRMED);

			stockTrans = stockTransDAO.createStockTrans(stockTrans);

			BigDecimal weight = BigDecimal.ZERO;
			BigDecimal amount = BigDecimal.ZERO;

			ProductOrderVO vo = null;
			Product product = null;
			Warehouse warehouse = null;
			StockTransDetail detail = null;
			List<StockTransDetail> lstDetail = new ArrayList<StockTransDetail>();
			StockTransLot lot = null;
			BigDecimal amTmp = BigDecimal.ZERO;
			BigDecimal qtt = null;
			List<Long> lstPTmp = new ArrayList<Long>();
			int idx = -1;
			for (int i = 0, sz = lstProducts.size(); i < sz; i++) {
				vo = lstProducts.get(i);
				if (vo == null) {
					continue;
				}
				product = productDAO.getProductById(vo.getProductId());
				if (product == null) {
					throw new IllegalArgumentException(ProductOrderVO.ERR_PRODUCT_NULL);
				}
				warehouse = stockManagerDAO.getWarehouseById(vo.getWarehouseId());
				if (warehouse == null) {
					throw new IllegalArgumentException(ProductOrderVO.ERR_WAREHOUSE_NULL);
				}
				if (warehouse.getShop() == null || !sh.getId().equals(warehouse.getShop().getId())) {
					throw new IllegalArgumentException(ProductOrderVO.ERR_WAREHOUSE_NOT_BELONG_SHOP);
				}

				idx = lstPTmp.indexOf(product.getId());
				if (idx < 0) {
					detail = new StockTransDetail();

					detail.setStockTrans(stockTrans);
					detail.setProduct(product);
					detail.setCreateDate(sysdate);
					detail.setCreateUser(logInfo.getStaffCode());
					detail.setStockTransDate(sysdate); //
					detail.setQuantity(vo.getQuantity());
					detail.setPrice(vo.getPrice());

					detail = stockTransDetailDAO.createStockTransDetail(detail);
					lstDetail.add(detail);
					lstPTmp.add(product.getId());
				} else {
					detail = lstDetail.get(idx);
					detail.setQuantity(detail.getQuantity() + vo.getQuantity());
				}

				lot = new StockTransLot();
				lot.setShop(sh);
				lot.setStockTrans(stockTrans);
				lot.setStockTransDetail(detail);
				lot.setProduct(product);
				lot.setCreateDate(sysdate);
				lot.setCreateUser(logInfo.getStaffCode());
				lot.setStockTransDate(sysdate); //
				lot.setQuantity(vo.getQuantity());
				lot.setFromOwnerType(StockTransLotOwnerType.STAFF);
				lot.setFromOwnerId(st.getId());
				lot.setToOwnerType(StockTransLotOwnerType.WAREHOUSE);
				lot.setToOwnerId(warehouse.getId());

				lot = stockTransLotDAO.createStockTransLot(lot);

				qtt = new BigDecimal(vo.getQuantity());
				amTmp = vo.getPrice().multiply(qtt);

				weight = weight.add(product.getGrossWeight().multiply(qtt));
				amount = amount.add(amTmp);
			}

			// kiem tra so luong con lai trong kho nv
			StockTotal stockTotal = null;
			for (int i = 0, sz = lstDetail.size(); i < sz; i++) {
				detail = lstDetail.get(i);
				product = detail.getProduct();
				stockTotal = stockTotalDAO.getStockTotalByProductAndOwner(product.getId(), StockObjectType.STAFF, st.getId(), null);
				if (stockTotal == null) {
					throw new IllegalArgumentException(ProductOrderVO.ERR_WAREHOUSE_NOT_BELONG_SHOP);
				}
				if (!stockTotal.getApprovedQuantity().equals(detail.getQuantity())) {
					throw new IllegalArgumentException(ProductOrderVO.ERR_NOT_ENOUGH + product.getProductCode());
				}
			}

			stockTrans.setTotalWeight(weight);
			stockTrans.setTotalAmount(amount);

			stockTransDAO.updateStockTrans(stockTrans);
			return stockTrans;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public boolean checkExistsGOStockTrans(long staffId, Date pDate) throws BusinessException {
		try {
			return stockTransDAO.checkExistsGOStockTrans(staffId, pDate);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author longnh15
	 */
	@Override
	public boolean checkExistsGOStockTransNotApproves(long staffId, String type) throws BusinessException {
		try {
			return stockTransDAO.checkExistsGOStockTransNotApproves(staffId,type);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	//TODO Danh cho cap nhat phieu xuat kho kiem van chuyen noi bo
	@Override
	public ObjectVO<StockGeneralVO> searchUpdInformationOutputWarehouse(StockGeneralFilter<StockGeneralVO> filter) throws BusinessException {
		try {
			if (filter == null || filter.getShopId() == null) {
				throw new IllegalArgumentException("invalid parameters");
			}
			List<StockGeneralVO> lst = stockManagerDAO.searchUpdInformationOutputWarehouse(filter);
			ObjectVO<StockGeneralVO> vo = new ObjectVO<StockGeneralVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public String generateStockTransCodeByTranType(Long shopId, String transType) throws BusinessException {
		try {
			return stockTransDAO.generateStockTransCodeVansale(shopId, transType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Tim kiem san pham co trong kho ung voi Don vi Xac dinh
	 * 
	 * @author hunglm16
	 * @since October 31,2014
	 * */
	@Override
	public ObjectVO<StockGeneralVO> searchProductInWareHouse(StockGeneralFilter<StockGeneralVO> filter) throws BusinessException {
		try {
			if (filter == null || filter.getShopId() == null) {
				throw new IllegalArgumentException("invalid parameters");
			}
			List<StockGeneralVO> lst = stockManagerDAO.searchProductInWareHouse(filter);
			ObjectVO<StockGeneralVO> vo = new ObjectVO<StockGeneralVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createStockTransferInternalWarehouse(Long wareHouseOutputId, Long wareHouseInputId, Long shopRootId, List<StockTransDetail> lstStockTransDetail, LogInfoVO logInfo) throws BusinessException {
		try {
			//TODO Khoi tao tham so dung chung
			Date sysDate = commonDAO.getSysDate();
			Date lockDate = shopLockDAO.getNextLockedDay(shopRootId);
			if (lockDate == null) {
				lockDate = sysDate;
			}
			Shop shopRoot = shopDAO.getShopById(shopRootId);

			//TODO Them moi StockTrans
			StockTrans stockTransNew = new StockTrans();
			String stockTransCode = stockTransDAO.generateStockTransCodeVansale(shopRootId, "DC");
			stockTransNew.setCreateDate(sysDate);
			stockTransNew.setCreateUser(logInfo.getStaffCode());
			stockTransNew.setStaffId(staffDAONew.getStaffByCode(logInfo.getStaffCode()));
			stockTransNew.setStockTransDate(lockDate);
			stockTransNew.setStockDate(stockTransNew.getStockTransDate());
			stockTransNew.setStockTransCode(stockTransCode);
			stockTransNew.setShop(shopRoot);
			stockTransNew.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
			stockTransNew.setApprovedStep(SaleOrderStep.CONFIRMED);
			stockTransNew.setTransType("DC");
			stockTransNew = stockTransDAO.createStockTrans(stockTransNew);

			//TODO Them moi Stock_Trans_Detail
			BigDecimal weight = BigDecimal.ZERO;
			BigDecimal amount = BigDecimal.ZERO;
			BigDecimal amTmp = BigDecimal.ZERO;
			BigDecimal qtt = null;

			StockTotal stockTotal = null;
			for (StockTransDetail sttDetail : lstStockTransDetail) {
				// lacnv1 - 10.03.2015 - Kiem tra ton kho dap ung cua kho xuat
				stockTotal = stockTotalDAO.getStockTotalByProductAndOwner(sttDetail.getProduct().getId(),
						StockObjectType.SHOP, shopRootId, wareHouseOutputId);
				if (stockTotal == null || stockTotal.getAvailableQuantity() == null
						|| (sttDetail.getQuantity() != null && stockTotal.getAvailableQuantity() < sttDetail.getQuantity())) {
					throw new IllegalArgumentException("STOCK_TOTAL_NOT_ENOUGH_"+sttDetail.getProduct().getProductCode());
				}
				
				sttDetail.setStockTrans(stockTransNew);
				sttDetail.setStockTransDate(lockDate);
				sttDetail.setCreateDate(sysDate);
				sttDetail.setCreateUser(logInfo.getStaffCode());
				sttDetail = stockTransDetailDAO.createStockTransDetail(sttDetail);
				//TODO Them moi Stock_Trans_Lot
				StockTransLot stockTransLot = new StockTransLot();
				stockTransLot.setShop(shopRoot);
				stockTransLot.setStockTrans(stockTransNew);
				stockTransLot.setStockTransDetail(sttDetail);
				stockTransLot.setStockTransDate(lockDate);
				stockTransLot.setFromOwnerId(wareHouseOutputId);
				stockTransLot.setToOwnerId(wareHouseInputId);
				stockTransLot.setQuantity(sttDetail.getQuantity());
				stockTransLot.setProduct(sttDetail.getProduct());
				stockTransLot.setFromOwnerType(StockTransLotOwnerType.WAREHOUSE);
				stockTransLot.setToOwnerType(StockTransLotOwnerType.WAREHOUSE);
				stockTransLot.setCreateDate(sysDate);
				stockTransLot.setCreateUser(logInfo.getStaffCode());
				stockTransLot = stockTransLotDAO.createStockTransLot(stockTransLot);

				qtt = new BigDecimal(sttDetail.getQuantity());
				amTmp = sttDetail.getPrice().multiply(qtt);
				weight = weight.add(sttDetail.getProduct().getGrossWeight().multiply(qtt));
				amount = amount.add(amTmp);
				
				// lacnv1 - 10.03.2015 - giam ton kho dap ung kho xuat
				stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() - sttDetail.getQuantity());
				stockTotal.setUpdateDate(sysDate);
				stockTotal.setUpdateUser(logInfo.getStaffCode());
				stockTotalDAO.updateStockTotal(stockTotal);
			}
			stockTransNew.setTotalWeight(weight);
			stockTransNew.setTotalAmount(amount);
			stockTransDAO.updateStockTrans(stockTransNew);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<Warehouse> searchWarehouseByShop(StockGeneralFilter<Warehouse> filter) throws BusinessException {
		try {
			List<Warehouse> lst = stockManagerDAO.searchWarehouseByShop(filter);
			ObjectVO<Warehouse> vo = new ObjectVO<Warehouse>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createStockIssueByFilter(StockGeneralFilter<StockGeneralVO> filter) throws BusinessException {
		try {
			//TODO Khoi tao du lieu bien tam
			Map<Long, List<Long>> stockIsIdAttrStrMap = new HashMap<Long, List<Long>>();
			Map<Long, String> stockIssueNumberStrMap = new HashMap<Long, String>();
			Map<Long, String> aboutMap = new HashMap<Long, String>();
			Map<Long, String> staffTextMap = new HashMap<Long, String>();
			Map<Long, String> contractNumberMap = new HashMap<Long, String>();
			Map<Long, String> carMap = new HashMap<Long, String>();
			Map<Long, String> warehouseInputNameMap = new HashMap<Long, String>();
			Map<Long, String> warehouseOutPutNameMap = new HashMap<Long, String>();
			//TODO Xu ly lay du lieu tu cac tham so filter
			if (!StringUtility.isNullOrEmpty(filter.getIsIdAttrStr())) {
				String[] arrTmpStr = filter.getIsIdAttrStr().split("#,");
				for (String value : arrTmpStr) {
					String[] arrValue = value.split("#>");
					if (arrValue != null && arrValue.length > 1 && !StringUtility.isNullOrEmpty(arrValue[0]) && !StringUtility.isNullOrEmpty(arrValue[1])) {
						String[] arrValueIdDetail = arrValue[1].split(",");
						List<Long> lstItemTmp = new ArrayList<Long>();
						for (String itemTmp : arrValueIdDetail) {
							lstItemTmp.add(Long.valueOf(itemTmp));
						}
						stockIsIdAttrStrMap.put(Long.valueOf(arrValue[0]), lstItemTmp);
					}
				}
			}
			if (!StringUtility.isNullOrEmpty(filter.getStockIssueNumberStr())) {
				String[] arrTmpStr = filter.getStockIssueNumberStr().split("#,");
				for (String value : arrTmpStr) {
					String[] arrValue = value.split("#>");
					if (arrValue != null && arrValue.length > 1 && !StringUtility.isNullOrEmpty(arrValue[0]) && !StringUtility.isNullOrEmpty(arrValue[1])) {
						stockIssueNumberStrMap.put(Long.valueOf(arrValue[0]), arrValue[1].trim());
					}
				}
			}
			if (!StringUtility.isNullOrEmpty(filter.getAbout())) {
				String[] arrTmpStr = filter.getAbout().split("#,");
				for (String value : arrTmpStr) {
					String[] arrValue = value.split("#>");
					if (arrValue != null && arrValue.length > 1 && !StringUtility.isNullOrEmpty(arrValue[0]) && !StringUtility.isNullOrEmpty(arrValue[1])) {
						aboutMap.put(Long.valueOf(arrValue[0]), arrValue[1].trim());
					}
				}
			}
			if (!StringUtility.isNullOrEmpty(filter.getStaffText())) {
				String[] arrTmpStr = filter.getStaffText().split("#,");
				for (String value : arrTmpStr) {
					String[] arrValue = value.split("#>");
					if (arrValue != null && arrValue.length > 1 && !StringUtility.isNullOrEmpty(arrValue[0]) && !StringUtility.isNullOrEmpty(arrValue[1])) {
						aboutMap.put(Long.valueOf(arrValue[0]), arrValue[1].trim());
					}
				}
			}
			if (!StringUtility.isNullOrEmpty(filter.getContractNumber())) {
				String[] arrTmpStr = filter.getContractNumber().split("#,");
				for (String value : arrTmpStr) {
					String[] arrValue = value.split("#>");
					if (arrValue != null && arrValue.length > 1 && !StringUtility.isNullOrEmpty(arrValue[0]) && !StringUtility.isNullOrEmpty(arrValue[1])) {
						contractNumberMap.put(Long.valueOf(arrValue[0]), arrValue[1].trim());
					}
				}
			}
			if (!StringUtility.isNullOrEmpty(filter.getCar())) {
				String[] arrTmpStr = filter.getCar().split("#,");
				for (String value : arrTmpStr) {
					String[] arrValue = value.split("#>");
					if (arrValue != null && arrValue.length > 1 && !StringUtility.isNullOrEmpty(arrValue[0]) && !StringUtility.isNullOrEmpty(arrValue[1])) {
						carMap.put(Long.valueOf(arrValue[0]), arrValue[1].trim());
					}
				}
			}
			if (!StringUtility.isNullOrEmpty(filter.getWarehouseInputName())) {
				String[] arrTmpStr = filter.getWarehouseInputName().split("#,");
				for (String value : arrTmpStr) {
					String[] arrValue = value.split("#>");
					if (arrValue != null && arrValue.length > 1 && !StringUtility.isNullOrEmpty(arrValue[0]) && !StringUtility.isNullOrEmpty(arrValue[1])) {
						warehouseInputNameMap.put(Long.valueOf(arrValue[0]), arrValue[1].trim());
					}
				}
			}
			if (!StringUtility.isNullOrEmpty(filter.getWarehouseOutPutName())) {
				String[] arrTmpStr = filter.getWarehouseOutPutName().split("#,");
				for (String value : arrTmpStr) {
					String[] arrValue = value.split("#>");
					if (arrValue != null && arrValue.length > 1 && !StringUtility.isNullOrEmpty(arrValue[0]) && !StringUtility.isNullOrEmpty(arrValue[1])) {
						warehouseOutPutNameMap.put(Long.valueOf(arrValue[0]), arrValue[1].trim());
					}
				}
			}
			//TODO Xu lay lay du lieu tho
			for (Entry<Long, String> entry : stockIssueNumberStrMap.entrySet()) {
				Long keyG = entry.getKey();
				//Them moi dong du lieu vao Stock Issue
				StockIssue stockIssue = new StockIssue();
				stockIssue.setStockIssueNumber(entry.getValue());
				stockIssue.setAbout(aboutMap.get(keyG));
				stockIssue.setContractNumber(contractNumberMap.get(keyG));
				stockIssue.setCar(carMap.get(keyG));
				stockIssue.setFromOwnerStock(warehouseInputNameMap.get(keyG));
				stockIssue.setToOwnerStock(warehouseOutPutNameMap.get(keyG));
				stockIssue = stockTransDAO.createStockIssue(stockIssue);
				//Them moi dong du lieu vao Stock Issue Detail
				if (!StringUtility.isNullOrEmpty(filter.getTransType()) && (filter.DP.equals(filter.getTransType()) || filter.DC.equals(filter.getTransType()))) {
					//Lay du lieu tu bang stock_trans_detail
				} else if (!StringUtility.isNullOrEmpty(filter.getTransType()) && filter.POVNM.equals(filter.getTransType())) {
					//Lay du lieu tu bang po_vnm_detail
				} else {
					throw new BusinessException("TransType Is Undefined by "+entry.getValue());
				}
			}

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<StockTotalVO> getListStockTransDetail(BasicFilter<StockTotalVO> filter) throws BusinessException {
		try {
			List<StockTotalVO> lst = stockManagerDAO.getListStockTransDetail(filter);
			ObjectVO<StockTotalVO> vo = new ObjectVO<StockTotalVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<StockTotalVO> getListStockTransDetailForDC(BasicFilter<StockTotalVO> filter) throws BusinessException {
		try {
			List<StockTotalVO> lst = stockManagerDAO.getListStockTransDetailForDC(filter);
			ObjectVO<StockTotalVO> vo = new ObjectVO<StockTotalVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}


	/**
	 * @author lacnv1
	 */
	@Override
	public Warehouse getMostPriorityWareHouse(long shopId, Long productId) throws BusinessException {
		try {
			Warehouse wh = stockTotalDAO.getMostPriorityWareHouse(shopId, productId);
			return wh;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

}

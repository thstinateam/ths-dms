package ths.dms.core.business;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ShopLock;
import ths.dms.core.entities.ShopLockReleasedLog;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.SpParam;
import ths.dms.core.entities.filter.StaffFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ShopLockLogVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.function.common.PkgFuncGetCloseDayErrorMsg;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.SaleOrderDAO;
import ths.dms.core.dao.ShopLockDAO;
import ths.dms.core.dao.StockTransDAO;
import ths.dms.core.dao.repo.IRepository;

public class ShopLockMgrImpl implements ShopLockMgr {
	
	@Autowired
	private IRepository repo;
	
	@Autowired
	ShopLockDAO shopLockDAO;
	
	@Autowired
	StockTransDAO stockTransDAO;
	
	@Autowired
	SaleOrderDAO saleOrderDAO;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public ShopLock createShopLock(ShopLock shopLock) throws BusinessException {
		try {
			return shopLockDAO.createShopLock(shopLock);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public boolean isShopLocked(Long shopId) throws BusinessException {
		try {
			return shopLockDAO.isShopLocked(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public boolean isShopLockedByCode(String shopCode) throws BusinessException {
		try {
			return shopLockDAO.isShopLockedByCode(shopCode);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * @author hoanv25
	 * Lay danh sach quan ly kho
	 */
	@Override
	public ObjectVO<StaffVO> getListStockVOFilter(StaffFilter filter) throws BusinessException {
		try {
			List<StaffVO> lst = shopLockDAO.getListStockVOFilter(filter);
			ObjectVO<StaffVO> objVO = new ObjectVO<StaffVO>();
			objVO.setkPaging(filter.getkPagingStockVO());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public Date getLockedDay(Long shopId) throws BusinessException {
		try {
			return shopLockDAO.getLockedDay(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public Date getApplicationDate(Long shopId) throws BusinessException {
		try {
			return shopLockDAO.getApplicationDate(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Date getNextLockedDay(Long shopId) throws BusinessException {
		try {
			return shopLockDAO.getNextLockedDay(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void runProcess(Date lockDate, Long shopId) throws BusinessException {
		try{
			List<SpParam> inParams = new ArrayList<SpParam>();
	        List<SpParam> outParams = new ArrayList<SpParam>();
	        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String ngayChot = df.format(lockDate);
	        
	        inParams.add(new SpParam(1, java.sql.Types.VARCHAR, ngayChot));
	        inParams.add(new SpParam(2, java.sql.Types.NUMERIC, shopId));
	        repo.executeSP("PKG_SHOP_REPORT_PROCESS.STOCK_DATE_REPORT_WITH_SHOP", inParams, outParams);
	        
	        inParams = new ArrayList<SpParam>();
	        outParams = new ArrayList<SpParam>();
	        
			inParams.add(new SpParam(1, java.sql.Types.NUMERIC, shopId));
	        inParams.add(new SpParam(2, java.sql.Types.DATE, lockDate));
	        inParams.add(new SpParam(3, java.sql.Types.NUMERIC, null));
	        repo.executeSP("P_RPT_CTTL", inParams, outParams);
		}catch (DataAccessException e) {
			throw new BusinessException(e);
		}catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateShopLock(ShopLock shopLock, Long shopId) throws BusinessException {
		try {
			shopLockDAO.updateShopLock(shopLock, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public int checkStockTrans(Long shopId, Date lockDate)
			throws BusinessException {
		try {
			return stockTransDAO.checkStockTrans(shopId, lockDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public int checkStockTransVansale(Long shopId) throws BusinessException {
		try {
			return stockTransDAO.checkStockTransVansale(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public boolean isStockTransLocked(Long shopId, Date lockDate) throws BusinessException {
		try {
			return shopLockDAO.isStockTransLocked(shopId, lockDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public String getStockTransNotYetReturn(Long shopId)
			throws BusinessException {
		try {
			return stockTransDAO.getStockTransNotYetReturn(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ShopLock getShopLockVanSale(Long shopId, Date lockDate)
			throws BusinessException {
		try {
			return shopLockDAO.getShopLockVanSale(shopId, lockDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateShopLockVansale(ShopLock shopLock)
			throws BusinessException {
		try {
			shopLockDAO.updateShopLockVansale(shopLock);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public String getListStockTransStaffId(long shopId, Date date) throws BusinessException {
		try {
			return stockTransDAO.getListStockTransStaffId(shopId, date);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public String getListStaffLockStockYet(String lstStaffId, Date date) throws BusinessException {
		try {
			return stockTransDAO.getListStaffLockStockYet(lstStaffId, date);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public String getListStaffStockReturnYet(String lstStaffId) throws BusinessException {
		try {
			return stockTransDAO.getListStaffStockReturnYet(lstStaffId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public Integer countSaleOrderApprovedYet(long shopId, Date date, int orderTypeGroup) throws BusinessException {
		try {
			return saleOrderDAO.countSaleOrderApprovedYet(shopId, date, orderTypeGroup);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author phuongvm
	 */
	@Override
	public List<ShopLock> getListShopLock(Long shopId, Date fromDate)
			throws BusinessException {
		try {
			return shopLockDAO.getListShopLock(shopId, fromDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * @author phuongvm
	 */
	@Override
	public void deleteListShopLock(List<ShopLock> lst)
			throws BusinessException {
		try {
			shopLockDAO.deleteShopLock(lst);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public String getListStaffStockReturnYet2(long shopId) throws BusinessException {
		try {
			return stockTransDAO.getListStaffStockReturnYet(shopId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	/**
	 * @author phuongvm
	 */
	@Override
	public Date getMinLockDate(Long shopId) throws BusinessException {
		try {
			return shopLockDAO.getMinLockDate(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ShopVO> getListChildShopForCloseDay(ShopFilter filter) throws BusinessException {
		try {
			List<ShopVO> lst = shopLockDAO.getListChildShopForCloseDay(filter);
			ObjectVO<ShopVO> vo = new ObjectVO<ShopVO>();
			vo.setkPaging(filter.getkPagingVO());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * @author hunglm16
	 */
	@Override
	public List<ShopLockLogVO> getShopLockLogVOByShopAndLockDate(Long shopId, Date lockDate) throws BusinessException {
		try {
			return shopLockDAO.getShopLockLogVOByShopAndLockDate(shopId, lockDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<BasicVO> countAndSumSaleOrderApprovedYet(long shopId, Date date, int orderTypeGroup) throws BusinessException {
		try {
			return saleOrderDAO.countAndSumSaleOrderApprovedYet(shopId, date, orderTypeGroup);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<PkgFuncGetCloseDayErrorMsg> getErrorForCloseDay (Long shopId) throws BusinessException {
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			int setPR = 2;
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			//Xu ly lay header bao cao
			List<PkgFuncGetCloseDayErrorMsg> lstData = new ArrayList<PkgFuncGetCloseDayErrorMsg>();
			sql.append(" call pkg_for_function_g.GET_ERROR_FOR_CLOSE_DAY(?, :shopId) ");
			params.add(setPR++);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			lstData = repo.getListByQueryDynamicFromPackage(PkgFuncGetCloseDayErrorMsg.class, sql.toString(), params, null);
			if (lstData != null && !lstData.isEmpty()) {
				return lstData;
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		return new ArrayList<PkgFuncGetCloseDayErrorMsg>();
	}
	
	@Override
	public void updatePassedByClosingDay(long shopId, Date workingDay, String username) throws DataAccessException {
		List<SpParam> inParams = new ArrayList<SpParam>();
		List<SpParam> outParams = new ArrayList<SpParam>();

		int idx = 1;
		inParams.add(new SpParam(idx++, java.sql.Types.NUMERIC, shopId));
		inParams.add(new SpParam(idx++, java.sql.Types.DATE, workingDay.getTime()));
		inParams.add(new SpParam(idx++, java.sql.Types.VARCHAR, username));
		repo.executeSP("p_update_passed_closing_day", inParams, outParams);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopLockMgr#getShopLockShopAndLockDate(java.lang.Long, java.util.Date)
	 */
	@Override
	public ShopLock getShopLockShopAndLockDate(Long shopId, Date lockDate) throws BusinessException {
		try {
			return shopLockDAO.getShopLockShopAndLockDate(shopId, lockDate);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopLockMgr#deleteShopLock(ths.dms.core.entities.ShopLock)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteShopLockAndOpen(ShopLock shopLock, String userName) throws BusinessException {
		try {
			if (shopLock != null) {
				ShopLockReleasedLog shopLockReleasedLog = new ShopLockReleasedLog();
				shopLockReleasedLog.setShopLogId(shopLock.getId());
				if (shopLock.getShop() != null && shopLock.getShop().getId() != null) {
					shopLockReleasedLog.setShopId(shopLock.getShop().getId());
				}
				shopLockReleasedLog.setLockDate(shopLock.getLockDate());
				shopLockReleasedLog.setReleasedDate(commonDAO.getSysDate());
				shopLockReleasedLog.setReleasedUser(userName);
				commonDAO.createEntity(shopLockReleasedLog);
				commonDAO.deleteEntity(shopLock);
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopLockMgr#getShopLockReleaseLogShopAndReleaseDate(java.lang.Long, java.util.Date)
	 */
	@Override
	public ShopLockReleasedLog getShopLockReleaseLogShopAndReleaseDate(Long shopId, Date releaseDate) throws BusinessException {
		try {
			return shopLockDAO.getShopLockReleaseLogShopAndReleaseDate(shopId, releaseDate);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
}

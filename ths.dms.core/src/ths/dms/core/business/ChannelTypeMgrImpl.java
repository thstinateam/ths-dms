package ths.dms.core.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ChannelTypeVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.ChannelTypeDAO;
import ths.dms.core.dao.CustomerDAO;

public class ChannelTypeMgrImpl implements ChannelTypeMgr {

	@Autowired
	ChannelTypeDAO channelTypeDAO;
	
	@Autowired
	CustomerDAO customerDAO;
	
	@Override
	public ChannelType createChannelType(ChannelType channelType, LogInfoVO logInfo) throws BusinessException {
		try {
			return channelTypeDAO.createChannelType(channelType, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateChannelType(ChannelType channelType, LogInfoVO logInfo) throws BusinessException {
		try {
			channelTypeDAO.updateChannelType(channelType, logInfo);
			channelTypeDAO.updateDescendantChannelType(channelType.getId(), channelType.getObjectType());
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteChannelType(ChannelType channelType, LogInfoVO logInfo) throws BusinessException{
		try {
			channelTypeDAO.deleteChannelType(channelType, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ChannelType getChannelTypeById(Long id) throws BusinessException{
		try {
			return id==null?null:channelTypeDAO.getChannelTypeById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ChannelType getChannelTypeByCode(String code, ChannelTypeType type) throws BusinessException {
		try {
			return channelTypeDAO.getChannelTypeByCode(code, type);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ChannelType> getListChannelType(ChannelTypeFilter filter, KPaging<ChannelType> kPaging) throws BusinessException {
		try {
			List<ChannelType> lst = channelTypeDAO.getListChannelType(filter, kPaging);
			ObjectVO<ChannelType> vo = new ObjectVO<ChannelType>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ChannelType> getListChannelTypeOrderById(KPaging<ChannelType> kPaging,
			String channelTypeCode, String channelTypeName, Long parentId,
			ChannelTypeType type, ActiveType status, Integer isEdit, ActiveType statusSku,
			ActiveType statusAmount, Integer objectType) throws BusinessException {
		try {
			List<ChannelType> lst = channelTypeDAO.getListChannelTypeOrderById(kPaging, channelTypeCode, 
					channelTypeName, parentId, type, status, isEdit, objectType);
			ObjectVO<ChannelType> vo = new ObjectVO<ChannelType>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean isUsingByOthers(long channelTypeId, ChannelTypeType type) throws BusinessException {
		try {
			return channelTypeDAO.isUsingByOthers(channelTypeId, type);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean isUsingByOtherRunningItem(long channelTypeId, ChannelTypeType type) throws BusinessException {
		try {
			return channelTypeDAO.isUsingByOtherRunningItem(channelTypeId, type);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<ChannelType> getListSubChannelType(Long parentId, ChannelTypeType type, Boolean isOrderByName) throws BusinessException {
		try {
			return channelTypeDAO.getListSubChannelType(parentId, type, isOrderByName);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkChannelTypeHasAnyChild(Long parentId) throws BusinessException {
		try {
			return channelTypeDAO.checkChannelTypeHasAnyChild(parentId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public TreeVO<ChannelType> getChannelTypeTreeVO(ChannelType channelType, ChannelTypeType type, Boolean isOrderByName) throws BusinessException {
		TreeVO<ChannelType> vo = new TreeVO<ChannelType>();
		vo.setObject(channelType);
		
		List<ChannelType> lstChild = this.getListSubChannelType(channelType==null?null:channelType.getId(), type, isOrderByName);
		/*--Chech ATTT XSS hunglm16: Start */
		for(int i=0; i<lstChild.size(); i++){
			String channelTypeCode = lstChild.get(i).getChannelTypeCode();
			String channelTypeName = lstChild.get(i).getChannelTypeName();
			/*lstChild.get(i).setChannelTypeCode(StringEscapeUtils.escapeHtml3(channelTypeCode));
			lstChild.get(i).setChannelTypeName(StringEscapeUtils.escapeHtml3(channelTypeName));*/
			lstChild.get(i).setChannelTypeCode(channelTypeCode); // bo ra vi hien thi loi tieng Viet
			lstChild.get(i).setChannelTypeName(channelTypeName);
		}
		/*--End--*/
		
		if (lstChild != null)
			for (ChannelType cType: lstChild) {
				TreeVO<ChannelType> childVO = getChannelTypeTreeVO(cType, type, isOrderByName);
				List<TreeVO<ChannelType>> lst = vo.getListChildren();
				lst.add(childVO);
				vo.setListChildren(lst);
			}
		
		return vo;
	}
	
	@Override
	public TreeVO<ChannelType> getChannelTypeTreeVOEx(ChannelType channelType, ChannelTypeType type, Long exceptionalChannelType) throws BusinessException {
		TreeVO<ChannelType> vo = new TreeVO<ChannelType>();
		vo.setObject(channelType);
		
		List<ChannelType> lstChild = this.getListSubChannelType(channelType==null?null:channelType.getId(), type, false);
		if (lstChild != null)
			for (ChannelType cType: lstChild) {
				if (cType.getId().equals(exceptionalChannelType))
					continue;
				TreeVO<ChannelType> childVO = getChannelTypeTreeVOEx(cType, type, exceptionalChannelType);
				List<TreeVO<ChannelType>> lst = vo.getListChildren();
				lst.add(childVO);
				vo.setListChildren(lst);
			}
		
		return vo;
	}
	
	@Override
	public ObjectVO<ChannelType> getListDescendantChannelType(KPaging<ChannelType> kPaging, Long parentId, ChannelTypeType type)
			throws BusinessException {
		try {
			List<ChannelType> lst = channelTypeDAO.getListDescendantChannelType(kPaging, parentId, type);
			ObjectVO<ChannelType> vo = new ObjectVO<ChannelType>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkAncestor(Long parentId, Long channelTypeId) throws BusinessException {
		try {
			return channelTypeDAO.checkAncestor(parentId, channelTypeId);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<ChannelType> getListDescendantChannelTypeOrderById(KPaging<ChannelType> kPaging, Long parentId, ChannelTypeType type)
			throws BusinessException {
		try {
			List<ChannelType> lst = channelTypeDAO.getListDescendantChannelTypeOrderById(kPaging, parentId, type);
			ObjectVO<ChannelType> vo = new ObjectVO<ChannelType>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean isAllChildStoped(long channelTypeId, ChannelTypeType type)
		throws BusinessException {
		try {
			return channelTypeDAO.isAllChildStoped(channelTypeId, type);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<ChannelType> getListCustomerTypeNotInPromotionProgram(KPaging<ChannelType> kPaging, Long promotionShopMapId)
			throws BusinessException {
		try {
			List<ChannelType> lst = channelTypeDAO.getListCustomerTypeNotInPromotionProgram(kPaging, promotionShopMapId);
			ObjectVO<ChannelType> vo = new ObjectVO<ChannelType>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkExsitedChildChannelType(String channelCode, ChannelTypeType type)
		throws BusinessException {
		try {
			return channelTypeDAO.checkExsitedChildChannelType(channelCode, type);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateCustomerType(ChannelType type, LogInfoVO logInfo) throws BusinessException {
		if (type == null) {
			throw new IllegalArgumentException("invalid type");
		}
		try {
			if (ActiveType.RUNNING.equals(type.getStatus())) {
				customerDAO.removeTypeOfCustomer(type.getId());
			}
			channelTypeDAO.updateChannelType(type, logInfo);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<ChannelType> getListSubChannelTypeByType(Integer type)
			throws BusinessException {
		if (type == null) {
			throw new IllegalArgumentException("invalid type");
		}
		try {
			List<ChannelType> lst = new ArrayList<ChannelType>();
			lst = channelTypeDAO.getListChannelTypeByType(type);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public List<ChannelTypeVO> getListChannelTypeVO() {
		try {
			return channelTypeDAO.getListChannelTypeVO();
		} catch (DataAccessException e) {
		}
		return null;
	}

	@Override
	public ChannelType getChannelTypeByName(String name, ChannelTypeType type)
			throws BusinessException {
		try {
			return channelTypeDAO.getChannelTypeByName(name, type);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
}
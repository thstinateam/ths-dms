/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import ths.dms.core.entities.Po;
import ths.dms.core.entities.PoDetail;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.PoVnmDetail;
import ths.dms.core.entities.PriceSaleIn;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActionType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApprovalStepPo;
import ths.dms.core.entities.enumtype.PoManualType;
import ths.dms.core.entities.enumtype.PoObjectType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.PoManualFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PoManualProductVO;
import ths.dms.core.entities.vo.PoManualVO;
import ths.dms.core.entities.vo.PoManualVODetail;
import ths.dms.core.entities.vo.PoManualVOError;
import ths.dms.core.entities.vo.PoManualVOSave;
import ths.dms.core.entities.vo.PoVnmManualVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.Constant;
import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.LogUtility;
import ths.dms.core.common.utils.MathUtil;
import ths.dms.core.common.utils.MessageUtil;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.PoManualDAO;
import ths.dms.core.dao.PoVnmDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.StockTotalDAO;
import ths.dms.core.dao.repo.IRepository;


/**
 * Mo ta class PoManualMgrImpl.java
 * @author vuongmq
 * @since Dec 7, 2015
 */
public class PoManualMgrImpl implements PoManualMgr {
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	PoManualDAO poManualDAO;

	@Autowired
	PoVnmDAO poVnmDAO;

	@Autowired
	ShopDAO shopDAO;
	
	@Autowired
	StockTotalDAO stockTotalDAO;
	
	@Autowired
	PlatformTransactionManager transactionManager;
	
	@Autowired
	private IRepository repo;

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#createPo(ths.dms.core.entities.Po)
	 */
	@Override
	public Po createPo(Po po) throws BusinessException {
		try {
			return poManualDAO.createPo(po);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#createListPo(java.util.List)
	 */
	@Override
	public void createListPo(List<Po> listPo) throws BusinessException {
		try {
			poManualDAO.createListPo(listPo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#deletePo(ths.dms.core.entities.Po)
	 */
	@Override
	public void deletePo(Po po) throws BusinessException {
		try {
			poManualDAO.deletePo(po);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#getPoById(long)
	 */
	@Override
	public Po getPoById(long id) throws BusinessException {
		try {
			return poManualDAO.getPoById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#updatePo(ths.dms.core.entities.Po)
	 */
	@Override
	public void updatePo(Po po) throws BusinessException {
		try {
			poManualDAO.updatePo(po);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#updateListPo(java.util.List)
	 */
	@Override
	public void updateListPo(List<Po> listPo) throws BusinessException {
		try {
			poManualDAO.updateListPo(listPo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#getPoByPoNumber(java.lang.String)
	 */
	@Override
	public Po getPoByPoNumber(String poNumber) throws BusinessException {
		try {
			return poManualDAO.getPoByPoNumber(poNumber);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#getListPoDetailByPoId(java.lang.Long)
	 */
	@Override
	public List<PoDetail> getListPoDetailByPoId(Long poId) throws BusinessException {
		try {
			return poManualDAO.getListPoDetailByPoId(poId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#getPoDetailByPoDetailIdAndProductId(java.lang.Long)
	 */
	@Override
	public PoDetail getPoDetailByPoDetailIdAndProductId(Long poDetailId) throws BusinessException {
		try {
			return poManualDAO.getPoDetailByPoDetailIdAndProductId(poDetailId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#getListOrderProductVO(ths.dms.core.entities.filter.PoManualFilter)
	 */
	@Override
	public List<OrderProductVO> getListOrderProductVO(PoManualFilter<OrderProductVO> filter) throws BusinessException {
		try {
			return poManualDAO.getListOrderProductVO(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#saveOrderManual(ths.dms.core.entities.vo.PoManualVOSave, ths.dms.core.entities.vo.LogInfoVO)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public String saveOrderManual(PoManualVOSave poManualVOSave, LogInfoVO logInfoVO) throws BusinessException {
		String poNumber = null;
		try {
			if (poManualVOSave != null && poManualVOSave.getLstProduct() != null && poManualVOSave.getLstProduct().size() > 0) {
				Shop shop = shopDAO.getShopByCode(poManualVOSave.getShopCode());
				if (shop == null || shop.getId() == null) {
					throw new IllegalArgumentException("shop is null or shop.getId() is null");
				}
				Date sys = commonDAO.getSysDate();
				Po po = null;
				// create PO
				Date requestDate = null;
				if (!StringUtility.isNullOrEmpty(poManualVOSave.getRequestDate())) {
					requestDate = DateUtility.parse(poManualVOSave.getRequestDate(), DateUtility.DATE_FORMAT_STR);
				}
				if (ActionType.INSERT.equals(poManualVOSave.getAction())) {
					Po poNew = new Po();
					poNew.setPoType(PoManualType.parseValue(poManualVOSave.getType()));
					if (PoManualType.PO_RETURN.equals(poNew.getPoType())) {
						PoVnmFilter filter = new PoVnmFilter();
						filter.setShopId(shop.getId());
						filter.setOrderNumber(poManualVOSave.getAsn()); // asn
						filter.setPoType(PoType.PO_CONFIRM);
						filter.setPoStatus(PoVNMStatus.IMPORTED);
						PoVnm poVnm = poVnmDAO.getPoDVKHBySaleOrderNumber(filter);
						poNew.setPoVnm(poVnm);
						poNew.setRefPoNumber(poManualVOSave.getAsn());
						if (poVnm != null) {
							poNew.setWarehouse(poVnm.getWarehouse());
						}
					} else {
						Warehouse warehouse = commonDAO.getEntityById(Warehouse.class, poManualVOSave.getWarehouseId());
						poNew.setWarehouse(warehouse);
					}
					poNew.setPoDate(sys);
					poNew.setShop(shop);
					poNew.setNote(poManualVOSave.getNote());
					poNew.setRequestDate(requestDate);
					poNew.setApprovedStep(ApprovalStepPo.NEW);
					poNew.setStatus(ActiveType.RUNNING);
					poNew.setCreateDate(sys);
					poNew.setCreateUser(logInfoVO.getStaffCode());
					po = poManualDAO.createPo(poNew);
				} else {
					po = poManualDAO.getPoById(poManualVOSave.getPoId());
					if (po != null) {
						po.setUpdateDate(sys);
						po.setUpdateUser(logInfoVO.getStaffCode());
						po.setNote(poManualVOSave.getNote());
						po.setRequestDate(requestDate);
						poManualDAO.updatePo(po);
					}
				}
				Integer totalQuantity = 0;
				BigDecimal totalAmount = BigDecimal.ZERO;
				if (ActionType.INSERT.equals(poManualVOSave.getAction())) {
					if (PoManualType.PO_IMPORT.equals(po.getPoType())) {
						for (int i = 0, sz = poManualVOSave.getLstProduct().size(); i < sz; i++) {
							PoManualProductVO poManualProductVO = poManualVOSave.getLstProduct().get(i);
							if (poManualProductVO != null) {
								PoManualFilter<OrderProductVO> filter = new PoManualFilter<>();
								filter.setShopId(shop.getId());
								filter.setProductId(poManualProductVO.getProductId());
								filter.setWarehouseId(poManualVOSave.getWarehouseId());
								List<OrderProductVO> lstProduct = poManualDAO.getListOrderProductVO(filter);
								if (lstProduct != null && lstProduct.size() > 0) {
									OrderProductVO productVO = lstProduct.get(0);
									PoDetail poDetail = this.setPoDetailManual(po, poManualProductVO, productVO, sys, logInfoVO, null);
									poDetail = commonDAO.createEntity(poDetail);
									totalQuantity = totalQuantity + poDetail.getQuantity();
									totalAmount = totalAmount.add(poDetail.getAmount());
								}
							}
						}
					} else {
						// create po_return detail
						for (int i = 0, sz = poManualVOSave.getLstProduct().size(); i < sz; i++) {
							PoManualProductVO poManualProductVO = poManualVOSave.getLstProduct().get(i);
							if (poManualProductVO != null) {
								PoVnmFilter filter = new PoVnmFilter();
								filter.setShopId(shop.getId());
								if (po.getPoVnm() != null && po.getWarehouse() != null) {
									filter.setPoVnmId(po.getPoVnm().getId());
									filter.setWarehouseId(po.getWarehouse().getId());
								}
								filter.setProductId(poManualProductVO.getProductId());
								List<OrderProductVO> listPoVNMDetail = poManualDAO.getListPoVNMDetailByFilter(filter);
								if (listPoVNMDetail != null && listPoVNMDetail.size() > 0) {
									OrderProductVO orderProductVO = listPoVNMDetail.get(0);
									if (poManualProductVO.getQuantity() != null) {
										if (orderProductVO.getConvfact() != null) {
											//int soluongLe = poManualProductVO.getQuantity().intValue() * orderProductVO.getConvfact().intValue();
											int soluongLe = poManualProductVO.getQuantity().intValue();
											if (orderProductVO.getAvailableQuantity() != null) {
												if (soluongLe > orderProductVO.getAvailableQuantity().intValue()) {
													logInfoVO.setIdObject((po.getPoVnm() != null && po.getPoVnm().getId() != null) ? po.getPoVnm().getId().toString() : po.getRefPoNumber());
													logInfoVO.setActionType(LogInfoVO.ACTION_INSERT);
													logInfoVO.setFunctionCode(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_AVAILABLE_ERR);
													LogUtility.logInfoWs(logInfoVO);
													throw new BusinessException(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_AVAILABLE_ERR + poManualProductVO.getProductCode());
												}
											} else {
												logInfoVO.setIdObject((po.getPoVnm() != null && po.getPoVnm().getId() != null) ? po.getPoVnm().getId().toString() : po.getRefPoNumber());
												logInfoVO.setActionType(LogInfoVO.ACTION_INSERT);
												logInfoVO.setFunctionCode(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_AVAILABLE_ERR);
												LogUtility.logInfoWs(logInfoVO);
												throw new BusinessException(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_AVAILABLE_ERR + poManualProductVO.getProductCode());
											}
											if (orderProductVO.getOldQuantity() == null || soluongLe > orderProductVO.getOldQuantity().intValue()) {
												// oldQuantity: so luong le quantity
												logInfoVO.setIdObject((po.getPoVnm() != null && po.getPoVnm().getId() != null) ? po.getPoVnm().getId().toString() : po.getRefPoNumber());
												logInfoVO.setActionType(LogInfoVO.ACTION_INSERT);
												logInfoVO.setFunctionCode(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_OLD_ERR);
												LogUtility.logInfoWs(logInfoVO);
												throw new BusinessException(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_OLD_ERR + poManualProductVO.getProductCode());
											}
										} else {
											logInfoVO.setIdObject((po.getPoVnm() != null && po.getPoVnm().getId() != null) ? po.getPoVnm().getId().toString() : po.getRefPoNumber());
											logInfoVO.setActionType(LogInfoVO.ACTION_INSERT);
											logInfoVO.setFunctionCode(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_CONVFACT_ERR);
											LogUtility.logInfoWs(logInfoVO);
											throw new BusinessException(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_CONVFACT_ERR + poManualProductVO.getProductCode());
										}
									}
									// them moi po_detail (don tra)
									PoDetail poDetail = this.setPoDetailManual(po, poManualProductVO, orderProductVO, sys, logInfoVO, null);
									poDetail = commonDAO.createEntity(poDetail);
									if (poDetail.getQuantity() != null) {
										totalQuantity = totalQuantity + poDetail.getQuantity();
									}
									if (poDetail.getAmount() != null) {
										totalAmount = totalAmount.add(poDetail.getAmount());
									}
								} else {
									logInfoVO.setIdObject((po.getPoVnm() != null && po.getPoVnm().getId() != null) ? po.getPoVnm().getId().toString() : po.getRefPoNumber());
									logInfoVO.setActionType(LogInfoVO.ACTION_INSERT);
									logInfoVO.setFunctionCode(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_ERR);
									LogUtility.logInfoWs(logInfoVO);
									throw new BusinessException(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_ERR + poManualProductVO.getProductCode());
								}
							}
						}
					}
					// cap nhat lai number, quantity, amount
					poNumber = this.generatePOManualNumber(po.getId(), Constant.PO, Constant.EIGHT_INT);
					po.setPoNumber(poNumber);
					po.setQuantity(totalQuantity);
					po.setAmount(totalAmount);
					poManualDAO.updatePo(po);
				} else {
					List<PoDetail> lstPoDetailOld = poManualDAO.getListPoDetailByPoId(po.getId());
					if (PoManualType.PO_IMPORT.equals(po.getPoType())) {
						for (int i = 0, sz = poManualVOSave.getLstProduct().size(); i < sz; i++) {
							PoManualProductVO poManualProductVO = poManualVOSave.getLstProduct().get(i);
							if (poManualProductVO != null) {
								PoManualFilter<OrderProductVO> filter = new PoManualFilter<>();
								filter.setShopId(shop.getId());
								filter.setProductId(poManualProductVO.getProductId());
								filter.setWarehouseId(po.getWarehouse().getId());
								List<OrderProductVO> lstProduct = poManualDAO.getListOrderProductVO(filter);
								if (lstProduct != null && lstProduct.size() > 0) {
									OrderProductVO productVO = lstProduct.get(0);
									PoDetail poDetail = null;
									int idx = this.checkPoDetailInList(lstPoDetailOld, productVO.getProductId());
									if (idx > -1) {
										poDetail = this.setPoDetailManual(po, poManualProductVO, productVO, sys, logInfoVO, lstPoDetailOld.get(idx));
										lstPoDetailOld.remove(idx);
										commonDAO.updateEntity(poDetail);
									} else {
										poDetail = this.setPoDetailManual(po, poManualProductVO, productVO, sys, logInfoVO, null);
										poDetail = commonDAO.createEntity(poDetail);
									}
									totalQuantity = totalQuantity + poDetail.getQuantity();
									totalAmount = totalAmount.add(poDetail.getAmount());
								}
							}
						}
					} else {
						// update po_return detail
						for (int i = 0, sz = poManualVOSave.getLstProduct().size(); i < sz; i++) {
							PoManualProductVO poManualProductVO = poManualVOSave.getLstProduct().get(i);
							if (poManualProductVO != null) {
								PoManualFilter<OrderProductVO> filter = new PoManualFilter<OrderProductVO>();
								filter.setShopId(shop.getId());
								filter.setId(po.getId());
								filter.setWarehouseId(po.getWarehouse().getId());
								filter.setProductId(poManualProductVO.getProductId());
								filter.setType(PoManualType.PO_RETURN.getValue());
								filter.setRefAsnId(po.getPoVnm().getId());
								List<OrderProductVO> listPoVNMDetail = poManualDAO.getListPoDetailEditByFilter(filter);
								if (listPoVNMDetail != null && listPoVNMDetail.size() > 0) {
									OrderProductVO orderProductVO = listPoVNMDetail.get(0);
									if (poManualProductVO.getQuantity() != null) {
										if (orderProductVO.getConvfact() != null) {
											//int soluongLe = poManualProductVO.getQuantity().intValue() * orderProductVO.getConvfact().intValue();
											int soluongLe = poManualProductVO.getQuantity().intValue();
											if (orderProductVO.getAvailableQuantity() != null) {
												if (soluongLe > orderProductVO.getAvailableQuantity().intValue()) {
													logInfoVO.setIdObject(po.getId() != null ? po.getId().toString() : po.getPoNumber());
													logInfoVO.setActionType(LogInfoVO.ACTION_UPDATE);
													logInfoVO.setFunctionCode(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_AVAILABLE_ERR);
													LogUtility.logInfoWs(logInfoVO);
													throw new BusinessException(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_AVAILABLE_ERR + poManualProductVO.getProductCode());
												}
											} else {
												logInfoVO.setIdObject(po.getId() != null ? po.getId().toString() : po.getPoNumber());
												logInfoVO.setActionType(LogInfoVO.ACTION_UPDATE);
												logInfoVO.setFunctionCode(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_AVAILABLE_ERR);
												LogUtility.logInfoWs(logInfoVO);
												throw new BusinessException(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_AVAILABLE_ERR + poManualProductVO.getProductCode());
											}
											if (orderProductVO.getOldQuantity() == null || soluongLe > orderProductVO.getOldQuantity().intValue()) {
												// oldQuantity: so luong le quantity
												logInfoVO.setIdObject(po.getId() != null ? po.getId().toString() : po.getPoNumber());
												logInfoVO.setActionType(LogInfoVO.ACTION_UPDATE);
												logInfoVO.setFunctionCode(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_OLD_ERR);
												LogUtility.logInfoWs(logInfoVO);
												throw new BusinessException(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_QUANTITY_OLD_ERR + poManualProductVO.getProductCode());
											}
										} else {
											logInfoVO.setIdObject(po.getId() != null ? po.getId().toString() : po.getPoNumber());
											logInfoVO.setActionType(LogInfoVO.ACTION_UPDATE);
											logInfoVO.setFunctionCode(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_CONVFACT_ERR);
											LogUtility.logInfoWs(logInfoVO);
											throw new BusinessException(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_CONVFACT_ERR + poManualProductVO.getProductCode());
										}
									}
									// cap nhat po_detail (don tra)
									PoDetail poDetail = null;
									int idx = this.checkPoDetailInList(lstPoDetailOld, orderProductVO.getProductId());
									if (idx > -1) {
										poDetail = this.setPoDetailManual(po, poManualProductVO, orderProductVO, sys, logInfoVO, lstPoDetailOld.get(idx));
										lstPoDetailOld.remove(idx);
										commonDAO.updateEntity(poDetail);
									} else {
										poDetail = this.setPoDetailManual(po, poManualProductVO, orderProductVO, sys, logInfoVO, null);
										poDetail = commonDAO.createEntity(poDetail);
									}
									if (poDetail.getQuantity() != null) {
										totalQuantity = totalQuantity + poDetail.getQuantity();
									}
									if (poDetail.getAmount() != null) {
										totalAmount = totalAmount.add(poDetail.getAmount());
									}
								} else {
									logInfoVO.setIdObject(po.getId() != null ? po.getId().toString() : po.getPoNumber());
									logInfoVO.setActionType(LogInfoVO.ACTION_UPDATE);
									logInfoVO.setFunctionCode(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_ERR);
									LogUtility.logInfoWs(logInfoVO);
									throw new BusinessException(ExceptionCode.ERR_PO_MANUAL_ASN_PARENT_PRODUCT_ERR + poManualProductVO.getProductCode());
								}
							}
						}
					}
					// delete lstPoDetailOld
					if (lstPoDetailOld != null && !lstPoDetailOld.isEmpty()) {
						for (PoDetail poDetail : lstPoDetailOld) {
							commonDAO.deleteEntity(poDetail);
						}
					}
					// cap nhat lai quantity, amount
					po.setQuantity(totalQuantity);
					po.setAmount(totalAmount);
					poManualDAO.updatePo(po);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return poNumber;
	}
	
	/**
	 * Xu ly checkPoDetailInList
	 * @author vuongmq
	 * @param lstPoDetail
	 * @param productId
	 * @return int
	 * @since Dec 28, 2015
	 */
	private int checkPoDetailInList(List<PoDetail> lstPoDetail, Long productId) {
		int index = -1;
		for (int i = 0, sz = lstPoDetail.size(); i < sz; i ++) {
			PoDetail poDetail = lstPoDetail.get(i);
			if (poDetail != null && poDetail.getProduct() != null 
				&& poDetail.getProduct().getId() != null && poDetail.getProduct().getId().equals(productId)) {
				index = i;
				break;
			}
		}
		return index;
	}
	
	/**
	 * Xu ly setPoDetailManual
	 * @author vuongmq
	 * @param po
	 * @param poManualProductVO
	 * @param productVO
	 * @param sys
	 * @param logInfoVO
	 * @return PoDetail
	 * @throws BusinessException
	 * @since Dec 25, 2015
	 */
	private PoDetail setPoDetailManual(Po po, PoManualProductVO poManualProductVO, OrderProductVO productVO, Date sys, LogInfoVO logInfoVO, PoDetail poDetail) throws BusinessException {
		try {	
			Integer quantity = 0;
			BigDecimal amount = BigDecimal.ZERO;
			if (poDetail == null) {
				PoDetail poDetailNew = new PoDetail();
				poDetailNew.setPo(po);
				if (productVO.getPriceSaleInId() != null) {
					PriceSaleIn priceSaleIn = commonDAO.getEntityById(PriceSaleIn.class, productVO.getPriceSaleInId());
					poDetailNew.setPriceSaleIn(priceSaleIn);
				}
				Product product = commonDAO.getEntityById(Product.class, productVO.getProductId());
				poDetailNew.setProduct(product);
				Integer convfact = productVO.getConvfact();
				poDetailNew.setPackagePrice(productVO.getPackagePrice());
				// lay packageQuantity va retailQuantity tu Quantity
				Integer pkQuantity = poManualProductVO.getQuantity() / convfact;
				Integer reQuantity = poManualProductVO.getQuantity() % convfact;
				poDetailNew.setPackageQuantity(pkQuantity);
				poDetailNew.setConvfact(convfact);
				poDetailNew.setRetailPrice(productVO.getPrice());
				poDetailNew.setRetailQuantity(reQuantity);
				quantity = MathUtil.getQuantityPOManual(poDetailNew.getPackageQuantity(), convfact, poDetailNew.getRetailQuantity());
				amount = MathUtil.getAmountPOManual(poDetailNew.getPackagePrice(), poDetailNew.getPackageQuantity(), poDetailNew.getRetailPrice(), poDetailNew.getRetailQuantity());
				poDetailNew.setQuantity(quantity);
				poDetailNew.setAmount(amount);
				poDetailNew.setPoLineNumber(poManualProductVO.getPoLineNumber());
				poDetailNew.setCreateDate(sys);
				poDetailNew.setCreateUser(logInfoVO.getStaffCode());
				poDetail = poDetailNew;
			} else {
				Integer convfact = poDetail.getConvfact();
				// lay packageQuantity va retailQuantity tu Quantity
				Integer pkQuantity = poManualProductVO.getQuantity() / convfact;
				Integer reQuantity = poManualProductVO.getQuantity() % convfact;
				poDetail.setPackageQuantity(pkQuantity);
				poDetail.setRetailQuantity(reQuantity);
				quantity = MathUtil.getQuantityPOManual(poDetail.getPackageQuantity(), convfact, poDetail.getRetailQuantity());
				amount = MathUtil.getAmountPOManual(poDetail.getPackagePrice(), poDetail.getPackageQuantity(), poDetail.getRetailPrice(), poDetail.getRetailQuantity());
				poDetail.setQuantity(quantity);
				poDetail.setAmount(amount);
				poDetail.setPoLineNumber(poManualProductVO.getPoLineNumber());
				poDetail.setUpdateUser(logInfoVO.getStaffCode());
				poDetail.setUpdateDate(sys);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return poDetail;
	}
	
	/**
	 * Xu ly generatePOManualNumber
	 * @author vuongmq
	 * @param id
	 * @param poType
	 * @param sizeGen
	 * @return String
	 * @throws DataAccessException
	 * @since Dec 9, 2015
	 */
	private String generatePOManualNumber(Long id, String poType, Integer sizeGen) throws DataAccessException {
		if (id == null) {
			List<Object> params = new ArrayList<Object>();
			String sql = "select max(po_id) as count from po ";
			int val = repo.countBySQL(sql, params);
			val = val + 1;
			id = (long) val;
		}
		String result = id.toString();
		String temp = "";
		for (int i = 0, sz = sizeGen.intValue() - result.length(); i < sz; i++) {
			temp += "0";
		}
		return poType + temp + result;
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#getListPoVNMByFilter(ths.dms.core.entities.enumtype.PoVnmFilter)
	 */
	@Override
	public List<PoVnm> getListPoVNMByFilter(PoVnmFilter filter) throws BusinessException {
		try {
			return poManualDAO.getListPoVNMByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#getListPoVNMManualByFilter(ths.dms.core.entities.enumtype.PoVnmFilter)
	 */
	@Override
	public List<PoVnmManualVO> getListPoVNMManualByFilter(PoVnmFilter filter) throws BusinessException {
		try {
			return poManualDAO.getListPoVNMManualByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#getListObjectVOPoVNMManualByFilter(ths.dms.core.entities.enumtype.PoVnmFilter)
	 */
	@Override
	public ObjectVO<PoVnmManualVO> getListObjectVOPoVNMManualByFilter(PoVnmFilter filter) throws BusinessException {
		try {
			List<PoVnmManualVO> lst = poManualDAO.getListPoVNMManualByFilter(filter);
			ObjectVO<PoVnmManualVO> vo = new ObjectVO<>();
			vo.setLstObject(lst);
			vo.setkPaging(filter.getkPagingManualVO());
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#getListPoVNMDetailByFilter(ths.dms.core.entities.enumtype.PoVnmFilter)
	 */
	@Override
	public List<OrderProductVO> getListPoVNMDetailByFilter(PoVnmFilter filter) throws BusinessException {
		try {
			return poManualDAO.getListPoVNMDetailByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#getListPoManualVOByFilter(ths.dms.core.entities.filter.PoManualFilter)
	 */
	@Override
	public ObjectVO<PoManualVO> getListPoManualVOByFilter(PoManualFilter<PoManualVO> filter) throws BusinessException {
		try {
			List<PoManualVO> lstObject = poManualDAO.getListPoManualVOByFilter(filter);
			ObjectVO<PoManualVO> vo = new ObjectVO<PoManualVO>();
			vo.setLstObject(lstObject);
			vo.setkPaging(filter.getkPaging());
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#getListPoManualDetailVOByFilter(ths.dms.core.entities.filter.PoManualFilter)
	 */
	@Override
	public List<PoManualVODetail> getListPoManualDetailVOByFilter(PoManualFilter<PoManualVODetail> filter) throws BusinessException {
		try {
			return poManualDAO.getListPoManualDetailVOByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#updateStatusOrderManual(java.util.List, java.lang.Integer, ths.dms.core.entities.vo.LogInfoVO)
	 */
	@Override
	public List<PoManualVOError> updateStatusOrderManual(List<Long> lstId, Integer approvedStep, LogInfoVO logInfoVO) throws BusinessException {
		List<PoManualVOError> lstError = new ArrayList<PoManualVOError>();
		try {
			if (lstId != null && lstId.size() > 0) {
				Date sysDate = commonDAO.getSysDate();
				ApprovalStepPo stepTmp = ApprovalStepPo.parseValue(approvedStep);
				for (int i = 0, sz = lstId.size(); i < sz; i++) {
					Long poId = lstId.get(i); 
					Po po = commonDAO.getEntityById(Po.class, poId);
					if (po != null) {
						PoManualVOError poManualVOError = this.updateStatusPO(po, stepTmp, sysDate, logInfoVO);
						if (poManualVOError != null && !StringUtility.isNullOrEmpty(poManualVOError.getPoNumber())) {
							lstError.add(poManualVOError);
						}
					}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return lstError;
	}
	
	/**
	 * Xu ly updateStatusPO
	 * @author vuongmq
	 * @param po
	 * @param stepTmp
	 * @param sysDate
	 * @param logInfo
	 * @return PoManualVOError loi cua tung don hang
	 * @since Dec 22, 2015
	 */
	private PoManualVOError updateStatusPO(final Po po, final ApprovalStepPo stepTmp, final Date sysDate, final LogInfoVO logInfo) {
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		PoManualVOError poManualVOError = transactionTemplate.execute(new TransactionCallback<PoManualVOError>() {
			@Override
			public PoManualVOError doInTransaction(TransactionStatus transactionStatus) {
				PoManualVOError resultErr = new PoManualVOError();
				try {
					if (ApprovalStepPo.DESTROY.equals(stepTmp) || ApprovalStepPo.NEW.equals(stepTmp)) {
						// chon huy;  status = 1, approve_step = 1 -> status = 0
						// chon gui;  status = 1, approve_step = 1 -> approve_step = 2
						if (ApprovalStepPo.NEW.equals(po.getApprovedStep()) && ActiveType.RUNNING.equals(po.getStatus())) {
							if (ApprovalStepPo.DESTROY.equals(stepTmp)) {
								po.setStatus(ActiveType.STOPPED);
								po.setUpdateDate(sysDate);
								po.setUpdateUser(logInfo.getStaffCode());
								poManualDAO.updatePo(po);
							} else {
								if (PoManualType.PO_RETURN.equals(po.getPoType())) {
									//don tra hang thi validate so luong hang tra phai <= ton kho dap ung cua san pham.
									String productCodeErr = checkQuantityStockPoReturn(po);
									if (!StringUtility.isNullOrEmpty(productCodeErr)) {
										resultErr.setId(po.getId());
										resultErr.setPoNumber(po.getPoNumber());
										resultErr.setFailure(Constant.X);
										resultErr.setReason(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "po_manual_process_po_return_quantity_stock_err", productCodeErr));
										throw new BusinessException(ExceptionCode.ERROR);
									}
								}
								po.setApprovedStep(ApprovalStepPo.WAITING);
								po.setUpdateDate(sysDate);
								po.setUpdateUser(logInfo.getStaffCode());
								poManualDAO.updatePo(po);
							}
						} else {
							resultErr.setId(po.getId());
							resultErr.setPoNumber(po.getPoNumber());
							resultErr.setFailure(Constant.X);
							resultErr.setReason(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "po_manual_process_po_destroy_new_err"));
							throw new BusinessException(ExceptionCode.ERROR);
						}
						
					} else if (ApprovalStepPo.REJECTED.equals(stepTmp) || ApprovalStepPo.APPROVED.equals(stepTmp)) {
						// chon tu choi: chi la don tra status = 1, approve_step = 2
						// chon duyet; chi la don tra status = 1, approve_step = 2
						if (ApprovalStepPo.WAITING.equals(po.getApprovedStep()) && ActiveType.RUNNING.equals(po.getStatus())) {
							if (ApprovalStepPo.APPROVED.equals(stepTmp)) {
								if (PoManualType.PO_IMPORT.equals(po.getPoType())) {
									resultErr.setId(po.getId());
									resultErr.setPoNumber(po.getPoNumber());
									resultErr.setFailure(Constant.X);
									resultErr.setReason(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "po_manual_process_po_return_only_err"));
									throw new BusinessException(ExceptionCode.ERROR);
								}
								//don tra hang thi validate so luong hang tra phai <= ton kho dap ung cua san pham
								String productCodeErr = checkQuantityStockPoReturn(po);
								if (!StringUtility.isNullOrEmpty(productCodeErr)) {
									resultErr.setId(po.getId());
									resultErr.setPoNumber(po.getPoNumber());
									resultErr.setFailure(Constant.X);
									resultErr.setReason(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "po_manual_process_po_return_quantity_stock_err", productCodeErr));
									throw new BusinessException(ExceptionCode.ERROR);
								}
								po.setApprovedStep(ApprovalStepPo.APPROVED);
								po.setUpdateDate(sysDate);
								po.setUpdateUser(logInfo.getStaffCode());
								poManualDAO.updatePo(po);
								// dong thoi insert vao bang po_vnm va po_vnm_detail thong tin don tra hang
								PoVnm poVnm = new PoVnm();
								poVnm.setPoAutoNumber(po.getPoNumber());
								if (!StringUtility.isNullOrEmpty(po.getRefPoNumber())) {
									poVnm.setSaleOrderNumber(po.getRefPoNumber() + "_" + Constant.RE);
								}
								/*poVnm.setPoCoNumber(po.getRefPoNumber());
								poVnm.setFromPoVnmId(po.getPoVnm());*/
								poVnm.setShop(po.getShop());
								poVnm.setWarehouse(po.getWarehouse());
								poVnm.setNote(po.getNote());
								poVnm.setTotal(po.getAmount());
								poVnm.setAmount(po.getAmount());
								poVnm.setQuantity(po.getQuantity());
								poVnm.setPoVnmDate(po.getPoDate());
								poVnm.setRequestDate(po.getRequestDate());
								poVnm.setCreateDate(po.getCreateDate());
								poVnm.setType(PoType.RETURNED_SALES_ORDER);
								poVnm.setStatus(PoVNMStatus.NOT_IMPORT);
								poVnm.setObjectType(PoObjectType.NPP);
								poVnm = commonDAO.createEntity(poVnm);
								if (poVnm != null && poVnm.getId() != null) {
									List<PoVnmDetail> lstPoVNMDetail = new ArrayList<PoVnmDetail>();
									List<PoDetail> lstPoDetail = poManualDAO.getListPoDetailByPoId(po.getId());
									if (lstPoDetail != null && !lstPoDetail.isEmpty()) {
										for (PoDetail poDetail : lstPoDetail) {
											PoVnmDetail poVnmDetail = new PoVnmDetail();
											poVnmDetail.setPoVnm(poVnm);
											poVnmDetail.setPoVnmDate(poVnm.getPoVnmDate());
											poVnmDetail.setProduct(poDetail.getProduct());
											poVnmDetail.setAmount(poDetail.getAmount());
											poVnmDetail.setQuantity(poDetail.getQuantity());
											poVnmDetail.setQuantityPackage(poDetail.getPackageQuantity());
											poVnmDetail.setQuantityRetail(poDetail.getRetailQuantity());
											poVnmDetail.setPriceValue(poDetail.getRetailPrice());
											poVnmDetail.setPricePackage(poDetail.getPackagePrice());
											poVnmDetail.setPoLineNumber(poDetail.getPoLineNumber());
											poVnmDetail.setWarehouse(po.getWarehouse());
											poVnmDetail.setNote(po.getNote());
											lstPoVNMDetail.add(poVnmDetail);
										}
									}
									if (!lstPoVNMDetail.isEmpty()) {
										commonDAO.creatListEntity(lstPoVNMDetail);
									}
								}
							} else {
								if (PoManualType.PO_IMPORT.equals(po.getPoType())) {
									resultErr.setId(po.getId());
									resultErr.setPoNumber(po.getPoNumber());
									resultErr.setFailure(Constant.X);
									resultErr.setReason(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "po_manual_process_po_return_only_err"));
									throw new BusinessException(ExceptionCode.ERROR);
								}
								po.setStatus(ActiveType.STOPPED);
								po.setUpdateDate(sysDate);
								po.setUpdateUser(logInfo.getStaffCode());
								poManualDAO.updatePo(po);
							}
						} else {
							resultErr.setId(po.getId());
							resultErr.setPoNumber(po.getPoNumber());
							resultErr.setFailure(Constant.X);
							resultErr.setReason(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "po_manual_process_po_rejected_approved_err"));
							throw new BusinessException(ExceptionCode.ERROR);
						}
					}
				} catch (Exception e) {
					LogUtility.logError(e, "sessionId = " + logInfo.getSessionId() + "; updateStatusPO. po_id = " + po.getId() + "; " + e.getMessage());
					transactionStatus.setRollbackOnly();
				}
				return resultErr;
			}
		});
		return poManualVOError;
	}
	
	/**
	 * Xu ly checkQuantityStockPoReturn
	 * @author vuongmq
	 * @param po
	 * @return String chuoi san pham loi
	 * @throws BusinessException
	 * @since Dec 22, 2015
	 */
	private String checkQuantityStockPoReturn(final Po po) throws BusinessException {
		StringBuilder productCodeErr = new StringBuilder();
		try {
			if (po != null && po.getShop() != null && po.getWarehouse() != null) {
				PoManualFilter<PoManualVODetail> filter = new PoManualFilter<PoManualVODetail>();
				filter.setId(po.getId());
				List<PoManualVODetail> lstPoManualDetail = poManualDAO.getListPoManualDetailVOByFilter(filter);
				if (lstPoManualDetail != null && !lstPoManualDetail.isEmpty()) {
					for (PoManualVODetail poManualVODetail : lstPoManualDetail) {
						if (poManualVODetail != null) {
							StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwner(poManualVODetail.getProductId(), StockObjectType.SHOP, po.getShop().getId(), po.getWarehouse().getId());
							if (poManualVODetail.getQuantity() != null) {
								if (stockTotal != null && stockTotal.getAvailableQuantity() != null) {
									if (poManualVODetail.getQuantity().intValue() > stockTotal.getAvailableQuantity().intValue()) {
										productCodeErr.append(", ").append(poManualVODetail.getProductCode());
									}
								} else {
									productCodeErr.append(", ").append(poManualVODetail.getProductCode());
								}
							}
						}
					}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		if (productCodeErr.length() > 0) {
			productCodeErr = productCodeErr.replace(0, 2, "");
		}
		return productCodeErr.toString();
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoManualMgr#getListPoDetailEditByFilter(ths.dms.core.entities.filter.PoManualFilter)
	 */
	@Override
	public List<OrderProductVO> getListPoDetailEditByFilter(PoManualFilter<OrderProductVO> filter) throws BusinessException {
		try {
			return poManualDAO.getListPoDetailEditByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

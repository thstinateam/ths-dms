package ths.dms.core.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ShopProduct;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopProductFilter;
import ths.dms.core.entities.enumtype.ShopProductType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.ShopProductDAO;

public class ShopProductMgrImpl implements ShopProductMgr {

	@Autowired
	ShopProductDAO shopProductDAO;
	
	
	@Override
	public ShopProduct getShopProductByImport(String shopCode, String codeFlag, String calendarD, int flag) throws BusinessException {
		try {
			return shopProductDAO.getShopProductByImport(shopCode, codeFlag, calendarD, flag);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ShopProduct createShopProduct(ShopProduct shopProduct, LogInfoVO logInfo) throws BusinessException {
		try {
			return shopProductDAO.createShopProduct(shopProduct, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateShopProduct(ShopProduct shopProduct, LogInfoVO logInfo) throws BusinessException {
		try {
			shopProductDAO.updateShopProduct(shopProduct, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional (rollbackFor = Exception.class)
	public void deteteListShopProduct(List<ShopProduct> lstData, LogInfoVO logInfo, ShopProductFilter filter) throws BusinessException {
		try {
			List<Long> lstId = new ArrayList<Long>();
			//Delete
			for(ShopProduct temp:lstData){
				lstId.add(temp.getId());
			}
			//Lay danh sach shop product by lst cho co san
			List<ShopProduct> lstUpdate = shopProductDAO.getLitsShopProductByListId(lstId);
			if(lstUpdate!=null && lstUpdate.size()>0){
				//Delete
				for(ShopProduct temp:lstUpdate){
					temp.setStatus(ActiveType.DELETED);
					temp.setUpdateUser(filter.getUserName());
					temp.setUpdateDate(new Date());
					this.updateShopProduct(temp, logInfo);
				}	
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public void deleteShopProduct(ShopProduct shopProduct, LogInfoVO logInfo) throws BusinessException{
		try {
			shopProductDAO.deleteShopProduct(shopProduct, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ShopProduct getShopProductById(Long id) throws BusinessException{
		try {
			return id==null?null:shopProductDAO.getShopProductById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ShopProduct getShopProductByCat(Long shopId, Long catId) throws BusinessException{
		try {
			return shopProductDAO.getShopProductByCat(shopId, catId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ShopProduct getShopProductByProduct(Long shopId, Long productId) throws BusinessException{
		try {
			return shopProductDAO.getShopProductByProduct(shopId, productId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ShopProduct> getListShopProduct(KPaging<ShopProduct> kPaging, String shopCode,
			String shopName, Long catId, String productCode, ActiveType status,
			ShopProductType type, List<Long> listOrgAccess) throws BusinessException {
		try {
			List<ShopProduct> lst = shopProductDAO.getListShopProduct(kPaging, shopCode, shopName, catId, productCode, status, type, listOrgAccess);
			ObjectVO<ShopProduct> vo = new ObjectVO<ShopProduct>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean isUsingByOthers(long shopProductId) throws BusinessException {
		try {
			return shopProductDAO.isUsingByOthers(shopProductId);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<ShopProduct> getListShopProduct1(KPaging<ShopProduct> kPaging, String shopCode,
			String shopName, List<String> listProInfoCode, String productCode, ActiveType status,
			ShopProductType type, List<Long> lstParentShopId, Boolean flag) throws BusinessException {
		try {
			List<ShopProduct> lst = shopProductDAO.getListShopProduct(kPaging, shopCode, shopName, listProInfoCode, productCode, status, type, lstParentShopId, flag);
			ObjectVO<ShopProduct> vo = new ObjectVO<ShopProduct>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
}

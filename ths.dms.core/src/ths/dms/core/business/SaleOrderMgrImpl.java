package ths.dms.core.business;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.zip.DataFormatException;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Car;
import ths.dms.core.entities.CompoundSaleOrder;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.Debit;
import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.Invoice;
import ths.dms.core.entities.InvoiceDetail;
import ths.dms.core.entities.KSCusProductReward;
import ths.dms.core.entities.KSCustomer;
import ths.dms.core.entities.PayReceived;
import ths.dms.core.entities.PaymentDetail;
import ths.dms.core.entities.PoCustomer;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.ProcessHistory;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.PromotionCustomerMap;
import ths.dms.core.entities.PromotionMapDelta;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.PromotionStaffMap;
import ths.dms.core.entities.RoutingCustomer;
import ths.dms.core.entities.RptAccumulativePromotionProgram;
import ths.dms.core.entities.RptAccumulativePromotionProgramDetail;
import ths.dms.core.entities.RptCTTLPay;
import ths.dms.core.entities.RptCTTLPayDetail;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.SaleOrderPromoLot;
import ths.dms.core.entities.SaleOrderPromotion;
import ths.dms.core.entities.SalePromoMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffCustomer;
import ths.dms.core.entities.StockLock;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.StockTransLot;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActionSaleOrder;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.CommercialSupportType;
import ths.dms.core.entities.enumtype.CommonContanst;
import ths.dms.core.entities.enumtype.DebitDetailType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.DebtPaymentType;
import ths.dms.core.entities.enumtype.InvoiceCustPayment;
import ths.dms.core.entities.enumtype.InvoiceFilter;
import ths.dms.core.entities.enumtype.InvoiceStatus;
import ths.dms.core.entities.enumtype.IsFreeItemInSaleOderDetail;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.PayReceivedType;
import ths.dms.core.entities.enumtype.PaymentType;
import ths.dms.core.entities.enumtype.PrintOrderFilter;
import ths.dms.core.entities.enumtype.ProcessHistoryType;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.PromotionType;
import ths.dms.core.entities.enumtype.ReceiptType;
import ths.dms.core.entities.enumtype.RewardType;
import ths.dms.core.entities.enumtype.SaleOrderDetailFilter;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.SpParam;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTotalVOFilter;
import ths.dms.core.entities.enumtype.ValueType;
import ths.dms.core.entities.enumtype.WarehouseType;
import ths.dms.core.entities.filter.ConfirmFilter;
import ths.dms.core.entities.filter.KeyShopFilter;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.filter.SaleOrderDetailVATFilter;
import ths.dms.core.entities.filter.SaleOrderPromoDetailFilter;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.entities.vo.CommercialSupportCodeVO;
import ths.dms.core.entities.vo.CommercialSupportVO;
import ths.dms.core.entities.vo.GroupLevelDetailVO;
import ths.dms.core.entities.vo.KeyShopVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.OrderXmlDetailVO;
import ths.dms.core.entities.vo.OrderXmlHeaderVO;
import ths.dms.core.entities.vo.OrderXmlLineDataVO;
import ths.dms.core.entities.vo.OrderXmlObject;
import ths.dms.core.entities.vo.OrderXmlVO;
import ths.dms.core.entities.vo.PrintDeliveryCustomerGroupVO;
import ths.dms.core.entities.vo.PrintDeliveryCustomerGroupVO1;
import ths.dms.core.entities.vo.PrintDeliveryGroupExportVO;
import ths.dms.core.entities.vo.PrintDeliveryGroupExportVO1;
import ths.dms.core.entities.vo.PrintDeliveryGroupExportVO2;
import ths.dms.core.entities.vo.PrintDeliveryGroupVO;
import ths.dms.core.entities.vo.PrintDeliveryGroupVO1;
import ths.dms.core.entities.vo.PrintOrderVO;
import ths.dms.core.entities.vo.ProductStockTotal;
import ths.dms.core.entities.vo.ProgramCodeVO;
import ths.dms.core.entities.vo.PromotionMapDeltaVO;
import ths.dms.core.entities.vo.RptAccumulativePromotionProgramVO;
import ths.dms.core.entities.vo.SaleOrderDetailForInvoidVO;
import ths.dms.core.entities.vo.SaleOrderDetailGroupVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO2;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderLotVOEx;
import ths.dms.core.entities.vo.SaleOrderPromoLotVO;
import ths.dms.core.entities.vo.SaleOrderPromotionDetailVO;
import ths.dms.core.entities.vo.SaleOrderPromotionVO;
import ths.dms.core.entities.vo.SaleOrderStockDetailVO;
import ths.dms.core.entities.vo.SaleOrderStockVO;
import ths.dms.core.entities.vo.SaleOrderStockVOTemp;
import ths.dms.core.entities.vo.SaleOrderVO;
import ths.dms.core.entities.vo.SaleOrderVOEx;
import ths.dms.core.entities.vo.SaleOrderVOEx2;
import ths.dms.core.entities.vo.SaleProductVO;
import ths.dms.core.entities.vo.SearchSaleTransactionErrorVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.TaxInvoiceDetailVO;
import ths.dms.core.entities.vo.TaxInvoiceVO;
import ths.core.entities.vo.rpt.RptAggegateVATInvoiceDataVO;
import ths.core.entities.vo.rpt.RptBHTSP_DSNhanVienBH;
import ths.core.entities.vo.rpt.RptExSaleOrder1VO;
import ths.core.entities.vo.rpt.RptExSaleOrder2VO;
import ths.core.entities.vo.rpt.RptExSaleOrderVO;
import ths.core.entities.vo.rpt.RptOrderByPromotionDataVO;
import ths.core.entities.vo.rpt.RptPGHVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_DataConvert;
import ths.core.entities.vo.rpt.RptSaleOrderByProductDataVO;
import ths.core.entities.vo.rpt.RptSaleOrderByProductLv01VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.Constant;
import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.ExceptionUtil;
import ths.dms.core.common.utils.LogUtility;
import ths.dms.core.common.utils.MessageUtil;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.XmlGenerator;
import ths.dms.core.dao.ApParamDAO;
import ths.dms.core.dao.CarDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.CompoundSaleOrderDAO;
import ths.dms.core.dao.CustomerDAO;
import ths.dms.core.dao.DebitDAO;
import ths.dms.core.dao.DebitDetailDAO;
import ths.dms.core.dao.DisplayProgrameDAO;
import ths.dms.core.dao.InvoiceDAO;
import ths.dms.core.dao.KeyShopDAO;
import ths.dms.core.dao.PayReceivedDAO;
import ths.dms.core.dao.PaymentDetailDAO;
import ths.dms.core.dao.PoCustomerDAO;
import ths.dms.core.dao.PriceDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.ProductLotDAO;
import ths.dms.core.dao.PromotionCustomerMapDAO;
import ths.dms.core.dao.PromotionProgramDAO;
import ths.dms.core.dao.PromotionShopMapDAO;
import ths.dms.core.dao.PromotionStaffMapDAO;
import ths.dms.core.dao.RoutingCustomerDAO;
import ths.dms.core.dao.SaleOrderDAO;
import ths.dms.core.dao.SaleOrderDetailDAO;
import ths.dms.core.dao.SaleOrderLotDAO;
import ths.dms.core.dao.SaleOrderPromoDetailDAO;
import ths.dms.core.dao.SaleOrderPromotionDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.ShopLockDAO;
import ths.dms.core.dao.StaffCustomerDAO;
import ths.dms.core.dao.StaffDAO;
import ths.dms.core.dao.StockLockDAO;
import ths.dms.core.dao.StockTotalDAO;
import ths.dms.core.dao.StockTransDAO;
import ths.dms.core.dao.StockTransLotDAO;
import ths.dms.core.dao.repo.IRepository;

public class SaleOrderMgrImpl implements SaleOrderMgr {

	@Autowired
	private IRepository repo;

	@Autowired
	private StockTransDAO stockTransDAO;
	
	@Autowired
	PlatformTransactionManager transactionManager;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	SaleOrderDAO saleOrderDAO;

	@Autowired
	PromotionProgramDAO promotionProgramDAO;

	@Autowired
	DisplayProgrameDAO displayProgramDAO;

	@Autowired
	PriceDAO priceDAO;

	@Autowired
	ProductLotDAO productLotDAO;

	@Autowired
	StockTotalDAO stockTotalDAO;
	
	@Autowired
	StockLockDAO stockLockDAO;
	
	@Autowired
	StockManagerMgr stockManagerMgr;

	@Autowired
	InvoiceDAO invoiceDAO;

	@Autowired
	SaleOrderDetailDAO saleOrderDetailDAO;

	@Autowired
	SaleOrderLotDAO saleOrderLotDAO;

	@Autowired
	SaleOrderPromoDetailDAO saleOrderPromoDetailDAO;
	
	@Autowired
	SaleOrderPromotionDAO saleOrderPromotionDAO;

	@Autowired
	StaffDAO staffDAO;

	@Autowired
	CarDAO carDAO;

	@Autowired
	DebitDetailDAO debitDetailDAO;

	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	ProductDAO productDAO;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	DebitDAO debitDAO;

	@Autowired
	RoutingCustomerDAO routingCustomerDAO;

	@Autowired
	PromotionShopMapDAO promotionShopMapDAO;

	@Autowired
	PromotionCustomerMapDAO promotionCustomerMapDAO;

	@Autowired
	PromotionStaffMapDAO promotionStaffMapDAO;

	@Autowired
	PromotionStaffMapDAO promotionStaffMap;

	@Autowired
	StaffCustomerDAO staffCustomerDAO;

	@Autowired
	ApParamDAO apParamDAO;

	@Autowired
	ShopLockDAO shopLockDAO;

	@Autowired
	PaymentDetailDAO paymentDetailDAO;

	@Autowired
	PayReceivedDAO payReceivedDAO;

	@Autowired
	PoCustomerDAO poCustomerDAO;

	@Autowired
	CompoundSaleOrderDAO compoundSaleOrderDAO;
	
	@Autowired
	PromotionProgramMgr promotionProgramMgr;
	
	@Autowired
	ProductMgr productMgr;
	
	@Autowired
	StockTransLotDAO stockTransLotDAO;
	
	@Autowired
	ApParamMgr apParamMgr;
	
	@Autowired
	CycleMgr cycleMgr;
	
	@Autowired
	KeyShopDAO keyShopDAO;

	private static final String KS = "KS";//program_type_code for sale_order_detail
	private static final int PROMOTION = 1;
	
	@Transactional(rollbackFor = Exception.class)
	private void changePromotionQuantity(Integer index, LogInfoVO logInfo, String promotionCode, String shopCode, String customerShortCode, Long customerTypeId, Staff staff, Shop shop)
			throws DataAccessException, BusinessException {

		if (!StringUtility.isNullOrEmpty(promotionCode)) {
			// get promotionShopMap
			PromotionProgram pg = promotionProgramDAO.getPromotionProgramByCode(promotionCode);
			if (pg != null) {
				List<PromotionShopMap> lstPsm = promotionShopMapDAO.getListPromotionShopMapForUpdate(null, pg.getId(), shopCode, null, null, ActiveType.RUNNING);
				if (lstPsm != null && lstPsm.size() > 0) {
					PromotionShopMap psm = lstPsm.get(0);
					Integer quantityReceived = psm.getQuantityReceived() != null ? psm.getQuantityReceived() : 0;
					if (psm != null && psm.getQuantityMax() != null && quantityReceived + index > psm.getQuantityMax())
						throw new BusinessException(ExceptionCode.RECEIVED_QUANTITY_LARGER_THAN_MAX_QUANTITY);
					psm.setQuantityReceived(quantityReceived + index);
					promotionShopMapDAO.updatePromotionShopMap(psm, logInfo);
				}
			}

			// get promotionCustomerMap - khach hang
			Customer customer = customerDAO.getCustomerByCodeEx(StringUtility.getFullCode(shopCode, customerShortCode), ActiveType.RUNNING);
			if (customer == null)
				throw new BusinessException(ExceptionCode.CUSTOMER_IS_NULL);
			List<PromotionCustomerMap> lstPcm = promotionCustomerMapDAO.getListPromotionCustomerMapForUpdate(pg.getId(), customer.getId(), ActiveType.RUNNING);
			if (lstPcm != null && lstPcm.size() > 0) {
				PromotionCustomerMap pcm = lstPcm.get(0);
				Integer quantityReceived = pcm.getQuantityReceived() != null ? pcm.getQuantityReceived() : 0;
				if (pcm != null && pcm.getQuantityMax() != null && quantityReceived + index > pcm.getQuantityMax())
					throw new BusinessException(ExceptionCode.RECEIVED_QUANTITY_LARGER_THAN_MAX_QUANTITY);
				pcm.setQuantityReceived(quantityReceived + index);
				promotionCustomerMapDAO.updatePromotionCustomerMap(pcm, logInfo);
			}

			// get promotionStaffMap - NVBH
			if (staff == null)
				throw new BusinessException(ExceptionCode.STAFF_ID_IS_NULL);
			if (shop == null)
				throw new BusinessException(ExceptionCode.SHOP_ID_IS_NULL);

			List<PromotionStaffMap> lstPstm = promotionStaffMapDAO.getListPromotionStaffMapForCreateOrder(pg.getId(), staff.getId(), shop.getId(), ActiveType.RUNNING);
			if (lstPstm != null && lstPstm.size() > 0) {
				PromotionStaffMap pstm = lstPstm.get(0);
				Integer quantityReceived = pstm.getQuantityReceived() != null ? pstm.getQuantityReceived() : 0;
				if (pstm != null && pstm.getQuantityMax() != null && quantityReceived + index > pstm.getQuantityMax())
					throw new BusinessException(ExceptionCode.RECEIVED_QUANTITY_LARGER_THAN_MAX_QUANTITY);
				pstm.setQuantityReceived(quantityReceived + index);
				promotionStaffMapDAO.updatePromotionStaffMap(pstm, logInfo);
			}
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public SaleOrder modifySaleOrder(SaleOrder salesOrder, List<SaleProductVO> lstSaleProductVO, List<SaleProductVO> lstPromoProductVO, Boolean isCheckQuantity, List<SaleOrderPromotionVO> lstOrderPromotion,
			List<SaleOrderPromotionDetailVO> lstOrderPromoDetail, Integer isSaveNotCheckDebit, List<KeyShopVO> lstKeyShopVO,LogInfoVO logInfoVO ) throws BusinessException {
		try {
			// Lay danh sach chi tiet don hang
			List<SaleOrderDetail> lstSaleOrderDetail = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(salesOrder.getId(), null);
			LogInfoVO logInfo = new LogInfoVO();
			logInfo.setStaffCode(salesOrder.getUpdateUser());
			
			/*
			 * Xoa sale order detail, sale_order_lot,
			 * sale_order_promotion_detail
			 */
			deleteSaleOrderDetail(salesOrder, lstSaleOrderDetail, logInfo);
//			/*
//			 * Xoa thong tin khuyen mai tich luy (rpt_cttl_pay, rpt_cttl_detail_pay)
//			 */
//			deleteAccumulative(salesOrder.getId());
			
			// cap nhat bang routing_customer
			Staff st = null;
			if (salesOrder.getCustomer() != null && salesOrder.getShop() != null) {
				RoutingCustomerFilter filter = new RoutingCustomerFilter();
				filter.setCustomerId(salesOrder.getCustomer().getId());
				filter.setShopId(salesOrder.getShop().getId());
				filter.setOrderDate(salesOrder.getOrderDate());
				List<Staff> lstS = staffDAO.getSaleStaffByDate(filter);
				if (lstS != null && lstS.size() > 0) {
					st = lstS.get(0);
				}
			}
			if (salesOrder.getStaff() != null && st != null && salesOrder.getStaff().getId().equals(st.getId())) {
				st.setLastOrder(null);
				staffDAO.updateStaff(st, null);
			}
			// tao moi sale order
			return this.createOneSaleOrder(salesOrder, lstSaleProductVO, lstPromoProductVO, true, isCheckQuantity, lstOrderPromotion, lstOrderPromoDetail, isSaveNotCheckDebit, lstKeyShopVO,logInfoVO);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * Thuc hien xoa chi tiet don hang trong lstSaleOrderDetail.
	 * 
	 * @author tulv2
	 * @since 26.09.2014
	 * 
	 * @param salesOrder
	 * @param lstSaleOrderDetail
	 * @param lstPromotionCode
	 * @param logInfo
	 * @throws DataAccessException
	 * @throws BusinessException
	 */
	private void deleteSaleOrderDetail(SaleOrder salesOrder, List<SaleOrderDetail> lstSaleOrderDetail, LogInfoVO logInfo) throws DataAccessException, BusinessException {
		
		//xoa SALE_ORDER_PROMO_LOT truoc khi xoa saleOrderLot va saleOrderDetail
		List<SaleOrderPromoLot> lstPromoLot = saleOrderLotDAO.getListSaleOrderPromoLotBySaleOrderId(salesOrder.getId());
		for (SaleOrderPromoLot vo : lstPromoLot) {
			commonDAO.deleteEntity(vo);
		}
		//--------------------------------------
		//xoa SALE_PROMO_MAP 
		List<SalePromoMap> lstPromoMap = saleOrderDAO.getListSalePromoMapBySaleOrder(salesOrder.getId());
		for (SalePromoMap vo : lstPromoMap) {
			commonDAO.deleteEntity(vo);
		}
		//--------------------------------------
		List<SaleOrderPromotion> lstSOP = saleOrderDAO.getListSaleOrderPromotionBySaleOrder(salesOrder.getId());
		for (SaleOrderDetail sod : lstSaleOrderDetail) {
			if (sod.getProduct() != null && sod.getQuantity() != null) {
				List<SaleOrderLot> lstSaleOrderLot1 = saleOrderLotDAO.getListSaleOrderLotBySaleOrderDetail(null, sod.getId());
				for (SaleOrderLot sol : lstSaleOrderLot1) {
					StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(sod.getProduct().getId(), StockObjectType.SHOP, sod.getShop().getId(), sol.getWarehouse().getId());// tulv2 26.09.2014
					if (stockTotal != null) {
						
						/**Log conent */
						try {		
							logInfo.getContenFormat(
									sod.getProduct().getId(), 
									sol.getWarehouse().getId(), 
									sod.getQuantity(), 
									stockTotal.getAvailableQuantity(), 
									stockTotal.getAvailableQuantity() + sol.getQuantity(), 
									stockTotal.getQuantity(), 
									stockTotal.getQuantity());							
						} catch (Exception e) {}
						/** End log conent */
						
						stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() + sol.getQuantity());
						stockTotalDAO.updateStockTotal(stockTotal); // Cap nhat TON KHO DAP UNG trong StockTotal
						
					}
					saleOrderLotDAO.deleteSaleOrderLot(sol); // Xoa SALE ORDER LOT
				}
			}
			/** Xoa saleorderpromotiondetail *; */
			List<SaleOrderPromoDetail> orderPromoDetails = saleOrderDetailDAO.getListSaleOrderPromoDetail(sod.getId());
			if (orderPromoDetails != null && orderPromoDetails.size() > 0) {
				for (SaleOrderPromoDetail sop : orderPromoDetails) {
					commonDAO.deleteEntity(sop);
				}
			}
			saleOrderDetailDAO.deleteSaleOrderDetail(sod);// Xoa SALE_ODER_DETAIL
		}
		// giam so suat nhan khuyen mai theo ma CTKM
		/*if (lstSOP != null && lstSOP.size() > 0) {
			Long customerTypeId = 0L;
			if (salesOrder.getCustomer().getChannelType() != null) {
				customerTypeId = salesOrder.getCustomer().getChannelType().getId();
			}
			for (SaleOrderPromotion vo : lstSOP) {
				PromotionProgram pg = vo.getPromotionProgram();
				if (pg != null) {
					String promotionCode = pg.getPromotionProgramCode();
					this.changePromotionQuantity(-vo.getQuantityReceived(), logInfo, promotionCode, salesOrder.getCustomer().getShop().getShopCode(), 
								salesOrder.getCustomer().getShortCode(), customerTypeId, salesOrder.getStaff(), salesOrder.getShop());
				}
			}
		}*/
		//TODO: begin vuongmq; 11/01/2015; them dong bang: promotion_map_delta; gia tri *(-1) neu update
		// sale_order_promotion; insert dong am, them dong cho tao update sale order
		if (lstSOP != null && !lstSOP.isEmpty()) {
			for (SaleOrderPromotion vo : lstSOP) {
				saleOrderDAO.createPromotionMapDelta(salesOrder, vo, ActionSaleOrder.UPDATE, true);
			}
			//Xoa saleorder_promotion; 
			for (SaleOrderPromotion sop : lstSOP) {
				commonDAO.deleteEntity(sop);
			}
		}
		/** Ghi log qua trinh chenh lech kho */
		try {
			logInfo.setIdObject(salesOrder.getId().toString());
			logInfo.setActionType(LogInfoVO.ACTION_UPDATE);
			logInfo.setFunctionCode(LogInfoVO.FC_SALE_DELETE_ORDER_DETAIL);
			LogUtility.logInfoWs(logInfo);
		} catch (Exception e) {}
		
		/** end log */
	}
	
	/**
	 * Xoa khuyen mai tich luy
	 * 
	 * @author lacnv1
	 * @since Dec 05, 2015
	 */
	private void deleteAccumulative(long saleOrderId) throws BusinessException {
		try {
			SaleOrderFilter<RptCTTLPay> filter = new SaleOrderFilter<RptCTTLPay>();
			filter.setSaleOrderId(saleOrderId);
			List<RptCTTLPay> lst = promotionProgramDAO.getListRptCTTLPay(filter);
			if (lst == null || lst.size() == 0) {
				return;
			}
			
			RptCTTLPay pay = null;
			List<RptCTTLPayDetail> lstDetail = null;
			for (int i = 0, sz = lst.size(); i < sz; i++) {
				pay = lst.get(i);
				lstDetail = promotionProgramDAO.getRptCTTLPayDetailByRptPayId(pay.getId());
				if (lstDetail != null && lstDetail.size() > 0) {
					for (int j = 0, szj = lstDetail.size(); j < szj; j++) {
						commonDAO.deleteEntity(lstDetail.get(j));
					}
				}
				commonDAO.deleteEntity(pay);
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public StockTotal getStockTotalByProductAndOwnerForUpdate(Long productId, StockObjectType ownerType, Long ownerId, Long warehouseId) throws BusinessException {
		try {
			return stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(productId, ownerType, ownerId, warehouseId);
		} catch (DataAccessException e) {
			BusinessException ex = new BusinessException(e);
			if (!StringUtility.isNullOrEmpty(e.getMessage())) {
				if (e.getMessage() == "GENERATE_SALE_ORDER_NUMBER_FAIL") {
					List<String> errCodes = new ArrayList<String>();
					errCodes.add("sale.order.save.generate.order.number.fail");
					ex.setErrorCodes(errCodes);
				}
			}
			throw ex;
		}
	}
	
	public void createAccumutive(SaleOrder so, String user, List<GroupLevelDetailVO> lstCTTL) throws BusinessException{
		try{
			LogInfoVO logInfo = new LogInfoVO();
			logInfo.setStaffCode(user);
			Date dateNow = commonDAO.getSysDate();
			ObjectVO<SaleOrderDetail> objSOD = this.getListSaleOrderDetailBySaleOrderId(null, so.getId());
			List<SaleOrderDetail> lstSOD = new ArrayList<SaleOrderDetail>();
			if(objSOD!=null && objSOD.getLstObject()!=null){
				for(SaleOrderDetail sod : objSOD.getLstObject()){
					if(sod.getIsFreeItem()==0){
						lstSOD.add(sod);
					}
				}
			}
			BigDecimal totalDiscount = BigDecimal.ZERO;
			BigDecimal totalWeight = BigDecimal.ZERO;
			Long totalDetail = so.getTotalDetail();
			Map<Long, Object> mapRptDetail = new HashMap<Long, Object>();
			for(GroupLevelDetailVO temp : lstCTTL){
				//lay danh sach tich luy
				PromotionProgram pp = promotionProgramMgr.getPromotionProgramByCode(temp.getProgramCode());
				RptAccumulativePromotionProgramVO accumulativePPVO = promotionProgramMgr.getRptAccumulativePPForCustomer(so.getShop().getId(), so.getCustomer().getId(), pp.getId(), so.getOrderDate());
				RptAccumulativePromotionProgram rpp = accumulativePPVO.getRptAccumulativePP();
				if (accumulativePPVO != null
					&& accumulativePPVO.getRptAccumulativePPDetail() != null
					&& accumulativePPVO.getRptAccumulativePPDetail().size() > 0) {
					List<RptAccumulativePromotionProgramDetail> rptAccumulativePPDetails = accumulativePPVO.getRptAccumulativePPDetailFinal();
					for (RptAccumulativePromotionProgramDetail rptDetail : rptAccumulativePPDetails) {
						if(mapRptDetail.get(rptDetail.getProduct().getId()) == null){
							mapRptDetail.put(rptDetail.getProduct().getId(), rptDetail);
						}
					}
				}
				//SALE_ORDER_DETAIL
				if(temp.getProductId()!=null){
					Map<Long, SaleOrderDetail> mapProductId = new HashMap<Long, SaleOrderDetail>();
					for(GroupLevelDetailVO vo: temp.getPromoProducts()){
						if(vo.getProductId()!=null){
							SaleOrderDetail sod = mapProductId.get(vo.getProductId());
							if(sod == null){
								sod = new SaleOrderDetail();
								sod.setSaleOrder(so);
								sod.setOrderDate(so.getOrderDate());
								sod.setShop(so.getShop());
								sod.setStaff(so.getStaff());
								sod.setQuantity(vo.getValueActually()!=null ? vo.getValueActually().intValue() : vo.getValue().intValue());
								Product product = productMgr.getProductById(vo.getProductId());
								if(product!=null){
									sod.setProduct(product);
									sod.setTotalWeight(product.getGrossWeight().multiply(new BigDecimal(sod.getQuantity())));
									totalWeight = totalWeight.add(sod.getTotalWeight());
								}
								sod.setIsFreeItem(1);
								sod.setGroupLevelId(vo.getGroupLevelId());
								sod.setProductGroupId(vo.getProductGroupId());
//								sod.setJoinProgramCode(vo.getProgramCode());
								sod.setProgramCode(vo.getProgramCode());
								sod.setProgrameTypeCode("ZV23");
								sod.setProgramType(ProgramType.AUTO_PROM);
								if (vo.getMaxQuantityFree() == null) {
									sod.setMaxQuantityFree(vo.getMaxQuantity());
								} else {
									sod.setMaxQuantityFree(vo.getMaxQuantityFree());
								}
								sod.setPrice(productMgr.getProductPriceForCustomer(product.getId(), so.getCustomer()));
								sod.setPriceValue(sod.getPrice().getPrice());
								sod.setPriceNotVat(sod.getPrice().getPriceNotVat());
								sod.setVat(sod.getPrice().getVat());
								sod.setAmount(sod.getPrice().getPrice().multiply(new BigDecimal(sod.getQuantity())));
								sod.setPayingOrder(vo.getPayingOrder());
								sod.setCreateDate(dateNow);
								sod.setCreateUser(user);
								sod = this.createSaleOrderDetail(sod);
								totalDetail++;
								mapProductId.put(vo.getProductId(), sod);
							}
							
							
							StockTotal st = this.getStockTotalByProductAndOwnerForUpdate(sod.getProduct().getId(),StockObjectType.SHOP,so.getShop().getId(),vo.getWarehouseId());
							st.setAvailableQuantity(st.getAvailableQuantity() - sod.getQuantity());
							if(st.getAvailableQuantity() < 0){
								throw new BusinessException("sp.search.sale.order.cttl.stock.not.enought_"+sod.getProduct().getProductCode());
							}
							commonDAO.updateEntity(st);
							
							SaleOrderLot saleOrderLot = new SaleOrderLot();
							saleOrderLot.setProduct(sod.getProduct());
							saleOrderLot.setWarehouse(stockManagerMgr.getWarehouseById(vo.getWarehouseId()));
							saleOrderLot.setShop(so.getShop());
							saleOrderLot.setStaff(so.getStaff());
							saleOrderLot.setSaleOrder(so);
							saleOrderLot.setSaleOrderDetail(sod);
							saleOrderLot.setPriceValue(sod.getPriceValue());
							saleOrderLot.setQuantity(sod.getQuantity());
							saleOrderLot.setOrderDate(so.getOrderDate());// Kiem tra currentShop da chot ngay hay chua, neu co thi luu ngay don hang la ngay chot
							saleOrderLot.setCreateDate(dateNow);
							saleOrderLot.setCreateUser(user);
							this.createSaleOrderLot(saleOrderLot);
						}
					}
				}else{// KM tien & %
					totalDiscount = totalDiscount.add(temp.getValue());
					//BigDecimal amount = so.getAmount();
					for(int i=0 ; i < lstSOD.size() ; i++){
						SaleOrderDetail sod = lstSOD.get(i);
//							BigDecimal sodDiscount = sod.getAmount().multiply(temp.getValue());
//							sodDiscount = sodDiscount.divide(amount, 0, RoundingMode.HALF_UP);
						//sod.setDiscountAmount(sod.getDiscountAmount().add(sodDiscount));
						BigDecimal sodDiscount = BigDecimal.ZERO;
						for(GroupLevelDetailVO vo: temp.getShareDiscount()){
							if(sod.getProduct().getProductCode().equals(vo.getProductCode())){
								sodDiscount = vo.getValue();
							}
						}
						SaleOrderPromoDetail sop = new SaleOrderPromoDetail();
						sop.setSaleOrderDetail(sod);
						sop.setMaxAmountFree(temp.getValue());
						sop.setDiscountAmount(sodDiscount);
						sop.setGroupLevelId(temp.getGroupLevelId());
						sop.setProductGroupId(temp.getProductGroupId());
						sop.setOrderDate(so.getOrderDate());
						sop.setIsOwner(2);
						sop.setSaleOrder(so);
						sop.setShop(so.getShop());
						sop.setStaff(so.getStaff());
						if(rpp!=null && rpp.getTotalAmount()!=null && !BigDecimal.ZERO.equals(rpp.getTotalAmount())){
							sop.setDiscountPercent(temp.getValue().floatValue()*100/rpp.getTotalAmount().floatValue());
						}
						if(temp.getPercent()!=null){
							sop.setDiscountPercent(temp.getPercent());
						}
						sop.setProgramCode(temp.getProgramCode());
						sop.setProgrameTypeCode("ZV23");
						sop.setProgramType(ProgramType.AUTO_PROM);
						commonDAO.createEntity(sop);
						//saleOrderMgr.updateSaleOrderDetail(sod);
					}
				}
				
				//------------------------------------------------------------------------------------------------
				//RPT_CTTL_PAY & RPT_CTTL_PAY_DETAIL
				RptCTTLPay pay = new RptCTTLPay();
				if(pp != null){
					pay.setPromotionProgram(pp);
					pay.setPromotionFromDate(pp.getFromDate());
					pay.setPromotionToDate(pp.getToDate());
				}
				pay.setCustomer(so.getCustomer());
				pay.setShop(so.getShop());
				pay.setStaff(so.getStaff());
				pay.setPromotion(1);
				if(temp.getPromotion()!=null){
					pay.setPromotion(temp.getPromotion());
				}
				pay.setApproved(so.getApproved().getValue());
				pay.setSaleOrder(so);
				BigDecimal totalAmountPromotion = BigDecimal.ZERO;
				BigDecimal totalQuantityPromotion = BigDecimal.ZERO;
				pay.setTotalAmountPayPromotion(totalAmountPromotion);
				pay.setTotalQuantityPayPromotion(totalQuantityPromotion);
				pay.setCreateDate(so.getOrderDate());
				
				pay = promotionProgramMgr.createRptCTTLPay(pay, logInfo);
				
				for(GroupLevelDetailVO vo : temp.getBuyProducts()){
					RptCTTLPayDetail rptDetail = new RptCTTLPayDetail();
					rptDetail.setRptCTTLPay(pay);
					rptDetail.setShop(so.getShop());
					rptDetail.setStaff(so.getStaff());
					if(vo.getProductId()!=null){
						Product product = productMgr.getProductById(vo.getProductId());
						rptDetail.setProduct(product);
					}
					if(ValueType.QUANTITY.getValue().equals(vo.getValueType())){
						rptDetail.setQuantityPromotion(vo.getValue().setScale(2, RoundingMode.DOWN));
						rptDetail.setAmountPromotion(this.getAmountOrQuantityProductAccumulative(mapRptDetail,vo.getProductId(),vo.getValue(),ValueType.AMOUNT));
					}else{
						rptDetail.setAmountPromotion(vo.getValue());
						rptDetail.setQuantityPromotion(this.getAmountOrQuantityProductAccumulative(mapRptDetail,vo.getProductId(),vo.getValue(),ValueType.QUANTITY));
					}
					totalQuantityPromotion = totalQuantityPromotion.add(rptDetail.getQuantityPromotion());
					totalAmountPromotion = totalAmountPromotion.add(rptDetail.getAmountPromotion());
					rptDetail.setCreateDate(so.getOrderDate());
					promotionProgramMgr.createRptCTTLPayDetail(rptDetail, logInfo);
				}
				if(!BigDecimal.ZERO.equals(totalAmountPromotion)){
					pay.setTotalAmountPayPromotion(totalAmountPromotion);
					pay.setTotalQuantityPayPromotion(totalQuantityPromotion);
					commonDAO.updateEntity(pay);
				}
			}
			so.setTotalWeight(so.getTotalWeight().add(totalWeight));
			so.setTotalDetail(totalDetail);
			this.updateSaleOrder(so);
			this.updateListSaleOrderDetail(lstSOD);
		}catch(Exception e){
			BusinessException ex = new BusinessException(e);
			if (!StringUtility.isNullOrEmpty(e.getMessage())) {
				List<String> errCodes = new ArrayList<String>();
				errCodes.add(e.getMessage());
				ex.setErrorCodes(errCodes);
			}
			throw ex; 
		}
	}
	
	/*
	 * convert Amount va quantity
	 * valueType 1: get quantity,  2: get amount
	 * tungmt
	 * 24/12/2014
	 */
	public BigDecimal getAmountOrQuantityProductAccumulative(Map<Long, Object> mapRptDetail, Long productId, BigDecimal value, ValueType valueType){
		BigDecimal result = BigDecimal.ZERO;
		RptAccumulativePromotionProgramDetail temp =  (RptAccumulativePromotionProgramDetail)mapRptDetail.get(productId);
		if(temp!=null){
			if(ValueType.QUANTITY.equals(valueType) && !BigDecimal.ZERO.equals(temp.getAmount())){//quantity
				result = value.multiply(temp.getQuantity()).divide(temp.getAmount(), 2, RoundingMode.DOWN);
			}else if(!BigDecimal.ZERO.equals(temp.getQuantity())){//amount
				result = value.multiply(temp.getAmount()).divide(temp.getQuantity(), 0, RoundingMode.DOWN);
			}
		}
		return result;
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public SaleOrder createSaleOrder(SaleOrder salesOrder, List<SaleProductVO> lstSaleProductVO, 
			List<SaleProductVO> lstPromoProductVO, List<SaleOrderPromotionVO> lstOrderPromotion,
			List<SaleOrderPromotionDetailVO> lstOrderPromoDetail, List<GroupLevelDetailVO> lstCTTL, Integer isSaveNotCheckDebit, 
			List<KeyShopVO> lstKeyShop, List<SaleOrderPromoLotVO> lstPromoLot, LogInfoVO logInfoVO)
			throws BusinessException {
		SaleOrder order = null;
		String user = "";
		if (salesOrder.getId() != null) {
			order = this.modifySaleOrder(salesOrder, lstSaleProductVO, lstPromoProductVO, null, lstOrderPromotion, 
					lstOrderPromoDetail, isSaveNotCheckDebit, lstKeyShop,logInfoVO);
			user = order.getUpdateUser();
		}else{
			order = this.createOneSaleOrder(salesOrder, lstSaleProductVO, lstPromoProductVO, false, null, lstOrderPromotion, lstOrderPromoDetail,isSaveNotCheckDebit, lstKeyShop,logInfoVO);
			user = order.getCreateUser();
		}
//		if(lstCTTL != null){
//			this.createAccumutive(order, user, lstCTTL);
//		}
		if (lstKeyShop != null) {
			this.createKeyShop(order, user, lstKeyShop);// chi luu voi tra thuong keyshop san pham
		}
		if (lstPromoLot != null) {
			this.createPromoLot(order, user, lstPromoLot);
		}
		return order;
	}
	
	public void createPromoLot(SaleOrder so, String user, List<SaleOrderPromoLotVO> lstPromoLotVO) throws BusinessException{
		try{
			//Luu promolot doi voi tat ca KM
			List<SaleOrderLot> lstLot = saleOrderDAO.getListSaleOrderLotBySaleOrderId(so.getId());
			List<SaleOrderPromoLot> lstPromoLotForKS = new ArrayList<SaleOrderPromoLot>();//dung de luu promoDetail cho keyshop
			for (SaleOrderPromoLotVO vo : lstPromoLotVO) {
				SaleOrderPromoLot promoLot = new SaleOrderPromoLot();
				//tim saleorderlot tuong ung
				for (SaleOrderLot lot : lstLot) {
					if (lot.getSaleOrderDetail() != null && lot.getSaleOrderDetail().getIsFreeItem().equals(0)
						&& lot.getWarehouse().getId().equals(vo.getWarehouseId()) && lot.getProduct().getId().equals(vo.getProductId())) {
						promoLot.setSaleOrderLotId(lot.getId());
						promoLot.setSaleOrderDetail(lot.getSaleOrderDetail());
						break;
					}
				}
				promoLot.setSaleOrder(so);
				promoLot.setStaff(so.getStaff());
				promoLot.setShop(so.getShop());
				promoLot.setOrderDate(so.getOrderDate());
				promoLot.setDiscountAmount(vo.getDiscount());
				if (Boolean.TRUE.equals(vo.getIsKS())) {
					promoLot.setIsOwner(2);//keyshop
					promoLot.setProgramCode(vo.getProgramCode());
					promoLot.setProgrameTypeCode(KS);
					promoLot.setProgramType(ProgramType.KEY_SHOP);
				} else {
					PromotionProgram pp = promotionProgramDAO.getPromotionProgramByCode(vo.getProgramCode());
					if (pp != null) {
						promoLot.setIsOwner(this.getIsOwner(PromotionType.parseValue(pp.getType())));
						promoLot.setProgrameTypeCode(pp.getType());
						promoLot.setProgramCode(pp.getPromotionProgramCode());
						promoLot.setProgramType(ProgramType.AUTO_PROM);
					}
				}
				promoLot = commonDAO.createEntity(promoLot);
				lstPromoLotForKS.add(promoLot);
			}
			
			//Luu promoDetail voi keyshop (vi km khac da luu het roi)
			Map<Integer, Integer> mapTemp = new HashMap<Integer, Integer>();//luu cac promolot da xet
			for (int i = 0, n = lstPromoLotForKS.size(); i < n ; i++) {
				SaleOrderPromoLot sopl = lstPromoLotForKS.get(i);
				//chi xet cac dong cua keyshop
				if (ProgramType.KEY_SHOP.equals(sopl.getProgramType()) && mapTemp.get(i) == null) {
					BigDecimal discount = lstPromoLotForKS.get(i).getDiscountAmount();
					//Tinh tong tat cac cac lot theo saleOrderDetail
					for (int j = i + 1 ; j < n ; j++) {
						if (mapTemp.get(j) == null) { 
							SaleOrderPromoLot temp = lstPromoLotForKS.get(j);
							if (ProgramType.KEY_SHOP.equals(temp.getProgramType()) && sopl.getProgramCode().equals(temp.getProgramCode())
									&& sopl.getSaleOrderDetail().getId().equals(temp.getSaleOrderDetail().getId())) {
								discount = discount.add(temp.getDiscountAmount());
								mapTemp.put(j, j);
							}
						}
					}
					SaleOrderPromoDetail sopd = new SaleOrderPromoDetail();
					sopd.setDiscountAmount(discount);
					sopd.setIsOwner(sopl.getIsOwner());
					sopd.setProgramCode(sopl.getProgramCode());
					sopd.setProgrameTypeCode(sopl.getProgrameTypeCode());
					sopd.setProgramType(sopl.getProgramType());
					sopd.setOrderDate(sopl.getOrderDate());
					sopd.setSaleOrder(so);
					sopd.setSaleOrderDetail(sopl.getSaleOrderDetail());
					sopd.setShop(so.getShop());
					sopd.setStaff(so.getStaff());
					//set maxDiscount - Duyet qua lstPromoLotVO neu co dong nao cua keyshop va programCode tuong ung thi set maxAmount
					for (SaleOrderPromoLotVO vo : lstPromoLotVO) {
						if (Boolean.TRUE.equals(vo.getIsKS()) && vo.getProgramCode().equals(sopd.getProgramCode())) {
							sopd.setMaxAmountFree(vo.getMaxDiscount());
							break;
						}
					}
					commonDAO.createEntity(sopd);
				}
			}
		}catch(Exception e){
			BusinessException ex = new BusinessException(e);
			if (!StringUtility.isNullOrEmpty(e.getMessage())) {
				List<String> errCodes = new ArrayList<String>();
				errCodes.add(e.getMessage());
				ex.setErrorCodes(errCodes);
			}
			throw ex; 
		}
	}
	
	public void createKeyShop(SaleOrder so, String user, List<KeyShopVO> lstKeyShop) throws BusinessException{
		try{
			LogInfoVO logInfo = new LogInfoVO();
			logInfo.setStaffCode(user);
			Date dateNow = commonDAO.getSysDate();
			List<SaleOrderDetail> lstSOD = new ArrayList<SaleOrderDetail>();
			Map<String, SaleOrderDetail> mapSOD = new HashMap<String, SaleOrderDetail>();
			Boolean isCreateSOD = true;
			SaleOrderFilter<Price> filter = new SaleOrderFilter<Price>();
			BigDecimal totalWeight = BigDecimal.ZERO;
			int totalDetail = 0;
			//danh sach keyshop bao gom cac dong cung sp, cung ks nhung khac kho
			//Nen khi luu thi chi luu 1 dong saleOrderDetail, cac kho luu trong saleOrderLot
			for (KeyShopVO vo : lstKeyShop) {
				if (vo.getProductId() == null) {
					continue;
				}
				isCreateSOD = true;
				SaleOrderDetail sod = mapSOD.get(vo.getKsId() + "-" + vo.getProductId());
				if (sod == null) {
					sod = new SaleOrderDetail();
				} else {
					isCreateSOD = false;
				}
				//neu chua tao sod thi tao moi
				if (isCreateSOD) {
					sod.setSaleOrder(so);
					sod.setCreateDate(dateNow);
					sod.setCreateUser(user);
					sod.setIsFreeItem(1);
					sod.setOrderDate(so.getOrderDate());
					sod.setProgramCode(vo.getKsCode());
					sod.setProgramType(ProgramType.KEY_SHOP);
					sod.setProgrameTypeCode(KS);
					sod.setShop(so.getShop());
					sod.setStaff(so.getStaff());
					
					Product p = productDAO.getProductById(vo.getProductId());
					if (p == null) {
						throw new BusinessException("product is not exists");
					}
					sod.setProduct(p);
					sod.setConvfact(p.getConvfact());
					sod.setProductInfo(p.getCat());
					//tinh tong quantity nhan, tong weight
					Integer quantityAmount = 0;
					for (KeyShopVO tmp : lstKeyShop) {
						if (vo.getProductId().equals(tmp.getProductId()) && vo.getKsId().equals(tmp.getKsId())) {
							quantityAmount += tmp.getQuantityAmount().intValue();
						}
					}
					sod.setMaxQuantityFree(vo.getTotal().intValue() - vo.getTotalDone().intValue());
					sod.setQuantity(quantityAmount);
					sod.setQuantityPackage(quantityAmount / p.getConvfact());
					sod.setQuantityRetail(quantityAmount % p.getConvfact());
					sod.setTotalWeight(p.getGrossWeight().multiply(new BigDecimal(quantityAmount)));
					totalWeight = totalWeight.add(sod.getTotalWeight());
					
					//lay gia
					filter = new SaleOrderFilter<Price>();
					filter.setProductId(p.getId());
					filter.setShopId(so.getShop().getId());
					filter.setCustomerId(so.getCustomer().getId());
					filter.setOrderDate(dateNow);
					List<Price> lstPr = priceDAO.getPriceByRpt(filter);
					if (lstPr != null && lstPr.size() > 0) {
						sod.setPrice(lstPr.get(0));
						sod.setPriceValue(lstPr.get(0).getPrice());
						sod.setPriceNotVat(lstPr.get(0).getPriceNotVat());
						sod.setPackagePrice(lstPr.get(0).getPackagePrice());
						sod.setPackagePriceNotVAT(lstPr.get(0).getPackagePriceNotVat());
						sod.setVat(lstPr.get(0).getVat());
					}
						
					sod = commonDAO.createEntity(sod);
					totalDetail++;
					
					//luu map cho biet da co saleOrderDetail cua sp ung' voi ks
					mapSOD.put(vo.getKsId() + "-" + vo.getProductId(), sod);
					
					//luu saleOrderLot
					if (vo.getProductId() != null) {
						for (KeyShopVO tmp : lstKeyShop) {
							if (vo.getProductId().equals(tmp.getProductId()) && vo.getKsId().equals(tmp.getKsId())) {
								SaleOrderLot sol = new SaleOrderLot();
								sol.setSaleOrder(so);
								sol.setSaleOrderDetail(sod);
								sol.setShop(so.getShop());
								sol.setStaff(so.getStaff());
								sol.setCreateDate(dateNow);
								sol.setCreateUser(user);
								sol.setOrderDate(so.getOrderDate());
								sol.setPackagePrice(sod.getPackagePrice());
								sol.setPriceValue(sod.getPriceValue());
								sol.setPrice(sod.getPrice());
								sol.setProduct(sod.getProduct());
								
								quantityAmount = tmp.getQuantityAmount().intValue();
								sol.setQuantity(quantityAmount);
								sol.setQuantityPackage(quantityAmount / sol.getProduct().getConvfact());
								sol.setQuantityRetail(quantityAmount % sol.getProduct().getConvfact());
								
								//lay stocktotal
								StockTotal st = stockTotalDAO.getStockTotalByProductAndOwner(sol.getProduct().getId(), StockObjectType.SHOP, so.getShop().getId(), tmp.getWarehouseId());
								if (st != null) {
									sol.setStockTotal(st);
									sol.setWarehouse(st.getWarehouse());
									st.setAvailableQuantity(st.getAvailableQuantity() - sol.getQuantity());
									commonDAO.updateEntity(st);
								}
								commonDAO.createEntity(sol);
							}
						}
					}
				}
			}
			//update totalweight
			so.setTotalDetail(so.getTotalDetail() + totalDetail);
			so.setTotalWeight(so.getTotalWeight().add(totalWeight));
			this.updateSaleOrder(so);
		}catch(Exception e){
			BusinessException ex = new BusinessException(e);
			if (!StringUtility.isNullOrEmpty(e.getMessage())) {
				List<String> errCodes = new ArrayList<String>();
				errCodes.add(e.getMessage());
				ex.setErrorCodes(errCodes);
			}
			throw ex; 
		}
	}
	
	public Integer getIsOwner(PromotionType type) {
		if (PromotionType.ZV01.equals(type) || PromotionType.ZV02.equals(type) || PromotionType.ZV03.equals(type) 
				|| PromotionType.ZV04.equals(type) || PromotionType.ZV05.equals(type) || PromotionType.ZV06.equals(type)) {
			return 1;//line
		}
		if (PromotionType.ZV19.equals(type) || PromotionType.ZV20.equals(type) || PromotionType.ZV21.equals(type)) {
			return 2;
		}
		return 0;//bundle
	}

	/**
	 * Tao don hang
	 * 
	 * @author nhanlt6
	 * @param salesOrder
	 * @param lstSaleProductVO
	 * @param lstPromoProductVO
	 * @param isUpdate
	 * @param isCheckQuantity
	 * @return SaleOrder
	 * @throws BusinessException
	 */
	private SaleOrder createOneSaleOrder(SaleOrder salesOrder, List<SaleProductVO> lstSaleProductVO, List<SaleProductVO> lstPromoProductVO, Boolean isUpdate, Boolean isCheckQuantity, List<SaleOrderPromotionVO> lstOrderPromotion,
			List<SaleOrderPromotionDetailVO> lstOrderPromoDetail, Integer isSaveNotCheck, List<KeyShopVO> lstKeyShopVO,LogInfoVO logInfoVO) throws BusinessException {
		try {
						
			final Map<String, Integer> PROMOTION_TYPE_MAP = new HashMap<String, Integer>() {
				{
					put("ZM", 1);
					put("DISPLAY_SCORE", 2);
					put("ZD", 3);
					put("ZH", 4);
					put("ZT", 5);
				};
			};

			LogInfoVO logInfo = new LogInfoVO();
			logInfo.setStaffCode(salesOrder.getCreateUser());

			SortedMap<Long, Integer> sortedProductQuantity = new TreeMap<Long, Integer>();
			if (lstSaleProductVO == null)
				lstSaleProductVO = new ArrayList<SaleProductVO>();

			if (lstPromoProductVO == null)
				lstPromoProductVO = new ArrayList<SaleProductVO>();

			List<SaleOrderDetail> allSod = new ArrayList<SaleOrderDetail>();

			//Kiem tra muc han no cua Khach hang
			if (isSaveNotCheck==null && !BigDecimal.ZERO.equals(salesOrder.getAmount())) {
				this.checkDebitInSaleOrder(salesOrder.getShop().getId(), salesOrder.getCustomer().getId(), salesOrder.getAmount());
			}
			
			/*
			 * san pham ban + san pham KM dung de check san pham co gia hay ko
			 */
			List<String> productsToCheckPrice = new ArrayList<String>();

			/*
			 * Map<product_id + "_" + warehouse_id, available_quantity> luu tam
			 * thong tin ton kho san pham & kho trong qua trinh xu ly. Muc dich:
			 * kiem tra tong so luong cua san pham co vuot qua ton kho hay
			 * khong.
			 */
			
			
			Map<String, Integer> remainAvailableQuantityOfProduct = new HashMap<String, Integer>();
			// kiem tra quantity co du trong kho khong
			for (SaleProductVO vo : lstSaleProductVO) {
				// Luu program_code
				SaleOrderDetail detail = vo.getSaleOrderDetail();
				allSod.add(detail);
				String promotionCode = detail.getProgramCode();
				if (vo.getLstSaleOrderLot() != null) {
					for (SaleOrderLot lot : vo.getLstSaleOrderLot()) {
						// kiem tra trong moi Kho co con so luong sp khong va thuc hien tru ton kho
						StockTotal st = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(lot.getProduct().getId(), StockObjectType.SHOP, salesOrder.getShop().getId(), lot.getWarehouse().getId());
						if (st == null) {
							//Khong co san pham trong kho thi tao moi
							st = new StockTotal();
							st.setStatus(ActiveType.RUNNING);
							st.setAvailableQuantity(0);
							st.setQuantity(0);
							st.setCreateDate(salesOrder.getOrderDate());
							st.setCreateUser(logInfo.getStaffCode());
							st.setObjectId(salesOrder.getShop().getId());
							st.setObjectType(StockObjectType.SHOP);
							st.setShop(salesOrder.getShop());
							st.setWarehouse(lot.getWarehouse());
							st.setProduct(lot.getProduct());
							st = stockTotalDAO.createStockTotal(st);
							//lot.setStockTotal(st);
						} else if (!ActiveType.RUNNING.equals(st.getStatus())) {
							st.setStatus(ActiveType.RUNNING);
							st.setAvailableQuantity(0);
							st.setQuantity(0);
						}
						String keyMap = lot.getProduct().getId() + "_" + lot.getWarehouse().getId();
						if (!remainAvailableQuantityOfProduct.containsKey(keyMap)) {
							remainAvailableQuantityOfProduct.put(keyMap, st.getAvailableQuantity());
						}
						// kiem tra so luong co vuot qua ton kho hay khong
						Integer currentAvailableQuantity = remainAvailableQuantityOfProduct.get(keyMap);
						if (currentAvailableQuantity - lot.getQuantity() < 0) {
							BusinessException e = new BusinessException("SaleOrderMgr.createOneSaleOrder: product " + lot.getProduct().getProductCode() + " is out of stock.");
							List<String> errorCodes = new ArrayList<String>();
							errorCodes.add("sale.order.save.product.quantity.exceed.total.available.quantity" + "_" + lot.getProduct().getProductCode());
							e.setErrorCodes(errorCodes);
							throw e;
						}
						st.setAvailableQuantity(st.getAvailableQuantity() - lot.getQuantity());
						remainAvailableQuantityOfProduct.put(keyMap, currentAvailableQuantity - lot.getQuantity());
												
						/**Log conent */
						try {		
							logInfoVO.getContenFormat(
									lot.getProduct().getId(), 
									lot.getWarehouse().getId(), 
									lot.getQuantity(), 
									currentAvailableQuantity, 
									currentAvailableQuantity - lot.getQuantity(), 
									st.getQuantity(), 
									st.getQuantity());							
						} catch (Exception e) {}
						/** End log conent */
						
					}
				}

				if (promotionCode != null && detail.getIsFreeItem() == 1) {
					CommercialSupportCodeVO csVO = new CommercialSupportCodeVO();
					csVO.setCode(promotionCode);
					csVO.setCustomerId(salesOrder.getCustomer().getId());
					if (detail.getProduct() != null) {
						csVO.setProductId(detail.getProduct().getId());
					}
					csVO.setShopId(salesOrder.getShop().getId());
					if (ProgramType.DISPLAY_SCORE.equals(detail.getProgramType())) {
						csVO.setType(CommercialSupportType.DISPLAY_PROGRAM);
					} else {
						csVO.setType(CommercialSupportType.PROMOTION_PROGRAM);
					}
					List<CommercialSupportCodeVO> lstCommercialSupportCodeVO = new ArrayList<CommercialSupportCodeVO>();
					lstCommercialSupportCodeVO.add(csVO);
					//					Boolean rs = this.checkIfCommercialSupportExists(lstCommercialSupportCodeVO,salesOrder.getOrderDate(), null).get(0);
					//
					//					if (!rs)
					//						// chuong trinh khuyen mai khong ton tai
					//						throw new BusinessException("Promotion program does not exists");

					/*
					 * set program type: ZD, ZH, ZT, ZM
					 */
					PromotionProgram promotionProgram = promotionProgramDAO.getPromotionProgramByCode(promotionCode.trim());
					if (promotionProgram != null) {
						Integer programType = PROMOTION_TYPE_MAP.get(promotionProgram.getType());
						if (programType != null) {
							detail.setProgramType(ProgramType.parseValue(programType));
						}
						SaleOrderPromotionVO promotionVO = new SaleOrderPromotionVO(); // luu sale_order_promotion cho khuyen mai tay/huy/doi/tra
						promotionVO.setPromotionId(promotionProgram.getId());
						promotionVO.setPromotionCode(promotionProgram.getPromotionProgramCode());
						promotionVO.setQuantityReceived(1);
						promotionVO.setMaxQuantityReceived(1);
						promotionVO.setIsNotReceiveQuantity(false);
						promotionVO.setProduct(detail.getProduct());
						if (lstOrderPromotion == null) {
							lstOrderPromotion = new ArrayList<SaleOrderPromotionVO>();
						}
						lstOrderPromotion.add(promotionVO);
					} else {
						//Luu ma CTTB
						detail.setProgramType(ProgramType.parseValue(2));
						detail.setProgrameTypeCode("TB");
					}

				}
				if (detail.getProduct() != null) {
					Long key = detail.getProduct().getId();
					if (sortedProductQuantity.get(key) != null)
						sortedProductQuantity.put(key, sortedProductQuantity.get(key) + detail.getQuantity());
					else
						sortedProductQuantity.put(detail.getProduct().getId(), detail.getQuantity());
					
					Boolean isCheckProductPrice = detail.getIsFreeItem() == 0;
					isCheckProductPrice = isCheckProductPrice ||
										!(detail.getIsFreeItem() == 1 
										&& (detail.getProgramType() == ProgramType.DISPLAY_SCORE
											|| detail.getProgramType() == ProgramType.MANUAL_PROM
											|| detail.getProgramType() == ProgramType.DESTROY
											|| detail.getProgramType() == ProgramType.PRODUCT_EXCHANGE
											|| detail.getProgramType() == ProgramType.RETURN
										));
					if (isCheckProductPrice) {
						productsToCheckPrice.add(detail.getProduct().getProductCode());
					}
				}
			}

			for (SaleProductVO vo : lstPromoProductVO) {
				SaleOrderDetail promoDetail = vo.getSaleOrderDetail();
				allSod.add(promoDetail);

				if (vo.getLstSaleOrderLot() != null) {
					for (SaleOrderLot lot : vo.getLstSaleOrderLot()) {
						// kiem tra trong moi Kho co con so luong sp khong va thuc hien tru ton kho
						StockTotal st = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(lot.getProduct().getId(), StockObjectType.SHOP, salesOrder.getShop().getId(), lot.getWarehouse().getId());
						if (st == null) {
							//Khong co san pham trong kho thi tao moi
							st = new StockTotal();
							st.setStatus(ActiveType.RUNNING);
							st.setAvailableQuantity(0);
							st.setQuantity(0);
							st.setCreateDate(salesOrder.getOrderDate());
							st.setCreateUser(logInfo.getStaffCode());
							st.setObjectId(salesOrder.getShop().getId());
							st.setObjectType(StockObjectType.SHOP);
							st.setShop(salesOrder.getShop());
							st.setWarehouse(lot.getWarehouse());
							st.setProduct(lot.getProduct());
							st = stockTotalDAO.createStockTotal(st);
							//lot.setStockTotal(st);
						} else if (!ActiveType.RUNNING.equals(st.getStatus())) {
							st.setStatus(ActiveType.RUNNING);
							st.setAvailableQuantity(0);
							st.setQuantity(0);
						}
						String keyMap = lot.getProduct().getId() + "_" + lot.getWarehouse().getId();
						if (!remainAvailableQuantityOfProduct.containsKey(keyMap)) {
							remainAvailableQuantityOfProduct.put(keyMap, st.getAvailableQuantity());
						}
						// kiem tra so luong co vuot qua ton kho hay khong
						Integer currentAvailableQuantity = remainAvailableQuantityOfProduct.get(keyMap);
						/*
						 * san pham km khong du kho van chap nhan tao don
						if (currentAvailableQuantity - lot.getQuantity() < 0) {
							BusinessException e = new BusinessException("SaleOrderMgr.createOneSaleOrder: product " + lot.getProduct().getProductCode() + " is out of stock.");
							List<String> errorCodes = new ArrayList<String>();
							errorCodes.add("sale.order.save.product.quantity.exceed.total.available.quantity" + "_" + lot.getProduct().getProductCode());
							e.setErrorCodes(errorCodes);
							throw e;
						}
						*/
						st.setAvailableQuantity(st.getAvailableQuantity() - lot.getQuantity());
						remainAvailableQuantityOfProduct.put(keyMap, currentAvailableQuantity - lot.getQuantity());
						
						/**Log conent */
						try {		
							logInfoVO.getContenFormat(
									lot.getProduct().getId(), 
									lot.getWarehouse().getId(), 
									lot.getQuantity(), 
									currentAvailableQuantity, 
									currentAvailableQuantity - lot.getQuantity(), 
									st.getQuantity(), 
									st.getQuantity());							
						} catch (Exception e) {}
						/** End log conent */
					}
				}
				if (promoDetail.getQuantity() != 0 && promoDetail.getProduct() != null) {
					Long key = promoDetail.getProduct().getId();
					if (sortedProductQuantity.get(key) != null) {
						sortedProductQuantity.put(key, sortedProductQuantity.get(key) + promoDetail.getQuantity());
					} else {
						sortedProductQuantity.put(promoDetail.getProduct().getId(), promoDetail.getQuantity());
					}
				}
				
//				if (promoDetail.getProduct() != null) {
//					productsToCheckPrice.add(promoDetail.getProduct().getProductCode());
//				}
			}
			//Luu gia tri tong so SKU cua don hang
			salesOrder.setTotalDetail(Long.valueOf(allSod.size()));
			
			//Kiem tra neu tong SL SPKM duoc huong = 0 thi so suat nhan dc cua ctr se la 0 
			Integer quantityReceive = 0;
			if (lstOrderPromotion != null && lstOrderPromotion.size() > 0) {
				for (SaleOrderPromotionVO vo : lstOrderPromotion) {
					quantityReceive += vo.getQuantityReceived();
					Integer totalQuantityInProgram = 0;
					Boolean isCheck = false;
					for (SaleProductVO voS : lstPromoProductVO) {
						if (voS.getSaleOrderDetail().getProgramCode().equals(vo.getPromotionCode()) 
								&& voS.getSaleOrderDetail().getPromotionOrderNumber().equals(vo.getPromotionLevel())) {
							isCheck = true;
							totalQuantityInProgram += voS.getSaleOrderDetail().getQuantity();
						}
					}
					if (totalQuantityInProgram == 0 && isCheck) {
						vo.setIsNotReceiveQuantity(true);
						vo.setQuantityReceived(0);
					}
				}
			}
			
			// lacnv1 - khong tang so suat o day, chuyen sang buoc xac nhan IN
			// tang so suat nhan khuyen mai theo ma CTKM
			/*if (lstOrderPromotion != null && lstOrderPromotion.size() > 0) {
				Long customerTypeId = 0L;
				if (salesOrder.getCustomer().getChannelType() != null) {
					customerTypeId = salesOrder.getCustomer().getChannelType().getId();
				}
				for (SaleOrderPromotionVO vo : lstOrderPromotion) {
					PromotionProgram pg = promotionProgramDAO.getPromotionProgramById(vo.getPromotionId());
					if (pg != null) {
						String promotionCode = pg.getPromotionProgramCode();
						this.changePromotionQuantity(vo.getQuantityReceived(), logInfo, promotionCode, salesOrder.getCustomer().getShop().getShopCode(), 
									salesOrder.getCustomer().getShortCode(), customerTypeId, salesOrder.getStaff(), salesOrder.getShop());
					}
				}
			}*/
			
			// cap nhat bang routing_customer

			Date now = salesOrder.getOrderDate();
			if (now == null) {
				now = commonDAO.getSysDate();
			}
			/*
			 * check san pham co gia hay ko
			 */
			//trungtm6 tam thoi comment on 23/04/2015
			/*if (productsToCheckPrice != null && productsToCheckPrice.size() > 0) {
				List<Product> invalidPriceProducts = priceDAO.getInvalidPriceProducts(productsToCheckPrice, salesOrder.getCustomer().getId(), salesOrder.getCustomer().getShop().getId(), now);
				if (invalidPriceProducts != null && invalidPriceProducts.size() > 0) {
					BusinessException e = new BusinessException("create/update sale order: exists product has invalid price.");
					List<String> errCodes = new ArrayList<String>();
					String productCodes = "";
					for (Product p : invalidPriceProducts) {
						productCodes += ", " + p.getProductCode();
					}
					errCodes.add("sale.order.save.product.price.invalid" + "_" + productCodes.replaceFirst(", ", ""));
					e.setErrorCodes(errCodes);
					throw e;
				}
			}*/
			//Modified by NhanLT6 - 10/11/2014 - Bo cap nhat last order theo DTYC ban giao cua Mr.AnhNH51 cung ngay
//			if(!BigDecimal.ZERO.equals(salesOrder.getAmount()) && salesOrder.getAmount() != null) {
				StaffCustomer sc = staffCustomerDAO.getStaffCustomer(salesOrder.getStaff().getId(), salesOrder.getCustomer().getId());
				if (sc != null) {
					sc.setLastOrder(now);
					staffCustomerDAO.updateStaffCustomer(sc, null);
				} else {
					sc = new StaffCustomer();
					sc.setCustomer(salesOrder.getCustomer());
					sc.setStaff(salesOrder.getStaff());
					sc.setLastOrder(now);
					sc.setShop(salesOrder.getShop());
					staffCustomerDAO.createStaffCustomer(sc, logInfo);
				}
//				cap nhat bang customer
				Customer ct = salesOrder.getCustomer();
				ct.setLastOrder(now);
				customerDAO.updateCustomer(ct, null);
				
				/*
				 * Modified by NhanLT - 20/11/2014
				 * Load routing customer theo staff id truong hop KH thuoc nhieu tuyen
				 * */
				/*RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDate(salesOrder.getCustomer().getId(),salesOrder.getShop().getId(),
				salesOrder.getOrderDate());*/
				RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDateAndStaff(salesOrder.getCustomer().getId(),
						salesOrder.getShop().getId(), now,salesOrder.getStaff().getId());
				if (rc != null) {
					rc.setLastOrder(now);
					routingCustomerDAO.updateRoutingCustomer(rc);
					salesOrder.setRouting(rc.getRouting());//trungtm6 bo sung
				}
//			}
			
			return saleOrderDAO.createSaleOrder(salesOrder, lstSaleProductVO, lstPromoProductVO, isUpdate, lstOrderPromotion, lstOrderPromoDetail,logInfoVO);
		} catch (DataAccessException e) {
			BusinessException ex = new BusinessException(e);
			if (!StringUtility.isNullOrEmpty(e.getMessage())) {
				if (e.getMessage() == "GENERATE_SALE_ORDER_NUMBER_FAIL") {
					List<String> errCodes = new ArrayList<String>();
					errCodes.add("sale.order.save.generate.order.number.fail");
					ex.setErrorCodes(errCodes);
				}
			}
			throw ex;
		}
	}

	@Override
	public void checkDebitInSaleOrder(Long shopId, Long customerId, BigDecimal amount) throws BusinessException {
		try {
			Customer c = customerDAO.getCustomerById(customerId);
			// Kiem tra xem co shop co chot ngay hay ko
			Date lockDay = shopLockDAO.getNextLockedDay(shopId);
			if (lockDay == null) {
				lockDay = commonDAO.getSysDate();
			}
			if (c == null)
				throw new BusinessException("CustomerId is invalid");
			if (Integer.valueOf(1).equals(c.getApplyDebitLimited())) {
				if (!BigDecimal.ZERO.equals(c.getMaxDebitAmount())) {
					Debit d = debitDAO.getDebitForUpdate(customerId, DebitOwnerType.CUSTOMER);
					if (c.getMaxDebitAmount().intValue() != -1 && d.getTotalDebit().add(amount).compareTo(c.getMaxDebitAmount()) > 0)
						throw new BusinessException(ExceptionCode.TOTAL_DEBIT_LARGER_THAN_MAX_DEBIT);
				}
				Date longestDate = debitDAO.getLongestDebtSaleOrder(customerId, DebitOwnerType.CUSTOMER);
				if (longestDate != null) {
					Calendar lD = Calendar.getInstance();
					lD.setTime(longestDate);
					lD.set(Calendar.HOUR, 0);
					lD.set(Calendar.MINUTE, 0);
					lD.set(Calendar.SECOND, 0);
					lD.set(Calendar.MILLISECOND, 0);

					Calendar lN = Calendar.getInstance();
					//lN.setTime(now);
					//Neu co ngay chot thi tinh han no cua khach hang theo ngay chot
					lN.setTime(lockDay);
					lN.set(Calendar.HOUR, 0);
					lN.set(Calendar.MINUTE, 0);
					lN.set(Calendar.SECOND, 0);
					lN.set(Calendar.MILLISECOND, 0);
					if (c.getMaxDebitDate().intValue() != -1) {
						if ((lN.getTimeInMillis() - lD.getTimeInMillis()) / (1000 * 60 * 60 * 24) > c.getMaxDebitDate())
							throw new BusinessException(ExceptionCode.MAX_DAY_DEBIT);
					}
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updateSaleOrder(SaleOrder salesOrder) throws BusinessException {
		try {
			saleOrderDAO.updateSaleOrder(salesOrder);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteSaleOrderAIAD(SaleOrder salesOrder) throws BusinessException {
		try {
			this.deleteSaleOrderAIADDetail(salesOrder);
			saleOrderDAO.deleteSaleOrder(salesOrder);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	//@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteSaleOrderAIADDetail(SaleOrder salesOrder) throws BusinessException {
		try {
			List<SaleOrderDetail> lstDataDetail = new ArrayList<SaleOrderDetail>();
			List<Long> lstSaleOrderId = new ArrayList<Long>();
			lstSaleOrderId.add(salesOrder.getId());
			lstDataDetail = saleOrderDAO.getListSaleOrderDetailByListSaleOrderId(lstSaleOrderId, IsFreeItemInSaleOderDetail.SALE.getValue(), false);
			if (lstDataDetail != null && !lstDataDetail.isEmpty()) {
				for (SaleOrderDetail sod : lstDataDetail) {
					saleOrderDetailDAO.deleteSaleOrderDetail(sod);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateListSaleOrder(List<SaleOrder> lstSalesOrder) throws BusinessException {
		try {
			saleOrderDAO.updateSaleOrder(lstSalesOrder);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateListSaleOrderDetail(List<SaleOrderDetail> lstSalesOrder) throws BusinessException {
		try {
			saleOrderDAO.updateSaleOrderDetail(lstSalesOrder);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	private void deleteOneSaleOrder(SaleOrder salesOrder, String updatedStaffCode,LogInfoVO logInfoVO) throws DataAccessException, BusinessException {
		
		LogInfoVO logInfo = new LogInfoVO();
		logInfo.setStaffCode(salesOrder.getUpdateUser());
		if (SaleOrderStatus.NOT_YET_APPROVE.equals(salesOrder.getApproved()) && (salesOrder.getFromSaleOrder() == null) && !OrderType.CO.equals(salesOrder.getOrderType()) && !OrderType.SO.equals(salesOrder.getOrderType())
				&& !OrderType.TT.equals(salesOrder.getOrderType())) {

			List<String> lstProCode = new ArrayList<String>();

			List<SaleOrderDetail> lstSaleOrderDetail = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(salesOrder.getId(), null);

			// update product lot
			for (SaleOrderDetail sod : lstSaleOrderDetail) {
				List<SaleOrderLot> lstSaleOrderLot = saleOrderLotDAO.getListSaleOrderLotBySaleOrderDetail(null, sod.getId());
				for (SaleOrderLot sol : lstSaleOrderLot) {
					if (sol.getProduct() != null) {
						StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(sod.getProduct().getId(), StockObjectType.SHOP, sod.getShop().getId(), sol.getWarehouse().getId());
						if (stockTotal != null) {
							
							/**Log conent */
							try {		
								logInfoVO.getContenFormat(
										sol.getProduct().getId(), 
										sol.getWarehouse().getId(), 
										sol.getQuantity(), 
										stockTotal.getAvailableQuantity(), 
										stockTotal.getAvailableQuantity() + sod.getQuantity(), 
										stockTotal.getQuantity(), 
										stockTotal.getQuantity());							
							} catch (Exception e) {}
							/** End log conent */
							
							stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() + sod.getQuantity());
							stockTotalDAO.updateStockTotal(stockTotal);
						}
					}
				}
			}
			// cap nhat bang customer
			this.updateCustomerAndRouting(salesOrder, null, logInfo);

			if (SaleOrderSource.TABLET.equals(salesOrder.getOrderSource())) {
				if (salesOrder.getTimePrint() == null && SaleOrderStatus.APPROVED.equals(salesOrder.getApproved())) {
					rollbackSaleOrder(salesOrder.getId(), updatedStaffCode,logInfoVO);
				}
				salesOrder.setApproved(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM);
				salesOrder.setDestroyCode(updatedStaffCode);

				saleOrderDAO.updateSaleOrder(salesOrder);
			} else {
				if (salesOrder.getTimePrint() == null && SaleOrderStatus.APPROVED.equals(salesOrder.getApproved())) {
					rollbackSaleOrder(salesOrder.getId(), updatedStaffCode,logInfoVO);
				}
				// xoa lot + detail
				for (SaleOrderDetail sod : lstSaleOrderDetail) {
					List<SaleOrderLot> lstSaleOrderLot = saleOrderLotDAO.getListSaleOrderLotBySaleOrderDetail(null, sod.getId());
					for (SaleOrderLot sol : lstSaleOrderLot) {
						saleOrderLotDAO.deleteSaleOrderLot(sol);
					}
					saleOrderDetailDAO.deleteSaleOrderDetail(sod);
				}

				saleOrderDAO.deleteSaleOrder(salesOrder);
			}

			/** tungtt: Update PoCustomer */
			if (salesOrder.getFromPoCustomerId() != null) {
				PoCustomer poCustomer = poCustomerDAO.getPoCustomerById(salesOrder.getFromPoCustomerId());
				if (poCustomer != null) {
					poCustomer.setApproved(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM);
					poCustomerDAO.updatePoCustomer(poCustomer);
				}
			}
			
			/**Log conent */
			try {		
				logInfoVO.setActionType(LogInfoVO.ACTION_DELETE);
				logInfoVO.setFunctionCode(LogInfoVO.FC_SALE_DELETE_ORDER);
				logInfoVO.setIdObject(salesOrder.getId().toString());
				LogUtility.logInfoWs(logInfoVO);
			} catch (Exception e) {}
			/** End log conent */
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void deleteListSaleOrderByListId(List<Long> lstSalesOrderId, String updatedStaffCode,LogInfoVO logInfoVO) throws BusinessException {
		for (Long salesOrder : lstSalesOrderId) {
			try {
				SaleOrder saleOrder = saleOrderDAO.getSaleOrderById(salesOrder);
				this.deleteOneSaleOrder(saleOrder, updatedStaffCode,logInfoVO);
			} catch (DataAccessException e) {
				throw new BusinessException(e.getMessage());
			}
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void deleteListSaleOrder(List<SaleOrder> lstSalesOrder, String updatedStaffCode,LogInfoVO logInfoVO) throws BusinessException {
		for (SaleOrder saleOrder : lstSalesOrder) {
			try {
				this.deleteOneSaleOrder(saleOrder, updatedStaffCode,logInfoVO);
			} catch (DataAccessException e) {
				throw new BusinessException(e.getMessage());
			}
		}
	}

	//phuongvm
	private void approveOneSaleOrderVanSale(SaleOrder salesOrder) throws BusinessException {
		try {
			Date now = shopLockDAO.getNextLockedDay(salesOrder.getShop().getId());
			if (now == null) {
				now = commonDAO.getSysDate();
			}
			if (SaleOrderStatus.APPROVED.getValue().equals(salesOrder.getApproved().getValue()) && (salesOrder.getApprovedVan() == null || salesOrder.getApprovedVan() == 0) && salesOrder.getFromSaleOrder() == null
					&& !OrderType.IN.getValue().equals(salesOrder.getOrderType().getValue()) && !OrderType.CM.getValue().equals(salesOrder.getOrderType().getValue()) && !OrderType.TT.getValue().equals(salesOrder.getOrderType().getValue())
					&& (DateUtility.compareDateWithoutTime(salesOrder.getOrderDate(), now) == 0)) {
				LogInfoVO logInfo = new LogInfoVO();
				logInfo.setStaffCode(salesOrder.getUpdateUser());
				/** tungtt set new ordernumer khi duyet don hang */
				String newOrderNumber = saleOrderDAO.getLastestOrderNumberByShop(salesOrder.getShop().getId());
				newOrderNumber = newOrderNumber.replace("IN", "SO");
				salesOrder.setOrderNumber(newOrderNumber);
				salesOrder.setApprovedVan(1);
				saleOrderDAO.updateSaleOrder(salesOrder);
				saleOrderDAO.updateLastestOrderNumberByShop(salesOrder.getShop().getId());
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	private void updateCustomerAndRouting(SaleOrder salesOrder, SoFilter filter, LogInfoVO logInfo) throws DataAccessException {
		// cap nhat bang customer
		Date lockDay = commonDAO.getSysDate();
		if (filter != null && filter.getLockDay() != null) {
			lockDay = filter.getLockDay();
		}
		Customer customer = salesOrder.getCustomer();
		Staff staff = salesOrder.getStaff();
		customer.setLastOrder(saleOrderDAO.checkSaleOrderInLast60days(customer.getId(), null, salesOrder.getId(), lockDay));
		customerDAO.updateCustomer(customer, logInfo);
		// cap nhat bang routing_customer
		Date temp = saleOrderDAO.checkSaleOrderInLast60days(customer.getId(), staff.getId(), salesOrder.getId(), lockDay);
		StaffCustomer sc = staffCustomerDAO.getStaffCustomer(staff.getId(), customer.getId());
		if (sc == null) {
			sc = new StaffCustomer();
			sc.setCustomer(customer);
			sc.setStaff(staff);
			sc.setLastOrder(temp);
			sc.setShop(salesOrder.getShop());
			staffCustomerDAO.createStaffCustomer(sc, logInfo);
		} else {
			sc.setLastOrder(temp);
			staffCustomerDAO.updateStaffCustomer(sc, logInfo);
		}
		
		/*
		 * Modified by NhanLT - 20/11/2014
		 * Load routing customer theo staff id truong hop KH thuoc nhieu tuyen
		 * */
		/*RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDate(salesOrder.getCustomer().getId(),salesOrder.getShop().getId(),
		salesOrder.getOrderDate());*/
		RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDateAndStaff(salesOrder.getCustomer().getId(),salesOrder.getShop().getId(),
				salesOrder.getOrderDate(),salesOrder.getStaff().getId());
		if(rc != null) {
			rc.setLastOrder(temp);
			routingCustomerDAO.updateRoutingCustomer(rc);
		}
	}

	@Override
	public SaleOrder getSaleOrderById(Long id) throws BusinessException {
		try {
			return id == null ? null : saleOrderDAO.getSaleOrderById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public SaleOrder getSaleOrderByOrderNumber(String orderNumber, Long shopId) throws BusinessException {
		try {
			return saleOrderDAO.getSaleOrderByOrderNumber(orderNumber, shopId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<SaleOrder> getListSaleOrder(KPaging<SaleOrder> kPaging, String shopCode, Long customerId, Long staffId, Date fromDate, Date toDate, SaleOrderStatus approved, OrderType orderType, Integer state) throws BusinessException {
		try {
			List<SaleOrder> lst = saleOrderDAO.getListSaleOrder(kPaging, shopCode, customerId, staffId, fromDate, toDate, approved, orderType, state);
			ObjectVO<SaleOrder> vo = new ObjectVO<SaleOrder>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleOrder> getListSaleOrderExByFilter(SaleOrderFilter<SaleOrder> filter, String orderBy) throws BusinessException {
		try {
			List<SaleOrder> lst = saleOrderDAO.getListSaleOrderExByFilter(filter, orderBy);
			ObjectVO<SaleOrder> vo = new ObjectVO<SaleOrder>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<CommercialSupportVO> getListCommercialSupport(KPaging<CommercialSupportVO> kPaging, Long shopId, Long customerId, Date orderDate, Long saleOrderId) throws BusinessException {
		try {
			List<CommercialSupportVO> lst = promotionProgramDAO.getListCommercialSupport(kPaging, shopId, customerId, orderDate, saleOrderId);
			ObjectVO<CommercialSupportVO> vo = new ObjectVO<CommercialSupportVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	public ObjectVO<CommercialSupportVO> getCommercialSupport(KPaging<CommercialSupportVO> kPaging, Long shopId, Long customerId, Date orderDate, Long saleOrderId, String commercialSupportCode) throws BusinessException {
		try {
			List<CommercialSupportVO> lst = promotionProgramDAO.getCommercialSupport(kPaging, shopId, customerId, orderDate, saleOrderId, commercialSupportCode);
			ObjectVO<CommercialSupportVO> vo = new ObjectVO<CommercialSupportVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<Boolean> checkIfCommercialSupportExists(List<CommercialSupportCodeVO> lstCommercialSupportCodeVO, Date orderDate, Long saleOrderId) throws BusinessException {
		try {
			return promotionProgramDAO.checkIfCommercialSupportExists(lstCommercialSupportCodeVO, orderDate, saleOrderId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<Invoice> getListInvoice(InvoiceFilter filter) throws BusinessException {
		try {
			List<Invoice> lst = invoiceDAO.getListInvoice(filter);
			ObjectVO<Invoice> vo = new ObjectVO<Invoice>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateInvoice(Invoice invoice) throws BusinessException {
		try {
			invoiceDAO.updateInvoice(invoice);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateListInvoice(List<Invoice> lstInvoice) throws BusinessException {
		try {
			for (Invoice inv : lstInvoice) {
				invoiceDAO.updateInvoice(inv);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SaleOrderMgr#updateReturnSaleOrder(java.lang
	 * .Long)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public SaleOrder updateReturnSaleOrder(long saleOrderId, String createUser, boolean flag) throws BusinessException {
		try {
			// tao saleOrder
			SaleOrder tempsaleOrder = saleOrderDAO.getSaleOrderByIdForUpdate(saleOrderId);

			if (!tempsaleOrder.getType().equals(SaleOrderType.NOT_YET_RETURNED)) {
				throw new BusinessException("Sale order's status is not NOT_YET_RETURNED");
			}

			if (!saleOrderDAO.checkSaleOrderBeforeDueDate(saleOrderId)) {
				throw new BusinessException("Sale order is after due date");
			}

			Boolean isNoReturnKeyShop = false;
			ApParam ap = apParamMgr.getApParamByCodeEx(ApParamType.RETURN_KEYSHOP_CONFIG.getValue(), ActiveType.RUNNING);
			if (ap != null && Constant.RETURN_KEYSHOP_CONFIG_NO.equals(ap.getValue())){
				isNoReturnKeyShop = true;
			}
				
			Shop shop = tempsaleOrder.getShop();
			Date lockDate = shopLockDAO.getApplicationDate(shop.getId());
			Date now1 = commonDAO.getSysDate();

			SaleOrder saleOrder = new SaleOrder();

			saleOrder.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
			saleOrder.setApprovedStep(SaleOrderStep.CONFIRMED);
			if (tempsaleOrder.getOrderType().getValue().equals(OrderType.SO.getValue())) {
				saleOrder.setOrderType(OrderType.CO);
				saleOrder.setApprovedVan(1);
			} else {
				saleOrder.setOrderType(OrderType.CM);
			}
			saleOrder.setType(SaleOrderType.RETURNED_ORDER);
			saleOrder.setCar(tempsaleOrder.getCar());
			saleOrder.setCashier(tempsaleOrder.getCashier());
			saleOrder.setCreateUser(createUser);
			saleOrder.setCreateDate(now1);
			saleOrder.setCustomer(tempsaleOrder.getCustomer());
			saleOrder.setDelivery(tempsaleOrder.getDelivery());
			saleOrder.setDeliveryDate(tempsaleOrder.getDeliveryDate());
			saleOrder.setDescription(tempsaleOrder.getDescription());
			saleOrder.setFromSaleOrder(tempsaleOrder);
			saleOrder.setOrderDate(lockDate);
			saleOrder.setPriority(tempsaleOrder.getPriority());
			saleOrder.setShop(tempsaleOrder.getShop());
			saleOrder.setShopCode(tempsaleOrder.getShopCode());
			saleOrder.setStaff(tempsaleOrder.getStaff());
			saleOrder.setOrderSource(SaleOrderSource.WEB);
			saleOrder.setTotal(tempsaleOrder.getTotal());
			//saleOrder.setVat(tempsaleOrder.getVat());
			//saleOrder.setSaleOrderDiscount(tempsaleOrder.getSaleOrderDiscount());
			saleOrder.setTotalDetail(tempsaleOrder.getTotalDetail());
			saleOrder.setIsVisitPlan(tempsaleOrder.getIsVisitPlan());
			saleOrder.setRouting(tempsaleOrder.getRouting());
			saleOrder.setQuantity(tempsaleOrder.getQuantity());
			saleOrder.setAmount(tempsaleOrder.getAmount());
			saleOrder.setIsRewardKs(0);
			
			List<ShopParam> lstPr = shopDAO.getConfigShopParam(shop.getId(), Constant.SYS_RETURN_DATE);
			if (lstPr != null && lstPr.size() > 0) {
				if(Constant.SYS_RETURN_DATE_VALUE_ORDER_DATE.equals(lstPr.get(0).getValue())){
					saleOrder.setAccountDate(tempsaleOrder.getOrderDate());
				} else if(Constant.SYS_RETURN_DATE_VALUE_APPROVED_DATE.equals(lstPr.get(0).getValue())){
					saleOrder.setAccountDate(tempsaleOrder.getApprovedDate());
				} else if(Constant.SYS_RETURN_DATE_VALUE_LOCK_DATE.equals(lstPr.get(0).getValue())) {
					saleOrder.setAccountDate(lockDate);
				}
			}
			Cycle cycle = cycleMgr.getCycleByDate(saleOrder.getOrderDate());
			saleOrder.setCycle(cycle);
			
			// them moi danh sach saleOrderDetail tuong ung
			SaleOrderDetailFilter<SaleOrderDetail> filter = new SaleOrderDetailFilter<SaleOrderDetail>();
			filter.setSaleOrderId(saleOrderId);
			filter.setIsNotKeyShop(isNoReturnKeyShop);
			List<SaleOrderDetail> listSaleOrderDetail = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(filter);
//			List<SaleOrderDetail> listSaleOrderDetail = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(saleOrderId, null);
			saleOrder.setTotalDetail(Long.valueOf(listSaleOrderDetail.size()));
			
			//lay ds SaleOrderPromoLot
			List<SaleOrderPromoLot> lstOrderPromoLots = saleOrderLotDAO.getListSaleOrderPromoLotBySaleOrderId(saleOrderId);
			//lay ds SalePromoMap
			List<SalePromoMap> lstSalePromoMaps = saleOrderDAO.getListSalePromoMapBySaleOrder(saleOrderId);
			/*
			 * List<String> lstPromotionCode = new ArrayList<String>();
			 * LogInfoVO logInfo = new LogInfoVO();
			 * logInfo.setStaffCode(createUser); SaleOrder salesOrder =
			 * tempsaleOrder;
			 */
			
			List<SaleOrderDetail> lstDetail = new ArrayList<SaleOrderDetail>();
			List<SaleOrderLot> lstLot = new ArrayList<SaleOrderLot>();
			List<SaleOrderPromotion> lstSOPromo = new ArrayList<SaleOrderPromotion>();
			List<SaleOrderPromoDetail> lstSOPromoDetail = new ArrayList<SaleOrderPromoDetail>();
			List<SaleOrderPromoLot> lstSOPromoLot = new ArrayList<SaleOrderPromoLot>();
			List<SalePromoMap> lstSPromoMap = new ArrayList<SalePromoMap>();
			
			BigDecimal discountAmount = BigDecimal.ZERO;
			BigDecimal totalWeight = BigDecimal.ZERO;
			
			int j, szj;
			// copy sale_order_detail
			for (SaleOrderDetail itemSaleOrderDetail : listSaleOrderDetail) {
				Map<Long, SaleOrderLot> mapSaleOrderLot = new HashMap<Long, SaleOrderLot>();
				BigDecimal sumKSDiscountAmountLot = BigDecimal.ZERO;
				
				SaleOrderDetail detail = new SaleOrderDetail();
				detail.setAmount(itemSaleOrderDetail.getAmount());
				detail.setTotalWeight(itemSaleOrderDetail.getTotalWeight());
				totalWeight = totalWeight.add(itemSaleOrderDetail.getTotalWeight());
				detail.setDiscountPercent(itemSaleOrderDetail.getDiscountPercent());
				detail.setIsFreeItem(itemSaleOrderDetail.getIsFreeItem());
				detail.setOrderDate(lockDate);
				detail.setPrice(itemSaleOrderDetail.getPrice());
				detail.setPriceNotVat(itemSaleOrderDetail.getPriceNotVat());
				detail.setPriceValue(itemSaleOrderDetail.getPriceValue());
				detail.setProduct(itemSaleOrderDetail.getProduct());
				detail.setProgramCode(itemSaleOrderDetail.getProgramCode());
				detail.setProgrameTypeCode(itemSaleOrderDetail.getProgrameTypeCode());
//				detail.setJoinProgramCode(itemSaleOrderDetail.getJoinProgramCode());
				detail.setProgramType(itemSaleOrderDetail.getProgramType());
				detail.setProductGroupId(itemSaleOrderDetail.getProductGroupId());
				detail.setGroupLevelId(itemSaleOrderDetail.getGroupLevelId());
				detail.setMaxAmountFree(itemSaleOrderDetail.getMaxAmountFree());
				detail.setMaxQuantityFree(itemSaleOrderDetail.getMaxQuantityFree());
				detail.setPayingOrder(itemSaleOrderDetail.getPayingOrder());
				detail.setPromotionOrderNumber(itemSaleOrderDetail.getPromotionOrderNumber());
				detail.setShop(itemSaleOrderDetail.getShop());
				detail.setStaff(itemSaleOrderDetail.getStaff());
				detail.setVat(itemSaleOrderDetail.getVat());
				detail.setCreateUser(createUser);
				detail.setCreateDate(now1);
				detail.setSaleOrder(saleOrder);
				detail.setQuantity(itemSaleOrderDetail.getQuantity());
				detail.setQuantityRetail(itemSaleOrderDetail.getQuantityRetail());
				detail.setQuantityPackage(itemSaleOrderDetail.getQuantityPackage());
				detail.setPackagePrice(itemSaleOrderDetail.getPackagePrice());
				detail.setPackagePriceNotVAT(itemSaleOrderDetail.getPackagePriceNotVAT());
				detail.setProductInfo(itemSaleOrderDetail.getProductInfo());
				detail.setConvfact(itemSaleOrderDetail.getConvfact());
				// copy sale_order_lot
				List<SaleOrderLot> listSaleOrderLot = saleOrderLotDAO.getListSaleOrderLotBySaleOrderDetailId(itemSaleOrderDetail.getId());
				if (listSaleOrderLot != null) {
					for (j = 0, szj = listSaleOrderLot.size(); j < szj; j++) {
						SaleOrderLot itemSaleOrderLot = listSaleOrderLot.get(j);
						SaleOrderLot lot = new SaleOrderLot();
						// tinh discountAmount cua saleOrderPromoLot keyShop
						if (isNoReturnKeyShop && IsFreeItemInSaleOderDetail.SALE.getValue().equals(itemSaleOrderDetail.getIsFreeItem()) 
								&& itemSaleOrderLot.getDiscountAmount() != null) {
							// saleOrderLot cua saleOrderDetail chua san pham ban
							BigDecimal sumKSDiscountAmountPromoLot = BigDecimal.ZERO;
							if (lstOrderPromoLots != null && lstOrderPromoLots.size() > 0) {
								for (SaleOrderPromoLot saleOrderPromoLot : lstOrderPromoLots) {
									if (ProgramType.KEY_SHOP.equals(saleOrderPromoLot.getProgramType()) && saleOrderPromoLot.getSaleOrderLotId().equals(itemSaleOrderLot.getId())) {
										sumKSDiscountAmountPromoLot = sumKSDiscountAmountPromoLot.add(saleOrderPromoLot.getDiscountAmount());
									}
								}
							}
							lot.setDiscountAmount(itemSaleOrderLot.getDiscountAmount().subtract(sumKSDiscountAmountPromoLot));
							sumKSDiscountAmountLot = sumKSDiscountAmountLot.add(sumKSDiscountAmountPromoLot);
						} else {
							lot.setDiscountAmount(itemSaleOrderLot.getDiscountAmount());
						}
						
						lot.setDiscountPercent(itemSaleOrderLot.getDiscountPercent());
						lot.setExpirationDate(itemSaleOrderLot.getExpirationDate());
						lot.setLot(itemSaleOrderLot.getLot());
						//lot.setWarehouse(itemSaleOrderLot.getWarehouse());
						//lot.setStockTotal(itemSaleOrderLot.getStockTotal());
						lot.setOrderDate(itemSaleOrderLot.getOrderDate());
						lot.setPrice(itemSaleOrderLot.getPrice());
						lot.setPriceValue(itemSaleOrderLot.getPriceValue());
						lot.setPackagePrice(itemSaleOrderLot.getPackagePrice());
						lot.setProduct(itemSaleOrderLot.getProduct());
						lot.setQuantity(itemSaleOrderLot.getQuantity());
						lot.setQuantityPackage(itemSaleOrderLot.getQuantityPackage());
						lot.setQuantityRetail(itemSaleOrderLot.getQuantityRetail());
						lot.setShop(itemSaleOrderLot.getShop());
						lot.setStaff(itemSaleOrderLot.getStaff());
						lot.setCreateUser(createUser);
						lot.setCreateDate(now1);
						lot.setOrderDate(lockDate);

						if (OrderType.CM.equals(saleOrder.getOrderType())) {
							lot.setWarehouse(itemSaleOrderLot.getWarehouse());
							lot.setStockTotal(itemSaleOrderLot.getStockTotal());
						} else {
							Warehouse warehouse = stockTotalDAO.getMostPriorityWareHouse(saleOrder.getShop().getId(), lot.getProduct().getId());
							lot.setWarehouse(warehouse);
							if (warehouse != null) {
								StockTotal stock = stockTotalDAO.getStockTotalByProductAndOwner(lot.getProduct().getId(), StockObjectType.SHOP, saleOrder.getShop().getId(), warehouse.getId());
								lot.setStockTotal(stock);
							} else {
								lot.setStockTotal(itemSaleOrderLot.getStockTotal());
							}
						}

						lot.setSaleOrderDetail(detail);
						lot.setSaleOrder(saleOrder);

						lstLot.add(lot);
						mapSaleOrderLot.put(itemSaleOrderLot.getId(), lot);
					}

					// copy sale_order_promo_detail
					// lay ds SaleOrderPromoDetail khong co keyShop
					SaleOrderPromoDetailFilter saleOrderPromoDetailFilter = new SaleOrderPromoDetailFilter();
					saleOrderPromoDetailFilter.setSaleOrderDetailId(itemSaleOrderDetail.getId());
					saleOrderPromoDetailFilter.setIsNotKeyShop(isNoReturnKeyShop);
					List<SaleOrderPromoDetail> lstSaleOrderPromoDetail = saleOrderPromoDetailDAO.getListSaleOrderPromoDetailByFilter(saleOrderPromoDetailFilter);
//					List<SaleOrderPromoDetail> lstSaleOrderPromoDetail = saleOrderPromoDetailDAO.getListSaleOrderPromoByDetailId(itemSaleOrderDetail.getId());
					if (lstSOPromo != null && lstSaleOrderPromoDetail.size() > 0) {
						for (j = 0, szj = lstSaleOrderPromoDetail.size(); j < szj; j++) {
							SaleOrderPromoDetail promo = lstSaleOrderPromoDetail.get(j);
							SaleOrderPromoDetail soP = new SaleOrderPromoDetail();
							soP.setProgramCode(promo.getProgramCode());
							soP.setProgramType(promo.getProgramType());
							soP.setDiscountPercent(promo.getDiscountPercent());
							soP.setDiscountAmount(promo.getDiscountAmount());
							soP.setMaxAmountFree(promo.getMaxAmountFree());
							soP.setIsOwner(promo.getIsOwner());
							soP.setStaff(promo.getStaff());
							soP.setProgrameTypeCode(promo.getProgrameTypeCode());
							soP.setProductGroupId(promo.getProductGroupId());
							soP.setGroupLevelId(promo.getGroupLevelId());
							soP.setShop(promo.getShop());
							soP.setSaleOrderDetail(detail);
							soP.setSaleOrder(saleOrder);
							soP.setOrderDate(lockDate);
							lstSOPromoDetail.add(soP);
						}
					}
				}
				
				// copy saleOrderPromoLot
				if (lstOrderPromoLots != null && lstOrderPromoLots.size() > 0) {
					for (int i = 0, n = lstOrderPromoLots.size(); i < n; i++) {
						SaleOrderPromoLot saleOrderPromoLot = lstOrderPromoLots.get(i);
						if (!isNoReturnKeyShop || (itemSaleOrderDetail.getId().equals(saleOrderPromoLot.getSaleOrderDetail().getId())
								&& !ProgramType.KEY_SHOP.equals(saleOrderPromoLot.getProgramType()))) {
							SaleOrderPromoLot soPromoLot = new SaleOrderPromoLot();
							soPromoLot.setDiscountAmount(saleOrderPromoLot.getDiscountAmount());
							soPromoLot.setDiscountComputeAmount(saleOrderPromoLot.getDiscountComputeAmount());
							soPromoLot.setDiscountPercent(saleOrderPromoLot.getDiscountPercent());
							soPromoLot.setIsOwner(saleOrderPromoLot.getIsOwner());
							soPromoLot.setOrderDate(saleOrderPromoLot.getOrderDate());
							soPromoLot.setProgramCode(saleOrderPromoLot.getProgramCode());
							soPromoLot.setProgrameTypeCode(saleOrderPromoLot.getProgrameTypeCode());
							soPromoLot.setProgramType(saleOrderPromoLot.getProgramType());
							soPromoLot.setSaleOrder(saleOrder);
							soPromoLot.setSaleOrderDetail(detail);
							SaleOrderLot lot = mapSaleOrderLot.get(saleOrderPromoLot.getSaleOrderLotId());
							soPromoLot.setSaleOrderLotId(lot == null ? null : lot.getId());
							soPromoLot.setShop(saleOrderPromoLot.getShop());
							soPromoLot.setStaff(saleOrderPromoLot.getStaff());
							lstSOPromoLot.add(soPromoLot);
						}
					}
				}
				
				//copy SalePromoMap
				if (lstSalePromoMaps != null && lstSalePromoMaps.size() > 0) {
					for (int i = 0, n = lstSalePromoMaps.size(); i < n; i++) {
						SalePromoMap salePromoMap = lstSalePromoMaps.get(i);
						if (itemSaleOrderDetail.getId().equals(salePromoMap.getSaleOrderDetail().getId())) {
							SalePromoMap promoMap = new SalePromoMap();
							promoMap.setProgramCode(salePromoMap.getProgramCode());
							promoMap.setSaleOrder(saleOrder);
							promoMap.setSaleOrderDetail(detail);
							promoMap.setStaff(salePromoMap.getStaff());
							promoMap.setStatus(salePromoMap.getStatus());
							lstSPromoMap.add(promoMap);
						}
					}
				}
				
				if (isNoReturnKeyShop && IsFreeItemInSaleOderDetail.SALE.getValue().equals(itemSaleOrderDetail.getIsFreeItem())) {
					detail.setDiscountAmount(itemSaleOrderDetail.getDiscountAmount().subtract(sumKSDiscountAmountLot));
					discountAmount = discountAmount.add(sumKSDiscountAmountLot);
				} else {
					detail.setDiscountAmount(itemSaleOrderDetail.getDiscountAmount());
				}
				lstDetail.add(detail);
			}

			List<SaleOrderPromotion> lstSOPromotion = saleOrderPromoDetailDAO.getListSaleOrderPromotionByOrderId(tempsaleOrder.getId());
			if (lstSOPromotion != null && lstSOPromotion.size() > 0) {
				for (SaleOrderPromotion SOP : lstSOPromotion) {
					SaleOrderPromotion soPromotion = new SaleOrderPromotion();
					soPromotion.setSaleOrder(saleOrder);
					soPromotion.setPromotionCode(SOP.getPromotionCode());
					soPromotion.setProductGroupId(SOP.getProductGroupId());
					soPromotion.setPromotionLevel(SOP.getPromotionLevel());
					soPromotion.setQuantityReceived(SOP.getQuantityReceived());
					soPromotion.setMaxQuantityReceived(SOP.getMaxQuantityReceived());
					soPromotion.setPromotionProgram(SOP.getPromotionProgram());
					soPromotion.setShop(SOP.getShop());
					soPromotion.setStaff(SOP.getStaff());
					soPromotion.setGroupLevelId(SOP.getGroupLevelId());
					soPromotion.setOrderDate(lockDate);
					soPromotion.setCreateDate(now1);
					soPromotion.setCreateUser(createUser);
					//vuongmq; Begin 31/01/2015; luu cac gia tri them moi cho SaleOrderPromotion
					soPromotion.setExceedObject(SOP.getExceedObject());
					soPromotion.setExceedUnit(SOP.getExceedUnit());
					soPromotion.setNumReceived(SOP.getNumReceived());
					soPromotion.setMaxNumReceived(SOP.getMaxNumReceived());
					soPromotion.setAmountReceived(SOP.getAmountReceived());
					soPromotion.setMaxAmountReceived(SOP.getMaxAmountReceived());
					soPromotion.setPromotionDetail(SOP.getPromotionDetail());
					//vuongmq; End 31/01/2015; luu cac gia tri them moi cho SaleOrderPromotion
					lstSOPromo.add(soPromotion);
				}
			}
			
			if (isNoReturnKeyShop) {
				saleOrder.setDiscount(tempsaleOrder.getDiscount().subtract(discountAmount));
				saleOrder.setTotal(tempsaleOrder.getTotal().add(discountAmount));
				saleOrder.setTotalWeight(totalWeight);
			} else {
				saleOrder.setDiscount(tempsaleOrder.getDiscount());
				saleOrder.setTotal(tempsaleOrder.getTotal());
				saleOrder.setTotalWeight(tempsaleOrder.getTotalWeight());
			}
			
			// save
			saleOrder = saleOrderDAO.createSaleOrder(saleOrder);
			saleOrderDetailDAO.createSaleOrderDetail(lstDetail);
			saleOrderLotDAO.createSaleOrderLot(lstLot);
			saleOrderLotDAO.createSaleOrderPromoLot(lstSOPromoLot);
			saleOrderPromoDetailDAO.createListSaleOrderPromoDetail(lstSOPromoDetail);
			saleOrderDetailDAO.createSalePromoMap(lstSPromoMap);
			saleOrderPromoDetailDAO.createListSaleOrderPromotion(lstSOPromo);

//			tempsaleOrder.setType(SaleOrderType.RETURNED);
//			saleOrderDAO.updateSaleOrder(tempsaleOrder);
//			this.negateRptCTTLPayAndDetailForReturn(tempsaleOrder,saleOrder);
			return saleOrder;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public SaleOrder rollbackSaleOrder(long saleOrderId, String createUser,LogInfoVO logInfoVO ) throws BusinessException {
		try {
			// tao saleOrder
			SaleOrder tempsaleOrder = saleOrderDAO.getSaleOrderByIdForUpdate(saleOrderId);

			if (!tempsaleOrder.getType().equals(SaleOrderType.NOT_YET_RETURNED))
				throw new BusinessException("Sale order's status is not NOT_YET_RETURNED");

			if (!saleOrderDAO.checkSaleOrderBeforeDueDate(saleOrderId))
				throw new BusinessException("Sale order is after due date");

			// them moi danh sach saleOrderDetail tuong ung
			List<SaleOrderDetail> listSaleOrderDetail = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(saleOrderId, null);
			List<String> lstPromotionCode = new ArrayList<String>();
			LogInfoVO logInfo = new LogInfoVO();
			logInfo.setStaffCode(createUser);

			for (SaleOrderDetail itemSaleOrderDetail : listSaleOrderDetail) {

				// update: HUNGNM
				SaleOrderDetail promoDetail = itemSaleOrderDetail;
				String promotionCode = promoDetail.getProgramCode();

				Boolean checkQuantityReceived = lstPromotionCode.indexOf(promotionCode) == -1 && (promoDetail.getQuantity() > 0 || (promoDetail.getProduct() == null && promoDetail.getDiscountAmount().compareTo(BigDecimal.ZERO) == 1))
						&& (ProgramType.AUTO_PROM.equals(promoDetail.getProgramType()) || ProgramType.MANUAL_PROM.equals(promoDetail.getProgramType()));

				if (checkQuantityReceived) {
					lstPromotionCode.add(promotionCode);
					// giam so suat nhan khuyen mai
//					this.changePromotionQuantity(-1, logInfo, promotionCode, tempsaleOrder.getCustomer().getShop().getShopCode(), tempsaleOrder.getCustomer().getShortCode(), itemSaleOrderDetail.getProgramType(), tempsaleOrder.getCustomer()
//							.getChannelType() == null ? null : tempsaleOrder.getCustomer().getChannelType().getId(), itemSaleOrderDetail, tempsaleOrder.getStaff(), tempsaleOrder.getShop());
				}
				// end update

				// update stock total tuong ung
				Product product = itemSaleOrderDetail.getProduct();
				List<SaleOrderLot> listSaleOrderLot = null;
				listSaleOrderLot = saleOrderLotDAO.getListSaleOrderLotBySaleOrderDetailId(itemSaleOrderDetail.getId());
				if (listSaleOrderLot != null) {
					for (SaleOrderLot itemSaleOrderLot : listSaleOrderLot) {
						if (itemSaleOrderLot.getProduct() != null) {
							StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(product.getId(), StockObjectType.SHOP, tempsaleOrder.getShop().getId(), itemSaleOrderLot.getWarehouse().getId());
							
							/**Log conent */
							try {		
								logInfoVO.getContenFormat(
										itemSaleOrderLot.getProduct().getId(), 
										itemSaleOrderLot.getWarehouse().getId(), 
										itemSaleOrderLot.getQuantity(), 
										stockTotal.getAvailableQuantity(), 
										stockTotal.getAvailableQuantity(), 
										stockTotal.getQuantity(), 
										stockTotal.getQuantity() + itemSaleOrderLot.getQuantity());							
							} catch (Exception e) {}
							/** End log conent */
							
							stockTotal.setQuantity(stockTotal.getQuantity() + itemSaleOrderLot.getQuantity());
							stockTotalDAO.updateStockTotal(stockTotal);
						}
					}
				}
			}

			Debit dg = debitDAO.getDebitForUpdate(tempsaleOrder.getCustomer().getId(), DebitOwnerType.CUSTOMER);
			dg.setTotalAmount(dg.getTotalAmount().subtract(tempsaleOrder.getTotal()));
			dg.setTotalDebit(dg.getTotalDebit().subtract(tempsaleOrder.getTotal()));
			debitDAO.updateDebit(dg);

			DebitDetail debitDetail = new DebitDetail();
			debitDetail.setDebit(dg);
			debitDetail.setFromObjectId(tempsaleOrder.getId());
			debitDetail.setAmount(tempsaleOrder.getAmount().multiply(BigDecimal.valueOf(-1)));
			debitDetail.setDiscount(tempsaleOrder.getDiscount().multiply(BigDecimal.valueOf(-1)));
			debitDetail.setTotal(tempsaleOrder.getTotal().multiply(BigDecimal.valueOf(-1)));
			debitDetail.setRemain(tempsaleOrder.getTotal().multiply(BigDecimal.valueOf(-1)));
			debitDetail.setCreateUser(createUser);
			debitDetailDAO.createDebitDetail(debitDetail);
			
			
			/** Ghi log */
			try {
				logInfoVO.setIdObject(String.valueOf(saleOrderId));
				LogUtility.logInfoWs(logInfoVO);
			} catch (Exception e) {}
			/** End ghi log */

			return tempsaleOrder;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SaleOrderMgr#getListSaleOrderWithCondition(
	 * java.lang.Float, java.lang.String, java.util.Date, java.util.Date,
	 * java.lang.String, java.lang.String, int)
	 */
	@Override
	public List<SaleOrder> getListSaleOrderWithCondition(Float vat, String orderNumber, Date fromDate, Date toDate, String customerCode, String staffCode, Boolean hasInvoice) throws BusinessException {
		try {
			return saleOrderDAO.getListSaleOrderWithCondition(null, vat, orderNumber, fromDate, toDate, customerCode, staffCode, hasInvoice);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * @Override
	 * 
	 * @Transactional(rollbackFor = Exception.class) public void
	 * createInvoiceFromSaleOrder(Long shopId, String username, List<Long>
	 * lstSaleOrderId, List<String> lstInvoiceNumber, List<InvoiceCustPayment>
	 * listCustPayment) throws BusinessException { try {
	 * 
	 * Báº£ng SALE_ORDER: SKU = sáº½ Ä‘áº¿m cÃ¡c máº·t hÃ ng trong hÃ³a Ä‘Æ¡n (báº£ng
	 * SALE_ORDER_DETAIL, trÆ°á»�ng IS_FREE_ITEM = 0):
	 * 
	 * if (shopId == null) { throw new
	 * IllegalArgumentException("shopId is null"); } if (lstSaleOrderId == null)
	 * { throw new IllegalArgumentException("lstSaleOrderId is null"); } if
	 * (lstInvoiceNumber == null) { throw new
	 * IllegalArgumentException("lstInvoiceNumber is null"); } if
	 * (lstSaleOrderId.size() != lstInvoiceNumber.size()) { throw new
	 * IllegalArgumentException(
	 * "lstSaleOrderId.size() != lstInvoiceNumber.size()"); } if
	 * (lstSaleOrderId.size() != listCustPayment.size()) { throw new
	 * IllegalArgumentException(
	 * "lstSaleOrderId.size() != listCustPayment.size()"); } Boolean
	 * checkSaleOrder = saleOrderDAO.checkWithoutSaleOrderOfShop(shopId,
	 * lstSaleOrderId); if (checkSaleOrder == true) { throw new
	 * IllegalArgumentException
	 * ("request parameter is not correct, sale_order_id not belong of shop"); }
	 * List<SaleOrder> listSaleOrder = new ArrayList<SaleOrder>(); List<Invoice>
	 * listInvoice = new ArrayList<Invoice>(); for (int i = 0, length =
	 * lstSaleOrderId.size(); i < length; i++) { InvoiceCustPayment custPayment
	 * = listCustPayment.get(i); SaleOrder saleOrderUpdate =
	 * saleOrderDAO.getSaleOrderById(lstSaleOrderId.get(i)); Invoice
	 * invoiceUpdate =
	 * invoiceDAO.getInvoiceByInvoiceNumber(lstInvoiceNumber.get(i), shopId);
	 * if(invoiceUpdate != null &&
	 * InvoiceStatus.MODIFIED.getValue().equals(invoiceUpdate
	 * .getStatus().getValue())) { //trung voi invoice co status = 1
	 * if(saleOrderUpdate.getInvoiceNumber() != null) { //da co hoa don -> cap
	 * nhat lai thong tin hoa don: chuyen hoa don cu thanh hoa don co status = 1
	 * Invoice currentInvoice = saleOrderUpdate.getInvoice();
	 * currentInvoice.setStatus(InvoiceStatus.MODIFIED);
	 * listInvoice.add(currentInvoice);
	 * invoiceUpdate.setOrderDate(saleOrderUpdate.getOrderDate());
	 * invoiceUpdate.setVat(saleOrderUpdate.getVat());
	 * invoiceUpdate.setAmount(saleOrderUpdate.getAmount()); if
	 * (saleOrderUpdate.getAmount() != null) { Float tax =
	 * saleOrderUpdate.getAmount().floatValue(); tax = tax *
	 * (saleOrderUpdate.getVat()) / 110; invoiceUpdate.setTaxAmount(new
	 * BigDecimal(tax)); } if (saleOrderUpdate.getStaff() != null) {
	 * invoiceUpdate.setSelName(saleOrderUpdate.getStaff().getStaffName()); } if
	 * (saleOrderUpdate.getDelivery() != null) {
	 * invoiceUpdate.setDeliveryName(saleOrderUpdate.getDelivery()
	 * .getStaffName()); }
	 * invoiceUpdate.setOrderNumber(saleOrderUpdate.getOrderNumber());
	 * invoiceUpdate.setShop(saleOrderUpdate.getShop()); Long sku =
	 * saleOrderDetailDAO.getSku(saleOrderUpdate.getId());
	 * invoiceUpdate.setSku(sku);// lay trong sale_order_detail Customer
	 * updateSOCustomer = saleOrderUpdate.getCustomer(); if(updateSOCustomer !=
	 * null) { invoiceUpdate.setCustomerCode(updateSOCustomer.getShortCode());
	 * invoiceUpdate.setCustName(updateSOCustomer.getCustomerName());
	 * invoiceUpdate.setCustomerAddr(updateSOCustomer.getAddress());
	 * invoiceUpdate.setCustTaxNumber(updateSOCustomer.getInvoiceTax());
	 * invoiceUpdate.setCustDelAddr(updateSOCustomer.getDeliveryAddress());
	 * invoiceUpdate.setCustBankName(updateSOCustomer.getInvoiceNameBank());
	 * invoiceUpdate
	 * .setCustBankAccount(updateSOCustomer.getInvoiceNumberAccount()); }
	 * invoiceUpdate.setStaff(saleOrderUpdate.getStaff()); Shop updateSOShop =
	 * saleOrderUpdate.getShop(); if (updateSOShop != null) {
	 * invoiceUpdate.setShop(updateSOShop);
	 * invoiceUpdate.setCpnyName(updateSOShop.getShopName());
	 * invoiceUpdate.setCpnyAddr(updateSOShop.getAddress());
	 * invoiceUpdate.setCpnyBankName(updateSOShop .getInvoiceBankName());
	 * invoiceUpdate.setCpnyBankAccount(updateSOShop
	 * .getInvoiceNumberAccount()); invoiceUpdate.setCpnyTaxNumber(updateSOShop
	 * .getTaxNum()); invoiceUpdate.setCpnyPhone(updateSOShop.getPhone()); }
	 * invoiceUpdate.setStatus(InvoiceStatus.USING);
	 * invoiceUpdate.setCustPayment(custPayment);
	 * invoiceUpdate.setUpdateUser(username);
	 * invoiceUpdate.setUpdateDate(commonDAO.getSysDate());
	 * invoiceUpdate.setInvoiceDate(commonDAO.getSysDate());
	 * listInvoice.add(invoiceUpdate);
	 * saleOrderUpdate.setInvoice(invoiceUpdate);
	 * saleOrderUpdate.setInvoiceNumber(invoiceUpdate.getInvoiceNumber());
	 * listSaleOrder.add(saleOrderUpdate); } else { //thÃªm má»›i -> cap nhat lai
	 * hoa don trong bang bi trung
	 * invoiceUpdate.setOrderDate(saleOrderUpdate.getOrderDate());
	 * invoiceUpdate.setVat(saleOrderUpdate.getVat());
	 * invoiceUpdate.setAmount(saleOrderUpdate.getAmount()); if
	 * (saleOrderUpdate.getAmount() != null) { Float tax =
	 * saleOrderUpdate.getAmount().floatValue(); tax = tax *
	 * (saleOrderUpdate.getVat()) / 110; invoiceUpdate.setTaxAmount(new
	 * BigDecimal(tax)); } if (saleOrderUpdate.getStaff() != null) {
	 * invoiceUpdate.setSelName(saleOrderUpdate.getStaff().getStaffName()); } if
	 * (saleOrderUpdate.getDelivery() != null) {
	 * invoiceUpdate.setDeliveryName(saleOrderUpdate.getDelivery()
	 * .getStaffName()); }
	 * invoiceUpdate.setOrderNumber(saleOrderUpdate.getOrderNumber());
	 * invoiceUpdate.setShop(saleOrderUpdate.getShop()); Long sku =
	 * saleOrderDetailDAO.getSku(saleOrderUpdate.getId());
	 * invoiceUpdate.setSku(sku);// lay trong sale_order_detail Customer
	 * updateSOCustomer = saleOrderUpdate.getCustomer(); if(updateSOCustomer !=
	 * null) { invoiceUpdate.setCustomerCode(updateSOCustomer.getShortCode());
	 * invoiceUpdate.setCustName(updateSOCustomer.getCustomerName());
	 * invoiceUpdate.setCustomerAddr(updateSOCustomer.getAddress());
	 * invoiceUpdate.setCustTaxNumber(updateSOCustomer.getInvoiceTax());
	 * invoiceUpdate.setCustDelAddr(updateSOCustomer.getDeliveryAddress());
	 * invoiceUpdate.setCustBankName(updateSOCustomer.getInvoiceNameBank());
	 * invoiceUpdate
	 * .setCustBankAccount(updateSOCustomer.getInvoiceNumberAccount()); }
	 * invoiceUpdate.setStaff(saleOrderUpdate.getStaff()); Shop updateSOShop =
	 * saleOrderUpdate.getShop(); if (updateSOShop != null) {
	 * invoiceUpdate.setShop(updateSOShop);
	 * invoiceUpdate.setCpnyName(updateSOShop.getShopName());
	 * invoiceUpdate.setCpnyAddr(updateSOShop.getAddress());
	 * invoiceUpdate.setCpnyBankName(updateSOShop .getInvoiceBankName());
	 * invoiceUpdate.setCpnyBankAccount(updateSOShop
	 * .getInvoiceNumberAccount()); invoiceUpdate.setCpnyTaxNumber(updateSOShop
	 * .getTaxNum()); invoiceUpdate.setCpnyPhone(updateSOShop.getPhone()); }
	 * invoiceUpdate.setCustPayment(custPayment);
	 * invoiceUpdate.setStatus(InvoiceStatus.USING);
	 * invoiceUpdate.setUpdateUser(username);
	 * invoiceUpdate.setUpdateDate(commonDAO.getSysDate());
	 * invoiceUpdate.setInvoiceDate(commonDAO.getSysDate());
	 * listInvoice.add(invoiceUpdate);
	 * saleOrderUpdate.setInvoice(invoiceUpdate);
	 * saleOrderUpdate.setInvoiceNumber(invoiceUpdate.getInvoiceNumber());
	 * listSaleOrder.add(saleOrderUpdate); } } else {
	 * if(saleOrderUpdate.getInvoiceNumber() != null) { //da co hoa don -> them
	 * moi hoa don thanh hoa don co status = 1. roi chuyen hoa don cu thanh
	 * status = 1 Invoice newInvoice = new Invoice();
	 * newInvoice.setStatus(InvoiceStatus.USING);
	 * newInvoice.setInvoiceNumber(lstInvoiceNumber.get(i).toUpperCase());
	 * Invoice currentInvoice = saleOrderUpdate.getInvoice();
	 * currentInvoice.setStatus(InvoiceStatus.MODIFIED);
	 * listInvoice.add(currentInvoice);
	 * 
	 * newInvoice.setOrderDate(saleOrderUpdate.getOrderDate());
	 * newInvoice.setVat(saleOrderUpdate.getVat());
	 * newInvoice.setAmount(saleOrderUpdate.getAmount()); if
	 * (saleOrderUpdate.getAmount() != null) { Float tax =
	 * saleOrderUpdate.getAmount().floatValue(); tax = tax *
	 * (saleOrderUpdate.getVat()) / 110; newInvoice.setTaxAmount(new
	 * BigDecimal(tax)); } if (saleOrderUpdate.getStaff() != null) {
	 * newInvoice.setSelName(saleOrderUpdate.getStaff().getStaffName()); } if
	 * (saleOrderUpdate.getDelivery() != null) {
	 * newInvoice.setDeliveryName(saleOrderUpdate.getDelivery()
	 * .getStaffName()); }
	 * newInvoice.setOrderNumber(saleOrderUpdate.getOrderNumber());
	 * newInvoice.setShop(saleOrderUpdate.getShop()); Long sku =
	 * saleOrderDetailDAO.getSku(saleOrderUpdate.getId());
	 * newInvoice.setSku(sku);// lay trong sale_order_detail Customer
	 * updateSOCustomer = saleOrderUpdate.getCustomer(); if(updateSOCustomer !=
	 * null) { newInvoice.setCustomerCode(updateSOCustomer.getShortCode());
	 * newInvoice.setCustName(updateSOCustomer.getCustomerName());
	 * newInvoice.setCustomerAddr(updateSOCustomer.getAddress());
	 * newInvoice.setCustTaxNumber(updateSOCustomer.getInvoiceTax());
	 * newInvoice.setCustDelAddr(updateSOCustomer.getDeliveryAddress());
	 * newInvoice.setCustBankName(updateSOCustomer.getInvoiceNameBank());
	 * newInvoice
	 * .setCustBankAccount(updateSOCustomer.getInvoiceNumberAccount()); }
	 * newInvoice.setStaff(saleOrderUpdate.getStaff()); Shop updateSOShop =
	 * saleOrderUpdate.getShop(); if (updateSOShop != null) {
	 * newInvoice.setShop(updateSOShop);
	 * newInvoice.setCpnyName(updateSOShop.getShopName());
	 * newInvoice.setCpnyAddr(updateSOShop.getAddress());
	 * newInvoice.setCpnyBankName(updateSOShop .getInvoiceBankName());
	 * newInvoice.setCpnyBankAccount(updateSOShop .getInvoiceNumberAccount());
	 * newInvoice.setCpnyTaxNumber(updateSOShop .getTaxNum());
	 * newInvoice.setCpnyPhone(updateSOShop.getPhone()); }
	 * newInvoice.setCustPayment(custPayment);
	 * newInvoice.setCreateUser(username);
	 * newInvoice.setInvoiceDate(commonDAO.getSysDate()); Invoice _newInvoice =
	 * invoiceDAO.createInvoice(newInvoice);
	 * saleOrderUpdate.setInvoice(_newInvoice);
	 * saleOrderUpdate.setInvoiceNumber(_newInvoice.getInvoiceNumber());
	 * listSaleOrder.add(saleOrderUpdate); } else { //them moi hoa don Invoice
	 * newInvoice = new Invoice(); newInvoice.setStatus(InvoiceStatus.USING);
	 * newInvoice.setInvoiceNumber(lstInvoiceNumber.get(i).toUpperCase());
	 * 
	 * newInvoice.setOrderDate(saleOrderUpdate.getOrderDate());
	 * newInvoice.setVat(saleOrderUpdate.getVat());
	 * newInvoice.setAmount(saleOrderUpdate.getAmount()); if
	 * (saleOrderUpdate.getAmount() != null) { Float tax =
	 * saleOrderUpdate.getAmount().floatValue(); tax = tax *
	 * (saleOrderUpdate.getVat()) / 110; newInvoice.setTaxAmount(new
	 * BigDecimal(tax)); } if (saleOrderUpdate.getStaff() != null) {
	 * newInvoice.setSelName(saleOrderUpdate.getStaff().getStaffName()); } if
	 * (saleOrderUpdate.getDelivery() != null) {
	 * newInvoice.setDeliveryName(saleOrderUpdate.getDelivery()
	 * .getStaffName()); }
	 * newInvoice.setOrderNumber(saleOrderUpdate.getOrderNumber());
	 * newInvoice.setShop(saleOrderUpdate.getShop()); Long sku =
	 * saleOrderDetailDAO.getSku(saleOrderUpdate.getId());
	 * newInvoice.setSku(sku);// lay trong sale_order_detail Customer
	 * updateSOCustomer = saleOrderUpdate.getCustomer(); if(updateSOCustomer !=
	 * null) { newInvoice.setCustomerCode(updateSOCustomer.getShortCode());
	 * newInvoice.setCustName(updateSOCustomer.getCustomerName());
	 * newInvoice.setCustomerAddr(updateSOCustomer.getAddress());
	 * newInvoice.setCustTaxNumber(updateSOCustomer.getInvoiceTax());
	 * newInvoice.setCustDelAddr(updateSOCustomer.getDeliveryAddress());
	 * newInvoice.setCustBankName(updateSOCustomer.getInvoiceNameBank());
	 * newInvoice
	 * .setCustBankAccount(updateSOCustomer.getInvoiceNumberAccount()); }
	 * newInvoice.setStaff(saleOrderUpdate.getStaff()); Shop updateSOShop =
	 * saleOrderUpdate.getShop(); if (updateSOShop != null) {
	 * newInvoice.setShop(updateSOShop);
	 * newInvoice.setCpnyName(updateSOShop.getShopName());
	 * newInvoice.setCpnyAddr(updateSOShop.getAddress());
	 * newInvoice.setCpnyBankName(updateSOShop .getInvoiceBankName());
	 * newInvoice.setCpnyBankAccount(updateSOShop .getInvoiceNumberAccount());
	 * newInvoice.setCpnyTaxNumber(updateSOShop .getTaxNum());
	 * newInvoice.setCpnyPhone(updateSOShop.getPhone()); }
	 * newInvoice.setCustPayment(custPayment);
	 * newInvoice.setCreateUser(username);
	 * newInvoice.setInvoiceDate(commonDAO.getSysDate()); Invoice _newInvoice =
	 * invoiceDAO.createInvoice(newInvoice);
	 * saleOrderUpdate.setInvoice(_newInvoice);
	 * saleOrderUpdate.setInvoiceNumber(_newInvoice.getInvoiceNumber());
	 * listSaleOrder.add(saleOrderUpdate); } } }
	 * saleOrderDAO.updateSaleOrder(listSaleOrder);
	 * invoiceDAO.updateInvoice(listInvoice); } catch (DataAccessException e) {
	 * throw new BusinessException(e); } }
	 */

	/*
	 * @Override public List<SaleOrder> getListSaleOrderWithInvoice(Long shopId,
	 * String saleStaffCode, String deliveryStaffCode, Float vat, String
	 * orderNumber, Date fromDate, Date toDate, String customerCode,
	 * InvoiceCustPayment custPayment) throws BusinessException { try { return
	 * saleOrderDAO.getListSaleOrderWithInvoice(shopId, saleStaffCode,
	 * deliveryStaffCode, vat, orderNumber, fromDate, toDate, customerCode,
	 * custPayment); } catch (DataAccessException e) { throw new
	 * BusinessException(e); } }
	 */

	@Override
	public List<SaleOrderVOEx> getListRptSaleOrderWithInvoice(List<Long> lstSaleOrderId) throws BusinessException {
		if (lstSaleOrderId == null || lstSaleOrderId.size() == 0) {
			throw new IllegalArgumentException("lstSaleOrderId is null");
		}
		List<SaleOrderVOEx> lstSaleOrderVOEx = new ArrayList<SaleOrderVOEx>();
		try {
			ApParam apParamVATMax = apParamDAO.getApParamByCode("MAXVAT", ActiveType.RUNNING);
			int vatMax = Integer.valueOf(apParamVATMax.getValue());
			List<SaleOrder> lstSO = saleOrderDAO.getListSaleOrder(lstSaleOrderId);
			for (SaleOrder so : lstSO) {
				List<SaleOrderDetail> listSOD1 = saleOrderDetailDAO.getListSaleOrderDetailBySOIDOrderByVat1(so.getId(), null);
				if (listSOD1.size() == 0) {
					continue;
				}
				int index = 1;
				if (listSOD1.size() > vatMax) {
					int iRun = 0;
					SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
					saleOrderVOEx.setRowId(so.getId() + "-" + index);
					saleOrderVOEx.setSaleOrder(so);
					saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
					saleOrderVOEx.setAmount(BigDecimal.ZERO);
					saleOrderVOEx.setInvoice(listSOD1.get(0).getInvoice());
					saleOrderVOEx.setCustPayment(listSOD1.get(0).getInvoice().getCustPayment());
					lstSaleOrderVOEx.add(saleOrderVOEx);
					Float currentVAT = listSOD1.get(0).getVat();
					for (int i = 0; i < listSOD1.size(); i++) {
						if (iRun == vatMax) {
							// tach lun
							index++;
							saleOrderVOEx = new SaleOrderVOEx();
							saleOrderVOEx.setRowId(so.getId() + "-" + index);
							saleOrderVOEx.setSaleOrder(so);
							saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
							saleOrderVOEx.setAmount(BigDecimal.ZERO);
							saleOrderVOEx.setInvoice(listSOD1.get(i).getInvoice());
							saleOrderVOEx.setCustPayment(listSOD1.get(i).getInvoice().getCustPayment());
							lstSaleOrderVOEx.add(saleOrderVOEx);
							currentVAT = listSOD1.get(i).getVat();
							iRun = 1;
						} else {
							if (!currentVAT.equals(listSOD1.get(i).getVat())) {
								// tach
								index++;
								saleOrderVOEx = new SaleOrderVOEx();
								saleOrderVOEx.setRowId(so.getId() + "-" + index);
								saleOrderVOEx.setSaleOrder(so);
								saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
								saleOrderVOEx.setAmount(BigDecimal.ZERO);
								saleOrderVOEx.setInvoice(listSOD1.get(i).getInvoice());
								saleOrderVOEx.setCustPayment(listSOD1.get(i).getInvoice().getCustPayment());
								lstSaleOrderVOEx.add(saleOrderVOEx);
								currentVAT = listSOD1.get(i).getVat();
								iRun = 1;
							} else {
								iRun++;
							}
						}
						saleOrderVOEx.getLstSaleOrderDetail().add(listSOD1.get(i));
						saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(listSOD1.get(i).getAmount()));
					}
				} else {
					SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
					saleOrderVOEx.setRowId(so.getId() + "-" + index);
					saleOrderVOEx.setSaleOrder(so);
					saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
					saleOrderVOEx.setAmount(BigDecimal.ZERO);
					saleOrderVOEx.setInvoice(listSOD1.get(0).getInvoice());
					saleOrderVOEx.setCustPayment(listSOD1.get(0).getInvoice().getCustPayment());
					lstSaleOrderVOEx.add(saleOrderVOEx);
					Float currentVAT = listSOD1.get(0).getVat();
					for (int i = 0; i < listSOD1.size(); i++) {
						if (!currentVAT.equals(listSOD1.get(i).getVat())) {
							// tien hanh tach hd
							index++;
							saleOrderVOEx = new SaleOrderVOEx();
							saleOrderVOEx.setRowId(so.getId() + "-" + index);
							saleOrderVOEx.setSaleOrder(so);
							saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
							saleOrderVOEx.setAmount(BigDecimal.ZERO);
							saleOrderVOEx.setInvoice(listSOD1.get(i).getInvoice());
							saleOrderVOEx.setCustPayment(listSOD1.get(i).getInvoice().getCustPayment());
							lstSaleOrderVOEx.add(saleOrderVOEx);
							currentVAT = listSOD1.get(i).getVat();
						}
						saleOrderVOEx.getLstSaleOrderDetail().add(listSOD1.get(i));
						saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(listSOD1.get(i).getAmount()));
					}
				}
				index++;
				List<SaleOrderDetail> listSOD2 = saleOrderDetailDAO.getListSaleOrderDetailBySOIDOrderByVat2(so.getId(), null);
				List<SaleOrderDetail> listLastSaleSOD = lstSaleOrderVOEx.get(lstSaleOrderVOEx.size() - 1).getLstSaleOrderDetail();
				if (listSOD2.size() > 0) {
					if (listSOD2.size() + listLastSaleSOD.size() <= vatMax - 1) {
						// nhet vao cai cuoi cung cho du so luong
						for (SaleOrderDetail sod : listSOD2) {
							listLastSaleSOD.add(sod);
						}
					} else {
						if (listLastSaleSOD.size() == vatMax - 1) {
							// tao moi SaleOrderVOEx roi tach listSOD2
							SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
							saleOrderVOEx.setRowId(so.getId() + "-" + index);
							saleOrderVOEx.setSaleOrder(so);
							saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
							saleOrderVOEx.setAmount(BigDecimal.ZERO);
							saleOrderVOEx.setInvoice(listSOD2.get(0).getInvoice());
							saleOrderVOEx.setCustPayment(listSOD2.get(0).getInvoice().getCustPayment());
							lstSaleOrderVOEx.add(saleOrderVOEx);
							int iRun = 0;
							for (int i = 0; i < listSOD2.size(); i++) {
								if (iRun == vatMax - 1) {
									// tach luon
									index++;
									saleOrderVOEx = new SaleOrderVOEx();
									saleOrderVOEx.setRowId(so.getId() + "-" + index);
									saleOrderVOEx.setSaleOrder(so);
									saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
									saleOrderVOEx.setAmount(BigDecimal.ZERO);
									saleOrderVOEx.setInvoice(listSOD2.get(i).getInvoice());
									saleOrderVOEx.setCustPayment(listSOD2.get(i).getInvoice().getCustPayment());
									lstSaleOrderVOEx.add(saleOrderVOEx);
									iRun = 1;
								} else {
									// ko tach
									iRun++;
								}
								saleOrderVOEx.getLstSaleOrderDetail().add(listSOD2.get(i));
							}
						} else {
							// listLastSaleSOD.size() < vatMax
							// nhet vao cho listLastSaleSOD cho du so luong roi
							// tach nhung thu con lai
							int ii = 0;
							for (; ii < listSOD2.size(); ii++) {
								if (listLastSaleSOD.size() == vatMax - 1) {
									break;
								} else {
									listLastSaleSOD.add(listSOD2.get(ii));
								}
							}
							index++;
							SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
							saleOrderVOEx.setRowId(so.getId() + "-" + index);
							saleOrderVOEx.setSaleOrder(so);
							saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
							saleOrderVOEx.setAmount(BigDecimal.ZERO);
							saleOrderVOEx.setInvoice(listSOD2.get(ii).getInvoice());
							saleOrderVOEx.setCustPayment(listSOD2.get(ii).getInvoice().getCustPayment());
							lstSaleOrderVOEx.add(saleOrderVOEx);
							int iRun = 0;
							for (; ii < listSOD2.size(); ii++) {
								// tao moi SaleOrderVOEx moi
								if (iRun == vatMax - 1) {
									index++;
									saleOrderVOEx = new SaleOrderVOEx();
									saleOrderVOEx.setRowId(so.getId() + "-" + index);
									saleOrderVOEx.setSaleOrder(so);
									saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
									saleOrderVOEx.setAmount(BigDecimal.ZERO);
									saleOrderVOEx.setInvoice(listSOD2.get(ii).getInvoice());
									saleOrderVOEx.setCustPayment(listSOD2.get(ii).getInvoice().getCustPayment());
									lstSaleOrderVOEx.add(saleOrderVOEx);
									iRun = 1;
								} else {
									iRun++;
								}
								saleOrderVOEx.getLstSaleOrderDetail().add(listSOD2.get(ii));
							}
						}
					}
				}

				//				List<SaleOrderDetail> listSOD3 = saleOrderDetailDAO
				//						.getListSaleOrderDetailBySOIDOrderByVat3(so.getId(),
				//								null);
				//				listLastSaleSOD = lstSaleOrderVOEx.get(
				//						lstSaleOrderVOEx.size() - 1).getLstSaleOrderDetail();
				//				if (listSOD3.size() > 0) {
				//					if (listSOD3.size() + listLastSaleSOD.size() <= vatMax) {
				//						// nhet vao cai cuoi cung cho du so luong
				//						for (SaleOrderDetail sod : listSOD3) {
				//							listLastSaleSOD.add(sod);
				//						}
				//					} else {
				//						if (listLastSaleSOD.size() == vatMax) {
				//							index++;
				//							SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
				//							saleOrderVOEx.setRowId(so.getId() + "-" + index);
				//							saleOrderVOEx.setSaleOrder(so);
				//							saleOrderVOEx
				//									.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
				//							saleOrderVOEx.setAmount(BigDecimal.ZERO);
				//							saleOrderVOEx.setInvoice(listSOD3.get(0)
				//									.getInvoice());
				//							saleOrderVOEx.setCustPayment(listSOD3.get(0)
				//									.getInvoice().getCustPayment());
				//							lstSaleOrderVOEx.add(saleOrderVOEx);
				//							for (SaleOrderDetail sod : listSOD3) {
				//								saleOrderVOEx.getLstSaleOrderDetail().add(sod);
				//							}
				//						} else {
				//							for (SaleOrderDetail sod : listSOD3) {
				//								listLastSaleSOD.add(sod);
				//							}
				//						}
				//					}
				//				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return lstSaleOrderVOEx;
	}

	/*
	 * @Override public List<SaleOrderVOEx> getListSaleOrderWithInvoiceEx1(Long
	 * shopId, String saleStaffCode, String deliveryStaffCode, Float vat, String
	 * orderNumber, Date fromDate, Date toDate, String customerCode,
	 * InvoiceCustPayment custPayment) throws BusinessException {
	 * List<SaleOrderVOEx> lstSaleOrderVOEx = new ArrayList<SaleOrderVOEx>();
	 * try { ApParam apParamVATMax = apParamDAO.getApParamByCode("MAXVAT",
	 * ActiveType.RUNNING); int vatMax =
	 * Integer.valueOf(apParamVATMax.getValue()); List<SaleOrder> lstSO =
	 * saleOrderDAO.getListSaleOrderWithInvoiceEx1(shopId, saleStaffCode,
	 * deliveryStaffCode, vat, orderNumber, fromDate, toDate, customerCode,
	 * custPayment); for(SaleOrder so : lstSO) { List<SaleOrderDetail> listSOD1
	 * = saleOrderDetailDAO.getListSaleOrderDetailBySOIDOrderByVat1(so.getId(),
	 * null); if(listSOD1.size() == 0) { continue; } int index = 1;
	 * if(listSOD1.size() > vatMax) { int iRun = 0; SaleOrderVOEx saleOrderVOEx
	 * = new SaleOrderVOEx(); saleOrderVOEx.setRowId(so.getId()+"-"+index);
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setInvoice(listSOD1.get(0).getInvoice());
	 * saleOrderVOEx.setCustPayment
	 * (listSOD1.get(0).getInvoice().getCustPayment());
	 * lstSaleOrderVOEx.add(saleOrderVOEx); Float currentVAT =
	 * listSOD1.get(0).getVat(); for(int i = 0; i < listSOD1.size(); i++) {
	 * if(iRun == vatMax) { //tach lun index++; saleOrderVOEx = new
	 * SaleOrderVOEx(); saleOrderVOEx.setRowId(so.getId()+"-"+index);
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setInvoice(listSOD1.get(i).getInvoice());
	 * saleOrderVOEx.setCustPayment
	 * (listSOD1.get(i).getInvoice().getCustPayment());
	 * lstSaleOrderVOEx.add(saleOrderVOEx); currentVAT =
	 * listSOD1.get(i).getVat(); iRun = 1; } else {
	 * if(!currentVAT.equals(listSOD1.get(i).getVat())) { //tach index++;
	 * saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setRowId(so.getId()+"-"+index);
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setInvoice(listSOD1.get(i).getInvoice());
	 * saleOrderVOEx.setCustPayment
	 * (listSOD1.get(i).getInvoice().getCustPayment());
	 * lstSaleOrderVOEx.add(saleOrderVOEx); currentVAT =
	 * listSOD1.get(i).getVat(); iRun = 1; } else { iRun++; } }
	 * saleOrderVOEx.getLstSaleOrderDetail().add(listSOD1.get(i));
	 * saleOrderVOEx.
	 * setAmount(saleOrderVOEx.getAmount().add(listSOD1.get(i).getAmount())); }
	 * } else { SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setRowId(so.getId()+"-"+index);
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setInvoice(listSOD1.get(0).getInvoice());
	 * saleOrderVOEx.setCustPayment
	 * (listSOD1.get(0).getInvoice().getCustPayment());
	 * lstSaleOrderVOEx.add(saleOrderVOEx); Float currentVAT =
	 * listSOD1.get(0).getVat(); for(int i = 0; i < listSOD1.size(); i++) {
	 * if(!currentVAT.equals(listSOD1.get(i).getVat())) { //tien hanh tach hd
	 * index++; saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setRowId(so.getId()+"-"+index);
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setInvoice(listSOD1.get(i).getInvoice());
	 * saleOrderVOEx.setCustPayment
	 * (listSOD1.get(i).getInvoice().getCustPayment());
	 * lstSaleOrderVOEx.add(saleOrderVOEx); currentVAT =
	 * listSOD1.get(i).getVat(); }
	 * saleOrderVOEx.getLstSaleOrderDetail().add(listSOD1.get(i));
	 * saleOrderVOEx.
	 * setAmount(saleOrderVOEx.getAmount().add(listSOD1.get(i).getAmount())); }
	 * } index++; List<SaleOrderDetail> listSOD2 =
	 * saleOrderDetailDAO.getListSaleOrderDetailBySOIDOrderByVat2(so.getId(),
	 * null); List<SaleOrderDetail> listLastSaleSOD =
	 * lstSaleOrderVOEx.get(lstSaleOrderVOEx.size() -
	 * 1).getLstSaleOrderDetail(); if(listSOD2.size() > 0) { if(listSOD2.size()
	 * + listLastSaleSOD.size() <= vatMax) { //nhet vao cai cuoi cung cho du so
	 * luong for (SaleOrderDetail sod : listSOD2) { listLastSaleSOD.add(sod); }
	 * } else { if(listLastSaleSOD.size() == vatMax) { //tao moi SaleOrderVOEx
	 * roi tach listSOD2 SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setRowId(so.getId()+"-"+index);
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setInvoice(listSOD2.get(0).getInvoice());
	 * saleOrderVOEx.setCustPayment
	 * (listSOD2.get(0).getInvoice().getCustPayment());
	 * lstSaleOrderVOEx.add(saleOrderVOEx); int iRun = 0; for(int i = 0; i <
	 * listSOD2.size(); i++) { if(iRun == vatMax) { //tach luon index++;
	 * saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setRowId(so.getId()+"-"+index);
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setInvoice(listSOD2.get(i).getInvoice());
	 * saleOrderVOEx.setCustPayment
	 * (listSOD2.get(i).getInvoice().getCustPayment());
	 * lstSaleOrderVOEx.add(saleOrderVOEx); iRun = 1; } else { //ko tach iRun++;
	 * } saleOrderVOEx.getLstSaleOrderDetail().add(listSOD2.get(i)); } } else {
	 * //listLastSaleSOD.size() < vatMax //nhet vao cho listLastSaleSOD cho du
	 * so luong roi tach nhung thu con lai int ii = 0; for(; ii <
	 * listSOD2.size(); ii++){ if(listLastSaleSOD.size() == vatMax) { break; }
	 * else { listLastSaleSOD.add(listSOD2.get(ii)); } } index++; SaleOrderVOEx
	 * saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setRowId(so.getId()+"-"+index);
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setInvoice(listSOD2.get(ii).getInvoice());
	 * saleOrderVOEx.setCustPayment
	 * (listSOD2.get(ii).getInvoice().getCustPayment());
	 * lstSaleOrderVOEx.add(saleOrderVOEx); int iRun = 0; for(; ii <
	 * listSOD2.size(); ii++) { //tao moi SaleOrderVOEx moi if(iRun == vatMax) {
	 * index++; saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setRowId(so.getId()+"-"+index);
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setInvoice(listSOD2.get(ii).getInvoice());
	 * saleOrderVOEx.setCustPayment
	 * (listSOD2.get(ii).getInvoice().getCustPayment());
	 * lstSaleOrderVOEx.add(saleOrderVOEx); iRun = 1; } else { iRun++; }
	 * saleOrderVOEx.getLstSaleOrderDetail().add(listSOD2.get(ii)); } } } }
	 * 
	 * List<SaleOrderDetail> listSOD3 =
	 * saleOrderDetailDAO.getListSaleOrderDetailBySOIDOrderByVat3(so.getId(),
	 * null); listLastSaleSOD = lstSaleOrderVOEx.get(lstSaleOrderVOEx.size() -
	 * 1).getLstSaleOrderDetail(); if(listSOD3.size() > 0) { if(listSOD3.size()
	 * + listLastSaleSOD.size() <= vatMax) { //nhet vao cai cuoi cung cho du so
	 * luong for (SaleOrderDetail sod : listSOD3) { listLastSaleSOD.add(sod); }
	 * } else { if(listLastSaleSOD.size() == vatMax) { index++; SaleOrderVOEx
	 * saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setRowId(so.getId()+"-"+index);
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setInvoice(listSOD3.get(0).getInvoice());
	 * saleOrderVOEx.setCustPayment
	 * (listSOD3.get(0).getInvoice().getCustPayment());
	 * lstSaleOrderVOEx.add(saleOrderVOEx); for (SaleOrderDetail sod : listSOD3)
	 * { saleOrderVOEx.getLstSaleOrderDetail().add(sod); } } else { for
	 * (SaleOrderDetail sod : listSOD3) { listLastSaleSOD.add(sod); } } } } } }
	 * catch (DataAccessException e) { throw new BusinessException(e); } return
	 * lstSaleOrderVOEx; }
	 */

	@Override
	public List<Invoice> getListInvoiceByCondition(KPaging<Invoice> kPaging, String orderNumber, String redInvoiceNumber, String staffCode, String deliveryCode, String shortCode, Date fromDate, Date toDate, Float vat, InvoiceCustPayment custPayment,
			Date lockDate, Long shopId, Integer isValueOrder, Boolean isVATGop, Integer numberValueOrder) throws BusinessException {
		try {
			return invoiceDAO.getListInvoiceByConditionByNumber(kPaging, vat, orderNumber, redInvoiceNumber, fromDate, toDate, staffCode, deliveryCode, shortCode, custPayment, lockDate, shopId, isValueOrder, isVATGop, numberValueOrder);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	//lay danh sach don hang VAT de gop
	@Override
	public List<SaleOrder> getListSaleOrderWithNoInvoiceGOP(KPaging<SaleOrderVOEx> kPaging, Long shopId, String saleStaffCode, String deliveryStaffCode, String orderNumber, Date fromDate, Date toDate, String customerCode, Date lockDate,
			Integer isValueOrder, Integer numberValueOrder) throws BusinessException {
		List<SaleOrder> lstSO = new ArrayList<SaleOrder>();
		try {
			lstSO = saleOrderDAO.getListSaleOrderWithNoInvoiceEx1(null, shopId, saleStaffCode, deliveryStaffCode, orderNumber, fromDate, toDate, customerCode, lockDate, isValueOrder, 0, numberValueOrder);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return lstSO;
	}

	@Override
	public List<SaleOrderVOEx> getListSaleOrderWithNoInvoiceEx(KPaging<SaleOrderVOEx> kPaging, Long shopId, String saleStaffCode, String deliveryStaffCode, String orderNumber, Date fromDate, Date toDate, String customerCode, Date lockDate,
			Integer isValueOrder, Integer numberValueOrder) throws BusinessException {
		List<SaleOrderVOEx> lstSaleOrderVOEx = new ArrayList<SaleOrderVOEx>();
		try {
			ApParam apParamVATMax = apParamDAO.getApParamByCode("MAXVAT", ActiveType.RUNNING);
			int vatMax = Integer.valueOf(apParamVATMax.getValue());
			List<SaleOrder> lstSO = saleOrderDAO.getListSaleOrderWithNoInvoiceEx1(null, shopId, saleStaffCode, deliveryStaffCode, orderNumber, fromDate, toDate, customerCode, lockDate, isValueOrder, 1, numberValueOrder);
			List<SaleOrderDetail> lstAllSOD = saleOrderDAO.getListSaleOrderWithNoInvoiceEx1SOD(null, shopId, saleStaffCode, deliveryStaffCode, orderNumber, fromDate, toDate, customerCode, lockDate, isValueOrder, numberValueOrder);

			Map<Long, List<SaleOrderDetail>> mapSO = new HashMap<Long, List<SaleOrderDetail>>();
			Long saleOrderId = null;
			for (int i = 0, sizeAll = lstAllSOD.size(); i < sizeAll; i++) {
				if (lstAllSOD.get(i).getSaleOrder().getId() != saleOrderId) {
					saleOrderId = lstAllSOD.get(i).getSaleOrder().getId();
					List<SaleOrderDetail> list = new ArrayList<SaleOrderDetail>();
					list.add(lstAllSOD.get(i));
					mapSO.put(saleOrderId, list);
				} else {
					mapSO.get(saleOrderId).add(lstAllSOD.get(i));
				}
			}
			for (SaleOrder so : lstSO) {
				if (mapSO.get(so.getId()) == null) {
					continue;
				}
				//lay tat ca cac sod tru sod khuyen mai tien
				//List<SaleOrderDetail> listSOD = saleOrderDetailDAO.getListAllSaleOrderDetail(so.getId(), null, so.getOrderDate());
				List<SaleOrderDetail> listSOD = gopSanPham(mapSO.get(so.getId()), 1);
				//tach thanh cac list co VAT khac nhau
				List<List<SaleOrderDetail>> listVat = new ArrayList<List<SaleOrderDetail>>();
				List<SaleOrderDetail> tmp = new ArrayList<SaleOrderDetail>();
				listVat.add(tmp);
				if (listSOD != null && listSOD.size() > 0) {
					// tach theo VAT
					Float currentVAT = null;
					for (int i = 0, size = listSOD.size(); i < size; i++) {
						if (listSOD.get(i).getVat() != null && listSOD.get(i).getVat() > 0) {
							if (currentVAT == null) {
								tmp.add(listSOD.get(i));
								currentVAT = listSOD.get(i).getVat();
							} else if (!currentVAT.equals(listSOD.get(i).getVat())) {
								tmp = new ArrayList<SaleOrderDetail>();
								listVat.add(tmp);
								currentVAT = listSOD.get(i).getVat();
								tmp.add(listSOD.get(i));
							} else {
								tmp.add(listSOD.get(i));
							}
						} else {
							if (currentVAT != null && currentVAT > 0) {
								tmp = new ArrayList<SaleOrderDetail>();
								listVat.add(tmp);
								currentVAT = listSOD.get(i).getVat();
								tmp.add(listSOD.get(i));
							} else {
								tmp.add(listSOD.get(i));
							}
						}
					}
				}

				for (int i = 0, size1 = listVat.size(); i < size1; i++) {
					int sizeLstSaleOrder1 = lstSaleOrderVOEx.size();
					//moi VAT tach theo km + chiet khau
					List<SaleOrderDetail> lstSPCoCK = new ArrayList<SaleOrderDetail>();
					List<SaleOrderDetail> lstSPKoCoCK = new ArrayList<SaleOrderDetail>(); // co km sp
					List<SaleOrderDetail> lstConLai = new ArrayList<SaleOrderDetail>();// sp ban ko co khuye mai gi het
					List<SaleOrderDetail> listDetail = listVat.get(i);
					for (SaleOrderDetail sod : listDetail) {
						if (sod.getDiscountAmount() != null && sod.getDiscountAmount().doubleValue() > 0) {
							lstSPCoCK.add(sod);
						} else {
							if (sod.getProgramType() != null && sod.getProgramType().equals(ProgramType.AUTO_PROM) && sod.getProgramCode() != null) {
								lstSPKoCoCK.add(sod);
							} else {
								lstConLai.add(sod);
							}
						}
					}

					List<SaleOrderDetail> lstCoCK = new ArrayList<SaleOrderDetail>();// sp ban co ck

					int index = 1;
					BigDecimal ck = BigDecimal.ZERO;
					for (int j = 0; j < lstSPCoCK.size(); j++) {
						lstCoCK.add(lstSPCoCK.get(j));
						if ((index == lstSPCoCK.size()) || (index % (vatMax - 1) == 0)) {
							SaleOrderDetail detailCK = new SaleOrderDetail();
							//							ck = ck.add(lstSPCoCK.get(j).getDiscountAmount());
							detailCK.setDiscountAmount(ck);
							detailCK.setSaleOrder(lstSPCoCK.get(j).getSaleOrder());
							detailCK.setShop(lstSPCoCK.get(j).getShop());
							detailCK.setIsFreeItem(0);
							lstCoCK.add(detailCK);
							//							index= 1;
							index++;
							ck = BigDecimal.ZERO;
						} else {
							//							ck = ck.add(lstSPCoCK.get(j).getDiscountAmount());
							index++;
						}
					}
					List<SaleOrderDetailForInvoidVO> lstSPBanVaKhuyenMai = new ArrayList<SaleOrderDetailForInvoidVO>();
					SaleOrderDetailForInvoidVO tmpSOD = new SaleOrderDetailForInvoidVO();
					lstSPBanVaKhuyenMai.add(tmpSOD);
					String progrCode = "";
					for (SaleOrderDetail sod : lstSPKoCoCK) {
						if (StringUtility.isNullOrEmpty(progrCode)) {
							if (sod.getIsFreeItem().intValue() == 1) {
								tmpSOD.getLstKhuyenMai().add(sod);
							} else {
								tmpSOD.getLstSPBan().add(sod);
							}
							progrCode = sod.getProgramCode();
						} else if (!progrCode.equals(sod.getProgramCode())) {
							tmpSOD = new SaleOrderDetailForInvoidVO();
							lstSPBanVaKhuyenMai.add(tmpSOD);
							progrCode = sod.getProgramCode();
							if (sod.getIsFreeItem().intValue() == 1) {
								tmpSOD.getLstKhuyenMai().add(sod);
							} else {
								tmpSOD.getLstSPBan().add(sod);
							}
						} else {
							if (sod.getIsFreeItem().intValue() == 1) {
								tmpSOD.getLstKhuyenMai().add(sod);
							} else {
								tmpSOD.getLstSPBan().add(sod);
							}
						}
					}
					int idx = 1;
					if (!lstCoCK.isEmpty()) { //xu ly cho nhung dong hang co chiet khau tien ==> tao thanh 1 don vat
						if (lstCoCK.size() <= vatMax) {
							SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
							saleOrderVOEx.setSaleOrder(so);
							saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
							saleOrderVOEx.setAmount(BigDecimal.ZERO);
							saleOrderVOEx.setDiscount(BigDecimal.ZERO);
							saleOrderVOEx.setConLai(vatMax - lstCoCK.size()); //so luong dong hang con co the chua trong 1 don vat
							for (SaleOrderDetail d : lstCoCK) {
								saleOrderVOEx.getLstSaleOrderDetail().add(d);
								saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
								saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
							}
							lstSaleOrderVOEx.add(saleOrderVOEx);
						} else {
							SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
							saleOrderVOEx.setSaleOrder(so);
							saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
							saleOrderVOEx.setAmount(BigDecimal.ZERO);
							saleOrderVOEx.setDiscount(BigDecimal.ZERO);
							saleOrderVOEx.setConLai(vatMax);
							lstSaleOrderVOEx.add(saleOrderVOEx);
							for (SaleOrderDetail d : lstCoCK) {
								if ((idx == lstCoCK.size()) || (idx % vatMax == 0)) {
									if (idx != lstCoCK.size()) {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										saleOrderVOEx = new SaleOrderVOEx();
										saleOrderVOEx.setSaleOrder(so);
										saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
										saleOrderVOEx.setAmount(BigDecimal.ZERO);
										saleOrderVOEx.setDiscount(BigDecimal.ZERO);
										saleOrderVOEx.setConLai(vatMax);
										lstSaleOrderVOEx.add(saleOrderVOEx);
									} else {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
									}
								} else {
									saleOrderVOEx.getLstSaleOrderDetail().add(d);
									saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
									saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
									saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
								}
								idx++;
							}
						}
					}
					//					for(int t=lstSPBanVaKhuyenMai.size()-1;t>=0;t--){
					//						if(!lstSPBanVaKhuyenMai.get(t).getLstKhuyenMai().isEmpty() && lstSPBanVaKhuyenMai.get(t).getLstSPBan().isEmpty()){
					//							lstConLai.addAll(lstSPBanVaKhuyenMai.get(t).getLstKhuyenMai());
					//							lstSPBanVaKhuyenMai.remove(t);
					//						}
					//					}
					//xu ly cho list SPban va SPKM
					for (SaleOrderDetailForInvoidVO ex : lstSPBanVaKhuyenMai) {
						idx = 1;
						int size = ex.getLstSPBan().size() + ex.getLstKhuyenMai().size() + 1;
						if (size > 1) {
							if (size <= vatMax) { // truong hop nhom spban + km  cung chuong trinh < vat max thi tao thanh 1 don moi
								SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
								saleOrderVOEx.setSaleOrder(so);
								saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
								saleOrderVOEx.setAmount(BigDecimal.ZERO);
								saleOrderVOEx.setDiscount(BigDecimal.ZERO);
								saleOrderVOEx.setConLai(vatMax - size); //so luong dong hang con co the chua trong 1 don vat
								saleOrderVOEx.setHasFreeItem(true);
								for (SaleOrderDetail d : ex.getLstSPBan()) {
									saleOrderVOEx.getLstSaleOrderDetail().add(d);
									saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
									//saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
								}
								for (SaleOrderDetail d : ex.getLstKhuyenMai()) {
									saleOrderVOEx.getLstSaleOrderDetail().add(d);
									saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
									//saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
								}
								lstSaleOrderVOEx.add(saleOrderVOEx);
							} else {
								SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
								saleOrderVOEx.setSaleOrder(so);
								saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
								saleOrderVOEx.setAmount(BigDecimal.ZERO);
								saleOrderVOEx.setDiscount(BigDecimal.ZERO);
								saleOrderVOEx.setConLai(vatMax - 1);
								saleOrderVOEx.setHasFreeItem(true);
								lstSaleOrderVOEx.add(saleOrderVOEx);
								List<SaleOrderDetail> lstSPCoKM = new ArrayList<SaleOrderDetail>();
								if (!ex.getLstSPBan().isEmpty()) {
									lstSPCoKM.addAll(ex.getLstSPBan());
									lstSPCoKM.addAll(ex.getLstKhuyenMai());
								}
								for (SaleOrderDetail d : lstSPCoKM) {
									if ((idx == lstSPCoKM.size()) || (idx % (vatMax - 1) == 0)) {
										if (idx != lstSPCoKM.size()) {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
											saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
											saleOrderVOEx = new SaleOrderVOEx();
											saleOrderVOEx.setSaleOrder(so);
											saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
											saleOrderVOEx.setAmount(BigDecimal.ZERO);
											saleOrderVOEx.setDiscount(BigDecimal.ZERO);
											saleOrderVOEx.setConLai(vatMax - 1);
											saleOrderVOEx.setHasFreeItem(true);
											lstSaleOrderVOEx.add(saleOrderVOEx);
										} else {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
											saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										}
									} else {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
									}
									idx++;
								}
							}
						}
					}
					//xu ly cac don con trong dong
					int maxVAT = vatMax;
					for (int j = sizeLstSaleOrder1; j < lstSaleOrderVOEx.size(); j++) {
						for (int k = sizeLstSaleOrder1; k < lstSaleOrderVOEx.size(); k++) {
							if (j != k && j < lstSaleOrderVOEx.size()) {
								if (lstSaleOrderVOEx.get(j).isHasFreeItem() && lstSaleOrderVOEx.get(k).isHasFreeItem()) {
									maxVAT = vatMax - 1;
								} else {
									maxVAT = vatMax;
								}
								if ((lstSaleOrderVOEx.get(j).getConLai() + lstSaleOrderVOEx.get(k).getConLai()) >= maxVAT) { // co the gop lai thanh 1 don vat
									lstSaleOrderVOEx.get(j).setAmount(lstSaleOrderVOEx.get(j).getAmount().add(lstSaleOrderVOEx.get(k).getAmount()));
									lstSaleOrderVOEx.get(j).setDiscount(lstSaleOrderVOEx.get(j).getDiscount().add(lstSaleOrderVOEx.get(k).getDiscount()));
									List<SaleOrderDetail> lstgop = new ArrayList<SaleOrderDetail>();
									lstgop.addAll(lstSaleOrderVOEx.get(j).getLstSaleOrderDetail());
									lstgop.addAll(lstSaleOrderVOEx.get(k).getLstSaleOrderDetail());
									lstSaleOrderVOEx.get(j).setLstSaleOrderDetail(lstgop);
									lstSaleOrderVOEx.get(j).setConLai(vatMax - 1 - lstSaleOrderVOEx.get(j).getLstSaleOrderDetail().size());
									lstSaleOrderVOEx.remove(k);
									k--;
								}
							}
						}
					}
					//xu ly cho listConLai 
					if (!lstConLai.isEmpty()) {
						for (int n = sizeLstSaleOrder1; n < lstSaleOrderVOEx.size(); n++) {
							int conLai = lstSaleOrderVOEx.get(n).getConLai();
							if (conLai > 0) {
								for (int m = 0; m < conLai; m++) {
									if (lstConLai.size() > 0) {
										lstSaleOrderVOEx.get(n).setAmount(lstSaleOrderVOEx.get(n).getAmount().add(lstConLai.get(m).getAmount()));
										lstSaleOrderVOEx.get(n).setDiscount(lstSaleOrderVOEx.get(n).getDiscount().add(lstConLai.get(m).getDiscountAmount()));
										lstSaleOrderVOEx.get(n).getLstSaleOrderDetail().add(lstConLai.get(m));
										lstConLai.remove(m);
										m--;
										conLai--;
									}
								}
							}
						}
					}
					idx = 1;
					if (!lstConLai.isEmpty()) {
						if (lstConLai.size() <= vatMax) {
							SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
							saleOrderVOEx.setSaleOrder(so);
							saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
							saleOrderVOEx.setAmount(BigDecimal.ZERO);
							saleOrderVOEx.setDiscount(BigDecimal.ZERO);
							saleOrderVOEx.setConLai(vatMax - lstConLai.size()); //so luong dong hang con co the chua trong 1 don vat
							for (SaleOrderDetail d : lstConLai) {
								saleOrderVOEx.getLstSaleOrderDetail().add(d);
								saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
								saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
							}
							lstSaleOrderVOEx.add(saleOrderVOEx);
						} else {
							SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
							saleOrderVOEx.setSaleOrder(so);
							saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
							saleOrderVOEx.setAmount(BigDecimal.ZERO);
							saleOrderVOEx.setDiscount(BigDecimal.ZERO);
							saleOrderVOEx.setConLai(vatMax);
							lstSaleOrderVOEx.add(saleOrderVOEx);
							for (SaleOrderDetail d : lstConLai) {
								if ((idx == lstConLai.size()) || (idx % vatMax == 0)) {
									if (idx != lstConLai.size()) {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										saleOrderVOEx = new SaleOrderVOEx();
										saleOrderVOEx.setSaleOrder(so);
										saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
										saleOrderVOEx.setAmount(BigDecimal.ZERO);
										saleOrderVOEx.setDiscount(BigDecimal.ZERO);
										saleOrderVOEx.setConLai(vatMax);
										lstSaleOrderVOEx.add(saleOrderVOEx);
									} else {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
									}
								} else {
									saleOrderVOEx.getLstSaleOrderDetail().add(d);
									saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
									saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
									saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
								}
								idx++;
							}
						}
					}
					//set TAX amount
					for (int l = sizeLstSaleOrder1, size = lstSaleOrderVOEx.size(); l < size; l++) {
						SaleOrderVOEx saleOrderVOEx = lstSaleOrderVOEx.get(l);
						if (saleOrderVOEx != null) {
							BigDecimal taxAmount = null;
							Float vat = 0F;
							if (saleOrderVOEx.getLstSaleOrderDetail().get(0).getVat() == 0) {
								vat = this.getVATListSaleOrderDetail(saleOrderVOEx.getLstSaleOrderDetail());
							} else {
								vat = saleOrderVOEx.getLstSaleOrderDetail().get(0).getVat();
							}
							if (saleOrderVOEx.getAmount() != null) {
								BigDecimal d = (new BigDecimal(100 + vat));
								taxAmount = ((saleOrderVOEx.getAmount().subtract(saleOrderVOEx.getDiscount())).multiply((new BigDecimal(vat))));
								taxAmount = taxAmount.divide(d, RoundingMode.HALF_UP);
								saleOrderVOEx.setTaxAmount(taxAmount);
							} else {
								saleOrderVOEx.setTaxAmount(BigDecimal.ZERO);
							}
							saleOrderVOEx.setStringVat(vat != null ? (vat + " %") : "");
						}
					}
				}
				//tach xong
			}
			List<Invoice> listInvoiceCanceled = invoiceDAO.getListInvoiceCanceledByConditionEx1(null, shopId, saleStaffCode, deliveryStaffCode, orderNumber, fromDate, toDate, customerCode, lockDate, isValueOrder, numberValueOrder);
			for (Invoice invoice : listInvoiceCanceled) {
				SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
				saleOrderVOEx.setAmount(invoice.getAmount());
				saleOrderVOEx.setDiscount(invoice.getDiscount());
				saleOrderVOEx.setTaxAmount(invoice.getTaxAmount());
				saleOrderVOEx.setSaleOrder(invoice.getSaleOrder());
				saleOrderVOEx.setStringVat(invoice.getVat() + "%");
				lstSaleOrderVOEx.add(saleOrderVOEx);
			}

			for (SaleOrderVOEx saleOrderVOEx : lstSaleOrderVOEx) {
				saleOrderVOEx.formatData();
				saleOrderVOEx.setLstSaleOrderDetail(null);
				if (StringUtility.isNullOrEmpty(saleOrderVOEx.getCpnyName()) && saleOrderVOEx.getSaleOrder() != null && saleOrderVOEx.getSaleOrder().getCustomer() != null) {
					saleOrderVOEx.setCpnyName(saleOrderVOEx.getSaleOrder().getCustomer().getInvoiceConpanyName());
				}
			}

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return lstSaleOrderVOEx;
	}

	/*
	 * @Override public List<SaleOrderVOEx> getListSaleOrderWithNoInvoiceEx1(
	 * KPaging<SaleOrderVOEx> kPaging, Long shopId, String saleStaffCode, String
	 * deliveryStaffCode, String orderNumber, Date fromDate, Date toDate, String
	 * customerCode, Date lockDate, Integer isValueOrder) throws
	 * BusinessException { List<SaleOrderVOEx> lstSaleOrderVOEx = new
	 * ArrayList<SaleOrderVOEx>(); try { ApParam apParamVATMax =
	 * apParamDAO.getApParamByCode("MAXVAT",ActiveType.RUNNING); int vatMax =
	 * Integer.valueOf(apParamVATMax.getValue()); List<SaleOrder> lstSO =
	 * saleOrderDAO.getListSaleOrderWithNoInvoiceEx1(null, shopId,saleStaffCode,
	 * deliveryStaffCode, orderNumber, fromDate, toDate, customerCode, lockDate,
	 * isValueOrder,1);
	 * 
	 * List<SaleOrderDetail> lstAllSOD =
	 * saleOrderDAO.getListSaleOrderWithNoInvoiceEx1SOD(null,
	 * shopId,saleStaffCode, deliveryStaffCode, orderNumber, fromDate, toDate,
	 * customerCode, lockDate, isValueOrder);
	 * 
	 * Map<Long, List<SaleOrderDetail>> mapSO = new HashMap<Long,
	 * List<SaleOrderDetail>>(); Long saleOrderId = null; for(int i = 0, sizeAll
	 * = lstAllSOD.size(); i < sizeAll; i++){
	 * if(lstAllSOD.get(i).getSaleOrder().getId() != saleOrderId){ saleOrderId =
	 * lstAllSOD.get(i).getSaleOrder().getId(); List<SaleOrderDetail> list = new
	 * ArrayList<SaleOrderDetail>(); list.add(lstAllSOD.get(i));
	 * mapSO.put(saleOrderId, list); }else{
	 * mapSO.get(saleOrderId).add(lstAllSOD.get(i)); } }
	 * 
	 * for (SaleOrder so : lstSO) { int sizeLstSaleOrder1 =
	 * lstSaleOrderVOEx.size(); //get list km tien BigDecimal allDiscount =
	 * so.getDiscount();
	 * 
	 * //lay tat ca cac sod tru sod khuyen mai tien //List<SaleOrderDetail>
	 * listSOD = saleOrderDetailDAO.getListAllSaleOrderDetail(so.getId(), null,
	 * so.getOrderDate()); List<SaleOrderDetail> listSOD =
	 * mapSO.get(so.getId()); //tach thanh cac list co VAT khac nhau
	 * List<List<SaleOrderDetail>> listVat = new
	 * ArrayList<List<SaleOrderDetail>>(); List<SaleOrderDetail> tmp = new
	 * ArrayList<SaleOrderDetail>(); listVat.add(tmp);
	 * 
	 * if(listSOD != null && listSOD.size() > 0 ){ Float currentVAT = 0F; for
	 * (int i = 0, size = listSOD.size(); i < size; i++) {
	 * if(listSOD.get(i).getIsFreeItem() == 0 && listSOD.get(i).getVat() != null
	 * && listSOD.get(i).getVat() > 0){ if(currentVAT == 0){
	 * tmp.add(listSOD.get(i)); currentVAT = listSOD.get(i).getVat(); } else if
	 * (!currentVAT.equals(listSOD.get(i).getVat())) { tmp = new
	 * ArrayList<SaleOrderDetail>(); listVat.add(tmp); currentVAT =
	 * listSOD.get(i).getVat(); tmp.add(listSOD.get(i)); }else{
	 * tmp.add(listSOD.get(i)); } }else{ tmp.add(listSOD.get(i)); } } //tach
	 * thanh cac co cau khac nhau: for (int i = 0, size1 = listVat.size(); i <
	 * size1; i++){ List<SaleOrderDetail> listDetail = listVat.get(i); //nhom 1:
	 * hang ban ko khuyen mai,km tay,huy doi tra,trung bay
	 * if((checkListSaleOrderDetail(listDetail) &&
	 * allDiscount.compareTo(BigDecimal.ZERO) == 0 && listDetail.size() <=
	 * vatMax) || (!checkListSaleOrderDetail(listDetail) &&
	 * allDiscount.compareTo(BigDecimal.ZERO) == 0 && listDetail.size() <=
	 * vatMax - 1) || (!checkListSaleOrderDetail(listDetail) &&
	 * allDiscount.compareTo(BigDecimal.ZERO) > 0 && listDetail.size() <= vatMax
	 * - 2)){ SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setRowId(so.getId() + "-" + (lstSaleOrderVOEx.size() + 1));
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setDiscount(BigDecimal.ZERO);
	 * lstSaleOrderVOEx.add(saleOrderVOEx); for (int j = 0; j <
	 * listDetail.size(); j++) {
	 * saleOrderVOEx.getLstSaleOrderDetail().add(listDetail.get(j));
	 * saleOrderVOEx
	 * .setAmount(saleOrderVOEx.getAmount().add(listDetail.get(j).getAmount()));
	 * } }else{ //list khuyen mai tu dong // List<SaleOrderDetail> listType0 =
	 * new ArrayList<SaleOrderDetail>(); // // //list hang khong km + km tay +
	 * tra thuong CTKM // List<SaleOrderDetail> listType1 = new
	 * ArrayList<SaleOrderDetail>(); //
	 * System.out.println("Tach thanh 2 co cau: " + commonDAO.getSysDate()); //
	 * for(int j = 0, size2 = listDetail.size(); j < size2; j++){ //
	 * SaleOrderDetail sod = listDetail.get(j); // if(sod.getProgramType() !=
	 * null && sod.getProgramType().equals(ProgramType.AUTO_PROM)){ //
	 * listType0.add(sod); // //neu sp ban ma program_code != ---> la km tu dong
	 * // }else if(sod.getIsFreeItem() == 0 && sod.getProgramCode() != null &&
	 * sod.getProgramType() == null){ // listType0.add(sod); // }else{ //
	 * listType1.add(sod); // } // } //
	 * System.out.println("Tach thanh 2 co cau: " + commonDAO.getSysDate());
	 * 
	 * //tach list hang khong km + km tay + tra thuong CTKM + huy + doi + tra //
	 * if((checkListSaleOrderDetail(listSOD) &&
	 * allDiscount.compareTo(BigDecimal.ZERO) == 0 && listSOD.size() <= vatMax)
	 * // || (!checkListSaleOrderDetail(listSOD) &&
	 * allDiscount.compareTo(BigDecimal.ZERO) == 0 && listSOD.size() <= vatMax -
	 * 1) // || (!checkListSaleOrderDetail(listSOD) &&
	 * allDiscount.compareTo(BigDecimal.ZERO) > 0 && listSOD.size() <= vatMax -
	 * 2)){ // SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx(); //
	 * saleOrderVOEx.setRowId(so.getId() + "-" + (lstSaleOrderVOEx.size() + 1));
	 * // saleOrderVOEx.setSaleOrder(so); //
	 * saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>()); //
	 * saleOrderVOEx.setAmount(BigDecimal.ZERO); //
	 * lstSaleOrderVOEx.add(saleOrderVOEx); // for (int j = 0; j <
	 * listSOD.size(); j++) { //
	 * saleOrderVOEx.getLstSaleOrderDetail().add(listSOD.get(j)); //
	 * saleOrderVOEx
	 * .setAmount(saleOrderVOEx.getAmount().add(listSOD.get(j).getAmount())); //
	 * } // }else{ int index = 1; int iRun = 0; SaleOrderVOEx saleOrderVOEx =
	 * new SaleOrderVOEx(); saleOrderVOEx.setRowId(so.getId() + "-" +
	 * (lstSaleOrderVOEx.size() + 1)); saleOrderVOEx.setSaleOrder(so);
	 * saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
	 * saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setDiscount(BigDecimal.ZERO);
	 * lstSaleOrderVOEx.add(saleOrderVOEx);
	 * 
	 * for (int j = 0; j < listDetail.size(); j++) { if (iRun == vatMax - 1) {
	 * // tach lun index++; saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setRowId(so.getId() + "-" + (lstSaleOrderVOEx.size() + 1));
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setDiscount(BigDecimal.ZERO);
	 * lstSaleOrderVOEx.add(saleOrderVOEx); iRun = 1; } else { iRun++; }
	 * saleOrderVOEx.getLstSaleOrderDetail().add(listDetail.get(j));
	 * saleOrderVOEx
	 * .setAmount(saleOrderVOEx.getAmount().add(listDetail.get(j).getAmount()));
	 * } //}
	 * 
	 * //tach list khuyen mai tu dong //
	 * if((allDiscount.compareTo(BigDecimal.ZERO) == 0 && listType0.size() <=
	 * vatMax - 1) // || (allDiscount.compareTo(BigDecimal.ZERO) > 0 &&
	 * listType0.size() <= vatMax - 2)){ //
	 * System.out.println("Co cau 2 chi co 1 don VAT: " +
	 * commonDAO.getSysDate()); // SaleOrderVOEx saleOrderVOEx = new
	 * SaleOrderVOEx(); // saleOrderVOEx.setRowId(so.getId() + "-" +
	 * (lstSaleOrderVOEx.size() + 1)); // saleOrderVOEx.setSaleOrder(so); //
	 * saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>()); //
	 * saleOrderVOEx.setAmount(BigDecimal.ZERO); //
	 * lstSaleOrderVOEx.add(saleOrderVOEx); // for (int j = 0; j <
	 * listType0.size(); j++) { //
	 * saleOrderVOEx.getLstSaleOrderDetail().add(listType0.get(j)); //
	 * saleOrderVOEx
	 * .setAmount(saleOrderVOEx.getAmount().add(listType0.get(j).getAmount()));
	 * // } // System.out.println("Co cau 2 chi co 1 don VAT: " +
	 * commonDAO.getSysDate()); // }else{ // //tach ra tung CTKM cu the //
	 * //list cac chuong trinh khuyen mai tu dong // Map<String,
	 * List<SaleOrderDetail>> map = new HashMap<String,
	 * List<SaleOrderDetail>>(); // List<String> code = new ArrayList<String>();
	 * // // if (listType0 != null && listType0.size() > 0) { //
	 * System.out.println("Tach thanh cac CTKM: " + commonDAO.getSysDate()); //
	 * for (SaleOrderDetail child : listType0) { // if
	 * (map.get(child.getProgramCode()) != null) { // List<SaleOrderDetail> rpt
	 * = map.get(child.getProgramCode()); // rpt.add(child); // } else { //
	 * List<SaleOrderDetail> rpt = new ArrayList<SaleOrderDetail>(); //
	 * rpt.add(child); // map.put(child.getProgramCode(), rpt); //
	 * code.add(child.getProgramCode()); // } // } //
	 * System.out.println("Tach thanh cac CTKM: " + commonDAO.getSysDate()); //
	 * // //duyet qua list cac CTKM // System.out.println("Duyet qua cac CTKM: "
	 * + commonDAO.getSysDate()); // for (String tmpCode : code) { //
	 * List<SaleOrderDetail> listDetailCTKM = map.get(tmpCode); // //
	 * if(listDetailCTKM.size() <= vatMax - 1){ // SaleOrderVOEx saleOrderVOEx =
	 * new SaleOrderVOEx(); // saleOrderVOEx.setRowId(so.getId() + "-" +
	 * (lstSaleOrderVOEx.size() + 1)); // saleOrderVOEx.setSaleOrder(so); //
	 * saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>()); //
	 * saleOrderVOEx.setAmount(BigDecimal.ZERO); //
	 * saleOrderVOEx.setDiscount(BigDecimal.ZERO); //
	 * lstSaleOrderVOEx.add(saleOrderVOEx); // for (int k = 0; k <
	 * listDetailCTKM.size(); k++) { //
	 * saleOrderVOEx.getLstSaleOrderDetail().add(listDetailCTKM.get(k)); //
	 * saleOrderVOEx
	 * .setAmount(saleOrderVOEx.getAmount().add(listDetailCTKM.get(k
	 * ).getAmount())); // } // }else{ // int index = 1; // int iRun = 0; //
	 * SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx(); //
	 * saleOrderVOEx.setRowId(so.getId() + "-" + (lstSaleOrderVOEx.size() + 1));
	 * // saleOrderVOEx.setSaleOrder(so); //
	 * saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>()); //
	 * saleOrderVOEx.setAmount(BigDecimal.ZERO); //
	 * saleOrderVOEx.setDiscount(BigDecimal.ZERO); //
	 * lstSaleOrderVOEx.add(saleOrderVOEx); // // for (int k = 0; k <
	 * listDetailCTKM.size(); k++) { // if (iRun == vatMax - 1) { // // tach lun
	 * // index++; // saleOrderVOEx = new SaleOrderVOEx(); //
	 * saleOrderVOEx.setRowId(so.getId() + "-" + (lstSaleOrderVOEx.size() + 1));
	 * // saleOrderVOEx.setSaleOrder(so); //
	 * saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>()); //
	 * saleOrderVOEx.setAmount(BigDecimal.ZERO); //
	 * saleOrderVOEx.setDiscount(BigDecimal.ZERO); //
	 * lstSaleOrderVOEx.add(saleOrderVOEx); // iRun = 1; // } else { // iRun++;
	 * // } // saleOrderVOEx.getLstSaleOrderDetail().add(listDetailCTKM.get(k));
	 * //
	 * saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(listDetailCTKM.get
	 * (k).getAmount())); // } // } // } //
	 * System.out.println("Duyet qua cac CTKM: " + commonDAO.getSysDate()); // }
	 * // } }
	 * 
	 * // for (int l = sizeLstSaleOrder, size = lstSaleOrderVOEx.size(); l <
	 * size - 1; l++){ // SaleOrderVOEx saleOrderVOEx = lstSaleOrderVOEx.get(l);
	 * // SaleOrderVOEx saleOrderVOEx1 = lstSaleOrderVOEx.get(l + 1); // //
	 * if(saleOrderVOEx1 != null){ // List<SaleOrderDetail> list =
	 * saleOrderVOEx.getLstSaleOrderDetail(); // List<SaleOrderDetail> list1 =
	 * saleOrderVOEx1.getLstSaleOrderDetail(); // // if(list.size() +
	 * list1.size() <= vatMax - 1){ //
	 * saleOrderVOEx.getLstSaleOrderDetail().addAll(list1); //
	 * saleOrderVOEx.setAmount
	 * (saleOrderVOEx.getAmount().add(saleOrderVOEx1.getAmount())); //
	 * lstSaleOrderVOEx.remove(l + 1); // l--; // size--; // } // } // } }
	 * 
	 * //set discount for (int l = sizeLstSaleOrder1, size =
	 * lstSaleOrderVOEx.size(); l < size; l++){ SaleOrderVOEx saleOrderVOEx =
	 * lstSaleOrderVOEx.get(l); if(saleOrderVOEx != null){ List<SaleOrderDetail>
	 * listDetail = saleOrderVOEx.getLstSaleOrderDetail();
	 * if(saleOrderVOEx.getAmount().compareTo(allDiscount) > 0){
	 * if(listDetail.size() <= vatMax - 2){
	 * saleOrderVOEx.setDiscount(allDiscount); break; }else
	 * if(checkListSaleOrderDetail(listDetail)){
	 * saleOrderVOEx.setDiscount(allDiscount); break; } } } }
	 * 
	 * //set tax_amount for (int l = sizeLstSaleOrder1, size =
	 * lstSaleOrderVOEx.size(); l < size; l++){ SaleOrderVOEx saleOrderVOEx =
	 * lstSaleOrderVOEx.get(l); if(saleOrderVOEx != null){ BigDecimal taxAmount
	 * = null; Float vat = 0F; // if
	 * (saleOrderVOEx.getLstSaleOrderDetail().get(0).getIsFreeItem() == 1 // ||
	 * saleOrderVOEx.getLstSaleOrderDetail().get(0).getVat() == null) { if
	 * (saleOrderVOEx.getLstSaleOrderDetail().get(0).getVat() == 0) { vat =
	 * this.getVATListSaleOrderDetail(saleOrderVOEx.getLstSaleOrderDetail()); }
	 * else { vat = saleOrderVOEx.getLstSaleOrderDetail().get(0).getVat(); }
	 * 
	 * if (saleOrderVOEx.getAmount() != null) { taxAmount =
	 * (saleOrderVOEx.getAmount().subtract(saleOrderVOEx.getDiscount() !=
	 * null?saleOrderVOEx.getDiscount(): BigDecimal.ZERO)).multiply( new
	 * BigDecimal(vat)); taxAmount = taxAmount.divide(new BigDecimal(100 + vat),
	 * 0,RoundingMode.HALF_UP); saleOrderVOEx.setTaxAmount(taxAmount); } else {
	 * saleOrderVOEx.setTaxAmount(BigDecimal.ZERO); } } } } }
	 * 
	 * //List<Invoice> listInvoiceCanceled =
	 * invoiceDAO.getListInvoiceCanceledByCondition(null, null, orderNumber,
	 * null, fromDate, toDate, saleStaffCode, deliveryStaffCode, customerCode,
	 * null, lockDate, shopId, isValueOrder); List<Invoice> listInvoiceCanceled
	 * = invoiceDAO.getListInvoiceCanceledByConditionEx1(null, shopId,
	 * saleStaffCode, deliveryStaffCode, orderNumber, fromDate, toDate,
	 * customerCode, lockDate, isValueOrder); for (Invoice invoice :
	 * listInvoiceCanceled) { SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setAmount(invoice.getAmount());
	 * saleOrderVOEx.setDiscount(invoice.getDiscount());
	 * saleOrderVOEx.setTaxAmount(invoice.getTaxAmount());
	 * saleOrderVOEx.setSaleOrder(invoice.getSaleOrder());
	 * saleOrderVOEx.setStringVat(invoice.getVat() + "%");
	 * lstSaleOrderVOEx.add(saleOrderVOEx); }
	 * 
	 * for (SaleOrderVOEx saleOrderVOEx : lstSaleOrderVOEx) {
	 * saleOrderVOEx.formatData(); saleOrderVOEx.setLstSaleOrderDetail(null); }
	 * } catch (DataAccessException e) { throw new BusinessException(e); }
	 * return lstSaleOrderVOEx; }
	 */
	public List<SaleOrderDetail> sortVAT(List<SaleOrderDetail> lst) {
		List<SaleOrderDetail> kq = new ArrayList<SaleOrderDetail>();
		for (int i = 0; i < lst.size() - 1; i++) {
			for (int j = i + 1; j < lst.size(); j++) {
				if (lst.get(i).getVat() < lst.get(j).getVat()) {
					SaleOrderDetail tempi = lst.get(i);
					SaleOrderDetail tempj = lst.get(j);
					lst.set(i, tempj);
					lst.set(j, tempi);
				}
			}
		}
		kq.addAll(lst);
		return kq;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<SaleOrderVOEx> taoDonGopVaTraVeDanhSachDonTach(SaleOrder donChinh, List<SaleOrderDetail> lstDonGop, List<SaleOrder> lstSO) throws BusinessException {
		List<SaleOrderVOEx> lstSaleOrderVOEx = new ArrayList<SaleOrderVOEx>();
		try {
			ApParam apParamVATMax = apParamDAO.getApParamByCode("MAXVAT", ActiveType.RUNNING);
			int vatMax = Integer.valueOf(apParamVATMax.getValue());

			List<SaleOrderDetail> lstAllSOD = lstDonGop;

			//lay tat ca cac sod tru sod khuyen mai tien
			//List<SaleOrderDetail> listSOD = saleOrderDetailDAO.getListAllSaleOrderDetail(so.getId(), null, so.getOrderDate());
			List<SaleOrderDetail> listSOD = sortVAT(lstAllSOD);
			listSOD = gopSanPham(listSOD, 1);
			//tach thanh cac list co VAT khac nhau
			List<List<SaleOrderDetail>> listVat = new ArrayList<List<SaleOrderDetail>>();
			List<SaleOrderDetail> tmp = new ArrayList<SaleOrderDetail>();
			listVat.add(tmp);

			if (listSOD != null && listSOD.size() > 0) {
				Float currentVAT = null;
				for (int i = 0, size = listSOD.size(); i < size; i++) {
					if (listSOD.get(i).getVat() != null && listSOD.get(i).getVat() > 0) {
						if (currentVAT == null) {
							tmp.add(listSOD.get(i));
							currentVAT = listSOD.get(i).getVat();
						} else if (!currentVAT.equals(listSOD.get(i).getVat())) {
							tmp = new ArrayList<SaleOrderDetail>();
							listVat.add(tmp);
							currentVAT = listSOD.get(i).getVat();
							tmp.add(listSOD.get(i));
						} else {
							tmp.add(listSOD.get(i));
						}
					} else {
						if (currentVAT != null && currentVAT > 0) {
							tmp = new ArrayList<SaleOrderDetail>();
							listVat.add(tmp);
							currentVAT = listSOD.get(i).getVat();
							tmp.add(listSOD.get(i));
						} else {
							tmp.add(listSOD.get(i));
						}
					}
				}
				//tach thanh cac co cau khac nhau:
				for (int i = 0, size1 = listVat.size(); i < size1; i++) {
					int sizeLstSaleOrder1 = lstSaleOrderVOEx.size();
					//moi VAT tach theo km + chiet khau
					List<SaleOrderDetail> lstSPCoCK = new ArrayList<SaleOrderDetail>();
					List<SaleOrderDetail> lstSPKoCoCK = new ArrayList<SaleOrderDetail>(); // co km sp
					List<SaleOrderDetail> lstConLai = new ArrayList<SaleOrderDetail>();// sp ban ko co khuye mai gi het
					List<SaleOrderDetail> listDetail = listVat.get(i);
					for (SaleOrderDetail sod : listDetail) {
						if (sod.getDiscountAmount() != null && sod.getDiscountAmount().doubleValue() > 0) {
							lstSPCoCK.add(sod);
						} else {
							if (sod.getProgramType() != null && sod.getProgramType().equals(ProgramType.AUTO_PROM) && sod.getProgramCode() != null) {
								lstSPKoCoCK.add(sod);
							} else {
								lstConLai.add(sod);
							}
						}
					}

					List<SaleOrderDetail> lstCoCK = new ArrayList<SaleOrderDetail>();// sp ban co ck

					int index = 1;
					BigDecimal ck = BigDecimal.ZERO;
					for (int j = 0; j < lstSPCoCK.size(); j++) {
						lstCoCK.add(lstSPCoCK.get(j));
						if ((index == lstSPCoCK.size()) || (index % (vatMax - 1) == 0)) {
							SaleOrderDetail detailCK = new SaleOrderDetail();
							//							ck = ck.add(lstSPCoCK.get(j).getDiscountAmount());
							detailCK.setDiscountAmount(ck);
							detailCK.setSaleOrder(lstSPCoCK.get(j).getSaleOrder());
							detailCK.setShop(lstSPCoCK.get(j).getShop());
							detailCK.setIsFreeItem(0);
							lstCoCK.add(detailCK);
							//							index= 1;
							index++;
							ck = BigDecimal.ZERO;
						} else {
							//							ck = ck.add(lstSPCoCK.get(j).getDiscountAmount());
							index++;
						}
					}
					List<SaleOrderDetailForInvoidVO> lstSPBanVaKhuyenMai = new ArrayList<SaleOrderDetailForInvoidVO>();
					SaleOrderDetailForInvoidVO tmpSOD = new SaleOrderDetailForInvoidVO();
					lstSPBanVaKhuyenMai.add(tmpSOD);
					String progrCode = "";
					for (SaleOrderDetail sod : lstSPKoCoCK) {
						if (StringUtility.isNullOrEmpty(progrCode)) {
							if (sod.getIsFreeItem().intValue() == 1) {
								tmpSOD.getLstKhuyenMai().add(sod);
							} else {
								tmpSOD.getLstSPBan().add(sod);
							}
							progrCode = sod.getProgramCode();
						} else if (!progrCode.equals(sod.getProgramCode())) {
							tmpSOD = new SaleOrderDetailForInvoidVO();
							lstSPBanVaKhuyenMai.add(tmpSOD);
							progrCode = sod.getProgramCode();
							if (sod.getIsFreeItem().intValue() == 1) {
								tmpSOD.getLstKhuyenMai().add(sod);
							} else {
								tmpSOD.getLstSPBan().add(sod);
							}
						} else {
							if (sod.getIsFreeItem().intValue() == 1) {
								tmpSOD.getLstKhuyenMai().add(sod);
							} else {
								tmpSOD.getLstSPBan().add(sod);
							}
						}
					}
					int idx = 1;
					if (!lstCoCK.isEmpty()) { //xu ly cho nhung dong hang co chiet khau tien ==> tao thanh 1 don vat
						if (lstCoCK.size() <= vatMax) {
							SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
							saleOrderVOEx.setSaleOrder(donChinh);
							saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
							saleOrderVOEx.setAmount(BigDecimal.ZERO);
							saleOrderVOEx.setDiscount(BigDecimal.ZERO);
							saleOrderVOEx.setConLai(vatMax - lstCoCK.size()); //so luong dong hang con co the chua trong 1 don vat
							for (SaleOrderDetail d : lstCoCK) {
								saleOrderVOEx.getLstSaleOrderDetail().add(d);
								saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
								saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
							}
							lstSaleOrderVOEx.add(saleOrderVOEx);
						} else {
							SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
							saleOrderVOEx.setSaleOrder(donChinh);
							saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
							saleOrderVOEx.setAmount(BigDecimal.ZERO);
							saleOrderVOEx.setDiscount(BigDecimal.ZERO);
							saleOrderVOEx.setConLai(vatMax);
							lstSaleOrderVOEx.add(saleOrderVOEx);
							for (SaleOrderDetail d : lstCoCK) {
								if ((idx == lstCoCK.size()) || (idx % vatMax == 0)) {
									if (idx != lstCoCK.size()) {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										saleOrderVOEx = new SaleOrderVOEx();
										saleOrderVOEx.setSaleOrder(donChinh);
										saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
										saleOrderVOEx.setAmount(BigDecimal.ZERO);
										saleOrderVOEx.setDiscount(BigDecimal.ZERO);
										saleOrderVOEx.setConLai(vatMax);
										lstSaleOrderVOEx.add(saleOrderVOEx);
									} else {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
									}
								} else {
									saleOrderVOEx.getLstSaleOrderDetail().add(d);
									saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
									saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
									saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
								}
								idx++;
							}
						}
					}
					//					for(int t=lstSPBanVaKhuyenMai.size()-1;t>=0;t--){
					//						if(!lstSPBanVaKhuyenMai.get(t).getLstKhuyenMai().isEmpty() && lstSPBanVaKhuyenMai.get(t).getLstSPBan().isEmpty()){
					//							lstConLai.addAll(lstSPBanVaKhuyenMai.get(t).getLstKhuyenMai());
					//							lstSPBanVaKhuyenMai.remove(t);
					//						}
					//					}
					//xu ly cho list SPban va SPKM
					for (SaleOrderDetailForInvoidVO ex : lstSPBanVaKhuyenMai) {
						idx = 1;
						int size = ex.getLstSPBan().size() + ex.getLstKhuyenMai().size() + 1;
						if (size > 1) {
							if (size <= vatMax) { // truong hop nhom spban + km  cung chuong trinh < vat max thi tao thanh 1 don moi
								SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
								saleOrderVOEx.setSaleOrder(donChinh);
								saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
								saleOrderVOEx.setAmount(BigDecimal.ZERO);
								saleOrderVOEx.setDiscount(BigDecimal.ZERO);
								saleOrderVOEx.setConLai(vatMax - size); //so luong dong hang con co the chua trong 1 don vat
								saleOrderVOEx.setHasFreeItem(true);
								for (SaleOrderDetail d : ex.getLstSPBan()) {
									saleOrderVOEx.getLstSaleOrderDetail().add(d);
									saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
//									saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
								}
								for (SaleOrderDetail d : ex.getLstKhuyenMai()) {
									saleOrderVOEx.getLstSaleOrderDetail().add(d);
									saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
//									saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
								}
								lstSaleOrderVOEx.add(saleOrderVOEx);
							} else {
								SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
								saleOrderVOEx.setSaleOrder(donChinh);
								saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
								saleOrderVOEx.setAmount(BigDecimal.ZERO);
								saleOrderVOEx.setDiscount(BigDecimal.ZERO);
								saleOrderVOEx.setConLai(vatMax - 1);
								saleOrderVOEx.setHasFreeItem(true);
								lstSaleOrderVOEx.add(saleOrderVOEx);
								List<SaleOrderDetail> lstSPCoKM = new ArrayList<SaleOrderDetail>();
								if (!ex.getLstSPBan().isEmpty()) {
									lstSPCoKM.addAll(ex.getLstSPBan());
									lstSPCoKM.addAll(ex.getLstKhuyenMai());
								}
								for (SaleOrderDetail d : lstSPCoKM) {
									if ((idx == lstSPCoKM.size()) || (idx % (vatMax - 1) == 0)) {
										if (idx != lstSPCoKM.size()) {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
											saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
											saleOrderVOEx = new SaleOrderVOEx();
											saleOrderVOEx.setSaleOrder(donChinh);
											saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
											saleOrderVOEx.setAmount(BigDecimal.ZERO);
											saleOrderVOEx.setDiscount(BigDecimal.ZERO);
											saleOrderVOEx.setConLai(vatMax - 1);
											saleOrderVOEx.setHasFreeItem(true);
											lstSaleOrderVOEx.add(saleOrderVOEx);
										} else {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
											saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										}
									} else {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
									}
									idx++;
								}
							}
						}
					}
					//xu ly cac don con trong dong
					int maxVAT = vatMax;
					for (int j = sizeLstSaleOrder1; j < lstSaleOrderVOEx.size(); j++) { 
						for (int k = sizeLstSaleOrder1; k < lstSaleOrderVOEx.size(); k++) {
							if (j != k  && j < lstSaleOrderVOEx.size()) {
								if (lstSaleOrderVOEx.get(j).isHasFreeItem() && lstSaleOrderVOEx.get(k).isHasFreeItem()) {
									maxVAT = vatMax - 1;
								} else {
									maxVAT = vatMax;
								}
								if ((lstSaleOrderVOEx.get(j).getConLai() + lstSaleOrderVOEx.get(k).getConLai()) >= maxVAT) { // co the gop lai thanh 1 don vat
									lstSaleOrderVOEx.get(j).setAmount(lstSaleOrderVOEx.get(j).getAmount().add(lstSaleOrderVOEx.get(k).getAmount()));
									lstSaleOrderVOEx.get(j).setDiscount(lstSaleOrderVOEx.get(j).getDiscount().add(lstSaleOrderVOEx.get(k).getDiscount()));
									List<SaleOrderDetail> lstgop = new ArrayList<SaleOrderDetail>();
									lstgop.addAll(lstSaleOrderVOEx.get(j).getLstSaleOrderDetail());
									lstgop.addAll(lstSaleOrderVOEx.get(k).getLstSaleOrderDetail());
									lstSaleOrderVOEx.get(j).setLstSaleOrderDetail(lstgop);
									lstSaleOrderVOEx.get(j).setConLai(vatMax - 1 - lstSaleOrderVOEx.get(j).getLstSaleOrderDetail().size());
									lstSaleOrderVOEx.remove(k);
									k--;
								}
							}
						}
					}
					//xu ly cho listConLai 
					if (!lstConLai.isEmpty()) {
						for (int n = sizeLstSaleOrder1; n < lstSaleOrderVOEx.size(); n++) {
							int conLai = lstSaleOrderVOEx.get(n).getConLai();
							if (conLai > 0) {
								for (int m = 0; m < conLai; m++) {
									if (lstConLai.size() > 0) {
										lstSaleOrderVOEx.get(n).setAmount(lstSaleOrderVOEx.get(n).getAmount().add(lstConLai.get(m).getAmount()));
										lstSaleOrderVOEx.get(n).setDiscount(lstSaleOrderVOEx.get(n).getDiscount().add(lstConLai.get(m).getDiscountAmount()));
										lstSaleOrderVOEx.get(n).getLstSaleOrderDetail().add(lstConLai.get(m));
										lstConLai.remove(m);
										m--;
										conLai--;
									}
								}
							}
						}
					}
					idx = 1;
					if (!lstConLai.isEmpty()) {
						if (lstConLai.size() <= vatMax) {
							SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
							saleOrderVOEx.setSaleOrder(donChinh);
							saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
							saleOrderVOEx.setAmount(BigDecimal.ZERO);
							saleOrderVOEx.setDiscount(BigDecimal.ZERO);
							saleOrderVOEx.setConLai(vatMax - lstConLai.size()); //so luong dong hang con co the chua trong 1 don vat
							for (SaleOrderDetail d : lstConLai) {
								saleOrderVOEx.getLstSaleOrderDetail().add(d);
								saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
								saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
							}
							lstSaleOrderVOEx.add(saleOrderVOEx);
						} else {
							SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
							saleOrderVOEx.setSaleOrder(donChinh);
							saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
							saleOrderVOEx.setAmount(BigDecimal.ZERO);
							saleOrderVOEx.setDiscount(BigDecimal.ZERO);
							saleOrderVOEx.setConLai(vatMax);
							lstSaleOrderVOEx.add(saleOrderVOEx);
							for (SaleOrderDetail d : lstConLai) {
								if ((idx == lstConLai.size()) || (idx % vatMax == 0)) {
									if (idx != lstConLai.size()) {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										saleOrderVOEx = new SaleOrderVOEx();
										saleOrderVOEx.setSaleOrder(donChinh);
										saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
										saleOrderVOEx.setAmount(BigDecimal.ZERO);
										saleOrderVOEx.setDiscount(BigDecimal.ZERO);
										saleOrderVOEx.setConLai(vatMax);
										lstSaleOrderVOEx.add(saleOrderVOEx);
									} else {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
									}
								} else {
									saleOrderVOEx.getLstSaleOrderDetail().add(d);
									saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
									saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
									saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
								}
								idx++;
							}
						}
					}
					//set tax_amount
					for (int l = sizeLstSaleOrder1, size = lstSaleOrderVOEx.size(); l < size; l++) {
						SaleOrderVOEx saleOrderVOEx = lstSaleOrderVOEx.get(l);
						if (saleOrderVOEx != null) {
							BigDecimal taxAmount = null;
							Float vat = 0F;
							if (saleOrderVOEx.getLstSaleOrderDetail().get(0).getVat() == 0) {
								vat = this.getVATListSaleOrderDetail(saleOrderVOEx.getLstSaleOrderDetail());
							} else {
								vat = saleOrderVOEx.getLstSaleOrderDetail().get(0).getVat();
							}
							if (saleOrderVOEx.getAmount() != null) {
								BigDecimal d = (new BigDecimal(100 + vat));
								taxAmount = ((saleOrderVOEx.getAmount().subtract(saleOrderVOEx.getDiscount())).multiply((new BigDecimal(vat))));
								taxAmount = taxAmount.divide(d, RoundingMode.HALF_UP);
								saleOrderVOEx.setTaxAmount(taxAmount);
							} else {
								saleOrderVOEx.setTaxAmount(BigDecimal.ZERO);
							}
						}
					}
				}

			}

			//			List<Invoice> listInvoiceCanceled = invoiceDAO.getListInvoiceCanceledByConditionEx1(null, shopId, saleStaffCode, deliveryStaffCode, orderNumber, fromDate, toDate, customerCode, lockDate, isValueOrder);
			//			for (Invoice invoice : listInvoiceCanceled) {
			//				SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
			//				saleOrderVOEx.setAmount(invoice.getAmount());
			//				saleOrderVOEx.setDiscount(invoice.getDiscount());
			//				saleOrderVOEx.setTaxAmount(invoice.getTaxAmount());
			//				saleOrderVOEx.setSaleOrder(invoice.getSaleOrder());
			//				saleOrderVOEx.setStringVat(invoice.getVat() + "%");
			//				lstSaleOrderVOEx.add(saleOrderVOEx);
			//			}

			for (SaleOrderVOEx saleOrderVOEx : lstSaleOrderVOEx) {
				saleOrderVOEx.formatData();
				saleOrderVOEx.setLstSaleOrderDetail(null);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return lstSaleOrderVOEx;
	}

	//check list saleorder detail toan la hang ban: true
	public boolean checkListSaleOrderDetail(List<SaleOrderDetail> listDetail) {
		for (SaleOrderDetail saleOrderDetail : listDetail) {
			if (saleOrderDetail.getIsFreeItem().equals(1)) {
				return false;
			}
		}
		return true;
	}

	public Float getVATListSaleOrderDetail(List<SaleOrderDetail> listDetail) {
		for (SaleOrderDetail saleOrderDetail : listDetail) {
			if (saleOrderDetail.getIsFreeItem().equals(0) && saleOrderDetail.getVat() > 0) {
				return saleOrderDetail.getVat();
			}
		}
		return 0F;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateInvoiceFromSaleOrder(Long shopId, List<Long> listInvoiceId, List<String> listInvoiceNumber, String username) throws BusinessException {
		if (listInvoiceId == null || listInvoiceNumber == null || listInvoiceId.size() != listInvoiceNumber.size()) {
			throw new IllegalArgumentException("listInvoiceId is null or listInvoiceNumber is null");
		}
		try {
			Date date = commonDAO.getSysDate();
			for (int i = 0; i < listInvoiceId.size(); i++) {
				Date lockDate = shopLockDAO.getNextLockedDay(shopId);
				if (lockDate == null) {
					lockDate = commonDAO.getSysDate();
				}

				Long invoiceId = listInvoiceId.get(i);
				String invoiceNumber = listInvoiceNumber.get(i).toUpperCase();
				Invoice existInvoice = invoiceDAO.getInvoiceByInvoiceNumber(invoiceNumber, shopId);
				if (existInvoice == null) {
					// tao moi
					Invoice currentInvoice = invoiceDAO.getInvoiceById(invoiceId);
					if (currentInvoice == null) {
						throw new BusinessException("invoice not exist");
					}
					if (currentInvoice.getSaleOrderType() != null && currentInvoice.getSaleOrderType().intValue() == 1) {// don gop
						if (currentInvoice.getStatus().getValue().equals(InvoiceStatus.USING.getValue())) {// don status = 0 thi chi cap nhat lai so hoa don
							currentInvoice.setInvoiceNumber(invoiceNumber);
							currentInvoice.setUpdateDate(date);
							currentInvoice.setUpdateUser(username);
							invoiceDAO.updateInvoice(currentInvoice);
						} else if (currentInvoice.getStatus().getValue().equals(InvoiceStatus.CANCELED.getValue())) { // don da huy thi tao 1 don moi co satus =0
							Invoice newInvoice = currentInvoice.clone();
							newInvoice.setStatus(InvoiceStatus.USING);
							newInvoice.setCreateDate(date);
							newInvoice.setCreateUser(username);
							newInvoice.setInvoiceNumber(invoiceNumber);
							currentInvoice.setSaleOrder(null);
							invoiceDAO.updateInvoice(currentInvoice);
							newInvoice = invoiceDAO.createInvoice(newInvoice);
							List<InvoiceDetail> lstDetail = invoiceDAO.getListInvoiceDetailByInvoiceId(invoiceId);
							List<InvoiceDetail> lstDetailNew = new ArrayList<InvoiceDetail>();
							if (!lstDetail.isEmpty()) {
								for (InvoiceDetail d : lstDetail) {
									d.setInvoice(newInvoice);
									d.setUpdateDate(date);
									d.setUpdateUser(username);
									lstDetailNew.add(d);
								}
								invoiceDAO.updateInvoiceDetail(lstDetailNew);
							}
						}
					} else {
						if (currentInvoice.getStatus().getValue().equals(InvoiceStatus.USING.getValue())) {// don status = 0 thi chi cap nhat lai so hoa don
							currentInvoice.setInvoiceNumber(invoiceNumber);
							currentInvoice.setUpdateDate(date);
							currentInvoice.setUpdateUser(username);
							invoiceDAO.updateInvoice(currentInvoice);
						} else {
							List<SaleOrderDetail> listSOD = saleOrderDetailDAO.getListSaleOrderDetailByInvoiceId(invoiceId, null);

							SaleOrder __saleOrder = null;
							SaleOrderDetail __sod = null;

							if (listSOD.size() > 0) {
								__saleOrder = listSOD.get(0).getSaleOrder();
								__sod = listSOD.get(0);
							} else {
								throw new BusinessException("invoice is not belong to any sale order detail");
							}

							Invoice __newInvoice = new Invoice();
							__newInvoice.setInvoiceNumber(invoiceNumber);
							__newInvoice.setStatus(InvoiceStatus.USING);
							__newInvoice.setCreateUser(username);
							__newInvoice.setCreateDate(commonDAO.getSysDate());
							__newInvoice.setOrderDate(__saleOrder.getOrderDate());
							//					if (__sod.getIsFreeItem() == 1 || __sod.getVat() == null) {
							//						__newInvoice.setVat(0F);
							//					} else {
							//						__newInvoice.setVat(__sod.getVat());
							//					}
							if (listSOD.get(0).getVat() == 0 || listSOD.get(0).getVat() == null) {
								__newInvoice.setVat(this.getVATListSaleOrderDetail(listSOD));
							} else {
								__newInvoice.setVat(listSOD.get(0).getVat());
							}
							__newInvoice.setAmount(currentInvoice.getAmount());
							__newInvoice.setStaff(__saleOrder.getStaff());
							Float __newInvoiceVat = __newInvoice.getVat();

							__newInvoice.setSelName(__saleOrder.getStaff().getStaffName());
							if (__saleOrder.getDelivery() != null) {
								__newInvoice.setDeliveryStaff(__saleOrder.getDelivery());
								__newInvoice.setDeliveryName(__saleOrder.getDelivery().getStaffName());
							} else {
								throw new BusinessException("the sale order need to update dilivery:" + __saleOrder.getOrderNumber());
							}
							__newInvoice.setOrderNumber(__saleOrder.getOrderNumber());
							__newInvoice.setSku(currentInvoice.getSku());
							__newInvoice.setShop(__saleOrder.getShop());
							__newInvoice.setDiscount(currentInvoice.getDiscount());
							if (__newInvoiceVat != null) {
								BigDecimal taxAmount = null;
								if (__newInvoice.getAmount() != null) {
									taxAmount = (__newInvoice.getAmount().subtract(__newInvoice.getDiscount())).multiply(new BigDecimal(__newInvoiceVat));
									taxAmount = taxAmount.divide(new BigDecimal(100 + __newInvoiceVat), 0, RoundingMode.HALF_UP);
									__newInvoice.setTaxAmount(taxAmount);
								} else {
									__newInvoice.setTaxAmount(BigDecimal.ZERO);
								}

							}
							if (__saleOrder.getCustomer() != null) {
								__newInvoice.setCustomerCode(__saleOrder.getCustomer().getShortCode());
								__newInvoice.setCustName(__saleOrder.getCustomer().getCustomerName());
								__newInvoice.setCustomerAddr(__saleOrder.getCustomer().getAddress());
								__newInvoice.setCustTaxNumber(__saleOrder.getCustomer().getInvoiceTax());
								__newInvoice.setCustDelAddr(__saleOrder.getCustomer().getDeliveryAddress());
								if (InvoiceCustPayment.MONEY.equals(currentInvoice.getCustPayment())) {
									__newInvoice.setCustBankName(null);
									__newInvoice.setCustBankAccount(null);
								} else {
									__newInvoice.setCustBankName(__saleOrder.getCustomer().getInvoiceNameBank());
									__newInvoice.setCustBankAccount(__saleOrder.getCustomer().getInvoiceNumberAccount());
								}
							}

							if (__saleOrder.getShop() != null) {
								__newInvoice.setCpnyName(__saleOrder.getShop().getShopName());
								__newInvoice.setCpnyAddr(__saleOrder.getShop().getAddress());
								__newInvoice.setCpnyBankName(__saleOrder.getShop().getInvoiceBankName());
								__newInvoice.setCpnyBankAccount(__saleOrder.getShop().getInvoiceNumberAccount());
								__newInvoice.setCpnyTaxNumber(__saleOrder.getShop().getTaxNum());
								__newInvoice.setCpnyPhone(__saleOrder.getShop().getPhone());
							}

							__newInvoice.setInvoiceDate(null);
							//TUNGTT set sale_order
							__newInvoice.setSaleOrder(__saleOrder);
							__newInvoice.setCustPayment(currentInvoice.getCustPayment());

							Invoice newInvoice = invoiceDAO.createInvoice(__newInvoice);

							for (SaleOrderDetail sod : listSOD) {
								if (sod.getProgramType() == null || (!sod.getProgramType().equals(ProgramType.DESTROY) && !sod.getProgramType().equals(ProgramType.PRODUCT_EXCHANGE) && !sod.getProgramType().equals(ProgramType.RETURN))) {
									Invoice sodInvoice = sod.getInvoice();
									if (sodInvoice == null) {
										throw new BusinessException("data error in sale order detail");
									}
									sodInvoice.setStatus(InvoiceStatus.CANCELED);
									invoiceDAO.updateInvoice(sodInvoice);// cap nhat invoice cu

									SaleOrder saleOrder = sod.getSaleOrder();

									sod.setInvoice(newInvoice);
									saleOrderDetailDAO.updateSaleOrderDetail(sod);

									//TUNGTT insert sale_order_detail vao invoice_detail
									InvoiceDetail invoiceDetail = new InvoiceDetail();
									if (sod.getProduct() != null) {
										invoiceDetail.setProduct(sod.getProduct());
									}
									if (sod.getQuantity() != null) {
										invoiceDetail.setQuantity(sod.getQuantity());
									}
									if (sod.getPrice() != null) {
										invoiceDetail.setPrice(sod.getPrice());
									}
									if (sod.getDiscountAmount() != null) {
										invoiceDetail.setDiscountAmount(sod.getDiscountAmount());
									}
									if (sod.getDiscountPercent() != null) {
										invoiceDetail.setDiscountPercent(sod.getDiscountPercent());
									}
									if (sod.getAmount() != null) {
										invoiceDetail.setAmount(sod.getAmount());
									}
									if (sod.getVat() != null) {
										invoiceDetail.setVat(sod.getVat());
									}
									if (sod.getIsFreeItem() != null) {
										invoiceDetail.setIsFreeItem(sod.getIsFreeItem());
									}
									if (sod.getPriceValue() != null) {
										invoiceDetail.setPriceValue(sod.getPriceValue());
									}
									if (sod.getPriceNotVat() != null) {
										invoiceDetail.setPriceNotVat(sod.getPriceNotVat());
									}
									invoiceDetail.setSaleOrderDetail(sod);
									invoiceDetail.setInvoice(newInvoice);
									invoiceDetail.setCreateUser(username);
									invoiceDetail.setCreateDate(commonDAO.getSysDate());
									invoiceDAO.createInvoiceDetail(invoiceDetail);
								}
							}
						}
					}
				} else if (InvoiceStatus.USING.getValue().equals(existInvoice.getStatus().getValue()) || InvoiceStatus.CANCELED.getValue().equals(existInvoice.getStatus().getValue())) {
					throw new BusinessException("number of lstInvoiceNumber is not equals with list seperated sale orders");
				} else {
					Invoice currentInvoice = invoiceDAO.getInvoiceById(invoiceId);
					if (currentInvoice == null) {
						throw new BusinessException("invoice not exist");
					}
					currentInvoice.setStatus(InvoiceStatus.MODIFIED);
					invoiceDAO.updateInvoice(currentInvoice);
					List<SaleOrderDetail> listSOD = saleOrderDetailDAO.getListSaleOrderDetailByInvoiceId(invoiceId, null);

					SaleOrderDetail sod = listSOD.get(0);

					SaleOrder saleOrder = sod.getSaleOrder();
					existInvoice.setStatus(InvoiceStatus.USING);
					existInvoice.setUpdateDate(commonDAO.getSysDate());
					existInvoice.setUpdateUser(username);
					existInvoice.setOrderDate(saleOrder.getOrderDate());
					existInvoice.setStaff(saleOrder.getStaff());
					saleOrder.setOrderDate(saleOrder.getOrderDate());

					//					if (sod.getIsFreeItem() == 1 || sod.getVat() == null) {
					//						existInvoice.setVat(0F);
					//					} else {
					//						existInvoice.setVat(sod.getVat());
					//					}

					if (listSOD.get(0).getVat() == 0 || listSOD.get(0).getVat() == null) {
						existInvoice.setVat(this.getVATListSaleOrderDetail(listSOD));
					} else {
						existInvoice.setVat(listSOD.get(0).getVat());
					}

					existInvoice.setAmount(currentInvoice.getAmount());
					Float __existInvoiceVat = existInvoice.getVat();

					if (existInvoice != null) {
						BigDecimal taxAmount = null;
						if (existInvoice.getAmount() != null) {
							taxAmount = (existInvoice.getAmount().subtract(existInvoice.getDiscount())).multiply(new BigDecimal(__existInvoiceVat));
							taxAmount = taxAmount.divide(new BigDecimal(100 + __existInvoiceVat), 0, RoundingMode.HALF_UP);
							existInvoice.setTaxAmount(taxAmount);
						} else {
							existInvoice.setTaxAmount(BigDecimal.ZERO);
						}
					}

					existInvoice.setSelName(saleOrder.getStaff().getStaffName());
					if (saleOrder.getDelivery() != null) {
						existInvoice.setDeliveryStaff(saleOrder.getDelivery());
						existInvoice.setDeliveryName(saleOrder.getDelivery().getStaffName());
					} else {
						throw new BusinessException("the sale order need to update dilivery:" + saleOrder.getOrderNumber());
					}
					existInvoice.setOrderNumber(saleOrder.getOrderNumber());
					existInvoice.setSku(currentInvoice.getSku());
					existInvoice.setShop(saleOrder.getShop());

					if (saleOrder.getCustomer() != null) {
						existInvoice.setCustomerCode(saleOrder.getCustomer().getShortCode());
						existInvoice.setCustName(saleOrder.getCustomer().getCustomerName());
						existInvoice.setCustomerAddr(saleOrder.getCustomer().getAddress());
						existInvoice.setCustTaxNumber(saleOrder.getCustomer().getInvoiceTax());
						existInvoice.setCustDelAddr(saleOrder.getCustomer().getDeliveryAddress());
						if (InvoiceCustPayment.MONEY.equals(currentInvoice.getCustPayment())) {
							existInvoice.setCustBankName(null);
							existInvoice.setCustBankAccount(null);
						} else {
							existInvoice.setCustBankName(saleOrder.getCustomer().getInvoiceNameBank());
							existInvoice.setCustBankAccount(saleOrder.getCustomer().getInvoiceNumberAccount());
						}
					}

					if (saleOrder.getShop() != null) {
						existInvoice.setCpnyName(saleOrder.getShop().getShopName());
						existInvoice.setCpnyAddr(saleOrder.getShop().getAddress());
						existInvoice.setCpnyBankName(saleOrder.getShop().getInvoiceBankName());
						existInvoice.setCpnyBankAccount(saleOrder.getShop().getInvoiceNumberAccount());
						existInvoice.setCpnyTaxNumber(saleOrder.getShop().getTaxNum());
						existInvoice.setCpnyPhone(saleOrder.getShop().getPhone());
					}
					existInvoice.setDiscount(currentInvoice.getDiscount());
					existInvoice.setInvoiceDate(null);
					existInvoice.setCustPayment(currentInvoice.getCustPayment());

					invoiceDAO.updateInvoice(existInvoice);

					for (SaleOrderDetail __sod : listSOD) {
						if (__sod.getProgramType() == null || (!__sod.getProgramType().equals(ProgramType.DESTROY) && !__sod.getProgramType().equals(ProgramType.PRODUCT_EXCHANGE) && !__sod.getProgramType().equals(ProgramType.RETURN))) {

							__sod.setInvoice(existInvoice);
							saleOrderDetailDAO.updateSaleOrderDetail(__sod);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * gop cac san pham khuyen mai co cung VAT
	 * 
	 * @return List<SaleOrderDetail>
	 * @author phuongvm
	 * @since 20/10/2014
	 */
	public List<SaleOrderDetail> gopSanPham(List<SaleOrderDetail> lst,Integer isFreeItem){
		List<SaleOrderDetail> lstKQ = new ArrayList<SaleOrderDetail>();
		do {
			SaleOrderDetail sod = new SaleOrderDetail();
			sod = lst.get(lst.size() - 1);
			lstKQ.add(sod);
			lst.remove(lst.size()-1);
			if(sod.getProduct()!=null && sod.getIsFreeItem().equals(isFreeItem)){
				for(int i=lst.size()-1;i>=0;i--){
					SaleOrderDetail detail = lst.get(i);
					if(detail.getProduct()!=null && detail.getIsFreeItem().equals(isFreeItem)){
						if(detail.getProduct().getId().equals(sod.getProduct().getId()) && detail.getVat().equals(sod.getVat())){
							sod.setQuantity(sod.getQuantity()+detail.getQuantity());
							sod.setAmount(sod.getAmount().add(detail.getAmount()));
							sod.setDiscountPercent(null);
							lst.remove(i);
						}
					}
				}
			}
		} while (lst.size()>0);
		List<SaleOrderDetail> lstKQF = new ArrayList<SaleOrderDetail>();
		for(int i =lstKQ.size()-1;i>=0;i--){
			lstKQF.add(lstKQ.get(i)); 
		}
		return lstKQF;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.SaleOrderMgr#createInvoiceFromSaleOrder(long,
	 * java.lang.String)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createInvoiceFromSaleOrder(Long shopId, String username, Map<Long, List<String>> listSaleOrder, Map<Long, List<InvoiceCustPayment>> listCustPayment) throws BusinessException {
		try {
			/*
			 * Báº£ng SALE_ORDER: SKU = sáº½ Ä‘áº¿m cÃ¡c máº·t hÃ ng trong hÃ³a Ä‘Æ¡n (báº£ng
			 * SALE_ORDER_DETAIL, trÆ°á»�ng IS_FREE_ITEM = 0):
			 */
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (listSaleOrder == null) {
				throw new IllegalArgumentException("listSaleOrder is null");
			}
			if (listCustPayment == null) {
				throw new IllegalArgumentException("listCustPayment is null");
			}
			Date date = commonDAO.getSysDate();
			Set<Long> setSOId = listSaleOrder.keySet();
			//Iterator<Long> soIterator = setSOId.iterator();

			ApParam apParamVATMax = apParamDAO.getApParamByCode("MAXVAT", ActiveType.RUNNING);

			List<SaleOrder> lstSO = saleOrderDAO.getListSaleOrderByIterator(setSOId.iterator());

			List<SaleOrderDetail> lstAllSOD = saleOrderDAO.getListSaleOrderDetailByIterator(setSOId.iterator());
			List<SaleOrderDetail> lstAllSOD1 = new ArrayList<SaleOrderDetail>(); 
			for (int i = 0, sizeAll = lstAllSOD.size(); i < sizeAll; i++) {
				SaleOrderDetail sdt = lstAllSOD.get(i).clone();
				lstAllSOD1.add(sdt);
			}

			Map<Long, List<SaleOrderDetail>> mapSO = new HashMap<Long, List<SaleOrderDetail>>();
			Long saleOrderId = null;
			for (int i = 0, sizeAll = lstAllSOD1.size(); i < sizeAll; i++) {
				if (lstAllSOD1.get(i).getSaleOrder().getId() != saleOrderId) {
					saleOrderId = lstAllSOD1.get(i).getSaleOrder().getId();
					List<SaleOrderDetail> list = new ArrayList<SaleOrderDetail>();
					list.add(lstAllSOD1.get(i));
					mapSO.put(saleOrderId, list);
				} else {
					mapSO.get(saleOrderId).add(lstAllSOD1.get(i));
				}
			}

			for (SaleOrder so : lstSO) {
				List<String> lstInvoiceNumber = listSaleOrder.get(so.getId());
				List<InvoiceCustPayment> lstCustPayment = listCustPayment.get(so.getId());
				List<SaleOrderVOEx> lstSaleOrderVOEx = new ArrayList<SaleOrderVOEx>();

				// tach hoa don
				int vatMax = Integer.valueOf(apParamVATMax.getValue());

				List<Invoice> listRemovedInvoice = invoiceDAO.checkHasInvoiceRemoveOfSaleOrder(so.getId());

				if (listRemovedInvoice == null || listRemovedInvoice.size() == 0) {
					//lay tat ca cac sod tru sod khuyen mai tien
					List<SaleOrderDetail> listSOD = gopSanPham(mapSO.get(so.getId()),1);

					//tach thanh cac list co VAT khac nhau
					List<List<SaleOrderDetail>> listVat = new ArrayList<List<SaleOrderDetail>>();
					List<SaleOrderDetail> tmp = new ArrayList<SaleOrderDetail>();
					listVat.add(tmp);

					if (listSOD != null && listSOD.size() > 0) {
						Float currentVAT = null;
						for (int i = 0, size = listSOD.size(); i < size; i++) {
							if (listSOD.get(i).getVat() != null && listSOD.get(i).getVat() > 0) {
								if (currentVAT == null) {
									tmp.add(listSOD.get(i));
									currentVAT = listSOD.get(i).getVat();
								} else if (!currentVAT.equals(listSOD.get(i).getVat())) {
									tmp = new ArrayList<SaleOrderDetail>();
									listVat.add(tmp);
									currentVAT = listSOD.get(i).getVat();
									tmp.add(listSOD.get(i));
								} else {
									tmp.add(listSOD.get(i));
								}
							} else {
								if (currentVAT != null && currentVAT > 0) {
									tmp = new ArrayList<SaleOrderDetail>();
									listVat.add(tmp);
									currentVAT = listSOD.get(i).getVat();
									tmp.add(listSOD.get(i));
								} else {
									tmp.add(listSOD.get(i));
								}
							}
						}

						//tach thanh cac co cau khac nhau:
						for (int i = 0, size1 = listVat.size(); i < size1; i++) {
							int sizeLstSaleOrder1 = lstSaleOrderVOEx.size();
							//moi VAT tach theo km + chiet khau
							List<SaleOrderDetail> lstSPCoCK = new ArrayList<SaleOrderDetail>();
							List<SaleOrderDetail> lstSPKoCoCK = new ArrayList<SaleOrderDetail>(); // co km sp
							List<SaleOrderDetail> lstConLai = new ArrayList<SaleOrderDetail>();// sp ban ko co khuye mai gi het
							List<SaleOrderDetail> listDetail = listVat.get(i);
							for (SaleOrderDetail sod : listDetail) {
								if (sod.getDiscountAmount() != null && sod.getDiscountAmount().doubleValue() > 0) {
									lstSPCoCK.add(sod);
								} else {
									if (sod.getProgramType() != null && sod.getProgramType().equals(ProgramType.AUTO_PROM) && sod.getProgramCode() != null) {
										lstSPKoCoCK.add(sod);
									} else {
										lstConLai.add(sod);
									}
								}
							}

							List<SaleOrderDetail> lstCoCK = new ArrayList<SaleOrderDetail>();// sp ban co ck

							int index = 1;
							BigDecimal ck = BigDecimal.ZERO;
							for (int j = 0; j < lstSPCoCK.size(); j++) {
								lstCoCK.add(lstSPCoCK.get(j));
								if ((index == lstSPCoCK.size()) || (index % (vatMax - 1) == 0)) {
									SaleOrderDetail detailCK = new SaleOrderDetail();
									//									ck = ck.add(lstSPCoCK.get(j).getDiscountAmount());
									detailCK.setDiscountAmount(ck);
									detailCK.setSaleOrder(lstSPCoCK.get(j).getSaleOrder());
									detailCK.setShop(lstSPCoCK.get(j).getShop());
									detailCK.setIsFreeItem(0);
									lstCoCK.add(detailCK);
									//									index= 1;
									index++;
									ck = BigDecimal.ZERO;
								} else {
									//									ck = ck.add(lstSPCoCK.get(j).getDiscountAmount());
									index++;
								}
							}
							List<SaleOrderDetailForInvoidVO> lstSPBanVaKhuyenMai = new ArrayList<SaleOrderDetailForInvoidVO>();
							SaleOrderDetailForInvoidVO tmpSOD = new SaleOrderDetailForInvoidVO();
							lstSPBanVaKhuyenMai.add(tmpSOD);
							String progrCode = "";
							for (SaleOrderDetail sod : lstSPKoCoCK) {
								if (StringUtility.isNullOrEmpty(progrCode)) {
									if (sod.getIsFreeItem().intValue() == 1) {
										tmpSOD.getLstKhuyenMai().add(sod);
									} else {
										tmpSOD.getLstSPBan().add(sod);
									}
									progrCode = sod.getProgramCode();
								} else if (!progrCode.equals(sod.getProgramCode())) {
									tmpSOD = new SaleOrderDetailForInvoidVO();
									lstSPBanVaKhuyenMai.add(tmpSOD);
									progrCode = sod.getProgramCode();
									if (sod.getIsFreeItem().intValue() == 1) {
										tmpSOD.getLstKhuyenMai().add(sod);
									} else {
										tmpSOD.getLstSPBan().add(sod);
									}
								} else {
									if (sod.getIsFreeItem().intValue() == 1) {
										tmpSOD.getLstKhuyenMai().add(sod);
									} else {
										tmpSOD.getLstSPBan().add(sod);
									}
								}
							}
							int idx = 1;
							if (!lstCoCK.isEmpty()) { //xu ly cho nhung dong hang co chiet khau tien ==> tao thanh 1 don vat
								if (lstCoCK.size() <= vatMax) {
									SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
									saleOrderVOEx.setSaleOrder(so);
									saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
									saleOrderVOEx.setAmount(BigDecimal.ZERO);
									saleOrderVOEx.setDiscount(BigDecimal.ZERO);
									saleOrderVOEx.setConLai(vatMax - lstCoCK.size()); //so luong dong hang con co the chua trong 1 don vat
									for (SaleOrderDetail d : lstCoCK) {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
									}
									lstSaleOrderVOEx.add(saleOrderVOEx);
								} else {
									SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
									saleOrderVOEx.setSaleOrder(so);
									saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
									saleOrderVOEx.setAmount(BigDecimal.ZERO);
									saleOrderVOEx.setDiscount(BigDecimal.ZERO);
									saleOrderVOEx.setConLai(vatMax);
									lstSaleOrderVOEx.add(saleOrderVOEx);
									for (SaleOrderDetail d : lstCoCK) {
										if ((idx == lstCoCK.size()) || (idx % vatMax == 0)) {
											if (idx != lstCoCK.size()) {
												saleOrderVOEx.getLstSaleOrderDetail().add(d);
												saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
												saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
												saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
												saleOrderVOEx = new SaleOrderVOEx();
												saleOrderVOEx.setSaleOrder(so);
												saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
												saleOrderVOEx.setAmount(BigDecimal.ZERO);
												saleOrderVOEx.setDiscount(BigDecimal.ZERO);
												saleOrderVOEx.setConLai(vatMax);
												lstSaleOrderVOEx.add(saleOrderVOEx);
											} else {
												saleOrderVOEx.getLstSaleOrderDetail().add(d);
												saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
												saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
												saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
											}
										} else {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
											saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										}
										idx++;
									}
								}
							}
							//							for(int t=lstSPBanVaKhuyenMai.size()-1;t>=0;t--){
							//								if(!lstSPBanVaKhuyenMai.get(t).getLstKhuyenMai().isEmpty() && lstSPBanVaKhuyenMai.get(t).getLstSPBan().isEmpty()){
							//									lstConLai.addAll(lstSPBanVaKhuyenMai.get(t).getLstKhuyenMai());
							//									lstSPBanVaKhuyenMai.remove(t);
							//								}
							//							}
							//xu ly cho list SPban va SPKM
							for (SaleOrderDetailForInvoidVO ex : lstSPBanVaKhuyenMai) {
								idx = 1;
								int size = ex.getLstSPBan().size() + ex.getLstKhuyenMai().size() + 1;
								if (size > 1) {
									if (size <= vatMax) { // truong hop nhom spban + km  cung chuong trinh < vat max thi tao thanh 1 don moi
										SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
										saleOrderVOEx.setSaleOrder(so);
										saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
										saleOrderVOEx.setAmount(BigDecimal.ZERO);
										saleOrderVOEx.setDiscount(BigDecimal.ZERO);
										saleOrderVOEx.setConLai(vatMax - size); //so luong dong hang con co the chua trong 1 don vat
										saleOrderVOEx.setHasFreeItem(true);
										for (SaleOrderDetail d : ex.getLstSPBan()) {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
//											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										}
										for (SaleOrderDetail d : ex.getLstKhuyenMai()) {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
//											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										}
										lstSaleOrderVOEx.add(saleOrderVOEx);
									} else {
										SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
										saleOrderVOEx.setSaleOrder(so);
										saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
										saleOrderVOEx.setAmount(BigDecimal.ZERO);
										saleOrderVOEx.setDiscount(BigDecimal.ZERO);
										saleOrderVOEx.setConLai(vatMax - 1);
										saleOrderVOEx.setHasFreeItem(true);
										lstSaleOrderVOEx.add(saleOrderVOEx);
										List<SaleOrderDetail> lstSPCoKM = new ArrayList<SaleOrderDetail>();
										if (!ex.getLstSPBan().isEmpty()) {
											lstSPCoKM.addAll(ex.getLstSPBan());
											lstSPCoKM.addAll(ex.getLstKhuyenMai());
										}
										for (SaleOrderDetail d : lstSPCoKM) {
											if ((idx == lstSPCoKM.size()) || (idx % (vatMax - 1) == 0)) {
												if (idx != lstSPCoKM.size()) {
													saleOrderVOEx.getLstSaleOrderDetail().add(d);
													saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
													saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
													saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
													saleOrderVOEx = new SaleOrderVOEx();
													saleOrderVOEx.setSaleOrder(so);
													saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
													saleOrderVOEx.setAmount(BigDecimal.ZERO);
													saleOrderVOEx.setDiscount(BigDecimal.ZERO);
													saleOrderVOEx.setConLai(vatMax - 1);
													saleOrderVOEx.setHasFreeItem(true);
													lstSaleOrderVOEx.add(saleOrderVOEx);
												} else {
													saleOrderVOEx.getLstSaleOrderDetail().add(d);
													saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
													saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
													saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
												}
											} else {
												saleOrderVOEx.getLstSaleOrderDetail().add(d);
												saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
												saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
												saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
											}
											idx++;
										}
									}
								}
							}
							//xu ly cac don con trong dong
							int maxVAT = vatMax;
							for (int j = sizeLstSaleOrder1; j < lstSaleOrderVOEx.size(); j++) {
								for (int k = sizeLstSaleOrder1; k < lstSaleOrderVOEx.size(); k++) {
									if (j != k  && j < lstSaleOrderVOEx.size()) {
										if (lstSaleOrderVOEx.get(j).isHasFreeItem() && lstSaleOrderVOEx.get(k).isHasFreeItem()) {
											maxVAT = vatMax - 1;
										} else {
											maxVAT = vatMax;
										}
										if ((lstSaleOrderVOEx.get(j).getConLai() + lstSaleOrderVOEx.get(k).getConLai()) >= maxVAT) { // co the gop lai thanh 1 don vat
											lstSaleOrderVOEx.get(j).setAmount(lstSaleOrderVOEx.get(j).getAmount().add(lstSaleOrderVOEx.get(k).getAmount()));
											lstSaleOrderVOEx.get(j).setDiscount(lstSaleOrderVOEx.get(j).getDiscount().add(lstSaleOrderVOEx.get(k).getDiscount()));
											List<SaleOrderDetail> lstgop = new ArrayList<SaleOrderDetail>();
											lstgop.addAll(lstSaleOrderVOEx.get(j).getLstSaleOrderDetail());
											lstgop.addAll(lstSaleOrderVOEx.get(k).getLstSaleOrderDetail());
											lstSaleOrderVOEx.get(j).setLstSaleOrderDetail(lstgop);
											lstSaleOrderVOEx.get(j).setConLai(vatMax - 1 - lstSaleOrderVOEx.get(j).getLstSaleOrderDetail().size());
											lstSaleOrderVOEx.remove(k);
											k--;
										}
									}
								}
							}
							//xu ly cho listConLai 
							if (!lstConLai.isEmpty()) {
								for (int n = sizeLstSaleOrder1; n < lstSaleOrderVOEx.size(); n++) {
									int conLai = lstSaleOrderVOEx.get(n).getConLai();
									if (conLai > 0) {
										for (int m = 0; m < conLai; m++) {
											if (lstConLai.size() > 0) {
												lstSaleOrderVOEx.get(n).setAmount(lstSaleOrderVOEx.get(n).getAmount().add(lstConLai.get(m).getAmount()));
												lstSaleOrderVOEx.get(n).setDiscount(lstSaleOrderVOEx.get(n).getDiscount().add(lstConLai.get(m).getDiscountAmount()));
												lstSaleOrderVOEx.get(n).getLstSaleOrderDetail().add(lstConLai.get(m));
												lstConLai.remove(m);
												m--;
												conLai--;
											}
										}
									}
								}
							}
							idx = 1;
							if (!lstConLai.isEmpty()) {
								if (lstConLai.size() <= vatMax) {
									SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
									saleOrderVOEx.setSaleOrder(so);
									saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
									saleOrderVOEx.setAmount(BigDecimal.ZERO);
									saleOrderVOEx.setDiscount(BigDecimal.ZERO);
									saleOrderVOEx.setConLai(vatMax - lstConLai.size()); //so luong dong hang con co the chua trong 1 don vat
									for (SaleOrderDetail d : lstConLai) {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
									}
									lstSaleOrderVOEx.add(saleOrderVOEx);
								} else {
									SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
									saleOrderVOEx.setSaleOrder(so);
									saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
									saleOrderVOEx.setAmount(BigDecimal.ZERO);
									saleOrderVOEx.setDiscount(BigDecimal.ZERO);
									saleOrderVOEx.setConLai(vatMax);
									lstSaleOrderVOEx.add(saleOrderVOEx);
									for (SaleOrderDetail d : lstConLai) {
										if ((idx == lstConLai.size()) || (idx % vatMax == 0)) {
											if (idx != lstConLai.size()) {
												saleOrderVOEx.getLstSaleOrderDetail().add(d);
												saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
												saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
												saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
												saleOrderVOEx = new SaleOrderVOEx();
												saleOrderVOEx.setSaleOrder(so);
												saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
												saleOrderVOEx.setAmount(BigDecimal.ZERO);
												saleOrderVOEx.setDiscount(BigDecimal.ZERO);
												saleOrderVOEx.setConLai(vatMax);
												lstSaleOrderVOEx.add(saleOrderVOEx);
											} else {
												saleOrderVOEx.getLstSaleOrderDetail().add(d);
												saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
												saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
												saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
											}
										} else {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
											saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										}
										idx++;
									}
								}
							}
						}
					}
					//da tach hoa don xong

					if (lstInvoiceNumber.size() != lstSaleOrderVOEx.size()) {
						throw new IllegalArgumentException("number of lstInvoiceNumber is not equals with list seperated sale orders");
					}

					Date lockDate = shopLockDAO.getNextLockedDay(shopId);
					if (lockDate == null) {
						lockDate = commonDAO.getSysDate();
					}

					for (int i = 0; i < lstInvoiceNumber.size(); i++) {
						String invoiceNumber = lstInvoiceNumber.get(i).toUpperCase();
						SaleOrder saleOrder = lstSaleOrderVOEx.get(i).getSaleOrder();
						List<SaleOrderDetail> lstSOD = lstSaleOrderVOEx.get(i).getLstSaleOrderDetail();

						Invoice existInvoice = invoiceDAO.getInvoiceByInvoiceNumber(invoiceNumber, shopId);
						if (existInvoice == null) {
							// chua co invoice nay trong db -> them moi
							Invoice __newInvoice = new Invoice();
							__newInvoice.setInvoiceNumber(invoiceNumber);
							__newInvoice.setStatus(InvoiceStatus.USING);
							__newInvoice.setCreateUser(username);
							__newInvoice.setCreateDate(commonDAO.getSysDate());
							__newInvoice.setOrderDate(saleOrder.getOrderDate());
							if (lstSOD.get(0).getVat() == 0 || lstSOD.get(0).getVat() == null) {
								__newInvoice.setVat(this.getVATListSaleOrderDetail(lstSaleOrderVOEx.get(i).getLstSaleOrderDetail()));
							} else {
								__newInvoice.setVat(lstSOD.get(0).getVat());
							}
							__newInvoice.setAmount(lstSaleOrderVOEx.get(i).getAmount());

							//TUNGTT
							__newInvoice.setDiscount(lstSaleOrderVOEx.get(i).getDiscount());

							__newInvoice.setStaff(saleOrder.getStaff());
							Float __newInvoiceVat = __newInvoice.getVat();
							if (__newInvoiceVat != null) {
								BigDecimal taxAmount = null;
								if (__newInvoice.getAmount() != null) {
									taxAmount = (__newInvoice.getAmount().subtract(__newInvoice.getDiscount())).multiply(new BigDecimal(__newInvoiceVat));
									taxAmount = taxAmount.divide(new BigDecimal(100 + __newInvoiceVat), 0, RoundingMode.HALF_UP);
									__newInvoice.setTaxAmount(taxAmount);
								} else {
									__newInvoice.setTaxAmount(BigDecimal.ZERO);
								}
							}
							__newInvoice.setSelName(saleOrder.getStaff().getStaffName());
							if (saleOrder.getDelivery() != null) {
								__newInvoice.setDeliveryStaff(saleOrder.getDelivery());
								__newInvoice.setDeliveryName(saleOrder.getDelivery().getStaffName());
							} else {
								throw new BusinessException("the sale order need to update dilivery:" + saleOrder.getOrderNumber());
							}
							__newInvoice.setOrderNumber(saleOrder.getOrderNumber());
							__newInvoice.setSku(lstSaleOrderVOEx.get(i).getSku());
							__newInvoice.setShop(saleOrder.getShop());

							if (saleOrder.getCustomer() != null) {
								__newInvoice.setCustomerCode(saleOrder.getCustomer().getShortCode());
								__newInvoice.setCustName(saleOrder.getCustomer().getCustomerName());
								__newInvoice.setCustomerAddr(saleOrder.getCustomer().getAddress());
								__newInvoice.setCustTaxNumber(saleOrder.getCustomer().getInvoiceTax());
								__newInvoice.setCustDelAddr(saleOrder.getCustomer().getDeliveryAddress());
								if (InvoiceCustPayment.MONEY.equals(lstCustPayment.get(i))) {
									__newInvoice.setCustBankName(null);
									__newInvoice.setCustBankAccount(null);
								} else {
									__newInvoice.setCustBankName(saleOrder.getCustomer().getInvoiceNameBank());
									__newInvoice.setCustBankAccount(saleOrder.getCustomer().getInvoiceNumberAccount());
								}
							}

							if (saleOrder.getShop() != null) {
								__newInvoice.setCpnyName(saleOrder.getShop().getShopName());
								__newInvoice.setCpnyAddr(saleOrder.getShop().getAddress());
								__newInvoice.setCpnyBankName(saleOrder.getShop().getInvoiceBankName());
								__newInvoice.setCpnyBankAccount(saleOrder.getShop().getInvoiceNumberAccount());
								__newInvoice.setCpnyTaxNumber(saleOrder.getShop().getTaxNum());
								__newInvoice.setCpnyPhone(saleOrder.getShop().getPhone());
							}

							//TUNGTT: them sale_order
							__newInvoice.setSaleOrder(saleOrder);

							//TUNGTT set theo ngay chot
							__newInvoice.setInvoiceDate(null);
							__newInvoice.setCustPayment(lstCustPayment.get(i));
							__newInvoice.setSaleOrderType(0); // don le

							Invoice newInvoice = invoiceDAO.createInvoice(__newInvoice);

							for (int k = 0; k < lstSOD.size(); k++) {
								if (lstSOD.get(k).getId() == null) {
									continue;
								}
								SaleOrderDetailVATFilter filter = new SaleOrderDetailVATFilter();
								filter.setProductId(lstSOD.get(k).getProduct().getId());
								filter.setVat(lstSOD.get(k).getVat());
								List<Long> lstSOId = new ArrayList<Long>();
								lstSOId.add(saleOrder.getId());
								filter.setLstSOId(lstSOId);
								if (lstSOD.get(k).getIsFreeItem().intValue() == 1) {
									filter.setIsFreeItem(1);
								} else {
									filter.setIsFreeItem(0);	
								}
								List<SaleOrderDetail> lstDetail1 = null;
								lstDetail1=saleOrderDAO.getListSaleOrderDetailByFilter(filter);
								if (lstDetail1 != null && !lstDetail1.isEmpty()) {
									for (SaleOrderDetail sod : lstDetail1) {
										sod.setInvoice(newInvoice);
										sod.setUpdateDate(date);
										sod.setUpdateUser(username);
										saleOrderDetailDAO.updateSaleOrderDetail(sod);
									}
								}
								//TUNGTT insert sale_order_detail vao invoice_detail
								InvoiceDetail invoiceDetail = new InvoiceDetail();
								if (lstSOD.get(k).getProduct() != null) {
									invoiceDetail.setProduct(lstSOD.get(k).getProduct());
								}
								if (lstSOD.get(k).getQuantity() != null) {
									invoiceDetail.setQuantity(lstSOD.get(k).getQuantity());
								}
								if (lstSOD.get(k).getPrice() != null) {
									invoiceDetail.setPrice(lstSOD.get(k).getPrice());
								}
								if (lstSOD.get(k).getDiscountAmount() != null) {
									invoiceDetail.setDiscountAmount(lstSOD.get(k).getDiscountAmount());
								}
								if (lstSOD.get(k).getDiscountPercent() != null) {
									invoiceDetail.setDiscountPercent(lstSOD.get(k).getDiscountPercent());
								}
								if (lstSOD.get(k).getAmount() != null) {
									invoiceDetail.setAmount(lstSOD.get(k).getAmount());
								}
								if (lstSOD.get(k).getVat() != null) {
									invoiceDetail.setVat(lstSOD.get(k).getVat());
								}
								if (lstSOD.get(k).getIsFreeItem() != null) {
									invoiceDetail.setIsFreeItem(lstSOD.get(k).getIsFreeItem());
								}
								if (lstSOD.get(k).getPriceValue() != null) {
									invoiceDetail.setPriceValue(lstSOD.get(k).getPriceValue());
								}
								if (lstSOD.get(k).getPriceNotVat() != null) {
									invoiceDetail.setPriceNotVat(lstSOD.get(k).getPriceNotVat());
								}
								invoiceDetail.setSaleOrderDetail(lstSOD.get(k));
//								System.out.println(invoiceDetail.getSaleOrderDetail().getQuantity());
								invoiceDetail.setInvoice(newInvoice);
								invoiceDetail.setCreateUser(username);
								invoiceDetail.setCreateDate(commonDAO.getSysDate());
								invoiceDAO.createInvoiceDetail(invoiceDetail);

							}
						} else if (InvoiceStatus.USING.getValue().equals(existInvoice.getStatus().getValue()) || InvoiceStatus.CANCELED.getValue().equals(existInvoice.getStatus().getValue())) {
							throw new IllegalArgumentException("number of lstInvoiceNumber is not equals with list seperated sale orders");
						} else if (InvoiceStatus.MODIFIED.getValue().equals(existInvoice.getStatus().getValue())) {
							// da co invoice trung db->cap nhat
							existInvoice.setStatus(InvoiceStatus.USING);
							existInvoice.setUpdateDate(commonDAO.getSysDate());
							existInvoice.setUpdateUser(username);

							existInvoice.setOrderDate(saleOrder.getOrderDate());
							existInvoice.setStaff(saleOrder.getStaff());

							if (lstSOD.get(0).getVat() == 0 || lstSOD.get(0).getVat() == null) {
								existInvoice.setVat(this.getVATListSaleOrderDetail(lstSaleOrderVOEx.get(i).getLstSaleOrderDetail()));
							} else {
								existInvoice.setVat(lstSOD.get(0).getVat());
							}

							existInvoice.setAmount(lstSaleOrderVOEx.get(i).getAmount());

							//TUNGTT
							existInvoice.setDiscount(lstSaleOrderVOEx.get(i).getDiscount());

							Float __existInvoiceVat = existInvoice.getVat();

							BigDecimal taxAmount = null;
							if (existInvoice.getAmount() != null) {
								taxAmount = (existInvoice.getAmount().subtract(existInvoice.getDiscount())).multiply(new BigDecimal(__existInvoiceVat));
								taxAmount = taxAmount.divide(new BigDecimal(100 + __existInvoiceVat), 0, RoundingMode.HALF_UP);
								existInvoice.setTaxAmount(taxAmount);

							} else {
								existInvoice.setTaxAmount(BigDecimal.ZERO);
							}

							existInvoice.setSelName(saleOrder.getStaff().getStaffName());
							if (saleOrder.getDelivery() != null) {
								saleOrder.setDelivery(saleOrder.getDelivery());
								existInvoice.setDeliveryName(saleOrder.getDelivery().getStaffName());
							} else {
								throw new BusinessException("the sale order need to update dilivery:" + saleOrder.getOrderNumber());
							}
							existInvoice.setOrderNumber(saleOrder.getOrderNumber());
							existInvoice.setSku(lstSaleOrderVOEx.get(i).getSku());
							existInvoice.setShop(saleOrder.getShop());

							if (saleOrder.getCustomer() != null) {
								existInvoice.setCustomerCode(saleOrder.getCustomer().getShortCode());
								existInvoice.setCustName(saleOrder.getCustomer().getCustomerName());
								existInvoice.setCustomerAddr(saleOrder.getCustomer().getAddress());
								existInvoice.setCustTaxNumber(saleOrder.getCustomer().getInvoiceTax());
								existInvoice.setCustDelAddr(saleOrder.getCustomer().getDeliveryAddress());
								if (InvoiceCustPayment.MONEY.equals(lstCustPayment.get(i))) {
									existInvoice.setCustBankName(null);
									existInvoice.setCustBankAccount(null);
								} else {
									existInvoice.setCustBankName(saleOrder.getCustomer().getInvoiceNameBank());
									existInvoice.setCustBankAccount(saleOrder.getCustomer().getInvoiceNumberAccount());
								}
							}

							if (saleOrder.getShop() != null) {
								existInvoice.setCpnyName(saleOrder.getShop().getShopName());
								existInvoice.setCpnyAddr(saleOrder.getShop().getAddress());
								existInvoice.setCpnyBankName(saleOrder.getShop().getInvoiceBankName());
								existInvoice.setCpnyBankAccount(saleOrder.getShop().getInvoiceNumberAccount());
								existInvoice.setCpnyTaxNumber(saleOrder.getShop().getTaxNum());
								existInvoice.setCpnyPhone(saleOrder.getShop().getPhone());
							}

							//TUNGTT set theo ngay chot
							existInvoice.setInvoiceDate(null);
							existInvoice.setCustPayment(lstCustPayment.get(i));
							invoiceDAO.updateInvoice(existInvoice);

							for (int k = 0; k < lstSOD.size(); k++) {
								lstSOD.get(k).setInvoice(existInvoice);
								saleOrderDetailDAO.updateSaleOrderDetail(lstSOD.get(k));
							}
						}
					}
				} else {
					for (int i = 0; i < lstInvoiceNumber.size(); i++) {
						Invoice oldInvoice = listRemovedInvoice.get(i);
						Date lockDate = shopLockDAO.getNextLockedDay(shopId);
						if (lockDate == null) {
							lockDate = commonDAO.getSysDate();
						}
						Invoice newInvoice = oldInvoice.clone();
						newInvoice.setInvoiceDate(null);
						newInvoice.setInvoiceNumber(lstInvoiceNumber.get(i).toUpperCase());
						newInvoice.setStatus(InvoiceStatus.USING);
						newInvoice.setCreateDate(commonDAO.getSysDate());
						newInvoice.setCreateUser(username);

						newInvoice = invoiceDAO.createInvoice(newInvoice);

						List<SaleOrderDetail> listSODRemoved = saleOrderDetailDAO.getListDetailByInvoiceAndInvoceDetail(oldInvoice.getId());

						for (int k = 0; k < listSODRemoved.size(); k++) {
							listSODRemoved.get(k).setInvoice(newInvoice);
							saleOrderDetailDAO.updateSaleOrderDetail(listSODRemoved.get(k));

							//TUNGTT insert sale_order_detail vao invoice_detail
							InvoiceDetail invoiceDetail = new InvoiceDetail();
							if (listSODRemoved.get(k).getProduct() != null) {
								invoiceDetail.setProduct(listSODRemoved.get(k).getProduct());
							}
							if (listSODRemoved.get(k).getQuantity() != null) {
								invoiceDetail.setQuantity(listSODRemoved.get(k).getQuantity());
							}
							if (listSODRemoved.get(k).getPrice() != null) {
								invoiceDetail.setPrice(listSODRemoved.get(k).getPrice());
							}
							if (listSODRemoved.get(k).getDiscountAmount() != null) {
								invoiceDetail.setDiscountAmount(listSODRemoved.get(k).getDiscountAmount());
							}
							if (listSODRemoved.get(k).getDiscountPercent() != null) {
								invoiceDetail.setDiscountPercent(listSODRemoved.get(k).getDiscountPercent());
							}
							if (listSODRemoved.get(k).getAmount() != null) {
								invoiceDetail.setAmount(listSODRemoved.get(k).getAmount());
							}
							if (listSODRemoved.get(k).getVat() != null) {
								invoiceDetail.setVat(listSODRemoved.get(k).getVat());
							}
							if (listSODRemoved.get(k).getIsFreeItem() != null) {
								invoiceDetail.setIsFreeItem(listSODRemoved.get(k).getIsFreeItem());
							}
							if (listSODRemoved.get(k).getPriceValue() != null) {
								invoiceDetail.setPriceValue(listSODRemoved.get(k).getPriceValue());
							}
							if (listSODRemoved.get(k).getPriceNotVat() != null) {
								invoiceDetail.setPriceNotVat(listSODRemoved.get(k).getPriceNotVat());
							}
							invoiceDetail.setSaleOrderDetail(listSODRemoved.get(k));
							invoiceDetail.setInvoice(newInvoice);
							invoiceDetail.setCreateUser(username);
							invoiceDetail.setCreateDate(commonDAO.getSysDate());
							invoiceDAO.createInvoiceDetail(invoiceDetail);
						}
					}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * @Override
	 * 
	 * @Transactional(rollbackFor = Exception.class) public void
	 * createInvoiceFromSaleOrder(Long shopId, String username, Map<Long,
	 * List<String>> listSaleOrder, Map<Long, List<InvoiceCustPayment>>
	 * listCustPayment) throws BusinessException { try {
	 * 
	 * Báº£ng SALE_ORDER: SKU = sáº½ Ä‘áº¿m cÃ¡c máº·t hÃ ng trong hÃ³a Ä‘Æ¡n (báº£ng
	 * SALE_ORDER_DETAIL, trÆ°á»�ng IS_FREE_ITEM = 0):
	 * 
	 * if (shopId == null) { throw new
	 * IllegalArgumentException("shopId is null"); } if (listSaleOrder == null)
	 * { throw new IllegalArgumentException("listSaleOrder is null"); } if
	 * (listCustPayment == null) { throw new
	 * IllegalArgumentException("listCustPayment is null"); }
	 * 
	 * Set<Long> setSOId = listSaleOrder.keySet(); //Iterator<Long> soIterator =
	 * setSOId.iterator();
	 * 
	 * ApParam apParamVATMax =
	 * apParamDAO.getApParamByCode("MAXVAT",ActiveType.RUNNING);
	 * 
	 * List<SaleOrder> lstSO =
	 * saleOrderDAO.getListSaleOrderByIterator(setSOId.iterator());
	 * 
	 * List<SaleOrderDetail> lstAllSOD =
	 * saleOrderDAO.getListSaleOrderDetailByIterator(setSOId.iterator());
	 * 
	 * Map<Long, List<SaleOrderDetail>> mapSO = new HashMap<Long,
	 * List<SaleOrderDetail>>(); Long saleOrderId = null; for(int i = 0, sizeAll
	 * = lstAllSOD.size(); i < sizeAll; i++){
	 * if(lstAllSOD.get(i).getSaleOrder().getId() != saleOrderId){ saleOrderId =
	 * lstAllSOD.get(i).getSaleOrder().getId(); List<SaleOrderDetail> list = new
	 * ArrayList<SaleOrderDetail>(); list.add(lstAllSOD.get(i));
	 * mapSO.put(saleOrderId, list); }else{
	 * mapSO.get(saleOrderId).add(lstAllSOD.get(i)); } }
	 * 
	 * //while (soIterator.hasNext()) { for (SaleOrder so : lstSO) { //Long soId
	 * = soIterator.next(); List<String> lstInvoiceNumber =
	 * listSaleOrder.get(so.getId()); List<InvoiceCustPayment> lstCustPayment =
	 * listCustPayment.get(so.getId()); List<SaleOrderVOEx> lstSaleOrderVOEx =
	 * new ArrayList<SaleOrderVOEx>(); // tach hoa don int vatMax =
	 * Integer.valueOf(apParamVATMax.getValue());
	 * 
	 * List<Invoice> listRemovedInvoice =
	 * invoiceDAO.checkHasInvoiceRemoveOfSaleOrder(so.getId());
	 * 
	 * if(listRemovedInvoice == null || listRemovedInvoice.size() == 0){ //get
	 * list km tien BigDecimal allDiscount = so.getDiscount();
	 * 
	 * //lay tat ca cac sod tru sod khuyen mai tien List<SaleOrderDetail>
	 * listSOD = mapSO.get(so.getId());
	 * 
	 * //tach thanh cac list co VAT khac nhau List<List<SaleOrderDetail>>
	 * listVat = new ArrayList<List<SaleOrderDetail>>(); List<SaleOrderDetail>
	 * tmp = new ArrayList<SaleOrderDetail>(); listVat.add(tmp);
	 * 
	 * if(listSOD != null && listSOD.size() > 0 ){ Float currentVAT = 0F; for
	 * (int i = 0, size = listSOD.size(); i < size; i++) {
	 * if(listSOD.get(i).getIsFreeItem() == 0 && listSOD.get(i).getVat() != null
	 * && listSOD.get(i).getVat() > 0){ if(currentVAT == 0){
	 * tmp.add(listSOD.get(i)); currentVAT = listSOD.get(i).getVat(); } else if
	 * (!currentVAT.equals(listSOD.get(i).getVat())) { tmp = new
	 * ArrayList<SaleOrderDetail>(); listVat.add(tmp); currentVAT =
	 * listSOD.get(i).getVat(); tmp.add(listSOD.get(i)); }else{
	 * tmp.add(listSOD.get(i)); } }else{ tmp.add(listSOD.get(i)); } }
	 * 
	 * //tach thanh cac co cau khac nhau: for (int i = 0, size1 =
	 * listVat.size(); i < size1; i++){ List<SaleOrderDetail> listDetail =
	 * listVat.get(i);
	 * 
	 * if((checkListSaleOrderDetail(listDetail) &&
	 * allDiscount.compareTo(BigDecimal.ZERO) == 0 && listDetail.size() <=
	 * vatMax) || (!checkListSaleOrderDetail(listDetail) &&
	 * allDiscount.compareTo(BigDecimal.ZERO) == 0 && listDetail.size() <=
	 * vatMax - 1) || (!checkListSaleOrderDetail(listDetail) &&
	 * allDiscount.compareTo(BigDecimal.ZERO) > 0 && listDetail.size() <= vatMax
	 * - 2)){ SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setRowId(so.getId() + "-" + (lstSaleOrderVOEx.size() + 1));
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setDiscount(BigDecimal.ZERO);
	 * lstSaleOrderVOEx.add(saleOrderVOEx); for (int j = 0; j <
	 * listDetail.size(); j++) {
	 * saleOrderVOEx.getLstSaleOrderDetail().add(listDetail.get(j));
	 * saleOrderVOEx
	 * .setAmount(saleOrderVOEx.getAmount().add(listDetail.get(j).getAmount()));
	 * } }else{ int index = 1; int iRun = 0; SaleOrderVOEx saleOrderVOEx = new
	 * SaleOrderVOEx(); saleOrderVOEx.setRowId(so.getId() + "-" +
	 * (lstSaleOrderVOEx.size() + 1)); saleOrderVOEx.setSaleOrder(so);
	 * saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
	 * saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setDiscount(BigDecimal.ZERO);
	 * lstSaleOrderVOEx.add(saleOrderVOEx);
	 * 
	 * for (int j = 0; j < listDetail.size(); j++) { if (iRun == vatMax - 1) {
	 * // tach lun index++; saleOrderVOEx = new SaleOrderVOEx();
	 * saleOrderVOEx.setRowId(so.getId() + "-" + (lstSaleOrderVOEx.size() + 1));
	 * saleOrderVOEx.setSaleOrder(so); saleOrderVOEx.setLstSaleOrderDetail(new
	 * ArrayList<SaleOrderDetail>()); saleOrderVOEx.setAmount(BigDecimal.ZERO);
	 * saleOrderVOEx.setDiscount(BigDecimal.ZERO);
	 * lstSaleOrderVOEx.add(saleOrderVOEx); iRun = 1; } else { iRun++; }
	 * saleOrderVOEx.getLstSaleOrderDetail().add(listDetail.get(j));
	 * saleOrderVOEx
	 * .setAmount(saleOrderVOEx.getAmount().add(listDetail.get(j).getAmount()));
	 * } } } //set discount for (int l = 0, size = lstSaleOrderVOEx.size(); l <
	 * size; l++){ SaleOrderVOEx saleOrderVOEx = lstSaleOrderVOEx.get(l);
	 * if(saleOrderVOEx != null){ List<SaleOrderDetail> listDetail =
	 * saleOrderVOEx.getLstSaleOrderDetail();
	 * if(saleOrderVOEx.getAmount().compareTo(allDiscount) > 0){
	 * if(listDetail.size() <= vatMax - 2){
	 * saleOrderVOEx.setDiscount(allDiscount); break; }else
	 * if(checkListSaleOrderDetail(listDetail)){
	 * saleOrderVOEx.setDiscount(allDiscount); break; } } } } } //da tach hoa
	 * don xong
	 * 
	 * if (lstInvoiceNumber.size() != lstSaleOrderVOEx.size()) { throw new
	 * IllegalArgumentException
	 * ("number of lstInvoiceNumber is not equals with list seperated sale orders"
	 * ); }
	 * 
	 * Date lockDate = shopLockDAO.getNextLockedDay(shopId); if(lockDate ==
	 * null){ lockDate = commonDAO.getSysDate(); }
	 * 
	 * for (int i = 0; i < lstInvoiceNumber.size(); i++) { String invoiceNumber
	 * = lstInvoiceNumber.get(i).toUpperCase(); SaleOrder saleOrder =
	 * lstSaleOrderVOEx.get(i).getSaleOrder(); List<SaleOrderDetail> lstSOD =
	 * lstSaleOrderVOEx.get(i).getLstSaleOrderDetail();
	 * 
	 * Invoice existInvoice =
	 * invoiceDAO.getInvoiceByInvoiceNumber(invoiceNumber, shopId); if
	 * (existInvoice == null) { // chua co invoice nay trong db -> them moi
	 * Invoice __newInvoice = new Invoice();
	 * __newInvoice.setInvoiceNumber(invoiceNumber);
	 * __newInvoice.setStatus(InvoiceStatus.USING);
	 * __newInvoice.setCreateUser(username);
	 * __newInvoice.setCreateDate(commonDAO.getSysDate());
	 * __newInvoice.setOrderDate(saleOrder.getOrderDate()); if
	 * (lstSOD.get(0).getVat() == 0 || lstSOD.get(0).getVat() == null) {
	 * __newInvoice
	 * .setVat(this.getVATListSaleOrderDetail(lstSaleOrderVOEx.get(i)
	 * .getLstSaleOrderDetail())); } else {
	 * __newInvoice.setVat(lstSOD.get(0).getVat()); }
	 * __newInvoice.setAmount(lstSaleOrderVOEx.get(i).getAmount());
	 * 
	 * //TUNGTT __newInvoice.setDiscount(lstSaleOrderVOEx.get(i).getDiscount());
	 * 
	 * __newInvoice.setStaff(saleOrder.getStaff()); Float __newInvoiceVat =
	 * __newInvoice.getVat(); if (__newInvoiceVat != null) { BigDecimal
	 * taxAmount = null; if (__newInvoice.getAmount() != null) { taxAmount =
	 * (__newInvoice
	 * .getAmount().subtract(__newInvoice.getDiscount())).multiply(new
	 * BigDecimal(__newInvoiceVat)); taxAmount = taxAmount.divide(new
	 * BigDecimal(100 + __newInvoiceVat), 0,RoundingMode.HALF_UP);
	 * __newInvoice.setTaxAmount(taxAmount); } else {
	 * __newInvoice.setTaxAmount(BigDecimal.ZERO); } }
	 * __newInvoice.setSelName(saleOrder.getStaff().getStaffName()); if
	 * (saleOrder.getDelivery() != null) {
	 * __newInvoice.setDeliveryStaff(saleOrder.getDelivery());
	 * __newInvoice.setDeliveryName(saleOrder.getDelivery().getStaffName()); }
	 * else { throw new
	 * BusinessException("the sale order need to update dilivery:" +
	 * saleOrder.getOrderNumber()); }
	 * __newInvoice.setOrderNumber(saleOrder.getOrderNumber());
	 * __newInvoice.setSku(lstSaleOrderVOEx.get(i).getSku());
	 * __newInvoice.setShop(saleOrder.getShop());
	 * 
	 * 
	 * if (saleOrder.getCustomer() != null) {
	 * __newInvoice.setCustomerCode(saleOrder .getCustomer().getShortCode());
	 * __newInvoice.setCustName(saleOrder.getCustomer() .getCustomerName());
	 * __newInvoice.setCustomerAddr(saleOrder .getCustomer().getAddress());
	 * __newInvoice.setCustTaxNumber(saleOrder .getCustomer().getInvoiceTax());
	 * __newInvoice.setCustDelAddr(saleOrder.getCustomer()
	 * .getDeliveryAddress()); if
	 * (InvoiceCustPayment.MONEY.equals(lstCustPayment .get(i))) {
	 * __newInvoice.setCustBankName(null);
	 * __newInvoice.setCustBankAccount(null); } else {
	 * __newInvoice.setCustBankName(saleOrder
	 * .getCustomer().getInvoiceNameBank());
	 * __newInvoice.setCustBankAccount(saleOrder .getCustomer()
	 * .getInvoiceNumberAccount()); } }
	 * 
	 * if (saleOrder.getShop() != null) {
	 * __newInvoice.setCpnyName(saleOrder.getShop() .getShopName());
	 * __newInvoice.setCpnyAddr(saleOrder.getShop() .getAddress());
	 * __newInvoice.setCpnyBankName(saleOrder.getShop() .getInvoiceBankName());
	 * __newInvoice.setCpnyBankAccount(saleOrder.getShop()
	 * .getInvoiceNumberAccount());
	 * __newInvoice.setCpnyTaxNumber(saleOrder.getShop() .getTaxNum());
	 * __newInvoice.setCpnyPhone(saleOrder.getShop() .getPhone()); }
	 * 
	 * //TUNGTT: them sale_order __newInvoice.setSaleOrder(saleOrder);
	 * 
	 * //TUNGTT set theo ngay chot __newInvoice.setInvoiceDate(null);
	 * __newInvoice.setCustPayment(lstCustPayment.get(i));
	 * 
	 * Invoice newInvoice = invoiceDAO.createInvoice(__newInvoice);
	 * 
	 * for (int k = 0; k < lstSOD.size(); k++) {
	 * lstSOD.get(k).setInvoice(newInvoice);
	 * saleOrderDetailDAO.updateSaleOrderDetail(lstSOD.get(k));
	 * 
	 * //TUNGTT insert sale_order_detail vao invoice_detail InvoiceDetail
	 * invoiceDetail = new InvoiceDetail(); if(lstSOD.get(k).getProduct() !=
	 * null){ invoiceDetail.setProduct(lstSOD.get(k).getProduct()); }
	 * if(lstSOD.get(k).getQuantity() != null){
	 * invoiceDetail.setQuantity(lstSOD.get(k).getQuantity()); }
	 * if(lstSOD.get(k).getPrice() != null){
	 * invoiceDetail.setPrice(lstSOD.get(k).getPrice()); }
	 * if(lstSOD.get(k).getDiscountAmount() != null){
	 * invoiceDetail.setDiscountAmount(lstSOD.get(k).getDiscountAmount()); }
	 * if(lstSOD.get(k).getDiscountPercent() != null){
	 * invoiceDetail.setDiscountPercent(lstSOD.get(k).getDiscountPercent()); }
	 * if(lstSOD.get(k).getAmount() != null){
	 * invoiceDetail.setAmount(lstSOD.get(k).getAmount()); }
	 * if(lstSOD.get(k).getVat() != null){
	 * invoiceDetail.setVat(lstSOD.get(k).getVat()); }
	 * if(lstSOD.get(k).getIsFreeItem() != null){
	 * invoiceDetail.setIsFreeItem(lstSOD.get(k).getIsFreeItem()); }
	 * if(lstSOD.get(k).getPriceValue() != null){
	 * invoiceDetail.setPriceValue(lstSOD.get(k).getPriceValue()); }
	 * if(lstSOD.get(k).getPriceNotVat() != null){
	 * invoiceDetail.setPriceNotVat(lstSOD.get(k).getPriceNotVat()); }
	 * invoiceDetail.setSaleOrderDetail(lstSOD.get(k));
	 * invoiceDetail.setInvoice(newInvoice);
	 * invoiceDetail.setCreateUser(username);
	 * invoiceDetail.setCreateDate(commonDAO.getSysDate());
	 * invoiceDAO.createInvoiceDetail(invoiceDetail);
	 * 
	 * } } else if (InvoiceStatus.USING.getValue().equals(
	 * existInvoice.getStatus().getValue()) ||
	 * InvoiceStatus.CANCELED.getValue().
	 * equals(existInvoice.getStatus().getValue())) { throw new
	 * IllegalArgumentException
	 * ("number of lstInvoiceNumber is not equals with list seperated sale orders"
	 * ); } else if
	 * (InvoiceStatus.MODIFIED.getValue().equals(existInvoice.getStatus
	 * ().getValue())) { // da co invoice trung db->cap nhat
	 * existInvoice.setStatus(InvoiceStatus.USING);
	 * existInvoice.setUpdateDate(commonDAO.getSysDate());
	 * existInvoice.setUpdateUser(username);
	 * 
	 * existInvoice.setOrderDate(saleOrder.getOrderDate());
	 * existInvoice.setStaff(saleOrder.getStaff());
	 * 
	 * if (lstSOD.get(0).getVat() == 0 || lstSOD.get(0).getVat() == null) {
	 * existInvoice
	 * .setVat(this.getVATListSaleOrderDetail(lstSaleOrderVOEx.get(i)
	 * .getLstSaleOrderDetail())); } else {
	 * existInvoice.setVat(lstSOD.get(0).getVat()); }
	 * 
	 * existInvoice.setAmount(lstSaleOrderVOEx.get(i) .getAmount());
	 * 
	 * //TUNGTT existInvoice.setDiscount(lstSaleOrderVOEx.get(i).getDiscount());
	 * 
	 * Float __existInvoiceVat = existInvoice.getVat();
	 * 
	 * BigDecimal taxAmount = null; if (existInvoice.getAmount() != null) {
	 * taxAmount =
	 * (existInvoice.getAmount().subtract(existInvoice.getDiscount())).multiply(
	 * new BigDecimal(__existInvoiceVat)); taxAmount = taxAmount.divide(new
	 * BigDecimal( 100 + __existInvoiceVat), 0, RoundingMode.HALF_UP);
	 * existInvoice.setTaxAmount(taxAmount);
	 * 
	 * } else { existInvoice.setTaxAmount(BigDecimal.ZERO); }
	 * 
	 * existInvoice.setSelName(saleOrder.getStaff() .getStaffName()); if
	 * (saleOrder.getDelivery() != null) {
	 * saleOrder.setDelivery(saleOrder.getDelivery());
	 * existInvoice.setDeliveryName(saleOrder .getDelivery().getStaffName()); }
	 * else { throw new BusinessException(
	 * "the sale order need to update dilivery:" + saleOrder.getOrderNumber());
	 * } existInvoice.setOrderNumber(saleOrder.getOrderNumber());
	 * existInvoice.setSku(lstSaleOrderVOEx.get(i).getSku());
	 * existInvoice.setShop(saleOrder.getShop());
	 * 
	 * if (saleOrder.getCustomer() != null) {
	 * existInvoice.setCustomerCode(saleOrder .getCustomer().getShortCode());
	 * existInvoice.setCustName(saleOrder.getCustomer() .getCustomerName());
	 * existInvoice.setCustomerAddr(saleOrder .getCustomer().getAddress());
	 * existInvoice.setCustTaxNumber(saleOrder .getCustomer().getInvoiceTax());
	 * existInvoice.setCustDelAddr(saleOrder.getCustomer()
	 * .getDeliveryAddress()); if
	 * (InvoiceCustPayment.MONEY.equals(lstCustPayment .get(i))) {
	 * existInvoice.setCustBankName(null);
	 * existInvoice.setCustBankAccount(null); } else {
	 * existInvoice.setCustBankName(saleOrder
	 * .getCustomer().getInvoiceNameBank());
	 * existInvoice.setCustBankAccount(saleOrder .getCustomer()
	 * .getInvoiceNumberAccount()); } }
	 * 
	 * if (saleOrder.getShop() != null) {
	 * existInvoice.setCpnyName(saleOrder.getShop() .getShopName());
	 * existInvoice.setCpnyAddr(saleOrder.getShop() .getAddress());
	 * existInvoice.setCpnyBankName(saleOrder.getShop() .getInvoiceBankName());
	 * existInvoice.setCpnyBankAccount(saleOrder.getShop()
	 * .getInvoiceNumberAccount());
	 * existInvoice.setCpnyTaxNumber(saleOrder.getShop() .getTaxNum());
	 * existInvoice.setCpnyPhone(saleOrder.getShop() .getPhone()); }
	 * 
	 * //TUNGTT set theo ngay chot existInvoice.setInvoiceDate(null);
	 * existInvoice.setCustPayment(lstCustPayment.get(i));
	 * invoiceDAO.updateInvoice(existInvoice);
	 * 
	 * for (int k = 0; k < lstSOD.size(); k++) {
	 * lstSOD.get(k).setInvoice(existInvoice);
	 * saleOrderDetailDAO.updateSaleOrderDetail(lstSOD.get(k)); } } } }else{ for
	 * (int i = 0; i < lstInvoiceNumber.size(); i++) { Invoice oldInvoice =
	 * listRemovedInvoice.get(i); Date lockDate =
	 * shopLockDAO.getNextLockedDay(shopId); if(lockDate == null){ lockDate =
	 * commonDAO.getSysDate(); } Invoice newInvoice = oldInvoice.clone();
	 * newInvoice.setInvoiceDate(null);
	 * newInvoice.setInvoiceNumber(lstInvoiceNumber.get(i).toUpperCase());
	 * newInvoice.setStatus(InvoiceStatus.USING);
	 * newInvoice.setCreateDate(commonDAO.getSysDate());
	 * newInvoice.setCreateUser(username);
	 * 
	 * newInvoice = invoiceDAO.createInvoice(newInvoice);
	 * 
	 * List<SaleOrderDetail> listSODRemoved =
	 * saleOrderDetailDAO.getListDetailByInvoiceAndInvoceDetail
	 * (oldInvoice.getId());
	 * 
	 * for (int k = 0; k < listSODRemoved.size(); k++) {
	 * listSODRemoved.get(k).setInvoice(newInvoice);
	 * saleOrderDetailDAO.updateSaleOrderDetail(listSODRemoved.get(k));
	 * 
	 * //TUNGTT insert sale_order_detail vao invoice_detail InvoiceDetail
	 * invoiceDetail = new InvoiceDetail();
	 * if(listSODRemoved.get(k).getProduct() != null){
	 * invoiceDetail.setProduct(listSODRemoved.get(k).getProduct()); }
	 * if(listSODRemoved.get(k).getQuantity() != null){
	 * invoiceDetail.setQuantity(listSODRemoved.get(k).getQuantity()); }
	 * if(listSODRemoved.get(k).getPrice() != null){
	 * invoiceDetail.setPrice(listSODRemoved.get(k).getPrice()); }
	 * if(listSODRemoved.get(k).getDiscountAmount() != null){
	 * invoiceDetail.setDiscountAmount
	 * (listSODRemoved.get(k).getDiscountAmount()); }
	 * if(listSODRemoved.get(k).getDiscountPercent() != null){
	 * invoiceDetail.setDiscountPercent
	 * (listSODRemoved.get(k).getDiscountPercent()); }
	 * if(listSODRemoved.get(k).getAmount() != null){
	 * invoiceDetail.setAmount(listSODRemoved.get(k).getAmount()); }
	 * if(listSODRemoved.get(k).getVat() != null){
	 * invoiceDetail.setVat(listSODRemoved.get(k).getVat()); }
	 * if(listSODRemoved.get(k).getIsFreeItem() != null){
	 * invoiceDetail.setIsFreeItem(listSODRemoved.get(k).getIsFreeItem()); }
	 * if(listSODRemoved.get(k).getPriceValue() != null){
	 * invoiceDetail.setPriceValue(listSODRemoved.get(k).getPriceValue()); }
	 * if(listSODRemoved.get(k).getPriceNotVat() != null){
	 * invoiceDetail.setPriceNotVat(listSODRemoved.get(k).getPriceNotVat()); }
	 * invoiceDetail.setSaleOrderDetail(listSODRemoved.get(k));
	 * invoiceDetail.setInvoice(newInvoice);
	 * invoiceDetail.setCreateUser(username);
	 * invoiceDetail.setCreateDate(commonDAO.getSysDate());
	 * invoiceDAO.createInvoiceDetail(invoiceDetail); } } } } } catch
	 * (DataAccessException e) { throw new BusinessException(e); } }
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createInvoiceFromSaleOrderEx(Long shopId, String username, Map<Long, List<String>> listSaleOrder, Map<Long, List<InvoiceCustPayment>> listCustPayment, SaleOrder donChinh, List<SaleOrder> lstSO1, List<SaleOrderDetail> lstDetailGop)
			throws BusinessException {
		try {
			/*
			 * Báº£ng SALE_ORDER: SKU = sáº½ Ä‘áº¿m cÃ¡c máº·t hÃ ng trong hÃ³a Ä‘Æ¡n (báº£ng
			 * SALE_ORDER_DETAIL, trÆ°á»�ng IS_FREE_ITEM = 0):
			 */
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (listSaleOrder == null) {
				throw new IllegalArgumentException("listSaleOrder is null");
			}
			if (listCustPayment == null) {
				throw new IllegalArgumentException("listCustPayment is null");
			}
			Date date = commonDAO.getSysDate();
			CompoundSaleOrder cso = new CompoundSaleOrder();
			if (donChinh != null) {
				cso.setCreateDate(DateUtility.now());
				cso.setCreateUser(username);
				cso.setPriSaleOrder(donChinh);
				cso.setPriOrderDate(donChinh.getOrderDate());
				cso.setPriOrderNumber(donChinh.getOrderNumber());
				cso.setShop(donChinh.getShop());
				cso.setShopCode(donChinh.getShop().getShopCode());
				cso = compoundSaleOrderDAO.createCompoundSaleOrder(cso);
				for (int i = 0; i < lstSO1.size(); i++) {
					lstSO1.get(i).setParentCP(cso.getId());
					if (donChinh.getId().equals(lstSO1.get(i).getId())) {
						lstSO1.get(i).setIsPrimary(1);
					} else {
						lstSO1.get(i).setIsPrimary(0);
					}
					lstSO1.get(i).setUpdateUser(username);
					lstSO1.get(i).setUpdateDate(date);
				}
				saleOrderDAO.updateSaleOrder(lstSO1);
			}
			//Iterator<Long> soIterator = setSOId.iterator();

			ApParam apParamVATMax = apParamDAO.getApParamByCode("MAXVAT", ActiveType.RUNNING);

			List<SaleOrderDetail> lstAllSOD = sortVAT(lstDetailGop);

			//Long soId = soIterator.next();
			List<String> lstInvoiceNumber = listSaleOrder.get(donChinh.getId());
			List<InvoiceCustPayment> lstCustPayment = listCustPayment.get(donChinh.getId());
			List<SaleOrderVOEx> lstSaleOrderVOEx = new ArrayList<SaleOrderVOEx>();

			// tach hoa don
			int vatMax = Integer.valueOf(apParamVATMax.getValue());

			List<Invoice> listRemovedInvoice = invoiceDAO.checkHasInvoiceRemoveOfSaleOrder(donChinh.getId());

			if (listRemovedInvoice == null || listRemovedInvoice.size() == 0) {
				//lay tat ca cac sod tru sod khuyen mai tien
				List<SaleOrderDetail> listSOD = gopSanPham(lstAllSOD, 1);

				//tach thanh cac list co VAT khac nhau
				List<List<SaleOrderDetail>> listVat = new ArrayList<List<SaleOrderDetail>>();
				List<SaleOrderDetail> tmp = new ArrayList<SaleOrderDetail>();
				listVat.add(tmp);
				if (listSOD != null && listSOD.size() > 0) {
					Float currentVAT = null;
					for (int i = 0, size = listSOD.size(); i < size; i++) {
						if (listSOD.get(i).getVat() != null && listSOD.get(i).getVat() > 0) {
							if (currentVAT == null) {
								tmp.add(listSOD.get(i));
								currentVAT = listSOD.get(i).getVat();
							} else if (!currentVAT.equals(listSOD.get(i).getVat())) {
								tmp = new ArrayList<SaleOrderDetail>();
								listVat.add(tmp);
								currentVAT = listSOD.get(i).getVat();
								tmp.add(listSOD.get(i));
							} else {
								tmp.add(listSOD.get(i));
							}
						} else {
							if (currentVAT != null && currentVAT > 0) {
								tmp = new ArrayList<SaleOrderDetail>();
								listVat.add(tmp);
								currentVAT = listSOD.get(i).getVat();
								tmp.add(listSOD.get(i));
							} else {
								tmp.add(listSOD.get(i));
							}
						}
					}

					//tach thanh cac co cau khac nhau:
					for (int i = 0, size1 = listVat.size(); i < size1; i++) {
						int sizeLstSaleOrder1 = lstSaleOrderVOEx.size();
						//moi VAT tach theo km + chiet khau
						List<SaleOrderDetail> lstSPCoCK = new ArrayList<SaleOrderDetail>();
						List<SaleOrderDetail> lstSPKoCoCK = new ArrayList<SaleOrderDetail>(); // co km sp
						List<SaleOrderDetail> lstConLai = new ArrayList<SaleOrderDetail>();// sp ban ko co khuye mai gi het
						List<SaleOrderDetail> listDetail = listVat.get(i);
						for (SaleOrderDetail sod : listDetail) {
							if (sod.getDiscountAmount() != null && sod.getDiscountAmount().doubleValue() > 0) {
								lstSPCoCK.add(sod);
							} else {
								if (sod.getProgramType() != null && sod.getProgramType().equals(ProgramType.AUTO_PROM) && sod.getProgramCode() != null) {
									lstSPKoCoCK.add(sod);
								} else {
									lstConLai.add(sod);
								}
							}
						}

						List<SaleOrderDetail> lstCoCK = new ArrayList<SaleOrderDetail>();// sp ban co ck

						int index = 1;
						BigDecimal ck = BigDecimal.ZERO;
						for (int j = 0; j < lstSPCoCK.size(); j++) {
							lstCoCK.add(lstSPCoCK.get(j));
							if ((index == lstSPCoCK.size()) || (index % (vatMax - 1) == 0)) {
								SaleOrderDetail detailCK = new SaleOrderDetail();
								//								ck = ck.add(lstSPCoCK.get(j).getDiscountAmount());
								detailCK.setDiscountAmount(ck);
								detailCK.setSaleOrder(lstSPCoCK.get(j).getSaleOrder());
								detailCK.setShop(lstSPCoCK.get(j).getShop());
								detailCK.setIsFreeItem(0);
								lstCoCK.add(detailCK);
								//								index= 1;
								index++;
								ck = BigDecimal.ZERO;
							} else {
								//								ck = ck.add(lstSPCoCK.get(j).getDiscountAmount());
								index++;
							}
						}
						List<SaleOrderDetailForInvoidVO> lstSPBanVaKhuyenMai = new ArrayList<SaleOrderDetailForInvoidVO>();
						SaleOrderDetailForInvoidVO tmpSOD = new SaleOrderDetailForInvoidVO();
						lstSPBanVaKhuyenMai.add(tmpSOD);
						String progrCode = "";
						for (SaleOrderDetail sod : lstSPKoCoCK) {
							if (StringUtility.isNullOrEmpty(progrCode)) {
								if (sod.getIsFreeItem().intValue() == 1) {
									tmpSOD.getLstKhuyenMai().add(sod);
								} else {
									tmpSOD.getLstSPBan().add(sod);
								}
								progrCode = sod.getProgramCode();
							} else if (!progrCode.equals(sod.getProgramCode())) {
								tmpSOD = new SaleOrderDetailForInvoidVO();
								lstSPBanVaKhuyenMai.add(tmpSOD);
								progrCode = sod.getProgramCode();
								if (sod.getIsFreeItem().intValue() == 1) {
									tmpSOD.getLstKhuyenMai().add(sod);
								} else {
									tmpSOD.getLstSPBan().add(sod);
								}
							} else {
								if (sod.getIsFreeItem().intValue() == 1) {
									tmpSOD.getLstKhuyenMai().add(sod);
								} else {
									tmpSOD.getLstSPBan().add(sod);
								}
							}
						}
						int idx = 1;
						if (!lstCoCK.isEmpty()) { //xu ly cho nhung dong hang co chiet khau tien ==> tao thanh 1 don vat
							if (lstCoCK.size() <= vatMax) {
								SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
								saleOrderVOEx.setSaleOrder(donChinh);
								saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
								saleOrderVOEx.setAmount(BigDecimal.ZERO);
								saleOrderVOEx.setDiscount(BigDecimal.ZERO);
								saleOrderVOEx.setConLai(vatMax - lstCoCK.size()); //so luong dong hang con co the chua trong 1 don vat
								for (SaleOrderDetail d : lstCoCK) {
									saleOrderVOEx.getLstSaleOrderDetail().add(d);
									saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
									saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
								}
								lstSaleOrderVOEx.add(saleOrderVOEx);
							} else {
								SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
								saleOrderVOEx.setSaleOrder(donChinh);
								saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
								saleOrderVOEx.setAmount(BigDecimal.ZERO);
								saleOrderVOEx.setDiscount(BigDecimal.ZERO);
								saleOrderVOEx.setConLai(vatMax);
								lstSaleOrderVOEx.add(saleOrderVOEx);
								for (SaleOrderDetail d : lstCoCK) {
									if ((idx == lstCoCK.size()) || (idx % vatMax == 0)) {
										if (idx != lstCoCK.size()) {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
											saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
											saleOrderVOEx = new SaleOrderVOEx();
											saleOrderVOEx.setSaleOrder(donChinh);
											saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
											saleOrderVOEx.setAmount(BigDecimal.ZERO);
											saleOrderVOEx.setDiscount(BigDecimal.ZERO);
											saleOrderVOEx.setConLai(vatMax);
											lstSaleOrderVOEx.add(saleOrderVOEx);
										} else {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
											saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										}
									} else {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
									}
									idx++;
								}
							}
						}
						//						for(int t=lstSPBanVaKhuyenMai.size()-1;t>=0;t--){
						//							if(!lstSPBanVaKhuyenMai.get(t).getLstKhuyenMai().isEmpty() && lstSPBanVaKhuyenMai.get(t).getLstSPBan().isEmpty()){
						//								lstConLai.addAll(lstSPBanVaKhuyenMai.get(t).getLstKhuyenMai());
						//								lstSPBanVaKhuyenMai.remove(t);
						//							}
						//						}
						//xu ly cho list SPban va SPKM
						for (SaleOrderDetailForInvoidVO ex : lstSPBanVaKhuyenMai) {
							idx = 1;
							int size = ex.getLstSPBan().size() + ex.getLstKhuyenMai().size() + 1;
							if (size > 1) {
								if (size <= vatMax) { // truong hop nhom spban + km  cung chuong trinh < vat max thi tao thanh 1 don moi
									SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
									saleOrderVOEx.setSaleOrder(donChinh);
									saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
									saleOrderVOEx.setAmount(BigDecimal.ZERO);
									saleOrderVOEx.setDiscount(BigDecimal.ZERO);
									saleOrderVOEx.setConLai(vatMax - size); //so luong dong hang con co the chua trong 1 don vat
									saleOrderVOEx.setHasFreeItem(true);
									for (SaleOrderDetail d : ex.getLstSPBan()) {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
//										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
									}
									for (SaleOrderDetail d : ex.getLstKhuyenMai()) {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
//										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
									}
									lstSaleOrderVOEx.add(saleOrderVOEx);
								} else {
									SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
									saleOrderVOEx.setSaleOrder(donChinh);
									saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
									saleOrderVOEx.setAmount(BigDecimal.ZERO);
									saleOrderVOEx.setDiscount(BigDecimal.ZERO);
									saleOrderVOEx.setConLai(vatMax - 1);
									saleOrderVOEx.setHasFreeItem(true);
									lstSaleOrderVOEx.add(saleOrderVOEx);
									List<SaleOrderDetail> lstSPCoKM = new ArrayList<SaleOrderDetail>();
									if (!ex.getLstSPBan().isEmpty()) {
										lstSPCoKM.addAll(ex.getLstSPBan());
										lstSPCoKM.addAll(ex.getLstKhuyenMai());
									}
									for (SaleOrderDetail d : lstSPCoKM) {
										if ((idx == lstSPCoKM.size()) || (idx % (vatMax - 1) == 0)) {
											if (idx != lstSPCoKM.size()) {
												saleOrderVOEx.getLstSaleOrderDetail().add(d);
												saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
												saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
												saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
												saleOrderVOEx = new SaleOrderVOEx();
												saleOrderVOEx.setSaleOrder(donChinh);
												saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
												saleOrderVOEx.setAmount(BigDecimal.ZERO);
												saleOrderVOEx.setDiscount(BigDecimal.ZERO);
												saleOrderVOEx.setConLai(vatMax - 1);
												saleOrderVOEx.setHasFreeItem(true);
												lstSaleOrderVOEx.add(saleOrderVOEx);
											} else {
												saleOrderVOEx.getLstSaleOrderDetail().add(d);
												saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
												saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
												saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
											}
										} else {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
											saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										}
										idx++;
									}
								}
							}
						}
						//xu ly cac don con trong dong
						int maxVAT = vatMax;
						for (int j = sizeLstSaleOrder1; j < lstSaleOrderVOEx.size(); j++) {
							for (int k = sizeLstSaleOrder1; k < lstSaleOrderVOEx.size(); k++) {
								if (j != k  && j < lstSaleOrderVOEx.size()) {
									if (lstSaleOrderVOEx.get(j).isHasFreeItem() && lstSaleOrderVOEx.get(k).isHasFreeItem()) {
										maxVAT = vatMax - 1;
									} else {
										maxVAT = vatMax;
									}
									if ((lstSaleOrderVOEx.get(j).getConLai() + lstSaleOrderVOEx.get(k).getConLai()) >= maxVAT) { // co the gop lai thanh 1 don vat
										lstSaleOrderVOEx.get(j).setAmount(lstSaleOrderVOEx.get(j).getAmount().add(lstSaleOrderVOEx.get(k).getAmount()));
										lstSaleOrderVOEx.get(j).setDiscount(lstSaleOrderVOEx.get(j).getDiscount().add(lstSaleOrderVOEx.get(k).getDiscount()));
										List<SaleOrderDetail> lstgop = new ArrayList<SaleOrderDetail>();
										lstgop.addAll(lstSaleOrderVOEx.get(j).getLstSaleOrderDetail());
										lstgop.addAll(lstSaleOrderVOEx.get(k).getLstSaleOrderDetail());
										lstSaleOrderVOEx.get(j).setLstSaleOrderDetail(lstgop);
										lstSaleOrderVOEx.get(j).setConLai(vatMax - 1 - lstSaleOrderVOEx.get(j).getLstSaleOrderDetail().size());
										lstSaleOrderVOEx.remove(k);
										k--;
									}
								}
							}
						}
						//xu ly cho listConLai 
						if (!lstConLai.isEmpty()) {
							for (int n = sizeLstSaleOrder1; n < lstSaleOrderVOEx.size(); n++) {
								int conLai = lstSaleOrderVOEx.get(n).getConLai();
								if (conLai > 0) {
									for (int m = 0; m < conLai; m++) {
										if (lstConLai.size() > 0) {
											lstSaleOrderVOEx.get(n).setAmount(lstSaleOrderVOEx.get(n).getAmount().add(lstConLai.get(m).getAmount()));
											lstSaleOrderVOEx.get(n).setDiscount(lstSaleOrderVOEx.get(n).getDiscount().add(lstConLai.get(m).getDiscountAmount()));
											lstSaleOrderVOEx.get(n).getLstSaleOrderDetail().add(lstConLai.get(m));
											lstConLai.remove(m);
											m--;
											conLai--;
										}
									}
								}
							}
						}
						idx = 1;
						if (!lstConLai.isEmpty()) {
							if (lstConLai.size() <= vatMax) {
								SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
								saleOrderVOEx.setSaleOrder(donChinh);
								saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
								saleOrderVOEx.setAmount(BigDecimal.ZERO);
								saleOrderVOEx.setDiscount(BigDecimal.ZERO);
								saleOrderVOEx.setConLai(vatMax - lstConLai.size()); //so luong dong hang con co the chua trong 1 don vat
								for (SaleOrderDetail d : lstConLai) {
									saleOrderVOEx.getLstSaleOrderDetail().add(d);
									saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
									saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
								}
								lstSaleOrderVOEx.add(saleOrderVOEx);
							} else {
								SaleOrderVOEx saleOrderVOEx = new SaleOrderVOEx();
								saleOrderVOEx.setSaleOrder(donChinh);
								saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
								saleOrderVOEx.setAmount(BigDecimal.ZERO);
								saleOrderVOEx.setDiscount(BigDecimal.ZERO);
								saleOrderVOEx.setConLai(vatMax);
								lstSaleOrderVOEx.add(saleOrderVOEx);
								for (SaleOrderDetail d : lstConLai) {
									if ((idx == lstConLai.size()) || (idx % vatMax == 0)) {
										if (idx != lstConLai.size()) {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
											saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
											saleOrderVOEx = new SaleOrderVOEx();
											saleOrderVOEx.setSaleOrder(donChinh);
											saleOrderVOEx.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
											saleOrderVOEx.setAmount(BigDecimal.ZERO);
											saleOrderVOEx.setDiscount(BigDecimal.ZERO);
											saleOrderVOEx.setConLai(vatMax);
											lstSaleOrderVOEx.add(saleOrderVOEx);
										} else {
											saleOrderVOEx.getLstSaleOrderDetail().add(d);
											saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
											saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
											saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
										}
									} else {
										saleOrderVOEx.getLstSaleOrderDetail().add(d);
										saleOrderVOEx.setAmount(saleOrderVOEx.getAmount().add(d.getAmount()));
										saleOrderVOEx.setDiscount(saleOrderVOEx.getDiscount().add(d.getDiscountAmount() != null ? d.getDiscountAmount() : BigDecimal.ZERO));
										saleOrderVOEx.setConLai(saleOrderVOEx.getConLai() - 1);
									}
									idx++;
								}
							}
						}
					}
				}
				//da tach hoa don xong

				if (lstInvoiceNumber.size() != lstSaleOrderVOEx.size()) {
					throw new IllegalArgumentException("number of lstInvoiceNumber is not equals with list seperated sale orders");
				}

				Date lockDate = shopLockDAO.getNextLockedDay(shopId);
				if (lockDate == null) {
					lockDate = commonDAO.getSysDate();
				}

				for (int i = 0; i < lstInvoiceNumber.size(); i++) {
					String invoiceNumber = lstInvoiceNumber.get(i).toUpperCase();
					SaleOrder saleOrder = lstSaleOrderVOEx.get(i).getSaleOrder();
					List<SaleOrderDetail> lstSOD = lstSaleOrderVOEx.get(i).getLstSaleOrderDetail();

					Invoice existInvoice = invoiceDAO.getInvoiceByInvoiceNumber(invoiceNumber, shopId);
					if (existInvoice == null) {
						// chua co invoice nay trong db -> them moi
						Invoice __newInvoice = new Invoice();
						__newInvoice.setInvoiceNumber(invoiceNumber);
						__newInvoice.setStatus(InvoiceStatus.USING);
						__newInvoice.setCreateUser(username);
						__newInvoice.setCreateDate(date);
						__newInvoice.setOrderDate(saleOrder.getOrderDate());
						if (lstSOD.get(0).getVat() == 0 || lstSOD.get(0).getVat() == null) {
							__newInvoice.setVat(this.getVATListSaleOrderDetail(lstSaleOrderVOEx.get(i).getLstSaleOrderDetail()));
						} else {
							__newInvoice.setVat(lstSOD.get(0).getVat());
						}
						__newInvoice.setAmount(lstSaleOrderVOEx.get(i).getAmount());

						//TUNGTT
						__newInvoice.setDiscount(lstSaleOrderVOEx.get(i).getDiscount());

						__newInvoice.setStaff(saleOrder.getStaff());
						Float __newInvoiceVat = __newInvoice.getVat();
						if (__newInvoiceVat != null) {
							BigDecimal taxAmount = null;
							if (__newInvoice.getAmount() != null) {
								taxAmount = (__newInvoice.getAmount().subtract(__newInvoice.getDiscount())).multiply(new BigDecimal(__newInvoiceVat));
								taxAmount = taxAmount.divide(new BigDecimal(100 + __newInvoiceVat), 0, RoundingMode.HALF_UP);
								__newInvoice.setTaxAmount(taxAmount);
							} else {
								__newInvoice.setTaxAmount(BigDecimal.ZERO);
							}
						}
						__newInvoice.setSelName(saleOrder.getStaff().getStaffName());
						if (saleOrder.getDelivery() != null) {
							__newInvoice.setDeliveryStaff(saleOrder.getDelivery());
							__newInvoice.setDeliveryName(saleOrder.getDelivery().getStaffName());
						} else {
							throw new BusinessException("the sale order need to update dilivery:" + saleOrder.getOrderNumber());
						}
						__newInvoice.setOrderNumber(saleOrder.getOrderNumber());
						__newInvoice.setSku(lstSaleOrderVOEx.get(i).getSku());
						__newInvoice.setShop(saleOrder.getShop());

						if (saleOrder.getCustomer() != null) {
							__newInvoice.setCustomerCode(saleOrder.getCustomer().getShortCode());
							__newInvoice.setCustName(saleOrder.getCustomer().getCustomerName());
							__newInvoice.setCustomerAddr(saleOrder.getCustomer().getAddress());
							__newInvoice.setCustTaxNumber(saleOrder.getCustomer().getInvoiceTax());
							__newInvoice.setCustDelAddr(saleOrder.getCustomer().getDeliveryAddress());
							if (InvoiceCustPayment.MONEY.equals(lstCustPayment.get(i))) {
								__newInvoice.setCustBankName(null);
								__newInvoice.setCustBankAccount(null);
							} else {
								__newInvoice.setCustBankName(saleOrder.getCustomer().getInvoiceNameBank());
								__newInvoice.setCustBankAccount(saleOrder.getCustomer().getInvoiceNumberAccount());
							}
						}

						if (saleOrder.getShop() != null) {
							__newInvoice.setCpnyName(saleOrder.getShop().getShopName());
							__newInvoice.setCpnyAddr(saleOrder.getShop().getAddress());
							__newInvoice.setCpnyBankName(saleOrder.getShop().getInvoiceBankName());
							__newInvoice.setCpnyBankAccount(saleOrder.getShop().getInvoiceNumberAccount());
							__newInvoice.setCpnyTaxNumber(saleOrder.getShop().getTaxNum());
							__newInvoice.setCpnyPhone(saleOrder.getShop().getPhone());
						}

						//TUNGTT: them sale_order
						__newInvoice.setSaleOrder(saleOrder);

						//TUNGTT set theo ngay chot
						//						__newInvoice.setInvoiceDate(lockDate);
						__newInvoice.setCustPayment(lstCustPayment.get(i));
						__newInvoice.setSaleOrderType(1); // don gop

						Invoice newInvoice = invoiceDAO.createInvoice(__newInvoice);

						for (int k = 0; k < lstSOD.size(); k++) {
							if (lstSOD.get(k).getId() == null) {
								continue;
							}
//							if (lstSOD.get(k).getIsFreeItem().intValue() == 0) {
								SaleOrderDetailVATFilter filter = new SaleOrderDetailVATFilter();
								filter.setProductId(lstSOD.get(k).getProduct().getId());
								filter.setVat(lstSOD.get(k).getVat());
								List<Long> lstSOId = new ArrayList<Long>();
								for (SaleOrder so : lstSO1) {
									lstSOId.add(so.getId());
								}
								filter.setLstSOId(lstSOId);
								List<SaleOrderDetail> lstDetail = saleOrderDAO.getListSaleOrderDetailByFilter(filter);
								if (lstDetail != null && !lstDetail.isEmpty()) {
									for (SaleOrderDetail sod : lstDetail) {
										sod.setInvoice(newInvoice);
										sod.setUpdateDate(date);
										sod.setUpdateUser(username);
										saleOrderDetailDAO.updateSaleOrderDetail(sod);
									}
								}
//							} else {
//								lstSOD.get(k).setInvoice(newInvoice);
//								lstSOD.get(k).setUpdateDate(date);
//								lstSOD.get(k).setUpdateUser(username);
//								saleOrderDetailDAO.updateSaleOrderDetail(lstSOD.get(k));
//							}
							//TUNGTT insert sale_order_detail vao invoice_detail
							InvoiceDetail invoiceDetail = new InvoiceDetail();
							if (lstSOD.get(k).getProduct() != null) {
								invoiceDetail.setProduct(lstSOD.get(k).getProduct());
							}
							if (lstSOD.get(k).getQuantity() != null) {
								invoiceDetail.setQuantity(lstSOD.get(k).getQuantity());
							}
							if (lstSOD.get(k).getPrice() != null) {
								invoiceDetail.setPrice(lstSOD.get(k).getPrice());
							}
							if (lstSOD.get(k).getDiscountAmount() != null) {
								invoiceDetail.setDiscountAmount(lstSOD.get(k).getDiscountAmount());
							}
							if (lstSOD.get(k).getDiscountPercent() != null) {
								invoiceDetail.setDiscountPercent(lstSOD.get(k).getDiscountPercent());
							}
							if (lstSOD.get(k).getAmount() != null) {
								invoiceDetail.setAmount(lstSOD.get(k).getAmount());
							}
							if (lstSOD.get(k).getVat() != null) {
								invoiceDetail.setVat(lstSOD.get(k).getVat());
							}
							if (lstSOD.get(k).getIsFreeItem() != null) {
								invoiceDetail.setIsFreeItem(lstSOD.get(k).getIsFreeItem());
							}
							if (lstSOD.get(k).getPriceValue() != null) {
								invoiceDetail.setPriceValue(lstSOD.get(k).getPriceValue());
							}
							if (lstSOD.get(k).getPriceNotVat() != null) {
								invoiceDetail.setPriceNotVat(lstSOD.get(k).getPriceNotVat());
							}
							//							invoiceDetail.setSaleOrderDetail(lstSOD.get(k));
							invoiceDetail.setInvoice(newInvoice);
							invoiceDetail.setCreateUser(username);
							invoiceDetail.setCreateDate(date);
							invoiceDAO.createInvoiceDetail(invoiceDetail);

						}
					} else if (InvoiceStatus.USING.getValue().equals(existInvoice.getStatus().getValue()) || InvoiceStatus.CANCELED.getValue().equals(existInvoice.getStatus().getValue())) {
						throw new IllegalArgumentException("number of lstInvoiceNumber is not equals with list seperated sale orders");
					} else if (InvoiceStatus.MODIFIED.getValue().equals(existInvoice.getStatus().getValue())) {
						// da co invoice trung db->cap nhat
						existInvoice.setStatus(InvoiceStatus.USING);
						existInvoice.setUpdateDate(commonDAO.getSysDate());
						existInvoice.setUpdateUser(username);

						existInvoice.setOrderDate(saleOrder.getOrderDate());
						existInvoice.setStaff(saleOrder.getStaff());

						if (lstSOD.get(0).getVat() == 0 || lstSOD.get(0).getVat() == null) {
							existInvoice.setVat(this.getVATListSaleOrderDetail(lstSaleOrderVOEx.get(i).getLstSaleOrderDetail()));
						} else {
							existInvoice.setVat(lstSOD.get(0).getVat());
						}

						existInvoice.setAmount(lstSaleOrderVOEx.get(i).getAmount());

						//TUNGTT
						existInvoice.setDiscount(lstSaleOrderVOEx.get(i).getDiscount());

						Float __existInvoiceVat = existInvoice.getVat();

						BigDecimal taxAmount = null;
						if (existInvoice.getAmount() != null) {
							taxAmount = (existInvoice.getAmount().subtract(existInvoice.getDiscount())).multiply(new BigDecimal(__existInvoiceVat));
							taxAmount = taxAmount.divide(new BigDecimal(100 + __existInvoiceVat), 0, RoundingMode.HALF_UP);
							existInvoice.setTaxAmount(taxAmount);

						} else {
							existInvoice.setTaxAmount(BigDecimal.ZERO);
						}

						existInvoice.setSelName(saleOrder.getStaff().getStaffName());
						if (saleOrder.getDelivery() != null) {
							saleOrder.setDelivery(saleOrder.getDelivery());
							existInvoice.setDeliveryName(saleOrder.getDelivery().getStaffName());
						} else {
							throw new BusinessException("the sale order need to update dilivery:" + saleOrder.getOrderNumber());
						}
						existInvoice.setOrderNumber(saleOrder.getOrderNumber());
						existInvoice.setSku(lstSaleOrderVOEx.get(i).getSku());
						existInvoice.setShop(saleOrder.getShop());

						if (saleOrder.getCustomer() != null) {
							existInvoice.setCustomerCode(saleOrder.getCustomer().getShortCode());
							existInvoice.setCustName(saleOrder.getCustomer().getCustomerName());
							existInvoice.setCustomerAddr(saleOrder.getCustomer().getAddress());
							existInvoice.setCustTaxNumber(saleOrder.getCustomer().getInvoiceTax());
							existInvoice.setCustDelAddr(saleOrder.getCustomer().getDeliveryAddress());
							if (InvoiceCustPayment.MONEY.equals(lstCustPayment.get(i))) {
								existInvoice.setCustBankName(null);
								existInvoice.setCustBankAccount(null);
							} else {
								existInvoice.setCustBankName(saleOrder.getCustomer().getInvoiceNameBank());
								existInvoice.setCustBankAccount(saleOrder.getCustomer().getInvoiceNumberAccount());
							}
						}

						if (saleOrder.getShop() != null) {
							existInvoice.setCpnyName(saleOrder.getShop().getShopName());
							existInvoice.setCpnyAddr(saleOrder.getShop().getAddress());
							existInvoice.setCpnyBankName(saleOrder.getShop().getInvoiceBankName());
							existInvoice.setCpnyBankAccount(saleOrder.getShop().getInvoiceNumberAccount());
							existInvoice.setCpnyTaxNumber(saleOrder.getShop().getTaxNum());
							existInvoice.setCpnyPhone(saleOrder.getShop().getPhone());
						}

						//TUNGTT set theo ngay chot
						existInvoice.setInvoiceDate(null);
						existInvoice.setCustPayment(lstCustPayment.get(i));
						invoiceDAO.updateInvoice(existInvoice);

						for (int k = 0; k < lstSOD.size(); k++) {
							lstSOD.get(k).setInvoice(existInvoice);
							saleOrderDetailDAO.updateSaleOrderDetail(lstSOD.get(k));
						}
					}
				}
			} else {
				for (int i = 0; i < lstInvoiceNumber.size(); i++) {
					Invoice oldInvoice = listRemovedInvoice.get(i);
					Date lockDate = shopLockDAO.getNextLockedDay(shopId);
					if (lockDate == null) {
						lockDate = commonDAO.getSysDate();
					}
					Invoice newInvoice = oldInvoice.clone();
					newInvoice.setInvoiceDate(null);
					newInvoice.setInvoiceNumber(lstInvoiceNumber.get(i).toUpperCase());
					newInvoice.setStatus(InvoiceStatus.USING);
					newInvoice.setCreateDate(commonDAO.getSysDate());
					newInvoice.setCreateUser(username);

					newInvoice = invoiceDAO.createInvoice(newInvoice);

					List<SaleOrderDetail> listSODRemoved = saleOrderDetailDAO.getListDetailByInvoiceAndInvoceDetail(oldInvoice.getId());

					for (int k = 0; k < listSODRemoved.size(); k++) {
						listSODRemoved.get(k).setInvoice(newInvoice);
						saleOrderDetailDAO.updateSaleOrderDetail(listSODRemoved.get(k));

						//TUNGTT insert sale_order_detail vao invoice_detail
						InvoiceDetail invoiceDetail = new InvoiceDetail();
						if (listSODRemoved.get(k).getProduct() != null) {
							invoiceDetail.setProduct(listSODRemoved.get(k).getProduct());
						}
						if (listSODRemoved.get(k).getQuantity() != null) {
							invoiceDetail.setQuantity(listSODRemoved.get(k).getQuantity());
						}
						if (listSODRemoved.get(k).getPrice() != null) {
							invoiceDetail.setPrice(listSODRemoved.get(k).getPrice());
						}
						if (listSODRemoved.get(k).getDiscountAmount() != null) {
							invoiceDetail.setDiscountAmount(listSODRemoved.get(k).getDiscountAmount());
						}
						if (listSODRemoved.get(k).getDiscountPercent() != null) {
							invoiceDetail.setDiscountPercent(listSODRemoved.get(k).getDiscountPercent());
						}
						if (listSODRemoved.get(k).getAmount() != null) {
							invoiceDetail.setAmount(listSODRemoved.get(k).getAmount());
						}
						if (listSODRemoved.get(k).getVat() != null) {
							invoiceDetail.setVat(listSODRemoved.get(k).getVat());
						}
						if (listSODRemoved.get(k).getIsFreeItem() != null) {
							invoiceDetail.setIsFreeItem(listSODRemoved.get(k).getIsFreeItem());
						}
						if (listSODRemoved.get(k).getPriceValue() != null) {
							invoiceDetail.setPriceValue(listSODRemoved.get(k).getPriceValue());
						}
						if (listSODRemoved.get(k).getPriceNotVat() != null) {
							invoiceDetail.setPriceNotVat(listSODRemoved.get(k).getPriceNotVat());
						}
						invoiceDetail.setSaleOrderDetail(listSODRemoved.get(k));
						invoiceDetail.setInvoice(newInvoice);
						invoiceDetail.setCreateUser(username);
						invoiceDetail.setCreateDate(commonDAO.getSysDate());
						invoiceDAO.createInvoiceDetail(invoiceDetail);
					}
				}
			}

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.business.SaleOrderMgr#removeInvoice(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void removeInvoice(List<Long> lstInvoiceId, Long shopId, String userName) throws BusinessException {
		if (lstInvoiceId == null) {
			throw new IllegalArgumentException("saleOrderId is null");
		}
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		try {
			List<Long> listIsUpdate = new ArrayList<Long>();
			for (Long invoiceId : lstInvoiceId) {
				List<SaleOrderDetail> listSaleOrderDetail = saleOrderDetailDAO.getListSaleOrderDetailByInvoiceId(invoiceId, null);
				Invoice existInvoice = invoiceDAO.getInvoiceById(invoiceId);
				if (!InvoiceStatus.USING.getValue().equals(existInvoice.getStatus().getValue())) {
					throw new BusinessException("Data exception : invoice is not using");
				}
				existInvoice.setDestroyUser(userName);
				Date lockDate = shopLockDAO.getNextLockedDay(shopId);
				if (lockDate == null) {
					lockDate = commonDAO.getSysDate();
				}
				existInvoice.setDestroyDate(lockDate);
				existInvoice.setStatus(InvoiceStatus.CANCELED);
				invoiceDAO.updateInvoice(existInvoice);
				for (SaleOrderDetail sod : listSaleOrderDetail) {
					if (listIsUpdate.contains(sod.getId())) {
						continue;
					}
					if (sod.getInvoice() == null) {
						throw new BusinessException("Data exception : sale order detail must be existed invoice");
					}
					// Invoice existInvoice = sod.getInvoice();
					// if(!InvoiceStatus.USING.getValue().equals(existInvoice.getStatus().getValue()))
					// {
					// throw new
					// BusinessException("Data exception : invoice is not using");
					// }
					if (!shopId.equals(sod.getShop().getId()) || !shopId.equals(existInvoice.getShop().getId())) {
						throw new BusinessException("Data exception : invoice not belong to shop");
					}
					// existInvoice.setStatus(InvoiceStatus.CANCELED);
					sod.setInvoice(null);
					saleOrderDetailDAO.updateSaleOrderDetail(sod);
					listIsUpdate.add(sod.getId());
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<SaleOrder> getListSaleOrderForDelivery(KPaging<SaleOrder> paging, SoFilter filter) throws BusinessException {
		try {
			ObjectVO<SaleOrder> vo = new ObjectVO<SaleOrder>();
			List<SaleOrder> list = saleOrderDAO.getListSaleOrderForDelivery(paging, filter);
			vo.setLstObject(list);
			vo.setkPaging(paging);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<SaleOrderDetailVOEx> getListSaleOrderDetailVOEx(KPaging<SaleOrderDetailVOEx> kPaging, Long saleOrderId, Integer isFreeItem, ProgramType programType, Boolean isAutoPromotion, Long customerTypeId, Long shopId, Date lockDay)
			throws BusinessException {
		try {
			ObjectVO<SaleOrderDetailVOEx> vo = new ObjectVO<SaleOrderDetailVOEx>();
			vo.setkPaging(kPaging);
			List<SaleOrderDetailVOEx> lst = saleOrderDetailDAO.getListSaleOrderDetailVOEx(kPaging, saleOrderId, isFreeItem, programType, isAutoPromotion, customerTypeId, shopId, lockDay);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<SaleOrderDetailVOEx> getListSaleOrderDetailVOExForTablet(SaleOrder saleOrder, Integer isFreeItem, ProgramType programType, Boolean isAutoPromotion, Date lockDay, LogInfoVO logInfo) throws BusinessException {
		try {
			final Map<Integer, String> PROMOTION_TYPE_MAP = new HashMap<Integer, String>() {
				{
					put(1, "ZM");
					put(2, "DISPLAY_SCORE");
					put(3, "ZD");
					put(4, "ZH");
					put(5, "ZT");
				};
			};

			//Kiem tra da tach kho chua
			Boolean isSaleOrderHadSaleOrderLot = false;
			List<SaleOrderLot> saleOrderLots = saleOrderDAO.getListSaleOrderLotBySaleOrderId(saleOrder.getId());
			if (saleOrderLots != null && saleOrderLots.size() > 0) {
				isSaleOrderHadSaleOrderLot = true;//da tach roi thi khong tach nua
			} else {
				isSaleOrderHadSaleOrderLot = false;
			}

			ObjectVO<SaleOrderDetailVOEx> vo = new ObjectVO<SaleOrderDetailVOEx>();
			//			vo.setkPaging(kPaging);
			//			List<SaleOrderDetailVOEx> lst = saleOrderDetailDAO.getListSaleOrderDetailVOExForTablet(kPaging, saleOrderId, isFreeItem, programType,isAutoPromotion,customerTypeId,shopId,lockDay);
			//			vo.setLstObject(lst);
			List<SaleOrderDetailVOEx> lst = new ArrayList<SaleOrderDetailVOEx>();
			SaleOrderDetailVOEx sodVO;
			
			// lay cau hinh phan biet kho ban - kho KM
			boolean isDiVidualWarehouse = false;
			List<ApParam> aparams = apParamMgr.getListApParam(ApParamType.SYS_WAREHOUSE_CONFIG, ActiveType.RUNNING);
			if (aparams == null || aparams.size() == 0 || Constant.SYS_WAREHOUSE_CONFIG_DIVIDUAL.equals(aparams.get(0).getValue())) {
				isDiVidualWarehouse = true;
			}
			List<SaleOrderLot> lstLot = this.autoSplitWarehouse(saleOrder, false, isDiVidualWarehouse, logInfo);//false: ko tao saleOrderLot
			/*
			 * map(productCode_programCode, REVERTED-accumulative quantity)
			 * purpose: used to distribute when split warehouse
			 */
			Map<String, Integer> _productQuantityMap = new HashMap<String, Integer>();
			/*
			 * map(productCode_programCode, REVERTED-accumulative discount
			 * amount) purpose: used to distribute discount amount when split
			 * warehouse
			 */
			Map<String, BigDecimal> _productDiscountAmountMap = new HashMap<String, BigDecimal>();
			/*
			 * map(productCode_programCode, REVERTED-accumulative discount
			 * percent) purpose: used to distribute discount percent when split
			 * warehouse
			 */
			Map<String, Float> _productDiscountPercentMap = new HashMap<String, Float>();
			final Float ONE_HUNDRED_PERCENT = 100f;
			//int PRECISION = 2;
			//MathContext mc = new MathContext(PRECISION, RoundingMode.HALF_UP);
			
			Map<String, Integer> mapWh = new HashMap<String, Integer>();
			Integer qtt = null;
			StockTotal stk = null;
			List<StockTotal> lstStk = null;
			if (SaleOrderSource.WEB.equals(saleOrder.getOrderSource())
					|| (SaleOrderSource.TABLET.equals(saleOrder.getOrderSource()) && isSaleOrderHadSaleOrderLot)) {
				for (SaleOrderLot lot : lstLot) {
					if (lot.getWarehouse() != null) {
						qtt = mapWh.get(lot.getProduct().getId() + "_" + lot.getWarehouse().getId());
						if (qtt == null) {
							stk = stockTotalDAO.getStockTotalByProductAndOwner(lot.getSaleOrderDetail().getProduct().getId(), StockObjectType.SHOP, saleOrder.getShop().getId(), lot.getWarehouse().getId());
							if (stk == null) {
								qtt = 0;
							} else {
								qtt = stk.getAvailableQuantity();
							}
						}
						qtt = qtt + lot.getQuantity();
						mapWh.put(lot.getProduct().getId() + "_" + lot.getWarehouse().getId(), qtt);
					}
				}
			}
			
			for (SaleOrderLot lot : lstLot) {
				String _key = (lot.getProduct() != null ? lot.getProduct().getProductCode() : "") + "_" + (lot.getSaleOrderDetail() != null ? lot.getSaleOrderDetail().getProgramCode() : "");
				if (!_productQuantityMap.containsKey(_key)) {
					_productQuantityMap.put(_key, lot.getSaleOrderDetail() != null ? lot.getSaleOrderDetail().getQuantity() : 0);
					_productDiscountAmountMap.put(_key, lot.getSaleOrderDetail() != null ? lot.getSaleOrderDetail().getDiscountAmount() : BigDecimal.ZERO);
					_productDiscountPercentMap.put(_key, ONE_HUNDRED_PERCENT);
				}
				sodVO = new SaleOrderDetailVOEx();
				if (lot.getStockTotal() == null) {
					if (lot.getWarehouse() != null) {
						stk = stockTotalDAO.getStockTotalByProductAndOwner(lot.getSaleOrderDetail().getProduct().getId(), StockObjectType.SHOP, saleOrder.getShop().getId(), lot.getWarehouse().getId());
						lot.setStockTotal(stk);
					}
				}
				if (lot.getStockTotal() != null) {
					if (SaleOrderSource.WEB.equals(saleOrder.getOrderSource())
						|| (SaleOrderSource.TABLET.equals(saleOrder.getOrderSource()) && isSaleOrderHadSaleOrderLot)) {
						if (lot.getWarehouse() == null) {
							qtt = lot.getStockTotal().getAvailableQuantity() + (lot.getQuantity() != null ? lot.getQuantity() : 0);
						} else {
							qtt = mapWh.get(lot.getProduct().getId() + "_" + lot.getWarehouse().getId());
						}
						sodVO.setAvailableQuantity(qtt);
					} else {
						sodVO.setAvailableQuantity(lot.getStockTotal().getAvailableQuantity());
					}

					sodVO.setStockQuantity(lot.getStockTotal().getQuantity());
				} else if (OrderType.SO.equals(saleOrder.getOrderType()) || OrderType.CO.equals(saleOrder.getOrderType())) {
					StockTotalFilter stockTotalFilter = new StockTotalFilter();
					stockTotalFilter.setProductId(lot.getSaleOrderDetail().getProduct().getId());
					stockTotalFilter.setOwnerType(StockObjectType.STAFF);
					stockTotalFilter.setOwnerId(saleOrder.getStaff().getId());
					lstStk = stockTotalDAO.getListStockTotalByProductAndOwner(stockTotalFilter);
					if (lstStk == null || lstStk.size() == 0) {
						sodVO.setAvailableQuantity(0);
						sodVO.setStockQuantity(0);
					} else {
						sodVO.setAvailableQuantity(lstStk.get(0).getApprovedQuantity());
						sodVO.setStockQuantity(lstStk.get(0).getApprovedQuantity());
					}
				}
				if (lot.getSaleOrderDetail().getIsFreeItem() == 1 && PromotionType.ZV23.getValue().equalsIgnoreCase(lot.getSaleOrderDetail().getProgrameTypeCode())) {
					sodVO.setAccumulation(1);
				}
				sodVO.setSaleOrderLotId(lot.getId());
				sodVO.setId(lot.getSaleOrderDetail().getId());
				sodVO.setProductId(lot.getProduct().getId());
				sodVO.setPrice(lot.getPriceValue());
				sodVO.setPkPrice(lot.getPackagePrice());
				sodVO.setProductCode(lot.getProduct().getProductCode());
				sodVO.setProductName(lot.getProduct().getProductName());
				if (lot.getWarehouse() != null) {
					sodVO.setWarehouseId(lot.getWarehouse().getId());
					sodVO.setWarehouseName(lot.getWarehouse().getWarehouseName());
				} else {
					sodVO.setWarehouseId(null);
					sodVO.setWarehouseName(null);
				}
				sodVO.setConvfact(lot.getProduct().getConvfact());
				sodVO.setQuantity(lot.getQuantity());
				sodVO.setQuantityPackage(lot.getQuantityPackage());
				sodVO.setQuantityRetail(lot.getQuantityRetail());
				sodVO.setAmount(lot.getPriceValue().multiply(new BigDecimal(lot.getQuantity())));
				/*
				 * calculate quantity ratio between warehouses and distribute discount amount/percent
				 */
				//sodVO.setDiscountAmount(lot.getSaleOrderDetail().getDiscountAmount());
				//sodVO.setDiscountPercent(lot.getSaleOrderDetail().getDiscountPercent());
				Integer totalQuantity = lot.getSaleOrderDetail() != null ? lot.getSaleOrderDetail().getQuantity() : 0;
				BigDecimal totalDiscountAmount = lot.getSaleOrderDetail() != null ? lot.getSaleOrderDetail().getDiscountAmount() : BigDecimal.ZERO;
				BigDecimal discountAmount = BigDecimal.ZERO;
				Float discountPercent = 0f;
				if (totalQuantity != 0) {
					if (lot.getQuantity() == _productQuantityMap.get(_key)) {
						discountAmount = _productDiscountAmountMap.get(_key);
						discountPercent = _productDiscountPercentMap.get(_key);
					} else {
						double ratio = lot.getQuantity() * 1.0 / totalQuantity;
						discountAmount = totalDiscountAmount.multiply(new BigDecimal(ratio));//.round(mc);
						discountAmount = discountAmount.setScale(2, RoundingMode.HALF_UP);
						discountPercent = (float) ratio;
						// subtract quantity, discount_amount, discount_percent
						Integer remainQuantity = _productQuantityMap.get(_key) - lot.getQuantity();
						_productQuantityMap.put(_key, remainQuantity);
						BigDecimal remainDiscountAmount = _productDiscountAmountMap.get(_key).subtract(discountAmount);
						_productDiscountAmountMap.put(_key, remainDiscountAmount);
						float remainPercent = _productDiscountPercentMap.get(_key) - discountPercent;
						_productDiscountPercentMap.put(_key, remainPercent);
					}
				}
				sodVO.setDiscountAmount(discountAmount);
				sodVO.setDiscountPercent(discountPercent);

				sodVO.setUom1(lot.getProduct().getUom1());
				if (lot.getProduct().getGrossWeight() != null) {
					sodVO.setGrossWeight(lot.getProduct().getGrossWeight().floatValue());
				} else {
					sodVO.setGrossWeight(0f);
				}
				sodVO.setProgramCode(lot.getSaleOrderDetail().getProgramCode());
//				sodVO.setJoinProgramCode(lot.getSaleOrderDetail().getJoinProgramCode());
				if (lot.getSaleOrderDetail().getProgramType() != null) {
					sodVO.setProgramType(lot.getSaleOrderDetail().getProgramType().getValue());
					/*
					 * set promotion type
					 */
					sodVO.setPromotionType(PROMOTION_TYPE_MAP.get(sodVO.getProgramType().getValue()));
				}
				sodVO.setMaxQuantityFree(lot.getSaleOrderDetail().getMaxQuantityFree());
				sodVO.setIsFreeItem(lot.getSaleOrderDetail().getIsFreeItem());
				if (lot.getSaleOrderDetail() != null) {
					sodVO.setPriceView(lot.getSaleOrderDetail().getPriceValue());
					sodVO.setPkPriceView(lot.getSaleOrderDetail().getPackagePrice());
				}
				//BO sung them mot so truong
				lst.add(sodVO);
			}
			vo.setLstObject(lst);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<SaleOrderDetailGroupVO> getListSaleOrderDetailGroupVO(KPaging<SaleOrderDetailGroupVO> kPaging, Long saleOrderId, Integer isFreeItem) throws BusinessException {
		try {
			ObjectVO<SaleOrderDetailGroupVO> vo = new ObjectVO<SaleOrderDetailGroupVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(saleOrderDetailDAO.getListSaleOrderDetailGroupVO(kPaging, saleOrderId, isFreeItem));
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateSaleOrderForDelivery(List<Long> lstSaleOrderId, Long deliveryId, Long cashierId, Long carId) throws BusinessException {
		try {
			/*
			 * if (carId == null && deliveryId == null) { throw new
			 * IllegalArgumentException( "deliveryId && carId is null"); }
			 */
			if (lstSaleOrderId == null || lstSaleOrderId.size() == 0) {
				return;
			}
			Staff delivery = null;
			if (deliveryId != null) {
				delivery = staffDAO.getStaffById(deliveryId);
				if (delivery == null) {
					throw new IllegalArgumentException("delivery is null");
				}
			}

			Staff cashier = null;
			if (cashierId != null) {
				cashier = staffDAO.getStaffById(cashierId);
				if (cashier == null) {
					throw new IllegalArgumentException("cashier is null");
				}
			}

			Car car = null;
			if (carId != null) {
				car = carDAO.getCarById(carId);
				if (car == null) {
					throw new IllegalArgumentException("car is null");
				}
			}
			for (Long saleOrderId : lstSaleOrderId) {
				if (saleOrderId == null) {
					throw new IllegalArgumentException("saleOrderId is null");
				}
				SaleOrder saleOrder = saleOrderDAO.getSaleOrderByIdForUpdate(saleOrderId);
				if (saleOrder == null) {
					throw new IllegalArgumentException("saleOrder is null");
				}
				if (delivery != null) {
					saleOrder.setDelivery(delivery);
				}
				if (cashier != null) {
					saleOrder.setCashier(cashier);
				}
				if (car != null) {
					saleOrder.setCar(car);
				}
				saleOrderDAO.updateSaleOrder(saleOrder);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<SaleOrder> getListSaleOrderFromTablet() throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			return getListSaleOrderFromTabletByCondition(null, SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM, null, null, null, null, currentDate, currentDate);
		} catch (BusinessException e) {
			throw e;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<SaleOrder> getListSaleOrderFromTabletByCondition(String orderNumber, SaleOrderStatus approved, String customerCode, String customerName, Long staffId, Integer priority, Date fromDate, Date toDate) throws BusinessException {
		if (null != approved && SaleOrderStatus.TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM.getValue().compareTo(approved.getValue()) != 0 && SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM.getValue().compareTo(approved.getValue()) != 0) {
			throw new BusinessException("APPROVED is invalidate.");
		}

		try {
			return saleOrderDAO.getListSaleOrderFormTableByCondition(orderNumber, approved, customerCode, customerName, staffId, priority, fromDate, toDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<SaleOrder> acceptSaleOrderFromTable(List<Long> listSaleOrderId, LogInfoVO logInfoVo) throws BusinessException {
		// TODO: sua db
		List<SaleOrder> listUnacceptId = new ArrayList<SaleOrder>();
		if (null == listSaleOrderId || listSaleOrderId.size() <= 0) {
			throw new BusinessException("List sale order id is null or empty.");
		}

		try {
			for (Long orderId : listSaleOrderId) {
				SaleOrder saleOrder = saleOrderDAO.getSaleOrderById(orderId);
				if (saleOrder == null) {
					throw new BusinessException("Order has id '" + orderId + "' is invalidate.");
				}

				List<SaleOrderDetailVOEx> listOrderDetail = saleOrderDetailDAO.getListSaleOrderDetailVOEx(null, orderId, null, null, null, null, null, null);
				if (null == listOrderDetail || listOrderDetail.size() <= 0) {
					throw new BusinessException("Order has id '" + orderId + "' no row in detail.");
				}

				String listPromotionCode = checkQuantityCheckAndMax(saleOrder);
				if (StringUtility.isNullOrEmpty(listPromotionCode)) {
					String listInvalidatePro = this.checkNotEnoughQuantityProduct(saleOrder);

					// du dieu kien
					if (StringUtility.isNullOrEmpty(listInvalidatePro)) {
						saleOrder.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
						// saleOrder.setImportCode("Ä�Ã£ xÃ¡c nháº­n");

						for (SaleOrderDetailVOEx item : listOrderDetail) {
							if (ProgramType.AUTO_PROM.equals(item.getProgramType())) {
								PromotionProgram myOwnPromotion = promotionProgramDAO.getPromotionProgramByCode(item.getProgramCode());
								PromotionShopMap myPromotionShopMap = promotionShopMapDAO.getPromotionShopMap(saleOrder.getShop().getId(), myOwnPromotion.getId());
								// if(PromotionObjectApply.SHOP.equals(myPromotionShopMap.getObjectApply()))
								// {
								// //ap dung cho shop
								myPromotionShopMap.setQuantityReceived(myPromotionShopMap.getQuantityReceived() == null ? 1 : myPromotionShopMap.getQuantityReceived() + 1);
								promotionShopMapDAO.updatePromotionShopMap(myPromotionShopMap, logInfoVo);
								// } else {
								// //ap dung cho loai khach hang va khach hang
								// PromotionCustomerMap myPromotionCustMap =
								// promotionCustomerMapDAO.getPromotionCustomerMap(myPromotionShopMap.getId(),
								// saleOrder.getCustomer() == null ? null :
								// saleOrder.getCustomer().getId(), null);
								// myPromotionCustMap.setQuantityReceived(myPromotionCustMap.getQuantityReceived()
								// == null ? 1 :
								// myPromotionCustMap.getQuantityReceived() +
								// 1);
								// promotionCustomerMapDAO.updatePromotionCustomerMap(myPromotionCustMap,
								// logInfoVo);
								//
								// myPromotionShopMap.setQuantityReceived(myPromotionShopMap.getQuantityReceived()
								// == null ? 1 :
								// myPromotionShopMap.getQuantityReceived() +
								// 1);
								// promotionShopMapDAO.updatePromotionShopMap(myPromotionShopMap,
								// logInfoVo);
								// }
							}

							stockTotalDAO.updateAvailableQuatity(item.getProductId(), StockObjectType.SHOP, saleOrder.getShop().getId(), -1 * item.getQuantity());
						}

						saleOrderDAO.updateSaleOrder(saleOrder);
					}
					// khong du dieu kien
					else {
						saleOrder.setApproved(SaleOrderStatus.TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM);
						// saleOrder.setImportCode("Thiáº¿u máº·t hÃ ng: "
						// + listInvalidatePro);

						// Khong update lai availableQuantity
						// for(SaleOrderDetailVOEx item : listOrderDetail){
						// stockTotalDAO.updateAvailableQuatity(item.getProductId(),
						// StockObjectType.SHOP, shopId, item.getQuantity());
						// }

						saleOrderDAO.updateSaleOrder(saleOrder);
						listUnacceptId.add(saleOrder);
					}
				} else {
					saleOrder.setApproved(SaleOrderStatus.TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM);
					// saleOrder.setImportCode("Sá»‘ suáº¥t cá»§a CTKM "
					// + listPromotionCode);

					saleOrderDAO.updateSaleOrder(saleOrder);
					listUnacceptId.add(saleOrder);
				}
			}

			return listUnacceptId;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Kiem tra mat hang co du so luong de chuyen ko
	 * 
	 * @param listOrderDetail
	 * @return
	 * @author ThuatTQ
	 */
	private String checkNotEnoughQuantityProduct(SaleOrder saleOrder) throws DataAccessException {
		String listProductCode = "";
		// for (SaleOrderDetailVOEx detail : listOrderDetail) {
		// // neu khong du mat hang
		// if (detail.getStockQuantity() < detail.getQuantity()) {
		// listProductCode += detail.getProductCode() + ";";
		// }
		// }
		//
		// if (!StringUtility.isNullOrEmpty(listProductCode)) {
		// listProductCode = listProductCode.substring(0,
		// listProductCode.lastIndexOf(";"));
		// }

		List<Product> listProNotEnoughQuantity = saleOrderDAO.checkEnoughProductQuantityInOrder(saleOrder.getId(), saleOrder.getShop().getId());

		if (null != listProNotEnoughQuantity && listProNotEnoughQuantity.size() > 0) {
			// neu khong du mat hang
			for (Product pro : listProNotEnoughQuantity) {
				listProductCode += pro.getProductCode() + ";";
			}

			if (!StringUtility.isNullOrEmpty(listProductCode)) {
				listProductCode = listProductCode.substring(0, listProductCode.lastIndexOf(";"));
			}
		}

		return listProductCode;
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void cancelSaleOrderFromTable(List<Long> listSaleOrderId, String importCode) throws BusinessException {
		if (null == listSaleOrderId || listSaleOrderId.size() <= 0) {
			throw new BusinessException("List sale order id is null or empty.");
		}

		try {
			for (Long orderId : listSaleOrderId) {
				SaleOrder saleOrder = saleOrderDAO.getSaleOrderById(orderId);
				if (saleOrder == null) {
					throw new BusinessException("Order has id '" + orderId + "' is invalidate.");
				}

				saleOrder.setApproved(SaleOrderStatus.TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM);
				// saleOrder.setImportCode(importCode);

				saleOrderDAO.updateSaleOrder(saleOrder);
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public TaxInvoiceVO getTaxInvoice(Long saleOrderId) throws BusinessException {
		if (null == saleOrderId) {
			throw new BusinessException("Sale order id is invalidate.");
		}

		try {
			TaxInvoiceVO taxInvoice = new TaxInvoiceVO();
			Invoice invoice = null;
			List<TaxInvoiceDetailVO> listInvoiceDetail = null;

			// lay thong tin hoa don
			SaleOrder saleOrder = saleOrderDAO.getSaleOrderById(saleOrderId);
			if (null == saleOrder) {
				throw new BusinessException("Sale order has id: '" + saleOrderId + "' not exist.");
			}

			// TODO: VDMS2 - chua biet sua nhu the nao
			// invoice = saleOrder.getInvoice();
			if (null == invoice) {
				throw new BusinessException("Sale order has id '" + saleOrderId + "' not have tax invoice.");
			}

			taxInvoice.setInvoice(invoice);

			// lay thong tin chi tiet hoa don
			listInvoiceDetail = saleOrderDetailDAO.getListTaxInvoiceDetailVO(saleOrderId);
			taxInvoice.setListDetail(listInvoiceDetail);

			return taxInvoice;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleOrderLot> getListSaleOrderLotBySaleOrderDetail(KPaging<SaleOrderLot> kPaging, long saleOrderDetailId) throws BusinessException {
		try {
			List<SaleOrderLot> lst = saleOrderLotDAO.getListSaleOrderLotBySaleOrderDetail(kPaging, saleOrderDetailId);
			ObjectVO<SaleOrderLot> vo = new ObjectVO<SaleOrderLot>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Integer sumQuatitySaleOrderLotByCondition(Long saleOrderId, Long saleOrderDetailId, String lot) throws BusinessException {
		try {
			return saleOrderLotDAO.sumQuatitySaleOrderLotByCondition(saleOrderId, saleOrderDetailId, lot);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleOrder> getListSaleOrderEx(SoFilter filter) throws BusinessException {
		try {
			List<SaleOrder> lst = saleOrderDAO.getListSaleOrder(filter);
			ObjectVO<SaleOrder> vo = new ObjectVO<SaleOrder>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author sangtn
	 * @since 10-06-2014
	 * @description clone from getListSaleOrderEx, change result from Entity to
	 *              VO
	 * @note for search sale order, sale order manager screen:
	 *       SearchSaleTransactionAction->searchOrder()
	 */
	@Override
	public ObjectVO<SaleOrderVOEx2> getListSaleOrderExEx(KPaging<SaleOrderVOEx2> kPaging, SoFilter filter) throws BusinessException {
		try {
			List<SaleOrderVOEx2> lst = saleOrderDAO.getListSaleOrderEx(kPaging, filter);
			ObjectVO<SaleOrderVOEx2> vo = new ObjectVO<SaleOrderVOEx2>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<SaleOrder> getListSaleOrderConfirm(SoFilter filter) throws BusinessException {
		try {
			return saleOrderDAO.getListSaleOrderConfirm(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleOrderVOEx2> getListSaleOrderVOConfirm(SoFilter filter) throws BusinessException {
		try {
			List<SaleOrderVOEx2> lst = saleOrderDAO.getListSaleOrderVOConfirm(filter);
			ObjectVO<SaleOrderVOEx2> vo = new ObjectVO<SaleOrderVOEx2>();
			vo.setkPaging(filter.getkPagingVOEx2());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailConfirm(SoFilter filter) throws BusinessException {
		try {
			return saleOrderDAO.getListSaleOrderDetailConfirm(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleOrderStockVO> getListSaleOrderStock(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws BusinessException {
		try {
			List<SaleOrderStockVO> lst = saleOrderDAO.getListSaleOrderStock(kPaging, filter);
			ObjectVO<SaleOrderStockVO> vo = new ObjectVO<SaleOrderStockVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<SaleOrderStockVO> getListSaleOrderStockUpdate(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws BusinessException {
		try {
			List<SaleOrderStockVO> lst = saleOrderDAO.getListSaleOrderStockUpdate(kPaging, filter);
			List<SaleOrderStockVO> lstWarehouse = saleOrderDAO.getListSaleOrderStockWarehouseUpdate(kPaging, filter);
			for (int i = 0, n = lst.size(); i < n; i++) {
				SaleOrderStockVO saleOrderStock = lst.get(i);
				for (int j = 0, m = lstWarehouse.size(); j < m; j++) {
					SaleOrderStockVO warehouse = lstWarehouse.get(j);
					if (saleOrderStock.getId().equals(warehouse.getId())) {
						saleOrderStock.setFromShop(warehouse.getFromShop());
						saleOrderStock.setFromWarehouse(warehouse.getFromWarehouse());
						saleOrderStock.setToShop(warehouse.getToShop());
						saleOrderStock.setToWarehouse(warehouse.getToWarehouse());
						break;
					}
				}
			}
			ObjectVO<SaleOrderStockVO> vo = new ObjectVO<SaleOrderStockVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleOrderStockVO> getListSaleOrderStockTrans(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws BusinessException {
		try {
			List<SaleOrderStockVO> lst = saleOrderDAO.getListSaleOrderStockTrans(kPaging, filter);
			ObjectVO<SaleOrderStockVO> vo = new ObjectVO<SaleOrderStockVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author sangtn
	 * @since 10-06-2014
	 * @description clone from getListSaleOrderEx2, change result from Entity to
	 *              VO
	 */
	@Override
	public ObjectVO<SaleOrderVOEx2> getListSaleOrderEx2Ex(KPaging<SaleOrderVOEx2> kPaging, SoFilter filter) throws BusinessException {
		try {
			List<SaleOrderVOEx2> lst = saleOrderDAO.getListSaleOrder2Ex(kPaging, filter);
			ObjectVO<SaleOrderVOEx2> vo = new ObjectVO<SaleOrderVOEx2>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleOrder> getListSaleOrderToReturn(SoFilter filter) throws BusinessException {
		try {
			//filter.setCheckDefault(true);
			// filter.setCheckDueDate(true);
			List<SaleOrder> lst = saleOrderDAO.getListSaleOrder(filter);
			ObjectVO<SaleOrder> vo = new ObjectVO<SaleOrder>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleOrder> getListSaleOrderEx1(SoFilter filter) throws BusinessException {
		try {
			List<SaleOrder> lst = saleOrderDAO.getListSaleOrder(filter);
			ObjectVO<SaleOrder> vo = new ObjectVO<SaleOrder>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<OrderProductVO> getListOrderProductVO(KPaging<OrderProductVO> kPaging, Long shopId, Long staffId, Long custId, String pdCode, String pdName, String ppCode, Date lockDay, Long custTypeId, Long shopType) throws BusinessException {
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (staffId == null) {
				throw new IllegalArgumentException("staffId is null");
			}
			if (custId == null) {
				throw new IllegalArgumentException("custId is null");
			}
			List<OrderProductVO> lst = productDAO.getListOrderProductVO(kPaging, shopId, staffId, custId, pdCode, pdName, ppCode, lockDay, custTypeId, shopType);
			ObjectVO<OrderProductVO> vo = new ObjectVO<OrderProductVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<OrderProductVO> getListOrderProductVOWithZ(KPaging<OrderProductVO> kPaging, Long shopId, Long staffId, Long custId, String pdCode, String pdName, String ppCode, Date lockDay) throws BusinessException {
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (staffId == null) {
				throw new IllegalArgumentException("staffId is null");
			}
			if (custId == null) {
				throw new IllegalArgumentException("custId is null");
			}
			List<OrderProductVO> lst = productDAO.getListOrderProductVOWithZ(kPaging, shopId, staffId, custId, pdCode, pdName, ppCode, lockDay);
			ObjectVO<OrderProductVO> vo = new ObjectVO<OrderProductVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public boolean approvedSaleOrder(Long saleOrderId, Boolean isApproved, String note) throws BusinessException {
		try {
			if (saleOrderId == null) {
				throw new IllegalArgumentException("saleOrderId is null");
			}
			if (isApproved == null) {
				throw new IllegalArgumentException("isApproved is null");
			}
			if (StringUtility.isNullOrEmpty(note) && isApproved == false) {
				throw new IllegalArgumentException("note is null or empty");
			}
			SaleOrder saleOrder = saleOrderDAO.getSaleOrderById(saleOrderId);
			if (saleOrder == null) {
				throw new IllegalArgumentException("saleOrder is null");
			}
			if (isApproved) {
				SortedMap<String, Integer> map = new TreeMap<String, Integer>();
				List<SaleOrderDetail> lst = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(saleOrderId, null);
				for (SaleOrderDetail saleOrderDetail : lst) {
					Integer qty = map.get(saleOrderDetail.getProduct().getProductCode());
					qty = (qty == null ? 0 + saleOrderDetail.getQuantity() : qty + saleOrderDetail.getQuantity());
					map.put(saleOrderDetail.getProduct().getProductCode(), qty);
				}
				boolean ret = true;
				String importCode = MessageUtil.getResourceString("vi", "core.info.msg.001");
				for (String productCode : map.keySet()) {
					if (!stockTotalDAO.checkQuantityInStockTotal(saleOrder.getShop().getId(), productCode, map.get(productCode))) {
						ret = false;
						importCode = importCode + String.format(" %s,", productCode);
					}
				}
				if (ret) {
					for (String productCode : map.keySet()) {
						StockTotal stockTotal = stockTotalDAO.getStockTotalByProductCodeAndOwner(productCode, StockObjectType.SHOP, saleOrder.getShop().getId());
						stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() - map.get(productCode));
						stockTotalDAO.updateStockTotal(stockTotal);
					}
					saleOrder.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
					// saleOrder.setImportCode(note);
					saleOrderDAO.updateSaleOrder(saleOrder);
				} else {
					saleOrder.setApproved(SaleOrderStatus.TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM);
					// saleOrder.setImportCode(importCode);
					saleOrderDAO.updateSaleOrder(saleOrder);
				}
				return ret;
			} else {
				saleOrder.setApproved(SaleOrderStatus.TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM);
				// saleOrder.setImportCode(note);
				saleOrderDAO.updateSaleOrder(saleOrder);
				return true;
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<SaleOrder> checkDuplicateInvoiceNumber(Long shopId, Map<Long, List<String>> listSaleOrderId) throws BusinessException {
		List<SaleOrder> listSOResult = new ArrayList<SaleOrder>();
		try {
			Set<Long> setSOId = listSaleOrderId.keySet();
			Iterator<Long> soIdIterator = setSOId.iterator();
			while (soIdIterator.hasNext()) {
				Long soId = soIdIterator.next();
				if (!invoiceDAO.checkDuplicateInvoiceNumber(shopId, listSaleOrderId.get(soId))) {
					SaleOrder so = saleOrderDAO.getSaleOrderById(soId);
					listSOResult.add(so);
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		return listSOResult;
	}

	@Override
	public List<String> checkDuplicateInvoiceNumberEx1(Long shopId, List<String> lstInvoiceNumber) throws BusinessException {
		try {
			List<String> lstResult = new ArrayList<String>();
			for (String invoiceNumber : lstInvoiceNumber) {
				if (!invoiceDAO.checkDuplicateInvoiceNumber(shopId, Arrays.asList(invoiceNumber))) {
					lstResult.add(invoiceNumber);
				}
			}
			return lstResult;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public SaleOrderDetail createSaleOrderDetail(SaleOrderDetail saleOrderDetail) throws BusinessException {
		try {
			return saleOrderDetailDAO.createSaleOrderDetail(saleOrderDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateSaleOrderDetail(SaleOrderDetail saleOrderDetail) throws BusinessException {
		try {
			saleOrderDetailDAO.updateSaleOrderDetail(saleOrderDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteSaleOrderDetail(SaleOrderDetail saleOrderDetail) throws BusinessException {
		try {
			saleOrderDetailDAO.deleteSaleOrderDetail(saleOrderDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptSaleOrderByProductLv01VO> getDataForSaleByProductReport(Long shopId, List<Long> staffIds, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<RptSaleOrderByProductDataVO> listData = saleOrderDAO.getDataForSaleOrderByProductReport(shopId, staffIds, fromDate, toDate);

			return RptSaleOrderByProductDataVO.groupByStaffAndCat(listData);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptOrderByPromotionDataVO> getDataForOrderByPromotionReport(Long shopId, List<Long> staffIds, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<RptOrderByPromotionDataVO> listData = saleOrderDAO.getDataForOrderByPromotionReport(shopId, staffIds, fromDate, toDate);

			return listData;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<SaleOrderDetail> getListSaleOrderDetailBySaleOrderId(KPaging<SaleOrderDetail> kPaging, Long saleOrderId) throws BusinessException {
		try {
			if (saleOrderId == null) {
				throw new IllegalArgumentException("saleOrderId is null");
			}
			ObjectVO<SaleOrderDetail> vo = new ObjectVO<SaleOrderDetail>();
			List<SaleOrderDetail> lst = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(saleOrderId, kPaging);
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailBySaleOrderIdAndType(Long saleOrderId, Boolean isAutoPromotion) throws BusinessException {
		try {
			if (saleOrderId == null) {
				throw new IllegalArgumentException("saleOrderId is null");
			}
			return saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderIdAndType(saleOrderId, isAutoPromotion);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ArrayList<PrintDeliveryGroupExportVO> getListPrintDeliveryGroupExportVO(List<Long> lstSaleOrderId, Date fromDate, Date toDate) throws BusinessException {
		try {
			if (lstSaleOrderId == null) {
				throw new IllegalArgumentException("listSaleOrderId is null");
			}
			//
			ArrayList<PrintDeliveryGroupExportVO> ret = new ArrayList<PrintDeliveryGroupExportVO>();
			List<PrintDeliveryGroupExportVO2> lst = saleOrderDetailDAO.getListPrintDeliveryGroupExportVO2(lstSaleOrderId, fromDate, toDate);
			if (lst == null) {
				return new ArrayList<PrintDeliveryGroupExportVO>();
			}
			PrintDeliveryGroupExportVO vo = null;
			PrintDeliveryGroupExportVO1 vo1 = null;
			String orderNumber = "";
			String deliveryCode = "";
			String deliveryName = "";

			String productCode = null;
			String productName = null;
			Integer convfact = null;
			BigDecimal price = null;

			for (PrintDeliveryGroupExportVO2 obj : lst) {
				if ((obj.getQuantity() != null && obj.getQuantity() > 0) || (obj.getProQuantity() != null && obj.getProQuantity() > 0) || (obj.getProMoney() != null && obj.getProMoney().compareTo(BigDecimal.ZERO) > 0)) {
					if (deliveryCode != null && !deliveryCode.equals(obj.getDeliveryCode())) {
						deliveryCode = obj.getDeliveryCode();
						deliveryName = obj.getDeliveryName();
						orderNumber = obj.getOrderNumber();
						//
						vo = new PrintDeliveryGroupExportVO();
						vo.setOrderNumber(orderNumber);
						vo.setDeliveryCode(deliveryCode);
						vo.setDeliveryName(deliveryName);
						ret.add(vo);
						//
						productCode = null;
						productName = null;
						convfact = new Integer(-1);
						price = new BigDecimal(-1);
					}
					if (obj.getProductCode() == null || !obj.getProductCode().equals(productCode) || obj.getConvfact().compareTo(convfact) != 0 || obj.getPrice().compareTo(price) != 0) {
						productCode = obj.getProductCode();
						productName = obj.getProductName();
						convfact = obj.getConvfact();
						price = obj.getPrice();
						//
						vo1 = new PrintDeliveryGroupExportVO1();
						vo1.setOrderNumber(orderNumber);
						vo1.setDeliveryCode(deliveryCode);
						vo1.setDeliveryName(deliveryName);
						vo1.setProductCode(productCode);
						vo1.setProductName(productName);
						vo1.setConvfact(convfact);
						vo1.setPrice(price);
						//
						if (obj.getProductCode() == null) {
							vo.getLstDeliveryGroupProMoney().add(vo1);
						} else {
							vo.getLstDeliveryGroup().add(vo1);
						}
					}
					vo1.getLstDeliveryGroup().add(obj);
				}
			}
			return ret;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptAggegateVATInvoiceDataVO> getDataForAggegateVATInvoiceReport(Long shopId, List<Long> customerId, Long staffId, Integer VAT, InvoiceStatus status, Date fromDate, Date toDate) throws BusinessException {
		try {
			if (null == shopId) {
				throw new IllegalArgumentException("Shop id is invalidate");
			}

			return invoiceDAO.getDataForAggegateVATInvoiceReport(shopId, customerId, staffId, VAT, status, fromDate, toDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Product> checkEnoughProductQuantityInListOrder(Long shopId, List<Long> listOrderId) throws BusinessException {
		try {
			if (null == shopId) {
				throw new IllegalArgumentException("Shop id is invalidate");
			}

			if (null == listOrderId || listOrderId.size() <= 0) {
				throw new IllegalArgumentException("List order is null or empty");
			}

			return saleOrderDAO.checkEnoughProductQuantityInListOrder(shopId, listOrderId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Invoice> getListInvoiceByLstInvoiceNumber(Long shopId, List<String> lstInvoiceNumber) throws BusinessException {
		try {
			if (lstInvoiceNumber == null || lstInvoiceNumber.size() == 0) {
				throw new IllegalArgumentException("lstInvoiceNumber is null or empty");
			}
			return invoiceDAO.getListInvoiceByLstInvoiceNumber(shopId, lstInvoiceNumber);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Invoice> getListInvoiceByLstInvoiceId(List<Long> lstInvoiceId) throws BusinessException {
		try {
			if (lstInvoiceId == null || lstInvoiceId.size() == 0) {
				throw new IllegalArgumentException("lstInvoiceNumber is null or empty");
			}
			return invoiceDAO.getListInvoiceByLstInvoiceId(lstInvoiceId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Car getLastestCarFromListSaleOrderNumber(List<String> lstOrderNumber) throws BusinessException {
		Car result = null;
		try {
			if (lstOrderNumber.size() == 0) {
				return null;
			}
			List<SaleOrder> lstSO = saleOrderDAO.getListSaleOrderByLstOrderNumber(null, lstOrderNumber);
			if (lstSO != null && lstSO.size() > 0) {
				int i = 0;
				do {
					result = lstSO.get(i).getCar();
				} while (result == null);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return result;
	}

	@Override
	public List<PrintDeliveryGroupVO> getListPrintDeliveryGroupVO(List<Long> listSaleOrderId) throws BusinessException {
		try {
			if (listSaleOrderId == null) {
				throw new IllegalArgumentException("listSaleOrderId is null");
			}
			List<PrintDeliveryGroupVO1> lst = saleOrderDetailDAO.getListPrintDeliveryGroupVO(listSaleOrderId);
			ArrayList<PrintDeliveryGroupVO> ret = new ArrayList<PrintDeliveryGroupVO>();
			if (lst == null) {
				return new ArrayList<PrintDeliveryGroupVO>();
			}
			PrintDeliveryGroupVO vo = null;
			String deliveryCode = "";
			String deliveryName = "";
			String customerShortCode = "";
			String customerCode = "";
			String customerName = "";
			String customerAddress = "";
			for (PrintDeliveryGroupVO1 obj : lst) {
				if (deliveryCode != null && customerShortCode != null && (!deliveryCode.equals(obj.getDeliveryCode()) || !customerShortCode.equals(obj.getCustomerShortCode()))) {
					deliveryCode = obj.getDeliveryCode();
					deliveryName = obj.getDeliveryName();
					customerShortCode = obj.getCustomerShortCode();
					customerCode = obj.getCustomerCode();
					customerName = obj.getCustomerName();
					customerAddress = obj.getCustomerAddress();
					//
					vo = new PrintDeliveryGroupVO();
					vo.setDeliveryCode(deliveryCode);
					vo.setDeliveryName(deliveryName);
					vo.setCustomerShortCode(customerShortCode);
					vo.setCustomerCode(customerCode);
					vo.setCustomerName(customerName);
					vo.setCustomerAddress(customerAddress);
					ret.add(vo);
				}
				vo.setOrderNumber(obj.getOrderNumber());
				if (obj.getIsFreeItem() != null && obj.getIsFreeItem() == 0) {
					vo.getLstDeliveryGroup().add(obj);
				}
				if (obj.getIsFreeItem() != null && obj.getIsFreeItem() == 1) {
					if (obj.getDiscountAmount() != null && obj.getDiscountAmount().compareTo(BigDecimal.ZERO) > 0) {
						vo.getLstDeliveryGroupFreeAmount().add(obj);
					} else if (obj.getQuantity() != null && obj.getQuantity() > 0) {
						vo.getLstDeliveryGroupFree().add(obj);
					}
				}
			}
			return ret;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<PrintDeliveryCustomerGroupVO> getListPrintDeliveryCustomerGroupVO(List<Long> listSaleOrderId) throws BusinessException {
		try {
			if (listSaleOrderId == null) {
				throw new IllegalArgumentException("listSaleOrderId is null");
			}
			List<PrintDeliveryCustomerGroupVO1> lst = saleOrderDetailDAO.getListPrintDeliveryCustomerGroupVO(listSaleOrderId);
			ArrayList<PrintDeliveryCustomerGroupVO> ret = new ArrayList<PrintDeliveryCustomerGroupVO>();
			if (lst == null) {
				return ret;
			}
			PrintDeliveryCustomerGroupVO vo = null;
			String deliveryCode = "";
			String deliveryName = "";
			String customerCode = "";
			String customerName = "";
			String customerAddress = "";
			String carNumber = "";
			String invoiceNumber = "";
			for (PrintDeliveryCustomerGroupVO1 obj : lst) {
				if (deliveryCode == null || !deliveryCode.equals(obj.getDeliveryCode()) || !customerCode.equals(obj.getCustomerCode()) || !carNumber.equals(obj.getCarNumber())) {
					deliveryCode = obj.getDeliveryCode();
					deliveryName = obj.getDeliveryName();
					customerCode = obj.getCustomerCode();
					customerName = obj.getCustomerName();
					customerAddress = obj.getCustomerAddress();
					carNumber = obj.getCarNumber();
					invoiceNumber = obj.getInvoiceNumber();
					//
					vo = new PrintDeliveryCustomerGroupVO();
					vo.setDeliveryCode(deliveryCode);
					vo.setDeliveryName(deliveryName);
					vo.setCustomerCode(customerCode);
					vo.setCustomerName(customerName);
					vo.setCustomerAddress(customerAddress);
					vo.setCarNumber(carNumber);
					vo.setInvoiceNumber(invoiceNumber);
					ret.add(vo);
				}
				if (obj.getIsFreeItem() != null && obj.getIsFreeItem() == 0) {
					vo.getLstDeliveryGroup().add(obj);
				}
				if (obj.getIsFreeItem() != null && obj.getIsFreeItem() == 1) {
					vo.getLstDeliveryGroupFree().add(obj);
				}
			}
			return ret;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public String checkQuantityCheckAndMax(SaleOrder saleOrder) throws BusinessException {
		try {
			List<PromotionProgram> listPP = promotionProgramDAO.getCheckQuantityPromotionShopMap(saleOrder.getId());
			StringBuilder result = new StringBuilder();
			for (int i = 0, size = listPP.size(); i < size; i++) {
				PromotionProgram pp = listPP.get(i);
				result.append(pp.getPromotionProgramCode());
				result.append(", ");
			}
			return result.toString();
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Invoice getInvoiceByInvoiceNumber(String invoiceNumber, Long shopId) throws BusinessException {
		try {
			return invoiceDAO.getInvoiceByInvoiceNumber(invoiceNumber, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public SaleOrderLot createSaleOrderLot(SaleOrderLot salesOrderLot) throws BusinessException {
		try {
			return saleOrderLotDAO.createSaleOrderLot(salesOrderLot);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author hungnm
	 * @return
	 */
	@Override
	public List<SaleOrderVOEx> getListSaleOrderVOEx(List<Long> lstInvoiceId) throws BusinessException {
		try {
			List<SaleOrderVOEx> lstRs = new ArrayList<SaleOrderVOEx>();

			List<Long> __lstInvoiceId = new ArrayList<Long>();

			List<Invoice> listInvoice = invoiceDAO.getListInvoiceByLstInvoiceIdEx(lstInvoiceId);

			for (int i = 0; i < listInvoice.size(); i++) {
				__lstInvoiceId.add(listInvoice.get(i).getId());
			}

			for (Long invoiceId : __lstInvoiceId) {
				SaleOrderVOEx vo = new SaleOrderVOEx();
				Invoice invoice = invoiceDAO.getInvoiceById(invoiceId);

				/** tungtt nhat nut in cho cap nhap invoiceDate */
				//invoice.setInvoiceDate(commonDAO.getSysDate());
				//invoiceDAO.updateInvoice(invoice);
				vo.setInvoice(invoice);
				vo.setAmount(invoice.getAmount());
				vo.setCustPayment(invoice.getCustPayment());
				SaleOrder so = saleOrderDAO.getSaleOrderByOrderNumberAndShopId(invoice.getOrderNumber(), invoice.getShop().getId());

				Customer c = so.getCustomer();
				String diachiKH = c.getAddress();
				diachiKH = diachiKH + (c.getArea() != null ? "," + c.getArea().getAreaName() : "") + (c.getArea() != null && c.getArea().getParentArea() != null ? "," + c.getArea().getParentArea().getAreaName() : "")
						+ (c.getArea() != null && c.getArea().getParentArea() != null && c.getArea().getParentArea().getParentArea() != null ? "," + c.getArea().getParentArea().getParentArea().getAreaName() : "");

				String diachiHD = c.getDeliveryAddress();
				//				if(diachiHD == null || "".equals(diachiHD)) {
				//					diachiHD = diachiKH;
				//				}

				vo.setDiachiHD(diachiHD);
				vo.setDiachiKH(diachiKH);

				vo.setSaleOrder(so);
				if (so != null) {
					List<SaleOrderDetail> lst = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(null, invoiceId, null);
					if(lst!=null && lst.size() > 0){
						lst = gopSanPham(lst, 0);
						lst = gopSanPham(lst, 1);
						vo.setLstSaleOrderDetail(lst);
					} else {
						vo.setLstSaleOrderDetail(new ArrayList<SaleOrderDetail>());
					}
				}
				lstRs.add(vo);
			}
			return lstRs;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<SaleOrder> getListSaleOrder(SaleOrderFilter filter, KPaging<SaleOrder> kPaging) throws BusinessException {
		try {
			ObjectVO<SaleOrder> vo = new ObjectVO<SaleOrder>();
			List<SaleOrder> lst = saleOrderDAO.getListSaleOrder(filter, kPaging);
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkOrderInDelay(Long staffId, int delayTime) throws BusinessException {
		try {
			return saleOrderDAO.checkOrderInDelay(staffId,delayTime);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkProductLotInSO(Long saleOrderId) throws BusinessException {
		try {
			return saleOrderDAO.checkProductLotInSO(saleOrderId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<SaleOrderLot> getListSaleOrderLotBySaleOrderId(Long saleOrderId) throws BusinessException {
		try {
			List<SaleOrderLot> list = saleOrderDAO.getListSaleOrderLotBySaleOrderId(saleOrderId);
			if (list == null) {
				list = new ArrayList<SaleOrderLot>();
			}
			return list;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Integer getOrderDebitMaxCustomer(SaleOrder saleOrder, Date lockDay) throws BusinessException {
		try {
			return saleOrderDAO.getOrderDebitMaxCustomer(saleOrder, lockDay);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * 3.1.3.1.2 Phiáº¿u xuáº¥t hÃ ng theo nhÃ¢n viÃªn giao hÃ ng
	 * 
	 * @author hungnm
	 * @param kPaging
	 * @param shopId
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public List<RptExSaleOrder2VO> getListSaleOrderByDeliveryStaffGroupByProductCode(List<Long> listSaleOrderId, Long shopId) throws BusinessException {
		try {
			List<RptExSaleOrderVO> lst = saleOrderDetailDAO.getListSaleOrderByDeliveryStaffGroupByProductCode(listSaleOrderId, shopId);
			List<RptExSaleOrder2VO> lstVo2 = new ArrayList<RptExSaleOrder2VO>();

			for (int i = 0; i < lst.size(); i++) {
				RptExSaleOrderVO voS = lst.get(i);
				RptExSaleOrder2VO vo2 = new RptExSaleOrder2VO();
				SortedMap<String, String> sortedOrderNumber = new TreeMap<String, String>();
				vo2.setDeliveryStaffInfo(voS.getDeliveryStaffInfo());
				vo2.setDeliveryStaffId(voS.getDeliveryStaffId());

				Integer thungPromo = 0;
				Integer thungSale = 0;
				Integer lePromo = 0;
				Integer leSale = 0;
				BigDecimal totalPromoAmount = new BigDecimal(0);
				BigDecimal totalSaleAmount = new BigDecimal(0);

				List<RptExSaleOrder1VO> lstVo1 = new ArrayList<RptExSaleOrder1VO>();
				for (int j = i; j < lst.size(); j++) {
					if (lst.get(j).getDeliveryStaffInfo() != null)
						if (lst.get(j).getDeliveryStaffInfo().equals(voS.getDeliveryStaffInfo())) {
							RptExSaleOrder1VO vo1 = new RptExSaleOrder1VO();
							vo1.setPrice(lst.get(j).getPrice());
							vo1.setProductCode(lst.get(j).getProductCode());
							vo1.setProductName(lst.get(j).getProductName());

							BigDecimal promoAmount = new BigDecimal(0);
							BigDecimal saleAmount = new BigDecimal(0);
							BigDecimal discountAmount = new BigDecimal(0);

							List<RptExSaleOrderVO> lstVo = new ArrayList<RptExSaleOrderVO>();
							for (int k = j; k < lst.size(); k++) {
								if (lst.get(k).getProductCode().equals(vo1.getProductCode()) && lst.get(k).getDeliveryStaffInfo().equals(voS.getDeliveryStaffInfo())) {
									RptExSaleOrderVO vo = lst.get(k);
									lstVo.add(vo);
									if (vo.getPrice() != null && vo.getSaleQuantity() != null) {
										saleAmount = saleAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getSaleQuantity())));
										totalSaleAmount = totalSaleAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getSaleQuantity())));
										discountAmount = discountAmount.add(vo.getDiscountAmount());
									}
									if (vo.getPrice() != null && vo.getPromoQuantity() != null) {
										promoAmount = promoAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getPromoQuantity())));
										totalPromoAmount = totalPromoAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getPromoQuantity())));
										discountAmount = discountAmount.add(vo.getDiscountAmount());
									}
									thungPromo += vo.getThungPromo();
									thungSale += vo.getThungSale();
									lePromo += vo.getLePromo();
									leSale += vo.getLeSale();
									j = k;
								} else
									break;
							}
							vo1.setPromoAmount(promoAmount);
							vo1.setSaleAmount(saleAmount);
							for (RptExSaleOrderVO vo : lstVo) {
								vo.setPromoAmount(promoAmount);
								vo.setSaleAmount(saleAmount);
								if (vo.getOrderNumber() != null)
									sortedOrderNumber.put(vo.getOrderNumber(), vo.getOrderNumber());
							}

							vo1.setLstRptExSaleOrderVO(lstVo);
							vo1.setDiscountAmount(discountAmount);
							lstVo1.add(vo1);
							i = j;
						} else
							break;
				}
				vo2.setTotalPromoQuantity(thungPromo.toString() + "/" + lePromo.toString());
				vo2.setTotalSaleQuantity(thungSale.toString() + "/" + leSale.toString());
				vo2.setTotalPromoAmount(totalPromoAmount);
				vo2.setTotalSaleAmount(totalSaleAmount);
				vo2.setLstSaleOrder(lstVo1);
				String temp = "";
				for (String item : sortedOrderNumber.keySet()) {
					temp += item + ",";
				}
				sortedOrderNumber.clear();
				vo2.setLstOrderNumber(temp);
				lstVo2.add(vo2);
			}
			return lstVo2;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * In phieu giao hang gop theo KH
	 * 
	 * @author thongnm
	 */
	@Override
	public List<RptPGHVO> getPGHGroupByKH(List<Long> lstSaleOrderId, String strListShopId) throws BusinessException {
		try {
			List<RptPGHVO> lst = saleOrderDAO.getPGHGroupByKH(lstSaleOrderId, strListShopId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * In phieu giao hang gop theo DH
	 * 
	 * @author thongnm
	 */
	@Override
	public List<RptPGHVO> getPGHGroupByDH(List<Long> listSaleOrderId, String strListShopId) throws BusinessException {
		try {
			List<RptPGHVO> lst = saleOrderDAO.getPGHGroupByDH(listSaleOrderId, strListShopId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptBHTSP_DSNhanVienBH> getBHTSP_DSNhanVienVO(String shopCode, String dt_tu_ngay, String dt_den_ngay, String nhan_vien_ban_hang) {
		List<RptBHTSP_DSNhanVienBH> lst = new ArrayList<RptBHTSP_DSNhanVienBH>();

		try {
			// saleOrderDAO = new SaleOrderDAOImpl();
			lst = saleOrderDAO.getBHTSP_DSNhanVienVO(shopCode, dt_tu_ngay, dt_den_ngay, nhan_vien_ban_hang);
			System.out.println("Tong la : " + lst.size());
		} catch (Exception ex) {
			System.out.println("Error of system : " + ex.getMessage());
		}

		return lst;
	}

	@Override
	public List<RptPGHVO> getPGHGroupByNVGH(List<Long> listSaleOrderId, String strListShopId) throws BusinessException {
		try {
			List<RptPGHVO> lst = saleOrderDAO.getPGHGroupByNVGH(listSaleOrderId, strListShopId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public int countSaleOrderNotApprovedOfShopInDay(Long shopId, Date lockDate) throws BusinessException {
		try {
			int count = saleOrderDAO.countSaleOrderNotApprovedOfShopInDay(shopId, lockDate);
			return count;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<SaleOrder> getListSaleOrderById(List<Long> listSaleOrderId) throws BusinessException {
		try {
			List<SaleOrder> lst = saleOrderDAO.getListSaleOrderById(listSaleOrderId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public List<SaleOrder> getListSaleOrderById2(List<Long> listSaleOrderId) throws BusinessException {
		try {
			List<SaleOrder> lst = saleOrderDAO.getListSaleOrderById2(listSaleOrderId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public SaleOrder getSaleOrderByOrderNumberAndShopId(String orderNumber, Long shopId) throws BusinessException {
		try {
			SaleOrder lst = saleOrderDAO.getSaleOrderByOrderNumberAndShopId(orderNumber, shopId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public SaleOrder getSaleOrderByOrderNumberAndShopIdApproved(String orderNumber, Long shopId) throws BusinessException {
		try {
			SaleOrder lst = saleOrderDAO.getSaleOrderByOrderNumberAndShopIdApproved(orderNumber, shopId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public SaleOrder getSaleOrderByOrderNumberAndListShop(String orderNumber, String lstShopId) throws BusinessException {
		try {
			SaleOrder lst = saleOrderDAO.getSaleOrderByOrderNumberAndListShop(orderNumber, lstShopId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<SaleOrder> copySaleOrderForListCustomer(SaleOrder so, List<Long> lstCustomerId,LogInfoVO logInfoVO) throws BusinessException {
		try {
			List<SaleOrder> lst = new ArrayList<SaleOrder>();
			LogInfoVO logInfo = new LogInfoVO();
			logInfo.setStaffCode(so.getCreateUser());
			List<SaleOrderDetail> lstSODetail = null;
			List<SaleOrderLot> lstSOLot = null;
			SaleOrderDetail soDetail = null;
			SaleOrder s = null;
			List<SaleProductVO> lstSaleProductVO = null;
			Date now = shopLockDAO.getNextLockedDay(so.getShop().getId());
			if (now == null) {
				now = commonDAO.getSysDate();
			}
			Long productId = null;
			for (Long id : lstCustomerId) {
				lstSaleProductVO = new ArrayList<SaleProductVO>();
				Customer cus = customerDAO.getCustomerById(id);
				if (cus == null) {
					throw new BusinessException("the customer id is invalid");
				}
				//				SaleOrder s = (SaleOrder) org.apache.commons.lang.SerializationUtils.clone(so);
				s = so.clone();
				s.setOrderSource(SaleOrderSource.WEB);
				s.setTimePrint(null);
				s.setPrintBatch(null);
				s.setCustomer(cus);
				s.setOrderDate(now);
				s.setApproved(SaleOrderStatus.NOT_YET_APPROVE);
				s.setOrderType(OrderType.IN);
				s.setType(SaleOrderType.NOT_YET_RETURNED);
				s.setOrderNumber(null);

				//TUNGTT
				/**
				 * Kiem tra don hang trong tuyen va ngoai tuyen Tim duoc nhan
				 * vien ban hang thi co nghia la trong tuyen IS_VISIT_PLAN
				 * =1,nguoc lai thi ngoai tuyen IS_VISIT_PLAN =0
				 */
				Staff sStaff = null;
				if (s.getCustomer() != null && s.getShop() != null) {
					RoutingCustomerFilter filter = new RoutingCustomerFilter();
					filter.setCustomerId(s.getCustomer().getId());
					filter.setShopId(s.getShop().getId());
					filter.setOrderDate(now);
					List<Staff> lstS = staffDAO.getSaleStaffByDate(filter);
					if (lstS != null && lstS.size() > 0) {
						sStaff = lstS.get(0);
					}
				}
				if (sStaff != null && sStaff.getStaffCode().equals(s.getStaff().getStaffCode())) {
					s.setIsVisitPlan(1);
				} else {
					s.setIsVisitPlan(0);
				}

				//				//check han no cua customer
				//				if (!BigDecimal.ZERO.equals(s.getAmount())) {
				//					this.checkDebitInSaleOrder(s.getShop().getId(), s.getCustomer().getId(), s.getAmount());
				//				}
				StaffCustomer sc = staffCustomerDAO.getStaffCustomer(s.getStaff().getId(), s.getCustomer().getId());
				if (sc != null) {
					sc.setLastOrder(now);
					staffCustomerDAO.updateStaffCustomer(sc, null);
				} else {
					sc = new StaffCustomer();
					sc.setCustomer(s.getCustomer());
					sc.setStaff(s.getStaff());
					sc.setLastOrder(now);
					sc.setShop(s.getShop());
					staffCustomerDAO.createStaffCustomer(sc, logInfo);
				}
				// cap nhat bang customer
				Customer ct = s.getCustomer();
				ct.setLastOrder(now);
				customerDAO.updateCustomer(ct, null);
				
				//Modified by NhanLT6 - 10/11/2014 - Cap nhat them truong last_approved_order de phuc vu 
				//dong bo du lieu voi sales_tablet
				RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDate(s.getCustomer().getId(),s.getShop().getId(), s.getOrderDate());
				if(rc != null) {
					rc.setLastOrder(s.getOrderDate());
					routingCustomerDAO.updateRoutingCustomer(rc);
				}

				lstSODetail = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(so.getId(), null);
				if (lstSODetail != null) {
					for (int i = 0; i < lstSODetail.size(); i++) {
						soDetail = lstSODetail.get(i).clone();
						soDetail.setId(null);
						soDetail.setOrderDate(now);
						//Tinh lai lo
						lstSOLot = saleOrderLotDAO.getListSaleOrderLotBySaleOrderDetailId(lstSODetail.get(i).getId());
						if (lstSOLot != null) {
							for (SaleOrderLot lot : lstSOLot) {
								if (s != null && s.getShop() != null) {
									StockTotal st = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(soDetail.getProduct().getId(), StockObjectType.SHOP, s.getShop().getId(), lot.getWarehouse().getId());
									/** Khong co san pham trong kho thi tao moi */
									if (st == null) {
										st = new StockTotal();
										st.setAvailableQuantity(0);
										st.setQuantity(0);
										st.setCreateDate(s.getOrderDate());
										st.setCreateUser(logInfo.getStaffCode());
										st.setObjectId(s.getShop().getId());
										st.setShop(s.getShop());
										st.setObjectType(StockObjectType.SHOP);
										st.setProduct(productDAO.getProductById(productId));
										st.setWarehouse(lot.getWarehouse());
										st = stockTotalDAO.createStockTotal(st);
									}
									st.setAvailableQuantity(st.getAvailableQuantity() - soDetail.getQuantity());
								}
							}
						}
						SaleProductVO spVO = new SaleProductVO();
						spVO.setSaleOrderDetail(soDetail);
						spVO.setLstSaleOrderLot(lstSOLot);
						lstSaleProductVO.add(spVO);
					}
				}
				SaleOrder tmp = saleOrderDAO.createSaleOrder(s, lstSaleProductVO, null, false, null, null, logInfoVO);
				lst.add(tmp);
			}
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updateReturnOrderAutoDebit(long saleOrderId, String staffCode) throws BusinessException {
		try {
			SaleOrder saleOrder = saleOrderDAO.getSaleOrderById(saleOrderId);

			if (saleOrder == null) {
				throw new BusinessException("Saleorder does not exist");
			}
			if (SaleOrderType.NOT_YET_RETURNED.equals(saleOrder.getType()) || SaleOrderType.NOT_YET_RETURNED.equals(saleOrder.getType())) {
				throw new BusinessException("Saleorder's status is not NOT_YET_RETURNED or GIFT_ORDER");
			}
			//thong tin debit detail cua don hang goc
			DebitDetail debitDetailDG = debitDetailDAO.getDebitDetailBySaleOrderId(saleOrder.getFromSaleOrder().getId());
			//thong tin debit detail cua don tra
			DebitDetail debitDetailDT = debitDetailDAO.getDebitDetailBySaleOrderId(saleOrder.getId());
			Date lockDate = shopLockDAO.getNextLockedDay(saleOrder.getShop().getId());
			if (lockDate == null) {
				lockDate = commonDAO.getSysDate();
			}
			Date now = commonDAO.getSysDate();
			if (debitDetailDG != null) {
				//chÆ°a thanh toÃ¡n
				if (debitDetailDG.getTotalPay().intValue() == 0) {
					//Tao phieu thu cho don hang goc
					//TÃ­nh toÃ¡n láº¡i sá»‘ tiá»�n ná»£, tráº£ cá»§a tá»«ng Ä‘Æ¡n hÃ ng báº£ng DEBIT_DETAIL, SHOP_ID = id NPP, FROM_OBJECT_ID = ID Ä‘Æ¡n hÃ ng gá»‘c
					debitDetailDG.setTotalPay(debitDetailDG.getTotal());
					debitDetailDG.setRemain(new BigDecimal(0));
					debitDetailDAO.updateDebitDetail(debitDetailDG);

					//LÆ°u thÃ´ng tin vÃ o báº£ng PAY_RECEIVED 
					PayReceived pr = new PayReceived();
					pr.setPayReceivedNumber(saleOrderDAO.generatePayNumber(saleOrder.getShop().getId(), ReceiptType.RECEIVED, commonDAO.getSysDate()));
					pr.setPaymentType(PaymentType.MONEY);
					pr.setShop(saleOrder.getShop());
					//pr.setCustomer(saleOrder.getCustomer());
					pr.setAmount(debitDetailDG.getTotal());
					pr.setReceiptType(ReceiptType.RECEIVED);
					pr.setType(PayReceivedType.THANH_TOAN.getValue());
					pr.setCreateUser(staffCode);
					pr.setCreateDate(lockDate);
					pr.setCreateDateSys(now);
					pr = payReceivedDAO.createPayReceived(pr);

					//LÆ°u thÃ´ng tin vÃ o báº£ng PAYMENT_DETAIL
					PaymentDetail pd = new PaymentDetail();
					pd.setDebit(debitDetailDG);
					pd.setPayReceived(pr);
					pd.setAmount(debitDetailDG.getTotal());
					pd.setPaymentType(DebtPaymentType.AUTO);
					pd.setCreateUser(staffCode);
					pd.setPayDate(lockDate);
					pd.setCreateDate(now);
					pd.setCustomer(saleOrder.getCustomer());
					pd = paymentDetailDAO.createPaymentDetail(pd);

					//hoatv13 Update 
					//Cap nhat gia tri truong debit
					Debit debit = debitDAO.getDebitForUpdate(saleOrder.getCustomer().getId(), DebitOwnerType.CUSTOMER);
					if (debit != null) {
						debit.setTotalPay(debit.getTotalPay().add(debitDetailDG.getTotal()));
						debit.setTotalDebit(debit.getTotalDebit().subtract(debitDetailDG.getTotal()));
						debitDAO.updateDebit(debit);
					}
					//end update

					//Tao phieu chi cho don hang tra
					if (debitDetailDT != null) {
						//TÃ­nh toÃ¡n láº¡i sá»‘ tiá»�n ná»£, tráº£ cá»§a tá»«ng Ä‘Æ¡n hÃ ng báº£ng DEBIT_DETAIL,FROM_OBJECT_ID = ID Ä‘Æ¡n hÃ ng tra
						debitDetailDT.setTotalPay(debitDetailDT.getTotalPay().add(debitDetailDT.getTotal()));
						debitDetailDT.setRemain(new BigDecimal(0));
						debitDetailDT.setFromObjectNumber(saleOrder.getOrderNumber());//
						debitDetailDAO.updateDebitDetail(debitDetailDT);

						//LÆ°u thÃ´ng tin vÃ o báº£ng PAY_RECEIVED 
						PayReceived prPC = new PayReceived();
						prPC.setPayReceivedNumber(saleOrderDAO.generatePayNumber(saleOrder.getShop().getId(), ReceiptType.PAID, commonDAO.getSysDate()));
						prPC.setPaymentType(PaymentType.MONEY);
						prPC.setShop(saleOrder.getShop());
						//prPC.setCustomer(saleOrder.getCustomer());
						prPC.setAmount(debitDetailDT.getTotal());
						prPC.setReceiptType(ReceiptType.PAID);
						prPC.setType(PayReceivedType.THANH_TOAN.getValue());
						prPC.setCreateUser(staffCode);
						prPC.setCreateDate(lockDate);
						prPC.setCreateDateSys(now);
						prPC = payReceivedDAO.createPayReceived(prPC);

						//LÆ°u thÃ´ng tin vÃ o báº£ng PAYMENT_DETAIL
						PaymentDetail pdPC = new PaymentDetail();
						pdPC.setDebit(debitDetailDT);
						pdPC.setPayReceived(prPC);
						pdPC.setAmount(debitDetailDT.getTotal());
						pdPC.setPaymentType(DebtPaymentType.AUTO);
						pdPC.setCreateUser(staffCode);
						pdPC.setCreateDate(now);
						pdPC.setPayDate(lockDate);
						pdPC.setCustomer(saleOrder.getCustomer());
						pdPC = paymentDetailDAO.createPaymentDetail(pdPC);

						//hoatv13 Update 
						//Cap nhat gia tri truong debit
						if (debit != null) {
							debit.setTotalPay(debit.getTotalPay().add(debitDetailDT.getTotal()));
							debit.setTotalDebit(debit.getTotalDebit().subtract(debitDetailDT.getTotal()));
							debitDAO.updateDebit(debit);
						}
						//end update
					}
				}

				//da thanh toÃ¡n hoan toan
				else if (debitDetailDG.getRemain().intValue() == 0) {
					//Tao phieu chi cho don hang tra
					if (debitDetailDT != null) {

						//TUNGTT
						BigDecimal discountDG = debitDetailDG.getDiscountAmount().multiply(new BigDecimal(-1));

						//TÃ­nh toÃ¡n láº¡i sá»‘ tiá»�n ná»£, tráº£ cá»§a tá»«ng Ä‘Æ¡n hÃ ng báº£ng DEBIT_DETAIL,FROM_OBJECT_ID = ID Ä‘Æ¡n hÃ ng tra
						//LamNH fix bug 24842
						debitDetailDT.setTotalPay(debitDetailDT.getTotalPay().add(debitDetailDT.getTotal()).subtract(discountDG));
						debitDetailDT.setRemain(new BigDecimal(0));
						debitDetailDT.setDiscountAmount(discountDG);
						debitDetailDT.setFromObjectNumber(saleOrder.getOrderNumber());
						debitDetailDAO.updateDebitDetail(debitDetailDT);

						//LÆ°u thÃ´ng tin vÃ o báº£ng PAY_RECEIVED 
						PayReceived prPC = new PayReceived();
						prPC.setPayReceivedNumber(saleOrderDAO.generatePayNumber(saleOrder.getShop().getId(), ReceiptType.PAID, commonDAO.getSysDate()));
						prPC.setPaymentType(PaymentType.MONEY);
						prPC.setShop(saleOrder.getShop());
						//prPC.setCustomer(saleOrder.getCustomer());
						prPC.setAmount(debitDetailDT.getTotal().subtract(discountDG));
						prPC.setReceiptType(ReceiptType.PAID);
						prPC.setType(PayReceivedType.THANH_TOAN.getValue());
						prPC.setCreateUser(staffCode);
						prPC.setCreateDate(lockDate);
						prPC.setCreateDateSys(now);
						prPC = payReceivedDAO.createPayReceived(prPC);

						//LÆ°u thÃ´ng tin vÃ o báº£ng PAYMENT_DETAIL
						PaymentDetail pdPC = new PaymentDetail();
						pdPC.setDebit(debitDetailDT);
						pdPC.setPayReceived(prPC);
						pdPC.setAmount(debitDetailDT.getTotal());
						pdPC.setPaymentType(DebtPaymentType.AUTO);
						pdPC.setCreateUser(staffCode);
						pdPC.setCreateDate(now);
						pdPC.setPayDate(lockDate);
						pdPC.setDiscount(discountDG);
						pdPC.setCustomer(saleOrder.getCustomer());
						pdPC = paymentDetailDAO.createPaymentDetail(pdPC);

						//Cap nhat gia tri truong debit
						Debit debit = debitDAO.getDebitForUpdate(saleOrder.getCustomer().getId(), DebitOwnerType.CUSTOMER);
						if (debit != null) {
							debit.setTotalPay(debit.getTotalPay().add(debitDetailDT.getTotal()).subtract(discountDG));
							debit.setTotalDebit(debit.getTotalDebit().subtract(debitDetailDT.getTotal()));
							debit.setTotalDiscount(debit.getTotalDiscount().add(discountDG));
							debitDAO.updateDebit(debit);
						}
					}
				}
				//thanh toÃ¡n 1 pháº§n
				else if (debitDetailDG.getTotal().intValue() > debitDetailDG.getTotalPay().intValue()) {

					//TUNGTT
					BigDecimal discountDG = debitDetailDG.getDiscountAmount().multiply(new BigDecimal(-1));

					//LÆ°u thÃ´ng tin vÃ o báº£ng PAY_RECEIVED 
					PayReceived pr = new PayReceived();
					pr.setPayReceivedNumber(saleOrderDAO.generatePayNumber(saleOrder.getShop().getId(), ReceiptType.RECEIVED, commonDAO.getSysDate()));
					pr.setPaymentType(PaymentType.MONEY);
					pr.setShop(saleOrder.getShop());
					//pr.setCustomer(saleOrder.getCustomer());
					pr.setAmount(debitDetailDG.getRemain());
					pr.setReceiptType(ReceiptType.RECEIVED);
					pr.setType(PayReceivedType.THANH_TOAN.getValue());
					pr.setCreateUser(staffCode);
					pr.setCreateDate(lockDate);
					pr.setCreateDateSys(now);
					pr = payReceivedDAO.createPayReceived(pr);

					//LÆ°u thÃ´ng tin vÃ o báº£ng PAYMENT_DETAIL
					PaymentDetail pd = new PaymentDetail();
					pd.setDebit(debitDetailDG);
					pd.setPayReceived(pr);
					pd.setAmount(debitDetailDG.getRemain());
					pd.setPaymentType(DebtPaymentType.AUTO);
					pd.setCreateUser(staffCode);
					pd.setPayDate(lockDate);
					pd.setCreateDate(now);
					pd.setCustomer(saleOrder.getCustomer());
					pd = paymentDetailDAO.createPaymentDetail(pd);

					//hoatv13 Update 
					//Cap nhat gia tri truong debit
					Debit debit = debitDAO.getDebitForUpdate(saleOrder.getCustomer().getId(), DebitOwnerType.CUSTOMER);
					if (debit != null) {
						debit.setTotalPay(debit.getTotalPay().add(debitDetailDG.getRemain()));
						debit.setTotalDebit(debit.getTotalDebit().subtract(debitDetailDG.getRemain()));
						debitDAO.updateDebit(debit);
					}

					debitDetailDG.setTotalPay(debitDetailDG.getTotalPay().add(debitDetailDG.getRemain()));
					debitDetailDG.setRemain(new BigDecimal(0));
					debitDetailDAO.updateDebitDetail(debitDetailDG);

					//end update

					//Tao phieu chi cho don hang tra
					if (debitDetailDT != null) {
						//TÃ­nh toÃ¡n láº¡i sá»‘ tiá»�n ná»£, tráº£ cá»§a tá»«ng Ä‘Æ¡n hÃ ng báº£ng DEBIT_DETAIL,FROM_OBJECT_ID = ID Ä‘Æ¡n hÃ ng tra
						debitDetailDT.setTotalPay(debitDetailDT.getTotalPay().add(debitDetailDT.getTotal()).subtract(discountDG));
						debitDetailDT.setRemain(new BigDecimal(0));
						debitDetailDT.setDiscountAmount(discountDG);
						debitDetailDT.setFromObjectNumber(saleOrder.getOrderNumber());//
						debitDetailDAO.updateDebitDetail(debitDetailDT);

						//LÆ°u thÃ´ng tin vÃ o báº£ng PAY_RECEIVED 
						PayReceived prPC = new PayReceived();
						prPC.setPayReceivedNumber(saleOrderDAO.generatePayNumber(saleOrder.getShop().getId(), ReceiptType.PAID, commonDAO.getSysDate()));
						prPC.setPaymentType(PaymentType.MONEY);
						prPC.setShop(saleOrder.getShop());
						//prPC.setCustomer(saleOrder.getCustomer());
						prPC.setAmount(debitDetailDT.getTotal().subtract(discountDG));
						prPC.setReceiptType(ReceiptType.PAID);
						prPC.setType(PayReceivedType.THANH_TOAN.getValue());
						prPC.setCreateUser(staffCode);
						prPC.setCreateDate(now);
						prPC.setCreateDateSys(lockDate);
						prPC = payReceivedDAO.createPayReceived(prPC);

						//LÆ°u thÃ´ng tin vÃ o báº£ng PAYMENT_DETAIL
						PaymentDetail pdPC = new PaymentDetail();
						pdPC.setDebit(debitDetailDT);
						pdPC.setPayReceived(prPC);
						pdPC.setAmount(debitDetailDT.getTotal());
						pdPC.setPaymentType(DebtPaymentType.AUTO);
						pdPC.setCreateUser(staffCode);
						pdPC.setCreateDate(now);
						pdPC.setPayDate(lockDate);
						pdPC.setDiscount(discountDG);
						pdPC.setCustomer(saleOrder.getCustomer());
						pdPC = paymentDetailDAO.createPaymentDetail(pdPC);

						//hoatv13 Update 
						//Cap nhat gia tri truong debit
						if (debit != null) {
							debit.setTotalPay(debit.getTotalPay().add(debitDetailDT.getTotal()).subtract(discountDG));
							debit.setTotalDebit(debit.getTotalDebit().subtract(debitDetailDT.getTotal()));
							debit.setTotalDiscount(debit.getTotalDiscount().add(discountDG));
							debitDAO.updateDebit(debit);
						}
						//end update
					}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<SaleOrder> getListSaleOrderById(List<Long> listSaleOrderId, Integer orderType) throws BusinessException {
		try {
			List<SaleOrder> lst = saleOrderDAO.getListSaleOrderById(listSaleOrderId, orderType);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<String> getListProductHasPriceError(SaleOrder so) throws BusinessException {
		try {
			List<String> listResult = new ArrayList<String>();

			List<SaleOrderDetail> listDetail = saleOrderDetailDAO.getListSaleProductBySaleOrderId(so.getId());
			for (int i = 0, size = listDetail.size(); i < size; i++) {
				SaleOrderDetail detail = listDetail.get(i);
				Price currentPrice = priceDAO.getPriceByProductId(detail.getProduct().getId());
				if (currentPrice.getPrice().compareTo(detail.getPriceValue()) != 0) {
					listResult.add(detail.getProduct().getProductCode());
				}
			}
			return listResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<OrderProductVO> getListOrderProductVansale(KPaging<OrderProductVO> kPaging, Long shopId, Long staffId, String pdCode, String pdName, Date lockDay) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (staffId == null) {
				throw new IllegalArgumentException("staffId is null");
			}
			List<OrderProductVO> lst = productDAO.getListOrderProductVansale(kPaging, shopId, staffId, pdCode, pdName, lockDay);
			ObjectVO<OrderProductVO> vo = new ObjectVO<OrderProductVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<Invoice> getListInvoiceBySaleOrderId(Long saleOrderId) throws BusinessException {
		try {
			return invoiceDAO.getListInvoiceBySaleOrderId(saleOrderId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public List<Invoice> getListInvoiceBySaleOrderId2(long saleOrderId, int invoiceType) throws BusinessException {
		try {
			return invoiceDAO.getListInvoiceBySaleOrderId2(saleOrderId, invoiceType);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<RptPXKKVCNB_DataConvert> getPhieuXKVCNB(List<Long> lstsoId) throws BusinessException {
		try {
			List<RptPXKKVCNB_DataConvert> lst = saleOrderDAO.getPhieuXKVCNB(lstsoId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<Invoice> getListInvoiceByConditionEx(KPaging<Invoice> kPaging, String orderNumber, String redInvoiceNumber, String staffCode, String deliveryCode, String shortCode, Date fromDate, Date toDate, Float vat,
			InvoiceCustPayment custPayment, Date lockDate, Long shopId, Integer isValueOrder) throws BusinessException {
		try {
			List<Invoice> lst = invoiceDAO.getListInvoiceByCondition(kPaging, vat, orderNumber, redInvoiceNumber, fromDate, toDate, staffCode, deliveryCode, shortCode, custPayment, lockDate, shopId, isValueOrder, null, null);

			ObjectVO<Invoice> vo = new ObjectVO<Invoice>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * lay danh sach kho theo SP
	 * 
	 * @author nhanlt6
	 * @param paging
	 * @param shopId
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public ObjectVO<OrderProductVO> getListOrderProductWarehouseVO(KPaging<OrderProductVO> paging, Long productId, Long shopId) throws BusinessException {
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (productId == null) {
				throw new IllegalArgumentException("productId is null");
			}
			List<OrderProductVO> lst = productDAO.getListOrderProductWarehouseVO(paging, productId, shopId);
			ObjectVO<OrderProductVO> vo = new ObjectVO<OrderProductVO>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * lay danh sach Don hang da bi sua so luong
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 * @throws BusinessException
	 */
	@Override
	public List<OrderProductVO> getListProductChangeQuantityWarning(SoFilter filter) throws BusinessException {
		try {
			if (filter.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			List<OrderProductVO> lst = saleOrderDAO.getListProductChangeQuantityWarning(filter);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	/**
	 * lay danh sach Don hang da bi sua so suat
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 * @throws BusinessException
	 */
	@Override
	public List<OrderProductVO> getListProductChangeRationWarning(SoFilter filter) throws BusinessException {
		try {
			if (filter.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			List<OrderProductVO> lst = saleOrderDAO.getListProductChangeRationWarning(filter);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * lay danh sach SP vuot ton kho
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 * @throws BusinessException
	 */
	@Override
	public List<OrderProductVO> getListProductQuantityWarning(SoFilter filter) throws BusinessException {
		try {
			if (filter.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			List<OrderProductVO> lst = saleOrderDAO.getListProductQuantityWarning(filter);
			List<Long> lstProductId = new ArrayList<Long>();
			List<Long> lstSaleOrderId = new ArrayList<Long>();
			Map<Long, Integer> mapProduct = new HashMap<Long , Integer>();
			if (lst != null && lst.size() > 0) {
				//loai bo dong trung
				for (int i = 0; i < lst.size(); i++) {
					for (int j = i + 1; j < lst.size(); j++) {
						if (lst.get(i).getSaleOrderId().equals(lst.get(j).getSaleOrderId()) && lst.get(i).getProductId().equals(lst.get(j).getProductId())) {
							lst.remove(j);
							j--;
						}
					}
					if (!lstProductId.contains(lst.get(i).getProductId())) {
						lstProductId.add(lst.get(i).getProductId());
					}
					if (!lstSaleOrderId.contains(lst.get(i).getSaleOrderId())) {
						lstSaleOrderId.add(lst.get(i).getSaleOrderId());
					}
					mapProduct.put(lst.get(i).getProductId(), lst.get(i).getQuantity());
				}
				//phan bo so luong de tinh don hang
				if(lstProductId.size()>0){
					SoFilter soF = new SoFilter();
					soF.setLstProductId(lstProductId);
					soF.setLstSaleOrderId(lstSaleOrderId);
					List<OrderProductVO> lstSOD = saleOrderDAO.getListSOProductQuantityByList(soF);
					for(int i = 0 ; i < lstSOD.size(); i++){
						Integer stock = mapProduct.get(lstSOD.get(i).getProductId());
						if(stock!=null && stock.compareTo(lstSOD.get(i).getQuantity()) >= 0){
							//xoa khoi danh sach loi
							for (int k = 0; k < lst.size() ; k++) {
								if(lst.get(k).getSaleOrderId().equals(lstSOD.get(i).getSaleOrderId()) 
										&& lst.get(k).getProductId().equals(lstSOD.get(i).getProductId())){
									lst.remove(k);
									k--;
								}
							}
							//neu la confirm thi tru` ton kho
							if(Boolean.TRUE.equals(filter.getIsConfirm())){
								stock = stock - lstSOD.get(i).getQuantity();
								mapProduct.put(lstSOD.get(i).getProductId(), stock);
							}
						}
					}
					//set lai ton kho khi confirm (lay ton kho sau khi xac nhan)
					if(Boolean.TRUE.equals(filter.getIsConfirm())){
						for (int i = 0; i < lst.size() ; i++) {
							Integer stock = mapProduct.get(lst.get(i).getProductId());
							if(stock != null){
								lst.get(i).setQuantity(stock);
							}
						}
					}
				}
			}
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * lay danh sach SP sai gia
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 * @throws BusinessException
	 */
	@Override
	public List<OrderProductVO> getListProductPriceWarning(SoFilter filter) throws BusinessException {
		try {
			if (filter.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			List<OrderProductVO> lst = saleOrderDAO.getListProductPriceWarning(filter);
			//distinct nhung don hang co 2 dong sp trung nhau
			if (lst != null && lst.size() > 0) {
				for (int i = 0; i < lst.size() - 1; i++) {
					for (int j = i + 1; j < lst.size(); j++) {
						if (lst.get(i).getSaleOrderId().equals(lst.get(j).getSaleOrderId()) && lst.get(i).getProductId().equals(lst.get(j).getProductId())) {
							lst.remove(j);
							j--;
						}
					}
				}
			}
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * lay danh sach CTKM het han
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 * @throws BusinessException
	 */
	@Override
	public List<OrderProductVO> getListProductPromotionWarning(SoFilter filter) throws BusinessException {
		try {
			if (filter.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			return saleOrderDAO.getListProductPromotionWarning(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public String getListOrderNumberLostDetail(SoFilter filter) throws BusinessException {
		try {
			List<SpParam> inParams = new ArrayList<SpParam>();
	        List<SpParam> outParams = new ArrayList<SpParam>();
	        
	        String strOrderId = "";
	        if(filter.getLstSaleOrderId()!=null){
	        	strOrderId = filter.getLstSaleOrderId().get(0).toString();
	        	for(int i = 1 ; i < filter.getLstSaleOrderId().size() ; i++){
	        		strOrderId += "," + filter.getLstSaleOrderId().get(i).toString();
	        	}
	        }
	        inParams.add(new SpParam(2, java.sql.Types.NUMERIC, filter.getShopId()));
	        inParams.add(new SpParam(3, java.sql.Types.DATE, filter.getFromDate()));
	        inParams.add(new SpParam(4, java.sql.Types.DATE, filter.getLockDay()));
	        inParams.add(new SpParam(5, java.sql.Types.VARCHAR, strOrderId));
	        outParams.add(new SpParam(1, java.sql.Types.VARCHAR,null));
	        repo.executeFunc("F_CHECK_SOD", inParams, outParams);
	        if(outParams.get(0).getValue()!=null){
	        	return outParams.get(0).getValue().toString();
	        }
	        return null;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public String checkOrderIntegrity(SoFilter filter) throws BusinessException {
		try {
			List<SpParam> inParams = new ArrayList<SpParam>();
			List<SpParam> outParams = new ArrayList<SpParam>();

			String strOrderId = "";
			if (filter.getLstSaleOrderId() != null) {
				strOrderId = filter.getLstSaleOrderId().get(0).toString();
				for (int i = 1; i < filter.getLstSaleOrderId().size(); i++) {
					strOrderId += "," + filter.getLstSaleOrderId().get(i).toString();
				}
			}
			inParams.add(new SpParam(2, java.sql.Types.NUMERIC, filter.getShopId()));
			inParams.add(new SpParam(3, java.sql.Types.DATE, filter.getFromDate()));
			inParams.add(new SpParam(4, java.sql.Types.DATE, filter.getLockDay()));
			inParams.add(new SpParam(5, java.sql.Types.VARCHAR, strOrderId));
			outParams.add(new SpParam(1, java.sql.Types.VARCHAR, null));
			repo.executeFunc("F_CHECK_ORDER_INTEGRIRY", inParams, outParams);
			if (outParams.get(0).getValue() != null) {
				return outParams.get(0).getValue().toString();
			}
			return null;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<OrderProductVO> getListWarningPromotionOfOrder(SoFilter filter) throws BusinessException {
		try {
			if (filter.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			return saleOrderDAO.getListWarningPromotionOfOrder(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * lay danh sach amount va discount
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 * @throws BusinessException
	 */
	@Override
	public List<OrderProductVO> getListProductAmountWarning(SoFilter filter) throws BusinessException {
		try {
			if (filter.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			return saleOrderDAO.getListProductAmountWarning(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * lay danh sach tra thuong keyshop vuot muc
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 * @throws BusinessException
	 */
	@Override
	public List<OrderProductVO> getListKeyShopWarning(SoFilter filter) throws BusinessException {
		try {
			if (filter.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			return saleOrderDAO.getListKeyShopWarning(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * lay danh sach SP vuot ton kho
	 * 
	 * @author tungmt
	 * @since 25/08/2014
	 * @throws BusinessException
	 */
	@Override
	public List<SaleOrderDetailVOEx> getListSaleOrderDetailForConfirm(SoFilter filter) throws BusinessException {
		try {
			if (filter.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (filter.getSaleOrderId() == null) {
				throw new IllegalArgumentException("saleOrderId is null");
			}
			return saleOrderDAO.getListSaleOrderDetailForConfirm(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<SaleOrderDetailVOEx> getListSaleOrderDetailByOrderId(long saleOrderId, Integer isFreeItem, ProgramType programType, Boolean isAutoPromotion, Boolean isGetCTTL) throws BusinessException {
		try {
			List<SaleOrderDetailVOEx> lst = saleOrderDetailDAO.getListSaleOrderDetailByOrderId(saleOrderId, isFreeItem, programType, isAutoPromotion,isGetCTTL);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updatePOCustomer(SoFilter filter) throws BusinessException {
		try {
			saleOrderDAO.updatePOCustomer(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void denyListSaleOrder(List<SaleOrder> lstOrder, SoFilter filter, LogInfoVO loginfo) throws BusinessException {
		try {
			List<Long> lstPoCustomerId = new ArrayList<Long>();
			for (SaleOrder so : lstOrder) {
				if (so.getFromPoCustomerId() != null) {
					lstPoCustomerId.add(so.getFromPoCustomerId());
				}
				so.setApproved(SaleOrderStatus.TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM);
				if (!StringUtility.isNullOrEmpty(filter.getDescription())) {
					so.setDescription(filter.getDescription());
				}

				//Cap nhat don hang cuoi cung
				updateCustomerAndRouting(so, filter, loginfo);
				so.setUpdateDate(commonDAO.getSysDate());
				so.setUpdateUser(loginfo.getStaffCode());

				if(OrderType.IN.equals(so.getOrderType())){
					List<SaleOrderLot> lstSaleOrderLot1 = saleOrderDAO.getListSaleOrderLotBySaleOrderId(so.getId());
					if (lstSaleOrderLot1 != null && lstSaleOrderLot1.size() > 0) {
						for (SaleOrderLot sol : lstSaleOrderLot1) {
							StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(sol.getProduct().getId(), StockObjectType.SHOP, sol.getShop().getId(), sol.getWarehouse().getId());// tulv2 26.09.2014
							if (stockTotal != null) {
								
								/** Ghi log */
								try {
									loginfo.getContenFormat(sol.getProduct().getId(), 
											sol.getWarehouse().getId(), 
											sol.getQuantity(), 
											stockTotal.getAvailableQuantity(), 
											stockTotal.getAvailableQuantity() + sol.getQuantity(), 
											stockTotal.getQuantity(), 
											stockTotal.getQuantity());									
								} catch (Exception e) {}
								/**End ghi log*/
								
								stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() + sol.getQuantity());
								stockTotalDAO.updateStockTotal(stockTotal); // Cap nhat TON KHO DAP UNG trong StockTotal
							}
							saleOrderLotDAO.deleteSaleOrderLot(sol); // Xoa SALE ORDER LOT
						}
					}
				}
				
				// Xu ly tra thuong khuyen mai tich luy (them dong co gia tri am de bu lai)
//				this.negateRptCTTLPayAndDetail(so, 2); // tu choi
				
				/** Ghi log ra file */
				try {
					loginfo.setActionType(LogInfoVO.ACTION_UPDATE);
					loginfo.setFunctionCode(LogInfoVO.FC_SALE_DENY_ORDER);
					loginfo.setIdObject(so.getId().toString());
					LogUtility.logInfoWs(loginfo);
				} catch (Exception e) {}
				/** End ghi log ra file */
			}
			
			insertNewTable(lstOrder, 3, loginfo, -1);
			
			saleOrderDAO.updateSaleOrder(lstOrder);
			if (lstPoCustomerId.size() > 0) {
				SoFilter filterPO = new SoFilter();
				filterPO.setStaffCode(loginfo.getStaffCode());
				filterPO.setLstPOCustomerId(lstPoCustomerId);
				filterPO.setApproval(SaleOrderStatus.TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM);
				saleOrderDAO.updatePOCustomer(filterPO);
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void cancelListSaleOrder(List<SaleOrder> lstOrder, SoFilter filter, LogInfoVO loginfo) throws BusinessException {
		try {
			List<Long> lstPoCustomerId = new ArrayList<Long>();
			Date sysdate = commonDAO.getSysDate();
			for (SaleOrder so : lstOrder) {
				if (OrderType.IN.equals(so.getOrderType())) {
					if (so.getFromPoCustomerId() != null) {
						lstPoCustomerId.add(so.getFromPoCustomerId());
					}
					so.setApproved(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM);
					if (!StringUtility.isNullOrEmpty(filter.getDescription())) {
						so.setDescription(filter.getDescription());
					}
					//update don hang cuoi cung
					updateCustomerAndRouting(so, filter, loginfo);
					so.setUpdateDate(sysdate);
					so.setUpdateUser(loginfo.getStaffCode());

					List<SaleOrderLot> lstSaleOrderLot1 = saleOrderDAO.getListSaleOrderLotBySaleOrderId(so.getId());
					if (lstSaleOrderLot1 != null && lstSaleOrderLot1.size() > 0) {
						for (SaleOrderLot sol : lstSaleOrderLot1) {
							StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(sol.getProduct().getId(), StockObjectType.SHOP, sol.getShop().getId(), sol.getWarehouse().getId());// tulv2 26.09.2014
							if (stockTotal != null) {
								
								/** Ghi log */
								try {
									loginfo.getContenFormat(sol.getProduct().getId(), 
											sol.getWarehouse().getId(), 
											sol.getQuantity(), 
											stockTotal.getAvailableQuantity(), 
											stockTotal.getAvailableQuantity() + sol.getQuantity(), 
											stockTotal.getQuantity(), 
											stockTotal.getQuantity());									
								} catch (Exception e) {}
								/**End ghi log*/
								
								stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() + sol.getQuantity());
								stockTotalDAO.updateStockTotal(stockTotal); // Cap nhat TON KHO DAP UNG trong StockTotal
							}
							saleOrderLotDAO.deleteSaleOrderLot(sol); // Xoa SALE ORDER LOT
						}
					}
					
					// Xu ly tra thuong khuyen mai tich luy (them dong co gia tri am de bu lai)
//					this.negateRptCTTLPayAndDetail(so, 3); // huy
				}
				
				/** Ghi log ra file */
				try {
					loginfo.setActionType(LogInfoVO.ACTION_UPDATE);
					loginfo.setFunctionCode(LogInfoVO.FC_SALE_CANCEL_ORDER);
					loginfo.setIdObject(so.getId().toString());
					LogUtility.logInfoWs(loginfo);
				} catch (Exception e) {}
				/** End ghi log ra file */
			}
			insertNewTable(lstOrder, 4, loginfo, -1);
			saleOrderDAO.updateSaleOrder(lstOrder);
			if (lstPoCustomerId.size() > 0) {
				SoFilter filterPO = new SoFilter();
				filterPO.setStaffCode(loginfo.getStaffCode());
				filterPO.setLstPOCustomerId(lstPoCustomerId);
				filterPO.setApproval(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM);
				saleOrderDAO.updatePOCustomer(filterPO);
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * Sao chep rpt_cttl_pay, rpt_cttl_detail_pay cua don hang, va doi nguoc dau cua cac gia tri khuyen mai
	 * 
	 * @author lacnv1
	 * @since Jan 05, 2015
	 */
	private void negateRptCTTLPayAndDetail(SaleOrder so, Integer approved) throws BusinessException {
		try {
			SaleOrderFilter<RptCTTLPay> cttlFilter = new SaleOrderFilter<RptCTTLPay>();
			RptCTTLPay pay = null;
			RptCTTLPay payNew = null;
			List<RptCTTLPayDetail> lstPayDetails = null;
			RptCTTLPayDetail payDetail = null;
			RptCTTLPayDetail payDetailNew = null;
			Date ld = commonDAO.getSysDate();
			if(so != null && so.getShop() != null) {
				ld = shopLockDAO.getNextLockedDay(so.getShop().getId());
			}
			cttlFilter.setSaleOrderId(so.getId());
			List<RptCTTLPay> lstPays = promotionProgramDAO.getListRptCTTLPay(cttlFilter);
			if (lstPays != null && lstPays.size() > 0) {
				for (int j = 0, szj = lstPays.size(); j < szj; j++) {
					pay = lstPays.get(j);
					
					// tao 1 RptCTTLPay moi de bu lai
					payNew = new RptCTTLPay();
					payNew.setSaleOrder(so);
					payNew.setCreateDate(ld);
					payNew.setPromotionProgram(pay.getPromotionProgram());
					payNew.setPromotionFromDate(pay.getPromotionFromDate());
					payNew.setPromotionToDate(pay.getPromotionToDate());
					payNew.setShop(so.getShop());
					payNew.setCustomer(so.getCustomer());
					if (pay.getTotalQuantityPayPromotion() != null) {
						payNew.setTotalQuantityPayPromotion(pay.getTotalQuantityPayPromotion().negate());
					}
					if (pay.getTotalAmountPayPromotion() != null) {
						payNew.setTotalAmountPayPromotion(pay.getTotalAmountPayPromotion().negate());
					}
					if (pay.getPromotion() != null) {
						payNew.setPromotion(pay.getPromotion() * (-1));
					}
					payNew.setStaff(pay.getStaff());

					/*if (pay.getAmountPromotion() != null) {
						payNew.setAmountPromotion(pay.getAmountPromotion().negate());
					}*/
					/*if (pay.getActuallyPromotion() != null) {
						payNew.setActuallyPromotion(pay.getActuallyPromotion().negate());
					}*/
					
					if (approved != null) {
						payNew.setApproved(approved);
						pay.setApproved(approved);
					} else {
						payNew.setApproved(pay.getApproved());
					}
					payNew.setIsMigrate(0);
					
					payNew = commonDAO.createEntity(payNew);
					
					// chep RptCTTLDetailPay
					lstPayDetails = promotionProgramDAO.getRptCTTLPayDetailByRptPayId(pay.getId());
					if (payNew != null && lstPayDetails != null && lstPayDetails.size() > 0) {
						for (int k = 0, szk = lstPayDetails.size(); k < szk; k++) {
							payDetail = lstPayDetails.get(k);
							
							payDetailNew = new RptCTTLPayDetail();
							
							payDetailNew.setCreateDate(ld);
							payDetailNew.setRptCTTLPay(payNew);
							payDetailNew.setProduct(payDetail.getProduct());
							if (payDetail.getQuantityPromotion() != null) {
								payDetailNew.setQuantityPromotion(payDetail.getQuantityPromotion().negate());
							}
							if (payDetail.getAmountPromotion() != null) {
								payDetailNew.setAmountPromotion(payDetail.getAmountPromotion().negate());
							}
							payDetailNew.setStaff(payDetail.getStaff());
							payDetailNew.setShop(so.getShop());
							
							commonDAO.createEntity(payDetailNew);
						}
					}
					
					commonDAO.updateEntity(pay);
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * Sao chep rpt_cttl_pay, rpt_cttl_detail_pay cua don hang, va doi nguoc dau cua cac gia tri khuyen mai cho tra hang
	 * 
	 * @author tungmt
	 * @since 09/01/2015
	 */
	private void negateRptCTTLPayAndDetailForReturn(SaleOrder so, SaleOrder soNew) throws BusinessException {
		try {
			SaleOrderFilter<RptCTTLPay> cttlFilter = new SaleOrderFilter<RptCTTLPay>();
			RptCTTLPay pay = null;
			RptCTTLPay payNew = null;
			List<RptCTTLPayDetail> lstPayDetails = null;
			RptCTTLPayDetail payDetail = null;
			RptCTTLPayDetail payDetailNew = null;
			Date sysdate = commonDAO.getSysDate();
			
			cttlFilter.setSaleOrderId(so.getId());
			List<RptCTTLPay> lstPays = promotionProgramDAO.getListRptCTTLPay(cttlFilter);
			if (lstPays != null && lstPays.size() > 0) {
				for (int j = 0, szj = lstPays.size(); j < szj; j++) {
					pay = lstPays.get(j);
					
					// tao 1 RptCTTLPay moi de bu lai
					payNew = new RptCTTLPay();
					payNew.setSaleOrder(soNew);
					payNew.setCreateDate(sysdate);
					payNew.setPromotionProgram(pay.getPromotionProgram());
					payNew.setPromotionFromDate(pay.getPromotionFromDate());
					payNew.setPromotionToDate(pay.getPromotionToDate());
					payNew.setShop(soNew.getShop());
					payNew.setCustomer(soNew.getCustomer());
					if (pay.getTotalQuantityPayPromotion() != null) {
						payNew.setTotalQuantityPayPromotion(pay.getTotalQuantityPayPromotion().negate());
					}
					if (pay.getTotalAmountPayPromotion() != null) {
						payNew.setTotalAmountPayPromotion(pay.getTotalAmountPayPromotion().negate());
					}
					if (pay.getPromotion() != null) {
						payNew.setPromotion(pay.getPromotion() * (-1));
					}
					payNew.setStaff(pay.getStaff());

					if (soNew.getApproved() != null) {
						payNew.setApproved(soNew.getApproved().getValue());
//						pay.setApproved(soNew.getApproved().getValue());
					} else {
						payNew.setApproved(pay.getApproved());
					}
					payNew.setIsMigrate(0);
					
					payNew = commonDAO.createEntity(payNew);
					
					// chep RptCTTLDetailPay
					lstPayDetails = promotionProgramDAO.getRptCTTLPayDetailByRptPayId(pay.getId());
					if (payNew != null && lstPayDetails != null && lstPayDetails.size() > 0) {
						for (int k = 0, szk = lstPayDetails.size(); k < szk; k++) {
							payDetail = lstPayDetails.get(k);
							
							payDetailNew = new RptCTTLPayDetail();
							
							payDetailNew.setCreateDate(sysdate);
							payDetailNew.setRptCTTLPay(payNew);
							payDetailNew.setProduct(payDetail.getProduct());
							if (payDetail.getQuantityPromotion() != null) {
								payDetailNew.setQuantityPromotion(payDetail.getQuantityPromotion().negate());
							}
							if (payDetail.getAmountPromotion() != null) {
								payDetailNew.setAmountPromotion(payDetail.getAmountPromotion().negate());
							}
							payDetailNew.setStaff(payDetail.getStaff());
							payDetailNew.setShop(soNew.getShop());
							
							commonDAO.createEntity(payDetailNew);
						}
					}
//					commonDAO.updateEntity(pay);
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	public void payListOrder(SoFilter filter, LogInfoVO loginfo) throws BusinessException {
		try {
			// lacnv1 - chinh sua do thay doi cach day du lieu ben tablet
			
			//lay danh sach paymentDetail group theo khach hang va don hang
			List<SaleOrderVO> lstVO = saleOrderDAO.getListDiscountAndAmount(filter);//payment_detail
			
			Date sysdate = commonDAO.getSysDate();
			Date lockDate = null;
			Shop sh = null;
			if (lstVO != null && lstVO.size() > 0) {
				sh = shopDAO.getShopById(lstVO.get(0).getShopId());
				if (sh != null) {
					lockDate = shopLockDAO.getApplicationDate(sh.getId());
				}
			}
			
			//cap nhat debit cua khach hang
			//List<Long> lstCustomerId = new ArrayList<Long>();
			List<DebitDetail> lstDD = null;
			DebitDetail dd = null;
			int i, sz;
			Debit db = null;
			Customer cust = null;
			Staff st = null;
			for (SaleOrderVO vo : lstVO) {
				//lstCustomerId.add(vo.getCustomerId());
				db = debitDAO.getDebit(vo.getCustomerId(), DebitOwnerType.CUSTOMER);
				
				db.setTotalPay(db.getTotalPay().add(vo.getAmount()));
				db.setTotalDebit(db.getTotalDebit().subtract(vo.getAmount().add(vo.getDiscount())));
				db.setTotalDiscount(db.getTotalDiscount().add(vo.getDiscount()));
				
				debitDAO.updateDebit(db);
				
				// debit_detail
				filter.setLstSaleOrderId(Arrays.asList(vo.getSaleOrderId()));
				lstDD = debitDetailDAO.getListDebitDetailByFilter(filter);//debitdetail
				
				if (lstDD != null && lstVO != null && lstDD.size() > 0) {
					for (i = 0, sz = lstDD.size(); i < sz; i++) {
						dd = lstDD.get(i);
						if (dd.getFromObjectId() != null && dd.getFromObjectId().equals(vo.getSaleOrderId()) && dd.getCustomer().getId().equals(vo.getCustomerId())) {
							dd.setTotalPay(dd.getTotalPay().add(vo.getAmount()));
							dd.setDiscountAmount(dd.getDiscountAmount().add(vo.getDiscount()));
							dd.setRemain(dd.getTotal().subtract(dd.getTotalPay()).subtract(dd.getDiscountAmount()));
							if (dd.getTotal().signum() * dd.getRemain().signum() < 0) {
								throw new IllegalArgumentException("TOTAL_PAY_MORE_THAN_TOTAL"+vo.getOrderNumber());
							}
							break;
						}
					}
					debitDetailDAO.updateDebitDetail(lstDD);
				} else {
					dd = new DebitDetail();
					
					dd.setCreateDate(sysdate);
					dd.setDebitDate(lockDate);
					dd.setAmount(dd.getAmount().add(vo.getDiscount()));
					dd.setDiscount(vo.getDiscount());
					dd.setTotal(dd.getAmount());
					dd.setTotalPay(vo.getAmount());
					dd.setRemain(dd.getTotal().subtract(dd.getTotalPay()));
					if (loginfo != null) {
						dd.setCreateUser(loginfo.getStaffCode().toUpperCase());
					}
					dd.setDebit(db);
					dd.setFromObjectId(vo.getSaleOrderId());
					dd.setFromObjectNumber(vo.getOrderNumber());
					dd.setType(DebitDetailType.BAN_HANG.getValue());
					cust = customerDAO.getCustomerById(vo.getCustomerId());
					dd.setCustomer(cust);
					dd.setShop(sh);
					st = staffDAO.getStaffById(vo.getStaffId());
					dd.setStaff(st);
					
					if (dd.getTotal().signum() * dd.getRemain().signum() < 0) {
						throw new IllegalArgumentException("TOTAL_PAY_MORE_THAN_TOTAL"+vo.getOrderNumber());
					}
					
					dd = debitDetailDAO.createDebitDetail(dd);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void confirmPayListSaleOrder(SoFilter filter, String description, LogInfoVO loginfo) throws BusinessException {
		try {
			//Thanh toan cong no cho don hang
			this.payListOrder(filter, loginfo);
			//Chep du lieu tu bang temp sang bang chinh
			saleOrderDAO.coppyPayment(filter.getLstPayReceivedId(), loginfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<SearchSaleTransactionErrorVO> confirmListSaleOrder(List<SaleOrder> lstOrder, String description, LogInfoVO loginfo) throws BusinessException {
		try {
			if (lstOrder == null || lstOrder.size() == 0) {
				return null;
			}
			Date sysDate = commonDAO.getSysDate();
			List<SearchSaleTransactionErrorVO> lstErr = new ArrayList<SearchSaleTransactionErrorVO>();
			// lay cau hinh phan biet kho ban - kho KM
			boolean isDiVidualWarehouse = false;
			List<ApParam> aparams = apParamMgr.getListApParam(ApParamType.SYS_WAREHOUSE_CONFIG, ActiveType.RUNNING);
			if (aparams == null || aparams.size() == 0 || Constant.SYS_WAREHOUSE_CONFIG_DIVIDUAL.equals(aparams.get(0).getValue())) {
				isDiVidualWarehouse = true;
			}
			for (SaleOrder so : lstOrder) {
				try {
					this.confirmOneSaleOrder(so, description, isDiVidualWarehouse, sysDate, loginfo);
				} catch (Exception e) {
					String s = e.getMessage();
					SearchSaleTransactionErrorVO err = new SearchSaleTransactionErrorVO();
					err.setId(so.getId());
					err.setOrderNumber(so.getOrderNumber());
					if (s != null && s.startsWith("INVALID_AVAILABLE_QUANTITY_STOCK_TOTAL")) {
						err.setQuantityErr(s.replaceFirst("INVALID_AVAILABLE_QUANTITY_STOCK_TOTAL:", ""));
					} else if (s != null && s.equals("DON_HANG_DA_XU_LY")) {
						err.setConfirmedErr("X");
					} else {
						err.setSystemError("X");
					}
					lstErr.add(err);
				}
			}
			return lstErr;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	private void confirmOneSaleOrder(SaleOrder so, String description, boolean isDividualWarehouse, Date sysDate, LogInfoVO loginfo) throws Exception {
		if (OrderType.IN.equals(so.getOrderType())) {
			List<SaleOrderLot> lstSOL = saleOrderDAO.getListSaleOrderLotBySaleOrderId(so.getId());
			if (lstSOL == null || lstSOL.size() == 0) {
				//lay sod cua so
				List<SaleOrderDetail> lst = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(so.getId(), null);
				if (lst != null && lst.size() > 0) {
					//Tach kho cho tung san pham
					lstSOL = this.autoSplitWarehouseForProductEx(lst, true, isDividualWarehouse, loginfo);
					//sau khi tach kho saleOrderLot thi them saleOrderPromoLot
					this.createPromoLotForTablet(so);
				}
			}
		}
		checkOrder(so.getId(), CommonContanst.SALE_ORDER_CONFIRM_STEP);
		so.setApprovedStep(SaleOrderStep.CONFIRMED);
		so.setDescription(description);
		so.setUpdateDate(sysDate);
		so.setUpdateUser(loginfo.getStaffCode());
		saleOrderDAO.updateSaleOrder(so);
	}

	/**
	 * Tach kho tu dong
	 * 
	 * @author tungmt
	 */
	public List<SaleOrderLot> autoSplitWarehouse(SaleOrder so, boolean isCreateSaleOrderLot, boolean isDividualWarehouse, LogInfoVO loginfo) throws BusinessException {
		List<SaleOrderLot> lstSOL = null;
		try {
			//Kiem tra da tach kho chua
			lstSOL = saleOrderDAO.getListSaleOrderLotBySaleOrderId(so.getId());
			if (lstSOL != null && lstSOL.size() > 0) {
				return lstSOL;//da tach roi thi khong tach nua
			}

			//lay sod cua so
			List<SaleOrderDetail> lst = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(so.getId(), null);
			if (lst != null && lst.size() > 0) {
				//Tach kho cho tung san pham
				lstSOL = this.autoSplitWarehouseForProduct(lst, isCreateSaleOrderLot, isDividualWarehouse, loginfo);
				
				//sau khi tach kho saleOrderLot thi them saleOrderPromoLot
				this.createPromoLotForTablet(so);
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		return lstSOL;
	}
	
	/**
	 * Tao du lieu sale_order_promo_lot cho don hang tablet khi xac nhan
	 * @author tungmt
	 * @version 1.0
	 * @param filter
	 * @return Luu thong tin promolot khi xac nhan don hang tablet
	 * @exception BusinessException
	 * @see
	 * @since 18/8/2015
	 * @serial
	 */
	public void createPromoLotForTablet(SaleOrder so) throws BusinessException {
		try {
			List<SaleOrderLot> lstLot = saleOrderDAO.getListSaleOrderLotBySaleOrderId(so.getId());
			if (lstLot != null && lstLot.size() > 0) {
				//Lay danh sach promoDetail
				List<SaleOrderPromoDetail> lstSPD = saleOrderDAO.getListSaleOrderPromoDetailBySaleOrder(so.getId());
				if (lstSPD != null && lstSPD.size() > 0) {
					for (SaleOrderPromoDetail detail : lstSPD) {
						//phan bo theo tung promodetail
						BigDecimal totalQuantity = new BigDecimal(detail.getSaleOrderDetail().getQuantity());
						BigDecimal totalDiscount = detail.getDiscountAmount();
						BigDecimal remain = totalDiscount;//dong` cuoi se + them phan remain cho du so luong tong
						List<SaleOrderPromoLot> lstPromoLot = new ArrayList<SaleOrderPromoLot>();
						for (SaleOrderLot lot : lstLot) {
							if (detail.getSaleOrderDetail().getId().equals(lot.getSaleOrderDetail().getId())) {
								SaleOrderPromoLot promoLot = new SaleOrderPromoLot();
								promoLot.setIsOwner(detail.getIsOwner());
								promoLot.setOrderDate(so.getOrderDate());
								promoLot.setProgramCode(detail.getProgramCode());
								promoLot.setProgrameTypeCode(detail.getProgrameTypeCode());
								promoLot.setProgramType(detail.getProgramType());
								promoLot.setSaleOrder(so);
								promoLot.setSaleOrderDetail(detail.getSaleOrderDetail());
								promoLot.setSaleOrderLotId(lot.getId());
								promoLot.setShop(so.getShop());
								promoLot.setStaff(so.getStaff());
								promoLot.setDiscountAmount(totalDiscount.multiply(new BigDecimal(lot.getQuantity())).divide(totalQuantity, 0, RoundingMode.FLOOR));
								remain = remain.subtract(promoLot.getDiscountAmount());
								lstPromoLot.add(promoLot);
							}
						}
						if (lstPromoLot.size() > 0) {
							//dong` cuoi sẽ + thêm phần remain cho đủ số tổng
							lstPromoLot.get(lstPromoLot.size() - 1).setDiscountAmount(lstPromoLot.get(lstPromoLot.size() - 1).getDiscountAmount().add(remain));
							saleOrderLotDAO.createSaleOrderPromoLot(lstPromoLot);
						}
					}
				}
			}
		}catch(Exception e){
			BusinessException ex = new BusinessException(e);
			if (!StringUtility.isNullOrEmpty(e.getMessage())) {
				List<String> errCodes = new ArrayList<String>();
				errCodes.add(e.getMessage());
				ex.setErrorCodes(errCodes);
			}
			throw ex; 
		}
	}
	
	private List<SaleOrderLot> autoSplitWarehouseForProductEx(List<SaleOrderDetail> lstSod, boolean isCreateSaleOrderLot, boolean isDividualWarehouse, LogInfoVO loginfo) throws Exception {
		List<SaleOrderLot> lstSol = new ArrayList<SaleOrderLot>();
		String productCodeError = "";
		List<Long> lstProductIdError = new ArrayList<Long>();
		List<Long> lstProductId = new ArrayList<Long>();
		for (SaleOrderDetail sod : lstSod) {
			if (sod.getProduct() != null) {
				lstProductId.add(sod.getProduct().getId());
			}
		}
		List<StockTotal> lstStockTotal = new ArrayList<StockTotal>();
		//lay danh sach stocktotal cua cac san pham trong kho npp
		StockTotalVOFilter filter = new StockTotalVOFilter();
		filter.setShopId(lstSod.get(0).getShop().getId());
		filter.setOwnerType(StockObjectType.SHOP);
		filter.setStatus(ActiveType.RUNNING);
		filter.setLstProductId(lstProductId);
		lstStockTotal = stockTotalDAO.getListStockTotalByShopAndProduct(filter);
		////==============
		String logContent = loginfo.getContent();
		for (SaleOrderDetail sod : lstSod) {
			if (sod.getProduct() != null && sod.getQuantity() != 0 && !lstProductIdError.contains(sod.getProduct().getId())) {
				Long productId = sod.getProduct().getId();
				Integer quantity = sod.getQuantity();
				if (!isDividualWarehouse) {
					if (IsFreeItemInSaleOderDetail.SALE.getValue().equals(sod.getIsFreeItem())) {
						// order by st.product_id, w.warehouse_type, w.seq
						Collections.sort(lstStockTotal, new Comparator<StockTotal>() {
							@Override
							public int compare(StockTotal o1, StockTotal o2) {
								if (o1.getProduct() != null && o2.getProduct() != null) {
									int compareProduct = o1.getProduct().getId().compareTo(o2.getProduct().getId());
									if (compareProduct != 0) {
										return compareProduct;
									}
								}
								if (o1.getWarehouse() != null && o2.getWarehouse() != null) {
									if (o1.getWarehouse().getWarehouseType() != null && o2.getWarehouse().getWarehouseType() != null) {
										int compareWarehouseType = (o1.getWarehouse().getWarehouseType().getValue().compareTo(o2.getWarehouse().getWarehouseType().getValue()));
										if (compareWarehouseType != 0) {
											return compareWarehouseType;
										}
									}
									if (o1.getWarehouse().getSeq() != null && o2.getWarehouse().getSeq() != null) {
										return o1.getWarehouse().getSeq().compareTo(o2.getWarehouse().getSeq());
									}
								}
								return 0;
							}
						});
					} else if (IsFreeItemInSaleOderDetail.PROMOTION.getValue().equals(sod.getIsFreeItem())) {
						// order by st.product_id, w.warehouse_type desc, w.seq 
						Collections.sort(lstStockTotal, new Comparator<StockTotal>() {
							@Override
							public int compare(StockTotal o1, StockTotal o2) {
								if (o1.getProduct() != null && o2.getProduct() != null) {
									int compareProduct = o1.getProduct().getId().compareTo(o2.getProduct().getId());
									if (compareProduct != 0) {
										return compareProduct;
									}
								}
								if (o1.getWarehouse() != null && o2.getWarehouse() != null) {
									if (o1.getWarehouse().getWarehouseType() != null && o2.getWarehouse().getWarehouseType() != null) {
										int compareWarehouseType = (o2.getWarehouse().getWarehouseType().getValue().compareTo(o1.getWarehouse().getWarehouseType().getValue()));
										if (compareWarehouseType != 0) {
											return compareWarehouseType;
										}
									}
									if (o1.getWarehouse().getSeq() != null && o2.getWarehouse().getSeq() != null) {
										return o1.getWarehouse().getSeq().compareTo(o2.getWarehouse().getSeq());
									}
								}
								return 0;
							}
						});
					}
				}
				for (StockTotal st : lstStockTotal) {
					//tach kho theo loai kho ban or kho km
					if (st.getProduct() != null
							&& productId.equals(st.getProduct().getId())
							&& st.getWarehouse() != null
							&& (!isDividualWarehouse || ((WarehouseType.SALES.equals(st.getWarehouse().getWarehouseType()) && IsFreeItemInSaleOderDetail.SALE.getValue().equals(sod.getIsFreeItem())) || (WarehouseType.PROMOTION.equals(st
									.getWarehouse().getWarehouseType()) && IsFreeItemInSaleOderDetail.PROMOTION.getValue().equals(sod.getIsFreeItem()))))) {
						if (st.getAvailableQuantity() > 0) {
							Integer quantityLot = quantity;
							BigDecimal discountAmount = sod.getDiscountAmount();
							Float discountPercent = sod.getDiscountPercent();

							Integer availableQuantityBeforeChange = st.getAvailableQuantity();
							Integer availableQuantityChange = 0;
							if (st.getAvailableQuantity() < quantity) {//kho khong du so luong cho sod
								quantityLot = st.getAvailableQuantity();
								quantity = quantity - quantityLot;
								availableQuantityChange = quantityLot;
								st.setAvailableQuantity(0);
							} else {//kho du v
								availableQuantityChange = quantity;
								st.setAvailableQuantity(st.getAvailableQuantity() - availableQuantityChange);
								quantity = 0;
							}

							/** Ghi log */
							try {
								loginfo.getContenFormat(st.getProduct().getId(), st.getWarehouse().getId(), availableQuantityChange, availableQuantityBeforeChange, st.getAvailableQuantity(), st.getQuantity(), st.getQuantity());
							} catch (Exception e) {
							}
							/** End ghi log */

							st.setUpdateDate(commonDAO.getSysDate());
							st.setUpdateUser(loginfo.getStaffCode());
							Float percentQuantity = (float) quantityLot / sod.getQuantity();
							SaleOrderLot sol = new SaleOrderLot();
							sol.setWarehouse(st.getWarehouse());
							sol.setStockTotal(st);
							sol.setSaleOrder(sod.getSaleOrder());
							sol.setSaleOrderDetail(sod);
							sol.setProduct(sod.getProduct());
							sol.setShop(sod.getShop());
							sol.setStaff(sod.getStaff());
							sol.setQuantity(quantityLot);
							if (st.getProduct().getConvfact() == null || st.getProduct().getConvfact() == 0) {
								sol.setQuantityPackage(0);
								sol.setQuantityRetail(quantityLot);
							} else {
								sol.setQuantityPackage(quantityLot / st.getProduct().getConvfact());
								sol.setQuantityRetail(quantityLot % st.getProduct().getConvfact());
							}
							sol.setDiscountAmount(discountAmount.multiply(BigDecimal.valueOf(percentQuantity)));
							sol.setDiscountPercent(discountPercent);
							sol.setCreateDate(commonDAO.getSysDate());
							sol.setCreateUser(loginfo.getStaffCode());
							sol.setOrderDate(sod.getOrderDate());
							sol.setPriceValue(sod.getPriceValue());
							sol.setPackagePrice(sod.getPackagePrice());
							sol.setPrice(sod.getPrice());

							lstSol.add(sol);
							if (quantity == 0) {//het quantity de tach
								break;
							}
						}
					}
				}
				if (quantity > 0) {
					productCodeError += (", " + sod.getProduct().getProductCode());
					lstProductIdError.add(productId);
				}
			}
		}
		if (productCodeError.length() > 0) {
			throw new BusinessException("INVALID_AVAILABLE_QUANTITY_STOCK_TOTAL:" + productCodeError.replaceFirst(", ", ""));
		}
		if (Boolean.TRUE.equals(isCreateSaleOrderLot) && lstSol != null && lstSol.size() > 0) {
			saleOrderLotDAO.createSaleOrderLot(lstSol);
			stockTotalDAO.updateListStockTotal(lstStockTotal);
		} else {
			loginfo.setContent(logContent);
		}
		return lstSol;
	}

	/**
	 * Tach kho cho san pham
	 * 
	 * @author tungmt
	 */
	public List<SaleOrderLot> autoSplitWarehouseForProduct(List<SaleOrderDetail> lstSod, boolean isCreateSaleOrderLot, boolean isDividualWarehouse, LogInfoVO loginfo) throws BusinessException {
		List<SaleOrderLot> lstSol = new ArrayList<SaleOrderLot>();
		try {
			List<Long> lstProductId = new ArrayList<Long>();
			for (SaleOrderDetail sod : lstSod) {
				if (sod.getProduct() != null) {
					lstProductId.add(sod.getProduct().getId());
				}
			}
			List<StockTotal> lstStockTotal = new ArrayList<StockTotal>();
			//lay danh sach stocktotal cua cac san pham trong kho npp
			StockTotalVOFilter filter = new StockTotalVOFilter();
			filter.setShopId(lstSod.get(0).getShop().getId());
			filter.setOwnerType(StockObjectType.SHOP);
			filter.setStatus(ActiveType.RUNNING);
			filter.setLstProductId(lstProductId);
			lstStockTotal = stockTotalDAO.getListStockTotalByShopAndProduct(filter);
			////==============
			String logContent = loginfo.getContent();
			Warehouse wh = null;
			for (SaleOrderDetail sod : lstSod) {
				if (sod.getProduct() != null && sod.getQuantity() != 0) {
					Long productId = sod.getProduct().getId();
					Integer quantity = sod.getQuantity();
					Boolean isProductHasStockTotal = false;
					int ij = 0;
					wh = null;
					if (!isDividualWarehouse) {
						if (IsFreeItemInSaleOderDetail.SALE.getValue().equals(sod.getIsFreeItem())) {
							// order by st.product_id, w.warehouse_type, w.seq
							Collections.sort(lstStockTotal, new Comparator<StockTotal>() {
								@Override
								public int compare(StockTotal o1, StockTotal o2) {
									if (o1.getProduct() != null && o2.getProduct() != null) {
										int compareProduct = o1.getProduct().getId().compareTo(o2.getProduct().getId());
										if (compareProduct != 0) {
											return compareProduct;
										}
									}
									if (o1.getWarehouse() != null && o2.getWarehouse() != null) {
										if (o1.getWarehouse().getWarehouseType() != null && o2.getWarehouse().getWarehouseType() != null) {
											int compareWarehouseType = (o1.getWarehouse().getWarehouseType().getValue().compareTo(o2.getWarehouse().getWarehouseType().getValue()));
											if (compareWarehouseType != 0) {
												return compareWarehouseType;
											}
										}
										if (o1.getWarehouse().getSeq() != null && o2.getWarehouse().getSeq() != null) {
											return o1.getWarehouse().getSeq().compareTo(o2.getWarehouse().getSeq());
										}
									}
									return 0;
								}
							});
						} else if (IsFreeItemInSaleOderDetail.PROMOTION.getValue().equals(sod.getIsFreeItem())) {
							// order by st.product_id, w.warehouse_type desc, w.seq 
							Collections.sort(lstStockTotal, new Comparator<StockTotal>() {
								@Override
								public int compare(StockTotal o1, StockTotal o2) {
									if (o1.getProduct() != null && o2.getProduct() != null) {
										int compareProduct = o1.getProduct().getId().compareTo(o2.getProduct().getId());
										if (compareProduct != 0) {
											return compareProduct;
										}
									}
									if (o1.getWarehouse() != null && o2.getWarehouse() != null) {
										if (o1.getWarehouse().getWarehouseType() != null && o2.getWarehouse().getWarehouseType() != null) {
											int compareWarehouseType = (o2.getWarehouse().getWarehouseType().getValue().compareTo(o1.getWarehouse().getWarehouseType().getValue()));
											if (compareWarehouseType != 0) {
												return compareWarehouseType;
											}
										}
										if (o1.getWarehouse().getSeq() != null && o2.getWarehouse().getSeq() != null) {
											return o1.getWarehouse().getSeq().compareTo(o2.getWarehouse().getSeq());
										}
									}
									return 0;
								}
							});
						}
					}
					for (StockTotal st : lstStockTotal) {
						//tach kho theo loai kho ban or kho km
						if (st.getProduct() != null
								&& productId.equals(st.getProduct().getId())
								&& st.getWarehouse() != null
								&& (!isDividualWarehouse || ((WarehouseType.SALES.equals(st.getWarehouse().getWarehouseType()) && IsFreeItemInSaleOderDetail.SALE.getValue().equals(sod.getIsFreeItem())) || (WarehouseType.PROMOTION.equals(st
										.getWarehouse().getWarehouseType()) && IsFreeItemInSaleOderDetail.PROMOTION.getValue().equals(sod.getIsFreeItem()))))) {
							if (ij == 0) {
								wh = st.getWarehouse();
							}
							ij++;
							if (st.getAvailableQuantity() > 0) {
								isProductHasStockTotal = true;
								Integer quantityLot = quantity;
								BigDecimal discountAmount = sod.getDiscountAmount();
								Float discountPercent = sod.getDiscountPercent();

								Integer availableQuantityBeforeChange = st.getAvailableQuantity();
								Integer availableQuantityChange = 0;
								if (st.getAvailableQuantity() < quantity) {//kho khong du so luong cho sod
									quantityLot = st.getAvailableQuantity();
									quantity = quantity - quantityLot;
									availableQuantityChange = quantityLot;
									st.setAvailableQuantity(0);
								} else {//kho du v
									availableQuantityChange = quantity;
									st.setAvailableQuantity(st.getAvailableQuantity() - availableQuantityChange);
									quantity = 0;
								}

								/** Ghi log */
								try {
									loginfo.getContenFormat(st.getProduct().getId(), st.getWarehouse().getId(), availableQuantityChange, availableQuantityBeforeChange, st.getAvailableQuantity(), st.getQuantity(), st.getQuantity());
								} catch (Exception e) {
								}
								/** End ghi log */

								st.setUpdateDate(commonDAO.getSysDate());
								st.setUpdateUser(loginfo.getStaffCode());
								Float percentQuantity = (float) quantityLot / sod.getQuantity();
								SaleOrderLot sol = new SaleOrderLot();
								sol.setWarehouse(st.getWarehouse());
								sol.setStockTotal(st);
								sol.setSaleOrder(sod.getSaleOrder());
								sol.setSaleOrderDetail(sod);
								sol.setProduct(sod.getProduct());
								sol.setShop(sod.getShop());
								sol.setStaff(sod.getStaff());
								sol.setQuantity(quantityLot);
								if (st.getProduct().getConvfact() == null || st.getProduct().getConvfact() == 0) {
									sol.setQuantityPackage(0);
									sol.setQuantityRetail(quantityLot);
								} else {
									sol.setQuantityPackage(quantityLot / st.getProduct().getConvfact());
									sol.setQuantityRetail(quantityLot % st.getProduct().getConvfact());
								}
								sol.setDiscountAmount(discountAmount.multiply(BigDecimal.valueOf(percentQuantity)));
								sol.setDiscountPercent(discountPercent);
								sol.setCreateDate(commonDAO.getSysDate());
								sol.setCreateUser(loginfo.getStaffCode());
								sol.setOrderDate(sod.getOrderDate());
								sol.setPriceValue(sod.getPriceValue());
								sol.setPackagePrice(sod.getPackagePrice());
								sol.setPrice(sod.getPrice());

								lstSol.add(sol);
								if (quantity == 0) {//het quantity de tach
									break;
								}
							}
						}
					}
					/*
					 * neu nhu sau tach kho cho san pham, ma van con du, tuc la
					 * het kho & van con vuot qua ton kho => cong het vao kho da
					 * tach cuoi cung
					 */
					if (quantity > 0 && lstSol.size() != 0 && isProductHasStockTotal) {
						SaleOrderLot saleOrderLot = lstSol.get(lstSol.size() - 1);
						int qty = saleOrderLot.getQuantity() + quantity;
						saleOrderLot.setQuantity(qty);
						if (sod.getProduct().getConvfact() == null || sod.getProduct().getConvfact() == 0) {
							saleOrderLot.setQuantityPackage(0);
							saleOrderLot.setQuantityRetail(qty);
						} else {
							saleOrderLot.setQuantityPackage(qty / sod.getProduct().getConvfact());
							saleOrderLot.setQuantityRetail(qty % sod.getProduct().getConvfact());
						}
						/*
						 * log
						 */
						try {
							loginfo.getContenFormat(saleOrderLot.getProduct().getId(), saleOrderLot.getStockTotal().getWarehouse().getId(), quantity, null, null, null, null);
						} catch (Exception e) {
						}
					}
					/*
					 * neu sp khong co kho thi van tao moi mot dong sale order
					 * lot voi warehouse la null
					 */
					if (!isProductHasStockTotal) {
						SaleOrderLot sol = new SaleOrderLot();
						sol.setWarehouse(null);
						sol.setSaleOrder(sod.getSaleOrder());
						sol.setSaleOrderDetail(sod);
						sol.setProduct(sod.getProduct());
						sol.setShop(sod.getShop());
						sol.setStaff(sod.getStaff());
						sol.setQuantity(sod.getQuantity());
						if (sod.getProduct().getConvfact() == null || sod.getProduct().getConvfact() == 0) {
							sol.setQuantityPackage(0);
							sol.setQuantityRetail(sod.getQuantity());
						} else {
							sol.setQuantityPackage(sod.getQuantity() / sod.getProduct().getConvfact());
							sol.setQuantityRetail(sod.getQuantity() % sod.getProduct().getConvfact());
						}
						sol.setDiscountAmount(sod.getDiscountAmount());
						sol.setDiscountPercent(sod.getDiscountPercent());
						sol.setCreateDate(commonDAO.getSysDate());
						sol.setCreateUser(loginfo.getStaffCode());
						sol.setOrderDate(sod.getOrderDate());
						sol.setPriceValue(sod.getPriceValue());
						sol.setPackagePrice(sod.getPackagePrice());
						sol.setWarehouse(wh);

						lstSol.add(sol);
					}
				}
			}
			if (Boolean.TRUE.equals(isCreateSaleOrderLot) && lstSol != null && lstSol.size() > 0) {
				saleOrderLotDAO.createSaleOrderLot(lstSol);
				stockTotalDAO.updateListStockTotal(lstStockTotal);
			} else {
				loginfo.setContent(logContent);
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		return lstSol;
	}

	@Override
	public List<SaleOrderStockDetailVO> getListSaleOrderStockDetail(long saleOrderId, String orderType) throws BusinessException {
		try {
			List<SaleOrderStockDetailVO> lst = saleOrderDAO.getListSaleOrderStockDetail(saleOrderId, orderType, false);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<SaleOrderStockDetailVO> getListSaleOrderStockDetailUpdate(long saleOrderId, String orderType) throws BusinessException {
		try {
			List<SaleOrderStockDetailVO> lst = saleOrderDAO.getListSaleOrderStockDetailUpdate(saleOrderId, orderType);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/***
	 * @author vuongmq
	 * @since 29 August, 2014
	 * @description update don hang, cap nhat phai thu va kho
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateSaleOrderStock(List<SaleOrderStockDetailVO> listDetail, SaleOrderStockVOTemp orderVO, String userName, LogInfoVO logInfoVO) throws BusinessException {
		try {
			Date sysdate = commonDAO.getSysDate();
			return updateStockCommon(listDetail, orderVO, userName, sysdate, logInfoVO);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	/**
	 * Cap nhat kho, dung chung cho tat ca cac don
	 * @author trietptm
	 * @param listDetail
	 * @param orderVO
	 * @param userName
	 * @param logInfoVO
	 * @return
	 * @throws Exception
	 * @since Oct 22, 2015
	 */
	private boolean updateStockCommon(List<SaleOrderStockDetailVO> listDetail, SaleOrderStockVOTemp orderVO, String userName, Date sysdate, LogInfoVO logInfoVO) throws Exception {
		if (orderVO == null) {
			throw new IllegalArgumentException(" invalid saleOrder");
		}
		String orderTemp = orderVO.getOrderType();
//		Date lockDate = shopLockDAO.getApplicationDate(orderVO.getShopId());
		if (orderTemp.equals(OrderType.IN.getValue()) || orderTemp.equals(OrderType.DP.getValue()) || orderTemp.equals(OrderType.DCG.getValue())) {
			// xu ly don IN , DP va DCG
			for (int i = 0, sz = listDetail.size(); i < sz; i++) {
				StockTotalVOFilter filter = null;
				SaleOrderStockDetailVO detailVO = listDetail.get(i);
				if (orderTemp.equals(OrderType.IN.getValue())) {
					// don IN co them warehouseId
					filter = new StockTotalVOFilter();
					//filter.setStockTotalId(detailVO.getStockId());
					filter.setProductId(detailVO.getProductId());
					filter.setOwnerId(detailVO.getShopId());
					filter.setOwnerType(StockObjectType.SHOP);
					filter.setWarehouseId(detailVO.getWarehouseId());
				} else if (orderTemp.equals(OrderType.DCG.getValue())) {
					filter = new StockTotalVOFilter();
					filter.setProductId(detailVO.getProductId());
					filter.setWarehouseId(detailVO.getFromOwnerId());
					filter.setOwnerType(StockObjectType.SHOP);
				} else if (orderTemp.equals(OrderType.DP.getValue())) {
					filter = new StockTotalVOFilter();
					filter.setProductId(detailVO.getProductId());
					filter.setWarehouseId(detailVO.getFromOwnerId());
					filter.setOwnerType(StockObjectType.SHOP);
				}
				// xu ly don IN , DP va DCG de thuc hien
				List<StockTotal> listStockTotal = stockTotalDAO.getListStockTotalNewQuantity(null, filter);
				if (listStockTotal != null && listStockTotal.size() > 0) {
					StockTotal stockNew = listStockTotal.get(0);
					if (stockNew.getQuantity() == null) {
						stockNew.setQuantity(0);
					}
					Integer oldQuantity = stockNew.getQuantity();
					Integer oldAvailableQuantity = stockNew.getAvailableQuantity() != null ? stockNew.getAvailableQuantity() : 0;
					if (stockNew.getQuantity() >= detailVO.getQuatity()) {													
						Integer quantityNew = stockNew.getQuantity() - detailVO.getQuatity();
						
						LogInfoVO newLogInfoVO = logInfoVO.clone();
						try {
							newLogInfoVO.setIdObject((orderVO.getId() != null ? orderVO.getId().toString() : "N/A") + "_" + orderVO.getOrderNumber() + "_" + orderTemp);
						} catch (Exception e) {
							// pass through
						}
						stockNew.setQuantity(quantityNew);
						
						/**
						 * o Giam ton kho dap ung cua mat hang va lo (neu co)
						 * . > lacnv1 - 11.03.2015 - giam o buoc tao
						 */
						/*if (orderTemp.equals(OrderType.DCG.getValue())) {
							if (stockNew.getAvailableQuantity() == null) {
								stockNew.setAvailableQuantity(0);
							}
							if (stockNew.getAvailableQuantity() >= detailVO.getQuatity()) {
								Integer availableQuantityNew = stockNew.getAvailableQuantity() - detailVO.getQuatity();
								stockNew.setAvailableQuantity(availableQuantityNew);
							} else if (detailVO.getQuatity() > 0) {
								throw new BusinessException("KHONG_DU_SOLUONG_KHO_NPP-" + stockNew.getProduct().getProductCode());
							}
						}*/
						
						try {
							newLogInfoVO.getContenFormat(
									stockNew.getProduct().getId(),
									stockNew.getWarehouse().getId(),
									detailVO.getQuatity(),
									oldAvailableQuantity, stockNew.getAvailableQuantity(),
									oldQuantity, stockNew.getQuantity());
						} catch (Exception e) {
							// pass through
						}
						
						stockNew.setUpdateDate(sysdate);
						stockNew.setUpdateUser(userName);
						stockTotalDAO.updateStockTotal(stockNew);
						try {
							LogUtility.logInfoWs(newLogInfoVO);
						} catch (Exception e) {}
					} else if (detailVO.getQuatity() > 0) {
						//Rollback
						//return false;
						throw new BusinessException("KHONG_DU_SOLUONG_KHO_NPP-" + stockNew.getProduct().getProductCode());
						//String a = stockNew.getProduct().getProductCode();
					}
				} else if (detailVO.getQuatity() > 0) {
					Product product = productDAO.getProductById(detailVO.getProductId());
					throw new BusinessException("KHONG_DU_SOLUONG_KHO_NPP-" + product.getProductCode());
				}
				/**
				 * neu don DP thi them o tang kho dap ung, ton kho thuc te
				 * (APPROVED_QUANTITY)
				 * kho vansale (ca 2 bang: STOCK_TOTAL, PRODUCT_LOT).
				 */
				if (orderTemp.equals(OrderType.DP.getValue())) {
					filter = new StockTotalVOFilter(); // tao filter moi lay kho NV
					filter.setProductId(detailVO.getProductId());
					filter.setOwnerId(detailVO.getToOwnerId());
					filter.setOwnerType(StockObjectType.STAFF);
					List<StockTotal> listStockTotalNVVan = stockTotalDAO.getListStockTotalNewQuantity(null, filter);
					
					LogInfoVO newLogInfoVO = logInfoVO.clone();
					try {
						newLogInfoVO.setIdObject((orderVO.getId() != null ? orderVO.getId().toString() : "N/A") + "_" + orderVO.getOrderNumber() + "_" + orderTemp);
					} catch (Exception e) {
						// pass through
					}
					
					if (listStockTotalNVVan != null && listStockTotalNVVan.size() > 0) {
						StockTotal stockNew = listStockTotalNVVan.get(0);
						
						if (stockNew.getQuantity() == null) {
							stockNew.setQuantity(0);
						}
						if (stockNew.getAvailableQuantity() == null) {
							stockNew.setAvailableQuantity(0);
						}
						if (stockNew.getApprovedQuantity() == null) {
							stockNew.setApprovedQuantity(0);
						}
						Integer quantityNew = stockNew.getQuantity() + detailVO.getQuatity();
						Integer availableQuantityNew = stockNew.getAvailableQuantity() + detailVO.getQuatity();
						Integer approvedQuantityNew = stockNew.getApprovedQuantity() + detailVO.getQuatity();
						
						try {
							newLogInfoVO.getContenFormat(
									stockNew.getProduct().getId(),
									stockNew.getWarehouse().getId(),
									detailVO.getQuatity(),
									stockNew.getAvailableQuantity(), availableQuantityNew,
									stockNew.getQuantity(), quantityNew);
						} catch (Exception e) {
							// pass through
						}
						
						stockNew.setQuantity(quantityNew);
						stockNew.setAvailableQuantity(availableQuantityNew);
						stockNew.setApprovedQuantity(approvedQuantityNew);
						stockNew.setUpdateDate(sysdate);
						stockNew.setUpdateUser(userName);
						stockTotalDAO.updateStockTotal(stockNew);
					} else {
						/**
						 * Khong co kho thi tao kho insert vao kho DP
						 */
						StockTotal stockNew = new StockTotal();
						stockNew.setWarehouse(null);
						stockNew.setObjectId(detailVO.getToOwnerId());
						stockNew.setObjectType(StockObjectType.STAFF);
						Product product = productDAO.getProductById(detailVO.getProductId());
						stockNew.setProduct(product);
						stockNew.setDescr(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "update.order.stock.staff.vansale"));
						stockNew.setQuantity(detailVO.getQuatity());
						stockNew.setAvailableQuantity(detailVO.getQuatity());
						stockNew.setApprovedQuantity(detailVO.getQuatity());
						stockNew.setCreateUser(userName);
						stockNew.setCreateDate(sysdate);
						stockNew.setStatus(ActiveType.RUNNING);
						// lay ware house
						StringBuilder sql = new StringBuilder();
						List<Object> params = new ArrayList<Object>();
						sql.append("select * from warehouse where warehouse_id = ? ");
						params.add(detailVO.getFromOwnerId());
						Warehouse wr = repo.getEntityBySQL(Warehouse.class, sql.toString(), params);
						if (wr != null) {
							stockNew.setShop(wr.getShop());
						} else {
							throw new BusinessException("KHONG_CO_WAREHOUSE");
						}
						
						try {
							newLogInfoVO.getContenFormat(
									stockNew.getProduct().getId(),
									stockNew.getWarehouse().getId(),
									detailVO.getQuatity(),
									0, stockNew.getAvailableQuantity(),
									0, stockNew.getQuantity());
						} catch (Exception e) {
							// pass through
						}
						
						stockTotalDAO.createStockTotal(stockNew);
					}

					StockLock stockLock = stockLockDAO.getStockLock(detailVO.getShopId(), detailVO.getToOwnerId());
					if (stockLock == null) {
						Shop shop = shopDAO.getShopById(detailVO.getShopId());
						Staff staff = staffDAO.getStaffById(detailVO.getToOwnerId());
						stockLock = new StockLock();
						stockLock.setShop(shop);
						stockLock.setStaff(staff);
						stockLock.setVanLock(ActiveType.RUNNING);
						stockLock.setUpdateUser(userName);	
						stockLock.setUpdateDate(sysdate);
						stockLock.setCreateUser(userName);
						stockLock.setCreateDate(sysdate);
						stockTransDAO.createStockTransLock(stockLock);
					} else if (ActiveType.STOPPED.equals(stockLock.getVanLock())) {
						stockLock.setVanLock(ActiveType.RUNNING);
						stockLock.setUpdateUser(userName);	
						stockLock.setUpdateDate(sysdate);
						stockTotalDAO.updateStockLock(stockLock);
					}
					
					try {
						LogUtility.logInfoWs(newLogInfoVO);
					} catch (Exception e) {}
				}
			}
		} else if (orderTemp.equals(OrderType.CM.getValue()) || orderTemp.equals(OrderType.GO.getValue()) || (orderTemp.equals(OrderType.CO.getValue()) && orderVO.getOrderSource() == SaleOrderSource.WEB.getValue())
				|| orderTemp.equals(OrderType.DCT.getValue())) {
			boolean fromOrderUpdatedStock = true;
			if (orderTemp.equals(OrderType.CM.getValue())) {
				SaleOrder CMOrder = saleOrderDAO.getSaleOrderById(orderVO.getId());
				if (CMOrder != null && CMOrder.getFromSaleOrder() != null) {
					SaleOrder INOrder = saleOrderDAO.getSaleOrderById(CMOrder.getFromSaleOrder().getId());
					if (SaleOrderStep.PRINT_CONFIRMED.equals(INOrder.getApprovedStep())) {
						INOrder.setApprovedStep(SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT);
//						INOrder.setStockDate(lockDate);
						saleOrderDAO.updateSaleOrder(INOrder);
						fromOrderUpdatedStock = false; // don CM ma don goc chua qua buoc cap nhat phai thu va kho thi khong cap nhat ton kho thuc te
					}
				}
			}
			// xu ly don  CM, GO, CO[web], DCT
			for (int i = 0, sz = listDetail.size(); i < sz; i++) {
				StockTotalVOFilter filter = null;
				SaleOrderStockDetailVO detailVO = listDetail.get(i);
				if (orderTemp.equals(OrderType.CM.getValue()) || (orderTemp.equals(OrderType.CO.getValue()) && orderVO.getOrderSource() == SaleOrderSource.WEB.getValue())) {
					// don CM co them warehouseId
					filter = new StockTotalVOFilter();
					//filter.setStockTotalId(detailVO.getStockId());
					filter.setProductId(detailVO.getProductId());
					filter.setOwnerId(detailVO.getShopId());
					filter.setOwnerType(StockObjectType.SHOP);
					filter.setWarehouseId(detailVO.getWarehouseId());
				} else if (orderTemp.equals(OrderType.DCT.getValue()) || orderTemp.equals(OrderType.GO.getValue())) {
					filter = new StockTotalVOFilter();
					filter.setProductId(detailVO.getProductId());
					filter.setOwnerId(detailVO.getShopId());
					filter.setOwnerType(StockObjectType.SHOP);
					filter.setWarehouseId(detailVO.getToOwnerId());
				}
				// xu ly don  CM, GO, CO[web], DCT deu thuc hien
				List<StockTotal> listStockTotal = stockTotalDAO.getListStockTotalNewQuantity(null, filter);
				
				LogInfoVO newLogInfoVO = logInfoVO.clone();
				try {
					newLogInfoVO.setIdObject((orderVO.getId() != null ? orderVO.getId().toString() : "N/A") + "_" + orderVO.getOrderNumber() + "_" + orderTemp);
				} catch (Exception e) {
					// pass through
				}
				
				if (listStockTotal != null && listStockTotal.size() > 0) {
					/** Tang kho */
					StockTotal stockNew = listStockTotal.get(0);
					if (stockNew.getQuantity() == null) {
						stockNew.setQuantity(0);
					}
					if (stockNew.getAvailableQuantity() == null) {
						stockNew.setAvailableQuantity(0);
					}
					if (stockNew.getApprovedQuantity() == null) {
						stockNew.setApprovedQuantity(0);
					}
					Integer quantityNew = stockNew.getQuantity();
					if (fromOrderUpdatedStock) {
						quantityNew = stockNew.getQuantity() + detailVO.getQuatity();
					}
					Integer availableQuantityNew = stockNew.getAvailableQuantity() + detailVO.getQuatity();
					
					try {
						newLogInfoVO.getContenFormat(
								stockNew.getProduct().getId(),
								stockNew.getWarehouse().getId(),
								detailVO.getQuatity(),
								stockNew.getAvailableQuantity(), availableQuantityNew,
								stockNew.getQuantity(), quantityNew);
					} catch (Exception e) {
						// pass through
					}
					
					if (fromOrderUpdatedStock) {
						stockNew.setQuantity(quantityNew);
					}
					stockNew.setAvailableQuantity(availableQuantityNew);
					stockNew.setUpdateDate(sysdate);
					stockNew.setUpdateUser(userName);
					stockTotalDAO.updateStockTotal(stockNew);
				} else {
					if (orderTemp.equals(OrderType.DCT.getValue())) {
						/** Khong co kho thi tao kho insert vao kho DCT */
						StockTotal stockNew = new StockTotal();
						// lay ware house
						StringBuilder sql = new StringBuilder();
						List<Object> params = new ArrayList<Object>();
						sql.append("select * from warehouse where warehouse_id = ? ");
						params.add(detailVO.getToOwnerId());
						if (detailVO.getShopId() != null) {
							sql.append(" and shop_id = ? ");
							params.add(detailVO.getShopId());
						}
						Warehouse wr = repo.getEntityBySQL(Warehouse.class, sql.toString(), params);
						if (wr != null) {
							stockNew.setWarehouse(wr);
							stockNew.setShop(wr.getShop());
						} else {
							throw new BusinessException("KHONG_CO_WAREHOUSE");
						}
						// end // lay ware house
						//stockNew.setObjectId(detailVO.getShopId());
						if (wr.getShop() != null) {
							stockNew.setObjectId(wr.getShop().getId()); // Object_id = id shop cua kho dich.
						}
						stockNew.setObjectType(StockObjectType.SHOP);
						Product product = productDAO.getProductById(detailVO.getProductId());
						stockNew.setProduct(product);
						stockNew.setDescr(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "update.order.stock.increase.npp"));
						stockNew.setQuantity(detailVO.getQuatity());
						stockNew.setAvailableQuantity(detailVO.getQuatity());
						stockNew.setApprovedQuantity(null);
						stockNew.setCreateUser(userName);
						stockNew.setCreateDate(sysdate);
						stockNew.setStatus(ActiveType.RUNNING);
						
						try {
							newLogInfoVO.getContenFormat(
									stockNew.getProduct().getId(),
									stockNew.getWarehouse().getId(),
									detailVO.getQuatity(),
									0, stockNew.getAvailableQuantity(),
									0, stockNew.getQuantity());
						} catch (Exception e) {
							// pass through
						}
						
						stockTotalDAO.createStockTotal(stockNew);

					} else {
						/** don CM, GO, CO[web], */
						Product product = productDAO.getProductById(detailVO.getProductId());
						throw new BusinessException("KHONG_DU_SOLUONG_KHO_NPP-" + product.getProductCode());
					}
				}
				
				try {
					LogUtility.logInfoWs(newLogInfoVO);
				} catch (Exception e) {}
				
				/** Giam Kho GO (chi giam ApprovedQuantity) */
				if (orderTemp.equals(OrderType.GO.getValue())) {
					// xu ly don GO, giam kho NVvansale
					filter = new StockTotalVOFilter();
					filter.setProductId(detailVO.getProductId());
					filter.setOwnerId(detailVO.getFromOwnerId());
					filter.setOwnerType(StockObjectType.STAFF);
					List<StockTotal> listStockTotalNVVan = stockTotalDAO.getListStockTotalNewQuantity(null, filter);
					if (listStockTotalNVVan != null && listStockTotalNVVan.size() > 0) {
						newLogInfoVO = logInfoVO.clone();
						try {
							newLogInfoVO.setIdObject((orderVO.getId() != null ? orderVO.getId().toString() : "N/A") + "_" + orderVO.getOrderNumber() + "_" + orderTemp);
						} catch (Exception e) {
							// pass through
						}
						
						StockTotal stockNew = listStockTotalNVVan.get(0);
						if (stockNew.getApprovedQuantity() == null) {
							stockNew.setApprovedQuantity(0);
						}
						stockNew.setQuantity(0); // lacnv1 - clear ton kho goc nhin NV
						stockNew.setAvailableQuantity(0);
						if (stockNew.getApprovedQuantity() >= detailVO.getQuatity()) {
							Integer approvedQuantityNew = stockNew.getApprovedQuantity() - detailVO.getQuatity();
							try {
								newLogInfoVO.getContenFormat(
										stockNew.getProduct().getId(),
										stockNew.getWarehouse().getId(),
										0,
										0, stockNew.getAvailableQuantity(),
										0, stockNew.getQuantity());
							} catch (Exception e) {
								// pass through
							}
							
							stockNew.setApprovedQuantity(approvedQuantityNew);
							
							stockNew.setUpdateDate(sysdate);
							stockNew.setUpdateUser(userName);
							stockTotalDAO.updateStockTotal(stockNew);
							
							try {
								LogUtility.logInfoWs(newLogInfoVO);
							} catch (Exception e) {}
						} else {
							//Rollback
							//return false;
							throw new BusinessException("KHONG_DU_SOLUONG_KHO_NV_VANSALE-" + stockNew.getProduct().getProductCode());
						}
					} else {
						Product product = productDAO.getProductById(detailVO.getProductId());
						throw new BusinessException("KHONG_DU_SOLUONG_KHO_NV_VANSALE-" + product.getProductCode());
					}
				}
			}

		} else if (orderTemp.equals(OrderType.SO.getValue())) {
			//xu ly don SO
			/** Giam kho NV availableQuantity */
			for (int i = 0, sz = listDetail.size(); i < sz; i++) {
				StockTotalVOFilter filter = null;
				SaleOrderStockDetailVO detailVO = listDetail.get(i);
				// don SO warehouseId la null
				filter = new StockTotalVOFilter();
				//filter.setStockTotalId(detailVO.getStockId());
				filter.setProductId(detailVO.getProductId());
				filter.setOwnerId(detailVO.getStaffId());
				filter.setOwnerType(StockObjectType.STAFF);
				List<StockTotal> listStockTotal = stockTotalDAO.getListStockTotalNewQuantity(null, filter);
				if (listStockTotal != null && listStockTotal.size() > 0) {
					LogInfoVO newLogInfoVO = logInfoVO.clone();
					try {
						newLogInfoVO.setIdObject((orderVO.getId() != null ? orderVO.getId().toString() : "N/A") + "_" + orderVO.getOrderNumber() + "_" + orderTemp);
					} catch (Exception e) {
						// pass through
					}
					
					StockTotal stockNew = listStockTotal.get(0);
					if (stockNew.getApprovedQuantity() == null) {
						stockNew.setApprovedQuantity(0);
					}
					if (stockNew.getApprovedQuantity() >= detailVO.getQuatity()) {
						Integer approvedQuantityNew = stockNew.getApprovedQuantity() - detailVO.getQuatity();
						try {
							newLogInfoVO.getContenFormat(
									stockNew.getProduct().getId(),
									stockNew.getWarehouse().getId(),
									0,
									stockNew.getAvailableQuantity(), stockNew.getAvailableQuantity(),
									stockNew.getQuantity(), stockNew.getQuantity());
						} catch (Exception e) {
							// pass through
						}
						stockNew.setApprovedQuantity(approvedQuantityNew);
						stockNew.setUpdateDate(sysdate);
						stockNew.setUpdateUser(userName);
						stockTotalDAO.updateStockTotal(stockNew);
						try {
							LogUtility.logInfoWs(newLogInfoVO);
						} catch (Exception e) {}
					} else if (detailVO.getQuatity() > 0) {
						//Rollback
						//return false;
						throw new BusinessException("KHONG_DU_SOLUONG_KHO_NV_VANSALE-" + stockNew.getProduct().getProductCode());
					}
				} else if (detailVO.getQuatity() > 0) {
					Product product = productDAO.getProductById(detailVO.getProductId());
					throw new BusinessException("KHONG_DU_SOLUONG_KHO_NV_VANSALE-" + product.getProductCode());
				}
			}

		} else if (orderTemp.equals(OrderType.CO.getValue()) && orderVO.getOrderSource() == SaleOrderSource.TABLET.getValue()) {
			//xu ly don CO tren tablet
			/** Tang kho NV availableQuantity */
			for (int i = 0, sz = listDetail.size(); i < sz; i++) {
				StockTotalVOFilter filter = null;
				SaleOrderStockDetailVO detailVO = listDetail.get(i);
				// don SO warehouseId la null
				filter = new StockTotalVOFilter();
				//filter.setStockTotalId(detailVO.getStockId());
				filter.setProductId(detailVO.getProductId());
				filter.setOwnerId(detailVO.getStaffId());
				filter.setOwnerType(StockObjectType.STAFF);
				//filter.setWarehouseId(detailVO.getWarehouseId());
				List<StockTotal> listStockTotal = stockTotalDAO.getListStockTotalNewQuantity(null, filter);
				if (listStockTotal != null && listStockTotal.size() > 0) {
					LogInfoVO newLogInfoVO = logInfoVO.clone();
					try {
						newLogInfoVO.setIdObject((orderVO.getId() != null ? orderVO.getId().toString() : "N/A") + "_" + orderVO.getOrderNumber() + "_" + orderTemp);
					} catch (Exception e) {
						// pass through
					}
					
					StockTotal stockNew = listStockTotal.get(0);
					if (stockNew.getApprovedQuantity() == null) {
						stockNew.setApprovedQuantity(0);
					}
					Integer approvedQuantityNew = stockNew.getApprovedQuantity() + detailVO.getQuatity();
					try {
						newLogInfoVO.getContenFormat(
								stockNew.getProduct().getId(),
								stockNew.getWarehouse().getId(),
								0,
								stockNew.getApprovedQuantity(), stockNew.getApprovedQuantity(),
								stockNew.getQuantity(), stockNew.getQuantity());
					} catch (Exception e) {
						// pass through
					}
					stockNew.setApprovedQuantity(approvedQuantityNew);
					stockNew.setUpdateDate(sysdate);
					stockNew.setUpdateUser(userName);
					stockTotalDAO.updateStockTotal(stockNew);
					try {
						LogUtility.logInfoWs(newLogInfoVO);
					} catch (Exception e) {}
				} else {
					Product product = productDAO.getProductById(detailVO.getProductId());
					throw new BusinessException("KHONG_DU_SOLUONG_KHO_NV_VANSALE-" + product.getProductCode());
				}
			}
		} else if (orderTemp.equals(OrderType.DC.getValue())) {
			//xu ly don DC
			for (int i = 0, sz = listDetail.size(); i < sz; i++) {
				LogInfoVO newLogInfoVO = logInfoVO.clone();
				try {
					newLogInfoVO.setIdObject((orderVO.getId() != null ? orderVO.getId().toString() : "N/A") + "_" + orderVO.getOrderNumber() + "_" + orderTemp);
				} catch (Exception e) {
					// pass through
				}
				
				StockTotalVOFilter filter = null;
				SaleOrderStockDetailVO detailVO = listDetail.get(i);
				filter = new StockTotalVOFilter();
				filter.setProductId(detailVO.getProductId());
				filter.setWarehouseId(detailVO.getFromOwnerId());
				filter.setOwnerType(StockObjectType.SHOP);
				/** Giam kho nguon(FromOwner) cua don DC; chi giam Quantity; availableQuantity da giam luc tao */
				List<StockTotal> listStockTotal = stockTotalDAO.getListStockTotalNewQuantity(null, filter);
				if (listStockTotal != null && listStockTotal.size() > 0) {
					StockTotal stockNew = listStockTotal.get(0);
					if (stockNew.getQuantity() == null) {
						stockNew.setQuantity(0);
					}
					if (stockNew.getAvailableQuantity() == null) {
						stockNew.setAvailableQuantity(0);
					}
					//if (stockNew.getQuantity() >= detailVO.getQuatity() && stockNew.getAvailableQuantity() >= detailVO.getQuatity()) {
					if (stockNew.getQuantity() >= detailVO.getQuatity()) {
						Integer quantityNew = stockNew.getQuantity() - detailVO.getQuatity();
						Integer availableQuantityNew = stockNew.getAvailableQuantity();// - detailVO.getQuatity();
						try {
							newLogInfoVO.getContenFormat(
									stockNew.getProduct().getId(),
									stockNew.getWarehouse().getId(),
									detailVO.getQuatity(),
									stockNew.getAvailableQuantity(), availableQuantityNew,
									stockNew.getQuantity(), quantityNew);
						} catch (Exception e) {
							// pass through
						}
						stockNew.setQuantity(quantityNew);
						//stockNew.setAvailableQuantity(availableQuantityNew); // lacnv1 - 11.03.2015 - giam o buoc tao
						stockNew.setUpdateDate(sysdate);
						stockNew.setUpdateUser(userName);
						stockTotalDAO.updateStockTotal(stockNew);
						try {
							LogUtility.logInfoWs(newLogInfoVO);
						} catch (Exception e) {}
					} else if (detailVO.getQuatity() > 0) {
						//Rollback
						//return false;
						throw new BusinessException("KHONG_DU_SOLUONG_KHO_NPP-" + stockNew.getProduct().getProductCode());
					}
				} else if (detailVO.getQuatity() > 0) {
					Product product = productDAO.getProductById(detailVO.getProductId());
					throw new BusinessException("KHONG_DU_SOLUONG_KHO_NPP-" + product.getProductCode());
				}
				/** Tang kho dich(ToOwner) cua don DC */
				filter = new StockTotalVOFilter();
				filter.setProductId(detailVO.getProductId());
				//filter.setOwnerId(detailVO.getToOwnerId());
				filter.setWarehouseId(detailVO.getToOwnerId());
				filter.setOwnerType(StockObjectType.SHOP);
				List<StockTotal> listStockTotalNPP = stockTotalDAO.getListStockTotalNewQuantity(null, filter);
				
				newLogInfoVO = logInfoVO.clone();
				try {
					newLogInfoVO.setIdObject((orderVO.getId() != null ? orderVO.getId().toString() : "N/A") + "_" + orderVO.getOrderNumber() + "_" + orderTemp);
				} catch (Exception e) {
					// pass through
				}
				
				if (listStockTotalNPP != null && listStockTotalNPP.size() > 0) {
					StockTotal stockNew = listStockTotalNPP.get(0);
					if (stockNew.getQuantity() == null) {
						stockNew.setQuantity(0);
					}
					if (stockNew.getAvailableQuantity() == null) {
						stockNew.setAvailableQuantity(0);
					}
					Integer quantityNew = stockNew.getQuantity() + detailVO.getQuatity();
					Integer availableQuantityNew = stockNew.getAvailableQuantity() + detailVO.getQuatity();
					try {
						newLogInfoVO.getContenFormat(
								stockNew.getProduct().getId(),
								stockNew.getWarehouse().getId(),
								detailVO.getQuatity(),
								stockNew.getApprovedQuantity(), availableQuantityNew,
								stockNew.getQuantity(), quantityNew);
					} catch (Exception e) {
						// pass through
					}
					stockNew.setQuantity(quantityNew);
					stockNew.setAvailableQuantity(availableQuantityNew);
					stockNew.setUpdateDate(sysdate);
					stockNew.setUpdateUser(userName);
					stockTotalDAO.updateStockTotal(stockNew);
				} else {
					/** Khong co stock_total kho dich(ToOwner) Insert kho dich cua don DC */
					StockTotal stockNew = new StockTotal();
					// lay ware house
					StringBuilder sql = new StringBuilder();
					List<Object> params = new ArrayList<Object>();
					sql.append("select * from warehouse where warehouse_id = ? ");
					params.add(detailVO.getToOwnerId());
//					if (detailVO.getShopId() != null) {
//						sql.append(" and shop_id = ? ");
//						params.add(detailVO.getShopId());
//					}
					Warehouse wr = repo.getEntityBySQL(Warehouse.class, sql.toString(), params);
					if (wr != null) {
						stockNew.setWarehouse(wr);
						stockNew.setShop(wr.getShop());
					} else {
						throw new BusinessException("KHONG_CO_WAREHOUSE");
					}
					// end // lay ware house
					//stockNew.setObjectId(detailVO.getToOwnerId());
					//stockNew.setObjectId(detailVO.getShopId()); // Object_id = id shop.
					if (wr.getShop() != null) {
						stockNew.setObjectId(wr.getShop().getId()); // Object_id = id shop cua kho dich.
					}
					stockNew.setObjectType(StockObjectType.SHOP);
					Product product = productDAO.getProductById(detailVO.getProductId());
					stockNew.setProduct(product);
					stockNew.setDescr(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "update.order.stock.npp"));
					stockNew.setQuantity(detailVO.getQuatity());
					stockNew.setAvailableQuantity(detailVO.getQuatity());
					stockNew.setApprovedQuantity(null);
					stockNew.setCreateUser(userName);
					stockNew.setCreateDate(DateUtility.now());
					stockNew.setStatus(ActiveType.RUNNING);
					try {
						newLogInfoVO.getContenFormat(
								stockNew.getProduct().getId(),
								stockNew.getWarehouse().getId(),
								detailVO.getQuatity(),
								0, stockNew.getApprovedQuantity(),
								0, stockNew.getQuantity());
					} catch (Exception e) {
						// pass through
					}
					stockTotalDAO.createStockTotal(stockNew);
				}
				try {
					LogUtility.logInfoWs(newLogInfoVO);
				} catch (Exception e) {}
			}
		}
		/** Cap nhat trang thai cua SALE_ORDER/ STOCK_TRANS
		 *  cap nhat SALE_ORDER; APPROVED_STEP = 3  */
		if (orderTemp.equals(OrderType.IN.getValue()) || orderTemp.equals(OrderType.CM.getValue()) || orderTemp.equals(OrderType.SO.getValue()) || orderTemp.equals(OrderType.CO.getValue())) {
			SaleOrder saleOrderApproved = saleOrderDAO.getSaleOrderById(listDetail.get(0).getId());
			if (saleOrderApproved != null) {
				checkOrder(saleOrderApproved.getId(), CommonContanst.SALE_ORDER_STOCKUPDATING_STEP);
				saleOrderApproved.setApprovedStep(SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT);
				saleOrderApproved.setStockDate(sysdate);
				saleOrderDAO.updateSaleOrder(saleOrderApproved);
				
				/*
				 * luu lich su chuyen trang thai don hang
				 */
//				if (!saleOrderDAO.checkExistsProcessHistoryForOrder(saleOrderApproved.getId(), CommonContanst.SALE_ORDER_STOCKUPDATING_STEP)) {
//					ProcessHistory processHistory = new ProcessHistory();
//					processHistory.setType(ProcessHistoryType.SALE_ORDER);
//					processHistory.setObjectType(CommonContanst.SALE_ORDER_STOCKUPDATING_STEP); // cap nhat phai thu va kho
//					processHistory.setObjectId(saleOrderApproved.getId());
//					processHistory.setLogTime(sysdate);
//					commonDAO.createEntity(processHistory);
//				} else {
//					throw new BusinessException("DON_HANG_DA_XU_LY");
//				}
			} else {
				throw new BusinessException("KHONG_CO_SALE_ORDER");
			}
		} else if (orderTemp.equals(OrderType.GO.getValue()) || orderTemp.equals(OrderType.DP.getValue())) {
			/** cap nhat STOCK_TRANS; APPROVED_STEP = 3  */
			if (listDetail.get(0).getId() != null && listDetail.get(0).getId() > 0) {
				StockTrans stockTrans = commonDAO.getEntityById(StockTrans.class, listDetail.get(0).getId());
				if (!SaleOrderStatus.APPROVED.equals(stockTrans.getApproved()) || !SaleOrderStep.PRINT_CONFIRMED.equals(stockTrans.getApprovedStep())) {
					throw new BusinessException("DON_HANG_DA_XU_LY-" + stockTrans.getStockTransCode());
				}
				stockTrans.setApprovedStep(SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT);
				stockTrans.setStockDate(sysdate);
				stockTrans.setUpdateDate(sysdate);
				stockTrans.setUpdateUser(userName);
				commonDAO.updateEntity(stockTrans);
//				if (orderTemp.equals(OrderType.DC.getValue()) || orderTemp.equals(OrderType.DCT.getValue()) || orderTemp.equals(OrderType.DCG.getValue())) {
//					/*
//					 * luu lich su chuyen trang thai don hang
//						*/
//					if (!saleOrderDAO.checkExistsProcessHistoryForOrder(stockTrans.getId(), CommonContanst.SALE_ORDER_STOCKUPDATING_STEP)) {
//						ProcessHistory processHistory = new ProcessHistory();
//						processHistory.setType(ProcessHistoryType.STOCK_TRANS);
//						processHistory.setObjectType(CommonContanst.STOCK_TRANS_STOCKUPDATING_STEP); // cap nhat phai thu va kho
//						processHistory.setObjectId(stockTrans.getId());
//						processHistory.setLogTime(sysdate);
//						commonDAO.createEntity(processHistory);
//					} else {
//						throw new BusinessException("DON_HANG_DA_XU_LY");
//					}
//				}
			} else {
				throw new BusinessException("KHONG_CO_STOCK_TRANS");
			}
		} else if (orderTemp.equals(OrderType.DC.getValue()) || orderTemp.equals(OrderType.DCT.getValue()) || orderTemp.equals(OrderType.DCG.getValue())) {
			/** cap nhat STOCK_TRANS; APPROVED = 1;  APPROVED_STEP = 3  */
			if (listDetail.get(0).getId() != null && listDetail.get(0).getId() > 0) {
				StockTrans stockTrans = commonDAO.getEntityById(StockTrans.class, listDetail.get(0).getId());
				if (!SaleOrderStatus.NOT_YET_APPROVE.equals(stockTrans.getApproved()) || !SaleOrderStep.CONFIRMED.equals(stockTrans.getApprovedStep())) {
					throw new BusinessException("DON_HANG_DA_XU_LY-" + stockTrans.getStockTransCode());
				}
				stockTrans.setApproved(SaleOrderStatus.APPROVED);
				stockTrans.setApprovedStep(SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT);
				stockTrans.setStockDate(sysdate);
				stockTrans.setUpdateDate(sysdate);
				stockTrans.setUpdateUser(userName);
				commonDAO.updateEntity(stockTrans);
			} else {
				throw new BusinessException("KHONG_CO_STOCK_TRANS");
			}
		}
		return true;
	}
	
	/***
	 * @author trietptm
	 * @since Jul 24, 2015
	 * @description update don hang, duyet dieu chinh kho
	 */
	/*@Override
	@Transactional(rollbackFor = Exception.class)
	public boolean updateSaleOrderStockUpdate(List<SaleOrderStockDetailVO> listDetail, SaleOrderStockVOTemp orderVO, String userName,LogInfoVO logInfoVO) throws BusinessException {
		try {
			if (orderVO == null) {
				throw new IllegalArgumentException(" invalid saleOrder");
			}
			Date sysdate = commonDAO.getSysDate();
			String orderTemp = orderVO.getOrderType();
			Date lockDate = shopLockDAO.getApplicationDate(orderVO.getShopId());
			if (orderTemp.equals(OrderType.DC.getValue()) || orderTemp.equals(OrderType.DCT.getValue()) || orderTemp.equals(OrderType.DCG.getValue())) {
				if (listDetail.get(0).getId() != null && listDetail.get(0).getId() > 0) {
					StockTrans stockTrans = commonDAO.getEntityById(StockTrans.class, listDetail.get(0).getId());
					if(!SaleOrderStatus.NOT_YET_APPROVE.equals(stockTrans.getApproved()) || !SaleOrderStep.CONFIRMED.equals(stockTrans.getApprovedStep())){
						throw new BusinessException("DON_HANG_KHONG_DUNG_TRANG_THAI");
					}
//					stockTrans.setApproved(SaleOrderStatus.APPROVED);
//					stockTrans.setApprovedStep(SaleOrderStep.PRINT_CONFIRMED);
					stockTrans.setApproved(SaleOrderStatus.APPROVED);
					stockTrans.setApprovedStep(SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT);
					stockTrans.setStockDate(lockDate);
					stockTrans.setUpdateDate(sysdate);
					stockTrans.setUpdateUser(userName);
					commonDAO.updateEntity(stockTrans);
				} else {
					throw new BusinessException("KHONG_CO_STOCK_TRANS");
				}
			}
			return true;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

	}*/

	@Override
	public ObjectVO<SaleOrderVO> getListSaleOrderForPay(SoFilter filter) throws BusinessException {
		try {
			List<SaleOrderVO> lst = saleOrderDAO.getListSaleOrderForPay(filter);
			ObjectVO<SaleOrderVO> vo = new ObjectVO<SaleOrderVO>();
			vo.setkPaging(filter.getkPagingVO());
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional (rollbackFor = Exception.class)
	public void cancelPay(SoFilter filter) throws BusinessException {
		try {
			saleOrderDAO.cancelPay(filter);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleOrder> getListSaleOrderForPrint(SoFilter filter) throws BusinessException {
		try {
			List<SaleOrder> lst = saleOrderDAO.getListSaleOrderForPrint(filter);
			ObjectVO<SaleOrder> vo = new ObjectVO<SaleOrder>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleOrderVO> getListSaleOrderAIADVOByFilter(SaleOrderFilter<SaleOrderVO> filter) throws BusinessException {
		try {
			List<SaleOrderVO> lst = saleOrderDAO.getListSaleOrderAIADVOByFilter(filter);
			ObjectVO<SaleOrderVO> vo = new ObjectVO<SaleOrderVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<SaleOrderPromotionVO> getListSOPromotionBySOId(Long saleOrderId) throws BusinessException {
		try {
			return saleOrderDAO.getListSOPromotionBySOId(saleOrderId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<PromotionProgram> checkListPromotionReceived(List<SaleOrderPromotionVO> listReceived, Long shopId, Long staffId, Long customerId) throws BusinessException {
		try {
			List<PromotionProgram> listResult = new ArrayList<PromotionProgram>();
			for (SaleOrderPromotionVO vo : listReceived) {
				PromotionProgram pp = saleOrderDAO.checkQuantityReceived(vo.getPromotionId(), vo.getQuantity(), shopId, staffId, customerId);
				if (pp != null) {
					listResult.add(pp);
				}
			}
			return listResult;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * kiem tra max so tien KM
	 * @author trietptm
	 * @param saleOrderFilter
	 * @return
	 * @throws BusinessException
	 * @since Oct 23, 2015
	 */
	@Override
	public String checkDiscountAmountPromotionReceived(@SuppressWarnings("rawtypes") SaleOrderFilter saleOrderFilter) throws BusinessException {
		try {
			return checkMaxDiscountAmountPromotionReceived(saleOrderFilter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * kiem tra max so tien KM
	 */
	private String checkMaxDiscountAmountPromotionReceived(@SuppressWarnings("rawtypes") SaleOrderFilter saleOrderFilter) throws Exception {
		String promotionCodes = "";
		SaleOrderPromoDetailFilter filter = new SaleOrderPromoDetailFilter();
		filter.setSaleOrderId(saleOrderFilter.getSaleOrderId());
		filter.setIsNotKeyShop(true);
		List<SaleOrderPromotionDetailVO> lstSOPromoDetail = saleOrderPromoDetailDAO.getListSaleOrderPromoByFilter(filter);
		if (lstSOPromoDetail != null) {
			for (int i = 0, n = lstSOPromoDetail.size(); i < n; i++) {
				SaleOrderPromotionDetailVO SOPromoDetail = lstSOPromoDetail.get(i);
				if (!StringUtility.isNullOrEmpty(SOPromoDetail.getProgramCode()) && SOPromoDetail.getDiscountAmount() != null) {
					BigDecimal remainAmount = promotionProgramMgr.getAmountRemainApprovedOfPromotionProgram(SOPromoDetail.getProgramCode(), saleOrderFilter.getShopId(), 
																saleOrderFilter.getStaffId(), saleOrderFilter.getCustomerId(), saleOrderFilter.getLockDate());
					if (remainAmount != null && remainAmount.subtract(SOPromoDetail.getDiscountAmount()).compareTo(BigDecimal.ZERO) < 0) {
						promotionCodes += ", " + SOPromoDetail.getProgramCode() + "(" + remainAmount.setScale(2, BigDecimal.ROUND_HALF_UP) + ")";
					}
				}
			}
		}
		return StringUtility.isNullOrEmpty(promotionCodes) ? promotionCodes : promotionCodes.replaceFirst(", ", "");
	}
	
	/**
	 * kiem tra max so luong KM
	 * @author trietptm
	 * @param saleOrderFilter
	 * @return
	 * @throws BusinessException
	 * @since Oct 23, 2015
	 */
	@Override
	public String checkNumPromotionReceived(@SuppressWarnings("rawtypes") SaleOrderFilter saleOrderFilter) throws BusinessException {
		try {
			return checkMaxNumPromotionReceived(saleOrderFilter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * kiem tra max so luong KM
	 */
	private String checkMaxNumPromotionReceived(@SuppressWarnings("rawtypes") SaleOrderFilter saleOrderFilter) throws Exception {
		String promotionCodes = "";
		@SuppressWarnings("rawtypes")
		SaleOrderDetailFilter filter = new SaleOrderDetailFilter();
		filter.setSaleOrderId(saleOrderFilter.getSaleOrderId());
		filter.setIsNotKeyShop(true);
		List<SaleOrderDetailVO2> lstSOPromoDetail = saleOrderDetailDAO.getListDiscountAmountByProgramCode(filter);
		if (lstSOPromoDetail != null) {
			for (int i = 0, n = lstSOPromoDetail.size(); i < n; i++) {
				SaleOrderDetailVO2 saleOrderDetail = lstSOPromoDetail.get(i);
				if (!StringUtility.isNullOrEmpty(saleOrderDetail.getProgramCode()) && saleOrderDetail.getQuantity() != null) {
					Integer remainQuantity = promotionProgramMgr.getNumRemainApprovedOfPromotionProgram(saleOrderDetail.getProgramCode(), saleOrderFilter.getShopId(), 
															saleOrderFilter.getStaffId(), saleOrderFilter.getCustomerId(), saleOrderFilter.getLockDate());
					if (remainQuantity != null && remainQuantity - saleOrderDetail.getQuantity() < 0) {
						promotionCodes += ", " + saleOrderDetail.getProgramCode() + "(" + remainQuantity + ")";
					}
				}
			}
		}
		return StringUtility.isNullOrEmpty(promotionCodes) ? promotionCodes : promotionCodes.replaceFirst(", ", "");
	}

	/**
	 * Duyet don GO & DP
	 * @author trietptm
	 * @since Oct 22, 2015
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateGODPOrder4Print(ConfirmFilter param) throws BusinessException {
		try {
			List<StockTrans> listStockTrans = stockTransDAO.getListStockTrans(param.getListId());
			Date currentDate = commonDAO.getSysDate();
			for (StockTrans stt : listStockTrans) {
				stt.setApproved(SaleOrderStatus.APPROVED);
				stt.setApprovedStep(SaleOrderStep.PRINT_CONFIRMED);
				if (param.getLogInfo() != null) {
					stt.setUpdateUser(param.getLogInfo().getStaffCode());
				}
				stt.setUpdateDate(currentDate);
				stockTransDAO.updateStockTrans(stt);
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * Duyet & cap nhat kho danh sach don GO & DP 
	 * @author trietptm
	 * @param param
	 * @return
	 * @throws BusinessException
	 * @since Oct 22, 2015
	 */
	@Override
	public Map<String, List<String>> updateGODPOrderAndUpdateStock4Print(ConfirmFilter param) throws BusinessException {
		try {
			List<String> lstCode = new ArrayList<String>();
			Map<String, List<String>> errorMap = new HashMap<String, List<String>>();
			List<StockTrans> listStockTrans = stockTransDAO.getListStockTrans(param.getListId());
			Date currentDate = commonDAO.getSysDate();
			for (StockTrans stockTrans : listStockTrans) {
				String orderNumber = stockTrans.getStockTransCode();
				Integer confirmResult = this.confirmOneOrderGODP(stockTrans, param.getIsUpdateStock(), currentDate, param.getLogInfo());
				if (confirmResult != null && confirmResult != ORDER_COMFIRMED_SUCCESS) {
					constructErrorAfterConfirmSaleOrderAtPrintStep(errorMap, orderNumber, confirmResult);
				} else if (param.getIsUpdateStock()) {
					SaleOrderStockVOTemp orderVO = new SaleOrderStockVOTemp();
					orderVO.setId(stockTrans.getId());
					orderVO.setOrderNumber(stockTrans.getStockTransCode());
					orderVO.setOrderType(stockTrans.getTransType());
					orderVO.setShopId(stockTrans.getShop() == null ? null : stockTrans.getShop().getId());
					orderVO.setStaffId(stockTrans.getStaffId() == null ? null : stockTrans.getStaffId().getId());
					lstCode = checkProductStockTotalSalePlan(orderVO, lstCode);
				}
			}
			if (lstCode.size() > 0) {
				errorMap.put("productStockTotalSalePlan", lstCode);
			}
			return errorMap;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * Duyet & cap nhat kho tung don GO & DP 
	 * @author trietptm
	 * @param stockTrans
	 * @param isUpdateStock
	 * @param currentDate
	 * @param logInfo
	 * @return ma loi
	 * @since Oct 22, 2015
	 */
	private Integer confirmOneOrderGODP(final StockTrans stockTrans, final boolean isUpdateStock, final Date currentDate, final LogInfoVO logInfo) {
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		Integer comfirmResult = transactionTemplate.execute(new TransactionCallback<Integer>() {
			@Override
			public Integer doInTransaction(TransactionStatus transactionStatus) {
				Integer comfirmResult = null;
				try {
					if (!SaleOrderStatus.NOT_YET_APPROVE.equals(stockTrans.getApproved()) || !SaleOrderStep.CONFIRMED.equals(stockTrans.getApprovedStep())) {
						comfirmResult = ORDER_COMFIRMED_ALREADY;
						throw new BusinessException("ORDER_COMFIRMED_ALREADY");
					}
					stockTrans.setApproved(SaleOrderStatus.APPROVED);
					stockTrans.setApprovedStep(SaleOrderStep.PRINT_CONFIRMED);
					if (logInfo != null) {
						stockTrans.setUpdateUser(logInfo.getStaffCode());
					}
					stockTrans.setUpdateDate(currentDate);
					stockTransDAO.updateStockTrans(stockTrans);

					// cap nhat kho
					if (isUpdateStock) {
						List<SaleOrderStockDetailVO> listDetail = saleOrderDAO.getListSaleOrderStockDetail(stockTrans.getId(), stockTrans.getTransType(), true);
						if (listDetail.size() > 0) {
							SaleOrderStockVOTemp orderVO = new SaleOrderStockVOTemp();
							orderVO.setId(stockTrans.getId());
							orderVO.setOrderNumber(stockTrans.getStockTransCode());
							orderVO.setOrderType(stockTrans.getTransType());
							orderVO.setShopId(stockTrans.getShop() == null ? null : stockTrans.getShop().getId());
							orderVO.setStaffId(stockTrans.getStaffId() == null ? null : stockTrans.getStaffId().getId());
							updateStockCommon(listDetail, orderVO, logInfo.getStaffCode(), currentDate, logInfo);
						}
					}
					comfirmResult = ORDER_COMFIRMED_SUCCESS;
				} catch (Exception e) {
					String s = e.getMessage();
					// xu ly loi tu cap nhat kho updateStockCommon
					if (s.startsWith("KHONG_DU_SOLUONG_KHO_NV_VANSALE-") || s.startsWith("KHONG_DU_SOLUONG_KHO_NPP-")) {
						comfirmResult = SALE_ORDER_OVER_QUANTITY_STOCK_TOTAL;
					} else if (s.startsWith("DON_HANG_DA_XU_LY")) {
						comfirmResult = ORDER_COMFIRMED_ALREADY;
					}
					if (comfirmResult == null) { // system error
						comfirmResult = ORDER_CONFIRM_ERROR;
					}
					LogUtility.logError(e, "sessionId = " + logInfo.getSessionId() + "; confirm one order GO & DP at Print Step. stock_trans_id = " + stockTrans.getId() + "; " + e.getMessage());
					transactionStatus.setRollbackOnly();
				}
				return comfirmResult;
			}
		});
		return comfirmResult;
	}
	
	/**
	 * get num day sale order expire
	 * @author tuannd20
	 * @return
	 * @date 04/12/2014
	 */
	private Integer getNumDaySaleOrderExpire() {
		final String AP_PARAM_CODE = "NUM_DAYS_ORDER_EXPIRE";
		final Integer DEFAULT_NUM_DAY_ORDER_EXPIRE = 60;
		ApParamFilter apParamFilter = new ApParamFilter();
		apParamFilter.setType(ApParamType.SALE_ORDER_CONFIG);
		apParamFilter.setStatus(ActiveType.RUNNING);
		apParamFilter.setApParamCode(AP_PARAM_CODE);
		List<ApParam> apParams = null;
		try {
			apParams = apParamDAO.getListApParamByFilter(apParamFilter);
		} catch (DataAccessException e) {
			// pass through
		}
		if (apParams != null && apParams.size() >= 1 && !StringUtility.isNullOrEmpty(apParams.get(0).getValue())) {
			return new Integer(apParams.get(0).getValue());
		}
		return DEFAULT_NUM_DAY_ORDER_EXPIRE;
	}
	
	/**
	 * kiem tra du lieu ton kho con hop le sau khi xac nhan don hang hay khong
	 * @author tuannd20
	 * @param saleOrder don hang can kiem tra
	 * @param remainProductStockQuantity so luong ton kho thuc te con lai cua san pham theo kho de kiem tra ton kho co hop le sau khi duyet don hang hay ko
	 * @return true: neu nhu du lieu hop le sau khi xac nhan don hang; nguoc lai, false
	 * @throws DataAccessException 
	 * @since 30/01/2015
	 */
	private Boolean isStockDataValidAfterConfirmOrder(SaleOrder saleOrder, final Map<String, Integer> remainProductStockQuantity) throws DataAccessException {
		if (saleOrder == null) {
			throw new IllegalArgumentException("saleOrder is null.");
		}
		List<SaleOrderLot> saleOrderLots = saleOrderDAO.getListSaleOrderLotBySaleOrderId(saleOrder.getId());
		if (saleOrderLots != null && saleOrderLots.size() > 0) {
			List<StockTotalVO> stockTotalsForSaleOrder = stockTotalDAO.retrieveStockTotalsForSaleOrder(saleOrder.getId(), false);
			if (stockTotalsForSaleOrder != null && stockTotalsForSaleOrder.size() > 0) {
				Map<String, Integer> tempSuctractedProductStockQuantity = new HashMap<String, Integer>();	// luu so luong ton kho tru tam cho don hang dang xet, dung de restore so luong da tru de xet cho cac don sau
				for (int i = 0, saleOrderLotCount = saleOrderLots.size(); i < saleOrderLotCount; i++) {
					SaleOrderLot saleOrderLot = saleOrderLots.get(i);
					String productWarehouseKey = "";
					if (saleOrderLot != null && saleOrderLot.getWarehouse() != null && saleOrderLot.getProduct() != null) {
						productWarehouseKey = saleOrderLot.getProduct().getId() + "_" + saleOrderLot.getWarehouse().getId();
					}
					
					Integer saleOrderLotQuantity = saleOrderLot.getQuantity() != null ? saleOrderLot.getQuantity() : 0;
					/*
					 * Lay ton kho thuc te theo kho-san pham.
					 * Neu nhu san pham da tru ton kho thuc te truoc do (cho san pham khuyen mai hoac cho don hang truoc do) thi lay tu Map tam.
					 * Nguoc lai lay du lieu ton kho thuc te tu DB
					 */
					Integer stockTotalQuantity = 0;
					if (remainProductStockQuantity.containsKey(productWarehouseKey)) {
						stockTotalQuantity = remainProductStockQuantity.get(productWarehouseKey);
					} else {
						for (int j = 0, stockTotalCount = stockTotalsForSaleOrder.size(); j < stockTotalCount; j++) {
							StockTotalVO stockTotalVO = stockTotalsForSaleOrder.get(j);
							if (saleOrderLot != null && stockTotalVO != null
									&& saleOrderLot.getWarehouse() != null
									&& saleOrderLot.getWarehouse().getId().equals(stockTotalVO.getWarehouseId())
									&& saleOrderLot.getProduct() != null
									&& saleOrderLot.getProduct().getId().equals(stockTotalVO.getProductId())) {
								stockTotalQuantity = stockTotalVO.getQuantity() != null ? stockTotalVO.getQuantity() : 0;
								break;
							}
						}
					}
					
					// neu nhu sau khi tru ton kho lam ton kho thuc te am thi ko hop le
					if (stockTotalQuantity - saleOrderLotQuantity < 0) {
						/*
						 * restore so luong da tru tam de kiem tra ton kho cho cac don hang sau
						 */
						for (Entry<String, Integer> entry : tempSuctractedProductStockQuantity.entrySet()) {
							if (remainProductStockQuantity.containsKey(entry.getKey())) {
								Integer currentStockTotalQuantity = remainProductStockQuantity.get(entry.getKey());
								currentStockTotalQuantity = (stockTotalQuantity != null ? stockTotalQuantity : 0) + (entry.getValue() != null ? entry.getValue() : 0);
								remainProductStockQuantity.put(entry.getKey(), currentStockTotalQuantity);
							} else {
								remainProductStockQuantity.put(entry.getKey(), entry.getValue() != null ? entry.getValue() : 0);
							}
						}
						
						return false;
					}
					
					remainProductStockQuantity.put(productWarehouseKey, stockTotalQuantity - saleOrderLotQuantity);
					/*
					 * luu du lieu tru tam
					 */
					if (tempSuctractedProductStockQuantity.containsKey(productWarehouseKey)) {
						Integer prevSubtractedQuantity = tempSuctractedProductStockQuantity.get(productWarehouseKey);
						if (prevSubtractedQuantity == null) {
							prevSubtractedQuantity = 0;
						}
						tempSuctractedProductStockQuantity.put(productWarehouseKey, saleOrderLotQuantity + prevSubtractedQuantity);
					} else {
						tempSuctractedProductStockQuantity.put(productWarehouseKey, saleOrderLotQuantity);
					}
				}
			}
		}
		
		return true;
	}
	
	/**
	 * put thong tin loi khi xu ly xac nhan don hang o buoc xac nhan don IN
	 * @author tuannd20
	 * @param errorMap thong tin loi
	 * @param orderNumber so don hang loi
	 * @param errorCode ma loi xu ly
	 * @since 13/02/2015
	 */
	private void constructErrorAfterConfirmSaleOrderAtPrintStep(Map<String, List<String>> errorMap, String orderNumber, Integer errorCode) {
		if (errorCode != null) {
			if (errorMap == null) {
				errorMap = new HashMap<String, List<String>>();
			}
			String keyMap = "";
			if (errorCode == ORDER_COMFIRMED_ALREADY) {
				keyMap = "orderConfirmedAlready";
			} else if (errorCode == ORDER_CONFIRM_ERROR) {
				keyMap = "systemError";
			} else if (errorCode == SALE_ORDER_OVER_QUANTITY_STOCK_TOTAL) {
				keyMap = "failQuantityStockTotal";
			} else if (errorCode == SALE_ORDER_OVER_REWARD_MONEY) {
				keyMap = "overRewardMoney";
			} else if (errorCode == SALE_ORDER_OVER_REWARD_PRODUCT) {
				keyMap = "overRewardProduct";
			}
			if (!errorMap.containsKey(keyMap)) {
				errorMap.put(keyMap, new ArrayList<String>());
			}
			errorMap.get(keyMap).add(orderNumber);
		}
	}
	
	@Override
	public Map<String, Map<String, String>> prepareUpdateSaleOrder4Print(List<SaleOrder> lstSaleOrder, Date lockDay) throws BusinessException {
		Map<String, Map<String, String>> listError = new HashMap<String, Map<String, String>>();
		Map<String, Integer> remainProductStockQuantity = new HashMap<String, Integer>();	// so luong ton kho thuc te theo san pham & kho cua san pham con lai de check duyet don hang
		Map<String, List<KSCusProductReward>> mapKSCusProductReward = new HashMap<String, List<KSCusProductReward>>(); // map luu cac KSCusProductReward da thay doi
		Map<String, List<KSCustomer>> mapListKSCustomer = new HashMap<String, List<KSCustomer>>(); // map luu cac KSCustomer da thay doi
		try {
			for (SaleOrder so : lstSaleOrder) {
				Map<String, String> mapErrorOrder = new HashMap<String, String>();
				//check ton kho san pham
				if (OrderType.IN.equals(so.getOrderType())) {
					if (!this.isStockDataValidAfterConfirmOrder(so, remainProductStockQuantity)) {
						mapErrorOrder.put("failQuantityStockTotal", "x");
					}
				}
				//check tra thuong keyShop
				Map<String, String> mapError = this.prepareUpdateRewardKeyShop(so, mapKSCusProductReward, mapListKSCustomer);
				if (mapError.size() > 0) {
					mapErrorOrder.putAll(mapError);
				}
				// check so xuat
				String errImplode = "";
				List<SaleOrderPromotionVO> checkReceived = saleOrderDAO.getListSOPromotionBySOId(so.getId());
				for (SaleOrderPromotionVO vo : checkReceived) {
					PromotionProgram pp = saleOrderDAO.checkQuantityReceived(vo.getPromotionId(), vo.getQuantity(), so.getShop().getId(), so.getStaff().getId(), so.getCustomer().getId());
					if (pp != null) {
						errImplode += ", " + pp.getPromotionProgramCode();
					}
				}
				if (errImplode.length() > 0) {
					mapErrorOrder.put("promotion", errImplode.replaceFirst(", ", ""));
				}
				// kiem tra max so tien KM
				@SuppressWarnings("rawtypes")
				SaleOrderFilter saleOrderFilter = new SaleOrderFilter();
				saleOrderFilter.setSaleOrderId(so.getId());
				if (so.getShop() != null) {
					saleOrderFilter.setShopId(so.getShop().getId());
				}
				if (so.getStaff() != null) {
					saleOrderFilter.setStaffId(so.getStaff().getId());
				}
				if (so.getCustomer() != null) {
					saleOrderFilter.setCustomerId(so.getCustomer().getId());
				}
				String promotionCodes = this.checkMaxDiscountAmountPromotionReceived(saleOrderFilter);
				if (!StringUtility.isNullOrEmpty(promotionCodes)) {
					mapErrorOrder.put("discountAmountPromotion", promotionCodes);
				}
				// kiem tra max so luong KM
				promotionCodes = this.checkMaxNumPromotionReceived(saleOrderFilter);
				if (!StringUtility.isNullOrEmpty(promotionCodes)) {
					mapErrorOrder.put("numPromotion", promotionCodes);
				}
				
				if (mapErrorOrder.size() > 0) {
					listError.put(so.getOrderNumber(), mapErrorOrder);
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		} finally {
			remainProductStockQuantity.clear();
			mapKSCusProductReward.clear();
			mapListKSCustomer.clear();
			remainProductStockQuantity = null;
			mapKSCusProductReward = null;
			mapListKSCustomer = null;
		}
		return listError;
	}
	
	/**
	 * validate truoc khi tra thuong keyshop
	 * @author trietptm
	 * @param saleOrder
	 * @return Map error
	 * @throws BusinessException
	 * @since 10/02/2015
	 */
	private Map<String, String> prepareUpdateRewardKeyShop(SaleOrder saleOrder, final Map<String, List<KSCusProductReward>> mapKSCusProductReward, 
			final Map<String, List<KSCustomer>> mapListKSCustomer) throws DataAccessException {
		Map<String, String> err = new HashMap<String, String>();
		SaleOrderDetailFilter<SaleOrderDetail> filter = new SaleOrderDetailFilter<SaleOrderDetail>();
		filter.setSaleOrderId(saleOrder.getId());
		filter.setIsFreeItem(PROMOTION);
		filter.setProgramType(ProgramType.KEY_SHOP);
		List<SaleOrderDetail> lstSaleOrderDetail = saleOrderDetailDAO.getSaleOrderDetails(filter);
		for (int i = 0, n = lstSaleOrderDetail.size(); i < n; i++) {
			SaleOrderDetail saleOrderDetail = lstSaleOrderDetail.get(i);
			if (saleOrderDetail.getProduct() != null && saleOrder.getCustomer() != null && saleOrderDetail.getQuantity() != null && saleOrderDetail.getQuantity() > 0) {
				String key = saleOrderDetail.getProgramCode() + saleOrder.getCustomer().getId() + saleOrderDetail.getProduct().getId();
				List<KSCusProductReward> lstCusProductRewards = null;
				if (mapKSCusProductReward.containsKey(key)) {
					lstCusProductRewards = mapKSCusProductReward.get(key);
				} else {
					KeyShopFilter keyShopFilter = new KeyShopFilter();
					keyShopFilter.setKsCode(saleOrderDetail.getProgramCode());
					keyShopFilter.setCustomerId(saleOrder.getCustomer().getId());
					keyShopFilter.setProductId(saleOrderDetail.getProduct().getId());
					keyShopFilter.setIsReturn(false);
					lstCusProductRewards = keyShopDAO.getListKSCusProductRewardForReward(keyShopFilter);
					mapKSCusProductReward.put(key, lstCusProductRewards);
				}
				if (lstCusProductRewards != null && lstCusProductRewards.size() > 0) {
					int quantity = saleOrderDetail.getQuantity();
					int j = 0;
					while (quantity > 0 && j < lstCusProductRewards.size()) {
						KSCusProductReward ksCusProductReward = lstCusProductRewards.get(j++);
						int productNumDB = ksCusProductReward.getProductNum();
						int productNumDoneDB = ksCusProductReward.getProductNumDone();
						if (productNumDoneDB < productNumDB) {
							int productNumDone = productNumDB - productNumDoneDB;
							if (quantity < productNumDone) {
								productNumDone = quantity;
							}
							ksCusProductReward.setProductNumDone(productNumDoneDB + productNumDone);
							quantity = quantity - productNumDone;
						}
					}
					if (quantity > 0) {
						err.put("overRewardProduct", "x");
						break;
					}

//						int sumProductNum = 0;
//						int sumProductNumDone = 0;
//						for (int j = 0, m = lstCusProductRewards.size(); j < m; j++) {
//							KSCusProductReward ksCusProductReward = lstCusProductRewards.get(j);
//							sumProductNum += ksCusProductReward.getProductNum();
//							sumProductNumDone += ksCusProductReward.getProductNumDone();
//						}
//						if (sumProductNum < sumProductNumDone + saleOrderDetail.getQuantity()) {
//							err.put("overRewardProduct", "x");
//							break;
//						}
				} else {
					err.put("overRewardProduct", "x");
					break;
				}
			}
//			List<SaleOrderLot> lstSaleOrderLots = saleOrderLotDAO.getListSaleOrderLotBySaleOrderDetailId(saleOrderDetail.getId());
//			for (int k = 0, l = lstSaleOrderLots.size(); k < l; k++) {
//				SaleOrderLot saleOrderLot = lstSaleOrderLots.get(k);
//				String key = saleOrderLot.getProduct().getId() + "_" + saleOrderLot.getWarehouse().getId();
//				Integer stockTotalQuantity = 0;
//				if (remainProductStockQuantity.containsKey(key)) {
//					stockTotalQuantity = remainProductStockQuantity.get(key);
//				} else {
//					StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwner(saleOrderDetail.getProduct().getId(), StockObjectType.SHOP, saleOrderDetail.getShop().getId(), saleOrderLot.getWarehouse().getId());
//					stockTotalQuantity = stockTotal.getQuantity();
//				}
//				if (stockTotalQuantity < saleOrderLot.getQuantity()) {
//					err.put("failQuantityStockTotal", "x");
//					break;
//				}
//				remainProductStockQuantity.put(key, stockTotalQuantity - saleOrderLot.getQuantity());
//			}
		}

		SaleOrderPromoDetailFilter saleOrderPromoDetailFilter = new SaleOrderPromoDetailFilter();
		saleOrderPromoDetailFilter.setSaleOrderId(saleOrder.getId());
		saleOrderPromoDetailFilter.setProgramType(ProgramType.KEY_SHOP);
		saleOrderPromoDetailFilter.setProgrameTypeCode(KS);
		List<SaleOrderPromotionDetailVO> lstSaleOrderPromoDetails = saleOrderPromoDetailDAO.getListSaleOrderPromoByFilter(saleOrderPromoDetailFilter);
		for (int i = 0, n = lstSaleOrderPromoDetails.size(); i < n; i++) {
			SaleOrderPromotionDetailVO saleOrderPromoDetail = lstSaleOrderPromoDetails.get(i);
			BigDecimal discountAmount = saleOrderPromoDetail.getDiscountAmount();
			if (discountAmount != null && discountAmount.doubleValue() > 0 && saleOrder.getCustomer() != null) {
				String key = saleOrderPromoDetail.getProgramCode() + saleOrder.getCustomer().getId();
				List<KSCustomer> lstKSCustomer = null;
				if (mapListKSCustomer.containsKey(key)) {
					lstKSCustomer = mapListKSCustomer.get(key);
				} else {
					KeyShopFilter keyShopFilter = new KeyShopFilter();
					keyShopFilter.setKsCode(saleOrderPromoDetail.getProgramCode());
					keyShopFilter.setCustomerId(saleOrder.getCustomer().getId());
					keyShopFilter.setIsReturn(false);
					lstKSCustomer = keyShopDAO.getListKSCustomerForReward(keyShopFilter);
					mapListKSCustomer.put(key, lstKSCustomer);
				}

				if (lstKSCustomer != null && lstKSCustomer.size() > 0) {
					int j = 0;
					while (discountAmount.doubleValue() > 0 && j < lstKSCustomer.size()) {
						KSCustomer ksCustomer = lstKSCustomer.get(j++);
						BigDecimal totalRewardMoneyDB = ksCustomer.getTotalRewardMoney();
						BigDecimal totalRewardMoneyDoneDB = ksCustomer.getTotalRewardMoneyDone();
						if (totalRewardMoneyDoneDB.doubleValue() < totalRewardMoneyDB.doubleValue()) {
							BigDecimal totalRewardMoneyDone = totalRewardMoneyDB.subtract(totalRewardMoneyDoneDB);
							if (discountAmount.doubleValue() < totalRewardMoneyDone.doubleValue()) {
								totalRewardMoneyDone = discountAmount;
							}
							ksCustomer.setTotalRewardMoneyDone(totalRewardMoneyDoneDB.add(totalRewardMoneyDone));
							discountAmount = discountAmount.subtract(totalRewardMoneyDone);
						}
					}
					if (discountAmount.doubleValue() > 0) {
						err.put("overRewardMoney", "x");
						break;
					}

//						BigDecimal sumTotalRewardMoney = BigDecimal.ZERO;
//						BigDecimal sumTotalRewardMoneyDone = BigDecimal.ZERO;
//						for (int j = 0, m = lstKSCustomer.size(); j < m; j++) {
//							KSCustomer ksCustomer = lstKSCustomer.get(j);
//							sumTotalRewardMoney = sumTotalRewardMoney.add(ksCustomer.getTotalRewardMoney());
//							sumTotalRewardMoneyDone = sumTotalRewardMoneyDone.add(ksCustomer.getTotalRewardMoneyDone());
//						}
//						if (sumTotalRewardMoney.doubleValue() < sumTotalRewardMoneyDone.add(discountAmount).doubleValue()) {
//							err.put("overRewardMoney", "x");
//							break;
//						}
				} else {
					err.put("overRewardMoney", "x");
					break;
				}
			}
		}
		return err;
	}
	
	/**
	 * kiem tra va set lai RewardType cho KSCustomer & luu KSCustomer, KSCusProductReward
	 * @author trietptm
	 * @param mapKSCustomer
	 * @param mapKSCusProductReward
	 * @param currentDate
	 * @param logInfo
	 * @throws DataAccessException
	 * @since Oct 19, 2015
	 */
	private void updateRewardType(Map<Long, KSCustomer> mapKSCustomer, Map<Long, KSCusProductReward> mapKSCusProductReward, Date currentDate, LogInfoVO logInfo) throws DataAccessException {
		if (mapKSCustomer.isEmpty()) {
			return;
		}
		List<Long> lstKSCustomerId = new ArrayList<Long>(mapKSCustomer.keySet());
		List<KSCustomer> lstKSCustomers = new ArrayList<KSCustomer>(mapKSCustomer.values());
		KeyShopFilter keyShopFilter = new KeyShopFilter();
		keyShopFilter.setLstKsCustomerId(lstKSCustomerId);
		List<KSCusProductReward> lstKSCusProductReward = keyShopDAO.getListKSCusProductRewardByFilter(keyShopFilter);
		for (int i = 0, n = lstKSCustomers.size(); i < n; i++) {
			KSCustomer ksCustomer = lstKSCustomers.get(i);
			int sumProductNum = 0;
			int sumProductNumDone = 0;
			for (int j = 0, m = lstKSCusProductReward.size(); j < m; j++) {
				KSCusProductReward ksCusProductReward = lstKSCusProductReward.get(j);
				if (ksCusProductReward.getKsCustomer() != null && ksCustomer.getId().equals(ksCusProductReward.getKsCustomer().getId())) {
					if (mapKSCusProductReward.containsKey(ksCusProductReward.getId())) {
						ksCusProductReward = mapKSCusProductReward.get(ksCusProductReward.getId());
					}
					sumProductNum += ksCusProductReward.getProductNum();
					sumProductNumDone += ksCusProductReward.getProductNumDone();
				}
			}
			if (ksCustomer.getTotalRewardMoney().doubleValue() == ksCustomer.getTotalRewardMoneyDone().doubleValue() 
					&& (sumProductNum == sumProductNumDone)) {
				ksCustomer.setRewardType(RewardType.DA_TRA_TOAN_BO);
			} else if ((ksCustomer.getTotalRewardMoney().doubleValue() == 0 || ksCustomer.getTotalRewardMoneyDone().doubleValue() == 0) 
					&& (sumProductNum == 0 || sumProductNumDone == 0)) {
				ksCustomer.setRewardType(RewardType.MO_KHOA_CHUA_TRA);
			} else {
				ksCustomer.setRewardType(RewardType.DA_TRA_MOT_PHAN);
			}
			ksCustomer.setUpdateDate(currentDate);
			ksCustomer.setUpdateUser(logInfo.getStaffCode());
		}
		if (!mapKSCusProductReward.isEmpty()) {
			commonDAO.updateListEntity(new ArrayList<KSCusProductReward>(mapKSCusProductReward.values()));
		}
		commonDAO.updateListEntity(lstKSCustomers);
	}
	
	
	/**
	 * tra thuong keyshop (phai validate cac TH tra thuong truoc khi goi)
	 */
	private boolean updateRewardKeyShop(SaleOrder saleOrder, Date currentDate, LogInfoVO logInfo) throws Exception {
		if (OrderType.IN.equals(saleOrder.getOrderType()) || OrderType.SO.equals(saleOrder.getOrderType())) {
			Map<Long, KSCustomer> mapKSCustomer = new HashMap<Long, KSCustomer>();
			Map<Long, KSCusProductReward> mapKSCusProductReward = new HashMap<Long, KSCusProductReward>();
			try {
				SaleOrderDetailFilter<SaleOrderDetail> filter = new SaleOrderDetailFilter<SaleOrderDetail>();
				filter.setSaleOrderId(saleOrder.getId());
				filter.setIsFreeItem(PROMOTION);
				filter.setProgramType(ProgramType.KEY_SHOP);
				List<SaleOrderDetail> lstSaleOrderDetail = saleOrderDetailDAO.getSaleOrderDetails(filter);
				for (int i = 0, n = lstSaleOrderDetail.size(); i < n; i++) {
					SaleOrderDetail saleOrderDetail = lstSaleOrderDetail.get(i);
					//tra san pham
					if (saleOrderDetail.getProduct() != null && saleOrder.getCustomer() != null && saleOrderDetail.getQuantity() != null && saleOrderDetail.getQuantity() > 0) {
						KeyShopFilter keyShopFilter = new KeyShopFilter();
						keyShopFilter.setKsCode(saleOrderDetail.getProgramCode());
						keyShopFilter.setCustomerId(saleOrder.getCustomer().getId());
						keyShopFilter.setProductId(saleOrderDetail.getProduct().getId());
						keyShopFilter.setIsReturn(false);
						List<KSCusProductReward> lstCusProductRewards = keyShopDAO.getListKSCusProductRewardForReward(keyShopFilter);
						if (lstCusProductRewards != null && lstCusProductRewards.size() > 0) {
							int quantity = saleOrderDetail.getQuantity();
							int j = 0;
							while (quantity > 0 && j < lstCusProductRewards.size()) {
								KSCusProductReward ksCusProductReward = lstCusProductRewards.get(j++);
								int productNumDB = ksCusProductReward.getProductNum();
								int productNumDoneDB = ksCusProductReward.getProductNumDone();
								if (productNumDoneDB < productNumDB) {
									int productNumDone = productNumDB - productNumDoneDB;
									if (quantity < productNumDone) {
										productNumDone = quantity;
									}
									ksCusProductReward.setProductNumDone(productNumDoneDB + productNumDone);
									quantity = quantity - productNumDone;
									ksCusProductReward.setUpdateDate(currentDate);
									ksCusProductReward.setUpdateUser(logInfo.getStaffCode());
									mapKSCusProductReward.put(ksCusProductReward.getId(), ksCusProductReward);
									mapKSCustomer.put(ksCusProductReward.getKsCustomer().getId(), ksCusProductReward.getKsCustomer());
								}
							}
							if (quantity > 0) {
								// overRewardProduct
								throw new BusinessException("SALE_ORDER_OVER_REWARD_PRODUCT");
							}
						} else {
							// overRewardProduct
							throw new BusinessException("SALE_ORDER_OVER_REWARD_PRODUCT");
						}
					}
				}
				SaleOrderPromoDetailFilter saleOrderPromoDetailFilter = new SaleOrderPromoDetailFilter();
				saleOrderPromoDetailFilter.setSaleOrderId(saleOrder.getId());
				saleOrderPromoDetailFilter.setProgramType(ProgramType.KEY_SHOP);
				saleOrderPromoDetailFilter.setProgrameTypeCode(KS);
				List<SaleOrderPromotionDetailVO> lstSaleOrderPromoDetails = saleOrderPromoDetailDAO.getListSaleOrderPromoByFilter(saleOrderPromoDetailFilter);
				for (int i = 0, n = lstSaleOrderPromoDetails.size(); i < n; i++) {
					SaleOrderPromotionDetailVO saleOrderPromoDetail = lstSaleOrderPromoDetails.get(i);
					// tra tien
					BigDecimal discountAmount = saleOrderPromoDetail.getDiscountAmount();
					if (discountAmount != null && discountAmount.doubleValue() > 0 && saleOrder.getCustomer() != null) {
						KeyShopFilter keyShopFilter = new KeyShopFilter();
						keyShopFilter.setKsCode(saleOrderPromoDetail.getProgramCode());
						keyShopFilter.setCustomerId(saleOrder.getCustomer().getId());
						keyShopFilter.setIsReturn(false);
						List<KSCustomer> lstKeyCustomers = keyShopDAO.getListKSCustomerForReward(keyShopFilter);
						if (lstKeyCustomers != null && lstKeyCustomers.size() > 0) {
							int j = 0;
							while (discountAmount.doubleValue() > 0 && j < lstKeyCustomers.size()) {
								KSCustomer ksCustomer = lstKeyCustomers.get(j++);
								BigDecimal totalRewardMoneyDB = ksCustomer.getTotalRewardMoney();
								BigDecimal totalRewardMoneyDoneDB = ksCustomer.getTotalRewardMoneyDone();
								if (totalRewardMoneyDoneDB.doubleValue() < totalRewardMoneyDB.doubleValue()) {
									BigDecimal totalRewardMoneyDone = totalRewardMoneyDB.subtract(totalRewardMoneyDoneDB);
									if (discountAmount.doubleValue() < totalRewardMoneyDone.doubleValue()) {
										totalRewardMoneyDone = discountAmount;
									}
									ksCustomer.setTotalRewardMoneyDone(totalRewardMoneyDoneDB.add(totalRewardMoneyDone));
									discountAmount = discountAmount.subtract(totalRewardMoneyDone);
									mapKSCustomer.put(ksCustomer.getId(), ksCustomer);
								}
							}
							if (discountAmount.doubleValue() > 0) {
								//overRewardMoney
								throw new BusinessException("SALE_ORDER_OVER_REWARD_MONEY");
							}
						} else {
							//overRewardMoney
							throw new BusinessException("SALE_ORDER_OVER_REWARD_MONEY");
						}
					}
				}
				updateRewardType(mapKSCustomer, mapKSCusProductReward, currentDate, logInfo);
			} finally {
				mapKSCusProductReward.clear();
				mapKSCustomer.clear();
				mapKSCusProductReward = null;
				mapKSCustomer = null;
			}
		}
		return true;
	}

	/**
	 * tra hang co tra thuong keyshop
	 * @author trietptm
	 * @param saleOrder
	 * @param currentDate
	 * @param logInfo
	 * @return
	 * @throws Exception
	 * @since Oct 16, 2015
	 */
	private boolean returnRewardKeyShop(SaleOrder saleOrder, Date currentDate, LogInfoVO logInfo) throws Exception {
		if (OrderType.CO.equals(saleOrder.getOrderType()) || OrderType.CM.equals(saleOrder.getOrderType())) {
			Map<Long, KSCustomer> mapKSCustomer = new HashMap<Long, KSCustomer>();
			Map<Long, KSCusProductReward> mapKSCusProductReward = new HashMap<Long, KSCusProductReward>();
			try {
				SaleOrderDetailFilter<SaleOrderDetail> filter = new SaleOrderDetailFilter<SaleOrderDetail>();
				filter.setSaleOrderId(saleOrder.getId());
				filter.setIsFreeItem(PROMOTION);
				filter.setProgramType(ProgramType.KEY_SHOP);
				List<SaleOrderDetail> lstSaleOrderDetail = saleOrderDetailDAO.getSaleOrderDetails(filter);
				for (int i = 0, n = lstSaleOrderDetail.size(); i < n; i++) {
					SaleOrderDetail saleOrderDetail = lstSaleOrderDetail.get(i);
					//tra san pham
					if (saleOrderDetail.getProduct() != null && saleOrderDetail.getQuantity() != null && saleOrderDetail.getQuantity() > 0) {
						KeyShopFilter keyShopFilter = new KeyShopFilter();
						keyShopFilter.setKsCode(saleOrderDetail.getProgramCode());
						keyShopFilter.setCustomerId(saleOrder.getCustomer().getId());
						keyShopFilter.setProductId(saleOrderDetail.getProduct().getId());
						keyShopFilter.setIsReturn(true);
						List<KSCusProductReward> lstCusProductRewards = keyShopDAO.getListKSCusProductRewardForReward(keyShopFilter);
						if (lstCusProductRewards != null && lstCusProductRewards.size() > 0) {
							int quantity = saleOrderDetail.getQuantity();
							int j = 0;
							while (quantity > 0 && j < lstCusProductRewards.size()) {
								KSCusProductReward ksCusProductReward = lstCusProductRewards.get(j++);
								int productNumDoneDB = ksCusProductReward.getProductNumDone();
								if (productNumDoneDB > 0) {
									int productNumDone = productNumDoneDB;
									if (quantity < productNumDoneDB) {
										productNumDone = quantity;
									}
									ksCusProductReward.setProductNumDone(productNumDoneDB - productNumDone);
									quantity = quantity - productNumDone;
									ksCusProductReward.setUpdateDate(currentDate);
									ksCusProductReward.setUpdateUser(logInfo.getStaffCode());
									mapKSCusProductReward.put(ksCusProductReward.getId(), ksCusProductReward);
									mapKSCustomer.put(ksCusProductReward.getKsCustomer().getId(), ksCusProductReward.getKsCustomer());
								}
							}
							if (quantity > 0) {
								// overRewardProduct
								throw new BusinessException();
							}
						} else {
							// overRewardProduct
							throw new BusinessException();
						}
					}
				}
				SaleOrderPromoDetailFilter saleOrderPromoDetailFilter = new SaleOrderPromoDetailFilter();
				saleOrderPromoDetailFilter.setSaleOrderId(saleOrder.getId());
				saleOrderPromoDetailFilter.setProgramType(ProgramType.KEY_SHOP);
				saleOrderPromoDetailFilter.setProgrameTypeCode(KS);
				List<SaleOrderPromotionDetailVO> lstSaleOrderPromoDetails = saleOrderPromoDetailDAO.getListSaleOrderPromoByFilter(saleOrderPromoDetailFilter);
				for (int i = 0, n = lstSaleOrderPromoDetails.size(); i < n; i++) {
					SaleOrderPromotionDetailVO saleOrderPromoDetail = lstSaleOrderPromoDetails.get(i);
					// tra tien
					BigDecimal discountAmount = saleOrderPromoDetail.getDiscountAmount();
					if (discountAmount != null && discountAmount.doubleValue() > 0) {
						KeyShopFilter keyShopFilter = new KeyShopFilter();
						keyShopFilter.setKsCode(saleOrderPromoDetail.getProgramCode());
						keyShopFilter.setCustomerId(saleOrder.getCustomer().getId());
						keyShopFilter.setIsReturn(true);
						List<KSCustomer> lstKeyCustomers = keyShopDAO.getListKSCustomerForReward(keyShopFilter);
						if (lstKeyCustomers != null && lstKeyCustomers.size() > 0) {
							int j = 0;
							while (discountAmount.doubleValue() > 0 && j < lstKeyCustomers.size()) {
								KSCustomer ksCustomer = lstKeyCustomers.get(j++);
								BigDecimal totalRewardMoneyDoneDB = ksCustomer.getTotalRewardMoneyDone();
								if (totalRewardMoneyDoneDB.doubleValue() > 0) {
									BigDecimal totalRewardMoneyDone = totalRewardMoneyDoneDB;
									if (discountAmount.doubleValue() < totalRewardMoneyDone.doubleValue()) {
										totalRewardMoneyDone = discountAmount;
									}
									ksCustomer.setTotalRewardMoneyDone(totalRewardMoneyDoneDB.subtract(totalRewardMoneyDone));
									discountAmount = discountAmount.subtract(totalRewardMoneyDone);
									// commonDAO.updateEntity(ksCustomer); 
									// put vao map se update ben duoi
									mapKSCustomer.put(ksCustomer.getId(), ksCustomer);
								}
							}
							if (discountAmount.doubleValue() > 0) {
								//overRewardMoney
								throw new BusinessException();
							}
						} else {
							//overRewardMoney
							throw new BusinessException();
						}
					}
				}
				updateRewardType(mapKSCustomer, mapKSCusProductReward, currentDate, logInfo);
			} finally {
				mapKSCusProductReward.clear();
				mapKSCustomer.clear();
				mapKSCusProductReward = null;
				mapKSCustomer = null;
			}
		}
		return true;
	}
	
	/**
	 * so sanh so luong san pham trong stock total va sale plan
	 * @author trietptm
	 * @param tempOrder
	 * @param lstCodes
	 * @throws Exception
	 * @since Oct 23, 2015
	 */
	private List<String> checkProductStockTotalSalePlan(SaleOrderStockVOTemp tempOrder, List<String> lstCodes) throws Exception {
		SaleOrder so = null;
		StockTrans st = null;
		List<ProductStockTotal> lstProductStock = null;
		List<ProductStockTotal> lstProductSalePlan = null;
		if (tempOrder.getOrderType().equals(OrderType.IN.getValue()) || tempOrder.getOrderType().equals(OrderType.CM.getValue()) || tempOrder.getOrderType().equals(OrderType.SO.getValue())
				|| tempOrder.getOrderType().equals(OrderType.CO.getValue())) {
			int SALE_ORDER = 0;
			so = this.getSaleOrderById(tempOrder.getId());
			lstProductStock = saleOrderDAO.lstProductStockTotal(so, null, SALE_ORDER);
			lstProductSalePlan = saleOrderDAO.lstProductSalePlan(so, null, SALE_ORDER);
		} else if (tempOrder.getOrderType().equals(OrderType.GO.getValue()) || tempOrder.getOrderType().equals(OrderType.DP.getValue()) || tempOrder.getOrderType().equals(OrderType.DC.getValue())
				|| tempOrder.getOrderType().equals(OrderType.DCT.getValue()) || tempOrder.getOrderType().equals(OrderType.DCG.getValue())) {
			int STOCK_TRANS = 1;
			st = stockTransDAO.getStockTransById(tempOrder.getId());
			lstProductStock = saleOrderDAO.lstProductStockTotal(null, st, STOCK_TRANS);
			lstProductSalePlan = saleOrderDAO.lstProductSalePlan(null, st, STOCK_TRANS);
		}
		if (lstProductSalePlan != null && lstProductStock != null) {
			int size = lstProductSalePlan.size();
			for (ProductStockTotal productStock : lstProductStock) {
				for (int j = 0; j < size; j++) {
					ProductStockTotal productSalePlan = lstProductSalePlan.get(j);
					long productStockId = productStock.getProductId();
					long productSalePlanId = productSalePlan.getProductId();
					if (productStockId == productSalePlanId && productSalePlan.getNesStock() != null && productStock.getQuantity() != null) {
						long stockQuantity = productStock.getQuantity().longValue();
						long salePlanQuantity = productSalePlan.getNesStock();
						if (stockQuantity < salePlanQuantity && !lstCodes.contains(productStock.getProductCode())) {
							lstCodes.add(productStock.getProductCode());
						}
					}
				}
			}
		}
		return lstCodes;
	}
	
	/**
	 * Duyet don hang
	 * @author trietptm
	 * @param param
	 * @return map loi
	 * @throws BusinessException
	 * @since Oct 23, 2015
	 */
	@Override
	public Map<String, List<String>> updateSaleOrder4Print(ConfirmFilter param) throws BusinessException {
		try {
			Date currentDate = commonDAO.getSysDate();
			List<SaleOrder> listSaleOrder = saleOrderDAO.getListSaleOrder(param.getListSaleOrderId());
			/*
			 * get sale order config info
			 */
			final Integer DEFAULT_NUM_DAY_ORDER_EXPIRE = getNumDaySaleOrderExpire();
			String CONFIG_SYS_CAL_DATE = null;
			ApParam ap = apParamMgr.getApParamByCodeEx(Constant.SYS_CAL_DATE, ActiveType.RUNNING);
			if (ap != null) {
				CONFIG_SYS_CAL_DATE = ap.getValue();
			}

			boolean IS_RETURN_KEY_SHOP = true;
			ap = apParamMgr.getApParamByCodeEx(ApParamType.RETURN_KEYSHOP_CONFIG.getValue(), ActiveType.RUNNING);
			if (ap != null && Constant.RETURN_KEYSHOP_CONFIG_NO.equals(ap.getValue())) {
				IS_RETURN_KEY_SHOP = false;
			}
			List<String> lstCode = new ArrayList<String>();
			Map<String, List<String>> errorMap = new HashMap<String, List<String>>();
			for (SaleOrder saleOrder : listSaleOrder) {
				if (saleOrder != null) {
					String orderNumber = saleOrder.getOrderNumber();
					Integer confirmResult = this.confirmOneOrderInPrintStep(saleOrder, param.getLockDate(), currentDate, DEFAULT_NUM_DAY_ORDER_EXPIRE, CONFIG_SYS_CAL_DATE,
							IS_RETURN_KEY_SHOP, param.getIsUpdateStock(), param.getLstRemovedInvoice().contains(saleOrder.getId()), param.getLogInfo());
					if (confirmResult != null && confirmResult != ORDER_COMFIRMED_SUCCESS) {
						constructErrorAfterConfirmSaleOrderAtPrintStep(errorMap, orderNumber, confirmResult);
					} else if (param.getIsUpdateStock()) {
						SaleOrderStockVOTemp orderVO = new SaleOrderStockVOTemp();
						orderVO.setId(saleOrder.getId());
						orderVO.setOrderNumber(saleOrder.getOrderNumber());
						orderVO.setOrderSource(saleOrder.getOrderSource() == null ? null : saleOrder.getOrderSource().getValue());
						orderVO.setOrderType(saleOrder.getOrderType() == null ? null : saleOrder.getOrderType().getValue());
						orderVO.setShopId(saleOrder.getShop() == null ? null : saleOrder.getShop().getId());
						orderVO.setStaffId(saleOrder.getStaff() == null ? null : saleOrder.getStaff().getId());
						lstCode = checkProductStockTotalSalePlan(orderVO, lstCode);
					}
				}
			}
			if (lstCode.size() > 0) {
				errorMap.put("productStockTotalSalePlan", lstCode);
			}
			return errorMap;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * xac nhan don hang o buoc xac nhan IN
	 * @author tuannd20
	 * @param saleOrder don hang xac nhan
	 * @param lockDate ngay chot
	 * @param currentDate ngay hien tai he thong
	 * @param DEFAULT_NUM_DAY_ORDER_EXPIRE so ngay toi da de tinh don hang cuoi cung
	 * @param CONFIG_SYS_CAL_DATE cau hinh set accountDate
	 * @param IS_RETURN_KEY_SHOP cau hinh tra hang keyshop
	 * @param isUpdateStock cap nhat kho
	 * @param logInfo doi tuong chua thong tin ghi log
	 * @since 01/02/2015
	 */
	private Integer confirmOneOrderInPrintStep(final SaleOrder saleOrder, final Date lockDate,
			final Date currentDate, final Integer DEFAULT_NUM_DAY_ORDER_EXPIRE, final String CONFIG_SYS_CAL_DATE, final boolean IS_RETURN_KEY_SHOP,
			final boolean isUpdateStock, final boolean removingInvoice, final LogInfoVO logInfo) {
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		Integer comfirmResult = transactionTemplate.execute(new TransactionCallback<Integer>() {
			@Override
			public Integer doInTransaction(TransactionStatus transactionStatus) {
				Integer comfirmResult = null;
				try {
					/*
					 * check if sale order has confirmed already
					 */
					if (saleOrder.getApproved() == SaleOrderStatus.APPROVED && saleOrder.getApprovedStep() == SaleOrderStep.PRINT_CONFIRMED) {
						comfirmResult = ORDER_COMFIRMED_ALREADY;
						BusinessException e = new BusinessException("Sale Order confirmed already.");
						LogUtility.logError(e, "sessionId = " + logInfo.getSessionId() + ";confirm one order at Print Step. sale_order_id = " + saleOrder.getId() + "; " + e.getMessage());
						return comfirmResult;
					}
					
//					SaleOrderFilter<RptCTTLPay> cttlFilter = new SaleOrderFilter<RptCTTLPay>();
//					List<RptCTTLPay> lstPays = null;
//					cttlFilter.setSaleOrderId(saleOrder.getId());
//					lstPays = promotionProgramDAO.getListRptCTTLPay(cttlFilter);
//					if (lstPays != null && lstPays.size() > 0) {
//						for (int j = 0, szj = lstPays.size(); j < szj; j++) {
//							lstPays.get(j).setApproved(SaleOrderStatus.APPROVED.getValue());
//						}
//						commonDAO.updateListEntity(lstPays);
//					}
					
					// lacnv1 - Huy so hoa don cua don hang goc neu la don tra hang
					if (removingInvoice && saleOrder.getFromSaleOrder() != null
							&& (OrderType.CM.equals(saleOrder.getOrderType()) || OrderType.CO.equals(saleOrder.getOrderType()))) {
						List<Invoice> listInvoice = null;
						List<SaleOrderDetail> listSaleOrderDetail = null;
						listInvoice = invoiceDAO.getListInvoiceBySaleOrderId2(saleOrder.getFromSaleOrder().getId(), 0);
						if (listInvoice != null && listInvoice.size() > 0) {
							for (int i = 0; i < listInvoice.size(); i++) {
								Invoice invoice = listInvoice.get(i);
								if (!InvoiceStatus.CANCELED.equals(invoice.getStatus())) {
									invoice.setDestroyUser(logInfo.getStaffCode());
									invoice.setDestroyDate(lockDate);
									invoice.setStatus(InvoiceStatus.CANCELED);
									invoice.setUpdateUser(logInfo.getStaffCode());
									invoiceDAO.updateInvoice(invoice);
								}
							}
						}
						listSaleOrderDetail = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(saleOrder.getFromSaleOrder().getId(), null);
						if (listSaleOrderDetail != null && listSaleOrderDetail.size() > 0) {
							for (SaleOrderDetail itemSaleOrderDetail : listSaleOrderDetail) {
								itemSaleOrderDetail.setInvoice(null);
								saleOrderDetailDAO.updateSaleOrderDetail(itemSaleOrderDetail);
							}
						}
					}
					
					if (OrderType.IN.equals(saleOrder.getOrderType())) {
						/* cap nhap sale_order */
						checkOrder(saleOrder.getId(),CommonContanst.SALE_ORDER_APPROVED_STEP);
						saleOrder.setApproved(SaleOrderStatus.APPROVED);
						saleOrder.setApprovedDate(lockDate);
						if (Constant.TWO_TEXT.equals(CONFIG_SYS_CAL_DATE)) {
							saleOrder.setAccountDate(saleOrder.getApprovedDate());
						}
						saleOrder.setApprovedStep(SaleOrderStep.PRINT_CONFIRMED);
						saleOrder.setOrderNumber(saleOrderDAO.getLastestOrderNumberAndUpdateByShop(saleOrder.getShop().getId(),OrderType.IN));
						saleOrder.setUpdateUser(logInfo.getStaffCode());
						saleOrder.setUpdateDate(currentDate);
						saleOrderDAO.updateSaleOrder(saleOrder);
//						saleOrderDAO.updateLastestOrderNumberByShop(saleOrder.getShop().getId());
						
						/* cap nhap po_customer */
						if (SaleOrderSource.TABLET.equals(saleOrder.getOrderSource()) && saleOrder.getFromPoCustomerId() != null) {
							PoCustomer poCustomer = poCustomerDAO.getPoCustomerById(saleOrder.getFromPoCustomerId());
							if (poCustomer != null) {
								poCustomer.setApproved(SaleOrderStatus.APPROVED);
								//poCustomer.setOrderNumber(so.getOrderNumber());
								
								poCustomer.setApprovedDate(lockDate);
								if (Constant.TWO_TEXT.equals(CONFIG_SYS_CAL_DATE)) {
									poCustomer.setAccountDate(poCustomer.getApprovedDate());
								}
								poCustomer.setUpdateUser(logInfo.getStaffCode());
								poCustomer.setUpdateDate(currentDate);
								poCustomerDAO.updatePoCustomer(poCustomer);
							}
						}
						
						/*
						 * update so xuat, so tien tra, so luong sp tra PROMOTION_SHOP_MAP, PROMOTION_CUSTOMER_MAP, PROMOTION_STAFF_MAP
						 */
						updatePromotionQuantityReceived(saleOrder, 1, currentDate, logInfo);
						updatePromotionAmountReceived(saleOrder, 1, currentDate, logInfo);
						updatePromotionNumReceived(saleOrder, 1, currentDate, logInfo);
						
						if(saleOrder.getAmount() != null && saleOrder.getAmount().compareTo(BigDecimal.ZERO) == 1) {
							Customer customer = customerDAO.getCustomerById(saleOrder.getCustomer().getId());
							customer.setLastApproveOrder(lockDate);
							customer.setUpdateUser(logInfo.getStaffCode());
							customer.setUpdateDate(currentDate);
							customerDAO.updateCustomer(customer, logInfo);
							
							StaffCustomer staffCustomer = staffCustomerDAO.getStaffCustomer(saleOrder.getStaff().getId(), saleOrder.getCustomer().getId());
							if (staffCustomer == null) {
								staffCustomer = new StaffCustomer();
								staffCustomer.setStaff(saleOrder.getStaff());
								staffCustomer.setCustomer(saleOrder.getCustomer());
								staffCustomer.setLastApproveOrder(lockDate);
								staffCustomer.setShop(saleOrder.getShop());
								staffCustomerDAO.createStaffCustomer(staffCustomer, logInfo);
							} else {
								staffCustomer.setLastApproveOrder(lockDate);
								staffCustomerDAO.updateStaffCustomer(staffCustomer, logInfo);
							}
							
							//Modified by NhanLT6 - 10/11/2014 - Cap nhat them truong last_approved_order de phuc vu 
							//dong bo du lieu voi sales_tablet
							//RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDate(so.getCustomer().getId(),so.getShop().getId(), so.getOrderDate());
							RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDateAndStaff(saleOrder.getCustomer().getId(),saleOrder.getShop().getId(), saleOrder.getOrderDate(), saleOrder.getStaff().getId());
							
							if(rc != null) {
								rc.setLastApproveOrder(lockDate);
								routingCustomerDAO.updateRoutingCustomer(rc);
							}
						}
						
						Debit debit = debitDAO.getDebitForUpdate(saleOrder.getCustomer().getId(), DebitOwnerType.CUSTOMER);
						if (debit == null) {
							debit = new Debit();
							debit.setObjectId(saleOrder.getCustomer().getId());
							debit.setObjectType(DebitOwnerType.CUSTOMER);
							debit.setTotalAmount(saleOrder.getTotal());
							debit.setTotalDebit(saleOrder.getTotal());
							debit.setMaxDebitAmount(saleOrder.getCustomer().getMaxDebitAmount());
							debit.setMaxDebitDate(saleOrder.getCustomer().getMaxDebitDate());
							debit.setTotalPay(BigDecimal.ZERO);
							debit.setTotalDiscount(BigDecimal.ZERO);
							debit = debitDAO.createDebit(debit);
						} else {
							debit.setTotalAmount(debit.getTotalAmount().add(saleOrder.getTotal()));
							debit.setTotalDebit(debit.getTotalDebit().add(saleOrder.getTotal()));
							debit.setMaxDebitAmount(saleOrder.getCustomer().getMaxDebitAmount());
							debit.setMaxDebitDate(saleOrder.getCustomer().getMaxDebitDate());
							debitDAO.updateDebit(debit);
						}
						DebitDetail debitDetail = debitDetailDAO.getDebitDetailByFromObjectId(saleOrder.getId());
						if (debitDetail != null) {
							BusinessException e = new BusinessException("sale_order_exist_debit_detail");
							comfirmResult = ORDER_COMFIRMED_ALREADY;
							throw e;
						}
						debitDetail = new DebitDetail();
						debitDetail.setFromObjectId(saleOrder.getId());
						debitDetail.setFromObjectNumber(saleOrder.getOrderNumber());
						debitDetail.setCreateDate(lockDate);
						debitDetail.setAmount(saleOrder.getAmount());
						debitDetail.setDiscount(saleOrder.getDiscount());
						debitDetail.setTotal(saleOrder.getTotal());
						debitDetail.setRemain(saleOrder.getTotal());
						debitDetail.setType(4);//co: tang no KH do ban hang
						debitDetail.setDebitDate(lockDate);
						debitDetail.setShop(saleOrder.getShop());
						debitDetail.setCustomer(saleOrder.getCustomer());
						debitDetail.setStaff(saleOrder.getStaff());
						debitDetail.setDebit(debit);
						debitDetail.setCreateDate(currentDate);
						debitDetail.setCreateUser(logInfo.getStaffCode());
						
						debitDetail.setOrderType(saleOrder.getOrderType()); // lacnv1 - cap nhat them 1 so truong cho tablet dong bo
						debitDetail.setOrderDate(saleOrder.getOrderDate());
						
						debitDetail = debitDetailDAO.createDebitDetail(debitDetail);
					} else if (OrderType.CM.equals(saleOrder.getOrderType()) || (OrderType.CO.equals(saleOrder.getOrderType()) && SaleOrderSource.WEB.equals(saleOrder.getOrderSource()))) {
						/* cap nhap sale_order */
						checkOrder(saleOrder.getId(),CommonContanst.SALE_ORDER_APPROVED_STEP);
						saleOrder.setApproved(SaleOrderStatus.APPROVED);
						saleOrder.setApprovedDate(lockDate);
						saleOrder.setApprovedStep(SaleOrderStep.PRINT_CONFIRMED);
						if (OrderType.CM.equals(saleOrder.getOrderType())) {
							saleOrder.setOrderNumber(saleOrderDAO.getLastestOrderNumberAndUpdateByShop(saleOrder.getShop().getId(),OrderType.CM));
						} else if (OrderType.CO.equals(saleOrder.getOrderType())) {
							saleOrder.setOrderNumber(saleOrderDAO.getLastestOrderNumberAndUpdateByShop(saleOrder.getShop().getId(),OrderType.CO));
						}
						saleOrder.setUpdateUser(logInfo.getStaffCode());
						saleOrder.setUpdateDate(currentDate);
						saleOrderDAO.updateSaleOrder(saleOrder);
//						saleOrderDAO.updateLastestOrderNumberByShop(saleOrder.getShop().getId());
						
						/* cap nhap don bi tra */
						if (saleOrder.getFromSaleOrder() != null) {
							saleOrder.getFromSaleOrder().setType(SaleOrderType.RETURNED);
							saleOrder.getFromSaleOrder().setUpdateUser(logInfo.getStaffCode());
							saleOrder.getFromSaleOrder().setUpdateDate(currentDate);
							saleOrderDAO.updateSaleOrder(saleOrder.getFromSaleOrder());
						}
						
						/* cap nhap po_customer */
						if (SaleOrderSource.TABLET.equals(saleOrder.getOrderSource()) && saleOrder.getFromPoCustomerId() != null) {
							PoCustomer poCustomer = poCustomerDAO.getPoCustomerById(saleOrder.getFromPoCustomerId());
							if (poCustomer != null) {
								poCustomer.setApproved(SaleOrderStatus.APPROVED);
								//poCustomer.setOrderNumber(so.getOrderNumber());
								poCustomer.setUpdateUser(logInfo.getStaffCode());
								poCustomer.setUpdateDate(currentDate);
								poCustomerDAO.updatePoCustomer(poCustomer);
							}
						}
						
						/*
						 * update so xuat, so tien tra, so luong sp tra PROMOTION_SHOP_MAP, PROMOTION_CUSTOMER_MAP, PROMOTION_STAFF_MAP
						 */
						updatePromotionQuantityReceived(saleOrder, -1, currentDate, logInfo);
						updatePromotionAmountReceived(saleOrder, -1, currentDate, logInfo);
						updatePromotionNumReceived(saleOrder, -1, currentDate, logInfo);
						
						if(saleOrder.getAmount() != null && saleOrder.getAmount().compareTo(BigDecimal.ZERO) == 1) {
							Customer customer = customerDAO.getCustomerById(saleOrder.getCustomer().getId());
							
							SaleOrderFilter<SaleOrder> soFilter = new SaleOrderFilter<SaleOrder>();
							soFilter.setCustomerId(customer.getId());
							soFilter.setGetBothINandSO(true);
							soFilter.setLstApprovedStatus(Arrays.asList(SaleOrderStatus.APPROVED));
							soFilter.setAnchorDateDiff(saleOrder.getOrderDate());
							soFilter.setType(SaleOrderType.NOT_YET_RETURNED.getValue());
							soFilter.setDiffDaysFromDate(DEFAULT_NUM_DAY_ORDER_EXPIRE);
							soFilter.setIsPositiveAmountSaleOrder(true);
							soFilter.setShopId(saleOrder.getShop().getId());
							
							if (customer.getLastApproveOrder() != null
									&& DateUtility.compareDateWithoutTime(customer.getLastApproveOrder(), saleOrder.getOrderDate()) <= 0) {
								/*SaleOrder lastestOrder = saleOrderDAO.getLastestOrderWithException(so.getShop().getId(), so.getCustomer().getId()
																							, so.getFromSaleOrder().getId()
																							, Arrays.asList(OrderType.IN, OrderType.SO));*/
								List<SaleOrder> listSaleOrderWithoutStaffInfo = saleOrderDAO.getListSaleOrder(soFilter, null);
								
								if (listSaleOrderWithoutStaffInfo == null || listSaleOrderWithoutStaffInfo.size() == 0) {
									customer.setLastApproveOrder(null);
								} else {
									customer.setLastApproveOrder(listSaleOrderWithoutStaffInfo.get(0).getOrderDate());
								}
								customer.setUpdateUser(logInfo.getStaffCode());
								customer.setUpdateDate(currentDate);
								customerDAO.updateCustomer(customer, logInfo);
							}
							
							soFilter.setStaffId(saleOrder.getStaff().getId());
							List<SaleOrder> listSaleOrderWithStaffInfo = saleOrderDAO.getListSaleOrder(soFilter, null);
							StaffCustomer staffCustomer = staffCustomerDAO.getStaffCustomer(saleOrder.getStaff().getId(), saleOrder.getCustomer().getId());
							if (staffCustomer == null) {
								staffCustomer = new StaffCustomer();
								staffCustomer.setStaff(saleOrder.getStaff());
								staffCustomer.setShop(saleOrder.getShop());
								staffCustomer.setCustomer(saleOrder.getCustomer());
							}
							if (listSaleOrderWithStaffInfo == null || listSaleOrderWithStaffInfo.size() == 0) {
								staffCustomer.setLastApproveOrder(null);
							} else {
								staffCustomer.setLastApproveOrder(listSaleOrderWithStaffInfo.get(0).getOrderDate());
							}
							if (staffCustomer.getId() != null) {
								staffCustomerDAO.updateStaffCustomer(staffCustomer, logInfo);							
							} else {
								staffCustomerDAO.createStaffCustomer(staffCustomer, logInfo);
							}
							
							//Modified by NhanLT6 - 10/11/2014 - Cap nhat them truong last_approved_order de phuc vu 
							//dong bo du lieu voi sales_tablet
							//RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDate(so.getCustomer().getId(),so.getShop().getId(), so.getOrderDate());
							RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDateAndStaff(saleOrder.getCustomer().getId(),saleOrder.getShop().getId(), saleOrder.getOrderDate(), saleOrder.getStaff().getId());
							if(rc != null) {
								//rc.setLastApproveOrder(so.getOrderDate());
								if (listSaleOrderWithStaffInfo == null || listSaleOrderWithStaffInfo.size() == 0) {
									rc.setLastApproveOrder(null);
								} else {
									rc.setLastApproveOrder(listSaleOrderWithStaffInfo.get(0).getOrderDate());
								}
								routingCustomerDAO.updateRoutingCustomer(rc);
							}
						}
						
						Debit debit = debitDAO.getDebitForUpdate(saleOrder.getCustomer().getId(), DebitOwnerType.CUSTOMER);
						if (BigDecimal.ZERO.compareTo(saleOrder.getTotal()) >= 0) {
							//so.getTotal < 0
							debit.setTotalAmount(debit.getTotalAmount().add(saleOrder.getTotal()));
							debit.setTotalDebit(debit.getTotalDebit().add(saleOrder.getTotal()));
						} else {
							debit.setTotalAmount(debit.getTotalAmount().subtract(saleOrder.getTotal()));
							debit.setTotalDebit(debit.getTotalDebit().subtract(saleOrder.getTotal()));
						}
						debit.setMaxDebitAmount(saleOrder.getCustomer().getMaxDebitAmount());
						debit.setMaxDebitDate(saleOrder.getCustomer().getMaxDebitDate());
						debitDAO.updateDebit(debit);
						DebitDetail debitDetail = debitDetailDAO.getDebitDetailByFromObjectId(saleOrder.getId());
						if (debitDetail != null) {
							BusinessException e = new BusinessException("sale_order_exist_debit_detail");
							comfirmResult = ORDER_COMFIRMED_ALREADY;
							throw e;
						}
						debitDetail = new DebitDetail();
						debitDetail.setFromObjectId(saleOrder.getId());
						debitDetail.setFromObjectNumber(saleOrder.getOrderNumber());
						debitDetail.setCreateDate(lockDate);
						debitDetail.setAmount(saleOrder.getAmount().negate());
						debitDetail.setDiscount(saleOrder.getDiscount().negate());
						debitDetail.setTotal(saleOrder.getTotal().negate());
						debitDetail.setRemain(debitDetail.getTotal());
						debitDetail.setType(5);//giam no KH do tra hang
						debitDetail.setDebitDate(lockDate);
						debitDetail.setShop(saleOrder.getShop());
						debitDetail.setCustomer(saleOrder.getCustomer());
						debitDetail.setStaff(saleOrder.getStaff());
						debitDetail.setDebit(debit);
						debitDetail.setCreateDate(currentDate);
						debitDetail.setCreateUser(logInfo.getStaffCode());
						
						debitDetail.setOrderType(saleOrder.getOrderType()); // lacnv1 - cap nhat them 1 so truong cho tablet dong bo
						if (saleOrder.getFromSaleOrder() != null) {
							debitDetail.setOrderDate(saleOrder.getFromSaleOrder().getOrderDate());
						}
						debitDetail.setReturnDate(saleOrder.getOrderDate());
						
						debitDetail = debitDetailDAO.createDebitDetail(debitDetail);
						
						if (OrderType.CO.equals(saleOrder.getOrderType())) {
							// lacnv1 - debit_detail
							debitDetailDAO.cloneDebitDetailToTemp(debitDetail);
							// --
						}
						List<SaleOrder> listSaleOrder = new ArrayList<SaleOrder>();
						listSaleOrder.add(saleOrder);
						insertNewTable(listSaleOrder, ActionSaleOrder.APPROVED.getValue(), logInfo, -1);
					} else if (OrderType.SO.equals(saleOrder.getOrderType()) 
							|| (OrderType.CO.equals(saleOrder.getOrderType()) && SaleOrderSource.TABLET.equals(saleOrder.getOrderSource())) 
							|| OrderType.AI.equals(saleOrder.getOrderType()) || OrderType.AD.equals(saleOrder.getOrderType())) {
						/* update sale_order */
						checkOrder(saleOrder.getId(),CommonContanst.SALE_ORDER_APPROVED_STEP);
						saleOrder.setApproved(SaleOrderStatus.APPROVED);
						saleOrder.setApprovedDate(lockDate);
						saleOrder.setApprovedStep(SaleOrderStep.PRINT_CONFIRMED);
						int ration = 0;
						if (OrderType.SO.equals(saleOrder.getOrderType())) {
							saleOrder.setOrderNumber(saleOrderDAO.getLastestOrderNumberAndUpdateByShop(saleOrder.getShop().getId(),OrderType.SO));
							if (Constant.TWO_TEXT.equals(CONFIG_SYS_CAL_DATE)) {
								saleOrder.setAccountDate(lockDate);
							}
							ration = 1;
						} else if (OrderType.CO.equals(saleOrder.getOrderType())) {
							saleOrder.setOrderNumber(saleOrderDAO.getLastestOrderNumberAndUpdateByShop(saleOrder.getShop().getId(),OrderType.CO));
							ration = -1;
						}
						saleOrder.setUpdateUser(logInfo.getStaffCode());
						saleOrder.setUpdateDate(currentDate);
						saleOrderDAO.updateSaleOrder(saleOrder);
//						saleOrderDAO.updateLastestOrderNumberByShop(saleOrder.getShop().getId());
						/* update po_customer cho don SO va CO */
						if (OrderType.SO.equals(saleOrder.getOrderType()) || OrderType.CO.equals(saleOrder.getOrderType())) {
							if (saleOrder.getFromPoCustomerId() != null) {
								PoCustomer poCustomer = poCustomerDAO.getPoCustomerById(saleOrder.getFromPoCustomerId());
								if (poCustomer != null) {
									poCustomer.setApproved(SaleOrderStatus.APPROVED);
									if (OrderType.SO.equals(saleOrder.getOrderType())) {;
										if (Constant.TWO_TEXT.equals(CONFIG_SYS_CAL_DATE)) {
											poCustomer.setAccountDate(lockDate);
										}
									}
									//poCustomer.setOrderNumber(so.getOrderNumber());
									poCustomer.setUpdateUser(logInfo.getStaffCode());
									poCustomer.setUpdateDate(currentDate);
									poCustomerDAO.updatePoCustomer(poCustomer);
								}
							}
						}
						
						/*
						 * update so xuat, so tien tra, so luong sp tra PROMOTION_SHOP_MAP, PROMOTION_CUSTOMER_MAP, PROMOTION_STAFF_MAP
						 */
						if (OrderType.SO.equals(saleOrder.getOrderType()) || OrderType.CO.equals(saleOrder.getOrderType())) {
							updatePromotionQuantityReceived(saleOrder, ration, currentDate, logInfo);
							updatePromotionAmountReceived(saleOrder, ration, currentDate, logInfo);
							updatePromotionNumReceived(saleOrder, ration, currentDate, logInfo);
						}
						
						/* ghi nhan no cho don SO */
						if (OrderType.SO.equals(saleOrder.getOrderType()) || OrderType.CO.equals(saleOrder.getOrderType())) { // cap nhat lai so hoa don cho bang temp
							debitDetailDAO.updateOrderNumberForDebitDetailTemp(saleOrder);
						}
						if (OrderType.SO.equals(saleOrder.getOrderType())) {
							Debit debit = debitDAO.getDebitForUpdate(saleOrder.getCustomer().getId(), DebitOwnerType.CUSTOMER);
							if (debit == null) {
								debit = new Debit();
								debit.setObjectId(saleOrder.getCustomer().getId());
								debit.setObjectType(DebitOwnerType.CUSTOMER);
								debit.setTotalAmount(saleOrder.getTotal());
								debit.setTotalDebit(saleOrder.getTotal());
								debit.setMaxDebitAmount(saleOrder.getCustomer().getMaxDebitAmount());
								debit.setMaxDebitDate(saleOrder.getCustomer().getMaxDebitDate());
								debit.setTotalPay(BigDecimal.ZERO);
								debit.setTotalDiscount(BigDecimal.ZERO);
								debit = debitDAO.createDebit(debit);
							} else {
								debit.setTotalAmount(debit.getTotalAmount().add(saleOrder.getTotal()));
								debit.setTotalDebit(debit.getTotalDebit().add(saleOrder.getTotal()));
								debit.setMaxDebitAmount(saleOrder.getCustomer().getMaxDebitAmount());
								debit.setMaxDebitDate(saleOrder.getCustomer().getMaxDebitDate());
								debitDAO.updateDebit(debit);
							}
							DebitDetail debitDetail = debitDetailDAO.getDebitDetailByFromObjectId(saleOrder.getId());
							if (debitDetail != null) {
								BusinessException e = new BusinessException("sale_order_exist_debit_detail");
								comfirmResult = ORDER_COMFIRMED_ALREADY;
								throw e;
							}
							debitDetail = new DebitDetail();
							debitDetail.setFromObjectId(saleOrder.getId());
							debitDetail.setFromObjectNumber(saleOrder.getOrderNumber());
							debitDetail.setCreateDate(lockDate);
							debitDetail.setAmount(saleOrder.getAmount());
							debitDetail.setDiscount(saleOrder.getDiscount());
							debitDetail.setTotal(saleOrder.getTotal());
							debitDetail.setRemain(saleOrder.getTotal());
							debitDetail.setType(4);//co: tang no KH do ban hang
							debitDetail.setDebitDate(lockDate);
							debitDetail.setShop(saleOrder.getShop());
							debitDetail.setCustomer(saleOrder.getCustomer());
							;
							debitDetail.setStaff(saleOrder.getStaff());
							debitDetail.setDebit(debit);
							debitDetail.setCreateDate(currentDate);
							debitDetail.setCreateUser(logInfo.getStaffCode());
							
							debitDetail.setOrderType(saleOrder.getOrderType()); // lacnv1 - cap nhat them 1 so truong cho tablet dong bo
							debitDetail.setOrderDate(saleOrder.getOrderDate());
							
							debitDetail = debitDetailDAO.createDebitDetail(debitDetail);
							/*
							 * neu nhu [SALE_ORDER].AMOUNT > 0 thi cap nhat thong tin [CUSTOMER].LAST_APPROVED_ORDER, [STAFF_CUSTOMER].LAST_APPROVED_ORDER ve ngay chot 
							 */
							if(saleOrder.getAmount() != null && saleOrder.getAmount().compareTo(BigDecimal.ZERO) == 1) {
								Customer customer = customerDAO.getCustomerById(saleOrder.getCustomer().getId());
								customer.setLastApproveOrder(lockDate);
								customer.setUpdateUser(logInfo.getStaffCode());
								customer.setUpdateDate(currentDate);
								customerDAO.updateCustomer(customer, logInfo);
								
								StaffCustomer staffCustomer = staffCustomerDAO.getStaffCustomer(saleOrder.getStaff().getId(), saleOrder.getCustomer().getId());
								if (staffCustomer == null) {
									staffCustomer = new StaffCustomer();
									staffCustomer.setStaff(saleOrder.getStaff());
									staffCustomer.setCustomer(saleOrder.getCustomer());
									staffCustomer.setLastApproveOrder(lockDate);
									staffCustomer.setShop(saleOrder.getShop());
									staffCustomerDAO.createStaffCustomer(staffCustomer, logInfo);
								} else {
									staffCustomer.setLastApproveOrder(lockDate);
									staffCustomerDAO.updateStaffCustomer(staffCustomer, logInfo);
								}
								
								//RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDate(so.getCustomer().getId(),so.getShop().getId(), so.getOrderDate());
								RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDateAndStaff(saleOrder.getCustomer().getId(),saleOrder.getShop().getId(), saleOrder.getOrderDate(), saleOrder.getStaff().getId());
								if(rc != null) {
									rc.setLastApproveOrder(lockDate);
									routingCustomerDAO.updateRoutingCustomer(rc);
								}
							}
						} else if (OrderType.CO.equals(saleOrder.getOrderType())) {
							Debit debit = debitDAO.getDebitForUpdate(saleOrder.getCustomer().getId(), DebitOwnerType.CUSTOMER);
							if (BigDecimal.ZERO.compareTo(saleOrder.getTotal()) >= 0) {
								//so.getTotal < 0
								debit.setTotalAmount(debit.getTotalAmount().add(saleOrder.getTotal()));
								debit.setTotalDebit(debit.getTotalDebit().add(saleOrder.getTotal()));
							} else {
								debit.setTotalAmount(debit.getTotalAmount().subtract(saleOrder.getTotal()));
								debit.setTotalDebit(debit.getTotalDebit().subtract(saleOrder.getTotal()));
							}
							debit.setMaxDebitAmount(saleOrder.getCustomer().getMaxDebitAmount());
							debit.setMaxDebitDate(saleOrder.getCustomer().getMaxDebitDate());
							debitDAO.updateDebit(debit);
							DebitDetail debitDetail = debitDetailDAO.getDebitDetailByFromObjectId(saleOrder.getId());
							if (debitDetail != null) {
								BusinessException e = new BusinessException("sale_order_exist_debit_detail");
								comfirmResult = ORDER_COMFIRMED_ALREADY;
								throw e;
							}
							debitDetail = new DebitDetail();
							debitDetail.setFromObjectId(saleOrder.getId());
							debitDetail.setFromObjectNumber(saleOrder.getOrderNumber());
							debitDetail.setCreateDate(lockDate);
							debitDetail.setAmount(saleOrder.getAmount().negate());
							debitDetail.setDiscount(saleOrder.getDiscount().negate());
							debitDetail.setTotal(saleOrder.getTotal().negate());
							debitDetail.setRemain(debitDetail.getTotal());
							debitDetail.setType(5);//giam no KH do tra hang
							debitDetail.setDebitDate(lockDate);
							debitDetail.setShop(saleOrder.getShop());
							debitDetail.setCustomer(saleOrder.getCustomer());
							;
							debitDetail.setStaff(saleOrder.getStaff());
							debitDetail.setDebit(debit);
							debitDetail.setCreateDate(currentDate);
							debitDetail.setCreateUser(logInfo.getStaffCode());
							
							debitDetail.setOrderType(saleOrder.getOrderType()); // lacnv1 - cap nhat them 1 so truong cho tablet dong bo
							if (saleOrder.getFromSaleOrder() != null) {
								debitDetail.setOrderDate(saleOrder.getFromSaleOrder().getOrderDate());
							}
							debitDetail.setReturnDate(saleOrder.getOrderDate());
							
							debitDetail = debitDetailDAO.createDebitDetail(debitDetail);
							
							/*
							 * cap nhat don hang cuoi cung
							 */
							if(!BigDecimal.ZERO.equals(saleOrder.getAmount()) && saleOrder.getAmount() != null) {
								Customer customer = customerDAO.getCustomerById(saleOrder.getCustomer().getId());
								
								SaleOrderFilter<SaleOrder> soFilter = new SaleOrderFilter<SaleOrder>();
								soFilter.setCustomerId(customer.getId());
								soFilter.setGetBothINandSO(true);
								soFilter.setLstApprovedStatus(Arrays.asList(SaleOrderStatus.APPROVED));
								soFilter.setAnchorDateDiff(saleOrder.getOrderDate());
								soFilter.setType(SaleOrderType.NOT_YET_RETURNED.getValue());
								soFilter.setDiffDaysFromDate(DEFAULT_NUM_DAY_ORDER_EXPIRE);
								soFilter.setIsPositiveAmountSaleOrder(true);
								soFilter.setShopId(saleOrder.getShop().getId());
								
								if (customer.getLastApproveOrder() != null
										&& DateUtility.compareDateWithoutTime(customer.getLastApproveOrder(), saleOrder.getOrderDate()) <= 0) {
									//SaleOrder lastestOrder = saleOrderDAO.getLastestINOrderWithException(so.getShop().getId(), so.getCustomer().getId(), so.getFromSaleOrder().getId());
									List<SaleOrder> listSaleOrderWithoutStaffInfo = saleOrderDAO.getListSaleOrder(soFilter, null);
									if (listSaleOrderWithoutStaffInfo == null || listSaleOrderWithoutStaffInfo.size() == 0) {
										customer.setLastApproveOrder(null);
									} else {
										customer.setLastApproveOrder(listSaleOrderWithoutStaffInfo.get(0).getOrderDate());
									}
									customer.setUpdateUser(logInfo.getStaffCode());
									customer.setUpdateDate(currentDate);
									customerDAO.updateCustomer(customer, logInfo);
								}
								
								soFilter.setStaffId(saleOrder.getStaff().getId());
								List<SaleOrder> listSaleOrderWithStaffInfo = saleOrderDAO.getListSaleOrder(soFilter, null);
								StaffCustomer staffCustomer = staffCustomerDAO.getStaffCustomer(saleOrder.getStaff().getId(), saleOrder.getCustomer().getId());
								if (staffCustomer == null) {
									staffCustomer = new StaffCustomer();
									staffCustomer.setStaff(saleOrder.getStaff());
									staffCustomer.setCustomer(saleOrder.getCustomer());
									staffCustomer.setShop(saleOrder.getShop());
								}
								if (listSaleOrderWithStaffInfo == null || listSaleOrderWithStaffInfo.size() == 0) {
									staffCustomer.setLastApproveOrder(null);
								} else {
									staffCustomer.setLastApproveOrder(listSaleOrderWithStaffInfo.get(0).getOrderDate());
								}
								if (staffCustomer.getId() != null) {
									staffCustomerDAO.updateStaffCustomer(staffCustomer, logInfo);							
								} else {
									staffCustomerDAO.createStaffCustomer(staffCustomer, logInfo);
								}
								
								//Modified by NhanLT6 - 10/11/2014 - Cap nhat them truong last_approved_order de phuc vu 
								//dong bo du lieu voi sales_tablet
								//RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDate(so.getCustomer().getId(),so.getShop().getId(), so.getOrderDate());
								RoutingCustomer rc = routingCustomerDAO.getRoutingCustomerByDateAndStaff(saleOrder.getCustomer().getId(),saleOrder.getShop().getId(), saleOrder.getOrderDate(), saleOrder.getStaff().getId());
								if(rc != null) {
									if (listSaleOrderWithStaffInfo == null || listSaleOrderWithStaffInfo.size() == 0) {
										rc.setLastApproveOrder(null);
									} else {
										rc.setLastApproveOrder(listSaleOrderWithStaffInfo.get(0).getOrderDate());
									}
									routingCustomerDAO.updateRoutingCustomer(rc);
								}
							}
						}
					}
					//tra thuong keyshop
					updateRewardKeyShop(saleOrder, currentDate, logInfo);
					//tra hang keyshop
					if (IS_RETURN_KEY_SHOP) {
						returnRewardKeyShop(saleOrder, currentDate, logInfo);
					}
					// cap nhat kho
					if (isUpdateStock) {
						String orderType = "";
						if (saleOrder.getOrderType() != null) {
							orderType = saleOrder.getOrderType().getValue();
						}

						List<SaleOrderStockDetailVO> listDetail = saleOrderDAO.getListSaleOrderStockDetail(saleOrder.getId(), orderType, true);
						if (listDetail.size() > 0) {
							SaleOrderStockVOTemp orderVO = new SaleOrderStockVOTemp();
							orderVO.setId(saleOrder.getId());
							orderVO.setOrderNumber(saleOrder.getOrderNumber());
							orderVO.setOrderSource(saleOrder.getOrderSource() == null ? null : saleOrder.getOrderSource().getValue());
							orderVO.setOrderType(orderType);
							orderVO.setShopId(saleOrder.getShop() == null ? null : saleOrder.getShop().getId());
							orderVO.setStaffId(saleOrder.getStaff() == null ? null : saleOrder.getStaff().getId());
							updateStockCommon(listDetail, orderVO, logInfo.getStaffCode(), currentDate, logInfo);
						}
					}
					
					comfirmResult = ORDER_COMFIRMED_SUCCESS;
					
				} catch (Exception e) {
					String s = e.getMessage();
					//xu ly loi tra thuong keyshop updateRewardKeyShop
					if (s.equals("SALE_ORDER_OVER_REWARD_PRODUCT")) {
						comfirmResult = SALE_ORDER_OVER_REWARD_PRODUCT;
					} else if(s.equals("SALE_ORDER_OVER_REWARD_MONEY")) {
						comfirmResult = SALE_ORDER_OVER_REWARD_MONEY;
					}
					// xu ly loi tu cap nhat kho updateStockCommon
					else if (s.startsWith("KHONG_DU_SOLUONG_KHO_NV_VANSALE-") || s.startsWith("KHONG_DU_SOLUONG_KHO_NPP-")) {
						comfirmResult = SALE_ORDER_OVER_QUANTITY_STOCK_TOTAL;
					}
					if (comfirmResult == null) {	// system error
						comfirmResult = ORDER_CONFIRM_ERROR;
					}
					LogUtility.logError(e, "sessionId = " + logInfo.getSessionId() + ";confirm one order at Print Step. sale_order_id = " + saleOrder.getId() + "; " + e.getMessage());
					transactionStatus.setRollbackOnly();
				}
				return comfirmResult;
			}
		});
		return comfirmResult;
	}
	
	/*
	 * update so xuat PROMOTION_SHOP_MAP, PROMOTION_CUSTOMER_MAP, PROMOTION_STAFF_MAP
	 */
	private void updatePromotionQuantityReceived(SaleOrder saleOrder, int ration, Date currentDate, LogInfoVO logInfo) throws DataAccessException {
		List<SaleOrderPromotionVO> checkReceived = saleOrderDAO.getListSOPromotionBySOId(saleOrder.getId());
		for (SaleOrderPromotionVO vo : checkReceived) {
			PromotionShopMap psm = promotionShopMapDAO.getPromotionParentShopMap(vo.getPromotionId(), saleOrder.getShop().getId(), ActiveType.RUNNING.getValue());
			if (psm != null) {
				if (psm.getQuantityReceived() == null) {
					psm.setQuantityReceived(ration < 0 ? 0 : vo.getQuantity());
				} else {
					psm.setQuantityReceived(psm.getQuantityReceived() + vo.getQuantity() * ration);
				}
				psm.setUpdateUser(logInfo.getStaffCode());
				psm.setUpdateDate(currentDate);
				promotionShopMapDAO.updatePromotionShopMap(psm, logInfo);

				PromotionCustomerMap pcm = promotionCustomerMapDAO.getPromotionCustomerMap(psm.getId(), saleOrder.getCustomer().getId());
				if (pcm != null) {
					if (pcm.getQuantityReceived() == null) {
						pcm.setQuantityReceived(ration < 0 ? 0 : vo.getQuantity());
					} else {
						pcm.setQuantityReceived(pcm.getQuantityReceived() + vo.getQuantity() * ration);
					}
					pcm.setUpdateUser(logInfo.getStaffCode());
					pcm.setUpdateDate(currentDate);
					promotionCustomerMapDAO.updatePromotionCustomerMap(pcm, logInfo);
				}

				PromotionStaffMap pstm = promotionStaffMap.getPromotionStaffMapByShopMapAndStaff(psm.getId(), saleOrder.getStaff().getId());
				if (pstm != null) {
					if (pstm.getQuantityReceived() == null) {
						pstm.setQuantityReceived(ration < 0 ? 0 : vo.getQuantity());
					} else {
						pstm.setQuantityReceived(pstm.getQuantityReceived() + vo.getQuantity() * ration);
					}
					pstm.setUpdateUser(logInfo.getStaffCode());
					pstm.setUpdateDate(currentDate);
					promotionStaffMap.updatePromotionStaffMap(pstm, logInfo);
				}
			}
		}
	}
	
	//cap nhat so tien tra khuyen mai
	private void updatePromotionAmountReceived(SaleOrder saleOrder, int ration, Date currentDate, LogInfoVO logInfo) throws DataAccessException {
		SaleOrderPromoDetailFilter filter = new SaleOrderPromoDetailFilter();
		filter.setSaleOrderId(saleOrder.getId());
		filter.setIsNotKeyShop(true);
		List<SaleOrderPromotionDetailVO> lstSOPromoDetail = saleOrderPromoDetailDAO.getListSaleOrderPromoByFilter(filter);
		if (lstSOPromoDetail != null && lstSOPromoDetail.size() > 0) {
			for (int i = 0, n = lstSOPromoDetail.size(); i < n; i++) {
				SaleOrderPromotionDetailVO vo = lstSOPromoDetail.get(i);
				if (!StringUtility.isNullOrEmpty(vo.getProgramCode()) && vo.getDiscountAmount() != null) {
					PromotionProgram promotionProgram = promotionProgramDAO.getPromotionProgramByCode(vo.getProgramCode());
					if (promotionProgram != null) {
						PromotionShopMap psm = promotionShopMapDAO.getPromotionParentShopMap(promotionProgram.getId(), saleOrder.getShop().getId(), ActiveType.RUNNING.getValue());
						if (psm != null) {
							if (psm.getAmountReceived() == null) {
								psm.setAmountReceived(ration < 0 ? BigDecimal.ZERO : vo.getDiscountAmount());
							} else {
								psm.setAmountReceived(psm.getAmountReceived().add(vo.getDiscountAmount().multiply(BigDecimal.valueOf(ration))));
							}
							psm.setUpdateUser(logInfo.getStaffCode());
							psm.setUpdateDate(currentDate);
							promotionShopMapDAO.updatePromotionShopMap(psm, logInfo);

							PromotionCustomerMap pcm = promotionCustomerMapDAO.getPromotionCustomerMap(psm.getId(), saleOrder.getCustomer().getId());
							if (pcm != null) {
								if (pcm.getAmountReceived() == null) {
									pcm.setAmountReceived(ration < 0 ? BigDecimal.ZERO : vo.getDiscountAmount());
								} else {
									pcm.setAmountReceived(pcm.getAmountReceived().add(vo.getDiscountAmount().multiply(BigDecimal.valueOf(ration))));
								}
								pcm.setUpdateUser(logInfo.getStaffCode());
								pcm.setUpdateDate(currentDate);
								promotionCustomerMapDAO.updatePromotionCustomerMap(pcm, logInfo);
							}

							PromotionStaffMap pstm = promotionStaffMap.getPromotionStaffMapByShopMapAndStaff(psm.getId(), saleOrder.getStaff().getId());
							if (pstm != null) {
								if (pstm.getAmountReceived() == null) {
									pstm.setAmountReceived(ration < 0 ? BigDecimal.ZERO : vo.getDiscountAmount());
								} else {
									pstm.setAmountReceived(pstm.getAmountReceived().add(vo.getDiscountAmount().multiply(BigDecimal.valueOf(ration))));
								}
								pstm.setUpdateUser(logInfo.getStaffCode());
								pstm.setUpdateDate(currentDate);
								promotionStaffMap.updatePromotionStaffMap(pstm, logInfo);
							}
						}
					}
				}
			}
		}
	}
	
	//cap nhat so luong tra khuyen mai
	private void updatePromotionNumReceived(SaleOrder saleOrder, int ration, Date currentDate, LogInfoVO logInfo) throws DataAccessException {
		@SuppressWarnings("rawtypes")
		SaleOrderDetailFilter filter = new SaleOrderDetailFilter();
		filter.setSaleOrderId(saleOrder.getId());
		filter.setIsNotKeyShop(true);
		List<SaleOrderDetailVO2> lstSOPromoDetail = saleOrderDetailDAO.getListDiscountAmountByProgramCode(filter);
		if (lstSOPromoDetail != null && lstSOPromoDetail.size() > 0) {
			for (int i = 0, n = lstSOPromoDetail.size(); i < n; i++) {
				SaleOrderDetailVO2 vo = lstSOPromoDetail.get(i);
				if (!StringUtility.isNullOrEmpty(vo.getProgramCode()) && vo.getQuantity() != null) {
					PromotionProgram promotionProgram = promotionProgramDAO.getPromotionProgramByCode(vo.getProgramCode());
					if (promotionProgram != null) {
						PromotionShopMap psm = promotionShopMapDAO.getPromotionParentShopMap(promotionProgram.getId(), saleOrder.getShop().getId(), ActiveType.RUNNING.getValue());
						if (psm != null) {
							if (psm.getNumReceived() == null) {
								psm.setNumReceived(BigDecimal.valueOf(ration < 0 ? 0 : vo.getQuantity()));
							} else {
								psm.setNumReceived(psm.getNumReceived().add(BigDecimal.valueOf(vo.getQuantity() * ration)));
							}
							psm.setUpdateUser(logInfo.getStaffCode());
							psm.setUpdateDate(currentDate);
							promotionShopMapDAO.updatePromotionShopMap(psm, logInfo);

							PromotionCustomerMap pcm = promotionCustomerMapDAO.getPromotionCustomerMap(psm.getId(), saleOrder.getCustomer().getId());
							if (pcm != null) {
								if (pcm.getNumReceived() == null) {
									pcm.setNumReceived(BigDecimal.valueOf(ration < 0 ? 0 : vo.getQuantity()));
								} else {
									pcm.setNumReceived(pcm.getNumReceived().add(BigDecimal.valueOf(vo.getQuantity() * ration)));
								}
								pcm.setUpdateUser(logInfo.getStaffCode());
								pcm.setUpdateDate(currentDate);
								promotionCustomerMapDAO.updatePromotionCustomerMap(pcm, logInfo);
							}

							PromotionStaffMap pstm = promotionStaffMap.getPromotionStaffMapByShopMapAndStaff(psm.getId(), saleOrder.getStaff().getId());
							if (pstm != null) {
								if (pstm.getNumReceived() == null) {
									pstm.setNumReceived(BigDecimal.valueOf(ration < 0 ? 0 : vo.getQuantity()));
								} else {
									pstm.setNumReceived(pstm.getNumReceived().add(BigDecimal.valueOf(vo.getQuantity() * ration)));
								}
								pstm.setUpdateUser(logInfo.getStaffCode());
								pstm.setUpdateDate(currentDate);
								promotionStaffMap.updatePromotionStaffMap(pstm, logInfo);
							}
						}
					}
				}
			}
		}
	}

	@Override
	public ObjectVO<PrintOrderVO> getListPrintOrderVO(PrintOrderFilter filter) throws BusinessException {
		try {
			List<PrintOrderVO> lst = saleOrderDAO.getListPrintOrderVO(filter);
			ObjectVO<PrintOrderVO> vo = new ObjectVO<PrintOrderVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<SaleOrderDetailVOEx> getListSaleOrderDetailVOByFilter(SaleOrderDetailFilter<SaleOrderDetailVOEx> filter) throws BusinessException {
		try {
			List<SaleOrderDetailVOEx> lst = saleOrderDAO.getListSaleOrderDetailVOByFilter(filter);
			ObjectVO<SaleOrderDetailVOEx> vo = new ObjectVO<SaleOrderDetailVOEx>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public SaleOrderVO getSaleOrderVOSingle(SaleOrderFilter<SaleOrderVO> filter) throws BusinessException {
		try {
			return saleOrderDAO.getSaleOrderVOSingle(filter);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleOrderDetail> getListSaleOrderDetailByListSaleOrderId(SaleOrderDetailFilter<SaleOrderDetail> filter) throws BusinessException {
		try {
			List<SaleOrderDetail> lst = saleOrderDAO.getListSaleOrderDetailByListSaleOrderId(filter.getArrLongId(), IsFreeItemInSaleOderDetail.SALE.getValue(), filter.getFlag());
			ObjectVO<SaleOrderDetail> vo = new ObjectVO<SaleOrderDetail>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateSaleOrderAIAD(SaleOrder saleOrder, String arrStrInsertData, List<Long> arrStrDeleteData, String arrStrUpdateData) throws BusinessException {
		try {
			//Thuc hien xoa, them, sua Saleorder Detail AI, AD
			List<SaleOrderDetail> lsSodNew = new ArrayList<SaleOrderDetail>();
			List<SaleOrderDetail> lsSodUpd = new ArrayList<SaleOrderDetail>();
			List<SaleOrderDetail> lsDel = new ArrayList<SaleOrderDetail>();
			if (!StringUtility.isNullOrEmpty(arrStrInsertData) || !StringUtility.isNullOrEmpty(arrStrUpdateData) || !arrStrDeleteData.isEmpty()) {
				List<Long> arrId = new ArrayList<Long>();
				arrId.add(saleOrder.getFromSaleOrder().getId());
				List<SaleOrderDetail> lstIsJoinSODetail = saleOrderDAO.getListSaleOrderDetailByListSaleOrderId(arrId, IsFreeItemInSaleOderDetail.SALE.getValue(), false);
				if (lstIsJoinSODetail != null && !lstIsJoinSODetail.isEmpty()) {
					arrId = new ArrayList<Long>();
					arrId.add(saleOrder.getId());
					List<SaleOrderDetail> lstSODetail = saleOrderDAO.getListSaleOrderDetailByListSaleOrderId(arrId, IsFreeItemInSaleOderDetail.SALE.getValue(), false);
					//Xu ly xoa
					if (arrStrDeleteData != null && !arrStrDeleteData.isEmpty()) {
						for (SaleOrderDetail isDetail : lstSODetail) {
							lsDel.add(isDetail);
						}
					}
					if (lsDel != null && !lsDel.isEmpty()) {
						saleOrderDetailDAO.deleteSaleOrderDetail(lsDel);
					}
					//Xu ly them
					String[] arrInsertData = arrStrInsertData.split("ESC");
					if (arrInsertData.length > 0) {
						List<String> lstPrCode = new ArrayList<String>();
						for (int i = 0; i < arrInsertData.length; i++) {
							String[] arrValue = arrInsertData[i].split("_");
							if (arrValue.length > 1 && lstPrCode.indexOf(arrValue[0].trim().toUpperCase()) < 0) {
								for (SaleOrderDetail isJoinDT : lstIsJoinSODetail) {
									if (!StringUtility.isNullOrEmpty(arrValue[0]) && isJoinDT.getProduct().getProductCode().trim().toUpperCase().equals(arrValue[0].trim().toUpperCase())) {
										//Insert
										SaleOrderDetail saleOrderDetailNew = isJoinDT.clone();
										saleOrderDetailNew.setId(null);
										saleOrderDetailNew.setSaleOrder(saleOrder);
										saleOrderDetailNew.setUpdateDate(null);
										saleOrderDetailNew.setUpdateUser(null);
										saleOrderDetailNew.setUpdateDate(new Date());
										saleOrderDetailNew.setUpdateUser(saleOrder.getCreateUser());
										if (!StringUtility.isNullOrEmpty(arrValue[1])) {
											Double price = Double.parseDouble("0");
											try {
												price = Double.parseDouble(arrValue[1]);
											} catch (Exception e) {
												price = Double.parseDouble("0");
											}
											if (!saleOrderDetailNew.getPriceValue().equals(BigDecimal.valueOf(price))) {
												saleOrderDetailNew.setPriceValue(BigDecimal.valueOf(price));
												saleOrderDetailNew.setAmount(BigDecimal.valueOf(price * saleOrderDetailNew.getQuantity().doubleValue()));
											}
										}
										lsSodNew.add(saleOrderDetailNew);
									}
								}
								lstPrCode.add(arrValue[0].trim().toUpperCase());
							}
						}
					}
					if (lsSodNew != null && !lsSodNew.isEmpty()) {
						saleOrderDetailDAO.createSaleOrderDetail(lsSodNew);
					}
					//Xu ly sua
					String[] arrUpdateData = arrStrUpdateData.split("ESC");
					if (arrUpdateData.length > 0) {
						for (int i = 0; i < arrUpdateData.length; i++) {
							String[] arrValue = arrUpdateData[i].split("_");
							if (arrValue.length > 1) {
								for (SaleOrderDetail isJoinDT : lstSODetail) {
									if (!StringUtility.isNullOrEmpty(arrValue[0]) && isJoinDT.getProduct().getProductCode().trim().toUpperCase().equals(arrValue[0].trim().toUpperCase())) {
										SaleOrderDetail saleOrderDetailUpd = isJoinDT.clone();
										if (!StringUtility.isNullOrEmpty(arrValue[1])) {
											Double price = Double.parseDouble("0");
											try {
												price = Double.parseDouble(arrValue[1]);
											} catch (Exception e) {
												price = Double.parseDouble("0");
											}
											if (!saleOrderDetailUpd.getPriceValue().equals(BigDecimal.valueOf(price))) {
												saleOrderDetailUpd.setPriceValue(BigDecimal.valueOf(price));
												saleOrderDetailUpd.setAmount(BigDecimal.valueOf(price * saleOrderDetailUpd.getQuantity().doubleValue()));
												saleOrderDetailUpd.setUpdateUser(saleOrder.getUpdateUser());
												lsSodUpd.add(saleOrderDetailUpd);
											}
										}
									}
								}
							}
						}
					}
					if (lsSodUpd != null && !lsSodUpd.isEmpty()) {
						saleOrderDetailDAO.updateSaleOrderDetail(lsSodUpd);
					}

					BigDecimal amount = new BigDecimal(0);
					for (SaleOrderDetail saleOrderDetailNew : lsSodNew) {
						if (saleOrderDetailNew.getAmount() != null) {
							amount = amount.add(saleOrderDetailNew.getAmount());
						}
					}
					for (SaleOrderDetail saleOrderDetailNew : lsSodUpd) {
						if (saleOrderDetailNew.getAmount() != null) {
							amount = amount.add(saleOrderDetailNew.getAmount());
						}
					}
					saleOrder.setAmount(amount);
					saleOrder.setTotal(amount);
				}
			}
			saleOrderDAO.updateSaleOrder(saleOrder);

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Long createSaleOrderAIAD(SaleOrder saleOrder, String arrStrInsertData) throws BusinessException, DataFormatException {
		try {
			//Thuc hien cap nhat Saleorder AI, AD
			SaleOrder saleOrderNew = saleOrderDAO.createSaleOrder(saleOrder);

			List<SaleOrderDetail> lsSodNew = new ArrayList<SaleOrderDetail>();
			if (!StringUtility.isNullOrEmpty(arrStrInsertData)) {
				List<Long> lstSaleOrderId = new ArrayList<Long>();
				lstSaleOrderId.add(saleOrder.getFromSaleOrder().getId());
				List<SaleOrderDetail> lstIsJoinSODetail = saleOrderDAO.getListSaleOrderDetailByListSaleOrderId(lstSaleOrderId, IsFreeItemInSaleOderDetail.SALE.getValue(), false);
				if (lstIsJoinSODetail != null && !lstIsJoinSODetail.isEmpty()) {
					String[] arrInsertData = arrStrInsertData.split("ESC");
					if (arrInsertData.length > 0) {
						for (int i = 0; i < arrInsertData.length; i++) {
							String[] arrValue = arrInsertData[i].split("_");
							if (arrValue.length > 1) {
								for (SaleOrderDetail isJoinDT : lstIsJoinSODetail) {
									if (!StringUtility.isNullOrEmpty(arrValue[0]) && isJoinDT.getProduct().getProductCode().trim().toUpperCase().equals(arrValue[0].trim().toUpperCase())) {
										SaleOrderDetail saleOrderDetailNew = isJoinDT.clone();
										saleOrderDetailNew.setId(null);
										saleOrderDetailNew.setSaleOrder(saleOrderNew);
										saleOrderDetailNew.setUpdateDate(null);
										saleOrderDetailNew.setUpdateUser(null);
										saleOrderDetailNew.setCreateDate(new Date());
										saleOrderDetailNew.setCreateUser(saleOrder.getCreateUser());
										saleOrderDetailNew.setDiscountAmount(null);
										if (!StringUtility.isNullOrEmpty(arrValue[1])) {
											Double price = Double.valueOf(0);
											try {
												price = Double.parseDouble(arrValue[1]);
											} catch (Exception e) {
												price = Double.valueOf(0);
											}
											if (!saleOrderDetailNew.getPriceValue().equals(BigDecimal.valueOf(price))) {
												saleOrderDetailNew.setPriceValue(BigDecimal.valueOf(price));
												saleOrderDetailNew.setAmount(BigDecimal.valueOf(price * saleOrderDetailNew.getQuantity().doubleValue()));
											}
										}
										lsSodNew.add(saleOrderDetailNew);
									}
								}
							}
						}
					}
				}
			}
			if (lsSodNew != null && !lsSodNew.isEmpty()) {
				saleOrderDetailDAO.createSaleOrderDetail(lsSodNew);
			}
			BigDecimal amount = new BigDecimal(0);
			for (SaleOrderDetail saleOrderDetailNew : lsSodNew) {
				if (saleOrderDetailNew.getAmount() != null) {
					amount = amount.add(saleOrderDetailNew.getAmount());
				}
			}
			saleOrderNew.setAmount(amount);
			saleOrderNew.setTotal(amount);
			saleOrderDAO.updateSaleOrder(saleOrderNew);
			return saleOrderNew.getId();
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void cancelWebOrder(long orderId, LogInfoVO logInfo) throws BusinessException {
		try {
			SaleOrder saleOrder = saleOrderDAO.getSaleOrderById(orderId);
			if (saleOrder == null || logInfo == null) {
				throw new IllegalArgumentException("invalid parameters");
			}
			Date databaseSysdate = commonDAO.getSysDate();
			Customer customer = saleOrder.getCustomer();
			Staff staff = saleOrder.getStaff();
			if (customer == null || staff == null) {
				throw new IllegalArgumentException("sale.product.print.order.cancel.order.order.invalid" + "_" + saleOrder.getOrderNumber());
			}

			saleOrder.setApproved(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM);
			saleOrder.setUpdateUser(logInfo.getStaffCode());
			saleOrder.setUpdateDate(databaseSysdate);
			saleOrderDAO.updateSaleOrder(saleOrder);
			
			//TODO: begin vuongmq; 20/01/2015; them dong bang: promotion_map_delta; gia tri *(-1) neu update
			List<SaleOrderPromotion> lstSOP = saleOrderDAO.getListSaleOrderPromotionBySaleOrder(saleOrder.getId());	
			// sale_order_promotion; insert dong am, them dong cho tao update sale order
			if (lstSOP != null && !lstSOP.isEmpty()) {
				for (SaleOrderPromotion vo : lstSOP) {
					saleOrderDAO.createPromotionMapDelta(saleOrder, vo, ActionSaleOrder.UPDATE, true);
				}
			}
			//TODO: End vuongmq; 20/01/2015; them dong bang: promotion_map_delta; gia tri *(-1) neu update
			PoCustomer poCustomer = poCustomerDAO.getPoCustomerByFromSaleorderId(saleOrder.getId());
			if (poCustomer != null) {
				poCustomer.setApproved(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM);
				poCustomer.setUpdateDate(databaseSysdate);
				poCustomer.setUpdateUser(logInfo.getStaffCode());
				poCustomerDAO.updatePoCustomer(poCustomer);
			}

			if (saleOrder.getAmount() != null && saleOrder.getAmount().compareTo(BigDecimal.ZERO) == 1 && saleOrder.getShop() != null) {
				SaleOrderFilter<SaleOrder> soFilter = new SaleOrderFilter<SaleOrder>();
				soFilter.setCustomerId(customer.getId());
				soFilter.setGetBothINandSO(true);
				soFilter.setLstApprovedStatus(Arrays.asList(SaleOrderStatus.NOT_YET_APPROVE, SaleOrderStatus.APPROVED));
				soFilter.setType(SaleOrderType.NOT_YET_RETURNED.getValue());
				soFilter.setAnchorDateDiff(saleOrder.getOrderDate());
				soFilter.setDiffDaysFromDate(getNumDaySaleOrderExpire());
				soFilter.setIsPositiveAmountSaleOrder(true);
				soFilter.setShopId(saleOrder.getShop().getId());

				if (customer.getLastOrder() != null && saleOrder.getOrderDate() != null && DateUtility.compareDateWithoutTime(customer.getLastOrder(), saleOrder.getOrderDate()) <= 0) {
					/*
					 * customer information
					 */
					List<SaleOrder> listSaleOrderWithoutStaffInfo = saleOrderDAO.getListSaleOrder(soFilter, null);
					if (listSaleOrderWithoutStaffInfo == null || listSaleOrderWithoutStaffInfo.size() == 0) {
						customer.setLastOrder(null);
					} else {
						customer.setLastOrder(listSaleOrderWithoutStaffInfo.get(0).getOrderDate());
					}
					customer.setUpdateDate(databaseSysdate);
					customer.setUpdateUser(logInfo.getStaffCode());
					customerDAO.updateCustomer(customer, logInfo);
				}

				/*
				 * staff_customer information
				 */
				soFilter.setStaffId(staff.getId());
				List<SaleOrder> listSaleOrderWithStaffInfo = saleOrderDAO.getListSaleOrder(soFilter, null);
				StaffCustomer staffCustomer = staffCustomerDAO.getStaffCustomer(staff.getId(), customer.getId());
				if (staffCustomer == null) {
					staffCustomer = new StaffCustomer();
					staffCustomer.setCustomer(customer);
					staffCustomer.setStaff(staff);
					staffCustomer.setShop(saleOrder.getShop());
				}
				if (listSaleOrderWithStaffInfo == null || listSaleOrderWithStaffInfo.size() == 0) {
					staffCustomer.setLastOrder(null);
				} else {
					staffCustomer.setLastOrder(listSaleOrderWithStaffInfo.get(0).getOrderDate());
				}
				if (staffCustomer.getId() != null) {
					staffCustomerDAO.updateStaffCustomer(staffCustomer, logInfo);
				} else {
					staffCustomerDAO.createStaffCustomer(staffCustomer, logInfo);
				}

				/*
				 * update routing customer
				 */
				RoutingCustomer routingCustomer = routingCustomerDAO.getRoutingCustomerByDateAndStaff(customer.getId(), saleOrder.getShop().getId(), saleOrder.getOrderDate(), staff.getId());
				if (routingCustomer != null) {
					if (listSaleOrderWithStaffInfo == null || listSaleOrderWithStaffInfo.size() == 0) {
						routingCustomer.setLastOrder(null);
					} else {
						routingCustomer.setLastOrder(listSaleOrderWithStaffInfo.get(0).getOrderDate());
					}
					routingCustomer.setUpdateDate(databaseSysdate);
					routingCustomer.setUpdateUser(logInfo.getStaffCode());
					routingCustomerDAO.updateRoutingCustomer(routingCustomer);
				}
			}

			// Lay danh sach chi tiet don hang
			List<SaleOrderDetail> lstSaleOrderDetail = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(saleOrder.getId(), null);
			for (SaleOrderDetail sod : lstSaleOrderDetail) {
				if (sod.getProduct() != null && sod.getQuantity() != null) {
					List<SaleOrderLot> lstSaleOrderLot1 = saleOrderLotDAO.getListSaleOrderLotBySaleOrderDetail(null, sod.getId());
					for (SaleOrderLot sol : lstSaleOrderLot1) {
						if (sol.getWarehouse() != null) {
							StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(sod.getProduct().getId(), StockObjectType.SHOP, sod.getShop().getId(), sol.getWarehouse().getId());
							if (stockTotal != null) {
								/** Log conent */
								try {
									logInfo.getContenFormat(sod.getProduct().getId(), sol.getWarehouse().getId(), sod.getQuantity(), 
											stockTotal.getAvailableQuantity(), stockTotal.getAvailableQuantity() + sol.getQuantity(), 
											stockTotal.getQuantity(), stockTotal.getQuantity());
								} catch (Exception e) {
								}
								/** End log conent */
								
								stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() + sol.getQuantity());
								stockTotal.setUpdateDate(databaseSysdate);
								stockTotal.setUpdateUser(logInfo.getStaffCode());
								stockTotalDAO.updateStockTotal(stockTotal); // Cap nhat TON KHO DAP UNG trong StockTotal
							}
						}
					}
				}
			}

			// cap nhat bang routing_customer
			Staff st = null;
			if (saleOrder.getCustomer() != null && saleOrder.getShop() != null) {
				RoutingCustomerFilter filter = new RoutingCustomerFilter();
				filter.setCustomerId(saleOrder.getCustomer().getId());
				filter.setShopId(saleOrder.getShop().getId());
				filter.setOrderDate(saleOrder.getOrderDate());
				List<Staff> lstS = staffDAO.getSaleStaffByDate(filter);
				if (lstS != null && lstS.size() > 0) {
					st = lstS.get(0);
				}
			}
			if (saleOrder.getStaff() != null && st != null && saleOrder.getStaff().getId().equals(st.getId())) {
				st.setLastOrder(null);
				st.setUpdateDate(databaseSysdate);
				st.setUpdateUser(logInfo.getStaffCode());
				staffDAO.updateStaff(st, null);
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<SaleOrder> getListReturnOrderByOrderId(long soId, SaleOrderStatus approved, SaleOrderStep approvedStep) throws BusinessException {
		try {
			List<SaleOrder> lst = saleOrderDAO.getListReturnOrderByOrderId(soId, approved, approvedStep);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public void cancelReturnOrder(long orderId, LogInfoVO logInfo) throws BusinessException {
		try {
			SaleOrder saleOrder = saleOrderDAO.getSaleOrderById(orderId);
			if (saleOrder == null || logInfo == null) {
				throw new IllegalArgumentException("invalid parameters");
			}
			// Lay danh sach chi tiet don hang
			List<SaleOrderDetail> lstSaleOrderDetail = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(saleOrder.getId(), null);
			// xoa chi tiet don hang
			List<SaleOrderLot> lstSaleOrderLot1 = null;
			SaleOrderLot sol = null;
			List<SaleOrderPromoDetail> lstPromo = null;
			SaleOrderPromoDetail promo = null;
			if (lstSaleOrderDetail != null) {
				for (SaleOrderDetail sod : lstSaleOrderDetail) {
					if (sod.getProduct() != null && sod.getQuantity() != 0 && sod.getQuantity() != null) {
						lstSaleOrderLot1 = saleOrderLotDAO.getListSaleOrderLotBySaleOrderDetail(null, sod.getId());
						if (lstSaleOrderLot1 != null) {
							for (int i = 0, sz = lstSaleOrderLot1.size(); i < sz; i++) {
								sol = lstSaleOrderLot1.get(i);
								saleOrderLotDAO.deleteSaleOrderLot(sol); // Xoa SALE ORDER LOT
							}
						}
						lstPromo = saleOrderPromoDetailDAO.getListSaleOrderPromoByDetailId(sod.getId());
						if (lstPromo != null) {
							for (int i = 0, sz = lstPromo.size(); i < sz; i++) {
								promo = lstPromo.get(i);
								saleOrderPromoDetailDAO.deleteSaleOrderPromoDetail(promo);
							}
						}
					}
					saleOrderDetailDAO.deleteSaleOrderDetail(sod); // Xoa SALE_ODER_DETAIL
				}
			}
			List<SaleOrderPromotion> lstSOPromotion = saleOrderPromoDetailDAO.getListSaleOrderPromotionByOrderId(saleOrder.getId());
			if (lstSOPromotion != null && lstSOPromotion.size() > 0) {
				for (SaleOrderPromotion sop : lstSOPromotion) {
					saleOrderPromoDetailDAO.deleteSaleOrderPromotion(sop);
				}
			}
			
			saleOrderDAO.deleteSaleOrderPromoLot(saleOrder.getId());
			saleOrderDAO.deleteSalePromoMap(saleOrder.getId());
			saleOrderDAO.deleteSaleOrder(saleOrder);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<SaleOrderDetailVOEx> getListSaleOrderPromoDetailForOrder(Long saleOrderId, Integer promotionType) throws BusinessException {
		try {
			return saleOrderPromoDetailDAO.getListSaleOrderPromoDetailForOrder(saleOrderId, promotionType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Map<String, List<String>> cancelOrdersInPrintOrderStep(Long shopId, List<String> lstOrderNumber, LogInfoVO logInfo) throws BusinessException {
		List<String> successOrders = new ArrayList<String>();
		List<String> errorOrders = new ArrayList<String>();
		/*
		 * validate input data
		 */
		List<String> errCodes = new ArrayList<String>();
		if (shopId == null) {
			BusinessException e = new BusinessException("[SaleOrderMgr.cancelOrdersInPrintOrderStep] shop_id is null.");
			errCodes.add("common.session.expired");
			e.setErrorCodes(errCodes);
			throw e;
		}
		if (lstOrderNumber == null || lstOrderNumber.size() == 0) {
			BusinessException e = new BusinessException("[SaleOrderMgr.cancelOrdersInPrintOrderStep] no order to cancel.");
			errCodes.add("sale.product.print.order.cancel.no.order");
			e.setErrorCodes(errCodes);
			throw e;
		}
		
		/*
		 * process business logic
		 */
		final Integer DEFAULT_NUM_DAY_ORDER_EXPIRE = getNumDaySaleOrderExpire();
		for (String orderNumber : lstOrderNumber) {
			StringBuilder processResult = new StringBuilder();
			try {
				Boolean isProcessOrderSuccess = cancelOneOrderInPrintOrderStep(processResult, shopId, orderNumber, DEFAULT_NUM_DAY_ORDER_EXPIRE, logInfo);
				if (isProcessOrderSuccess) {
					successOrders.add(orderNumber);
				} else {
					errorOrders.add(processResult.toString());
				}
			} catch (Exception e) {
				processResult.append("sale.product.print.order.cancel.order.process.error.happened" + "_" + orderNumber);
				errorOrders.add(processResult.toString());
			}			
		}
		
		Map<String, List<String>> result = new HashMap<String, List<String>>();
		result.put("successOrders", successOrders);
		result.put("errorOrders", errorOrders);
		return result;
	}
	
	/**
	 * 
	 * @param outMsg
	 * @param shopId
	 * @param orderNumber
	 * @param logInfo
	 * @return
	 * @throws BusinessException
	 */
	private Boolean cancelOneOrderInPrintOrderStep(final StringBuilder outMsg, final Long shopId, final String orderNumber, final Integer numDayOrderExpire, final LogInfoVO logInfo) throws BusinessException {
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		return transactionTemplate.execute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus transactionStatus) {
				String processResultMsg = "";
				Boolean isProcessOrderSuccess = false;
				try {
					Date databaseSysdate = commonDAO.getSysDate();
					
					SaleOrder saleOrder = saleOrderDAO.getSaleOrderByOrderNumber(orderNumber, null);
					/*
					 * validate order
					 */
					if (saleOrder == null) {
						processResultMsg = "sale.product.print.order.cancel.order.not.exists" + "_" + orderNumber;
					}
					if (saleOrder.getOrderType() != OrderType.IN) {
						processResultMsg = "sale.product.print.order.cancel.order.order.not.type.IN" + "_" + saleOrder.getOrderNumber();
					}
					if (saleOrder.getApprovedStep() == SaleOrderStep.PRINT_CONFIRMED
							|| saleOrder.getApprovedStep() == SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT
							|| saleOrder.getApprovedStep() == SaleOrderStep.DEBIT_ASSIGNED) {
						processResultMsg = "sale.product.print.order.cancel.order.order.print.confirmed.already" + "_" + saleOrder.getOrderNumber();
					}
					Customer customer = saleOrder.getCustomer();
					Staff staff = saleOrder.getStaff();
					if (customer == null || staff == null) {
						processResultMsg = "sale.product.print.order.cancel.order.order.invalid" + "_" + saleOrder.getOrderNumber();
					}
					
					if (!StringUtility.isNullOrEmpty(processResultMsg)) {
						outMsg.append(processResultMsg);
						return isProcessOrderSuccess;
					}
					
					/*
					 * all are OK, process to cancel order.
					 * First, update sale order's approved status
					 */
					
					saleOrder.setApproved(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM);
					saleOrder.setUpdateDate(databaseSysdate);
					saleOrder.setUpdateUser(logInfo.getStaffCode());
					saleOrderDAO.updateSaleOrder(saleOrder);
					
					PoCustomer poCustomer = poCustomerDAO.getPoCustomerByFromSaleorderId(saleOrder.getId());
					if (saleOrder.getOrderSource() == SaleOrderSource.TABLET && poCustomer == null) {	// sale order made by TABLET must have po_customer
						processResultMsg = "sale.product.print.order.cancel.order.order.invalid" + "_" + saleOrder.getOrderNumber();
						throw new BusinessException("Print Order, cancel order: order data is invalid (lack of data)");
					}
					
					if (poCustomer != null) {
						poCustomer.setApproved(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM);
						poCustomer.setUpdateDate(databaseSysdate);
						poCustomer.setUpdateUser(logInfo.getStaffCode());
						poCustomerDAO.updatePoCustomer(poCustomer);
					}
					
					/*
					 * restore stock total
					 */
					Long orderId = saleOrder.getId();
					List<SaleOrderLot> saleOrderLots = saleOrderDAO.getListSaleOrderLotBySaleOrderId(orderId);
					if (SaleOrderSource.TABLET.equals(saleOrder.getOrderSource())
							&& saleOrderLots != null && saleOrderLots.size() > 0) {
						for (SaleOrderLot saleOrderLot : saleOrderLots) {
							if (saleOrderLot != null) {
								StockTotal productStockTotal = stockTotalDAO.getStockTotalByProductAndOwner(saleOrderLot.getProduct() != null ? saleOrderLot.getProduct().getId() : -1, 
										StockObjectType.SHOP, shopId, 
										saleOrderLot.getWarehouse() != null ? saleOrderLot.getWarehouse().getId() : -1);
								if (productStockTotal != null) {
									Integer availableQuantity = productStockTotal.getAvailableQuantity() != null ? productStockTotal.getAvailableQuantity() : 0;
									availableQuantity += saleOrderLot.getQuantity() != null ? saleOrderLot.getQuantity() : 0;
									
									/** Noi dung log */
									try {										
										logInfo.getContenFormat(saleOrderLot.getProduct().getId(), 
												saleOrderLot.getWarehouse().getId(), 
												saleOrderLot.getQuantity(), 
												productStockTotal.getAvailableQuantity(), 
												availableQuantity,
												productStockTotal.getQuantity(), 
												productStockTotal.getQuantity());
									} catch (Exception e) {}
									/** End noi dung log */
									
									productStockTotal.setAvailableQuantity(availableQuantity);
									productStockTotal.setUpdateDate(databaseSysdate);
									productStockTotal.setUpdateUser(logInfo.getStaffCode());
									stockTotalDAO.updateStockTotal(productStockTotal);
								}
								saleOrderLotDAO.deleteSaleOrderLot(saleOrderLot);
							}
						}
					}
					/** Ghi log kiem tra ton kho */
					try {
						logInfo.setIdObject(orderId.toString());
						logInfo.setActionType(LogInfoVO.ACTION_UPDATE);
						logInfo.setFunctionCode(LogInfoVO.FC_SALE_CANCEL_ORDER);
						LogUtility.logInfoWs(logInfo);
					} catch (Exception e) {}
					/** End ghi log kiem tra ton kho */
					/*
					 * restore promotion ration
					 */
					// lacnv1 - khong tru di so suat vi khi xac nhan so suat moi tang
					/*SaleOrderPromotionFilter sodpFilter = new SaleOrderPromotionFilter();
					sodpFilter.setSaleOrderId(orderId);
					List<SaleOrderPromotion> saleOrderPromotions = saleOrderPromotionDAO.getSaleOrderPromotions(sodpFilter);
					if (saleOrderPromotions != null && saleOrderPromotions.size() > 0) {
						for (SaleOrderPromotion saleOrderPromotion : saleOrderPromotions) {
							SaleOrderMgrImpl.this.restorePromotionProgramRation(saleOrderPromotion.getPromotionProgram(), 
									saleOrderPromotion.getQuantityReceived(), 
									saleOrder.getShop().getShopCode(), 
									customer.getShortCode(),
									customer.getChannelType().getId(),
									staff, saleOrder.getShop(), logInfo);
						}
					}*/
					
					/*
					 * update customer, staff_customer, routing_customer information
					 */
					if (saleOrder.getAmount() != null && saleOrder.getAmount().compareTo(BigDecimal.ZERO) == 1) {
						SaleOrderFilter<SaleOrder> soFilter = new SaleOrderFilter<SaleOrder>();
						soFilter.setCustomerId(customer.getId());
						soFilter.setGetBothINandSO(true);
						soFilter.setLstApprovedStatus(Arrays.asList(SaleOrderStatus.NOT_YET_APPROVE, SaleOrderStatus.APPROVED));
						soFilter.setType(SaleOrderType.NOT_YET_RETURNED.getValue());
						soFilter.setAnchorDateDiff(saleOrder.getOrderDate());
						soFilter.setDiffDaysFromDate(numDayOrderExpire);
						soFilter.setIsPositiveAmountSaleOrder(true);
						soFilter.setShopId(shopId);

						if (customer.getLastOrder() != null && saleOrder.getOrderDate() != null && DateUtility.compareDateWithoutTime(customer.getLastOrder(), saleOrder.getOrderDate()) <= 0) {
							/*
							 * customer information
							 */
							List<SaleOrder> listSaleOrderWithoutStaffInfo = saleOrderDAO.getListSaleOrder(soFilter, null);
							if (listSaleOrderWithoutStaffInfo == null || listSaleOrderWithoutStaffInfo.size() == 0) {
								customer.setLastOrder(null);
							} else {
								customer.setLastOrder(listSaleOrderWithoutStaffInfo.get(0).getOrderDate());
							}
							customer.setUpdateDate(databaseSysdate);
							customer.setUpdateUser(logInfo.getStaffCode());
							customerDAO.updateCustomer(customer, logInfo);
						}

						/*
						 * staff_customer information
						 */
						soFilter.setStaffId(staff.getId());
						List<SaleOrder> listSaleOrderWithStaffInfo = saleOrderDAO.getListSaleOrder(soFilter, null);
						StaffCustomer staffCustomer = staffCustomerDAO.getStaffCustomer(staff.getId(), customer.getId());
						if (staffCustomer == null) {
							staffCustomer = new StaffCustomer();
							staffCustomer.setCustomer(customer);
							staffCustomer.setStaff(staff);
							staffCustomer.setShop(saleOrder.getShop());
						}
						if (listSaleOrderWithStaffInfo == null || listSaleOrderWithStaffInfo.size() == 0) {
							staffCustomer.setLastOrder(null);
						} else {
							staffCustomer.setLastOrder(listSaleOrderWithStaffInfo.get(0).getOrderDate());
						}
						if (staffCustomer.getId() != null) {
							staffCustomerDAO.updateStaffCustomer(staffCustomer, logInfo);
						} else {
							staffCustomerDAO.createStaffCustomer(staffCustomer, logInfo);
						}

						/*
						 * update routing customer
						 */
						RoutingCustomer routingCustomer = routingCustomerDAO.getRoutingCustomerByDateAndStaff(customer.getId(), shopId, saleOrder.getOrderDate(), staff.getId());
						if (routingCustomer != null) {
							if (listSaleOrderWithStaffInfo == null || listSaleOrderWithStaffInfo.size() == 0) {
								routingCustomer.setLastOrder(null);
							} else {
								routingCustomer.setLastOrder(listSaleOrderWithStaffInfo.get(0).getOrderDate());
							}
							routingCustomer.setUpdateDate(databaseSysdate);
							routingCustomer.setUpdateUser(logInfo.getStaffCode());
							routingCustomerDAO.updateRoutingCustomer(routingCustomer);
						}
					}
					
					if (SaleOrderSource.WEB.equals(saleOrder.getOrderSource())) {
						// Lay danh sach chi tiet don hang
						List<SaleOrderDetail> lstSaleOrderDetail = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(saleOrder.getId(), null);
						for (SaleOrderDetail sod : lstSaleOrderDetail) {
							if (sod.getProduct() != null && sod.getQuantity() != null) {
								List<SaleOrderLot> lstSaleOrderLot1 = saleOrderLotDAO.getListSaleOrderLotBySaleOrderDetail(null, sod.getId());
								for (SaleOrderLot sol : lstSaleOrderLot1) {
									if (sol.getWarehouse() != null) {
										StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(sod.getProduct().getId(), StockObjectType.SHOP, sod.getShop().getId(), sol.getWarehouse().getId());
										if (stockTotal != null) {
											/** Log conent */
											try {
												logInfo.getContenFormat(sod.getProduct().getId(), sol.getWarehouse().getId(), sod.getQuantity(), 
														stockTotal.getAvailableQuantity(), stockTotal.getAvailableQuantity() + sol.getQuantity(), 
														stockTotal.getQuantity(), stockTotal.getQuantity());
											} catch (Exception e) {
											}
											/** End log conent */
											
											stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() + sol.getQuantity());
											stockTotal.setUpdateDate(databaseSysdate);
											stockTotal.setUpdateUser(logInfo.getStaffCode());
											stockTotalDAO.updateStockTotal(stockTotal); // Cap nhat TON KHO DAP UNG trong StockTotal
										}
									}
								}
							}
						}
					}
					
					isProcessOrderSuccess = true;
					if(!saleOrder.getOrderType().equals(OrderType.CM) && !saleOrder.getOrderType().equals(OrderType.CO)){
						List<SaleOrder> lstSaleOrder = new ArrayList<SaleOrder>();
						lstSaleOrder.add(saleOrder);
						insertNewTable(lstSaleOrder, 5, logInfo, -1);
					}
					processResultMsg = orderNumber;
				} catch (Exception e) {
					isProcessOrderSuccess = false;
					if (StringUtility.isNullOrEmpty(processResultMsg)) {
						if (ExceptionUtil.isExceptionCauseBy(e, ObjectNotFoundException.class)) {
							processResultMsg = "sale.product.print.order.cancel.order.process.error.happened.order.data.invalid" + "_" + orderNumber;
						} else {
							processResultMsg = "sale.product.print.order.cancel.order.process.error.happened" + "_" + orderNumber;
						}
					}
					LogUtility.logError(e, e.getMessage());
					transactionStatus.setRollbackOnly();
				}
				outMsg.append(processResultMsg);
				return isProcessOrderSuccess;
			}			
		});
	}
	
	/**
	 * restore promotion program ration
	 * @author tuannd20
	 * @param promotionProgram
	 * @param rationQuantity
	 * @param shopCode
	 * @param customerShortCode
	 * @param customerTypeId
	 * @param staff
	 * @param shop
	 * @param logInfo
	 * @throws Exception
	 * @date 03/11/2014
	 */
	private void restorePromotionProgramRation(PromotionProgram promotionProgram, Integer rationQuantity,
												String shopCode, String customerShortCode, Long customerTypeId, 
												Staff staff, Shop shop, LogInfoVO logInfo) throws Exception {
		this.changePromotionQuantity((int) (-1 * Math.asin(rationQuantity)), logInfo, 
									promotionProgram.getPromotionProgramCode(), shopCode, customerShortCode, customerTypeId, staff, shop);
	}
	
	
	@Override
	public ObjectVO<SaleOrderVOEx2> searchSaleOrder(SoFilter filter) throws BusinessException {
		try {
			List<SaleOrderVOEx2> lst = saleOrderDAO.searchSaleOrder(filter);
			ObjectVO<SaleOrderVOEx2> vo = new ObjectVO<SaleOrderVOEx2>();
			vo.setkPaging(filter.getkPagingVOEx2());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<SaleOrderVOEx2> searchStockTrans(SoFilter filter) throws BusinessException {
		try {
			List<SaleOrderVOEx2> lst = saleOrderDAO.searchStockTrans(filter);
			ObjectVO<SaleOrderVOEx2> vo = new ObjectVO<SaleOrderVOEx2>();
			vo.setkPaging(filter.getkPagingVOEx2());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public ObjectVO<CommercialSupportVO> getListCommercialSupportOfSaleOrder(long saleOrderId, KPaging<CommercialSupportVO> paging)
			throws BusinessException {
		try {
			List<CommercialSupportVO> lst = promotionProgramDAO.getListCommercialSupportOfSaleOrder(saleOrderId, paging);
			ObjectVO<CommercialSupportVO> objVO = new ObjectVO<CommercialSupportVO>();
			objVO.setkPaging(paging);
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<StockTotalVO> getListWaringAccumulativeProductQuantity(long shopId, long saleOrderId) throws BusinessException {
		try {
			List<StockTotalVO> lst = saleOrderDAO.getListWaringAccumulativeProductQuantity(shopId, saleOrderId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public boolean checkPromotionOpenOfSaleOrder(long saleOrderId, long customerId, Date lockDate, List<Long> lstPassOrderId) throws BusinessException {
		try {
			return saleOrderDAO.checkPromotionOpenOfSaleOrder(saleOrderId, customerId, lockDate, lstPassOrderId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public boolean checkAccumulativePromotionOfSaleOrder(long saleOrderId, Date lockDate, List<Long> lstPassOrderId) throws BusinessException {
		try {
			return saleOrderDAO.checkAccumulativePromotionOfSaleOrder(saleOrderId, lockDate, lstPassOrderId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<SearchSaleTransactionErrorVO> confirmAccumulativeSaleOrder(List<SaleOrder> lstOrder, String description, LogInfoVO loginfo) throws BusinessException {
		try {
			if (lstOrder == null || lstOrder.size() == 0) {
				return null;
			}
			// lay cau hinh phan biet kho ban - kho KM
			boolean isDiVidualWarehouse = false;
			List<ApParam> aparams = apParamMgr.getListApParam(ApParamType.SYS_WAREHOUSE_CONFIG, ActiveType.RUNNING);
			if (aparams == null || aparams.size() == 0 || Constant.SYS_WAREHOUSE_CONFIG_DIVIDUAL.equals(aparams.get(0).getValue())) {
				isDiVidualWarehouse = true;
			}
			Date sysDate = commonDAO.getSysDate();
			boolean b = true;
			Shop sh = lstOrder.get(0).getShop();
			Date lockDate = null;
			if (sh != null) {
				lockDate = shopLockDAO.getApplicationDate(sh.getId());
			} else {
				lockDate = sysDate;
			}
			SearchSaleTransactionErrorVO errVO = null;
			List<SearchSaleTransactionErrorVO> lstErr = new ArrayList<SearchSaleTransactionErrorVO>();
			List<SaleOrderDetail> lstSODetails = null;
			List<RptCTTLPay> lstPays = null;
			List<RptCTTLPayDetail> lstPayDetails = null;
			List<StockTotalVO> lstStocks = null;
			StockTotalVO stock = null;
			SaleOrderDetail soDetail = null;
			SaleOrderFilter<RptCTTLPay> filter = new SaleOrderFilter<RptCTTLPay>();
			Map<String, Integer> map1 = null;
			String[] str = null;
			PromotionProgram pp = null;
			List<Long> lstPassOrderId = new ArrayList<Long>();
			
			for (SaleOrder so : lstOrder) {
				// xu ly khuyen mai mo moi
				b = saleOrderDAO.checkPromotionOpenOfSaleOrder(so.getId(), so.getCustomer().getId(), lockDate, lstPassOrderId);
				if (!b) {
					errVO = new SearchSaleTransactionErrorVO();
					errVO.setId(so.getId());
					errVO.setOrderNumber(so.getOrderNumber());
					errVO.setCustomerCode(so.getCustomer().getCustomerCode());
					errVO.setCustomerName(so.getCustomer().getCustomerName());
					errVO.setOpenPromo("X");
					lstErr.add(errVO);
					continue;
				}
				
				// xu ly xoa cac lan tra thuong tich luy khong co hang
				lstSODetails = saleOrderDetailDAO.getListSaleOrderDetailZV23(so.getId());
				filter.setSaleOrderId(so.getId());
				if (lstSODetails != null && lstSODetails.size() > 0) {
					lstStocks = saleOrderDAO.getListWaringAccumulativeProductQuantity(so.getShop().getId(), so.getId());
					if (lstStocks == null || lstStocks.size() == 0) {
						throw new IllegalArgumentException("NOT_ENOUGH_QUANTITY");
					}
					map1 = new HashMap<String, Integer>();
					for (int i = 0, sz = lstStocks.size(); i < sz; i++) { // cap nhat lai gia tri cho nhung sale_order_detail khong du kho
						stock = lstStocks.get(i);
						for (int j = 0, szj = lstSODetails.size(); j < szj; j++) {
							soDetail = lstSODetails.get(j);
							if (soDetail.getPayingOrder() == null) {
								soDetail.setPayingOrder(1);
							}
							if (stock.getProductId().equals(soDetail.getProduct().getId())) {
								if (stock.getAvailableQuantity() >= soDetail.getQuantity()) {
									stock.setAvailableQuantity(stock.getAvailableQuantity() - soDetail.getQuantity());
								} else {
									soDetail.setQuantity(stock.getAvailableQuantity());
									stock.setAvailableQuantity(0);
								}
								if (soDetail.getQuantity() > 0) {
									Integer n = map1.get(soDetail.getProgramCode() + "_" + soDetail.getPayingOrder());
									if (n == null) {
										n = 0;
									}
									map1.put(soDetail.getProgramCode() + "_" + soDetail.getPayingOrder(), n+1);
								} else if (map1.get(soDetail.getProgramCode() + "_" + soDetail.getPayingOrder()) == null) {
									map1.put(soDetail.getProgramCode() + "_" + soDetail.getPayingOrder(), 0);
								}
							}
						}
					}
					saleOrderDetailDAO.updateSaleOrderDetail(lstSODetails);
					for (String s : map1.keySet()) { // xoa nhung lan tra ma sp khong con ton kho de tra
						int n = map1.get(s);
						if (n == 0) {
							str = s.split("_");
							for (int j = 0, szj = lstSODetails.size(); j < szj; j++) {
								soDetail = lstSODetails.get(j);
								if (str[0].equals(soDetail.getProgramCode()) && soDetail.getPayingOrder().equals(Integer.valueOf(str[1]))) {
									saleOrderDetailDAO.deleteSaleOrderDetail(soDetail);
								}
							}
							pp = promotionProgramDAO.getPromotionProgramByCode(str[0]);
							if (pp != null) {
								filter.setProgramId(pp.getId());
								lstPays = promotionProgramDAO.getListRptCTTLPay(filter);
								if (lstPays != null && lstPays.size() > 0) {
									lstPayDetails = promotionProgramDAO.getRptCTTLPayDetailByRptPayId(lstPays.get(0).getId());
									if (lstPayDetails != null && lstPayDetails.size() > 0) {
										for (int j = 0, szj = lstPayDetails.size(); j < szj; j++) {
											commonDAO.deleteEntity(lstPayDetails.get(j));
										}
									}
									commonDAO.deleteEntity(lstPays.get(0));
								}
							}
						}
					}
				}
				
				// xu ly khuyen mai tich luy
				b = saleOrderDAO.checkAccumulativePromotionOfSaleOrder(so.getId(), lockDate, lstPassOrderId);
				if (!b) {
					errVO = new SearchSaleTransactionErrorVO();
					errVO.setId(so.getId());
					errVO.setOrderNumber(so.getOrderNumber());
					errVO.setCustomerCode(so.getCustomer().getCustomerCode());
					errVO.setCustomerName(so.getCustomer().getCustomerName());
					errVO.setAccumulation("X");
					lstErr.add(errVO);
					continue;
				}
				lstPassOrderId.add(so.getId());
				
				// xac nhan don hang
				so.setApprovedStep(SaleOrderStep.CONFIRMED);
				if (OrderType.IN.equals(so.getOrderType())) {
					this.autoSplitWarehouse(so, true, isDiVidualWarehouse, loginfo);
				}
				so.setUpdateDate(sysDate);
				so.setUpdateUser(loginfo.getStaffCode());
				commonDAO.updateEntity(so);
				
				ProcessHistory processHistory = new ProcessHistory();
				processHistory.setType(ProcessHistoryType.SALE_ORDER);
				processHistory.setObjectType(CommonContanst.SALE_ORDER_CONFIRM_STEP);
				processHistory.setObjectId(so.getId());
				processHistory.setLogTime(sysDate);
				commonDAO.createEntity(processHistory);
			}
			return lstErr;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public SearchSaleTransactionErrorVO prepareAprroveSaleOrder(SaleOrder so, List<Long> lstPassOrderId) throws BusinessException {
		try {
			SearchSaleTransactionErrorVO errVO = null;
			if (so == null) {
				return errVO;
			}
			if (!OrderType.SO.equals(so.getOrderType()) && !OrderType.IN.equals(so.getOrderType())) {
				return errVO;
			}
			boolean b = false;
			List<SaleOrderDetail> lstDetails = saleOrderDetailDAO.getListSaleProductBySaleOrderId(so.getId());
			if (lstDetails == null || lstDetails.size() == 0) {
				return errVO;
			}
			
			// check co duoc phep tra thuong mo moi hay khong
			b = saleOrderDAO.checkApprovedPromotionOpenOfSaleOrder(so.getId(), so.getCustomer().getId(), so.getOrderDate(), lstPassOrderId);
			if (!b) {
				errVO = new SearchSaleTransactionErrorVO();
				errVO.setId(so.getId());
				errVO.setOrderNumber(so.getOrderNumber());
				errVO.setCustomerCode(so.getCustomer().getCustomerCode());
				errVO.setCustomerName(so.getCustomer().getCustomerName());
				errVO.setLostOpenPromo("X");
			} else {
				b = false;
				// check co khuyen mai mo moi hay khong, kiem tra xem co tra hay khong
				//triet tam thoi comment
				/*List<PromotionProgram> lstPrograms = promotionProgramDAO.getPromotionProgramsZV24InSaleOrder(so.getId(), so.getCustomer().getId(), so.getOrderDate(), lstPassOrderId);
				if (lstPrograms != null && lstPrograms.size() > 0) { // nhung CTKM ZV24 co trong don hang chua duoc tra mo moi
					SortedMap<Long, List<SaleOrderDetailVO>> sortListOutPut = new TreeMap<Long, List<SaleOrderDetailVO>>();
					List<SaleOrderDetailVO> listProductPromotionsale = new ArrayList<SaleOrderDetailVO>();
					SaleOrderVO orderVO = new SaleOrderVO();
					Long keyList = Long.valueOf(100);
					
					SortedMap<Long, SaleOrderDetailVO> sortListProductSale = new TreeMap<Long, SaleOrderDetailVO>();
					SaleOrderDetailVO orderDetail = null;
					SaleOrderDetail orderDTO = null;
					SaleOrderDetail detail = null;
					for (int i = 0, size = lstDetails.size(); i < size; i++) {
						detail = lstDetails.get(i);
						
						orderDetail = new SaleOrderDetailVO();
						orderDTO = new SaleOrderDetail();
						orderDetail.setSaleOrderDetail(orderDTO);
						
						orderDTO.setQuantity(detail.getQuantity());
						orderDTO.setProduct(detail.getProduct());
						orderDTO.setPrice(detail.getPrice());
						orderDTO.setPriceValue(detail.getPriceValue());
						orderDTO.setAmount(detail.getAmount());
						orderDTO.setProgramCode(detail.getProgramCode());
						//totalAmount = totalAmount.add(detail.getSaleOrderDetail().getAmount());
						sortListProductSale.put(Long.valueOf(detail.getProduct().getId()), orderDetail);
					}
					
					for (int i = 0, sz = lstPrograms.size(); i < sz; i++) {
						promotionProgramDAO.calcPromotion(so.getAmount(), lstPrograms.get(i), sortListProductSale,
								listProductPromotionsale, keyList, sortListOutPut, so.getShop().getId(),
								SaleOrderDetailVO.ownerTypePromoProduct, orderVO, so.getCustomer().getId(), so.getStaff().getId(), so.getOrderDate());
						if (sortListOutPut.size() > 0) {
							b = true; // duoc huong khuyen mai mo moi
							break;
						} else {
							promotionProgramDAO.calcPromotion(so.getAmount(), lstPrograms.get(i), sortListProductSale,
									listProductPromotionsale, keyList, sortListOutPut, so.getShop().getId(),
									SaleOrderDetailVO.ownerTypePromoOrder, orderVO, so.getCustomer().getId(), so.getStaff().getId(), so.getOrderDate());
							if (sortListOutPut.size() > 0) {
								b = true; // duoc huong khuyen mai mo moi
								break;
							}
						}
					}
				}*/
				/*if (b) {
					if (errVO == null) {
						errVO = new SearchSaleTransactionErrorVO();
						errVO.setId(so.getId());
						errVO.setOrderNumber(so.getOrderNumber());
						errVO.setCustomerCode(so.getCustomer().getCustomerCode());
						errVO.setCustomerName(so.getCustomer().getCustomerName());
					}
					errVO.setOpenPromo("X");
				}*/
			}
			
			// check co duoc phep tra thuong tich luy hay khong
			b = saleOrderDAO.checkApprovedAccumulativePromotionOfSaleOrder(so.getId(), so.getOrderDate(), lstPassOrderId);
			if (!b) {
				if (errVO == null) {
					errVO = new SearchSaleTransactionErrorVO();
					errVO.setId(so.getId());
					errVO.setOrderNumber(so.getOrderNumber());
					errVO.setCustomerCode(so.getCustomer().getCustomerCode());
					errVO.setCustomerName(so.getCustomer().getCustomerName());
				}
				errVO.setAccumulation("X");
			}
			
			return errVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrderPromoDetail> getListSaleOrderPromoDetailOfOrder(long saleOrderId, String programCode) throws BusinessException {
		try {
			List<SaleOrderPromoDetail> lst = saleOrderDetailDAO.getListSaleOrderPromoDetailOfOrder(saleOrderId, programCode);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<OrderProductVO> getCONotHaveApprovedSO(SoFilter filter) throws BusinessException {
		try {
			List<OrderProductVO> lst = saleOrderDAO.getCONotHaveApprovedSO(filter);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<OrderProductVO> getOrderNotEnoughPortion(SoFilter filter) throws BusinessException {
		try {
			List<OrderProductVO> lst = saleOrderPromotionDAO.getOrderNotEnoughPortion(filter);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateShopParamByInvoice(ShopParam numberValueOrderParam) throws BusinessException {
		try {
			numberValueOrderParam.setUpdateDate(commonDAO.getSysDate());
			saleOrderDAO.updateEquipment(numberValueOrderParam);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		
	}

	@Override
	public void createShopParamByInvoice(ShopParam numberValueOrderParam) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			numberValueOrderParam.setCreateDate(commonDAO.getSysDate());
			saleOrderDAO.createShopParamByInvoice(numberValueOrderParam);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	private void checkOrder(Long saleOrderId,Integer objectType) throws BusinessException {
		try {
			SaleOrder soCheck = saleOrderDAO.getSaleOrderById(saleOrderId);
			if((objectType == CommonContanst.SALE_ORDER_CONFIRM_STEP 
						&& (!SaleOrderStatus.NOT_YET_APPROVE.equals(soCheck.getApproved()) || !SaleOrderStep.NOT_YET_CONFIRM.equals(soCheck.getApprovedStep())))
				|| (objectType == CommonContanst.SALE_ORDER_APPROVED_STEP 
						&& (!SaleOrderStatus.NOT_YET_APPROVE.equals(soCheck.getApproved()) || !SaleOrderStep.CONFIRMED.equals(soCheck.getApprovedStep())))
				|| (objectType == CommonContanst.SALE_ORDER_STOCKUPDATING_STEP 
						&& (!SaleOrderStatus.APPROVED.equals(soCheck.getApproved()) || !SaleOrderStep.PRINT_CONFIRMED.equals(soCheck.getApprovedStep())))){
				throw new BusinessException("DON_HANG_DA_XU_LY");
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional (rollbackFor = Exception.class)
	public List<SearchSaleTransactionErrorVO> cancelStockTrans(List<Long> lstId, long shopId, Date lockDate, LogInfoVO logInfo) throws BusinessException {
		if (lstId == null || lstId.size() == 0) {
			return null;
		}
		if (lockDate == null || logInfo == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		try {
			Date curDate = commonDAO.getSysDate();
			List<SearchSaleTransactionErrorVO> errVOs = new ArrayList<SearchSaleTransactionErrorVO>();
			
			StockTrans stockTrans = null;
			SearchSaleTransactionErrorVO err = null;
			Integer res = null;
			for (Long stockId : lstId) {
				stockTrans = stockTransDAO.getStockTransById(stockId);
				if (stockTrans == null || !SaleOrderStatus.APPROVED.equals(stockTrans.getApproved())
						|| stockTrans.getApprovedStep().getValue() >= SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT.getValue()) {
					err = new SearchSaleTransactionErrorVO();
					err.setOrderNumber(stockTrans.getStockTransCode());
					err.setApprove("X");
				} else {
					try {
						res = this.cancelOneStockTrans(stockTrans, lockDate, curDate, logInfo);
						if (ORDER_CONFIRM_ERROR.equals(res)) {
							err = new SearchSaleTransactionErrorVO();
							err.setOrderNumber(stockTrans.getStockTransCode());
							err.setQuantityErr("X");
						}
					} catch (BusinessException e1) {
						throw e1;
					} catch (Exception e2) {
						throw new BusinessException(e2);
					}
				}
				
				if (err != null) {
					errVOs.add(err);
				}
			}
		
			return errVOs;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author trietptm
	 */
	@Override
	@Transactional (rollbackFor = Exception.class)
	public List<SearchSaleTransactionErrorVO> cancelStockTransUpdate(List<Long> lstId, Date lockDate, LogInfoVO logInfo) throws BusinessException {
		if (lstId == null || lstId.size() == 0) {
			return null;
		}
		if (lockDate == null || logInfo == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		try {
			Date curDate = commonDAO.getSysDate();
			List<SearchSaleTransactionErrorVO> errVOs = new ArrayList<SearchSaleTransactionErrorVO>();
			
			StockTrans stockTrans = null;
			SearchSaleTransactionErrorVO err = null;
			Integer res = null;
			for (Long stockId : lstId) {
				stockTrans = stockTransDAO.getStockTransById(stockId);
				if (stockTrans == null || !SaleOrderStatus.NOT_YET_APPROVE.equals(stockTrans.getApproved())
						|| stockTrans.getApprovedStep().getValue() >= SaleOrderStep.PRINT_CONFIRMED.getValue()) {
					err = new SearchSaleTransactionErrorVO();
					err.setOrderNumber(stockTrans.getStockTransCode());
					err.setApprove("X");
				} else {
					try {
						res = this.cancelOneStockTrans(stockTrans, lockDate, curDate, logInfo);
						if (ORDER_CONFIRM_ERROR.equals(res)) {
							err = new SearchSaleTransactionErrorVO();
							err.setOrderNumber(stockTrans.getStockTransCode());
							err.setQuantityErr("X");
						}
					} catch (BusinessException e1) {
						throw e1;
					} catch (Exception e2) {
						throw new BusinessException(e2);
					}
				}
				
				if (err != null) {
					errVOs.add(err);
				}
			}
		
			return errVOs;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * Huy bo don DC
	 * 
	 * @author lacnv1
	 * @since Mar 11, 2015
	 */
	private Integer cancelOneStockTrans(final StockTrans stockTrans, final Date lockDate, final Date curDate, final LogInfoVO logInfo) throws BusinessException {
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		Integer comfirmResult = transactionTemplate.execute(new TransactionCallback<Integer>() {
			@Override
			public Integer doInTransaction(TransactionStatus transactionStatus) {
				Integer comfirmResult1 = null;
				try {
					// Neu la don deu chinh giam DCG, dieu chuyen DC thi tang lai ton kho dap ung
					if (OrderType.DC.getValue().equals(stockTrans.getTransType())
							|| OrderType.DCG.getValue().equals(stockTrans.getTransType())) {
						StockTotal stockTotal = null;
						LogInfoVO newLogInfoVO = null;
						List<StockTransLot> lstLot = stockTransLotDAO.getListStockTransLotByStockTransId(null, stockTrans.getId());
						if (lstLot != null && lstLot.size() > 0) {
							newLogInfoVO = logInfo.clone();
							try {
								newLogInfoVO.setIdObject("CANCEL_DC_"+(stockTrans.getId() != null ? stockTrans.getId().toString() : "N/A") + "_" + stockTrans.getStockTransCode() + "DC");
							} catch (Exception e) {
								// pass through
							}
							for (StockTransLot lot : lstLot) {
								stockTotal = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(lot.getProduct().getId(),
										StockObjectType.SHOP, stockTrans.getShop().getId(), lot.getFromOwnerId());
								if (stockTotal == null || (stockTotal.getAvailableQuantity() + lot.getQuantity() > stockTotal.getQuantity())) {
									//throw new IllegalArgumentException("StockTotal null_" +lot.getProduct().getId()+"_"+lot.getFromOwnerId());
									return ORDER_CONFIRM_ERROR;
								}
								
								try {
									newLogInfoVO.getContenFormat(
											lot.getProduct().getId(),
											lot.getFromOwnerId(),
											lot.getQuantity(),
											stockTotal.getAvailableQuantity(), stockTotal.getAvailableQuantity() + lot.getQuantity(),
											stockTotal.getQuantity(), stockTotal.getQuantity());
								} catch (Exception e) {
									// pass through
								}
								
								if (stockTotal.getAvailableQuantity() == null) {
									stockTotal.setAvailableQuantity(lot.getQuantity());
								} else {
									stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() + lot.getQuantity());
								}
								stockTotal.setUpdateDate(curDate);
								stockTotal.setUpdateUser(logInfo.getStaffCode());
								
								stockTotalDAO.updateStockTotal(stockTotal);
							}
							try {
								LogUtility.logInfoWs(newLogInfoVO);
							} catch (Exception e) {}
						}
					}
					
					stockTrans.setUpdateDate(curDate);
					stockTrans.setUpdateUser(logInfo.getStaffCode());
					stockTrans.setApproved(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM);
					stockTransDAO.updateStockTrans(stockTrans);
				} catch (Exception e) {
					transactionStatus.setRollbackOnly();
				}
				return comfirmResult1;
			}
		});
		return comfirmResult;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<OrderXmlObject> getSaleOrderXmlObject(SoFilter filter) throws BusinessException {
		try {
			List<SaleOrderVO> lst = saleOrderDAO.getListOrderVOForXml(filter);
			if (lst == null) {
				return null;
			}
			OrderXmlHeaderVO soHeader = null;
			OrderXmlDetailVO detailVO = null;
			OrderXmlLineDataVO lineData = null;
			List<SaleOrderDetailVO2> lstDetail = null;
			SaleOrderDetailVO2 detail = null;
			BigDecimal totalAmount = null;
			
			BigDecimal ONE_HUNDRED = new BigDecimal(100);
			BigDecimal tmp = null;
			BigDecimal percent = null;
			
			List<OrderXmlVO> lstXmlVO = new ArrayList<OrderXmlVO>();
			OrderXmlVO xmlVO = null;
			for (SaleOrderVO vo : lst) {
				soHeader = new OrderXmlHeaderVO();
				
				if (vo.getAmount() != null) {
					soHeader.setAmount(vo.getAmount().toString());
				}
				soHeader.setDeliveryCode(vo.getDeliveryCode());
				
				soHeader.setDistCode(vo.getShopCode());
				soHeader.setDocPromoCode(vo.getPromoCode());
				if (vo.getDiscount() != null) {
					soHeader.setDocDiscount(vo.getDiscount().toString());
				}
				soHeader.setInvoiceNbr(vo.getInvoiceNumber());
				soHeader.setOrderDate(vo.getOrderDateStr());
				soHeader.setOrderNbr(vo.getOrderNumber());
				soHeader.setOrderType(vo.getOrderType().getValue());
				
				/*soHeader.setDeliveryDate(vo.getDeliveryDateStr());
				soHeader.setPriority(vo.getPriorityStr());*/
				
				soHeader.setRetailerCode(vo.getCustomerCode());
				
				soHeader.setSRCode(vo.getStaffCode());
				if (vo.getTotal() != null) {
					soHeader.setTotal(vo.getTotal().toString());
				}
				
				detailVO = new OrderXmlDetailVO();
				lstDetail = saleOrderDAO.getListOrderDetailVOForXml(vo.getSaleOrderId(), vo.getOrderDate());
				if (lstDetail != null) {
					totalAmount = BigDecimal.ZERO;
					for (int i = 0, sz = lstDetail.size(); i < sz; i++) {
						detail = lstDetail.get(i);
						
						lineData = new OrderXmlLineDataVO();
						
						if (detail.getDiscount() != null) {
							lineData.setDiscountAmt(detail.getDiscount().toString());
						}
						
						if (detail.getDiscount() != null && detail.getAmount() != null
								&& detail.getAmount().signum() != 0) {
							tmp = detail.getDiscount().multiply(ONE_HUNDRED);
							percent = tmp.divide(detail.getAmount(), 2, RoundingMode.HALF_UP);
							lineData.setDiscountPer(percent.toString());
						} else if (detail.getDiscountPercent() != null) {
							lineData.setDiscountPer(detail.getDiscountPercent().toString());
						}

						lineData.setDistCode(soHeader.getDistCode());
						if (detail.getIsFreeItem() != null) {
							lineData.setFreeItem(detail.getIsFreeItem().toString());
						}
						lineData.setItemCode(detail.getProductCode());
						if (detail.getAmount() != null) {
							lineData.setLineAmt(detail.getAmount().toString());
						}
						lineData.setLineProCode(detail.getProgramCode());
						if (detail.getQuantity() != null) {
							lineData.setLineQty(detail.getQuantity().toString());
						}
						
						lineData.setOrderNbr(soHeader.getOrderNbr());
						lineData.setReasonCode(detail.getReasonCode());
						if (detail.getIsFreeItem() != null && detail.getIsFreeItem() == 1) {
							lineData.setSalePrice("0");
						} else if (detail.getPrice() != null) {
							lineData.setSalePrice(detail.getPrice().toString());
						}
						lineData.setUOM2(detail.getUom2());
						
						detailVO.addLine(lineData);
						
						if (detail.getAmount() != null) {
							totalAmount = totalAmount.add(detail.getAmount());
						}
					}
					
					if (!totalAmount.equals(vo.getAmount())) {
						continue;
					}
					
					xmlVO = new OrderXmlVO();
					xmlVO.setHeader(soHeader);
					xmlVO.setDetail(detailVO);
					xmlVO.setOrderId(vo.getSaleOrderId());
					
					lstXmlVO.add(xmlVO);
				}
			}
			
			List<OrderXmlObject> lstXml = new ArrayList<OrderXmlObject>();
			//List<Long> lstId = new ArrayList<Long>();
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String dateString = sdf.format(date);
			OrderXmlObject xmlObj = null;
			for (OrderXmlVO vo1 : lstXmlVO) {
				xmlObj = new OrderXmlObject();
				xmlObj.setLstOrderId(new ArrayList<Long>());
				xmlObj.getLstOrderId().add(vo1.getOrderId());
				String xmlContent = XmlGenerator.getIntance().generate(vo1);
				xmlObj.setXmlContent(xmlContent);
				long milis = System.currentTimeMillis();
				String fileName = "Exp_SalesOrder_" + vo1.getShopCode() + "_" + dateString
							+ "" + milis + "_" + vo1.getHeader().getOrderNbr() + ".xml";
				xmlObj.setFileName(fileName);
				lstXml.add(xmlObj);
				//lstId.add(vo1.getOrderId());
			}
					
			return lstXml;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrderLotVOEx> getListOrderDetailVOByOrderId(long orderId) throws BusinessException {
		try {
			List<SaleOrderLotVOEx> lst = saleOrderDetailDAO.getListOrderDetailVOByOrderId(orderId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrderLotVOEx> getListOrderPromoDetailByOrderId(long orderId) throws BusinessException {
		try {
			List<SaleOrderLotVOEx> lst = saleOrderDetailDAO.getListOrderPromoDetailByOrderId(orderId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public int getMaxPrintBatch(long shopId, Date pDate) throws BusinessException {
		try {
			int n = saleOrderDAO.getMaxPrintBatch(shopId, pDate);
			return n;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author trietptm
	 */
	@Override
	public List<ProgramCodeVO> getListProgramCodeVO(SaleOrderFilter<SaleOrder> filter) throws BusinessException {
		try {
			return saleOrderDAO.getListProgramCodeVO(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author trietptm
	 */
	@Override
	public boolean checkStockCounting(long saleOrderId) throws BusinessException {
		try {
			return saleOrderDAO.checkStockCounting(saleOrderId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public boolean checkStockCountingForStockTrans(StockStransFilter filter) throws BusinessException {
		try {
			return saleOrderDAO.checkStockCountingForStockTrans(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public boolean checkStockTransCycleCount(StockStransFilter filter) throws BusinessException {
		try {
			return saleOrderDAO.checkStockTransCycleCount(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<ProductStockTotal> lstProductStockTotal(SaleOrder tempOrder, StockTrans stockTrans, Integer type) throws BusinessException {
		try {
			return saleOrderDAO.lstProductStockTotal(tempOrder, stockTrans, type);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<ProductStockTotal> lstProductSalePlan(SaleOrder tempOrder, StockTrans stockTrans, Integer type) throws BusinessException {
		try {
			return saleOrderDAO.lstProductSalePlan(tempOrder, stockTrans, type);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void insertNewTable(List<SaleOrder> lstOrder, int action, LogInfoVO loginfo, Integer multipli) throws BusinessException {
		// TODO Auto-generated method stub
		List<Long> lstSaleOrderID = new ArrayList<Long>();
		for (SaleOrder saleOrder : lstOrder) {
			lstSaleOrderID.add(saleOrder.getId());
		}
		List<PromotionMapDeltaVO> listDelta;
		try {
			listDelta = saleOrderDAO.getSaleOrderDelta(lstSaleOrderID);
			insertIntoPromotionMapDelta(listDelta, action, loginfo);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * insert into table PROMOTION_MAP_DELTA
	 * 
	 * @author: agile_dungdq3
	 * @return: void
	 * @throws DataAccessException 
	 * @since: 5:27:28 PM Jan 6, 2016
	 */
	private void insertIntoPromotionMapDelta(List<PromotionMapDeltaVO> listDelta, int action, LogInfoVO loginfo) throws DataAccessException{
		Date createDate = commonDAO.getSysDate();
		for (PromotionMapDeltaVO promotionMapDeltaVO : listDelta) {
			//Mac dinh la se cac loai don khac se insert am
			Integer multipli = -1;
			//Tu choi don ban vansale thi insert dong am
			if(OrderType.SO.getValue().equals(promotionMapDeltaVO.getOrderType())) {
				multipli = -1;
			} else if (OrderType.CO.getValue().equals(promotionMapDeltaVO.getOrderType())) {
				multipli = 1;
			}
			
			PromotionMapDelta delta = new PromotionMapDelta();
			delta.setAction(ActionSaleOrder.parseValue(action));
			delta.setAmountDelta(promotionMapDeltaVO.getAmount() != null ? promotionMapDeltaVO.getAmount().multiply(new BigDecimal(multipli)): new BigDecimal(0));
			delta.setCreateDate(createDate);
			delta.setCreateUser(loginfo.getStaffCode());
			Customer cus = customerDAO.getCustomerById(promotionMapDeltaVO.getCustomerID());
			delta.setCustomer(cus);
			delta.setFromObjectId(promotionMapDeltaVO.getFromObjectID());
			delta.setNumDelta(promotionMapDeltaVO.getNum() != null ? promotionMapDeltaVO.getNum().multiply(new BigDecimal(multipli)): new BigDecimal(0));
			if(promotionMapDeltaVO.getPromotionShopMapID() != null) {
				PromotionCustomerMap customerMap = promotionCustomerMapDAO.getPromotionCustomerMap(promotionMapDeltaVO.getPromotionShopMapID(), promotionMapDeltaVO.getCustomerID());
				delta.setPromotionCustomerMap(customerMap);
			}
			if(promotionMapDeltaVO.getPromotionProgramID() != null) {
				PromotionProgram pp = promotionProgramDAO.getPromotionProgramById(promotionMapDeltaVO.getPromotionProgramID());
				delta.setPromotionProgram(pp);
			}
			if(promotionMapDeltaVO.getPromotionProgramID() != null) {
				Long promotionShopMapID = promotionShopMapDAO.getPromotionShopMapFromFunction(promotionMapDeltaVO.getShopID(), promotionMapDeltaVO.getPromotionProgramID());
				PromotionShopMap psm = promotionShopMapDAO.getPromotionShopMapById(promotionShopMapID);
				delta.setPromotionShopMap(psm);
			}
			if(promotionMapDeltaVO.getPromotionStaffMapID() != null) {
				PromotionStaffMap pstm = promotionStaffMapDAO.getPromotionStaffMapById(promotionMapDeltaVO.getPromotionStaffMapID());
				delta.setPromotionStaffMap(pstm);
			}
			if (promotionMapDeltaVO.getQuantity() != null) {
				Integer quantity = Integer.parseInt(promotionMapDeltaVO.getQuantity().multiply(new BigDecimal(multipli)).toString());
				delta.setQuantityDelta(quantity);
			}
			Shop sh = shopDAO.getShopById(promotionMapDeltaVO.getShopID());
			delta.setShop(sh);
			/*SaleOrder so = saleOrderDAO.getSaleOrderById(promotionMapDeltaVO.getFromObjectID());
			delta.setSource(so.getOrderSource());*/
			delta.setSource(SaleOrderSource.WEB);
			Staff st = staffDAO.getStaffById(promotionMapDeltaVO.getStaffID());
			delta.setStaff(st);
			commonDAO.createEntity(delta);
		}
	}

	@Override
	public List<SaleOrder> getListCustomerNotApproved(Long customerID) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return saleOrderDAO.getListCustomerNotApproved(customerID);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<OrderProductVO> getListSaleOrderID(List<Long> listSaleOrderID) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return saleOrderDAO.getListSaleOrderID(listSaleOrderID);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
}
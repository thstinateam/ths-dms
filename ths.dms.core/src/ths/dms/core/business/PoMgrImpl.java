package ths.dms.core.business;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Bank;
import ths.dms.core.entities.Debit;
import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.PayReceived;
import ths.dms.core.entities.PaymentDetail;
import ths.dms.core.entities.Po;
import ths.dms.core.entities.PoAuto;
import ths.dms.core.entities.PoAutoDetail;
import ths.dms.core.entities.PoAutoGroup;
import ths.dms.core.entities.PoAutoGroupDetail;
import ths.dms.core.entities.PoAutoGroupShopMap;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.PoVnmDetail;
import ths.dms.core.entities.PoVnmDetailReceived;
import ths.dms.core.entities.PoVnmLot;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.ApprovalStepPo;
import ths.dms.core.entities.enumtype.DebitDetailType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.DebtPaymentType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PayReceivedType;
import ths.dms.core.entities.enumtype.PaymentStatus;
import ths.dms.core.entities.enumtype.PaymentType;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailType;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailVO;
import ths.dms.core.entities.enumtype.PoAutoGroupFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupShopMapFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupVO;
import ths.dms.core.entities.enumtype.PoDisStatus;
import ths.dms.core.entities.enumtype.PoDvStatus;
import ths.dms.core.entities.enumtype.PoObjectType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.enumtype.ReceiptType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTotalVOFilter;
import ths.dms.core.entities.filter.PoVnmFilterBasic;
import ths.dms.core.entities.vo.CreatePoVnmDetailVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.MapVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PoAuto01VO;
import ths.dms.core.entities.vo.PoAutoGroupByCatVO;
import ths.dms.core.entities.vo.PoAutoGroupShopMapVO;
import ths.dms.core.entities.vo.PoAutoVO;
import ths.dms.core.entities.vo.PoCSVO;
import ths.dms.core.entities.vo.PoConfirmVO;
import ths.dms.core.entities.vo.PoDVKHVO;
import ths.dms.core.entities.vo.PoSecVO;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.entities.vo.PoVnmDetailVO;
import ths.dms.core.entities.vo.PoVnmStockInVO;
import ths.dms.core.entities.vo.ProductInfoVOEx;
import ths.core.entities.vo.rpt.RptF1DataVO;
import ths.core.entities.vo.rpt.RptF1Lv01VO;
import ths.core.entities.vo.rpt.RptPoAutoDataVO;
import ths.core.entities.vo.rpt.RptPoAutoLV01VO;
import ths.core.entities.vo.rpt.RptPoAutoLV02VO;
import ths.core.entities.vo.rpt.RptPoAutoLV03VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.dao.ApParamDAO;
import ths.dms.core.dao.BankDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.DebitDAO;
import ths.dms.core.dao.DebitDetailDAO;
import ths.dms.core.dao.PayReceivedDAO;
import ths.dms.core.dao.PaymentDetailDAO;
import ths.dms.core.dao.PoAutoDAO;
import ths.dms.core.dao.PoAutoDetailDAO;
import ths.dms.core.dao.PoAutoGroupDAO;
import ths.dms.core.dao.PoAutoGroupDetailDAO;
import ths.dms.core.dao.PoAutoGroupShopMapDAO;
import ths.dms.core.dao.PoManualDAO;
import ths.dms.core.dao.PoVnmDAO;
import ths.dms.core.dao.PoVnmDetailDAO;
import ths.dms.core.dao.PoVnmLotDAO;
import ths.dms.core.dao.PriceDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.ProductLotDAO;
import ths.dms.core.dao.SaleDayDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.ShopLockDAO;
import ths.dms.core.dao.StaffDAO;
import ths.dms.core.dao.StockTotalDAO;
import ths.dms.core.dao.WareHouseDAO;
import ths.dms.core.dao.repo.IRepository;

public class PoMgrImpl implements PoMgr {

	@Autowired
	ProductDAO productDAO;

	@Autowired
	PoVnmDAO poVnmDAO;

	@Autowired
	PoVnmDetailDAO poVnmDetailDAO;

	@Autowired
	PoAutoDAO poAutoDAO;

	@Autowired
	PoAutoDetailDAO poAutoDetailDAO;

	@Autowired
	PoVnmLotDAO poVnmLotDAO;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	ApParamDAO apParamDAO;

	@Autowired
	ProductLotDAO productLotDAO;

	@Autowired
	PriceDAO priceDAO;

	@Autowired
	StockTotalDAO stockTotalDAO;

	@Autowired
	ProductMgr productMgr;

	@Autowired
	SaleDayMgr saleDayMgr;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	DebitDetailDAO debitDetailDAO;

	@Autowired
	SaleDayDAO saleDayDAO;

	@Autowired
	StaffDAO staffDAO;
	
	@Autowired
	DebitDAO debitDAO;
	
	@Autowired
	PoAutoGroupDAO poAutoGroupDAO;
	
	@Autowired
	PoAutoGroupDetailDAO poAutoGroupDetailDAO;
	
	@Autowired
	PoAutoGroupShopMapDAO poAutoGroupShopMapDAO;
	
	@Autowired
	PayReceivedDAO payReceivedDAO;
	
	@Autowired
	private IRepository repo;
	
	@Autowired
	private PaymentDetailDAO paymentDetailDAO;
	
	@Autowired
	ShopLockDAO shopLockDAO;
	
	@Autowired
	BankDAO bankDAO;
	
	@Autowired
	WareHouseDAO wareHouseDAO;
	
	@Autowired
	PoManualDAO poManualDAO;
	
	@Override
	public PoAuto getPoAutoById(Long id) throws BusinessException {
		try {
			if (id == null) {
				throw new IllegalArgumentException("id is null");
			}
			return poAutoDAO.getPoAutoById(id);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}

	}

	@Override
	public PoAutoGroupShopMap getPoAutoGroupShopMapById(Long id) throws BusinessException {
		try {
			if (id == null) {
				throw new IllegalArgumentException("id is null");
			}
			return poAutoDAO.getPoAutoGroupShopMapById(id);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}

	}
	
	@Override
	public PoVnm getPoVnmById(Long id) throws BusinessException {
		try {
			if (id == null) {
				throw new IllegalArgumentException("id is null");
			}
			return poVnmDAO.getPoVnmById(id);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<PoAuto> getListPoAuto(KPaging<PoAuto> kPaging, Long shopId,
			String poAutoNumber, Date fromDate, Date toDate,
			ApprovalStatus status) throws BusinessException {
		try {
			return poAutoDAO.getListPoVnm(kPaging, shopId, poAutoNumber,
					fromDate, toDate, status);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<PoAutoDetail> getListPoAutoDetail(KPaging<PoAutoDetail> kPaging, Long poAutoId)
			throws BusinessException {
		try {
			return poAutoDetailDAO.getListPoAutoDetail(kPaging, poAutoId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	/*@Override
	public List<PoAutoVO> getListPoAuto(Long shopId, Boolean checkAll)
			throws BusinessException {
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (checkAll == null) {
				throw new IllegalArgumentException("checkAll is null");
			}
			Integer currSaleDay = saleDayMgr.getNumberWorkingDayByFromDateAndToDate(
					commonDAO.getFirstDateOfMonth(), commonDAO.getSysDate());
			Integer saleDay = saleDayDAO.getSaleDayByDate(commonDAO
					.getSysDate());
			if (saleDay == null) {
				throw new IllegalArgumentException("saleDay is null");
			}
			List<PoAutoVO> l = productDAO.getListPoAuto(shopId, checkAll);
			for (PoAutoVO poAutoVO : l) {
				poAutoVO.setCurrSaleDay(currSaleDay);
				poAutoVO.setSaleDay(saleDay);
				// poAutoVO.setPrice(priceDAO.getPriceById(poAutoVO.getPriceId()));
			}
			return l;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}*/
	
	@Override
	public List<PoAutoVO> getListPoAuto(Long shopId, Boolean checkAll)
			throws BusinessException {
		if (shopId == null) {
			throw new BusinessException("shopId is null");
		}
		if (checkAll == null) {
			throw new BusinessException("checkAll is null");
		}

		try {
			Date now = commonDAO.getSysDate();
			Date firstOfMonth = commonDAO.getFirstDateOfMonth();
			Integer currSaleDay = saleDayMgr.getNumberWorkingDayByFromDateAndToDate(firstOfMonth, now);
			Integer saleDay = saleDayDAO.getSaleDayByDate(now);
			if (saleDay == null) {
				throw new IllegalArgumentException("saleDay is null");
			}

			List<Object> inParams = new ArrayList<Object>();

			inParams.add("shopId");
			inParams.add(shopId.toString());
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("checkAll");
			inParams.add(checkAll.compareTo(Boolean.TRUE) == 0 ? 1 : 0);
			inParams.add(StandardBasicTypes.INTEGER);

			inParams.add("currSaleDay");
			inParams.add(currSaleDay);
			inParams.add(StandardBasicTypes.INTEGER);

			inParams.add("saleDay");
			inParams.add(saleDay);
			inParams.add(StandardBasicTypes.INTEGER);

			List<PoAutoVO> lstResult = repo.getListByNamedQuery(PoAutoVO.class, "PKG_PO_AUTO.PRO_CREATE_POAUTO", inParams);

			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<PoVnm> getListPoDVKH(PoVnmFilter filter) throws BusinessException {
		try {
			filter.setPoType(PoType.PO_CUSTOMER_SERVICE);
//			filter.setSortField("INVOICE_NUMBER desc");
			filter.setIsLikePoNumber(false);
			
//			return poVnmDAO.getListPoVnm(null, PoType.PO_CUSTOMER_SERVICE,
//					shopId, poAutoNumber, fromDate, toDate, poStatus, null,
//					hasFindChildShop, "INVOICE_NUMBER desc",false);
			
			return poVnmDAO.getListPoVnm(filter, null);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<PoVnm> getListPoConfirm() throws BusinessException {
		try {
			PoVnmFilter filter = new PoVnmFilter();
			filter.setPoType(PoType.PO_CONFIRM);
//			filter.setSortField("INVOICE_NUMBER desc");
			filter.setIsLikePoNumber(false);
			filter.setHasFindChildShop(false);
			//return poVnmDAO.getListPoVnm(null, PoType.PO_CONFIRM, null, null,
					//null, null, null, null, false, "INVOICE_NUMBER desc",false);
			return poVnmDAO.getListPoVnm(filter, null);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Transactional
	@Override
	public boolean setSuspendPoVNM(List<PoVnm> listPo, Long shopId) throws BusinessException {
		try {
			List<PoVnm> listPoVnmTmp = new ArrayList<PoVnm>();
			for (PoVnm po : listPo) {
				PoVnm child = poVnmDAO.getPoVnmByIdForUpdate(po.getId());
                if(child.getStatus().equals(PoVNMStatus.SUSPEND)) {
                	throw new BusinessException("status aldready suspend");
                } else{
                	child.setStatus(PoVNMStatus.SUSPEND);
                	
                	//TUNGTT set ngay chot
                	//child.setModifyDate(new Date());
                	Date lockDate = shopLockDAO.getApplicationDate(shopId);
                	child.setModifyDate(lockDate);
                	child.setHoldDate(lockDate);
                	
    				listPoVnmTmp.add(po);
                }
			}
			poVnmDAO.updatePoVnm(listPoVnmTmp);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	@Override
	public List<PoVnm> getListPoConfirmByCondition(PoVnmFilter filter) throws BusinessException {
		try {
			return poVnmDAO.getListPoVnm(filter, null);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author vuongmq
	 * @date 07/08/2015
	 * @description lay danh sach po confirm theo filter
	 * Dung cho Lay danh sach Po confirm dieu chinh hoa don
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public List<PoVnm> getListPoConfirmByFilter(PoVnmFilter filter) throws BusinessException {
		try {
			return poVnmDAO.getListPoConfirmByFilter(filter);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	/*
	 * (non-Javadoc) lay danh sach PO confirm author: ThanhNN co truyen len
	 * pagging
	 * 
	 * @see ths.dms.core.business.PoMgr#getListPoConfirm(java.lang.String,
	 * java.lang.String, java.sql.Date, java.sql.Date, java.lang.String)
	 */
	
	/***
	 * Lay danh sach ben nhap hang gom (don nhap va don tra) 
	 * @author vuongmq
	 * @date 10/08/2015
	 * @since 27/01/2016
	 */
	@Override
	public ObjectVO<PoVnmStockInVO> getListObjectVOPoConfirm(PoVnmFilter filter) throws BusinessException {
		try {
			//List<PoVnm> listPo = poVnmDAO.getListPoVnm(filter, kPaging);
			List<PoVnmStockInVO> listPo = poVnmDAO.getListPoConfirmPoVnmByFilter(filter);
			ObjectVO<PoVnmStockInVO> vo = new ObjectVO<PoVnmStockInVO>();
			vo.setkPaging(filter.getkPagingPoStockInVO());
			vo.setLstObject(listPo);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.PoMgr#updatePoVnm(ths.dms.core.entities
	 * .PoVnm)
	 */
	@Override
	public void updatePoVnm(PoVnm poVnm) throws BusinessException {

		try {
			poVnmDAO.updatePoVnm(poVnm);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/**
	 * @author vuongmq
	 * @date 11/08/2015
	 * @desciption Nhap /tra hang ung voi quantity input
	 * @param filter
	 * @throws BusinessException
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateInputPoVnm(PoVnmFilterBasic<PoVnm> filter) throws BusinessException {
		if (filter.getPoVnmId() == null) {
			throw new IllegalArgumentException("poVnmId is null");
		}
		if (filter.getStatus() == null) {
			throw new IllegalArgumentException("status is null");
		}
		if (filter.getPoCoNumber() == null) {
			throw new IllegalArgumentException("PONumber is null");
		}
		if (filter.getInvoiceNumber() == null) {
			throw new IllegalArgumentException("invoice_number is null");
		}
		if (filter.getDiscount() == null) {
			throw new IllegalArgumentException("discount is null");
		}
		try {
			/*Shop shop = shopDAO.getShopById(filter.getShopId());
			if (shop == null) {
				throw new IllegalArgumentException("get getShopById is null");
			}*/
			
			// lay Povnm_id
			PoVnm poVnm = poVnmDAO.getPoVnmById(filter.getPoVnmId());
			if (poVnm == null) {
				throw new IllegalArgumentException("get poVnm is null");
			}
			Shop shop = poVnm.getShop();
			if (shop == null) {
				throw new IllegalArgumentException("get getShop() of Povnm is null");
			}
			Date sys = commonDAO.getSysDate();
			Date lockDate = shopLockDAO.getNextLockedDay(shop.getId());
			if (lockDate == null) {
				lockDate = sys;
			}
			String saleOrderNumber = poVnm.getSaleOrderNumber();
			// update PO DVKH
			PoVnmFilter filterPoDVKH = new PoVnmFilter();
			if (poVnm.getFromPoVnmId() != null) {
				filterPoDVKH.setFromPoVnmId(poVnm.getFromPoVnmId().getId());
			}/* else {
				filterPoDVKH.setOrderNumber(saleOrderNumber);
			}*/
			if (PoType.PO_CONFIRM.equals(poVnm.getType())) {
				filterPoDVKH.setPoType(PoType.PO_CUSTOMER_SERVICE);
			} else if (PoType.RETURNED_SALES_ORDER.equals(poVnm.getType())) {
				filterPoDVKH.setPoType(PoType.PO_CUSTOMER_SERVICE_RETURN);
			}
			filterPoDVKH.setObjectType(PoObjectType.NPP);
			PoVnm poAuto = poVnmDAO.getPoDVKHBySaleOrderNumber(filterPoDVKH);
			Integer quantityTotalInput = 0;
			BigDecimal amountTotalInput = BigDecimal.ZERO;
			Integer debitQuantity = 0;
			BigDecimal debitAmount = BigDecimal.ZERO;
			// lay danh sach povnm_detail cua PO confirm
			for (int i = 0, sz = filter.getLstPovnmDetail().size(); i < sz; i++) {
				PoVnmDetailLotVO pDetailVOInput = filter.getLstPovnmDetail().get(i); // chi co: poVnmDetailLotId, wareHouseId, quantity 
				PoVnmDetail poVnmDetail = poVnmDetailDAO.getPoVnmDetailById(pDetailVOInput.getPoVnmDetailLotId());
				if (poVnmDetail != null) {
					if (poVnmDetail.getWarehouse() == null) {
						//voi dong po_vnm_detail khong kho; lay warehouse da chon tren combobox
						Warehouse warehouse = commonDAO.getEntityById(Warehouse.class, pDetailVOInput.getWareHouseId());
						if (warehouse == null) {
							throw new BusinessException("KHONG_CO_WAREHOUSE-" + (i + 1));
						}
						poVnmDetail.setWarehouse(warehouse); // gan warehouse moi
					}
					// cap nhat ton kho stock_total
					StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(poVnmDetail.getProduct().getId(), StockObjectType.SHOP, shop.getId(), poVnmDetail.getWarehouse().getId());
					if (PoType.PO_CONFIRM.equals(poVnm.getType())) {
						/***type 2: nhap hang */
						if (stockTotal == null) {
							stockTotal = new StockTotal();
							stockTotal.setProduct(poVnmDetail.getProduct());
							stockTotal.setQuantity(pDetailVOInput.getQuantity());
							stockTotal.setAvailableQuantity(pDetailVOInput.getQuantity());
							stockTotal.setObjectType(StockObjectType.SHOP);
							stockTotal.setObjectId(shop.getId());
							stockTotal.setShop(shop);
							stockTotal.setWarehouse(poVnmDetail.getWarehouse());
							stockTotal.setCreateDate(sys);
							stockTotal.setCreateUser(filter.getUserName());
							stockTotalDAO.createStockTotal(stockTotal);
						} else {
							if (stockTotal.getQuantity() == null) {
								stockTotal.setQuantity(0);
							}
							if (stockTotal.getAvailableQuantity() == null) {
								stockTotal.setAvailableQuantity(0);
							}
							stockTotal.setQuantity(stockTotal.getQuantity() + pDetailVOInput.getQuantity());
							stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() + pDetailVOInput.getQuantity());
							stockTotal.setUpdateDate(sys);
							stockTotal.setUpdateUser(filter.getUserName());
							stockTotalDAO.updateStockTotal(stockTotal);	
						}
					} else if (PoType.RETURNED_SALES_ORDER.equals(poVnm.getType())) {
						/***type 3: tra hang */
						if (stockTotal == null) {
							// thong bao loi khong co ton kho trong stock_total
							throw new BusinessException("KHONG_CO_STOCKTOTAL-" + (i + 1));
						} else {
							if (stockTotal.getQuantity() == null) {
								stockTotal.setQuantity(0);
							}
							if (stockTotal.getAvailableQuantity() == null) {
								stockTotal.setAvailableQuantity(0);
							}
							stockTotal.setQuantity(stockTotal.getQuantity() - pDetailVOInput.getQuantity());
							stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() - pDetailVOInput.getQuantity());
							if (stockTotal.getQuantity() < 0) {
								// thong bao loi vuot qua so luong ton kho
								throw new BusinessException("TON_KHO_KHONG_DU-" + (i + 1));
							}
							/*if (stockTotal.getAvailableQuantity() < 0) {
								// thong bao loi vuot qua so luong ton kho dap ung
								throw new BusinessException("TON_KHO_DAP_UNG_KHONG_DU-" + (i + 1));
							}*/
							stockTotal.setUpdateDate(sys);
							stockTotal.setUpdateUser(filter.getUserName());
							stockTotalDAO.updateStockTotal(stockTotal);	
						}
					}
					
					/*** Cap nhat gia tri po_vnm_detail cua Po Confirm **/
					if (poVnmDetail.getQuantityReceived() == null) {
						poVnmDetail.setQuantityReceived(0);
					}
					if (poVnmDetail.getPriceValue() == null) {
						poVnmDetail.setPriceValue(BigDecimal.ZERO);
					}
					Integer quantityRecivedAdd = poVnmDetail.getQuantityReceived() + pDetailVOInput.getQuantity();
					if (quantityRecivedAdd > poVnmDetail.getQuantity()) {
						if (PoType.PO_CONFIRM.equals(poVnm.getType())) {
							// thong bao loi vuot qua so luong nhap
							throw new BusinessException("VUOT_QUA_SO_LUONG_NHAP-" + (i + 1));
						} else if (PoType.RETURNED_SALES_ORDER.equals(poVnm.getType())) {
							throw new BusinessException("VUOT_QUA_SO_LUONG_TRA-" + (i + 1));
						}
					}
					poVnmDetail.setQuantityReceived(quantityRecivedAdd);
					BigDecimal quantityReceived = BigDecimal.valueOf(poVnmDetail.getQuantityReceived());
					BigDecimal amountReceived = poVnmDetail.getPriceValue().multiply(quantityReceived);
					poVnmDetail.setAmountReceived(amountReceived);
					quantityTotalInput = quantityTotalInput + quantityRecivedAdd; // so luong moi nhap vao
					amountTotalInput = amountTotalInput.add(amountReceived); // amount tinh moi
					// Chi lay amount = so luong nhap vao * gia; debit amount moi vao
					BigDecimal debitInput = poVnmDetail.getPriceValue().multiply(BigDecimal.valueOf(pDetailVOInput.getQuantity()));
					debitAmount = debitAmount.add(debitInput);
					debitQuantity = debitQuantity + pDetailVOInput.getQuantity();
					poVnmDetailDAO.updatePoVnmDetail(poVnmDetail);
					/*** Tao moi Po Detail Received (Input) khi nhap/ tra hang**/
					PoVnmDetailReceived poVnmDetailReceived = new PoVnmDetailReceived();
					poVnmDetailReceived.setType(poVnm.getType());
					poVnmDetailReceived.setPoVnm(poVnm);
					poVnmDetailReceived.setPoVnmDetail(poVnmDetail);
					poVnmDetailReceived.setPriceValue(poVnmDetail.getPriceValue());
					poVnmDetailReceived.setPrice(poVnmDetail.getPrice());
					poVnmDetailReceived.setWarehouse(poVnmDetail.getWarehouse());
					poVnmDetailReceived.setProduct(poVnmDetail.getProduct());
					poVnmDetailReceived.setProductType(poVnmDetail.getProductType());
					poVnmDetailReceived.setQuantityReceived(pDetailVOInput.getQuantity());
					poVnmDetailReceived.setAmountReceived(debitInput);
					poVnmDetailReceived.setImportDate(sys);
					poVnmDetailReceived.setReceivedDate(sys);
					poVnmDetailReceived.setCreateDate(sys);
					poVnmDetailReceived.setDeliveryDate(filter.getDeliveryDate());
					poVnmDetailReceived.setCreateUser(filter.getUserName());
					commonDAO.createEntity(poVnmDetailReceived);
					/*** Cap nhat Detail cua Po DVKH **/
					if (poAuto != null) {
						if (poVnmDetail.getProduct() != null && poVnmDetail.getProductType() != null) {
							PoVnmDetail poAutoDetail = poVnmDetailDAO.getPoVnmDetail(poAuto.getId(), poVnmDetail.getProduct().getId(), poVnmDetail.getProductType().getValue());
							if (poAutoDetail != null) {
								if (poAutoDetail.getQuantityReceived() == null) {
									poAutoDetail.setQuantityReceived(0);
								}
								if (poAutoDetail.getPriceValue() == null) {
									poAutoDetail.setPriceValue(BigDecimal.ZERO);
								}
								poAutoDetail.setQuantityReceived(poAutoDetail.getQuantityReceived() + pDetailVOInput.getQuantity());
								if (poAutoDetail.getAmountReceived() == null) {
									poAutoDetail.setAmountReceived(BigDecimal.ZERO);
								}
								BigDecimal quantityAutoReceived = BigDecimal.valueOf(poAutoDetail.getQuantityReceived());
								BigDecimal amountAutoReceived = poAutoDetail.getPriceValue().multiply(quantityAutoReceived);
								poAutoDetail.setAmountReceived(amountAutoReceived);
								// cap nhat ware house
								if (poAutoDetail.getWarehouse() == null) {
									poAutoDetail.setWarehouse(poVnmDetail.getWarehouse());
								}
								poVnmDetailDAO.updatePoVnmDetail(poAutoDetail);
							}
						}
					}
				} // end if (poVnmDetail != null) {
			} // end for (int i = 0, sz = filter.getLstPovnmDetail().size(); i < sz; i++) {
			
			// update PoVnm po Confirm
			if (PoVNMStatus.IMPORTING.getValue().equals(filter.getStatus())) {
				if (PoVNMStatus.NOT_IMPORT.equals(poVnm.getStatus())) {
					//trang thai: 1: da nhap/tra mot phan;
					poVnm.setStatus(PoVNMStatus.IMPORTING);
				}
			} else {
				//trang thai: 2: da nhap/tra
				poVnm.setStatus(PoVNMStatus.IMPORTED);
				/** vuongmq; 02/02/2016; đơn trả: status của ASN (type = 2) sang đã nhập xong -> Cập nhật PO của ASN (po_auto_number, join bảng PO với po_number) sang trạng thái đã nhập (status = 1 & approved_step = 4). */
				if (PoType.RETURNED_SALES_ORDER.equals(poVnm.getType())) {
					Po po = poManualDAO.getPoByPoNumber(poVnm.getPoAutoNumber());
					if (po != null) {
						po.setStatus(ActiveType.RUNNING);
						po.setApprovedStep(ApprovalStepPo.COMPLETED);
						po.setUpdateDate(sys);
						po.setUpdateUser(filter.getUserName());
						poManualDAO.updatePo(po);
					}
				}
			}
			poVnm.setInvoiceNumber(filter.getInvoiceNumber());
			poVnm.setQuantityReceived(quantityTotalInput);
			poVnm.setAmountReceived(amountTotalInput);
			if (poVnm.getDiscount() == null) {
				poVnm.setDiscount(BigDecimal.ZERO);
			}
			poVnm.setDiscount(poVnm.getDiscount().add(filter.getDiscount()));
			poVnm.setDeliveryDate(filter.getDeliveryDate());
			poVnm.setInvoiceDate(filter.getInvoiceDate());
			//TUNGTT set ngay chot
			poVnm.setObjectType(PoObjectType.NPP);
			poVnm.setModifyDate(lockDate);
			poVnm.setImportDate(lockDate);
			poVnm.setInvoiceDate(lockDate);
			poVnm.setImportUser(filter.getUserName());
			poVnmDAO.updatePoVnm(poVnm);
			
			/**Lay danh sach po Confirm da nhap de lay tong so luong quantity; 
			 * check truong hop chon status: 2 Hoan tat nhap hang, ngc lai luon la: 1 Da nhap 1 phan
			 */
			PoVnmFilter filterPoConfirm = new PoVnmFilter();
			if (poVnm.getFromPoVnmId() != null) {
				filterPoConfirm.setFromPoVnmId(poVnm.getFromPoVnmId().getId());
			}/* else {
				filterPoConfirm.setOrderNumber(saleOrderNumber);
			}*/
			filterPoConfirm.setPoType(poVnm.getType());
			filterPoConfirm.setObjectType(PoObjectType.NPP);
			//filterPoConfirm.setPoStatus(PoVNMStatus.IMPORTED);
			List<PoVnm> listPoVnm = poVnmDAO.getListPoConfirmBySaleOrderNumber(filterPoConfirm);
			Integer totalqualtity = 0;
			boolean flagInputFinish = true; // true: all cac PO confirm deu da nhap, ngc lai false
			for (PoVnm item : listPoVnm) {
				if (item != null) {
					if (item.getQuantity() != null) {
						totalqualtity += item.getQuantity();
					}
					if (!PoVNMStatus.IMPORTED.equals(item.getStatus())) {
						flagInputFinish = false;
						break;
					}
				}
			}
			// update Po DVKH
			if (poAuto != null) {
				if (PoVNMStatus.IMPORTING.getValue().equals(filter.getStatus())) {
					if (PoVNMStatus.NOT_IMPORT.equals(poAuto.getStatus())) {
						//trang thai: 1: da nhap/tra mot phan;
						poAuto.setStatus(PoVNMStatus.IMPORTING);
					}
				} else {
					//trang thai: 2: da nhap/tra
					if (poAuto.getQuantity().compareTo(totalqualtity) == 0 && flagInputFinish) {
						poAuto.setStatus(PoVNMStatus.IMPORTED);
						/** vuongmq; 02/02/2016; đơn nhập: status của sale_order (type = 1) sang đã nhập xong -> Cập nhật PO của sale_order (po_auto_number, join bảng PO với po_number) sang trạng thái đã nhập (status = 1 & approved_step = 4). */
						if (PoType.PO_CONFIRM.equals(poVnm.getType())) {
							Po po = poManualDAO.getPoByPoNumber(poAuto.getPoAutoNumber());
							if (po != null) {
								po.setStatus(ActiveType.RUNNING);
								po.setApprovedStep(ApprovalStepPo.COMPLETED);
								po.setUpdateDate(sys);
								po.setUpdateUser(filter.getUserName());
								poManualDAO.updatePo(po);
							}
						}
					} else {
						poAuto.setStatus(PoVNMStatus.IMPORTING);
					}
				}
				//TUNGTT set ngay chot
				poAuto.setModifyDate(lockDate);
				BigDecimal amountReceived = poAuto.getAmountReceived();
				if (amountReceived == null) {
					amountReceived = BigDecimal.ZERO;
				}
				// cong vao gia tri 1 phan dang nhap
				//poAuto.setAmountReceived(amountReceived.add(poVnm.getAmountReceived()));
				poAuto.setAmountReceived(amountReceived.add(debitAmount));
				Integer quantityReceived = poAuto.getQuantityReceived();
				if (quantityReceived == null) {
					quantityReceived = 0;
				}
				//cong vao gia tri 1 phan dang nhap
				//poAuto.setQuantityReceived(quantityReceived + poVnm.getQuantityReceived());
				poAuto.setQuantityReceived(quantityReceived + debitQuantity);
				//TUNGTT
				BigDecimal discountAuto = poAuto.getDiscount();
				if (discountAuto == null) {
					discountAuto = BigDecimal.ZERO;
				}
				poAuto.setDiscount(discountAuto.add(filter.getDiscount()));
				BigDecimal totalAuto = poAuto.getTotal();
				if (totalAuto == null) {
					totalAuto = BigDecimal.ZERO;
				}
				poAuto.setTotal(totalAuto.subtract(filter.getDiscount()));
				//TUNGTT
				poVnmDAO.updatePoVnm(poAuto);
			}

			// cap nha Debit cho Po confirm
			BigDecimal totalDetail = (debitAmount.subtract(filter.getDiscount())).multiply(new BigDecimal("1.1"));
			Debit debit = debitDAO.getDebitForUpdate(shop.getId(), DebitOwnerType.SHOP); // khong có debit thi tao debit
			if (debit == null) {
//				throw new IllegalArgumentException("debit is null");
				throw new BusinessException("CONG_NO_CHUA_XU_LY-");
			}
			if (PoType.PO_CONFIRM.equals(poVnm.getType())) {
				debit.setTotalAmount(debit.getTotalAmount().add(totalDetail));
				debit.setTotalDebit(debit.getTotalDebit().add(totalDetail));
			} else if (PoType.RETURNED_SALES_ORDER.equals(poVnm.getType())) {
				debit.setTotalAmount(debit.getTotalAmount().add(totalDetail.negate()));
				debit.setTotalDebit(debit.getTotalDebit().add(totalDetail.negate()));
			}
			debitDAO.updateDebit(debit);
			
			DebitDetail debitDetail = debitDetailDAO.getDebitDetailForUpdatePoVNM(poVnm.getId(), debit.getId());
			if (debitDetail != null) {
				// cap nhat debit_detail cho NPP
				if (PoType.PO_CONFIRM.equals(poVnm.getType())) {
					/*** Nhap hang*/
					debitDetail.setAmount(debitDetail.getAmount().add(debitAmount));
					debitDetail.setTotal(debitDetail.getTotal().add(totalDetail));
					debitDetail.setRemain(debitDetail.getRemain().add(totalDetail));
				} else if (PoType.RETURNED_SALES_ORDER.equals(poVnm.getType())) {
					/*** Tra hang*/
					debitDetail.setAmount(debitDetail.getAmount().add(debitAmount.negate()));
					debitDetail.setTotal(debitDetail.getTotal().add(totalDetail.negate()));
					debitDetail.setRemain(debitDetail.getRemain().add(totalDetail.negate()));
				}
				debitDetail.setDiscount(debitDetail.getDiscount().add(filter.getDiscount()));
				debitDetail.setUpdateDate(sys);
				debitDetail.setUpdateUser(filter.getUserName());
				debitDetailDAO.updateDebitDetail(debitDetail);
			} else {
				// tao moi debit_detail cho NPP
				debitDetail = new DebitDetail();
				debitDetail.setFromObjectId(poVnm.getId());
				//debitDetail.setFromObjectNumber(poVnm.getPoCoNumber());
				debitDetail.setFromObjectNumber(poVnm.getSaleOrderNumber());
				debitDetail.setInvoiceNumber(poVnm.getInvoiceNumber());
				debitDetail.setDebit(debit);
				debitDetail.setCreateDate(sys);
				debitDetail.setCreateUser(filter.getUserName());
				debitDetail.setShop(shop);
				//TUNGTT set chot ngay
				debitDetail.setDebitDate(lockDate);
				if (PoType.PO_CONFIRM.equals(poVnm.getType())) {
					/*** Nhap hang*/
					debitDetail.setAmount(debitAmount);
					debitDetail.setTotal(totalDetail);
					debitDetail.setType(DebitDetailType.NHAP_VNM.getValue()); // lacnv1 - them type cua debit_detail
				} else if (PoType.RETURNED_SALES_ORDER.equals(poVnm.getType())) {
					/*** Tra hang*/
					debitDetail.setAmount(debitAmount.negate());
					debitDetail.setTotal(totalDetail.negate());
					debitDetail.setType(DebitDetailType.TRA_VNM.getValue());  // lacnv1 - them type cua debit_detail
				}
				debitDetail.setDiscount(filter.getDiscount());
				debitDetail.setRemain(debitDetail.getTotal());
				debitDetailDAO.createDebitDetail(debitDetail);
				
				// tra hang
				// tao debit
				// cap nhat shop debit
				/*Debit debit = debitDAO.getDebitForUpdate(shopId, DebitOwnerType.SHOP);
				if (debit == null) {
					throw new IllegalArgumentException("debit is null");
				}
				debit.setTotalAmount(debit.getTotalAmount().subtract(totalPoVnmAmountDis));
				debit.setTotalDebit(debit.getTotalDebit().subtract(totalPoVnmAmountDis));
				debitDAO.updateDebit(debit);
				// tao debit
				DebitDetail debitDetail = new DebitDetail();
				debitDetail.setFromObjectId(nPoVnm.getId());
				debitDetail.setFromObjectNumber(nPoVnm.getPoCoNumber());
				debitDetail.setDebit(debit);
				debitDetail.setAmount(totalPoVnmAmount.multiply(new BigDecimal(-1)));
				debitDetail.setTotal(totalPoVnmAmountDis.multiply(new BigDecimal(-1)));
				debitDetail.setDiscount(disCount);
				debitDetail.setRemain(debitDetail.getTotal());
				debitDetail.setCreateUser(user);
				debitDetail.setCreateDate(now);
				debitDetail.setType(DebitDetailType.TRA_VNM.getValue());  // lacnv1 - them type cua debit_detail
				debitDetail.setShop(shop);
				//debitDetail.setDebitDate(now); //
				//TUNGTT
				debitDetail.setDebitDate(lockDate);
				debitDetailDAO.createDebitDetail(debitDetail);*/
			}
			//------------------- ENd vuongmq---------/////////
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	
	// @Transactional(rollbackFor = Exception.class)
	// @Override
	// public void createShopPoAuto(PoAuto poAuto,
	// List<PoAutoDetail> listPoAutoDetail) throws BusinessException {
	// try {
	// poAuto.setPoAutoDate(commonDAO.getSysDatetime());
	// poAuto.setModifyDate(null);
	// poAuto = poAutoDAO.createPoAuto(poAuto);
	// poAuto.setPoAutoNumber(getPoAutoNumber(poAuto.getId()));
	// poAutoDAO.updatePoAuto(poAuto);
	// for (PoAutoDetail poAutoDetail : listPoAutoDetail) {
	// poAutoDetail.setPoAuto(poAuto);
	// poAutoDetailDAO.createPoAutoDetail(poAutoDetail);
	// }
	// } catch (DataAccessException e) {
	// throw new BusinessException(e);
	// }
	// }
	//15, 176, true
	
	/*@Transactional(rollbackFor = Exception.class)
	@Override
	public void createShopPoAuto(Long shopId, Long staffId,
			Boolean checkAllProductType) throws BusinessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (staffId == null) {
			throw new IllegalArgumentException("staffId is null");
		}
		if (checkAllProductType == null) {
			throw new IllegalArgumentException(
					"checkAllProductType is null");
		}
		
		try {
			Shop shop = shopDAO.getShopById(shopId);
			
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			Staff staff = staffDAO.getStaffById(staffId);
			if (staff == null) {
				throw new IllegalArgumentException("staff is null");
			}
			
			Date now = commonDAO.getSysDate();
			List<PoAutoVO> listPoAutoVo = getListPoAuto(shopId,
					checkAllProductType);
			
			ArrayList<PoAutoDetail> lstPdInfoAB = new ArrayList<PoAutoDetail>();
			BigDecimal totalPdInfoABAmt = BigDecimal.ZERO;
			ArrayList<PoAutoDetail> lstPdInfoCE = new ArrayList<PoAutoDetail>();
			BigDecimal totalPdInfoCEAmt = BigDecimal.ZERO;
			ArrayList<PoAutoDetail> lstPdInfoD = new ArrayList<PoAutoDetail>();
			BigDecimal totalPdInfoDAmt = BigDecimal.ZERO;
			
			for (PoAutoVO poAutoVo : listPoAutoVo) {
				if (poAutoVo.getQuatity() != null && poAutoVo.getQuatity() > 0) {
					Product product = productDAO.getProductByCode(poAutoVo
							.getProductCode());
					
					if (product == null) {
						throw new IllegalArgumentException("product is null");
					}
					if ("ABCDE".indexOf(product.getCat().getProductInfoCode()
							.toUpperCase()) < 0) {
						continue;
					}
					Price price = priceDAO.getPriceById(poAutoVo.getPriceId());
					if (price == null) {
						throw new IllegalArgumentException("price is null");
					}
					// tao PoAutoDetail
					PoAutoDetail poAutoDetail = new PoAutoDetail();
					poAutoDetail.setAmount(poAutoVo.getAmount());
					poAutoDetail.setConvfact(poAutoVo.getConvfact());
					poAutoDetail.setCreateDate(now);
					poAutoDetail.setDayPlan(poAutoVo.getDayPlan());
					poAutoDetail
							.setDayReservePlan(poAutoVo.getDayReservePlan());
					poAutoDetail.setDayReserveReal(new BigDecimal(poAutoVo
							.getDayReserveReal()));
					poAutoDetail.setExport(poAutoVo.getExpQty());
					poAutoDetail.setImportQuantity(poAutoVo.getImpQty());
					poAutoDetail.setLead(poAutoVo.getLead());
					poAutoDetail.setMonthCumulate(poAutoVo.getMonthCumulate());
					poAutoDetail.setMonthPlan(poAutoVo.getMonthPlan());
					poAutoDetail.setNext(poAutoVo.getNext());
					poAutoDetail.setOpenStock(poAutoVo.getOpenStockTotal());
					poAutoDetail.setPoAutoDate(now);
					poAutoDetail.setPriceValue(poAutoVo.getPriceValue());
					poAutoDetail.setPrice(price);
					poAutoDetail.setProduct(product);
					poAutoDetail.setQuantity(poAutoVo.getQuatity()
							* poAutoVo.getConvfact());
					poAutoDetail
							.setRequimentStock(poAutoVo.getRequimentStock());
					poAutoDetail
							.setSafetyStockMin(poAutoVo.getSafetyStockMin());
					poAutoDetail.setStock(poAutoVo.getStockQty());
					poAutoDetail
							.setStockPoConfirm(poAutoVo.getQtyGoPoConfirm());
					poAutoDetail.setStockPoDvkh(poAutoVo.getStockPoDvkh());
					poAutoDetail.setWarning(poAutoVo.getWarning());
					// nganh hang
					if ("AB".indexOf(product.getCat().getProductInfoCode()
							.toUpperCase()) >= 0) {
						totalPdInfoABAmt = totalPdInfoABAmt.add(poAutoVo
								.getAmount());
						lstPdInfoAB.add(poAutoDetail);
					} else if ("CE".indexOf(product.getCat()
							.getProductInfoCode().toUpperCase()) >= 0) {
						totalPdInfoCEAmt = totalPdInfoCEAmt.add(poAutoVo
								.getAmount());
						lstPdInfoCE.add(poAutoDetail);
					} else if ("D".indexOf(product.getCat()
							.getProductInfoCode().toUpperCase()) >= 0) {
						totalPdInfoDAmt = totalPdInfoDAmt.add(poAutoVo
								.getAmount());
						lstPdInfoD.add(poAutoDetail);
					}
				}
			}
			
			if (lstPdInfoAB.size() > 0) {
				PoAuto poAuto = new PoAuto();
				poAuto.setStatus(ApprovalStatus.NOT_YET);
				poAuto.setDiscount(BigDecimal.ZERO);
				poAuto.setPoAutoDate(now);
				poAuto.setShop(shop);
				poAuto.setStaff(staff);
				poAuto.setModifyDate(null);
				poAuto.setAmount(totalPdInfoABAmt);
				poAuto.setTotal(totalPdInfoABAmt);
				poAuto = poAutoDAO.createPoAuto(poAuto);
				poAuto.setPoAutoNumber(getPoAutoNumber(poAuto.getId()));
				poAutoDAO.updatePoAuto(poAuto);
				for (PoAutoDetail poAutoDetail : lstPdInfoAB) {
					poAutoDetail.setPoAuto(poAuto);
					poAutoDetailDAO.createPoAutoDetail(poAutoDetail);
				}
			}
			if (lstPdInfoCE.size() > 0) {
				PoAuto poAuto = new PoAuto();
				poAuto.setStatus(ApprovalStatus.NOT_YET);
				poAuto.setDiscount(BigDecimal.ZERO);
				poAuto.setPoAutoDate(now);
				poAuto.setShop(shop);
				poAuto.setStaff(staff);
				poAuto.setModifyDate(null);
				poAuto.setAmount(totalPdInfoCEAmt);
				poAuto.setTotal(totalPdInfoCEAmt);
				poAuto = poAutoDAO.createPoAuto(poAuto);
				poAuto.setPoAutoNumber(getPoAutoNumber(poAuto.getId()));
				poAutoDAO.updatePoAuto(poAuto);
				for (PoAutoDetail poAutoDetail : lstPdInfoCE) {
					poAutoDetail.setPoAuto(poAuto);
					poAutoDetailDAO.createPoAutoDetail(poAutoDetail);
				}
			}
			if (lstPdInfoD.size() > 0) {
				PoAuto poAuto = new PoAuto();
				poAuto.setStatus(ApprovalStatus.NOT_YET);
				poAuto.setDiscount(BigDecimal.ZERO);
				poAuto.setPoAutoDate(now);
				poAuto.setShop(shop);
				poAuto.setStaff(staff);
				poAuto.setModifyDate(null);
				poAuto.setAmount(totalPdInfoDAmt);
				poAuto.setTotal(totalPdInfoDAmt);
				poAuto = poAutoDAO.createPoAuto(poAuto);
				poAuto.setPoAutoNumber(getPoAutoNumber(poAuto.getId()));
				poAutoDAO.updatePoAuto(poAuto);
				for (PoAutoDetail poAutoDetail : lstPdInfoD) {
					poAutoDetail.setPoAuto(poAuto);
					poAutoDetailDAO.createPoAutoDetail(poAutoDetail);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		} catch (BusinessException e) {
			throw e;
		}
	}*/

	private String getPoAutoNumber(Long id) {
		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		String idText = "000000000000" + id.toString();
		return "PO" + idText.substring(idText.length() - 12);
	}

	private String getPoCoNumber(Long id) {
		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		String idText = "000000000000" + id.toString();
		return "CM" + idText.substring(idText.length() - 12);
	}

	@Override
	public ObjectVO<PoVnm> getListPoConfirmByPoCS(KPaging<PoVnm> kPaging, PoVnm poCS, PoObjectType objectType)
			throws BusinessException {
		try {
			ObjectVO<PoVnm> res = new ObjectVO<PoVnm>();
			List<PoVnm> lstPoVnm = poVnmDAO.getListPoConfirmByPoCS(kPaging, poCS.getSaleOrderNumber(), objectType, poCS.getType());
			res.setkPaging(kPaging);
			res.setLstObject(lstPoVnm);
			return res;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<PoVnm> getListPoConfirmByPoCSFromPoId(KPaging<PoVnm> kPaging, PoVnm poCS, PoObjectType objectType)
			throws BusinessException {
		try {
			ObjectVO<PoVnm> res = new ObjectVO<PoVnm>();
			List<PoVnm> lstPoVnm = poVnmDAO.getListPoConfirmByPoCSFromPoId(kPaging, poCS.getId(), objectType, poCS.getType());
			res.setkPaging(kPaging);
			res.setLstObject(lstPoVnm);
			return res;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public boolean checkValidateDatePoReturn(PoVnm poConfirm, Long shopId)
			throws BusinessException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date currentDate = null;
		Date importDate = null;
		ApParam apParam = null;
		int validaReturn = 0;

		try {
			//TUNGTT current la ngay chot
			//currentDate = commonDAO.getSysDate();
			currentDate = shopLockDAO.getNextLockedDay(shopId);
			if(currentDate == null){
				currentDate = commonDAO.getSysDate();
			}

			if (null == currentDate) {
				throw new Exception();
			} else {
				currentDate = sdf.parse(sdf.format(currentDate));
			}
		} catch (Exception ex) {
			throw new BusinessException("Can't get sysdate from server.", ex);
		}

		try {
			apParam = apParamDAO.getApParamByCondition(null, "NTVNM",ApParamType.TH, ActiveType.RUNNING);
			
			if (null == apParam) {
				return true;
			} else {
				if (apParam.getValue() == null || apParam.getValue().trim() == "") {
					return true;
				} else {
					validaReturn = Integer.parseInt(apParam.getValue().trim());
					
					if (0 == validaReturn) {
						return true;
					}
				}
			}
		} catch (Exception ex) {
			throw new BusinessException("Can't get AP_PARAM.", ex);
		}
		
		try {
			if (null == poConfirm || null == poConfirm.getImportDate()) {
				throw new Exception();
			} else {
				importDate = sdf.parse(sdf.format(poConfirm.getImportDate()));
			}
		} catch (Exception ex) {
			throw new BusinessException("Can't get POConfirm.", ex);
		}
		
		if (importDate.compareTo(DateUtility.addDate(currentDate,
				(-1 * validaReturn))) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<PoVnmLot> getListPoVnmLotByPoVnmId(Long poVnmId)
			throws BusinessException {
		try {
			if (poVnmId == null) {
				throw new IllegalArgumentException("poVnmId is null");
			}
			return poVnmLotDAO.getListPoVnmLotByPoVnmId(poVnmId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void refusePoAuto(List<PoAuto> listPoAuto) throws BusinessException {
		try {
			if (listPoAuto == null || listPoAuto.size() == 0)
				return;
			List<PoAuto> listPoAutoTmp = new ArrayList<PoAuto>();
			Date date = commonDAO.getSysDate();
            for (PoAuto poAutoTmp : listPoAuto) {
            	PoAuto poAuto = poAutoDAO.getPoAutoByIdForUpdate(poAutoTmp.getId());
                if(!poAuto.getStatus().equals(ApprovalStatus.NOT_YET)) {
                	throw new BusinessException(ExceptionCode.PO_AUTO_STATUS_NOT_YET);
                } else{
                	poAuto.setStatus(ApprovalStatus.DESTROY);
    				poAuto.setModifyDate(date);
    				listPoAutoTmp.add(poAuto);
                }
            }
			poAutoDAO.updatePoAuto(listPoAutoTmp);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void approvedPoAuto(List<PoAuto> listPoAuto) throws BusinessException {
		try {
			if (listPoAuto == null || listPoAuto.size() == 0) {
				return;
			}
			Date now = commonDAO.getSysDate();
			//PoAuto poAuto = null;
			//List<PoAuto> listPoAutoTmp = new ArrayList<PoAuto>();
            for (PoAuto poAuto : listPoAuto) {
            	//poAuto = poAutoDAO.getPoAutoByIdForUpdate(poAutoTmp.getId());
                if(!ApprovalStatus.NOT_YET.equals(poAuto.getStatus())) {
                	throw new BusinessException(ExceptionCode.PO_AUTO_STATUS_NOT_YET);
                }
                
            	poAuto.setStatus(ApprovalStatus.APPROVED);
				poAuto.setModifyDate(now);
				//listPoAutoTmp.add(poAuto);
            }
			//poAutoDAO.updatePoAuto(listPoAutoTmp);
            poAutoDAO.updatePoAuto(listPoAuto);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public boolean checkExistedPoVnmByPoCoNumber(String poCoNumber)
			throws BusinessException {
		try {
			return poVnmDAO.checkExistedPoVnmByPoCoNumber(poCoNumber);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}

	}

	@Override
	public List<PoAuto01VO> getListPoAuto01(Long shopId, String poAutoNumber, Date fromDate, Date toDate, ApprovalStatus status,
			boolean hasFindChildShop) throws BusinessException {
		try {
			return poAutoDAO.getListPoAuto01(shopId, poAutoNumber, fromDate, toDate, status, hasFindChildShop);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public boolean checkQuantityInStockTotal(long shopId, long productId,
			int quantity) throws BusinessException {
		try {
			return stockTotalDAO.checkQuantityInStockTotal(shopId, productId,
					quantity);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.business.PoMgr#getPoDVKH(java.lang.String)
	 */
	@Override
	public PoVnm getPoDVKH(String poAutoNumber) throws BusinessException {

		try {
			return poVnmDAO.getPoDVKH(poAutoNumber);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public boolean checkQuantityInProductLot(long shopId, long productId,
			int quantity, String lot) throws BusinessException {
		try {
			if (lot == null) {
				throw new IllegalArgumentException("lot is null");
			}
			return productLotDAO.checkQuantityInProductLot(shopId, productId,
					quantity, lot);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}

	}

	@Override
	public PoVnm createPoVnm(PoVnm poVnm) throws BusinessException {
		try {
			return poVnmDAO.createPoVnm(poVnm);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updatePoVnmDetail(PoVnmDetail poVnmDetail)
			throws BusinessException {
		try {
			poVnmDetailDAO.updatePoVnmDetail(poVnmDetail);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public PoVnmDetail getPoVnmDetailById(long id) throws BusinessException {
		try {
			return poVnmDetailDAO.getPoVnmDetailById(id);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}

	}

	@Override
	public void updateProductLot(ProductLot productLot)
			throws BusinessException {
		try {
			productLotDAO.updateProductLot(productLot);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ProductLot getProductLotById(long id) throws BusinessException {
		try {
			return productLotDAO.getProductLotById(id);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}

	}

	@Override
	public PoVnm getPoVnmByCode(String code) throws BusinessException {
		try {
			return poVnmDAO.getPoVnmByCode(code);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<PoDVKHVO> getPoDVKHforReport(Long shopId, Date fromDate, Date toDate, Long poVnmId) throws BusinessException {
		try {
			List<PoVnm> listPoDVKH = poVnmDAO.getListPoDVKHByShop(shopId, fromDate, toDate, poVnmId);
			if (null == listPoDVKH || listPoDVKH.size() <= 0) {
				return null;
			} else {
				List<PoDVKHVO> listData = new ArrayList<PoDVKHVO>();
				PoDVKHVO data = null;
				for (PoVnm poDVKH : listPoDVKH) {
					data = new PoDVKHVO();
					data.setSaleOrderNumber(poDVKH.getSaleOrderNumber());
					if (poDVKH.getType() != null) {
						data.setPoType(poDVKH.getType().getValue());
					}
					PoVnmFilter filter = new PoVnmFilter();
					filter.setShopId(shopId);
					filter.setPoVnmId(poDVKH.getId());
					filter.setPoType(poDVKH.getType());
					List<PoConfirmVO> listPoConfirm = poVnmDAO.getListPoConfirmForReport(filter);
					data.setListPoConfirmVo(listPoConfirm);
					listData.add(data);
				}
				return listData;
			}
		} catch (Exception ex) {
			throw new BusinessException("Loi trong qua trinh lay du lieu.", ex);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.business.PoMgr#getListPoVnmVoDetail(java.lang.Long)
	 */
	@Override
	public List<PoVnmDetailVO> getListPoVnmVoDetail(Long poVnmId, Long shopId)
			throws BusinessException {

		try {
			return poVnmDetailDAO.getListPoVnmDetailVO(poVnmId, shopId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<PoVnmDetail> getListPoVnmVoDetail(Long poVnmId, Long shopId,Long productId)
			throws BusinessException {

		try {
			return poVnmDetailDAO.getListPoVnmDetailByPoVnmId(poVnmId, shopId,productId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public PoVnmLot getPoVnmLotById(long id) throws BusinessException {
		try {
			return poVnmLotDAO.getPoVnmLotById(id);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	// @Override
	// public List<RptCompareImExStF1VO> getDataForCompareImExStF1Report(
	// Long shopId, Date fromDate, Date toDate) throws BusinessException {
	// try {
	// List<RptCompareImExStF1VO> listVO = new
	// ArrayList<RptCompareImExStF1VO>();
	// String[] listSubCat = new String[] { "A", "B", "C", "D", "E" };
	//
	// // So ngay ban hang trong thang
	// Integer workDateInMonth = saleDayDAO.getSaleDayByDate(toDate);
	// if (workDateInMonth == null) {
	// throw new IllegalArgumentException("workDateInMonth is null");
	// }
	//
	// //So ngay ban hang thuc te
	// int realWorkDateInMonth = saleDayMgr.getSaleDayInMonth(fromDate,
	// toDate);
	// for (String cat : listSubCat) {
	// RptCompareImExStF1VO vo = new RptCompareImExStF1VO();
	// vo.setSubCat(cat);
	// List<RptCompareImExStF1DataVO> listData = productDAO
	// .getDataForCompareImExStF1Report(shopId, cat,
	// workDateInMonth, realWorkDateInMonth);
	//
	// if (null != listData && listData.size() > 0) {
	// vo.setData(listData);
	// listVO.add(vo);
	// }
	// }
	//
	// return listVO;
	// } catch (Exception ex) {
	// throw new BusinessException(ex);
	// }
	// }
	
	/*@Override
	public List<RptCompareImExStF1VO> getDataForComparePOAutoReport(
			Long shopId, Long poId) throws BusinessException {
		List<RptCompareImExStF1VO> listVO = null;

		try {
			Date currentDate = commonDAO.getSysDate();
			Date firstDateOfMonth = commonDAO.getFirstDateOfMonth();

			// So ngay ban hang trong thang
			Integer workDateInMonth = saleDayDAO.getSaleDayByDate(currentDate);
			// So ngay ban hang thuc te
			int realWorkDateInMonth = saleDayMgr.getNumberWorkingDayByFromDateAndToDate(
					firstDateOfMonth, currentDate);
			if (workDateInMonth == null) {
				throw new IllegalArgumentException("workDateInMonth is null");
			}

			List<RptCompareImExStF1DataVO> listData = productDAO
					.getDataForComparePOAutoReport(shopId, poId,
							workDateInMonth, realWorkDateInMonth);

			if (null == listData || listData.size() > 0) {
				listVO = new ArrayList<RptCompareImExStF1VO>();

				listVO = RptCompareImExStF1DataVO.groupByCat(listData);
			}

			return listVO;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}*/
	
	@Override
	public List<PoAutoGroupByCatVO> getDataForComparePOAutoReport(Long shopId,
			Long poId) throws BusinessException {
		if (shopId == null) {
			throw new BusinessException("shopId is null");
		}
		if (poId == null) {
			throw new BusinessException("poId is null");
		}

		List<PoAutoGroupByCatVO> listVO = null;
		try {
			Integer currSaleDay = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(
							commonDAO.getFirstDateOfMonth(),
							commonDAO.getSysDate());
			Integer saleDay = saleDayDAO.getSaleDayByDate(commonDAO
					.getSysDate());
			if (saleDay == null) {
				throw new IllegalArgumentException("saleDay is null");
			}

			List<Object> inParams = new ArrayList<Object>();

			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);
//			inParams.add(shopId.toString());
//			inParams.add(StandardBasicTypes.STRING);

			inParams.add("poId");
			inParams.add(poId);
			inParams.add(StandardBasicTypes.LONG);
//			inParams.add(poId.toString());
//			inParams.add(StandardBasicTypes.STRING);

			inParams.add("checkAll");
			inParams.add(1);
			inParams.add(StandardBasicTypes.INTEGER);

			inParams.add("currSaleDay");
			inParams.add(currSaleDay);
			inParams.add(StandardBasicTypes.INTEGER);

			inParams.add("saleDay");
			inParams.add(saleDay);
			inParams.add(StandardBasicTypes.INTEGER);

			List<PoAutoVO> lstResult = repo.getListByNamedQuery(PoAutoVO.class, "PKG_PO_AUTO.PRO_POAUTO_COMPARE", inParams);

			if (null == lstResult || lstResult.size() > 0) {
				listVO = new ArrayList<PoAutoGroupByCatVO>();

				listVO = PoAutoVO.groupByCat(lstResult);
			}

			return listVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<PoVnmDetail> getListPoVnmDetailByPoVnmId(KPaging<PoVnmDetail> kPaging, Long poVnmId)
			throws BusinessException {
		try {
			ObjectVO<PoVnmDetail> vo = new ObjectVO<PoVnmDetail>();
			vo.setLstObject(poVnmDetailDAO.getListPoVnmDetailByPoVnmId(kPaging, poVnmId));
			vo.setkPaging(kPaging);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmId(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId)
			throws BusinessException {
		try {
			if (poVnmId == null) {
				throw new IllegalArgumentException("poVnmId is null");
			}
			ObjectVO<PoVnmDetailLotVO> vo = new ObjectVO<PoVnmDetailLotVO>();
			vo.setLstObject(poVnmDetailDAO.getListPoVnmDetailLotVOByPoVnmId(kPaging, poVnmId));
			vo.setkPaging(kPaging);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public ObjectVO<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmIdEx(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId,Long shopId)
			throws BusinessException {
		try {
			if (poVnmId == null) {
				throw new IllegalArgumentException("poVnmId is null");
			}
			Date lockDate = shopLockDAO.getNextLockedDay(shopId);
			if(lockDate == null){
				lockDate = commonDAO.getSysDate();
			}
			ObjectVO<PoVnmDetailLotVO> vo = new ObjectVO<PoVnmDetailLotVO>();
			vo.setLstObject(poVnmDetailDAO.getListPoVnmDetailLotVOByPoVnmIdEx(kPaging, poVnmId,shopId,lockDate));
			vo.setkPaging(kPaging);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException("PRODUCT_HAS_DUPLICATE_PRICE_IN_PO_VNM");
		}
	}
	@Override
	public ObjectVO<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmIdStockIn(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId)
			throws BusinessException {
		try {
			if (poVnmId == null) {
				throw new IllegalArgumentException("poVnmId is null");
			}
			ObjectVO<PoVnmDetailLotVO> vo = new ObjectVO<PoVnmDetailLotVO>();
			vo.setLstObject(poVnmDetailDAO.getListPoVnmDetailLotVOByPoVnmIdStockIn(kPaging, poVnmId));
			vo.setkPaging(kPaging);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author hunglm16
	 * @since February 7, 2014
	 * @description get list full POVNM Detail by list PoVnmId
	 * */
	@Override
	public ObjectVO<PoVnmDetailLotVO> getListFullPoVnmDetailVOByListPoVnmId(KPaging<PoVnmDetailLotVO> kPaging, List<Long> lstPoVnmId) throws BusinessException {
		try {
			if (lstPoVnmId == null || lstPoVnmId.size()==0) {
				throw new IllegalArgumentException("poVnmId is null");
			}
			//Ke thua class PoVnmDetailLotVO co san
			ObjectVO<PoVnmDetailLotVO> vo = new ObjectVO<PoVnmDetailLotVO>();
			vo.setLstObject(poVnmDetailDAO.getListFullPoVnmDetailLotVOByPoVnmId(kPaging, lstPoVnmId));
			vo.setkPaging(kPaging);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<PoVnmLot> getListPoVnmLotByPoVnmDetailId(Long poVnmDetailId)
			throws BusinessException {
		try {
			if (poVnmDetailId == null) {
				throw new IllegalArgumentException("poVnmDetailId is null");
			}
			return poVnmLotDAO.getListPoVnmLotByPoVnmDetailId(poVnmDetailId, null);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public PoVnmDetail createPoVnmDetail(PoVnmDetail poVnmDetail)
			throws BusinessException {
		try {
			return poVnmDetailDAO.createPoVnmDetail(poVnmDetail);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public PoVnmLot createPoVnmLot(PoVnmLot poVnmLot) throws BusinessException {
		try {
			return poVnmLotDAO.createPoVnmLot(poVnmLot);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updatePoVnmLot(PoVnmLot poVnmLot) throws BusinessException {
		try {
			poVnmLotDAO.updatePoVnmLot(poVnmLot);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<ProductLot> getProductLotByProductAndOwner(long productId,
			long ownerId, StockObjectType ownerType) throws BusinessException {
		try {
			return productLotDAO.getProductLotByProductAndOwner(productId,
					ownerId, ownerType);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updateListStockTotal(List<StockTotal> listStockTotal)
			throws BusinessException {
		try {
			stockTotalDAO.updateListStockTotal(listStockTotal);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void createListPoVnmDetail(List<PoVnmDetail> listPoVnmDetail)
			throws BusinessException {
		try {
			poVnmDetailDAO.createListPoVnmDetail(listPoVnmDetail);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updateListPoVnmDetail(List<PoVnmDetail> listPoVnmDetail)
			throws BusinessException {
		try {
			poVnmDetailDAO.updateListPoVnmDetail(listPoVnmDetail);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void createListProductLot(List<ProductLot> listProductLot)
			throws BusinessException {
		try {
			productLotDAO.createListProductLot(listProductLot);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updateListProductLot(List<ProductLot> listProductLot)
			throws BusinessException {
		try {
			productLotDAO.updateListProductLot(listProductLot);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void createListPoVnmLot(List<PoVnmLot> listPoVnmLot)
			throws BusinessException {
		try {
			poVnmLotDAO.createListPoVnmLot(listPoVnmLot);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updateListPoVnmLot(List<PoVnmLot> listPoVnmLot)
			throws BusinessException {
		try {
			poVnmLotDAO.updateListPoVnmLot(listPoVnmLot);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public PoVnm createReturnPoVnm(Long shopId, Long oPoVnmId,
			List<Long> oLstPoVnmDetailLotId, List<Boolean> oLstCheckLot,
			List<Integer> lstPoVnmDetailLotQty, String invoiceNumber,
			Date invoiceDate, Integer quantityCheck, BigDecimal amountCheck, BigDecimal disCount,
			String user,List<Long> lstWarehouseId,List<Long> lstProductIdKho,List<Integer> lstValue,List<Long> lstPoVNMDetailId) throws BusinessException {
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (oPoVnmId == null) {
				throw new IllegalArgumentException("oPoVnmId is null");
			}
			if (oLstPoVnmDetailLotId == null) {
				throw new IllegalArgumentException("oLstPoVnmDetailLotId is null");
			}
			if (oLstCheckLot == null) {
				throw new IllegalArgumentException("oLstCheckLot is null");
			}
			if (lstPoVnmDetailLotQty == null) {
				throw new IllegalArgumentException("lstPoVnmDetailLotQty is null");
			}
			if (oLstPoVnmDetailLotId.size() != oLstCheckLot.size()|| oLstPoVnmDetailLotId.size() != lstPoVnmDetailLotQty.size()) {
				throw new IllegalArgumentException("oLstPoVnmDetailLotId.size() != oLstCheckLot.size() || oLstPoVnmDetailLotId.size() != lstPoVnmDetailLotQty.size()");
			}
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			// /////////////////////
			PoVnm oPoVnm = poVnmDAO.getPoVnmById(oPoVnmId);
			if (oPoVnm == null) {
				throw new IllegalArgumentException("oPoVnm is null");
			}
			
			Date lockDate = shopLockDAO.getApplicationDate(shopId);
			
			ArrayList<PoVnmDetail> lstPoVnmDetail = new ArrayList<PoVnmDetail>();
			MapVO<PoVnmDetail, List<PoVnmLot>> mapLstPoVnmLot = new MapVO<PoVnmDetail, List<PoVnmLot>>();
			Integer totalPoVnmQty = 0;
			BigDecimal totalPoVnmAmount = BigDecimal.ZERO;
			Date now = commonDAO.getSysDate();
			StockTotalVOFilter filter = new StockTotalVOFilter();
			filter.setShopId(shopId);
			filter.setOwnerType(StockObjectType.SHOP);
			for (int i = 0; i < lstWarehouseId.size(); i++) { // cap nhat ton kho
				Long wareHouseId = lstWarehouseId.get(i);
				Long productId = lstProductIdKho.get(i);
				Integer value = lstPoVnmDetailLotQty.get(i);
				List<Long> lstProductId = new ArrayList<Long>();
				lstProductId.add(productId);
				filter.setLstProductId(lstProductId);
				filter.setWarehouseId(wareHouseId);
				List<StockTotal> lstStockTT = stockTotalDAO.getListStockTotalByShopAndProduct(filter);	
				if(!lstStockTT.isEmpty()){
					StockTotal stockTotal = lstStockTT.get(0);
					stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() - value);
					stockTotal.setQuantity(stockTotal.getQuantity()- value);
					stockTotalDAO.updateStockTotal(stockTotal);
				}
			}
			Price price = null;
			PoVnmDetail oPoVnmDetail = null;
			PoVnmDetail nPoVnmDetail = null;
			BigDecimal am = null;
			for (int i = 0; i < lstPoVNMDetailId.size(); i++) {
				Long pPoVnmDetailId = lstPoVNMDetailId.get(i);
				if (pPoVnmDetailId == null) {
					throw new IllegalArgumentException("pPoVnmDetailId is null");
				}
				Boolean checkLot = oLstCheckLot.get(i);
				if (checkLot == null) {
					throw new IllegalArgumentException("checkLot is null");
				}
				Integer poVnmDetailLotQty = lstValue.get(i);
				if (poVnmDetailLotQty == null) {
					throw new IllegalArgumentException("poVnmLotQty is null");
				}
				if (checkLot) {//hien tai ko xu ly cho lot
					// check lot PoVnmLot
					/*Long pPoVnmLotId = oLstPoVnmDetailLotId.get(i);
					if (pPoVnmLotId == null) {
						throw new IllegalArgumentException("pPoVnmLotId is null");
					}
					PoVnmLot oPoVnmLot = poVnmLotDAO.getPoVnmLotByIdForUpdate(pPoVnmLotId);
					if (oPoVnmLot == null) {
						throw new IllegalArgumentException("oPoVnmLot is null");
					}
					if (oPoVnmLot.getPoVnm().getId().longValue() != oPoVnmId) {
						throw new IllegalArgumentException("oPoVnmLot.getPoVnm().getId().longValue() != oPoVnmId");
					}
					if (oPoVnmLot.getPoVnmDetail().getPoVnm().getId().longValue() != oPoVnmId) {
						throw new IllegalArgumentException("oPoVnmLot.getPoVnmDetail().getPoVnm().getId().longValue() != oPoVnmId");
					}
					if (oPoVnmLot.getProduct().getCheckLot() != 1) {
						throw new IllegalArgumentException("oPoVnmLot.getProduct().getCheckLot() != 1");
					}
					if (StringUtility.isNullOrEmpty(oPoVnmLot.getLot())) {
						throw new IllegalArgumentException("StringUtility.isNullOrEmpty(oPoVnmLot.getLot())");
					}
					if (poVnmDetailLotQty > oPoVnmLot.getQuantity() - oPoVnmLot.getQuantityPay()) {
						throw new IllegalArgumentException("poVnmDetailLotQty > oPoVnmLot.getQuantity() - oPoVnmLot.getQuantityPay()");
					}
					///////////////////////cap nhat stock total
					StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(oPoVnmLot.getProduct().getId(),StockObjectType.SHOP, shopId);
					if (stockTotal == null) {
						throw new IllegalArgumentException("stockTotal is null");
					}
					if (stockTotal.getAvailableQuantity() < poVnmDetailLotQty)
						throw new BusinessException("stockTotal.getAvailableQuantity() < poVnmDetailLotQty");
					if (stockTotal.getQuantity() < poVnmDetailLotQty)
						throw new BusinessException("stockTotal.getQuantity() < poVnmDetailLotQty");
					
					stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() - poVnmDetailLotQty);
					stockTotal.setQuantity(stockTotal.getQuantity() - poVnmDetailLotQty);
					stockTotalDAO.updateStockTotal(stockTotal);
					////////cap nhat product lot
					ProductLot productLot = productLotDAO.getProductLotByCodeAndOwnerAndProductForUpdate(oPoVnmLot.getLot(), 
											shopId,StockObjectType.SHOP, oPoVnmLot.getProduct().getId());
					if (productLot == null) {
						throw new IllegalArgumentException("productLot is null");
					}
					if (productLot.getQuantity() < poVnmDetailLotQty)
						throw new BusinessException("productLot.getQuantity() < poVnmDetailLotQty");
					productLot.setAvailableQuantity(productLot.getAvailableQuantity() - poVnmDetailLotQty);
					productLot.setQuantity(productLot.getQuantity()- poVnmDetailLotQty);
					productLotDAO.updateProductLot(productLot);
					///////////////////////
					oPoVnmLot.setQuantityPay(oPoVnmLot.getQuantityPay()+ poVnmDetailLotQty);
					oPoVnmLot.setAmountPay(oPoVnmLot.getAmountPay().add(oPoVnmLot.getPriceValue().multiply(new BigDecimal(poVnmDetailLotQty))));
					poVnmLotDAO.updatePoVnmLot(oPoVnmLot);
					// /// cap nhat PoVnmDetail
					PoVnmDetail oPoVnmDetail = oPoVnmLot.getPoVnmDetail();
					oPoVnmDetail.setQuantityPay(oPoVnmDetail.getQuantityPay()+ poVnmDetailLotQty);
					oPoVnmDetail.setAmountPay(oPoVnmDetail.getAmountPay().add(oPoVnmDetail.getPriceValue().multiply(new BigDecimal(poVnmDetailLotQty))));
					poVnmDetailDAO.updatePoVnmDetail(oPoVnmDetail);
					// /////////////
					PoVnmDetail nPoVnmDetail = mapPoVnmDetail.get(oPoVnmLot.getProduct().getProductCode());
					if (nPoVnmDetail == null) {
						///////tao moi PoVnmDetail
						nPoVnmDetail = new PoVnmDetail();
						nPoVnmDetail.setPoVnmDate(now);
						nPoVnmDetail.setPrice(oPoVnmLot.getPoVnmDetail().getPrice());
						nPoVnmDetail.setPriceValue(oPoVnmLot.getPoVnmDetail().getPriceValue());
						nPoVnmDetail.setProduct(oPoVnmLot.getPoVnmDetail().getProduct());
						nPoVnmDetail.setQuantity(poVnmDetailLotQty);
						nPoVnmDetail.setAmount(nPoVnmDetail.getPriceValue().multiply(new BigDecimal(poVnmDetailLotQty)));
						nPoVnmDetail.setQuantityPay(0);
						nPoVnmDetail.setAmountPay(BigDecimal.ZERO);
						mapPoVnmDetail.put(oPoVnmLot.getProduct().getProductCode(), nPoVnmDetail);
						///////////////////
						ArrayList<PoVnmLot> lstPoVnmLot = new ArrayList<PoVnmLot>();
						mapLstPoVnmLot.put(nPoVnmDetail, lstPoVnmLot);
					} else {
						nPoVnmDetail.setQuantity(nPoVnmDetail.getQuantity()+ poVnmDetailLotQty);
						nPoVnmDetail.setAmount(nPoVnmDetail.getAmount().add(nPoVnmDetail.getPriceValue().multiply(new BigDecimal(poVnmDetailLotQty))));
					}
					List<PoVnmLot> lstPoVnmLot = mapLstPoVnmLot.get(nPoVnmDetail);
					if (lstPoVnmLot == null) {
						throw new IllegalArgumentException("lstPoVnmLot is null");
					}
					//////////////
					PoVnmLot nPoVnmLot = new PoVnmLot();
					nPoVnmLot.setPoVnmDate(now);
					nPoVnmLot.setPrice(oPoVnmLot.getPrice());
					nPoVnmLot.setPriceValue(oPoVnmLot.getPriceValue());
					nPoVnmLot.setProduct(oPoVnmLot.getProduct());
					nPoVnmLot.setQuantity(poVnmDetailLotQty);
					nPoVnmLot.setLot(oPoVnmLot.getLot());
					nPoVnmLot.setAmount(nPoVnmLot.getPriceValue().multiply(new BigDecimal(poVnmDetailLotQty)));
					nPoVnmLot.setQuantityPay(0);
					nPoVnmLot.setAmountPay(BigDecimal.ZERO);
			
					//////////
					lstPoVnmLot.add(nPoVnmLot);
					///////tinh tong cho PoVnm moi
					totalPoVnmQty += poVnmDetailLotQty;
					totalPoVnmAmount = totalPoVnmAmount.add(oPoVnmLot.getPriceValue().multiply(new BigDecimal(poVnmDetailLotQty)));*/
				} else {
					oPoVnmDetail = poVnmDetailDAO.getPoVnmDetailByIdForUpdate(pPoVnmDetailId);
					if (oPoVnmDetail == null) {
						throw new IllegalArgumentException("oPoVnmDetail is null");
					}
					if (oPoVnmDetail.getPoVnm().getId().longValue() != oPoVnmId) {
						throw new IllegalArgumentException("oPoVnmDetail.getPoVnm().getId().longValue() != oPoVnmId");
					}
					// ////cap nhat StockTotal
//					StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwnerForUpdate(oPoVnmDetail.getProduct().getId(),StockObjectType.SHOP, shopId,oPoVnmDetail.getWarehouse().getId());
//					if (stockTotal == null) {
//						throw new IllegalArgumentException("stockTotal is null");
//					}
					oPoVnmDetail.setQuantityPay(oPoVnmDetail.getQuantityPay()+ poVnmDetailLotQty);
					oPoVnmDetail.setAmountPay(oPoVnmDetail.getAmountPay().add(oPoVnmDetail.getPriceValue().multiply(new BigDecimal(poVnmDetailLotQty))));
					poVnmDetailDAO.updatePoVnmDetail(oPoVnmDetail);
					// //////tao moi PoVnmDetail
					if(lstPoVnmDetail.isEmpty() || (!lstPoVnmDetail.get(lstPoVnmDetail.size()-1).getProduct().getId().equals(oPoVnmDetail.getProduct().getId()))){
						for (int j = 0; j < lstWarehouseId.size(); j++) { // tao poVNMDetail moi
							if(oPoVnmDetail.getProduct().getId().equals(lstProductIdKho.get(j))){
								Integer quantity = lstPoVnmDetailLotQty.get(j); //quantity
								if(quantity<=0){
									continue;
								}
								nPoVnmDetail = new PoVnmDetail();
								//nPoVnmDetail.setPoVnmDate(now);
								nPoVnmDetail.setPoVnmDate(lockDate);
								price = null;
								if (oPoVnmDetail.getProduct() != null) {
									price = priceDAO.getPriceByProductAndShopId(oPoVnmDetail.getProduct().getId(), shopId);
								}
								am = null;
								if (oPoVnmDetail.getPrice() != null || oPoVnmDetail.getPriceValue() != null) {
									nPoVnmDetail.setPrice(oPoVnmDetail.getPrice());
									nPoVnmDetail.setPriceValue(oPoVnmDetail.getPriceValue());
									if (oPoVnmDetail.getPriceValue() != null
											&& !BigDecimal.ZERO.equals(oPoVnmDetail.getPriceValue())) { // gia tu vnm dua sang khong thue -> + 0.1
										am = oPoVnmDetail.getPriceValue().multiply(new BigDecimal(quantity));
										am = am.multiply(BigDecimal.valueOf(1.1));
									} else if (oPoVnmDetail.getPrice() != null && oPoVnmDetail.getPrice().getPrice() != null){
										am = oPoVnmDetail.getPrice().getPrice().multiply(new BigDecimal(quantity));
									}
								} else if (price != null) {
									nPoVnmDetail.setPrice(price);
									nPoVnmDetail.setPriceValue(price.getPrice());
									am = nPoVnmDetail.getPriceValue().multiply(new BigDecimal(quantity));
								}
								nPoVnmDetail.setProduct(oPoVnmDetail.getProduct());
								nPoVnmDetail.setQuantity(quantity);
								if (am != null) {
									nPoVnmDetail.setAmount(am);
								} else {
									nPoVnmDetail.setAmount(BigDecimal.ZERO);
								}
								nPoVnmDetail.setQuantityPay(0);
								nPoVnmDetail.setAmountPay(BigDecimal.ZERO);
								Long wareHouseId = lstWarehouseId.get(j);
								Warehouse wh = wareHouseDAO.getWarehouseById(wareHouseId);
								if(wh!=null){
									nPoVnmDetail.setWarehouse(wh);
								}
								lstPoVnmDetail.add(nPoVnmDetail);
								// ///tinh tong PoVnm
								totalPoVnmQty += quantity;
								totalPoVnmAmount = totalPoVnmAmount.add(nPoVnmDetail.getAmount());
							}
						}
					}
				}
			}
			//BigDecimal totalPoVnmAmountVAT = totalPoVnmAmount.multiply(BigDecimal.valueOf(0.1));
			//totalPoVnmAmount = totalPoVnmAmount.add(totalPoVnmAmountVAT);
			
			BigDecimal totalPoVnmAmountDis = totalPoVnmAmount;
			// tao PoVnm moi
			PoVnm nPoVnm = new PoVnm();
			nPoVnm.setShop(shop);
			//nPoVnm.setPoVnmDate(now);
			nPoVnm.setPoVnmDate(lockDate);
			nPoVnm.setCreateDate(now);
			nPoVnm.setType(PoType.RETURNED_SALES_ORDER);
			nPoVnm.setStatus(PoVNMStatus.IMPORTED);
			nPoVnm.setTotal(totalPoVnmAmountDis);
			nPoVnm.setAmount(totalPoVnmAmount);
			nPoVnm.setQuantity(totalPoVnmQty);
			nPoVnm.setFromPoVnmId(oPoVnm);
			nPoVnm.setPoAutoNumber(oPoVnm.getPoAutoNumber());
			nPoVnm.setSaleOrderNumber(oPoVnm.getSaleOrderNumber());
			nPoVnm.setInvoiceNumber(invoiceNumber);
			nPoVnm.setInvoiceDate(invoiceDate);
			nPoVnm.setQuantityCheck(quantityCheck);
			nPoVnm.setAmountCheck(amountCheck);
			nPoVnm.setImportUser(user);
			nPoVnm.setDiscount(disCount);
			nPoVnm.setObjectType(PoObjectType.NPP);
			nPoVnm = poVnmDAO.createPoVnm(nPoVnm);
			nPoVnm.setPoCoNumber(getPoCoNumber(nPoVnm.getId()));
			poVnmDAO.updatePoVnm(nPoVnm);
			// ////
			for (PoVnmDetail poVnmDetail1 : lstPoVnmDetail) {
				poVnmDetail1.setPoVnm(nPoVnm);
			}
			poVnmDetailDAO.createListPoVnmDetail(lstPoVnmDetail);
			for (PoVnmDetail nPoVnmDetail1 : mapLstPoVnmLot.keyList()) {
				// /tao nPoVnmDetail
				nPoVnmDetail1.setPoVnm(nPoVnm);
				nPoVnmDetail1 = poVnmDetailDAO.createPoVnmDetail(nPoVnmDetail1);
				List<PoVnmLot> lstPoVnmLot = mapLstPoVnmLot.get(nPoVnmDetail1);
				if (lstPoVnmLot != null) {
					for (PoVnmLot poVnmLot : lstPoVnmLot) {
						poVnmLot.setPoVnm(nPoVnm);
						poVnmLot.setPoVnmDetail(nPoVnmDetail1);
					}
					poVnmLotDAO.createListPoVnmLot(lstPoVnmLot);
				}
			}
			// cap nhat shop debit
			Debit debit = debitDAO.getDebitForUpdate(shopId, DebitOwnerType.SHOP);
			if (debit == null) {
				throw new IllegalArgumentException("debit is null");
			}
			debit.setTotalAmount(debit.getTotalAmount().subtract(totalPoVnmAmountDis));
			debit.setTotalDebit(debit.getTotalDebit().subtract(totalPoVnmAmountDis));
			debitDAO.updateDebit(debit);
			// tao debit
			DebitDetail debitDetail = new DebitDetail();
			debitDetail.setFromObjectId(nPoVnm.getId());
			debitDetail.setFromObjectNumber(nPoVnm.getPoCoNumber());
			debitDetail.setDebit(debit);
			debitDetail.setAmount(totalPoVnmAmount.multiply(new BigDecimal(-1)));
			debitDetail.setTotal(totalPoVnmAmountDis.multiply(new BigDecimal(-1)));
			debitDetail.setDiscount(disCount);
			debitDetail.setRemain(debitDetail.getTotal());
			debitDetail.setCreateUser(user);
			debitDetail.setCreateDate(now);
			debitDetail.setType(DebitDetailType.TRA_VNM.getValue());  // lacnv1 - them type cua debit_detail
			debitDetail.setShop(shop);
			//debitDetail.setDebitDate(now); //
			//TUNGTT
			debitDetail.setDebitDate(lockDate);
			debitDetailDAO.createDebitDetail(debitDetail);
			return nPoVnm;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<PoCSVO> getListPoCSVO(Long shopId, String orderNumber, String poCoNumber,
			Date fromDate, Date toDate, PoVNMStatus poStatus, boolean hasFindChildShop)
			throws BusinessException {
		try {
			return poVnmDAO.getListPoCSVO(shopId, orderNumber, poCoNumber, fromDate,
					toDate, poStatus, hasFindChildShop);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptPoAutoLV01VO> getDataForReportPoAutoShop(
			List<Long> listShopId, Date fromDate, Date toDate)
			throws BusinessException {
		try {
			List<RptPoAutoDataVO> listData = poVnmDAO
					.getListPoAutoForReport(listShopId, fromDate, toDate);

			if (null == listData || listData.size() <= 0) {
				return null;
			} else {
				List<RptPoAutoLV03VO> listLV03 = RptPoAutoLV03VO.groupBy(
						listData, "poAutoNumber");
				List<RptPoAutoLV02VO> listLV02 = RptPoAutoLV02VO.groupBy(
						listLV03, "strPoAutoDate");
				return RptPoAutoLV01VO.groupBy(listLV02, "shopCode");
			}
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptF1Lv01VO> getDataForF1Report(Long shopId, Date fromDate,
			Date toDate) throws BusinessException {
		try {
			// So ngay ban hang trong thang
			Integer workDateInMonth = saleDayDAO.getSaleDayByDate(toDate);
			// So ngay ban hang thuc te
			int realWorkDateInMonth = saleDayMgr.getNumberWorkingDayByFromDateAndToDate(fromDate,
					toDate);
			if (workDateInMonth == null) {
				throw new IllegalArgumentException("workDateInMonth is null");
			}

			List<RptF1DataVO> listData = productDAO.getDataForF1Report(shopId,
					fromDate, toDate, workDateInMonth, realWorkDateInMonth);

			if (null == listData || listData.size() <= 0) {
				return null;
			} else {
				return RptF1DataVO.groupByCat(listData);
			}
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}


	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoMgr#updateConfirmPoAuto(ths.dms.core.entities.PoAuto, java.util.List, java.util.List)
	 */
	@Override
	@Transactional (rollbackFor = Exception.class)
	public void updateConfirmPoAuto(PoAuto poAuto, List<Long> listProductId,
			List<Integer> listQuantity) throws BusinessException {
		try {
			//update poAuto
			poAutoDAO.updatePoAuto(poAuto);
			Date date = commonDAO.getSysDate();
			//tao poVnm
			PoVnm poVnm = new PoVnm();
			poVnm.setShop(poAuto.getShop());
			poVnm.setPoAutoNumber(poAuto.getPoAutoNumber());
			poVnm.setType(PoType.PO_CUSTOMER_SERVICE);
			poVnm.setOrderdate(poAuto.getPoAutoDate());
			poVnm.setObjectType(PoObjectType.NCC);
			poVnm.setDisStatus(PoDisStatus.CHUA_CHUYEN);
			poVnm.setPoDvStatus(PoDvStatus.CHUA_CO_PO_CONFIRM);
			poVnm.setPoVnmDate(date);
			poVnm.setCreateDate(date);
			PoVnm iPoVnm = poVnmDAO.createPoVnm(poVnm);
			BigDecimal totalAmount = BigDecimal.ZERO;
			Integer totalQuantity = 0;
			//tao list poVnmDetail
			List<PoVnmDetail> listPoVnmDetail = new ArrayList<PoVnmDetail>();
			for (int i = 0, size = listProductId.size(); i < size; i++) {
				Long productId = listProductId.get(i);
				Integer quantity = listQuantity.get(i);
				Price price = priceDAO.getPriceByProductId(productId);
				PoVnmDetail poVnmDetail = new PoVnmDetail();
				poVnmDetail.setPoVnm(iPoVnm);
				if (price != null) {
					poVnmDetail.setPrice(price);
					poVnmDetail.setPriceValue(price.getPrice());
					BigDecimal amount = price.getPrice().multiply(new BigDecimal(quantity));
					poVnmDetail.setAmount(amount);
					totalAmount = totalAmount.add(amount);
				}
				poVnmDetail.setQuantity(quantity);
				totalQuantity = totalQuantity + quantity;
				poVnmDetail.setPoVnmDate(date);
				poVnmDetail.setProduct(productDAO.getProductById(productId));
				listPoVnmDetail.add(poVnmDetail);
			}
			if (listPoVnmDetail.size() > 0) {
				poVnmDetailDAO.createListPoVnmDetail(listPoVnmDetail);
			}
			
			//update lai poVnm
			iPoVnm.setAmount(totalAmount);
			iPoVnm.setQuantity(totalQuantity);
			String saleOrderNumber = "PODVKH_" + iPoVnm.getId();
			iPoVnm.setSaleOrderNumber(saleOrderNumber);
			poVnmDAO.updatePoVnm(iPoVnm);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
		
	}


	@Override
	public ObjectVO<PoVnm> getListObjectVOPoVnm(PoVnmFilter filter,
			KPaging<PoVnm> kPaging) throws BusinessException {
		try {
			ObjectVO<PoVnm> objectVO = new ObjectVO<PoVnm>();
			List<PoVnm> list = poVnmDAO.getListPoVnm(filter,kPaging);
			objectVO.setLstObject(list);
			objectVO.setkPaging(kPaging);
			return objectVO;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}


	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoMgr#rejectPoAuto(java.util.List)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void rejectPoAuto(List<PoAuto> listPoAuto) throws BusinessException {
		try {
			if (listPoAuto == null || listPoAuto.size() == 0)
				return;
			List<PoAuto> listPoAutoTmp = new ArrayList<PoAuto>();
            for (PoAuto poAutoTmp : listPoAuto) {
            	PoAuto poAuto = poAutoDAO.getPoAutoByIdForUpdate(poAutoTmp.getId());
                if(!poAuto.getStatus().equals(ApprovalStatus.NOT_YET)) {
                	throw new BusinessException(ExceptionCode.PO_AUTO_STATUS_NOT_YET);
                } else{
                	poAuto.setDescription(poAutoTmp.getDescription());
                	poAuto.setStatus(ApprovalStatus.REJECT);
    				poAuto.setModifyDate(commonDAO.getSysDate());
    				listPoAutoTmp.add(poAuto);
                }
            }
			poAutoDAO.updatePoAuto(listPoAutoTmp);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public PoVnm transferPoDVKH(PoVnm poDVKH)
			throws BusinessException {
		try {
			Date now = commonDAO.getSysDate();
			poDVKH.setDisStatus(PoDisStatus.DA_CHUYEN);
			poVnmDAO.updatePoVnm(poDVKH);
			List<PoVnmDetail> lstDetail = poVnmDetailDAO.getListPoVnmDetailByPoVnmId(poDVKH.getId());
			PoVnm poVnm = new PoVnm();
			poVnm.setShop(poDVKH.getShop());
			poVnm.setPoAutoNumber(poDVKH.getPoAutoNumber());
			poVnm.setStatus(PoVNMStatus.NOT_IMPORT);
			poVnm.setPoVnmDate(now);
			poVnm.setCreateDate(now);
			//amount, quantity
			
			poVnm.setType(PoType.PO_CUSTOMER_SERVICE);
			poVnm.setOrderdate(poDVKH.getOrderdate());
			poVnm.setObjectType(PoObjectType.NPP);
			poVnm = poVnmDAO.createPoVnm(poVnm);
			poVnm.setSaleOrderNumber(poDVKH.getSaleOrderNumber());
			poVnmDAO.updatePoVnm(poVnm);
			BigDecimal amount  = BigDecimal.ZERO;
			Integer quantity = 0;
			if(lstDetail != null && lstDetail.size() >0){
				List<PoVnmDetail> lstTmp = new ArrayList<PoVnmDetail>();
				for (PoVnmDetail child : lstDetail) {
					PoVnmDetail tmp = new PoVnmDetail();
					tmp.setPoVnm(poVnm);
					tmp.setProduct(child.getProduct());
					tmp.setPrice(child.getPrice());
					tmp.setPriceValue(child.getPriceValue());
					tmp.setQuantity(child.getQuantity());
					tmp.setAmount(child.getAmount());
					tmp.setPoVnmDate(now);
					amount = amount.add(child.getAmount());
					quantity = quantity + child.getQuantity();
					lstTmp.add(tmp);
				}
				poVnmDetailDAO.createListPoVnmDetail(lstTmp);
			}
			poVnm.setAmount(amount);
			poVnm.setQuantity(quantity);
			poVnmDAO.updatePoVnm(poVnm);
			return poVnm;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public PoVnm createPoConfirm(PoVnm poConfirm,
			List<CreatePoVnmDetailVO> lstPoConfirmDetail)
			throws BusinessException {
		try {
			if(poConfirm.getId() != null){
				//delete
				poVnmDAO.deletePoVnmConfirmDetail(poConfirm);
				poVnmDAO.updatePoVnm(poConfirm);
			}else{
//				Long poVnmId = poVnmDAO.getPoVnmSeq();
				poConfirm = poVnmDAO.createPoVnm(poConfirm);
				if(poConfirm != null){
					poConfirm.setPoCoNumber("POCO_" + poConfirm.getId());
					poVnmDAO.updatePoVnm(poConfirm);
				}
			}
			if(lstPoConfirmDetail != null && lstPoConfirmDetail.size() >0){
				for (CreatePoVnmDetailVO child : lstPoConfirmDetail) {
					PoVnmDetail detail = child.getPoVnmDetail();
					detail.setPoVnm(poConfirm);
					detail.setId(null);
					detail = poVnmDetailDAO.createPoVnmDetail(detail);
					List<PoVnmLot> listLot = child.getListPoVnmLot();
					if(listLot != null && listLot.size() > 0){
						for (PoVnmLot poVnmLot : listLot) {
							poVnmLot.setPoVnm(poConfirm);
							poVnmLot.setPoVnmDetail(detail);
							poVnmLot.setId(null);
						}
						poVnmLotDAO.createListPoVnmLot(listLot);
					}
				}
			}
			return poConfirm;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Integer checkProductInPoDVKH(Long poDvkhId) throws BusinessException {
		try {
			return poVnmDAO.checkProductInPoDVKH(poDvkhId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public PoVnm transferPoConfirm(PoVnm poConfirm)
			throws BusinessException {
		try {
			Date now = commonDAO.getSysDate();
			poConfirm.setDisStatus(PoDisStatus.DA_CHUYEN);
			poVnmDAO.updatePoVnm(poConfirm);
			List<PoVnmDetail> lstDetail = poVnmDetailDAO.getListPoVnmDetailByPoVnmId(poConfirm.getId());
			
			PoVnm poVnm = new PoVnm();
			poVnm.setShop(poConfirm.getShop());
			poVnm.setPoAutoNumber(poConfirm.getPoAutoNumber());
			poVnm.setPoCoNumber(poConfirm.getPoCoNumber());
			poVnm.setStatus(PoVNMStatus.NOT_IMPORT);
			poVnm.setPoVnmDate(now);
			poVnm.setCreateDate(now);
			//amount, quantity
			poVnm.setSaleOrderNumber(poConfirm.getSaleOrderNumber());
			//poVnm.setBilltolocation(poConfirm.getBilltolocation());
			//poVnm.setShiptolocation(poConfirm.getShiptolocation());
			poVnm.setType(PoType.PO_CONFIRM);
			poVnm.setOrderdate(poConfirm.getOrderdate());
			poVnm.setObjectType(PoObjectType.NPP);
			poVnm = poVnmDAO.createPoVnm(poVnm);
			BigDecimal amount  = BigDecimal.ZERO;
			Integer quantity = 0;
			if(lstDetail != null && lstDetail.size() >0){
				for (PoVnmDetail child : lstDetail) {
					PoVnmDetail tmp = new PoVnmDetail();
					tmp.setPoVnm(poVnm);
					tmp.setProduct(child.getProduct());
					tmp.setPrice(child.getPrice());
					tmp.setPriceValue(child.getPriceValue());
					tmp.setQuantity(child.getQuantity());
					tmp.setAmount(child.getAmount());
					tmp.setPoVnmDate(now);
					amount = amount.add(child.getAmount());
					quantity = quantity + child.getQuantity();
					tmp = poVnmDetailDAO.createPoVnmDetail(tmp);
					List<PoVnmLot> lstLot = poVnmLotDAO.getListPoVnmLotByPoVnmDetailId(child.getId(), null);
					if(lstLot != null && lstLot.size() > 0){
						for (PoVnmLot poVnmLot : lstLot) {
							PoVnmLot lot = new PoVnmLot();
							lot.setPoVnm(poVnm);
							lot.setPoVnmDetail(tmp);
							lot.setProduct(poVnmLot.getProduct());
							lot.setPrice(poVnmLot.getPrice());
							lot.setPriceValue(poVnmLot.getPriceValue());
							lot.setLot(poVnmLot.getLot());
							lot.setQuantity(poVnmLot.getQuantity());
							lot.setAmount(poVnmLot.getAmount());
							lot.setPoVnmDate(now);
							poVnmLotDAO.createPoVnmLot(lot);
						}
					}
				}
			}
			poVnm.setAmount(amount);
			poVnm.setQuantity(quantity);
			poVnmDAO.updatePoVnm(poVnm);
			return poVnm;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<PoVnmDetailLotVO> getPoConfirmDetail(
			KPaging<PoVnmDetailLotVO> kPaging, Long poDVKHId, Long poConfirmId)
			throws BusinessException {
		try {
			List<PoVnmDetailLotVO> listPo = poVnmDetailDAO.getPoConfirmDetail(kPaging, poDVKHId, poConfirmId);
			
			ObjectVO<PoVnmDetailLotVO> vo = new ObjectVO<PoVnmDetailLotVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(listPo);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateListPoVnm(List<PoVnm> lstPoVnm) throws BusinessException {
		try {
			if (lstPoVnm != null && lstPoVnm.size() > 0) {
				DebitDetail dd = null;
				for (PoVnm child : lstPoVnm) {
					poVnmDAO.updatePoVnm(child);
					dd = debitDetailDAO.getDebitDetailByFromObjectId(child.getId());
					if(dd != null) {
						dd.setInvoiceNumber(child.getInvoiceNumber());
						debitDetailDAO.updateDebitDetail(dd);
					}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public PoAutoGroup createPoAutoGroup(PoAutoGroup poAutoGroup) throws BusinessException{
		try {
			return poAutoGroupDAO.createPoAutoGroup(poAutoGroup);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void deletePoAutoGroup(PoAutoGroup poAutoGroup) throws BusinessException{
		try {
			poAutoGroupDAO.deletePoAutoGroup(poAutoGroup);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updatePoAutoGroup(PoAutoGroup poAutoGroup) throws BusinessException{
		try {
			poAutoGroupDAO.updatePoAutoGroup(poAutoGroup);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public PoAutoGroup getPoAutoGroupById(Long id) throws BusinessException{
		try {
			return poAutoGroupDAO.getPoAutoGroupById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<PoAutoGroupVO> getPoAutoGroupVO(
			PoAutoGroupFilter filter, KPaging<PoAutoGroupVO> kPaging)
			throws BusinessException{
		try {
			
			List<PoAutoGroupVO> lst = poAutoGroupDAO.getPoAutoGroupVO(filter, kPaging);
			ObjectVO<PoAutoGroupVO> vo = new ObjectVO<PoAutoGroupVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<PoAutoGroupVO> getPoAutoGroupVOEx(
			PoAutoGroupFilter filter, KPaging<PoAutoGroupVO> kPaging)
			throws BusinessException{
		try {
			
			List<PoAutoGroupVO> lst = poAutoGroupDAO.getPoAutoGroupVOEx(filter, kPaging);
			ObjectVO<PoAutoGroupVO> vo = new ObjectVO<PoAutoGroupVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public PoAutoGroupDetail createPoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail) throws BusinessException{
		try {
			return poAutoGroupDetailDAO.createPoAutoGroupDetail(poAutoGroupDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void deletePoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail) throws BusinessException{
		try {
			poAutoGroupDetailDAO.deletePoAutoGroupDetail(poAutoGroupDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updatePoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail) throws BusinessException{
		try {
			poAutoGroupDetailDAO.updatePoAutoGroupDetail(poAutoGroupDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public PoAutoGroupDetail getPoAutoGroupDetailById(Long id) throws BusinessException{
		try {
			return poAutoGroupDetailDAO.getPoAutoGroupDetailById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<PoAutoGroupDetail> getListPoAutoGroupDetail(KPaging<PoAutoGroupDetail> kPaging,
			Long poAutoGroupId) throws BusinessException{
		try {
			List<PoAutoGroupDetail> lst = poAutoGroupDetailDAO.getListPoAutoGroupDetail(kPaging, poAutoGroupId);
			ObjectVO<PoAutoGroupDetail> vo = new ObjectVO<PoAutoGroupDetail>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<PoAutoGroupDetailVO> getListPoAutoGroupDetailVO(PoAutoGroupDetailFilter filter, KPaging<PoAutoGroupDetailVO> kPaging) throws BusinessException{
		try {
			
			List<PoAutoGroupDetailVO> lst = poAutoGroupDetailDAO.getListPoAutoGroupDetailVO(filter, kPaging);
			ObjectVO<PoAutoGroupDetailVO> vo = new ObjectVO<PoAutoGroupDetailVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createShopPoAuto(Long shopId, Long staffId, Boolean checkAllProductType) 
			throws BusinessException {
		final String GROUP_NON_POAUTO_GROUP = "GROUP_NON_POAUTO_GROUP";
		
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (staffId == null) {
			throw new IllegalArgumentException("staffId is null");
		}
		if (checkAllProductType == null) {
			throw new IllegalArgumentException("checkAllProductType is null");
		}

		try {
			Date now = commonDAO.getSysDate();
			Shop shop = this.shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			Staff staff = staffDAO.getStaffById(staffId);
			if (staff == null) {
				throw new IllegalArgumentException("staff is null");
			}

			List<PoAutoVO> listPoAutoVo = getListPoAuto(shopId, checkAllProductType);
			
			if(null == listPoAutoVo || listPoAutoVo.size() <= 0){
				throw new IllegalArgumentException("list product is null");
			}
			
			boolean isSeparatePO = false;
			Map<Integer, List<PoAutoGroupDetailVO>> groupMap = null;
			if(Boolean.TRUE.equals(this.poAutoGroupShopMapDAO.checkExistsShopInGroup(shop.getId()))){
				groupMap = this.getListPoAutoGroupVO(shop.getId());
				if(null != groupMap && !groupMap.isEmpty()){
					isSeparatePO = true;
				}
			} else{
				isSeparatePO = true;
				groupMap = this.getListPoAutoGroupVO(null);
			}
			
			Map<String, List<PoAutoDetail>> poAutoMap = new HashMap<String, List<PoAutoDetail>>();
			
			if (!isSeparatePO) {
				List<PoAutoDetail> listData = new ArrayList<PoAutoDetail>();
				
				for (PoAutoVO item : listPoAutoVo) {
					Product product = productDAO.getProductById(item.getProductId());
					if (product == null) {
						throw new IllegalArgumentException("product is null");
					}
					
					Price price = priceDAO.getPriceById(item.getPriceId());
					if (price == null) {
						throw new IllegalArgumentException("price is null");
					}
					
					PoAutoDetail poAutoDetail = new PoAutoDetail();
					
					poAutoDetail.setAmount(item.getAmount());
					poAutoDetail.setConvfact(item.getConvfact());
					poAutoDetail.setCreateDate(now);
					poAutoDetail.setDayPlan(item.getDayPlan());
					poAutoDetail.setDayReservePlan(item.getDayReservePlan());
					poAutoDetail.setDayReserveReal(new BigDecimal(item.getDayReserveReal()));
					poAutoDetail.setExport(item.getExpQty());
					poAutoDetail.setImportQuantity(item.getImpQty());
					poAutoDetail.setLead(item.getLead());
					poAutoDetail.setMonthCumulate(item.getMonthCumulate());
					poAutoDetail.setMonthPlan(item.getMonthPlan());
					poAutoDetail.setNext(item.getNext());
					poAutoDetail.setOpenStock(item.getOpenStockTotal());
					poAutoDetail.setPoAutoDate(now);
					poAutoDetail.setPriceValue(item.getPriceValue());
					poAutoDetail.setPrice(price);
					poAutoDetail.setProduct(product);
					poAutoDetail.setQuantity(item.getQuantity() * item.getConvfact());
					poAutoDetail.setRequimentStock(item.getRequimentStock());
					poAutoDetail.setSafetyStockMin(item.getSafetyStockMin());
					poAutoDetail.setStock(item.getStockQty());
					poAutoDetail.setStockPoConfirm(item.getGoPoConfirmQty());
					poAutoDetail.setStockPoDvkh(item.getStockPoDvkh());
					poAutoDetail.setWarning(item.getWarning());
					
					listData.add(poAutoDetail);
				}
				
			    PoAuto poAuto = new PoAuto();
			    
				poAuto.setStatus(ApprovalStatus.NOT_YET);
				poAuto.setDiscount(BigDecimal.ZERO);
				poAuto.setPoAutoDate(now);
				poAuto.setShop(shop);
				poAuto.setStaff(staff);
				poAuto.setPoAutoDate(now);
				poAuto.setModifyDate(null);
				poAuto = poAutoDAO.createPoAuto(poAuto);
				
				BigDecimal totalPdInfoCEAmt = BigDecimal.ZERO;
			
				for (PoAutoDetail poAutoDetail : listData) {
					poAutoDetail.setPoAuto(poAuto);
					poAutoDetailDAO.createPoAutoDetail(poAutoDetail);
					
					if (poAutoDetail.getAmount() != null) {
						totalPdInfoCEAmt = totalPdInfoCEAmt.add(poAutoDetail.getAmount());
					}
				}
				
				poAuto.setPoAutoNumber(getPoAutoNumber(poAuto.getId()));
				poAuto.setAmount(totalPdInfoCEAmt);
				poAuto.setTotal(totalPdInfoCEAmt);
				
				poAutoDAO.updatePoAuto(poAuto);
			} else {
				List<PoAutoGroupDetailVO> listGroupProduct = groupMap.get(PoAutoGroupDetailType.PRODUCT.getValue());
				List<PoAutoGroupDetailVO> listGroupSubcat = groupMap.get(PoAutoGroupDetailType.SUB_CAT.getValue());
				List<PoAutoGroupDetailVO> listGroupCat = groupMap.get(PoAutoGroupDetailType.CAT.getValue());

				for (PoAutoVO item : listPoAutoVo) {
					Product product = productDAO.getProductById(item.getProductId());					
					if (product == null) {
						throw new IllegalArgumentException("product is null");
					}
					
					Price price = priceDAO.getPriceById(item.getPriceId());
					/*if (price == null) {
						throw new IllegalArgumentException("price is null");
					}*/
					
					PoAutoDetail poAutoDetail = new PoAutoDetail();
					
					poAutoDetail.setAmount(item.getAmount());
					poAutoDetail.setConvfact(item.getConvfact());
					poAutoDetail.setCreateDate(now);
					poAutoDetail.setDayPlan(item.getDayPlan());
					poAutoDetail.setDayReservePlan(item.getDayReservePlan());
					poAutoDetail.setDayReserveReal(new BigDecimal(item.getDayReserveReal()));
					poAutoDetail.setExport(item.getExpQty());
					poAutoDetail.setImportQuantity(item.getImpQty());
					poAutoDetail.setLead(item.getLead());
					poAutoDetail.setMonthCumulate(item.getMonthCumulate());
					poAutoDetail.setMonthPlan(item.getMonthPlan());
					poAutoDetail.setNext(item.getNext());
					poAutoDetail.setOpenStock(item.getOpenStockTotal());
					poAutoDetail.setPoAutoDate(now);
					poAutoDetail.setPriceValue(item.getPriceValue());
					poAutoDetail.setPrice(price);
					poAutoDetail.setProduct(product);
					poAutoDetail.setQuantity(item.getQuantity()* item.getConvfact());
					poAutoDetail.setRequimentStock(item.getRequimentStock());
					poAutoDetail.setSafetyStockMin(item.getSafetyStockMin());
					poAutoDetail.setStock(item.getStockQty());
					poAutoDetail.setStockPoConfirm(item.getGoPoConfirmQty());
					poAutoDetail.setStockPoDvkh(item.getStockPoDvkh());
					poAutoDetail.setWarning(item.getWarning());
					
					/*if(poAutoDetail.getProduct().getSubCat().getId().compareTo(145l) == 0){
						System.out.println("ABC");
					}*/
					
					boolean isExists = false;
					if (listGroupProduct != null && listGroupProduct.size() > 0) {
						for(PoAutoGroupDetailVO groupDetail : listGroupProduct){
							if(groupDetail.getObjectId().compareTo(poAutoDetail.getProduct().getId()) == 0){
								List<PoAutoDetail> listPoAutoDetail = poAutoMap.get(groupDetail.getGroupCode());
								
								if(listPoAutoDetail == null){
									listPoAutoDetail = new ArrayList<PoAutoDetail>();
									
									poAutoMap.put(groupDetail.getGroupCode(), listPoAutoDetail);
								}
								
								listPoAutoDetail.add(poAutoDetail);
								isExists = true;
								break;
							}
						}
						
						if(isExists){
							continue;
						}
					}

					if (listGroupSubcat != null && listGroupSubcat.size() > 0) {
						for(PoAutoGroupDetailVO groupDetail : listGroupSubcat){
							if(groupDetail.getObjectId().compareTo(poAutoDetail.getProduct().getSubCat().getId()) == 0){
								List<PoAutoDetail> listPoAutoDetail = poAutoMap.get(groupDetail.getGroupCode());
								
								if(listPoAutoDetail == null){
									listPoAutoDetail = new ArrayList<PoAutoDetail>();
									
									poAutoMap.put(groupDetail.getGroupCode(), listPoAutoDetail);
								}
								
								listPoAutoDetail.add(poAutoDetail);
								isExists = true;
								break;
							}
						}
						
						if(isExists){
							continue;
						}
					}

					if (listGroupCat != null && listGroupCat.size() > 0) {
						for(PoAutoGroupDetailVO groupDetail : listGroupCat){
							if(groupDetail.getObjectId().compareTo(poAutoDetail.getProduct().getCat().getId()) == 0){
								List<PoAutoDetail> listPoAutoDetail = poAutoMap.get(groupDetail.getGroupCode());
								
								if(listPoAutoDetail == null){
									listPoAutoDetail = new ArrayList<PoAutoDetail>();
									
									poAutoMap.put(groupDetail.getGroupCode(), listPoAutoDetail);
								}
								
								listPoAutoDetail.add(poAutoDetail);
								isExists = true;
								break;
							}
						}
						
						if(isExists){
							continue;
						}
					}
					
					// Neu khong thuoc nhom nao thi tao ra 1 nhom moi.
					List<PoAutoDetail> listPoAutoDetail = poAutoMap.get(GROUP_NON_POAUTO_GROUP);
					
					if(listPoAutoDetail == null){
						listPoAutoDetail = new ArrayList<PoAutoDetail>();
						
						poAutoMap.put(GROUP_NON_POAUTO_GROUP, listPoAutoDetail);
					}
					
					listPoAutoDetail.add(poAutoDetail);
				}
				
				// them vao DB
				for (Map.Entry<String, List<PoAutoDetail>> entry : poAutoMap.entrySet()) {
					//String key = entry.getKey();
				    List<PoAutoDetail> value = entry.getValue();
				    
				    PoAuto poAuto = new PoAuto();
				    
					poAuto.setStatus(ApprovalStatus.NOT_YET);
					poAuto.setDiscount(BigDecimal.ZERO);
					poAuto.setPoAutoDate(now);
					poAuto.setShop(shop);
					poAuto.setStaff(staff);
					poAuto.setPoAutoDate(now);
					poAuto.setModifyDate(null);
					poAuto = poAutoDAO.createPoAuto(poAuto);
					
					BigDecimal totalPdInfoCEAmt = BigDecimal.ZERO;
				
					for (PoAutoDetail poAutoDetail : value) {
						poAutoDetail.setPoAuto(poAuto);
						poAutoDetailDAO.createPoAutoDetail(poAutoDetail);
						
						totalPdInfoCEAmt = totalPdInfoCEAmt.add(poAutoDetail.getAmount());
					}
					
					poAuto.setPoAutoNumber(getPoAutoNumber(poAuto.getId()));
					poAuto.setAmount(totalPdInfoCEAmt);
					poAuto.setTotal(totalPdInfoCEAmt);
					
					poAutoDAO.updatePoAuto(poAuto);
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	private Map<Integer, List<PoAutoGroupDetailVO>> getListPoAutoGroupVO(Long shopId)
			throws BusinessException {
		Map<Integer, List<PoAutoGroupDetailVO>> map = new HashMap<Integer, List<PoAutoGroupDetailVO>>();
		
		try {
			List<PoAutoGroupDetailVO> listData = poAutoGroupDetailDAO.getPoAutoGroupDetailVOByShop(shopId);
			if(null == listData || listData.size() <= 0){
				return map;
			} else{
				for(PoAutoGroupDetailVO item : listData){
					if(item.getObjectType() == null){
						continue;
					}
					
					List<PoAutoGroupDetailVO> mapItem = map.get(item.getObjectType()); 
					if(mapItem == null){
						mapItem = new ArrayList<PoAutoGroupDetailVO>();
						
						map.put(item.getObjectType(), mapItem);
					}
					
					mapItem.add(item);
				}
			}
			
			return map;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<ProductInfoVOEx> getListProductInfoCanBeAdded(
			int poAutoGroupId, String name, String code, boolean isSubCat, KPaging<ProductInfoVOEx> kPaging)
			throws BusinessException{
		try {
			List<ProductInfoVOEx> lst = poAutoGroupDAO.getListProductInfoCanBeAdded(poAutoGroupId, name, code, isSubCat, kPaging);
			ObjectVO<ProductInfoVOEx> res= new ObjectVO<ProductInfoVOEx>();
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ProductInfoVOEx> getListProductInfoCanBeAddedNPP(
			int poAutoGroupId, String name, String code, boolean isSubCat, KPaging<ProductInfoVOEx> kPaging)
			throws BusinessException{
		try {
			List<ProductInfoVOEx> lst = poAutoGroupDAO.getListProductInfoCanBeAddedNPP(poAutoGroupId, name, code, isSubCat, kPaging);
			ObjectVO<ProductInfoVOEx> res= new ObjectVO<ProductInfoVOEx>();
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Product> getListProductCanBeAdded(
			int poAutoGroupId, String name, String code,KPaging<Product> kPaging)
			throws BusinessException{
		try {
			List<Product> lst = poAutoGroupDAO.getListProductCanBeAdded(poAutoGroupId, name, code, kPaging);
			ObjectVO<Product> res= new ObjectVO<Product>();
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<PoSecVO> getListPoSecVO(KPaging<PoSecVO> paging,Long shopId,String poConfirmNumber,
			Date fromDate, Date toDate, Integer type) throws BusinessException {
		try {
			List<PoSecVO> lst = poVnmDAO.getListPoSecVO(paging,shopId, poConfirmNumber, fromDate, toDate, type);
			ObjectVO<PoSecVO> res= new ObjectVO<PoSecVO>();
			res.setkPaging(paging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void createSec(String payReceivedNumber, BigDecimal amount, Long poVnmId,
			Long shopId,String userName, Long debitDetailId, PayReceivedType payReceivedType, Integer bankId) 
			throws BusinessException {
		try {
			//DebitDetail dd = debitDetailDAO.getDebitDetailForUpdatePo(poVnmId, shopId, debitDetailId);
			DebitDetail dd = debitDetailDAO.getDebitDetailByIdForUpdate(debitDetailId);
			if (dd == null) {
				throw new BusinessException("debitDetailId not exists");
			}
		
			/*if (dd.getRemain().compareTo(amount) < 0 || dd.getRemain().compareTo(BigDecimal.ZERO) <= 0)
				throw new BusinessException("So tien nhap vao khong hop le");
			*/
			Debit d = debitDAO.getDebit(shopId, DebitOwnerType.SHOP);
			/*if(null == amount || d.getTotalDebit().compareTo(amount) < 0) {
				throw new BusinessException("So tien nhap vao vuot qua tong cong no NPP");
			}*/
			
			if(dd.getRemain().compareTo(amount) < 0) {
				throw new BusinessException("So tien nhap vao vuot qua no don hang");
			}
			
			Date now = commonDAO.getSysDate();
			Date lockDate = shopLockDAO.getApplicationDate(shopId); // lacnv1
			
			d.setTotalPay(d.getTotalPay().add(amount));
			d.setTotalDebit(d.getTotalDebit().subtract(amount));			
			debitDAO.updateDebit(d);
			
			dd.setTotalPay(dd.getTotalPay().add(amount));
			dd.setRemain(dd.getTotal().subtract(dd.getTotalPay()));
			dd.setUpdateDate(now);
			dd.setUpdateUser(userName);
			debitDetailDAO.updateDebitDetail(dd);
			
			PayReceived pr = new PayReceived();
			pr.setPayReceivedNumber(payReceivedNumber.toUpperCase());
			pr.setPaymentType(PaymentType.MONEY);
			Shop shop = shopDAO.getShopById(shopId);
			pr.setShop(shop);
			pr.setAmount(amount);
			pr.setReceiptType(ReceiptType.PHIEU_UY_NHIEM_CHI);
			pr.setCreateDate(lockDate);
			pr.setCreateDateSys(now); //tungtt21
			pr.setCreateUser(userName);
			pr.setType(payReceivedType.getValue());//
			pr.setPaymentStatus(PaymentStatus.PAID);
			/** vuongmq, 15 September, 2014 ; description: insert ngan hang */
			Bank bank = null;
			if (bankId != null) {
				bank = bankDAO.getBankById(bankId);
			}
			if(bank != null){
				pr.setBank(bank);
			}
			pr = payReceivedDAO.createPayReceived(pr);
			
			Integer time = paymentDetailDAO.getTimeOfPay(dd.getId()); // lacnv1
			PaymentDetail pd = new PaymentDetail();
			pd.setDebit(dd);
			pd.setPayReceived(pr);
			pd.setAmount(amount);
			pd.setPaymentType(DebtPaymentType.MANUAL);
			pd.setCreateDate(now);
			pd.setCreateUser(userName);
			pd.setCreateDate(now); //tungtt21
			pd.setPayDate(lockDate); // lacnv1
			pd.setDiscount(BigDecimal.ZERO);
			pd.setTime(time + 1); // lacnv1
			pd.setPaymentStatus(PaymentStatus.PAID);
			pd.setShop(shop);
			paymentDetailDAO.createPaymentDetail(pd);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createSecGiamNo(String payReceivedNumber, BigDecimal amount, Long poVnmId,
			Long shopId, String userName, Long debitDetailId, Integer bankId)
			throws BusinessException {
		try {
			DebitDetail dd = debitDetailDAO.getDebitDetailByIdForUpdate(debitDetailId);
			if (dd == null) {
				throw new BusinessException("debitDetailId not exists");
			}

			Debit d = debitDAO.getDebit(shopId, DebitOwnerType.SHOP);

			if (amount.signum() == 1) {
				amount = amount.negate();
			}
			if(dd.getRemain().compareTo(amount) > 0) {
				throw new BusinessException("So tien nhap vao vuot qua no don hang");
			}
			
			Date now = commonDAO.getSysDate();
			Date lockDate = shopLockDAO.getApplicationDate(shopId); // lacnv1
			
			d.setTotalPay(d.getTotalPay().add(amount));
			d.setTotalDebit(d.getTotalDebit().subtract(amount));			
			debitDAO.updateDebit(d);
			
			dd.setTotalPay(dd.getTotalPay().add(amount));
			dd.setRemain(dd.getTotal().subtract(dd.getTotalPay()));
			dd.setUpdateDate(now);
			dd.setUpdateUser(userName);
			debitDetailDAO.updateDebitDetail(dd);
			
			PayReceived pr = new PayReceived();
			pr.setPayReceivedNumber(payReceivedNumber.toUpperCase());
			pr.setPaymentType(PaymentType.MONEY);
			Shop shop = shopDAO.getShopById(shopId);
			pr.setShop(shop);
			pr.setAmount(amount);
			pr.setReceiptType(ReceiptType.PHIEU_CHI_UY_NHIEM_CHI);
			pr.setCreateDate(lockDate); //tungtt21
			pr.setCreateDateSys(now); //tungtt21
			pr.setCreateUser(userName);
			pr.setType(PayReceivedType.THANH_TOAN.getValue());//
			pr.setPaymentStatus(PaymentStatus.PAID);
			/** vuongmq, 15 September, 2014 ; description: insert ngan hang */
			Bank bank = null;
			if (bankId != null) {
				bank = bankDAO.getBankById(bankId);
			}
			if(bank != null){
				pr.setBank(bank);
			}
			pr = payReceivedDAO.createPayReceived(pr);
			
			Integer time = paymentDetailDAO.getTimeOfPay(dd.getId()); // lacnv1
			PaymentDetail pd = new PaymentDetail();
			pd.setDebit(dd);
			pd.setPayReceived(pr);
			pd.setAmount(amount);
			pd.setPaymentType(DebtPaymentType.MANUAL);
			pd.setCreateDate(now);
			pd.setCreateUser(userName);
			pd.setPayDate(lockDate); //tungtt21
			pd.setDiscount(BigDecimal.ZERO);
			pd.setTime(time + 1); // lacnv1
			pd.setShop(shop);
			pd.setPaymentStatus(PaymentStatus.PAID);
			paymentDetailDAO.createPaymentDetail(pd);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Boolean isExistsInfoOfPoAutoGroup(PoAutoGroup poAutoGroup)
			throws BusinessException {
		try {
			return poAutoGroupDAO.isExistsInfoOfPoAutoGroup(poAutoGroup);
		}
		catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public PoAutoGroup getPoAutoGroupByCode(String groupCode) throws BusinessException {
		try {
			return poAutoGroupDAO.getPoAutoGroupByCode(groupCode);
		}
		catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public PoAutoGroupShopMap createPoAutoGroupShopMap(PoAutoGroupShopMap item, LogInfoVO logInfo) throws BusinessException {
		if(null == item){
			throw new IllegalArgumentException("PoAutoGroupShopMap is null");
		}
		
		try{
			return this.poAutoGroupShopMapDAO.createPoAutoGroupShopMap(item, logInfo); 
		} catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public void updatePoAutoGroupShopMap(PoAutoGroupShopMap item, LogInfoVO logInfo) throws BusinessException {
		if(null == item){
			throw new IllegalArgumentException("PoAutoGroupShopMap is null");
		}
		
		try{
			this.poAutoGroupShopMapDAO.updatePoAutoGroupShopMap(item, logInfo); 
		} catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public void deletePoAutoGroupShopMap(PoAutoGroupShopMap item, LogInfoVO logInfo) throws BusinessException {
		if(null == item){
			throw new IllegalArgumentException("PoAutoGroupShopMap is null");
		}
		
		try{
			this.poAutoGroupShopMapDAO.deletePoAutoGroupShopMap(item, logInfo); 
		} catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<PoAutoGroupShopMapVO> getListShopInPoGroup(PoAutoGroupShopMapFilter filter,
			ActiveType groupStatus) throws BusinessException {
		try {
			List<PoAutoGroupShopMapVO> lst = this.poAutoGroupShopMapDAO.getListShopInPoGroup(filter, groupStatus);
			ObjectVO<PoAutoGroupShopMapVO> vo = new ObjectVO<PoAutoGroupShopMapVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
			
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void removePOConfirm(Long poVnmId) throws BusinessException{
		if (poVnmId == null) {
			throw new IllegalArgumentException("poVnmId is null");
		}
		try{
			// update PoVnm
			PoVnm poVnm = poVnmDAO.getPoVnmById(poVnmId);
			if (poVnm == null) {
				throw new IllegalArgumentException("get poVnm is null");
			}
			Date date = commonDAO.getSysDate();
			Date lockDate = shopLockDAO.getNextLockedDay(poVnm.getShop().getId());
			if(lockDate == null){
				lockDate = date;
			}
			
			poVnm.setStatus(PoVNMStatus.PENDING);
			
			//Sua lai la ngay chot
			poVnm.setModifyDate(lockDate);
			poVnm.setCancelDate(lockDate);
			
			poVnmDAO.updatePoVnm(poVnm);
		}catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Map<String, List<PoAutoDetail>> getMapPOAuto(Long shopId,
			Long staffId, Boolean checkAllProductType) throws BusinessException {
		final String GROUP_NON_POAUTO_GROUP = "GROUP_NON_POAUTO_GROUP";
		
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (staffId == null) {
			throw new IllegalArgumentException("staffId is null");
		}
		if (checkAllProductType == null) {
			throw new IllegalArgumentException("checkAllProductType is null");
		}

		try {
			Date now = commonDAO.getSysDate();
			Shop shop = this.shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			Staff staff = staffDAO.getStaffById(staffId);
			if (staff == null) {
				throw new IllegalArgumentException("staff is null");
			}

			List<PoAutoVO> listPoAutoVo = getListPoAuto(shopId, checkAllProductType);
			
			if(null == listPoAutoVo || listPoAutoVo.size() <= 0){
				throw new IllegalArgumentException("list product is null");
			}
			
			boolean isSeparatePO = false;
			Map<Integer, List<PoAutoGroupDetailVO>> groupMap = null;
			if(Boolean.TRUE.equals(this.poAutoGroupShopMapDAO.checkExistsShopInGroup(shop.getId()))){
				groupMap = this.getListPoAutoGroupVO(shop.getId());
				if(null != groupMap && !groupMap.isEmpty()){
					isSeparatePO = true;
				}
			} else{
				groupMap = this.getListPoAutoGroupVO(null);
				if(null != groupMap && !groupMap.isEmpty()){
					isSeparatePO = true;
				}
			}
			
			Map<String, List<PoAutoDetail>> poAutoMap = new HashMap<String, List<PoAutoDetail>>();
			
			if (!isSeparatePO) {
				List<PoAutoDetail> listData = new ArrayList<PoAutoDetail>();
				
				Product product = null;
				Price price = null;
				PoAutoDetail poAutoDetail = null;
				for (PoAutoVO item : listPoAutoVo) {
					product = productDAO.getProductById(item.getProductId());
					if (product == null) {
						throw new IllegalArgumentException("product is null");
					}
					
					price = priceDAO.getPriceById(item.getPriceId());
					if (price == null) {
						throw new IllegalArgumentException("price is null");
					}
					
					poAutoDetail = new PoAutoDetail();
					
					poAutoDetail.setAmount(item.getAmount());
					poAutoDetail.setConvfact(item.getConvfact());
					poAutoDetail.setCreateDate(now);
					poAutoDetail.setDayPlan(item.getDayPlan());
					poAutoDetail.setDayReservePlan(item.getDayReservePlan());
					poAutoDetail.setDayReserveReal(new BigDecimal(item.getDayReserveReal()));
					poAutoDetail.setExport(item.getExpQty());
					poAutoDetail.setImportQuantity(item.getImpQty());
					poAutoDetail.setLead(item.getLead());
					poAutoDetail.setMonthCumulate(item.getMonthCumulate());
					poAutoDetail.setMonthPlan(item.getMonthPlan());
					poAutoDetail.setNext(item.getNext());
					poAutoDetail.setOpenStock(item.getOpenStockTotal());
					poAutoDetail.setPoAutoDate(now);
					poAutoDetail.setPriceValue(item.getPriceValue());
					poAutoDetail.setPrice(price);
					poAutoDetail.setProduct(product);
					//poAutoDetail.setQuantity(item.getQuantity() * item.getConvfact());
					poAutoDetail.setQuantity(item.getQuantity());
					poAutoDetail.setRequimentStock(item.getRequimentStock());
					poAutoDetail.setSafetyStockMin(item.getSafetyStockMin());
					poAutoDetail.setStock(item.getStockQty());
					poAutoDetail.setStockPoConfirm(item.getGoPoConfirmQty());
					poAutoDetail.setStockPoDvkh(item.getStockPoDvkh());
					poAutoDetail.setWarning(item.getWarning());
					
					listData.add(poAutoDetail);
				}
				poAutoMap.put(GROUP_NON_POAUTO_GROUP, listData);
			} else {
				List<PoAutoGroupDetailVO> listGroupProduct = groupMap.get(PoAutoGroupDetailType.PRODUCT.getValue());
				List<PoAutoGroupDetailVO> listGroupSubcat = groupMap.get(PoAutoGroupDetailType.SUB_CAT.getValue());
				List<PoAutoGroupDetailVO> listGroupCat = groupMap.get(PoAutoGroupDetailType.CAT.getValue());

				Product product = null;
				Price price = null;
				PoAutoDetail poAutoDetail = null;
				List<PoAutoDetail> listPoAutoDetail = null;
				PoAutoGroupDetailVO groupDetail = null;
				for (PoAutoVO item : listPoAutoVo) {
					product = productDAO.getProductById(item.getProductId());					
					if (product == null) {
						throw new IllegalArgumentException("product is null");
					}
					
					price = priceDAO.getPriceById(item.getPriceId());
					/*if (price == null) {
						throw new IllegalArgumentException("price is null");
					}*/
					
					poAutoDetail = new PoAutoDetail();
					
					poAutoDetail.setAmount(item.getAmount());
					poAutoDetail.setConvfact(item.getConvfact());
					poAutoDetail.setCreateDate(now);
					poAutoDetail.setDayPlan(item.getDayPlan());
					poAutoDetail.setDayReservePlan(item.getDayReservePlan());
					poAutoDetail.setDayReserveReal(new BigDecimal(item.getDayReserveReal()));
					poAutoDetail.setExport(item.getExpQty());
					poAutoDetail.setImportQuantity(item.getImpQty());
					poAutoDetail.setLead(item.getLead());
					poAutoDetail.setMonthCumulate(item.getMonthCumulate());
					poAutoDetail.setMonthPlan(item.getMonthPlan());
					poAutoDetail.setNext(item.getNext());
					poAutoDetail.setOpenStock(item.getOpenStockTotal());
					poAutoDetail.setPoAutoDate(now);
					poAutoDetail.setPriceValue(item.getPriceValue());
					poAutoDetail.setPrice(price);
					poAutoDetail.setProduct(product);
					//poAutoDetail.setQuantity(item.getQuantity()* item.getConvfact());
					poAutoDetail.setQuantity(item.getQuantity());
					poAutoDetail.setRequimentStock(item.getRequimentStock());
					poAutoDetail.setSafetyStockMin(item.getSafetyStockMin());
					poAutoDetail.setStock(item.getStockQty());
					poAutoDetail.setStockPoConfirm(item.getGoPoConfirmQty());
					poAutoDetail.setStockPoDvkh(item.getStockPoDvkh());
					poAutoDetail.setWarning(item.getWarning());
					
					/*if(poAutoDetail.getProduct().getSubCat().getId().compareTo(145l) == 0){
						System.out.println("ABC");
					}*/
					
					boolean isExists = false;
					if (listGroupProduct != null && listGroupProduct.size() > 0) {
						for(int j = 0, szj = listGroupProduct.size(); j < szj; j++){
							groupDetail = listGroupProduct.get(j);
							if(groupDetail.getObjectId().compareTo(poAutoDetail.getProduct().getId()) == 0){
								listPoAutoDetail = poAutoMap.get(groupDetail.getGroupCode());
								
								if(listPoAutoDetail == null){
									listPoAutoDetail = new ArrayList<PoAutoDetail>();
									
									poAutoMap.put(groupDetail.getGroupCode(), listPoAutoDetail);
								}
								
								listPoAutoDetail.add(poAutoDetail);
								isExists = true;
								break;
							}
						}
						
						if(isExists){
							continue;
						}
					}

					if (listGroupSubcat != null && listGroupSubcat.size() > 0) {
						for(int j = 0, szj = listGroupSubcat.size(); j < szj; j++){
							groupDetail = listGroupSubcat.get(j);
							if(groupDetail.getObjectId().compareTo(poAutoDetail.getProduct().getSubCat().getId()) == 0){
								listPoAutoDetail = poAutoMap.get(groupDetail.getGroupCode());
								
								if(listPoAutoDetail == null){
									listPoAutoDetail = new ArrayList<PoAutoDetail>();
									
									poAutoMap.put(groupDetail.getGroupCode(), listPoAutoDetail);
								}
								
								listPoAutoDetail.add(poAutoDetail);
								isExists = true;
								break;
							}
						}
						
						if(isExists){
							continue;
						}
					}

					if (listGroupCat != null && listGroupCat.size() > 0) {
						for(int j = 0, szj = listGroupCat.size(); j < szj; j++){
							groupDetail = listGroupCat.get(j);
							if(groupDetail.getObjectId().compareTo(poAutoDetail.getProduct().getCat().getId()) == 0){
								listPoAutoDetail = poAutoMap.get(groupDetail.getGroupCode());
								
								if(listPoAutoDetail == null){
									listPoAutoDetail = new ArrayList<PoAutoDetail>();
									
									poAutoMap.put(groupDetail.getGroupCode(), listPoAutoDetail);
								}
								
								listPoAutoDetail.add(poAutoDetail);
								isExists = true;
								break;
							}
						}
						
						if(isExists){
							continue;
						}
					}
					
					// Neu khong thuoc nhom nao thi tao ra 1 nhom moi.
					listPoAutoDetail = poAutoMap.get(GROUP_NON_POAUTO_GROUP);
					
					if(listPoAutoDetail == null){
						listPoAutoDetail = new ArrayList<PoAutoDetail>();
						
						poAutoMap.put(GROUP_NON_POAUTO_GROUP, listPoAutoDetail);
					}
					
					listPoAutoDetail.add(poAutoDetail);
				}
			}
			return poAutoMap;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<PoCSVO> getListPoCSVOEx(Long shopId, String orderNumber, String poCoNumber, Date fromDate, Date toDate,
			List<Integer> lstPoStatus, boolean hasFindChildShop) throws BusinessException {
		try {
			return poVnmDAO.getListPoCSVO(shopId, orderNumber, poCoNumber, fromDate,
					toDate, lstPoStatus, hasFindChildShop);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<PoCSVO> getListPoCSVOFilter(PoVnmFilter filter) throws BusinessException {
		try {
			return poVnmDAO.getListPoCSVOFilter(filter);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.PoMgr#getPoDVKHBySaleOrderNumber(ths.dms.core.entities.enumtype.PoVnmFilter)
	 */
	@Override
	public PoVnm getPoDVKHBySaleOrderNumber(PoVnmFilter filter) throws BusinessException {
		try {
			return poVnmDAO.getPoDVKHBySaleOrderNumber(filter);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
}
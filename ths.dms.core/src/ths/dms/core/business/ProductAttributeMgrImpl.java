/**
 * 
 */
package ths.dms.core.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ProductAttribute;
import ths.dms.core.entities.ProductAttributeDetail;
import ths.dms.core.entities.ProductAttributeEnum;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.ProductAttributeReqType;
import ths.dms.core.entities.filter.ProductAttributeFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProductAttributeEnumVO;
import ths.dms.core.entities.vo.ProductAttributeVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.ProductAttributeDAO;

/**
 * Quan ly thuoc tinh san pham
 * @author tientv11
 *
 */
public class ProductAttributeMgrImpl implements ProductAttributeMgr {
	
	@Autowired
	ProductAttributeDAO productAttributeDAO;

	
	@Override
	public List<ProductAttributeVO> getListProductAttributeVOFilter(ProductAttributeFilter filter) throws BusinessException {
		try {
			List<ProductAttributeVO> attributeVOs = new ArrayList<ProductAttributeVO>();
			
			attributeVOs = productAttributeDAO.getListProductAttributeVOFilter(filter);
			
			for(ProductAttributeVO vo : attributeVOs){
				filter.setProductAttId(vo.getId());
				if(AttributeColumnType.CHOICE.getValue().equals(vo.getType())
					|| AttributeColumnType.MULTI_CHOICE.getValue().equals(vo.getType())){					
					vo.setAttributeEnumVOs(productAttributeDAO.getListProductAttributeEnumVOFilter(filter));										
				}				
				if(filter.getProductId() != null){
					vo.setAttributeDetailVOs(productAttributeDAO.getListProductAttributeDetailVOFilter(filter));
				}
			}
			return attributeVOs;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveOrUpdateProductAttribute(AttributeDynamicVO vo, LogInfoVO logInfo ) throws BusinessException{
		if(vo == null){
			throw new IllegalArgumentException("attributeDynamicVO is null");
		}
		try{			
			//ProductAttribute att = productAttributeDAO.getProductAttributeById(vo.getAttributeId());
//			ProductAttribute att = productAttributeDAO.getProductAttributeByCode(vo.getAttributeCode());
			if(vo.getAttributeId()!=null && vo.getAttributeId() >0L ){
				ProductAttribute att = productAttributeDAO.getProductAttributeById(vo.getAttributeId());
				if(att == null){
					throw new IllegalArgumentException("ProductAttribute id is null");
				}
				att.setAttributeName(vo.getAttributeName());
				att.setAttributeReqType(ProductAttributeReqType.parseValue(vo.getMandatory()));
				att.setDescription(vo.getDescription());
				att.setDisplayOrder(vo.getDisplayOrder());
				att.setStatus(ActiveType.parseValue(vo.getStatus()));
				if(vo.getType()!=null){
					if(AttributeColumnType.CHARACTER.getValue().equals(vo.getType())){
						if(vo.getDataLength() != null){
							att.setDataLength(Integer.valueOf(vo.getDataLength().toString()));
						}else{
							att.setDataLength(null);
						}
					}else if(AttributeColumnType.NUMBER.getValue().equals(vo.getType())){
						if(vo.getDataLength() != null){
							att.setDataLength(Integer.valueOf(vo.getDataLength().toString()));
						}else{
							att.setDataLength(null);
						}
						att.setMinValue(Integer.valueOf(vo.getMinValue().toString()));
						att.setMaxValue(Integer.valueOf(vo.getMaxValue().toString()));
					}
				}
				productAttributeDAO.updateProductAttribute(att,logInfo);
			}else{
				ProductAttribute att = new ProductAttribute();
				att.setAttributeCode(vo.getAttributeCode());
				att.setAttributeName(vo.getAttributeName());
				att.setValueType(AttributeColumnType.parseValue(vo.getType()));
				att.setAttributeReqType(ProductAttributeReqType.parseValue(vo.getMandatory()));
				att.setDescription(vo.getDescription());
				att.setDisplayOrder(vo.getDisplayOrder());
				att.setStatus(ActiveType.RUNNING);
				if(vo.getType()!=null){
					if(AttributeColumnType.CHARACTER.getValue().equals(vo.getType())){
						if(vo.getDataLength() != null){
							att.setDataLength(Integer.valueOf(vo.getDataLength().toString()));
						}else{
							att.setDataLength(null);
						}
					}else if(AttributeColumnType.NUMBER.getValue().equals(vo.getType())){
						if(vo.getDataLength() != null){
							att.setDataLength(Integer.valueOf(vo.getDataLength().toString()));
						}else{
							att.setDataLength(null);
						}
						att.setMinValue(Integer.valueOf(vo.getMinValue().toString()));
						att.setMaxValue(Integer.valueOf(vo.getMaxValue().toString()));
					}
				}
				att = productAttributeDAO.createProductAttribute(att,logInfo);
			}
		}catch(DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveOrUpdateProductAttributeEnum(AttributeEnumVO vo, LogInfoVO logInfo ) throws BusinessException{
		if(vo == null){
			throw new IllegalArgumentException("attributeEnumVO is null");
		}
		try{
			ProductAttribute att = productAttributeDAO.getProductAttributeById(vo.getAttributeId());			
			if(vo.getEnumId()!=null && vo.getEnumId()> 0L){
				ProductAttributeEnum attEnum = productAttributeDAO.getProductAttributeEnumById(vo.getEnumId());
				attEnum.setValue(vo.getEnumValue());
				productAttributeDAO.updateProductAttributeEnum(attEnum,logInfo);
			}else{
				ProductAttributeEnum attEnum = new ProductAttributeEnum();
				attEnum.setCode(vo.getEnumCode().toUpperCase());
				attEnum.setValue(vo.getEnumValue());
				attEnum.setProductAttribute(att);
				attEnum.setStatus(ActiveType.RUNNING);
				attEnum = productAttributeDAO.createProductAttributeEnum(attEnum,logInfo);
			}
		}catch(DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ProductAttributeEnum getProductAttributeEnumById(long id) throws BusinessException {
		try {
			return productAttributeDAO.getProductAttributeEnumById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateProductAttributeEnum(ProductAttributeEnum attribute,LogInfoVO logInfo) throws BusinessException {
		try {
			productAttributeDAO.updateProductAttributeEnum(attribute,logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public void updateProductAttributeDetail(ProductAttributeDetail attribute,LogInfoVO logInfo) throws BusinessException {
		try {
			productAttributeDAO.updateProductAttributeDetail(attribute,logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public List<ProductAttributeDetail> getListProductAttributeByEnumId(long id) throws BusinessException {
		try {
			return productAttributeDAO.getListProductAttributeDetailByEnumId(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ProductAttributeEnumVO> getListProductAttributeEnumVOFilter(
			ProductAttributeFilter filter) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

}

package ths.dms.core.business;

/**
 * Import Thu Vien
 * */
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipDeliveryRecord;
import ths.dms.core.entities.EquipFormHistory;
import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipHistory;
import ths.dms.core.entities.EquipLiquidationForm;
import ths.dms.core.entities.EquipLiquidationFormDtl;
import ths.dms.core.entities.EquipLostMobileRec;
import ths.dms.core.entities.EquipLostRecord;
import ths.dms.core.entities.EquipStatisticCustomer;
import ths.dms.core.entities.EquipStatisticGroup;
import ths.dms.core.entities.EquipStatisticRecDtl;
import ths.dms.core.entities.EquipStatisticRecord;
import ths.dms.core.entities.EquipStatisticStaff;
import ths.dms.core.entities.EquipStatisticUnit;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.EquipStockTotal;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DeliveryType;
import ths.dms.core.entities.enumtype.EquipObjectType;
import ths.dms.core.entities.enumtype.EquipStockEquipFilter;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeStatus;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUsageStatus;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.LinquidationStatus;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.SpParam;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.TableName;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipStatisticFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.vo.EquipLostMobileRecVO;
import ths.dms.core.entities.vo.EquipRecordShopVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipStatisticCustomerVO;
import ths.dms.core.entities.vo.EquipStatisticRecordDetailVO;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentLiquidationFormVO;
import ths.dms.core.entities.vo.EquipmentRecordDetailVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StatisticCheckingVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.EquipRecordDAO;
import ths.dms.core.dao.EquipStatisticGroupDAO;
import ths.dms.core.dao.EquipStatisticRecordDAO;
import ths.dms.core.dao.EquipmentLiquidationFormDAO;
import ths.dms.core.dao.EquipmentManagerDAO;
import ths.dms.core.dao.EquipmentStockTotalDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.StaffDAO;
import ths.dms.core.dao.StockTotalDAO;
import ths.dms.core.dao.repo.IRepository;

/**
 * Xu ly du lieu
 * 
 * @author hunglm16
 * @since December 31,2014
 * @description Dung chung cho Bien ban Thiet Bi, hi
 * */
public class EquipRecordMgrImpl implements EquipRecordMgr {
	@Autowired
	private IRepository repo;
	@Autowired
	CommonDAO commDAO;
	@Autowired
	StaffDAO staffDAO;
	@Autowired
	StockTotalDAO stockTotalDAO;
	@Autowired
	ShopDAO shopDAO;
	@Autowired
	EquipRecordDAO equipRecordDAO;
	@Autowired
	EquipmentLiquidationFormDAO equipliquidationFormDAO;
	@Autowired
	EquipmentManagerDAO equipMngDAO;
	@Autowired
	EquipmentStockTotalDAO equipmentStockTotalDAO;
	@Autowired
	EquipmentManagerMgr equipmentManagerMgr;

	@Autowired
	EquipStatisticRecordDAO equipStatisticRecordDAO;
	@Autowired
	EquipStatisticGroupDAO equipStatisticGroupDAO;

	@Override
	public EquipLostRecord getEquipLostRecordById(Long id) throws BusinessException {
		try {
			return id == null ? null : equipRecordDAO.getEquipLostRecordById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipLostRecord getEquipLostRecordById(Long id, Long shopId) throws BusinessException {
		try {
			return id == null ? null : equipRecordDAO.getEquipLostRecordById(id, shopId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipLostMobileRec getEquipLostMobileRecById(Long id) throws BusinessException {
		try {
			return id == null ? null : equipRecordDAO.getEquipLostMobileRecById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipLostMobileRec getEquipLostMobileRecById(Long id, Long shopId) throws BusinessException {
		try {
			return id == null ? null : equipRecordDAO.getEquipLostMobileRecById(id, shopId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public Integer countEquipLostMobileRecByFilter(EquipRecordFilter<EquipRecordVO> filter) throws BusinessException {
		try {
			return filter == null ? null : equipRecordDAO.countEquipLostMobileRecByFilter(filter);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipLostRecord createEquipLostRecord(EquipLostRecord equipLostRecord) throws BusinessException {
		try {
			return equipRecordDAO.createEquipLostRecord(equipLostRecord);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipStatisticRecDtl geEquipStatisticRecDtlById(Long id) throws BusinessException {
		try {
			return equipRecordDAO.getEquipStatisticRecDtlById(id);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipRecordVO> searchListEquipLostRecordVOByFilter(EquipRecordFilter<EquipRecordVO> filter) throws BusinessException {
		try {
			List<EquipRecordVO> lst = equipRecordDAO.searchListEquipLostRecordVOByFilter(filter);
			ObjectVO<EquipRecordVO> objVO = new ObjectVO<EquipRecordVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * save detail after checking
	 * @author phut
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveGroupCheking(List<Long> listDetailId, List<Integer> listExists, LogInfoVO logInfo) throws BusinessException {
		if(listDetailId.isEmpty() || listExists.isEmpty() || listDetailId.size() != listExists.size()) {
			throw new IllegalAccessError("listDetailId or listExists error");
		}
		try {
			Date currentDate = commDAO.getSysDate();
			Staff staff = staffDAO.getStaffByCode(logInfo.getStaffCode());
			for(int i = 0 ; i < listDetailId.size(); i++) {
				EquipStatisticRecDtl detail = equipRecordDAO.getEquipStatisticRecDtlById(listDetailId.get(i));
				if(detail == null) {
					continue;
				}
				if(listExists.get(i) == 0) {//mat
					detail.setStatus(3);
					detail.setQuantity(1);
					detail.setQuantityActually(0);
				} else if(listExists.get(i) == 1) {//con
					detail.setStatus(2);
					detail.setQuantity(1);
					detail.setQuantityActually(1);
				}
//				detail.setStatisticDate(currentDate);
				detail.setStaff(staff);
				detail.setUpdateDate(currentDate);
				detail.setUpdateUser(logInfo.getStaffCode());
				commDAO.updateEntity(detail);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * save detail after checking
	 * @author phut
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveShelfCheking(List<Long> listDetailId, List<Integer> listActQty, LogInfoVO logInfo) throws BusinessException {
		if(listDetailId.isEmpty() || listActQty.isEmpty() || listDetailId.size() != listActQty.size()) {
			throw new IllegalAccessError("listDetailId or listActQty error");
		}
		
		try {
			Date currentDate = commDAO.getSysDate();
			Staff staff = staffDAO.getStaffByCode(logInfo.getStaffCode());
			for(int i = 0 ; i < listDetailId.size(); i++) {
				EquipStatisticRecDtl detail = equipRecordDAO.getEquipStatisticRecDtlById(listDetailId.get(i));
				if(detail == null) {
					continue;
				}
				detail.setStatus(1);
				StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwner(detail.getObjectId(), StockObjectType.SHOP, detail.getShop().getId(), detail.getObjectStockId());
				if(stockTotal == null) {
					continue;
				}
				detail.setQuantity(stockTotal.getQuantity());
				detail.setQuantityActually(listActQty.get(i));
				detail.setStaff(staff);
//				detail.setStatisticDate(currentDate);
				detail.setUpdateDate(currentDate);
				detail.setUpdateUser(logInfo.getStaffCode());
				commDAO.updateEntity(detail);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * load list detail for record. check is exists. if not create new
	 * @author phut
	 */
	@Override
	public ObjectVO<StatisticCheckingVO> getListDetailGroupSoLan(KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId, Long equipGroupId, String equipCode, String seri, Long stockId, Integer stepCheck, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commDAO.getSysDate();
			ObjectVO<StatisticCheckingVO> voObj = new ObjectVO<StatisticCheckingVO>();
			EquipStatisticRecord __record = equipStatisticRecordDAO.getEquipStatisticRecordById(recordId);
			if(__record == null) {
				throw new IllegalArgumentException("record is null");
			}
			if(kPaging!=null && kPaging.getPage() == 0) {//get first page => load all to check is exists
				List<StatisticCheckingVO> list = equipRecordDAO.getListDetailGroupSoLan(null, recordId, shopId, equipGroupId, equipCode, seri, stockId, stepCheck);
				for(StatisticCheckingVO vo : list) {
					if(vo.getDetailId() == null) {//create new
						EquipStatisticRecDtl detail = new EquipStatisticRecDtl();
						detail.setEquipStatisticRecord(__record);
						detail.setShop(shopDAO.getShopById(vo.getShopId()));
						detail.setObjectStockId(vo.getStockId());
						detail.setObjectId(vo.getEquipId());
						detail.setStatus(0);//chua kiem ke
						detail.setObjectType(1);//thiet bi
						/** check lan kiem ke */
//						List<EquipStatisticRecDtl> __listDetail = equipRecordDAO.getListEquipStatisticRecDetail(null, recordId, vo.getShopId(), vo.getStockId(), vo.getEquipId());
//						if(__listDetail != null && !__listDetail.isEmpty()) {
//							/**
//							 * if largest statisticTime is not equal stepCheck - 1
//							 * do not insert new record
//							 */
//							EquipStatisticRecDtl __detail = __listDetail.get(0);
//							if(__detail.getStatisticTime() != stepCheck - 1) {
//								continue;
//							}
//						} /*else if(stepCheck != 1) {
//							continue;
//						}*/
						detail.setStatisticTime(stepCheck);
						detail.setObjectStockType(1);
						detail.setCreateDate(currentDate);
						detail.setStatisticDate(currentDate);
						detail.setCreateUser(logInfo.getStaffCode());
						commDAO.createEntity(detail);
					}
				}
				
				list = equipRecordDAO.getListDetailGroupSoLan(kPaging, recordId, shopId, equipGroupId, equipCode, seri, stockId, stepCheck);
				voObj.setkPaging(kPaging);
				voObj.setLstObject(list);
			} else {
				List<StatisticCheckingVO> list = equipRecordDAO.getListDetailGroupSoLan(kPaging, recordId, shopId, equipGroupId, equipCode, seri, stockId, stepCheck);
				voObj.setkPaging(kPaging);
				voObj.setLstObject(list);
			}
			return voObj;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * load list detail for record. check is exists. if not create new
	 * @author phut
	 */
	@Override
	public ObjectVO<StatisticCheckingVO> getListDetailGroupTuyen(KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId, Long equipGroupId, String equipCode, String seri, Long stockId, Date checkDate, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commDAO.getSysDate();
			ObjectVO<StatisticCheckingVO> voObj = new ObjectVO<StatisticCheckingVO>();
			EquipStatisticRecord __record = equipStatisticRecordDAO.getEquipStatisticRecordById(recordId);
			if(__record == null) {
				throw new IllegalArgumentException("record is null");
			}
			Calendar cal = Calendar.getInstance();
			String dayOfWeek = "";
			if (checkDate != null) {
				cal.setTime(checkDate);
				if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					dayOfWeek = "CN";
				} else {
					dayOfWeek = "T" + cal.get(Calendar.DAY_OF_WEEK);
				}
			}
			if(kPaging!=null && kPaging.getPage() == 0) {//get first page => load all to check is exists
				List<StatisticCheckingVO> list = equipRecordDAO.getListDetailGroupTuyen(null, recordId, shopId, equipGroupId, equipCode, seri, stockId, checkDate);
				for(StatisticCheckingVO vo : list) {
					if(vo.getDetailId() == null) {//create new
						EquipStatisticRecDtl detail = new EquipStatisticRecDtl();
						detail.setEquipStatisticRecord(__record);
						detail.setShop(shopDAO.getShopById(vo.getShopId()));
						detail.setObjectStockId(vo.getStockId());
						detail.setObjectId(vo.getEquipId());
						detail.setStatus(0);//chua kiem ke
						detail.setObjectType(1);//
						detail.setStatisticDate(checkDate);
						detail.setDateInWeek(dayOfWeek);
						detail.setObjectStockType(1);//thiet bi
						detail.setCreateDate(currentDate);
						detail.setCreateUser(logInfo.getStaffCode());
						commDAO.createEntity(detail);
					}
				}
				
				list = equipRecordDAO.getListDetailGroupTuyen(kPaging, recordId, shopId, equipGroupId, equipCode, seri, stockId, checkDate);
				voObj.setkPaging(kPaging);
				voObj.setLstObject(list);
			} else {
				List<StatisticCheckingVO> list = equipRecordDAO.getListDetailGroupTuyen(kPaging, recordId, shopId, equipGroupId, equipCode, seri, stockId, checkDate);
				voObj.setkPaging(kPaging);
				voObj.setLstObject(list);
			}
			return voObj;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * load list detail for record. check is exists. if not create new
	 * @author phut
	 */
	@Override
	public ObjectVO<StatisticCheckingVO> getListDetailShelfSoLan(KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId, Long productId, Long stockId, Integer stepCheck, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commDAO.getSysDate();
			ObjectVO<StatisticCheckingVO> voObj = new ObjectVO<StatisticCheckingVO>();
			EquipStatisticRecord __record = equipStatisticRecordDAO.getEquipStatisticRecordById(recordId);
			if(__record == null) {
				throw new IllegalArgumentException("record is null");
			}
			if(kPaging!=null && kPaging.getPage() == 0) {//get first page => load to check is exists. in case don't exist=> insert
				List<StatisticCheckingVO> list = equipRecordDAO.getListDetailShelfSoLan(null, recordId, shopId, productId, stockId, stepCheck);
				for(StatisticCheckingVO vo : list) {
					if(vo.getDetailId() == null) {//create new
						EquipStatisticRecDtl detail = new EquipStatisticRecDtl();
						detail.setEquipStatisticRecord(__record);
						detail.setShop(shopDAO.getShopById(vo.getShopId()));
						detail.setObjectStockId(vo.getStockId());
						detail.setObjectId(vo.getEquipId());
						detail.setStatus(0);//chua kiem ke
						detail.setObjectType(2);//u ke
//						List<EquipStatisticRecDtl> __listDetail = equipRecordDAO.getListEquipStatisticRecDetail(null, recordId, vo.getShopId(), vo.getStockId(), vo.getEquipId());
//						if(__listDetail != null && !__listDetail.isEmpty()) {
//							/**
//							 * if largest statisticTime is not equal stepCheck - 1
//							 * do not insert new record
//							 */
//							EquipStatisticRecDtl __detail = __listDetail.get(0);
//							if(__detail.getStatisticTime() != stepCheck - 1) {
//								continue;
//							}
//						} else if(stepCheck != 1) {
//							continue;
//						}
						detail.setStatisticTime(stepCheck);
						detail.setObjectStockType(1);
						detail.setCreateDate(currentDate);
						detail.setStatisticDate(currentDate);
						detail.setCreateUser(logInfo.getStaffCode());
						commDAO.createEntity(detail);
					}
				}
				
				list = equipRecordDAO.getListDetailShelfSoLan(kPaging, recordId, shopId, productId, stockId, stepCheck);
				voObj.setkPaging(kPaging);
				voObj.setLstObject(list);
			} else {
				List<StatisticCheckingVO> list = equipRecordDAO.getListDetailShelfSoLan(kPaging, recordId, shopId, productId, stockId, stepCheck);
				voObj.setkPaging(kPaging);
				voObj.setLstObject(list);
			}
			return voObj;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * load list detail for record. check is exists. if not create new
	 * @author phut
	 */
	@Override
	public ObjectVO<StatisticCheckingVO> getListDetailShelfTuyen(KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId, Long productId, Long stockId, Date checkDate, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commDAO.getSysDate();
			ObjectVO<StatisticCheckingVO> voObj = new ObjectVO<StatisticCheckingVO>();
			EquipStatisticRecord __record = equipStatisticRecordDAO.getEquipStatisticRecordById(recordId);
			if(__record == null) {
				throw new IllegalArgumentException("record is null");
			}
			Calendar cal = Calendar.getInstance();
			String dayOfWeek = "";
			if (checkDate != null) {
				cal.setTime(checkDate);
				if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					dayOfWeek = "CN";
				} else {
					dayOfWeek = "T" + cal.get(Calendar.DAY_OF_WEEK);
				}
			}
			if(kPaging!=null && kPaging.getPage() == 0) {//get first page => load to check is exists. in case don't exist=> insert
				List<StatisticCheckingVO> list = equipRecordDAO.getListDetailShelfTuyen(null, recordId, shopId, productId, stockId, checkDate);
				for(StatisticCheckingVO vo : list) {
					if(vo.getDetailId() == null) {//create new
						EquipStatisticRecDtl detail = new EquipStatisticRecDtl();
						detail.setEquipStatisticRecord(__record);
						detail.setShop(shopDAO.getShopById(vo.getShopId()));
						detail.setObjectStockId(vo.getStockId());
						detail.setObjectId(vo.getEquipId());
						detail.setStatus(0);//chua kiem ke
						detail.setObjectType(2);//u ke
						detail.setStatisticDate(checkDate);
						detail.setDateInWeek(dayOfWeek);
						detail.setObjectStockType(1);
						detail.setCreateDate(currentDate);
						detail.setCreateUser(logInfo.getStaffCode());
						commDAO.createEntity(detail);
					}
				}
				
				list = equipRecordDAO.getListDetailShelfTuyen(kPaging, recordId, shopId, productId, stockId, checkDate);
				voObj.setkPaging(kPaging);
				voObj.setLstObject(list);
			} else {
				List<StatisticCheckingVO> list = equipRecordDAO.getListDetailShelfTuyen(kPaging, recordId, shopId, productId, stockId, checkDate);
				voObj.setkPaging(kPaging);
				voObj.setLstObject(list);
			}
			return voObj;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<FileVO> searchListFileVOByFilter(EquipRecordFilter<FileVO> filter) throws BusinessException {
		try {
			List<FileVO> lst = equipRecordDAO.searchListFileVOByFilter(filter);
			ObjectVO<FileVO> objVO = new ObjectVO<FileVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipRecordVO> printfEquipLostRecordVOByFilter(EquipRecordFilter<EquipRecordVO> filter) throws BusinessException {
		try {
			List<EquipRecordVO> lst = equipRecordDAO.printfEquipLostRecordVOByFilter(filter);
			ObjectVO<EquipRecordVO> objVO = new ObjectVO<EquipRecordVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipLostRecord> searchEquipLostRecordByFilter(EquipRecordFilter<EquipLostRecord> filter) throws BusinessException {
		try {
			List<EquipLostRecord> lst = equipRecordDAO.searchEquipLostRecordByFilter(filter);
			ObjectVO<EquipLostRecord> objVO = new ObjectVO<EquipLostRecord>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipAttachFile> searchEquipAttachFileByFilter(BasicFilter<EquipAttachFile> filter) throws BusinessException {
		try {
			List<EquipAttachFile> lst = equipRecordDAO.searchEquipAttachFileByFilter(filter);
			ObjectVO<EquipAttachFile> objVO = new ObjectVO<EquipAttachFile>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipAttachFile getEquipAttachFileById(Long id) throws BusinessException {
		try {
			return equipRecordDAO.getEquipAttachFileById(id);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<EquipStatisticGroup> getListEquipStatisticGroup(Long id) throws BusinessException {
		try {
			return equipStatisticGroupDAO.getListEquipStatisticGroup(id);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<EquipmentRecordDetailVO> getListEquipmentRecordDetailVOByRecordId(Long recordId) throws BusinessException {
		try {
			return equipStatisticGroupDAO.getListEquipmentRecordDetailVOByRecordId(recordId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<StatisticCheckingVO> getListStatisticCheckingVOByRecordId(KPaging<StatisticCheckingVO> kPaging, Long recordId, String shopCode, String shortCode, String address, String equipCode, String seri, Integer status)
			throws BusinessException {
		try {
			ObjectVO<StatisticCheckingVO> vo = new ObjectVO<StatisticCheckingVO>();
			List<StatisticCheckingVO> list = equipStatisticGroupDAO.getListStatisticCheckingVOByRecordId(kPaging, recordId, shopCode, shortCode, address, equipCode, seri, status);
			vo.setkPaging(kPaging);
			vo.setLstObject(list);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ImageVO> getListImageVO(EquipStatisticFilter filter) throws BusinessException {
		try {
			ObjectVO<ImageVO> vo = new ObjectVO<ImageVO>();
			List<ImageVO> list = equipStatisticGroupDAO.getListImageVO(filter);
			vo.setkPaging(filter.getkPagingImageVO());
			vo.setLstObject(list);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<EquipGroup> getListEquipGroupByRecordId(Long recordId) throws BusinessException {
		try {
			return equipStatisticGroupDAO.getListEquipGroupByRecordId(recordId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Equipment> getListEquipByRecordId(Long recordId) throws BusinessException {
		try {
			return equipStatisticGroupDAO.getListEquipByRecordId(recordId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * get list u, ke
	 * @author phut
	 */
	@Override
	public List<Product> getListShelfByRecordId(Long recordId) throws BusinessException {
		try {
			return equipStatisticGroupDAO.getListShelfByRecordId(recordId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipGroup> getListEquipGroupExceptInRecord(KPaging<EquipGroup> kPaging, Long recordId, String code, Long catId, Float fromCapacity, Float toCapacity, String brand) throws BusinessException {
		try {
			List<EquipGroup> lst = equipStatisticGroupDAO.getListEquipGroupExceptInRecord(kPaging, recordId, code, catId, fromCapacity, toCapacity, brand);
			ObjectVO<EquipGroup> vo = new ObjectVO<EquipGroup>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * get list u, ke not in equip_statistic_group
	 * @author phut
	 */
	@Override
	public ObjectVO<Product> getListShelfExceptInRecord(KPaging<Product> kPaging, Long recordId, String code, String name) throws BusinessException {
		try {
			List<Product> lst = equipStatisticGroupDAO.getListShelfExceptInRecord(kPaging, recordId, code, name);
			ObjectVO<Product> vo = new ObjectVO<Product>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<EquipRecordShopVO> getListShopOfRecord(Long recordId, List<ShopSpecificType> lstShopSpecificType, String shopCode, String shopName) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getListShopOfRecord(recordId, lstShopSpecificType, shopCode, shopName);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * get list shop exist in record
	 * @author phut
	 */
	@Override
	public List<Shop> getAllShopByLstId(Long recordId, List<Long> lstId) throws BusinessException {
		try {
			return shopDAO.getAllShopByLstId(recordId, lstId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipRecordShopVO> searchShopForRecord(Long recordId, Long pShopId, String shopCode, String shopName, Integer isLevel) throws BusinessException {
		try {
			return equipStatisticRecordDAO.searchShopForRecord(recordId, pShopId, shopCode, shopName, isLevel);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * check shop in record
	 * @author phut
	 */
	@Override
	public Integer checkShopInRecordUnit(Long recordId, Long shopId) throws BusinessException {
		try {
			return equipStatisticRecordDAO.checkShopInRecordUnit(recordId, shopId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteCustomer(Long id) throws BusinessException {
		try {
		EquipStatisticCustomer obj = commDAO.getEntityById(EquipStatisticCustomer.class, id);
		if(obj != null) {
			commDAO.deleteEntity(obj);
		}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteAllCustomer(Long id) throws BusinessException {
		try {
			List<EquipStatisticCustomer> lst = equipStatisticRecordDAO.getListEquipStatisticCustomer(id);
			if(lst != null && !lst.isEmpty()) {
				for(EquipStatisticCustomer obj : lst){
					commDAO.deleteEntity(obj);
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * get list customer join in record
	 * @author phut
	 */
	@Override
	public ObjectVO<EquipStatisticCustomerVO> searchCustomer4Record(KPaging<EquipStatisticCustomerVO> kPaging, Long recordId, String shortCode, String customerName, Integer statusCus) throws BusinessException {
		try {
			EquipStatisticRecord record = equipStatisticRecordDAO.getEquipStatisticRecordById(recordId);
			List<EquipStatisticCustomerVO> list = null;
			final Integer STATUS_CUS_CHECK_STOP = 0; 
			final Integer STATUS_CUS_CHECK_RUN = 1; 
			if (record.getRecordStatus() > 0) {
				list = equipStatisticRecordDAO.searchCustomer4Record(kPaging, recordId, shortCode, customerName, STATUS_CUS_CHECK_RUN, statusCus);
			} else {
				List<Product> listShelf = equipStatisticGroupDAO.getListShelfByRecordId(recordId);
				if (!listShelf.isEmpty()) {
					list = equipStatisticRecordDAO.searchCustomer4Record(kPaging, recordId, shortCode, customerName, STATUS_CUS_CHECK_STOP, statusCus);
				} else {
					list = equipStatisticRecordDAO.searchCustomer4RecordEquipGroup(kPaging, recordId, shortCode, customerName, statusCus);
				}
			}
			ObjectVO<EquipStatisticCustomerVO> vo = new ObjectVO<EquipStatisticCustomerVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(list);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<EquipRecordShopVO> getListShopInRecord(long recordId, List<Long> lstShopId, boolean shopMapOnly) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getListShopInRecord(recordId, lstShopId, shopMapOnly);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean createListEquipStatisticUnit(List<EquipStatisticUnit> lstUnit, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commDAO.getSysDate();
			for (EquipStatisticUnit psm : lstUnit) {
				psm.setCreateDate(currentDate);
				psm.setCreateUser(logInfo.getStaffCode());
				equipStatisticRecordDAO.createEquipStatisticUnit(psm, logInfo);
			}
			return true;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * create list equip_statistic_customer when import
	 * @author phut
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean createListEquipStatisticCustomer(List<EquipStatisticCustomer> list, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commDAO.getSysDate();
			List<EquipStatisticCustomer> listInsert = new ArrayList<EquipStatisticCustomer>();
			Map<Long, EquipStatisticCustomer> listExist = equipStatisticRecordDAO.getListExistsCustomer(list);
			for(EquipStatisticCustomer en : list) {
				if(listExist.get(en.getCustomer().getId()) != null) {
					continue;
				}
				
				en.setCreateDate(currentDate);
				en.setCreateUser(logInfo.getStaffCode());
				
				listInsert.add(en);
			}
			
			if(listInsert.isEmpty()) {
				return false;
			}
			
			equipStatisticRecordDAO.insertListEquipStatisticCustomer(listInsert);
			
			return true;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * delete customer from record
	 * @author phut
	 */
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void deleteEquipStatisticUnit(long recordId, long shopId, LogInfoVO logInfo, Boolean deleteChild) throws BusinessException {
		try {
			Shop sh = shopDAO.getShopById(shopId);
			if (sh == null) {
				throw new BusinessException("shopId is not exist");
			}
			EquipStatisticRecord pp = equipStatisticRecordDAO.getEquipStatisticRecordById(recordId);
			if (pp == null) {
				throw new BusinessException("EquipStatisticRecord is not exist");
			}

			List<EquipStatisticUnit> listPromotionShopMap = equipStatisticRecordDAO.getListEquipStatisticUnitChildShopMapWithShopAndRecord(shopId, recordId, deleteChild);

			for (EquipStatisticUnit psm : listPromotionShopMap) {
				equipStatisticRecordDAO.deleteEquipStatisticUnit(psm);
			}
			if (pp.getType() != null && pp.getType().equals(EquipObjectType.EQUIP.getValue())) {
				EquipStatisticFilter filter = new EquipStatisticFilter();
				filter.setRecordId(recordId);
				filter.setShopId(shopId);
				List<EquipStatisticGroup> lstEntity = equipStatisticRecordDAO.getListEquipStatisticGroupByShop(filter);
				equipStatisticRecordDAO.deleteEquipStatisticGroup(lstEntity);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * them shop don vi tham gia kiem ke thiet bi
	 * @author trietptm
	 * @param recordId
	 * @param lstShopId
	 * @param lstUnit
	 * @param logInfo
	 * @throws BusinessException
	 * @since Mar 9, 2016
	 */
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void addShopForEquipStatistic(long recordId, List<Long> lstShopId, List<EquipStatisticUnit> lstUnit, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commDAO.getSysDate();
			for (EquipStatisticUnit psm : lstUnit) {
				psm.setCreateDate(currentDate);
				psm.setCreateUser(logInfo.getStaffCode());
				equipStatisticRecordDAO.createEquipStatisticUnit(psm, logInfo);
			}
			List<EquipStatisticUnit> listPromotionShopMap = equipStatisticRecordDAO.getListEquipStatisticUnitParentShop(lstShopId, recordId);
			for (EquipStatisticUnit psm : listPromotionShopMap) {
				equipStatisticRecordDAO.deleteEquipStatisticUnit(psm);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * check shop has child
	 * @author phut
	 */
	@Override
	public Boolean checkHashChild(Long recordId, Long shopId) throws BusinessException {
		try {
			List<EquipStatisticUnit> listPromotionShopMap = equipStatisticRecordDAO.getListEquipStatisticUnitChildShopMapWithShopAndRecord(shopId, recordId, true);
			if(listPromotionShopMap.isEmpty() || (listPromotionShopMap.size() == 1 && listPromotionShopMap.get(0).getId().equals(shopId))) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipStatisticRecord createEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord, LogInfoVO logInfo) throws BusinessException {
		try {
			Date sysdate = commDAO.getSysDate();
			equipStatisticRecord.setCreateDate(commDAO.getSysDate());
			equipStatisticRecord.setCreateUser(logInfo.getStaffCode());
			equipStatisticRecord = equipStatisticRecordDAO.createEquipStatisticRecord(equipStatisticRecord);
			// luu lich su
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(sysdate);
			equipFormHistory.setCreateDate(sysdate);
			equipFormHistory.setCreateUser(equipStatisticRecord.getCreateUser());
			equipFormHistory.setRecordId(equipStatisticRecord.getId());
			equipFormHistory.setRecordStatus(equipStatisticRecord.getRecordStatus());
			equipFormHistory.setRecordType(EquipTradeType.INVENTORY.getValue());
			Staff staff = staffDAO.getStaffByCode(equipStatisticRecord.getCreateUser());
			equipFormHistory.setStaff(staff);
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);

			return equipStatisticRecord;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipStatisticRecord getEquipStatisticRecordById(Long id) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getEquipStatisticRecordById(id);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<EquipStatisticRecord> getListEquipStatisticRecord(EquipRecordFilter<EquipStatisticRecord> filter) throws BusinessException {
		try {
			List<EquipStatisticRecord> lst = equipStatisticRecordDAO.getListEquipStatisticRecord(filter);
			ObjectVO<EquipStatisticRecord> objVO = new ObjectVO<EquipStatisticRecord>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createListEquipStatisticGroup(List<EquipStatisticGroup> list, LogInfoVO logInfo) throws BusinessException {
		try {
			Date currentDate = commDAO.getSysDate();
			for (EquipStatisticGroup obj : list) {
				obj.setCreateDate(currentDate);
				obj.setCreateUser(logInfo.getStaffCode());
			}
			equipStatisticRecordDAO.createListEquipStatisticGroup(list);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * delete group from record
	 * @author phut
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteEquipStatisticGroup(Long recordId, Long groupId, EquipObjectType type) throws BusinessException {
		try {
			EquipStatisticGroup entity = equipStatisticRecordDAO.getEquipStatisticGroupById(recordId, groupId, type);
			if (entity != null) {
				equipStatisticRecordDAO.deleteEquipStatisticGroup(entity);
			} else {
				throw new BusinessException("group is not found");
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteEquipStatisticGroup(EquipStatisticFilter filter) throws BusinessException {
		try {
			List<EquipStatisticGroup> lstEntity = equipStatisticRecordDAO.getListEquipStatisticGroup(filter);
			if (lstEntity != null && lstEntity.size() > 0) {
				equipStatisticRecordDAO.deleteEquipStatisticGroup(lstEntity);
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipLiquidationForm createLiquidationForm(EquipLiquidationForm equipLiquidationForm, List<EquipLiquidationFormDtl> lstLiquidationFormDtls, EquipRecordFilter<EquipmentLiquidationFormVO> filter) throws BusinessException {
		try {
			// luu bien ban
			Date sysDateIns = commDAO.getSysDate(); //phuongvm
			String code = equipliquidationFormDAO.getCodeEquipLiquidation();
			equipLiquidationForm.setEquipLiquidationFormCode(code);
			equipLiquidationForm.setCreateDate(sysDateIns);
			if (StatusRecordsEquip.APPROVED.getValue().equals(equipLiquidationForm.getStatus()) && equipLiquidationForm.getApproveDate() == null) {
				equipLiquidationForm.setApproveDate(sysDateIns);
			}
			equipLiquidationForm = equipliquidationFormDAO.createEquipLiquidationForm(equipLiquidationForm);

			// luu lich su bien ban
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(sysDateIns);
			equipFormHistory.setCreateDate(sysDateIns);
			equipFormHistory.setCreateUser(equipLiquidationForm.getCreateUser());
			equipFormHistory.setRecordId(equipLiquidationForm.getId());
			equipFormHistory.setRecordStatus(equipLiquidationForm.getStatus());
			equipFormHistory.setRecordType(EquipTradeType.LIQUIDATION.getValue());
			Staff staff = staffDAO.getStaffByCode(equipLiquidationForm.getCreateUser());
			equipFormHistory.setStaff(staff);
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);

			// luu chi tiet bien ban
			for (int i = 0, sz = lstLiquidationFormDtls.size(); i < sz ; i++) {
				EquipLiquidationFormDtl ed = lstLiquidationFormDtls.get(i);
				Equipment eq = ed.getEquip();

				// luu thay doi thiet bi trong chi tiet bien ban
				if (equipLiquidationForm.getStatus().equals(StatusRecordsEquip.DRAFT.getValue())) {
					/** Du thao: Kiem tra TB thanh ly hay bao mat*/
					if (!LinquidationStatus.WAITING_LIQUIDATION.getValue().equals(eq.getLinquidationStatus()) && !EquipTradeType.NOTICE_LOST.equals(eq.getTradeType())) {
						// dang o kho, dang su dung
						eq.setTradeType(EquipTradeType.LIQUIDATION);
						eq.setTradeStatus(EquipTradeStatus.TRADING.getValue());
						
						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(equipLiquidationForm.getCreateUser());
						equipHistory.setEquip(eq);
						equipHistory.setHealthStatus(eq.getHealthStatus());
						equipHistory.setObjectId(eq.getStockId());
						equipHistory.setObjectType(eq.getStockType());
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setFormId(equipLiquidationForm.getId());
						equipHistory.setFormType(EquipTradeType.LIQUIDATION);
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
						//equipHistory.setPriceActually(ed.getEquipValue());
						// luu gia tri thu hoi, gia tri thanh ly
						equipHistory.setLiquidationValue(ed.getLiquidationValue());
						equipHistory.setEvictionValue(ed.getEvictionValue());
						equipMngDAO.createEquipHistory(equipHistory);
					} else {
						// bao mat
						eq.setLinquidationStatus(LinquidationStatus.PROCESS_LIQUIDATION.getValue());
						
						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(equipLiquidationForm.getCreateUser());
						equipHistory.setEquip(eq);
						equipHistory.setHealthStatus(eq.getHealthStatus());
						equipHistory.setObjectId(eq.getStockId());
						equipHistory.setObjectType(eq.getStockType());
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setFormId(equipLiquidationForm.getId());
						equipHistory.setFormType(EquipTradeType.LIQUIDATION);
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
						//equipHistory.setPriceActually(ed.getEquipValue());
						// luu gia tri thu hoi, gia tri thanh ly
						equipHistory.setLiquidationValue(ed.getLiquidationValue());
						equipHistory.setEvictionValue(ed.getEvictionValue());
						equipHistory.setLiquidationStatus(eq.getLinquidationStatus()); // vuongmq; 15/07/2015; chi co thiet bi bao mat moi co truong nay luu lich su
						equipMngDAO.createEquipHistory(equipHistory);
					}
					eq.setUpdateDate(sysDateIns);
					eq.setUpdateUser(equipLiquidationForm.getCreateUser());
					equipMngDAO.updateEquipment(eq);

				} else if (equipLiquidationForm.getStatus().equals(StatusRecordsEquip.APPROVED.getValue())) {
					/** Duyet: Kiem tra TB thanh ly hay bao mat*/
					if (!LinquidationStatus.WAITING_LIQUIDATION.getValue().equals(eq.getLinquidationStatus()) && !EquipTradeType.NOTICE_LOST.equals(eq.getTradeType())) {
						// dang o kho, dang su dung
						eq.setTradeType(EquipTradeType.LIQUIDATION);
						eq.setTradeStatus(EquipTradeStatus.TRADING.getValue());
						
						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(equipLiquidationForm.getCreateUser());
						equipHistory.setEquip(eq);
						equipHistory.setHealthStatus(eq.getHealthStatus());
						equipHistory.setObjectId(eq.getStockId());
						equipHistory.setObjectType(eq.getStockType());
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setFormId(equipLiquidationForm.getId());
						equipHistory.setFormType(EquipTradeType.LIQUIDATION);
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
						//equipHistory.setPriceActually(ed.getEquipValue());
						// luu gia tri thu hoi, gia tri thanh ly
						equipHistory.setLiquidationValue(ed.getLiquidationValue());
						equipHistory.setEvictionValue(ed.getEvictionValue());
						equipMngDAO.createEquipHistory(equipHistory);
					} else {
						// bao mat
						eq.setLinquidationStatus(LinquidationStatus.PROCESS_LIQUIDATION.getValue());
						
						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(equipLiquidationForm.getCreateUser());
						equipHistory.setEquip(eq);
						equipHistory.setHealthStatus(eq.getHealthStatus());
						equipHistory.setObjectId(eq.getStockId());
						equipHistory.setObjectType(eq.getStockType());
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setFormId(equipLiquidationForm.getId());
						equipHistory.setFormType(EquipTradeType.LIQUIDATION);
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
						//equipHistory.setPriceActually(ed.getEquipValue());
						// luu gia tri thu hoi, gia tri thanh ly
						equipHistory.setLiquidationValue(ed.getLiquidationValue());
						equipHistory.setEvictionValue(ed.getEvictionValue());
						equipHistory.setLiquidationStatus(eq.getLinquidationStatus()); // vuongmq; 15/07/2015; chi co thiet bi bao mat moi co truong nay luu lich su
						equipMngDAO.createEquipHistory(equipHistory);
					}
					eq.setUpdateDate(sysDateIns);
					eq.setUpdateUser(equipLiquidationForm.getCreateUser());
					equipMngDAO.updateEquipment(eq);
				} else if (equipLiquidationForm.getStatus().equals(StatusRecordsEquip.LIQUIDATED.getValue())) {
					/** Da thanh ly: Kiem tra TB thanh ly hay bao mat*/
					// giam kho NPP, KH
					EquipStockTotal equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.parseValue(eq.getStockType().getValue()), eq.getEquipGroup().getId());
					if (equipStockTotal != null) {
						if (equipStockTotal.getQuantity() > 0) {
							equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
						} else {
							equipStockTotal.setQuantity(0);
						}
						equipStockTotal.setUpdateDate(sysDateIns);
						equipStockTotal.setUpdateUser(equipLiquidationForm.getCreateUser());
						equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
					}
					// giam kho tong
					EquipStockEquipFilter<EquipStockTotal> filterSt = new EquipStockEquipFilter<EquipStockTotal>();
					filterSt.setEquipGroupId(eq.getEquipGroup().getId());
					filterSt.setStockId(null);
					filterSt.setStockType(EquipStockTotalType.KHO_TONG.getValue()); // set lai loai kho la kho tong: 0; van nhom do, stockId null
					EquipStockTotal equipStockTotalTong = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
					//EquipStockTotal equipStockTotalTong = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.KHO_TONG, eq.getEquipGroup().getId());
					if (equipStockTotalTong != null) {
						if (equipStockTotalTong.getQuantity() > 0) {
							equipStockTotalTong.setQuantity(equipStockTotalTong.getQuantity() - 1);
						} else {
							equipStockTotalTong.setQuantity(0);
						}
						equipStockTotalTong.setUpdateDate(sysDateIns);
						equipStockTotalTong.setUpdateUser(equipLiquidationForm.getCreateUser());
						equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotalTong);
					}

					// cap nhat thiet bi
					// eq.setStockCode(null);
					// eq.setStockId(null);
					// eq.setStockType(null);
					if (!LinquidationStatus.WAITING_LIQUIDATION.getValue().equals(eq.getLinquidationStatus()) && !EquipTradeType.NOTICE_LOST.equals(eq.getTradeType()) && !EquipTradeType.NOTICE_LOSS_MOBILE.equals(eq.getTradeType())) {
						// dang o kho, dang su dung
						eq.setUsageStatus(EquipUsageStatus.LIQUIDATED);
						eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						eq.setTradeType(null);
						
						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(equipLiquidationForm.getCreateUser());
						equipHistory.setEquip(eq);
						equipHistory.setHealthStatus(eq.getHealthStatus());
						equipHistory.setObjectId(eq.getStockId());
						equipHistory.setObjectType(eq.getStockType());
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setFormId(equipLiquidationForm.getId());
						equipHistory.setFormType(EquipTradeType.LIQUIDATION);
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
						//equipHistory.setPriceActually(ed.getEquipValue());
						// luu gia tri thu hoi, gia tri thanh ly
						equipHistory.setLiquidationValue(ed.getLiquidationValue());
						equipHistory.setEvictionValue(ed.getEvictionValue());
						equipMngDAO.createEquipHistory(equipHistory);
					} else {
						// eq.setUsageStatus(EquipUsageStatus.LIQUIDATED); // cap nhat la thiet bi da thanh ly; vuongmq; 28/07/2015; chi cap nhat trang thai thiet bi thoi
						// thanh ly thiet bi Chi dung bao mat: trang thai thanh ly la: da thanh ly
						eq.setLinquidationStatus(LinquidationStatus.FINISH_LIQUIDATION.getValue());
						
						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(equipLiquidationForm.getCreateUser());
						equipHistory.setEquip(eq);
						equipHistory.setHealthStatus(eq.getHealthStatus());
						equipHistory.setObjectId(eq.getStockId());
						equipHistory.setObjectType(eq.getStockType());
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setFormId(equipLiquidationForm.getId());
						equipHistory.setFormType(EquipTradeType.LIQUIDATION);
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
						//equipHistory.setPriceActually(ed.getEquipValue());
						// luu gia tri thu hoi, gia tri thanh ly
						equipHistory.setLiquidationValue(ed.getLiquidationValue());
						equipHistory.setEvictionValue(ed.getEvictionValue());
						equipHistory.setLiquidationStatus(eq.getLinquidationStatus()); // vuongmq; 15/07/2015; chi co thiet bi bao mat moi co truong nay luu lich su
						equipMngDAO.createEquipHistory(equipHistory);
					}
					eq.setUpdateDate(sysDateIns);
					eq.setUpdateUser(equipLiquidationForm.getCreateUser());
					eq.setLiquidationDate(sysDateIns);
					equipMngDAO.updateEquipment(eq);
				}
				
				// luu chi tiet bien ban
				ed.setEquipLiquidationForm(equipLiquidationForm);
				ed.setCreateDate(sysDateIns);
				ed.setStockId(eq.getStockId());
				ed.setStockType(eq.getStockType()); //vuongmq; 14/07/2015; dac ta cap nhat; 1. kho DonVi, 2. KH,
				ed = equipliquidationFormDAO.createEquipLiquidationFormDetail(ed);
			}
			//Xu ly them file
			if (filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
				for (FileVO voFile : filter.getLstFileVo()) {
					EquipAttachFile equipAttFile = new EquipAttachFile();
					equipAttFile.setUrl(voFile.getUrl());
					equipAttFile.setObjectId(equipLiquidationForm.getId());
					equipAttFile.setFileName(voFile.getFileName());
					equipAttFile.setThumbUrl(voFile.getThumbUrl());
					equipAttFile.setObjectType(EquipTradeType.LIQUIDATION.getValue());
					equipAttFile.setCreateDate(sysDateIns);
					equipAttFile.setCreateUser(equipLiquidationForm.getCreateUser());
					equipRecordDAO.createEquipAttachFile(equipAttFile);
				}
			}
			return equipLiquidationForm;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * cap nhat danh sach cac trang thai cua bao mat
	 * changeByListEquipLoadRecord
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void changeByListEquipLostRecord(EquipRecordFilter<EquipLostRecord> filter) throws BusinessException {
		try {
			/*Shop shopObj = shopDAO.getShopByIdChannelIslevel(null, null, ShopDecentralizationSTT.VNM.getValue());
			if (shopObj == null) {
				throw new BusinessException("Shop Is level VNM COM undefined");
			}
			ShopVO shopVo = new ShopVO();
			shopVo = shopVo.coppyByEntites(shopObj);
			filter.setShopVO(shopVo);*/
			/** Cac bien mac dinh khoi tao ban dau */
			Date sysDateIns = commDAO.getSysDate();
			filter.setChangeDate(sysDateIns);
			List<EquipLostRecord> lstLostRecord = new ArrayList<EquipLostRecord>();
			if (filter.getLstId() != null && !filter.getLstId().isEmpty()) {
				EquipRecordFilter<EquipLostRecord> filterLR = new EquipRecordFilter<EquipLostRecord>();
				filterLR.setLstId(filter.getLstId());
				filterLR.setStockType(filter.getStockType()); // truong hop nay bao mat tu mobile co stock type
				lstLostRecord = equipRecordDAO.searchEquipLostRecordByFilter(filterLR);
			}
			if (lstLostRecord == null || lstLostRecord.isEmpty()) {
				if (filter.getAttribute() == null) {
					throw new BusinessException("lstLostRecord is null");
				} else {
					lstLostRecord = new ArrayList<EquipLostRecord>();
					lstLostRecord.add(filter.getAttribute());
				}
			}
			for (EquipLostRecord lostRecord : lstLostRecord) {
				EquipLostRecord equipLostRecOld = lostRecord.clone();
				//filter.setStockId(shopObj.getId());
				//filter.setStockCode(shopObj.getShopCode());
				if (filter.getRecordStatus() != null && !filter.getRecordStatus().equals(lostRecord.getRecordStatus().getValue())) {
					//lostRecord.setRecordStatus(filter.getRecordStatus());
					lostRecord.setRecordStatus(StatusRecordsEquip.parseValue(filter.getRecordStatus()));
				}
				if (filter.getDeliveryStatus() != null && !filter.getDeliveryStatus().equals(lostRecord.getDeliveryStatus().getValue())) {
					//lostRecord.setDeliveryStatus(filter.getDeliveryStatus());
					lostRecord.setDeliveryStatus(DeliveryType.parseValue(filter.getDeliveryStatus()));
				}
				lostRecord.setUpdateDate(filter.getChangeDate());
				lostRecord.setUpdateUser(filter.getUserName());
				filter.setAttribute(lostRecord);
				this.changeEquipLostRecord(filter, equipLostRecOld);
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public void createEquipFormHistoryByEquipLostRec(EquipLostRecord equipLostRec, String createUserName, Integer recordType) throws BusinessException {
		try {
			Date sysDate = commDAO.getSysDate();
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setCreateUser(createUserName);
			//equipFormHistory.setDeliveryStatus(equipLostRec.getDeliveryStatus());
			equipFormHistory.setDeliveryStatus(equipLostRec.getDeliveryStatus().getValue());
			equipFormHistory.setRecordId(equipLostRec.getId());
			//equipFormHistory.setRecordStatus(equipLostRec.getRecordStatus());
			equipFormHistory.setRecordStatus(equipLostRec.getRecordStatus().getValue());
			equipFormHistory.setRecordType(recordType);
			equipFormHistory.setActDate(sysDate);
			equipFormHistory.setCreateDate(sysDate);
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * Thuc hien ham chung cho mot bien ban bao mat
	 * 
	 * @author hunglm16
	 * 
	 * @author vuongmq
	 * @date 07/05/2015
	 * cap nhat phieu bao mat;
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void changeEquipLostRecord(EquipRecordFilter<EquipLostRecord> filter, EquipLostRecord equipLostRecOld) throws BusinessException {
		try {
			EquipLostRecord equipLostRec = filter.getAttribute().clone();
			if (filter.getDeliveryStatus() != null) {
				//equipLostRec.setDeliveryStatus(filter.getDeliveryStatus());
				equipLostRec.setDeliveryStatus(DeliveryType.parseValue(filter.getDeliveryStatus()));
			}
			if (filter.getChangeDate() == null) {
				filter.setChangeDate(commDAO.getSysDate());
			}
			Equipment equipment = equipLostRec.getEquip().clone();
			/**
			 * begin vuongmq; 26/05/2015; cho nay cap nhat gia tri con lai cho
			 * equipment
			 */

			/**
			 * begin vuongmq; 08/05/2015; cho nay cap nhat nguyen gia cho
			 * equipment v?� equipLostRec
			 */
			/*
			 * if (filter.getMapPriceActually() != null &&
			 * !filter.getMapPriceActually().isEmpty()) { BigDecimal priceActual
			 * = BigDecimal.ZERO; Long pActually =
			 * filter.getMapPriceActually().get(equipment.getId()); if
			 * (pActually != null) { priceActual =
			 * BigDecimal.valueOf(pActually); }
			 * filter.setPriceActually(priceActual);
			 * equipLostRec.setPriceActually(priceActual);
			 * equipment.setPriceActually(priceActual);
			 * equipLostRec.setEquip(equipment);
			 * 
			 * equipment.setUpdateUser(filter.getUserName());
			 * equipment.setUpdateDate(filter.getChangeDate());
			 * equipMngDAO.updateEquipment(equipment); }
			 */
			/**
			 * end vuongmq; 08/05/2015; cho nay cap nhat nguyen gia cho
			 * equipment v?� equipLostRec
			 */
			//Cap nhat thiet bi neu co
			if (equipLostRecOld.getEquip() == null || (equipLostRecOld.getEquip() != null && !equipment.getId().equals(equipLostRecOld.getEquip().getId()))) {
				/**
				 * Thuc hien luong them moi thiet bi chi bien ban Xu ly cho tat
				 * ca cac luong lien quan
				 */
				//equipLostRec.setRecordStatus(filter.getRecordStatus());
				equipLostRec.setDeliveryStatus(DeliveryType.parseValue(filter.getDeliveryStatus()));
				/** Cap nhat lai trang thai thiet bi moi thay the */
				//equipment.setUsageStatus(EquipUsageStatus.SHOWING_WAREHOUSE); // khong cap nhat EquipUsageStatus tru truong hop duyet
				Equipment equipmentOld = equipLostRecOld.getEquip().clone();
				//equipmentOld.setUsageStatus(EquipUsageStatus.IS_USED);
				equipmentOld.setTradeType(null);
				equipmentOld.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
				equipmentOld.setUpdateUser(filter.getUserName());
				equipmentOld.setUpdateDate(filter.getChangeDate());
				equipMngDAO.updateEquipment(equipmentOld);

				/***
				 * begin vuongmq; 20/05/2015; cap nhat them lich sua cho thiet
				 * bi
				 */
				/** Them lich su Thiet bị */
				EquipHistory equipHistoryOld = new EquipHistory();
				equipHistoryOld.setCreateDate(filter.getChangeDate());
				equipHistoryOld.setCreateUser(filter.getUserName());
				equipHistoryOld.setEquip(equipmentOld);
				equipHistoryOld.setStatus(ActiveType.RUNNING.getValue());
				equipHistoryOld.setHealthStatus(equipmentOld.getHealthStatus());
				equipHistoryOld.setUsageStatus(equipmentOld.getUsageStatus());
				equipHistoryOld.setTradeStatus(equipmentOld.getTradeStatus());
				equipHistoryOld.setTradeType(equipmentOld.getTradeType());
				equipHistoryOld.setObjectType(equipmentOld.getStockType());
				equipHistoryOld.setObjectId(equipmentOld.getStockId());
				equipHistoryOld.setObjectCode(equipmentOld.getStockCode());
				equipHistoryOld.setFormId(equipLostRecOld.getId());
				equipHistoryOld.setFormType(EquipTradeType.NOTICE_LOST); // bao mat
				equipHistoryOld.setStockName(equipmentOld.getStockName());
				equipHistoryOld.setTableName(TableName.EQUIP_LOST_RECORD.getValue());
				equipHistoryOld.setPriceActually(equipmentOld.getPriceActually());

				equipMngDAO.createEquipHistory(equipHistoryOld);
				/*** end vuongmq; 20/05/2015; cap nhat them lich sua cho thiet bi */
				/** Them lich su Thiet bị */
				/*
				 * equipmentManagerMgr.createEquipHistoryByEquipment(equipmentOld
				 * , filter.getUserName());
				 */
				//Them moi truong Equip thay doi moi
				//equipment.setUsageStatus(EquipUsageStatus.IS_USED);
				equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
				equipment.setTradeType(EquipTradeType.NOTICE_LOST);
				equipment.setUpdateUser(filter.getUserName());
				equipment.setUpdateDate(filter.getChangeDate());
				equipMngDAO.updateEquipment(equipment);
				/***
				 * begin vuongmq; 20/05/2015; cap nhat them lich sua cho thiet
				 * bi
				 */
				/** Them lich su Thiet bị */
				EquipHistory equipHistory = new EquipHistory();
				equipHistory.setCreateDate(filter.getChangeDate());
				equipHistory.setCreateUser(filter.getUserName());
				equipHistory.setEquip(equipment);
				equipHistory.setStatus(ActiveType.RUNNING.getValue());
				equipHistory.setHealthStatus(equipment.getHealthStatus());
				equipHistory.setUsageStatus(equipment.getUsageStatus());
				equipHistory.setTradeStatus(equipment.getTradeStatus());
				equipHistory.setTradeType(equipment.getTradeType());
				equipHistory.setObjectType(equipment.getStockType());
				equipHistory.setObjectId(equipment.getStockId());
				equipHistory.setObjectCode(equipment.getStockCode());
				equipHistory.setFormId(equipLostRec.getId());
				equipHistory.setFormType(EquipTradeType.NOTICE_LOST); // bao mat
				equipHistory.setStockName(equipment.getStockName());
				equipHistory.setTableName(TableName.EQUIP_LOST_RECORD.getValue());
				equipHistory.setPriceActually(equipment.getPriceActually());

				equipMngDAO.createEquipHistory(equipHistory);
				/*** end vuongmq; 20/05/2015; cap nhat them lich sua cho thiet bi */
				/** Them lich su Thiet bị */
				/*
				 * equipmentManagerMgr.createEquipHistoryByEquipment(equipment,
				 * filter.getUserName());
				 */
			}
			//Xu ly tat ca cac luong lien quan; cap nhat trang thai bien ban
			if (!equipLostRec.getRecordStatus().equals(equipLostRecOld.getRecordStatus())) {
				if (filter.getRecordStatus() != null) {
					equipLostRec.setRecordStatus(StatusRecordsEquip.parseValue(filter.getRecordStatus()));
				}
				if (StatusRecordsEquip.APPROVED.equals(equipLostRec.getRecordStatus())) {
					/** Xet truong hop da duyet */
					equipment.setLinquidationStatus(LinquidationStatus.FINISH_LIQUIDATION.getValue());//Cho thanh ly
					/**
					 * vuongmq; 20/05/2015 cap nhat them ngay duyet; va bien ban
					 * giao nhan moi nhat
					 */
					equipLostRec.setApproveDate(filter.getChangeDate());
					EquipmentFilter<EquipDeliveryRecord> filterDelivery = new EquipmentFilter<EquipDeliveryRecord>();
					if (equipLostRec.getEquip() != null) {
						filterDelivery.setEquipId(equipLostRec.getEquip().getId());
					}
					EquipDeliveryRecord eqDeliveryRecord = equipMngDAO.getEquipDeliveryRecordNewOfEquipmentByFilter(filterDelivery);
					equipLostRec.setEquipDeliveryRecord(eqDeliveryRecord);
					//Giam tru kho
					/***
					 * vuongmq; 08/05/2015; cap nhat EquipStockTotal khi bao mat
					 * 1. tru ton kho cua loai Thiet bi 1: NPP or 2: KH, va tru
					 * ton kho chung: 0 2. ben duoi - Tu dong chon 1 kho dang
					 * hoat dong, thuoc kho cap tren truc tiep cua npp cua kh
					 * hoac npp bi mat. (cho vao kho thu tu 1) - Neu cap tren
					 * truc tiep khong co kho thi chon nhung kho ngang cap cua
					 * cap tren. (cho vao kho thu tu 1) 3. cap nhat Thiet bi lai
					 * kho moi, la kho cua mien cap tren da lay duoc
					 */
					if (equipment != null && equipment.getEquipGroup() != null) {
						/*** buoc 1 va 2 tru kho truoc; ung voi thiet bi do */
						/*
						EquipStockEquipFilter<EquipStockTotal> filterSt = new EquipStockEquipFilter<EquipStockTotal>();
						filterSt.setEquipGroupId(equipment.getEquipGroup().getId());
						filterSt.setStockId(equipment.getStockId()); //them stock_ID; cho du du lieu nhieu van OK
						filterSt.setStockType(equipment.getStockType().getValue());
						EquipStockTotal equipStockTotal = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
						*/
						/**
						 * vuongmq; 11/05/2015; giam tru ton kho thiet bi cho
						 * tung loai; 1: NPP or 2: KH tuy theo loai Thiet bi
						 */
						/*
						if (equipStockTotal != null) {
							//Cap nhat kho
							if (equipStockTotal.getQuantity() != null && equipStockTotal.getQuantity() > 0) {
								equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
							} else {
								equipStockTotal.setQuantity(0);
							}
							//equipStockTotal.setStockType(EquipStockTotalType.KHO_KH);
							equipStockTotal.setUpdateDate(filter.getChangeDate());
							equipStockTotal.setUpdateUser(filter.getUserName());
							equipMngDAO.updateEquipStockTotal(equipStockTotal);
						}
						*/
						// kho tong khong can stock_id
						/*
						filterSt.setStockId(null);
						filterSt.setStockType(EquipStockTotalType.KHO_TONG.getValue()); // set lai loai kho la kho tong: 0; van nhom do, stockId do
						EquipStockTotal equipStockTotalKHO_TONG = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
						*/
						/**
						 * vuongmq; 11/05/2015; giam tru ton kho tong cua Thiet
						 * bi: 0
						 */
						/*
						if (equipStockTotalKHO_TONG != null) {
							//Cap nhat kho
							if (equipStockTotalKHO_TONG.getQuantity() != null && equipStockTotalKHO_TONG.getQuantity() > 0) {
								equipStockTotalKHO_TONG.setQuantity(equipStockTotalKHO_TONG.getQuantity() - 1);
							} else {
								equipStockTotalKHO_TONG.setQuantity(0);
							}
							equipStockTotalKHO_TONG.setUpdateDate(filter.getChangeDate());
							equipStockTotalKHO_TONG.setUpdateUser(filter.getUserName());
							equipMngDAO.updateEquipStockTotal(equipStockTotalKHO_TONG);
						}
						*/
						/** B3: cap nhat thiet bi ve kho cua Mien */
						EquipStockEquipFilter<EquipStock> filterB12 = new EquipStockEquipFilter<EquipStock>();
						filterB12.setStockId(equipment.getStockId());
						filterB12.setStockType(equipment.getStockType().getValue());
//						List<EquipStock> lsEquipStockParent = equipMngDAO.getListEquipStockParentByFilter(filterB12);
						
						/** -- 11/04/2016
						 * Không cần bắt buộc chuyển thiết bị báo mất về kho miền vì bản chất thiết bị lúc này đã mất
						 * -> không xác định được thiết bị ở đâu 
						 * Chỉ cần cập nhật lại trạng thái thiết bị thành báo mất là được
						Boolean flagKhoMien = false; // co nay de cap nhat
						if (lsEquipStockParent != null && lsEquipStockParent.size() > 0) {
							EquipStock equipStockParent = lsEquipStockParent.get(0);
							equipment.setStockId(equipStockParent.getId());
							equipment.setStockCode(equipStockParent.getCode());
							equipment.setStockName(equipStockParent.getName());
							//equipment.setStockType(EquipStockTotalType.KHO); // cap nhat lai kho NPP la mien
							flagKhoMien = true;
						} else {
							filterB12 = new EquipStockEquipFilter<EquipStock>();
							List<EquipStock> lsEquipStockParentSameLevel = equipMngDAO.getListEquipStockParentSameLevelByFilter(filterB12);
							if (lsEquipStockParentSameLevel != null && lsEquipStockParentSameLevel.size() > 0) {
								EquipStock equipStockParentSamelevel = lsEquipStockParentSameLevel.get(0);
								equipment.setStockId(equipStockParentSamelevel.getId());
								equipment.setStockCode(equipStockParentSamelevel.getCode());
								equipment.setStockName(equipStockParentSamelevel.getName());
								//equipment.setStockType(EquipStockTotalType.KHO); // cap nhat lai kho NPP la mien
								flagKhoMien = true;
							}
						}
						if (flagKhoMien) {
							equipment.setStockType(EquipStockTotalType.KHO); // cap nhat lai kho NPP la mien
							*/
							equipment.setUpdateDate(filter.getChangeDate());
							equipment.setUpdateUser(filter.getUserName());
							equipment.setLostDate(equipLostRec.getLostDate()); // vuongmq; 20 cap nhat ngay mat cua thiet bi;
							/**
							 * begin vuongmq; 26/05/2015; cho nay cap nhat gia
							 * tri con lai cho equipment
							 */
							equipment.setPriceActually(equipLostRec.getPriceActually());

							equipment.setUsageStatus(EquipUsageStatus.LOST);
							equipment.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
							equipment.setTradeType(null);
							equipMngDAO.updateEquipment(equipment);

							/***
							 * begin vuongmq; 20/05/2015; cap nhat them lich sua
							 * cho thiet bi
							 */
							/** Them lich su Thiet bị */
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(filter.getChangeDate());
							equipHistory.setCreateUser(filter.getUserName());
							equipHistory.setEquip(equipment);
							equipHistory.setStatus(ActiveType.RUNNING.getValue());
							equipHistory.setHealthStatus(equipment.getHealthStatus());
							equipHistory.setUsageStatus(equipment.getUsageStatus());
							equipHistory.setTradeStatus(equipment.getTradeStatus());
							equipHistory.setTradeType(equipment.getTradeType());
							equipHistory.setObjectType(equipment.getStockType());
							equipHistory.setObjectId(equipment.getStockId());

							equipHistory.setObjectCode(equipment.getStockCode());
							equipHistory.setFormId(equipLostRec.getId());
							equipHistory.setFormType(EquipTradeType.NOTICE_LOST); // bao mat
							equipHistory.setStockName(equipment.getStockName());
							equipHistory.setTableName(TableName.EQUIP_LOST_RECORD.getValue());
							equipHistory.setPriceActually(equipment.getPriceActually());

							equipMngDAO.createEquipHistory(equipHistory);
							/***
							 * end vuongmq; 20/05/2015; cap nhat them lich sua
							 * cho thiet bi
							 */
							/*
							 * // Them lich su Thiet bị
							 * equipmentManagerMgr.createEquipHistoryByEquipment
							 * (equipment, filter.getUserName());
							 */
//						}
					}
					//Them lich su bien ban
					this.createEquipFormHistoryByEquipLostRec(equipLostRec, filter.getUserName(), EquipTradeType.NOTICE_LOST.getValue());
				} else if (StatusRecordsEquip.WAITING_APPROVAL.equals(equipLostRec.getRecordStatus())) {
					/** xet truong hop Cho Duyet */
					//equipLostRec.setRecordStatus(StatusRecordsEquip.WAITING_APPROVAL.getValue());
					equipLostRec.setRecordStatus(StatusRecordsEquip.WAITING_APPROVAL);
					//[-- Bo sung cho truong hop Du thao chuyen sang Cho duyet--]
					equipment.setLinquidationStatus(LinquidationStatus.WAITING_LIQUIDATION.getValue());//Cho thanh ly
					if (!StatusRecordsEquip.DRAFT.equals(equipLostRecOld.getRecordStatus())) {
						//[-- Truong hop Khong duyet chuyen sang Cho duyet--]
						equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
						//Them trang thai thanh ly tai san
						// Them moi lich su thiet bi
						/** Cap nhat cho phan luu lich su */
						//equipment.setUsageStatus(EquipUsageStatus.IS_USED);
						equipment.setTradeType(EquipTradeType.NOTICE_LOST);
					}
					equipment.setUpdateUser(filter.getUserName());
					equipment.setUpdateDate(filter.getChangeDate());
					equipMngDAO.updateEquipment(equipment);
					/** Them lich su Thiet bị */
					EquipHistory equipHistory = new EquipHistory();
					equipHistory.setCreateDate(filter.getChangeDate());
					equipHistory.setCreateUser(filter.getUserName());
					equipHistory.setEquip(equipment);
					equipHistory.setStatus(ActiveType.RUNNING.getValue());
					equipHistory.setHealthStatus(equipment.getHealthStatus());
					equipHistory.setUsageStatus(equipment.getUsageStatus());
					equipHistory.setTradeStatus(equipment.getTradeStatus());
					equipHistory.setTradeType(equipment.getTradeType());
					equipHistory.setObjectType(equipment.getStockType());
					equipHistory.setObjectId(equipment.getStockId());

					equipHistory.setObjectCode(equipment.getStockCode());
					equipHistory.setFormId(equipLostRec.getId());
					equipHistory.setFormType(EquipTradeType.NOTICE_LOST); // bao mat
					equipHistory.setStockName(equipment.getStockName());
					equipHistory.setTableName(TableName.EQUIP_LOST_RECORD.getValue());
					equipHistory.setPriceActually(equipment.getPriceActually());

					equipMngDAO.createEquipHistory(equipHistory);
					/***
					 * end vuongmq; 20/05/2015; cap nhat them lich sua cho thiet
					 * bi
					 */
					/*
					 * // Them lich su Thiet bị
					 * equipmentManagerMgr.createEquipHistoryByEquipment
					 * (equipment, filter.getUserName());
					 */
					//Them lich su bien ban
					this.createEquipFormHistoryByEquipLostRec(equipLostRec, filter.getUserName(), EquipTradeType.NOTICE_LOST.getValue());
				} else if (StatusRecordsEquip.NO_APPROVAL.equals(equipLostRec.getRecordStatus())) {
					/** xet truong hop Khong Duyet */
					equipLostRec.setRecordStatus(StatusRecordsEquip.NO_APPROVAL);
					equipment.setLinquidationStatus(null);//Khong thanh ly
					equipment.setTradeType(null);
					equipment.setTradeStatus(StatusType.INACTIVE.getValue());
					equipment.setUpdateUser(filter.getUserName());
					/** Cap nhat cho phan luu lich su */
					equipment.setUpdateDate(filter.getChangeDate());
					equipMngDAO.updateEquipment(equipment);

					/***
					 * begin vuongmq; 20/05/2015; cap nhat them lich sua cho
					 * thiet bi
					 */
					/** Them lich su Thiet bị */
					EquipHistory equipHistory = new EquipHistory();
					equipHistory.setCreateDate(filter.getChangeDate());
					equipHistory.setCreateUser(filter.getUserName());
					equipHistory.setEquip(equipment);
					equipHistory.setStatus(ActiveType.RUNNING.getValue());
					equipHistory.setHealthStatus(equipment.getHealthStatus());
					equipHistory.setUsageStatus(equipment.getUsageStatus());
					equipHistory.setTradeStatus(equipment.getTradeStatus());
					equipHistory.setTradeType(equipment.getTradeType());
					equipHistory.setObjectType(equipment.getStockType());
					equipHistory.setObjectId(equipment.getStockId());

					equipHistory.setObjectCode(equipment.getStockCode());
					equipHistory.setFormId(equipLostRec.getId());
					equipHistory.setFormType(EquipTradeType.NOTICE_LOST); // bao mat
					equipHistory.setStockName(equipment.getStockName());
					equipHistory.setTableName(TableName.EQUIP_LOST_RECORD.getValue());
					equipHistory.setPriceActually(equipment.getPriceActually());

					equipMngDAO.createEquipHistory(equipHistory);
					/***
					 * end vuongmq; 20/05/2015; cap nhat them lich sua cho thiet
					 * bi
					 */
					/*
					 * // Them lich su Thiet bị
					 * equipmentManagerMgr.createEquipHistoryByEquipment
					 * (equipment, filter.getUserName());
					 */
					//Them lich su bien ban
					this.createEquipFormHistoryByEquipLostRec(equipLostRec, filter.getUserName(), EquipTradeType.NOTICE_LOST.getValue());
				} else if (StatusRecordsEquip.CANCELLATION.equals(equipLostRec.getRecordStatus())) {
					if (StatusRecordsEquip.DRAFT.equals(equipLostRecOld.getRecordStatus()) || StatusRecordsEquip.WAITING_APPROVAL.equals(equipLostRecOld.getRecordStatus())) {
						/** xet truong hop: du thao, cho duyet -> huy */
						//equipLostRec.setRecordStatus(StatusRecordsEquip.NO_APPROVAL);
						equipment.setLinquidationStatus(null);//Cho thanh ly
						equipment.setTradeType(null);
						equipment.setTradeStatus(StatusType.INACTIVE.getValue());
						equipment.setUpdateUser(filter.getUserName());
						/** Cap nhat cho phan luu lich su */
						equipment.setUpdateDate(filter.getChangeDate());
						equipMngDAO.updateEquipment(equipment);

						/***
						 * begin vuongmq; 20/05/2015; cap nhat them lich sua cho
						 * thiet bi
						 */
						/** Them lich su Thiet bị */
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(filter.getChangeDate());
						equipHistory.setCreateUser(filter.getUserName());
						equipHistory.setEquip(equipment);
						equipHistory.setStatus(ActiveType.RUNNING.getValue());
						equipHistory.setHealthStatus(equipment.getHealthStatus());
						equipHistory.setUsageStatus(equipment.getUsageStatus());
						equipHistory.setTradeStatus(equipment.getTradeStatus());
						equipHistory.setTradeType(equipment.getTradeType());
						equipHistory.setObjectType(equipment.getStockType());
						equipHistory.setObjectId(equipment.getStockId());

						equipHistory.setObjectCode(equipment.getStockCode());
						equipHistory.setFormId(equipLostRec.getId());
						equipHistory.setFormType(EquipTradeType.NOTICE_LOST); // bao mat
						equipHistory.setStockName(equipment.getStockName());
						equipHistory.setTableName(TableName.EQUIP_LOST_RECORD.getValue());
						equipHistory.setPriceActually(equipment.getPriceActually());

						equipMngDAO.createEquipHistory(equipHistory);
						/***
						 * end vuongmq; 20/05/2015; cap nhat them lich sua cho
						 * thiet bi
						 */
						/*
						 * // Them lich su Thiet bị
						 * equipmentManagerMgr.createEquipHistoryByEquipment
						 * (equipment, filter.getUserName());
						 */
					}
					//equipLostRec.setRecordStatus(StatusRecordsEquip.CANCELLATION.getValue());
					/** xet truong hop: khong duyet -> huy */
					equipLostRec.setRecordStatus(StatusRecordsEquip.CANCELLATION);
					//Them lich su bien ban
					this.createEquipFormHistoryByEquipLostRec(equipLostRec, filter.getUserName(), EquipTradeType.NOTICE_LOST.getValue());
				} else {
					//Them lich su bien ban
					this.createEquipFormHistoryByEquipLostRec(equipLostRec, filter.getUserName(), EquipTradeType.NOTICE_LOST.getValue());
				}
			} else {
				/**
				 * Truong hop la thay doi gia tri priceActually; khong thay doi
				 * thiet bi
				 */
				if (equipment.getPriceActually() != null && equipLostRecOld.getPriceActually() != null && !equipLostRecOld.getPriceActually().equals(equipment.getPriceActually())) {
					/***
					 * begin vuongmq; 20/05/2015; cap nhat them lich sua cho
					 * thiet bi
					 */
					/** Them lich su Thiet bị */
					EquipHistory equipHistory = new EquipHistory();
					equipHistory.setCreateDate(filter.getChangeDate());
					equipHistory.setCreateUser(filter.getUserName());
					equipHistory.setEquip(equipment);
					equipHistory.setStatus(ActiveType.RUNNING.getValue());
					equipHistory.setHealthStatus(equipment.getHealthStatus());
					equipHistory.setUsageStatus(equipment.getUsageStatus());
					equipHistory.setTradeStatus(equipment.getTradeStatus());
					equipHistory.setTradeType(equipment.getTradeType());
					equipHistory.setObjectType(equipment.getStockType());
					equipHistory.setObjectId(equipment.getStockId());

					equipHistory.setObjectCode(equipment.getStockCode());
					equipHistory.setFormId(equipLostRec.getId());
					equipHistory.setFormType(EquipTradeType.NOTICE_LOST); // bao mat
					equipHistory.setStockName(equipment.getStockName());
					equipHistory.setTableName(TableName.EQUIP_LOST_RECORD.getValue());
					equipHistory.setPriceActually(equipment.getPriceActually());

					equipMngDAO.createEquipHistory(equipHistory);
					/***
					 * end vuongmq; 20/05/2015; cap nhat them lich sua cho thiet
					 * bi
					 */
				}
				/**
				 * vuongmq; 18/05/2015 truong hop nay: cap nhat trang thai giao
				 * nhan; va cap nhat lich su
				 */
				this.createEquipFormHistoryByEquipLostRec(equipLostRec, filter.getUserName(), EquipTradeType.NOTICE_LOST.getValue());
			}

			//Xu ly cap nhat bien ban thiet bi
			equipRecordDAO.updateEquipLostRecord(equipLostRec);

			//Xu ly xoa file
			if (filter.getLstEquipAttachFileId() != null && !filter.getLstEquipAttachFileId().isEmpty()) {
				BasicFilter<EquipAttachFile> filterB = new BasicFilter<EquipAttachFile>();
				filterB.setLstId(filter.getLstEquipAttachFileId());
				filterB.setObjectId(equipLostRec.getId());
				filterB.setObjectType(EquipTradeType.NOTICE_LOST.getValue());
				List<EquipAttachFile> lstFile = equipRecordDAO.searchEquipAttachFileByFilter(filterB);
				if (lstFile != null && !lstFile.isEmpty()) {
					for (EquipAttachFile fileVItem : lstFile) {
						equipRecordDAO.deletEquipAttachFile(fileVItem);
					}
				}
			}
			//Xu ly them hinh anh
			if (filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
				for (FileVO voFile : filter.getLstFileVo()) {
					EquipAttachFile equipAttFile = new EquipAttachFile();
					equipAttFile.setUrl(voFile.getUrl());
					equipAttFile.setObjectId(equipLostRec.getId());
					equipAttFile.setFileName(voFile.getFileName());
					equipAttFile.setThumbUrl(voFile.getThumbUrl());
					equipAttFile.setObjectType(EquipTradeType.NOTICE_LOST.getValue());
					equipAttFile.setCreateDate(filter.getChangeDate());
					equipAttFile.setCreateUser(filter.getUserName());
					equipRecordDAO.createEquipAttachFile(equipAttFile);
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author vuongmq
	 * @date 07/05/2015
	 * tao moi phieu bao mat;
	 * goi tu ham: insertLoadRecordByListEquip
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Long insertEquipLostRecord(EquipRecordFilter<EquipLostRecord> filter) throws BusinessException {
		try {
			EquipLostRecord equipLostRec = filter.getAttribute().clone();
			//Xu ly them moi bien ban
			if (StringUtility.isNullOrEmpty(equipLostRec.getCode())) {
				String maxCode = equipRecordDAO.getMaxCodeEquipLostRecord("BM");
				equipLostRec.setCode(getEquipLostRecordCodeByMaxCode(maxCode));
			}
			equipLostRec.setUpdateDate(null);
			equipLostRec.setCreateDate(commDAO.getSysDate());
			equipLostRec = equipRecordDAO.createEquipLostRecord(equipLostRec);
			
			//Xu ly them hinh anh
			if (filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
				for (FileVO voFile : filter.getLstFileVo()) {
					EquipAttachFile equipAttFile = new EquipAttachFile();
					equipAttFile.setUrl(voFile.getUrl());
					equipAttFile.setObjectId(equipLostRec.getId());
					equipAttFile.setFileName(voFile.getFileName());
					equipAttFile.setThumbUrl(voFile.getThumbUrl());
					equipAttFile.setObjectType(EquipTradeType.NOTICE_LOST.getValue());
					equipAttFile.setCreateDate(filter.getChangeDate());
					equipAttFile.setCreateUser(filter.getUserName());
					equipRecordDAO.createEquipAttachFile(equipAttFile);
				}
			}
			
			/** Them lich su bien ban */
			this.createEquipFormHistoryByEquipLostRec(equipLostRec, filter.getUserName(), EquipTradeType.NOTICE_LOST.getValue());
			//Xu ly cho tat ca cac luong lien quan
			//equipLostRec.setRecordStatus(filter.getRecordStatus());
			if (filter.getRecordStatus() != null && filter.getRecordStatus() > ActiveType.DELETED.getValue()) {
				equipLostRec.setRecordStatus(StatusRecordsEquip.parseValue(filter.getRecordStatus()));
			}
			Equipment equipment = equipLostRec.getEquip().clone();
			if (filter.getEqLostMobiRecId() != null && filter.getEqLostMobiRecId() > 0) {
				EquipLostMobileRec eqLostMobiRec = equipRecordDAO.getEquipLostMobileRecById(filter.getEqLostMobiRecId());
				if (eqLostMobiRec != null) {
					eqLostMobiRec.setRecordStatus(StatusType.ACTIVE.getValue());
					eqLostMobiRec.setEquipLostRecId(equipLostRec.getId());
					eqLostMobiRec.setUpdateUser(filter.getUserName());
					eqLostMobiRec.setUpdateDate(filter.getChangeDate());
					equipRecordDAO.updateEquipLostMobileRec(eqLostMobiRec);

					/*** begin vuongmq; 20/05/2015; cap nhat them lich sua cho thiet bi*/
					/** Them lich su Thiet bị */
					EquipHistory equipHistory = new EquipHistory();
					equipHistory.setCreateDate(filter.getChangeDate());
					equipHistory.setCreateUser(filter.getUserName());
					equipHistory.setEquip(equipment);
					equipHistory.setStatus(equipment.getStatus().getValue());
					equipHistory.setHealthStatus(equipment.getHealthStatus());
					equipHistory.setUsageStatus(equipment.getUsageStatus());
					equipHistory.setTradeStatus(equipment.getTradeStatus());
					equipHistory.setTradeType(equipment.getTradeType()); 
					equipHistory.setObjectType(equipment.getStockType());
					equipHistory.setObjectId(equipment.getStockId());
					
					equipHistory.setObjectCode(equipment.getStockCode());
					equipHistory.setFormId(equipLostRec.getId());
					equipHistory.setFormType(EquipTradeType.NOTICE_LOST); // bao mat 
					equipHistory.setStockName(equipment.getStockName());
					equipHistory.setTableName(TableName.EQUIP_LOST_RECORD.getValue());
					/***vuongmq; 25/05/2015; khi nao duyet thi moi cap nhatPriceActually() cho equipment */
					//equipHistory.setPriceActually(equipLostRec.getPriceActually());
					
					equipMngDAO.createEquipHistory(equipHistory);
					
					if (StatusRecordsEquip.WAITING_APPROVAL.equals(equipLostRec.getRecordStatus())) {
						equipment = eqLostMobiRec.getEquip();
						if (equipment != null) {
							equipment.setLinquidationStatus(LinquidationStatus.WAITING_LIQUIDATION.getValue());//Cho thanh ly		
							equipment.setUpdateUser(filter.getUserName());
							equipment.setUpdateDate(filter.getChangeDate());
							equipMngDAO.updateEquipment(equipment);
						}
					}
					/*** end vuongmq; 20/05/2015; cap nhat them lich sua cho thiet bi*/
					/*equipment.setTradeType(EquipTradeType.NOTICE_LOST);
					equipment.setUpdateUser(filter.getUserName());
					equipment.setUpdateDate(filter.getChangeDate());
					equipMngDAO.updateEquipment(equipment);
					*//** Them lich su Thiet bị *//*
					equipmentManagerMgr.createEquipHistoryByEquipment(equipment, filter.getUserName());*/

				}
			} else {
				if (!StatusType.ACTIVE.getValue().equals(equipment.getTradeStatus())) {
					if (StatusRecordsEquip.DRAFT.equals(equipLostRec.getRecordStatus()) || StatusRecordsEquip.WAITING_APPROVAL.equals(equipLostRec.getRecordStatus())) {
						/** Xet truong hop du thao; cho duyet */
						//Cap nhat Equipment chuyen trang thai Dang Su Dung
						//equipment.setUsageStatus(EquipUsageStatus.IS_USED);
						if (StatusRecordsEquip.WAITING_APPROVAL.equals(equipLostRec.getRecordStatus())) {
							equipment.setLinquidationStatus(LinquidationStatus.WAITING_LIQUIDATION.getValue());//Cho thanh ly							
						} else {
							equipment.setLinquidationStatus(null);//Cho thanh ly
						}
						equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
						equipment.setTradeType(EquipTradeType.NOTICE_LOST);
						equipment.setUpdateUser(filter.getUserName());
						equipment.setUpdateDate(filter.getChangeDate());
						equipMngDAO.updateEquipment(equipment);
						
						/*** begin vuongmq; 20/05/2015; cap nhat them lich sua cho thiet bi*/
						/** Them lich su Thiet bị */
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(filter.getChangeDate());
						equipHistory.setCreateUser(filter.getUserName());
						equipHistory.setEquip(equipment);
						equipHistory.setStatus(ActiveType.RUNNING.getValue());
						equipHistory.setHealthStatus(equipment.getHealthStatus());
						equipHistory.setUsageStatus(equipment.getUsageStatus());
						equipHistory.setTradeStatus(equipment.getTradeStatus());
						equipHistory.setTradeType(equipment.getTradeType()); 
						equipHistory.setObjectType(equipment.getStockType());
						equipHistory.setObjectId(equipment.getStockId());
						
						equipHistory.setObjectCode(equipment.getStockCode());
						equipHistory.setFormId(equipLostRec.getId());
						equipHistory.setFormType(EquipTradeType.NOTICE_LOST); // bao mat
						equipHistory.setStockName(equipment.getStockName());
						equipHistory.setTableName(TableName.EQUIP_LOST_RECORD.getValue());
						/***vuongmq; 25/05/2015; khi nao duyet thi moi cap nhatPriceActually() cho equipment */
						//equipHistory.setPriceActually(equipLostRec.getPriceActually()); 
						
						equipMngDAO.createEquipHistory(equipHistory);
						/*** end vuongmq; 20/05/2015; cap nhat them lich sua cho thiet bi*/
						
						/** Them lich su Thiet bị *//*
						equipmentManagerMgr.createEquipHistoryByEquipment(equipment, filter.getUserName());*/
						//Cap nhat lich su tu Mobile
					} else {
						throw new BusinessException("StatusRecordsEquip not in (0, 1)");
					}
				}
			}
			return equipLostRec.getId();
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author vuongmq
	 * @date 07/05/2015
	 * tao moi phieu bao mat;
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<Long> insertLoadRecordByListEquip(EquipRecordFilter<EquipLostRecord> filter) throws BusinessException {
		List<Long> lstRes = new ArrayList<Long>();
		/** Kiem tra du lieu dau vao */
		if (filter == null || filter.getLstLong() == null || (filter.getLstLong() != null && filter.getLstLong().isEmpty())) {
			throw new BusinessException("List Equipment Is Null");
		}
		if (filter.getAttribute() == null) {
			throw new BusinessException("EquipLostRecord Is Null");
		}
		try {
			/** Cac bien mac dinh khoi tao ban dau */
			Date sysDateIns = commDAO.getSysDate();
			filter.setChangeDate(sysDateIns);

			//Lay danh sach thiet bi dua tren Danh sach Id cua thiet bi
			EquipmentFilter<Equipment> filterE = new EquipmentFilter<Equipment>();
			filterE.setLstEquipId(filter.getLstLong());
			List<Equipment> lstEquip = equipMngDAO.getListEquipmentEntitesByFilter(filterE);
			if (lstEquip == null || (lstEquip != null && lstEquip.isEmpty())) {
				throw new BusinessException("List Equipment Is Null");
			}
			//Xu ly lay danh sach Ban Hop Dong Giao Nhan Neu Co
			List<Long> lstLongDecrec = new ArrayList<Long>();
			Map<Long, EquipDeliveryRecord> mapEquipDeliveryRecord = new HashMap<Long, EquipDeliveryRecord>();
			for (Entry<Long, Long> mValue : filter.getMapLongLong().entrySet()) {
				if (mValue.getValue() != null) {
					lstLongDecrec.add(mValue.getValue());
				}
			}
			if (lstLongDecrec != null && !lstLongDecrec.isEmpty()) {
				EquipmentFilter<EquipDeliveryRecord> filterDR = new EquipmentFilter<EquipDeliveryRecord>();
				filterDR.setLstId(lstLongDecrec);
				List<EquipDeliveryRecord> lstEquipDelivRec = equipMngDAO.getListEquipDeliveryRecordEntitesByFilter(filterDR);
				for (EquipDeliveryRecord eqDeliveryRec : lstEquipDelivRec) {
					for (Entry<Long, Long> mValue : filter.getMapLongLong().entrySet()) {
						if (mValue.getValue() != null && eqDeliveryRec.getId().equals(mValue.getValue())) {
							mapEquipDeliveryRecord.put(mValue.getKey(), eqDeliveryRec);
						}
					}
				}
			}
			String maxCode = equipRecordDAO.getMaxCodeEquipLostRecord("BM");
			//Xu ly them moi cac bien ban voi danh sach thiet bi bao mat
			for (Equipment equip : lstEquip) {
				EquipLostRecord eqLostRect = filter.getAttribute().clone();
				eqLostRect.setCode(getEquipLostRecordCodeByMaxCode(maxCode));
				/***vuongmq; 25/05/2015; khi nao duyet thi moi cap nhatPriceActually() cho equipment */
				
				/** begin  vuongmq; 06/05/2015; cho nay cap nhat nguyen gia cho equipment v?� equipLostRec */
				/*if (filter.getMapPriceActually() != null && !filter.getMapPriceActually().isEmpty()) {
					BigDecimal priceActual = BigDecimal.ZERO;
					Long pActually = filter.getMapPriceActually().get(equip.getId());
					if (pActually != null) {
						priceActual = BigDecimal.valueOf(pActually);
					}
					eqLostRect.setPriceActually(priceActual);
					equip.setPriceActually(priceActual);
					eqLostRect.setEquip(equip);
					
					equip.setUpdateUser(filter.getUserName());
					equip.setUpdateDate(filter.getChangeDate());
					equipMngDAO.updateEquipment(equip);
				}
				// truong hop import update lai thiet bi gia tri con lai
				if (filter.getPriceActually() != null) {
					eqLostRect.setPriceActually(priceActual);
					equip.setPriceActually(priceActual);
					eqLostRect.setEquip(equip);
					
					equip.setUpdateUser(filter.getUserName());
					equip.setUpdateDate(filter.getChangeDate());
					equipMngDAO.updateEquipment(equip);
				}*/
				/** end  vuongmq; 06/05/2015; cho nay cap nhat nguyen gia cho equipment v?� equipLostRec */
				eqLostRect.setEquipDeliveryRecord(mapEquipDeliveryRecord.get(equip.getId()));
				//Them moi Bien ban bao mat
				filter.setAttribute(eqLostRect);
				lstRes.add(this.insertEquipLostRecord(filter));
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		return lstRes;
	}

	@Override
	public String getEquipLostRecordCodeByMaxCode(String maxCode) {
		if (StringUtility.isNullOrEmpty(maxCode) || maxCode.trim().length() < 10) {
			return "BM00000001";
		}
		String maxCodeNew = maxCode.replaceAll("BM", "").trim();
		try {
			if (!StringUtility.isNumber(maxCodeNew)) {
				return "BM00000001";
			}
			Integer numberMaxCode = Integer.valueOf(maxCodeNew);
			numberMaxCode = numberMaxCode + 1;
			String kq = "00000000" + String.valueOf(numberMaxCode);
			kq = kq.substring(kq.length() - 8).trim();
			kq = "BM" + kq.trim();
			return kq.trim();
		} catch (Exception e) {
			throw new IllegalArgumentException("error data in by maxCode not format");
		}
	}

	@Override
	public ObjectVO<EquipmentDeliveryVO> getListEquipmentLiquidationVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException {
		try {
			List<EquipmentDeliveryVO> lst = equipRecordDAO.getListEquipmentLiquidationVOByFilter(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> objVO = new ObjectVO<EquipmentDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<EquipmentDeliveryVO> getListEquipmentLiquidationPopup(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException {
		try {
			List<EquipmentDeliveryVO> lst = equipRecordDAO.getListEquipmentLiquidationPopup(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> objVO = new ObjectVO<EquipmentDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public EquipmentLiquidationFormVO getEquipmentLiquidationVOById(Long id, Long shopId) throws BusinessException {
		try {
			return id == null ? null : equipliquidationFormDAO.getEquipmentLiquidationFormVOById(id, shopId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void updateLiquidationForm(EquipLiquidationForm equipLiquidationForm, List<EquipLiquidationFormDtl> lstLiquidationFormDtls, Integer status, EquipRecordFilter<EquipmentLiquidationFormVO> filter ) throws BusinessException {
		try {
			Date sysDateIns = commDAO.getSysDate();
			equipLiquidationForm.setUpdateDate(sysDateIns);
			if (status != null) {
				equipLiquidationForm.setStatus(status);
			}
			if (StatusRecordsEquip.APPROVED.getValue().equals(equipLiquidationForm.getStatus()) && equipLiquidationForm.getApproveDate() == null) {
				equipLiquidationForm.setApproveDate(sysDateIns);
			}
			equipliquidationFormDAO.updateEquipLiquidationForm(equipLiquidationForm);

			if (!status.equals(StatusRecordsEquip.DRAFT.getValue())) {
				// ghi lich su equipFormHistory
				EquipFormHistory equipFormHistory = new EquipFormHistory();
				equipFormHistory.setActDate(sysDateIns);
				equipFormHistory.setCreateDate(sysDateIns);
				equipFormHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
				equipFormHistory.setRecordId(equipLiquidationForm.getId());
				equipFormHistory.setRecordStatus(equipLiquidationForm.getStatus());
				equipFormHistory.setRecordType(EquipTradeType.LIQUIDATION.getValue());
				Staff staff = staffDAO.getStaffByCode(equipLiquidationForm.getUpdateUser());
				equipFormHistory.setStaff(staff);
				equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
			}
			if (!status.equals(StatusRecordsEquip.CANCELLATION.getValue())) { 
				List<EquipLiquidationFormDtl> liquidationFormDtlsOld = equipliquidationFormDAO.getListEquipLiquidationFormDtlByEquipLiquidationFormId(equipLiquidationForm.getId());
				if (!status.equals(StatusRecordsEquip.LIQUIDATED.getValue())) {
					//duyet hoac du thao
					for (int i = 0, sz = liquidationFormDtlsOld.size(); i < sz; i++) {
						EquipLiquidationFormDtl ed = liquidationFormDtlsOld.get(i);
						Boolean isDelete = true;
						int n = lstLiquidationFormDtls.size();
						for (int j = 0; j < n; j++) {
							if (ed.getEquip().getId().equals(lstLiquidationFormDtls.get(j).getEquip().getId())) {
								isDelete = false;
								break;
							}
						}
						// xoa chi tiet
						if (isDelete) {
							Equipment eq = ed.getEquip();
							if (!EquipTradeType.NOTICE_LOST.equals(eq.getTradeType())) {
								// dang o kho, dang su dung
								eq.setTradeType(null);
								eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
								
								// luu lich su thiet bi
								EquipHistory equipHistory = new EquipHistory();
								equipHistory.setCreateDate(sysDateIns);
								equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
								equipHistory.setEquip(eq);
								equipHistory.setHealthStatus(eq.getHealthStatus());
								equipHistory.setObjectId(eq.getStockId());
								equipHistory.setObjectType(eq.getStockType());
								equipHistory.setStatus(eq.getStatus().getValue());
								equipHistory.setTradeStatus(eq.getTradeStatus());
								equipHistory.setTradeType(eq.getTradeType());
								equipHistory.setUsageStatus(eq.getUsageStatus());
								equipHistory.setObjectCode(eq.getStockCode());
								equipHistory.setFormId(equipLiquidationForm.getId());
								equipHistory.setFormType(EquipTradeType.LIQUIDATION);
								equipHistory.setStockName(eq.getStockName());
								equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
								//equipHistory.setPriceActually(ed.getEquipValue());
								equipHistory.setLiquidationValue(ed.getLiquidationValue());
								equipHistory.setEvictionValue(ed.getEvictionValue());
								equipMngDAO.createEquipHistory(equipHistory);
							} else {
								// bao mat
								eq.setLinquidationStatus(LinquidationStatus.WAITING_LIQUIDATION.getValue());
								
								// luu lich su thiet bi
								EquipHistory equipHistory = new EquipHistory();
								equipHistory.setCreateDate(sysDateIns);
								equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
								equipHistory.setEquip(eq);
								equipHistory.setHealthStatus(eq.getHealthStatus());
								equipHistory.setObjectId(eq.getStockId());
								equipHistory.setObjectType(eq.getStockType());
								equipHistory.setStatus(eq.getStatus().getValue());
								equipHistory.setTradeStatus(eq.getTradeStatus());
								equipHistory.setTradeType(eq.getTradeType());
								equipHistory.setUsageStatus(eq.getUsageStatus());
								equipHistory.setObjectCode(eq.getStockCode());
								equipHistory.setFormId(equipLiquidationForm.getId());
								equipHistory.setFormType(EquipTradeType.LIQUIDATION);
								equipHistory.setStockName(eq.getStockName());
								equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
								//equipHistory.setPriceActually(ed.getEquipValue());
								equipHistory.setLiquidationValue(ed.getLiquidationValue());
								equipHistory.setEvictionValue(ed.getEvictionValue());
								equipHistory.setLiquidationStatus(eq.getLinquidationStatus()); // vuongmq; 15/07/2015; chi co thiet bi bao mat moi co truong nay luu lich su
								equipMngDAO.createEquipHistory(equipHistory);
							}
							eq.setUpdateUser(equipLiquidationForm.getUpdateUser());
							eq.setUpdateDate(sysDateIns);
							equipMngDAO.updateEquipment(eq);
	
							// xoa chi tiet bien ban
							equipliquidationFormDAO.deleteEquipLiquidationFormDetail(ed);
						}
					}
					// van la lstLiquidationFormDtls, thay doi gia tri thanh ly va thu hoi
					for (int i = 0, sz = lstLiquidationFormDtls.size(); i < sz; i++) {
						EquipLiquidationFormDtl ed = lstLiquidationFormDtls.get(i);
						Equipment eq = ed.getEquip();
						Boolean isNew = true;
						int n = liquidationFormDtlsOld.size();
						for (int j = 0; j < n; j++) {
							if (liquidationFormDtlsOld.get(j).getEquip().getId().equals(ed.getEquip().getId())) {
								isNew = false;
								// sua chi tiet
								/*if (ed.getEquipValue().compareTo(liquidationFormDtlsOld.get(j).getEquipValue()) != 0) {
									EquipLiquidationFormDtl edtemp = liquidationFormDtlsOld.get(j);
									edtemp.setEquipValue(ed.getEquipValue());
									edtemp.setUpdateDate(sysDateIns);
									edtemp.setUpdateUser(equipLiquidationForm.getUpdateUser());
									equipliquidationFormDAO.updateEquipLiquidationFormDtl(edtemp);
								}*/
								if ( (ed.getLiquidationValue() != null && liquidationFormDtlsOld.get(j).getLiquidationValue() == null)
									|| (ed.getLiquidationValue() == null && liquidationFormDtlsOld.get(j).getLiquidationValue() != null)
									|| (ed.getEvictionValue() != null && liquidationFormDtlsOld.get(j).getEvictionValue() == null)
									|| (ed.getEvictionValue() == null && liquidationFormDtlsOld.get(j).getEvictionValue() != null)
									|| (ed.getLiquidationValue() != null && ed.getLiquidationValue().compareTo(liquidationFormDtlsOld.get(j).getLiquidationValue()) != 0)
									|| (ed.getEvictionValue() != null && ed.getEvictionValue().compareTo(liquidationFormDtlsOld.get(j).getEvictionValue()) != 0)) {
									EquipLiquidationFormDtl edtemp = liquidationFormDtlsOld.get(j);
									edtemp.setLiquidationValue(ed.getLiquidationValue());
									edtemp.setEvictionValue(ed.getEvictionValue());
									edtemp.setUpdateDate(sysDateIns);
									edtemp.setUpdateUser(equipLiquidationForm.getUpdateUser());
									equipliquidationFormDAO.updateEquipLiquidationFormDtl(edtemp);
									// luu lich su thiet bi
									EquipHistory equipHistory = new EquipHistory();
									equipHistory.setCreateDate(sysDateIns);
									equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
									equipHistory.setEquip(eq);
									equipHistory.setHealthStatus(eq.getHealthStatus());
									equipHistory.setObjectId(eq.getStockId());
									equipHistory.setObjectType(eq.getStockType());
									equipHistory.setStatus(eq.getStatus().getValue());
									equipHistory.setTradeStatus(eq.getTradeStatus());
									equipHistory.setTradeType(eq.getTradeType());
									equipHistory.setUsageStatus(eq.getUsageStatus());
									equipHistory.setObjectCode(eq.getStockCode());
									equipHistory.setFormId(equipLiquidationForm.getId());
									equipHistory.setFormType(EquipTradeType.LIQUIDATION);
									equipHistory.setStockName(eq.getStockName());
									equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
									//equipHistory.setPriceActually(ed.getEquipValue());
									/** gia tri thanh ly va thu hoi*/
									equipHistory.setLiquidationValue(ed.getLiquidationValue());
									equipHistory.setEvictionValue(ed.getEvictionValue());
									
									//vuongmq; 15/07/2015; neu la TB bao mat co them cot lich su trang thai thanh ly
									if (EquipTradeType.NOTICE_LOST.equals(eq.getTradeType())) {
										equipHistory.setLiquidationStatus(eq.getLinquidationStatus()); // vuongmq; 15/07/2015; chi co thiet bi bao mat moi co truong nay luu lich su
									}
									equipMngDAO.createEquipHistory(equipHistory);
								}
								break;
							}
						}
						// them chi tiet
						if (isNew) {
							if (!EquipTradeType.NOTICE_LOST.equals(eq.getTradeType())) {
								// dang o kho, dang su dung
								eq.setTradeType(EquipTradeType.LIQUIDATION);
								eq.setTradeStatus(EquipTradeStatus.TRADING.getValue());
								
								// luu lich su thiet bi
								EquipHistory equipHistory = new EquipHistory();
								equipHistory.setCreateDate(sysDateIns);
								equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
								equipHistory.setEquip(eq);
								equipHistory.setHealthStatus(eq.getHealthStatus());
								equipHistory.setObjectId(eq.getStockId());
								equipHistory.setObjectType(eq.getStockType());
								equipHistory.setStatus(eq.getStatus().getValue());
								equipHistory.setTradeStatus(eq.getTradeStatus());
								equipHistory.setTradeType(eq.getTradeType());
								equipHistory.setUsageStatus(eq.getUsageStatus());
								equipHistory.setObjectCode(eq.getStockCode());
								equipHistory.setFormId(equipLiquidationForm.getId());
								equipHistory.setFormType(EquipTradeType.LIQUIDATION);
								equipHistory.setStockName(eq.getStockName());
								equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
								//equipHistory.setPriceActually(ed.getEquipValue());
								/** gia tri thanh ly va thu hoi*/
								equipHistory.setLiquidationValue(ed.getLiquidationValue());
								equipHistory.setEvictionValue(ed.getEvictionValue());
								equipMngDAO.createEquipHistory(equipHistory);
							} else {
								// bao mat
								eq.setLinquidationStatus(LinquidationStatus.PROCESS_LIQUIDATION.getValue());
								
								// luu lich su thiet bi
								EquipHistory equipHistory = new EquipHistory();
								equipHistory.setCreateDate(sysDateIns);
								equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
								equipHistory.setEquip(eq);
								equipHistory.setHealthStatus(eq.getHealthStatus());
								equipHistory.setObjectId(eq.getStockId());
								equipHistory.setObjectType(eq.getStockType());
								equipHistory.setStatus(eq.getStatus().getValue());
								equipHistory.setTradeStatus(eq.getTradeStatus());
								equipHistory.setTradeType(eq.getTradeType());
								equipHistory.setUsageStatus(eq.getUsageStatus());
								equipHistory.setObjectCode(eq.getStockCode());
								equipHistory.setFormId(equipLiquidationForm.getId());
								equipHistory.setFormType(EquipTradeType.LIQUIDATION);
								equipHistory.setStockName(eq.getStockName());
								equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
								//equipHistory.setPriceActually(ed.getEquipValue());
								/** gia tri thanh ly va thu hoi*/
								equipHistory.setLiquidationValue(ed.getLiquidationValue());
								equipHistory.setEvictionValue(ed.getEvictionValue());
								equipHistory.setLiquidationStatus(eq.getLinquidationStatus()); // vuongmq; 15/07/2015; chi co thiet bi bao mat moi co truong nay luu lich su
								equipMngDAO.createEquipHistory(equipHistory);
							}
							eq.setUpdateDate(sysDateIns);
							eq.setUpdateUser(equipLiquidationForm.getUpdateUser());
							equipMngDAO.updateEquipment(eq);
	
							// luu chi tiet bien ban
							ed.setEquipLiquidationForm(equipLiquidationForm);
							ed.setCreateDate(sysDateIns);
							ed.setStockId(eq.getStockId());
							ed.setStockType(eq.getStockType()); //vuongmq; 14/07/2015; dac ta cap nhat; 1. kho DonVi, 2. KH,
							ed = equipliquidationFormDAO.createEquipLiquidationFormDetail(ed);
						}
					} // end Dieu kien du thao va duyet
				} else {
					// begin thanh ly; cap nhat trang thai thanh ly; liquidationFormDtlsOld cua phieu danh sach
					int size = liquidationFormDtlsOld.size();
					for (int i = 0; i < size; i++) {
						EquipLiquidationFormDtl ed = liquidationFormDtlsOld.get(i);
						Equipment eq = liquidationFormDtlsOld.get(i).getEquip();
						// update kho NPP, KH
						EquipStockTotal equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.parseValue(eq.getStockType().getValue()), eq.getEquipGroup().getId());
						if (equipStockTotal.getQuantity() > 0) {
							equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
						} else {
							equipStockTotal.setQuantity(0);
						}
						equipStockTotal.setUpdateDate(sysDateIns);
						equipStockTotal.setUpdateUser(equipLiquidationForm.getUpdateUser());
						equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
						// kho tong
						EquipStockEquipFilter<EquipStockTotal> filterSt = new EquipStockEquipFilter<EquipStockTotal>();
						filterSt.setEquipGroupId(eq.getEquipGroup().getId());
						filterSt.setStockId(null);
						filterSt.setStockType(EquipStockTotalType.KHO_TONG.getValue()); // set lai loai kho la kho tong: 0; van nhom do, stockId null
						EquipStockTotal equipStockTotalTong = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
						//EquipStockTotal equipStockTotalTong = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.KHO_TONG, eq.getEquipGroup().getId());
						if (equipStockTotalTong != null) {
							if (equipStockTotal.getQuantity() > 0) {
								equipStockTotalTong.setQuantity(equipStockTotalTong.getQuantity() - 1);
							} else {
								equipStockTotalTong.setQuantity(0);
							}
							equipStockTotalTong.setUpdateDate(sysDateIns);
							equipStockTotalTong.setUpdateUser(equipLiquidationForm.getUpdateUser());
							equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotalTong);
						}
						// cap nhat thiet bi	
						//eq.setStockType(null);
						if (!EquipTradeType.NOTICE_LOST.equals(eq.getTradeType())) {
							// dang o kho, dang su dung
							eq.setUsageStatus(EquipUsageStatus.LIQUIDATED);
							eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
							eq.setTradeType(null);
							
							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
							equipHistory.setEquip(eq);
							equipHistory.setHealthStatus(eq.getHealthStatus());
							equipHistory.setObjectId(eq.getStockId());
							equipHistory.setObjectType(eq.getStockType());
							equipHistory.setStatus(eq.getStatus().getValue());
							equipHistory.setTradeStatus(eq.getTradeStatus());
							equipHistory.setTradeType(eq.getTradeType());
							equipHistory.setUsageStatus(eq.getUsageStatus());
							equipHistory.setObjectCode(eq.getStockCode());
							equipHistory.setFormId(equipLiquidationForm.getId());
							equipHistory.setFormType(EquipTradeType.LIQUIDATION);
							equipHistory.setStockName(eq.getStockName());
							equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
							equipHistory.setLiquidationValue(ed.getLiquidationValue());
							equipHistory.setEvictionValue(ed.getEvictionValue());
							equipMngDAO.createEquipHistory(equipHistory);
						} else {
							// eq.setUsageStatus(EquipUsageStatus.LIQUIDATED); // cap nhat la thiet bi da thanh ly; vuongmq; 28/07/2015; chi cap nhat trang thai thiet bi thoi
							// thanh ly thiet bi Chi dung bao mat: trang thai thanh ly la: da thanh ly
							eq.setLinquidationStatus(LinquidationStatus.FINISH_LIQUIDATION.getValue());
							
							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
							equipHistory.setEquip(eq);
							equipHistory.setHealthStatus(eq.getHealthStatus());
							equipHistory.setObjectId(eq.getStockId());
							equipHistory.setObjectType(eq.getStockType());
							equipHistory.setStatus(eq.getStatus().getValue());
							equipHistory.setTradeStatus(eq.getTradeStatus());
							equipHistory.setTradeType(eq.getTradeType());
							equipHistory.setUsageStatus(eq.getUsageStatus());
							equipHistory.setObjectCode(eq.getStockCode());
							equipHistory.setFormId(equipLiquidationForm.getId());
							equipHistory.setFormType(EquipTradeType.LIQUIDATION);
							equipHistory.setStockName(eq.getStockName());
							equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
							equipHistory.setLiquidationValue(ed.getLiquidationValue());
							equipHistory.setEvictionValue(ed.getEvictionValue());
							equipHistory.setLiquidationStatus(eq.getLinquidationStatus()); // vuongmq; 15/07/2015; chi co thiet bi bao mat moi co truong nay luu lich su
							equipMngDAO.createEquipHistory(equipHistory);
						}
						eq.setUpdateUser(equipLiquidationForm.getUpdateUser());
						eq.setUpdateDate(sysDateIns);
						eq.setLiquidationDate(sysDateIns);
						equipMngDAO.updateEquipment(eq);
					}
				}
			} else {
				// huy
				List<EquipLiquidationFormDtl> liquidationFormDtls = equipliquidationFormDAO.getListEquipLiquidationFormDtlByEquipLiquidationFormId(equipLiquidationForm.getId());
				int size = liquidationFormDtls.size();
				for (int i = 0; i < size; i++) {
					EquipLiquidationFormDtl ed = liquidationFormDtls.get(i);
					Equipment eq = liquidationFormDtls.get(i).getEquip();
					if (!EquipTradeType.NOTICE_LOST.equals(eq.getTradeType())) {
						// dang o kho, dang su dung
						eq.setTradeType(null);
						eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						
						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
						equipHistory.setEquip(eq);
						equipHistory.setHealthStatus(eq.getHealthStatus());
						equipHistory.setObjectId(eq.getStockId());
						equipHistory.setObjectType(eq.getStockType());
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setFormId(equipLiquidationForm.getId());
						equipHistory.setFormType(EquipTradeType.LIQUIDATION);
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
						//equipHistory.setPriceActually(liquidationFormDtls.get(i).getEquipValue());
						equipHistory.setLiquidationValue(ed.getLiquidationValue());
						equipHistory.setEvictionValue(ed.getEvictionValue());
						equipMngDAO.createEquipHistory(equipHistory);
					}  else {
						// bao mat; neu huy chuyen sang Khong thanh ly
						eq.setLinquidationStatus(LinquidationStatus.NO_LIQUIDATION.getValue());
						
						// luu lich su thiet bi
						EquipHistory equipHistory = new EquipHistory();
						equipHistory.setCreateDate(sysDateIns);
						equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
						equipHistory.setEquip(eq);
						equipHistory.setHealthStatus(eq.getHealthStatus());
						equipHistory.setObjectId(eq.getStockId());
						equipHistory.setObjectType(eq.getStockType());
						equipHistory.setStatus(eq.getStatus().getValue());
						equipHistory.setTradeStatus(eq.getTradeStatus());
						equipHistory.setTradeType(eq.getTradeType());
						equipHistory.setUsageStatus(eq.getUsageStatus());
						equipHistory.setObjectCode(eq.getStockCode());
						equipHistory.setFormId(equipLiquidationForm.getId());
						equipHistory.setFormType(EquipTradeType.LIQUIDATION);
						equipHistory.setStockName(eq.getStockName());
						equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
						equipHistory.setLiquidationValue(ed.getLiquidationValue());
						equipHistory.setEvictionValue(ed.getEvictionValue());
						equipHistory.setLiquidationStatus(eq.getLinquidationStatus()); // vuongmq; 15/07/2015; chi co thiet bi bao mat moi co truong nay luu lich su
						equipMngDAO.createEquipHistory(equipHistory);
					}
					eq.setUpdateUser(equipLiquidationForm.getUpdateUser());
					eq.setUpdateDate(sysDateIns);
					equipMngDAO.updateEquipment(eq);

					// xoa chi tiet bien ban
					//equipliquidationFormDAO.deleteEquipLiquidationFormDetail(liquidationFormDtls.get(i));
				}
			}
			//Xu ly xoa file
			if (filter.getLstEquipAttachFileId() != null && !filter.getLstEquipAttachFileId().isEmpty()) {
				BasicFilter<EquipAttachFile> filterB = new BasicFilter<EquipAttachFile>();
				filterB.setLstId(filter.getLstEquipAttachFileId());
				List<EquipAttachFile> lstFile = equipRecordDAO.searchEquipAttachFileByFilter(filterB);
				if (lstFile != null && !lstFile.isEmpty()) {
					for (EquipAttachFile fileVItem : lstFile) {
						equipRecordDAO.deletEquipAttachFile(fileVItem);
					}
				}
			}
			//Xu ly them file
			if (filter.getLstFileVo() != null && !filter.getLstFileVo().isEmpty()) {
				for (FileVO voFile : filter.getLstFileVo()) {
					EquipAttachFile equipAttFile = new EquipAttachFile();
					equipAttFile.setUrl(voFile.getUrl());
					equipAttFile.setObjectId(equipLiquidationForm.getId());
					equipAttFile.setFileName(voFile.getFileName());
					equipAttFile.setThumbUrl(voFile.getThumbUrl());
					equipAttFile.setObjectType(EquipTradeType.LIQUIDATION.getValue());
					equipAttFile.setCreateDate(sysDateIns);
					equipAttFile.setCreateUser(equipLiquidationForm.getUpdateUser());
					equipRecordDAO.createEquipAttachFile(equipAttFile);
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveEquipLiquidationForm(EquipLiquidationForm equipLiquidationForm, Integer status) throws BusinessException {
		try {
			Date sysDateIns = commDAO.getSysDate();
			equipLiquidationForm.setUpdateDate(sysDateIns);
			if (StatusRecordsEquip.APPROVED.getValue().equals(status) && equipLiquidationForm.getApproveDate() == null) {
				equipLiquidationForm.setApproveDate(sysDateIns);
			}
			equipliquidationFormDAO.updateEquipLiquidationForm(equipLiquidationForm);

			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(sysDateIns);
			equipFormHistory.setCreateDate(sysDateIns);
			equipFormHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
			equipFormHistory.setRecordId(equipLiquidationForm.getId());
			equipFormHistory.setRecordStatus(equipLiquidationForm.getStatus());
			equipFormHistory.setRecordType(EquipTradeType.LIQUIDATION.getValue());
			String staffCode = equipLiquidationForm.getUpdateUser() != null ? equipLiquidationForm.getUpdateUser() : equipLiquidationForm.getCreateUser();
			if (staffCode != null) {
				Staff staff = staffDAO.getStaffByCode(staffCode);
				equipFormHistory.setStaff(staff);
			}
			equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
			
			if (StatusRecordsEquip.CANCELLATION.getValue().equals(status) || StatusRecordsEquip.APPROVED.getValue().equals(status) || StatusRecordsEquip.LIQUIDATED.getValue().equals(status)) {

				// thay doi trang thai giao nhan va trang thai bien ban
				List<EquipLiquidationFormDtl> liquidationFormDtls = equipliquidationFormDAO.getListEquipLiquidationFormDtlByEquipLiquidationFormId(equipLiquidationForm.getId());
				int size = liquidationFormDtls.size();
				for (int i = 0; i < size; i++) {
					EquipLiquidationFormDtl ed = liquidationFormDtls.get(i);
					Equipment eq = liquidationFormDtls.get(i).getEquip();
					if (status.equals(StatusRecordsEquip.CANCELLATION.getValue())) {
						if (!EquipTradeType.NOTICE_LOST.equals(eq.getTradeType())) {
							// dang o kho, dang su dung
							eq.setTradeType(null);
							eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
							
							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
							equipHistory.setEquip(eq);
							equipHistory.setHealthStatus(eq.getHealthStatus());
							equipHistory.setObjectId(eq.getStockId());
							equipHistory.setObjectType(eq.getStockType());
							equipHistory.setStatus(eq.getStatus().getValue());
							equipHistory.setTradeStatus(eq.getTradeStatus());
							equipHistory.setTradeType(eq.getTradeType());
							equipHistory.setUsageStatus(eq.getUsageStatus());
							equipHistory.setObjectCode(eq.getStockCode());
							equipHistory.setFormId(equipLiquidationForm.getId());
							equipHistory.setFormType(EquipTradeType.LIQUIDATION);
							equipHistory.setStockName(eq.getStockName());
							equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
							//equipHistory.setPriceActually(liquidationFormDtls.get(i).getEquipValue());
							equipHistory.setLiquidationValue(ed.getLiquidationValue());
							equipHistory.setEvictionValue(ed.getEvictionValue());
							equipMngDAO.createEquipHistory(equipHistory);
						} else {
							// bao mat; neu huy chuyen sang Khong thanh ly
							eq.setLinquidationStatus(LinquidationStatus.NO_LIQUIDATION.getValue());
							
							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
							equipHistory.setEquip(eq);
							equipHistory.setHealthStatus(eq.getHealthStatus());
							equipHistory.setObjectId(eq.getStockId());
							equipHistory.setObjectType(eq.getStockType());
							equipHistory.setStatus(eq.getStatus().getValue());
							equipHistory.setTradeStatus(eq.getTradeStatus());
							equipHistory.setTradeType(eq.getTradeType());
							equipHistory.setUsageStatus(eq.getUsageStatus());
							equipHistory.setObjectCode(eq.getStockCode());
							equipHistory.setFormId(equipLiquidationForm.getId());
							equipHistory.setFormType(EquipTradeType.LIQUIDATION);
							equipHistory.setStockName(eq.getStockName());
							equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
							//equipHistory.setPriceActually(liquidationFormDtls.get(i).getEquipValue());
							equipHistory.setLiquidationValue(ed.getLiquidationValue());
							equipHistory.setEvictionValue(ed.getEvictionValue());
							equipHistory.setLiquidationStatus(eq.getLinquidationStatus()); // vuongmq; 15/07/2015; chi co thiet bi bao mat moi co truong nay luu lich su
							equipMngDAO.createEquipHistory(equipHistory);
						}
						eq.setUpdateUser(equipLiquidationForm.getUpdateUser());
						eq.setUpdateDate(sysDateIns);
						equipMngDAO.updateEquipment(eq);

						// xoa chi tiet bien ban
						//equipmentLiquidationFormDAO.deleteEquipLiquidationFormDetail(liquidationFormDtls.get(i));
					} else if (status.equals(StatusRecordsEquip.APPROVED.getValue())) {
						if (!status.equals(equipLiquidationForm.getStatus())) {
							if (!EquipTradeType.NOTICE_LOST.equals(eq.getTradeType())) {
								// dang o kho, dang su dung
								eq.setTradeType(EquipTradeType.LIQUIDATION);
								eq.setTradeStatus(EquipTradeStatus.TRADING.getValue());
								
								// luu lich su thiet bi
								EquipHistory equipHistory = new EquipHistory();
								equipHistory.setCreateDate(sysDateIns);
								equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
								equipHistory.setEquip(eq);
								equipHistory.setHealthStatus(eq.getHealthStatus());
								equipHistory.setObjectId(eq.getStockId());
								equipHistory.setObjectType(eq.getStockType());
								equipHistory.setStatus(eq.getStatus().getValue());
								equipHistory.setTradeStatus(eq.getTradeStatus());
								equipHistory.setTradeType(eq.getTradeType());
								equipHistory.setUsageStatus(eq.getUsageStatus());
								equipHistory.setObjectCode(eq.getStockCode());
								equipHistory.setFormId(equipLiquidationForm.getId());
								equipHistory.setFormType(EquipTradeType.LIQUIDATION);
								equipHistory.setStockName(eq.getStockName());
								equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
								//equipHistory.setPriceActually(liquidationFormDtls.get(i).getEquipValue());
								equipHistory.setLiquidationValue(ed.getLiquidationValue());
								equipHistory.setEvictionValue(ed.getEvictionValue());
								equipMngDAO.createEquipHistory(equipHistory);
							} else {
								// bao mat
								eq.setLinquidationStatus(LinquidationStatus.PROCESS_LIQUIDATION.getValue());
								
								// luu lich su thiet bi
								EquipHistory equipHistory = new EquipHistory();
								equipHistory.setCreateDate(sysDateIns);
								equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
								equipHistory.setEquip(eq);
								equipHistory.setHealthStatus(eq.getHealthStatus());
								equipHistory.setObjectId(eq.getStockId());
								equipHistory.setObjectType(eq.getStockType());
								equipHistory.setStatus(eq.getStatus().getValue());
								equipHistory.setTradeStatus(eq.getTradeStatus());
								equipHistory.setTradeType(eq.getTradeType());
								equipHistory.setUsageStatus(eq.getUsageStatus());
								equipHistory.setObjectCode(eq.getStockCode());
								equipHistory.setFormId(equipLiquidationForm.getId());
								equipHistory.setFormType(EquipTradeType.LIQUIDATION);
								equipHistory.setStockName(eq.getStockName());
								equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
								//equipHistory.setPriceActually(liquidationFormDtls.get(i).getEquipValue());
								equipHistory.setLiquidationValue(ed.getLiquidationValue());
								equipHistory.setEvictionValue(ed.getEvictionValue());
								equipHistory.setLiquidationStatus(eq.getLinquidationStatus()); // vuongmq; 15/07/2015; chi co thiet bi bao mat moi co truong nay luu lich su
								equipMngDAO.createEquipHistory(equipHistory);
							}
							eq.setUpdateUser(equipLiquidationForm.getUpdateUser());
							eq.setUpdateDate(sysDateIns);
							equipMngDAO.updateEquipment(eq);
						}
					} else {
						// DA THANH LY
						// update kho NPP, KH
						EquipStockTotal equipStockTotal = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.parseValue(eq.getStockType().getValue()), eq.getEquipGroup().getId());
						if (equipStockTotal != null) {
							if (equipStockTotal.getQuantity() > 0) {
								equipStockTotal.setQuantity(equipStockTotal.getQuantity() - 1);
							} else {
								equipStockTotal.setQuantity(0);
							}
							equipStockTotal.setUpdateDate(sysDateIns);
							equipStockTotal.setUpdateUser(equipLiquidationForm.getUpdateUser());
							equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotal);
						}
						// update kho tong
						EquipStockEquipFilter<EquipStockTotal> filterSt = new EquipStockEquipFilter<EquipStockTotal>();
						filterSt.setEquipGroupId(eq.getEquipGroup().getId());
						filterSt.setStockId(null);
						filterSt.setStockType(EquipStockTotalType.KHO_TONG.getValue()); // set lai loai kho la kho tong: 0; van nhom do, stockId null
						EquipStockTotal equipStockTotalTong = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
						//EquipStockTotal equipStockTotalTong = equipmentStockTotalDAO.getEquipStockTotalById(eq.getStockId(), EquipStockTotalType.KHO_TONG, eq.getEquipGroup().getId());
						if (equipStockTotalTong != null) {
							if (equipStockTotal.getQuantity() > 0) {
								equipStockTotalTong.setQuantity(equipStockTotalTong.getQuantity() - 1);
							} else {
								equipStockTotalTong.setQuantity(0);
							}
							equipStockTotalTong.setUpdateDate(sysDateIns);
							equipStockTotalTong.setUpdateUser(equipLiquidationForm.getUpdateUser());
							equipmentStockTotalDAO.updateEquipmentStockTotal(equipStockTotalTong);
						}

						// cap nhat thiet bi	
						// eq.setStockCode(null);
						// eq.setStockId(null);
						//eq.setStockType(null);
						if (!EquipTradeType.NOTICE_LOST.equals(eq.getTradeType()) && !EquipTradeType.NOTICE_LOSS_MOBILE.equals(eq.getTradeType())) {
							// dang o kho, dang su dung
							eq.setUsageStatus(EquipUsageStatus.LIQUIDATED);
							eq.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
							eq.setTradeType(null);
							
							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
							equipHistory.setEquip(eq);
							equipHistory.setHealthStatus(eq.getHealthStatus());
							equipHistory.setObjectId(eq.getStockId());
							equipHistory.setObjectType(eq.getStockType());
							equipHistory.setStatus(eq.getStatus().getValue());
							equipHistory.setTradeStatus(eq.getTradeStatus());
							equipHistory.setTradeType(eq.getTradeType());
							equipHistory.setUsageStatus(eq.getUsageStatus());
							equipHistory.setObjectCode(eq.getStockCode());
							equipHistory.setFormId(equipLiquidationForm.getId());
							equipHistory.setFormType(EquipTradeType.LIQUIDATION);
							equipHistory.setStockName(eq.getStockName());
							equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
							//equipHistory.setPriceActually(liquidationFormDtls.get(i).getEquipValue());
							equipHistory.setLiquidationValue(ed.getLiquidationValue());
							equipHistory.setEvictionValue(ed.getEvictionValue());
							equipMngDAO.createEquipHistory(equipHistory);
						} else {
							//eq.setUsageStatus(EquipUsageStatus.LIQUIDATED); // cap nhat la thiet bi da thanh ly; vuongmq; 28/07/2015; chi cap nhat trang thai thiet bi thoi
							// thanh ly thiet bi Chi dung bao mat: trang thai thanh ly la: da thanh ly
							eq.setLinquidationStatus(LinquidationStatus.FINISH_LIQUIDATION.getValue());
							
							// luu lich su thiet bi
							EquipHistory equipHistory = new EquipHistory();
							equipHistory.setCreateDate(sysDateIns);
							equipHistory.setCreateUser(equipLiquidationForm.getUpdateUser());
							equipHistory.setEquip(eq);
							equipHistory.setHealthStatus(eq.getHealthStatus());
							equipHistory.setObjectId(eq.getStockId());
							equipHistory.setObjectType(eq.getStockType());
							equipHistory.setStatus(eq.getStatus().getValue());
							equipHistory.setTradeStatus(eq.getTradeStatus());
							equipHistory.setTradeType(eq.getTradeType());
							equipHistory.setUsageStatus(eq.getUsageStatus());
							equipHistory.setObjectCode(eq.getStockCode());
							equipHistory.setFormId(equipLiquidationForm.getId());
							equipHistory.setFormType(EquipTradeType.LIQUIDATION);
							equipHistory.setStockName(eq.getStockName());
							equipHistory.setTableName(TableName.EQUIP_LIQUIDATION_FORM.getValue());
							//equipHistory.setPriceActually(liquidationFormDtls.get(i).getEquipValue());
							equipHistory.setLiquidationValue(ed.getLiquidationValue());
							equipHistory.setEvictionValue(ed.getEvictionValue());
							equipHistory.setLiquidationStatus(eq.getLinquidationStatus()); // vuongmq; 15/07/2015; chi co thiet bi bao mat moi co truong nay luu lich su
							equipMngDAO.createEquipHistory(equipHistory);
						}
						eq.setUpdateUser(equipLiquidationForm.getUpdateUser());
						eq.setUpdateDate(sysDateIns);
						eq.setLiquidationDate(sysDateIns);
						equipMngDAO.updateEquipment(eq);
					}
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}


	@Override
	public ObjectVO<EquipLostMobileRecVO> getListEquipLostMobileRecByFilter(EquipmentFilter<EquipLostMobileRecVO> equipmentFilter) throws BusinessException {
		try {
			List<EquipLostMobileRecVO> lst = equipRecordDAO.getListEquipLostMobileRecByFilter(equipmentFilter);
			ObjectVO<EquipLostMobileRecVO> objVO = new ObjectVO<EquipLostMobileRecVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<EquipmentVO> getListEquipmentVOByEquiId(Long id) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipRecordDAO.getListEquipmentVOByEquiId(id);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}

	}

	@Override
	public ObjectVO<EquipLostMobileRec> searchEquipRecordFilter(EquipRecordFilter<EquipLostMobileRec> filter) throws BusinessException {
		try {
			List<EquipLostMobileRec> lst = equipRecordDAO.searchEquipRecordFilter(filter);
			ObjectVO<EquipLostMobileRec> objVO = new ObjectVO<EquipLostMobileRec>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateByListRecordMobile(EquipRecordFilter<EquipLostMobileRec> filter) throws BusinessException {
		/** Kiem tra du lieu dau vao */
		if (filter == null || filter.getLstId() == null || filter.getLstId().isEmpty()) {
			throw new BusinessException("List EquipLostMobileRec Is Null");
		}
		try {
			Date sysDateDb = commDAO.getSysDate();
			filter.setChangeDate(sysDateDb);

			List<EquipLostMobileRec> lstData = equipRecordDAO.searchEquipRecordFilter(filter);
			for (EquipLostMobileRec mobileRec : lstData) {
				mobileRec.setRecordStatus(StatusType.CANCALLED.getValue());
				mobileRec.setUpdateUser(filter.getUserName());
				mobileRec.setUpdateDate(sysDateDb);
				commDAO.updateEntity(mobileRec);
				//Cap nhat Thiet bi tuong ung
				if (mobileRec.getEquip() != null) {
					Equipment equip = mobileRec.getEquip().clone();
					equip.setUpdateDate(sysDateDb);
					equip.setUpdateUser(filter.getUserName());
					equip.setTradeStatus(StatusType.INACTIVE.getValue());
					equip.setTradeType(null);
					equipMngDAO.updateEquipment(equip);
					
					/*** begin vuongmq; 20/05/2015; cap nhat them lich sua cho thiet bi*/
					/** Them lich su Thiet bị */
					EquipHistory equipHistory = new EquipHistory();
					equipHistory.setCreateDate(sysDateDb);
					equipHistory.setCreateUser(filter.getUserName());
					equipHistory.setEquip(equip);
					equipHistory.setStatus(ActiveType.RUNNING.getValue());
					equipHistory.setHealthStatus(equip.getHealthStatus());
					equipHistory.setUsageStatus(equip.getUsageStatus());
					equipHistory.setTradeStatus(equip.getTradeStatus());
					equipHistory.setTradeType(equip.getTradeType()); 
					equipHistory.setObjectType(equip.getStockType());
					equipHistory.setObjectId(equip.getStockId());

					equipHistory.setObjectCode(equip.getStockCode());
					equipHistory.setFormId(mobileRec.getId());
					equipHistory.setFormType(EquipTradeType.NOTICE_LOSS_MOBILE); // bao mat mobile
					equipHistory.setStockName(equip.getStockName());
					equipHistory.setTableName(TableName.EQUIP_LOST_MOBILE_REC.getValue());
					equipHistory.setPriceActually(equip.getPriceActually());

					equipMngDAO.createEquipHistory(equipHistory);
					/*** end vuongmq; 20/05/2015; cap nhat them lich sua cho thiet bi*/
					/** Them lich su Thiet bị *//*
					equipmentManagerMgr.createEquipHistoryByEquipment(equip, filter.getUserName());*/
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipStatisticRecord getEquipStatisticRecordByCode(String code) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getEquipStatisticRecordByCode(code);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkEquipStatisticRecordByCode(String code) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getEquipStatisticRecordByCode(code) == null ? false : true;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void getLstEquipAttachFileId(EquipRecordFilter<EquipLostRecord> filter) throws BusinessException {
		try {
			if (filter.getLstEquipAttachFileId() != null && !filter.getLstEquipAttachFileId().isEmpty()) {
				BasicFilter<EquipAttachFile> filterB = new BasicFilter<EquipAttachFile>();
				filterB.setLstId(filter.getLstEquipAttachFileId());
			//	filterB.setObjectId(filter.getLstId());
				filterB.setObjectType(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
				List<EquipAttachFile> lstFile = equipRecordDAO.searchEquipAttachFileByFilter(filterB);
				if (lstFile != null && !lstFile.isEmpty()) {
					for (EquipAttachFile fileVItem : lstFile) {
						equipRecordDAO.deletEquipAttachFile(fileVItem);
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		
	}
	
	@Override
	public ObjectVO<StatisticCheckingVO> getListDetailShelf(EquipStatisticFilter filter) throws BusinessException {
		try {
			List<StatisticCheckingVO> list = equipRecordDAO.getListDetailShelf(filter);
			ObjectVO<StatisticCheckingVO> voObj = new ObjectVO<StatisticCheckingVO>();
			voObj.setkPaging(filter.getkPaging());
			voObj.setLstObject(list);
			return voObj;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public ObjectVO<StatisticCheckingVO> getListDetailGroup(EquipStatisticFilter filter) throws BusinessException {
		try {
			List<StatisticCheckingVO> list = equipRecordDAO.getListDetailGroup(filter);
			ObjectVO<StatisticCheckingVO> voObj = new ObjectVO<StatisticCheckingVO>();
			voObj.setkPaging(filter.getkPaging());
			voObj.setLstObject(list);
			return voObj;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkEquipInUnitAndGroup(Long recordId, Long equipId) throws BusinessException {
		try {
			return equipStatisticRecordDAO.checkEquipInUnitAndGroup(recordId, equipId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkEquipInUnit(Long recordId, Long equipId) throws BusinessException {
		try {
			return equipStatisticRecordDAO.checkEquipInUnit(recordId, equipId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Shop getShopInEquip(Long stockId) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getShopInEquip(stockId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createListEquipStatisticRecDetail(List<EquipStatisticRecDtl> lst) throws BusinessException {
		try {
			commDAO.creatListEntity(lst);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEquipStatisticRecDetail(EquipStatisticRecDtl equipStatisticRecDtl) throws BusinessException {
		try {
			commDAO.updateEntity(equipStatisticRecDtl);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipStatisticRecDtl geEquipStatisticRecDtlByFilter(EquipStatisticFilter filter) throws BusinessException {
		try {
			return equipRecordDAO.geEquipStatisticRecDtlByFilter(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkExistInEquipStatisticUnit(Long recordId, Long shopId) throws BusinessException {
		try {
			return equipStatisticRecordDAO.checkExistInEquipStatisticUnit(recordId, shopId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkExistInEquipStatisticGroup(Long recordId, Long objectId) throws BusinessException {
		try {
			return equipStatisticRecordDAO.checkExistInEquipStatisticGroup(recordId, objectId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<EquipmentDeliveryVO> getListEquipmentLostPopUp(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws BusinessException {
		try {
			List<EquipmentDeliveryVO> lst = equipRecordDAO.getListEquipmentLostPopUp(equipmentFilter);
			ObjectVO<EquipmentDeliveryVO> objVO = new ObjectVO<EquipmentDeliveryVO>();
			objVO.setkPaging(equipmentFilter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<EquipStockVO> getListStockLostByFilter(EquipStockFilter filter) throws BusinessException {
		try {
			List<EquipStockVO> lst = equipRecordDAO.getListStockLostByFilter(filter);
			ObjectVO<EquipStockVO> objVO = new ObjectVO<EquipStockVO>();
			objVO.setLstObject(lst);
			objVO.setkPaging(filter.getkPaging());
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<Customer> getListCustomerBuTCHTTM(Long recordId) throws BusinessException {
		try {
			return equipMngDAO.getListCustomerBuTCHTTM(recordId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public Boolean checkCustomerInEquipStatisCus(Long recordId, Long customerId) throws BusinessException {
		try {
			return equipStatisticRecordDAO.checkCustomerInEquipStatisCus(recordId, customerId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<EquipRecordShopVO> getListShopOfRecordExport(Long recordId, List<ShopObjectType> lstShopObjectType, String shopCode, String shopName) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getListShopOfRecordExport(recordId, lstShopObjectType, shopCode, shopName);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipStatisticUnit> getListStatisticUnitChild(Long recordId,Long shopId) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getListStatisticUnitChild(recordId,shopId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void deleteListEquipStatisticUnit(List<EquipStatisticUnit> lst) throws BusinessException {
		try {
			if(lst!=null && !lst.isEmpty()){
				for(EquipStatisticUnit item : lst){
					commDAO.deleteEntity(item);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Shop> getListShopForEquipStatisticUnit(Long shopId,List<Long> lstShopIdExcept) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getListShopForEquipStatisticUnit(shopId, lstShopIdExcept);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<EquipmentVO> getListEquipmentByStatisticFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			List<EquipmentVO> lst = equipStatisticGroupDAO.getListEquipmentByStatisticFilter(filter);
			ObjectVO<EquipmentVO> objVO = new ObjectVO<EquipmentVO>();
			objVO.setLstObject(lst);
			objVO.setkPaging(filter.getkPaging());
			return objVO;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipStatisticGroup getEquipStatisticGroupById(Long recordId,
			Long groupId, EquipObjectType type) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getEquipStatisticGroupById(recordId, groupId, type);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public Boolean checkStaffInEquipStatisStaff(Long recordId, Long staffId,Long equipId) throws BusinessException {
		try {
			return equipStatisticRecordDAO.checkStaffInEquipStatisStaff(recordId, staffId, equipId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipStatisticStaff createEquipStatisticStaff(EquipStatisticStaff entity) throws BusinessException {
		try {
			return commDAO.createEntity(entity);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<EquipmentVO> getListEquipmentStaffByStatisticFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			return equipStatisticGroupDAO.getListEquipmentStaffByStatisticFilter(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public void runProcessForEquipShelfTotal(Long recordId,String programCode,String lstShopId) throws BusinessException {
		try{
			EquipStatisticRecord record = equipStatisticRecordDAO.getEquipStatisticRecordById(recordId);
			if(record!=null && record.getPromotionCode()!=null){
				List<EquipStatisticUnit> lstShop = equipStatisticRecordDAO.getListStatisticUnitChild(recordId, null);
				String lstShopIdStr = "-1";
				if(!lstShop.isEmpty()){
					for(EquipStatisticUnit unit : lstShop){
						lstShopIdStr+=","+unit.getUnitId();
					}
				}
				List<SpParam> inParams = new ArrayList<SpParam>();
		        List<SpParam> outParams = new ArrayList<SpParam>();
		      
		        inParams.add(new SpParam(1, java.sql.Types.NUMERIC, record.getId()));
		        inParams.add(new SpParam(2, java.sql.Types.VARCHAR, record.getPromotionCode()));
		        inParams.add(new SpParam(3, java.sql.Types.VARCHAR, lstShopIdStr));
		        repo.executeSP("P_UPDATE_EQUIP_SHELF_TOTAL", inParams, outParams);
			}
		}catch (DataAccessException e) {
			throw new BusinessException(e);
		}catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public void deleteEquipStatisticStaffNotBelong(Long recordId) throws BusinessException {
		try{
			List<EquipStatisticStaff> lst = equipStatisticRecordDAO.getListEquipStatisticStaffNotBelong(recordId);
			if(!lst.isEmpty()){
				for(EquipStatisticStaff equip : lst){
					commDAO.deleteEntity(equip);
				}
			}
		}catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateEquipAttachFile(EquipAttachFile equipAttachFile) throws BusinessException {
		/** Kiem tra du lieu dau vao */
		if (equipAttachFile == null) {
			throw new BusinessException("equipAttachFile Is Null");
		}
		try {
			commDAO.updateEntity(equipAttachFile);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<EquipmentVO> getListChecking(Date fromDate, Date toDate, Long shopId) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getListChecking(fromDate, toDate, shopId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipRecordVO getLostRecordNewestByEquipId(Long equipId) throws BusinessException {
		try {
			return equipRecordDAO.getLostRecordNewestByEquipId(equipId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipLiquidationFormDtl> getListEquipLiquidationFormDtlByRecordId(Long recordId) throws BusinessException {
		try {
			return equipRecordDAO.getListEquipLiquidationFormDtlByRecordId(recordId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteEquipStatisticStaff(Long recordId, Long staffId, Long equipId) throws BusinessException {
		try {
			equipStatisticRecordDAO.deleteEquipStatisticStaff(recordId, staffId, equipId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipmentVO> getListEquipStaffByStatisticFilter(EquipmentFilter<EquipmentVO> filter) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getListEquipStaffByStatisticFilter(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipStatisticStaff getEquipStatisticStaff(Long recordId, Long staffId, Long equipId) throws BusinessException {
		try {
			return equipStatisticRecordDAO.getEquipStatisticStaff(recordId, staffId, equipId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean isStaffBelongShop(long staffId, long shopId) throws BusinessException {
		try {
			return equipStatisticRecordDAO.isStaffBelongShop(staffId, shopId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean isExistContractNumInDeliveryRecord(String contractNumber) throws BusinessException {
		try {
			return equipRecordDAO.isExistContractNumInDeliveryRecord(contractNumber);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<EquipStatisticRecordDetailVO> getListStatisticTime(EquipStatisticFilter filter) throws BusinessException {
		try {
			List<EquipStatisticRecordDetailVO> lst = equipRecordDAO.getListStatisticTime(filter);
			ObjectVO<EquipStatisticRecordDetailVO> objVO = new ObjectVO<EquipStatisticRecordDetailVO>();
			objVO.setLstObject(lst);
			objVO.setkPaging(filter.getkPagingESRD());
			return objVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

package ths.dms.core.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.InvoiceStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.core.entities.vo.rpt.RptInvDetailStat1VO;
import ths.core.entities.vo.rpt.RptInvDetailStatVO;
import ths.core.entities.vo.rpt.RptInvVO;
import ths.core.entities.vo.rpt.RptInvoiceComparingVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.InvoiceDAO;
import ths.dms.core.dao.SaleOrderDAO;
import ths.dms.core.dao.SaleOrderDetailDAO;
import ths.dms.core.dao.ShopDAO;

public class RptTaxMgrImpl implements RptTaxMgr {

	@Autowired
	SaleOrderDetailDAO saleOrderDetailDAO;

	@Autowired
	ShopDAO shopDAO;
	
	@Autowired
	SaleOrderDAO saleOrderDAO;
	
	@Autowired
	InvoiceDAO invoiceDAO;

	
	@Override
	public List<RptInvVO> getListRptInvVO(KPaging<SaleOrder> kPaging,
			Long shopId, String staffCode, String shortCode, Float vat,
			InvoiceStatus invSatus, Date fromDate, Date toDate)
			throws BusinessException {
		try {
			List<SaleOrder> lst = saleOrderDAO.getListSaleOrder(kPaging,
					shopId, shortCode, shortCode, vat, invSatus, fromDate,
					toDate);
			ArrayList<RptInvVO> ret = new ArrayList<RptInvVO>();
			for (SaleOrder so : lst) {
				RptInvVO vo = new RptInvVO();
				List<SaleOrderDetail> tmp = saleOrderDetailDAO.getListSaleOrderDetailBySaleOrderId(so.getId(), null);
				vo.setSaleOrder(so);
				vo.setLstSaleOrderDetail(tmp);
				ret.add(vo);
			}
			return ret;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<RptInvoiceComparingVO> getListRptInvoiceComparingVO(
			KPaging<RptInvoiceComparingVO> kPaging, Float vat, String customerCode,
			Long staffId, Date fromDate, Date toDate, Long shopId,
			ActiveType status) throws BusinessException {
		try {
			List<RptInvoiceComparingVO> lst = invoiceDAO.getListRptInvoiceComparingVO(kPaging, vat, customerCode, staffId, fromDate, toDate, shopId, status);
			ObjectVO<RptInvoiceComparingVO> vo = new ObjectVO<RptInvoiceComparingVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

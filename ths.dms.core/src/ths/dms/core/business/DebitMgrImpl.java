package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.Bank;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Debit;
import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.PayReceived;
import ths.dms.core.entities.PayReceivedTemp;
import ths.dms.core.entities.PaymentDetail;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DebitDetailType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.DebitPaymentType;
import ths.dms.core.entities.enumtype.DebitType;
import ths.dms.core.entities.enumtype.DebtPaymentType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PayReceivedType;
import ths.dms.core.entities.enumtype.PaymentStatus;
import ths.dms.core.entities.enumtype.PaymentType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.ReceiptType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.filter.DebitFilter;
import ths.dms.core.entities.vo.CustomerDebitVO;
import ths.dms.core.entities.vo.DebitCustomerParams;
import ths.dms.core.entities.vo.DebitCustomerVO;
import ths.dms.core.entities.vo.DebitObjectVO;
import ths.dms.core.entities.vo.DebitShopVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PayReceivedVO;
import ths.core.entities.vo.rpt.RptDebtOfCustomerDataVO;
import ths.core.entities.vo.rpt.RptDebtOfCustomerLv01VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.BankDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.CustomerDAO;
import ths.dms.core.dao.DebitDAO;
import ths.dms.core.dao.DebitDetailDAO;
import ths.dms.core.dao.PayReceivedDAO;
import ths.dms.core.dao.PaymentDetailDAO;
import ths.dms.core.dao.SaleOrderDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.ShopLockDAO;
import ths.dms.core.dao.StaffDAO;

public class DebitMgrImpl implements DebitMgr {

	@Autowired
	DebitDetailDAO debitDetailDAO;
	
	@Autowired
	ShopDAO shopDAO;
	
	@Autowired
	StaffDAO staffDAO;
	
	@Autowired
	PayReceivedDAO payReceivedDAO;
	
	@Autowired
	SaleOrderDAO saleOrderDAO;
	
	@Autowired
	PaymentDetailDAO paymentDetailDAO;
	
	@Autowired
	CustomerDAO customerDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	DebitDAO debitDAO;
	
	@Autowired
	ShopLockDAO shopLockDAO;
	
	@Autowired
	BankDAO bankDAO;
	
	@Override
	public DebitDetail createDebitDetail(DebitDetail debitDetail) throws BusinessException {
		try {
			return debitDetailDAO.createDebitDetail(debitDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateDebitDetail(DebitDetail debitDetail) throws BusinessException {
		try {
			debitDetailDAO.updateDebitDetail(debitDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteDebitDetail(DebitDetail debitDetail) throws BusinessException{
		try {
			debitDetailDAO.deleteDebitDetail(debitDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public DebitDetail getDebitDetailById(Long id) throws BusinessException{
		try {
			return id==null?null:debitDetailDAO.getDebitDetailById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<DebitDetail> getListDebitDetailByFilter(SoFilter filter) throws BusinessException{
		try {
			return debitDetailDAO.getListDebitDetailByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<DebitDetail> getListPositiveDebitDetail(KPaging<DebitDetail> kPaging,
			String shortCode, Long fromObjectId, DebitOwnerType type,
			BigDecimal fromAmount, BigDecimal toAmount, Long shopId) throws BusinessException {
		try {
			List<DebitDetail> lst = debitDetailDAO.getListPositiveDebitDetail(kPaging, shortCode, fromObjectId, type, fromAmount, toAmount, shopId);
			ObjectVO<DebitDetail> vo = new ObjectVO<DebitDetail>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	//------------------------------------------------
	
	@Override
	public PaymentDetail createPaymentDetail(PaymentDetail paymentDetail) throws BusinessException {
		try {
			return paymentDetailDAO.createPaymentDetail(paymentDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updatePaymentDetail(PaymentDetail paymentDetail) throws BusinessException {
		try {
			paymentDetailDAO.updatePaymentDetail(paymentDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deletePaymentDetail(PaymentDetail paymentDetail) throws BusinessException{
		try {
			paymentDetailDAO.deletePaymentDetail(paymentDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public PaymentDetail getPaymentDetailById(Long id) throws BusinessException{
		try {
			return id==null?null:paymentDetailDAO.getPaymentDetailById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	//--------------------------------------------------------------------------
	/**
	 * Thanh toan theo lo: PaymentDetail.DEBT_PAYEMENT_TYPE=MANUAL  
	 * Thanh toan tu dong: PaymentDetail.DEBT_PAYEMENT_TYPE=AUTO
	 * Xoa no nho: PaymentDetail.DEBT_PAYEMENT_TYPE=REMOVE_SMALL_DEBT; tao moi payReceieved voi
	 * 			payReceivedNumber="PT" + randomNumber;
	 *   */
	@Transactional(rollbackFor=Exception.class)
	@Override
	public PayReceived createPayReceived(PayReceived payReceived, 
			List<PaymentDetail> lstPaymentDetail) throws BusinessException {
		try {
			payReceived.setReceiptType(ReceiptType.RECEIVED);
			PayReceived p =  payReceivedDAO.createPayReceived(payReceived);
			if (p == null)
				return null;
			//check total money
			BigDecimal totalMoney = new BigDecimal(0);
			for (PaymentDetail dd: lstPaymentDetail) {
				totalMoney = totalMoney.add(dd.getAmount());
				dd.setPayReceived(p);
			}
			if (totalMoney.compareTo(payReceived.getAmount()) == 1)
				throw new BusinessException("Total money in debit detail is larger than payReceived.");
			payReceivedDAO.updatePayReceived(p);
			paymentDetailDAO.createListPaymentDetail(lstPaymentDetail);
			return p;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updatePayReceived(PayReceived payReceived) throws BusinessException {
		try {
			payReceivedDAO.updatePayReceived(payReceived);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deletePayReceived(PayReceived payReceived) throws BusinessException{
		try {
			payReceivedDAO.deletePayReceived(payReceived);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public PayReceived getPayReceivedById(Long id) throws BusinessException{
		try {
			return id==null?null:payReceivedDAO.getPayReceivedById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public PayReceived getPayReceivedByNumber(String payreceivedNumber, Long shopId) throws BusinessException{
		try {
			return payReceivedDAO.getPayReceivedByNumber(payreceivedNumber, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	//----------------------------------------

	@Override
	public List<DebitShopVO> getListDebitShopVO(Long shopId, PoType poType) throws BusinessException {
		try {
			return debitDetailDAO.getListDebitShopVO(shopId, poType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		
	}
	
	@Override
	public BigDecimal getDebitOfShop(Long shopId) throws BusinessException {
		try {
			Debit debit = debitDAO.getDebit(shopId, DebitOwnerType.SHOP);
			if (debit == null) {
				throw new IllegalArgumentException("Debit is null");
			}
			return debit.getTotalDebit();
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Debit getDebit(Long ownerId, DebitOwnerType ownerType) throws BusinessException {
		try {
			if (ownerId == null) {
				throw new IllegalArgumentException("ownerId is null");
			}
			if (ownerType == null) {
				throw new IllegalArgumentException("ownerType is null");
			}
			return debitDAO.getDebit(ownerId, ownerType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional (rollbackFor = Exception.class)
	public void updateDebitOfShop(long shopId, String payReceivedNumber, BigDecimal amount,
			List<Long> lstDebitId, List<BigDecimal> lstDebitAmt, ReceiptType receiptType, String user) throws BusinessException {
		try {
			if (StringUtility.isNullOrEmpty(payReceivedNumber)) {
				throw new IllegalArgumentException("payReceivedNumber is null or empty");
			}
			if (amount == null) {
				throw new IllegalArgumentException("amount is null");
			}
			if (lstDebitId == null) {
				throw new IllegalArgumentException("lstDebitId is null");
			}
			if (lstDebitAmt == null) {
				throw new IllegalArgumentException("lstDebitAmt is null");
			}
//			if (logShopInfo == null) {
//				throw new IllegalArgumentException("logShopInfo is null");
//			}
			if (lstDebitId.size() != lstDebitAmt.size()) {
				throw new IllegalArgumentException("lstDebitId.size() != lstDebitAmt.size()");
			}
			if (receiptType == null) {
				throw new IllegalArgumentException("receiptType is null");
			}
			if (receiptType != ReceiptType.PAID && receiptType != ReceiptType.RECEIVED) {
				throw new IllegalArgumentException("receiptType != ReceiptType.PAID && receiptType != ReceiptType.RECEIVED");
			}
			//////cap nhat Shop
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			Debit debit = debitDAO.getDebitForUpdate(shopId, DebitOwnerType.SHOP);
			if (debit == null) {
				throw new IllegalArgumentException("debit is null");
			}
			Date lockDate = shopLockDAO.getApplicationDate(shop.getId()); // lacnv1
			Date now = commonDAO.getSysDate();
			Integer time = 0;
			//35.	Thanh toán đơn hàng
			if (receiptType == ReceiptType.PAID) {
				//
				debit.setTotalPay((debit.getTotalPay() != null ? debit.getTotalPay() : BigDecimal.ZERO).add(amount));
				debit.setTotalDebit((debit.getTotalDebit() != null ? debit.getTotalDebit() : BigDecimal.ZERO).subtract(amount));
				debitDAO.updateDebit(debit);
				////////cap nhat PayReceived
				PayReceived payReceived = new PayReceived();
				payReceived.setCreateUser(user);
				payReceived.setPayReceivedNumber(payReceivedNumber);
				payReceived.setPaymentType(PaymentType.MONEY);
				payReceived.setShop(shop);
				payReceived.setAmount(amount);
				payReceived.setReceiptType(receiptType);
				payReceived.setType(PayReceivedType.THANH_TOAN.getValue()); // lacnv1
				payReceived.setCreateDate(lockDate);
				payReceived.setCreateDateSys(now); //tungtt21
				payReceived.setPaymentStatus(PaymentStatus.PAID);
				payReceived = payReceivedDAO.createPayReceived(payReceived);
				//////////
				ArrayList<PaymentDetail> lstPaymentDetail = new ArrayList<PaymentDetail>();
				DebitDetail debitDetail = null;
				PaymentDetail paymentDetail = null;
				for (int i = 0, sz = lstDebitId.size(); i < sz; i++) {
					Long debitId = lstDebitId.get(i);
					if (debitId == null) {
						throw new IllegalArgumentException("debitId is null");
					}
					BigDecimal debitAmt = lstDebitAmt.get(i);
					if (debitAmt == null) {
						throw new IllegalArgumentException("debitAmt is null");
					}
					if (debitAmt.compareTo(BigDecimal.ZERO) <= 0) {
						throw new IllegalArgumentException("debitAmt.compareTo(BigDecimal.ZERO) <= 0");
					}
					////////////////cap nhat Debit
					debitDetail = debitDetailDAO.getDebitDetailByIdForUpdate(debitId);
					if (debitDetail == null) {
						throw new IllegalArgumentException("debit is null");
					}
					if (debitDetail.getRemain() == null ) {
						throw new IllegalArgumentException("debit.getRemain() == null");
					}
					if(debitDetail.getRemain().compareTo(BigDecimal.ZERO) == 0) {
						throw new BusinessException(ExceptionCode.DEBIT_DETAIL_NOT_REMAIN);
					}
					if (debitDetail.getRemain().compareTo(debitAmt) < 0) {
						throw new IllegalArgumentException("debit.getRemain().compareTo(debitAmt) < 0");
					}
					/*if (debitDetail.getAmount().compareTo(debitAmt) < 0) {
						throw new IllegalArgumentException("debit.getAmount().compareTo(debitAmt) < 0");
					}*/
					if (debitDetail.getTotal() == null || debitDetail.getTotal().compareTo(debitAmt) < 0) {
						throw new IllegalArgumentException("debit.getTotal().compareTo(debitAmt) < 0");
					}
					debitDetail.setTotalPay((debitDetail.getTotalPay() != null ? debitDetail.getTotalPay() : BigDecimal.ZERO).add(debitAmt));
					debitDetail.setRemain((debitDetail.getTotal() != null ? debitDetail.getTotal(): BigDecimal.ZERO).subtract(debitDetail.getTotalPay()));
					debitDetail.setUpdateUser(user);
					debitDetailDAO.updateDebitDetail(debitDetail);
					////////////////tao PaymentDetail
					time = paymentDetailDAO.getTimeOfPay(debitDetail.getId()); // lacnv1
					paymentDetail = new PaymentDetail();
					paymentDetail.setDebit(debitDetail);
					paymentDetail.setPaymentType(DebtPaymentType.AUTO);
					paymentDetail.setAmount(debitAmt);
					paymentDetail.setPayReceived(payReceived);
					paymentDetail.setCreateUser(user);
					paymentDetail.setCreateDate(now); //tungtt21
					paymentDetail.setPayDate(lockDate); // lacnv1
					paymentDetail.setDiscount(BigDecimal.ZERO);
					paymentDetail.setTime(time + 1); // lacnv1
					paymentDetail.setShop(shop);
					paymentDetail.setPaymentStatus(PaymentStatus.PAID);
					//////
					lstPaymentDetail.add(paymentDetail);
				}
				paymentDetailDAO.createListPaymentDetail(lstPaymentDetail);
			}
			//36.	Nhận lại tiền khi trả hàng
			if (receiptType == ReceiptType.RECEIVED) {
				if (amount.compareTo(BigDecimal.ZERO) >= 0) {
					throw new IllegalArgumentException("amount.compareTo(BigDecimal.ZERO) >= 0");
				}
				//
				debit.setTotalPay((debit.getTotalPay() != null ? debit.getTotalPay() : BigDecimal.ZERO).add(amount));
				debit.setTotalDebit((debit.getTotalDebit() != null ? debit.getTotalDebit() : BigDecimal.ZERO).subtract(amount));
				debitDAO.updateDebit(debit);
				////////cap nhat PayReceived
				PayReceived payReceived = new PayReceived();
				payReceived.setCreateUser(user);
				payReceived.setPayReceivedNumber(payReceivedNumber);
				payReceived.setPaymentType(PaymentType.MONEY);
				payReceived.setShop(shop);
				payReceived.setAmount(amount);
				payReceived.setReceiptType(receiptType);
				payReceived.setType(PayReceivedType.THANH_TOAN.getValue()); // lacnv1
				payReceived.setCreateDate(lockDate);
				payReceived.setCreateDateSys(now);//tungtt21
				payReceived.setPaymentStatus(PaymentStatus.PAID);
				payReceivedDAO.createPayReceived(payReceived);
				//////////
				ArrayList<PaymentDetail> lstPaymentDetail = new ArrayList<PaymentDetail>();
				DebitDetail debitDetail = null;
				PaymentDetail paymentDetail = null;
				for (int i = 0; i < lstDebitId.size(); i++) {
					Long debitId = lstDebitId.get(i);
					if (debitId == null) {
						throw new IllegalArgumentException("debitId is null");
					}
					BigDecimal debitAmt = lstDebitAmt.get(i);
					if (debitAmt == null) {
						throw new IllegalArgumentException("debitAmt is null");
					}
					if (debitAmt.compareTo(BigDecimal.ZERO) >= 0) {
						throw new IllegalArgumentException("debitAmt.compareTo(BigDecimal.ZERO) >= 0");
					}
					////////////////cap nhat Debit
					debitDetail = debitDetailDAO.getDebitDetailByIdForUpdate(debitId);
					if (debitDetail == null) {
						throw new IllegalArgumentException("debit is null");
					}
					if (debitDetail.getRemain() == null || debitDetail.getRemain().compareTo(BigDecimal.ZERO) > 0) {
						throw new IllegalArgumentException("debit.getRemain() == null || debit.getRemain().compareTo(BigDecimal.ZERO) > 0");
					}
					if(debitDetail.getRemain().compareTo(BigDecimal.ZERO) == 0) {
						throw new BusinessException(ExceptionCode.DEBIT_DETAIL_NOT_REMAIN);
					}
					if (debitDetail.getRemain().compareTo(debitAmt) > 0) {
						throw new IllegalArgumentException("debit.getRemain().compareTo(debitAmt) > 0");
					}
					/*if (debitDetail.getAmount().compareTo(debitAmt) > 0) {
						throw new IllegalArgumentException("debit.getAmount().compareTo(debitAmt) > 0");
					}*/
					if (debitDetail.getTotal() == null || debitDetail.getTotal().compareTo(debitAmt) > 0) {
						throw new IllegalArgumentException("debit.getTotal().compareTo(debitAmt) > 0");
					}
					debitDetail.setTotalPay((debitDetail.getTotalPay() != null ? debitDetail.getTotalPay() : BigDecimal.ZERO).add(debitAmt));
					debitDetail.setRemain((debitDetail.getTotal() != null ? debitDetail.getTotal() : BigDecimal.ZERO).subtract(debitDetail.getTotalPay()));
					debitDetail.setUpdateUser(user);
					debitDetailDAO.updateDebitDetail(debitDetail);
					////////////////tao PaymentDetail
					time = paymentDetailDAO.getTimeOfPay(debitDetail.getId()); // lacnv1
					paymentDetail = new PaymentDetail();
					paymentDetail.setDebit(debitDetail);
					paymentDetail.setPaymentType(DebtPaymentType.AUTO);
					paymentDetail.setAmount(debitAmt);
					paymentDetail.setPayReceived(payReceived);
					paymentDetail.setCreateUser(user);
					paymentDetail.setCreateDate(now); //tungtt21
					paymentDetail.setPayDate(lockDate); // lacnv1
					paymentDetail.setDiscount(BigDecimal.ZERO);
					paymentDetail.setTime(time + 1); // lacnv1
					paymentDetail.setShop(shop);
					paymentDetail.setPaymentStatus(PaymentStatus.PAID);
					//////
					lstPaymentDetail.add(paymentDetail);
				}
				paymentDetailDAO.createListPaymentDetail(lstPaymentDetail);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	
	/**
	 * 
	 * @modify by lacnv1 - them id NVBH
	 * @modify date Mar 26, 2014
	 */
	@Override
	public List<CustomerDebitVO> getListCustomerDebitVO(Long shopId, String shortCode, String orderNumber, BigDecimal amount,
			BigDecimal fromAmount, BigDecimal toAmount, Boolean isReveived,
			Date fromDate, Date toDate, Long nvghId, Long nvttId, Long nvbhId) throws BusinessException {	
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (amount != null && isReveived != null) {
				if (amount.compareTo(new BigDecimal(0)) == 0 
						||amount.compareTo(new BigDecimal(0)) > 0 && isReveived == false
						|| amount.compareTo(new BigDecimal(0)) < 0 && isReveived == true) {
					throw new IllegalArgumentException("amount or isReveived is invalid");
				}
			}
			List<CustomerDebitVO> lst = debitDetailDAO.getListCustomerDebitVO(shopId, shortCode, orderNumber, fromAmount, toAmount,
					isReveived, fromDate, toDate, nvghId, nvttId, nvbhId);
			if (amount == null) {
				return lst;
			} else {
				ArrayList<CustomerDebitVO> ret = new ArrayList<CustomerDebitVO>();
				if (amount.compareTo(new BigDecimal(0)) > 0) {
					for (CustomerDebitVO customerDebitVO : lst) {
						ret.add(customerDebitVO);
						if (customerDebitVO.getRemain().compareTo(new BigDecimal(0)) > 0) {
							if (amount.abs().compareTo(customerDebitVO.getRemain().abs()) >= 0) {
								customerDebitVO.setPayAmt(customerDebitVO.getRemain());
								amount = amount.subtract(customerDebitVO.getRemain());
								if (amount.compareTo(new BigDecimal(0)) == 0) {
									break;
								}
							} else {
								customerDebitVO.setPayAmt(amount);
								amount = new BigDecimal(0);
								break;
							}
						}
					}
				} else {
					for (CustomerDebitVO customerDebitVO : lst) {
						ret.add(customerDebitVO);
						if (customerDebitVO.getRemain().compareTo(new BigDecimal(0)) < 0) {
							if (amount.abs().compareTo(customerDebitVO.getRemain().abs()) >= 0) {
								customerDebitVO.setPayAmt(customerDebitVO.getRemain());
								amount = amount.subtract(customerDebitVO.getRemain());
								if (amount.compareTo(new BigDecimal(0)) == 0) {
									break;
								}
							} else {
								customerDebitVO.setPayAmt(amount);
								amount = new BigDecimal(0);
								break;
							}
						}
					}
				}
				return ret;
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<CustomerDebitVO> getListCustomerDebitVOByFilter(DebitFilter<CustomerDebitVO> filter) throws BusinessException {	
		try {
			if (filter == null) {
				throw new IllegalArgumentException("fillter null");
			}
			if (filter.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			BigDecimal amount = null;
			if (filter.getAmount() != null && filter.getIsReveived() != null) {
				amount = filter.getAmount();
				if (amount.signum() == 0 || (amount.signum() > 0 && !filter.getIsReveived())
						|| (amount.signum() < 0 && filter.getIsReveived())) {
					throw new IllegalArgumentException("amount or isReveived is invalid");
				}
			}
			List<CustomerDebitVO> lst = debitDetailDAO.getListCustomerDebitVO(filter);
			if (amount == null) {
				return lst;
			}
			
			ArrayList<CustomerDebitVO> ret = new ArrayList<CustomerDebitVO>();
			if (amount.signum() > 0) {
				for (CustomerDebitVO customerDebitVO : lst) {
					ret.add(customerDebitVO);
					if (customerDebitVO.getRemain() != null && customerDebitVO.getRemain().signum() > 0) {
						if (amount.abs().compareTo(customerDebitVO.getRemain().abs()) >= 0) {
							customerDebitVO.setPayAmt(customerDebitVO.getRemain());
							amount = amount.subtract(customerDebitVO.getRemain());
							if (amount.signum() == 0) {
								break;
							}
						} else {
							customerDebitVO.setPayAmt(amount);
							amount = BigDecimal.ZERO;
							break;
						}
					}
				}
			} else {
				for (CustomerDebitVO customerDebitVO : lst) {
					ret.add(customerDebitVO);
					if (customerDebitVO.getRemain() != null && customerDebitVO.getRemain().signum() < 0) {
						if (amount.abs().compareTo(customerDebitVO.getRemain().abs()) >= 0) {
							customerDebitVO.setPayAmt(customerDebitVO.getRemain());
							amount = amount.subtract(customerDebitVO.getRemain());
							if (amount.signum() == 0) {
								break;
							}
						} else {
							customerDebitVO.setPayAmt(amount);
							amount = BigDecimal.ZERO;
							break;
						}
					}
				}
			}
			return ret;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * Chỉ Dùng cho hàm xuất excel Công nợ-Thanh toán theo đơn hàng
	 */
	@Override
	public List<CustomerDebitVO> getListCustomerDebitVOEx(Long shopId, String shortCode, String orderNumber, BigDecimal amount,
			BigDecimal fromAmount, BigDecimal toAmount, Boolean isReveived,
			Date fromDate, Date toDate, Long nvghId, Long nvttId, Long nvbhId,List<Long> lstDebitId) throws BusinessException {	
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (amount != null && isReveived != null) {
				if (amount.compareTo(new BigDecimal(0)) == 0 
						||amount.compareTo(new BigDecimal(0)) > 0 && isReveived == false
						|| amount.compareTo(new BigDecimal(0)) < 0 && isReveived == true) {
					throw new IllegalArgumentException("amount or isReveived is invalid");
				}
			}
			List<CustomerDebitVO> lst = debitDetailDAO.getListCustomerDebitVOEx(shopId, shortCode, orderNumber, fromAmount, toAmount,
					isReveived, fromDate, toDate, nvghId, nvttId, nvbhId,lstDebitId);
			if (amount == null) {
				return lst;
			} else {
				ArrayList<CustomerDebitVO> ret = new ArrayList<CustomerDebitVO>();
				if (amount.compareTo(new BigDecimal(0)) > 0) {
					for (CustomerDebitVO customerDebitVO : lst) {
						ret.add(customerDebitVO);
						if (customerDebitVO.getRemain().compareTo(new BigDecimal(0)) > 0) {
							if (amount.abs().compareTo(customerDebitVO.getRemain().abs()) >= 0) {
								customerDebitVO.setPayAmt(customerDebitVO.getRemain());
								amount = amount.subtract(customerDebitVO.getRemain());
								if (amount.compareTo(new BigDecimal(0)) == 0) {
									break;
								}
							} else {
								customerDebitVO.setPayAmt(amount);
								amount = new BigDecimal(0);
								break;
							}
						}
					}
				} else {
					for (CustomerDebitVO customerDebitVO : lst) {
						ret.add(customerDebitVO);
						if (customerDebitVO.getRemain().compareTo(new BigDecimal(0)) < 0) {
							if (amount.abs().compareTo(customerDebitVO.getRemain().abs()) >= 0) {
								customerDebitVO.setPayAmt(customerDebitVO.getRemain());
								amount = amount.subtract(customerDebitVO.getRemain());
								if (amount.compareTo(new BigDecimal(0)) == 0) {
									break;
								}
							} else {
								customerDebitVO.setPayAmt(amount);
								amount = new BigDecimal(0);
								break;
							}
						}
					}
				}
				return ret;
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateListDebitOfCustomer(List<DebitCustomerParams> lstDebitCustomerParams, PaymentStatus paymentStatus) throws BusinessException {
		PaymentStatus status = null;
		for (DebitCustomerParams params: lstDebitCustomerParams) {
			if (paymentStatus == null) {
				if (params.getPaymentStatus() == null) {
					status = PaymentStatus.PAID;
				} else {
					status = PaymentStatus.parseValue(params.getPaymentStatus());
				}
			} else {
				status = paymentStatus;
			}
			this.updateOneDebitOfCustomer(params.getShopId(), params.getCustomerId(),
					params.getPayReceivedNumber(), params.getDebtPaymentType(), 
					params.getAmount(), params.getLstDebitId(), params.getLstDebitAmt(),
					params.getLstDiscount(), params.getCreateUser(), params.getPayReceivedType(),params.getBankId(),
					params.getLstCustId(), status,
					params.getPayerName(), params.getPayerAddress(), params.getPaymentReason());
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateListCashierIdSaleOder(List<Long> lstFromObject, List<String> lstStaffCode, String user, Long shopId) throws BusinessException {
		//Lay danh sach Sale Oder
		try{
			List<SaleOrder> lstSaleOder = debitDetailDAO.getListSaleOderByListDebitdetailId(lstFromObject);
			if(lstSaleOder!=null && lstSaleOder.size()>0){
				for(SaleOrder item:lstSaleOder){
					for(int i=0; i<lstFromObject.size(); i++){
						if(lstFromObject.get(i).equals(item.getId()) && lstStaffCode.get(i)!=null){
							Staff staffCar = staffDAO.getStaffByCodeAndShopId(lstStaffCode.get(i).trim(), shopId);
							//Cap nhat truong NVTT
							item.setCashier(staffCar);
							item.setUpdateUser(user);
							//item.setUpdateDate(new Date());
							saleOrderDAO.updateSaleOrder(item);
						}
					}
				}
			}
		}catch(Exception ex){
			throw new BusinessException(ex);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateDebitOfCustomer(Long shopId, Long customerId,
			String payReceivedNumber, DebtPaymentType debtPaymentType,
			BigDecimal amount, List<Long> lstDebitId,
			List<BigDecimal> lstDebitAmt, List<BigDecimal> lstDiscount, String createUser,
			PayReceivedType payReceivedType,Long bankId, PaymentStatus paymentStatus)
			throws BusinessException {
		this.updateOneDebitOfCustomer(shopId, customerId, payReceivedNumber, debtPaymentType,
				amount, lstDebitId, lstDebitAmt, lstDiscount, createUser, payReceivedType,bankId, null, paymentStatus,
				null, null, null);
	}
	
	private void updateOneDebitOfCustomer(Long shopId, Long customerId,
			String payReceivedNumber, DebtPaymentType debtPaymentType,
			BigDecimal amount, List<Long> lstDebitId,
			List<BigDecimal> lstDebitAmt, List<BigDecimal> lstDiscount,
			String createUser, PayReceivedType payReceivedType,
			Long bankId, List<Long> lstCustId, PaymentStatus paymentStatus,
			String payerName, String payerAddress, String paymentReason)
			throws BusinessException {
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (debtPaymentType == null) {
				throw new IllegalArgumentException("debtPaymentType is null");
			}
			if (debtPaymentType != DebtPaymentType.REMOVE_SMALL_DEBT) {
				if (StringUtility.isNullOrEmpty(payReceivedNumber)) {
					throw new IllegalArgumentException(
							"payReceivedNumber is null or empty");
				}
			} else {
				payReceivedNumber = "PT";
			}
			if (amount == null && debtPaymentType != DebtPaymentType.REMOVE_SMALL_DEBT) {
				throw new IllegalArgumentException("amount is null");
			}
			if (lstDebitId == null) {
				throw new IllegalArgumentException("lstDebitId is null");
			}
			if (lstDebitAmt == null) {
				throw new IllegalArgumentException("lstDebitAmt is null");
			}
			if (lstDebitId.size() != lstDebitAmt.size()) {
				throw new IllegalArgumentException("lstDebitId.size() != lstDebitAmt.size()");
			}
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			
			Date now = commonDAO.getSysDate();
			//cap nhat cong no Customer
			Customer customer = null;
			if (lstCustId == null || lstCustId.size() == 0) {
				customer = customerDAO.getCustomerById(customerId);
				if (customer == null) {
					throw new IllegalArgumentException("customer is null");
				}
				if (customer.getShop().getId().compareTo(shopId) != 0) {
					throw new IllegalArgumentException("customer.getShop().getId() != shopId");
				}
			}
			
			Debit debit = null;
			BigDecimal debitAmt = null;
			BigDecimal discount = BigDecimal.ZERO;
			SaleOrder so = null;
			//phuongvm - xoa so du no nho : xet amount khac null
			if (DebtPaymentType.REMOVE_SMALL_DEBT.equals(debtPaymentType)) {
				amount = BigDecimal.ZERO;
				for (int i = 0, sz = lstDebitAmt.size(); i < sz; i++) {
					debitAmt = lstDebitAmt.get(i);
					if (debitAmt == null) {
						throw new IllegalArgumentException("debitAmt is null");
					}
					amount = amount.add(debitAmt);
				}
			}
			Date lockDate = shopLockDAO.getNextLockedDay(shop.getId());
			if(lockDate == null){
				lockDate = now;
			}
			
			// cap nhat PayReceived
			PayReceived payReceived = new PayReceived();
			payReceived.setCreateUser(createUser);
			//payReceived.setCustomer(customer);
			payReceived.setCreateDate(lockDate);
			payReceived.setCreateDateSys(now);
			payReceived.setPayReceivedNumber(payReceivedNumber);
			payReceived.setPaymentType(PaymentType.MONEY);
			payReceived.setShop(shop);
			payReceived.setAmount(amount);
			payReceived.setPaymentStatus(paymentStatus);
			payReceived.setPayerName(payerName);
			payReceived.setPayerAddress(payerAddress);
			payReceived.setPaymentReason(paymentReason);
			Bank bank = null;
			if(bankId!=null){
				bank = bankDAO.getBankById(bankId);
			}
			if(bank!=null){
				payReceived.setBank(bank);
			}
			if (payReceivedType != null) {
				payReceived.setType(payReceivedType.getValue());
			} else{
				payReceived.setType(PayReceivedType.THANH_TOAN.getValue());
			}
			
			BigDecimal amount1 = null;
			Integer time = 0;
			if (amount.compareTo(BigDecimal.ZERO) > 0) {
				payReceived.setReceiptType(ReceiptType.RECEIVED);
				payReceived = payReceivedDAO.createPayReceived(payReceived);
				if (debtPaymentType != null && debtPaymentType == DebtPaymentType.REMOVE_SMALL_DEBT) {
					payReceived.setPayReceivedNumber(getPTNumber(payReceived.getId(),"PT"));
				}
				// ////////
				ArrayList<PaymentDetail> lstPaymentDetail = new ArrayList<PaymentDetail>();
				for (int i = 0; i < lstDebitId.size(); i++) {
					Long debitId = lstDebitId.get(i);
					if (debitId == null) {
						throw new IllegalArgumentException("debitId is null");
					}
					debitAmt = lstDebitAmt.get(i);
					if (lstDiscount != null) {
						discount = lstDiscount.get(i);
					} else {
						discount = BigDecimal.ZERO;
					}
					if (debitAmt == null) {
						throw new IllegalArgumentException("debitAmt is null");
					}
					amount1 = discount.add(debitAmt);
					if (amount1.compareTo(BigDecimal.ZERO) <= 0) {
						throw new IllegalArgumentException("debitAmt.compareTo(new BigDecimal(0)) <= 0");
					}
					// //////////////cap nhat Debit
					DebitDetail debitDetail = debitDetailDAO.getDebitDetailByIdForUpdate(debitId);
					if (debitDetail == null) {
						throw new IllegalArgumentException("debit is null");
					}
					if (debitDetail.getRemain() == null || debitDetail.getRemain().compareTo(BigDecimal.ZERO) < 0) {
						throw new IllegalArgumentException("debit.getRemain() == null || debit.getRemain().compareTo(new BigDecimal(0)) < 0");
					}
					if(debitDetail.getRemain().compareTo(BigDecimal.ZERO) == 0){
						throw new BusinessException(ExceptionCode.DEBIT_DETAIL_NOT_REMAIN);
					}
					if (debitDetail.getRemain().compareTo(amount1) < 0) {
						throw new IllegalArgumentException("debit.getRemain().compareTo(debitAmt) < 0");
					}
					/*if (debitDetail.getAmount().compareTo(amount1) < 0) {
						throw new IllegalArgumentException("debit.getAmount().compareTo(debitAmt) < 0");
					}*/
					if (debitDetail.getTotal() == null || debitDetail.getTotal().compareTo(amount1) < 0) {
						throw new IllegalArgumentException("debit.getTotal().compareTo(debitAmt) < 0");
					}
					if (debitDetail.getFromObjectId() != null) {
						so = commonDAO.getEntityById(SaleOrder.class, debitDetail.getFromObjectId());
					}
					if (PaymentStatus.PAID.equals(paymentStatus)) {
						debitDetail.setTotalPay((debitDetail.getTotalPay() != null ? debitDetail.getTotalPay() : BigDecimal.ZERO).add(debitAmt));
						debitDetail.setDiscountAmount((debitDetail.getDiscountAmount() != null ? debitDetail.getDiscountAmount() : BigDecimal.ZERO).add(discount));
						debitDetail.setRemain((debitDetail.getRemain() != null ? debitDetail.getRemain() : BigDecimal.ZERO).subtract(amount1));
						debitDetail.setUpdateDate(now);
						debitDetail.setUpdateUser(createUser);
						debitDetailDAO.updateDebitDetail(debitDetail);
						
						if (lstCustId != null && lstCustId.size() > i) {
							debit = debitDAO.getDebitForUpdate(lstCustId.get(i), DebitOwnerType.CUSTOMER);
						} else {
							debit = debitDAO.getDebitForUpdate(customerId, DebitOwnerType.CUSTOMER);
						}
						if (debit == null) {
							throw new IllegalArgumentException("debit is null");
						}
						debit.setTotalPay((debit.getTotalPay() != null ? debit.getTotalPay() : BigDecimal.ZERO).add(debitAmt));
						debit.setTotalDebit((debit.getTotalDebit() != null ? debit.getTotalDebit() : BigDecimal.ZERO).subtract(amount1));
						debit.setTotalDiscount((debit.getTotalDiscount() != null ? debit.getTotalDiscount() : BigDecimal.ZERO).add(discount));
						debitDAO.updateDebit(debit);
					}
					
					// //////////////tao PaymentDetail
					time = paymentDetailDAO.getTimeOfPay(debitDetail.getId()); // lacnv1
					PaymentDetail paymentDetail = new PaymentDetail();
					paymentDetail.setDebit(debitDetail);
					paymentDetail.setPaymentType(debtPaymentType);
					paymentDetail.setAmount(debitAmt);
					paymentDetail.setPayReceived(payReceived);
					paymentDetail.setCreateUser(createUser);
					paymentDetail.setPayDate(lockDate); //tungtt21
					paymentDetail.setCreateDate(now); //tungtt21
					paymentDetail.setDiscount(discount);
					paymentDetail.setTime(time + 1); // lacnv1
					paymentDetail.setPaymentStatus(paymentStatus);
					if (so != null) {
						paymentDetail.setStaff(so.getStaff());
					}
					if (lstCustId != null && lstCustId.size() > i) {
						customer = customerDAO.getCustomerById(lstCustId.get(i));
					}
					paymentDetail.setCustomer(customer);
					if (customer != null) {
						paymentDetail.setShop(customer.getShop());
					}
					// ////
					lstPaymentDetail.add(paymentDetail);
					//sum = sum.add(debitAmt);
				}
				payReceivedDAO.updatePayReceivedWithoutUpdateDate(payReceived);
				// lacnv1 - debit_detail
				if (PaymentStatus.PAID.equals(paymentStatus)) {
					PayReceivedTemp p = payReceivedDAO.clonePayReceivedToTemp(payReceived);
					paymentDetailDAO.createListPaymentDetailAndCloneToTemp(lstPaymentDetail, p);
				} else {
					paymentDetailDAO.createListPaymentDetail(lstPaymentDetail);
				}
				// --
			} else {
				payReceived.setReceiptType(ReceiptType.PAID);
				payReceived = payReceivedDAO.createPayReceived(payReceived);
				if (debtPaymentType != null && debtPaymentType == DebtPaymentType.REMOVE_SMALL_DEBT) {
					payReceived.setPayReceivedNumber(getPTNumber(payReceived.getId(),"PC"));
				}
				// ////////
				ArrayList<PaymentDetail> lstPaymentDetail = new ArrayList<PaymentDetail>();
				for (int i = 0; i < lstDebitId.size(); i++) {
					Long debitId = lstDebitId.get(i);
					if (debitId == null) {
						throw new IllegalArgumentException("debitId is null");
					}
					debitAmt = lstDebitAmt.get(i);
					if (debitAmt == null) {
						throw new IllegalArgumentException("debitAmt is null");
					}
					if (lstDiscount != null) {
						discount = lstDiscount.get(i);
					} else {
						discount = BigDecimal.ZERO;
					}
					amount1 = discount.add(debitAmt);
					if (amount1.compareTo(new BigDecimal(0)) >= 0) {
						throw new IllegalArgumentException("debitAmt.compareTo(new BigDecimal(0)) >= 0");
					}
					// //////////////cap nhat Debit
					DebitDetail debitDetail = debitDetailDAO.getDebitDetailByIdForUpdate(debitId);
					if (debitDetail == null) {
						throw new IllegalArgumentException("debit is null");
					}
					if (debitDetail.getRemain() == null || debitDetail.getRemain().compareTo(BigDecimal.ZERO) > 0) {
						throw new IllegalArgumentException("debit.getRemain() == null || debit.getRemain().compareTo(new BigDecimal(0)) > 0");
					}
					
					if(debitDetail.getRemain().compareTo(BigDecimal.ZERO) == 0){
						throw new BusinessException(ExceptionCode.DEBIT_DETAIL_NOT_REMAIN);
					}
					if (debitDetail.getRemain().compareTo(amount1) > 0) {
						throw new IllegalArgumentException("debit.getRemain().compareTo(debitAmt) > 0");
					}
					/*if (debitDetail.getAmount().compareTo(amount1) > 0) {
						throw new IllegalArgumentException("debit.getAmount().compareTo(debitAmt) > 0");
					}*/
					if (debitDetail.getTotal() == null || debitDetail.getTotal().compareTo(amount1) > 0) {
						throw new IllegalArgumentException("debit.getTotal().compareTo(debitAmt) > 0");
					}
					if (debitDetail.getFromObjectId() != null) {
						so = commonDAO.getEntityById(SaleOrder.class, debitDetail.getFromObjectId());
					}
					if (PaymentStatus.PAID.equals(paymentStatus)) {
						debitDetail.setTotalPay((debitDetail.getTotalPay() != null ? debitDetail.getTotalPay() : BigDecimal.ZERO).add(debitAmt));
						debitDetail.setDiscountAmount((debitDetail.getDiscountAmount() != null ? debitDetail.getDiscountAmount() : BigDecimal.ZERO).add(discount));
						debitDetail.setRemain((debitDetail.getRemain() != null ? debitDetail.getRemain() : BigDecimal.ZERO).subtract(amount1));
						debitDetail.setUpdateDate(now);
						debitDetail.setUpdateUser(createUser);
						debitDetailDAO.updateDebitDetail(debitDetail);
						
						if (lstCustId != null && lstCustId.size() > i) {
							debit = debitDAO.getDebitForUpdate(lstCustId.get(i), DebitOwnerType.CUSTOMER);
						} else {
							debit = debitDAO.getDebitForUpdate(customerId, DebitOwnerType.CUSTOMER);
						}
						if (debit == null) {
							throw new IllegalArgumentException("debit is null");
						}
						debit.setTotalPay((debit.getTotalPay() != null ? debit.getTotalPay() : BigDecimal.ZERO).add(debitAmt));
						debit.setTotalDebit((debit.getTotalDebit() != null ? debit.getTotalDebit() : BigDecimal.ZERO).subtract(amount1));
						debit.setTotalDiscount((debit.getTotalDiscount() != null ? debit.getTotalDiscount() : BigDecimal.ZERO).add(discount));
						debitDAO.updateDebit(debit);
					}
					
					// //////////////tao PaymentDetail
					time = paymentDetailDAO.getTimeOfPay(debitDetail.getId()); // lacnv1
					PaymentDetail paymentDetail = new PaymentDetail();
					paymentDetail.setDebit(debitDetail);
					paymentDetail.setPaymentType(debtPaymentType);
					paymentDetail.setAmount(debitAmt);
					paymentDetail.setPayReceived(payReceived);
					paymentDetail.setCreateUser(createUser);
					paymentDetail.setPayDate(lockDate);
					paymentDetail.setCreateDate(now);
					paymentDetail.setDiscount(discount);
					paymentDetail.setTime(time + 1); // lacnv1
					paymentDetail.setPaymentStatus(paymentStatus);
					if (so != null) {
						paymentDetail.setStaff(so.getStaff());
					}
					if (lstCustId != null && lstCustId.size() > i) { // lacnv1
						customer = customerDAO.getCustomerById(lstCustId.get(i));
					}
					paymentDetail.setCustomer(customer);
					if (customer != null) {
						paymentDetail.setShop(customer.getShop());
					}
					// ////
					lstPaymentDetail.add(paymentDetail);
					//sum = sum.add(debitAmt);
				}
				payReceivedDAO.updatePayReceivedWithoutUpdateDate(payReceived);
				// lacnv1 - debit_detail
				if (PaymentStatus.PAID.equals(paymentStatus)) {
					PayReceivedTemp p = payReceivedDAO.clonePayReceivedToTemp(payReceived);
					paymentDetailDAO.createListPaymentDetailAndCloneToTemp(lstPaymentDetail, p);
				} else {
					paymentDetailDAO.createListPaymentDetail(lstPaymentDetail);
				}
				// --
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	private String getPTNumber(Long id,String prefix) {
		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		String idText = "000000000000" + id.toString();
		return prefix + idText.substring(idText.length() - 12);
	}

	@Override
	public List<RptDebtOfCustomerLv01VO> getDataForReportDebtOfCustomer(
			Long shopId, List<Long> customerIds, Date reportDate)
			throws BusinessException {
		try {
			List<RptDebtOfCustomerDataVO> listData = debitDetailDAO
					.getDataForReportDebtOfCustomer(shopId, customerIds,
							reportDate);

			if (null != listData && listData.size() >= 0) {
				return RptDebtOfCustomerDataVO.groupByCustomerCode(listData);
			} else {
				return null;
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public String getPayReceivedStringSugguest(Date lockDate) throws BusinessException {
		try {
			return payReceivedDAO.getPayReceivedStringSugguest(lockDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public BigDecimal getCustomerTotalDebitDetail(Long shopId, String staffCode,
			String custName, String shortCode, String orderNumber,
			BigDecimal fromAmount, BigDecimal toAmount, Boolean isReveived, Long deliveryId, Long cashierId, Date fromDate, Date toDate)
			throws BusinessException {
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			return debitDetailDAO.getCustomerTotalDebitDetail(shopId, staffCode, custName,
					shortCode, orderNumber, fromAmount, toAmount, isReveived, deliveryId, cashierId, fromDate, toDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<DebitCustomerVO> getDebitCustomer(Long shopId, Long customerId,
			String customerCode, String customerName, Date fromDate, Date toDate, DebitPaymentType type)
			throws BusinessException {
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			
			return this.debitDAO.getDebitCustomer(shopId, customerId,
					customerCode, customerName, fromDate, toDate, type);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public DebitDetail getDebitCustomerRemain(Long customerId)
			throws BusinessException {
		try {
			if (customerId == null) {
				throw new IllegalArgumentException("customerId is null");
			}

			return this.debitDAO.getLastDebitDetailHaveRemain(customerId,DebitOwnerType.CUSTOMER);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public BigDecimal getTotalDebit(Long ownerId, DebitOwnerType ownerType) throws BusinessException {
		try {
			if (ownerType == null) {
				throw new IllegalArgumentException("ownerType is null");
			}
			return debitDAO.getTotalDebit(ownerId, ownerType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public ObjectVO<DebitCustomerVO> getListDebitCustomerVO(DebitFilter<DebitCustomerVO> filter) throws BusinessException {
		try {
			List<DebitCustomerVO> lst = debitDAO.getListDebitCustomerVO(filter);
			ObjectVO<DebitCustomerVO> objVO = new ObjectVO<DebitCustomerVO>();
			objVO.setkPaging(filter.getPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<DebitShopVO> getDebitVNMOfShop(Long shopId, Long typeInvoice) throws BusinessException {
		try {
			return debitDetailDAO.getDebitVNMOfShop(shopId, typeInvoice);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public List<DebitShopVO> getDebitVNMOfShopAndId(Long shopId, List<Long> listDebitDetailId) throws BusinessException {
		try {
			return debitDetailDAO.getDebitVNMOfShopAndId(shopId, listDebitDetailId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createDebitIncrement(Long shopId, String number, BigDecimal amount, Customer customer, String reason, LogInfoVO logInfo)
			throws BusinessException {
		try {
			if (StringUtility.isNullOrEmpty(number) || amount == null ||
					shopId == null || logInfo == null) {
				throw new IllegalArgumentException("invalid parameters");
			}
			Debit debit = null;
			Integer debitDetailType = null;
			Shop shop = shopDAO.getShopById(shopId);
			Integer debitLimit = null;
			if (customer == null || customer.getId() == null) { // neu id KH la null -> tang no NPP doi voi cong ty
				if (shop.getDebitDateLimit() != null) {
					debitLimit = shop.getDebitDateLimit().intValue();
				}
				debit = debitDAO.getDebitForUpdate(shopId, DebitOwnerType.SHOP);
				if (amount.signum() == 1) {
					debitDetailType = DebitDetailType.TANG_NO_VNM.getValue();
				} else {
					debitDetailType = DebitDetailType.GIAM_NO_VNM.getValue();
				}
			} else {
				debitLimit = customer.getMaxDebitDate();
				debit = debitDAO.getDebitForUpdate(customer.getId(), DebitOwnerType.CUSTOMER);
				if (amount.signum() == 1) {
					debitDetailType = DebitDetailType.TANG_NO_KH.getValue();
				} else {
					debitDetailType = DebitDetailType.GIAM_NO_KH.getValue();
				}
			}
			if (debit == null) {
				throw new BusinessException("debit is null");
			}
			
			Date now = commonDAO.getSysDate();
			Date lockDate = shopLockDAO.getApplicationDate(shopId);
			
			// them debit_detail
			DebitDetail debitDetail = new DebitDetail();
			debitDetail.setCreateDate(now);
			debitDetail.setDebitDate(lockDate);
			debitDetail.setAmount(amount);
			debitDetail.setDiscount(BigDecimal.ZERO);
			debitDetail.setTotal(amount);
			debitDetail.setTotalPay(BigDecimal.ZERO);
			debitDetail.setRemain(amount);
			debitDetail.setCreateUser(logInfo.getStaffCode().toUpperCase());
			debitDetail.setDebit(debit);
			debitDetail.setFromObjectNumber(number.toUpperCase());
			debitDetail.setType(debitDetailType);
			debitDetail.setReason(reason);
			debitDetail.setCustomer(customer);
			debitDetail.setShop(shop);
			if (debitLimit != null && debitLimit > 0) {
				debitDetail.setDebitDateLimit(debitLimit);
			}
			debitDetailDAO.createDebitDetail(debitDetail);
			// cap nhat debit
			BigDecimal t = amount.add(debit.getTotalAmount());
			debit.setTotalAmount(t);
			t = amount.add(debit.getTotalDebit());
			debit.setTotalDebit(t);
			
			debitDAO.updateDebit(debit);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author tungtt21
	 * Lay no NPP voi VNM hien tai
	 */
	@Override
	public BigDecimal getCurrentDebit(Long objectId, DebitOwnerType ownerType, DebitType debitType)
			throws BusinessException {
		try {
			if (objectId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			return debitDAO.getCurrentDebit(objectId, ownerType, debitType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createListDebitIncrement(List<DebitObjectVO> lstDebit, LogInfoVO logInfo) throws BusinessException {
		if (lstDebit == null || lstDebit.size() == 0) {
			return;
		}
		DebitObjectVO vo = null;
		for (int i = 0, sz = lstDebit.size(); i < sz; i++) {
			vo = lstDebit.get(i);
			createDebitIncrement(vo.getShopId(), vo.getDebitNumber(), vo.getAmount(), vo.getCustomer(), vo.getReason(), logInfo);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public boolean existsDebitNumber(long shopId, String number) throws BusinessException {
		try {
			return debitDetailDAO.existsDebitNumber(shopId, number);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	/**
	 * @author phuongvm
	 */
	@Override
	public List<Bank> getListBankByShop(Long shopId, ActiveType status) throws BusinessException {	
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			return bankDAO.getListBankByShop(shopId, status);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public Bank getBankByCodeAndShop(Long shopId,String Code) throws BusinessException {	
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			return bankDAO.getBankByCodeAndShop(shopId,Code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public PayReceived exportPayment(DebitCustomerParams payment, LogInfoVO logInfo) throws BusinessException {
		try {
			if (payment == null) {
				throw new IllegalArgumentException("payment null");
			}
			if (logInfo == null) {
				throw new IllegalArgumentException("logInfo null");
			}
			if (payment.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			if (payment.getDebtPaymentType() == null) {
				throw new IllegalArgumentException("debtPaymentType is null");
			}
			DebtPaymentType debtPaymentType = payment.getDebtPaymentType();
			if (StringUtility.isNullOrEmpty(payment.getPayReceivedNumber())) {
				throw new IllegalArgumentException("payReceivedNumber is null or empty");
			}
			
			if (payment.getLstDebitId() == null) {
				throw new IllegalArgumentException("lstDebitId is null");
			}
			if (payment.getLstDebitAmt() == null) {
				throw new IllegalArgumentException("lstDebitAmt is null");
			}
			if (payment.getAmount() == null) {
				throw new IllegalArgumentException("amount is null");
			}
			List<Long> lstDebitId = payment.getLstDebitId();
			List<BigDecimal> lstDebitAmt = payment.getLstDebitAmt();
			if (lstDebitId.size() != lstDebitAmt.size()) {
				throw new IllegalArgumentException("lstDebitId.size() != lstDebitAmt.size()");
			}
			Shop shop = shopDAO.getShopById(payment.getShopId());
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			
			Date now = commonDAO.getSysDate();
			//cap nhat cong no Customer
			Customer customer = null;
			List<Long> lstCustId = payment.getLstCustId();
			Long customerId = payment.getCustomerId();
			if (lstCustId == null || lstCustId.size() == 0) {
				customer = customerDAO.getCustomerById(customerId);
				if (customer == null) {
					throw new IllegalArgumentException("customer is null");
				}
				if (customer.getShop().getId().compareTo(shop.getId()) != 0) {
					throw new IllegalArgumentException("customer.getShop().getId() != shopId");
				}
			}
			
			BigDecimal debitAmt = null;
			BigDecimal discount = BigDecimal.ZERO;
			SaleOrder so = null;
			
			Date lockDate = shopLockDAO.getNextLockedDay(shop.getId());
			if(lockDate == null){
				lockDate = now;
			}
			
			// cap nhat PayReceived
			PayReceived payReceived = new PayReceived();
			payReceived.setCreateUser(logInfo.getStaffCode());
			payReceived.setCreateDate(lockDate);
			payReceived.setCreateDateSys(now);
			payReceived.setPayReceivedNumber(payment.getPayReceivedNumber());
			payReceived.setPaymentType(PaymentType.MONEY);
			payReceived.setShop(shop);
			payReceived.setAmount(payment.getAmount());
			payReceived.setPaymentStatus(PaymentStatus.NOT_PAID_YET);
			payReceived.setPayerName(payment.getPayerName());
			payReceived.setPayerAddress(payment.getPayerAddress());
			payReceived.setPaymentReason(payment.getPaymentReason());
			Bank bank = null;
			if (payment.getBankId() != null) {
				bank = bankDAO.getBankById(payment.getBankId());
			}
			if (bank != null) {
				payReceived.setBank(bank);
			}
			payReceived.setType(PayReceivedType.THANH_TOAN.getValue());
			
			BigDecimal amount1 = null;
			Integer time = 0;
			if (payment.getAmount().signum() > 0) {
				payReceived.setReceiptType(ReceiptType.RECEIVED);
			} else {
				payReceived.setReceiptType(ReceiptType.PAID);
			}
			payReceived = payReceivedDAO.createPayReceived(payReceived);
			
			List<BigDecimal> lstDiscount = payment.getLstDiscount();
			
			List<PaymentDetail> lstPaymentDetail = new ArrayList<PaymentDetail>();
			DebitDetail debitDetail = null;
			for (int i = 0; i < lstDebitId.size(); i++) {
				Long debitId = lstDebitId.get(i);
				if (debitId == null) {
					throw new IllegalArgumentException("debitId is null");
				}
				debitAmt = lstDebitAmt.get(i);
				if (lstDiscount != null) {
					discount = lstDiscount.get(i);
				} else {
					discount = BigDecimal.ZERO;
				}
				if (debitAmt == null) {
					throw new IllegalArgumentException("debitAmt is null");
				}
				amount1 = discount.add(debitAmt);
				if (payment.getAmount().signum() > 0 && amount1.signum() < 1) {
					throw new IllegalArgumentException("debitAmt.compareTo(new BigDecimal(0)) <= 0");
				} else if (payment.getAmount().signum() < 0 && amount1.signum() > -1) {
					throw new IllegalArgumentException("debitAmt.compareTo(new BigDecimal(0)) >= 0");
				}
				// //////////////cap nhat Debit
				debitDetail = debitDetailDAO.getDebitDetailByIdForUpdate(debitId);
				if (debitDetail == null) {
					throw new IllegalArgumentException("debit is null");
				}
				if(debitDetail.getRemain().signum() == 0){
					throw new BusinessException(ExceptionCode.DEBIT_DETAIL_NOT_REMAIN);
				}
				if (payment.getAmount().signum() > 0) { // phieu thu
					if (debitDetail.getRemain() == null || debitDetail.getRemain().signum() < 0) {
						throw new IllegalArgumentException("debit.getRemain() == null || debit.getRemain().compareTo(new BigDecimal(0)) < 0");
					}
					if (debitDetail.getRemain().compareTo(amount1) < 0) {
						throw new IllegalArgumentException("debit.getRemain().compareTo(debitAmt) < 0");
					}
					if (debitDetail.getTotal() == null || debitDetail.getTotal().compareTo(amount1) < 0) {
						throw new IllegalArgumentException("debit.getTotal().compareTo(debitAmt) < 0");
					}
				} else { // phieu chi
					if (debitDetail.getRemain() == null || debitDetail.getRemain().signum() > 0) {
						throw new IllegalArgumentException("debit.getRemain() == null || debit.getRemain().compareTo(new BigDecimal(0)) > 0");
					}
					if (debitDetail.getRemain().compareTo(amount1) > 0) {
						throw new IllegalArgumentException("debit.getRemain().compareTo(debitAmt) > 0");
					}
					if (debitDetail.getTotal() == null || debitDetail.getTotal().compareTo(amount1) > 0) {
						throw new IllegalArgumentException("debit.getTotal().compareTo(debitAmt) > 0");
					}
				}
				if (debitDetail.getFromObjectId() != null) {
					so = commonDAO.getEntityById(SaleOrder.class, debitDetail.getFromObjectId());
				}
				
				// //////////////tao PaymentDetail
				time = paymentDetailDAO.getTimeOfPay(debitDetail.getId()); // lacnv1
				PaymentDetail paymentDetail = new PaymentDetail();
				paymentDetail.setDebit(debitDetail);
				paymentDetail.setPaymentType(debtPaymentType);
				paymentDetail.setAmount(debitAmt);
				paymentDetail.setPayReceived(payReceived);
				paymentDetail.setCreateUser(logInfo.getStaffCode());
				paymentDetail.setPayDate(lockDate); //tungtt21
				paymentDetail.setCreateDate(now); //tungtt21
				paymentDetail.setDiscount(discount);
				paymentDetail.setTime(time + 1); // lacnv1
				paymentDetail.setPaymentStatus(PaymentStatus.NOT_PAID_YET);
				if (so != null) {
					paymentDetail.setStaff(so.getStaff());
				}
				if (lstCustId != null && lstCustId.size() > i) {
					customer = customerDAO.getCustomerById(lstCustId.get(i));
				}
				paymentDetail.setCustomer(customer);
				if (customer != null) {
					paymentDetail.setShop(customer.getShop());
				}
				
				lstPaymentDetail.add(paymentDetail);
			}
			payReceivedDAO.updatePayReceivedWithoutUpdateDate(payReceived);
			paymentDetailDAO.createListPaymentDetail(lstPaymentDetail);
			
			if (payment.getLstFromObjectId() != null && payment.getLstCashierCode() != null
					&& payment.getLstFromObjectId().size() > 0) {
				List<Long> lstFromObjectId = payment.getLstFromObjectId();
				List<String> lstCashierCode = payment.getLstCashierCode();
				List<SaleOrder> lstSaleOder = debitDetailDAO.getListSaleOderByListDebitdetailId(lstFromObjectId);
				if (lstSaleOder != null && lstSaleOder.size() > 0) {
					Staff staffCar = null;
					int idx = -1;
					int sz = lstCashierCode.size();
					for (SaleOrder item : lstSaleOder) {
						idx = lstFromObjectId.indexOf(item.getId());
						if (idx > -1 && idx < sz && lstCashierCode.get(idx) != null) {
							staffCar = staffDAO.getStaffByCodeAndShopId(lstCashierCode.get(idx).trim(), shop.getId());
							// Cap nhat truong NVTT
							item.setCashier(staffCar);
							item.setUpdateUser(logInfo.getStaffCode());
							// item.setUpdateDate(new Date());
							saleOrderDAO.updateSaleOrder(item);
						}
					}
				}
			}
			
			return payReceived;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public BigDecimal getPaymentAmountOfDebitDetail(long debitDetailId, PaymentStatus paymentStatus) throws BusinessException {
		try {
			return paymentDetailDAO.getPaymentAmountOfDebitDetail(debitDetailId, paymentStatus);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public PayReceivedVO getPayReceivedVOForExport(long payId) throws BusinessException {
		try {
			return payReceivedDAO.getPayReceivedVOForExport(payId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<CustomerDebitVO> getCustomerDebitOfPayReceived(long shopId, long payId) throws BusinessException {
		try {
			List<CustomerDebitVO> lst = payReceivedDAO.getCustomerDebitOfPayReceived(shopId, payId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public DebitDetail getDebitDetailByObjectNumber(long shopId, String fromObjectNumber, Date pDate) throws BusinessException {
		try {
			return debitDetailDAO.getDebitDetailByObjectNumber(shopId, fromObjectNumber, pDate);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateCashierForListDebitDetail(DebitFilter<CustomerDebitVO> filter) throws BusinessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null");
		}
		if (filter.getLstDebitDetailId() == null) {
			throw new IllegalArgumentException("filter.getLstDebitDetailId() is null");
		}
		try {
			List<Long> lstSaleOrderId = new ArrayList<Long>();
			Date sysDate = commonDAO.getSysDate();
			String userName = filter.getUserName();
			Staff cashierstaff = filter.getCashierStaff();
			SoFilter soFilter = new SoFilter();
			soFilter.setShopId(filter.getShopId());
			soFilter.setLstId(filter.getLstDebitDetailId());
			List<DebitDetail> lstDebitDetails = debitDetailDAO.getListDebitDetailByFilter(soFilter);
			if (lstDebitDetails != null) {
				for (int i = 0, n = lstDebitDetails.size(); i < n; i++) {
					DebitDetail debitDetail = lstDebitDetails.get(i);
					debitDetail.setCashier(cashierstaff);
					debitDetail.setUpdateDate(sysDate);
					debitDetail.setUpdateUser(userName);
					if (debitDetail.getFromObjectId() != null) {
						lstSaleOrderId.add(debitDetail.getFromObjectId());
					}
				}
				debitDetailDAO.updateDebitDetail(lstDebitDetails);
				if (!lstSaleOrderId.isEmpty()) {
					List<SaleOrder> lstSaleOrders = saleOrderDAO.getListSaleOrder(lstSaleOrderId);
					if (lstSaleOrders != null && !lstSaleOrders.isEmpty()) {
						for (int i = 0, sz = lstSaleOrders.size(); i < sz; i++) {
							SaleOrder saleOrder = lstSaleOrders.get(i);
							if (saleOrder != null) {
								saleOrder.setCashier(cashierstaff);
								saleOrder.setUpdateDate(sysDate);
								saleOrder.setUpdateUser(userName);
								commonDAO.updateEntity(saleOrder);
							}
						}
					}
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateListSaleOrderInDebitDetail(DebitFilter<CustomerDebitVO> filter) throws BusinessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null");
		}
		if (filter.getLstSaleOrderId() == null) {
			throw new IllegalArgumentException("filter.getLstSaleOrderId() is null");
		}
		try {
			Date sysDate = commonDAO.getSysDate();
			String userName = filter.getUserName();
			if (filter.getCashierStaff() != null) {
				Staff cashierstaff = filter.getCashierStaff();
				List<SaleOrder> lstSaleOrders = saleOrderDAO.getListSaleOrder(filter.getLstSaleOrderId());

				if (lstSaleOrders != null && !lstSaleOrders.isEmpty()) {
					for (int i = 0, sz = lstSaleOrders.size(); i < sz; i++) {
						SaleOrder saleOrder = lstSaleOrders.get(i);
						if (saleOrder != null) {
							saleOrder.setCashier(cashierstaff);
							saleOrder.setUpdateDate(sysDate);
							saleOrder.setUpdateUser(userName);
							commonDAO.updateEntity(saleOrder);
						}
					}
				}
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
}

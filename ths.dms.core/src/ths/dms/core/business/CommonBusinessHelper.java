/**
 * Copyright (c) 2016 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.ConstantManager;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.memcached.Configuration;

/**
 * Cac xu ly nghiep vu chung
 * @author tuannd20
 * @since 13/01/2016
 */
class CommonBusinessHelper {
	@Autowired
	ShopDAO shopDAO;
	
	/**
	 * tao stock total entity cho NPP
	 * @author tuannd20
	 * @param shop
	 * @param product
	 * @param warehouse
	 * @param sysdate
	 * @param logInfo
	 * @return
	 * @since 13/01/2016
	 */
	public StockTotal createStockTotalEntity(Shop shop, Product product, Warehouse warehouse, Date sysdate, LogInfoVO logInfo) {
		StockTotal stockTotal = new StockTotal();
		stockTotal.setAvailableQuantity(0);
		stockTotal.setCreateDate(sysdate);
		stockTotal.setCreateUser(logInfo.getStaffCode());
		stockTotal.setObjectId(shop.getId());
		stockTotal.setObjectType(StockObjectType.SHOP);
		stockTotal.setProduct(product);
		stockTotal.setQuantity(0);
		stockTotal.setWarehouse(warehouse);
		stockTotal.setApprovedQuantity(0);
		stockTotal.setStatus(ActiveType.RUNNING);
		stockTotal.setShop(shop);
		return stockTotal;
	}
	
	/**
	 * lay danh sach tat ca cac NPP
	 * @author tuannd20
	 * @return
	 * @throws DataAccessException
	 * @since 13/01/2016
	 */
	public List<Shop> getAllDistributorShop() throws DataAccessException {
		return shopDAO.getAllDistributorShop();
	}
}

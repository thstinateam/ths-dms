package ths.dms.core.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ToolFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ToolVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.SuperviseDeviceDAO;

public class SuperviserDeviceMgrImpl implements SuperviserDeviceMgr {


	@Autowired
	SuperviseDeviceDAO superviseDeviceDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public List<ToolVO> getListShopTool(Long parentShopId, Integer typeSup)
			throws BusinessException {
		try {
			List<ToolVO> list=superviseDeviceDAO.getListShopTool(parentShopId,typeSup);
			return list;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ToolVO> getListTool(Long parentShopId, Integer typeSup)
			throws BusinessException {
		try {
			List<ToolVO> list=superviseDeviceDAO.getListTool(parentShopId, typeSup);
			return list;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ToolVO> getListShopToolOneNode(Long parentShopId,Integer typeSup) throws BusinessException {
		try {
			List<ToolVO> list=superviseDeviceDAO.getListShopToolOneNode(parentShopId,typeSup);
			return list;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Shop> getListAncestor(Long parentShopId, String shopCode,
			String shopName) throws BusinessException {
		try {
			List<Shop> list=superviseDeviceDAO.getListAncestor(parentShopId,shopCode,shopName);
			return list;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ToolVO> getListToolKP(ToolFilter filter) throws BusinessException {
		try {
			List<ToolVO> lst = superviseDeviceDAO.getListToolKP(filter);
			ObjectVO<ToolVO> vo = new ObjectVO<ToolVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ToolVO> getListChip(ToolFilter filter) throws BusinessException {
		try {
			List<ToolVO> lst = superviseDeviceDAO.getListChip(filter);
			ObjectVO<ToolVO> vo = new ObjectVO<ToolVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ToolVO> getListChipForCustomer(ToolFilter filter) throws BusinessException {
		try {
			List<ToolVO> lst = superviseDeviceDAO.getListChipForCustomer(filter);
			ObjectVO<ToolVO> vo = new ObjectVO<ToolVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateChip(Long chipId,Float minTemp,Float maxTemp) throws BusinessException {
		try {
			superviseDeviceDAO.updateChip(chipId,minTemp,maxTemp);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateConfig(int tempConfig, int positionConfig) throws BusinessException {
		try {
			superviseDeviceDAO.updateConfig(tempConfig,positionConfig);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateConfigDistance(int distanceConfig) throws BusinessException {
		try {
			superviseDeviceDAO.updateConfigDistance(distanceConfig);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ToolVO> getBCTemperature(ToolFilter filter) throws BusinessException {
		try {
			List<ToolVO> list=superviseDeviceDAO.getBCTemperature(filter);
			return list;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ToolVO> getBCPosition(ToolFilter filter) throws BusinessException {
		try {
			List<ToolVO> list=superviseDeviceDAO.getBCPosition(filter);
			return list;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
}

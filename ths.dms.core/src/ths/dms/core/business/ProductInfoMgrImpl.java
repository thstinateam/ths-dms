package ths.dms.core.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.ProductInfoMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductInfoMapType;
import ths.dms.core.entities.enumtype.ProductInfoObjectType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.CatalogFilter;
import ths.dms.core.entities.vo.CatalogVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductInfoVOEx;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.ProductInfoDAO;
import ths.dms.core.dao.ProductInfoMapDAO;

public class ProductInfoMgrImpl implements ProductInfoMgr {

	@Autowired
	ProductInfoDAO productInfoDAO;

	@Autowired
	ProductInfoMapDAO productInfoMapDAO;

	@Autowired
	private CommonDAO commonDAO;

	@Override
	public ProductInfo createProductInfo(ProductInfo productInfo, LogInfoVO logInfo) throws BusinessException {
		try {
			return productInfoDAO.createProductInfo(productInfo, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void updateProductInfo(ProductInfo productInfo, LogInfoVO logInfo) throws BusinessException {
		try {
			productInfoDAO.updateProductInfo(productInfo, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteProductInfo(ProductInfo productInfo, LogInfoVO logInfo) throws BusinessException {
		try {
			if (productInfo != null) {
				if (ProductType.SUB_CAT.getValue().equals(productInfo.getType())) {
					ProductInfoMap pdInfoMap = productInfoMapDAO.getProductInfoMapByToId(productInfo.getId(), ProductInfoMapType.SUB_CAT);
					if (pdInfoMap != null) {
						productInfoMapDAO.deleteProductInfoMap(pdInfoMap, logInfo);
					}
				}
				productInfoDAO.deleteProductInfo(productInfo, logInfo);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ProductInfo getProductInfoById(Long id) throws BusinessException {
		try {
			return id == null ? null : productInfoDAO.getProductInfoById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/**
	 * Lấy ProductInfo code : Ma can tim kiem type : Loai can tim kiem catCode :
	 * Chi dung cho ma nganh hang con hoac nganh hang con phu ( Con lai de null)
	 * 
	 * @author thongnm
	 */
	@Override
	public ProductInfo getProductInfoByCode(String code, ProductType type, String catCode, Boolean isActiveTypeRunning) throws BusinessException {
		try {
			return productInfoDAO.getProductInfoByCode(code, type, catCode, isActiveTypeRunning);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ProductInfo> getListProductInfoFromProduct(KPaging<ProductInfo> kPaging, String desc, ActiveType status, ProductType type) throws BusinessException {
		try {
			List<ProductInfo> lst = productInfoDAO.getListProductInfoFromProduct(kPaging, desc, status, type);
			ObjectVO<ProductInfo> vo = new ObjectVO<ProductInfo>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ProductInfo> getListProductInfo(KPaging<ProductInfo> kPaging, String productInfoCode, String productInfoName, String desc, ActiveType status, ProductType type, Boolean isExceptZCat, String sort, String order)
			throws BusinessException {
		try {
			List<ProductInfo> lst = productInfoDAO.getListProductInfo(kPaging, productInfoCode, productInfoName, desc, status, type, isExceptZCat, sort, order);
			ObjectVO<ProductInfo> vo = new ObjectVO<ProductInfo>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ProductInfo> getListProductInfoForStock(KPaging<ProductInfo> kPaging, String productInfoCode, String productInfoName, String desc, ActiveType status, ProductType type) throws BusinessException {
		try {
			List<ProductInfo> lst = productInfoDAO.getListProductInfoForStock(kPaging, productInfoCode, productInfoName, desc, status, type);
			ObjectVO<ProductInfo> vo = new ObjectVO<ProductInfo>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean isUsingByOthers(long productInfoId) throws BusinessException {
		try {
			return productInfoDAO.isUsingByOthers(productInfoId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Boolean removeProductInfo(Long productInfoId, LogInfoVO logInfo) {
		try {
			if (productInfoId == null)
				throw new IllegalArgumentException("productInfoId is null");
			ProductInfo productInfo = productInfoDAO.getProductInfoById(productInfoId);
			if (productInfo == null)
				throw new IllegalArgumentException("Cannot get product info object");
			productInfo.setStatus(ActiveType.DELETED);
			productInfoDAO.updateProductInfo(productInfo, logInfo);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	@Override
	public ObjectVO<ProductInfo> getListSubCatNotInDisplayGroup(KPaging<ProductInfo> kPaging, Long displayGroupId, String subCatCode, String subCatName) throws BusinessException {
		try {
			List<ProductInfo> lst = productInfoDAO.getListSubCatNotInDisplayGroup(kPaging, displayGroupId, subCatCode, subCatName);
			ObjectVO<ProductInfo> vo = new ObjectVO<ProductInfo>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean isEquimentCat(String catCode) throws BusinessException {
		try {
			return productInfoDAO.isEquimentCat(catCode);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/***************************** Nganh hang con phu ****************************************/
	/**
	 * Tim kiem danh sach nganh hang con phu
	 * 
	 * @author thongnm
	 */

	@Override
	public ObjectVO<ProductInfoVOEx> getListProductInfoOfSecondSubCat(KPaging<ProductInfoVOEx> kPaging, String catCode, String subCatCode, String subCatName) throws BusinessException {
		try {
			List<ProductInfoVOEx> lst = productInfoDAO.getListProductInfoOfSecondSubCat(kPaging, catCode, subCatCode, subCatName);
			ObjectVO<ProductInfoVOEx> vo = new ObjectVO<ProductInfoVOEx>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Danh sach nganh hang trong nganh hang con phu
	 * 
	 * @author thongnm
	 */
	@Override
	public List<ProductInfo> getListCatInSecondSubCat() throws BusinessException {
		try {
			List<ProductInfo> lst = productInfoDAO.getListProductInfo(null, null, null, null, ActiveType.RUNNING, ProductType.CAT, false, null, null);//sort = null,order = null
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Them moi nganh hang con phu
	 * 
	 * @author thongnm
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void addToProductInfoOfSecondSubCat(String catCode, String subCatCode, String subCatName, String description, LogInfoVO logInfo) throws BusinessException {
		try {
			if (StringUtility.isNullOrEmpty(catCode)) {
				throw new IllegalArgumentException("catCode is null");
			}
			if (StringUtility.isNullOrEmpty(subCatCode)) {
				throw new IllegalArgumentException("subCatCode is null");
			}
			if (StringUtility.isNullOrEmpty(subCatName)) {
				throw new IllegalArgumentException("subCatName is null");
			}
			ProductInfo cat = productInfoDAO.getProductInfoByCode(catCode, ProductType.CAT, null, true);
			if (cat == null) {
				throw new IllegalArgumentException("cat is not existed");
			}
			ProductInfo subCatSecond = productInfoDAO.getProductInfoByCode(subCatCode, ProductType.SECOND_SUB_CAT, catCode, true);
			if (subCatSecond == null) {
				subCatSecond = new ProductInfo();
				subCatSecond.setStatus(ActiveType.RUNNING);
				subCatSecond.setProductInfoCode(subCatCode);
				subCatSecond.setProductInfoName(subCatName);
				subCatSecond.setDescription(description);
				subCatSecond.setType(ProductType.SECOND_SUB_CAT);
				ProductInfo subCatSecondCreate = productInfoDAO.createProductInfo(subCatSecond, logInfo);

				ProductInfoMap productInfoMap = new ProductInfoMap();
				productInfoMap.setFromObject(cat);
				productInfoMap.setToObject(subCatSecondCreate);
				productInfoMap.setObjectType(ProductInfoMapType.SECOND_SUB_CAT);
				productInfoMap.setStatus(ActiveType.RUNNING);
				productInfoMap.setCreateDate(commonDAO.getSysDate());
				productInfoMap.setCreateUser(logInfo.getStaffCode());
				productInfoMapDAO.createProductInfoMap(productInfoMap, logInfo);
			} else {
				throw new IllegalArgumentException("subCatCode is existed");
			}
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * Cap nhat nganh hang con phu
	 * 
	 * @author thongnm
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateToProductInfoOfSecondSubCat(String catCode, String subCatCode, String subCatName, String description, LogInfoVO logInfo) throws BusinessException {
		try {
			if (StringUtility.isNullOrEmpty(subCatCode)) {
				throw new IllegalArgumentException("updateToProductInfoOfSecondSubCat: subCatCode is null");
			}
			if (StringUtility.isNullOrEmpty(subCatName)) {
				throw new IllegalArgumentException("updateToProductInfoOfSecondSubCat: subCatName is null");
			}
			ProductInfo cat = productInfoDAO.getProductInfoByCode(catCode, ProductType.CAT, null, true);
			if (cat == null) {
				throw new IllegalArgumentException("updateToProductInfoOfSecondSubCat: cat not is existed or not running");
			}
			ProductInfo subCatSecond = productInfoDAO.getProductInfoByCode(subCatCode, ProductType.SECOND_SUB_CAT, catCode, true);
			if (subCatSecond == null) {
				throw new IllegalArgumentException("updateToProductInfoOfSecondSubCat: subCatCode not is existed or not runing");
			}
			ProductInfoMap productInfoMap = productInfoMapDAO.getProductInfoMap(cat.getId(), subCatSecond.getId(), ProductInfoMapType.SECOND_SUB_CAT);
			if (productInfoMap == null) {
				throw new IllegalArgumentException("updateToProductInfoOfSecondSubCat: productInfoMap not is existed");
			}
			subCatSecond.setProductInfoName(subCatName);
			subCatSecond.setDescription(description);
			productInfoDAO.updateProductInfo(subCatSecond, logInfo);
			productInfoMapDAO.updateProductInfoMap(productInfoMap, logInfo);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<ProductInfo> getListSubCat(ActiveType status, ProductType type, Long from_object) throws BusinessException {
		try {
			List<ProductInfo> lst = productInfoDAO.getListSubCat(status, type, from_object);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

	}

	@Override
	public List<ProductInfo> getListSubCatByListCat(ActiveType status, ProductType type, List<Long> lstObjectId) throws BusinessException {
		try {
			List<ProductInfo> lst = productInfoDAO.getListSubCatByListCat(status, type, lstObjectId);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

	}

	@Override
	public List<ProductInfo> getListSubCat(ActiveType status, ProductType type, ProductInfoMapType mapType, Long from_object) throws BusinessException {
		try {
			List<ProductInfo> lst = productInfoDAO.getListSubCat(status, type, mapType, from_object);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

	}

	@Override
	public ObjectVO<ProductInfo> getListProductInfoByFilter(BasicFilter<ProductInfo> filter) throws BusinessException {
		try {
			List<ProductInfo> lst = productInfoDAO.getListProductInfoByFilter(filter);
			ObjectVO<ProductInfo> vo = new ObjectVO<ProductInfo>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

	}

	@Override
	public ObjectVO<ProductInfo> getListProductInfoEx(KPaging<ProductInfo> kPaging, String productInfoCode, String productInfoName, ActiveType status, ProductType type, List<ProductInfoObjectType> objTypes, boolean orderByCode)
			throws BusinessException {
		try {
			List<ProductInfo> lst = productInfoDAO.getListProductInfoEx(kPaging, productInfoCode, productInfoName, status, type, objTypes, orderByCode);
			ObjectVO<ProductInfo> vo = new ObjectVO<ProductInfo>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ProductInfo> getListProductInfoStock(KPaging<ProductInfo> kPaging, String productInfoCode, String productInfoName, String desc, ActiveType status, ProductType type) throws BusinessException {
		try {
			List<ProductInfo> lst = productInfoDAO.getListProductInfoStock(kPaging, productInfoCode, productInfoName, desc, status, type);
			ObjectVO<ProductInfo> vo = new ObjectVO<ProductInfo>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ProductInfo> getListProductInfoTypeStock(KPaging<ProductInfo> kPaging, ActiveType producttype) throws BusinessException {
		try {
			List<ProductInfo> lst = productInfoDAO.getListProductInfoTypeStock(kPaging, producttype);
			ObjectVO<ProductInfo> vo = new ObjectVO<ProductInfo>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ApParam> getListShopProductInfoType(Object object, ActiveType producttype) throws BusinessException {
		try {
			List<ApParam> lst = productInfoDAO.getListShopProductInfoType(object, producttype);
			ObjectVO<ApParam> vo = new ObjectVO<ApParam>();
			vo.setLstObject(lst);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<CatalogVO> getListCatalogByFilter(CatalogFilter filter) throws BusinessException {
		try {
			List<CatalogVO> lst = productInfoDAO.getListCatalogByFilter(filter);
			ObjectVO<CatalogVO> vo = new ObjectVO<CatalogVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ProductInfo getProductInfoByCodeCat(String code, ProductType type, Boolean isActiveTypeRunning) throws BusinessException {
		try {
			return productInfoDAO.getProductInfoByCodeCat(code, type, isActiveTypeRunning);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ProductInfo getProductInfoByWithNotSubCat(String code, ProductType type, Boolean isActiveTypeRunning) throws BusinessException {
		try {
			return productInfoDAO.getProductInfoByWithNotSubCat(code, type, isActiveTypeRunning);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ProductInfo> getListSubCatByCatId(Long catId, ActiveType status) throws BusinessException {
		try {
			return productInfoDAO.getListSubCatByCatId(catId, status);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void changeStatusDeleteProductInfo(ProductInfo productInfo, LogInfoVO logInfo) throws BusinessException{
		try {
			if (productInfo == null) {
				throw new IllegalArgumentException("productInfo is null");
			}
			if (logInfo == null) {
				throw new IllegalArgumentException("logInfo is null");
			}
			if (productInfo.getType() != null && ProductType.SUB_CAT.getValue().equals(productInfo.getType().getValue())) {
				ProductInfoMap pdInfoMap = productInfoMapDAO.getProductInfoMapByToId(productInfo.getId(), ProductInfoMapType.SUB_CAT);
				if (pdInfoMap != null) {
					pdInfoMap.setStatus(ActiveType.DELETED);
					productInfoMapDAO.updateProductInfoMap(pdInfoMap, logInfo);
				}
			}
			productInfo.setStatus(ActiveType.DELETED);
			productInfoDAO.updateProductInfo(productInfo, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateProductInfo(ProductInfo productInfo, ProductInfo parentProductInfo, LogInfoVO logInfo) throws BusinessException {
		try {
			if (productInfo == null) {
				throw new IllegalArgumentException("productInfo is null");
			}
			if (logInfo == null) {
				throw new IllegalArgumentException("logInfo is null");
			}
			productInfoDAO.updateProductInfo(productInfo, logInfo);
			if (productInfo.getType() != null && ProductType.SUB_CAT.getValue().equals(productInfo.getType().getValue())) {
				ProductInfoMap pdInfoMap = productInfoMapDAO.getProductInfoMapByToId(productInfo.getId(), ProductInfoMapType.SUB_CAT);
				if (pdInfoMap != null) {
					pdInfoMap.setFromObject(parentProductInfo);
					pdInfoMap.setToObject(productInfo);
					pdInfoMap.setStatus(ActiveType.RUNNING);
					pdInfoMap.setObjectType(ProductInfoMapType.SUB_CAT);
					productInfoMapDAO.updateProductInfoMap(pdInfoMap, logInfo);
				} else {
					pdInfoMap = new ProductInfoMap();
					pdInfoMap.setFromObject(parentProductInfo);
					pdInfoMap.setToObject(productInfo);
					pdInfoMap.setStatus(ActiveType.RUNNING);
					pdInfoMap.setObjectType(ProductInfoMapType.SUB_CAT);
					productInfoMapDAO.createProductInfoMap(pdInfoMap, logInfo);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public ProductInfo createProductInfo(ProductInfo productInfo, ProductInfo parentProductInfo, LogInfoVO logInfo) throws BusinessException {
		try {
			if (productInfo == null) {
				throw new IllegalArgumentException("productInfo is null");
			}
			if (logInfo == null) {
				throw new IllegalArgumentException("logInfo is null");
			}
			productInfo = productInfoDAO.createProductInfo(productInfo, logInfo);
			if (productInfo.getType() != null && ProductType.SUB_CAT.getValue().equals(productInfo.getType().getValue())) {
				ProductInfoMap pdInfoMap = new ProductInfoMap();
				pdInfoMap.setFromObject(parentProductInfo);
				pdInfoMap.setToObject(productInfo);
				pdInfoMap.setStatus(ActiveType.RUNNING);
				pdInfoMap.setObjectType(ProductInfoMapType.SUB_CAT);
				productInfoMapDAO.createProductInfoMap(pdInfoMap, logInfo);
			}
			return productInfo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ProductInfoMap createProductInfoMap(ProductInfoMap pdInfoMap, LogInfoVO logInfo) throws BusinessException {
		try {
			return productInfoMapDAO.createProductInfoMap(pdInfoMap, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateProductInfoMap(ProductInfoMap pdInfoMap, LogInfoVO logInfo) throws BusinessException {
		try {
			productInfoMapDAO.updateProductInfoMap(pdInfoMap, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ProductInfoMap getProductInfoMap(long catId, long subCatId, ProductInfoMapType productInfoMapType) throws BusinessException {
		try {
			return productInfoMapDAO.getProductInfoMap(catId, subCatId, productInfoMapType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ProductInfoMap getProductInfoMapByToId(long toId, ProductInfoMapType productInfoMapType) throws BusinessException {
		try {
			return productInfoMapDAO.getProductInfoMapByToId(toId, productInfoMapType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

}

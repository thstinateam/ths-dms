package ths.dms.core.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.Area;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AreaType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.AreaVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.AreaDAO;

public class AreaMgrImpl implements AreaMgr {

	@Autowired
	AreaDAO areaDAO;
	
	@Override
	public Area createArea(Area area, LogInfoVO logInfo) throws BusinessException {
		try {
			return areaDAO.createArea(area, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateArea(Area area, LogInfoVO logInfo) throws BusinessException {
		try {
			areaDAO.updateArea(area, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateListArea(List<Area> lstArea, LogInfoVO logInfo) throws BusinessException {
		try {
			for (Area area: lstArea) {
				areaDAO.updateArea(area, logInfo);
			}
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteArea(Area area, LogInfoVO logInfo) throws BusinessException{
		try {
			areaDAO.deleteArea(area, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Area getAreaById(Long id) throws BusinessException{
		try {
			return id==null?null:areaDAO.getAreaById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Area getAreaByCode(String code) throws BusinessException {
		try {
			return areaDAO.getAreaByCode(code);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Area> getListArea(KPaging<Area> kPaging, String areaCode,
			String areaName, Long parentId, ActiveType status,
			String provinceCode, String provinceName, String districtCode,
			String districtName, String precinctCode, String precinctName,
			AreaType type, Boolean isGetOneChildLevel,
			String sort,String order) throws BusinessException {
		try {
			List<Area> lst = areaDAO.getListArea(kPaging, areaCode, areaName, parentId, status, provinceCode, provinceName, districtCode, districtName, precinctCode, precinctName, type, isGetOneChildLevel,sort,order);
			
			ObjectVO<Area> vo = new ObjectVO<Area>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
			
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean isUsingByOthers(long areaId) throws BusinessException {
		try {
			return areaDAO.isUsingByOthers(areaId);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<Area> getListSubArea(Long parentId) throws BusinessException {
		try {
			return areaDAO.getListSubArea(parentId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Area> getListSubArea(Long parentId, ActiveType activeType, Boolean isOrderByCode) throws BusinessException {
		try {
			return areaDAO.getListSubArea(parentId, activeType, null, isOrderByCode);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Area> getListSubAreaEx(Long parentId, ActiveType activeType, Long exceptionalAreaId) throws BusinessException {
		try {
			return areaDAO.getListSubArea(parentId, activeType, exceptionalAreaId, true);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public TreeVO<Area> getAreaTreeVO(Area area, Long exceptionalArea) throws BusinessException {
		TreeVO<Area> vo = new TreeVO<Area>();
		vo.setObject(area);
		
		List<Area> lstChild = this.getListSubArea(area==null?null:area.getId());
		
		if (lstChild != null)
			for (Area type: lstChild) {
				if (type.getId().equals(exceptionalArea))
					continue;
				TreeVO<Area> childVO = getAreaTreeVO(type, exceptionalArea);
				List<TreeVO<Area>> lst = vo.getListChildren();
				lst.add(childVO);
				vo.setListChildren(lst);
			}
		
		return vo;
	}
	
	@Override
	public TreeVO<Area> getAreaTreeVOEx(Area area, Long exceptionalArea, ActiveType status) throws BusinessException {
		TreeVO<Area> vo = new TreeVO<Area>();
		vo.setObject(area);
		
		List<Area> lstChild = this.getListSubArea(area==null?null:area.getId());
		
		if (lstChild != null)
			for (Area type: lstChild) {
				if (type.getId().equals(exceptionalArea) || !type.getStatus().equals(status))
					continue;
				TreeVO<Area> childVO = getAreaTreeVOEx(type, exceptionalArea, status);
				List<TreeVO<Area>> lst = vo.getListChildren();
				lst.add(childVO);
				vo.setListChildren(lst);
			}
		
		return vo;
	}
	
	@Override
	public TreeVO<Area> getAreaTreeVOEx1(Area area, Long exceptionalArea, ActiveType status) throws BusinessException {
		TreeVO<Area> vo = new TreeVO<Area>();
		vo.setObject(area);
		
		List<Area> lstChild = this.getListSubArea(area==null?null:area.getId());
		Boolean flag = true;
		
		if (lstChild != null)
			for (Area type: lstChild) {
				if (type.getId().equals(exceptionalArea) || !type.getStatus().equals(status))
					continue;
				TreeVO<Area> childVO = new TreeVO<Area>();
				childVO.setObject(type);
				
				if (flag == true) {
					childVO = getAreaTreeVOEx(type, exceptionalArea, status);
					flag = false;
				}
				
				//cong them vao 1 child
				List<TreeVO<Area>> lst = vo.getListChildren();
				lst.add(childVO);
				vo.setListChildren(lst);
			}
		
		return vo;
	}
	
	@Override
	public Boolean checkIfAreaHasAnyChildStopped(Long areaId)
			throws BusinessException {
		try {
			return areaDAO.checkIfAreaHasAnyChildStopped(areaId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkIfAreaHasAllChildStopped(Long areaId)
			throws BusinessException {
		try {
			return areaDAO.checkIfAreaHasAllChildStopped(areaId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public TreeVO<Area> getAncestor(Long AreaId) throws BusinessException {
		try {
			Area area = this.getAreaById(AreaId);
			TreeVO<Area> tree = new TreeVO<Area>();
			
			if (area.getParentArea() == null) {
				tree.setObject(area);
				return tree;
			}
			
			while (area.getParentArea() != null) {
				area = area.getParentArea();
				
				List<Area> lstChild = this.getListSubArea(area.getId(), ActiveType.RUNNING, null);
				List<TreeVO<Area>> lstChildren = new ArrayList<TreeVO<Area>>();
				for (Area s: lstChild) {
					TreeVO<Area> subTree = new TreeVO<Area>();
					subTree.setObject(s);
					subTree.setListChildren(null);
					if (tree.getObject() != null && s.getId().equals(tree.getObject().getId()))
						lstChildren.add(tree);
					else
						lstChildren.add(subTree);
				}
				
				tree = getAncestor(area, lstChildren);
			}
			
			//get areas with parent_area_id is null
			List<Area> lstChild = this.getListSubArea(null, ActiveType.RUNNING, null);
			List<TreeVO<Area>> lstChildren = new ArrayList<TreeVO<Area>>();
			for (Area s: lstChild) {
				TreeVO<Area> subTree = new TreeVO<Area>();
				subTree.setObject(s);
				subTree.setListChildren(null);
				if (tree.getObject() != null && s.getId().equals(tree.getObject().getId()))
					lstChildren.add(tree);
				else
					lstChildren.add(subTree);
			}
			tree = getAncestor(null, lstChildren);
			//end
			return tree;
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	private TreeVO<Area> getAncestor(Area object, List<TreeVO<Area>> lstChildren) {
		TreeVO<Area> tree = new TreeVO<Area>();
		tree.setObject(object);
		tree.setListChildren(lstChildren);
		return tree;
	}
	
	@Override
	public Area getArea(String provinceCode, 
			String districtCode, String precinctCode, 
			AreaType type, ActiveType status) throws BusinessException {
		try {
			return areaDAO.getArea(provinceCode, districtCode, precinctCode, type, status);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Area getAreaEqual(String areaCode,
			String areaName, Long parentId, ActiveType status,
			String provinceCode, String provinceName, 
			String districtCode, String districtName, 
			String precinctCode, String precinctName, AreaType type,
			Boolean isGetOneChildLevel) throws BusinessException {
		try {
			return areaDAO.getAreaEqual(areaCode, areaName, parentId, status, provinceCode, provinceName, districtCode, districtName, precinctCode, precinctName, type, isGetOneChildLevel);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<Area> getListAreaFull() throws BusinessException {
		try {
			return areaDAO.getListAreaFull();
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
	}

	@Override
	public Area getAreaByPrecinct(String code) throws BusinessException {
		try {
			return areaDAO.getAreaByPrecinct(code);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<AreaVO> getListAreaByType(Long parentId, Integer type, Integer status)
			throws BusinessException {
		try {
			return areaDAO.getListAreaByType(parentId, type, status);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
	}

	@Override
	public List<AreaVO> retrieveAreaWithParentAreaInfo(ActiveType status)
			throws BusinessException {
		try {
			return areaDAO.retrieveAreaWithParentAreaInfo(status);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}
	}
}

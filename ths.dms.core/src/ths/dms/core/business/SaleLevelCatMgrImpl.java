package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SaleLevelCat;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;

import ths.dms.core.dao.SaleLevelCatDAO;

public class SaleLevelCatMgrImpl implements SaleLevelCatMgr {

	@Autowired
	SaleLevelCatDAO saleLevelCatDAO;

	@Override
	public SaleLevelCat createSaleLevelCat(SaleLevelCat saleLevelCat, LogInfoVO logInfo) throws BusinessException {
		try {
			return saleLevelCatDAO.createSaleLevelCat(saleLevelCat, logInfo);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void deleteSaleLevelCat(SaleLevelCat saleLevelCat, LogInfoVO logInfo) throws BusinessException {
		try {
			saleLevelCatDAO.deleteSaleLevelCat(saleLevelCat, logInfo);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}

	}

	@Override
	public void updateSaleLevelCat(SaleLevelCat saleLevelCat, LogInfoVO logInfo) throws BusinessException {
		try {
			saleLevelCatDAO.updateSaleLevelCat(saleLevelCat, logInfo);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}

	}

	@Override
	public SaleLevelCat getSaleLevelCatById(long id) throws BusinessException {
		try {
			return saleLevelCatDAO.getSaleLevelCatById(id);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Boolean checkIfRecordExist(long catId, String saleLevel, Long id) throws BusinessException {
		try {
			return saleLevelCatDAO.checkIfRecordExist(catId, saleLevel, id);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleLevelCat> getListSaleLevelCat(KPaging<SaleLevelCat> kPaging, Long catId, String catName, String saleLevel, ActiveType status) throws BusinessException {
		try {
			List<SaleLevelCat> lst = saleLevelCatDAO.getListSaleLevelCat(kPaging, catId, catName, saleLevel, status);
			ObjectVO<SaleLevelCat> vo = new ObjectVO<SaleLevelCat>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<String> getListSaleLevel(Long catId, String catName, String saleLevel, ActiveType status) throws BusinessException {
		try {
			return saleLevelCatDAO.getListSaleLevel(catId, catName, saleLevel, status);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public SaleLevelCat getSaleLevelCatByCatAndSaleLevel(Long catId, String saleLevel) throws BusinessException {
		try {
			return saleLevelCatDAO.getSaleLevelCatByCatAndSaleLevel(catId, saleLevel);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<SaleLevelCat> getListSaleLevelCatByProductInfo(KPaging<SaleLevelCat> kPaging, List<String> listProInfoCode, String saleLevelCode, String saleLevelName, BigDecimal fromAmount, BigDecimal toAmount, ActiveType status
			,String sort,String order)
			throws BusinessException {
		try {
			List<SaleLevelCat> lst = saleLevelCatDAO.getListSaleLevelCatByProductInfo(kPaging, listProInfoCode, saleLevelCode, saleLevelName, fromAmount, toAmount, status,sort,order);
			ObjectVO<SaleLevelCat> vo = new ObjectVO<SaleLevelCat>();

			vo.setkPaging(kPaging);
			vo.setLstObject(lst);

			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
}

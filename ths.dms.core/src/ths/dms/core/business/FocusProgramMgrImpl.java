package ths.dms.core.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.FocusChannelMap;
import ths.dms.core.entities.FocusChannelMapProduct;
import ths.dms.core.entities.FocusProgram;
import ths.dms.core.entities.FocusShopMap;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.CmsFilter;
import ths.dms.core.entities.enumtype.FocusProgramFilter;
import ths.dms.core.entities.enumtype.HTBHFilter;
import ths.dms.core.entities.enumtype.HTBHVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductCatVO;
import ths.dms.core.entities.vo.ProductSubCatVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.ApParamDAO;
import ths.dms.core.dao.ChannelTypeDAO;
import ths.dms.core.dao.CmsDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.FocusChannelMapDAO;
import ths.dms.core.dao.FocusChannelMapProductDAO;
import ths.dms.core.dao.FocusProgramDAO;
import ths.dms.core.dao.FocusShopMapDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.ShopDAO;

public class FocusProgramMgrImpl implements FocusProgramMgr {
	@Autowired
	FocusProgramDAO focusProgramDAO;
	
	@Autowired
	FocusChannelMapProductDAO focusChannelMapProductDAO;
	
	@Autowired
	FocusShopMapDAO focusShopMapDAO;
	
	@Autowired
	FocusChannelMapDAO focusChannelMapDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	ChannelTypeDAO channelTypeDAO;
	
	@Autowired
	ApParamDAO apParamDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	ShopDAO shopDAO;
	
	@Autowired
	CmsDAO cmsDAO;
	
	@Override
	public FocusProgram createFocusProgram(FocusProgram focusProgram, LogInfoVO logInfo) throws BusinessException {
		try {
			return focusProgramDAO.createFocusProgram(focusProgram, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateFocusProgram(FocusProgram focusProgram, LogInfoVO logInfo) throws BusinessException {
		try {
//			if(!this.checkUpdateFocusProgramForActive(focusProgram.getId()).equals(ExceptionCode.ERR_ACTIVE_PROGRAM_OK)){
//				throw new IllegalArgumentException("CTTT khong co sp, don vi");
//			}
			focusProgramDAO.updateFocusProgram(focusProgram, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	/**
	 * Kiem tra khi cap nhat CTTT sang hoat dong
	 * @author thongnm
	 */
	@Override
	public String checkUpdateFocusProgramForActive(Long focusProgramId)
			throws BusinessException {
		try {
			List<FocusShopMap> listFocusShopMap = null;
			List<FocusChannelMap> listFocusChannelMap = null;
			
			listFocusShopMap = this.focusShopMapDAO.getListFocusShopMapByFocusProgramId(null, focusProgramId,null, null, ActiveType.RUNNING, null);
			listFocusChannelMap = this.focusChannelMapDAO.getListFocusChannelMapByFocusProgramId(null,focusProgramId, null, ActiveType.RUNNING);
			
			
			if(listFocusShopMap == null || listFocusShopMap.size() <= 0){
				return ExceptionCode.ERR_ACTIVE_FOCUS_PROGRAM_DVTG;
			}
			if(listFocusChannelMap == null || listFocusChannelMap.size() <= 0){
				return ExceptionCode.ERR_ACTIVE_FOCUS_PROGRAM_HTBH;
			}
			for (FocusChannelMap child : listFocusChannelMap) {
				List<FocusChannelMapProduct> lstDetail = this.focusChannelMapProductDAO.getListFocusChannelMapProductByFocusProgramId(null,null, null, focusProgramId, child.getId(),null);
				if(lstDetail != null && lstDetail.size() > 0){
					return  ExceptionCode.ERR_ACTIVE_PROGRAM_OK;
				}
			}
			return ExceptionCode.ERR_ACTIVE_FOCUS_PROGRAM_SPTT;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteFocusProgram(FocusProgram focusProgram, LogInfoVO logInfo) throws BusinessException{
		try {
			focusProgramDAO.deleteFocusProgram(focusProgram, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public FocusProgram getFocusProgramById(Long id) throws BusinessException{
		try {
			return id==null?null:focusProgramDAO.getFocusProgramById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public FocusProgram getFocusProgramByCode(String code) throws BusinessException {
		try {
			return focusProgramDAO.getFocusProgramByCode(code);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<FocusProgram> getListFocusProgram(KPaging<FocusProgram> kPaging,FocusProgramFilter filter, StaffRole staffRole) throws BusinessException {
		try {
			List<FocusProgram> lst = focusProgramDAO.getListFocusProgram(kPaging, filter, staffRole);
			ObjectVO<FocusProgram> vo = new ObjectVO<FocusProgram>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<FocusShopMap> getListFocusShopMapV2(KPaging<FocusShopMap> kPaging,
			String focusProgramCode, String focusProgramName, String shopCode,
			String shopName, Date fromDate, Date toDate, ActiveType status) throws BusinessException {
		try {
			List<FocusShopMap> lst = focusProgramDAO.getListFocusShopMapV2(kPaging, focusProgramCode, focusProgramName, shopCode, shopName, fromDate, toDate, status);
			ObjectVO<FocusShopMap> vo = new ObjectVO<FocusShopMap>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	//-----------------------------------------------------------------
	
//	@Override
//	public FocusChannelMapProduct createFocusChannelMapProduct(FocusChannelMapProduct focusChannelMapProduct, 
//			LogInfoVO logInfo) throws BusinessException {
//		try {
//			return focusChannelMapProductDAO.createFocusChannelMapProduct(focusChannelMapProduct, logInfo);
//		} catch (DataAccessException e) {
//			
//			throw new BusinessException(e);
//		}
//	}
	
	@Override
	public void updateFocusChannelMapProduct(FocusChannelMapProduct focusChannelMapProduct, LogInfoVO logInfo) throws BusinessException {
		try {
			focusChannelMapProductDAO.updateFocusChannelMapProduct(focusChannelMapProduct, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteFocusChannelMapProduct(FocusChannelMapProduct focusChannelMapProduct, LogInfoVO logInfo) throws BusinessException{
		try {
			focusChannelMapProductDAO.deleteFocusChannelMapProduct(focusChannelMapProduct, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public FocusChannelMapProduct getFocusChannelMapProductById(Long id) throws BusinessException{
		try {
			return id==null?null:focusChannelMapProductDAO.getFocusChannelMapProductById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<FocusChannelMapProduct> getListFocusChannelMapProduct(KPaging<FocusChannelMapProduct> kPaging, String productCode, String productName, Long focusProgramId, String type) throws BusinessException {
		try {
			List<FocusChannelMapProduct> lst = focusChannelMapProductDAO.getListFocusChannelMapProductByFocusProgramId(kPaging, productCode, productName, focusProgramId, type);
			ObjectVO<FocusChannelMapProduct> vo = new ObjectVO<FocusChannelMapProduct>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<FocusChannelMapProduct> getListFocusChannelMapProductEx(
			KPaging<FocusChannelMapProduct> kPaging, String productCode, String productName,
			Long focusProgramId, Long focusChannelMapId, String type) throws BusinessException {
		try {
			List<FocusChannelMapProduct> lst = focusChannelMapProductDAO
					.getListFocusChannelMapProductByFocusProgramId(kPaging, productCode, productName, focusProgramId, focusChannelMapId, type);
			ObjectVO<FocusChannelMapProduct> vo = new ObjectVO<FocusChannelMapProduct>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	//-----------------------------------------------------------------

	@Transactional(rollbackFor = Exception.class)
	@Override
	public FocusShopMap createFocusShopMap(FocusShopMap focusShopMap,
			LogInfoVO logInfo) throws BusinessException {
		/*try {
			Shop shop = focusShopMap.getShop();

			if (shop.getType().getObjectType().equals(ShopType.SHOP.getValue())
					|| Boolean.FALSE.equals(createAll)) {
				FocusShopMap m = focusShopMapDAO.createFocusShopMap(
						focusShopMap, logInfo);
				return focusShopMapDAO
						.createFocusShopMap(focusShopMap, logInfo);
			} else if (shop.getType().getObjectType()
					.equals(ShopType.AREA.getValue())
					|| shop.getType().getObjectType()
							.equals(ShopType.REGION.getValue())) {
				// return this.createListFocusShopMap(focusShopMap, logInfo);
				return this.createOneFocusShopMap(focusShopMap, logInfo);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}*/
		
		return this.createOneFocusShopMap(focusShopMap, logInfo);
	}
	
	/*@Override
	@Transactional (rollbackFor = Exception.class)
	public Boolean createListFocusShopMap(FocusShopMap focusShopMap, LogInfoVO logInfo) throws BusinessException {
		try {
			return focusShopMapDAO.createListFocusShopMap(focusShopMap, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}*/
	
	@Override
	// @Transactional(rollbackFor = Exception.class)
	public Boolean createListFocusShopMap(List<FocusShopMap> list,
			LogInfoVO logInfo) throws BusinessException {
		for (FocusShopMap psm : list) {
			if (!this.isExistChildJoinProgram(psm.getShop().getShopCode(), psm
					.getFocusProgram().getId())) {
				this.createOneFocusShopMap(psm, logInfo);
			}
		}

		return true;
	}
	
	@Override
	public Boolean hasChildShopJoinFocusProgram(Long focusProgramId, Long shopId)
			throws BusinessException {
		try {
			return this.focusProgramDAO.hasChildShopJoinFocusProgram(
					focusProgramId, shopId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	private FocusShopMap createOneFocusShopMap(FocusShopMap focusShopMap,
			LogInfoVO logInfo) throws BusinessException {
		try {
			List<FocusShopMap> listFocusShopMap = focusShopMapDAO
					.getListFocusShopMapWithShopAndFocusProgram(
							focusShopMap.getShop().getId(),
							focusShopMap.getFocusProgram().getId());

			for (FocusShopMap program : listFocusShopMap) {
				FocusShopMap temp = program.clone();
				temp.setStatus(ActiveType.DELETED);
				
				focusShopMapDAO.updateFocusShopMap(temp, logInfo);
			}

			FocusShopMap result = focusShopMapDAO
					.createFocusShopMap(focusShopMap, logInfo);
			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional (rollbackFor = Exception.class)
	public Boolean createListFocusShopMapEx(Long focusProgramId, List<Long> lstShopId, ActiveType status, LogInfoVO logInfo) throws BusinessException {
		try {
			return focusShopMapDAO.createListFocusShopMap(focusProgramId, lstShopId, status, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional (rollbackFor = Exception.class)
	public void updateFocusShopMap(FocusShopMap focusShopMap, LogInfoVO logInfo) throws BusinessException {
		try {
			focusShopMapDAO.updateFocusShopMap(focusShopMap, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteFocusShopMap(Long shopId, Long focusProgramId, long shopRootId, LogInfoVO logInfo) throws BusinessException {
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfoVO null");
		}
		this.deleteOneFocusShopMap(shopId, focusProgramId, shopRootId, logInfo);
	}
	
	private void deleteOneFocusShopMap(Long shopId, Long focusProgramId, long shopRootId, LogInfoVO logInfo) throws BusinessException {
		try {
			Shop sh = shopDAO.getShopById(shopId);
			if (sh == null) {
				throw new BusinessException("shopId is not exist");
			}
			Date now1 = commonDAO.getSysDate();

			List<FocusShopMap> list = focusShopMapDAO.getListFocusChildShopMapWithShopAndFocusProgram(shopId, focusProgramId);

			FocusShopMap temp = null;
			for (FocusShopMap program : list) {
				temp = program.clone();
				temp.setStatus(ActiveType.DELETED);
				temp.setUpdateDate(now1);
				temp.setUpdateUser(logInfo.getStaffCode());
				
				focusShopMapDAO.updateFocusShopMap(temp, logInfo);
			}

			// kiem tra cha no coi co con chau gi tham gia khong, neu khong thi
			// them cha no vao
			if (sh.getParentShop() != null) {
				CmsFilter filter = new CmsFilter();
				filter.setShopId(sh.getParentShop().getId());
				filter.setShopRootId(logInfo.getShopRootId());
				if (cmsDAO.checkShopInCMSByFilter(filter) == 1) {
					list = focusShopMapDAO.getListFocusChildShopMapWithShopAndFocusProgram(sh.getParentShop().getId(), focusProgramId);
					if (list.size() == 0) {
						FocusShopMap psm = new FocusShopMap();
						psm.setShop(sh.getParentShop());
						FocusProgram fp = focusProgramDAO.getFocusProgramById(focusProgramId);
						psm.setFocusProgram(fp);
						psm.setCreateDate(now1);
						psm.setCreateUser(logInfo.getStaffCode());
						
						focusShopMapDAO.createFocusShopMap(psm, logInfo);
					}
				}
			}

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public FocusShopMap getFocusShopMapById(Long id) throws BusinessException{
		try {
			return id==null?null:focusShopMapDAO.getFocusShopMapById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<FocusShopMap> getListFocusShopMap(
			KPaging<FocusShopMap> kPaging,
			Long focusProgramId, String shopCode, String shopName,  ActiveType status,
			Long parentShopId) throws BusinessException {
		try {
			List<FocusShopMap> lst = focusShopMapDAO.getListFocusShopMapByFocusProgramId(kPaging, focusProgramId, shopCode, shopName, status, parentShopId);
			ObjectVO<FocusShopMap> vo = new ObjectVO<FocusShopMap>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	//-----------------------------------------------------------------
	
	@Override
	public FocusChannelMap createFocusChannelMap(FocusChannelMap focusChannelMap, LogInfoVO logInfo) throws BusinessException {
		try {
			return focusChannelMapDAO.createFocusChannelMap(focusChannelMap, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateFocusChannelMap(FocusChannelMap focusChannelMap, LogInfoVO logInfo) throws BusinessException {
		try {
			focusChannelMapDAO.updateFocusChannelMap(focusChannelMap, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteFocusChannelMap(FocusChannelMap focusChannelMap, LogInfoVO logInfo) throws BusinessException{
		try {
			focusChannelMapDAO.deleteFocusChannelMap(focusChannelMap, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public FocusChannelMap getFocusChannelMapById(Long id) throws BusinessException{
		try {
			return id==null?null:focusChannelMapDAO.getFocusChannelMapById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public FocusChannelMap getFocusChannelMapByCode(String code) throws BusinessException{
		try {
			return focusChannelMapDAO.getFocusChannelMapByCode(code);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<FocusChannelMap> getListFocusChannelMap(KPaging<FocusChannelMap> kPaging,Long focusProgramId,
			String saleTypeCode, ActiveType status) throws BusinessException {
		try {
			List<FocusChannelMap> lst = focusChannelMapDAO.getListFocusChannelMapByFocusProgramId(kPaging, focusProgramId, saleTypeCode, status);
			ObjectVO<FocusChannelMap> vo = new ObjectVO<FocusChannelMap>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override 
	public Boolean isUsingByOthers(Long focusProgramId) throws BusinessException {
		try{
			return focusProgramDAO.isUsingByOthers(focusProgramId);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override 
	public Boolean isDetailUsingByOthers(Long focusChannelMapProductId) throws BusinessException {
		try{
			return focusChannelMapProductDAO.isUsingByOthers(focusChannelMapProductId);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override 
	public Boolean isFocusChannelMapUsingByOthers(Long focusChannelMapId) throws BusinessException {
		try{
			return focusChannelMapDAO.isUsingByOthers(focusChannelMapId);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override 
	public Boolean isFocusShopMapUsingByOthers(Long focusShopMapId) throws BusinessException {
		try{
			return focusShopMapDAO.isUsingByOthers(focusShopMapId);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkIfFocusShopMapExist(long focusProgramId, String shopCode, Long id)
			throws DataAccessException, BusinessException {
		try{
			return focusShopMapDAO.checkIfRecordExist(focusProgramId, shopCode, id);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkIfFocusChannelMapExist(long focusProgramId, String saleTypeCode, Date fromDate, Date toDate, Long id)
			throws DataAccessException, BusinessException {
		try{
			return focusChannelMapDAO.checkIfRecordExist(focusProgramId, saleTypeCode, fromDate, toDate, id);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkIfFocusChannelMapProductExist(long focusProgramId, String saleTypeCode, String productCode, Long id)
			throws DataAccessException, BusinessException {
		try{
			saleTypeCode = saleTypeCode.toUpperCase();
			return focusChannelMapProductDAO.checkIfRecordExists(focusProgramId, saleTypeCode, productCode, id);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Shop getShopMapArea(long focusShopMapId) throws BusinessException {
		try{
			return focusShopMapDAO.getShopMapArea(focusShopMapId);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean createFocusChannelMapProduct(long focusProgramId, String saleTypeCode,
			String code, ProductType type, LogInfoVO logInfo) throws BusinessException {
		try{
			return focusChannelMapProductDAO.createFocusChannelMapProduct(focusProgramId, saleTypeCode, code, type, logInfo);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkIfProductInFocusProgram(Long focusProgramId, String productCode) 
			throws BusinessException {
		try{
			return focusChannelMapProductDAO.checkIfProductInFocusProgram(focusProgramId, productCode);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createListFocusChannelMapProduct(List<Long> lstProductId, Long focusProgramId, String saleTypeCode, LogInfoVO logInfo, String type) throws BusinessException {
		try {
			if (lstProductId == null || lstProductId.isEmpty()) {
				throw new IllegalArgumentException("lstProductId is null");
			}
			if (focusProgramId == null) {
				throw new IllegalArgumentException("focusProgramId is null");
			}
			if (saleTypeCode == null) {
				throw new IllegalArgumentException("channelTypeId is null");
			}
			if (logInfo == null) {
				throw new IllegalArgumentException("logInfo null");
			}
			saleTypeCode = saleTypeCode.toUpperCase();
			FocusProgram focusProgram = focusProgramDAO.getFocusProgramById(focusProgramId);
			if (focusProgram == null) {
				throw new IllegalArgumentException("focusProgram is null");
			}

			Date sysdate = commonDAO.getSysDate();

			//kiem tra co FocusChannelMap chua
			FocusChannelMap focusChannelMap = focusChannelMapDAO.getFocusChannelMapByProgramIdAndSaleTypeCode(focusProgramId, saleTypeCode);
			if (focusChannelMap == null) {
				//chua co tao ra moi
				focusChannelMap = new FocusChannelMap();
				focusChannelMap.setFocusProgram(focusProgram);
				focusChannelMap.setFromDate(focusProgram.getFromDate());
				focusChannelMap.setToDate(focusProgram.getToDate());
				focusChannelMap.setSaleTypeCode(saleTypeCode);
				focusChannelMap.setStatus(ActiveType.RUNNING);
				focusChannelMap.setCreateDate(sysdate);
				focusChannelMap.setCreateUser(logInfo.getStaffCode());
				
				focusChannelMap = focusChannelMapDAO.createFocusChannelMap(focusChannelMap, logInfo);
			}
			//Xu ly loai bo Id trung nhau
			List<Long> lstProductIdTmp = new ArrayList<>();
//			lstProductIdTmp.add(lstProductId.get(0));
			for (Long productId : lstProductId) {
				if (productId == null) {
					throw new IllegalArgumentException("productId is null");
				}
				//Xu ly bo qua cac id trung nhau
				if (lstProductIdTmp.indexOf(productId) > -1) {
					continue;
				}
				lstProductIdTmp.add(productId);
				
				Product product = productDAO.getProductById(productId);
				if (product == null) {
					throw new IllegalArgumentException("product is null");
				}
				if (ActiveType.RUNNING.equals(product.getStatus())) {
					//co san pham trong chuong trinh trong tam
					FocusChannelMapProduct focusChannelMapProduct = focusChannelMapProductDAO.getFocusChannelMapProduct(focusProgramId, productId);
					if (focusChannelMapProduct == null) {
						//chua co tao moi
						focusChannelMapProduct = new FocusChannelMapProduct();
						focusChannelMapProduct.setFocusChannelMap(focusChannelMap);
						focusChannelMapProduct.setProduct(product);
						focusChannelMapProduct.setType(type);
						focusChannelMapProduct.setCreateDate(sysdate);
						focusChannelMapProduct.setCreateUser(logInfo.getStaffCode());

						focusChannelMapProductDAO.createFocusChannelMapProduct(focusChannelMapProduct, logInfo);
					} else {
						focusChannelMapProduct = focusChannelMapProduct.clone();
						focusChannelMapProduct.setFocusChannelMap(focusChannelMap);
						focusChannelMapProduct.setType(type);
						focusChannelMapProduct.setUpdateDate(sysdate);
						focusChannelMapProduct.setUpdateUser(logInfo.getStaffCode());

						focusChannelMapProductDAO.updateFocusChannelMapProduct(focusChannelMapProduct, logInfo);
					}
				}
			}
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<ProductCatVO> getListProductCatVO(String productCode,
			String productName, Long exFpId, Integer limit) throws BusinessException {
		try {
			List<Product> lst = productDAO.getListFocusProductTreeVO(productCode, productName,
					exFpId, limit);
			ArrayList<ProductCatVO> ret = new ArrayList<ProductCatVO>();
			ProductCatVO pdCat = null;
			ProductSubCatVO pdSubCat = null;
			Long catId = 0L;
			Long subCatId = 0L;
			for (Product product : lst) {
				Long cat = product.getCat().getId();
				if (cat != null) {
					if (catId.compareTo(cat) != 0) {
						catId = cat;
						//
						pdCat = new ProductCatVO();
						pdCat.setProductInfo(product.getCat());
						//
						ret.add(pdCat);
						//
						subCatId = 0L;
					}
					Long subCat = product.getSubCat().getId();
					if (subCat != null) {
						if (subCatId.compareTo(subCat) != 0) {
							subCatId = subCat;
							//
							pdSubCat = new ProductSubCatVO();
							pdSubCat.setProductInfo(product.getSubCat());
							//
							pdCat.getLstProductSubCatVO().add(pdSubCat);
						}
					}
				}
				pdSubCat.getLstProduct().add(product);
			}
			return ret;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkShopFamilyInFocusProgram(Long shopId, Long focusProgramId)
			throws BusinessException {
		try {
			return focusShopMapDAO.checkShopFamilyInFocusProgram(shopId, focusProgramId);
		}
		catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkShopAncestorInFocusProgram(Long focusProgramId, Long shopId) 
			throws BusinessException {
		try{
			return focusShopMapDAO.checkShopAncestorInFocusProgram(focusProgramId, shopId);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Boolean checkIfSaleTypeExistProduct(long focusProgramId,
			String saleTypeCode) throws BusinessException {
		try {
			return focusChannelMapDAO.checkIfSaleTypeExistProduct(
					focusProgramId, saleTypeCode);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkIfAncestorInFp(Long shopId, Long focusProgramId) throws BusinessException {
		try {
			return focusShopMapDAO.checkIfAncestorInFp(shopId, focusProgramId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkExitsFocusProgramWithParentShop(Long focusProgramid,
			String shopCode) throws BusinessException {
		try {
			return focusShopMapDAO.checkExitsFocusProgramWithParentShop(focusProgramid, shopCode);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<ProductCatVO> getListProductCatVOEx(String productCode,
			String productName, Long exFpId) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProductInfo> getListSubCat(String productCode,
			String productName, Long exFpId, Long catId)
			throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Product> getListProductBySubCat(String productCode,
			String productName, Long exFpId, Long catId, Long subCatId)
			throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ObjectVO<HTBHVO> getListHTBHVO(HTBHFilter filter, KPaging<HTBHVO> kPaging) throws BusinessException {
		try {
			ObjectVO<HTBHVO> res = new ObjectVO<HTBHVO>();
			List<HTBHVO> lst = focusProgramDAO.getListHTBHVO(filter, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public FocusProgram copyFocusProgram(Long focusProgramId, LogInfoVO logInfo, String fpCode, String fpName)
			throws BusinessException {
		Date sysDate = null;
		if (StringUtility.isNullOrEmpty(fpCode)) {
			throw new IllegalArgumentException("focus programe code is empty");
		}
		if (StringUtility.isNullOrEmpty(fpName)) {
			throw new IllegalArgumentException("focus programe name is empty");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo null");
		}
		try {
			sysDate = this.commonDAO.getSysDate();
		} catch (Exception ex) {
			throw new BusinessException("Can't get sysdate ...");
		}

		FocusProgram originalFP = this.getFocusProgramById(focusProgramId);
		FocusProgram newFP = new FocusProgram();

		// focus_program_id
		newFP.setFocusProgramCode(fpCode.toUpperCase());
		newFP.setFocusProgramName(fpName);
		newFP.setStatus(ActiveType.WAITING);
		newFP.setFromDate(originalFP.getFromDate());
		newFP.setToDate(originalFP.getToDate());
		newFP.setCreateDate(sysDate);
		newFP.setCreateUser(logInfo.getStaffCode());

		// newFP.setNameText(originalFP.getNameText());

		List<FocusShopMap> listFocusShopMap = null;
		List<FocusChannelMap> listFocusChannelMap = null;
		
		try {
			listFocusShopMap = this.focusShopMapDAO.getListFocusShopMapByFocusProgramId(null, focusProgramId,null, null, ActiveType.RUNNING, null);
			listFocusChannelMap = this.focusChannelMapDAO.getListFocusChannelMapByFocusProgramId(null,focusProgramId, null, ActiveType.RUNNING);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		
		newFP = this.createFocusProgram(newFP, logInfo);

		for (FocusShopMap oriFocusShopMap : listFocusShopMap) {
			FocusShopMap newFocusShopMap = new FocusShopMap();

			newFocusShopMap.setShop(oriFocusShopMap.getShop());
			newFocusShopMap.setStatus(oriFocusShopMap.getStatus());
			newFocusShopMap.setCreateDate(sysDate);
			newFocusShopMap.setCreateUser(logInfo.getStaffCode());

			newFocusShopMap.setFocusProgram(newFP);

			try {
				newFocusShopMap = this.focusShopMapDAO.createFocusShopMap(newFocusShopMap, logInfo);
			} catch (DataAccessException ex) {
				throw new BusinessException(ex);
			}
		}

		for (FocusChannelMap item : listFocusChannelMap) {
			List<FocusChannelMapProduct> listFocusChannelMapProduct;

			try {
				listFocusChannelMapProduct = this.focusChannelMapProductDAO.getListFocusChannelMapProductByFocusProgramId(null,null, null, originalFP.getId(), item.getId(),null);
			} catch (DataAccessException e) {
				throw new BusinessException(e);
			}

			FocusChannelMap focusChannelMap = new FocusChannelMap();

			focusChannelMap.setFocusProgram(newFP);
			focusChannelMap.setStatus(item.getStatus());
			focusChannelMap.setFromDate(item.getFromDate());
			focusChannelMap.setToDate(item.getToDate());
			focusChannelMap.setSaleTypeCode(item.getSaleTypeCode());
			focusChannelMap.setCreateDate(sysDate);
			focusChannelMap.setCreateUser(logInfo.getStaffCode());

			focusChannelMap = this.createFocusChannelMap(focusChannelMap,logInfo);

			for (FocusChannelMapProduct oriFocusChannelMapProduct : listFocusChannelMapProduct) {
				FocusChannelMapProduct newFocusChannelMapProduct = new FocusChannelMapProduct();

				newFocusChannelMapProduct.setFocusChannelMap(focusChannelMap);
				newFocusChannelMapProduct.setProduct(oriFocusChannelMapProduct.getProduct());
				newFocusChannelMapProduct.setType(oriFocusChannelMapProduct.getType());
				newFocusChannelMapProduct.setCreateDate(sysDate);
				newFocusChannelMapProduct.setCreateUser(logInfo.getStaffCode());

				try {
					newFocusChannelMapProduct = this.focusChannelMapProductDAO.createFocusChannelMapProduct(newFocusChannelMapProduct, logInfo);
				} catch (DataAccessException ex) {
					throw new BusinessException(ex);
				}
			}
		}

		return newFP;
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void createListFocusChannelMap(List<FocusChannelMap> listUpdate,
			List<FocusChannelMap> listAdd, LogInfoVO logInfo)
			throws BusinessException {
		try {
			for (FocusChannelMap item : listAdd) {
				this.focusChannelMapDAO.createFocusChannelMap(item, logInfo);
			}

			for (FocusChannelMap item : listUpdate) {
				if(ActiveType.DELETED.equals(item.getStatus()) && item.getFocusProgram() != null ){
					List<FocusChannelMapProduct> lstProduct = this.focusChannelMapProductDAO.getListFocusChannelMapProductByFocusProgramId(null, null, null, item.getFocusProgram().getId(), item.getId(), null);
					for(FocusChannelMapProduct product: lstProduct){
						this.focusChannelMapProductDAO.deleteFocusChannelMapProduct(product, logInfo);
					}
				}
				this.focusChannelMapDAO.updateFocusChannelMap(item, logInfo);
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean isExistChildJoinProgram(String shopCode, Long focusProgramId)
			throws BusinessException{
		try {
			return focusShopMapDAO.isExistChildJoinProgram(shopCode, focusProgramId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public boolean checkExistsProductMap(long focusId, String saleTypeCode,
			String prodCode, String type) throws BusinessException {
		try {
			return focusChannelMapProductDAO.checkExistsProductMap(focusId, saleTypeCode, prodCode, type);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public int countFocusProgram(FocusProgramFilter filter) throws BusinessException {
		try {
			return focusProgramDAO.countFocusProgram(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
}

package ths.dms.core.business;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.enumtype.CmsFilter;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.CmsVO;
import ths.dms.core.entities.vo.RoleVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.TreeVOBasic;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.CmsDAO;
import ths.dms.core.dao.ShopDAO;

public class CmsMgrImpl implements CmsMgr {
	@Autowired
	CmsDAO cmsDAO;
	@Autowired
	ShopDAO shopDAO;

	/**
	 * @author hunglm16
	 * @since September 30,2014
	 * @description Lay danh sanh man hinh
	 * */
	@Override
	public List<CmsVO> getListFormControlByRole(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getListFormControlByRole(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<CmsVO> getListUrl(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getListUrl(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public TreeVOBasic<ShopVO> getListOrgAccess(CmsFilter filter) throws BusinessException {
		try {
			List<ShopVO> lstShop = cmsDAO.getListOrgAccess(filter);
			TreeVOBasic<ShopVO> root = new TreeVOBasic<ShopVO>();
			if (lstShop != null && !lstShop.isEmpty()) {
				for (ShopVO shopVO : lstShop) {
					if (shopVO.getParentId() == null || shopVO.getParentId() <= 0) {
						root.setId(shopVO.getShopId());
						root.setCode(shopVO.getShopCode());
						root.setName(shopVO.getShopName());
						root.setIsLevel(shopVO.getIsLevel());
						root.setObjectType(shopVO.getObjectType());
						root.setAttribute(shopVO);
						break;
					}
				}
			}
			if (root != null) {
				root.setChildren(this.getChildShopNewTreeVO(root.getAttribute(), lstShop));
			}
			return root;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ShopVO> getListShopInOrgAccess(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getListShopInOrgAccess(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author hunglm16
	 * @since JULY 18,2014
	 * @description Tao node cho cay don vi
	 * */
	private List<TreeVOBasic<ShopVO>> getChildShopNewTreeVO(ShopVO shop, List<ShopVO> lst) {
		List<TreeVOBasic<ShopVO>> res = new ArrayList<TreeVOBasic<ShopVO>>();
		for (ShopVO obj : lst) {
			if (obj.getParentId() != null && shop.getShopId().compareTo(obj.getParentId()) == 0) {
				TreeVOBasic<ShopVO> vo = new TreeVOBasic<ShopVO>();
				vo.setId(obj.getShopId());
				vo.setCode(obj.getShopCode());
				vo.setName(obj.getShopName());
				vo.setParentId(shop.getShopId());
				vo.setIsLevel(obj.getIsLevel());
				vo.setObjectType(obj.getObjectType());
				vo.setAttribute(obj);
				vo.setText(obj.getShopCode() + " - " + obj.getShopName());
				res.add(vo);
			}
		}
		for (TreeVOBasic<ShopVO> childrenVO : res) {
			childrenVO.setChildren(this.getChildShopNewTreeVO(childrenVO.getAttribute(), lst));
			if (childrenVO.getChildren() == null || childrenVO.getChildren().size() == 0) {
				childrenVO.setState("leaf");
			}
		}
		return res;
	}

	@Override
	public List<CmsVO> getListShopInRoleByUser(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getListShopInRoleByUser(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<CmsVO> getListRoleByUser(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getListRoleByUser(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author hunglm16
	 * @since AUGUST 14,2014
	 * @description Tao cay don vi voi Don vi chon mac dinh
	 * */
	@Override
	public TreeVOBasic<ShopVO> getTreeFullShopVOByFilter(BasicFilter<ShopVO> filter) throws BusinessException {
		try {
			List<ShopVO> lstShop = shopDAO.getListChildShopVOByFilter(filter);
			TreeVOBasic<ShopVO> root = new TreeVOBasic<ShopVO>();
			if (lstShop == null || lstShop.isEmpty()) {
				//lstShop = shopDAO.getListChildShopVOByShopRoot(filter.getId(), ActiveType.RUNNING.getValue());
				lstShop = shopDAO.searchListChildShopVOByFilter(filter);
			}
			for (ShopVO shopVO : lstShop) {
				if (filter.getId().equals(shopVO.getShopId())) {
					root.setId(shopVO.getShopId());
					root.setText(shopVO.getShopCode() + " - " + shopVO.getShopName());
					root.setCode(shopVO.getShopCode());
					root.setName(shopVO.getShopName());
					root.setIsLevel(shopVO.getIsLevel());
					root.setAttribute(shopVO);
					break;
				}
			}
			if (root == null || root.getAttribute() == null) {
				ShopVO shopVO = shopDAO.getShopVOByShopId(filter.getId());
				root.setId(shopVO.getShopId());
				root.setText(shopVO.getShopCode() + " - " + shopVO.getShopName());
				root.setCode(shopVO.getShopCode());
				root.setName(shopVO.getShopName());
				root.setIsLevel(shopVO.getIsLevel());
				root.setAttribute(shopVO);
			}
			root.setChildren(this.getChildShopNewTreeVO(root.getAttribute(), lstShop));
			return root;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author hunglm16
	 * @since AUGUST 14,2014
	 * @description Tao cay don vi voi user dang nhap theo Org_access
	 * */
	@Override
	public TreeVOBasic<ShopVO> getTreeShopByRole(CmsFilter filter) throws BusinessException {
		try {
			List<ShopVO> lstShop = cmsDAO.getListShopByRole(filter);
			TreeVOBasic<ShopVO> root = new TreeVOBasic<ShopVO>();
			if (lstShop != null && !lstShop.isEmpty()) {
				for (ShopVO shopVO : lstShop) {
					if (shopVO.getParentId() == null || shopVO.getParentId() <= 0) {
						root.setId(shopVO.getShopId());
						root.setText(shopVO.getShopCode() + " - " + shopVO.getShopName());
						root.setCode(shopVO.getShopCode());
						root.setName(shopVO.getShopName());
						root.setIsLevel(shopVO.getIsLevel());
						root.setAttribute(shopVO);
						break;
					}
				}
			}
			if (root != null) {
				root.setChildren(this.getChildShopNewTreeVO(root.getAttribute(), lstShop));
			}
			return root;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Tao cay Form
	 * 
	 * @author hunglm16
	 * @since September 30,2014
	 * */
	@Override
	public TreeVOBasic<CmsVO> getTreeFormControlByRole(CmsFilter filter) throws BusinessException {
		try {
			List<CmsVO> lstForm = cmsDAO.getListFormControlByRole(filter);
			TreeVOBasic<CmsVO> root = new TreeVOBasic<CmsVO>();
			if (lstForm != null && !lstForm.isEmpty()) {
				for (CmsVO vo : lstForm) {
					root.setId(vo.getFormId());
					root.setText(vo.getFormName());
					root.setCode(vo.getFormCode());
					root.setName(vo.getFormName());
					root.setParentId(vo.getParentFormId());
					root.setUrl(vo.getUrl());
					root.setAttribute(vo);
					break;
				}
			}
			if (root != null) {
				root.setChildren(this.getChildFormNewTreeVO(root.getAttribute(), lstForm));
			}
			return root;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author hunglm16
	 * @since JULY 18,2014
	 * @description Tao node cho cay form
	 * */
	private List<TreeVOBasic<CmsVO>> getChildFormNewTreeVO(CmsVO shop, List<CmsVO> lst) {
		List<TreeVOBasic<CmsVO>> res = new ArrayList<TreeVOBasic<CmsVO>>();
		for (CmsVO obj : lst) {
			if (obj.getParentId() != null && shop.getFormId().compareTo(obj.getParentId()) == 0) {
				TreeVOBasic<CmsVO> vo = new TreeVOBasic<CmsVO>();
				vo.setId(obj.getFormId());
				vo.setText(obj.getFormName());
				vo.setCode(obj.getFormCode());
				vo.setName(obj.getFormName());
				vo.setParentId(obj.getParentFormId());
				vo.setUrl(obj.getUrl());
				vo.setAttribute(obj);
				res.add(vo);
			}
		}
		for (TreeVOBasic<CmsVO> childrenVO : res) {
			childrenVO.setChildren(this.getChildFormNewTreeVO(childrenVO.getAttribute(), lst));
		}
		return res;
	}

	@Override
	public List<ShopVO> getListShopChildrenByRole(CmsFilter filter) throws BusinessException {
		TreeVOBasic<ShopVO> root = this.getTreeShopByRole(filter);
		List<ShopVO> lstData = new ArrayList<ShopVO>();
		try {
			lstShopVO = new ArrayList<ShopVO>();
			this.getListShopByTreeShop(root, lstShopVO);
			if (lstShopVO != null && !lstShopVO.isEmpty()) {
				List<Long> lstId = new ArrayList<Long>();
				for (ShopVO vo : lstShopVO) {
					lstId.add(vo.getShopId());
				}
				filter.setLstId(lstId);
				lstData = cmsDAO.getListShopByListParentId(filter);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		lstShopVO = null;
		return lstData;
	}

	@Override
	public List<ShopVO> getListShopByLstIdParent(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getListShopByListParentId(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public CmsVO getAppTockenVOByCode(String code) throws BusinessException {
		try {
			return cmsDAO.getApplicationVOByCode(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author hunglm16
	 * @since AUGUST 14,2014
	 * @description get List Shop with Permission Data by User
	 * */
	private void getListShopByTreeShop(TreeVOBasic<ShopVO> node, List<ShopVO> lstShopVO) {
		if (node != null) {
			List<TreeVOBasic<ShopVO>> children = node.getChildren();
			if (children != null && !children.isEmpty()) {
				for (TreeVOBasic<ShopVO> nodeC : children) {
					this.getListShopByTreeShop(nodeC, lstShopVO);
				}
			} else {
				ShopVO shopTK = new ShopVO();
				shopTK.setShopId(node.getId());
				shopTK.setShopCode(node.getCode());
				shopTK.setShopName(node.getName());
				shopTK.setParentId(node.getParentId());
				lstShopVO.add(shopTK);
			}
		}
	}

	/**
	 * Lay danh sach shop thoa man quyen du lieu
	 * 
	 * @author hunglm16
	 * @since August 21,2014
	 * @return List ShopVO
	 * @description Ham lay tat ca danh sach tinh ca con chau cua no voi nhieu
	 *              tham so
	 * */
	@Override
	public List<ShopVO> getFullShopInOrgAccessByFilter(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getFullShopInOrgAccessByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<StaffVO> getListStaffByUserWithRole(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getListStaffByUserWithRole(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<CmsVO> getListFormInRoleByFilter(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getListFormInRoleByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<CmsVO> getListFormInRoleForTreeByFilter(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getListFormInRoleForTreeByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public int checkShopInCMSByFilter(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.checkShopInCMSByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<RoleVO> getListOwnerRoleShopVOByStaff(Long staffId) throws BusinessException {
		try {
			return cmsDAO.getListOwnerRoleShopVOByStaff(staffId);
		} catch (Exception e) {
			throw new BusinessException("Can't get list sub shop " + e.getMessage(), e);
		}
	}
	
	@Override
	public List<ShopVO> getListShopVOByStaffVO(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getListShopVOByStaffVO(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage(), e);
		}
	}
	
	@Override
	public List<CmsVO> getListChildStaffInherit(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getListChildStaffInherit(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage(), e);
		}
	}
	
	@Override
	public List<CmsVO> getListChildShopInherit(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getListChildShopInherit(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage(), e);
		}
	}
	
	@Override
	public List<ShopVO> getFullShopOwnerByFilter(CmsFilter filter) throws BusinessException {
		try {
			return cmsDAO.getFullShopOwnerByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public CmsVO getFormCmsVoByUrl(String url, Integer permissionType, Long roleId, Long userId, Long inheritStaffId) throws BusinessException {
		try {
			return cmsDAO.getFormCmsVoByUrl(url, permissionType, roleId, userId, inheritStaffId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/////////////////////// GETTER/SETTER ////////////////////////////////////////////////////////
	private List<ShopVO> lstShopVO = new ArrayList<ShopVO>();

	public List<ShopVO> getLstShopVO() {
		return lstShopVO;
	}

	public void setLstShopVO(List<ShopVO> lstShopVO) {
		this.lstShopVO = lstShopVO;
	}
}

package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.RoutingCar;
import ths.dms.core.entities.RoutingCarCust;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.CarFilter;
import ths.dms.core.entities.vo.CarVO;
import ths.dms.core.entities.vo.LatLngVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.SuperviseLogisticsDAO;

public class SuperviserLogisticsMgrImpl implements SuperviserLogisticsMgr {


	@Autowired
	SuperviseLogisticsDAO superviseLogisticsDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public void createListCustomerArea(List<LatLngVO> lst, LogInfoVO logInfo) throws BusinessException {
		try {
			superviseLogisticsDAO.createListCustomerArea(lst, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteCustomerArea(Long shopId, LogInfoVO logInfo) throws BusinessException{
		try {
			superviseLogisticsDAO.deleteCustomerArea(shopId, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CarVO> getListShopCar(Long parentShopId)
			throws BusinessException {
		try {
			List<CarVO> list=superviseLogisticsDAO.getListShopCar(parentShopId);
			return list;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CarVO> getListCar(Long parentShopId)
			throws BusinessException {
		try {
			List<CarVO> list=superviseLogisticsDAO.getListCar(parentShopId);
			return list;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public List<CarVO> getListShopCarOneNode(Long parentShopId) throws BusinessException {
		try {
			List<CarVO> list=superviseLogisticsDAO.getListShopCarOneNode(parentShopId);
			return list;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Shop> getListAncestor(Long parentShopId, String shopCode,
			String shopName) throws BusinessException {
		try {
			List<Shop> list=superviseLogisticsDAO.getListAncestor(parentShopId,shopCode,shopName);
			return list;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<CarVO> getListCarKP(CarFilter filter) throws BusinessException {
		try {
			List<CarVO> lst = superviseLogisticsDAO.getListCarKP(filter);
			ObjectVO<CarVO> vo = new ObjectVO<CarVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CarVO> getListCarVungKP(CarFilter filter) throws BusinessException {
		try {
			List<CarVO> lst = superviseLogisticsDAO.getListCarVungKP(filter);
			ObjectVO<CarVO> vo = new ObjectVO<CarVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CarVO> getListRoutingExistKP(CarFilter filter) throws BusinessException {
		try {
			List<CarVO> lst = superviseLogisticsDAO.getListRoutingExistKP(filter);
			ObjectVO<CarVO> vo = new ObjectVO<CarVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CarVO> getListCustShop(Long parentShopId) throws BusinessException {
		try {
			return superviseLogisticsDAO.getListCustShop(parentShopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CarVO> getListCarPosition(Long parentShopId,Long maxLogId) throws BusinessException {
		try {
			return superviseLogisticsDAO.getListCarPosition(parentShopId,maxLogId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CarVO> getListRoutingCustPosition(CarFilter filter) throws BusinessException {
		try {
			return superviseLogisticsDAO.getListRoutingCustPosition(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CarVO> getListRoutingCustStatus(CarFilter filter) throws BusinessException {
		try {
			return superviseLogisticsDAO.getListRoutingCustStatus(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<LatLngVO> getListCustomerArea(Long parentShopId) throws BusinessException {
		try {
			return superviseLogisticsDAO.getListCustomerArea(parentShopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkDriverHasCar(Long idRouting, Long staffId,Date deliveryDate) throws BusinessException {
		try {
			return superviseLogisticsDAO.checkDriverHasCar(idRouting,staffId,deliveryDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<LatLngVO> getListCustomerByShop(CarFilter filter) throws BusinessException {
		try {
			return superviseLogisticsDAO.getListCustomerByShop(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public RoutingCar getRoutingCarById(Long id) throws BusinessException{
		try {
			return id==null?null:superviseLogisticsDAO.getRoutingCarById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RoutingCarCust getRoutingCarCustById(Long id) throws BusinessException{
		try {
			return id==null?null:superviseLogisticsDAO.getRoutingCarCustById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RoutingCar createRoutingCar(RoutingCar rcar, LogInfoVO logInfo) throws BusinessException {
		try {
			return superviseLogisticsDAO.createRoutingCar(rcar, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	@Override
	public RoutingCarCust createRoutingCarCust(RoutingCarCust rcar, LogInfoVO logInfo) throws BusinessException {
		try {
			return superviseLogisticsDAO.createRoutingCarCust(rcar, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateRoutingCar(RoutingCar rcar, LogInfoVO logInfo) throws BusinessException {
		try {
			superviseLogisticsDAO.updateRoutingCar(rcar, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateRoutingCarCust(RoutingCarCust rcar, LogInfoVO logInfo) throws BusinessException {
		try {
			superviseLogisticsDAO.updateRoutingCarCust(rcar, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CarVO> getListCustomerByRouting(CarFilter filter) throws BusinessException {
		try {
			List<CarVO> lst = superviseLogisticsDAO.getListCustomerByRouting(filter);
			ObjectVO<CarVO> vo = new ObjectVO<CarVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<CarVO> getListCustomerBySaleOrder(CarFilter filter) throws BusinessException {
		try {
			List<CarVO> lst = superviseLogisticsDAO.getListCustomerBySaleOrder(filter);
			ObjectVO<CarVO> vo = new ObjectVO<CarVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<RoutingCar> getRoutingCarByFilter(CarFilter filter) throws BusinessException {
		try {
			return superviseLogisticsDAO.getRoutingCarByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CarVO> getBCVT11(CarFilter filter) throws BusinessException {
		try {
			return superviseLogisticsDAO.getBCVT11(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CarVO> getBCVT12(CarFilter filter) throws BusinessException {
		try {
			return superviseLogisticsDAO.getBCVT12(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CarVO> getBCVT13(CarFilter filter) throws BusinessException {
		try {
			return superviseLogisticsDAO.getBCVT13(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CarVO> getBCVT14(CarFilter filter) throws BusinessException {
		try {
			return superviseLogisticsDAO.getBCVT14(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CarVO> getBCVT15(CarFilter filter) throws BusinessException {
		try {
			return superviseLogisticsDAO.getBCVT15(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<CarVO> getBCVT16(CarFilter filter) throws BusinessException {
		try {
			return superviseLogisticsDAO.getBCVT16(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

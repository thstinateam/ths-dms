package ths.dms.core.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProgramObjectType;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.SpParam;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.RptStaffSaleFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.NewTreeVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.UnitFilter;

import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.FocusShopMapDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.repo.IRepository;

public class ShopMgrImpl implements ShopMgr {

	@Autowired
	private IRepository repo;
	
	@Autowired
	ShopDAO shopDAO;
	
	@Autowired
	FocusShopMapDAO focusShopMapDAO;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public Shop createShop(Shop shop, LogInfoVO logInfo) throws BusinessException {
		try {
			return shopDAO.createShop(shop, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateShop(Shop shop, LogInfoVO logInfo) throws BusinessException {
		try {
			shopDAO.updateShop(shop, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public TreeVO<Shop> getShopTreeVO(Shop shop, Long exceptionalShop, String shopCode, String shopName,Boolean isOrderByName) throws BusinessException {
		return this.getShopTreeVOEx(shop.getId(), Arrays.asList(exceptionalShop), shopCode, shopName, ActiveType.RUNNING, isOrderByName);
	}
	
	@Override
	public List<Shop> getListSubShopExSaleMT(Long parentId,Boolean oneLevel) throws BusinessException {
		try {
			return shopDAO.getListSubShopSaleMT(parentId,oneLevel);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Shop> getListSubShopForFocusProgram(Long parentId, ActiveType status,
			Long focusProgramId) throws BusinessException {
		try {
			return shopDAO.getListSubShop(parentId, null, null, status, focusProgramId, ProgramObjectType.FOCUS_PROGRAM, null);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Shop> getListSubShop(Long parentId, ActiveType status) throws BusinessException {
		try {
			return shopDAO.getListSubShop(parentId, null, null, status, null, null, null);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Shop> getListSubShopForPromotionProgram(Long parentId, ActiveType status,
			Long promotionProgramId) throws BusinessException {
		try {
			return shopDAO.getListSubShop(parentId, null, null, status, promotionProgramId, ProgramObjectType.PROMOTION_PROGRAM, null);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Shop> getListSubShopExSaleMTIncludeStopped(Long parentId,Boolean oneLevel, Boolean includeStopped) throws BusinessException {
		try {
			return shopDAO.getListSubShopSaleMTIncludeStopped(parentId,oneLevel, includeStopped);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public TreeVO<Shop> getShopTreeVOEx(Long shopId,
			List<Long> exceptionalShopId, String shopCode, String shopName, ActiveType status,Boolean isOrderByName)
			throws BusinessException {
		try {
			if (shopId == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			TreeVO<Shop> vo = new TreeVO<Shop>();
			List<Shop> lst = shopDAO.getSearchShopTreeVO(shopId,
					exceptionalShopId, shopCode, shopName, status, null, null, isOrderByName);
			ArrayList<Shop> lstShop = new ArrayList<Shop>();
			for (Shop shop : lst) {
				if (shop.getId() != null) {
					if (shop.getId().compareTo(shopId) == 0) {
						vo.setObject(shop);
					} else {
						lstShop.add(shop);
					}
				}
			}
			vo.setListChildren(getChildShopTreeVO(vo.getObject(), lstShop));
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	private List<TreeVO<Shop>> getChildShopTreeVO(Shop shop, List<Shop> lst) {
		ArrayList<TreeVO<Shop>> ret = new ArrayList<TreeVO<Shop>>();
		for (Shop obj : lst) {
			if (obj.getParentShop() != null && shop.getId().compareTo(obj.getParentShop().getId()) == 0) {
				TreeVO<Shop> vo = new TreeVO<Shop>();
				vo.setObject(obj);
				ret.add(vo);
			}
		}
		for (TreeVO<Shop> treeVO : ret) {
			lst.remove(treeVO.getObject());
		}
		for (TreeVO<Shop> treeVO : ret) {
			treeVO.setListChildren(getChildShopTreeVO(treeVO.getObject(), lst));
		}
		return ret;
	}
	
	@Override
	public List<Shop> getListShopByCodeOrName(String searchStr , Long shopId)
			throws BusinessException {
		List<Shop> listShop;
		
		try {
			listShop = shopDAO.getListShopSaleMTByShopCode(searchStr, shopId);
			
			if(listShop == null || listShop.size() == 0){
				listShop = shopDAO.getListShopSaleMTByShopName(searchStr, shopId);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		
		if(listShop == null){
			listShop = new ArrayList<Shop>();
		}
		return listShop;
	}
	
	@Override
	public void deleteShop(Shop shop, LogInfoVO logInfo) throws BusinessException{
		try {
			shopDAO.deleteShop(shop, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Shop getShopById(Long id) throws BusinessException{
		try {
			return id==null?null:shopDAO.getShopById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	/*****************************	CTTT ****************************************/
	@Override
	public NewTreeVO<Shop, ProgrameExtentVO> getShopTreeInFocusProgram(Long focusProgramId,
			String shopCode, String shopName, Long parentShopId) throws BusinessException {
		try {
			List<Shop> lstShop = focusShopMapDAO.getListShopInFocusProgram(focusProgramId, shopCode, shopName, parentShopId, true);
			// List<ProgrameExtentVO> lstQuant = focusShopMapDAO.getListProgrameExtentVOEx(lstShop, focusProgramId);
			List<ProgrameExtentVO> lstQuant = focusShopMapDAO.getListProgrameExtentVO(lstShop, focusProgramId);
			SortedMap<Long, ProgrameExtentVO> map = new TreeMap<Long, ProgrameExtentVO>();
			
			for (ProgrameExtentVO vo: lstQuant) {
				map.put(vo.getShopId(), vo);
			}
			
			Set<Shop> topShop = new HashSet<Shop>();
			for (Shop s: lstShop) {
				Shop tmp = s;
				while (tmp.getParentShop() != null) {
					tmp = tmp.getParentShop();
				}
				topShop.add(tmp);
			}
			
			List<NewTreeVO<Shop, ProgrameExtentVO>> lstChildren = new ArrayList<NewTreeVO<Shop, ProgrameExtentVO>>();
			for (Shop s: topShop) {
				NewTreeVO<Shop, ProgrameExtentVO> child = new NewTreeVO<Shop, ProgrameExtentVO>();
				child.setObject(s);
				child.setDetail(map.get(s.getId()));
				child.setListChildren(this.getChildShopNewTreeVOEx(s, lstShop, map));
				lstChildren.add(child);
			}
			
			NewTreeVO<Shop, ProgrameExtentVO> rs = new NewTreeVO<Shop, ProgrameExtentVO>();
			rs.setListChildren(lstChildren);
			
			return rs;
		}
		catch (DataAccessException ex) {
			throw new BusinessException();
		}
	}
	
	@Override
	public Shop getShopByIdAndIslevel(Long id, Integer isLevel)  throws BusinessException{
		try {
			return shopDAO.getShopByIdAndIslevel(id, isLevel);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Shop getShopByCode(String code) throws BusinessException {
		try {
			return shopDAO.getShopByCode(code);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Shop getShopRunningByCode(String code) throws BusinessException {
		try {
			return shopDAO.getShopRunningByCode(code);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Shop> getListShopByFilter(BasicFilter<Shop> filter) throws BusinessException {
		try {
			List<Shop> lst = shopDAO.getListShopByFilter(filter);
			ObjectVO<Shop> vo = new ObjectVO<Shop>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ShopVO> getListShopVOBasicByFilter(BasicFilter<ShopVO> filter) throws BusinessException {
		try {
			List<ShopVO> lst = shopDAO.getListShopVOBasicByFilter(filter);
			ObjectVO<ShopVO> vo = new ObjectVO<ShopVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ShopVO> getListShopVOBasicByFilter_GTMT(BasicFilter<ShopVO> filter) throws BusinessException {
		try {
			List<ShopVO> lst = shopDAO.getListShopVOBasicByFilter_GTMT(filter);
			ObjectVO<ShopVO> vo = new ObjectVO<ShopVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Shop getShopRoot(Long shopId) throws BusinessException {
		try{
			return shopDAO.getShopRoot(shopId);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<Shop> getListSubShopEx(Long parentId, ActiveType status, Long exceptionalShopId) throws BusinessException {
		try {
			return shopDAO.getListSubShop(parentId, null, null, status, null, null, exceptionalShopId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<Shop> getListShopNotInPoGroup(ShopFilter filter, long shopId) throws BusinessException {
		try {
			List<Shop> lst = shopDAO.getListShopNotInPoGroup(filter, shopId);
			ObjectVO<Shop> vo = new ObjectVO<Shop>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Object getDMDHT(Long shopId) throws BusinessException {
		try{
			return shopDAO.getDMDHT(shopId);
		}catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Shop> getAllShopChildrenByLstId(List<Long> lstId) throws BusinessException {
		try {
			return shopDAO.getAllShopChildrenByLstId(lstId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Shop> getListShop(ShopFilter filter) throws BusinessException {
		try {
			List<Shop> lst = shopDAO.getListShop(filter);
			ObjectVO<Shop> vo = new ObjectVO<Shop>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkIfShopExists(ShopFilter filter) throws BusinessException {
		try {
			return shopDAO.checkIfShopExists(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateShopWithCancelSalerTrainingPlan(Shop shop, LogInfoVO logInfo) throws BusinessException {
		try {
			shopDAO.updateShop(shop, logInfo);
			shopDAO.cancelTrainingPlanOfSaler(shop.getId());
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<Shop> getListShopOfUnitTree(KPaging<Shop> kPaging, Long curShopId, String shopCode,String shopName, ActiveType status) throws BusinessException {
		try {
			List<Shop> lst = shopDAO.getListShopOfUnitTree(kPaging, curShopId, shopCode, shopName, status);
			ObjectVO<Shop> vo = new ObjectVO<Shop>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public NewTreeVO<Shop, ProgrameExtentVO> getShopTreeVOForOfficeDocument(Long userId, Long roleId, Long shopId, Long offDocuID, String shopCode, String shopName) throws BusinessException {
		try {
			BasicFilter<ShopVO> filter = new BasicFilter<ShopVO>();
			filter.setUserId(userId);
			filter.setId(shopId);
			filter.setLongG(roleId);
			filter.setCode(shopCode);
			filter.setName(shopName);
			List<Shop> lst = shopDAO.getListChildShopByFilterEx(filter);
			List<ProgrameExtentVO> lstQuant = shopDAO.getListProgrameExtentVOExOD(lst, offDocuID);
			
			SortedMap<Long, ProgrameExtentVO> map = new TreeMap<Long, ProgrameExtentVO>();
			for (ProgrameExtentVO vo: lstQuant) {
				map.put(vo.getShopId(), vo);
			}
			NewTreeVO<Shop, ProgrameExtentVO> vo = new NewTreeVO<Shop, ProgrameExtentVO>();
			List<Shop> lstShop = new ArrayList<Shop>();
			for (Shop shop : lst) {
				if (shop.getId() != null) {
					if (shop.getId().compareTo(shopId) == 0) {
						vo.setObject(shop);
						vo.setDetail(map.get(shop.getId()));
					} else {
						lstShop.add(shop);
					}
				}
			}
			
			vo.setListChildren(this.getChildShopNewTreeVOEx(vo.getObject(), lstShop, map));
			
			return vo;
		}
		catch (DataAccessException ex) {
			throw new BusinessException();
		}
	}
	
	/*****************************	GLOBAL ****************************************/
	private List<NewTreeVO<Shop, ProgrameExtentVO>> getChildShopNewTreeVOEx(
			Shop shop, List<Shop> lst, SortedMap<Long, ProgrameExtentVO> map) {
		List<NewTreeVO<Shop, ProgrameExtentVO>> ret = new ArrayList<NewTreeVO<Shop, ProgrameExtentVO>>();
		for (Shop obj : lst) {
			if (obj.getParentShop() != null  && shop.getId().compareTo(obj.getParentShop().getId()) == 0) {
				NewTreeVO<Shop, ProgrameExtentVO> vo = new NewTreeVO<Shop, ProgrameExtentVO>();
				vo.setObject(obj);
				vo.setDetail(map.get(obj.getId()));
				ret.add(vo);
			}
		}
		for (NewTreeVO<Shop, ProgrameExtentVO> treeVO : ret) {
			lst.remove(treeVO.getObject());
		}
		for (NewTreeVO<Shop, ProgrameExtentVO> treeVO : ret) {
			treeVO.setListChildren(getChildShopNewTreeVOEx(treeVO.getObject(),
					lst, map));
		}
		return ret;
	}
	
	@Override
	public List<Shop> getListChildShopId(Integer status, Long... arrShopId)throws BusinessException{
		try{
			List<Shop> lstShop = shopDAO.getListChildShopId(status, arrShopId);
			return lstShop;
		}catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<Shop> getListParentShopId(Integer status, Long... arrShopId)throws BusinessException{
		try{
			List<Shop> lstShop = shopDAO.getListParentShopId(status, arrShopId);
			return lstShop;
		}catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
		@Override
	public Shop getShopArea(Long shopId) throws BusinessException {
		try {
			return shopDAO.getShopArea(shopId);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	

	
		@Override
	public Boolean checkAncestor(String parentCode, String shopCode) throws BusinessException {
		try {
			return shopDAO.checkAncestor(parentCode, shopCode);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
		
	@Override
	public List<Shop> getListShopExport(Long shopId) throws BusinessException {
			try {
				return shopDAO.getListShopExport(shopId);
			} catch (DataAccessException e) {
				
				throw new BusinessException(e);
			}
	}
	
	@Override
	public ObjectVO<Shop> getAllShopChild(KPaging<Shop> kPaging, String shopCode,
			String shopName, ActiveType status, String taxNum, 
			String invoiceNumberAccount, String phone, String mobilePhone,String email,
			Long shopTypeId, String shopParentCode) throws BusinessException {
		try {
			List<Shop> lstShop = shopDAO.getAllShopChild(shopDAO.getShopByCode(shopParentCode).getId());
			List<Long> lstParentId = new ArrayList<Long>();
			for (Shop s: lstShop) {
				lstParentId.add(s.getId());
			}
			ShopFilter filter = new ShopFilter();
			filter.setkPaging(kPaging);
			filter.setShopCode(shopCode);
			filter.setShopName(shopName);
			filter.setLstParentId(lstParentId);
			filter.setStatus(status);
			filter.setTaxNum(taxNum);
			filter.setInvoiceNumberAccount(invoiceNumberAccount);
			filter.setPhone(phone);
			filter.setMobilePhone(mobilePhone);
			filter.setEmail(email);
			filter.setShopTypeId(shopTypeId);
			
			List<Shop> rs = shopDAO.getListShop(filter);
			ObjectVO<Shop> vo = new ObjectVO<Shop>();
			vo.setkPaging(kPaging);
			vo.setLstObject(rs);
			return vo;
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<Shop> getListShopOfUnitTreeSaleMT(KPaging<Shop> kPaging, String shopCode,String shopName, ActiveType status) throws BusinessException {
		try {
			List<Shop> lst = shopDAO.getListShopOfUnitTreeSaleMT(kPaging, shopCode, shopName, status);
			ObjectVO<Shop> vo = new ObjectVO<Shop>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public int getCountChildByShop(ShopFilter filter) throws BusinessException {
		try {
			return shopDAO.getCountChildByShop(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Shop getShopByCodeSaleMT(String code) throws BusinessException {
		try {
			return shopDAO.getShopByCodeSaleMT(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Shop getShopByCodeNEW(String code) throws BusinessException {
		try {
			return shopDAO.getShopByCodeSaleMT(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Integer getShopByTienTv() throws BusinessException {
		return 0;
	}
	
	@Override
	public List<ShopVO> getListChildShopVOByFilter (BasicFilter<ShopVO> filter) throws BusinessException {
		try {
			return shopDAO.getListChildShopVOByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Shop> getListShopOrganization(ShopFilter filter) throws BusinessException {
		try {
			return shopDAO.getListShopOrganization(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ShopVO> findShopVOBy(KPaging<ShopVO> kPaging, UnitFilter unitFilter) throws BusinessException {
		try {
			List<ShopVO> shops = shopDAO.findShopVOBy(kPaging, unitFilter);
			ObjectVO<ShopVO> result = new ObjectVO<ShopVO>();
			result.setkPaging(kPaging);
			result.setLstObject(shops);
			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Shop> getShopsOnTree(List<Long> startFromShops, Boolean includeStoppedShop) throws BusinessException {
		try {
			return shopDAO.getShopsOnTree(startFromShops, includeStoppedShop);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public List<Shop> getShopsInAccessOnTree(ShopFilter shopFilter, Long keyshopId) throws BusinessException {
		try {
			List<Shop> lstData = shopDAO.getShopsInAccessOnTree(shopFilter, keyshopId);
			/**
			 * Lay thong tin Key Shop Target
			 * @author hunglm16
			 * @since 17/11/2015
			 * @description yeu cau neu bo sung them truong vao chuc nang nay thi phai code lai bang ShopVO 
			 **/
			if (lstData != null && !lstData.isEmpty()) {
				List<ShopVO> lstDataVOTpm = shopDAO.searchShopsInAccessOnTreeForKeyShop(shopFilter, keyshopId);
				if (lstDataVOTpm != null && !lstDataVOTpm.isEmpty()) {
					Map<Long, Integer> mapShopQuantity = new HashMap<Long, Integer>();
					for (ShopVO rowVO: lstDataVOTpm) {
						mapShopQuantity.put(rowVO.getId(), rowVO.getQuantityTarget());
					}
					for (int i = 0, size = lstData.size(); i < size; i++) {
						lstData.get(i).setQuantity(mapShopQuantity.get(lstData.get(i).getId()));
					}
				}
			}
			return lstData;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * checkShopHaveActiveSubShop: kiem tra shop co shop con hoat dong
	 * @author phuocdh2
	 * @description: Nhan vao shopId va kiem tra shop con co hoat dong
	 * @return boolean : true : co shop con con hoat dong false : cac shop con da tam ngung het
	 * @since 02/03/2015
	 */
	@Override
	public Boolean checkShopHaveActiveSubShop(ActiveType status, Long shopId) throws BusinessException{
		try {
			return shopDAO.checkShopHaveActiveSubShop(status,shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * checkShopHaveActiveStaff lay so luong nhan vien truc thuoc shop nhap vao va 
	 * @author phuocdh2
	 * @return true: co nhan vien dang hoat dong false : shop khong co nhan vien dang hoat dong
	 * @throws BusinessException
	 * @since 02/03/2015
	 */
	@Override
	public Boolean checkShopHaveActiveStaff(ActiveType status,Long shopId) throws BusinessException {
		try {
			return shopDAO.checkShopHaveActiveStaff(status,shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * checkShopHaveActiveCustomer lay so luong khach hang hoat dong truc thuoc shop nhap vao va 
	 * @author phuocdh2
	 * @return true : shop co khach hang hoat dong false : shop co khach hang het hoat dong
	 * @throws BusinessException
	 * @since 02/03/2015
	 */
	@Override
	public Boolean checkShopHaveActiveCustomer(ActiveType status,Long shopId) throws BusinessException {
		try {
			return shopDAO.checkShopHaveActiveCustomer(status,shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * tra ve danh sach don vi de export ra file excel
	 * @param kPaging thong tin phan trang
	 * @param unitFilter tieu chi tim kiem
	 * @author phuocdh2
	 * @return danh sach don vi de export
	 * @since 24 Mar 2015
	 */
	@Override
	public ObjectVO<ShopVO> findShopVOExport(KPaging<ShopVO> kPaging, UnitFilter unitFilter) throws BusinessException{
		try {
			List<ShopVO> shops = shopDAO.findShopVOExport(kPaging, unitFilter);
			ObjectVO<ShopVO> result = new ObjectVO<ShopVO>();
			result.setkPaging(kPaging);
			result.setLstObject(shops);
			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * lay danh sach shop_id theo level co cau hinh hien gia, an gia theo level tu 1-> 4... 
	 * shop con -> shop cha .. uu tien muc con, muc co level thap nhat
	 * @author phuocdh2
	 * @return List<ShopVO> danh sach shop_param
	 * @throws DataAccessException
	 * @since 08/04/2015
	 */
	@Override
	public List<ShopVO> getListShopVOConfigShowPrice(Long shopId, Integer status) throws BusinessException {
		try {
			return shopDAO.getListShopVOConfigShowPrice(shopId,status);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	
	public ObjectVO<Shop> getListShopSpectific(ShopFilter unitFilter) throws BusinessException{
		try {
			List<Shop> shops = shopDAO.getListShopSpectific( unitFilter);
			ObjectVO<Shop> result = new ObjectVO<Shop>();
			result.setLstObject(shops);
			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ShopVO> getListShopConfigByParamCode(Long shopId,
			ApParamType sysModifyPrice) throws BusinessException {
		try {
			return shopDAO.getListShopVOConfigShowPrice(shopId,sysModifyPrice);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ShopParam> getConfigShopParam(Long shopId, String paramCode)
			throws BusinessException {
		try {
			return shopDAO.getConfigShopParam(shopId, paramCode);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Shop> getListChildShop(ShopFilter filter)throws BusinessException{
		try{
			List<Shop> shops = shopDAO.getListChildShop( filter);
			ObjectVO<Shop> result = new ObjectVO<Shop>();
			result.setLstObject(shops);
			result.setkPaging(filter.getkPaging());
			return result;
		}catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public boolean isChild(Long parentID, Long childId) throws BusinessException{

		try {
			return shopDAO.isChild(parentID, childId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Shop getShopByParentIdNull() throws BusinessException{
		try {
			return shopDAO.getShopByParentIdNull();
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Shop> getShopsOnTreeFilter(ShopFilter shopFilter, Long keyShopId) throws BusinessException {
		try {
			List<Shop> lstData = shopDAO.getShopsOnTree(shopFilter);
			/**
			 * Lay thong tin Key Shop Target
			 * 
			 * @author hunglm16
			 * @since 17/11/2015
			 * @description yeu cau neu bo sung them truong vao chuc nang nay thi phai code lai bang ShopVO
			 **/
			if (lstData != null && !lstData.isEmpty()) {
				List<ShopVO> lstDataVOTpm = shopDAO.searchShopsOnTreeForKeyShop(shopFilter, keyShopId);
				if (lstDataVOTpm != null && !lstDataVOTpm.isEmpty()) {
					Map<Long, Integer> mapShopQuantity = new HashMap<Long, Integer>();
					for (ShopVO rowVO : lstDataVOTpm) {
						mapShopQuantity.put(rowVO.getId(), rowVO.getQuantityTarget());
					}
					for (int i = 0, size = lstData.size(); i < size; i++) {
						lstData.get(i).setQuantity(mapShopQuantity.get(lstData.get(i).getId()));
					}
				}
			}
			return lstData;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public List<Shop> retriveShopsForKeyShop(Long keyshopId) throws BusinessException {
		try {
			return shopDAO.retriveShopsForKeyShop(keyshopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public NewTreeVO<Shop, ProgrameExtentVO> getShopTreeVOForFocusProgramEx(
			Long shopId, List<Long> exceptionalShopId, String shopCode,
			String shopName, ActiveType status, Long focusProgramId, String strListShopId)
			throws BusinessException {
		try {
			if (shopId == null) {
				Shop shop = shopDAO.getShopByParentIdNull();

				if (shop == null)
					throw new IllegalArgumentException("shopId is null");

				shopId = shop.getId();
			}

			NewTreeVO<Shop, ProgrameExtentVO> vo = new NewTreeVO<Shop, ProgrameExtentVO>();
			List<Shop> lst = shopDAO.getSearchShopTreeVO(shopId,
					exceptionalShopId, shopCode, shopName, status,
					focusProgramId, ProgramObjectType.FOCUS_PROGRAM, null,
					null, null, strListShopId);

			List<ProgrameExtentVO> lstQuant = focusShopMapDAO.getListProgrameExtentVOEx(lst, focusProgramId);
			SortedMap<Long, ProgrameExtentVO> map = new TreeMap<Long, ProgrameExtentVO>();
			for (ProgrameExtentVO q : lstQuant) {
				map.put(q.getShopId(), q);
			}

			List<Shop> lstShop = new ArrayList<Shop>();
			for (Shop shop : lst) {
				if (shop.getId() != null) {
					if (shop.getId().compareTo(shopId) == 0) {
						vo.setObject(shop);
						vo.setDetail(map.get(shop.getId()));
					} else {
						lstShop.add(shop);
					}
				}
			}
			
			vo.setListChildren(this.getChildShopNewTreeVOEx(vo.getObject(), lstShop, map));
			
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<Shop> getListNextChildShop(ShopFilter filter) throws BusinessException {
		try {
			return shopDAO.getListNextChildShop(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author vuongmq
	 * @date 21/08/2015
	 * @descriptip Lay danh sach shop cua NVGS quan ly
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public List<Shop> getListShopOfSupervise(RptStaffSaleFilter filter) throws BusinessException {
		try {
			return shopDAO.getListShopOfSupervise(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Shop> getAncestorAndSiblingShops(Long shopId) throws BusinessException {
		try {
			return shopDAO.getAncestorAndSiblingShops(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Shop> getShopForConnectChildrenByKeyShop(Long keyShopId, Integer status) throws BusinessException {
		try {
			return shopDAO.getShopForConnectChildrenByKeyShop(keyShopId, status);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateKSShopTagertByKeyShop(long keyShopId, String username) throws DataAccessException {
		List<SpParam> inParams = new ArrayList<SpParam>();
		List<SpParam> outParams = new ArrayList<SpParam>();
		int idx = 1;
		inParams.add(new SpParam(idx++, java.sql.Types.NUMERIC, keyShopId));
		inParams.add(new SpParam(idx++, java.sql.Types.VARCHAR, username));
		repo.executeSP("p_update_ks_shop_taget", inParams, outParams);
	}
	
	@Override
	public List<ShopVO> getShopForConnectChildrenByKeyShopVO(Long keyShopId, Long cycleId, Integer status, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		try {
			return shopDAO.getShopForConnectChildrenByKeyShopVO(keyShopId, cycleId, status, staffRootId, roleId, shopRootId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Long> getListLongShopParentByShopId(Long shopId) throws BusinessException {
		try {
			return shopDAO.getListLongShopParentByShopId(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		
	}
	
	@Override
	public List<Long> getListLongShopChildByShopId(Long shopId) throws BusinessException {
		try {
			return shopDAO.getListLongShopChildByShopId(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

}
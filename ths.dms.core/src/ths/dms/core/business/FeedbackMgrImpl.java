/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Feedback;
import ths.dms.core.entities.FeedbackStaff;
import ths.dms.core.entities.FeedbackStaffCustomer;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.FeedbackStatus;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.FeedbackFilter;
import ths.dms.core.entities.vo.FeedbackVO;
import ths.dms.core.entities.vo.FeedbackVOSave;
import ths.dms.core.entities.vo.FeedbackVOTmp;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.Constant;
import ths.dms.core.dao.ApParamDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.FeedbackDAO;
import ths.dms.core.dao.MediaItemDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.StaffDAO;

/**
 * FeedbackMgrImpl
 * @author vuongmq
 * @since 11/11/2015 
 */
public class FeedbackMgrImpl implements FeedbackMgr {

	@Autowired
	StaffDAO staffDAO;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	ApParamDAO apParamDAO;
	
	@Autowired
	FeedbackDAO feedbackDAO;

	@Autowired
	MediaItemDAO mediaItemDAO;
	
	@Override
	public List<Feedback> getListFeedbackByFilter(FeedbackFilter filter) throws BusinessException {
		try {
			return feedbackDAO.getListFeedbackByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<FeedbackStaff> getListFeedbackStaffByFilter(FeedbackFilter filter) throws BusinessException {
		try {
			return feedbackDAO.getListFeedbackStaffByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public FeedbackVO getFeedBackVOByFilter(FeedbackFilter filter) throws BusinessException {
		try {
			return feedbackDAO.getFeedBackVOByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<FeedbackVO> getListFeedbackVOByFilter(FeedbackFilter filter) throws BusinessException {
		try {
			List<FeedbackVO> lst = feedbackDAO.getListFeedbackVOByFilter(filter);
			ObjectVO<FeedbackVO> vo = new ObjectVO<>();
			vo.setkPaging(filter.getkPagingVO());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<MediaItem> getListMediaItemFeedbackFilter(FeedbackFilter filter) throws BusinessException {
		try {
			return mediaItemDAO.getListMediaItemFeedbackFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateFeedback(List<FeedbackStaff> lstFeedbackStaff, Integer type, LogInfoVO logInfoVO) throws BusinessException {
		try {
			Date sys = commonDAO.getSysDate();
			for (FeedbackStaff feedbackStaff : lstFeedbackStaff) {
				// Can thuc hien; type: hoan thanh: 1
				if (FeedbackStatus.COMPLETE.getValue().equals(type)) {
					if (FeedbackStatus.NEW.equals(feedbackStaff.getResult())) {
						feedbackStaff.setResult(FeedbackStatus.parseValue(type));
						feedbackStaff.setDoneDate(sys);
						feedbackStaff.setUpdateDate(sys);
						feedbackStaff.setUpdateUser(logInfoVO.getStaffCode());
						commonDAO.updateEntity(feedbackStaff);
					}
				} else {
					//Danh sach van de; type: tu choi: 0, or duyet: 2
					if (FeedbackStatus.COMPLETE.equals(feedbackStaff.getResult())) {
						feedbackStaff.setResult(FeedbackStatus.parseValue(type));
						if (FeedbackStatus.NEW.equals(feedbackStaff.getResult())) {
							feedbackStaff.setNumReturn(feedbackStaff.getNumReturn() != null ? (feedbackStaff.getNumReturn() + 1) : Constant.ZERO_INT);
							feedbackStaff.setDoneDate(null);
						}
						feedbackStaff.setUpdateDate(sys);
						feedbackStaff.setUpdateUser(logInfoVO.getStaffCode());
						commonDAO.updateEntity(feedbackStaff);
					}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.FeedbackMgr#saveFeedback(ths.dms.core.entities.vo.FeedbackVOSave, ths.dms.core.entities.vo.LogInfoVO)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveFeedback(FeedbackVOSave feedbackVOSave, LogInfoVO logInfoVO) throws BusinessException {
		try {
			if (feedbackVOSave != null) {
				Date sys = commonDAO.getSysDate();
				Feedback feedback = new Feedback();
				feedback.setContent(feedbackVOSave.getContent());
				feedback.setRemindDate(feedbackVOSave.getRemindDate());
				feedback.setStatus(StatusType.ACTIVE);
				ApParam apParam = apParamDAO.getApParamByCodeX(feedbackVOSave.getTypeParam(), ApParamType.FEEDBACK_TYPE, ActiveType.RUNNING);
				feedback.setType(apParam.getApParamCode());
				Staff crUser = staffDAO.getStaffById(logInfoVO.getStaffRootId());
				feedback.setStaff(crUser);
				feedback.setCreateDate(sys);
				feedback = commonDAO.createEntity(feedback);
				if (feedback != null && feedback.getId() != null ) {
					if (feedbackVOSave.getLstStaffId() != null && feedbackVOSave.getLstStaffId().size() > 0) {
						for (int i = 0, sz = feedbackVOSave.getLstStaffId().size(); i < sz; i++) {
							FeedbackVOTmp feedbackVOTmp = feedbackVOSave.getLstStaffId().get(i);
							if (feedbackVOTmp != null) {
								Long staffId = feedbackVOTmp.getId();
								Staff staff = staffDAO.getStaffById(staffId);
								if (staff != null) {
									FeedbackStaff feedbackStaff = new FeedbackStaff();
									feedbackStaff.setFeedback(feedback);
									feedbackStaff.setResult(FeedbackStatus.NEW);
									feedbackStaff.setNumReturn(Constant.ZERO_INT);
									feedbackStaff.setStaff(staff);
									feedbackStaff.setCreateStaff(crUser);
									feedbackStaff.setCreateDate(sys);
									feedbackStaff = commonDAO.createEntity(feedbackStaff);
									if (feedbackStaff != null && feedbackStaff.getId() != null && feedbackVOTmp.getLstCustomerId() != null && feedbackVOTmp.getLstCustomerId().size() > 0) {
										for (int j = 0, szCus = feedbackVOTmp.getLstCustomerId().size(); j < szCus; j++) {
											Long customerId = feedbackVOTmp.getLstCustomerId().get(j);
											Customer customer = commonDAO.getEntityById(Customer.class, customerId);
											if (customer != null) {
												FeedbackStaffCustomer feedbackStaffCustomer = new FeedbackStaffCustomer();
												feedbackStaffCustomer.setFeedbackStaff(feedbackStaff);
												feedbackStaffCustomer.setCustomer(customer);
												feedbackStaffCustomer.setStaff(staff);
												feedbackStaffCustomer.setCreateStaff(crUser);
												feedbackStaffCustomer.setCreateDate(sys);
												commonDAO.createEntity(feedbackStaffCustomer);
											}
										}
									}
								}
							}
						}
					}
					// luu media Item
					if (feedbackVOSave.getLstMediaItem() != null && feedbackVOSave.getLstMediaItem().size() > 0) {
						for (int i = 0, sz = feedbackVOSave.getLstMediaItem().size(); i < sz; i++) {
							MediaItem mediaItem = feedbackVOSave.getLstMediaItem().get(i);
							mediaItem.setObjectId(feedback.getId());
							mediaItem.setStaff(feedback.getStaff());
							mediaItem.setCreateDate(sys);
							mediaItem.setCreateUser(logInfoVO.getStaffCode());
							commonDAO.createEntity(mediaItem);
						}
					}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.FeedbackMgr#getListFeedbackIdCheckByFilter(ths.dms.core.entities.filter.FeedbackFilter)
	 */
	@Override
	public List<FeedbackVO> getListFeedbackIdCheckByFilter(FeedbackFilter filter) throws BusinessException {
		try {
			return feedbackDAO.getListFeedbackIdCheckByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

package ths.dms.core.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Bank;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.BankFilter;
import ths.dms.core.entities.vo.BankVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.BankDAO;

public class BankMgrImpl implements BankMgr {

	@Autowired
	BankDAO bankDAO;
	
	@Override
	public Bank createBank(Bank bank, LogInfoVO logInfo)
			throws BusinessException {
		try {
			return bankDAO.createBank(bank, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateBank(Bank bank, LogInfoVO logInfo)
			throws BusinessException {
		try {
			
			bankDAO.updateBank(bank, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
		
	}

	@Override
	public void deleteBank(Bank bank, LogInfoVO logInfo)
			throws BusinessException {
		try {
			bankDAO.deleteBank(bank, logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
		
	}

	@Override
	public Bank getBankById(Long id) throws BusinessException {
		try {
			return id==null?null: bankDAO.getBankById(id);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public Bank getBankByCode(String codeBank) throws BusinessException {
		try {
			return bankDAO.getBankByCode(codeBank);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Bank> getListBankByShop(Long shopId) throws BusinessException {
		try {
			return bankDAO.getListBankByShop(shopId, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Bank> getListBankByShopAscBankName(Long shopId) throws BusinessException {
		try {
			return bankDAO.getListBankByShopAscBankName(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<BankVO> getListBank(KPaging<BankVO> kPaging, BankFilter filter)
			throws BusinessException {
		try {
			List<BankVO> list = bankDAO.getListBank(kPaging, filter);
			ObjectVO<BankVO> vo = new ObjectVO<BankVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(list);
			return vo;
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	
}

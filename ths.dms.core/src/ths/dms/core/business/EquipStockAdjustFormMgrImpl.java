package ths.dms.core.business;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.EquipFormHistory;
import ths.dms.core.entities.EquipHistory;
import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.EquipStockAdjustForm;
import ths.dms.core.entities.EquipStockAdjustFormDtl;
import ths.dms.core.entities.EquipStockTotal;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.EquipPeriodType;
import ths.dms.core.entities.enumtype.EquipStockEquipFilter;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.EquipStockAdjustFormFilter;
import ths.dms.core.entities.vo.EquipStockAdjustFormDtlVO;
import ths.dms.core.entities.vo.EquipStockAdjustFormRecord;
import ths.dms.core.entities.vo.EquipStockAdjustFormVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.EquipStockAdjustFormDAO;
import ths.dms.core.dao.EquipmentManagerDAO;
import ths.dms.core.dao.EquipmentPeriodDAO;
import ths.dms.core.dao.StaffDAO;

public class EquipStockAdjustFormMgrImpl implements  EquipStockAdjustFormMgr {
	
	@Autowired
	EquipStockAdjustFormDAO equipStockAdjustFormDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	EquipmentPeriodDAO equipmentPeriodDAO;

	@Autowired
	PlatformTransactionManager transactionManager;
	
	EquipmentManagerMgrImpl equipmentManagerMgrImpl;
	
	@Autowired
	EquipmentManagerMgr equipmentManagerMgr;
	
	@Autowired
	EquipmentManagerDAO equipMngDAO;
	
	@Autowired
	StaffDAO staffDAO;
	
	@Autowired
	EquipmentManagerDAO equipmentManagerDAO;
	
	@Override
	public ObjectVO<EquipStockAdjustFormVO> getListEquipStockAdjustFormByFilter(EquipStockAdjustFormFilter filter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<EquipStockAdjustFormVO> lstObject = equipStockAdjustFormDAO.getListEquipStockAdjustForm(filter);
			ObjectVO<EquipStockAdjustFormVO> vo = new ObjectVO<EquipStockAdjustFormVO>();
			vo.setLstObject(lstObject);
			vo.setkPaging(filter.getkPaging());
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipStockAdjustForm getEquipStockAdjustFormById(Long id) throws BusinessException {
		try {
			return equipStockAdjustFormDAO.getEquipStockAdjustFormByID(id);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<EquipStockAdjustFormDtlVO> getListEquipStockAdjustFormDtlFilter(EquipStockAdjustFormFilter filter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<EquipStockAdjustFormDtlVO> lstObject = equipStockAdjustFormDAO.getEquipStockAdjustFormDtlFilter(filter);
			ObjectVO<EquipStockAdjustFormDtlVO> vo = new ObjectVO<EquipStockAdjustFormDtlVO>();
			vo.setLstObject(lstObject);
			vo.setkPaging(filter.getkPagingDtl());
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	//@Transactional(rollbackFor = Exception.class)
	public void updateStatus(EquipStockAdjustFormFilter filter, LogInfoVO logInfoVO) throws BusinessException {
		int maxLengthCode = 8;
		try {
			Date sysdate = commonDAO.getSysDate();
			if (filter != null && filter.getLstId() != null && filter.getLstId().size() > 0) {
				List<EquipStockAdjustForm> lstEquipStockAdjustForm = equipStockAdjustFormDAO.getListEquipStockAdjustFormByListId(filter.getLstId());
				if (lstEquipStockAdjustForm != null && lstEquipStockAdjustForm.size() > 0) {
					for(EquipStockAdjustForm equipStockAdjustForm: lstEquipStockAdjustForm){
						
						//Cap nhat lich su equipFormHistory khi trang thai thay doi
						if(!filter.getStatus().equals(equipStockAdjustForm.getStatus().getValue())){
							
							EquipFormHistory equipFormHistory = new EquipFormHistory();
							equipFormHistory.setActDate(sysdate);
							equipFormHistory.setCreateDate(sysdate);
							equipFormHistory.setCreateUser(logInfoVO.getStaffCode());
							//equipFormHistory.setDeliveryStatus(equipStockAdjustForm.getStatus());
							equipFormHistory.setRecordId(equipStockAdjustForm.getId());
							equipFormHistory.setRecordStatus(filter.getStatus());
							equipFormHistory.setRecordType(EquipTradeType.EQUIP_STOCK_ADJUST.getValue());
							Staff staff = staffDAO.getStaffByCode(equipStockAdjustForm.getCreateUser());
							equipFormHistory.setStaff(staff);
							equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
						}
						
						equipStockAdjustForm.setStatus(StatusRecordsEquip.parseValue(filter.getStatus()));
						equipStockAdjustForm.setUpdateDate(sysdate);
						equipStockAdjustForm.setUpdateUser(logInfoVO.getStaffCode());
						equipStockAdjustForm.setStatus(StatusRecordsEquip.parseValue(filter.getStatus()));
						if(filter.getStatus().equals(StatusRecordsEquip.APPROVED.getValue())){
							equipStockAdjustForm.setApproveDate(sysdate);
						}
						if(filter.getStatus().equals(StatusRecordsEquip.CANCELLATION.getValue())){
							equipStockAdjustForm.setRefuseDate(sysdate);
						}
						filter.setId(equipStockAdjustForm.getId());
						List<EquipStockAdjustFormDtl> lstEquipStockAdjustFormDtl = equipStockAdjustFormDAO.getListEquipStockAdjustFormDetailById(equipStockAdjustForm.getId());
						for(EquipStockAdjustFormDtl equipStockAdjustFormDtl: lstEquipStockAdjustFormDtl){
							equipStockAdjustFormDtl.setUpdateDate(sysdate);
							equipStockAdjustFormDtl.setUpdateUser(logInfoVO.getStaffCode());
							Equipment equipment = new Equipment();
							if(filter.getStatus().equals(StatusRecordsEquip.APPROVED.getValue())){
								Map<Long, String> mapMaxCode = new HashMap<Long, String>();
								/**Thuc hien them moi vao Equipment*/
								String maxEquipmentCode = null;
								
//								if (equipStockAdjustFormDtl.getEquipGroup() != null) {
//									//equipmentManagerMgrImpl = new EquipmentManagerMgrImpl();
//									if (mapMaxCode.containsKey(equipStockAdjustFormDtl.getEquipGroup().getId()) == true) {
//										maxEquipmentCode = equipmentManagerMgrImpl.getEquipmentCodeByMaxCodeAndPrefix(mapMaxCode.get(equipStockAdjustFormDtl.getEquipGroup().getId()), equipStockAdjustFormDtl.getEquipGroup().getCode());
//										mapMaxCode.put(equipStockAdjustFormDtl.getEquipGroup().getId(), maxEquipmentCode);
//									} else {
//										maxEquipmentCode = equipMngDAO.getMaxEquipmentCodeByEquipGroupId(equipStockAdjustFormDtl.getEquipGroup().getId());
//										maxEquipmentCode = equipmentManagerMgrImpl.getEquipmentCodeByMaxCodeAndPrefix(maxEquipmentCode, equipStockAdjustFormDtl.getEquipGroup().getCode());
//										mapMaxCode.put(equipStockAdjustFormDtl.getEquipGroup().getId(), maxEquipmentCode);
//									}
//								}
								//tamvnm: update lai phat sinh code thiet bi tu dong
								if (equipStockAdjustFormDtl.getEquipGroup() != null && equipStockAdjustFormDtl.getEquipGroup().getEquipCategory() != null) {
									EquipCategory ec = equipStockAdjustFormDtl.getEquipGroup().getEquipCategory();
									
									if (mapMaxCode.containsKey(ec.getId()) == true) {
										maxEquipmentCode = equipmentManagerMgr.getEquipmentCodeByMaxCodeAndPrefixMaxLength(mapMaxCode.get(ec.getId()), ec.getCode(), maxLengthCode);
										mapMaxCode.put(ec.getId(), maxEquipmentCode);
									} else {
										maxEquipmentCode = equipMngDAO.getMaxEquipmentCodeByEquipCategoryId(ec.getId());
										maxEquipmentCode = equipmentManagerMgr.getEquipmentCodeByMaxCodeAndPrefixMaxLength(maxEquipmentCode, ec.getCode(), maxLengthCode);
										mapMaxCode.put(ec.getId(), maxEquipmentCode);
									}
								}
								
								equipment.setCode(maxEquipmentCode);
								equipment.setCreateDate(sysdate);
								equipment.setCreateUser(logInfoVO.getStaffCode());
								equipment.setEquipGroup(equipStockAdjustFormDtl.getEquipGroup());
								equipment.setEquipProvider(equipStockAdjustFormDtl.getEquipProvider());
								equipment.setHealthStatus(equipStockAdjustFormDtl.getHealthStatus());
								equipment.setManufacturingYear(equipStockAdjustFormDtl.getManufacturingYear());
								equipment.setPrice(equipStockAdjustFormDtl.getPrice());
								equipment.setSerial(equipStockAdjustFormDtl.getSerial());
								equipment.setStatus(StatusType.ACTIVE);
								equipment.setStockId(equipStockAdjustFormDtl.getStockId());
								equipment.setStockName(equipStockAdjustFormDtl.getStockName());
								equipment.setStockType(EquipStockTotalType.KHO);
								equipment.setWarrantyExpiredDate(equipStockAdjustFormDtl.getWarrantyExpiredDate());
								
								EquipStock stockById =  equipmentManagerDAO.getEquipStockById(equipStockAdjustFormDtl.getStockId());
								equipment.setStockCode(stockById.getCode());
								equipment.setTradeStatus(0);
								equipment.setTradeType(null);
								equipment = equipMngDAO.createEquipment(equipment);
								
								/**Luu lich su thiet bi*/
								EquipHistory equipHistory = new EquipHistory();
								equipHistory.setEquip(equipment);
								equipHistory.setStatus(StatusType.ACTIVE.getValue());
								equipHistory.setHealthStatus(equipment.getHealthStatus());
								equipHistory.setUsageStatus(equipment.getUsageStatus());
								equipHistory.setTradeType(null);
								equipHistory.setTradeStatus(StatusType.INACTIVE.getValue());
								equipHistory.setObjectCode(equipStockAdjustForm.getCode());
								equipHistory.setObjectId(equipment.getStockId());//kho chon
								equipHistory.setStockName(equipment.getStockName());
								equipHistory.setObjectCode(equipment.getStockCode());
								equipHistory.setObjectType(EquipStockTotalType.KHO);
								equipHistory.setCreateUser(equipment.getCreateUser());
								equipHistory.setCreateDate(sysdate);
								equipHistory.setFormId(equipStockAdjustForm.getId());
								equipHistory.setFormType(EquipTradeType.EQUIP_STOCK_ADJUST);
								equipHistory.setTableName("EQUIP_STOCK_ADJUST_FORM");
								equipHistory = equipMngDAO.createEquipHistory(equipHistory);
								
								//Tang ton kho tong
								EquipStockEquipFilter<EquipStockTotal> filterSt = new EquipStockEquipFilter<EquipStockTotal>();
								filterSt.setEquipGroupId(equipment.getEquipGroup().getId());
								filterSt.setStockType(EquipStockTotalType.KHO_TONG.getValue()); 

								EquipStockTotal equipStockTotal = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);
								
								if(equipStockTotal==null){
									equipStockTotal = new EquipStockTotal();
									//thuc hien them moi equip_stock_total 1: kho tong
									equipStockTotal.setStockType(EquipStockTotalType.KHO_TONG);
									equipStockTotal.setQuantity(1);
									equipStockTotal.setEquipGroup(equipStockAdjustFormDtl.getEquipGroup());
									equipStockTotal.setCreateDate(sysdate);
									equipStockTotal.setCreateUser(logInfoVO.getStaffCode());
									commonDAO.createEntity(equipStockTotal);
									//2:kho thiet bi
									equipStockTotal = new EquipStockTotal();
									equipStockTotal.setStockType(EquipStockTotalType.KHO);
									equipStockTotal.setQuantity(1);
									equipStockTotal.setEquipGroup(equipStockAdjustFormDtl.getEquipGroup());
									equipStockTotal.setCreateDate(sysdate);
									equipStockTotal.setCreateUser(logInfoVO.getStaffCode());
									equipStockTotal.setStockId(equipStockAdjustFormDtl.getStockId());
									this.createEquipStockTotalCommit(equipStockTotal);
									//commonDAO.createEntity(equipStockTotal);
								} else {
									if (equipStockTotal.getQuantity() != null) {
										equipStockTotal.setQuantity(equipStockTotal.getQuantity()+1);
									}
									equipStockTotal.setUpdateUser(equipment.getCreateUser());
									equipStockTotal.setUpdateDate(sysdate);
									equipMngDAO.updateEquipStockTotal(equipStockTotal);
									
									/**Cap nhat kho thiet bi*/
									filterSt = new EquipStockEquipFilter<EquipStockTotal>();
									filterSt.setEquipGroupId(equipment.getEquipGroup().getId());
									filterSt.setStockId(equipStockAdjustFormDtl.getStockId());
									filterSt.setStockType(EquipStockTotalType.KHO.getValue());
									EquipStockTotal stock = equipMngDAO.getFirtEquipStockTotalByFilter(filterSt);									
									if(stock==null){
										stock = new EquipStockTotal();
										stock.setStockType(EquipStockTotalType.KHO);
										stock.setQuantity(1);
										stock.setEquipGroup(equipStockAdjustFormDtl.getEquipGroup());
										stock.setCreateDate(sysdate);
										stock.setCreateUser(logInfoVO.getStaffCode());
										stock.setStockId(equipStockAdjustFormDtl.getStockId());
										equipMngDAO.createEquipStockTotal(stock);
									} else {
										if (stock.getQuantity() != null) {
											stock.setQuantity(stock.getQuantity() + 1);
										}
										stock.setUpdateUser(equipment.getCreateUser());
										stock.setUpdateDate(sysdate);
										equipMngDAO.updateEquipStockTotal(stock);
									}
								}
							}
							equipStockAdjustFormDtl.setEquipCode(equipment.getCode());
							equipStockAdjustFormDtl.setEquipId(equipment.getId());
							commonDAO.updateEntity(equipStockAdjustFormDtl);
						}
						commonDAO.updateEntity(equipStockAdjustForm);
					}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * Create entity
	 * 
	 * @author datpv4
	 * @param equipStockTotal
	 * @throws BusinessException
	 * @since 29/07/2015
	 */
	//@Transactional(rollbackFor = Exception.class)
	public void createEquipStockTotalCommit(EquipStockTotal equipStockTotal) throws BusinessException {
		try {
			commonDAO.createEntity(equipStockTotal);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public String createOrUpdateEquipStockAdjustForm(final EquipStockAdjustFormRecord equipStockAdjustFormRecord, final LogInfoVO logInfo) throws BusinessException {
		TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
		final StringBuilder errorMessage = new StringBuilder();
		final StringBuilder codeAndId = new StringBuilder();
		Boolean isUpdateSuccess = transactionTemplate.execute(new TransactionCallback<Boolean>() {
			@Override
			public Boolean doInTransaction(TransactionStatus transactionStatus) {
				Boolean isUpdateSuccess = true;
				try {
					Date sysdate = commonDAO.getSysDate();
					EquipPeriod currentEquipmentPeriod = equipmentPeriodDAO.getEquipPeriodCurrent();
					if (currentEquipmentPeriod == null || currentEquipmentPeriod.getStatus() != EquipPeriodType.OPENED) {
						errorMessage.append("equipment.repair.payment.record.equipment.period.not.open");
						isUpdateSuccess = false;
						return isUpdateSuccess;
					}
					Long id = equipStockAdjustFormRecord.getId();
					if (id == null) {
						
						//Them lich su equipFormHistory
						
						
						//** tao moi phieu nhap kho dieu chinh*//*
						EquipStockAdjustForm equipStockAdjustForm = new EquipStockAdjustForm();
						equipStockAdjustForm.setStatus(equipStockAdjustFormRecord.getStatus());
						equipStockAdjustForm.setCreateUser(logInfo.getStaffCode());
						equipStockAdjustForm.setCreateDate(sysdate);
						equipStockAdjustForm.setCreateFormDate(sysdate);
						equipStockAdjustForm.setEquipPeriodId(currentEquipmentPeriod.getId());
						equipStockAdjustForm.setDescription(equipStockAdjustFormRecord.getDescription());
						equipStockAdjustForm.setNote(equipStockAdjustFormRecord.getNote());
						if(equipStockAdjustFormRecord.getStatus().equals(StatusRecordsEquip.CANCELLATION)){
							equipStockAdjustForm.setRefuseDate(sysdate);
						}
						equipStockAdjustForm = commonDAO.createEntity(equipStockAdjustForm);
						List<EquipStockAdjustFormDtl> lstDtl = equipStockAdjustFormRecord.getEquipStockAdjustFormDtl();
						if (lstDtl != null && lstDtl.size() > 0) {
							for (EquipStockAdjustFormDtl equipStockAdjustFormDtl : lstDtl) {
								equipStockAdjustFormDtl.setCreateDate(sysdate);
								equipStockAdjustFormDtl.setCreateUser(logInfo.getStaffCode());
								equipStockAdjustFormDtl.setEquipStockAdjustForm(equipStockAdjustForm);
								commonDAO.createEntity(equipStockAdjustFormDtl);
							}
						}
						//Luu lich su
						/*EquipFormHistory equipFormHis = equipMngDAO.getEquipFormHistory(equipStockAdjustForm.getId());
						// neu trang thai cua equipForm != equip moi nhat trong DB thi luu lai, ko thi bo qua
						if(!equipFormHis.getRecordStatus().equals(equipStockAdjustFormRecord.getStatus().getValue())) {*/
							EquipFormHistory equipFormHistory = new EquipFormHistory();
							equipFormHistory.setActDate(sysdate);
							equipFormHistory.setCreateDate(sysdate);
							equipFormHistory.setCreateUser(logInfo.getStaffCode());
							equipFormHistory.setRecordStatus(equipStockAdjustFormRecord.getStatus().getValue());
							equipFormHistory.setRecordType(EquipTradeType.EQUIP_STOCK_ADJUST.getValue());
							Staff staff = staffDAO.getStaffByCode(logInfo.getStaffCode());
							equipFormHistory.setStaff(staff);
							equipFormHistory.setRecordId(equipStockAdjustForm.getId());
							equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);//Them lich su equipFormHistory
//						}

						//Tao ma code
						String code = "";
						code = "000000" + equipStockAdjustForm.getId().toString();
						code = code.substring(code.length() - 6);
						code = "DCT" + code;
						equipStockAdjustForm.setCode(code);
						commonDAO.updateEntity(equipStockAdjustForm);
						codeAndId.append(code).append(";").append(equipStockAdjustForm.getId()); // truong hop tao thanh cong tra ma code de view cho nguoi dung
					} else {
						
					
						
						//** cap nhat lai phieu  *//*
						EquipStockAdjustForm equipStockAdjustForm = equipStockAdjustFormDAO.getEquipStockAdjustFormByID(id);
						if (equipStockAdjustForm != null) {
							
							List<EquipStockAdjustFormDtl> lstDetail =equipStockAdjustFormDAO.getListEquipStockAdjustFormDetailById(equipStockAdjustForm.getId());
							
							// xoa danh tat ca danh sach lstDetail
							for (EquipStockAdjustFormDtl item : lstDetail) {
								commonDAO.deleteEntity(item);
							}
							// tao moi lai danh sach chi tiet nhap kho dieu chinh
							List<EquipStockAdjustFormDtl> lstDtl = equipStockAdjustFormRecord.getEquipStockAdjustFormDtl();
							if (lstDtl != null && lstDtl.size() > 0) {
								for (EquipStockAdjustFormDtl equipStockAdjustFormDtl : lstDtl) {
									equipStockAdjustFormDtl.setEquipStockAdjustForm(equipStockAdjustForm);
									equipStockAdjustFormDtl.setUpdateDate(sysdate);
									equipStockAdjustFormDtl.setUpdateUser(logInfo.getStaffCode());
									commonDAO.updateEntity(equipStockAdjustFormDtl);
								}
							}
							//cap nhat lai phieu nhap kho dieu chinh
							equipStockAdjustForm.setStatus(equipStockAdjustFormRecord.getStatus());
							equipStockAdjustForm.setNote(equipStockAdjustFormRecord.getNote());
							if(equipStockAdjustForm.getStatus().equals(StatusRecordsEquip.CANCELLATION)){
								equipStockAdjustForm.setRefuseDate(sysdate);
							}
							equipStockAdjustForm.setDescription(equipStockAdjustFormRecord.getDescription());
							equipStockAdjustForm.setUpdateDate(sysdate);
							equipStockAdjustForm.setUpdateUser(logInfo.getStaffCode());
							commonDAO.updateEntity(equipStockAdjustForm);
							EquipFormHistory equipFormHis = equipMngDAO.getEquipFormHistory(equipStockAdjustForm.getId());
							if(!equipFormHis.getRecordStatus().equals(equipStockAdjustFormRecord.getStatus().getValue())) {
								//Cap nhat lich su equipFormHistory
								EquipFormHistory equipFormHistory = new EquipFormHistory();
								equipFormHistory.setActDate(sysdate);
								equipFormHistory.setCreateDate(sysdate);
								equipFormHistory.setCreateUser(logInfo.getStaffCode());
								equipFormHistory.setRecordId(id);
								equipFormHistory.setRecordStatus(equipStockAdjustFormRecord.getStatus().getValue());
								equipFormHistory.setRecordType(EquipTradeType.EQUIP_STOCK_ADJUST.getValue());
								Staff staff = staffDAO.getStaffByCode(equipStockAdjustForm.getCreateUser());
								equipFormHistory.setStaff(staff);
								equipFormHistory = equipMngDAO.createEquipmentHistoryRecord(equipFormHistory);
							}
								
						} else {
							errorMessage.append("equipment.stock.adjust.record.code.not.exists");
							isUpdateSuccess = false;
							return isUpdateSuccess;
						}
					}

				} catch (DataAccessException e) {
					isUpdateSuccess = false;
					errorMessage.append("system.error");
					transactionStatus.setRollbackOnly();
				}
				if (!isUpdateSuccess) {
					transactionStatus.setRollbackOnly();
				}
				return isUpdateSuccess;
			}
		});
		if (isUpdateSuccess) {
			if (!StringUtility.isNullOrEmpty(codeAndId.toString())) {
				// truong hop tao thanh cong tra ma code ve de view cho nguoi dung
				return codeAndId.toString();
			}
			return null;
		}
		return errorMessage.toString();
	}

	@Override
	public List<EquipStockAdjustForm> getListEquipStockAdjustFormByListId(List<Long> lstId) throws BusinessException {
		try {
			return equipStockAdjustFormDAO.getListEquipStockAdjustFormByListId(lstId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public EquipStockAdjustFormDtl getEquipStockAdjustFormDetailByID(Long id) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return equipStockAdjustFormDAO.getEquipStockAdjustFormDetailByID(id);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
}
	

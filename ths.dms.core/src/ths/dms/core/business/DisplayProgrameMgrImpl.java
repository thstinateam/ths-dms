package ths.dms.core.business;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.CustomerDisplayPrograme;
import ths.dms.core.entities.DisplayShopMap;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StDisplayPdGroup;
import ths.dms.core.entities.StDisplayPdGroupDtl;
import ths.dms.core.entities.StDisplayPlAmtDtl;
import ths.dms.core.entities.StDisplayPlDpDtl;
import ths.dms.core.entities.StDisplayProgram;
import ths.dms.core.entities.StDisplayProgramExclusion;
import ths.dms.core.entities.StDisplayProgramLevel;
import ths.dms.core.entities.StDisplayProgramVNM;
import ths.dms.core.entities.StDisplayStaffMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.DisplayProgramExclusionFilter;
import ths.dms.core.entities.enumtype.DisplayProgramFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StDisplayPdGroupFilter;
import ths.dms.core.entities.enumtype.StDisplayProgramFilter;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.enumtype.StaffRoleType;
import ths.dms.core.entities.vo.CustomerDisplayProgrameVO;
import ths.dms.core.entities.vo.DisplayDetailVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.entities.vo.StDisplayPDGroupDTLVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.CustomerDisplayProgrameDAO;
import ths.dms.core.dao.DisplayProductGroupDAO;
import ths.dms.core.dao.DisplayProductGroupDTLDAO;
import ths.dms.core.dao.DisplayProgramExclusionDAO;
import ths.dms.core.dao.DisplayProgramLevelDTLDAO;
import ths.dms.core.dao.DisplayProgrameDAO;
import ths.dms.core.dao.DisplayProgrameDetailDAO;
import ths.dms.core.dao.DisplayProgrameLevelDAO;
import ths.dms.core.dao.DisplayShopMapDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.StDisplayPdGroupDtlDAO;
import ths.dms.core.dao.StDisplayStaffMapDAO;

public class DisplayProgrameMgrImpl implements DisplayProgrameMgr {

	@Autowired
	DisplayProgrameDAO displayProgrameDAO;
	@Autowired
	DisplayProgrameLevelDAO displayProgrameLevelDAO;
	@Autowired
	DisplayProgrameDetailDAO displayProgrameDetailDAO;
	@Autowired
	CustomerDisplayProgrameDAO customerDisplayProgrameDAO;
	@Autowired
	DisplayProductGroupDTLDAO displayProductGroupDTLDAO;
	@Autowired
	StDisplayStaffMapDAO stDisplayStaffMapDAO;	
	@Autowired
	ProductDAO productDAO;
	@Autowired
	DisplayShopMapDAO displayShopMapDAO;
	@Autowired
	DisplayProgrameDAO displayProgramDAO;
	@Autowired
	CommonDAO commonDAO;
	@Autowired
	ShopDAO shopDAO;

	@Autowired
	StDisplayPdGroupDtlDAO stDisplayPdGroupDtlDAO;
	
	@Override
	public ObjectVO<StDisplayStaffMap> getListDisplayStaffMap(KPaging<StDisplayStaffMap> kPaging, Long displayProgramId,
			List<Long> lstShopId,String month, boolean checkMap, long parentStaffId)
			throws BusinessException {
		try {			
			List<StDisplayStaffMap> lst = stDisplayStaffMapDAO.getListDisplayStaffMap(kPaging, displayProgramId, ActiveType.RUNNING,lstShopId,month, checkMap, parentStaffId);
			ObjectVO<StDisplayStaffMap> vo = new ObjectVO<StDisplayStaffMap>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception e) {			
			throw new BusinessException(e);
		}
	}
	@Override
	public StDisplayStaffMap getDisplayStaffMap(Long dpId,Long staffId,Date month) throws BusinessException{
		try {
			return stDisplayStaffMapDAO.getDisplayStaffMap(dpId, staffId, month);
		} catch (Exception e) {			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StDisplayPlAmtDtl getStDisplayPlAmtDtlByAmtGr(Long levelId,Long grId) throws BusinessException{
		try {
			return stDisplayPdGroupDtlDAO.getStDisplayPlAmtDtlByAmtGr(levelId,grId);
		} catch (Exception e) {			
			throw new BusinessException(e);
		}		
	}
	
	@Override
	public StDisplayPlDpDtl getStDisplayPlPrDtlByGr(Long grId,Long productId) throws BusinessException{
		try {
			return stDisplayPdGroupDtlDAO.getStDisplayPlPrDtlByGr(grId, productId);
		} catch (Exception e) {			
			throw new BusinessException(e);
		}		
	}
		
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void appendStDisplayStaffMap(List<StDisplayStaffMap> stDisplayStaffMaps, LogInfoVO logInfo)
			throws BusinessException {
		try {
			for(StDisplayStaffMap staffMap:stDisplayStaffMaps){
				if(staffMap.getId()==null){
					stDisplayStaffMapDAO.createDisplayStaffMap(staffMap, logInfo);
				}else {
					stDisplayStaffMapDAO.updateDisplayStaffMap(staffMap, logInfo);
				}
			}
		} catch (Exception e) {			
			throw new BusinessException(e);
		}
		
	}
	
	@Override
	public Integer checkExistsProductDisplayGr(Long dpId, DisplayProductGroupType type) throws BusinessException {
		try {
			return displayProgrameDAO.checkExistsProductDisplayGr(dpId, type);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public List<StDisplayPDGroupDTLVO> getDisplayPDGroupDTLVOs(Long levelId,Long st_display_pd_group_id) throws BusinessException {		
		
		try {
			return stDisplayPdGroupDtlDAO.getDisplayPDGroupDTLVOs(levelId,st_display_pd_group_id);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public StDisplayProgram getStDisplayProgrameById(Long id)
			throws BusinessException {
		try {
			return displayProgrameDAO.getStDisplayProgramById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public StDisplayPdGroupDtl getDisplayProductGroupDTLByGroupId(Long stDisplayPdGroupId,
			Long productId)
			throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return stDisplayPdGroupDtlDAO.getDisplayProductGroupDTLByGroupId(stDisplayPdGroupId, productId);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public ObjectVO<DisplayDetailVO> getListCTTBbyId(
			KPaging<DisplayDetailVO> kPaging, Long shopId, Long idCTTB,
			Date date) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<DisplayDetailVO> lstObject = displayProgrameDetailDAO
					.getListCTTBbyId(kPaging, shopId, idCTTB, date);
			ObjectVO<DisplayDetailVO> vo = new ObjectVO<DisplayDetailVO>();
			vo.setLstObject(lstObject);
			vo.setkPaging(kPaging);
			return vo;
		} catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public void createOrUpdateDisplayProgameCustomer(
			CustomerDisplayPrograme customerDisplayPrograme)
			throws BusinessException {
		// TODO Auto-generated method stub
		try {
			CustomerDisplayPrograme c = customerDisplayProgrameDAO
					.getCustomerDisplayPrograme(customerDisplayPrograme, false);
			if (c != null) {
				if (c.getStaff().getId() != customerDisplayPrograme
						.getStaff().getId()
						|| c.getLevelCode().compareTo(
								customerDisplayPrograme.getLevelCode()) != 0) {
					c.setUpdateDate(DateUtility.now());
					c.setUpdateUser(customerDisplayPrograme.getCreateUser());
					c.setLevelCode(customerDisplayPrograme.getLevelCode());
					c.setStaff(customerDisplayPrograme.getStaff());
					c.setFromDate(DateUtility.parse(
							"01/"
									+ DateUtility
											.toMonthYearString(customerDisplayPrograme
													.getFromDate()),
							DateUtility.DATE_FORMAT_STR));
					if (customerDisplayPrograme.getToDate() != null) {
						c.setToDate(commonDAO
								.getLastDate(customerDisplayPrograme
										.getToDate()));
					}
					customerDisplayProgrameDAO.updateCustomerDisplayPrograme(c);
				}
			} else {
				customerDisplayPrograme.setCreateDate(commonDAO.getSysDate());
				customerDisplayPrograme
						.setFromDate(DateUtility.parse(
								"01/"
										+ DateUtility
												.toMonthYearString(customerDisplayPrograme
														.getFromDate()),
								DateUtility.DATE_FORMAT_STR));
				customerDisplayPrograme.setToDate(commonDAO
						.getLastDate(customerDisplayPrograme.getFromDate()));
				customerDisplayProgrameDAO
						.createCustomerDisplayPrograme(customerDisplayPrograme);
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}

	}

	@Override
	public List<StDisplayProgram> getListDisplayProgramSysdateEX(List<Long> shopId,
			Date month) throws BusinessException {
		try {
			return displayProgrameDAO.getListDisplayProgramSysdateEX(shopId,month);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<CustomerDisplayProgrameVO> getListCustomerDisplayPrograme(
			KPaging<CustomerDisplayProgrameVO> kPaging, Long shopId,
			Long id, String month) throws BusinessException {
		try {
			List<CustomerDisplayProgrameVO> lst = customerDisplayProgrameDAO
					.getListCustomerDisplayPrograme(kPaging, shopId,
							id, month);
			ObjectVO<CustomerDisplayProgrameVO> vo = new ObjectVO<CustomerDisplayProgrameVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public ObjectVO<Customer> getListCustomerInLevel(KPaging<Customer> kPaging,
			String displayProgramCode, String levelCode)
			throws BusinessException {
		try {
			List<Customer> lst = customerDisplayProgrameDAO
					.getListCustomerInLevel(kPaging, displayProgramCode,
							levelCode);
			ObjectVO<Customer> vo = new ObjectVO<Customer>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteListCustomerDisplayPrograme(
			List<Long> lstCustomerDisplayPrograme) throws BusinessException {
		// TODO Auto-generated method stub
		try {

			customerDisplayProgrameDAO
					.deleteListCustomerDisplayPrograme(lstCustomerDisplayPrograme);

		} catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public int countShopConnectBy(Long shopId) throws BusinessException {
		try {
			return customerDisplayProgrameDAO.countShopConnectBy(shopId);
		} catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public StDisplayProgramLevel checkLevelCodeExists(
			Long id, String levelCode)
			throws BusinessException {
		try {
			return customerDisplayProgrameDAO.checkLevelCodeExists(
					id, levelCode);
		} catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public List<CustomerDisplayPrograme> checkListDisplayProgrameCustomer(
			List<CustomerDisplayPrograme> lstCusDisplayPrograme)
			throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<CustomerDisplayPrograme> list = new ArrayList<CustomerDisplayPrograme>();
			for (CustomerDisplayPrograme item : lstCusDisplayPrograme) {
				if (customerDisplayProgrameDAO.getCustomerDisplayPrograme(item,
						true) != null) {
					list.add(item);
				}
			}
			return list;
		} catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public Boolean checkDisplayProgrameInShop(Long displayProgrameId,
			Long shopId, Date month) throws BusinessException {
		try {
			return displayProgrameDAO.checkDisplayProgrameInShop(
					displayProgrameId, shopId, month);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkDisplayProgrameInShopForCustomer(
			Long displayProgrameId, Long shopIdOfCustomer, Date month)
			throws BusinessException {
		try {
			return displayProgrameDAO.checkDisplayProgrameInShopForCustomer(
					displayProgrameId, shopIdOfCustomer, month);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/***************************** CTTB ****************************************/
	/**
	 * Search CTTB
	 * 
	 * @author thachnn
	 */

	@Override
	public ObjectVO<StDisplayProgram> getListDisplayProgram(
			KPaging<StDisplayProgram> kPaging, StDisplayProgramFilter filter,
			StaffRoleType staffRole) throws BusinessException {
		try {
			List<StDisplayProgram> lst = displayProgrameDAO
					.getListDisplayProgram(kPaging, filter, staffRole);
			ObjectVO<StDisplayProgram> vo = new ObjectVO<StDisplayProgram>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	// loctt
	@Override
	public StDisplayProgram getStDisplayProgramById(Long id)
			throws BusinessException {
		try {
			return displayProgrameDAO.getStDisplayProgramById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}

	// loctt
	@Autowired
	DisplayProgramExclusionDAO displayProgramExclusionDAO;

	@Override
	public ObjectVO<StDisplayProgramExclusion> getListDisplayProgramExclusion(
			KPaging<StDisplayProgramExclusion> kPaging,
			DisplayProgramExclusionFilter filter, StaffRole staffRole)
			throws BusinessException {
		try {
			List<StDisplayProgramExclusion> lst = displayProgramExclusionDAO
					.getListDisplayProgramExclusion(kPaging, filter, staffRole);
			ObjectVO<StDisplayProgramExclusion> vo = new ObjectVO<StDisplayProgramExclusion>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	// loctt

	@Override
	public ObjectVO<StDisplayProgram> getListDisplayProgramForExclusion(
			KPaging<StDisplayProgram> kPaging, DisplayProgramFilter filter)
			throws BusinessException {
		try {
			List<StDisplayProgram> lst = displayProgrameDAO
					.getListDisplayProgramForExclusion(kPaging, filter);
			ObjectVO<StDisplayProgram> vo = new ObjectVO<StDisplayProgram>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * Them moi List CTTB loai tru
	 * @author loctt
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createListDisplayProgramExclusion(StDisplayProgram displayProgram, List<Long> listDPExclusionId,
			LogInfoVO logInfo) throws BusinessException {
		try {
			StDisplayProgram dProgram = this.getStDisplayProgramById(displayProgram.getId());
			for (Long displayProgramExclusionId : listDPExclusionId) {
				StDisplayProgram 	displayPE = displayProgrameDAO.getStDisplayProgramById(displayProgramExclusionId);
				if (displayPE == null) {
					throw new IllegalArgumentException("DisplayProgramMgrImpl.createListDisplayProgramExclusion: Get displayPE from id is null");
				}
				StDisplayProgramExclusion displayProgramExclusion = new StDisplayProgramExclusion();
				displayProgramExclusion.setDisplayProgram(dProgram);
				displayProgramExclusion.setExDisplayProgram(displayPE);
				
				//loctt-23sep2013- them dong setCreateDate
				displayProgramExclusion.setCreateDate(DateUtility.now());
				displayProgramExclusion.setStatus(ActiveType.RUNNING);
				displayProgramExclusionDAO.createDisplayProgramExclusion(displayProgramExclusion, logInfo);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * 
	 * @author loctt
	 */
	@Override
	public StDisplayProgramExclusion getDisplayProgramExclusionById(Long id)
			throws BusinessException {
		try {
			return id == null ? null : displayProgramExclusionDAO.getDisplayProgramExclusionById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	/**
	 * 
	 * @author loctt
	 */
	@Override
	public void updateDisplayProgramExclusion(StDisplayProgramExclusion displayProgramExclusion,
			LogInfoVO logInfo) throws BusinessException {
		try {
			displayProgramExclusionDAO.updateDisplayProgramExclusion(displayProgramExclusion, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	/**
	 * @author thachnn
	 */
	@Override
	public ObjectVO<StDisplayPdGroup> getListDisplayGroup(
			KPaging<StDisplayPdGroup> kPaging, StDisplayPdGroupFilter filter,
			StaffRoleType staffRole) throws BusinessException {
		try {
			List<StDisplayPdGroup> lst = displayProgrameDAO
					.getListDisplayGroup(kPaging, filter, staffRole);
			ObjectVO<StDisplayPdGroup> vo = new ObjectVO<StDisplayPdGroup>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	//loctt
	/**
	 * Danh sach muc cua CTTB
	 * @author thongnm
	 */
	@Override
	public ObjectVO<StDisplayProgramLevel> getListDisplayProgramLevel(KPaging<StDisplayProgramLevel> kPaging, Long displayProgramId)
			throws BusinessException {
		try {
			List<StDisplayProgramLevel> lst = displayProgrameLevelDAO.getListDisplayProgramLevel(kPaging, displayProgramId,null, ActiveType.RUNNING);
			ObjectVO<StDisplayProgramLevel> vo = new ObjectVO<StDisplayProgramLevel>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * @author thachnn
	 */
	@Override
	public ObjectVO<StDisplayPdGroupDtl> getListDisplayGroupDetail(
			KPaging<StDisplayPdGroupDtl> kPaging, Long stDisplayPdGroupId,
			StaffRoleType staffRole) throws BusinessException {
		try {
			List<StDisplayPdGroupDtl> lst = displayProgrameDAO
					.getListDisplayGroupDetail(kPaging, stDisplayPdGroupId, staffRole);
			ObjectVO<StDisplayPdGroupDtl> vo = new ObjectVO<StDisplayPdGroupDtl>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	//loctt sep18, 2013
	@Autowired
	DisplayProductGroupDAO displayProductGroupDAO;
	/**loctt-sep18
	 * Danh sach nhom san pham (type) 1 :DS 2:SKU
	 * @author thongnm
	 */
	@Override
	public ObjectVO<StDisplayPdGroup> getListDisplayProductGroup( KPaging<StDisplayPdGroup> kPaging, 
			Long displayProgramId,DisplayProductGroupType type) throws BusinessException {
		try {
			List<StDisplayPdGroup> lst = displayProductGroupDAO.getListDisplayProductGroup(kPaging, displayProgramId, type, ActiveType.RUNNING,false);
			ObjectVO<StDisplayPdGroup> vo = new ObjectVO<StDisplayPdGroup>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * @author loctt
	 * @since 19sep2013
	 */
	@Override
	public StDisplayProgramLevel getDisplayProgramLevelById(Long id)
			throws BusinessException {
		try {
			return id == null ? null : displayProgrameLevelDAO.getDisplayProgramLevelById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	@Autowired
	DisplayProgramLevelDTLDAO displayProgramLevelDTLDAO;
	/**
	 * @author loctt
	 * @since 19sep2013
	 */
	@Override
	public List<StDisplayPlAmtDtl> getListDisplayProgramLevelDetailByLevelId(Long levelId)
			throws BusinessException {
		try {
			return displayProgramLevelDTLDAO.getListDisplayProgramLevelDetailByLevelId(levelId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * @author loctt
	 * @since 19sep2013
	 */
	@Override
	public StDisplayPdGroup getDisplayProductGroupById(Long id)throws BusinessException {
		try {
			return id == null ? null : displayProductGroupDAO.getDisplayProductGroupById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	/**
	 * @author loctt
	 * @since 19sep2013
	 */
	@Override
	public StDisplayProgramLevel getDisplayProgramLevelByCode(String code, Long displayProgramId)
			throws BusinessException {
		try {
			return displayProgrameLevelDAO.getDisplayProgramLevelByLevelCode(displayProgramId, code, ActiveType.RUNNING);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	/**
	 * Tao moi hoac cap nhat muc cua CTTB
	 * @author loctt
	 * @since 19sep2013
	 */
	@Override
	@Transactional (rollbackFor = Exception.class)
	public void createOrUpdateDisplayProgramLevel(StDisplayProgramLevel displayProgramLevel, 
			List<StDisplayPlAmtDtl> displayPlAmtDtls,
			List<StDisplayPlDpDtl> stDisplayPlDpDtls,
			LogInfoVO logInfo, Boolean isUpdate) throws BusinessException {
		if (displayProgramLevel == null) {
			throw new IllegalArgumentException("displayProgramLevel is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		try {
			StDisplayProgramLevel  programLevel = null;
			if(displayProgramLevel.getId()==null){
				programLevel =  displayProgrameLevelDAO.createDisplayProgramLevel(displayProgramLevel, logInfo);
			}else{
				displayProgrameLevelDAO.updateDisplayProgramLevel(displayProgramLevel, logInfo);
				programLevel = displayProgramLevel;
			}			
			for (StDisplayPlAmtDtl child: displayPlAmtDtls) {
				child.setDisplayProgramLevel(programLevel);
				if(child.getId()==null){					
					stDisplayPdGroupDtlDAO.createStDisplayPlAmtDtlByAmtGr(child);
				}else{
					stDisplayPdGroupDtlDAO.updateStDisplayPlAmtDtlByAmtGr(child);
				}
			}
			for (StDisplayPlDpDtl child: stDisplayPlDpDtls) {
				child.setDisplayProgramLevel(programLevel);
				if(child.getId()==null){
					stDisplayPdGroupDtlDAO.createStDisplayPlPrDtlByGr(child);
				}else{
					stDisplayPdGroupDtlDAO.updateDisplayPlPrDtlByGr(child);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}		
	}
	/**
	 * @author thachnn
	 */
	@Override
	public void updateStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup, LogInfoVO logInfo) throws BusinessException
	{
		try{
			displayProgrameDAO.updateStDisplayPdGroup(stDisplayPdGroup, logInfo);
		}catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StDisplayPdGroup getStDisplayPdGroupByCode(Long stDisplayProgramId,DisplayProductGroupType type, String stDisplayPdGroupCode) throws BusinessException 
	{
		try {
			return displayProgrameDAO.getStDisplayPdGroupByCode(stDisplayProgramId, type, stDisplayPdGroupCode);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StDisplayPdGroup createStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup, LogInfoVO logInfo)throws BusinessException{
		try {
			if (stDisplayPdGroup.getDisplayProgram().getId() == null) {
				throw new IllegalArgumentException("displayProgramId is null");
			}
			if (stDisplayPdGroup.getType() == null) {
				throw new IllegalArgumentException("type is null");
			}
			
			StDisplayPdGroup tmp = displayProgrameDAO.getStDisplayPdGroupByCode(stDisplayPdGroup.getDisplayProgram().getId(), stDisplayPdGroup.getType() ,stDisplayPdGroup.getDisplayProductGroupCode());
			if(tmp != null) throw new IllegalArgumentException("displayProductGroupCode exist");
			return displayProgrameDAO.createStDisplayPdGroup(stDisplayPdGroup, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup) throws BusinessException{
		try{
			displayProgrameDAO.deleteStDisplayPdGroup(stDisplayPdGroup);
		}
		catch(DataAccessException e){
			throw new BusinessException(e);
		}
	}
	@Override
	public StDisplayPdGroup getDisplayProductGroupByCode(Long displayProgramId,
			DisplayProductGroupType type, String displayProductGroupCode)
			throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return displayProductGroupDAO.getDisplayProductGroupByCode(displayProgramId, type, displayProductGroupCode);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}	
	
//	@Override
//	public StDisplayPdGroup getStDisplayPdGroupByCode(Long stDisplayProgramId,DisplayProductGroupType type, String stDisplayPdGroupCode) throws BusinessException 
//	{
//		try {
//			return displayProgrameDAO.getStDisplayPdGroupByCode(stDisplayProgramId, type, stDisplayPdGroupCode);
//		} catch (DataAccessException e) {
//			throw new BusinessException(e);
//		}
//	}
	
//	@Override
//	public StDisplayPdGroup getStDisplayPdGroupByCode(Long stDisplayProgramId,DisplayProductGroupType type, String stDisplayPdGroupCode) throws BusinessException 
//	{
//		try {
//			return displayProgrameDAO.getStDisplayPdGroupByCode(stDisplayProgramId, type, stDisplayPdGroupCode);
//		} catch (DataAccessException e) {
//			throw new BusinessException(e);
//		}
//	}
//	
//	@Override
//	public StDisplayPdGroup createStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup, LogInfoVO logInfo)throws BusinessException{
//		try {
//			if (stDisplayPdGroup.getDisplayProgram().getId() == null) {
//				throw new IllegalArgumentException("displayProgramId is null");
//			}
//			if (stDisplayPdGroup.getType() == null) {
//				throw new IllegalArgumentException("type is null");
//			}
//			
//			StDisplayPdGroup tmp = displayProgrameDAO.getStDisplayPdGroupByCode(stDisplayPdGroup.getDisplayProgram().getId(), stDisplayPdGroup.getType() ,stDisplayPdGroup.getDisplayProductGroupCode());
//			if(tmp != null) throw new IllegalArgumentException("displayProductGroupCode exist");
//			return displayProgrameDAO.createStDisplayPdGroup(stDisplayPdGroup, logInfo);
//		} catch (DataAccessException e) {
//			throw new BusinessException(e);
//		}
//	}
//
//	@Override
//	public void deleteStDisplayPdGroup(StDisplayPdGroup displayProductGroup,
//			LogInfoVO logInfo) throws BusinessException {
//		// TODO Auto-generated method stub
//		try {
//			displayProductGroupDAO.deleteDisplayProductGroup(displayProductGroup, logInfo);
//		} catch (DataAccessException e) {
//
//			throw new BusinessException(e);
//		}
//	}

//	@Override
//	public void deleteStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup) throws BusinessException{
//		try{
//			displayProgrameDAO.deleteStDisplayPdGroup(stDisplayPdGroup);
//		}
//		catch(DataAccessException e){
//			throw new BusinessException(e);
//		}
//	}
	
	

	/*
	 * @author: lochp
	 * danh sach san pham doanh so cua CTTB
	 * 
	 */
	@Override
	public ObjectVO<StDisplayPdGroup> getListDisplayProgrameAmount(KPaging<StDisplayPdGroup> kPaging, StDisplayPdGroupFilter filter, StaffRole staffRole)
	throws BusinessException{
		try {
			List<StDisplayPdGroup> lst = displayProgrameDAO
					.getListDisplayProgrameAmount(kPaging, filter, staffRole);
			ObjectVO<StDisplayPdGroup> vo = new ObjectVO<StDisplayPdGroup>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/*
	 * @author: lochp
	 * danh sach CHI TIET san pham doanh so 
	 * 
	 */
	@Override
	public ObjectVO<StDisplayPdGroupDtl> getListDisplayProgrameAmountProductDetail(
			KPaging<StDisplayPdGroupDtl> kPaging, StDisplayPdGroupFilter filter,
			StaffRole staffRole) throws BusinessException {
		// TODO Auto-generated method stub
		try{
			List<StDisplayPdGroupDtl> lst = displayProgrameDAO.getListDisplayProgrameAmountProductDetail(kPaging, filter, staffRole);
			ObjectVO<StDisplayPdGroupDtl> vo = new ObjectVO<StDisplayPdGroupDtl>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		}catch(DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional (rollbackFor = Exception.class)
	public void deleteListDisplayStaffMap(List<Long> lstId, LogInfoVO logInfo)  throws BusinessException {
		try {
			for(int i = 0; i < lstId.size(); i++) {
				StDisplayStaffMap stDisplayStaffMap = stDisplayStaffMapDAO.getDisplayStaffMapById(lstId.get(i));
		//		stDisplayStaffMap.setStatus(ActiveType.DELETED); commented -loctt - Oct1, 2013
				stDisplayStaffMap.setQuantityMax(null);
				stDisplayStaffMap.setUpdateDate(commonDAO.getSysDate());
				stDisplayStaffMap.setUpdateUser(logInfo.getStaffCode());
				stDisplayStaffMapDAO.updateDisplayStaffMap(stDisplayStaffMap, logInfo);
			}
		} catch(DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional (rollbackFor = Exception.class)
	public void deleteAllDisplayStaffMap(Long displayId, String month, boolean checkMap, long parentStaffId, LogInfoVO logInfo)  throws BusinessException {
		try {
			List<StDisplayStaffMap> lst = stDisplayStaffMapDAO.getListDisplayStaffMap(null, displayId, ActiveType.RUNNING, null, month, checkMap, parentStaffId);
			for(StDisplayStaffMap stD : lst) {
	//			stD.setStatus(ActiveType.DELETED); commented -loctt - Oct1, 2013
				stD.setQuantityMax(null);
				stD.setUpdateDate(commonDAO.getSysDate());
				stD.setUpdateUser(logInfo.getStaffCode());
				stDisplayStaffMapDAO.updateDisplayStaffMap(stD, logInfo);
			}
		} catch(DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author loctt
	 * @since 20sep2013
	 * @param displayProgramLevel
	 * @param logInfo
	 * @throws BusinessException
	 */
	@Override
	public void updateDisplayProgramLevel(StDisplayProgramLevel displayProgramLevel,
			LogInfoVO logInfo) throws BusinessException {
		try {
			displayProgrameLevelDAO.updateDisplayProgramLevel(displayProgramLevel, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}	
	
	@Transactional(rollbackFor = Exception.class)
	@Override
	public void deleteDisplayShopMap(Long shopId, Long displayProgramId,LogInfoVO logInfo) throws BusinessException {
		this.deleteOneDisplayShopMap(shopId, displayProgramId, logInfo);
	}
	private void deleteOneDisplayShopMap(Long shopId, Long displayProgramId, LogInfoVO logInfo) throws BusinessException {
		try {
			Shop sh = shopDAO.getShopById(shopId);
			if (sh == null) throw new BusinessException("shopId is not exist");
			
			List<DisplayShopMap> listDisplayShopMap = displayShopMapDAO.getListChildDisplayShopMap(shopId, displayProgramId, null);
			for (DisplayShopMap dsm: listDisplayShopMap) {
				dsm.setStatus(ActiveType.DELETED);
				displayShopMapDAO.updateDisplayShopMap(dsm, logInfo);
			}
			//kiem tra cha no coi co con chau gi tham gia khong, neu khong thi them cha no vao
//			if (sh.getParentShop() != null) {
//				listDisplayShopMap = displayShopMapDAO.getListChildDisplayShopMap(sh.getParentShop().getShopId(), displayProgramId, false);
//				if (listDisplayShopMap.size() == 0) {
//					DisplayShopMap dsm = new DisplayShopMap();
//					dsm.setShop(sh.getParentShop());
//					//dsm.setDisplayProgram(displayProgramDAO.getDisplayProgrameById(displayProgramId));
//					dsm.setStatus(ActiveType.RUNNING);
//					displayShopMapDAO.createDisplayShopMap(dsm, logInfo);
//				}
//			}
			
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional (rollbackFor = Exception.class)
	public void createListDisplayShopMap(List<DisplayShopMap> lstDisplayShopMap, LogInfoVO logInfo) throws BusinessException {
		try {
			for (DisplayShopMap child: lstDisplayShopMap) {
				displayShopMapDAO.createDisplayShopMap(child, logInfo);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional (rollbackFor = Exception.class)
	public void createDisplayShopMap(DisplayShopMap displayShopMap, LogInfoVO logInfo) throws BusinessException {
		try {
			displayShopMapDAO.createDisplayShopMap(displayShopMap, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StDisplayProgram getStDisplayProgramByCode(String code)
			throws BusinessException {
		try {
			return displayProgramDAO.getStDisplayProgramByCode(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public String checkUpdateDisplayProgramForActive(Long stDisplayProgramId) throws BusinessException {
		try {
			
			List<DisplayShopMap> listDisplayShopMap = null;
			List<StDisplayProgramLevel> listDisplayProgramLevel = null;
			List<StDisplayPdGroup> listDisplayProductGroupDS = null;
			
			listDisplayShopMap = displayShopMapDAO.getListDisplayShopMapByDisplayProgramId(null, stDisplayProgramId, null, null, ActiveType.RUNNING, null);
			listDisplayProgramLevel = displayProgrameLevelDAO.getListDisplayProgramLevel(null, stDisplayProgramId, null, ActiveType.RUNNING);
			listDisplayProductGroupDS = displayProductGroupDAO.getListDisplayProductGroup(null, stDisplayProgramId,null, ActiveType.RUNNING,false);
			
			if(listDisplayShopMap == null || listDisplayShopMap.size() <= 0){
				return ExceptionCode.ERR_ACTIVE_DISPLAY_PROGRAM_DVTG;
			}
			if(listDisplayProgramLevel == null || listDisplayProgramLevel.size() <= 0){
				return ExceptionCode.ERR_ACTIVE_DISPLAY_PROGRAM_MUC;
			}
			if(listDisplayProductGroupDS == null || listDisplayProductGroupDS.size() <= 0){
				return ExceptionCode.ERR_ACTIVE_DISPLAY_PROGRAM_SPDS;
			}
			for (StDisplayPdGroup child : listDisplayProductGroupDS) {
				List<StDisplayPdGroupDtl> lstDetail = displayProductGroupDTLDAO.getListDisplayProductGroupDTL(null, null,child.getId(), ActiveType.RUNNING);
				if(lstDetail != null && lstDetail.size()>0){
					return ExceptionCode.ERR_ACTIVE_PROGRAM_OK;
				}
			}	
			return ExceptionCode.ERR_ACTIVE_DISPLAY_PROGRAM_SPDS;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}			
	}
	
	@Override
	public void updateDisplayProgram(StDisplayProgram stDisplayProgram,LogInfoVO logInfo) throws BusinessException {
		try {
			displayProgramDAO.updateDisplayProgram(stDisplayProgram, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public StDisplayProgram createDisplayProgram(
			StDisplayProgram stDisplayProgram, LogInfoVO logInfo)
			throws BusinessException {
		try {
			if (stDisplayProgram.getFromDate() == null) {
				throw new IllegalArgumentException("DisplayProgramMgrImpl.createDisplayProgram: fromDate must be not null");
			}
			return displayProgramDAO.createDisplayProgram(stDisplayProgram,logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	/*
	 * author: lochp
	 * (non-Javadoc)
	 * @see ths.dms.core.business.DisplayProgrameMgr#deleteStDisplayPdGroup(ths.dms.core.entities.StDisplayPdGroup, ths.dms.core.entities.vo.LogInfoVO)
	 */
	@Override
	public void deleteStDisplayPdGroup(StDisplayPdGroup displayProductGroup,LogInfoVO logInfo) throws BusinessException{		
	
	// TODO Auto-generated method stub
		try {
			displayProductGroupDAO.deleteDisplayProductGroup(displayProductGroup, logInfo);
		} catch (DataAccessException e) {
	
			throw new BusinessException(e);
		}
		
	}
//	@Override
//	public void updateStDisplayPdGroup(StDisplayPdGroup displayProductGroup,
//			LogInfoVO logInfo) throws BusinessException {
//		// TODO Auto-generated method stub
//		try {
//			displayProductGroupDAO.updateDisplayProductGroup(displayProductGroup, logInfo);
//		} catch (DataAccessException e) {
//
//			throw new BusinessException(e);
//		}
//	}

	@Override
	public ObjectVO<StDisplayPdGroupDtl> getListDisplayProductGroupDTL(
			KPaging<StDisplayPdGroupDtl> kPaging, Long displayProductGroupDTLId)
			throws BusinessException {
		try {
			List<StDisplayPdGroupDtl> lst = stDisplayPdGroupDtlDAO.getListDisplayProductGroupDTL(kPaging, displayProductGroupDTLId, ActiveType.RUNNING);
			ObjectVO<StDisplayPdGroupDtl> vo = new ObjectVO<StDisplayPdGroupDtl>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateDisplayProductGroupDTL(
			StDisplayPdGroupDtl displayProductGroupDTL, LogInfoVO logInfo)
			throws BusinessException {
		try {
			stDisplayPdGroupDtlDAO.updateDisplayProductGroupDTL(displayProductGroupDTL, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
		@Override
	public StDisplayPdGroupDtl getDisplayProductGroupDTLById(Long id)
			throws BusinessException {
		try {
			return id == null ? null : stDisplayPdGroupDtlDAO.getDisplayProductGroupDTLById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void createListDisplayProductGroupDTL(
			List<StDisplayPdGroupDtl> listDisplayProductGroupDTL,
			LogInfoVO logInfo) throws BusinessException {
		// TODO Auto-generated method stub
		for (StDisplayPdGroupDtl child: listDisplayProductGroupDTL) {
			try {
				stDisplayPdGroupDtlDAO.createDisplayProductGroupDTL(child, logInfo);
			} catch (DataAccessException e) {
				throw new BusinessException(e);
			}
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public StDisplayProgram copyDisplayProgram(Long displayProgramId,
			LogInfoVO logInfo, String displayProgramCode,
			String displayProgramName) throws BusinessException {
		if (StringUtility.isNullOrEmpty(displayProgramCode)) {
			throw new IllegalArgumentException("displayProgramCode is empty");
		}
		if (StringUtility.isNullOrEmpty(displayProgramName)) {
			throw new IllegalArgumentException("displayProgramName is empty");
		}
		StDisplayProgram newDP = null;
		try {
			StDisplayProgram originalIP = this.getStDisplayProgramById(displayProgramId);
			newDP = this.getStDisplayProgramByCode(displayProgramCode);
		if (newDP != null) {
			throw new IllegalArgumentException("displayProgramCode is exists ");
		}
		/** Thiet lap CTTB moi */
		newDP = new StDisplayProgram();
		newDP.setDisplayProgramCode(displayProgramCode.toUpperCase());
		newDP.setDisplayProgramName(displayProgramName);
		newDP.setStatus(ActiveType.WAITING);
		newDP.setFromDate(originalIP.getFromDate());
		newDP.setToDate(originalIP.getToDate());
		newDP.setCreateDate(commonDAO.getSysDate());
		newDP.setCreateUser(logInfo.getStaffCode());
		


		List<DisplayShopMap> listDisplayShopMap = null;
		List<StDisplayProgramLevel> listDisplayProgramLevel = null;
		List<StDisplayPdGroup> listDisplayProductGroup = null;
		List<StDisplayProgramExclusion> listDisplayProgramExclusion = null;
		
		listDisplayShopMap = displayShopMapDAO.getListDisplayShopMapByDisplayProgramId(null, displayProgramId, null, null, ActiveType.RUNNING, null);
		listDisplayProgramLevel = displayProgrameLevelDAO.getListDisplayProgramLevel(null, displayProgramId, null, ActiveType.RUNNING);
		listDisplayProductGroup = displayProductGroupDAO.getListDisplayProductGroup(null, displayProgramId, null, ActiveType.RUNNING,true);
		
		DisplayProgramExclusionFilter filterExclusion = new DisplayProgramExclusionFilter();
		filterExclusion.setDisplayProgramId(displayProgramId);
		filterExclusion.setStatus(ActiveType.RUNNING);
		listDisplayProgramExclusion = displayProgramExclusionDAO.getListDisplayProgramExclusion(null, filterExclusion, null);
		
		newDP = this.createDisplayProgram(newDP, logInfo);	
		Date sysDate = commonDAO.getSysDate();
		/** Sao chep don vi tham gia CTTB */
		if(listDisplayShopMap != null && listDisplayShopMap.size() > 0){
			for (DisplayShopMap child : listDisplayShopMap) {
				DisplayShopMap newDisplayShopMap = new DisplayShopMap();
				newDisplayShopMap.setShop(child.getShop());
				newDisplayShopMap.setStatus(child.getStatus());
				newDisplayShopMap.setDisplayProgram(newDP);
				newDisplayShopMap.setCreateUser(logInfo.getStaffCode());
				newDisplayShopMap.setCreateDate(sysDate);
				newDisplayShopMap = displayShopMapDAO.createDisplayShopMap(newDisplayShopMap, logInfo);
			}
		}
		/** Sao chep CTTB loai tru */
		if(listDisplayProgramExclusion != null && listDisplayProgramExclusion.size() > 0){
			for (StDisplayProgramExclusion child : listDisplayProgramExclusion) {
				StDisplayProgramExclusion newDisplayProgramExclusion = new StDisplayProgramExclusion();
				newDisplayProgramExclusion.setDisplayProgram(newDP);
				newDisplayProgramExclusion.setExDisplayProgram(child.getExDisplayProgram());
				newDisplayProgramExclusion.setStatus(child.getStatus());
				newDisplayProgramExclusion.setCreateUser(logInfo.getStaffCode());
				newDisplayProgramExclusion.setCreateDate(sysDate);
				newDisplayProgramExclusion = displayProgramExclusionDAO.createDisplayProgramExclusion(newDisplayProgramExclusion, logInfo);
			}
		}		
		/** Sao chep nhom */
		for (StDisplayPdGroup pdGroup : listDisplayProductGroup) {
			StDisplayPdGroup newDisplayPdGroup = new StDisplayPdGroup();				
			newDisplayPdGroup.setDisplayProgram(newDP);
			newDisplayPdGroup.setStatus(pdGroup.getStatus());
			newDisplayPdGroup.setDisplayProductGroupCode(pdGroup.getDisplayProductGroupCode());
			newDisplayPdGroup.setDisplayProductGroupName(pdGroup.getDisplayProductGroupName());
			newDisplayPdGroup.setType(pdGroup.getType());
			newDisplayPdGroup.setCreateUser(logInfo.getStaffCode());
			newDisplayPdGroup.setCreateDate(sysDate);				
			newDisplayPdGroup = displayProductGroupDAO.createDisplayProductGroup(newDisplayPdGroup, logInfo);
		}		
		/** Sao chep muc CTTB va chi tiet nhom san pham tao doanh so */
		if(listDisplayProgramLevel != null && listDisplayProgramLevel.size() > 0){
			for (StDisplayProgramLevel oldLevel : listDisplayProgramLevel) {
				StDisplayProgramLevel newLevel = new StDisplayProgramLevel();
				newLevel.setStDisplayProgram(newDP);
				newLevel.setAmount(oldLevel.getAmount());
				newLevel.setStatus(oldLevel.getStatus());
				newLevel.setLevelCode(oldLevel.getLevelCode());
				newLevel.setNumSku(oldLevel.getNumSku());
				newLevel.setCreateUser(logInfo.getStaffCode());
				newLevel.setCreateDate(sysDate);
				newLevel.setQuantity(oldLevel.getQuantity());
				newLevel = displayProgrameLevelDAO.createDisplayProgramLevel(newLevel, logInfo);
				
				List<StDisplayPlAmtDtl> lstDetail = displayProgramLevelDTLDAO.getListDisplayProgramLevelDTL(null, oldLevel.getId(), ActiveType.RUNNING);
				for (StDisplayPlAmtDtl oldPlAmtDtl : lstDetail) {
					StDisplayPlAmtDtl newPlAmtDtl = new StDisplayPlAmtDtl();
					StDisplayPdGroup pdGroup = displayProductGroupDAO.getDisplayProductGroupByCode(newDP.getId(), oldPlAmtDtl.getDisplayProductGroup().getType(), oldPlAmtDtl.getDisplayProductGroup().getDisplayProductGroupCode());
					newPlAmtDtl.setDisplayProductGroup(pdGroup);
					newPlAmtDtl.setDisplayProgramLevel(newLevel);
					newPlAmtDtl.setMin(oldPlAmtDtl.getMin());
					newPlAmtDtl.setMax(oldPlAmtDtl.getMax());
					newPlAmtDtl.setPrecent(oldPlAmtDtl.getPrecent());
					newPlAmtDtl.setStatus(oldPlAmtDtl.getStatus());
					newPlAmtDtl.setCreateUser(logInfo.getStaffCode());
					newPlAmtDtl.setCreateDate(sysDate);
					newPlAmtDtl = displayProgramLevelDTLDAO.createDisplayProgramLevelDTL(newPlAmtDtl, logInfo);
				}				
			}
		}		
		/** Sao chep nhom san pham trung bay */
		for (StDisplayPdGroup olDPGr : listDisplayProductGroup) {
			List<StDisplayPdGroupDtl> lstDetail = displayProductGroupDTLDAO.getListDisplayProductGroupDTL(null, null,olDPGr.getId(), ActiveType.RUNNING);
			for (StDisplayPdGroupDtl OldPdGroupDtl: lstDetail) {
				StDisplayPdGroupDtl newPdGroupDtl = new StDisplayPdGroupDtl();
				StDisplayPdGroup pdGroup = displayProductGroupDAO.getDisplayProductGroupByCode(newDP.getId(), OldPdGroupDtl.getDisplayProductGroup().getType(), OldPdGroupDtl.getDisplayProductGroup().getDisplayProductGroupCode());
				newPdGroupDtl.setDisplayProductGroup(pdGroup);
				newPdGroupDtl.setProduct(OldPdGroupDtl.getProduct());
				newPdGroupDtl.setConvfact(OldPdGroupDtl.getConvfact());
				newPdGroupDtl.setStatus(OldPdGroupDtl.getStatus());
				newPdGroupDtl.setCreateUser(logInfo.getStaffCode());
				newPdGroupDtl.setCreateDate(sysDate);
				newPdGroupDtl = displayProductGroupDTLDAO.createDisplayProductGroupDTL(newPdGroupDtl, logInfo);
				
				List<StDisplayPlDpDtl> stDisplayPlDpDtls = displayProductGroupDTLDAO.getListDisplayPDGroupDTL(OldPdGroupDtl.getId(), ActiveType.RUNNING);
				for(StDisplayPlDpDtl dpDtl:stDisplayPlDpDtls){
					StDisplayPlDpDtl plDpDtl = new StDisplayPlDpDtl();
					plDpDtl.setCreateDate(sysDate);
					plDpDtl.setCreateUser(logInfo.getStaffCode());
					plDpDtl.setStatus(ActiveType.RUNNING);
					plDpDtl.setQuantity(dpDtl.getQuantity());						
					StDisplayProgramLevel displayProgramLevel = displayProgrameLevelDAO.getDisplayProgramLevelByLevelCode(newDP.getId(), dpDtl.getDisplayProgramLevel().getLevelCode(), ActiveType.RUNNING);
					plDpDtl.setDisplayProgramLevel(displayProgramLevel);
					plDpDtl.setDisplayProductGroupDetail(newPdGroupDtl);
					plDpDtl = stDisplayPdGroupDtlDAO.createStDisplayPlPrDtlByGr(plDpDtl);
				}
			}
		}				
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return newDP;
	}

	@Override
	public Boolean checkExistRecordDisplayShopMap(Long shopId,Long displayProgramId) throws BusinessException {
		try {
			return displayShopMapDAO.checkExistRecordDisplayShopMap(shopId, displayProgramId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Shop> getListShopActiveInDisplayProgram(Long displayProgramId, String shopCode, String shopName) throws BusinessException {
		try {
			if (displayProgramId == null) {
				throw new IllegalArgumentException("displayProgramId is null");
			}
			StDisplayProgram displayProgram = displayProgramDAO.getStDisplayProgramById(displayProgramId);
			if (displayProgram == null) {
				throw new IllegalArgumentException("displayProgram is null");
			}
			return displayShopMapDAO.getListShopActiveInDisplayProgram(displayProgramId, shopCode, shopName);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}


//	@Override
//	public StDisplayPdGroup createStDisplayPdGroup(
//			StDisplayPdGroup displayProductGroup, LogInfoVO logInfo)
//			throws BusinessException {
//		// TODO Auto-generated method stub
//		try{
//			return this.displayProductGroupDAO.createDisplayProductGroup(displayProductGroup, logInfo);
//		}catch(DataAccessException e){
//			throw new BusinessException(e);
//		}
//	}

	/**
	 * @author thachnn
	 * Them, xoa, sua SP vào CTTB - SPTB
	 */
	@Override
	public StDisplayPdGroupDtl getStDisplayPdGroupDtlById(Long id)throws BusinessException
	{
		try {
			return id == null ? null : stDisplayPdGroupDtlDAO.getDisplayProductGroupDTLById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	@Transactional (rollbackFor = Exception.class)
	public void createListStDisplayPdGroupDtl(List<StDisplayPdGroupDtl> listStDisplayPdGroupDtl,LogInfoVO logInfo) throws BusinessException
	{
		for (StDisplayPdGroupDtl child: listStDisplayPdGroupDtl) {
			try {
				stDisplayPdGroupDtlDAO.createStDisplayPdGroupDtl(child, logInfo);
			} catch (DataAccessException e) {
				throw new BusinessException(e);
			}
		}
	}
	@Override
	public void updateStDisplayPdGroupDTL(StDisplayPdGroupDtl stDisplayPdGroupDtl, LogInfoVO logInfo)throws BusinessException{
		try {
			stDisplayPdGroupDtlDAO.updateDisplayProductGroupDTL(stDisplayPdGroupDtl, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public StDisplayPdGroupDtl getStDisplayPdGroupDtlByGroupId(Long displayProductGroupId,Long productId) throws BusinessException
	{
		try {
			return stDisplayPdGroupDtlDAO.getDisplayProductGroupDTLByGroupId(displayProductGroupId,productId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**loctt-sep25
	 * 
	 * @author loctt
	 */

	@Override
	public StDisplayPdGroupDtl getStDisplayPdGroupDtlByProductId(Long productId)
	throws BusinessException {
try {
	return stDisplayPdGroupDtlDAO.getStDisplayPdGroupDtlbyProductId(productId);
} catch (DataAccessException e) {
	throw new BusinessException(e);
}
}
	@Override
	public ObjectVO<StDisplayPdGroup> getListLevelProductGroup( KPaging<StDisplayPdGroup> kPaging, 
			Long displayProgramId,DisplayProductGroupType type,Long levelId) throws BusinessException {
		try {
			List<StDisplayPdGroup> lst = displayProductGroupDAO.getListLevelProductGroup(kPaging, displayProgramId, type, ActiveType.RUNNING,levelId);
			ObjectVO<StDisplayPdGroup> vo = new ObjectVO<StDisplayPdGroup>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author thachnn
	 * @throws DataAccessException 
	 */
	public ObjectVO<Product> getListProduct(KPaging<Product> kPaging, Long displayProgramId, String code, String name, DisplayProductGroupType type) throws BusinessException{
		try{
			List<Product> lst  = displayProgramDAO.getListProduct(kPaging,displayProgramId,code, name, type);
			ObjectVO<Product> vo = new ObjectVO<Product>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author loctt
	 * @since 25sep2013
	 */
	@Override
	public List<StDisplayPlDpDtl> getListDisplayProductLevelDetailByLevelId(Long levelId)
			throws BusinessException {
		try {
			return displayProgramLevelDTLDAO.getListDisplayProductLevelDetailByLevelId(levelId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/*
	 * lochp
	 * (non-Javadoc)
	 * @see ths.dms.core.business.DisplayProgrameMgr#getStDisplayPdGroupDtlByProductId(java.lang.Long, java.lang.Long)
	 */
	@Override
	public StDisplayPdGroupDtl getStDisplayPdGroupDtlByProductId(Long stDisplayPdGroupDtlId, Long productId) throws BusinessException{
		try{
			return stDisplayPdGroupDtlDAO.getStDisplayPdGroupDtlByProductId(stDisplayPdGroupDtlId, productId);
		}catch(DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ProductVO> getListGroupProductVO( KPaging<ProductVO> kPaging, String productCode, String productName,
			Long displayProgramId,DisplayProductGroupType type) throws BusinessException {
		try {
			List<ProductVO> lst = productDAO.getListGroupProductVO(kPaging, productCode, productName, displayProgramId, type);
			ObjectVO<ProductVO> vo = new ObjectVO<ProductVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateDisplayStaffMap(StDisplayStaffMap displayStaffMap, LogInfoVO logInfo)
			throws BusinessException {
		try {
			stDisplayStaffMapDAO.updateDisplayStaffMap(displayStaffMap,logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public StDisplayStaffMap createDisplayStaffMap(StDisplayStaffMap displayStaffMap, LogInfoVO logInfo)
			throws BusinessException {
		try {
			return stDisplayStaffMapDAO.createDisplayStaffMap(displayStaffMap,logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public CustomerDisplayPrograme createCustomerDisplayPrograme(
			CustomerDisplayPrograme customerDisplayPrograme)
			throws BusinessException {
		try {
			return customerDisplayProgrameDAO.createCustomerDisplayPrograme(customerDisplayPrograme);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateCustomerDisplayPrograme(CustomerDisplayPrograme customerDisplayPrograme)
			throws BusinessException {
		try {
			customerDisplayProgrameDAO.updateCustomerDisplayPrograme(customerDisplayPrograme);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public CustomerDisplayPrograme checkExistDisplayCustomer(Long id,Long customerId,Long staffId,String levelCode,String month)throws BusinessException {
		try {
			return customerDisplayProgrameDAO.checkExistDisplayCustomer(id,customerId,staffId,levelCode,month);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public CustomerDisplayPrograme getCustomerDisplayProgrameById(Long id)
			throws BusinessException {
		try {
			return customerDisplayProgrameDAO.getCustomerDisplayProgrameById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public void deleteCustomerDisplayPrograme(CustomerDisplayPrograme customerDisplayPrograme)
			throws BusinessException {
		try {
			customerDisplayProgrameDAO.deleteCustomerDisplayPrograme(customerDisplayPrograme);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public ObjectVO<CustomerDisplayProgrameVO> getListCustomerDisplayProgrameEX(
			KPaging<CustomerDisplayProgrameVO> kPaging, List<Long> shopId,
			Long id, String month, boolean checkMap, long parentStaffId) throws BusinessException {
		try {
			List<CustomerDisplayProgrameVO> lst = customerDisplayProgrameDAO
					.getListCustomerDisplayProgrameEX(kPaging, shopId, id, month, checkMap, parentStaffId);
			ObjectVO<CustomerDisplayProgrameVO> vo = new ObjectVO<CustomerDisplayProgrameVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	@Override
	public ObjectVO<StDisplayProgram> getListDisplayProgramByShop(KPaging<StDisplayProgram> kPaging, Long shopId, String displayProgramCode, String displayProgramName) throws BusinessException {
		try {
			if(shopId == null) {
				throw new IllegalArgumentException("shopId can not null");
			}
			List<StDisplayProgram> lst = displayProgrameDAO.getListDisplayProgramByShop(kPaging, shopId, displayProgramCode, displayProgramName);
			ObjectVO<StDisplayProgram> vo = new ObjectVO<StDisplayProgram>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		}  catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public Integer checkShopOfStaffJoinDisplayProgram(Long displayProgramId,Long shopId) throws BusinessException{
		try {
			return stDisplayStaffMapDAO.checkShopOfStaffJoinDisplayProgram(displayProgramId, shopId);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public ObjectVO<StDisplayProgram> getListDisplayProgramByYear(KPaging<StDisplayProgram> kPaging, Long shopId, Integer year, String code, String name) throws BusinessException{
		try {
			List<StDisplayProgram> lst = new ArrayList<StDisplayProgram>();
			lst =  displayProgrameDAO.getListDisplayProgramByYear(kPaging, shopId, year, code, name);
			ObjectVO<StDisplayProgram> vo = new ObjectVO<StDisplayProgram>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		}  catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public List<StDisplayProgram> getListDisplayProgramByListId(Long shopId, List<Long> lstId, Boolean flag, Integer year, String code, String name) throws BusinessException{
		try {
			return displayProgrameDAO.getListDisplayProgramByListId(shopId, lstId, flag, year, code, name);
		}  catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public ObjectVO<StDisplayProgram> getListDisplayProgramByShopAndYear(KPaging<StDisplayProgram> kPaging,Long shopID,Integer year,String code, String name) throws BusinessException{
		try{
			List<StDisplayProgram> lstObject =  displayShopMapDAO.getListDisplayProgramByShopAndYear(kPaging,shopID, year,code,name);
			ObjectVO<StDisplayProgram> result = new ObjectVO<StDisplayProgram>();
			result.setkPaging(kPaging);
			result.setLstObject(lstObject);
			return result;
		}catch(DataAccessException e){
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<StDisplayProgramVNM> getListDisplayProgramInTowDate(
			KPaging<StDisplayProgramVNM> kPaging, Long shopId, Date fDate,
			Date tDate, String code, String name) throws BusinessException {
		try {
			List<StDisplayProgramVNM> lst = new ArrayList<StDisplayProgramVNM>();
			lst =  displayProgrameDAO.getListDisplayProgramInTowDate(kPaging, shopId, fDate, tDate, code, name);
			ObjectVO<StDisplayProgramVNM> vo = new ObjectVO<StDisplayProgramVNM>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		}  catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	@Override
	public StDisplayProgramVNM getDisplayProgramByCode(String code)
			throws BusinessException {
		try {
			return displayProgrameDAO.getDisplayProgramByCode(code);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
}

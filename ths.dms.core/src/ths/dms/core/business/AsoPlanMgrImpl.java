/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.AsoPlan;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.Routing;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.AsoPlanType;
import ths.dms.core.entities.vo.AsoPlanVO;
import ths.dms.core.entities.vo.DynamicBigVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.RoutingVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.CommonFilter;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.AsoPlanDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.CycleDAO;
import ths.dms.core.dao.RoutingDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.StaffDAO;

/**
 * AsoPlanMgrImpl phan bo Aso
 * @author vuongmq
 * @since 19/10/2015 
 */
public class AsoPlanMgrImpl implements AsoPlanMgr {

	@Autowired
	StaffDAO staffDAO;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	CycleDAO cycleDAO;
	
	@Autowired
	RoutingDAO routingDAO;
	
	@Autowired
	AsoPlanDAO asoPlanDAO;
	
	@Override
	public List<AsoPlan> getListAsoByFilter(CommonFilter filter) throws BusinessException {
		try {
			return asoPlanDAO.getListAsoByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public AsoPlanVO getAsoPlanVOByFilter(CommonFilter filter) throws BusinessException {
		try {
			return asoPlanDAO.getAsoPlanVOByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<AsoPlanVO> getListAsoPlanVOByFilter(CommonFilter filter) throws BusinessException {
		try {
			return asoPlanDAO.getListAsoPlanVOByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<RoutingVO> getRoutingVOByFilter(CommonFilter filter) throws BusinessException {
		try {
			return asoPlanDAO.getRoutingVOByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createOrUpdateListAso(AsoPlanVO asoPlanVO, List<DynamicBigVO> lstAsoPlanObject, LogInfoVO log) throws BusinessException {
		try {
			Cycle cycle = cycleDAO.getCycleById(asoPlanVO.getCycleId());
			Routing routing = routingDAO.getRoutingById(asoPlanVO.getRoutingId());
			Shop shop = shopDAO.getShopByCode(asoPlanVO.getShopCode());
			Staff staff = staffDAO.getStaffByCode(asoPlanVO.getStaffCode());
			if (shop != null && cycle != null && staff != null) {
				CommonFilter fi = new CommonFilter();
				Integer type = asoPlanVO.getObjectType();
				if (AsoPlanType.SKU.getValue().equals(type)) {
					type = AsoPlanType.SUB_CAT.getValue();
				} else {
					type = AsoPlanType.SKU.getValue();
				}
				fi.setPlanType(type);
				fi.setShopId(shop.getId());
				fi.setCycleId(cycle.getId());
				fi.setRoutingId(asoPlanVO.getRoutingId());
				List<AsoPlan> lstAsoPlan = asoPlanDAO.getListAsoByFilter(fi);
				if (lstAsoPlan != null && lstAsoPlan.size() > 0) {
					// xoa het du lieu khac type
					for (AsoPlan asoPlan : lstAsoPlan) {
						commonDAO.deleteEntity(asoPlan);
					}
				}
				Date sys = commonDAO.getSysDate();
				Map<Long, BigDecimal> mapAsoPlanValue = new HashMap<Long, BigDecimal>();
				List<Long> lstObjectId = new ArrayList<Long>();
				for (int i = 0, n = lstAsoPlanObject.size(); i < n; i++) {
					DynamicBigVO row = lstAsoPlanObject.get(i);
					if (!StringUtility.isNullOrEmpty(row.getKey()) && row.getValue() != null) {
						Long id = Long.parseLong(row.getKey());
						lstObjectId.add(id);
						mapAsoPlanValue.put(id, row.getValue());
					}
				}
				//xu ly xoa danh sach khac lstObjectId neu co trong DB thi delete
				CommonFilter filter = new CommonFilter();
				filter.setPlanType(asoPlanVO.getObjectType());
				if (shop != null) {
					filter.setShopId(shop.getId());
				}
				filter.setCycleId(asoPlanVO.getCycleId());
				filter.setRoutingId(asoPlanVO.getRoutingId());
				filter.setLstId(lstObjectId);
				filter.setNotIn(true);
				List<AsoPlan> lstAsoPlanDeleteDB = asoPlanDAO.getListAsoByFilter(filter);
				if (lstAsoPlanDeleteDB != null && lstAsoPlanDeleteDB.size() > 0) {
					for (AsoPlan asoPlan : lstAsoPlanDeleteDB) {
						commonDAO.deleteEntity(asoPlan);
					}
				}
				// xu ly lay danh sach lstObjectId neu co trong DB thi update
				filter.setNotIn(false);
				List<AsoPlan> lstAsoPlanUpdate = asoPlanDAO.getListAsoByFilter(filter);
				List<Long> lstObjectIdUpdate = new ArrayList<Long>();
				if (lstAsoPlanUpdate != null && lstAsoPlanUpdate != null) {
					for (AsoPlan asoPlan : lstAsoPlanUpdate) {
						//danh sach update
						Long id = asoPlan.getObjectId();
						asoPlan.setUpdateDate(sys);
						asoPlan.setUpdateUser(log.getStaffCode());
						asoPlan.setPlan(mapAsoPlanValue.get(id));
						lstObjectIdUpdate.add(id);
					}
				}
				List<AsoPlan> lstAsoPlanNew = new ArrayList<AsoPlan>();
				for (Entry<Long, BigDecimal> item : mapAsoPlanValue.entrySet()) {
					if (!lstObjectIdUpdate.contains(item.getKey())) {
						//danh sach tao moi
						AsoPlan asoPlan = new AsoPlan();
						asoPlan.setCycle(cycle);
						asoPlan.setRouting(routing);
						asoPlan.setShop(shop);
						asoPlan.setObjectId(item.getKey());
						asoPlan.setObjectType(AsoPlanType.parseValue(asoPlanVO.getObjectType()));
						asoPlan.setPlan(mapAsoPlanValue.get(item.getKey()));
						asoPlan.setCreateDate(sys);
						asoPlan.setCreateUser(log.getStaffCode());
						lstAsoPlanNew.add(asoPlan);
					}
				}
				if (lstAsoPlanNew != null && lstAsoPlanNew.size() > 0) {
					commonDAO.creatListEntity(lstAsoPlanNew);
				}
				if (lstAsoPlanUpdate != null && lstAsoPlanUpdate.size() > 0) {
					commonDAO.updateListEntity(lstAsoPlanUpdate);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createOrUpdateImportAso(AsoPlan asoPlan, List<AsoPlan> lstAsoPlan) throws BusinessException {
		try {
			if (asoPlan != null) {
				if (lstAsoPlan != null && lstAsoPlan.size() > 0) {
					// xoa het du lieu khac type
					for (AsoPlan asoPlanDelete : lstAsoPlan) {
						commonDAO.deleteEntity(asoPlanDelete);
					}
				}
				CommonFilter filter = new CommonFilter();
				if (asoPlan.getShop() != null) {
					filter.setShopId(asoPlan.getShop().getId());
				}
				if (asoPlan.getRouting() != null) {
					filter.setRoutingId(asoPlan.getRouting().getId());
				}
				if (asoPlan.getCycle() != null) {
					filter.setCycleId(asoPlan.getCycle().getId());
				}
				if (asoPlan.getObjectType() != null) {
					filter.setPlanType(asoPlan.getObjectType().getValue());
				}
				filter.setObjectId(asoPlan.getObjectId());
				List<AsoPlan> lstAsoPlanCheck = asoPlanDAO.getListAsoByFilter(filter);
				if (lstAsoPlanCheck != null && lstAsoPlanCheck.size() > 0) {
					// ton tai recode cua aso thi update
					AsoPlan asoPlanUpdate = lstAsoPlanCheck.get(0);
					asoPlanUpdate.setUpdateDate(asoPlan.getCreateDate());
					asoPlanUpdate.setUpdateUser(asoPlan.getCreateUser());
					asoPlanUpdate.setPlan(asoPlan.getPlan());
					commonDAO.updateEntity(asoPlanUpdate);
				} else {
					// tao moi aso
					commonDAO.createEntity(asoPlan);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void resetAso(AsoPlanVO asoPlanVO, LogInfoVO logInfoVO) throws BusinessException {
		try {
			if (asoPlanVO != null) {
				CommonFilter fi = new CommonFilter();
				fi.setPlanType(asoPlanVO.getObjectType());
				fi.setShopId(asoPlanVO.getShopId());
				fi.setCycleId(asoPlanVO.getCycleId());
				fi.setRoutingId(asoPlanVO.getRoutingId());
				List<AsoPlan> lstAsoPlan = asoPlanDAO.getListAsoByFilter(fi);
				if (lstAsoPlan != null && lstAsoPlan.size() > 0) {
					// xoa het du lieu 
					for (AsoPlan asoPlanDelete : lstAsoPlan) {
						commonDAO.deleteEntity(asoPlanDelete);
					}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
}

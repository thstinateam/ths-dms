package ths.dms.core.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.OfficeDocShopMap;
import ths.dms.core.entities.OfficeDocument;
//import ths.dms.core.entities.OfficeDocShopMap;
//import ths.dms.core.entities.OfficeDocument;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DocumentType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.filter.ImageFilter;
import ths.dms.core.entities.filter.OfficeDocumentFilter;
//import ths.dms.core.entities.filter.OfficeDocumentFilter;
import ths.dms.core.entities.vo.ImageVO;
//import ths.dms.core.entities.vo.ImageVO1;
import ths.dms.core.entities.vo.MediaItemCustomerVO;
import ths.dms.core.entities.vo.MediaItemDetailVO;
import ths.dms.core.entities.vo.MediaItemProgramVO;
import ths.dms.core.entities.vo.MediaItemShopVO;
import ths.dms.core.entities.vo.MediaItemVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OfficeDocmentVO;
//import ths.dms.core.entities.vo.OfficeDocmentVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.LogUtility;
import ths.dms.core.dao.DisplayShopMapDAO;
import ths.dms.core.dao.MediaItemDAO;

public class ImageMgrImpl implements ImageMgr{

	@Autowired
	private MediaItemDAO mediaItemDAO;
	@Autowired
	private DisplayShopMapDAO displayProgrameShopMapDAO;
	
	@Override
	public MediaItem getMediaItemById(Long id) throws BusinessException {
		try{
			return mediaItemDAO.getMediaItemById(id);
		}catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public MediaItemVO getMediaItemVOById(Long id) throws BusinessException {		
		try {
			return mediaItemDAO.getMediaItemVOById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ImageVO> getListImageVO(ImageFilter filter)
			throws BusinessException {		
		try{
			List<ImageVO> list=mediaItemDAO.getListImageVO(filter);
			ObjectVO<ImageVO> vo=new ObjectVO<ImageVO>();
			vo.setLstObject(list);
			vo.setkPaging(filter.getkPaging());
			return vo;
		}catch (Exception e) {			
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ImageVO> getListImageVOMT(ImageFilter filter)
			throws BusinessException {		
		try{
			List<ImageVO> list=mediaItemDAO.getListImageVOMT(filter);
			ObjectVO<ImageVO> vo=new ObjectVO<ImageVO>();
			vo.setLstObject(list);
			vo.setkPaging(filter.getkPaging());
			return vo;
		}catch (Exception e) {			
			throw new BusinessException(e);
		}
	}


	@Override
	public ObjectVO<ImageVO> getListImageVOGroup(ImageFilter filter)
			throws BusinessException {
		try{
			List<ImageVO> list=mediaItemDAO.getListImageVOGroup(filter);
			ObjectVO<ImageVO> vo=new ObjectVO<ImageVO>();
			vo.setLstObject(list);
			vo.setkPaging(filter.getkPaging());
			return vo;
		}catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ImageVO> getListImageVOGroupMT(ImageFilter filter)
			throws BusinessException {
		try{
			List<ImageVO> list=mediaItemDAO.getListImageVOGroupMT(filter);
			ObjectVO<ImageVO> vo=new ObjectVO<ImageVO>();
			vo.setLstObject(list);
			vo.setkPaging(filter.getkPaging());
			return vo;
		}catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ImageVO> getListImageVODetail(ImageFilter filter)
			throws BusinessException {
		try{
			List<ImageVO> list=mediaItemDAO.getListImageVO(filter);
			ObjectVO<ImageVO> vo=new ObjectVO<ImageVO>();
			vo.setLstObject(list);
			vo.setkPaging(filter.getkPaging());
			return vo;
		}catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ImageVO> getListImageVODetailMT(ImageFilter filter)
			throws BusinessException {
		try{
			List<ImageVO> list=mediaItemDAO.getListImageVOMT(filter);
			ObjectVO<ImageVO> vo=new ObjectVO<ImageVO>();
			vo.setLstObject(list);
			vo.setkPaging(filter.getkPaging());
			return vo;
		}catch (Exception e) {
			throw new BusinessException(e);
		}
	}


	@Override
	public ObjectVO<ImageVO> getListCTTB(ImageFilter filter)
			throws BusinessException {
		try{
			List<ImageVO> list=displayProgrameShopMapDAO.getListCTTB(filter);
			ObjectVO<ImageVO> vo=new ObjectVO<ImageVO>();
			vo.setLstObject(list);
			vo.setkPaging(filter.getkPaging());
			return vo;
		}catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ImageVO> getListCTTBByNPP(Long id) throws BusinessException {
		try {
			return displayProgrameShopMapDAO.getListCTTBByNPP(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ImageVO> getListCTTBByListShop(List<Long> lstShopId) throws BusinessException {
		try {
			return displayProgrameShopMapDAO.getListCTTBByListShop(lstShopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean deleteImageVO(Long id) throws BusinessException {
		try {
			MediaItem mItem=mediaItemDAO.getMediaItemById(id);
			mItem.setStatus(ActiveType.DELETED);
			mediaItemDAO.updateMediaItem(mItem);
			return true;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

//	@Override
//	public ObjectVO<ImageVO1> getListImagesKA(KPaging<ImageVO1> kPaging, Long shopId, Long staffId, Date fromDate, Date toDate, Integer type, Long customerId) throws BusinessException{
//		try {
//			List<ImageVO1> list = mediaItemDAO.getListImagesKA(kPaging, shopId, staffId, fromDate, toDate, type, customerId);
//			ObjectVO<ImageVO1> vo=new ObjectVO<ImageVO1>();
//			vo.setLstObject(list);
//			vo.setkPaging(kPaging);
//			return vo;
//		} catch (Exception e) {
//			throw new BusinessException(e);
//		}
//	}

//	@Override
//	public OfficeDocument createDocument(OfficeDocument document)
//			throws BusinessException {
//		try {
//			return mediaItemDAO.createDocument(document);
//		} catch (Exception e) {
//			throw new BusinessException(e);
//		}		
//	}

	@Override
	public ObjectVO<OfficeDocument> getListOfficeDoc(KPaging<OfficeDocument> kPaging, OfficeDocumentFilter filter) throws BusinessException {
		try {
			List<OfficeDocument> list = mediaItemDAO.getListOfficeDoc(kPaging, filter);
			
			ObjectVO<OfficeDocument> vo = new ObjectVO<OfficeDocument>();
			vo.setLstObject(list);
			vo.setkPaging(kPaging);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public OfficeDocument getOfficeDocumentById(Long id)throws BusinessException {
		try {
			return mediaItemDAO.getOfficeDocumentById(id);
		} catch (Exception e) {
			throw new BusinessException(e);
		}	
	}
	
	@Override
	public MediaItem getMediaItemByDocumentID(Long id)throws BusinessException {
		try {
			return mediaItemDAO.getMediaItemByDocumentID(id);
		} catch (Exception e) {
			throw new BusinessException(e);
		}	
	}
	
	@Override
	public MediaItem getMediaItemByDocumentID (Long id, Integer object_type) throws BusinessException {
		try {
			return mediaItemDAO.getMediaItemByDocumentID(id, object_type);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public OfficeDocument getOfficeDocumentByCode(String documentCode)throws BusinessException {
		try {
			return mediaItemDAO.getOfficeDocumentByCode(documentCode);
		} catch (Exception e) {
			throw new BusinessException(e);
		}	
	}

/*	@Override
	public void deleteDocument(Long Id) throws BusinessException {
		try {
			mediaItemDAO.deleteDocument(Id);
		} catch (Exception e) {
			throw new BusinessException(e);
		}	
		
	}*/
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Integer insertOfficeDocument(OfficeDocmentVO filterVO) throws BusinessException {
		try {
			//Insert Office Document
			OfficeDocument offdoc = new OfficeDocument();
			OfficeDocument offdocIS = new OfficeDocument();
			offdoc.setCreateDate(filterVO.getCreateDate());
			offdoc.setCreateUser(filterVO.getCreateUser());
			offdoc.setDocumentCode(filterVO.getDocumentCode().toUpperCase());
			offdoc.setDocumentName(filterVO.getDocumentName());
			offdoc.setStatus(ActiveType.RUNNING);
			if(filterVO.getType()!=null){
				offdoc.setType(DocumentType.parseValue(filterVO.getType()));
			}
			offdoc.setDocumentName(filterVO.getDocumentName());
			if(filterVO.getFromDate()!=null){
				offdoc.setFromDate(filterVO.getFromDate());
			}
			if(filterVO.getToDate()!=null){
				offdoc.setToDate(filterVO.getToDate());
			}
			offdocIS = mediaItemDAO.createDocument(offdoc);
			if(offdocIS!=null){
				//Insert Media Item
				MediaItem mediaItem = new MediaItem();
				mediaItem.setWidth(filterVO.getWidth());
				mediaItem.setHeight(filterVO.getHeight());
				mediaItem.setUrl(filterVO.getUrl());
				mediaItem.setObjectId(offdocIS.getId());
				mediaItem.setObjectType(MediaObjectType.IMAGE_OFFICE_DOCUMENT);
				mediaItem.setCreateDate(filterVO.getCreateDate());
				mediaItem.setCreateUser(filterVO.getCreateUser());
				mediaItem.setStatus(ActiveType.RUNNING);
				if(mediaItemDAO.createMediaItem(mediaItem)==null){
					return -1;
				}
			}else{
				return 0;
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		return 1;
	}
	
	@Override
	public Integer deleteOfficeDocument(OfficeDocument officeDocument) throws BusinessException {
		try {
			return mediaItemDAO.deleteOfficeDocument(officeDocument);
		} catch (Exception e) {
			throw new BusinessException(e);
		}	
		
	}

	@Override
	public void updateMediaItem(MediaItem image) throws BusinessException {
		try{
			mediaItemDAO.updateMediaItem(image);
		}catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<OfficeDocShopMap> getListOffShopMapToDocumentId(Long documentID) throws BusinessException {
		try{
			return	mediaItemDAO.getListOffShopMapToDocumentId(documentID);
		}catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public Integer insertOffDocShopMap (List<Shop>listShop, OfficeDocument offDoc, OfficeDocmentVO filterVO) throws BusinessException {
		/*Integer kq = 1;
		try {
			Integer flag = 1;
			for(Shop s:listShop){
				if(s.getType().getObjectType() ==3){ // shop NPP
					OfficeDocShopMap docShopMap = new OfficeDocShopMap();
					docShopMap.setShopId(s);
					docShopMap.setOfficeDocumentId(offDoc);
					docShopMap.setCreateDate(filterVO.getCreateDate());
					docShopMap.setCreateUser(filterVO.getCreateUser());
					docShopMap.setStatus(ActiveType.RUNNING);
					flag = mediaItemDAO.insertOffDocShopMap(docShopMap);
				}
				if(flag!=1){
					break;
				}
			}
			kq = flag;
		} catch (Exception e) {
			kq = -2;
			throw new BusinessException(e);
		}
		return kq;*/
		return null;
	}
	
//	@Override
//	public Integer updateOffDocShopMap(List<Shop>listShop, OfficeDocument offDoc, OfficeDocmentVO filterVO) throws BusinessException {
//		Integer kq = 1;
//		try {
//			Integer flag = 1;
//			for(Shop s:listShop){
//				OfficeDocShopMap docShopMap = new OfficeDocShopMap();
//				docShopMap.setShopId(s);
//				docShopMap.setOfficeDocumentId(offDoc);
//				docShopMap.setUpdateDate(filterVO.getCreateDate());
//				docShopMap.setUpdateUser(filterVO.getCreateUser());
//				docShopMap.setStatus(ActiveType.DELETED);
//				flag = mediaItemDAO.updateOffDocShopMap(docShopMap);
//				if(flag!=1){
//					break;
//				}
//			}
//			kq = flag;
//		} catch (Exception e) {
//			kq = -2;
//			throw new BusinessException(e);
//		}
//		return kq;
//	}
	
	@Override
	public Integer deleteOffDocShopMap(List<Shop>listShop, OfficeDocument offDoc, OfficeDocmentVO filterVO) throws BusinessException {
		Integer kq = 1;
		try {
			Integer flag = 1;
			
			for(Shop s:listShop){
				OfficeDocShopMap docShopMap = new OfficeDocShopMap();
				docShopMap = mediaItemDAO.getListOffShopMapToDocIdShopID(offDoc.getId(), s.getId());
				docShopMap.setShopId(s);
				docShopMap.setOfficeDocumentId(offDoc);
				docShopMap.setUpdateDate(filterVO.getCreateDate());
				docShopMap.setUpdateUser(filterVO.getCreateUser());
				docShopMap.setStatus(ActiveType.DELETED);
				flag = mediaItemDAO.updateOffDocShopMap(docShopMap);
				if(flag!=1){
					break;
				}
			}
			kq = flag;
		} catch (Exception e) {
			kq = -2;
			throw new BusinessException(e);
		}
		return kq;
	}
	
	@Override
	public Integer deleteOffDocShopMapALL(List<OfficeDocShopMap>lstoffDoc, OfficeDocmentVO filterVO) throws BusinessException {
		Integer kq = 1;
		try {
			Integer flag = 1;
			for(OfficeDocShopMap ofs:lstoffDoc){
				ofs.setUpdateDate(filterVO.getCreateDate());
				ofs.setUpdateUser(filterVO.getCreateUser());
				ofs.setStatus(ActiveType.DELETED);
				flag = mediaItemDAO.updateOffDocShopMap(ofs);
				if(flag!=1){
					break;
				}
			}
			kq = flag;
		} catch (Exception e) {
			kq = -2;
			LogUtility.logError(e, e.getMessage());
			throw new BusinessException(e);
		}
		return kq;
	}

	@Override
	public List<MediaItemShopVO> getListMediaItemShopVO(
			List<Long> lstCustomerId, Date fromDate, Date toDate,
			String displayProgramCode) throws BusinessException {
		List<MediaItemShopVO> res = new ArrayList<MediaItemShopVO>();
		Map<String, MediaItemShopVO> mapShop = new HashMap<String, MediaItemShopVO>();
		try {
			List<MediaItemDetailVO> lst = mediaItemDAO.getListMediaItemShopVO(lstCustomerId, fromDate, toDate, displayProgramCode);
			if(lst != null && lst.size() >0){
				for (MediaItemDetailVO shopChild : lst) {
					if(mapShop.containsKey(shopChild.getShopCode())){
						MediaItemShopVO shopVO = mapShop.get(shopChild.getShopCode());
						Map<String, MediaItemCustomerVO> mapCustomer = shopVO.getMapCustomer();
						if(mapCustomer.containsKey(shopChild.getCustomerCode())){
							MediaItemCustomerVO customerVO = mapCustomer.get(shopChild.getCustomerCode());
							Map<String, MediaItemProgramVO> mapProgram = customerVO.getMapProgram();
							MediaItemProgramVO programVO = null;
							if(mapProgram.containsKey(shopChild.getDisplayProgramCode())){
								programVO =  mapProgram.get(shopChild.getDisplayProgramCode());
								programVO.getLstMediaItem().add(shopChild);
							}else{
								List<MediaItemDetailVO> lstMediaItem = new ArrayList<MediaItemDetailVO>();
								lstMediaItem.add(shopChild);
								
								programVO = new MediaItemProgramVO();
								programVO.setDisplayProgramCode(shopChild.getDisplayProgramCode());
								programVO.setLstMediaItem(lstMediaItem);
								customerVO.getLstMediaItemProgram().add(programVO);
							}
						}else{
							List<MediaItemDetailVO> lstMediaItem = new ArrayList<MediaItemDetailVO>();
							lstMediaItem.add(shopChild);
							
							MediaItemProgramVO programVO = new MediaItemProgramVO();
							programVO.setDisplayProgramCode(shopChild.getDisplayProgramCode());
							programVO.setLstMediaItem(lstMediaItem);
							List<MediaItemProgramVO> lstMediaItemProgram = new ArrayList<MediaItemProgramVO>();
							lstMediaItemProgram.add(programVO);
							HashMap<String, MediaItemProgramVO> mapProgram = new HashMap<String, MediaItemProgramVO>();
							mapProgram.put(shopChild.getDisplayProgramCode(), programVO);
							MediaItemCustomerVO customerVO = new MediaItemCustomerVO();
							customerVO.setCustomerCode(shopChild.getCustomerCode());
							customerVO.setMapProgram(mapProgram);
							List<MediaItemCustomerVO> lstMediaItemCustomer = shopVO.getLstMediaItemCustomer();
							lstMediaItemCustomer.add(customerVO);
							
							customerVO.setLstMediaItemProgram(lstMediaItemProgram);
							mapCustomer.put(shopChild.getCustomerCode(), customerVO);
						}
						
					}else{
						
						MediaItemShopVO shopVO = new MediaItemShopVO();
						shopVO.setShopCode(shopChild.getShopCode());
						
						List<MediaItemDetailVO> lstMediaItem = new ArrayList<MediaItemDetailVO>();
						lstMediaItem.add(shopChild);
						
						MediaItemProgramVO programVO = new MediaItemProgramVO();
						programVO.setDisplayProgramCode(shopChild.getDisplayProgramCode());
						programVO.setLstMediaItem(lstMediaItem);
						List<MediaItemProgramVO> lstMediaItemProgram = new ArrayList<MediaItemProgramVO>();
						lstMediaItemProgram.add(programVO);
						HashMap<String, MediaItemProgramVO> mapProgram = new HashMap<String, MediaItemProgramVO>();
						mapProgram.put(shopChild.getDisplayProgramCode(), programVO);
						MediaItemCustomerVO customerVO = new MediaItemCustomerVO();
						customerVO.setCustomerCode(shopChild.getCustomerCode());
						customerVO.setMapProgram(mapProgram);
						List<MediaItemCustomerVO> lstMediaItemCustomer = new ArrayList<MediaItemCustomerVO>();
						lstMediaItemCustomer.add(customerVO);
						
						customerVO.setLstMediaItemProgram(lstMediaItemProgram);
						shopVO.setLstMediaItemCustomer(lstMediaItemCustomer);
						HashMap<String, MediaItemCustomerVO> mapCustomer = new HashMap<String, MediaItemCustomerVO>();
						mapCustomer.put(shopChild.getCustomerCode(), customerVO);
						shopVO.setMapCustomer(mapCustomer);
						res.add(shopVO);
						mapShop.put(shopChild.getShopCode(), shopVO);
					}
						
				}
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
		}
		return res;
	}
	
}

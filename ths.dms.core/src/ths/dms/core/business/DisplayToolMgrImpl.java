package ths.dms.core.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.DisplayTool;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.filter.DisplayToolCustomerFilter;
import ths.dms.core.entities.filter.DisplayToolFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.DisplayToolCustomerVO;
import ths.dms.core.entities.vo.DisplayToolVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffSimpleVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.dao.DisplayToolDAO;
import ths.dms.core.dao.DisplayToolsProductDAO;
import ths.dms.core.dao.ProductDAO;

public class DisplayToolMgrImpl implements DisplayToolMgr {
	@Autowired
	DisplayToolDAO displayToolDAO;
	
	@Autowired
	DisplayToolsProductDAO displayToolsProductDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	@Override
	public ObjectVO<DisplayToolVO> getListDisplayTool(DisplayToolFilter filter) throws BusinessException {
		try {
			ObjectVO<DisplayToolVO> vo = new ObjectVO<DisplayToolVO>();
			List<DisplayToolVO> lst = displayToolDAO.getListDisplayTool(filter);
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public ObjectVO<DisplayToolVO> getListDisplayToolOfProduct(DisplayToolFilter filter) throws BusinessException {
		try {
			ObjectVO<DisplayToolVO> vo = new ObjectVO<DisplayToolVO>();
			List<DisplayToolVO> lst = displayToolDAO.getListDisplayToolOfProduct(filter);
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public ObjectVO<Product> getListProductOfDisplayTool( KPaging<Product> kPaging, Long displayToolId) throws BusinessException {
		try {
			List<Product> lst = displayToolDAO.getListProductOfDisplayTool(kPaging,displayToolId);
			ObjectVO<Product> vo = new ObjectVO<Product>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
//	@Transactional(rollbackFor = Exception.class)
//	@Override
//	public void deleteListSaleOrderByListId(List<Long> lstSalesOrderId,
//			String updatedStaffCode) throws BusinessException {
//		for (Long salesOrder : lstSalesOrderId) {
//			try {
//				SaleOrder saleOrder = saleOrderDAO.getSaleOrderById(salesOrder);
//				this.deleteOneSaleOrder(saleOrder, updatedStaffCode);
//			} catch (DataAccessException e) {
//				throw new BusinessException(e.getMessage());
//			}
//		}
//	}
//
//	@Transactional(rollbackFor = Exception.class)
//	@Override
//	public void deleteListSaleOrder(List<SaleOrder> lstSalesOrder,
//			String updatedStaffCode) throws BusinessException {
//		for (SaleOrder saleOrder : lstSalesOrder) {
//			try {
//				this.deleteOneSaleOrder(saleOrder, updatedStaffCode);
//			} catch (DataAccessException e) {
//				throw new BusinessException(e.getMessage());
//			}
//		}
//	}
	@Override
	public ObjectVO<Product> getListProductForProductTool(KPaging<Product> kPaging, String productCode, String productName, String toolCode) throws BusinessException {
		try {
			List<Product> listProduct = productDAO.getListProduct4ProductTool(kPaging, productCode, productName, toolCode);
			ObjectVO<Product> vo = new ObjectVO<Product>();
			vo.setkPaging(kPaging);
			vo.setLstObject(listProduct);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	@Override
	public ObjectVO<Product> getListProductToMTDisplayTeamplate(KPaging<Product> kPaging, String productCode, String productName, Long idMTDisplayTeamplate) throws BusinessException{
		try {
			List<Product> listProduct = productDAO.getListProductToMTDisplayTeamplate(kPaging, productCode, productName, idMTDisplayTeamplate);
			ObjectVO<Product> vo = new ObjectVO<Product>();
			vo.setkPaging(kPaging);
			vo.setLstObject(listProduct);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	@Override
	public List<Product> getProductToIdDisplayTemplateAutocomlete(KPaging<Product> kPaging, String productCode, String productName, Long idMTDisplayTeamplate) throws BusinessException{
		try {
			return productDAO.getListProductToMTDisplayTeamplate(kPaging, productCode, productName, idMTDisplayTeamplate);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	@Override
	public Product checkProductToMTDisplayTeamplateChange(String productCode, Long idMTDisplayTeamplate) throws BusinessException{
		try {
			return productDAO.checkProductToMTDisplayTeamplateChange(productCode, idMTDisplayTeamplate);
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
	}
	
	
	@Override
	public ObjectVO<DisplayToolCustomerVO> getListDisplayToolCustomerVo(KPaging<DisplayToolCustomerVO> kPaging,
			DisplayToolCustomerFilter filter) throws BusinessException {
		// TODO Auto-generated method stub
		try{
			List<DisplayToolCustomerVO> list=displayToolDAO.getListDisplayToolCustomerVo(kPaging, filter);
			ObjectVO<DisplayToolCustomerVO> vo=new ObjectVO<DisplayToolCustomerVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(list);
			return vo;
		}catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}
	}
	@Override
	public List<DisplayTool> checkListDisplayTool(List<DisplayTool> list)
			throws BusinessException {
		// TODO Auto-generated method stub
		try{
			return displayToolDAO.checkListDisplayTool(list);
		}catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}
	}
	@Override
	public void createListDisplayTool(List<DisplayTool> list)
			throws BusinessException {
		// TODO Auto-generated method stub
		try{
			for(int i=0;i<list.size();i++){
				list.get(i).setCreateDate(DateUtility.now());
			}
			displayToolDAO.createListDisplayTool(list);
		}catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}
	}
	@Override
	public void createDisplayToolListProducts(Long id, List<Long> _list)
			throws BusinessException {
		// TODO Auto-generated method stub
		try{
			displayToolsProductDAO.createDisplayToolListProducts(id, _list);
		}catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}
	}
	@Override
	public void deleteDisplayToolListProducts(Long id, List<Long> _list)
			throws BusinessException {
		// TODO Auto-generated method stub
		try{
			displayToolsProductDAO.deleteDisplayToolListProducts(id, _list);
		}catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}
	}
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteDisplayTool(Long id) throws BusinessException {
		// TODO Auto-generated method stub
		try{
			//productDAO.deleteProductByUpdateStatus(id);
			displayToolsProductDAO.deleteDisplayToolListProducts(id,null);
		}catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}
	}
	@Override
	public void deleteListDisplayTools(List<Long> listId)	throws BusinessException {
		try{			
			displayToolDAO.deleteListDisplayTools(listId);
		}catch (Exception e) {
			// TODO: handle exception
			throw new BusinessException(e.getMessage());
		}
		
	}
	@Override
	public Boolean checkDateMonthToDay(Long shopId, Long staffId, String month)
			throws BusinessException {
		try{			
			return displayToolDAO.checkDateMonthToDay(shopId, staffId, month);
		}catch (Exception e) {			
			throw new BusinessException(e.getMessage());
		}		
	}
	@Override
	public ObjectVO<DisplayTool> getListDisplayTool(KPaging<DisplayTool> kPaging,DisplayToolFilter filter) throws BusinessException {
		
		try{
			List<DisplayTool> list=displayToolDAO.getListDisplayTool(kPaging,filter);
			ObjectVO<DisplayTool> vo= new ObjectVO<DisplayTool>();
			vo.setkPaging(kPaging);
			vo.setLstObject(list);
			return vo;
		}catch (Exception e) {			
			throw new BusinessException(e.getMessage());
		}
	}
	/* Sao chep nhieu khach hang muon tu cua nhieu nhan vien */
	@Override
	public List<StaffSimpleVO> getListDisplayToolStaffId(DisplayToolFilter filter) throws BusinessException {
		
		try{
			List<StaffSimpleVO> list=displayToolDAO.getListDisplayToolStaffId(filter);
			return list;
		}catch (Exception e) {			
			throw new BusinessException(e.getMessage());
		}
	}
}

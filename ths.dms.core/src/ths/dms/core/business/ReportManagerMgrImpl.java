package ths.dms.core.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Report;
import ths.dms.core.entities.ReportShopMap;
import ths.dms.core.entities.ReportUrl;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.ReportFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.ReportManagerDAO;

public class ReportManagerMgrImpl implements ReportManagerMgr{
	@Autowired
	private ReportManagerDAO reportManagerDAO;
	
	@Override
	public Report createReport(Report report) throws BusinessException {
		try{
			return reportManagerDAO.createReport(report);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public void updateReport(Report report) throws BusinessException {
		try{
			reportManagerDAO.updateReport(report);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public Report getReportById(Long id) throws BusinessException {
		try{
			return reportManagerDAO.getReportById(id);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public void deleteReport(Report report) throws BusinessException {
		try{
			reportManagerDAO.deleteReport(report);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Report getReportByCode(String code) throws BusinessException {
		try{
			return reportManagerDAO.getReportByCode(code);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ReportUrl createReportUrl(ReportUrl reportUrl)
			throws BusinessException {
		try{
			return reportManagerDAO.createReportUrl(reportUrl);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public ReportUrl getReportUrlById(Long id) throws BusinessException {
		try{
			return reportManagerDAO.getReportUrlById(id);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public void updateReportUrl(ReportUrl reportUrl) throws BusinessException {
		try{
			reportManagerDAO.updateReportUrl(reportUrl);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public void deleteReportUrl(ReportUrl reportUrl) throws BusinessException {
		try{
			reportManagerDAO.deleteReportUrl(reportUrl);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public List<ReportUrl> getListReportUrlByReportId(Long reportId) throws BusinessException {
		try{
			return reportManagerDAO.getListReportUrlByReportId(reportId);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public List<ReportUrl> getListReportUrlByNPP(Long reportId) throws BusinessException {
		try{
			return reportManagerDAO.getListReportUrlByNPP(reportId);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public ReportShopMap createReportShopMap(ReportShopMap reportShopMap)
			throws BusinessException {
		try{
			return reportManagerDAO.createReportShopMap(reportShopMap);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public ReportShopMap getReportShopMapById(Long id) throws BusinessException {
		try{
			return reportManagerDAO.getReportShopMapById(id);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public ReportShopMap getReportShopMapByShopIdReportId(Long shopId, Long reportId) throws BusinessException {
		try{
			return reportManagerDAO.getReportShopMapByShopIdReportId(shopId, reportId);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public void updateReportShopMap(ReportShopMap reportShopMap)
			throws BusinessException {
		try{
			reportManagerDAO.updateReportShopMap(reportShopMap);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public void deleteReportShopMap(ReportShopMap reportShopMap)
			throws BusinessException {
		try{
			reportManagerDAO.deleteReportShopMap(reportShopMap);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	@Override
	public List<ReportShopMap> getListReportShopMapByReportId(Long reportId)
			throws BusinessException {
		try{
			return reportManagerDAO.getListReportShopMapByReportId(reportId);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<Report> getListReport(KPaging<Report> kPaging, ReportFilter filter) throws BusinessException {
		try {
			List<Report> list = reportManagerDAO.getListReport(kPaging, filter);
			
			ObjectVO<Report> vo = new ObjectVO<Report>();
			vo.setLstObject(list);
			vo.setkPaging(kPaging);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public ObjectVO<ReportUrl> getListReportUrl(KPaging<ReportUrl> kPaging, ReportFilter filter) throws BusinessException {
		try {
			List<ReportUrl> list = reportManagerDAO.getListReportUrl(kPaging, filter);
			
			ObjectVO<ReportUrl> vo = new ObjectVO<ReportUrl>();
			vo.setLstObject(list);
			vo.setkPaging(kPaging);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	// end vuongmq
	@Override
	public ReportUrl getReportFileName(Long shopId, String maBC) throws BusinessException{
		try {
			return reportManagerDAO.getReportFileName(shopId, maBC);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public ReportUrl getDefaultReportUrl(ReportFilter filter) throws BusinessException {
		try {
			return reportManagerDAO.getDefaultReportUrl(filter);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
}

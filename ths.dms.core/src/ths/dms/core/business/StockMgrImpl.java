package ths.dms.core.business;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockLock;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.StockTransLot;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTotalVOFilter;
import ths.dms.core.entities.enumtype.StockTransLotOwnerType;
import ths.dms.core.entities.enumtype.StockTransType;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ProductAmountVO;
import ths.dms.core.entities.vo.ProductLotVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransDetailVO;
import ths.dms.core.entities.vo.StockTransLotVO;
import ths.dms.core.entities.vo.StockTransVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_Stock;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.LogUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.ApParamDAO;
import ths.dms.core.dao.PriceDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.ProductLotDAO;
import ths.dms.core.dao.SaleOrderDAO;
import ths.dms.core.dao.SaleOrderLotDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.StockLockDAO;
import ths.dms.core.dao.StockTotalDAO;
import ths.dms.core.dao.StockTransDAO;
import ths.dms.core.dao.StockTransDetailDAO;
import ths.dms.core.dao.StockTransLotDAO;
import ths.dms.core.dao.WareHouseDAO;

public class StockMgrImpl implements StockMgr {

	@Autowired
	StockTransDAO stockTransDAO;
	
	@Autowired
	StockTransDetailDAO stockTransDetailDAO;
	
	@Autowired
	StockTotalDAO stockTotalDAO;
	
	@Autowired
	ProductLotDAO productLotDAO;
	
	@Autowired
	StockTransLotDAO stockTransLotDAO;
	
	@Autowired
	PriceDAO priceDAO;
	
	@Autowired
	SaleOrderDAO saleOrderDAO;
	
	@Autowired
	private ApParamDAO apParamDAO;
	
	@Autowired
	private CommonMgr commonMgr;
	
	@Autowired
	private SaleOrderLotDAO saleOrderLotDAO;
	
	@Autowired
	private StockLockDAO stockLockDAO;
	
	@Autowired
	private WareHouseDAO wareHouseDAO;
	
	@Autowired
	private ShopDAO shopDAO;
	
	@Override
	public StockTrans createStockTrans(StockTrans stockTrans) throws BusinessException {
		try {
			return stockTransDAO.createStockTrans(stockTrans);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateStockTrans(StockTrans stockTrans) throws BusinessException {
		try {
			stockTransDAO.updateStockTrans(stockTrans);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteStockTrans(StockTrans stockTrans) throws BusinessException{
		try {
			stockTransDAO.deleteStockTrans(stockTrans);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StockTrans getStockTransById(Long id) throws BusinessException{
		try {
			return id==null?null:stockTransDAO.getStockTransById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StockTrans getStockTransByCode(String code) throws BusinessException {
		try {
			return stockTransDAO.getStockTransByCode(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StockTrans getStockTransByCodeAndShop(String code, Long shopId) throws BusinessException {
		try {
			return stockTransDAO.getStockTransByCodeAndShop(code, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<StockTrans> getListStockTrans(KPaging<StockTrans> kPaging,
			Date fromDate, Date toDate, Long shopId,
			StockTransType stockTransType) throws BusinessException {
		try {
			List<StockTrans> lst = stockTransDAO.getListStockTrans(kPaging, fromDate, toDate, shopId, stockTransType);
			ObjectVO<StockTrans> vo = new ObjectVO<StockTrans>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		
	}
	
	@Override
	public List<StockTrans> getListStockTransById(List<Long> listId) throws BusinessException {
		try {
			List<StockTrans> lst = new ArrayList<StockTrans>();
			for(Long id : listId) {
				lst.add(stockTransDAO.getStockTransById(id));
			}
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<StockTrans> getListStockTransByFromId(Long fromId) throws BusinessException {
		try {
			List<StockTrans> lst = new ArrayList<StockTrans>();
			return stockTransDAO.getListStockTransByFromId(fromId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override 
	public String generateStockTransCode(Long shopId) throws BusinessException {
		try {
			return stockTransDAO.generateStockTransCode(shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	private void splitSaleLot(List<ProductLot> lstProductLot, StockTransDetail stockTransDetail) throws DataAccessException {
		StockTrans st = stockTransDetail.getStockTrans();
		int quantity = stockTransDetail.getQuantity();
		Boolean flag = false;
		
		Integer quant = 0;
		Integer quantTemp = 0;
		for (ProductLot lot : lstProductLot) {
			StockTransLot stockTransLot = new StockTransLot();
			stockTransLot.setCreateUser(stockTransDetail.getCreateUser());
			stockTransLot.setStockTransDate(st.getStockTransDate());
			stockTransLot.setProduct(lot.getProduct());
			stockTransLot.setLot(lot.getLot());
			stockTransLot.setStockTransDetail(stockTransDetail);
			stockTransLot.setStockTrans(st);

			if (quantity <= lot.getAvailableQuantity()) {
				quant = quantity;
				quantTemp = quantity;;
				lot.setQuantity(lot.getQuantity() - quantity);
				lot.setAvailableQuantity(lot.getAvailableQuantity() - quantity);
				stockTransLot.setQuantity(quantity);
				stockTransLotDAO.createStockTransLot(stockTransLot);
				productLotDAO.updateProductLot(lot);
				flag = true;
			} else {
				/**
				 * @Description Cap nhat lai cach tinh moi
				 * @author hunglm16
				 * @since 10/10/2013
				 * */
				if(quantity>=lot.getQuantity()){
					quant = lot.getAvailableQuantity();
					lot.setAvailableQuantity(quant - lot.getQuantity());
					quantity = quantity - lot.getQuantity();
					quantTemp = lot.getQuantity();
					stockTransLot.setQuantity(lot.getQuantity());
					lot.setQuantity(0);
				}else{
					/**
					 * @Description Cap nhat lai cach tinh moi
					 * @author hunglm16
					 * @since 10/10/2013
					 * */
					quant = lot.getAvailableQuantity();
					lot.setAvailableQuantity(quant - quantity);
					lot.setQuantity((lot.getQuantity()- quantity));
					stockTransLot.setQuantity(quantity);
					quantTemp = quantity;
					flag = true;
				}
				stockTransLotDAO.createStockTransLot(stockTransLot);
				productLotDAO.updateProductLot(lot);
			}

			// cong them vao lot cua toOwner
			/*
			 * comment code loi compile
			 * @modified by tuannd20
			 * @date 20/12/2014
			 */
			/*ProductLot toLot = productLotDAO.getProductLotByCodeAndOwnerAndProductForUpdate(
							lot.getLot(), st.getToOwnerId(), st.getToOwnerType(),
							stockTransDetail.getProduct().getId());
			if (toLot == null) {
				toLot = new ProductLot();
				toLot.setExpiryDate(lot.getExpiryDate());
				toLot.setLot(lot.getLot());
				toLot.setObjectId(st.getToOwnerId());
				toLot.setObjectType(st.getToOwnerType());
				toLot.setProduct(lot.getProduct());
				toLot.setQuantity(quantTemp);
				toLot.setAvailableQuantity(quantTemp);
				toLot = productLotDAO.createProductLot(toLot);
			} else {
				toLot.setQuantity(toLot.getQuantity() + quantTemp);
				toLot.setAvailableQuantity(toLot.getAvailableQuantity() + quantTemp);
				productLotDAO.updateProductLot(toLot);
			}*/

			if (flag)
				break;
		}
	}
	
	private StockTransDetail createOneStockTransDetail(StockTransDetailVO stockTransDetailVO, Integer fifo,Long shopId) throws BusinessException {
		try {
			StockTransDetail stockTransDetail = stockTransDetailVO.getStockTransDetail();
			
			StockTransDetail stdInDb = stockTransDetailDAO.getStockTransDetailForUpdate(stockTransDetail.getStockTrans().getId(),
																			stockTransDetail.getProduct().getId());
			int quantity = stockTransDetail.getQuantity();
			
			if (stdInDb != null) {
				stdInDb.setQuantity(stdInDb.getQuantity() + quantity);
				stockTransDetailDAO.updateStockTransDetail(stdInDb);
				stockTransDetail = stdInDb;
			}
			else {
				stockTransDetail.setPrice(priceDAO.getPriceByProductId(shopId,stockTransDetail.getProduct().getId()).getPrice());
				stockTransDetail = stockTransDetailDAO.createStockTransDetail(stockTransDetail);	
			}
			
			stockTransDetailVO.setStockTransDetail(stockTransDetail);
			
			StockTrans st = stockTransDetail.getStockTrans();
			
			/*
			 * comment code loi compile
			 * @modified by tuannd20
			 * @date 20/12/2014
			 */
			/*if (st.getToOwnerId() != null) {
				StockTotal toStockTotal = stockTotalDAO.
						getStockTotalByProductAndOwnerForUpdate(stockTransDetail.getProduct().getId(), 
								st.getToOwnerType(), st.getToOwnerId(),null);
				if (toStockTotal == null) {
					toStockTotal = new StockTotal();
					toStockTotal.setProduct(stockTransDetail.getProduct());
					toStockTotal.setObjectId(st.getToOwnerId());
					toStockTotal.setObjectType(st.getToOwnerType());
					toStockTotal.setQuantity(quantity);
					toStockTotal.setAvailableQuantity(quantity);
					toStockTotal.setCreateUser(stockTransDetail.getCreateUser());
					toStockTotal = stockTotalDAO.createStockTotal(toStockTotal);
				}
				else {
					toStockTotal.setQuantity(toStockTotal.getQuantity() + quantity);
					toStockTotal.setAvailableQuantity(toStockTotal.getAvailableQuantity() + quantity);
					stockTotalDAO.updateStockTotal(toStockTotal);
				}
			}*/
			
			//giam so luong trong stock_total - from_owner_id - from_owner_type
			/*if (st.getFromOwnerId() != null) {
				if (st.getFromOwnerType().equals(StockObjectType.SHOP)) {
//					StockTotal fromStockTotal = stockTotalDAO.
//							getStockTotalByProductAndOwnerForUpdate(stockTransDetail.getProduct().getId(), 
//									st.getFromOwnerType(), st.getFromOwnerId(),null);
					
//					if (fromStockTotal.getQuantity() < quantity)
//						throw new DataAccessException("stock total has not enough quantity");
//					fromStockTotal.setQuantity(fromStockTotal.getQuantity() - quantity);
//					fromStockTotal.setAvailableQuantity(fromStockTotal.getAvailableQuantity() -quantity);
//					stockTotalDAO.updateStockTotal(fromStockTotal);
					
					if (st.getToOwnerId() != null) {
						if (stockTransDetail.getProduct().getCheckLot() == 1) {
							// xuat kho vansale - lay lot bang tay
							if (stockTransDetailVO.getListHandleLot() != null && stockTransDetailVO.getListHandleLot().size() > 0) {
								
								for (int i = 0, lotSize = stockTransDetailVO.getListHandleLot().size(); i < lotSize; i++) {
									
									String stringLot = stockTransDetailVO.getListHandleLot().get(i);
									Integer quantityLot = stockTransDetailVO.getListHandleQuantity().get(i);
									ProductLot lot = productLotDAO.getProductLotByCodeAndOwnerAndProductForUpdate(
													stringLot, st.getFromOwnerId(), st.getFromOwnerType(),
													stockTransDetail.getProduct().getId());
									if (lot != null) {
										StockTransLot stockTransLot = new StockTransLot();
										stockTransLot.setCreateUser(stockTransDetail.getCreateUser());
										stockTransLot.setStockTransDate(st.getStockTransDate());
										stockTransLot.setProduct(lot.getProduct());
										stockTransLot.setLot(lot.getLot());
										stockTransLot.setStockTransDetail(stockTransDetail);
										stockTransLot.setStockTrans(st);

										lot.setQuantity(lot.getQuantity() - quantityLot);
										lot.setAvailableQuantity(lot.getAvailableQuantity() - quantityLot);
										stockTransLot.setQuantity(quantityLot);
										stockTransLotDAO.createStockTransLot(stockTransLot);
										productLotDAO.updateProductLot(lot);
									} else {
										//throw lot is not exits
										throw new DataAccessException("lot is not exits");
									}

									// cong them vao lot cua toOwner
									ProductLot toLot = productLotDAO.getProductLotByCodeAndOwnerAndProductForUpdate(
													lot.getLot(), st.getToOwnerId(), st.getToOwnerType(),
													stockTransDetail.getProduct().getId());
									if (toLot == null) {
										toLot = new ProductLot();
										toLot.setExpiryDate(lot.getExpiryDate());
										toLot.setLot(lot.getLot());
										toLot.setObjectId(st.getToOwnerId());
										toLot.setObjectType(st.getToOwnerType());
										toLot.setProduct(lot.getProduct());
										toLot.setQuantity(quantityLot);
										toLot.setAvailableQuantity(quantityLot);
										toLot = productLotDAO.createProductLot(toLot);
									} else {
										toLot.setQuantity(toLot.getQuantity() + quantityLot);
										toLot.setAvailableQuantity(toLot.getAvailableQuantity() + quantityLot);
										productLotDAO.updateProductLot(toLot);
									}
								}
							} else {
								// xuat kho vansale - lay lot tu dong
								List<ProductLot> lstProductLot = productLotDAO.getProductLotByProductAndOwnerForUpdate(
										stockTransDetail.getProduct().getId(), st.getFromOwnerId(), st.getFromOwnerType(), fifo);
								this.splitSaleLot(lstProductLot, stockTransDetail);
							}
						}
					}
					 else {
						 //xuat kho dieu chinh - lot nguoi dung nhap vao (neu check_lot = 1 moi xet)
						 if (stockTransDetail.getProduct().getCheckLot() == 1) {
							 ProductLot lot = productLotDAO.getProductLotByCodeAndOwnerAndProductForUpdate(
									 stockTransDetailVO.getLot(), 
									 st.getFromOwnerId(), 
									 st.getFromOwnerType(), 
									 stockTransDetail.getProduct().getId());
							 if (lot.getQuantity() < quantity)
								 throw new DataAccessException("lot has not enough quantity");
							 
							 lot.setQuantity(lot.getQuantity() - quantity);
							 lot.setAvailableQuantity(lot.getAvailableQuantity() - quantity);
							 productLotDAO.updateProductLot(lot);
							 StockTransLot stockTransLot = new StockTransLot();
							 stockTransLot.setCreateUser(stockTransDetail.getCreateUser());
							 stockTransLot.setStockTransDate(st.getStockTransDate());
							 stockTransLot.setProduct(lot.getProduct());
							 stockTransLot.setLot(lot.getLot());
							 stockTransLot.setStockTransDetail(stockTransDetail);
							 stockTransLot.setStockTrans(st);
							 stockTransLot.setQuantity(quantity);
							 stockTransLotDAO.createStockTransLot(stockTransLot);
						 }
					 }
				}
				else if (st.getFromOwnerType() == StockObjectType.STAFF) {
					List<StockTotal> fromStockTotal = stockTotalDAO.
							getListStockTotalByProductAndOwnerForUpdate(stockTransDetail.getProduct().getId(), 
									st.getFromOwnerType(), st.getFromOwnerId());
					Integer totalQuantity = 0;
					if(fromStockTotal != null){
						for(StockTotal stockTotal: fromStockTotal){
							totalQuantity += stockTotal.getQuantity();
						}
					}
					if (totalQuantity < quantity)
						throw new DataAccessException("stock total has not enough quantity");
					if(fromStockTotal != null){
						for(StockTotal stockTotal: fromStockTotal){
							stockTotal.setQuantity(0);
							stockTotal.setAvailableQuantity(0);
							stockTotalDAO.updateStockTotal(stockTotal);
						}
					}
					if (stockTransDetail.getProduct().getCheckLot() == 1) {
						ProductLot lot = productLotDAO.getProductLotByCodeAndOwnerAndProductForUpdate(
								stockTransDetailVO.getLot(), st.getFromOwnerId(), st.getFromOwnerType(), 
								stockTransDetail.getProduct().getId());
						if (lot.getQuantity() < quantity)
							 throw new DataAccessException("product lot has not enough quantity");
						 
						lot.setQuantity(lot.getQuantity() - quantity);
						lot.setAvailableQuantity(lot.getAvailableQuantity() - quantity);
						productLotDAO.updateProductLot(lot);
						
						ProductLot toLot = productLotDAO.getProductLotByCodeAndOwnerAndProductForUpdate(
								stockTransDetailVO.getLot(), 
								st.getToOwnerId(), 
								st.getToOwnerType(), 
								stockTransDetail.getProduct().getId());
						toLot.setQuantity(toLot.getQuantity() + quantity);
						toLot.setAvailableQuantity(toLot.getAvailableQuantity() + quantity);
						productLotDAO.updateProductLot(toLot);
						
						StockTransLot stockTransLot = new StockTransLot();
						stockTransLot.setCreateUser(stockTransDetail.getCreateUser());
						stockTransLot.setStockTransDate(st.getStockTransDate());
						stockTransLot.setProduct(lot.getProduct());
						stockTransLot.setLot(lot.getLot());
						stockTransLot.setStockTransDetail(stockTransDetail);
						stockTransLot.setStockTrans(st);
						stockTransLot.setQuantity(quantity);
						stockTransLotDAO.createStockTransLot(stockTransLot);
					}
				}
			}*/
			//Cong so luong trong stock_trans_lot - to_owner_id - to_owner_type
			/*if (st.getToOwnerId() != null && st.getFromOwnerId() == null) {
				//nhap kho dieu chinh
				if (stockTransDetail.getProduct().getCheckLot() == 1) {
					ProductLot lot = productLotDAO.getProductLotByCodeAndOwnerAndProductForUpdate(
							stockTransDetailVO.getLot(), st.getToOwnerId(), st.getToOwnerType(), stockTransDetail.getProduct().getId());
					if (lot == null) {
						lot = new ProductLot();
						lot.setLot(stockTransDetailVO.getLot());
						lot.setObjectId(st.getToOwnerId());
						lot.setObjectType(st.getToOwnerType());
						lot.setProduct(stockTransDetail.getProduct());
						
						Date expiryDate = this.moveDate(this.converLotToDate(stockTransDetailVO.getLot(), 
								"dd/MM/yyyy"), stockTransDetail.getProduct().getExpiryDate(), 
								stockTransDetail.getProduct().getExpiryType());
						lot.setExpiryDate(expiryDate);
						lot = productLotDAO.createProductLot(lot);
					}
					lot.setQuantity(lot.getQuantity() + quantity);
					lot.setAvailableQuantity(lot.getAvailableQuantity() + quantity);
					productLotDAO.updateProductLot(lot);
					StockTransLot stockTransLot = new StockTransLot();
					stockTransLot.setCreateUser(stockTransDetail.getCreateUser());
					stockTransLot.setStockTransDate(st.getStockTransDate());
					stockTransLot.setProduct(lot.getProduct());
					stockTransLot.setLot(lot.getLot());
					stockTransLot.setStockTransDetail(stockTransDetail);
					stockTransLot.setStockTrans(st);
					stockTransLot.setQuantity(quantity);
					stockTransLotDAO.createStockTransLot(stockTransLot);
				}
			}*/
			
			return stockTransDetail;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	private Date moveDate(Date date,Integer val,Integer type){ //type = 1 Date,2: Month
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if(type==1){
			cal.add(Calendar.DATE, val);
		}else if(type==2){
			cal.add(Calendar.MONTH, val);
		}		
		Date d = cal.getTime();
		return d;
	}
	
	private Date converLotToDate(String lot,String format) throws BusinessException{
		String year = null;
		DateFormat f = new SimpleDateFormat("yyyy");
		year = f.format(new Date());
		Integer nowYear = Integer.parseInt(year);
		
		String dd = lot.substring(0,2);
		String MM = lot.substring(2,4);
		String yy = lot.substring(4);
		DateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.parse(dd +"/"+MM +"/" + String.valueOf(nowYear).substring(0,2)+yy);
		} catch(Exception ex) {
			throw new BusinessException("Parsing error");
		}
	}	
	
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public Long createListStockTransDetail(StockTrans stockTrans, List<StockTransDetailVO> lstStockTransDetailVO, Integer fifo,Long shopId) throws BusinessException {
		StockTransDetail std = null;
		Long codeStock = Long.valueOf(0);
		if (lstStockTransDetailVO.size() == 0){
			return codeStock;
		}
		//StockTrans stockTrans = lstStockTransDetailVO.get(0).getStockTransDetail().getStockTrans();
		stockTrans = this.createStockTrans(stockTrans);
		codeStock = stockTrans.getId();
		BigDecimal totalWeight = new BigDecimal(0);
		BigDecimal totalAmount = new BigDecimal(0);
		Integer quant = 0;
		List<Long> lstProductId = new ArrayList<Long>();
		
		for (StockTransDetailVO vo: lstStockTransDetailVO) {
			
			if (!lstProductId.contains(vo.getStockTransDetail().getProduct().getId()))
				lstProductId.add(vo.getStockTransDetail().getProduct().getId());
			vo.getStockTransDetail().setStockTrans(stockTrans);
			quant = vo.getStockTransDetail().getQuantity();
			if (vo.getStockTransDetail().getQuantity() != 0) {
				std = this.createOneStockTransDetail(vo, fifo,shopId);
				if (std == null) {
					throw new RuntimeException();
				}
				totalWeight = totalWeight.add(std.getProduct().getGrossWeight().multiply(new BigDecimal(quant)));
				totalAmount = totalAmount.add(std.getPrice().multiply(BigDecimal.valueOf(quant)));
			}
		}
		
		try {
			//tim kiem don hang vansale va phan bo					
			/*
			 * comment code loi compile
			 * @modified by tuannd20
			 * @date 20/12/2014
			 */
			/*for (Long productId: lstProductId) {
				if (stockTrans.getFromOwnerId() != null && stockTrans.getToOwnerId() != null &&
						StockObjectType.STAFF.equals(stockTrans.getFromOwnerType())) {
					List<SaleOrderDetail> lstSod = saleOrderDAO.getListVansaleSaleOrderDetail(
							Long.valueOf(stockTrans.getFromOwnerId()), productId);
					List<ProductLot> lstProductLot = productLotDAO.getListProductLotForUpdate(
							null, Long.valueOf(stockTrans.getFromOwnerId()), StockObjectType.STAFF, 0, 
							ActiveType.RUNNING, productId, null);
					
					for (SaleOrderDetail sod: lstSod) {
						int quant123 = sod.getQuantity();
						for (ProductLot pl: lstProductLot) {
							if (pl.getQuantity() > 0 && quant123 > 0) {
								SaleOrderLot sol = new SaleOrderLot();
								sol.setSaleOrder(sod.getSaleOrder());
								sol.setSaleOrderDetail(sod);
								sol.setProduct(sod.getProduct());
								sol.setPrice(sod.getPriceValue());
								sol.setShop(sod.getShop());
								sol.setStaff(sod.getStaff());
								sol.setLot(pl.getLot());
								sol.setOrderDate(sod.getSaleOrder().getOrderDate());
								sol.setCreateUser(stockTrans.getCreateUser());
								
								Date expiryDate = this.moveDate(this.converLotToDate(pl.getLot(), 
										"dd/MM/yyyy"), pl.getProduct().getExpiryDate(), 
										pl.getProduct().getExpiryType());
								
								sol.setExpirationDate(expiryDate);
								
								if (quant123 <= pl.getQuantity()) {
									pl.setQuantity(pl.getQuantity() - quant123);
									pl.setAvailableQuantity(pl.getAvailableQuantity() - quant123);
									sol.setQuantity(quant123);
									quant123 = 0;
									saleOrderLotDAO.createSaleOrderLot(sol);
									break;
								}
								else {//quant > pl.getQuantity()
									sol.setQuantity(pl.getQuantity());
									quant123 = quant123 - pl.getQuantity();
									pl.setQuantity(0);
									pl.setAvailableQuantity(0);
									saleOrderLotDAO.createSaleOrderLot(sol);
								}
							}
						}
					}
					
					for (ProductLot pl: lstProductLot) {
						productLotDAO.updateProductLot(pl);
					} 
				}
			}*/
			//end
			
			stockTrans.setTotalWeight(totalWeight);
			stockTrans.setTotalAmount(totalAmount);
			stockTransDAO.updateStockTrans(stockTrans);
		}
		catch (DataAccessException ex) {
			codeStock = Long.valueOf(0);
			throw new BusinessException(ex);
		}
		return codeStock;
	}
	
	@Transactional
	@Override
	public StockTransDetail createStockTransDetail(StockTransDetailVO stockTransDetailVO, Integer fifo,Long shopId) throws BusinessException {
		return this.createOneStockTransDetail(stockTransDetailVO, fifo,shopId);
	}
	
	@Override
	public void deleteStockTransDetail(StockTransDetail stockTransDetail) throws BusinessException{
		try {
			stockTransDetailDAO.deleteStockTransDetail(stockTransDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StockTransDetail getStockTransDetailById(Long id) throws BusinessException{
		try {
			return id==null?null:stockTransDetailDAO.getStockTransDetailById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StockTotal createStockTotal(StockTotal stockTotal) throws BusinessException {
		try {
			return stockTotalDAO.createStockTotal(stockTotal);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateStockTotal(StockTotal stockTotal) throws BusinessException {
		try {
			stockTotalDAO.updateStockTotal(stockTotal);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateStockLock(StockLock stockLock) throws BusinessException {
		try {
			stockTotalDAO.updateStockLock(stockLock);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void insertStockLock(StockLock stockLock) throws BusinessException {
		try {
			stockTotalDAO.insertStockLock(stockLock);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteStockTotal(StockTotal stockTotal) throws BusinessException{
		try {
			stockTotalDAO.deleteStockTotal(stockTotal);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<StockTotal> getListStockTotal(KPaging<StockTotal> kPaging,
			String productCode, String productName,
			String promotionProgramCode, Long ownerId,
			StockObjectType ownerType, Long catId, Long subCatId,
			Integer fromQuantity, Integer toQuantity, Long cycleCountId) throws BusinessException {
		try {
			List<StockTotal> lst = stockTotalDAO.getListStockTotal(kPaging, productCode, productName, promotionProgramCode, ownerId, ownerType, catId, subCatId, fromQuantity, toQuantity, cycleCountId);
			ObjectVO<StockTotal> vo = new ObjectVO<StockTotal>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<StockTotal> getListStockTotalNew(KPaging<StockTotal> kPaging, StockTotalVOFilter stockTotalVoFilter) throws BusinessException {
		try {
			List<StockTotal> lst = stockTotalDAO.getListStockTotalNew(kPaging, stockTotalVoFilter);
			ObjectVO<StockTotal> vo = new ObjectVO<StockTotal>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<StockTotalVO> getListStockTotalVONew(KPaging<StockTotalVO> kPaging, StockTotalVOFilter stockTotalVoFilter) throws BusinessException {
		try {
			List<StockTotalVO> lst = stockTotalDAO.getListStockTotalVONew(kPaging, stockTotalVoFilter);
			ObjectVO<StockTotalVO> vo = new ObjectVO<StockTotalVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<StockTotal> getListStockTotalNewQuantity(KPaging<StockTotal> kPaging, StockTotalVOFilter stockTotalVoFilter) throws BusinessException {
		try {
			List<StockTotal> lst = stockTotalDAO.getListStockTotalNew(kPaging, stockTotalVoFilter);
			ObjectVO<StockTotal> vo = new ObjectVO<StockTotal>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<StockTotalVO> getListStockTotalVO(StockTotalFilter filter) throws BusinessException {
		try {
			List<StockTotalVO> lst = stockTotalDAO.getListStockTotalVO(filter);
			ObjectVO<StockTotalVO> vo = new ObjectVO<StockTotalVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<StockTotalVO> getListStockTotalVOForImExport(KPaging<StockTotalVO> kPaging,
			String productCode, String productName,
			String promotionProgramCode, Long ownerId,
			StockObjectType ownerType, Long catId, Long subCatId,
			Integer fromQuantity, Integer toQuantity, List<Long> exceptProductId) throws BusinessException {
		try {
			List<StockTotalVO> lst = stockTotalDAO.getListStockTotalVOForImExport(kPaging, productCode, 
					productName, promotionProgramCode, ownerId, ownerType, 
					catId, subCatId, fromQuantity, toQuantity, exceptProductId);
			ObjectVO<StockTotalVO> vo = new ObjectVO<StockTotalVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public List<Boolean> checkIfProductHasEnough(List<ProductAmountVO> lstProductAmountVO)
			throws BusinessException{
		try {
			return stockTotalDAO.checkIfProductHasEnough(lstProductAmountVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public StockTransLot createStockTransLot(StockTransLot stockTransLot) throws BusinessException {
		try {
			return stockTransLotDAO.createStockTransLot(stockTransLot);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateStockTransLot(StockTransLot stockTransLot) throws BusinessException {
		try {
			stockTransLotDAO.updateStockTransLot(stockTransLot);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteStockTransLot(StockTransLot stockTransLot) throws BusinessException{
		try {
			stockTransLotDAO.deleteStockTransLot(stockTransLot);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StockTransLot getStockTransLotById(Long id) throws BusinessException{
		try {
			return id==null?null:stockTransLotDAO.getStockTransLotById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<StockTransLot> getListStockTransLotByStockTransId(KPaging<StockTransLot> kPaging, long stockTransId) 
			throws BusinessException {
		try {
			List<StockTransLot> lst = stockTransLotDAO.getListStockTransLotByStockTransId(kPaging, stockTransId);
			ObjectVO<StockTransLot> vo = new ObjectVO<StockTransLot>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ProductLot createProductLot(ProductLot productLot) throws BusinessException {
		try {
			return productLotDAO.createProductLot(productLot);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateProductLot(ProductLot productLot) throws BusinessException {
		try {
			productLotDAO.updateProductLot(productLot);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteProductLot(ProductLot productLot) throws BusinessException{
		try {
			productLotDAO.deleteProductLot(productLot);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ProductLot getProductLotById(Long id) throws BusinessException{
		try {
			return id==null?null:productLotDAO.getProductLotById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ProductLot getProductLotByCodeAndOwnerAndProduct(String lot, long ownerId,
			StockObjectType ownerType, long productId) throws BusinessException {
		try {
			return productLotDAO.getProductLotByCodeAndOwnerAndProduct(lot, ownerId, ownerType, productId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ProductLot> getListProductLot(KPaging<ProductLot> kPaging,
			Long ownerId,StockObjectType ownerType, Integer quantity, ActiveType status) throws BusinessException {
		try {
			List<ProductLot> lst = productLotDAO.getListProductLot(kPaging, ownerId, ownerType, quantity, status, null, null);
			ObjectVO<ProductLot> vo = new ObjectVO<ProductLot>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public ObjectVO<ProductLotVO> getListProductLotVO(KPaging<ProductLotVO> kPaging,
			Long ownerId,StockObjectType ownerType, Integer quantity, ActiveType status) throws BusinessException {
		try {
			List<ProductLotVO> lst = productLotDAO.getListProductLotVO(kPaging, ownerId, ownerType, quantity, status);
			ObjectVO<ProductLotVO> vo = new ObjectVO<ProductLotVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ProductLotVO> getListProductInStock(KPaging<ProductLotVO> kPaging, Long shopId, StockObjectType shopType,
			Long ownerId,StockObjectType ownerType, Integer quantity, ActiveType status, Integer fifo) throws BusinessException {
		try {
			List<ProductLotVO> lst;
			if (null != fifo && fifo != 0) {
				lst = stockTotalDAO.getListProductInStockTotal(shopId,ownerId, ownerType, 0);
			} else {
				lst = productLotDAO.getListProductInStock(kPaging, shopId, ownerId, ownerType, quantity, status);
			}
			if (null != fifo && fifo != 0 && lst.size() > 0) {
				Integer lotQuantity = 0;
				Integer productId = 0;
				ProductLotVO temp = null;
				List<ProductLotVO> newList = new ArrayList<ProductLotVO>();
				for (ProductLotVO lotVO: lst) {
					if (lotQuantity > 0) {
						// lay ra toan bo lot tu xa -> gan nhat va cap nhat lai
						List<ProductLotVO> listVS = productLotDAO.getListProductLotForInstock(shopId,ownerId, ownerType, 0, productId);
						for (ProductLotVO pVO: listVS) {
							if (lotQuantity > pVO.getQuantity()) {
								if (temp != null) {
									ProductLotVO pl = temp.clone();
									pl.setQuantity(pVO.getQuantity());
									pl.setAvailableQuantity(pVO.getAvailableQuantity());
									pl.setLot(pVO.getLot());
									newList.add(pl);
									lotQuantity = lotQuantity - pVO.getQuantity();
								}
							} else {
								if (temp != null) {
									ProductLotVO pl = temp.clone();
									pl.setQuantity(lotQuantity);
									pl.setAvailableQuantity(lotQuantity);
									pl.setLot(pVO.getLot());
									newList.add(pl);
								}
								lotQuantity = 0;
								break;
							}
						}
					}
					if (lotVO.getCheckLot() == null || lotVO.getCheckLot().intValue() == 0) {
						newList.add(lotVO);
					} else {
						lotQuantity = 0;
						temp = lotVO;
						productId = lotVO.getProductId();
						lotQuantity += lotVO.getQuantity();
					}
				}
				//add if lotquantiy > 0
				if (lotQuantity > 0) {
					List<ProductLotVO> listVS = productLotDAO.getListProductLotForInstock(shopId,ownerId, ownerType, 0, productId);
					for (ProductLotVO pVO: listVS) {
						if (lotQuantity > pVO.getQuantity()) {
							if (temp != null) {
								ProductLotVO pl = temp.clone();
								pl.setQuantity(pVO.getQuantity());
								pl.setAvailableQuantity(pVO.getAvailableQuantity());
								pl.setLot(pVO.getLot());
								newList.add(pl);
								lotQuantity = lotQuantity - pVO.getQuantity();
							}
						} else {
							if (temp != null) {
								ProductLotVO pl = temp.clone();
								pl.setQuantity(lotQuantity);
								pl.setAvailableQuantity(lotQuantity);
								pl.setLot(pVO.getLot());
								newList.add(pl);
							}
							lotQuantity = 0;
							break;
						}
					}
				}
				lst = newList;
			}
			ObjectVO<ProductLotVO> vo = new ObjectVO<ProductLotVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<StockTransVO> getListStockTransVO(KPaging<StockTransVO> kPaging,
				Date fromDate, Date toDate, Long shopId,
				StockTransType stockTransType) throws BusinessException {
		try {
			List<StockTransVO> lst = stockTransDAO.getListStockTransVO(kPaging, fromDate, toDate, shopId, stockTransType);
			ObjectVO<StockTransVO> vo = new ObjectVO<StockTransVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Autowired 
	ProductDAO productDAO;
	
	@Transactional()
	@Override
	public void test() throws BusinessException {
		try {
			Product p = productDAO.getProductByCode("SP1");
			System.out.println(p.getProductName());
			Product pp = productDAO.getProductByCode("SP1");
			System.out.println(pp.getProductName());
			
		}
		catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
		}
	}
	
	@Override
	public ObjectVO<StockTransLotVO> getListStockTransLotVO(KPaging<StockTransLotVO> kPaging, long stockTransId)
		throws BusinessException {
		try {
			List<StockTransLotVO> lst=stockTransLotDAO.getListStockTransLotVO(kPaging, stockTransId);
			ObjectVO<StockTransLotVO> vo = new ObjectVO<StockTransLotVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		}
		catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<StockTransLotVO> getListStockTransLotVOForSearchSale(KPaging<StockTransLotVO> kPaging, long stockTransId) throws BusinessException {
		try {
			List<StockTransLotVO> lst = stockTransLotDAO.getListStockTransLotVOForSearchSale(kPaging, stockTransId);
			ObjectVO<StockTransLotVO> vo = new ObjectVO<StockTransLotVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<StockTransLotVO> getListStockTransLotVOForStockUpdate(KPaging<StockTransLotVO> kPaging, long stockTransId)
		throws BusinessException {
		try {
			List<StockTransLotVO> lst=stockTransLotDAO.getListStockTransLotVOForStockUpdate(kPaging, stockTransId);
			ObjectVO<StockTransLotVO> vo = new ObjectVO<StockTransLotVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		}
		catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public StockTotal getStockTotalByProductCodeAndOwner(String productCode,
			StockObjectType ownerType, Long ownerId) throws BusinessException {
		try {
			return stockTotalDAO.getStockTotalByProductCodeAndOwner(productCode, ownerType, ownerId);
		}
		catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<ProductLot> getListProductLotWithProductID(
			KPaging<ProductLot> kPaging, Long ownerId,
			StockObjectType ownerType, Integer quantity, ActiveType status,
			Long productId) throws BusinessException {
		try {
			List<ProductLot> lst = productLotDAO.getListProductLot(kPaging, ownerId, ownerType, quantity, status, productId, null);
			ObjectVO<ProductLot> vo = new ObjectVO<ProductLot>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ProductLot> getListProductLotWithHandleApparms(Long ownerId,
			StockObjectType ownerType, Integer quantity, ActiveType status,
			Long productId) throws BusinessException {
		
		try {
			return productLotDAO.getListProductLot(null, ownerId, ownerType, quantity, status, productId, Boolean.TRUE);
		}
		catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<ProductLotVO> getListProductForVanSale(
			KPaging<ProductLotVO> kPaging, Long ownerId,
			StockObjectType ownerType) throws BusinessException {
		
		try {
			List<ProductLotVO> lst = productLotDAO.getListProductForVanSale(kPaging, ownerId, ownerType);
			ObjectVO<ProductLotVO> vo = new ObjectVO<ProductLotVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<ProductLotVO> getListProductVanSale(
			KPaging<ProductLotVO> kPaging, String productCode,
			String productName, Long ownerId, StockObjectType ownerType,
			Integer quantity, ActiveType status) throws BusinessException {
		try {
			List<ProductLotVO> lst = productLotDAO.getListProductVanSale(kPaging, productCode, productName, ownerId, ownerType, quantity, status);
			ObjectVO<ProductLotVO> vo = new ObjectVO<ProductLotVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<StockTransLotVO> getListLot(Long fromOwnerId,
			StockObjectType fromOwnerType, Long toOwnerId,
			StockObjectType toOwnerType, Date stockTransDate)
			throws BusinessException {
		try {
			return stockTransLotDAO.getListLot(fromOwnerId, fromOwnerType, toOwnerId, toOwnerType, stockTransDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public boolean isNotProductLotExpide(Long productId, String productLot, Long objectId, StockObjectType objectType) throws BusinessException {
		if (null == productId) {
			throw new BusinessException(
					ExceptionCode.PRODUCT_LOT_PRODUCT_ID_IS_NULL);
		}
		if (StringUtility.isNullOrEmpty(productLot)) {
			throw new BusinessException(
					ExceptionCode.PRODUCT_LOT_PRODUCT_LOT_IS_NULL_OR_EMPTY);
		}
		try {
			Date sysdate = this.commonMgr.getSysDate();
			ProductLot prLot = this.productLotDAO
					.getProductLotByCodeAndOwnerAndProduct(productLot,
							objectId, objectType, productId);

			if (null == prLot) {
				throw new BusinessException(
						ExceptionCode.PRODUCT_LOT_INSTANCE_IS_NULL);
			}

			if (null == prLot.getExpiryDate()) {
				throw new BusinessException(
						ExceptionCode.PRODUCT_LOT_EXPIRY_DATE_IS_NULL);
			}

			ApParam ap = apParamDAO.getApParamByCode("NUM_DAY_LOT_EXPIRE",
					ActiveType.RUNNING);

			int delta = 0;
			if (ap != null && Integer.parseInt(ap.getValue()) != -1) {
				delta = Integer.parseInt(ap.getValue());
			}

			return DateUtility.compareDateWithoutTime(prLot.getExpiryDate(),
					DateUtility.getShortDateFromDates(sysdate, delta)) >= 0;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<StockTotalVO> getListProductForImExport(
			KPaging<StockTotalVO> kPaging, String productCode,
			String productName, Long objectId, StockObjectType objectType,
			Integer fromQuantity) throws BusinessException {
		try {
			List<StockTotalVO> lst = stockTotalDAO.getListProductForImExport(kPaging, productCode, productName, objectId, objectType, fromQuantity);
			ObjectVO<StockTotalVO> vo = new ObjectVO<StockTotalVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<StockTotalVO> getListProductsCanSalesForShop(Long shopId)throws BusinessException {		
		try {
			List<StockTotalVO> lst = stockTotalDAO.getListProductsCanSalesForShop(shopId);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}		
	}
	
	@Override
	public List<StockTrans> getListStockTrans(String staffCode, Long shopID)
			throws BusinessException {
		return stockTransDAO.getListStockTrans(staffCode, shopID);
	}

	@Override
	public List<Staff> getListStaffTrans(Long shopId) throws BusinessException {
		
		return stockTransDAO.getListStaffTrans(shopId);
	}

	@Override
	public StockLock getStockLock(Long shopId, Long staffId) throws BusinessException {
		try{
			return stockLockDAO.getStockLock(shopId, staffId);
		}catch(DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public String generateStockTransCodeVansale(Long shopId, String transType) throws BusinessException {
		try {
			return stockTransDAO.generateStockTransCodeVansale(shopId, transType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * Lay danh sach Stock_Lock
	 * 
	 * @author hunglm16
	 * @return List StockLock
	 * @since JUNE 3,2014
	 * @description Get List Stock Lock by List Staff Id
	 * */
	@Override
	public List<StockLock> getListStockLockByListStaffId(List<Long> lstStaffId, Long shopId) throws BusinessException {
		try{
			return stockLockDAO.getListStockLockByListStaffId(lstStaffId, shopId);
		}catch(DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public StockLock getStockLockByStaffCode(String staffCode, Long shopId) throws BusinessException {
		try{
			return stockLockDAO.getStockLockByStaffCode(staffCode, shopId);
		}catch(DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public StockTotalVO getStockTotalByProductCodeAndOwnerEx(
			String productCode, StockObjectType ownerType, Long ownerId)
			throws BusinessException {
		try {
			return stockTotalDAO.getStockTotalByProductCodeAndOwnerEx(productCode, ownerType, ownerId);
		}
		catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public RptPXKKVCNB_Stock getPXKKVCNB(Long shopId, String staffCode, Long stockTransId, String transacName, String transacContent) throws BusinessException {
		try {
			return stockTransDAO.getPXKKVCNB(shopId, staffCode, stockTransId, transacName, transacContent);
		}catch(DataAccessException ex){
			throw new BusinessException(ex);
		}
	}
	
	
	@Override
	public List<StockTotalVO> getListProductsCanSalesForShopByWarehouse(Long shopId, Long productId, Long saleOrderId)throws BusinessException {		
		try {
			List<StockTotalVO> lst = stockTotalDAO.getListProductsCanSalesForShopByWarehouse(shopId,productId, saleOrderId);
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}		
	}

	@Override
	public StockTotal getStockTotalByProductAndOwner(long productId,
			StockObjectType ownerType, long ownerId, Long warehouseId) throws BusinessException {
		try {
			return stockTotalDAO.getStockTotalByProductAndOwner(productId,ownerType,ownerId,warehouseId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/***
	 * @author vuongmq
	 * @date 11/08/2015
	 * @description Lay getStockTotalByEntities theo filter
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public StockTotal getStockTotalByEntities(StockTotalFilter filter) throws BusinessException {
		try {
			return stockTotalDAO.getStockTotalByEntities(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public	List<StockTotal> getListStockTotalByProductAndOwner(StockTotalFilter stockTotalFilter) throws BusinessException {
		try {
			return stockTotalDAO.getListStockTotalByProductAndOwner(stockTotalFilter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<StockTotalVO> getListProductImportExport(StockTotalFilter filter)
			throws BusinessException {
		try {
			return stockTotalDAO.getListProductImportExport(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Warehouse> getListWarehouseByFilter(StockTotalFilter filter)
			throws BusinessException {
		try {
			return wareHouseDAO.getListWarehouseByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor=Exception.class)
	public Long createStockTransForUpdated(StockTrans stockTrans,List<StockTransDetailVO> lstStockTransDetailVO, 
			Integer fifo,Long shopId) throws BusinessException {
		try {
			Shop shop = null;
			if (shopId != null) {
				shop = shopDAO.getShopById(shopId);
			}
			Date sysDate = commonMgr.getSysDate();
			Long codeStock = Long.valueOf(0);			
			if (lstStockTransDetailVO.size() == 0){
				return codeStock;
			}
			stockTrans.setCreateDate(sysDate);
			stockTrans.setStockTransCode("TMP");
			stockTrans = this.createStockTrans(stockTrans);
			codeStock = stockTrans.getId();
			String transCode = stockTrans.getTransType() + "_" + stockTrans.getId();
			stockTrans.setStockTransCode(transCode);
			
			BigDecimal totalWeight = new BigDecimal(0);
			BigDecimal totalAmount = new BigDecimal(0);
			Integer totalQuantityLot = 0;
			
			List<StockTotal> lstStockTotals = new ArrayList<StockTotal>();
			/** Tao giao dich kho */
			Map<String, StockTransDetailVO> mapProducts = new HashMap<String, StockTransDetailVO>();
			for (StockTransDetailVO vo: lstStockTransDetailVO) {
				StockTransDetailVO stockTransDetailVO = mapProducts.get(vo.getStockTransDetail().getProduct().getProductCode()); 
				
				StockTransLot stockTransLot = new StockTransLot();				
				stockTransLot.setStockTrans(stockTrans);
				stockTransLot.setCreateDate(sysDate);
				stockTransLot.setCreateUser(stockTrans.getCreateUser());
				stockTransLot.setProduct(vo.getStockTransDetail().getProduct());
				stockTransLot.setQuantity(vo.getStockTransDetail().getQuantity());
				stockTransLot.setStockTransDate(stockTrans.getStockTransDate());
				stockTransLot.setShop(shop);
				if (stockTrans.getTransType().equals("DCG")) {
					stockTransLot.setFromOwnerId(vo.getStockTransDetail().getWarehouseId());
					stockTransLot.setToOwnerId(null);
					
					stockTransLot.setFromOwnerType(StockTransLotOwnerType.WAREHOUSE);
					stockTransLot.setToOwnerType(null);
					// giam ton kho dap ung
					StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwner(stockTransLot.getProduct().getId(), StockObjectType.SHOP, shopId, stockTransLot.getFromOwnerId());
					stockTotal.setAvailableQuantity(stockTotal.getAvailableQuantity() - stockTransLot.getQuantity());
					stockTotal.setUpdateDate(sysDate);
					stockTotal.setUpdateUser(stockTrans.getCreateUser());
					lstStockTotals.add(stockTotal);
				} else if (stockTrans.getTransType().equals("DCT")) {
					stockTransLot.setFromOwnerId(null);
					stockTransLot.setToOwnerId(vo.getStockTransDetail().getWarehouseId());
					
					stockTransLot.setFromOwnerType(null);
					stockTransLot.setToOwnerType(StockTransLotOwnerType.WAREHOUSE);
				} else {
					continue;
				}
				if (stockTransDetailVO == null) {
					stockTransDetailVO = new StockTransDetailVO();
					StockTransDetail stockTransDetail = vo.getStockTransDetail();
//					Price price = priceDAO.getPriceByProductAndShopId(stockTransDetail.getProduct().getId(),shopId);
//					if(price != null){
//						stockTransDetail.setPrice(price.getPrice());
//					}
					stockTransDetail.setStockTrans(stockTrans);
					stockTransDetail.setCreateDate(sysDate);
					stockTransDetail.setCreateUser(stockTrans.getCreateUser());
					stockTransDetail.setStockTransDate(stockTrans.getStockTransDate());
					
					stockTransDetailVO.setStockTransDetail(stockTransDetail);
					stockTransDetailVO.setStockTransLots(new ArrayList<StockTransLot>());					
				}
				stockTransDetailVO.getStockTransLots().add(stockTransLot);
				mapProducts.put(vo.getStockTransDetail().getProduct().getProductCode(),stockTransDetailVO);				
			}
			for (Entry<String, StockTransDetailVO> entry : mapProducts.entrySet()) {
			    StockTransDetailVO vo = entry.getValue();
			    StockTransDetail stockTransDetail = vo.getStockTransDetail();			   					
				stockTransDetail = stockTransDetailDAO.createStockTransDetail(stockTransDetail);
				totalQuantityLot = 0;
				for (StockTransLot stockTransLot : vo.getStockTransLots()) {
					totalQuantityLot +=  stockTransLot.getQuantity();
					stockTransLot.setStockTransDetail(stockTransDetail);	
					stockTransLotDAO.createStockTransLot(stockTransLot);
				}
				stockTransDetail.setQuantity(totalQuantityLot);
				stockTransDetailDAO.updateStockTransDetail(stockTransDetail);
				
				totalWeight = totalWeight.add(stockTransDetail.getProduct().getGrossWeight().multiply(new BigDecimal(totalQuantityLot)));
				totalAmount = totalAmount.add(stockTransDetail.getPrice().multiply(BigDecimal.valueOf(totalQuantityLot)));
			}
			// giam ton kho 
			for (int i = 0, size = lstStockTotals.size(); i < size; i++) {
				stockTotalDAO.updateStockTotal(lstStockTotals.get(i));
			}
			stockTrans.setTotalWeight(totalWeight);
			stockTrans.setTotalAmount(totalAmount);
			stockTransDAO.updateStockTrans(stockTrans);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return stockTrans.getId();
	}

	@Override
	public Warehouse getWarehouseByCode(String code,Long shopId) throws BusinessException {
		try {
			return wareHouseDAO.getWarehouseByCode(code,shopId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public ObjectVO<StockTotalVO> getListProductForCycleCount(KPaging<StockTotalVO> paging, long cycleCountId, StockTotalVOFilter filter) throws BusinessException {
		try {
			List<StockTotalVO> lst = stockTotalDAO.getListProductForCycleCount(paging, cycleCountId, filter);
			ObjectVO<StockTotalVO> objVO = new ObjectVO<StockTotalVO>();
			objVO.setkPaging(paging);
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author hoanv25
	 * Lay danh sach quan ly kho
	 */
	@Override
	public ObjectVO<StockTransVO> getListStockTransVOFilter(StockStransFilter filter) throws BusinessException {
		try {
			List<StockTransVO> lst = stockTransDAO.getListStockTransVOFilter(filter);
			ObjectVO<StockTransVO> objVO = new ObjectVO<StockTransVO>();
			objVO.setkPaging(filter.getkPagingStockTransVO());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	/**
	 * @author hoanv25
	 */
	@Override
	public ObjectVO<StockTotalVO> getListProductForCycleCountCategoryStock(KPaging<StockTotalVO> paging, long cycleCountId, StockTotalVOFilter filter) throws BusinessException {
		try {
			List<StockTotalVO> lst = stockTotalDAO.getListProductForCycleCountCategoryStock(paging, cycleCountId, filter);
			ObjectVO<StockTotalVO> objVO = new ObjectVO<StockTotalVO>();
			objVO.setkPaging(paging);
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}


	@Override
	@Transactional(rollbackFor=Exception.class)
	public StockLock createStockLock(StockLock stl) throws BusinessException {
		try {
		  stockTransDAO.createStockTransLock(stl);		 
		  return stl;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public StockLock getStockLockByFilter(StockLock filter) throws BusinessException {
		try{
			return stockLockDAO.getStockLockByFilter(filter);
		}catch(DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<StockTotal> getListStockTotalByProductAndShop(StockTotalFilter filter) throws BusinessException {
		try {
			List<StockTotal> lst = stockTotalDAO.getListStockTotalByProductAndShop(filter);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public List<StockTotal> getListStockTotalByCycleCount(CycleCount cycleCount) throws BusinessException {
		try {
			List<StockTotal> lst = stockTotalDAO.getListStockTotalByCycleCount(cycleCount);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public int countVansaleOrderNotUpdatedStock(long shopId, long staffId, Date pDate) throws BusinessException {
		try {
			int c = saleOrderDAO.countVansaleOrderNotUpdatedStock(shopId, staffId, pDate);
			return c;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<Warehouse> getListWarehouseByFilterChecking(StockTotalFilter filter)
			throws BusinessException {
		try {
			return wareHouseDAO.getListWarehouseByFilterChecking(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

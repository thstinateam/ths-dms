package ths.dms.core.business;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PoCustomer;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.PoCustomerDAO;

public class PoCustomerMgrImpl implements PoCustomerMgr {
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	PoCustomerDAO poCustomerDAO;

	@Override
	public void updatePoCustomer(PoCustomer poCustomer)
			throws BusinessException {
		try {
			poCustomerDAO.updatePoCustomer(poCustomer);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public PoCustomer getPoCustomerByOrderNumberAndShopId(String orderNumber,
			Long shopId, Date orderDate) throws BusinessException {
		try {
			PoCustomer lst = poCustomerDAO.getPoCustomerByOrderNumberAndShopId(
					orderNumber, shopId, orderDate);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}


}

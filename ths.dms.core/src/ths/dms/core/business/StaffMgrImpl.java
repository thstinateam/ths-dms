package ths.dms.core.business;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.SortedMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ActionLog;
import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ParentStaffMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffAttribute;
import ths.dms.core.entities.StaffAttributeDetail;
import ths.dms.core.entities.StaffAttributeEnum;
import ths.dms.core.entities.StaffGroup;
import ths.dms.core.entities.StaffGroupDetail;
import ths.dms.core.entities.StaffPositionLog;
import ths.dms.core.entities.StaffSaleCat;
import ths.dms.core.entities.StaffType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffFilterNew;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.ImageFilter;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.filter.StaffTypeFilter;
import ths.dms.core.entities.filter.SupFilter;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ParentStaffMapVO;
import ths.dms.core.entities.vo.ShopTreeVO;
import ths.dms.core.entities.vo.StaffComboxVO;
import ths.dms.core.entities.vo.StaffExportVO;
import ths.dms.core.entities.vo.StaffGroupDetailVO;
import ths.dms.core.entities.vo.StaffListPositionVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.entities.vo.StaffPositionVOEx;
import ths.dms.core.entities.vo.StaffSimpleVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.TreeExVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.UnitFilter;

import ths.dms.core.common.utils.Constant;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.ApParamDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.ProductAttributeDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.ShopLockDAO;
import ths.dms.core.dao.StaffAttributeDAO;
import ths.dms.core.dao.StaffDAO;
import ths.dms.core.dao.StaffDAONew;
import ths.dms.core.dao.StaffGroupDAO;
import ths.dms.core.dao.StaffGroupDetailDAO;
import ths.dms.core.dao.StaffPositionLogDAO;
import ths.dms.core.dao.StaffSaleCatDAO;
import ths.dms.core.dao.TrainingPlanDetailDAO;
import ths.dms.core.dao.VisitPlanDAO;

public class StaffMgrImpl implements StaffMgr {

	@Autowired
	StaffDAO staffDAO;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	StaffDAONew staffDAONew;

	@Autowired
	StaffSaleCatDAO staffSaleCatDAO;

	@Autowired
	VisitPlanDAO visitPlanDAO;

	@Autowired
	ShopMgr shopMgr;

	@Autowired
	StaffGroupDAO staffGroupDAO;

	@Autowired
	StaffPositionLogDAO staffPositionLogDAO;

	@Autowired
	StaffGroupDetailDAO staffGroupDetailDAO;

	@Autowired
	TrainingPlanDetailDAO trainingPlanDetailDAO;

	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	ApParamDAO apParamDAO;

	@Autowired
	ShopLockDAO shopLockDAO;

	@Autowired
	StaffAttributeDAO staffAttributeDAO;

	@Autowired
	ProductAttributeDAO productAttributeDAO;

	/**
	 * @author hunglm16
	 * @since September
	 * @description Ham ho tro,regex la dau phau (,);
	 *              filter.getInheritUserPriv() is not null
	 * */
	@Override
	public String returnStringGetArrayStaffCodeByArrCode(StaffPrsmFilter<StaffVO> filter, String arrStaffCodeStr) throws BusinessException, DataAccessException {
		String arrStaffCodeStrNew = "";
		if (StringUtility.isNullOrEmpty(arrStaffCodeStr)) {
			return arrStaffCodeStrNew;
		}
		String[] arrStrTemp = arrStaffCodeStr.split(",");
		if (arrStrTemp == null || arrStrTemp.length == 0) {
			return arrStaffCodeStrNew;
		}
		List<String> lstCode = new ArrayList<String>();
		for (String value : arrStrTemp) {
			lstCode.add(value);
		}
		filter.setLstCode(lstCode);
		List<StaffVO> lstStaffVo = staffDAONew.getListStaffVOFullChilrentByCMS(filter);
		if (lstStaffVo != null && !lstStaffVo.isEmpty()) {
			arrStaffCodeStrNew = lstStaffVo.get(0).getStaffCode().trim();
			for (int i = 1; i < lstStaffVo.size(); i++) {
				arrStaffCodeStrNew = arrStaffCodeStrNew.trim() + "," + lstStaffVo.get(i).getStaffCode().trim();
			}
		}
		return arrStaffCodeStr.trim();
	}

	/**
	 * @author hunglm16
	 * @since September
	 * @description Ham ho tro,regex la dau phau (,);
	 *              filter.getInheritUserPriv() is not null
	 * */
	@Override
	public List<String> returnLstStringGetArrayLstStaffCode(StaffPrsmFilter<StaffVO> filter, List<String> lstShortCode) throws BusinessException, DataAccessException {
		if (lstShortCode == null || lstShortCode.size() == 0) {
			return new ArrayList<String>();
		}
		List<String> lstData = new ArrayList<String>();
		filter.setLstCode(lstShortCode);
		List<StaffVO> lstStaffVo = staffDAONew.getListStaffVOFullChilrentByCMS(filter);
		if (lstStaffVo != null && !lstStaffVo.isEmpty()) {
			for (int i = 1; i < lstStaffVo.size(); i++) {
				lstData.add(lstStaffVo.get(i).getStaffCode().trim());
			}
		}
		return lstData;
	}

	/**
	 * @author hunglm16
	 * @since September
	 * @description Ham ho tro,regex la dau phau (,); filter.getParentStaffId()
	 *              is not null
	 * */
	@Override
	public String returnStringGetArrayStaffCodeByArrId(StaffPrsmFilter<StaffVO> filter, String arrStaffIdStr) throws BusinessException, DataAccessException {
		String arrStaffIdStrNew = "";
		if (StringUtility.isNullOrEmpty(arrStaffIdStr)) {
			return arrStaffIdStrNew;
		}
		String[] arrStrTemp = arrStaffIdStr.split(",");
		if (arrStrTemp == null || arrStrTemp.length == 0) {
			return arrStaffIdStrNew;
		}
		List<Long> lstId = new ArrayList<Long>();
		for (String value : arrStrTemp) {
			if (StringUtility.isNullOrEmpty(value)) {
				lstId.add(Long.valueOf(value));
			}
		}
		filter.setLstId(lstId);
		List<StaffVO> lstStaffVo = staffDAONew.getListStaffVOFullChilrentByCMS(filter);
		if (lstStaffVo != null && !lstStaffVo.isEmpty()) {
			arrStaffIdStrNew = lstStaffVo.get(0).getStaffCode().trim();
			for (int i = 1; i < lstStaffVo.size(); i++) {
				arrStaffIdStrNew = arrStaffIdStrNew.trim() + "," + lstStaffVo.get(i).getStaffCode().trim();
			}
		}
		return arrStaffIdStrNew.trim();
	}

	/**
	 * Tich hop theo Phan quyen CMS
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * @description Yeu cau phai duoc su dung neu lien quan den Staff
	 * */
	@Override
	public ObjectVO<StaffVO> searchListStaffVOByFilter(StaffPrsmFilter<StaffVO> filter) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAONew.searchListStaffVOByFilter(filter);
			ObjectVO<StaffVO> vo = new ObjectVO<StaffVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/**
	 * @author longnh15
	 * @since 25/05/2015
	 * @description Danh sach NVBH,NVGS cua NPP
	 * */
	@Override
	public ObjectVO<StaffVO> searchListStaffVOByShop(StaffPrsmFilter<StaffVO> filter) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAONew.searchListStaffVOByShop(filter);
			ObjectVO<StaffVO> vo = new ObjectVO<StaffVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/**
	 * Lay danh sach giam sat theo NVBH voi phan quyen CMS
	 * 
	 * @author hunglm16
	 * @since October 25 ,2014
	 * */
	@Override
	public ObjectVO<StaffVO> getListGSByStaffRootWithNVBH(StaffPrsmFilter<StaffVO> filter) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAONew.getListGSByStaffRootWithNVBH(filter);
			ObjectVO<StaffVO> vo = new ObjectVO<StaffVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/**
	 * Lay danh sach giam sat theo StaffRoot duoc phan quyen CMS
	 * 
	 * @author hunglm16
	 * @since October 25 ,2014
	 * */
	@Override
	public ObjectVO<StaffVO> getListGSByStaffRootWithInParentStaffMap(StaffPrsmFilter<StaffVO> filter) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAONew.getListGSByStaffRootWithInParentStaffMap(filter);
			ObjectVO<StaffVO> vo = new ObjectVO<StaffVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<StaffVO> getListStaffVOByFilter(StaffPrsmFilter<StaffVO> filter) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAONew.getListStaffVOByFilter(filter);
			ObjectVO<StaffVO> vo = new ObjectVO<StaffVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<StaffVO> getListStaffVOByShopId(BasicFilter<StaffVO> filter) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAONew.getListStaffVOByShopId(filter);
			ObjectVO<StaffVO> vo = new ObjectVO<StaffVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	///The end Tich hop theo Phan quyen CMS
	@Override
	public Staff createStaff(Staff staff, LogInfoVO logInfo) throws BusinessException {
		try {
			return staffDAO.createStaff(staff, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateStaff(Staff staff, LogInfoVO logInfo) throws BusinessException {
		try {
			/** liemtpt: dong code phan ben duoi lai ngay 02/03/2015 **/
			visitPlanDAO.updateVisitPlanStatusToStop(staff.getId());
			visitPlanDAO.deleteVisitPlanStatusToStop(staff.getId());
			staffDAO.updateStaff(staff, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteStaff(Staff staff, LogInfoVO logInfo) throws BusinessException {
		try {
			staffDAO.deleteStaff(staff, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ParentStaffMap createParentStaffMap(ParentStaffMap psm, LogInfoVO logInfo) throws BusinessException {
		try {
			return staffDAO.createParentStaffMap(psm, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void createListParentStaffMap(Long staffId, List<Long> lstParentStaffId, LogInfoVO logInfo) throws BusinessException {
		try {
			Staff staff = staffDAO.getStaffById(staffId);
			if (staff != null) {
				staffDAO.deleteParentByStaff(staffId, lstParentStaffId, logInfo);
				if (lstParentStaffId != null && lstParentStaffId.size() > 0) {
					StaffFilter filter = new StaffFilter();
					filter.setStaffId(staff.getId());
					filter.setIsGetAll(true);
					filter.setStatus(ActiveType.RUNNING);
					List<ParentStaffMap> lstParentStaffMap = staffDAO.getListParentStaffMap(filter);
					if (lstParentStaffMap != null && lstParentStaffMap.size() > 0) {
						for (int i = 0; i < lstParentStaffId.size(); i++) {
							for (ParentStaffMap psm : lstParentStaffMap) {
								if (lstParentStaffId.get(i).equals(psm.getParentStaff().getId())) {
									lstParentStaffId.remove(i);
									i--;
									break;
								}
							}
						}
					}
					for (Long parentId : lstParentStaffId) {
						ParentStaffMap psm = new ParentStaffMap();
						psm.setStatus(ActiveType.RUNNING);
						psm.setFromDate(commonDAO.getSysDate());
						psm.setStaff(staff);
						Staff parentStaff = staffDAO.getStaffById(parentId);
						if (parentStaff != null) {
							psm.setParentStaff(parentStaff);
							staffDAO.createParentStaffMap(psm, logInfo);
						}
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void updateParentStaffMap(ParentStaffMap psm, LogInfoVO logInfo) throws BusinessException {
		try {
			staffDAO.updateParentStaffMap(psm, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteParentStaffMap(ParentStaffMap psm, LogInfoVO logInfo) throws BusinessException {
		try {
			staffDAO.deleteParentStaffMap(psm, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Staff getStaffById(Long id) throws BusinessException {
		try {
			return id == null ? null : staffDAO.getStaffById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Staff getStaffByCode(String code) throws BusinessException {
		try {
			return staffDAO.getStaffByCode(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Staff getStaffLogin(String code, String password) throws BusinessException {
		try {
			return staffDAO.getStaffLogin(code, password);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Staff getStaffByInfoBasic(String code, Long shopId, ActiveType status) throws BusinessException {
		try {
			return staffDAO.getStaffByInfoBasic(code, shopId, status);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaff(StaffFilter filter) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListStaff(filter);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffNew(StaffFilterNew filter) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListStaffNew(filter);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<StaffVO> getListStaffVO(StaffFilter filter) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAO.getListStaffVO(filter);
			ObjectVO<StaffVO> vo = new ObjectVO<StaffVO>();
			vo.setkPaging(filter.getStaffVOPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffForExportExcel(StaffFilter filter) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListStaffForExportExcel(filter);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListPreAndVanStaff(KPaging<Staff> kPaging, String staffCode, String staffName, Long shopId, ActiveType staffActiveType, List<StaffObjectType> channelType) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListStaffByShopId(kPaging, staffCode, staffName, shopId, staffActiveType, channelType, null, null);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Staff> getListStaffByShop(List<Long> lstShopId, ActiveType staffActiveType, List<StaffObjectType> objectTypes) throws BusinessException {
		try {
			return staffDAO.getListStaffByShop(lstShopId, staffActiveType, objectTypes);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<StaffSimpleVO> getListStaffByShopCombo(List<Long> lstShopId, ActiveType staffActiveType, List<StaffObjectType> objectTypes) throws BusinessException {
		try {
			return staffDAO.getListStaffByShopCombo(lstShopId, staffActiveType, objectTypes);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<Staff> getListStaffByShopIdWithRunning(Long shopId) throws BusinessException {
		try {
			return staffDAO.getListStaffByShopIdWithRunning(shopId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<Staff> getStaffMultilChoice(Long staffId, String staffCode, Long shopId, Integer status) throws BusinessException {
		try {
			return staffDAO.getStaffMultilChoice(staffId, staffCode, shopId, status);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffEx1(KPaging<Staff> kPaging, String staffCode, String staffTypeCode, String staffName, String mobilePhoneNum, ActiveType status, String shopCode, StaffObjectType staffType, String saleTypeCode, String shopName)
			throws BusinessException {
		try {
			StaffFilter filter = new StaffFilter();
			filter.setkPaging(kPaging);
			filter.setStaffCode(staffCode);
			filter.setStaffTypeCode(staffTypeCode);
			filter.setStaffName(staffName);
			filter.setMobilePhoneNum(mobilePhoneNum);
			filter.setStatus(status);
			filter.setShopCode(shopCode);
			filter.setStaffType(staffType);
			filter.setSaleTypeCode(saleTypeCode);
			filter.setShopName(shopName);
			filter.setIsGetShopOnly(false);
			List<Staff> lst = staffDAO.getListStaff(filter);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffEx(KPaging<Staff> kPaging, String staffCode, String staffTypeCode, String staffName, String mobilePhoneNum, ActiveType status, String shopCode, StaffObjectType staffType, ChannelTypeType channelTypeType,
			Integer channelObjectType) throws BusinessException {
		try {
			StaffFilter filter = new StaffFilter();
			filter.setkPaging(kPaging);
			filter.setStaffCode(staffCode);
			filter.setStaffTypeCode(staffTypeCode);
			filter.setStaffName(staffName);
			filter.setMobilePhoneNum(mobilePhoneNum);
			filter.setStatus(status);
			filter.setShopCode(shopCode);
			filter.setStaffType(staffType);
			filter.setChannelTypeType(channelTypeType);
			if (channelObjectType != null)
				filter.setLstChannelObjectType(Arrays.asList(channelObjectType));
			filter.setIsGetShopOnly(false);
			List<Staff> lst = staffDAO.getListStaff(filter);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffEx2(StaffFilter filter) throws BusinessException {
		try {
			filter.setIsGetShopOnly(true);
			List<Staff> lst = staffDAO.getListStaff(filter);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffEx5(StaffFilter filter) throws BusinessException {
		try {
			filter.setIsGetShopOnly(false);
			List<Staff> lst = staffDAO.getListStaff(filter);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffByOwnerId(KPaging<Staff> kPaging, Long ownerId, String shopCode, String shopName, String staffCode, String staffName) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListStaffByOwnerId(kPaging, ownerId, shopCode, shopName, staffCode, staffName);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffByShopCode(KPaging<Staff> kPaging, String shopCode) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListStaffByShopCode(kPaging, shopCode);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean isUsingByOthers(int staffId) throws BusinessException {
		try {
			return staffDAO.isUsingByOthers(staffId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	//------------------------------------
	@Override
	public StaffSaleCat createStaffSaleCat(StaffSaleCat staffSaleCat, LogInfoVO logInfo) throws BusinessException {
		try {
			return staffSaleCatDAO.createStaffSaleCat(staffSaleCat, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void updateStaffSaleCat(StaffSaleCat staffSaleCat, LogInfoVO logInfo) throws BusinessException {
		try {
			staffSaleCatDAO.updateStaffSaleCat(staffSaleCat, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	//	@Transactional(rollbackFor = Exception.class )
	@Override
	public void deleteStaffSaleCat(StaffSaleCat staffSaleCat, LogInfoVO logInfo) throws BusinessException {
		try {
			//			List<StaffSaleCat> lst = staffSaleCatDAO.getListStaffSaleCat(null, staffSaleCat.getStaff().getId());
			//			for (StaffSaleCat s: lst)
			//				staffSaleCatDAO.deleteStaffSaleCat(s, logInfo);
			staffSaleCatDAO.deleteStaffSaleCat(staffSaleCat, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public StaffSaleCat getStaffSaleCatById(Long id) throws BusinessException {
		try {
			return id == null ? null : staffSaleCatDAO.getStaffSaleCatById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<StaffSaleCat> getListStaffSaleCat(KPaging<StaffSaleCat> kPaging, Long staffId) throws BusinessException {
		try {
			List<StaffSaleCat> lst = staffSaleCatDAO.getListStaffSaleCat(kPaging, staffId);
			ObjectVO<StaffSaleCat> vo = new ObjectVO<StaffSaleCat>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkIfRecordExist(long staffId, Long catId, Long id) throws BusinessException {
		try {
			return staffSaleCatDAO.checkIfRecordExist(staffId, catId, id);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Boolean checkStaffExistsInShopWithStaffGroup(long shopId) throws BusinessException {
		try {
			return staffGroupDAO.checkStaffExistsInShopWithStaffGroup(shopId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Staff getStaffByRoutingCustomer(Long customerId) throws BusinessException {
		try {
			return staffDAO.getStaffByRoutingCustomer(customerId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Boolean checkIfStaffExists(String email, String mobiphone, String idno, Long id) throws BusinessException {
		try {
			return staffDAO.checkIfStaffExists(email, mobiphone, idno, id);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<Staff> getListSupervisorByShop(KPaging<Staff> kPaging, String staffCode, String staffName, String childShopCode) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListSupervisorByShop(kPaging, staffCode, staffName, childShopCode);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<Staff> getListSupervisorByShopAndAncestor(KPaging<Staff> kPaging, String staffCode, String staffName, String childShopCode) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListSupervisorByShop(kPaging, staffCode, staffName, childShopCode, true);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	private TreeExVO<Shop, Staff> getListTreeEx(TreeVO<Shop> shopTree, List<Staff> lstStaff, Boolean isGetAll) {
		TreeExVO<Shop, Staff> treeEx = new TreeExVO<Shop, Staff>();
		treeEx.setObject(shopTree.getObject());
		List<TreeExVO<Shop, Staff>> lstChildShopTreeEx = new ArrayList<TreeExVO<Shop, Staff>>();
		List<Staff> lstLeaves = new ArrayList<Staff>();

		List<Staff> lstTempStaff = new ArrayList<Staff>();
		for (Staff st : lstStaff) {
			if (st.getShop().getId().equals(shopTree.getObject().getId())) {
				lstLeaves.add(st);
			} else
				lstTempStaff.add(st);
		}
		lstStaff.removeAll(lstStaff);
		lstStaff.addAll(lstTempStaff);

		for (TreeVO<Shop> childTree : shopTree.getListChildren()) {
			TreeExVO<Shop, Staff> childTreeEx = new TreeExVO<Shop, Staff>();
			childTreeEx = this.getListTreeEx(childTree, lstStaff, isGetAll);
			if ((childTreeEx.getListChildren().size() + childTreeEx.getListLeaves().size() > 0) || Boolean.TRUE.equals(isGetAll)) {
				lstChildShopTreeEx.add(childTreeEx);
			}
		}
		treeEx.setListChildren(lstChildShopTreeEx);
		treeEx.setListLeaves(lstLeaves);

		return treeEx;
	}

	@Override
	public List<Staff> getSaleStaffByDate(RoutingCustomerFilter filter) throws BusinessException {
		try {
			return staffDAO.getSaleStaffByDate(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Staff getSaleStaffByDateForCreateOrder(Long customerId, Long shopId, Date orderDate, Long staffId) throws BusinessException {
		try {
			return staffDAO.getSaleStaffByDateForCreateOrder(customerId, shopId, orderDate, staffId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Boolean checkIfStaffManageAnySaleMan(Long staffId) throws BusinessException {
		try {
			return staffDAO.checkIfStaffManageAnySaleMan(staffId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updateSaleManagerToNull(Long staffOwnerId) throws BusinessException {
		try {
			staffDAO.updateSaleManagerToNull(staffOwnerId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updateSaleManager(Long staffOwnerId, List<Long> lstStaffId) throws BusinessException {
		try {
			staffDAO.updateSaleManager(staffOwnerId, lstStaffId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<Staff> getListStaffByOwner(String staffOwnerCode, String staffCode, String staffName, StaffObjectType staffObjectType) throws BusinessException {
		try {
			return staffDAO.getListStaffByOwner(staffOwnerCode, staffCode, staffName, staffObjectType);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffNotManagedByOwnerId(KPaging<Staff> kPaging, Long ownerId, String shopCode, String shopName, String staffCode, String staffName, Long parentShopId) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListStaffNotManagedByOwnerId(kPaging, ownerId, shopCode, shopName, staffCode, staffName, parentShopId);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Boolean checkStaffInShopOldFamily(String staffCode, String shopCode) throws BusinessException {
		try {
			return staffDAO.checkStaffInShopOldFamily(staffCode, shopCode);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public StaffSaleCat getStaffSaleCat(Long staffId, Long productInfoId) throws BusinessException {
		try {
			return staffSaleCatDAO.getStaffSaleCat(staffId, productInfoId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updateOwnerIdToNull(Long staffOwnerId) throws BusinessException {
		try {
			staffDAO.updateOwnerIdToNull(staffOwnerId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public String generateStaffCode() throws BusinessException {
		try {
			return staffDAO.generateStaffCode();
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Boolean checkStaffPhoneExists(String phoneNum) throws BusinessException {
		try {
			return staffDAO.checkStaffPhoneExists(phoneNum);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.StaffMgr#getListStaffWithFullCondition(com.
	 * viettel.core.entities.enumtype.KPaging, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String,
	 * ths.dms.core.entities.enumtype.ActiveType, java.lang.String,
	 * ths.dms.core.entities.enumtype.StaffObjectType,
	 * ths.dms.core.entities.enumtype.ChannelTypeType, java.util.List,
	 * java.lang.String, java.lang.String, java.lang.Boolean, java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public ObjectVO<Staff> getListStaffWithFullCondition(StaffFilter filter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<Staff> lst = staffDAO.getListStaff(filter);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.StaffMgr#getListStaffForSuperVisor(com.viettel
	 * .core.entities.enumtype.KPaging, java.lang.String, java.lang.String,
	 * java.lang.String, ths.dms.core.entities.enumtype.ChannelTypeType,
	 * java.util.List, java.lang.Boolean)
	 */
	@Override
	public ObjectVO<StaffVO> getListStaffForSuperVisorWithCondition(KPaging<StaffVO> kPaging, String staffCode, String staffName, String shopCode, ActiveType status, ChannelTypeType type, List<StaffObjectType> listObjectType, Boolean isHasChild)
			throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<StaffVO> lst = staffDAO.getListStaffForSuperVisorWithCondition(kPaging, staffCode, staffName, shopCode, status, type, listObjectType, isHasChild);
			ObjectVO<StaffVO> vo = new ObjectVO<StaffVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffByShop(KPaging<Staff> kPaging, Long shopId, ActiveType activeType, List<StaffObjectType> objectType) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListStaffByShopId(kPaging, shopId, activeType, objectType);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.StaffMgr#getListSupervisorAllowShop(com.viettel
	 * .core.entities.enumtype.KPaging, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.Boolean)
	 */
	@Override
	public ObjectVO<Staff> getListSupervisorAllowShop(KPaging<Staff> kPaging, String staffCode, String staffName, String shopCode, Boolean isHasChild) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<Staff> lst = staffDAO.getListSupervisorAllowShop(kPaging, staffCode, staffName, shopCode, isHasChild);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.StaffMgr#getListStaffAllowOwnerAndShop(com.
	 * viettel.core.entities.enumtype.KPaging, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.Boolean)
	 */
	@Override
	public ObjectVO<Staff> getListStaffAllowOwnerAndShop(KPaging<Staff> kPaging, String staffOwnerCode, String staffCode, String staffName, String shopCode, Boolean isHasChild) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<Staff> lst = staffDAO.getListStaffAllowOwnerAndShop(kPaging, staffOwnerCode, staffCode, staffName, shopCode, isHasChild);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListNVGS(KPaging<Staff> kPaging, String staffCode, String staffName, Long shopId, List<String> listTBHVCode) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListNVGS(kPaging, staffCode, staffName, shopId, listTBHVCode);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListNVGS2(KPaging<Staff> kPaging, Long shopId, String lstStaffCode) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListNVGS2(kPaging, shopId, lstStaffCode);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListNVGS3(KPaging<Staff> kPaging, String strListShopId, String staffCode, String staffName, String lstStaffCode) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListNVGS3(kPaging, strListShopId, staffCode, staffName, lstStaffCode);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListNVGS4(KPaging<Staff> kPaging, String strListShopId, String staffCode, String staffName) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListNVGS4(kPaging, strListShopId, staffCode, staffName);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListNVBH(KPaging<Staff> kPaging, String staffCode, String staffName, List<String> lstStaffOwnerCode, Long shopId, Boolean isGetChildShop, List<String> lstTBHVCode) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListNVBH(kPaging, staffCode, staffName, lstStaffOwnerCode, shopId, isGetChildShop, lstTBHVCode);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<StaffComboxVO> getListNVBHisShop(KPaging<StaffComboxVO> kPaging, String staffCode, String staffName, List<String> lstStaffOwnerCode, Long shopId, Boolean isGetChildShop, List<String> lstTBHVCode) throws BusinessException {
		try {
			List<StaffComboxVO> lst = staffDAO.getListNVBHisShop(kPaging, staffCode, staffName, lstStaffOwnerCode, shopId, isGetChildShop, lstTBHVCode);
			ObjectVO<StaffComboxVO> vo = new ObjectVO<StaffComboxVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListNVBH2(KPaging<Staff> kPaging, String staffCode, String staffName, List<String> lstStaffOwnerCode, String strListShopId, Boolean isGetChildShop, List<String> lstTBHVCode) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListNVBH2(kPaging, staffCode, staffName, lstStaffOwnerCode, strListShopId, isGetChildShop, lstTBHVCode);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/**
	 * Tim kiem nhan vien them moi trong NVPTKH (CTTB)
	 * 
	 * @author thongnm
	 */
	@Override
	public ObjectVO<Staff> getListNVBHForDisplayProgram(KPaging<Staff> kPaging, Long shopId, Long displayProgramId, String code, String name) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListNVBHForDisplayProgram(kPaging, shopId, displayProgramId, code, name);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListNVGH(KPaging<Staff> kPaging, Long shopId, String customerShortCode) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListNVGH(kPaging, shopId, customerShortCode);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.StaffMgr#getListStaffExceptListStaff(com.viettel
	 * .core.entities.enumtype.KPaging, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String,
	 * ths.dms.core.entities.enumtype.ActiveType, java.lang.String,
	 * ths.dms.core.entities.enumtype.StaffObjectType, java.lang.String,
	 * java.lang.Boolean, java.lang.String, java.lang.String, java.lang.String,
	 * java.util.List)
	 */
	@Override
	public ObjectVO<Staff> getListStaffExceptListStaff(KPaging<Staff> kPaging, String staffCode, String staffName, ActiveType status, String shopCode, Long staffTypeId, List<Long> exceptStaffIds) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<Staff> lst = staffDAO.getListStaffExceptListStaff(kPaging, staffCode, staffName, status, shopCode, staffTypeId, exceptStaffIds);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffForAbsentReport(KPaging<Staff> kPaging, String staffCode, String staffName, Long shopId) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListStaffForAbsentReport(kPaging, staffCode, staffName, shopId);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.StaffMgr#getListPreAndVanStaffHasChild(com.
	 * viettel.core.entities.enumtype.KPaging, java.lang.String,
	 * java.lang.String, java.lang.Long,
	 * ths.dms.core.entities.enumtype.ActiveType, java.util.List,
	 * java.lang.Boolean)
	 */
	@Override
	public ObjectVO<Staff> getListPreAndVanStaffHasChild(KPaging<Staff> kPaging, String staffCode, String staffName, List<Long> shopIds, ActiveType staffActiveType, List<StaffObjectType> channelType, Boolean isHasChild, List<Long> listStaffOwnerIds)
			throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<Staff> lst = staffDAO.getListPreAndVanStaffHasChild(kPaging, staffCode, staffName, shopIds, staffActiveType, channelType, isHasChild, listStaffOwnerIds);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	//------------------staff_group----------------------

	@Override
	public StaffGroup createStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws BusinessException {
		try {
			return staffGroupDAO.createStaffGroup(staffGroup, logInfo);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void deleteStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws BusinessException {
		try {
			staffGroupDAO.deleteStaffGroup(staffGroup, logInfo);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updateStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws BusinessException {
		try {
			staffGroupDAO.updateStaffGroup(staffGroup, logInfo);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public StaffGroup getStaffGroupById(long id) throws BusinessException {
		try {
			return staffGroupDAO.getStaffGroupById(id);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<StaffGroup> getListStaffGroup(KPaging<StaffGroup> kPaging, Long shopId, ActiveType activeType) throws BusinessException {
		try {
			ObjectVO<StaffGroup> vo = new ObjectVO<StaffGroup>();
			List<StaffGroup> lst = staffGroupDAO.getListStaffGroup(kPaging, shopId, activeType);
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public StaffGroupDetail checkIfStaffExistsInStaffGroup(Long staffId, Long staffGroupId, ActiveType activeType) throws BusinessException {
		try {
			return staffGroupDAO.checkIfStaffExistsInStaffGroup(staffId, staffGroupId, activeType);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	//-----------staff_group_detail-----------------------
	@Transactional(rollbackFor = Exception.class)
	@Override
	public StaffGroupDetail createStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws BusinessException {
		/*
		 * try { Staff s = staffGroupDetail.getStaff(); //update STAFF:
		 * stafF_owner_id, shop_id if
		 * (StaffObjectType.NVBH.getValue().equals(s.getStaffType
		 * ().getObjectType()) ||
		 * StaffObjectType.NVVS.getValue().equals(s.getStaffType
		 * ().getObjectType())) { //lay NVGS trong nhom Staff nvgs =
		 * staffDAO.getNVGSInGroup(staffGroupDetail.getStaffGroup().getId()); //
		 * s.setStaffOwner(nvgs);
		 * s.setShop(staffGroupDetail.getStaffGroup().getShop());
		 * staffDAO.updateStaff(s, logInfo); } else if
		 * (StaffObjectType.NVGS.getValue
		 * ().equals(s.getStaffType().getObjectType())) { //shop trong group la
		 * npp //update staff_owner cho tat ca nvbh trong nhom List<Staff>
		 * lstNvbh =
		 * staffDAO.getListNVBHInGroup(staffGroupDetail.getStaffGroup()
		 * .getId()); for (Staff nvbh : lstNvbh) // for (Staff nvbh: lstNvbh) //
		 * nvbh.setStaffOwner(s); this.updateListStaff(lstNvbh, logInfo); Staff
		 * tbhv =
		 * staffDAO.getTBHVOfShop(staffGroupDetail.getStaffGroup().getShop
		 * ().getParentShop().getId()); //set staff_owner cua nvgs la tbhv //
		 * s.setStaffOwner(tbhv); //set shop_id la vung
		 * s.setShop(staffGroupDetail
		 * .getStaffGroup().getShop().getParentShop()); this.updateStaff(s,
		 * logInfo); } else if
		 * (StaffObjectType.TBHV.getValue().equals(s.getStaffType
		 * ().getObjectType())) { //shop trong group la vung List<Staff>
		 * listNvgs =
		 * staffDAO.getListNVGSInVung(staffGroupDetail.getStaffGroup()
		 * .getShop().getId()); for (Staff nvgs : listNvgs) // for (Staff nvgs:
		 * listNvgs) // nvgs.setStaffOwner(s); this.updateListStaff(listNvgs,
		 * logInfo);
		 * 
		 * Staff tbhm =
		 * staffDAO.getTBHMOfShop(staffGroupDetail.getStaffGroup().getShop
		 * ().getParentShop().getId()); // s.setStaffOwner(tbhm);
		 * s.setShop(staffGroupDetail.getStaffGroup().getShop());
		 * this.updateStaff(s, logInfo); } else if
		 * (StaffObjectType.TBHM.getValue
		 * ().equals(s.getStaffType().getObjectType())) { //shop trong group le
		 * mien List<Staff> listTbhv =
		 * staffDAO.getListTBHVInMien(staffGroupDetail
		 * .getStaffGroup().getShop().getId()); for (Staff tbhv : listTbhv) //
		 * for (Staff tbhv: listTbhv) // tbhv.setStaffOwner(s);
		 * this.updateListStaff(listTbhv, logInfo);
		 * s.setShop(staffGroupDetail.getStaffGroup().getShop());
		 * this.updateStaff(s, logInfo); }
		 * 
		 * return staffGroupDetailDAO.createStaffGroupDetail(staffGroupDetail,
		 * logInfo); } catch (DataAccessException ex) { throw new
		 * BusinessException(ex); }
		 */
		return null;
	}

	@Override
	public void removeStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws BusinessException {
		/*
		 * staffGroupDetail.setStatus(ActiveType.DELETED);
		 * this.updateStaffGroupDetail(staffGroupDetail, logInfo); try { Staff s
		 * = staffGroupDetail.getStaff();
		 * 
		 * trainingPlanDetailDAO.updateTrainingPlanDetailStatusToDelete(s,
		 * staffGroupDetail.getStaffGroup().getShop().getId());
		 * staffDAO.updateStaff(s, logInfo); //update STAFF: stafF_owner_id,
		 * shop_id if
		 * (StaffObjectType.NVGS.getValue().equals(s.getStaffType().getObjectType
		 * ())) { //shop trong group la npp //update staff_owner cho tat ca nvbh
		 * trong nhom List<Staff> lstNvbh =
		 * staffDAO.getListNVBHInGroup(staffGroupDetail
		 * .getStaffGroup().getId()); for (Staff nvbh : lstNvbh) // for (Staff
		 * nvbh: lstNvbh) // nvbh.setStaffOwner(null);
		 * this.updateListStaff(lstNvbh, logInfo); } else if
		 * (StaffObjectType.TBHV
		 * .getValue().equals(s.getStaffType().getObjectType())) { //shop trong
		 * group la vung List<Staff> listNvgs =
		 * staffDAO.getListNVGSInVung(staffGroupDetail
		 * .getStaffGroup().getShop().getId()); for (Staff nvgs : listNvgs) //
		 * for (Staff nvgs: listNvgs) // nvgs.setStaffOwner(null);
		 * this.updateListStaff(listNvgs, logInfo); } else if
		 * (StaffObjectType.TBHM
		 * .getValue().equals(s.getStaffType().getObjectType())) { //shop trong
		 * group la mien List<Staff> listTbhv =
		 * staffDAO.getListTBHVInMien(staffGroupDetail
		 * .getStaffGroup().getShop().getId()); for (Staff tbhv : listTbhv) //
		 * for (Staff tbhv: listTbhv) // tbhv.setStaffOwner(null);
		 * this.updateListStaff(listTbhv, logInfo); }
		 * 
		 * } catch (DataAccessException ex) { throw new BusinessException(ex); }
		 */
		return;
	}

	private void updateListStaff(List<Staff> listStaff, LogInfoVO logInfo) throws BusinessException {
		for (Staff st : listStaff) {
			this.updateStaff(st, logInfo);
		}
	}

	@Override
	public void deleteStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws BusinessException {
		try {
			staffGroupDetailDAO.deleteStaffGroupDetail(staffGroupDetail, logInfo);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void updateStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws BusinessException {
		try {
			staffGroupDetailDAO.updateStaffGroupDetail(staffGroupDetail, logInfo);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public StaffGroupDetail getStaffGroupDetailById(long id) throws BusinessException {
		try {
			return staffGroupDetailDAO.getStaffGroupDetailById(id);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<StaffGroupDetail> getListStaffGroupDetail(KPaging<StaffGroupDetail> kPaging, Long staffGroupId, ActiveType activeType) throws BusinessException {
		try {
			ObjectVO<StaffGroupDetail> vo = new ObjectVO<StaffGroupDetail>();
			List<StaffGroupDetail> lst = staffGroupDetailDAO.getListStaffGroupDetail(kPaging, staffGroupId, activeType);
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffInStaffGroup(KPaging<Staff> paging, Long staffGroupId, ActiveType activeType) throws BusinessException {
		try {
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			List<Staff> lst = staffGroupDetailDAO.getListStaffInStaffGroup(paging, staffGroupId, activeType);
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<Staff> getListNVGSNew(KPaging<Staff> paging, String staffCode, String staffName, Long vungId, Long shopId, List<Long> lstExceptId, SortedMap<Long, List<Long>> deletedStaffByVung) throws BusinessException {
		try {
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			List<Staff> lst = staffDAO.getListNVGSNew(paging, staffCode, staffName, vungId, shopId, lstExceptId, deletedStaffByVung);
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<ShopTreeVO> getListShopManagedByStaff(Long staffId, KPaging<ShopTreeVO> kPaging) throws BusinessException {
		try {
			ObjectVO<ShopTreeVO> vo = new ObjectVO<ShopTreeVO>();
			List<ShopTreeVO> lst = staffDAO.getListShopManagedByStaff(staffId, kPaging);
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public void removeStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws BusinessException {
		staffGroup.setStatus(ActiveType.DELETED);
		this.updateStaffGroup(staffGroup, logInfo);
	}

	@Override
	public void removeStaff(Staff staff, LogInfoVO logInfo) throws BusinessException {
		try {
			staff.setStatus(ActiveType.STOPPED);
			this.updateStaff(staff, logInfo);
			List<StaffGroupDetail> lst = staffGroupDetailDAO.getListStaffGroupDetailByStaff(staff.getId());
			for (StaffGroupDetail s : lst) {
				this.removeStaffGroupDetail(s, logInfo);
			}
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<StaffGroupDetail> getListStaffGroupDetailByStaff(Long staffId) throws BusinessException {
		try {
			return staffGroupDetailDAO.getListStaffGroupDetailByStaff(staffId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<StaffPositionVO> getListStaffPositionWithTraining(Long shopId) throws BusinessException {
		try {
			List<StaffPositionVO> list = staffDAO.getListStaffPositionWithTraining(shopId);
			return list;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<StaffPositionVO> getListStaffPosition(Long shopId, Long staffId, Boolean isVNM, Boolean isMien, Boolean isVung, Boolean isNPP, Boolean oneNode) throws BusinessException {
		try {
			List<StaffPositionVO> list = staffDAO.getListStaffPosition(shopId, staffId, isVNM, isMien, isVung, isNPP, oneNode);
			return list;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<StaffPositionVO> getListStaffPosition(SupFilter filter) throws BusinessException {
		try {
			return staffDAO.getListStaffPosition(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ParentStaffMapVO> getListParentStaffMapVO(SupFilter filter) throws BusinessException {
		try {
			return staffDAO.getListParentStaffMapVO(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public StaffPositionVO getStaffPosition(Long staffId) throws BusinessException {
		try {
			return staffDAO.getStaffPosition(staffId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<StaffPositionVO> getListStaffPosition1(Long shopId, Long staffOwnerId, StaffObjectType roleType, Boolean oneNode) throws BusinessException {
		try {
			List<StaffPositionVO> list = staffDAO.getListStaffPosition(shopId, staffOwnerId, roleType, oneNode);
			if (shopId != null && oneNode != null && oneNode == true) {
				List<StaffPositionVO> listFilter = new ArrayList<StaffPositionVO>();
				if (StaffObjectType.KTNPP.equals(roleType)) {
					for (StaffPositionVO temp : list) {
						if (temp.getRoleType() != null && (StaffObjectType.NVBH.getValue().equals(temp.getRoleType()) || StaffObjectType.NVVS.getValue().equals(temp.getRoleType()))) {
							listFilter.add(temp);
						}
					}
				} else {
					Integer roleTypeFlag = 0;

					for (StaffPositionVO temp : list) {//lay ra roleType > nhat
						if (StaffObjectType.NHVNM.getValue().equals(temp.getRoleType())
								&& (roleTypeFlag == 0 || roleTypeFlag == StaffObjectType.TBHM.getValue() || roleTypeFlag == StaffObjectType.TBHV.getValue() || roleTypeFlag == StaffObjectType.NVGS.getValue()
										|| roleTypeFlag == StaffObjectType.NVBH.getValue() || roleTypeFlag == StaffObjectType.NVVS.getValue())) {
							roleTypeFlag = StaffObjectType.NHVNM.getValue();
						} else if (StaffObjectType.TBHM.getValue().equals(temp.getRoleType())
								&& (roleTypeFlag != StaffObjectType.NHVNM.getValue())
								&& (roleTypeFlag == 0 || roleTypeFlag == StaffObjectType.TBHV.getValue() || roleTypeFlag == StaffObjectType.NVGS.getValue() || roleTypeFlag == StaffObjectType.NVBH.getValue() || roleTypeFlag == StaffObjectType.NVVS
										.getValue())) {
							roleTypeFlag = StaffObjectType.TBHM.getValue();
						} else if (StaffObjectType.TBHV.getValue().equals(temp.getRoleType()) && (roleTypeFlag != StaffObjectType.NHVNM.getValue() && (roleTypeFlag != StaffObjectType.TBHM.getValue()))
								&& (roleTypeFlag == 0 || roleTypeFlag == StaffObjectType.NVGS.getValue() || roleTypeFlag == StaffObjectType.NVBH.getValue() || roleTypeFlag == StaffObjectType.NVVS.getValue())) {
							roleTypeFlag = StaffObjectType.TBHV.getValue();
						} else if (StaffObjectType.NVGS.getValue().equals(temp.getRoleType()) && (roleTypeFlag != StaffObjectType.NHVNM.getValue() && roleTypeFlag != StaffObjectType.TBHM.getValue() && roleTypeFlag != StaffObjectType.TBHV.getValue())
								&& (roleTypeFlag == 0 || roleTypeFlag == StaffObjectType.NVBH.getValue() || roleTypeFlag == StaffObjectType.NVVS.getValue())) {
							roleTypeFlag = StaffObjectType.NVGS.getValue();
						} else if ((StaffObjectType.NVBH.getValue().equals(temp.getRoleType()) || StaffObjectType.NVVS.getValue().equals(temp.getRoleType()))
								&& (roleTypeFlag != StaffObjectType.NHVNM.getValue() && roleTypeFlag != StaffObjectType.TBHM.getValue() && roleTypeFlag != StaffObjectType.TBHV.getValue() && roleTypeFlag != StaffObjectType.NVGS.getValue())) {
							if (roleTypeFlag < temp.getRoleType()) {
								roleTypeFlag = temp.getRoleType();
							}
						}
					}

					if (StaffObjectType.NVGS.equals(roleType) && (roleTypeFlag == StaffObjectType.NHVNM.getValue() || roleTypeFlag == StaffObjectType.TBHM.getValue() || roleTypeFlag == StaffObjectType.TBHV.getValue())) {
						roleTypeFlag = StaffObjectType.NVGS.getValue();
					} else if (StaffObjectType.TBHV.equals(roleType) && (roleTypeFlag == StaffObjectType.NHVNM.getValue() || roleTypeFlag == StaffObjectType.TBHM.getValue())) {
						roleTypeFlag = StaffObjectType.TBHV.getValue();
					} else if (StaffObjectType.TBHM.equals(roleType) && roleTypeFlag == StaffObjectType.NHVNM.getValue()) {
						roleTypeFlag = StaffObjectType.TBHM.getValue();
					}

					for (StaffPositionVO temp : list) {
						if (StaffObjectType.NVGS.equals(roleType)) {
							if (temp.getRoleType() != null && temp.getRoleType() <= roleTypeFlag) {
								listFilter.add(temp);
							}
						} else if (temp.getRoleType() != null && temp.getRoleType().equals(roleTypeFlag)) {
							listFilter.add(temp);
						}
					}
				}
				return listFilter;
			}
			return list;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkStaffAvailableLatLng(Long staffId) throws BusinessException {
		try {
			StaffPositionLog spl = staffDAO.getLatLngOfStaff(staffId);
			if (spl == null) {
				return false;
			} else {
				return true;
			}
		} catch (DataAccessException e) {
			return false;
		}
	}

	/**
	 * @author hungnm
	 * @param shopId
	 * @param staffOwnerId
	 * @param roleType
	 * @param oneNode
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public List<StaffPositionLog> getListStaffPosition2(Long userId, Boolean getTbhv, Boolean getGsnpp, Boolean getNvbh) throws BusinessException {
		try {
			List<StaffPositionLog> list = staffDAO.getListStaffPosition2(userId, getTbhv, getGsnpp, getNvbh);
			return list;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<StaffGroupDetail> getListStaffByGroupDetail(StaffFilter filter) throws BusinessException {
		try {
			ObjectVO<StaffGroupDetail> vo = new ObjectVO<StaffGroupDetail>();
			List<StaffGroupDetail> lst = staffDAO.getListStaffGroupDetail(filter);
			vo.setkPaging(filter.getSgdPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Staff getGDMByShop(Long shopId) throws BusinessException {
		try {
			return staffDAO.getStaffOwnerOfShop(shopId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Staff getStaffOnwerOfShop(Long shopId) throws BusinessException {
		try {
			return staffDAO.getStaffOwnerOfShop(shopId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public TreeVO<StaffPositionVO> searchStaff(SupFilter filter) throws BusinessException {
		try {
			return staffDAO.searchStaff(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	// LIEMTPT : Danh sach NVGS theo TBHV (HuanLuyen)
	@Override
	public ObjectVO<Staff> getListNVGSByTBHVId(KPaging<Staff> kPaging, Long tbhvId) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListNVGSByTBHVId(kPaging, tbhvId);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void saveTree(List<String> lstStaffAddId, List<Long> lstStaffShopAddId, LogInfoVO logInfo) throws BusinessException {
		try {
			if (lstStaffAddId == null || lstStaffShopAddId == null || lstStaffAddId.size() != lstStaffShopAddId.size()) {
				throw new BusinessException("List invalid");
			}
			for (int i = 0, n = lstStaffAddId.size(); i < n; i++) {
				Staff staff = staffDAO.getStaffById(Long.valueOf(lstStaffAddId.get(i)));
				if (staff != null) {
					Shop shop = shopDAO.getShopById(lstStaffShopAddId.get(i));
					if (shop != null) {
						staff.setShop(shop);
						staffDAO.updateStaff(staff, logInfo);
					} else {
						throw new BusinessException("shopId not exists");
					}
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	//	public void updateWhenDeleteStaff(Staff staff, StaffGroup staffGroup){
	//	try{
	//		if(StaffObjectType.NVGS.getValue().equals(staff.getStaffType().getObjectType())){//GSNPP
	//			//update lai staffowner cua nvbh trong nhom thanh null
	//			//neu trong bang staffGroupDetail ko co dong nao cua gsnpp nua thi update shop va staffowner la null
	//		}
	//	}catch(BusinessException e){
	//		LogUtility.logError(e, "UnitTreeCatalogAction.updateWhenAddStaff");
	//	}
	//}
	//public void updateWhenAddStaff(Staff staff,StaffGroup staffGroup){
	//	try{
	//		if(StaffObjectType.NVGS.getValue().equals(staff.getStaffType().getObjectType())){//GSNPP
	//			//update lai shop va staffOwner cua vung va staffowner cua nvbh trong group
	//			List<StaffObjectType> lstSOT=new ArrayList<StaffObjectType>();
	//			lstSOT.add(StaffObjectType.TBHV);
	//			ObjectVO<Staff> tbhvVO=staffMgr.getListStaffByShop(null, staffGroup.getShop().getParentShop().getId(), ActiveType.RUNNING, lstSOT);
	//			if(tbhvVO!=null && tbhvVO.getLstObject()!=null && tbhvVO.getLstObject().size()>0){
	//				staff.setStaffOwner(tbhvVO.getLstObject().get(0));
	//				staff.setShop(staffGroup.getShop().getParentShop());
	//				staffMgr.updateStaff(staff, getLogInfoVO());
	//			}
	//			ObjectVO<StaffGroupDetail> sgdVO = staffMgr.getListStaffGroupDetail(null, staffGroup.getId(), ActiveType.RUNNING);
	//			if(sgdVO!=null && sgdVO.getLstObject()!=null && sgdVO.getLstObject().size()>0){
	//				for(StaffGroupDetail temp:sgdVO.getLstObject()){
	//					Staff nvbh = temp.getStaff();
	//					if( StaffObjectType.NVBH.getValue().equals(nvbh.getStaffType().getObjectType()) || 
	//						StaffObjectType.NVGH.getValue().equals(nvbh.getStaffType().getObjectType()) ||
	//						StaffObjectType.NVTT.getValue().equals(nvbh.getStaffType().getObjectType()) ||
	//						StaffObjectType.NVVS.getValue().equals(nvbh.getStaffType().getObjectType())){
	//						nvbh.setStaffOwner(staff);
	//						staffMgr.updateStaff(nvbh, getLogInfoVO());
	//					}
	//				}
	//			}
	//		}else if(StaffObjectType.TBHV.getValue().equals(staff.getStaffType().getObjectType())){//TBHV
	//			//update lai gsnpp theo tbhv
	//			
	//		}
	//	}catch(BusinessException e){
	//		LogUtility.logError(e, "UnitTreeCatalogAction.updateWhenAddStaff");
	//	}
	//}
	/**
	 * @author thachnn
	 * @param code
	 * @param parentId
	 *            des : kiem tra code staff co thuoc quan ly user dang nhap
	 */
	@Override
	public Staff getStaffByCodeOfUserLogin(String code, Long parentId) throws BusinessException {
		try {
			return staffDAO.getStaffByCodeOfUserLogin(code, parentId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Staff> getListStaffByShop(KPaging<Staff> kPaging, List<Long> lstShopId, String staffName, String staffCode, ActiveType staffActiveType, List<StaffObjectType> objectTypes) throws BusinessException {
		try {
			List<Staff> lstObject = staffDAO.getListStaffByShop(kPaging, lstShopId, staffName, staffCode, staffActiveType, objectTypes);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lstObject);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Staff getStaffByCodeAndShopId(String staffCode, Long shopId) throws BusinessException {
		try {
			if (!StringUtility.isNullOrEmpty(staffCode)) {
				staffCode = staffCode.trim();
			}
			return staffDAO.getStaffByCodeAndShopId(staffCode, shopId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Staff getStaffByCodeAndListShop(String staffCode, String lstLshopId) throws BusinessException {
		try {
			if (!StringUtility.isNullOrEmpty(staffCode)) {
				staffCode = staffCode.trim();
			}
			return staffDAO.getStaffByCodeAndListShop(staffCode, lstLshopId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/** SangTN: Update Giam sat lo trinh cua nhan vien ban hang */

	@Override
	public TreeVO<StaffPositionVO> searchStaffEx(SupFilter filter) throws BusinessException {
		try {
			return staffDAO.searchStaffEx(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<StaffPositionVO> getListStaffPositionOptionsDate(SupFilter filter) throws BusinessException {
		try {
			return staffDAO.getListStaffPositionOptionsDate(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/** SangTN: Update Giam sat lo trinh cua nhan vien ban hang */
	@Override
	public StaffListPositionVO getStaffPositionEx(SupFilter filter) throws BusinessException {
		try {
			StaffListPositionVO staffListPosition = new StaffListPositionVO();

			List<StaffPositionVOEx> lstStaffPositionVO = staffDAO.getStaffPositionEx(filter);
			for (int i = 0; i < lstStaffPositionVO.size(); i++) {
				lstStaffPositionVO.get(i).setOrdinalVisit(i + 1);

			}
			if (lstStaffPositionVO != null && lstStaffPositionVO.size() > 0) {
				int indexLast = lstStaffPositionVO.size() - 1;
				staffListPosition.setStaffId(lstStaffPositionVO.get(indexLast).getStaffId());
				staffListPosition.setStaffCode(lstStaffPositionVO.get(indexLast).getStaffCode());
				staffListPosition.setStaffName(lstStaffPositionVO.get(indexLast).getStaffName());
				staffListPosition.setStaffOwnerId(lstStaffPositionVO.get(indexLast).getStaffOwnerId());
				staffListPosition.setStaffOwnerCode(lstStaffPositionVO.get(indexLast).getStaffOwnerCode());
				staffListPosition.setStaffOwnerName(lstStaffPositionVO.get(indexLast).getStaffOwnerName());

				staffListPosition.setShopId(lstStaffPositionVO.get(indexLast).getStaffId());
				staffListPosition.setShopCode(lstStaffPositionVO.get(indexLast).getShopCode());
				staffListPosition.setShopName(lstStaffPositionVO.get(indexLast).getShopName());
				staffListPosition.setRoleType(lstStaffPositionVO.get(indexLast).getRoleType());
				staffListPosition.setCountVisit(lstStaffPositionVO.get(indexLast).getCountVisit());
				staffListPosition.setAccuracy(lstStaffPositionVO.get(indexLast).getAccuracy());
				staffListPosition.setCreateTime(lstStaffPositionVO.get(indexLast).getCreateTime());
				staffListPosition.setCountStaff(lstStaffPositionVO.get(indexLast).getCountStaff());
				staffListPosition.setHhmm(lstStaffPositionVO.get(indexLast).getHhmm());
				staffListPosition.setIsBold(lstStaffPositionVO.get(indexLast).getIsBold());

				staffListPosition.setLat(lstStaffPositionVO.get(indexLast).getLat());
				staffListPosition.setLng(lstStaffPositionVO.get(indexLast).getLng());

				staffListPosition.setLstStaffPositionVO(lstStaffPositionVO);
			}
			return staffListPosition;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Staff getStaffByCodeAndShopId1(String staffCode, Long shopId) throws BusinessException {
		try {
			return staffDAO.getStaffByCodeAndShopId1(staffCode, shopId);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void offVisitPlanOfStaff(long staffId, long shopId, String userCode) throws BusinessException {
		try {
			if (StringUtility.isNullOrEmpty(userCode)) {
				throw new IllegalArgumentException("user code is null");
			}
			Date lockDate = shopLockDAO.getApplicationDate(shopId);
			staffDAO.offVisitPlanOfStaff(staffId, shopId, lockDate, userCode);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public ObjectVO<Staff> getListStaffPopup(StaffFilter filter) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListStaffPopup(filter);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setLstObject(lst);
			vo.setkPaging(filter.getkPaging());
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public ObjectVO<Staff> getListStaffOfShop(StaffFilter filter) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListStaffOfShop(filter);
			ObjectVO<Staff> vo = new ObjectVO<Staff>();
			vo.setLstObject(lst);
			vo.setkPaging(filter.getkPaging());
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public ObjectVO<StaffExportVO> getListExportStaffOfShop(StaffFilter filter) throws BusinessException {
		try {
			List<StaffExportVO> lst = staffDAO.getListExportStaffOfShop(filter);
			ObjectVO<StaffExportVO> vo = new ObjectVO<StaffExportVO>();
			vo.setLstObject(lst);
			vo.setkPaging(filter.getExPaging());
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public ObjectVO<StaffGroupDetailVO> getListStaffGroupOfUnitTree(StaffFilter filter, KPaging<StaffGroupDetailVO> paging) throws BusinessException {
		try {
			List<StaffGroupDetailVO> lst = staffDAO.getListStaffGroupOfUnitTree(filter, paging);
			ObjectVO<StaffGroupDetailVO> vo = new ObjectVO<StaffGroupDetailVO>();
			vo.setLstObject(lst);
			vo.setkPaging(paging);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public void stopStaff(Staff staff, LogInfoVO logInfo) throws BusinessException {
		try {
			staff.setStatus(ActiveType.STOPPED);
			this.updateStaff(staff, logInfo);
			staffDAO.stopParentStaffMap(staff, logInfo);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * Lay danh sach nhan vien giam sat theo shop
	 * 
	 * @author tulv2
	 * @since 06.10.2014
	 * */
	@Override
	public ObjectVO<StaffVO> getListNVGSAdapter(String shopId, Long staffRoot) throws BusinessException {
		if (shopId == null || shopId.isEmpty()) {
			throw new IllegalArgumentException("shopId is null");
		}
		ObjectVO<StaffVO> result = new ObjectVO<StaffVO>();
		try {
			StaffPrsmFilter<StaffVO> filter = new StaffPrsmFilter<StaffVO>();
			List<Long> lstShopId = new ArrayList<Long>();
			lstShopId.add(Long.valueOf(shopId));
			filter.setLstShopId(lstShopId);
			filter.setObjectType(new Integer(StaffObjectType.NVGS.getValue()));
			filter.setStatus(ActiveType.RUNNING.getValue());
			filter.setInheritUserPriv(staffRoot);
			result.setLstObject(staffDAONew.searchListStaffVOByFilter(filter));
			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Staff> getListStaffYMamagerStaffX(Long staffIdY, Long staffIdX) throws BusinessException {
		try {
			return staffDAO.getListStaffYMamagerStaffX(staffIdY, staffIdX);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Staff getStaffByFilterWithCMS(BasicFilter<Staff> filter) throws BusinessException {
		try {
			return staffDAONew.getStaffByFilterWithCMS(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Staff> getListStaffByShop(StaffFilter filter) throws BusinessException {
		try {
			return staffDAO.getListStaffByShop(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<StaffVO> getListStaffVOAndChilrentByShopId(BasicFilter<StaffVO> filter) throws BusinessException {
		try {

			List<StaffVO> lst = staffDAONew.getListStaffVOAndChilrentByShopId(filter);
			ObjectVO<StaffVO> vo = new ObjectVO<StaffVO>();
			vo.setLstObject(lst);
			vo.setkPaging(filter.getkPaging());
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public Long saveTree(List<String> lstGroupDelete, List<String> lstStaffDeleteId, List<String> lstGroupIdChange, List<String> lstGroupIdAdd, List<String> lstGroupNameAdd, List<String> lstGroupShopAdd, List<String> lstStaffAddId,
			List<String> lstGroupNameChange, List<String> lstStaffGroupDeleteId, List<String> lstStaffGroupAddId, String idGroupAddNewStaff, LogInfoVO logInfo) throws BusinessException {
		Long idGroupAddNewStaffReturn = null;
		try {
			//Xoa group
			if (lstGroupDelete != null && lstGroupDelete.size() > 0) {
				try {
					for (int i = 0; i < lstGroupDelete.size(); i++) {
						StaffGroup staffGroup = this.getStaffGroupById(Long.valueOf(lstGroupDelete.get(i)));
						if (staffGroup != null)
							this.removeStaffGroup(staffGroup, logInfo);
					}
				} catch (Exception e) {
					throw new IllegalArgumentException("staffMgr.saveTree.lstGroupDelete" + e.getMessage());
				}
			}
			//Xoa staff khoi group
			if (lstStaffDeleteId != null && lstStaffDeleteId.size() > 0 && lstStaffGroupDeleteId != null && lstStaffGroupDeleteId.size() == lstStaffDeleteId.size()) {
				try {
					for (int i = 0; i < lstStaffDeleteId.size(); i++) {
						StaffGroupDetail staffGroupDetail = this.checkIfStaffExistsInStaffGroup(Long.valueOf(lstStaffDeleteId.get(i)), Long.valueOf(lstStaffGroupDeleteId.get(i)), null);
						if (staffGroupDetail != null) {
							this.removeStaffGroupDetail(staffGroupDetail, logInfo);
						}
					}
				} catch (Exception e) {
					throw new IllegalArgumentException("staffMgr.saveTree.lstStaffDeleteId" + e.getMessage());
				}
			}
			//doi ten group da co trong db
			if (lstGroupIdChange != null && lstGroupIdChange.size() > 0 && lstGroupNameChange != null && lstGroupNameChange.size() == lstGroupIdChange.size()) {
				try {
					for (int i = 0; i < lstGroupIdChange.size(); i++) {
						StaffGroup staffGroup = this.getStaffGroupById(Long.valueOf(lstGroupIdChange.get(i)));
						if (staffGroup != null) {
							staffGroup.setStaffGroupName(lstGroupNameChange.get(i));
							staffGroup.setUpdateDate(commonDAO.getSysDate());
							staffGroup.setUpdateUser(logInfo.getStaffCode());
							this.updateStaffGroup(staffGroup, logInfo);
						}
					}
				} catch (Exception e) {
					throw new IllegalArgumentException("staffMgr.saveTree.lstGroupIdChange" + e.getMessage());
				}
			}
			//tao group moi
			if (lstGroupIdAdd != null && lstGroupIdAdd.size() > 0 && lstGroupNameAdd != null && lstGroupNameAdd.size() == lstGroupIdAdd.size()) {
				try {
					for (int i = 0; i < lstGroupIdAdd.size(); i++) {
						Shop shop = shopMgr.getShopById(Long.valueOf(lstGroupShopAdd.get(i)));
						if (shop != null) {
//							Boolean flag = true;
							//if(!ShopObjectType.NPP.getValue().equals(shop.getType().getObjectType())){//mien vung
							//							ObjectVO<StaffGroup> staffGroupTest = staffMgr.getListStaffGroup(null, shop.getId(), ActiveType.RUNNING);
							//							if(staffGroupTest!=null && staffGroupTest.getLstObject()!=null && staffGroupTest.getLstObject().size()>0){
							//								//da co roi thi ko cho add group nua
							//								flag=false;
							//							}
							//}
//							if (flag) {
								StaffGroup staffGroup = new StaffGroup();
								staffGroup.setCreateUser(logInfo.getStaffCode());
								staffGroup.setStaffGroupName(lstGroupNameAdd.get(i));
								staffGroup.setShop(shop);
								staffGroup.setStatus(ActiveType.RUNNING);
								staffGroup.setCreateDate(commonDAO.getSysDate());
								staffGroup = this.createStaffGroup(staffGroup, logInfo);
								//doi lai id cua nhung group moi them de them nhan vien vao group neu co
								if (lstStaffGroupAddId != null && lstStaffGroupAddId.size() > 0) {
									for (int k = 0; k < lstStaffGroupAddId.size(); k++) {
										if (lstStaffGroupAddId.get(k).equals(lstGroupIdAdd.get(i))) {
											lstStaffGroupAddId.set(k, String.valueOf(staffGroup.getId()));
										}
									}
								}
								//cap nhat lai nhom cua nhan vien can them moi neu nhom cung~ moi them
								if (!StringUtility.isNullOrEmpty(idGroupAddNewStaff) && lstGroupIdAdd.get(i).equals(idGroupAddNewStaff)) {
									idGroupAddNewStaffReturn = staffGroup.getId();
								}
//							}
						}
					}
				} catch (Exception e) {
					throw new IllegalArgumentException("staffMgr.saveTree.lstGroupIdAdd" + e.getMessage());
				}
			}
			//Them staff vao group
			if (lstStaffAddId != null && lstStaffAddId.size() > 0 && lstStaffGroupAddId != null && lstStaffGroupAddId.size() == lstStaffAddId.size()) {
				try {
					for (int i = 0; i < lstStaffAddId.size(); i++) {
						Staff staff = this.getStaffById(Long.valueOf(lstStaffAddId.get(i)));
						StaffGroup staffGroup = this.getStaffGroupById(Long.valueOf(lstStaffGroupAddId.get(i)));
						if (staff != null && staffGroup != null) {
							StaffGroupDetail staffGroupDetail = new StaffGroupDetail();
							staffGroupDetail.setCreateDate(commonDAO.getSysDate());
							staffGroupDetail.setCreateUser(logInfo.getStaffCode());
							staffGroupDetail.setStaff(staff);
							staffGroupDetail.setStaffGroup(staffGroup);
							staffGroupDetail.setStatus(ActiveType.RUNNING);
							this.createStaffGroupDetail(staffGroupDetail, logInfo);
							//							updateWhenAddStaff(staff,staffGroup);
						}
					}
				} catch (Exception e) {
					throw new IllegalArgumentException("staffMgr.saveTree.lstStaffAddId" + e.getMessage());
				}
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		return idGroupAddNewStaffReturn;
	}

	@Override
	public Boolean checkParentChildrentByDoubleStaffId(Long parentStaffId, Long childStaffId) throws BusinessException {
		try {
			return staffDAONew.checkParentChildrentByDoubleStaffId(parentStaffId, childStaffId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public ObjectVO<Staff> getListStaffLockedStock(StaffFilter filter) throws BusinessException {
		try {
			List<Staff> lst = staffDAO.getListStaffLockedStock(filter);
			ObjectVO<Staff> objVO = new ObjectVO<Staff>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author vuongmq
	 * @since Feb 27, 2015
	 * @param staff
	 *            cap nhat va tao moi thong tin cua staff, thuoc tinh dong staff
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Staff saveOrUpdateStaff(Staff staff, List<Long> lstAttributeId, List<String> lstAttributeValue, List<Integer> lstAttributeColumnValueType, LogInfoVO logInfo) throws BusinessException {
		if (staff == null || logInfo == null) {
			throw new IllegalArgumentException("invalid parameter");
		}
		try {
			Date sysDate = commonDAO.getSysDate();
			if (staff.getId() == null) {
				staff.setCreateDate(sysDate);
				staff = staffDAO.createStaff(staff, logInfo);
			} else { // cap nhat
				staff.setUpdateDate(sysDate);
				if (ActiveType.STOPPED.equals(staff.getStatus())) {
					visitPlanDAO.updateVisitPlanStatusToStop(staff.getId());
					visitPlanDAO.deleteVisitPlanStatusToStop(staff.getId());
				}
				staffDAO.updateStaff(staff, logInfo);
			}
			StaffAttributeDetail staffAttributeDetail = null;
			StaffAttributeEnum staffAttributeEnum = null;
			int index = 0;
			if (lstAttributeId != null && lstAttributeId.size() > 0) {
				for (Long attrId : lstAttributeId) {
					StaffAttribute staffAttribute = commonDAO.getEntityById(StaffAttribute.class, attrId);
					if (staffAttribute == null) {
						continue;
					}
					if (AttributeColumnType.CHARACTER.equals(staffAttribute.getType()) || AttributeColumnType.NUMBER.equals(staffAttribute.getType()) || AttributeColumnType.LOCATION.equals(staffAttribute.getType())
							|| AttributeColumnType.CHOICE.equals(staffAttribute.getType()) || AttributeColumnType.DATE_TIME.equals(staffAttribute.getType())) {

						staffAttributeDetail = staffAttributeDAO.getStaffAttributeDetailByFilter(attrId, staff.getId(), null);

						if (AttributeColumnType.CHOICE.equals(staffAttribute.getType()) && staffAttributeDetail != null) {
							staffAttributeDetail.setStatus(ActiveType.DELETED);
							staffAttributeDetail.setUpdateDate(sysDate);
							staffAttributeDetail.setUpdateUser(logInfo.getStaffCode());
							commonDAO.updateEntity(staffAttributeDetail);
							staffAttributeDetail = null;
						}

						if (staffAttributeDetail == null) {
							staffAttributeDetail = new StaffAttributeDetail();
							staffAttributeDetail.setCreateDate(sysDate);
							staffAttributeDetail.setCreateUser(logInfo.getStaffCode());

							staffAttributeDetail.setStaff(staff);
							staffAttributeDetail.setStatus(ActiveType.RUNNING);
							staffAttributeDetail.setStaffAttribute(staffAttribute);

							if (AttributeColumnType.CHOICE.equals(staffAttribute.getType())) {
								if (!StringUtility.isNullOrEmpty(lstAttributeValue.get(index))) {
									staffAttributeEnum = commonDAO.getEntityById(StaffAttributeEnum.class, Long.valueOf(lstAttributeValue.get(index)));
									if (staffAttributeEnum != null) {
										staffAttributeDetail.setStaffAttributeEnum(staffAttributeEnum);
										commonDAO.createEntity(staffAttributeDetail);
									}
								}
							} else {
								staffAttributeDetail.setValue(lstAttributeValue.get(index));
								commonDAO.createEntity(staffAttributeDetail);
							}
						} else {
							staffAttributeDetail.setUpdateDate(sysDate);
							staffAttributeDetail.setUpdateUser(logInfo.getStaffCode());
							staffAttributeDetail.setValue(lstAttributeValue.get(index));
							commonDAO.updateEntity(staffAttributeDetail);
						}
					} else if (AttributeColumnType.MULTI_CHOICE.equals(staffAttribute.getType())) {
						String valueStr = lstAttributeValue.get(index); //format x;y;z
						String[] valueStrArray = valueStr.split(";");

						List<StaffAttributeDetail> attributeDetails = staffAttributeDAO.getStaffAttributeDetailByFilter(attrId, staff.getId());

						for (StaffAttributeDetail detail : attributeDetails) {
							detail.setStatus(ActiveType.DELETED);
							detail.setUpdateDate(sysDate);
							detail.setUpdateUser(logInfo.getStaffCode());
							commonDAO.updateEntity(detail);
						}
						if (!StringUtility.isNullOrEmpty(valueStr)) {
							for (String str : valueStrArray) {
								staffAttributeDetail = new StaffAttributeDetail();
								staffAttributeDetail.setCreateDate(sysDate);
								staffAttributeDetail.setCreateUser(logInfo.getStaffCode());

								staffAttributeDetail.setStaff(staff);

								staffAttributeDetail.setStaffAttribute(staffAttribute);
								staffAttributeDetail.setStatus(ActiveType.RUNNING);

								staffAttributeEnum = commonDAO.getEntityById(StaffAttributeEnum.class, Long.valueOf(str));
								if (staffAttributeEnum != null) {
									staffAttributeDetail.setStaffAttributeEnum(staffAttributeEnum);
								}

								commonDAO.createEntity(staffAttributeDetail);
							}
						}
					}
					++index;
				}
			}
			return staff;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<StaffVO> findStaffVOBy(KPaging<StaffVO> kPaging, UnitFilter unitFilter) throws BusinessException {
		try {
			List<StaffVO> staffs = staffDAO.findStaffVOBy(kPaging, unitFilter);
			ObjectVO<StaffVO> result = new ObjectVO<StaffVO>();
			result.setkPaging(kPaging);
			result.setLstObject(staffs);
			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ImageVO> getListGSByNPP(Long shopId, ImageFilter filter) throws BusinessException {
		try {
			return staffDAO.getListGSByNPP(shopId, filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ImageVO> getListNVBHByNPP(Long shopId, ImageFilter filter) throws BusinessException {
		try {
			return staffDAO.getListNVBHByNPP(shopId, filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<StaffVO> getListGsById(List<Long> lstGsId) throws BusinessException {
		try {
			List<StaffVO> staffs = staffDAO.getListGsById(lstGsId);
			ObjectVO<StaffVO> result = new ObjectVO<StaffVO>();
			/* result.setkPaging(kPaging); */
			result.setLstObject(staffs);
			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ImageVO> getListNVBHByNPPShop(List<Long> lstShopId, ImageFilter filter) throws BusinessException {
		try {
			return staffDAO.getListNVBHByNPPShop(lstShopId, filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<StaffVO> listEquipCategoryCode(StaffFilter filter) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAO.listEquipCategoryCode(filter);
			ObjectVO<StaffVO> objVO = new ObjectVO<StaffVO>();
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<StaffVO> listCheckSubStaff(StaffSaleCat filter) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAO.listCheckSubStaff(filter);
			ObjectVO<StaffVO> objVO = new ObjectVO<StaffVO>();
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ObjectVO<StaffVO> listCheckDelSubStaff(Long id) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAO.listCheckDelSubStaff(id);
			ObjectVO<StaffVO> objVO = new ObjectVO<StaffVO>();
			objVO.setLstObject(lst);
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<StaffExportVO> getListStaffWithDynamicAttribute(StaffFilter filter) throws BusinessException {
		try {
			return staffDAO.getListStaffWithDynamicAttribute(filter);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author trietptm
	 */
	@Override
	public StaffPositionLog getLastStaffPositionLog(Long staffId, Date checkDate, String fromTime, String toTime) throws BusinessException {
		try {
			return staffPositionLogDAO.getLastStaffPositionLog(staffId, checkDate, fromTime, toTime);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author trietptm
	 */
	@Override
	public List<CustomerVO> getListCustomerHasVisitPlan(Long shopId, Long staffId, Date checkDate, String fromTime, String toTime) throws BusinessException {
		if (null == staffId) {
			throw new IllegalArgumentException("staff is null");
		}
		try {
			ApParam config = apParamDAO.getApParamByCode(Constant.CF_IS_CUSTOMER_WAITING, ApParamType.SYS_CONFIG);
			Integer val = null;
			if (config != null) {
				val = Integer.parseInt(config.getValue());
			}
			return staffDAO.getListCustomerHasVisitPlan(shopId, staffId, val, checkDate, fromTime, toTime);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author trietptm
	 */
	@Override
	public List<StaffPositionLog> showDirectionStaff(Long staffId, Date checkDate, Integer numPoint, String fromTime, String toTime) throws BusinessException {
		try {
			return staffDAO.showDirectionStaff(staffId, checkDate, numPoint, fromTime, toTime);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author trietptm
	 */
	@Override
	public List<ActionLog> getListActionLog(Long staffId, Date checkDate, String fromTime, String toTime) throws BusinessException {
		try {
			return staffDAO.getListActionLog(staffId, checkDate, fromTime, toTime);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<StaffVO> getListStaffVOInherit(KPaging<StaffVO> kPaging, UnitFilter unitFilter) throws BusinessException {
		try {
			return staffDAO.getListStaffVOInherit(kPaging, unitFilter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<StaffType> getListStaffType(UnitFilter unitFilter) throws BusinessException {
		try {
			return staffDAO.getListStaffType(unitFilter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<StaffType> getListAllStaffType(StaffTypeFilter filter) throws BusinessException {
		try {
			return staffDAO.getListAllStaffType(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<StaffVO> getListStaffVOFeedbackByFilter(StaffFilter filter) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAO.getListStaffVOFeedbackByFilter(filter);
			ObjectVO<StaffVO> objVO = new ObjectVO<StaffVO>();
			objVO.setLstObject(lst);
			objVO.setkPaging(filter.getStaffVOPaging());
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<StaffVO> getListStaffVOUserMapStaffByFilter(StaffFilter filter) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAO.getListStaffVOUserMapStaffByFilter(filter);
			ObjectVO<StaffVO> objVO = new ObjectVO<StaffVO>();
			objVO.setLstObject(lst);
			objVO.setkPaging(filter.getStaffVOPaging());
			return objVO;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public ObjectVO<StaffVO> getListGSByStaffRootKAMT(StaffPrsmFilter<StaffVO> filter) throws BusinessException {
		try {
			List<StaffVO> lst = staffDAONew.getListGSByStaffRootKAMT(filter);
			ObjectVO<StaffVO> vo = new ObjectVO<StaffVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.StaffMgr#getListGSByFilter(ths.dms.core.entities.filter.StaffPrsmFilter)
	 */
	@Override
	public List<StaffVO> getListGSByFilter(StaffPrsmFilter<StaffVO> filter) throws BusinessException {
		try {
			return staffDAONew.getListGSByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}
package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipFormHistory;
import ths.dms.core.entities.EquipSuggestEviction;
import ths.dms.core.entities.EquipSuggestEvictionDTL;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.EquipTradeStatus;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.StatusDeliveryEquip;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.filter.EquipSuggestEvictionFilter;
import ths.dms.core.entities.vo.EquipSuggestEvictionDetailVO;
import ths.dms.core.entities.vo.EquipmentSuggestEvictionVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.EquipSuggestEvictionDAO;
import ths.dms.core.dao.EquipmentManagerDAO;
import ths.dms.core.dao.StaffDAO;

public class EquipSuggestEvictionMgrImpl implements EquipSuggestEvictionMgr {

	@Autowired
	EquipSuggestEvictionDAO equipSuggestEvictionDAO;

	@Autowired
	EquipmentManagerDAO equipmentManagerDAO;
	
	@Autowired
	private CommonDAO commonDAO;
	
	@Autowired
	private StaffDAO staffDAO;

	@Override
	public ObjectVO<EquipmentSuggestEvictionVO> searchListEquipSuggestEvictionVOByFilter(EquipSuggestEvictionFilter<EquipmentSuggestEvictionVO> filter) throws BusinessException {
		try {
			List<EquipmentSuggestEvictionVO> lst = equipSuggestEvictionDAO.searchListEquipSuggestEvictionVOByFilter(filter);
			ObjectVO<EquipmentSuggestEvictionVO> objVO = new ObjectVO<EquipmentSuggestEvictionVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public EquipSuggestEviction getEquipSuggestEvictionByCode(String code) throws BusinessException {	
		try {			
			return equipSuggestEvictionDAO.getEquipSuggestEvictionByCode(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipSuggestEviction getEquipSuggestEvictionById(Long id) throws BusinessException {
		try {			
			return commonDAO.getEntityById(EquipSuggestEviction.class, id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipSuggestEvictionDTL> getListEquipSuggestEvictionDetailByFilter(EquipSuggestEvictionFilter<EquipSuggestEvictionDTL> filter) throws BusinessException {
		try {
			List<EquipSuggestEvictionDTL> lst = equipSuggestEvictionDAO.getListEquipSuggestEvictionDetailByFilter(filter);			
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEquipSuggestEviction(EquipSuggestEvictionFilter<EquipSuggestEviction> filter) throws BusinessException {
		final int ADD = 0;
		final int DELETE = -1;
		final int EXISTS = 1;
		try {
			Date sysdate = commonDAO.getSysDate();
			EquipSuggestEviction equipSuggestEviction = filter.getEquipSuggestEviction();
			Integer statusChange = filter.getStatus();
			if (equipSuggestEviction != null) {
				equipSuggestEviction.setUpdateDate(sysdate);
				
				// danh sach chi tiet
				EquipSuggestEvictionFilter<EquipSuggestEvictionDTL> evictionFilter = new EquipSuggestEvictionFilter<EquipSuggestEvictionDTL>();
				List<Long> lstId = new ArrayList<Long>();
				lstId.add(equipSuggestEviction.getId());
				evictionFilter.setLstObjectId(lstId);
				List<EquipSuggestEvictionDTL> lstDetails = equipSuggestEvictionDAO.getListEquipSuggestEvictionDetailByFilter(evictionFilter);				

				// cap nhat chi tiet
				if((StatusRecordsEquip.DRAFT.getValue().equals(equipSuggestEviction.getStatus()) && StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusChange))
					|| (StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipSuggestEviction.getStatus()) && StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusChange))
					|| (StatusRecordsEquip.DRAFT.getValue().equals(equipSuggestEviction.getStatus()) && StatusRecordsEquip.DRAFT.getValue().equals(statusChange)) 
					|| (StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipSuggestEviction.getStatus()) && StatusRecordsEquip.NO_APPROVAL.getValue().equals(statusChange))){
					if (filter.getLstEquipSuggestEvictionDTLs() != null && filter.getLstEquipSuggestEvictionDTLs().size() > 0) {
						List<EquipSuggestEvictionDTL> lstEquipSuggestEvictionDTLs = filter.getLstEquipSuggestEvictionDTLs();
						// xoa chi tiet
						for (int i = 0, sz = lstDetails.size(); i < sz; i++) {
							EquipSuggestEvictionDTL detail = lstDetails.get(i);
							Integer isExists = DELETE;
							for (int j = 0, szj = lstEquipSuggestEvictionDTLs.size(); j < szj; j++) {
								EquipSuggestEvictionDTL equipSuggestEvictionDTL = lstEquipSuggestEvictionDTLs.get(j);
								if (detail.getId().equals(equipSuggestEvictionDTL.getId())) {
									isExists = EXISTS;

									// sua chi tiet
									Boolean isChange = false;
									if (!detail.getCustomer().getId().equals(equipSuggestEvictionDTL.getCustomer().getId())) {
										detail.setCustomer(equipSuggestEvictionDTL.getCustomer());
										detail.setCustomerCode(equipSuggestEvictionDTL.getCustomerCode());
										detail.setCustomerName(equipSuggestEvictionDTL.getCustomerName());
										detail.setMobile(equipSuggestEvictionDTL.getMobile());
										isChange = true;
									}
									if (!detail.getEquipment().getId().equals(equipSuggestEvictionDTL.getEquipment().getId())) {
										Equipment equipment = equipSuggestEvictionDTL.getEquipment();
										Equipment equipOld = detail.getEquipment();

										detail.setRelation(equipSuggestEvictionDTL.getRelation());
										detail.setRefresentative(equipSuggestEvictionDTL.getRefresentative());
										detail.setHousenumber(equipSuggestEvictionDTL.getHousenumber());
										detail.setStreet(equipSuggestEvictionDTL.getStreet());
										detail.setWardName(equipSuggestEvictionDTL.getWardName());
										detail.setDistrictName(equipSuggestEvictionDTL.getDistrictName());
										detail.setProvinceName(equipSuggestEvictionDTL.getProvinceName());

										detail.setEquipCategoryId(equipment.getEquipGroup().getEquipCategory().getId());
										detail.setEquipCategoryName(equipment.getEquipGroup().getEquipCategory().getName());
										detail.setHealthStatus(equipment.getHealthStatus());

										detail.setEquipCode(equipment.getCode());
										detail.setEquipment(equipment);
										detail.setSerial(equipment.getSerial());
										isChange = true;
										// cap nhat trang thai thiet bi
										equipment = equipSuggestEvictionDTL.getEquipment();
										equipment.setUpdateDate(commonDAO.getSysDate());
										equipment.setUpdateUser(equipSuggestEviction.getCreateUser());
										equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
										equipment.setTradeType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
										commonDAO.updateEntity(equipment);
																				
										equipOld.setUpdateDate(commonDAO.getSysDate());
										equipOld.setUpdateUser(equipSuggestEviction.getCreateUser());
										equipOld.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
										equipOld.setTradeType(null);
										commonDAO.updateEntity(equipOld);
									}
									if (!detail.getShop().getId().equals(equipSuggestEvictionDTL.getShop().getId())) {
										detail.setShop(equipSuggestEvictionDTL.getShop());
										detail.setShopName(equipSuggestEvictionDTL.getShopName());
										detail.setRegion(equipSuggestEvictionDTL.getRegion());
										detail.setChannel(equipSuggestEvictionDTL.getChannel());
										isChange = true;
									}
									if (!detail.getObjectType().equals(equipSuggestEvictionDTL.getObjectType())) {
										detail.setObjectType(equipSuggestEvictionDTL.getObjectType());
										isChange = true;
									}
									if (!detail.getStaff().getId().equals(equipSuggestEvictionDTL.getStaff().getId())) {
										detail.setStaff(equipSuggestEvictionDTL.getStaff());
										detail.setStaffCode(equipSuggestEvictionDTL.getStaffCode());
										detail.setStaffName(equipSuggestEvictionDTL.getStaffName());
										detail.setPhone(equipSuggestEvictionDTL.getPhone());
										
										isChange = true;
									}
									if (DateUtility.compareDateWithoutTime(detail.getSuggestDate(), equipSuggestEvictionDTL.getSuggestDate()) != 0) {
										detail.setSuggestDate(equipSuggestEvictionDTL.getSuggestDate());
										isChange = true;
									}
									if (!detail.getSuggestReason().equals(equipSuggestEvictionDTL.getSuggestReason())) {
										detail.setSuggestReason(equipSuggestEvictionDTL.getSuggestReason());
										isChange = true;
									}

									if (isChange) {
										detail.setUpdateDate(commonDAO.getSysDate());
										commonDAO.updateEntity(detail);
									}
									break;
								}
							}
							if (isExists == DELETE) {
								commonDAO.deleteEntity(detail);
								// cap nhat trang thai thiet bi
								Equipment equipment = detail.getEquipment();
								equipment.setUpdateDate(commonDAO.getSysDate());
								equipment.setUpdateUser(equipSuggestEviction.getCreateUser());
								equipment.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
								equipment.setTradeType(null);
								commonDAO.updateEntity(equipment);
								
								lstDetails.remove(i);
								i--;
								sz--;
							}
						}
						// them chi tiet
						for (int i = 0, sz = lstEquipSuggestEvictionDTLs.size(); i < sz; i++) {
							EquipSuggestEvictionDTL equipSuggestEvictionDTL = lstEquipSuggestEvictionDTLs.get(i);
							Integer isExists = ADD;
							for (int j = 0, szj = lstDetails.size(); j < szj; j++) {
								EquipSuggestEvictionDTL detail = lstDetails.get(j);
								if (detail.getId().equals(equipSuggestEvictionDTL.getId())) {
									isExists = EXISTS;
									break;
								}
							}
							if (isExists == ADD) {
								equipSuggestEvictionDTL.setCreateDate(commonDAO.getSysDate());
								equipSuggestEvictionDTL.setEquipSugEviction(equipSuggestEviction);
								equipSuggestEvictionDTL = commonDAO.createEntity(equipSuggestEvictionDTL);
								lstDetails.add(equipSuggestEvictionDTL);
								
								Equipment equipment = equipSuggestEvictionDTL.getEquipment();
								equipment.setUpdateDate(commonDAO.getSysDate());
								equipment.setUpdateUser(equipSuggestEviction.getCreateUser());
								equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
								equipment.setTradeType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
								commonDAO.updateEntity(equipment);
							}
						}
					}
				}				
				
				// cho duỵet (chi xet tu Khong duyet -> Cho duyet)
				if (StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipSuggestEviction.getStatus())
					&& StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusChange)) {
					for (int i = 0, sz = lstDetails.size(); i < sz; i++) {
						EquipSuggestEvictionDTL detail = lstDetails.get(i);

						// thiet bi tren dong chi tiet
						Equipment equipment = detail.getEquipment();
						equipment.setUpdateDate(sysdate);
						equipment.setUpdateUser(equipSuggestEviction.getUpdateUser());
						equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
						equipment.setTradeType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
						commonDAO.updateEntity(equipment);
						// lich su thiet bi
						//createEquipHistory(equipment, equipSuggestEviction);				
					}
				}
				// huy ((chi xet tu Du thao -> Huy))
				if (StatusRecordsEquip.DRAFT.getValue().equals(equipSuggestEviction.getStatus())
					&& StatusRecordsEquip.CANCELLATION.getValue().equals(statusChange)) {										
					for (int i = 0, sz = lstDetails.size(); i < sz; i++) {
						EquipSuggestEvictionDTL detail = lstDetails.get(i);

						// thiet bi tren dong chi tiet
						Equipment equipment = detail.getEquipment();
						equipment.setUpdateDate(sysdate);
						equipment.setUpdateUser(equipSuggestEviction.getUpdateUser());
						equipment.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						equipment.setTradeType(null);
						commonDAO.updateEntity(equipment);
						// lich su thiet bi
						//createEquipHistory(equipment, equipSuggestEviction);
						
						// dong chi tiet
						detail.setUpdateDate(sysdate);
						detail.setUpdateUser(equipSuggestEviction.getUpdateUser());
						detail.setDeliveryStatus(null);
						commonDAO.updateEntity(detail);					
					}
				}
				// cho duyet -> duyet
				if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipSuggestEviction.getStatus())
					&& StatusRecordsEquip.APPROVED.getValue().equals(statusChange)) {
					if (equipSuggestEviction.getApproveDate() == null) {
						equipSuggestEviction.setApproveDate(sysdate);
						equipSuggestEviction.setDeliveryStatus(StatusDeliveryEquip.NOT_EXPORT.getValue());
					}
					
					for (int i = 0, sz = lstDetails.size(); i < sz; i++) {
						EquipSuggestEvictionDTL detail = lstDetails.get(i);

						// dong chi tiet
						detail.setUpdateDate(sysdate);
						detail.setUpdateUser(equipSuggestEviction.getUpdateUser());
						detail.setDeliveryStatus(StatusDeliveryEquip.NOT_EXPORT.getValue());
						commonDAO.updateEntity(detail);					
					}
				}
				// cho duyet -> khong duyet
				if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(equipSuggestEviction.getStatus())
					&& StatusRecordsEquip.NO_APPROVAL.getValue().equals(statusChange)) {										
					for (int i = 0, sz = lstDetails.size(); i < sz; i++) {
						EquipSuggestEvictionDTL detail = lstDetails.get(i);

						// thiet bi tren dong chi tiet
						Equipment equipment = detail.getEquipment();
						equipment.setUpdateDate(sysdate);
						equipment.setUpdateUser(equipSuggestEviction.getUpdateUser());
						equipment.setTradeStatus(EquipTradeStatus.NOT_TRADE.getValue());
						equipment.setTradeType(null);
						commonDAO.updateEntity(equipment);
						// lich su thiet bi
						//createEquipHistory(equipment, equipSuggestEviction);
						
						// dong chi tiet
						detail.setUpdateDate(sysdate);
						detail.setUpdateUser(equipSuggestEviction.getUpdateUser());
						detail.setDeliveryStatus(null);
						commonDAO.updateEntity(detail);					
					}
				}				
				
				// set trang thai
				equipSuggestEviction.setStatus(statusChange);
				commonDAO.updateEntity(equipSuggestEviction);
			}
			// luu lich su bien ban
			createEquipFormHistory(equipSuggestEviction);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	private EquipFormHistory createEquipFormHistory(EquipSuggestEviction equipSuggestEviction) throws BusinessException {
		try {
			if (equipSuggestEviction == null) {
				return null;
			}
			Date sysdate = commonDAO.getSysDate();
			// luu lich su bien ban
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(sysdate);
			equipFormHistory.setCreateDate(sysdate);
			if (StringUtility.isNullOrEmpty(equipSuggestEviction.getUpdateUser())) {
				equipFormHistory.setCreateUser(equipSuggestEviction.getCreateUser());
			} else {
				equipFormHistory.setCreateUser(equipSuggestEviction.getUpdateUser());
			}
			equipFormHistory.setDeliveryStatus(equipSuggestEviction.getDeliveryStatus());
			equipFormHistory.setRecordId(equipSuggestEviction.getId());
			equipFormHistory.setRecordStatus(equipSuggestEviction.getStatus());
			equipFormHistory.setRecordType(EquipTradeType.EQUIP_SUGGEST_EVICTION.getValue());
			Staff staff = staffDAO.getStaffByCode(equipSuggestEviction.getCreateUser());
			equipFormHistory.setStaff(staff);
			equipFormHistory = equipmentManagerDAO.createEquipmentHistoryRecord(equipFormHistory);

			return equipFormHistory;
		} catch (DataAccessException e) {
			throw new BusinessException();
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipSuggestEviction createEquipSuggestEvictionWithListDetail(EquipSuggestEviction equipSuggestEviction, List<EquipSuggestEvictionDTL> lstEquipSuggestEvictionDTLs) throws BusinessException {
		try {
			if (equipSuggestEviction == null) {
				return null;
			}
			String code = equipSuggestEvictionDAO.getCodeEquipSuggestEviction();
			equipSuggestEviction.setCode(code);
			equipSuggestEviction.setCreateDate(commonDAO.getSysDate());
			equipSuggestEviction = commonDAO.createEntity(equipSuggestEviction);
			// luu lich su bien ban
			createEquipFormHistory(equipSuggestEviction);
			if (lstEquipSuggestEvictionDTLs != null && lstEquipSuggestEvictionDTLs.size() > 0) {
				for (int i = 0, sz = lstEquipSuggestEvictionDTLs.size(); i < sz; i++) {
					EquipSuggestEvictionDTL equipSuggestEvictionDTL = lstEquipSuggestEvictionDTLs.get(i);
					equipSuggestEvictionDTL.setCreateDate(commonDAO.getSysDate());
					equipSuggestEvictionDTL.setEquipSugEviction(equipSuggestEviction);
					commonDAO.createEntity(equipSuggestEvictionDTL);
					
					Equipment equipment = equipSuggestEvictionDTL.getEquipment();
					equipment.setUpdateDate(commonDAO.getSysDate());
					equipment.setUpdateUser(equipSuggestEviction.getCreateUser());
					equipment.setTradeStatus(EquipTradeStatus.TRADING.getValue());
					equipment.setTradeType(EquipTradeType.WITHDRAWAL_LIQUIDATION);
					commonDAO.updateEntity(equipment);
				}
			}
			return equipSuggestEviction;
		} catch (DataAccessException e) {
			throw new BusinessException();
		}
	}
	
//	private EquipHistory createEquipHistory(Equipment equipment, EquipSuggestEviction equipSuggestEviction) throws BusinessException{
//		try{
//			Date sysdate = commonDAO.getSysDate();
//			// luu lich su bien ban
//			EquipHistory equipHistory = new EquipHistory();
//			equipHistory.setCreateDate(sysdate);
//			if(StringUtility.isNullOrEmpty(equipSuggestEviction.getUpdateUser())){
//				equipHistory.setCreateUser(equipSuggestEviction.getCreateUser());
//			} else {
//				equipHistory.setCreateUser(equipSuggestEviction.getUpdateUser());	
//			}
//			equipHistory.setEquip(equipment);
//			
//			equipHistory.setHealthStatus(equipment.getHealthStatus());
//			if (equipment.getStockId() != null) {
//				equipHistory.setObjectId(equipment.getStockId());
//			}
//			if (equipment.getStockType() != null) {
//				equipHistory.setObjectType(equipment.getStockType());
//			}
//			
//			equipHistory.setFormId(equipSuggestEviction.getId());
//			equipHistory.setFormType(EquipTradeType.EQUIP_SUGGEST_EVICTION);
//			equipHistory.setObjectCode(equipment.getStockCode());
//			equipHistory.setStockName(equipment.getStockName());
//			equipHistory.setTableName(TableName.EQUIP_DELIVERY_TABLE.getValue());
//			equipHistory.setPriceActually(equipment.getPriceActually());
//			equipHistory.setStatus(equipment.getStatus().getValue());
//			equipHistory.setTradeStatus(equipment.getTradeStatus());
//			equipHistory.setTradeType(equipment.getTradeType());
//			equipHistory.setUsageStatus(equipment.getUsageStatus());
//			equipHistory = equipmentManagerDAO.createEquipHistory(equipHistory);
//			
//			return equipHistory;
//		} catch (DataAccessException e) {
//			throw new BusinessException();
//		}
//	}

	@Override
	public List<EquipmentSuggestEvictionVO> getListEquipSuggestEvictionDetailVOForExportByFilter(EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO> filter) throws BusinessException {
		try {
			List<EquipSuggestEvictionDetailVO> lstEquipSuggestEvictionDetailVOs = equipSuggestEvictionDAO.getListEquipSuggestEvictionDetailVOByFilter(filter);
			if (lstEquipSuggestEvictionDetailVOs == null || lstEquipSuggestEvictionDetailVOs.size() == 0) {
				return null;
			}

			List<EquipmentSuggestEvictionVO> lstEquipmentSuggestEvictionVOs = new ArrayList<EquipmentSuggestEvictionVO>();
			EquipmentSuggestEvictionVO equipmentSuggestEvictionVO = new EquipmentSuggestEvictionVO();
			equipmentSuggestEvictionVO.setLstEquipSuggestEvictionDetailVOs(new ArrayList<EquipSuggestEvictionDetailVO>());
			equipmentSuggestEvictionVO.setId(lstEquipSuggestEvictionDetailVOs.get(0).getEquipSuggestEvictionId());
			equipmentSuggestEvictionVO.setCode(lstEquipSuggestEvictionDetailVOs.get(0).getEquipSuggestEvictionCode());
			equipmentSuggestEvictionVO.setTotalEquip(BigDecimal.ONE);
			equipmentSuggestEvictionVO.getLstEquipSuggestEvictionDetailVOs().add(lstEquipSuggestEvictionDetailVOs.get(0));
			for (int i = 1, sz = lstEquipSuggestEvictionDetailVOs.size(); i < sz; i++) {
				EquipSuggestEvictionDetailVO equipSuggestEvictionDetailVO = lstEquipSuggestEvictionDetailVOs.get(i);
				if (equipSuggestEvictionDetailVO.getEquipSuggestEvictionId().equals(equipmentSuggestEvictionVO.getId())) {
					equipmentSuggestEvictionVO.getLstEquipSuggestEvictionDetailVOs().add(equipSuggestEvictionDetailVO);
					equipmentSuggestEvictionVO.setTotalEquip(equipmentSuggestEvictionVO.getTotalEquip().add(BigDecimal.ONE));
				} else {
					lstEquipmentSuggestEvictionVOs.add(equipmentSuggestEvictionVO);
					// them moi
					equipmentSuggestEvictionVO = new EquipmentSuggestEvictionVO();
					equipmentSuggestEvictionVO.setLstEquipSuggestEvictionDetailVOs(new ArrayList<EquipSuggestEvictionDetailVO>());
					equipmentSuggestEvictionVO.setId(equipSuggestEvictionDetailVO.getEquipSuggestEvictionId());
					equipmentSuggestEvictionVO.setCode(equipSuggestEvictionDetailVO.getEquipSuggestEvictionCode());
					equipmentSuggestEvictionVO.setTotalEquip(BigDecimal.ONE);
					equipmentSuggestEvictionVO.getLstEquipSuggestEvictionDetailVOs().add(equipSuggestEvictionDetailVO);
				}
			}
			lstEquipmentSuggestEvictionVOs.add(equipmentSuggestEvictionVO);

			return lstEquipmentSuggestEvictionVOs;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<EquipSuggestEvictionDetailVO> getListEquipSuggestEvictionDetailVOByFilter(EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO> filter) throws BusinessException {
		try {
			List<EquipSuggestEvictionDetailVO> lst = equipSuggestEvictionDAO.getListEquipSuggestEvictionDetailVOByFilter(filter);	
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

package ths.dms.core.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.CustomerAttribute;
import ths.dms.core.entities.CustomerAttributeDetail;
import ths.dms.core.entities.CustomerAttributeEnum;
import ths.dms.core.entities.PromotionCustAttr;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductAttributeReqType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.CustomerAttributeDAO;
/**
 * @author liemtpt
 * @since 21/01/2015
 */
public class CustomerAttributeMgrImpl implements CustomerAttributeMgr {

	@Autowired
	CustomerAttributeDAO customerAttributeDAO;
	
	@Override
	public CustomerAttribute createCustomerAttribute(CustomerAttribute attribute,LogInfoVO logInfo) throws BusinessException {
		try {
			return customerAttributeDAO.createCustomerAttribute(attribute,logInfo);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteCustomerAttribute(CustomerAttribute attribute) throws BusinessException {
		try {
			customerAttributeDAO.deleteCustomerAttribute(attribute);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteCustomerAttributeEnum(CustomerAttributeEnum attribute) throws BusinessException {
		try {
			customerAttributeDAO.deleteCustomerAttributeEnum(attribute);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public CustomerAttribute getCustomerAttributeById(long id) throws BusinessException {
		try {
			return customerAttributeDAO.getCustomerAttributeById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public CustomerAttributeEnum getCustomerAttributeEnumById(long id) throws BusinessException {
		try {
			return customerAttributeDAO.getCustomerAttributeEnumById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public List<CustomerAttributeDetail> getListCustomerAttributeByEnumId(long id) throws BusinessException {
		try {
			return customerAttributeDAO.getListCustomerAttributeDetailByEnumId(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public void updateCustomerAttribute(CustomerAttribute attribute,LogInfoVO logInfo) throws BusinessException {
		try {
			customerAttributeDAO.updateCustomerAttribute(attribute,logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public void updateCustomerAttributeDetail(CustomerAttributeDetail attribute,LogInfoVO logInfo) throws BusinessException {
		try {
			customerAttributeDAO.updateCustomerAttributeDetail(attribute,logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public void updateCustomerAttributeEnum(CustomerAttributeEnum attribute,LogInfoVO logInfo) throws BusinessException {
		try {
			customerAttributeDAO.updateCustomerAttributeEnum(attribute,logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public ObjectVO<AttributeDynamicVO> getListCustomerAttributeVO(AttributeDynamicFilter filter, KPaging<AttributeDynamicVO> kPaging)
			throws BusinessException{
		try {
			ObjectVO<AttributeDynamicVO> res = new ObjectVO<AttributeDynamicVO>();
			List<AttributeDynamicVO> lst = customerAttributeDAO.getListCustomerAttributeVO(filter, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveOrUpdateCustomerAttribute(AttributeDynamicVO vo, LogInfoVO logInfo ) throws BusinessException{
		if(vo == null){
			throw new IllegalArgumentException("attributeDynamicVO is null");
		}
		try{
			if (vo.getAttributeId()!=null && vo.getAttributeId() >0L) {
				CustomerAttribute att = customerAttributeDAO.getCustomerAttributeById(vo.getAttributeId());
				if(att == null){
					throw new IllegalArgumentException("CustomerAttribute id is null");
				}
				att.setName(vo.getAttributeName());
				att.setMandatory(ProductAttributeReqType.parseValue(vo.getMandatory()));
				att.setDescription(vo.getDescription());
				att.setDisplayOrder(vo.getDisplayOrder());
				att.setStatus(ActiveType.parseValue(vo.getStatus()));
				if(vo.getType()!=null){
					if(AttributeColumnType.CHARACTER.getValue().equals(vo.getType())){
						att.setDataLength(vo.getDataLength());
					}else if(AttributeColumnType.NUMBER.getValue().equals(vo.getType())){
						att.setDataLength(vo.getDataLength());
						att.setMinValue(vo.getMinValue());
						att.setMaxValue(vo.getMaxValue());
					}
				}
				customerAttributeDAO.updateCustomerAttribute(att,logInfo);
			} else {
				CustomerAttribute att = new CustomerAttribute();
				att.setCode(vo.getAttributeCode());
				att.setName(vo.getAttributeName());
				att.setValueType(AttributeColumnType.parseValue(vo.getType()));
				att.setMandatory(ProductAttributeReqType.parseValue(vo.getMandatory()));
				att.setDescription(vo.getDescription());
				att.setDisplayOrder(vo.getDisplayOrder());
				att.setStatus(ActiveType.RUNNING);
				if(vo.getType()!=null){
					if(AttributeColumnType.CHARACTER.getValue().equals(vo.getType())){
						att.setDataLength(vo.getDataLength());
					}else if(AttributeColumnType.NUMBER.getValue().equals(vo.getType())){
						att.setDataLength(vo.getDataLength());
						att.setMinValue(vo.getMinValue());
						att.setMaxValue(vo.getMaxValue());
					}
				}
				att = customerAttributeDAO.createCustomerAttribute(att,logInfo);
			}
		}catch(DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor=Exception.class)
	public void saveOrUpdateCustomerAttributeEnum(AttributeEnumVO vo, LogInfoVO logInfo ) throws BusinessException{
		if(vo == null){
			throw new IllegalArgumentException("attributeEnumVO is null");
		}
		try{
			CustomerAttribute att = customerAttributeDAO.getCustomerAttributeById(vo.getAttributeId());
			CustomerAttributeEnum attEnum = customerAttributeDAO.getCustomerAttributeEnumByCode(vo.getEnumCode());
			if(attEnum!=null){
				attEnum.setValue(vo.getEnumValue());
				customerAttributeDAO.updateCustomerAttributeEnum(attEnum,logInfo);
			}else{
				attEnum = new CustomerAttributeEnum();
				attEnum.setCode(vo.getEnumCode().toUpperCase());
				attEnum.setValue(vo.getEnumValue());
				attEnum.setCustomerAttribute(att);
				attEnum.setStatus(ActiveType.RUNNING);
				attEnum = customerAttributeDAO.createCustomerAttributeEnum(attEnum,logInfo);
			}
		}catch(DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkExistPromotion(Long objectId) throws BusinessException {
		try {
			Boolean bool = false;
			PromotionCustAttr pca = customerAttributeDAO.getPromotionCustAttrByObjectId(objectId);
			if(pca!=null){
				bool = true;
			}
			return bool;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}
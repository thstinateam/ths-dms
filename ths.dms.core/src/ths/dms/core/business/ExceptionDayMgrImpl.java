package ths.dms.core.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ExceptionDay;
import ths.dms.core.entities.SaleDays;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.WorkingDateConfig;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.WorkingDateConfigFilter;
import ths.dms.core.entities.enumtype.WorkingDateProcedureType;
import ths.dms.core.entities.enumtype.WorkingDateType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.WorkDateTempVO;
import ths.dms.core.entities.vo.WorkReasonVO;
import ths.dms.core.entities.vo.WorkingDateConfigVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.ExceptionDayDAO;
import ths.dms.core.dao.SaleDayDAO;
import ths.dms.core.dao.ShopDAO;

public class ExceptionDayMgrImpl implements ExceptionDayMgr {

	@Autowired
	ExceptionDayDAO exceptionDayDAO;

	@Autowired
	SaleDayDAO saleDayDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	ShopDAO shopDAO;

	@Override
	public List<ExceptionDay> getExceptionDayByYear(int year)
			throws BusinessException {
		try {
			return exceptionDayDAO.getExceptionDayByYear(year);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<ExceptionDay> getExceptionDayByMonth(int month, int year)
			throws BusinessException {
		try {
			return exceptionDayDAO.getExceptionDayByMonth(month, year);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	public void addSaleDays(int year, LogInfoVO logInfo)
			throws BusinessException {

		SaleDays currSaleDay = new SaleDays();

		currSaleDay = new SaleDays();

		//currSaleDay.setStatus(ActiveType.RUNNING);
		currSaleDay.setT1(DateUtility.getWorkingDateInMonth(1, year));
		currSaleDay.setT2(DateUtility.getWorkingDateInMonth(2, year));
		currSaleDay.setT3(DateUtility.getWorkingDateInMonth(3, year));
		currSaleDay.setT4(DateUtility.getWorkingDateInMonth(4, year));
		currSaleDay.setT5(DateUtility.getWorkingDateInMonth(5, year));
		currSaleDay.setT6(DateUtility.getWorkingDateInMonth(6, year));
		currSaleDay.setT7(DateUtility.getWorkingDateInMonth(7, year));
		currSaleDay.setT8(DateUtility.getWorkingDateInMonth(8, year));
		currSaleDay.setT9(DateUtility.getWorkingDateInMonth(9, year));
		currSaleDay.setT10(DateUtility.getWorkingDateInMonth(10, year));
		currSaleDay.setT11(DateUtility.getWorkingDateInMonth(11, year));
		currSaleDay.setT12(DateUtility.getWorkingDateInMonth(12, year));
		//currSaleDay.setYear("" + year);

		try {
			saleDayDAO.createSaleDay(currSaleDay, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * 
	 * @param exceptionDay
	 * @param logInfo
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	public ExceptionDay addExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo) throws BusinessException {
		try {
			Calendar cel = Calendar.getInstance();
			cel.setTime(exceptionDay.getDayOff());

			int year = cel.get(Calendar.YEAR);
			int month = cel.get(Calendar.MONTH) + 1;
			int date = cel.get(Calendar.DATE);
			
			ExceptionDay newExDay = exceptionDayDAO.getExceptionDayByDate(exceptionDay.getDayOff(),exceptionDay.getShop().getId());
			
			if(null != newExDay){
				throw new BusinessException("Have exception date in DB");
			}

			newExDay = exceptionDayDAO.createExceptionDay(exceptionDay, logInfo);

			// khong phai chu nhat thi con them 1 ngay vao saleDay.
			/*if (cel.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				SaleDays saleDay = saleDayDAO.getSaleDayForUpdate(year);

				if (null != saleDay) {
					int saleDayInMonth = saleDayDAO.getSaleDayByYear(year, month);

					if (saleDayInMonth > 0) {
						saleDayDAO.addSaleDay(month, year, -1, logInfo);
					} else {
						saleDayDAO.addSaleDay(month, year, 0, logInfo);
					}
				} else {
					throw new BusinessException("Can't get saleDay of year "
							+ year + ", or saleDay is invalidate.");
				}
			}*/

			return newExDay;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public void removeExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo)
			throws BusinessException {
		try {
			Calendar cel = Calendar.getInstance();
			cel.setTime(exceptionDay.getDayOff());
			int year = cel.get(Calendar.YEAR);
			int month = cel.get(Calendar.MONTH) + 1;

			exceptionDayDAO.deleteExceptionDay(exceptionDay, logInfo);

			// khong phai chu nhat thi con tru 1 ngay vao saleDay.
//			if (cel.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				/*SaleDays saleDay = saleDayDAO.getSaleDayForUpdate(year);

				if (null != saleDay) {
					saleDayDAO.addSaleDay(month, year, 1, logInfo);
				} else {
					throw new BusinessException("Can't get saleDay of year "
							+ year + ", or saleDay is invalidate.");
				}*/
//			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public ExceptionDay createExceptionDay(ExceptionDay exceptionDay,
			LogInfoVO logInfo) throws BusinessException {
		Calendar cel = Calendar.getInstance();
		cel.setTime(exceptionDay.getDayOff());
		int year = cel.get(Calendar.YEAR);

		// neu chua co du lieu cua nam thi them moi vao.
			/*SaleDays currSaleDay;
			try {
				currSaleDay = saleDayDAO.getSaleDayByYear(year);
				if (null == currSaleDay) {
					addSaleDays(year, logInfo);
				}
			} catch (DataAccessException e) {
				throw new BusinessException(e);
			}*/
		return this.addExceptionDay(exceptionDay, logInfo);
	}

	@Override
	public void updateExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo)
			throws BusinessException {
		try {
			exceptionDayDAO.updateExceptionDay(exceptionDay, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo)
			throws BusinessException {
		// try {
		// exceptionDayDAO.deleteExceptionDay(exceptionDay, logInfo);
		// } catch (DataAccessException e) {
		// throw new BusinessException(e);
		// }
		this.removeExceptionDay(exceptionDay, logInfo);
	}

	@Override
	public ExceptionDay getExceptionDayById(Long id) throws BusinessException {
		try {
			return id == null ? null : exceptionDayDAO.getExceptionDayById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ExceptionDay getExceptionDayByDate(Date date,Long shopId)
			throws BusinessException {
		try {
			return date == null ? null : exceptionDayDAO
					.getExceptionDayByDate(date,shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ExceptionDay> getExceptionDayByMonth(int month, int year,Long shopId)
			throws BusinessException {
		try {
			return exceptionDayDAO.getExceptionDayByMonth(month, year,shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ExceptionDay> getExceptionDayByYear(int year,Long shopId)
			throws BusinessException {
		try {
			return exceptionDayDAO.getExceptionDayByYear(year,shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Integer getNumExceptionByDateInMonth(Date toDate)
			throws BusinessException {
		try {
			return exceptionDayDAO.getNumExceptionByDateInMonth(toDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Integer getNumOfHoliday(Date fromDate, Date toDate)
			throws BusinessException {
		try {
			return exceptionDayDAO.getNumOfHoliday(fromDate, toDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Integer getNumOfHoliday(Date fromDate, Date toDate, Long shopId)
			throws BusinessException {
		try {
			return exceptionDayDAO.getNumOfHoliday(fromDate, toDate, shopId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<WorkingDateConfig> getListWorkingDateConfigByFilter(WorkingDateConfigFilter filter)
			throws BusinessException {
		try {
			return exceptionDayDAO.getListWorkingDateConfigByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Shop getShopParentWorkingDateConfig(Long shopId,WorkingDateType type)
			throws BusinessException {
		try {
			return shopDAO.getShopParentWorkingDateConfig(shopId,type);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
   @Override
	public void callProceduceWorkingDate(Long shopId, Date fromDate, Date toDate,
		WorkingDateProcedureType type) throws BusinessException {
		try {
			exceptionDayDAO.callProceduceWorkingDate(shopId, fromDate, toDate, type);
		} catch (DataAccessException e) {
				throw new BusinessException(e);
		}
   }
   @Override
	@Transactional(rollbackFor=Exception.class)
	public int inheritWorkingDate(Shop shop, LogInfoVO logInfo) throws BusinessException{
		if(shop == null){
			throw new IllegalArgumentException("shop is null");
		}
		Integer value = 0;
		try{
			WorkingDateConfig workingDate = exceptionDayDAO.getWorkDateConfigByShopId(shop.getId(), ActiveType.RUNNING);
			/** kiem tra shop dang su dung la lich rieng: 1 hay khong*/
			if(workingDate != null && WorkingDateType.BAN_THAN_DON_VI.equals(workingDate.getType())){
				/***>> Bảng CONFIG: Update dòng cũ của shop có status = 1: Set TO_DATE = tháng hiện tại; Set Status = 0 (nếu có dòng cũ).*/
				Date sys = commonDAO.getSysDate();
				workingDate.setToDate(sys);
				workingDate.setStatus(ActiveType.STOPPED.getValue());
				commonDAO.updateEntity(workingDate);
				/***>> Bảng CONFIG: Thêm dòng mới cho shop: FROM_DATE = Tháng hiện tại + 1; TO_DATE = NULL; Cờ = Sử dụng lịch riêng; Status = 1;*/
				WorkingDateConfig workingDateNew = new WorkingDateConfig();
				Date sysNext = DateUtility.getFirstNextMonth(sys);
				workingDateNew.setShop(shop);
				workingDateNew.setFromDate(sysNext);
				workingDateNew.setType(WorkingDateType.DE_QUY);
				workingDateNew.setStatus(ActiveType.RUNNING.getValue());
				commonDAO.createEntity(workingDateNew);
				/***>> Bảng EXCEPTION: Xóa thông tin EXCEPTION_DAY của shop chọn từ Tháng hiện tại + 1 trở đi.**/
				List<ExceptionDay> lstDeleteFromMonth = exceptionDayDAO.getListByShopIdAndFromDateToDate(shop.getId(), sysNext, null);
				if(lstDeleteFromMonth != null){
					for (ExceptionDay exceptionDay : lstDeleteFromMonth) {
						commonDAO.deleteEntity(exceptionDay);
					}
				}
				/***>> Bảng EXCEPTION: Copy thông tin EXCEPTION_DAY của shop cha sang shop chọn từ Tháng hiện tại + 1 trở đi.**/
				List<WorkingDateConfigVO> lstWorkCheckParent =  exceptionDayDAO.getWorkDateConfigDateCheckParent(shop.getId(), WorkingDateType.BAN_THAN_DON_VI, ActiveType.RUNNING);
				if(lstWorkCheckParent != null && lstWorkCheckParent.size() > 0){
					Long parentShopId = lstWorkCheckParent.get(0).getShopId();
					List<ExceptionDay> lstCopyParent = exceptionDayDAO.getListByShopIdAndFromDateToDate(parentShopId, sysNext, null);
					this.createListExceptionDayCopy(lstCopyParent, shop, logInfo, sys);
					
				}
				/***>> Kích hoạt STORE PROCEDURE: Truyền vào: Shop_ID chọn, From = Tháng hiện tại + 1, To = NULL, Type = 2.*/
				//exceptionDayDAO.callProceduceWorkingDate(shop.getId(), sysNext, null, WorkingDateProcedureType.THUA_KE_CHA);
				value = WorkingDateType.BAN_THAN_DON_VI.getValue();
			}
		}catch(DataAccessException e) {
			throw new BusinessException(e);
		}
		return value;
	}
	/****************************BEGIN VUONGMQ*********************/
	
	/**
	 * @author vuongmq
	 * @date 30/01/2015
	 * @description: thiet lap lich lam viec
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int createWorkDate(Shop shop, Integer type, String fromMonth, String toMonth, List<WorkDateTempVO> lstWorkDate, List<Integer> lstDefaultWork, LogInfoVO logInfoVO)
			throws BusinessException {
		Integer value = 0;
		try {
			/** kiem tra shop dang su dung la lich cha: 2 hay lich rieng: 1*/
			WorkingDateConfig workingDate = exceptionDayDAO.getWorkDateConfigByShopId(shop.getId(), ActiveType.RUNNING);
			if(workingDate == null || (workingDate != null && WorkingDateType.DE_QUY.equals(workingDate.getType())) ){
				/** +Nếu đang sử dụng lịch theo lịch cha:*/
				/**>> Bảng CONFIG: Update dòng cũ của shop có status = 1: Set TO_DATE = tháng hiện tại; Set Status = 0 (nếu có dòng cũ).*/
				Date sys = commonDAO.getSysDate();
				if(workingDate != null){
					workingDate.setToDate(sys);
					workingDate.setStatus(ActiveType.STOPPED.getValue());
					commonDAO.updateEntity(workingDate);
				}
				/**  >> Bảng CONFIG: Thêm dòng mới cho shop: FROM_DATE = Tháng hiện tại + 1; TO_DATE = NULL; Cờ = Sử dụng lịch riêng; Status = 1;*/
				WorkingDateConfig workingDateNew = new WorkingDateConfig();
				Date sysNext = DateUtility.getFirstNextMonth(sys);
				workingDateNew.setShop(shop);
				workingDateNew.setFromDate(sysNext);
				workingDateNew.setType(WorkingDateType.BAN_THAN_DON_VI);
				workingDateNew.setStatus(ActiveType.RUNNING.getValue());
				commonDAO.createEntity(workingDateNew);
				/** >> Bảng EXCEPTION: Tìm tất cả các dòng từ Tháng bắt đầu thiết lập trở đi --> Xóa. **/
				Date fromMonthDate = DateUtility.parse("01/"+ fromMonth, DateUtility.DATE_FORMAT_STR);
				List<ExceptionDay> lstDeleteFromMonth = exceptionDayDAO.getListByShopIdAndFromDateToDate(shop.getId(), fromMonthDate, null);
				if(lstDeleteFromMonth != null){
					for (ExceptionDay exceptionDay : lstDeleteFromMonth) {
						commonDAO.deleteEntity(exceptionDay);
					}
				}
				/**Chạy cấu hình (xem miêu tả bên dưới) từ Tháng bắt đầu thiết lập đến Tháng kết thúc thiết lập. **/
				Date toMonthView = DateUtility.parse("01/"+ toMonth, DateUtility.DATE_FORMAT_STR); // ngay dau thang
				Date toMonthDate = DateUtility.getLastDateInMonth(toMonthView); // ngay cuoi thang
				if(type == 1){
					/** thiet lap lich rieng*/
					/** cap nhat sach sach ngay nghi exception_day*/
					this.runConfigExceptionDay(fromMonthDate, toMonthDate, lstDefaultWork, lstWorkDate, shop, logInfoVO, sys);
				} else {
					/** copy thiet lap tu cha*/
					this.copyParentWorkingDate(shop, fromMonthDate, toMonthView, logInfoVO, sys);
				} // end if(type == 1)
	            /**>> Kích hoạt STORE PROCEDURE: Truyền vào: Shop_ID chọn, From = Tháng bắt đầu thiết lập, To = Tháng kết thúc thiết lập, Type = 1 **/
				//exceptionDayDAO.callProceduceWorkingDate(shop.getId(), fromMonthDate, toMonthDate, WorkingDateProcedureType.THIET_LAP_RIENG);
				value = WorkingDateType.DE_QUY.getValue();
				
			} else if(workingDate != null && WorkingDateType.BAN_THAN_DON_VI.equals(workingDate.getType())){
				/** + Nếu đang sử dụng lịch riêng:*/
				/**>> Bảng EXCEPTION: Tìm tất cả các dòng từ Tháng bắt đầu thiết lập đến Tháng kết thúc thiết lập --> Xóa. **/
				Date fromMonthDate = DateUtility.parse("01/"+ fromMonth, DateUtility.DATE_FORMAT_STR);
				Date toMonthView = DateUtility.parse("01/"+ toMonth, DateUtility.DATE_FORMAT_STR); // lay ngay dau cua thang de lay sql
				List<ExceptionDay> lstDeleteMonth = exceptionDayDAO.getListByShopIdAndFromDateToDate(shop.getId(), fromMonthDate, toMonthView);
				if(lstDeleteMonth != null){
					for (ExceptionDay exceptionDay : lstDeleteMonth) {
						commonDAO.deleteEntity(exceptionDay);
					}
				}
                /**Chạy cấu hình (xem miêu tả bên dưới) từ Tháng bắt đầu thiết lập đến Tháng kết thúc thiết lập. **/
				Date toMonthDate = DateUtility.getLastDateInMonth(toMonthView); // lay ngay cuoi thang de lay sanh sach ngay lam viec
				Date sys = commonDAO.getSysDate();
				if(type == 1){
					/** thiet lap lich rieng*/
					/** cap nhat sach sach ngay nghi exception_day*/
					this.runConfigExceptionDay(fromMonthDate, toMonthDate, lstDefaultWork, lstWorkDate, shop, logInfoVO, sys);
				} else {
					/** copy thiet lap tu cha*/
					this.copyParentWorkingDate(shop, fromMonthDate, toMonthView, logInfoVO, sys);
				}
                /**>> Kích hoạt STORE PROCEDURE: Truyền vào: Shop_ID chọn, From = Tháng bắt đầu thiết lập, To = Tháng kết thúc thiết lập, Type = 2 **/
				//exceptionDayDAO.callProceduceWorkingDate(shop.getId(), fromMonthDate, toMonthDate, WorkingDateProcedureType.THUA_KE_CHA);
				value = WorkingDateType.BAN_THAN_DON_VI.getValue();
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return value;
	}
	/***
	 * @author vuongmq
	 * @date 30/01/2015
	 * @description chay danh sach ngay nghi EXCEPTION_DAY
	 * @throws BusinessException
	 */
	private void runConfigExceptionDay(Date fromMonthDate, Date toMonthDate, List<Integer> lstDefaultWork, List<WorkDateTempVO> lstWorkDate, Shop shop, LogInfoVO logInfoVO, Date sys)
			throws BusinessException {
		try {
			/**Cay thiet lap, lay danh sach ngay nghi*/
			List<WorkReasonVO> dateTemp = DateUtility.getHolidaysTwoDate(fromMonthDate, toMonthDate, lstDefaultWork, lstWorkDate);
			/**Add vao bang exception_day;  danh sach ngay nghi*/
			if(dateTemp != null && dateTemp.size() > 0){
				List<ExceptionDay> lstExceptionDay = new ArrayList<ExceptionDay>();
				for (WorkReasonVO workReasonVO : dateTemp) {
					ExceptionDay eDay = new ExceptionDay();
					eDay.setDayOff(workReasonVO.getDayOff());
					if(workReasonVO.getReason() != null){
						eDay.setReason(workReasonVO.getReason());
					}
					eDay.setShop(shop);
					eDay.setCreateDate(sys);
					eDay.setCreateUser(logInfoVO.getStaffCode());
					lstExceptionDay.add(eDay);
				}
				if(lstExceptionDay != null && lstExceptionDay.size() > 0){
					commonDAO.creatListEntity(lstExceptionDay);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/***
	 * @author vuongmq
	 * @date 30/01/2015
	 * @description copy shop cha co thiet lap rieng
	 * @throws BusinessException
	 */
	private void copyParentWorkingDate(Shop shop,Date fromMonthDate, Date toMonthView, LogInfoVO logInfoVO, Date sys)
			throws BusinessException {
		try {
			List<WorkingDateConfigVO> lstWorkCheckParent =  exceptionDayDAO.getWorkDateConfigDateCheckParent(shop.getId(), WorkingDateType.BAN_THAN_DON_VI, ActiveType.RUNNING);
			if(lstWorkCheckParent != null && lstWorkCheckParent.size() > 0){
				/** Copy EXCEPTION voi cha co cờ thiết lập riêng*/
				Long shopIdCopy = lstWorkCheckParent.get(0).getShopId();
				List<ExceptionDay> lstExceptionDayCopy =  exceptionDayDAO.getListByShopIdAndFromDateToDate(shopIdCopy, fromMonthDate, toMonthView);
				this.createListExceptionDayCopy(lstExceptionDayCopy, shop, logInfoVO, sys);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/***
	 * @author vuongmq
	 * @date 30/01/2015
	 * @description copy danh sach exceptionDay
	 * @throws BusinessException
	 */
	private void createListExceptionDayCopy(List<ExceptionDay> lstExceptionDayCopy, Shop shop, LogInfoVO logInfoVO, Date sys)
		throws BusinessException {
		try {
			List<ExceptionDay> lstExceptionNew = new ArrayList<ExceptionDay>();
			if(lstExceptionDayCopy != null){
				for (ExceptionDay exceptionDay : lstExceptionDayCopy) {
					ExceptionDay tmpEntity = new ExceptionDay();
					tmpEntity.setCreateDate(sys);
					tmpEntity.setCreateUser(logInfoVO.getStaffCode());
					tmpEntity.setDayOff(exceptionDay.getDayOff());
					tmpEntity.setReason(exceptionDay.getReason());
					tmpEntity.setShop(shop);
					lstExceptionNew.add(tmpEntity);
				}
			}
			if(lstExceptionNew != null && lstExceptionNew.size() > 0){
				commonDAO.creatListEntity(lstExceptionNew);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<WorkingDateConfigVO> getWorkDateConfigDateCheckParent(Long shopId, WorkingDateType type, ActiveType active) throws BusinessException {
		try {
			return exceptionDayDAO.getWorkDateConfigDateCheckParent(shopId, type, active);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/****************************END VUONGMQ*********************/

	@Override
	public Integer createWorkDateCopyShopParent(Shop newShop, WorkingDateType chiDinhDonVi, String fromMonth, Object object, Object object2, Object object3, LogInfoVO logInfoVO) throws BusinessException {
		Integer value = 0;
		try {
			/** kiem tra shop dang su dung la lich cha: 2 hay lich rieng: 1 */
			WorkingDateConfig workingDate = exceptionDayDAO.getWorkDateConfigByShopId(newShop.getId(), ActiveType.RUNNING);
			if (workingDate == null || (workingDate != null && WorkingDateType.DE_QUY.equals(workingDate.getType()))) {
				/** +Nếu đang sử dụng lịch theo lịch cha: */
				/**
				 * >> Bảng CONFIG: Update dòng cũ của shop có status = 1: Set
				 * TO_DATE = tháng hiện tại; Set Status = 0 (nếu có dòng cũ).
				 */
				Date sys = commonDAO.getSysDate();
				if (workingDate != null) {
					workingDate.setToDate(sys);
					workingDate.setStatus(ActiveType.STOPPED.getValue());
					commonDAO.updateEntity(workingDate);
				}
				/**
				 * >> Bảng CONFIG: Thêm dòng mới cho shop: FROM_DATE = Tháng
				 * hiện tại + 1; TO_DATE = NULL; Cờ = Sử dụng lịch riêng; Status
				 * = 1;
				 */
				WorkingDateConfig workingDateNew = new WorkingDateConfig();
				Date sysNext = DateUtility.getFirstNextMonth(sys);
				workingDateNew.setShop(newShop);
				workingDateNew.setBaseOnShop(null);
				workingDateNew.setFromDate(sys);
				workingDateNew.setType(WorkingDateType.DE_QUY);
				workingDateNew.setStatus(ActiveType.RUNNING.getValue());
				commonDAO.createEntity(workingDateNew);
				/** copy thiet lap tu cha */
				List<WorkingDateConfigVO> lstWorkCheckParent = exceptionDayDAO.getWorkDateConfigDateCheckParent(newShop.getId(), WorkingDateType.BAN_THAN_DON_VI, ActiveType.RUNNING);
				if (lstWorkCheckParent != null && lstWorkCheckParent.size() > 0) {
					/** Copy EXCEPTION voi cha co cờ thiết lập riêng */
					Long shopIdCopy = lstWorkCheckParent.get(0).getShopId();
					List<ExceptionDay> lstExceptionDayCopy = exceptionDayDAO.getListByShopIdAndFromDateToDate(shopIdCopy, fromMonth);
					this.createListExceptionDayCopy(lstExceptionDayCopy, newShop, logInfoVO, sys);
				}
				/*
				 * this.copyParentWorkingDate(newShop, fromMonth, null,
				 * logInfoVO, sys);
				 */
				value = WorkingDateType.DE_QUY.getValue();

			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return value;
	}
}

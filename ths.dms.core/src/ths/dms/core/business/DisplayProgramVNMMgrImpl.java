package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.DisplayProgramVNM;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.DisplayProgramVNMDAO;

public class DisplayProgramVNMMgrImpl implements DisplayProgramVNMMgr{
	@Autowired DisplayProgramVNMDAO displayProgramVNMDAO;
	@Override
	public DisplayProgramVNM getDisplayProgramVNMByID(Long id)
			throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return displayProgramVNMDAO.getDisplayProgramVNMByID(id);
		} catch (DataAccessException e) {
			// TODO: handle exception
			throw new BusinessException(e);
		}
		
	}

	@Override
	public DisplayProgramVNM getDisplayProgramVNMByCode(String code)
			throws BusinessException {
		// TODO Auto-generated method stub
		try {
			return displayProgramVNMDAO.getDisplayProgramVNMByCode(code);
		} catch (DataAccessException e) {
			// TODO: handle exception
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<DisplayProgramVNM> getListDisplayProgramVNM(
			KPaging<DisplayProgramVNM> kPaging, Long shopId, String code,
			String name, ActiveType status, Date fDate, Date tDate)
			throws BusinessException {
		// TODO Auto-generated method stub
		try {
			ObjectVO<DisplayProgramVNM> vo = new ObjectVO<DisplayProgramVNM>();
			List<DisplayProgramVNM> object = displayProgramVNMDAO.getListDisplayProgramVNM(kPaging, shopId, code, name, status, fDate, tDate);
			vo.setkPaging(kPaging);
			vo.setLstObject(object);
			return vo;
		} catch (DataAccessException e) {
			// TODO: handle exception
			throw new BusinessException(e);
		}
	}

}

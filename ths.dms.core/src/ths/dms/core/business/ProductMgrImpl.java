package ths.dms.core.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.MediaThumbnail;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.PriceSaleIn;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductAttribute;
import ths.dms.core.entities.ProductAttributeDetail;
import ths.dms.core.entities.ProductAttributeEnum;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.ProductIntroduction;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.enumtype.MediaType;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.enumtype.ProductFilter;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.ProgramObjectType;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.TreeVOType;
import ths.dms.core.entities.filter.PriceFilter;
import ths.dms.core.entities.filter.ProductGeneralFilter;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.entities.vo.GeneralProductVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.entities.vo.PriceVO;
import ths.dms.core.entities.vo.ProductExportVO;
import ths.dms.core.entities.vo.ProductTreeVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.entities.vo.ProductVOEx;
import ths.dms.core.entities.vo.ProductsForPoAutoParams;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.MediaItemDAO;
import ths.dms.core.dao.MediaThumbnailDAO;
import ths.dms.core.dao.PriceDAO;
import ths.dms.core.dao.PriceSaleInDAO;
import ths.dms.core.dao.ProductAttributeDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.ProductInfoDAO;
import ths.dms.core.dao.ProductIntroductionDAO;
import ths.dms.core.dao.ProductLotDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.StockTotalDAO;
import ths.dms.core.dao.StockTransDAO;
import ths.dms.core.dao.StockTransDetailDAO;
import ths.dms.core.dao.WareHouseDAO;

public class ProductMgrImpl implements ProductMgr {

	@Autowired
	ProductDAO productDAO;

	@Autowired
	ProductInfoDAO productInfoDAO;

	@Autowired
	ProductIntroductionDAO productIntroductionDAO;

	@Autowired
	MediaItemDAO mediaItemDAO;

	@Autowired
	MediaThumbnailDAO mediaThumbnailDAO;

	@Autowired
	ProductLotDAO productLotDAO;

	@Autowired
	PriceDAO priceDAO;
	
	@Autowired
	PriceSaleInDAO priceSaleInDAO;

	@Autowired
	StockTransDAO stockTransDAO;

	@Autowired
	StockTotalDAO stockTotalDAO;

	@Autowired
	StockTransDetailDAO stockTransDetailDAO;

	@Autowired
	CommonMgr commonMgr;
	
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	ProductAttributeDAO productAttributeDAO;
	
	@Autowired
	WareHouseDAO wareHouseDAO;
	
	@Autowired
	ShopDAO shopDAO;
	
	@Autowired
	CommonBusinessHelper commonBusinessHelper;

	@Override
	public Product createProduct(Product product, LogInfoVO logInfo) throws BusinessException {
		try {
			this.increaseOrderIndexProduct(product);
			return productDAO.createProduct(product, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void updateProduct(Product product, LogInfoVO logInfo) throws BusinessException {
		try {
			this.increaseOrderIndexProduct(product);
			productDAO.updateProduct(product, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteProduct(Product product, LogInfoVO logInfo) throws BusinessException {
		try {
			productDAO.deleteProduct(product, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Product getProductById(Long id) throws BusinessException {
		try {
			return id == null ? null : productDAO.getProductById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Product getProductByCode(String code) throws BusinessException {
		try {
			return productDAO.getProductByCode(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Product getProductByCode(String code, Integer status) throws BusinessException {
		try {
			return productDAO.getProductByCode(code, status);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public Product getProductByFilter(ProductFilter filter) throws BusinessException {
		try {
			return productDAO.getProductByFilter(filter);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Product getProductByCodeExceptZ(String code, Long staffId) throws BusinessException {
		try {
			return productDAO.getProductByCodeExceptZ(code, staffId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Product> getListProductEx(ProductFilter filter) throws BusinessException {
		try {
			List<Product> lst = productDAO.getListProduct(filter);
			ObjectVO<Product> vo = new ObjectVO<Product>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Product> getAllListProductEx(ProductFilter filter) throws BusinessException {
		try {
			List<Product> lst = productDAO.getAllListProduct(filter);
			ObjectVO<Product> vo = new ObjectVO<Product>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Product> getListProductExceptZ(KPaging<Product> paging, String productCode, String productName, Long staffId) throws BusinessException {
		try {
			List<Product> lst = productDAO.getListProductExceptZ(paging, productCode, productName, staffId);
			ObjectVO<Product> vo = new ObjectVO<Product>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	//-----------------------------------Price-------------------------------

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.ProductMgr#getListProduct(ths.dms.core.
	 * entities.enumtype.KPaging, ths.dms.core.entities.enumtype.ActiveType)
	 */
	@Override
	public ObjectVO<Product> getListProduct(KPaging<Product> kPaging, ActiveType status) throws BusinessException {
		try {
			ProductFilter filter = new ProductFilter();
			filter.setkPaging(kPaging);
			filter.setStatus(status);
			List<Product> lst = productDAO.getListProduct(filter);
			ObjectVO<Product> vo = new ObjectVO<Product>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	//-----------------------------------------------------------

	@Override
	public Price getPriceByProductId(Long shopId, Long productId) throws BusinessException {
		try {
			return priceDAO.getPriceByProductId(shopId, productId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Price getPriceByProductId(Long productId) throws BusinessException {
		try {
			return priceDAO.getPriceByProductId(productId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.ProductMgr#getListProductAddSkuForSalePlan(
	 * ths.dms.core.entities.enumtype.KPaging, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public ObjectVO<ProductVO> getListProductAddSkuForSalePlan(KPaging<ProductVO> kPaging, String productCode, String productName, Integer month, Integer year) throws BusinessException {
		try {
			ObjectVO<ProductVO> objectVO = new ObjectVO<ProductVO>();
			List<ProductVO> list = productDAO.getListProductAddSkuSalePlan(kPaging, productCode, productName, month, year);
			objectVO.setLstObject(list);
			objectVO.setkPaging(kPaging);
			return objectVO;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ProductIntroduction createProductIntroduction(ProductIntroduction productIntroduction, LogInfoVO logInfo) throws BusinessException {
		try {
			return productIntroductionDAO.createProductIntroduction(productIntroduction, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void updateProductIntroduction(ProductIntroduction productIntroduction, LogInfoVO logInfo) throws BusinessException {
		try {
			productIntroductionDAO.updateProductIntroduction(productIntroduction, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteProductIntroduction(ProductIntroduction productIntroduction, LogInfoVO logInfo) throws BusinessException {
		try {
			productIntroductionDAO.deleteProductIntroduction(productIntroduction, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ProductIntroduction getProductIntroductionById(Long id) throws BusinessException {
		try {
			return id == null ? null : productIntroductionDAO.getProductIntroductionById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public MediaItem createMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws BusinessException {
		try {
			return mediaItemDAO.createMediaItem(mediaItem, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	//	@Override
	//	public void updateMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws BusinessException {
	//		try {
	//			mediaItemDAO.updateMediaItem(mediaItem, logInfo);
	//		} catch (DataAccessException e) {
	//			
	//			throw new BusinessException(e);
	//		}
	//	}

	@Override
	public void deleteMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws BusinessException {
		try {
			mediaItemDAO.deleteMediaItem(mediaItem, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public MediaItem getMediaItemById(Long id) throws BusinessException {
		try {
			return id == null ? null : mediaItemDAO.getMediaItemById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	//------media thumb---------
	@Override
	public MediaThumbnail createMediaThumbnail(MediaThumbnail mediaThumbnail, LogInfoVO logInfo) throws BusinessException {
		try {
			return mediaThumbnailDAO.createMediaThumbnail(mediaThumbnail, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void updateMediaThumbnail(MediaThumbnail mediaThumbnail, LogInfoVO logInfo) throws BusinessException {
		try {
			mediaThumbnailDAO.updateMediaThumbnail(mediaThumbnail, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteMediaThumbnail(MediaThumbnail mediaThumbnail, LogInfoVO logInfo) throws BusinessException {
		try {
			mediaThumbnailDAO.deleteMediaThumbnail(mediaThumbnail, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public MediaThumbnail getMediaThumbnailById(Long id) throws BusinessException {
		try {
			return id == null ? null : mediaThumbnailDAO.getMediaThumbnailById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	//---end---

	@Override
	public List<MediaItem> getListMediaItemByMediaType(MediaType mediaType) throws BusinessException {
		try {
			return mediaItemDAO.getListMediaItemByMediaType(mediaType);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public ProductIntroduction getProductIntroductionByProduct(Long productId) throws BusinessException {
		try {
			return productIntroductionDAO.getProductIntroductionByProduct(productId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<MediaItem> getListMediaItemByProduct(KPaging<MediaItem> kPaging, Long productId) throws BusinessException {
		try {
			return mediaItemDAO.getListMediaItemByObjectIdAndObjectType(kPaging, productId, MediaObjectType.IMAGE_PRODUCT);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ProductLot> getProductLotByProductAndOwner(long productId, long ownerId, StockObjectType ownerType) throws BusinessException {
		try {
			return productLotDAO.getProductLotByProductAndOwner(productId, ownerId, ownerType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ProductLot> getProductLotByProductAndOwnerEx(long productId, long ownerId, StockObjectType ownerType) throws BusinessException {
		try {
			return productLotDAO.getProductLotByProductAndOwnerEx(productId, ownerId, ownerType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<PoVnmDetailLotVO> getListProductForPoConfirm(KPaging<PoVnmDetailLotVO> kPaging, PoVnmFilter filter) throws BusinessException {
		try {
			List<PoVnmDetailLotVO> lst = productDAO.getListProductForPoConfirm(kPaging, filter);
			ObjectVO<PoVnmDetailLotVO> vo = new ObjectVO<PoVnmDetailLotVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<StockTotalVO> getListProductForPoAuto(KPaging<StockTotalVO> kPaging, ProductsForPoAutoParams filter) throws BusinessException {
		try {
			List<StockTotalVO> lst = productDAO.getListProductForPoAuto(kPaging, filter);
			ObjectVO<StockTotalVO> vo = new ObjectVO<StockTotalVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.business.ProductMgr#getListProductHasInSaleOrder(com
	 * .viettel.core.entities.enumtype.KPaging, java.lang.String,
	 * java.lang.String, ths.dms.core.entities.enumtype.ActiveType)
	 */
	@Override
	public ObjectVO<Product> getListProductHasInSaleOrder(KPaging<Product> kPaging, String productCode, String productName, ActiveType status) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<Product> lst = productDAO.getListProductHasInSaleOrder(kPaging, productCode, productName, status);
			ObjectVO<Product> vo = new ObjectVO<Product>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<ProductTreeVO> getListProductTreeVO(String productCode, String productName, Long programObjectId, ProgramObjectType programObjectType, Boolean exceptEquipmentCat, DisplayProductGroupType type) throws BusinessException {
		try {
			List<ProductTreeVO> lst = productDAO.getListProductTreeVO(productCode, productName, programObjectId, programObjectType, exceptEquipmentCat, type);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<ProductTreeVO> getListProductTreeVOByParentNode(Long programObjectId, ProgramObjectType programObjectType, Long parentIdNode, TreeVOType parentTypeNode, Boolean exceptEquipmentCat, DisplayProductGroupType type)
			throws BusinessException {
		try {
			List<ProductTreeVO> lst = productDAO.getListProductTreeVOByParentNode(programObjectId, programObjectType, parentIdNode, parentTypeNode, exceptEquipmentCat, type);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Boolean checkProductInZ(String productCode) throws BusinessException {
		try {
			return productDAO.checkProductInZ(productCode);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Boolean checkDuplicateOrderIndex(Long productId, int orderIndex) throws BusinessException {
		try {
			return productDAO.checkDuplicateOrderIndex(productId, orderIndex);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Boolean isProductBelongEquipmentCat(Product product) throws BusinessException {
		try {
			return productDAO.isProductBelongEquipmentCat(product);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public MediaItem checkIsExistsVideoOfProduct(Long productId) throws BusinessException {
		try {
			return mediaItemDAO.checkIsExistsVideoOfProduct(productId);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public List<GeneralProductVO> getAllProducts(String productCode, KPaging<GeneralProductVO> kPaging, Boolean isExceptZCat) throws BusinessException {
		try {
			return productDAO.getAllProducts(productCode, kPaging, isExceptZCat);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public MediaItem updateMediaItem(MediaItem mediaItem, LogInfoVO infoVO) throws BusinessException {

		try {
			return mediaItemDAO.updateMediaItem(mediaItem, infoVO);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public List<Product> getListProductEquipmentZ() throws BusinessException {
		try {
			return productDAO.getListProductEquipmentZ();
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}

	@Override
	public ObjectVO<Product> getListProductInCatZ(KPaging<Product> paging, String productCode, String productName) throws BusinessException {
		try {
			List<Product> lst = productDAO.getListProductInCatZ(paging, productCode, productName);
			ObjectVO<Product> vo = new ObjectVO<Product>();
			vo.setkPaging(paging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}

	public List<ProductLot> getProductLotByProduct(Product product) throws BusinessException {
		try {
			return productDAO.getProductLotByProduct(product);
		} catch (DataAccessException e) {
			throw new BusinessException(e.getMessage());
		}
	}

	//SangTN
	@Override
	public ObjectVO<ProductVOEx> getListProductVOForVideo(ths.dms.core.entities.filter.ProductFilter filter, List<Product> lstProductExcept) throws BusinessException {
		try {
			List<ProductVOEx> list = productDAO.getListProductVOForVideo(filter, lstProductExcept);
			ObjectVO<ProductVOEx> vo = new ObjectVO<ProductVOEx>();
			if (null != filter) {
				vo.setkPaging(filter.getkPaging());
			}
			vo.setLstObject(list);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ProductVOEx> getListCategory(String expectOne) throws BusinessException {
		try {
			return productDAO.getListCategroy(expectOne);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Product> getListProductByNameOrCode(KPaging<Product> kPaging, String name, String code, String categoryCode) throws BusinessException {
		try {
			List<Product> lst = productDAO.getListProductByNameOrCode(kPaging, name, code, categoryCode);
			ObjectVO<Product> vo = new ObjectVO<Product>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<PriceVO> searchPriceVOByFilter(PriceFilter<PriceVO> filter) throws BusinessException {
		try {
			List<PriceVO> lst = priceDAO.searchPriceVOByFilter(filter);
			ObjectVO<PriceVO> vo = new ObjectVO<PriceVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<PriceVO> searchPriceSaleInVOByFilter(PriceFilter<PriceVO> filter) throws BusinessException {
		try {
			List<PriceVO> lst = priceSaleInDAO.searchPriceSaleInVOByFilter(filter);
			ObjectVO<PriceVO> vo = new ObjectVO<PriceVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Price getPricetByFilter(PriceFilter<Price> filter) throws BusinessException {
		try {
			return priceDAO.getPricetByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkPricetByFilter(PriceFilter<Price> filter) throws BusinessException {
		try {
			return priceDAO.checkPricetByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkPricetByFilterEx(PriceFilter<Price> filter) throws BusinessException {
		try {
			return priceDAO.checkPricetByFilterEx(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Price createPrice(Price price, LogInfoVO logInfo) throws BusinessException {
		try {
			return priceDAO.createPrice(price, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public PriceSaleIn createPriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws BusinessException {
		try {
			return priceSaleInDAO.createPriceSaleIn(priceSaleIn, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void updatePrice(Price price, LogInfoVO logInfo) throws BusinessException {
		try {
			priceDAO.updatePrice(price, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updatePriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws BusinessException {
		try {
			priceSaleInDAO.updatePriceSaleIn(priceSaleIn, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void deletePrice(Price price, LogInfoVO logInfo) throws BusinessException {
		try {
			priceDAO.deletePrice(price, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteListPrice(List<Price> lstPrice, LogInfoVO logInfo) throws BusinessException {
		try {
			for (Price price : lstPrice) {
				priceDAO.deletePrice(price, logInfo);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateWaitingPriceManager(List<Price> lstPrice, LogInfoVO logInfo, Integer flag) throws BusinessException {
		try {
			Date sysdateC = commonMgr.getSysDate();
			for (Price price : lstPrice) {
				if (ActiveType.STOPPED.getValue().equals(flag)) {
					priceDAO.deletePrice(price, logInfo);
				} else if (ActiveType.RUNNING.getValue().equals(flag)) {				
					price.setFromDate(sysdateC);
					price.setUpdateDate(sysdateC);
					price.setStatus(ActiveType.RUNNING);
					priceDAO.updatePrice(price, logInfo);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createAndUpdateOldPriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws BusinessException {
		try {
			PriceFilter<PriceSaleIn> filter = new PriceFilter<PriceSaleIn>();
			if (priceSaleIn.getShop() != null) {
				filter.setShopId(priceSaleIn.getShop().getId());
			}
			if (priceSaleIn.getProduct() != null) {
				filter.setProductId(priceSaleIn.getProduct().getId());
			}
			filter.setStatus(ActiveType.RUNNING.getValue());
			List<PriceSaleIn> lstRunning = priceSaleInDAO.getListPriceSaleInByFilter(filter);
			if (lstRunning != null && lstRunning.size() > 0) {
				for (int k = 0, t = lstRunning.size(); k < t; k++) {
					PriceSaleIn priceRunning = lstRunning.get(k);
					priceRunning.setToDate(logInfo.getStartDate()); // set toDate = sysDate
					priceRunning.setStatus(ActiveType.STOPPED);
				}
				priceSaleInDAO.updatePriceSaleIn(lstRunning, logInfo);
			}
			priceSaleInDAO.createPriceSaleIn(priceSaleIn, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Price getPriceById(Long id) throws BusinessException {
		try {
			return id == null ? null : priceDAO.getPriceById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public PriceSaleIn getPriceSaleInById(Long id) throws BusinessException {
		try {
			return id == null ? null : priceSaleInDAO.getPriceSaleInById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Price> getListPriceByShopId(Long shopId, Date lockDay, Long productId) throws BusinessException {
		try {
			return priceDAO.getListPriceByShopId(shopId, lockDay, productId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Price> getListPrice(KPaging<Price> kPaging, String productCode, BigDecimal price, Date fromDate, Date toDate, ActiveType status, BigDecimal priceNotVat, BigDecimal vat, boolean isExport) throws BusinessException {
		try {
			List<Price> lst = priceDAO.getListPrice(null, productCode, price, fromDate, toDate, status, priceNotVat, vat, isExport);
			ObjectVO<Price> vo = new ObjectVO<Price>();
			vo.setkPaging(null);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<Price> getListPriceByFilter(PriceFilter<Price> filter) throws BusinessException {
		try {
			List<Price> lst = priceDAO.getListPriceByFilter(filter);
			ObjectVO<Price> vo = new ObjectVO<Price>();
			vo.setkPaging(null);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean checkIfDateRangeInUse(Long productId, Date fromDate, Date toDate, Long priceId) throws BusinessException {
		try {
			return priceDAO.checkIfDateRangeInUse(productId, fromDate, toDate, priceId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean isProductUsingByOthers(long productId) throws BusinessException {
		try {
			return productDAO.isUsingByOthers(productId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Boolean isPriceUsingByOthers(long priceId) throws BusinessException {
		try {
			return priceDAO.isUsingByOthers(priceId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<StockTotalVO> getPromotionProductInfo(Long productId, Long shopId, Integer shopChannel, Long customerId, Integer objectType, Date orderDate) throws BusinessException {
		try {
			return productDAO.getPromotionProductInfo(productId, shopId, shopChannel, customerId, objectType, orderDate);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Price getPriceByProductAndShopId(Long productId, Long shopId) throws BusinessException {
		try {
			return priceDAO.getPriceByProductAndShopId(productId, shopId);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public Price getProductPriceForCustomer(long productId, Customer cust) throws BusinessException {
		if (cust == null) {
			throw new IllegalArgumentException("customer is null");
		}
		try {
			long shopId = cust.getShop().getId();
			if (cust.getChannelType() == null) {
				return priceDAO.getPriceByProductAndShopId(productId, shopId);
			}
			long type = cust.getChannelType().getId();
			return priceDAO.getProductPriceForCustomer(productId, shopId, type);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Integer stopPrice(Price price, LogInfoVO logInfo) throws BusinessException {
		try {
			price.setStatus(ActiveType.STOPPED);
			//price.setToDate(commonDAO.getSysDate());
			//vuongmq; 07/09/2015; off gia (giao dien + import) --> to_date = sysdate ==> sua lai to_date = sysdate - 1
			Date toDate = DateUtility.addDate(commonDAO.getSysDate(), -1);
			price.setToDate(toDate);
			priceDAO.updatePrice(price, logInfo);
			return 1;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Boolean stopPriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws BusinessException {
		try {
			priceSaleIn.setStatus(ActiveType.STOPPED);
			Date toDate = DateUtility.addDate(commonDAO.getSysDate(), -1);
			priceSaleIn.setToDate(toDate);
			priceSaleInDAO.updatePriceSaleIn(priceSaleIn, logInfo);
			return true;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ProductVO> getListProductForWarehouseExport(Integer status) throws BusinessException {
		try {
			return productDAO.getListProductForWarehouseExport(status);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ProductVO> getListProductForBorrowFilter(Integer status)throws BusinessException {
		try {
			return productDAO.getListProductForWarehouseExport(status);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Lay danh sach san pham theo ten (tim chinh xac) productName, tru san pham exceptProductId
	 * 
	 * @author lacnv1
	 * @since Dec 28, 2013
	 */
	@Override
	public List<Product> getListProductsByName(String productName, Long exceptProductId)
			throws BusinessException {
		try {
			return productDAO.getListProductsByName(productName, exceptProductId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	/**
	 * Lay danh sach san pham theo ten shortName, tru san pham exceptProductId
	 * 
	 * @author haupv3
	 * @since Feb 10, 2014
	 */
	@Override
	public List<Product> getListProductsByShortName(String shortName, Long exceptProductId)
			throws BusinessException {
		try {
			return productDAO.getListProductsByShortName(shortName, exceptProductId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * LacNV
	 */
	@Override
	public ObjectVO<ProductExportVO> getListProductExport(ProductFilter filter, KPaging<ProductExportVO> paging)
			throws BusinessException {
		try {
			List<ProductExportVO> lst = productDAO.getListProductExport(filter, paging);
			ObjectVO<ProductExportVO> vo = new ObjectVO<ProductExportVO>();
			vo.setLstObject(lst);
			vo.setkPaging(paging);
			return vo;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * Tang orderIndex len de chen san pham neu co san pham trung orderIndex
	 * @author Trietptm
	 * @since 19/06/2015
	 */
	@Override
	public void increaseOrderIndexProduct(Product product) throws BusinessException {
		try {
			if (productDAO.checkDuplicateOrderIndex(product.getId(), product.getOrderIndex())) {
				productDAO.increaseOrderIndexProduct(product.getOrderIndex());
			}
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author lacnv1
	 * @since Dec 16, 2013
	 * @param product
	 * @param price1 - gia cong ty ban cho npp
	 * @param price2 - gia npp ban cho KH
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Product saveOrUpdateProduct(Product product, List<Long> lstAttributeId, List<String> lstAttributeValue, List<Integer> lstAttributeColumnValueType, LogInfoVO logInfo)
			throws BusinessException {
		if (product == null || logInfo == null) {
			throw new IllegalArgumentException("invalid parameter");
		}
		try {
			Date sysDate = commonDAO.getSysDate();
			this.increaseOrderIndexProduct(product);
			Boolean isCreateProduct = false;
			if (product.getId() == null) {
				isCreateProduct = true;
	    		product = productDAO.createProduct(product, logInfo);	    		
	    	} else { // cap nhat
	    		productDAO.updateProduct(product, logInfo);
	    	}
			ProductAttributeDetail productAttributeDetail = null;
			ProductAttributeEnum productAttributeEnum = null;
			int index = 0;
			if (lstAttributeId != null) {
				for(Long attrId : lstAttributeId){
					ProductAttribute productAttribute = commonDAO.getEntityById(ProductAttribute.class, attrId);
					if(productAttribute == null){
						continue;
					}
					if(AttributeColumnType.CHARACTER.equals(productAttribute.getValueType())
							|| AttributeColumnType.NUMBER.equals(productAttribute.getValueType())
							|| AttributeColumnType.LOCATION.equals(productAttribute.getValueType())
							|| AttributeColumnType.CHOICE.equals(productAttribute.getValueType())
							|| AttributeColumnType.DATE_TIME.equals(productAttribute.getValueType())){
						
						productAttributeDetail = productAttributeDAO.getProductAttributeDetailByFilter(attrId, product.getId(), null);
						
						if(AttributeColumnType.CHOICE.equals(productAttribute.getValueType()) && productAttributeDetail != null){
							productAttributeDetail.setStatus(ActiveType.DELETED);
							productAttributeDetail.setUpdateDate(sysDate);
							productAttributeDetail.setUpdateUser(logInfo.getStaffCode());
							commonDAO.updateEntity(productAttributeDetail);
							productAttributeDetail = null;
						}
						
						if(productAttributeDetail == null){
							productAttributeDetail = new ProductAttributeDetail();
							productAttributeDetail.setCreateDate(sysDate);
							productAttributeDetail.setCreateUser(logInfo.getStaffCode());
							
							
							productAttributeDetail.setProduct(product);
							productAttributeDetail.setStatus(ActiveType.RUNNING);
							productAttributeDetail.setProductAttribute(productAttribute);
							
							if(AttributeColumnType.CHOICE.equals(productAttribute.getValueType())) {
								if(!StringUtility.isNullOrEmpty(lstAttributeValue.get(index))){
									productAttributeEnum = commonDAO.getEntityById(ProductAttributeEnum.class, Long.valueOf(lstAttributeValue.get(index)));
									if(productAttributeEnum != null){
										productAttributeDetail.setProductAttributeEnum(productAttributeEnum);
										commonDAO.createEntity(productAttributeDetail);
									}
								}							
							}else{
								productAttributeDetail.setValue(lstAttributeValue.get(index));
								commonDAO.createEntity(productAttributeDetail);
							}
						}else{
							productAttributeDetail.setUpdateDate(sysDate);
							productAttributeDetail.setUpdateUser(logInfo.getStaffCode());
							productAttributeDetail.setValue(lstAttributeValue.get(index));
							commonDAO.updateEntity(productAttributeDetail);
						}
					}else if(AttributeColumnType.MULTI_CHOICE.equals(productAttribute.getValueType())){					
						String valueStr = lstAttributeValue.get(index); //format x;y;z
						String[] valueStrArray = valueStr.split(";");
						
						List<ProductAttributeDetail> attributeDetails = productAttributeDAO.getProductAttributeDetailByFilter(attrId, product.getId());
						
						for(ProductAttributeDetail detail : attributeDetails ){
							detail.setStatus(ActiveType.DELETED);
							detail.setUpdateDate(sysDate);
							detail.setUpdateUser(logInfo.getStaffCode());
							commonDAO.updateEntity(detail);
						}
						if(!StringUtility.isNullOrEmpty(valueStr)){
							for(String str : valueStrArray){
								productAttributeDetail = new ProductAttributeDetail();
								productAttributeDetail.setCreateDate(sysDate);
								productAttributeDetail.setCreateUser(logInfo.getStaffCode());
								
								
								productAttributeDetail.setProduct(product);
								productAttributeDetail.setStatus(ActiveType.RUNNING);
								productAttributeDetail.setProductAttribute(productAttribute);						
								
								productAttributeEnum = commonDAO.getEntityById(ProductAttributeEnum.class, Long.valueOf(str));
								if(productAttributeEnum != null){
									productAttributeDetail.setProductAttributeEnum(productAttributeEnum);
								}
								
								commonDAO.createEntity(productAttributeDetail);
							}
						}
					}
					++index;
				}
			}
			if (isCreateProduct) {
				createStockTotalForNewProduct(product, sysDate, logInfo);				
			}
			return product;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	private void createStockTotalForNewProduct(Product product, Date sysdate, LogInfoVO logInfo) throws DataAccessException {
		List<Shop> distributorShops = commonBusinessHelper.getAllDistributorShop();
		for (Shop shop : distributorShops) {
			List<Warehouse> allWarehouses = getAllDistributorWarehouse(shop.getId());
			for (Warehouse warehouse : allWarehouses) {
				StockTotal stockTotal = commonBusinessHelper.createStockTotalEntity(shop, product, warehouse, sysdate, logInfo);
				stockTotal.setDescr("Tạo kho khi thêm sản phẩm");
				commonDAO.createEntity(stockTotal);
			}
		}
	}

	private List<Warehouse> getAllDistributorWarehouse(Long shopId) throws DataAccessException {
		StockTotalFilter filter = new StockTotalFilter();
		filter.setOwnerId(shopId);
		List<Warehouse> allWarehouses = wareHouseDAO.getListWarehouseByFilter(filter);
		return allWarehouses;
	}
	
	/***************************END HAM MOI TU HABECO *************************/
	
	/**
	 * @author vuongmq
	 * @date Jan 22, 2015
	 * save mediaItem (Image) cho product
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public List<MediaItem> saveUploadImageInfo(List<MediaItem> mediaItems, List<MediaItem> mediaItemsVideo, LogInfoVO logInfo)	throws BusinessException {
		if (logInfo == null) {
			throw new IllegalArgumentException("invalid parameter");
		}
		try {
			List<MediaItem> mediaItemsNew = new ArrayList<MediaItem>();
			Date sysDate = commonDAO.getSysDate();
			if(mediaItems != null && mediaItems.size() > 0){
				for(int i = 0, sz = mediaItems.size(); i< sz; i++){
					MediaItem item = mediaItems.get(i);
					item.setCreateDate(sysDate);
					item = mediaItemDAO.createMediaItem(item, logInfo);
					mediaItemsNew.add(item);
				}
			}
			if(mediaItemsVideo != null && mediaItemsVideo.size() > 0){
				for(int i = 0, sz = mediaItemsVideo.size(); i< sz; i++){
					MediaItem item = mediaItemsVideo.get(i);
					item.setCreateDate(sysDate);
					item = mediaItemDAO.createMediaItem(item, logInfo);
					mediaItemsNew.add(item);
				}
			}
			return mediaItemsNew;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<Price> getPriceByFullCondition(Long proId, Long shopId,
			Date date, Long shopTypeId, Long cusId, Long cusTypeId)
			throws BusinessException {
		try {
			return priceDAO.getPriceByFullCondition(proId, shopId, date, shopTypeId, cusId, cusTypeId);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<Price> getPriceByRpt(SaleOrderFilter<Price> filter) throws BusinessException {
		try {
			return priceDAO.getPriceByRpt(filter);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Price getPriceByProductAndCustomer(Long productId, Long customerId, List<Integer> status)
			throws BusinessException {
		try {
			return priceDAO.getPriceByProductAndCustomer(productId, customerId, status);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Price getPriceByProductChannelTypeShopAndCustomerNull(
			Long productId, Long channelTypeId, Long shopId, List<Integer> status)
			throws BusinessException {
		try {
			return priceDAO.getPriceByProductChannelTypeShopAndCustomerNull(productId, channelTypeId, shopId, status);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Price getPriceByProductChannelTypeShopType(Long customerTypeId,
			Long shopTypeId, Long productId, List<Integer> status) throws BusinessException {
		try {
			return priceDAO.getPriceByProductChannelTypeShopType(customerTypeId, shopTypeId, productId, status);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
		
	}

	@Override
	public Price getPriceByProductShop(Long shopId, Long productId, List<Integer> status)
			throws BusinessException {
		try {
			return priceDAO.getPriceByProductShop(shopId, productId, status);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public Price getPriceByProductShopType(Long shopTypeId, Long productId, List<Integer> status)
			throws BusinessException {
		try {
			return priceDAO.getPriceByProductShopType(shopTypeId, productId, status);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<ImageVO> getListParentCatByCat(Long id) throws BusinessException {
		try {
			return productDAO.getListParentCatByCat(id);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<ImageVO> getListParentCatBySubCat(Long id) throws BusinessException {
		try {
			return productDAO.getListParentCatBySubCat(id);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public ProductInfo getProductInfoById(Long id) throws BusinessException {
		try {
			return id == null ? null : productDAO.getProductInfoById(id);
		} catch (DataAccessException e) {			
			throw new BusinessException(e);
		}	
	}

	@Override
	public List<ImageVO> getListParentCatByCatAndSubCat(Long idParentCat, Long idParentSubCat) throws BusinessException {
		try {
			return productDAO.getListParentCatByCatAndSubCat(idParentCat,idParentSubCat);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<ProductInfo> getSubCatOnTree(Boolean b) throws BusinessException {
		try {
			return productDAO.getSubCatOnTree(b);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Product> checkIfExistsProductByProductInfo(ProductType pdType, Long productInfoId) throws BusinessException {
		try{
			return productDAO.checkIfExistsProductByProductInfo(pdType, productInfoId);			
		}catch(Exception ex){
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<ProductExportVO> getProductWithDynamicAttributeInfo(ProductFilter filter) throws BusinessException {
		try {
			return productDAO.getProductWithDynamicAttributeInfo(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Product getProductByCodeNotByStatus(String code) throws BusinessException {
		try {
			return productDAO.getProductByCodeNotByStatus(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<ProductVO> getListProductVOByFilter(ProductGeneralFilter<ProductVO> filter) throws BusinessException {
		try {
			List<ProductVO> lst = productDAO.getListProductVOByFilter(filter);
			ObjectVO<ProductVO> vo = new ObjectVO<ProductVO>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
}

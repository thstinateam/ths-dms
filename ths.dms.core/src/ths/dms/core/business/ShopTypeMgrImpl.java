package ths.dms.core.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ShopType;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.ShopTypeDAO;


public class ShopTypeMgrImpl implements ShopTypeMgr {

	@Autowired
	ShopTypeDAO shopTypeDAO;
	
	@Override
	public ShopType getShopTypeByCode(String code) throws BusinessException {
		try {
			return shopTypeDAO.getShopTypeByCode(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<ShopType> getListShopTypeByType(List<Integer> status)
			throws BusinessException {
		try {
			return shopTypeDAO.getListShopTypeByType(status);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}
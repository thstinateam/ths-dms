/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.ProductShopMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ShopParamType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.ShopParamVO;
import ths.dms.core.entities.vo.ShopParamVODayEnd;
import ths.dms.core.entities.vo.ShopParamVODayStart;
import ths.dms.core.entities.vo.ShopParamVOProduct;
import ths.dms.core.entities.vo.ShopParamVOSave;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.Constant;
import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.MessageUtil;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.ShopParamDAO;


/**
 * Mo ta class ShopParamMgrImpl.java
 * @author vuongmq
 * @since Nov 28, 2015
 */
public class ShopParamMgrImpl implements ShopParamMgr {
	@Autowired
	ShopParamDAO shopParamDAO;
	
	@Autowired
	CommonDAO commonDAO;

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#createShopParam(ths.dms.core.entities.ShopParam, ths.dms.core.entities.vo.LogInfoVO)
	 */
	@Override
	public ShopParam createShopParam(ShopParam shopParam, LogInfoVO logInfo) throws BusinessException {
		try {
			return shopParamDAO.createShopParam(shopParam, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#deleteShopParam(ths.dms.core.entities.ShopParam, ths.dms.core.entities.vo.LogInfoVO)
	 */
	@Override
	public void deleteShopParam(ShopParam shopParam, LogInfoVO logInfo) throws BusinessException {
		try {
			shopParamDAO.deleteShopParam(shopParam, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#updateShopParam(ths.dms.core.entities.ShopParam, ths.dms.core.entities.vo.LogInfoVO)
	 */
	@Override
	public void updateShopParam(ShopParam shopParam, LogInfoVO logInfo) throws BusinessException {
		try {
			shopParamDAO.updateShopParam(shopParam, logInfo);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#getShopParamById(long)
	 */
	@Override
	public ShopParam getShopParamById(long id) throws BusinessException {
		try {
			return shopParamDAO.getShopParamById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#getShopParamByShopIdCode(java.lang.Long, java.lang.String, ths.dms.core.entities.enumtype.ActiveType)
	 */
	@Override
	public ShopParam getShopParamByShopIdCode(Long shopId, String code, ActiveType activeType) throws BusinessException {
		try {
			return shopParamDAO.getShopParamByShopIdCode(shopId, code, activeType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#getShopParam(java.lang.Long, java.lang.String, ths.dms.core.entities.enumtype.ShopParamType, ths.dms.core.entities.enumtype.ActiveType)
	 */
	@Override
	public ShopParam getShopParam(Long shopId, String code, ShopParamType type, ActiveType activeType) throws BusinessException {
		try {
			return shopParamDAO.getShopParam(shopId, code, type, activeType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#saveInfo(ths.dms.core.entities.vo.ShopParamVOSave, ths.dms.core.entities.vo.LogInfoVO)
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveInfo(ShopParamVOSave shopParamVOSave, LogInfoVO logInfoVO) throws BusinessException {
		try {
			if (shopParamVOSave != null && shopParamVOSave.getShop() != null && shopParamVOSave.getShop().getId() != null) {
				Date sys = commonDAO.getSysDate();
				Long shopId = shopParamVOSave.getShop().getId();
				// update getShopDistanceOrder
				Shop shopUpdate = shopParamVOSave.getShop();
				shopUpdate.setDistanceOrder(shopParamVOSave.getShopDistanceOrder());
				shopUpdate.setUpdateDate(sys);
				shopUpdate.setUpdateUser(logInfoVO.getStaffCode());
				commonDAO.updateEntity(shopUpdate);
				ShopParam shopParam = shopParamDAO.getShopParamByShopIdCode(shopId, Constant.DT_START, ActiveType.RUNNING);
				if (shopParam == null) {
					if (!StringUtility.isNullOrEmpty(shopParamVOSave.getShopDTStart())) {
						ShopParam paramNew = new ShopParam();
						paramNew.setShop(shopUpdate);
						paramNew.setCode(Constant.DT_START);
						paramNew.setType(Constant.DT_START);
						paramNew.setValue(shopParamVOSave.getShopDTStart());
						paramNew.setName(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_dt_start"));
						paramNew.setDescription(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_dt_start"));
						paramNew.setStatus(ActiveType.RUNNING);
						paramNew.setCreateDate(sys);
						paramNew.setCreateUser(logInfoVO.getStaffCode());
						shopParamDAO.createShopParam(paramNew, logInfoVO);
					}
				} else {
					shopParam.setValue(shopParamVOSave.getShopDTStart());
					shopParam.setUpdateDate(sys);
					shopParam.setUpdateUser(logInfoVO.getStaffCode());
					shopParamDAO.updateShopParam(shopParam, logInfoVO);
				}
				shopParam = shopParamDAO.getShopParamByShopIdCode(shopId, Constant.DT_MIDDLE, ActiveType.RUNNING);
				if (shopParam == null) {
					if (!StringUtility.isNullOrEmpty(shopParamVOSave.getShopDTMiddle())) {
						ShopParam paramNew = new ShopParam();
						paramNew.setShop(shopUpdate);
						paramNew.setCode(Constant.DT_MIDDLE);
						paramNew.setType(Constant.DT_MIDDLE);
						paramNew.setValue(shopParamVOSave.getShopDTMiddle());
						paramNew.setName(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_dt_middle"));
						paramNew.setDescription(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_dt_middle"));
						paramNew.setStatus(ActiveType.RUNNING);
						paramNew.setCreateDate(sys);
						paramNew.setCreateUser(logInfoVO.getStaffCode());
						shopParamDAO.createShopParam(paramNew, logInfoVO);
					}
				} else {
					shopParam.setValue(shopParamVOSave.getShopDTMiddle());
					shopParam.setUpdateDate(sys);
					shopParam.setUpdateUser(logInfoVO.getStaffCode());
					shopParamDAO.updateShopParam(shopParam, logInfoVO);
				}
				shopParam = shopParamDAO.getShopParamByShopIdCode(shopId, Constant.DT_END, ActiveType.RUNNING);
				if (shopParam == null) {
					if (!StringUtility.isNullOrEmpty(shopParamVOSave.getShopDTEnd())) {
						ShopParam paramNew = new ShopParam();
						paramNew.setShop(shopUpdate);
						paramNew.setCode(Constant.DT_END);
						paramNew.setType(Constant.DT_END);
						paramNew.setValue(shopParamVOSave.getShopDTEnd());
						paramNew.setName(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_dt_end"));
						paramNew.setDescription(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_dt_end"));
						paramNew.setStatus(ActiveType.RUNNING);
						paramNew.setCreateDate(sys);
						paramNew.setCreateUser(logInfoVO.getStaffCode());
						shopParamDAO.createShopParam(paramNew, logInfoVO);
					}
				} else {
					shopParam.setValue(shopParamVOSave.getShopDTEnd());
					shopParam.setUpdateDate(sys);
					shopParam.setUpdateUser(logInfoVO.getStaffCode());
					shopParamDAO.updateShopParam(shopParam, logInfoVO);
				}
				shopParam = shopParamDAO.getShopParamByShopIdCode(shopId, Constant.CC_DISTANCE, ActiveType.RUNNING);
				if (shopParam == null) {
					if (!StringUtility.isNullOrEmpty(shopParamVOSave.getShopCCDistance())) {
						ShopParam paramNew = new ShopParam();
						paramNew.setShop(shopUpdate);
						paramNew.setCode(Constant.CC_DISTANCE);
						paramNew.setType(Constant.CC_DISTANCE);
						paramNew.setValue(shopParamVOSave.getShopCCDistance());
						paramNew.setName(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_cc_distance"));
						paramNew.setDescription(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_cc_distance"));
						paramNew.setStatus(ActiveType.RUNNING);
						paramNew.setCreateDate(sys);
						paramNew.setCreateUser(logInfoVO.getStaffCode());
						shopParamDAO.createShopParam(paramNew, logInfoVO);
					}
				} else {
					shopParam.setValue(shopParamVOSave.getShopCCDistance());
					shopParam.setUpdateDate(sys);
					shopParam.setUpdateUser(logInfoVO.getStaffCode());
					shopParamDAO.updateShopParam(shopParam, logInfoVO);
				}
				shopParam = shopParamDAO.getShopParamByShopIdCode(shopId, Constant.CC_START, ActiveType.RUNNING);
				if (shopParam == null) {
					if (!StringUtility.isNullOrEmpty(shopParamVOSave.getShopCCStart())) {
						ShopParam paramNew = new ShopParam();
						paramNew.setShop(shopUpdate);
						paramNew.setCode(Constant.CC_START);
						paramNew.setType(Constant.CC_START);
						paramNew.setValue(shopParamVOSave.getShopCCStart());
						paramNew.setName(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_cc_start"));
						paramNew.setDescription(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_cc_start"));
						paramNew.setStatus(ActiveType.RUNNING);
						paramNew.setCreateDate(sys);
						paramNew.setCreateUser(logInfoVO.getStaffCode());
						shopParamDAO.createShopParam(paramNew, logInfoVO);
					}
				} else {
					shopParam.setValue(shopParamVOSave.getShopCCStart());
					shopParam.setUpdateDate(sys);
					shopParam.setUpdateUser(logInfoVO.getStaffCode());
					shopParamDAO.updateShopParam(shopParam, logInfoVO);
				}
				shopParam = shopParamDAO.getShopParamByShopIdCode(shopId, Constant.CC_END, ActiveType.RUNNING);
				if (shopParam == null) {
					if (!StringUtility.isNullOrEmpty(shopParamVOSave.getShopCCEnd())) {
						ShopParam paramNew = new ShopParam();
						paramNew.setShop(shopUpdate);
						paramNew.setCode(Constant.CC_END);
						paramNew.setType(Constant.CC_END);
						paramNew.setValue(shopParamVOSave.getShopCCEnd());
						paramNew.setName(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_cc_end"));
						paramNew.setDescription(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_cc_end"));
						paramNew.setStatus(ActiveType.RUNNING);
						paramNew.setCreateDate(sys);
						paramNew.setCreateUser(logInfoVO.getStaffCode());
						shopParamDAO.createShopParam(paramNew, logInfoVO);
					}
				} else {
					shopParam.setValue(shopParamVOSave.getShopCCEnd());
					shopParam.setUpdateDate(sys);
					shopParam.setUpdateUser(logInfoVO.getStaffCode());
					shopParamDAO.updateShopParam(shopParam, logInfoVO);
				}
				// luu doi thoi gian chot ngay; delete va them moi
				if (shopParamVOSave.getLstDayStartDelete() != null && shopParamVOSave.getLstDayStartDelete().size() > 0) {
					List<ShopParam> lstDayStartDelete = new ArrayList<>();
					for (int i = 0, sz = shopParamVOSave.getLstDayStartDelete().size(); i < sz; i++) {
						Long id = shopParamVOSave.getLstDayStartDelete().get(i);
						ShopParam shopParamExecTime = shopParamDAO.getShopParamById(id);
						if (shopParamExecTime != null && Constant.SHOP_LOCK_EXEC_TIME.equals(shopParamExecTime.getCode()) 
								&& shopParamExecTime.getShop() != null && shopId.equals(shopParamExecTime.getShop().getId())) {
							shopParamExecTime.setStatus(ActiveType.DELETED);
							shopParamExecTime.setUpdateDate(sys);
							shopParamExecTime.setUpdateUser(logInfoVO.getStaffCode());
							lstDayStartDelete.add(shopParamExecTime);
						}
					}
					if (!lstDayStartDelete.isEmpty()) {
						commonDAO.updateListEntity(lstDayStartDelete);
					}
				}
				if (shopParamVOSave.getLstDayStart() != null && shopParamVOSave.getLstDayStart().size() > 0) {
					List<ShopParam> lstShopParamExecTime = new ArrayList<>();
					for (int i = 0, sz = shopParamVOSave.getLstDayStart().size(); i < sz; i++) {
						ShopParamVODayStart shopParamVODayStart = shopParamVOSave.getLstDayStart().get(i);
						if (shopParamVODayStart != null && !StringUtility.isNullOrEmpty(shopParamVODayStart.getValueDateStr()) && !StringUtility.isNullOrEmpty(shopParamVODayStart.getValueTimeStr())) {
							String valueDate = shopParamVODayStart.getValueDateStr() + " " + shopParamVODayStart.getValueTimeStr();
							Date date = DateUtility.parse(valueDate, DateUtility.DATETIME_FORMAT_STR);
							valueDate = DateUtility.toDateString(date, DateUtility.DATETIME_ATTRIBUTE);
							ShopParam paramNew = new ShopParam();
							paramNew.setShop(shopUpdate);
							paramNew.setCode(Constant.SHOP_LOCK_EXEC_TIME);
							paramNew.setType(Constant.SHOP_LOCK_EXEC_TIME);
							paramNew.setValue(valueDate);
							paramNew.setName(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_cn_start_date_change"));
							paramNew.setDescription(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_cn_start_date_change"));
							paramNew.setStatus(ActiveType.RUNNING);
							paramNew.setCreateDate(sys);
							paramNew.setCreateUser(logInfoVO.getStaffCode());
							lstShopParamExecTime.add(paramNew);
						}
					}
					if (!lstShopParamExecTime.isEmpty()) {
						commonDAO.creatListEntity(lstShopParamExecTime);
					}
				}
				// luu tam dung chot tu dong; delete va them moi
				if (shopParamVOSave.getLstDayEndDelete() != null && shopParamVOSave.getLstDayEndDelete().size() > 0) {
					List<ShopParam> lstDayEndDelete = new ArrayList<>();
					for (int i = 0, sz = shopParamVOSave.getLstDayEndDelete().size(); i < sz; i++) {
						Long id = shopParamVOSave.getLstDayEndDelete().get(i);
						ShopParam shopParamAbort = shopParamDAO.getShopParamById(id);
						if (shopParamAbort != null && Constant.SHOP_LOCK_ABORT_DURATION.equals(shopParamAbort.getCode()) 
								&& shopParamAbort.getShop() != null && shopId.equals(shopParamAbort.getShop().getId())) {
							shopParamAbort.setStatus(ActiveType.DELETED);
							shopParamAbort.setUpdateDate(sys);
							shopParamAbort.setUpdateUser(logInfoVO.getStaffCode());
							lstDayEndDelete.add(shopParamAbort);
						}
					}
					if (!lstDayEndDelete.isEmpty()) {
						commonDAO.updateListEntity(lstDayEndDelete);
					}
				}
				if (shopParamVOSave.getLstDayEnd() != null && shopParamVOSave.getLstDayEnd().size() > 0) {
					List<ShopParam> lstDayEndInsert = new ArrayList<>();
					List<ShopParam> lstDayEndUpdate = new ArrayList<>();
					for (int i = 0, sz = shopParamVOSave.getLstDayEnd().size(); i < sz; i++) {
						ShopParamVODayEnd shopParamVODayEnd = shopParamVOSave.getLstDayEnd().get(i);
						if (shopParamVODayEnd != null && !ActiveType.RUNNING.getValue().equals(shopParamVODayEnd.getUpdateFlag())) {
							if (!StringUtility.isNullOrEmpty(shopParamVODayEnd.getFromDateStr()) && !StringUtility.isNullOrEmpty(shopParamVODayEnd.getToDateStr())) {
								Date fDate = DateUtility.parse(shopParamVODayEnd.getFromDateStr(), DateUtility.DATE_FORMAT_STR);
								String fDateStr = DateUtility.toDateString(fDate, DateUtility.DATE_FORMAT_ATTRIBUTE);
								Date tDate = DateUtility.parse(shopParamVODayEnd.getToDateStr(), DateUtility.DATE_FORMAT_STR);
								String tDateStr = DateUtility.toDateString(tDate, DateUtility.DATE_FORMAT_ATTRIBUTE);
								String valueDate = fDateStr + ";" + tDateStr;
								if (ActiveType.WAITING.getValue().equals(shopParamVODayEnd.getUpdateFlag())) {
									//update SHOP_LOCK_ABORT_DURATION
									ShopParam spEndUpdate = shopParamDAO.getShopParamById(shopParamVODayEnd.getId());
									if (spEndUpdate != null && Constant.SHOP_LOCK_ABORT_DURATION.equals(spEndUpdate.getCode()) 
											&& spEndUpdate.getShop() != null && shopId.equals(spEndUpdate.getShop().getId())) {
										spEndUpdate.setUpdateDate(sys);
										spEndUpdate.setUpdateUser(logInfoVO.getStaffCode());
										spEndUpdate.setValue(valueDate);
										lstDayEndUpdate.add(spEndUpdate);
									}
								} else {
									// insert SHOP_LOCK_ABORT_DURATION
									ShopParam paramNew = new ShopParam();
									paramNew.setShop(shopUpdate);
									paramNew.setCode(Constant.SHOP_LOCK_ABORT_DURATION);
									paramNew.setType(Constant.SHOP_LOCK_ABORT_DURATION);
									paramNew.setValue(valueDate);
									paramNew.setName(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_cn_end_date_auto"));
									paramNew.setDescription(MessageUtil.getResourceString(MessageUtil.VI_LANGUAGE, "shop_param_cn_end_date_auto"));
									paramNew.setStatus(ActiveType.RUNNING);
									paramNew.setCreateDate(sys);
									paramNew.setCreateUser(logInfoVO.getStaffCode());
									lstDayEndInsert.add(paramNew);
								}
							}
						}
					}
					if (!lstDayEndUpdate.isEmpty()) {
						commonDAO.updateListEntity(lstDayEndUpdate);
					}
					if (!lstDayEndInsert.isEmpty()) {
						commonDAO.creatListEntity(lstDayEndInsert);
					}
				}
				// luu Product shop map; so ngay ton kho an toan
				if (shopParamVOSave.getLstProductShopMapVO() != null && shopParamVOSave.getLstProductShopMapVO().size() > 0) {
					List<ProductShopMap> lstStockDayInsert = new ArrayList<>();
					List<ProductShopMap> lstStockDayUpdate = new ArrayList<>();
					for (int i = 0, sz = shopParamVOSave.getLstProductShopMapVO().size(); i < sz; i++) {
						ShopParamVOProduct shopParamVOProduct = shopParamVOSave.getLstProductShopMapVO().get(i);
						if (shopParamVOProduct != null && shopParamVOProduct.getId() != null) {
							// update Product shop map
							ProductShopMap productShopMap = commonDAO.getEntityById(ProductShopMap.class, shopParamVOProduct.getId());
							if (productShopMap != null && shopId.equals(productShopMap.getShopId())) {
								productShopMap.setNessesaryStockDay(shopParamVOProduct.getStockDay());
								lstStockDayUpdate.add(productShopMap);
							}
						} else {
							// tao Product shop map
							ProductShopMap psmNew = new ProductShopMap();
							psmNew.setProductId(shopParamVOProduct.getProductId());
							psmNew.setShopId(shopUpdate.getId());
							psmNew.setNessesaryStockDay(shopParamVOProduct.getStockDay());
							lstStockDayInsert.add(psmNew);
						}
					}
					if (!lstStockDayUpdate.isEmpty()) {
						commonDAO.updateListEntity(lstStockDayUpdate);
					}
					if (!lstStockDayInsert.isEmpty()) {
						commonDAO.creatListEntity(lstStockDayInsert);
					}
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#getListShopParamVOByfilter(ths.dms.core.entities.filter.BasicFilter)
	 */
	@Override
	public List<ShopParamVO> getListShopParamVOByFilter(BasicFilter<ShopParamVO> filter) throws BusinessException {
		try {
			return shopParamDAO.getListShopParamVOByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#getListShopParamVOViewTimeByfilter(ths.dms.core.entities.filter.BasicFilter)
	 */
	@Override
	public List<ShopParamVO> getListShopParamVOViewTimeByFilter(BasicFilter<ShopParamVO> filter) throws BusinessException {
		try {
			return shopParamDAO.getListShopParamVOViewTimeByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#getListObjectShopParamVOByfilter(ths.dms.core.entities.filter.BasicFilter)
	 */
	@Override
	public ObjectVO<ShopParamVO> getListObjectShopParamVOByFilter(BasicFilter<ShopParamVO> filter) throws BusinessException {
		try {
			List<ShopParamVO> lst = shopParamDAO.getListShopParamVOByFilter(filter);
			ObjectVO<ShopParamVO> vo = new ObjectVO<>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#getListObjectShopParamVOViewTimeByfilter(ths.dms.core.entities.filter.BasicFilter)
	 */
	@Override
	public ObjectVO<ShopParamVO> getListObjectShopParamVOViewTimeByFilter(BasicFilter<ShopParamVO> filter) throws BusinessException {
		try {
			List<ShopParamVO> lst = shopParamDAO.getListShopParamVOViewTimeByFilter(filter);
			ObjectVO<ShopParamVO> vo = new ObjectVO<>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#getListProductShopMapByFilter(ths.dms.core.entities.filter.BasicFilter)
	 */
	@Override
	public List<ShopParamVOProduct> getListProductShopMapByFilter(BasicFilter<ShopParamVOProduct> filter) throws BusinessException {
		try {
			return shopParamDAO.getListProductShopMapByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#getListObjectProductShopMapByFilter(ths.dms.core.entities.filter.BasicFilter)
	 */
	@Override
	public ObjectVO<ShopParamVOProduct> getListObjectProductShopMapByFilter(BasicFilter<ShopParamVOProduct> filter) throws BusinessException {
		try {
			List<ShopParamVOProduct> lst = shopParamDAO.getListProductShopMapByFilter(filter);
			ObjectVO<ShopParamVOProduct> vo = new ObjectVO<>();
			vo.setkPaging(filter.getkPaging());
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.business.ShopParamMgr#getProductShopMapByShopProductId(java.lang.Long, java.lang.Long)
	 */
	@Override
	public ProductShopMap getProductShopMapByShopProductId(Long shopId, Long productId) throws BusinessException {
		try {
			return shopParamDAO.getProductShopMapByShopProductId(shopId, productId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

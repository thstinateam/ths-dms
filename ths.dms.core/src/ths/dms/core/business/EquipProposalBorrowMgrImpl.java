package ths.dms.core.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.EquipFormHistory;
import ths.dms.core.entities.EquipLend;
import ths.dms.core.entities.EquipLendDetail;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.vo.EquipBorrowVO;
import ths.dms.core.entities.vo.EquipLendDetailVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.EquipProposalBorrowDAO;
import ths.dms.core.dao.EquipmentManagerDAO;
import ths.dms.core.dao.StaffDAO;

public class EquipProposalBorrowMgrImpl implements EquipProposalBorrowMgr {
	
	@Autowired
	EquipProposalBorrowDAO equipProposalBorrowDAO;
	
	@Autowired
	EquipmentManagerDAO equipmentManagerDAO;
	
	@Autowired
	StaffDAO staffDAO;
	
	@Autowired
	private CommonDAO commonDAO;
	
	@Override
	public ObjectVO<EquipBorrowVO> searchListProposalBorrowVOByFilter(EquipProposalBorrowFilter<EquipBorrowVO> filter) throws BusinessException {
		try {
			List<EquipBorrowVO> lst = equipProposalBorrowDAO.searchListProposalBorrowVOByFilter(filter);
			ObjectVO<EquipBorrowVO> objVO = new ObjectVO<EquipBorrowVO>();
			objVO.setkPaging(filter.getkPaging());
			objVO.setLstObject(lst);
			return objVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipLend getEquipLendByCode(String code) throws BusinessException {
		// TODO Auto-generated method stub		
		try {			
			return equipProposalBorrowDAO.getEquipLendByCode(code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public EquipLend getEquipLendById(Long id) throws BusinessException {
		// TODO Auto-generated method stub
		try {			
			return commonDAO.getEntityById(EquipLend.class, id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEquipLend(EquipLend equipLend) throws BusinessException {
		// TODO Auto-generated method stub
		try {			
			Date sysdate = commonDAO.getSysDate();
			equipLend.setUpdateDate(sysdate);
			if(StatusRecordsEquip.APPROVED.getValue().equals(equipLend.getStatus()) && equipLend.getApproveDate() == null) {
				equipLend.setApproveDate(sysdate);
			} 
			if(StatusRecordsEquip.CANCELLATION.getValue().equals(equipLend.getStatus())) {
//				EquipProposalBorrowFilter<EquipBorrowVO> filter = new EquipProposalBorrowFilter<EquipBorrowVO>();
//				List<Long> lstId = new ArrayList<Long>();
//				lstId.add(equipLend.getEquipLendId());
//				filter.setLstObjectId(lstId);
//				List<EquipLendDetail> lstDetails = equipProposalBorrowDAO.getListEquipLendDetailByFilter(filter);
//				// xoa chi tiet
//				for(int i=0, sz = lstDetails.size(); i<sz; i++){
//					EquipLendDetail detail = lstDetails.get(i);
//					detail.setUpdateDate(sysdate);
//					detail.setUpdateUser(equipLend.getUpdateUser());
//					detail.setStatus(ActiveType.STOPPED.getValue());
//					equipProposalBorrowDAO.updateEquipLendDetail(detail);
//				}
			}
			
			equipProposalBorrowDAO.updateEquipLend(equipLend);	
			// luu lich su bien ban
			createEquipFormHistory(equipLend);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<EquipBorrowVO> getListEquipLendDetailVOForExportByFilter(EquipProposalBorrowFilter<EquipBorrowVO> filter) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<EquipLendDetailVO> lstEquipLendDetailVOs = equipProposalBorrowDAO.getListEquipLendDetailVOByFilter(filter);
			if (lstEquipLendDetailVOs == null || lstEquipLendDetailVOs.size() == 0) {
				return null;
			}

			List<EquipBorrowVO> lstEquipBorrowVOs = new ArrayList<EquipBorrowVO>();
			EquipBorrowVO equipBorrowVO = new EquipBorrowVO();
			equipBorrowVO.setLstEquipLendDetailVOs(new ArrayList<EquipLendDetailVO>());
			EquipLendDetailVO equipLendDetailVO = lstEquipLendDetailVOs.get(0);
			if (equipLendDetailVO != null) {
				/*if (StringUtility.isNullOrEmpty(equipLendDetailVO.getShopCodeKenh())) {
					// kenh ST Mien = NPP
					equipLendDetailVO.setShopCodeMien(equipLendDetailVO.getShopCode());
					equipLendDetailVO.setShopCodeKenh(equipLendDetailVO.getShopCodeVung());
				}*/
				equipBorrowVO.setId(equipLendDetailVO.getEquipLendId());
				equipBorrowVO.setCode(equipLendDetailVO.getEquipLendCode());
				equipBorrowVO.setTotalEquip(equipLendDetailVO.getQuantity());
				equipBorrowVO.getLstEquipLendDetailVOs().add(equipLendDetailVO);
			}

			for (int i = 1, sz = lstEquipLendDetailVOs.size(); i < sz; i++) {
				equipLendDetailVO = lstEquipLendDetailVOs.get(i);
				if (equipLendDetailVO != null) {
					/*if (StringUtility.isNullOrEmpty(equipLendDetailVO.getShopCodeKenh())) {
						// kenh ST Mien = NPP
						equipLendDetailVO.setShopCodeMien(equipLendDetailVO.getShopCode());
						equipLendDetailVO.setShopCodeKenh(equipLendDetailVO.getShopCodeVung());
					}*/
					if (equipLendDetailVO.getEquipLendId().equals(equipBorrowVO.getId())) {
						equipBorrowVO.getLstEquipLendDetailVOs().add(equipLendDetailVO);
						equipBorrowVO.setTotalEquip(equipBorrowVO.getTotalEquip().add(equipLendDetailVO.getQuantity()));
					} else {
						lstEquipBorrowVOs.add(equipBorrowVO);
						// them moi
						equipBorrowVO = new EquipBorrowVO();
						equipBorrowVO.setLstEquipLendDetailVOs(new ArrayList<EquipLendDetailVO>());
						equipBorrowVO.setId(equipLendDetailVO.getEquipLendId());
						equipBorrowVO.setCode(equipLendDetailVO.getEquipLendCode());
						equipBorrowVO.setTotalEquip(equipLendDetailVO.getQuantity());
						equipBorrowVO.getLstEquipLendDetailVOs().add(equipLendDetailVO);
					}
				}
			}
			lstEquipBorrowVOs.add(equipBorrowVO);

			return lstEquipBorrowVOs;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public EquipLend createEquipLendWithListDetail(EquipLend equipLend, List<EquipLendDetail> lstEquipLendDetails) throws BusinessException {
		// TODO Auto-generated method stub		
		try{
			if (equipLend ==null || lstEquipLendDetails ==  null || lstEquipLendDetails.size() == 0) {
				return null;
			}
			String code = equipProposalBorrowDAO.getCodeEquipLend();
			equipLend.setCode(code);
			equipLend.setCreateDate(commonDAO.getSysDate());
			equipLend = equipProposalBorrowDAO.createEquipLend(equipLend);
			// luu lich su bien ban
			createEquipFormHistory(equipLend);
			
			for(int i=0, sz = lstEquipLendDetails.size(); i<sz; i++){
				EquipLendDetail equipLendDetail = lstEquipLendDetails.get(i);
				equipLendDetail.setCreateDate(commonDAO.getSysDate());
				equipLendDetail.setEquipLend(equipLend);
				equipProposalBorrowDAO.createEquipLendDetail(equipLendDetail);
			}
			return equipLend;
		} catch (DataAccessException e) {
			throw new BusinessException();
		}
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateEquipLendWithListDetail(EquipLend equipLend, List<EquipLendDetail> lstEquipLendDetails, EquipProposalBorrowFilter<EquipBorrowVO> borrowFilter) throws BusinessException {
		// TODO Auto-generated method stub
		final int ADD = 0;
		final int DELETE = -1;
		final int EXISTS = 1;
		try{
			Integer statusChange = borrowFilter.getStatus();
			if (equipLend == null) {
				return;
			}
			equipLend.setUpdateDate(commonDAO.getSysDate());			
			if((StatusRecordsEquip.DRAFT.getValue().equals(equipLend.getStatus()) && StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusChange))
				|| (StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipLend.getStatus()) && StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(statusChange))
				|| (StatusRecordsEquip.DRAFT.getValue().equals(equipLend.getStatus()) && StatusRecordsEquip.DRAFT.getValue().equals(statusChange)) 
				|| (StatusRecordsEquip.NO_APPROVAL.getValue().equals(equipLend.getStatus()) && StatusRecordsEquip.NO_APPROVAL.getValue().equals(statusChange))){
				if (lstEquipLendDetails != null && lstEquipLendDetails.size() > 0) {
					EquipProposalBorrowFilter<EquipBorrowVO> filter = new EquipProposalBorrowFilter<EquipBorrowVO>();
					List<Long> lstId = new ArrayList<Long>();
					lstId.add(equipLend.getEquipLendId());
					filter.setLstObjectId(lstId);
					List<EquipLendDetail> lstDetails = equipProposalBorrowDAO.getListEquipLendDetailByFilter(filter);
					// xoa chi tiet
					for (int i = 0, sz = lstDetails.size(); i < sz; i++) {
						EquipLendDetail detail = lstDetails.get(i);
						Integer isExists = DELETE;
						for (int j = 0, szj = lstEquipLendDetails.size(); j < szj; j++) {
							EquipLendDetail equipLendDetail = lstEquipLendDetails.get(j);
							if (detail.getId().equals(equipLendDetail.getId())) {
								isExists = EXISTS;

								// sua chi tiet
								Boolean isChange = false;
								if (!detail.getAddress().equals(equipLendDetail.getAddress())) {
									detail.setAddress(equipLendDetail.getAddress());
									isChange = true;
								}
								if ((!StringUtility.isNullOrEmpty(detail.getBusinessNo()) && !detail.getBusinessNo().equals(equipLendDetail.getBusinessNo()))
									||(!StringUtility.isNullOrEmpty(equipLendDetail.getBusinessNo()) && !equipLendDetail.getBusinessNo().equals(detail.getBusinessNo()))) {
									detail.setBusinessNo(equipLendDetail.getBusinessNo());
									isChange = true;
								}
								if ((detail.getBusinessNoDate() == null && equipLendDetail.getBusinessNoDate() != null)
									||(detail.getBusinessNoDate() != null && equipLendDetail.getBusinessNoDate() == null)
									||(detail.getBusinessNoDate() != null && equipLendDetail.getBusinessNoDate() != null && DateUtility.compareDateWithoutTime(detail.getBusinessNoDate(), equipLendDetail.getBusinessNoDate()) != 0)){
									detail.setBusinessNoDate(equipLendDetail.getBusinessNoDate());
									isChange = true;
								}
								if ((!StringUtility.isNullOrEmpty(detail.getBusinessNoPlace()) && !detail.getBusinessNoPlace().equals(equipLendDetail.getBusinessNoPlace()))
									||(!StringUtility.isNullOrEmpty(equipLendDetail.getBusinessNoPlace()) && !equipLendDetail.getBusinessNoPlace().equals(detail.getBusinessNoPlace()))) {
									detail.setBusinessNoPlace(equipLendDetail.getBusinessNoPlace());
									isChange = true;
								}
								if (!detail.getCapacity().equals(equipLendDetail.getCapacity())) {
									detail.setCapacity(equipLendDetail.getCapacity());
									isChange = true;
								}
								if (!detail.getCustomer().getId().equals(equipLendDetail.getCustomer().getId())) {
									detail.setCustomer( equipLendDetail.getCustomer());
									detail.setCustomerAddress(equipLendDetail.getAddress());
									detail.setCustomerName(equipLendDetail.getCustomerName());
									detail.setPhone(equipLendDetail.getPhone());									
									detail.setAmount(equipLendDetail.getAmount());
									isChange = true;
								}
								if (!detail.getDistrictId().equals(equipLendDetail.getDistrictId())) {
									detail.setDistrictId(equipLendDetail.getDistrictId());
									detail.setDistrictName(equipLendDetail.getDistrictName());
									isChange = true;
								}
								if (!detail.getEquipCategory().getId().equals(equipLendDetail.getEquipCategory().getId())) {
									detail.setEquipCategory(equipLendDetail.getEquipCategory());
									isChange = true;
								}
								if (!detail.getHealthStatus().equals(equipLendDetail.getHealthStatus())) {
									detail.setHealthStatus(equipLendDetail.getHealthStatus());
									isChange = true;
								}
								if (!detail.getIdNo().equals(equipLendDetail.getIdNo())) {
									detail.setIdNo(equipLendDetail.getIdNo());
									isChange = true;
								}
								if (DateUtility.compareDateWithoutTime(detail.getIdNoDate(), equipLendDetail.getIdNoDate()) != 0) {
									detail.setIdNoDate(equipLendDetail.getIdNoDate());
									isChange = true;
								}
								if (!detail.getIdNoPlace().equals(equipLendDetail.getIdNoPlace())) {
									detail.setIdNoPlace(equipLendDetail.getIdNoPlace());
									isChange = true;
								}
								if (!detail.getObjectType().equals(equipLendDetail.getObjectType())) {
									detail.setObjectType(equipLendDetail.getObjectType());
									isChange = true;
								}
								if (!detail.getProvinceId().equals(equipLendDetail.getProvinceId())) {
									detail.setProvinceId(equipLendDetail.getProvinceId());
									detail.setProvinceName(equipLendDetail.getProvinceName());
									isChange = true;
								}
								if (!detail.getRelation().equals(equipLendDetail.getRelation())) {
									detail.setRelation(equipLendDetail.getRelation());
									isChange = true;
								}
								if (!detail.getRepresentative().equals(equipLendDetail.getRepresentative())) {
									detail.setRepresentative(equipLendDetail.getRepresentative());
									isChange = true;
								}
								if (!detail.getStreet().equals(equipLendDetail.getStreet())) {
									detail.setStreet(equipLendDetail.getStreet());
									isChange = true;
								}
								if (DateUtility.compareDateWithoutTime(detail.getTimeLend(), equipLendDetail.getTimeLend()) != 0) {
									detail.setTimeLend(equipLendDetail.getTimeLend());
									isChange = true;
								}
								if (!detail.getWardId().equals(equipLendDetail.getWardId())) {
									detail.setWardId(equipLendDetail.getWardId());
									detail.setWardName(equipLendDetail.getWardName());
									isChange = true;
								}
								if (!detail.getQuantity().equals(equipLendDetail.getQuantity())) {
									detail.setQuantity(equipLendDetail.getQuantity());
									isChange = true;
								}
								if (isChange) {
									detail.setUpdateDate(commonDAO.getSysDate());
									equipProposalBorrowDAO.updateEquipLendDetail(detail);
								}
								break;
							}
						}
						if (isExists == DELETE) {
							commonDAO.deleteEntity(detail);
							lstDetails.remove(i);
							i--;
							sz--;
						}
					}
					// them chi tiet
					for (int i = 0, sz = lstEquipLendDetails.size(); i < sz; i++) {
						EquipLendDetail equipLendDetail = lstEquipLendDetails.get(i);
						Integer isExists = ADD;
						for (int j = 0, szj = lstDetails.size(); j < szj; j++) {
							EquipLendDetail detail = lstDetails.get(j);
							if (detail.getId().equals(equipLendDetail.getId())) {
								isExists = EXISTS;
								break;
							}
						}
						if (isExists == ADD) {
							equipLendDetail.setCreateDate(commonDAO.getSysDate());
							equipLendDetail.setEquipLend(equipLend);
							equipProposalBorrowDAO.createEquipLendDetail(equipLendDetail);
						}
					}
				}
			} else if(StatusRecordsEquip.CANCELLATION.getValue().equals(statusChange)) {
//				EquipProposalBorrowFilter<EquipBorrowVO> filter = new EquipProposalBorrowFilter<EquipBorrowVO>();
//				List<Long> lstId = new ArrayList<Long>();
//				lstId.add(equipLend.getEquipLendId());
//				filter.setLstObjectId(lstId);
//				List<EquipLendDetail> lstDetails = equipProposalBorrowDAO.getListEquipLendDetailByFilter(filter);
//				// xoa chi tiet
//				for(int i=0, sz = lstDetails.size(); i<sz; i++){
//					EquipLendDetail detail = lstDetails.get(i);
//					detail.setUpdateDate(commonDAO.getSysDate());
//					detail.setUpdateUser(equipLend.getUpdateUser());
//					equipProposalBorrowDAO.updateEquipLendDetail(detail);
//				}			
			}
			if(StatusRecordsEquip.APPROVED.getValue().equals(statusChange) && equipLend.getApproveDate() == null) {
				equipLend.setApproveDate(commonDAO.getSysDate());
			} 
			if (!statusChange.equals(equipLend.getStatus())) {
				equipLend.setStatus(statusChange);
			}
			// cap nhat bien ban
			equipProposalBorrowDAO.updateEquipLend(equipLend);
			// luu lich su bien ban
			createEquipFormHistory(equipLend);
		} catch (DataAccessException e) {
			throw new BusinessException();
		}
	}
	
	private EquipFormHistory createEquipFormHistory(EquipLend equipLend) throws BusinessException{
		try{
			Date sysdate = commonDAO.getSysDate();
			// luu lich su bien ban
			EquipFormHistory equipFormHistory = new EquipFormHistory();
			equipFormHistory.setActDate(sysdate);
			equipFormHistory.setCreateDate(sysdate);
			if (StringUtility.isNullOrEmpty(equipLend.getUpdateUser())) {
				equipFormHistory.setCreateUser(equipLend.getCreateUser());
			} else {
				equipFormHistory.setCreateUser(equipLend.getUpdateUser());
			}
			equipFormHistory.setDeliveryStatus(equipLend.getDeliveryStatus());
			equipFormHistory.setRecordId(equipLend.getEquipLendId());
			equipFormHistory.setRecordStatus(equipLend.getStatus());
			equipFormHistory.setRecordType(EquipTradeType.EQUIP_LEND.getValue());
			Staff staff = staffDAO.getStaffByCode(equipLend.getCreateUser());
			equipFormHistory.setStaff(staff);
			equipFormHistory = equipmentManagerDAO.createEquipmentHistoryRecord(equipFormHistory);
			
			return equipFormHistory;
		} catch (DataAccessException e) {
			throw new BusinessException();
		}
	}
}

package ths.dms.core.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.Organization;
import ths.dms.core.entities.ShopType;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrganizationFilter;
import ths.dms.core.entities.enumtype.OrganizationNodeType;
import ths.dms.core.entities.enumtype.OrganizationUnitTypeFilter;
import ths.dms.core.entities.filter.OrganizationSystemFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrganizationUnitTypeVO;
import ths.dms.core.entities.vo.OrganizationVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.OrganizationDAO;

/**
 * The Interface OrganizationMgrImpl.
 * 
 * @author vuongmq
 * @date 11/02/2015
 */
public class OrganizationMgrImpl implements OrganizationMgr {

	/** BEGIN VUONGMQ */

	@Override
	public List<Organization> getListOrganization(OrganizationSystemFilter<Organization> filter) throws BusinessException {
		try {
			return organizationDAO.getListOrganization(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<OrganizationVO> getListOrganizationById4ContextMenu(Long orgId) throws BusinessException {
		try {
			return organizationDAO.getListOrganizationById4ContextMenu(orgId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Staff> getListStaffExitsStaffTypeIdInOrganization(Long orgId,Long staffId) throws BusinessException {
		try {
			return organizationDAO.getListStaffExitsStaffTypeIdInOrganization(orgId,staffId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Organization> getListOrganizationParent(OrganizationSystemFilter<Organization> filter) throws BusinessException {
		try {
			return organizationDAO.getListOrganizationParent(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public OrganizationVO getOrganizationVOById(Long id) throws BusinessException {
		try {
			return organizationDAO.getOrganizationVOById(id);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<OrganizationVO> getListOrganizationSystemVO(OrganizationSystemFilter<OrganizationVO> filter) throws BusinessException {
		try {
			return organizationDAO.getListOrganizationSystemVO(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public OrganizationVO getOrganizationVOByParent() throws BusinessException {
		try {
			return organizationDAO.getOrganizationVOByParent();
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<OrganizationVO> getListOrganizationSystemVOShop(OrganizationSystemFilter<OrganizationVO> filter) throws BusinessException {
		try {
			return organizationDAO.getListOrganizationSystemVOShop(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<OrganizationVO> getListOrganizationSystemVOStaff(OrganizationSystemFilter<OrganizationVO> filter) throws BusinessException {
		try {
			return organizationDAO.getListOrganizationSystemVOStaff(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<StaffType> getListStaffType(OrganizationSystemFilter<StaffType> filter) throws BusinessException {
		try {
			return organizationDAO.getListStaffType(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/** END VUONGMQ */
	
	/** PHUOCDH2 **/

	@Autowired
	OrganizationDAO organizationDAO;

	@Autowired
	CommonDAO commonDAO;

	@Override
	public ShopType createShopType(ShopType shopType) throws BusinessException {
		try {
			return commonDAO.createEntity(shopType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public StaffType createStaffType(StaffType staffType) throws BusinessException {
		try {
			return commonDAO.createEntity(staffType);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateStaffType(StaffType staffType) throws BusinessException {
		try {
			commonDAO.updateEntity(staffType);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Organization createOrganization(Organization organization) throws BusinessException {
		try {

			return commonDAO.createEntity(organization);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateOrganization(Organization organization) throws BusinessException {
		try {
			commonDAO.updateEntity(organization);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteOrganization(Organization organization) throws BusinessException {
		try {
			commonDAO.deleteEntity(organization);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public Organization getOrganizationById(Long id) throws BusinessException {
		try {
			return id == null ? null : organizationDAO.getOrganizationById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public void updateListOrganization(List<Organization> lstOrganization) throws BusinessException {
		// TODO Auto-generated method stub

	}

	@Override
	public Organization getOrganizationByCode(String code) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * @Override public Organization getOrganizationByCode(String code) throws
	 * BusinessException { try { return
	 * organizationDAO.getOrganizationByCode(code); } catch (DataAccessException
	 * e) {
	 * 
	 * throw new BusinessException(e); } }
	 */

	public ObjectVO<OrganizationUnitTypeVO> getListOrganizationUnitType(OrganizationUnitTypeFilter filter, KPaging<OrganizationUnitTypeVO> kPaging) throws BusinessException {
		try {
			ObjectVO<OrganizationUnitTypeVO> res = new ObjectVO<OrganizationUnitTypeVO>();
			List<OrganizationUnitTypeVO> lst = organizationDAO.getListOrganizationUnitType(filter, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public StaffType getStaffTypeById(long id) throws BusinessException {
		try {
			return organizationDAO.getStaffTypeById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Integer getCountByPrefixShop(String prefix) throws BusinessException {
		try {
			return organizationDAO.getCountByPrefixShop(prefix);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Integer getCountByPrefixStaff(String prefix) throws BusinessException {
		try {
			return organizationDAO.getCountByPrefixStaff(prefix);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * kiem tra loai don vi, chuc vu nay da co tren node cha hay chua 
	 * @author phuocdh2
	 * @since 03/05/2015
	 * @description : nhan vao loai don vi ,chuc vu va kiem tra loai don vi chuc vu nay da co tren node cha chua
	 */
	@Override
	public List<Organization> getListOrgExistParentNodeByFilter(OrganizationFilter filter)
			throws BusinessException {
		try {
			return organizationDAO.getListOrgExistParentNodeByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * kiem tra loai don vi, chuc vu nay da co duoi node con hay chua 
	 * @author phuocdh2
	 * @since 03/05/2015
	 * @description : nhan vao loai don vi ,chuc vu va kiem tra loai don vi chuc vu nay da co duoi node con chua
	 */
	@Override
	public List<Organization> getListOrgExistSubNodeByFilter(OrganizationFilter filter)
			throws BusinessException {	
		try {
			return organizationDAO.getListOrgExistSubNodeByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}		
	}
	/** END PHUOCDH2 **/
	@Override
	public ShopType getShopTypeById(long id) throws BusinessException {
		try {
			return organizationDAO.getShopTypeById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateShopType(ShopType shopType) throws BusinessException {
		try {
			commonDAO.updateEntity(shopType);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}

	@Override
	public List<Organization> getListOrganizationByFilter(OrganizationFilter filter) throws BusinessException {
		try {
			return organizationDAO.getListOrganizationByFilter(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void chooseOrganizationType(OrganizationUnitTypeVO vo, LogInfoVO logInfo) throws BusinessException {
		if (vo == null) {
			throw new IllegalArgumentException("organizationUnitTypeVO is null");
		}
		try {
			if (vo != null && vo.getOrganizationId() != null && vo.getOrganizationId() == 0L) {
				/** vuongmq; 24/02/2015; xu ly tao organization lan dau tien */
				Date sys = commonDAO.getSysDate();
				Organization org = new Organization();
				/**
				 * NodeType =1; loai don vi NodeOrdinal =1; so thuc tu = 1
				 * ParentOrg = null; node ParentOrg = null
				 */
				org.setNodeType(OrganizationNodeType.parseValue(vo.getNodeType()));
				org.setNodeTypeId(vo.getNodeTypeId());
				org.setStatus(ActiveType.RUNNING);
				org.setNodeOrdinal(1);
				org.setCreateDate(sys);
				org.setCreateUser(logInfo.getStaffCode());
				org = commonDAO.createEntity(org);
			} else {
				Date sys = commonDAO.getSysDate();
				Organization org = new Organization();
				org.setNodeType(OrganizationNodeType.parseValue(vo.getNodeType()));
				org.setNodeTypeId(vo.getNodeTypeId());
				Organization parentOrg = commonDAO.getEntityById(Organization.class, vo.getOrganizationId());
				org.setParentOrg(parentOrg);
				org.setStatus(ActiveType.RUNNING);
				Integer maxNodeOrdinal = organizationDAO.getMaxNodeOrdinal();
				org.setNodeOrdinal(maxNodeOrdinal + 1);
				org.setCreateDate(sys);
				org.setCreateUser(logInfo.getStaffCode());
				org = commonDAO.createEntity(org);
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/***
	 * @author vuongmq
	 * @date 24/02/2015 xu ly save order nodeOrdinal theo thu tu tren cay keo
	 *       tha
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void saveOrganization(List<Long> lstOrgIdSort, List<Integer> lstOrgSortNodeOrdinal, LogInfoVO logInfo) throws BusinessException {
		if (lstOrgIdSort == null && lstOrgSortNodeOrdinal == null) {
			throw new IllegalArgumentException("lstOrgIdSort is null and lstOrgSortNodeOrdinal is null");
		}
		try {
			Date sys = commonDAO.getSysDate();
			for (int i = 0, sz = lstOrgIdSort.size(); i < sz; i++) {
				if (lstOrgIdSort.get(i) != null) {
					Organization org = organizationDAO.getOrganizationById(lstOrgIdSort.get(i));
					org.setNodeOrdinal(lstOrgSortNodeOrdinal.get(i));
					org.setUpdateDate(sys);
					org.setUpdateUser(logInfo.getStaffCode());
					organizationDAO.updateOrganization(org, logInfo);
				}
			}
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * Lay organization tuong ung voi cac dieu kien trong staff_type, shop_type
	 * @author phuocdh2
	 * @since 03/26/2015
	 * @description : nhan vao staff_code, staff_name,staff.status tra ra organization ung voi staff_name  do  
	 */
	@Override
	public Organization getOrgJoinTypeByFilter(OrganizationFilter filter) throws BusinessException {
		try {
			return organizationDAO.getOrgJoinTypeByFilter(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public StaffType getStaffTypeByCondition(OrganizationFilter filter) throws BusinessException {
		try {
			return organizationDAO.getStaffTypeByCondition(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public ShopType getShopTypeByCondition(OrganizationFilter filter) throws BusinessException {
		try {
			return organizationDAO.getShopTypeByCondition(filter);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkParentOrg(OrganizationFilter filter) throws BusinessException {
		try {
			return organizationDAO.checkParentOrg(filter);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
}

package ths.dms.core.business;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SaleDays;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.dao.ExceptionDayDAO;
import ths.dms.core.dao.SaleDayDAO;

public class SaleDayMgrImpl implements SaleDayMgr {

	@Autowired
	SaleDayDAO saleDayDAO;
	
	@Autowired
	ExceptionDayDAO exceptionDayDAO;
	
	@Override
	public SaleDays createSaleDay(SaleDays saleDay, LogInfoVO logInfo) throws BusinessException {
		try {
			return saleDayDAO.createSaleDay(saleDay, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public void updateSaleDay(SaleDays saleDay, LogInfoVO logInfo) throws BusinessException {
		try {
			saleDayDAO.updateSaleDay(saleDay, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteSaleDay(SaleDays saleDay, LogInfoVO logInfo) throws BusinessException{
		try {
			saleDayDAO.deleteSaleDay(saleDay, logInfo);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public SaleDays getSaleDayById(Long id) throws BusinessException{
		try {
			return id==null?null:saleDayDAO.getSaleDayById(id);
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<SaleDays> getListSaleDay(KPaging<SaleDays> kPaging, Integer fromYear,
			Integer toYear, ActiveType status) throws BusinessException {
		try {
			List<SaleDays> lst = saleDayDAO.getListSaleDay(kPaging, fromYear, toYear, status);
			ObjectVO<SaleDays> vo = new ObjectVO<SaleDays>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {

			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean isUsingByOthers(long saleDayId) throws BusinessException {
		try {
			return saleDayDAO.isUsingByOthers(saleDayId);
		}
		catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public SaleDays getSaleDayByYear(Integer year) throws BusinessException {
		if (year == null) {
			throw new BusinessException("year is null");
		}
		try {
			return saleDayDAO.getSaleDayByYear(year);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Integer getSaleDayByDate(Date date) throws BusinessException {
		if (date == null) {
			throw new BusinessException("month is null");
		}
		try {
			return saleDayDAO.getSaleDayByDate(date);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Integer getNumOfHoliday(Date fromDate, Date toDate) throws BusinessException {
		try {
			return exceptionDayDAO.getNumOfHoliday(fromDate, toDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Integer getNumberWorkingDayByFromDateAndToDate(Date fromDate, Date toDate) throws BusinessException {
		SimpleDateFormat sfd = new SimpleDateFormat("yyyyMMdd");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sfd.parse(sfd.format(fromDate));
			endDate = sfd.parse(sfd.format(toDate));
		} catch (ParseException e) {
			throw new BusinessException(e);
		}
	    Calendar startCal;
	    Calendar endCal;
	    startCal = Calendar.getInstance();
	    startCal.setTime(startDate);
	    endCal = Calendar.getInstance();
	    endCal.setTime(endDate);
	    int workDays = 0;
	    if (startCal.getTimeInMillis() == endCal.getTimeInMillis() && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
	        return 1;
		}
		if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
			startCal.setTime(endDate);
			endCal.setTime(startDate);
		}
		
		while(startCal.getTimeInMillis() < endCal.getTimeInMillis()) {
			startCal.add(Calendar.DAY_OF_MONTH, 1);
			if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				++workDays;
			}
		}
		return workDays > 0?workDays:1;
	}
	
	//Xac dinh khoang cach de cho phep tra hang hay khong
	//Tinh khoang cach giua hay ngay khong tinh ngay chu nhat va ngay le
	@Override
	public Integer getNumberWorkingDayByFromDateAndToDateEx(Date fromDate, Date toDate) throws BusinessException {
		SimpleDateFormat sfd = new SimpleDateFormat("yyyyMMdd");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = sfd.parse(sfd.format(fromDate));
			endDate = sfd.parse(sfd.format(toDate));
		} catch (ParseException e) {
			throw new BusinessException(e);
		}
	    Calendar startCal;
	    Calendar endCal;
	    startCal = Calendar.getInstance();
	    startCal.setTime(startDate);
	    endCal = Calendar.getInstance();
	    endCal.setTime(endDate);
	    int workDays = 0;
	    if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
	        return 0;
		}
		if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
			startCal.setTime(endDate);
			endCal.setTime(startDate);
		}
		while(startCal.getTimeInMillis() < endCal.getTimeInMillis()) {
			startCal.add(Calendar.DAY_OF_MONTH, 1);
//			if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
//				++workDays;
//			}
			++workDays;
		}
		
		//countDayOff: So ngay nghi le khong giua hay ngay khong tin ngay chu nhat
		int countDayOff = 0;
	    try {
			countDayOff = exceptionDayDAO.getCountExDayOffBetween(startDate, DateUtility.getPreviousDay(endDate));
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	    return workDays > countDayOff ? workDays - countDayOff : 0;
	}
}

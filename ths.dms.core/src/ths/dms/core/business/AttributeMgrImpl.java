package ths.dms.core.business;

import java.util.List;

import org.apache.poi.hssf.record.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Attribute;
import ths.dms.core.entities.AttributeDetail;
import ths.dms.core.entities.AttributeValue;
import ths.dms.core.entities.AttributeValueDetail;
import ths.dms.core.entities.CustomerAttribute;
import ths.dms.core.entities.CustomerAttributeEnum;
import ths.dms.core.entities.ProductAttribute;
import ths.dms.core.entities.ProductAttributeEnum;
import ths.dms.core.entities.StaffAttribute;
import ths.dms.core.entities.StaffAttributeEnum;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeDetailFilter;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumFilter;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.AttributeFilter;
import ths.dms.core.entities.enumtype.AttributeVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.TableHasAttribute;
import ths.dms.core.entities.vo.DynamicAttributeVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.AttributeDAO;
import ths.dms.core.dao.AttributeDetailDAO;
import ths.dms.core.dao.AttributeValueDAO;
import ths.dms.core.dao.AttributeValueDetailDAO;
import ths.dms.core.dao.CustomerAttributeDAO;
import ths.dms.core.dao.ProductAttributeDAO;
import ths.dms.core.dao.StaffAttributeDAO;

public class AttributeMgrImpl implements AttributeMgr {

	/** The Constant CUSTOMER_OBJECT. */
	private static final int CUSTOMER_OBJECT = 1;

	/** The Constant PRODUCT_OBJECT. */
	private static final int PRODUCT_OBJECT = 2;
	
	/** The Constant STAFF_OBJECT  */
	private static final int STAFF_OBJECT = 3;
	
	@Autowired
	AttributeDAO attributeDAO;
	
	@Autowired
	AttributeDetailDAO attributeDetailDAO;
	
	@Autowired
	AttributeValueDetailDAO attributeValueDetailDAO;
	
	@Autowired
	AttributeValueDAO attributeValueDAO;

	@Autowired
	StaffAttributeDAO staffAttributeDAO;
	
//	@Autowired
//	StaffAttributeDetailDAO staffAttributeDetailDAO;
	
	@Autowired
	CustomerAttributeDAO customerAttributeDAO;
	
	@Autowired
	ProductAttributeDAO productAttributeDAO;
	
	@Override
	public Attribute createAttribute(Attribute attribute) throws BusinessException {
		try {
			return attributeDAO.createAttribute(attribute);
		} catch (DataAccessException e) {
			
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteAttribute(Attribute attribute) throws BusinessException {
		try {
			attributeDAO.deleteAttribute(attribute);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Attribute getAttributeByCode(String table, String code)
			throws BusinessException {
		try {
			return attributeDAO.getAttributeByCode(table, code);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Attribute getAttributeById(long id) throws BusinessException {
		try {
			return attributeDAO.getAttributeById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateAttribute(Attribute attribute) throws BusinessException {
		try {
			attributeDAO.updateAttribute(attribute);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<AttributeVO> getListAttributeVO(
			AttributeFilter filter, KPaging<AttributeVO> kPaging)
			throws BusinessException{
		try {
			ObjectVO<AttributeVO> res = new ObjectVO<AttributeVO>();
			List<AttributeVO> lst = attributeDAO.getListAttributeVO(filter, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<Attribute> getListAttribute(KPaging<Attribute> kPaging,
			TableHasAttribute table, String attributeCode,
			String attributeName, AttributeColumnType columnType,
			ActiveType status) throws BusinessException {
		if (null == table) {
			throw new IllegalArgumentException("table is null");
		}

		try {
			List<Attribute> lst = attributeDAO.getListAttribute(kPaging, table,
					attributeCode, attributeName, columnType, status);
			ObjectVO<Attribute> vo = new ObjectVO<Attribute>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<AttributeDetailVO> getListAttributeDetailByAttributeId(
			KPaging<AttributeDetailVO> kPaging, Long id, ActiveType status)
			throws BusinessException {
		try {
			List<AttributeDetailVO> lst = attributeDetailDAO
					.getListAttributeDetailByAttributeId(kPaging, id, status);
			ObjectVO<AttributeDetailVO> vo = new ObjectVO<AttributeDetailVO>();
			vo.setkPaging(kPaging);
			vo.setLstObject(lst);
			return vo;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public AttributeDetail createAttributeDetail(AttributeDetail attributeDetail) throws BusinessException{
		try {
			return attributeDetailDAO.createAttributeDetail(attributeDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void deleteAttributeDetail(AttributeDetail attributeDetail) throws BusinessException{
		try {
			attributeDetailDAO.deleteAttributeDetail(attributeDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public void deleteListAttributeDetailByAttribute(Long attributeId) throws BusinessException{
		try {
			attributeDetailDAO.deleteListAttributeDetailByAttribute(attributeId);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public void updateAttributeDetail(AttributeDetail attributeDetail) throws BusinessException{
		try {
			attributeDetailDAO.updateAttributeDetail(attributeDetail);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public AttributeDetail getAttributeDetailById(long id) throws BusinessException{
		try {
			return attributeDetailDAO.getAttributeDetailById(id);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public AttributeDetail getAttributeDetailByCode(Long attributeId,
			String attributeDetailCode) throws BusinessException{
		try {
			return attributeDetailDAO.getAttributeDetailByCode(attributeId, attributeDetailCode);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public ObjectVO<AttributeDetailVO> getListAttributeDetailVO(
			AttributeDetailFilter filter, KPaging<AttributeDetailVO> kPaging)
			throws BusinessException{
		try {
			ObjectVO<AttributeDetailVO> res = new ObjectVO<AttributeDetailVO>();
			List<AttributeDetailVO> lst = attributeDetailDAO.getListAttributeDetailVO(filter, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean isUsingByOther(String attributeCode) throws BusinessException {
		try {
			return attributeDAO.isUsingByOther(attributeCode);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public Boolean isExistAttributeDetailCode(Long attributeId, String attributeDetailCode)
			throws BusinessException {
		try {
			return attributeDetailDAO.isExistAttributeDetailCode(attributeId, attributeDetailCode);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<AttributeValueDetail> getListValue4Select(Long objId, Long attributeId, ActiveType status) throws BusinessException {
		try {
			List<AttributeValueDetail> listValue = attributeValueDetailDAO.getListAttrValueDetailFromObj(objId, attributeId, status);
			return listValue;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public AttributeValue getValueOfObject(Long objId, Long attributeId) throws BusinessException {
		try {
			AttributeValue attrValue = attributeValueDAO.getValueOfObject(objId, attributeId);
			return attrValue;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<AttributeDynamicVO> getListAttributeDynamicVO(AttributeDynamicFilter filter, KPaging<AttributeDynamicVO> kPaging)
			throws BusinessException{
		try {
			ObjectVO<AttributeDynamicVO> res = new ObjectVO<AttributeDynamicVO>();
			List<AttributeDynamicVO> lst = null;
			if(filter.getApplyObject()!=null){
				if(filter.getApplyObject().equals(CUSTOMER_OBJECT)){
					lst = customerAttributeDAO.getListCustomerAttributeVO(filter, kPaging);
				}else if(filter.getApplyObject().equals(PRODUCT_OBJECT)){
					lst = productAttributeDAO.getListProductAttributeVO(filter, kPaging);
				}else if(filter.getApplyObject().equals(STAFF_OBJECT)){
					lst = staffAttributeDAO.getListStaffAttributeVO(filter, kPaging);
				}
			}
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public ObjectVO<AttributeEnumVO> getListAttributeDetailDynamicVO(AttributeEnumFilter filter, KPaging<AttributeEnumVO> kPaging)
			throws BusinessException{
		try {
			ObjectVO<AttributeEnumVO> res = new ObjectVO<AttributeEnumVO>();
			List<AttributeEnumVO> lst = null;
			if(filter.getApplyObject()!=null){
				if(filter.getApplyObject().equals(CUSTOMER_OBJECT)){
					lst = customerAttributeDAO.getListCustomerAttributeEnumVO(filter, kPaging);
				}else if(filter.getApplyObject().equals(PRODUCT_OBJECT)){
					lst = productAttributeDAO.getListProductAttributeEnumVO(filter, kPaging);
				}else if(filter.getApplyObject().equals(STAFF_OBJECT)){
					lst = staffAttributeDAO.getListStaffAttributeEnumVO(filter, kPaging);
				}
			}
			res.setkPaging(kPaging);
			res.setLstObject(lst);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public Boolean checkExistAttributeDynamic(AttributeDynamicFilter filter) throws BusinessException {
		try {
			Boolean bool = false;
			if(filter.getApplyObject()!=null){
				if(filter.getApplyObject().equals(CUSTOMER_OBJECT)){
					CustomerAttribute att = customerAttributeDAO.getCustomerAttributeByCode(filter.getAttributeCode());
					if(att!=null){
						bool = true;
					}
				}else if(filter.getApplyObject().equals(PRODUCT_OBJECT)){
					ProductAttribute att = productAttributeDAO.getProductAttributeByCode(filter.getAttributeCode());
					if(att!=null){
						bool = true;
					}
				}else if(filter.getApplyObject().equals(STAFF_OBJECT)){
					StaffAttribute att = staffAttributeDAO.getStaffAttributeByCode(filter.getAttributeCode());
					if(att!=null){
						bool = true;
					}
				}
			}
			return bool;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public Boolean checkExistAttributeEnumDynamic(AttributeEnumFilter filter) throws BusinessException {
		try {
			Boolean bool = false;
			if(filter.getApplyObject()!=null){
				if(filter.getApplyObject().equals(CUSTOMER_OBJECT)){
					CustomerAttributeEnum att = customerAttributeDAO.getCustomerAttributeEnumByCode(filter.getCode());
					if(att!=null){
						bool = true;
					}
				}else if(filter.getApplyObject().equals(PRODUCT_OBJECT)){
					ProductAttributeEnum att = productAttributeDAO.getProductAttributeEnumByCode(filter.getCode());
					if(att!=null){
						bool = true;
					}
				}else if(filter.getApplyObject().equals(STAFF_OBJECT)){
					StaffAttributeEnum att = staffAttributeDAO.getStaffAttributeEnumByCode(filter.getCode());
					if(att!=null){
						bool = true;
					}
				}
			}
			return bool;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public <T> List<DynamicAttributeVO> getDynamicAttribute(Class<T> clazz, ActiveType status) throws BusinessException {
		try {
			return attributeDAO.getDynamicAttribute(clazz, status);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
}
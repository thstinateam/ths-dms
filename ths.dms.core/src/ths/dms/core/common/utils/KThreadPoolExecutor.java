package ths.dms.core.common.utils;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * The Class KThreadPoolExecutor.
 */
public class KThreadPoolExecutor {

	private static boolean isThreadEnable = true;

	private static final long KEEP_ALIVE_TIME = 10;

	private static final int MAX_POOL_SIZE = 100;

	private static final int POOL_SIZE = 50;

	/** The thread pool. */
	private static ThreadPoolExecutor threadPool;

	static {
		threadPool = new ThreadPoolExecutor(POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME, TimeUnit.SECONDS,
				new LinkedBlockingQueue<Runnable>());
		threadPool.allowCoreThreadTimeOut(true);
	}

	/**
	 * Run task.
	 * 
	 * @param task
	 *            the task
	 */
	public static void execute(Runnable task) {
		if (isThreadEnable) {
			threadPool.execute(task);
		} else {
			task.run();
		}
	}

	public static void setThreadEnable(boolean isThreadEnable) {
		KThreadPoolExecutor.isThreadEnable = isThreadEnable;
	}

	/**
	 * Shut down.
	 */
	public static void shutDown() {
		threadPool.shutdown();
	}
}

package ths.dms.core.common.utils;

public class PromotionCalc {
	public static final String ZV01 = "ZV01";//Line-Qtty- Percent
	public static final String ZV02 = "ZV02";//Line-Qtty-Amt
	public static final String ZV03 = "ZV03";//Line-Qtty-FreeItem
	public static final String ZV04 = "ZV04";//Line-Amt-Percent
	public static final String ZV05 = "ZV05";//Line-Amt-Amt
	public static final String ZV06 = "ZV06";//Line-Amt-FreeItem
	public static final String ZV07 = "ZV07";//Group-Qtty-Percent
	public static final String ZV08 = "ZV08";//Group-Qtty-Amount
	public static final String ZV09 = "ZV09";//Group-Qtty-FreeItem
	public static final String ZV10 = "ZV10";//Group-Amt-Percent
	public static final String ZV11 = "ZV11";//Group-Amt-Amount
	public static final String ZV12 = "ZV12";//Group-Amt-FreeItem
	public static final String ZV13 = "ZV13";//Bundle-Qtty-Percent
	public static final String ZV14 = "ZV14";//Bundle-Qtty-Amount
	public static final String ZV15 = "ZV15";//Bundle-Qtty-FreeItem
	public static final String ZV16 = "ZV16";//Bundle-Amt-Percent
	public static final String ZV17 = "ZV17";//Bundle-Amt-Amt
	public static final String ZV18 = "ZV18";//Bundle-Amt-FreeItem
	public static final String ZV19 = "ZV19";//Docmt-Amt-Percent
	public static final String ZV20 = "ZV20";//Docmt-Amt-Amt
	public static final String ZV21 = "ZV21";//Docmt-Amt-FreeItem
	
	//Dung cho viec lam tron
	// Lam tron 2 chu so sau dau phay
	public static int ROUND_PERCENT = 100;
	// Lay gia tri thap phan chinh xac
	public static int ROUND_NO_PERCENT = 100 * ROUND_PERCENT;
	
}

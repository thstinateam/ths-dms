package ths.dms.core.common.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Date;

public class GeneralUtil {
	/**
	 * Gets the format.
	 * @param quantity
	 * @param convfact
	 * @author khanhnl
	 * @since Sep 04, 2012
	 */
	public String cellQuantityFormatter(Integer quantity, Integer i_convfact){
		Integer bigUnit = 0;
		Integer smallUnit = 0;
		Integer convfact = 1;
		String cellQty = "";
		if(i_convfact != null && i_convfact > 0){
			convfact = i_convfact;
		}
		bigUnit = quantity/convfact;
		smallUnit = quantity%convfact;
		cellQty =  bigUnit.toString() + '/' + smallUnit.toString();
		return cellQty;
	}
	/**
	 * Gets the quantity.
	 * @param amount
	 * @param convfact
	 * @author khanhnl
	 * @since Sep 04, 2012
	 */
	public static Integer getQuantity(String amount, Integer convfact, String option){
		if(convfact==null || convfact <= 0){
			convfact = 1;
		}
		int bigUnit = 0;
		int smallUnit = 0;
		if(!amount.contains("/")){
			smallUnit =Integer.parseInt(amount);
		} else{
			String[] arrCount = amount.split("/");
			if(arrCount.length > 0){
				if(arrCount[0].trim().length() == 0){
					bigUnit = 0;
				} else{
					bigUnit =  Integer.parseInt(arrCount[0].trim());
				}
				if(arrCount.length >1){
					if(arrCount[1].trim().length() == 0){
						smallUnit = 0;
					}else {
						smallUnit = Integer.parseInt(arrCount[1].trim());
						if(smallUnit>=convfact){
							int addBig = smallUnit/convfact;
							int mod = smallUnit%convfact;
							bigUnit = bigUnit + addBig;
							smallUnit = mod;
						}	
					}
				}
			}
		}
		if(option.equalsIgnoreCase("Quantity")){
			return bigUnit*convfact + smallUnit;
		}
		else if(option.equalsIgnoreCase("Package")){
			return bigUnit;
		}
		else if(option.equalsIgnoreCase("Unit")){
			return smallUnit;
		}
		return 0;
	}
	
	/**
	 * @author nald
	 * Ham support cho getListByQueryDynamic
	 * @description gan gia tri tu "value" dong cho thuoc tinh co ten "proName"
	 */
	public static void applyValue2Property(Object object, String proName,
			String value) {
		try {
			Class<?> clz = object.getClass().getDeclaredField(proName).getType();
			String proNameUpperFirst = proName.substring(0, 1).toUpperCase() + proName.substring(1);
			Method setMethod = object.getClass().getMethod("set" + proNameUpperFirst, clz);
			if(value == ""){
				setMethod.invoke(object, new Object[] {null});
			}
			else{
				if (Integer.class.equals(clz) || int.class.equals(clz)) {
					setMethod.invoke(object, Integer.parseInt(value));
				} else if (Long.class.equals(clz) || long.class.equals(clz)) {
					setMethod.invoke(object, Long.parseLong(value));
				} else if (String.class.equals(clz)) {
					setMethod.invoke(object, value);
				} else if (Double.class.equals(clz) || double.class.equals(clz)) {
					setMethod.invoke(object, Double.parseDouble(value));
				} else if (Float.class.equals(clz) || float.class.equals(clz)) {
					setMethod.invoke(object, Float.parseFloat(value));
				} else if (Boolean.class.equals(clz)
						|| boolean.class.equals(clz)) {
					setMethod.invoke(object, Boolean.parseBoolean(value));
				} else if (java.util.Date.class.equals(clz) || Date.class.equals(clz)) {
					// parse kieu Date
					setMethod.invoke(object, DateUtility.parse(value, "yyyy-MM-dd HH:mm:ss"));
				} else if (BigDecimal.class.equals(clz)) {
					setMethod.invoke(object, new BigDecimal(value));
				} else {
					// error
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @author nald
	 * Ham support cho getListByQueryDynamic
	 * @description tim xem gia tri "key" co nam trong danh sach fields hay khong
	 */
	public static String hasInFields(Object key, Field[] fields) {
		for (Field field : fields) {
			if (field.getName().toUpperCase().equals(key))
				return field.getName();
		}
		return null;
	}
}

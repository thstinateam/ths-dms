package ths.dms.core.common.utils;

import java.util.Date;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
@Aspect
public class AopLogging {
	
	//@Before("execution(* code.CustomerBo.*(..))")
	public void logBefore(JoinPoint joinPoint) {
		//...
		System.out.println("before");
	}
	
	public Object logAround(ProceedingJoinPoint pjp) throws Throwable {
		Date begin = new Date();
		Object obj = null;
		
		String message = pjp.toShortString() + " - Params: ";
		Object[] args = pjp.getArgs();
		if (args != null)
			for (Object arg: args) {
				message = message + String.valueOf(arg) + ", ";
			}
		message += "; ";
		
		try {
			obj =pjp.proceed();
		}
		catch (Exception ex) {
			
			LogUtility.logError(ex, message);
			throw ex;
		}
		Date end = new Date();
		Long time = end.getTime() - begin.getTime();
		LogUtility.logPerf(message, time.toString());
		
		return obj;
	}
}

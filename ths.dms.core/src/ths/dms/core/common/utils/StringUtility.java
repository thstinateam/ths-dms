package ths.dms.core.common.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Shop;

public class StringUtility {

	/** STRING_SEARCH_LIKE_ALL. */
	public static final String STRING_SEARCH_LIKE_ALL = "%%";

	private static final String[] oracleTextKeywords = new String[] { "ACCUM", "ABOUT", "NOT", "OR", "AND", "BT", "BTG", "BTP", "BTI", "NT", "NTG", "NTP", "NTI", "PT", "RT", "RT", "SQE", "SYN", "TR", "TRSYN", "TT", "FUZZY", "HASPATH", "INPATH",
			"MINUS", "NEAR", "WITHIN", "84%", "8%", "MDATA" };

	/**
	 * Gets the m d5 hash.
	 * 
	 * @param string
	 *            the string
	 * 
	 * @return the m d5 hash
	 */
	public static String getMD5Hash(String string, String salt) {
		MessageDigest digest;
		try {
			digest = java.security.MessageDigest.getInstance("MD5");
			digest.update((salt + string).getBytes());
			final byte[] hash = digest.digest();
			final StringBuilder result = new StringBuilder(hash.length);
			for (int i = 0; i < hash.length; i++) {
				result.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
			}
			return result.toString();
		} catch (final NoSuchAlgorithmException e) {
			return "error";
		}
	}

	public static String generateHash(String input, String salt) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		// SHA or MD5
		MessageDigest md = MessageDigest.getInstance("MD5");
		String hash = "";
		if (null == salt || "".equals(salt)) {
			salt = "";
		}
		input += salt;
		byte[] data = input.getBytes("US-ASCII");

		md.update(data);
		byte[] digest = md.digest();
		for (int i = 0; i < digest.length; i++) {
			String hex = Integer.toHexString(digest[i]);
			if (hex.length() == 1)
				hex = "0" + hex;
			hex = hex.substring(hex.length() - 2);
			hash += hex;
		}
		return hash;
	}

	public static boolean isNullOrEmpty(final String s) {
		return (s == null || s.trim().length() == 0);
	}

	public static String removeSpecialChars(String text, String byString) {
		final String[] chars = new String[] { ",", ".", "/", "!", "@", "#", "$", "%", "^", "&", "*", "'", "\"", ";", "-", "_", "(", ")", ":", "|", "[", "]", "~", "+", "{", "}", "?", "\\", "<", ">" };
		if (StringUtility.isNullOrEmpty(text))
			return text;

		for (int i = 0; i < chars.length; i++) {
			if (text.indexOf(chars[i]) >= 0) {
				text = text.replace(chars[i], byString);
			}
		}
		return text;
	}

	public static String priceWithDecimal(Double price) {
		DecimalFormat formatter = new DecimalFormat("###,###,###.00");
		return formatter.format(price);
	}

	public static String priceWithoutDecimal(Double price) {
		DecimalFormat formatter = new DecimalFormat("###,###,###.##");
		return formatter.format(price);
	}

	public static String priceToString(Double price) {
		String toShow = priceWithoutDecimal(price);
		if (toShow.indexOf(".") > 0) {
			return priceWithDecimal(price);
		} else {
			return priceWithoutDecimal(price);
		}
	}

	public static String toOracleSearchText(String searchText, boolean isAutocomplete) {
		String[] splitString;
		StringBuilder text = new StringBuilder();
		String OpPat = ",;&"; // search operator pattern
		String SpPat = "<>./!@#$%^*'\"-_():|[]~+{}?\\\n"; // special char
		// pattern
		char[] searchTextArr;
		boolean preCheck = true;

		// [DungNTM commented on Jan 10 - 2011: remove all special characters
		// later]
		// searchText = clearAllHTMLTags(searchText);
		// [end]

		if (!StringUtility.isNullOrEmpty(searchText)) {
			searchTextArr = searchText.toCharArray();

			// remove special char, keep operator char
			for (int i = 0; i < searchTextArr.length; i++) {
				if (SpPat.indexOf(searchTextArr[i]) >= 0) {
					searchTextArr[i] = ' ';
				} else if (OpPat.indexOf(searchTextArr[i]) >= 0) {
					if (preCheck) {
						searchTextArr[i] = ' ';
					}
					preCheck = true;
				} else
					preCheck = false;
			}

			searchText = String.valueOf(searchTextArr).trim();
			if (StringUtility.isNullOrEmpty(searchText)) {
				return STRING_SEARCH_LIKE_ALL;
			}

			if (isAutocomplete)
				// searchText = "%" + text.toString().trim() + "%";
				searchText = searchText.trim() + "%";
			else
				searchText = searchText.trim();

			splitString = searchText.split(" ");
			// if (splitString.length > 1) {
			for (int i = 0; i < splitString.length; i++) {
				if (!"".equals(splitString[i])) {

					// if (!isAutocomplete) {
					for (int j = 0; j < oracleTextKeywords.length; j++) {
						if (oracleTextKeywords[j].equals(splitString[i].toUpperCase())) {
							splitString[i] = "{" + splitString[i] + "}";
							break;
						}
					}
					// }

					text.append(splitString[i] + " ");
				}
			}

			searchText = text.toString();

			// } else if (splitString.length == 1) {
			// text.append("%");
			// text.append(splitString[0].trim());
			// text.append("%");
			// }

			// remove last operator if exist
			if (OpPat.indexOf(searchText.charAt(searchText.length() - 1)) >= 0) {
				searchText = searchText.substring(0, searchText.length() - 1);
			}
		} else
			return STRING_SEARCH_LIKE_ALL;

		// System.out.println("searchString:" + searchText);
		return searchText;
	}

	public static Date getDateFromLot(String lot) throws Exception {
		Date date = null;
		if (lot.length() > 5) {
			String dateLot = lot.substring(0, 2) + "/" + lot.substring(2, 4) + "/20" + lot.substring(4);
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			try {
				date = formatter.parse(dateLot);
			} catch (ParseException e) {
				throw e;
			}
		}
		return date;
	}

	public static String toOracleSearchLikeSuffix(String searchText) {
		return toOracleSearchLike(searchText);
	}

	public static String toOracleSearchLike(String searchText) {
		String escapeChar = "/";
		String[] arrSpPat = { "/", "%", "_" };

		for (String str : arrSpPat) {
			if (!StringUtility.isNullOrEmpty(searchText)) {
				searchText = searchText.replaceAll(str, escapeChar + str);
			}
		}
		searchText = "%" + searchText + "%";
		return searchText;
	}

	/**
	 * Tim kiem theo tien to
	 * 
	 * @author hunglm16
	 * @since December 21,2014
	 * */
	public static String toOracleSearchLikePrefix(String searchText) {
		String escapeChar = "/";
		String[] arrSpPat = { "/", "%", "_" };

		for (String str : arrSpPat) {
			if (!StringUtility.isNullOrEmpty(searchText)) {
				searchText = searchText.replaceAll(str, escapeChar + str);
			}
		}
		searchText = searchText + "%";
		return searchText.trim();
	}

	public static boolean isNumber(final String s) {
		for (int i = 0; i < s.length(); i++) {
			if (!Character.isDigit(s.charAt(i))) {
				return false;
			}
		}

		return true;
	}

	/**
	 * @author tientv11
	 * @see
	 * @param code
	 * @param name
	 * @return
	 */
	public static String CodeAddNameEx(String code, String name) {
		if (StringUtility.isNullOrEmpty(code) && !StringUtility.isNullOrEmpty(name)) {
			return name;
		}
		if (!StringUtility.isNullOrEmpty(code) && StringUtility.isNullOrEmpty(name)) {
			return code;
		}
		if (!StringUtility.isNullOrEmpty(code) && !StringUtility.isNullOrEmpty(name)) {
			return code + " / " + name;
		}
		return "";
	}

	public static boolean isNumberWithDecimal(final String s) {
		try {
			new BigDecimal(s);

			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public static float roundFloat(float unrounded, int precision, int roundingMode) {
		BigDecimal bd = new BigDecimal(unrounded);
		BigDecimal rounded = bd.setScale(precision, roundingMode);
		return rounded.floatValue();
	}

	public static float roundFloatBottom(float unrounded, int precision) {
		Double prec = Math.pow(10, precision);
		Integer tmp = Double.valueOf(unrounded * prec).intValue();
		BigDecimal rounded = new BigDecimal(tmp).divide(new BigDecimal(prec));
		return rounded.floatValue();
	}

	public static String getFullCode(String shopCode, String shortCode) {
		String fullCode = "";
		if (!StringUtility.isNullOrEmpty(shortCode)) {
			fullCode += shortCode;
		} else {
			return "";
		}
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			fullCode += "_" + shopCode;
		}
		return fullCode;
	}

	/**
	 * @author nald
	 * @description bo dau '_' trong chuoi
	 */
	public static String dropUnderlined(String input) {
		input = input.replace("_", "");
		return input;
	}

	/**
	 * Convert money.
	 * 
	 * @param money
	 *            the money
	 * @return the string
	 * @author phut
	 * @since Aug 27, 2012
	 */
	public static String convertMoney(BigDecimal money) {
		String result = "";
		/* String _money = money.longValue() + ""; */
		String _money = money.toBigInteger() + "";
		int isDot = 1;
		for (int i = _money.length(); i > 0; i--) {
			char ch = _money.charAt(i - 1);
			if (isDot == 3 && i != 1) {
				if (_money.charAt(i - 2) == '-') {
					result = ch + result;
					isDot = 0;
				} else {
					result = "," + ch + result;
					isDot = 0;
				}
			} else {
				result = ch + result;
			}
			isDot++;
		}
		return result;
	}
	
	/**
	 * Convert the quantity.
	 * 
	 * @param amount
	 * @param convfact
	 * @author hunglm16
	 * @since March 21, 2015
	 */
	public static String convertConvfact(Integer quality, Integer convfact) {
		if (convfact == null || convfact <= 0) {
			convfact = 1;
		}
		int first = quality / convfact;
		int last = quality % convfact;
		return first + "/" + last;
	}
	
	/**
	 * lay danh sach param khong bi loi 1000 param, su dung list Long
	 * @author duongdt3
	 * @since 10/12/2015
	 * @param paramsIds
	 * @param colName
	 * @return
	 */
	public static String getParamsIdFixBugTooLongParams(List<Long> paramsIds , String colName) {
		StringBuilder builder = new StringBuilder();
		builder.append("-1");
		if (paramsIds != null && !paramsIds.isEmpty()) {
			for (Long id : paramsIds) {
				if (id != null) {
					builder.append("," + id);
				}
			}
		}
		return getParamsIdFixBugTooLongParams(builder.toString(), colName);
	}
	
	/**
	 * lay danh sach param khong bi loi 1000 param
	 * @author duongdt3
	 * @since 02/12/2015
	 * @param paramsIds
	 * @param colName
	 * @return
	 */
	public static String getParamsIdFixBugTooLongParams(String paramsIds, String colName) {
		StringBuilder builder = new StringBuilder();
		
		if (!isNullOrEmpty(paramsIds)) {
			String[] params = paramsIds.split(",");
			if (params != null && params.length > 0) {
				builder.append(" AND (");
				builder.append(" " + colName + " in (-1");
				for (int i = 0, size = params.length; i < size; i++) {
					if (i != 0 && (i % Constant.MAX_NUMBER_PARAM_ID) == 0) {
						builder.append(") OR " + colName + " IN (");
						builder.append(params[i]);
					} else {
						builder.append(",");
						builder.append(params[i]);
					}
				}
				builder.append(")");
				builder.append(") ");
			}
		}
		
		return builder.toString();
	}

	/**
	 * lay danh sach param khong bi loi 1000 param, su dung list Shop
	 * @author duongdt3
	 * @since 10/12/2015
	 * @param lstShop
	 * @param colName
	 * @return
	 */
	public static String getParamsIdFixBugTooLongParamsLstShop(List<Shop> lstShop, String colName) {
		StringBuilder builder = new StringBuilder();
		builder.append("-1");
		if (lstShop != null && !lstShop.isEmpty()) {
			for (Shop s : lstShop) {
				if (s != null) {
					builder.append("," + s.getId());
				}
			}
		}
		
		return getParamsIdFixBugTooLongParams(builder.toString(), colName);
	}
	
	/**
	 * Xu ly getColumnSort
	 * @author vuongmq
	 * @param sortField
	 * @param arrTableName
	 * @return String
	 * @since Apr 6, 2016
	 */
	public static String getColumnSort(String sortField, String [] arrTableName) {
		if (!StringUtility.isNullOrEmpty(sortField) && arrTableName != null && arrTableName.length > 0) {
			for (int i = 0, sz = arrTableName.length; i < sz; i++) {
				String fieldName = arrTableName[i];
				if (!StringUtility.isNullOrEmpty(fieldName) && fieldName.toLowerCase().equals(sortField.trim().toLowerCase())) {
					return sortField;
				}
			}
		}
		return null;
	}
	
	/**
	 * Xu ly getOrderBy
	 * @author vuongmq
	 * @param order
	 * @return String
	 * @since Apr 6, 2016
	 */
	public static String getOrderBy(String order) {
		if (!StringUtility.isNullOrEmpty(order)) {
			if (Constant.SORT_ASC.equals(order.trim().toLowerCase()) || Constant.SORT_DESC.equals(order.trim().toLowerCase())) {
				return order;
			}
		}
		return null;
	}
}

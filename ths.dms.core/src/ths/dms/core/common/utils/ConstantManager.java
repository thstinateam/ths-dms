package ths.dms.core.common.utils;

public class ConstantManager {

	//Cau hinh ap_param
	public static final String SYS_SALE_ROUTE = "SYS_SALE_ROUTE";
	public static final String SYS_CAL_DATE = "SYS_CAL_DATE";
	public static final String SYS_CURRENCY = "SYS_CURRENCY";
	public static final String SYS_CURRENCY_DIVIDE = "SYS_CURRENCY_DIVIDE";
	public static final String SYS_DIGIT_DECIMAL = "SYS_DIGIT_DECIMAL";
	public static final String SYS_DECIMAL_POINT = "SYS_DECIMAL_POINT";
	public static final String SYS_MAP_TYPE = "SYS_MAP_TYPE";
	public static final String SYS_DATE_FORMAT = "SYS_DATE_FORMAT";
	
	//shop_param
	public static final String SYS_USE_LOCKDATE = "SYS_USE_LOCKDATE";
	public static final String SYS_RETURN_DATE = "SYS_RETURN_DATE";
	public static final String SYS_CAL_UNAPPROVED = "SYS_CAL_UNAPPROVED";
	public static final String SYS_MAXDAY_APPROVE = "SYS_MAXDAY_APPROVE";
	public static final String SYS_MAXDAY_RETURN = "SYS_MAXDAY_RETURN";
	public static final String SYS_SHOW_PRICE = "SYS_SHOW_PRICE";
	public static final String SYS_MODIFY_PRICE = "SYS_MODIFY_PRICE";
	public static final String SYS_CAL_PLAN = "SYS_CAL_PLAN";
	
	//value 
	public static final String ZERO_TEXT = "0";
	public static final String ONE_TEXT = "1";
	public static final String TWO_TEXT = "2";
	
	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final int TWO = 2;
}

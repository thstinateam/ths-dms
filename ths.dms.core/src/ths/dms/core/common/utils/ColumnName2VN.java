package ths.dms.core.common.utils;
import java.util.HashMap;

public class ColumnName2VN {
	
	static private Boolean status = false;
	
	static public void init() {
		if (!status) {
			status = true;
			map.put("ADDRESS", "Địa chỉ");
			map.put("AREA_CODE", "Mã địa bàn");
			map.put("CREATE_DATE", "Ngày tạo");
			map.put("CREATE_USER", "Người tạo");
			map.put("EMAIL", "Email");
			map.put("INVOICE_BANK_NAME", "Tên ngân hàng");
			map.put("INVOICE_NUMBER_ACCOUNT", "Số tài khoản ngân hàng");
			map.put("MOBIPHONE", "Số di động");
			map.put("MOBILEPHONE", "Số di động");
			map.put("PARENT_SHOP_ID", "PARENT_SHOP_ID");
			map.put("PHONE", "Số cố định");
			map.put("SHOP_CODE", "Mã đơn vị");
			map.put("SHOP_ID", "ID Đơn vị");
			map.put("SHOP_NAME", "Tên đơn vị");
			map.put("SHOP_TYPE_ID", "Loại đối tượng");  // (0:vnm; 1: mi?n; 2: vùng; 3:npp)
			map.put("STATUS", "Trạng thái");
			map.put("TAX_NUM", "Mã số thuế");
			map.put("UPDATE_DATE", "Ngày cập nhật");
			map.put("UPDATE_USER", "Người cập nhật");
			map.put("AREA_ID", "AREA_ID");
			map.put("BILL_TO", "BILL_TO");
			map.put("SHIP_TO", "SHIP_TO");
			map.put("CONTACT_NAME", "Tên liên hệ");
			map.put("FAX", "FAX");
			map.put("LAT", "LAT");
			map.put("LNG", "LNG");
			map.put("NAME_TEXT", "NAME_TEXT");
			
			map.put("BIRTHDAY", "Ngày sinh");
			map.put("BIRTHDAY_PLACE", "Nơi sinh");
			map.put("EDUCATION", "Trình độ");
			map.put("GENDER", "Giới tính");
			map.put("HOUSENUMBER", "Số nhà");
			map.put("IDNO", "Số chứng minh thư nhân dân");
			map.put("IDNO_DATE", "Ngày cấp");
			map.put("IDNO_PLACE", "Nơi cấp");
			map.put("IMEI", "IMEI Máy tính nhân viên sử dụng");
			map.put("LAST_APPROVED_ORDER", "Đơn hàng cuối cùng được duyệt");
			map.put("LAST_ORDER", "LAST_ORDER");
			map.put("PLAN", "Kế hoạch ngày");
			map.put("POSITION", "Vị trí");
			map.put("RESET_THRESHOLD", "Số lần đồng bộ");
			map.put("SKU", "SKU kế hoạch");
			map.put("STAFF_CODE", "Mã nhân viên");
			map.put("STAFF_ID", "ID nhân viên");
			map.put("STAFF_NAME", "Tên nhân viên");
			map.put("SALE_TYPE_CODE", "Loại nhân viên bán hàng");
			map.put("STAFF_TYPE_ID", "STAFF_TYPE_ID");
			map.put("START_WORKING_DAY", "Ngày bắt đầu làm");
			map.put("STREET", "Đường");
			map.put("UPDATE_PLAN", "Ngày cập nhật kế hoạch ngày");
			map.put("SALE_GROUP", "SALE_GROUP");
			map.put("USER01", "USER01");
			map.put("USER02", "USER02");
			map.put("USER03", "USER03");
			map.put("USER04", "USER04");
			map.put("USER05", "USER05");
			map.put("USER06", "USER06");
			map.put("USER07", "USER07");
			map.put("USER08", "USER08");
			map.put("USER09", "USER09");
			map.put("USER10", "USER10");
			map.put("USER11", "USER11");
			map.put("USER12", "USER12");
			map.put("USER13", "USER13");
			map.put("USER14", "USER14");
			map.put("USER15", "USER15");
			map.put("USER16", "USER16");
			map.put("USER17", "USER17");
			map.put("USER18", "USER18");
			map.put("USER19", "USER19");
			map.put("USER20", "USER20");
			
			
			map.put("CASHIER_STAFF_ID", "ID Nhân viên thu tiền");
			map.put("CHANNEL_TYPE_ID", "CHANNEL_TYPE_ID");
			map.put("CUSTOMER_CODE", "Mã khách hàng");
			map.put("CUSTOMER_ID", "ID Khách hàng");
			map.put("CUSTOMER_NAME", "Tên khách hàng");
			map.put("DELIVERY_ADDRESS", "Địa chỉ giao hàng");
			map.put("FIRST_CODE", "Mã đầu tiên của khách hàng khi được tạo");
			map.put("GROUP_TRANSFER_ID", "ID Nhóm giao hàng");
			map.put("IMAGE_URL", "Thông tin ảnh");
			map.put("INVOICE_CONPANY_NAME", "Tên đơn vị");
			map.put("INVOICE_NAME_BANK", "Tên ngân hàng");
			map.put("INVOICE_OUTLET_NAME", "Tên người in tren hóa đơn");
			map.put("INVOICE_PAYMENT_TYPE", "Hình thức thanh toán");
			map.put("INVOICE_TAX", "Mã số thuế");
			map.put("LAST_APPROVE_ORDER", "Ngày tạo đơn hàng thành công cuối cùng");
			map.put("LAST_ORDER", "LAST_ORDER");//ngay tao don hang cuoi cung
			map.put("LOYALTY", "LOYALTY");
			map.put("MAX_DEBIT_AMOUNT", "Nợ tối đa");
			map.put("MAX_DEBIT_DATE", "Số ngày nợ tối đa");
			map.put("REGION", "Vùng");
			map.put("SHORT_CODE", "Mã ngắn KH");
			map.put("DISPLAY", "Thông tin trưng bày");
			map.put("LOCATION", "Thông tin loại địa điểm");
			map.put("APPLY_DEBIT_LIMITED", "APPLY_DEBIT_LIMITED");

			
			map.put("BARCODE", "Mã vạch của sản phẩm");
			map.put("BRAND_ID", "BRAND_ID");
			map.put("CAT_ID", "CAT_ID");
			map.put("CHECK_LOT", "CHECK_LOT");
			map.put("COMMISSION", "Hoa hồng");
			map.put("CONVFACT", "Giá trị qui đổi từ UOM2->UOM1");
			map.put("EXPIRY_DATE", "Thời gian hết hạn sử dụng");
			map.put("EXPIRY_TYPE", "Loại hạn sử dụng: 1:ngày , 2:tháng");
			map.put("FLAVOUR_ID", "FLAVOUR_ID");  //Hương thơm
			map.put("GROSS_WEIGHT", "Tổng trọng lượng");
			map.put("NET_WEIGHT", "Trọng lượng tinh");
			map.put("PACKING_ID", "PACKING_ID"); // Bao bi
			map.put("PARENT_PRODUCT_CODE", "Mã cha của SP");
			map.put("PRODUCT_CODE", "Mã sản phẩm");
			map.put("PRODUCT_ID", "ID sản  phẩm");
			map.put("PRODUCT_LEVEL_ID", "PRODUCT_LEVEL_ID"); //Muc SP
			map.put("PRODUCT_NAME", "Tên sản phẩm");
			map.put("PRODUCT_TYPE", "Loại sản phẩm (tam thoi chua dung)");
			map.put("SAFETY_STOCK", "Tồn kho an toàn(chứa đựng)");
			map.put("SUB_CAT_ID", "SUB_CAT_ID"); //Nganh hang con
			map.put("UOM1", "Đơn vị tính nhỏ nhất(hop..)");
			map.put("UOM2", "Package(thung..)");
			map.put("VOLUMN", "Thể tích");

			map.put("CUSTOMER_CAT_LEVEL_ID", "CUSTOMER_CAT_LEVEL_ID");
			map.put("SALE_LEVEL_CAT_ID", "SALE_LEVEL_CAT_ID");
			
			map.put("FROM_DATE", "Từ ngày");
			map.put("MULTIPLE", "Tính bội số");
			map.put("PROMOTION_PROGRAM_CODE", "Mã CTKM");
			map.put("PROMOTION_PROGRAM_ID", "ID CTKM");
			map.put("PROMOTION_PROGRAM_NAME", "Tên CTKM");
			map.put("PRO_FORMAT", "Kiểu KM");
			map.put("RECURSIVE", "Tính tối ưu");
			map.put("RELATION", "RELATION");//Quan hệ(và , hoặc)
			map.put("TO_DATE", "Đến ngày");
			map.put("DESCRIPTION", "Mô tả");

			map.put("INCENTIVE_PROGRAM_CODE", "Mã chương trình kích thích");
			map.put("INCENTIVE_PROGRAM_ID", "ID chương trình kích thích");
			map.put("INCENTIVE_PROGRAM_NAME", "Tên chương trình kích thích");
			
			
			map.put("FOCUS_PROGRAM_CODE", "Mã chương trình trọng tâm");
			map.put("FOCUS_PROGRAM_ID", "ID chương trình trọng tâm");
			map.put("FOCUS_PROGRAM_NAME", "Tên chương trình trọng tâm");

			map.put("DISPLAY_PROGRAM_CODE", "Mã CTTB");
			map.put("DISPLAY_PROGRAM_ID", "ID CTTB");
			map.put("DISPLAY_PROGRAM_NAME", "Tên CTTB");
		}
	}
	
	static private HashMap<String, String> map = new HashMap<String, String>();

	public static String getVNName(String columnName) {
		init();
		if (!map.containsKey(columnName))
			return columnName;
		else
			return map.get(columnName);
	}
}

package ths.dms.core.common.utils;

import java.util.Date;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import ths.dms.core.entities.vo.LogInfoVO;

public class LogUtility {
	private static Logger responseLog = Logger.getLogger("ths.dms.kunkun.log.response");
	private static Logger serverErrorLog = Logger.getLogger("ths.dms.kunkun.log.error.server");
	private static Logger clientErrorLog = Logger.getLogger("ths.dms.kunkun.log.error.client");
	
	private static Logger perfLog = Logger.getLogger("ths.dms.kunkun.log.perf");
	
	private static Logger infoLogWS = Logger.getLogger("ths.dms.log.info.ws"); 


	public static void logResponse(String info, String methodName,
			String className) {
		responseLog.trace(className + "," + methodName + "," + info);
	}

	public static void logError(Throwable e, String message) {
		if(serverErrorLog.isEnabledFor(Level.ERROR)) {
			StringBuilder sb = new StringBuilder();
//			sb.append("\"").append(Configuration.getAppName()).append("\",");
			sb.append("\"").append(toCSVCell(message)).append("\",");
			
			sb.append("\"");
			if(e != null) {
				sb.append(e.getMessage()).append("\n");
				StackTraceElement[] st = e.getStackTrace();
				for(int i = 0; i < st.length; i++) {
					sb.append(st[i]).append("\n");
				}
			}
			sb.append("\"");
			
			serverErrorLog.error(sb.toString());
		}
	}

	public static void logMobileClientError(String platform, String model,
			String version, String errName, String description) {
		if (clientErrorLog.isEnabledFor(Level.ERROR)) {
			StringBuilder sb = new StringBuilder();
			sb.append("\"").append(platform).append("\",");
			sb.append("\"").append(model).append("\",");
			sb.append("\"").append(toCSVCell(errName)).append("\",");
			sb.append("\"").append(toCSVCell(description)).append("\",");
			sb.append("\"").append(new Date()).append("\",");
			sb.append("\"").append(version).append("\"");

			clientErrorLog.error(sb.toString());
		}
	}
	
	/**
	 * a) Log chức năng, nghiệp vụ phải có đầy đủ các trường:
		1. Mã ứng dụng										
		2. Mã tiến trình: nếu ứng dụng chạy đa tiến trình.
		3. Thời gian bắt đầu
		4. Thời gian kết thúc
		5. Mã tài khoản tác động
		6. Địa chỉ IP máy tác động
		7. Mã chức năng
		8. Loại tác động: thêm, cập nhật, sửa, xóa, view
		9. ID đối tượng bị tác động: mã hoặc serial
		10. Mô tả tác động
		Ghi chú: 
		- Đối với trường mô tả tác động (content): tùy nghiệp vụ phải xuất được thông tin cần thiết tương ứng nhưng phải đảm bảo đầy đủ các thông tin về các bước và kết quả xử lý bước thực hiện. Ví dụ: Chức năng tác động thông tin khách hàng thì cần phải lưu số msisdn, địa chỉ,..; Chức năng gạch nợ: số msisdn, số tiền nợ trước khi thanh toán, số tiền thanh toán,..; Chức năng bán hàng: mã mặt hàng, số lượng,..
		- Đối với ứng dụng không có thông tin để ghi ra log là một trong các trường trên thì thông tin của trường đó để là “N/A”
		b) Log được ghi ra file định dạng .txt.  
		c) Tùy từng yêu cầu nghiệp vụ nếu cần tra cứu trực tiếp thì bổ sung ghi log vào cơ sở dữ liệu.
		d) Log ghi ra phải được đặt theo cấu trúc:
		app_code || thread_id || start_time || end_time || user_id || ip_address 
			|| function_code|| action_type || object_id || content (content phải thông tin cần thiết tương ứng và bắt theo từ khóa. VD: ISDN:098xxxxxxx | AMOUNT:50000 | SUB_ID:100001| GOODS:TC500+KITECO50+IPHONE4…).
		e) Các trường thông tin phân cách bằng dấu “||”, các thông tin trong trường mô tả tác động (content) phân cách bằng dấu “|” .					
	 * Ghi log chuc nang nghiep vu
	 * @param func
	 * @param duration
	 */
	public static void logInfoWs(LogInfoVO logInfo){
		try {
			if (infoLogWS.isEnabledFor(Level.INFO)){
				StringBuilder sb = new StringBuilder();			
				sb.append(logInfo.getAppCode()).append("||"); /** Ma ung dung */
				sb.append("N/A").append("||"); /** thread_id */
				String startDate = "N/A";
				if(logInfo.getStartDate() != null){
					startDate = DateUtility.toDateString(logInfo.getStartDate(), DateUtility.DATE_FORMAT_NOW);
				}
				sb.append(startDate).append("||");  /** start_time */			
				String endDate = DateUtility.toDateString(DateUtility.now(), DateUtility.DATE_FORMAT_NOW);		
				
				sb.append(endDate).append("||");   /** end_time */ 
				sb.append(logInfo.getStaffCode()).append("||"); /** id user login */			
				sb.append(logInfo.getIp()).append("||");  /** ip_address  */			
				sb.append(logInfo.getFunctionCode()).append("||"); /** function_code  */
				sb.append(logInfo.getActionType()).append("||");  /** action_type  */
				sb.append(logInfo.getIdObject()).append("||");	/** object_id  */
				
				/** Noi dung ghi o day */
				sb.append(logInfo.getContent());
				sb.append("\"");
				infoLogWS.error(sb.toString());
			}
			
		} catch (Exception e) {
			System.out.println("error write log");
		}
	}
	
	
	public static void logPerf(String func, String duration) {
		if (perfLog.isEnabledFor(Level.DEBUG)) {
			StringBuilder sb = new StringBuilder();
			sb.append("\"").append(func).append("\",");
			sb.append("\"").append(duration).append("\",");
			sb.append("\"").append(new Date()).append("\"");

			perfLog.info(sb.toString());
		}
	}

	private static String toCSVCell(String input) {
		return input.replaceAll("\"", "\"\"");
	}
	
	
	
}

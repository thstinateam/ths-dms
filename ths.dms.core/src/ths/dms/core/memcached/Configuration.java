/**
 * 
 */
package ths.dms.core.memcached;


/**
 * The Class Configuration.
 * 
 * @author huytran
 */
public class Configuration {

	public static String appName;

	// private static String serviceId;

	/**
	 * Load configuration.
	 * 
	 * @param event
	 *            the event
	 */
	public static void loadConfiguration(String appName) {
		Configuration.appName = appName;
	}

	public static String getAppName() {
		if (appName == null) {
			appName = "ANONYMOUS";
		}
		return appName;
	}
}

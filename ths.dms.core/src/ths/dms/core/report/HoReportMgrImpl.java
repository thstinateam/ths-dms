/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.report;


import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.SysRouteTypeEnum;
import ths.dms.core.entities.filter.ReportFilter;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.GS_1_4_VO;
import ths.dms.core.entities.vo.ProductForCatVO;
import ths.dms.core.entities.vo.ProgressProgramVO;
import ths.dms.core.entities.vo.RawDataIDPVO;
import ths.dms.core.entities.vo.RawDataStockTransVO;
import ths.dms.core.entities.vo.RawDataVO;
import ths.dms.core.entities.vo.ReceivableSummaryVO;
import ths.dms.core.entities.vo.RptSLDSVO;
import ths.dms.core.entities.vo.TimeKeepingVO;
import ths.core.entities.vo.rpt.RptBCBAOMAT1_5VO;
import ths.core.entities.vo.rpt.RptBCTDTMTHVO;
import ths.core.entities.vo.rpt.RptDebitRetrieveVO;
import ths.core.entities.vo.rpt.RptDebitReturnVO;
import ths.core.entities.vo.rpt.RptEqImExVO;
import ths.core.entities.vo.rpt.RptKK1_1VO;
import ths.core.entities.vo.rpt.Rpt_3116_WVKD_03F5SC1_1VO;
import ths.core.entities.vo.rpt.Rpt_3_1_1_10_BCTHTDTM_F12_VO;
import ths.core.entities.vo.rpt.Rpt_3_1_1_3_BCDSTL11_VO;
import ths.core.entities.vo.rpt.Rpt_3_1_1_4_SC1_1VODetail;
import ths.core.entities.vo.rpt.Rpt_3_1_1_8_BCTDTMSC_F10_VO;
import ths.core.entities.vo.rpt.Rpt_3_1_1_9_DSKHMT_F11_VO;
import ths.core.entities.vo.rpt.Rpt_SLDS1_5;
import ths.dms.core.entities.vo.rpt.ho.RptCTKMVO;
import ths.dms.core.entities.vo.rpt.ho.RptDHChoKHVO;
import ths.dms.core.entities.vo.rpt.ho.RptKGTKHTTVO;
import ths.dms.core.entities.vo.rpt.ho.RptNVBHActive;
import ths.dms.core.entities.vo.rpt.ho.RptTGGTKHVO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_1_DSPSTN_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_2_TKDHGTN_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_3_GHTNVGH_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_4_BCDHR_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_1_4_G_BCDHCR_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_2_1_CTHHMV;
import ths.dms.core.entities.vo.rpt.ho.Rpt_2_2_SSSLDHMH;
import ths.dms.core.entities.vo.rpt.ho.Rpt_2_3_CTMH;
import ths.dms.core.entities.vo.rpt.ho.Rpt_5_1_TKKMVO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_ASO_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_ASO_VO_Data;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_ASO_VO_Header;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_ASO_VO_Header_Value;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_GROWTH_SKUs_VO;
import ths.dms.core.entities.vo.rpt.ho.Rpt_HO_GROWTH_SKUs_VO_Data;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.ApParamDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.repo.IRepository;


/**
 * Bao cao HO
 * 
 * @author hunglm16
 * @since Mar 20, 2014
 */
public class HoReportMgrImpl implements HoReportMgr {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private CommonDAO commonDAO;
	
	@Autowired
	private ApParamDAO apParamDAO; 
	
	@Override
	public List<RptEqImExVO> exportEQImEx(Date fromDate, Date toDate) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<RptEqImExVO> lstResult = new ArrayList<RptEqImExVO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_rpt_equip.REPORT_20_2_EQ_IM_EX(?, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();

			params.add(2);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(3);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(RptEqImExVO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<GS_1_4_VO> exportBCSLDS21(Long staffIdRoot, Long userId, String strListShopId, Date fDate, Date tDate, String categoryId, String subCategoryId, String strListProductId) throws BusinessException {
		if (null == fDate || null == tDate) {
				throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
			}
			try {

				List<GS_1_4_VO> lstResult = new ArrayList<GS_1_4_VO>();
				StringBuilder sql = new StringBuilder();	
		//		if (check == 0) {
					sql.append("call PKG_REPORT_GS.REPORT_2_1_BCSLDS(?,:userId,:roleId,:lstShopId,:categoryId, :subCategoryId, :lstProductId, :fromDate, :toDate)");	
		/*		} else {
					sql.append("call PKG_REPORT_GS.REPORT_1_4_2_BCMMKH(?,:userId,:roleId,:lstShopId,:lstGSId, :lstNVBHId, :fromDate, :toDate)");		
				}	*/						
				List<Object> params = new ArrayList<Object>();
				params.add(2);
				params.add(userId);
				params.add(java.sql.Types.NUMERIC);
				
				params.add(3);
				params.add(staffIdRoot);
				params.add(java.sql.Types.NUMERIC);
				
				params.add(4);
				params.add(strListShopId);
				params.add(java.sql.Types.VARCHAR);
				
				params.add(5);
				params.add(categoryId);
				params.add(java.sql.Types.VARCHAR);
				
				params.add(6);
				params.add(subCategoryId);
				params.add(java.sql.Types.VARCHAR);
				
				params.add(7);
				params.add(strListProductId);
				params.add(java.sql.Types.VARCHAR);
				
				params.add(8);
				params.add(new java.sql.Date(fDate.getTime()));
				params.add(java.sql.Types.DATE);

				params.add(9);
				params.add(new java.sql.Date(tDate.getTime()));
				params.add(java.sql.Types.DATE);

				lstResult = repo.getListByQueryDynamicFromPackage(GS_1_4_VO.class, sql.toString(), params, null);
				return lstResult;

			} catch (DataAccessException ex) {
				throw new BusinessException(ex);
			}
	}

	@Override
	public List<GS_1_4_VO> exportBCSLDS22(long userId, long roleId, long chooseShopId, Date fDate, Date tDate, String categoryId, String subCategoryId, String strListProductId, Long cycleId) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<GS_1_4_VO> lstResult = new ArrayList<GS_1_4_VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_REPORT_GS.REPORT_2_2_BCSLDS(?, :userId, :roleId, :chooseShopId, :specificType, :categoryId, :subCategoryId, :lstProductId, :cycleId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);

			params.add(3);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);

			params.add(4);
			params.add(chooseShopId);
			params.add(java.sql.Types.NUMERIC);

			params.add(5);
			params.add(StaffSpecificType.STAFF.getValue());
			params.add(java.sql.Types.NUMERIC);

			params.add(6);
			params.add(categoryId);
			params.add(java.sql.Types.VARCHAR);

			params.add(7);
			params.add(subCategoryId);
			params.add(java.sql.Types.VARCHAR);

			params.add(8);
			params.add(strListProductId);
			params.add(java.sql.Types.VARCHAR);

			params.add(9);
			params.add(cycleId);
			params.add(java.sql.Types.VARCHAR);

			params.add(10);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(11);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_1_4_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * @author tungmt
	 * @version 1.0
	 * @param staffIdRoot
	 * @return Danh sach keyshop cho don hang
	 * @exception BusinessException
	 * @see
	 * @since 6/8/2015
	 * @serial
	 */
	@Override
	public List<RawDataVO> exportBCSLDS27(ReportFilter filter) throws BusinessException {
		if (null == filter.getfDate() || null == filter.gettDate()) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {

			List<RawDataVO> lstResult = new ArrayList<RawDataVO>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_REPORT_GS.REPORT_2_7_BCDH(?,:userId, :roleId, :lstShopId, :categoryId, :subCategoryId, :lstProductId, :fromDate, :toDate)");	
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(filter.getUserId());
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(filter.getRoleId());
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(filter.getStrListShopId());
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(filter.getCategoryId());
			params.add(java.sql.Types.VARCHAR);
			
			params.add(6);
			params.add(filter.getSubCategoryId());
			params.add(java.sql.Types.VARCHAR);
			
			params.add(7);
			params.add(filter.getStrListProductId());
			params.add(java.sql.Types.VARCHAR);
			
			params.add(8);
			params.add(new java.sql.Date(filter.getfDate().getTime()));
			params.add(java.sql.Types.DATE);

			params.add(9);
			params.add(new java.sql.Date(filter.gettDate().getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(RawDataVO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<GS_1_4_VO> exportBCSLDS23(Long staffIdRoot, Long userId, String strListShopId, Date fDate, Date tDate, String categoryId, String subCategoryId, String strListProductId, Long cycleId) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {

			List<GS_1_4_VO> lstResult = new ArrayList<GS_1_4_VO>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_REPORT_GS.REPORT_2_3_BCSLDS(?,:userId,:roleId,:lstShopId,:categoryId, :subCategoryId, :lstProductId, :cycleId, :fromDate, :toDate)");	
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(categoryId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(6);
			params.add(subCategoryId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(7);
			params.add(strListProductId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(8);
			params.add(cycleId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(9);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(10);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_1_4_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<RptSLDSVO> exportBCSLDSKH(Long staffIdRoot, Long roleId, String strListShopId, Date fDate, Date tDate, String categoryId, String subCategoryId, String strListProductId, Long cycleId) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<RptSLDSVO> lstResult = new ArrayList<RptSLDSVO>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_RPT_HO.REPORT_BCSLDSKH(?, :userId, :roleId, :lstShopId, :categoryId, :subCategoryId, :lstProductId, :cycleId, :fromDate, :toDate)");	
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(categoryId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(subCategoryId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(strListProductId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(cycleId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(RptSLDSVO.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<RptSLDSVO> exportBCDSCTKH(Long staffIdRoot, Long roleId, String strListShopId, Long ksId, Long cycleId, Long cycleIdTo) throws BusinessException {
		try {
			List<RptSLDSVO> lstResult = new ArrayList<RptSLDSVO>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_RPT_HO.REPORT_BCDSCTKH(?, :userId, :roleId, :lstShopId, :ksId, :cycleId, :cycleIdTo)");	
			List<Object> params = new ArrayList<Object>();
			int setPR = 2;
			params.add(setPR++);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(setPR++);
			params.add(ksId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(cycleIdTo);
			params.add(java.sql.Types.NUMERIC);
			
			lstResult = repo.getListByQueryDynamicFromPackage(RptSLDSVO.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<ProgressProgramVO> exportProgressProgram(Long staffIdRoot, Long roleId, String strListShopId, Long ksId, Long cycleId, Long cycleIdTo) throws BusinessException {
		try {
			List<ProgressProgramVO> lstResult = new ArrayList<ProgressProgramVO>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_RPT_HO_P2.REPORT_5_5_PROGRESS_PROGRAM(?, :userId, :roleId, :lstShopId, :ksId, :cycleId, :cycleIdTo)");	
			List<Object> params = new ArrayList<Object>();
			int setPR = 2;
			params.add(setPR++);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(setPR++);
			params.add(ksId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(cycleIdTo);
			params.add(java.sql.Types.NUMERIC);
			
			lstResult = repo.getListByQueryDynamicFromPackage(ProgressProgramVO.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptSLDSVO> exportBC_ASO_INACTIVE(Long staffIdRoot, Long roleId, String strListShopId, Long cycleId) throws BusinessException {
		try {
			List<RptSLDSVO> lstResult = new ArrayList<RptSLDSVO>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_RPT_HO.REPORT_ASO_INACTIVE(?, :userId, :roleId, :lstShopId, :cycleId)");	
			List<Object> params = new ArrayList<Object>();
			int setPR = 2;
			params.add(setPR++);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(setPR++);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			
			lstResult = repo.getListByQueryDynamicFromPackage(RptSLDSVO.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<RptTGGTKHVO> exportTGGTKH(Long staffIdRoot, Long roleId, String strListShopId, Date fDate, Date tDate) throws BusinessException {
		if (null == fDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<RptTGGTKHVO> lstResult = new ArrayList<RptTGGTKHVO>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_RPT_HO.REPORT_VISIT_CUSTOMER(?, :userId, :roleId, :lstShopId, :fromDate, :toDate)");	
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(RptTGGTKHVO.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<RptKGTKHTTVO> exportKGTKHTT(Long staffIdRoot, Long roleId, String strListShopId, Date fDate, Date tDate) throws BusinessException {
		if (null == fDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<RptKGTKHTTVO> lstResult = new ArrayList<RptKGTKHTTVO>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_RPT_HO.REPORT_NOT_VISIT_CUS_IN_ROUTE(?, :userId, :roleId, :lstShopId, :fromDate, :toDate)");	
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(RptKGTKHTTVO.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<GS_1_4_VO> exportBCSLDS26(Long staffIdRoot, Long userId, String strListShopId, String strListNvbhId, String orderNumber, String orderType, Integer orderSource, Integer approved, Integer approvedStep, Integer saleOrderType,
			Long cycleId, Date fDate, Date tDate) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {

			List<GS_1_4_VO> lstResult = new ArrayList<GS_1_4_VO>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_REPORT_GS.REPORT_2_6_BCDSDH(?,:userId,:roleId,:lstShopId,:strListNvbhId, :orderNumber, :orderType, :orderSource, :approved, :approvedStep, :saleOrderType, :cycleId, :fromDate, :toDate)");	
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(strListNvbhId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(6);
			params.add(orderNumber);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(7);
			params.add(orderType);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(8);
			params.add(orderSource);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(9);
			params.add(approved);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(10);
			params.add(approvedStep);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(11);
			params.add(saleOrderType);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(12);
			params.add(cycleId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(13);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(14);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_1_4_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<CycleVO> getHeaderBCSLDS24(String cycleId) throws BusinessException {
		try {
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_REPORT_GS.REPORT_2_4_HEADER(?, :cycleId)");	
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(cycleId);
			params.add(java.sql.Types.VARCHAR);
			return repo.getListByQueryDynamicFromPackage(CycleVO.class, sql.toString(), params, null);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<GS_1_4_VO> exportBCSLDS24(Long userId, Long roleId, String strListShopId, Long cycleId) throws BusinessException {
		try {

			List<GS_1_4_VO> lstResult = new ArrayList<GS_1_4_VO>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_REPORT_GS.REPORT_2_4_BCSLDSTDKPI(?,:userId,:roleId,:lstShopId, :cycleId)");	
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(cycleId);
			params.add(java.sql.Types.VARCHAR);	

			lstResult = repo.getListByQueryDynamicFromPackage(GS_1_4_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<GS_1_4_VO> exportBCKEYSHOP41(Long staffIdRoot, Long userId, String strListShopId, String strListKeyShopId, Long cycleId, Date fDate, Date tDate) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {

			List<GS_1_4_VO> lstResult = new ArrayList<GS_1_4_VO>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_REPORT_GS.REPORT_4_1_BCDSKS(?,:userId,:roleId,:lstShopId,:strListKeyShopId, :cycleId, :fromDate, :toDate)");	
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(strListKeyShopId);
			params.add(java.sql.Types.VARCHAR);			
			
			params.add(6);
			params.add(cycleId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(7);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(8);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_1_4_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<GS_1_4_VO> exportBCXNT31(Long staffIdRoot, Long userId, String strListShopId, Date fDate, Date tDate, String orderStock, String orderProduct, Long cycleId) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<GS_1_4_VO> lstResult = new ArrayList<GS_1_4_VO>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_REPORT_GS.REPORT_3_1_BCXNT(?,:userId,:roleId,:lstShopId, :orderStock, :orderProduct, :cycleId, :fromDate, :toDate)");	
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(orderStock);
			params.add(java.sql.Types.VARCHAR);			
			
			params.add(6);
			params.add(orderProduct);
			params.add(java.sql.Types.VARCHAR);			
			
			params.add(7);
			params.add(cycleId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(8);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(9);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_1_4_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<GS_1_4_VO> exportBCXNT32(Long userId, Long roleId, String strListShopId, Long cycleId, Date fDate, Date tDate) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<GS_1_4_VO> lstResult = new ArrayList<GS_1_4_VO>();
			StringBuilder sql = new StringBuilder();	
			/*if (orderStock != null && orderStock.equals("0")) {
				sql.append("call PKG_REPORT_GS.REPORT_3_2_BCXNTCTHB(?,:userId,:roleId,:lstShopId, :orderStock, :orderProduct, :cycleId, :fromDate, :toDate)");	
			} else if (orderStock != null && orderStock.equals("1")) {
				sql.append("call PKG_REPORT_GS.REPORT_3_2_BCXNTCTHKM(?,:userId,:roleId,:lstShopId, :orderStock, :orderProduct, :cycleId, :fromDate, :toDate)");	
			}*/
			sql.append("call PKG_REPORT_GS.REPORT_3_2_BCXNTCTHB(?, :userId, :roleId, :lstShopId, :cycleId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(cycleId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(6);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(7);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_1_4_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<GS_1_4_VO> exportBCTKTNVVS42(Long userId, Long roleId, String strListShopId, Date fDate, Date tDate) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<GS_1_4_VO> lstResult = new ArrayList<GS_1_4_VO>();
			StringBuilder sql = new StringBuilder();	
			
			sql.append("call PKG_REPORT_GS.REPORT_4_2_BCTKTNVVS(?, :userId, :roleId, :lstShopId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(6);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_1_4_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<GS_1_4_VO> exportBCTTNH35(Long staffIdRoot, Long userId, String strListShopId, Date fDate, Date tDate, String orderNumber, String orderStatus, Long cycleId) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<GS_1_4_VO> lstResult = new ArrayList<GS_1_4_VO>();
			StringBuilder sql = new StringBuilder();
			if (orderStatus != null && orderStatus.equals("0")) {
				sql.append("call PKG_REPORT_GS.REPORT_3_3_BCTTDHDND(?,:userId,:roleId,:lstShopId, :orderNumber, :orderStatus, :cycleId, :fromDate, :toDate)");
			} else if (orderStatus != null && orderStatus.equals("1")) {
				sql.append("call PKG_REPORT_GS.REPORT_3_3_BCTTDHDNMP(?,:userId,:roleId,:lstShopId, :orderNumber, :orderStatus, :cycleId, :fromDate, :toDate)");
			} else if (orderStatus != null && orderStatus.equals("2")) {
				sql.append("call PKG_REPORT_GS.REPORT_3_3_BCTTDHDTDD(?,:userId,:roleId,:lstShopId, :orderNumber, :orderStatus, :cycleId, :fromDate, :toDate)");
			} else {
				sql.append("call PKG_REPORT_GS.REPORT_3_3_BCTTDH(?,:userId,:roleId,:lstShopId, :orderNumber, :orderStatus, :cycleId, :fromDate, :toDate)");
			}
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);

			params.add(3);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);

			params.add(4);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);

			params.add(5);
			params.add(orderNumber);
			params.add(java.sql.Types.VARCHAR);

			params.add(6);
			params.add(orderStatus);
			params.add(java.sql.Types.VARCHAR);

			params.add(7);
			params.add(cycleId);
			params.add(java.sql.Types.VARCHAR);

			params.add(8);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(9);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_1_4_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<GS_1_4_VO> exportBCKK34(Long staffIdRoot, Long userId, String strListShopId, Date fDate, Date tDate, String orderStock, String orderProduct, String orderStatus, Long cycleId) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<GS_1_4_VO> lstResult = new ArrayList<GS_1_4_VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_REPORT_GS.REPORT_3_4_BCKK(?,:userId,:roleId,:lstShopId, :orderStock, :orderProduct, :orderStatus, :cycleId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);

			params.add(3);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);

			params.add(4);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);

			params.add(5);
			params.add(orderStock);
			params.add(java.sql.Types.VARCHAR);

			params.add(6);
			params.add(orderProduct);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(7);
			params.add(orderStatus);
			params.add(java.sql.Types.VARCHAR);

			params.add(8);
			params.add(cycleId);
			params.add(java.sql.Types.VARCHAR);

			params.add(9);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(10);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_1_4_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	///////////////////: Bao cao IDP ://////////////////////////////////////
	@Override
	public List<Rpt_5_1_TKKMVO> reportKM5D1TKKM(String shopId, String promationProgramId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		try {
			if (StringUtility.isNullOrEmpty(shopId)) {
				shopId = String.valueOf(ALL_INT_G); //Dinh nghia Id < 0
			}
			if (StringUtility.isNullOrEmpty(promationProgramId)) {
				promationProgramId = "";
			}
			if (fromDate == null) {
                fromDate = commonDAO.getSysDate();
            }
            if (toDate == null) {
                toDate = (fromDate == null) ? fromDate : commonDAO.getSysDate();
            }
			List<Rpt_5_1_TKKMVO> lstResult = new ArrayList<Rpt_5_1_TKKMVO>();
			
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho.KM_5_1_TKKM(?, :shopId, :promationProgramId, :fromDate, :toDate, :staffRootId, :roleId, :shopRootId)");
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(shopId);
			params.add(java.sql.Types.VARCHAR);
			params.add(sqlParamIndex++);
			params.add(promationProgramId);
			params.add(java.sql.Types.VARCHAR);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(Rpt_5_1_TKKMVO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptNVBHActive> exportBCNVBHActive(String shopId, Date fromDate) throws BusinessException {
		try {
			if (null == fromDate) {
				throw new IllegalArgumentException(ExceptionCode.REPORT_FROM_DATE_IS_NULL);
			}
			List<RptNVBHActive> lstResult = new ArrayList<RptNVBHActive>();
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call PKG_RPT_HO.BC_NVBH_ACTIVE(?, :shopId, :fromDate)");
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(shopId);
			params.add(java.sql.Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(RptNVBHActive.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<RptDHChoKHVO> exportBCDHChoKH(String shopId, Date fromDate) throws BusinessException {
		try {
			if (null == fromDate) {
				throw new IllegalArgumentException(ExceptionCode.REPORT_FROM_DATE_IS_NULL);
			}
			List<RptDHChoKHVO> lstResult = new ArrayList<RptDHChoKHVO>();
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call PKG_RPT_HO.BC_DH_KH(?, :shopId, :fromDate)");
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(shopId);
			params.add(java.sql.Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(RptDHChoKHVO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Rpt_HO_ASO_VO reportASOFullData(Long shopId, Long cycleId, int flagALL, String subCatId, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		Rpt_HO_ASO_VO data = new Rpt_HO_ASO_VO();
		try {
			if (shopId == null || cycleId == null || staffRootId == null || roleId == null || shopRootId == null) {
				throw new IllegalArgumentException(ExceptionCode.REPORT_PARAMETER_IS_NULL);
			}
			if (flagALL == ZEZO_INT_G && StringUtility.isNullOrEmpty(subCatId)) {
				throw new IllegalArgumentException("subCatId is null while flagALL is 0 ");
			}
			//Xu ly lay cau hinh mac dinh cho du lieu xuat bao cao
			Integer flagVal = ONE_INT_G;
			if (StringUtility.isNumber(SysRouteTypeEnum.IN_ROUTE.getValue())) {
				flagVal = Integer.valueOf(SysRouteTypeEnum.IN_ROUTE.getValue());
			}
			ApParam appram = apParamDAO.getApParamByCode(SysRouteTypeEnum.CODE.getValue(), ActiveType.RUNNING);
			if (appram != null && !StringUtility.isNullOrEmpty(appram.getValue()) && StringUtility.isNumber(appram.getValue()) && SysRouteTypeEnum.checkValueApp(Integer.valueOf(appram.getValue()))) {
				flagVal = Integer.valueOf(appram.getValue());
			}

			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;

			//Xu ly lay header bao cao
			List<ProductForCatVO> lstHeader = new ArrayList<ProductForCatVO>();
			sql.append(" call pkg_rpt_ho.GET_SUBCAT_PRODUCT(?, :subCatId) ");
			params.add(sqlParamIndex++);
			if (ONE_INT_G == flagALL) {
				params.add("");
			} else {
				params.add(subCatId.trim());
			}
			params.add(java.sql.Types.VARCHAR);
			lstHeader = repo.getListByQueryDynamicFromPackage(ProductForCatVO.class, sql.toString(), params, null);
			if (lstHeader != null && !lstHeader.isEmpty()) {
				Rpt_HO_ASO_VO_Header headerAutomatic = new Rpt_HO_ASO_VO_Header();

				//Xu ly cho Subcat
				List<Long> lstHeaderAsoId = new ArrayList<Long>();
				Map<Long, String> mapHeaderAsoName = new HashMap<Long, String>();
				Map<Long, String> mapHeaderAsoCode = new HashMap<Long, String>();
				Map<Long, Rpt_HO_ASO_VO_Header_Value> mapHeader = new HashMap<Long, Rpt_HO_ASO_VO_Header_Value>();

				//Xu ly cho Product
				List<Long> lstHeaderSKU = new ArrayList<Long>();
				Map<Long, String> mapHeaderSkuName = new HashMap<Long, String>();
				Map<Long, String> mapHeaderSkuCode = new HashMap<Long, String>();
				Map<Long, Integer> mapHeaderSkuType = new HashMap<Long, Integer>();
				
				//Xu ly cho du lieu tra ve
				int index = 0, size = lstHeader.size(), dem = 0;
				Long subCateId = lstHeader.get(index).getSubCatId();
				lstHeaderAsoId.add(subCateId);
				mapHeaderAsoCode.put(subCateId, lstHeader.get(index).getSubCatCode());
				mapHeaderAsoName.put(subCateId, lstHeader.get(index).getSubCatName());
				
				//Xu ly bien tam
				Rpt_HO_ASO_VO_Header_Value skuHeader = null;
				
				while (dem < size) {
					for (int i = index; i < size; i++) {
						if (subCateId.equals(lstHeader.get(i).getSubCatId())) {
							lstHeaderSKU.add(lstHeader.get(i).getId());
							mapHeaderSkuCode.put(lstHeader.get(i).getId(), lstHeader.get(i).getCode());
							mapHeaderSkuName.put(lstHeader.get(i).getId(), lstHeader.get(i).getName());
							mapHeaderSkuType.put(lstHeader.get(i).getId(), lstHeader.get(i).getTypeRow());
							dem++;
						} else {
							//Xu ly them thong tin san pham
							skuHeader = new Rpt_HO_ASO_VO_Header_Value();
							skuHeader.setLstHeaderSKU(lstHeaderSKU);
							skuHeader.setMapHeaderSkuCode(mapHeaderSkuCode);
							skuHeader.setMapHeaderSkuName(mapHeaderSkuName);
							skuHeader.setMapHeaderSkuType(mapHeaderSkuType);
							mapHeader.put(subCateId, skuHeader);
							//Set tham so cho vong lap moi
							index = i;
							subCateId = lstHeader.get(index).getSubCatId();
							lstHeaderAsoId.add(subCateId);
							mapHeaderAsoCode.put(subCateId, lstHeader.get(index).getSubCatCode());
							mapHeaderAsoName.put(subCateId, lstHeader.get(index).getSubCatName());
							
							lstHeaderSKU = new ArrayList<Long>();
							mapHeaderSkuName = new HashMap<Long, String>();
							mapHeaderSkuCode = new HashMap<Long, String>();
							mapHeaderSkuType = new HashMap<Long, Integer>();
							break;
						}
					}
					if (dem == size) {
						skuHeader = new Rpt_HO_ASO_VO_Header_Value();
						skuHeader.setLstHeaderSKU(lstHeaderSKU);
						skuHeader.setMapHeaderSkuCode(mapHeaderSkuCode);
						skuHeader.setMapHeaderSkuName(mapHeaderSkuName);
						skuHeader.setMapHeaderSkuType(mapHeaderSkuType);
						mapHeader.put(subCateId, skuHeader);
					}
				}
				headerAutomatic.setLstHeaderAsoId(lstHeaderAsoId);
				headerAutomatic.setMapHeaderAsoCode(mapHeaderAsoCode);
				headerAutomatic.setMapHeaderAsoName(mapHeaderAsoName);
				headerAutomatic.setMapHeader(mapHeader);
				headerAutomatic.setMapHeader(mapHeader);
				//Them vao du lieu tra ve
				data.setHeaderAutomatic(headerAutomatic);
			}
			//Xu ly du lieu
			sql = new StringBuilder();
			params = new ArrayList<Object>();
			sqlParamIndex = 2;
			List<Rpt_HO_ASO_VO_Data> lstData = new ArrayList<Rpt_HO_ASO_VO_Data>();
			sql.append(" call pkg_rpt_ho.KPI_6_2_2_ASO(?, :shopId, :cycleId, :flagVal, :flagALL, :subCatId, :staffRootId, :roleId, :shopRootId) ");
			params.add(sqlParamIndex++);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(flagVal);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(flagALL);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			if (ONE_INT_G == flagALL) {
				params.add("");
			} else {
				params.add(subCatId.trim());
			}
			params.add(java.sql.Types.VARCHAR);
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);
			lstData = repo.getListByQueryDynamicFromPackage(Rpt_HO_ASO_VO_Data.class, sql.toString(), params, null);
			if (lstData != null && !lstData.isEmpty()) {
				data.setLstData(lstData);
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		return data;
	}
	
	@Override
	public Rpt_HO_GROWTH_SKUs_VO reportGrowthSKUs(Long shopId, Integer year, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		Rpt_HO_GROWTH_SKUs_VO data = new Rpt_HO_GROWTH_SKUs_VO();
		try {
			if (shopId == null || year == null || staffRootId == null || roleId == null || shopRootId == null) {
				throw new IllegalArgumentException(ExceptionCode.REPORT_PARAMETER_IS_NULL);
			}
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("select * ");
			sql.append(" from CYCLE ");
			sql.append(" WHERE trunc(to_char(year, 'yyyy')) = ? and status <> ? ");
			params.add(year);
			params.add(ActiveType.DELETED.getValue());
			sql.append(" and ((trunc(to_char(YEAR, 'yyyy')) < trunc(to_char(sysdate, 'yyyy'))) or (BEGIN_DATE <= sysdate)) ");
			sql.append(" order by NUM ");
			List<Cycle> lstHeader = repo.getListBySQL(Cycle.class, sql.toString(), params);
			//Bo sung du lieu header
			data.setHeaderAutomatic(lstHeader);
			
			sql = new StringBuilder();
			params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			//Xu ly lay header bao cao
			List<Rpt_HO_GROWTH_SKUs_VO_Data> lstData = new ArrayList<Rpt_HO_GROWTH_SKUs_VO_Data>();
			sql.append("call pkg_rpt_ho_p2.bc_6_1_3_ttskus(?, :p_shopId, :p_year, :staffRootId, :roleId, :shoprootId) ");
			params.add(sqlParamIndex++);
			params.add(String.valueOf(shopId));
			params.add(java.sql.Types.VARCHAR);
			params.add(sqlParamIndex++);
			params.add(year);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);
			lstData = repo.getListByQueryDynamicFromPackage(Rpt_HO_GROWTH_SKUs_VO_Data.class, sql.toString(), params, null);
			if (lstData != null && !lstData.isEmpty()) {
				data.setLstData(lstData);
			}
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		return data;
	}
	
	
	/**
	 * @author vuongmq bao cao slds 1.5
	 */
	@Override
	public List<Rpt_SLDS1_5> getListSLDS1_5(String lstShopId, Date fromDate, Date toDate) throws BusinessException {
		List<Rpt_SLDS1_5> result = new ArrayList<Rpt_SLDS1_5>();
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_HO_REPORT.SLDS_1_5(?, :lstShopId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(lstShopId);
			params.add(java.sql.Types.VARCHAR);
			params.add(3);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(4);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			result = repo.getListByQueryDynamicFromPackage(Rpt_SLDS1_5.class, sql.toString(), params, null);
			for (Rpt_SLDS1_5 rpt : result) {
				rpt.safeSetNull();
			}

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
		return result;
	}
	
	@Override
	public List<Rpt_1_1_DSPSTN_VO> report1D1DSPSTN(String shopId, Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		try {
			if (StringUtility.isNullOrEmpty(shopId)) {
				shopId = String.valueOf(ALL_INT_G); //Dinh nghia Id < 0
			}
			if (cycleId == null) {
				cycleId = Long.valueOf(ALL_INT_G);
			}
			if (fromDate == null) {
				fromDate = commonDAO.getSysDate();
			}
			if (toDate == null) {
				toDate = (fromDate == null) ? fromDate : commonDAO.getSysDate();
			}
			List<Rpt_1_1_DSPSTN_VO> lstResult = new ArrayList<Rpt_1_1_DSPSTN_VO>();
			
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho.BC_1_1_DSPSTN(?, :shopId, :cycleId, :fromDate, :toDate, :staffRootId, :roleId, :shopRootId) ");
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(shopId);
			params.add(java.sql.Types.VARCHAR);
			params.add(sqlParamIndex++);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(Rpt_1_1_DSPSTN_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<RptDebitRetrieveVO> reportCnptCt32(Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		try {
//			if (StringUtility.isNullOrEmpty(shopId)) {
//				shopId = String.valueOf(ALL_INT_G); //Dinh nghia Id < 0
//			}
			if (cycleId == null) {
				cycleId = Long.valueOf(ALL_INT_G);
			}
			if (fromDate == null) {
				fromDate = commonDAO.getSysDate();
			}
			if (toDate == null) {
				toDate = (fromDate == null) ? fromDate : commonDAO.getSysDate();
			}
			List<RptDebitRetrieveVO> lstResult = new ArrayList<RptDebitRetrieveVO>();
			
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho.REPORT_3_2_CNPT_CT(?, :cycleId, :fromDate, :toDate, :userId, :roleId, :shopId) ");
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(RptDebitRetrieveVO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<RptDebitReturnVO> reportCnptCt33(Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		try {
//			if (StringUtility.isNullOrEmpty(shopId)) {
//				shopId = String.valueOf(ALL_INT_G); //Dinh nghia Id < 0
//			}
			if (cycleId == null) {
				cycleId = Long.valueOf(ALL_INT_G);
			}
			if (fromDate == null) {
				fromDate = commonDAO.getSysDate();
			}
			if (toDate == null) {
				toDate = (fromDate == null) ? fromDate : commonDAO.getSysDate();
			}
			List<RptDebitReturnVO> lstResult = new ArrayList<RptDebitReturnVO>();
			
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho.REPORT_3_3_CNPTR_TH(?, :cycleId, :fromDate, :toDate, :userId, :roleId, :shopId) ");
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(RptDebitReturnVO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<RptDebitReturnVO> reportCnptrCt34(Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		try {
//			if (StringUtility.isNullOrEmpty(shopId)) {
//				shopId = String.valueOf(ALL_INT_G); //Dinh nghia Id < 0
//			}
			if (cycleId == null) {
				cycleId = Long.valueOf(ALL_INT_G);
			}
			if (fromDate == null) {
				fromDate = commonDAO.getSysDate();
			}
			if (toDate == null) {
				toDate = (fromDate == null) ? fromDate : commonDAO.getSysDate();
			}
			List<RptDebitReturnVO> lstResult = new ArrayList<RptDebitReturnVO>();
			
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho.REPORT_3_4_CNPTR_CT(?, :cycleId, :fromDate, :toDate, :userId, :roleId, :shopId) ");
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(RptDebitReturnVO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<Rpt_1_2_TKDHGTN_VO> report1D2TKDHGTN(String shopId, Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		try {
			if (StringUtility.isNullOrEmpty(shopId)) {
				shopId = String.valueOf(ALL_INT_G); //Dinh nghia Id < 0
			}
			if (cycleId == null) {
				cycleId = Long.valueOf(ALL_INT_G);
			}
			if (fromDate == null) {
				fromDate = commonDAO.getSysDate();
			}
			if (toDate == null) {
				toDate = (fromDate == null) ? fromDate : commonDAO.getSysDate();
			}
			List<Rpt_1_2_TKDHGTN_VO> lstResult = new ArrayList<Rpt_1_2_TKDHGTN_VO>();
			
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho.BC_1_2_TKDHGTN(?, :shopId, :cycleId, :fromDate, :toDate, :staffRootId, :roleId, :shopRootId) ");
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(shopId);
			params.add(java.sql.Types.VARCHAR);
			params.add(sqlParamIndex++);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(Rpt_1_2_TKDHGTN_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<Rpt_1_3_GHTNVGH_VO> report1D3GHTNVGH(String shopId, Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		try {
			if (StringUtility.isNullOrEmpty(shopId)) {
				shopId = String.valueOf(ALL_INT_G); //Dinh nghia Id < 0
			}
			if (cycleId == null) {
				cycleId = Long.valueOf(ALL_INT_G);
			}
			if (fromDate == null) {
				fromDate = commonDAO.getSysDate();
			}
			if (toDate == null) {
				toDate = (fromDate == null) ? fromDate : commonDAO.getSysDate();
			}
			List<Rpt_1_3_GHTNVGH_VO> lstResult = new ArrayList<Rpt_1_3_GHTNVGH_VO>();
			
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho.bc_1_3_ghtnvgh(?, :shopId, :cycleId, :fromDate, :toDate, :staffRootId, :roleId, :shopRootId) ");
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(shopId);
			params.add(java.sql.Types.VARCHAR);
			params.add(sqlParamIndex++);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(Rpt_1_3_GHTNVGH_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public Rpt_1_4_G_BCDHCR_VO report1D4BCDHR(String shopId, Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		Rpt_1_4_G_BCDHCR_VO res = new Rpt_1_4_G_BCDHCR_VO();
		try {
			if (StringUtility.isNullOrEmpty(shopId)) {
				shopId = String.valueOf(ALL_INT_G); //Dinh nghia Id < 0
			}
			if (cycleId == null) {
				cycleId = Long.valueOf(ALL_INT_G);
			}
			if (fromDate == null) {
				fromDate = commonDAO.getSysDate();
			}
			if (toDate == null) {
				toDate = (fromDate == null) ? fromDate : commonDAO.getSysDate();
			}
			List<Rpt_1_4_BCDHR_VO> lstResult = new ArrayList<Rpt_1_4_BCDHR_VO>();

			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho.bc_1_4_bcdhr(?, :shopId, :cycleId, :fromDate, :toDate, :staffRootId, :roleId, :shopRootId) ");
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(shopId);
			params.add(java.sql.Types.VARCHAR);
			params.add(sqlParamIndex++);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(Rpt_1_4_BCDHR_VO.class, sql.toString(), params, null);

			//Xu ly cho du lieu tra ve
			BigDecimal tongGiaTriDH = BigDecimal.ZERO;
			BigDecimal tongChietKhau = BigDecimal.ZERO;
			BigDecimal tongSoTienPSTN = BigDecimal.ZERO;
			if (lstResult != null && !lstResult.isEmpty()) {
				for (Rpt_1_4_BCDHR_VO row : lstResult) {
					tongGiaTriDH = tongGiaTriDH.add(row.getGiaTriDH());
					tongChietKhau = tongChietKhau.add(row.getChietKhau());
					tongSoTienPSTN = tongSoTienPSTN.add(row.getSoTienPSTN());
				}
				res.setTongChietKhau(tongChietKhau);
				res.setTongGiaTriDH(tongGiaTriDH);
				res.setTongSoTienPSTN(tongSoTienPSTN);
				res.setLstData(lstResult);
			} else {
				res.setLstData(null);
			}
			res.setTongChietKhau(tongChietKhau);
			res.setTongGiaTriDH(tongGiaTriDH);
			res.setTongSoTienPSTN(tongSoTienPSTN);

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		//Tra ket qua ve
		return res;
	}
	
	@Override
	public List<RawDataIDPVO> exportRawData(Long userId, Long roleId, Long shopId, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<RawDataIDPVO> lstResult = new ArrayList<RawDataIDPVO>();
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho.KPI_6_4_4_RAW_DATA(?, :userId, :roleId, :shopId, :fromDate, :toDate) ");
			int setPR = 2;
			params.add(setPR++);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(setPR++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			lstResult = repo.getListByQueryDynamicFromPackage(RawDataIDPVO.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<RawDataStockTransVO> exportRawDataStockTrans(Long userId, Long roleId, Long shopId, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<RawDataStockTransVO> lstResult = new ArrayList<RawDataStockTransVO>();
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho_p2.KPI_6_4_5_RAW_DATA_STOCK_TRANS(?, :userId, :roleId, :shopId, :fromDate, :toDate) ");
			int setPR = 2;
			params.add(setPR++);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(setPR++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			lstResult = repo.getListByQueryDynamicFromPackage(RawDataStockTransVO.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<TimeKeepingVO> exportTimeKeeping(Long userId, Long roleId, Long shopId, Date fromDate, Date toDate, int numDay) throws BusinessException {
		try {
			List<TimeKeepingVO> lstResult = new ArrayList<TimeKeepingVO>();
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho.KPI_6_3_5_TIME_KEEPING(?, :userId, :roleId, :shopId, :fromDate, :toDate, :numDay) ");
			int setPR = 2;
			params.add(setPR++);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(setPR++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(setPR++);
			params.add(numDay);
			params.add(java.sql.Types.NUMERIC);
			
			lstResult = repo.getListByQueryDynamicFromPackage(TimeKeepingVO.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<ReceivableSummaryVO> exportReceivableSummary(Long userId, Long roleId, Long shopId, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<ReceivableSummaryVO> lstResult = new ArrayList<ReceivableSummaryVO>();
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho_p2.REPORT_3_1_RECEIVABLE_SUMMARY(?, :userId, :roleId, :shopId, :fromDate, :toDate) ");
			int setPR = 2;
			params.add(setPR++);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(setPR++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			lstResult = repo.getListByQueryDynamicFromPackage(ReceivableSummaryVO.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
//	@Override
//	public List<PayableDetailVO> exportPayableDetail(Long userId, Long roleId, Long shopId, Date fromDate, Date toDate) throws BusinessException {
//		try {
//			List<PayableDetailVO> lstResult = new ArrayList<PayableDetailVO>();
//			StringBuilder sql = new StringBuilder();
//			List<Object> params = new ArrayList<Object>();
//			sql.append("call pkg_rpt_ho_p2.REPORT_3_4_PAYABLE_DETAIL(?, :userId, :roleId, :shopId, :fromDate, :toDate) ");
//			int setPR = 2;
//			params.add(setPR++);
//			params.add(userId);
//			params.add(java.sql.Types.NUMERIC);
//			
//			params.add(setPR++);
//			params.add(roleId);
//			params.add(java.sql.Types.NUMERIC);
//			
//			params.add(setPR++);
//			params.add(shopId);
//			params.add(java.sql.Types.NUMERIC);
//			
//			params.add(setPR++);
//			params.add(new java.sql.Date(fromDate.getTime()));
//			params.add(java.sql.Types.DATE);
//			
//			params.add(setPR++);
//			params.add(new java.sql.Date(toDate.getTime()));
//			params.add(java.sql.Types.DATE);
//			
//			lstResult = repo.getListByQueryDynamicFromPackage(PayableDetailVO.class, sql.toString(), params, null);
//			return lstResult;
//		} catch (DataAccessException ex) {
//			throw new BusinessException(ex);
//		}
//	}

	@Override
	public List<RptCTKMVO> exportBCCTKM(Long staffIdRoot, Long roleId, Long shopRootId, Date fromDate, Date toDate) throws BusinessException {
		List<RptCTKMVO> lstResult = new ArrayList<RptCTKMVO>();
		try {
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call pkg_rpt_ho_p2.REPORT_5_2_BCCTKM(?, :userId, :roleId, :shopId, :fromDate, :toDate) ");
			int setPR = 2;
			params.add(setPR++);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(setPR++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			lstResult = repo.getListByQueryDynamicFromPackage(RptCTKMVO.class, sql.toString(), params, null);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
		return lstResult;
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.report.HoReportMgr#export2D3CTMH(java.lang.Long, java.lang.Long, java.lang.String, java.util.Date, java.util.Date)
	 */
	@Override
	public List<Rpt_2_3_CTMH> export2D3CTMH(Long staffIdRoot, Long roleId, String strListShopId, Date fDate, Date tDate) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<Rpt_2_3_CTMH> lstResult = new ArrayList<Rpt_2_3_CTMH>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_RPT_HO.REPORT_2_3_PO_DETAIL_ORDER(?, :userId, :roleId, :lstShopId, :fromDate, :toDate)");	
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(Rpt_2_3_CTMH.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<Rpt_2_1_CTHHMV> export2D1CTHHMV(Long staffIdRoot, Long roleId, String strListShopId, Date fDate, Date tDate) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<Rpt_2_1_CTHHMV> lstResult = new ArrayList<Rpt_2_1_CTHHMV>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_RPT_HO.REPORT_2_1_PO_ORDER(?, :userId, :roleId, :lstShopId, :fromDate, :toDate)");	
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(Rpt_2_1_CTHHMV.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<Rpt_2_2_SSSLDHMH> export2D2SSSLDHMH(Long staffIdRoot, Long roleId, String strListShopId, Date fDate, Date tDate) throws BusinessException {
		if (null == fDate || null == tDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<Rpt_2_2_SSSLDHMH> lstResult = new ArrayList<Rpt_2_2_SSSLDHMH>();
			StringBuilder sql = new StringBuilder();	
			sql.append("call PKG_RPT_HO.REPORT_2_2_PO_DETAIL_ORDER(?, :userId, :roleId, :lstShopId, :fromDate, :toDate)");	
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(Rpt_2_2_SSSLDHMH.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<Rpt_3_1_1_9_DSKHMT_F11_VO> getRptWVKD03F11_DSKHMTB(Long staffRootId, Long roleId, Long shopRootId, String lstShop, Integer typeInfo, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<Rpt_3_1_1_9_DSKHMT_F11_VO> lstData = new ArrayList<Rpt_3_1_1_9_DSKHMT_F11_VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_EQUIPMENT_REPORT.BAOCAO_KH_MUON_THIET_BI(?, :staffRootId, :roleId, :shopRootId, :lstShop, :typeInfo, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(Types.NUMERIC);

			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(lstShop);
			params.add(Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(typeInfo);
			params.add(Types.INTEGER);

			if (ActiveType.STOPPED.getValue().equals(typeInfo)) {
				Date sysDate = commonDAO.getSysDate();
				fromDate = sysDate; // truyen khong su dung typeInfo = 0
				toDate = sysDate; // truyen khong su dung typeInfo = 0
			}
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			lstData = repo.getListByQueryDynamicFromPackage(Rpt_3_1_1_9_DSKHMT_F11_VO.class, sql.toString(), params, null);
			return lstData;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<RptKK1_1VO> getExportKKTB11(Long staffRootId, Long roleId, Long shopRootId, String lstShopId, Long statisticRecordId, String period, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<RptKK1_1VO> lstData = new ArrayList<RptKK1_1VO>();
			StringBuilder sql = new StringBuilder();
			//sql.append("call PKG_EQUIPMENT_REPORT.BAOCAO_KKTB11(?, :staffRootId, :roleId, :shopRootId, :lstShopId, :statisticRecord, :period, :fromDate, :toDate)");
			sql.append("call PKG_EQUIPMENT_REPORT.BAOCAO_KKTB11(?, :staffRootId, :roleId, :shopRootId, :lstShopId, :statisticRecordId)");
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(Types.NUMERIC);

			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(lstShopId);
			params.add(Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(statisticRecordId);
			params.add(Types.VARCHAR);

			/*params.add(sqlParamIndex++);
			params.add(period);
			params.add(Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);*/

			lstData = repo.getListByQueryDynamicFromPackage(RptKK1_1VO.class, sql.toString(), params, null);

			return lstData;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Rpt_3_1_1_4_SC1_1VODetail> exportBCPSCCTH(Long staffRootId, Long roleId, Long shopRootId, String lstShopId, String groupMultilId, String categoryIdMultil, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<Rpt_3_1_1_4_SC1_1VODetail> lstData = new ArrayList<Rpt_3_1_1_4_SC1_1VODetail>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_EQUIPMENT_REPORT.BAOCAO_PSCCTH(?, :staffRootId, :roleId, :shopRootId, :lstShopId, :groupMultilId, :lstCategoryId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(Types.NUMERIC);

			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(lstShopId);
			params.add(Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(groupMultilId);
			params.add(Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(categoryIdMultil);
			params.add(Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstData = repo.getListByQueryDynamicFromPackage(Rpt_3_1_1_4_SC1_1VODetail.class, sql.toString(), params, null);

			return lstData;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Rpt_3_1_1_8_BCTDTMSC_F10_VO> getRptBCTDTMSC(Long staffRootId, Long roleId, Long shopRootId, String lstShopId, String groupMultilId, String categoryIdMultil, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<Rpt_3_1_1_8_BCTDTMSC_F10_VO> lstData = new ArrayList<Rpt_3_1_1_8_BCTDTMSC_F10_VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_EQUIPMENT_REPORT.BAOCAO_WV_KD_03_F10(?, :staffRootId, :roleId, :shopRootId, :lstShopId, :groupMultilId, :lstCategoryId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();

			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(Types.NUMERIC);

			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(lstShopId);
			params.add(Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(groupMultilId);
			params.add(Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(categoryIdMultil);
			params.add(Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			lstData = repo.getListByQueryDynamicFromPackage(Rpt_3_1_1_8_BCTDTMSC_F10_VO.class, sql.toString(), params, null);

			return lstData;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * Bao cao 3.1.1.5; Bao cao mat tu
	 * @author vuongmq
	 * @date 18/05/2015
	 * @return file XLS
	 * @throws IOException
	 */
	@Override
	public List<RptBCBAOMAT1_5VO> getRptBCBAOMAT1_5(Long staffRootId, Long roleId, Long shopRootId, String lstShopId, String lstEquipId, String categoryIdMultil, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<RptBCBAOMAT1_5VO> lstData = new ArrayList<RptBCBAOMAT1_5VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_EQUIPMENT_REPORT.BC_BAO_MAT_1_5(?, :lstShopId, :lstEquipId, :lstCategoryId, :fromDate, :toDate, :staffRootId, :roleId, :shopRootId)");
			List<Object> params = new ArrayList<Object>();

			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(lstShopId);
			params.add(Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(lstEquipId);
			params.add(Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(categoryIdMultil);
			params.add(Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);

			/*params.add(5);
			params.add(lstIslevel);
			params.add(Types.VARCHAR);*/
			lstData = repo.getListByQueryDynamicFromPackage(RptBCBAOMAT1_5VO.class, sql.toString(), params, null);

			return lstData;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * Bao cao 3.1.1.6; Bao cao danh sach Thông tin mượn thiết bị
	 * @author 
	 * @throws IOException
	 */
	@Override
	public List<Rpt_3116_WVKD_03F5SC1_1VO> getRptBCDSMTDTMTK(Long staffRootId, Long roleId, Long shopRootId, String lstShopId, String lstEquipId, String categoryIdMultil, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<Rpt_3116_WVKD_03F5SC1_1VO> lstData = new ArrayList<Rpt_3116_WVKD_03F5SC1_1VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_EQUIPMENT_REPORT.TB_3116_WV_KD_03F5(?, :lstShopId, :lstEquipId, :lstCategoryId, :fromDate, :toDate, :staffRootId, :roleId, :shopRootId)");
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			
			params.add(sqlParamIndex++);
			params.add(lstShopId);
			params.add(Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(lstEquipId);
			params.add(Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(categoryIdMultil);
			params.add(Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);
			
			lstData = repo.getListByQueryDynamicFromPackage(Rpt_3116_WVKD_03F5SC1_1VO.class, sql.toString(), params, null);
			return lstData;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<RptBCTDTMTHVO> getRptBCDSTDTMTH(Long staffRootId, Long roleId, Long shopRootId, String lstShopId, String lstEquipId, String categoryIdMultil, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<RptBCTDTMTHVO> lstData = new ArrayList<RptBCTDTMTHVO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_EQUIPMENT_REPORT.TB_3_1_1_7_TD_TM_TH_TK(?, :lstShopId, :lstEquipId, :lstCategoryId, :fromDate, :toDate, :staffRootId, :roleId, :shopRootId)");
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			
			params.add(sqlParamIndex++);
			params.add(lstShopId);
			params.add(Types.VARCHAR);

			params.add(sqlParamIndex++);
			params.add(lstEquipId);
			params.add(Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(categoryIdMultil);
			params.add(Types.VARCHAR);
			
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);

			/*params.add(5);
			params.add(lstIslevel);
			params.add(Types.VARCHAR);*/
			lstData = repo.getListByQueryDynamicFromPackage(RptBCTDTMTHVO.class, sql.toString(), params, null);

			return lstData;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Rpt_3_1_1_3_BCDSTL11_VO> getRptBCDSTL11(Long shopId, String lstEquipId, String categoryIdMultil, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		try {
			List<Rpt_3_1_1_3_BCDSTL11_VO> lstData = new ArrayList<Rpt_3_1_1_3_BCDSTL11_VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_EQUIPMENT_REPORT.BAOCAO_THIETBI_TL11(?, :shopId, :fromDate, :toDate, :lstEquipmentGroupId, :lstCategoryId, :staffRootId, :roleId, :shopRootId)");
			List<Object> params = new ArrayList<Object>();

			params.add(2);
			params.add(shopId);
			params.add(Types.NUMERIC);
			
			params.add(3);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(Types.DATE);
			
			params.add(4);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(Types.DATE);
			
			params.add(5);
			params.add(lstEquipId);
			params.add(Types.VARCHAR);
			
			params.add(6);
			params.add(categoryIdMultil);
			params.add(Types.VARCHAR);

			params.add(7);
			params.add(staffRootId);
			params.add(Types.NUMERIC);
			
			params.add(8);
			params.add(roleId);
			params.add(Types.NUMERIC);
			
			params.add(9);
			params.add(shopRootId);
			params.add(Types.NUMERIC);
			
			lstData = repo.getListByQueryDynamicFromPackage(Rpt_3_1_1_3_BCDSTL11_VO.class, sql.toString(), params, null);

			return lstData;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<Rpt_3_1_1_10_BCTHTDTM_F12_VO> getRptF12(Long shopId, String lstEquipId, String categoryIdMultil, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		try {
			List<Rpt_3_1_1_10_BCTHTDTM_F12_VO> lstData = new ArrayList<Rpt_3_1_1_10_BCTHTDTM_F12_VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_EQUIPMENT_REPORT.BAOCAO_WV_KD_03_F12(?, :shopId, :fromDate, :toDate, :lstEquipmentGroupId, :lstCategoryId, :staffRootId, :roleId, :shopRootId)");
			List<Object> params = new ArrayList<Object>();

			params.add(2);
			params.add(shopId);
			params.add(Types.NUMERIC);
			
			params.add(3);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(Types.DATE);
			
			params.add(4);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(Types.DATE);
			
			params.add(5);
			params.add(lstEquipId);
			params.add(Types.VARCHAR);
			
			params.add(6);
			params.add(categoryIdMultil);
			params.add(Types.VARCHAR);

			params.add(7);
			params.add(staffRootId);
			params.add(Types.NUMERIC);
			
			params.add(8);
			params.add(roleId);
			params.add(Types.NUMERIC);
			
			params.add(9);
			params.add(shopRootId);
			params.add(Types.NUMERIC);
			
			lstData = repo.getListByQueryDynamicFromPackage(Rpt_3_1_1_10_BCTHTDTM_F12_VO.class, sql.toString(), params, null);
			return lstData;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}
}
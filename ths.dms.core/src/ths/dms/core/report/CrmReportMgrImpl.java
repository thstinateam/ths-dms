package ths.dms.core.report;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ExceptionDay;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.enumtype.ProductType;
import ths.core.entities.vo.rpt.RptBCKD10;
import ths.core.entities.vo.rpt.RptBCKD10VO;
import ths.core.entities.vo.rpt.RptBCKD10_2DataVO;
import ths.core.entities.vo.rpt.RptBCKD10_2VO;
import ths.core.entities.vo.rpt.RptBCKD10_2_DLCPPTSkusDNVO;
import ths.core.entities.vo.rpt.RptBCKD10_3;
import ths.core.entities.vo.rpt.RptBCKD10_3VO;
import ths.core.entities.vo.rpt.RptBCKD11;
import ths.core.entities.vo.rpt.RptBCKD11VO;
import ths.core.entities.vo.rpt.RptBCKD11_1;
import ths.core.entities.vo.rpt.RptBCKD11_1VO;
import ths.core.entities.vo.rpt.RptBCKD11_2CTVO;
import ths.core.entities.vo.rpt.RptBCKD11_2VO;
import ths.core.entities.vo.rpt.RptBCKD12DataVO;
import ths.core.entities.vo.rpt.RptBCKD12VO;
import ths.core.entities.vo.rpt.RptBCKD13VO;
import ths.core.entities.vo.rpt.RptBCKD14DetailVO;
import ths.core.entities.vo.rpt.RptBCKD14VO;
import ths.core.entities.vo.rpt.RptBCKD15DataVO;
import ths.core.entities.vo.rpt.RptBCKD15VO;
import ths.core.entities.vo.rpt.RptBCKD16DataVO;
import ths.core.entities.vo.rpt.RptBCKD16VO;
import ths.core.entities.vo.rpt.RptBCKD17VO;
import ths.core.entities.vo.rpt.RptBCKD18CTVO;
import ths.core.entities.vo.rpt.RptBCKD18VO;
import ths.core.entities.vo.rpt.RptBCKD19DataVO;
import ths.core.entities.vo.rpt.RptBCKD19VO;
import ths.core.entities.vo.rpt.RptBCKD19_1;
import ths.core.entities.vo.rpt.RptBCKD19_1VO;
import ths.core.entities.vo.rpt.RptBCKD1_1;
import ths.core.entities.vo.rpt.RptBCKD1_1VO;
import ths.core.entities.vo.rpt.RptBCKD1_2;
import ths.core.entities.vo.rpt.RptBCKD1_2_Mien;
import ths.core.entities.vo.rpt.RptBCKD1_2_Vung;
import ths.core.entities.vo.rpt.RptBCKD2;
import ths.core.entities.vo.rpt.RptBCKD21;
import ths.core.entities.vo.rpt.RptBCKD21VO;
import ths.core.entities.vo.rpt.RptBCKD2Detail;
import ths.core.entities.vo.rpt.RptBCKD3;
import ths.core.entities.vo.rpt.RptBCKD3VO;
import ths.core.entities.vo.rpt.RptBCKD5;
import ths.core.entities.vo.rpt.RptBCKD5VO;
import ths.core.entities.vo.rpt.RptBCKD6;
import ths.core.entities.vo.rpt.RptBCKD6VO;
import ths.core.entities.vo.rpt.RptBCKD7;
import ths.core.entities.vo.rpt.RptBCKD7VO;
import ths.core.entities.vo.rpt.RptBCKD9VO;
import ths.core.entities.vo.rpt.RptDSBHMienCTVO;
import ths.core.entities.vo.rpt.RptDSBHMienVO;
import ths.core.entities.vo.rpt.RptDSPPNHCTVO;
import ths.core.entities.vo.rpt.RptDSPPNHVO;
import ths.core.entities.vo.rpt.RptDiemNVBHCTVO;
import ths.core.entities.vo.rpt.RptDiemNVBHVO;
import ths.core.entities.vo.rpt.RptDoanhSoBanHangNVBHSKUNgayDataVO;
import ths.core.entities.vo.rpt.RptDoanhSoBanHangNVBHSKUNgayVO;
import ths.core.entities.vo.rpt.RptKD10_1CTVO;
import ths.core.entities.vo.rpt.RptKD10_1VO;
import ths.core.entities.vo.rpt.RptKD1_VO;
import ths.core.entities.vo.rpt.RptKD3_VO;
import ths.core.entities.vo.rpt.RptKD9;
import ths.core.entities.vo.rpt.RptKD9_Detail;
import ths.core.entities.vo.rpt.RptKD9_Vung;
import ths.core.entities.vo.rpt.RptKDAllowSubCattVO;
import ths.core.entities.vo.rpt.RptKD_16_1VO;
import ths.core.entities.vo.rpt.RptKD_16_1_CTVO;
import ths.core.entities.vo.rpt.RptKD_8_1VO;
import ths.core.entities.vo.rpt.RptKD_8_1_CTVO;
import ths.core.entities.vo.rpt.RptKD_General_VO;
import ths.core.entities.vo.rpt.rpt_HO_KD15VO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.business.SaleDayMgr;
import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.ExceptionDayDAO;
import ths.dms.core.dao.ProductInfoDAO;
import ths.dms.core.dao.SaleDayDAO;
import ths.dms.core.dao.repo.IRepository;

public class CrmReportMgrImpl implements CrmReportMgr {

	@Autowired
	SaleDayMgr saleDayMgr;
	
	@Autowired
	SaleDayDAO saleDayDAO;
	
	@Autowired
	ProductInfoDAO productInfoDAO;
	
	@Autowired
	ExceptionDayDAO exceptionDayDAO;
	
	@Autowired
	private IRepository repo;
	
	/* (non-Javadoc)
	 * @see ths.dms.core.report.CrmReportMgr#getDSBHMienReport(java.util.Date, java.util.Date)
	 */
	@Override
	public RptDSBHMienVO getDSBHMienReport(Date toDate)
			throws BusinessException {
		try {
			
			Calendar calFrom = Calendar.getInstance();
			calFrom.setTime(toDate);
			calFrom.set(Calendar.DATE, 1);
			Date fromDate = calFrom.getTime();
			calFrom.set(Calendar.MONTH, calFrom.get(Calendar.MONTH) -1);
			Date fromLastMonth = calFrom.getTime();
			Calendar calTo = Calendar.getInstance();
			calTo.setTime(toDate);
			calTo.set(Calendar.MONTH, calTo.get(Calendar.MONTH) -1);
			Date toLastMonth = calTo.getTime();
			 int lastDate = calTo.getActualMaximum(Calendar.DATE);

			 calTo.set(Calendar.DATE, lastDate);
			Date endLastMonth = calTo.getTime();
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			
			int soNgayNghiLe = saleDayMgr.getNumOfHoliday(fromDate, toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan,2);
			RptDSBHMienVO res = new RptDSBHMienVO();
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("fromLastMonth");
			inParams.add(fromLastMonth);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("toLastMonth");
			inParams.add(toLastMonth);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("endLastMonth");
			inParams.add(endLastMonth);
			inParams.add(StandardBasicTypes.DATE);
			
			List<RptDSBHMienCTVO> lstResultDetail = repo.getListByNamedQuery(
					RptDSBHMienCTVO.class,
					"PKG_CRM_REPORT.GET_DSBHMIEN_REPORT", inParams);
			
			//
			if(lstResultDetail != null && lstResultDetail.size() > 0){
				for (RptDSBHMienCTVO child : lstResultDetail) {
					if(child.getKeHoachTT() != null){
						child.setKeHoachLK((child.getKeHoachTT().multiply(new BigDecimal(soNgayThucHien)).divide(new BigDecimal(soNgayBanHang+0.0000001),1,RoundingMode.HALF_DOWN)));
						child.setKeHoachBQNgay((child.getKeHoachTT().divide(new BigDecimal(soNgayBanHang+0.0000001),1,RoundingMode.HALF_DOWN)));
					}
					child.setDuThieuLK((child.getThLuyke()!=null?child.getThLuyke():BigDecimal.ZERO).subtract(child.getKeHoachLK()!=null?child.getKeHoachLK():BigDecimal.ZERO));
				}
			}
			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstMien(lstResultDetail);
			
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			res.setSoNgayNghiLe(soNgayNghiLe);
			
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	public static void main(String[] args) {
		Calendar cal = Calendar.getInstance();
		cal.set(2013,0, 10);
		System.out.println(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		cal.set(2013,1, 10);
		System.out.println(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		cal.set(2013,2, 10);
		System.out.println(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		cal.set(2013,3, 10);
		System.out.println(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		cal.set(2013,4, 10);
		System.out.println(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		cal.set(2013,5, 10);
		System.out.println(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
	}
	
	@Override
	public RptDSPPNHVO getDSPPNHReport(Long shopId, List<Long> lstSp, Date toDate)
			throws BusinessException {
		try {
			if(shopId == null || toDate == null){
				return null;
			}
			Calendar calFrom = Calendar.getInstance();
			calFrom.setTime(toDate);
			calFrom.set(Calendar.DATE, 1);
			Date fromDate = calFrom.getTime();
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);
			
			String lstSpId = "";
			if(lstSp != null && lstSp.size() > 0){
				Boolean isFirst = true;
				for (Long child : lstSp) {
					if(isFirst){
						isFirst = false;
						lstSpId += child;
					}else{
						lstSpId += ","+child;
					}
				}
			}
			//
			RptDSPPNHVO res = new RptDSPPNHVO();
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);
			inParams.add("lstSpId");
			inParams.add(StringUtility.isNullOrEmpty(lstSpId)?null:lstSpId);
			inParams.add(StandardBasicTypes.STRING);
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			List<RptDSPPNHCTVO> lstResultDetail = repo.getListByNamedQuery(
					RptDSPPNHCTVO.class,
					"PKG_CRM_REPORT.KD1_3_PRO", inParams);
			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public RptDoanhSoBanHangNVBHSKUNgayVO getDoanhSoBanHangNVBHSKUNgayVOReport(
			Date toDate, 
			List<Long> tbhvId, Long shopId, List<Long> staffOwnerId,
			List<Long> staffId, List<Long> productId) throws BusinessException {
		String listOwnerId = null;
		String listTBHVId = null;
		String listStaffId = null;
		String listProductId = null;
		Integer workingDaysInMonth = null;
		Integer workedDaysOfMonth = null;

		try {
			Calendar calFrom = Calendar.getInstance();
			calFrom.setTime(toDate);
			calFrom.set(Calendar.DATE, 1);
			// calFrom.set(Calendar.MONTH, calFrom.get(Calendar.MONTH) - 1);
			// So ngay ban hang trong thang
			workingDaysInMonth = saleDayDAO.getSaleDayByDate(toDate);
			// So ngay ban hang da qua
			workedDaysOfMonth = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(calFrom.getTime(),
							toDate);

			if (workingDaysInMonth == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			float tienDoChuan = (float) workedDaysOfMonth / workingDaysInMonth;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);

			if (null != staffOwnerId && staffOwnerId.size() > 0) {
				StringBuilder temp = new StringBuilder();

				for (Long item : staffOwnerId) {
					temp.append(String.format("%s,", item));
				}

				listOwnerId = temp.deleteCharAt(temp.lastIndexOf(","))
						.toString();
			}

			if (null != tbhvId && tbhvId.size() > 0) {
				StringBuilder temp = new StringBuilder();

				for (Long item : tbhvId) {
					temp.append(String.format("%s,", item));
				}

				listTBHVId = temp.deleteCharAt(temp.lastIndexOf(","))
						.toString();
			}

			if (null != staffId && staffId.size() > 0) {
				StringBuilder temp = new StringBuilder();

				for (Long item : staffId) {
					temp.append(String.format("%s,", item));
				}

				listStaffId = temp.deleteCharAt(temp.lastIndexOf(","))
						.toString();
			}

			if (null != productId && productId.size() > 0) {
				StringBuilder temp = new StringBuilder();

				for (Long item : productId) {
					temp.append(String.format("%s,", item));
				}

				listProductId = temp.deleteCharAt(temp.lastIndexOf(","))
						.toString();
			}

			RptDoanhSoBanHangNVBHSKUNgayVO res = new RptDoanhSoBanHangNVBHSKUNgayVO();
			
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("listOwnerId");
			inParams.add(StringUtility.isNullOrEmpty(listOwnerId) ? null
					: listOwnerId);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("listTBHVId");
			inParams.add(StringUtility.isNullOrEmpty(listTBHVId) ? null
					: listTBHVId);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("listStaffId");
			inParams.add(StringUtility.isNullOrEmpty(listStaffId) ? null
					: listStaffId);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("listProductId");
			inParams.add(StringUtility.isNullOrEmpty(listProductId) ? null
					: listProductId);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("workingDaysInMonth");
			inParams.add(workingDaysInMonth);
			inParams.add(StandardBasicTypes.INTEGER);

			inParams.add("workedDaysOfMonth");
			inParams.add(workedDaysOfMonth);
			inParams.add(StandardBasicTypes.INTEGER);

			List<RptDoanhSoBanHangNVBHSKUNgayDataVO> lstResultDetail = repo
					.getListByNamedQuery(RptDoanhSoBanHangNVBHSKUNgayDataVO.class,
							"PKG_CRM_REPORT.KD1_REPORT",
							inParams);

			res.setNgayBatDau(calFrom.getTime());
			res.setNgayKetThuc(toDate);
			res.setData(lstResultDetail);
			res.setSoNgayBanHang(workingDaysInMonth);
			res.setSoNgayThucHien(workedDaysOfMonth);
			res.setTienDoChuan(tienDoChuan);

			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	
	@Override
	public RptDiemNVBHVO getDiemNVBHTheoTuyenReport(Long shopId, List<Long> lstStaffId,
			Date fromDate, Date toDate) throws BusinessException {
		try {
			Calendar calFrom = Calendar.getInstance();
			calFrom.setTime(fromDate);
			calFrom.set(Calendar.DATE, 1);
			calFrom.set(Calendar.MONTH, calFrom.get(Calendar.MONTH) -1);
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);
			RptDiemNVBHVO res = new RptDiemNVBHVO();
			String listShopId = null;
			if (shopId != null) {
				listShopId = shopId + "";
			}
			
			String lstStaffIdStr = "";
			if(lstStaffId != null && lstStaffId.size() > 0){
				Boolean isFirst = true;
				for (Long child : lstStaffId) {
					if(isFirst){
						isFirst = false;
						lstStaffIdStr += child;
					}else{
						lstStaffIdStr += ","+child;
					}
				}
			}
			
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("listShopId");
			inParams.add(StringUtility.isNullOrEmpty(listShopId)?null:listShopId);
			inParams.add(StandardBasicTypes.STRING);
			inParams.add("listStaffId");
			inParams.add(StringUtility.isNullOrEmpty(lstStaffIdStr)?null:lstStaffIdStr);
			inParams.add(StandardBasicTypes.STRING);
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			
			List<RptDiemNVBHCTVO> lstResultDetail = repo.getListByNamedQuery(
					RptDiemNVBHCTVO.class,
					"PKG_CRM_REPORT.GET_DIEM_NVBH_TUYEN_REPORT", inParams);
			res.setNgayBatDau(fromDate);
			res.setNgayKetThuc(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptBCKD6 getRptBCKD6(Date fromDate, Date toDate) throws BusinessException {
		try {
			Calendar calFrom = Calendar.getInstance();
			calFrom.setTime(fromDate);
			calFrom.set(Calendar.DATE, 1);
			calFrom.set(Calendar.MONTH, calFrom.get(Calendar.MONTH) -1);
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			Integer soNgayNghiLe = saleDayMgr.getNumOfHoliday(fromDate, toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, new Date());
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);
			RptBCKD6 res = new RptBCKD6();
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("soNgayBan");
			inParams.add(soNgayBanHang);
			inParams.add(StandardBasicTypes.INTEGER);
			
			List<RptBCKD6VO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD6VO.class,
					"PKG_CRM_REPORT.KD6", inParams);
			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan*100);
			res.setSoNgayNghiLe(soNgayNghiLe);
			
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	private String listToString(List<Long> lst) {
		StringBuilder rs = new StringBuilder();
		if (lst != null) {
			for (Long item: lst) {
				rs.append(",");
				rs.append(item);
			}
		}
		if (rs.length() > 0) {
			return rs.substring(1);
		}
		else 
			return "";
	}
	
	@Override
	public RptBCKD1_1 getRptBCKD1_1(List<Long> listShopId, List<Long> listIdSp, Date toDate)
			throws BusinessException {
		try {
			
			String strListShopId = this.listToString(listShopId);
			String strIdSp = this.listToString(listIdSp);
			
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(toDate);
			fromDate.set(Calendar.DATE, 1);
			
			List<Object> inParams = new ArrayList<Object>();
			
			
			inParams.add("listShopId");
			inParams.add(strListShopId);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("listIdSp");
			inParams.add(strIdSp);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("fromDate");
			inParams.add(fromDate.getTime());
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			
			List<RptBCKD1_1VO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD1_1VO.class,
					"PKG_CRM_REPORT.KD1_1", inParams);
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate.getTime(), toDate);
			
//			if (soNgayBanHang == null) {
//				throw new IllegalArgumentException("soNgayBanHang is null");
//			}
			float tienDoChuan = soNgayBanHang == null ? 0 : (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);
			RptBCKD1_1 res = new RptBCKD1_1();
			res.setTuNgay(fromDate.getTime());
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.core.report.CrmReportMgr#getRptBCKD3(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 */
	@Override
	public RptBCKD3 getRptBCKD3(String matbhv,
			String magsnpp, String manvbh, String manpp, Date toDate, String cat)
			throws BusinessException {
		// TODO Auto-generated method stub
		try {
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(toDate);
			fromDate.set(Calendar.DATE, 1);
			
			List<Object> inParams = new ArrayList<Object>();
			/*
			 * :npp,:gsnpp,:tbhvung,:vung,:mien,:nvbh,:tungay,:denngay,:cat,:songaythuchien,:songaybanhang
			 */
			inParams.add("npp");
			inParams.add(manpp);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("gsnpp");
			inParams.add(magsnpp);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("tbhvung");
			inParams.add(matbhv);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("nvbh");
			inParams.add(manvbh);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("tungay");
			inParams.add(fromDate.getTime());
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("denngay");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("cat");
			inParams.add(cat);
			inParams.add(StandardBasicTypes.STRING);

			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate.getTime(), toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);
			inParams.add("songaythuchien");
			inParams.add(soNgayThucHien);
			inParams.add(StandardBasicTypes.INTEGER);
			
			inParams.add("songaybanhang");
			inParams.add(soNgayBanHang);
			inParams.add(StandardBasicTypes.INTEGER);
			
			List<RptBCKD3VO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD3VO.class,
					"PKG_CRM_REPORT.KD3_REPORT", inParams);
			
			for(RptBCKD3VO rpt : lstResultDetail) {
				try {
					rpt.safeSetNull();
				} catch (Exception e) {
					throw new BusinessException(e);
				}
			}
			
			RptBCKD3 res = new RptBCKD3();
			res.setTuNgay(fromDate.getTime());
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	
	@Override
	public RptBCKD2 getRptBCKD2(List<Long> listTbhv, List<Long> listShopId, List<Long> listGsNpp, List<Long> listNvbh,
			List<Long> listNhanId, Date toDate) throws BusinessException {
		try {
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(toDate);
			fromDate.set(Calendar.DATE, 1);
			
			String strListTbhv = this.listToString(listTbhv);
			String strListShopId = this.listToString(listShopId);
			String strListGsNpp = this.listToString(listGsNpp);
			String strListNvbh = this.listToString(listNvbh);
			String strListNhanId = this.listToString(listNhanId);
			
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate.getTime(), toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);
			
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add("listTbhv");
			inParams.add(strListTbhv);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("listShopId");
			inParams.add(strListShopId);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("listGsNpp");
			inParams.add(strListGsNpp);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("listNvbh");
			inParams.add(strListNvbh);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("listNhanId");
			inParams.add(strListNhanId);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("tien_do_chuan");
			inParams.add(tienDoChuan);
			inParams.add(StandardBasicTypes.FLOAT);
			
			inParams.add("so_ngay_ban_hang");
			inParams.add(soNgayBanHang);
			inParams.add(StandardBasicTypes.INTEGER);
			
			inParams.add("so_ngay_thuc_hien");
			inParams.add(soNgayThucHien);
			inParams.add(StandardBasicTypes.INTEGER);
			
			inParams.add("from_date");
			inParams.add(fromDate.getTime());
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("to_date");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			
			
			List<RptBCKD2Detail> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD2Detail.class,
					"PKG_CRM_REPORT.KD2", inParams);
			
			RptBCKD2 res = new RptBCKD2();
			res.setTuNgay(fromDate.getTime());
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	
	@Override
	public List<RptBCKD9VO> getRptBCKD9(List<Long> lstIdNpp, List<Long> lstIdCat, List<Long> lstIdSp, Date fromDate, Date toDate)
			throws BusinessException {
		try {
			if (fromDate == null) {
				throw new IllegalArgumentException("fromDate is null");
			}
			if (toDate == null) {
				throw new IllegalArgumentException("toDate is null");
			}
			String listIdNpp = this.listToString(lstIdNpp);
			String listIdCat = this.listToString(lstIdCat);
			String listIdSp = this.listToString(lstIdSp);
			
			List<Object> inParams = new ArrayList<Object>();
			
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			
			inParams.add("listIdNpp");
			inParams.add(listIdNpp);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("listIdCat");
			inParams.add(listIdCat);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("listIdSp");
			inParams.add(listIdSp);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("strFromDate");
			inParams.add(df.format(fromDate));
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("strToDate");
			inParams.add(df.format(toDate));
			inParams.add(StandardBasicTypes.STRING);
			
			
			List<RptBCKD9VO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD9VO.class,
					"PKG_CRM_REPORT.KD9", inParams);
			
			if(lstResultDetail != null && lstResultDetail.size() > 0) {
				for(RptBCKD9VO rpt : lstResultDetail) {
					try {
						rpt.safeSetNull();
					} catch (Exception e) {
						throw new BusinessException(e);
					}
				}
			}
			
			return lstResultDetail;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public RptBCKD10 getRptBCKD10(Long braneId, Date fromDate,
			Date toDate) throws BusinessException {
		try {
			List<Object> inParams = new ArrayList<Object>();
			/*
			 * :braneId,:thang,:nam,tungay,:denngay
			 */
			Integer month = DateUtility.getMonth(fromDate);
			Integer year = DateUtility.getYear(fromDate);
			
			inParams.add("braneId");
			inParams.add(braneId);
			inParams.add(StandardBasicTypes.LONG);
			
			inParams.add("thang");
			inParams.add(month);
			inParams.add(StandardBasicTypes.INTEGER);
			
			inParams.add("nam");
			inParams.add(year);
			inParams.add(StandardBasicTypes.INTEGER);
			
			inParams.add("tungay");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("denngay");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptBCKD10VO> lstResultDetail = repo.getListByNamedQuery(RptBCKD10VO.class,	"PKG_CRM_REPORT.KD10_REPORT", inParams);
			
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);
			RptBCKD10 res = new RptBCKD10();
			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public RptBCKD5 getRptBCKD5(Long shopId,String lstCategoryId,Date toDate)
			throws BusinessException {
		if(null == toDate){
			throw new IllegalArgumentException(ExceptionCode.REPORT_FROM_DATE_IS_NULL);
		}
		try {
			Calendar calFrom = Calendar.getInstance();
			calFrom.setTime(toDate);
			calFrom.set(Calendar.DATE, 1);
			Date fromDate = calFrom.getTime();
			
			List<Object> inParams = new ArrayList<Object>();

			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);

			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			
			int soNgayNghi = this.exceptionDayDAO.getCountDayOffBetween(fromDate, toDate);

			float tienDoChuan = (float) soNgayThucHien / soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);
			
			inParams.add(2);
			inParams.add(shopId);
			inParams.add(Types.NUMERIC);
			
			inParams.add(3);
			inParams.add(lstCategoryId);
			inParams.add(Types.VARCHAR);
			
			inParams.add(4);
			inParams.add(new java.sql.Date(fromDate.getTime()));
			inParams.add(Types.DATE);

			inParams.add(5);
			inParams.add(new java.sql.Date(toDate.getTime()));
			inParams.add(Types.DATE);
			
			inParams.add(6);
			inParams.add(soNgayThucHien);
			inParams.add(Types.NUMERIC);
			
			inParams.add(7);
			inParams.add(soNgayBanHang);
			inParams.add(Types.NUMERIC);
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.kd5_report(?,?,?,?,?,?,?)");
			//sql.append("call vuonghn.kd5_report(?,?,?,?,?,?,?)");
			List<RptBCKD5VO> lstResultDetail = repo.getListByQueryDynamicFromPackage(RptBCKD5VO.class, sql.toString(), inParams, null);

			RptBCKD5 res = new RptBCKD5();

			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setSoNgayNghiLe(soNgayNghi);
			res.setTienDoChuan(tienDoChuan);

			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptDiemNVBHVO getNVBHDiemKemReport(Long shopId,
			List<Long> listStaffId, Date fromDate, Date toDate, Integer soLan)
			throws BusinessException {
		StringBuilder strStaffId = new StringBuilder();

		if (null == shopId) {
			throw new IllegalArgumentException(
					ExceptionCode.REPORT_SHOP_ID_IS_NULL);
		}

		if (null == fromDate) {
			throw new IllegalArgumentException(
					ExceptionCode.REPORT_FROM_DATE_IS_NULL);
		}

		if (null == toDate) {
			throw new IllegalArgumentException(
					ExceptionCode.REPORT_TO_DATE_IS_NULL);
		}

		if (fromDate.after(toDate)) {
			throw new IllegalArgumentException(
					ExceptionCode.REPORT_FROM_DATE_AFTER_TO_DATE);
		}

		if (null == soLan) {
			throw new IllegalArgumentException(
					ExceptionCode.REPORT_KD8_1_SCORE_IS_NULL);
		}

		try {
			Calendar calFrom = Calendar.getInstance();

			calFrom.setTime(fromDate);
			calFrom.set(Calendar.DATE, 1);
			calFrom.set(Calendar.MONTH, calFrom.get(Calendar.MONTH) - 1);

			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);

			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);

			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}

			float tienDoChuan = (float) (soNgayThucHien) / soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);
			if (listStaffId != null) {
				for (int i = 0; i < listStaffId.size(); i++) {
					if (i < listStaffId.size() - 1) {
						strStaffId.append(listStaffId.get(i)).append(",");
					} else {
						strStaffId.append(listStaffId.get(i));
					}
				}
			}

			RptDiemNVBHVO res = new RptDiemNVBHVO();
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);
			inParams.add("listStaffId");
			inParams.add(StringUtility.isNullOrEmpty(strStaffId.toString()) ? null
					: strStaffId.toString());
			inParams.add(StandardBasicTypes.STRING);
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("numScore");
			inParams.add(soLan);
			inParams.add(StandardBasicTypes.INTEGER);

			List<RptDiemNVBHCTVO> lstResultDetail = repo.getListByNamedQuery(
					RptDiemNVBHCTVO.class, "PKG_CRM_REPORT.KD8_1_REPORT",
					inParams);
			res.setNgayBatDau(fromDate);
			res.setNgayKetThuc(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan*100);

			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptKD_8_1VO getKD8_1(Long shopId, List<Long> listStaffId, Date fromDate, Date toDate, Integer soLan)
			throws BusinessException {
		StringBuilder strStaffId = new StringBuilder();

		if (null == shopId) {
			throw new IllegalArgumentException(
					ExceptionCode.REPORT_SHOP_ID_IS_NULL);
		}

		if (null == fromDate) {
			throw new IllegalArgumentException(
					ExceptionCode.REPORT_FROM_DATE_IS_NULL);
		}

		if (null == toDate) {
			throw new IllegalArgumentException(
					ExceptionCode.REPORT_TO_DATE_IS_NULL);
		}

		if (fromDate.after(toDate)) {
			throw new IllegalArgumentException(
					ExceptionCode.REPORT_FROM_DATE_AFTER_TO_DATE);
		}

		if (null == soLan) {
			throw new IllegalArgumentException(
					ExceptionCode.REPORT_KD8_1_SCORE_IS_NULL);
		}

		try {
			Calendar calFrom = Calendar.getInstance();

			calFrom.setTime(fromDate);
			calFrom.set(Calendar.DATE, 1);
			calFrom.set(Calendar.MONTH, calFrom.get(Calendar.MONTH) - 1);

			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);

			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);

			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}

			float tienDoChuan = (float) (soNgayThucHien) / soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);
			if (listStaffId != null) {
				for (int i = 0; i < listStaffId.size(); i++) {
					if (i < listStaffId.size() - 1) {
						strStaffId.append(listStaffId.get(i)).append(",");
					} else {
						strStaffId.append(listStaffId.get(i));
					}
				}
			}

			RptKD_8_1VO res = new RptKD_8_1VO();
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);
			inParams.add("listStaffId");
			inParams.add(StringUtility.isNullOrEmpty(strStaffId.toString()) ? null
					: strStaffId.toString());
			inParams.add(StandardBasicTypes.STRING);
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("numScore");
			inParams.add(soLan);
			inParams.add(StandardBasicTypes.INTEGER);

			List<RptKD_8_1_CTVO> lstResultDetail = repo.getListByNamedQuery(
					RptKD_8_1_CTVO.class, "PKG_CRM_REPORT.KD8_1_REPORT",
					inParams);
			res.setNgayBatDau(fromDate);
			res.setNgayKetThuc(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan*100);

			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.core.report.CrmReportMgr#getRptBCKD11_1(java.lang.Long, java.util.Date, java.util.Date)
	 */
	@Override
	public RptBCKD11_1 getRptBCKD11_1(Long displayProgramId,
			Date fromDate, Date toDate) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<Object> inParams = new ArrayList<Object>();
			/*
			 * ( res out sys_refcursor , displayProId in NUMBER, fromDate IN
			 * DATE, toDate in DATE)
			 */
			
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan,2);
			
			RptBCKD11_1 res = new RptBCKD11_1();

			inParams.add("displayProId");
			inParams.add(displayProgramId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptBCKD11_1VO> lstResult = repo.getListByNamedQuery(
					RptBCKD11_1VO.class, "PKG_CRM_REPORT.KD11_1_REPORT",
					inParams);
			
			res.setLstDetail(lstResult);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/* (non-Javadoc)
	 * @see ths.dms.core.report.CrmReportMgr#getRptBCKD1_2(java.lang.String, java.lang.String, java.util.Date, java.util.Date)
	 */
	/**
	 * @author tungtt21
	 */
	@Override
	public RptBCKD1_2 getRptBCKD1_2(String listShopId, String productCode,
			Date fromDate, Date toDate) throws BusinessException {
		// TODO Auto-generated method stub
		try {
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add("shopId");
			inParams.add(listShopId);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("productCode");
			inParams.add(productCode);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("fDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("tDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptKDAllowSubCattVO> lstResultDetail = repo.getListByNamedQuery(RptKDAllowSubCattVO.class,"PKG_CRM_REPORT.KD1_2_REPORT", inParams);
			
			
			List<RptBCKD1_2_Mien> listMien = new ArrayList<RptBCKD1_2_Mien>(); 
			List<RptBCKD1_2_Vung> listVung = new ArrayList<RptBCKD1_2_Vung>();
			
			
			//map list detail qua list vung
			Map<String, RptBCKD1_2_Vung> mapVung= new HashMap<String, RptBCKD1_2_Vung>();
			List<String> codeVung = new ArrayList<String>();
			if (lstResultDetail != null && lstResultDetail.size() > 0) {
				for (RptKDAllowSubCattVO child : lstResultDetail) {
					if (mapVung.get(child.getSuperShopCode()) != null) {
						RptBCKD1_2_Vung rpt = mapVung.get(child.getSuperShopCode());
						rpt.getListItem().add(child);
					} else {
						RptBCKD1_2_Vung rpt = new RptBCKD1_2_Vung();
						rpt.setListItem(new ArrayList<RptKDAllowSubCattVO>());
						rpt.getListItem().add(child);
						mapVung.put(child.getSuperShopCode(), rpt);
						codeVung.add(child.getSuperShopCode());
					}
				}
				for (String code : codeVung) {
					listVung.add(mapVung.get(code));
				}
			}

			for (RptBCKD1_2_Vung res : listVung) {
				List<RptKDAllowSubCattVO> lst = res.getListItem();
				if (lst != null && lst.size() > 0) {
					res.setSuperShopCode(lst.get(0).getSuperShopCode());
					res.setParentSuperShopCode(lst.get(0).getParentSuperShopCode());
					
					Integer sumCustomer = 0;
					//Float sumAmount = 0F;
					BigDecimal sumAmount = BigDecimal.ZERO;
					for (RptKDAllowSubCattVO child : lst) {
						if (child.getSumCustomer() != null) {
							sumCustomer += child.getSumCustomer();
						}
						
						if (child.getSumAmount() != null) {
							//sumAmount += child.getSumAmount();
							sumAmount = sumAmount.add(child.getSumAmount());
						}
					}
					res.setSumCustomerVung(sumCustomer);
					res.setSumAmountVung(sumAmount);
				}
			}
			
			
			//map list vung qua list mien
			Map<String, RptBCKD1_2_Mien> mapMien = new HashMap<String, RptBCKD1_2_Mien>();
			List<String> codeMien = new ArrayList<String>();
			
			if (listVung != null && listVung.size() > 0) {
				for (RptBCKD1_2_Vung child : listVung) {
					if (mapMien.get(child.getParentSuperShopCode()) != null) {
						RptBCKD1_2_Mien rpt = mapMien.get(child.getParentSuperShopCode());
						rpt.getListVung().add(child);
					} else {
						RptBCKD1_2_Mien rpt = new RptBCKD1_2_Mien();
						rpt.setListVung(new ArrayList<RptBCKD1_2_Vung>());
						rpt.getListVung().add(child);
						mapMien.put(child.getParentSuperShopCode(), rpt);
						codeMien.add(child.getParentSuperShopCode());
					}
				}
				for (String code : codeMien) {
					listMien.add(mapMien.get(code));
				}
			}

			for (RptBCKD1_2_Mien res : listMien) {
				List<RptBCKD1_2_Vung> lst = res.getListVung();
				if (lst != null && lst.size() > 0) {
					res.setParentSuperShopCode(lst.get(0).getParentSuperShopCode());
					
					Integer sumCustomer = 0;
					//Float sumAmount = 0F;
					BigDecimal sumAmount = BigDecimal.ZERO;
					for (RptBCKD1_2_Vung child : lst) {
						if (child.getSumCustomerVung() != null) {
							sumCustomer += child.getSumCustomerVung();
						}
						
						if (child.getSumAmountVung() != null) {
							//sumAmount += child.getSumAmountVung();
							sumAmount = sumAmount.add(child.getSumAmountVung());
						}
					}
					res.setSumCustomerMien(sumCustomer);
					res.setSumAmountMien(sumAmount);
				}
			}
			
			
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			
			Integer tienDoChuan = (soNgayThucHien * 100)/soNgayBanHang;
			RptBCKD1_2 res = new RptBCKD1_2();
			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(listMien);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.core.report.CrmReportMgr#getRptBCKD17(java.util.Date, java.util.Date)
	 */
	@Override
	public List<RptBCKD17VO> getRptBCKD17(Date fromDate, Date toDate)
			throws BusinessException {
		try {
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptBCKD17VO> lstResult = repo.getListByNamedQuery(
					RptBCKD17VO.class, "PKG_CRM_REPORT.KD17_REPORT",
					inParams);
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public RptBCKD11_2VO getRptBCKD11_2(Date fromDate, Date toDate)
			throws BusinessException {
		try {
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);

			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);

			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			float tienDoChuan = (float) soNgayThucHien / soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);
			RptBCKD11_2VO res = new RptBCKD11_2VO();
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptBCKD11_2CTVO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD11_2CTVO.class, "PKG_CRM_REPORT.KD11_2_PRO",
					inParams);
			res.setNgayBatDau(fromDate);
			res.setNgayKetThuc(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public RptBCKD12VO getRptBCKD12(Long shopId, List<Long> listStaffId, Date toDate)
			throws BusinessException {
		try {
			if(null == toDate){
				throw new IllegalArgumentException(ExceptionCode.REPORT_TO_DATE_IS_NULL);
			}
			
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(toDate);
			fromDate.set(Calendar.DATE, 1);
			
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate.getTime(), toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan,2);
			
			StringBuilder strStaffId = new StringBuilder();
			for (int i = 0; i < listStaffId.size(); i++) {
				Long item = listStaffId.get(i);

				if (null != item) {
					if (i < listStaffId.size() - 1) {
						strStaffId.append(item).append(",");
					} else {
						strStaffId.append(item);
					}
				}
			}
			
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);
			
			inParams.add("staffId");
			inParams.add(StringUtility.isNullOrEmpty(strStaffId.toString()) ? null
					: strStaffId.toString());
			inParams.add(StandardBasicTypes.STRING);
			
			List<RptBCKD12DataVO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD12DataVO.class,
					"PKG_CRM_REPORT.PROC_KD12", inParams);
			
			RptBCKD12VO res = new RptBCKD12VO();
			
			res.setTuNgay(fromDate.getTime());
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public RptBCKD16VO getRptBCKD16(Date thangBC) throws BusinessException {
		try {
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(thangBC);
			fromDate.set(Calendar.DATE, 1);
			
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add("semonth");
			inParams.add(thangBC);
			inParams.add(StandardBasicTypes.DATE);
			
			List<RptBCKD16DataVO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD16DataVO.class,
					"PKG_CRM_REPORT.KD16_PRO", inParams);
			
			RptBCKD16VO res = new RptBCKD16VO();
			
			res.setThangBC(fromDate.getTime());
			res.setLstDetail(lstResultDetail);
			
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	@Override
	public RptBCKD11 getRptBCKD11(Long displayProId, Date fromDate, Date toDate)
			throws BusinessException {
		try {
			
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan,2);
			
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add("displayProId");
			inParams.add(displayProId);
			inParams.add(StandardBasicTypes.LONG);
			
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			
			List<RptBCKD11VO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD11VO.class,
					"PKG_CRM_REPORT.kd11_pro", inParams);
			
			RptBCKD11 res = new RptBCKD11();
			
			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public List<RptBCKD13VO> getRptBCKD13(Long shopId, String custCode, Date fromMonth, Date toMonth)
			throws BusinessException {
		try {
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);
			
			inParams.add("custCode");
			inParams.add(custCode);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("fromMonth");
			inParams.add(fromMonth);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("toMonth");
			inParams.add(toMonth);
			inParams.add(StandardBasicTypes.DATE);
			
			List<RptBCKD13VO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD13VO.class,
					"PKG_CRM_REPORT.proc_kd13", inParams);
			
			return lstResultDetail;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptKD_16_1VO getRptBCKD16_1(Date fromDate, Date toDate)
			throws BusinessException {
		try {
			
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan,2);
			
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			
			List<RptKD_16_1_CTVO> lstResultDetail = repo.getListByNamedQuery(
					RptKD_16_1_CTVO.class,
					"PKG_CRM_REPORT.kd16_1_pro", inParams);
			
			RptKD_16_1VO res = new RptKD_16_1VO();
			
			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * Bao cao KD19.1
	 * @author vuonghn
	 */
	@Override
	public RptBCKD19_1 getRptBCKD19_1(Long shopId,
		    Integer numTimeBadScore, Date fromDate, Date toDate)
			throws BusinessException {
		try {
			
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan,2);
			
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add(2);
			inParams.add(shopId);
			inParams.add(Types.NUMERIC);
			
			inParams.add(3);
			inParams.add(new java.sql.Date(fromDate.getTime()));
			inParams.add(Types.DATE);
			
			inParams.add(4);
			inParams.add(new java.sql.Date(toDate.getTime()));
			inParams.add(Types.DATE);
			
			inParams.add(5);
			inParams.add(numTimeBadScore);
			inParams.add(Types.NUMERIC);
			
			StringBuilder sql = new StringBuilder();
			//sql.append("call vuonghn.kd19_1_report(?,?,?,?,?)");
			sql.append("call pkg_shop_report.kd19_1_report(?,?,?,?,?)");
			List<RptBCKD19_1VO> lstResultDetail = repo.getListByQueryDynamicFromPackage(RptBCKD19_1VO.class, sql.toString(), inParams, null);
			
			RptBCKD19_1 res = new RptBCKD19_1();
			
			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptBCKD10_3 getRptBCKD10_3(Long displayProgramId, Long subCatId, 
			List<Long> listShopId, Date toDate)
			throws BusinessException {
		try {
			
			String strIdNpp = this.listToString(listShopId);
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(toDate);
			fromDate.set(Calendar.DATE, 1);
			
			List<Object> inParams = new ArrayList<Object>();
			//displayProgramId IN NUMBER, subCatId IN NUMBER, listShopId in STRING, strTungay IN STRING, strDenngay IN STRING
			inParams.add(2);
			inParams.add(displayProgramId);
			inParams.add(java.sql.Types.NUMERIC);
			
			inParams.add(3);
			inParams.add(subCatId);
			inParams.add(java.sql.Types.NUMERIC);
			
			inParams.add(4);
			inParams.add(strIdNpp);
			inParams.add(java.sql.Types.VARCHAR);
			
			inParams.add(5);
			inParams.add(df.format(fromDate.getTime()));
			inParams.add(java.sql.Types.VARCHAR);
			
			inParams.add(6);
			inParams.add(df.format(toDate.getTime()));
			inParams.add(java.sql.Types.VARCHAR);
			
			/*List<RptBCKD10_3VO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD10_3VO.class,
					"PKG_CRM_REPORT.KD10_3_REPORT", inParams);*/
			
			String sql = "{call PKG_SHOP_REPORT.KD10_3_REPORT(?, ?, ?, ?, ?, ?)}";
			
			List<RptBCKD10_3VO> lstResultDetail = repo.getListByQueryDynamicFromPackage(RptBCKD10_3VO.class, sql, inParams, null);
			
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate.getTime(), toDate);
			
			int soNgayNghiLe = saleDayMgr.getNumOfHoliday(fromDate.getTime(), toDate);
			
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			float tienDoChuan = (float)soNgayThucHien/soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);

			RptBCKD10_3 res = new RptBCKD10_3();
			res.setTuNgay(fromDate.getTime());
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			res.setSoNgayNghiLe(soNgayNghiLe);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public RptKD10_1VO getRptBCKD10_1(Long productInfoId, Date toDate)
			throws BusinessException {
		try {
			if (productInfoId == null) {
				throw new IllegalArgumentException(
						ExceptionCode.REPORT_KD10_2_PRODUCT_INFO_CODE_IS_NULL);
			}
			
			ProductInfo pi = productInfoDAO.getProductInfoById(productInfoId);

			if (pi == null) {
				return new RptKD10_1VO();
			}
			
			Calendar calFrom = Calendar.getInstance();
			calFrom.setTime(toDate);
			calFrom.set(Calendar.DATE, 1);
			Date fromDate = calFrom.getTime();

			List<Object> inParams = new ArrayList<Object>();

			inParams.add("braneId");
			inParams.add(pi.getId());
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("tungay");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("denngay");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptKD10_1CTVO> lstResultDetail = repo.getListByNamedQuery(
					RptKD10_1CTVO.class, "PKG_CRM_REPORT.KD10_1_REPORT",
					inParams);
			
			for(RptKD10_1CTVO vo : lstResultDetail) {
				vo.safeSetNull();
			}

			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);

			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);

			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}

			float tienDoChuan = (float) soNgayThucHien / soNgayBanHang;

			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);

			RptKD10_1VO res = new RptKD10_1VO();

			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			res.setCategoryName(pi.getProductInfoName());

			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		} catch (IllegalArgumentException e) {
			throw new BusinessException(e);
		} catch (IllegalAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptBCKD18VO getRptBCKD18(Date fromMonth, Date toMonth)
			throws BusinessException {
		try {
			RptBCKD18VO res = new RptBCKD18VO();
			Calendar calFrom = Calendar.getInstance();
			calFrom.setTime(fromMonth);
			calFrom.set(Calendar.DATE, calFrom.getActualMaximum(Calendar.DAY_OF_MONTH));
			
			Calendar calTo = Calendar.getInstance();
			calTo.setTime(toMonth);
			calTo.set(Calendar.DATE, calTo.getActualMaximum(Calendar.DAY_OF_MONTH));
			
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add("fromMonth");
			inParams.add(calFrom.getTime());
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("toMonth");
			inParams.add(calTo.getTime());
			inParams.add(StandardBasicTypes.DATE);
			
			List<RptBCKD18CTVO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD18CTVO.class,
					"PKG_CRM_REPORT.PROC_KD18", inParams);
			res.setLstDetail(lstResultDetail);
			res.setFromMonth(fromMonth);
			res.setFromMonth(toMonth);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.core.report.CrmReportMgr#getRptBCKD7(java.lang.String, java.util.Date, java.util.Date)
	 */
	@Override
	public RptBCKD7 getRptBCKD7(String maMien, Date fromDate, Date toDate)
			throws BusinessException {
		// TODO Auto-generated method stub
		try {
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);

			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);

			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			float tienDoChuan = (float) soNgayThucHien / soNgayBanHang;
			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);
			
			//tiendochuancuanam
			int soNgayBanHangCuaNamDenHienTai = saleDayDAO.getSaleDayFromStartYearToDate(toDate);
			
			Integer soNgayBanHangCuaCaNam = saleDayDAO.getSumSaleDateOfYear(toDate);
			
			if (soNgayBanHangCuaCaNam == null) {
				throw new IllegalArgumentException("soNgayBanHangCuaCaNam is null");
			}
			
			float tienDoChuanCuaNam = (float) soNgayBanHangCuaNamDenHienTai / soNgayBanHangCuaCaNam;
			tienDoChuanCuaNam = StringUtility.roundFloatBottom(tienDoChuanCuaNam, 2);
			
			RptBCKD7 res = new RptBCKD7();
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("mien_var");
			inParams.add(maMien);
			inParams.add(StandardBasicTypes.STRING);
			inParams.add("tungay");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("denngay");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptBCKD7VO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD7VO.class, "PKG_CRM_REPORT.KD7_REPORT", inParams);
			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			res.setTienDoChuanCuaNam(tienDoChuanCuaNam);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptBCKD10_2VO getRptBCKD10_2(String productInfoCode, Date fromDate,
			Date toDate) throws BusinessException {
		try {
			if (StringUtility.isNullOrEmpty(productInfoCode)) {
				throw new IllegalArgumentException(
						ExceptionCode.REPORT_KD10_2_PRODUCT_INFO_CODE_IS_NULL);
			}
			
			ProductInfo pi = productInfoDAO.getProductInfoByCode(productInfoCode, ProductType.SUB_CAT, null,null);

			if (pi == null) {
				return new RptBCKD10_2VO();
			}

			List<Object> inParams = new ArrayList<Object>();

			inParams.add("braneId");
			inParams.add(pi.getId());
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptBCKD10_2DataVO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD10_2DataVO.class, "PKG_CRM_REPORT.KD10_2_REPORT",
					inParams);

			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);

			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);

			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}

			float tienDoChuan = (float) soNgayThucHien / soNgayBanHang;

			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);

			RptBCKD10_2VO res = new RptBCKD10_2VO();

			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			res.setProductInfo(pi.getProductInfoName());

			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @return list RptBCKD10_2DataVO
	 * @author hunglm16
	 * @since February 10, 2014
	 * @description report CRM/ KD10.2
	 * */
	@Override
	public List<RptBCKD10_2_DLCPPTSkusDNVO> getRptBCKD10_2_DLCPPTSkusDN(Long subCat, Date fromDate, Date toDate) throws BusinessException {
		//KD10.2- Diem ban le can phan phoi theo SKUs de nghi
		try {
			if (subCat == null) {
				return new ArrayList<RptBCKD10_2_DLCPPTSkusDNVO>();
			}
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_CRM_REPORT.KD10_2_REPORT(?, :subCat, :fromDate, :toDate)");
//			sql.append("call HUNGLM16.KD10_2_REPORT(?, :braneId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(subCat);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(4);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			return repo.getListByQueryDynamicFromPackage(RptBCKD10_2_DLCPPTSkusDNVO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public RptBCKD19VO getRptBCKD19(Long shopId, Integer numTimeBadScore,
			Date fromDate, Date toDate) throws BusinessException {
		if (numTimeBadScore == null) {
			throw new IllegalArgumentException(ExceptionCode.REPORT_KD19_TIME_BAD_SCORE_ID_NULL);
		}
		
		if (fromDate == null) {
			throw new IllegalArgumentException(ExceptionCode.REPORT_FROM_DATE_IS_NULL);
		}
		
		if (toDate == null) {
			throw new IllegalArgumentException(ExceptionCode.REPORT_TO_DATE_IS_NULL);
		}
		
		if (fromDate.after(toDate)) {
			throw new IllegalArgumentException(ExceptionCode.REPORT_FROM_DATE_AFTER_TO_DATE);
		}
		
		try {
			int lowScrore = 3;
			List<Object> inParams = new ArrayList<Object>();

			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("numSeqBadScore");
			inParams.add(numTimeBadScore);
			inParams.add(StandardBasicTypes.INTEGER);

			List<RptBCKD19DataVO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD19DataVO.class, "PKG_CRM_REPORT.PROC_KD19",
					inParams);
			
			if(null != lstResultDetail && lstResultDetail.size() > 0){
				List<RptBCKD19DataVO> lstTemp = new ArrayList<RptBCKD19DataVO>();
				RptBCKD19DataVO objectFirst = null;
				int count = 0;
				Boolean isInvalidate = false;
				
				for(int i = 0; i< lstResultDetail.size(); i++){
					if(i == 0){
						objectFirst = lstResultDetail.get(i);
						lstTemp.add(objectFirst);
						if(objectFirst.getDiem() != null && objectFirst.getDiem() < lowScrore){
							++count;
						}
					} else{
						if(lstResultDetail.get(i).getMaNVGS().equalsIgnoreCase(objectFirst.getMaNVGS())&&
								lstResultDetail.get(i).getMaTBHV().equalsIgnoreCase(objectFirst.getMaTBHV())){
							lstTemp.add(lstResultDetail.get(i));
							
							if (!isInvalidate.booleanValue()) {
								if (null != lstResultDetail.get(i).getDiem()) {
									if (lstResultDetail.get(i).getDiem() < lowScrore) {
										++count;
										
										if(count == numTimeBadScore){
											isInvalidate = true;
										}
									} else {
										count = 0;
										isInvalidate = false;
										objectFirst = lstResultDetail.get(i);
									}
								}
							}
						} else {
							for (RptBCKD19DataVO item : lstTemp) {
								item.setIsInvalidate(isInvalidate);
							}

							count = 0;
							isInvalidate = false;
							objectFirst = lstResultDetail.get(i);

							lstTemp.clear();
						}
					}
				}
				
				for (RptBCKD19DataVO item : lstTemp) {
					item.setIsInvalidate(isInvalidate);
				}
			}

			RptBCKD19VO res = new RptBCKD19VO();

			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);

			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptBCKD14VO> getRptBCKD14(Long shopId, Date fromMonth,
			Date toMonth) throws BusinessException {
		List<RptBCKD14VO> lstData = new ArrayList<RptBCKD14VO>();
		try {
			List<Object> inParams = new ArrayList<Object>();

			inParams.add("shopId");
			inParams.add(shopId.toString());
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromMonth");
			inParams.add(fromMonth);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toMonth");
			inParams.add(toMonth);
			inParams.add(StandardBasicTypes.DATE);
			
			List<RptBCKD14DetailVO> lstDetailVO = repo.getListByNamedQuery(
					RptBCKD14DetailVO.class, "PKG_CRM_REPORT.KD14_REPORT",
					inParams);
			
			RptBCKD14VO vo = null;
			int numMonth = DateUtility.getMonth(toMonth) - DateUtility.getMonth(fromMonth);
			int indexDetailVO = 0;
			if(lstDetailVO != null && lstDetailVO.size() > 0){
				for(int i = 0; i< lstDetailVO.size(); i++){
					RptBCKD14DetailVO detailVo = lstDetailVO.get(i);
					detailVo.safeSetNull();
					
					if(indexDetailVO == 0){
						vo = new RptBCKD14VO();
						List<RptBCKD14DetailVO> lstDetail = new ArrayList<RptBCKD14DetailVO>();
						vo.setLstDetail(lstDetail);
						vo.setMaMien(detailVo.getMaMien());
						vo.setMaVung(detailVo.getMaVung());
						vo.setTbhv(detailVo.getTbhv());
						vo.setMaNPP(detailVo.getMaNPP());
						vo.setTenNPP(detailVo.getTenNPP());
						vo.setDiemLeT12(detailVo.getDiemLeT12());
						indexDetailVO++;
						lstData.add(vo);
					}else{
						vo.getLstDetail().add(detailVo);
						if(vo.getLstDetail().size() == numMonth){
							indexDetailVO = 0;
						}
					}
				}					
			}
			
			
			
			return lstData;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public RptBCKD21 getRptBCKD21(Long shopId, Date toDate)
			throws BusinessException {
		try {
			Calendar calFrom = Calendar.getInstance();
			calFrom.setTime(toDate);
			calFrom.set(Calendar.DATE, 1);
			Date fromDate = calFrom.getTime();

			List<Object> inParams = new ArrayList<Object>();

			inParams.add("shopId");
			inParams.add(shopId.toString());
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptBCKD21VO> lstResultDetail = repo.getListByNamedQuery(
					RptBCKD21VO.class, "PKG_CRM_REPORT.KD21_REPORT",
					inParams);

			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);

			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr
					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);

			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}

			float tienDoChuan = (float) soNgayThucHien / soNgayBanHang;

			tienDoChuan = StringUtility.roundFloatBottom(tienDoChuan, 2);

			RptBCKD21 res = new RptBCKD21();

			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(lstResultDetail);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);

			return res;
			
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	@Override
	public RptBCKD15VO getRptBCKD15(Date currDate, Integer numMonth)
			throws BusinessException {
		if (null == currDate) {
			throw new IllegalArgumentException(ExceptionCode.REPORT_KD15_DATE_IS_NULL);
		}

		if (null == numMonth) {
			throw new IllegalArgumentException(ExceptionCode.REPORT_KD15_NUMMONTH_IS_NULL);
		}

		if (1 > numMonth || 12 < numMonth) {
			throw new IllegalArgumentException(ExceptionCode.REPORT_KD15_NUMMONTH_INVALIDATE);
		}
		
		try {
			List<Object> inParams = new ArrayList<Object>();

			inParams.add("currDate");
			inParams.add(currDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("numMonth");
			inParams.add(numMonth);
			inParams.add(StandardBasicTypes.INTEGER);

			List<RptBCKD15DataVO> lstResult = repo.getListByNamedQuery(
					RptBCKD15DataVO.class, "PKG_CRM_REPORT.KD_15_REPORT",
					inParams);
			
			RptBCKD15VO result = new RptBCKD15VO();
			result.setLstDetail(lstResult);
			result.setSoThang(numMonth);
			result.setThangBC(currDate);

			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @author tungtt21
	 * Báo cáo KD1.3 Doanh số phân phối theo nhóm hàng
	 */
	@Override
	public RptBCKD1_2 getRptBCKD1_3(String listShopId, String productCode,
			Date fromDate, Date toDate) throws BusinessException {
		try {
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add("shopId");
			inParams.add(listShopId);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("productCode");
			inParams.add(productCode);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("fDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("tDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptKDAllowSubCattVO> lstResultDetail = repo.getListByNamedQuery(RptKDAllowSubCattVO.class,"PKG_CRM_REPORT.KD1_3_REPORT", inParams);
			
			
			List<RptBCKD1_2_Mien> listMien = new ArrayList<RptBCKD1_2_Mien>(); 
			List<RptBCKD1_2_Vung> listVung = new ArrayList<RptBCKD1_2_Vung>();
			
			
			//map list detail qua list vung
			Map<String, RptBCKD1_2_Vung> mapVung= new HashMap<String, RptBCKD1_2_Vung>();
			List<String> codeVung = new ArrayList<String>();
			if (lstResultDetail != null && lstResultDetail.size() > 0) {
				for (RptKDAllowSubCattVO child : lstResultDetail) {
					if (mapVung.get(child.getSuperShopCode()) != null) {
						RptBCKD1_2_Vung rpt = mapVung.get(child.getSuperShopCode());
						rpt.getListItem().add(child);
					} else {
						RptBCKD1_2_Vung rpt = new RptBCKD1_2_Vung();
						rpt.setListItem(new ArrayList<RptKDAllowSubCattVO>());
						rpt.getListItem().add(child);
						mapVung.put(child.getSuperShopCode(), rpt);
						codeVung.add(child.getSuperShopCode());
					}
				}
				for (String code : codeVung) {
					listVung.add(mapVung.get(code));
				}
			}

			for (RptBCKD1_2_Vung res : listVung) {
				List<RptKDAllowSubCattVO> lst = res.getListItem();
				if (lst != null && lst.size() > 0) {
					res.setSuperShopCode(lst.get(0).getSuperShopCode());
					res.setParentSuperShopCode(lst.get(0).getParentSuperShopCode());
					
					Integer sumCustomer = 0;
					//Float sumAmount = 0F;
					BigDecimal sumAmount = BigDecimal.ZERO;
					for (RptKDAllowSubCattVO child : lst) {
						if (child.getSumCustomer() != null) {
							sumCustomer += child.getSumCustomer();
						}
						
						if (child.getSumAmount() != null) {
							//sumAmount += child.getSumAmount();
							sumAmount = sumAmount.add(child.getSumAmount());
						}
					}
					res.setSumCustomerVung(sumCustomer);
					res.setSumAmountVung(sumAmount);
				}
			}
			
			
			//map list vung qua list mien
			Map<String, RptBCKD1_2_Mien> mapMien = new HashMap<String, RptBCKD1_2_Mien>();
			List<String> codeMien = new ArrayList<String>();
			
			if (listVung != null && listVung.size() > 0) {
				for (RptBCKD1_2_Vung child : listVung) {
					if (mapMien.get(child.getParentSuperShopCode()) != null) {
						RptBCKD1_2_Mien rpt = mapMien.get(child.getParentSuperShopCode());
						rpt.getListVung().add(child);
					} else {
						RptBCKD1_2_Mien rpt = new RptBCKD1_2_Mien();
						rpt.setListVung(new ArrayList<RptBCKD1_2_Vung>());
						rpt.getListVung().add(child);
						mapMien.put(child.getParentSuperShopCode(), rpt);
						codeMien.add(child.getParentSuperShopCode());
					}
				}
				for (String code : codeMien) {
					listMien.add(mapMien.get(code));
				}
			}

			for (RptBCKD1_2_Mien res : listMien) {
				List<RptBCKD1_2_Vung> lst = res.getListVung();
				if (lst != null && lst.size() > 0) {
					res.setParentSuperShopCode(lst.get(0).getParentSuperShopCode());
					
					Integer sumCustomer = 0;
					//Float sumAmount = 0F;
					BigDecimal sumAmount = BigDecimal.ZERO;
					for (RptBCKD1_2_Vung child : lst) {
						if (child.getSumCustomerVung() != null) {
							sumCustomer += child.getSumCustomerVung();
						}
						
						if (child.getSumAmountVung() != null) {
							//sumAmount += child.getSumAmountVung();
							sumAmount = sumAmount.add(child.getSumAmountVung());
						}
					}
					res.setSumCustomerMien(sumCustomer);
					res.setSumAmountMien(sumAmount);
				}
			}
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			Integer tienDoChuan = (soNgayThucHien * 100)/soNgayBanHang;
			RptBCKD1_2 res = new RptBCKD1_2();
			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(listMien);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	
	/**
	 * KD1 - Doanh so ban hang cua nhan vien ban hang cua nhan vien ban hang theo SKUs
	 * @author hunglm16
	 * @since January 23,2014
	 * */
	@Override
	public List<RptKD1_VO> exportRptBCKD1(Long shopId, String lstProductCode, Date fDate, Date tDate) throws BusinessException {
		try {
			//DS3 - Bao cao thoi gian ban hang hieu qua cua nhan vien ban hang
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_CRM_REPORT.PROC_KD1(?, :shopId, :lstProductCode, :fDate, :tDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(lstProductCode);
			params.add(java.sql.Types.VARCHAR);
			params.add(4);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(5);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			return repo.getListByQueryDynamicFromPackage(RptKD1_VO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * KD3 - Doanh so ban hang cua NVBH theo nhom
	 * @author hunglm16
	 * @since January 23,2014
	 * */
	@Override
	public List<RptKD3_VO> exportRptBCKD3(Long shopId, String lstProductCode, Date fDate, Date tDate) throws BusinessException {
		try {
			//DS3 - Bao cao thoi gian ban hang hieu qua cua nhan vien ban hang
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_CRM_REPORT.PROC_KD3(?, :shopId, :fDate, :tDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(4);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			return repo.getListByQueryDynamicFromPackage(RptKD3_VO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * General
	 * @author hunglm16
	 * @since January 23,2014
	 * */
	@Override
	public RptKD_General_VO generalKD(Date fromDate, Date toDate) throws BusinessException{
		RptKD_General_VO kq = new RptKD_General_VO();
		try{
			Double soNgayBanHang = 0.0;
			Double soNgayThucHien = 0.0;
			Double soNgayNghiLe = 0.0;
			Double tienDoChuan = 0.0;
			Double tienThucHien = 0.0;
			// So ngay ban hang trong thang
			if(toDate!=null){
				soNgayBanHang = Double.valueOf(saleDayDAO.getSaleDayByDate(toDate));
			}
			if(fromDate!=null && toDate!=null){
				// So ngay thuc hien
				soNgayThucHien = Double.valueOf(saleDayMgr.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate));
				//so ngay nghi le
				List<ExceptionDay> lstExcDay =  exceptionDayDAO.getLstExceptionDay(fromDate, toDate);
				if(lstExcDay!=null){
					soNgayNghiLe = Double.valueOf(lstExcDay.size());
				}
			}
			
			//Tien do chuan
			if(soNgayBanHang>0 || soNgayBanHang<0){
				tienDoChuan = soNgayThucHien/soNgayBanHang;
			}
			tienThucHien = tienDoChuan;
			
			kq.setSoNgayBanHang(soNgayBanHang);
			kq.setSoNgayThucHien(soNgayThucHien);
			kq.setTienDoChuan(tienDoChuan);
			kq.setTienThucHien(tienThucHien);
			kq.setSoNgayNghiLe(soNgayNghiLe);
		}catch(Exception e){
			throw new BusinessException(e);
		}
		return kq;
	}

	@Override
	public RptBCKD1_2 getRptBCKD1_1(String listShopId, String productCode,
			Date fromDate, Date toDate) throws BusinessException {
		try {
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add("shopId");
			inParams.add(listShopId);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("productCode");
			inParams.add(productCode);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("fDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("tDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptKDAllowSubCattVO> lstResultDetail = repo.getListByNamedQuery(RptKDAllowSubCattVO.class,"PKG_CRM_REPORT.KD1_1_REPORT", inParams);
			
			
			List<RptBCKD1_2_Mien> listMien = new ArrayList<RptBCKD1_2_Mien>(); 
			List<RptBCKD1_2_Vung> listVung = new ArrayList<RptBCKD1_2_Vung>();
			
			
			//map list detail qua list vung
			Map<String, RptBCKD1_2_Vung> mapVung= new HashMap<String, RptBCKD1_2_Vung>();
			List<String> codeVung = new ArrayList<String>();
			if (lstResultDetail != null && lstResultDetail.size() > 0) {
				for (RptKDAllowSubCattVO child : lstResultDetail) {
					if (mapVung.get(child.getSuperShopCode()) != null) {
						RptBCKD1_2_Vung rpt = mapVung.get(child.getSuperShopCode());
						rpt.getListItem().add(child);
					} else {
						RptBCKD1_2_Vung rpt = new RptBCKD1_2_Vung();
						rpt.setListItem(new ArrayList<RptKDAllowSubCattVO>());
						rpt.getListItem().add(child);
						mapVung.put(child.getSuperShopCode(), rpt);
						codeVung.add(child.getSuperShopCode());
					}
				}
				for (String code : codeVung) {
					listVung.add(mapVung.get(code));
				}
			}

			for (RptBCKD1_2_Vung res : listVung) {
				List<RptKDAllowSubCattVO> lst = res.getListItem();
				if (lst != null && lst.size() > 0) {
					res.setSuperShopCode(lst.get(0).getSuperShopCode());
					res.setParentSuperShopCode(lst.get(0).getParentSuperShopCode());
					
					Integer sumCustomer = 0;
					//Float sumAmount = 0F;
					BigDecimal sumAmount = BigDecimal.ZERO;
					for (RptKDAllowSubCattVO child : lst) {
						if (child.getSumCustomer() != null) {
							sumCustomer += child.getSumCustomer();
						}
						
						if (child.getSumAmount() != null) {
							//sumAmount += child.getSumAmount();
							sumAmount = sumAmount.add(child.getSumAmount());
						}
					}
					res.setSumCustomerVung(sumCustomer);
					res.setSumAmountVung(sumAmount);
				}
			}
			
			
			//map list vung qua list mien
			Map<String, RptBCKD1_2_Mien> mapMien = new HashMap<String, RptBCKD1_2_Mien>();
			List<String> codeMien = new ArrayList<String>();
			
			if (listVung != null && listVung.size() > 0) {
				for (RptBCKD1_2_Vung child : listVung) {
					if (mapMien.get(child.getParentSuperShopCode()) != null) {
						RptBCKD1_2_Mien rpt = mapMien.get(child.getParentSuperShopCode());
						rpt.getListVung().add(child);
					} else {
						RptBCKD1_2_Mien rpt = new RptBCKD1_2_Mien();
						rpt.setListVung(new ArrayList<RptBCKD1_2_Vung>());
						rpt.getListVung().add(child);
						mapMien.put(child.getParentSuperShopCode(), rpt);
						codeMien.add(child.getParentSuperShopCode());
					}
				}
				for (String code : codeMien) {
					listMien.add(mapMien.get(code));
				}
			}

			for (RptBCKD1_2_Mien res : listMien) {
				List<RptBCKD1_2_Vung> lst = res.getListVung();
				if (lst != null && lst.size() > 0) {
					res.setParentSuperShopCode(lst.get(0).getParentSuperShopCode());
					
					Integer sumCustomer = 0;
					//Float sumAmount = 0F;
					BigDecimal sumAmount = BigDecimal.ZERO;
					for (RptBCKD1_2_Vung child : lst) {
						if (child.getSumCustomerVung() != null) {
							sumCustomer += child.getSumCustomerVung();
						}
						
						if (child.getSumAmountVung() != null) {
							//sumAmount += child.getSumAmountVung();
							sumAmount = sumAmount.add(child.getSumAmountVung());
						}
					}
					res.setSumCustomerMien(sumCustomer);
					res.setSumAmountMien(sumAmount);
				}
			}
			
			
			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(toDate);
			// So ngay ban hang da qua
			int soNgayThucHien = saleDayMgr.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			
			if (soNgayBanHang == null) {
				throw new IllegalArgumentException("soNgayBanHang is null");
			}
			
			Integer tienDoChuan = (soNgayThucHien * 100)/soNgayBanHang;
			RptBCKD1_2 res = new RptBCKD1_2();
			res.setTuNgay(fromDate);
			res.setDenNgay(toDate);
			res.setLstDetail(listMien);
			res.setSoNgayBanHang(soNgayBanHang);
			res.setSoNgayThucHien(soNgayThucHien);
			res.setTienDoChuan(tienDoChuan);
			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	/**
	 * Bao cao KD9
	 * @author tungtt21
	 */
	@Override
	public List<RptKD9> getRptKD9(Long shopId, String productCode,
			Date tmpDate, Date fDate, Date aDate, Date tDate)
			throws BusinessException {
		try {
			List<Object> inParams = new ArrayList<Object>();
			
			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);
			
			inParams.add("productCode");
			inParams.add(productCode);
			inParams.add(StandardBasicTypes.STRING);
			
			inParams.add("tmpDate");
			inParams.add(tmpDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("fDate");
			inParams.add(fDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("aDate");
			inParams.add(aDate);
			inParams.add(StandardBasicTypes.DATE);
			
			inParams.add("tDate");
			inParams.add(tDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptKD9_Detail> lstResultDetail = repo.getListByNamedQuery(RptKD9_Detail.class,"PKG_CRM_REPORT.RptKD9", inParams);
	
			List<RptKD9> listMien = new ArrayList<RptKD9>(); 
			List<RptKD9_Vung> listVung = new ArrayList<RptKD9_Vung>();
			
			BigDecimal num1, num2, num3, num4, num5, num6, num7, num8; 
			num1 = num2 = num3 =  num4 = num5 = num6 = num7 = num8 = BigDecimal.ZERO;
			
			//map list detail qua list vung
			Map<String, RptKD9_Vung> mapVung= new HashMap<String, RptKD9_Vung>();
			List<String> codeVung = new ArrayList<String>();
			
			if (lstResultDetail != null && lstResultDetail.size() > 0) {
				for (RptKD9_Detail child : lstResultDetail) {
					if (mapVung.get(child.getVung()) != null) {
						RptKD9_Vung rpt = mapVung.get(child.getVung());
						rpt.getListDetail().add(child);
					} else {
						RptKD9_Vung rpt = new RptKD9_Vung();
						rpt.setListDetail(new ArrayList<RptKD9_Detail>());
						rpt.getListDetail().add(child);
						rpt.setVung(child.getVung());
						rpt.setMien(child.getMien());
						
						mapVung.put(child.getVung(), rpt);
						codeVung.add(child.getVung());
					}
				}
				for (String code : codeVung) {
					listVung.add(mapVung.get(code));
				}
			}
			
			for(RptKD9_Vung res : listVung){
				List<RptKD9_Detail> lst = res.getListDetail();
				
				if (lst != null && lst.size() > 0) {
					for (RptKD9_Detail child : lst) {
						if(null != child.getSoLuongConLai()){
							num1 = num1.add(child.getSoLuongConLai());
						}
						if(null != child.getGiaTriBan()){
							num2 = num2.add(child.getGiaTriBan());
						}
						if(null != child.getTonDauKy1()){
							num3 = num3.add(child.getTonDauKy1());
						}
						if(null != child.getTotalNhap()){
							num4 = num4.add(child.getTotalNhap());
						}
						if(null != child.getTotalXuat()){
							num5 = num5.add(child.getTotalXuat());
						}
						if(null != child.getCuoiKy1()){
							num6 = num6.add(child.getCuoiKy1());
						}
						if(null != child.getGiaTriCKy1()){
							num7 = num7.add(child.getGiaTriCKy1());
						}
						if(null != child.getSoLuongBan()){
							num8 = num8.add(child.getSoLuongBan());
						}
					}
					res.setSoLuongConLai(num1);
					res.setGiaTriBan(num2);
					res.setTonDauKy1(num3);
					res.setTotalNhap(num4);
					res.setTotalXuat(num5);
					res.setCuoiKy1(num6);
					res.setGiaTriCKy1(num7);
					res.setSoLuongBan(num8);

				}
			}
			
			//map list vung qua list mien
			Map<String, RptKD9> mapMien = new HashMap<String, RptKD9>();
			List<String> codeMien = new ArrayList<String>();
			num1 = num2 = num3 =  num4 = num5 = num6 = num7 = num8 = BigDecimal.ZERO;
			
			if (listVung != null && listVung.size() > 0) {
				for (RptKD9_Vung child : listVung) {
					if (mapMien.get(child.getMien()) != null) {
						RptKD9 rpt = mapMien.get(child.getMien());
						rpt.getListVung().add(child);
					} else {
						RptKD9 rpt = new RptKD9();
						rpt.setListVung(new ArrayList<RptKD9_Vung>());
						rpt.getListVung().add(child);
						rpt.setMien(child.getMien());
						
						mapMien.put(child.getMien(), rpt);
						codeMien.add(child.getMien());
					}
				}
				for (String code : codeMien) {
					listMien.add(mapMien.get(code));
				}
			}
			for(RptKD9 res : listMien){
				List<RptKD9_Vung> lst = res.getListVung();
				
				if (lst != null && lst.size() > 0) {
					for (RptKD9_Vung child : lst) {
						if(null != child.getSoLuongConLai()){
							num1 = num1.add(child.getSoLuongConLai());
						}
						if(null != child.getGiaTriBan()){
							num2 = num2.add(child.getGiaTriBan());
						}
						if(null != child.getTonDauKy1()){
							num3 = num3.add(child.getTonDauKy1());
						}
						if(null != child.getTotalNhap()){
							num4 = num4.add(child.getTotalNhap());
						}
						if(null != child.getTotalXuat()){
							num5 = num5.add(child.getTotalXuat());
						}
						if(null != child.getCuoiKy1()){
							num6 = num6.add(child.getCuoiKy1());
						}
						if(null != child.getGiaTriCKy1()){
							num7 = num7.add(child.getGiaTriCKy1());
						}
						if(null != child.getSoLuongBan()){
							num8 = num8.add(child.getSoLuongBan());
						}
					}
					res.setSoLuongConLai(num1);
					res.setGiaTriBan(num2);
					res.setTonDauKy1(num3);
					res.setTotalNhap(num4);
					res.setTotalXuat(num5);
					res.setCuoiKy1(num6);
					res.setGiaTriCKy1(num7);
					res.setSoLuongBan(num8);

				}
			}
			return listMien;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
	
	/**
	 * @return list rpt_HO_KD15VO
	 * @author hunglm16
	 * @since March 21, 2014
	 * @description report CRM/ KD115
	 * */
	@Override
	public List<rpt_HO_KD15VO> rpt_HO_KD15_SDBLCPSDDCT(Long shopId, Date fromDate, Date toDate, Integer numberMonth) throws BusinessException {
		//KD15- So diem ban le chua PSDS cua thang so voi 1,2,3...thang truoc
		try {
			if (shopId == null) {
				return new ArrayList<rpt_HO_KD15VO>();
			}
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_CRM_REPORT.KD15_SDBLCPSDDCT(?, :shopId,  :fromDate, :toDate, :numberMonth)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(4);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(5);
			params.add(numberMonth);
			params.add(java.sql.Types.NUMERIC);
			
			return repo.getListByQueryDynamicFromPackage(rpt_HO_KD15VO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}
}

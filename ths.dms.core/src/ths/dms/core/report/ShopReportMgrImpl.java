package ths.dms.core.report;

import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.InvoiceCustPayment;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.GS_1_2_VO;
import ths.dms.core.entities.vo.GS_1_4_VO;
import ths.dms.core.entities.vo.GS_4_2_VO;
import ths.dms.core.entities.vo.GS_KHTT;
import ths.dms.core.entities.vo.Rpt7_2_2_NVBHVO;
import ths.dms.core.entities.vo.Rpt7_2_2_NganhHangVO;
import ths.dms.core.entities.vo.Rpt7_2_2_SanPhamVO;
import ths.dms.core.entities.vo.RptCustomerVO;
import ths.dms.core.entities.vo.RptDSKHTMHCatAndListInfoVO;
import ths.dms.core.entities.vo.RptDSKHTMHCustomerAndListInfoVO;
import ths.dms.core.entities.vo.RptDSKHTMHRecordProductVO;
import ths.dms.core.entities.vo.RptDTBHTNTHVBHRecordOrderVO;
import ths.dms.core.entities.vo.RptDTBHTNTHVBHRecordOrder_7_2_2VO;
import ths.dms.core.entities.vo.RptDTBHTNTNVBHDateInfoVO;
import ths.dms.core.entities.vo.RptDTBHTNTNVBHDateInfo_7_2_2VO;
import ths.dms.core.entities.vo.RptDTBHTNTNVBHStaffInfoVO;
import ths.dms.core.entities.vo.RptDTBHTNTNVBHStaffInfo_7_2_2VO;
import ths.dms.core.entities.vo.RptPDHVNM_5_1VO;
import ths.dms.core.entities.vo.RptPDHVNM_5_1_CT_VO;
import ths.dms.core.entities.vo.RptTDBHTMHProductAndListInfoVO;
import ths.dms.core.entities.vo.RptTDBHTMHProductDateFollowStaffVO;
import ths.dms.core.entities.vo.RptTDBHTMHProductDateStaffFollowCustomerVO;
import ths.dms.core.entities.vo.RptTDBHTMHProductFollowDateVO;
import ths.dms.core.entities.vo.SaleOrderNumberVO;
import ths.dms.core.entities.vo.SaleOrderVO;
import ths.dms.core.entities.vo.TimeVisitCustomerVO;
import ths.core.entities.vo.rpt.*;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.business.SaleDayMgr;
import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.GroupUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.ActionLogDAO;
import ths.dms.core.dao.CommonDAO;
import ths.dms.core.dao.CustomerDAO;
import ths.dms.core.dao.CycleCountMapProductDAO;
import ths.dms.core.dao.CycleCountResultDAO;
import ths.dms.core.dao.DebitDAO;
import ths.dms.core.dao.ExceptionDayDAO;
import ths.dms.core.dao.InvoiceDAO;
import ths.dms.core.dao.PoAutoDetailDAO;
import ths.dms.core.dao.PoVnmDAO;
import ths.dms.core.dao.PoVnmDetailDAO;
import ths.dms.core.dao.ProductDAO;
import ths.dms.core.dao.SaleDayDAO;
import ths.dms.core.dao.SaleOrderDAO;
import ths.dms.core.dao.SaleOrderDetailDAO;
import ths.dms.core.dao.ShopDAO;
import ths.dms.core.dao.StaffDAO;
import ths.dms.core.dao.StockTotalDAO;
import ths.dms.core.dao.StockTransDAO;
import ths.dms.core.dao.StockTransDetailDAO;
import ths.dms.core.dao.repo.IRepository;

public class ShopReportMgrImpl implements ShopReportMgr {
	@Autowired
	private IRepository repo;

	@Autowired
	SaleDayMgr saleDayMgr;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	StaffDAO staffDAO;

	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	SaleDayDAO saleDayDAO;

	@Autowired
	SaleOrderDAO saleOrderDAO;

	@Autowired
	ProductDAO productDAO;

	@Autowired
	StockTotalDAO stockTotalDAO;

	@Autowired
	SaleOrderDetailDAO saleOrderDetailDAO;

	@Autowired
	PoVnmDAO poVnmDAO;

	// xuat nhap ton chi tiet
	@Autowired
	ShopDAO shopDAO;

	@Autowired
	PoVnmDetailDAO poVnmDetailDAO;

	@Autowired
	CycleCountResultDAO cycleCountResultDAO;

	@Autowired
	StockTransDetailDAO stockTransDetailDAO;

	@Autowired
	CycleCountMapProductDAO cycleCountMapProductDAO;

	@Autowired
	PoAutoDetailDAO poAutoDetailDAO;

	@Autowired
	InvoiceDAO invoiceDAO;

	@Autowired
	DebitDAO debitDAO;

	@Autowired
	ActionLogDAO actionLogDAO;

	@Autowired
	StockTransDAO stockTransDAO;

	@Autowired
	ExceptionDayDAO exceptionDayDAO;

	/**
	 * Bao cao xuat nhap ton chi tiet
	 */
	@Override
	public List<RptImExStDataVO> getXNTCTReport(Long shopId, Date fromDate, Date toDate) throws BusinessException {
		List<RptImExStDataVO> result = new ArrayList<RptImExStDataVO>();

		try {
			//
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			//List<RptImExStDataDetailVO> lstResult = repo.getListByNamedQuery(RptImExStDataDetailVO.class, "PKG_SHOP_REPORT.XNTCT_REPORT", inParams);
			List<RptImExStDataDetailVO> lstResult = repo.getListByNamedQuery(RptImExStDataDetailVO.class, "PKG_SHOP_REPORT.XNTCT_REPORT", inParams);
			RptImExStDataVO catDetail = null;
			int index = 1;
			for (RptImExStDataDetailVO child : lstResult) {

				child.initReportField();
				if (catDetail == null) {
					catDetail = new RptImExStDataVO();
					catDetail.setCatId(child.getCatId());
					catDetail.setCatName(child.getCatName());
					catDetail.setCatCode(child.getCatCode());
					child.setIndex(index);
					index++;
					catDetail.getListData().add(child);
					catDetail.setQuantityCloseSTBeginTotal(catDetail.getQuantityCloseSTBeginTotal().add(child.getQuantityCloseSTBeginTotal()));
					catDetail.setQuantityCloseSTEndTotal(catDetail.getQuantityCloseSTEndTotal().add(child.getQuantityCloseSTEndTotal()));
				} else {
					if (catDetail.getCatId().equals(child.getCatId())) {

						child.setIndex(index);
						index++;
						catDetail.getListData().add(child);
						catDetail.setQuantityCloseSTBeginTotal(catDetail.getQuantityCloseSTBeginTotal().add(child.getQuantityCloseSTBeginTotal()));
						catDetail.setQuantityCloseSTEndTotal(catDetail.getQuantityCloseSTEndTotal().add(child.getQuantityCloseSTEndTotal()));
					} else {
						index = 1;
						result.add(catDetail);
						catDetail = new RptImExStDataVO();
						catDetail.setCatId(child.getCatId());
						catDetail.setCatName(child.getCatName());
						catDetail.setCatCode(child.getCatCode());
						child.setIndex(index);
						index++;
						catDetail.setQuantityCloseSTBeginTotal(catDetail.getQuantityCloseSTBeginTotal().add(child.getQuantityCloseSTBeginTotal()));
						catDetail.setQuantityCloseSTEndTotal(catDetail.getQuantityCloseSTEndTotal().add(child.getQuantityCloseSTEndTotal()));
						catDetail.getListData().add(child);
					}
				}
			}
			if (catDetail != null) {
				result.add(catDetail);
			}
			//
			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

	}

	@Override
	public List<RptF1Lv01VO> getDataForF1Report(Long shopId, Date fromDate, Date toDate, Integer numSaleDayInMonth, Integer numSaleDay) throws BusinessException {
		try {
			int numExec = exceptionDayDAO.getNumExceptionDayF1(new Date(toDate.getTime() - 10 * 24 * 3600 * 1000), new Date(toDate.getTime() - 1 * 24 * 3600 * 1000));

			List<Object> inParams = new ArrayList<Object>();

			inParams.add("var_shopid");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);
			inParams.add("var_fromdate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("var_todate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("var_workdate");
			inParams.add(numSaleDayInMonth);
			inParams.add(StandardBasicTypes.INTEGER);
			inParams.add("var_realworkdate");
			inParams.add(numSaleDay);
			inParams.add(StandardBasicTypes.INTEGER);
			inParams.add("numExec");
			inParams.add(numExec);
			inParams.add(StandardBasicTypes.INTEGER);

			List<RptF1DataVO> lstResult = repo.getListByNamedQuery(RptF1DataVO.class, "PKG_SHOP_REPORT.XNTF1_REPORT", inParams);
			//
			if (null == lstResult || lstResult.size() <= 0) {
				return null;
			} else {
				return RptF1DataVO.groupByCat(lstResult);
			}
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public RptCustomerStockVO getRptCustomerStockVO(Long shopId, Date date) throws BusinessException {
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException(ExceptionCode.REPORT_SHOP_ID_IS_NULL);
			}

			Shop shop = shopDAO.getShopById(shopId);

			if (shop == null) {
				throw new IllegalArgumentException(ExceptionCode.REPORT_SHOP_INSTANCE_IS_NULL);
			}

			List<RptCustomerStock2VO> lst = stockTotalDAO.getListRptCustomerStockVO(shopId, date);

			RptCustomerStockVO rptCustomerStockVO = new RptCustomerStockVO();
			rptCustomerStockVO.setShop(shop);
			RptCustomerStock1VO rptCustomerStock1VO = null;
			String productInfoCode = "";

			for (RptCustomerStock2VO rptCustomerStock2VO : lst) {
				if (!productInfoCode.equals(rptCustomerStock2VO.getProductInfoCode())) {
					productInfoCode = rptCustomerStock2VO.getProductInfoCode();
					rptCustomerStock1VO = new RptCustomerStock1VO();
					rptCustomerStock1VO.setProductInfoCode(productInfoCode);
					rptCustomerStockVO.getLstRptCustomerStock1VO().add(rptCustomerStock1VO);
				}

				rptCustomerStock1VO.getLstRptCustomerStock2VO().add(rptCustomerStock2VO);

				//
				rptCustomerStock1VO.setAmount(rptCustomerStock1VO.getAmount().add(rptCustomerStock2VO.getAmount()));
			}

			return rptCustomerStockVO;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Lay danh sach sale_order_detail
	 * 
	 * @author hungnm
	 */
	@Override
	public List<RptSaleOrderLotVO> getListSaleOrderByDeliveryStaff(Long shopId, Long deliveryStaffId, Date fromDate, Date toDate, Integer isFreeItem, Boolean getPromotion, Long saleOrderId) throws BusinessException {
		try {
			List<RptSaleOrderLotVO> lst = saleOrderDetailDAO.getListSaleOrderLotVO(null, shopId, deliveryStaffId, fromDate, toDate, isFreeItem, getPromotion, saleOrderId);
			return lst;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	/**
	 * Bao cao chenh lech ton kho voi thuc te
	 * 
	 * @author trietptm
	 */
	@Override
	public List<RptCycleCountDiff3VO> getListRptCycleCountDiff3VO(long cycleCountId) throws BusinessException {
		try {
			List<RptCycleCountDiff3VO> tmp = cycleCountResultDAO.getListRptCycleCountDiff3VO(cycleCountId);
			return tmp;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * Bao cao chenh lech ton kho voi thuc te
	 */
	@Override
	public RptCycleCountDiffVO getCLTKTTReport(Long shopId, Date fromDate, Date toDate, Long cycleCountId) throws BusinessException {
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			RptCycleCountDiffVO rptCycleCountDiffVO = new RptCycleCountDiffVO();
			rptCycleCountDiffVO.setShop(shop);
			//
//			List<RptCycleCountDiff3VO> tmp = cycleCountResultDAO.getListRptCycleCountDiff3VO(shopId, fromDate, toDate, cycleCountCode);
			List<RptCycleCountDiff3VO> tmp = cycleCountResultDAO.getListRptCycleCountDiff3VO(cycleCountId);
			//
			RptCycleCountDiff1VO rptCycleCountDiff1VO = null;
			RptCycleCountDiff2VO rptCycleCountDiff2VO = null;
			String cycleCountCodeTmp = "";
			String stockCardNumber = "";
			String productCode = "";
			for (RptCycleCountDiff3VO rptCycleCountDiff3VO : tmp) {
				// update: HUNGNM
				rptCycleCountDiff3VO.setShopCode(shop.getShopCode());
				// end update
				if (!cycleCountCodeTmp.equals(rptCycleCountDiff3VO.getCycleCountCode())) {

					cycleCountCodeTmp = rptCycleCountDiff3VO.getCycleCountCode();
					rptCycleCountDiff1VO = new RptCycleCountDiff1VO();
					rptCycleCountDiff1VO.setCycleCountCode(cycleCountCodeTmp);
					//
					rptCycleCountDiffVO.getLstRptCycleCountDiff1VO().add(rptCycleCountDiff1VO);
					productCode = "";
				}
				if (!productCode.equals(rptCycleCountDiff3VO.getProductCode()) || !stockCardNumber.equals(rptCycleCountDiff3VO.getStockCardNumber())) {
					productCode = rptCycleCountDiff3VO.getProductCode();
					stockCardNumber = rptCycleCountDiff3VO.getStockCardNumber();
					//
					rptCycleCountDiff2VO = new RptCycleCountDiff2VO();
					rptCycleCountDiff2VO.setCycleCountCode(cycleCountCodeTmp);
					rptCycleCountDiff2VO.setProductCode(productCode);
					rptCycleCountDiff2VO.setStockCardNumber(stockCardNumber);
					rptCycleCountDiff2VO.setCheckLot(rptCycleCountDiff3VO.getCheckLot());
					rptCycleCountDiff2VO.setPrice(rptCycleCountDiff3VO.getPrice());
					//
					rptCycleCountDiff1VO.getLstRptCycleCountDiff2VO().add(rptCycleCountDiff2VO);
				}
				if (rptCycleCountDiff3VO.getCheckLot() != null && rptCycleCountDiff3VO.getCheckLot() == 1 && (rptCycleCountDiff3VO.getLot() == null || rptCycleCountDiff3VO.getLot().trim().length() == 0)) {
				} else {
					rptCycleCountDiff2VO.getLstRptCycleCountDiff3VO().add(rptCycleCountDiff3VO);
				}
				int quantityCounted = 0;
				if (rptCycleCountDiff3VO.getQuantityCounted() != null) {
					quantityCounted = rptCycleCountDiff3VO.getQuantityCounted();
				}
				int quantityBeforeCount = 0;
				if (rptCycleCountDiff3VO.getQuantityCounted() != null) {
					quantityBeforeCount = rptCycleCountDiff3VO.getQuantityBeforeCount();
				}
				int quantityDiff = 0;
				if (rptCycleCountDiff3VO.getQuantityCounted() != null) {
					quantityDiff = rptCycleCountDiff3VO.getQuantityDiff();
				}
				BigDecimal amountDiff = BigDecimal.ZERO;
				if (rptCycleCountDiff3VO.getQuantityCounted() != null) {
					amountDiff = rptCycleCountDiff3VO.getAmountDiff();
				}
				rptCycleCountDiff2VO.setQuantityCounted(rptCycleCountDiff2VO.getQuantityCounted() + quantityCounted);
				rptCycleCountDiff2VO.setQuantityBeforeCount(rptCycleCountDiff2VO.getQuantityBeforeCount() + quantityBeforeCount);
				rptCycleCountDiff2VO.setQuantityDiff(rptCycleCountDiff2VO.getQuantityDiff() + quantityDiff);
				rptCycleCountDiff2VO.setAmountDiff(rptCycleCountDiff2VO.getAmountDiff().add(amountDiff));

				rptCycleCountDiff1VO.setQuantityCounted(rptCycleCountDiff1VO.getQuantityCounted() + (rptCycleCountDiff3VO.getQuantityCounted() == null ? 0 : rptCycleCountDiff3VO.getQuantityCounted()));
				rptCycleCountDiff1VO.setQuantityBeforeCount(rptCycleCountDiff1VO.getQuantityBeforeCount() + quantityBeforeCount);
				rptCycleCountDiff1VO.setQuantityDiff(rptCycleCountDiff1VO.getQuantityDiff() + quantityDiff);
				rptCycleCountDiff1VO.setAmountDiff(rptCycleCountDiff1VO.getAmountDiff().add(amountDiff));
			}
			return rptCycleCountDiffVO;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * DS3 - Bao cao thoi gian ban hang hieu qua cua nhan vien ban hang
	 * 
	 * @author hunglm16
	 * @since January 17,2014
	 * */
	@Override
	public List<RptExSaleOrder2VO> getRptBCPXHTNVGH(Long shopId, Long deliveryStaffId, Date fDate, Date tDate) throws BusinessException {
		try {
			//DS3 - Bao cao thoi gian ban hang hieu qua cua nhan vien ban hang
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BC_PXHTNVGH(?, :shopId, :deliveryStaffId,:fromDate ,:toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(deliveryStaffId);
			params.add(java.sql.Types.NUMERIC);
			params.add(4);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(5);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			return repo.getListByQueryDynamicFromPackage(RptExSaleOrder2VO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * getRptBCPXHTNVGH_CallPacked
	 * 
	 * @author hunglm16
	 * @since March 11, 2014
	 * @description Bao cao phieu xuat hang theo nhan vien giao hang update giai
	 *              doan 1
	 * 
	 * */
	private List<RptExSaleOrderVO> getRptBCPXHTNVGH_CallPacked(Long shopId, String lstSaleStaffId, Integer isPrint, Long deliveryStaffId, String listSaleOrdernumber, Date fDate, Date tDate, Long staffIdRoot,
			Integer flagCMS) throws BusinessException {
		try {
			//Goi Procedure (PK): phieu xuat hang theo nhan vien giao hang
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BC_PXHTNVGH(?, :shopId, :lstSaleStaffId, :isPrint, :deliveryStaffId, :listSaleOrdernumber, :fromDate , :toDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(StringUtility.isNullOrEmpty(lstSaleStaffId) ? "" : lstSaleStaffId);
			params.add(Types.VARCHAR);
			params.add(4);
			params.add(isPrint == null ? -1 : isPrint);
			params.add(Types.NUMERIC);
			params.add(5);
			params.add(deliveryStaffId);
			params.add(java.sql.Types.NUMERIC);
			params.add(6);
			params.add(listSaleOrdernumber);
			params.add(java.sql.Types.VARCHAR);
			params.add(7);
			params.add(new java.sql.Timestamp(fDate.getTime()));
			params.add(java.sql.Types.TIMESTAMP);
			params.add(8);
			params.add(new java.sql.Timestamp(tDate.getTime()));
			params.add(java.sql.Types.TIMESTAMP);
			params.add(9);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			params.add(10);
			params.add(flagCMS);
			params.add(Types.NUMERIC);

			return repo.getListByQueryDynamicFromPackageClobOrArray(RptExSaleOrderVO.class, sql.toString(), params, null, ",");
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	private List<RptExSaleOrderVO> getRptBCPXHTNVGHNotApproved_CallPacked(Long shopId, String lstSaleStaffId, Long deliveryStaffId, String listSaleOrdernumber, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		try {
			//Goi Procedure (PK): phieu xuat hang theo nhan vien giao hang
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BC_PXHTNVGH_A(?, :shopId, :lstSaleStaffId, :deliveryStaffId, :listSaleOrdernumber, :fromDate , :toDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(StringUtility.isNullOrEmpty(lstSaleStaffId) ? "" : lstSaleStaffId);
			params.add(Types.VARCHAR);
			params.add(4);
			params.add(deliveryStaffId);
			params.add(java.sql.Types.NUMERIC);
			params.add(5);
			params.add(listSaleOrdernumber);
			params.add(java.sql.Types.VARCHAR);
			params.add(6);
			params.add(new java.sql.Timestamp(fDate.getTime()));
			params.add(java.sql.Types.TIMESTAMP);
			params.add(7);
			params.add(new java.sql.Timestamp(tDate.getTime()));
			params.add(java.sql.Types.TIMESTAMP);
			params.add(8);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			params.add(9);
			params.add(flagCMS);
			params.add(Types.NUMERIC);
			return repo.getListByQueryDynamicFromPackageClobOrArray(RptExSaleOrderVO.class, sql.toString(), params, null,",");
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * 3.1.3.1.2 Phiếu xuất h?�ng theo nh?�n vi?�n giao h?�ng
	 * 
	 * @author hungnm
	 * @param kPaging
	 * @param shopId
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public List<RptExSaleOrder2VO> getListSaleOrderByDeliveryStaffGroupByProductCode(Long shopId, String staffSaleCode, Integer isPrint, Long deliveryStaffId, String listSaleOrdernumber, Date fromDate, Date toDate,
			Long staffIdRoot, Integer flagCMS) throws BusinessException {
		try {
			List<RptExSaleOrderVO> lst = this.getRptBCPXHTNVGH_CallPacked(shopId, staffSaleCode, isPrint, deliveryStaffId, listSaleOrdernumber, fromDate, toDate, staffIdRoot, flagCMS);
			List<RptExSaleOrder2VO> lstVo2 = new ArrayList<RptExSaleOrder2VO>();

			//2: deliveryStaffInfo; lstOrderNumber
			//1: productCode; productName; price; saleAmount; promoAmount;
			for (int i = 0; i < lst.size(); i++) {
				RptExSaleOrderVO voS = lst.get(i);
				RptExSaleOrder2VO vo2 = new RptExSaleOrder2VO();
				SortedMap<String, String> sortedOrderNumber = new TreeMap<String, String>();
				vo2.setDeliveryStaffInfo(voS.getDeliveryStaffInfo());
				vo2.setDeliveryStaffId(voS.getDeliveryStaffId());

				String lstOrderNumber = "";
				Integer thungPromo = 0;
				Integer thungSale = 0;
				Integer lePromo = 0;
				Integer leSale = 0;
				BigDecimal totalPromoAmount = new BigDecimal(0);
				BigDecimal totalSaleAmount = new BigDecimal(0);
				BigDecimal totalWeight = new BigDecimal(0);

				List<RptExSaleOrder1VO> lstVo1 = new ArrayList<RptExSaleOrder1VO>();
				for (int j = i; j < lst.size(); j++) {
					if (lst.get(j).getDeliveryStaffInfo() != null)
						if (lst.get(j).getDeliveryStaffInfo().equals(voS.getDeliveryStaffInfo())) {
							RptExSaleOrder1VO vo1 = new RptExSaleOrder1VO();
							vo1.setPrice(lst.get(j).getPrice());
							vo1.setProductCode(lst.get(j).getProductCode());
							vo1.setProductName(lst.get(j).getProductName());
							vo1.setCheckLot(lst.get(j).getCheckLot());
							if (!StringUtility.isNullOrEmpty(lst.get(j).getOrderNumber())) {
								String[] __lstSaleOrder = lst.get(j).getOrderNumber().split(",");
								for (String __saleOrderTmp : __lstSaleOrder) {
									String saleOrderTmp = __saleOrderTmp.trim();
									if (StringUtility.isNullOrEmpty(lstOrderNumber)) {
										lstOrderNumber = lst.get(j).getOrderNumber().replaceAll(" ", "").trim();
										;
									} else if (lstOrderNumber.indexOf(saleOrderTmp) == -1) {
										lstOrderNumber += "," + saleOrderTmp;
									}
								}
							}
							//promoAmount
							//saleAmount

							BigDecimal promoAmount = new BigDecimal(0);
							BigDecimal saleAmount = new BigDecimal(0);
							//BigDecimal discountAmount = new BigDecimal(0);

							List<RptExSaleOrderVO> lstVo = new ArrayList<RptExSaleOrderVO>();
							for (int k = j; k < lst.size(); k++) {
								if (lst.get(k).getProductCode().equals(vo1.getProductCode()) && lst.get(k).getDeliveryStaffInfo().equals(voS.getDeliveryStaffInfo())) {
									RptExSaleOrderVO vo = lst.get(k);
									lstVo.add(vo);
									if (vo.getPrice() != null && vo.getSaleQuantity() != null) {
										saleAmount = saleAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getSaleQuantity())));
										totalSaleAmount = totalSaleAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getSaleQuantity())));
										//discountAmount = discountAmount.add(vo.getDiscountAmount());
									}
									if (vo.getPrice() != null && vo.getPromoQuantity() != null) {
										promoAmount = promoAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getPromoQuantity())));
										totalPromoAmount = totalPromoAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getPromoQuantity())));
										//discountAmount = discountAmount.add(vo.getDiscountAmount());
									}
									thungPromo += vo.getThungPromo();
									thungSale += vo.getThungSale();
									lePromo += vo.getLePromo();
									leSale += vo.getLeSale();
									totalWeight = totalWeight.add(vo.getTotalWeight());

									String sOrderNumberTemp = lst.get(k).getOrderNumber().trim();
									if (sOrderNumberTemp.indexOf(",") >= 0) {
										String[] sSplitChildOrderNumber = sOrderNumberTemp.split(",");
										for (int t = 0; t < sSplitChildOrderNumber.length; t++) {
											String orderNumberTest = sSplitChildOrderNumber[t].trim();
											if (lstOrderNumber != null && lstOrderNumber.indexOf(orderNumberTest) == -1) {
												lstOrderNumber += "," + orderNumberTest;
											}
										}
									} else {
										if (lstOrderNumber != null && lstOrderNumber.indexOf(sOrderNumberTemp) == -1) {
											lstOrderNumber += "," + lst.get(k).getOrderNumber().trim();
										}
									}

									j = k;
								} else
									break;
							}
							vo1.setPromoAmount(promoAmount);
							vo1.setSaleAmount(saleAmount);

							for (RptExSaleOrderVO vo : lstVo) {
								vo.setPromoAmount(promoAmount);
								vo.setSaleAmount(saleAmount);
								if (vo.getOrderNumber() != null)
									sortedOrderNumber.put(vo.getOrderNumber(), vo.getOrderNumber());
							}

							vo1.setLstRptExSaleOrderVO(lstVo);
							vo1.setDiscountAmount(lst.get(j).getDiscountAmount());
							lstVo1.add(vo1);
							i = j;
						} else
							break;
				}
				vo2.setTotalPromoQuantity(thungPromo.toString() + "/" + lePromo.toString());
				vo2.setTotalSaleQuantity(thungSale.toString() + "/" + leSale.toString());
				vo2.setTotalPromoAmount(totalPromoAmount);
				vo2.setTotalSaleAmount(totalSaleAmount);
				vo2.setLstSaleOrder(lstVo1);
				vo2.setTotalWeight(totalWeight);

//				List<RptExSaleOrderVO> lstMoneyPromVO = saleOrderDetailDAO.getListExSaleOrderMoneyPromoVO(shopId, fromDate, toDate, deliveryStaffId, lstOrderNumber);
//
//				RptExSaleOrder1VO moneyPromVO1 = new RptExSaleOrder1VO();
//				moneyPromVO1.setLstRptExSaleOrderVO(lstMoneyPromVO);
//				moneyPromVO1.setDiscountAmount(lstMoneyPromVO != null && lstMoneyPromVO.size() > 0 ? lstMoneyPromVO.get(0).getDiscountAmount() : BigDecimal.ZERO);

				//vo2.setLstSaleOrderPromotion(lstMoneyPromVO1);
//				vo2.getLstSaleOrder().add(moneyPromVO1);
				//				String temp = "";
				//				for (String item: sortedOrderNumber.keySet()) {
				//					temp += item + ",";
				//				}
				sortedOrderNumber.clear();
				//vuongmq: 03/06/2014 sort ordernumber tang dan
				String[] sortListOrderNumber = lstOrderNumber.split(",".trim());
				Arrays.sort(sortListOrderNumber);
				String orderNumberSort = "";
				for (int k = 0, sz = sortListOrderNumber.length; k < sz; k++) {
					if (orderNumberSort.length() == 0) {
						orderNumberSort += sortListOrderNumber[k].trim();
					} else {
						orderNumberSort += ", " + sortListOrderNumber[k].trim();
					}
				}
				//vo2.setLstOrderNumber(lstOrderNumber);
				vo2.setLstOrderNumber(orderNumberSort); // vuongmq: 03/06/2014 
				lstVo2.add(vo2);
			}
			//			for (RptExSaleOrder2VO vo2: lstVo2) {
			//				vo2.setLstOrderNumber(saleOrderDetailDAO.getListOrderNumberForReport(shopId, fromDate, toDate, vo2.getDeliveryStaffId()));
			//				for (RptExSaleOrder1VO vo1: vo2.getLstVO()) {
			//					for (RptExSaleOrderVO vo: vo1.getLstRptExSaleOrderVO()) {
			//						vo.setTotalPromoQuantity(vo2.getTotalPromoQuantity());
			//						vo.setTotalSaleQuantity(vo2.getTotalSaleQuantity());
			//					}
			//				}
			//			}
			return lstVo2;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptExSaleOrder2VO> getListSaleOrderByDeliveryStaffGroupByProductCodeNotApproved(Long shopId, String staffSaleCode, Long deliveryStaffId, String listSaleOrdernumber, Date fromDate, Date toDate, Long staffRootId, Integer flagCMS)
			throws BusinessException {
		try {
			List<RptExSaleOrderVO> lst = this.getRptBCPXHTNVGHNotApproved_CallPacked(shopId, staffSaleCode, deliveryStaffId, listSaleOrdernumber, fromDate, toDate, staffRootId, flagCMS);
			List<RptExSaleOrder2VO> lstVo2 = new ArrayList<RptExSaleOrder2VO>();

			for (int i = 0; i < lst.size(); i++) {
				RptExSaleOrderVO voS = lst.get(i);
				RptExSaleOrder2VO vo2 = new RptExSaleOrder2VO();
				SortedMap<String, String> sortedOrderNumber = new TreeMap<String, String>();
				vo2.setDeliveryStaffInfo(voS.getDeliveryStaffInfo());
				vo2.setDeliveryStaffId(voS.getDeliveryStaffId());

				String lstOrderNumber = "";
				Integer thungPromo = 0;
				Integer thungSale = 0;
				Integer lePromo = 0;
				Integer leSale = 0;
				BigDecimal totalPromoAmount = new BigDecimal(0);
				BigDecimal totalSaleAmount = new BigDecimal(0);
				BigDecimal totalWeight = new BigDecimal(0);

				List<RptExSaleOrder1VO> lstVo1 = new ArrayList<RptExSaleOrder1VO>();
				for (int j = i; j < lst.size(); j++) {
					if (lst.get(j).getDeliveryStaffInfo() != null && !lst.get(j).getDeliveryStaffInfo().isEmpty())
						if (lst.get(j).getDeliveryStaffInfo().equals(voS.getDeliveryStaffInfo())) {
							RptExSaleOrder1VO vo1 = new RptExSaleOrder1VO();
							vo1.setPrice(lst.get(j).getPrice());
							vo1.setProductCode(lst.get(j).getProductCode());
							vo1.setProductName(lst.get(j).getProductName());
							vo1.setTotalWeight(lst.get(j).getTotalWeight());
							vo1.setCheckLot(lst.get(j).getCheckLot());
							if (!StringUtility.isNullOrEmpty(lst.get(j).getOrderNumber())) {
								String[] __lstSaleOrder = lst.get(j).getOrderNumber().split(",");
								for (String __saleOrderTmp : __lstSaleOrder) {
									String saleOrderTmp = __saleOrderTmp.trim();
									if (StringUtility.isNullOrEmpty(lstOrderNumber)) {
										lstOrderNumber = lst.get(j).getOrderNumber().replaceAll(" ", "").trim();
										;
									} else if (lstOrderNumber.indexOf(saleOrderTmp) == -1) {
										lstOrderNumber += "," + saleOrderTmp;
									}
								}
							}
							BigDecimal promoAmount = new BigDecimal(0);
							BigDecimal saleAmount = new BigDecimal(0);
							//BigDecimal discountAmount = new BigDecimal(0);

							List<RptExSaleOrderVO> lstVo = new ArrayList<RptExSaleOrderVO>();
							for (int k = j; k < lst.size(); k++) {
								if (lst.get(k) != null && lst.get(k).getProductCode() != null && lst.get(k).getProductCode().equals(vo1.getProductCode()) && lst.get(k).getDeliveryStaffInfo().equals(voS.getDeliveryStaffInfo())) {
									RptExSaleOrderVO vo = lst.get(k);
									lstVo.add(vo);
									if (vo.getPrice() != null && vo.getSaleQuantity() != null) {
										saleAmount = saleAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getSaleQuantity())));
										totalSaleAmount = totalSaleAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getSaleQuantity())));
										//discountAmount = discountAmount.add(vo.getDiscountAmount());
									}
									if (vo.getPrice() != null && vo.getPromoQuantity() != null) {
										promoAmount = promoAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getPromoQuantity())));
										totalPromoAmount = totalPromoAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getPromoQuantity())));
										//discountAmount = discountAmount.add(vo.getDiscountAmount());
									}
									thungPromo += vo.getThungPromo();
									thungSale += vo.getThungSale();
									lePromo += vo.getLePromo();
									leSale += vo.getLeSale();
									totalWeight = totalWeight.add(vo.getTotalWeight());

									String sOrderNumberTemp = "";
									if (!StringUtility.isNullOrEmpty(lst.get(k).getOrderNumber())) {
										sOrderNumberTemp = lst.get(k).getOrderNumber().trim();
									}
									if (sOrderNumberTemp.indexOf(",") >= 0) {
										String[] sSplitChildOrderNumber = sOrderNumberTemp.split(",");
										for (int t = 0; t < sSplitChildOrderNumber.length; t++) {
											String orderNumberTest = sSplitChildOrderNumber[t].trim();
											if (lstOrderNumber != null && lstOrderNumber.indexOf(orderNumberTest) == -1) {
												lstOrderNumber += "," + orderNumberTest;
											}
										}
									} else {
										if (lstOrderNumber != null && lstOrderNumber.indexOf(sOrderNumberTemp) == -1) {
											lstOrderNumber += "," + lst.get(k).getOrderNumber().trim();
										}
									}

									j = k;
								} else
									break;
							}
							vo1.setPromoAmount(promoAmount);
							vo1.setSaleAmount(saleAmount);

							for (RptExSaleOrderVO vo : lstVo) {
								vo.setPromoAmount(promoAmount);
								vo.setSaleAmount(saleAmount);
								if (vo.getOrderNumber() != null)
									sortedOrderNumber.put(vo.getOrderNumber(), vo.getOrderNumber());
							}

							vo1.setLstRptExSaleOrderVO(lstVo);
							vo1.setDiscountAmount(lst.get(j).getDiscountAmount());
							lstVo1.add(vo1);
							i = j;
						} else
							break;
				}
				vo2.setTotalPromoQuantity(thungPromo.toString() + "/" + lePromo.toString());
				vo2.setTotalSaleQuantity(thungSale.toString() + "/" + leSale.toString());
				vo2.setTotalPromoAmount(totalPromoAmount);
				vo2.setTotalSaleAmount(totalSaleAmount);
				vo2.setTotalWeight(totalWeight);
				vo2.setLstSaleOrder(lstVo1);

//				List<RptExSaleOrderVO> lstMoneyPromVO = saleOrderDetailDAO.getListExSaleOrderMoneyPromoVO(shopId, fromDate, toDate, deliveryStaffId, lstOrderNumber);
//
//				RptExSaleOrder1VO moneyPromVO1 = new RptExSaleOrder1VO();
//				moneyPromVO1.setLstRptExSaleOrderVO(lstMoneyPromVO);
//				moneyPromVO1.setDiscountAmount(lstMoneyPromVO != null && lstMoneyPromVO.size() > 0 ? lstMoneyPromVO.get(0).getDiscountAmount() : BigDecimal.ZERO);
//
//				vo2.getLstSaleOrder().add(moneyPromVO1);
				sortedOrderNumber.clear();
				String[] sortListOrderNumber = lstOrderNumber.split(",".trim());
				Arrays.sort(sortListOrderNumber);
				String orderNumberSort = "";
				for (int k = 0, sz = sortListOrderNumber.length; k < sz; k++) {
					if (orderNumberSort.length() == 0) {
						orderNumberSort += sortListOrderNumber[k].trim();
					} else {
						orderNumberSort += ", " + sortListOrderNumber[k].trim();
					}
				}
				vo2.setLstOrderNumber(orderNumberSort);
				lstVo2.add(vo2);
			}
			return lstVo2;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public RptPoCfrmFromDvkhVO getPOConfirmDVKHReport(Long shopId, Date fromDate, Date toDate) throws BusinessException {
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			if (fromDate == null) {
				throw new IllegalArgumentException("fromDate is null");
			}
			if (toDate == null) {
				throw new IllegalArgumentException("toDate is null");
			}
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			RptPoCfrmFromDvkhVO rptPoCfrmFromDvkhVO = new RptPoCfrmFromDvkhVO();
			rptPoCfrmFromDvkhVO.setShop(shop);
			List<RptPoCfrmFromDvkh2VO> lst = poVnmDetailDAO.getListRptPoCfrmFromDvkh2VO(shopId, fromDate, toDate);
			RptPoCfrmFromDvkh1VO rptPoCfrmFromDvkh1VO = null;
			String saleOrderNumber = "";
			for (RptPoCfrmFromDvkh2VO rptPoCfrmFromDvkh2VO : lst) {
				if (!saleOrderNumber.endsWith(rptPoCfrmFromDvkh2VO.getSaleOrderNumber())) {
					saleOrderNumber = rptPoCfrmFromDvkh2VO.getSaleOrderNumber();
					//
					rptPoCfrmFromDvkh1VO = new RptPoCfrmFromDvkh1VO();
					rptPoCfrmFromDvkh1VO.setSaleOrderNumber(saleOrderNumber);
					//
					rptPoCfrmFromDvkhVO.getLstRptPoCfrmFromDvkh1VO().add(rptPoCfrmFromDvkh1VO);
				}
				rptPoCfrmFromDvkh1VO.getLstRptPoCfrmFromDvkh2VO().add(rptPoCfrmFromDvkh2VO);
			}
			return rptPoCfrmFromDvkhVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptBookProductVinamilkVO> getListProductBookFromVNM(Long shopId, Date fromDate, Date toDate) throws BusinessException {
		try {
			return poAutoDetailDAO.getListRptBookProduct(shopId, fromDate, toDate);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptPTHVO> getPTHReport(Long shopId, Date fromDate, Date toDate, Long customerId) throws BusinessException {
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		try {
			List<RptPTHVO> res = null;
			if (shopId != null) {
				Shop s = shopDAO.getShopById(shopId);
				if (s != null) {
					res = new ArrayList<RptPTHVO>();
					List<RptReturnSaleOrderVO> lst = saleOrderDetailDAO.getListRptReturnSaleOrder(shopId, fromDate, toDate, customerId);
					Map<Long, RptPTHVO> rptPTHMap = new HashMap<Long, RptPTHVO>();
					if (lst != null && lst.size() > 0) {
						for (RptReturnSaleOrderVO child : lst) {
							child.safeSetNull();
							if (child.getConvfact() != null && child.getConvfact() > 0) {
								child.setNumThung(child.getQuantity() / child.getConvfact());
								child.setNumLe(child.getQuantity() % child.getConvfact());
							}
							if (rptPTHMap.containsKey(child.getSoId())) {
								RptPTHVO tmp = rptPTHMap.get(child.getSoId());
								tmp.getLstSODetail().add(child);
								rptPTHMap.put(child.getSoId(), tmp);
							} else {
								RptPTHVO tmp = new RptPTHVO();
								tmp.setLstSODetail(new ArrayList<RptReturnSaleOrderVO>());
								tmp.getLstSODetail().add(child);
								rptPTHMap.put(child.getSoId(), tmp);
							}
						}
						for (Long id : rptPTHMap.keySet()) {
							RptPTHVO child = new RptPTHVO();
							child.setShopName(s.getShopName());
							child.setShopAddress(s.getAddress());
							child.setShopPhone(s.getPhone());
							child.setLstSODetail(rptPTHMap.get(id).getLstSODetail());
							if (child.getLstSODetail() != null && child.getLstSODetail().size() > 0) {
								RptReturnSaleOrderVO tmp = child.getLstSODetail().get(0);
								String cusName = "";
								if (!StringUtility.isNullOrEmpty(tmp.getShortCode())) {
									cusName = tmp.getShortCode();
								}
								if (!StringUtility.isNullOrEmpty(tmp.getCustomerName())) {
									if (StringUtility.isNullOrEmpty(cusName)) {
										cusName = tmp.getCustomerName();
									} else {
										cusName = cusName + "-" + tmp.getCustomerName();
									}
								}
								if (!StringUtility.isNullOrEmpty(tmp.getPhone())) {
									if (StringUtility.isNullOrEmpty(cusName)) {
										cusName = tmp.getPhone();
									} else {
										cusName = cusName + "-" + tmp.getPhone();
									}
								}

								child.setCustomerName(cusName);
								child.setCustomerAddress(tmp.getCustomerAddress());
								if (StringUtility.isNullOrEmpty(tmp.getStaffName()) && StringUtility.isNullOrEmpty(tmp.getStaffCode())) {
									child.setStaffName("");
								} else if (StringUtility.isNullOrEmpty(tmp.getStaffName())) {
									child.setStaffName(tmp.getStaffCode());
								} else if (StringUtility.isNullOrEmpty(tmp.getStaffCode())) {
									child.setStaffName(tmp.getStaffName());
								} else {
									child.setStaffName(tmp.getStaffName() + "-" + tmp.getStaffCode());
								}

								if (StringUtility.isNullOrEmpty(tmp.getDeliveryName()) && StringUtility.isNullOrEmpty(tmp.getDeliveryCode())) {
									child.setDeliveryName("");
								} else if (StringUtility.isNullOrEmpty(tmp.getDeliveryName())) {
									child.setDeliveryName(tmp.getDeliveryCode());
								} else if (StringUtility.isNullOrEmpty(tmp.getDeliveryCode())) {
									child.setDeliveryName(tmp.getDeliveryName());
								} else {
									child.setDeliveryName(tmp.getDeliveryName() + "-" + tmp.getDeliveryCode());
								}

								child.setAmount(tmp.getAmount());
								child.setDiscountAmount(tmp.getDiscountAmount());
								child.setFinalAmount(tmp.getAmount().subtract(tmp.getDiscountAmount()));
								child.setOrderDate(tmp.getOrderDate());
								child.setInvoiceNumber(tmp.getOrderNumber());
								child.setRootInvoiceNumber(tmp.getRootOrderNumber());

							}
							child.safeSetNull();
							res.add(child);
						}
					}

				}
			}
			return res;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.report.ShopReportMgr#getListReportCountStore(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public List<RptCountStoreVO> getListReportCountStore(String cycleCountCode, String shopCode) throws BusinessException {

		if (cycleCountCode == null) {
			throw new IllegalArgumentException("cycleCountCode is null");
		}
		if (shopCode == null) {
			throw new IllegalArgumentException("shopCode is null");
		}
		try {
			return cycleCountMapProductDAO.getListReportCountStore(cycleCountCode, shopCode);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.report.ShopReportMgr#getListReportStockTransDetail(java
	 * .lang.Long, java.util.Date, java.util.Date)
	 */
	@Override
	public List<RptStockTransDetailVO> getListReportStockTransDetail(Long shopId, Date fromDate, Date toDate) throws BusinessException {

		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		try {
			return stockTransDetailDAO.getListReportStockTransDetail(shopId, fromDate, toDate);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptTotalAmountParentVO> getSummaryStaffTurnoverReport(Long shopId, Date fromDate, Date toDate, String staffCode) throws BusinessException {
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		List<RptTotalAmountParentVO> listResult = new ArrayList<RptTotalAmountParentVO>();
		try {
			List<RptTotalAmountVO> list = saleOrderDAO.getListRptTotalAmount(shopId, fromDate, toDate, staffCode);
			// thuc hien tao parent VO
			String staff_code = "";
			RptTotalAmountParentVO parent = new RptTotalAmountParentVO();
			for (RptTotalAmountVO item : list) {
				if (item.getStaffCode().equals(staff_code)) {
					if (parent.getListTotalAmount() != null) {
						parent.getListTotalAmount().add(item);
					}
				} else {
					if (!staff_code.equals("")) {
						listResult.add(parent);
						parent = new RptTotalAmountParentVO();
					}
					staff_code = item.getStaffCode();
					parent.setStaffCode(staff_code);
					parent.setStaffName(item.getStaffName());
					parent.getListTotalAmount().add(item);
				}
			}
			if (!staff_code.equals("")) {
				listResult.add(parent);
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
		return listResult;
	}

	//sontt---------------------------------------------------------------------------------------------------------------
	//vuonghn update
	//tungtt update
	public List<RptPayDisplayProgrameVO> getListRptPayDisplayProgrameVO(Long shopId, String lstProgramDisplayCode, Date fDate, Date tDate) throws BusinessException {
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			List<RptPayDisplayProgrameVO> result = new ArrayList<RptPayDisplayProgrameVO>();
			Map<String, RptPayDisplayProgrameVO> soMap = new HashMap<String, RptPayDisplayProgrameVO>();
			List<RptPayDisplayProgrameRecordProductVO> list = new ArrayList<RptPayDisplayProgrameRecordProductVO>();
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("lstProgramDisplayCode");
			inParams.add(lstProgramDisplayCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(tDate);
			inParams.add(StandardBasicTypes.DATE);

			list = repo.getListByNamedQuery(RptPayDisplayProgrameRecordProductVO.class, "PKG_SHOP_REPORT.BAOCAO_THCTHTB", inParams);

			List<String> lstSoID = new ArrayList<String>();

			if (list != null && list.size() > 0) {
				for (RptPayDisplayProgrameRecordProductVO child : list) {
					child.safeSetNull();
					if (soMap.get(child.getDisplayProgrameCode()) != null) {
						RptPayDisplayProgrameVO rpt = soMap.get(child.getDisplayProgrameCode());
						rpt.getListRecord().add(child);
					} else {
						RptPayDisplayProgrameVO rpt = new RptPayDisplayProgrameVO();
						rpt.setListRecord(new ArrayList<RptPayDisplayProgrameRecordProductVO>());
						rpt.getListRecord().add(child);
						soMap.put(child.getDisplayProgrameCode(), rpt);
						lstSoID.add(child.getDisplayProgrameCode());
					}
				}
				for (String soID : lstSoID) {
					result.add(soMap.get(soID));
				}
			}

			for (RptPayDisplayProgrameVO res : result) {
				List<RptPayDisplayProgrameRecordProductVO> lst = res.getListRecord();
				if (lst != null && lst.size() > 0) {
					res.setDisplayProgrameCode(lst.get(0).getDisplayProgrameCode());
					res.setDisplayProgrameName(lst.get(0).getDisplayProgrameName());
					res.setfDate(lst.get(0).getfDate());
					res.settDate(lst.get(0).gettDate());
				}
			}

			return result;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	//SangTN
	@Override
	public List<RptParentSaleOrderOfDeliveryVO> getListRptSaleOrderOfDelivery(Long shopId, String lstSaleStaffId, Integer isPrint, Date fromDate, Date toDate, String lstDeliveryId, String lstSaleOrderNumber,
			Long staffIdRoot, Integer flagCMS) throws BusinessException {

		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.RPT1_2_BKDHTNVGH(?, :shopId, :lstSaleStaffId, :isPrint,  :deliveryStaffId, :saleOrderNumber, :fDate, :tDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(Types.NUMERIC);
			params.add(3);
			params.add(StringUtility.isNullOrEmpty(lstSaleStaffId) ? "" : lstSaleStaffId);
			params.add(Types.VARCHAR);
			params.add(4);
			params.add(isPrint == null ? -1 : isPrint);
			params.add(Types.NUMERIC);
			params.add(5);
			params.add(lstDeliveryId);
			params.add(Types.VARCHAR);
			params.add(6);
			params.add(lstSaleOrderNumber);
			params.add(Types.VARCHAR);
			params.add(7);
			params.add(new java.sql.Timestamp(fromDate.getTime()));
			params.add(Types.TIMESTAMP);
			params.add(8);
			params.add(new java.sql.Timestamp(toDate.getTime()));
			params.add(Types.TIMESTAMP);
			params.add(9);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			params.add(10);
			params.add(flagCMS);
			params.add(Types.NUMERIC);

			List<RptSaleOrderOfDeliveryVO> list = repo.getListByQueryDynamicFromPackage(RptSaleOrderOfDeliveryVO.class, sql.toString(), params, null);
			List<RptParentSaleOrderOfDeliveryVO> result = new ArrayList<RptParentSaleOrderOfDeliveryVO>();

			String deliveryCode = "";
			RptParentSaleOrderOfDeliveryVO parentVO = null;
			// parse data
			for (RptSaleOrderOfDeliveryVO rptVO : list) {
				if (!rptVO.getDeliveryCode().equals(deliveryCode)) {
					if (parentVO != null) {
						result.add(parentVO);
					}
					deliveryCode = rptVO.getDeliveryCode();
					parentVO = new RptParentSaleOrderOfDeliveryVO();
					parentVO.setDeliveryCode(deliveryCode);
					parentVO.setDeliveryName(rptVO.getDeliveryName());
					parentVO.getListVO().add(rptVO);
				} else {
					parentVO.getListVO().add(rptVO);
				}
			}
			if (parentVO != null) {
				result.add(parentVO);
			}
			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptParentSaleOrderOfDeliveryVO> getListRptSaleOrderOfDeliveryNotApproved(Long shopId, String lstSaleStaffId, Date fromDate, Date toDate, String lstDeliveryId, String lstSaleOrderNumber, Long staffRootId, Integer flagCMS)
			throws BusinessException {

		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.RPT1_2_BKDHTNVGH_A(?, :shopId, :lstSaleStaffId,:deliveryStaffId, :saleOrderNumber, :fDate, :tDate, :staffRootId, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(Types.NUMERIC);
			params.add(3);
			params.add(StringUtility.isNullOrEmpty(lstSaleStaffId) ? "" : lstSaleStaffId);
			params.add(Types.VARCHAR);
			params.add(4);
			params.add(lstDeliveryId);
			params.add(Types.VARCHAR);
			params.add(5);
			params.add(lstSaleOrderNumber);
			params.add(Types.VARCHAR);
			params.add(6);
			params.add(new java.sql.Timestamp(fromDate.getTime()));
			params.add(Types.TIMESTAMP);
			params.add(7);
			params.add(new java.sql.Timestamp(toDate.getTime()));
			params.add(Types.TIMESTAMP);
			params.add(8);
			params.add(staffRootId);
			params.add(Types.NUMERIC);
			params.add(9);
			params.add(flagCMS);
			params.add(Types.NUMERIC);

			List<RptSaleOrderOfDeliveryVO> list = repo.getListByQueryDynamicFromPackage(RptSaleOrderOfDeliveryVO.class, sql.toString(), params, null);
			List<RptParentSaleOrderOfDeliveryVO> result = new ArrayList<RptParentSaleOrderOfDeliveryVO>();

			String deliveryCode = "";
			RptParentSaleOrderOfDeliveryVO parentVO = null;
			// parse data
			for (RptSaleOrderOfDeliveryVO rptVO : list) {
				if (!rptVO.getDeliveryCode().equals(deliveryCode)) {
					if (parentVO != null) {
						result.add(parentVO);
					}
					deliveryCode = rptVO.getDeliveryCode();
					parentVO = new RptParentSaleOrderOfDeliveryVO();
					parentVO.setDeliveryCode(deliveryCode);
					parentVO.setDeliveryName(rptVO.getDeliveryName());
					parentVO.getListVO().add(rptVO);
				} else {
					parentVO.getListVO().add(rptVO);
				}
			}
			if (parentVO != null) {
				result.add(parentVO);
			}
			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.report.ShopReportMgr#getListRptReturnSaleOrderAllowDelivery
	 * (java.lang.Long, java.util.Date, java.util.Date, java.lang.Long)
	 */
	//	@Override
	//	public List<RptParentReturnSaleOrderAllowDeliveryVO> getListRptReturnSaleOrderAllowDelivery(
	//			Long shopId, Date fromDate, Date toDate, Long deliveryId)
	//			throws BusinessException {
	//		
	//		if (shopId == null || shopId <= 0) {
	//			throw new IllegalArgumentException("shopId is null or <= 0");
	//		}
	//		if (fromDate == null) {
	//			throw new IllegalArgumentException("fromDate is null");
	//		}
	//		if (toDate == null) {
	//			throw new IllegalArgumentException("toDate is null");
	//		}
	//		try {
	//			List<RptParentReturnSaleOrderAllowDeliveryVO> result = new ArrayList<RptParentReturnSaleOrderAllowDeliveryVO>();
	//			List<RptReturnSaleOrderAllowDeliveryVO> list = saleOrderDetailDAO.getListRptReturnSaleOrderAllowDelivery(shopId, fromDate,toDate, deliveryId);
	//			String deliveryCode = "";
	//			RptParentReturnSaleOrderAllowDeliveryVO parentVO = null;
	//			// parse data
	//			for (RptReturnSaleOrderAllowDeliveryVO rptVO : list) {
	//				BigDecimal pomotionQuantity = rptVO.getPromotionQuantity();
	//				BigDecimal price = rptVO.getPrice();
	//				rptVO.setPromotionAmount(pomotionQuantity.multiply(price));
	//				rptVO.safeSetNull();
	//				if (!rptVO.getDeliveryCode().equals(deliveryCode)) {
	//					if (parentVO != null) {
	//						result.add(parentVO);
	//					}
	//					deliveryCode = rptVO.getDeliveryCode();
	//					parentVO = new RptParentReturnSaleOrderAllowDeliveryVO();
	//					List<OrderType> listOrderType = new ArrayList<OrderType>();
	//					listOrderType.add(OrderType.CM);
	//					
	//					List<SaleOrderNumberVO> listSaleOrderNumberVO = saleOrderDAO.getListSaleOrderNumberVOForDelivery(shopId, deliveryCode,listOrderType, fromDate, toDate);
	//					parentVO.setListSaleOrderNumber(listSaleOrderNumberVO);
	//					String lstSaleOrderNumberString = "";
	//					for(SaleOrderNumberVO saleOrderNumberVOTmp : listSaleOrderNumberVO){
	//						lstSaleOrderNumberString += saleOrderNumberVOTmp.getOrderNumber() +";";
	//					}
	//					parentVO.setLstSaleOrderNumberString(lstSaleOrderNumberString.substring(0, lstSaleOrderNumberString.length()-1));
	//					
	//					parentVO.setDeliveryCode(deliveryCode);
	//					parentVO.setDeliveryName(rptVO.getDeliveryName());
	//					parentVO.setSumAmount(rptVO.getAmount());
	//					parentVO.setSumDiscountAmount(rptVO.getSumDiscountAmount());
	//					parentVO.setSumPromotionAmount(rptVO.getPromotionAmount());
	//					parentVO.getListVO().add(rptVO);
	//				} else {
	//					parentVO.setSumAmount(parentVO.getSumAmount().add(rptVO.getAmount()));
	//					parentVO.setSumPromotionAmount(parentVO.getSumPromotionAmount().add(rptVO.getPromotionAmount()));
	//					parentVO.getListVO().add(rptVO);
	//				}
	//			}
	//			if (parentVO != null) {
	//				result.add(parentVO);
	//			}
	//			return result;
	//		} catch (Exception e) {
	//
	//			throw new BusinessException(e);
	//		}
	//	}
	@Override
	public List<RptParentReturnSaleOrderAllowDeliveryVO> getListRptReturnSaleOrderAllowDeliveryGroupByProductCode(Long shopId, Date fromDate, Date toDate, Long deliveryId) throws BusinessException {

		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		if (toDate == null) {
			throw new IllegalArgumentException("toDate is null");
		}
		try {

			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("lstDeliveryId");
			inParams.add("");
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptParentReturnSaleOrderAllowDeliveryVO> result = new ArrayList<RptParentReturnSaleOrderAllowDeliveryVO>();
			//List<RptReturnSaleOrderAllowDeliveryVO> lst = saleOrderDetailDAO.getListRptReturnSaleOrderAllowDelivery(shopId, fromDate,toDate, deliveryId);
			List<RptReturnSaleOrderAllowDeliveryVO> lst = repo.getListByNamedQuery(RptReturnSaleOrderAllowDeliveryVO.class, "PKG_SHOP_REPORT.BAOCAO_PTHNVGH", inParams);

			for (int i = 0; i < lst.size(); ++i) {
				RptReturnSaleOrderAllowDeliveryVO rptVO = lst.get(i);

				RptParentReturnSaleOrderAllowDeliveryVO parentVO = new RptParentReturnSaleOrderAllowDeliveryVO();
				//Begin: Lay thong tin cho doi tuong parentVO
				rptVO.safeSetNull();
				String deliveryCode = rptVO.getDeliveryCode();
				parentVO.setDeliveryCode(deliveryCode);
				parentVO.setDeliveryName(rptVO.getDeliveryName());
				parentVO.setSumDiscountAmount(rptVO.getSumDiscountAmount());

				List<OrderType> listOrderType = new ArrayList<OrderType>();
				listOrderType.add(OrderType.CM);
				List<SaleOrderNumberVO> listSaleOrderNumberVO = saleOrderDAO.getListSaleOrderNumberVOForDelivery(shopId, deliveryCode, listOrderType, fromDate, toDate);
				parentVO.setListSaleOrderNumber(listSaleOrderNumberVO);
				String lstSaleOrderNumberString = "";
				for (SaleOrderNumberVO saleOrderNumberVOTmp : listSaleOrderNumberVO) {
					lstSaleOrderNumberString += saleOrderNumberVOTmp.getOrderNumber() + ",";
				}
				parentVO.setLstSaleOrderNumberString(lstSaleOrderNumberString.substring(0, lstSaleOrderNumberString.length() - 1));
				//End

				BigDecimal sumPromoAmount = new BigDecimal(0);
				BigDecimal sumAmount = new BigDecimal(0);
				Integer thungPromo = 0;
				Integer thung = 0;
				Integer lePromo = 0;
				Integer le = 0;

				List<RptReturnSaleOrderAllowDeliveryProductVO> lstProductVO = new ArrayList<RptReturnSaleOrderAllowDeliveryProductVO>();
				for (int j = i; j < lst.size(); j++) {
					if (lst.get(j).getDeliveryCode() != null)
						if (lst.get(j).getDeliveryCode().equals(rptVO.getDeliveryCode())) {
							RptReturnSaleOrderAllowDeliveryProductVO rptProductVO = new RptReturnSaleOrderAllowDeliveryProductVO();
							rptProductVO.setProductCode(lst.get(j).getProductCode());
							rptProductVO.setProductName(lst.get(j).getProductName());
							rptProductVO.setPrice(lst.get(j).getPrice());

							BigDecimal promoAmount = new BigDecimal(0);
							BigDecimal amount = new BigDecimal(0);

							List<RptReturnSaleOrderAllowDeliveryVO> lstRptVO = new ArrayList<RptReturnSaleOrderAllowDeliveryVO>();
							for (int k = j; k < lst.size(); k++) {
								if (lst.get(k).getProductCode().equals(rptProductVO.getProductCode())) {
									RptReturnSaleOrderAllowDeliveryVO rptVOTmp = lst.get(k);
									rptVOTmp.safeSetNull();
									lstRptVO.add(rptVOTmp);

									BigDecimal pomotionQuantity = rptVOTmp.getPromotionQuantity();
									BigDecimal quantity = rptVOTmp.getQuantity();
									BigDecimal price = rptVOTmp.getPrice();
									promoAmount = promoAmount.add(pomotionQuantity.multiply(price));
									sumPromoAmount = sumPromoAmount.add(pomotionQuantity.multiply(price));
									amount = amount.add(quantity.multiply(price));
									sumAmount = sumAmount.add(quantity.multiply(price));

									thung += rptVOTmp.getThung();
									le += rptVOTmp.getLe();
									thungPromo += rptVOTmp.getThungPromo();
									lePromo += rptVOTmp.getLePromo();
									j = k;
								} else
									break;
							}
							rptProductVO.setAmount(amount);
							rptProductVO.setPromotionAmount(promoAmount);
							for (RptReturnSaleOrderAllowDeliveryVO vo : lstRptVO) {
								vo.setPromotionAmount(promoAmount);
								vo.setAmount(amount);
							}
							rptProductVO.setListVO(lstRptVO);
							lstProductVO.add(rptProductVO);
							i = j;
						} else
							break;
				}
				parentVO.setSumAmount(sumAmount);
				parentVO.setSumPromotionAmount(sumPromoAmount);
				parentVO.setSumQuantity(thung.toString() + "/" + le.toString());
				parentVO.setSumPromotionQuantity(thungPromo.toString() + "/" + lePromo.toString());
				parentVO.setListProductVO(lstProductVO);
				result.add(parentVO);
			}
			return result;
		} catch (Exception e) {

			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.report.ShopReportMgr#getRptProductPoVnmDetailVO(java
	 * .lang.Long)
	 */
	@Override
	public RptProductPoVnmDetailVO getRptProductPoVnmDetailVO(Long shopId) throws BusinessException {

		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			Shop shop = shopDAO.getShopById(shopId);
			if (shop == null) {
				throw new IllegalArgumentException("shop is null");
			}
			RptProductPoVnmDetailVO rptProductPoVnmDetailVO = new RptProductPoVnmDetailVO();
			rptProductPoVnmDetailVO.setShop(shop);
			List<RptProductPoVnmDetail2VO> lst = poVnmDetailDAO.getListRptProductPoVnmDetailVO(shopId);
			RptProductPoVnmDetail1VO rptProductPoVnmDetail1VO = null;
			String productCode = "";
			for (RptProductPoVnmDetail2VO rptProductPoVnmDetail2VO : lst) {
				if (!productCode.equals(rptProductPoVnmDetail2VO.getProductCode())) {
					productCode = rptProductPoVnmDetail2VO.getProductCode();
					//
					rptProductPoVnmDetail1VO = new RptProductPoVnmDetail1VO();
					rptProductPoVnmDetail1VO.setProductCode(rptProductPoVnmDetail2VO.getProductCode());
					rptProductPoVnmDetail1VO.setProductName(rptProductPoVnmDetail2VO.getProductName());
					//
					rptProductPoVnmDetailVO.getLstRptProductPoVnmDetail2VO().add(rptProductPoVnmDetail1VO);
				}
				rptProductPoVnmDetail1VO.getLstRptProductPoVnmDetail2VO().add(rptProductPoVnmDetail2VO);
			}

			return rptProductPoVnmDetailVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptPoStatusTracking2VO> getRptPoStatusTrackingVO(Date fromDate, Date toDate, Long shopId) throws BusinessException {
		try {
			List<RptPoStatusTrackingVO> lst = poVnmDAO.getListRptPoStatusTrackingVO(fromDate, toDate, shopId);

			List<RptPoStatusTracking2VO> lst2 = new ArrayList<RptPoStatusTracking2VO>();

			if (lst.size() > 0) {
				RptPoStatusTrackingVO item = lst.get(0);
				item.safeSetNull();

				RptPoStatusTracking2VO item2 = new RptPoStatusTracking2VO();
				RptPoStatusTracking1VO item1 = new RptPoStatusTracking1VO();
				item1.setPoAutoNumber(item.getPoAutoNumber());
				item1.setLstRptPoStatusTrackingVO(Arrays.asList(item));
				item2.setPoAutoDate(item.getPoAutoDate());
				item2.setLstRptPoStatusTracking1VO(Arrays.asList(item1));
				lst2.add(item2);
				lst.remove(0);
			}

			for (RptPoStatusTrackingVO item : lst) {
				item.safeSetNull();

				RptPoStatusTracking2VO item2 = lst2.get(lst2.size() - 1);
				if (item2.getPoAutoDate().equals(item.getPoAutoDate())) {
					// dua vao item2
					List<RptPoStatusTracking1VO> lstItem1 = item2.getLstRptPoStatusTracking1VO();
					RptPoStatusTracking1VO item1 = lstItem1.get(lstItem1.size() - 1);
					if (item1.getPoAutoNumber().equals(item.getPoAutoNumber())) {
						List<RptPoStatusTrackingVO> lstItem = item1.getLstRptPoStatusTrackingVO();
						List<RptPoStatusTrackingVO> lstNewItem = new ArrayList<RptPoStatusTrackingVO>();
						lstNewItem.addAll(lstItem);
						lstNewItem.add(item);
						item1.setLstRptPoStatusTrackingVO(lstNewItem);
						item1.setSkuQuantity(lstNewItem.size());
					} else {
						item1 = new RptPoStatusTracking1VO();
						item1.setPoAutoNumber(item.getPoAutoNumber());
						item1.setLstRptPoStatusTrackingVO(Arrays.asList(item));
						item1.setSkuQuantity(1);

						List<RptPoStatusTracking1VO> lstNewItem1 = new ArrayList<RptPoStatusTracking1VO>();
						lstNewItem1.addAll(lstItem1);
						lstNewItem1.add(item1);
						item2.setLstRptPoStatusTracking1VO(lstNewItem1);
					}
				} else {
					RptPoStatusTracking1VO newItem1 = new RptPoStatusTracking1VO();
					newItem1.setPoAutoNumber(item.getPoAutoNumber());
					newItem1.setLstRptPoStatusTrackingVO(Arrays.asList(item));
					newItem1.setSkuQuantity(1);

					RptPoStatusTracking2VO newItem2 = new RptPoStatusTracking2VO();
					newItem2.setPoAutoDate(item.getPoAutoDate());
					newItem2.setLstRptPoStatusTracking1VO(Arrays.asList(newItem1));
					lst2.add(newItem2);
				}

			}

			return lst2;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * 
	 * @author hungnm
	 */
	@Override
	public List<RptSaleOrderInDate2VO> getListRptSaleOrderInDate2VO(Date fromDate, Date toDate, List<Long> lstStaffId, Long shopId) throws BusinessException {
		try {
			List<RptSaleOrderInDateVO> lst = saleOrderDAO.getListRptSaleOrderInDateVO(null, fromDate, toDate, lstStaffId, shopId);
			List<RptSaleOrderInDate2VO> lst2 = new ArrayList<RptSaleOrderInDate2VO>();

			if (lst.size() > 0) {
				RptSaleOrderInDateVO item = lst.get(0);

				RptSaleOrderInDate2VO item2 = new RptSaleOrderInDate2VO();
				RptSaleOrderInDate1VO item1 = new RptSaleOrderInDate1VO();
				item1.setOrderDate(item.getOrderDate());
				item1.setLstRptSaleOrderInDateVO(Arrays.asList(item));

				item2.setStaffInfo(item.getStaffInfo());
				item2.setLstRptSaleOrderInDate1VO(Arrays.asList(item1));

				lst2.add(item2);
				lst.remove(0);
			}

			for (RptSaleOrderInDateVO item : lst) {
				RptSaleOrderInDate2VO item2 = lst2.get(lst2.size() - 1);
				if (item2.getStaffInfo().equals(item.getStaffInfo())) {
					// dua vao item2
					List<RptSaleOrderInDate1VO> lstItem1 = item2.getLstRptSaleOrderInDate1VO();
					RptSaleOrderInDate1VO item1 = lstItem1.get(lstItem1.size() - 1);
					if (item1.getOrderDate().equals(item.getOrderDate())) {
						List<RptSaleOrderInDateVO> lstItem = item1.getLstRptSaleOrderInDateVO();
						List<RptSaleOrderInDateVO> lstNewItem = new ArrayList<RptSaleOrderInDateVO>();
						lstNewItem.addAll(lstItem);
						lstNewItem.add(item);
						item1.setLstRptSaleOrderInDateVO(lstNewItem);
						// List<RptPoStatusTracking1VO> lstNewItem1 = new
						// ArrayList<RptPoStatusTracking1VO>();
						// lstNewItem1.addAll(lstItem1);
						// lstNewItem1.add(item1);
						// item2.setLstRptPoStatusTracking1VO(lstNewItem1);
					} else {
						item1 = new RptSaleOrderInDate1VO();
						item1.setOrderDate(item.getOrderDate());
						item1.setLstRptSaleOrderInDateVO(Arrays.asList(item));

						List<RptSaleOrderInDate1VO> lstNewItem1 = new ArrayList<RptSaleOrderInDate1VO>();
						lstNewItem1.addAll(lstItem1);
						lstNewItem1.add(item1);
						item2.setLstRptSaleOrderInDate1VO(lstNewItem1);
					}
				} else {
					RptSaleOrderInDate1VO newItem1 = new RptSaleOrderInDate1VO();
					newItem1.setOrderDate(item.getOrderDate());
					newItem1.setLstRptSaleOrderInDateVO(Arrays.asList(item));

					RptSaleOrderInDate2VO newItem2 = new RptSaleOrderInDate2VO();
					newItem2.setStaffInfo(item.getStaffInfo());
					newItem2.setLstRptSaleOrderInDate1VO(Arrays.asList(newItem1));
					lst2.add(newItem2);
				}

			}

			return lst2;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	//	@Override
	//	public RptInvDetailStatVO getBKCTHDGTGTReport(Long shopId,
	//			String staffCode, String shortCode, Integer vat, Date fromDate,
	//			Date toDate, InvoiceStatus status) throws BusinessException {
	//		try {
	//			if (shopId == null || shopId <= 0) {
	//				throw new IllegalArgumentException("shopId is null or <= 0");
	//			}
	//			if (fromDate == null) {
	//				throw new IllegalArgumentException("fromDate is null");
	//			}
	//			if (toDate == null) {
	//				throw new IllegalArgumentException("toDate is null");
	//			}
	//			Shop shop = shopDAO.getShopById(shopId);
	//			if (shop == null) {
	//				throw new IllegalArgumentException("shop is null");
	//			}
	//			RptInvDetailStatVO rptInvDetailStatVO = new RptInvDetailStatVO();
	//			rptInvDetailStatVO.setShop(shop);
	//			List<RptInvDetailStatDetailVO> lst = saleOrderDetailDAO
	//					.getListRptInvDetailStat2VO(shopId, staffCode, shortCode,
	//							vat, fromDate, toDate, status);
	//			RptInvDetailStat1VO rptInvDetailStat1VO = new  RptInvDetailStat1VO();
	//			String invoiceNumber = "";
	//			Long totalSku = 0l;
	//			BigDecimal totalAmount = BigDecimal.ZERO;
	//			BigDecimal totalFinalAmount = BigDecimal.ZERO;
	//			BigDecimal totalDiscount = BigDecimal.ZERO;
	//			Long sku = 0l;
	//			BigDecimal amount = BigDecimal.ZERO;
	//			BigDecimal finalAmount = BigDecimal.ZERO;
	//			BigDecimal discount = BigDecimal.ZERO;
	//			BigDecimal taxAmount = BigDecimal.ZERO;
	//			for (RptInvDetailStatDetailVO child : lst) {
	//				if (!invoiceNumber.equals(child.getInvoiceNumber())) {
	//					sku = 0l;
	//					amount = BigDecimal.ZERO;
	//					finalAmount = BigDecimal.ZERO;
	//					discount = BigDecimal.ZERO;
	//					taxAmount = BigDecimal.ZERO;
	//					invoiceNumber = child.getInvoiceNumber();
	//					//
	//					rptInvDetailStat1VO = new RptInvDetailStat1VO();
	//					rptInvDetailStat1VO.setInvoiceNumber(invoiceNumber);
	//					//
	//					rptInvDetailStatVO.getLstRptInvDetailStat1VO().add(rptInvDetailStat1VO);
	//				}
	//				try {
	//					child.safeSetNull();
	//				} catch (Exception e) {
	//					throw new BusinessException(e);
	//				}
	//				if(child.getSku() != null){
	//					totalSku += child.getSku();
	//					rptInvDetailStat1VO.setTotalSku(rptInvDetailStat1VO.getTotalSku()+ child.getSku()) ;
	//				}
	//				if(child.getAmount() != null){
	//					totalAmount = totalAmount.add(child.getAmount());
	//					rptInvDetailStat1VO.setTotalAmount(rptInvDetailStat1VO.getTotalAmount().add(child.getAmount()));
	//				}
	//				if(child.getFinalAmount() != null){
	//					totalFinalAmount = totalFinalAmount.add(child.getFinalAmount());
	//					rptInvDetailStat1VO.setTotalFinalAmount(rptInvDetailStat1VO.getTotalFinalAmount().add(child.getFinalAmount()));
	//				}
	//				if(child.getDiscount() != null){
	//					totalDiscount = totalDiscount.add(child.getDiscount());
	//					rptInvDetailStat1VO.setTotalDiscount(rptInvDetailStat1VO.getTotalDiscount().add(child.getDiscount()));
	//				}
	//				if(child.getTaxAmount() != null){
	//					rptInvDetailStat1VO.setTotalTaxAmount(rptInvDetailStat1VO.getTotalTaxAmount().add(child.getTaxAmount()));
	//				}
	//				rptInvDetailStat1VO.setVat(child.getVat());
	//				rptInvDetailStat1VO.getLstRptInvDetailStat2VO().add(child);
	//			}
	//			rptInvDetailStatVO.setTotalSku(totalSku);
	//			rptInvDetailStatVO.setTotalAmount(totalAmount);
	//			rptInvDetailStatVO.setTotalFinalAmount(totalFinalAmount);
	//			return rptInvDetailStatVO;
	//		} catch (DataAccessException e) {
	//			throw new BusinessException(e);
	//		}
	//	}

	@Override
	public List<RptReturnSaleOrderByDeliveryLv01VO> getListRptReturnSaleOrderByDelivery(Long shopId, OrderType orderType, Date fromDate, Date toDate, Long deliveryId) throws BusinessException {
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		try {
			List<RptReturnSaleOrderByDeliveryDataVO> listData = saleOrderDAO.getListRptReturnSaleOrderByDelivery(shopId, orderType, fromDate, toDate, deliveryId);

			if (null == listData || listData.size() <= 0) {
				return new ArrayList<RptReturnSaleOrderByDeliveryLv01VO>();
			} else {
				return RptReturnSaleOrderByDeliveryDataVO.groupByCusAndCat(listData);
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.report.ShopReportMgr#getRptViewInvoice(ths.dms.core
	 * .entities.enumtype.KPaging, java.lang.Float, java.lang.String,
	 * java.lang.Long, java.util.Date, java.util.Date,
	 * ths.dms.core.entities.enumtype.InvoiceCustPayment, java.lang.String)
	 */
	@Override
	public List<RptParentViewInvoiceVO> getRptViewInvoice(Float vat, String customerCode, List<Long> listStaffId, Date fromDate, Date toDate, InvoiceCustPayment custPayment, String selName, String orderNumber, Long shopId, String invoiceNumber1)
			throws BusinessException {

		try {
			List<RptParentViewInvoiceVO> result = new ArrayList<RptParentViewInvoiceVO>();
			RptParentViewInvoiceVO vo = null;
			List<RptViewInvoiceVO> list = invoiceDAO.getRptViewInvoice(null, vat, customerCode, listStaffId, fromDate, toDate, custPayment, selName, orderNumber, shopId, invoiceNumber1);
			if (list != null && list.size() > 0) {
				String staffCode = "";
				String tempOrderNumber = "";
				String invoiceNumber = "";
				for (RptViewInvoiceVO rptViewInvoiceVO : list) {
					if (rptViewInvoiceVO.getIsFreeItem().intValue() == 1) {
						rptViewInvoiceVO.setToMoney(new BigDecimal(0));
					}
					if ((staffCode.equals(rptViewInvoiceVO.getStaffCode())) && (tempOrderNumber.equals(rptViewInvoiceVO.getOrderNumber())) && (invoiceNumber.equals(rptViewInvoiceVO.getInvoiceNumber()))) {
						vo.getListProducts().add(rptViewInvoiceVO);
					} else {
						if (vo != null) {
							result.add(vo);
						}
						staffCode = rptViewInvoiceVO.getStaffCode();
						tempOrderNumber = rptViewInvoiceVO.getOrderNumber();
						invoiceNumber = rptViewInvoiceVO.getInvoiceNumber();
						vo = new RptParentViewInvoiceVO();
						//set data for vo
						vo.setStaffCode(rptViewInvoiceVO.getStaffCode());
						vo.setInvoiceNumber(rptViewInvoiceVO.getInvoiceNumber());
						if (rptViewInvoiceVO.getCpnyAddr() != null) {
							vo.setCpnyAddr(rptViewInvoiceVO.getCpnyAddr());
						}
						if (rptViewInvoiceVO.getCpnyBankAccount() != null)
							vo.setCpnyBankAccount(rptViewInvoiceVO.getCpnyBankAccount());
						if (rptViewInvoiceVO.getCpnyName() != null)
							vo.setCpnyName(rptViewInvoiceVO.getCpnyName());
						if (rptViewInvoiceVO.getCustBankAccount() != null)
							vo.setCustBankAccount(rptViewInvoiceVO.getCustBankAccount());
						if (rptViewInvoiceVO.getCustBankName() != null)
							vo.setCustBankName(rptViewInvoiceVO.getCustBankName());
						if (rptViewInvoiceVO.getCustDelAddr() != null)
							vo.setCustDelAddr(rptViewInvoiceVO.getCustDelAddr());
						if (rptViewInvoiceVO.getCustName() != null)
							vo.setCustName(rptViewInvoiceVO.getCustName());
						if (rptViewInvoiceVO.getCustomerAddr() != null)
							vo.setCustomerAddr(rptViewInvoiceVO.getCustomerAddr());
						if (rptViewInvoiceVO.getCustomerCode() != null)
							vo.setCustomerCode(rptViewInvoiceVO.getCustomerCode());
						if (rptViewInvoiceVO.getCustPayment() != null)
							vo.setCustPayment(rptViewInvoiceVO.getCustPayment());
						if (rptViewInvoiceVO.getCustTaxNumber() != null)
							vo.setCustTaxNumber(rptViewInvoiceVO.getCustTaxNumber());
						if (rptViewInvoiceVO.getDeliveryName() != null)
							vo.setDeliveryName(rptViewInvoiceVO.getDeliveryName());
						if (rptViewInvoiceVO.getOrderDate() != null)
							vo.setOrderDate(rptViewInvoiceVO.getOrderDate());
						if (rptViewInvoiceVO.getOrderNumber() != null)
							vo.setOrderNumber(rptViewInvoiceVO.getOrderNumber());
						if (rptViewInvoiceVO.getSelName() != null)
							vo.setSelName(rptViewInvoiceVO.getSelName());
						//						if (rptViewInvoiceVO.getTaxAmount() != null)
						//							vo.setTaxAmount(rptViewInvoiceVO.getTaxAmount());
						if (rptViewInvoiceVO.getVat() != null)
							vo.setVat(rptViewInvoiceVO.getVat());
						if (rptViewInvoiceVO.getDiscount() != null)
							vo.setDiscount(rptViewInvoiceVO.getDiscount());

						vo.getListProducts().add(rptViewInvoiceVO);
					}
				}
				if (vo != null) {
					result.add(vo);
				}
			}
			return result;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	//vuonghn update
	/**
	 * Update
	 * 
	 * @author hunglm16
	 * @since March 14, 2013
	 * @description Phieu Doi Tra Hang
	 * */
	@Override
	public List<RptPDTHVO> getPDTHReport(Long shopId, Date fromDate, Date toDate, Long staffId, String lstSaleOderNumber) throws BusinessException {
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		try {
			/*
			 * List<Object> inParams = new ArrayList<Object>();
			 * inParams.add("shopId"); inParams.add(shopId == null ? 0 :
			 * shopId); inParams.add(StandardBasicTypes.LONG);
			 * inParams.add("lstDeliveryId"); inParams.add(deliveryCode);
			 * inParams.add(StandardBasicTypes.STRING);
			 * 
			 * inParams.add("fromDate"); inParams.add(fromDate);
			 * inParams.add(StandardBasicTypes.DATE);
			 * 
			 * inParams.add("toDate"); inParams.add(toDate);
			 * inParams.add(StandardBasicTypes.DATE); List<RptPDTHVO> lstRes =
			 * new ArrayList<RptPDTHVO>(); Map<Long, RptPDTHVO> soMap = new
			 * HashMap<Long, RptPDTHVO>(); /*List<RptSaleOrderDetailVO> lstFull
			 * = saleOrderDetailDAO.getPDTHReport( shopId, fromDate, toDate,
			 * staffId, deliveryCode);
			 */
			//			List<RptSaleOrderDetailVO> lstFull = repo.getListByNamedQuery(RptSaleOrderDetailVO.class, "PKG_SHOP_REPORT.BAOCAO_PDTH", inParams);*/

			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.BAOCAO_PDTH(?, :shopId, :lstSaleOderNumber, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);

			params.add(3);
			params.add(lstSaleOderNumber);
			params.add(java.sql.Types.VARCHAR);

			params.add(4);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(5);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			List<RptSaleOrderDetailVO> lstFull = repo.getListByQueryDynamicFromPackage(RptSaleOrderDetailVO.class, sql.toString(), params, null);

			List<RptPDTHVO> lstRes = new ArrayList<RptPDTHVO>();
			Map<Long, RptPDTHVO> soMap = new HashMap<Long, RptPDTHVO>();

			List<Long> lstSoID = new ArrayList<Long>();
			if (lstFull != null && lstFull.size() > 0) {
				for (RptSaleOrderDetailVO child : lstFull) {
					child.safeSetNull();
					if (soMap.get(child.getSoId().longValue()) != null) {
						RptPDTHVO rpt = soMap.get(child.getSoId().longValue());
						rpt.getLstSODetail().add(child);
					} else {
						RptPDTHVO rpt = new RptPDTHVO();
						rpt.setLstSODetail(new ArrayList<RptSaleOrderDetailVO>());
						rpt.getLstSODetail().add(child);
						soMap.put(child.getSoId().longValue(), rpt);
						lstSoID.add(child.getSoId().longValue());
					}
				}
				for (Long soId : lstSoID) {
					lstRes.add(soMap.get(soId));
				}
			}
			String s = null;
			for (RptPDTHVO res : lstRes) {
				List<RptSaleOrderDetailVO> lst = res.getLstSODetail();
				if (lst != null && lst.size() > 0) {
					res.setCustomerAddress(lst.get(0).getCustomerAddress());
					res.setCustomerCode(lst.get(0).getCustomerCode());
					res.setCustomerName(lst.get(0).getCustomerName());
					res.setCustomerShortCode(lst.get(0).getCustomerShortCode());
					res.setDeliveryCode(lst.get(0).getDeliveryCode());
					res.setDeliveryName(lst.get(0).getDeliveryName());
					res.setOrderDate(lst.get(0).getOrderDate());
					res.setStaffCode(lst.get(0).getStaffCode());
					res.setStaffName(lst.get(0).getStaffName());
					s = lst.get(0).getCustomerPhone();
					if (s == null) {
						s = "";
					}
					if (!StringUtility.isNullOrEmpty(lst.get(0).getCustomerMobiphone())) {
						if (!StringUtility.isNullOrEmpty(s)) {
							s = s + " / ";
						}
						s = s + lst.get(0).getCustomerMobiphone();
					}
					res.setCustomerPhone(s);
					res.setCarNumber(lst.get(0).getCarNumber());
					BigDecimal total = BigDecimal.ZERO;
					for (RptSaleOrderDetailVO child : lst) {
						if (child.getAmount() != null) {
							total = total.add(child.getAmount());
						}
					}
					res.setTotal(total);
					res.setInvoiceNumber(lst.get(0).getInvoiceNumber());
					res.setIsHasInvoice(false);
					if (!StringUtility.isNullOrEmpty(res.getInvoiceNumber())) {
						res.setIsHasInvoice(true);
					}
				}
			}
			return lstRes;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	//vuonghn
	@Override
	public List<RptPDHCGVO> getPDHCGReport(Long shopId, Date fromDate, Date toDate, String deliveryCode) throws BusinessException {
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		try {
			List<RptPDHCGVO> lstRes = new ArrayList<RptPDHCGVO>();
			Map<Long, RptPDHCGVO> soMap = new HashMap<Long, RptPDHCGVO>();
			//List<RptSaleOrderDetail2VO> lstFull = saleOrderDetailDAO.getPDHCGReport(shopId, fromDate, toDate, deliveryCode);
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("deliveryCode");
			inParams.add(deliveryCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptSaleOrderDetail2VO> lstFull = repo.getListByNamedQuery(RptSaleOrderDetail2VO.class, "PKG_SHOP_REPORT.BAOCAO_PDHCG", inParams);
			List<Long> lstSoID = new ArrayList<Long>();
			if (lstFull != null && lstFull.size() > 0) {
				for (RptSaleOrderDetail2VO child : lstFull) {
					child.safeSetNull();
					if (soMap.get(child.getSoID()) != null) {
						RptPDHCGVO rpt = soMap.get(child.getSoID());
						rpt.getLst().add(child);
					} else {
						RptPDHCGVO rpt = new RptPDHCGVO();
						rpt.setLst(new ArrayList<RptSaleOrderDetail2VO>());
						rpt.getLst().add(child);
						soMap.put(child.getSoID(), rpt);
						lstSoID.add(child.getSoID());
					}
				}
				for (Long soId : lstSoID) {
					lstRes.add(soMap.get(soId));
				}
			}
			for (RptPDHCGVO res : lstRes) {
				List<RptSaleOrderDetail2VO> lst = res.getLst();
				if (lst != null && lst.size() > 0) {
					res.setSoID(lst.get(0).getSoID());
					res.setCustomerName(lst.get(0).getCustomerName());
					res.setCustomerAddress(lst.get(0).getCustomerAddress());
					res.setDeliveryCode(lst.get(0).getDeliveryCode());
					res.setDeliveryName(lst.get(0).getDeliveryName());
					res.setStaffCode(lst.get(0).getStaffCode());
					res.setStaffName(lst.get(0).getStaffName());
					res.setOrderNumber(lst.get(0).getOrderNumber());
					res.setOrderDate(lst.get(0).getOrderDate());
					res.setDiscount(lst.get(0).getDiscount());
					BigDecimal total = BigDecimal.ZERO;
					for (RptSaleOrderDetail2VO child : lst) {
						if (child.getAmount() != null) {
							total = total.add(child.getAmount());
						}
					}
					res.setTotal(total);
				}
			}
			return lstRes;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptExImStockOfStaffLv01VO> getRptExImStockOfStaff(Long shopId, List<Long> listStaffId, Date fromDate, Date toDate) throws BusinessException {
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException(ExceptionCode.SHOP_ID_IS_NULL);
		}
		try {
			// neu ko co danh sach nhan vien ban hang, lay het thong tin nhan
			// vien
			// ban hang trong shop
			if (null == listStaffId || listStaffId.size() <= 0) {
				List<StaffObjectType> listStaffType = new ArrayList<StaffObjectType>();
				listStaffType.add(StaffObjectType.NVBH);
				listStaffType.add(StaffObjectType.NVVS);

				List<Staff> listStaff = this.staffDAO.getListStaffByShopId(null, shopId, ActiveType.RUNNING, listStaffType);

				if (null == listStaff || listStaff.size() <= 0) {
					throw new IllegalArgumentException(ExceptionCode.LIST_STAFF_ID_IS_NULL);
				} else {
					listStaffId = new ArrayList<Long>();
					for (Staff item : listStaff) {
						listStaffId.add(item.getId());
					}
				}
			}

			List<RptExImStockOfStaffDataVO> listData = this.staffDAO.getRptExImStockOfStaff(shopId, listStaffId, fromDate, toDate);

			if (null != listData && listData.size() > 0) {
				return RptExImStockOfStaffDataVO.groupByCusAndCat(listData);
			} else {
				return null;
			}
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptCustomerByRoutingLv01VO> getRptCustomerByRouting(Long shopId, String lstStaffCode, String lstCustomerCode, String customerName, Long staffId, Integer flagCMS) throws BusinessException {
		if (null == shopId) {
			throw new IllegalArgumentException(ExceptionCode.SHOP_ID_IS_NULL);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BAOCAO_DSKHTTBH(?, :shopId, :lstStaffCode, :lstCustomerCode, :customerName, :staffIdRoot, :flagCMS)");
			List<Object> inParams = new ArrayList<Object>();
			inParams.add(2);
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(java.sql.Types.NUMERIC);
			inParams.add(3);
			inParams.add(lstStaffCode == null ? "" : lstStaffCode);
			inParams.add(java.sql.Types.VARCHAR);
			inParams.add(4);
			inParams.add(lstCustomerCode == null ? "" : lstCustomerCode);
			inParams.add(java.sql.Types.VARCHAR);
			inParams.add(5);
			inParams.add(customerName);
			inParams.add(java.sql.Types.VARCHAR);
			inParams.add(6);
			inParams.add(staffId);
			inParams.add(java.sql.Types.NUMERIC);
			inParams.add(7);
			inParams.add(flagCMS);
			inParams.add(java.sql.Types.NUMERIC);

			List<RptCustomerByRoutingDataVO> lstDataDetail = repo.getListByQueryDynamicFromPackage(RptCustomerByRoutingDataVO.class, sql.toString(), inParams, null);
			//List<RptCustomerByRoutingDataVO> lstDataDetail = repo.getListByNamedQuery(RptCustomerByRoutingDataVO.class, "PKG_SHOP_REPORT.BAOCAO_DSKHTTBH", inParams);

			List<RptCustomerByRoutingLv01VO> result = new ArrayList<RptCustomerByRoutingLv01VO>();
			if (null != lstDataDetail && lstDataDetail.size() > 0) {
				Map<String, List<RptCustomerByRoutingDataVO>> mapData = GroupUtility.collectionToMap(lstDataDetail, "staffId");
				for (String keyMapDataStaffId : mapData.keySet()) {
					List<RptCustomerByRoutingDataVO> lstDetailOneKey = mapData.get(keyMapDataStaffId);
					RptCustomerByRoutingLv01VO voLv1 = new RptCustomerByRoutingLv01VO();
					voLv1.setListData(lstDetailOneKey);
					voLv1.setStaffId(lstDetailOneKey.get(0).getStaffId());
					voLv1.setStaffCode(lstDetailOneKey.get(0).getStaffCode());
					voLv1.setStaffName(lstDetailOneKey.get(0).getStaffName());
					voLv1.setStatus(lstDetailOneKey.get(0).getStaffStatus());
					result.add(voLv1);
				}
			}
			return result;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}

	}

	@Override
	public List<RptDeliveryAndPaymentOfStaffVO> getListDeliveryAndPaymentOfStaff(Long shopId, Date fromDate, Date toDate, List<Long> deliveryStaffId) throws BusinessException {
		List<RptDeliveryAndPaymentOfStaffVO> result = new ArrayList<RptDeliveryAndPaymentOfStaffVO>();

		try {
			// neu khong ton tai thi lay het danh sach nhan vien ban hang cua
			// shop
			if (null == deliveryStaffId || deliveryStaffId.size() <= 0) {
				List<StaffObjectType> listStaffType = new ArrayList<StaffObjectType>();
				listStaffType.add(StaffObjectType.NVGH);

				List<Staff> listStaff = this.staffDAO.getListStaffByShopId(null, shopId, ActiveType.RUNNING, listStaffType);

				if (null == listStaff || listStaff.size() <= 0) {
					throw new IllegalArgumentException(ExceptionCode.DELIVERY_STAFF_IS_NULL);
				} else {
					deliveryStaffId = new ArrayList<Long>();
					for (Staff item : listStaff) {
						deliveryStaffId.add(item.getId());
					}
				}
			}

			for (Long staffId : deliveryStaffId) {
				if (null == staffId) {
					throw new IllegalArgumentException("staffId is null");
				}
				Staff staff = this.staffDAO.getStaffById(staffId);
				if (null == staff) {
					throw new IllegalArgumentException("staff install is null");
				}
				if (staff.getShop().getId().compareTo(shopId) != 0) {
					throw new IllegalArgumentException("staff not in shop");
				}

				List<OrderType> orderType = new ArrayList<OrderType>();
				orderType.add(OrderType.IN);
				orderType.add(OrderType.TT);

				List<SaleOrderVO> listSaleOrder = this.saleOrderDAO.getListSaleOrderVOByDelivery(null, shopId, orderType, SaleOrderStatus.APPROVED, SaleOrderType.NOT_YET_RETURNED, fromDate, toDate, staffId);

				if (null != listSaleOrder && listSaleOrder.size() > 0) {
					for (SaleOrderVO saleOrder : listSaleOrder) {
						RptDeliveryAndPaymentOfStaffVO item = new RptDeliveryAndPaymentOfStaffVO();

						item.setAmount(saleOrder.getAmount());
						item.setCustomerAddress(saleOrder.getCustomerAddress());
						item.setCustomerCode(saleOrder.getCustomerCode());
						item.setCustomerName(saleOrder.getCustomerName());
						item.setDeliveryName(saleOrder.getDeliveryName());
						item.setShortCode(saleOrder.getShortCode());
						item.setStaffName(saleOrder.getStaffName());
						item.setCustomerPhoneNumber(saleOrder.getCustomerPhone());
						item.setDeliveryStaff(saleOrder.getDeliveryCode());
						item.setDiscount(saleOrder.getDiscount());
						if (null != saleOrder.getIsInvoice()) {
							item.setInvoice(saleOrder.getIsInvoice() > 0 ? true : false);
						}
						item.setInvoiceDate(saleOrder.getOrderDate());
						item.setInvoiceNumber(saleOrder.getOrderNumber());

						item.setSaleStaff(saleOrder.getStaffCode());
						item.setTotal(saleOrder.getTotal());

						//						List<RptSaleOrderLotVO> listProduct = this
						//								.getListSaleOrderByDeliveryStaff(shopId,
						//										staffId, fromDate, toDate, 0, false,
						//										saleOrder.getSaleOrderId());

						List<RptSaleOrderLotVO> listProduct = saleOrderDetailDAO.getListSaleOrderLotVOEx(null, shopId, staffId, fromDate, toDate, 0, false, saleOrder.getSaleOrderId());

						//						List<RptSaleOrderLotVO> listFreeProduct = this
						//								.getListSaleOrderByDeliveryStaff(shopId,
						//										staffId, fromDate, toDate, 1, true,
						//										saleOrder.getSaleOrderId());

						List<RptSaleOrderLotVO> listFreeProduct = saleOrderDetailDAO.getListSaleOrderLotVOEx(null, shopId, staffId, fromDate, toDate, 1, true, saleOrder.getSaleOrderId());

						item.setListProduct(listProduct);
						item.setListPromotionProduct(listFreeProduct);

						result.add(item);
					}
				}
			}

			return result;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/** SangTN: 7_1_3 */
	@Override
	public List<RptDeliveryAndPaymentOfStaffVO> getListDeliveryAndPaymentOfStaffUseProcedure(Long shopId, Date fromDate, Date toDate, List<Long> deliveryStaffId, Integer isPrint, Long staffIdRoot, Integer flagCMS)
			throws BusinessException {
		List<RptDeliveryAndPaymentOfStaffVO> result = new ArrayList<RptDeliveryAndPaymentOfStaffVO>();

		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}

		String lstDeliveryId = "";
		if (!deliveryStaffId.isEmpty()) {
			for (int i = 0; i < deliveryStaffId.size() - 1; i++) {
				lstDeliveryId += deliveryStaffId.get(i) + ",";
			}
			lstDeliveryId += deliveryStaffId.get(deliveryStaffId.size() - 1);
		}

		StringBuilder sql = new StringBuilder();
		sql.append("call PKG_SHOP_REPORT.BC_7_1_3_PGNVTT(?, :shopId, :lstDeliveryId, :fDate, :tDate, :isPrint, :staffIdRoot, :flagCMS)");
		List<Object> params = new ArrayList<Object>();
		params.add(2);
		params.add(shopId);
		params.add(Types.NUMERIC);
		params.add(3);
		params.add(lstDeliveryId);
		params.add(Types.VARCHAR);
		params.add(4);
		params.add(new java.sql.Timestamp(fromDate.getTime()));
		params.add(Types.TIMESTAMP);
		params.add(5);
		params.add(new java.sql.Timestamp(toDate.getTime()));
		params.add(Types.TIMESTAMP);
		params.add(6);
		params.add(isPrint);
		params.add(Types.NUMERIC);
		params.add(7);
		params.add(staffIdRoot);
		params.add(Types.NUMERIC);
		params.add(8);
		params.add(flagCMS);
		params.add(Types.NUMERIC);

		List<RptDeliveryAndPaymentVO> lstData = new ArrayList<RptDeliveryAndPaymentVO>();
		try {
			lstData = repo.getListByQueryDynamicFromPackage(RptDeliveryAndPaymentVO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

		RptDeliveryAndPaymentVO voDetail = new RptDeliveryAndPaymentVO();
		List<Long> lstSaleOrderId = new ArrayList<Long>();
		if (!lstData.isEmpty()) {
			for (int i = 0; i < lstData.size(); i++) {
				voDetail = lstData.get(i);
				voDetail.safeSetNull();
				RptSaleOrderLotVO voProduct = new RptSaleOrderLotVO();
				voProduct.setConvfact(voDetail.getConvfact());
				voProduct.setDiscountAmount(voDetail.getDiscountAmount());
				voProduct.setIsFreeItem(voDetail.getIsFreeItem());
				voProduct.setLot(voDetail.getLot());
				voProduct.setLotQuantity(voDetail.getLotQuantity());
				voProduct.setPrice(voDetail.getPrice());
				voProduct.setProductCode(voDetail.getProductCode());
				voProduct.setProductName(voDetail.getProductName());
				voProduct.setPromotionCode(voDetail.getPromotionCode());
				voProduct.setPromotionLotQuantity(voDetail.getPromotionLotQuantity());
				voProduct.setPromotionName(voDetail.getPromotionName());
				voProduct.setQuantity(voDetail.getQuantity());
				voProduct.setTotalWeight(voDetail.getTotalWeight());
				if (!lstSaleOrderId.contains(voDetail.getSaleOrderId())) {
					RptDeliveryAndPaymentOfStaffVO vo = new RptDeliveryAndPaymentOfStaffVO();
					List<RptSaleOrderLotVO> lstProduct = new ArrayList<RptSaleOrderLotVO>();
					List<RptSaleOrderLotVO> lstFreeProduct = new ArrayList<RptSaleOrderLotVO>();
					vo.setAmount(voDetail.getAmount());
					vo.setCustomerAddress(voDetail.getCustomerAddress());
					vo.setCustomerCode(voDetail.getCustomerCode());
					vo.setCustomerName(voDetail.getCustomerName());
					vo.setCustomerPhoneNumber(voDetail.getCustomerPhone());
					vo.setDeliveryName(voDetail.getDeliveryName());
					vo.setDeliveryStaff(voDetail.getDeliveryCode());
					vo.setDiscount(voDetail.getDiscount());
					vo.setInvoice(voDetail.getIsInvoice() > 0 ? true : false);
					vo.setInvoiceDate(voDetail.getOrderDate());
					vo.setInvoiceNumber(voDetail.getOrderNumber());
					vo.setSaleOrderId(voDetail.getSaleOrderId());
					vo.setListProduct(lstProduct);
					vo.setListPromotionProduct(lstFreeProduct);
					vo.setSaleStaff(voDetail.getStaffCode());
					vo.setShortCode(voDetail.getShortCode());
					vo.setStaffName(voDetail.getStaffName());
					vo.setTotal(voDetail.getTotal());
					vo.setTotalString(voDetail.getTotalString());
					if (voDetail.getIsFreeItem() == 0) {
						vo.getListProduct().add(voProduct);
					} else {
						vo.getListPromotionProduct().add(voProduct);
					}
					result.add(vo);
					lstSaleOrderId.add(voDetail.getSaleOrderId());
				} else {
					for (int iResult = 0; iResult < result.size(); iResult++) {
						if (result.get(iResult).getSaleOrderId().equals(voDetail.getSaleOrderId())) {
							if (voDetail.getIsFreeItem() == 0) {
								result.get(iResult).getListProduct().add(voProduct);
							} else {
								result.get(iResult).getListPromotionProduct().add(voProduct);
							}
						}
					}
				}
			}
		}
		if (!result.isEmpty()) {
			for (int i = 0; i < result.size(); i++) {
				BigDecimal weight = BigDecimal.ZERO;
				if (result.get(i).getListProduct() != null) {
					int sizeP = result.get(i).getListProduct().size();
					for (int j = 0; j < sizeP; j++) {
						weight = weight.add(result.get(i).getListProduct().get(j).getTotalWeight());
					}
				}
				result.get(i).setTotalWeight(weight);
			}
		}
		return result;
	}

	//7.1.3A
	@Override
	public List<RptDeliveryAndPaymentOfStaffVO> getListDeliveryAndPaymentOfStaffUseProcedureNotApproved(Long shopId, Date fromDate, Date toDate, List<Long> deliveryStaffId, Long staffRootId, Integer flagCMS) throws BusinessException {
		List<RptDeliveryAndPaymentOfStaffVO> result = new ArrayList<RptDeliveryAndPaymentOfStaffVO>();

		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}

		String lstDeliveryId = "";
		if (!deliveryStaffId.isEmpty()) {
			for (int i = 0; i < deliveryStaffId.size() - 1; i++) {
				lstDeliveryId += deliveryStaffId.get(i) + ",";
			}
			lstDeliveryId += deliveryStaffId.get(deliveryStaffId.size() - 1);
		}

		StringBuilder sql = new StringBuilder();
		sql.append("call PKG_SHOP_REPORT.BC_7_1_3_PGNVTT_A(?, :shopId, :lstDeliveryId, :fDate, :tDate, :staffRootId, :flagCMS)");
		List<Object> params = new ArrayList<Object>();
		params.add(2);
		params.add(shopId);
		params.add(Types.NUMERIC);
		params.add(3);
		params.add(lstDeliveryId);
		params.add(Types.VARCHAR);
		params.add(4);
		params.add(new java.sql.Timestamp(fromDate.getTime()));
		params.add(Types.TIMESTAMP);
		params.add(5);
		params.add(new java.sql.Timestamp(toDate.getTime()));
		params.add(Types.TIMESTAMP);
		params.add(6);
		params.add(staffRootId);
		params.add(Types.NUMERIC);
		params.add(7);
		params.add(flagCMS);
		params.add(Types.NUMERIC);

		List<RptDeliveryAndPaymentVO> lstData = new ArrayList<RptDeliveryAndPaymentVO>();
		try {
			lstData = repo.getListByQueryDynamicFromPackage(RptDeliveryAndPaymentVO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

		RptDeliveryAndPaymentVO voDetail = new RptDeliveryAndPaymentVO();
		List<Long> lstSaleOrderId = new ArrayList<Long>();
		if (!lstData.isEmpty()) {
			for (int i = 0; i < lstData.size(); i++) {
				voDetail = lstData.get(i);
				voDetail.safeSetNull();
				RptSaleOrderLotVO voProduct = new RptSaleOrderLotVO();
				voProduct.setConvfact(voDetail.getConvfact());
				voProduct.setDiscountAmount(voDetail.getDiscountAmount());
				voProduct.setIsFreeItem(voDetail.getIsFreeItem());
				voProduct.setLot(voDetail.getLot());
				voProduct.setLotQuantity(voDetail.getLotQuantity());
				voProduct.setPrice(voDetail.getPrice());
				voProduct.setProductCode(voDetail.getProductCode());
				voProduct.setProductName(voDetail.getProductName());
				voProduct.setPromotionCode(voDetail.getPromotionCode());
				voProduct.setPromotionLotQuantity(voDetail.getPromotionLotQuantity());
				voProduct.setPromotionName(voDetail.getPromotionName());
				voProduct.setQuantity(voDetail.getQuantity());
				voProduct.setTotalWeight(voDetail.getTotalWeight());
				if (!lstSaleOrderId.contains(voDetail.getSaleOrderId())) {
					RptDeliveryAndPaymentOfStaffVO vo = new RptDeliveryAndPaymentOfStaffVO();
					List<RptSaleOrderLotVO> lstProduct = new ArrayList<RptSaleOrderLotVO>();
					List<RptSaleOrderLotVO> lstFreeProduct = new ArrayList<RptSaleOrderLotVO>();
					vo.setAmount(voDetail.getAmount());
					vo.setCustomerAddress(voDetail.getCustomerAddress());
					vo.setCustomerCode(voDetail.getCustomerCode());
					vo.setCustomerName(voDetail.getCustomerName());
					vo.setCustomerPhoneNumber(voDetail.getCustomerPhone());
					vo.setDeliveryName(voDetail.getDeliveryName());
					vo.setDeliveryStaff(voDetail.getDeliveryCode());
					vo.setDiscount(voDetail.getDiscount());
					vo.setInvoice(voDetail.getIsInvoice() > 0 ? true : false);
					vo.setInvoiceDate(voDetail.getOrderDate());
					vo.setInvoiceNumber(voDetail.getOrderNumber());
					vo.setSaleOrderId(voDetail.getSaleOrderId());
					vo.setListProduct(lstProduct);
					vo.setListPromotionProduct(lstFreeProduct);
					vo.setSaleStaff(voDetail.getStaffCode());
					vo.setShortCode(voDetail.getShortCode());
					vo.setStaffName(voDetail.getStaffName());
					vo.setTotal(voDetail.getTotal());
					vo.setTotalString(voDetail.getTotalString());
					if (voDetail.getIsFreeItem() == 0) {
						vo.getListProduct().add(voProduct);
					} else {
						vo.getListPromotionProduct().add(voProduct);
					}
					result.add(vo);
					lstSaleOrderId.add(voDetail.getSaleOrderId());
				} else {
					for (int iResult = 0; iResult < result.size(); iResult++) {
						if (result.get(iResult).getSaleOrderId().equals(voDetail.getSaleOrderId())) {
							if (voDetail.getIsFreeItem() == 0) {
								result.get(iResult).getListProduct().add(voProduct);
							} else {
								result.get(iResult).getListPromotionProduct().add(voProduct);
							}
						}
					}
				}
			}
		}
		if (!result.isEmpty()) {
			for (int i = 0; i < result.size(); i++) {
				BigDecimal weight = BigDecimal.ZERO;
				BigDecimal discountAmount = BigDecimal.ZERO;
				BigDecimal discount = BigDecimal.ZERO;
				if (result.get(i).getListProduct() != null) {
					int sizeP = result.get(i).getListProduct().size();
					for (int j = 0; j < sizeP; j++) {
						weight = weight.add(result.get(i).getListProduct().get(j).getTotalWeight());
						discountAmount = discountAmount.add(result.get(i).getListProduct().get(j).getDiscountAmount());
						discount = discount.add(result.get(i).getListProduct().get(j).getDiscount());
					}
				}
				result.get(i).setTotalWeight(weight);
				result.get(i).setDiscountAmount(discountAmount);
				result.get(i).setDiscount(discount);
			}
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.report.ShopReportMgr#getListRptStrockTransInMove(java
	 * .lang.Long)
	 */
	@Override
	public List<RptStockTransInMoveVO> getListRptStrockTransInMove(Long stockTransId) throws BusinessException {

		try {
			return stockTransDetailDAO.getListRptStrockTransInMove(stockTransId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.report.ShopReportMgr#getListDebitPayment(java.util.Date,
	 * java.util.Date, java.lang.Long, java.lang.Long, java.lang.Integer)
	 */
	@Override
	public List<RptParentDebitPaymentVO> getListDebitPayment(Date fromDate, Date toDate, Long shopId, List<Long> staffIds, List<Long> customerIds, Integer type) throws BusinessException {

		try {
			List<RptParentDebitPaymentVO> result = new ArrayList<RptParentDebitPaymentVO>();
			List<RptDebitPaymentVO> list = debitDAO.getListDebitPayment(fromDate, toDate, shopId, staffIds, customerIds, type);
			String groupCode = "";
			RptParentDebitPaymentVO parentVO = null;
			// parse data
			for (RptDebitPaymentVO rptVO : list) {// NVBH
				if (type != null && type == 0) {
					if (!rptVO.getStaffCode().equals(groupCode)) {
						if (parentVO != null) {
							result.add(parentVO);
						}
						groupCode = rptVO.getStaffCode();
						parentVO = new RptParentDebitPaymentVO();
						parentVO.setGroupCode(rptVO.getStaffCode());
						parentVO.setGroupName(rptVO.getStaffName());
						parentVO.getList().add(rptVO);
					} else {
						parentVO.getList().add(rptVO);
					}
				} else {// KH
					if (!rptVO.getCustomerCode().equals(groupCode)) {
						if (parentVO != null) {
							result.add(parentVO);
						}
						groupCode = rptVO.getCustomerCode();
						parentVO = new RptParentDebitPaymentVO();
						parentVO.setGroupCode(rptVO.getShortCode());
						parentVO.setGroupName(rptVO.getCustomerName());
						parentVO.getList().add(rptVO);
					} else {
						parentVO.getList().add(rptVO);
					}
				}
				rptVO.setGroupCode(parentVO.getGroupCode());
				rptVO.setGroupName(parentVO.getGroupName());
			}
			if (parentVO != null) {
				result.add(parentVO);
			}
			return result;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.report.ShopReportMgr#getRptVisitPlan(ths.dms.core
	 * .entities.enumtype.KPaging, java.lang.Long, java.lang.String,
	 * java.util.Date, java.lang.Boolean)
	 */
	@Override
	public List<RptActionLogVO> getRptVisitPlan(Long shopId, String staffCode, String superCode, Date visitPlanDate, Boolean isHasChild) throws BusinessException {
		try {
			// call
			// PKG_SHOP_REPORT.TGGHOFNVBH_REPORT(?,:npp,:nvbh,:gsnpp,:ngay,:hasChild)
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("npp");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("nvbh");
			inParams.add(staffCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("gsnpp");
			inParams.add(superCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("ngay");
			inParams.add(visitPlanDate);
			inParams.add(StandardBasicTypes.DATE);

			if (Boolean.TRUE.equals(isHasChild)) {
				inParams.add("hasChild");
				inParams.add(1);
				inParams.add(StandardBasicTypes.INTEGER);
			} else {
				inParams.add("hasChild");
				inParams.add(0);
				inParams.add(StandardBasicTypes.INTEGER);
			}

			List<RptActionLogVO> lstResult = repo.getListByNamedQuery(RptActionLogVO.class, "PKG_SHOP_REPORT.TGGHOFNVBH_REPORT", inParams);
			for (RptActionLogVO rpt : lstResult) {
				try {
					rpt.safeSetNull();
				} catch (Exception e) {
				}
			}
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Bao cao thoi gian ghe tham nhan vien ban hang VT1
	 */
	@Override
	public List<RptBCVT1> getRptBCVT1(String shopId, String listGsnppCode, String listStaffCode, Date visitPlanDate) throws BusinessException {
		try {
			// call
			// @NamedNativeQuery(name = "PKG_SHOP_REPORT.BAOCAO_TGGTNVBH_VT1", resultSetMapping = "ths.core.entities.vo.rpt.RptBCVT1", query = "{ call PKG_SHOP_REPORT.BAOCAO_TGGTNVBH_VT1(?,:shopId,:lstStaffOwnerCode,:lstStaffSaleCode,:pDate)}", hints = { @QueryHint(name = "org.hibernate.callable", value = "true") }),   //vuongmq
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.STRING);

			/*
			 * inParams.add("lstStaffOwnerCode"); inParams.add(listGsnppCode);
			 * inParams.add(StandardBasicTypes.STRING);
			 */

			inParams.add("lstStaffSaleCode");
			inParams.add(listStaffCode);
			inParams.add(StandardBasicTypes.STRING);

			//SimpleDateFormat fm = new SimpleDateFormat("dd/MM/yyyy");

			inParams.add("pDate");
			inParams.add(visitPlanDate);
			//inParams.add(fm.format(visitPlanDate));
			inParams.add(StandardBasicTypes.DATE);

			List<RptBCVT1> lstResult = repo.getListByNamedQuery(RptBCVT1.class, "TULV2.BAOCAO_TGGTNVBH_VT1", inParams);
			for (RptBCVT1 rpt : lstResult) {
				rpt.safeSetNull();
			}
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	//shopId IN STRING, lstStaffOwnerCode IN STRING, lstStaffSaleCode IN STRING, pDate IN DATE, staffIdRoot in number, flagCMS in number) AS
	@Override
	public List<RptBCVT1> getRptBCVT12(String strListShopId, String staffOwnerCode, String staffSaleCode, Date visitPlanDateTmp, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		try {
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(strListShopId);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("lstStaffOwnerCode");
			inParams.add(staffOwnerCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("lstStaffSaleCode");
			inParams.add(staffSaleCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("pDate");
			inParams.add(visitPlanDateTmp);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("staffIdRoot");
			inParams.add(staffIdRoot);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("flagCMS");
			inParams.add(flagCMS);
			inParams.add(StandardBasicTypes.INTEGER);

			List<RptBCVT1> lstResult = repo.getListByNamedQuery(RptBCVT1.class, "PKG_SHOP_REPORT.BAOCAO_TGGTNVBH_VT1", inParams);
			for (RptBCVT1 rpt : lstResult) {
				rpt.safeSetNull();
			}
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.report.ShopReportMgr#getListStockTransWithCondition(
	 * ths.dms.core.entities.enumtype.KPaging,
	 * ths.dms.core.entities.enumtype.StockObjectType,
	 * ths.dms.core.entities.enumtype.StockObjectType, java.lang.Long,
	 * java.util.Date, java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<StockTrans> getListStockTransWithCondition(KPaging<StockTrans> kPaging, StockObjectType fromOwnerType, StockObjectType toOwnerType, Long staffId, Date stockTransDate, Long fromOwnerId, Long toOwnerId) throws BusinessException {

		try {
			return stockTransDAO.getListStockTransWithCondition(kPaging, fromOwnerType, toOwnerType, staffId, stockTransDate, fromOwnerId, toOwnerId);
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptPGNVTTGVO> getListRptPGNVTTGVOs(Long shopId, Long deliveryStaffId, Date fromDate, Date toDate) throws BusinessException {
		try {
			return saleOrderDAO.getPGNVTTG(shopId, deliveryStaffId, fromDate, toDate);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	//SangTN
	//	@Override	
	//	public List<RptBCXNKNVBHVO> getListRptBCXNKNVBHVOs(Long shopId, List<Long> lstStaffId, Date fromDate, Date toDate) throws BusinessException {
	//		try {
	//			return stockTransDAO.getBCXNKNVBH(shopId, lstStaffId, fromDate, toDate);
	//		}
	//		catch (DataAccessException ex) {
	//			throw new BusinessException(ex);
	//		}
	//	}
	//	@Override	
	//	public List<RptBCXNKNVBHVO> getListRptBCXNKNVBHVOs(Long shopId,
	//			String listStaffId, Date fromDate, Date toDate) throws BusinessException{
	//		//new result tra ve:
	//		List<RptBCXNKNVBHVO>  result = new ArrayList<RptBCXNKNVBHVO>();
	//		
	//		try {
	//		
	//			List<Object> inParams = new ArrayList<Object>();
	//			inParams.add("shopId");
	//			inParams.add(shopId == null ? 0 : shopId);
	//			inParams.add(StandardBasicTypes.LONG);
	//
	//			inParams.add("listStaffId");
	//			inParams.add(listStaffId);
	//			inParams.add(StandardBasicTypes.STRING);
	//
	//			inParams.add("fromDate");
	//			inParams.add(fromDate);
	//			inParams.add(StandardBasicTypes.DATE);
	//
	//			inParams.add("toDate");
	//			inParams.add(toDate);
	//			inParams.add(StandardBasicTypes.DATE);
	//			
	//			List<RptBCXNKNVBHDetailVO> lstRptBCXNKNVBHDetailVO = repo.getListByNamedQuery(RptBCXNKNVBHDetailVO.class, "PKG_SHOP_REPORT.BAOCAO_XNKNVBH", inParams);
	//			
	//			Map<String, List<RptBCXNKNVBHDetailVO>> map1 = GroupUtility.collectionToMap(String.class, lstRptBCXNKNVBHDetailVO, "stockTransDate");			
	//			for(String keyMap1StockTransDate: map1.keySet()){
	//				List<RptBCXNKNVBHDetailVO> elementMap1 = map1.get(keyMap1StockTransDate);
	//				RptBCXNKNVBHVO vo1 = new RptBCXNKNVBHVO();
	//				vo1.setStockTransDate(keyMap1StockTransDate);				
	//				ArrayList<RptBCXNKNVBHStaffVO> lstDataStaffFollowDate = new ArrayList<RptBCXNKNVBHStaffVO>();
	//				vo1.setLstDataStaffFollowDate(lstDataStaffFollowDate);
	//				
	//				Map<String, List<RptBCXNKNVBHDetailVO>> map2From1ElementOfMap1 = GroupUtility.collectionToMap(String.class, elementMap1, "staffCode");
	//				for(String keyMap2StaffCode:map2From1ElementOfMap1.keySet()){
	//					List<RptBCXNKNVBHDetailVO> elementMap2 = map2From1ElementOfMap1.get(keyMap2StaffCode);
	//					RptBCXNKNVBHStaffVO vo2 = new RptBCXNKNVBHStaffVO();
	//					vo2.setStaffCode(keyMap2StaffCode);
	//					vo2.setStaffId(elementMap2.size()>0 ? elementMap2.get(0).getStaffId():null);
	//					vo2.setStaffName(elementMap2.size()>0 ? elementMap2.get(0).getStaffName():null);
	//					vo2.setStockTransCode(elementMap2.size()>0 ? elementMap2.get(0).getStockTransCode():null);
	//			
	//					
	////					BigDecimal sumAmount = 0;
	////					for(int i = 0; i< elementMap2.size(); i++){
	////						sumAmount = elementMap2.get(i).getThanhTien();
	////					}
	//					
	//					//Gia tri sai,de tam thoi
	//					//vo2.setSumAmount(elementMap2.size()>0 ? elementMap2.get(0).getThanhTien():null);
	//					
	//					vo2.setLstDataProductFollowStaff(elementMap2);					
	//					vo1.setShopId(elementMap2.size()>0 ? elementMap2.get(0).getShopId():null);		
	//					
	//					lstDataStaffFollowDate.add(vo2);
	//				}				
	//				result.add(vo1);
	//			}						
	//			return result;
	//		} catch (Exception e) {
	//			throw new BusinessException(e);
	//		}
	//				
	//	}

	@Override
	public List<RptBCXNKNVBHVO2> getListRptBCXNKNVBHVOs(Long shopId, String listStaffId, Date fromDate, Date toDate, Integer typeBill) throws BusinessException {
		//new result tra ve:
		List<RptBCXNKNVBHVO2> result = new ArrayList<RptBCXNKNVBHVO2>();

		try {

			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("listStaffId");
			inParams.add(listStaffId);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptBCXNKNVBHDetailVO2> lstRptBCXNKNVBHDetailVO2 = null;
			List<String> lstGroupBy = new ArrayList<String>();
			Map<String, List<RptBCXNKNVBHDetailVO2>> map1 = null;
			if (typeBill == 1) {
				lstRptBCXNKNVBHDetailVO2 = repo.getListByNamedQuery(RptBCXNKNVBHDetailVO2.class, "PKG_SHOP_REPORT.BAOCAO_XNKNVBH_XUAT", inParams);
				lstGroupBy.add("stockTransDate");
				lstGroupBy.add("staffCode");
				lstGroupBy.add("stockTransCode");
				map1 = GroupUtility.collectionToHash2(lstRptBCXNKNVBHDetailVO2, lstGroupBy);

				for (String keyMap1StockTransDate : map1.keySet()) {
					List<RptBCXNKNVBHDetailVO2> elementMap1 = map1.get(keyMap1StockTransDate);
					RptBCXNKNVBHVO2 vo1 = new RptBCXNKNVBHVO2();
					String[] keyGroupBy = keyMap1StockTransDate.split("-", 3);
					vo1.setTitle("PHIẾU XUẤT KHO NH?�N VI??N B??N H?�NG - VANSALE");
					vo1.setStockTransDate(keyGroupBy[0]);
					vo1.setStaffCode(keyGroupBy[1]);
					vo1.setStockTransCode(keyGroupBy[2].substring(0, keyGroupBy[2].lastIndexOf("-")).trim());
					vo1.setShopId(elementMap1.get(0).getShopId());
					vo1.setStaffName(elementMap1.get(0).getStaffName());
					vo1.setStaffId(elementMap1.get(0).getStaffId());
					vo1.setLstDetail(elementMap1);

					result.add(vo1);
				}
			} else if (typeBill == 2) {
				lstRptBCXNKNVBHDetailVO2 = repo.getListByNamedQuery(RptBCXNKNVBHDetailVO2.class, "PKG_SHOP_REPORT.BAOCAO_XNKNVBH_NHAP", inParams);
				lstGroupBy.add("stockTransDate");
				lstGroupBy.add("staffCode");
				lstGroupBy.add("stockTransCode");
				map1 = GroupUtility.collectionToHash2(lstRptBCXNKNVBHDetailVO2, lstGroupBy);

				for (String keyMap1StockTransDate : map1.keySet()) {
					List<RptBCXNKNVBHDetailVO2> elementMap1 = map1.get(keyMap1StockTransDate);
					RptBCXNKNVBHVO2 vo1 = new RptBCXNKNVBHVO2();
					String[] keyGroupBy = keyMap1StockTransDate.split("-", 3);
					vo1.setTitle("PHIẾU NHẬP KHO NH?�N VI??N B??N H?�NG - VANSALE");
					vo1.setStockTransDate(keyGroupBy[0]);
					vo1.setStaffCode(keyGroupBy[1]);
					vo1.setStockTransCode(keyGroupBy[2].substring(0, keyGroupBy[2].lastIndexOf("-")).trim());
					vo1.setShopId(elementMap1.get(0).getShopId());
					vo1.setStaffName(elementMap1.get(0).getStaffName());
					vo1.setStaffId(elementMap1.get(0).getStaffId());
					vo1.setLstDetail(elementMap1);

					result.add(vo1);
				}
			} else {
				//lstRptBCXNKNVBHDetailVO2 = repo.getListByNamedQuery(RptBCXNKNVBHDetailVO2.class, "PKG_SHOP_REPORT.BAOCAO_XNKNVBH", inParams);
				lstRptBCXNKNVBHDetailVO2 = repo.getListByNamedQuery(RptBCXNKNVBHDetailVO2.class, "PKG_SHOP_REPORT.BAOCAO_XNKNVBH_XUAT", inParams);
				lstGroupBy = new ArrayList<String>();
				lstGroupBy.add("stockTransDate");
				lstGroupBy.add("staffCode");
				lstGroupBy.add("stockTransCode");
				map1 = GroupUtility.collectionToHash2(lstRptBCXNKNVBHDetailVO2, lstGroupBy);

				for (String keyMap1StockTransDate : map1.keySet()) {
					List<RptBCXNKNVBHDetailVO2> elementMap1 = map1.get(keyMap1StockTransDate);
					RptBCXNKNVBHVO2 vo1 = new RptBCXNKNVBHVO2();
					String[] keyGroupBy = keyMap1StockTransDate.split("-", 3);
					vo1.setTitle("PHIẾU XUẤT KHO NH?�N VI??N B??N H?�NG - VANSALE");
					vo1.setStockTransDate(keyGroupBy[0]);
					vo1.setStaffCode(keyGroupBy[1]);
					vo1.setStockTransCode(keyGroupBy[2].substring(0, keyGroupBy[2].lastIndexOf("-")).trim());
					vo1.setShopId(elementMap1.get(0).getShopId());
					vo1.setStaffName(elementMap1.get(0).getStaffName());
					vo1.setStaffId(elementMap1.get(0).getStaffId());
					vo1.setLstDetail(elementMap1);

					result.add(vo1);
				}

				lstRptBCXNKNVBHDetailVO2 = repo.getListByNamedQuery(RptBCXNKNVBHDetailVO2.class, "PKG_SHOP_REPORT.BAOCAO_XNKNVBH_NHAP", inParams);
				map1 = GroupUtility.collectionToHash2(lstRptBCXNKNVBHDetailVO2, lstGroupBy);

				for (String keyMap1StockTransDate : map1.keySet()) {
					List<RptBCXNKNVBHDetailVO2> elementMap1 = map1.get(keyMap1StockTransDate);
					RptBCXNKNVBHVO2 vo1 = new RptBCXNKNVBHVO2();
					String[] keyGroupBy = keyMap1StockTransDate.split("-", 3);
					vo1.setTitle("PHIẾU NHẬP KHO NH?�N VI??N B??N H?�NG - VANSALE");
					vo1.setStockTransDate(keyGroupBy[0]);
					vo1.setStaffCode(keyGroupBy[1]);
					vo1.setStockTransCode(keyGroupBy[2].substring(0, keyGroupBy[2].lastIndexOf("-")).trim());
					vo1.setShopId(elementMap1.get(0).getShopId());
					vo1.setStaffName(elementMap1.get(0).getStaffName());
					vo1.setStaffId(elementMap1.get(0).getStaffId());
					vo1.setLstDetail(elementMap1);

					result.add(vo1);
				}
			}
			return result;
		} catch (Exception e) {
			throw new BusinessException(e);
		}

	}

	@Override
	public List<RptBCTGGTCNVBH_1VO> getListRptBCTGGT_NVBH_1(String strListShopId, String listNVGSCode, String listNVBHCode, Date fromDate, Date toDate) throws BusinessException {
		if (null == strListShopId) {
			throw new IllegalArgumentException(ExceptionCode.LIST_SHOP_ID_IS_NULL);
		}
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		if (fromDate.after(toDate)) {
			throw new IllegalArgumentException(ExceptionCode.BEGIN_TIME_AFTER_END_TIME);
		}
		//return this.staffDAO.getListRptBCTGGT_NVBH_1(listShopId, listNVGSId, listNVBHId, fromDate, toDate);
		// vuong mq
		// date: 28/03/2014
		try {
			List<RptBCTGGTCNVBH_1VO> lstResult = new ArrayList<RptBCTGGTCNVBH_1VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BAOCAO_VT1_1(?, :lstShopId, :lstStaffOwnerCode, :lstStaffCode, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);

			params.add(3);
			params.add(listNVGSCode);
			params.add(java.sql.Types.VARCHAR);

			params.add(4);
			params.add(listNVBHCode);
			params.add(java.sql.Types.VARCHAR);

			// v?� truy�?n xuống ng?�y c?� HH24:MI n?�n d?�ng TIMESTAMP
			params.add(5);
			params.add(new java.sql.Timestamp(fromDate.getTime()));
			params.add(java.sql.Types.TIMESTAMP);

			//v?� truy�?n xuống ng?�y c?� HH24:MI n?�n d?�ng TIMESTAMP
			params.add(6);
			params.add(new java.sql.Timestamp(toDate.getTime()));
			params.add(java.sql.Types.TIMESTAMP);

			lstResult = repo.getListByQueryDynamicFromPackage(RptBCTGGTCNVBH_1VO.class, sql.toString(), params, null);

			for (RptBCTGGTCNVBH_1VO rs : lstResult) {
				rs.safeSetNull();
			}
			return lstResult;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptBCTGGTCNVBH_1VO> getListRptBCTGGT_NVBH_12(String strListShopId, String lstNVGSCode, String lstNVBHCode, Date fromDate, Date toDate, Long staffRootId, Integer flagCMS) throws BusinessException {
		// BAOCAO_VT1_1 lstShopId STRING, lstStaffOwnerCode string, lstStaffCode String, fromDate IN DATE, toDate IN DATE, staffIdRoot in number, flagCMS in number);
		if (null == strListShopId) {
			throw new IllegalArgumentException(ExceptionCode.LIST_SHOP_ID_IS_NULL);
		}
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		if (fromDate.after(toDate)) {
			throw new IllegalArgumentException(ExceptionCode.BEGIN_TIME_AFTER_END_TIME);
		}
		try {
			List<RptBCTGGTCNVBH_1VO> lstResult = new ArrayList<RptBCTGGTCNVBH_1VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.BAOCAO_VT1_1(?, :lstShopId, :lstStaffOwnerCode, :lstStaffCode, :fromDate, :toDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);

			params.add(3);
			params.add(lstNVGSCode);
			params.add(java.sql.Types.VARCHAR);

			params.add(4);
			params.add(lstNVBHCode);
			params.add(java.sql.Types.VARCHAR);

			// v?� truy�?n xuống ng?�y c?� HH24:MI n?�n d?�ng TIMESTAMP
			params.add(5);
			params.add(new java.sql.Timestamp(fromDate.getTime()));
			params.add(java.sql.Types.TIMESTAMP);

			//v?� truy�?n xuống ng?�y c?� HH24:MI n?�n d?�ng TIMESTAMP
			params.add(6);
			params.add(new java.sql.Timestamp(toDate.getTime()));
			params.add(java.sql.Types.TIMESTAMP);

			params.add(7);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);

			params.add(8);
			params.add(flagCMS);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(RptBCTGGTCNVBH_1VO.class, sql.toString(), params, null);

			for (RptBCTGGTCNVBH_1VO rs : lstResult) {
				rs.safeSetNull();
			}
			return lstResult;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new BusinessException(e);
		}
	}

	public List<RptDSKHTMHCustomerAndListInfoVO> getDSKHTMHReport2(Long shopId, Long customerId, Date fromDate, Date toDate) throws BusinessException {
		try {
			//new result tra ve:
			List<RptDSKHTMHCustomerAndListInfoVO> result = new ArrayList<RptDSKHTMHCustomerAndListInfoVO>();
			//
			//lay record tu DAO:
			List<RptDSKHTMHRecordProductVO> listRptDSKHTMHRecordProductVO = saleOrderDAO.getListRptDSKHTMHRecordProductVO(shopId, customerId, fromDate, toDate);
			//tao map 1:
			Map<String, List<RptDSKHTMHRecordProductVO>> map1 = GroupUtility.collectionToMap(listRptDSKHTMHRecordProductVO, "customerCode");
			//for qua map 1, set data vao vo1:
			for (String keyMap1CustomerCode : map1.keySet()) {
				//lay element cua for
				List<RptDSKHTMHRecordProductVO> elementMap1ListRptDSKHTMHRecordProductVO = map1.get(keyMap1CustomerCode);
				//new vo1 va set data vao vo1:
				RptDSKHTMHCustomerAndListInfoVO vo1 = new RptDSKHTMHCustomerAndListInfoVO();
				vo1.setCustomerCode(keyMap1CustomerCode);
				//				vo1.setCustomerName(customerName);
				//				vo1.setSumMoneyForCustomer(sumMoneyForCustomer);
				//				vo1.setSumQuantityDisplayForCustomer(sumQuantityDisplayForCustomer);
				//				vo1.setSumQuantitySaleForCustomer(sumQuantitySaleForCustomer);
				ArrayList<RptDSKHTMHCatAndListInfoVO> listRptDSKHTMHCatAndListInfoVOForVO1 = new ArrayList<RptDSKHTMHCatAndListInfoVO>();
				vo1.setListStock(listRptDSKHTMHCatAndListInfoVOForVO1);
				Map<String, List<RptDSKHTMHRecordProductVO>> map2From1ElementOfMap1 = GroupUtility.collectionToMap(elementMap1ListRptDSKHTMHRecordProductVO, "catCode");
				for (String keyMap2catCode : map2From1ElementOfMap1.keySet()) {
					//lay element cua for
					List<RptDSKHTMHRecordProductVO> elementMap2ListRptDSKHTMHRecordProductVO = map2From1ElementOfMap1.get(keyMap2catCode);
					//new vo2 va set data vao vo2:
					RptDSKHTMHCatAndListInfoVO vo2 = new RptDSKHTMHCatAndListInfoVO();
					vo2.setCatCode(keyMap2catCode);
					//					vo2.setSumMoneyForCategory(sumMoneyForCategory);
					//					vo2.setSumQuantityDisplayForCategory(sumQuantityDisplayForCategory);
					//					vo2.setSumQuantitySaleForCategory(sumQuantitySaleForCategory);
					List<RptDSKHTMHRecordProductVO> listRptDSKHTMHRecordProductVOForVO2 = elementMap2ListRptDSKHTMHRecordProductVO;
					vo2.setListDetail(listRptDSKHTMHRecordProductVOForVO2);
					//vo2.setSumMoneyForCategory(listRptDSKHTMHRecordProductVOForVO2.size()>0 ? listRptDSKHTMHRecordProductVOForVO2.get(0).getSumMoneyForCategory():null);
					//vo2.setSumQuantityDisplayForCategory(listRptDSKHTMHRecordProductVOForVO2.size()>0 ? listRptDSKHTMHRecordProductVOForVO2.get(0).getSumQuantityDisplayForCategory():null);
					//vo2.setSumQuantitySaleForCategory(listRptDSKHTMHRecordProductVOForVO2.size()>0 ? listRptDSKHTMHRecordProductVOForVO2.get(0).getSumQuantitySaleForCategory():null);

					listRptDSKHTMHCatAndListInfoVOForVO1.add(vo2);

					vo1.setCustomerName(listRptDSKHTMHRecordProductVOForVO2.size() > 0 ? listRptDSKHTMHRecordProductVOForVO2.get(0).getCustomerName() : null);
					//vo1.setSumMoneyForCustomer(listRptDSKHTMHRecordProductVOForVO2.size()>0 ? listRptDSKHTMHRecordProductVOForVO2.get(0).getSumMoneyForCustomer():null);
					//vo1.setSumQuantityDisplayForCustomer(listRptDSKHTMHRecordProductVOForVO2.size()>0 ? listRptDSKHTMHRecordProductVOForVO2.get(0).getSumQuantityDisplayForCustomer():null);
					//vo1.setSumQuantitySaleForCustomer(listRptDSKHTMHRecordProductVOForVO2.size()>0 ? listRptDSKHTMHRecordProductVOForVO2.get(0).getSumQuantitySaleForCustomer():null);

				}//xong for la xong data for vo1
				result.add(vo1);
			}

			return result;
		} catch (Exception e) {
			throw new BusinessException(e);
		}

	}

	@Override
	public List<RptDSKHTMHCustomerAndListInfoVO> getDSKHTMHReport(Long shopId, String lstCustomerCode, String lstCategoryCode, String lstProductCode, Date fromDate, Date toDate) throws BusinessException {
		List<RptDSKHTMHCustomerAndListInfoVO> res = new ArrayList<RptDSKHTMHCustomerAndListInfoVO>();
		List<RptDSKHTMHRecordProductVO> listRptDSKHTMHRecordProductVO;
		try {
			//listRptDSKHTMHRecordProductVO = saleOrderDAO.getListRptDSKHTMHRecordProductVO(shopId, customerId,fromDate,toDate);
			List<Object> inParams = new ArrayList<Object>();
			inParams.add(2);
			inParams.add(shopId);
			inParams.add(java.sql.Types.NUMERIC);
			inParams.add(3);
			inParams.add(lstCustomerCode);
			inParams.add(java.sql.Types.VARCHAR);
			inParams.add(4);
			inParams.add(lstCategoryCode);
			inParams.add(java.sql.Types.VARCHAR);
			inParams.add(5);
			inParams.add(lstProductCode);
			inParams.add(java.sql.Types.VARCHAR);
			inParams.add(6);
			inParams.add(new java.sql.Date(fromDate.getTime()));
			inParams.add(java.sql.Types.DATE);
			inParams.add(7);
			inParams.add(new java.sql.Date(toDate.getTime()));
			inParams.add(java.sql.Types.DATE);
			String sql = "{call PKG_SHOP_REPORT.DSKHTMH3_7(?, ?, ?, ?, ?, ?, ?)}";

			listRptDSKHTMHRecordProductVO = repo.getListByQueryDynamicFromPackage(RptDSKHTMHRecordProductVO.class, sql, inParams, null);

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

		Map<String, Object[]> moreInfoForCustomer = new HashMap<String, Object[]>();
		Map<String, Map<String, RptDSKHTMHCatAndListInfoVO>> mapTestCustomer = new TreeMap<String, Map<String, RptDSKHTMHCatAndListInfoVO>>();
		if (listRptDSKHTMHRecordProductVO != null && listRptDSKHTMHRecordProductVO.size() > 0) {
			for (RptDSKHTMHRecordProductVO child : listRptDSKHTMHRecordProductVO) {
				if (!StringUtility.isNullOrEmpty(child.getCustomerCode())) {
					if (mapTestCustomer.containsKey(child.getCustomerCode())) {//co trong MAP roi thi lay ra, add them
						String keyCustomer = child.getCustomerCode();
						Map<String, RptDSKHTMHCatAndListInfoVO> oldMapTestCat = mapTestCustomer.get(keyCustomer);
						//Lay ra customer do roi, gio kiem tra xem thuoc nganh hang nao.
						if (oldMapTestCat.containsKey(child.getCatCode())) {
							//Neu co nganh hang do roi, lay ra, add sp vao
							String oldCat = child.getCatCode();
							RptDSKHTMHCatAndListInfoVO a = oldMapTestCat.get(oldCat);
							a.getListDetail().add(child);

							//tinh tong cho category
							a.setSumQuantitySaleForCategory(a.getSumQuantitySaleForCategory().add(child.getQuantitySale()));
							a.setSumQuantityDisplayForCategory(a.getSumQuantityDisplayForCategory().add(child.getQuantityDisplay()));
							a.setSumMoneyForCategory(a.getSumMoneyForCategory().add(child.getMoney()));
						} else {
							//chua co nganh hang thi add them nganh hang:
							String tenNganhHangMoi = child.getCatCode();
							RptDSKHTMHCatAndListInfoVO rptCatVONewValue = new RptDSKHTMHCatAndListInfoVO();
							rptCatVONewValue.setCatCode(tenNganhHangMoi);
							rptCatVONewValue.setListDetail(new ArrayList<RptDSKHTMHRecordProductVO>());
							rptCatVONewValue.getListDetail().add(child);
							rptCatVONewValue.setSumMoneyForCategory(BigDecimal.ZERO);
							rptCatVONewValue.setSumQuantityDisplayForCategory(BigDecimal.ZERO);
							rptCatVONewValue.setSumQuantitySaleForCategory(BigDecimal.ZERO);

							//tinh tong cho category
							rptCatVONewValue.setSumQuantitySaleForCategory(rptCatVONewValue.getSumQuantitySaleForCategory().add(child.getQuantitySale()));
							rptCatVONewValue.setSumQuantityDisplayForCategory(rptCatVONewValue.getSumQuantityDisplayForCategory().add(child.getQuantityDisplay()));
							rptCatVONewValue.setSumMoneyForCategory(rptCatVONewValue.getSumMoneyForCategory().add(child.getMoney()));
							//put nganh hang vao
							oldMapTestCat.put(tenNganhHangMoi, rptCatVONewValue);
						}
					} else {//chua co customer do: new moi key+value
						String customerNewKey = child.getCustomerCode();
						Map<String, RptDSKHTMHCatAndListInfoVO> mapTestCatNewValue = new TreeMap<String, RptDSKHTMHCatAndListInfoVO>();
						//						mapTestCustomer.put(customerNewKey, mapTestCatNewValue);// (*)
						//set du lieu moi vao mapTestCat
						String catNewKey = child.getCatCode();
						RptDSKHTMHCatAndListInfoVO rptCatVONewValue = new RptDSKHTMHCatAndListInfoVO();
						rptCatVONewValue.setCatCode(catNewKey);
						rptCatVONewValue.setListDetail(new ArrayList<RptDSKHTMHRecordProductVO>());
						rptCatVONewValue.getListDetail().add(child);
						rptCatVONewValue.setSumMoneyForCategory(BigDecimal.ZERO);
						rptCatVONewValue.setSumQuantityDisplayForCategory(BigDecimal.ZERO);
						rptCatVONewValue.setSumQuantitySaleForCategory(BigDecimal.ZERO);

						//tinh tong cho category
						rptCatVONewValue.setSumQuantitySaleForCategory(rptCatVONewValue.getSumQuantitySaleForCategory().add(child.getQuantitySale()));
						rptCatVONewValue.setSumQuantityDisplayForCategory(rptCatVONewValue.getSumQuantityDisplayForCategory().add(child.getQuantityDisplay()));
						rptCatVONewValue.setSumMoneyForCategory(rptCatVONewValue.getSumMoneyForCategory().add(child.getMoney()));
						mapTestCatNewValue.put(catNewKey, rptCatVONewValue);

						mapTestCustomer.put(customerNewKey, mapTestCatNewValue);//put vi tri nay tot hon vi tri (*)

						//add more info:
						Object[] arrayMoreInfo = new Object[3];
						arrayMoreInfo[0] = child.getCustomerName();
						arrayMoreInfo[1] = child.getCustomerHouseNumber();
						arrayMoreInfo[2] = child.getCustomerStreet();
						//						arrayMoreInfo[1]=child.getSumQuantitySaleForCustomer();
						//						arrayMoreInfo[2]=child.getSumQuantityDisplayForCustomer();
						//						arrayMoreInfo[3]=child.getSumMoneyForCustomer();
						moreInfoForCustomer.put(customerNewKey, arrayMoreInfo);
					}
				}
			}
			//Xong for:
			//Xong phan lay du lieu tu DB va luu vao Map. Gio do du lieu tu Map ra VO chuan.
			for (String key : mapTestCustomer.keySet()) {
				Map<String, RptDSKHTMHCatAndListInfoVO> value = mapTestCustomer.get(key);
				RptDSKHTMHCustomerAndListInfoVO vo = new RptDSKHTMHCustomerAndListInfoVO();
				//set info 
				vo.setCustomerCode(key);
				//set more info:
				Object[] arrayMoreInfoForCustomer = moreInfoForCustomer.get(key);
				vo.setCustomerName((String) arrayMoreInfoForCustomer[0]);
				vo.setCustomerAddress((String) arrayMoreInfoForCustomer[1] + "-" + (String) arrayMoreInfoForCustomer[2]);
				vo.setSumQuantitySaleForCustomer(BigDecimal.ZERO);
				vo.setSumQuantityDisplayForCustomer(BigDecimal.ZERO);
				vo.setSumMoneyForCustomer(BigDecimal.ZERO);
				//set info from map "value"
				for (String childKey : value.keySet()) {
					RptDSKHTMHCatAndListInfoVO voCat = value.get(childKey);
					vo.getListStock().add(voCat);
					vo.setSumQuantitySaleForCustomer(vo.getSumQuantitySaleForCustomer().add(voCat.getSumQuantitySaleForCategory()));
					vo.setSumQuantityDisplayForCustomer(vo.getSumQuantityDisplayForCustomer().add(voCat.getSumQuantityDisplayForCategory()));
					vo.setSumMoneyForCustomer(vo.getSumMoneyForCustomer().add(voCat.getSumMoneyForCategory()));
				}
				res.add(vo);
			}
		}
		return res;
		//staff tuong ung voi customer cua m?�nh. Nganh hang cat ung voi area.
		//staffMap === customerMap
	}

	/**
	 * Bao cao 7.2.2 theo san pham
	 * 
	 * @author phuongvm
	 */
	@Override
	public List<Rpt7_2_2_NVBHVO> getRpt7_2_2SanPham(Long shopId, String listStaffId, Date fromDate, Date toDate, Long staffRootId, Integer hasCheckStaff) throws BusinessException {
		List<Rpt7_2_2_NVBHVO> res = new ArrayList<Rpt7_2_2_NVBHVO>();
		List<Rpt7_2_2_SanPhamVO> listRpt7_2_2_SanPhamVO;
		try {
			List<Object> inParams = new ArrayList<Object>();
			inParams.add(2);
			inParams.add(shopId);
			inParams.add(java.sql.Types.NUMERIC);
			inParams.add(3);
			inParams.add(listStaffId);
			inParams.add(java.sql.Types.VARCHAR);
			inParams.add(4);
			inParams.add(new java.sql.Date(fromDate.getTime()));
			inParams.add(java.sql.Types.DATE);
			inParams.add(5);
			inParams.add(new java.sql.Date(toDate.getTime()));
			inParams.add(java.sql.Types.DATE);
			inParams.add(6);
			inParams.add(staffRootId);
			inParams.add(java.sql.Types.NUMERIC);
			inParams.add(7);
			inParams.add(hasCheckStaff);
			inParams.add(java.sql.Types.NUMERIC);
			String sql = "{call PKG_SHOP_REPORT.BC_7_2_2_SanPham(?, ?, ?, ?, ?, ?, ?)}";

			listRpt7_2_2_SanPhamVO = repo.getListByQueryDynamicFromPackage(Rpt7_2_2_SanPhamVO.class, sql, inParams, null);

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

		Map<String, Object[]> moreInfoForCustomer = new HashMap<String, Object[]>();
		Map<String, Map<String, Rpt7_2_2_NganhHangVO>> mapTestCustomer = new TreeMap<String, Map<String, Rpt7_2_2_NganhHangVO>>();
		if (listRpt7_2_2_SanPhamVO != null && listRpt7_2_2_SanPhamVO.size() > 0) {
			for (Rpt7_2_2_SanPhamVO child : listRpt7_2_2_SanPhamVO) {
				if (!StringUtility.isNullOrEmpty(child.getMaNVBH())) {
					if (mapTestCustomer.containsKey(child.getMaNVBH())) {//co trong MAP roi thi lay ra, add them
						String keyCustomer = child.getMaNVBH();
						Map<String, Rpt7_2_2_NganhHangVO> oldMapTestCat = mapTestCustomer.get(keyCustomer);
						//Lay ra customer do roi, gio kiem tra xem thuoc nganh hang nao.
						if (oldMapTestCat.containsKey(child.getMaNganhHang())) {
							//Neu co nganh hang do roi, lay ra, add sp vao
							String oldCat = child.getMaNganhHang();
							Rpt7_2_2_NganhHangVO a = oldMapTestCat.get(oldCat);
							a.getListRpt7_2_2_SanPhamVO().add(child);

							//tinh tong cho category
							a.setTongSL(a.getTongSL().add(child.getSoLuong()));
							a.setTongTien(a.getTongTien().add(child.getThanhTien()));
						} else {
							//chua co nganh hang thi add them nganh hang:
							String tenNganhHangMoi = child.getMaNganhHang();
							Rpt7_2_2_NganhHangVO rptCatVONewValue = new Rpt7_2_2_NganhHangVO();
							rptCatVONewValue.setMaNganhHang(tenNganhHangMoi);
							rptCatVONewValue.setListRpt7_2_2_SanPhamVO(new ArrayList<Rpt7_2_2_SanPhamVO>());
							rptCatVONewValue.getListRpt7_2_2_SanPhamVO().add(child);

							rptCatVONewValue.setTongSL(BigDecimal.ZERO);
							rptCatVONewValue.setTongTien(BigDecimal.ZERO);
							//tinh tong cho category
							rptCatVONewValue.setTongSL(rptCatVONewValue.getTongSL().add(child.getSoLuong()));
							rptCatVONewValue.setTongTien(rptCatVONewValue.getTongTien().add(child.getThanhTien()));
							//put nganh hang vao
							oldMapTestCat.put(tenNganhHangMoi, rptCatVONewValue);
						}
					} else {//chua co customer do: new moi key+value
						String customerNewKey = child.getMaNVBH();
						Map<String, Rpt7_2_2_NganhHangVO> mapTestCatNewValue = new TreeMap<String, Rpt7_2_2_NganhHangVO>();
						//						mapTestCustomer.put(customerNewKey, mapTestCatNewValue);// (*)
						//set du lieu moi vao mapTestCat
						String catNewKey = child.getMaNganhHang();
						Rpt7_2_2_NganhHangVO rptCatVONewValue = new Rpt7_2_2_NganhHangVO();
						rptCatVONewValue.setMaNganhHang(catNewKey);
						rptCatVONewValue.setListRpt7_2_2_SanPhamVO(new ArrayList<Rpt7_2_2_SanPhamVO>());
						rptCatVONewValue.getListRpt7_2_2_SanPhamVO().add(child);
						rptCatVONewValue.setTongSL(BigDecimal.ZERO);
						rptCatVONewValue.setTongTien(BigDecimal.ZERO);

						//tinh tong cho category
						rptCatVONewValue.setTongSL(rptCatVONewValue.getTongSL().add(child.getSoLuong()));
						rptCatVONewValue.setTongTien(rptCatVONewValue.getTongTien().add(child.getThanhTien()));
						mapTestCatNewValue.put(catNewKey, rptCatVONewValue);

						mapTestCustomer.put(customerNewKey, mapTestCatNewValue);//put vi tri nay tot hon vi tri (*)

						//add more info:
						Object[] arrayMoreInfo = new Object[3];
						arrayMoreInfo[0] = child.getTenNVBH();
						//						arrayMoreInfo[1]=child.getCustomerHouseNumber();
						//						arrayMoreInfo[2]=child.getCustomerStreet();
						moreInfoForCustomer.put(customerNewKey, arrayMoreInfo);
					}
				}
			}
			//Xong for:
			//Xong phan lay du lieu tu DB va luu vao Map. Gio do du lieu tu Map ra VO chuan.
			for (String key : mapTestCustomer.keySet()) {
				Map<String, Rpt7_2_2_NganhHangVO> value = mapTestCustomer.get(key);
				Rpt7_2_2_NVBHVO vo = new Rpt7_2_2_NVBHVO();
				//set info 
				vo.setMaNVBH(key);
				//set more info:
				Object[] arrayMoreInfoForCustomer = moreInfoForCustomer.get(key);
				vo.setTenNVBH((String) arrayMoreInfoForCustomer[0]);
				//				vo.setCustomerAddress((String)arrayMoreInfoForCustomer[1]+"-"+(String)arrayMoreInfoForCustomer[2]);
				vo.setTongSL(BigDecimal.ZERO);
				vo.setTongTien(BigDecimal.ZERO);
				//set info from map "value"
				for (String childKey : value.keySet()) {
					Rpt7_2_2_NganhHangVO voCat = value.get(childKey);
					vo.getListRpt7_2_2_NganhHangVO().add(voCat);
					vo.setTongSL(vo.getTongSL().add(voCat.getTongSL()));
					vo.setTongTien(vo.getTongTien().add(voCat.getTongTien()));
				}
				res.add(vo);
			}
		}
		return res;
		//staff tuong ung voi customer cua m?�nh. Nganh hang cat ung voi area.
		//staffMap === customerMap
	}

	/**
	 * Bao cao doanh thu tong hop cua nhan vien
	 * 
	 * @author tungtt
	 */
	@Override
	public List<RptDTTHNVStaffAndListInfoVO> getDTTHNVReport(Long shopId, String listStaffId, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<RptDTTHNVStaffAndListInfoVO> lstRes = new ArrayList<RptDTTHNVStaffAndListInfoVO>();
			//List<RptDTTHNVRecordVO> listRecord = saleOrderDAO.getDTTHNVReport(shopId, listStaffId, fromDate, toDate);

			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("listStaffId");
			inParams.add(listStaffId);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptDTTHNVRecordVO> listRecord = repo.getListByNamedQuery(RptDTTHNVRecordVO.class, "PKG_SHOP_REPORT.BAOCAO_DTTHNV", inParams);

			Map<String, RptDTTHNVStaffAndListInfoVO> mapStaff = new HashMap<String, RptDTTHNVStaffAndListInfoVO>();
			List<String> lstStaffCode = new ArrayList<String>();

			if (listRecord != null && listRecord.size() > 0) {
				for (RptDTTHNVRecordVO child : listRecord) {
					if (mapStaff.get(child.getStaffCode()) != null) {
						RptDTTHNVStaffAndListInfoVO rpt = mapStaff.get(child.getStaffCode());
						rpt.getListDetail().add(child);
					} else {
						RptDTTHNVStaffAndListInfoVO rpt = new RptDTTHNVStaffAndListInfoVO();
						rpt.setListDetail(new ArrayList<RptDTTHNVRecordVO>());
						rpt.getListDetail().add(child);
						mapStaff.put(child.getStaffCode(), rpt);
						lstStaffCode.add(child.getStaffCode());
					}
				}
				for (String staffCode : lstStaffCode) {
					lstRes.add(mapStaff.get(staffCode));
				}
			}

			for (RptDTTHNVStaffAndListInfoVO res : lstRes) {
				List<RptDTTHNVRecordVO> lst = res.getListDetail();
				if (lst != null && lst.size() > 0) {
					res.setStaffCode(lst.get(0).getStaffCode());
					res.setStaffName(lst.get(0).getStaffName());

					BigDecimal revenue = BigDecimal.ZERO;
					for (RptDTTHNVRecordVO child : lst) {
						if (child.getRevenueForDay() != null) {
							revenue = revenue.add(child.getRevenueForDay());
						}
					}
					res.setRevenueForStaff(revenue);

					BigDecimal money = BigDecimal.ZERO;
					for (RptDTTHNVRecordVO child : lst) {
						if (child.getMoneyForDay() != null) {
							money = money.add(child.getMoneyForDay());
							//money = money + child.getMoneyForDay();
						}
					}
					res.setMoneyForStaff(money);

					BigDecimal remain = BigDecimal.ZERO;
					for (RptDTTHNVRecordVO child : lst) {
						if (child.getRemainForDay() != null) {
							remain = remain.add(child.getRemainForDay());
						}
					}
					res.setRemainForStaff(remain);

					BigDecimal discount = BigDecimal.ZERO;
					for (RptDTTHNVRecordVO child : lst) {
						if (child.getDiscountForDay() != null) {
							discount = discount.add(child.getDiscountForDay());
						}
					}
					res.setDiscountForStaff(discount);

					BigDecimal promotionMoney = BigDecimal.ZERO;
					for (RptDTTHNVRecordVO child : lst) {
						if (child.getPromotionMoneyForDay() != null) {
							promotionMoney = promotionMoney.add(child.getPromotionMoneyForDay());
						}
					}
					res.setPromotionMoneyForStaff(promotionMoney);

					BigDecimal promotionStock = BigDecimal.ZERO;
					for (RptDTTHNVRecordVO child : lst) {
						if (child.getPromotionStockForDay() != null) {
							promotionStock = promotionStock.add(child.getPromotionStockForDay());
						}
					}
					res.setPromotionStockForStaff(promotionStock);

					BigDecimal salesForStaff = BigDecimal.ZERO;
					for (RptDTTHNVRecordVO child : lst) {
						if (child.getSalesForDay() != null) {
							salesForStaff = salesForStaff.add(child.getSalesForDay());
						}
					}
					res.setSalesForStaff(salesForStaff);
				}
			}

			return lstRes;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	//vuongmq
	public List<RptDSTMCTTBVO> getDSTMCTTBReport(Long shopId, Integer pYear, String lstDisplayProgram, String lstCustomer) throws BusinessException {
		try {
			List<RptDSTMCTTBVO> lstResult = new ArrayList<RptDSTMCTTBVO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BAOCAO_DTBH_DSTMCTTB(?, :shopId, :pYear, :lstDisplayProgram, :lstCustomer)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);

			params.add(3);
			params.add(pYear);
			params.add(java.sql.Types.NUMERIC);

			params.add(4);
			params.add(lstDisplayProgram);
			params.add(java.sql.Types.VARCHAR);

			params.add(5);
			params.add(lstCustomer);
			params.add(java.sql.Types.VARCHAR);

			lstResult = repo.getListByQueryDynamicFromPackage(RptDSTMCTTBVO.class, sql.toString(), params, null);
			for (RptDSTMCTTBVO rs : lstResult) {
				rs.safeSetNull();
			}
			return lstResult;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new BusinessException(e);
		}
	}

	//namlb
	@Override
	public List<RptDTBHTNTNVBHStaffInfoVO> getDTBHTNTNVBHReport(Long shopId, String listStaffId, Date fromDate, Date toDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		List<RptDTBHTNTNVBHStaffInfoVO> res = new ArrayList<RptDTBHTNTNVBHStaffInfoVO>();
		List<RptDTBHTNTHVBHRecordOrderVO> listRptDTBHTNTHVBHRecordOrderVO;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BAOCAO_DTBHTNTNVBH(?, :shopId, :listStaffId, :fromDate,  :toDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(Types.NUMERIC);
			params.add(3);
			params.add(StringUtility.isNullOrEmpty(listStaffId) ? "" : listStaffId);
			params.add(Types.VARCHAR);
			params.add(4);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(Types.DATE);
			params.add(5);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(Types.DATE);
			params.add(6);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			params.add(7);
			params.add(flagCMS);
			params.add(Types.NUMERIC);

			listRptDTBHTNTHVBHRecordOrderVO = repo.getListByQueryDynamicFromPackage(RptDTBHTNTHVBHRecordOrderVO.class, sql.toString(), params, null);

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

		Map<String, Object[]> moreInfoForStaff = new HashMap<String, Object[]>();
		Map<String, Map<String, RptDTBHTNTNVBHDateInfoVO>> mapTestStaff = new TreeMap<String, Map<String, RptDTBHTNTNVBHDateInfoVO>>();
		Map<String, String> mapStaffName = new TreeMap<String, String>();
		if (listRptDTBHTNTHVBHRecordOrderVO != null && listRptDTBHTNTHVBHRecordOrderVO.size() > 0) {
			for (RptDTBHTNTHVBHRecordOrderVO child : listRptDTBHTNTHVBHRecordOrderVO) {
				if (!StringUtility.isNullOrEmpty(child.getStaffCode().toString())) {
					if (mapTestStaff.containsKey(child.getStaffCode())) {//co trong MAP roi thi lay ra, add them
						String keyStaff = child.getStaffCode().toString();
						Map<String, RptDTBHTNTNVBHDateInfoVO> oldMapTestDate = mapTestStaff.get(keyStaff);
						//Lay ra customer do roi, gio kiem tra xem thuoc nganh hang nao.
						if (oldMapTestDate.containsKey(child.getDateOrder())) {
							//Neu co nganh hang do roi, lay ra, add sp vao
							String oldDate = child.getDateOrder();
							RptDTBHTNTNVBHDateInfoVO a = oldMapTestDate.get(oldDate);
							//a.getListRptDSKHTMHRecordProductVO().add(child);
							a.getListRptDTBHTNTHVBHRecordOrderVO().add(child);
						} else {
							//chua co nganh hang thi add them nganh hang:
							String ngayMoi = child.getDateOrder();
							RptDTBHTNTNVBHDateInfoVO rptDateVONewValue = new RptDTBHTNTNVBHDateInfoVO();
							rptDateVONewValue.setDateOrder(ngayMoi);
							rptDateVONewValue.setStaffCode(child.getStaffCode());
							rptDateVONewValue.setStaffName(child.getStaffName());
							//rptDateVONewValue.setListRptDSKHTMHRecordProductVO(new ArrayList<RptDSKHTMHRecordProductVO>());
							rptDateVONewValue.setListRptDTBHTNTHVBHRecordOrderVO(new ArrayList<RptDTBHTNTHVBHRecordOrderVO>());
							//rptDateVONewValue.getListRptDSKHTMHRecordProductVO().add(child);
							rptDateVONewValue.getListRptDTBHTNTHVBHRecordOrderVO().add(child);

							rptDateVONewValue.setSumCashDate(child.getSumCashDate());
							rptDateVONewValue.setSumDebitDate(child.getSumDebitDate());
							rptDateVONewValue.setSumDiscountDate(child.getSumDiscountDate());
							rptDateVONewValue.setSumMoneyDate(child.getSumMoneyDate());
							rptDateVONewValue.setSumMoneyDiscountDate(child.getSumMoneyDiscountDate());
							rptDateVONewValue.setSumProductDiscountDate(child.getSumProductDiscountDate());
							rptDateVONewValue.setSumRevenueDate(child.getSumRevenueDate());
							rptDateVONewValue.setSumSKUDate(child.getSumSKUDate());
							//put nganh hang vao
							//oldMapTestCat.put(tenNganhHangMoi, rptCatVONewValue);
							oldMapTestDate.put(ngayMoi, rptDateVONewValue);
						}
					} else {//chua co customer do: new moi key+value
						String staffNewKey = child.getStaffCode().toString();
						Map<String, RptDTBHTNTNVBHDateInfoVO> mapTestDateNewValue = new TreeMap<String, RptDTBHTNTNVBHDateInfoVO>();
						//set du lieu moi vao mapTestCat
						String dateNewKey = child.getDateOrder();
						RptDTBHTNTNVBHDateInfoVO rptDateVONewValue = new RptDTBHTNTNVBHDateInfoVO();
						rptDateVONewValue.setStaffCode(child.getstaffCode());
						rptDateVONewValue.setStaffName(child.getStaffName());
						rptDateVONewValue.setDateOrder(dateNewKey);
						rptDateVONewValue.setListRptDTBHTNTHVBHRecordOrderVO(new ArrayList<RptDTBHTNTHVBHRecordOrderVO>());
						rptDateVONewValue.getListRptDTBHTNTHVBHRecordOrderVO().add(child);
						rptDateVONewValue.setSumRevenueDate(child.getSumRevenueDate());
						rptDateVONewValue.setSumMoneyDiscountDate(child.getSumMoneyDiscountDate());
						rptDateVONewValue.setSumProductDiscountDate(child.getSumProductDiscountDate());
						rptDateVONewValue.setSumMoneyDate(child.getSumMoneyDate());
						rptDateVONewValue.setSumDiscountDate(child.getSumDiscountDate());
						rptDateVONewValue.setSumSKUDate(child.getSumSKUDate());
						rptDateVONewValue.setSumDebitDate(child.getSumDebitDate());
						rptDateVONewValue.setSumCashDate(child.getSumCashDate());
						//mapTestCatNewValue.put(catNewKey, rptCatVONewValue);
						mapTestDateNewValue.put(dateNewKey, rptDateVONewValue);
						//mapTestCustomer.put(customerNewKey, mapTestCatNewValue);//put vi tri nay tot hon vi tri (*)
						mapTestStaff.put(staffNewKey, mapTestDateNewValue);
						mapStaffName.put(staffNewKey, child.getStaffName());
						//add more info:
						Object[] arrayMoreInfo = new Object[8];
						arrayMoreInfo[0] = child.getSumCashStaff();
						arrayMoreInfo[1] = child.getSumDebitStaff();
						arrayMoreInfo[2] = child.getSumDiscountStaff();
						arrayMoreInfo[3] = child.getSumMoneyStaff();
						arrayMoreInfo[4] = child.getSumMoneyDiscountStaff();
						arrayMoreInfo[5] = child.getSumProductDiscountStaff();
						arrayMoreInfo[6] = child.getSumRevenueStaff();
						arrayMoreInfo[7] = child.getSumSKUStaff();
						moreInfoForStaff.put(staffNewKey, arrayMoreInfo);
					}

				}
			}
			//Xong phan lay du lieu tu DB va luu vao Map. Gio do du lieu tu Map ra VO chuan.
			for (String key : mapTestStaff.keySet()) {
				Map<String, RptDTBHTNTNVBHDateInfoVO> value = mapTestStaff.get(key);
				String staffName = mapStaffName.get(key);
				RptDTBHTNTNVBHStaffInfoVO vo = new RptDTBHTNTNVBHStaffInfoVO();
				//set info 
				vo.setStaffCode(key);
				vo.setStaffName(staffName);
				//set more info:
				Object[] arrayMoreInfoForStaff = moreInfoForStaff.get(key);
				vo.setSumCashStaff((BigDecimal) arrayMoreInfoForStaff[0]);
				vo.setSumDebitStaff((BigDecimal) arrayMoreInfoForStaff[1]);
				vo.setSumDiscountStaff((BigDecimal) arrayMoreInfoForStaff[2]);
				vo.setSumMoneyStaff((BigDecimal) arrayMoreInfoForStaff[3]);
				vo.setSumMoneyDiscountStaff((BigDecimal) arrayMoreInfoForStaff[4]);
				vo.setSumProductDiscountStaff((BigDecimal) arrayMoreInfoForStaff[5]);
				vo.setSumRevenueStaff((BigDecimal) arrayMoreInfoForStaff[6]);
				vo.setSumSKUStaff((BigDecimal) arrayMoreInfoForStaff[7]);
				for (String childKey : value.keySet()) {
					RptDTBHTNTNVBHDateInfoVO voDate = value.get(childKey);
					vo.getListRptDTBHTNTNVBHDateInfoVO().add(voDate);
				}
				res.add(vo);
			}
		}
		return res;
	}

	//phuongvm 7.2.2 bao cao doanh thu chi tiet ban hang theo nvbh 
	@Override
	public List<RptDTBHTNTNVBHStaffInfo_7_2_2VO> getDTBHTNTNVBH_7_2_2Report(Long shopId, String listStaffId, Date fromDate, Date toDate, Long staffRootId, Integer hasCheckStaff) throws BusinessException {
		List<RptDTBHTNTNVBHStaffInfo_7_2_2VO> res = new ArrayList<RptDTBHTNTNVBHStaffInfo_7_2_2VO>();
		List<RptDTBHTNTHVBHRecordOrder_7_2_2VO> listRptDTBHTNTHVBHRecordOrderVO;
		try {
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId == null ? 0 : shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(listStaffId);
			params.add(java.sql.Types.VARCHAR);
			params.add(4);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(5);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(6);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(7);
			params.add(hasCheckStaff);
			params.add(java.sql.Types.NUMERIC);
			listRptDTBHTNTHVBHRecordOrderVO = repo.getListByQueryDynamicFromPackage(RptDTBHTNTHVBHRecordOrder_7_2_2VO.class, "call PKG_SHOP_REPORT.BAOCAO_DTBHTNTNVBH_7_2_2(?, :shopId, :listStaffId, :fromDate, :toDate, :staffIdRoot, :flagCMS)", params, null);

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

		Map<String, Object[]> moreInfoForStaff = new HashMap<String, Object[]>();
		Map<String, Map<String, RptDTBHTNTNVBHDateInfo_7_2_2VO>> mapTestStaff = new TreeMap<String, Map<String, RptDTBHTNTNVBHDateInfo_7_2_2VO>>();
		Map<String, String> mapStaffName = new TreeMap<String, String>();
		if (listRptDTBHTNTHVBHRecordOrderVO != null && listRptDTBHTNTHVBHRecordOrderVO.size() > 0) {
			for (RptDTBHTNTHVBHRecordOrder_7_2_2VO child : listRptDTBHTNTHVBHRecordOrderVO) {
				if (!StringUtility.isNullOrEmpty(child.getStaffCode().toString())) {
					if (mapTestStaff.containsKey(child.getStaffCode())) {//co trong MAP roi thi lay ra, add them
						String keyStaff = child.getStaffCode().toString();
						Map<String, RptDTBHTNTNVBHDateInfo_7_2_2VO> oldMapTestDate = mapTestStaff.get(keyStaff);
						//Lay ra customer do roi, gio kiem tra xem thuoc nganh hang nao.
						if (oldMapTestDate.containsKey(child.getDateOrder())) {
							//Neu co nganh hang do roi, lay ra, add sp vao
							String oldDate = child.getDateOrder();
							RptDTBHTNTNVBHDateInfo_7_2_2VO a = oldMapTestDate.get(oldDate);
							//a.getListRptDSKHTMHRecordProductVO().add(child);
							a.getListRptDTBHTNTHVBHRecordOrderVO().add(child);
						} else {
							//chua co nganh hang thi add them nganh hang:
							String ngayMoi = child.getDateOrder();
							RptDTBHTNTNVBHDateInfo_7_2_2VO rptDateVONewValue = new RptDTBHTNTNVBHDateInfo_7_2_2VO();
							rptDateVONewValue.setDateOrder(ngayMoi);
							rptDateVONewValue.setStaffCode(child.getStaffCode());
							rptDateVONewValue.setStaffName(child.getStaffName());
							//rptDateVONewValue.setListRptDSKHTMHRecordProductVO(new ArrayList<RptDSKHTMHRecordProductVO>());
							rptDateVONewValue.setListRptDTBHTNTHVBHRecordOrderVO(new ArrayList<RptDTBHTNTHVBHRecordOrder_7_2_2VO>());
							//rptDateVONewValue.getListRptDSKHTMHRecordProductVO().add(child);
							rptDateVONewValue.getListRptDTBHTNTHVBHRecordOrderVO().add(child);

							rptDateVONewValue.setSumCashDate(child.getSumCashDate());
							rptDateVONewValue.setSumDebitDate(child.getSumDebitDate());
							rptDateVONewValue.setSumDiscountDate(child.getSumDiscountDate());
							rptDateVONewValue.setSumMoneyDate(child.getSumMoneyDate());
							rptDateVONewValue.setSumMoneyDiscountDate(child.getSumMoneyDiscountDate());
							rptDateVONewValue.setSumProductDiscountDate(child.getSumProductDiscountDate());
							rptDateVONewValue.setSumRevenueDate(child.getSumRevenueDate());
							rptDateVONewValue.setSumSKUDate(child.getSumSKUDate());
							rptDateVONewValue.setCountSoDHDate(child.getCountSoDHDate());
							//put nganh hang vao
							//oldMapTestCat.put(tenNganhHangMoi, rptCatVONewValue);
							oldMapTestDate.put(ngayMoi, rptDateVONewValue);
						}
					} else {//chua co customer do: new moi key+value
						String staffNewKey = child.getStaffCode().toString();
						Map<String, RptDTBHTNTNVBHDateInfo_7_2_2VO> mapTestDateNewValue = new TreeMap<String, RptDTBHTNTNVBHDateInfo_7_2_2VO>();
						//set du lieu moi vao mapTestCat
						String dateNewKey = child.getDateOrder();
						RptDTBHTNTNVBHDateInfo_7_2_2VO rptDateVONewValue = new RptDTBHTNTNVBHDateInfo_7_2_2VO();
						rptDateVONewValue.setStaffCode(child.getstaffCode());
						rptDateVONewValue.setStaffName(child.getStaffName());
						rptDateVONewValue.setDateOrder(dateNewKey);
						rptDateVONewValue.setListRptDTBHTNTHVBHRecordOrderVO(new ArrayList<RptDTBHTNTHVBHRecordOrder_7_2_2VO>());
						rptDateVONewValue.getListRptDTBHTNTHVBHRecordOrderVO().add(child);
						rptDateVONewValue.setSumRevenueDate(child.getSumRevenueDate());
						rptDateVONewValue.setSumMoneyDiscountDate(child.getSumMoneyDiscountDate());
						rptDateVONewValue.setSumProductDiscountDate(child.getSumProductDiscountDate());
						rptDateVONewValue.setSumMoneyDate(child.getSumMoneyDate());
						rptDateVONewValue.setSumDiscountDate(child.getSumDiscountDate());
						rptDateVONewValue.setSumSKUDate(child.getSumSKUDate());
						rptDateVONewValue.setSumDebitDate(child.getSumDebitDate());
						rptDateVONewValue.setSumCashDate(child.getSumCashDate());
						rptDateVONewValue.setCountSoDHDate(child.getCountSoDHDate());
						//mapTestCatNewValue.put(catNewKey, rptCatVONewValue);
						mapTestDateNewValue.put(dateNewKey, rptDateVONewValue);
						//mapTestCustomer.put(customerNewKey, mapTestCatNewValue);//put vi tri nay tot hon vi tri (*)
						mapTestStaff.put(staffNewKey, mapTestDateNewValue);
						mapStaffName.put(staffNewKey, child.getStaffName());
						//add more info:
						Object[] arrayMoreInfo = new Object[9];
						arrayMoreInfo[0] = child.getSumCashStaff();
						arrayMoreInfo[1] = child.getSumDebitStaff();
						arrayMoreInfo[2] = child.getSumDiscountStaff();
						arrayMoreInfo[3] = child.getSumMoneyStaff();
						arrayMoreInfo[4] = child.getSumMoneyDiscountStaff();
						arrayMoreInfo[5] = child.getSumProductDiscountStaff();
						arrayMoreInfo[6] = child.getSumRevenueStaff();
						arrayMoreInfo[7] = child.getSumSKUStaff();
						arrayMoreInfo[8] = child.getCountSoDHStaff();
						moreInfoForStaff.put(staffNewKey, arrayMoreInfo);
					}

				}
			}
			//Xong phan lay du lieu tu DB va luu vao Map. Gio do du lieu tu Map ra VO chuan.
			for (String key : mapTestStaff.keySet()) {
				Map<String, RptDTBHTNTNVBHDateInfo_7_2_2VO> value = mapTestStaff.get(key);
				String staffName = mapStaffName.get(key);
				RptDTBHTNTNVBHStaffInfo_7_2_2VO vo = new RptDTBHTNTNVBHStaffInfo_7_2_2VO();
				//set info 
				vo.setStaffCode(key);
				vo.setStaffName(staffName);
				//set more info:
				Object[] arrayMoreInfoForStaff = moreInfoForStaff.get(key);
				vo.setSumCashStaff((BigDecimal) arrayMoreInfoForStaff[0]);
				vo.setSumDebitStaff((BigDecimal) arrayMoreInfoForStaff[1]);
				vo.setSumDiscountStaff((BigDecimal) arrayMoreInfoForStaff[2]);
				vo.setSumMoneyStaff((BigDecimal) arrayMoreInfoForStaff[3]);
				vo.setSumMoneyDiscountStaff((BigDecimal) arrayMoreInfoForStaff[4]);
				vo.setSumProductDiscountStaff((BigDecimal) arrayMoreInfoForStaff[5]);
				vo.setSumRevenueStaff((BigDecimal) arrayMoreInfoForStaff[6]);
				vo.setSumSKUStaff((BigDecimal) arrayMoreInfoForStaff[7]);
				vo.setCountSoDHStaff((BigDecimal) arrayMoreInfoForStaff[8]);
				for (String childKey : value.keySet()) {
					RptDTBHTNTNVBHDateInfo_7_2_2VO voDate = value.get(childKey);
					vo.getListRptDTBHTNTNVBHDateInfoVO().add(voDate);
				}
				res.add(vo);
			}
		}
		return res;
	}

	// namlb
	/**
	 * 7.1.8 Bao cao Phieu Thu Tra Cua Khach Hang
	 * 
	 * @author hunglm16
	 * @author October 08,2014
	 * @description Update Phan Quyen CMS
	 * */
	@Override
	public List<RptPTHCKHVO> getPTHCKH(Long shopId, String lstStaffCode, Date fromDate, Date toDate, String customerCode, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}

		try {
			List<RptPTHCKHVO> res = new ArrayList<RptPTHCKHVO>();

			List<RptPTHCKHProductVO> listRptPTHCKHProductVO;
			try {
				StringBuilder sql = new StringBuilder();
				sql.append("call PKG_SHOP_REPORT.BAOCAO_PTHCKH(?, :shopId, :lstStaffCode, :customerCode, :fromDate, :toDate, :staffIdRoot, :flagCMS)");
				List<Object> params = new ArrayList<Object>();
				params.add(2);
				params.add(shopId);
				params.add(java.sql.Types.NUMERIC);
				params.add(3);
				params.add(lstStaffCode);
				params.add(java.sql.Types.VARCHAR);
				params.add(4);
				params.add(customerCode);
				params.add(java.sql.Types.VARCHAR);
				params.add(5);
				params.add(new java.sql.Date(fromDate.getTime()));
				params.add(java.sql.Types.DATE);
				params.add(6);
				params.add(new java.sql.Date(toDate.getTime()));
				params.add(java.sql.Types.DATE);
				params.add(7);
				params.add(staffIdRoot);
				params.add(java.sql.Types.NUMERIC);
				params.add(8);
				params.add(flagCMS);
				params.add(java.sql.Types.NUMERIC);
				listRptPTHCKHProductVO = repo.getListByQueryDynamicFromPackage(RptPTHCKHProductVO.class, sql.toString(), params, null);
			} catch (DataAccessException e) {
				throw new BusinessException(e);
			}

			if (listRptPTHCKHProductVO != null && listRptPTHCKHProductVO.size() > 0) {
				Map<BigDecimal, RptPTHCKHVO> mapOrder = new TreeMap<BigDecimal, RptPTHCKHVO>();
				for (RptPTHCKHProductVO child : listRptPTHCKHProductVO) {
					if (mapOrder.containsKey(child.getSoId())) {
						RptPTHCKHVO tmp = mapOrder.get(child.getSoId());
						tmp.getListRptPTHCKHProductVO().add(child);
						mapOrder.put(child.getSoId(), tmp);
					} else {
						RptPTHCKHVO tmp = new RptPTHCKHVO();
						tmp.setListRptPTHCKHProductVO(new ArrayList<RptPTHCKHProductVO>());
						tmp.getListRptPTHCKHProductVO().add(child);
						mapOrder.put(child.getSoId(), tmp);
					}
				}

				for (BigDecimal soId : mapOrder.keySet()) {
					RptPTHCKHVO rptPTHCKH = new RptPTHCKHVO();
					rptPTHCKH.setListRptPTHCKHProductVO(mapOrder.get(soId).getListRptPTHCKHProductVO());
					if (rptPTHCKH.getListRptPTHCKHProductVO() != null && rptPTHCKH.getListRptPTHCKHProductVO().size() > 0) {
						RptPTHCKHProductVO child = rptPTHCKH.getListRptPTHCKHProductVO().get(0);
						rptPTHCKH.setAmount(child.getAmount());
						String customerString = "";
						if (!StringUtility.isNullOrEmpty(child.getShortCode())) {
							customerString = child.getShortCode();
						}
						if (!StringUtility.isNullOrEmpty(child.getCustomerName())) {
							if (!StringUtility.isNullOrEmpty(customerString)) {
								customerString += " - " + child.getCustomerName();
							}
						}
						if (!StringUtility.isNullOrEmpty(child.getMobiphone())) {
							if (!StringUtility.isNullOrEmpty(customerString)) {
								customerString += " S�?T: " + child.getMobiphone();
							}
						}
						if (!StringUtility.isNullOrEmpty(child.getCustomerAddress())) {
							rptPTHCKH.setCustomerAddress(child.getCustomerAddress());
						} else {
							rptPTHCKH.setCustomerAddress("");
						}
						String deliveryName = "";
						if (!StringUtility.isNullOrEmpty(child.getDeliveryName())) {
							deliveryName = child.getDeliveryName();
						}
						if (!StringUtility.isNullOrEmpty(child.getDeliveryCode())) {
							if (!StringUtility.isNullOrEmpty(deliveryName)) {
								deliveryName += " - " + child.getDeliveryCode();
							}
						}
						rptPTHCKH.setCustomerName(customerString);
						//rptPTHCKH.setDeliveryCode(child.getDeliveryCode());
						rptPTHCKH.setDeliveryName(deliveryName);
						rptPTHCKH.setOrderDate(child.getOrderDate());
						rptPTHCKH.setOrderNumber(child.getOrderNumber());
						//rptPTHCKH.setPhone(child.getPhone());
						rptPTHCKH.setRootInvoiceNumber(child.getRootOrderNumber());
						//rptPTHCKH.setStaffCode(child.getStaffCode());
						String staffName = "";
						if (!StringUtility.isNullOrEmpty(child.getStaffName())) {
							staffName = child.getStaffName();
						}
						if (!StringUtility.isNullOrEmpty(child.getStaffCode())) {
							if (!StringUtility.isNullOrEmpty(staffName)) {
								staffName += " - " + child.getStaffCode();
							}
						}
						rptPTHCKH.setStaffName(staffName);
						rptPTHCKH.setTotalPay(child.getTotalPay());
						//rptPTHCKH.setShortCode(child.getShortCode());
						BigDecimal disCountAmount = BigDecimal.ZERO;
						BigDecimal disCountOder = BigDecimal.ZERO;
						for (RptPTHCKHProductVO voChid : rptPTHCKH.getListRptPTHCKHProductVO()) {
							disCountAmount = disCountAmount.add(voChid.getDiscountAmount());
							disCountOder = disCountOder.add(voChid.getDiscountOrder());
						}
						rptPTHCKH.setDiscountAmount(disCountAmount);
						rptPTHCKH.setDiscountOrder(disCountOder);
					}
					res.add(rptPTHCKH);
				}
			}
			return res;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author namlb
	 * 
	 * */
	@Override
	public List<RptBKCTCTMHPurchaseOrderVO> getRptBKCTCTMHPurchaseOrderVO(Long shopId, Date fromDate, Date toDate) throws BusinessException {
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		try {
			List<RptBKCTCTMHPurchaseOrderVO> res = new ArrayList<RptBKCTCTMHPurchaseOrderVO>();
			if (shopId != null) {
				Shop s = shopDAO.getShopById(shopId);
				if (s != null) {
					List<RptBKCTCTMHProductRecordVO> listRptBKCTCTMHProductRecordVO = new ArrayList<RptBKCTCTMHProductRecordVO>();
					List<Object> inParams = new ArrayList<Object>();
					inParams.add("shopId");
					inParams.add(shopId == null ? 0 : shopId);
					inParams.add(StandardBasicTypes.LONG);

					inParams.add("fromDate");
					inParams.add(fromDate);
					inParams.add(StandardBasicTypes.DATE);

					inParams.add("toDate");
					inParams.add(toDate);
					inParams.add(StandardBasicTypes.DATE);

					listRptBKCTCTMHProductRecordVO = repo.getListByNamedQuery(RptBKCTCTMHProductRecordVO.class, "PKG_SHOP_REPORT.BAOCAO_BKCTCTMH", inParams);

					//Map<Long, RptPTHVO> rptPTHMap = new HashMap<Long, RptPTHVO>();

					if (listRptBKCTCTMHProductRecordVO != null && listRptBKCTCTMHProductRecordVO.size() > 0) {
						Map<String, RptBKCTCTMHPurchaseOrderVO> mapPurchaseOrder = new TreeMap<String, RptBKCTCTMHPurchaseOrderVO>();

						for (RptBKCTCTMHProductRecordVO child : listRptBKCTCTMHProductRecordVO) {
							if (mapPurchaseOrder.containsKey(child.getPurchaseOrder())) {
								RptBKCTCTMHPurchaseOrderVO tmp = mapPurchaseOrder.get(child.getPurchaseOrder());
								tmp.getListRptBKCTCTMHProductRecordVO().add(child);
								mapPurchaseOrder.put(child.getPurchaseOrder(), tmp);
							} else {
								RptBKCTCTMHPurchaseOrderVO tmp = new RptBKCTCTMHPurchaseOrderVO();
								//tmp.setListRptPTHCKHProductVO(new ArrayList<RptPTHCKHProductVO>());
								tmp.setListRptBKCTCTMHProductRecordVO(new ArrayList<RptBKCTCTMHProductRecordVO>());
								//tmp.getListRptPTHCKHProductVO().add(child);
								tmp.getListRptBKCTCTMHProductRecordVO().add(child);
								mapPurchaseOrder.put(child.getPurchaseOrder(), tmp);
							}
						}

						for (String purchaseOrder : mapPurchaseOrder.keySet()) {
							RptBKCTCTMHPurchaseOrderVO rpt = new RptBKCTCTMHPurchaseOrderVO();
							//rpt.setListRptPTHCKHProductVO(mapPurchaseOrder.get(soId).getListRptPTHCKHProductVO());
							rpt.setListRptBKCTCTMHProductRecordVO(mapPurchaseOrder.get(purchaseOrder).getListRptBKCTCTMHProductRecordVO());

							if (rpt.getListRptBKCTCTMHProductRecordVO() != null && rpt.getListRptBKCTCTMHProductRecordVO().size() > 0) {
								RptBKCTCTMHProductRecordVO child = rpt.getListRptBKCTCTMHProductRecordVO().get(0);
								rpt.setImportDate(child.getImportDate());
								rpt.setInvoiceNumber(child.getInvoiceNumber());
								rpt.setOrderDate(child.getOrderDate());
								rpt.setPoConfirm(child.getPoConfirm());
								rpt.setPurchaseOrder(child.getPurchaseOrder());
								rpt.setSaleOrderNumber(child.getSaleOrderNumber());
								if (child.getStatus().intValue() == 0)
									rpt.setStatus("Chưa nhập");
								if (child.getStatus().intValue() == 2)
									rpt.setStatus("�??� nhập h?�ng");
							}
							res.add(rpt);
						}
					}

				}
			}
			return res;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @return list RptBKCTCTMHProductRecordVO
	 * @author hunglm16
	 * @since February 10, 2014
	 * @description report NPP/Mua Hang/5.8 Bang ke chi tiet chung tu mua hang
	 * */
	@Override
	public List<RptBKCTCTMHProductRecordVO> rptMuaHang_BKCTCTMH(Long shopId, Date fromDate, Date toDate, Integer type) throws BusinessException {
		//Bang ke chi tiet chung tu mua hang
		try {
			if (shopId == null) {
				return new ArrayList<RptBKCTCTMHProductRecordVO>();
			}
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BAOCAO_BKCTCTMH(?, :shopId, :fromDate, :toDate, :type)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(4);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(5);
			params.add(type);
			params.add(java.sql.Types.NUMERIC);

			return repo.getListByQueryDynamicFromPackage(RptBKCTCTMHProductRecordVO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author hunglm16
	 * @since JUNE 11, 2014
	 * @description SHOP_REPPORT 7.7.8
	 */
	@Override
	public List<Rpt_SHOP_7_7_8VO> rptShop_7_7_8VO(Long shopId, Date fromDate, Date toDate) throws BusinessException {
		//Bang ke chi tiet chung tu mua hang
		try {
			if (shopId == null) {
				return new ArrayList<Rpt_SHOP_7_7_8VO>();
			}

			StringBuilder sql = new StringBuilder();
			//sql.append("call PKG_SHOP_REPORT.RPT7_7_8(?, :shopId, :fromDate, :toDate)");
			sql.append("call PKG_SHOP_REPORT.RPT7_7_8(?, :shopId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(4);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			return repo.getListByQueryDynamicFromPackage(Rpt_SHOP_7_7_8VO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	
	/**
	 * 7.2.1 Theo Doi Ban Hang Theo Mat Hang
	 * 
	 * @author sangtn
	 * 
	 * @author hunglm16
	 * @since October 14,2014
	 * @description Phan Quyen Theo CMS
	 * */
	@Override
	public List<RptTDBHTMHProductAndListInfoVO> getTDBHTMHReport(Long shopId, String listStaffId, Date fromDate, Date toDate, Long staffRootId, Integer flagCMS) throws BusinessException {
		//new result tra ve:
		List<RptTDBHTMHProductAndListInfoVO> result = new ArrayList<RptTDBHTMHProductAndListInfoVO>();
		//lay record tu DAO:
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BAOCAO_TDBHTMH(?, :shopId, :listStaffId, :fromDate, :toDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			if (!StringUtility.isNullOrEmpty(listStaffId)) {
				params.add(listStaffId);
			} else {
				params.add(null);
			}
			params.add(java.sql.Types.VARCHAR);
			params.add(4);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(5);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(6);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(7);
			params.add(flagCMS);
			params.add(java.sql.Types.NUMERIC);

			List<RptTDBHTMHProductDateStaffFollowCustomerVO> listRptTDBHTMHProductDateStaffFollowCustomerVO = repo.getListByQueryDynamicFromPackage(RptTDBHTMHProductDateStaffFollowCustomerVO.class, sql.toString(), params, null);

			//tao map 1:
			//Map<String, List<RptTDBHTMHProductDateStaffFollowCustomerVO>> map1 = GroupUtility.collectionToMap(String.class,listRptTDBHTMHProductDateStaffFollowCustomerVO, "productCode");

			List<String> lstGroupBy = new ArrayList<String>();
			lstGroupBy.add("productCode");
			lstGroupBy.add("price");
			Map<String, List<RptTDBHTMHProductDateStaffFollowCustomerVO>> map1 = GroupUtility.collectionToHash2(listRptTDBHTMHProductDateStaffFollowCustomerVO, lstGroupBy);

			//for qua map 1, set data vao vo1:
			for (String keyMap1ProductCode : map1.keySet()) {
				//lay element cua for
				List<RptTDBHTMHProductDateStaffFollowCustomerVO> elementMap1 = map1.get(keyMap1ProductCode);
				//
				//new vo1 va set data vao vo1:
				RptTDBHTMHProductAndListInfoVO vo1 = new RptTDBHTMHProductAndListInfoVO();

				String[] keyGroupBy = keyMap1ProductCode.split("-", 2);

				vo1.setProductCode(keyGroupBy[0]);
				vo1.setPrice(BigDecimal.valueOf(Long.parseLong(keyGroupBy[1].substring(0, keyGroupBy[1].lastIndexOf("-")))));
				//vo1.setProductId(productId);
				//vo1.setProductName(productName);
				//vo1.setSumProductAndPriceAmount(sumProductAndPriceAmount);
				//vo1.setSumProductAndPriceQuantity(sumProductAndPriceQuantity);								

				ArrayList<RptTDBHTMHProductFollowDateVO> lstRptTDBHTMHProductFollowDateVO = new ArrayList<RptTDBHTMHProductFollowDateVO>();
				vo1.setLstRptTDBHTMHProductFollowDateVO(lstRptTDBHTMHProductFollowDateVO);
				Map<String, List<RptTDBHTMHProductDateStaffFollowCustomerVO>> map2From1ElementOfMap1 = GroupUtility.collectionToMap(String.class, elementMap1, "createDate");

				for (String keyMap2CreateDate : map2From1ElementOfMap1.keySet()) {//Khong sap xep theo truong ngay
					//for(int iKey= 0; iKey<map2From1ElementOfMap1.size(); iKey++){
					//String keyMap2CreateDate = map2From1ElementOfMap1.keySet().toArray()[iKey].toString();
					//lay element cua for
					List<RptTDBHTMHProductDateStaffFollowCustomerVO> elementMap2 = map2From1ElementOfMap1.get(keyMap2CreateDate);
					//
					//new vo2 va set data vao vo2:
					RptTDBHTMHProductFollowDateVO vo2 = new RptTDBHTMHProductFollowDateVO();
					vo2.setCreateDate(keyMap2CreateDate);
					//vo2.setSumDayAmount(sumDayAmount);
					//vo2.setSumDayQuantity(sumDayQuantity);
					ArrayList<RptTDBHTMHProductDateFollowStaffVO> lstRptTDBHTMHProductDateFollowStaffVO = new ArrayList<RptTDBHTMHProductDateFollowStaffVO>();
					vo2.setLstRptTDBHTMHProductDateFollowStaffVO(lstRptTDBHTMHProductDateFollowStaffVO);
					Map<String, List<RptTDBHTMHProductDateStaffFollowCustomerVO>> map3From1ElementOfMap2 = GroupUtility.collectionToMap(String.class, elementMap2, "staffCode");
					for (String keyMap3StaffCode : map3From1ElementOfMap2.keySet()) {
						//lay element cua for
						List<RptTDBHTMHProductDateStaffFollowCustomerVO> elementMap3 = map3From1ElementOfMap2.get(keyMap3StaffCode);
						//
						//new vo2 va set data vao vo2:
						RptTDBHTMHProductDateFollowStaffVO vo3 = new RptTDBHTMHProductDateFollowStaffVO();
						vo3.setStaffCode(keyMap3StaffCode);
						vo3.setStaffName(elementMap3.size() > 0 ? elementMap3.get(0).getStaffName() : null);
						vo3.setSumStaffAmount(elementMap3.size() > 0 ? elementMap3.get(0).getSumStaffAmount() : null);
						vo3.setSumStaffQuantity(elementMap3.size() > 0 ? elementMap3.get(0).getSumStaffQuantity() : null);
						vo3.setLstRptTDBHTMHProductDateStaffFollowCustomerVO(elementMap3);

						vo2.setSumDayAmount(elementMap3.size() > 0 ? elementMap3.get(0).getSumDayAmount() : null);
						vo2.setSumDayQuantity(elementMap3.size() > 0 ? elementMap3.get(0).getSumDayQuantity() : null);
						lstRptTDBHTMHProductDateFollowStaffVO.add(vo3);

						//vo1.setPrice(elementMap3.size()>0 ? elementMap3.get(0).getPrice():null);
						vo1.setProductId(elementMap3.size() > 0 ? elementMap3.get(0).getProductId() : null);
						vo1.setProductName(elementMap3.size() > 0 ? elementMap3.get(0).getProductName() : null);
						vo1.setSumProductAndPriceAmount(elementMap3.size() > 0 ? elementMap3.get(0).getSumProductAndPriceAmount() : null);
						vo1.setSumProductAndPriceQuantity(elementMap3.size() > 0 ? elementMap3.get(0).getSumProductAndPriceQuantity() : null);
					}
					lstRptTDBHTMHProductFollowDateVO.add(vo2);
				}
				result.add(vo1);
			}
			return result;
		} catch (Exception e) {
			throw new BusinessException(e);
		}

	}

	//SangTN
	@Override
	public List<RptCustomerVO> getDSKHReport(Long shopId, String staffCode, String customerCode, String customerName, boolean isGroupByStaff, Integer status, Long parentStaffId, boolean checkMap) throws BusinessException {
		try {
			List<RptCustomerVO> res = null;

			/*
			 * List<Object> inParams = new ArrayList<Object>();
			 * inParams.add("shopId"); inParams.add(shopId == null ? 0 :
			 * shopId); inParams.add(StandardBasicTypes.LONG);
			 * 
			 * inParams.add("staffCode"); inParams.add(staffCode);
			 * inParams.add(StandardBasicTypes.STRING);
			 * 
			 * inParams.add("customerCode"); inParams.add(customerCode);
			 * inParams.add(StandardBasicTypes.STRING);
			 * 
			 * inParams.add("isGroupByStaff"); inParams.add(isGroupByStaff);
			 * inParams.add(StandardBasicTypes.INTEGER);
			 * 
			 * inParams.add("status"); inParams.add(status);
			 * inParams.add(StandardBasicTypes.INTEGER);
			 */

			//NamLB
			//		List<CustomerVO> lstCustomer = repo
			//				.getListByNamedQuery(
			//						CustomerVO.class,
			//						"PKG_SHOP_REPORT.BAOCAO_DSKH", inParams);
			//		

			//		List<CustomerVO> lstCustomer = customerDAO.getListCustomerVO(shopId, staffCode, 
			//				customerCode, customerName, isGroupByStaff); 

			List<CustomerVO> lstCustomer = customerDAO.getListCustomerVO(shopId, staffCode, customerCode, customerName, isGroupByStaff, status, parentStaffId, checkMap);

			if (isGroupByStaff) {
				Map<String, Map<String, RptCustomerVO>> staffMap = new HashMap<String, Map<String, RptCustomerVO>>();
				List<String> staffKey = new ArrayList<String>();
				Map<String, List<String>> mapAreaKey = new HashMap<String, List<String>>();
				if (lstCustomer != null && lstCustomer.size() > 0) {
					for (CustomerVO child : lstCustomer) {
						if (!StringUtility.isNullOrEmpty(child.getStaffName())) {
							if (staffMap.containsKey(child.getStaffName())) {
								Map<String, RptCustomerVO> areaMap = staffMap.get(child.getStaffName());
								List<String> areaKey = mapAreaKey.get(child.getStaffName());
								if (areaKey == null) {
									areaKey = new ArrayList<String>();
									mapAreaKey.put(child.getStaffName(), areaKey);
								}
								if (areaMap.containsKey(child.getAreaName())) {
									RptCustomerVO c = areaMap.get(child.getAreaName());
									c.setAreaName(child.getAreaName());
									c.getLstCustomer().add(child);
								} else {
									RptCustomerVO c = new RptCustomerVO();
									c.setAreaName(child.getAreaName());
									c.setLstCustomer(new ArrayList<CustomerVO>());
									c.getLstCustomer().add(child);
									areaMap.put(child.getAreaName(), c);
									areaKey.add(child.getAreaName());

								}
							} else {
								staffKey.add(child.getStaffName());
								Map<String, RptCustomerVO> areaMap = new HashMap<String, RptCustomerVO>();
								RptCustomerVO c = new RptCustomerVO();
								c.setAreaName(child.getAreaName());
								c.setLstCustomer(new ArrayList<CustomerVO>());
								c.getLstCustomer().add(child);
								areaMap.put(child.getAreaName(), c);
								staffMap.put(child.getStaffName(), areaMap);
								List<String> areaKey = mapAreaKey.get(child.getStaffName());
								if (areaKey == null) {
									areaKey = new ArrayList<String>();
									mapAreaKey.put(child.getStaffName(), areaKey);
								}
								areaKey.add(child.getAreaName());
							}
						}
					}
				}
				res = new ArrayList<RptCustomerVO>();
				for (String staffName : staffKey) {
					RptCustomerVO child = new RptCustomerVO();
					child.setStaffName(staffName);
					Map<String, RptCustomerVO> areaMap = staffMap.get(staffName);
					List<String> staffAreaKey = mapAreaKey.get(staffName);
					for (String areaName : staffAreaKey) {
						RptCustomerVO area = new RptCustomerVO();
						area.setAreaName(areaName);
						area.setLstCustomer(areaMap.get(areaName).getLstCustomer());
						child.getLstArea().add(area);
					}
					res.add(child);
				}
			} else {//group by area
				if (lstCustomer != null && lstCustomer.size() > 0) {
					List<String> areaKey = new ArrayList<String>();
					Map<String, RptCustomerVO> areaMap = new HashMap<String, RptCustomerVO>();
					for (CustomerVO child : lstCustomer) {
						if (!StringUtility.isNullOrEmpty(child.getAreaName())) {
							if (areaMap.containsKey(child.getAreaName())) {
								RptCustomerVO c = areaMap.get(child.getAreaName());
								c.setAreaName(child.getAreaName());
								c.getLstCustomer().add(child);
							} else {
								areaKey.add(child.getAreaName());
								RptCustomerVO c = new RptCustomerVO();
								c.setAreaName(child.getAreaName());
								c.setLstCustomer(new ArrayList<CustomerVO>());
								c.getLstCustomer().add(child);
								areaMap.put(child.getAreaName(), c);
							}
						}
					}
					res = new ArrayList<RptCustomerVO>();
					for (String areaName : areaKey) {
						RptCustomerVO area = new RptCustomerVO();
						area.setAreaName(areaName);
						area.setLstCustomer(areaMap.get(areaName).getLstCustomer());
						res.add(area);
					}
				}
			}
			return res;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptBCTCNVO> getListRptBCTCNVO(Long shopId, List<Long> lstObjectId, StockObjectType objectType) throws BusinessException {
		try {
			List<RptBCTCNVO> rs = new ArrayList<RptBCTCNVO>();

			String lastIdx = null;
			RptBCTCNVO lastVO = null;
			List<RptBCTCNDetailVO> lstItem = new ArrayList<RptBCTCNDetailVO>();
			List<RptBCTCNDetailVO> lstDetail = null;

			if (StockObjectType.CUSTOMER.equals(objectType))
				lstDetail = debitDAO.getRptBCTCNDetailVO(shopId, lstObjectId, null, objectType);
			else
				lstDetail = debitDAO.getRptBCTCNDetailVO(shopId, null, lstObjectId, objectType);
			try {
				for (RptBCTCNDetailVO detail : lstDetail) {
					detail.safeSetNull();
					String idx = StockObjectType.CUSTOMER.equals(objectType) ? detail.getCustomerInfo() : detail.getStaffInfo();
					if (lastIdx != null && lastIdx.equals(idx)) {
						lstItem.add(detail);
						lastVO.setAvailableDebt(lastVO.getAvailableDebt().add(detail.getAvailableDebt()));
						lastVO.setNextDebt(lastVO.getNextDebt().add(detail.getNextDebt()));
						lastVO.setOverdueDebt(lastVO.getOverdueDebt().add(detail.getOverdueDebt()));
						lastVO.setTotal(lastVO.getTotal().add(detail.getAvailableDebt().add(detail.getNextDebt().add(detail.getOverdueDebt()))));
					} else {
						if (lastVO != null) {
							lastVO.setLstDetail(lstItem);
							rs.add(lastVO);
							lstItem = new ArrayList<RptBCTCNDetailVO>();
						}
						RptBCTCNVO vo = new RptBCTCNVO();
						lastIdx = idx;

						vo.setStaffInfo(detail.getStaffInfo());
						vo.setCustomerInfo(detail.getCustomerInfo());

						vo.setAvailableDebt(detail.getAvailableDebt());
						vo.setNextDebt(detail.getNextDebt());
						vo.setOverdueDebt(detail.getOverdueDebt());
						vo.setTotal(detail.getAvailableDebt().add(detail.getNextDebt().add(detail.getOverdueDebt())));

						lastVO = vo;
						lstItem.add(detail);
					}
				}
				if (lastVO != null) {
					lastVO.setLstDetail(lstItem);
					lastVO.safeSetNull();
					rs.add(lastVO);
					lstItem = new ArrayList<RptBCTCNDetailVO>();
				}
			} catch (Exception e) {
				throw new BusinessException(e);
			}
			return rs;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptCustomerNotVisitOfSalesVO> getListRptKHKGTTT_NVBH(List<Long> listShopId, List<Long> listNVGSId, List<Long> listNVBHId, Date fromDate, Date toDate) throws BusinessException {
		if (null == listShopId || listShopId.size() <= 0) {
			throw new IllegalArgumentException(ExceptionCode.LIST_SHOP_ID_IS_NULL);
		}
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		if (fromDate.after(toDate)) {
			throw new IllegalArgumentException(ExceptionCode.BEGIN_TIME_AFTER_END_TIME);
		}

		try {
			return this.staffDAO.getListRptKHKGTTT_NVBH(listShopId, listNVGSId, listNVBHId, fromDate, toDate);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	// shopId in string, lstStaffCode in string, lstGsnppCode in string, fromDate in date, toDate in date, staffIdRoot in number, flagCMS in number)
	@Override
	public List<RptBCNVKGTKHTTRecordVO> getListBCNVKGTKHTT(String shopId, String lstStaffCode, String lstOwnerCode, Date fromDate, Date toDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		List<RptBCNVKGTKHTTRecordVO> listRptBCNVKGTKHTTRecordVO = null;
		try {
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("lstStaffCode");
			inParams.add(lstStaffCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("lstGsnppCode");
			inParams.add(lstOwnerCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("staffIdRoot");
			inParams.add(staffIdRoot);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("flagCMS");
			inParams.add(flagCMS);
			inParams.add(StandardBasicTypes.INTEGER);

			listRptBCNVKGTKHTTRecordVO = repo.getListByNamedQuery(RptBCNVKGTKHTTRecordVO.class, "PKG_SHOP_REPORT.BAOCAO_BCKGTKHTT", inParams);
			return listRptBCNVKGTKHTTRecordVO;

		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.report.ShopReportMgr#getListVisitCustomer(java.util.
	 * Date, java.util.Date, java.lang.Long, java.lang.Long, java.lang.Long,
	 * java.lang.Boolean)
	 */
	@Override
	public List<TimeVisitCustomerVO> getListVisitCustomer2(Date fromDate, Date toDate, Long shopId, Long superId, Long staffId, Boolean getShopOnly) throws BusinessException {

		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			return actionLogDAO.getListVisitCustomer(fromDate, toDate, shopId, superId, staffId, getShopOnly);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}


	@Override
	public List<GS_4_2_VO> getListVisitCustomer(Long userId, Long roleId, String lstShopId, Date fromDate, Date toDate) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {

			List<GS_4_2_VO> lstResult = new ArrayList<GS_4_2_VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_REPORT_GS.REPORT_GS_4_2(?,:userId,:roleId,:lstShopId,:lstGSId, :lstNVBHId, :i_from_date, :i_to_date)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(lstShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(null);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(6);
			params.add(null);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(7);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(8);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_4_2_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<GS_1_2_VO> getListVisitNVBH(Long userId, Long roleId, String lstShopId, Date fromDate, Date toDate, Integer staffType) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {

			List<GS_1_2_VO> lstResult = new ArrayList<GS_1_2_VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_REPORT_GS.P_REPORT_GS_1_2(?,:userId,:roleId,:lstShopId,:lstGSId, :lstNVBHId, :i_from_date, :i_to_date, :i_staff_type)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(lstShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(null);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(6);
			params.add(null);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(7);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(8);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(9);
			params.add(staffType);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_1_2_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	
	@Override
	public List<Rpt_TGLVCNVBH_VO> reportTGLVCNVBH(Long shopId, Long cycleId, Date fromDate, Date toDate, Long staffRootId, Long roleId, Long shopRootId) throws BusinessException {
		if (shopId == null || staffRootId == null || roleId == null || shopRootId == null) {
			throw new IllegalArgumentException(ExceptionCode.REPORT_PARAMETER_IS_NULL);
		}
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<Rpt_TGLVCNVBH_VO> lstResult = new ArrayList<Rpt_TGLVCNVBH_VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_REPORT_GS_P2.BC_TGLVCNVBH(?, :shopId, :cycleId, :fromDate, :toDate, :staffRootId, :roleId, :shopRootId) ");
			List<Object> params = new ArrayList<Object>();
			int sqlParamIndex = 2;
			params.add(sqlParamIndex++);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(sqlParamIndex++);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			params.add(sqlParamIndex++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(Rpt_TGLVCNVBH_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptBCDS1VO> getListReportDS1(String strListShopId, Date fromDate, Date toDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<RptBCDS1VO> lstResult = new ArrayList<RptBCDS1VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.BAOCAO_DS1(?,:lstShopId,:fromDate,:toDate,:staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);

			params.add(3);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(4);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(5);
			params.add(staffIdRoot);
			params.add(Types.NUMERIC);

			params.add(6);
			params.add(flagCMS);
			params.add(Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(RptBCDS1VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/*
	 * @Override public List<RptDMVSVO> getDMVSReport(String strListShopId,
	 * List<Long> staffOwnerId, List<Long> staffId, Date fromDate, Date toDate,
	 * Integer fromHour, Integer fromMinute, Integer toHour, Integer toMinute)
	 * throws BusinessException { try { // String staffOwnerIdStr = "";
	 * if(staffOwnerId != null && staffOwnerId.size() > 0){ Boolean isFirst =
	 * true; for (Long child : staffOwnerId) { if(isFirst){ isFirst = false;
	 * staffOwnerIdStr += child; }else{ staffOwnerIdStr += ","+child; } } }
	 * 
	 * String staffIdStr = ""; if(staffId != null && staffId.size() > 0){
	 * Boolean isFirst = true; for (Long child : staffId) { if(isFirst){ isFirst
	 * = false; staffIdStr += child; }else{ staffIdStr += ","+child; } } }
	 * 
	 * List<Object> inParams = new ArrayList<Object>();
	 * inParams.add("strListShopId"); inParams.add(strListShopId);
	 * inParams.add(StandardBasicTypes.STRING); inParams.add("staffOwnerId");
	 * inParams
	 * .add(StringUtility.isNullOrEmpty(staffOwnerIdStr)?null:staffOwnerIdStr);
	 * inParams.add(StandardBasicTypes.STRING); inParams.add("staffId");
	 * inParams.add(StringUtility.isNullOrEmpty(staffIdStr)?null:staffIdStr);
	 * inParams.add(StandardBasicTypes.STRING); inParams.add("fromDate");
	 * inParams.add(fromDate); inParams.add(StandardBasicTypes.DATE);
	 * inParams.add("toDate"); inParams.add(toDate);
	 * inParams.add(StandardBasicTypes.DATE); inParams.add("fromHour");
	 * inParams.add(fromHour==null?0:fromHour);
	 * inParams.add(StandardBasicTypes.INTEGER); inParams.add("fromMinute");
	 * inParams.add(fromMinute==null?0:fromMinute);
	 * inParams.add(StandardBasicTypes.INTEGER); inParams.add("toHour");
	 * inParams.add(toHour==null?0:toHour);
	 * inParams.add(StandardBasicTypes.INTEGER); inParams.add("toMinute");
	 * inParams.add(toMinute==null?0:toMinute);
	 * inParams.add(StandardBasicTypes.INTEGER); List<RptDMVSVO> lstResult =
	 * repo.getListByNamedQuery( RptDMVSVO.class,
	 * "PKG_SHOP_REPORT.DIMUONVESOM_REPORT", inParams); if(lstResult != null &&
	 * lstResult.size() > 0){ for (RptDMVSVO child : lstResult) { try {
	 * child.safeSetNull(); } catch (Exception e) { throw new
	 * BusinessException(e); } } } return lstResult; } catch
	 * (DataAccessException e) { throw new BusinessException(e); } }
	 */

	/**
	 * VT1.2: Bao cao di muon ve som nhan vien ban hang
	 * 
	 * @author anonymous
	 * @modify tulv2
	 * @since 04.10.2014
	 * */

	@Override
	public List<RptDMVSVO> getDMVSReport(String strListShopId, List<Long> staffOwnerId, List<Long> staffId, Date fromDate, Date toDate, Integer fromHour, Integer fromMinute, Integer toHour, Integer toMinute, Long staffIdRoot, Integer flagCMS)
			throws BusinessException {
		try {

			String staffOwnerIdStr = "";
			if (staffOwnerId != null && staffOwnerId.size() > 0) {
				Boolean isFirst = true;
				for (Long child : staffOwnerId) {
					if (isFirst) {
						isFirst = false;
						staffOwnerIdStr += child;
					} else {
						staffOwnerIdStr += "," + child;
					}
				}
			}

			String staffIdStr = "";
			if (staffId != null && staffId.size() > 0) {
				Boolean isFirst = true;
				for (Long child : staffId) {
					if (isFirst) {
						isFirst = false;
						staffIdStr += child;
					} else {
						staffIdStr += "," + child;
					}
				}
			}

			// strListShopId in STRING, staffOwnerId in string, staffId in string, fromDate IN DATE, toDate in DATE, 
			// fromHour IN NUMBER, fromMinute in NUMBER,toHour IN NUMBER, toMinute in NUMBER, staffIdRoot in number, flagCMS in number)

			// strListShopId in STRING, staffOwnerId in string, staffId in string, fromDate IN DATE, toDate in DATE, fromHour IN NUMBER, 
			// fromMinute in NUMBER,toHour IN NUMBER, toMinute in NUMBER, staffIdRoot in number, flagCMS in number)
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("strListShopId");
			inParams.add(strListShopId);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("staffOwnerId");
			inParams.add(staffOwnerIdStr);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("staffId");
			inParams.add(StringUtility.isNullOrEmpty(staffIdStr) ? null : staffIdStr);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("fromHour");
			inParams.add(fromHour == null ? 0 : fromHour);
			inParams.add(StandardBasicTypes.INTEGER);

			inParams.add("fromMinute");
			inParams.add(fromMinute == null ? 0 : fromMinute);
			inParams.add(StandardBasicTypes.INTEGER);

			inParams.add("toHour");
			inParams.add(toHour == null ? 0 : toHour);
			inParams.add(StandardBasicTypes.INTEGER);

			inParams.add("toMinute");
			inParams.add(toMinute == null ? 0 : toMinute);
			inParams.add(StandardBasicTypes.INTEGER);

			inParams.add("staffIdRoot");
			inParams.add(staffIdRoot);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("flagCMS");
			inParams.add(flagCMS);
			inParams.add(StandardBasicTypes.INTEGER);

			List<RptDMVSVO> lstResult = repo.getListByNamedQuery(RptDMVSVO.class, "PKG_SHOP_REPORT.DIMUONVESOM_REPORT", inParams);
			if (lstResult != null && lstResult.size() > 0) {
				for (RptDMVSVO child : lstResult) {
					try {
						child.safeSetNull();
					} catch (Exception e) {
						throw new BusinessException(e);
					}
				}
			}
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptBCNgayVO> getListBCNgayReport(Long shopId) throws BusinessException {
		if (null == shopId) {
			throw new IllegalArgumentException(ExceptionCode.SHOP_ID_IS_NULL);
		}

		try {
			Date toDate = this.commonDAO.getSysDate();
			Date fromDate = this.commonDAO.getFirstDateOfMonth();

			// So ngay ban hang trong thang
			Integer workingDaysInMonth = saleDayDAO.getSaleDayByDate(toDate);

			// So ngay ban hang da qua
			int workedDaysInMonth = saleDayMgr.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);

			if (workingDaysInMonth == null) {
				throw new IllegalArgumentException("workDateInMonth is null");
			}

			//
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);
			inParams.add("WORKING_DAYS_IN_MONTH");
			inParams.add(workingDaysInMonth);
			inParams.add(StandardBasicTypes.INTEGER);
			inParams.add("WORKED_DAYS_OF_MONTH");
			inParams.add(workedDaysInMonth);
			inParams.add(StandardBasicTypes.INTEGER);

			List<RptBCNgayVO> lstResult = repo.getListByNamedQuery(RptBCNgayVO.class, "PKG_SHOP_REPORT.BAOCAONGAY_REPORT", inParams);

			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptBCTGTMVO> getListRptBCTGTMVO(Long shopId, Long gsnppId, Long staffId, Date fromDate, Date toDate, Integer n_minutes) throws BusinessException {
		try {

			//shopId NUMBER, gsnppId NUMBER, staffId NUMBER, fromDate DATE, toDate DATE, n_minutes NUMBER
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("gsnppId");
			inParams.add(gsnppId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("staffId");
			inParams.add(staffId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("n_minutes");
			inParams.add(n_minutes);
			inParams.add(StandardBasicTypes.INTEGER);
			//
			//			List<RptBCTGTMVO> lstResult = repo.getListByNamedQuery(
			//					RptBCTGTMVO.class, "PKG_SHOP_REPORT.BAO_CAO_THOI_GIAN_TAT_MAY",
			//					inParams);
			//
			//			return lstResult;
			//			toDate = this.commonDAO.getSysDate();
			//			fromDate = this.commonDAO.getFirstDateOfMonth();
			//
			//			// So ngay ban hang trong thang
			//			Integer workingDaysInMonth = saleDayDAO.getSaleDayByDate(toDate);
			//
			//			// So ngay ban hang da qua
			//			int workedDaysInMonth = saleDayMgr
			//					.getNumberWorkingDayByFromDateAndToDate(fromDate, toDate);
			//
			//			if (workingDaysInMonth == null) {
			//				throw new IllegalArgumentException("workDateInMonth is null");
			//			}

			//
			//			List<Object> inParams = new ArrayList<Object>();
			//			inParams.add("shopId");
			//			inParams.add(shopId == null ? 0 : shopId);
			//			inParams.add(StandardBasicTypes.LONG);
			//			inParams.add("WORKING_DAYS_IN_MONTH");
			//			inParams.add(workingDaysInMonth);
			//			inParams.add(StandardBasicTypes.INTEGER);
			//			inParams.add("WORKED_DAYS_OF_MONTH");
			//			inParams.add(workedDaysInMonth);
			//			inParams.add(StandardBasicTypes.INTEGER);

			List<RptBCTGTMVO> lstResult = repo.getListByNamedQuery(RptBCTGTMVO.class, "PKG_SHOP_REPORT.BAO_CAO_THOI_GIAN_TAT_MAY", inParams);
			System.out.println("NMH:" + lstResult.size());
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptBCTGTMVO> getListRptBCTGTM(Long staffIdRoot, Long roleId, Long shopRootId, Date fromDate, Date toDate, Integer nMinute) throws BusinessException {
		List<RptBCTGTMVO> lstResult = new ArrayList<RptBCTGTMVO>();
		try {
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("call PKG_REPORT_GS_P2.REPORT_TGTM(?, :userId, :roleId, :shopId, :fromDate, :toDate, :nMinute) ");
			int setPR = 2;
			params.add(setPR++);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(roleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(shopRootId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(setPR++);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(setPR++);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(setPR++);
			params.add(nMinute);
			params.add(java.sql.Types.NUMERIC);
			
			lstResult = repo.getListByQueryDynamicFromPackage(RptBCTGTMVO.class, sql.toString(), params, null);
			
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptBCDS2VO> getListRptBCDS2(String lstShopId, Date fromDate, Date toDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		try {
			if (null == lstShopId) {
				throw new IllegalArgumentException(ExceptionCode.LIST_SHOP_ID_IS_NULL);
			}
			/*
			 * List<RptBCDS2VO> lstResult =
			 * repo.getListByNamedQuery(RptBCDS2VO.class,
			 * "PKG_SHOP_REPORT.BAOCAO_DSBHSKUCTTKH",inParams);
			 */
			List<RptBCDS2VO> lstResult = new ArrayList<RptBCDS2VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.BAOCAO_DS2(?,:lstShopId,:fromDate,:toDate,:staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(lstShopId);
			params.add(java.sql.Types.VARCHAR);

			params.add(3);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(4);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(5);
			params.add(staffIdRoot);
			params.add(Types.NUMERIC);

			params.add(6);
			params.add(flagCMS);
			params.add(Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(RptBCDS2VO.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	public static void main(String[] args) {
		Calendar calFrom = Calendar.getInstance();
		calFrom.set(Calendar.YEAR, 2013);
		calFrom.set(Calendar.MONTH, 0);
		calFrom.set(Calendar.DATE, 1);
		Calendar calTo = Calendar.getInstance();
		calTo.set(Calendar.YEAR, 2013);
		calTo.set(Calendar.MONTH, 0);
		calTo.set(Calendar.DATE, 10);
		List<Date> lstRealDate = new ArrayList<Date>();
		while (calFrom.before(calTo)) {
//			Boolean isValid = true;
			if (calFrom.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				//calFrom.get(Calendar.)
//				if (isValid) {
					lstRealDate.add(calFrom.getTime());
//				}
			}
			calFrom.add(Calendar.DATE, 1);
		}
		for (Date date : lstRealDate) {
			System.out.println(date);
		}
	}

	@Override
	public List<RptAbsentVO> getAbsentReport2(Long shopId, Long staffId, Date fromDate, Date toDate) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);
			inParams.add("staffId");
			inParams.add(staffId);
			inParams.add(StandardBasicTypes.LONG);
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptAbsentVO> lstResult = repo.getListByNamedQuery(RptAbsentVO.class, "PKG_SHOP_REPORT.KOBATMAY_EX_REPORT", inParams);
			if (lstResult != null && lstResult.size() > 0) {
				for (RptAbsentVO child : lstResult) {
					List<RptAbsentDetailVO> lstDay = new ArrayList<RptAbsentDetailVO>();
					if (!StringUtility.isNullOrEmpty(child.getSummaryValue())) {
						String[] lstTmp = child.getSummaryValue().split(",");
						for (String day : lstTmp) {
							String[] dayDetail = day.split(":");
							RptAbsentDetailVO vo = new RptAbsentDetailVO();
							vo.setDay(dayDetail[0]);
							try {
								vo.setDayVal(dayDetail[1]);
							} catch (Exception e) {
								//do nothing
							}
							lstDay.add(vo);
						}
					}
					child.setLstDay(lstDay);
				}
			}
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public RptBCVT5VO getAbsentReport(String strListShopId, String strListStaffId, Date fromDate, Date toDate) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			//SangTN
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("lstShopId");
			inParams.add(strListShopId);
			inParams.add(StandardBasicTypes.STRING);
			inParams.add("lstStaffId");
			inParams.add(strListStaffId);
			inParams.add(StandardBasicTypes.STRING);
			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptAbsentVO> lstResult = repo.getListByNamedQuery(RptAbsentVO.class, "PKG_SHOP_REPORT.KOBATMAY_EX_REPORT2", inParams);

			//			List<RptAbsentVO> lstResult = new ArrayList<RptAbsentVO>();
			//			StringBuilder sql = new StringBuilder(); 
			//			sql.append("call PKG_SHOP_REPORT.KOBATMAY_EX_REPORT2(?,:lstShopId,:staffId,:fromDate,:toDate)");
			//			List<Object> params = new ArrayList<Object>();
			//			params.add(2);
			//			params.add(shopId.toString());
			//			params.add(java.sql.Types.NVARCHAR);
			//			params.add(3);
			//			params.add(staffId);
			//			params.add(java.sql.Types.NUMERIC);
			//			params.add(4);
			//			params.add(fromDate);
			//			params.add(java.sql.Types.DATE);
			//			params.add(5);
			//			params.add(toDate);
			//			params.add(java.sql.Types.DATE);
			//			
			//			lstResult = repo.getListByQueryDynamicFromPackage(RptAbsentVO.class, sql.toString(), params, null);

			for (RptAbsentVO rpt : lstResult) {
				rpt.safeSetNull();
			}

			RptBCVT5VO objVO = new RptBCVT5VO();
			List<String> lstDay = new ArrayList<String>();
			objVO.setLstValue(lstResult);
			objVO.setLstDay(lstDay);
			boolean isAssigned = false;

			if (lstResult != null && !lstResult.isEmpty()) {
				for (RptAbsentVO child : lstResult) {
					if (!StringUtility.isNullOrEmpty(child.getSummaryValue())) {
						String[] lstTmp = child.getSummaryValue().split(",");
						List<String> lstVal = new ArrayList<String>();
						for (String day : lstTmp) {
							String[] dayDetail = day.split(":");
							if (dayDetail != null && dayDetail.length > 0) {
								if (!isAssigned) {
									lstDay.add(dayDetail[0]);
								}
								if (dayDetail.length == 2) {
									lstVal.add(dayDetail[1]);
								} else {
									lstVal.add("");
								}

							}
						}
						isAssigned = true;
						child.setLstDayVal(lstVal);
					}
				}
			}
			return objVO;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/*
	 * @Override public List<RptVT5> getProcVT5(String strListShopId, String
	 * strListStaffId, Date fromDate, Date toDate) throws BusinessException { if
	 * (null == fromDate || null == toDate) { throw new
	 * IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL); } try {
	 * 
	 * List<RptVT5> lstResult = new ArrayList<RptVT5>(); StringBuilder sql = new
	 * StringBuilder(); sql.append(
	 * "call PKG_SHOP_REPORT.proc_vt5(?,:lstShopId,:lstStaffId,:fromDate,:toDate)"
	 * ); List<Object> params = new ArrayList<Object>(); params.add(2);
	 * params.add(strListShopId); params.add(java.sql.Types.VARCHAR);
	 * params.add(3); params.add(strListStaffId);
	 * params.add(java.sql.Types.VARCHAR); params.add(4); params.add(new
	 * java.sql.Date(fromDate.getTime())); params.add(java.sql.Types.DATE);
	 * params.add(5); params.add(new java.sql.Date(toDate.getTime()));
	 * params.add(java.sql.Types.DATE);
	 * 
	 * lstResult = repo.getListByQueryDynamicFromPackage(RptVT5.class,
	 * sql.toString(), params, null);
	 * 
	 * return lstResult; } catch (DataAccessException e) { throw new
	 * BusinessException(e); } }
	 */
	@Override
	public List<RptVT5> getProcVT5(String strListShopId, String strListStaffId, Date fromDate, Date toDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {

			List<RptVT5> lstResult = new ArrayList<RptVT5>();
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.proc_vt5(?,:lstShopId,:lstStaffId,:fromDate,:toDate,:staffIdRoot,:flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);

			params.add(3);
			params.add(strListStaffId);
			params.add(java.sql.Types.VARCHAR);

			params.add(4);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(5);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(6);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);

			params.add(7);
			params.add(flagCMS);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(RptVT5.class, sql.toString(), params, null);

			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * 3.1.4.3 Phiếu thu
	 * 
	 * @author tungtt
	 */
	@Override
	public List<PT> getListPTDetail(Long shopId, String listCustomerId, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<PT> lstRes = new ArrayList<PT>();

			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("listCustomerId");
			inParams.add(listCustomerId);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<PTDetail> listRecord = repo.getListByNamedQuery(PTDetail.class, "PKG_SHOP_REPORT.BAOCAO_PT", inParams);

			Map<String, PT> mapStaff = new HashMap<String, PT>();
			List<String> lstStaffCode = new ArrayList<String>();

			if (listRecord != null && listRecord.size() > 0) {
				for (PTDetail child : listRecord) {
					if (mapStaff.get(child.getCustomerName()) != null) {
						PT rpt = mapStaff.get(child.getCustomerName());
						rpt.getListDetail().add(child);
					} else {
						PT rpt = new PT();
						rpt.setListDetail(new ArrayList<PTDetail>());
						rpt.getListDetail().add(child);
						mapStaff.put(child.getCustomerName(), rpt);
						lstStaffCode.add(child.getCustomerName());
					}
				}
				for (String staffCode : lstStaffCode) {
					lstRes.add(mapStaff.get(staffCode));
				}
			}

			for (PT res : lstRes) {
				List<PTDetail> lst = res.getListDetail();
				if (lst != null && lst.size() > 0) {
					res.setCustomerName(lst.get(0).getCustomerName());
					res.setShortCode(lst.get(0).getShortCode());

					BigDecimal amount = BigDecimal.ZERO;
					for (PTDetail child : lst) {
						if (child.getAmount() != null) {
							amount = amount.add(child.getAmount());
						}
					}
					res.setSumAmount(amount);
				}
			}

			return lstRes;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Bao cao xuất b?�n tồn theo nh?�n vi?�n
	 * 
	 * @author tungtt
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param listStaffId
	 */
	@Override
	public List<RptXBTNV> getXBTNVReport(Long shopId, String listStaffId, Date fromDate, Date toDate) throws BusinessException {
		try {
			List<RptXBTNV> lstRes = new ArrayList<RptXBTNV>();

			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("listStaffId");
			inParams.add(listStaffId);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptXBTNVRecord> listRecord = repo.getListByNamedQuery(RptXBTNVRecord.class, "PKG_SHOP_REPORT.BAOCAO_XBTNV", inParams);

			Map<String, RptXBTNV> mapStaff = new HashMap<String, RptXBTNV>();
			List<String> lstStaffCode = new ArrayList<String>();

			if (listRecord != null && listRecord.size() > 0) {
				for (RptXBTNVRecord child : listRecord) {
					if (mapStaff.get(child.getStaffCode()) != null) {
						RptXBTNV rpt = mapStaff.get(child.getStaffCode());
						rpt.getListDetail().add(child);
					} else {
						RptXBTNV rpt = new RptXBTNV();
						rpt.setListDetail(new ArrayList<RptXBTNVRecord>());
						rpt.getListDetail().add(child);
						mapStaff.put(child.getStaffCode(), rpt);
						lstStaffCode.add(child.getStaffCode());
					}
				}
				for (String staffCode : lstStaffCode) {
					lstRes.add(mapStaff.get(staffCode));
				}
			}

			for (RptXBTNV res : lstRes) {
				List<RptXBTNVRecord> lst = res.getListDetail();
				if (lst != null && lst.size() > 0) {
					res.setStaffCode(lst.get(0).getStaffCode());
					res.setStaffName(lst.get(0).getStaffName());

					BigDecimal sumMoneyExport = BigDecimal.ZERO;
					for (RptXBTNVRecord child : lst) {
						if (child.getExportAmount() == null) {
							child.setExportAmount(BigDecimal.ZERO);
						}
						sumMoneyExport = sumMoneyExport.add(child.getExportAmount());
					}
					res.setSumMoneyExport(sumMoneyExport);

					BigDecimal sumMoneySale = BigDecimal.ZERO;
					for (RptXBTNVRecord child : lst) {
						if (child.getSellQuantity() == null)
							child.setSellQuantity(BigDecimal.ZERO);
						if (child.getSellAmount() == null) {
							child.setSellAmount(BigDecimal.ZERO);
						}
						sumMoneySale = sumMoneySale.add(child.getSellAmount());
					}
					res.setSumMoneySale(sumMoneySale);

					BigDecimal sumMoneyPromotion = BigDecimal.ZERO;
					for (RptXBTNVRecord child : lst) {
						if (child.getPromoteQuantity() == null)
							child.setPromoteQuantity(BigDecimal.ZERO);
						if (child.getPromoteAmount() == null) {
							child.setPromoteAmount(BigDecimal.ZERO);
						}
						sumMoneyPromotion = sumMoneyPromotion.add(child.getPromoteAmount());
					}
					res.setSumMoneyPromotion(sumMoneyPromotion);

					BigDecimal sumMoneyStock = BigDecimal.ZERO;
					for (RptXBTNVRecord child : lst) {
						if (child.getStockQuantity() == null)
							child.setStockQuantity(child.getExportQuantity().subtract(child.getSellQuantity().add(child.getPromoteQuantity())));
						if (child.getStockAmount() == null) {
							child.setStockAmount(child.getExportAmount().subtract(child.getSellAmount().add(child.getPromoteAmount())));
						}
						sumMoneyStock = sumMoneyStock.add(child.getStockAmount());
					}
					res.setSumMoneyStock(sumMoneyStock);
				}
			}

			return lstRes;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public RptPXKKVCNB_Stock getPXKKVCNB(Long shopId, String staffCode, String stockTransId, String nameManeuver, String content) throws DataAccessException {
		try {
			List<Object> params = new ArrayList<Object>();
			params.add("shopId");
			params.add(shopId);
			params.add(StandardBasicTypes.LONG);

			params.add("staffCode");
			params.add(staffCode);
			params.add(StandardBasicTypes.STRING);

			params.add("stockTransId");
			params.add(stockTransId);
			params.add(StandardBasicTypes.STRING);
			List<RptPXKKVCNB_DataConvert> list = repo.getListByNamedQuery(RptPXKKVCNB_DataConvert.class, "PKG_SHOP_REPORT.BAOCAO_PXKKVCNB", params);
			if (list != null && list.size() > 0) {
				RptPXKKVCNB_Stock result = new RptPXKKVCNB_Stock(list, nameManeuver, content);
				return result;
			}
			return null;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}

	}

	@Override
	public List<Rpt_PO_CTTTVO> getPOCTTT(Long shopId, int typePerson, String codePerson, String fromDate, String toDate) throws DataAccessException {
		return poVnmDetailDAO.getPOCTTT(shopId, typePerson, codePerson, fromDate, toDate);
	}

	@Override
	public List<Rpt_TDXNTCT_NganhVO> LayDanhSachXuatNhapKhoChiTiet(Long shopId, Date fromDate, Date toDate) throws DataAccessException {

		return productDAO.LayDanhSachXuatNhapKhoChiTiet(shopId, fromDate, toDate);
	}

	/**
	 * Bao cao chi tiet khuyen mai
	 * 
	 * @author vuonghn
	 */
	@Override
	public List<RptBCCTKMVO> getListRptCTKM(Long shopId, String lstStaffCode, Date fromDate, Date toDate) throws DataAccessException {

		try {
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("lstStaffCode");
			inParams.add(lstStaffCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			List<RptBCCTKM_RecordDetailVO> listRecordDetail = repo.getListByNamedQuery(RptBCCTKM_RecordDetailVO.class, "PKG_SHOP_REPORT.BAOCAO_CTKM", inParams);

			List<RptBCCTKMVO> lst = new ArrayList<RptBCCTKMVO>();

			List<Long> lstShopId = new ArrayList<Long>();
			Map<Long, Map<String, RptBCCTKMDetailVO>> mapLstRecord = new HashMap<Long, Map<String, RptBCCTKMDetailVO>>();
			if (listRecordDetail != null) {
				for (RptBCCTKM_RecordDetailVO child : listRecordDetail) {
					Map<String, RptBCCTKMDetailVO> mapDetail = mapLstRecord.get(child.getShopId().longValue());
					if (mapDetail != null) {
						RptBCCTKMDetailVO rpt = mapDetail.get(child.getPromotionProgramCode());
						if (rpt != null) {
							rpt.getLstBCCTKM_RecordDetailVO().add(child);
						} else {
							rpt = new RptBCCTKMDetailVO();
							rpt.setLstBCCTKM_RecordDetailVO(new ArrayList<RptBCCTKM_RecordDetailVO>());
							rpt.getLstBCCTKM_RecordDetailVO().add(child);
							rpt.setShopId(child.getShopId().longValue());
							rpt.setShopName(child.getShopName());
							rpt.setShopAddress(child.getShopAddress());
							rpt.setPromotionProgramCode(child.getPromotionProgramCode());
							rpt.setPromotionProgramDecription(child.getPromotionProgramDecription());
							mapDetail.put(child.getPromotionProgramCode(), rpt);
						}
						if (child.getFreeItem().intValue() == 0) {
							if (child.getQuantity() != null) {
								rpt.setTotalQuantitySale(rpt.getTotalQuantitySale() + child.getQuantity().intValue());
							}
						} else {
							if (child.getQuantityFree() != null) {
								rpt.setTotalQuantityFree(rpt.getTotalQuantityFree() + child.getQuantityFree().intValue());
							}
						}
					} else {
						mapDetail = new HashMap<String, RptBCCTKMDetailVO>();
						RptBCCTKMDetailVO rpt = new RptBCCTKMDetailVO();
						rpt.setLstBCCTKM_RecordDetailVO(new ArrayList<RptBCCTKM_RecordDetailVO>());
						rpt.getLstBCCTKM_RecordDetailVO().add(child);
						rpt.setShopId(child.getShopId().longValue());
						rpt.setShopName(child.getShopName());
						rpt.setShopAddress(child.getShopAddress());
						rpt.setPromotionProgramCode(child.getPromotionProgramCode());
						rpt.setPromotionProgramDecription(child.getPromotionProgramDecription());
						if (child.getFreeItem().intValue() == 0) {
							if (child.getQuantity() != null) {
								rpt.setTotalQuantitySale(rpt.getTotalQuantitySale() + child.getQuantity().intValue());
							}
						} else {
							if (child.getQuantityFree() != null) {
								rpt.setTotalQuantityFree(rpt.getTotalQuantityFree() + child.getQuantityFree().intValue());
							}
						}
						mapDetail.put(child.getPromotionProgramCode(), rpt);
						mapLstRecord.put(child.getShopId().longValue(), mapDetail);
						lstShopId.add(child.getShopId().longValue());
					}
				}
				for (Long l : lstShopId) {
					RptBCCTKMVO rpt = new RptBCCTKMVO();
					List<RptBCCTKMDetailVO> lstDetail = new ArrayList<RptBCCTKMDetailVO>();
					Map<String, RptBCCTKMDetailVO> mapDetail = mapLstRecord.get(l);
					Iterator<?> iter = mapDetail.entrySet().iterator();
					while (iter.hasNext()) {
						Map.Entry mEntry = (Map.Entry) iter.next();
						lstDetail.add((RptBCCTKMDetailVO) mEntry.getValue());
					}
					rpt.setLstBCCTKMDetailVO(lstDetail);
					rpt.setShopId(l);
					rpt.setShopName(lstDetail.get(0).getShopName());
					rpt.setShopAddress(lstDetail.get(0).getShopAddress());
					lst.add(rpt);
				}
			}
			return lst;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}

	}

	/**
	 * Lay bang ke chung tu hang hoa mua vao
	 * 
	 * @author vuonghn
	 */
	@Override
	public List<RptBKCTHHMVVO> getRptBKCTHHMV(Long shopId, Date fDate, Date tDate) throws DataAccessException {
		try {
			List<Object> inParams = new ArrayList<Object>();
			inParams.add(2);
			inParams.add(shopId);
			inParams.add(java.sql.Types.NUMERIC);
			inParams.add(3);
			inParams.add(new java.sql.Date(fDate.getTime()));
			inParams.add(java.sql.Types.DATE);
			inParams.add(4);
			inParams.add(new java.sql.Date(tDate.getTime()));
			inParams.add(java.sql.Types.DATE);
			String sql = "{call PKG_SHOP_REPORT.BAOCAO_BKCTHHMV(?, ?, ?, ?)}";

			List<RptBKCTHHMV_RecordVO> listRecord = repo.getListByQueryDynamicFromPackage(RptBKCTHHMV_RecordVO.class, sql, inParams, null);

			List<RptBKCTHHMVVO> result = new ArrayList<RptBKCTHHMVVO>();
			if (listRecord != null && listRecord.size() > 0) {
				RptBKCTHHMVVO rpt = new RptBKCTHHMVVO();
				rpt.setLstRecord(listRecord);
				rpt.setShopName(listRecord.get(0).getShopName());
				rpt.setShopAddress(listRecord.get(0).getShopAddress());
				for (RptBKCTHHMV_RecordVO child : listRecord) {
					if (child.getTotal() != null) {
						rpt.setTotalPO(rpt.getTotalPO().add(child.getTotal()));
					}
					if (child.getDiscount() != null) {
						rpt.setTotalDiscount(rpt.getTotalDiscount().add(child.getDiscount()));
					}
					if (child.getVat() != null) {
						rpt.setTotalVAT(rpt.getTotalVAT().add(child.getVat()));
					}
					if (child.getAmount() != null) {
						rpt.setTotalAmount(rpt.getTotalAmount().add(child.getAmount()));
					}
				}
				result.add(rpt);
			}
			return result;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	/**
	 * Bao cao PO Auto NPP
	 * 
	 * @author namlb
	 */
	@Override
	public List<RptBCPOAUTONPP_5_10> getRptBCPOAutoNPP(Long shopId, Date fDate, Date tDate) throws DataAccessException {
		try {
			List<Object> inParams = new ArrayList<Object>();
			inParams.add(2);
			inParams.add(shopId);
			inParams.add(java.sql.Types.NUMERIC);
			inParams.add(3);
			inParams.add(new java.sql.Date(fDate.getTime()));
			inParams.add(java.sql.Types.DATE);
			inParams.add(4);
			inParams.add(new java.sql.Date(tDate.getTime()));
			inParams.add(java.sql.Types.DATE);
			String sql = "{call PKG_SHOP_REPORT.BCPOAUTONPP_5_10(?, ?, ?, ?)}";

			List<RptBCPOAUTONPP_5_10> listRecord = repo.getListByQueryDynamicFromPackage(RptBCPOAUTONPP_5_10.class, sql, inParams, null);

			return listRecord;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@Override
	public List<RptBCTDDSTKHNHMHLv1VO> getBCTDDSTKHNHMHReport(Long shopId, String lstCustomerCode, Date fDate, Date tDate) throws DataAccessException {
		try {
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("fromDate");
			inParams.add(fDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(tDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("lstCustomerCode");
			inParams.add(lstCustomerCode);
			inParams.add(StandardBasicTypes.STRING);

			List<RptBCTDDSTKHNHMHDetailVO> lstData = repo.getListByNamedQuery(RptBCTDDSTKHNHMHDetailVO.class, "PKG_SHOP_REPORT.BAOCAO_TDDSTKHNHMH", inParams);
			List<RptBCTDDSTKHNHMHLv1VO> result = new ArrayList<RptBCTDDSTKHNHMHLv1VO>();
			if (null != lstData && lstData.size() > 0) {
				Map<String, List<RptBCTDDSTKHNHMHDetailVO>> mapLv1 = GroupUtility.collectionToMap(lstData, "customerCode");
				for (String keyMapCustomerCode : mapLv1.keySet()) {
					RptBCTDDSTKHNHMHLv1VO voLv1 = new RptBCTDDSTKHNHMHLv1VO();
					voLv1.safeSetNull();
					voLv1.setCustomerCode(keyMapCustomerCode);
					List<RptBCTDDSTKHNHMHCatLv2VO> lstLv2 = new ArrayList<RptBCTDDSTKHNHMHCatLv2VO>();

					List<RptBCTDDSTKHNHMHDetailVO> lstDataLv1 = mapLv1.get(keyMapCustomerCode);
					Map<String, List<RptBCTDDSTKHNHMHDetailVO>> mapLv2 = GroupUtility.collectionToMap(lstDataLv1, "catCode");
					for (String keyMapCatCode : mapLv2.keySet()) {
						RptBCTDDSTKHNHMHCatLv2VO voLv2 = new RptBCTDDSTKHNHMHCatLv2VO();
						voLv2.safeSetNull();
						voLv2.setCatCode(keyMapCatCode);
						List<RptBCTDDSTKHNHMHSubCatLv3VO> lstLv3 = new ArrayList<RptBCTDDSTKHNHMHSubCatLv3VO>();

						List<RptBCTDDSTKHNHMHDetailVO> lstDataLv2 = mapLv2.get(keyMapCatCode);
						Map<String, List<RptBCTDDSTKHNHMHDetailVO>> mapLv3 = GroupUtility.collectionToMap(lstDataLv2, "subCatCode");
						for (String keyMapSubCatCode : mapLv3.keySet()) {
							RptBCTDDSTKHNHMHSubCatLv3VO voLv3 = new RptBCTDDSTKHNHMHSubCatLv3VO();
							voLv3.safeSetNull();
							voLv3.setSubCatCode(keyMapSubCatCode);
							List<RptBCTDDSTKHNHMHBrandLv4VO> lstLv4 = new ArrayList<RptBCTDDSTKHNHMHBrandLv4VO>();

							List<RptBCTDDSTKHNHMHDetailVO> lstDataLv3 = mapLv3.get(keyMapSubCatCode);
							Map<String, List<RptBCTDDSTKHNHMHDetailVO>> mapLv4 = GroupUtility.collectionToMap(lstDataLv3, "brandCode");
							for (String keyMapBrandCode : mapLv4.keySet()) {
								RptBCTDDSTKHNHMHBrandLv4VO voLv4 = new RptBCTDDSTKHNHMHBrandLv4VO();
								voLv4.safeSetNull();
								voLv4.setBrandCode(keyMapBrandCode);
								List<RptBCTDDSTKHNHMHFlavourLv5VO> lstLv5 = new ArrayList<RptBCTDDSTKHNHMHFlavourLv5VO>();

								List<RptBCTDDSTKHNHMHDetailVO> lstDataLv4 = mapLv4.get(keyMapBrandCode);
								Map<String, List<RptBCTDDSTKHNHMHDetailVO>> mapLv5 = GroupUtility.collectionToMap(lstDataLv4, "flavourCode");
								for (String keyMapFlavourCode : mapLv5.keySet()) {
									RptBCTDDSTKHNHMHFlavourLv5VO voLv5 = new RptBCTDDSTKHNHMHFlavourLv5VO();
									voLv5.setFlavourCode(keyMapFlavourCode);
									List<RptBCTDDSTKHNHMHDetailVO> lstDataLv5 = mapLv5.get(keyMapFlavourCode);
									voLv5.safeSetNull();
									voLv5.setLstData(lstDataLv5);

									voLv5.setCustomerId(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getCustomerId() : null);
									voLv5.setCustomerCode(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getCustomerCode() : null);
									voLv5.setCustomerName(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getCustomerName() : null);
									voLv5.setShortCode(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getShortCode() : null);

									voLv5.setCatId(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getCatId() : null);
									voLv5.setCatCode(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getCatCode() : null);
									voLv5.setCatName(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getCatName() : null);

									voLv5.setSubCatId(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getSubCatId() : null);
									voLv5.setSubCatCode(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getSubCatCode() : null);
									voLv5.setSubCatName(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getSubCatName() : null);

									voLv5.setBrandId(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getBrandId() : null);
									voLv5.setBrandCode(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getBrandCode() : null);
									voLv5.setBrandName(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getBrandName() : null);

									voLv5.setFlavourId(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getFlavourId() : null);
									voLv5.setFlavourName(lstDataLv5.size() > 0 ? lstDataLv5.get(0).getFlavourName() : null);

									lstLv5.add(voLv5);
								}
								voLv4.setLstDataLv5(lstLv5);
								voLv4.setCustomerId(lstDataLv4.size() > 0 ? lstDataLv4.get(0).getCustomerId() : null);
								voLv4.setCustomerCode(lstDataLv4.size() > 0 ? lstDataLv4.get(0).getCustomerCode() : null);
								voLv4.setCustomerName(lstDataLv4.size() > 0 ? lstDataLv4.get(0).getCustomerName() : null);
								voLv4.setShortCode(lstDataLv4.size() > 0 ? lstDataLv4.get(0).getShortCode() : null);

								voLv4.setCatId(lstDataLv4.size() > 0 ? lstDataLv4.get(0).getCatId() : null);
								voLv4.setCatCode(lstDataLv4.size() > 0 ? lstDataLv4.get(0).getCatCode() : null);
								voLv4.setCatName(lstDataLv4.size() > 0 ? lstDataLv4.get(0).getCatName() : null);

								voLv4.setSubCatId(lstDataLv4.size() > 0 ? lstDataLv4.get(0).getSubCatId() : null);
								voLv4.setSubCatCode(lstDataLv4.size() > 0 ? lstDataLv4.get(0).getSubCatCode() : null);
								voLv4.setSubCatName(lstDataLv4.size() > 0 ? lstDataLv4.get(0).getSubCatName() : null);

								voLv4.setBrandId(lstDataLv4.size() > 0 ? lstDataLv4.get(0).getBrandId() : null);
								voLv4.setBrandName(lstDataLv4.size() > 0 ? lstDataLv4.get(0).getBrandName() : null);

								lstLv4.add(voLv4);
							}
							voLv3.setLstDataLv4(lstLv4);
							voLv3.setCustomerId(lstDataLv3.size() > 0 ? lstDataLv3.get(0).getCustomerId() : null);
							voLv3.setCustomerCode(lstDataLv3.size() > 0 ? lstDataLv3.get(0).getCustomerCode() : null);
							voLv3.setCustomerName(lstDataLv3.size() > 0 ? lstDataLv3.get(0).getCustomerName() : null);
							voLv3.setShortCode(lstDataLv3.size() > 0 ? lstDataLv3.get(0).getShortCode() : null);

							voLv3.setCatId(lstDataLv3.size() > 0 ? lstDataLv3.get(0).getCatId() : null);
							voLv3.setCatCode(lstDataLv3.size() > 0 ? lstDataLv3.get(0).getCatCode() : null);
							voLv3.setCatName(lstDataLv3.size() > 0 ? lstDataLv3.get(0).getCatName() : null);

							voLv3.setSubCatId(lstDataLv3.size() > 0 ? lstDataLv3.get(0).getSubCatId() : null);
							voLv3.setSubCatName(lstDataLv3.size() > 0 ? lstDataLv3.get(0).getSubCatName() : null);

							lstLv3.add(voLv3);
						}
						voLv2.setLstDataLv3(lstLv3);
						voLv2.setCustomerId(lstDataLv2.size() > 0 ? lstDataLv2.get(0).getCustomerId() : null);
						voLv2.setCustomerCode(lstDataLv2.size() > 0 ? lstDataLv2.get(0).getCustomerCode() : null);
						voLv2.setCustomerName(lstDataLv2.size() > 0 ? lstDataLv2.get(0).getCustomerName() : null);
						voLv2.setShortCode(lstDataLv2.size() > 0 ? lstDataLv2.get(0).getShortCode() : null);

						voLv2.setCatId(lstDataLv2.size() > 0 ? lstDataLv2.get(0).getCatId() : null);
						voLv2.setCatName(lstDataLv2.size() > 0 ? lstDataLv2.get(0).getCatName() : null);

						lstLv2.add(voLv2);
					}
					voLv1.setLstDataLv2(lstLv2);
					voLv1.setCustomerId(lstDataLv1.size() > 0 ? lstDataLv1.get(0).getCustomerId() : null);
					voLv1.setCustomerName(lstDataLv1.size() > 0 ? lstDataLv1.get(0).getCustomerName() : null);
					voLv1.setShortCode(lstDataLv1.size() > 0 ? lstDataLv1.get(0).getShortCode() : null);
					result.add(voLv1);
				}
			}
			return result;

		} catch (Exception e) {
			throw new DataAccessException();
		}
	}

	/**
	 * DS3 - Bao cao thoi gian ban hang hieu qua cua nhan vien ban hang
	 * 
	 * @author hunglm16
	 * @since January 15,2014
	 * @modify: tulv2 08.10.2014 Them 2 tham so staffIdRoot, flagCMS phuc vu
	 *          phan quyen
	 * */
	@Override
	public List<RptBCTGBHHQCNVBH_DS3_VO> getRptBCDS3(String strListShopId, String listGSNPPCode, String listStaffCode, Date rptDate, Long fromHour, Long fromMinute, Long toHour, Long toMinute, Long sprTime, Long staffIdRoot, Integer flagCMS)
			throws BusinessException {
		try {
			List<RptBCTGBHHQCNVBH_DS3_VO> lstResult = new ArrayList<RptBCTGBHHQCNVBH_DS3_VO>();
			//DS3 - Bao cao thoi gian ban hang hieu qua cua nhan vien ban hang
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.BAOCAO_DS3(?, :strListShopId, :lstGSNPPCode, :lstStaffCode, :rptDate, :fromHour, :fromMinute, :toHour, :toMinute, :sprTime, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);

			params.add(3);
			params.add(listGSNPPCode);
			params.add(java.sql.Types.VARCHAR);

			params.add(4);
			params.add(listStaffCode);
			params.add(java.sql.Types.VARCHAR);

			params.add(5);
			params.add(new java.sql.Date(rptDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(6);
			params.add(fromHour);
			params.add(java.sql.Types.NUMERIC);

			params.add(7);
			params.add(fromMinute);
			params.add(java.sql.Types.NUMERIC);

			params.add(8);
			params.add(toHour);
			params.add(java.sql.Types.NUMERIC);

			params.add(9);
			params.add(toMinute);
			params.add(java.sql.Types.NUMERIC);

			params.add(10);
			params.add(sprTime);
			params.add(java.sql.Types.NUMERIC);

			params.add(11);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);

			params.add(12);
			params.add(flagCMS);
			params.add(java.sql.Types.NUMERIC);

			lstResult = repo.getListByQueryDynamicFromPackage(RptBCTGBHHQCNVBH_DS3_VO.class, sql.toString(), params, null);
			for (RptBCTGBHHQCNVBH_DS3_VO rpt : lstResult) {
				rpt.safeSetNull();
			}
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * B?�o c?�o bảng k?� chi tiết h?�a đơn GTGT
	 * 
	 * @author tungtt21
	 */
	@Override
	public List<RptInvDetailStatVO> getBKCTHDGTGT(Long shopId, String staffCode, Integer tax, Date fromDate, Date toDate, Integer status) throws BusinessException {
		try {
			List<RptInvDetailStatVO> lstRes = new ArrayList<RptInvDetailStatVO>();
			List<Object> inParams = new ArrayList<Object>();

			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("staffCode");
			inParams.add(staffCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("tax");
			inParams.add(tax);
			inParams.add(StandardBasicTypes.INTEGER);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("status");
			inParams.add(status);
			inParams.add(StandardBasicTypes.INTEGER);

			List<RptInvDetailStatDetailVO> listRecord = repo.getListByNamedQuery(RptInvDetailStatDetailVO.class, "PKG_SHOP_REPORT.BAOCAO_BKCTHDGTGT", inParams);
			//List<RptInvDetailStatDetailVO> listRecord = repo.getListByNamedQuery(RptInvDetailStatDetailVO.class, "PKG_SHOP_REPORT.BAOCAO_BKCTHDGTGT", inParams);

			Map<String, RptInvDetailStatVO> mapStaff = new HashMap<String, RptInvDetailStatVO>();
			List<String> lstInvoiceNumber = new ArrayList<String>();

			if (listRecord != null && listRecord.size() > 0) {
				for (RptInvDetailStatDetailVO child : listRecord) {
					if (mapStaff.get(child.getInvoiceNumber()) != null) {
						RptInvDetailStatVO rpt = mapStaff.get(child.getInvoiceNumber());
						rpt.getListDetail().add(child);
					} else {
						RptInvDetailStatVO rpt = new RptInvDetailStatVO();
						rpt.setListDetail(new ArrayList<RptInvDetailStatDetailVO>());
						rpt.getListDetail().add(child);
						mapStaff.put(child.getInvoiceNumber(), rpt);
						lstInvoiceNumber.add(child.getInvoiceNumber());
					}
				}
				for (String invoiceNUmber : lstInvoiceNumber) {
					lstRes.add(mapStaff.get(invoiceNUmber));
				}
			}

			for (RptInvDetailStatVO res : lstRes) {
				List<RptInvDetailStatDetailVO> lst = res.getListDetail();
				if (lst != null && lst.size() > 0) {
					res.setInvoiceNumber(lst.get(0).getInvoiceNumber());
					BigDecimal totalSku, totalAmount, totalFinalAmount, totalDiscount, totalTaxAmount;
					totalSku = totalAmount = totalFinalAmount = totalDiscount = totalTaxAmount = BigDecimal.ZERO;
					for (RptInvDetailStatDetailVO child : lst) {
						if (child.getQuantity() != null) {
							totalSku = totalSku.add(child.getQuantity());
						}

						if (child.getAmount() != null) {
							totalAmount = totalAmount.add(child.getAmount());
						}

						if (child.getFinalAmount() != null) {
							totalFinalAmount = totalFinalAmount.add(child.getFinalAmount());
						}

						if (child.getTaxAmount() != null) {
							totalTaxAmount = totalTaxAmount.add(child.getTaxAmount());
						}
						if (child.getDiscount() != null) {
							totalDiscount = totalDiscount.add(child.getDiscount());
						}
					}
					
					res.setTotalSku(totalSku);
					res.setTotalAmount(totalAmount);

					res.setTotalDiscount(totalDiscount);
					if (totalDiscount.compareTo(BigDecimal.ZERO) == 0) {
						res.setTotalFinalAmount(totalFinalAmount);
						res.setTotalTaxAmount(totalTaxAmount);
					} else {
						BigDecimal discount = totalDiscount.divide(new BigDecimal(11), MathContext.DECIMAL128).setScale(2, BigDecimal.ROUND_HALF_EVEN);

						totalAmount = totalAmount.subtract(totalDiscount).add(discount);
						res.setTotalAmount(totalAmount);

						totalTaxAmount = totalTaxAmount.subtract(discount);
						res.setTotalTaxAmount(totalTaxAmount);

						totalFinalAmount = totalFinalAmount.subtract(totalDiscount);
						res.setTotalFinalAmount(totalFinalAmount);
					}

				}
			}

			return lstRes;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptTDCTDSVO> getBCTDCTDSReport(Long shopId, String staffCode, Integer month, Integer year) throws BusinessException {
		try {
			List<RptTDCTDSVO> lstRes = new ArrayList<RptTDCTDSVO>();
			List<Object> inParams = new ArrayList<Object>();

			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("staffCode");
			inParams.add(staffCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("month");
			inParams.add(month);
			inParams.add(StandardBasicTypes.INTEGER);

			inParams.add("year");
			inParams.add(year);
			inParams.add(StandardBasicTypes.INTEGER);

			List<RptTDCTDSVODetail> listRecord = repo.getListByNamedQuery(RptTDCTDSVODetail.class, "PKG_SHOP_REPORT.BAOCAO_TDCTDS", inParams);

			Map<String, RptTDCTDSVO> map = new HashMap<String, RptTDCTDSVO>();
			List<String> lstStaffCode = new ArrayList<String>();

			if (listRecord != null && listRecord.size() > 0) {
				for (RptTDCTDSVODetail child : listRecord) {
					if (map.get(child.getStaffCode()) != null) {
						RptTDCTDSVO rpt = map.get(child.getStaffCode());
						rpt.getListDetail().add(child);
					} else {
						RptTDCTDSVO rpt = new RptTDCTDSVO();
						rpt.setListDetail(new ArrayList<RptTDCTDSVODetail>());
						rpt.getListDetail().add(child);
						map.put(child.getStaffCode(), rpt);
						lstStaffCode.add(child.getStaffCode());
					}
				}
				for (String invoiceNUmber : lstStaffCode) {
					lstRes.add(map.get(invoiceNUmber));
				}
			}

			for (RptTDCTDSVO res : lstRes) {
				List<RptTDCTDSVODetail> lst = res.getListDetail();
				if (lst != null && lst.size() > 0) {
					res.setStaffCode(lst.get(0).getStaffCode());
					res.setStaffName(lst.get(0).getStaffName());

					BigDecimal sumQuantityPlan, sumAmountPlan, sumQuantityReal, sumAmountReal;
					sumQuantityPlan = sumAmountPlan = sumQuantityReal = sumAmountReal = BigDecimal.ZERO;

					for (RptTDCTDSVODetail child : lst) {
						if (child.getQuantityPlan() != null) {
							sumQuantityPlan = sumQuantityPlan.add(child.getQuantityPlan());
						}

						if (child.getAmountPlan() != null) {
							sumAmountPlan = sumAmountPlan.add(child.getAmountPlan());
						}

						if (child.getQuantityReal() != null) {
							sumQuantityReal = sumQuantityReal.add(child.getQuantityReal());
						}

						if (child.getAmountReal() != null) {
							sumAmountReal = sumAmountReal.add(child.getAmountReal());
						}
					}
					res.setSumQuantityPlan(sumQuantityPlan);
					res.setSumAmountPlan(sumAmountPlan);
					res.setSumQuantityReal(sumQuantityReal);
					res.setSumAmountReal(sumAmountReal);
				}
			}

			return lstRes;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * DS3.11 - Bao cao chi tiet tra hang trung bay
	 * 
	 * @author hunglm16
	 * @since January 21,2014
	 * */
	@Override
	public List<PROC_3_11_BCCTCTHTB> getRptBCDS3_11(Long shopId, Integer year, String lstDisProCode, String customerCode) throws BusinessException {
		try {
			List<PROC_3_11_BCCTCTHTB> lstResult = new ArrayList<PROC_3_11_BCCTCTHTB>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.PROC_3_11_BCCTCTHTB(?, :shopId, :year, :lstDisProCode, :customerCode)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(year);
			params.add(java.sql.Types.NUMERIC);
			params.add(4);
			params.add(lstDisProCode);
			params.add(java.sql.Types.VARCHAR);
			params.add(5);
			params.add(customerCode);
			params.add(java.sql.Types.VARCHAR);

			lstResult = repo.getListByQueryDynamicFromPackage(PROC_3_11_BCCTCTHTB.class, sql.toString(), params, null);
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author hunglm16
	 * @since January 25, 2014
	 * @description report Dt3: Bao cao danh sach don hang trong ngay theo NVBH
	 * */
	@Override
	public List<RptBCDSDHTNTNVBH_DS3_1_VO> rptDSDHTNTNVBH3_1(Long shopId, String lstStaff, Date fDate, Date tDate) throws BusinessException {
		try {
			//DT3 - Danh sach don hang trong ngay theo NVBH
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.PROC_DT3_1_DSDHTNTNVBH(?, :shopId, :lstStaff, :fDate, :tDate)");
			//			sql.append("call hunglm16.PROC_DT3_1_DSDHTNTNVBH(?, :shopId, :lstStaff, :fDate, :tDate)");//danh cho Test du lieu
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(lstStaff);
			params.add(java.sql.Types.VARCHAR);
			params.add(4);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(5);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);
			return repo.getListByQueryDynamicFromPackage(RptBCDSDHTNTNVBH_DS3_1_VO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	//PROC_DT3_1_DSDHTNTNVBH

	/**
	 * @author tungtt21 B?�o c?�o ng?�y
	 */
	@Override
	public List<RptBCNVO> getBCNReport(Long shopId, Long parentStaffId, Date fDate, Date tDate, Date nextMonth, Date lastMonth, boolean checkMap) throws BusinessException {
		try {
			List<RptBCNVO> lstRes = new ArrayList<RptBCNVO>();
			List<Object> inParams = new ArrayList<Object>();

			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("parentStaffId");
			inParams.add(parentStaffId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("fromDate");
			inParams.add(fDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(tDate);
			inParams.add(StandardBasicTypes.DATE);

			Calendar fromDate = Calendar.getInstance();
			fromDate.setTime(fDate);

			// So ngay ban hang trong thang
			Integer soNgayBanHang = saleDayDAO.getSaleDayByDate(fDate);

			// So ngay ban hang da qua
			Integer soNgayThucHien = saleDayMgr.getNumberWorkingDayByFromDateAndToDate(fromDate.getTime(), tDate);

			inParams.add("planDay");
			inParams.add(soNgayBanHang.longValue());
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("realDay");
			inParams.add(soNgayThucHien.longValue());
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("nextMonth");
			inParams.add(nextMonth);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("lastMonth");
			inParams.add(lastMonth);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("checkMap");
			if (checkMap) {
				inParams.add(1);
			} else {
				inParams.add(0);
			}
			inParams.add(StandardBasicTypes.INTEGER);

			lstRes = repo.getListByNamedQuery(RptBCNVO.class, "PKG_SHOP_REPORT.BAOCAO_NGAY", inParams);

			return lstRes;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptBCVT9> exportBCVT9(String strListShopId, String strListStaffId, Date fromDate, Date toDate, String typeId, String displayId, Long staffId, Integer flagCMS) throws BusinessException {
		try {
			StringBuilder sql = new StringBuilder();
			List<Object> inParams = new ArrayList<Object>();
			sql.append(" call pkg_shop_report.PROC_VT9(?,:strListShopId,:strListStaffId,:fromDate,:toDate,:typeId,:displayId,:staffId,:flagCMS) ");
			inParams.add(2);
			inParams.add(strListShopId);
			inParams.add(Types.VARCHAR);

			inParams.add(3);
			inParams.add(strListStaffId);
			inParams.add(Types.VARCHAR);

			inParams.add(4);
			inParams.add(new java.sql.Date(fromDate.getTime()));
			inParams.add(Types.DATE);

			inParams.add(5);
			inParams.add(new java.sql.Date(toDate.getTime()));
			inParams.add(Types.DATE);

			inParams.add(6);
			inParams.add(typeId);
			inParams.add(Types.VARCHAR);

			inParams.add(7);
			inParams.add(displayId);
			inParams.add(Types.VARCHAR);

			inParams.add(8);
			inParams.add(staffId);
			inParams.add(Types.NUMERIC);

			inParams.add(9);
			inParams.add(flagCMS);
			inParams.add(Types.NUMERIC);

			List<RptBCVT9> lstResult = repo.getListByQueryDynamicFromPackage(RptBCVT9.class, sql.toString(), inParams, null);
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptBCVT6> getBCChamCong(String shopId, String dateStr, String gsnppId, String nvbhId, Long staffIdRoot, Long flagCMS) throws BusinessException {
		if (null == shopId || null == dateStr) {
			throw new IllegalArgumentException("shop or date is null");
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.PROC_VT6(?, :shopId, :dateStr, :gsnppId, :nvbhId, :staffIdRoot, :flagCMS) ");
			List<Object> inParams = new ArrayList<Object>();
			inParams.add(2);
			inParams.add(shopId);
			inParams.add(Types.VARCHAR);
			inParams.add(3);
			inParams.add(dateStr.trim());
			inParams.add(Types.VARCHAR);
			inParams.add(4);
			inParams.add(gsnppId);
			inParams.add(Types.VARCHAR);
			inParams.add(5);
			inParams.add(nvbhId);
			inParams.add(Types.VARCHAR);
			inParams.add(6);
			inParams.add(staffIdRoot);
			inParams.add(Types.NUMERIC);
			inParams.add(7);
			inParams.add(flagCMS);
			inParams.add(Types.NUMERIC);

			List<RptBCVT6> lstResult = repo.getListByQueryDynamicFromPackage(RptBCVT6.class, sql.toString(), inParams, null);

			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * VT7 - Bao cao ghe tham khach hang
	 * 
	 * @author vuonghn
	 */
	/*
	 * @Override public List<RptBCVT7> getBCGheThamKHVT7(String lstShopId,
	 * String lstGSNPPId, String fromDate, String toDate) throws
	 * BusinessException{ if (null == fromDate || null == toDate) { throw new
	 * IllegalArgumentException("date is null"); } try { StringBuilder sql = new
	 * StringBuilder(); sql.append(
	 * "call PKG_SHOP_REPORT.PROC_VT7(?, :lstShopId, :gsnppId, :fromDateStr, :toDateStr )"
	 * ); List<Object> inParams = new ArrayList<Object>(); inParams.add(2);
	 * inParams.add(lstShopId); inParams.add(Types.VARCHAR); inParams.add(3);
	 * inParams.add(lstGSNPPId); inParams.add(Types.VARCHAR); inParams.add(4);
	 * inParams.add(fromDate); inParams.add(Types.VARCHAR); inParams.add(5);
	 * inParams.add(toDate); inParams.add(Types.VARCHAR);
	 * 
	 * List<RptBCVT7> lstResult =
	 * repo.getListByQueryDynamicFromPackage(RptBCVT7.class, sql.toString(),
	 * inParams, null);
	 * 
	 * return lstResult; } catch (DataAccessException e) { throw new
	 * BusinessException(e); } }
	 */

	@Override
	public List<RptBCVT7> getBCGheThamKHVT7(String lstShopId, String lstGSNPPId, String fromDate, String toDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException("date is null");
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.PROC_VT7(?, :lstShopId, :gsnppId, :fromDateStr, :toDateStr, :staffIdRoot, :flagCMS )");
			List<Object> inParams = new ArrayList<Object>();
			inParams.add(2);
			inParams.add(lstShopId);
			inParams.add(Types.VARCHAR);
			inParams.add(3);
			inParams.add(lstGSNPPId);
			inParams.add(Types.VARCHAR);
			inParams.add(4);
			inParams.add(fromDate);
			inParams.add(Types.VARCHAR);
			inParams.add(5);
			inParams.add(toDate);
			inParams.add(Types.VARCHAR);

			inParams.add(6);
			inParams.add(staffIdRoot);
			inParams.add(Types.NUMERIC);

			inParams.add(7);
			inParams.add(flagCMS);
			inParams.add(Types.NUMERIC);

			List<RptBCVT7> lstResult = repo.getListByQueryDynamicFromPackage(RptBCVT7.class, sql.toString(), inParams, null);

			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * VT7.1 - Bao cao chi tiet ghe tham khach hang
	 * 
	 * @author phuongvm
	 */
	/*
	 * @Override public List<RptBCVT7_1> getBCVT7_1(String lstShopId, String
	 * lstGSNPPId, String fromDate, String toDate) throws BusinessException{ if
	 * (null == fromDate || null == toDate) { throw new
	 * IllegalArgumentException("date is null"); } try { StringBuilder sql = new
	 * StringBuilder(); sql.append(
	 * "call PKG_SHOP_REPORT.PROC_VT7_1(?, :lstShopId, :gsnppId, :fromDateStr, :toDateStr )"
	 * ); List<Object> inParams = new ArrayList<Object>(); inParams.add(2);
	 * inParams.add(lstShopId); inParams.add(Types.VARCHAR); inParams.add(3);
	 * inParams.add(lstGSNPPId); inParams.add(Types.VARCHAR); inParams.add(4);
	 * inParams.add(fromDate); inParams.add(Types.VARCHAR); inParams.add(5);
	 * inParams.add(toDate); inParams.add(Types.VARCHAR);
	 * 
	 * List<RptBCVT7_1> lstResult =
	 * repo.getListByQueryDynamicFromPackage(RptBCVT7_1.class, sql.toString(),
	 * inParams, null);
	 * 
	 * return lstResult; } catch (DataAccessException e) { throw new
	 * BusinessException(e); } }
	 */

	@Override
	public List<RptBCVT7_1> getBCVT7_1(String lstShopId, String lstGSNPPId, String fromDate, String toDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException("date is null");
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.PROC_VT7_1(?, :lstShopId, :gsnppId, :fromDateStr, :toDateStr, :staffIdRoot, :flagCMS )");
			List<Object> inParams = new ArrayList<Object>();
			inParams.add(2);
			inParams.add(lstShopId);
			inParams.add(Types.VARCHAR);
			inParams.add(3);
			inParams.add(lstGSNPPId);
			inParams.add(Types.VARCHAR);
			inParams.add(4);
			inParams.add(fromDate);
			inParams.add(Types.VARCHAR);
			inParams.add(5);
			inParams.add(toDate);
			inParams.add(Types.VARCHAR);

			inParams.add(6);
			inParams.add(staffIdRoot);
			inParams.add(Types.NUMERIC);

			inParams.add(7);
			inParams.add(flagCMS);
			inParams.add(Types.NUMERIC);
			List<RptBCVT7_1> lstResult = repo.getListByQueryDynamicFromPackage(RptBCVT7_1.class, sql.toString(), inParams, null);

			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * VT10 - Bao cao ket qua cham trung bay
	 * 
	 * @author tientv11
	 */
	@Override
	public List<RptBCVT10> exportBCVT10(String lstShopId, String lstGSMTId, String fromDate, String toDate, Long staffId) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException("date is null");
		}
		try {
			StringBuilder sql = new StringBuilder();
			List<Object> inParams = new ArrayList<Object>();
			sql.append(" call PKG_HO_REPORT.PROC_VT10(?,:lstShopId,:lstGSMTId,:fromDateStr,:toDateStr,:staffId ) ");
			inParams.add(2);
			inParams.add(lstShopId);
			inParams.add(Types.VARCHAR);

			inParams.add(3);
			inParams.add(lstGSMTId);
			inParams.add(Types.VARCHAR);

			inParams.add(4);
			inParams.add(fromDate);
			inParams.add(Types.VARCHAR);

			inParams.add(5);
			inParams.add(toDate);
			inParams.add(Types.VARCHAR);

			inParams.add(6);
			inParams.add(staffId);
			inParams.add(Types.NUMERIC);

			List<RptBCVT10> lstResult = repo.getListByQueryDynamicFromPackage(RptBCVT10.class, sql.toString(), inParams, null);
			for (RptBCVT10 rpt : lstResult) {
				rpt.safeSetNull();
			}
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptBCVT12> exportBCVT12(String lstShopId, String displayProgrameId, String strDate, Long staffId) throws BusinessException {

		try {
			if (null == strDate) {
				throw new IllegalArgumentException("date is null");
			}

			StringBuilder sql = new StringBuilder();
			List<Object> inParams = new ArrayList<Object>();
			sql.append(" call pkg_shop_report.PROC_VT12(?,:lstshopid,:cttbid,:strdate,:staffId ) ");
			inParams.add(2);
			inParams.add(lstShopId);
			inParams.add(Types.VARCHAR);

			inParams.add(3);
			inParams.add(displayProgrameId);
			inParams.add(Types.VARCHAR);

			inParams.add(4);
			inParams.add(strDate);
			inParams.add(Types.VARCHAR);

			inParams.add(5);
			inParams.add(staffId);
			inParams.add(Types.NUMERIC);

			List<RptBCVT12> lstResult = repo.getListByQueryDynamicFromPackage(RptBCVT12.class, sql.toString(), inParams, null);
			for (RptBCVT12 rpt : lstResult) {
				rpt.safeSetNull();
			}
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptBCVT11> exportBCVT11(String lstShopId, String fromDate, String toDate, Integer flagCMS, Long staffId) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException("date is null");
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.PROC_VT11(?, :lstShopId, :fromDateStr, :toDateStr, :flagCMS, :staffId )");
			List<Object> inParams = new ArrayList<Object>();
			inParams.add(2);
			inParams.add(lstShopId);
			inParams.add(Types.VARCHAR);

			inParams.add(3);
			inParams.add(fromDate);
			inParams.add(Types.VARCHAR);

			inParams.add(4);
			inParams.add(toDate);
			inParams.add(Types.VARCHAR);

			inParams.add(5);
			inParams.add(flagCMS);
			inParams.add(Types.NUMERIC);

			inParams.add(6);
			inParams.add(staffId);
			inParams.add(Types.NUMERIC);

			List<RptBCVT11> lstResult = repo.getListByQueryDynamicFromPackage(RptBCVT11.class, sql.toString(), inParams, null);
			for (RptBCVT11 rpt : lstResult) {
				rpt.safeSetNull();
			}
			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Rpt5_5_DCPOCTDVKH> exportDCPOCTDVKH_5_5(Long shopId, Date fDate, Date tDate) throws BusinessException {
		try {
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(Types.NUMERIC);
			params.add(3);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(Types.DATE);
			params.add(4);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(Types.DATE);
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.RPT5_5_DCPOCTDVKH(?, :SHOPID, :FROMDATE, :TODATE)");
			//sql.append("call VUONGHN.RPT5_5_DCPOCTDVKH(?, :SHOPID, :FROMDATE, :TODATE)");
			List<Rpt5_5_DCPOCTDVKH> results = repo.getListByQueryDynamicFromPackage(Rpt5_5_DCPOCTDVKH.class, sql.toString(), params, null);
			for (Rpt5_5_DCPOCTDVKH rpt : results) {
				rpt.safeSetNull();
			}
			return results;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * Bang tong hop doi hang
	 * 
	 * @author tungtt21
	 * 
	 */
	@Override
	public List<Rpt3_16_BTHDH> getBTHDHReport(Long shopId, String catCode, Date fDate, Date tDate) throws BusinessException {
		try {
			List<Rpt3_16_BTHDH> lstRes = new ArrayList<Rpt3_16_BTHDH>();

			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("catCode");
			inParams.add(catCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(tDate);
			inParams.add(StandardBasicTypes.DATE);

			lstRes = repo.getListByNamedQuery(Rpt3_16_BTHDH.class, "PKG_SHOP_REPORT.BC_3_16_BTHDH", inParams);
			return lstRes;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptPDHVNM_5_1VO> getRptBCPDHVNM(Long shopId, Date fDate, Date tDate) throws DataAccessException {
		try {
			List<Object> inParams = new ArrayList<Object>();
			inParams.add(2);
			inParams.add(shopId);
			inParams.add(java.sql.Types.NUMERIC);
			inParams.add(3);
			inParams.add(new java.sql.Date(fDate.getTime()));
			inParams.add(java.sql.Types.DATE);
			inParams.add(4);
			inParams.add(new java.sql.Date(tDate.getTime()));
			inParams.add(java.sql.Types.DATE);
			String sql = "{call PKG_SHOP_REPORT.PDHVNM_5_1(?, ?, ?, ?)}";

			List<RptPDHVNM_5_1VO> listRecord = repo.getListByQueryDynamicFromPackage(RptPDHVNM_5_1VO.class, sql, inParams, null);
			if (!listRecord.isEmpty()) {
				sql = "{call PKG_SHOP_REPORT.PDHVNM_5_1_CT(?, ?)}";
				List<RptPDHVNM_5_1_CT_VO> lstDetail = new ArrayList<RptPDHVNM_5_1_CT_VO>();
				for (int i = 0; i < listRecord.size(); i++) {
					RptPDHVNM_5_1VO row = listRecord.get(i);
					if (row.getPoAutoId() != null) {
						inParams = new ArrayList<Object>();
						inParams.add(2);
						inParams.add(row.getPoAutoId());
						inParams.add(java.sql.Types.NUMERIC);
						lstDetail = repo.getListByQueryDynamicFromPackage(RptPDHVNM_5_1_CT_VO.class, sql, inParams, null);
						listRecord.get(i).setLstDetail(lstDetail);
					}
				}
			}
			return listRecord;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	/**
	 * Bao cao chi tiet khuyen mai theo chuong trinh
	 * 
	 * @author tungtt21
	 * 
	 */
	/*
	 * @Override public List<Rpt3_13_CTKMTCT> getBCCTKMTCT(Long shopId, String
	 * staffCode, Date fDate, Date tDate) throws BusinessException { try {
	 * List<Rpt3_13_CTKMTCT> lstRes = new ArrayList<Rpt3_13_CTKMTCT>();
	 * 
	 * List<Object> inParams = new ArrayList<Object>(); inParams.add("shopId");
	 * inParams.add(shopId == null ? 0 : shopId);
	 * inParams.add(StandardBasicTypes.LONG);
	 * 
	 * inParams.add("staffCode"); inParams.add(staffCode);
	 * inParams.add(StandardBasicTypes.STRING);
	 * 
	 * inParams.add("fromDate"); inParams.add(fDate);
	 * inParams.add(StandardBasicTypes.DATE);
	 * 
	 * inParams.add("toDate"); inParams.add(tDate);
	 * inParams.add(StandardBasicTypes.DATE);
	 * 
	 * List<Rpt3_13_CTKMTCT_Detail> lstDetail = new
	 * ArrayList<Rpt3_13_CTKMTCT_Detail>(); lstDetail =
	 * repo.getListByNamedQuery(Rpt3_13_CTKMTCT_Detail.class,
	 * "PKG_SHOP_REPORT.BC_3_13_CTKMTCT", inParams);
	 * 
	 * 
	 * List<Rpt3_13_CTKMTCT_Date> listDate = new
	 * ArrayList<Rpt3_13_CTKMTCT_Date>(); List<Rpt3_13_CTKMTCT_Staff> listStaff
	 * = new ArrayList<Rpt3_13_CTKMTCT_Staff>(); List<Rpt3_13_CTKMTCT_Customer>
	 * listCustomer = new ArrayList<Rpt3_13_CTKMTCT_Customer>();
	 * 
	 * //map list detail qua list customer Map<String, Rpt3_13_CTKMTCT_Customer>
	 * mapCustomer= new HashMap<String, Rpt3_13_CTKMTCT_Customer>();
	 * List<String> customerCode = new ArrayList<String>(); if (lstDetail !=
	 * null && lstDetail.size() > 0) { for (Rpt3_13_CTKMTCT_Detail child :
	 * lstDetail) { if (mapCustomer.get(child.getCustomer() + child.getStaff() +
	 * DateUtility.toDateSimpleFormatString(child.getOrderDate()) +
	 * child.getProgramName()) != null) { Rpt3_13_CTKMTCT_Customer rpt =
	 * mapCustomer.get(child.getCustomer() + child.getStaff() +
	 * DateUtility.toDateSimpleFormatString(child.getOrderDate()) +
	 * child.getProgramName()); rpt.getListDetail().add(child); } else {
	 * Rpt3_13_CTKMTCT_Customer rpt = new Rpt3_13_CTKMTCT_Customer();
	 * rpt.setListDetail(new ArrayList<Rpt3_13_CTKMTCT_Detail>());
	 * rpt.getListDetail().add(child); mapCustomer.put(child.getCustomer() +
	 * child.getStaff() +
	 * DateUtility.toDateSimpleFormatString(child.getOrderDate()) +
	 * child.getProgramName(), rpt); customerCode.add(child.getCustomer() +
	 * child.getStaff() +
	 * DateUtility.toDateSimpleFormatString(child.getOrderDate()) +
	 * child.getProgramName()); } } for (String code : customerCode) {
	 * listCustomer.add(mapCustomer.get(code)); } }
	 * 
	 * for (Rpt3_13_CTKMTCT_Customer res : listCustomer) {
	 * List<Rpt3_13_CTKMTCT_Detail> lst = res.getListDetail(); if (lst != null
	 * && lst.size() > 0) { res.setCustomer(lst.get(0).getCustomer());
	 * res.setStaff(lst.get(0).getStaff());
	 * res.setOrderDate(lst.get(0).getOrderDate());
	 * res.setProgramName(lst.get(0).getProgramName());
	 * 
	 * BigDecimal num1, num2, num3, num4, num5; num1 = num2 = num3 = num4 = num5
	 * = BigDecimal.ZERO; for (Rpt3_13_CTKMTCT_Detail child : lst) { if
	 * (child.getQuantityPromotion() != null) { num1 =
	 * num1.add(child.getQuantityPromotion()); } if (child.getAmountPromotion()
	 * != null) { num2 = num2.add(child.getAmountPromotion()); } if
	 * (child.getQuantityMPromotion() != null) { num3 =
	 * num3.add(child.getQuantityMPromotion()); } if (child.getMoneyPromotion()
	 * != null) { num4 = num4.add(child.getMoneyPromotion()); } if
	 * (child.getTotal() != null) { num5 = num5.add(child.getTotal()); } }
	 * res.setQuantityPromotion(num1); res.setAmountPromotion(num2);
	 * res.setQuantityMPromotion(num3); res.setMoneyPromotion(num4);
	 * res.setTotal(num5); } }
	 * 
	 * 
	 * //map list customer qua list staff Map<String, Rpt3_13_CTKMTCT_Staff>
	 * mapStaff= new HashMap<String, Rpt3_13_CTKMTCT_Staff>(); List<String>
	 * codeStaff = new ArrayList<String>(); if (listCustomer != null &&
	 * listCustomer.size() > 0) { for (Rpt3_13_CTKMTCT_Customer child :
	 * listCustomer) { if (mapStaff.get(child.getStaff() +
	 * DateUtility.toDateSimpleFormatString(child.getOrderDate()) +
	 * child.getProgramName()) != null) { Rpt3_13_CTKMTCT_Staff rpt =
	 * mapStaff.get(child.getStaff() +
	 * DateUtility.toDateSimpleFormatString(child.getOrderDate()) +
	 * child.getProgramName()); rpt.getListCustomer().add(child); } else {
	 * Rpt3_13_CTKMTCT_Staff rpt = new Rpt3_13_CTKMTCT_Staff();
	 * rpt.setListCustomer(new ArrayList<Rpt3_13_CTKMTCT_Customer>());
	 * rpt.getListCustomer().add(child); mapStaff.put(child.getStaff() +
	 * DateUtility.toDateSimpleFormatString(child.getOrderDate()) +
	 * child.getProgramName(), rpt); codeStaff.add(child.getStaff() +
	 * DateUtility.toDateSimpleFormatString(child.getOrderDate()) +
	 * child.getProgramName()); } } for (String code : codeStaff) {
	 * listStaff.add(mapStaff.get(code)); } }
	 * 
	 * for (Rpt3_13_CTKMTCT_Staff res : listStaff) {
	 * List<Rpt3_13_CTKMTCT_Customer> lst = res.getListCustomer(); if (lst !=
	 * null && lst.size() > 0) { res.setStaff(lst.get(0).getStaff());
	 * res.setOrderDate(lst.get(0).getOrderDate());
	 * res.setProgramName(lst.get(0).getProgramName());
	 * 
	 * BigDecimal num1, num2, num3, num4, num5; num1 = num2 = num3 = num4 = num5
	 * = BigDecimal.ZERO; for (Rpt3_13_CTKMTCT_Customer child : lst) { if
	 * (child.getQuantityPromotion() != null) { num1 =
	 * num1.add(child.getQuantityPromotion()); } if (child.getAmountPromotion()
	 * != null) { num2 = num2.add(child.getAmountPromotion()); } if
	 * (child.getQuantityMPromotion() != null) { num3 =
	 * num3.add(child.getQuantityMPromotion()); } if (child.getMoneyPromotion()
	 * != null) { num4 = num4.add(child.getMoneyPromotion()); } if
	 * (child.getTotal() != null) { num5 = num5.add(child.getTotal()); } }
	 * res.setQuantityPromotion(num1); res.setAmountPromotion(num2);
	 * res.setQuantityPromotion(num3); res.setMoneyPromotion(num4);
	 * res.setTotal(num5); } }
	 * 
	 * 
	 * //map list staff qua list date Map<String, Rpt3_13_CTKMTCT_Date> mapDate
	 * = new HashMap<String, Rpt3_13_CTKMTCT_Date>(); List<String> codeDate =
	 * new ArrayList<String>(); if (listStaff != null && listStaff.size() > 0) {
	 * for (Rpt3_13_CTKMTCT_Staff child : listStaff) { if
	 * (mapDate.get(DateUtility.toDateSimpleFormatString(child.getOrderDate()) +
	 * child.getProgramName()) != null) { Rpt3_13_CTKMTCT_Date rpt =
	 * mapDate.get(DateUtility.toDateSimpleFormatString(child.getOrderDate()) +
	 * child.getProgramName()); rpt.getListStaff().add(child); } else {
	 * Rpt3_13_CTKMTCT_Date rpt = new Rpt3_13_CTKMTCT_Date();
	 * rpt.setListStaff(new ArrayList<Rpt3_13_CTKMTCT_Staff>());
	 * rpt.getListStaff().add(child);
	 * mapDate.put(DateUtility.toDateSimpleFormatString(child.getOrderDate()) +
	 * child.getProgramName(), rpt);
	 * codeDate.add(DateUtility.toDateSimpleFormatString(child.getOrderDate()) +
	 * child.getProgramName()); } } for (String code : codeDate) {
	 * listDate.add(mapDate.get(code)); } }
	 * 
	 * for (Rpt3_13_CTKMTCT_Date res : listDate) { List<Rpt3_13_CTKMTCT_Staff>
	 * lst = res.getListStaff(); if (lst != null && lst.size() > 0) {
	 * res.setOrderDate(lst.get(0).getOrderDate());
	 * res.setProgramName(lst.get(0).getProgramName());
	 * 
	 * BigDecimal num1, num2, num3, num4, num5; num1 = num2 = num3 = num4 = num5
	 * = BigDecimal.ZERO; for (Rpt3_13_CTKMTCT_Staff child : lst) { if
	 * (child.getQuantityPromotion() != null) { num1 =
	 * num1.add(child.getQuantityPromotion()); } if (child.getAmountPromotion()
	 * != null) { num2 = num2.add(child.getAmountPromotion()); } if
	 * (child.getQuantityMPromotion() != null) { num3 =
	 * num3.add(child.getQuantityMPromotion()); } if (child.getMoneyPromotion()
	 * != null) { num4 = num4.add(child.getMoneyPromotion()); } if
	 * (child.getTotal() != null) { num5 = num5.add(child.getTotal()); } }
	 * res.setQuantityPromotion(num1); res.setAmountPromotion(num2);
	 * res.setQuantityPromotion(num3); res.setMoneyPromotion(num4);
	 * res.setTotal(num5); } }
	 * 
	 * 
	 * //map list date qua list program Map<String, Rpt3_13_CTKMTCT> map = new
	 * HashMap<String, Rpt3_13_CTKMTCT>(); List<String> codePro = new
	 * ArrayList<String>(); if (listDate != null && listDate.size() > 0) { for
	 * (Rpt3_13_CTKMTCT_Date child : listDate) { if
	 * (map.get(child.getProgramName()) != null) { Rpt3_13_CTKMTCT rpt =
	 * map.get(child.getProgramName()); rpt.getListDate().add(child); } else {
	 * Rpt3_13_CTKMTCT rpt = new Rpt3_13_CTKMTCT(); rpt.setListDate(new
	 * ArrayList<Rpt3_13_CTKMTCT_Date>()); rpt.getListDate().add(child);
	 * map.put(child.getProgramName(), rpt);
	 * codePro.add(child.getProgramName()); } } for (String code : codePro) {
	 * lstRes.add(map.get(code)); } }
	 * 
	 * for (Rpt3_13_CTKMTCT res : lstRes) { List<Rpt3_13_CTKMTCT_Date> lst =
	 * res.getListDate(); if (lst != null && lst.size() > 0) {
	 * res.setProgramName(lst.get(0).getProgramName());
	 * 
	 * BigDecimal num1, num2, num3, num4, num5; num1 = num2 = num3 = num4 = num5
	 * = BigDecimal.ZERO; for (Rpt3_13_CTKMTCT_Date child : lst) { if
	 * (child.getQuantityPromotion() != null) { num1 =
	 * num1.add(child.getQuantityPromotion()); } if (child.getAmountPromotion()
	 * != null) { num2 = num2.add(child.getAmountPromotion()); } if
	 * (child.getQuantityMPromotion() != null) { num3 =
	 * num3.add(child.getQuantityMPromotion()); } if (child.getMoneyPromotion()
	 * != null) { num4 = num4.add(child.getMoneyPromotion()); } if
	 * (child.getTotal() != null) { num5 = num5.add(child.getTotal()); } }
	 * res.setQuantityPromotion(num1); res.setAmountPromotion(num2);
	 * res.setQuantityPromotion(num3); res.setMoneyPromotion(num4);
	 * res.setTotal(num5); } } return lstRes; } catch (Exception e) { throw new
	 * BusinessException(e); } }
	 */

	@Override
	public List<Rpt3_13_CTKMTCT> getBCCTKMTCT(Long shopId, String staffCode, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BC_3_13_CTKMTCT(?, :shopId, :staffCode, :fromDate, :toDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();

			params.add(2);
			params.add(shopId);
			params.add(Types.NUMERIC);

			params.add(3);
			params.add(staffCode);
			params.add(Types.VARCHAR);

			params.add(4);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(Types.DATE);

			params.add(5);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(Types.DATE);

			params.add(6);
			params.add(staffIdRoot);
			params.add(Types.NUMERIC);

			params.add(7);
			params.add(flagCMS);
			params.add(Types.NUMERIC);

			List<Rpt3_13_CTKMTCT> lstRes = new ArrayList<Rpt3_13_CTKMTCT>();
			List<Rpt3_13_CTKMTCT_Detail> lstDetail = repo.getListByQueryDynamicFromPackage(Rpt3_13_CTKMTCT_Detail.class, sql.toString(), params, null);

			List<Rpt3_13_CTKMTCT_Date> listDate = new ArrayList<Rpt3_13_CTKMTCT_Date>();
			List<Rpt3_13_CTKMTCT_Staff> listStaff = new ArrayList<Rpt3_13_CTKMTCT_Staff>();
			List<Rpt3_13_CTKMTCT_Customer> listCustomer = new ArrayList<Rpt3_13_CTKMTCT_Customer>();

			//map list detail qua list customer
			Map<String, Rpt3_13_CTKMTCT_Customer> mapCustomer = new HashMap<String, Rpt3_13_CTKMTCT_Customer>();
			List<String> customerCode = new ArrayList<String>();
			if (lstDetail != null && lstDetail.size() > 0) {
				for (Rpt3_13_CTKMTCT_Detail child : lstDetail) {
					if (mapCustomer.get(child.getCustomer() + child.getStaff() + DateUtility.toDateSimpleFormatString(child.getOrderDate()) + child.getProgramName()) != null) {
						Rpt3_13_CTKMTCT_Customer rpt = mapCustomer.get(child.getCustomer() + child.getStaff() + DateUtility.toDateSimpleFormatString(child.getOrderDate()) + child.getProgramName());
						rpt.getListDetail().add(child);
					} else {
						Rpt3_13_CTKMTCT_Customer rpt = new Rpt3_13_CTKMTCT_Customer();
						rpt.setListDetail(new ArrayList<Rpt3_13_CTKMTCT_Detail>());
						rpt.getListDetail().add(child);
						mapCustomer.put(child.getCustomer() + child.getStaff() + DateUtility.toDateSimpleFormatString(child.getOrderDate()) + child.getProgramName(), rpt);
						customerCode.add(child.getCustomer() + child.getStaff() + DateUtility.toDateSimpleFormatString(child.getOrderDate()) + child.getProgramName());
					}
				}
				for (String code : customerCode) {
					listCustomer.add(mapCustomer.get(code));
				}
			}

			for (Rpt3_13_CTKMTCT_Customer res : listCustomer) {
				List<Rpt3_13_CTKMTCT_Detail> lst = res.getListDetail();
				if (lst != null && lst.size() > 0) {
					res.setCustomer(lst.get(0).getCustomer());
					res.setStaff(lst.get(0).getStaff());
					res.setOrderDate(lst.get(0).getOrderDate());
					res.setProgramName(lst.get(0).getProgramName());

					BigDecimal num1, num2, num3, num4, num5;
					num1 = num2 = num3 = num4 = num5 = BigDecimal.ZERO;
					for (Rpt3_13_CTKMTCT_Detail child : lst) {
						if (child.getQuantityPromotion() != null) {
							num1 = num1.add(child.getQuantityPromotion());
						}
						if (child.getAmountPromotion() != null) {
							num2 = num2.add(child.getAmountPromotion());
						}
						if (child.getQuantityMPromotion() != null) {
							num3 = num3.add(child.getQuantityMPromotion());
						}
						if (child.getMoneyPromotion() != null) {
							num4 = num4.add(child.getMoneyPromotion());
						}
						if (child.getTotal() != null) {
							num5 = num5.add(child.getTotal());
						}
					}
					res.setQuantityPromotion(num1);
					res.setAmountPromotion(num2);
					res.setQuantityMPromotion(num3);
					res.setMoneyPromotion(num4);
					res.setTotal(num5);
				}
			}

			//map list customer qua list staff
			Map<String, Rpt3_13_CTKMTCT_Staff> mapStaff = new HashMap<String, Rpt3_13_CTKMTCT_Staff>();
			List<String> codeStaff = new ArrayList<String>();
			if (listCustomer != null && listCustomer.size() > 0) {
				for (Rpt3_13_CTKMTCT_Customer child : listCustomer) {
					if (mapStaff.get(child.getStaff() + DateUtility.toDateSimpleFormatString(child.getOrderDate()) + child.getProgramName()) != null) {
						Rpt3_13_CTKMTCT_Staff rpt = mapStaff.get(child.getStaff() + DateUtility.toDateSimpleFormatString(child.getOrderDate()) + child.getProgramName());
						rpt.getListCustomer().add(child);
					} else {
						Rpt3_13_CTKMTCT_Staff rpt = new Rpt3_13_CTKMTCT_Staff();
						rpt.setListCustomer(new ArrayList<Rpt3_13_CTKMTCT_Customer>());
						rpt.getListCustomer().add(child);
						mapStaff.put(child.getStaff() + DateUtility.toDateSimpleFormatString(child.getOrderDate()) + child.getProgramName(), rpt);
						codeStaff.add(child.getStaff() + DateUtility.toDateSimpleFormatString(child.getOrderDate()) + child.getProgramName());
					}
				}
				for (String code : codeStaff) {
					listStaff.add(mapStaff.get(code));
				}
			}

			for (Rpt3_13_CTKMTCT_Staff res : listStaff) {
				List<Rpt3_13_CTKMTCT_Customer> lst = res.getListCustomer();
				if (lst != null && lst.size() > 0) {
					res.setStaff(lst.get(0).getStaff());
					res.setOrderDate(lst.get(0).getOrderDate());
					res.setProgramName(lst.get(0).getProgramName());

					BigDecimal num1, num2, num3, num4, num5;
					num1 = num2 = num3 = num4 = num5 = BigDecimal.ZERO;
					for (Rpt3_13_CTKMTCT_Customer child : lst) {
						if (child.getQuantityPromotion() != null) {
							num1 = num1.add(child.getQuantityPromotion());
						}
						if (child.getAmountPromotion() != null) {
							num2 = num2.add(child.getAmountPromotion());
						}
						if (child.getQuantityMPromotion() != null) {
							num3 = num3.add(child.getQuantityMPromotion());
						}
						if (child.getMoneyPromotion() != null) {
							num4 = num4.add(child.getMoneyPromotion());
						}
						if (child.getTotal() != null) {
							num5 = num5.add(child.getTotal());
						}
					}
					res.setQuantityPromotion(num1);
					res.setAmountPromotion(num2);
					res.setQuantityMPromotion(num3);
					res.setMoneyPromotion(num4);
					res.setTotal(num5);
				}
			}

			//map list staff qua list date
			Map<String, Rpt3_13_CTKMTCT_Date> mapDate = new HashMap<String, Rpt3_13_CTKMTCT_Date>();
			List<String> codeDate = new ArrayList<String>();
			if (listStaff != null && listStaff.size() > 0) {
				for (Rpt3_13_CTKMTCT_Staff child : listStaff) {
					if (mapDate.get(DateUtility.toDateSimpleFormatString(child.getOrderDate()) + child.getProgramName()) != null) {
						Rpt3_13_CTKMTCT_Date rpt = mapDate.get(DateUtility.toDateSimpleFormatString(child.getOrderDate()) + child.getProgramName());
						rpt.getListStaff().add(child);
					} else {
						Rpt3_13_CTKMTCT_Date rpt = new Rpt3_13_CTKMTCT_Date();
						rpt.setListStaff(new ArrayList<Rpt3_13_CTKMTCT_Staff>());
						rpt.getListStaff().add(child);
						mapDate.put(DateUtility.toDateSimpleFormatString(child.getOrderDate()) + child.getProgramName(), rpt);
						codeDate.add(DateUtility.toDateSimpleFormatString(child.getOrderDate()) + child.getProgramName());
					}
				}
				for (String code : codeDate) {
					listDate.add(mapDate.get(code));
				}
			}

			for (Rpt3_13_CTKMTCT_Date res : listDate) {
				List<Rpt3_13_CTKMTCT_Staff> lst = res.getListStaff();
				if (lst != null && lst.size() > 0) {
					res.setOrderDate(lst.get(0).getOrderDate());
					res.setProgramName(lst.get(0).getProgramName());

					BigDecimal num1, num2, num3, num4, num5;
					num1 = num2 = num3 = num4 = num5 = BigDecimal.ZERO;
					for (Rpt3_13_CTKMTCT_Staff child : lst) {
						if (child.getQuantityPromotion() != null) {
							num1 = num1.add(child.getQuantityPromotion());
						}
						if (child.getAmountPromotion() != null) {
							num2 = num2.add(child.getAmountPromotion());
						}
						if (child.getQuantityMPromotion() != null) {
							num3 = num3.add(child.getQuantityMPromotion());
						}
						if (child.getMoneyPromotion() != null) {
							num4 = num4.add(child.getMoneyPromotion());
						}
						if (child.getTotal() != null) {
							num5 = num5.add(child.getTotal());
						}
					}
					res.setQuantityPromotion(num1);
					res.setAmountPromotion(num2);
					res.setQuantityMPromotion(num3);
					res.setMoneyPromotion(num4);
					res.setTotal(num5);
				}
			}

			//map list date qua list program
			Map<String, Rpt3_13_CTKMTCT> map = new HashMap<String, Rpt3_13_CTKMTCT>();
			List<String> codePro = new ArrayList<String>();
			if (listDate != null && listDate.size() > 0) {
				for (Rpt3_13_CTKMTCT_Date child : listDate) {
					if (map.get(child.getProgramName()) != null) {
						Rpt3_13_CTKMTCT rpt = map.get(child.getProgramName());
						rpt.getListDate().add(child);
					} else {
						Rpt3_13_CTKMTCT rpt = new Rpt3_13_CTKMTCT();
						rpt.setListDate(new ArrayList<Rpt3_13_CTKMTCT_Date>());
						rpt.getListDate().add(child);
						map.put(child.getProgramName(), rpt);
						codePro.add(child.getProgramName());
					}
				}
				for (String code : codePro) {
					lstRes.add(map.get(code));
				}
			}

			for (Rpt3_13_CTKMTCT res : lstRes) {
				List<Rpt3_13_CTKMTCT_Date> lst = res.getListDate();
				if (lst != null && lst.size() > 0) {
					res.setProgramName(lst.get(0).getProgramName());

					BigDecimal num1, num2, num3, num4, num5;
					num1 = num2 = num3 = num4 = num5 = BigDecimal.ZERO;
					for (Rpt3_13_CTKMTCT_Date child : lst) {
						if (child.getQuantityPromotion() != null) {
							num1 = num1.add(child.getQuantityPromotion());
						}
						if (child.getAmountPromotion() != null) {
							num2 = num2.add(child.getAmountPromotion());
						}
						if (child.getQuantityMPromotion() != null) {
							num3 = num3.add(child.getQuantityMPromotion());
						}
						if (child.getMoneyPromotion() != null) {
							num4 = num4.add(child.getMoneyPromotion());
						}
						if (child.getTotal() != null) {
							num5 = num5.add(child.getTotal());
						}
					}
					res.setQuantityPromotion(num1);
					res.setAmountPromotion(num2);
					res.setQuantityMPromotion(num3);
					res.setMoneyPromotion(num4);
					res.setTotal(num5);
				}
			}
			return lstRes;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Rpt3_14_CTKMTCTNV> getBCCTKMTCTNV(Long shopId, String staffCode, Date fDate, Date tDate) throws BusinessException {
		try {
			List<Rpt3_14_CTKMTCTNV> lstRes = new ArrayList<Rpt3_14_CTKMTCTNV>();

			List<Object> inParams = new ArrayList<Object>();
			inParams.add("shopId");
			inParams.add(shopId == null ? 0 : shopId);
			inParams.add(StandardBasicTypes.LONG);

			inParams.add("staffCode");
			inParams.add(staffCode);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(tDate);
			inParams.add(StandardBasicTypes.DATE);

			List<Rpt3_14_CTKMTCTNV_Detail> lstDetail = new ArrayList<Rpt3_14_CTKMTCTNV_Detail>();
			lstDetail = repo.getListByNamedQuery(Rpt3_14_CTKMTCTNV_Detail.class, "PKG_SHOP_REPORT.BC_3_14_CTKMTCTNV", inParams);

			//map list detail qua list program
			Map<String, Rpt3_14_CTKMTCTNV> mapCustomer = new HashMap<String, Rpt3_14_CTKMTCTNV>();
			List<String> customerCode = new ArrayList<String>();
			if (lstDetail != null && lstDetail.size() > 0) {
				for (Rpt3_14_CTKMTCTNV_Detail child : lstDetail) {
					if (mapCustomer.get(child.getProgramName()) != null) {
						Rpt3_14_CTKMTCTNV rpt = mapCustomer.get(child.getProgramName());
						rpt.getListDetail().add(child);
					} else {
						Rpt3_14_CTKMTCTNV rpt = new Rpt3_14_CTKMTCTNV();
						rpt.setListDetail(new ArrayList<Rpt3_14_CTKMTCTNV_Detail>());
						rpt.getListDetail().add(child);
						mapCustomer.put(child.getProgramName(), rpt);
						customerCode.add(child.getProgramName());
					}
				}
				for (String code : customerCode) {
					lstRes.add(mapCustomer.get(code));
				}
			}

			for (Rpt3_14_CTKMTCTNV res : lstRes) {
				List<Rpt3_14_CTKMTCTNV_Detail> lst = res.getListDetail();
				if (lst != null && lst.size() > 0) {
					res.setProgramName(lst.get(0).getProgramName());

					BigDecimal num1, num2, num3, num4;
					num1 = num2 = num3 = num4 = BigDecimal.ZERO;
					for (Rpt3_14_CTKMTCTNV_Detail child : lst) {
						if (child.getQuantityPromotion() != null) {
							num1 = num1.add(child.getQuantityPromotion());
						}
						if (child.getAmountPromotion() != null) {
							num2 = num2.add(child.getAmountPromotion());
						}
						if (child.getMoneyPromotion() != null) {
							num3 = num3.add(child.getMoneyPromotion());
						}
						if (child.getTotal() != null) {
							num4 = num4.add(child.getTotal());
						}
					}
					res.setQuantityPromotion(num1);
					res.setAmountPromotion(num2);
					res.setMoneyPromotion(num3);
					res.setTotal(num4);
				}
			}

			return lstRes;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<RptBCDKTT_9_1VO> getRptBCDKTT(Long shopId, String maKiemKe) throws BusinessException {
		try {
			List<Object> inParams = new ArrayList<Object>();

			inParams.add(2);
			inParams.add(shopId);
			inParams.add(Types.NUMERIC);

			inParams.add(3);
			inParams.add(maKiemKe);
			inParams.add(Types.VARCHAR);

			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.bcdktt_9_1(?,?,?)");
			List<RptBCDKTT_9_1VO> res = repo.getListByQueryDynamicFromPackage(RptBCDKTT_9_1VO.class, sql.toString(), inParams, null);

			return res;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Rpt3_11_CTCTHTB> getBCCTCTHTB(Long shopId, String lstDisProCode, Date fDate, Date tDate) throws BusinessException {
		try {
			List<Rpt3_11_CTCTHTB> lstResult = new ArrayList<Rpt3_11_CTCTHTB>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.Rpt3_11_CTCTHTB(?,?,?,?,?)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);

			params.add(3);
			params.add(lstDisProCode);
			params.add(java.sql.Types.VARCHAR);

			params.add(4);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(5);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			List<Rpt3_11_CTCTHTB_Detail> listDetail = new ArrayList<Rpt3_11_CTCTHTB_Detail>();
			listDetail = repo.getListByQueryDynamicFromPackage(Rpt3_11_CTCTHTB_Detail.class, sql.toString(), params, null);

			Map<String, Rpt3_11_CTCTHTB> map = new HashMap<String, Rpt3_11_CTCTHTB>();
			List<String> programCode = new ArrayList<String>();

			if (listDetail != null && listDetail.size() > 0) {
				for (Rpt3_11_CTCTHTB_Detail child : listDetail) {
					if (map.get(child.getProgramCode()) != null) {
						Rpt3_11_CTCTHTB rpt = map.get(child.getProgramCode());
						rpt.getListDetail().add(child);
					} else {
						Rpt3_11_CTCTHTB rpt = new Rpt3_11_CTCTHTB();
						rpt.setListDetail(new ArrayList<Rpt3_11_CTCTHTB_Detail>());
						rpt.getListDetail().add(child);
						map.put(child.getProgramCode(), rpt);
						programCode.add(child.getProgramCode());
					}
				}
				for (String code : programCode) {
					lstResult.add(map.get(code));
				}
			}

			for (Rpt3_11_CTCTHTB res : lstResult) {
				List<Rpt3_11_CTCTHTB_Detail> lst = res.getListDetail();
				if (lst != null && lst.size() > 0) {
					res.setProgramCode(lst.get(0).getProgramCode());
					res.setProgramName(lst.get(0).getProgramName());
					res.setFromDate(lst.get(0).getFromDate());
					res.setToDate(lst.get(0).getToDate());

					BigDecimal num1, num2;
					num1 = num2 = BigDecimal.ZERO;

					for (Rpt3_11_CTCTHTB_Detail child : lst) {
						if (child.getQuantity() != null) {
							num1 = num1.add(child.getQuantity());
						}

						if (child.getAmount() != null) {
							num2 = num2.add(child.getAmount());
						}
					}
					res.setSumQuantity(num1);
					res.setSumAmount(num2);
				}
			}

			return lstResult;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @return list RptDSTTCTDLVO
	 * 
	 * @author
	 * @since March 08, 2014
	 * @description report Bao cao bo sung NPP - Bao cao doanh so tieu thu cua
	 *              tung diem le
	 * */
	@Override
	public List<RptDSTTCTDLVO> Rpt7_2_8_DSTTCTDL(Long shopId, String lstStaff, Date fDate, Date tDate, Long staffRootId, Integer hasCheckStaff) throws BusinessException {
		try {
			//7.2.8 - Bao cao doanh so tieu thu cua tung diem le
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BC_7_2_8_DSTTCTDL(?, :shopId, :lstStaff, :fDate, :tDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			if (!StringUtility.isNullOrEmpty(lstStaff)) {
				params.add(lstStaff);
			} else {
				params.add(null);
			}
			params.add(java.sql.Types.VARCHAR);
			params.add(4);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(5);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(6);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(7);
			params.add(hasCheckStaff);
			params.add(java.sql.Types.NUMERIC);

			return repo.getListByQueryDynamicFromPackage(RptDSTTCTDLVO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @return list RptDDHF2VO
	 * @author tungmt
	 * @since March 08, 2014
	 * @description report Bao cao bo sung NPP - Bao cao don dat hang f2
	 * */
	@Override
	public List<RptDDHF2VO> rpt7_3_9_DDHF2(Long shopId, Date fDate, Date tDate) throws BusinessException {
		try {
			//7.3.9 - Bao cao don dat hang f2
			StringBuilder sql = new StringBuilder();
			//sql.append("call PKG_SHOP_REPORT.PROC_7_3_9_F2(?, :shopId, :fDate, :tDate)");
			sql.append("call PKG_SHOP_REPORT.PROC_7_3_9_F2(?, :shopId, :fDate, :tDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(4);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			return repo.getListByQueryDynamicFromPackage(RptDDHF2VO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @return list RptCTKMTCK739VO
	 * @author tungmt
	 * @since March 10, 2014
	 * @description report Bao cao bo sung NPP - Bao cao chi tiet km theo chu ky
	 * */
	@Override
	public List<RptCTKMTCK739VO> rpt7_2_7_CTKMTCK(Long shopId, Date fDate, Date tDate, Long staffRootId, Integer hasCheckStaff) throws BusinessException {
		try {
			//7.3.9 - Bao cao don dat hang f2
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.PROC_7_2_7_KM(?, :shopId, :fDate, :tDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(4);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(5);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(6);
			params.add(hasCheckStaff);
			params.add(java.sql.Types.NUMERIC);

			return repo.getListByQueryDynamicFromPackage(RptCTKMTCK739VO.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<Rpt10_1_18_DHHH> export10_1_18_DHHH(Long shopId, String programCodes, String categoryCodes, Date fDate, Date tDate) throws BusinessException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.RPT_10_1_18(?, :shopId, :progCodes, :catCodes, :fDate, :tDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);

			params.add(3);
			if (StringUtility.isNullOrEmpty(programCodes)) {
				params.add(null);
			} else {
				params.add(programCodes);
			}
			params.add(java.sql.Types.VARCHAR);

			params.add(4);
			if (StringUtility.isNullOrEmpty(categoryCodes)) {
				params.add(null);
			} else {
				params.add(categoryCodes);
			}
			params.add(java.sql.Types.VARCHAR);

			params.add(5);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(6);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			return repo.getListByQueryDynamicFromPackage(Rpt10_1_18_DHHH.class, sql.toString(), params, null);
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<Rpt7_5_1TH> getNoPhaiThuTH(Long shopId) throws BusinessException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.P_NoPhaiThu_TongHop(?, :shopId)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);

			return repo.getListByQueryDynamicFromPackage(Rpt7_5_1TH.class, sql.toString(), params, null);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<Rpt7_5_1CT> getNoPhaiThuCT(Long shopId) throws BusinessException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.P_NoPhaiThu_ChiTiet(?, :shopId)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);

			return repo.getListByQueryDynamicFromPackage(Rpt7_5_1CT.class, sql.toString(), params, null);
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<Rpt7_2_5_CTTT> getBCCTTT_KH(Long shopId, String lstNVBHCode, String lstNVGHCode, String lstCustomerCode, Date fDate, Date tDate) throws BusinessException {
		try {
			List<Rpt7_2_5_CTTT> lstRes = new ArrayList<Rpt7_2_5_CTTT>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.Rpt7_2_5_CTTT_KH(?, :shopId, :lstNVBHCode, :lstNVGHCode, :lstCustomerCode, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);

			params.add(3);
			params.add(lstNVBHCode);
			params.add(java.sql.Types.VARCHAR);

			params.add(4);
			params.add(lstNVGHCode);
			params.add(java.sql.Types.VARCHAR);

			params.add(5);
			params.add(lstCustomerCode);
			params.add(java.sql.Types.VARCHAR);

			params.add(6);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(7);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(java.sql.Types.DATE);

			List<Rpt7_2_5_CTTT_Detail> lstDetail = new ArrayList<Rpt7_2_5_CTTT_Detail>();
			lstDetail = repo.getListByQueryDynamicFromPackage(Rpt7_2_5_CTTT_Detail.class, sql.toString(), params, null);

			//map list detail qua list program
			Map<String, Rpt7_2_5_CTTT> mapCustomer = new HashMap<String, Rpt7_2_5_CTTT>();
			List<String> customerCode = new ArrayList<String>();

			if (lstDetail != null && lstDetail.size() > 0) {
				for (Rpt7_2_5_CTTT_Detail child : lstDetail) {
					if (mapCustomer.get(child.getCode()) != null) {
						Rpt7_2_5_CTTT rpt = mapCustomer.get(child.getCode());
						rpt.getListDetail().add(child);
					} else {
						Rpt7_2_5_CTTT rpt = new Rpt7_2_5_CTTT();
						rpt.setListDetail(new ArrayList<Rpt7_2_5_CTTT_Detail>());
						rpt.getListDetail().add(child);
						mapCustomer.put(child.getCode(), rpt);
						customerCode.add(child.getCode());
					}
				}
				for (String code : customerCode) {
					lstRes.add(mapCustomer.get(code));
				}
			}

			for (Rpt7_2_5_CTTT res : lstRes) {
				List<Rpt7_2_5_CTTT_Detail> lst = res.getListDetail();
				if (lst != null && lst.size() > 0) {
					res.setCode(lst.get(0).getCode());

					BigDecimal num1, num2, num3;
					num1 = num2 = num3 = BigDecimal.ZERO;
					for (Rpt7_2_5_CTTT_Detail child : lst) {
						if (child.getAmount() != null) {
							num1 = num1.add(child.getAmount());
						}
						if (child.getPayAmount() != null) {
							num2 = num2.add(child.getPayAmount());
						}
						if (child.getDebitAmount() != null) {
							num3 = num3.add(child.getDebitAmount());
						}
					}
					res.setSumAmount(num1);
					res.setSumPayAmount(num2);
					res.setSumDebitAmount(num3);
				}
			}

			return lstRes;
		} catch (Exception e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Rpt7_2_5_PT_Detail> getRpt7_2_5PT(Long shopId, String lstSaler, String lstCustomer, String lstDelivery, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.RPT7_2_5_PT(?, :shopId, :lstSaler, :lstCustomer, :lstDelivery, :fDate, :tDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();

			params.add(2);
			params.add(shopId);
			params.add(Types.NUMERIC);

			params.add(3);
			params.add(lstSaler);
			params.add(Types.VARCHAR);

			params.add(4);
			params.add(lstCustomer);
			params.add(Types.VARCHAR);

			params.add(5);
			params.add(lstDelivery);
			params.add(Types.VARCHAR);

			params.add(6);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(Types.DATE);

			params.add(7);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(Types.DATE);

			params.add(8);
			params.add(staffIdRoot);
			params.add(Types.NUMERIC);

			params.add(9);
			params.add(flagCMS);
			params.add(Types.NUMERIC);

			return repo.getListByQueryDynamicFromPackage(Rpt7_2_5_PT_Detail.class, sql.toString(), params, null);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptTDTTCN_NVTT> getRpt7_2_5NVTT(Long shopId, String lstNVBH, String lstKH, String lstNVGH, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.P_CT_ThanhToan_NVTT(?, :p_shopId, :p_lstStaffId, :p_lstCustId, :p_lstDeliveryId, :p_fDate, :p_tDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();

			params.add(2);
			params.add(shopId);
			params.add(Types.NUMERIC);

			params.add(3);
			params.add(lstNVBH);
			params.add(Types.VARCHAR);

			params.add(4);
			params.add(lstKH);
			params.add(Types.VARCHAR);

			params.add(5);
			params.add(lstNVGH);
			params.add(Types.VARCHAR);

			params.add(6);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(Types.DATE);

			params.add(7);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(Types.DATE);

			params.add(8);
			params.add(staffIdRoot);
			params.add(Types.NUMERIC);

			params.add(9);
			params.add(flagCMS);
			params.add(Types.NUMERIC);
			return repo.getListByQueryDynamicFromPackage(RptTDTTCN_NVTT.class, sql.toString(), params, null);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptTDTTCN_NVTT> getRpt7_2_5KH(Long shopId, String lstNVBH, String lstKH, String lstNVGH, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.P_CT_ThanhToan_KH(?, :p_shopId, :p_lstStaffId, :p_lstCustId, :p_lstDeliveryId, :p_fDate, :p_tDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();

			params.add(2);
			params.add(shopId);
			params.add(Types.NUMERIC);

			params.add(3);
			params.add(lstNVBH);
			params.add(Types.VARCHAR);

			params.add(4);
			params.add(lstKH);
			params.add(Types.VARCHAR);

			params.add(5);
			params.add(lstNVGH);
			params.add(Types.VARCHAR);

			params.add(6);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(Types.DATE);

			params.add(7);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(Types.DATE);

			params.add(8);
			params.add(staffIdRoot);
			params.add(Types.NUMERIC);

			params.add(9);
			params.add(flagCMS);
			params.add(Types.NUMERIC);
			return repo.getListByQueryDynamicFromPackage(RptTDTTCN_NVTT.class, sql.toString(), params, null);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<RptTDTTCNData> getRpt7_2_5TDTTCN(Long shopId, String lstNVBH, String lstKH, String lstNVGH, Date fDate, Date tDate, Long staffIdRoot, Integer flagCMS) throws BusinessException {
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.P_CT_THANHTOAN_CONGNO(?, :p_shopId, :p_lstStaffId, :p_lstCustId, :p_lstDeliveryId, :p_fDate, :p_tDate, :staffIdRoot, :flagCMS)");
			List<Object> params = new ArrayList<Object>();

			params.add(2);
			params.add(shopId);
			params.add(Types.NUMERIC);

			params.add(3);
			params.add(lstNVBH);
			params.add(Types.VARCHAR);

			params.add(4);
			params.add(lstKH);
			params.add(Types.VARCHAR);

			params.add(5);
			params.add(lstNVGH);
			params.add(Types.VARCHAR);

			params.add(6);
			params.add(new java.sql.Date(fDate.getTime()));
			params.add(Types.DATE);

			params.add(7);
			params.add(new java.sql.Date(tDate.getTime()));
			params.add(Types.DATE);

			params.add(8);
			params.add(staffIdRoot);
			params.add(Types.NUMERIC);

			params.add(9);
			params.add(flagCMS);
			params.add(Types.NUMERIC);
			return repo.getListByQueryDynamicFromPackage(RptTDTTCNData.class, sql.toString(), params, null);
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<Rpt10_1_2> getListRpt10_1_2(Long shopId, Long parentStaffId, String lstSaleStaffId, Date fromDate, Date toDate, boolean checkMap) throws BusinessException {
		try {
			if (shopId == null || shopId <= 0) {
				throw new IllegalArgumentException("shopId is null or <= 0");
			}
			StringBuilder sql = new StringBuilder();
			sql.append("call pkg_shop_report.PROC_10_1_2(?, :shopId, :parentStaffId, :lstNVBHId, :fDate, :tDate, :checkMap)");
			List<Object> params = new ArrayList<Object>();

			params.add(2);
			params.add(shopId);
			params.add(Types.NUMERIC);

			params.add(3);
			params.add(parentStaffId);
			params.add(Types.NUMERIC);

			params.add(4);
			params.add(StringUtility.isNullOrEmpty(lstSaleStaffId) ? "" : lstSaleStaffId);
			params.add(Types.VARCHAR);

			params.add(5);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(Types.DATE);

			params.add(6);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(Types.DATE);

			params.add(7);
			if (checkMap) {
				params.add(1);
			} else {
				params.add(0);
			}
			params.add(Types.NUMERIC);

			List<Rpt10_1_2> lst = repo.getListByQueryDynamicFromPackage(Rpt10_1_2.class, sql.toString(), params, null);

			if (!lst.isEmpty()) {
				sql = new StringBuilder();
				sql.append("call pkg_shop_report.PROC_10_1_2_Detail(?, :nvbhId, :fDate, :tDate,:thucDatLuyKe)");
				List<Rpt10_1_2Detail> detail = null;
				BigDecimal thucDatLuyKe = null;
				Long nvbhID = null;
				for (int i = 0, sz = lst.size(); i < sz; i++) {
					nvbhID = Long.valueOf(lst.get(i).getNvbhId());
					thucDatLuyKe = lst.get(i).getDoanhSoThucDatLK();
					params = new ArrayList<Object>();

					params.add(2);
					params.add(nvbhID);
					params.add(Types.NUMERIC);

					params.add(3);
					params.add(new java.sql.Date(fromDate.getTime()));
					params.add(Types.DATE);

					params.add(4);
					params.add(new java.sql.Date(toDate.getTime()));
					params.add(Types.DATE);

					params.add(5);
					params.add(thucDatLuyKe);
					params.add(Types.NUMERIC);

					detail = repo.getListByQueryDynamicFromPackage(Rpt10_1_2Detail.class, sql.toString(), params, null);
					lst.get(i).setLstDetail(detail);
				}
			}
			return lst;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Rpt7_2_12_PTDTBHNPPVO> getListReport7_2_12(String strListShopId, Date fromDate, Date toDate) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BC_7_2_12_PTDTBHNPP(?, :strListShopId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();

			params.add(2);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);

			params.add(3);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(4);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			List<Rpt7_2_12_DetailVO> lstDetail = new ArrayList<Rpt7_2_12_DetailVO>();
			List<Object> inParams = new ArrayList<Object>();

			inParams.add("strListShopId");
			inParams.add(strListShopId);
			inParams.add(StandardBasicTypes.STRING);

			inParams.add("fromDate");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);

			inParams.add("toDate");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);

			lstDetail = repo.getListByQueryDynamicFromPackage(Rpt7_2_12_DetailVO.class, sql.toString(), params, null);
			List<Rpt7_2_12_PTDTBHNPPVO> lstResult = new ArrayList<Rpt7_2_12_PTDTBHNPPVO>();
			List<Rpt7_2_12_VungVO> lstVung = new ArrayList<Rpt7_2_12_VungVO>();
			List<Rpt7_2_12_NPPVO> lstNPP = new ArrayList<Rpt7_2_12_NPPVO>();

			/** Chuyen tu list detail sang list result */

			//Map list detail qua list NPP
			Map<String, Rpt7_2_12_NPPVO> mapNPP = new HashMap<String, Rpt7_2_12_NPPVO>();
			List<String> nppCode = new ArrayList<String>();
			if (lstDetail != null && lstDetail.size() > 0) {
				for (Rpt7_2_12_DetailVO child : lstDetail) {
					if (mapNPP.get(child.getMaNPP()) != null) {
						Rpt7_2_12_NPPVO rpt = mapNPP.get(child.getMaNPP());
						rpt.getListDetail().add(child);
					} else {
						Rpt7_2_12_NPPVO rpt = new Rpt7_2_12_NPPVO();
						rpt.setListDetail(new ArrayList<Rpt7_2_12_DetailVO>());
						rpt.getListDetail().add(child);
						mapNPP.put(child.getMaNPP(), rpt);
						nppCode.add(child.getMaNPP());
					}
				}
				for (String code : nppCode) {
					lstNPP.add(mapNPP.get(code));
				}
			}

			for (Rpt7_2_12_NPPVO res : lstNPP) {
				List<Rpt7_2_12_DetailVO> lst = res.getListDetail();
				if (lst != null && lst.size() > 0) {
					res.setMaNPP(lst.get(0).getMaNPP());
					res.setTenNPP(lst.get(0).getTenNPP());
					res.setVung(lst.get(0).getVung());
					res.setMien(lst.get(0).getMien());
					res.setTenMien(lst.get(0).getTenMien());

					BigDecimal num1, num2, num3;
					num1 = num2 = num3 = BigDecimal.ZERO;
					for (Rpt7_2_12_DetailVO child : lst) {
						if (child.getDsKeHoach() != null) {
							num1 = num1.add(child.getDsKeHoach());
						}
						if (child.getDsMTB() != null) {
							num2 = num2.add(child.getDsMTB());
						}
						if (child.getDsThucTe() != null) {
							num3 = num3.add(child.getDsThucTe());
						}
					}
					res.setDsKeHoach(num1);
					res.setDsMTB(num2);
					res.setDsThucTe(num3);
				}
			}

			//Map list NPP qua list Vung
			Map<String, Rpt7_2_12_VungVO> mapVung = new HashMap<String, Rpt7_2_12_VungVO>();
			List<String> vungCode = new ArrayList<String>();
			if (lstDetail != null && lstDetail.size() > 0) {
				for (Rpt7_2_12_NPPVO child : lstNPP) {
					if (mapVung.get(child.getVung()) != null) {
						Rpt7_2_12_VungVO rpt = mapVung.get(child.getVung());
						rpt.getListNPP().add(child);
					} else {
						Rpt7_2_12_VungVO rpt = new Rpt7_2_12_VungVO();
						rpt.setListNPP(new ArrayList<Rpt7_2_12_NPPVO>());
						rpt.getListNPP().add(child);
						mapVung.put(child.getVung(), rpt);
						vungCode.add(child.getVung());
					}
				}
				for (String code : vungCode) {
					lstVung.add(mapVung.get(code));
				}
			}

			for (Rpt7_2_12_VungVO res : lstVung) {
				List<Rpt7_2_12_NPPVO> lst = res.getListNPP();
				if (lst != null && lst.size() > 0) {
					res.setVung(lst.get(0).getVung());
					res.setMien(lst.get(0).getMien());
					res.setTenMien(lst.get(0).getTenMien());

					BigDecimal num1, num2, num3;
					num1 = num2 = num3 = BigDecimal.ZERO;
					for (Rpt7_2_12_NPPVO child : lst) {
						if (child.getDsKeHoach() != null) {
							num1 = num1.add(child.getDsKeHoach());
						}
						if (child.getDsMTB() != null) {
							num2 = num2.add(child.getDsMTB());
						}
						if (child.getDsThucTe() != null) {
							num3 = num3.add(child.getDsThucTe());
						}
					}
					res.setDsKeHoach(num1);
					res.setDsMTB(num2);
					res.setDsThucTe(num3);
				}
			}

			//Map list Vung qua list Mien
			Map<String, Rpt7_2_12_PTDTBHNPPVO> mapMien = new HashMap<String, Rpt7_2_12_PTDTBHNPPVO>();
			List<String> mienCode = new ArrayList<String>();
			if (lstDetail != null && lstDetail.size() > 0) {
				for (Rpt7_2_12_VungVO child : lstVung) {
					if (mapMien.get(child.getMien()) != null) {
						Rpt7_2_12_PTDTBHNPPVO rpt = mapMien.get(child.getMien());
						rpt.getListVung().add(child);
					} else {
						Rpt7_2_12_PTDTBHNPPVO rpt = new Rpt7_2_12_PTDTBHNPPVO();
						rpt.setListVung(new ArrayList<Rpt7_2_12_VungVO>());
						rpt.getListVung().add(child);
						mapMien.put(child.getMien(), rpt);
						mienCode.add(child.getMien());
					}
				}
				for (String code : mienCode) {
					lstResult.add(mapMien.get(code));
				}
			}

			for (Rpt7_2_12_PTDTBHNPPVO res : lstResult) {
				List<Rpt7_2_12_VungVO> lst = res.getListVung();
				if (lst != null && lst.size() > 0) {
					res.setMien(lst.get(0).getMien());
					res.setTenMien(lst.get(0).getTenMien());

					BigDecimal num1, num2, num3;
					num1 = num2 = num3 = BigDecimal.ZERO;
					for (Rpt7_2_12_VungVO child : lst) {
						if (child.getDsKeHoach() != null) {
							num1 = num1.add(child.getDsKeHoach());
						}
						if (child.getDsMTB() != null) {
							num2 = num2.add(child.getDsMTB());
						}
						if (child.getDsThucTe() != null) {
							num3 = num3.add(child.getDsThucTe());
						}
					}
					res.setDsKeHoach(num1);
					res.setDsMTB(num2);
					res.setDsThucTe(num3);
				}
			}

			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author sangtn
	 * @since 13/05/2014
	 * @description Xuat bao cao 7.2.13. Phan tich doanh thu ban hang theo NVBH
	 * */
	@Override
	public List<Rpt7_2_13_DetailVO> getListReport7_2_13(String strListShopId, Long parentStaffId, Date fromDate, Date toDate, boolean checkMap) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {

			List<Rpt7_2_13_DetailVO> result = new ArrayList<Rpt7_2_13_DetailVO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BC_7_2_13(?, :strListShopId, :parentStaffId, :fromDate, :toDate, :checkMap)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			params.add(3);
			params.add(parentStaffId);
			params.add(java.sql.Types.NUMERIC);
			params.add(4);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(5);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(6);
			if (checkMap) {
				params.add(1);
			} else {
				params.add(0);
			}
			params.add(java.sql.Types.NUMERIC);
			result = repo.getListByQueryDynamicFromPackage(Rpt7_2_13_DetailVO.class, sql.toString(), params, null);

			//			inParams.add("strListShopId");
			//			inParams.add(strListShopId);
			//			inParams.add(StandardBasicTypes.STRING);
			//			
			//			inParams.add("fromDate");
			//			inParams.add(fromDate);
			//			inParams.add(StandardBasicTypes.DATE);
			//			
			//			inParams.add("toDate");
			//			inParams.add(toDate);
			//			inParams.add(StandardBasicTypes.DATE);
			//			
			//			result = repo.getListByNamedQuery(Rpt7_2_13_DetailVO.class, "PKG_SHOP_REPORT.BC_7_2_13", inParams);

			return result;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<Rpt7_2_14VO> getListReport7_2_14(String strListShopId, Long parentStaffId, Date fromDate, Date toDate, boolean checkMap) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.RPT7_2_14(?, :LISTSHOPID, :parentStaffId, :FDATE, :TDATE, :checkMap)");
			List<Rpt7_2_14VO> lstData = new ArrayList<Rpt7_2_14VO>();
			List<Object> inParams = new ArrayList<Object>();

			inParams.add(2);
			inParams.add(strListShopId);
			inParams.add(Types.VARCHAR);

			inParams.add(3);
			inParams.add(parentStaffId);
			inParams.add(Types.NUMERIC);

			inParams.add(4);
			inParams.add(new java.sql.Date(fromDate.getTime()));
			inParams.add(Types.DATE);

			inParams.add(5);
			inParams.add(new java.sql.Date(toDate.getTime()));
			inParams.add(Types.DATE);

			inParams.add(6);
			if (checkMap) {
				inParams.add(1);
			} else {
				inParams.add(0);
			}
			inParams.add(java.sql.Types.NUMERIC);

			lstData = repo.getListByQueryDynamicFromPackage(Rpt7_2_14VO.class, sql.toString(), inParams, null);

			return lstData;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author vuongmq
	 * @param strListShopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @desciption: bao cao phan tich doanh thu ban hang SKU 7_2_15
	 * @throws BusinessException
	 */
	@Override
	public List<Rpt_7_2_15> getListPTDTBHSKU7_2_15(String strListShopId, Long parentStaffId, Date fromDate, Date toDate, boolean checkMap) throws BusinessException {
		try {
			if (strListShopId == null || strListShopId.length() <= 0) {
				throw new IllegalArgumentException("strListShopId is null or <= 0");
			}
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_SHOP_REPORT.BAOCAO_PTDTBHSKU_7_2_15(?, :listShopId, :parentStaffId, :fromDate, :toDate, :checkMap)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);

			params.add(3);
			params.add(parentStaffId);
			params.add(java.sql.Types.NUMERIC);

			params.add(4);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(5);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(6);
			if (checkMap) {
				params.add(1);
			} else {
				params.add(0);
			}
			params.add(java.sql.Types.NUMERIC);

			List<Rpt_7_2_15> list = repo.getListByQueryDynamicFromPackage(Rpt_7_2_15.class, sql.toString(), params, null);
			/*
			 * if(list.size() > 0) { for (Rpt_7_2_15 result : list) {
			 * result.safeSetNull(); } return list; }
			 */
			return list;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author vuongmq
	 * @param strListShopId
	 * @param fromDate
	 * @param toDate
	 * @since 14-August 2014
	 * @return
	 * @desciption: bao cao don hang tra theo gia tri
	 * @throws BusinessException
	 * */
	@Override
	public List<Rpt_7_2_16> getListBCDHT_THEOGIATRI_7_2_16(String strListShopId, Date fromDate, Date toDate) throws BusinessException {
		try {
			if (strListShopId == null || strListShopId.length() <= 0) {
				throw new IllegalArgumentException("strListShopId is null or <= 0");
			}
			StringBuilder sql = new StringBuilder();
			//sql.append("call PKG_SHOP_REPORT.BAOCAO_DHT_TONGGIATRI_7_2_16(?, :listShopId, :fromDate, :toDate)");
			sql.append("call PKG_SHOP_REPORT.BAOCAO_DHT_TONGGIATRI_7_2_16(?, :listShopId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			params.add(3);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(4);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			List<Rpt_7_2_16> list = repo.getListByQueryDynamicFromPackage(Rpt_7_2_16.class, sql.toString(), params, null);
			if (list.size() > 0) {
				for (Rpt_7_2_16 result : list) {
					result.safeSetNull();
				}
				return list;
			}
			return null;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	/**
	 * @author vuongmq
	 * @param strListShopId
	 * @param fromDate
	 * @param toDate
	 * @since 14-August 2014
	 * @return
	 * @desciption: bao cao don hang tra theo tong so don hang
	 * @throws BusinessException
	 * */
	@Override
	public List<Rpt_7_2_17> getListBCDHT_THEOTONGDONHANG_7_2_17(String strListShopId, Date fromDate, Date toDate) throws BusinessException {
		try {
			if (strListShopId == null || strListShopId.length() <= 0) {
				throw new IllegalArgumentException("strListShopId is null or <= 0");
			}
			StringBuilder sql = new StringBuilder();
			//sql.append("call PKG_SHOP_REPORT.BAOCAO_DHT_TONGDONHANG_7_2_17(?, :listShopId, :fromDate, :toDate)");
			sql.append("call PKG_SHOP_REPORT.BAOCAO_DHT_TONGDONHANG_7_2_17(?, :listShopId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			params.add(3);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(4);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			List<Rpt_7_2_17> list = repo.getListByQueryDynamicFromPackage(Rpt_7_2_17.class, sql.toString(), params, null);
			if (list.size() > 0) {
				for (Rpt_7_2_17 result : list) {
					result.safeSetNull();
				}
				return list;
			}
			return null;
		} catch (DataAccessException e) {
			throw new BusinessException(e);
		}
	}

	@Override
	public List<Rpt7_7_6VO> getListReport7_7_6(String strListShopId, Date fromDate, Date toDate) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			StringBuilder sql = new StringBuilder();
			//sql.append("call VUONGHN.RPT7_7_6(?, :LISTSHOPID, :FDATE, :TDATE)");
			//sql.append("call PKG_SHOP_REPORT.RPT7_7_6(?, :LISTSHOPID, :FDATE, :TDATE)");
			sql.append("call PKG_SHOP_REPORT.RPT7_7_6(?, :LISTSHOPID, :FDATE, :TDATE)");
			List<Object> inParams = new ArrayList<Object>();

			inParams.add(2);
			inParams.add(strListShopId);
			inParams.add(Types.VARCHAR);

			inParams.add(3);
			inParams.add(new java.sql.Date(fromDate.getTime()));
			inParams.add(Types.DATE);

			inParams.add(4);
			inParams.add(new java.sql.Date(toDate.getTime()));
			inParams.add(Types.DATE);

			List<Rpt7_7_6VODetail> lstData = repo.getListByQueryDynamicFromPackage(Rpt7_7_6VODetail.class, sql.toString(), inParams, null);
			List<Rpt7_7_6VO> result = new ArrayList<Rpt7_7_6VO>();
			if (lstData != null && lstData.size() > 0) {
				Map<String, List<Rpt7_7_6VODetail>> map = GroupUtility.collectionToMap(lstData, "shopCode");
				for (String key : map.keySet()) {
					Rpt7_7_6VO vo = new Rpt7_7_6VO();
					List<Rpt7_7_6VODetail> lstDetail = map.get(key);
					Integer tongSKU = 0;
					BigDecimal tongTTT = BigDecimal.ZERO;
					BigDecimal tongTST = BigDecimal.ZERO;
					vo.setShopCode(lstDetail.get(0).getShopCode());
					vo.setShopName(lstDetail.get(0).getShopName());
					vo.setShopAddress(lstDetail.get(0).getShopAddress());
					vo.setShopPhone(lstDetail.get(0).getShopPhone());
					for (Rpt7_7_6VODetail detail : lstDetail) {
						tongSKU += detail.getSKU();
						tongTTT = tongTTT.add(detail.getThanhTienTT());
						tongTST = tongTST.add(detail.getThanhTienST());
					}
					vo.setTongSKU(tongSKU);
					vo.setTongTTT(tongTTT);
					vo.setTongTST(tongTST);
					//vo.setLstDetail(lstData);
					vo.setLstDetail(lstDetail);
					result.add(vo);
				}
			}
			return result;
		} catch (Exception ex) {
			throw new BusinessException(ex);
		}
	}

	/**
	 * @author sangtn
	 * @since 23/05/2014
	 * @description Phiếu xuất h?�ng theo nh?�n vi?�n b?�n h?�ng 7.1.7
	 * 
	 * @author hunglm16
	 * @since October 3,2014
	 * @description Update phan quyen CMS
	 * */
	@Override
	public List<RptExSaleOrder2VO> getListSaleOrderBySaleStaffGroupByProductCode(Long shopId, String staffSaleCode, Long deliveryStaffId, String listSaleOrdernumber, Date fromDate, Date toDate, Long staffRootId, Integer flagCMS)
			throws BusinessException {
		try {
			List<RptExSaleOrderVO> lst = new ArrayList<RptExSaleOrderVO>();
			StringBuilder sql = new StringBuilder();

			sql.append("call PKG_SHOP_REPORT.BC_PXHTNVBH(?, :shopId, :lstSaleStaffId, :deliveryStaffId, :listSaleOrdernumber, :fromDate , :toDate, :staffRootId, :flagCMS)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			params.add(3);
			params.add(StringUtility.isNullOrEmpty(staffSaleCode) ? "" : staffSaleCode);
			params.add(Types.VARCHAR);
			params.add(4);
			params.add(deliveryStaffId);
			params.add(java.sql.Types.NUMERIC);
			params.add(5);
			params.add(listSaleOrdernumber);
			params.add(java.sql.Types.VARCHAR);
			params.add(6);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(7);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			params.add(8);
			params.add(staffRootId);
			params.add(java.sql.Types.NUMERIC);
			params.add(9);
			params.add(flagCMS);
			params.add(java.sql.Types.NUMERIC);

			lst = repo.getListByQueryDynamicFromPackageClobOrArray(RptExSaleOrderVO.class, sql.toString(), params, null, ",");

			List<RptExSaleOrder2VO> lstVo2 = new ArrayList<RptExSaleOrder2VO>();

			//2: SaleStaffInfo; lstOrderNumber
			//1: productCode; productName; price; saleAmount; promoAmount;
			for (int i = 0; i < lst.size(); i++) {
				RptExSaleOrderVO voS = lst.get(i);
				RptExSaleOrder2VO vo2 = new RptExSaleOrder2VO();
				SortedMap<String, String> sortedOrderNumber = new TreeMap<String, String>();
				vo2.setSaleStaffInfo(voS.getSaleStaffInfo());
				vo2.setSaleStaffId(voS.getSaleStaffId());

				String lstOrderNumber = "";
				Integer thungPromo = 0;
				Integer thungSale = 0;
				Integer lePromo = 0;
				Integer leSale = 0;
				BigDecimal totalPromoAmount = new BigDecimal(0);
				BigDecimal totalSaleAmount = new BigDecimal(0);

				List<RptExSaleOrder1VO> lstVo1 = new ArrayList<RptExSaleOrder1VO>();
				for (int j = i; j < lst.size(); j++) {
					if (lst.get(j).getSaleStaffInfo() != null)
						if (lst.get(j).getSaleStaffInfo().equals(voS.getSaleStaffInfo())) {
							RptExSaleOrder1VO vo1 = new RptExSaleOrder1VO();
							vo1.setPrice(lst.get(j).getPrice());
							vo1.setProductCode(lst.get(j).getProductCode());
							vo1.setProductName(lst.get(j).getProductName());
							vo1.setCheckLot(lst.get(j).getCheckLot());
							if (!StringUtility.isNullOrEmpty(lst.get(j).getOrderNumber())) {
								String[] __lstSaleOrder = lst.get(j).getOrderNumber().split(",");
								for (String __saleOrderTmp : __lstSaleOrder) {
									String saleOrderTmp = __saleOrderTmp.trim();
									if (StringUtility.isNullOrEmpty(lstOrderNumber)) {
										lstOrderNumber = lst.get(j).getOrderNumber();
									} else if (lstOrderNumber.indexOf(saleOrderTmp) == -1) {
										lstOrderNumber += ", " + saleOrderTmp;
									}
								}
							}
							//promoAmount
							//saleAmount
							BigDecimal promoAmount = new BigDecimal(0);
							BigDecimal saleAmount = new BigDecimal(0);
							//BigDecimal discountAmount = new BigDecimal(0);

							List<RptExSaleOrderVO> lstVo = new ArrayList<RptExSaleOrderVO>();
							for (int k = j; k < lst.size(); k++) {
								if (lst.get(k).getProductCode().equals(vo1.getProductCode()) && lst.get(k).getSaleStaffInfo().equals(voS.getSaleStaffInfo())) {
									RptExSaleOrderVO vo = lst.get(k);
									lstVo.add(vo);
									if (vo.getPrice() != null && vo.getSaleQuantity() != null) {
										saleAmount = saleAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getSaleQuantity())));
										totalSaleAmount = totalSaleAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getSaleQuantity())));
										//discountAmount = discountAmount.add(vo.getDiscountAmount());
									}
									if (vo.getPrice() != null && vo.getPromoQuantity() != null) {
										promoAmount = promoAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getPromoQuantity())));
										totalPromoAmount = totalPromoAmount.add(vo.getPrice().multiply(new BigDecimal(vo.getPromoQuantity())));
										//discountAmount = discountAmount.add(vo.getDiscountAmount());
									}
									thungPromo += vo.getThungPromo();
									thungSale += vo.getThungSale();
									lePromo += vo.getLePromo();
									leSale += vo.getLeSale();

									String sOrderNumberTemp = lst.get(k).getOrderNumber().trim();
									if (sOrderNumberTemp.indexOf(",") >= 0) {
										String[] sSplitChildOrderNumber = sOrderNumberTemp.split(",");
										for (int t = 0; t < sSplitChildOrderNumber.length; t++) {
											String orderNumberTest = sSplitChildOrderNumber[t].trim();
											if (lstOrderNumber != null && lstOrderNumber.indexOf(orderNumberTest) == -1) {
												lstOrderNumber += ", " + orderNumberTest;
											}
										}
									} else {
										if (lstOrderNumber != null && lstOrderNumber.indexOf(sOrderNumberTemp) == -1) {
											lstOrderNumber += ", " + lst.get(k).getOrderNumber();
										}
									}

									j = k;
								} else
									break;
							}
							vo1.setPromoAmount(promoAmount);
							vo1.setSaleAmount(saleAmount);

							for (RptExSaleOrderVO vo : lstVo) {
								vo.setPromoAmount(promoAmount);
								vo.setSaleAmount(saleAmount);
								if (vo.getOrderNumber() != null)
									sortedOrderNumber.put(vo.getOrderNumber(), vo.getOrderNumber());
							}

							vo1.setLstRptExSaleOrderVO(lstVo);
							vo1.setDiscountAmount(lst.get(j).getDiscountAmount());
							lstVo1.add(vo1);
							i = j;
						} else
							break;
				}
				vo2.setTotalPromoQuantity(thungPromo.toString() + "/" + lePromo.toString());
				vo2.setTotalSaleQuantity(thungSale.toString() + "/" + leSale.toString());
				vo2.setTotalPromoAmount(totalPromoAmount);
				vo2.setTotalSaleAmount(totalSaleAmount);
				vo2.setLstSaleOrder(lstVo1);

				sortedOrderNumber.clear();
				vo2.setLstOrderNumber(lstOrderNumber);
				lstVo2.add(vo2);
			}
			return lstVo2;
		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

	@Override
	public List<GS_4_2_VO> getListVisit(Long staffIdRoot, Long userId, String strListShopId, Date fromDate, Date toDate) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<GS_4_2_VO> lstResult = new ArrayList<GS_4_2_VO>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_REPORT_GS.REPORT_1_3_KQDT(?,:userId,:roleId,:lstShopId,:lstGSId, :lstNVBHId, :fromDate, :toDate)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(null);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(6);
			params.add(null);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(7);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(8);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_4_2_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public List<GS_KHTT> getListKHTT(Long cycleId, Long shopId, Date fromDate, Date toDate, String product, String subCat, String cat, String lstStaff) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {
			List<GS_KHTT> lstResult = new ArrayList<GS_KHTT>();
			StringBuilder sql = new StringBuilder();
			sql.append("call PKG_RPT_KHTT.GET_KHTT(?,:cycleId,:shopId,:productId,:subcatId, :catId, :fromDate, :toDate, :lstStaff)");
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(cycleId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(shopId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(product);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(subCat);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(6);
			params.add(cat);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(7);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(8);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);
			
			params.add(9);
			params.add(lstStaff);
			params.add(java.sql.Types.VARCHAR);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_KHTT.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}
	@Override
	public List<GS_1_4_VO> getNewOpendCustomerByNVBH(Long staffIdRoot, Long userId, String strListShopId, Date fromDate, Date toDate, Integer check, String strListGsId, String strListNvbhId) throws BusinessException {
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		try {

			List<GS_1_4_VO> lstResult = new ArrayList<GS_1_4_VO>();
			StringBuilder sql = new StringBuilder();	
			if (check == 0) {
				sql.append("call PKG_REPORT_GS.REPORT_1_4_BCMMKH(?,:userId,:roleId,:lstShopId,:lstGSId, :lstNVBHId, :fromDate, :toDate)");	
			} else {
				sql.append("call PKG_REPORT_GS.REPORT_1_4_2_BCMMKH(?,:userId,:roleId,:lstShopId,:lstGSId, :lstNVBHId, :fromDate, :toDate)");		
			}							
			List<Object> params = new ArrayList<Object>();
			params.add(2);
			params.add(userId);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(3);
			params.add(staffIdRoot);
			params.add(java.sql.Types.NUMERIC);
			
			params.add(4);
			params.add(strListShopId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(5);
			params.add(strListGsId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(6);
			params.add(strListNvbhId);
			params.add(java.sql.Types.VARCHAR);
			
			params.add(7);
			params.add(new java.sql.Date(fromDate.getTime()));
			params.add(java.sql.Types.DATE);

			params.add(8);
			params.add(new java.sql.Date(toDate.getTime()));
			params.add(java.sql.Types.DATE);

			lstResult = repo.getListByQueryDynamicFromPackage(GS_1_4_VO.class, sql.toString(), params, null);
			return lstResult;

		} catch (DataAccessException ex) {
			throw new BusinessException(ex);
		}
	}

}

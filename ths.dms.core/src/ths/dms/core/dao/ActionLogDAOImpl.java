package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ActionLog;
import ths.dms.core.entities.enumtype.ActionLogObjectType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.filter.RptStaffSaleFilter;
import ths.dms.core.entities.vo.ActionLogCustomerVO;
import ths.dms.core.entities.vo.ActionLogVO;
import ths.dms.core.entities.vo.AmountPlanCustomerVO;
import ths.dms.core.entities.vo.CustomerPositionVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.TimeVisitCustomerVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class ActionLogDAOImpl implements ActionLogDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public ActionLog createActionLog(ActionLog actionLog, LogInfoVO logInfo)
			throws DataAccessException {
		if (actionLog == null) {
			throw new IllegalArgumentException("actionLog");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.create(actionLog);
		actionLog = repo.getEntityById(ActionLog.class, actionLog.getId());
		try {
			actionGeneralLogDAO
					.createActionGeneralLog(null, actionLog, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return actionLog;
	}

	@Override
	public void deleteActionLog(ActionLog actionLog, LogInfoVO logInfo)
			throws DataAccessException {
		if (actionLog == null) {
			throw new IllegalArgumentException("actionLog");
		}
		repo.delete(actionLog);
		try {
			actionGeneralLogDAO
					.createActionGeneralLog(actionLog, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public ActionLog getActionLogById(long id) throws DataAccessException {
		return repo.getEntityById(ActionLog.class, id);
	}

	@Override
	public void updateActionLog(ActionLog actionLog, LogInfoVO logInfo)
			throws DataAccessException {
		if (actionLog == null) {
			throw new IllegalArgumentException("actionLog");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}

		ActionLog temp = this.getActionLogById(actionLog.getId());
		repo.update(actionLog);
		try {
			actionGeneralLogDAO
					.createActionGeneralLog(temp, actionLog, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.ActionLogDAO#getListVisitPlanBySuperVisor(java.lang.String, java.lang.String, java.util.Date)
	 */
	@Override
	public List<ActionLogVO> getListVisitPlanBySuperVisor(KPaging<ActionLogVO> kPaging, Long shopId,
			String staffCode, Date visitPlanDate, Boolean isHasChild) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * from (");
		sql.append("SELECT tb1.shop_code AS shopCode,");
		sql.append(" tb1.shop_id 		AS shopId,");
		sql.append(" tb1.shop_name      AS shopName,");
		sql.append(" tb1.staff_id       AS staffId,");
		sql.append(" tb1.staff_code     AS staffCode,");
		sql.append(" tb1.staff_name     AS staffName,");
		sql.append(" tb1.count          AS totalVisit,");
		sql.append(" nvl(tb2.less2Phut,0)      AS numLess2Minute,");
		sql.append(" nvl(tb2.less30Phut,0)          AS numBetween2And30Minute,");
		sql.append(" nvl(tb2.over30Phut,0)          AS numOver30Minute,");
		sql.append(" nvl(tb3.countOrder, 0)     AS numOrder,");
		sql.append(" nvl(tb2.countNT,0)     AS numWrongRouting,");
		sql.append(" nvl(tb2.countTT,0)     AS numCorrectRouting");
		sql.append(" FROM");
		sql.append(" (SELECT COUNT(rc.customer_id) AS COUNT,");
		sql.append(" s.shop_code,");
		sql.append(" s.shop_id,");
		sql.append(" s.shop_name,");
		sql.append(" sf.staff_id,");
		sql.append(" sf.staff_code,");
		sql.append(" sf.staff_name");
		sql.append(" FROM visit_plan vp,");
		sql.append(" routing_customer rc,");
		sql.append(" routing r,");
		sql.append(" shop s,");
		sql.append(" customer cmer,");
		sql.append(" staff sf");
		sql.append(" WHERE vp.routing_id = rc.routing_id");
		sql.append(" AND vp.from_date   < TRUNC(? + 1)");
		params.add(visitPlanDate);
		sql.append(" AND (vp.to_date     >= TRUNC(?) or vp.to_date is null)");
		params.add(visitPlanDate);
		sql.append(" AND rc.customer_id  = cmer.customer_id");
		sql.append(" AND cmer.status     = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND sf.status       = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND s.shop_id       = vp.shop_id");
		sql.append(" AND vp.staff_id     = sf.staff_id");
		sql.append(" AND vp.status 		 = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND rc.status 		 = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND r.status 		 = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND r.routing_id 	 = rc.routing_id");
		sql.append(" 	and (  (to_char(?, 'D') = 1 and rc.sunday = 1 )");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 2 and rc.monday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 3 and rc.tuesday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 4 and rc.wednesday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 5 and rc.thursday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 6 and rc.friday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 7 and rc.saturday = 1) ");
		params.add(visitPlanDate);
		sql.append("     ) ");
		sql.append(" AND mod ((to_char(?,'iw') - rc.START_WEEK), rc.WEEK_INTERVAL) = 0");
		params.add(visitPlanDate);
		sql.append(" and to_char(?,'iw') - rc.START_WEEK >= 0");
		params.add(visitPlanDate);
		sql.append(" GROUP BY s.shop_code,");
		sql.append(" s.shop_id,");
		sql.append(" s.shop_name,");
		sql.append(" sf.staff_id,");
		sql.append(" sf.staff_code,");
		sql.append(" sf.staff_name");
		sql.append(" ) tb1");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT staff_id,");
		sql.append(" sum( CASE  WHEN IS_OR = 1 and is_or is not null THEN 1 ELSE 0 END) AS countNT,");
		sql.append(" sum( CASE WHEN IS_OR = 0 and is_or is not null THEN 1 ELSE 0 END) AS countTT,");
		sql.append(" sum (case when VISIT_TIME is not null and VISIT_TIME < 2 then 1 else 0 end) as less2Phut,");
		sql.append(" sum (case when VISIT_TIME is not null and VISIT_TIME <= 30 and visit_time >= 2 then 1 else 0 end) as less30Phut,");
		sql.append(" sum (case when VISIT_TIME is not null and VISIT_TIME > 30 then 1 else 0 end) as over30Phut");
		sql.append(" FROM");
		sql.append(" (SELECT AL.STAFF_ID                AS STAFF_ID,");
		sql.append(" AL.CUSTOMER_ID                     AS CUSTOMER_ID,");
		sql.append(" AL.OBJECT_ID                       AS OBJECT_ID,");
		sql.append(" AL.OBJECT_TYPE                     AS OBJECT_TYPE,");
		sql.append(" AL.IS_OR                           AS IS_OR,");
		sql.append(" AL.START_TIME                      AS START_TIME,");
		sql.append(" AL.END_TIME                        AS END_TIME,");
		sql.append(" ROUND ((AL.END_TIME - AL.START_TIME)*1440 , 2) AS VISIT_TIME");
		sql.append(" FROM ACTION_LOG AL,");
		sql.append(" CUSTOMER CT");
		sql.append(" WHERE 1               = 1");
		sql.append(" AND AL.CUSTOMER_ID    = CT.CUSTOMER_ID");
		sql.append(" AND CT.STATUS         = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND (AL.OBJECT_TYPE = ? OR AL.OBJECT_TYPE     = ?)");
		params.add(ActionLogObjectType.VISIT.getValue());
		params.add(ActionLogObjectType.END_VISIT.getValue());
		sql.append(" AND AL.END_TIME      IS NOT NULL");
//		sql.append(" OR AL.OBJECT_TYPE     = ?)");
//		params.add(ActionLogObjectType.ORDER.getValue());
		sql.append(" AND AL.START_TIME    >= TRUNC(?)");
		params.add(visitPlanDate);
		sql.append(" AND AL.START_TIME        < TRUNC(? + 1)");
		params.add(visitPlanDate);
		sql.append(" ) GROUP BY staff_id");
		sql.append(" ) tb2");
		sql.append(" ON tb1.staff_id = tb2.staff_id");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT staff_id,");
		sql.append(" COUNT(distinct(customer_id)) AS countOrder");
		sql.append(" FROM sale_order");
		sql.append(" WHERE type      = ?");
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		sql.append(" AND order_date >= TRUNC(?)");
		params.add(visitPlanDate);
		sql.append(" AND order_date  < TRUNC(? + 1)");
		params.add(visitPlanDate);
		sql.append(" AND APPROVED in (?, ?)");
		params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append(" GROUP BY staff_id) tb3");
		sql.append(" ON tb1.staff_id = tb3.staff_id");
		sql.append(" )");
		sql.append(" WHERE 1 = 1");
		if (shopId != null) {
			if (Boolean.TRUE.equals(isHasChild)) {
				sql.append(" and shopId in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
			} else {
				sql.append(" and shopId = ?");
			}
			params.add(shopId);
		}
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and upper(staffCode) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		sql.append(" order by shopCode, staffCode");
//		System.out.println(TestUtil.addParamToQuery(sql.toString(), params));
		String[] fieldNames = { "shopCode", // 0
				"shopName", // 1
				"staffId", // 2
				"staffCode", // 2
				"staffName", // 3
				"totalVisit", // 4
				"numLess2Minute", // 5
				"numBetween2And30Minute", // 6
				"numOver30Minute", // 6''
				"numOrder", // 6'
				"numWrongRouting", // 7
				"numCorrectRouting", // 7'
				"shopId", // 8
		};

		Type[] fieldTypes = { StandardBasicTypes.STRING, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.LONG, // 2
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.INTEGER, // 4
				StandardBasicTypes.INTEGER, // 5
				StandardBasicTypes.INTEGER, // 6
				StandardBasicTypes.INTEGER, // 6''
				StandardBasicTypes.INTEGER, // 6'
				StandardBasicTypes.INTEGER, // 7
				StandardBasicTypes.INTEGER, // 7'
				StandardBasicTypes.LONG, // 8
		};
		
		if (kPaging == null)
			return repo.getListByQueryAndScalar(ActionLogVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
		else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(*) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(
					ActionLogVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params,
					kPaging);
		}
	}
	

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.ActionLogDAO#getListCustomerPosition(ths.dms.core.entities.enumtype.KPaging, java.lang.Long, java.util.Date)
	 */
	@Override
	public List<CustomerPositionVO> getListCustomerPosition(
			KPaging<CustomerPositionVO> kPaging, Long staffId,
			Date visitPlanDate, Integer day) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT tb1.customer_code as customerCode,");
		sql.append(" tb1.customer_name as customerName,");
		sql.append(" tb1.lat as customerLat,");
		sql.append(" tb1.lng as customerLng,");
		sql.append(" tb1.address            AS address,");
		sql.append(" tb1.mobiphone          AS mobiphone,");
		sql.append(" tb1.phone              AS phone,");
		switch (day.intValue()) {
		case 1: {
			sql.append(" tb1.seq8 as seq,");
			break;
		}
		case 2: {
			sql.append(" tb1.seq2 as seq,");
			break;
		}
		case 3: {
			sql.append(" tb1.seq3 as seq,");
			break;
		}
		case 4: {
			sql.append(" tb1.seq4 as seq,");
			break;
		}
		case 5: {
			sql.append(" tb1.seq5 as seq,");
			break;
		}
		case 6: {
			sql.append(" tb1.seq6 as seq,");
			break;
		}
		case 7: {
			sql.append(" tb1.seq7 as seq,");
			break;
		}

		default:
			break;
		}
		sql.append(" nvl((select distance_order from shop where shop_id = ");
		sql.append(" (select shop_id from staff where staff_id = tb1.staff_id)), 0) as distanceOrder,");
		sql.append(" tb2.object_type as objectType,");
		sql.append(" tb2.start_time as startTime,");
		sql.append(" tb2.end_time as endtime,");
		sql.append(" tb2.lat AS staffLat,");
		sql.append(" tb2.lng AS staffLng,");
		sql.append(" nvl(tb2.is_or, 0) as isOr");
		sql.append(" FROM");
		sql.append(" (SELECT c.customer_id, c.customer_code, c.customer_name, c.lat, c.lng, c.mobiphone, c.phone, c.address,");
		sql.append(" tba.staff_id, tba.seq2, tba.seq3, tba.seq4, tba.seq5, tba.seq6, tba.seq7, tba.seq8, tba.seq");
		sql.append(" FROM customer c,");
		sql.append(" ((SELECT c.customer_id,");
		sql.append(" vp.staff_id,");
		sql.append(" rc.seq2 seq2,");
		sql.append(" rc.seq3 seq3,");
		sql.append(" rc.seq4 seq4,");
		sql.append(" rc.seq5 seq5,");
		sql.append(" rc.seq6 seq6,");
		sql.append(" rc.seq7 seq7,");
		sql.append(" rc.seq8 seq8,");
		sql.append(" NVL(rc.seq, 0) seq");
		sql.append(" FROM visit_plan vp,");
		sql.append(" routing_customer rc,");
		sql.append(" routing r,");
		sql.append(" customer c");
		sql.append(" WHERE vp.routing_id = rc.routing_id");
		sql.append(" AND vp.routing_id   = r.routing_id");
		sql.append(" AND rc.customer_id  = c.customer_id");
		sql.append(" AND r.status 		 = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND rc.status 		 = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND c.status        = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" 	and (  (to_char(?, 'D') = 1 and rc.sunday = 1 )");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 2 and rc.monday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 3 and rc.tuesday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 4 and rc.wednesday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 5 and rc.thursday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 6 and rc.friday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 7 and rc.saturday = 1) ");
		params.add(visitPlanDate);
		sql.append("     ) ");
		sql.append(" AND mod ((to_char(?,'iw') - rc.START_WEEK), rc.WEEK_INTERVAL) = 0");
		params.add(visitPlanDate);
		sql.append(" AND to_char(?,'iw') - rc.START_WEEK >= 0");
		params.add(visitPlanDate);
		sql.append(" AND vp.from_date   < TRUNC(? + 1)");
		params.add(visitPlanDate);
		sql.append(" AND (vp.to_date     >= TRUNC(?)");
		params.add(visitPlanDate);
		sql.append(" OR vp.to_date      IS NULL)");
		sql.append(" AND vp.staff_id     = ?)");
		params.add(staffId);
		sql.append(" UNION");
		sql.append(" (SELECT DISTINCT(customer_id) as customer_id, staff_id, null seq2, null seq3, null seq4, null seq5, null seq6, null seq7, null seq8, 0 AS seq");
		sql.append(" FROM action_log");
		sql.append(" WHERE staff_id  = ?");
		params.add(staffId);
		sql.append(" AND start_time >= TRUNC(?)");
		params.add(visitPlanDate);
		sql.append(" AND (end_time  IS NULL");
		sql.append(" OR start_time     < TRUNC(? + 1))");
		params.add(visitPlanDate);
		sql.append(" AND is_or		 = 1");
		sql.append("  )) tba");
		sql.append(" WHERE c.customer_id = tba.customer_id");
		sql.append(" ) tb1");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT *");
		sql.append(" FROM action_log");
		sql.append(" WHERE staff_id  = ?");
		params.add(staffId);
		sql.append(" AND (object_type = ? or object_type = ? or object_type = ?)");
		params.add(ActionLogObjectType.VISIT.getValue());
		params.add(ActionLogObjectType.END_VISIT.getValue());
		params.add(ActionLogObjectType.ORDER.getValue());
		sql.append(" AND start_time >= TRUNC(?)");
		params.add(visitPlanDate);
		sql.append(" AND (end_time  IS NULL");
		sql.append(" OR start_time     < TRUNC(? + 1))");
		params.add(visitPlanDate);
		sql.append(" ) tb2");
		sql.append(" ON tb1.staff_id = tb2.staff_id AND tb1.customer_id = tb2.customer_id");
		sql.append(" ORDER BY ");
		switch (day.intValue()) {
		case 1: {
			sql.append(" tb1.seq8 nulls last,");
			break;
		}
		case 2: {
			sql.append(" tb1.seq2 nulls last,");
			break;
		}
		case 3: {
			sql.append(" tb1.seq3 nulls last,");
			break;
		}
		case 4: {
			sql.append(" tb1.seq4 nulls last,");
			break;
		}
		case 5: {
			sql.append(" tb1.seq5 nulls last,");
			break;
		}
		case 6: {
			sql.append(" tb1.seq6 nulls last,");
			break;
		}
		case 7: {
			sql.append(" tb1.seq7 nulls last,");
			break;
		}

		default:
			break;
		}
		sql.append(" tb2.start_time, tb1.customer_code");

		String[] fieldNames = { "customerCode", // 0
				"customerName", // 1
				"customerLat", // 2
				"customerLng", // 2
				"address", // 2'
				"mobiphone", // 2''
				"phone", // 2'''
				"seq", // 3
				"distanceOrder", // 3'
				"objectType", // 4
				"startTime", // 5
				"endtime", // 6
				"staffLat", // 7
				"staffLng", // 8
				"isOr", // 9
		};

		Type[] fieldTypes = { StandardBasicTypes.STRING, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.DOUBLE, // 2
				StandardBasicTypes.DOUBLE, // 2
				StandardBasicTypes.STRING, // 2'
				StandardBasicTypes.STRING, // 2''
				StandardBasicTypes.STRING, // 2'''
				StandardBasicTypes.INTEGER, // 3
				StandardBasicTypes.BIG_DECIMAL, // 3'
				StandardBasicTypes.INTEGER, // 4
				StandardBasicTypes.TIMESTAMP, // 5
				StandardBasicTypes.TIMESTAMP, // 6
				StandardBasicTypes.DOUBLE, // 7
				StandardBasicTypes.DOUBLE, // 8
				StandardBasicTypes.INTEGER, // 9
		};

		if (kPaging == null){
			/*System.out.println(TestUtils.addParamToQuery(sql.toString(),params.toArray()));*/
			return repo.getListByQueryAndScalar(CustomerPositionVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
		}
		else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(*) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(
					CustomerPositionVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params,
					kPaging);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.ActionLogDAO#getListVisitCustomer(java.util.Date, java.util.Date, java.lang.Long, java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<TimeVisitCustomerVO> getListVisitCustomer(Date fromDate,
			Date toDate, Long shopId, Long superId, Long staffId,
			Boolean getShopOnly) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select parentSuperShopCode, superShopCode, shopCode, shopName, superName, staffCode, staffName, toDate,");
		sql.append(" nvl(customerCode, countCustomer) as customerCode, customerName, customerAddress, startTime, endTime ");
		sql.append(" FROM (");
		sql.append(" SELECT parentSuperShopCode, superShopCode, shopCode, shopName, superName, staffCode, staffName,");
		sql.append(" toDate, customerCode, customerName, customerAddress, startTime, endTime,");
		sql.append(" count(customerCode) as countCustomer,");
		sql.append(" grouping_id(parentSuperShopCode, superShopCode, shopCode, shopName, superName, staffCode, staffName, toDate, customerCode, customerName, customerAddress, startTime, endTime) g");
		sql.append(" FROM");
		sql.append(" (SELECT al.start_time as startTime,");
		sql.append(" al.end_time as endTime,");
		sql.append(" trunc(al.start_time) as toDate,");
		sql.append(" st.staff_code as staffCode,");
		sql.append(" st.staff_name as staffName,");
		sql.append(" s.shop_code as shopCode,");
		sql.append(" s.shop_name as shopName,");
		sql.append(" (select staff_name from staff where staff_id = st.staff_owner_id) as superName,");
		sql.append(" c.short_code as customerCode,");
		sql.append(" c.customer_name as customerName,");
		sql.append(" c.address as customerAddress,");
		sql.append(" (select s.shop_code from shop s, channel_type ct where s.shop_id in (");
		sql.append(" select shop_id from shop start with shop_id = st.shop_id connect by prior parent_shop_id = shop_id) and s.shop_type_id = ct.channel_type_id");
		sql.append(" and ct.type = 1 and ct.object_type = 2) as superShopCode,");
		sql.append(" (select s.shop_code from shop s, channel_type ct where s.shop_id in (");
		sql.append(" select shop_id from shop start with shop_id = st.shop_id connect by prior parent_shop_id = shop_id) and s.shop_type_id = ct.channel_type_id");
		sql.append(" and ct.type = 1 and ct.object_type = 1) as parentSuperShopCode");
		sql.append(" FROM action_log al");
		sql.append(" JOIN staff st");
		sql.append(" ON al.staff_id = st.staff_id and st.status = 1");
		sql.append(" JOIN shop s");
		sql.append(" ON s.shop_id         = st.shop_id and s.status = 1");
		sql.append(" JOIN customer c");
		sql.append(" ON al.customer_id     = c.customer_id");
		sql.append(" WHERE (al.object_type = ? or al.object_type = ?)");
		params.add(ActionLogObjectType.VISIT.getValue());
		params.add(ActionLogObjectType.END_VISIT.getValue());
		sql.append(" AND s.status         = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND st.status        = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND st.staff_type_id in (select channel_type_id from channel_type where status = ? and type = ? and object_type in (?,?))");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ChannelTypeType.STAFF.getValue());
		params.add(StaffObjectType.NVBH.getValue());
		params.add(StaffObjectType.NVVS.getValue());
		sql.append(" AND al.customer_id is not null");
		sql.append(" AND al.start_time   >= TRUNC(?)");
		params.add(fromDate);
		sql.append(" AND al.end_time     IS NOT NULL");
		sql.append(" AND al.start_time      < TRUNC(? + 1)");
		params.add(toDate);
		if (shopId != null) {
			if (Boolean.TRUE.equals(getShopOnly)) {
				sql.append(" AND st.shop_id = ?");
			} else {
				sql.append(" AND st.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
			}
			params.add(shopId);
		}
		if (superId != null) {
			sql.append(" AND st.staff_owner_id = ?");
			params.add(superId);
		}
		if (staffId != null) {
			sql.append(" AND st.staff_id = ?");
			params.add(staffId);
		}
		sql.append(" )");
		sql.append(" GROUP BY rollup (parentSuperShopCode, superShopCode, shopCode, shopName, superName, staffCode, staffName, toDate, customerCode, customerName, customerAddress, startTime, endTime)");
		sql.append(" having grouping_id(parentSuperShopCode, superShopCode, shopCode, shopName, superName, staffCode, staffName, toDate, customerCode, customerName, customerAddress, startTime, endTime) in (0,63,511,2047,4095,8191)");
		sql.append(" ORDER BY parentSuperShopCode nulls last, superShopCode nulls last, shopCode,");
		sql.append(" superName nulls last, staffCode, toDate, customerCode, startTime");
		sql.append(" )");
		
		String[] fieldNames = { "startTime", // 0
				"endTime", // 1
				"toDate", // 2
				"staffCode", // 3
				"staffName", // 4
				"shopCode", // 5
				"shopName", // 6
				"superName", // 7
				"customerCode", // 8
				"customerName", // 9
				"customerAddress", // 10
				"superShopCode", // 11
				"parentSuperShopCode", // 12
		};

		Type[] fieldTypes = { StandardBasicTypes.TIMESTAMP, // 0
				StandardBasicTypes.TIMESTAMP, // 1
				StandardBasicTypes.DATE, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.STRING, // 7
				StandardBasicTypes.STRING, // 8
				StandardBasicTypes.STRING, // 9
				StandardBasicTypes.STRING, // 10
				StandardBasicTypes.STRING, // 11
				StandardBasicTypes.STRING, // 12
		};
		return repo.getListByQueryAndScalar(TimeVisitCustomerVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.ActionLogDAO#getRptVisitPlan(ths.dms.core.entities.enumtype.KPaging, java.lang.Long, java.lang.String, java.lang.String, java.util.Date, java.lang.Boolean)
	 */
	@Override
	public List<ActionLogVO> getRptVisitPlan(KPaging<ActionLogVO> kPaging,
			Long shopId, String staffCode, String superCode,
			Date visitPlanDate, Boolean isHasChild) throws DataAccessException {		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * from (");
		sql.append("SELECT tb1.shop_code AS shopCode,");
		sql.append(" tb1.shop_id 		AS shopId,");
		sql.append(" tb1.shop_name      AS shopName,");
		sql.append(" tb1.staff_id       AS staffId,");
		sql.append(" (select staff_code from staff where staff_id = tb1.staff_owner_id) as superCode,");
		sql.append(" (select staff_name from staff where staff_id = tb1.staff_owner_id) as superName,");
		sql.append(" tb1.staff_code     AS staffCode,");
		sql.append(" tb1.staff_name     AS staffName,");
		sql.append(" tb1.count          AS totalVisit,");
		sql.append(" tb2.minTime    	AS startTime,");
		sql.append(" tb2.maxTime     	AS endTime,");
		sql.append(" nvl(tb2.countNT,0)    		AS numWrongRouting,");
		sql.append(" nvl(tb2.countTT,0)            AS numCorrectRouting,");
		sql.append(" nvl(tb2.less5Phut,0)          AS numLess2Minute,");
		sql.append(" nvl(tb2.less30Phut,0)         AS numBetween2And30Minute,");
		sql.append(" nvl(tb2.less60Phut,0)         AS numOver30Minute,");
		sql.append(" nvl(tb2.over60Phut,0)         AS numOver60Minute,");
		sql.append(" nvl(tb2.totalVisitTime,0)     AS totalVisitTime");
		sql.append(" FROM");
		sql.append(" (SELECT COUNT(rc.customer_id) AS COUNT,");
		sql.append(" s.shop_code,");
		sql.append(" s.shop_id,");
		sql.append(" s.shop_name,");
		sql.append(" sf.staff_id,");
		sql.append(" sf.staff_owner_id,");
		sql.append(" sf.staff_code,");
		sql.append(" sf.staff_name");
		sql.append(" FROM visit_plan vp,");
		sql.append(" routing_customer rc,");
		sql.append(" routing r,");
		sql.append(" shop s,");
		sql.append(" customer cmer,");
		sql.append(" staff sf");
		sql.append(" WHERE vp.routing_id = rc.routing_id");
		sql.append(" AND vp.from_date   < TRUNC(? + 1)");
		params.add(visitPlanDate);
		sql.append(" AND (vp.to_date     >= TRUNC(?) or vp.to_date is null)");
		params.add(visitPlanDate);
		sql.append(" AND rc.customer_id  = cmer.customer_id");
		sql.append(" AND cmer.status     = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND sf.status       = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND s.shop_id       = vp.shop_id");
		sql.append(" AND vp.staff_id     = sf.staff_id");
		sql.append(" AND vp.status 		 = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND rc.status 		 = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND r.status 		 = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND r.routing_id 	 = rc.routing_id");
		sql.append(" 	and (  (to_char(?, 'D') = 1 and rc.sunday = 1 )");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 2 and rc.monday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 3 and rc.tuesday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 4 and rc.wednesday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 5 and rc.thursday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 6 and rc.friday = 1) ");
		params.add(visitPlanDate);
		sql.append("     or (to_char(?, 'D') = 7 and rc.saturday = 1) ");
		params.add(visitPlanDate);
		sql.append("     ) ");
		sql.append(" AND mod ((to_char(?,'iw') - rc.START_WEEK), rc.WEEK_INTERVAL) = 0");
		params.add(visitPlanDate);
		sql.append(" AND to_char(?,'iw') - rc.START_WEEK >= 0");
		params.add(visitPlanDate);
		sql.append(" GROUP BY s.shop_code,");
		sql.append(" s.shop_id,");
		sql.append(" s.shop_name,");
		sql.append(" sf.staff_id,");
		sql.append(" sf.staff_owner_id,");
		sql.append(" sf.staff_code,");
		sql.append(" sf.staff_name");
		sql.append(" ) tb1");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT staff_id,");
		sql.append(" sum( CASE  WHEN is_or = 1 and is_or is not null THEN 1 ELSE 0 END) AS countNT,");
		sql.append(" sum( CASE WHEN is_or = 0 and is_or is not null THEN 1 ELSE 0 END) AS countTT,");
		sql.append(" sum (case when visit_time is not null and visit_time < 5 then 1 else 0 end) as less5Phut,");
		sql.append(" sum (case when visit_time is not null and visit_time < 30 and visit_time >= 5 then 1 else 0 end) as less30Phut,");
		sql.append(" sum (case when visit_time is not null and visit_time <= 60 and visit_time >= 30 then 1 else 0 end) as less60Phut,");
		sql.append(" sum (case when visit_time is not null and visit_time > 60 then 1 else 0 end) as over60Phut,");
		sql.append(" min (start_time) as minTime,");
		sql.append(" max (end_time) as maxTime,");
		sql.append(" sum (case when visit_time is not null then visit_time else 0 end) as totalVisitTime");
		sql.append(" FROM");
		sql.append(" (SELECT AL.STAFF_ID                AS STAFF_ID,");
		sql.append(" AL.CUSTOMER_ID                     AS CUSTOMER_ID,");
		sql.append(" AL.OBJECT_ID                       AS OBJECT_ID,");
		sql.append(" AL.OBJECT_TYPE                     AS OBJECT_TYPE,");
		sql.append(" AL.IS_OR                           AS IS_OR,");
		sql.append(" AL.START_TIME                      AS START_TIME,");
		sql.append(" AL.END_TIME                        AS END_TIME,");
		sql.append(" ROUND ((AL.END_TIME - AL.START_TIME)*1440 , 2) AS VISIT_TIME");
		sql.append(" FROM ACTION_LOG AL,");
		sql.append(" CUSTOMER CT");
		sql.append(" WHERE 1               = 1");
		sql.append(" AND AL.CUSTOMER_ID    = CT.CUSTOMER_ID");
		sql.append(" AND CT.STATUS         = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND (AL.OBJECT_TYPE = ? OR AL.OBJECT_TYPE     = ?)");
		params.add(ActionLogObjectType.VISIT.getValue());
		params.add(ActionLogObjectType.END_VISIT.getValue());
		sql.append(" AND AL.END_TIME      IS NOT NULL");
//		sql.append(" OR AL.OBJECT_TYPE     = ?)");
//		params.add(ActionLogObjectType.ORDER.getValue());
		sql.append(" AND AL.START_TIME    >= TRUNC(?)");
		params.add(visitPlanDate);
		sql.append(" AND AL.START_TIME        < TRUNC(? + 1)");
		params.add(visitPlanDate);
		sql.append(" ) GROUP BY staff_id");
		sql.append(" ) tb2");
		sql.append(" ON tb1.staff_id = tb2.staff_id");
//		sql.append(" LEFT JOIN");
//		sql.append(" (SELECT staff_id,");
//		sql.append(" COUNT(distinct(customer_id)) AS countOrder");
//		sql.append(" FROM sale_order");
//		sql.append(" WHERE type      = ?");
//		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
//		sql.append(" AND order_date >= TRUNC(?)");
//		params.add(visitPlanDate);
//		sql.append(" AND order_date  < TRUNC(? + 1)");
//		params.add(visitPlanDate);
//		sql.append(" AND APPROVED in (?, ?)");
//		params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
//		params.add(SaleOrderStatus.APPROVED.getValue());
//		sql.append(" GROUP BY staff_id) tb3");
//		sql.append(" ON tb1.staff_id = tb3.staff_id");
		sql.append(" )");
		sql.append(" WHERE 1 = 1");
		if (shopId != null) {
			if (Boolean.TRUE.equals(isHasChild)) {
				sql.append(" and shopId in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
			} else {
				sql.append(" and shopId = ?");
			}
			params.add(shopId);
		}
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and upper(staffCode) = ? ");
			params.add(staffCode.toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(superCode)) {
			sql.append(" and upper(superCode) = ? ");
			params.add(superCode.toUpperCase());
		}
		//superCode
		sql.append(" order by shopCode, superCode, staffCode, startTime");
		
		String[] fieldNames = { "shopCode", // 0
				"shopName", // 1
				"staffId", // 2
				"superCode", // 2'
				"superName", // 2''
				"staffCode", // 2
				"staffName", // 3
				"totalVisit", // 4
				"shopId", // 5
				"startTime", // 6
				"endTime", // 7
				"numWrongRouting", // 8
				"numCorrectRouting", // 9
				"numLess2Minute", // 10
				"numBetween2And30Minute", // 11
				"numOver30Minute", // 12
				"numOver60Minute", // 13
				"totalVisitTime", // 14
		};

		Type[] fieldTypes = { StandardBasicTypes.STRING, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.LONG, // 2
				StandardBasicTypes.STRING, // 2'
				StandardBasicTypes.STRING, // 2''
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.INTEGER, // 4
				StandardBasicTypes.LONG, // 5
				StandardBasicTypes.DATE, // 6
				StandardBasicTypes.DATE, // 7
				StandardBasicTypes.INTEGER, // 8
				StandardBasicTypes.INTEGER, // 9
				StandardBasicTypes.INTEGER, // 10
				StandardBasicTypes.INTEGER, // 11
				StandardBasicTypes.INTEGER, // 12
				StandardBasicTypes.INTEGER, // 13
				StandardBasicTypes.FLOAT, // 14
		};
		
		if (kPaging == null)
			return repo.getListByQueryAndScalar(ActionLogVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
		else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(*) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(
					ActionLogVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params,
					kPaging);
		}
	}
	
	@Override
	public List<ActionLogCustomerVO> getActionLogCustomerOfStaff(RptStaffSaleFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ac.action_log_id as id, ");
		sql.append(" ac.customer_id as customerId, ");
		sql.append(" ac.staff_id as staffId, ");
		sql.append(" ac.object_id as objectId, ");
		sql.append(" ac.object_type as objectType, ");
		sql.append(" ac.start_time as startTime, ");
		sql.append(" ac.end_time as endTime ");
		sql.append(" from action_log ac ");
		sql.append(" where 1 = 1 ");
		if (filter.getStaffId() != null) {
			sql.append(" and ac.staff_id = ? ");
			params.add(filter.getStaffId());
		}
		if (filter.getCustomerId() != null) {
			sql.append(" and ac.customer_id = ? ");
			params.add(filter.getCustomerId());
		}
		if (filter.getObjetType() != null) {
			sql.append(" and ac.object_type = ? ");
			params.add(filter.getObjetType());
		}
		if (filter.getLstObjetType() != null && filter.getLstObjetType().size() > 0) {
			sql.append(" and ac.object_type in (-1 ");
			for (int i = 0, sz = filter.getLstObjetType().size(); i < sz; i++) {
				sql.append(", ? ");
				params.add(filter.getLstObjetType().get(i));
			}
			sql.append(" ) ");
		}
		sql.append(" and ac.start_time >= trunc(sysdate) ");
		sql.append(" and ac.start_time < trunc(sysdate) + 1 ");
		// lay thoi gian dang ghe tham luon; nen bo dieu kien end_time
		/*sql.append(" and ac.end_time >= trunc(sysdate) ");
		sql.append(" and ac.end_time < trunc(sysdate) + 1 ");*/
		sql.append(" order by start_time desc ");
		
		String[] fieldNames = { 
				"id",
				"customerId", //2
				"staffId",
				"objectId",  //4
				"objectType",
				"startTime", //6
				"endTime", 
		};
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.TIMESTAMP,// 6 ; de lay HH:MI
				StandardBasicTypes.TIMESTAMP,// 7 ; de lay HH:MI
		};
		return repo.getListByQueryAndScalar(ActionLogCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
}
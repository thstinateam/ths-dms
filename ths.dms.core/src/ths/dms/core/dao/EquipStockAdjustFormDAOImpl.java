package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.EquipStockAdjustForm;
import ths.dms.core.entities.EquipStockAdjustFormDtl;
import ths.dms.core.entities.filter.EquipStockAdjustFormFilter;
import ths.dms.core.entities.vo.EquipStockAdjustFormDtlVO;
import ths.dms.core.entities.vo.EquipStockAdjustFormVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class EquipStockAdjustFormDAOImpl implements EquipStockAdjustFormDAO {
	@Autowired
	private IRepository repo;

	@Override
	public List<EquipStockAdjustFormVO> getListEquipStockAdjustForm(EquipStockAdjustFormFilter filter) throws DataAccessException {
		// TODO Auto-generated method stub
		
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		//Chi hien thi nhung phieu duoc phan quyen
		
		
		/*sql.append(" with p_shop as ( ");
		sql.append(" 	select sh.shop_id from shop sh ");
		sql.append(" 	start with sh.shop_id IN ( ");
		sql.append(" 	select regexp_substr(? , '[^,]+', 1, level) ");
		params.add(filter.getShopId());
		sql.append(" 	from dual connect by regexp_substr(? , '[^,]+', 1, level) is not null ) ");
		params.add(filter.getShopId());
		sql.append(" 	connect by prior sh.shop_id = sh.parent_shop_id ");
		sql.append(" 	group by sh.shop_id ), ");
		sql.append(" r1 as( select ");
		sql.append(" 	esaf.equip_stock_adjust_form_id ");
		sql.append(" 	from equip_stock_adjust_form   esaf ");
		sql.append(" 	join equip_stock_adjust_form_dtl esafdtf on esafdtf.equip_stock_adjust_form_id=esaf.equip_stock_adjust_form_id ");
		sql.append(" 	join equip_stock es on es.equip_stock_id = esafdtf.stock_id ");
		sql.append(" 	where es.shop_id in (select shop_id from p_shop)) ");		
		
		sql.append(" select  ");
		sql.append(" EQUIP_STOCK_ADJUST_FORM_ID as id, ");
		sql.append(" to_char(CREATE_FORM_DATE, 'dd/mm/yyyy')  as createFormDate, ");
		sql.append(" CODE as code, ");
		sql.append(" DESCRIPTION as description, ");
		sql.append(" STATUS as status ");
		sql.append(" from equip_stock_adjust_form  ");
		sql.append(" where 1 = 1 ");
		sql.append(" and equip_stock_adjust_form_id in (select equip_stock_adjust_form_id from r1) ");*/
		
		
		sql.append("WITH rdta ");
		sql.append("     AS (SELECT es.equip_stock_id AS equipStockId, ");
		sql.append("                es.code           AS equipStockCode, ");
		sql.append("                es.name           AS equipStockName, ");
		sql.append("                es.shop_id        AS shopId, ");
		sql.append("                sh.shop_code      AS shopCode, ");
		sql.append("                sh.shop_name      AS shopName, ");
		sql.append("                erd.is_under      AS isUnder ");
		sql.append("         FROM   equip_role_user eru ");
		sql.append("                join equip_role er ");
		sql.append("                  ON eru.equip_role_id = er.equip_role_id ");
		sql.append("                join equip_role_detail erd ");
		sql.append("                  ON er.equip_role_id = erd.equip_role_id ");
		sql.append("                join equip_stock es ");
		sql.append("                  ON erd.equip_stock_id = es.equip_stock_id ");
		sql.append("                join shop sh ");
		sql.append("                  ON es.shop_id = sh.shop_id ");
		sql.append("         WHERE  eru.status = 1 ");
		sql.append("                AND er.status = 1 ");
		sql.append("                AND es.status = 1 ");
		sql.append("                AND eru.user_id = ?), ");
		params.add(filter.getStaffRootId());
		sql.append("     rstockfull ");
		sql.append("     AS (SELECT DISTINCT equipstockid, ");
		sql.append("                         equipstockcode, ");
		sql.append("                         equipstockname, ");
		sql.append("                         shopcode, ");
		sql.append("                         shopname, ");
		sql.append("                         shopid ");
		sql.append("         FROM   ((SELECT es.equip_stock_id AS equipStockId, ");
		sql.append("                         es.code           AS equipStockCode, ");
		sql.append("                         es.name           AS equipStockName, ");
		sql.append("                         es.shop_id        AS shopId, ");
		sql.append("                         sh.shop_code      AS shopCode, ");
		sql.append("                         sh.shop_name      AS shopName, ");
		sql.append("                         1                 AS isUnder ");
		sql.append("                  FROM   (SELECT * ");
		sql.append("                          FROM   shop ");
		sql.append("                          WHERE  1 = 1 ");
		sql.append("                          START WITH shop_id IN (SELECT shopid ");
		sql.append("                                                 FROM   rdta ");
		sql.append("                                                 WHERE  isunder = 1) ");
		sql.append("                                     AND status = 1 ");
		sql.append("                          CONNECT BY PRIOR shop_id = parent_shop_id) sh ");
		sql.append("                         join equip_stock es ");
		sql.append("                           ON sh.shop_id = es.shop_id ");
		sql.append("                  WHERE  es.status = 1 ");
		sql.append("                  MINUS ");
		sql.append("                  (SELECT es.equip_stock_id AS equipStockId, ");
		sql.append("                          es.code           AS equipStockCode, ");
		sql.append("                          es.name           AS equipStockName, ");
		sql.append("                          es.shop_id        AS shopId, ");
		sql.append("                          sh.shop_code      AS shopCode, ");
		sql.append("                          sh.shop_name      AS shopName, ");
		sql.append("                          1                 AS isUnder ");
		sql.append("                   FROM   shop sh ");
		sql.append("                          join equip_stock es ");
		sql.append("                            ON sh.shop_id = es.shop_id ");
		sql.append("                   WHERE  1 = 1 ");
		sql.append("                          AND sh.shop_id IN (SELECT shopid ");
		sql.append("                                             FROM   rdta ");
		sql.append("                                             WHERE  isunder = 1) ");
		sql.append("                          AND es.equip_stock_id NOT IN (SELECT equipstockid ");
		sql.append("                                                        FROM   rdta))) ");
		sql.append("                 UNION ");
		sql.append("                 (SELECT * ");
		sql.append("                  FROM   rdta ");
		sql.append("                  WHERE  isunder = 0)) ");
		sql.append("         ORDER  BY equipstockcode) ");
		
		
		
		sql.append(" select  ");
		sql.append(" distinct esaf.EQUIP_STOCK_ADJUST_FORM_ID as id, ");
		sql.append(" to_char(esaf.CREATE_FORM_DATE, 'dd/mm/yyyy')  as createFormDate, ");
		sql.append(" esaf.CODE as code, ");
		sql.append(" esaf.note as note, ");
		sql.append(" esaf.DESCRIPTION as description, ");
		sql.append(" esaf.STATUS as status ");
		sql.append(" from equip_stock_adjust_form esaf ");
		sql.append(" join equip_stock_adjust_form_dtl esafdtl ");
		sql.append(" on esaf.equip_stock_adjust_form_id = esafdtl.equip_stock_adjust_form_id ");
		sql.append(" where esafdtl.stock_id in ( SELECT s.equipStockId ");
		sql.append(" FROM rstockfull s ) ");
	
		
		//Voi quyen duyet thi chi hien thi trang thai duyet va cho duyet 
		if(filter.getIsCreate()!=null && filter.getIsCreate()==0){
			sql.append(" and esaf.status in (1,2) ");
		}
		
		if (filter.getId()!=null) {
			sql.append(" and esaf.EQUIP_STOCK_ADJUST_FORM_ID = ? ");
			params.add(filter.getId());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(esaf.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
		}
		if (filter.getStatus() != null) {
			sql.append(" and esaf.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and esaf.status != -1 ");
		}
		
		if (filter.getFromDate() != null) {
			sql.append("and esaf.CREATE_FORM_DATE >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append("and esaf.CREATE_FORM_DATE < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		sql.append(" order by esaf.EQUIP_STOCK_ADJUST_FORM_ID desc ");
		
		countSql.append("  select count(*) as count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		
		final String[] fieldNames = new String[] { 
				"id", 
				"createFormDate", 
				"code",
				"note",
				"description",
				"status"
				};
		
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER
				};
		
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipStockAdjustFormVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(EquipStockAdjustFormVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
		
	}
	

	@Override
	public List<EquipStockAdjustForm> getListEquipStockAdjustFormByListId(List<Long> listId) throws DataAccessException {
		// TODO Auto-generated method stub
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from EQUIP_STOCK_ADJUST_FORM  ");
		sql.append(" where 1=1 ");
		if (listId != null && listId.size()>0) {
			sql.append(" and EQUIP_STOCK_ADJUST_FORM_ID in (-1 ");
			for (Long id : listId) {
				if (id != null) {
					sql.append(" ,?");
					params.add(id);
				}
			}
		}
		sql.append(" ) ");
		return repo.getListBySQL(EquipStockAdjustForm.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipStockAdjustFormDtlVO> getEquipStockAdjustFormDtlFilter(EquipStockAdjustFormFilter filter) throws DataAccessException {
		
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		
		/*sql.append(" with p_shop as ( ");
		sql.append(" 	select sh.shop_id from shop sh ");
		sql.append(" 	start with sh.shop_id IN ( ");
		sql.append(" 	select regexp_substr(? , '[^,]+', 1, level) ");
		params.add(filter.getShopId());
		sql.append(" 	from dual connect by regexp_substr(? , '[^,]+', 1, level) is not null ) ");
		params.add(filter.getShopId());
		sql.append(" 	connect by prior sh.shop_id = sh.parent_shop_id ");
		sql.append(" 	group by sh.shop_id )");*/
		
		sql.append(" select  ");
		sql.append(" esaf.code as code, ");
		sql.append(" esaf.note as note, ");
		sql.append(" esafdtl.equip_stock_adjust_form_dtl_id as id, ");
		sql.append(" esafdtl.EQUIP_ID as equipId, ");
		sql.append(" (select eq.code from equipment eq where eq.EQUIP_ID = esafdtl.EQUIP_ID) as equipCode, ");
		sql.append(" esafdtl.SERIAL as serial, ");
		sql.append(" (select apr.value from ap_param apr where apr.ap_param_code = esafdtl.health_status and apr.type LIKE 'EQUIP_CONDITION' and apr.status = 1) healthStatusStr, ");
		sql.append(" esafdtl.health_status as healthStatus, ");
		sql.append(" esafdtl.EQUIP_GROUP_ID as equipGroupId, ");
		sql.append(" (select eqg.name from equip_group eqg where eqg.equip_group_id = esafdtl.equip_group_id) as equipGroupName, ");
		sql.append(" (select eqg.code from equip_group eqg where eqg.equip_group_id = esafdtl.equip_group_id) as equipGroupCode, ");
		sql.append(" (select eqc.NAME from EQUIP_GROUP eqg join EQUIP_CATEGORY eqc on eqg.EQUIP_CATEGORY_ID=eqc.EQUIP_CATEGORY_ID where eqg.EQUIP_GROUP_ID=esafdtl.EQUIP_GROUP_ID) as equipCateGogyName, ");
		sql.append("(case when esafdtl.capacity_from is null and esafdtl.capacity_to is null then '' ");
		sql.append(" when esafdtl.capacity_from is null then '<= '|| to_char(esafdtl.capacity_to) ");
		sql.append(" when esafdtl.capacity_to is null then '>= '|| to_char(esafdtl.capacity_from)  ");
		sql.append(" else to_char(esafdtl.capacity_from) ||' - '|| to_char(esafdtl.capacity_to) end) capacity, ");
		sql.append(" (select eqg.BRAND_NAME from equip_group eqg where eqg.equip_group_id = esafdtl.equip_group_id) as equipBrandName,  ");
		sql.append(" esafdtl.EQUIP_PROVIDER_ID equipProviderId,  ");
		sql.append(" esafdtl.EQUIP_PROVIDER_NAME equipProviderName,  ");
		sql.append(" ( select ep.code from EQUIP_PROVIDER ep where ep.EQUIP_PROVIDER_ID = esafdtl.EQUIP_PROVIDER_ID) as equipProviderCode,  ");
		sql.append(" esafdtl.MANUFACTURING_YEAR as manufacturingYear,  ");
		sql.append(" to_char(esafdtl.WARRANTY_EXPIRED_DATE, 'dd/mm/yyyy') as warrantyExpiredDate,  ");
		sql.append(" esafdtl.PRICE as price,  ");
		sql.append(" esafdtl.STOCK_ID as stockId,  ");
		sql.append(" esafdtl.STOCK_TYPE as stockType,  ");
		sql.append(" esafdtl.STOCK_NAME as stockName, ");
		sql.append(" (select et.code from equip_stock  et where et.equip_stock_id = esafdtl.STOCK_ID ) as stockCode ");
		sql.append(" from equip_stock_adjust_form_dtl esafdtl ");
		sql.append(" join equip_stock es on es.equip_stock_id = esafdtl.stock_id ");
		sql.append(" join equip_stock_adjust_form esaf on esaf.equip_stock_adjust_form_id = esafdtl.equip_stock_adjust_form_id ");
		sql.append(" where 1 = 1 ");
		//sql.append(" and es.shop_id in (select shop_id from p_shop) ");	
		if (filter.getId()!= null) {
			sql.append(" and esafdtl.equip_stock_adjust_form_id = ? ");
			params.add(filter.getId());
		}
		
		if (filter.getLstId() != null && filter.getLstId().size()>0) {
			sql.append(" and esaf.EQUIP_STOCK_ADJUST_FORM_ID in (-1 ");
			for (Long id : filter.getLstId()) {
				if (id != null) {
					sql.append(" ,?");
					params.add(id);
				}
			}
			sql.append(" ) ");
		}
		
		//Voi quyen duyet thi chi hien thi trang thai duyet va cho duyet 
		if(filter.getIsCreate()!=null && filter.getIsCreate()==0){
			sql.append(" and esaf.status in (1,2) ");
		}
				
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(esaf.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
		}
		if (filter.getStatus() != null) {
			sql.append(" and esaf.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and esaf.status != -1 ");
		}
		
		if (filter.getFromDate() != null) {
			sql.append("and esaf.CREATE_FORM_DATE >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append("and esaf.CREATE_FORM_DATE < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		sql.append(" order by esaf.CREATE_FORM_DATE desc ");
		
		countSql.append("  select count(*) as count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		
		final String[] fieldNames = new String[] {
				"code",//1
				"note",
				"id", //2
				"equipId",//3 
				"equipCode",//4
				"serial",//5
				"healthStatus",//6
				"healthStatusStr",//7
				"equipGroupId",//8
				"equipGroupName",//9
				"equipGroupCode",//10
				"equipCateGogyName",//11
				"capacity",//12
				"equipBrandName",//13
				"equipProviderId",//14
				"equipProviderName",//15
				"equipProviderCode",//16
				"manufacturingYear",//17
				"warrantyExpiredDate",//18
				"price",//19
				"stockId",//20
				"stockType",//21
				"stockName",//22
				"stockCode"//23
				};
		
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.STRING,//1
				StandardBasicTypes.STRING,
				StandardBasicTypes.LONG, //2
				StandardBasicTypes.LONG, //3
				StandardBasicTypes.STRING,//4
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.STRING,//6
				StandardBasicTypes.STRING,//7
				StandardBasicTypes.LONG,//8
				StandardBasicTypes.STRING,//9
				StandardBasicTypes.STRING,//10
				StandardBasicTypes.STRING,//11
				StandardBasicTypes.STRING,//12
				StandardBasicTypes.STRING,//13
				StandardBasicTypes.LONG,//14
				StandardBasicTypes.STRING,//15
				StandardBasicTypes.STRING,//16
				StandardBasicTypes.INTEGER,//17
				StandardBasicTypes.STRING,//18
				StandardBasicTypes.BIG_DECIMAL,//19
				StandardBasicTypes.LONG,//20
				StandardBasicTypes.STRING,//21
				StandardBasicTypes.STRING,//22
				StandardBasicTypes.STRING//23
				};
		
		if (filter.getkPagingDtl() == null) {
			return repo.getListByQueryAndScalar(EquipStockAdjustFormDtlVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(EquipStockAdjustFormDtlVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingDtl());
		}
		
	}

	@Override
	public EquipStockAdjustForm getEquipStockAdjustFormByID(
			Long id) throws DataAccessException {
		return repo.getEntityById(EquipStockAdjustForm.class, id);
	}

	
	
	@Override
	public List<EquipStockAdjustFormDtl> getListEquipStockAdjustFormDetailById(Long id) throws DataAccessException{
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from EQUIP_STOCK_ADJUST_FORM_DTL  ");
		sql.append(" where 1=1 ");
		if (id != null) {
			sql.append(" and EQUIP_STOCK_ADJUST_FORM_ID = ? ");
			params.add(id);
		}
		
		return repo.getListBySQL(EquipStockAdjustFormDtl.class, sql.toString(), params);
	}


	@Override
	public EquipStockAdjustFormDtl getEquipStockAdjustFormDetailByID(Long equipStockAdjustFormDetailId) throws DataAccessException {
		// TODO Auto-generated method stub
		return repo.getEntityById(EquipStockAdjustFormDtl.class, equipStockAdjustFormDetailId);
	}

	
	
}

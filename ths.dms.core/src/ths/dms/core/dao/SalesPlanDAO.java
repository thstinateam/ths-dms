package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.SalesPlan;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

public interface SalesPlanDAO {

	SalesPlan createSalesPlan(SalesPlan salesPlan) throws DataAccessException;

	void deleteSalesPlan(SalesPlan salesPlan) throws DataAccessException;

	void updateSalesPlan(SalesPlan salesPlan) throws DataAccessException;
	
	SalesPlan getSalesPlanById(long id) throws DataAccessException;
	
	/**
	 * 
	*  lay danh sach sale plan phan quan ly ke hoach tieu thu cua nvbh (phan ket hop tablet)
	*  @author: thanhnn
	*  @param kPaging
	*  @param shopId
	*  @param staffId
	*  @param productId
	*  @param date
	*  @return
	*  @throws DataAccessException
	*  @return: List<SalesPlan>
	*  @throws:
	 */
	List<SalesPlan> getListSalePlanForManagerPlan(KPaging<SalesPlan> kPaging,
			Long shopId, Long staffId, Long productId, Date date)
			throws DataAccessException;
	
	/**
	 * 
	*  lay danh sach sale plan phan quan ly ke hoach tieu thu cua nvbh (phan ket hop tablet)
	*  @author: thanhnn
	*  @param kPaging
	*  @param shopId
	*  @param staffId
	*  @param productCode
	*  @param date
	*  @return
	*  @throws DataAccessException
	*  @return: List<SalesPlan>
	*  @throws:
	 */
	List<SalesPlan> getListSalePlanForManagerPlanSearchLike(KPaging<SalesPlan> kPaging,
			Long shopId, Long staffId, String productCode, Date date)
					throws DataAccessException;

}

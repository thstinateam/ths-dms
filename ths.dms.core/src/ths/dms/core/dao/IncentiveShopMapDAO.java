package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.IncentiveShopMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.IncentiveShopMapFilter;
import ths.dms.core.entities.enumtype.IncentiveShopMapVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.entities.vo.QuantityVO;
import ths.dms.core.exceptions.DataAccessException;

public interface IncentiveShopMapDAO {

	IncentiveShopMap createIncentiveShopMap(IncentiveShopMap incentiveShopMap, LogInfoVO logInfo) throws DataAccessException;

	void deleteIncentiveShopMap(IncentiveShopMap incentiveShopMap, LogInfoVO logInfo) throws DataAccessException;

	void updateIncentiveShopMap(IncentiveShopMap incentiveShopMap, LogInfoVO logInfo) throws DataAccessException;
	
	IncentiveShopMap getIncentiveShopMapById(long id) throws DataAccessException;

	List<IncentiveShopMap> getListIncentiveShopMap(
			Long incentiveProgramId, ActiveType status,
			KPaging<IncentiveShopMap> kPaging)
			throws DataAccessException;
	
	List<IncentiveShopMapVO> getListIncentiveShopMapVO(IncentiveShopMapFilter filter,
			KPaging<IncentiveShopMapVO> kPaging)
			throws DataAccessException;
	
	List<Shop> getListShopNotJoinIncentiveProgram(
			Long incentiveProgramId, 
			KPaging<Shop> kPaging)
			throws DataAccessException;
	
	Boolean isExistShop(Long incentiveProgramId, Long shopId)
			throws DataAccessException;
	
	List<IncentiveShopMap> getListAncestorIncentiveShopMap(
			Long incentiveProgramId, Long shopId)
			throws DataAccessException;

	Boolean checkIfAncestorInIp(Long shopId, Long incentiveProgramId)
			throws DataAccessException;

	List<Shop> getListShopInIncentiveProgram(Long incentiveProgramId, Long shopId,
			String shopCode, String shopName, Boolean isJoin)
			throws DataAccessException;

	List<ProgrameExtentVO> getListProgrameExtentVOEx(List<Shop> lstShop,
			Long incentiveProgramId) throws DataAccessException;

	List<IncentiveShopMap> getListIncentiveChildShopMapWithShopAndIncentiveProgram(
			Long shopId, Long incentiveProgramId) throws DataAccessException;

	List<QuantityVO> getListQuantityVOEx(List<Shop> lstShop,
			Long incentiveProgramId) throws DataAccessException;

	Boolean isExistChildJoinProgram(String shopCode, Long incentiveProgramId)
			throws DataAccessException;

	Boolean checkExistChildIncentiveShopMap(Long shopId,Long incentiveProgramId, Boolean exceptShopCheck)throws DataAccessException;
}

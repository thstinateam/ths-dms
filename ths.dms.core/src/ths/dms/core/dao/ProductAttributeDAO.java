/**
 * 
 */
package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.ProductAttribute;
import ths.dms.core.entities.ProductAttributeDetail;
import ths.dms.core.entities.ProductAttributeEnum;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumFilter;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.ProductAttributeFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProductAttributeDetailVO;
import ths.dms.core.entities.vo.ProductAttributeEnumVO;
import ths.dms.core.entities.vo.ProductAttributeVO;
import ths.dms.core.exceptions.DataAccessException;

// TODO: Auto-generated Javadoc
/**
 * Quan ly truy cap du lieu PRODUCT_ATTRIBUTE...
 * @author tientv11
 * @since 16/01/2015
 *
 */
public interface ProductAttributeDAO {
	
	/**
	 * Danh sach theo dieu kien tim kiem ProductAttribute.
	 *
	 * @param filter the filter
	 * @return the list product attribute vo filter
	 * @throws DataAccessException the data access exception
	 * @author tientv11
	 */
	List<ProductAttributeVO> getListProductAttributeVOFilter(ProductAttributeFilter filter)
		throws DataAccessException;
	
	
	
	/**
	 * Danh sach theo dieu kien tim kiem ProductAttributeEnum.
	 *
	 * @param filter the filter
	 * @return the list product attribute enum vo filter
	 * @throws DataAccessException the data access exception
	 * @author tientv11
	 */
	List<ProductAttributeEnumVO> getListProductAttributeEnumVOFilter(ProductAttributeFilter filter)
		throws DataAccessException;
	
	
	/**
	 * Danh sach theo dieu kien tim kiem ProductAttributeDetail.
	 *
	 * @param filter the filter
	 * @return the list product attribute detail vo filter
	 * @throws DataAccessException the data access exception
	 * @author tientv11
	 */
	List<ProductAttributeDetailVO> getListProductAttributeDetailVOFilter(ProductAttributeFilter filter)
		throws DataAccessException;
	
	/**
	 * Thong tin ProductAttributeDetail theo dieu kien.
	 *
	 * @param productAtt the product att
	 * @param productId the product id
	 * @param productAttEnum the product att enum
	 * @return the product attribute detail by filter
	 * @throws DataAccessException the data access exception
	 * @author tientv11
	 */
	ProductAttributeDetail getProductAttributeDetailByFilter(Long productAtt, Long productId,Long productAttEnum) throws DataAccessException;
	
	/**
	 * Thong tin ProductAttributeDetail theo dieu kien.
	 *
	 * @param productAtt the product att
	 * @param productId the product id
	 * @return the product attribute detail by filter
	 * @throws DataAccessException the data access exception
	 * @author tientv11
	 */
	List<ProductAttributeDetail> getProductAttributeDetailByFilter(Long productAtt, Long productId) throws DataAccessException;

	/**
	 * Danh sach thuoc tinh dong theo product.
	 *
	 * @param filter the filter
	 * @param kPaging the k paging
	 * @return list attribute dynamic vo
	 * @throws DataAccessException the data access exception
	 * @author liemtpt
	 */
	List<AttributeDynamicVO> getListProductAttributeVO(
			AttributeDynamicFilter filter, KPaging<AttributeDynamicVO> kPaging)
			throws DataAccessException;

	/**
	 * **
	 * Danh sach gia tri thuoc tinh khi chon mot va chon nhieu.
	 *
	 * @param filter the filter
	 * @param kPaging the k paging
	 * @return list product attribute enum vo
	 * @throws DataAccessException the data access exception
	 * @author liemtpt
	 */
	List<AttributeEnumVO> getListProductAttributeEnumVO(
			AttributeEnumFilter filter, KPaging<AttributeEnumVO> kPaging)
			throws DataAccessException;



	/**
	 * Gets the product attribute by code.
	 *
	 * @param code the code
	 * @return the product attribute by code
	 * @throws DataAccessException the data access exception
	 */
	ProductAttribute getProductAttributeByCode(String code)
			throws DataAccessException;



	/**
	 * Update product attribute.
	 *
	 * @param productAttribute the product attribute
	 * @param logInfo the log info
	 * @throws DataAccessException the data access exception
	 */
	void updateProductAttribute(ProductAttribute productAttribute,
			LogInfoVO logInfo) throws DataAccessException;



	/**
	 * Gets the product attribute by id.
	 *
	 * @param id the id
	 * @return the product attribute by id
	 * @throws DataAccessException the data access exception
	 */
	ProductAttribute getProductAttributeById(long id)
			throws DataAccessException;



	/**
	 * Creates the product attribute.
	 *
	 * @param productAttribute the product attribute
	 * @param logInfo the log info
	 * @return the product attribute
	 * @throws DataAccessException the data access exception
	 */
	ProductAttribute createProductAttribute(ProductAttribute productAttribute,
			LogInfoVO logInfo) throws DataAccessException;



	/**
	 * Gets the product attribute enum by code.
	 *
	 * @param code the code
	 * @return the product attribute enum by code
	 * @throws DataAccessException the data access exception
	 */
	ProductAttributeEnum getProductAttributeEnumByCode(String code)
			throws DataAccessException;



	/**
	 * Creates the product attribute enum.
	 *
	 * @param staffAttributeEnum the staff attribute enum
	 * @param logInfo the log info
	 * @return the product attribute enum
	 * @throws DataAccessException the data access exception
	 */
	ProductAttributeEnum createProductAttributeEnum(ProductAttributeEnum staffAttributeEnum, LogInfoVO logInfo)
			throws DataAccessException;



	/**
	 * Update product attribute enum.
	 *
	 * @param productAttributeEnum the product attribute enum
	 * @param logInfo the log info
	 * @throws DataAccessException the data access exception
	 */
	void updateProductAttributeEnum(ProductAttributeEnum productAttributeEnum,
			LogInfoVO logInfo) throws DataAccessException;



	/**
	 * Gets the product attribute enum by id.
	 *
	 * @param id the id
	 * @return the product attribute enum by id
	 * @throws DataAccessException the data access exception
	 */
	ProductAttributeEnum getProductAttributeEnumById(long id)
			throws DataAccessException;



	/**
	 * Gets the list product attribute detail by enum id.
	 *
	 * @param id the id
	 * @return the list product attribute detail by enum id
	 * @throws DataAccessException the data access exception
	 */
	List<ProductAttributeDetail> getListProductAttributeDetailByEnumId(Long id)
			throws DataAccessException;



	/**
	 * Update product attribute detail.
	 *
	 * @param productAttributeDetail the product attribute detail
	 * @param logInfo the log info
	 * @throws DataAccessException the data access exception
	 */
	void updateProductAttributeDetail(
			ProductAttributeDetail productAttributeDetail, LogInfoVO logInfo)
			throws DataAccessException;

	
}

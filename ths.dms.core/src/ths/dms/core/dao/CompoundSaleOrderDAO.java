package ths.dms.core.dao;

import ths.dms.core.entities.CompoundSaleOrder;


import ths.dms.core.exceptions.DataAccessException;

public interface CompoundSaleOrderDAO {

	CompoundSaleOrder createCompoundSaleOrder(CompoundSaleOrder cso)
			throws DataAccessException;

	void deleteCompoundSaleOrder(CompoundSaleOrder cso)
			throws DataAccessException;

	CompoundSaleOrder getCompoundSaleOrderById(long id)
			throws DataAccessException;

	void updateCompoundSaleOrder(CompoundSaleOrder cso)
			throws DataAccessException;


}

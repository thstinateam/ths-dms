package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ShopLock;
import ths.dms.core.entities.ShopLockReleasedLog;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.ShopLockLogStatus;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.StaffFilter;
import ths.dms.core.entities.vo.ShopLockLogVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class ShopLockDAOImpl implements ShopLockDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	CommonDAO commonDAO;

	/**
	 * @author hoanv25
	 */
	@Override
	public List<StaffVO> getListStockVOFilter(StaffFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select st.staff_id staffId, st.staff_code staffCode, st.staff_Name staffName,");
		sql.append(" NVL(sl.van_lock,1) vanLock, sl.update_date lockDate");
		sql.append(" from staff st");
		sql.append(" left join stock_lock sl");
		sql.append(" on (st.shop_id=sl.shop_id and st.staff_id=sl.staff_id)");
		sql.append(" where st.shop_id=?");
		params.add(filter.getShopId());
		sql.append(" and st.status=1");
		//longnh15 add: check phan quyen
		if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "st.staff_id");
			sql.append(paramsFix);
		}
		//end
		sql.append(" and st.staff_type_id IN");
		sql.append(" (Select staff_type_id from staff_type ");
		sql.append(" where staff_type_id=st.staff_type_id");
		sql.append(" and specific_type=1 and status=1)");
		sql.append(" Order by st.staff_code");

		String[] fieldNames = { "staffId", "staffCode", "staffName", "vanLock", "lockDate" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (filter.getkPagingStockVO() != null) {
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingStockVO());
		} else {
			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public ShopLock createShopLock(ShopLock shopLock) throws DataAccessException {
		if (shopLock == null) {
			throw new IllegalArgumentException("shopLock");
		}
		return repo.create(shopLock);
	}

	@Override
	public boolean isShopLocked(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select count(1) as count from shop_lock ");
		sql.append("where shop_id = ? ");
		params.add(shopId);

		sql.append("AND lock_date >= TRUNC(?) ");
		params.add(commonDAO.getSysDate());

		sql.append("AND lock_date  < TRUNC(?) + 1 and shop_lock = 1");
		params.add(commonDAO.getSysDate());

		int xx = repo.countBySQL(sql.toString(), params);
		if (xx == 0) {
			return false;
		}
		return true;
	}

	@Override
	public boolean isShopLockedByCode(String shopCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select count(1) as count from shop_lock ");
		sql.append("where shop_id = (select shop_id from shop where shop_code = ?) ");
		params.add(shopCode);

		sql.append("AND lock_date >= TRUNC(?) ");
		params.add(commonDAO.getSysDate());

		sql.append("AND lock_date  < TRUNC(?) + 1 and shop_lock = 1");
		params.add(commonDAO.getSysDate());

		int xx = repo.countBySQL(sql.toString(), params);
		if (xx == 0) {
			return false;
		}
		return true;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public Date getLockedDay(Long shopId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("invalid id");
		}
		String sql = "select max(lock_date) from shop_lock where shop_id=? and shop_lock = 1";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);
		Object date = repo.getObjectByQuery(sql, params);
		if (date instanceof Date) {
			return (Date) date;
		}
		return null;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public Date getApplicationDate(Long shopId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("invalid id");
		}
		String sql = "select max(lock_date) from shop_lock where shop_id=? and shop_lock = 1";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);
		Object date = repo.getObjectByQuery(sql, params);
		if (date instanceof Date) {
			Date d = DateUtility.addDate((Date) date, 1);

			Date sysDate = commonDAO.getSysDate();
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(sysDate);

			Calendar cal = Calendar.getInstance();
			cal.setTime(d);

			cal1.set(Calendar.DATE, cal.get(Calendar.DATE));
			cal1.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
			cal1.set(Calendar.DAY_OF_WEEK_IN_MONTH, cal.get(Calendar.DAY_OF_WEEK_IN_MONTH));
			cal1.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR));
			cal1.set(Calendar.MONTH, cal.get(Calendar.MONTH));
			cal1.set(Calendar.YEAR, cal.get(Calendar.YEAR));

			d = cal1.getTime();
			return d;
		}
		return commonDAO.getSysDate();
	}

	@Override
	public Date getNextLockedDay(Long shopId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("invalid id");
		}
		String sql = "select max(lock_date) + 1 as maxLockDate from shop_lock where shop_id=? and shop_lock = 1";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);
		Object date = repo.getObjectByQuery(sql, params);
		if (date instanceof Date) {
			Date lockDate = (Date) date;

			Date sysDate = commonDAO.getSysDate();
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(sysDate);

			Calendar cal = Calendar.getInstance();
			cal.setTime(lockDate);

			cal1.set(Calendar.DATE, cal.get(Calendar.DATE));
			cal1.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
			cal1.set(Calendar.DAY_OF_WEEK_IN_MONTH, cal.get(Calendar.DAY_OF_WEEK_IN_MONTH));
			cal1.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR));
			cal1.set(Calendar.MONTH, cal.get(Calendar.MONTH));
			cal1.set(Calendar.YEAR, cal.get(Calendar.YEAR));

			lockDate = cal1.getTime();
			return lockDate;
		}
		return commonDAO.getSysDate();
	}

	@Override
	public void updateShopLock(ShopLock shopLock, Long shopId) throws DataAccessException {
		if (shopLock == null) {
			throw new IllegalArgumentException("shopLock");
		}

		String sql = "delete shop_lock where shop_id = ? and shop_lock = 1";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);
		repo.executeSQLQuery(sql, params);
		repo.create(shopLock);
	}

	@Override
	public boolean isStockTransLocked(Long shopId, Date lockDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select count(1) as count from shop_lock ");
		sql.append("where shop_id = ? ");
		params.add(shopId);

		sql.append("AND lock_date >= TRUNC(?) ");
		params.add(lockDate);

		sql.append("AND lock_date  < TRUNC(?) + 1 and staff_lock = 1 and shop_lock = 0");
		params.add(lockDate);

		int xx = repo.countBySQL(sql.toString(), params);
		if (xx == 0) {
			return true;
		}
		return false;
	}

	@Override
	public ShopLock getShopLockVanSale(Long shopId, Date lockDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from shop_lock ");
		sql.append("where shop_id = ? ");
		params.add(shopId);

		sql.append("AND lock_date >= TRUNC(?) ");
		params.add(lockDate);

		sql.append("AND lock_date  < TRUNC(?) + 1 and staff_lock = 1 and shop_lock = 0");
		params.add(lockDate);

		return repo.getEntityBySQL(ShopLock.class, sql.toString(), params);
	}

	@Override
	public void updateShopLockVansale(ShopLock shopLock) throws DataAccessException {
		repo.update(shopLock);
	}

	/**
	 * @author phuongvm
	 */
	@Override
	public List<ShopLock> getListShopLock(Long shopId, Date fromDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from shop_lock ");
		sql.append("where shop_id = ? ");
		params.add(shopId);

		sql.append("AND lock_date >= TRUNC(?) ");
		params.add(fromDate);

		sql.append("AND lock_date  < TRUNC(sysdate) + 1 ");
		sql.append(" order by lock_date ");

		return repo.getListBySQL(ShopLock.class, sql.toString(), params);
	}

	@Override
	public void deleteShopLock(List<ShopLock> lst) throws DataAccessException {
		if (lst.isEmpty()) {
			throw new IllegalArgumentException("shopLock");
		}
		for (ShopLock s : lst) {
			repo.delete(s);
		}
	}

	@Override
	public ShopLock getShopLockShopAndLockDate(Long shopId, Date lockDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from shop_lock ");
		sql.append("where shop_id = ? ");
		params.add(shopId);

		sql.append("and lock_date >= trunc(?) ");
		sql.append("and lock_date < trunc(?) + 1 ");
		params.add(lockDate);
		params.add(lockDate);

		return repo.getEntityBySQL(ShopLock.class, sql.toString(), params);
	}
	
	@Override
	public ShopLockReleasedLog getShopLockReleaseLogShopAndReleaseDate(Long shopId, Date releaseDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from shop_lock_released_log ");
		sql.append("where shop_id = ? ");
		params.add(shopId);

		sql.append("and released_date >= trunc(?) ");
		sql.append("and released_date < trunc(?) + 1 ");
		params.add(releaseDate);
		params.add(releaseDate);

		return repo.getEntityBySQL(ShopLockReleasedLog.class, sql.toString(), params);
	}
	
	@Override
	public Date getMinLockDate(Long shopId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("invalid id");
		}
		String sql = "select min(lock_date) from shop_lock where shop_id=? and shop_lock = 1";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);
		Object date = repo.getObjectByQuery(sql, params);
		if (date instanceof Date) {
			return (Date) date;
		}
		return null;
	}

	@Override
	public List<ShopVO> getListChildShopForCloseDay(ShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShop as ( ");
		sql.append("     select shop_id from shop where status = 1 start with shop_id in (-1");
		if (filter.getLstParentId() != null) {
			for (Long id : filter.getLstParentId()) {
				sql.append(",?");
				params.add(id);
			}
		}
		sql.append("		) connect by prior shop_id = parent_shop_id and status=1 ");
		sql.append(" ) ");
		sql.append(" select ");
		sql.append(" r1.shopId ");
		sql.append(" , r1.shopCode ");
		sql.append(" , r1.shopName ");
		sql.append(" , (case when r1.lockDate is not null then TO_CHAR(r1.lockDate + 1,'dd/MM/yyyy') else TO_CHAR(sysdate,'dd/MM/yyyy') end) lockDateStr ");
		sql.append(" , TO_CHAR(NVL(r1.lockDate + 2, sysdate + 1),'dd/MM/yyyy') nextLockDateStr ");
		sql.append(" , decode(sll.ERROR_TYPE, 1, 'Lỗi tiến trình', 2, 'Tồn kho không đủ chưa xử lý hết đơn', 3, 'Đơn vị đang kiểm kê, có đơn không cập nhật kho được', 4, 'Có đơn chưa cập nhật kho thành công', 5, 'Có đơn chưa duyệt không hủy được', 6, 'Có đơn Điều chỉnh/ Điều chuyển chưa duyệt', '') as errorTypeStr ");
		sql.append(" , decode(sll.status, 1, 'Đang xử lý', 2, 'Đang đợi xử lý', 3, 'Đã xử lý', 4, 'Lỗi ('||to_char(NVL(sll.quantity, 0) - NVL(sll.quantity_success, 0))||' đơn chưa xử lý được)') as statusLogStr ");
		sql.append(" from ( ");
		sql.append(" SELECT shop_id shopId ");
		sql.append("   , shop_code shopCode ");
		sql.append("   , shop_name shopName ");
		sql.append("   , (SELECT MAX(lock_date) FROM shop_lock WHERE shop_id =s.shop_id AND shop_lock = 1) as lockDate ");
		sql.append(" from shop s join shop_type st on st.shop_type_id = s.shop_type_id ");
		if (StringUtility.isNullOrEmpty(filter.getStrShopId()) && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s.shop_id = ogr.number_key_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		sql.append(" where 1=1 and s.status=1 ");
		sql.append(" and st.specific_type=1 ");
		sql.append(" and s.shop_id in (select shop_id from lstShop) ");
		
		if (!StringUtility.isNullOrEmpty(filter.getStrShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrShopId(), "s.shop_id");
			sql.append(paramsFix);
		}
		
		sql.append(" ) r1  ");
		sql.append(" left join ( ");

		sql.append(" select * from ( ");
		sql.append(" 		SELECT shop_id, WORKING_DATE, error_type, quantity_success, quantity, status, dense_rank() over (partition by shop_id, WORKING_DATE order by shop_id, WORKING_DATE, status) as gr ");
		sql.append(" 		FROM (SELECT s1.shop_id, s1.WORKING_DATE, status, NULL error_type, NULL quantity_success, NULL quantity, MAX(create_date) AS create_date FROM SHOP_LOCK_LOG s1 ");
		sql.append(" 		WHERE s1.STATUS IN(?, ?) ");
		params.add(ShopLockLogStatus.RUNNING.getValue());
		params.add(ShopLockLogStatus.PENDING.getValue());
		sql.append(" 		GROUP BY s1.shop_id, s1.WORKING_DATE, status ");
		sql.append(" UNION ALL ");
		sql.append(" SELECT shop_id, WORKING_DATE, status, error_type, quantity_success, quantity, create_date ");
		sql.append(" FROM ");
		sql.append("   (SELECT s1.shop_id, s1.WORKING_DATE, s1.status, s1.error_type, s1.quantity_success, s1.quantity, s1.create_date , ");
		sql.append("     dense_rank() over (partition BY s1.shop_id, s1.WORKING_DATE, status order by s1.shop_id, s1.WORKING_DATE, s1.status, s1.create_date DESC) AS gr ");
		sql.append("   FROM SHOP_LOCK_LOG s1 WHERE s1.STATUS = ? and s1.passed <> ? ");
		params.add(ShopLockLogStatus.ERROR.getValue());
		params.add(StatusType.ACTIVE.getValue());
		sql.append("   ) WHERE gr = 1 ");
		sql.append(" )) WHERE gr = 1) sll on r1.shopId = sll.SHOP_ID and r1.lockDate is not null and trunc(r1.lockDate) + 1 = trunc(sll.working_date) ");

		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "lockDateStr", "nextLockDateStr", "errorTypeStr", "statusLogStr" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getkPagingVO() == null) {
			//Sap xep theo dieu kien
			String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
			if (!StringUtility.isNullOrEmpty(sort)) {
				sql.append(" order by ").append(sort).append(" ");
				String order = StringUtility.getOrderBy(filter.getOrder());
				if (!StringUtility.isNullOrEmpty(order)) {
					sql.append(order);
				}
			} else {
				sql.append(" order by lockDate, shopCode ");
			}
			return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			//Dem de phan trang
			countSql.append(" select count(1) count from (").append(sql.toString()).append(")");
			//Sap xep theo dieu kien
			String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
			if (!StringUtility.isNullOrEmpty(sort)) {
				sql.append(" order by ").append(sort).append(" ");
				String order = StringUtility.getOrderBy(filter.getOrder());
				if (!StringUtility.isNullOrEmpty(order)) {
					sql.append(order);
				}
			} else {
				sql.append(" order by lockDate, shopCode ");
			}
			return repo.getListByQueryAndScalarPaginated(ShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVO());
		}
	}

	/**
	 * @author hunglm16
	 */
	@Override
	public List<ShopLockLogVO> getShopLockLogVOByShopAndLockDate(Long shopId, Date lockDate) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (lockDate == null) {
			throw new IllegalArgumentException("lockDate is null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select s1.shop_id as shopId, to_char(s1.WORKING_DATE, 'dd/MM/yyyy') as lockDate, status,  to_char(max(create_date), 'dd/MM/yyyy') as createDate ");
		sql.append("from SHOP_LOCK_LOG s1 ");
		sql.append("where s1.STATUS in(?, ?) ");
		params.add(ShopLockLogStatus.RUNNING.getValue());
		params.add(ShopLockLogStatus.PENDING.getValue());
		sql.append("and shop_id = ? ");
		params.add(shopId);
		sql.append("and trunc(working_date) = trunc(?) ");
		params.add(lockDate);
		sql.append("GROUP BY s1.shop_id, s1.WORKING_DATE, status ");

		final String[] fieldNames = new String[] { "shopId", "lockDate", "status", "createDate" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(ShopLockLogVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
}

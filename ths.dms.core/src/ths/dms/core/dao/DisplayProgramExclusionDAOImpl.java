/**
 * @author loctt
 * @since 17sep2013
 */

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StDisplayProgram;
import ths.dms.core.entities.StDisplayProgramExclusion;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DisplayProgramExclusionFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class DisplayProgramExclusionDAOImpl implements
		DisplayProgramExclusionDAO {

	@Autowired
	private IRepository repo;

//	@Autowired
//	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	private CommonDAO commonDAO;

	@Override
	public StDisplayProgramExclusion createDisplayProgramExclusion(
			StDisplayProgramExclusion displayProgramExclusion, LogInfoVO logInfo)
			throws DataAccessException {
		if (displayProgramExclusion == null) {
			throw new IllegalArgumentException("displayProgramExclusion");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		displayProgramExclusion.setCreateDate(DateUtility.now());
		displayProgramExclusion.setCreateUser(logInfo.getStaffCode());
		repo.create(displayProgramExclusion);
		displayProgramExclusion = repo.getEntityById(
				StDisplayProgramExclusion.class,
				displayProgramExclusion.getId());
//		try {
//			actionGeneralLogDAO.createActionGeneralLog(null,
//					displayProgramExclusion, logInfo);
//		} catch (Exception ex) {
//			throw new DataAccessException(ex);
//		}

		return displayProgramExclusion;
	}

	@Override
	public void deleteDisplayProgramExclusion(
			StDisplayProgramExclusion displayProgramExclusion, LogInfoVO logInfo)
			throws DataAccessException {
		if (displayProgramExclusion == null) {
			throw new IllegalArgumentException("displayProgramExclusion");
		}
		repo.delete(displayProgramExclusion);
//		try {
//			actionGeneralLogDAO.createActionGeneralLog(displayProgramExclusion,
//					null, logInfo);
//		} catch (Exception ex) {
//			throw new DataAccessException(ex);
//		}
	}

	@Override
	public StDisplayProgramExclusion getDisplayProgramExclusionById(long id)
			throws DataAccessException {
		return repo.getEntityById(StDisplayProgramExclusion.class, id);
	}

	@Override
	public void updateDisplayProgramExclusion(
			StDisplayProgramExclusion displayProgramExclusion, LogInfoVO logInfo)
			throws DataAccessException {
		if (displayProgramExclusion == null) {
			throw new IllegalArgumentException("displayProgramExclusion");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		StDisplayProgramExclusion temp = this
				.getDisplayProgramExclusionById(displayProgramExclusion.getId());
		displayProgramExclusion.setUpdateDate(commonDAO.getSysDate());
		displayProgramExclusion.setUpdateUser(logInfo.getStaffCode());
		temp = temp.clone();
		repo.update(displayProgramExclusion);
//		try {
//			actionGeneralLogDAO.createActionGeneralLog(temp,
//					displayProgramExclusion, logInfo);
//		} catch (Exception ex) {
//			throw new DataAccessException(ex);
//		}
	}

	@Override
	public List<StDisplayProgramExclusion> getListDisplayProgramExclusion(
			KPaging<StDisplayProgramExclusion> kPaging,
			DisplayProgramExclusionFilter filter, StaffRole staffRole)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
//
//		if (StaffRole.VNM_TABLET_PORTAL.equals(staffRole)) {
//			if (filter.getShopId() == null) {
//				throw new IllegalArgumentException("shopId is null");
//			}
//			sql.append("with lstshop as (select distinct shop_id from shop start with shop_id = ? connect by prior parent_shop_id = shop_id) ");
//			params.add(filter.getShopId());
//		}
//moi sua sep18-loctt
		sql.append(" SELECT dpex.* ");
		sql.append(" FROM DISPLAY_PROGRAM dp, DISPLAY_PROGRAM_EXCLUSION dpex ");
		sql.append(" WHERE dp.DISPLAY_PROGRAM_ID = dpex.DISPLAY_P_EXCLUSION_ID ");

		if (filter.getDisplayProgramId() != null) {
			sql.append(" and dpex.DISPLAY_PROGRAM_ID = ? ");
			params.add(filter.getDisplayProgramId());
		}
		if (!StringUtility.isNullOrEmpty(filter
				.getDisplayProgramExclusionCode())) {
			sql.append(" and upper(dp.DISPLAY_PROGRAM_CODE) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter
					.getDisplayProgramExclusionCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter
				.getDisplayProgramExclusionName())) {
			sql.append(" and upper(dp.DISPLAY_PROGRAM_NAME) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter
					.getDisplayProgramExclusionName().toUpperCase()));
		}
		if (null != filter.getStatus()) {
			sql.append(" and dpex.STATUS = ? ");
			params.add(filter.getStatus().getValue());
		}
//		if (StaffRole.VNM_SALESONLINE_DISTRIBUTOR.equals(staffRole)) {
//		if (StaffRole.VNM_TABLET_PORTAL.equals(staffRole)) {
//			sql.append(" and exists(select 1 from display_shop_map dsm where dsm.display_program_id = dp.display_program_id  and dsm.status = 1 and exists (select 1 from lstshop where dsm.shop_id = shop_id))");
//		}
		sql.append(" order by dp.DISPLAY_PROGRAM_CODE");
		if (null == kPaging) {
			return repo.getListBySQL(StDisplayProgramExclusion.class,
					sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(StDisplayProgramExclusion.class,
					sql.toString(), params, kPaging);
		}
	}

	@Override
	public List<StDisplayProgramExclusion> getDisplayProgramExclusionByDP(
			KPaging<StDisplayProgramExclusion> kPaging,
			StDisplayProgram displayProgram) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from DISPLAY_PROGRAM_EXCLUSION where 1 = 1");

		if (displayProgram != null && displayProgram.getId() != null) {
			sql.append(" and DISPLAY_P_EXCLUSION_ID = ?");
			params.add(displayProgram.getId());
		}

		if (null == kPaging) {
			return repo.getListBySQL(StDisplayProgramExclusion.class,
					sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(StDisplayProgramExclusion.class,
					sql.toString(), params, kPaging);
		}
	}

	// @Override
	// public List<DisplayProgramExclusion> getDisplayProgramByDP(
	// KPaging<DisplayProgramExclusion> kPaging, Long displayProgramId,
	// String displayProgramCode, ActiveType displayProgramExclusionStatus)
	// throws DataAccessException {
	// StringBuilder sql = new StringBuilder();
	// List<Object> params = new ArrayList<Object>();
	// sql.append(" SELECT dppx.* ");
	// sql.append(" FROM DISPLAY_PROGRAM dp, DISPLAY_PROGRAM_EXCLUSION dppx ");
	// sql.append(" WHERE dp.DISPLAY_PROGRAM_ID = dppx.DISPLAY_PROGRAM_ID ");
	//
	// if (displayProgramId != null) {
	// sql.append(" and dp.DISPLAY_PROGRAM_ID = ? ");
	// params.add(displayProgramId);
	// }
	// if (!StringUtility.isNullOrEmpty(displayProgramCode)) {
	// sql.append(" and upper(dp.DISPLAY_PROGRAM_CODE) like ? ESCAPE '/' ");
	// params.add(StringUtility
	// .toOracleSearchLikeSuffix(displayProgramCode.toUpperCase()));
	// }
	// if (null != displayProgramExclusionStatus) {
	// sql.append(" and dppx.STATUS = ? ");
	// params.add(displayProgramExclusionStatus.getValue());
	// }
	//
	// sql.append(" order by dp.DISPLAY_PROGRAM_CODE");
	//
	// if (null == kPaging) {
	// return repo.getListBySQL(DisplayProgramExclusion.class,
	// sql.toString(), params);
	// } else {
	// return repo.getListBySQLPaginated(DisplayProgramExclusion.class,
	// sql.toString(), params, kPaging);
	// }
	// }

	@Override
	public StDisplayProgramExclusion getDisplayProgramExclusionByDisplayAndExclusion(
			long displayProgramId, long exclusionProgramId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from DISPLAY_PROGRAM_EXCLUSION where 1 = 1");
		sql.append(" and DISPLAY_PROGRAM_ID = ?");
		params.add(displayProgramId);
		sql.append(" and DISPLAY_P_EXCLUSION_ID = ?");
		params.add(exclusionProgramId);
		sql.append(" and STATUS <> ?");
		params.add(ActiveType.DELETED.getValue());
		return repo.getEntityBySQL(StDisplayProgramExclusion.class,
				sql.toString(), params);
	}

	// @Override
	// public void updateRemoveListDisplayProgramExclusionIn(
	// Long displayProgramId, List<Long> listDisplayProgramExclusionId)
	// throws DataAccessException {
	// // TODO Auto-generated method stub
	// StringBuilder sql = new StringBuilder();
	// List<Object> params = new ArrayList<Object>();
	// sql.append("update DISPLAY_PROGRAM_EXCLUSION set STATUS = ?");
	// params.add(ActiveType.DELETED.getValue());
	// sql.append(" where DISPLAY_PROGRAM_ID = ?");
	// params.add(displayProgramId);
	// sql.append(" and status <> ?");
	// params.add(ActiveType.DELETED.getValue());
	// if (listDisplayProgramExclusionId.size() > 0) {
	// StringBuilder listDPEId = new StringBuilder();
	// listDPEId.append(" (");
	// for (int i = 0, size = listDisplayProgramExclusionId.size(); i < size;
	// i++) {
	// Long displayProgramExclusionId = listDisplayProgramExclusionId.get(i);
	// listDPEId.append(displayProgramExclusionId);
	// if (i < size - 1) {
	// listDPEId.append(", ");
	// }
	// }
	// listDPEId.append(")");
	// sql.append(" and DP_EXCLUSION_ID not in ");
	// sql.append(listDPEId.toString());
	// }
	// repo.executeSQLQuery(sql.toString(), params);
	// }
}

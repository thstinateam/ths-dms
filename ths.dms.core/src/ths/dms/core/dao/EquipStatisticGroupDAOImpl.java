package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipStatisticGroup;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.EquipObjectType;
import ths.dms.core.entities.enumtype.EquipStatisticStatus;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeStatus;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUsageStatus;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.EquipStatisticFilter;
import ths.dms.core.entities.vo.EquipmentRecordDetailVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.StatisticCheckingVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class EquipStatisticGroupDAOImpl implements EquipStatisticGroupDAO {
	@Autowired
	private IRepository repo;
	
	@Override
	public List<EquipStatisticGroup> getListEquipStatisticGroup(Long recordId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from EQUIP_STATISTIC_GROUP where EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
		return repo.getListBySQL(EquipStatisticGroup.class, sql.toString(), params);
	}
	
	/**
	 * list equip_statistic_group in record
	 * @author phut
	 */
	@Override
	public List<EquipGroup> getListEquipGroupByRecordId(Long recordId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from EQUIP_GROUP where EQUIP_GROUP_ID IN (select OBJECT_ID from EQUIP_STATISTIC_GROUP where EQUIP_STATISTIC_RECORD_ID = ? and OBJECT_TYPE = ? and status =1 )");
		params.add(recordId);
		params.add(EquipObjectType.GROUP.getValue());
		return repo.getListBySQL(EquipGroup.class, sql.toString(), params);
	}
	
	@Override
	public List<Equipment> getListEquipByRecordId(Long recordId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from EQUIPMENT where EQUIP_ID IN (select OBJECT_ID from EQUIP_STATISTIC_GROUP where EQUIP_STATISTIC_RECORD_ID = ? and OBJECT_TYPE = ? and status =1 )");
		params.add(recordId);
		params.add(EquipObjectType.EQUIP.getValue());
		return repo.getListBySQL(Equipment.class, sql.toString(), params);
	}
	
	/**
	 * list u ke 
	 * @author phut
	 */
	@Override
	public List<Product> getListShelfByRecordId(Long recordId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PRODUCT where PRODUCT_ID IN (select OBJECT_ID from EQUIP_STATISTIC_GROUP where status = 1 and EQUIP_STATISTIC_RECORD_ID = ? and OBJECT_TYPE = ?)");
		params.add(recordId);
		params.add(EquipObjectType.SHELF.getValue());
		return repo.getListBySQL(Product.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipmentRecordDetailVO> getListEquipmentRecordDetailVOByRecordId(Long recordId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
//		sql.append("select code, name, catName, total, (total - numLost) numInstn, numLost ");
//		sql.append("from ");
//		sql.append("(select distinct EQUIP_GROUP.CODE code, EQUIP_GROUP.NAME name, EQUIP_CATEGORY.NAME catName, EQUIP_STOCK_TOTAL.QUANTITY total, ");
//		sql.append("(select count(*) from EQUIP_LOST_RECORD  inner join EQUIPMENT on EQUIP_LOST_RECORD.EQUIP_ID = EQUIPMENT.EQUIP_ID ");
//		sql.append("where EQUIP_LOST_RECORD.EQUIP_PERIOD_ID = EQUIP_STATISTIC_RECORD.EQUIP_PRIOD_ID ");
//		sql.append("and EQUIPMENT.EQUIP_GROUP_ID = EQUIP_STATISTIC_GROUP.EQUIP_GROUP_ID ");
//		sql.append(") as numLost ");
//		sql.append("from EQUIP_STATISTIC_RECORD ");
//		sql.append("inner join EQUIP_STATISTIC_GROUP on EQUIP_STATISTIC_RECORD.EQUIP_STATISTIC_RECORD_ID = EQUIP_STATISTIC_GROUP.EQUIP_STATISTIC_RECORD_ID and EQUIP_STATISTIC_RECORD.EQUIP_STATISTIC_RECORD_ID = ? ");
//		params.add(recordId);
//		sql.append("inner join EQUIP_GROUP on EQUIP_STATISTIC_GROUP.EQUIP_GROUP_ID = EQUIP_GROUP.EQUIP_GROUP_ID ");
//		sql.append("inner join EQUIP_STOCK_TOTAL on EQUIP_STOCK_TOTAL.EQUIP_GROUP_ID = EQUIP_GROUP.EQUIP_GROUP_ID ");
//		sql.append("inner join EQUIP_CATEGORY on EQUIP_CATEGORY.EQUIP_CATEGORY_ID = EQUIP_GROUP.EQUIP_CATEGORY_ID)");
		if(recordId == null){
			throw new IllegalArgumentException("recordId is null");
		}
		sql.append(" WITH lstNumber AS ");
		sql.append(" (SELECT EQUIP_GROUP.CODE code, ");
		sql.append(" EQUIP_GROUP.NAME name, ");
		sql.append(" equip_statistic_rec_dtl.equip_id, ");
		sql.append(" equip_statistic_rec_dtl.customer_id, ");
		sql.append(" equip_statistic_rec_dtl.status ");
		sql.append(" FROM EQUIP_STATISTIC_RECORD ");
		sql.append(" JOIN EQUIP_STATISTIC_GROUP ");
		sql.append(" ON EQUIP_STATISTIC_RECORD.EQUIP_STATISTIC_RECORD_ID = EQUIP_STATISTIC_GROUP.EQUIP_STATISTIC_RECORD_ID ");
		sql.append(" AND EQUIP_STATISTIC_RECORD.EQUIP_STATISTIC_RECORD_ID = 6 ");
		sql.append(" JOIN EQUIP_GROUP ");
		sql.append(" ON EQUIP_STATISTIC_GROUP.EQUIP_GROUP_ID = EQUIP_GROUP.EQUIP_GROUP_ID ");
		sql.append(" JOIN equip_statistic_rec_dtl ");
		sql.append(" ON EQUIP_STATISTIC_RECORD.EQUIP_STATISTIC_RECORD_ID = equip_statistic_rec_dtl.EQUIP_STATISTIC_RECORD_ID ");
		sql.append(" join equipment eq ");
		sql.append(" on eq.equip_id = equip_statistic_rec_dtl.equip_id ");
		sql.append(" and eq.equip_group_id = EQUIP_GROUP.EQUIP_GROUP_ID ");
		sql.append(" ), ");
		sql.append(" lstNumberTotal AS ");
		sql.append(" (SELECT distinct equip_group.equip_group_id id, ");
		sql.append(" EQUIP_GROUP.CODE code, ");
		sql.append(" EQUIP_GROUP.NAME name, ");
		sql.append(" EQUIP_CATEGORY.NAME catName, ");
		sql.append(" EQUIP_STOCK_TOTAL.stock_id, ");
		sql.append(" EQUIP_STOCK_TOTAL.QUANTITY total ");
		sql.append(" FROM EQUIP_STATISTIC_RECORD ");
		sql.append(" JOIN EQUIP_STATISTIC_GROUP ");
		sql.append(" ON EQUIP_STATISTIC_RECORD.EQUIP_STATISTIC_RECORD_ID = EQUIP_STATISTIC_GROUP.EQUIP_STATISTIC_RECORD_ID ");
		sql.append(" AND EQUIP_STATISTIC_RECORD.EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(recordId);
		sql.append(" JOIN EQUIP_GROUP ");
		sql.append(" ON EQUIP_STATISTIC_GROUP.EQUIP_GROUP_ID = EQUIP_GROUP.EQUIP_GROUP_ID ");
		sql.append(" JOIN equip_statistic_rec_dtl ");
		sql.append(" on equip_statistic_rec_dtl.EQUIP_STATISTIC_RECORD_ID = EQUIP_STATISTIC_RECORD.EQUIP_STATISTIC_RECORD_ID ");
		sql.append(" left JOIN EQUIP_STOCK_TOTAL ");
		sql.append(" ON EQUIP_STOCK_TOTAL.EQUIP_GROUP_ID = EQUIP_GROUP.EQUIP_GROUP_ID ");
		sql.append(" AND EQUIP_STOCK_TOTAL.stock_type = ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append(" and equip_statistic_rec_dtl.customer_id = EQUIP_STOCK_TOTAL.stock_id ");
		sql.append(" JOIN EQUIP_CATEGORY ");
		sql.append(" ON EQUIP_CATEGORY.EQUIP_CATEGORY_ID = EQUIP_GROUP.EQUIP_CATEGORY_ID ");
		sql.append(" ) ");
		sql.append(" SELECT ");
		sql.append(" tt.id AS id, ");
		sql.append(" tt.code AS code, ");
		sql.append(" tt.name AS name, ");
		sql.append(" tt.catName AS catName, ");
		sql.append(" nvl(sum(total),0) total, ");
		sql.append(" (select count(*) from lstNumber lst ");
		sql.append(" where lst.status = ? ");
		params.add(EquipStatisticStatus.STILL.getValue());
		sql.append(" and tt.code = lst.code ");
		sql.append(" AND tt.name = lst.name) as numInstn , ");
		sql.append(" (select count(*) from lstNumber lst ");
		sql.append(" where lst.status = ? ");
		params.add(EquipStatisticStatus.LOST.getValue());
		sql.append(" and tt.code = lst.code ");
		sql.append(" AND tt.name = lst.name) as numLost ");
		sql.append(" FROM lstNumberTotal tt ");
		sql.append(" GROUP BY tt.id, ");
		sql.append(" tt.CODE, ");
		sql.append(" tt.NAME, ");
		sql.append(" tt.catName ");
		sql.append(" order by tt.code desc ");
		
		String[] fieldNames = { 
			"code",//0
			"name",//1
			"catName",//2			
			"numInstn",//4
			"numLost",//5
			"total"//3
		};
		Type[] fieldTypes = {
				StandardBasicTypes.STRING,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.INTEGER, // 4
				StandardBasicTypes.INTEGER, // 5
				StandardBasicTypes.INTEGER // 3
		};
		return repo.getListByQueryAndScalar(EquipmentRecordDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<ImageVO> getListImageVO(EquipStatisticFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("SELECT EQUIP_ATTACH_FILE.EQUIP_ATTACH_FILE_ID id, \n");
		sql.append("  EQUIP_ATTACH_FILE.URL urlImage, \n");
		sql.append("  EQUIP_ATTACH_FILE.THUMB_URL urlThum, \n");
		sql.append("  EQUIP_ATTACH_FILE.CREATE_DATE createDate, \n");
		sql.append("  EQUIP_ATTACH_FILE.LAT lat, \n");
		sql.append("  EQUIP_ATTACH_FILE.LNG lng, \n");
		sql.append("  dt.OBJECT_STOCK_ID customerId, \n");
//		sql.append("  c.SHORT_CODE customerCode, \n");
//		sql.append("  c.CUSTOMER_NAME customerName, \n");
//		sql.append("  c.STREET street, \n");
//		sql.append("  c.HOUSENUMBER housenumber, \n");
		sql.append("  e.CODE equipCode, \n");
		sql.append("  e.SERIAL seriNumber, \n");
		sql.append("  eg.code equipCodeGroup, ");
		sql.append("  eg.name equipNameGroup, ");
		sql.append("  ec.NAME equipCategoryName, \n");
		sql.append("  sh.SHOP_NAME shopName, \n");
		sql.append("  sh.SHOP_CODE shopCode, \n");
		sql.append("  st.STAFF_ID staffId, \n");
		sql.append("  st.STAFF_CODE staffCode, \n");
		sql.append("  st.STAFF_NAME staffName, \n");
		//SQL.append("  CUSTOMER.LAT custLat, \n");
		//SQL.append("  CUSTOMER.LNG custLng \n");
		sql.append(" EQUIP_ATTACH_FILE.num_product as numberProduct,");
		sql.append(" EQUIP_ATTACH_FILE.POSM,");
		sql.append(" EQUIP_ATTACH_FILE.note as displayNote,");
		sql.append(" EQUIP_ATTACH_FILE.RESULT,");
		sql.append(" EQUIP_ATTACH_FILE.IS_INSPECTED isInSpected ");
		
		sql.append("FROM EQUIP_ATTACH_FILE \n");
		sql.append("INNER JOIN EQUIP_STATISTIC_REC_DTL dt \n");
		sql.append("ON EQUIP_ATTACH_FILE.OBJECT_ID                         = dt.EQUIP_STATISTIC_REC_DTL_ID \n");
		sql.append("INNER JOIN CUSTOMER c \n");
		sql.append("ON dt.OBJECT_STOCK_ID = c.CUSTOMER_ID \n");
		sql.append("INNER JOIN SHOP sh \n");
		sql.append("ON dt.SHOP_ID = sh.SHOP_ID \n");
		sql.append("INNER JOIN STAFF st \n");
		sql.append("ON EQUIP_ATTACH_FILE.STAFF_ID = st.STAFF_ID \n");
		sql.append("INNER JOIN EQUIPMENT e \n");
		sql.append("ON dt.OBJECT_ID = e.EQUIP_ID \n");
		sql.append("INNER JOIN EQUIP_GROUP eg \n");
		sql.append("ON e.EQUIP_GROUP_ID = eg.EQUIP_GROUP_ID \n");
		sql.append("INNER JOIN EQUIP_CATEGORY ec\n");
		sql.append("ON eg.EQUIP_CATEGORY_ID  = ec.EQUIP_CATEGORY_ID \n");
		sql.append("WHERE 1                           = 1 \n");
		sql.append("AND EQUIP_ATTACH_FILE.OBJECT_TYPE = ? \n");
		params.add(EquipTradeType.INVENTORY.getValue());
		if (filter.getRecordId() != null) {
			sql.append(" and dt.EQUIP_STATISTIC_RECORD_ID = ? ");
			params.add(filter.getRecordId());
		}
		if (filter.getEquipId() != null) {
			sql.append(" and dt.object_id = ? ");
			params.add(filter.getEquipId());
		}
		if (filter.getNotStatus() != null) {
			sql.append(" and dt.status != ? ");
			params.add(filter.getNotStatus());
		}
		if(filter.getStatus() != null && filter.getStatus() > -1) {
			sql.append(" and dt.status = ? ");
			params.add(filter.getStatus());
		}
		if(filter.getStepCheck() != null) {
			sql.append(" and dt.STATISTIC_TIME = ? ");
			params.add(filter.getStepCheck());
		}
		if(filter.getShopId() != null) {
			sql.append(" and dt.shop_id = ? ");
			params.add(filter.getShopId());
		}
		if(!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" AND lower(sh.shop_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			sql.append(" AND lower(e.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipCode().trim().toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getSeri())) {
			sql.append(" AND lower(e.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeri().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and (upper(c.short_code) like ? ESCAPE '/' or c.name_text like ? ESCAPE '/')");
			String customerCode = Unicode2English.codau2khongdau(filter.getCustomerCode().trim().toUpperCase());
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
		}
		if(!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" AND (upper(st.staff_code) like  ? ESCAPE '/' OR st.name_text like ? ESCAPE '/' )");
			String staffCode = Unicode2English.codau2khongdau(filter.getStaffCode().trim().toUpperCase());
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode));
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode));
		}
		if (filter.getFromDate() != null) {
			sql.append(" and dt.STATISTIC_DATE >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and dt.STATISTIC_DATE < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		String[] fieldNames = { 
			"id", //1
			"urlImage", //2
			"urlThum", //3
			"createDate", //4
			"lat", //5
			"lng", //6
			"customerId", //7
			//"customerCode", //8
			//"customerName", //9
			//"street", //10
			//"housenumber", //11
			"shopName", //12
			"shopCode", //13
			"staffId", //14
			"staffCode", //15
			"staffName", //16
			//"custLat", //17
			//"custLng", //18
			"equipCode", //19
			"seriNumber", //20
			"equipNameGroup",//21
			"equipCodeGroup",//22
			"equipCategoryName", //23
			"numberProduct",
			"POSM",
			"displayNote",
			"result",
			"isInSpected",
		};

		Type[] fieldTypes = { 
			StandardBasicTypes.LONG, // 1
			StandardBasicTypes.STRING, // 2
			StandardBasicTypes.STRING, // 3
			StandardBasicTypes.TIMESTAMP, // 4
			StandardBasicTypes.FLOAT, // 5
			StandardBasicTypes.FLOAT, // 6
			StandardBasicTypes.LONG, // 7
			//StandardBasicTypes.STRING, // 8
			//StandardBasicTypes.STRING, // 9
			//StandardBasicTypes.STRING, // 10
			//StandardBasicTypes.STRING, // 11
			StandardBasicTypes.STRING, // 12
			StandardBasicTypes.STRING, // 13
			StandardBasicTypes.LONG, //14
			StandardBasicTypes.STRING, //15
			StandardBasicTypes.STRING, //16
			//StandardBasicTypes.FLOAT, //17
			//StandardBasicTypes.FLOAT, //18
			StandardBasicTypes.STRING, //19
			StandardBasicTypes.STRING, //20
			StandardBasicTypes.STRING, //21
			StandardBasicTypes.STRING, //22
			StandardBasicTypes.STRING, //23
			StandardBasicTypes.INTEGER, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.INTEGER, 
			StandardBasicTypes.INTEGER, 
		};
	
		if(filter.getkPagingImageVO() == null) {
			sql.append(" ORDER BY createDate desc ");
			return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(*) as count from (").append(sql).append(" ) ");
			return repo.getListByQueryAndScalarPaginated(ImageVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingImageVO());
		}
	}
	
	@Override
	public List<StatisticCheckingVO> getListStatisticCheckingVOByRecordId(KPaging<StatisticCheckingVO> kPaging, Long recordId, String shopCode, String shortCode, String address, String equipCode, String seri, Integer status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select EQUIP_STATISTIC_REC_DTL.EQUIP_STATISTIC_REC_DTL_ID detailId, shop.shop_code shopCode, customer.SHORT_CODE shortCode, customer.CUSTOMER_NAME customerName, customer.ADDRESS address, EQUIPMENT.CODE equipCode, EQUIPMENT.SERIAL seri, EQUIP_STATISTIC_REC_DTL.STATUS status ");
		sql.append("from EQUIP_STATISTIC_REC_DTL inner join shop on EQUIP_STATISTIC_REC_DTL.shop_id = shop.shop_id and EQUIP_STATISTIC_REC_DTL.EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(recordId);
		sql.append("inner join customer on EQUIP_STATISTIC_REC_DTL.OBJECT_STOCK_ID = customer.CUSTOMER_ID ");
		sql.append("inner join EQUIPMENT on EQUIP_STATISTIC_REC_DTL.OBJECT_ID = EQUIPMENT.EQUIP_ID ");
		sql.append("where 1 = 1 ");
		if(!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append("and upper(shop.shop_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shopCode.trim().toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append("and upper(customer.short_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shortCode.trim().toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(address)) {
			sql.append("and (customer.CUSTOMER_NAME like ? ESCAPE '/' OR customer.address like ? ESCAPE '/') ");
			params.add(StringUtility.toOracleSearchLike(address.trim()));
			params.add(StringUtility.toOracleSearchLike(address.trim()));
		}
		if(!StringUtility.isNullOrEmpty(equipCode)) {
			sql.append("and EQUIPMENT.CODE like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(equipCode.trim().toUpperCase()));			
		}
		if(!StringUtility.isNullOrEmpty(seri)) {
			sql.append("and EQUIPMENT.SERIAL like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(seri.trim()));
		}
		if(status != null && status != -1) {
			sql.append("and EQUIP_STATISTIC_REC_DTL.STATUS = ? ");
			params.add(status);
		}
		sql.append("order by shop.shop_code, customer.short_code, EQUIPMENT.CODE");
		String[] fieldNames = {
			"detailId",
			"shopCode",//0
			"shortCode",//1
			"customerName",//2
			"address",//3
			"equipCode",//4
			"seri",//6
			"status"//7
		};
		Type[] fieldTypes = {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.INTEGER // 8
		};
				
		if(kPaging != null) {
			String sqlCount = "select count(*) as count from (" + sql.toString() + ")";
			return repo.getListByQueryAndScalarPaginated(StatisticCheckingVO.class, fieldNames, fieldTypes, sql.toString(), sqlCount, params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(StatisticCheckingVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	/**
	 * get list equip_statistic_group in record
	 * @author phut
	 */
	@Override
	public List<EquipGroup> getListEquipGroupExceptInRecord(KPaging<EquipGroup> kPaging, Long recordId, String code, Long catId, Float fromCapacity, Float toCapacity, String brand) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select DISTINCT g.* from EQUIP_GROUP g");
		sql.append(" join EQUIP_CATEGORY c on g.EQUIP_CATEGORY_ID = c. EQUIP_CATEGORY_ID");
		sql.append(" join equipment e on g.equip_group_id=e.equip_group_id");
		sql.append(" join equip_provider p on p.equip_provider_id = e.equip_provider_id");
		sql.append(" where g.status=1 and c.status=1 and e.status=1 and p.status=1 and g.EQUIP_GROUP_ID NOT IN (select OBJECT_ID from EQUIP_STATISTIC_GROUP where EQUIP_STATISTIC_RECORD_ID = ? and OBJECT_TYPE = ?) ");
		params.add(recordId);
		params.add(EquipObjectType.GROUP.getValue());
		sql.append(" and g.STATUS = ? ");
		params.add(ActiveType.RUNNING.getValue());
		if(!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and (g.CODE like ? ESCAPE '/' OR lower(g.NAME_TEXT) like ? ESCAPE '/')");
			params.add(StringUtility.toOracleSearchLike(code.trim().toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(code.toLowerCase())));
		}
		if(catId != null && catId != -1) {
			sql.append(" and g.EQUIP_CATEGORY_ID = ? ");
			params.add(catId);
		}
		//Lay khoang giao
		if (toCapacity != null && fromCapacity != null) {
			sql.append(" and ( ");
			sql.append(" (g.CAPACITY_FROM is null and g.CAPACITY_TO is not null and g.CAPACITY_TO >= ?) ");
			params.add(fromCapacity);
			sql.append(" or (g.CAPACITY_FROM is not null and g.CAPACITY_TO is null and g.CAPACITY_FROM < ?) ");
			params.add(toCapacity + 1);
			sql.append(" or (g.CAPACITY_FROM < ? and g.CAPACITY_TO >= ?)  ");
			params.add(fromCapacity + 1);
			params.add(fromCapacity);
			sql.append(" or (g.CAPACITY_FROM >= ? and g.CAPACITY_FROM < ?)  ");
			params.add(fromCapacity);
			params.add(toCapacity + 1);
			sql.append(" or (g.CAPACITY_TO >= ? and g.CAPACITY_TO < ?)  ");
			params.add(fromCapacity);
			params.add(toCapacity + 1);
			sql.append(" ) ");			
		} else if (toCapacity != null) {
			// Truong hop From is null, To not null
			sql.append(" and ( ");
			sql.append(" g.CAPACITY_FROM is null ");
			sql.append(" or (g.CAPACITY_FROM < ?) ");
			params.add(toCapacity + 1);
			sql.append(" ) ");
		} else if (fromCapacity != null) {
			// Truong hop From not null, To is null
			sql.append(" and ( ");
			sql.append(" g.CAPACITY_TO is null or (g.CAPACITY_TO >= ?) ");
			params.add(fromCapacity);
			sql.append(" ) ");
		}
		/*
		if(fromCapacity != null) {
			sql.append(" and g.CAPACITY_FROM >= ? ");
			params.add(fromCapacity);
		}
		if(toCapacity != null) {
			sql.append(" and g.CAPACITY_TO <= ? ");
			params.add(toCapacity);
		}
		*/
		if(!StringUtility.isNullOrEmpty(brand)) {
			sql.append(" and UNICODE2ENGLISH(g.BRAND_NAME) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(brand).trim().toLowerCase()));
		}
		
		if(kPaging != null) {
			String sqlCount = "select count(*) as count from (" + sql.toString() + ")";
			return repo.getListBySQLPaginated(EquipGroup.class, sql.toString(),sqlCount, params,params, kPaging);
		} else {
			return repo.getListBySQL(EquipGroup.class, sql.toString(), params);
		}
	}
	
	/**
	 * get list u ke not in record
	 * @author phut
	 */
	@Override
	public List<Product> getListShelfExceptInRecord(KPaging<Product> kPaging, Long recordId, String code, String name) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PRODUCT where PRODUCT_ID NOT IN (select OBJECT_ID from EQUIP_STATISTIC_GROUP where EQUIP_STATISTIC_RECORD_ID = ? and OBJECT_TYPE = ?) ");
		params.add(recordId);
		params.add(EquipObjectType.SHELF.getValue());
		sql.append(" and STATUS = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and CAT_ID IN (select PRODUCT_INFO_ID from PRODUCT_INFO where PRODUCT_INFO_CODE = ?)");
		params.add("Z");
		if(!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and PRODUCT_CODE like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(code.trim()));
		}
		if(!StringUtility.isNullOrEmpty(name)) {
			sql.append(" and (lower(NAME_TEXT) like ? ESCAPE '/' OR lower(PRODUCT_NAME) like ? ESCAPE '/')");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(name.toLowerCase())));
			params.add(StringUtility.toOracleSearchLike(name.toLowerCase()));
		}
		if(kPaging != null) {
			return repo.getListBySQLPaginated(Product.class, sql.toString(), params, kPaging);
		} else {
			return repo.getListBySQL(Product.class, sql.toString(), params);
		}
	}

	@Override
	public List<Equipment> getEquipmentsInEquipmentGroup(Long equipmentGroupId, Long objectId, EquipStockTotalType stockType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from equipment et ");
		sql.append(" where 1=1 ");
		sql.append(" and et.status = ? ");
		params.add(StatusType.ACTIVE.getValue());
		sql.append(" and et.trade_status = ? ");
		params.add(EquipTradeStatus.NOT_TRADE.getValue());		
		sql.append(" and (et.usage_status = ? ");
		params.add(EquipUsageStatus.IS_USED.getValue());
		sql.append(" or et.usage_status = ?) ");
		params.add(EquipUsageStatus.SHOWING_REPAIR.getValue());
		sql.append(" and et.stock_type = ? ");
		params.add(stockType.getValue());
		if(equipmentGroupId != null){
			sql.append(" and et.equip_group_id = ? ");
			params.add(equipmentGroupId);
		}
		if(objectId != null){
			sql.append(" and et.stock_id = ? ");
			params.add(objectId);
		}
		sql.append(" and exists (select 1 from equip_group where status = ? and equip_group_id = ?) ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(equipmentGroupId);
				
		return repo.getListBySQL(Equipment.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipmentVO> getListEquipmentByStatisticFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (filter.getId() == null) {
			throw new IllegalArgumentException("filter.getId is null");
		}
		sql.append("  with temp as ( ");
		sql.append("  select eq.code as code ");
		sql.append("   ,eq.equip_id as equipId ");
		sql.append("  ,eq.status as status ");
		sql.append("  ,eq.serial as serial ");
		sql.append("   ,eq.stock_id as stockId ");
		sql.append("   ,eq.stock_type as stockType ");
		sql.append("  ,eg.name equipGroupName ");
		sql.append("   ,(SELECT ec.name FROM equip_category ec WHERE eg.equip_category_id = ec.equip_category_id)       AS equipCategoryName ");
		sql.append("   ,eg.brand_name as equipBrandName ");
		sql.append("   ,(SELECT ep.name FROM equip_provider ep WHERE eq.equip_provider_id = ep.equip_provider_id)       AS equipProviderName ");
		sql.append(" ,eq.stock_code stockCode ");
		sql.append(" ,eq.stock_name stockName ");
		sql.append(" ,eg.capacity_from as capacityFrom ");
		sql.append(" ,eg.capacity_to as capacityTo ");
		sql.append(" , cus.short_code AS shortCode ");
		sql.append(" , cus.customer_name AS customerName  ");
		sql.append(" , cus.address AS customerAddress ");
		sql.append(" , cus.status AS statusCus ");
		sql.append(" ,(case when eq.stock_type = 1 then (select s1.shop_code from shop s1 join equip_stock es1 on es1.shop_id = s1.shop_id where es1.equip_stock_id = eq.stock_id)  ");
		sql.append("       when eq.stock_type = 2 then (select s1.shop_code from shop s1 join customer c1 on c1.shop_id = s1.shop_id where c1.customer_id = eq.stock_id) end ) as shopCode  ");
		sql.append(" FROM (SELECT OBJECT_ID ");
		sql.append(" 	   FROM EQUIP_STATISTIC_GROUP ");
		sql.append("       WHERE EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(filter.getId());
		sql.append("       AND OBJECT_TYPE                 = ? ");
		params.add(EquipObjectType.EQUIP.getValue());
		sql.append("        AND status                      = ?   ) ESGP JOIN EQUIPMENT EQ ON EQ.EQUIP_ID = ESGP.OBJECT_ID");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("       JOIN EQUIP_GROUP EG ON EQ.EQUIP_GROUP_ID = EG.EQUIP_GROUP_ID ");
		sql.append("       JOIN CUSTOMER cus ON EQ.STOCK_TYPE = ? AND EQ.STOCK_ID = CUS.CUSTOMER_ID ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append(" WHERE 1            = 1 ");
		if (filter.getStatus() != null) {
			sql.append("  and eq.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append("  and eq.status in (?,?) ");
			params.add(StatusType.ACTIVE.getValue());
			params.add(StatusType.INACTIVE.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			sql.append(" and eq.code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getEquipGroupName())) {
			sql.append(" and upper(eg.name_text) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getEquipGroupName()).trim().toUpperCase()));
		}
		sql.append(" ) ");
		sql.append(" select * from temp where 1=1 ");
		if (filter.getStatusRecord() != null) {
			sql.append(" and statusCus = ? ");
			params.add(filter.getStatusRecord());
		}
		
		String[] fieldNames = { "equipId", 
				"code", 
				"status", 
				"serial",
				"stockCode", 
				"equipGroupName",
				"equipCategoryName", 
				"equipBrandName",
				"equipProviderName", 
				"stockType", 
				"stockName", 
				"stockId", 
				"shortCode", 
				"statusCus", 
				"customerName", 
				"customerAddress",
				"capacityFrom", 
				"capacityTo", 
				"shopCode"};
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING,  StandardBasicTypes.INTEGER, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING};
		
		if (filter.getkPaging() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from ( ");
			countSql.append(sql);
			countSql.append(" ) ");
			sql.append("  order by code ");
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			sql.append("  order by code ");
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public List<EquipmentVO> getListEquipmentStaffByStatisticFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with equip_statistic_staffex as (");
		sql.append(" 	select st.staff_code,et.equip_id");
		sql.append(" 	from equip_statistic_staff et join staff st on et.staff_id = st.staff_id");
		sql.append(" 	where et.equip_statistic_record_id = ?");
		params.add(filter.getId());
		sql.append(" ),");
		sql.append(" rShop as ( ");
		sql.append(" 	select shop_id,shop_code from shop where status = 1 start with shop_id = ? and status = 1 connect by prior shop_id = PARENT_SHOP_ID");
		params.add(filter.getShopRoot());
		sql.append(" )");

		sql.append(" SELECT e.code equipCode, ");
		sql.append(" st.staff_code staffCode, ");
		sql.append(" eg.name AS equipGroupName , ");
		sql.append(" s.shop_code shopCode , ");
		sql.append(" c.short_code shortCode , ");
		sql.append(" c.customer_name customerName , ");
		sql.append(" c.address customerAddress ");
		sql.append(" FROM equipment e ");
		sql.append(" JOIN equip_group eg ON eg.equip_group_id = e.equip_group_id AND e.status = 1 ");
		sql.append(" JOIN equip_statistic_group g ON g.object_type = 1 AND g.object_id = eg.equip_group_id ");
		sql.append(" LEFT JOIN equip_statistic_staffex st ON st.equip_id = e.equip_id ");
		sql.append(" JOIN customer c ON c.customer_id = e.stock_id AND stock_type = ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append(" join rShop s on s.shop_id = c.shop_id ");
		sql.append(" WHERE g.equip_statistic_record_id = ? and e.status = 1 ");
		params.add(filter.getId());
		sql.append(" union ");
		sql.append(" SELECT e.code equipCode, ");
		sql.append(" st.staff_code staffCode, ");
		sql.append(" eg.name AS equipGroupName , ");
		sql.append(" s.shop_code shopCode , ");
		sql.append(" null shortCode , ");
		sql.append(" null customerName , ");
		sql.append(" null customerAddress ");
		sql.append(" FROM equipment e ");
		sql.append(" JOIN equip_group eg ON eg.equip_group_id = e.equip_group_id AND e.status = 1 ");
		sql.append(" JOIN equip_statistic_group g ON g.object_type = 1 AND g.object_id = eg.equip_group_id ");
		sql.append(" LEFT JOIN equip_statistic_staffex st ON st.equip_id = e.equip_id ");
		sql.append(" JOIN equip_stock est ON est.equip_stock_id = e.stock_id AND e.stock_type = ? ");
		params.add(EquipStockTotalType.KHO.getValue());
		sql.append(" join rShop s on s.shop_id = est.shop_id ");
		sql.append(" WHERE g.equip_statistic_record_id = ? and e.status = 1 ");
		params.add(filter.getId());
		sql.append(" union ");
		sql.append(" SELECT e.code equipCode, ");
		sql.append(" st.staff_code staffCode, ");
		sql.append(" eg.name AS equipGroupName , ");
		sql.append(" s.shop_code shopCode , ");
		sql.append(" c.short_code shortCode , ");
		sql.append(" c.customer_name customerName , ");
		sql.append(" c.address customerAddress ");
		sql.append(" FROM equip_statistic_group g ");
		sql.append(" LEFT JOIN equip_statistic_staffex st ON st.equip_id = g.object_id ");
		sql.append(" JOIN equipment e ON e.equip_id = g.object_id AND g.object_type = 3 ");
		sql.append(" JOIN equip_group eg ON eg.equip_group_id = e.equip_group_id ");
		sql.append(" JOIN customer c ON c.customer_id = e.stock_id AND e.stock_type = ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append(" join rShop s on s.shop_id = c.shop_id ");
		sql.append(" WHERE g.equip_statistic_record_id = ? and e.status = 1 ");
		params.add(filter.getId());
		sql.append(" union ");
		sql.append(" SELECT e.code equipCode, ");
		sql.append(" st.staff_code staffCode, ");
		sql.append(" eg.name AS equipGroupName , ");
		sql.append(" s.shop_code shopCode , ");
		sql.append(" null shortCode , ");
		sql.append(" null customerName , ");
		sql.append(" null customerAddress ");
		sql.append(" FROM equip_statistic_group g ");
		sql.append(" LEFT JOIN equip_statistic_staffex st ON st.equip_id = g.object_id ");
		sql.append(" JOIN equipment e ON e.equip_id = g.object_id AND g.object_type = 3 ");
		sql.append(" JOIN equip_group eg ON eg.equip_group_id = e.equip_group_id ");
		sql.append(" JOIN equip_stock est ON est.equip_stock_id = e.stock_id AND e.stock_type = ? ");
		params.add(EquipStockTotalType.KHO.getValue());
		sql.append(" join rShop s on s.shop_id = est.shop_id ");
		sql.append(" WHERE g.equip_statistic_record_id = ? and e.status = 1 ");
		params.add(filter.getId());

		sql.append("  order by equipCode ");
		String[] fieldNames = { "equipCode", "staffCode", "equipGroupName", "shopCode", "shortCode", "customerName", "customerAddress" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
}

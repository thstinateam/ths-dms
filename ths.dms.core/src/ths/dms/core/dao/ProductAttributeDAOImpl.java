package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ProductAttribute;
import ths.dms.core.entities.ProductAttributeDetail;
import ths.dms.core.entities.ProductAttributeEnum;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumFilter;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.ProductAttributeFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProductAttributeDetailVO;
import ths.dms.core.entities.vo.ProductAttributeEnumVO;
import ths.dms.core.entities.vo.ProductAttributeVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

/**
 * Quan ly truy cap du lieu PRODUCT_ATTRIBUTE...
 * @author tientv11
 * @since 16/01/2015
 *
 */
public class ProductAttributeDAOImpl implements ProductAttributeDAO {
	
	@Autowired
	private IRepository repo;
	
	@Autowired
	private CommonDAO commonDAO;

	@Override
	public ProductAttribute getProductAttributeById(long id) throws DataAccessException {
		return repo.getEntityById(ProductAttribute.class, id);
	}
	
	@Override
	public ProductAttributeEnum getProductAttributeEnumById(long id) throws DataAccessException {
		return repo.getEntityById(ProductAttributeEnum.class, id);
	}
	
	@Override
	public ProductAttribute createProductAttribute(ProductAttribute productAttribute,LogInfoVO logInfo)
			throws DataAccessException {
		if (productAttribute == null) {
			throw new IllegalArgumentException("productAttribute is null");
		}
		productAttribute.setCreateDate(commonDAO.getSysDate());
		if (logInfo != null){
			productAttribute.setCreateUser(logInfo.getStaffCode());
		}
		repo.create(productAttribute);
		return repo.getEntityById(ProductAttribute.class, productAttribute.getId());
	}
	
	@Override
	public void updateProductAttribute(ProductAttribute productAttribute, LogInfoVO logInfo) throws DataAccessException {
		if (productAttribute == null) {
			throw new IllegalArgumentException("productAttribute is null");
		}
		productAttribute.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			productAttribute.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(productAttribute);
	}
	
	@Override
	public List<ProductAttributeVO> getListProductAttributeVOFilter(ProductAttributeFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select product_attribute_id id,code,name,type,mandatory,data_length dataLength,min_value minValue,max_value maxValue ");		
		sql.append(" from product_attribute pa ");
		sql.append(" where 1 = 1 and pa.status = 1 ");
		
		sql.append(" order by display_order ");
		
		final String[] fieldNames = new String[] { "id", "code", 
				"name" , 
				"type",
				"mandatory",
				"dataLength",
				"minValue",
				"maxValue"
		};
		
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER
		};
		
		return repo.getListByQueryAndScalar(ProductAttributeVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ProductAttributeEnumVO> getListProductAttributeEnumVOFilter(ProductAttributeFilter filter) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select product_attribute_enum_id id,product_attribute_id productAttId,code,value ");
		
		sql.append(" from product_attribute_enum pae ");		
		sql.append(" where 1 = 1 and pae.status = 1 ");
		
		if(filter.getProductAttId() != null){
			sql.append(" and pae.product_attribute_id = ? ");
			params.add(filter.getProductAttId());
		}
		
		final String[] fieldNames = new String[] { "id", "productAttId","code", "value"};

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING};
		
		return repo.getListByQueryAndScalar(ProductAttributeEnumVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ProductAttributeDetailVO> getListProductAttributeDetailVOFilter(ProductAttributeFilter filter) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select product_attribute_detail_id id,product_attribute_id productAttId,product_id productId,value,product_attribute_enum_id productAttEnumId");
		
		sql.append(" from product_attribute_detail pad ");		
		sql.append(" where 1 = 1 and pad.status = 1 ");
		
		if(filter.getProductAttId() != null){
			sql.append(" and pad.product_attribute_id = ? ");
			params.add(filter.getProductAttId());
		}
		
		if(filter.getProductId() != null){
			sql.append(" and pad.product_id = ? ");
			params.add(filter.getProductId());
		}
		
		final String[] fieldNames = new String[] { "id", "productAttId","productId", 
				"value",
				"productAttEnumId"
		};

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,StandardBasicTypes.LONG, StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.LONG
		};
		
		return repo.getListByQueryAndScalar(ProductAttributeDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public ProductAttributeDetail getProductAttributeDetailByFilter(
			Long productAtt, Long productId, Long productAttEnum)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_attribute_detail ");
		sql.append(" where status = 1 ");
		if(productAtt != null){
			sql.append(" and product_attribute_id = ? ");
			params.add(productAtt);
		}
		
		if(productId != null){
			sql.append(" and product_id = ? ");
			params.add(productId);
		}
		if(productAttEnum != null){
			sql.append(" and product_attribute_enum_id = 1 ");
			params.add(productAttEnum);
		}
		return repo.getEntityBySQL(ProductAttributeDetail.class, sql.toString(), params);
	}

	@Override
	public List<ProductAttributeDetail> getProductAttributeDetailByFilter(
			Long productAtt, Long productId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_attribute_detail ");
		sql.append(" where status = 1 ");
		if(productAtt != null){
			sql.append(" and product_attribute_id = ? ");
			params.add(productAtt);
		}
		
		if(productId != null){
			sql.append(" and product_id = ? ");
			params.add(productId);
		}		
		return repo.getListBySQL(ProductAttributeDetail.class, sql.toString(), params);
	}
	@Override
	public List<AttributeDynamicVO> getListProductAttributeVO(AttributeDynamicFilter filter, KPaging<AttributeDynamicVO> kPaging) throws DataAccessException {
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		selectSql.append("select pa.product_attribute_id attributeId ");
		selectSql.append(" ,pa.code attributeCode ");
		selectSql.append(" ,pa.name attributeName ");
		selectSql.append(" ,pa.type type ");
		selectSql.append(" ,pa.mandatory mandatory ");
		selectSql.append(" ,pa.display_order displayOrder ");
		selectSql.append(" ,pa.status status ");
		selectSql.append(" ,pa.description description ");
		selectSql.append(" ,pa.data_length dataLength ");
		selectSql.append(" ,pa.min_value minValue ");
		selectSql.append(" ,pa.max_value maxValue ");
		fromSql.append(" from product_attribute pa  ");
		fromSql.append("WHERE 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(filter.getAttributeCode())) {
			fromSql.append(" and upper(pa.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getAttributeCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getAttributeName())) {
			fromSql.append(" and upper(pa.name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getAttributeName().toUpperCase()));
		}
		if (filter.getType()!=null) {
			fromSql.append(" and pa.type = ? ");
			params.add(filter.getType());
		}
		if(filter.getStatus() != null){
			fromSql.append(" and pa.status =?");
			params.add(filter.getStatus());
		}
		
		selectSql.append(fromSql.toString());
		
		countSql.append("select count(pa.product_attribute_id) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"attributeId", "attributeCode", "attributeName" 
				,"type","mandatory","displayOrder", "status","description"
				,"dataLength","minValue","maxValue"
		};
		
		String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			selectSql.append(" order by ").append(sort).append(" ");
			String order = StringUtility.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				selectSql.append(order);
			}
		} else {
			selectSql.append(" order by pa.code, pa.name");
		}
		
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING
				,StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER
				,StandardBasicTypes.STRING,StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER
		};	
		List<AttributeDynamicVO> lst =  null;
		if (kPaging == null){
			lst =  repo.getListByQueryAndScalar(AttributeDynamicVO.class, fieldNames,fieldTypes, selectSql.toString(), params);
		}else{
			lst = repo.getListByQueryAndScalarPaginated(AttributeDynamicVO.class, fieldNames, fieldTypes,
					selectSql.toString(), countSql.toString(), params, params, kPaging);
		}
		return lst;
	}
	
	@Override
	public List<AttributeEnumVO> getListProductAttributeEnumVO(AttributeEnumFilter filter,
			KPaging<AttributeEnumVO> kPaging) throws DataAccessException {
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		selectSql.append("select pa.product_attribute_id attributeId ");
		selectSql.append(" ,pae.product_attribute_enum_id enumId ");
		selectSql.append(" ,pae.code enumCode ");
		selectSql.append(" ,pae.value enumValue ");
		selectSql.append(" ,pae.status status");
		fromSql.append(" from product_attribute pa ");
		fromSql.append(" join product_attribute_enum pae on pa.product_attribute_id = pae.product_attribute_id ");
		fromSql.append(" WHERE 1 = 1 and pae.status = 1 ");
		List<Object> params = new ArrayList<Object>();
		
		if(filter.getStatus() != null){
			selectSql.append(" and pa.status = ? ");
			params.add(filter.getStatus().getValue());
		}
		
		if (filter.getAttributeId() != null) {
			fromSql.append(" and pa.product_attribute_id = ? ");
			params.add(filter.getAttributeId());
		}
		
		selectSql.append(fromSql.toString());
		
		countSql.append("select count(pae.product_attribute_enum_id) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"attributeId", "enumId", "enumCode", "enumValue", "status"
				};
		
		String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			selectSql.append(" order by ").append(sort).append(" ");
			String order = StringUtility.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				selectSql.append(order);
			}
		} else {
			selectSql.append(" order by pae.code, pae.value");
		}
		
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER
		};	
		List<AttributeEnumVO> lst =  null;
		if (kPaging == null) {
			lst = repo.getListByQueryAndScalar(AttributeEnumVO.class, fieldNames, fieldTypes, selectSql.toString(), params);
		} else {
			lst = repo.getListByQueryAndScalarPaginated(AttributeEnumVO.class, fieldNames, fieldTypes, selectSql.toString(), countSql.toString(), params, params, kPaging);
		}
		return lst;
	}
	@Override
	public ProductAttribute getProductAttributeByCode(String code)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_attribute ");
		sql.append(" where 1 = 1 and (status = 1 or status = 0)");
		sql.append(" and code = ? ");
		params.add(code.toUpperCase());
		return repo.getEntityBySQL(ProductAttribute.class, sql.toString(), params);
	}
	
	@Override
	public ProductAttributeEnum getProductAttributeEnumByCode(String code)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_attribute_enum ");
		sql.append(" where 1 = 1 and (status = 1 or status = 0) ");
		sql.append(" and code = ? ");
		params.add(code.toUpperCase());
		return repo.getEntityBySQL(ProductAttributeEnum.class, sql.toString(), params);
	}
	@Override
	public ProductAttributeEnum createProductAttributeEnum(ProductAttributeEnum staffAttributeEnum,LogInfoVO logInfo)
			throws DataAccessException {
		if (staffAttributeEnum == null) {
			throw new IllegalArgumentException("staffAttributeEnum is null");
		}
		staffAttributeEnum.setCreateDate(commonDAO.getSysDate());
		if (logInfo != null){
			staffAttributeEnum.setCreateUser(logInfo.getStaffCode());
		}
		repo.create(staffAttributeEnum);
		return repo.getEntityById(ProductAttributeEnum.class, staffAttributeEnum.getId());
	}	
	
	@Override
	public void updateProductAttributeEnum(ProductAttributeEnum productAttributeEnum, LogInfoVO logInfo) throws DataAccessException {
		if (productAttributeEnum == null) {
			throw new IllegalArgumentException("productAttributeEnum is null");
		}
		productAttributeEnum.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			productAttributeEnum.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(productAttributeEnum);
	}
	
	@Override
	public void updateProductAttributeDetail(ProductAttributeDetail productAttributeDetail, LogInfoVO logInfo) throws DataAccessException {
		if (productAttributeDetail == null) {
			throw new IllegalArgumentException("productAttributeDetail is null");
		}
		productAttributeDetail.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			productAttributeDetail.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(productAttributeDetail);
	}
	
	@Override
	public List<ProductAttributeDetail> getListProductAttributeDetailByEnumId(Long id) throws DataAccessException {

		if (id == null)
			return new ArrayList<ProductAttributeDetail>();
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from product_attribute_detail where 1 = 1");
		sql.append(" and status = 1 and product_attribute_enum_id = ? ");
		params.add(id);
		return repo.getListBySQL(ProductAttributeDetail.class, sql.toString(), params);
	}
}

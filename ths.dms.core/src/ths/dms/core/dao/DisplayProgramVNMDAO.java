package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.DisplayProgramVNM;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

public interface DisplayProgramVNMDAO {
	DisplayProgramVNM getDisplayProgramVNMByID(Long id) throws DataAccessException;
	DisplayProgramVNM getDisplayProgramVNMByCode(String code) throws DataAccessException;
	List<DisplayProgramVNM> getListDisplayProgramVNM(KPaging<DisplayProgramVNM> kPaging, Long shopId, String code, 
			String name, ActiveType status, Date fDate, Date tDate) throws DataAccessException;
}

package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.EquipDeliveryRecDtl;
import ths.dms.core.entities.EquipDeliveryRecord;
import ths.dms.core.entities.EquipEvictionForm;
import ths.dms.core.entities.EquipEvictionFormDtl;
import ths.dms.core.entities.EquipFormHistory;
import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipGroupProduct;
import ths.dms.core.entities.EquipHistory;
import ths.dms.core.entities.EquipImportRecord;
import ths.dms.core.entities.EquipItem;
import ths.dms.core.entities.EquipItemConfig;
import ths.dms.core.entities.EquipItemConfigDtl;
import ths.dms.core.entities.EquipLend;
import ths.dms.core.entities.EquipLendDetail;
import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.EquipProvider;
import ths.dms.core.entities.EquipRepairForm;
import ths.dms.core.entities.EquipRepairFormDtl;
import ths.dms.core.entities.EquipRepairPayForm;
import ths.dms.core.entities.EquipRepairPayFormDtl;
import ths.dms.core.entities.EquipRole;
import ths.dms.core.entities.EquipRoleUser;
import ths.dms.core.entities.EquipSalePlan;
import ths.dms.core.entities.EquipStatisticCustomer;
import ths.dms.core.entities.EquipStatisticRecDtl;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.EquipStockTotal;
import ths.dms.core.entities.EquipStockTransForm;
import ths.dms.core.entities.EquipStockTransFormDtl;
import ths.dms.core.entities.EquipSuggestEvictionDTL;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.EquipGFilter;
import ths.dms.core.entities.enumtype.EquipStockEquipFilter;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.EquipmentRepairFormFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.EquipItemConfigDtlFilter;
import ths.dms.core.entities.filter.EquipItemConfigFilter;
import ths.dms.core.entities.filter.EquipItemFilter;
import ths.dms.core.entities.filter.EquipPermissionFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipRepairFilter;
import ths.dms.core.entities.filter.EquipRoleFilter;
import ths.dms.core.entities.filter.EquipStaffFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.filter.EquipmentEvictionFilter;
import ths.dms.core.entities.filter.EquipmentGroupProductFilter;
import ths.dms.core.entities.filter.EquipmentSalePlaneFilter;
import ths.dms.core.entities.vo.EquipItemConfigDtlVO;
import ths.dms.core.entities.vo.EquipItemConfigVO;
import ths.dms.core.entities.vo.EquipItemVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipRepairFormVO;
import ths.dms.core.entities.vo.EquipRepairFormVOList;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentDeliveryPrintVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentEvictionVO;
import ths.dms.core.entities.vo.EquipmentExVO;
import ths.dms.core.entities.vo.EquipmentGroupProductVO;
import ths.dms.core.entities.vo.EquipmentGroupVO;
import ths.dms.core.entities.vo.EquipmentManagerVO;
import ths.dms.core.entities.vo.EquipmentPermissionVO;
import ths.dms.core.entities.vo.EquipmentRecordDeliveryVO;
import ths.dms.core.entities.vo.EquipmentRepairFormDtlVO;
import ths.dms.core.entities.vo.EquipmentRepairFormVO;
import ths.dms.core.entities.vo.EquipmentStaffVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProductTreeVO;
import ths.dms.core.entities.vo.ShopViewParentVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Quan ly thiet bi DAO
 * 
 * @author hunglm16
 * @since December 14,2014
 * */
public interface EquipmentManagerDAO {
	/**
	 * Them moi thong tin thiet bi
	 * 
	 * @author hunglm16
	 * @since December 14,2014
	 * */
	Equipment createEquipment(Equipment equipment) throws DataAccessException;

	/**
	 * Cap nhat thong tin thiet bi
	 * 
	 * @author hunglm16
	 * @since December 14,2014
	 * */
	void updateEquipment(Equipment equipment) throws DataAccessException;

	/**
	 * Lay danh sach ky quan ly thiet bj
	 * 
	 * @author tientv11
	 * @sine 17/12/2014
	 */
	List<EquipPeriod> getListEquipPeriodByFilter(EquipmentFilter<EquipPeriod> filter) throws DataAccessException;

	/**
	 * Tim kiem danh muc thiet bi
	 * 
	 * @author hoanv25
	 * @since December 14,2014
	 * */
	List<EquipmentVO> getListEquipmentGeneral(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Creat EquipCategory
	 * 
	 * @author hoanv25
	 * @since December 14,2014
	 * */
	EquipCategory createEquipCategoryManagerCatalog(EquipCategory equipcategory, LogInfoVO logInfoVO) throws DataAccessException;

	/**
	 * Creat EquipProvider
	 * 
	 * @author hoanv25
	 * @since December 14,2014
	 * */
	EquipProvider createEquipProviderManagerCatalog(EquipProvider equipprovider, LogInfoVO logInfoVO) throws DataAccessException;

	/**
	 * Creat EquipItem
	 * 
	 * @author hoanv25
	 * @since December 14,2014
	 * */
	EquipItem createEquipItemManagerCatalog(EquipItem equipItem, LogInfoVO logInfoVO) throws DataAccessException;

	/**
	 * Update EquipCategory
	 * 
	 * @author hoanv25
	 * @return
	 * @since December 14,2014
	 * */

	EquipCategory updateEquipCategoryManagerCatalog(EquipCategory equipCategory, LogInfoVO logInfoVO) throws DataAccessException;

	/**
	 * Update EquipProvider
	 * 
	 * @author hoanv25
	 * @return
	 * @since December 14,2014
	 * */
	EquipProvider updateEquipProviderManagerCatalog(EquipProvider equipProvider, LogInfoVO logInfoVO) throws DataAccessException;

	/**
	 * Update EquipItem
	 * 
	 * @author hoanv25
	 * @return
	 * @since December 14,2014
	 * */
	EquipItem updateEquipItemManagerCatalog(EquipItem equipItem, LogInfoVO logInfoVO) throws DataAccessException;

	/**
	 * Them moi phieu thu hoi va thanh ly
	 * 
	 * @author phuongvm
	 * @since 15/12/2014
	 * */
	EquipEvictionForm createEquipEvictionForm(EquipEvictionForm equipEvictionForm) throws DataAccessException;

	/**
	 * cap nhat phieu thu hoi va thanh ly
	 * 
	 * @author phuongvm
	 * @since 15/12/2014
	 * */
	void updateEquipEvictionForm(EquipEvictionForm equipEvictionForm) throws DataAccessException;

	/**
	 * lay danh sach phieu thu hoi va thanh ly
	 * 
	 * @author phuongvm
	 * @since 15/12/2014
	 * */
	List<EquipmentEvictionVO> getListEquipmentEviction(EquipmentEvictionFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi
	 * 
	 * @author nhutnn
	 * @since 15/12/2014
	 * @param kPaging
	 * @param equipmentFilter
	 * @throws DataAccessException
	 */
	List<EquipmentDeliveryVO> getListEquipmentDeliveryVOByFilter(EquipmentFilter<EquipmentDeliveryVO> filter) throws DataAccessException;

	/**
	 * lay danh sach lich su cac phieu
	 * 
	 * @author phuongvm
	 * @since 15/12/2014
	 * */
	List<EquipmentEvictionVO> getHistory(Long idRecord, EquipTradeType recordType) throws DataAccessException;

	/**
	 * Lay equipment Entity by code an status
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	Equipment getEquipmentByCodeAndStatus(String code, ActiveType status)  throws DataAccessException;
	
	/**
	 * Lay equipment Entity by code theo filter
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	Equipment getEquipmentByCodeFilter(EquipmentFilter<Equipment> filter) throws DataAccessException;
	
	/**
	 * Lay equipment Entity
	 * 
	 * @author nhutnn
	 * @since 16/12/2014
	 * @return
	 * @throws DataAccessException
	 */
	Equipment getEquipmentByCode(String code) throws DataAccessException;

	EquipEvictionForm getEquipEvictionFormById(long id) throws DataAccessException;

	/**
	 * Them moi lich su bien ban
	 * 
	 * @author nhutnn
	 * @since 17/12/2014
	 * @param equipFormHistory
	 * @return
	 * @throws DataAccessException
	 */
	EquipFormHistory createEquipmentHistoryRecord(EquipFormHistory equipFormHistory) throws DataAccessException;

	/**
	 * Lay EquipCategory theo Id
	 * 
	 * @author hoanv25
	 * @since December 16,2014
	 * */
	EquipCategory getEquipCategoryById(Long id) throws DataAccessException;

	/**
	 * Lay EquipItem theo Id
	 * 
	 * @author hoanv25
	 * @since December 16,2014
	 * */
	EquipItem getEquipItemById(Long id) throws DataAccessException;

	/**
	 * Lay EquipProvider theo Id
	 * 
	 * @author hoanv25
	 * @since December 16,2014
	 * */
	EquipProvider getEquipProviderById(Long id) throws DataAccessException;
	
	

	/**
	 * Lay EquipProvider theo Id
	 * 
	 * @author hoanv25
	 * @since December 16,2014
	 * */
	EquipGroup getEquipGroupById(Long id) throws DataAccessException;

	/**
	 * lay danh chi tiet phieu thu hoi va thanh ly
	 * 
	 * @author phuongvm
	 * @since 17/12/2014
	 * */
	List<EquipEvictionFormDtl> getEquipEvictionFormDtlByEquipEvictionFormId(Long equipEvictionFormId) throws DataAccessException;

	/**
	 * Lay danh sach hieu thiet bi
	 * 
	 * @author hoanv25
	 * @since December 16,2014
	 * */
	List<EquipmentVO> getListEquipmentBrand(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach hieu thiet bi
	 * 
	 * @author hoanv25
	 * @since December 16,2014
	 * */
	List<EquipmentVO> getListEquipmentCategory(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach nhom thiet bi
	 * 
	 * @author hoanv25
	 * @since December 17,2014
	 * */
	List<EquipmentVO> searchEquipmentGroup(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach NCC thiet bi
	 * 
	 * @author hoanv25
	 * @since December 17,2014
	 * */
	List<EquipmentVO> getListEquipmentProvider(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach theo Id khi update nhom thiet bi
	 * 
	 * @author hoanv25
	 * @since December 17,2014
	 * */
	List<EquipmentVO> getEquipFlan(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi theo tham so Filter
	 * 
	 * @author hoanv25
	 * @since December 16,2014
	 * */
	List<EquipmentVO> getListEquipmentByFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach loai thiet bi theo Filter
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	List<EquipCategory> getListEquipmentCategoryByFilter(EquipmentFilter<EquipCategory> filter) throws DataAccessException;

	/**
	 * Lay danh sach NCC thiet bi theo Filter
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	List<EquipProvider> getListEquipProviderByFilter(EquipmentFilter<EquipProvider> filter) throws DataAccessException;

	/**
	 * Tim kiem lich su thiet bi theo Filter
	 * 
	 * @author hunglm16
	 * @since December 19,2014
	 * */
	List<EquipmentRepairFormVO> searchListEquipmentRepairFormVOByFilter(EquipmentRepairFormFilter<EquipmentRepairFormVO> filter) throws DataAccessException;

	/**
	 * Them moi chi tiet phieu thu hoi va thanh ly
	 * 
	 * @author phuongvm
	 * @since 18/12/2014
	 * */
	EquipEvictionFormDtl createEquipEvictionFormDtl(EquipEvictionFormDtl equipEvictionFormDtl) throws DataAccessException;

	/**
	 * Creat EquipGroup
	 * 
	 * @author hoanv25
	 * @since December 18,2014
	 * */
	EquipGroup createEquipmentGroup(EquipGroup equipGroup) throws DataAccessException;

	/**
	 * Creat EquipSalePlan
	 * 
	 * @author hoanv25
	 * @since December 18,2014
	 * */
	EquipSalePlan createEquipSalePlan(EquipSalePlan equipSalePlan) throws DataAccessException;

	/**
	 * Lay EquipSalePlan theo Id
	 * 
	 * @author hoanv25
	 * @since December 18,2014
	 * */
	EquipSalePlan getEquipSalePlanById(Long id) throws DataAccessException;

	/**
	 * Update EquipSalePlan
	 * 
	 * @author hoanv25
	 * @since December 18,2014
	 * */
	EquipGroup updateEquipGroup(EquipGroup equipGroup, LogInfoVO logInfoVO) throws DataAccessException;

	/**
	 * Update EquipSalePlan
	 * 
	 * @author hoanv25
	 * @since December 18,2014
	 * */
	EquipSalePlan updateEquipSalePlan(EquipSalePlan equipSalePlan, LogInfoVO logInfoVO) throws DataAccessException;

	/**
	 * Lay Equipment theo Id
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	Equipment getEquipmentById(Long id) throws DataAccessException;
	
	/**
	 * Lay equipment theo id & shop
	 * 
	 * @author dungnt19
	 * @since 2016/04/15
	 */
	Equipment getEquipmentById(Long equipmentId, Long shopId) throws DataAccessException;

	/**
	 * Sinh EquipImportRecordCode
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	String getEquipImportRecordCodeGenetated() throws DataAccessException;

	/**
	 * Tim kiem loai thiet bi
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	List<EquipmentVO> searchEquipmentGroupVObyFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Xoa nhom thiet bi
	 * 
	 * @author hoanv25
	 * @since December 18,2014
	 * */
	EquipSalePlan deleteEquipSalePlan(EquipSalePlan equipSalePlan, LogInfoVO logInfoVO) throws DataAccessException;

	/**
	 * Tim kiem nhung thiet bi da duoc lap bien ban giao nhan theo khach hang
	 * 
	 * @author phuongvm
	 * @since 20/12/2014
	 * */
	List<EquipmentExVO> getListEquipmentDeliveryByCustomerId(Long customerId) throws DataAccessException;

	/**
	 * Them moi bien ban cap moi thiet bi
	 * 
	 * @author hunglm16
	 * @since December 21,2014
	 * */
	EquipImportRecord createEquipImportRecord(EquipImportRecord equipImportRecord) throws DataAccessException;

	/**
	 * Lay Max EquipImportRecordCode
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	String getMaxEquipImportRecordCode() throws DataAccessException;

	/**
	 * Lay Max getMaxEquipmentCode theo tien to
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * */
	String getMaxEquipmentCode(String prefix) throws DataAccessException;

	/**
	 * Them moi EquipStockTransForm
	 * 
	 * @author hunglm16
	 * @since December 21,2014
	 * */
	EquipStockTransForm createEquipStockTransForm(EquipStockTransForm equipStockTransForm) throws DataAccessException;

	/**
	 * Lay kho dau tien tim thay duoc voi tham so filter
	 * 
	 * @author hunglm16
	 * @since December 21,2014
	 * */
	EquipStockTotal getFirtEquipStockTotalByFilter(EquipStockEquipFilter<EquipStockTotal> filter) throws DataAccessException;

	/**
	 * Them moi kho thiet bi EquipStockTotal
	 * 
	 * @author hunglm16
	 * @since December 21,2014
	 * */
	EquipStockTotal createEquipStockTotal(EquipStockTotal equipStockTotal) throws DataAccessException;

	/**
	 * Cap nhat kho thiet bi EquipStockTotal
	 * 
	 * @author hunglm16
	 * @since December 21,2014
	 * */
	void updateEquipStockTotal(EquipStockTotal equipStockTotal) throws DataAccessException;

	/**
	 * Lay nhom thiet bi Equip Group
	 * 
	 * @author hunglm16
	 * @since December 22,2014
	 * */
	EquipGroup getEquipGroupByCode(String code) throws DataAccessException;

	/**
	 * Lay nhom thiet bi Equip Group
	 * 
	 * @author hunglm16
	 * @since December 22,2014
	 * */
	EquipProvider getEquipProviderByCode(String code) throws DataAccessException;

	/**
	 * Them moi lich su thiet bi EquipHistory
	 * 
	 * @author hunglm16
	 * @since December 22,2014
	 * */
	EquipHistory createEquipHistory(EquipHistory equipHistory) throws DataAccessException;

	/**
	 * Lay danh sach lich su thiet bi
	 * 
	 * @author tamvnm
	 * @since 26/06/2015
	 * */
	List<EquipHistory> getListEquipHistoryByEquipId(Long equipId)  throws DataAccessException;
	
	
	/**
	 * Tim kiem tat ca thiet bi da duoc lap bien ban giao nhan theo khach hang
	 * 
	 * @author phuongvm
	 * @since 20/12/2014
	 * */
	List<EquipmentExVO> getListAllEquipmentDeliveryByCustomerId(Long customerId, List<Long> lstEquipId) throws DataAccessException;

	/**
	 * xoa chi tiet bien ban thu hoi thanh ly
	 * 
	 * @author phuongvm
	 * @since 22/12/2014
	 * */
	void deleteEquipEvictionFormDtl(EquipEvictionFormDtl equipEvictionFormDtl) throws DataAccessException;

	/**
	 * Tim kiem bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @since 22/12/2014
	 * */
	List<EquipmentVO> searchEquipmentStockChange(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi
	 * 
	 * @author hoanv25
	 * @since 25/12/2014
	 * @param equipmentFilter
	 * @throws DataAccessException
	 */
	List<EquipmentDeliveryVO> getListEquipmentStockVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws DataAccessException;

	/**
	 * Lay danh sach kho nguon va kho dich
	 * 
	 * @author hoanv25
	 * @since 25/12/2014
	 * @param equipmentFilter
	 * @throws DataAccessException
	 */
	List<EquipmentVO> getListEquipToStock(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	List<EquipmentVO> getListEquipStock(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Kiem tra ma Loai, NCC, Hieu, HangMucSuaChua trung
	 * 
	 * @author hoanv25
	 * @since 29/12/2014
	 * @param Code
	 * @return
	 * @throws BusinessException
	 */
	List<EquipmentVO> listEquipCategoryCode(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	List<EquipmentVO> listEquipEquipProvider(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	List<EquipmentVO> listEquipBrand(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	List<EquipmentVO> listEquipItem(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi theo id loai
	 * 
	 * @author hoanv25
	 * @since 25/12/2014
	 * @param equipmentFilter
	 * @throws DataAccessException
	 */
	List<EquipmentVO> getListEquipGroupById(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi theo id ncc
	 * 
	 * @author hoanv25
	 * @since 25/12/2014
	 * @param equipmentFilter
	 * @throws DataAccessException
	 */
	List<EquipmentVO> getListEquipGroupByIdEquipProvider(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi theo id hangmucsuachua
	 * 
	 * @author hoanv25
	 * @since 25/12/2014
	 * @param equipmentFilter
	 * @throws DataAccessException
	 */
	List<EquipmentVO> listEquipItemById(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/***
	 * @author vuongmq
	 * @date 22/04/2015
	 * lay danh sach theo in; giong giong nhu getListEquipRepair; nhung them mot so cot
	 */
	List<EquipRepairFormVOList> getListEquipRepairPrint(EquipRepairFilter filter) throws DataAccessException;
	/**
	 * Lay danh sach phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @param EquipRepairFilter
	 * @throws DataAccessException
	 */
	List<EquipRepairFormVO> getListEquipRepair(EquipRepairFilter filter) throws DataAccessException;

	/***
	 * @author vuongmq
	 * @date 20/04/2015
	 * lay equip_repair_form_dtl theo filter, 
	 * lay hang muc con thoi gian bao hanh
	 */
	EquipRepairFormDtl getEquipRepairFormDtlByFilter(EquipItemFilter filter) throws DataAccessException;
	/**
	 * Lay danh sach chit tiet phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @param EquipRepairFilter
	 *            .EquipRepairFormId
	 * @throws DataAccessException
	 */
	List<EquipRepairFormDtl> getListEquipRepairDtlByEquipRepairId(EquipRepairFilter filter) throws DataAccessException;

	/**
	 * Kiem tra ma nhom thiet bi khi tao moi
	 * 
	 * @author hoanv25
	 * @since 25/12/2014
	 * @param equipmentFilter
	 * @throws DataAccessException
	 */
	List<EquipmentVO> getlistEquipGroup(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Tao moi phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @param EquipRepairForm
	 * @throws DataAccessException
	 */
	EquipRepairForm createEquipRepairForm(EquipRepairForm equipRepairForm) throws DataAccessException;

	/**
	 * lay 1 phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @param EquipRepairForm
	 * @throws DataAccessException
	 */
	EquipRepairForm getEquipRepairFormById(long id) throws DataAccessException;

	/**
	 * cap nhat phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 30/12/2014
	 * @param EquipRepairForm
	 * @throws DataAccessException
	 */
	EquipRepairForm updateEquipRepairForm(EquipRepairForm equipRepairForm) throws DataAccessException;

	/**
	 * Lay danh sach phieu sua chua man hinh duyet
	 * 
	 * @author phuongvm
	 * @since 31/12/2014
	 * @param EquipRepairFilter
	 * @throws DataAccessException
	 */
	List<EquipRepairFormVO> getListEquipRepairEx(EquipRepairFilter filter) throws DataAccessException;

	/**
	 * Lay lich su Bien ban thiet bi
	 * 
	 * @author hunglm16
	 * @since January 05,2014
	 */
	List<EquipRecordVO> getHistoryEquipRecordByFilter(EquipRecordFilter<EquipRecordVO> filter) throws DataAccessException;

	/**
	 * Lay id sale_lane theo id equip_group
	 * 
	 * @author hoanv25
	 * @since 3/1/2015
	 */
	List<EquipmentVO> getListEquipSalePlan(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach hang muc
	 * 
	 * @author phuongvm
	 * @since 05/01/2015
	 * @param EquipItemFilter
	 * @throws DataAccessException
	 */
	List<EquipItemVO> getListEquipItem(EquipItemFilter filter) throws DataAccessException;

	/***
	 * @author vuongmq
	 * @date 06/04/2015
	 * lay danh sach hang muc popup lap phieu yeu cau sua chua
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipItemVO> getListEquipItemPopUpRepair(EquipItemFilter filter) throws DataAccessException;
	
	/**
	 * Lay Max Equipment theo Id Nhom Thiet Bi
	 * 
	 * @author hoanv25
	 * @since January 05,2014
	 */
	String getMaxEquipmentCodeByEquipGroupId(Long equipGroupId) throws DataAccessException;
	
	/**
	 * Lay ma thiet bi lon nhat thuoc Loai thiet bi
	 * 
	 * @author tamvnm
	 * @param equipCategoryId
	 * @return Ma thiet bi lon nhat
	 * @throws DataAccessException
	 * @since 31/08/2015
	 */
	String getMaxEquipmentCodeByEquipCategoryId(Long equipCategoryId) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi co ban hop dong moi nhat
	 * 
	 * @author hunglm16
	 * @since January 06,2014
	 */
	List<EquipmentVO> searchEquipByEquipDeliveryRecordNew(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi Entites
	 * 
	 * @author hunglm16
	 * @since January 06,2014
	 */
	List<Equipment> getListEquipmentEntitesByFilter(EquipmentFilter<Equipment> filter) throws DataAccessException;

	/**
	 * Xoa nhom thiet bi
	 * 
	 * @author hoanv25
	 * @since 3/1/2015
	 */
	EquipSalePlan deleteEquipmentSalePlan(EquipSalePlan eqs) throws DataAccessException;

	/**
	 * Them nhom thiet bi
	 * 
	 * @author hoanv25
	 * @since 3/1/2015
	 */
	EquipSalePlan createEquipmentSalePlane(EquipSalePlan eqs) throws DataAccessException;

	/**
	 * Lay danh sach EquipSalePlane theo id
	 * 
	 * @author hoanv25
	 * @since 3/1/2015
	 */
	EquipSalePlan getEquipSalePlaneById(Long id) throws DataAccessException;

	/**
	 * Sua nhom thiet bi
	 * 
	 * @author hoanv25
	 * @since 3/1/2015
	 */
	EquipSalePlan updateEquipmentSalePlane(EquipSalePlan eqs) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi
	 * 
	 * @author phuongvm
	 * @since 06/01/2015
	 * @param kPaging
	 * @param equipmentFilter
	 * @throws DataAccessException
	 */
	List<EquipmentDeliveryVO> getListEquipmentDeliveryVOByFilterEx(EquipmentFilter<EquipmentDeliveryVO> filter) throws DataAccessException;

	/**
	 * Tim kiem danh sach thiet bi bien ban sua chua, 
	 * lay theo phan quyen; popup thiet bi
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	List<EquipmentDeliveryVO> getListEquipmentPopUp(EquipmentFilter<EquipmentDeliveryVO> filter) throws DataAccessException;
	/**
	 * Sua trang thai bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @since 5/1/2015
	 */
	EquipStockTransForm updateEquipStockTransForm(EquipStockTransForm e) throws DataAccessException;

	/**
	 * Lay danh sach bien ban chuyen kho theo id
	 * 
	 * @author hoanv25
	 * @since 5/1/2015
	 */
	EquipStockTransForm getEquipStockTransForm(Long id) throws DataAccessException;

	/**
	 * Lay ma kho nguon theo id
	 * 
	 * @author hoanv25
	 * @since 5/1/2015
	 */
	Shop getListEquipStockTransForm(Long fromStockId) throws DataAccessException;

	/**
	 * Lay ma kho nguon theo id
	 * 
	 * @author hoanv25
	 * @since 5/1/2015
	 */
	Shop getListToStockTransForm(Long toStockId) throws DataAccessException;

	/**
	 * Tu sinh ma bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @since 5/1/2015
	 */
	String getCodeStockTransForm() throws DataAccessException;

	/**
	 * Tao moi bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @since 5/1/2015
	 */
	EquipStockTransForm createEquipmentStockTransForm(EquipStockTransForm equipStockTransForm) throws DataAccessException;

	/**
	 * Tao moi chi tiet bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @since 5/1/2015
	 */
	EquipStockTransFormDtl createEquipStockTransFormDtl(EquipStockTransFormDtl ed) throws DataAccessException;

	/**
	 * tim kiem nhom thiet bi theo filter
	 * 
	 * @author tuannd20
	 * @param equipmentGroupFilter
	 *            dieu kien tim kiem thiet bi
	 * @return ket qua tim kiem nhom thiet bi
	 * @throws BusinessException
	 * @date 05/01/2015
	 */
	List<EquipmentGroupVO> searchEquipmentGroupVO(EquipmentGroupProductFilter<EquipmentGroupVO> equipmentGroupFilter) throws DataAccessException;
	
	/**
	 * Lay danh sach san pham nhom thiet bi cho export
	 * 
	 * @author tamvnm
	 * @param equipmentGroupFilter
	 *            dieu kien tim kiem thiet bi
	 * @return danh sach nhom thiet bi va san pham
	 * @throws BusinessException
	 * @since Sep 17 2015
	 */
	List<EquipmentGroupVO> getListEquipmentGroupVOForExport(EquipmentGroupProductFilter<EquipmentGroupVO> equipmentGroupFilter) throws DataAccessException;

	/**
	 * lay danh sach san pham trong equipment_group
	 * 
	 * @author tuannd20
	 * @param equipmentGroupCode
	 *            ma nhom thiet bi
	 * @param equipmentGroupProductStatus
	 *            trang thai equipmentGroupProduct
	 * @return danh sach san pham trong equipment_group
	 * @param kPaging
	 *            phan trang ket qua
	 * @throws BusinessException
	 * @date 05/01/2015
	 */
	List<EquipmentGroupProductVO> retrieveProductsInEquipmentGroup(String equipmentGroupCode, ActiveType equipmentGroupProductStatus, KPaging<EquipmentGroupProductVO> kPaging) throws DataAccessException;

	/**
	 * tim kiem equipment_group_product
	 * 
	 * @author tuannd20
	 * @param equipmentGroupId
	 *            id equipment group
	 * @param productId
	 *            id s?n ph?m
	 * @param equipmentGroupProductStatus
	 *            trang thai equipment group product
	 * @return equipment_group_product
	 * @throws DataAccessException
	 */
	EquipGroupProduct retrieveEquipmentGroupProduct(Long equipmentGroupId, Long productId, ActiveType equipmentGroupProductStatus) throws DataAccessException;

	/**
	 * tim kiem equipment_group_product
	 * 
	 * @author tuannd20
	 * @param equipmentGroupCode
	 *            ma equipment_group
	 * @param productCode
	 *            ma san pham
	 * @param equipmentGroupProductStatus
	 *            trang thai equipment group product
	 * @return equipment_group_product
	 * @throws DataAccessException
	 */
	EquipGroupProduct retrieveEquipmentGroupProduct(String equipmentGroupCode, String productCode, ActiveType equipmentGroupProductStatus) throws DataAccessException;

	/**
	 * lay danh sach san pham, nganh hang, nganh hang con
	 * 
	 * @author tuannd20
	 * @param productCode
	 *            ma san pham de tim kiem
	 * @param productName
	 *            ten san pham de tim kiem
	 * @param equipmentGroupId
	 *            id cua equipment_group
	 * @param exceptProductsInZCategory
	 *            loai bo san pham nganh hang Z hay ko
	 * @param outProducts
	 *            danh sach san pham tim kiem
	 * @param outCategories
	 *            danh sach nganh hang tim kiem
	 * @param outSubCategories
	 *            danh sach nganh hang con tim kiem
	 * @throws DataAccessException
	 */
	void retrieveProductsAndCategories(String productCode, String productName, Long equipmentGroupId, Boolean exceptProductsInZCategory, List<ProductTreeVO> outProducts, List<ProductTreeVO> outCategories, List<ProductTreeVO> outSubCategories)
			throws DataAccessException;

	/**
	 * Lay danh sach Hop dong
	 * 
	 * @author hunglm16
	 * @since January 07,2015
	 * */
	List<EquipDeliveryRecord> getListEquipDeliveryRecordEntitesByFilter(EquipmentFilter<EquipDeliveryRecord> filter) throws DataAccessException;

	/**
	 * Tao moi chi tiet phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 07/01/2015
	 * @param EquipRepairForm
	 * @throws DataAccessException
	 */
	EquipRepairFormDtl createEquipRepairFormDtl(EquipRepairFormDtl equipRepairFormDtl) throws DataAccessException;

	/**
	 * tim kiem thiet bi theo id
	 * 
	 * @author hoanv25
	 * @param equipmentId
	 *            dieu kien tim kiem thiet bi
	 * @return ket qua tim kiem thiet bi
	 * @throws BusinessException
	 * @date 07/01/2015
	 */
	List<EquipmentVO> getEquipStockTransFormDtlByEquipId(Long equipmentId) throws DataAccessException;

	/**
	 * Xoa thiet bi trong bien ban chuyen kho theo id
	 * 
	 * @author hoanv25
	 * @param equipmentId
	 *            dieu kien tim kiem thiet bi
	 * @return ket qua tim kiem thiet bi
	 * @throws BusinessException
	 * @date 07/01/2015
	 */
	void deleteEquipStockTransFormDtl(EquipStockTransFormDtl id) throws DataAccessException;

	//	 void deleteEquipStockTransFormDtl(Long id);

	/**
	 * Lay danh sach thiet bi theo id bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @param equipmentFilter
	 *            dieu kien tim kiem thiet bi
	 * @return ket qua tim kiem thiet bi
	 * @throws BusinessException
	 * @date 07/01/2015
	 */
	List<EquipmentDeliveryVO> getListEquipmentequipmentStockTransFormVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws DataAccessException;

	/**
	 * Lay danh sach chi tiet bien ban chuyen kho theo id
	 * 
	 * @author hoanv25
	 * @param equipmentFilter
	 *            dieu kien tim kiem thiet bi
	 * @return ket qua tim kiem thiet bi
	 * @throws BusinessException
	 * @date 07/01/2015
	 */
	EquipStockTransFormDtl getEquipStockTransFormDtlById(Long id) throws DataAccessException;

	/**
	 * Cap nhat bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @throws BusinessException
	 * @date 07/01/2015
	 */
	EquipStockTransForm updateEquipmentStockTransForm(EquipStockTransForm equipStockTransForm) throws DataAccessException;

	/**
	 * Xu ly su kien enter ma thiet bi
	 * 
	 * @author hoanv25
	 * @since 08/01/2015
	 * 
	 * @throws DataAccessException
	 */
	List<EquipmentDeliveryVO> getListEquipmentStockTransByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws DataAccessException;

	/**
	 * Lay danh sach chi tiet phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 08/01/2015
	 * @param equipRepairFormId
	 * @throws DataAccessException
	 */
	List<EquipRepairFormDtl> getEquipRepairFormDtlByEquipRepairFormId(Long equipRepairFormId) throws DataAccessException;

	/**
	 * Xoa chi tiet phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 08/01/2015
	 * */
	void deleteEquipRepairFormDtl(EquipRepairFormDtl equipRepairFormDtl) throws DataAccessException;


	/***
	 * Lay EquipRepairPayFormBy Code; lay phieu thanh toan sua chua chinh xac code
	 * @author vuongmq
	 * @date 19/04/2015
	 * @return EquipRepairPayForm
	 */
	EquipRepairPayForm getEquipRepairPayFormByCode(String code) throws DataAccessException;
	
	/**
	 * lay phieu thanh toan sua chua thiet bi theo code
	 * 
	 * @author tuannd20
	 * @param equipmentRepairPaymentRecord
	 *            ma cua phieu thanh toan sua chua thiet bi
	 * @return phieu thanh toan sua chua thiet bi
	 * @throws DataAccessException
	 */
	EquipRepairPayForm retrieveEquipmentRepairPaymentRecord(String equipmentRepairPaymentRecord) throws DataAccessException;

	/**
	 * lay danh sach cac hang muc da thanh toan cua phieu sua chua
	 * 
	 * @author tuannd20
	 * @param equipmentRepairFormId
	 *            id phieu sua chua
	 * @return chi tiet cac hang muc sua chua trong phieu thanh toan
	 * @throws DataAccessException
	 */
	List<EquipRepairPayFormDtl> retrieveEquipmentRepairPaymentDetails(Long equipmentRepairFormId) throws DataAccessException;

	/**
	 * lay phieu thanh toan cua phieu sua chua
	 * 
	 * @author tuannd20
	 * @param equipmentRepairFormId
	 *            id phieu sua chua
	 * @return chi tiet cac hang muc sua chua trong phieu thanh toan
	 * @throws BusinessException
	 */
	EquipRepairPayForm retrieveEquipmentRepairPayment(Long equipmentRepairFormId) throws DataAccessException;

	/**
	 * Kiem tra kho nguon cua thiet bi
	 * 
	 * @author hoanv25
	 * @param equipmentFilter
	 * @return danh sach thiet bi co trong kho nguon
	 * @throws BusinessException
	 */
	List<EquipmentDeliveryVO> getCheckFromStockEquipment(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws DataAccessException;

	/**
	 * lay danh sach KH cua tat ca cac don vi tham gia bien ban kiem ke
	 * 
	 * @author tuannd20
	 * @param equipmentStatisticRecordId
	 *            id cua bien ban kiem ke
	 * @return cac KH trong bien ban kiem ke
	 * @throws DataAccessException
	 */
	List<Customer> retrieveCustomersOfShopsInEquipmentStatisticRecord(Long equipmentStatisticRecordId) throws DataAccessException;

	/**
	 * lay chi tiet cua bien ban kiem ke
	 * 
	 * @author tuannd20
	 * @param equipmentStatisticRecordId
	 *            id cua bien ban kiem ke
	 * @return danh sach detail cua bien ban kiem ke
	 * @throws DataAccessException
	 */
	List<EquipStatisticRecDtl> getEquipmentStatisticRecordDetails(Long equipmentStatisticRecordId) throws DataAccessException;

	/**
	 * Lay EquipItem theo code
	 * 
	 * @author phuongvm
	 * @since 13/01/2015
	 * */
	EquipItem getEquipItemByCode(String code) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi theo id chi tiet bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @param equip
	 * @return danh sach thiet bi
	 * @since 14/01/2015
	 * @throws BusinessException
	 */
	Equipment getEquipmentById(Equipment equip) throws DataAccessException;

	/**
	 * Lay danh sach in bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @param equipmentFilter
	 * @return danh sach in bien ban chuyen kho
	 * @since 14/01/2015
	 * @throws BusinessException
	 */
	List<EquipmentDeliveryVO> searchRecordStockChangeByEquipment(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws DataAccessException;

	/**
	 * Lay so lan sua chua thiet bi
	 * 
	 * @author phuongvm
	 * @since 14/01/2015
	 * */
	BigDecimal getRepairCountEquip(Long equipId) throws DataAccessException;

	/**
	 * Lay so lan sua chua hang muc
	 * 
	 * @author phuongvm
	 * @since 14/01/2015
	 * */
	BigDecimal getRepairCountEquipItem(Long equipItemId, Long equipId) throws DataAccessException;

	/**
	 * Lay danh sach bien ban thu hoi va thanh ly cho export
	 * 
	 * @author tamvnm
	 * @since 12/01/2015
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	List<EquipmentEvictionVO> getListEquipmentEvictionVOForExport(Long long1) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi theo id chi tiet bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @param equip
	 * @return danh sach thiet bi
	 * @since 14/01/2015
	 * @throws BusinessException
	 */
	List<EquipmentVO> getEquipStockTransFormDtlByIdEquip(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi theo id chi tiet bien ban chuyen kho
	 * 
	 * @author hoanv25
	 * @param equip
	 * @return danh sach thiet bi
	 * @since 14/01/2015
	 * @throws BusinessException
	 */
	Equipment getEquipmentByEquipId(Long equipId) throws DataAccessException;

	/**
	 * Lay danh sach bien ban thu hoi va thanh ly cho in
	 * 
	 * @author phuongvm
	 * @since 16/01/2015
	 * @param evictionId
	 * @throws BusinessException
	 */
	List<EquipmentEvictionVO> getListEquipmentEvictionVOForPrint(Long evictionId) throws DataAccessException;

	/**
	 * lay danh sach cac thiet bi bi xoa khoi bien ban
	 * 
	 * @author tuannd20
	 * @param equipStockTransFormId
	 *            id cua bien ban
	 * @param addEquipmentIds
	 *            danh sach id cua cac thiet bi se them vao bien ban
	 * @return danh sach thiet bi se xoa khoi bien ban
	 * @throws DataAccessException
	 * @since 15/01/2015
	 */
	List<Equipment> retrieveRemovedEquipmentFromStockTransRecord(Long equipStockTransFormId, List<Long> addEquipmentIds) throws DataAccessException;

	/**
	 * Lay danh sach chi tiet VO
	 * 
	 * @author nhutnn
	 * @since 16/01/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipmentRepairFormDtlVO> getListEquipRepairDtlVOByEquipRepairId(EquipRepairFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach kho man hinh phieu sua chua
	 * 
	 * @author phuongvm
	 * @since 19/01/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipStockVO> getListStockByFilter(EquipStockFilter filter) throws DataAccessException;

	/**
	 * Lay Equip Stock theo so thu tu  va son vi
	 * @author vuongmq
	 * @date Mar 16,2015
	 * */
	List<EquipStock> getEquipStockByShopAndOrdinal(Long id, Integer stt) throws DataAccessException;
	/**
	 * Lay Equip Stock theo so thu tu kho lon nhat
	 * @author vuongmq
	 * @date Mar 16,2015
	 * */
	Integer getEquipStockOrdinalMaxByShop (Long id)  throws DataAccessException;
	/**
	 * Lay Equip Stock theo code
	 * @author vuongmq
	 * @date Mar 16,2015
	 * */
	EquipStock getEquipStockbyCode(String code)  throws DataAccessException;
	/**
	 * Lay danh sach kho thiet bi
	 * @author vuongmq
	 * @date Mar 16,2015
	 * */
	List<EquipStock> getListEquipmentStockByFilter(EquipmentFilter<EquipStock> filter) throws DataAccessException;
	
	
	/**
	 * Lay so hop dong moi nhat
	 * 
	 * @author hunglm16
	 * @since January 26,2014
	 * */
	String getContractNumberByFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;
	/**
	 * Tim kiem so hop dong moi nhat
	 * 
	 * @author hunglm16
	 * @since January 26,2014
	 * */
	List<EquipmentVO> searchContractNumberByFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;
	
	/**
	 * Tim kiem nhom thiet bi dang hoat dong trong bang equipment
	 * 
	 * @author hoanv25
	 * @since 28/1/2014
	 * */
	List<EquipmentVO> getListCheckEquipGroupById(EquipmentFilter<EquipmentVO> filter)throws DataAccessException;

	/**
	 * Tim kiem nhom thiet bi dang hoat dong trong bang equip_group_product
	 * 
	 * @author hoanv25
	 * @since 28/1/2014
	 * */
	List<EquipmentVO> getListCheckEquipGroupProductByIdEquipGroup(EquipmentFilter<EquipmentVO> filter)throws DataAccessException;
	/**
	 * get equip category by code
	 * 
	 * @author liemtpt
	 * @since 19/03/2015
	 * */
	EquipCategory getEquipCategoryByCode(String code, ActiveType status) throws DataAccessException;
	/**
	 * get equip category by code
	 * 
	 * @author liemtpt
	 * @since 19/03/2015
	 * */
	List<EquipSalePlan> getListEquipSalePlanByFilter(EquipmentSalePlaneFilter<EquipSalePlan> filter) throws DataAccessException;

	/**
	 * Xu ly search shop tree 
	 * @author tamvnm
	 * @param shopId id npp
	 * @param code ma npp
	 * @param name ten npp
	 * @param filter filter
	 * @return list shop tree
	 * @throws DataAccessException
	 * @since Jan 26, 2016
	 */
	List<ShopViewParentVO> searchShopTreeView(Long shopId, String code,	String name, EquipmentFilter<ShopViewParentVO> filter) throws DataAccessException;
	/**
	 * get equip stock vo by filter
	 * 
	 * @author liemtpt
	 * @since 23/03/2015
	 * 
	 * @author hunglm16
	 * @since May 26,2015
	 * @descrription Ra soat va update
	 * */
	List<EquipStockVO> getListEquipStockVOByFilter(EquipStockFilter filter) throws DataAccessException;

	/**
	 * Tao moi Quyen nguoi dung
	 * 
	 * @author tamvnm
	 * @since 24/03/2015
	 * @param 
	 * @throws DataAccessException
	 */
	EquipRoleUser createEquipRoleUser(EquipRoleUser roleUser) throws DataAccessException;;

	/**
	 * Lay danh sach staff cho phan quyen - Thiet bi
	 * 
	 * @author tamvnm
	 * @since 24/03/2015
	 * @param 
	 * @throws DataAccessException
	 */
	List<EquipmentStaffVO> getStaffForPermission(EquipStaffFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach staff cho phan quyen - Thiet bi
	 * Note: lay danh sach staff giong nhu getStaffForPermission o tren; cong them roleCode quyen vao cho nhan vien nua
	 * @author vuongmq
	 * @date 26/03/2015
	 * @param 
	 * @throws DataAccessException
	 */
	List<EquipmentStaffVO> getStaffForPermissionExportExcel(EquipStaffFilter filter) throws DataAccessException;
	/**
	 * Lay danh sach quyen - Gan Quyen Cho Nguoi Dung
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param 
	 * @throws BusinessException
	 */
	List<EquipmentPermissionVO> getPermissionByFilter(EquipPermissionFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach quyen - popup search
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param 
	 * @throws BusinessException
	 */
	List<EquipRole> searchListEquipRoleByFilter(EquipRoleFilter filter) throws DataAccessException;

	/**
	 * Lay EquipRole
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param  id
	 * @throws BusinessException
	 */
	EquipRole getEquipRoleById(Long id) throws DataAccessException;

	/**
	 * Lay Entities EquipRoleUser ung voi filter
	 * @author vuongmq
	 * @since 27/03/2015
	 */
	EquipRoleUser getEquipEquipRoleUserByFilter(EquipPermissionFilter filter)	throws DataAccessException;
	/**
	 * Lay Entities EquipStock ung voi id
	 * @author phuongvm
	 * @since 30/03/2015
	 */
	EquipStock getEquipStockById(Long id) throws DataAccessException;
	/**
	 * cap nhat EquipStockTransFormDtl
	 * @author phuongvm
	 * @since 03/04/2015
	 */
	EquipStockTransFormDtl updateEquipStockTransFormDtl(EquipStockTransFormDtl e)
			throws DataAccessException;
	/**
	 * Ham kiem tra xem dong StockTranFormDtl co ton tai trang thai dang thuc hien (performStatus = OnGoing)
	 * @author phuongvm
	 * @since 04/04/2015
	 */
	Boolean checkExistStockTranFormDtlOnGoing(Long stockTranFormId)
			throws DataAccessException;
	/**
	 * Ham lay danh sach StockTranFormDtl co ton tai trang thai dang thuc hien (performStatus = OnGoing)
	 * @author phuongvm
	 * @since 04/04/2015
	 */
	List<EquipStockTransFormDtl> getListEquipStockTransFormDtlOnGoing(
			Long stockTranFormId) throws DataAccessException;
	/**
	 * Ham lay danh sach StockTranFormDtl 
	 * @author phuongvm
	 * @since 04/04/2015
	 */
	List<EquipStockTransFormDtl> getListEquipStockTransFormDtlById(
			Long stockTranFormId) throws DataAccessException;
	/**
	 * Ham lay danh sach StockTranFormDtl xuat excel
	 * @author phuongvm
	 * @since 06/04/2015
	 */
	List<EquipmentVO> searchEquipmentStockChangeExcel(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;
	
	/**
	 * get list customer in record and shop
	 * @author phut
	 */
	List<Customer> getListCustomerByShop(Long recordId) throws DataAccessException;

	/**
	 * get list customer in record by promotion_code
	 * @author phut
	 */
	List<Customer> getListCustomerBuTCHTTM(Long recordId) throws DataAccessException;
	/**
	 * Lay danh sach thiet bi cho muon
	 * 
	 * @author tamvnm
	 * @since 09/04/2015
	 * */
	List<EquipLendDetail> getListEquipmentLendDetailByLendId(Long equipLendId) throws DataAccessException;

	/**
	 * get list equip item config by filter
	 * @author liemtpt
	 * @since 08/04/2015
	 * @param EquipItemConfigFilter
	 * @throws DataAccessException
	 * @description  Lay danh sach dinh muc
	 */
	List<EquipItemConfigVO> getListEquipItemConfigByFilter(EquipItemConfigFilter filter) throws DataAccessException;
	/**
	 * get list equip item config detail by filter
	 * @author liemtpt
	 * @since 07/04/2015
	 * @param EquipItemConfigFilter
	 * @throws DataAccessException
	 * @description  Lay danh sach hang muc
	 */
	List<EquipItemConfigDtlVO> getListEquipItemConfigDetailByFilter(EquipItemConfigDtlFilter filter) throws DataAccessException;
	
	/**
	 * get code equip item config
	 * @author liemtpt
	 * @since 13/04/2015
	 * @param 
	 * @throws DataAccessException
	 * @description 
	 */
	String getCodeEquipItemConfig() throws DataAccessException;

	/**
	 * @author vuongmq
	 * @since 14/04/2015
	 * get list equip item config dtl by filter
	 * @author liemtpt
	 * @since 15/04/2015
	 * @param EquipItemConfigDtlFilter
	 * @throws DataAccessException
	 * @description lay danh sach hang muc 
	 */
	List<EquipItemConfigDtl> getListEquipItemConfigDtlByFilter(EquipItemConfigDtlFilter filter) throws DataAccessException;
	
	/**
	 * get equip item by code
	 * @author liemtpt
	 * @since 15/04/2015
	 * @param code
	 * @param status
	 * @throws DataAccessException
	 * @description lay dinh muc theo code 
	 */
	EquipItem getEquipItemByCode(String code, ActiveType status) throws DataAccessException;
	
	/**
	 * get list check exist crossing
	 * @author liemtpt
	 * @since 15/04/2015
	 * @param EquipItemConfigFilter
	 * @throws DataAccessException
	 * @description Check xem dung tich tu va den co trung chom du lieu trong DB 
	 */
	List<EquipItemConfig> getListCheckExistCrossing(EquipItemConfigFilter filter) throws DataAccessException;
	
	/**
	 * get list equip item config detail by filter
	 * @author liemtpt
	 * @since 07/04/2015
	 * @param EquipItemConfigFilter
	 * @throws DataAccessException
	 * @description  Lay hang muc cau hinh
	 */
	EquipItemConfigDtl getEquipItemConfigDtl(EquipItemFilter filter) throws DataAccessException;

	/***
	 * @author vuongmq
	 * @date 15/04/2015
	 * Lấy doanh so theo nganh hang cua Ql sua chua
	 */
	Object getDoanhSoEquipRepair(EquipRepairFilter filter)	throws DataAccessException;
	
	/**
	 * Lay danh sach hop dong 
	 * 
	 * @author tamvnm
	 * @since 14/04/2015
	 * @params: equipmentFilter.numberContract
	 * */
	List<EquipDeliveryRecord> getListDeliveryRecordByFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach hop dong cho export excel
	 * 
	 * @author tamvnm
	 * @since 14/04/2015
	 * @params: EquipmentFilter
	 * */
	List<EquipmentRecordDeliveryVO> getListDeliveryRecordForExport(EquipmentFilter<EquipmentRecordDeliveryVO> equipmentFilter) throws DataAccessException;


	/**
	 * get map customer exists in record
	 * @author phut
	 */
	Map<Long, EquipStatisticCustomer> getListExistsCustomer(Long recordId, List<Customer> list) throws DataAccessException;

	/**
	 * get list shop child in record
	 * @author phut
	 */
	List<Shop> retrieveShopsOfShopsInEquipmentStatisticRecord(Long equipmentStatisticRecordId) throws DataAccessException;

	/**
	 * Lay bien ban de nghi muon thiet bi, voi tat ca cac bien bang giao nhan va hop dong Da Duyet hoac Huy
	 * 
	 * @author tamvnm
	 * @since 15/04/2015
	 * @params: id bien ban
	 * */
	EquipLend getEquipLendById(Long id)  throws DataAccessException;
	/**
	 * lay danh sach pop up don vi
	 * 
	 * @author hoanv25
	 * @since 16/04/2015
	 * @params: shop_id
	 * */
	List<ShopViewParentVO> searchShopTreeCustomer(Long shopId, String code, String name) throws DataAccessException;
    /***
     * get equip item config by filter
     * @param EquipItemConfigFilter
     * @return
     * @throws DataAccessException
     */
	EquipItemConfig getEquipItemConfigByFilter(EquipItemConfigFilter filter) throws DataAccessException;
	/**
	 * Lay danh sach cac ky da dong ma co from_Date > filter.fromdate truyen vao 
	 * 
	 * @author phuongvm
	 * @since 20/04/2015
	 * @params: EquipmentFilter filter 
	 * */
	List<EquipPeriod> getListEquipPeriodClosedByFromDate(EquipmentFilter<EquipPeriod> filter) throws DataAccessException;

	/**
	 * Lay danh sach chi tiet bien ban de nghi thu hoi
	 * 
	 * @author tamvnm
	 * @since 04/05/2015
	 * @params: id equipSuggestEviction, id shop
	 * */
	List<EquipSuggestEvictionDTL> getListEquipSugEvictionDTL(Long id, Long shopId) throws DataAccessException;
	
	/**
	 * Lay danh sach bien ban giao nhan 
	 * 
	 * @author tamvnm
	 * @since 22/04/2015
	 * @params: list id bien ban giao nhan
	 * */
	List<EquipmentDeliveryPrintVO> getListEquipDeliveryRecordForPrint(List<Long> lstId) throws DataAccessException;

	/**
	 * Lay stock code - name ben EquipStock voi stock_id; cua kho don vi
	 * @author vuongmq
	 * @since 23/04/2015
	 * @return
	 */
	EquipStock getEquipStockByFilter(EquipStockEquipFilter<EquipStock> filter) throws DataAccessException;
	
	/**
	 * Lay danh sach Muc doanh so theo nhom thiet bi
	 * 
	 * @param equipGroupId la Id trong Equip_Group
	 * 
	 * @author hunglm16
	 * @since May 03,2015
	 * */
	List<EquipSalePlan> getListEquipSalePlanByEquipGroup(Long equipGroupId) throws DataAccessException;
	
	/**
	 * Kiem tra tinh chung chom Dung tich
	 * 
	 * @param fCapacity: dung tich tu
	 * @param tCapacity: dung tich den
	 * @param status: tinh trang
	 * @param longsException[]: danh sach Id loai tru khong muon kiem tra
	 * 
	 * @author hunglm16
	 * @since May 04,2015
	 * */
	Integer countIntersectionCapacity(Integer fCapacity, Integer tCapacity, Integer status, Long[] longsException) throws DataAccessException;

	/**
	 * Lay doi tuong dau tien EquipItemConfig theo Dung tich
	 * 
	 * @param fCapacity: dung tich tu
	 * @param tCapacity: dung tich den
	 * @param status: tinh trang
	 * 
	 * @author hunglm16
	 * @since May 04,2015
	 * */
	EquipItemConfig getEquipItemConfigByFromAndToCapacity(Integer fCapacity, Integer tCapacity, Integer status) throws DataAccessException;
	
	/**
	 * Lay doi tuong dau tien EquipItemConfig theo Id
	 * 
	 * @param equipItemConfigId: Ma ID
	 * 
	 * @author hunglm16
	 * @since May 04,2015
	 * */
	List<EquipItemConfigDtl> getListEquipItemConfigDtlByEquipItemConfig(Long equipItemConfigId, Integer status) throws DataAccessException;
	
	/**
	 * Xuat excel Thiet lap dinh muc hang muc
	 * 
	 * @param equipItemConfigId: Ma ID
	 * 
	 * @author hunglm16
	 * @since May 04,2015
	 * */
	List<EquipItemConfigVO> exportListEquipItemConfigByFilter(EquipItemConfigFilter filter) throws DataAccessException;

	/**
	 * Lay kho thiet bi theo phan quyen
	 * 
	 * @author tamvnm
	 * @since 05/05/2015
	 * @params: Ma kho
	 * */
	List<EquipStockVO> getEquipStockCMSbyCode(EquipStockFilter filter) throws DataAccessException;
	
	/**
	 * Lay danh sach ky trong nam de in bao cao
	 * 
	 * @author hoanv25
	 * @since May 05,2015
	 * @param filter
	 * @throws DataAccessException
	 * @description
	 */
	List<EquipmentVO> getListEquipmentPeriod(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach ky theo Id
	 * @author hoanv25
	 * @since May 05,2015
	 * @param filter
	 * @throws DataAccessException
	 * @description 
	 */
	EquipPeriod getListEquipPeriodById(Long id) throws DataAccessException;
	/**
	 * Lay danh sach ky khi thay doi nam
	 * @author hoanv25
	 * @since May 05,2015
	 * @param filter
	 * @throws DataAccessException
	 * @description 
	 */
	List<EquipmentVO> getListEquipmentPeriodChangeYear(EquipmentFilter<EquipmentVO> filter)throws DataAccessException;

	/**
	 * Lay tat ca hang muc voi Dinh muc
	 * Nhung hang muc co dinh muc tu fill thong tin
	 * Nhung hang muc khac chua co de thong tin trong chi tiet hang muc rong
	 * 
	 * @param equipItemConfigId: Id Dinh Muc
	 * 
	 * @author hunglm16
	 * @since May 07, 2015
	 * */
	List<EquipItemConfigDtlVO> getEquipItemInDetailByEquipItemConfig(Long equipItemConfigId) throws DataAccessException;


	/**
	 * Chọn một thiết bị ở trạng thái ko giao dịch và khóa nó lại
	 * 
	 * @author tamvnm
	 * @since 08/05/2015
	 * @param LongID
	 * @throws DataAccessException
	 */
	Equipment getEquipmentByIdAndBlock(EquipItemFilter filter) throws DataAccessException;


	/***
	 * @author vuongmq
	 * @date 11/05/2015
	 * lay danh sách kho cap tren cua Thiet bi ung voi tung loai 1: NPP; 2: KH
	 */
	List<EquipStock> getListEquipStockParentByFilter(EquipStockEquipFilter<EquipStock> filter) throws DataAccessException;

	/***
	 * @author vuongmq
	 * @date 11/05/2015
	 * lay danh sách kho cap tren cua Thiet bi ung voi tung loai 1: NPP; 2: KH ; IF getListEquipStockParentByFilter() KHONG CO DU LIEU
	 */
	List<EquipStock> getListEquipStockParentSameLevelByFilter(EquipStockEquipFilter<EquipStock> filter) throws DataAccessException;
	
	/***
	 * @author tamvnm
	 * @date 21/07/2015
	 * Lay danh sach nhom thiet bi status = 1
	 */
	List<EquipCategory> getListCategory() throws DataAccessException;
	
	/**
	 * Lay danh hang muc sua chua
	 * 
	 * @author hoanv25
	 * @since May 13,2015
	 * @param statisticRecord
	 * @throws DataAccessException
	 * @description
	 */
	List<EquipmentVO> getListCategoryCount(Long statisticRecord) throws DataAccessException;
	/**
	 * Kiểm tra tồn tại thiết bị trong phiếu chuyển kho
	 * 
	 * @author phuongvm
	 * @since 13/05/2015
	 * @params: stockTranFormId
	 * @params: equipId
	 * */
	Boolean checkExistStockTranFormDtlByEquip(Long stockTranFormId, Long equipId) throws DataAccessException;

	/**
	 * Lay danh sach nhom thiet bi theo Filter
	 * 
	 * @author hunglm16
	 * @since May 18, 2015
	 * */
	List<EquipmentGroupVO> getListEquipGroupForFilter(EquipGFilter<EquipmentGroupVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach chi tiet thiet bi 
	 * 
	 * @author tamvnm
	 * @since 18/05/2015
	 * */
	List<EquipDeliveryRecDtl> getListDeliveryDetailByRecordId(Long equipDeliveryId) throws DataAccessException;

	/**
	 * Lay danh sach san pham, nhom thiet bi
	 * 
	 * @author hunglm16
	 * @since May 20,2015
	 * */
	List<EquipGroupProduct> getListEquipGroupProductByFilter(EquipmentGroupProductFilter<EquipGroupProduct> filter) throws DataAccessException;

	/**
	 * Lay danh sac kiem ke theo ky
	 * 
	 * @author hoanv25
	 * @since May 20,2015
	 * @param
	 * @throws DataAccessException
	 * @description
	 */
	List<EquipmentVO> getListEquipmentStatisticPeriod(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;
	/**
	 * Kiem tra chung chom tu ngay den ngay tren tung san pham trong nhom thiet bi
	 * 
	 * @author hunglm16
	 * @since May 22,2015
	 * 
	 * @param equipGroupId -> Id nhom thiet bi
	 * @param productCode -> Ma san pham
	 * @param fDateStr -> Ngay bat dau
	 * @param tDateStr -> Ngay ket thuc
	 * @param flag -> 1: Truong hop chung chom; 2: Truong hop toDate in DB is null
	 * */
	int checkFromDateToDateInEquipGroupProduct(Long equipGroupId, String productCode, String fDateStr, String tDateStr, int flag) throws DataAccessException;
	/**
	 * Lay san pham nhom thiet bi
	 * 
	 * @author hunglm16
	 * @since May 22,2015
	 * 
	 * @param flagToDateIsNull -> true: lay voi dieu kien to_date is null
	 * */
	EquipGroupProduct getPrdInEquipGroupPrdByFlagToDate(Long equipGroupId, String productCode, Date fromDate, Boolean flagToDateIsNull) throws DataAccessException;

	/***
	 * @author vuongmq
	 * Lay danh sach hop dong giao nhan moi nhat cua thiet bi
	 * @date 20/05/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * ------------ chua khai bao trong Mgr
	 */
	EquipDeliveryRecord getEquipDeliveryRecordNewOfEquipmentByFilter(	EquipmentFilter<EquipDeliveryRecord> filter) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi o kho phan quyen
	 * 
	 * @author tamvnm
	 * @since 14/04/2015
	 * @params: filter
	 * */
	List<EquipmentDeliveryVO> getListEquipmentByRole(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws DataAccessException;

	/**
	 * Lay danh sach cac thiet bi
	 * 
	 * @author tamvnm
	 * @since 22/05/2015
	 * @param kPaging
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	List<EquipStockVO> getListEquipStockVOByRole(EquipStockFilter filter) throws DataAccessException;
	/**
	 * Lay san pham nhom thiet bi co trong tuong lai so voi ngay hien tai
	 * 
	 * @author hunglm16
	 * @since May 22,2015
	 * 
	 * @param flagNextSysdate -> true: lay voi dieu kien from_date > sysdate
	 * */
	List<EquipGroupProduct> getPrdInEquipGroupPrdByFdateForDel(Long equipGroupId, String productCode, Date fromDate, Boolean flagNextSysdate) throws DataAccessException;
	/**
	 * Lay san pham nhom thiet bi co khoang giao from_date < fDate < to_date
	 * 
	 * @author hunglm16
	 * @since May 22,2015
	 * 
	 * @param flagNextSysdate -> true: lay voi dieu kien from_date > sysdate
	 * */
	List<EquipGroupProduct> getPrdInEquipGroupPrdByFdateForUpdateToDate(Long equipGroupId, String productCode, Date fromDate) throws DataAccessException;
	/**
	 * Lay danh sach cac thiet bi theo phan quyen kho cms
	 * 
	 * @author phuongvm
	 * @since 26/05/2015
	 * @param equipCode
	 * @param staffRootId
	 * @param shopRootId
	 * @throws BusinessException
	 */
	Equipment getEquipmentByCodeHasPermission(String code, Long staffRootId, Long shopRootId) throws DataAccessException;
	/**
	 * dem so luong kho theo phan quyen kho
	 * 
	 * @author hunglm16
	 * @since May 26, 2015
	 * 
	 * @param staffRootId: id Nhan vien quyen thay the
	 * @param shopRootId: id don vi tuong tac he thong
	 * @param stockCode: ma kho
	 * */
	int countStockEquipForPermisionCSM(Long staffRootId, Long shopRootId, String stockCode) throws DataAccessException;

	/**
	 * Lay danh sach chi tiet thiet bi 
	 * 
	 * @author tamvnm
	 * @since 18/05/2015
	 * @param equip suggest eviction id, status
	 * */
	List<EquipSuggestEvictionDTL> getListSuggestDetailByRecordId(Long equipSuggestId, Integer status) throws DataAccessException;
	/**
	 * get equip stock vo by filter for equipstocktrans
	 * 
	 * @author phuongvm
	 * @since 28/05/2015
	 * */
	List<EquipStockVO> getListEquipStockVOByFilterForStockTrans(EquipStockFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi
	 * 
	 * @author hunglm16
	 * @since May 27,2015
	 * @description ban chat gion getListEquipmentByFilter, toi uu hoa lai cau truy van
	 * */
	List<EquipmentVO> getListEquipmentByFilterNew(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;
	
	/**
	 * Lay danh sach thiet bi co an theo phan quyen CMS
	 * 
	 * @author hunglm16
	 * @since May 27,2015
	 * @description filter.getShopRoot() is not null
	 * */
	List<EquipmentManagerVO> getListEquipmentVOByShopInCMS(EquipmentFilter<EquipmentManagerVO> filter) throws DataAccessException;
	/**
	 * Lay danh sach khach hang theo Don vi va nhom san pham
	 * 
	 * @author hunglm16
	 * @since June 15, 2015
	 */
	List<Customer> getListCustomerByEquipStatisticRecordId(Long recordId) throws DataAccessException;
	/**
	 * Lay danh sach kh theo u ke va CT HTTM
	 * 
	 * @author hunglm16
	 * @sin June 15,2015
	 */
	List<Customer> getListCustomerByEquipSttRecIdbyCTHTTM(Long recordId) throws DataAccessException;
	/**
	 * Lay danh sach khach hang theo Import tring kiem ke thiet bi
	 * 
	 * @author hunglm16
	 * @since June 15, 2015
	 */
	List<EquipStatisticCustomer> getListCusInImportEquipStatisticCus(Long recordId, Integer status) throws DataAccessException;

	/**
	 * Lay danh sach phieu sua chua; dung cho tao moi ds chi tiet cua phieu thanh toan
	 * @author vuongmq
	 * @since 24/06/2015
	 * @return
	 */
	List<EquipRepairForm> getListEquipRepairFormFilter(EquipRepairFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach chi tiet cua phieu thanh toan;
	 * @author vuongmq
	 * @since 24/06/2015
	 * @return
	 */
	List<EquipRepairPayFormDtl> getEquipmentRepairPaymentDtlFilter(	EquipRepairFilter filter) throws DataAccessException;

	/**
	 * Lay ky dang mo co ngay thuoc d
	 * 
	 * @author tamvnm
	 * @since 30/06/2015
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipPeriod> getListEquipPeriodByDate(Date d) throws DataAccessException;
	/**
	 * Lay danh sach nhom thiet bi theo Filter
	 * Danh cho bao cao Thiet bi
	 * 
	 * @author hunglm16
	 * @since July 10, 2015
	 * */
	List<EquipmentGroupVO> getListEquipGroupForFilterByRPT(EquipGFilter<EquipmentGroupVO> filter) throws DataAccessException;

	/**
	 * Su dung cho combobox Bao cao Thiet Bi
	 * 
	 * */
	List<EquipmentManagerVO> getListEquipmentVOByShopInCMSEx(EquipmentFilter<EquipmentManagerVO> filter) throws DataAccessException;
	
	/**
	 * Lay danh sach bien ban giao nha
	 * @author nhutnn
	 * @since 23/07/2015
	 * @param filter
	 * @return
	 */
	List<EquipmentRecordDeliveryVO> getEquipmentRecordDeliveryVOByFilter(EquipmentFilter<EquipmentRecordDeliveryVO> filter) throws DataAccessException;
	
	/**
	 * Lay equipFormHistory theo recordId
	 * 
	 * @author: agile_dungdq3
	 * @return: EquipFormHistory
	 * @throws:
	 * @since: 5:02:31 PM Mar 1, 2016
	 */
	EquipFormHistory getEquipFormHistory(Long recordId) throws DataAccessException;
}

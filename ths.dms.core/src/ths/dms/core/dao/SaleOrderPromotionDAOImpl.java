package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SaleOrderPromotion;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.filter.SaleOrderPromotionFilter;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class SaleOrderPromotionDAOImpl implements SaleOrderPromotionDAO {
	@Autowired
	IRepository repo;
	
	@Autowired
	CommonDAO commonDAO;

	@Override
	public List<SaleOrderPromotion> getSaleOrderPromotions(
			SaleOrderPromotionFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select * ");
		sql.append(" from sale_order_promotion sop ");
		sql.append(" where 1 = 1 ");
		if (filter.getSaleOrderId() != null) {
			sql.append(" and sop.sale_order_id = ? ");
			params.add(filter.getSaleOrderId());
		}
		
		return repo.getListBySQL(SaleOrderPromotion.class, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<OrderProductVO> getOrderNotEnoughPortion(SoFilter filter) throws DataAccessException {
		if (filter == null || filter.getLstSaleOrderId() == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		Date lockDate = filter.getLockDay();
		if (lockDate == null) {
			lockDate = commonDAO.getSysDate();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("with lstPromotion as (");
		sql.append(" select sale_order_id, promotion_program_id,");
		sql.append("(nvl(sum(quantity_received), 0)");
		/*sql.append(" + (select nvl(sum(quantity_received), 0) from sale_order_promotion");
		sql.append(" where sale_order_id in (-1");
		if (filter.getLstPassOrderId() != null && filter.getLstPassOrderId().size() > 0) {
			for (Long idt : filter.getLstPassOrderId()) {
				sql.append(",?");
				params.add(idt);
			}
		}
		sql.append(")");
		sql.append(" and promotion_program_id = sop.promotion_program_id");
		sql.append(" )");*/
		sql.append(" )	as quantity_received");
		sql.append(" from sale_order_promotion sop");
		/*sql.append(" where sale_order_id = ?");
		params.add(filter.getSaleOrderId());*/
		sql.append(" where sale_order_id in (-1");
		for (Long idt : filter.getLstSaleOrderId()) {
			sql.append(",?");
			params.add(idt);
		}
		sql.append(")");
		sql.append(" group by sale_order_id, promotion_program_id");
		sql.append(" )");
		sql.append(" select so.sale_order_id as saleOrderId, so.order_number as orderNumber");
		sql.append(" from lstPromotion sop");
		sql.append(" join sale_order so on (so.sale_order_id = sop.sale_order_id and so.order_type in (?))");
		params.add(OrderType.IN.getValue());
		sql.append(" where exists (");
		sql.append(" select 1 from promotion_shop_map psm");
		sql.append(" where promotion_program_id = sop.promotion_program_id");
		sql.append(" and shop_id = so.shop_id and status = ? and from_date < trunc(?)+1");
		params.add(ActiveType.RUNNING.getValue());
		params.add(lockDate);
		sql.append(" and (to_date is null or to_date >= trunc(?))");
		params.add(lockDate);
		sql.append(" and (");
		sql.append(" (quantity_max is not null and quantity_max - nvl(quantity_received, 0) < sop.quantity_received)");
		sql.append(" or exists (");
		sql.append(" select 1 from promotion_customer_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and customer_id = so.customer_id");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and (quantity_max is not null and quantity_max - nvl(quantity_received, 0) < sop.quantity_received)");
		sql.append(" )");
		sql.append(" or exists (");
		sql.append(" select 1 from promotion_staff_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and staff_id = so.staff_id");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and (quantity_max is not null and quantity_max - nvl(quantity_received, 0) < sop.quantity_received)");
		sql.append(" )");
		sql.append(" )");
		sql.append(" )");
		sql.append(" group by so.sale_order_id, so.order_number");
		
		String[] fieldNames = {
				"saleOrderId", "orderNumber"
		};
		Type[] fieldTypes = {
				StandardBasicTypes.LONG, StandardBasicTypes.STRING
		};
		
		List<OrderProductVO> lst = repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

}

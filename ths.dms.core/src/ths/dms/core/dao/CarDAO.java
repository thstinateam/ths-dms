package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Car;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.CarSOFilter;
import ths.dms.core.entities.vo.CarSOVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface CarDAO {

	Car createCar(Car car, LogInfoVO logInfo) throws DataAccessException;

	void deleteCar(Car car, LogInfoVO logInfo) throws DataAccessException;

	void updateCar(Car car, LogInfoVO logInfo) throws DataAccessException;
	
	Car getCarById(long id) throws DataAccessException;

	Boolean isUsingByOthers(long carId) throws DataAccessException;
	
	Car getCarByNumberAndListShop(String carNumber, String lstShopId)	throws DataAccessException;

	List<Car> getListCar(KPaging<Car> kPaging, Long shopId, String type,
			String carNumber, Long labelId, Float gross, String category,
			String origin, ActiveType status, Boolean getCarInChildShop)
			throws DataAccessException;

	Boolean checkIfRecordExists(String carNumber, Long id) throws DataAccessException;
	
	Car getCarByNumberAndShopId(String carNumber, Long shopId) throws DataAccessException;
	
	/**
	 * Lay ds xe
	 * 
	 * @author lacnv1
	 * @since Sep 22, 2014
	 */
	List<CarSOVO> getListCar(CarSOFilter filter) throws DataAccessException;
}

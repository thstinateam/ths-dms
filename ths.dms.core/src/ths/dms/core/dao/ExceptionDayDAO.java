package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.ExceptionDay;
import ths.dms.core.entities.WorkingDateConfig;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.WorkingDateConfigFilter;
import ths.dms.core.entities.enumtype.WorkingDateProcedureType;
import ths.dms.core.entities.enumtype.WorkingDateType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.WorkingDateConfigVO;
import ths.dms.core.exceptions.DataAccessException;

// TODO: Auto-generated Javadoc
/**
 * The Interface ExceptionDayDAO.
 */
public interface ExceptionDayDAO {

	/**
	 * Creates the exception day.
	 *
	 * @param exceptionDay the exception day
	 * @param logInfo the log info
	 * @return the exception day
	 * @throws DataAccessException the data access exception
	 */
	ExceptionDay createExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo)
			throws DataAccessException;

	/**
	 * Delete exception day.
	 *
	 * @param exceptionDay the exception day
	 * @param logInfo the log info
	 * @throws DataAccessException the data access exception
	 */
	void deleteExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo)
			throws DataAccessException;

	/**
	 * Gets the exception day by year.
	 *
	 * @param year the year
	 * @return the exception day by year
	 * @throws DataAccessException the data access exception
	 */
	List<ExceptionDay> getExceptionDayByYear(int year)
			throws DataAccessException;
	
	/**
	 * Update exception day.
	 *
	 * @param exceptionDay the exception day
	 * @param logInfo the log info
	 * @throws DataAccessException the data access exception
	 */
	void updateExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo)
			throws DataAccessException;

	/**
	 * Gets the exception day by month.
	 *
	 * @param month the month
	 * @param year the year
	 * @return the exception day by month
	 * @throws DataAccessException the data access exception
	 */
	List<ExceptionDay> getExceptionDayByMonth(int month, int year)
			throws DataAccessException;
	
	/**
	 * Gets the exception day by id.
	 *
	 * @param id the id
	 * @return the exception day by id
	 * @throws DataAccessException the data access exception
	 */
	ExceptionDay getExceptionDayById(long id) throws DataAccessException;

	/**
	 * Gets the count day off between.
	 *
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the count day off between
	 * @throws DataAccessException the data access exception
	 */
	Integer getCountDayOffBetween(Date fromDate, Date toDate)
			throws DataAccessException;

	/**
	 * Gets the exception day by date.
	 *
	 * @param date the date
	 * @param shopId the shop id
	 * @return the exception day by date
	 * @throws DataAccessException the data access exception
	 */
	ExceptionDay getExceptionDayByDate(Date date,Long shopId) throws DataAccessException;

	/**
	 * Gets the exception day by date.
	 *
	 * @param month the month
	 * @param year the year
	 * @param date the date
	 * @return the exception day by date
	 * @throws DataAccessException the data access exception
	 */
	ExceptionDay getExceptionDayByDate(int month, int year, int date)
			throws DataAccessException;
	
	/**
	 * Gets the exception day by month.
	 *
	 * @param month the month
	 * @param year the year
	 * @param shopId the shop id
	 * @return the exception day by month
	 * @throws DataAccessException the data access exception
	 */
	List<ExceptionDay> getExceptionDayByMonth(int month, int year,Long shopId)
			throws DataAccessException;

	/**
	 * Gets the exception day by year.
	 *
	 * @param year the year
	 * @param shopId the shop id
	 * @return the exception day by year
	 * @throws DataAccessException the data access exception
	 */
	List<ExceptionDay> getExceptionDayByYear(int year,Long shopId)
			throws DataAccessException;
	
	/**
	 * Gets the num exception by date in month.
	 *
	 * @param toDate the to date
	 * @return the num exception by date in month
	 * @throws DataAccessException the data access exception
	 */
	Integer getNumExceptionByDateInMonth(Date toDate)
			throws DataAccessException;

	/**
	 * Gets the lst exception day.
	 *
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the lst exception day
	 * @throws DataAccessException the data access exception
	 */
	List<ExceptionDay> getLstExceptionDay(Date fromDate, Date toDate)
			throws DataAccessException;

	/**
	 * Gets the num of holiday.
	 *
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the num of holiday
	 * @throws DataAccessException the data access exception
	 */
	Integer getNumOfHoliday(Date fromDate, Date toDate)
			throws DataAccessException;

	/**
	 * Gets the exception day for supervise.
	 *
	 * @return the exception day for supervise
	 * @throws DataAccessException the data access exception
	 */
	Integer getExceptionDayForSupervise() throws DataAccessException;
	
	/**
	 * Lay so ngay nghi cua luy ke tieu thu 10 ngay gan nhat cua BC F1.
	 *
	 * @param fDate the f date
	 * @param tDate the t date
	 * @return the num exception day f1
	 * @throws DataAccessException the data access exception
	 * @author vuonghn
	 */
	Integer getNumExceptionDayF1(Date fDate, Date tDate) throws DataAccessException;

	/**
	 * Gets the count ex day off between.
	 *
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the count ex day off between
	 * @throws DataAccessException the data access exception
	 */
	Integer getCountExDayOffBetween(Date fromDate, Date toDate)
			throws DataAccessException;
	
	
	/**
	 * *************BEGIN VUONGMQ *****.
	 *
	 * @param shopId the shop id
	 * @param active the active
	 * @return the work date config by shop id
	 * @throws DataAccessException the data access exception
	 */
	/**
	 * @author vuongmq
	 * @date 30/01/2015
	 * @description: lay work config
	 * chua khai bao o Mgr
	 */
	WorkingDateConfig getWorkDateConfigByShopId(Long shopId, ActiveType active) throws DataAccessException;
	
	/**
	 * @author vuongmq
	 * @date 30/01/2015
	 * @description: lay work config
	 * chua khai bao o Mgr
	 */
	List<ExceptionDay> getListByShopIdAndFromDateToDate(Long shopId, Date fromMonth, Date toMonth) throws DataAccessException;
	
	/**
	 * @author vuongmq
	 * @date 30/01/2015
	 * @description: lay work config VO
	 * chua khai bao o Mgr
	 */
	/*List<WorkingDateConfigVO> getListWorkingDateConfigVOByFilter(
			WorkingDateConfigFilter filter) throws DataAccessException;
	*/
	
	/**
	 * @author vuongmq
	 * @date 30/01/2015
	 * @description: lay work config VO check parent
	 */
	 List<WorkingDateConfigVO> getWorkDateConfigDateCheckParent(Long shopId, WorkingDateType type, ActiveType active) throws DataAccessException;
	
	/***************END VUONGMQ ******/
	
	/**
	 * get list working date config by filter
	 * @author liemtpt
	 * @date 30/01/2015
	 * @description: 
	 */
	List<WorkingDateConfig> getListWorkingDateConfigByFilter(
			WorkingDateConfigFilter filter) throws DataAccessException;
	/**
	 * call proceduce working date 
	 * @author liemtpt
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param type
	 * @date 30/01/2015
	 * @description: 
	 */
	void callProceduceWorkingDate(Long shopId, Date fromDate, Date toDate,
			WorkingDateProcedureType type) throws DataAccessException;

	/**
	 * Gets the num of holiday by shop
	 *
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @param shopId
	 * @return the num of holiday of shop
	 * @throws DataAccessException the data access exception
	 */
	Integer getNumOfHoliday(Date fromDate, Date toDate, Long shopId) throws DataAccessException;

	/**
	 * Gets the num of holiday by shop
	 * 
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param shopId
	 * @return the num of holiday of shop
	 * @throws DataAccessException the data access exception
	 */
	List<ExceptionDay> getListByShopIdAndFromDateToDate(Long shopIdCopy, String fromMonth) throws DataAccessException;	
}

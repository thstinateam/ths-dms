package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Attribute;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.TableHasAttribute;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class BK_AttributeDAOImpl implements BK_AttributeDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;

	@Override
	public Attribute createAttribute(Attribute attribute, LogInfoVO logInfo)
			throws DataAccessException {
		if (attribute == null) {
			throw new IllegalArgumentException("attribute");
		}
		if (!StringUtility.isNullOrEmpty(attribute.getAttributeCode())) {
			attribute.setAttributeCode(attribute.getAttributeCode()
					.toUpperCase());
		}
		attribute.setCreateUser(logInfo.getStaffCode());
		repo.create(attribute);
		try {
			actionGeneralLogDAO
					.createActionGeneralLog(null, attribute, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(Attribute.class, attribute.getId());
	}

	@Override
	public void deleteAttribute(Attribute attribute, LogInfoVO logInfo)
			throws DataAccessException {
		if (attribute == null) {
			throw new IllegalArgumentException("attribute");
		}
		repo.delete(attribute);
		try {
			actionGeneralLogDAO
					.createActionGeneralLog(attribute, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Attribute getAttributeById(long id) throws DataAccessException {
		return repo.getEntityById(Attribute.class, id);
	}

	@Override
	public void updateAttribute(Attribute attribute, LogInfoVO logInfo)
			throws DataAccessException {
		if (attribute == null) {
			throw new IllegalArgumentException("attribute");
		}
		if (!StringUtility.isNullOrEmpty(attribute.getAttributeCode())) {
			attribute.setAttributeCode(attribute.getAttributeCode()
					.toUpperCase());
		}

		Attribute temp = this.getAttributeById(attribute.getId());
		attribute.setUpdateDate(commonDAO.getSysDate());
		attribute.setUpdateUser(logInfo.getStaffCode());
		repo.update(attribute);
		try {
			actionGeneralLogDAO
					.createActionGeneralLog(temp, attribute, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Attribute getAttributeByCode(TableHasAttribute table, String code)
			throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from ATTRIBUTE where ATTRIBUTE_CODE=? and STATUS!=? "
				+ " and TABLE_NAME = ? ";
		List<Object> params = new ArrayList<Object>();

		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		params.add(table.getTableName());

		Attribute attribute = repo.getEntityBySQL(Attribute.class, sql, params);
		return attribute;
	}

	@Override
	public List<Attribute> getListAttribute(KPaging<Attribute> kPaging,
			TableHasAttribute table, String attributeCode,
			String attributeName, AttributeColumnType columnType,
			ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from ATTRIBUTE where 1 = 1");

		sql.append(" and TABLE_NAME = ? ");
		params.add(table.getTableName());

		if (!StringUtility.isNullOrEmpty(attributeCode)) {
			sql.append(" and lower(attribute_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(attributeCode
					.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(attributeName)) {
			sql.append(" and lower(attribute_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(attributeName
					.toLowerCase()));
		}
		
		if (null != columnType) {
			sql.append(" and column_type = ? ");
			params.add(columnType.getValue());
		}
		
		if (status != null) {
			sql.append(" and status=? ");
			params.add(status.getValue());
		} else {
			sql.append(" and status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		sql.append(" order by attribute_code");
		
		if (kPaging == null)
			return repo.getListBySQL(Attribute.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Attribute.class, sql.toString(),
					params, kPaging);
	}

	// S.O.S THAY DOI DAC TA YEU CAU
	@Override
	public void deleteAttributeValue(List<Attribute> listAttribute)
			throws DataAccessException {
		/*if (null == listAttribute || listAttribute.size() <= 0) {
			throw new DataAccessException("List attribute is empty");
		}

		String sql = null;
		StringBuilder body = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String tableName = null;

		for (int i = 0; i < listAttribute.size(); i++) {
			Attribute attribute = listAttribute.get(i);
			
			if (0 == i) {
				tableName = attribute.getTableName();
			}

			if (i < listAttribute.size() - 1) {
				body.append(String.format("%s = null, ",
						attribute.getColumnName()));
			} else {
				body.append(String.format("%s = null",
						attribute.getColumnName()));
			}
		}

		sql = String.format("UPDATE %s SET %s ", tableName, body.toString());

		System.out.println("SQL command: " + sql);

		this.repo.executeSQLQuery(sql.toString(), params);*/
	}

	// S.O.S THAY DOI DAC TA YEU CAU
	@Override
	public Boolean isUsingByOthers(Attribute attr) throws DataAccessException {
		/*if(attr != null){
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("select count(" + attr.getColumnName() + ") as count from ");
			sql.append(attr.getTableName());
			sql.append(" 	where " + attr.getColumnName() + " is not null");
			int c = repo.countBySQL(sql.toString(), params);
			return c==0?false:true;
		}*/
		return false;
	}
	
	/*@Override
	@Transactional(rollbackFor = Exception.class)
	public void importAttribute(TableHasAttribute table,
			List<Attribute> listActiveAttribute, List<List<String>> data,
			LogInfoVO logInfo) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		int keyLength = table.getTableKey().length;

		sql.append(String.format("update %s set ", table.getTableName()));

		for (int j = 0; j < listActiveAttribute.size(); j++) {
			if (j < listActiveAttribute.size() - 1) {
				sql.append(String.format(" %s = ?, ", listActiveAttribute
						.get(j).getColumnName()));
			} else {
				sql.append(String.format(" %s = ? ", listActiveAttribute.get(j)
						.getColumnName()));
			}
		}

		if (table.isEqual(TableHasAttribute.DISPLAY_PROGRAM_LEVEL)) {
			if (keyLength < 2) {
				throw new IllegalArgumentException(
						ExceptionCode.ATTRIBUTE_KEY_LENGTH_INVALIDATE);
			}

			sql.append(" where LEVEL_CODE = ? ");
			sql.append(" and DISPLAY_PROGRAM_ID = (select DISPLAY_PROGRAM_ID from DISPLAY_PROGRAM_LEVEL where DISPLAY_PROGRAM_CODE = ?)");
		} else if (table.isEqual(TableHasAttribute.INCENTIVE_PROGRAM_LEVEL)) {
			if (keyLength < 2) {
				throw new IllegalArgumentException(
						ExceptionCode.ATTRIBUTE_KEY_LENGTH_INVALIDATE);
			}

			sql.append(" where LEVEL_CODE = ? ");
			sql.append(" and INCENTIVE_PROGRAM_ID = (select INCENTIVE_PROGRAM_ID from INCENTIVE_PROGRAM_LEVEL where INCENTIVE_PROGRAM_CODE = ?)");
		} else if (table.isEqual(TableHasAttribute.DP_PAY_PERIOD_RESULT)) {
			if (keyLength < 3) {
				throw new IllegalArgumentException(
						ExceptionCode.ATTRIBUTE_KEY_LENGTH_INVALIDATE);
			}

			sql.append(" where DP_PAY_PERIOD_RESULT_ID = ( ");
			sql.append(" 	    select DP_PAY_PERIOD_RESULT_ID ");
			sql.append(" 	    from DP_PAY_PERIOD dp, DP_PAY_PERIOD_RESULT dpr, CUSTOMER c ");
			sql.append(" 	    where dp.DP_PAY_PERIOD_ID = dpr.DP_PAY_PERIOD_ID ");
			sql.append(" 	        and dpr.CUSTOMER_ID = c.CUSTOMER_ID ");
			sql.append(" 	        and c.CUSTOMER_CODE = ? ");
			sql.append(" 	        and dp.DP_PAY_PERIOD = ?) ");
		} else {
			if (keyLength < 1) {
				throw new IllegalArgumentException(
						ExceptionCode.ATTRIBUTE_KEY_LENGTH_INVALIDATE);
			}

			sql.append(String.format(" where %s = ?", table.getTableKey()[0]));
		}

		for (int i = 0; i < data.size(); i++) {
			List<String> item = data.get(i);
			String[] keyValue = new String[keyLength];

			for (int j = 0; j < keyLength; j++) {
				keyValue[j] = item.get(j);
			}

			// xoa tham so
			params.clear();

			// them tham so
			for (int j = keyLength; j < item.size(); j++) {
				params.add(item.get(j));
			}

			// them khoa
			if (table.isEqual(TableHasAttribute.DISPLAY_PROGRAM_LEVEL)) {
				params.add(keyValue[1]);
				params.add(keyValue[0]);
			} else if (table.isEqual(TableHasAttribute.INCENTIVE_PROGRAM_LEVEL)) {
				params.add(keyValue[1]);
				params.add(keyValue[0]);
			} else if (table.isEqual(TableHasAttribute.DP_PAY_PERIOD_RESULT)) {
				String cusCode = keyValue[0] + "_" + keyValue[1];
				params.add(cusCode);
				params.add(keyValue[2]);
			} else {
				params.add(keyValue[0]);
			}

			this.repo.executeSQLQuery(sql.toString(), params);

			try {
				actionGeneralLogDAO.createActionGeneralLog(attribute, null,
						logInfo);
			} catch (Exception ex) {
				throw new DataAccessException(ex);
			}
		}
	}*/

	// S.O.S THAY DOI DAC TA YEU CAU
	@Override
	public List<List<String>> exportAttribute(TableHasAttribute table,
			List<Attribute> listActiveAttribute) throws DataAccessException {
		/*StringBuilder sql = new StringBuilder();
		StringBuilder conditionSQL = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		int keyLength = table.getTableKey().length;
		int attributeLength = listActiveAttribute.size();
		List<List<String>> returnData = new ArrayList<List<String>>();

		if (table.isEqual(TableHasAttribute.INCENTIVE_PROGRAM_LEVEL)) {
			sql.append("select ip.INCENTIVE_PROGRAM_CODE as INCENTIVE_PROGRAM_CODE, ipl.LEVEL_CODE as LEVEL_CODE, ");

			for (int i = 0; i < attributeLength; i++) {
				if (i < attributeLength - 1) {
					sql.append(String.format(" ipl.%s AS %s, ",
							listActiveAttribute.get(i).getColumnName(),
							listActiveAttribute.get(i).getColumnName()));

				} else {
					sql.append(String.format(" ipl.%s AS %s ",
							listActiveAttribute.get(i).getColumnName(),
							listActiveAttribute.get(i).getColumnName()));
				}

				if (i == 0) {
					conditionSQL
							.append(String
									.format(" where ip.INCENTIVE_PROGRAM_ID = ipl.INCENTIVE_PROGRAM_ID AND (ipl.%s is not null ",
											listActiveAttribute.get(i)
													.getColumnName()));
				} else {
					conditionSQL.append(String.format(
							" or ipl.%s is not null ",
							listActiveAttribute.get(i).getColumnName()));
				}
			}

			conditionSQL.append(")");

			sql.append(String
					.format(" from INCENTIVE_PROGRAM_LEVEL ipl, INCENTIVE_PROGRAM ip %s ORDER BY %s",
							conditionSQL.toString(),
							"ip.INCENTIVE_PROGRAM_CODE"));
		} else if (table.isEqual(TableHasAttribute.DISPLAY_GROUP)) {
			sql.append("select ip.DISPLAY_PROGRAM_CODE as DISPLAY_PROGRAM_CODE, ipl.DISPLAY_GROUP_CODE as DISPLAY_GROUP_CODE, ");

			for (int i = 0; i < attributeLength; i++) {
				if (i < attributeLength - 1) {
					sql.append(String.format(" ipl.%s AS %s, ",
							listActiveAttribute.get(i).getColumnName(),
							listActiveAttribute.get(i).getColumnName()));

				} else {
					sql.append(String.format(" ipl.%s AS %s ",
							listActiveAttribute.get(i).getColumnName(),
							listActiveAttribute.get(i).getColumnName()));
				}

				if (i == 0) {
					conditionSQL
							.append(String
									.format(" where ip.DISPLAY_PROGRAM_ID = ipl.DISPLAY_PROGRAM_ID AND (ipl.%s is not null ",
											listActiveAttribute.get(i)
													.getColumnName()));
				} else {
					conditionSQL.append(String.format(
							" or ipl.%s is not null ",
							listActiveAttribute.get(i).getColumnName()));
				}
			}

			conditionSQL.append(")");

			sql.append(String
					.format(" from DISPLAY_GROUP ipl, DISPLAY_PROGRAM ip %s ORDER BY %s",
							conditionSQL.toString(),
							"ip.DISPLAY_PROGRAM_CODE, ipl.DISPLAY_GROUP_CODE"));
		} else if (table.isEqual(TableHasAttribute.DISPLAY_PROGRAM_LEVEL)) {
			sql.append("select ip.DISPLAY_PROGRAM_CODE as DISPLAY_PROGRAM_CODE, ipl.LEVEL_CODE as LEVEL_CODE, ");

			for (int i = 0; i < attributeLength; i++) {
				if (i < attributeLength - 1) {
					sql.append(String.format(" ipl.%s AS %s, ",
							listActiveAttribute.get(i).getColumnName(),
							listActiveAttribute.get(i).getColumnName()));

				} else {
					sql.append(String.format(" ipl.%s AS %s ",
							listActiveAttribute.get(i).getColumnName(),
							listActiveAttribute.get(i).getColumnName()));
				}

				if (i == 0) {
					conditionSQL
							.append(String
									.format(" where ip.DISPLAY_PROGRAM_ID = ipl.DISPLAY_PROGRAM_ID AND (ipl.%s is not null ",
											listActiveAttribute.get(i)
													.getColumnName()));
				} else {
					conditionSQL.append(String.format(
							" or ipl.%s is not null ",
							listActiveAttribute.get(i).getColumnName()));
				}
			}

			conditionSQL.append(")");

			sql.append(String
					.format(" from DISPLAY_PROGRAM_LEVEL ipl, DISPLAY_PROGRAM ip %s ORDER BY %s",
							conditionSQL.toString(), "ip.DISPLAY_PROGRAM_CODE"));
		} else if (table.isEqual(TableHasAttribute.DP_PAY_PERIOD_RESULT)) {
			sql.append("select dp.SHOP_CODE as SHOP_CODE, c.CUSTOMER_CODE as CUSTOMER_CODE, dpr.LEVEL_CODE as LEVEL_CODE, ");

			for (int i = 0; i < attributeLength; i++) {
				if (i < attributeLength - 1) {
					sql.append(String.format(" dpr.%s AS %s, ",
							listActiveAttribute.get(i).getColumnName(),
							listActiveAttribute.get(i).getColumnName()));

				} else {
					sql.append(String.format(" dpr.%s AS %s ",
							listActiveAttribute.get(i).getColumnName(),
							listActiveAttribute.get(i).getColumnName()));
				}

				if (i == 0) {
					conditionSQL
							.append(String
									.format(" where dp.DP_PAY_PERIOD_ID = dpr.DP_PAY_PERIOD_ID and dpr.CUSTOMER_ID = c.CUSTOMER_ID AND (dpr.%s is not null ",
											listActiveAttribute.get(i)
													.getColumnName()));
				} else {
					conditionSQL.append(String.format(
							" or dpr.%s is not null ",
							listActiveAttribute.get(i).getColumnName()));
				}
			}

			conditionSQL.append(")");

			sql.append(String
					.format(" from DP_PAY_PERIOD dp, DP_PAY_PERIOD_RESULT dpr, CUSTOMER c %s",
							conditionSQL.toString()));
		} else {
			sql.append("select ");

			for (int i = 0; i < keyLength; i++) {
				sql.append(table.getTableKey()[i] + ", ");
			}

			for (int i = 0; i < attributeLength; i++) {
				if (i < attributeLength - 1) {
					sql.append(listActiveAttribute.get(i).getColumnName()
							+ ", ");
				} else {
					sql.append(listActiveAttribute.get(i).getColumnName());
				}

				if (i == 0) {
					conditionSQL.append(String.format(" where %s is not null ",
							listActiveAttribute.get(i).getColumnName()));
				} else {
					conditionSQL.append(String.format(" or %s is not null ",
							listActiveAttribute.get(i).getColumnName()));
				}
			}

			sql.append(String.format(" from %s %s", table.getTableName(),
					conditionSQL.toString()));
		}

		// System.out.println(TestUtils.addParamToQuery(sql.toString(),
		// params.toArray()));

		List<Object> data = this.repo.getDataToListPaginated(sql.toString(),
				params, 0, 0);

		if (null != data && data.size() > 0) {
			for (Object obj : data) {
				HashMap<String, String> row = (HashMap<String, String>) obj;

				List<String> item = new ArrayList<String>();

				// add key
				for (int i = 0; i < keyLength; i++) {
					item.add(row.get(table.getTableKey()[i]));
				}

				// add value
				for (int i = 0; i < attributeLength; i++) {
					item.add(row
							.get(listActiveAttribute.get(i).getColumnName()));
				}

				returnData.add(item);
			}
		}

		return returnData;*/
		
		return null;
	}

	@Override
	public boolean checkKeyIsExists(TableHasAttribute table,
			List<String> listValueKey) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		int keyLength = listValueKey.size();

		if (table.isEqual(TableHasAttribute.DISPLAY_PROGRAM_LEVEL)) {
			if(keyLength < 2){
				throw new IllegalArgumentException(ExceptionCode.ATTRIBUTE_KEY_LENGTH_INVALIDATE);
			}
			
			sql.append("select count(*) as count from DISPLAY_PROGRAM dp, DISPLAY_PROGRAM_LEVEL dpl ");
			sql.append(" where dp.DISPLAY_PROGRAM_ID = dpl.DISPLAY_PROGRAM_ID ");
			sql.append(" 	and dp.DISPLAY_PROGRAM_CODE = upper(?) ");
			sql.append(" 	and dpl.LEVEL_CODE = upper(?) ");
			
			params.add(listValueKey.get(0));
			params.add(listValueKey.get(1));
		} else if (table.isEqual(TableHasAttribute.DISPLAY_PROGRAM)) {
			if(keyLength < 1){
				throw new IllegalArgumentException(ExceptionCode.ATTRIBUTE_KEY_LENGTH_INVALIDATE);
			}
			
			sql.append("select count(*) as count from DISPLAY_PROGRAM ");
			sql.append(" where DISPLAY_PROGRAM_CODE = upper(?) ");
			sql.append(" 	and STATUS <> ? ");
			
			params.add(listValueKey.get(0));
			params.add(ActiveType.DELETED.getValue());
		} else if (table.isEqual(TableHasAttribute.DISPLAY_GROUP)) {
			if(keyLength < 2){
				throw new IllegalArgumentException(ExceptionCode.ATTRIBUTE_KEY_LENGTH_INVALIDATE);
			}
			
			sql.append("select count(*) as count from DISPLAY_PROGRAM ip, DISPLAY_GROUP ipl ");
			sql.append(" where ip.DISPLAY_PROGRAM_ID = ipl.DISPLAY_PROGRAM_ID ");
			sql.append(" 	and ip.DISPLAY_PROGRAM_CODE = upper(?) ");
			sql.append(" 	and ipl.DISPLAY_GROUP_CODE = upper(?) ");
			
			params.add(listValueKey.get(0));
			params.add(listValueKey.get(1));
		} else if (table.isEqual(TableHasAttribute.INCENTIVE_PROGRAM_LEVEL)) {
			if(keyLength < 2){
				throw new IllegalArgumentException(ExceptionCode.ATTRIBUTE_KEY_LENGTH_INVALIDATE);
			}
			
			sql.append("select count(*) as count from INCENTIVE_PROGRAM ip, INCENTIVE_PROGRAM_LEVEL ipl ");
			sql.append(" where ip.INCENTIVE_PROGRAM_ID = ipl.INCENTIVE_PROGRAM_ID ");
			sql.append(" 	and ip.INCENTIVE_PROGRAM_CODE = upper(?) ");
			sql.append(" 	and ipl.LEVEL_CODE = upper(?) ");
			
			params.add(listValueKey.get(0));
			params.add(listValueKey.get(1));
		} else if(table.isEqual(TableHasAttribute.DP_PAY_PERIOD_RESULT)){
			if(keyLength < 3){
				throw new IllegalArgumentException(ExceptionCode.ATTRIBUTE_KEY_LENGTH_INVALIDATE);
			}
			
			String cusCode = listValueKey.get(0) + "_" + listValueKey.get(1);
			
			sql.append(" select count(*) as count ");
			sql.append(" from DP_PAY_PERIOD dp, DP_PAY_PERIOD_RESULT dpr, CUSTOMER c ");
			sql.append(" where dp.DP_PAY_PERIOD_ID = dpr.DP_PAY_PERIOD_ID ");
			sql.append("         and dpr.CUSTOMER_ID = c.CUSTOMER_ID ");
			sql.append("         and c.CUSTOMER_CODE = upper(?) ");
			sql.append("         and dp.DP_PAY_PERIOD = upper(?) ");
			
			params.add(cusCode);
			params.add(listValueKey.get(2));
		} else if(table.isEqual(TableHasAttribute.DP_PAY_PERIOD_RESULT)){
			if(keyLength < 3){
				throw new IllegalArgumentException(ExceptionCode.ATTRIBUTE_KEY_LENGTH_INVALIDATE);
			}
			
			String cusCode = listValueKey.get(0) + "_" + listValueKey.get(1);
			
			sql.append(" select count(*) as count ");
			sql.append(" from DP_PAY_PERIOD dp, DP_PAY_PERIOD_RESULT dpr, CUSTOMER c ");
			sql.append(" where dp.DP_PAY_PERIOD_ID = dpr.DP_PAY_PERIOD_ID ");
			sql.append("         and dpr.CUSTOMER_ID = c.CUSTOMER_ID ");
			sql.append("         and c.CUSTOMER_CODE = upper(?) ");
			sql.append("         and dp.DP_PAY_PERIOD = upper(?) ");
			
			params.add(cusCode);
			params.add(listValueKey.get(2));
		} else {
			if(keyLength < 1){
				throw new IllegalArgumentException(ExceptionCode.ATTRIBUTE_KEY_LENGTH_INVALIDATE);
			}
			
			sql.append(String.format(
					"select count(*) as count from %s where 1 = 1 ",
					table.getTableName()));

			for (int i = 0; i < listValueKey.size(); i++) {
				String keyName = table.getTableKey()[i];

				sql.append(String.format(" and %s = upper(?) ", keyName));
				params.add(listValueKey.get(i));
			}
		}

		return this.repo.countBySQL(sql.toString(), params) > 0 ? true : false;
	}

	@Override
	public boolean checkAttributeIsExists(TableHasAttribute table,
			String attributeCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select count(1) as count from ATTRIBUTE where TABLE_NAME = ? AND ATTRIBUTE_CODE = ?");

		params.add(table.getTableName());
		params.add(attributeCode.toUpperCase());

		return this.repo.countBySQL(sql.toString(), params) > 0 ? true : false;
	}
}

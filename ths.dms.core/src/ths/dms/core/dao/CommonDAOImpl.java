package ths.dms.core.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.EquipLender;
import ths.dms.core.entities.ImportFileData;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ArithmeticEnum;
import ths.dms.core.entities.enumtype.EquipPeriodType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class CommonDAOImpl implements CommonDAO {
	@Autowired
	private IRepository repo;

	@Override
	public Date getSysDate() throws DataAccessException {
		return (Date) repo.getObjectByQuery("SELECT SYSDATE FROM DUAL", null);
	}

	@Override
	public Date getYesterday() throws DataAccessException {
		return (Date) repo.getObjectByQuery("SELECT (SYSDATE - 1) FROM DUAL", null);
	}

	@Override
	public Date getFirstDateOfMonth() throws DataAccessException {
		return (Date) repo.getObjectByQuery("SELECT TRUNC(SYSDATE, 'MONTH') FROM DUAL", null);
	}

	@Override
	public Date getLastDate(Date date) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		params.add(date);
		return (Date) repo.getObjectByQuery(" select last_day(?) from dual ", params);
	}

	@Override
	public BigDecimal checkValidDateDate(String dateStr) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "select checkValidDate (?) as checkDate from dual";
		params.add(dateStr);
		return (BigDecimal) repo.getObjectByQuery(sql, params);
	}

	/**
	 * @author hunglm16
	 * @since March 24, 2014
	 * @description Lay ngay so voi ngay hien tai qua so khoang cach ngay
	 * */
	@Override
	public Date getDateBySysdateForNumberDay(Integer numberDay, boolean trunc) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "";
		if (!trunc) {
			sql = "SELECT (SYSDATE + (?)) FROM DUAL";
		} else {
			sql = "SELECT TRUNC(SYSDATE + (?)) FROM DUAL";
		}
		params.add(numberDay);
		return (Date) repo.getObjectByQuery(sql, params);
	}

	/**
	 * @author hunglm16
	 * @since March 28, 2014
	 * @description Lay ngay so voi ngay nhap vao qua so khoang cach ngay
	 * */
	@Override
	public Date getDateByDateForNumberDay(Date date, Integer numberDay, boolean trunc) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "";
		if (!trunc) {
			sql = "SELECT ((?) + (?)) FROM DUAL";
		} else {
			sql = "SELECT TRUNC((?) + (?)) FROM DUAL";
		}
		params.add(date);
		params.add(numberDay);
		return (Date) repo.getObjectByQuery(sql, params);
	}

	/**
	 * @author hunglm16
	 * @since March 28, 2014
	 * @description So sanh 2 ngay
	 * */
	@Override
	public Integer compareDateWithoutTime(Date date1, Date date2) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "SELECT (case when trunc(?) < trunc(?) then -1  when trunc(?) = trunc(?) then 0 else 1 end) as key FROM DUAL";
		params.add(date1);
		params.add(date2);
		params.add(date1);
		params.add(date2);
		BigDecimal kq = (BigDecimal) repo.getObjectByQuery(sql, params);
		if (kq != null) {
			return kq.intValue();
		}
		return 0;
	}
	
	@Override
	public int arithmeticForDateByDateWithTrunc(Date date1, Date date2, ArithmeticEnum arithmetic) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "select trunc(?) + trunc(?) as key from dual";
		if (arithmetic != null && ArithmeticEnum.MINUS.getValue() == arithmetic.getValue()) {
			sql = "select trunc(?) - trunc(?) as key from dual";
		}
		params.add(date1);
		params.add(date2);
		BigDecimal kq = (BigDecimal) repo.getObjectByQuery(sql, params);
		if (kq != null) {
			return kq.intValue();
		}
		return 0;
	}

	@Override
	public ShopParam getShopParamByType(Long shopId, String type) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "SELECT * FROM SHOP_PARAM WHERE STATUS = 1 AND SHOP_ID = ? AND TYPE = ? AND ROWNUM = 1";
		params.add(shopId);
		params.add(type);
		return repo.getEntityBySQL(ShopParam.class, sql, params);
	}

	@Override
	public Integer getIslevel(Long shopId) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "select (select specific_type from shop_type st where st.status = 1 and st.shop_type_id = s.shop_type_id) as isLevel ";
		sql += " from shop s where shop_id = ? ";
		params.add(shopId);
		BigDecimal kq = (BigDecimal) repo.getObjectByQuery(sql, params);
		if (kq != null) {
			return kq.intValue();
		}
		return 0;
	}

	@Override
	public <T> T createEntity(T object) throws DataAccessException {
		return repo.create(object);
	}

	@Override
	public <T> List<T> creatListEntity(List<T> lstObjects) throws DataAccessException {
		return repo.create(lstObjects);
	}

	@Override
	public void updateEntity(Object object) throws DataAccessException {
		repo.update(object);
	}

	@Override
	public <T> List<T> updateListEntity(List<T> lstObjects) throws DataAccessException {
		return repo.update(lstObjects);
	}
	
	@Override
	public ImportFileData createImportFileData(ImportFileData importFileData) throws DataAccessException {
		return repo.create(importFileData);
	}
	
	@Override
	public ImportFileData getImportFileData(String fileName, Long staffId) throws DataAccessException {
		String sql = "select * from IMPORT_FILE_DATA where DATA_FILE_NAME = ? and STAFF_ID = ? and (STATUS = 2 or STATUS = 6)";
		List<Object> params = new ArrayList<Object>();
		params.add(fileName);
		params.add(staffId);
		return repo.getEntityBySQL(ImportFileData.class, sql, params);
	}
	
	@Override
	public List<Staff> getListStaffImportFile(KPaging<Staff> kPaging, List<Integer> listStatus, String serverId) throws DataAccessException {
		String sql = "select * from staff where staff_id in (";
		sql += "select staff_id from IMPORT_FILE_DATA where 1 = 1 ";
		List<Object> params = new ArrayList<Object>();
		if(!StringUtility.isNullOrEmpty(serverId)) {
			sql += " and SERVER_IP = ? ";
			params.add(serverId.trim());
		}
		if(listStatus != null && listStatus.size() > 0) {
			sql += " and (status = ?";
			params.add(listStatus.get(0));
			for(int i = 1; i < listStatus.size(); i++) {
				sql += " or status = ?";
				params.add(listStatus.get(i));
			}
			sql += ")";
		}
		sql += ")";
		
		if(kPaging == null) {
			return repo.getListBySQL(Staff.class, sql, params);
		} else {
			return repo.getListBySQLPaginated(Staff.class, sql, params, kPaging);
		}
	}
	
	@Override
	public List<ImportFileData> getListImportFileData(KPaging<ImportFileData> kPaging, Long staffId, List<Integer> listStatus, String serverId) throws DataAccessException {
		String sql = "select * from IMPORT_FILE_DATA where 1 = 1 ";
		List<Object> params = new ArrayList<Object>();
		if(staffId != null) {
			sql += " and STAFF_ID = ? ";
			params.add(staffId);
		}
		if(!StringUtility.isNullOrEmpty(serverId)) {
			sql += " and SERVER_IP = ? ";
			params.add(serverId.trim());
		}
		if(listStatus != null && listStatus.size() > 0) {
			sql += " and (status = ?";
			params.add(listStatus.get(0));
			for(int i = 1; i < listStatus.size(); i++) {
				sql += " or status = ?";
				params.add(listStatus.get(i));
			}
			sql += ")";
		}
		sql += " order by IMPORT_FILE_DATA_ID desc";
		if(kPaging == null) {
			return repo.getListBySQL(ImportFileData.class, sql, params);
		} else {
			return repo.getListBySQLPaginated(ImportFileData.class, sql, params, kPaging);
		}
	}

	@Override
	public <T> T getEntityById(Class<T> clazz, Serializable id) throws DataAccessException {
		return repo.getEntityById(clazz, id);
	}

	@Override
	public void deleteEntity(Object object) throws DataAccessException {
		repo.delete(object);
	}
	/**
	 * Lay danh sach Equip_Lender
	 * 
	 * @author hunglm16
	 * @since August 18, 2015
	 * 
	 * @param [shopId]: id Don vi - Khong duoc de rong
	 * */
	@Override
	public EquipLender getFirtInformationEquipLender (Long shopId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("Id Shop Is Null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select el.* ");
		sql.append(" from EQUIP_LENDER el join ( ");
		sql.append("  select shop_id, level as lvel from shop start with shop_id = ? connect by shop_id = prior parent_shop_id ");
		params.add(shopId);
		sql.append(" ) sh on el.shop_id = sh.shop_id ");
		sql.append(" where el.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" order by sh.lvel ");
		List<EquipLender> res = repo.getListBySQL(EquipLender.class, sql.toString(), params);
		if (res != null && !res.isEmpty()) {
			return res.get(0);//Lay gia tri dau tien lay duoc
		}
		return null;
	}	
	
	/**
	 * Lay ky dong co ngay den lon nhat cua kenh thiet bi
	 * 
	 * @author hunglm16
	 * @since Jun 07,2015
	 * */
	@Override
	public Long getEquipPeriodIdByCloseForMaxToDate() throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select dta.equip_period_id as equipPeriodId ");
		sql.append("   from ( ");
		sql.append("      select epd.equip_period_id , epd.from_date , epd.to_date , row_number() over (order by to_date desc) as rn ");
		sql.append("      from equip_period epd where 1 = 1 and status in (?) ");
		params.add(EquipPeriodType.CLOSED.getValue());
		sql.append("   ) dta ");
		sql.append("   where dta.rn = 1 ");
		BigDecimal kq = (BigDecimal) repo.getObjectByQuery(sql.toString(), params);
		if (kq != null) {
			return kq.longValue();
		}
		return null;
	}
}

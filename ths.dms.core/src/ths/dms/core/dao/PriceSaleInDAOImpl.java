package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PriceSaleIn;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.filter.PriceFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PriceVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class PriceSaleInDAOImpl implements PriceSaleInDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	ShopLockDAO shopLockDAO;

	@Override
	public PriceSaleIn createPriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws DataAccessException {
		if (priceSaleIn == null) {
			throw new IllegalArgumentException("priceSaleIn is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		try {
			priceSaleIn.setCreateDate(commonDAO.getSysDate());
			priceSaleIn.setCreateUser(logInfo.getStaffCode());
			repo.create(priceSaleIn);
			actionGeneralLogDAO.createActionGeneralLog(null, priceSaleIn, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		PriceSaleIn x = repo.getEntityById(PriceSaleIn.class, priceSaleIn.getId());
		return x;
	}

	@Override
	public void updatePriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws DataAccessException {
		if (priceSaleIn == null) {
			throw new IllegalArgumentException("priceSaleIn is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		PriceSaleIn temp = this.getPriceSaleInById(priceSaleIn.getId());
		priceSaleIn.setUpdateDate(commonDAO.getSysDate());
		priceSaleIn.setUpdateUser(logInfo.getStaffCode());
		repo.update(priceSaleIn);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, priceSaleIn, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public void updatePriceSaleIn(List<PriceSaleIn> lst, LogInfoVO logInfo) throws DataAccessException {
		if (lst == null || lst.size() == 0) {
			throw new IllegalArgumentException("list PriceSaleIn is null or size equals to zero");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		try {
			Date sysDate = commonDAO.getSysDate();
			for (int i = 0, n = lst.size(); i < n; i++) {
				PriceSaleIn priceSaleIn = lst.get(i);
				PriceSaleIn temp = this.getPriceSaleInById(priceSaleIn.getId());
				priceSaleIn.setUpdateDate(sysDate);
				priceSaleIn.setUpdateUser(logInfo.getStaffCode());
				actionGeneralLogDAO.createActionGeneralLog(temp, priceSaleIn, logInfo);
			}
			repo.update(lst);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public void deletePriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws DataAccessException {
		if (priceSaleIn == null) {
			throw new IllegalArgumentException("priceSaleIn is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(priceSaleIn);
		try {
			actionGeneralLogDAO.createActionGeneralLog(priceSaleIn, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public PriceSaleIn getPriceSaleInById(long id) throws DataAccessException {
		return repo.getEntityById(PriceSaleIn.class, id);
	}

	@Override
	public List<PriceSaleIn> getListPriceSaleInByFilter(PriceFilter<PriceSaleIn> filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from price_sale_in pr ");
		sql.append(" where 1 = 1 ");
		if (filter.getArrlongG() != null && !filter.getArrlongG().isEmpty()) {
			sql.append(" and pr.price_sale_in_id in (-1 ");
			for (Long priceId : filter.getArrlongG()) {
				sql.append(" ,? ");
				params.add(priceId);
			}
			sql.append(" ) ");
		}
		if (filter.getStatus() != null) {
			sql.append(" and pr.status = ? ");
			params.add(filter.getStatus());
		}
		if (filter.getProductId() != null) {
			sql.append(" and pr.product_id = ? ");
			params.add(filter.getProductId());
		}
		if (filter.getShopId() != null) {
			if (Boolean.TRUE.equals(filter.getIsGetChildShop())) {
				sql.append(" and pr.shop_id in (select shop_id from shop where status = ? ");
				params.add(ActiveType.RUNNING.getValue());
				sql.append(" start with shop_id = ? ");
				params.add(filter.getShopId());
				sql.append(" connect by prior shop_id = parent_shop_id)  ");
			} else {
				sql.append(" and pr.shop_id = ? ");
				params.add(filter.getShopId());
			}
		}
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(PriceSaleIn.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(PriceSaleIn.class, sql.toString(), params, filter.getkPaging());
		}
	}

	@Override
	public List<PriceVO> searchPriceSaleInVOByFilter(PriceFilter<PriceVO> filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" WITH shopTmp AS ( ");
			sql.append("     SELECT shop_id ");
			sql.append("     FROM shop ");
			sql.append("     WHERE status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("       START WITH shop_code = ? ");
			params.add(filter.getShopCode().trim());
			sql.append("       CONNECT BY prior shop_id = parent_shop_id ");
			sql.append("   ) ");
		}
		sql.append("SELECT ");
		sql.append(" sh.shop_id shopId,");
		sql.append(" sh.shop_code shopCode,");
		sql.append(" sh.shop_name shopName,");
		sql.append(" pr.product_id productId,");
		sql.append(" pd.product_code productCode,");
		sql.append(" pd.product_name productName, ");
		sql.append(" TO_CHAR(pr.from_date, 'dd/MM/yyyy') fromDateStr,");
		sql.append(" TO_CHAR(pr.to_date, 'dd/MM/yyyy') toDateStr,");
		sql.append(" pr.price_sale_in_id id, ");
		sql.append(" pr.vat vat,");
		sql.append(" pr.package_price pricePackage,");
		sql.append(" pr.package_price_not_vat pricePackageNotVAT,");
		sql.append(" pr.retail_price price,");
		sql.append(" pr.retail_price_not_vat priceNotVAT,");
		sql.append(" pr.status status");
		sql.append(" FROM price_sale_in pr");
		sql.append(" join product pd on pr.product_id = pd.product_id");
		sql.append(" left join shop sh on pr.shop_id = sh.shop_id and sh.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" where pd.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		//Ma san pham
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			sql.append("  and pr.product_id in (select product_id from product where status = 1 and lower(product_code) like ? escape '/') ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProductCode().trim().toLowerCase()));
		}

		//Ten san pham
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			sql.append("  and pr.product_id in (select product_id from product where status = 1 and unicode2english(product_name) like ? escape '/') ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getProductName().trim().toLowerCase().trim())));
		}

		//Ma Don vi
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("  and pr.shop_id in (select shop_id from shopTmp) ");
		}

		//Tu ngay- den ngay
		if (filter.getFromDate() != null && filter.getToDate() != null) {
			sql.append(" and ((pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?)))  ");
			params.add(filter.getToDate());
			params.add(filter.getToDate());
			sql.append("	or (pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?)))  ");
			params.add(filter.getFromDate());
			params.add(filter.getFromDate());
			sql.append("	or (pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?)))  ");
			params.add(filter.getFromDate());
			params.add(filter.getToDate());
			sql.append("	or (pr.from_date >= trunc(?) and pr.to_date < trunc(?) + 1)  ");
			params.add(filter.getFromDate());
			params.add(filter.getToDate());
			sql.append(" )  ");
		} else if (filter.getFromDate() != null && filter.getToDate() == null) {
			sql.append(" and (pr.to_date is null or pr.to_date >= trunc(?)) ");
			params.add(filter.getFromDate());
		} else if (filter.getFromDate() == null && filter.getToDate() != null) {
			sql.append(" and pr.from_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}

		if (filter.getStatus() != null) {
			sql.append("  and pr.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append("  and pr.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		countSql.append(" SELECT count(1) as count from ( ");
		countSql.append(sql.toString());
		countSql.append("  )  ");

		sql.append("  order by sh.shop_code, pd.product_code, TO_CHAR(pr.from_date, 'dd/MM/yyyy') desc, TO_CHAR(pr.to_date, 'dd/MM/yyyy') desc");
		String[] fieldNames = new String[] { "shopId", "shopName", "shopCode", "productId", "productCode", "productName", "fromDateStr", "toDateStr", "id", "vat", "pricePackage", "pricePackageNotVAT", "price", "priceNotVAT", "status" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.FLOAT, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER };
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(PriceVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(PriceVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
}

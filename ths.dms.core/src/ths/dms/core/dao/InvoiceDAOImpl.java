package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Invoice;
import ths.dms.core.entities.InvoiceDetail;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.InvoiceCustPayment;
import ths.dms.core.entities.enumtype.InvoiceFilter;
import ths.dms.core.entities.enumtype.InvoiceStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.core.entities.vo.rpt.RptAggegateVATInvoiceDataVO;
import ths.core.entities.vo.rpt.RptInvoiceComparingVO;
import ths.core.entities.vo.rpt.RptViewInvoiceVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class InvoiceDAOImpl implements InvoiceDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public Invoice createInvoice(Invoice invoice) throws DataAccessException {
		if (invoice == null) {
			throw new IllegalArgumentException("invoice");
		}
		repo.create(invoice);
		return repo.getEntityById(Invoice.class, invoice.getId());
	}

	@Override
	public void deleteInvoice(Invoice invoice) throws DataAccessException {
		if (invoice == null) {
			throw new IllegalArgumentException("invoice");
		}
		repo.delete(invoice);
	}

	@Override
	public Invoice getInvoiceById(long id) throws DataAccessException {
		return repo.getEntityById(Invoice.class, id);
	}

	@Override
	public void updateInvoice(Invoice invoice) throws DataAccessException {
		if (invoice == null) {
			throw new IllegalArgumentException("invoice");
		}
		invoice.setUpdateDate(commonDAO.getSysDate());
		repo.update(invoice);
	}

	@Override
	public List<Invoice> getListInvoice(InvoiceFilter filter) throws DataAccessException {

		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate()))
			return new ArrayList<Invoice>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from invoice where 1 = 1");
		if(filter.getIsValueOrder()!=null && filter.getIsValueOrder() == 0){
			sql.append(" and amount <= 0 ");
		}
		if(filter.getIsValueOrder()!=null && filter.getIsValueOrder() == 1){
			sql.append(" and amount > 0 ");
		}
		if (filter.getTaxValue() != null) {
			sql.append(" and vat=?");
			params.add(filter.getTaxValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and order_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber().toUpperCase()));
		}
		if (filter.getFromDate() != null) {
			sql.append(" and order_date >= trunc(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and order_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		 if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			 sql.append(" and exists (select 1 from staff where staff.staff_id = invoice.staff_id and staff.staff_code like ? ESCAPE '/' )");
			 params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode()));
		 }
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and customer_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShortCode().toUpperCase()));
		}
		if (filter.getCustPayment() != null) {
			sql.append(" and cust_payment = ?");
			params.add(filter.getCustPayment().getValue());
		}
		if (filter.getStatus() != null) {
			sql.append(" and status = ?");
			params.add(filter.getStatus().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerAddr())) {
			sql.append(" and customer_addr like ? escape '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerAddr().toUpperCase()));
		}
		if(filter.getShopId()!=null){
			sql.append(" and shop_id = ? ");
			params.add(filter.getShopId());
		}
		if(filter.getIsSOPrint() != null) {
			if(Boolean.TRUE.equals(filter.getIsSOPrint())){
				sql.append(" and exists(select 1 from sale_order so where so.order_number = invoice.order_number and so.TIME_PRINT is not null) ");
			}else{
				sql.append(" and exists(select 1 from sale_order so where so.order_number = invoice.order_number and so.TIME_PRINT is null) ");
			}
		}
		if(filter.getIsInvoicePrint()!= null) {
			if(Boolean.TRUE.equals(filter.getIsInvoicePrint())) {
				sql.append(" and invoice_date is not null");
			} else {
				sql.append(" and invoice_date is null");
			}
		}
		//sql.append(" and invoice_date is null");
		sql.append(" order by order_number, INVOICE_NUMBER asc ");
		if (filter.getkPaging() != null) {
			return repo.getListBySQLPaginated(Invoice.class, sql.toString(),
					params, filter.getkPaging());
		} else {
			return repo.getListBySQL(Invoice.class, sql.toString(), params);
		}
	}
	
	@Override
	public List<Invoice> getListInvoiceByCondition(KPaging<Invoice> kPaging,
			Float taxValue, String orderNumber, String redInvoiceNumber, Date fromDate, Date toDate,
			String staffCode, String deliveryCode, String shortCode, InvoiceCustPayment custPayment, Date lockDate, Long shopId, Integer isValueOrder,Boolean isVATGop,Integer numberValueOrder) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<Invoice>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select i.* from invoice i"); 
		sql.append(" left join staff ss on i.staff_id = ss.staff_id");
		sql.append(" left join staff ds on i.delivery_id = ds.staff_id");
		sql.append(" where 1 = 1");		
		if (taxValue != null) {
			sql.append(" and i.vat=?");
			params.add(taxValue);
		}
		if(isValueOrder!=null && isValueOrder == 0){
			sql.append(" and i.amount <= 0 ");
		}
		if(isValueOrder!=null && isValueOrder == 1){
			sql.append(" and i.amount > 0 ");
		}
		if (shopId != null) {
			sql.append(" and i.shop_id = ? ");
			params.add(shopId);
		}
		if (orderNumber != null) {
			sql.append(" and upper(i.order_number) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(orderNumber.toUpperCase()));
		}
		if (redInvoiceNumber != null) {
			sql.append(" and upper(i.invoice_number) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(redInvoiceNumber.toUpperCase()));
		}
		if (fromDate != null) {
			sql.append(" and i.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			if(DateUtility.compareDateWithoutTime(toDate, lockDate) == 1){
				sql.append(" and i.order_date < trunc(?) + 1");
				params.add(lockDate);
			}else{
				sql.append(" and i.order_date < trunc(?) + 1");
				params.add(toDate);
			}
			
		}
		if(!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and upper(ss.staff_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(staffCode.toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(deliveryCode)) {
			sql.append(" and upper(ds.staff_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(deliveryCode.toUpperCase()));
		}
		/* if (staffCode != null) {
			 sql.append(" and exists (select 1 from staff where staff.staff_id = invoice.staff_id and staff.staff_code like ? ESCAPE '/' )");
			 params.add(StringUtility.toOracleSearchLikeSuffix(staffCode));
		 }*/
		if (shortCode != null) {
			sql.append(" and i.customer_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
		}
		if (custPayment != null) {
			sql.append(" and i.cust_payment = ?");
			params.add(custPayment.getValue());
		}
		if(isVATGop!=null){
			if(isVATGop){
				sql.append(" and i.status in (?,?)");
				params.add(InvoiceStatus.USING.getValue());
				params.add(InvoiceStatus.CANCELED.getValue());
				sql.append(" and i.sale_order_type = 1");
				sql.append(" and i.sale_order_id in (select pri_sale_order_id from compound_sale_order where shop_id = ?)");		
				params.add(shopId);
			}else{
				sql.append(" and i.status = ?");
				params.add(InvoiceStatus.USING.getValue());
				sql.append(" and i.sale_order_type = 0");
			}
		}else{
			sql.append(" and i.status = ?");
			params.add(InvoiceStatus.USING.getValue());
		}
		sql.append(" order by i.order_number, i.invoice_id "); //tungtt
		if (kPaging != null) {
			return repo.getListBySQLPaginated(Invoice.class, sql.toString(),
					params, kPaging);
		} else {
			return repo.getListBySQL(Invoice.class, sql.toString(), params);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.InvoiceDAO#updateInvoice(java.util.List)
	 */
	@Override
	public void updateInvoice(List<Invoice> lstInvoice)
			throws DataAccessException {
		
		if (lstInvoice == null) {
			throw new IllegalArgumentException("invoice");
		}
		repo.update(lstInvoice);
	}

	@Override
	public List<RptAggegateVATInvoiceDataVO> getDataForAggegateVATInvoiceReport(
			Long shopId, List<Long> customerIds, Long staffId, Integer VAT,
			InvoiceStatus status, Date fromDate, Date toDate)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptAggegateVATInvoiceDataVO>();
		
		if (null == fromDate) {
			throw new IllegalArgumentException("From date is invalidate");
		}

		if (null == toDate) {
			throw new IllegalArgumentException("To date is invalidate");
		}

		if (fromDate.compareTo(toDate) > 0) {
			throw new IllegalArgumentException(
					"From date can't greated than to date");
		}

		if (null == shopId) {
			throw new IllegalArgumentException("Shop id is invalidate");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT STA.STAFF_CODE AS staffCode, ");
		sql.append("     INV.INVOICE_NUMBER AS invoiceNumber, ");
		sql.append("     INV.INVOICE_DATE AS invoiceDate, ");
		sql.append("     INV.CUSTOMER_CODE AS customerCode, CUS.CUSTOMER_NAME AS customerName, CUS.ADDRESS AS customerAddress, CUS.INVOICE_TAX AS customerTax,  ");
		sql.append("     NVL(INV.SKU, 0) AS SKU, ");
		sql.append("     NVL(INV.VAT, 0) AS VAT, ");
		sql.append("     NVL(INV.AMOUNT, 0) AS amount, ");
		sql.append("     NVL(INV.AMOUNT + INV.TAX_AMOUNT, 0) AS amountWithTax, ");
		sql.append("     INV.STATUS AS status, ");
		sql.append("     INV.ORDER_NUMBER AS orderNumber ");
		sql.append(" FROM INVOICE INV, STAFF STA, CUSTOMER CUS, SALE_ORDER SO ");
		sql.append(" WHERE SO.ORDER_NUMBER = INV.ORDER_NUMBER ");
		sql.append("     AND INV.STAFF_ID = STA.STAFF_ID ");
		sql.append("     AND INV.CUSTOMER_CODE = CUS.CUSTOMER_CODE ");
		sql.append("     AND INV.INVOICE_DATE BETWEEN TRUNC(?) AND TRUNC(?) ");
		sql.append("     AND SO.SHOP_ID = ? ");
		
		params.add(fromDate);
		params.add(toDate);
		params.add(shopId);

		if (null != VAT) {
			sql.append("     AND INV.VAT = ? ");
			params.add(VAT);
		}
		if (null != staffId) {
			sql.append("     AND INV.STAFF_ID = ? ");
			params.add(staffId);
		}
		if (null != status) {
			sql.append("     AND INV.STATUS = ? ");
			params.add(status.getValue());
		}

		if (null != customerIds && customerIds.size() > 0) {
			sql.append("     AND CUS.CUSTOMER_ID IN (");

			for (int i = 0; i < customerIds.size(); i++) {
				if (i < customerIds.size() - 1) {
					sql.append(" ?,");
				} else {
					sql.append(" ?");
				}

				params.add(customerIds.get(i));
			}

			sql.append(") ");
		}

		sql.append(" ORDER BY INV.INVOICE_NUMBER ASC, INV.INVOICE_DATE ASC, CUS.CUSTOMER_CODE ASC");

		String[] fieldNames = { //
				"staffCode",//1
				"invoiceNumber",//2
				"invoiceDate",//3
				"customerCode",//4
				"customerName",//5
				"customerAddress",//6
				"customerTax",//7
				"SKU",//8
				"VAT",//9
				"amount",//10
				"amountWithTax",//11
				"status",//12
				"orderNumber"//13
		};

		Type[] fieldTypes = { //
			StandardBasicTypes.STRING, // 1
			StandardBasicTypes.STRING, // 2
			StandardBasicTypes.DATE, // 3
			StandardBasicTypes.STRING, // 4
			StandardBasicTypes.STRING, // 5
			StandardBasicTypes.STRING, // 6
			StandardBasicTypes.STRING, // 7
			StandardBasicTypes.INTEGER, // 8
			StandardBasicTypes.INTEGER, // 9
			StandardBasicTypes.BIG_DECIMAL, // 10
			StandardBasicTypes.BIG_DECIMAL, // 11
			StandardBasicTypes.INTEGER, // 12
			StandardBasicTypes.STRING, // 13
		};

		return repo.getListByQueryAndScalar(RptAggegateVATInvoiceDataVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<RptInvoiceComparingVO> getListRptInvoiceComparingVO(
			KPaging<RptInvoiceComparingVO> kPaging, Float vat, String customerCode,
			Long staffId, Date fromDate, Date toDate, Long shopId,
			ActiveType status) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptInvoiceComparingVO>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" select i.order_number as orderNumber,");
		sql.append("     (i.amount + i.tax_amount) as saleOverValue,");
		sql.append("     i.status as status,");
		sql.append("     i.customer_code as customerCode,");
		sql.append("     i.cust_name as customerName,");
		sql.append("     i.invoice_number as invoiceNumber,");
		sql.append("     i.order_date as orderDate,");
		sql.append("     i.invoice_date as invoiceDate");

		fromSql.append(" from invoice i join sale_order s on s.invoice_id = i.invoice_id");
		fromSql.append(" where 1 = 1");
		if (vat != null) {
			fromSql.append(" 	 and i.vat = ?");
			params.add(vat);
		}
		if (!StringUtility.isNullOrEmpty(customerCode)) {
			fromSql.append("     and i.customer_code = ?");
			params.add(customerCode);
		}
		if (staffId != null) {
			fromSql.append("     and i.staff_id = ?");
			params.add(staffId);
		}
		if (fromDate != null) {
			fromSql.append("     and i.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			fromSql.append("     and i.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		if (shopId != null) {
			fromSql.append("     and s.shop_id = ?");
			params.add(shopId);
		}
		if (status != null) {
			fromSql.append("     and i.status = ?");
			params.add(status.getValue());
		}
		sql.append(fromSql);
		sql.append("     order by orderNumber");
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count");
		countSql.append(fromSql);

		String[] fieldNames = { "orderNumber", // 0
				"saleOverValue", // 1
				"status", // 2
				"customerCode", // 3
				"customerName", // 4
				"invoiceNumber", // 5
				"orderDate", // 6
				"invoiceDate", // 7
		};

		Type[] fieldTypes = { StandardBasicTypes.STRING, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.INTEGER, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.DATE, // 6
				StandardBasicTypes.DATE, // 7
		};

		if (kPaging == null)
			return repo.getListByQueryAndScalar(RptInvoiceComparingVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(
					RptInvoiceComparingVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params,
					kPaging);
	}
	@Override
	public List<Invoice> getListInvoiceByLstInvoiceNumber(Long shopId, List<String> lstInvoiceNumber)
			throws DataAccessException {
		if (lstInvoiceNumber == null || lstInvoiceNumber.size() == 0) {
			throw new IllegalArgumentException("lstInvoiceNumber is null or empty");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM invoice WHERE status IN (?, ?) ");
		params.add(InvoiceStatus.USING.getValue());
		params.add(InvoiceStatus.CANCELED.getValue());
		if(shopId != null) {
			sql.append(" AND shop_id = ?");
			params.add(shopId);
		}
		sql.append(" and (1 != 1");
		for (String object : lstInvoiceNumber) {
			sql.append(" OR invoice_number = ?");
			params.add(object);
		}
		sql.append(" )");
		
		return repo.getListBySQL(Invoice.class, sql.toString(), params);
	}
	
	
	@Override
	public List<Invoice> getListInvoiceByLstInvoiceId(List<Long> lstInvoiceId)
			throws DataAccessException {
		if (lstInvoiceId == null || lstInvoiceId.size() == 0) {
			throw new IllegalArgumentException("lstInvoiceNumber is null or empty");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM invoice WHERE status IN (?, ?) ");
		params.add(InvoiceStatus.USING.getValue());
		params.add(InvoiceStatus.CANCELED.getValue());
		sql.append(" and (1 != 1");
		for (Long id : lstInvoiceId) {
			sql.append(" OR invoice_id = ?");
			params.add(id);
		}
		sql.append(" )");
		
		return repo.getListBySQL(Invoice.class, sql.toString(), params);
	}
	
	@Override
	public List<Invoice> getListInvoiceByLstInvoiceIdEx(List<Long> lstInvoiceId)
			throws DataAccessException {
		if (lstInvoiceId == null || lstInvoiceId.size() == 0) {
			throw new IllegalArgumentException("lstInvoiceNumber is null or empty");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM invoice WHERE status IN (?, ?) ");
		params.add(InvoiceStatus.USING.getValue());
		params.add(InvoiceStatus.CANCELED.getValue());
		sql.append(" and (1 != 1");
		for (Long id : lstInvoiceId) {
			sql.append(" OR invoice_id = ?");
			params.add(id);
		}
		sql.append(" )");
		sql.append(" order by order_number, INVOICE_NUMBER asc ");
		
		return repo.getListBySQL(Invoice.class, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.InvoiceDAO#getInvoiceByInvoiceNumber(java.lang.String)
	 */
	@Override
	public Invoice getInvoiceByInvoiceNumber(String invoiceNumber, Long shopId)
			throws DataAccessException {
		
		if (StringUtility.isNullOrEmpty(invoiceNumber)) {//bo dau !
			throw new IllegalArgumentException("invoiceNumber is null or empty");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM invoice WHERE status = ? and lower(invoice_number) = ?");
		params.add(InvoiceStatus.MODIFIED.getValue());
		params.add(invoiceNumber.toLowerCase());
		if (shopId != null) {
			sql.append(" and shop_id = ?");
			params.add(shopId);
		}
		//sql.append(" order by invoice_id desc");//tungtt
		
		return repo.getEntityBySQL(Invoice.class, sql.toString(), params);
	}
	
	/**
	 * tra ve true neu ko bi trung
	 */
	@Override
	public boolean checkDuplicateInvoiceNumber(Long shopId, List<String> lstInvoiceNumber)
			throws DataAccessException {
		
		if (shopId == null || shopId == 0) {//bo dau !
			throw new IllegalArgumentException("invoiceNumber is null or empty");
		}
		if(lstInvoiceNumber.size() == 0) {
			throw new IllegalArgumentException("lstInvoiceNumber is empty");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select count(1) as count");
		sql.append(" from invoice");
		sql.append(" where 1 = 1");
		sql.append(" and shop_id = ?");
		params.add(shopId);
		sql.append(" and (status = ? or status = ?)");
		params.add(InvoiceStatus.USING.getValue());
		params.add(InvoiceStatus.CANCELED.getValue());
		sql.append(" and ( upper(invoice_number) = ? ");
		params.add(lstInvoiceNumber.get(0).toUpperCase());
		for(int i = 1; i < lstInvoiceNumber.size(); i++) {
			sql.append(" or upper(invoice_number) = ?");
			params.add(lstInvoiceNumber.get(i).toUpperCase());
		}
		sql.append(")");
		
		int count = repo.countBySQL(sql.toString(), params);
		return count == 0;
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.InvoiceDAO#getRptViewInvoice(java.lang.Float, java.lang.String, java.lang.Long, java.util.Date, java.util.Date, java.lang.Long)
	 */
	@Override
	public List<RptViewInvoiceVO> getRptViewInvoice(
			KPaging<RptViewInvoiceVO> kPaging, Float vat, String customerCode,
			List<Long> listStaffId, Date fromDate, Date toDate,
			InvoiceCustPayment custPayment, String selName, String orderNumber,
			Long shopId, String invoiceNumber) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT iv.order_date orderDate,");
		sql.append(" iv.invoice_number invoiceNumber,");
		sql.append(" iv.cpny_name cpnyName,");
		sql.append(" iv.cpny_addr cpnyAddr,");
		sql.append(" iv.cpny_bank_account cpnyBankAccount,");
		sql.append(" iv.delivery_name deliveryName,");
		sql.append(" iv.sel_name selName,");
		sql.append(" st.staff_code staffCode,");
		sql.append(" st.staff_name staffName,");
		sql.append(" iv.order_number orderNumber,");
		sql.append(" iv.cust_name custName,");
		sql.append(" iv.cust_tax_number custTaxNumber,");
		sql.append(" iv.customer_code customerCode,");
		sql.append(" iv.customer_addr customerAddr,");
		sql.append(" iv.cust_payment custPayment,");
		sql.append(" iv.cust_del_addr custDelAddr,");
		sql.append(" iv.cust_bank_account custBankAccount,");
		sql.append(" iv.cust_bank_name custBankName,");
		sql.append(" iv.vat vat,");
		sql.append(" iv.amount amount,");
		sql.append(" iv.tax_amount taxAmount,");
		sql.append(" iv.discount discount,");
		sql.append(" sod.product_id productId,");
		sql.append(" nvl(sod.price_not_vat,0) priceNotVat,");
		sql.append(" sod.quantity quantity,");
		sql.append(" sod.is_free_item isFreeItem,");
		sql.append(" p.product_code productCode,");
		sql.append(" p.product_name productName,");
		sql.append(" p.uom1 uom1,");
		sql.append(" p.uom2 uom2,");
		sql.append(" p.convfact convfact,");
		sql.append(" (nvl(sod.price_not_vat,0) * NVL(sod.quantity,0)) toMoney");
//		sql.append(" (sod.price_not_vat * NVL(sod.quantity,0) + NVL(iv.tax_amount,0) - NVL(iv.discount,0)) AS totalAmount");
		sql.append(" FROM invoice iv,");
		sql.append(" sale_order_detail sod,");
		sql.append(" staff st,");
		sql.append(" product p");
		sql.append(" WHERE iv.invoice_id   = sod.invoice_id");
		sql.append(" AND p.product_id      = sod.product_id");
		sql.append(" AND iv.staff_id = st.staff_id");
		sql.append(" AND st.status	 = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND iv.status   = ?");
		params.add(InvoiceStatus.USING.getValue());
		if (vat != null) {
			sql.append(" AND iv.vat = ?");
			params.add(vat);
		}
		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append(" and upper(iv.customer_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(customerCode.toUpperCase()));
		}
		if (listStaffId != null) {
			sql.append("     and iv.staff_id in (");
			for (Long staffId: listStaffId) {
				sql.append("?, ");
				params.add(staffId);
			}
			sql.append(" 0)");
		} else {
			if (shopId != null) {
				sql.append("  and iv.staff_id in (");
				sql.append(" select s.staff_id from staff s, channel_type ct where s.shop_id = ?");
				sql.append(" and s.staff_type_id = ct.channel_type_id and ct.type = ? and (ct.object_type = ? or ct.object_type = ?))");
				params.add(shopId);
				params.add(ChannelTypeType.STAFF.getValue());
				params.add(StaffObjectType.NVBH.getValue());
				params.add(StaffObjectType.NVVS.getValue());
			}
		}
		if (fromDate != null) {
			sql.append("     and iv.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("     and iv.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		if (custPayment != null) {
			sql.append("     and iv.cust_payment = ?");
			params.add(custPayment.getValue());
		}
		if (!StringUtility.isNullOrEmpty(selName)) {
			sql.append(" and upper(iv.sel_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(selName.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(orderNumber)) {
			sql.append(" and upper(iv.order_number) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(orderNumber.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(invoiceNumber)) {
			sql.append(" and upper(iv.invoice_number) = ? ");
			params.add(invoiceNumber.toUpperCase());
		}
		sql.append(" ORDER BY st.staff_code, iv.order_number, iv.invoice_number, sod.is_free_item, p.product_code");
		
//		System.out.println(TestUtil.addParamToQuery(sql.toString(), params));
		
		String[] fieldNames = { "orderDate", // 0
				"invoiceNumber", // 1'
				"cpnyName", // 1
				"cpnyAddr", // 2
				"cpnyBankAccount", // 3
				"deliveryName", // 4
				"selName", // 5
				"staffCode", // 5'
				"staffName", // 5'
				"orderNumber", // 6
				"custName", // 7
				"custTaxNumber", // 8
				"customerCode", // 9
				"customerAddr", // 10
				"custPayment", // 11
				"custDelAddr", // 12
				"custBankAccount", // 13
				"custBankName", // 14
				"vat", // 15
				"amount", // 16
				"taxAmount", // 17
				"discount", // 17'
				"productId", // 18
				"priceNotVat", // 19
				"quantity", // 20
				"isFreeItem", // 20'
				"productCode", // 21
				"productName", // 22
				"uom1", // 23
				"uom2", // 23''
				"convfact", // 23'
				"toMoney", // 24
//				"totalAmount", // 25
		};
		
		Type[] fieldTypes = { StandardBasicTypes.TIMESTAMP, // 0
				StandardBasicTypes.STRING, // 1'
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, // 5'
				StandardBasicTypes.STRING, // 5''
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.STRING, // 7
				StandardBasicTypes.STRING, // 8
				StandardBasicTypes.STRING, // 9
				StandardBasicTypes.STRING, // 10
				StandardBasicTypes.INTEGER, // 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.STRING, // 13
				StandardBasicTypes.STRING, // 14
				StandardBasicTypes.FLOAT, // 15
				StandardBasicTypes.BIG_DECIMAL, // 16
				StandardBasicTypes.BIG_DECIMAL, // 17
				StandardBasicTypes.BIG_DECIMAL, // 17'
				StandardBasicTypes.LONG, // 18
				StandardBasicTypes.FLOAT, // 19
				StandardBasicTypes.INTEGER, // 20
				StandardBasicTypes.INTEGER, // 20'
				StandardBasicTypes.STRING, // 21
				StandardBasicTypes.STRING, // 22
				StandardBasicTypes.STRING, // 23
				StandardBasicTypes.STRING, // 23''
				StandardBasicTypes.INTEGER, // 23'
				StandardBasicTypes.BIG_DECIMAL, // 24
//				StandardBasicTypes.BIG_DECIMAL, // 25
		};

		if (kPaging == null) {
			return repo.getListByQueryAndScalar(
					RptViewInvoiceVO.class, fieldNames,
					fieldTypes, sql.toString(), params);
		}
		else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(*) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(
					RptViewInvoiceVO.class, fieldNames,
					fieldTypes, sql.toString(), countSql.toString(), params,
					params, kPaging);
		}
	}

	@Override
	public InvoiceDetail createInvoiceDetail(InvoiceDetail invoiceDetail)
			throws DataAccessException {
		if (invoiceDetail == null) {
			throw new IllegalArgumentException("invoiceDetail");
		}
		repo.create(invoiceDetail);
		return repo.getEntityById(InvoiceDetail.class, invoiceDetail.getId());
	}

	@Override
	public List<Invoice> getListInvoiceBySaleOrderId(Long saleOrderId)
			throws DataAccessException {
		if (saleOrderId == null) {
			throw new IllegalArgumentException("saleOrderId is null or empty");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from invoice ");
		sql.append("where Sale_Order_Id = ? and Status = ?");
		params.add(saleOrderId);
		params.add(InvoiceStatus.USING.getValue());
		return repo.getListBySQL(Invoice.class, sql.toString(), params);
	}
	@Override
	public List<Invoice> getListInvoiceBySaleOrderId2(long saleOrderId, int invoiceType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from invoice ");
		sql.append("where sale_order_id = ? and status = ? and sale_order_type = ?");
		params.add(saleOrderId);
		params.add(InvoiceStatus.USING.getValue());
		params.add(invoiceType);
		return repo.getListBySQL(Invoice.class, sql.toString(), params);
	}

	@Override
	public List<Invoice> getListInvoiceCanceledByCondition(KPaging<Invoice> kPaging,
			Float taxValue, String orderNumber, String redInvoiceNumber, Date fromDate, Date toDate,
			String staffCode, String deliveryCode, String shortCode, InvoiceCustPayment custPayment, Date lockDate, Long shopId, Integer isValueOrder) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<Invoice>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from invoice where sale_order_id in ");
		sql.append("( select i.sale_order_id from invoice i"); 
		sql.append(" left join staff ss on i.staff_id = ss.staff_id");
		sql.append(" left join staff ds on i.delivery_id = ds.staff_id");
		sql.append(" where 1 = 1");		
		if (taxValue != null) {
			sql.append(" and i.vat=?");
			params.add(taxValue);
		}
		if(isValueOrder!=null && isValueOrder == 0){
			sql.append(" and i.amount <= 0 ");
		}
		if(isValueOrder!=null && isValueOrder == 1){
			sql.append(" and i.amount > 0 ");
		}
		if (shopId != null) {
			sql.append(" and i.shop_id = ? ");
			params.add(shopId);
		}
		if (orderNumber != null) {
			sql.append(" and upper(i.order_number) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(orderNumber.toUpperCase()));
		}
		if (redInvoiceNumber != null) {
			sql.append(" and upper(i.invoice_number) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(redInvoiceNumber.toUpperCase()));
		}
		if (fromDate != null) {
			sql.append(" and i.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			if(DateUtility.compareDateWithoutTime(toDate, lockDate) == 1){
				sql.append(" and i.order_date < trunc(?) + 1");
				params.add(lockDate);
			}else{
				sql.append(" and i.order_date < trunc(?) + 1");
				params.add(toDate);
			}
		}
		if(!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and upper(ss.staff_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(staffCode.toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(deliveryCode)) {
			sql.append(" and upper(ds.staff_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(deliveryCode.toUpperCase()));
		}
		if (shortCode != null) {
			sql.append(" and i.customer_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
		}
		if (custPayment != null) {
			sql.append(" and i.cust_payment = ?");
			params.add(custPayment.getValue());
		}
		sql.append(" and i.status = ? )");
		sql.append(" and status = ? ");
		params.add(InvoiceStatus.USING.getValue());
		params.add(InvoiceStatus.CANCELED.getValue());
		sql.append(" order by order_number, invoice_id "); //tungtt
		
		return repo.getListBySQL(Invoice.class, sql.toString(), params);
	}
	
	@Override
	public List<Invoice> getListInvoiceCanceledByConditionEx1(
			KPaging<SaleOrder> kPaging, Long shopId, String saleStaffCode,
			String deliveryStaffCode, String orderNumber, Date fromDate,
			Date toDate, String customerCode, Date lockDate,
			Integer isValueOrder, Integer numberValueOrder) throws DataAccessException {
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<Invoice>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" With saleOrder as(");
		sql.append(" SELECT distinct so.*");
		sql.append(" FROM sale_order so");
		sql.append(" INNER JOIN customer c ON so.customer_id = c.customer_id");
		sql.append(" LEFT JOIN staff ss ON so.staff_id = ss.staff_id");
		sql.append(" LEFT JOIN staff ds ON so.delivery_id = ds.staff_id");
		sql.append(" INNER JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id");
		sql.append(" WHERE 1          = 1");
		if(isValueOrder!=null && isValueOrder == 0){
			sql.append(" and so.amount < ? ");
			params.add(numberValueOrder);
		}
		if(isValueOrder!=null && isValueOrder == 1){
			sql.append(" and so.amount >= ? ");
			params.add(numberValueOrder);
		}
		if(shopId != null) {
			sql.append(" AND so.shop_id = ?");
			params.add(shopId);
		}
		if(customerCode != null) {
			sql.append(" AND lower(c.short_code) LIKE ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode.toLowerCase()));
		}
		if(saleStaffCode != null) {
			sql.append(" AND lower(ss.staff_code) LIKE ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(saleStaffCode.toLowerCase()));
		}
		if(deliveryStaffCode != null) {
			sql.append(" AND lower(ds.staff_code) LIKE ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(deliveryStaffCode.toLowerCase()));
		}
		sql.append(" AND sod.invoice_id is null and (sod.program_type is null or sod.program_type = 0 or sod.program_type = 1 or sod.program_type = 2)");
		sql.append(" AND so.order_type IN (?, ?)");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		sql.append(" AND so.approved = ?");
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append(" AND so.type = ?");
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		if(orderNumber != null) {
			sql.append(" AND lower(so.order_number) LIKE ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(orderNumber.toLowerCase()));
		}
		if(fromDate != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if(toDate != null) {
			if(DateUtility.compareDateWithoutTime(toDate, lockDate) == 1){
				sql.append(" AND so.order_date < TRUNC(?) + 1");
				params.add(lockDate);
			}else{
				sql.append(" AND so.order_date < TRUNC(?) + 1");
				params.add(toDate);
			}
		}
		sql.append(" AND so.sale_order_id in (");
		sql.append(" select sale_order_id ");
		sql.append(" from invoice ");
		sql.append(" where status = 0");
		if(shopId != null) {
			sql.append(" AND shop_id = ?");
			params.add(shopId);
		}
		if(fromDate != null) {
			sql.append(" AND order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if(toDate != null) {
			if(DateUtility.compareDateWithoutTime(toDate, lockDate) == 1){
				sql.append(" AND order_date < TRUNC(?) + 1");
				params.add(lockDate);
			}else{
				sql.append(" AND order_date < TRUNC(?) + 1");
				params.add(toDate);
			}
		}
		sql.append(" )");
		sql.append(" ORDER BY so.order_number");
		sql.append(" ),");
		sql.append(" sumInvoice as(");
		sql.append(" select sale_order_id, sum(amount) as amount");
		sql.append(" from invoice ");
		sql.append(" where status = ?");
		params.add(InvoiceStatus.USING.getValue());
		if(shopId != null) {
			sql.append(" AND shop_id = ?");
			params.add(shopId);
		}
		if(fromDate != null) {
			sql.append(" AND order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if(toDate != null) {
			if(DateUtility.compareDateWithoutTime(toDate, lockDate) == 1){
				sql.append(" AND order_date < TRUNC(?) + 1");
				params.add(lockDate);
			}else{
				sql.append(" AND order_date < TRUNC(?) + 1");
				params.add(toDate);
			}
		}
		sql.append(" group by sale_order_id");
		sql.append(" ),");
		sql.append(" joinOrder as(");
		sql.append(" select so.*");
		sql.append(" from saleOrder so");
		sql.append(" join sumInvoice i on so.sale_order_id = i.sale_order_id");
		sql.append(" where so.amount > i.amount");
		sql.append(" and i.amount > 0");
		sql.append(" )");
		
		sql.append(" select distinct i.* ");
		sql.append(" from invoice i");
		sql.append(" where i.sale_order_id in (select sale_order_id from joinOrder)");
		sql.append(" and i.status = ?");
		params.add(InvoiceStatus.CANCELED.getValue());
		sql.append(" and i.amount not in (select amount from invoice where sale_order_id = i.sale_order_id");
		sql.append(" and status = ?)");
		sql.append(" order by i.sale_order_id, i.amount, i.invoice_id asc");
		params.add(InvoiceStatus.USING.getValue());
		List<Invoice> listInvoice = repo.getListBySQL(Invoice.class, sql.toString(), params);
		List<Invoice> listResult = new ArrayList<Invoice>();
		if(listInvoice != null){
			if(listInvoice.size() > 1){
				Invoice tmpInvoice = null;
				for(int i = 0; i < listInvoice.size(); i++){
					if(tmpInvoice != null){
						if(listInvoice.get(i).getSaleOrder().getId() != tmpInvoice.getSaleOrder().getId()){
							tmpInvoice = listInvoice.get(i);
							listResult.add(tmpInvoice);
						}else{
							if(listInvoice.get(i).getAmount().compareTo(tmpInvoice.getAmount()) != 0){
								tmpInvoice = listInvoice.get(i);
								listResult.add(tmpInvoice);
							}
						}
					}else{
						tmpInvoice = listInvoice.get(i);
						listResult.add(tmpInvoice);
					}
				}
			}else if(listInvoice.size() == 1){
				listResult.add(listInvoice.get(0));
			}
		}
		return listResult;
	}
	
	@Override
	public List<Invoice> checkHasInvoiceRemoveOfSaleOrder(Long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" With saleOrder as(");
		sql.append(" SELECT so.*");
		sql.append(" FROM sale_order so");
		sql.append(" where so.sale_order_id = ?");
		params.add(saleOrderId);
		sql.append(" ),");
		sql.append(" sumInvoice as(");
		sql.append(" select sale_order_id, sum(amount) as amount");
		sql.append(" from invoice ");
		sql.append(" where status = ?");
		params.add(InvoiceStatus.USING.getValue());
		sql.append(" and sale_order_id = ?");
		params.add(saleOrderId);
		sql.append(" group by sale_order_id");
		sql.append(" ),");
		sql.append(" joinOrder as(");
		sql.append(" select so.*");
		sql.append(" from saleOrder so");
		sql.append(" join sumInvoice i on so.sale_order_id = i.sale_order_id");
		sql.append(" where so.amount > i.amount");
		sql.append(" and i.amount > 0");
		sql.append(" )");
		
		sql.append(" select distinct i.* ");
		sql.append(" from invoice i");
		sql.append(" where i.sale_order_id in (select sale_order_id from joinOrder)");
		sql.append(" and i.status = ?");
		params.add(InvoiceStatus.CANCELED.getValue());
		sql.append(" and i.amount not in (select amount from invoice where sale_order_id = i.sale_order_id");
		sql.append(" and status = ?)");
		params.add(InvoiceStatus.USING.getValue());
		return repo.getListBySQL(Invoice.class, sql.toString(), params);
	}

	@Override
	public List<InvoiceDetail> getListInvoiceDetailByInvoiceId(Long invoiceId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from invoice_detail where 1 = 1");
		if(invoiceId!=null && invoiceId > 0){
			sql.append(" and invoice_id = ? ");
			params.add(invoiceId);
		}
		return repo.getListBySQL(InvoiceDetail.class, sql.toString(), params);
	}
	@Override
	public void updateInvoiceDetail(List<InvoiceDetail> lstInvoiceDetail)
			throws DataAccessException {
		
		if (lstInvoiceDetail.isEmpty()) {
			throw new IllegalArgumentException("lstInvoiceDetail");
		}
		repo.update(lstInvoiceDetail);
	}

	@Override
	public List<Invoice> getListInvoiceByConditionByNumber(KPaging<Invoice> kPaging, Float taxValue, String orderNumber, String redInvoiceNumber, Date fromDate, Date toDate, String staffCode, String deliveryCode, String shortCode,
			InvoiceCustPayment custPayment, Date lockDate, Long shopId, Integer isValueOrder, Boolean isVATGop, Integer numberValueOrder) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<Invoice>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select i.* from invoice i"); 
		sql.append(" left join staff ss on i.staff_id = ss.staff_id");
		sql.append(" left join staff ds on i.delivery_id = ds.staff_id");
		sql.append(" where 1 = 1");		
		if (taxValue != null) {
			sql.append(" and i.vat=?");
			params.add(taxValue);
		}
		if(isValueOrder!=null && isValueOrder == 0 && numberValueOrder!= null){
			sql.append(" and i.amount < ? ");
			params.add(numberValueOrder);
		}
		if(isValueOrder!=null && isValueOrder == 1 && numberValueOrder != null){
			sql.append(" and i.amount >= ? ");
			params.add(numberValueOrder);
		}
		if (shopId != null) {
			sql.append(" and i.shop_id = ? ");
			params.add(shopId);
		}
		if (orderNumber != null) {
			sql.append(" and upper(i.order_number) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(orderNumber.toUpperCase()));
		}
		if (redInvoiceNumber != null) {
			sql.append(" and upper(i.invoice_number) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(redInvoiceNumber.toUpperCase()));
		}
		if (fromDate != null) {
			sql.append(" and i.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			if(DateUtility.compareDateWithoutTime(toDate, lockDate) == 1){
				sql.append(" and i.order_date < trunc(?) + 1");
				params.add(lockDate);
			}else{
				sql.append(" and i.order_date < trunc(?) + 1");
				params.add(toDate);
			}
			
		}
		if(!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and upper(ss.staff_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(staffCode.toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(deliveryCode)) {
			sql.append(" and upper(ds.staff_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(deliveryCode.toUpperCase()));
		}
		/* if (staffCode != null) {
			 sql.append(" and exists (select 1 from staff where staff.staff_id = invoice.staff_id and staff.staff_code like ? ESCAPE '/' )");
			 params.add(StringUtility.toOracleSearchLikeSuffix(staffCode));
		 }*/
		if (shortCode != null) {
			sql.append(" and i.customer_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
		}
		if (custPayment != null) {
			sql.append(" and i.cust_payment = ?");
			params.add(custPayment.getValue());
		}
		if(isVATGop!=null){
			if(isVATGop){
				sql.append(" and i.status in (?,?)");
				params.add(InvoiceStatus.USING.getValue());
				params.add(InvoiceStatus.CANCELED.getValue());
				sql.append(" and i.sale_order_type = 1");
				sql.append(" and i.sale_order_id in (select pri_sale_order_id from compound_sale_order where shop_id = ?)");		
				params.add(shopId);
			}else{
				sql.append(" and i.status = ?");
				params.add(InvoiceStatus.USING.getValue());
				sql.append(" and i.sale_order_type = 0");
			}
		}else{
			sql.append(" and i.status = ?");
			params.add(InvoiceStatus.USING.getValue());
		}
		sql.append(" order by i.order_number, i.invoice_id "); //tungtt
		if (kPaging != null) {
			return repo.getListBySQLPaginated(Invoice.class, sql.toString(),
					params, kPaging);
		} else {
			return repo.getListBySQL(Invoice.class, sql.toString(), params);
		}
	}
}

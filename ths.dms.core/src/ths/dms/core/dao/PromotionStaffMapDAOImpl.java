package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PromotionStaffMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.PromotionShopMapFilter;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.PromotionStaffFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PromotionShopMapVO;
import ths.dms.core.entities.vo.PromotionStaffVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class PromotionStaffMapDAOImpl implements PromotionStaffMapDAO {

	@Autowired
	IRepository repo;
	
	@Autowired
	CommonDAO commonDAO;
	
	/**
	 * @author lacnv1
	 */
	@Override
	public PromotionStaffMap getPromotionStaffMapById(long id) throws DataAccessException {
		return repo.getEntityById(PromotionStaffMap.class, id);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionStaffVO> getSalerInPromotionProgram(long promotionId, String shopCode, String shopName,
			Integer quantity) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("with");
		
		if (!StringUtility.isNullOrEmpty(shopCode) || !StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" lstShopTmp as (");
			sql.append("select shop_id from shop where 1=1");
			/*sql.append(" select distinct shop_id");
			sql.append(" from shop");
			sql.append(" start with 1=1");*/
			if (!StringUtility.isNullOrEmpty(shopCode)) {
				sql.append(" and lower(shop_code) like ? escape '/'");
				shopCode = shopCode.toLowerCase();
				shopCode = StringUtility.toOracleSearchLike(shopCode);
				params.add(shopCode);
			}
			if (!StringUtility.isNullOrEmpty(shopName)) {
				sql.append(" and lower(shop_name) like ? escape '/'");
				shopName = shopName.toLowerCase();
				shopName = StringUtility.toOracleSearchLike(shopName);
				params.add(shopName);
			}
			/*sql.append(" connect by prior shop_id = parent_shop_id");*/
			sql.append(" ),");
		}
		
		sql.append(" lstShopMap as (");
		sql.append(" select psm.promotion_shop_map_id");
		sql.append(" from promotion_shop_map psm");
		sql.append(" where promotion_program_id = ?");
		params.add(promotionId);
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (!StringUtility.isNullOrEmpty(shopCode) || !StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and psm.shop_id in (select shop_id from lstShopTmp)");
		}
		sql.append(" ),");
		
		sql.append("lstStaffMap as (");
		sql.append(" select promotion_staff_map_id as mapId, staff_id as id, shop_id as parentId,");
		sql.append(" (select staff_code from staff where staff_id = psm.staff_id) as code,");
		sql.append(" (select staff_name from staff where staff_id = psm.staff_id) as name,");
//		sql.append(" quantity_max as quantity, quantity_received as receivedQtt,");
		sql.append(" quantity_max as quantity, QUANTITY_RECEIVED_TOTAL as receivedQtt,");
//		sql.append(" amount_max      AS amountMax,amount_received AS receivedAmt,");
		sql.append(" amount_max      AS amountMax, AMOUNT_RECEIVED_TOTAL AS receivedAmt,");
//		sql.append(" num_max      AS numMax,num_received AS receivedNum");
		sql.append(" num_max      AS numMax, NUM_RECEIVED_TOTAL AS receivedNum");
		sql.append(" from promotion_staff_map psm");
		sql.append(" where promotion_shop_map_id in (select promotion_shop_map_id from lstShopMap)");
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (quantity != null) {
			sql.append(" and quantity_max = ?");
			params.add(quantity);
		}
		sql.append(" ),");
		sql.append(" lstDV as (");
		sql.append(" select distinct parent_shop_id as parentId, shop_id as id, shop_code as code, shop_name as name,");
		sql.append("(select sum(quantity_max) from promotion_staff_map");
		sql.append(" where promotion_shop_map_id in (select promotion_shop_map_id from lstShopMap)");
		sql.append(" and shop_id in (select shop_id from shop start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id)");
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ) as quantity,");
//		sql.append(" (select sum(quantity_received) from promotion_staff_map");
		sql.append(" (select sum(QUANTITY_RECEIVED_TOTAL) from promotion_staff_map");
		sql.append(" where promotion_shop_map_id in (select promotion_shop_map_id from lstShopMap)");
		sql.append(" and shop_id in (select shop_id from shop start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id)");
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ) as receivedQtt,");
		sql.append(" (SELECT SUM(amount_max) ");
		sql.append(" FROM promotion_staff_map ");
		sql.append(" WHERE promotion_shop_map_id IN ");
		sql.append("   (SELECT promotion_shop_map_id FROM lstShopMap ");
		sql.append("   ) ");
		sql.append("  AND shop_id IN ");
		sql.append("    (SELECT shop_id ");
		sql.append("   FROM shop ");
		sql.append("     START WITH shop_id       = s.shop_id ");
		sql.append("     CONNECT BY prior shop_id = parent_shop_id ");
		sql.append("  ) ");
		sql.append(" AND status = 1 ");
		sql.append("  ) AS amountMax, ");
//		sql.append(" (SELECT SUM(amount_received) ");
		sql.append(" (SELECT SUM(AMOUNT_RECEIVED_TOTAL) ");
		sql.append(" FROM promotion_staff_map ");
		sql.append("  WHERE promotion_shop_map_id IN ");
		sql.append("  (SELECT promotion_shop_map_id FROM lstShopMap ");
		sql.append("  ) ");
		sql.append("  AND shop_id IN ");
		sql.append("   (SELECT shop_id ");
		sql.append("  FROM shop ");
		sql.append("    START WITH shop_id       = s.shop_id ");
		sql.append("     CONNECT BY prior shop_id = parent_shop_id ");
		sql.append("  ) ");
		sql.append(" AND status = 1 ");
		sql.append(" ) AS receivedAmt, ");
		sql.append("  (SELECT SUM(num_max) ");
		sql.append(" FROM promotion_staff_map ");
		sql.append("  WHERE promotion_shop_map_id IN ");
		sql.append("   (SELECT promotion_shop_map_id FROM lstShopMap ");
		sql.append("  ) ");
		sql.append(" AND shop_id IN ");
		sql.append("   (SELECT shop_id ");
		sql.append("  FROM shop ");
		sql.append("    START WITH shop_id       = s.shop_id ");
		sql.append("    CONNECT BY prior shop_id = parent_shop_id ");
		sql.append("   ) ");
		sql.append(" AND status = 1 ");
		sql.append(" ) AS numMax, ");
//		sql.append(" (SELECT SUM(num_received) ");
		sql.append(" (SELECT SUM(NUM_RECEIVED_TOTAL) ");
		sql.append("  FROM promotion_staff_map ");
		sql.append(" WHERE promotion_shop_map_id IN ");
		sql.append("    (SELECT promotion_shop_map_id FROM lstShopMap ");
		sql.append("  ) ");
		sql.append("  AND shop_id IN ");
		sql.append("   (SELECT shop_id ");
		sql.append("  FROM shop ");
		sql.append("    START WITH shop_id       = s.shop_id ");
		sql.append("    CONNECT BY prior shop_id = parent_shop_id ");
		sql.append("   ) ");
	    sql.append("  AND status = 1 ");
	    sql.append(" ) AS receivedNum, ");
		sql.append(" NVL(ct.SPECIFIC_TYPE, 0) specific, ");
		sql.append(" level lv "); 
		sql.append(" from shop s");
		sql.append(" join shop_type ct on (ct.shop_type_id = s.shop_type_id)");
		sql.append(" start with shop_id in (select parentId from lstStaffMap)");
		sql.append(" connect by prior parent_shop_id = shop_id");
		sql.append(" )");
		sql.append(" select mapId, id, parentId, code, name, quantity, nvl(receivedQtt, 0) as receivedQtt,amountMax,NVL(receivedAmt, 0) as receivedAmt,numMax,NVL(receivedNum, 0) as receivedNum, isSaler");
		sql.append(" from (");
		sql.append(" select null as mapId, id, parentId,");
		sql.append(" code, name,");
		sql.append(" quantity, receivedQtt,amountMax,receivedAmt,numMax,receivedNum,");
		sql.append(" 0 as isSaler, ");
		sql.append(" specific, ");
		sql.append(" lv ");
		sql.append(" from lstDV");
		sql.append(" union all");
		sql.append(" select mapId, id, parentId,");
		sql.append(" code, name,");
		sql.append(" quantity, receivedQtt, amountMax,receivedAmt,numMax,receivedNum,");
		sql.append(" 1 as isSaler, ");
		sql.append(" 2 AS specific, ");
		sql.append(" 0 as lv ");
		sql.append(" from lstStaffMap");
		sql.append(" )");
		sql.append(" order by isSaler, specific, lv DESC, code, name");
		
		String[] fieldNames = new String[] {
				"mapId",//1
				"id",//2
				"parentId",//3
				"code",//4
				"name",//5
				"quantity",//6
				"receivedQtt",//7
				"amountMax",//8
				"receivedAmt",//9
				"numMax",//10
				"receivedNum",//11
				"isSaler"//12
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,//1
				StandardBasicTypes.LONG,//2
				StandardBasicTypes.LONG,//3
				StandardBasicTypes.STRING,//4
				StandardBasicTypes.STRING,//5
				StandardBasicTypes.BIG_DECIMAL,//6
				StandardBasicTypes.BIG_DECIMAL,//7
				StandardBasicTypes.BIG_DECIMAL,//8
				StandardBasicTypes.BIG_DECIMAL,//9
				StandardBasicTypes.BIG_DECIMAL,//10
				StandardBasicTypes.BIG_DECIMAL,//11
				StandardBasicTypes.INTEGER//12
		};
		
		List<PromotionStaffVO> lst = repo.getListByQueryAndScalar(PromotionStaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public void updatePromotionStaffMap(PromotionStaffMap promotionStaffMap, LogInfoVO logInfo) throws DataAccessException {
		if (logInfo != null) {
			promotionStaffMap.setUpdateUser(logInfo.getStaffCode());
		}
		Date now = commonDAO.getSysDate();
		promotionStaffMap.setUpdateDate(now);
		
		repo.update(promotionStaffMap);
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public PromotionStaffMap createPromotionStaffMap(PromotionStaffMap promotionStaffMap, LogInfoVO logInfo) throws DataAccessException {
		if (logInfo != null) {
			promotionStaffMap.setCreateUser(logInfo.getStaffCode());
		}
		Date now = commonDAO.getSysDate();
		promotionStaffMap.setCreateDate(now);
		
		return repo.create(promotionStaffMap);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionStaffMap> getListPromotionStaffMap(long promotionId, long shopId, Integer status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select *");
		sql.append(" from promotion_staff_map psm");
		sql.append(" where promotion_shop_map_id in (");
		
		sql.append(" select psm.promotion_shop_map_id");
		sql.append(" from promotion_shop_map psm");
		sql.append(" where promotion_program_id = ?");
		params.add(promotionId);
		sql.append(" and shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
		params.add(shopId);
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		
		sql.append(")");
		
		if (status != null) {
			sql.append(" and status = ?");
			params.add(status);
		} else {
			sql.append(" and status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		List<PromotionStaffMap> lst = repo.getListBySQL(PromotionStaffMap.class, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionStaffVO> searchStaffForPromotion(PromotionStaffFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		if (StringUtility.isNullOrEmpty(filter.getCode()) && StringUtility.isNullOrEmpty(filter.getName())) {
			if (ShopSpecificType.NPP.getValue().equals(filter.getShopType())) {
				sql.append("with lstShopMap as (");
				sql.append(" select psm.promotion_shop_map_id");
				sql.append(" from promotion_shop_map psm");
				sql.append(" where promotion_program_id = ? and status = ?");
				params.add(filter.getPromotionId());
				params.add(ActiveType.RUNNING.getValue());
				sql.append(" ),");
				sql.append(" lstStaffMap as (");
				sql.append(" select staff_id");
				sql.append(" from promotion_staff_map psm");
				sql.append(" where promotion_shop_map_id in (select promotion_shop_map_id from lstShopMap)");
				sql.append(" and status = ?");
				params.add(ActiveType.RUNNING.getValue());
				sql.append(" )");
				sql.append("select staff_id as id, shop_id as parentId, staff_code as code, staff_name as name, 1 as isSaler");
				sql.append(" from staff st");
				sql.append(" join STAFF_TYPE stt on st.staff_type_id = stt.staff_type_id ");
				sql.append(" where staff_id not in (select staff_id from lstStaffMap)");
				sql.append(" and st.status = ? ");
				params.add(ActiveType.RUNNING.getValue());
				sql.append(" and stt.SPECIFIC_TYPE = ? ");
				params.add(StaffSpecificType.STAFF.getValue());
				sql.append(" and stt.status = ? ");
				params.add(ActiveType.RUNNING.getValue());
				sql.append(" and shop_id = ?");
				params.add(filter.getShopId());
				if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
					String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "shop_id");
					sql.append(paramsFix);
				}
			} else {
				sql.append("select shop_id as id, parent_shop_id as parentId,");
				sql.append("shop_code as code, shop_name as name, 0 as isSaler");
				sql.append(" from shop");
				sql.append(" where status = ? ");
				params.add(ActiveType.RUNNING.getValue());
				if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
					String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "shop_id");
					sql.append(paramsFix);
				}
				sql.append(" and parent_shop_id = ?");
				params.add(filter.getShopId());
				sql.append(" and (");
				sql.append("shop_id in (");
				sql.append("select distinct shop_id from shop where status = ?");
				params.add(ActiveType.RUNNING.getValue());
				sql.append(" start with shop_id in (");
				sql.append(" select psm.shop_id");
				sql.append(" from promotion_shop_map psm");
				sql.append(" where promotion_program_id = ? and status = ?");
				params.add(filter.getPromotionId());
				params.add(ActiveType.RUNNING.getValue());
				sql.append(") connect by prior shop_id = parent_shop_id)");
				sql.append(" or shop_id in (");
				sql.append("select distinct shop_id from shop where status = ?");
				params.add(ActiveType.RUNNING.getValue());
				sql.append(" start with shop_id in (");
				sql.append(" select psm.shop_id");
				sql.append(" from promotion_shop_map psm");
				sql.append(" where promotion_program_id = ? and status = ?");
				params.add(filter.getPromotionId());
				params.add(ActiveType.RUNNING.getValue());
				sql.append(") connect by prior parent_shop_id = shop_id)");
				sql.append(")");
			}
			
			sql.append(" order by code, name");
		} else {
			sql.append("with lstShopId as (");
			sql.append(" select shop_id");
			sql.append(" from shop");
			sql.append(" where status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
				String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "shop_id");
				sql.append(paramsFix);
			}
			sql.append(" and (");
			sql.append("shop_id in (");
			sql.append("select distinct shop_id from shop where status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" start with shop_id in (");
			sql.append(" select psm.shop_id");
			sql.append(" from promotion_shop_map psm");
			sql.append(" where promotion_program_id = ? and status = ?");
			params.add(filter.getPromotionId());
			params.add(ActiveType.RUNNING.getValue());
			sql.append(") connect by prior shop_id = parent_shop_id)");
			sql.append(" or shop_id in (");
			sql.append("select distinct shop_id from shop where status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" start with shop_id in (");
			sql.append(" select psm.shop_id");
			sql.append(" from promotion_shop_map psm");
			sql.append(" where promotion_program_id = ? and status = ?");
			params.add(filter.getPromotionId());
			params.add(ActiveType.RUNNING.getValue());
			sql.append(") connect by prior parent_shop_id = shop_id)");
			sql.append(")");
			sql.append(" start with shop_id = ? connect by prior shop_id = parent_shop_id");
			params.add(filter.getShopId());
			sql.append(" ),");
			sql.append(" lstShopMap as (");
			sql.append(" select psm.promotion_shop_map_id");
			sql.append(" from promotion_shop_map psm");
			sql.append(" where promotion_program_id = ? and status = ?");
			params.add(filter.getPromotionId());
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" ),");
			sql.append(" lstStaffMap as (");
			sql.append(" select staff_id");
			sql.append(" from promotion_staff_map psm");
			sql.append(" where promotion_shop_map_id in (select promotion_shop_map_id from lstShopMap)");
			sql.append(" and status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" ),");
			sql.append(" lstStaff as (");
			sql.append(" select staff_id as id, shop_id as parentId,");
			sql.append(" staff_code as code, staff_name as name");
			sql.append(" from staff st");
			sql.append(" JOIN STAFF_TYPE stt on st.STAFF_TYPE_ID = stt.STAFF_TYPE_ID ");
			sql.append(" where staff_id not in (select staff_id from lstStaffMap)");
			sql.append(" and st.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" AND stt.SPECIFIC_TYPE = ? ");
			params.add(StaffSpecificType.STAFF.getValue());
			sql.append(" AND stt.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" and shop_id in (select shop_id from lstShopId)");
			if (!StringUtility.isNullOrEmpty(filter.getCode())) {
				sql.append(" and staff_code like ? escape '/'");
				String s = filter.getCode().toUpperCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (!StringUtility.isNullOrEmpty(filter.getName())) {
				sql.append(" and unicode2english(staff_name) like ? escape '/'");
				String s = filter.getName().trim().toLowerCase();
				s = Unicode2English.codau2khongdau(s);
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			sql.append(" ),");
			sql.append(" lstDV as (");
			sql.append(" select distinct parent_shop_id as parentId, shop_id as id, shop_code as code, shop_name as name,");
			sql.append(" NVL(ct.SPECIFIC_TYPE, 0) specificType, ");
			sql.append(" level lv ");
			sql.append(" from shop s");
			sql.append(" join shop_type ct on (ct.shop_type_id = s.shop_type_id)");
			sql.append(" where s.shop_id in (select shop_id from lstShopId)");
			sql.append(" start with shop_id in (select parentId from lstStaff)");
			sql.append(" connect by prior parent_shop_id = shop_id");
			sql.append(" )");
			sql.append(" select id, parentId, code, name, isSaler");
			sql.append(" from (");
			sql.append(" select id, parentId,");
			sql.append(" code, name,");
			sql.append(" 0 as isSaler, ");
			sql.append(" specificType, ");
			sql.append(" lv ");
			sql.append(" from lstDV");
			sql.append(" union all");
			sql.append(" select id, parentId,");
			sql.append(" code, name,");
			sql.append(" 1 as isSaler, ");
			sql.append(" 2 AS specificType, ");
			sql.append(" 0 as lv ");
			sql.append(" from lstStaff");
			sql.append(" )");
			sql.append(" order by isSaler, specificType, lv DESC, code, name");
		}
		
		String[] fieldNames = new String[] {
				"id",
				"parentId",
				"code",
				"name",
				"isSaler"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER
		};
		
		List<PromotionStaffVO> lst = repo.getListByQueryAndScalar(PromotionStaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionStaffVO> getListStaffInPromotion(long promotionId, List<Long> lstStaffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select staff_id as id, shop_id as parentId,");
		sql.append(" (select staff_code from staff where staff_id = psm.staff_id) as code,");
		sql.append(" (select staff_name from staff where staff_id = psm.staff_id) as name");
		sql.append(" from promotion_staff_map psm");
		sql.append(" where promotion_shop_map_id in (");
		sql.append(" select promotion_shop_map_id");
		sql.append(" from promotion_shop_map");
		sql.append(" where promotion_program_id = ? and status = ?");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(")");
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (lstStaffId != null && lstStaffId.size() > 0) {
			sql.append(" and staff_id in (-1");
			for (Long idt : lstStaffId) {
				sql.append(",?");
				params.add(idt);
			}
			sql.append(")");
		}
		
		String[] fieldNames = new String[] {
				"id",
				"parentId",
				"code",
				"name"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING
		};
		
		List<PromotionStaffVO> lst = repo.getListByQueryAndScalar(PromotionStaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public void createListPromotionStaffMap(List<PromotionStaffMap> lst, LogInfoVO logInfo) throws DataAccessException {
		for(PromotionStaffMap promotionStaffMap : lst) {
			if(promotionStaffMap.getId() != null) {
				repo.update(promotionStaffMap);
			} else {
				repo.create(promotionStaffMap);
			}
		}
//		repo.create(lst);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionStaffMap> getListPromotionStaffMapByShopMap(long shopMapId, Integer status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select *");
		sql.append(" from promotion_staff_map psm");
		sql.append(" where promotion_shop_map_id = ?");
		params.add(shopMapId);
		
		if (status != null) {
			sql.append(" and status = ?");
			params.add(status);
		} else {
			sql.append(" and status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		List<PromotionStaffMap> lst = repo.getListBySQL(PromotionStaffMap.class, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionShopMapVO> getListPromotionShopMapVO(PromotionShopMapFilter filter) throws DataAccessException {
		if (filter == null || filter.getProgramId() == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select (select shop_code from shop where shop_id = pm.shop_id) as shopCode,");
		sql.append(" (select staff_code from staff where staff_id = pm.staff_id) as staffCode,");
		sql.append(" pm.quantity_max as quantityMaxStaff, ");
		sql.append(" pm.NUM_MAX as numMax, ");
		sql.append(" pm.AMOUNT_MAX as amountMax ");
		sql.append(" from promotion_staff_map pm");
		sql.append(" where 1=1");
		sql.append(" and promotion_shop_map_id in (");
		sql.append("select promotion_shop_map_id from promotion_shop_map");
		sql.append(" where promotion_program_id = ? and status = ?");
		params.add(filter.getProgramId());
		params.add(ActiveType.RUNNING.getValue());
		if (!StringUtility.isNullOrEmpty(filter.getShopCode()) || !StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and shop_id in (");
			sql.append("select shop_id from shop where status = ?");
			params.add(ActiveType.RUNNING.getValue());
			if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
				sql.append(" and shop_code like ? escape '/'");
				String s = filter.getShopCode().toUpperCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
				sql.append(" and lower(shop_name) like ? escape '/'");
				String s = filter.getShopName().toLowerCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			sql.append(")");
		}
		if (filter.getParentShopId() != null) {
			sql.append(" and shop_id in (");
			sql.append("select shop_id from shop where status = ? start with shop_id = ? connect by prior shop_id = parent_shop_id");
			params.add(ActiveType.RUNNING.getValue());
			params.add(filter.getParentShopId());
			sql.append(")");
		}
		sql.append(")");
		
		if (filter.getQuantityMax() != null) {
			sql.append(" and pm.quantity_max = ?");
			params.add(filter.getQuantityMax());
		}
		
		if (filter.getStatus() != null) {
			sql.append(" and status = ?");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		sql.append(" order by shopCode, staffCode");
		
		String[] fieldNames = new String[] {
				"shopCode", "staffCode", "quantityMaxStaff", "numMax", "amountMax"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL
		};
		
		List<PromotionShopMapVO> lst = repo.getListByQueryAndScalar(PromotionShopMapVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public PromotionStaffMap getPromotionStaffMapByShopMapAndStaff(long shopMapId, long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select *");
		sql.append(" from promotion_staff_map psm");
		sql.append(" where promotion_shop_map_id = ?");
		params.add(shopMapId);
		
		sql.append(" and staff_id = ?");
		params.add(staffId);
		
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		
		return repo.getEntityBySQL(PromotionStaffMap.class, sql.toString(), params);
	}
	
	@Override
	public List<PromotionStaffMap> getListPromotionStaffMapForCreateOrder(Long promotionProgramId, Long staffId, Long shopId, ActiveType status)throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (promotionProgramId == null) {
			throw new IllegalArgumentException("getListPromotionStaffMapForCreateOrder - promotionProgramId is null");
		}
		if (staffId == null) {
			throw new IllegalArgumentException("getListPromotionStaffMapForCreateOrder - staffId is null");
		}
		if (shopId == null) {
			throw new IllegalArgumentException("getListPromotionStaffMapForCreateOrder - shopId is null");
		}
		
		sql.append(" SELECT * FROM promotion_staff_map pstm ");
		sql.append(" WHERE EXISTS ( SELECT 1 FROM promotion_shop_map psm WHERE pstm.promotion_shop_map_id = psm.promotion_shop_map_id ");
		sql.append(" AND psm.promotion_program_id= ? ) ");
		params.add(promotionProgramId);
		if (status != null) {
			sql.append(" AND pstm.status= ? ");
			params.add(status.getValue());
		}
		sql.append(" AND pstm.staff_id = ? AND pstm.shop_id = ? ");
		params.add(staffId);
		params.add(shopId);
		
		return repo.getListBySQL(PromotionStaffMap.class, sql.toString(), params);
	}

	@Override
	public List<PromotionStaffMap> getListPromotionStaffMapAddPromotionStaff(Long promotionShopMapId, Long staffId, Long shopId) throws DataAccessException {
		// TODO Auto-generated method stub
		List<Object> params = new ArrayList<Object>();
		StringBuilder  varname1 = new StringBuilder();
		varname1.append("SELECT * ");
		varname1.append("FROM   promotion_staff_map ");
		varname1.append("WHERE  1 = 1 ");
		varname1.append("       AND status = 1");
		varname1.append("       AND promotion_shop_map_id = ? ");
		params.add(promotionShopMapId);
		varname1.append("       AND shop_id = ? ");
		params.add(shopId);
		varname1.append("       AND staff_id = ? ");
		params.add(staffId);

		return repo.getListBySQL(PromotionStaffMap.class, varname1.toString(), params);
	}
	
}
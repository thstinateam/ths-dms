package ths.dms.core.dao.dialect;

import java.sql.Types;

import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.type.StandardBasicTypes;

public class OracleDialect extends Oracle10gDialect
{
	public OracleDialect() {
		registerHibernateType(Types.NVARCHAR,
				StandardBasicTypes.STRING.getName());
	}
}
package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.StDisplayPdGroupDtl;
import ths.dms.core.entities.StDisplayPlDpDtl;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface DisplayProductGroupDTLDAO {
	StDisplayPdGroupDtl createDisplayProductGroupDTL(StDisplayPdGroupDtl displayProductGroupDTL, LogInfoVO logInfo) throws DataAccessException;

	void deleteDisplayProductGroupDTL(StDisplayPdGroupDtl displayProductGroupDTL, LogInfoVO logInfo) throws DataAccessException;

	void updateDisplayProductGroupDTL(StDisplayPdGroupDtl displayProductGroupDTL, LogInfoVO logInfo) throws DataAccessException;
	
	StDisplayPdGroupDtl getDisplayProductGroupDTLById(long id) throws DataAccessException;

	List<StDisplayPdGroupDtl> getListDisplayProductGroupDTL(KPaging<StDisplayPdGroupDtl> kPaging,
			Long displayProductGroupDTLId, Long displayProductGroupId,ActiveType status) throws DataAccessException;
	
	List<StDisplayPlDpDtl> getListDisplayPDGroupDTL(Long displayProductGroupDTLId, ActiveType status) throws DataAccessException;

	StDisplayPdGroupDtl getDisplayProductGroupDTLByGroupId(Long displayProductGroupId,
			Long productId) throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.FocusProgram;
import ths.dms.core.entities.FocusShopMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.FocusProgramFilter;
import ths.dms.core.entities.enumtype.HTBHFilter;
import ths.dms.core.entities.enumtype.HTBHVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface FocusProgramDAO {

	FocusProgram createFocusProgram(FocusProgram focusProgram, LogInfoVO logInfo) throws DataAccessException;

	void deleteFocusProgram(FocusProgram focusProgram, LogInfoVO logInfo) throws DataAccessException;

	void updateFocusProgram(FocusProgram focusProgram, LogInfoVO logInfo) throws DataAccessException;
	
	FocusProgram getFocusProgramByCode(String code) throws DataAccessException;

	Boolean isUsingByOthers(long focusProgramId) throws DataAccessException;

	List<FocusProgram> getListFocusProgram(KPaging<FocusProgram> kPaging,
			FocusProgramFilter filter, StaffRole staffRole)
			throws DataAccessException;
	
	FocusProgram getFocusProgramById(long id) throws DataAccessException;

	List<FocusShopMap> getListFocusShopMapV2(KPaging<FocusShopMap> kPaging,
			String focusProgramCode, String focusProgramName, String shopCode,
			String shopName, Date fromDate, Date toDate, ActiveType status)
			throws DataAccessException;
	
	List<HTBHVO> getListHTBHVO(
			HTBHFilter filter, KPaging<HTBHVO> kPaging)
			throws DataAccessException;

	/**
	 * Kiem tra shop co shop con tham gia CTTT chua
	 * @param focusProgramId
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 */
	boolean hasChildShopJoinFocusProgram(Long focusProgramId, Long shopId) throws DataAccessException;

	/**
	 * Dem chuong trinh trong tam theo don vi tham gia
	 * @author hunglm16
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @since 15/11/2015
	 */
	int countFocusProgram(FocusProgramFilter filter) throws DataAccessException;
	
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.FocusChannelMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class FocusChannelMapDAOImpl implements FocusChannelMapDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public FocusChannelMap createFocusChannelMap(FocusChannelMap focusChannelMap, LogInfoVO logInfo) throws DataAccessException {
		if (focusChannelMap == null) {
			throw new IllegalArgumentException("focusChannelMap");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		focusChannelMap.setCreateUser(logInfo.getStaffCode());
		focusChannelMap = repo.create(focusChannelMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, focusChannelMap, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(FocusChannelMap.class, focusChannelMap.getId());
	}

	@Override
	public void deleteFocusChannelMap(FocusChannelMap focusChannelMap, LogInfoVO logInfo) throws DataAccessException {
		if (focusChannelMap == null) {
			throw new IllegalArgumentException("focusChannelMap");
		}
		repo.delete(focusChannelMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(focusChannelMap, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public FocusChannelMap getFocusChannelMapById(long id) throws DataAccessException {
		return repo.getEntityById(FocusChannelMap.class, id);
	}
	
	@Override
	public void updateFocusChannelMap(FocusChannelMap focusChannelMap, LogInfoVO logInfo) throws DataAccessException {
		if (focusChannelMap == null) {
			throw new IllegalArgumentException("focusChannelMap");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		FocusChannelMap temp = this.getFocusChannelMapById(focusChannelMap.getId());
		focusChannelMap.setUpdateDate(commonDAO.getSysDate());
		
		focusChannelMap.setUpdateUser(logInfo.getStaffCode());
		repo.update(focusChannelMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, focusChannelMap, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public List<FocusChannelMap> getListFocusChannelMapByFocusProgramId(KPaging<FocusChannelMap> kPaging, Long focusProgramId, String saleTypeCode, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from focus_channel_map m where 1 = 1 ");
		if (status != null) {
			sql.append(" and m.status = ? ");
			params.add(status.getValue());
		}
		if (focusProgramId != null) {
			sql.append(" and m.focus_program_id = ? ");
			params.add(focusProgramId);
		}
		if (!StringUtility.isNullOrEmpty(saleTypeCode)) {
			sql.append(" and m.sale_type_code = ? ");
			params.add(saleTypeCode.toUpperCase());
		}
		sql.append(" order by m.sale_type_code ");
		if (kPaging == null) {
			return repo.getListBySQL(FocusChannelMap.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(FocusChannelMap.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public Boolean isUsingByOthers(long focusChannelMapId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(1) as count from focus_channel_map");
		sql.append(" 	where exists (select 1 from focus_channel_map_product where focus_channel_map_id=?)");
		List<Object> params = new ArrayList<Object>();
		params.add(focusChannelMapId);
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}
	
	@Override
	public Boolean checkIfRecordExist (long focusProgramId, String saleTypeCode, Date fromDate, Date toDate, Long id) 
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return null;
		
		StringBuilder sql = new StringBuilder();
		sql.append(" select count(1) as count from focus_channel_map m where focus_program_id = ?");
		sql.append("	and exists (select 1 from channel_type c where m.sale_type_code = ?) and status = ?");
		
		List<Object> params = new ArrayList<Object>();
		params.add(focusProgramId);
		params.add(saleTypeCode.toUpperCase());
		params.add(ActiveType.RUNNING.getValue());
		
		if (fromDate != null) {
			sql.append("	and to_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("	and from_date < trunc(?) + 1");
			params.add(toDate);
		}
		
		if (id != null) {
			sql.append("	and focus_channel_map_id != ?");
			params.add(id);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}
	
	@Override
	public FocusChannelMap getFocusChannelMapByProgramIdAndSaleTypeCode(long focusProgramId, String saleTypeCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from focus_channel_map where focus_program_id = ? and sale_type_code = ? AND STATUS != -1");
		
		params.add(focusProgramId);
		params.add(saleTypeCode.toUpperCase());
		
		return repo.getEntityBySQL(FocusChannelMap.class, sql.toString(), params);
	}

	@Override
	public Boolean checkIfSaleTypeExistProduct(long focusProgramId,
			String saleTypeCode) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT count(1) as count");
		sql.append(" FROM focus_channel_map fm");
		sql.append(" WHERE fm.focus_program_id = ?");
		sql.append(" AND upper(fm.sale_type_code)     = ?");
		sql.append(" AND EXISTS");
		sql.append(" (SELECT 1");
		sql.append(" FROM FOCUS_CHANNEL_MAP_PRODUCT fmp");
		sql.append(" WHERE fmp.focus_channel_map_id = fm.focus_channel_map_id");
		sql.append(" )");
		
		List<Object> params = new ArrayList<Object>();
		params.add(focusProgramId);
		params.add(saleTypeCode.toUpperCase());
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}

	@Override
	public FocusChannelMap getFocusChannelMapByCode(String code)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from focus_channel_map where sale_type_code = ? AND STATUS != -1");
		params.add(code.toUpperCase());
		return repo.getEntityBySQL(FocusChannelMap.class, sql.toString(), params);
	}
}

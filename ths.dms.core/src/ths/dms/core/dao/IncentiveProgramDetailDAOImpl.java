package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.IncentiveProgramDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.IncentiveProgramDetailFilter;
import ths.dms.core.entities.enumtype.IncentiveProgramDetailVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class IncentiveProgramDetailDAOImpl implements IncentiveProgramDetailDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public IncentiveProgramDetail createIncentiveProgramDetail(IncentiveProgramDetail incentiveProgramDetail, LogInfoVO logInfo) throws DataAccessException {
		if (incentiveProgramDetail == null) {
			throw new IllegalArgumentException("incentiveProgramDetail");
		}
		repo.create(incentiveProgramDetail);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, incentiveProgramDetail, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(IncentiveProgramDetail.class, incentiveProgramDetail.getId());
	}

	@Override
	public void deleteIncentiveProgramDetail(IncentiveProgramDetail incentiveProgramDetail, LogInfoVO logInfo) throws DataAccessException {
		if (incentiveProgramDetail == null) {
			throw new IllegalArgumentException("incentiveProgramDetail");
		}
		repo.delete(incentiveProgramDetail);
		try {
			actionGeneralLogDAO.createActionGeneralLog(incentiveProgramDetail, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public IncentiveProgramDetail getIncentiveProgramDetailById(long id) throws DataAccessException {
		return repo.getEntityById(IncentiveProgramDetail.class, id);
	}
	
	@Override
	public void updateIncentiveProgramDetail(IncentiveProgramDetail incentiveProgramDetail, LogInfoVO logInfo) throws DataAccessException {
		if (incentiveProgramDetail == null) {
			throw new IllegalArgumentException("incentiveProgramDetail");
		}
		IncentiveProgramDetail temp = this.getIncentiveProgramDetailById(incentiveProgramDetail.getId());
		repo.update(incentiveProgramDetail);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, incentiveProgramDetail, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public List<IncentiveProgramDetail> getListIncentiveProgramDetail(
			Long incentiveProgramId, ActiveType status,
			KPaging<IncentiveProgramDetail> kPaging)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from incentive_program_detail where 1 = 1");
		
		if (status != null) {
			sql.append(" and status = ?");
			params.add(status.getValue());
		}else {
			sql.append(" and status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (incentiveProgramId != null) {
			sql.append(" and incentive_program_id = ?");
			params.add(incentiveProgramId);
		}
		
		sql.append(" order by incentive_program_detail_id desc");
		ObjectVO<IncentiveProgramDetail> res = new ObjectVO<IncentiveProgramDetail>();
		if (kPaging == null){
			List<IncentiveProgramDetail> lst = repo.getListBySQL(IncentiveProgramDetail.class, sql.toString(), params);
			return lst;
		}else{
			List<IncentiveProgramDetail> lst = repo.getListBySQLPaginated(IncentiveProgramDetail.class, sql.toString(), params, kPaging);
			return lst;
		}
	}

	@Override
	public List<IncentiveProgramDetailVO> getListIncentiveProgramDetailVO(IncentiveProgramDetailFilter filter,
			KPaging<IncentiveProgramDetailVO> kPaging)
			throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate())){
			return new ArrayList<IncentiveProgramDetailVO>();
		}
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT dt.incentive_program_detail_id id, ");
		sql.append(" cat.product_info_code as catCode, ");
		sql.append(" cat.product_info_name as catName, ");
		sql.append(" p.product_name as productName, ");
		sql.append(" p.product_code as productCode, ");
		sql.append(" dt.from_date fromDate, ");
		sql.append(" dt.to_date toDate, ");
		sql.append(" dt.status status ");
		sql.append(" FROM incentive_program_detail dt ");
		sql.append(" JOIN incentive_program pr ");
		sql.append(" ON pr.incentive_program_id = dt.incentive_program_id ");
		sql.append(" JOIN product p ON p.product_id = dt.product_id ");
		sql.append(" JOIN product_info cat ON cat.product_info_id = p.cat_id ");
		sql.append("WHERE 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		
		if (filter.getProgramId() != null) {
			sql.append(" and pr.incentive_program_id = ? ");
			params.add(filter.getProgramId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCatCode())) {
			sql.append(" and exists(select 1 from product_info cat where cat.PRODUCT_INFO_ID = dt.cat_id and  upper(cat.product_info_code) like ? ESCAPE '/') ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCatCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			sql.append(" and upper(p.product_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProductCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			sql.append(" and upper(p.product_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProductName().toUpperCase()));
		}
		if (filter.getFromDate() != null) {
			sql.append(" and ((dt.to_date is not null and dt.to_date >= trunc(?)) or dt.to_date is null)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and ((dt.from_date is not null and dt.from_date < trunc(?) + 1) or dt.from_date is null)");
			params.add(filter.getToDate());
		}
		if (filter.getStatus() != null) {
			sql.append(" and dt.status=?");
			params.add(filter.getStatus().getValue());
		}else {
			sql.append(" and dt.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" order by catCode, productCode");
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from (");
		countSql.append(sql.toString());
		countSql.append(" )");
		String[] fieldNames = {
				"id", "catName", "catCode", "productName",
				"productCode", "fromDate","toDate", "status"
				};

		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.DATE, StandardBasicTypes.DATE, StandardBasicTypes.INTEGER
		};	
		if (kPaging == null){
			List<IncentiveProgramDetailVO> lst = repo.getListByQueryAndScalar(IncentiveProgramDetailVO.class, fieldNames,
					fieldTypes, sql.toString(), params);
			return lst;
		}else{
			List<IncentiveProgramDetailVO> lst = repo.getListByQueryAndScalarPaginated(IncentiveProgramDetailVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params, kPaging);
			return lst;
		}
	}

	@Override
	public Boolean isExistProduct(Long incentiveProgramId, Long productId, Date fromDate, Date toDate, Long exceptedId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("SELECT count(1) as count ");
		sql.append("FROM incentive_program_detail dt ");
		sql.append("JOIN incentive_program pr ");
		sql.append("ON pr.incentive_program_id = dt.incentive_program_Id ");
		sql.append("WHERE 1                    = 1 ");
		if(incentiveProgramId != null){
			sql.append(" AND pr.incentive_program_id = ? ");
			params.add(incentiveProgramId);
		}
		if(productId != null){
			sql.append(" AND dt.product_id = ? ");
			params.add(productId);
		}
		if(exceptedId != null){
			sql.append(" AND dt.incentive_program_detail_id <> ? ");
			params.add(exceptedId);
		}
		if (fromDate != null) {
			sql.append(" and ((dt.to_date is not null and dt.to_date >= trunc(?)) or dt.to_date is null)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and ((dt.from_date is not null and dt.from_date < trunc(?) + 1) or dt.from_date is null)");
			params.add(toDate);
		}
		sql.append(" and dt.status != ?");
		params.add(ActiveType.DELETED.getValue());
		
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}

	@Override
	public Boolean isExistCategory(Long incentiveProgramId, Long catId,
			Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("SELECT count(1) as count ");
		sql.append("FROM incentive_program_detail dt ");
		sql.append("JOIN incentive_program pr ");
		sql.append("ON pr.incentive_program_id = dt.incentive_program_Id ");
		sql.append("WHERE 1                    = 1 ");
		if(incentiveProgramId != null){
			sql.append(" AND pr.incentive_program_id = ? ");
			params.add(incentiveProgramId);
		}
		if(catId != null){
			sql.append(" AND dt.CAT_ID = ? and dt.product_id is null");
			params.add(catId);
		}
		if (fromDate != null) {
			sql.append(" and ((dt.to_date is not null and dt.to_date >= trunc(?)) or dt.to_date is null)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and ((dt.from_date is not null and dt.from_date < trunc(?) + 1) or dt.from_date is null)");
			params.add(toDate);
		}
		sql.append(" and dt.status != ?");
		params.add(ActiveType.DELETED.getValue());
		
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}
}

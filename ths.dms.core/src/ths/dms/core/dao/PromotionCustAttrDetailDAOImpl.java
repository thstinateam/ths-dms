package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PromotionCustAttrDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class PromotionCustAttrDetailDAOImpl implements PromotionCustAttrDetailDAO {

	@Autowired
	private IRepository repo;

	@Override
	public PromotionCustAttrDetail createPromotionCustAttrDetail(
			PromotionCustAttrDetail promotionCustAttrDetail, LogInfoVO logInfo)
			throws DataAccessException {
		if (promotionCustAttrDetail == null) {
			throw new IllegalArgumentException("poAuto");
		}
		repo.create(promotionCustAttrDetail);
		return repo.getEntityById(PromotionCustAttrDetail.class, promotionCustAttrDetail.getId());
	}

	@Override
	public void deletePromotionCustAttrDetail(PromotionCustAttrDetail promotionCustAttrDetail,
			LogInfoVO logInfo) throws DataAccessException {
		if (promotionCustAttrDetail == null) {
			throw new IllegalArgumentException("promotionCustAttrDetail");
		}
		repo.delete(promotionCustAttrDetail);
	}

	@Override
	public void updatePromotionCustAttrDetail(PromotionCustAttrDetail promotionCustAttrDetail,
			LogInfoVO logInfo) throws DataAccessException {
		if (promotionCustAttrDetail == null) {
			throw new IllegalArgumentException("promotionCustAttrDetail");
		}
		repo.update(promotionCustAttrDetail);
	}

	@Override
	public PromotionCustAttrDetail getPromotionCustAttrDetailById(long id)
			throws DataAccessException {
		return repo.getEntityById(PromotionCustAttrDetail.class, id);
	}

	@Override
	public List<PromotionCustAttrDetail> getListPromotionCustAttrDetail(long id)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" WHERE PROMOTION_CUST_ATTR_ID=? ");
		params.add(id);
		sql.append(" AND STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		return repo.getListBySQL(PromotionCustAttrDetail.class, sql.toString(), params);
	}

	@Override
	public PromotionCustAttrDetail getPromotionCustAttrDetailbyObjectType_and_ObjectId(Long promotionCustAttrId, 
			Long objectType, Long objectId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from PROMOTION_CUST_ATTR_DETAIL where ");
		sql.append(" PROMOTION_CUST_ATTR_ID = ? ");
		params.add(promotionCustAttrId);
		sql.append(" and OBJECT_TYPE =  ? ");
		params.add(objectType);
		sql.append(" and OBJECT_ID = ? ");
		params.add(objectId);
		return repo.getEntityBySQL(PromotionCustAttrDetail.class, sql.toString(), params);
	}

	
}

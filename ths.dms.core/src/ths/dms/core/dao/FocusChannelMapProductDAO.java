package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.FocusChannelMapProduct;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface FocusChannelMapProductDAO {

	void deleteFocusChannelMapProduct(FocusChannelMapProduct focusChannelMapProduct, LogInfoVO logInfo) throws DataAccessException;

	void updateFocusChannelMapProduct(FocusChannelMapProduct focusChannelMapProduct, LogInfoVO logInfo) throws DataAccessException;
	
	FocusChannelMapProduct getFocusChannelMapProductById(long id) throws DataAccessException;

	Boolean isUsingByOthers(long focusProgramId) throws DataAccessException;

	Boolean checkIfProductInFocusProgram(Long focusProgramId, String productCode)
			throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param focusProgramId
	 * @param productId
	 * @return
	 * @throws DataAccessException
	 */
	FocusChannelMapProduct getFocusChannelMapProduct(Long focusProgramId,
			Long productId) throws DataAccessException;


	Boolean createFocusChannelMapProduct(long focusProgramId,
			String saleTypeCode, String code, ProductType type,
			LogInfoVO logInfo) throws DataAccessException;

	FocusChannelMapProduct createFocusChannelMapProduct(
			FocusChannelMapProduct focusChannelMapProduct, LogInfoVO logInfo)
			throws DataAccessException;

	Boolean checkIfRecordExists(long focusProgramId, String saleTypeCode,
			String productCode, Long id) throws DataAccessException;

	List<FocusChannelMapProduct> getListFocusChannelMapProductByFocusProgramId(
			KPaging<FocusChannelMapProduct> kPaging, String productCode,
			String productName, Long focusProgramId, String type)
			throws DataAccessException;

	List<FocusChannelMapProduct> getListFocusChannelMapProductByFocusProgramId(
			KPaging<FocusChannelMapProduct> kPaging, String productCode,
			String productName, Long focusProgramId, Long focusChannelMapId,
			String type) throws DataAccessException;
	
	/**
	 * Kiem tra trung san pham chuong trinh trong tam theo bo: CTTT - HTBH - SP - loaiMHTT
	 * 
	 * @author lacnv1
	 * @since Jun 17, 2014
	 */
	boolean checkExistsProductMap(long focusId, String saleTypeCode, String prodCode, String type) throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.EquipRepairPayForm;
import ths.dms.core.entities.EquipRepairPayFormDtl;
import ths.dms.core.entities.filter.EquipRepairFilter;
import ths.dms.core.entities.vo.EquipRepairFormVO;
import ths.dms.core.entities.vo.EquipRepairPayFormVO;
import ths.dms.core.exceptions.DataAccessException;


/**
 * Quan ly thanh toan cua sua chua
 * @author vuongmq
 * @since 22/06/2015
 * */
public interface EquipmentRepairPayFormDAO {
	/**
	 * @author vuongmq
	 * @date 22/06/2015
	 * @description Danh sach search Quan ly thanh toan
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipRepairPayFormVO> getListEquipRepairPayForm (EquipRepairFilter filter) throws DataAccessException;
	
	/**
	 * @author vuongmq
	 * @date 22/06/2015
	 * @description Danh sach search Quan ly thanh toan Detail
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipRepairPayFormVO> getListEquipRepairPayFormDtlById (EquipRepairFilter filter) throws DataAccessException;

	/***
	 * @author vuongmq
	 * @date 23/06/2015
	 * danh sach phieu sua chua tren popup
	 */
	List<EquipRepairFormVO> getListEquipRepairPopup(EquipRepairFilter filter) throws DataAccessException;

	
	/***
	 * @author vuongmq
	 * @date 23/06/2015
	 * danh sach detai cua phieu thanh toan theo entities
	 */
	List<EquipRepairPayFormDtl> getEquipmentRepairPaymentDtlFilter(	EquipRepairFilter filter) throws DataAccessException;

	/***
	 * @author vuongmq
	 * @date 23/06/2015
	 * phieu thanh toan theo entities
	 */
	EquipRepairPayForm retrieveEquipmentRepairPayment(Long equipmentRepairPayFormId) throws DataAccessException;

	/***
	 * @author vuongmq
	 * @date 23/06/2015
	 * danh sach cua phieu thanh toan theo entities
	 */
	List<EquipRepairPayForm> retrieveListEquipmentRepairPayment(EquipRepairFilter filter) throws DataAccessException;

	/**
	 * @author vuongmq
	 * @date 29/06/2015
	 * @description Danh sach Quan ly thanh toan Xuat file excel theo dieu kien check chon danh sach lstId
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipRepairPayFormVO> exportRepairPayForm(EquipRepairFilter filter) throws DataAccessException;
}

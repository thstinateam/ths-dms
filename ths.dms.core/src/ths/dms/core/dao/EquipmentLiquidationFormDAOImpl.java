package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.EquipLiquidationForm;
import ths.dms.core.entities.EquipLiquidationFormDtl;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.vo.EquipmentLiquidationFormVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class EquipmentLiquidationFormDAOImpl implements EquipmentLiquidationFormDAO {
	@Autowired
	private IRepository repo;

	@Override
	public EquipLiquidationForm createEquipLiquidationForm(EquipLiquidationForm equipLiquidationForm) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipLiquidationForm == null) {
			throw new IllegalArgumentException("equipLiquidationForm is null");
		}
		return repo.create(equipLiquidationForm);
	}

	@Override
	public void updateEquipLiquidationForm(EquipLiquidationForm equipLiquidationForm) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipLiquidationForm == null) {
			throw new IllegalArgumentException("equipLiquidationForm is null");
		}
		repo.update(equipLiquidationForm);
	}

	@Override
	public EquipLiquidationFormDtl createEquipLiquidationFormDetail(EquipLiquidationFormDtl equipLiquidationFormDtl) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipLiquidationFormDtl == null) {
			throw new IllegalArgumentException("equipLiquidationFormDtl is null");
		}
		return repo.create(equipLiquidationFormDtl);
	}

	@Override
	public void deleteEquipLiquidationFormDetail(EquipLiquidationFormDtl equipLiquidationFormDtl) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipLiquidationFormDtl == null) {
			throw new IllegalArgumentException("equipLiquidationFormDtl is null");
		}
		repo.delete(equipLiquidationFormDtl);
		
	}

	@Override
	public List<EquipmentLiquidationFormVO> getListEquipmentLiquidationFormVOByFilter(EquipmentFilter<EquipmentLiquidationFormVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" Select distinct * from (  SELECT elf.equip_liquidation_form_id AS idLiquidation, ");
		sql.append(" elf.equip_liquidation_form_code AS liquidationCode, ");
		sql.append(" elf.note AS note, ");
		sql.append(" elf.equip_liquidation_doc_number AS docNumber, ");
		sql.append(" elf.buyer_address AS buyerAddress, ");
		sql.append(" elf.buyer_phone_number AS buyerPhoneNumber, ");
		sql.append(" elf.buyer_name AS buyerName, ");
//		sql.append(" TO_CHAR(elf.create_date,'dd/mm/yyyy') AS createDate, ");
		//tamvnm: thay doi thanh ngay lap - 14/07/2015
		sql.append(" TO_CHAR(elf.create_form_date,'dd/mm/yyyy') AS createDate, ");
		sql.append(" (SELECT listagg(TO_CHAR(eq.code), ',') within GROUP ( ");
		sql.append(" ORDER BY eq.code) ");
		sql.append(" FROM equipment eq ");
		sql.append(" join equip_liquidation_form_dtl elfd on elfd.equip_id = eq.equip_id WHERE elfd.equip_liquidation_form_id = elf.equip_liquidation_form_id ");
		sql.append(" ) AS equipCode, ");
		sql.append(" (SELECT listagg(TO_CHAR(eq.serial), ',') within GROUP ( ");
		sql.append(" ORDER BY eq.serial) ");
		sql.append(" FROM equipment eq ");
		sql.append(" join equip_liquidation_form_dtl elfd on elfd.equip_id = eq.equip_id WHERE elfd.equip_liquidation_form_id = elf.equip_liquidation_form_id ");
		sql.append(" ) AS equipSeri, ");
//		sql.append(" (SELECT listagg(TO_CHAR(eaf.file_name), '|') within GROUP ( ");
//		sql.append(" ORDER BY eaf.file_name) ");
//		sql.append(" FROM equip_attach_file eaf ");
//		sql.append(" WHERE 1 = 1 ");
//		sql.append(" AND eaf.object_type = ? ");
//		params.add(EquipTradeType.LIQUIDATION.getValue());
//		sql.append(" AND eaf.object_id = elf.equip_liquidation_form_id ");
//		sql.append(" GROUP BY eaf.object_id ");
//		sql.append(" ) AS attachFilesName, ");
//		sql.append(" (SELECT listagg(TO_CHAR(eaf.url), '|') within GROUP ( ");
//		sql.append(" ORDER BY eaf.file_name) ");
//		sql.append(" FROM equip_attach_file eaf ");
//		sql.append(" WHERE 1 = 1 ");
//		sql.append(" AND eaf.object_type = ? ");
//		params.add(EquipTradeType.LIQUIDATION.getValue());
//		sql.append(" AND eaf.object_id = elf.equip_liquidation_form_id ");
//		sql.append(" GROUP BY eaf.object_id ");
//		sql.append(" ) AS urlAttachFiles, ");
		sql.append(" elf.status AS status, ");
		sql.append(" elf.reason AS reasonLiquidation, ");
		sql.append(" elf.create_date AS createDateTmp ");
		sql.append(" FROM equip_liquidation_form elf ");
		sql.append(" join equip_liquidation_form_dtl dt on dt.equip_liquidation_form_id = elf.equip_liquidation_form_id ");
		sql.append(" JOIN equipment e ON e.equip_id = dt.equip_id ");
		sql.append(" WHERE 1 =1 ");		
		if (filter.getShopRoot() != null) {
			/*sql.append(" and dt.stock_id IN (SELECT equip_stock_id from equip_stock  where shop_id in (  SELECT shop_id");
			sql.append("  FROM shop WHERE status               = 1  START WITH shop_id      IN (?) AND status                 = 1");
			sql.append("   CONNECT BY prior shop_id = parent_shop_id) )");*/
			sql.append(" AND ( ");
			sql.append("  (e.stock_type = 1 and e.stock_id IN ");
			sql.append(" (SELECT equip_stock_id ");
			sql.append(" FROM equip_stock ");
			sql.append(" WHERE status = 1 ");
			sql.append(" and shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append(" ) ");
			params.add(filter.getShopRoot());
			/** end kho NPP */
			sql.append(" or  (e.stock_type = 2 and e.stock_id IN ");
			sql.append(" (SELECT cu.customer_id ");
			sql.append(" FROM customer cu ");
			sql.append(" WHERE  ");
			sql.append(" cu.shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append("  ");
			sql.append(" ) ");
			/** end kho KH */
			sql.append(" ) "); // end And
			params.add(filter.getShopRoot());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and upper(elf.equip_liquidation_form_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getDocNumber())) {
			sql.append(" and upper(elf.equip_liquidation_doc_number) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDocNumber().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			sql.append(" and elf.equip_liquidation_form_id in ");
			sql.append(" (SELECT DISTINCT elfd.equip_liquidation_form_id FROM equip_liquidation_form_dtl elfd ");
			sql.append(" JOIN equipment eq ON elfd.equip_id = eq.equip_id ");
			sql.append(" WHERE upper(eq.code) LIKE ? ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipCode().toUpperCase()));
		}
//		if (filter.getFromDate() != null) {
//			sql.append(" and elf.CREATE_DATE >= trunc(?) ");
//			params.add(filter.getFromDate());
//		}
//		if (filter.getToDate() != null) {
//			sql.append(" and elf.CREATE_DATE < trunc(?)+1 ");
//			params.add(filter.getToDate());
//		}	
		//tamvnm: thay doi tim kiem theo ngay lap
		if (filter.getFromDate() != null) {
			sql.append(" and elf.CREATE_FORM_DATE >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and elf.CREATE_FORM_DATE < trunc(?)+1 ");
			params.add(filter.getToDate());
		}		
		if (filter.getStatus() != null) {
			sql.append(" and elf.status = ? ");
			params.add(filter.getStatus());
		}
		sql.append(" ) ORDER BY createDateTmp DESC, liquidationCode DESC ");
		
		String[] fieldNames = { "idLiquidation",// 0
				"liquidationCode",// 1
				"note",
				"docNumber",// 2
				"buyerAddress",// 3
				"buyerPhoneNumber",// 4
				"buyerName",// 5
				"createDate",// 6
				"equipCode",// 7
				"equipSeri",// 8				
//				"attachFilesName",// 9
//				"urlAttachFiles",// 10
				"status",// 11
				"reasonLiquidation",// 12
				"createDateTmp"
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
//				StandardBasicTypes.STRING,// 9
//				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.INTEGER,// 11
				StandardBasicTypes.STRING,// 12
				StandardBasicTypes.DATE,
		};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentLiquidationFormVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentLiquidationFormVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public String getCodeEquipLiquidation() throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select max(equip_liquidation_form_code) as maxCode from equip_liquidation_form ");
		String code = (String)repo.getObjectByQuery(sql.toString(), params);
		if(code == null){
			return "TL00000001";
		}
		code = code.substring(code.length() - 8);
		code = "00000000" + (Long.parseLong(code.toString()) + 1);
		code = code.substring(code.length() - 8);
		code = "TL" + code;
		return code;
	}

	@Override
	public EquipmentLiquidationFormVO getEquipmentLiquidationFormVOById(Long idForm, Long shopId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT elf.equip_liquidation_form_id AS idLiquidation, ");
		sql.append(" elf.equip_liquidation_form_code AS liquidationCode, ");
		sql.append(" elf.note as note, ");
		sql.append(" elf.equip_liquidation_doc_number AS docNumber, ");
		sql.append(" elf.buyer_address AS buyerAddress, ");
		sql.append(" elf.buyer_phone_number AS buyerPhoneNumber, ");
		sql.append(" elf.buyer_name AS buyerName, ");
		sql.append(" TO_CHAR(elf.create_date,'dd/mm/yyyy') AS createDate, ");
		sql.append(" (SELECT listagg(TO_CHAR(eq.code), ',') within GROUP ( ");
		sql.append(" ORDER BY eq.code) ");
		sql.append(" FROM equipment eq ");
		sql.append(" WHERE eq.equip_id in(select elfd.equip_id from equip_liquidation_form_dtl elfd where elfd.equip_liquidation_form_id = elf.equip_liquidation_form_id ) ");
		sql.append(" ) AS equipCode, ");
		sql.append(" (SELECT listagg(TO_CHAR(eq.serial), ',') within GROUP ( ");
		sql.append(" ORDER BY eq.serial) ");
		sql.append(" FROM equipment eq ");
		sql.append(" WHERE eq.equip_id in(select elfd.equip_id from equip_liquidation_form_dtl elfd where elfd.equip_liquidation_form_id = elf.equip_liquidation_form_id ) ");
		sql.append(" ) AS equipSeri, ");
//		sql.append(" (SELECT listagg(TO_CHAR(eaf.file_name), '|') within GROUP ( ");
//		sql.append(" ORDER BY eaf.file_name) ");
//		sql.append(" FROM equip_attach_file eaf ");
//		sql.append(" WHERE 1 = 1 ");
//		sql.append(" AND eaf.object_type = ? ");
//		params.add(EquipTradeType.LIQUIDATION.getValue());
//		sql.append(" AND eaf.object_id = elf.equip_liquidation_form_id ");
//		sql.append(" GROUP BY eaf.object_id ");
//		sql.append(" ) AS attachFilesName, ");
//		sql.append(" (SELECT listagg(eaf.url, '|') within GROUP ( ");
//		sql.append(" ORDER BY eaf.file_name) ");
//		sql.append(" FROM equip_attach_file eaf ");
//		sql.append(" WHERE 1 = 1 ");
//		sql.append(" AND eaf.object_type = ? ");
//		params.add(EquipTradeType.LIQUIDATION.getValue());
//		sql.append(" AND eaf.object_id = elf.equip_liquidation_form_id ");
//		sql.append(" GROUP BY eaf.object_id ");
//		sql.append(" ) AS urlAttachFiles, ");
		sql.append(" elf.status AS status, ");
		sql.append(" elf.reason AS reasonLiquidation, ");
		
		sql.append(" elf.is_check_lot AS checkLot, ");
		sql.append(" elf.liquidation_value AS liquidationValue, ");
		sql.append(" elf.eviction_value AS evictionValue, ");
		sql.append(" elf.create_form_date AS createFormDate ");
		/** From Table*/
		sql.append(" FROM equip_liquidation_form elf ");
		sql.append(" join equip_liquidation_form_dtl dt on dt.equip_liquidation_form_id = elf.equip_liquidation_form_id ");
		sql.append(" JOIN equipment e ON e.equip_id = dt.equip_id ");
		sql.append(" WHERE 1 = 1 ");		
		if (shopId != null) {
			sql.append(" AND ( ");
			sql.append("  (e.stock_type = 1 and e.stock_id IN ");
			sql.append(" (SELECT equip_stock_id ");
			sql.append(" FROM equip_stock ");
			sql.append(" WHERE status = 1 ");
			sql.append(" and shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append(" ) ");
			params.add(shopId);
			/** end kho NPP */
			sql.append(" or  (e.stock_type = 2 and e.stock_id IN ");
			sql.append(" (SELECT cu.customer_id ");
			sql.append(" FROM customer cu ");
			sql.append(" WHERE  ");
			sql.append(" cu.shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append("  ");
			sql.append(" ) ");
			/** end kho KH */
			sql.append(" ) "); // end And
			params.add(shopId);
		}
		if (idForm != null) {
			sql.append(" and elf.equip_liquidation_form_id = ? ");
			params.add(idForm);
		}

		sql.append(" ORDER BY elf.create_date DESC ");

		String[] fieldNames = { "idLiquidation",// 0
				"liquidationCode",// 1
				"note",
				"docNumber",// 2
				"buyerAddress",// 3
				"buyerPhoneNumber",// 4
				"buyerName",// 5
				"createDate",// 6
				"equipCode",// 7
				"equipSeri",// 8				
//				"attachFilesName",// 9
//				"urlAttachFiles",// 10
				"status",// 11
				"reasonLiquidation",// 12
				"checkLot", //13
				"liquidationValue", //14
				"evictionValue", //15
				"createFormDate",
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
//				StandardBasicTypes.STRING,// 9
//				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.INTEGER,// 11
				StandardBasicTypes.STRING,// 12
				StandardBasicTypes.INTEGER,// 13
				StandardBasicTypes.BIG_DECIMAL,// 14
				StandardBasicTypes.BIG_DECIMAL,// 15
				StandardBasicTypes.DATE,
		};

		return repo.getListByQueryAndScalarFirst(EquipmentLiquidationFormVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public EquipLiquidationForm getEquipmentLiquidationFormById(Long idForm) throws DataAccessException {
		// TODO Auto-generated method stub
		return repo.getEntityById(EquipLiquidationForm.class, idForm);
	}
	
	@Override
	public EquipLiquidationForm getEquipmentLiquidationFormById(Long idForm, Long shopId) throws DataAccessException {
		if (idForm == null) {
			throw new IllegalArgumentException("idForm is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select DISTINCT elf.* ");
		sql.append(" from equip_liquidation_form elf ");
		sql.append(" join equip_liquidation_form_dtl dt on dt.equip_liquidation_form_id = elf.equip_liquidation_form_id ");
		sql.append(" join equipment e ON e.equip_id = dt.equip_id ");
		sql.append(" where elf.equip_liquidation_form_id = ? ");
		params.add(idForm);
		if (shopId != null) {
			sql.append(" AND ( ");
			sql.append("  (e.stock_type = 1 and e.stock_id IN ");
			sql.append(" (SELECT equip_stock_id ");
			sql.append(" FROM equip_stock ");
			sql.append(" WHERE status = 1 ");
			sql.append(" and shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append(" ) ");
			params.add(shopId);
			/** end kho NPP */
			sql.append(" or  (e.stock_type = 2 and e.stock_id IN ");
			sql.append(" (SELECT cu.customer_id ");
			sql.append(" FROM customer cu ");
			sql.append(" WHERE  ");
			sql.append(" cu.shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append("  ");
			sql.append(" ) ");
			/** end kho KH */
			sql.append(" ) "); // end And
			params.add(shopId);
		}
		return repo.getEntityBySQL(EquipLiquidationForm.class, sql.toString(), params);
	}

	@Override
	public List<EquipLiquidationFormDtl> getListEquipLiquidationFormDtlByEquipLiquidationFormId(Long idForm) throws DataAccessException {
		// TODO Auto-generated method stub
		if (idForm == null) {
			throw new IllegalArgumentException("idForm is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_liquidation_form_dtl where equip_liquidation_form_id = ? ");
		params.add(idForm);
		return repo.getListBySQL(EquipLiquidationFormDtl.class, sql.toString(), params);
	}

	@Override
	public EquipLiquidationFormDtl getEquipLiquidationFormDtlByFilter(EquipmentFilter<EquipLiquidationFormDtl> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from equip_liquidation_form_dtl elfd ");
		sql.append(" join equipment eq on eq.equip_id = elfd.equip_id ");
		sql.append(" where 1=1 ");
		if(filter.getIdLiquidation() != null){
			sql.append(" and elfd.equip_liquidation_form_dtl_id = ? ");
			params.add(filter.getIdRecordDelivery());
		}
		if(filter.getId() != null){
			sql.append(" and eq.equip_id = ? ");
			params.add(filter.getId());
		}
		if(!StringUtility.isNullOrEmpty(filter.getCode())){
			sql.append(" and eq.code = ? ");
			params.add(filter.getCode().toUpperCase());
		}
		if(!StringUtility.isNullOrEmpty(filter.getSeriNumber())){
			sql.append(" and eq.serial = ? ");
			params.add(filter.getSeriNumber().toUpperCase());
		}
		return repo.getEntityBySQL(EquipLiquidationFormDtl.class, sql.toString(), params);
	}

	@Override
	public void updateEquipLiquidationFormDtl(EquipLiquidationFormDtl equipLiquidationFormDtl) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipLiquidationFormDtl == null) {
			throw new IllegalArgumentException("equipLiquidationFormDtl is null");
		}
		repo.update(equipLiquidationFormDtl);		
	}

	/**
	 * Lay danh sach bien ban thanh ly cho export
	 * 
	 * @author tamvnm
	 * @since 12/01/2015
	 * @param equipmentFilter
	 * @throws BusinessException
	 * 
	 * vuongmq; 14/07/2015;
	 * lay danh sach theo filter danh sach Id khi export
	 */
	@Override
	public List<EquipmentLiquidationFormVO> getListEquipmentLiquidationFormVOForExport(EquipmentFilter<EquipmentLiquidationFormVO> filter)
			throws DataAccessException {
		StringBuffer  sql = new StringBuffer();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT elf.equip_liquidation_form_code AS liquidationCode, ");
		sql.append("       e.code                          AS equipCode, ");
		sql.append("       elf.reason                      AS reasonLiquidation, ");
		sql.append("       elf.note						   as note, ");
		sql.append("       elf.status                      AS status, ");
		sql.append("       elf.buyer_name                  AS buyerName, ");
		sql.append("       elf.buyer_address               AS buyerAddress, ");
		sql.append("       elf.buyer_phone_number          AS buyerPhoneNumber, ");
		sql.append("       elf.EQUIP_LIQUIDATION_DOC_NUMBER          AS equip_liquidation_doc, ");
		sql.append(" elfd.liquidation_value AS liquidationValue, ");
		sql.append(" elfd.eviction_value AS evictionValue, ");
		sql.append(" elf.liquidation_value AS liquidationValueSum, ");
		sql.append(" elf.eviction_value AS evictionValueSum, ");
		sql.append(" to_char(elf.create_form_date,'dd/mm/yyyy') AS createDate ");
		sql.append("FROM   equip_liquidation_form elf, ");
		sql.append("       equip_liquidation_form_dtl elfd, ");
		sql.append("       equipment e ");
		sql.append("WHERE  1 = 1 ");
		if (filter.getLstId() != null) {
			sql.append(" AND elf.equip_liquidation_form_id in (-1 ");
			for (int i = 0, sz = filter.getLstId().size() ; i < sz; i++) {
				sql.append(", ? ");
				params.add(filter.getLstId().get(i));
			}
			sql.append(" ) ");
		}
		if (filter.getId() != null) {
			sql.append(" AND elf.equip_liquidation_form_id = ? ");
			params.add(filter.getId());
		}
		sql.append("       AND elfd.equip_liquidation_form_id = elf.equip_liquidation_form_id ");
		sql.append("       AND e.equip_id = elfd.equip_id ");
		sql.append("       AND e.status = 1");
		sql.append(" ORDER BY liquidationCode ");

		String[] fieldNames = { 
				"liquidationCode",		// 1
				"equipCode",			// 2
				"reasonLiquidation",	// 4
				"note",	
				"status",				// 5
				"buyerName",			// 6
				"buyerAddress",			// 7
				"buyerPhoneNumber", 	// 8
				"equip_liquidation_doc",// 9
				"liquidationValue",     //10
				"evictionValue",        //11
				"liquidationValueSum", //12
				"evictionValueSum",   //13
				"createDate"  //14
		};

		Type[] fieldTypes = { 
				StandardBasicTypes.STRING,	// 1
				StandardBasicTypes.STRING,	// 2
				StandardBasicTypes.STRING,	// 4
				StandardBasicTypes.STRING,	
				StandardBasicTypes.INTEGER,	// 5
				StandardBasicTypes.STRING,	// 6
				StandardBasicTypes.STRING,	// 7
				StandardBasicTypes.STRING,	// 8
				StandardBasicTypes.STRING,  // 9
				StandardBasicTypes.BIG_DECIMAL,  // 10
				StandardBasicTypes.BIG_DECIMAL,  // 11
				StandardBasicTypes.BIG_DECIMAL,  // 12
				StandardBasicTypes.BIG_DECIMAL,  // 13
				StandardBasicTypes.STRING,  // 14
		};
		
		return repo.getListByQueryAndScalar(EquipmentLiquidationFormVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	
}

package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.SalePromoMap;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.SaleOrderDetailFilter;
import ths.dms.core.entities.vo.PrintDeliveryCustomerGroupVO1;
import ths.dms.core.entities.vo.PrintDeliveryGroupExportVO2;
import ths.dms.core.entities.vo.PrintDeliveryGroupVO1;
import ths.dms.core.entities.vo.SaleOrderDetailGroupVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO2;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderLotVO;
import ths.dms.core.entities.vo.SaleOrderLotVOEx;
import ths.dms.core.entities.vo.TaxInvoiceDetailVO;
import ths.core.entities.vo.rpt.RptCustomerProductSaleOrder1VO;
import ths.core.entities.vo.rpt.RptExSaleOrderVO;
import ths.core.entities.vo.rpt.RptOrderDetailProduct4VO;
import ths.core.entities.vo.rpt.RptPayDetailDisplayProgr1VO;
import ths.core.entities.vo.rpt.RptProductDistrResult2VO;
import ths.core.entities.vo.rpt.RptPromotionDetailStaff2VO;
import ths.core.entities.vo.rpt.RptReturnSaleOrderAllowDeliveryVO;
import ths.core.entities.vo.rpt.RptReturnSaleOrderVO;
import ths.core.entities.vo.rpt.RptSaleOrderDetail2VO;
import ths.core.entities.vo.rpt.RptSaleOrderDetailVO;
import ths.core.entities.vo.rpt.RptSaleOrderLotVO;
import ths.dms.core.exceptions.DataAccessException;

public interface SaleOrderDetailDAO {

	SaleOrderDetail createSaleOrderDetail(SaleOrderDetail salesOrderDetail) throws DataAccessException;

	void deleteSaleOrderDetail(SaleOrderDetail salesOrderDetail) throws DataAccessException;

	void updateSaleOrderDetail(SaleOrderDetail salesOrderDetail) throws DataAccessException;
	
	SaleOrderDetail getSaleOrderDetailById(long id) throws DataAccessException;

	void createSaleOrderDetail(List<SaleOrderDetail> lstSaleOrderDetail) throws DataAccessException;

	/**
	 * insert ds SalePromoMap
	 * @author trietptm
	 * @param lstSalePromoMaps
	 * @throws DataAccessException
	 * @since Aug 25, 2015
	 */
	void createSalePromoMap(List<SalePromoMap> lstSalePromoMaps) throws DataAccessException;
	/**
	 * lay danh sach order detail dua vao order id
	 * @author thanhnguyen
	 * @param saleOrderId
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleOrderDetail> getListSaleOrderDetailBySaleOrderId(long saleOrderId, KPaging<SaleOrderDetail> kPaging) throws DataAccessException;
	
	
	/**
	 * lay danh sach order detail dua vao order id va theo loai ( sp ban hoac sp khuyen mai )
	 * @author nhanlt6
	 * @param saleOrderId
	 * @param isAutoPromotion
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleOrderDetail> getListSaleOrderDetailBySaleOrderIdAndType(long saleOrderId, Boolean isAutoPromotion) throws DataAccessException;

	/**
	 * Lay danh sach product de in hoa don gia tri gia tang
	 * @param salerOrderId
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<TaxInvoiceDetailVO> getListTaxInvoiceDetailVO(Long saleOrderId) throws DataAccessException;

	/**
	 * 3.1.5.2	Bảng kê chi tiết đơn hàng theo từng mặt hàng
	 * @author hieunq1
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @param staffId
	 * @return
	 * @throws DataAccessException
	 */
	List<RptOrderDetailProduct4VO> getListRptOrderDetailProduct4VO(Long shopId,
			String staffCode, Date fromDate, Date toDate)
			throws DataAccessException;

	/**
	 * getListCustomerProductSaleOrderRptVO
	 * @author hieunq1
	 * @param shopId
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException 
	 */
	List<RptCustomerProductSaleOrder1VO> getListRptCustomerProductSaleOrderVO(
			Long shopId, Long staffId, Date fromDate, Date toDate) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptPayDetailDisplayProgr1VO> getListRptPayDetailDisplayProgrVO(
			Long shopId, String ppCode, Date fromDate, Date toDate) throws DataAccessException;
	
	/**
	 * @author hungnm
	 * @param kPaging
	 * @param shopId
	 * @param deliveryStaffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptSaleOrderLotVO> getListSaleOrderLotVO(
			KPaging<RptSaleOrderLotVO> kPaging, Long shopId,
			Long deliveryStaffId, Date fromDate, Date toDate,
			Integer isFreeItem, Boolean getPromotion, Long saleOrderId)
			throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptPromotionDetailStaff2VO> getListRptPromotionDetailStaffV2O(
			Long shopId, String staffCode, Date fromDate, Date toDate, Boolean programe_staff) throws DataAccessException;
	
	/**
	 * lay danh sach bao cao tra hang
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param staffId
	 * @param deliveryId
	 * @return
	 * @throws DataAccessException
	 */
	List<RptReturnSaleOrderVO> getListRptReturnSaleOrder(Long shopId,
			Date fromDate, Date toDate, Long customerId)
			throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptProductDistrResult2VO> getListRptProductDistrResult2VO(Long shopId,
			String staffCode, Date fromDate, Date toDate)
			throws DataAccessException;
	/**
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @param staffCode
	 * @param custCode
	 * @param vat
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
//	List<RptInvDetailStat2VO> getListRptInvDetailStat2VO(Long shopId,
//			String staffCode, String shortCode, Integer vat, Date fromDate,
//			Date toDate, InvoiceStatus status) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param kPaging
	 * @param saleOrderId
	 * @param isFreeItem
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleOrderDetailGroupVO> getListSaleOrderDetailGroupVO(
			KPaging<SaleOrderDetailGroupVO> kPaging, Long saleOrderId,
			Integer isFreeItem) throws DataAccessException;
	
	/**
	 * lay danh sach bao cao doi tra hang
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param deliveryId
	 * @return
	 * @throws DataAccessException
	 */
	List<RptReturnSaleOrderVO> getListChangeReturnSaleOrder(Long shopId,
			Date fromDate, Date toDate, Long deliveryId)
			throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param listSaleOrderId
	 * @return
	 * @throws DataAccessException 
	 */
	List<PrintDeliveryGroupExportVO2> getListPrintDeliveryGroupExportVO2(
			List<Long> listSaleOrderId, Date fromDate, Date toDate) throws DataAccessException;
	
	/**
	 * lay danh sach bao cao tong hop chi tiet trung bay
	 * @author thanhnguyen
	 * @param shopId
	 * @param displayProgramCode
	 * @return
	 * @throws DataAccessException
	 */
	List<RptPayDetailDisplayProgr1VO> getListRptGenaralDetailDisplayProgram(
			Long shopId, String displayProgramCode) throws DataAccessException;
	
	/**
	 * 3.1.3.8 phieu tra hang theo nvgh
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param deliveryId
	 * @return
	 * @throws DataAccessException
	 */
	List<RptReturnSaleOrderAllowDeliveryVO> getListRptReturnSaleOrderAllowDelivery(
			Long shopId, Date fromDate, Date toDate, Long deliveryId)
			throws DataAccessException;

	/**
	 * Load sod cua don WEB
	 * @author hungnm
	 * @param kPaging
	 * @param saleOrderId
	 * @param isFreeItem
	 * @param programType
	 * @param isGetAutoPromotion
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleOrderDetailVOEx> getListSaleOrderDetailVOEx(
			KPaging<SaleOrderDetailVOEx> kPaging, Long saleOrderId,
			Integer isFreeItem, ProgramType programType,
			Boolean isGetAutoPromotion,Long customerTypeId,Long shopId,Date lockDay) throws DataAccessException;
	
	
	/**
	 * Load sod cua don hang TABLET
	 * @author nhanlt6
	 * @param kPaging
	 * @param saleOrderId
	 * @param isFreeItem
	 * @param programType
	 * @param isGetAutoPromotion
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleOrderDetailVOEx> getListSaleOrderDetailVOExForTablet(
			KPaging<SaleOrderDetailVOEx> kPaging, Long saleOrderId,
			Integer isFreeItem, ProgramType programType,
			Boolean isGetAutoPromotion,Long customerTypeId,Long shopId,Date lockDay) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param listSaleOrderId
	 * @return
	 * @throws DataAccessException
	 */
	List<PrintDeliveryGroupVO1> getListPrintDeliveryGroupVO(
			List<Long> listSaleOrderId) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param listSaleOrderId
	 * @return
	 * @throws DataAccessException
	 */
	List<PrintDeliveryCustomerGroupVO1> getListPrintDeliveryCustomerGroupVO(
			List<Long> listSaleOrderId) throws DataAccessException;

	List<SaleOrderDetail> getListSaleOrderDetailBySOIDOrderByVat1(long saleOrderId, KPaging<SaleOrderDetail> kPaging) throws DataAccessException;

	List<RptExSaleOrderVO> getListExSaleOrderVO(Long shopId, Date fromDate,
			Date toDate, Long deliveryStaffId) throws DataAccessException;
	
	List<RptExSaleOrderVO> getListSaleOrderByDeliveryStaffGroupByProductCode(
			List<Long> listSaleOrderId,
			Long shopId) throws DataAccessException;

	List<RptSaleOrderDetailVO> getPDTHReport(Long shopId, Date fromDate,
			Date toDate, Long staffId, String deliveryCode)
			throws DataAccessException;
	
	/**
	 * 
	 * @author vuonghn
	 * @param 
	 * @return RptSaleOrderDetail2VO
	 * @throws DataAccessException
	 */
	List<RptSaleOrderDetail2VO> getPDHCGReport(Long shopID, Date fromDate, Date toDate, String deliveryCode) throws DataAccessException;

	List<SaleOrderDetail> getListSaleOrderDetailBySOIDOrderByVat2(long saleOrderId, KPaging<SaleOrderDetail> kPaging) throws DataAccessException;

	List<SaleOrderDetail> getListSaleOrderDetailBySOIDOrderByVat3(long saleOrderId, KPaging<SaleOrderDetail> kPaging) throws DataAccessException;

	List<SaleOrderDetail> getListSaleOrderDetailByInvoiceId(Long invoiceId, KPaging<SaleOrderDetail> kPaging) throws DataAccessException;

	List<SaleOrderDetail> getListSaleOrderDetailBySaleOrderId(Long saleOrderId,
			Long invoiceId, KPaging<SaleOrderDetail> kPaging)
			throws DataAccessException;

	String getListOrderNumberForReport(Long shopId, Date fromDate, Date toDate,
			Long deliveryStaffId) throws DataAccessException;

	Long getSku(Long saleOrderId, Long invoiceId) throws DataAccessException;

	List<RptExSaleOrderVO> getListExSaleOrderMoneyPromoVO(Long shopId,
			Date fromDate, Date toDate, Long deliveryStaffId,
			String lstOrderNumber) throws DataAccessException;

	List<RptSaleOrderLotVO> getListSaleOrderLotVOEx(
			KPaging<RptSaleOrderLotVO> kPaging, Long shopId,
			Long deliveryStaffId, Date fromDate, Date toDate,
			Integer isFreeItem, Boolean getPromotion, Long saleOrderId)
			throws DataAccessException;
	
	List<SaleOrderDetail> getListAllSaleOrderDetail(long saleOrderId, KPaging<SaleOrderDetail> kPaging, Date orderDate) throws DataAccessException;
	
	/**
	 * Lay danh sach tat ca cac san pham ban cua don hang
	 * @author tungtt21
	 * @param saleOrderId
	 * @param kPaging
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleOrderDetail> getListSaleProductBySaleOrderId(long saleOrderId) throws DataAccessException;
	
	
	/**
	 * Lay danh sach cac sale_order_detail cua hoa don da bi huy dua vao invoice_detail
	 * @author tungtt21
	 * @param invoiceId
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleOrderDetail> getListDetailByInvoiceAndInvoceDetail(Long invoiceId) throws DataAccessException;
	
	/**
	 * Lay danh sach sp cua don hang
	 * 
	 * @since Aug 25, 2014
	 */
	List<SaleOrderDetailVOEx> getListSaleOrderDetailByOrderId(long saleOrderId, Integer isFreeItem, ProgramType programType,
			Boolean isAutoPromotion, Boolean isGetCTTL) throws DataAccessException;

	void updateSaleOrderDetail(List<SaleOrderDetail> lstSalesOrderDetail)
			throws DataAccessException;

	void deleteSaleOrderDetail(List<SaleOrderDetail> lstSalesOrderDetail)
			throws DataAccessException;
	
	List<SaleOrderPromoDetail> getListSaleOrderPromoDetail(Long saleOrderDetailId) throws DataAccessException;
	
	/**
	 * get sale order detail by filter
	 * @author tuannd20
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @date 10/10/2014
	 */
	List<SaleOrderDetail> getSaleOrderDetails(SaleOrderDetailFilter<SaleOrderDetail> filter) throws DataAccessException;

	/**
	 * Lay danh sach sale_order_lot cua sale_order_detail
	 * 
	 * @author lacnv1
	 * @since Dec 31, 2014
	 */
	List<SaleOrderLotVO> getSaleOrderLotVOOfOrder(long saleOrderId) throws DataAccessException;
	
	/**
	 * Lay danh sach sale_order_promo_detail cua don hang theo program_code
	 * 
	 * @author lacnv1
	 * @since Jan 01, 2015
	 */
	List<SaleOrderPromoDetail> getListSaleOrderPromoDetailOfOrder(long saleOrderId, String programCode) throws DataAccessException;
	
	/**
	 * Lay danh sach sale_order_detail cua khuyen mai tich luy
	 * 
	 * @author lacnv1
	 * @since Jan 08, 2015
	 */
	List<SaleOrderDetail> getListSaleOrderDetailZV23(long saleOrderId) throws DataAccessException;
	
	/**
	 * Lay danh sach chi tiet don hang cho man hinh tra hang
	 * 
	 * @author lacnv1
	 * @since Mar 26, 2015
	 */
	List<SaleOrderLotVOEx> getListOrderDetailVOByOrderId(long orderId) throws DataAccessException;
	
	/**
	 * Lay chi tiet khuyen mai tien, phan tram cua khuyen mai don hang, khuyen mai tich luy
	 * 
	 * @author lacnv1
	 * @since Mar 30, 2015
	 */
	List<SaleOrderLotVOEx> getListOrderPromoDetailByOrderId(long orderId) throws DataAccessException;

	/**
	 * @author trietptm
	 * @param filter
	 * @return  danh sach SaleOrderDetail theo filter
	 * @throws DataAccessException
	 * @since Aug 19, 2015
	 */
	List<SaleOrderDetail> getListSaleOrderDetailBySaleOrderId(SaleOrderDetailFilter<SaleOrderDetail> filter) throws DataAccessException;

	/**
	 * lay ds discountAmount cua CTKM
	 * @author trietptm
	 * @param filter
	 * @return ds discountAmount cua CTKM
	 * @throws DataAccessException
	 * @since Sep 09, 2015
	 */
	List<SaleOrderDetailVO2> getListDiscountAmountByProgramCode(@SuppressWarnings("rawtypes") SaleOrderDetailFilter filter) throws DataAccessException;
}
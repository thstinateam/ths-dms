package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Report;
import ths.dms.core.entities.ReportShopMap;
import ths.dms.core.entities.ReportUrl;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.ReportFilter;
import ths.dms.core.exceptions.DataAccessException;

public interface ReportManagerDAO {
	
	Report createReport(Report report) throws DataAccessException;
	Report getReportById(Long id) throws DataAccessException;
	void updateReport(Report report) throws DataAccessException;
	void deleteReport(Report report) throws DataAccessException;
	Report getReportByCode(String code) throws DataAccessException;
	
	ReportUrl createReportUrl(ReportUrl reportUrl) throws DataAccessException;
	ReportUrl getReportUrlById(Long id) throws DataAccessException;
	void updateReportUrl(ReportUrl reportUrl) throws DataAccessException;
	void deleteReportUrl(ReportUrl reportUrl) throws DataAccessException;
	List<ReportUrl> getListReportUrlByReportId(Long reportId) throws DataAccessException;
	List<ReportUrl> getListReportUrlByNPP(Long reportId) throws DataAccessException;
	
	ReportShopMap createReportShopMap(ReportShopMap reportShopMap) throws DataAccessException;
	ReportShopMap getReportShopMapById(Long id) throws DataAccessException;
	ReportShopMap getReportShopMapByShopIdReportId(Long shopId, Long reportId) throws DataAccessException;
	void updateReportShopMap(ReportShopMap reportShopMap) throws DataAccessException;
	void deleteReportShopMap(ReportShopMap reportShopMap) throws DataAccessException;
	List<ReportShopMap> getListReportShopMapByReportId(Long reportId) throws DataAccessException;
	
	//Report getListReport(Report reportItem, LogInfoVO logInfo) throws DataAccessException;
	List<Report> getListReport(KPaging<Report> kPaging, ReportFilter filter) throws DataAccessException;
	
	List<ReportUrl> getListReportUrl(KPaging<ReportUrl> kPaging, ReportFilter filter) throws DataAccessException;
	/**
	 * 
	 * @param shopId
	 * @param maBC
	 * @return
	 * @throws DataAccessException
	 */
	ReportUrl getReportFileName(Long shopId, String maBC) throws DataAccessException;
	
	/**
	 * Lay file bao cao mac dinh
	 * 
	 * @author lacnv1
	 * @since Oct 14, 2014
	 */
	ReportUrl getDefaultReportUrl(ReportFilter filter) throws DataAccessException;
}

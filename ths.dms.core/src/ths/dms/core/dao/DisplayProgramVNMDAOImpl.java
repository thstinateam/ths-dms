package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.DisplayProgramVNM;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class DisplayProgramVNMDAOImpl implements DisplayProgramVNMDAO{
	@Autowired
	private IRepository repo;
	
	@Override
	public DisplayProgramVNM getDisplayProgramVNMByID(Long id)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if(id == null){
			throw new IllegalArgumentException("getDisplayProgramVNMByID_Id_Null");
		}
		return repo.getEntityById(DisplayProgramVNM.class, id);
	}

	@Override
	public DisplayProgramVNM getDisplayProgramVNMByCode(String code)
			throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from Display_Program_VNM where display_program_code = ?");
		params.add(code);
		return repo.getEntityBySQL(DisplayProgramVNM.class, sql.toString(), params);
	}

	@Override
	public List<DisplayProgramVNM> getListDisplayProgramVNM(KPaging<DisplayProgramVNM> kPaging, Long shopId, String code, String name,
			ActiveType status, Date fDate, Date tDate)
			throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		sql.append("with lstshop as (select distinct shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
		params.add(shopId);
		sql.append("select dp.* from Display_Program_VNM dp where 1=1");
		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and upper(dp.display_program_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(code.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(name)) {
			sql.append(" and lower(dp.display_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(name.toLowerCase()));
		}
		if (status != null) {
			sql.append(" and dp.status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (fDate != null) {
			sql.append(" and ((dp.to_date is not null and dp.to_date >= trunc(?)) or dp.to_date is null)");
			params.add(fDate);
		}
		if (tDate != null) {
			sql.append(" and ((dp.from_date is not null and dp.from_date < trunc(?) + 1) or dp.from_date is null)");
			params.add(tDate);
		}
		sql.append(" and exists(select 1 from display_pro_shop_map_vnm dsm where dsm.display_program_id = dp.display_program_vnm_id  and dsm.status = 1 and exists (select 1 from lstshop where dsm.shop_id = shop_id))");
		sql.append(" order by trunc(dp.from_date) desc, dp.display_program_code ");
		if(kPaging!=null){
			return repo.getListBySQLPaginated(DisplayProgramVNM.class, sql.toString(), params, kPaging);
		}else{
			return repo.getListBySQL(DisplayProgramVNM.class, sql.toString(), params);
		}
	}

}

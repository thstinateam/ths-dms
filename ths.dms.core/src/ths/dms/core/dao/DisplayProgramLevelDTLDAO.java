package ths.dms.core.dao;


import java.util.List;

import ths.dms.core.entities.StDisplayPlAmtDtl;
import ths.dms.core.entities.StDisplayPlDpDtl;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.DisplayProgrameExVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface DisplayProgramLevelDTLDAO {

	StDisplayPlAmtDtl createDisplayProgramLevelDTL(StDisplayPlAmtDtl displayProgramLevelDTL, LogInfoVO logInfo) throws DataAccessException;

	void deleteDisplayProgramLevelDTL(StDisplayPlAmtDtl displayProgramLevelDTL, LogInfoVO logInfo) throws DataAccessException;

	void updateDisplayProgramLevelDTL(StDisplayPlAmtDtl displayProgramLevelDTL, LogInfoVO logInfo) throws DataAccessException;
	
	StDisplayPlAmtDtl getDisplayProgramLevelDTLById(long id) throws DataAccessException;

	List<StDisplayPlAmtDtl> getListDisplayProgramLevelDTL(KPaging<StDisplayPlAmtDtl> kPaging,
			Long displayProgramLevelDTLId, ActiveType status)  throws DataAccessException;

	List<DisplayProgrameExVO> getListDisplayProgramLevelDetail(Long displayProgramId, String levelCode,DisplayProductGroupType type) throws DataAccessException;

	List<StDisplayPlAmtDtl> getListDisplayProgramLevelDetailByLevelId(
			Long levelId) throws DataAccessException;
	/**
	 * @author loctt
	 * @since 25sep2013
	 * @param levelId
	 * @return
	 * @throws DataAccessException
	 */
	List<StDisplayPlDpDtl> getListDisplayProductLevelDetailByLevelId(Long levelId)
	throws DataAccessException;
}

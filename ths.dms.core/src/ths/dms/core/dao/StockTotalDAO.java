package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.StockLock;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTotalVOFilter;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.entities.vo.ProductAmountVO;
import ths.dms.core.entities.vo.ProductLotVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.core.entities.vo.rpt.RptCustomerStock2VO;
import ths.dms.core.exceptions.DataAccessException;

public interface StockTotalDAO {

	StockTotal createStockTotal(StockTotal stockTotal) throws DataAccessException;

	void deleteStockTotal(StockTotal stockTotal) throws DataAccessException;

	void updateStockTotal(StockTotal stockTotal) throws DataAccessException;
	
	StockTotal getStockTotalByProductAndOwner(long productId,
			StockObjectType ownerType, long ownerId) throws DataAccessException;
	
	StockTotal getStockTotalById(Long id) throws DataAccessException;

	StockTotal getStockTotalByProductAndOwner(long productId,
			StockObjectType ownerType, long ownerId, Long warehouseId) throws DataAccessException;
	/**
	 * Lay stockTotal theo filter
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<StockTotal> getListStockTotalByProductAndOwner(StockTotalFilter filter) throws DataAccessException;

	List<Boolean> checkIfProductHasEnough(
			List<ProductAmountVO> lstProductAmountVO)
			throws DataAccessException;

	List<StockTotal> getListStockTotal(KPaging<StockTotal> kPaging,
			String productCode, String productName,
			String promotionProgramCode, Long ownerId,
			StockObjectType ownerType, Long catId, Long subCatId,
			Integer fromQuantity, Integer toQuantity, Long cycleCountId)
			throws DataAccessException;

	boolean checkQuantityInStockTotal(long shopId, long productId, int quantity) throws DataAccessException;
	
	/***
	 * @author vuongmq
	 * @date 11/08/2015
	 * @description Lay getStockTotalByEntities theo filter
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	StockTotal getStockTotalByEntities(StockTotalFilter filter) throws DataAccessException;
	
	/**
	 * @author thanhnguyen
	 * @param ownerId
	 * @return
	 */
	StockTotal getStockTotalFromNPPandProductId(Long ownerId, Long productId) throws DataAccessException;

	
	/**
	 * @param productId
	 * @param ownerType
	 * @param ownerId
	 * @param warehouseId
	 * @return
	 * @throws DataAccessException
	 */
	StockTotal getStockTotalByProductAndOwnerForUpdate(Long productId, StockObjectType ownerType, Long ownerId, Long warehouseId) throws DataAccessException;
	
	List<StockTotal> getListStockTotalByProductAndOwnerForUpdate(long productId,
			StockObjectType ownerType, long ownerId) throws DataAccessException;

	/**
	 * updateListStockTotal
	 * @author hieunq1
	 * @param listStockTotal
	 * @throws DataAccessException
	 */
	void updateListStockTotal(List<StockTotal> listStockTotal)
			throws DataAccessException;

	/**
	 * Cap nhat truong AvailableQuantity
	 * @param productId
	 * @param ownerType
	 * @param ownerId
	 * @param avaiQuantity
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	int updateAvailableQuatity(Long productId,
			StockObjectType ownerType, Long ownerId, Integer avaiQuantity)
			throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @return
	 * @throws DataAccessException 
	 */
	List<RptCustomerStock2VO> getListRptCustomerStockVO(Long shopId, Date date)
			throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @param productCode
	 * @param quantity
	 * @return
	 * @throws DataAccessException
	 */
	boolean checkQuantityInStockTotal(long shopId, String productCode,
			int quantity) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param productCode
	 * @param shop
	 * @param id
	 * @throws DataAccessException 
	 */
	StockTotal getStockTotalByProductCodeAndOwner(String productCode,
			StockObjectType ownerType, Long ownerId) throws DataAccessException;
	StockTotalVO getStockTotalByProductCodeAndOwnerEx(String productCode,
			StockObjectType ownerType, Long ownerId) throws DataAccessException;

	/**
	 * Lay danh sach san pham ton kho cho nhap kho, xuat kho 
	 * @author tientv11
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<StockTotalVO> getListStockTotalVO(StockTotalFilter filter )throws DataAccessException;
	
	
	/**
	 * Lay danh sach san pham nhap lieu tren grid
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<StockTotalVO> getListProductImportExport(StockTotalFilter filter) throws DataAccessException;
	
	

	List<StockTotalVO> getListStockTotalVOForImExport(
			KPaging<StockTotalVO> kPaging, String productCode,
			String productName, String promotionProgramCode, Long ownerId,
			StockObjectType ownerType, Long catId, Long subCatId,
			Integer fromQuantity, Integer toQuantity, List<Long> exceptProductId)
			throws DataAccessException;
	
	List<ProductLotVO> getListProductInStockTotal(Long shopId,Long ownerId,
			StockObjectType ownerType, Integer fromQuantity)
			throws DataAccessException;
	
	List<StockTotalVO> getListProductForImExport(KPaging<StockTotalVO> kPaging,
			String productCode, String productName, Long objectId,
			StockObjectType objectType, Integer fromQuantity)
			throws DataAccessException;
	
	/**
	 * 
	 * @author tientv	
	 * @throws DataAccessException
	 * @param shopId
	 * @description : Danh sach san pham ma co the ban cho npp, bao gom ca ton kho 
	 */
	List<StockTotalVO> getListProductsCanSalesForShop(Long shopId) throws DataAccessException;

	List<StockTotal> getListStockTotalNew(KPaging<StockTotal> kPaging,
			StockTotalVOFilter stockTotalVoFilter) throws DataAccessException;

	List<StockTotalVO> getListStockTotalVONew(KPaging<StockTotalVO> kPaging,
			StockTotalVOFilter stockTotalVoFilter) throws DataAccessException;
	
	List<StockTotal> getListStockTotalNewQuantity(KPaging<StockTotal> kPaging,
			StockTotalVOFilter stockTotalVoFilter) throws DataAccessException;
	
	void updateStockLock(StockLock stockLock) throws DataAccessException;

	void insertStockLock(StockLock stockLock) throws DataAccessException;
	
	/**
	 * @author nhanlt6	
	 * @throws DataAccessException
	 * @param shopId
	 * @param warehouseId
	 * @param saleOrderId
	 * @description : Danh sach so luong san pham ban cho npp load theo kho
	 */
	public List<StockTotalVO> getListProductsCanSalesForShopByWarehouse(Long shopId, Long warehouseId, Long saleOrderId) throws DataAccessException;
	
	/**
	 * @author tungmt	
	 * @throws DataAccessException
	 * @param shopId
	 * @description : Danh sach stocktotal
	 */
	public List<StockTotal> getListStockTotalByShopAndProduct(StockTotalVOFilter filter) throws DataAccessException;
	
	/**
	 * Lay kho co do uu tien cao nhat
	 * 
	 * @author lacnv1
	 * @since Sep 08, 2014
	 */
	Warehouse getMostPriorityWareHouse(long shopId, Long productId) throws DataAccessException;
	
	/**
	 * Lay ds sp trong kho
	 * 
	 * @author lacnv1
	 * @since Nov 03, 2014
	 */
	List<StockTotalVO> getListProductForCycleCount(KPaging<StockTotalVO> paging, long cycleCountId, StockTotalVOFilter filter) throws DataAccessException;
	/**
	 * Lay ds sp kiem ke
	 * 
	 * @author hoanv25
	 * @since Dec 11, 2014
	 */
	List<StockTotalVO> getListProductForCycleCountCategoryStock(KPaging<StockTotalVO> paging, long cycleCountId, StockTotalVOFilter filter) throws DataAccessException;
	
	/**
	 * lay danh sach stock_total tuong ung voi sale_order_lot cua don hang
	 * @author tuannd20
	 * @param saleOrderId id don hang
	 * @param isSubtractProductQuantityInApprovedOrder true: tru di so luong cua san pham - kho tuong ung cua cac don hang da qua buoc xac nhan don IN trong ngay. Nguoc lai, false
	 * @return danh sach stock_total tuong ung voi sale_order_lot trong don hang
	 * @throws DataAccessException
	 * @since 30/01/2015
	 */
	List<StockTotalVO> retrieveStockTotalsForSaleOrder(Long saleOrderId, Boolean isSubtractProductQuantityInApprovedOrder) throws DataAccessException;
	
	/**
	 * Lay danh sach ton kho cua sp trong kho nha phan phoi, co cong lai so luong da dat trong don hang neu co sale_order_lot
	 * 
	 * @author lacnv1
	 * @param StockTotalFilter
	 * @return
	 * @throws DataAccessException
	 */
	List<StockTotal> getListStockTotalByProductAndShop(StockTotalFilter filter) throws DataAccessException;

	/**
	 * lay danh sach ton kho theo the kiem ke
	 * @param cycleCount
	 * @return
	 * @throws DataAccessException
	 */
	List<StockTotal> getListStockTotalByCycleCount(CycleCount cycleCount) throws DataAccessException;

	/**
	 * insert danh sach stockTotal
	 * @author trietptm
	 * @param listStockTotal
	 * @throws DataAccessException
	 * @since Oct 07, 2015
	 */
	void createListStockTotal(List<StockTotal> listStockTotal) throws DataAccessException;

}

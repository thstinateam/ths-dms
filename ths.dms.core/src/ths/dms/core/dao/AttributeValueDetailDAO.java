package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.AttributeValueDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.exceptions.DataAccessException;

public interface AttributeValueDetailDAO {

	List<AttributeValueDetail> getListAttrValueDetailFromObj(Long objId, Long attributeId, ActiveType status) throws DataAccessException;

	AttributeValueDetail getAttributeValueDetailByDetailAndValue(
			Long attrDetailId, Long attrValueId) throws DataAccessException;

	AttributeValueDetail createAttributeValueDetail(
			AttributeValueDetail attributeValueDetail)
			throws DataAccessException;

	void updateAttributeValueDetail(AttributeValueDetail attributeValueDetail)
			throws DataAccessException;

}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StaffCustomer;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class StaffCustomerDAOImpl implements StaffCustomerDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public StaffCustomer createStaffCustomer(StaffCustomer staffCustomer, LogInfoVO logInfo) throws DataAccessException {
		if (staffCustomer == null) {
			throw new IllegalArgumentException("staffCustomer");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}

		repo.create(staffCustomer);
		
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, staffCustomer, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(StaffCustomer.class, staffCustomer.getId());
	}

	@Override
	public void deleteStaffCustomer(StaffCustomer staffCustomer, LogInfoVO logInfo) throws DataAccessException {
		if (staffCustomer == null) {
			throw new IllegalArgumentException("staffCustomer");
		}
		repo.delete(staffCustomer);
		try {
			actionGeneralLogDAO.createActionGeneralLog(staffCustomer, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public StaffCustomer getStaffCustomerById(long id) throws DataAccessException {
		return repo.getEntityById(StaffCustomer.class, id);
	}
	
	@Override
	public void updateStaffCustomer(StaffCustomer staffCustomer, LogInfoVO logInfo) throws DataAccessException {
		if (staffCustomer == null) {
			throw new IllegalArgumentException("staffCustomer");
		}
		StaffCustomer temp = this.getStaffCustomerById(staffCustomer.getId());
		
		repo.update(staffCustomer);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, staffCustomer, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public StaffCustomer getStaffCustomer(Long staffId, Long customerId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from staff_customer where staff_id = ? and customer_id = ?");
		params.add(staffId);
		params.add(customerId);
		return repo.getEntityBySQL(StaffCustomer.class, sql.toString(), params);
	}
}

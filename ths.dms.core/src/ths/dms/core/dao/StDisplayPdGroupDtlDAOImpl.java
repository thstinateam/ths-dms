package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StDisplayPdGroupDtl;
import ths.dms.core.entities.StDisplayPlAmtDtl;
import ths.dms.core.entities.StDisplayPlDpDtl;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StDisplayPdGroupFilter;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.StDisplayPDGroupDTLVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class StDisplayPdGroupDtlDAOImpl implements StDisplayPdGroupDtlDAO{

	@Autowired
	private IRepository repo;
	
	@Autowired
	CommonDAO commonDAO;
	
	
	@Override
	public List<StDisplayPdGroupDtl> getListDisplayProductGroupDTL(
			KPaging<StDisplayPdGroupDtl> kPaging,
			Long displayProductGroupDTLId, ActiveType status)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT dpgd.* FROM display_product_group_dtl dpgd JOIN product p ON dpgd.product_id = p.product_id WHERE 1=1 ");
		if (status != null) {
			sql.append(" and dpgd.status=?");
			params.add(status.getValue());
		}
		if (displayProductGroupDTLId != null) {
			sql.append(" and dpgd.DISPLAY_PRODUCT_GROUP_ID=?");
			params.add(displayProductGroupDTLId);
		}
		sql.append(" order by p.product_code");
		if (kPaging == null)
			return repo.getListBySQL(StDisplayPdGroupDtl.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StDisplayPdGroupDtl.class, sql.toString(), params, kPaging);
	}

	@Override
	public void updateDisplayProductGroupDTL(
			StDisplayPdGroupDtl displayProductGroupDTL, LogInfoVO logInfo)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (displayProductGroupDTL == null) {
			throw new IllegalArgumentException("displayProductGroupDTL is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		StDisplayPdGroupDtl temp = this.getDisplayProductGroupDTLById(displayProductGroupDTL.getId());
		displayProductGroupDTL.setUpdateDate(commonDAO.getSysDate());
		displayProductGroupDTL.setUpdateUser(logInfo.getStaffCode());
		repo.update(displayProductGroupDTL);
	}
	
	@Override
	public List<StDisplayPDGroupDTLVO> getDisplayPDGroupDTLVOs(Long levelId,Long st_display_pd_group_id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>(); 
		sql.append(" select sdpgd.DISPLAY_PRODUCT_GROUP_DTL_ID as id,sdpg.display_dp_group_code as groupCode,p.product_code as productCode,p.product_name as productName ,quantity");
		sql.append(" from DISPLAY_PRODUCT_GROUP_DTL sdpgd");
		sql.append(" left join display_product_group sdpg on sdpgd.display_product_group_id = sdpg.display_product_group_id");
		sql.append(" left join product p on sdpgd.product_id = p.product_id");
		sql.append(" left join display_pl_dp_dtl sdpdd on sdpgd.DISPLAY_PRODUCT_GROUP_DTL_ID =  sdpdd.DISPLAY_PRODUCT_GROUP_DTL_ID and sdpdd.display_progRAM_level_id = ?");
		sql.append(" where sdpgd.display_product_group_id = ? and sdpgd.status=1 ");
		sql.append(" order by p.product_code,p.product_name ");
		
		params.add(levelId);
		params.add(st_display_pd_group_id);
		
		 String[] fieldNames = { 
				 	"id",//1
					"groupCode",//1
					"productCode",//2
					"productName",//3
					"quantity"
		 };
		 Type[] fieldTypes = { 
				 	StandardBasicTypes.LONG, // 1
					StandardBasicTypes.STRING, // 1
					StandardBasicTypes.STRING, // 2
					StandardBasicTypes.STRING, // 3
					StandardBasicTypes.INTEGER, // 4
		 };
		 return repo.getListByQueryAndScalar(StDisplayPDGroupDTLVO.class, fieldNames, fieldTypes, sql.toString(), params);		
	}
	
	
	@Override
	public StDisplayPlAmtDtl getStDisplayPlAmtDtlByAmtGr(Long levelId,Long grId)	throws DataAccessException {
		if(grId==null || levelId==null){
			throw new IllegalArgumentException("displayProductGroupDTL is null");
		}
		String sql = "select * from display_pl_amt_dtl where DISPLAY_PRODUCT_GROUP_ID=? and  status=1 and display_program_level_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(grId);
		params.add(levelId);
		return repo.getEntityBySQL(StDisplayPlAmtDtl.class, sql.toString(), params);
	}
	
	@Override
	public StDisplayPlDpDtl getStDisplayPlPrDtlByGr(Long grId,Long productId)	throws DataAccessException {
		if(grId==null){
			throw new IllegalArgumentException("grId is null");
		}
		String sql = "select * from display_pl_dp_dtl where display_program_level_id=? and display_product_group_dtl_id=? and status=1 ";
		List<Object> params = new ArrayList<Object>();
		params.add(grId);
		params.add(productId);
		return repo.getEntityBySQL(StDisplayPlDpDtl.class, sql, params);
	}
	
	@Override
	public StDisplayPlAmtDtl createStDisplayPlAmtDtlByAmtGr(StDisplayPlAmtDtl displayPlAmtDtl)
			throws DataAccessException {
		return repo.create(displayPlAmtDtl);
	}

	@Override
	public void updateStDisplayPlAmtDtlByAmtGr(StDisplayPlAmtDtl displayPlAmtDtl) throws DataAccessException {
		repo.update(displayPlAmtDtl);
		
	}

	@Override
	public StDisplayPlDpDtl createStDisplayPlPrDtlByGr(StDisplayPlDpDtl displayPlAmtDtl) throws DataAccessException {
		return repo.create(displayPlAmtDtl);
	}

	@Override
	public void updateDisplayPlPrDtlByGr(StDisplayPlDpDtl displayPlAmtDtl) throws DataAccessException {
		repo.update(displayPlAmtDtl);
		
	}
	
	@Override
	public StDisplayPdGroupDtl getDisplayProductGroupDTLById(long id) throws DataAccessException {
		return repo.getEntityById(StDisplayPdGroupDtl.class, id);
	}
	
	@Override
	public StDisplayPdGroupDtl createDisplayProductGroupDTL(
			StDisplayPdGroupDtl displayProductGroupDTL, LogInfoVO logInfo)
			throws DataAccessException {
		if (displayProductGroupDTL == null) {
			throw new IllegalArgumentException("displayProductGroupDTL is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		displayProductGroupDTL.setCreateUser(logInfo.getStaffCode());
		displayProductGroupDTL = repo.create(displayProductGroupDTL);
		return displayProductGroupDTL;//repo.getEntityById(StDisplayPdGroupDtl.class, displayProductGroupDTL.getId());
	}
	
	@Override
	public StDisplayPdGroupDtl getStDisplayPdGroupDtlbyProductId(Long productId) throws DataAccessException{
		if (productId != null && productId.longValue() > 0){
			StringBuilder sql = new StringBuilder();
			sql.append("select * from display_product_group_dtl where product_id = ?");
			List<Object> params = new ArrayList<Object>();
			params.add(productId);
			return repo.getEntityBySQL(StDisplayPdGroupDtl.class, sql.toString(), params);
		}
		
		return null;
	}
	
	@Override
	public StDisplayPdGroupDtl getStDisplayPdGroupDtlByProductId(Long stDisplayPdGroupDtlId, Long productId) throws DataAccessException{
		if (stDisplayPdGroupDtlId != null && stDisplayPdGroupDtlId.longValue() >= 0){
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			params.add(stDisplayPdGroupDtlId);
			sql.append("select * from display_product_group_dtl where display_product_group_dtl_id = ? ");
			if (productId != null && productId.longValue() >= 0){
				sql.append(" and product_id = ?");
				params.add(productId);
			}
			return repo.getEntityBySQL(StDisplayPdGroupDtl.class, sql.toString(), params);
		}
		return null;
	}
	/**
	 * @author thachnn
	 */
	@Override
	public StDisplayPdGroupDtl createStDisplayPdGroupDtl(StDisplayPdGroupDtl stDisplayPdGroupDtl, LogInfoVO logInfo) throws DataAccessException{
		if (stDisplayPdGroupDtl == null) {
			throw new IllegalArgumentException("displayProductGroupDTL is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		stDisplayPdGroupDtl.setCreateUser(logInfo.getStaffCode());
		stDisplayPdGroupDtl = repo.create(stDisplayPdGroupDtl);
		
		return stDisplayPdGroupDtl;//repo.getEntityById(StDisplayPdGroupDtl.class, stDisplayPdGroupDtl.getId());
	}
	
	@Override
	public StDisplayPdGroupDtl getDisplayProductGroupDTLByGroupId(Long stDisplayPdGroupId,
			Long productId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM display_product_group_dtl dpgd WHERE 1=1 and dpgd.status != -1");
		if (stDisplayPdGroupId != null) {
			sql.append(" and dpgd.display_product_group_id = ? ");
			params.add(stDisplayPdGroupId);
		}
		if (stDisplayPdGroupId != null) {
			sql.append(" and dpgd.product_id = ? ");
			params.add(productId);
		}
		return repo.getEntityBySQL(StDisplayPdGroupDtl.class, sql.toString(), params);
	}
}

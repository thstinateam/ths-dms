package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PoAutoGroupShopMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoAutoGroupShopMapFilter;
import ths.dms.core.entities.vo.GroupStaffPayrollVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PoAutoGroupShopMapVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class PoAutoGroupShopMapDAOImpl implements PoAutoGroupShopMapDAO{
	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Override
	public PoAutoGroupShopMap createPoAutoGroupShopMap(PoAutoGroupShopMap poAutoGroupShopMap, LogInfoVO logInfo) 
			throws DataAccessException {
		if (poAutoGroupShopMap == null) {
			throw new IllegalArgumentException("PoAutoGroupShopMap is null");
		}
		repo.create(poAutoGroupShopMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, poAutoGroupShopMap, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(PoAutoGroupShopMap.class,
				poAutoGroupShopMap.getId());
	}

	@Override
	public void deletePoAutoGroupShopMap(PoAutoGroupShopMap poAutoGroupShopMap, LogInfoVO logInfo)
			throws DataAccessException {
		if (poAutoGroupShopMap == null) {
			throw new IllegalArgumentException("PoAutoGroupShopMap is null");
		}
		repo.delete(poAutoGroupShopMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(poAutoGroupShopMap, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public void updatePoAutoGroupShopMap(PoAutoGroupShopMap poAutoGroupShopMap,
			LogInfoVO logInfo) throws DataAccessException {
		if (poAutoGroupShopMap == null) {
			throw new IllegalArgumentException("PoAutoGroupShopMap is null");
		}
		PoAutoGroupShopMap temp = this.getPoAutoGroupShopMapById(poAutoGroupShopMap.getId());
		temp = temp.clone();
		repo.update(poAutoGroupShopMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, poAutoGroupShopMap, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public PoAutoGroupShopMap getPoAutoGroupShopMapById(Long id) throws DataAccessException {
		return repo.getEntityById(PoAutoGroupShopMap.class, id);
	}
	
	@Override
	public Boolean checkExistsShopInGroup(Long shopId)
			throws DataAccessException {
		if(null == shopId){
			throw new IllegalArgumentException("shop id is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select count(1) as count ");
		sql.append(" from po_auto_group_shop_map ");
		sql.append(" where shop_id = ?  ");
		params.add(shopId);
		sql.append("     and status = ? ");
		params.add(ActiveType.RUNNING.getValue());

		return this.repo.countBySQL(sql.toString(), params) > 0 ? true : false;
	}
	
	@Override
	public List<PoAutoGroupShopMapVO> getListShopInPoGroup(PoAutoGroupShopMapFilter filter,
			ActiveType groupStatus) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select sh.shop_id as shopId, sh.shop_code as shopCode, sh.shop_name as shopName, ");
		sql.append("	gr.po_auto_group_shop_map_id as groupId, gr.status as groupStatus ");
		sql.append(" from shop sh, po_auto_group_shop_map gr ");
		sql.append(" where sh.shop_id = gr.shop_id ");
		sql.append("     and sh.status != ? and sh.status = 1 ");
		params.add(ActiveType.DELETED.getValue());
		
		if (null != groupStatus) {
			sql.append("     and gr.status = ? ");
			params.add(groupStatus.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("     and sh.shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode()).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append("     and upper(sh.shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getShopName()).toUpperCase());
		}
		final String[] fieldNames = new String[] {
				"shopId", "shopCode", "shopName",
				"groupId", "groupStatus"};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.LONG, StandardBasicTypes.INTEGER};
		sql.append(" order by sh.shop_code ASC ");
		if (filter.getkPaging() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(PoAutoGroupShopMapVO.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, filter.getkPaging());
		}
		return repo.getListByQueryAndScalar(PoAutoGroupShopMapVO.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}
}

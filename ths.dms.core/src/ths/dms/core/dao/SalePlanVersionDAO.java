package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.SalePlanVersion;
import ths.dms.core.exceptions.DataAccessException;


public interface SalePlanVersionDAO {
	
	SalePlanVersion createSalePlanVersion(SalePlanVersion salePlanVersion) throws DataAccessException;
	
	void createSalePlanVersion(List<SalePlanVersion> listSalePlanVersion) throws DataAccessException;

	void deleteSalePlanVersion(SalePlanVersion salePlanVersion) throws DataAccessException;

	void updateSalePlanVersion(SalePlanVersion salePlanVersion) throws DataAccessException;
	
	SalePlanVersion getSalePlanVersionById(long id) throws DataAccessException;

	/**
	 * getMaxVersionBySalePlanSersionId
	 * @author hieunq1
	 * @param id
	 * @return
	 * @throws DataAccessException
	 */
	Integer getMaxVersionBySalePlanSersionId(long id) throws DataAccessException;
}

package ths.dms.core.dao;

import ths.dms.core.entities.StaffCustomer;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface StaffCustomerDAO {

	StaffCustomer createStaffCustomer(StaffCustomer staffStaffCustomer, LogInfoVO logInfo) throws DataAccessException;

	void deleteStaffCustomer(StaffCustomer staffStaffCustomer, LogInfoVO logInfo) throws DataAccessException;

	void updateStaffCustomer(StaffCustomer staffStaffCustomer, LogInfoVO logInfo) throws DataAccessException;
	
	StaffCustomer getStaffCustomerById(long id) throws DataAccessException;

	StaffCustomer getStaffCustomer(Long staffId, Long customerId)
			throws DataAccessException;

	

}

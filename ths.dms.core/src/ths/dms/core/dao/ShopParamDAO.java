/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.ProductShopMap;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ShopParamType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ShopParamVO;
import ths.dms.core.entities.vo.ShopParamVOProduct;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Mo ta class ShopParamDAO.java
 * @author vuongmq
 * @since Nov 28, 2015
 */
public interface ShopParamDAO {

	/**
	 * Xu ly createShopParam
	 * @author vuongmq
	 * @param shopParam
	 * @param logInfo
	 * @return ShopParam
	 * @throws DataAccessException
	 * @since Nov 28, 2015
	*/
	ShopParam createShopParam(ShopParam shopParam, LogInfoVO logInfo) throws DataAccessException;

	/**
	 * Xu ly deleteShopParam
	 * @author vuongmq
	 * @param shopParam
	 * @param logInfo
	 * @throws DataAccessException
	 * @since Nov 28, 2015
	*/
	void deleteShopParam(ShopParam shopParam, LogInfoVO logInfo) throws DataAccessException;

	/**
	 * Xu ly updateShopParam
	 * @author vuongmq
	 * @param shopParam
	 * @param logInfo
	 * @throws DataAccessException
	 * @since Nov 28, 2015
	*/
	void updateShopParam(ShopParam shopParam, LogInfoVO logInfo) throws DataAccessException;

	/**
	 * Xu ly getShopParamById
	 * @author vuongmq
	 * @param id
	 * @return ShopParam
	 * @throws DataAccessException
	 * @since Nov 28, 2015
	*/
	ShopParam getShopParamById(long id) throws DataAccessException;

	/**
	 * Xu ly getShopParamByShopIdCode
	 * @author vuongmq
	 * @param shopId
	 * @param code
	 * @param activeType
	 * @return ShopParam
	 * @throws DataAccessException
	 * @since Nov 28, 2015
	*/
	ShopParam getShopParamByShopIdCode(Long shopId, String code, ActiveType activeType) throws DataAccessException;

	/**
	 * Xu ly getShopParam
	 * @author vuongmq
	 * @param shopId
	 * @param code
	 * @param type
	 * @param activeType
	 * @return ShopParam
	 * @throws DataAccessException
	 * @since Nov 28, 2015
	*/
	ShopParam getShopParam(Long shopId, String code, ShopParamType type, ActiveType activeType) throws DataAccessException;

	/**
	 * Xu ly getListShopParamVOByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<ShopParamVO>
	 * @throws DataAccessException
	 * @since Nov 30, 2015
	*/
	List<ShopParamVO> getListShopParamVOByFilter(BasicFilter<ShopParamVO> filter) throws DataAccessException;

	/**
	 * Xu ly getListShopParamVOViewTimeByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<ShopParamVO>
	 * @throws DataAccessException
	 * @since Nov 30, 2015
	*/
	List<ShopParamVO> getListShopParamVOViewTimeByFilter(BasicFilter<ShopParamVO> filter) throws DataAccessException;

	/**
	 * Xu ly getListProductShopMapByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<ShopParamVOProduct>
	 * @throws DataAccessException
	 * @since Dec 2, 2015
	*/
	List<ShopParamVOProduct> getListProductShopMapByFilter(BasicFilter<ShopParamVOProduct> filter) throws DataAccessException;

	/**
	 * Xu ly getProductShopMapByShopProductId
	 * @author vuongmq
	 * @param shopId
	 * @param productId
	 * @return ProductShopMap
	 * @throws DataAccessException
	 * @since Dec 2, 2015
	*/
	ProductShopMap getProductShopMapByShopProductId(Long shopId, Long productId) throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.DisplayTool;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.DisplayToolCustomerFilter;
import ths.dms.core.entities.filter.DisplayToolFilter;
import ths.dms.core.entities.vo.DisplayToolCustomerVO;
import ths.dms.core.entities.vo.DisplayToolVO;
import ths.dms.core.entities.vo.StaffSimpleVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class DisplayToolDAOImpl implements DisplayToolDAO {

	@Autowired
	private IRepository repo;

	@Override
	public DisplayTool createDisplayTool(DisplayTool displayTool) throws DataAccessException {
		if (displayTool == null) {
			throw new IllegalArgumentException("displayTool");
		}
		repo.create(displayTool);
		return repo.getEntityById(DisplayTool.class, displayTool.getDisplayToolsId());
	}

	@Override
	public void deleteDisplayTool(DisplayTool displayTool) throws DataAccessException {
		if (displayTool == null) {
			throw new IllegalArgumentException("displayTool");
		}
		repo.delete(displayTool);
	}

	@Override
	public DisplayTool getDisplayToolById(long id) throws DataAccessException {
		return repo.getEntityById(DisplayTool.class, id);
	}
	
	@Override
	public void updateDisplayTool(DisplayTool displayTool) throws DataAccessException {
		if (displayTool == null) {
			throw new IllegalArgumentException("displayTool");
		}
		repo.update(displayTool);
	}
	
	@Override
	public List<DisplayToolVO> getListDisplayTool(DisplayToolFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT p.product_id AS displayToolId, p.product_code AS displayToolCode, p.product_name AS displayToolName");
		//sql.append(" FROM  product p");
		//vuongmq: them dk ket sang bang product_info lay thong tin nganh hang
		sql.append(" FROM product p join product_info pi on p.cat_id = pi.product_info_id");
		sql.append(" where 1=1 and pi.product_info_code = ?");
		params.add('Z');
		if (!StringUtility.isNullOrEmpty(filter.getDisplayToolCode())) {
			sql.append(" and p.product_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getDisplayToolCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getDisplayToolName())) {
			sql.append(" and upper(p.product_name) like ? ESCAPE '/'");
			//params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getDisplayToolName()).toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getDisplayToolName()).toUpperCase()));
		}
		if (filter.getStatus() != null) {
			sql.append(" and p.status = ? ");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and p.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if(Boolean.TRUE.equals(filter.getExceptAllDisplayToolOfProduct())){
			sql.append(" and p.product_id not in (select distinct display_tools_product.tool_id from display_tools_product)");
		}
		if (filter.getLstExceptionalDisplayToolId() != null) {
			sql.append(" and p.product_id not in (");
			for (Long id: filter.getLstExceptionalDisplayToolId()) {
				if (id != null) {
					sql.append("?,");
					params.add(id);
				}
			}
			sql.append("-1)");
		}
		
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from (");
		countSql.append(sql.toString());
		countSql.append(")");
		
		sql.append(" order by p.product_code");
		
		String[] fieldNames = {"displayToolId", "displayToolCode", "displayToolName"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING};
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(DisplayToolVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		else
			return repo.getListByQueryAndScalarPaginated(DisplayToolVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}
	
	@Override
	public List<DisplayToolVO> getListDisplayToolOfProduct(DisplayToolFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct dtp.tool_id AS displayToolId, p.product_code as displayToolCode  , p.product_name as displayToolName");
		sql.append(" from display_tools_product dtp join product p on dtp.tool_id = p.product_id");
		sql.append(" where 1=1");
		
		if (!StringUtility.isNullOrEmpty(filter.getDisplayToolCode())) {
			sql.append(" and p.product_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getDisplayToolCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getDisplayToolName())) {
			sql.append(" and upper(p.PRODUCT_NAME) like ? ESCAPE '/'");
			//params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getDisplayToolName()).toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getDisplayToolName()).toUpperCase()));
			//params.add(StringUtility.toOracleSearchLike(Unicode2English.unicode2javaUnicode(filter.getDisplayToolName()).toUpperCase()));
		}
		if (filter.getStatus() != null) {
			sql.append(" and p.status = ? ");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and p.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from (");
		countSql.append(sql.toString());
		countSql.append(")");
		
		sql.append(" order by p.product_code");
		
		String[] fieldNames = {"displayToolId","displayToolCode", "displayToolName"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING};
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(DisplayToolVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		else
			return repo.getListByQueryAndScalarPaginated(DisplayToolVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}
	
	@Override
	public List<Product> getListProductOfDisplayTool(KPaging<Product> kPaging, Long displayToolId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (displayToolId == null) {
			throw new IllegalArgumentException("displayToolId");
		}
		sql.append(" select * from product where 1=1");
		sql.append(" and status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and product_id in (select dtp.product_id from display_tools_product dtp where dtp.tool_id = ?) ");
		params.add(displayToolId);
		sql.append(" order by product_code");
		if (kPaging != null)
			return repo.getListBySQLPaginated(Product.class, sql.toString(), params, kPaging);
		else
			return repo.getListBySQL(Product.class, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * @see KYTN
	 */
	@Override
	public List<DisplayToolCustomerVO> getListDisplayToolCustomerVo(KPaging<DisplayToolCustomerVO> kPaging,
			DisplayToolCustomerFilter filter) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT ");
		sql.append(" DT.DISPLAY_TOOLS_ID as id, ");
		sql.append(" DT.SHOP_ID as shopId, ");
		sql.append(" (SELECT s.SHOP_CODE FROM SHOP s WHERE s.SHOP_ID=dt.shop_id) as shopCode, ");
		sql.append(" DT.STAFF_ID as staffId, ");
		sql.append(" (SELECT ST.STAFF_CODE FROM STAFF st WHERE ST.STAFF_ID=DT.STAFF_ID) as staffCode, ");
		sql.append(" (SELECT ST.STAFF_NAME FROM STAFF st WHERE ST.STAFF_ID=DT.STAFF_ID) as staffName, ");
		sql.append(" (SELECT concat(concat(ST.STAFF_CODE,'-'),ST.STAFF_NAME) FROM STAFF st WHERE ST.STAFF_ID=DT.STAFF_ID) as staffCodeName, ");
		sql.append(" (SELECT SUBSTR(c.CUSTOMER_CODE,0,3) FROM CUSTOMER c WHERE c.CUSTOMER_ID=DT.CUSTOMER_ID) as shorCode, ");
		sql.append(" (SELECT c.CUSTOMER_CODE FROM CUSTOMER c WHERE c.CUSTOMER_ID=DT.CUSTOMER_ID) as customerCode, ");
		sql.append(" (SELECT CONCAT(CONCAT(SUBSTR(c.CUSTOMER_CODE,0,3),'-'),c.CUSTOMER_NAME) FROM CUSTOMER c WHERE c.CUSTOMER_ID=DT.CUSTOMER_ID) as customerCodeName, ");
		sql.append(" (SELECT c.CUSTOMER_NAME FROM CUSTOMER c WHERE c.CUSTOMER_ID=DT.CUSTOMER_ID) as customerName, ");
		sql.append(" (SELECT p.PRODUCT_CODE FROM PRODUCT p WHERE p.PRODUCT_ID=DT.PRODUCT_ID) as toolCode, ");
		sql.append(" DT.AMOUNT as amount, ");
		sql.append(" dt.QUANTITY as quantity, ");
		sql.append(" to_char(DT.IN_MONTH,'mm/yyyy') as month ");
		sql.append(" FROM DISPLAY_TOOLS dt ");
		sql.append(" WHERE 1=1 ");
		//sql.append(" and DT.CUSTOMER_ID in (SELECT c.CUSTOMER_ID FROM CUSTOMER c WHERE c.status=1) ");
		if(filter.getGsnppId()!=null && filter.getGsnppId()>0){
			sql.append(" AND DT.STAFF_ID IN (SELECT ST.STAFF_ID FROM STAFF st WHERE ST.status=1 and st.staff_owner_id =? ) ");
			params.add(filter.getGsnppId());
		}else{
			//sql.append(" AND DT.STAFF_ID IN (SELECT ST.STAFF_ID FROM STAFF st WHERE ST.status=1) ");
		}		
		//sql.append(" AND DT.PRODUCT_ID IN (SELECT PRODUCT_ID FROM PRODUCT p WHERE p.status=1) ");
		if(!StringUtility.isNullOrEmpty(filter.getMonth())){
			sql.append(" AND TO_CHAR(DT.IN_MONTH, 'MM/YYYY') =? ");
			params.add(filter.getMonth());
		}
		if(filter.getShopId()!=null){
			sql.append(" AND DT.SHOP_ID in ( ");
			sql.append(" SELECT shop_id FROM shop START WITH shop_id = ? ");
			params.add(filter.getShopId());
			sql.append(" CONNECT BY prior shop_id = parent_shop_id)");
		}
		if(filter.getStaffId()!=null){
			sql.append(" AND DT.STAFF_ID=? ");
			params.add(filter.getStaffId());
		}	
		
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from (");
		countSql.append(sql.toString());
		countSql.append(")");		
		sql.append(" order by shopCode,staffCode,customerCodeName,toolCode ");

		final String[] fieldNames = new String[] {
				"id", //1
				"shopId", //2
				"shopCode",//3
				"staffId",//4
				"staffCode",//5
				"staffName",//5
				"staffCodeName",//5
				"shorCode",//6
				"customerCode",//6
				"customerCodeName",//6
				"customerName",//6
				"toolCode",//7
				"amount",//8
				"quantity",//9
				"month"};//10
		final Type[] fieldTypes = new Type[] {
		StandardBasicTypes.LONG, //1
		StandardBasicTypes.LONG, //2
		StandardBasicTypes.STRING, //3
		StandardBasicTypes.LONG,//4
		StandardBasicTypes.STRING,//5
		StandardBasicTypes.STRING,//5
		StandardBasicTypes.STRING,//5
		StandardBasicTypes.STRING,//6
		StandardBasicTypes.STRING,//6
		StandardBasicTypes.STRING,//6
		StandardBasicTypes.STRING,//6
		StandardBasicTypes.STRING,//7
		StandardBasicTypes.BIG_DECIMAL,//8
		StandardBasicTypes.INTEGER,//9
		StandardBasicTypes.STRING};//10
		
		if (kPaging == null)
			return repo.getListByQueryAndScalar(DisplayToolCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(DisplayToolCustomerVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	@Override
	public List<DisplayTool> checkListDisplayTool(List<DisplayTool> list)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if(list==null){
			throw new IllegalArgumentException("list can be not null");
		}
		StringBuilder sql=new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * ");
		sql.append(" FROM DISPLAY_TOOLS DP ");
		sql.append(" WHERE 1=1  ");
		
		int i=0;
		sql.append(" AND ( ");
		for( DisplayTool item:list){
			if(i==0){
				sql.append(" ( ");
			}
			else{
				sql.append(" OR( ");
			}
			sql.append(" DP.SHOP_ID=? ");
			//params.add(item.getShop().getShopId());
			params.add(item.getShop().getId());
			sql.append(" AND DP.STAFF_ID=? ");
			//params.add(item.getStaff().getStaffId());
			params.add(item.getStaff().getId());
			sql.append(" AND DP.CUSTOMER_ID=? ");
			params.add(item.getCustomerId());
			sql.append(" AND DP.PRODUCT_ID=? ");
			//params.add(item.getProduct().getProductId());
			params.add(item.getProduct().getId());
			sql.append(" AND TRUNC(DP.IN_MONTH,'YYYY')=TRUNC(?, 'YYYY') ");
			params.add(item.getInMonth());
			sql.append(" AND TRUNC(DP.IN_MONTH,'MM')=TRUNC(?, 'MM') ");
			params.add(item.getInMonth());
			sql.append(" ) ");
		}
		sql.append(" ) ");
		return repo.getListBySQL(DisplayTool.class, sql.toString(), params);
	}

	@Override
	public void createListDisplayTool(List<DisplayTool> list)
			throws DataAccessException {
		// TODO Auto-generated method stub
		repo.create(list);
	}

	@Override
	public void deleteListDisplayTools(List<Long> listId)throws DataAccessException {
		for(Long id : listId){
			DisplayTool dt = this.getDisplayToolById(id);
			if(dt==null)
				continue;
			this.deleteDisplayTool(dt);
		}		
	}

	@Override
	public Boolean checkDateMonthToDay(Long shopId, Long staffId, String month)
			throws DataAccessException {
		StringBuilder sql=new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as Count ");
		sql.append(" from display_tools dt ");
		sql.append(" where 1 = 1 ");
		if(staffId != null) {
			sql.append(" and dt.staff_id=? ");
			params.add(staffId);
		}
		if(shopId!=null){
			sql.append(" AND dt.SHOP_ID in ( ");
			sql.append(" SELECT shop_id FROM shop START WITH shop_id = ? ");
			params.add(shopId);
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ");
		}
		sql.append(" and trunc(dt.in_month,'MM') = trunc(to_date(?,'MM/YYYY')) ");
		params.add(month);
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}

	@Override
	public List<DisplayTool> getListDisplayTool(KPaging<DisplayTool> kPaging,
			DisplayToolFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();		
		sql.append(" select * from display_tools dt");
		sql.append(" where 1=1 ");
		if(filter.getStaffId() != null) {
			sql.append(" and dt.staff_id=? ");
			params.add(filter.getStaffId());
		}
		if(filter.getShopId() !=null){
			sql.append(" AND dt.SHOP_ID in ( ");
			sql.append(" SELECT shop_id FROM shop START WITH shop_id = ? ");
			params.add(filter.getShopId());
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ");
		}
		if(!StringUtility.isNullOrEmpty(filter.getInMonth())){
			sql.append(" and trunc(dt.in_month,'MM') = trunc(to_date(?,'MM/YYYY')) ");
			params.add(filter.getInMonth());
		}
		if (kPaging == null)
			return repo.getListBySQL(DisplayTool.class, sql.toString(),	params);
		else
			return repo.getListBySQLPaginated(DisplayTool.class,sql.toString(), params, kPaging);
	}
	
	/* Sao chep nhieu khach hang muon tu cua nhieu nhan vien */
	@Override
	public List<StaffSimpleVO> getListDisplayToolStaffId(DisplayToolFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();		
		sql.append(" select distinct dt.staff_id staffId, st.staff_code staffCode, st.staff_name staffName ");
		sql.append(" from display_tools dt join staff st on st.staff_id = dt.staff_id ");
		sql.append(" where 1=1 ");
		if(filter.getStaffId() != null) {
			sql.append(" and dt.staff_id = ? ");
			params.add(filter.getStaffId());
		}
		if(filter.getShopId()!=null){
			sql.append(" AND dt.SHOP_ID in ( ");
			sql.append(" SELECT shop_id FROM shop START WITH shop_id = ? ");
			params.add(filter.getShopId());
			sql.append(" CONNECT BY prior shop_id = parent_shop_id)");
		}
		if(!StringUtility.isNullOrEmpty(filter.getInMonth())){
			sql.append(" and trunc(dt.in_month,'MM') = trunc(to_date(?,'MM/YYYY')) ");
			params.add(filter.getInMonth());
		}
		String fileNames [] = new String[] {"staffId", "staffCode", "staffName"};
		Type fileTypes [] = new Type[] {StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING};
		return repo.getListByQueryAndScalar(StaffSimpleVO.class, fileNames, fileTypes, sql.toString(), params);
		//return repo.getListByNamedQuery(sql.toString(), params);
		//repo.getObjectByQuery(sql, params, synchronizedClass)
	}

	
}

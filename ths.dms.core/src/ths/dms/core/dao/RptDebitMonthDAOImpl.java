package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.RptDebitMonth;
import ths.dms.core.entities.enumtype.RptDebitMonthObjectType;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class RptDebitMonthDAOImpl implements RptDebitMonthDAO {

	@Autowired
	private IRepository repo;

	@Override
	public RptDebitMonth createRptDebitMonth(RptDebitMonth rptDebitMonth) throws DataAccessException {
		if (rptDebitMonth == null) {
			throw new IllegalArgumentException("rptDebitMonth");
		}
		repo.create(rptDebitMonth);
		return repo.getEntityById(RptDebitMonth.class, rptDebitMonth.getId());
	}

	@Override
	public void deleteRptDebitMonth(RptDebitMonth rptDebitMonth) throws DataAccessException {
		if (rptDebitMonth == null) {
			throw new IllegalArgumentException("rptDebitMonth");
		}
		repo.delete(rptDebitMonth);
	}

	@Override
	public RptDebitMonth getRptDebitMonthById(long id) throws DataAccessException {
		return repo.getEntityById(RptDebitMonth.class, id);
	}
	
	@Override
	public void updateRptDebitMonth(RptDebitMonth rptDebitMonth) throws DataAccessException {
		if (rptDebitMonth == null) {
			throw new IllegalArgumentException("rptDebitMonth");
		}
		repo.update(rptDebitMonth);
	}

	@Override
	public RptDebitMonth getRptDebitMonthByOwner(Long objectId, RptDebitMonthObjectType objectType, Date date) throws DataAccessException {
		if (objectId == null || objectId <= 0) {
			throw new IllegalArgumentException("ownerId is null or <= 0");
		}
		if (objectType == null) {
			throw new IllegalArgumentException("ownerType is null");
		}
		if (date == null) {
			throw new IllegalArgumentException("date is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM rpt_debit_month WHERE object_id = ? AND object_type = ? AND rpt_in_month >= trunc(?, 'MONTH') AND rpt_in_month < (trunc(?, 'MONTH') + interval '1' month)");
		params.add(objectId);
		params.add(objectType.getValue());
		params.add(date);
		return repo.getFirstBySQL(RptDebitMonth.class, sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.PoAutoGroupShopMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.PoAutoGroupShopMapFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PoAutoGroupShopMapVO;
import ths.dms.core.exceptions.DataAccessException;

public interface PoAutoGroupShopMapDAO {
	PoAutoGroupShopMap createPoAutoGroupShopMap(PoAutoGroupShopMap poAutoGroupShopMap, LogInfoVO logInfo) 
			throws DataAccessException;

	void deletePoAutoGroupShopMap(PoAutoGroupShopMap poAutoGroupShopMap, LogInfoVO logInfo)
			throws DataAccessException;

	void updatePoAutoGroupShopMap(PoAutoGroupShopMap poAutoGroupShopMap, LogInfoVO logInfo)
			throws DataAccessException;

	PoAutoGroupShopMap getPoAutoGroupShopMapById(Long id)
			throws DataAccessException;
	
	Boolean checkExistsShopInGroup(Long shopId) throws DataAccessException;

	List<PoAutoGroupShopMapVO> getListShopInPoGroup(PoAutoGroupShopMapFilter filter,
			ActiveType groupStatus) throws DataAccessException;
}
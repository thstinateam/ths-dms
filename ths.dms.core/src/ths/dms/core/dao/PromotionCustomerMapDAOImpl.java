package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PromotionCustomerMap;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PromotionCustomerMapType;
import ths.dms.core.entities.enumtype.PromotionShopMapFilter;
import ths.dms.core.entities.filter.PromotionCustomerFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PromotionCustomerVO;
import ths.dms.core.entities.vo.PromotionShopMapVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class PromotionCustomerMapDAOImpl implements PromotionCustomerMapDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Autowired
	CommonDAO commonDAO;

	@Autowired
	CustomerDAO customerDAO;
	
	@Override
	public PromotionCustomerMap createPromotionCustomerMap(PromotionCustomerMap promotionCustomerMap, LogInfoVO logInfo) throws DataAccessException {
		if (promotionCustomerMap == null) {
			throw new IllegalArgumentException("promotionCustomerMap");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		
//		if (promotionCustomerMap.getPromotionShopMap() != null) {
////			promotionCustomerMap.setPromotionProgram(promotionProgramDAO.getPromotionProgramById((promotionCustomerMap.getPromotionShopMap().getPromotionProgram().getId())));
//			if (promotionCustomerMap.getCustomer() != null)
//				if (checkIfRecordExist(promotionCustomerMap.getPromotionShopMap().getPromotionProgram().getId(), promotionCustomerMap.getCustomer().getCustomerCode(), null))
//					return null;
//		}
//		if (promotionCustomerMap.getQuantityMax() == null)
//			promotionCustomerMap.setQuantityReceived(null);
//		else if (promotionCustomerMap.getQuantityMax() != null && promotionCustomerMap.getQuantityReceived() == null)
//			promotionCustomerMap.setQuantityReceived(0);
		promotionCustomerMap.setCreateDate(commonDAO.getSysDate());
		promotionCustomerMap.setCreateUser(logInfo.getStaffCode());
		if(promotionCustomerMap.getId() != null) {
			repo.update(promotionCustomerMap);
		} else {
			repo.create(promotionCustomerMap);
		}
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, promotionCustomerMap, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		promotionCustomerMap = repo.getEntityById(PromotionCustomerMap.class, promotionCustomerMap.getId());
//		if (promotionCustomerMap.getType() == 1) {
//			this.deleteRelevantPromotionCustomerMap(promotionCustomerMap.getPromotionShopMap().getPromotionProgram().getId(), promotionCustomerMap.getId());
//		}
		return promotionCustomerMap;
	}

	@Override
	public void deletePromotionCustomerMap(PromotionCustomerMap promotionCustomerMap, LogInfoVO logInfo) throws DataAccessException {
		if (promotionCustomerMap == null) {
			throw new IllegalArgumentException("promotionCustomerMap");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.update(promotionCustomerMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(promotionCustomerMap, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public PromotionCustomerMap getPromotionCustomerMapById(long id) throws DataAccessException {
		return repo.getEntityById(PromotionCustomerMap.class, id);
	}
	
	@Override
	public PromotionCustomerMap getPromotionCustomerMap(Long promotionShopMapId, Long customerId) 
		throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from promotion_customer_map where promotion_shop_map_id = ? and status = 1");
		params.add(promotionShopMapId);
		if (customerId != null) {
			sql.append(" and customer_id = ?");
			params.add(customerId);
		}
		return repo.getFirstBySQL(PromotionCustomerMap.class, sql.toString(), params);
	}
	
	@Override
	public List<PromotionCustomerMap> getListPromotionCustomerMapByShop(Long promotionShopMapId) 
				throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from promotion_customer_map where promotion_shop_map_id = ? ");
		params.add(promotionShopMapId);
		
		return repo.getListBySQL(PromotionCustomerMap.class, sql.toString(), params);
	}
	
	@Override
	public void updatePromotionCustomerMap(PromotionCustomerMap promotionCustomerMap, LogInfoVO logInfo) throws DataAccessException {
		if (promotionCustomerMap == null) {
			throw new IllegalArgumentException("promotionCustomerMap");
		}
		
//		if (promotionCustomerMap.getPromotionShopMap() != null && promotionCustomerMap.getStatus() == ActiveType.RUNNING) {
////			promotionCustomerMap.setPromotionProgram(promotionCustomerMap.getPromotionShopMap().getPromotionProgram());
//			if (promotionCustomerMap.getCustomer() != null)
//				if (checkIfRecordExist(promotionCustomerMap.getPromotionShopMap().getPromotionProgram().getId(), promotionCustomerMap.getCustomer().getCustomerCode(), promotionCustomerMap.getId()))
//					return;
//		}
		PromotionCustomerMap temp = this.getPromotionCustomerMapById(promotionCustomerMap.getId());
		
		promotionCustomerMap.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null) {
			promotionCustomerMap.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(promotionCustomerMap);
//		if (promotionCustomerMap.getType() == 1) {
//			this.deleteRelevantPromotionCustomerMap(promotionCustomerMap.getPromotionShopMap().getPromotionProgram().getId(), promotionCustomerMap.getId());
//		}
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, promotionCustomerMap, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	
	@Override
	public Boolean checkIfRecordExist(long promotionProgramId, String customerCode, Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(1) as count from promotion_customer_map p ");
		sql.append(" where exists (select 1 from promotion_shop_map psm where p.promotion_shop_map_id=psm.promotion_shop_map_id and promotion_program_id=?) and exists(select 1 from customer c where p.customer_id=c.customer_id and customer_code=?) and status != ?");
		
		List<Object> params = new ArrayList<Object>();
		params.add(promotionProgramId);
		params.add(customerCode.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		
		if (id != null) {
			sql.append(" and promotion_customer_map_id != ?");
			params.add(id);
		}
		
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true; 
	}
	
	@Override
	public Boolean checkIfRecordExistEx(long promotionProgramId, String channelTypeCode, String shopCode, Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) as count from promotion_customer_map p ");
		sql.append(" where type = ? and ");
		params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER_TYPE.getValue());
		sql.append(" 	exists (select 1 from promotion_shop_map psm join shop s on psm.shop_id = s.shop_id");
		sql.append("		where p.promotion_shop_map_id=psm.promotion_shop_map_id and s.shop_code = ?");
		params.add(shopCode.toUpperCase());
		sql.append("		and promotion_program_id=?) ");
		params.add(promotionProgramId);
		sql.append("		and exists (select 1 from channel_type c ");
		sql.append("			where p.customer_type_id=c.channel_type_id and channel_type_code=?) ");
		params.add(channelTypeCode.toUpperCase());
		sql.append("		and status != ?");
		params.add(ActiveType.DELETED.getValue());
		
		if (id != null) {
			sql.append(" and promotion_customer_map_id != ?");
			params.add(id);
		}
		
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true; 
	}
	
	@Override
	public void createPromotionCustomerMap(List<PromotionCustomerMap> lstPromotionCustomerMap, LogInfoVO logInfo) throws DataAccessException {
		if (lstPromotionCustomerMap == null) {
			throw new IllegalArgumentException("lstPromotionCustomerMap");
		}
		List<PromotionCustomerMap> lstRs = new ArrayList<PromotionCustomerMap>();
		
		for (PromotionCustomerMap map: lstPromotionCustomerMap) {
			try {
//				if (map.getPromotionShopMap() != null) 
//					if (!checkIfRecordExist(map.getPromotionShopMap().getPromotionProgram().getId(), map.getCustomer().getCustomerCode(), null))
				lstRs.add(map);
			}
			catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		for (PromotionCustomerMap pcm: lstPromotionCustomerMap) {
			this.createPromotionCustomerMap(pcm, logInfo);
		}
	}
	
	@Override
	public PromotionCustomerMap checkPromotionCustomerMap(Long promotionProgramId,
			String shopCode, String shortCode, String channelTypeCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select pcm.* from promotion_shop_map psm join promotion_customer_map pcm on psm.promotion_shop_map_id = pcm.promotion_shop_map_id where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" and pcm.status = ? and psm.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
			
		if (promotionProgramId != null) {
			sql.append(" and psm.promotion_program_id=?");
			params.add(promotionProgramId);
		}
		else 
			sql.append(" and psm.promotion_program_id is null");
		
		if (!StringUtility.isNullOrEmpty(channelTypeCode)) {
			sql.append(" and exists (select 1 from channel_type ct where ct.channel_type_id = pcm.customer_type_id and ct.channel_type_code like ? ESCAPE '/' AND pcm.TYPE = ?)");//error http://192.168.1.92:8000/mantis/view.php?id=2734 PhuT
			params.add(StringUtility.toOracleSearchLikeSuffix(channelTypeCode.toUpperCase()));
			params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER_TYPE.getValue());
		}
		
		//shortCode
		if (!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append(" and exists (select 1 from customer c where c.customer_id = pcm.customer_id and c.short_code like ? ESCAPE '/'  and pcm.type = ?)");
			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
			params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER.getValue());
		}
		
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and psm.shop_id in (select shop_id from shop s where status != -1 start with s.shop_code = ? connect by prior parent_shop_id = shop_id)");
			params.add(shopCode.toUpperCase());
		}
		return repo.getEntityBySQL(PromotionCustomerMap.class, sql.toString(), params);
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionShopMapVO> getListPromotionShopMapVO(PromotionShopMapFilter filter) throws DataAccessException {
		if (filter == null || filter.getProgramId() == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select (select shop_code from shop where shop_id = pm.shop_id) as shopCode,");
		sql.append(" (select short_code from customer where customer_id = pm.customer_id) as shortCode,");
		sql.append(" pm.quantity_max as quantityMaxStaff, ");
		sql.append(" pm.NUM_MAX as numMax, ");
		sql.append(" pm.AMOUNT_MAX as amountMax ");
		sql.append(" from promotion_customer_map pm ");
		sql.append(" join customer cus on cus.CUSTOMER_ID = pm.CUSTOMER_ID ");
		sql.append(" where 1=1");
		sql.append(" and pm.promotion_shop_map_id in (");
		sql.append(" select promotion_shop_map_id from promotion_shop_map");
		sql.append(" where promotion_program_id = ? and status = ?");
		params.add(filter.getProgramId());
		params.add(ActiveType.RUNNING.getValue());
		if (!StringUtility.isNullOrEmpty(filter.getShopCode()) || !StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and shop_id in (");
			sql.append("select shop_id from shop where status = ?");
			params.add(ActiveType.RUNNING.getValue());
			if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
				sql.append(" and shop_code like ? escape '/'");
				String s = filter.getShopCode().toUpperCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
				sql.append(" and lower(shop_name) like ? escape '/'");
				String s = filter.getShopName().toLowerCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			sql.append(")");
		}
		if (filter.getParentShopId() != null) {
			sql.append(" and shop_id in (");
			sql.append("select shop_id from shop where status = ? start with shop_id = ? connect by prior shop_id = parent_shop_id");
			params.add(ActiveType.RUNNING.getValue());
			params.add(filter.getParentShopId());
			sql.append(")");
		}
		sql.append(")");
		if (!StringUtility.isNullOrEmpty(filter.getCusCode())) {
			String custCode = filter.getCusCode().trim().toUpperCase();
			sql.append(" AND (upper(cus.short_code) like ? escape '/' or upper(cus.customer_code) like ? escape '/' ) ");
			params.add(StringUtility.toOracleSearchLike(custCode));
			params.add(StringUtility.toOracleSearchLike(custCode));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCusName())) {
			String custName = filter.getCusName().trim().toUpperCase();
			sql.append(" and lower(name_text) like ? ESCAPE '/' ");
			String s = custName.trim().toLowerCase();
			s = Unicode2English.codau2khongdau(s);
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(filter.getAddress())) {
			sql.append(" and lower(name_text) like ? ESCAPE '/' ");
			String s = filter.getAddress().trim().toLowerCase();
			s = Unicode2English.codau2khongdau(s);
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (filter.getQuantityMax() != null) {
			sql.append(" and pm.quantity_max = ?");
			params.add(filter.getQuantityMax());
		}
		
		if (filter.getStatus() != null) {
			sql.append(" and pm.status = ?");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and pm.status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		sql.append(" order by shopCode, shortCode");
		
		String[] fieldNames = new String[] {
				"shopCode", "shortCode", "quantityMaxStaff", "numMax", "amountMax"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL
		};
		
		List<PromotionShopMapVO> lst = repo.getListByQueryAndScalar(PromotionShopMapVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
	
	@Override
	public List<PromotionCustomerMap> getListPromotionCustomerMap(
			KPaging<PromotionCustomerMap> kPaging, Long promotionProgramId, 
			String shopCode, Long channelTypeId, String shortCode, 
			Integer quantityMax, ActiveType status, String channelTypeName, Integer quantityReceived,
			String customerName, String channelTypeCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from promotion_customer_map c left join customer s on c.customer_id = s.customer_id");
		sql.append(" 	left join channel_type ct on c.customer_type_id = ct.channel_type_id");
		sql.append(" where exists(select 1 from promotion_shop_map s where c.promotion_shop_map_id = s.promotion_shop_map_id ");
		if (promotionProgramId != null) {
			sql.append("   and s.promotion_program_id=? )");
			params.add(promotionProgramId);
		}
		else {
			sql.append("    )");
		}

		if (status != null) {
			sql.append(" and c.status=?");
			params.add(status.getValue());
		}
		else {
			sql.append(" and c.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		if (quantityMax != null) {
			sql.append(" and c.QUANTITY_MAX=?");
			params.add(quantityMax);
		}
		
		if (quantityReceived != null) {
			sql.append(" and c.QUANTITY_RECEIVED=?");
			params.add(quantityReceived);
		}
		
		//shopCode
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and exists (select 1 from shop s where s.shop_id=c.shop_id and s.shop_code like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}
		
		//channelTypeId
		if (channelTypeId != null) {
			sql.append(" and c.CUSTOMER_TYPE_ID = ? AND c.TYPE = ?");
			params.add(channelTypeId);
			params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER_TYPE.getValue());
		}
		
		if (!StringUtility.isNullOrEmpty(channelTypeName)) {
			sql.append(" and lower(ct.channel_type_name) like ? AND c.TYPE = ?");
			params.add(StringUtility.toOracleSearchLike(channelTypeName).toLowerCase());
			params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER_TYPE.getValue());
		}
		
		if (!StringUtility.isNullOrEmpty(channelTypeCode)) {
			sql.append(" and ct.channel_type_code like ? ESCAPE '/' AND c.TYPE = ?");//error http://192.168.1.92:8000/mantis/view.php?id=2734 PhuT
			params.add(StringUtility.toOracleSearchLikeSuffix(channelTypeCode.toUpperCase()));
			params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER_TYPE.getValue());
		}
		
		//shortCode
		if (!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append(" and s.short_code like ? ESCAPE '/'  and c.type = ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
			params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER.getValue());
		}
		
		//customerCode
		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append("  and lower(s.customer_name) like ? ESCAPE '/'  and c.type = ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(customerName.toLowerCase()));
			params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER.getValue());
		}
		
		sql.append(" order by ct.channel_type_code, s.customer_code, c.promotion_customer_map_id desc");
		if (kPaging == null)
			return repo.getListBySQL(PromotionCustomerMap.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(PromotionCustomerMap.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public List<PromotionCustomerMap> getListPromotionCustomerMapForUpdate(Long promotionProgramId, Long customerId, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (promotionProgramId == null) {
			throw new IllegalArgumentException("getListPromotionCustomerMapForUpdate - promotionProgramId is null");
		}
		if (customerId == null) {
			throw new IllegalArgumentException("getListPromotionCustomerMapForUpdate - customerId is null");
		}
		sql.append(" select * from promotion_customer_map pcm ");
		sql.append(" where exists(select 1 from promotion_shop_map psm where pcm.promotion_shop_map_id = psm.promotion_shop_map_id ");
		sql.append("   and psm.promotion_program_id=? )");
		params.add(promotionProgramId);
		if (status != null) {
			sql.append(" and pcm.status=?");
			params.add(status.getValue());
		}
		sql.append(" and pcm.customer_id = ?");
		params.add(customerId);
		sql.append(" for update");
		return repo.getListBySQL(PromotionCustomerMap.class, sql.toString(), params);
	}
	
//	@Override (VERSION DB CU )
//	public List<PromotionCustomerMap> getListPromotionCustomerMapForUpdate(
//			KPaging<PromotionCustomerMap> kPaging, Long promotionProgramId, 
//			String shopCode, Long channelTypeId, String shortCode, ActiveType status,
//			String customerCode) throws DataAccessException {
//		StringBuilder sql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		if (promotionProgramId != null) {
//			sql.append(" select * from promotion_customer_map c ");
//			sql.append(" where exists(select 1 from promotion_shop_map s where c.promotion_shop_map_id = s.promotion_shop_map_id ");
//			sql.append("   and s.promotion_program_id=? )");
//			params.add(promotionProgramId);
//		}
//		else {
//			sql.append(" select * from promotion_customer_map c ");
//			sql.append(" where exists(select 1 from promotion_shop_map s where c.promotion_shop_map_id = s.promotion_shop_map_id ");
//			sql.append("   and s.promotion_program_id is null )");
//		}
//
//		if (status != null) {
//			sql.append(" and status=?");
//			params.add(status.getValue());
//		}
//		
//		//shopCode
//		if (!StringUtility.isNullOrEmpty(shopCode)) {
//			sql.append(" and exists (select 1 from shop s where s.shop_id=c.shop_id and s.shop_code like ? ESCAPE '/' )");
//			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
//		}
//		
//		//channelTypeId
//		if (channelTypeId != null) {
//			sql.append(" and c.CUSTOMER_TYPE_ID = ? AND c.TYPE = ?");
//			params.add(channelTypeId);
//			params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER_TYPE.getValue());
//		}
//		
//		//customerCode
//		if (!StringUtility.isNullOrEmpty(shortCode)) {
//			sql.append(" and exists (select 1 from customer s where s.customer_id=c.customer_id and s.short_code like ? ESCAPE '/' ) and c.type = ?");
//			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
//			params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER.getValue());
//		}
//		
//		//customerCode
//		if (!StringUtility.isNullOrEmpty(customerCode)) {
//			sql.append(" and exists (select 1 from customer s where s.customer_id=c.customer_id and s.customer_code like ? ESCAPE '/' ) and c.type = ?");
//			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode.toUpperCase()));
//			params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER.getValue());
//		}
//		
//		sql.append(" for update");
//		if (kPaging == null)
//			return repo.getListBySQL(PromotionCustomerMap.class, sql.toString(), params);
//		else
//			return repo.getListBySQLPaginated(PromotionCustomerMap.class, sql.toString(), params, kPaging);
//	}
	
	@SuppressWarnings("unused")
	private void deleteRelevantPromotionCustomerMap(Long promotionProgramId, Long promotionCustomerMapId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" delete from promotion_customer_map pcm");
		sql.append(" where 1=1");
		if (promotionCustomerMapId != null) {
			sql.append(" and pcm.promotion_customer_map_id!=?");
			params.add(promotionCustomerMapId);
		}
		sql.append("     and exists (select 1 from promotion_shop_map psm where psm.promotion_program_id=?");
		sql.append("		and pcm.promotion_shop_map_id = psm.promotion_shop_map_id)");
		params.add(promotionProgramId);
		
		repo.executeSQLQuery(sql.toString(), params);
	}
	
	@Override
	public Boolean checkPromotionShopMapInCustomerMap(Long promotionShopMapId) throws DataAccessException {
		String sql = "select count(1) as count from promotion_customer_map where promotion_shop_map_id = ? and status != ?";
		List<Object> params = new ArrayList<Object>();
		params.add(promotionShopMapId);
		params.add(ActiveType.DELETED.getValue());
		int c = repo.countBySQL(sql, params);
		return c > 0 ? true : false;
	}
	
	@Override
	public void deleteRelevantCustomerMap(Long promotionShopMapId) throws DataAccessException {
		String sql = "update promotion_customer_map set status = ? where promotion_shop_map_id = ? and status != ?";
		List<Object> params = new ArrayList<Object>();
		params.add(ActiveType.DELETED.getValue());
		params.add(promotionShopMapId);
		params.add(ActiveType.DELETED.getValue());
		repo.executeSQLQuery(sql, params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.PromotionCustomerMapDAO#getListPromotionCustomerMapWithListPromotionId(java.util.List)
	 */
	@Override
	public List<PromotionCustomerMap> getListPromotionCustomerMapWithListPromotionId(
			List<Long> listPromotionProgramId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select pcm.* from promotion_customer_map pcm join promotion_shop_map psm on pcm.promotion_shop_map_id = psm.promotion_shop_map_id join shop s on psm.shop_id = s.shop_id");
		sql.append(" join promotion_program pp on psm.promotion_program_id = pp.promotion_program_id left join channel_type ct on pcm.customer_type_id = ct.channel_type_id");
		sql.append(" left join customer c on pcm.customer_id = c.customer_id");
		sql.append("  where 1 = 1 ");
		sql.append(" and pcm.status <> ?");
		params.add(ActiveType.DELETED.getValue());
		if (listPromotionProgramId != null) {
			sql.append(" and psm.promotion_program_id in (");
			for (int i = 0, size = listPromotionProgramId.size(); i < size; i++) {
				sql.append("?");
				if (i<size - 1) {
					sql.append(", ");
				}
				params.add(listPromotionProgramId.get(i));
			}
			sql.append(")");
		}
		sql.append(" order by pp.from_date desc, s.shop_code");
		
		return repo.getListBySQL(PromotionCustomerMap.class, sql.toString(), params);
	}


	@Override
	public List<PromotionCustomerMap> getListPromotionCustomerMapWithShopAndPromotionProgram( Long shopId,
			Long promotionProgramId, Boolean exceptShopCheck) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from promotion_customer_map where promotion_shop_map_id in (");
		sql.append(" select promotion_shop_map_id from promotion_shop_map where status = 1 and shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
		params.add(shopId);
		sql.append(" and promotion_program_id = ?)");
		params.add(promotionProgramId);
		if(Boolean.TRUE.equals(exceptShopCheck)) {
			sql.append(" and shop_id <> ?");
			params.add(shopId);
		}
		return repo.getListBySQL(PromotionCustomerMap.class, sql.toString(), params);
	}
	
	@Override
	public List<PromotionCustomerMap> getListPromotionCustomerMapByShopAndPP(KPaging<PromotionCustomerMap> paging,
			Long shopId, Long promotionProgramId, String customerCode, String customerName, String address) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select pcm.* from promotion_customer_map pcm join customer c on pcm.customer_id = c.customer_id ");
		sql.append(" where promotion_shop_map_id = (select promotion_shop_map_id from promotion_shop_map where shop_id = ? ");
		params.add(shopId);
		sql.append("	and promotion_program_id = ? and status = 1) and pcm.status = 1");
		params.add(promotionProgramId);
		
		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append(" and short_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(customerCode.toUpperCase()));
		}
		
		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append(" and upper(customer_name) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(customerName.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(address)) {
			sql.append(" and upper(address) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(address.toUpperCase()));
		}
		sql.append(" order by c.customer_code");
		if (paging == null)
			return repo.getListBySQL(PromotionCustomerMap.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(PromotionCustomerMap.class, sql.toString(), params, paging);
	}
	
	@Override
	public Boolean checkExistCustomerMap(Long id)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(*) as count from (");
		sql.append(" select pcm.* from promotion_customer_map pcm,promotion_shop_map psm ");
		sql.append(" where psm.promotion_shop_map_id = pcm.promotion_shop_map_id ");
		sql.append(" and psm.promotion_shop_map_id = ? )");
		params.add(id);
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}

	@Override
	public Integer getSumQuantityMaxOfShopMap(long shopMapId, List<Long> lstexceptCusMapId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select sum(quantity_max) from promotion_customer_map");
		sql.append(" where promotion_shop_map_id = ?  and status = ?");
		params.add(shopMapId);
		params.add(ActiveType.RUNNING.getValue());
		if (lstexceptCusMapId != null) {
			sql.append(" and promotion_customer_map_id not in (-1");
			for (Long id : lstexceptCusMapId) {
				sql.append(",?");
				params.add(id);
			}
			sql.append(")");
		}
		Object r = repo.getObjectByQuery(sql.toString(), params);
		if (r != null) {
			if (r instanceof BigDecimal) {
				return ((BigDecimal) r).intValue();
			}
			return Integer.valueOf(r.toString());
		}
		return Integer.valueOf(0);
	}

	@Override
	public List<PromotionCustomerVO> getCustomerInPromotionProgram(PromotionCustomerFilter filter) throws DataAccessException {
		String custCode = filter.getCode();
		String custName = filter.getName();
		String address = filter.getAddress();
		if (custCode != null) {
			custCode = custCode.trim();
		}
		if (custName != null) {
			custName = custName.trim();
		}
		if (address != null) {
			address = address.trim();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("WITH lstShopMap AS( ");
		sql.append("SELECT psm.promotion_shop_map_id FROM promotion_shop_map psm ");
		sql.append("WHERE promotion_program_id = ?  ");
		params.add(filter.getPromotionId());
		if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "shop_id");
			sql.append(paramsFix);
		}
		if (filter.getShopId() != null) {
			sql.append(" and shop_id = ? ");
			params.add(filter.getShopId());
		}
		sql.append(" AND status = 1)");

		if (Boolean.TRUE.equals(filter.getIsCustomerOnly())) {
			sql.append(" SELECT promotion_customer_map_id AS mapId ");
			sql.append(" , c.customer_id  AS id ");
			sql.append(" , psm.shop_id AS parentId ");
			sql.append(" , c.short_code AS customerCode ");
			sql.append(" , c.customer_name AS customerName ");
			sql.append(" , c.address ");
			sql.append(" , quantity_max AS quantity ");
//			sql.append(" , quantity_received AS receivedQtt ");
			sql.append(" , QUANTITY_RECEIVED_TOTAL AS receivedQtt ");
			sql.append(" , amount_max as amountMax ");
//			sql.append(" , AMOUNT_RECEIVED as receivedAmt ");
			sql.append(" , AMOUNT_RECEIVED_TOTAL as receivedAmt ");
			sql.append(" , num_max as numMax ");
//			sql.append(" , NUM_RECEIVED as receivedNum ");
			sql.append(" , NUM_RECEIVED_TOTAL as receivedNum ");
			sql.append(" , 1 as isCustomer");
			sql.append(" FROM promotion_customer_map psm ");
			sql.append(" join customer c on psm.customer_id = c.customer_id WHERE promotion_shop_map_id IN (SELECT promotion_shop_map_id FROM lstShopMap)");
			if (!StringUtility.isNullOrEmpty(custCode)) {
				custCode = custCode.toUpperCase();
				sql.append(" AND (upper(c.short_code) like ? escape '/' or upper(c.customer_code)  like ? escape '/' )");
				params.add(StringUtility.toOracleSearchLike(custCode));
				params.add(StringUtility.toOracleSearchLike(custCode));
			}
			if (!StringUtility.isNullOrEmpty(custName)) {
				custName = custName.toUpperCase();
				sql.append(" and lower(c.name_text) like ? ESCAPE '/' ");
				String s = custName.trim().toLowerCase();
				s = Unicode2English.codau2khongdau(s);
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (!StringUtility.isNullOrEmpty(address)) {
				sql.append(" and lower(c.name_text) like ? ESCAPE '/' ");
				String s = address.trim().toLowerCase();
				s = Unicode2English.codau2khongdau(s);
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			sql.append(" AND psm.status = 1");
		} else {
//			sql.append(", lstCustomerMap AS (SELECT promotion_customer_map_id AS mapId, c.customer_id  AS id, psm.shop_id AS parentId, c.short_code AS code, c.customer_name AS name, quantity_max AS quantity, quantity_received AS receivedQtt,amount_max      AS amountMax,amount_received AS receivedAmt,num_max      AS numMax,num_received AS receivedNum ");
			sql.append(", lstCustomerMap AS (SELECT promotion_customer_map_id AS mapId, c.customer_id  AS id, psm.shop_id AS parentId, c.short_code AS code, c.customer_name AS name, quantity_max AS quantity, QUANTITY_RECEIVED_TOTAL AS receivedQtt,amount_max      AS amountMax,AMOUNT_RECEIVED_TOTAL AS receivedAmt,num_max      AS numMax,NUM_RECEIVED_TOTAL AS receivedNum ");
			sql.append(" , c.address ");
			sql.append("FROM promotion_customer_map psm inner join customer c on psm.customer_id = c.customer_id WHERE promotion_shop_map_id IN (SELECT promotion_shop_map_id FROM lstShopMap) ");
			if (!StringUtility.isNullOrEmpty(custCode)) {
				custCode = custCode.toUpperCase();
				sql.append(" AND (upper(c.short_code) like ? escape '/' or upper(c.customer_code) like ? escape '/' ) ");
				params.add(StringUtility.toOracleSearchLike(custCode));
				params.add(StringUtility.toOracleSearchLike(custCode));
			}
			if (!StringUtility.isNullOrEmpty(custName)) {
				custName = custName.toUpperCase();
				sql.append(" and lower(c.name_text) like ? ESCAPE '/' ");
				String s = custName.trim().toLowerCase();
				s = Unicode2English.codau2khongdau(s);
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (!StringUtility.isNullOrEmpty(address)) {
				sql.append(" and lower(c.name_text) like ? ESCAPE '/' ");
				String s = address.trim().toLowerCase();
				s = Unicode2English.codau2khongdau(s);
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			sql.append("AND psm.status = 1) ");
			sql.append(" , ");
			sql.append("lstDV AS (SELECT DISTINCT parent_shop_id AS parentId, shop_id AS id, shop_code AS code, shop_name AS name, ");
			sql.append("(SELECT SUM(quantity_max) FROM promotion_customer_map WHERE promotion_shop_map_id IN (SELECT promotion_shop_map_id FROM lstShopMap) ");
			sql.append("AND shop_id IN (SELECT shop_id FROM shop START WITH shop_id = s.shop_id CONNECT BY prior shop_id = parent_shop_id) ");
			sql.append("AND status = 1) AS quantity, ");
//			sql.append("(SELECT SUM(quantity_received) FROM promotion_customer_map WHERE promotion_shop_map_id IN (SELECT promotion_shop_map_id FROM lstShopMap) ");
			sql.append("(SELECT SUM(QUANTITY_RECEIVED_TOTAL) FROM promotion_customer_map WHERE promotion_shop_map_id IN (SELECT promotion_shop_map_id FROM lstShopMap) ");
			sql.append("AND shop_id IN (SELECT shop_id FROM shop START WITH shop_id = s.shop_id CONNECT BY prior shop_id = parent_shop_id)AND status = 1) AS receivedQtt, ");
			
			sql.append("(SELECT SUM(amount_max) FROM promotion_customer_map WHERE promotion_shop_map_id IN (SELECT promotion_shop_map_id FROM lstShopMap) ");
			sql.append("AND shop_id IN (SELECT shop_id FROM shop START WITH shop_id = s.shop_id CONNECT BY prior shop_id = parent_shop_id) ");
			sql.append("AND status = 1) AS amountMax, ");
//			sql.append("(SELECT SUM(amount_received) FROM promotion_customer_map WHERE promotion_shop_map_id IN (SELECT promotion_shop_map_id FROM lstShopMap) ");
			sql.append("(SELECT SUM(AMOUNT_RECEIVED_TOTAL) FROM promotion_customer_map WHERE promotion_shop_map_id IN (SELECT promotion_shop_map_id FROM lstShopMap) ");
			sql.append("AND shop_id IN (SELECT shop_id FROM shop START WITH shop_id = s.shop_id CONNECT BY prior shop_id = parent_shop_id)AND status = 1) AS receivedAmt, ");
			
			sql.append("(SELECT SUM(num_max) FROM promotion_customer_map WHERE promotion_shop_map_id IN (SELECT promotion_shop_map_id FROM lstShopMap) ");
			sql.append("AND shop_id IN (SELECT shop_id FROM shop START WITH shop_id = s.shop_id CONNECT BY prior shop_id = parent_shop_id) ");
			sql.append("AND status = 1) AS numMax, ");
//			sql.append("(SELECT SUM(num_received) FROM promotion_customer_map WHERE promotion_shop_map_id IN (SELECT promotion_shop_map_id FROM lstShopMap) ");
			sql.append("(SELECT SUM(NUM_RECEIVED_TOTAL) FROM promotion_customer_map WHERE promotion_shop_map_id IN (SELECT promotion_shop_map_id FROM lstShopMap) ");
			sql.append("AND shop_id IN (SELECT shop_id FROM shop START WITH shop_id = s.shop_id CONNECT BY prior shop_id = parent_shop_id)AND status = 1) AS receivedNum, ");
			
			sql.append(" level lv ");
			sql.append("FROM shop s START WITH shop_id  IN ");
			sql.append("(SELECT parentId FROM lstCustomerMap) CONNECT BY prior parent_shop_id = shop_id ");
			sql.append(") ");
			sql.append("SELECT mapId, id, parentId, code customerCode, name customerName, quantity, NVL(receivedQtt, 0) AS receivedQtt,amountMax,NVL(receivedAmt, 0) as receivedAmt,numMax,NVL(receivedNum, 0) as receivedNum, isCustomer ");
			sql.append("FROM (");
			sql.append("SELECT NULL AS mapId, id, parentId, code, name, quantity, receivedQtt,amountMax,receivedAmt,numMax,receivedNum, 0 AS isCustomer, lv FROM lstDV ");
			sql.append("UNION ALL ");
			sql.append("SELECT mapId, id, parentId, code, name, quantity, receivedQtt, amountMax,receivedAmt,numMax,receivedNum, 1 AS isCustomer, 0 AS lv FROM lstCustomerMap) ");
			sql.append(" ORDER BY isCustomer, lv DESC, code, name ");
		}

		String[] fieldNames = new String[] { 
				"mapId", //1
				"id", //2
				"parentId", //3 
				"customerCode", //4 
				"customerName", //5
				"quantity", //6
				"receivedQtt", //7
				"amountMax", //8
				"receivedAmt", //9
				"numMax", //10
				"receivedNum", //11 
				"isCustomer" //12 
				//, "address" //13
				};

		Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, //1
				StandardBasicTypes.LONG, //2
				StandardBasicTypes.LONG, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.BIG_DECIMAL, //6
				StandardBasicTypes.BIG_DECIMAL, //7
				StandardBasicTypes.BIG_DECIMAL, //8 
				StandardBasicTypes.BIG_DECIMAL, //9
				StandardBasicTypes.BIG_DECIMAL, //10
				StandardBasicTypes.BIG_DECIMAL, //11
				StandardBasicTypes.INTEGER //12
				//, StandardBasicTypes.STRING //13
		};
		if (filter.getkPaging() == null) {
			List<PromotionCustomerVO> lst = repo.getListByQueryAndScalar(PromotionCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
			return lst;
		}

		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count");
		countSql.append(" from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");

		if (Boolean.TRUE.equals(filter.getIsCustomerOnly())) {
			sql.append(" order by customerCode");
		} else {
			sql.append("ORDER BY isCustomer, customerCode, customerName");
		}

		List<PromotionCustomerVO> lst = repo.getListByQueryAndScalarPaginated(PromotionCustomerVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		return lst;
	}
	
	/**
	 * @author trietptm
	 */
	@Override
	public List<PromotionCustomerVO> searchCustomerForPromotionProgram(PromotionCustomerFilter filter) throws DataAccessException {
		StringBuilder withSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		withSql.append("with lstCust as (");
		withSql.append(" select customer_id from promotion_customer_map pcm");
		withSql.append(" join promotion_shop_map psm on (psm.promotion_shop_map_id = pcm.promotion_shop_map_id");
		withSql.append(" and psm.status = ? and psm.promotion_program_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(filter.getPromotionId());
		if (filter.getShopId() != null) {
			withSql.append(" and psm.shop_id = ?");
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "psm.shop_id");
			withSql.append(paramsFix);
		}
		withSql.append(")");
		withSql.append(" where pcm.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		withSql.append(" )");

		sql.append(withSql.toString());
		sql.append(" select customer_id as customerId, short_code as customerCode,");
		sql.append(" customer_name as customerName, address");

		fromSql.append(" from customer");
		fromSql.append(" where 1=1");
		if (filter.getShopId() != null) {
			fromSql.append(" and shop_id = ?");
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "shop_id");
			fromSql.append(paramsFix);
		}
		fromSql.append(" and shop_id in (select SHOP_ID from PROMOTION_SHOP_MAP where promotion_program_id = ? ) ");
		params.add(filter.getPromotionId());
		fromSql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" and customer_id not in (select * from lstCust)");

		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			fromSql.append(" and lower(short_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			fromSql.append(" and lower(name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getName().trim()).toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getAddress())) {
			fromSql.append(" and lower(address) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getAddress().trim().toLowerCase()));
		}
		sql.append(fromSql.toString());
		sql.append(" order by customerCode, customerName");

		String[] fieldNames = new String[] { "customerId", "customerCode", "customerName", "address", };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };

		if (filter.getkPaging() == null) {
			List<PromotionCustomerVO> lst = repo.getListByQueryAndScalar(PromotionCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
			return lst;
		}

		StringBuilder countSql = new StringBuilder();
		countSql.append(withSql.toString());
		countSql.append("select count(1) as count");
		countSql.append(fromSql.toString());

		List<PromotionCustomerVO> lst = repo.getListByQueryAndScalarPaginated(PromotionCustomerVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionCustomerVO> getListCustomerInPromotion(long promotionId, List<Long> lstCustId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select pcm.customer_id as customerId,");
		sql.append(" (select short_code from customer where customer_id = pcm.customer_id) as customerCode,");
		sql.append(" (select customer_name from customer where customer_id = pcm.customer_id) as customerName,");
		sql.append(" (select address from customer where customer_id = pcm.customer_id) as address");
		sql.append(" from promotion_customer_map pcm");
		sql.append(" join promotion_shop_map psm on (psm.promotion_shop_map_id = pcm.promotion_shop_map_id");
		sql.append(" and psm.status = ? and psm.promotion_program_id = ?)");
		params.add(ActiveType.RUNNING.getValue());
		params.add(promotionId);
		sql.append(" where pcm.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		
		if (lstCustId != null && lstCustId.size() > 0) {
			sql.append(" and pcm.customer_id in (-1");
			for (int i = 0, sz = lstCustId.size(); i < sz; i++) {
				sql.append(",?");
				params.add(lstCustId.get(i));
			}
			sql.append(")");
		}
		
		String[] fieldNames = new String[] {
				"customerId",
				"customerCode",
				"customerName",
				"address",
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING
		};
		
		List<PromotionCustomerVO> lst = repo.getListByQueryAndScalar(PromotionCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	@Override
	public List<PromotionCustomerMap> getListPromotionCustomerMapEntity(Long customerID, PromotionShopMap promotionShopMap) throws DataAccessException {
		// TODO Auto-generated method stub
		List<Object> params = new ArrayList<Object>();
		
		StringBuilder  varname1 = new StringBuilder();
		varname1.append("SELECT * ");
		varname1.append("FROM   promotion_customer_map ");
		varname1.append("WHERE  1 = 1 ");
		varname1.append("and status = 1 ");
		/*if(listCustomerID != null && listCustomerID.size() > 0) {
			varname1.append("       AND customer_id IN (-1 ");
			for (Long id : listCustomerID) {
				varname1.append(", ?");
			}
			varname1.append(")");
		}
		if(listPromotionShopMap != null && listPromotionShopMap.size() > 0) {
			varname1.append("       AND promotion_shop_map_id IN (-1 ");
			for (PromotionShopMap promotionShopMap : listPromotionShopMap) {
				varname1.append(", ?");
				params.add(promotionShopMap.getId());
			}
			varname1.append(") ");
		}*/
		if(customerID != null) {
			varname1.append("       AND customer_id = ? ");
			params.add(customerID);
		}
		
		if(promotionShopMap != null) {
			varname1.append("       AND promotion_shop_map_id = ? ");
			params.add(promotionShopMap.getId());
		}

		return repo.getListBySQL(PromotionCustomerMap.class, varname1.toString(), params);
	}
}
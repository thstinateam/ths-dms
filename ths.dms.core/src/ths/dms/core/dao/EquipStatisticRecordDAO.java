package ths.dms.core.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import ths.dms.core.entities.EquipStatisticCustomer;
import ths.dms.core.entities.EquipStatisticGroup;
import ths.dms.core.entities.EquipStatisticRecord;
import ths.dms.core.entities.EquipStatisticStaff;
import ths.dms.core.entities.EquipStatisticUnit;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.EquipObjectType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipStatisticFilter;
import ths.dms.core.entities.vo.EquipRecordShopVO;
import ths.dms.core.entities.vo.EquipStatisticCustomerVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.StatisticCheckingVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface EquipStatisticRecordDAO {

	EquipStatisticRecord getEquipStatisticRecordById(Long id) throws DataAccessException;
	EquipStatisticRecord getEquipStatisticRecordByCode(String code) throws DataAccessException;

	EquipStatisticRecord createEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord) throws DataAccessException;

	void updateEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord) throws DataAccessException;

	List<EquipRecordShopVO> searchShopForRecord(Long recordId, Long pShopId, String shopCode, String shopName, Integer isLevel) throws DataAccessException;

	List<EquipRecordShopVO> getListShopInRecord(long recordId, List<Long> lstShopId, boolean shopMapOnly) throws DataAccessException;

	EquipStatisticUnit createEquipStatisticUnit(EquipStatisticUnit equipStatisticUnit, LogInfoVO logInfo) throws DataAccessException;

	void deleteEquipStatisticUnit(EquipStatisticUnit equipStatisticUnit) throws DataAccessException;

	List<EquipStatisticGroup> createListEquipStatisticGroup(List<EquipStatisticGroup> list) throws DataAccessException;

	void deleteEquipStatisticGroup(EquipStatisticGroup equipStatisticGroup)
			throws DataAccessException;

	List<StatisticCheckingVO> getListEquipmentStatisticCheckingVOForExport(
			Long idRecord)throws DataAccessException;

	/**
	 * get equip_statistic_group by id
	 * @author phut
	 */
	EquipStatisticGroup getEquipStatisticGroupById(Long recordId, Long id,
			EquipObjectType type) throws DataAccessException;

	/**
	 * check shop exists in record
	 * @author phut
	 */
	Integer checkShopInRecordUnit(Long recordId, Long shopId)
			throws DataAccessException;

	/**
	 * Ham tim kiem danh sach khach hang tham gia kiem ke
	 * 
	 * @author phuongvm
	 * @param kPaging
	 * @param statisId
	 * @param shortCode
	 * @param customerName
	 * @param status
	 * @param statusCus
	 * @return Danh sach khach hang tham gia kiem ke
	 * @throws DataAccessException
	 * @since 17/12/2015
	 */
	List<EquipStatisticCustomerVO> searchCustomer4Record(
			KPaging<EquipStatisticCustomerVO> kPaging, Long statisId,
			String shortCode, String customerName, Integer status, Integer statusCus) throws DataAccessException;

	/**
	 * get list customer join in record
	 * @author phut
	 */
	Map<Long, EquipStatisticCustomer> getListExistsCustomer(
			List<EquipStatisticCustomer> list) throws DataAccessException;

	/**
	 * insert list equip_statistic_customer
	 * @author phut
	 */
	void insertListEquipStatisticCustomer(List<EquipStatisticCustomer> list)
			throws DataAccessException;

	/**
	 * get list Equip_statistic_unit
	 * @author phut
	 */
	List<EquipStatisticUnit> getListEquipStatisticUnitChildShopMapWithShopAndRecord(
			Long shopId, Long recordId, Boolean isGetChild)
			throws DataAccessException;
	/**
	 * Kiem tra xem thiet bi co thuoc nhom thiet bi va don vi cua kiem ke ko (true : thuoc, false: ko thuoc)
	 * 
	 * @author phuongvm
	 * @param recordId
	 * @param equipId
	 * @since 25/04/2015
	 * */
	Boolean checkEquipInUnitAndGroup(Long recordId, Long equipId) throws DataAccessException;
	/**
	 * Lay don vi cua thiet bi o kho don vi
	 * 
	 * @author phuongvm
	 * @param stockId
	 * @since 25/04/2015
	 * */
	Shop getShopInEquip(Long stockId) throws DataAccessException;
	/**
	 * check don vi co thuoc danh sach don vi kiem ke
	 * @author phuongvm
	 * @param recordId
	 * @param shopId
	 * @since 27/04/2015
	 */
	Boolean checkExistInEquipStatisticUnit(Long recordId, Long shopId) throws DataAccessException;
	/**
	 * check u ke/nhom thiet bi co thuoc danh sach nhom kiem ke
	 * @author phuongvm
	 * @param recordId
	 * @param objectId
	 * @since 27/04/2015
	 */
	Boolean checkExistInEquipStatisticGroup(Long recordId, Long objectId) throws DataAccessException;
	/**
	 * check khach hang co thuoc kiem ke
	 * @author phuongvm
	 * @param recordId
	 * @param customerId
	 * @since 20/05/2015
	 */
	Boolean checkCustomerInEquipStatisCus(Long recordId, Long customerId) throws DataAccessException;
	/**
	 * Lay danh sach khach hang co status = 0
	 * 
	 * @author phuongvm
	 * @param recordId
	 * @since 20/05/2015
	 * */
	List<EquipStatisticCustomer> getListEquipStatisticCustomer(Long recordId) throws DataAccessException;

	/**
	 * Lay danh sach xuat excel don vi tham gia bien ban kiem ke
	 * 
	 * @author hoanv25
	 * @since May 29/2015
	 * @param recordId
	 *            , lstShopObjectType
	 * @throws DataAccessException
	 */
	List<EquipRecordShopVO> getListShopOfRecordExport(Long recordId, List<ShopObjectType> lstShopObjectType, String shopCode, String shopName)throws DataAccessException;
	
	/**
	 * Lay danh sach don vi kiem ke con 
	 * 
	 * @author phuongvm
	 * @since 02/07/2015
	 * @param recordId
	 * @param shopId
	 * @throws DataAccessException
	 */
	List<EquipStatisticUnit> getListStatisticUnitChild(Long recordId,
			Long shopId) throws DataAccessException;
	/**
	 * Lay danh sách shop con cháu loại trừ 
	 * 
	 * @author phuongvm
	 * @since 02/07/2015
	 * @param shopId
	 * @param lstShopIdExcept
	 * @throws DataAccessException
	 */
	List<Shop> getListShopForEquipStatisticUnit(Long shopId,
			List<Long> lstShopIdExcept) throws DataAccessException;
	/**
	 * Kiem tra xem thiet bi co thuoc don vi cua kiem ke ko (true : thuoc, false: ko thuoc)
	 * 
	 * @author phuongvm
	 * @param recordId
	 * @param equipId
	 * @since 15/07/2015
	 * */
	Boolean checkEquipInUnit(Long recordId, Long equipId)
			throws DataAccessException;
	/**
	 * Kiem tra xem thiet bi ,nvbh co ton tai
	 * 
	 * @author phuongvm
	 * @param recordId
	 * @param staffId
	 * @param equipId
	 * @since 20/07/2015
	 * */
	Boolean checkStaffInEquipStatisStaff(Long recordId, Long staffId,
			Long equipId) throws DataAccessException;
	/**
	 * tim kiem khach hang cho nhom thiet bi cua kiem ke trang thai du thao 
	 * 
	 * @author phuongvm
	 * @param kPaging
	 * @param statisId
	 * @param shortCode
	 * @param customerName
	 * @param status
	 * @since 21/07/2015
	 * */
	List<EquipStatisticCustomerVO> searchCustomer4RecordEquipGroup(
			KPaging<EquipStatisticCustomerVO> kPaging, Long statisId,
			String shortCode, String customerName, Integer status)
			throws DataAccessException;
	/**
	 * Lay danh sach equipstatisticStaff khong thoa khi kiem ke thiet bi chuyen sang hoat dong 
	 * 
	 * @author phuongvm
	 * @param recordId
	 * @since 22/07/2015
	 * */
	List<EquipStatisticStaff> getListEquipStatisticStaffNotBelong(Long recordId)
			throws DataAccessException;
	
	/**
	 * Lay danh sach kiem ke giao voi tu ngay den ngay truyen vao
	 * @author phuongvm
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @return List<EquipmentVO> 
	 * @throws BusinessException
	 * @since 17/09/2015
	 */
	List<EquipmentVO> getListChecking(Date fromDate, Date toDate, Long shopId) throws DataAccessException;
	
	/**
	 * Xoa du lieu bang EquipStatisticStaff dua vao bien ban kiem ke, nhan vien, thiet bi
	 * 
	 * @author phuongvm
	 * @param recordId
	 * @param staffId
	 * @param equipId
	 * @throws DataAccessException
	 * @since 03/12/2015
	 */
	void deleteEquipStatisticStaff(Long recordId, Long staffId, Long equipId) throws DataAccessException;
	
	/**
	 * lay danh sach thiet bi kem nhan vien duoc chi dinh de xuat excel
	 * 
	 * @author phuongvm
	 * @param filter
	 * @return Danh sach thiet bi, nhan vien kiem ke
	 * @throws DataAccessException
	 * @since 03/12/2015
	 */
	List<EquipmentVO> getListEquipStaffByStatisticFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException;
	
	/**
	 * Lay nhan vien chi dinh thiet bi trong bien ban kiem ke
	 * 
	 * @author phuongvm
	 * @param recordId
	 * @param staffId
	 * @param equipId
	 * @return EquipStatisticStaff
	 * @throws DataAccessException
	 * @since 04/12/2015
	 */
	EquipStatisticStaff getEquipStatisticStaff(Long recordId, Long staffId, Long equipId) throws DataAccessException;
	
	/**
	 * Kiem tra nhan vien co quan ly shop 
	 * 
	 * @author phuongvm
	 * @param staffId
	 * @param shopId
	 * @return true: co quan ly, false: khong quan ly shop
	 * @throws DataAccessException
	 * @since 09/12/2015
	 */
	Boolean isStaffBelongShop(long staffId, long shopId) throws DataAccessException;
	
	/**
	 * Xu ly lay ds EquipStatisticRecord
	 * @author trietptm
	 * @param filter
	 * @return List<EquipStatisticRecord>
	 * @throws DataAccessException
	 * @since Mar 4, 2016
	*/
	List<EquipStatisticRecord> getListEquipStatisticRecord(EquipRecordFilter<EquipStatisticRecord> filter) throws DataAccessException;
	
	/**
	 * Lay danh sach shop kiem ke
	 * @author trietptm
	 * @param recordId
	 * @param lstShopSpecificType
	 * @param shopCode
	 * @param shopName
	 * @return List<EquipRecordShopVO> 
	 * @throws DataAccessException
	 * @since Mar 7, 2016
	*/
	List<EquipRecordShopVO> getListShopOfRecord(Long recordId, List<ShopSpecificType> lstShopSpecificType, String shopCode, String shopName) throws DataAccessException;

	/**
	 * xoa EquipStatisticUnit chua shop cha
	 * @author trietptm
	 * @param shopId
	 * @param recordId
	 * @return
	 * @throws DataAccessException
	 * @since Mar 9, 2016
	*/
	List<EquipStatisticUnit> getListEquipStatisticUnitParentShop(List<Long> shopId, long recordId) throws DataAccessException;
	
	/**
	 * Xoa danh sach EquipStatisticGroup
	 * @author trietptm
	 * @param lstEquipStatisticGroup
	 * @throws DataAccessException
	 * @since Mar 22, 2016
	*/
	void deleteEquipStatisticGroup(List<EquipStatisticGroup> lstEquipStatisticGroup) throws DataAccessException;
	
	/**
	 * Lay danh sach EquipStatisticGroup
	 * @author trietptm
	 * @param filter
	 * @return List<EquipStatisticGroup>
	 * @throws DataAccessException
	 * @since Mar 22, 2016
	*/
	List<EquipStatisticGroup> getListEquipStatisticGroup(EquipStatisticFilter filter) throws DataAccessException;
	
	/**
	 * Lay danh sach EquipStatisticGroup theo shop
	 * @author trietptm
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @since Mar 23, 2016
	*/
	List<EquipStatisticGroup> getListEquipStatisticGroupByShop(EquipStatisticFilter filter) throws DataAccessException;
}

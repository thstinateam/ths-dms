package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.StDisplayPdGroupDtl;
import ths.dms.core.entities.StDisplayPlAmtDtl;
import ths.dms.core.entities.StDisplayPlDpDtl;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StDisplayPdGroupFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.StDisplayPDGroupDTLVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface StDisplayPdGroupDtlDAO {
	StDisplayPlAmtDtl getStDisplayPlAmtDtlByAmtGr(Long levelId,Long grId) throws DataAccessException;
	StDisplayPlDpDtl getStDisplayPlPrDtlByGr(Long grId,Long productId) throws DataAccessException;
	
	StDisplayPlAmtDtl createStDisplayPlAmtDtlByAmtGr(StDisplayPlAmtDtl displayPlAmtDtl) throws DataAccessException;
	void updateStDisplayPlAmtDtlByAmtGr(StDisplayPlAmtDtl displayPlAmtDtl) throws DataAccessException;
	StDisplayPlDpDtl createStDisplayPlPrDtlByGr(StDisplayPlDpDtl displayPlAmtDtl) throws DataAccessException;
	void updateDisplayPlPrDtlByGr(StDisplayPlDpDtl displayPlAmtDtl) throws DataAccessException;
	List<StDisplayPDGroupDTLVO> getDisplayPDGroupDTLVOs(Long levelId,Long st_display_pd_group_id) throws DataAccessException;
	
	List<StDisplayPdGroupDtl> getListDisplayProductGroupDTL
				(KPaging<StDisplayPdGroupDtl> kPaging, Long displayProductGroupDTLId, ActiveType status) throws DataAccessException;
	void updateDisplayProductGroupDTL(StDisplayPdGroupDtl displayProductGroupDTL, LogInfoVO logInfo) throws DataAccessException;
	StDisplayPdGroupDtl getDisplayProductGroupDTLById(long id) throws DataAccessException;
	StDisplayPdGroupDtl createDisplayProductGroupDTL(StDisplayPdGroupDtl displayProductGroupDTL, LogInfoVO logInfo) throws DataAccessException;
	StDisplayPdGroupDtl getStDisplayPdGroupDtlbyProductId(Long productId) throws DataAccessException;
	StDisplayPdGroupDtl getStDisplayPdGroupDtlByProductId(Long stDisplayPdGroupDtlId, Long productId) throws DataAccessException;
	
	/**
	 * @author thachnn
	 */
	StDisplayPdGroupDtl createStDisplayPdGroupDtl(StDisplayPdGroupDtl stDisplayPdGroupDtl, LogInfoVO logInfo) throws DataAccessException;
	StDisplayPdGroupDtl getDisplayProductGroupDTLByGroupId(Long displayProductGroupId,
			Long productId) throws DataAccessException;
}

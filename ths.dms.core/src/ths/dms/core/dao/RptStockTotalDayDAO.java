package ths.dms.core.dao;

import ths.dms.core.entities.RptStockTotalDay;
import ths.dms.core.exceptions.DataAccessException;

public interface RptStockTotalDayDAO {

	RptStockTotalDay createRptStockTotalDay(RptStockTotalDay rptStockTotalDay) throws DataAccessException;

	void deleteRptStockTotalDay(RptStockTotalDay rptStockTotalDay) throws DataAccessException;

	void updateRptStockTotalDay(RptStockTotalDay rptStockTotalDay) throws DataAccessException;
	
	RptStockTotalDay getRptStockTotalDayById(long id) throws DataAccessException;
}

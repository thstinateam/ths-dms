package ths.dms.core.dao.repo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.type.Type;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

public interface SelectRepository {
	<T> T getEntityById(Class<T> clazz, Serializable id)
			throws DataAccessException;

	<T> T getEntityByHQL(String hql, List<Object> params)
			throws DataAccessException;

	<T> T getEntityBySQL(Class<T> clazz, String sql, List<Object> params)
			throws DataAccessException;

	<T> T getEntityBySQL(Class<T> clazz, String sql, List<Object> params,
			List<Class<?>> synchronizedClass) throws DataAccessException;

	<T> T getFirstBySQL(Class<T> clazz, String sql, List<Object> params)
			throws DataAccessException;

	<T> T getFirstBySQL(Class<T> clazz, String sql, List<Object> params,
			List<Class<?>> synchronizedClass) throws DataAccessException;

	<T> List<T> getListByHQL(String hql, List<Object> params)
			throws DataAccessException;

	<T> List<T> getListByHQLPaginated(String selectHql, String countHql,
			List<Object> selectParams, List<Object> countParams,
			KPaging<T> paging) throws DataAccessException;

	<T> List<T> getListByHQLPaginated(String hql, List<Object> params,
			KPaging<T> paging) throws DataAccessException;

	<T> List<T> getListBySQL(Class<T> clazz, String sql, List<Object> params)
			throws DataAccessException;

	<T> List<T> getListBySQL(Class<T> clazz, String sql, List<Object> params,
			List<Class<?>> synchronizedClass) throws DataAccessException;

	<T> List<T> getListBySQLPaginated(Class<T> clazz, String selectSql,
			String countSql, List<Object> selectParams,
			List<Object> countParams, KPaging<T> paging)
			throws DataAccessException;

	<T> List<T> getListBySQLPaginated(Class<T> clazz, String selectSql,
			String countSql, List<Object> selectParams,
			List<Object> countParams, KPaging<T> paging,
			List<Class<?>> synchronizedClass) throws DataAccessException;

	<T> List<T> getListBySQLPaginated(Class<T> clazz, String sql,
			List<Object> params, KPaging<T> paging) throws DataAccessException;

	<T> List<T> getListBySQLPaginated(Class<T> clazz, String sql,
			List<Object> params, KPaging<T> paging,
			List<Class<?>> synchronizedClass) throws DataAccessException;

	<T> List<T> getListByQueryAndScalar(Class<T> clazz, String[] fieldNames,
			Type[] fieldTypes, String sql, List<Object> params)
			throws DataAccessException;

	<T> List<T> getListByQueryAndScalar(Class<T> clazz, String[] fieldNames,
			Type[] fieldTypes, String sql, List<Object> params,
			List<Class<?>> synchronizedClass) throws DataAccessException;

	// <T> List<T> getListByQueryAndScalar(Class<T> clazz, String[] fieldNames,
	// Type[] fieldTypes, String sql, List<Object> params,
	// CacheConfig cacheConfig);

	<T> List<T> getListByQueryAndScalarPaginated(Class<T> clazz,
			String[] fieldNames, Type[] fieldTypes, String selectSql,
			String countSql, List<Object> selectParams,
			List<Object> countParams, KPaging<T> paging)
			throws DataAccessException;

	<T> List<T> getListByQueryAndScalarPaginated(Class<T> clazz,
			String[] fieldNames, Type[] fieldTypes, String selectSql,
			String countSql, List<Object> selectParams,
			List<Object> countParams, KPaging<T> paging,
			List<Class<?>> synchronizedClass) throws DataAccessException;

	// <T> List<T> getListByQueryAndScalarPaginated(Class<T> clazz,
	// String[] fieldNames, Type[] fieldTypes, String selectSql,
	// String countSql, List<Object> selectParams,
	// List<Object> countParams, KPaging<T> paging, CacheConfig cacheConfig);

	int countByHQL(String countHql, List<Object> countParams)
			throws DataAccessException;

	int countBySQL(String countSql, List<Object> countParams)
			throws DataAccessException;

	int countBySQL(String countSql, List<Object> countParams,
			List<Class<?>> synchronizedClass) throws DataAccessException;

	BigDecimal countBySQLReturnBigDecimal(String countSql, List<Object> countParams) throws DataAccessException;
	
	<T> List<T> getListByNamedQuery(String namedQuerySQL, List<Object> params)
			throws DataAccessException;

	Object getObjectByQuery(String sql, List<Object> params)
			throws DataAccessException;

	Object getObjectByQuery(String sql, List<Object> params,
			List<Class<?>> synchronizedClass) throws DataAccessException;

	SessionFactory getSessionFactory();

	<T> List<T> getListByNamedQuery(Class<T> clazz, String namedQuerySQL,
			List<Object> params) throws DataAccessException;

	<T> List<T> getListBySQL(Class<T> clarzz, String sql, List<Object> params,
			int maxResult) throws DataAccessException;

	<T> List<T> getListBySQL(Class<T> clazz, String sql, List<Object> params,
			List<Class<?>> synchronizedClass, Integer maxResult)
			throws DataAccessException;

	<T> List<T> getListByQueryAndScalar(Class<T> clazz, String[] fieldNames,
			Type[] fieldTypes, String sql, List<Object> params,
			List<Class<?>> synchronizedClass, Integer maxResult)
			throws DataAccessException;

	<T> List<T> getListByQueryAndScalar(Class<T> clazz, String[] fieldNames,
			Type[] fieldTypes, String sql, List<Object> params,
			Integer maxResult) throws DataAccessException;

	List<Boolean> checkExistBySQL(List<String> lstSql,
			List<List<Object>> lstParams) throws DataAccessException;

	List<Object> getDataToListPaginated(String sql, List<Object> params,
			int fetchSize, int firstResult) throws DataAccessException;
	
	<T> List<T> getListByQueryDynamic(Class<T> clazz, String sql,
			List<Object> params, Integer maxResult) throws DataAccessException;
	
	<T> List<T> getListByQueryDynamicFromPackage(Class<T> clazz, String sql,
			List<Object> params, Integer maxResult) throws DataAccessException;

	Boolean isExistBySQL(String sql, List<Object> params)
			throws DataAccessException;

	<T> T getListByQueryAndScalarFirst(Class<T> clazz, String[] fieldNames,
			Type[] fieldTypes, String sql, List<Object> params)
			throws DataAccessException;

	<T> List<T> getListByQueryDynamicFromPackageClobOrArray(Class<T> clazz, String sql,
			List<Object> params, Integer maxResult, String betweenChar)
			throws DataAccessException;
	
	<T> List<T> getListByQueryAndScalar(String[] fieldNames,
			Type[] fieldTypes, String sql, List<Object> params,
			List<Class<?>> synchronizedClass, Integer maxResult)
			throws DataAccessException;
}
	
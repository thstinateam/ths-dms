package ths.dms.core.dao.repo;

import java.io.Serializable;

import ths.dms.core.exceptions.DataAccessException;

/**
 * @author thanhtc
 * 
 */
public interface IRepository extends SelectRepository, InsertRepository {
	<T> T getEntityById(Class<T> clazz, Serializable id,
			boolean oneSession) throws DataAccessException;
}

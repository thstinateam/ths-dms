package ths.dms.core.dao.repo;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.sql.Array;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Types;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import oracle.jdbc.OracleTypes;
import ths.dms.core.common.utils.GeneralUtil;
import ths.dms.core.common.utils.LogUtility;
import ths.dms.core.common.utils.StringUtility;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.internal.SessionImpl;
import org.hibernate.transform.Transformers;
import org.hibernate.type.AbstractStandardBasicType;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import ths.dms.core.entities.enumtype.KPaging;
import ths.core.entities.vo.rpt.DynamicVO;
import ths.dms.core.exceptions.DataAccessException;

public class SelectRepositoryImpl extends AbstractRepository implements
		SelectRepository {
	/** The session factory. */
	private SessionFactory selectSessionFactory;

	public SelectRepositoryImpl(SessionFactory selectSessionFactory) {
		this.selectSessionFactory = selectSessionFactory;
	}

	@Override
	public <T> T getEntityById(Class<T> clazz, Serializable id)
			throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();
			return clazz.cast(session.get(clazz, id));
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T getEntityByHQL(String hql, List<Object> params)
			throws DataAccessException {
		try {
			
			Session session = selectSessionFactory.getCurrentSession();
			Query query = session.createQuery(hql);
			addParameters(query, params);
			query.setCacheable(true);

			query.setMaxResults(1);
			return (T) query.uniqueResult();
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@Override
	public <T> T getEntityBySQL(Class<T> clazz, String sql, List<Object> params)
			throws DataAccessException {
		return getEntityBySQL(clazz, sql, params, null);
	}

	@Override
	public <T> T getEntityBySQL(Class<T> clazz, String sql,
			List<Object> params, List<Class<?>> synchronizedClass)
			throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();
			SQLQuery query = session.createSQLQuery(sql);
			addParameters(query, params);
			addSynchronizedClass(query, synchronizedClass);
			query.setCacheable(true);
			query.addEntity(clazz);

			query.setMaxResults(1);
			return clazz.cast(query.uniqueResult());
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getListByHQL(String hql, List<Object> params)
			throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();
			Query query = session.createQuery(hql);
			addParameters(query, params);
			query.setCacheable(true);
			return query.list();
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getListByHQLPaginated(String selectHql, String countHql,
			List<Object> selectParams, List<Object> countParams,
			KPaging<T> paging) throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();

			Query selectQuery = session.createQuery(selectHql);
			addParameters(selectQuery, selectParams);
			selectQuery.setCacheable(true);
			selectQuery.setFirstResult(paging.getPage() * paging.getPageSize());
			selectQuery.setMaxResults(paging.getPageSize());
			paging.setList(selectQuery.list());

			int totalRows = countByHQL(countHql, countParams);
			paging.setTotalRows(totalRows);

			return paging.getList();
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@Override
	public <T> List<T> getListByHQLPaginated(String hql, List<Object> params,
			KPaging<T> paging) throws DataAccessException {
		try {
			String countHql = hql.toLowerCase().trim();
			if (countHql.startsWith("select")) {
				countHql = hql.substring(countHql.indexOf("from"));
			} else {
				countHql = hql;
			}
			countHql = "select count(*) " + countHql;
			return getListByHQLPaginated(hql, countHql, params, params, paging);
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@Override
	public <T> List<T> getListBySQL(Class<T> clazz, String sql,
			List<Object> params) throws DataAccessException {
		return getListBySQL(clazz, sql, params, null);
	}

	@Override
	public <T> List<T> getListBySQL(Class<T> clazz, String sql,
			List<Object> params, int maxResult) throws DataAccessException {
		return getListBySQL(clazz, sql, params, null, maxResult);
	}

	@Override
	public <T> List<T> getListBySQL(Class<T> clazz, String sql,
			List<Object> params, List<Class<?>> synchronizedClass)
			throws DataAccessException {
		return getListBySQL(clazz, sql, params, synchronizedClass, null);
		// try {
		// Session session = selectSessionFactory.getCurrentSession();
		// SQLQuery query = session.createSQLQuery(sql);
		// addParameters(query, params);
		// addSynchronizedClass(query, synchronizedClass);
		// query.setCacheable(true);
		// query.addEntity(clazz);
		//
		// return query.list();
		// } catch (Exception e) {
		// throw new DataAccessException(e);
		// }
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getListBySQL(Class<T> clazz, String sql,
			List<Object> params, List<Class<?>> synchronizedClass,
			Integer maxResult) throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();
			SQLQuery query = session.createSQLQuery(sql);
			addParameters(query, params);
			addSynchronizedClass(query, synchronizedClass);
			query.setCacheable(true);
			query.addEntity(clazz);

			if (maxResult != null && maxResult > 0) {
				query.setMaxResults(maxResult);
			}

			return query.list();
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@Override
	public <T> List<T> getListBySQLPaginated(Class<T> clazz, String selectSql,
			String countSql, List<Object> selectParams,
			List<Object> countParams, KPaging<T> paging)
			throws DataAccessException {
		return getListBySQLPaginated(clazz, selectSql, countSql, selectParams,
				countParams, paging, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getListBySQLPaginated(Class<T> clazz, String selectSql,
			String countSql, List<Object> selectParams,
			List<Object> countParams, KPaging<T> paging,
			List<Class<?>> synchronizedClass) throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();

			SQLQuery selectQuery = session.createSQLQuery(selectSql);
			addParameters(selectQuery, selectParams);
			addSynchronizedClass(selectQuery, synchronizedClass);
			selectQuery.setCacheable(true);
			selectQuery.addEntity(clazz);
			selectQuery.setFirstResult(paging.getPage() * paging.getPageSize());
			selectQuery.setMaxResults(paging.getPageSize());
			paging.setList(selectQuery.list());

			int totalRows = countBySQL(countSql, countParams, synchronizedClass);
			paging.setTotalRows(totalRows);

			return paging.getList();
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@Override
	public <T> List<T> getListBySQLPaginated(Class<T> clazz, String sql,
			List<Object> params, KPaging<T> paging) throws DataAccessException {
		return getListBySQLPaginated(clazz, sql, params, paging, null);
	}

	@Override
	public <T> List<T> getListBySQLPaginated(Class<T> clazz, String sql,
			List<Object> params, KPaging<T> paging,
			List<Class<?>> synchronizedClass) throws DataAccessException {
		try {
			String countSql = sql.trim();

			if (countSql.toLowerCase().startsWith("select")) {
				countSql = "select count(*) AS count "
						+ countSql.substring(countSql.toLowerCase().indexOf("from"));
			} else if (countSql.toLowerCase().startsWith("with")) {
				countSql = "select count(*) AS count from (" + sql + ")";
			} else {
				countSql = "select count(*) AS count " + sql;
			}

			return getListBySQLPaginated(clazz, sql, countSql, params, params,
					paging, synchronizedClass);
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@Override
	public <T> List<T> getListByQueryAndScalar(Class<T> clazz,
			String[] fieldNames, Type[] fieldTypes, String sql,
			List<Object> params) throws DataAccessException {
		return getListByQueryAndScalar(clazz, fieldNames, fieldTypes, sql,
				params, null, null);
	}

	@Override
	public <T> List<T> getListByQueryAndScalar(Class<T> clazz,
			String[] fieldNames, Type[] fieldTypes, String sql,
			List<Object> params, Integer maxResult) throws DataAccessException {
		return getListByQueryAndScalar(clazz, fieldNames, fieldTypes, sql,
				params, null, maxResult);
	}

	// @SuppressWarnings("unchecked")
	// @Override
	// public <T> List<T> getListByQueryAndScalar(Class<T> clazz,
	// String[] fieldNames, Type[] fieldTypes, String sql,
	// List<Object> params, CacheConfig cacheConfig) {
	// Session session = sessionFactory.getCurrentSession();
	// SQLQuery query = session.createSQLQuery(sql);
	// addParameters(query, params);
	// addScalar(query, fieldNames, fieldTypes);
	// addCacheConfig(query, cacheConfig);
	// query.setResultTransformer(Transformers.aliasToBean(clazz));
	//
	// return query.list();
	// }

	@Override
	public <T> List<T> getListByQueryAndScalar(Class<T> clazz,
			String[] fieldNames, Type[] fieldTypes, String sql,
			List<Object> params, List<Class<?>> synchronizedClass)
			throws DataAccessException {
		return getListByQueryAndScalar(clazz, fieldNames, fieldTypes, sql,
				params, synchronizedClass, null);
		// try {
		// Session session = selectSessionFactory.getCurrentSession();
		// SQLQuery query = session.createSQLQuery(sql);
		// addParameters(query, params);
		// addScalar(query, fieldNames, fieldTypes);
		// addSynchronizedClass(query, synchronizedClass);
		// query.setResultTransformer(Transformers.aliasToBean(clazz));
		//
		// return query.list();
		// } catch (Exception e) {
		// throw new DataAccessException(e);
		// }
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getListByQueryAndScalar(Class<T> clazz,
			String[] fieldNames, Type[] fieldTypes, String sql,
			List<Object> params, List<Class<?>> synchronizedClass,
			Integer maxResult) throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();
			SQLQuery query = session.createSQLQuery(sql);
			addParameters(query, params);
			addScalar(query, fieldNames, fieldTypes);
			addSynchronizedClass(query, synchronizedClass);
			query.setResultTransformer(Transformers.aliasToBean(clazz));
			if (maxResult != null && maxResult > 0) {
				query.setMaxResults(maxResult);
			}

			return query.list();
		} catch (Exception e) {
			System.out.print(e.getMessage());
			throw new DataAccessException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> T getListByQueryAndScalarFirst(Class<T> clazz,
			String[] fieldNames, Type[] fieldTypes, String sql,
			List<Object> params) throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();
			SQLQuery query = session.createSQLQuery(sql);
			addParameters(query, params);
			addScalar(query, fieldNames, fieldTypes);
			addSynchronizedClass(query, null);
			query.setResultTransformer(Transformers.aliasToBean(clazz));

			if (!query.list().isEmpty()){
				return (T) query.list().get(0);	
			}else{
				return null;
			}
			
		} catch (Exception e) {
			System.out.print(e.getMessage());
			throw new DataAccessException(e);
		}
	}

	@Override
	public <T> List<T> getListByQueryAndScalarPaginated(Class<T> clazz,
			String[] fieldNames, Type[] fieldTypes, String selectSql,
			String countSql, List<Object> selectParams,
			List<Object> countParams, KPaging<T> paging)
			throws DataAccessException {
		return getListByQueryAndScalarPaginated(clazz, fieldNames, fieldTypes,
				selectSql, countSql, selectParams, countParams, paging, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getListByQueryAndScalarPaginated(Class<T> clazz,
			String[] fieldNames, Type[] fieldTypes, String selectSql,
			String countSql, List<Object> selectParams,
			List<Object> countParams, KPaging<T> paging,
			List<Class<?>> synchronizedClass) throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();
			SQLQuery selectQuery = session.createSQLQuery(selectSql);
			addParameters(selectQuery, selectParams);
			addScalar(selectQuery, fieldNames, fieldTypes);
			addSynchronizedClass(selectQuery, synchronizedClass);

			selectQuery.setResultTransformer(Transformers.aliasToBean(clazz));
			selectQuery.setFirstResult(paging.getPage() * paging.getPageSize());
			selectQuery.setMaxResults(paging.getPageSize());

			paging.setList(selectQuery.list());

			int totalRows = countBySQL(countSql, countParams, synchronizedClass);
			paging.setTotalRows(totalRows);

			return paging.getList();
		} catch (Exception e) {
			System.out.print(e.getMessage());
			throw new DataAccessException(e);
		}
	}

	// @SuppressWarnings("unchecked")
	// @Override
	// public <T> List<T> getListByQueryAndScalarPaginated(Class<T> clazz,
	// String[] fieldNames, Type[] fieldTypes, String selectSql,
	// String countSql, List<Object> selectParams,
	// List<Object> countParams, KPaging<T> paging, CacheConfig cacheConfig) {
	// Session session = sessionFactory.getCurrentSession();
	// SQLQuery selectQuery = session.createSQLQuery(selectSql);
	// addParameters(selectQuery, selectParams);
	// addScalar(selectQuery, fieldNames, fieldTypes);
	// addCacheConfig(selectQuery, cacheConfig);
	//
	// selectQuery.setResultTransformer(Transformers.aliasToBean(clazz));
	// selectQuery.setFirstResult(paging.getPage() * paging.getPageSize());
	// selectQuery.setMaxResults(paging.getPageSize());
	//
	// paging.setList(selectQuery.list());
	//
	// int totalRows = countBySQL(countSql, countParams);
	// paging.setTotalRows(totalRows);
	//
	// return paging.getList();
	// }

	@Override
	public int countByHQL(String countHql, List<Object> countParams)
			throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();
			Query countQuery = session.createQuery(countHql);
			addParameters(countQuery, countParams);
			countQuery.setCacheable(true);
			return ((Number) countQuery.uniqueResult()).intValue();
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@Override
	public int countBySQL(String countSql, List<Object> countParams)
			throws DataAccessException {
		return countBySQL(countSql, countParams, null);
	}

	@Override
	public int countBySQL(String countSql, List<Object> countParams,
			List<Class<?>> synchronizedClass) throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();
			SQLQuery countQuery = session.createSQLQuery(countSql);
			addParameters(countQuery, countParams);
			addSynchronizedClass(countQuery, synchronizedClass);
			countQuery.addScalar("count", StandardBasicTypes.BIG_DECIMAL);
			countQuery.setCacheable(true);
			if (countQuery.uniqueResult() != null) {
				return ((Number) countQuery.uniqueResult()).intValue();				
			}
			return 0;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getListByNamedQuery(String namedQuerySQL,
			List<Object> params) throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();
			Query namedQuery = session.getNamedQuery(namedQuerySQL);
			addParameters(namedQuery, params);

			namedQuery.setCacheable(true);
			return namedQuery.list();
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getListByNamedQuery(final Class<T> clazz,
			final String namedQuerySQL, final List<Object> params)
			throws DataAccessException {
		try {
			final Session session = selectSessionFactory.getCurrentSession();
			final Query namedQuery = session.getNamedQuery(namedQuerySQL);
			// for (int i = 0; i < fieldNames.length; i++) {
			// qSelect.addScalar(fieldNames[i], fieldTypes[i]);
			// }

			namedQuery.setResultTransformer(Transformers.aliasToBean(clazz));

			// SQLQuery sqlQuery = (SQLQuery) namedQuery;
			// sqlQuery.addScalar("theGeom",
			// Hibernate.custom(SDOGeometryType.class));

			// try {
			// CommonUtility.addScalar(namedQuery, PromotionBasicInfo.class);
			// } catch (Exception e) {
			// throw new DataAccessException(e);
			// }

			for (int i = 0; i < params.size(); i += 3) {
				namedQuery.setParameter(params.get(i).toString(),
						params.get(i + 1),
						(AbstractStandardBasicType) params.get(i + 2));
			}
			return namedQuery.list();

		} catch (final HibernateException e) {
			throw new DataAccessException(
					"Failed to getTopByNamedNativeQuery: " + e.getMessage(), e);
		}
	}

	@Override
	public Object getObjectByQuery(String sql, List<Object> params)
			throws DataAccessException {
		return getObjectByQuery(sql, params, null);
	}

	@Override
	public Object getObjectByQuery(String sql, List<Object> params,
			List<Class<?>> synchronizedClass) throws DataAccessException {
		try {
			Session sess = selectSessionFactory.getCurrentSession();
			SQLQuery qSelect = sess.createSQLQuery(sql);
			addParameters(qSelect, params);
			addSynchronizedClass(qSelect, synchronizedClass);

			qSelect.setMaxResults(1);
			return qSelect.uniqueResult();

		} catch (final HibernateException e) {
			throw new DataAccessException("Failed to getObjectByQuery: "
					+ e.getMessage(), e);
		}
	}

	@Override
	public SessionFactory getSessionFactory() {
		return selectSessionFactory;
	}

	@Override
	public <T> T getFirstBySQL(Class<T> clazz, String sql, List<Object> params)
			throws DataAccessException {
		return getFirstBySQL(clazz, sql, params, null);
	}

	@Override
	public <T> T getFirstBySQL(Class<T> clazz, String sql, List<Object> params,
			List<Class<?>> synchronizedClass) throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();
			SQLQuery query = session.createSQLQuery(sql);
			addParameters(query, params);
			addSynchronizedClass(query, synchronizedClass);
			query.setCacheable(true);
			query.addEntity(clazz);
			query.setMaxResults(1);

			@SuppressWarnings("unchecked")
			List<T> list = query.list();

			if (list.size() > 0) {
				return list.get(0);
			} else {
				return null;
			}
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@Override
	public List<Boolean> checkExistBySQL(List<String> lstSql,
			List<List<Object>> lstParams) throws DataAccessException {

		List<Boolean> rs = new ArrayList<Boolean>();
		Session session = selectSessionFactory.getCurrentSession();
		for (int i = 0; i < lstSql.size(); i++) {
			String sql = lstSql.get(i);
			sql = "select 1 as count " + sql;
			List<Object> params = lstParams.get(i);

			SQLQuery query = session.createSQLQuery(sql);
			addParameters(query, params);
			query.setCacheable(true);

			query.addScalar("count", StandardBasicTypes.BIG_DECIMAL);
			Object c = query.uniqueResult();
			rs.add(c == null ? false : true);
		}
		return rs;
	}
	
	@Override
	public Boolean isExistBySQL(String sql, List<Object> params) throws DataAccessException {
		Session session = selectSessionFactory.getCurrentSession();
		sql = "select 1 as count " + sql;
		SQLQuery query = session.createSQLQuery(sql);
		addParameters(query, params);
		query.setCacheable(true);
		query.addScalar("count", StandardBasicTypes.BIG_DECIMAL);
		Object c = query.uniqueResult();
		return c == null ? false : true;
	}

	@Override
	public List<Object> getDataToListPaginated(final String sql,
			final List<Object> params, final int fetchSize,
			final int firstResult) throws DataAccessException {
		try {

			final Session session = selectSessionFactory.getCurrentSession();
			SQLQuery query = session.createSQLQuery(sql);

			addParameters(query, params);

			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);

			if (fetchSize > 0) {
				query.setMaxResults(fetchSize);
			}

			if (firstResult > 0) {
				query.setFirstResult(firstResult);
			}

			List<Object> data = query.list();

			return data;
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public BigDecimal countBySQLReturnBigDecimal(String countSql,
			List<Object> countParams) throws DataAccessException {
		Session session = selectSessionFactory.getCurrentSession();
		SQLQuery countQuery = session.createSQLQuery(countSql);
		addParameters(countQuery, countParams);
		addSynchronizedClass(countQuery, null);
		countQuery.addScalar("count", StandardBasicTypes.BIG_DECIMAL);
		countQuery.setCacheable(true);
		return (BigDecimal)countQuery.uniqueResult();
	}
	
	/**
	 * @author nald
	 * @description ham ho tro lay report cot dong tu cau SQL truy van co truoc
	 * @param clazz: VO nhan ket qua tra ve. VO gom cac thuoc tinh tinh, va 1 list kieu DynamicVO. Tham khao TestDynamicVO.java
	 * @param sql: cau truy van SQL
	 * @param maxResult: so ket qua toi da tra ve
	 * @return List<VO>
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getListByQueryDynamic(Class<T> clazz, String sql,
			List<Object> params, Integer maxResult) throws DataAccessException {
		try {
			List<Object> listTmp;

			Session session = selectSessionFactory.getCurrentSession();
			SQLQuery query = session.createSQLQuery(sql);
			addParameters(query, params);
			//addScalar(query, fieldNames, fieldTypes);
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);

			if (maxResult != null && maxResult > 0) {
				query.setMaxResults(maxResult);
			}

			listTmp = query.list();
			
			
			List<T> listResults = convert2ClassVO(clazz, listTmp);
			
			return (List<T>) listResults;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	/**
	 * @author nald
	 * @description ham ho tro lay report cot dong tu cau SQL truy van co truoc
	 * @param clazz: VO nhan ket qua tra ve. VO gom cac thuoc tinh tinh, va 1 list kieu DynamicVO. Tham khao TestDynamicVO.java
	 * @param sql: cau truy van SQL
	 * @param maxResult: so ket qua toi da tra ve
	 * @return List<VO>
	 * 
	 * @modify hunglm16
	 * @since October 11, 2015
	 * @description Bo sung loai du lieu dong
	 */
	@Override
	public <T> List<T> getListByQueryDynamicFromPackage(Class<T> clazz, String sql, List<Object> params, Integer maxResult) throws DataAccessException {
		try {
			List<Object> listTmp;
			Session session = selectSessionFactory.getCurrentSession();
			Connection connection = ((SessionImpl) session).connection();
			CallableStatement callable = connection.prepareCall(sql);
			for (int i = 0; i < params.size(); i += 3) {
				Integer tmpName = (Integer) params.get(i);
				Object tmpValue = params.get(i + 1);
				Integer tmpTypes = (Integer) params.get(i + 2);

				if (tmpTypes == Types.CLOB) {
					Clob clob = connection.createClob();
					clob.setString(1, tmpValue.toString());
					callable.setClob(tmpName, clob);
				} else {
					callable.setObject(tmpName, tmpValue, tmpTypes);
				}
			}

			callable.registerOutParameter(1, OracleTypes.CURSOR);
			callable.execute();

			ResultSet rs = null;
			rs = (ResultSet) callable.getObject(1);

			ResultSetMetaData metaData = rs.getMetaData();
			int columns = metaData.getColumnCount();

			listTmp = new ArrayList<Object>();

			while (rs.next()) {
				LinkedHashMap<String, Object> listItem = new LinkedHashMap<String, Object>();
				for (int i = 1; i <= columns; i++) {
					Object value = rs.getObject(i);
					String key = StringUtility.dropUnderlined(metaData.getColumnName(i));
					listItem.put(key, value);
				}
				listTmp.add(listItem);
			}

			List<T> listResults = convert2ClassVO(clazz, listTmp);

			return (List<T>) listResults;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	
	/**
	 * @author thongnm1
	 * @description ham ho tro lay report cot dong tu cau SQL truy van co truoc bo sung kieu du liệu clob
	 * @param clazz: VO nhan ket qua tra ve. VO gom cac thuoc tinh tinh, va 1 list kieu DynamicVO. Tham khao TestDynamicVO.java
	 * @param sql: cau truy van SQL
	 * @param maxResult: so ket qua toi da tra ve
	 * @return List<VO>
	 */
	@Override
	public <T> List<T> getListByQueryDynamicFromPackageClobOrArray(Class<T> clazz,
			String sql, List<Object> params, Integer maxResult, String betweenChar)
			throws DataAccessException {
		try {
			List<Object> listTmp;
			Session session = selectSessionFactory.getCurrentSession();
			Connection connection = ((SessionImpl)session).connection();
		    CallableStatement callable = connection.prepareCall(sql);
		    for (int i = 0; i < params.size(); i += 3) {
		    	Integer tmpName = (Integer) params.get(i);
		    	Object tmpValue = params.get(i + 1);
		    	Integer tmpTypes = (Integer) params.get(i + 2);
		    	
		    	callable.setObject(tmpName,tmpValue, tmpTypes);
			}
		    
		    callable.registerOutParameter(1, OracleTypes.CURSOR);
		    callable.execute();
		    
		    ResultSet rs = null;
		    rs = (ResultSet) callable.getObject(1);
		    
		    ResultSetMetaData metaData = rs.getMetaData();
		    int columns = metaData.getColumnCount();

		    listTmp = new ArrayList<Object>();

		    while (rs.next()) {
		        LinkedHashMap<String, Object> listItem = new LinkedHashMap<String, Object>();
		        for (int i = 1; i <= columns; i++) {
		            Object value = rs.getObject(i);
		            String key = StringUtility.dropUnderlined(metaData.getColumnName(i));
		            if(value instanceof Array || value instanceof Clob){
		            	value = convertClobOrArray(value, betweenChar);
		            }
		            listItem.put(key, value);
		        }
		        listTmp.add(listItem);
		    }
			
			List<T> listResults = convert2ClassVO(clazz, listTmp);

			return (List<T>) listResults;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	/**
	 * @author thongnm1
	 * @param convert du lieu kieu Clob Or Array
	 */
	private Object convertClobOrArray(Object value, String betweenChar) {
	    try {
			if (value instanceof Array) {
				StringBuilder strValue  = new StringBuilder();
				Array z = (Array) value;
				String[] values = (String[])z.getArray();
				for (int i = 0; i < values.length; i++) {
					strValue.append(values[i]);
					strValue.append(betweenChar);
				}
			    value = strValue.toString();
			}
			else if (value instanceof Clob) {
			    Clob c=(Clob)value;
			    value=c.getSubString(1,(int)c.length());
			}
		} catch (SQLException e) {
			LogUtility.logError(e, e.getMessage());
		}
		return value;
	}
	/**
	 * @author nald
	 * @param clazz
	 * @param listTmp
	 * @return List<T> class VO
	 */
	private <T> List<T> convert2ClassVO(Class<T> clazz, List<Object> listTmp)
			throws InstantiationException, IllegalAccessException,
			NoSuchFieldException, NoSuchMethodException,
			InvocationTargetException {
		List<T> listResults = new ArrayList<T>();

		Field[] fields = clazz.getDeclaredFields();
		if (listTmp.size() != 0) {

			for (Object object : listTmp) {
				T subClazz = clazz.newInstance();

				Map row = (Map) object;
				Set keySet = row.keySet();
				
				List<DynamicVO> tmpDynamicVOs = new ArrayList<DynamicVO>();
				
				for (Object key : keySet) {
					String tmp = GeneralUtil.hasInFields(key, fields);
					if (tmp != null) {
						Object tmpKey = row.get(key);
						if (tmpKey == null) {
							GeneralUtil.applyValue2Property(subClazz, tmp, "");
						} else {
							GeneralUtil.applyValue2Property(subClazz, tmp, row
									.get(key).toString());
						}
					} else {
						if(!key.toString().startsWith("not_view_")){
							DynamicVO tmpDynamicVO = new DynamicVO();
							tmpDynamicVO.setKey(key.toString());
							tmpDynamicVO.setValue(row.get(key));
							tmpDynamicVOs.add(tmpDynamicVO);
						}
					}
				}
				
				for (Field f : fields) {
					java.lang.reflect.Type type = f.getGenericType();
					
					// Tim thuoc tinh kieu List<DynamicVO> trong Class
					if (type instanceof ParameterizedType) {
						ParameterizedType stringListType = (ParameterizedType) f
								.getGenericType();
						Class<?> stringListClass = (Class<?>) stringListType
								.getActualTypeArguments()[0];
						
						if(stringListClass.equals(DynamicVO.class))
						{
							//Thuoc tinh kieu List<DynamicVO>
							String proName = f.getName();
							Class<?> clz = subClazz.getClass().getDeclaredField(proName)
									.getType();
							String proNameUpperFirst = proName.substring(0, 1).toUpperCase()
									+ proName.substring(1);
							Method setMethod = subClazz.getClass().getMethod(
									"set" + proNameUpperFirst, clz);
							setMethod.invoke(subClazz, tmpDynamicVOs);
							
							break;
						}
						
					}
				}

				listResults.add(subClazz);
			}
		}
		return listResults;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> getListByQueryAndScalar(
			String[] fieldNames, Type[] fieldTypes, String sql,
			List<Object> params, List<Class<?>> synchronizedClass,
			Integer maxResult) throws DataAccessException {
		try {
			Session session = selectSessionFactory.getCurrentSession();
			SQLQuery query = session.createSQLQuery(sql);
			addParameters(query, params);
			addScalar(query, fieldNames, fieldTypes);
			addSynchronizedClass(query, synchronizedClass);
			if (maxResult != null && maxResult > 0) {
				query.setMaxResults(maxResult);
			}

			return query.list();
		} catch (Exception e) {
			System.out.print(e.getMessage());
			throw new DataAccessException(e);
		}
	}
}

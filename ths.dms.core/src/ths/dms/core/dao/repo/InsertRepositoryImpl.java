package ths.dms.core.dao.repo;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;

import ths.dms.core.entities.enumtype.SpParam;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.LogUtility;


public class InsertRepositoryImpl extends AbstractRepository implements InsertRepository {
	
	/** The session factory. */
	private SessionFactory insertSessionFactory;
	
	public InsertRepositoryImpl(SessionFactory insertSessionFactory) {
		this.insertSessionFactory = insertSessionFactory;
	}
	
	@Override
	public <T> T create(T object) throws DataAccessException {
		try {
			Session session = insertSessionFactory.getCurrentSession();
			session.persist(object);
			return (T) object;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	
	@Override
	public <T> List<T> create(List<T> lstObject) throws DataAccessException  {
		try {
			Session session = insertSessionFactory.getCurrentSession();
//			int i = 0;
			for (T obj: lstObject) {
//				i++;
//				session.save(obj);
//	            if (i%50 == 0)
//	            {
//	                session.flush();
//	                session.clear();
//	            }
				if (obj != null)
					session.persist(obj);
			}
			return lstObject;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	
	@Override
	public <T> List<T> update(List<T> lstObject) throws DataAccessException  {
		try {
			Session session = insertSessionFactory.getCurrentSession();
			int i = 0;
			for (T obj: lstObject) {
				i++;
				session.update(obj);
	            if (i%50 == 0)
	            {
	                session.flush();
	                session.clear();
	            }
			}
			return lstObject;
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@Override
	public void update(Object object) throws DataAccessException {
		try {
			Session session = insertSessionFactory.getCurrentSession();
//			session.update(object);
			session.merge(object);
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@Override
	public int executeSQLQuery(String sql, List<Object> params)
			throws DataAccessException {
		try {
			Session session = insertSessionFactory.getCurrentSession();
			SQLQuery query = session.createSQLQuery(sql);
			addParameters(query, params);
			return query.executeUpdate();
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}

	@Override
	public void delete(Object object) throws DataAccessException {
		try {
			Session session = insertSessionFactory.getCurrentSession();
			session.delete(object);
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	
	@Override
	public <T> T getEntityById(Class<T> clazz, Serializable id)
			throws DataAccessException {
		try {
			Session session = insertSessionFactory.getCurrentSession();
			return clazz.cast(session.get(clazz, id));
		} catch (Exception e) {
			throw new DataAccessException(e);
		}
	}
	@Override
	public void executeSP(final String spName, final List<SpParam> inParams,
			final List<SpParam> outParams) throws DataAccessException {
		try {
			int numParam = inParams.size() + outParams.size();

			final StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("{call ");
			stringBuilder.append(spName);

			if (numParam > 0) {
				stringBuilder.append("(?");
				for (int i = 1; i < numParam; i++) {
					stringBuilder.append(", ?");
				}
				stringBuilder.append(")");
			}
			stringBuilder.append("}");

			insertSessionFactory.getCurrentSession().doReturningWork(
				new ReturningWork<Boolean>() {
					public Boolean execute(Connection con)
							throws SQLException {
						try {
							CallableStatement st = con
									.prepareCall(stringBuilder.toString());
							if (inParams.size() > 0) {
								for (SpParam param : inParams) {
									setParam(st, param.getParamIndex(),
											param.getSqlType(),
											param.getValue());
								}
							}

							if (outParams.size() > 0) {
								for (SpParam param : outParams) {
									st.registerOutParameter(
											param.getParamIndex(),
											param.getSqlType());
								}
							}
							st.executeUpdate();

							for (SpParam outParam : outParams) {
								outParam.setValue(getValue(st,
										outParam.getParamIndex(),
										outParam.getSqlType()));
							}
						} catch (Exception e) {
							LogUtility.logError(e, e.getMessage());
							return false;
						}
						return true;
					}
				});

		} catch (HibernateException e) {
			throw new DataAccessException(e);
		}
	}
	
	//dung cho 1 output
	@Override
	public void executeFunc(final String spName, final List<SpParam> inParams,
			final List<SpParam> outParams) throws DataAccessException {
		try {
			int numParam = inParams.size();

			final StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("{? = call ");
			stringBuilder.append(spName);

			if (numParam > 0) {
				stringBuilder.append("(?");
				for (int i = 1; i < numParam; i++) {
					stringBuilder.append(", ?");
				}
				stringBuilder.append(")");
			}
			stringBuilder.append("}");
			
			insertSessionFactory.getCurrentSession().doWork(new Work() {
				public void execute(Connection con) throws SQLException {
					try{
						CallableStatement st = con.prepareCall(stringBuilder.toString());
						if (outParams.size() > 0) {
							for (SpParam param : outParams) {
								st.registerOutParameter( param.getParamIndex(), param.getSqlType());
							}
						}
						if (inParams.size() > 0) {
							for (SpParam param : inParams) {
								setParam(st, param.getParamIndex(), param.getSqlType(), param.getValue());
							}
						}
						st.execute();
						for (SpParam outParam : outParams) {
							outParam.setValue(getValue(st, outParam.getParamIndex(), outParam.getSqlType()));
						}
					}catch (Exception ex){
						System.out.println(ex.getMessage());
					}
				}
			});
		} catch (HibernateException e) {
			throw new DataAccessException(e);
		}
	}

	private void setParam(CallableStatement st, int index, int sqlType, Object value) throws NumberFormatException, SQLException, DataAccessException {

		switch (sqlType) {
			case Types.INTEGER:
				if (value != null) {
					st.setInt(index, (Integer) value);
				} else {
					st.setNull(index, Types.INTEGER);
				}
				break;
	
			case Types.VARCHAR:
				if (value != null) {
					st.setString(index, value.toString());
				} else {
					st.setNull(index, Types.VARCHAR);
				}
				break;
	
			case Types.NUMERIC:
				if (value != null) {
					if ("Long".equals(value.getClass().getSimpleName()))
						st.setLong(index, (Long) value);
					else
						st.setInt(index, (Integer) value);
				} else {
					st.setNull(index, Types.NUMERIC);
				}
				break;
			case Types.DATE:
				if (value != null) {
					java.util.Date valuex = (java.util.Date) value;
					java.sql.Date inputDate = new java.sql.Date(valuex.getTime());
					st.setDate(index, inputDate);
				} else {
					st.setNull(index, Types.DATE);
				}
				break;
			default:
				throw new DataAccessException("setParam: Format invalid");
		}
	}
	
	private Object getValue(CallableStatement st, int index, int sqlType)
			throws NumberFormatException, SQLException {

		Object result;
		switch (sqlType) {
		case Types.INTEGER:
			result = st.getInt(index);
			break;

		case Types.VARCHAR:
			result = st.getString(index);
			break;

		case Types.NUMERIC:
			result = st.getInt(index);
			break;

		default:
			result = st.getString(index);
			break;
		}
		return result;
	}
}

package ths.dms.core.dao.repo;

import java.io.Serializable;
import java.util.List;

import ths.dms.core.entities.enumtype.SpParam;
import ths.dms.core.exceptions.DataAccessException;

public interface InsertRepository {
	<T> T create(T object) throws DataAccessException;

	void update(Object object) throws DataAccessException;

	int executeSQLQuery(String sql, List<Object> params)
			throws DataAccessException;

	void delete(Object object) throws DataAccessException;

	<T> T getEntityById(Class<T> clazz, Serializable id)
			throws DataAccessException;

	<T> List<T> create(List<T> lstObject) throws DataAccessException;

	<T> List<T> update(List<T> lstObject) throws DataAccessException;

	void executeSP(String spName, List<SpParam> inParams,
			List<SpParam> outParams) throws DataAccessException;
	void executeFunc(String spName, List<SpParam> inParams,
			List<SpParam> outParams) throws DataAccessException;
}

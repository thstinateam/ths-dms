package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.FocusProgram;
import ths.dms.core.entities.FocusShopMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class FocusShopMapDAOImpl implements FocusShopMapDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	FocusProgramDAO focusProgramDAO;

	@Override
	public FocusShopMap createFocusShopMap(FocusShopMap focusShopMap,
			LogInfoVO logInfo) throws DataAccessException {
		if (focusShopMap == null) {
			throw new IllegalArgumentException("focusShopMap");
		}

		if (focusShopMap.getFocusProgram() != null
				&& focusShopMap.getShop() != null) {
			if (focusShopMap.getStatus().getValue().compareTo(ActiveType.RUNNING.getValue()) == 0)
				if (checkIfRecordExist(focusShopMap.getFocusProgram().getId(),
						focusShopMap.getShop().getShopCode(), null))
					return null;
		} else {
			return null;
		}
		
		focusShopMap.setCreateUser(logInfo.getStaffCode());
		focusShopMap = repo.create(focusShopMap);

		/*Boolean b = this.checkIfAncestorInFp(focusShopMap.getShop().getId(),
				focusShopMap.getFocusProgram().getId());
		
		if (b){
			focusShopMap.setStatus(ActiveType.DELETED);
		}
		
		repo.create(focusShopMap);
		
		if (!b) {
			List<FocusShopMap> lst = this.getListFocusShopMapByParentShop(
					focusShopMap.getShop().getId(), focusShopMap
							.getFocusProgram().getId());
			for (FocusShopMap fsm : lst) {
				fsm.setStatus(ActiveType.DELETED);
				this.updateFocusShopMap(fsm, logInfo);
			}
		}*/
		
		/*List<FocusShopMap> lst = this.getListFocusShopMapByParentShop(
				focusShopMap.getShop().getId(), focusShopMap
						.getFocusProgram().getId());
		
		if (null != lst && lst.size() > 0) {
			for (FocusShopMap fsm : lst) {
				fsm.setStatus(ActiveType.DELETED);
				this.updateFocusShopMap(fsm, logInfo);
			}
		}*/

		try {
			actionGeneralLogDAO.createActionGeneralLog(null, focusShopMap,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(FocusShopMap.class, focusShopMap.getId());
	}

	@Override
	public List<FocusShopMap> getListFocusShopMapWithShopAndFocusProgram(
			Long shopId, Long focusProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select * from focus_shop_map where status = 1 and shop_id in (select shop_id from shop start with shop_id = ? connect by prior parent_shop_id = shop_id)");
		// sql.append(" and shop_id <> ?");
		// params.add(shopId);
		params.add(shopId);
		sql.append(" and focus_program_id = ?");
		params.add(focusProgramId);
		
		return repo.getListBySQL(FocusShopMap.class, sql.toString(), params);
	}

	@Override
	public Boolean createListFocusShopMap(Long focusProgramId,
			List<Long> lstShopId, ActiveType status, LogInfoVO logInfo)
			throws DataAccessException {
		if (focusProgramId == null) {
			throw new IllegalArgumentException("focusProgramId is null");
		}
		List<FocusShopMap> lstFocusShopMap = new ArrayList<FocusShopMap>();
		List<FocusShopMap> lstFocusShopMapUpdate = new ArrayList<FocusShopMap>();
		FocusProgram pg = focusProgramDAO.getFocusProgramById(focusProgramId);
		if (pg == null) {
			throw new IllegalArgumentException("focusProgram is null");
		}

		List<Shop> lstShop = new ArrayList<Shop>();
		List<Long> lstShopId1 = new ArrayList<Long>();

		// lay tat ca shop con chau
		// for (Long shopId: lstShopId) {
		// if (shopId == null)
		// continue;
		// List<Shop> lst = shopDAO.getAllShopChild(shopId);
		// for (Shop s: lst) {
		// // if (s.getType() != null)
		// // if
		// (ShopObjectType.NPP.getValue().equals(s.getType().getObjectType()) &&
		// !lstShopId1.contains(s.getId())) {
		// // lstShop.add(s);
		// // lstShopId1.add(s.getId());
		// // }
		// if (!lstShopId1.contains(s.getId())) {
		// lstShop.add(s);
		// lstShopId1.add(s.getId());
		// }
		// }
		// }

		for (Long shopId : lstShopId) {
			if (shopId == null)
				continue;
			Shop s = shopDAO.getShopById(shopId);
			if (s != null
					&& !lstShopId1.contains(s.getId())
					&& ActiveType.RUNNING.getValue() == s.getStatus()
							.getValue()) {
				lstShop.add(s);
				lstShopId1.add(s.getId());
			}
		}

		for (Shop s : lstShop) {
			if (s == null) {
				throw new IllegalArgumentException("shop is null");
			}
			FocusShopMap m = getFocusShopMap(focusProgramId, s.getId(), null);

			// for log purpose
			if (m != null) {
				m = m.clone();
			}

			if (m == null) {
				if (lstShopId.contains(s.getId())) {
					m = new FocusShopMap();
					m.setFocusProgram(pg);
					m.setShop(s);
					m.setStatus(ActiveType.RUNNING);
					m.setCreateUser(logInfo.getStaffCode());
					lstFocusShopMap.add(m);
				}
			} else if (status != null) {
				m.setStatus(status);
				lstFocusShopMapUpdate.add(m);
			}
		}
		if (lstFocusShopMap.size() > 0) {
			for (FocusShopMap fsm : lstFocusShopMap) {
				fsm = this.createFocusShopMap(fsm, logInfo);
			}
		}
		if (lstFocusShopMapUpdate.size() > 0) {
			for (FocusShopMap fsm : lstFocusShopMapUpdate) {
				this.updateFocusShopMap(fsm, logInfo);
			}
		}
		if (lstFocusShopMap.size() == 0 && lstFocusShopMapUpdate.size() == 0)
			return false;
		return true;
	}

	@Override
	public void deleteFocusShopMap(FocusShopMap focusShopMap, LogInfoVO logInfo)
			throws DataAccessException {
		if (focusShopMap == null) {
			throw new IllegalArgumentException("focusShopMap");
		}
		repo.delete(focusShopMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(focusShopMap, null,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public FocusShopMap getFocusShopMapById(long id) throws DataAccessException {
		return repo.getEntityById(FocusShopMap.class, id);
	}

	@Override
	public void updateFocusShopMap(FocusShopMap focusShopMap, LogInfoVO logInfo)
			throws DataAccessException {
		if (focusShopMap == null) {
			throw new IllegalArgumentException("focusShopMap");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (focusShopMap.getFocusProgram() != null
				&& focusShopMap.getShop() != null) {
			if (focusShopMap.getStatus() == ActiveType.RUNNING)
				if (checkIfRecordExist(focusShopMap.getFocusProgram().getId(),
						focusShopMap.getShop().getShopCode(),
						focusShopMap.getId()))
					return;
		} else
			return;
		FocusShopMap temp = this.getFocusShopMapById(focusShopMap.getId());
		FocusShopMap mm = new FocusShopMap();
		mm.setCreateDate(temp.getCreateDate());
		mm.setCreateUser(temp.getUpdateUser());
		mm.setFocusProgram(temp.getFocusProgram());
		mm.setId(temp.getId());
		mm.setShop(temp.getShop());
		mm.setStatus(temp.getStatus());
		mm.setUpdateDate(temp.getUpdateDate());
		mm.setUpdateUser(temp.getUpdateUser());
		temp = mm;

		focusShopMap.setUpdateDate(commonDAO.getSysDate());
		focusShopMap.setUpdateUser(logInfo.getStaffCode());
		repo.update(focusShopMap);
		temp.setId(focusShopMap.getId());
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, focusShopMap,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public List<FocusShopMap> getListFocusShopMapByFocusProgramId(
			KPaging<FocusShopMap> kPaging, Long focusProgramId,
			String shopCode, String shopName, ActiveType status,
			Long parentShopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select f.* from focus_shop_map f join shop s on f.shop_id = s.shop_id where 1=1 ");

		if (status != null) {
			sql.append(" and f.status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and f.status!=?");
			params.add(ActiveType.DELETED.getValue());// sua lai ko delete
		}

		if (focusProgramId != null) {
			sql.append(" and f.focus_program_id=?");
			params.add(focusProgramId);
		}
//		else
//			sql.append(" and f.focus_program_id is null");

		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and upper(s.shop_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode
					.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(shopName)) {
			shopName = shopName.toUpperCase();
			sql.append(" and (upper(s.shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shopName));
			sql.append(" or upper(s.name_text) like ? escape '/' ) ");
			shopName = Unicode2English.codau2khongdau(shopName);
			shopName = StringUtility.toOracleSearchLike(shopName);
			params.add(shopName);
		}

		if (parentShopId != null) {
			sql.append(" and f.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
			params.add(parentShopId);
		}

		// sql.append(" order by focus_shop_map_id desc");
		sql.append(" order by s.shop_code asc");
		if (kPaging == null)
			return repo
					.getListBySQL(FocusShopMap.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(FocusShopMap.class,
					sql.toString(), params, kPaging);
	}

	@Override
	public Boolean isUsingByOthers(long focusShopMapId)
			throws DataAccessException {

		return false;
	}

	@Override
	public Boolean checkIfRecordExist(long focusProgramId, String shopCode,
			Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		shopCode = shopCode.toUpperCase();
		sql.append(" select count(1) as count from focus_shop_map m where focus_program_id=? ");
		sql.append("  and exists (select 1 from shop s where m.shop_id=s.shop_id and shop_code=?) and status!=?");
		List<Object> params = new ArrayList<Object>();
		params.add(focusProgramId);
		params.add(shopCode);
		params.add(ActiveType.DELETED.getValue());
		if (id != null) {
			sql.append(" and focus_shop_map_id!=?");
			params.add(id);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	/** lay vung duoc gan vao FocusShopMap */
	@Override
	public Shop getShopMapArea(long focusShopMapId) throws DataAccessException {
		/*String sql = "select * from focus_shop_map";
		List<Object> params = new ArrayList<Object>();
		FocusShopMap focusShopMap = repo.getFirstBySQL(FocusShopMap.class, sql,
				params);
		if (focusShopMap == null)
			return null;
		Shop s = focusShopMap.getShop();
		if (s == null)
			return null;
		while (s != null) {
			if (s.getType().getObjectType() == ShopType.AREA.getValue())
				return s;
			s = s.getParentShop();
		}*/
		return null;
	}

	@Override
	public FocusShopMap getFocusShopMap(long focusProgramId, Long shopId,
			Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from focus_shop_map m where focus_program_id = ? and m.shop_id = ? and status != ?");
		List<Object> params = new ArrayList<Object>();
		params.add(focusProgramId);
		params.add(shopId);
		params.add(ActiveType.DELETED.getValue());
		if (id != null) {
			sql.append(" and focus_shop_map_id!=?");
			params.add(id);
		}
		return repo.getFirstBySQL(FocusShopMap.class, sql.toString(), params);
	}

	@Override
	public Boolean checkShopFamilyInFocusProgram(Long shopId,
			Long focusProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from shop s where s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and s.shop_id not in (select shop_id from focus_shop_map where focus_program_id = ? and status = ?)");
		params.add(focusProgramId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" start with shop_id = ? connect by prior shop_id = parent_shop_id");
		params.add(shopId);
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? false : true;
	}

	@Override
	public Boolean checkShopAncestorInFocusProgram(Long focusProgramId,
			Long shopId) throws DataAccessException {
		// lay mien cua shopId
		StringBuilder sql1 = new StringBuilder();
		List<Object> params1 = new ArrayList<Object>();
		sql1.append(" select s.* from shop s join channel_type c ");
		sql1.append(" on s.shop_type_id = c.channel_type_id where c.object_type = ? ");
		params1.add(ShopObjectType.MIEN.getValue());
		sql1.append(" and s.status != ? start with shop_id = ? connect by prior parent_shop_id = shop_id");
		params1.add(ActiveType.DELETED.getValue());
		params1.add(shopId);
		Shop shop = repo.getFirstBySQL(Shop.class, sql1.toString(), params1);
		if (shop == null)
			return true;

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from focus_shop_map s where s.focus_program_id = ? and s.status != ?");
		params.add(focusProgramId);
		params.add(ActiveType.DELETED.getValue());
		sql.append(" and s.shop_id in (select shop_id from shop ");
		//sql.append("	start with shop_id = ? connect by prior shop_id = parent_shop_id)");
		sql.append("	start with shop_id = ? connect by prior parent_shop_id = shop_id )");
		params.add(shop.getId());
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false; /*tientv11*/
	}

	private Shop getShopInLevel1(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select *");
		sql.append(" from shop s");
		sql.append(" where exists (select 1 from shop pr where pr.shop_id = s.parent_shop_id and pr.parent_shop_id is null)");
		sql.append(" start with s.shop_id = ? connect by prior s.parent_shop_id = s.shop_id");
		params.add(shopId);
		return repo.getEntityBySQL(Shop.class, sql.toString(), params);

	}

	
	private List<FocusShopMap> getListFocusShopMapByParentShop(Long parentShopId, Long focusProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from focus_shop_map f where focus_program_id = ? ");
		params.add(focusProgramId);
		sql.append(" and f.shop_id != ? and f.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
		params.add(parentShopId);
		params.add(parentShopId);
		return repo.getListBySQL(FocusShopMap.class, sql.toString(), params);
	}
	
	@Override
	public Boolean checkIfAncestorInFp(Long shopId, Long focusProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from focus_shop_map f where status != -1 and focus_program_id = ? ");
		params.add(focusProgramId);
		sql.append(" and f.shop_id in (select shop_id from shop start with shop_id = ? connect by prior parent_shop_id = shop_id)");
		params.add(shopId);
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	
	@Override
	public Boolean checkExitsFocusProgramWithParentShop(Long focusProgramid,
			String shopCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT COUNT(1) AS COUNT");
		sql.append(" FROM focus_shop_map");
		sql.append(" WHERE focus_program_id = ?");
		params.add(focusProgramid);
		sql.append(" AND status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND shop_id               IN");
		sql.append(" (SELECT shop_id");
		sql.append(" FROM shop");
		sql.append(" START WITH shop_code      = ? ");
		params.add(shopCode);
		sql.append(" CONNECT BY prior parent_shop_id = shop_id)");
		sql.append(" AND shop_id <> (select shop_id from shop where shop_code = ?)");
		params.add(shopCode);
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}

	@Override
	public List<Shop> getListShopInFocusProgram(Long focusProgramId,
			String shopCode, String shopName, Long parentShopId, Boolean isJoin)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select distinct * from shop s start with shop_id in (");
		//sql.append("	(select ps.shop_id from focus_shop_map ps join shop sh on ps.shop_id = sh.shop_id where ps.status = 1 and focus_program_id = ?");
		
		if (Boolean.TRUE.equals(isJoin)) {
			sql.append(" select sh.shop_id from shop sh where sh.shop_id in (select shop_id from focus_shop_map where focus_program_id = ? and status = ?)");
		} else {
			sql.append(" select sh.shop_id from shop sh where sh.shop_id not in (select shop_id from focus_shop_map where focus_program_id = ? and status = ?)");
		}
		params.add(focusProgramId);
		params.add(ActiveType.RUNNING.getValue());
		
		if (parentShopId != null) {
			sql.append(" and sh.shop_id in (");
			sql.append("select shop_id from shop where status = ? start with shop_id = ? connect by prior shop_id = parent_shop_id and status = ?");
			params.add(ActiveType.RUNNING.getValue());
			params.add(parentShopId);
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" union all");
			sql.append(" select shop_id from shop where status = ? start with shop_id = ? connect by prior parent_shop_id = shop_id and status = ?");
			params.add(ActiveType.RUNNING.getValue());
			params.add(parentShopId);
			params.add(ActiveType.RUNNING.getValue());
			sql.append(")");
		}
		
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and upper(sh.shop_code) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(shopCode).toUpperCase());
		}
		
		if (!StringUtility.isNullOrEmpty(shopName)) {
			shopName = shopName.toUpperCase();
			sql.append(" and (upper(sh.shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shopName));
			sql.append(" or upper(sh.name_text) like ? escape '/' ) ");
			shopName = Unicode2English.codau2khongdau(shopName);
			shopName = StringUtility.toOracleSearchLike(shopName);
			params.add(shopName);
		//	params.add(StringUtility.toOracleSearchLike(shopName).toUpperCase());
		}
		sql.append("	) ");
		sql.append(" connect by prior parent_shop_id = shop_id and s.status = ?");
		params.add(ActiveType.RUNNING.getValue());

		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	/*@Override
	public List<QuantityVO> getListQuantityVO(List<Shop> lstShop, Long focusProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT shop_id shopId, ");
		sql.append("   ( ");
		sql.append("     SELECT promotion_shop_map_id FROM promotion_shop_map WHERE promotion_program_id = ? and status = 1 and shop_id = s.shop_id  ");
		params.add(focusProgramId);
		sql.append("   ) promotionShopMapId, ");
		sql.append("   (");
		sql.append("	select case object_type when 3 then 1 else 0 end from channel_type where channel_type_id = shop_type_id");
		sql.append("   ) isNPP ");
		sql.append(" FROM shop s ");
		sql.append(" WHERE shop_id IN  (-1");
		
		for (Shop s: lstShop) {
			sql.append(",?");
			params.add(s.getId());
		}
		
		sql.append(")");
		
		String[] fieldNames = { "shopId", "isNPP", "promotionShopMapId" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER, StandardBasicTypes.LONG };
		return repo.getListByQueryAndScalar(QuantityVO.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}*/
	
	@Override
	public List<ProgrameExtentVO> getListProgrameExtentVO(List<Shop> lstShop, Long focusProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT shop_id shopId, ");
		sql.append("   ( ");
		sql.append("     SELECT focus_shop_map_id FROM focus_shop_map WHERE focus_program_id = ? and status = 1 and shop_id = s.shop_id  ");
		params.add(focusProgramId);
		sql.append("   ) programShopMapId, ");
		sql.append("   (");
		sql.append("	select case specific_type when ? then 1 else 0 end from shop_type where shop_type_id = s.shop_type_id");
		params.add(ShopSpecificType.NPP.getValue());
		sql.append("   ) isNPP ");
		sql.append(" FROM shop s ");
		sql.append(" WHERE 1=1 ");
		
		String paramFix = StringUtility.getParamsIdFixBugTooLongParamsLstShop(lstShop, "shop_id");
		sql.append(paramFix);
		
		String[] fieldNames = {"shopId", "isNPP", "programShopMapId"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG};
		return repo.getListByQueryAndScalar(ProgrameExtentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<ProgrameExtentVO> getListProgrameExtentVOEx(List<Shop> lstShop, Long focusProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT shop_id shopId, ");
		sql.append("   (");
		sql.append("	select case object_type when 3 then 1 else 0 end from channel_type where channel_type_id = shop_type_id");
		sql.append("   ) isNPP, ");
		
		sql.append("   (");
		// sql.append("	select count(1) from focus_shop_map where focus_program_id = ? and shop_id = s.shop_id and status = 1");
		sql.append("	select count(1) from shop ss where exists (select 1 from focus_shop_map fsm where fsm.shop_id = ss.shop_id and fsm.status = 1 and fsm.focus_program_id = ?) START WITH ss.shop_id = s.shop_id CONNECT BY prior ss.shop_id = ss.parent_shop_id ");
		params.add(focusProgramId);
		sql.append("   ) isJoin ");
		
		sql.append(" FROM shop s ");
		sql.append(" WHERE status = 1 ");
		
		String paramFix = StringUtility.getParamsIdFixBugTooLongParamsLstShop(lstShop, "shop_id");
		sql.append(paramFix);
		
		String[] fieldNames = {"shopId", "isNPP", "isJoin"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER};
		return repo.getListByQueryAndScalar(ProgrameExtentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<FocusShopMap> getListFocusChildShopMapWithShopAndFocusProgram(
			Long shopId, Long focusProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from focus_shop_map where status = 1 and shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
		// sql.append(" and shop_id <> ?");
		// params.add(shopId);
		params.add(shopId);
		sql.append(" and focus_program_id = ?");
		params.add(focusProgramId);
		return repo.getListBySQL(FocusShopMap.class, sql.toString(), params);
	}
	
	@Override
	public Boolean isExistChildJoinProgram(String shopCode, Long focusProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from focus_shop_map f where status != -1");
		if(focusProgramId != null){
			sql.append(" and focus_program_id = ? ");
			params.add(focusProgramId);
		}
		sql.append(" and f.shop_id in (select shop_id from shop start with shop_code = ? connect by prior shop_id = parent_shop_id)");
		params.add(shopCode.toUpperCase());
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
}

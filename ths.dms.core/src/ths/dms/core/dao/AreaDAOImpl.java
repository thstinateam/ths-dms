package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Area;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AreaType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.AreaVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class AreaDAOImpl implements AreaDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Autowired
	CommonDAO commonDAO;

	@Override
	public Area createArea(Area area, LogInfoVO logInfo) throws DataAccessException {
		if (area == null) {
			throw new IllegalArgumentException("area");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (area.getAreaCode() != null)
			area.setAreaCode(area.getAreaCode().toUpperCase());
		if (area.getDistrict() != null)
			area.setDistrict(area.getDistrict().toUpperCase());
		if (area.getPrecinct() != null)
			area.setPrecinct(area.getPrecinct().toUpperCase());
		if (area.getProvince() != null)
			area.setProvince(area.getProvince().toUpperCase());
		
		
		area.setCreateUser(logInfo.getStaffCode());
		area.setCreateDate(commonDAO.getSysDate());
		if (area.getAreaName() != null) {
			area.setNameText(Unicode2English.codau2khongdau(area.getAreaName()).toUpperCase());
		}
		repo.create(area);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, area, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(Area.class, area.getId());
	}
	
//	@Override
//	public void createArea(List<Area> lstArea, LogInfoVO logInfo) throws DataAccessException {
//		repo.create(lstArea);
//	}

	@Override
	public void deleteArea(Area area, LogInfoVO logInfo) throws DataAccessException {
		if (area == null) {
			throw new IllegalArgumentException("area");
		}
		repo.delete(area);
		try {
			actionGeneralLogDAO.createActionGeneralLog(area, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Area getAreaById(long id) throws DataAccessException {
		return repo.getEntityById(Area.class, id);
	}
	
	@Override
	public void updateArea(Area area, LogInfoVO logInfo) throws DataAccessException {
		if (area == null) {
			throw new IllegalArgumentException("area");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (area.getAreaCode()!=null)
			area.setAreaCode(area.getAreaCode().toUpperCase());
		Area temp = this.getAreaById(area.getId());
		area.setUpdateDate(commonDAO.getSysDate());
		area.setUpdateUser(logInfo.getStaffCode());
		if (area.getAreaName() != null) {
			area.setNameText(Unicode2English.codau2khongdau(area.getAreaName()).toUpperCase());
		}
		repo.update(area);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, area, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public Area getAreaByCode(String code) throws DataAccessException{
		String sql = "select * from AREA where lower(area_code)=? and status!=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.trim().toLowerCase());
		params.add(ActiveType.DELETED.getValue());
		Area rs = repo.getEntityBySQL(Area.class, sql, params);
		return rs;
	}
	
	@Override
	public Area getArea(String provinceCode, 
			String districtCode, String precinctCode, 
			AreaType type, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from AREA a where 1 = 1");
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		}
		else {
			sql.append(" and status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(provinceCode)) {
			sql.append(" and upper(province) = ?");
			params.add(provinceCode.toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(districtCode)) {
			sql.append(" and upper(district) = ?");
			params.add(districtCode.toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(precinctCode)) {
			sql.append(" and upper(PRECINCT) = ?");
			params.add(precinctCode.toUpperCase());
		}
		if (type != null) {
			sql.append(" and type=?");
			params.add(type.getValue());
		}
		return repo.getFirstBySQL(Area.class, sql.toString(), params);
	}
	
	@Override
	public List<Area> getListArea(KPaging<Area> kPaging, String areaCode,
			String areaName, Long parentId, ActiveType status,
			String provinceCode, String provinceName, 
			String districtCode, String districtName, 
			String precinctCode, String precinctName, AreaType type,
			Boolean isGetOneChildLevel,String sort,String order) throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select a.* from AREA a ");
		//if ( "ar.area_name".equals(sort) ) {
		sql.append(" join  area ar on  a.parent_area_id = ar.area_id ");
		sql.append(" where 1 = 1");
		if (!StringUtility.isNullOrEmpty(areaCode)) {
			sql.append(" and a.area_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(areaCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(areaName)) {
			sql.append(" and lower(a.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(areaName.trim().toLowerCase())));
		}
		if (parentId != null) {
			if (Boolean.FALSE.equals(isGetOneChildLevel)) {
				sql.append(" and (a.parent_area_id=? or a.area_id = ?)");
				params.add(parentId);
			}
			else {
				sql.append(" and a.area_id in (select area_id from area start with area_id = ? connect by prior area_id = parent_area_id)");
			}
			params.add(parentId);
		}
		if (status != null) {
			sql.append(" and a.status=?");
			params.add(status.getValue());
		}
		else {
			sql.append(" and a.status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(provinceCode)) {
			sql.append(" and upper(a.province) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(provinceCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(districtCode)) {
			sql.append(" and upper(a.district) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(districtCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(precinctCode)) {
			sql.append(" and upper(a.PRECINCT) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(precinctCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(provinceName)) {
			sql.append(" and lower(a.province_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(provinceName.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(districtName)) {
			sql.append(" and lower(a.district_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(districtName.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(precinctName)) {
			sql.append(" and lower(a.PRECINCT_NAME) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(precinctName.toLowerCase()));
		}

		if (type != null) {
			sql.append(" and a.type=?");
			params.add(type.getValue());
		}
		String[] arrColumnAction = new String[] { "a.area_Code", "a.area_Name",
							"ar.area_code", "ar.area_name", "a.status" };
		sort = StringUtility.getColumnSort(sort, arrColumnAction);
		if (!StringUtility.isNullOrEmpty(sort)) {
			sql.append(" order by ").append(sort).append(" ");
			order = StringUtility.getOrderBy(order);
			if (!StringUtility.isNullOrEmpty(order)) {
				sql.append(order);
			}
		} else {
			sql.append(" order by a.area_code, a.parent_area_id ");
		}
		if (kPaging == null) {
			return repo.getListBySQL(Area.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(Area.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public Area getAreaEqual(String areaCode,
			String areaName, Long parentId, ActiveType status,
			String provinceCode, String provinceName, 
			String districtCode, String districtName, 
			String precinctCode, String precinctName, AreaType type,
			Boolean isGetOneChildLevel) throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from AREA a where 1 = 1");
		if (!StringUtility.isNullOrEmpty(areaCode)) {
			sql.append(" and area_code = ? ");
			params.add(areaCode.toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(areaName)) {
			sql.append(" and lower(area_name) = ? ");
			params.add(areaName.toLowerCase());
		}
		if (parentId != null) {
			if (Boolean.FALSE.equals(isGetOneChildLevel)) {
				sql.append(" and (parent_area_id=? or area_id = ?)");
				params.add(parentId);
			}
			else {
				sql.append(" and area_id in (select area_id from area start with area_id = ? connect by prior area_id = parent_area_id)");
			}
			params.add(parentId);
		}
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		}
		else {
			sql.append(" and status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(provinceCode)) {
			sql.append(" and upper(province) = ? ");
			params.add(provinceCode.toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(districtCode)) {
			sql.append(" and upper(district) = ? ");
			params.add(districtCode.toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(precinctCode)) {
			sql.append(" and upper(PRECINCT) = ? ");
			params.add(precinctCode.toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(provinceName)) {
			sql.append(" and lower(province_name) = ? ");
			params.add(provinceName.toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(districtName)) {
			sql.append(" and lower(district_name) = ? ");
			params.add(districtName.toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(precinctName)) {
			sql.append(" and lower(PRECINCT_NAME) = ? ");
			params.add(precinctName.toLowerCase());
		}

		if (type != null) {
			sql.append(" and type=?");
			params.add(type.getValue());
		}
		return repo.getFirstBySQL(Area.class, sql.toString(), params);
	}
	
	@Override
	public Boolean isUsingByOthers(long areaId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(1) as count from Area ");
		sql.append(" where exists(select 1 from area where parent_area_id=?)");
		sql.append("	or exists(select 1 from Staff where area_id=? and status!=?)");
		sql.append("	or exists(select 1 from CUSTOMER where area_id=? and status!=?)");
		List<Object> params = new ArrayList<Object>();
		params.add(areaId);
		params.add(areaId);
		params.add(ActiveType.DELETED.getValue());
		params.add(areaId);
		params.add(ActiveType.DELETED.getValue());
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}
	
	@Override
	public List<Area> getListSubArea(Long parentId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (parentId != null) {
			sql.append("select * from AREA where parent_area_id=? and status !=?");
			params.add(parentId);
		}
		else {
			sql.append("select * from AREA where parent_area_id is null and status !=?");
		}
		params.add(ActiveType.DELETED.getValue());
		sql.append(" order by NLSSORT(area_name,'NLS_SORT=vietnamese')");
		return repo.getListBySQL(Area.class, sql.toString(), params);
	}
	
	@Override
	public List<Area> getListSubArea(Long parentId, ActiveType activeType, Long exceptionalAreaId, Boolean isOrderByCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (parentId != null) {
			sql.append("select * from AREA where parent_area_id=? and status =?");
			params.add(parentId);
		}
		else {
			sql.append("select * from AREA where parent_area_id is null and status =?");
		}
		params.add(activeType.getValue());
		if (exceptionalAreaId != null) {
			sql.append(" and area_id != ?");
			params.add(exceptionalAreaId);
		}
		if (Boolean.TRUE.equals(isOrderByCode)) {
			sql.append(" order by area_code");
		}
		else {
			sql.append(" order by NLSSORT(area_name,'NLS_SORT=vietnamese')");
		}
		return repo.getListBySQL(Area.class, sql.toString(), params);
	}
	
	@Override
	public Boolean checkIfAreaHasAnyChildStopped(Long areaId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select count(1) as count from area ");		
		sql.append(" start with parent_area_id=? and status=?");
		params.add(areaId);
		params.add(ActiveType.STOPPED.getValue());

		sql.append(" connect by prior area_id=parent_area_id");
		
		int c = repo.countBySQL(sql.toString(), params);
		return c>0?true:false;
	}
	
	@Override
	public Boolean checkIfAreaHasAllChildStopped(Long areaId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select count(1) as count from area ");		
		sql.append(" start with parent_area_id=? and status=?");
		params.add(areaId);
		params.add(ActiveType.RUNNING.getValue());

		sql.append(" connect by prior area_id=parent_area_id");
		
		int c = repo.countBySQL(sql.toString(), params);
		return c>0?false:true;
	}

	@Override
	public List<Area> getListAreaFull() throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from area where status=1 and province is not null and district is not null and precinct is not null and status =? order by Province,District,Precinct");
		params.add(ActiveType.RUNNING.getValue());
		return repo.getListBySQL(Area.class, sql.toString(), params);
	}

	@Override
	public Area getAreaByPrecinct(String code) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from AREA where precinct=? and status!=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		Area rs = repo.getEntityBySQL(Area.class, sql, params);
		return rs;
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<AreaVO> getListAreaByType(Long parentId, Integer type, Integer status)
			throws DataAccessException {
		if (type == null) {
			throw new IllegalArgumentException("invalid type");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select area_id as id, area_code as areaCode,");
		sql.append(" area_name as areaName, type as type, center_code as centerCode");
		sql.append(" from area where type = ?");
		params.add(type);
		
		if (parentId != null) {
			sql.append(" and parent_area_id = ?");
			params.add(parentId);
		}
		if (status != null) {
			sql.append(" and status = ?");
			params.add(status);
		} else {
			sql.append(" and status <> -1");
		}
		
		String fieldNames[] = new String[] {
				"id", "areaCode",
				"areaName", "type", "centerCode"
		};
		
		Type fieldTypes[] = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING
		};
		
		return repo.getListByQueryAndScalar(AreaVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<AreaVO> retrieveAreaWithParentAreaInfo(ActiveType status)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select ");
		sql.append("     a.area_code areaCode, ");
		sql.append("     a.area_name areaName, ");
		sql.append("     a.precinct precinctCode, ");
		sql.append("     a.precinct_name precinctName, ");
		sql.append("     a.district districtCode, ");
		sql.append("     a.district_name districtName, ");
		sql.append("     a.province provinceCode, ");
		sql.append("     a.province_name provinceName, ");
		sql.append("     (select area_code from area where area_id = province.parent_area_id) countryCode, ");
		sql.append("     (select area_name from area where area_id = province.parent_area_id) countryName, ");
		sql.append("     a.status ");
		sql.append(" from area a ");
		sql.append(" join area province on a.province = province.area_code ");
		sql.append(" where 1=1 ");
		sql.append(" and a.type = ? ");
		params.add(AreaType.WARD.getValue());
		if (status != null) {
			sql.append(" and a.status = ? ");
			params.add(status.getValue());			
		}
		
		String fieldNames[] = new String[] {
				"areaCode", "areaName", 
				"precinctCode", "precinctName",
				"districtCode", "districtName",
				"provinceCode", "provinceName",
				"countryCode", "countryName",
				"status"
		};
		
		Type fieldTypes[] = new Type[] {
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER
		};
		
		return repo.getListByQueryAndScalar(AreaVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
}

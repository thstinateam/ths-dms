package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.StockTransLot;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.StockTransLotVO;
import ths.dms.core.exceptions.DataAccessException;

public interface StockTransLotDAO {

	StockTransLot createStockTransLot(StockTransLot stockTransLot) throws DataAccessException;

	void deleteStockTransLot(StockTransLot stockTransLot) throws DataAccessException;

	void updateStockTransLot(StockTransLot stockTransLot) throws DataAccessException;
	
	StockTransLot getStockTransLotById(long id) throws DataAccessException;

	List<StockTransLot> getListStockTransLotByStockTransId(
			KPaging<StockTransLot> kPaging, long stockTransId) throws DataAccessException;

	List<StockTransLotVO> getListStockTransLotVO(
			KPaging<StockTransLotVO> kPaging, long stockTransId)
			throws DataAccessException;
	
	/**
	 * @author trietptm
	 * @param kPaging
	 * @param stockTransId
	 * @return Danh sach san pham dieu chinh cho man hinh SearchSaleTransaction
	 * @throws DataAccessException
	 */
	List<StockTransLotVO> getListStockTransLotVOForSearchSale(KPaging<StockTransLotVO> kPaging, long stockTransId) throws DataAccessException;
	
	List<StockTransLotVO> getListLot(Long fromOwnerId,
			StockObjectType fromOwnerType, Long toOwnerId,
			StockObjectType toOwnerType, Date stockTransDate)
			throws DataAccessException;

	/**
	 * @author tungmt
	 * @since 13/5/2015
	 * @param kPaging
	 * @param stockTransId
	 * @return Danh sach san pham dieu chinh cho man hinh nhap xuat kho dieu chinh
	 * @throws DataAccessException
	 */
	List<StockTransLotVO> getListStockTransLotVOForStockUpdate(KPaging<StockTransLotVO> kPaging, long stockTransId) throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Organization;
import ths.dms.core.entities.ShopType;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrganizationFilter;
import ths.dms.core.entities.enumtype.OrganizationNodeType;
import ths.dms.core.entities.enumtype.OrganizationUnitTypeFilter;
import ths.dms.core.entities.filter.OrganizationSystemFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.OrganizationUnitTypeVO;
import ths.dms.core.entities.vo.OrganizationVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;


/**
 * The Interface OrganizationDAOImpl.
 * @author vuongmq
 * @date 11/02/2015
 */
public class OrganizationDAOImpl  implements OrganizationDAO {
	/**BEGIN VUONGMQ*/
	
	@Override
	public Organization getOrganizationById(Long id) throws DataAccessException {
		return repo.getEntityById(Organization.class, id);
	}

	@Override
	public List<Organization> getListOrganization( OrganizationSystemFilter<Organization> filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select org.* from organization org ");
		return repo.getListBySQL(Organization.class, sql.toString(), params);
	}


	@Override
	public List<Organization> getListOrganizationParent( OrganizationSystemFilter<Organization> filter) throws DataAccessException {
		if(filter == null || (filter != null && filter.getId() == null)){
			return null;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select org.* from organization org ");
		sql.append(" start with org.organization_id = ? ");
		params.add(filter.getId());
		sql.append(" and org.status = 1 ");
		sql.append(" connect by prior org.organization_id = org.parent_org_id ");
		return repo.getListBySQL(Organization.class, sql.toString(), params);
	}
	@Override
	public OrganizationVO getOrganizationVOById(Long Id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with tmpShop as ( ");
		sql.append(" select org.organization_id as id, ");
		sql.append(" org.parent_org_id as parentOrgId, ");
		sql.append(" sht.prefix as code, ");
		sql.append(" sht.name as name, ");
		sql.append(" sht.description as description, ");
		sql.append(" sht.icon_url as iconUrl, ");
		sql.append(" sht.shop_type_id as typeId, ");
		sql.append(" org.node_type as nodeType, ");
		sql.append(" org.node_ordinal as nodeOrdinal, ");
		sql.append(" org.status as status, ");
		sql.append(" org.IS_MANAGE as isManage ");
		sql.append(" from organization org ");
		sql.append(" left join shop_type sht on sht.shop_type_id = org.node_type_id ");
		sql.append(" where org.node_type = 1 ");
		sql.append(" and org.status = 1 ");
		sql.append(" and sht.status = 1 ");
		sql.append(" and org.organization_id = ? ");
		params.add(Id);
		sql.append(" ), tmpStaff as ( ");
		sql.append(" select org.organization_id as id, ");
		sql.append(" org.parent_org_id as parentOrgId, ");
		sql.append(" st.prefix as code, ");
		sql.append(" st.name as name, ");
		sql.append(" st.description as description, ");
		sql.append(" st.icon_url as iconUrl, ");
		sql.append(" st.staff_type_id as typeId, ");
		sql.append(" org.node_type as nodeType, ");
		sql.append(" org.node_ordinal as nodeOrdinal, ");
		sql.append(" org.status as status, ");
		sql.append(" org.IS_MANAGE as isManage ");
		sql.append(" from organization org ");
		sql.append(" left join staff_type st on st.staff_type_id = org.node_type_id ");
		sql.append(" where org.node_type = 2 ");
		sql.append(" and org.status = 1 ");
		sql.append(" and st.status = 1 ");
		sql.append(" and org.organization_id = ? ");
		params.add(Id);
		sql.append(" ) ");
		sql.append(" select * from tmpshop ");
		sql.append(" union ");
		sql.append(" select * from tmpstaff ");
		String[] fieldNames = {"id", "parentOrgId",
								"code", "name",
								"description", "iconUrl",
								"typeId","nodeType",
								"nodeOrdinal", "status","isManage"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.LONG,
				 			 StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				 			 StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				 			 StandardBasicTypes.LONG, StandardBasicTypes.INTEGER,
				 			 StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER};
		List<OrganizationVO> lstVo = repo.getListByQueryAndScalar(OrganizationVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return (lstVo !=null && lstVo.size() > 0) ? lstVo.get(0): null;
	}

	@Override
	public List<OrganizationVO> getListOrganizationSystemVO( OrganizationSystemFilter<OrganizationVO> filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with tmpShop as ( ");
		sql.append(" select org.organization_id as id, ");
		sql.append(" org.parent_org_id as parentOrgId, ");
		sql.append(" sht.prefix as code, ");
		sql.append(" sht.name as name, ");
		sql.append(" sht.description as description, ");
		sql.append(" sht.icon_url as iconUrl, ");
		sql.append(" sht.shop_type_id as typeId, ");
		sql.append(" org.node_type as nodeType, ");
		sql.append(" org.node_ordinal as nodeOrdinal, ");
		sql.append(" org.status as status, ");
		sql.append(" org.IS_MANAGE as isManage ");
		sql.append(" from organization org ");
		sql.append(" left join shop_type sht on sht.shop_type_id = org.node_type_id ");
		sql.append(" where org.node_type = 1 ");
		sql.append(" and org.status = 1 ");
		sql.append(" and sht.status = 1 ");
		sql.append(" start with org.organization_id = 2 ");
		sql.append(" connect by prior org.organization_id = org.parent_org_id ");
		sql.append(" order by org.node_ordinal ");
		sql.append(" ), tmpStaff as ( ");
		sql.append(" select org.organization_id as id, ");
		sql.append(" org.parent_org_id as parentOrgId, ");
		sql.append(" st.prefix as code, ");
		sql.append(" st.name as name, ");
		sql.append(" st.description as description, ");
		sql.append(" st.icon_url as iconUrl, ");
		sql.append(" st.staff_type_id as typeId, ");
		sql.append(" org.node_type as nodeType, ");
		sql.append(" org.node_ordinal as nodeOrdinal, ");
		sql.append(" org.status as status, ");
		sql.append(" org.IS_MANAGE as isManage ");
		sql.append(" from organization org ");
		sql.append(" left join staff_type st on st.staff_type_id = org.node_type_id ");
		sql.append(" where org.node_type = 2 ");
		sql.append(" and org.status = 1 ");
		sql.append(" and st.status = 1 ");
		sql.append(" start with org.organization_id = 2 ");
		sql.append(" connect by prior org.organization_id = org.parent_org_id ");
		sql.append(" order by org.node_ordinal ");
		sql.append(" ) ");
		sql.append(" select * from tmpshop ");
		sql.append(" union ");
		sql.append(" select * from tmpstaff ");
		String[] fieldNames = {"id", "parentOrgId",
				"code", "name",
				"description", "iconUrl",
				"typeId","nodeType",
				"nodeOrdinal", "status","isManage"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.LONG,
 			 StandardBasicTypes.STRING, StandardBasicTypes.STRING,
 			 StandardBasicTypes.STRING, StandardBasicTypes.STRING,
 			 StandardBasicTypes.LONG, StandardBasicTypes.INTEGER,
 			 StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER};
		return repo.getListByQueryAndScalar(OrganizationVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public OrganizationVO getOrganizationVOByParent() throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with tmpShop as ( ");
		sql.append(" select org.organization_id as id, ");
		sql.append(" org.parent_org_id as parentOrgId, ");
		sql.append(" sht.prefix as code, ");
		sql.append(" sht.name as name, ");
		sql.append(" sht.description as description, ");
		sql.append(" sht.icon_url as iconUrl, ");
		sql.append(" sht.shop_type_id as typeId, ");
		sql.append(" org.node_type as nodeType, ");
		sql.append(" org.node_ordinal as nodeOrdinal, ");
		sql.append(" org.status as status, ");
		sql.append(" org.IS_MANAGE as isManage ");
		sql.append(" from organization org ");
		sql.append(" left join shop_type sht on sht.shop_type_id = org.node_type_id ");
		sql.append(" where org.node_type = 1 ");
		sql.append(" and org.status = 1 ");
		sql.append(" and sht.status = 1 ");
		sql.append(" and org.parent_org_id is null ");
		sql.append(" ), tmpStaff as ( ");
		sql.append(" select org.organization_id as id, ");
		sql.append(" org.parent_org_id as parentOrgId, ");
		sql.append(" st.prefix as code, ");
		sql.append(" st.name as name, ");
		sql.append(" st.description as description, ");
		sql.append(" st.icon_url as iconUrl, ");
		sql.append(" st.staff_type_id as typeId, ");
		sql.append(" org.node_type as nodeType, ");
		sql.append(" org.node_ordinal as nodeOrdinal, ");
		sql.append(" org.status as status, ");
		sql.append(" org.IS_MANAGE as isManage ");
		sql.append(" from organization org ");
		sql.append(" left join staff_type st on st.staff_type_id = org.node_type_id ");
		sql.append(" where org.node_type = 2 ");
		sql.append(" and org.status = 1 ");
		sql.append(" and st.status = 1 ");
		sql.append(" and org.parent_org_id is null ");
		sql.append(" ) ");
		sql.append(" select * from tmpshop ");
		sql.append(" union ");
		sql.append(" select * from tmpstaff ");
		String[] fieldNames = {"id", "parentOrgId",
				"code", "name",
				"description", "iconUrl",
				"typeId","nodeType",
				"nodeOrdinal","status","isManage"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.LONG,
 			 StandardBasicTypes.STRING, StandardBasicTypes.STRING,
 			 StandardBasicTypes.STRING, StandardBasicTypes.STRING,
 			 StandardBasicTypes.LONG, StandardBasicTypes.INTEGER,
 			 StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER};
		List<OrganizationVO> lstVo = repo.getListByQueryAndScalar(OrganizationVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return (lstVo !=null && lstVo.size() > 0) ? lstVo.get(0): null;
	}
	
	@Override
	public List<OrganizationVO> getListOrganizationSystemVOShop(OrganizationSystemFilter<OrganizationVO> filter)
			throws DataAccessException {
		if(filter == null || (filter != null && filter.getId() == null)){
			return null;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select org.organization_id as id, ");
		sql.append(" org.parent_org_id as parentOrgId, ");
		sql.append(" sht.prefix as code, ");
		sql.append(" sht.name as name, ");
		sql.append(" sht.description as description, ");
		sql.append(" sht.icon_url as iconUrl, ");
		sql.append(" sht.shop_type_id as typeId, ");
		sql.append(" org.node_type as nodeType, ");
		sql.append(" org.node_ordinal as nodeOrdinal, ");
		sql.append(" org.status as status, ");
		sql.append(" org.IS_MANAGE as isManage ");
		sql.append(" from organization org ");
		sql.append(" left join shop_type sht on sht.shop_type_id = org.node_type_id ");
		sql.append(" where 1= 1 ");
		sql.append(" and org.parent_org_id = ? ");
		params.add(filter.getId());
		sql.append(" and org.node_type = 1 ");
		sql.append(" and org.status = 1 ");
		sql.append(" and sht.status = 1 ");
		sql.append(" order by org.node_ordinal ");
		/*sql.append(" where org.node_type = 1 ");
		sql.append(" and org.status = 1 ");
		sql.append(" and sht.status = 1 ");
		sql.append(" and org.organization_id <> ? ");
		params.add(filter.getId());
		sql.append(" start with org.organization_id = ? ");
		params.add(filter.getId());
		sql.append(" connect by prior org.organization_id = org.parent_org_id ");
		sql.append(" order by org.node_ordinal ");*/
		String[] fieldNames = {"id", "parentOrgId",
				"code", "name",
				"description", "iconUrl",
				"typeId","nodeType",
				"nodeOrdinal","status","isManage"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.LONG,
 			 StandardBasicTypes.STRING, StandardBasicTypes.STRING,
 			 StandardBasicTypes.STRING, StandardBasicTypes.STRING,
 			 StandardBasicTypes.LONG, StandardBasicTypes.INTEGER,
 			 StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER};
		return repo.getListByQueryAndScalar(OrganizationVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<OrganizationVO> getListOrganizationSystemVOStaff(OrganizationSystemFilter<OrganizationVO> filter)
			throws DataAccessException {
		if(filter == null || (filter != null && filter.getId() == null)){
			return null;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select org.organization_id as id, ");
		sql.append(" org.parent_org_id as parentOrgId, ");
		sql.append(" st.prefix as code, ");
		sql.append(" st.name as name, ");
		sql.append(" st.description as description, ");
		sql.append(" st.icon_url as iconUrl, ");
		sql.append(" st.staff_type_id as typeId, ");
		sql.append(" org.node_type as nodeType, ");
		sql.append(" org.node_ordinal as nodeOrdinal, ");
		sql.append(" org.status as status, ");
		sql.append(" org.IS_MANAGE as isManage ");
		sql.append(" from organization org ");
		sql.append(" left join staff_type st on st.staff_type_id = org.node_type_id ");
		sql.append(" where 1= 1 ");
		sql.append(" and org.parent_org_id = ? ");
		params.add(filter.getId());
		sql.append(" and org.node_type = 2 ");
		sql.append(" and org.status =1 ");
		sql.append(" and st.status = 1 ");
		sql.append(" order by org.node_ordinal ");
		String[] fieldNames = {"id", "parentOrgId",
				"code", "name",
				"description", "iconUrl",
				"typeId","nodeType",
				"nodeOrdinal","status","isManage"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.LONG,
 			 StandardBasicTypes.STRING, StandardBasicTypes.STRING,
 			 StandardBasicTypes.STRING, StandardBasicTypes.STRING,
 			 StandardBasicTypes.LONG, StandardBasicTypes.INTEGER,
 			 StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER};
		return repo.getListByQueryAndScalar(OrganizationVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	@Override
	public List<StaffType> getListStaffType(OrganizationSystemFilter<StaffType> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from staff_type sty ");
		sql.append(" where 1 = 1 ");
		sql.append(" and sty.status = 1 ");
		sql.append(" and sty.staff_type_id in (select distinct node_type_id from organization where node_type = 2 and status = 1)  ");
		return repo.getListBySQL(StaffType.class, sql.toString(), params);
	}
	/**END VUONGMQ*/
	
	/**BEGIN PHUOCDH2*/
	@Autowired 
	private IRepository repo;
	
	@Autowired
    ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public Organization createOrganization(Organization organization, LogInfoVO logInfo) throws DataAccessException {
		if (organization == null) {
			throw new IllegalArgumentException("Organization is null");
		}
		organization.setCreateDate(commonDAO.getSysDate());
		if (logInfo != null) {
			organization.setCreateUser(logInfo.getStaffCode());
		}
		Organization result = repo.create(organization);
		return result;
	}
	
	@Override
	public void deleteOrganization(Organization organization, LogInfoVO logInfo) throws DataAccessException {
		if (organization == null) {
			throw new IllegalArgumentException("Organization is null");
		}
		if(logInfo != null){
			try {
				actionGeneralLogDAO.createActionGeneralLog(null, organization, logInfo);
			}
			catch (Exception ex) {
				throw new DataAccessException(ex);
			}
		}
		repo.delete(organization);
	}

	@Override
	public Organization getOrganizationById(long id) throws DataAccessException {
		return repo.getEntityById(Organization.class, id);
	}
	@Override
	public Integer getCountByPrefixShop(String prefix) throws DataAccessException {
		String sql = "select count(*) from shop_type where 1 = 1 and status = 1 and upper(name)= ? ";
		List<Object> params = new ArrayList<Object>();
		if(prefix != null){
			params.add(prefix.toUpperCase());
		}
		Object obj = repo.getObjectByQuery(sql, params);
		if (obj == null)
			return 0;
		return Integer.parseInt(obj.toString());
	}
	@Override
	public Integer getCountByPrefixStaff(String prefix) throws DataAccessException {
		String sql = "select count(*) from staff_type where 1 = 1 and status = 1 and upper(name)= ? ";
		List<Object> params = new ArrayList<Object>();
		if(prefix != null){
			params.add(prefix.toUpperCase());
		}
		Object obj = repo.getObjectByQuery(sql, params);
		if (obj == null)
			return 0;
		return Integer.parseInt(obj.toString());
	}
	@Override
	public void updateOrganization(Organization organization, LogInfoVO logInfo) throws DataAccessException {
		if (organization == null) {
			throw new IllegalArgumentException("Organization is null");
		}
		organization.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
//			organization.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(organization);
	}
	@Override
	public ShopType getShopTypeById(Long id) throws DataAccessException {
		return repo.getEntityById(ShopType.class, id);
	}
	@Override
	public List<Organization> getListOrganizationByFilter(OrganizationFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from organization where 1 = 1");
		if(filter.getStatus()!=null){
			sql.append(" and status = ?");
			params.add(filter.getStatus());
		}
		if(filter.getNodeType()!=null){
			sql.append(" and node_type = ?");
			params.add(filter.getNodeType());
		}
		if(filter.getNodeTypeId()!=null){
			sql.append(" and node_type_id =? ");
			params.add(filter.getNodeTypeId());
		}
		if(filter.getNodeOrdinal()!=null){
			sql.append(" and node_ordinal = ? ");
			params.add(filter.getNodeOrdinal());
		}
		if(filter.getOrganizationId()!=null){
			sql.append(" and organization_id = ? ");
			params.add(filter.getOrganizationId());
		}
		if(filter.getParentOrgId()!=null){
			sql.append(" and parent_org_id = ? ");
			params.add(filter.getParentOrgId());
		}
		return repo.getListBySQL(Organization.class, sql.toString(), params);
	}
	@Override
	public List<OrganizationVO> getListOrganizationById4ContextMenu(Long orgId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from ( ");
		sql.append(" SELECT org.organization_id id ");
		sql.append(" ,org.node_type_id typeId ");
		sql.append(" ,org.node_type nodeType ");
		sql.append(" ,org.node_ordinal nodeOrdinal ");
		sql.append(" ,s.name name ");
		sql.append(" ,s.icon_url iconUrl ");
		sql.append(" FROM organization org ");
		sql.append(" join shop_type s on s.shop_type_id = org.node_type_id and s.status = 1 ");
		sql.append(" WHERE org.parent_org_id = ? ");
		params.add(orgId);
		sql.append(" and org.status = 1 ");
		sql.append(" and node_type = ? ");
		params.add(OrganizationNodeType.SHOP.getValue());
		sql.append(" union ");
		sql.append(" SELECT org.organization_id id ");
		sql.append(" ,org.node_type_id typeId ");
		sql.append(" ,org.node_type nodeType ");
		sql.append(" ,org.node_ordinal nodeOrdinal ");
		sql.append(" ,s.name name ");
		sql.append(" ,s.icon_url iconUrl ");
		sql.append(" FROM organization org ");
		sql.append(" join staff_type s on s.staff_type_id = org.node_type_id and s.status = 1 ");
		sql.append(" WHERE org.parent_org_id = ? ");
		params.add(orgId);
		sql.append(" and org.status = 1 ");
		sql.append(" and node_type = ? ");
		params.add(OrganizationNodeType.STAFF.getValue());
		sql.append(" ) ORDER BY nodeOrdinal,name");
		String[] fieldNames = { 
				"id" //1
				,"typeId" //2
				,"nodeType" //3
				,"nodeOrdinal" //4
				,"name" //5
				,"iconUrl" //6
		};
		Type[] fieldTypes = { 
				 StandardBasicTypes.LONG //1
				,StandardBasicTypes.LONG //2
				,StandardBasicTypes.INTEGER //3
				,StandardBasicTypes.INTEGER //4
				,StandardBasicTypes.STRING //5
				,StandardBasicTypes.STRING //6
		};
		return repo.getListByQueryAndScalar(OrganizationVO.class, fieldNames,fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<Staff> getListStaffExitsStaffTypeIdInOrganization(Long orgId,Long staffId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from staff where 1 = 1");
		sql.append(" and status = 1 ");
		sql.append(" and staff_id = ? "); //staff_id chon tren cay
		params.add(staffId);
		sql.append(" and staff_type_id in ( ");
		sql.append(" select node_type_id ");
		sql.append(" from organization ");
		sql.append(" where node_type = ? ");
		params.add(OrganizationNodeType.STAFF.getValue());
		sql.append(" and parent_org_id = ? "); //--- org_id don vi dich
		params.add(orgId);
		sql.append(" )");
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}
	
	
	@Override
	public Integer getMaxNodeOrdinal() throws DataAccessException {
		//String sql = "select max(node_ordinal) from organization where 1 = 1 and status = ? and organization_id = ?";
		String sql = " select max(node_ordinal) from organization ";
		List<Object> params = new ArrayList<Object>();
		Object obj = repo.getObjectByQuery(sql, params);
		if (obj == null){
			return 0;
		}
		return Integer.parseInt(obj.toString());	
	}
	
	@Override
	public List<OrganizationUnitTypeVO> getListOrganizationUnitType(OrganizationUnitTypeFilter filter,KPaging<OrganizationUnitTypeVO> kPaging)throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String[] fieldNames = { 
				"nodeTypeId" //1
				,"prefix" //2
				,"name" //3
				,"iconUrl" //4
				,"description" //5
		};
		if(filter.getNodeType()!=null){
			if (OrganizationNodeType.SHOP.getValue().equals(filter.getNodeType().getValue())) {
				sql.append(" select st.shop_type_id nodeTypeId  ");
				sql.append(" ,st.prefix as prefix ");
				sql.append(" ,st.name as name ");
				sql.append(" ,ST.ICON_URL as iconUrl ");
				sql.append(" ,ST.DESCRIPTION as description ");
				sql.append(" from shop_type st ");
				sql.append(" where 1= 1 ");
				sql.append(" and st.status = ? ");
				params.add(ActiveType.RUNNING.getValue());
				if(!StringUtility.isNullOrEmpty(filter.getCodeOrName())){
					sql.append(" and upper(ST.NAME_TEXT) like ? ESCAPE '/' ");
					params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getCodeOrName()).toUpperCase()));
					//params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCodeOrName().toUpperCase()));
				}
				String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
				if (!StringUtility.isNullOrEmpty(sort)) {
					sql.append(" order by ").append(sort).append(" ");
					String order = StringUtility.getOrderBy(filter.getOrder());
					if (!StringUtility.isNullOrEmpty(order)) {
						sql.append(order);
					}
				} else {
					sql.append(" order by st.name ");
				}
			} else {
				sql.append(" select st.staff_type_id nodeTypeId  ");
				sql.append(" ,st.prefix as prefix ");
				sql.append(" ,st.name as name ");
				sql.append(" ,ST.ICON_URL as iconUrl ");
				sql.append(" ,ST.DESCRIPTION as description ");
				sql.append(" from staff_type st ");
				sql.append(" where 1= 1 ");
				sql.append(" and st.status = ? ");
				params.add(ActiveType.RUNNING.getValue());
				if(!StringUtility.isNullOrEmpty(filter.getCodeOrName())){
					sql.append(" and upper(ST.NAME_TEXT) like ? ESCAPE '/' ");
					params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getCodeOrName()).toUpperCase()));
				}
				String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
				if (!StringUtility.isNullOrEmpty(sort)) {
					sql.append(" order by ").append(sort).append(" ");
					String order = StringUtility.getOrderBy(filter.getOrder());
					if (!StringUtility.isNullOrEmpty(order)) {
						sql.append(order);
					}
				} else {
					sql.append(" order by st.name ");
				}
			}
		}
		// Count total
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		
		Type[] fieldTypes = { 
				 StandardBasicTypes.LONG //1
				,StandardBasicTypes.STRING //2
				,StandardBasicTypes.STRING //3
				,StandardBasicTypes.STRING //4
				,StandardBasicTypes.STRING //5
				
		};
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(OrganizationUnitTypeVO.class, fieldNames,fieldTypes, sql.toString(), params);
		}
		return repo.getListByQueryAndScalarPaginated(OrganizationUnitTypeVO.class,fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}
	
	
	//// new function
	@Override
	public StaffType getStaffTypeById(Long id) throws DataAccessException {
		return repo.getEntityById(StaffType.class, id);
	}
	/**
	 * kiem tra loai don vi, chuc vu nay da co tren node cha hay chua 
	 * @author phuocdh2
	 * @since 03/05/2015
	 * @description : nhan vao loai don vi ,chuc vu va kiem tra loai don vi chuc vu nay da co tren node cha chua
	 */
	@Override
	public List<Organization> getListOrgExistParentNodeByFilter(OrganizationFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from organization where 1 = 1");
		if (filter.getStatus() != null) {
			sql.append(" and status = ?");
			params.add(filter.getStatus());
		}
		if (filter.getNodeType() != null) {
			sql.append(" and node_type = ?");
			params.add(filter.getNodeType());
		}
		if (filter.getNodeTypeId() != null) {
			sql.append(" and node_type_id =? ");
			params.add(filter.getNodeTypeId());
		}
		if (filter.getNodeOrdinal() != null  ) {
			sql.append(" and node_ordinal = ? ");
			params.add(filter.getNodeOrdinal());
		}
		if (filter.getOrganizationId() != null && filter.getNodeType().equals(OrganizationNodeType.SHOP.getValue())) {
			sql.append(" and organization_id in (select t.organization_id from organization t start with t.organization_id = ? connect by prior t.parent_org_id =t.organization_id  and t.status =1)  ");
			params.add(filter.getOrganizationId());
		}
		if (filter.getParentOrgId() != null && filter.getNodeType().equals(OrganizationNodeType.STAFF.getValue())) {
			sql.append(" and parent_org_id in (select t.organization_id from organization t start with t.organization_id = ? connect by prior t.parent_org_id =t.organization_id  and t.status =1)  ");
			params.add(filter.getParentOrgId());
		}
		return repo.getListBySQL(Organization.class, sql.toString(), params);
	}
	/**
	 * kiem tra loai don vi, chuc vu nay da co duoi node con hay chua 
	 * @author phuocdh2
	 * @since 03/05/2015
	 * @description : nhan vao loai don vi ,chuc vu va kiem tra loai don vi chuc vu nay da co duoi node con chua
	 */
	@Override
	public List<Organization> getListOrgExistSubNodeByFilter(OrganizationFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from organization where 1 = 1");
		if (filter.getStatus() != null) {
			sql.append(" and status = ?");
			params.add(filter.getStatus());
		}
		if (filter.getNodeType() != null) {
			sql.append(" and node_type = ?");
			params.add(filter.getNodeType());
		}
		if (filter.getNodeTypeId()!= null){
			sql.append(" and node_type_id =? ");
			params.add(filter.getNodeTypeId());
		}
		if (filter.getNodeOrdinal() != null){
			sql.append(" and node_ordinal = ? ");
			params.add(filter.getNodeOrdinal());
		}
		if (filter.getOrganizationId()!= null && filter.getNodeType().equals(OrganizationNodeType.SHOP.getValue()) ) {
			sql.append(" and organization_id in (select t.organization_id from organization t start with t.organization_id = ? connect by prior t.organization_id =  t.parent_org_id  and t.status =1) ");
			params.add(filter.getOrganizationId());
		}
		if (filter.getParentOrgId() != null && filter.getNodeType().equals(OrganizationNodeType.STAFF.getValue()) ){
			sql.append(" and parent_org_id in (select t.organization_id from organization t start with t.organization_id = ? connect by prior t.organization_id =  t.parent_org_id  and t.status =1) ");
			params.add(filter.getParentOrgId());
		}
		return repo.getListBySQL(Organization.class, sql.toString(), params);
	}
	/**
	 * Lay organization tuong ung voi cac dieu kien trong staff_type
	 * @author phuocdh2
	 * @since 03/26/2015
	 * @description : nhan vao staff_code tra ra organization ung voi staff code do
	 */
	@Override
	public Organization getOrgJoinTypeByFilter(OrganizationFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select org.* from organization org ");
		if( filter.getNodeType() == 1){ // don vi : 1 , loai nhan vien : 2
			sql.append(" , shop_type st ");
			sql.append(" where 1 = 1 ");
			sql.append(" and org.node_type_id = st.shop_type_id  ");
		} else {
			sql.append(" , staff_type st ");
			sql.append(" where 1 = 1 "); 
			sql.append(" and org.node_type_id = st.staff_type_id ");
		}
		if ( filter.getStatus() != null ) {
			sql.append(" and org.status = ?");
			params.add(filter.getStatus());
		}
		if ( filter.getNodeType() != null ) {
			sql.append(" and org.node_type = ?");
			params.add(filter.getNodeType());
		}
		if ( filter.getNodeTypeId() != null ) {
			sql.append(" and org.node_type_id =? ");
			params.add(filter.getNodeTypeId());
		}
		if ( filter.getNodeOrdinal() != null ){
			sql.append(" and org.node_ordinal = ? ");
			params.add(filter.getNodeOrdinal());
		}
		if ( filter.getOrganizationId() != null ) {
			sql.append(" and org.organization_id = ? ");
			params.add(filter.getOrganizationId());
		}
		if (filter.getParentOrgId() != null ) {
			sql.append(" and org.parent_org_id = ? ");
			params.add(filter.getParentOrgId());
		}
		if ( filter.getTypeName() != null ) {
			sql.append(" and upper(st.name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getTypeName().toUpperCase()));
		}
		if ( filter.getTypeCode() != null ) {
			sql.append(" and upper(st.prefix) = ?  ");
			params.add(filter.getTypeCode().toUpperCase());
		}
		if (filter.getStatusType() != null) {
			sql.append(" and st.status = ?");
			params.add(filter.getStatusType());
		}
		return repo.getEntityBySQL(Organization.class, sql.toString(), params);
	}
	/**
	 * Lay StaffType tuong ung voi cac dieu kien 
	 * @author phuocdh2
	 * @since 03/26/2015
	 * @description : nhan vao staff_code,staff_type , lay ra StaffType
	 */
	@Override
	public StaffType getStaffTypeByCondition(OrganizationFilter filter) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from STAFF_TYPE st where 1 = 1 ");
		if ( !StringUtility.isNullOrEmpty(filter.getTypeName()) ) {
			sql.append(" and upper(st.name) = ? ");
			params.add(filter.getTypeName().toUpperCase());
		}
		if ( !StringUtility.isNullOrEmpty(filter.getTypeCode()) ) {
			sql.append(" and upper(st.prefix) = ?  ");
			params.add(filter.getTypeCode().toUpperCase());
		}
		if ( filter.getStatusType() != null) {
			sql.append(" and st.status = ?");
			params.add(filter.getStatusType());
		}
		return repo.getEntityBySQL(StaffType.class,sql.toString(), params);
	}
	/**
	 * Lay ShopType tuong ung voi cac dieu kien 
	 * @author phuocdh2
	 * @since 03/26/2015
	 * @description : nhan vao shop_code,shop_type , lay ra ShopType
	 */
	@Override
	public ShopType getShopTypeByCondition(OrganizationFilter filter) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from SHOP_TYPE st where 1 = 1 ");
		if ( !StringUtility.isNullOrEmpty(filter.getTypeName()) ) {
			sql.append(" and upper(st.name) = ? ");
			params.add(filter.getTypeName().toUpperCase());
		}
		if ( !StringUtility.isNullOrEmpty(filter.getTypeCode()) ) {
			sql.append(" and upper(st.prefix) = ?  ");
			params.add(filter.getTypeCode().toUpperCase());
		}
		if ( filter.getStatusType() != null) {
			sql.append(" and st.status = ?");
			params.add(filter.getStatusType());
		}
		return repo.getEntityBySQL(ShopType.class,sql.toString(), params);
	}
	
	/**END PHUOCDH2*/

	/**
	 * @author tungmt
	 * @since 9/6/2015
	 * Check
	 */
	@Override
	public Boolean checkParentOrg(OrganizationFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select orgId id from ( ");
		sql.append(" 	select organization_id orgId ");
		sql.append(" 	from organization where status=1  ");
		sql.append(" 	start with parent_org_id in (select parent_org_id from organization where organization_id=?) ");
		sql.append("	and status=1 connect by prior organization_id=parent_org_id ");
		params.add(filter.getParentOrgId());
		sql.append(" 	minus ");
		sql.append(" 	select org_except_id orgId from organization_except where org_id=? ");
		params.add(filter.getParentOrgId());
		sql.append(" ) org ");
		sql.append(" where org.orgId=? ");
		params.add(filter.getOrganizationId());
		String[] fieldNames = {"id"};
		Type[] fieldTypes = {StandardBasicTypes.LONG};
		List<OrganizationVO> lst = repo.getListByQueryAndScalar(OrganizationVO.class, fieldNames,fieldTypes, sql.toString(), params);
		return lst==null || lst.size()==0 ? false : true;
	}

}

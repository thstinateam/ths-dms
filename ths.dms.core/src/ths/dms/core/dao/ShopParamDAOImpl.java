/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ProductShopMap;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ShopParamType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ShopParamVO;
import ths.dms.core.entities.vo.ShopParamVOProduct;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.Constant;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

/**
 * Mo ta class ShopParamDAOImpl.java
 * 
 * @author vuongmq
 * @since Nov 28, 2015
 */
public class ShopParamDAOImpl implements ShopParamDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public ShopParam createShopParam(ShopParam shopParam, LogInfoVO logInfo) throws DataAccessException {
		if (shopParam == null) {
			throw new IllegalArgumentException("ShopParam is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfoVO is null");
		}
		if (!StringUtility.isNullOrEmpty(shopParam.getCode())) {
			shopParam.setCode(shopParam.getCode().toUpperCase());
		}
		shopParam = repo.create(shopParam);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, shopParam, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return shopParam;
	}

	@Override
	public void deleteShopParam(ShopParam shopParam, LogInfoVO logInfo) throws DataAccessException {
		if (shopParam == null) {
			throw new IllegalArgumentException("ShopParam is null");
		}
		repo.delete(shopParam);
		try {
			actionGeneralLogDAO.createActionGeneralLog(shopParam, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public void updateShopParam(ShopParam shopParam, LogInfoVO logInfo) throws DataAccessException {
		if (shopParam == null) {
			throw new IllegalArgumentException("ShopParam is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfoVO is null");
		}
		if (shopParam.getCode() != null) {
			shopParam.setCode(shopParam.getCode().toUpperCase());
		}
		ShopParam temp = this.getShopParamById(shopParam.getId());
		repo.update(shopParam);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, shopParam, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public ShopParam getShopParamById(long id) throws DataAccessException {
		return repo.getEntityById(ShopParam.class, id);
	}

	@Override
	public ShopParam getShopParamByShopIdCode(Long shopId, String code, ActiveType activeType) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("ShopParam shopId is null");
		}
		if (StringUtility.isNullOrEmpty(code)) {
			throw new IllegalArgumentException("ShopParam code is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from shop_param where shop_id = ? and code = ? ");
		params.add(shopId);
		params.add(code.trim().toUpperCase());
		if (activeType != null) {
			sql.append(" and status = ? ");
			params.add(activeType.getValue());
		}
		return repo.getEntityBySQL(ShopParam.class, sql.toString(), params);
	}

	@Override
	public ShopParam getShopParam(Long shopId, String code, ShopParamType type, ActiveType activeType) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("ShopParam shopId is null");
		}
		if (StringUtility.isNullOrEmpty(code)) {
			throw new IllegalArgumentException("ShopParam code is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from shop_param where shop_id = ? and code = ?  ");
		params.add(shopId);
		params.add(code.trim().toUpperCase());
		if (type != null) {
			sql.append(" and type = ? ");
			params.add(type.getValue());
		}
		if (activeType != null) {
			sql.append(" and status = ? ");
			params.add(activeType.getValue());
		}
		return repo.getEntityBySQL(ShopParam.class, sql.toString(), params);
	}
	@Override
	public List<ShopParamVO> getListShopParamVOByFilter(BasicFilter<ShopParamVO> filter) throws DataAccessException {
		if (filter == null || filter.getShopId() == null) {
			throw new IllegalArgumentException("ShopParam shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sp.shop_param_id as id, ");
		sql.append(" sp.shop_id as shopId, ");
		sql.append(" sp.code as code, ");
		sql.append(" sp.name as name, ");
		sql.append(" sp.value as value, ");
		if (Constant.SHOP_LOCK_ABORT_DURATION.equals(filter.getCode())) {
			sql.append(" (select regexp_substr(sp.value,'[^;]+', 1, level) as tmp from dual  ");
			sql.append("  where rownum = 1 ");
			sql.append(" connect by regexp_substr(sp.value, '[^;]+', 1, level) is not null ");
			sql.append(" ) as tmpView, ");
		} else {
			sql.append(" null as tmpView, ");
		}
		sql.append(" 1 as updateFlag ");
		sql.append(" from shop_param sp where sp.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and sp.shop_id = ? ");
		params.add(filter.getShopId());
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and sp.code = ? ");
			params.add(filter.getCode());
		}
		if (!StringUtility.isNullOrEmpty(filter.getTypeStr())) {
			sql.append(" and sp.type = ? ");
			params.add(filter.getTypeStr());
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (Constant.SHOP_LOCK_ABORT_DURATION.equals(filter.getCode())) {
			sql.append(" order by to_date(tmpView, 'yyyy-mm-dd') asc ");
		}
		String[] fieldNames = { 
				"id", //1
				"shopId", //2
				"code",  //3
				"name", //4
				"value", //5
				"tmpView", //6
				"updateFlag",  //8
		};
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG,  //1
				StandardBasicTypes.LONG, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.INTEGER, //8
		};
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(ShopParamVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
		return repo.getListByQueryAndScalar(ShopParamVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<ShopParamVO> getListShopParamVOViewTimeByFilter(BasicFilter<ShopParamVO> filter) throws DataAccessException {
		if (filter == null || filter.getShopId() == null) {
			throw new IllegalArgumentException("ShopParam shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sp.shop_param_id as id, ");
		sql.append(" sp.shop_id as shopId, ");
		sql.append(" sp.code as code, ");
		sql.append(" sp.name as name, ");
		sql.append(" sp.value as value, ");
		sql.append(" to_char(to_date(sp.value, 'yyyy-mm-dd hh24:mi:ss'), 'dd/mm/yyyy') as valueDate, ");
		sql.append(" to_char(to_date(sp.value, 'yyyy-mm-dd hh24:mi:ss'), 'hh24:mi') as valueTime, ");
		sql.append(" 1 as updateFlag ");
		sql.append(" from shop_param sp where sp.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and sp.shop_id = ? ");
		params.add(filter.getShopId());
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and sp.code = ? ");
			params.add(filter.getCode());
		}
		if (!StringUtility.isNullOrEmpty(filter.getTypeStr())) {
			sql.append(" and sp.type = ? ");
			params.add(filter.getTypeStr());
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (Constant.SHOP_LOCK_EXEC_TIME.equals(filter.getCode())) {
			sql.append(" order by to_date(sp.value, 'yyyy-mm-dd hh24:mi:ss') desc ");
		}
		String[] fieldNames = { 
				"id", //1
				"shopId", //2
				"code",  //3
				"name", //4
				"value", //5
				"valueDate",  //6
				"valueTime",  //7
				"updateFlag",  //8
		};
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG,  //1
				StandardBasicTypes.LONG, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.STRING, //7
				StandardBasicTypes.INTEGER, //8
		};
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(ShopParamVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
		return repo.getListByQueryAndScalar(ShopParamVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<ShopParamVOProduct> getListProductShopMapByFilter(BasicFilter<ShopParamVOProduct> filter) throws DataAccessException {
		if (filter == null || filter.getShopId() == null) {
			throw new IllegalArgumentException("ShopParam shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select p.product_id AS productId, ");
		sql.append(" p.product_code AS productCode, ");
		sql.append(" p.product_name as productName, ");
		sql.append(" psm.product_shop_map_id as id, ");
		sql.append(" psm.shop_id as shopId, ");
		sql.append(" psm.nessesary_stock_day as stockDay ");
		sql.append(" from product p ");
		sql.append(" left join product_shop_map psm on (psm.product_id = p.product_id and psm.shop_id = ?) ");
		params.add(filter.getShopId());
		sql.append(" where p.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		if (filter.getProductId() != null) {
			sql.append(" and p.product_id = ? ");
			params.add(filter.getProductId());
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		sql.append(" order by p.product_code ");
		String[] fieldNames = { 
				"productId", //1
				"productCode", //2
				"productName",  //3
				"id", //4
				"shopId", //5
				"stockDay",  //6
		};
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG,  //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.LONG, //4
				StandardBasicTypes.LONG, //5
				StandardBasicTypes.INTEGER, //6
		};
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(ShopParamVOProduct.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
		return repo.getListByQueryAndScalar(ShopParamVOProduct.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public ProductShopMap getProductShopMapByShopProductId(Long shopId, Long productId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("ShopParam shopId is null");
		}
		if (productId == null) {
			throw new IllegalArgumentException("ShopParam productId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_shop_map ");
		sql.append(" where shop_id = ? and product_id = ? ");
		params.add(shopId);
		params.add(productId);
		return repo.getEntityBySQL(ProductShopMap.class, sql.toString(), params);
	}
}

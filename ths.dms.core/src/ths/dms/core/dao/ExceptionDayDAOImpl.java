package ths.dms.core.dao;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.type.Type;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ExceptionDay;
import ths.dms.core.entities.WorkingDateConfig;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.SpParam;
import ths.dms.core.entities.enumtype.WorkingDateConfigFilter;
import ths.dms.core.entities.enumtype.WorkingDateProcedureType;
import ths.dms.core.entities.enumtype.WorkingDateType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.WorkingDateConfigVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class ExceptionDayDAOImpl implements ExceptionDayDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public List<ExceptionDay> getExceptionDayByYear(int year)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select *  from EXCEPTION_DAY  where to_char(day_off, 'yyyy') = ? ");

		params.add(year);

		return repo.getListBySQL(ExceptionDay.class, sql.toString(), params);
	}
	
	@Override
	public List<ExceptionDay> getExceptionDayByMonth(int month, int year)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select *  from EXCEPTION_DAY  where day_off >= trunc(?, 'MONTH') and day_off < (trunc(?, 'MONTH') + interval '1' month)");

		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, 1);
		params.add(cal.getTime());
		params.add(cal.getTime());

		return repo.getListBySQL(ExceptionDay.class, sql.toString(), params);
	}
	
	@Override
	public ExceptionDay createExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo) throws DataAccessException {
		if (exceptionDay == null) {
			throw new IllegalArgumentException("exceptionDay");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		
		exceptionDay.setCreateDate(this.commonDAO.getSysDate());
		exceptionDay.setCreateUser(logInfo.getStaffCode());
		
		exceptionDay = repo.create(exceptionDay);

		//exceptionDay = repo.getEntityById(ExceptionDay.class, exceptionDay.getId());

		try {
			actionGeneralLogDAO.createActionGeneralLog(null, exceptionDay, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}

		return exceptionDay;
	}

	@Override
	public void deleteExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo)
			throws DataAccessException {
		if (exceptionDay == null) {
			throw new IllegalArgumentException("exceptionDay");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}

		try {
			actionGeneralLogDAO.createActionGeneralLog(exceptionDay, null,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		repo.delete(exceptionDay);
	}

	@Override
	public ExceptionDay getExceptionDayById(long id) throws DataAccessException {
		return repo.getEntityById(ExceptionDay.class, id);
	}

	@Override
	public void updateExceptionDay(ExceptionDay exceptionDay, LogInfoVO logInfo)
			throws DataAccessException {
		if (exceptionDay == null) {
			throw new IllegalArgumentException("exceptionDay");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		
		ExceptionDay temp = this.getExceptionDayById(exceptionDay.getId());
		
		exceptionDay.setUpdateUser(logInfo.getStaffCode());
		exceptionDay.setUpdateDate(commonDAO.getSysDate());

		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, exceptionDay,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}

		repo.update(exceptionDay);
	}

	@Override
	public Integer getCountDayOffBetween(Date fromDate, Date toDate)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return null;

		if (fromDate.after(toDate)) {
			throw new DataAccessException("fromDate is after toDate");
		}
		StringBuilder countSql = new StringBuilder();
		List<Object> countParams = new ArrayList<Object>();
		countSql.append(" select count(1) as count from (");
		countSql.append(" select to_char(day_off, 'YYYYMMDD') from exception_day");
		countSql.append(" where day_off >= trunc(?) and day_off < trunc(?) + 1 and TO_CHAR(day_off, 'D') != '1'");
		countSql.append(" and day_off IS NOT NULL");
		countSql.append(" group by to_char(day_off, 'YYYYMMDD') )");
		countParams.add(fromDate);
		countParams.add(toDate);
		return repo.countBySQL(countSql.toString(), countParams);
	}
	
	@Override
	public Integer getCountExDayOffBetween(Date fromDate, Date toDate)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return null;

		if (fromDate.after(toDate)) {
			throw new DataAccessException("fromDate is after toDate");
		}
		StringBuilder countSql = new StringBuilder();
		List<Object> countParams = new ArrayList<Object>();
		countSql.append(" select count(1) as count from (");
		countSql.append(" select to_char(day_off, 'YYYYMMDD') from exception_day");
//		countSql.append(" where day_off >= trunc(?) and day_off < trunc(?) + 1 and TO_CHAR(day_off, 'D') != '1'");
		countSql.append(" where day_off >= trunc(?) and day_off < trunc(?) + 1 ");
		countSql.append(" and day_off IS NOT NULL");
		countSql.append(" group by to_char(day_off, 'YYYYMMDD') )");
		countParams.add(fromDate);
		countParams.add(toDate);
		return repo.countBySQL(countSql.toString(), countParams);
	}

	@Override
	public ExceptionDay getExceptionDayByDate(Date date,Long shopId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from EXCEPTION_DAY where day_off >= trunc(?) and day_off < trunc(?) + 1");
		sql.append(" and shop_id = ?");
		params.add(date);
		params.add(date);
		params.add(shopId);
		return repo.getEntityBySQL(ExceptionDay.class, sql.toString(), params);
	}

	@Override
	public List<ExceptionDay> getExceptionDayByMonth(int month, int year,Long shopId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select *  from EXCEPTION_DAY  where day_off >= trunc(?, 'MONTH') and day_off < (trunc(?, 'MONTH') + interval '1' month)");
		sql.append(" and shop_id = ?");
		Calendar cal = Calendar.getInstance();
		cal.set(year, month - 1, 1);
		params.add(cal.getTime());
		params.add(cal.getTime());
		params.add(shopId);
		return repo.getListBySQL(ExceptionDay.class, sql.toString(), params);
	}

	@Override
	public List<ExceptionDay> getExceptionDayByYear(int year,Long shopId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select *  from EXCEPTION_DAY  where to_char(day_off, 'yyyy') = ? ");
		sql.append(" and shop_id = ?");
		params.add(year);
		params.add(shopId);
		return repo.getListBySQL(ExceptionDay.class, sql.toString(), params);
	}

	@Override
	public ExceptionDay getExceptionDayByDate(int month, int year, int date)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		String strDate = "" + year + "-" + month + "-" + date;
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from EXCEPTION_DAY where day_off >= to_date(?, 'yyyy-mm-dd') and day_off < to_date(?, 'yyyy-mm-dd') + 1");

		params.add(strDate);
		params.add(strDate);

		return repo.getEntityBySQL(ExceptionDay.class, sql.toString(), params);
	}
	
	@Override
	public Integer getNumExceptionByDateInMonth(Date toDate)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) as count from exception_day where day_off >= trunc(?, 'MONTH') and day_off <= trunc(?)");		
		params.add(toDate);
		params.add(toDate);
		return repo.countBySQL(sql.toString(), params);		
	}
	
	@Override
	public List<ExceptionDay> getLstExceptionDay(Date fromDate, Date toDate)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from EXCEPTION_DAY where 1 = 1 ");
		if(fromDate != null){
			sql.append(" AND day_off >= trunc(?)");
			params.add(fromDate);
		}
		if(toDate != null){
			sql.append(" AND day_off <= trunc(?)");
			params.add(toDate);
		}

		return repo.getListBySQL(ExceptionDay.class, sql.toString(), params);
	}
	
	@Override
	public Integer getNumOfHoliday(Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from EXCEPTION_DAY where to_char(day_off, 'DY') != 'SUN' ");
		if(fromDate != null){
			sql.append(" AND day_off >= trunc(?)");
			params.add(fromDate);
		}
		if(toDate != null){
			sql.append(" AND day_off <= trunc(?)");
			params.add(toDate);
		}

		return repo.countBySQL(sql.toString(), params);
	}
	
	@Override
	public Integer getNumOfHoliday(Date fromDate, Date toDate, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from EXCEPTION_DAY where 1 = 1 ");
		if(fromDate != null){
			sql.append(" AND day_off >= trunc(?)");
			params.add(fromDate);
		}
		if(toDate != null){
			sql.append(" AND day_off <= trunc(?)");
			params.add(toDate);
		}
		
		if(shopId != null){
			sql.append(" AND shop_id = ?");
			params.add(shopId);
		}

		return repo.countBySQL(sql.toString(), params);
	}
	
	@Override
	public Integer getExceptionDayForSupervise() throws DataAccessException {		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT NVL(EXTRACT(DAY FROM SYSDATE)-(select count(*) from EXCEPTION_DAY ");
		sql.append("where day_off between TRUNC(sysdate,'MM') and sysdate)-( SELECT count(1) count ");
		sql.append("FROM (SELECT (TRUNC(sysdate, 'MM') + LEVEL - 1) days ");
		sql.append("FROM DUAL ");
		sql.append("CONNECT BY LEVEL <= TO_CHAR((sysdate) - TRUNC(sysdate, 'MM')) ");
		sql.append(") tb ");
		sql.append("WHERE TO_CHAR(tb.days, 'fmDAY') IN ('SUNDAY') ");
		sql.append("),0) as snBHDQ ");
		sql.append("FROM DUAL");
		return ((BigDecimal) repo.getObjectByQuery(sql.toString(), params)).intValue();
	}

	@Override
	public Integer getNumExceptionDayF1(Date fDate, Date tDate)
			throws DataAccessException {
		int total = 0;
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM EXCEPTION_DAY WHERE DAY_OFF >= TRUNC(?) ");
		sql.append("AND DAY_OFF < TRUNC(?) +1 ");
		params.add(fDate);
		params.add(tDate);
		Integer i = repo.getListBySQL(ExceptionDay.class, sql.toString(), params).size();
		while(i>0){
			total+=i;
			params = new ArrayList<Object>();
			Date tDateBefore = new Date(fDate.getTime() - 1 * 24 * 3600 * 1000 );
			fDate = new Date(fDate.getTime() - i * 24 * 3600 * 1000);
			params.add(fDate);
			params.add(tDateBefore);
			i = repo.getListBySQL(ExceptionDay.class, sql.toString(), params).size();
		}
		return total;
	}

	@Override
	public List<WorkingDateConfig> getListWorkingDateConfigByFilter(WorkingDateConfigFilter filter)	
		 	throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from working_date_config  where 1 = 1 ");
		if(filter.getShopId()!=null){
			sql.append(" and shop_id = ? ");
			params.add(filter.getShopId());
		}
		if(filter.getStatus()!=null){
			sql.append(" and status = ? ");
			params.add(filter.getStatus().getValue());
		}
		if(filter.getType()!=null){
			sql.append(" and type = ? ");
			params.add(filter.getType().getValue());
		}
		return repo.getListBySQL(WorkingDateConfig.class, sql.toString(), params);
	}
	
	@Override
	public void callProceduceWorkingDate(Long shopId,Date fromDate, Date toDate, WorkingDateProcedureType type) throws DataAccessException {
		List<SpParam> inParams = new ArrayList<SpParam>();
		List<SpParam> outParams = new ArrayList<SpParam>();

		inParams.add(new SpParam(1, Types.NUMERIC, shopId));
		if(fromDate!=null){
			inParams.add(new SpParam(2, Types.DATE,new java.sql.Date(fromDate.getTime())));
		}else{
			inParams.add(new SpParam(2, Types.DATE,null));
		}
		if(toDate!=null){
			inParams.add(new SpParam(3, Types.DATE,new java.sql.Date(toDate.getTime())));
		}else{
			inParams.add(new SpParam(3, Types.DATE,null));
		}
		inParams.add(new SpParam(4, Types.NUMERIC, type.getValue()));
		String storeProcedure = "PROC_RECURSIVE_WORKING_DATE";		
		repo.executeSP(storeProcedure, inParams, outParams);
	}	
		
		
		
	/****************************BEGIN VUONGMQ*********************/
	@Override
	public WorkingDateConfig getWorkDateConfigByShopId(Long shopId, ActiveType active)	throws DataAccessException {
		if(shopId == null){
			return null;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM working_date_config ");
		sql.append(" WHERE 1= 1 ");
		if(active != null){
			sql.append(" and status = ? ");
			params.add(active.getValue());
		}
		if(shopId != null){
			sql.append("AND shop_id = ? ");
			params.add(shopId);
		}
		return repo.getEntityBySQL(WorkingDateConfig.class, sql.toString(), params);
	}
	@Override
	public List<ExceptionDay> getListByShopIdAndFromDateToDate(Long shopId,
			Date fromMonth, Date toMonth) throws DataAccessException {
		if(shopId == null){
			return null;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM exception_day ");
		sql.append(" WHERE 1= 1 ");
		if(shopId != null){
			sql.append("AND shop_id = ? ");
			params.add(shopId);
		}
		if(fromMonth != null){
			sql.append(" and day_off >= trunc(?, 'MONTH') ");
			params.add(fromMonth);
		}
		if(toMonth != null){
			sql.append(" and day_off < (trunc(?, 'MONTH') + interval '1' month) ");
			params.add(toMonth);
		}
		return repo.getListBySQL(ExceptionDay.class, sql.toString(), params);
	}
	/*@Override
	public List<WorkingDateConfigVO> getListWorkingDateConfigVOByFilter(WorkingDateConfigFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select w.working_date_config_id as id,  ");
		sql.append(" w.shop_id as shopId, ");
		sql.append(" w.type as type ");
		sql.append(" where 1 = 1 ");
		if(filter.getStatus() != null){
			sql.append(" and w.status = ? ");
			params.add(filter.getStatus().getValue());
		}
		if(filter.getType() != null){
			sql.append(" and w.type = ? ");
			params.add(filter.getType().getValue());
		}
		String[] fieldNames = {"id", "shopId", "type"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER};
		return repo.getListByQueryAndScalar(WorkingDateConfigVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}*/
	@Override
	public List<WorkingDateConfigVO> getWorkDateConfigDateCheckParent(Long shopId, WorkingDateType type, ActiveType active)	throws DataAccessException {
		if(shopId == null){
			return null;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select tmp.working_date_config_id as id, ");
		sql.append(" tmp.type as type, ");
		sql.append(" tmp.shop_id as shopId, ");
		sql.append(" tmp.shop_code as shopCode, ");
		sql.append(" tmp.shop_name as shopName ");
		sql.append(" from ");
		sql.append(" (select wdc.*, sp.shop_code, sp.shop_name from shop sp left join working_date_config wdc on wdc.shop_id = sp.shop_id and sp.status = 1 ");
		sql.append(" start with sp.shop_id = ? ");
		sql.append(" connect by prior sp.parent_shop_id = sp.shop_id ");
		sql.append(" order by level) tmp ");
		params.add(shopId);
		sql.append(" where shop_id <> ? ");
		params.add(shopId);
		if(type != null){
			sql.append(" and type = ? ");
			params.add(type.getValue());
		}
		if(active != null){
			sql.append(" and status = ? ");
			params.add(active.getValue());
		}
		sql.append(" and rownum <= 1 "); // list nay chi co mot dong cua VO
		String[] fieldNames = {"id", "type",
								"shopId", "shopCode", "shopName"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.INTEGER,
				 			StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING};
		return repo.getListByQueryAndScalar(WorkingDateConfigVO.class, fieldNames, fieldTypes, sql.toString(), params);
		
	}
	/****************************END VUONGMQ*********************/

	@Override
	public List<ExceptionDay> getListByShopIdAndFromDateToDate(Long shopIdCopy, String fromMonth) throws DataAccessException {
		if(shopIdCopy == null){
			return null;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM exception_day ");
		sql.append(" WHERE 1= 1 ");
		if(shopIdCopy != null){
			sql.append("AND shop_id = ? ");
			params.add(shopIdCopy);
		}
		if(fromMonth != null){
			sql.append(" and trunc(day_off) >= trunc(sysdate) ");
	/*		params.add(fromMonth);*/
		}		
		return repo.getListBySQL(ExceptionDay.class, sql.toString(), params);
	}
	

}

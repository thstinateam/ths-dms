package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Kpi;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.CommonFilter;

import ths.dms.core.dao.repo.IRepository;

public class KpiDAOImpl implements KpiDAO{
	
	@Autowired
	private IRepository repo;

	@Override
	public List<Kpi> getListKpiByFilter(KPaging<Kpi> kPaging,
			CommonFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from kpi ");
		sql.append(" where 1 = 1 ");
		
		if (filter.getPlanType() != null){
			sql.append(" and plan_type = ? ");
			params.add(filter.getPlanType());
		}
		if (filter.getFromDate() != null){
			sql.append(" and begin_date >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null){
			sql.append(" and begin_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if (filter.getStatus() != null){
			sql.append(" and status = ? ");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		
		sql.append(" order by order_index ");
		
		if (kPaging != null) {
			return repo.getListBySQLPaginated(Kpi.class, sql.toString(), params, kPaging);
		}
		return repo.getListBySQL(Kpi.class, sql.toString(), params);
	}

}

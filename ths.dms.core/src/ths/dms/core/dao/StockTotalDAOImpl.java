package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.StockLock;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTotalVOFilter;
import ths.dms.core.entities.enumtype.WarehouseType;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.entities.vo.ProductAmountVO;
import ths.dms.core.entities.vo.ProductLotVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.core.entities.vo.rpt.RptCustomerStock2VO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class StockTotalDAOImpl implements StockTotalDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	ShopLockDAO shopLockDAO;
	
	public final Integer TYPE_EXPORT = 1;
	public final Integer TYPE_IMPORT = 0;

	@Override
	public StockTotal createStockTotal(StockTotal stockTotal) throws DataAccessException {
		if (stockTotal == null) {
			throw new IllegalArgumentException("stockTotal");
		}
		repo.create(stockTotal);
		return repo.getEntityById(StockTotal.class, stockTotal.getId());
	}

	@Override
	public void deleteStockTotal(StockTotal stockTotal) throws DataAccessException {
		if (stockTotal == null) {
			throw new IllegalArgumentException("stockTotal");
		}
		repo.delete(stockTotal);
	}

	@Override
	public StockTotal getStockTotalById(Long id) throws DataAccessException {
		return repo.getEntityById(StockTotal.class, id);
	}
	
	@Override
	public void updateStockTotal(StockTotal stockTotal) throws DataAccessException {
		if (stockTotal == null) {
			throw new IllegalArgumentException("stockTotal");
		}
		stockTotal.setUpdateDate(commonDAO.getSysDate());
		
		repo.update(stockTotal);
	}
	
	@Override
	public void updateStockLock(StockLock stockLock) throws DataAccessException {
		if (stockLock == null) {
			throw new IllegalArgumentException("stockLock");
		}
		stockLock.setUpdateDate(commonDAO.getSysDate());
		repo.update(stockLock);
	}
	
	@Override
	public void insertStockLock(StockLock stockLock) throws DataAccessException {
		if (stockLock == null) {
			throw new IllegalArgumentException("stockLock");
		}
		stockLock.setUpdateDate(commonDAO.getSysDate());
		repo.create(stockLock);
	}
	
	@Override
	public void createListStockTotal(List<StockTotal> listStockTotal) throws DataAccessException {
		if (listStockTotal == null) {
			throw new IllegalArgumentException("stockTotal");
		} else {
			Date createDate = commonDAO.getSysDate();
			for (StockTotal item : listStockTotal) {
				item.setCreateDate(createDate);
			}
		}
		repo.create(listStockTotal);
	}
	
	@Override
	public void updateListStockTotal(List<StockTotal> listStockTotal) throws DataAccessException {
		if (listStockTotal == null) {
			throw new IllegalArgumentException("stockTotal");
		} else {
			Date updateDate = commonDAO.getSysDate();
			for (StockTotal item : listStockTotal) {
				item.setUpdateDate(updateDate);
			}
		}
		repo.update(listStockTotal);
	}
	
	@Override
	public List<Boolean> checkIfProductHasEnough(List<ProductAmountVO> lstProductAmountVO) throws DataAccessException {
		List<String> lstSql = new ArrayList<String>();
		List<List<Object>> lstParams = new ArrayList<List<Object>>();
		for (ProductAmountVO vo: lstProductAmountVO) {
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append(" from dual where exists ");
			sql.append(" (select 1 ");
			sql.append(" from stock_total ");
			sql.append(" where product_id =? ");
			params.add(vo.getProductId());
			sql.append(" and object_id =? ");
			params.add(vo.getOwnerId());
			sql.append(" and object_type =? ");
			params.add(vo.getOwnerType().getValue());			
			if(vo.getWarehouseId() != null){
				sql.append(" and warehouse_id = ? ");
				params.add(vo.getWarehouseId());
			}
			sql.append(" and AVAILABLE_QUANTITY >= ");
			//sql.append(" and QUANTITY >= ");
			sql.append(" (select convfact from product where product_id=? ");
			params.add(vo.getProductId());
			sql.append(" )*?+? ");
			params.add(vo.getPakage());
			params.add(vo.getUnit());
			sql.append(" ) ");
			lstSql.add(sql.toString());			
			lstParams.add(params);
		}
		return repo.checkExistBySQL(lstSql, lstParams);
	}
	
	/***
	 * @author vuongmq
	 * @date 11/08/2015
	 * @description Lay getStockTotalByEntities theo filter
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public StockTotal getStockTotalByEntities(StockTotalFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from stock_total ");
		sql.append(" where 1 = 1 ");
		if (filter.getStatus() != null) {
			sql.append(" and status=? ");
			params.add(filter.getStatus());
		}
		if (filter.getOwnerId() != null) {
			sql.append(" and object_id=? ");
			params.add(filter.getOwnerId());
		}
		if (filter.getOwnerType() != null) {
			sql.append(" and object_type= ?");
			params.add(filter.getOwnerType().getValue());
		}
		if (filter.getProductId() != null) {
			sql.append(" and product_id=? ");
			params.add(filter.getProductId());
		}
		if (filter.getWarehouseId() != null) {
			sql.append(" and warehouse_id=? ");
			params.add(filter.getWarehouseId());
		}
		return repo.getEntityBySQL(StockTotal.class, sql.toString(), params);
	}
	
	@Override
	public StockTotal getStockTotalByProductAndOwner(long productId, StockObjectType ownerType, long ownerId, Long warehouseId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from stock_total where object_id=? and object_type= ? and product_id=? ");
		List<Object> params = new ArrayList<Object>();
		params.add(ownerId);
		params.add(ownerType.getValue());
		params.add(productId);
		if (warehouseId != null && ownerType == StockObjectType.SHOP) {
			sql.append("  and warehouse_id = ? ");
			params.add(warehouseId);
		}
		return repo.getEntityBySQL(StockTotal.class, sql.toString(), params);
	}
	
	@Override
	public StockTotal getStockTotalByProductAndOwnerForUpdate(Long productId, StockObjectType ownerType, Long ownerId, Long warehouseId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from stock_total where status = 1 and object_id=? and product_id=?");
		List<Object> params = new ArrayList<Object>();
		params.add(ownerId);
		params.add(productId);
		if (ownerType != null) {
			sql.append("  and object_type=?");
			params.add(ownerType.getValue());
		}
		else{
			sql.append("  and object_type is null");
		}
		if(warehouseId!=null){
			sql.append("  and warehouse_id = ?");
			params.add(warehouseId);
		}
		sql.append(" for update");
		return repo.getEntityBySQL(StockTotal.class, sql.toString(), params);
	}
	
	
	@Override
	public List<StockTotalVO> getListStockTotalVO(StockTotalFilter filter) throws DataAccessException {
//		Date appDate = commonDAO.getSysDate();
//		if(StockObjectType.SHOP.equals(filter.getOwnerType()) && filter.getOwnerId()!=null){
//			appDate=shopLockDAO.getApplicationDate(filter.getOwnerId());
//		}
		StringBuilder sql = new StringBuilder();
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT distinct st.stock_total_id AS stockTotalId, ");
		sql.append(" 	st.product_id AS productId, ");
		sql.append(" 	prd.product_code AS productCode, ");
		sql.append(" 	prd.product_name AS productName, ");
		sql.append(" 	st.available_quantity AS availableQuantity, ");
		sql.append(" 	st.quantity AS quantity, ");
		sql.append(" 	prd.convfact, ");
		sql.append(" 	prd.gross_weight AS grossWeight, ");
		sql.append(" 	prd.uom1, ");
		sql.append(" 	wh.warehouse_id as warehouseId, ");
		sql.append(" 	wh.warehouse_name as warehouseName, ");
		sql.append(" 	wh.seq, ");
		sql.append(" 	prd.check_lot AS checkLot, ");
		sql.append(" 	(select product_info_code from product_info pi where pi.product_info_id = prd.cat_id ) as cat, ");
		sql.append(" 	(select product_info_code from product_info pi where pi.product_info_id = prd.sub_cat_id ) as subCat ");
		sql.append(" from product prd ");
		if (TYPE_IMPORT.equals(filter.getTypeUpdate())) {
			sql.append(" join stock_total st on st.product_id = prd.product_id ");
			sql.append(" join warehouse wh on wh.warehouse_id = st.warehouse_id and wh.status = 1 ");
		} else {
			sql.append(" left join stock_total st on st.product_id = prd.product_id ");
			sql.append(" left join warehouse wh on wh.warehouse_id = st.warehouse_id and wh.status = 1 ");
		}
		sql.append(" where prd.status = 1  ");
		
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			sql.append(" and exists (select 1 from product p where p.product_id=st.product_id and p.product_code like ? ESCAPE '/')");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProductCode().toUpperCase()));
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			sql.append(" and lower(prd.name_text) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getProductName().toLowerCase())));
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getPromotionProgramCode())) {
			sql.append(" and exists (select 1 from promotion_program_detail ppd where ppd.product_id=st.product_id and exists(select 1 from promotion_program pp where pp.promotion_program_id=ppd.promotion_program_id and promotion_program_code like ? ESCAPE '/'))");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getPromotionProgramCode().toUpperCase()));
		}
		
		if(filter.getSeq() != null){
			sql.append(" and seq = ? ");
			params.add(filter.getSeq());
		}
		
		if (filter.getOwnerId() != null) {
			sql.append(" and object_id = ? ");
			params.add(filter.getOwnerId());
		}
		if (filter.getOwnerType() != null) {
			sql.append(" and st.object_type = ? ");
			params.add(filter.getOwnerType().getValue());
		}		
		if (filter.getCatId() != null) {
			sql.append(" and exists (select 1 from product_info_map pim where prd.product_level_id=pim.product_info_map_id and pim.from_object_id = ? )");
			params.add(filter.getCatId());
		}
		if (filter.getSubCatId() != null) {
			sql.append(" and exists (select 1 from product_info_map pim where prd.product_level_id = pim.product_info_map_id and pim.to_object_id = ? )");
			params.add(filter.getSubCatId());
		}
		if (filter.getFromQuantity() != null) {
			sql.append(" and quantity >= ? ");
			params.add(filter.getFromQuantity());
		}
		if (filter.getToQuantity() != null) {
			sql.append(" and quantity <= ?");
			params.add(filter.getToQuantity());
		}
		if (filter.getExceptProductId() != null) {
			sql.append(" and prd.product_id not in (-1");
			for (Long id: filter.getExceptProductId()) {
				if (id != null) {
					sql.append(",?");
					params.add(id);
				}
			}
			sql.append(")");
		}
		
		if(filter.getStaffId() != null) {
			sql.append(" and (not exists (select 1 from staff_sale_cat where staff_id = ?)");
			sql.append(" or exists (select 1 from staff_sale_cat where cat_id = prd.cat_id and staff_id = ?))");
			params.add(filter.getStaffId());
			params.add(filter.getStaffId());
		}		
		sql.append(" order by productCode, seq ");
		selectSql.append(sql.toString());
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql.toString());
		countSql.append(") tbcount ");
		
		String[] fieldNames = {
				"stockTotalId"
				,"productId", "productCode", "productName", 
				"availableQuantity", "quantity", 
				"convfact","grossWeight","uom1", 
				"warehouseId","warehouseName","seq",
				"checkLot", "cat", "subCat"
			};		
		Type[] fieldTypes = {
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT, StandardBasicTypes.STRING,
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING
			};
		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, selectSql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(StockTotalVO.class, fieldNames, fieldTypes, selectSql.toString(), countSql.toString(), params, params, filter.getkPaging());
	
		
	}
	
	@Override
	public List<StockTotalVO> getListStockTotalVONew(KPaging<StockTotalVO> kPaging, StockTotalVOFilter stockTotalVoFilter) throws DataAccessException {
		Date appDate = commonDAO.getSysDate();
		if(stockTotalVoFilter.getShopId()!=null){
			appDate=shopLockDAO.getApplicationDate(stockTotalVoFilter.getShopId());
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		selectSql.append("select distinct st.stock_total_id as id, ");
		selectSql.append("  st.product_id as productId, prd.product_code as productCode, prd.product_name as productName,");
		selectSql.append(" 	st.available_quantity as availableQuantity, st.quantity as quantity, pr.price as price,");
		selectSql.append("  prd.convfact,");
		selectSql.append("  prd.gross_weight as grossWeight, prd.uom1, prd.check_lot as checkLot,");
		selectSql.append("	(select product_info_code from product_info pi where pi.product_info_id = pim.from_object_id) as cat, ");
		selectSql.append("	(select product_info_code from product_info pi where pi.product_info_id = pim.to_object_id) as subCat");
		
		countSql.append(" select count(1) as count ");
		
		sql.append(" from stock_total st join product prd on st.product_id=prd.product_id ");
		sql.append(" 	join price pr on pr.product_id = prd.product_id ");
		sql.append("	left join product_info_map pim on prd.product_level_id = pim.product_info_map_id and pim.object_type=12 ");
		sql.append(" where prd.status = ? and pr.status = ? and pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?))");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		params.add(appDate);
		params.add(appDate);
		
//		sql.append(" and exists (select 1 from product_info pi where (pi.product_info_id = pim.from_object_id or pi.product_info_id = pim.to_object_id) and product_info_code in ('A', 'B', 'C', 'D', 'E', 'F'))");
		
		if (!StringUtility.isNullOrEmpty(stockTotalVoFilter.getProductCode())) {
			sql.append(" and exists (select 1 from product p where p.product_id=st.product_id and p.product_code like ? ESCAPE '/')");
			params.add(StringUtility.toOracleSearchLikeSuffix(stockTotalVoFilter.getProductCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(stockTotalVoFilter.getProductName())) {
			sql.append(" and lower(prd.product_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(stockTotalVoFilter.getProductName().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(stockTotalVoFilter.getPromotionProgramCode())) {
			sql.append(" and exists (select 1 from promotion_program_detail ppd where ppd.product_id=st.product_id and exists(select 1 from promotion_program pp where pp.promotion_program_id=ppd.promotion_program_id and promotion_program_code like ? ESCAPE '/'))");
			params.add(StringUtility.toOracleSearchLikeSuffix(stockTotalVoFilter.getPromotionProgramCode().toUpperCase()));
		}
		if (stockTotalVoFilter.getOwnerId() != null) {
			sql.append(" and object_id=?");
			params.add(stockTotalVoFilter.getOwnerId());
		}
		if (stockTotalVoFilter.getOwnerType() != null) {
			sql.append(" and st.object_type=?");
			params.add(stockTotalVoFilter.getOwnerType().getValue());
		}
		//sql.append(" and pim.object_type=12");
		if (stockTotalVoFilter.getCatId() != null) {
			sql.append(" and exists (select 1 from product_info_map pim where prd.product_level_id=pim.product_info_map_id and pim.from_object_id=?)");
			params.add(stockTotalVoFilter.getCatId());
		}
		if (stockTotalVoFilter.getSubCatId() != null) {
			sql.append(" and exists (select 1 from product_info_map pim where prd.product_level_id = pim.product_info_map_id and pim.to_object_id=?)");
			params.add(stockTotalVoFilter.getSubCatId());
		}
		if (stockTotalVoFilter.getFromQuantity() != null) {
			sql.append(" and quantity>=?");
			params.add(stockTotalVoFilter.getFromQuantity());
		}
		if (stockTotalVoFilter.getToQuantity() != null) {
			sql.append(" and quantity<=?");
			params.add(stockTotalVoFilter.getToQuantity());
		}
		if (stockTotalVoFilter.getExceptProductId() != null) {
			sql.append(" and prd.product_id not in (-1");
			for (Long id: stockTotalVoFilter.getExceptProductId()) {
				if (id != null) {
					sql.append(",?");
					params.add(id);
				}
			}
			sql.append(")");
		}
		
		if(stockTotalVoFilter.getStaffSalerId() != null) {
			sql.append(" and (not exists (select 1 from staff_sale_cat where staff_id = ?)");
			sql.append(" or exists (select 1 from staff_sale_cat where cat_id = prd.cat_id and staff_id = ?))");
			params.add(stockTotalVoFilter.getStaffSalerId());
			params.add(stockTotalVoFilter.getStaffSalerId());
		}
//		sql.append(" and st.quantity >0");
		selectSql.append(sql.toString());
		selectSql.append(" order by productCode");
		countSql.append(sql.toString());
		String[] fieldNames = {"id","productId", "productCode", "productName", "availableQuantity", "quantity", "price", "convfact","grossWeight", "uom1", "checkLot", "cat", "subCat"};
		Type[] fieldTypes = {StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING};
		if (kPaging == null)
			return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, selectSql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(StockTotalVO.class, fieldNames, fieldTypes, selectSql.toString(), countSql.toString(), params, params, kPaging);
	
		
	}
	
//	@Override
//	public List<StockTotalVO> getListStockTotalVOForImExport(StockTotalFilter filter ) throws DataAccessException {
//		StringBuilder sql = new StringBuilder();
//		StringBuilder selectSql = new StringBuilder();
//		StringBuilder countSql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		List<Object> selectParams = new ArrayList<Object>();
//		
//		selectSql.append("select ");
//		selectSql.append("  prd.product_id as productId, prd.product_code as productCode, prd.product_name as productName,");
//		selectSql.append("  (select st.available_quantity from stock_total st where st.product_id = prd.product_id and object_id = ? and object_type = ?) as availableQuantity, ");
//		selectParams.add(ownerId);
//		selectParams.add(ownerType.getValue());
//		selectSql.append("  (select st.quantity from stock_total st where st.product_id = prd.product_id and object_id = ? and object_type = ?) as quantity, ");
//		selectParams.add(ownerId);
//		selectParams.add(ownerType.getValue());
//		selectSql.append("  pr.price as price, prd.convfact,");
//		selectSql.append("  prd.gross_weight as grossWeight, prd.uom1, prd.check_lot as checkLot,");
//		selectSql.append("	(select product_info_code from product_info pi where pi.product_info_id = pl.cat_id) as cat, ");
//		selectSql.append("	(select product_info_code from product_info pi where pi.product_info_id = pl.sub_cat_id) as subCat,");
//		selectSql.append("	(select st.stock_total_id from stock_total st where st.product_id = prd.product_id and object_id = ? and object_type = ?) as stockTotalId");
//		selectParams.add(ownerId);
//		selectParams.add(ownerType.getValue());
//		
//		countSql.append(" select count(1) as count ");
//		
//		sql.append(" from product prd");
//		sql.append(" 	join price pr on pr.product_id = prd.product_id ");
//		sql.append("	left join product_level pl on prd.product_level_id = pl.product_level_id");
//		sql.append(" where prd.status = ? and pr.status = ? and pr.from_date < trunc(sysdate) + 1 and (pr.to_date is null or pr.to_date >= trunc(sysdate))");
//		params.add(ActiveType.RUNNING.getValue());
//		params.add(ActiveType.RUNNING.getValue());
//		
////		sql.append(" and exists (select 1 from product_info pi where (pi.product_info_id = pl.cat_id or pi.product_info_id = pl.sub_cat_id) and product_info_code in ('A', 'B', 'C', 'D', 'E', 'F'))");
//		
//		if (!StringUtility.isNullOrEmpty(productCode)) {
//			sql.append(" and exists (select 1 from product p where p.product_id=prd.product_id and p.product_code like ? ESCAPE '/')");
//			params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase()));
//		}
//		if (!StringUtility.isNullOrEmpty(productName)) {
//			sql.append(" and lower(prd.product_name) like ? ESCAPE '/'");
//			params.add(StringUtility.toOracleSearchLike(productName.toLowerCase()));
//		}
//		if (!StringUtility.isNullOrEmpty(promotionProgramCode)) {
//			sql.append(" and exists (select 1 from promotion_program_detail ppd where ppd.product_id=prd.product_id and exists(select 1 from promotion_program pp where pp.promotion_program_id=ppd.promotion_program_id and promotion_program_code like ? ESCAPE '/'))");
//			params.add(StringUtility.toOracleSearchLikeSuffix(promotionProgramCode.toUpperCase()));
//		}
//
//		if (catId != null) {
//			sql.append(" and exists (select 1 from product_level pl where prd.product_level_id=pl.product_level_id and pl.CAT_ID=?)");
//			params.add(catId);
//		}
//		if (subCatId != null) {
//			sql.append(" and exists (select 1 from product_level pl where prd.product_level_id=pl.product_level_id and pl.SUB_CAT_ID=?)");
//			params.add(subCatId);
//		}
//		if (fromQuantity != null) {
//			sql.append("  and exists (select 1 from stock_total st where st.product_id = prd.product_id and object_id = ? and object_type = ? and st.available_quantity >= ?) ");
//			params.add(ownerId);
//			params.add(ownerType.getValue());
//			params.add(fromQuantity);
//		}
//		if (toQuantity != null) {
//			sql.append("  and exists (select 1 from stock_total st where st.product_id = prd.product_id and object_id = ? and object_type = ? and st.available_quantity <= ?) ");
//			params.add(ownerId);
//			params.add(ownerType.getValue());
//			params.add(toQuantity);
//		}
//		if (exceptProductId != null) {
//			sql.append(" and prd.product_id not in (-1");
//			for (Long id: exceptProductId) {
//				if (id != null) {
//					sql.append(",?");
//					params.add(id);
//				}
//			}
//			sql.append(")");
//		}
//		
//		selectSql.append(sql.toString());
//		selectSql.append(" order by productCode");
//		selectParams.addAll(params);
//		countSql.append(sql.toString());
//		String[] fieldNames = {"productId", "productCode", "productName", "availableQuantity", "quantity", "price", "convfact","grossWeight", "uom1", "checkLot", "cat", "subCat", "stockTotalId"};
//		Type[] fieldTypes = {StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG};
//		if (kPaging == null)
//			return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, selectSql.toString(), selectParams);
//		else
//			return repo.getListByQueryAndScalarPaginated(StockTotalVO.class, fieldNames, fieldTypes, selectSql.toString(), countSql.toString(), selectParams, params, kPaging);
//	}
	
	@Override
	public List<StockTotal> getListStockTotal(
			KPaging<StockTotal> kPaging, String productCode, String productName,
			String promotionProgramCode, Long ownerId, StockObjectType ownerType,
			Long catId, Long subCatId, Integer fromQuantity, Integer toQuantity,
			Long cycleCountId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from stock_total st join product p on st.product_id=p.product_id where 1=1 ");
		
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and p.product_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" and lower(p.product_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(productName.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(promotionProgramCode)) {
			sql.append(" and exists (select 1 from promotion_program_detail ppd where ppd.product_id=st.product_id and exists(select 1 from promotion_program pg where pg.promotion_program_id=ppd.promotion_program_id and promotion_program_code like ? ESCAPE '/'))");
			params.add(StringUtility.toOracleSearchLikeSuffix(promotionProgramCode.toUpperCase()));
		}
		if (ownerId != null) {
			sql.append(" and object_id=?");
			params.add(ownerId);
		}
		if (ownerType != null) {
			sql.append(" and object_type=?");
			params.add(ownerType.getValue());
		}
		if (catId != null) {
			sql.append(" and exists (select 1 from product_level pp where p.product_level_id=pp.product_level_id and pp.CAT_ID=?)");
			params.add(catId);
		}
		if (subCatId != null) {
			sql.append(" and exists (select 1 from product_level pp where p.product_level_id=pp.product_level_id and pp.SUB_CAT_ID=?)");
			params.add(subCatId);
		}
		if (fromQuantity != null) {
			sql.append(" and st.quantity>=?");
			params.add(fromQuantity);
		}
		if (toQuantity != null) {
			sql.append(" and st.quantity<=?");
			params.add(toQuantity);
		}
		if (cycleCountId != null) {
			sql.append(" and not exists (select 1 from cycle_count_map_product ccm where ccm.cycle_count_id=? and ccm.product_id=p.product_id)");
			params.add(cycleCountId);
		}
		sql.append(" order by p.product_code");
		if (kPaging == null)
			return repo.getListBySQL(StockTotal.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StockTotal.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public List<StockTotal> getListStockTotalNew( KPaging<StockTotal> kPaging, StockTotalVOFilter stockTotalVoFilter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from stock_total st join product p on st.product_id=p.product_id AND st.quantity>0 where 1=1 ");
		
		if (!StringUtility.isNullOrEmpty(stockTotalVoFilter.getProductCode())) {
			sql.append(" and p.product_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(stockTotalVoFilter.getProductCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(stockTotalVoFilter.getProductName())) {
			sql.append(" and lower(p.product_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(stockTotalVoFilter.getProductName().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(stockTotalVoFilter.getPromotionProgramCode())) {
			sql.append(" and exists (select 1 from promotion_program_detail ppd where ppd.product_id=st.product_id and exists(select 1 from promotion_program pg where pg.promotion_program_id=ppd.promotion_program_id and promotion_program_code like ? ESCAPE '/'))");
			params.add(StringUtility.toOracleSearchLikeSuffix(stockTotalVoFilter.getPromotionProgramCode().toUpperCase()));
		}
		if (stockTotalVoFilter.getOwnerId() != null) {
			sql.append(" and object_id=?");
			params.add(stockTotalVoFilter.getOwnerId());
		}
		if (stockTotalVoFilter.getOwnerType() != null) {
			sql.append(" and object_type=?");
			params.add(stockTotalVoFilter.getOwnerType().getValue());
		}
		if (stockTotalVoFilter.getCatId() != null) {
			sql.append(" and exists (select 1 from product_level pp where p.product_level_id=pp.product_level_id and pp.CAT_ID=?)");
			params.add(stockTotalVoFilter.getCatId());
		}
		if (stockTotalVoFilter.getSubCatId() != null) {
			sql.append(" and exists (select 1 from product_level pp where p.product_level_id=pp.product_level_id and pp.SUB_CAT_ID=?)");
			params.add(stockTotalVoFilter.getSubCatId());
		}
		if (stockTotalVoFilter.getFromQuantity() != null) {
			sql.append(" and st.quantity>=?");
			params.add(stockTotalVoFilter.getFromQuantity());
		}
		if (stockTotalVoFilter.getToQuantity() != null) {
			sql.append(" and st.quantity<=?");
			params.add(stockTotalVoFilter.getToQuantity());
		}
		if (stockTotalVoFilter.getCycleCountId() != null) {
			sql.append(" and not exists (select 1 from cycle_count_map_product ccm where ccm.cycle_count_id=? and ccm.product_id=p.product_id)");
			params.add(stockTotalVoFilter.getCycleCountId());
		}
		/** vuongmq them warehouseID and product ID and stock_total_id*/
		if(stockTotalVoFilter.getProductId() != null) {
			sql.append(" and st.product_id = ? ");
			params.add(stockTotalVoFilter.getProductId());
		}
		if(stockTotalVoFilter.getWarehouseId() != null) {
			sql.append(" and st.warehouse_id = ? ");
			params.add(stockTotalVoFilter.getWarehouseId());
		}
		if(stockTotalVoFilter.getStockTotalId() != null) {
			sql.append(" and st.stock_total_id = ? ");
			params.add(stockTotalVoFilter.getStockTotalId());
		}
		/** vuongmq END them warehouseID and product ID*/
		sql.append(" order by p.product_code");
		if (kPaging == null)
			return repo.getListBySQL(StockTotal.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StockTotal.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public List<StockTotal> getListStockTotalNewQuantity( KPaging<StockTotal> kPaging, StockTotalVOFilter stockTotalVoFilter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from stock_total st ");
		sql.append(" where st.status = ? "); 
		params.add(ActiveType.RUNNING.getValue());
		
		if (stockTotalVoFilter.getOwnerId() != null) {
			sql.append(" and st.object_id=?");
			params.add(stockTotalVoFilter.getOwnerId());
		}
		if (stockTotalVoFilter.getOwnerType() != null) {
			sql.append(" and st.object_type=?");
			params.add(stockTotalVoFilter.getOwnerType().getValue());
		}
		if (stockTotalVoFilter.getFromQuantity() != null) {
			sql.append(" and st.quantity>=?");
			params.add(stockTotalVoFilter.getFromQuantity());
		}
		if (stockTotalVoFilter.getToQuantity() != null) {
			sql.append(" and st.quantity<=?");
			params.add(stockTotalVoFilter.getToQuantity());
		}
		/** vuongmq them warehouseID and product ID and stock_total_id*/
		if(stockTotalVoFilter.getProductId() != null) {
			sql.append(" and st.product_id = ? ");
			params.add(stockTotalVoFilter.getProductId());
		}
		if(stockTotalVoFilter.getWarehouseId() != null) {
			sql.append(" and st.warehouse_id = ? ");
			params.add(stockTotalVoFilter.getWarehouseId());
		}
		if(stockTotalVoFilter.getStockTotalId() != null) {
			sql.append(" and st.stock_total_id = ? ");
			params.add(stockTotalVoFilter.getStockTotalId());
		}
		/** vuongmq END them warehouseID and product ID*/
		if (kPaging == null)
			return repo.getListBySQL(StockTotal.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StockTotal.class, sql.toString(), params, kPaging);
	}
	/**
	 * @author 
	 *	AVAILABLE_QUANTITY => QUANTITY  > 0 
	 * @see tientv
	 */

	@Override
	public boolean checkQuantityInStockTotal(long shopId, long productId,
			int quantity) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select count(1) count from stock_total st"); 
		sql.append(" where st.object_id = ? and st.object_type = ?");
		params.add(shopId);
		params.add(StockObjectType.SHOP.getValue());
		sql.append(" and st.product_id = ?");
		params.add(productId);
		/*sql.append(" and st.quantity > 0 and st.quantity >= ?");
		params.add(quantity);*/
		sql.append(" and st.quantity > 0 ");
		
		return repo.countBySQL(sql.toString(), params) > 0 ? true : false;
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.StockTotalDAO#getStockTotalFromNPP(java.lang.Long)
	 */
	@Override
	public StockTotal getStockTotalFromNPPandProductId(Long ownerId, Long productId)
			throws DataAccessException {
		String sql = "select * from stock_total where object_id = ? and object_type = ? and product_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(ownerId);
		params.add(StockObjectType.SHOP.getValue());
		params.add(productId);
		return repo.getEntityBySQL(StockTotal.class, sql, params);
	}

	@Override
	public int updateAvailableQuatity(Long productId,
			StockObjectType ownerType, Long ownerId, Integer avaiQuantity)
			throws DataAccessException {

		String sql = "update stock_total set AVAILABLE_QUANTITY = AVAILABLE_QUANTITY + ? where object_id = ? and object_type = ? and product_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(avaiQuantity);
		params.add(ownerId);
		params.add(ownerType.getValue());
		params.add(productId);
		return repo.executeSQLQuery(sql.toString(), params);
	}

	@Override
	public List<RptCustomerStock2VO> getListRptCustomerStockVO(Long shopId,
			Date date) throws DataAccessException {
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT pi.product_info_code   			AS productInfoCode,");
		sql.append("   pd.product_code             			AS productCode,");
		sql.append("   pd.product_name             			AS productName,");
		sql.append("   pr.price                    			AS price,");
		sql.append("   pd.uom1                     			AS uom1,");
		sql.append("   pd.uom2                     			AS uom2,");
		sql.append("   pd.convfact                 			AS convfact,");
		sql.append("   SUM(NVL(st.quantity, 0))            	AS quantity,");
		sql.append("   SUM(NVL(st.quantity, 0)) * pr.price 	AS amount");
		sql.append(" FROM stock_total st,");
		sql.append("   product pd,");
		sql.append("   product_level pl,");
		sql.append("   product_info pi,");
		sql.append("   price pr,");
		sql.append("   customer c");
		sql.append(" WHERE st.product_id      = pd.product_id");
		sql.append(" AND pd.status            = ?");
		params.add(ActiveType.RUNNING.getValue());
		
		sql.append(" AND (st.create_date >= trunc(?) AND st.create_date < trunc(?) + 1)");
		params.add(date);
		params.add(date);
		
		sql.append(" AND pd.product_level_id  = pl.product_level_id");
		sql.append(" AND pl.cat_id            = pi.product_info_id");
		sql.append(" AND pi.type              = ?");
		params.add(ProductType.CAT.getValue());
		
		sql.append(" AND pi.status            = ?");
		params.add(ActiveType.RUNNING.getValue());
		
		sql.append(" AND pd.product_id        = pr.product_id");
		sql.append(" AND pr.from_date < TRUNC(sysdate) + 1");
		sql.append(" AND (pr.to_date         IS NULL");
		sql.append(" OR to_date       >= TRUNC(sysdate))");
		sql.append(" AND c.customer_id        = st.object_id");
		sql.append(" AND st.object_type        = ?");
		params.add(StockObjectType.CUSTOMER.getValue());
		
		sql.append(" AND c.shop_id            = ?");
		params.add(shopId);
		
		sql.append(" GROUP BY pi.product_info_code,");
		sql.append("   pd.product_code,");
		sql.append("   pd.product_name,");
		sql.append("   pr.price,");
		sql.append("   pd.uom1,");
		sql.append("   pd.uom2,");
		sql.append("   pd.convfact");
		sql.append(" HAVING SUM(NVL(st.quantity, 0)) > 0");
		sql.append(" ORDER BY pi.product_info_code, pd.product_code");

		String[] fieldNames = new String[] {//
		"productInfoCode",// 1
				"productCode",// 2
				"productName",// 3
				"price",// 4
				"uom1",// 5
				"uom2",// 6
				"convfact",// 7
				"quantity",// 8
				"amount",// 9
		};
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.BIG_DECIMAL,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.INTEGER,// 8
				StandardBasicTypes.BIG_DECIMAL,// 9
		};
		return repo.getListByQueryAndScalar(RptCustomerStock2VO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public boolean checkQuantityInStockTotal(long shopId, String productCode,
			int quantity) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) count from stock_total st"); 
		sql.append(" where st.object_id = ? and st.object_type = ?");
		params.add(StockObjectType.SHOP.getValue());
		params.add(shopId);
		sql.append(" and exists (select * from product p where p.product_id = st.product_id and p.product_code = ?)");
		params.add(productCode);
		sql.append(" and st.available_quantity > 0 and st.available_quantity >= ?");
		params.add(quantity);
		return repo.countBySQL(sql.toString(), params) > 0 ? true : false;
	}

	@Override
	public StockTotal getStockTotalByProductCodeAndOwner(String productCode, StockObjectType ownerType, Long ownerId) throws DataAccessException {
		String sql = "select * from stock_total st where object_id = ? and object_type = ? and exists (select * from product p where p.product_id = st.product_id and p.product_code = ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(ownerId);
		params.add(ownerType.getValue());
		params.add(productCode);
		return repo.getEntityBySQL(StockTotal.class, sql, params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.StockTotalDAO#getListProductInStockTotal(java.lang.Long, ths.dms.core.entities.enumtype.StockObjectType, java.lang.Integer)
	 */
	@Override
	public List<ProductLotVO> getListProductInStockTotal(Long shopId,Long ownerId,
			StockObjectType ownerType, Integer fromQuantity)
			throws DataAccessException {
		// TODO Auto-generated method stub
		Date appDate=commonDAO.getSysDate();
		if(shopId!=null){
			appDate = shopLockDAO.getApplicationDate(shopId);
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select '' lot, sum(st.quantity) quantity, sum(st.available_quantity) availableQuantity,");
		sql.append(" p.product_id productId, p.product_code productCode, p.product_name productName,");
		sql.append(" p.net_weight netWeight, p.gross_weight grossWeight, p.convfact convfact, pr.price price, p.check_lot checkLot");
		sql.append(" from stock_total st, product p, price pr where st.object_id = ? and st.object_type in (4,?) and st.quantity > ?");
		params.add(ownerId);
		params.add(ownerType.getValue());
		params.add(fromQuantity);
		sql.append(" and st.product_id = p.product_id");
		sql.append(" and pr.product_id = p.product_id");
		sql.append(" and p.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and pr.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?))");
		params.add(appDate);
		params.add(appDate);
		sql.append(" group by p.product_id, p.product_code, p.product_name, p.net_weight, p.gross_weight, p.convfact, pr.price, p.check_lot");
		sql.append(" ORDER BY p.product_code");
		final String[] fieldNames = new String[] { "lot", "quantity", "availableQuantity"
				, "productId", "productCode", "productName", "netWeight", "grossWeight", "convfact", "price", "checkLot"};
		
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER
				, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING
				, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER};
		
		return repo.getListByQueryAndScalar(ProductLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.StockTotalDAO#getListProductForImExport(ths.dms.core.entities.enumtype.KPaging, java.lang.String, java.lang.String, java.lang.Long, ths.dms.core.entities.enumtype.StockObjectType, java.lang.Integer)
	 */
	@Override
	public List<StockTotalVO> getListProductForImExport(
			KPaging<StockTotalVO> kPaging, String productCode,
			String productName, Long objectId, StockObjectType objectType,
			Integer fromQuantity) throws DataAccessException {
		Date appDate = commonDAO.getSysDate();
		if(StockObjectType.SHOP.equals(objectType) && objectId!=null){
			appDate=shopLockDAO.getApplicationDate(objectId);
		}
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		List<Object> selectParams = new ArrayList<Object>();
		selectSql.append(" SELECT tb1.product_id productId,");
		selectSql.append(" tb1.product_code productCode,");
		selectSql.append(" tb1.product_name productName,");
		selectSql.append(" tb1.convfact convfact,");
		selectSql.append(" tb1.gross_weight AS grossWeight,");
		selectSql.append(" tb1.uom1 uom1,");
		selectSql.append(" tb1.check_lot checkLot,");
		selectSql.append(" tb1.price price,");
		selectSql.append(" tb2.stock_total_id stockTotalId,");
		selectSql.append(" tb2.quantity quantity,");
		selectSql.append(" tb2.available_quantity availableQuantity,");
		selectSql.append(" tb1.product_info_code cat,");
		selectSql.append(" tb4.product_info_code subCat");
		
		countSql.append(" select count(1) as count ");
		
		sql.append(" FROM");
		sql.append(" (SELECT p.*,");
		sql.append(" pr.price,");
		sql.append(" pi.product_info_code");
		sql.append(" FROM product p,");
		sql.append(" price pr,");
		sql.append(" product_info pi");
		sql.append(" WHERE p.status    = ?");
		sql.append(" AND pr.status     = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND p.cat_id      = pi.product_info_id");
		sql.append(" AND pr.product_id = p.product_id");
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and lower(p.product_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(productCode.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" and lower(p.product_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(productName.toLowerCase()));
		}
		sql.append(" AND pr.from_date  < TRUNC(?) + 1");
		sql.append(" AND (pr.to_date  IS NULL");
		sql.append(" OR pr.to_date    >= TRUNC(?))");
		params.add(appDate);
		params.add(appDate);
		sql.append(" ) tb1");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT product_info_id, product_info_code FROM product_info");
		sql.append(" ) tb4");
		sql.append(" ON tb1.sub_cat_id = tb4.product_info_id");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT *");
		sql.append(" FROM stock_total");
		sql.append(" WHERE object_id = ?");
		params.add(objectId);
		sql.append(" AND object_type = ?");
		params.add(objectType.getValue());
		if (fromQuantity != null) {
			sql.append(" AND quantity    > ?");
			params.add(fromQuantity);
		}
		sql.append(" ) tb2");
		sql.append(" ON tb1.product_id = tb2.product_id");
		sql.append(" ORDER BY tb1.product_code");
		
		selectSql.append(sql.toString());
		selectParams.addAll(params);
		countSql.append(sql.toString());
		String[] fieldNames = {"productId", "productCode", "productName", "availableQuantity", "quantity", "price", "convfact","grossWeight", "uom1", "checkLot", "cat", "subCat", "stockTotalId"};
		Type[] fieldTypes = {StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG};
		if (kPaging == null)
			return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, selectSql.toString(), selectParams);
		else
			return repo.getListByQueryAndScalarPaginated(StockTotalVO.class, fieldNames, fieldTypes, selectSql.toString(), countSql.toString(), selectParams, params, kPaging);
	}

	@Override
	public List<StockTotalVO> getListProductsCanSalesForShop(Long shopId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct(p.product_id) as productId, p.product_code as productCode, p.product_name as productName, p.convfact as convfact ");
		sql.append(" from product p ");
		sql.append(" join stock_total st on st.product_id = p.product_id and st.object_type=1 and object_id = ? ");
		params.add(shopId);
		sql.append(" where p.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		String[] fieldNames = {"productId", "productCode", "productName", "convfact",};
		Type[] fieldTypes = {StandardBasicTypes.LONG,StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public StockTotalVO getStockTotalByProductCodeAndOwnerEx(
			String productCode, StockObjectType ownerType, Long ownerId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select product_id as productId,  sum(quantity) as quantity");
		sql.append(" from stock_total st where object_id = ? and object_type in (4,?) and exists (select * from product p where p.product_id = st.product_id and p.product_code = ?)");
		sql.append(" group by object_id, product_id");
		params.add(ownerId);
		params.add(ownerType.getValue());
		params.add(productCode);
		String[] fieldNames = {"productId", "quantity"};
		Type[] fieldTypes = {StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER};
		List<StockTotalVO> lst = repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if(lst != null && lst.size() >0){
			return lst.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<StockTotal> getListStockTotalByProductAndOwnerForUpdate(
			long productId, StockObjectType ownerType, long ownerId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from stock_total where object_id= ? and product_id= ? and status = 1 ");
		List<Object> params = new ArrayList<Object>();
		params.add(ownerId);
		params.add(productId);
		if (ownerType != null) {
			sql.append("  and object_type in (4,?)");
			params.add(ownerType.getValue());
		}
		else
			sql.append("  and object_type is null");	
		sql.append(" for update");
		return repo.getListBySQL(StockTotal.class, sql.toString(), params);
	}
	
	/**
	 * @author nhanlt6	
	 * @throws DataAccessException
	 * @param shopId
	 * @description : Danh sach so luong san pham ban cho npp load theo kho
	 */
	@Override
	public List<StockTotalVO> getListProductsCanSalesForShopByWarehouse(Long shopId, Long productId, Long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/*sql.append(" select distinct(p.product_id) as productId,product_code as productCode, p.convfact as convfact, ");
		sql.append(" st.quantity as stockQuantity,st.available_quantity as availableQuantity,wh.warehouse_name as warehouseName,wh.warehouse_id as warehouseId, wh.seq as seq ");
		sql.append(" from product p ");
		sql.append(" join stock_total st on st.product_id = p.product_id and st.object_type=1 and object_id = ? ");
		params.add(shopId);
		sql.append(" join warehouse wh on st.warehouse_id = wh.warehouse_id and st.object_id = wh.shop_id ");
		sql.append(" where p.status = ? and st.product_id = ? order by p.product_code, seq");
		params.add(ActiveType.RUNNING.getValue());
		params.add(productId);*/
		
		sql.append(" select distinct(p.product_id) as productId, ");
		sql.append(" product_code as productCode, ");
		sql.append(" p.convfact as convfact, ");
		sql.append(" st.quantity as stockQuantity, ");
		sql.append(" st.available_quantity ");
		/*
		 * add sale order id to load subtracted quantity for sale product
		 * @modified by tuannd20
		 * @date 02/10/2014
		 */
		if (saleOrderId != null) {
			sql.append(" + case ");
			sql.append(" when exists (select 1 from sale_order_detail sod  ");
			sql.append(" 				join sale_order_lot sol on sod.sale_order_detail_id = sol.sale_order_detail_id  ");
			sql.append(" 				where sod.sale_order_id = ? and sod.product_id = p.product_id and sol.warehouse_id = wh.warehouse_id ");
			//sql.append(" 				and sod.is_free_item = 0 ");
			sql.append(" 				) ");
			params.add(saleOrderId);
			sql.append(" then ");
			sql.append(" (	select decode(sum(sol.quantity), null, 0, sum(sol.quantity)) ");
			sql.append(" 	from sale_order_detail sod ");
			sql.append(" 	join sale_order_lot sol on sod.sale_order_detail_id = sol.sale_order_detail_id ");
			sql.append(" 	where sod.sale_order_id = ? ");
			params.add(saleOrderId);
			//sql.append(" 	and sod.is_free_item = 0 ");
			sql.append(" 	and sod.product_id = p.product_id ");
			sql.append(" 	and sol.warehouse_id = wh.warehouse_id ");
			//sql.append(" 	and rownum = 1 ");
			sql.append(" ) ");
			sql.append(" else 0 ");
			sql.append(" end ");
		}		
		sql.append(" as availableQuantity, ");
		sql.append(" wh.warehouse_name as warehouseName, ");
		sql.append(" wh.warehouse_id as warehouseId, ");
		sql.append(" wh.seq as seq, wh.warehouse_type warehouseType ");
		sql.append(" from product p ");
		sql.append(" join stock_total st on st.product_id = p.product_id and st.object_type=1 and object_id = ? ");
		params.add(shopId);
		sql.append(" join warehouse wh on st.warehouse_id = wh.warehouse_id and st.object_id = wh.shop_id ");
		sql.append(" where p.status = ? and wh.status = ? and st.status = ?  ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		if(productId != null){
			sql.append(" and st.product_id = ? ");
			params.add(productId);
		}
		sql.append(" order by p.product_code, wh.warehouse_type, wh.seq ");
		String[] fieldNames = {"productId", "productCode", "convfact", "availableQuantity", "stockQuantity", "warehouseName", "warehouseId", "seq", "warehouseType"};
		Type[] fieldTypes = {StandardBasicTypes.LONG,StandardBasicTypes.STRING,StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER};
		return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<StockTotal> getListStockTotalByShopAndProduct(StockTotalVOFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select st.* ");
		sql.append(" from stock_total st ");
		sql.append(" join warehouse w on w.warehouse_id = st.warehouse_id ");
		sql.append(" where 1=1 ");
		if(filter.getShopId()!=null){
			sql.append(" and st.object_id=?");
			params.add(filter.getShopId());
		}
		if(filter.getLstProductId()!=null && filter.getLstProductId().size()>0){
			sql.append(" and st.product_id in (-1");
			for(Long id:filter.getLstProductId()){
				sql.append(",?");
				params.add(id);
			}
			sql.append(") ");
		}
		if(filter.getOwnerType()!=null){
			sql.append(" and st.object_type =? ");
			params.add(filter.getOwnerType().getValue());
		}
		if(filter.getWarehouseId()!=null){
			sql.append(" and st.warehouse_id =? ");
			params.add(filter.getWarehouseId());
		}
		if(filter.getStatus() != null){
			sql.append(" and st.status = ? ");
			params.add(filter.getStatus().getValue());
		}
		if (filter.getOrderByType() != null) {
			if (WarehouseType.SALES.getValue() == filter.getOrderByType().getValue()) {
				sql.append(" order by st.product_id, w.warehouse_type, w.seq");
			} else if (WarehouseType.PROMOTION.getValue() == filter.getOrderByType().getValue()) {
				sql.append(" order by st.product_id, w.warehouse_type desc, w.seq ");
			}
		} else {
			sql.append(" order by st.product_id, w.seq ");
		}
		return repo.getListBySQL(StockTotal.class, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public Warehouse getMostPriorityWareHouse(long shopId, Long productId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from (");
		sql.append(" select wh.*");
		sql.append(" from stock_total st");
		sql.append(" join warehouse wh on (wh.warehouse_id = st.warehouse_id and wh.status = ?)");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" where st.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and st.object_type = ? and wh.shop_id = ?");
		params.add(StockObjectType.SHOP.getValue());
		params.add(shopId);
		if (productId != null) {
			sql.append(" and st.product_id = ?");
			params.add(productId);
		}
		sql.append(" order by seq");
		sql.append(" ) where rownum = 1");
		
		return repo.getEntityBySQL(Warehouse.class, sql.toString(), params);
	}
	
	@Override
	public List<StockTotal> getListStockTotalByProductAndOwner(StockTotalFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from stock_total st ");
		sql.append(" left join warehouse w on w.warehouse_id = st.warehouse_id and w.shop_id = st.object_id ");
		sql.append(" where st.status = 1 ");
		if (filter.getOwnerId() != null) {
			sql.append(" and object_id = ? ");
			params.add(filter.getOwnerId());
		}
		if (filter.getOwnerType() != null) {
			sql.append(" and object_type = ? ");
			params.add(filter.getOwnerType().getValue());
		}
		if (filter.getProductId() != null) {
			sql.append(" and product_id = ?  ");
			params.add(filter.getProductId());
		}
		if (filter.getWarehouseType() != null) {
			sql.append(" and w.warehouse_type = ? ");
			params.add(filter.getWarehouseType().getValue());
		}
		if (filter.getOrderByType() != null) {
			if (filter.getOrderByType().getValue() == WarehouseType.SALES.getValue()) {
				sql.append(" order by w.warehouse_type, w.seq ");
			} else if (filter.getOrderByType().getValue() == WarehouseType.PROMOTION.getValue()) {
				sql.append(" order by w.warehouse_type desc, w.seq ");
			}
		} else {
			sql.append(" order by w.seq ");
		}

		return repo.getListBySQL(StockTotal.class, sql.toString(), params);

	}

	@Override
	public List<StockTotalVO> getListProductImportExport(StockTotalFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct prd.product_id as productId, ");
		sql.append(" prd.product_code AS productCode, ");
		sql.append(" prd.product_name AS productName,prd.convfact ");
		sql.append(" from product prd ");
		sql.append(" left join price pri on prd.product_id = pri.product_id ");
		sql.append(" and pri.status = 1 ");
		sql.append(" and from_date < trunc(sysdate) + 1 ");
		sql.append(" and (to_date >= trunc(sysdate) or to_date is null) ");
		sql.append(" where prd.status = 1 ");
		if(filter.getOwnerId() != null){
			sql.append(" and exists (select 1 from stock_total where product_id = prd.product_id and object_type = 1 and object_id = ?) ");
			params.add(filter.getOwnerId());
		}		
		sql.append(" order by productCode");
		String[] fieldNames = {"productId", "productCode", "productName","convfact"};		
		Type[] fieldTypes = {				
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,StandardBasicTypes.INTEGER
			};
		return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public StockTotal getStockTotalByProductAndOwner(long productId, 
			StockObjectType ownerType, long ownerId) throws DataAccessException {
		String sql = "select * from stock_total where object_id=? and object_type=? and product_id=?";
		List<Object> params = new ArrayList<Object>();
		params.add(ownerId);
		params.add(ownerType.getValue());
		params.add(productId);
		return repo.getEntityBySQL(StockTotal.class, sql, params);
	}

	@Override
	public List<StockTotalVO> getListStockTotalVOForImExport(KPaging<StockTotalVO> kPaging, String productCode, String productName, String promotionProgramCode, Long ownerId, StockObjectType ownerType, Long catId, Long subCatId,
			Integer fromQuantity, Integer toQuantity, List<Long> exceptProductId) throws DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<StockTotalVO> getListProductForCycleCount(KPaging<StockTotalVO> paging, long cycleCountId, StockTotalVOFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select stt.stock_total_id as stockTotalId,");
		sql.append(" p.product_id as productId, p.product_code as productCode, p.product_name as productName,");
		sql.append(" wh.warehouse_name as warehouseName, stt.available_quantity as availableQuantity, stt.quantity,");
		sql.append(" p.uom1, p.check_lot as checkLot, p.convfact,");
		sql.append(" (select product_info_code from product_info pi where pi.product_info_id = p.cat_id) as cat,");
		sql.append(" (select product_info_code from product_info pi where pi.product_info_id = p.sub_cat_id) as subCat");
		
		StringBuilder fromSql = new StringBuilder();
		fromSql.append(" from product p");
		fromSql.append(" join stock_total stt on (stt.product_id = p.product_id and stt.status = ?)");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" join warehouse wh on (wh.warehouse_id = stt.warehouse_id and wh.status = ?)");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" where p.status = ? and stt.object_type = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(StockObjectType.SHOP.getValue());
		
		fromSql.append(" and p.product_id not in (select product_id from cycle_count_map_product where cycle_count_id = ?)");
		params.add(cycleCountId);
		
		if (filter.getWarehouseId() != null) {
			fromSql.append(" and wh.warehouse_id = ?");
			params.add(filter.getWarehouseId());
		}
		
		if (filter.getShopId() != null) {
			fromSql.append(" and stt.object_id = ?");
			params.add(filter.getShopId());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			fromSql.append(" and product_code like ? escape '/'");
			String s = filter.getProductCode().toUpperCase().trim();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			fromSql.append(" and lower(product_name) like ? escape '/'");
			String s = filter.getProductName().toLowerCase().trim();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		
		if (filter.getExceptProductId() != null && filter.getExceptProductId().size() > 0) {
			fromSql.append(" and p.product_id not in (-1");
			List<Long> lstP = filter.getExceptProductId();
			for (Long pId : lstP) {
				fromSql.append(",?");
				params.add(pId);
			}
			fromSql.append(")");
		}
		if (filter.getCatId() != null && filter.getCatId() != 0) {
			fromSql.append(" and p.cat_id = ? ");
			params.add(filter.getCatId());
		}
		if (filter.getSubCatId() != null && filter.getSubCatId() != 0) {
			fromSql.append(" and p.sub_cat_id= ? ");
			params.add(filter.getSubCatId());
		}		
		if (filter.getFromQuantity()!=null ){
			fromSql.append("and stt.quantity >= ?");
			params.add(filter.getFromQuantity());
		}
		if (filter.getToQuantity()!= null ){
			fromSql.append("and stt.quantity <=?");
			params.add(filter.getToQuantity());
		}
		sql.append(fromSql.toString());	
		sql.append(" order by productCode, productName");		
		String fieldNames[] = {
				"stockTotalId", "productId",
				"productCode", "productName",
				"warehouseName",
				"availableQuantity", "quantity",
				"uom1", "checkLot", "convfact",
				"cat", "subCat"
		};		
		Type fieldTypes[] = {
				StandardBasicTypes.LONG, StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING
		};		
		if (paging == null) {
			fromSql = null;
			List<StockTotalVO> lst = repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
			return lst;
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count ").append(fromSql.toString());
		fromSql = null;
		List<StockTotalVO> lst = repo.getListByQueryAndScalarPaginated(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, paging);
		return lst;
	}
	/**
	 * @author hoanv25
	 *truong hop ds mat hang kiem ke all
	 */
	@Override
	public List<StockTotalVO> getListProductForCycleCountCategoryStock(KPaging<StockTotalVO> paging, long cycleCountId, StockTotalVOFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select stt.stock_total_id as stockTotalId,");
		sql.append(" p.product_id as productId, p.product_code as productCode, p.product_name as productName,");
		sql.append(" wh.warehouse_name as warehouseName, stt.available_quantity as availableQuantity, stt.quantity,");
		sql.append(" p.uom1, p.check_lot as checkLot, p.convfact,");
		sql.append(" (select product_info_code from product_info pi where pi.product_info_id = p.cat_id) as cat,");
		sql.append(" (select product_info_code from product_info pi where pi.product_info_id = p.sub_cat_id) as subCat");
		
		StringBuilder fromSql = new StringBuilder();
		fromSql.append(" from product p");
		fromSql.append(" join stock_total stt on (stt.product_id = p.product_id and stt.status = ?)");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" join warehouse wh on (wh.warehouse_id = stt.warehouse_id and wh.status = ?)");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" where p.status = ? and stt.object_type = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(StockObjectType.SHOP.getValue());
		
		/*fromSql.append(" and p.product_id not in (select product_id from cycle_count_map_product where cycle_count_id = ?)");
		params.add(cycleCountId);
		*/
		if (filter.getWarehouseId() != null) {
			fromSql.append(" and wh.warehouse_id = ?");
			params.add(filter.getWarehouseId());
		}
		
		if (filter.getShopId() != null) {
			fromSql.append(" and stt.object_id = ?");
			params.add(filter.getShopId());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			fromSql.append(" and product_code like ? escape '/'");
			String s = filter.getProductCode().toUpperCase().trim();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			fromSql.append(" and lower(product_name) like ? escape '/'");
			String s = filter.getProductName().toLowerCase().trim();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		
		if (filter.getExceptProductId() != null && filter.getExceptProductId().size() > 0) {
			fromSql.append(" and p.product_id not in (-1");
			List<Long> lstP = filter.getExceptProductId();
			for (Long pId : lstP) {
				fromSql.append(",?");
				params.add(pId);
			}
			fromSql.append(")");
		}
		if (filter.getCatId() != null && filter.getCatId() != 0) {
			fromSql.append(" and p.cat_id = ? ");
			params.add(filter.getCatId());
		}
		if (filter.getSubCatId() != null && filter.getSubCatId() != 0) {
			fromSql.append(" and p.sub_cat_id= ? ");
			params.add(filter.getSubCatId());
		}		
		if (filter.getFromQuantity()!=null ){
			fromSql.append("and stt.quantity >= ?");
			params.add(filter.getFromQuantity());
		}
		if (filter.getToQuantity()!= null ){
			fromSql.append("and stt.quantity <=?");
			params.add(filter.getToQuantity());
		}
		sql.append(fromSql.toString());	
		sql.append(" order by productCode, productName");		
		String fieldNames[] = {
				"stockTotalId", "productId",
				"productCode", "productName",
				"warehouseName",
				"availableQuantity", "quantity",
				"uom1", "checkLot", "convfact",
				"cat", "subCat"
		};		
		Type fieldTypes[] = {
				StandardBasicTypes.LONG, StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING
		};		
		if (paging == null) {
			fromSql = null;
			List<StockTotalVO> lst = repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
			return lst;
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count ").append(fromSql.toString());
		fromSql = null;
		List<StockTotalVO> lst = repo.getListByQueryAndScalarPaginated(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, paging);
		return lst;
	}

	@Override
	public List<StockTotalVO> retrieveStockTotalsForSaleOrder(Long saleOrderId, Boolean isSubtractProductQuantityInApprovedOrder)
			throws DataAccessException {
		if (saleOrderId == null) {
			throw new IllegalArgumentException("saleOrderId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select st.stock_total_id stockTotalId, ");
		sql.append("   st.warehouse_id warehouseId, ");
		sql.append("   st.product_id productId, ");
		sql.append("   st.available_quantity availableQuantity, ");
		if (isSubtractProductQuantityInApprovedOrder) {
			sql.append("   st.quantity - ");
			sql.append("       nvl( (select sum(case ");
			sql.append("       					when exists (select 1 from sale_order where sale_order_id = sol.sale_order_id and order_type = ?) then sol.quantity ");
			params.add(OrderType.IN.getValue());
			sql.append("       					when exists (select 1 from sale_order where sale_order_id = sol.sale_order_id and order_type = ?) then -sol.quantity ");
			params.add(OrderType.CM.getValue());
			sql.append("       					else 0 ");
			sql.append("       					end ");
			sql.append("       				) ");
			sql.append("         from sale_order_lot sol ");
			sql.append("         where exists ( ");
			sql.append("           select 1 ");
			sql.append("           from sale_order so ");
			sql.append("           where so.sale_order_id = sol.sale_order_id ");
			sql.append("           and so.approved = ? ");
			params.add(SaleOrderStatus.APPROVED.getValue());
			sql.append("           and so.approved_step = ? ");
			params.add(SaleOrderStep.PRINT_CONFIRMED.getValue());
			sql.append("           and so.order_date >= (select trunc(order_date) from sale_order where sale_order_id = ?) ");
			params.add(saleOrderId);
			sql.append("           and so.order_date < (select trunc(order_date + 1) from sale_order where sale_order_id = ?) ");
			params.add(saleOrderId);
			sql.append("         ) ");
			sql.append("         and sol.product_id = st.product_id ");
			sql.append("         and sol.warehouse_id = st.warehouse_id) ");
			sql.append("       , 0) quantity ");			
		} else {
			sql.append("   st.quantity quantity ");
		}
		sql.append(" from stock_total st ");
		sql.append(" where 1=1 ");
		sql.append(" and st.object_type = ? ");
		params.add(StockObjectType.SHOP.getValue());
		sql.append(" and st.object_id = (select shop_id from sale_order where sale_order_id = ?) ");
		params.add(saleOrderId);
		sql.append(" and exists ( ");
		sql.append("     select 1 ");
		sql.append("     from sale_order_lot sol ");
		sql.append("     where sol.sale_order_id = ? ");
		params.add(saleOrderId);
		sql.append("     and sol.warehouse_id = st.warehouse_id ");
		sql.append("     and sol.product_id = st.product_id ");
		sql.append(" ) ");
		
		String fieldNames[] = {
			"stockTotalId",
			"warehouseId",
			"productId",
			"availableQuantity",
			"quantity",
		};		
		Type fieldTypes[] = {
			StandardBasicTypes.LONG, 
			StandardBasicTypes.LONG,
			StandardBasicTypes.LONG,
			StandardBasicTypes.INTEGER,
			StandardBasicTypes.INTEGER,
		};
		
		return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString().replaceAll("  ", " "), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<StockTotal> getListStockTotalByProductAndShop(StockTotalFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select st.create_date, st.create_user, st.descr, st.object_type, st.object_id,");
		sql.append(" st.product_id, st.quantity, st.stock_total_id, st.update_date, st.update_user,");
		sql.append(" st.warehouse_id, st.approved_quantity, st.status, st.shop_id,");
		if (filter.getOrderId() == null) {
			sql.append(" st.available_quantity");
		} else {
			sql.append(" (st.available_quantity +");
			sql.append(" (select nvl(sum(quantity), 0) from sale_order_lot where sale_order_id = ? ");
			params.add(filter.getOrderId());
			if (filter.getProductId() != null) {
				sql.append(" and product_id = ? ");
				params.add(filter.getProductId());
			}
			sql.append(" and warehouse_id = w.warehouse_id) ");
			sql.append(" ) as available_quantity");
		}
		sql.append(" from stock_total st");
		sql.append(" join warehouse w on (w.warehouse_id = st.warehouse_id and w.shop_id = st.object_id)");
		sql.append(" where st.status = ? and st.object_type= ? ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(StockObjectType.SHOP.getValue());
		if (filter.getOwnerId() != null) {
			sql.append(" and st.object_id = ? ");
			params.add(filter.getOwnerId());
		}
		if (filter.getProductId() != null) {
			sql.append(" and st.product_id= ? ");
			params.add(filter.getProductId());
		}
		if (filter.getWarehouseType() != null) {
			sql.append(" and w.warehouse_type = ? ");
			params.add(filter.getWarehouseType().getValue());
		}
		if (filter.getOrderByType() != null) {
			if (filter.getOrderByType().getValue() == WarehouseType.SALES.getValue()) {
				sql.append(" order by w.warehouse_type, w.seq ");
			} else if (filter.getOrderByType().getValue() == WarehouseType.PROMOTION.getValue()) {
				sql.append(" order by w.warehouse_type desc, w.seq ");
			}
		} else {
			sql.append(" order by w.seq ");
		}

		List<StockTotal> lst = repo.getListBySQL(StockTotal.class, sql.toString(), params);
		return lst;
	}
	
	@Override
	public List<StockTotal> getListStockTotalByCycleCount(CycleCount cycleCount) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT st.* ");
		sql.append(" FROM CYCLE_COUNT cc ");
		sql.append(" JOIN CYCLE_COUNT_MAP_PRODUCT ccmp on cc.CYCLE_COUNT_ID = ccmp.CYCLE_COUNT_ID ");
		sql.append(" JOIN STOCK_TOTAL st on cc.WAREHOUSE_ID = st.WAREHOUSE_ID and ccmp.PRODUCT_ID = st.PRODUCT_ID ");
		sql.append(" where st.STATUS = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND cc.CYCLE_COUNT_ID = ? ");
		params.add(cycleCount.getId());
		
		List<StockTotal> lst = repo.getListBySQL(StockTotal.class, sql.toString(), params);
		return lst;
	}
}

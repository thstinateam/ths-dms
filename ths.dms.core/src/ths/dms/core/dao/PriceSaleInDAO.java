package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.PriceSaleIn;
import ths.dms.core.entities.filter.PriceFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PriceVO;
import ths.dms.core.exceptions.DataAccessException;

public interface PriceSaleInDAO {

	/**
	 * 
	 * Tao moi PriceSaleIn
	 * @author trietptm
	 * @param priceSaleIn
	 * @param logInfo
	 * @return PriceSaleIn
	 * @throws DataAccessException
	 * @since Jan 21, 2016
	 */
	PriceSaleIn createPriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws DataAccessException;
	
	/**
	 * 
	 * Xu ly cap nhat PriceSaleIn
	 * @author trietptm
	 * @param priceSaleIn
	 * @param logInfo
	 * @throws DataAccessException
	 * @since Jan 21, 2016
	 */
	void updatePriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws DataAccessException;
	
	/**
	 * Xu ly cap nhat danh sach PriceSaleIn
	 * @author trietptm
	 * @param lst
	 * @param logInfo
	 * @throws DataAccessException
	 * @since Jan 25, 2016
	*/
	void updatePriceSaleIn(List<PriceSaleIn> lst, LogInfoVO logInfo) throws DataAccessException;
	
	/**
	 * 
	 * Xu ly xoa PriceSaleIn
	 * @author trietptm
	 * @param priceSaleIn
	 * @param logInfo
	 * @throws DataAccessException
	 * @since Jan 21, 2016
	 */
	void deletePriceSaleIn(PriceSaleIn priceSaleIn, LogInfoVO logInfo) throws DataAccessException;
	
	/**
	 * 
	 * Lay PriceSaleIn theo id
	 * @author trietptm
	 * @param id
	 * @return
	 * @throws DataAccessException
	 * @since Jan 21, 2016
	 */
	PriceSaleIn getPriceSaleInById(long id) throws DataAccessException;

	/**
	 * Xu ly tim kiem gia sale in
	 * @author trietptm
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @since Jan 20, 2016
	*/
	List<PriceVO> searchPriceSaleInVOByFilter(PriceFilter<PriceVO> filter) throws DataAccessException;

	/**
	 * lay ds PriceSaleIn theo filter
	 * @author trietptm
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @since Jan 22, 2016
	*/
	List<PriceSaleIn> getListPriceSaleInByFilter(PriceFilter<PriceSaleIn> filter) throws DataAccessException;
}

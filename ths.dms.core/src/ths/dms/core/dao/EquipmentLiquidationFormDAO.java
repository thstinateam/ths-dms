package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.EquipLiquidationForm;
import ths.dms.core.entities.EquipLiquidationFormDtl;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.vo.EquipmentLiquidationFormVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
/**
 * EquipmentLiquidationFormDAO
 * 
 * @author nhutnn
 * @since 15/12/2014
 * @description: Lop DAO quan ly bien ban thanh ly thiet bi
 */
public interface EquipmentLiquidationFormDAO {
	
	/**
	 * Them moi bien ban
	 * @author nhutnn
	 * @since 26/12/2014
	 * @param equipLiquidationForm
	 * @throws DataAccessException
	 */
	EquipLiquidationForm createEquipLiquidationForm(EquipLiquidationForm equipLiquidationForm) throws DataAccessException;
	
	/**
	 * Chinh sua bien ban 
	 * @author nhutnn
	 * @since 26/12/2014
	 * @param equipLiquidationForm
	 * @return
	 * @throws DataAccessException
	 */
	void updateEquipLiquidationForm(EquipLiquidationForm equipLiquidationForm) throws DataAccessException;
			
	/**
	 * Them moi thiet bi vao bien ban
	 * @author nhutnn
	 * @since 26/12/2014
	 * @param equipLiquidationFormDtl
	 * @throws DataAccessException
	 */
	EquipLiquidationFormDtl createEquipLiquidationFormDetail(EquipLiquidationFormDtl equipLiquidationFormDtl) throws DataAccessException;
	
	/**
	 * Xoa thiet bi trong bien ban 
	 * @author nhutnn
	 * @since 26/12/2014
	 * @param equipLiquidationFormDtl
	 * @return
	 * @throws DataAccessException
	 */
	void deleteEquipLiquidationFormDetail(EquipLiquidationFormDtl equipLiquidationFormDtl) throws DataAccessException;
	
	/**
	 * Lay danh sach bien ban thanh ly
	 * @author nhutnn
	 * @since 26/12/2014
	 * @param equipmentFilter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipmentLiquidationFormVO> getListEquipmentLiquidationFormVOByFilter(EquipmentFilter<EquipmentLiquidationFormVO> filter) throws DataAccessException;
	
	/**
	 * Lay  ma bien ban
	 * @author nhutnn
	 * @since 26/12/2014
	 * @return
	 * @throws DataAccessException
	 */
	String getCodeEquipLiquidation() throws DataAccessException;
	
	/**
	 * Lay bien ban VO
	 * @author nhutnn
	 * @since 26/12/2014
	 * @param idForm
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 */
	EquipmentLiquidationFormVO getEquipmentLiquidationFormVOById(Long idForm, Long shopId) throws DataAccessException;
	
	/**
	 * Lay bien ban
	 * @author nhutnn
	 * @since 26/12/2014
	 * @param idForm
	 * @return
	 * @throws DataAccessException
	 */
	EquipLiquidationForm getEquipmentLiquidationFormById(Long idForm) throws DataAccessException;
	
	/**
	 * Lay danh sach chi tiet bien ban
	 * @author nhutnn
	 * @since 26/12/2014
	 * @param idForm
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipLiquidationFormDtl> getListEquipLiquidationFormDtlByEquipLiquidationFormId(Long idForm) throws DataAccessException;
	
	/**
	 * Lay chi tiet bien ban
	 * @author nhutnn
	 * @since 26/12/2014
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	EquipLiquidationFormDtl getEquipLiquidationFormDtlByFilter(EquipmentFilter<EquipLiquidationFormDtl> filter) throws DataAccessException;
	
	/**
	 * Chinh sua chi tiet bien ban 
	 * @author nhutnn
	 * @since 26/12/2014
	 */
	void updateEquipLiquidationFormDtl(EquipLiquidationFormDtl equipLiquidationFormDtl) throws DataAccessException;

	/**
	 * Lay danh sach bien ban thanh ly cho export
	 * 
	 * @author tamvnm
	 * @since 12/01/2015
	 * @param equipmentFilter
	 * @throws BusinessException
	 * 
	 * vuongmq; 14/07/2015;
	 * lay danh sach theo filter danh sach Id khi export
	 */
	List<EquipmentLiquidationFormVO> getListEquipmentLiquidationFormVOForExport(EquipmentFilter<EquipmentLiquidationFormVO> filter) throws DataAccessException;

	/**
	 * Lay EquipLiquidationForm theo id va shopId
	 * @author trietptm
	 * @param idForm
	 * @param shopId
	 * @return EquipLiquidationForm
	 * @throws DataAccessException
	 * @since Apr 14, 2016
	*/
	EquipLiquidationForm getEquipmentLiquidationFormById(Long idForm, Long shopId) throws DataAccessException;
}

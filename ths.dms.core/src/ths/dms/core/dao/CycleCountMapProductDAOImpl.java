package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.CycleCountMapProduct;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.CycleCountType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.MapProductFilter;
import ths.dms.core.entities.vo.CycleCountDiffVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.MapProductVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.core.entities.vo.rpt.RptCountStoreVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class CycleCountMapProductDAOImpl implements CycleCountMapProductDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	StockTotalDAO stockTotalDAO;
	
	@Override
	public CycleCountMapProduct createCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct, Boolean isUpdateQuantityBfrCount, LogInfoVO logInfo) throws DataAccessException {
		if (cycleCountMapProduct == null) {
			throw new IllegalArgumentException("cycleCountMapProduct");
		}
		if (cycleCountMapProduct.getProduct() != null && cycleCountMapProduct.getCycleCount() != null) {
			if (checkIfRecordExist(cycleCountMapProduct.getCycleCount().getId(), cycleCountMapProduct.getProduct().getProductCode(), null)) {
				return null;
			}
		}
		else 
			return null;
		if (Boolean.TRUE.equals(isUpdateQuantityBfrCount)) {
			//update quantity_before_count
//			StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwner(
//					cycleCountMapProduct.getProduct().getId(), 
//					StockObjectType.SHOP, cycleCountMapProduct.getCycleCount().getShop().getId());
//			cycleCountMapProduct.setQuantityBeforeCount(stockTotal.getQuantity());
		}
		cycleCountMapProduct.setCreateUser(logInfo.getStaffCode());
		Date sysdate = commonDAO.getSysDate();
		cycleCountMapProduct.setCreateDate(sysdate);
		repo.create(cycleCountMapProduct);
		return repo.getEntityById(CycleCountMapProduct.class, cycleCountMapProduct.getId());
	}
	

	@Override
	public void deleteCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct) throws DataAccessException {
		if (cycleCountMapProduct == null) {
			throw new IllegalArgumentException("cycleCountMapProduct");
		}
		repo.delete(cycleCountMapProduct);
	}

	@Override
	public CycleCountMapProduct getCycleCountMapProductById(long id) throws DataAccessException {
		return repo.getEntityById(CycleCountMapProduct.class, id);
	}
	
	@Override
	public CycleCountMapProduct getCycleCountMapProductByIdForUpdate(long id) throws DataAccessException {
		String sql = "select * from cycle_count_map_product where cycle_count_map_product_id = ? for update";
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		return repo.getEntityBySQL(CycleCountMapProduct.class, sql, params);
	}
	
	@Override
	public void updateCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct, Boolean isUpdateQuantityBfrCount, Boolean isNewProduct) throws DataAccessException {
		if (cycleCountMapProduct == null) {
			throw new IllegalArgumentException("cycleCountMapProduct");
		}
		if (cycleCountMapProduct.getProduct() != null && cycleCountMapProduct.getCycleCount() != null) {
			if (checkIfRecordExist(cycleCountMapProduct.getCycleCount().getId(), cycleCountMapProduct.getProduct().getProductCode(), cycleCountMapProduct.getId())) {
				return;
			}
		}
		else 
			return;
		if (Boolean.TRUE.equals(isUpdateQuantityBfrCount)) {
			//update quantity_before_count
//			StockTotal stockTotal = stockTotalDAO.getStockTotalByProductAndOwner(
//					cycleCountMapProduct.getProduct().getId(), 
//					StockObjectType.SHOP, cycleCountMapProduct.getCycleCount().getShop().getId());
//			cycleCountMapProduct.setQuantityBeforeCount(stockTotal.getQuantity());
		}
		if(Boolean.FALSE.equals(isNewProduct)) {
			cycleCountMapProduct.setUpdateDate(commonDAO.getSysDate());
		}
		repo.update(cycleCountMapProduct);
	}
	
	@Override
	public void updateListCycleCountMapProduct(List<CycleCountMapProduct> lstCycleCountMapProduct, LogInfoVO log)
		throws DataAccessException {
		
		if (lstCycleCountMapProduct == null) {
			throw new IllegalArgumentException("cycleCountMapProduct");
		}
		List<CycleCountMapProduct> lstRs = new ArrayList<CycleCountMapProduct>();
		for (CycleCountMapProduct cycleCountMapProduct: lstCycleCountMapProduct) {
			if (cycleCountMapProduct.getProduct() != null && cycleCountMapProduct.getCycleCount() != null) {
				if (checkIfRecordExist(cycleCountMapProduct.getCycleCount().getId(), cycleCountMapProduct.getProduct().getProductCode(), cycleCountMapProduct.getId())) {
					continue;
				}
			}
			else 
				continue;
			cycleCountMapProduct.setUpdateDate(commonDAO.getSysDate());
			cycleCountMapProduct.setUpdateUser(log.getStaffCode());
			lstRs.add(cycleCountMapProduct);
		}
		repo.update(lstRs);
	}
	
	@Override
	public List<CycleCountMapProduct> getListCycleCountMapProductByCycleCountId(
			KPaging<CycleCountMapProduct> kPaging, long cycleCountId) throws DataAccessException {
		String sql = "select c.* from cycle_count_map_product c join product p on p.product_id = c.product_id where c.cycle_count_id=? order by c.stock_card_number, p.product_code asc";
		List<Object> params = new ArrayList<Object>();
		params.add(cycleCountId);
		if (kPaging != null)
			return repo.getListBySQLPaginated(CycleCountMapProduct.class, sql, params, kPaging);
		else
			return repo.getListBySQL(CycleCountMapProduct.class, sql, params);
	}
	
	@Override
	public CycleCountMapProduct getCycleCountMapProduct(Long cycleCountId, Long productId) throws DataAccessException {
		if (cycleCountId == null || productId == null) {
			throw new IllegalArgumentException("params cannot null");
		}
		String sql = "select * from cycle_count_map_product where cycle_count_id = ? and product_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(cycleCountId);
		params.add(productId);
		return repo.getEntityBySQL(CycleCountMapProduct.class, sql, params);
	}
	
	@Override
	public List<CycleCountMapProduct> getListCycleCountMapProduct(KPaging<CycleCountMapProduct> kPaging,
			Long cycleCountId, String productCode, String productName, Long catId, Long subCatId, Integer minQuantity, Integer maxQuantity, Boolean isOnlyGetDifferentQuantity) 
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT ccmp.* ");
		sql.append(" FROM CYCLE_COUNT cc ");
		sql.append(" JOIN CYCLE_COUNT_MAP_PRODUCT ccmp on cc.CYCLE_COUNT_ID = ccmp.CYCLE_COUNT_ID ");
//		sql.append(" JOIN STOCK_TOTAL tt on cc.WAREHOUSE_ID = tt.WAREHOUSE_ID AND ccmp.PRODUCT_ID = tt.PRODUCT_ID AND cc.SHOP_ID = tt.SHOP_ID ");
		sql.append(" JOIN PRODUCT p on p.product_id = ccmp.product_id ");
		sql.append(" WHERE 1 = 1");
		if(isOnlyGetDifferentQuantity) {
			sql.append(" AND ccmp.QUANTITY_BEFORE_COUNT <> NVL(ccmp.QUANTITY_COUNTED,0) ");
		}
		if (cycleCountId != null) {
			sql.append(" and cc.cycle_count_id = ?");
			params.add(cycleCountId);
		}
		if (!StringUtility.isNullOrEmpty(productCode)) {
			String temp = productCode.toUpperCase();
			sql.append(" and exists (select 1 from product p where c.product_id=p.product_id and p.product_code=? and p.status=? )");
			params.add(temp);
			params.add(ActiveType.RUNNING.getValue());
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" and exists (select 1 from product p where c.product_id=p.product_id and p.product_name like ? ESCAPE '/'  and p.status=?)");
			params.add(StringUtility.toOracleSearchLike(productName).toLowerCase());
			params.add(ActiveType.RUNNING.getValue());
		}
		if (catId != null) {
			sql.append(" and p.category_id=?");
			params.add(catId);
		}
		if (subCatId != null) {
			sql.append(" and p.sub_category_id=?");
			params.add(subCatId);
		}
		if (minQuantity != null) {
			sql.append(" and ccmp.QUANTITY_BEFORE_COUNT >= ?");
			params.add(minQuantity);
		}
		if (maxQuantity != null) {
			sql.append(" and ccmp.QUANTITY_BEFORE_COUNT <= ?");
			params.add(maxQuantity);
		}		
		sql.append(" order by p.order_index, p.product_code asc");
		if (kPaging != null) {
			return repo.getListBySQLPaginated(CycleCountMapProduct.class, sql.toString(), params, kPaging);
		}
		return repo.getListBySQL(CycleCountMapProduct.class, sql.toString(), params);
	}
	
	@Override
	public List<MapProductVO> searchCycleCountMapProductByFilter (MapProductFilter<MapProductVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlRes = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT  ");
		sql.append(" p.product_id as productId ");
		sql.append(" , rs.CYCLE_COUNT_MAP_PRODUCT_ID as id ");
		sql.append(" , rs.STOCK_CARD_NUMBER as stockCardNumber ");
		sql.append(" , NVL(rs.QUANTITY_BEFORE_COUNT, 0) as quantityBeforeCount ");
		sql.append(" , NVL(rs.QUANTITY_COUNTED, 0) as quantityCounted ");
		sql.append(" , p.PRODUCT_CODE as productCode ");
		sql.append(" , p.PRODUCT_NAME as productName ");
		sql.append(" , p.UOM1 ");
		sql.append(" , NVL(p.CHECK_LOT, 0) as checkLot ");
		sql.append(" , NVL(p.CONVFACT, 0) as convfact ");
		sql.append(" , cc.STATUS as statusCycleCount ");
		sql.append(" , rs.COUNT_DATE as countDate ");
		sql.append(" , to_char(rs.COUNT_DATE, 'dd/MM/yyyy') as countDateStr ");
		sql.append(" , 0 as flag  ");
		sql.append(" FROM CYCLE_COUNT cc ");
		sql.append(" JOIN CYCLE_COUNT_MAP_PRODUCT rs on cc.CYCLE_COUNT_ID = rs.CYCLE_COUNT_ID ");
		sql.append(" JOIN PRODUCT p on p.product_id = rs.product_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" and p.STATUS in (0, 1) ");
		if (filter.getCycleCountId() != null) {
			sql.append(" and cc.cycle_count_id = ? ");
			params.add(filter.getCycleCountId());
		}
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		sql.append(" order by p.order_index, p.product_code asc");
		sqlRes.append(" 	select rs.*, ROWNUM as stt from (  ");
		sqlRes.append(sql.toString());
		sqlRes.append(" ) rs ");
		final String[] fieldNames = new String[] { 
				"productId",
				"id",//1
				"stockCardNumber",//2
				"quantityBeforeCount",//3
				"quantityCounted",//4
				"productCode",//5
				"productName",//6
				"uom1",//7
				"checkLot",//8
				"convfact",//9
				"statusCycleCount", //10
				"countDate", //11
				"flag", //12
				"stt", //13
				"countDateStr"
				};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG, //1
				StandardBasicTypes.INTEGER, //2
				StandardBasicTypes.INTEGER, //3
				StandardBasicTypes.INTEGER, //4
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.STRING, //7
				StandardBasicTypes.INTEGER, //8
				StandardBasicTypes.INTEGER, //9
				StandardBasicTypes.INTEGER, //10
				StandardBasicTypes.DATE, //11
				StandardBasicTypes.INTEGER, //12
				StandardBasicTypes.INTEGER, //13
				StandardBasicTypes.STRING
		};
		if (filter.getPagination() == null || !filter.getPagination()) {
			return repo.getListByQueryAndScalar(MapProductVO.class, fieldNames, fieldTypes, sqlRes.toString(), params);
		}
		return repo.getListByQueryAndScalarPaginated(MapProductVO.class, fieldNames, fieldTypes, sqlRes.toString(), countSql.toString(), params, params, filter.getkPaging());
	}
	
	@Override
	public List<MapProductVO> getCycleCountMapProductByProductInsertTemp (MapProductFilter<MapProductVO> filter, List<Long> lstProductId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" 	with r_map as (  ");
		sql.append(" 	    SELECT ");
		sql.append("        p.product_id as productId, ");
		sql.append(" 		rs.CYCLE_COUNT_MAP_PRODUCT_ID as id, ");
		sql.append(" 	      rs.STOCK_CARD_NUMBER               AS stockCardNumber ,  ");
		sql.append(" 	      NVL(rs.QUANTITY_BEFORE_COUNT, 0)           AS quantityBeforeCount ,  ");
		sql.append(" 	      NVL(rs.QUANTITY_COUNTED, 0)                AS quantityCounted ,  ");
		sql.append(" 	      p.PRODUCT_CODE                     AS productCode ,  ");
		sql.append(" 	      p.PRODUCT_NAME                     AS productName ,  ");
		sql.append(" 	      p.UOM1 ,  ");
		sql.append("          NVL(p.CHECK_LOT, 0) AS checkLot ,  ");
		sql.append(" 	      NVL(p.CONVFACT, 0) as convfact ,  ");
		sql.append(" 	      cc.STATUS     AS statusCycleCount ,  ");
		sql.append(" 	      rs.COUNT_DATE AS countDate  ");
		sql.append(" , to_char(rs.COUNT_DATE, 'dd/MM/yyyy') as countDateStr ");
		sql.append(" 	      , 0 as flag  ");
		sql.append(" 	    FROM CYCLE_COUNT cc  ");
		sql.append("     JOIN CYCLE_COUNT_MAP_PRODUCT rs  ");
		sql.append("     ON cc.CYCLE_COUNT_ID = rs.CYCLE_COUNT_ID  ");
		sql.append(" 	    JOIN PRODUCT p  ");
		sql.append(" 	    ON p.product_id       = rs.product_id  ");
		sql.append("     WHERE 1               = 1  ");
		sql.append("     AND p.STATUS         IN (0, 1)  ");
		if (filter.getCycleCountId() != null) {
			sql.append(" and cc.cycle_count_id = ? ");
			params.add(filter.getCycleCountId());
		}
		sql.append("     order by p.order_index, p.product_code asc  ");
		sql.append(" ),  ");
		sql.append(" r_minus_join as (  ");
		sql.append("       select   ");
		sql.append("       rs.productId ");
		sql.append(" 		,0 as id ");
		sql.append("        ,0            AS stockCardNumber  ");
		sql.append("        ,0            AS quantityBeforeCount  ");
		sql.append("        ,0            AS quantityCounted  ");
		sql.append("        ,rs.productCode  ");
		sql.append("        ,rs.productName  ");
		sql.append("        ,rs.UOM1  ");
		sql.append("        ,rs.checkLot  ");
		sql.append("      ,rs.CONVFACT  ");
		sql.append("      ,0 AS statusCycleCount  ");
		sql.append("      ,null countDate  ");
		sql.append("      , null as countDateStr  ");
		sql.append("      ,1 as flag  ");
		sql.append("     from (  ");
		sql.append("        SELECT pd.PRODUCT_ID AS productId,  ");
		sql.append("         pd.PRODUCT_CODE    AS productCode,  ");
		sql.append("         pd.PRODUCT_NAME    AS productName,  ");
		sql.append("         pd.uom1,  ");
		sql.append("        pd.CHECK_LOT AS checkLot,  ");
		sql.append("        pd.CONVFACT  ");
		sql.append("        FROM product pd  ");
		sql.append("        WHERE 1        = 1  ");
		sql.append("         AND pd.STATUS  = 1  ");
		sql.append("         AND pd.STATUS IN (0, 1)  ");
		if (lstProductId != null && !lstProductId.isEmpty()) {
			sql.append("   AND pd.PRODUCT_ID in (? ");
			params.add(lstProductId.get(0));
			for (int i = 1, size = lstProductId.size(); i < size; i++) {
				sql.append("  ,?  ");
				params.add(lstProductId.get(i));
			}
			sql.append("   )  ");
		}
		sql.append("         MINUS  ");
		sql.append("         SELECT p.PRODUCT_ID AS productId,  ");
		sql.append("           p.PRODUCT_CODE    AS productCode,  ");
		sql.append("           p.PRODUCT_NAME    AS productName,  ");
		sql.append("          p.uom1,  ");
		sql.append("           p.CHECK_LOT AS checkLot,  ");
		sql.append("           p.CONVFACT  ");
		sql.append("         FROM CYCLE_COUNT cc  ");
		sql.append("         JOIN CYCLE_COUNT_MAP_PRODUCT ccmp  ");
		sql.append("         ON cc.CYCLE_COUNT_ID = ccmp.CYCLE_COUNT_ID  ");
		sql.append("         JOIN PRODUCT p  ");
		sql.append("         ON p.product_id       = ccmp.product_id  ");
		sql.append(" 	        WHERE 1               = 1  ");
		if (filter.getCycleCountId() != null) {
			sql.append(" and cc.cycle_count_id = ? ");
			params.add(filter.getCycleCountId());
		}
		sql.append(" 	      ) rs  ");
		sql.append(" 	)  ");
		sql.append(" 	select rs.*, ROWNUM as stt from (  ");
		sql.append(" 	    select * from r_map  ");
		sql.append(" 	    union all  ");
		sql.append(" 	    select * from r_minus_join  ");
		sql.append(" 	) rs  ");
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		final String[] fieldNames = new String[] { 
				"productId",
				"id",//1
				"stockCardNumber",//2
				"quantityBeforeCount",//3
				"quantityCounted",//4
				"productCode",//5
				"productName",//6
				"uom1",//7
				"checkLot",//8
				"convfact",//9
				"statusCycleCount", //10
				"countDate", //11
				"flag", //12
				"stt", //13
				"countDateStr"
				};
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG, //1
				StandardBasicTypes.INTEGER, //2
				StandardBasicTypes.INTEGER, //3
				StandardBasicTypes.INTEGER, //4
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.STRING, //7
				StandardBasicTypes.INTEGER, //8
				StandardBasicTypes.INTEGER, //9
				StandardBasicTypes.INTEGER, //10
				StandardBasicTypes.DATE, //11
				StandardBasicTypes.INTEGER, //12
				StandardBasicTypes.INTEGER, //13
				StandardBasicTypes.STRING
		};
		if (filter.getPagination() == null || !filter.getPagination()) {
			return repo.getListByQueryAndScalar(MapProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		return repo.getListByQueryAndScalarPaginated(MapProductVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}
	
	@Override
	public Boolean checkIfRecordExist(long cycleCountId, String productCode, Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count ");
		sql.append(" from cycle_count_map_product d ");
		sql.append(" where d.cycle_count_id= ? ");
		params.add(cycleCountId);
		if(!StringUtility.isNullOrEmpty(productCode)){
			sql.append(" and exists (select 1 from product p where p.product_id=d.product_id and product_code=?)");
			params.add(productCode.toUpperCase());
		}		
		if (id != null) {
			sql.append(" and d.cycle_count_map_product_id!=?");
			params.add(id);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}
	
	@Transactional
	@Override
	public Boolean createListCycleCountMapProduct(CycleCount cycleCount, List<CycleCountMapProduct> lstCycleCountMapProduct, Boolean isCreatingAll, LogInfoVO log) throws DataAccessException {
		Integer firstNumber = cycleCount.getFirstNumber();
		if (firstNumber == null)
			throw new DataAccessException();
		if (!isCreatingAll) {
			List<CycleCountMapProduct> lstCCMapProduct = new ArrayList<CycleCountMapProduct>();
			for (CycleCountMapProduct d : lstCycleCountMapProduct) {
				if (d.getProduct() == null)
					throw new DataAccessException("Product cannot be null");
				if (!checkIfRecordExist(cycleCount.getId(), d.getProduct().getProductCode(), null)) {
					d.setCreateDate(commonDAO.getSysDate());
					d.setCreateUser(log.getStaffCode());
					lstCCMapProduct.add(d);
				} else {
					d.setUpdateDate(commonDAO.getSysDate());
					d.setUpdateUser(log.getStaffCode());
					repo.update(d);
				}
			}
			lstCycleCountMapProduct = lstCCMapProduct;
		} else {
			String sql = "delete from cycle_count_map_product where cycle_count_id=?";
			List<Object> deletedParams = new ArrayList<Object>();
			deletedParams.add(cycleCount.getId());
			repo.executeSQLQuery(sql, deletedParams);

			if (cycleCount.getShop() == null)
				throw new DataAccessException("Shop cannot be null");

			StringBuilder allShopProductSql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();

			allShopProductSql.append(" select * from product p where p.status=? and exists ");
			params.add(ActiveType.RUNNING.getValue());
			allShopProductSql.append("		(select 1 from stock_total s join price pr on s.product_id = pr.product_id and pr.status = ? and pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?)) ");
			params.add(ActiveType.RUNNING.getValue());
			params.add(cycleCount.getStartDate());
			params.add(cycleCount.getStartDate());
			allShopProductSql.append("	where p.product_id = s.product_id and s.OBJECT_ID = ? and s.object_type = ? ");
			params.add(cycleCount.getShop().getId());
			params.add(StockObjectType.SHOP.getValue());
			allShopProductSql.append("  and s.quantity > 0 ");
			if (cycleCount.getWarehouse() != null) {
				allShopProductSql.append(" and s.warehouse_id = ? ");
				params.add(cycleCount.getWarehouse().getId());
			}
			allShopProductSql.append(" ) ");
			allShopProductSql.append(" order by product_code asc ");//@tientv11 order by product_code

			List<Product> lstProduct = repo.getListBySQL(Product.class, allShopProductSql.toString(), params);

			lstCycleCountMapProduct = new ArrayList<CycleCountMapProduct>();

			for (Product p : lstProduct) {
				CycleCountMapProduct c = new CycleCountMapProduct();
				c.setCycleCount(cycleCount);
				c.setProduct(p);
//				StockTotal stocktotal = stockTotalDAO.getStockTotalByProductCodeAndOwner(p.getProductCode(), StockObjectType.SHOP, cycleCount.getShop().getId());
				StockTotal stocktotal = stockTotalDAO.getStockTotalByProductAndOwner(p.getId(), StockObjectType.SHOP, cycleCount.getShop().getId(), cycleCount.getWarehouse().getId());
				if (stocktotal != null) {
					c.setQuantityBeforeCount(stocktotal.getQuantity());
				}
				Integer stockCardNumber = this.getMaxStockCardNumber(cycleCount.getId());
				if (stockCardNumber == 0) {
					stockCardNumber = firstNumber++;
				} else {
					stockCardNumber++;
				}
				c.setStockCardNumber(stockCardNumber);
				c.setCreateDate(commonDAO.getSysDate());
				c.setCreateUser(log.getStaffCode());
				lstCycleCountMapProduct.add(c);

			}
		}
		repo.create(lstCycleCountMapProduct);
		return true;
	}
	
	@Override
	public Integer getMaxStockCardNumber(Long cycleCountId) throws DataAccessException {
		String sql = "select max(stock_card_number) from cycle_count_map_product where cycle_count_id=?";
		List<Object> params = new ArrayList<Object>();
		params.add(cycleCountId);
		Object obj = repo.getObjectByQuery(sql, params);
		if (obj == null) {
			return 0;
		}
		return Integer.valueOf(obj.toString());
	}

	@Override
	public Integer getTotalStockCardNumber(Long cycleCountId) throws DataAccessException {
		String sql = "select count(1) as count from cycle_count_map_product where cycle_count_id=?";
		List<Object> params = new ArrayList<Object>();
		params.add(cycleCountId);
		return repo.countBySQL(sql, params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.CycleCountMapProductDAO#getListReportCountStore(java.lang.String, java.lang.String)
	 */
	@Override
	public List<RptCountStoreVO> getListReportCountStore(
			String cycleCountCode, String shopCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select p.product_code as productCode,");
		sql.append(" p.product_name as productName,");
		sql.append(" p.convfact  as convfact,");
		sql.append(" s.shop_name as shopName,");
		sql.append(" s.address as address,");
		sql.append(" ccmp.create_date as createDate,");
		sql.append(" ccr.lot as lot,");
		sql.append(" ccr.quantity_counted as quantityCounted,");
		sql.append(" ccmp.stock_card_number as stockCardNumber");
		sql.append(" from product p, shop s, cycle_count cc, cycle_count_map_product ccmp, cycle_count_result ccr");
		sql.append(" where cc.cycle_count_id = ccmp.cycle_count_id");
		sql.append(" AND cc.status = ?");
		params.add(CycleCountType.COMPLETED.getValue());
		if (!StringUtility.isNullOrEmpty(cycleCountCode)) {
			sql.append(" and cc.cycle_count_code = ?");
			params.add(cycleCountCode);
		}
		sql.append(" and cc.shop_id = s.shop_id");
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and s.shop_code = ?");
			params.add(shopCode);
		}
		sql.append(" and ccmp.product_id = p.product_id");
		sql.append(" and ccr.cycle_count_id = cc.cycle_count_id and ccr.cycle_count_map_product_id = ccmp.cycle_count_map_product_id");
		sql.append(" order by p.product_code, ccmp.stock_card_number");
		String[] fieldNames = new String[] {//
				"productCode",//1
				"productName",//2
				"convfact",//new
				"shopName",//3
				"address",//4
				"createDate",//5
				"lot",//6
				"quantityCounted",//7
				"stockCardNumber"//8
		};
		Type[] fieldTypes = new Type[] {//
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.INTEGER,// new
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.TIMESTAMP,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.INTEGER// 8
		};
		return repo.getListByQueryAndScalar(RptCountStoreVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public Boolean checkIfAnyProductCounted(Long cycleCountId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from cycle_count_map_product cp where cycle_count_id = ? and count_date is not null");
		params.add(cycleCountId);
		Integer c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	/**
	 * @author trietptm
	 */
	@Override
	public List<CycleCountDiffVO> getListCycleCountDiff(long cycleCountId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" WITH R AS ( ");
		sql.append("   SELECT p.PRODUCT_CODE productCode, ");
		sql.append("         ccmp.QUANTITY_BEFORE_COUNT quantityBeforeCount, ");
		sql.append("         NVL(ccmp.QUANTITY_COUNTED, 0) quantityCounted, ");
		sql.append("         NVL(psd.PRICE, 0) price, ");
		sql.append("         (ccmp.QUANTITY_BEFORE_COUNT - NVL(ccmp.QUANTITY_COUNTED, 0)) quantityDiff, ");
		sql.append("         (ccmp.QUANTITY_BEFORE_COUNT - NVL(ccmp.QUANTITY_COUNTED, 0)) * NVL(psd.PRICE, 0) amountDiff, ");
		sql.append(" 		 p.convfact convfact");
		sql.append("   FROM CYCLE_COUNT cc  ");
		sql.append("   JOIN CYCLE_COUNT_MAP_PRODUCT ccmp on cc.CYCLE_COUNT_ID = ccmp.CYCLE_COUNT_ID ");
		sql.append("   JOIN PRODUCT p on ccmp.PRODUCT_ID = p.PRODUCT_ID ");
		sql.append("   LEFT join PRICE_SHOP_DEDUCED psd on ccmp.PRODUCT_ID = psd.PRODUCT_ID and cc.SHOP_ID = psd.SHOP_ID ");
		sql.append("                                   AND psd.FROM_DATE < cc.START_DATE + 1 ");
		sql.append("                                   AND (psd.TO_DATE >= cc.START_DATE OR psd.TO_DATE IS NULL) ");
		sql.append("   WHERE cc.cycle_count_id = ? ");
		params.add(cycleCountId);
		sql.append(" ) ");
		sql.append(" SELECT (row_number() over (order by productCode)) AS rowNo,");
		sql.append("   productCode, ");
		sql.append("   SUM(quantityBeforeCount)         AS quantityBeforeCount, ");
		sql.append("   SUM(quantityCounted)             AS quantityCounted, ");
		sql.append("   NVL(price, 0)                    AS price, ");
		sql.append("   SUM(quantityDiff) AS quantityDiff, ");
		sql.append("   SUM(amountDiff)   AS amountDiff, ");
		sql.append("   convfact ");
		sql.append(" FROM R ");
		sql.append(" GROUP BY rollup ((productCode, quantityBeforeCount, quantityCounted, price, quantityDiff, amountDiff, convfact)) ");
		sql.append(" ORDER BY productCode ");
		
		String fieldNames[] = {
				"rowNo",
				"productCode",
				"quantityCounted",
				"quantityBeforeCount",
				"price",
				"quantityDiff",
				"amountDiff",
				"convfact"
		};
		
		Type fieldTypes[] = {
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER
		};
		
		List<CycleCountDiffVO> lst = repo.getListByQueryAndScalar(CycleCountDiffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
	
	@Override
	public List<ProductVO> searchProductNotInCycleProductMap(KPaging<ProductVO> kPaging, Integer status, Long cycleCountId, List<Long> lstProductId, List<Long> lstProductIdExcep, String productCode, String productName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select pd.PRODUCT_ID as productId, pd.PRODUCT_CODE as productCode, pd.PRODUCT_NAME as productName, pd.uom1, pd.CHECK_LOT as checkLot ");
		sql.append(" from product pd ");
		sql.append(" where 1 = 1  ");
		if (cycleCountId != null) {
			sql.append(" and pd.STATUS = ? ");
			params.add(status);
		} else {
			sql.append(" and pd.STATUS in (0, 1) ");
		}
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and pd.product_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode));
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" and lower(pd.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(productName).toLowerCase()));
		}
		if (lstProductId != null && !lstProductId.isEmpty()) {
			sql.append("   AND pd.PRODUCT_ID in (? ");
			params.add(lstProductId.get(0));
			for (int i = 1, size = lstProductId.size(); i < size; i++) {
				sql.append("  ,?  ");
				params.add(lstProductId.get(i));
			}
			sql.append("  )  ");
		}
		if (lstProductIdExcep != null && !lstProductIdExcep.isEmpty()) {
			sql.append("   AND pd.PRODUCT_ID not in (? ");
			params.add(lstProductIdExcep.get(0));
			for (int i = 1, size = lstProductIdExcep.size(); i < size; i++) {
				sql.append("  ,?  ");
				params.add(lstProductIdExcep.get(i));
			}
			sql.append("  )  ");
		}
		sql.append(" MINUS ");
		sql.append(" SELECT p.PRODUCT_ID as productId, p.PRODUCT_CODE as productCode, p.PRODUCT_NAME as productName, p.uom1, p.CHECK_LOT as checkLot ");
		sql.append(" FROM CYCLE_COUNT cc ");
		sql.append(" JOIN CYCLE_COUNT_MAP_PRODUCT ccmp ");
		sql.append(" ON cc.CYCLE_COUNT_ID = ccmp.CYCLE_COUNT_ID ");
		sql.append(" JOIN PRODUCT p ");
		sql.append(" ON p.product_id       = ccmp.product_id ");
		sql.append(" WHERE 1               = 1 ");
		if (cycleCountId != null) {
			sql.append(" and cc.cycle_count_id = ? ");
			params.add(cycleCountId);
		}
		countSql.append("SELECT count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");

		String[] fieldNames = new String[] { 
				"productId",// 1
				"productCode",// 2
				"productName", //3
				"uom1", //4
				"checkLot" //5
		};
		Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.INTEGER // 5
		};
		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(ProductVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
		return repo.getListByQueryAndScalar(ProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.ProductLotVO;
import ths.dms.core.exceptions.DataAccessException;

public interface ProductLotDAO {

	ProductLot createProductLot(ProductLot productLot)
			throws DataAccessException;

	void deleteProductLot(ProductLot productLot) throws DataAccessException;

	void updateProductLot(ProductLot productLot) throws DataAccessException;

	ProductLot getProductLotById(long id) throws DataAccessException;

	Boolean isUsingByOthers(Long productLotId) throws DataAccessException;

	List<ProductLot> getProductLotByProductAndOwner(long productId, long ownerId,
			StockObjectType ownerType) throws DataAccessException;

	ProductLot getProductLotByCodeAndOwnerAndProduct(String lot, long ownerId,
			StockObjectType ownerType, long productId)
			throws DataAccessException;

	List<ProductLot> getListProductLot(KPaging<ProductLot> kPaging,
			Long ownerId, StockObjectType ownerType, Integer quantity,
			ActiveType status, Long productId, Boolean checkApparam) throws DataAccessException;

	List<ProductLotVO> getListProductLotVO(KPaging<ProductLotVO> kPaging,
			Long ownerId, StockObjectType ownerType, Integer quantity,
			ActiveType status) throws DataAccessException;

	boolean checkQuantityInProductLot(long shopId, long productId, int quantity, String lot) throws DataAccessException;
	
	ProductLot getProductLogForUpdatePo(long poVnmId) throws DataAccessException;


	List<ProductLot> getProductLotByProductAndOwnerForUpdate(long productId,
			long ownerId, StockObjectType ownerType, Integer fifo) throws DataAccessException;

	ProductLot getProductLotByCodeAndOwnerAndProductForUpdate(String code,
			long ownerId, StockObjectType ownerType, long productId)
			throws DataAccessException;

	void createListProductLot(List<ProductLot> listProductLot)
			throws DataAccessException;

	void updateListProductLot(List<ProductLot> listProductLot)
			throws DataAccessException;
	
	ProductLot getProductLotByProductIdAndLot(Long productId, String lot) throws DataAccessException;
	
	void updateQuantityAndAvaiableQuantityForProductLot(Long productLotId, Integer quantity) throws DataAccessException;

	List<ProductLotVO> getListProductInStock(KPaging<ProductLotVO> kPaging,Long shopId,
			Long ownerId, StockObjectType ownerType, Integer quantity,
			ActiveType status) throws DataAccessException;
	
	List<ProductLotVO> getListProductForVanSale(KPaging<ProductLotVO> kPaging,
			Long ownerId, StockObjectType ownerType) throws DataAccessException;

	List<ProductLot> getProductLotByProductAndOwnerForUpdate(long productId,
			long ownerId, StockObjectType ownerType, String orderBy)
			throws DataAccessException;
	
	List<ProductLotVO> getListProductVanSale(KPaging<ProductLotVO> kPaging, String productCode, String productName,
			Long ownerId, StockObjectType ownerType, Integer quantity,
			ActiveType status) throws DataAccessException;
	
	/**
	 * 
	*  lay lot de set tra hang tu dong
	*  @author: thanhnn
	*  @param objectId
	*  @param objectType
	*  @param fromQuantity
	*  @return
	*  @throws DataAccessException
	*  @return: List<ProductLotVO>
	*  @throws:
	 */
	List<ProductLotVO> getListProductLotForInstock(Long shopId,Long objectId,
			StockObjectType objectType, Integer fromQuantity, Integer productId)
			throws DataAccessException;

	List<ProductLot> getListProductLotForUpdate(KPaging<ProductLot> kPaging,
			Long ownerId, StockObjectType ownerType, Integer quantity,
			ActiveType status, Long productId, Boolean checkApparam)
			throws DataAccessException;

	List<ProductLot> getProductLotByProductAndOwnerEx(long productId, long ownerId, StockObjectType ownerType) throws DataAccessException;

}

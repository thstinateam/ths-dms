package ths.dms.core.dao;

/**
 * Import thu vien ho tro
 * */
import java.util.List;

import ths.dms.core.entities.EquipLend;
import ths.dms.core.entities.EquipLendDetail;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.vo.EquipBorrowVO;
import ths.dms.core.entities.vo.EquipLendDetailVO;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Danh cho tat ca cac lien quan den Bien ban thiet bi DAO
 * 
 * @author hunglm16
 * @since December 14,2014
 * */
public interface EquipProposalBorrowDAO {
	/**
	 * Lay danh sach Equip Lend
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * 
	 * @author update nhutnn
	 * @since 03/07/2015
	 * */
	List<EquipBorrowVO> searchListProposalBorrowVOByFilter(EquipProposalBorrowFilter<EquipBorrowVO> filter)throws DataAccessException;
	
	/**
	 * Lay equip lend bang id
	 * @author nhutnn
	 * @since 03/07/2015
	 * @param code
	 * @return
	 * @throws DataAccessException
	 */
	EquipLend getEquipLendByCode(String code) throws DataAccessException;
		
	/**
	 * Cap nhat equip lend
	 * @author nhutnn
	 * @since 03/07/2015
	 * @param equipLend
	 * @throws DataAccessException
	 */
	void updateEquipLend(EquipLend equipLend) throws DataAccessException;
	
	/**
	 * Tao moi equip lend
	 * @author nhutnn
	 * @since 03/07/2015
	 * @param equipLend
	 * @return
	 * @throws DataAccessException
	 */
	EquipLend createEquipLend(EquipLend equipLend) throws DataAccessException;
	
	/**
	 * Cap nhat equip lend chi tiet
	 * @author nhutnn
	 * @since 03/07/2015
	 * @param equipLendDetail
	 * @return
	 * @throws DataAccessException
	 */
	EquipLendDetail createEquipLendDetail(EquipLendDetail equipLendDetail) throws DataAccessException;
	
	/**
	 * Lay danh sach chi tiet bien ban VO
	 * 
	 * @author nhutnn
	 * @since 08/07/2015
	 * */
	List<EquipLendDetailVO> getListEquipLendDetailVOByFilter(EquipProposalBorrowFilter<EquipBorrowVO> filter)throws DataAccessException;
	
	/**
	 * Lay ma bien ban
	 * @author nhutnn
	 * @since 06/07/2015
	 * @return
	 * @throws DataAccessException
	 */
	String getCodeEquipLend() throws DataAccessException;
	
	/**
	 * Cap nhat equip lend chi tiet
	 * @author nhutnn
	 * @since 08/07/2015
	 * @param equipLendDetail
	 * @throws DataAccessException
	 */
	void updateEquipLendDetail(EquipLendDetail equipLendDetail) throws DataAccessException;
	
	/**
	 * Lay danh sach chi tiet bien ban Entity
	 * 
	 * @author nhutnn
	 * @since 08/07/2015
	 * */
	List<EquipLendDetail> getListEquipLendDetailByFilter(EquipProposalBorrowFilter<EquipBorrowVO> filter)throws DataAccessException;

	/**
	 * Lay danh sach bien ban
	 * 
	 * @author nhutnn
	 * @since 09/07/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipLend> getListEquipLendByFilter(EquipProposalBorrowFilter<EquipBorrowVO> filter) throws DataAccessException;
}

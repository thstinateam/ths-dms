package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StaffGroup;
import ths.dms.core.entities.StaffGroupDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class StaffGroupDAOImpl implements StaffGroupDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public StaffGroup createStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws DataAccessException {
		if (staffGroup == null) {
			throw new IllegalArgumentException("staffGroup is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		staffGroup.setCreateUser(logInfo.getStaffCode());
		repo.create(staffGroup);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, staffGroup, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(StaffGroup.class, staffGroup.getId());
	}

	@Override
	public void deleteStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws DataAccessException {
		if (staffGroup == null) {
			throw new IllegalArgumentException("staffGroup");
		}
		repo.delete(staffGroup);
		try {
			actionGeneralLogDAO.createActionGeneralLog(staffGroup, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public StaffGroup getStaffGroupById(long id) throws DataAccessException {
		return repo.getEntityById(StaffGroup.class, id);
	}
	
	@Override
	public void updateStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws DataAccessException {
		if (staffGroup == null) {
			throw new IllegalArgumentException("staffGroup");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		
		StaffGroup temp = this.getStaffGroupById(staffGroup.getId());
		staffGroup.setUpdateDate(commonDAO.getSysDate());
		staffGroup.setUpdateUser(logInfo.getStaffCode());
		temp = StaffGroup.clone(temp);
		repo.update(staffGroup);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, staffGroup, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public List<StaffGroup> getListStaffGroup(KPaging<StaffGroup> kPaging, Long shopId, ActiveType activeType)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from staff_group where 1 = 1 ");
		
		if (shopId != null) {
			sql.append(" and shop_id = ?");
			params.add(shopId);
		}
		
		if (activeType != null) {
			sql.append(" and status = ?");
			params.add(activeType.getValue());
		}
		else {
			sql.append(" and status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		sql.append(" order by STAFF_GROUP_NAME");
		if (kPaging == null)
			return repo.getListBySQL(StaffGroup.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StaffGroup.class, sql.toString(), params, kPaging);
	}

	@Override
	public StaffGroupDetail checkIfStaffExistsInStaffGroup(Long staffId,
			Long staffGroupId, ActiveType activeType)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select * from staff_group_detail where staff_group_id = ? and staff_id = ? and status != -1");
		params.add(staffGroupId);
		params.add(staffId);
		
		return repo.getFirstBySQL(StaffGroupDetail.class, sql.toString(), params);
	}
	
	
	@Override
	public Boolean checkStaffExistsInShopWithStaffGroup(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) count from staff_group_detail d join staff_group s on d.staff_group_id = s.staff_group_id where s.shop_id = ? and d.status = 1 and s.status = 1");
		params.add(shopId);
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
}
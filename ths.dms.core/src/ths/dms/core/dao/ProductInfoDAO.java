package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductInfoMapType;
import ths.dms.core.entities.enumtype.ProductInfoObjectType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.CatalogFilter;
import ths.dms.core.entities.vo.CatalogVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProductInfoVO;
import ths.dms.core.entities.vo.ProductInfoVOEx;
import ths.dms.core.exceptions.DataAccessException;

public interface ProductInfoDAO {

	ProductInfo createProductInfo(ProductInfo productInfo, LogInfoVO logInfo) throws DataAccessException;

	void deleteProductInfo(ProductInfo productInfo, LogInfoVO logInfo) throws DataAccessException;

	void updateProductInfo(ProductInfo productInfo, LogInfoVO logInfo) throws DataAccessException;
	
	ProductInfo getProductInfoById(long id) throws DataAccessException;

	ProductInfo getProductInfoByCode(String code, ProductType type,
			String catCode, Boolean isActiveTypeRunning)
			throws DataAccessException;
	
//	/**
//	 * Lay doi tuong nganh hang con
//	 * @author thongnm
//	 */
//	ProductInfo getProductInfoWithSubCat(Long catId, String code,
//			ProductType productType, ProductInfoMapType productInfoMapType,
//			Boolean isActiveTypeRunning) throws DataAccessException;
	
	
	List<ProductInfo> getListSubCat(ActiveType status,ProductType type, Long from_object)throws DataAccessException;

	List<ProductInfo> getListSubCatByListCat(ActiveType status, ProductType type, List<Long> lstObjectId) throws DataAccessException;
	
	List<ProductInfo> getListProductInfo(KPaging<ProductInfo> kPaging,
			String productInfoCode, String productInfoName, String desc,
			ActiveType status, ProductType type, Boolean isExceptZCat,String sort,String name) throws DataAccessException;
	
	List<ProductInfo> getListProductInfoFromProduct(KPaging<ProductInfo> kPaging, String desc,
			ActiveType status, ProductType type) throws DataAccessException;

	Boolean isUsingByOthers(long productInfoId) throws DataAccessException;

	List<ProductInfo> getListSubCatNotInDisplayGroup(
			KPaging<ProductInfo> kPaging, Long displayGroupId,
			String subCatCode, String subCatName) throws DataAccessException;

	/**
	 * Lay thong tin cat san pham, order by Name
	 * @param type
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<ProductInfo> getListProductInfoOrderByName(ProductType type)
			throws DataAccessException;

	List<ProductInfo> getListProductInfoForStock(KPaging<ProductInfo> kPaging,
			String productInfoCode, String productInfoName, String desc,
			ActiveType status, ProductType type) throws DataAccessException;
	
	List<ProductInfoVO> getListProductInfoVO () throws DataAccessException;

	Boolean isEquimentCat(String catCode) throws DataAccessException;

	
	/*****************************	Nganh hang con phu****************************************/
	/**
	 * Tim kiem danh sach nganh hang con phu
	 * @author thongnm
	 */
	List<ProductInfoVOEx> getListProductInfoOfSecondSubCat( KPaging<ProductInfoVOEx> kPaging, String catCode,
			String subCatCode, String subCatName) throws DataAccessException;

	List<ProductInfo> getListSubCat(ActiveType status, ProductType type, ProductInfoMapType mapType,
			Long from_object) throws DataAccessException;

	/**
	 * Lay danh sach Nganh hang theo Filter Basic
	 * 
	 * @author hunglm16
	 * @since September 23,2014
	 * */
	List<ProductInfo> getListProductInfoByFilter(BasicFilter<ProductInfo> filter) throws DataAccessException;
	
	/**
	 * Lay danh sach Nganh hang theo danh sach kiem ke
	 * 
	 * @author hoanv25
	 * @since November 20,2014
	 * */
	List<ProductInfo> getListProductInfoStock(KPaging<ProductInfo> kPaging,
			String productInfoCode, String productInfoName, String desc,
			ActiveType status, ProductType type) throws DataAccessException;
	
	/**
	 * @author vuongmq, copy habeco
	 * @date 15/01/2015
	 * @description danh sach categories
	 */
	List<ProductInfo> getListProductInfoEx(KPaging<ProductInfo> kPaging,
			String productInfoCode, String productInfoName, ActiveType status,
			ProductType type, List<ProductInfoObjectType> objTypes, boolean orderByCode)
					throws DataAccessException;

	/**
	 * Lay danh sach type cua san pham
	 * 
	 * @param producttype
	 * @author hoanv25
	 * @since August, 04,2015
	 * */
	List<ProductInfo> getListProductInfoTypeStock(KPaging<ProductInfo> kPaging, ActiveType producttype) throws DataAccessException;

	/**
	 * Lay danh sach hinh thuc ban hang
	 * 
	 * @param producttype
	 * @author hoanv25
	 * @since August, 14,2015
	 * */
	List<ApParam> getListShopProductInfoType(Object object, ActiveType producttype) throws DataAccessException;
	
	/**
	 * Lay danh sach danh muc san pham
	 * @author trietptm
	 * @param filter
	 * @return danh sach danh muc san pham
	 * @throws DataAccessException
	 * @since Nov 17, 2015
	 */
	List<CatalogVO> getListCatalogByFilter(CatalogFilter filter) throws DataAccessException;
	
	/**
	 * Check danh sach nganh hang
	 * 
	 * @param code
	 * @param type
	 * @param catCode
	 * @param isActiveTypeRunning
	 * @author hoanv25
	 * @since August, 14,2015
	 * */
	ProductInfo getProductInfoByCodeCat(String code, ProductType type, Boolean isActiveTypeRunning) throws DataAccessException;

	/**
	 * lay danh sach nganh hang con su dung nganh hang
	 * @author trietptm
	 * @param catId
	 * @param status
	 * @return danh sach nganh hang con su dung nganh hang
	 * @throws DataAccessException
	 * @since Nov 18, 2015
	 */
	List<ProductInfo> getListSubCatByCatId(Long catId, ActiveType status) throws DataAccessException;
	
	/**
	 * @author vuongmq
	 * @param code
	 * @param type
	 * @param isActiveTypeRunning
	 * @return ProductInfo
	 * @throws DataAccessException
	 * @since 23/10/2015
	 */
	ProductInfo getProductInfoByWithNotSubCat(String code, ProductType type, Boolean isActiveTypeRunning) throws DataAccessException;

}
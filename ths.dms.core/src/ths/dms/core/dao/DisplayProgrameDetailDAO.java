package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.DisplayDetailVO;
import ths.dms.core.exceptions.DataAccessException;

public interface DisplayProgrameDetailDAO {

//	DisplayProgrameDetail createDisplayProgrameDetail(DisplayProgrameDetail displayProgrameDetail) throws DataAccessException;
//
//	void deleteDisplayProgrameDetail(DisplayProgrameDetail displayProgrameDetail) throws DataAccessException;
//
//	void updateDisplayProgrameDetail(DisplayProgrameDetail displayProgrameDetail) throws DataAccessException;
//	
//	DisplayProgrameDetail getDisplayProgrameDetailById(long id) throws DataAccessException;
//	
//	DisplayProgrameDetail getDisplayProgrameDetail(DisplayProgrameDetail dpPro) throws DataAccessException;

	List<DisplayDetailVO> getListCTTBbyId(KPaging<DisplayDetailVO> kPaging,Long shopId, Long idCTTB, Date date) throws DataAccessException;
}

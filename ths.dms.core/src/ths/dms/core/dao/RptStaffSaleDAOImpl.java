package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.RptStaffSale;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.RptStaffSaleFilter;
import ths.dms.core.entities.vo.AmountAndAmountPlanVO;
import ths.dms.core.entities.vo.AmountPlanCustomerVO;
import ths.dms.core.entities.vo.CustomerSaleVO;
import ths.dms.core.entities.vo.NVBHSaleVO;
import ths.dms.core.entities.vo.SupervisorSaleVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.ConstantManager;
import ths.dms.core.dao.repo.IRepository;

public class RptStaffSaleDAOImpl implements RptStaffSaleDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private SaleDayDAO saleDayDAO;
	
	@Autowired
	private ExceptionDayDAO exceptionDayDAO;

	@Override
	public RptStaffSale createRptStaffSale(RptStaffSale rptStaffSale) throws DataAccessException {
		if (rptStaffSale == null) {
			throw new IllegalArgumentException("rptStaffSale");
		}
		repo.create(rptStaffSale);
		return repo.getEntityById(RptStaffSale.class, rptStaffSale.getId());
	}

	@Override
	public void deleteRptStaffSale(RptStaffSale rptStaffSale) throws DataAccessException {
		if (rptStaffSale == null) {
			throw new IllegalArgumentException("rptStaffSale");
		}
		repo.delete(rptStaffSale);
	}

	@Override
	public RptStaffSale getRptStaffSaleById(long id) throws DataAccessException {
		return repo.getEntityById(RptStaffSale.class, id);
	}
	
	@Override
	public void updateRptStaffSale(RptStaffSale rptStaffSale) throws DataAccessException {
		if (rptStaffSale == null) {
			throw new IllegalArgumentException("rptStaffSale");
		}
		repo.update(rptStaffSale);
	}
	
	
	@Override
	public BigDecimal getDayAmountPlanByTBHV(Long tbhvId) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT SUM(NVL(RPT.DAY_AMOUNT_PLAN, 0)) AS COUNT");
		sql.append("	FROM RPT_STAFF_SALE RPT");
		sql.append("	WHERE rpt.PARENT_STAFF_ID IN");
		sql.append("  (SELECT NVGS.staff_id");
		sql.append("  FROM STAFF NVGS");
		sql.append("  INNER JOIN SHOP S");
		sql.append("  ON NVGS.SHOP_ID        = S.SHOP_ID");
		sql.append("  WHERE S.PARENT_SHOP_ID =");
		sql.append("    (SELECT SHOP_ID FROM STAFF TBHV WHERE TBHV.STAFF_ID = ?");
		params.add(tbhvId);
		sql.append("    )");
		sql.append("  )");
		sql.append("AND TO_CHAR(rpt.create_date, 'MMYYYY') = TO_CHAR(sysdate, 'MMYYYY')");
		return repo.countBySQLReturnBigDecimal(sql.toString(), params);
	}

	@Override
	public BigDecimal getDayAmountByTBHV(Long tbhvId) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT SUM(NVL(RPT.DAY_AMOUNT, 0)) AS COUNT");
		sql.append("	FROM RPT_STAFF_SALE RPT");
		sql.append("	WHERE rpt.PARENT_STAFF_ID IN");
		sql.append("  (SELECT NVGS.staff_id");
		sql.append("  FROM STAFF NVGS");
		sql.append("  INNER JOIN SHOP S");
		sql.append("  ON NVGS.SHOP_ID        = S.SHOP_ID");
		sql.append("  WHERE S.PARENT_SHOP_ID =");
		sql.append("    (SELECT SHOP_ID FROM STAFF TBHV WHERE TBHV.STAFF_ID = ?");
		params.add(tbhvId);
		sql.append("    )");
		sql.append("  )");
		sql.append("AND TO_CHAR(rpt.create_date, 'MMYYYY') = TO_CHAR(sysdate, 'MMYYYY')");
		return repo.countBySQLReturnBigDecimal(sql.toString(), params);
	}
	


	@Override
	public BigDecimal getDayAmountPlanByNVGS(Long nvgsId) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT sum(nvl(rpt.day_amount_plan, 0)) AS count ");
		sql.append("	FROM rpt_staff_sale rpt");
		sql.append("	WHERE rpt.parent_staff_id = ?");
		params.add(nvgsId);
		sql.append("  	and to_char(rpt.create_date, 'MMYYYY') = to_char(sysdate, 'MMYYYY')");
		return repo.countBySQLReturnBigDecimal(sql.toString(), params);
	}

	@Override
	public BigDecimal getDayAmountByNVGS(Long nvgsId) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT sum(nvl(rpt.day_amount, 0)) AS count");
		sql.append("	FROM rpt_staff_sale rpt");
		sql.append("	WHERE rpt.parent_staff_id = ?");
		params.add(nvgsId);
		sql.append("	AND to_char(rpt.create_date, 'MMYYYY') = to_char(sysdate, 'MMYYYY')");
		return repo.countBySQLReturnBigDecimal(sql.toString(), params);
	}
	
	@Override
	public SupervisorSaleVO getDayAmountAndDayAmountPlanByNVGS(Long nvgs) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder("");
		sql.append("SELECT SUM(NVL(RPT.DAY_AMOUNT_PLAN, 0)) AS dayAmountPlan , ");
		sql.append(" SUM(NVL(RPT.DAY_AMOUNT, 0)) AS dayAmount");
		sql.append(" FROM RPT_STAFF_SALE RPT");
		sql.append(" WHERE RPT.PARENT_STAFF_ID              = 543 ");
		sql.append(" AND TO_CHAR(RPT.CREATE_DATE, 'MMYYYY') = TO_CHAR(SYSDATE, 'MMYYYY')");
		final String[] fieldNames = new String[] {
				"dayAmountPlan", //1
				"dayAmount" };//2

		final Type[] fieldTypes = new Type[] {
		StandardBasicTypes.BIG_DECIMAL, //1
		StandardBasicTypes.BIG_DECIMAL };//2
		List<SupervisorSaleVO> list = repo.getListByQueryAndScalar(SupervisorSaleVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if(list != null && list.size() != 0) {
			return list.get(0);
		} else {
			return null;
		}
	}
	
	@Override
	public BigDecimal getDayAmountPlanByNVBH(Long nvbhId) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT sum(nvl(rpt.day_amount_plan, 0)) AS count ");
		sql.append("	FROM rpt_staff_sale rpt");
		sql.append("	WHERE rpt.staff_id = ?");
		params.add(nvbhId);
		sql.append("  	and to_char(rpt.create_date, 'MMYYYY') = to_char(sysdate, 'MMYYYY')");
		return repo.countBySQLReturnBigDecimal(sql.toString(), params);
	}
	
	@Override
	public BigDecimal getDayAmountByNVBH(Long nvbhId) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT sum(nvl(rpt.day_amount, 0)) AS count");
		sql.append("	FROM rpt_staff_sale rpt");
		sql.append("	WHERE rpt.staff_id = ?");
		params.add(nvbhId);
		sql.append("	AND to_char(rpt.create_date, 'MMYYYY') = to_char(sysdate, 'MMYYYY')");
		return repo.countBySQLReturnBigDecimal(sql.toString(), params);
	}

	@Override
	public BigDecimal getMonthAmountPlanByStaffType(StaffObjectType roleType,
			Staff staff) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		if(roleType.equals(StaffObjectType.TBHV)){
			sql.append("select round(sum(MONTH_AMOUNT_PLAN)/1000) as count ");
			sql.append("from RPT_STAFF_SALE rss ");
			sql.append("where rss.parent_staff_id in (select staff_id ");
			sql.append("from staff  ");
			sql.append("where 1=1  ");			
			sql.append("and shop_id in (select shop_id from shop where parent_shop_id = ? )) ");
			params.add(staff.getShop().getId());
			sql.append("and TO_CHAR(rss.create_date, 'MMYYYY') = TO_CHAR(sysdate,'MMYYYY') ");
		}else if(roleType.equals(StaffObjectType.NVGS)){
			sql.append("select round(sum(MONTH_AMOUNT_PLAN)/1000) as count ");
			sql.append("from RPT_STAFF_SALE rss ");
			sql.append("where rss.parent_staff_id = ? ");
			params.add(staff.getId());
			sql.append("and TO_CHAR(rss.create_date, 'MMYYYY') = TO_CHAR(sysdate,'MMYYYY') ");
		}else if(roleType.equals(StaffObjectType.NVBH)){
			sql.append("select round(sum(MONTH_AMOUNT_PLAN)/1000) as count ");
			sql.append("from RPT_STAFF_SALE rss ");
			sql.append("where rss.staff_id = ? ");
			params.add(staff.getId());
			sql.append("and TO_CHAR(rss.create_date, 'MMYYYY') = TO_CHAR(sysdate,'MMYYYY') ");			
		}
		return repo.countBySQLReturnBigDecimal(sql.toString(), params);	
	}

	@Override
	public BigDecimal getMonthAmountByStaffType(StaffObjectType roleType,
			Staff staff) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		if(roleType.equals(StaffObjectType.TBHV)){
			sql.append("select round(sum(MONTH_AMOUNT)/1000) as count ");
			sql.append("from RPT_STAFF_SALE rss ");
			sql.append("where rss.parent_staff_id in (select staff_id ");
			sql.append("from staff  ");
			sql.append("where 1=1  ");			
			sql.append("and shop_id in (select shop_id from shop where parent_shop_id = ? )) ");
			params.add(staff.getShop().getId());
			sql.append("and TO_CHAR(rss.create_date, 'MMYYYY') = TO_CHAR(sysdate,'MMYYYY') ");
		}else if(roleType.equals(StaffObjectType.NVGS)){
			sql.append("select round(sum(MONTH_AMOUNT)/1000) as count ");
			sql.append("from RPT_STAFF_SALE rss ");
			sql.append("where rss.parent_staff_id = ? ");
			params.add(staff.getId());
			sql.append("and TO_CHAR(rss.create_date, 'MMYYYY') = TO_CHAR(sysdate,'MMYYYY') ");
		}else if(roleType.equals(StaffObjectType.NVBH)){
			sql.append("select round(sum(MONTH_AMOUNT)/1000) as count ");
			sql.append("from RPT_STAFF_SALE rss ");
			sql.append("where rss.staff_id = ? ");
			params.add(staff.getId());
			sql.append("and TO_CHAR(rss.create_date, 'MMYYYY') = TO_CHAR(sysdate,'MMYYYY') ");	
		}
		return repo.countBySQLReturnBigDecimal(sql.toString(), params);		
	}
	
	@Override
	public AmountAndAmountPlanVO getAmountAndAmountPlanByStaff(Staff staff, String type) throws DataAccessException {
		if (staff == null) {
			throw new IllegalArgumentException("staff is null");
		}
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		/*sql.append(" with lstNVBH as ( ");
		sql.append(" 	    select s.* from staff s ");
		sql.append(" 	    join channel_type ct on ct.channel_type_id=s.staff_type_id and ct.object_type in (?,?) ");
		params.add(StaffObjectType.NVBH.getValue());
		params.add(StaffObjectType.NVVS.getValue());
		if (staff.getStaffType() != null
				&& (StaffObjectType.NVBH.getValue().equals(staff.getStaffType().getObjectType())
						|| StaffObjectType.NVVS.getValue().equals(staff.getStaffType().getObjectType()))) {
			sql.append(" where s.staff_id = ?");
			params.add(staff.getId());
			sql.append(")");
		} else {
			sql.append(" 	    where s.staff_id  in ( ");
			sql.append(" 	      select psm.staff_id as staffId from parent_staff_map psm ");
			sql.append(" 	      where psm.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" 	      and psm.staff_id not in (select staff_id from exception_user_access where status = ?  and staff_id = parent_staff_id) ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" 	      start with psm.parent_staff_id = ? AND PSM.STATUS = ?  connect by prior psm.staff_id = psm.parent_staff_id ");
			params.add(staff.getId());
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" )) ");
		}
		sql.append(" SELECT NVL(kq1.monthAmount, 0) AS monthAmount, NVL(kq1.monthAmountApproved, 0) AS monthAmountApproved, NVL(kq1.monthAmountPlan, 0) AS monthAmountPlan, NVL(kq2.dayAmount, 0) AS dayAmount, NVL(kq2.dayAmountApproved, 0)        AS dayAmountApproved, NVL(kq2.dayAmountPlan, 0) AS dayAmountPlan ");
		sql.append(" FROM   ");
		sql.append(" ( SELECT ROUND (SUM(MONTH_AMOUNT) / 1000) AS monthAmount, ROUND (SUM(MONTH_APPROVED_AMOUNT)  / 1000)  AS monthAmountApproved, ROUND ( SUM (MONTH_AMOUNT_PLAN) / 1000 ) AS monthAmountPlan  ");
		sql.append(" 		FROM RPT_STAFF_SALE rss WHERE rss.STAFF_ID IN (select staff_id from lstNVBH)  ");
//		sql.append(" 		AND TO_CHAR (rss.create_date, 'MMYYYY') = TO_CHAR (SYSDATE, 'MMYYYY')");
		sql.append(" 		AND rss.create_date   >= TRUNC(SYSDATE,'MM') ");
		sql.append(" 		AND rss.create_date      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		sql.append(" ) kq1, ");			
		sql.append(" ( SELECT ROUND (SUM(DAY_AMOUNT) / 1000) AS dayAmount, ROUND (SUM(DAY_APPROVED_AMOUNT) / 1000) AS dayAmountApproved, ROUND (SUM(DAY_AMOUNT_PLAN) / 1000) AS dayAmountPlan ");
		sql.append(" 		FROM RPT_STAFF_SALE rss WHERE rss.STAFF_ID IN (select staff_id from lstNVBH) ");
//		sql.append(" 		AND TO_CHAR (rss.create_date, 'MMYYYY') = TO_CHAR (SYSDATE, 'MMYYYY')");
		sql.append(" 		AND rss.create_date   >= TRUNC(SYSDATE,'MM') ");
		sql.append(" 		AND rss.create_date      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		sql.append(" ) kq2 ");
		
		String[] fieldNames = { 
				"monthAmount",
				"monthAmountApproved",
				"monthAmountPlan", 
				"dayAmount",
				"dayAmountApproved",
				"dayAmountPlan"
		};
		Type[] fieldTypes = { StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.BIG_DECIMAL
		};*/
		
		//trungtm6 modified on 09/09/2015
		sql.append(" WITH temp AS ");
		sql.append(" (SELECT distinct st.staff_id ");
		sql.append(" ,vp.routing_id ");
		sql.append(" FROM staff st ");
		sql.append(" LEFT JOIN visit_plan vp ON st.staff_id = vp.staff_id ");
							sql.append(" AND vp.from_date <= TRUNC(sysdate) ");
							sql.append(" AND ( vp.to_date >= TRUNC (sysdate) ");
							sql.append(" OR vp.to_date IS NULL) and vp.status in (0,1) ");
		sql.append(" WHERE st.staff_id = ? ");
		params.add(staff.getId());
		sql.append(" ) ");
		
		/*sql.append(" WITH lstNVBH AS ");
		sql.append(" (SELECT s.staff_id ");
		sql.append(" FROM staff s ");
		sql.append(" JOIN staff_type st ");
		sql.append(" ON st.staff_type_id = s.staff_type_id ");
		if (staff.getStaffType() != null && staff.getStaffType().getSpecificType() != null) {
			sql.append(" AND st.specific_type = ?  ");
			params.add(staff.getStaffType().getSpecificType().getValue());
		}
		sql.append(" WHERE s.staff_id = ? ");
		params.add(staff.getId());
		sql.append(" ) ");*/
		
		
		
		sql.append(" SELECT ROUND (SUM(NVL(rss.MONTH_AMOUNT, 0))/ 1000) AS monthAmount, ");
		sql.append(" ROUND (SUM(NVL(rss.MONTH_AMOUNT_APPROVED, 0))/ 1000) AS monthAmountApproved, ");
		sql.append(" ROUND (SUM(NVL(rss.MONTH_AMOUNT_PLAN, 0))/ 1000) AS monthAmountPlan, ");
		sql.append(" ROUND (SUM(NVL(rss.DAY_AMOUNT, 0))/ 1000) AS dayAmount, ");
		sql.append(" ROUND (SUM(NVL(rss.DAY_AMOUNT_APPROVED, 0))/ 1000) AS dayAmountApproved, ");
		sql.append(" ROUND (SUM(NVL(rss.DAY_AMOUNT_PLAN, 0))/ 1000) AS dayAmountPlan, ");
		sql.append(" SUM(NVL(rss.MONTH_QUANTITY, 0)) AS monthQuantity, ");
		sql.append(" SUM(NVL(rss.MONTH_QUANTITY_APPROVED, 0)) AS monthQuantityApproved, ");
		sql.append(" SUM(NVL(rss.MONTH_QUANTITY_PLAN, 0)) AS monthQuantityPlan, ");
		sql.append(" SUM(NVL(rss.DAY_QUANTITY, 0)) AS dayQuantity, ");
		sql.append(" SUM(NVL(rss.DAY_QUANTITY_APPROVED, 0)) AS dayQuantityApproved, ");
		sql.append(" SUM(NVL(rss.DAY_QUANTITY_PLAN, 0)) AS dayQuantityPlan ");
		sql.append(" from rpt_staff_sale rss ");
		sql.append(" where rss.sale_date >= trunc(sysdate) ");
		sql.append(" and rss.sale_date < trunc(sysdate) + 1 ");
		if (ConstantManager.TWO_TEXT.equals(type)) {//routing
			sql.append(" and rss.object_ID IN (SELECT routing_id FROM temp ) ");
			sql.append(" and rss.OBJECT_TYPE = ? ");
			params.add(ConstantManager.TWO);
		} else {//staff
			sql.append(" and rss.object_ID IN (SELECT staff_id FROM temp ) ");
			sql.append(" and rss.OBJECT_TYPE = ? ");
			params.add(ConstantManager.ONE);
		}
		
		String[] fieldNames = { 
				"monthAmount",
				"monthAmountApproved",
				"monthAmountPlan", 
				"dayAmount",
				"dayAmountApproved",
				"dayAmountPlan",
				"monthQuantity",
				"monthQuantityApproved",
				"monthQuantityPlan", 
				"dayQuantity",
				"dayQuantityApproved",
				"dayQuantityPlan"
		};
		Type[] fieldTypes = { StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER
		};
		List<AmountAndAmountPlanVO> lst = repo.getListByQueryAndScalar(AmountAndAmountPlanVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (lst != null && lst.size() > 0) {
			AmountAndAmountPlanVO vo = lst.get(0);
			//vo.setDsBHKH(saleDayDAO.getSalesMonthByYear());
			//vo.setSnBHDQ(exceptionDayDAO.getExceptionDayForSupervise());
			if (vo.getDsBHKH() == null || vo.getDsBHKH() == 0) {
				vo.setTienDoChuan(0);
			} else {
				vo.setTienDoChuan((Integer)(vo.getSnBHDQ() * 100 / vo.getDsBHKH()));
			}	
			return vo;
		}
		return null;
	}
	
	@Override
	public AmountAndAmountPlanVO getAmountAndAmountPlanByStaffSupervise(Staff staff, RptStaffSaleFilter filter) throws DataAccessException {
		if (staff == null) {
			throw new IllegalArgumentException("staff is null");
		}
		/** filter.getObjetType(): la theo loai cua staff quan ly, 1: NVBH */
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH lstNVBHofGS AS ");
		sql.append(" (SELECT mst.staff_id ");
		sql.append(" FROM map_user_staff mst ");
		sql.append(" WHERE mst.user_id = ? ");
		params.add(staff.getId());
		if (staff.getStaffType() != null && staff.getStaffType().getSpecificType() != null) {
			sql.append(" and mst.user_specific_type = ? ");
			params.add(staff.getStaffType().getSpecificType().getValue());
		}
		if (filter != null && filter.getObjetType() != null) {
			sql.append(" and mst.specific_type = ? ");
			params.add(filter.getObjetType());
		}
		sql.append(" and mst.from_date < trunc(sysdate) + 1 ");
		sql.append(" and (mst.to_date is null or mst.to_date >= trunc(sysdate)) ");
		sql.append(" ) ");
		
		//trungtm6 modified on 09/09/2015
		sql.append(" ,temp as ( ");
		sql.append(" select distinct st.staff_id, vp.routing_id ");
		sql.append(" from lstNVBHofGS st ");
		sql.append(" LEFT JOIN visit_plan vp on st.staff_id = vp.staff_id ");
						sql.append(" AND vp.from_date <= TRUNC(sysdate) ");
						sql.append(" AND ( vp.to_date >= TRUNC (sysdate) ");
						sql.append(" OR vp.to_date IS NULL) and vp.status in (0,1) ");
		sql.append(" ) ");
		
		sql.append(" SELECT ROUND (SUM(NVL(rss.MONTH_AMOUNT, 0))/ 1000) AS monthAmount, ");
		sql.append(" ROUND (SUM(NVL(rss.MONTH_AMOUNT_APPROVED, 0))/ 1000) AS monthAmountApproved, ");
		sql.append(" ROUND (SUM(NVL(rss.MONTH_AMOUNT_PLAN, 0))/ 1000) AS monthAmountPlan, ");
		sql.append(" ROUND (SUM(NVL(rss.DAY_AMOUNT, 0))/ 1000) AS dayAmount, ");
		sql.append(" ROUND (SUM(NVL(rss.DAY_AMOUNT_APPROVED, 0))/ 1000) AS dayAmountApproved, ");
		sql.append(" ROUND (SUM(NVL(rss.DAY_AMOUNT_PLAN, 0))/ 1000) AS dayAmountPlan, ");
		sql.append(" SUM(NVL(rss.MONTH_QUANTITY, 0)) AS monthQuantity, ");
		sql.append(" SUM(NVL(rss.MONTH_QUANTITY_APPROVED, 0)) AS monthQuantityApproved, ");
		sql.append(" SUM(NVL(rss.MONTH_QUANTITY_PLAN, 0)) AS monthQuantityPlan, ");
		sql.append(" SUM(NVL(rss.DAY_QUANTITY, 0)) AS dayQuantity, ");
		sql.append(" SUM(NVL(rss.DAY_QUANTITY_APPROVED, 0)) AS dayQuantityApproved, ");
		sql.append(" SUM(NVL(rss.DAY_QUANTITY_PLAN, 0)) AS dayQuantityPlan ");
		sql.append(" from rpt_staff_sale rss ");
		sql.append(" where rss.sale_date >= trunc(sysdate) ");
		sql.append(" and rss.sale_date < trunc(sysdate) + 1 ");
		if (ConstantManager.TWO_TEXT.equals(filter.getType())) {//routing
			sql.append(" and rss.object_ID IN (SELECT routing_id FROM temp ) ");//lstNVBHofGS
			sql.append(" and rss.OBJECT_TYPE = ? ");
			params.add(ConstantManager.TWO);
		} else {//staff
			sql.append(" and rss.object_ID IN (SELECT staff_id FROM temp ) ");//lstNVBHofGS
			sql.append(" and rss.OBJECT_TYPE = ? ");
			params.add(ConstantManager.ONE);
		}
		
		String[] fieldNames = { 
				"monthAmount",
				"monthAmountApproved",
				"monthAmountPlan", 
				"dayAmount",
				"dayAmountApproved",
				"dayAmountPlan",
				"monthQuantity",
				"monthQuantityApproved",
				"monthQuantityPlan", 
				"dayQuantity",
				"dayQuantityApproved",
				"dayQuantityPlan"
		};
		Type[] fieldTypes = { StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER
		};
		List<AmountAndAmountPlanVO> lst = repo.getListByQueryAndScalar(AmountAndAmountPlanVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (lst != null && lst.size() > 0) {
			AmountAndAmountPlanVO vo = lst.get(0);
			//vo.setDsBHKH(saleDayDAO.getSalesMonthByYear());
			//vo.setSnBHDQ(exceptionDayDAO.getExceptionDayForSupervise());
			if (vo.getDsBHKH() == null || vo.getDsBHKH() == 0) {
				vo.setTienDoChuan(0);
			} else {
				vo.setTienDoChuan((Integer) (vo.getSnBHDQ() * 100 / vo.getDsBHKH()));
			}	
			return vo;
		}
		return null;
	}
	
	@Override
	public List<AmountPlanCustomerVO> getAmountPlanCustomerByStaff(RptStaffSaleFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT c.SHORT_CODE as shortCode, ");
		sql.append(" c.CUSTOMER_CODE as customerCode, ");
		sql.append(" c.CUSTOMER_NAME as customerName, ");
		sql.append(" c.ADDRESS as address, ");
		sql.append(" c.PHONE as phone, ");
		sql.append(" c.MOBIPHONE as mobiphone, ");
		sql.append(" c.LAT as lat, ");
		sql.append(" c.LNG as lng, ");
		sql.append(" ROUND (SUM(NVL(rssd.DAY_AMOUNT_PLAN, 0))/ 1000) AS dayAmountPlan, ");
		sql.append(" ROUND (SUM(NVL(rssd.DAY_AMOUNT_APPROVED, 0))/ 1000) AS dayAmountApproved, ");
		sql.append(" ROUND (SUM(NVL(rssd.DAY_AMOUNT, 0))/ 1000) AS dayAmount, ");
		sql.append(" SUM(NVL(rssd.DAY_QUANTITY_PLAN, 0)) AS dayQuantityPlan, ");
		sql.append(" SUM(NVL(rssd.DAY_QUANTITY_APPROVED, 0)) AS dayQuantityApproved, ");
		sql.append(" SUM(NVL(rssd.DAY_QUANTITY, 0)) AS dayQuantity, ");
		sql.append(" ROUND (SUM(NVL(rssd.MONTH_AMOUNT_PLAN, 0))/ 1000) AS monthAmountPlan, ");
		sql.append(" ROUND (SUM(NVL(rssd.MONTH_AMOUNT_APPROVED, 0))/ 1000) AS monthAmountApproved, ");
		sql.append(" ROUND (SUM(NVL(rssd.MONTH_AMOUNT, 0))/ 1000) AS monthAmount, ");
		sql.append(" SUM(NVL(rssd.MONTH_QUANTITY_PLAN, 0)) AS monthQuantityPlan, ");
		sql.append(" SUM(NVL(rssd.MONTH_QUANTITY_APPROVED, 0)) AS monthQuantityApproved, ");
		sql.append(" SUM(NVL(rssd.MONTH_QUANTITY, 0)) AS monthQuantity ");
		/*sql.append(" FROM RPT_STAFF_SALE_DETAIL rssd ");
		sql.append(" join customer c on c.customer_id = rssd.customer_id ");*/
		sql.append(" FROM customer c ");
		sql.append(" left JOIN RPT_STAFF_SALE_DETAIL rssd ");
		sql.append(" ON c.customer_id = rssd.customer_id ");
		sql.append(" AND (1 = 1 ");
		if (filter.getStaffId() != null) {
			sql.append(" and rssd.object_id = ? ");
			params.add(filter.getStaffId());
		}
		if (filter.getObjetType() != null) {
			sql.append(" and rssd.object_type = ? ");
			params.add(filter.getObjetType());
		}
		if (filter.getCustomerId() != null) {
			sql.append(" and rssd.customer_id = ? ");
			params.add(filter.getCustomerId());
		}
		sql.append(" AND rssd.sale_date >= TRUNC(sysdate) ");
		sql.append(" AND rssd.sale_date < TRUNC(sysdate) + 1 ");
		sql.append(" ) ");
		sql.append(" where 1 = 1 ");
		if (filter.getCustomerId() != null) {
			sql.append(" and c.customer_id = ? ");
			params.add(filter.getCustomerId());
		}
		// group by cua select
		sql.append(" group by SHORT_CODE, ");
		sql.append(" CUSTOMER_CODE, ");
		sql.append(" CUSTOMER_NAME, ");
		sql.append(" ADDRESS, ");
		sql.append(" PHONE, ");
		sql.append(" MOBIPHONE, ");
		sql.append(" LAT, ");
		sql.append(" LNG ");
		String[] fieldNames = { 
				"shortCode",
				"customerCode", //2
				"customerName",
				"address",  //4
				"phone",
				"mobiphone", //6
				"lat", 
				"lng", //8
				"dayAmountPlan",	//9			
				"dayAmountApproved", //10
				"dayAmount", //11
				"dayQuantityPlan",
				"dayQuantityApproved",
				"dayQuantity", //14
				"monthAmountPlan",
				"monthAmountApproved",
				"monthAmount", //17
				"monthQuantityPlan",
				"monthQuantityApproved",
				"monthQuantity", //20
		};
		Type[] fieldTypes = { 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, // 8
				StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, //11
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, //14
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.BIG_DECIMAL, //17
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, //20
		};
		return repo.getListByQueryAndScalar(AmountPlanCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<CustomerSaleVO> getListCustomerSaleVO(KPaging<CustomerSaleVO> kPaging, Long nvbhId, String type) throws DataAccessException {
		if (nvbhId == null) {
			throw new IllegalArgumentException("nvbhId is null");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH temp AS ");
		sql.append(" (SELECT distinct st.staff_id ");
		sql.append(" ,vp.routing_id ");
		sql.append(" FROM staff st ");
		sql.append(" LEFT JOIN visit_plan vp ON st.staff_id = vp.staff_id ");
						sql.append(" AND vp.from_date <= TRUNC(sysdate) ");
						sql.append(" AND ( vp.to_date >= TRUNC (sysdate) ");
						sql.append(" OR vp.to_date IS NULL) and vp.status in (0,1) ");
		sql.append(" WHERE st.staff_id = ? ");
		params.add(nvbhId);
		sql.append(" ) ");
		
		sql.append(" SELECT ");
		sql.append(" c.CUSTOMER_ID as customerId, ");
		sql.append(" c.SHORT_CODE as shortCode, ");
		sql.append(" c.CUSTOMER_CODE as customerCode, ");
		sql.append(" c.CUSTOMER_NAME as customerName, ");
		sql.append(" c.ADDRESS as address, ");
		sql.append(" ROUND (SUM(NVL(rssd.DAY_AMOUNT_PLAN, 0))/ 1000) AS dayAmountPlan, ");
		sql.append(" ROUND (SUM(NVL(rssd.DAY_AMOUNT_APPROVED, 0))/ 1000) AS dayAmountApproved, ");
		sql.append(" ROUND (SUM(NVL(rssd.DAY_AMOUNT, 0))/ 1000) AS dayAmount, ");
		sql.append(" ROUND (SUM(NVL(rssd.MONTH_AMOUNT_PLAN, 0))/ 1000) AS monthAmountPlan, ");
		sql.append(" ROUND (SUM(NVL(rssd.MONTH_AMOUNT_APPROVED, 0))/ 1000) AS monthAmountApproved, ");
		sql.append(" ROUND (SUM(NVL(rssd.MONTH_AMOUNT, 0))/ 1000) AS monthAmount, ");
		sql.append(" SUM(NVL(rssd.DAY_QUANTITY_PLAN, 0)) AS dayQuantityPlan, ");
		sql.append(" SUM(NVL(rssd.DAY_QUANTITY_APPROVED, 0)) AS dayQuantityApproved, ");
		sql.append(" SUM(NVL(rssd.DAY_QUANTITY, 0)) AS dayQuantity, ");
		sql.append(" SUM(NVL(rssd.MONTH_QUANTITY_PLAN, 0)) AS monthQuantityPlan, ");
		sql.append(" SUM(NVL(rssd.MONTH_QUANTITY_APPROVED, 0)) AS monthQuantityApproved, ");
		sql.append(" SUM(NVL(rssd.MONTH_QUANTITY, 0)) AS monthQuantity ");
		sql.append(" FROM RPT_STAFF_SALE_DETAIL rssd ");
		sql.append(" join customer c on c.customer_id = rssd.customer_id ");
		sql.append(" where rssd.sale_date >= trunc(sysdate) ");
		sql.append(" and rssd.sale_date < trunc(sysdate) + 1 ");
		
		/*if(ngay = ){
			
		}*/
		//trungtm6 modified on 12/09/2015
		/*if (nvbhId != null) {
			sql.append(" and rssd.object_id = ? ");
			params.add(nvbhId);
		}*/
		if (ConstantManager.TWO_TEXT.equals(type)) {//routing
			sql.append(" and rssd.object_id IN (select routing_id from temp) ");
			sql.append(" and rssd.object_type = ? ");
			params.add(ConstantManager.TWO);
		} else {//staff
			sql.append(" and rssd.object_id IN (select staff_id from temp) ");
			sql.append(" and rssd.object_type = ? ");
			params.add(ConstantManager.ONE);
		}
		
		
		// group by cua select
		sql.append(" group by c.CUSTOMER_ID, ");
		sql.append(" c.SHORT_CODE, ");
		sql.append(" c.CUSTOMER_CODE, ");
		sql.append(" c.CUSTOMER_NAME, ");
		sql.append(" c.ADDRESS ");
		// lay danh sach phan trang
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql.toString());
		countSql.append(")");
		// order by
		sql.append(" order by shortCode, customerName ");
		final String[] fieldNames = new String[] { "customerId", //1
				"shortCode", // 1.1
				"customerCode", //2
				"customerName", //3
				"address",//4
				"dayAmountPlan", //5
				"dayAmountApproved", //6
				"dayAmount", //7
				"monthAmountPlan",  //8
				"monthAmountApproved", // 9
				"monthAmount",  //10
				"dayQuantityPlan",
				"dayQuantityApproved",
				"dayQuantity",
				"monthQuantityPlan",
				"monthQuantityApproved",
				"monthQuantity",
		};
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //1.1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING,//4
				StandardBasicTypes.BIG_DECIMAL,//5
				StandardBasicTypes.BIG_DECIMAL,//6
				StandardBasicTypes.BIG_DECIMAL, //7
				StandardBasicTypes.BIG_DECIMAL,//8
				StandardBasicTypes.BIG_DECIMAL,//9
				StandardBasicTypes.BIG_DECIMAL, //10
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, 
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, 
				StandardBasicTypes.INTEGER,
		};
		if (null == kPaging) {
			return repo.getListByQueryAndScalar(CustomerSaleVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		return repo.getListByQueryAndScalarPaginated(CustomerSaleVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}
	
	@Override
	public List<NVBHSaleVO> getListNVBHSaleVO(KPaging<NVBHSaleVO> kPaging, RptStaffSaleFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (filter == null) {
			throw new IllegalArgumentException(" RptStaffSaleFilter is null ");
		}
		sql.append(" WITH lstNVBHofGS AS ");
		sql.append(" ( ");
		sql.append(" SELECT mst.staff_id ");
		sql.append(" FROM map_user_staff mst ");
		sql.append(" where 1 = 1 ");
		if (filter.getStaffId() != null) {
			sql.append(" and mst.user_id = ? ");
			params.add(filter.getStaffId());
		}
		//sql.append(" and mst.user_specific_type = 2 "); // NVGS
		if (filter.getSpecificType() != null) {
			sql.append(" and mst.user_specific_type = ? ");
			params.add(filter.getSpecificType());
		}
		if (filter.getObjetType() != null) {
			sql.append(" and  mst.specific_type = ? ");
			params.add(filter.getObjetType());
		}
		if (filter.getShopId() != null) {
			sql.append(" and mst.shop_id = ? ");
			params.add(filter.getShopId());
		}
		sql.append(" and mst.from_date < trunc(sysdate) + 1 ");
		sql.append(" and (mst.to_date is null or mst.to_date >= trunc(sysdate)) ");
		sql.append(" ) ");
		
		
		//trungtm6 modified on 09/09/2015
		sql.append(" ,temp as ( ");
		sql.append(" select distinct st.staff_id, st.staff_code, st.staff_name,  r.routing_id, r.routing_code, r.routing_name, st.shop_id ");
		sql.append(" from lstNVBHofGS t ");
		sql.append(" join staff st on t.staff_id = st.staff_id ");
		sql.append(" left join visit_plan vp on st.staff_id = vp.staff_id ");
					sql.append(" AND vp.from_date <= trunc(sysdate) ");
					sql.append(" AND (vp.to_date >= trunc (sysdate) OR vp.to_date is null ) and vp.status in (0,1)");
		sql.append(" left join routing r on vp.routing_id = r.routing_id ");			
					
		sql.append(" ) ");
		
		
		sql.append(" select st.STAFF_ID AS staffId, ");
		sql.append(" st.STAFF_CODE AS staffCode, ");
		sql.append(" st.STAFF_NAME AS staffName, ");
		sql.append(" st.ROUTING_CODE routingCode, ");
		sql.append(" st.ROUTING_NAME routingName, ");
		
		sql.append(" st.shop_id AS shopID, ");
		sql.append(" ROUND (SUM(NVL(rss.DAY_AMOUNT_PLAN, 0))/ 1000) AS dayAmountPlan, ");
		sql.append(" ROUND (SUM(NVL(rss.DAY_AMOUNT_APPROVED, 0))/ 1000) AS dayAmountApproved, ");
		sql.append(" ROUND (SUM(NVL(rss.DAY_AMOUNT, 0))/ 1000) AS dayAmount, ");
		sql.append(" ROUND (SUM(NVL(rss.MONTH_AMOUNT_PLAN, 0))/ 1000) AS monthAmountPlan, ");
		sql.append(" ROUND (SUM(NVL(rss.MONTH_AMOUNT_APPROVED, 0))/ 1000) AS monthAmountApproved, ");
		sql.append(" ROUND (SUM(NVL(rss.MONTH_AMOUNT, 0))/ 1000) AS monthAmount, ");
		sql.append(" SUM(NVL(rss.DAY_QUANTITY_PLAN, 0)) AS dayQuantityPlan, ");
		sql.append(" SUM(NVL(rss.DAY_QUANTITY_APPROVED, 0)) AS dayQuantityApproved, ");
		sql.append(" SUM(NVL(rss.DAY_QUANTITY, 0)) AS dayQuantity, ");
		sql.append(" SUM(NVL(rss.MONTH_QUANTITY_PLAN, 0)) AS monthQuantityPlan, ");
		sql.append(" SUM(NVL(rss.MONTH_QUANTITY_APPROVED, 0)) AS monthQuantityApproved, ");
		sql.append(" SUM(NVL(rss.MONTH_QUANTITY, 0)) AS monthQuantity ");
		sql.append(" FROM temp st ");
		sql.append(" LEFT JOIN RPT_STAFF_SALE rss ON ");
		sql.append(" rss.SALE_DATE >= TRUNC(sysdate) ");
		sql.append(" AND rss.SALE_DATE < TRUNC(sysdate)+1 ");
		if (filter.getShopId() != null) {
			sql.append(" and rss.shop_id = ? ");
			params.add(filter.getShopId());
		}
		if (ConstantManager.TWO_TEXT.equals(filter.getType())) {//routing
			sql.append(" AND rss.object_id = st.routing_id ");
			sql.append(" and rss.OBJECT_TYPE = ? ");
			params.add(ConstantManager.TWO);
		} else {
			sql.append(" AND rss.object_id = st.staff_id ");
			sql.append(" and rss.OBJECT_TYPE = ? ");
			params.add(ConstantManager.ONE);
		}
		
//		sql.append(" join staff st on st.staff_id = rss.object_id ");
//		sql.append(" where rss.object_ID IN (select staff_id from lstNVBHofGS) ");
		
		//trungtm6 comment on 10/09/2015
		/*if (filter.getObjetType() != null) {
			sql.append(" and rss.OBJECT_TYPE = ? ");
			params.add(filter.getObjetType());
		}*/
		
		// group by cua select
		sql.append(" group by st.STAFF_ID, ");
		sql.append(" st.STAFF_CODE, ");
		sql.append(" st.STAFF_NAME, ");
		sql.append(" st.ROUTING_CODE, ");
		sql.append(" st.ROUTING_NAME, ");
		sql.append(" st.shop_id ");
		
		// lay danh sach phan trang
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql.toString());
		countSql.append(")");
		// order by
		sql.append(" order by staffCode, staffName ");
		final String[] fieldNames = new String[] { "staffId", //1
				"staffCode", // 2
				"staffName", //3
				"routingCode", // 2
				"routingName", //3
				"shopID", //4
				"dayAmountPlan", //5
				"dayAmountApproved", //6
				"dayAmount", //7
				"monthAmountPlan",  //8
				"monthAmountApproved", // 9
				"monthAmount",  //10
				"dayQuantityPlan",
				"dayQuantityApproved", //12
				"dayQuantity",
				"monthQuantityPlan", //14
				"monthQuantityApproved",
				"monthQuantity", //16
		};
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.LONG, //4
				StandardBasicTypes.BIG_DECIMAL,//5
				StandardBasicTypes.BIG_DECIMAL,//6
				StandardBasicTypes.BIG_DECIMAL, //7
				StandardBasicTypes.BIG_DECIMAL,//8
				StandardBasicTypes.BIG_DECIMAL,//9
				StandardBasicTypes.BIG_DECIMAL, //10
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, //12
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, //14
				StandardBasicTypes.INTEGER, 
				StandardBasicTypes.INTEGER, //16
		};
		if (null == kPaging) {
			return repo.getListByQueryAndScalar(NVBHSaleVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		return repo.getListByQueryAndScalarPaginated(NVBHSaleVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}
}

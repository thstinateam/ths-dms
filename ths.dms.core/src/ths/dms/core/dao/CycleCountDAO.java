package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.enumtype.CycleCountFilter;
import ths.dms.core.entities.enumtype.CycleCountSearchFilter;
import ths.dms.core.entities.enumtype.CycleCountType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.CycleCountSearchVO;
import ths.dms.core.entities.vo.CycleCountVO;
import ths.dms.core.exceptions.DataAccessException;

public interface CycleCountDAO {

	CycleCount createCycleCount(CycleCount cycleCount) throws DataAccessException;

	void deleteCycleCount(CycleCount cycleCount) throws DataAccessException;

	void updateCycleCount(CycleCount cycleCount) throws DataAccessException;
	
	CycleCount getCycleCountById(long id) throws DataAccessException;

	/*CycleCount getCycleCountByCode(String code) throws DataAccessException;*/

	Boolean checkOngoingCycleCount(long shopId, long warehouseId) throws DataAccessException;

	List<CycleCountVO> getListCycleCountVO(KPaging<CycleCountVO> kPaging,
			String cycleCountCode, CycleCountType cycleCountStatus,
			String desc, Long shopId, Date fromDate, Date toDate,Date fromCreate,Date toCreate, Long wareHouseId, String strListShopId)
			throws DataAccessException;

	List<CycleCount> getListCycleCount(KPaging<CycleCount> kPaging,
			String cycleCountCode, CycleCountType cycleCountStatus,
			String desc, Long shopId, Date fromDate, Date toDate,
			Boolean isCompleteOrOngoing, Long wareHouseId, String strListShopId) throws DataAccessException;
	
	List<CycleCount> getListCycleCount(CycleCountSearchFilter filter) throws DataAccessException;

	CycleCount getCycleCountByCodeAndShop(String code, Long shopId)throws DataAccessException;

	Boolean checkCompleteCheckStock(long cycleCounting) throws DataAccessException;

	List<CycleCountSearchVO> getListCycleCountSearchVO(CycleCountFilter filter)	throws DataAccessException;

}

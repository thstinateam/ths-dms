package ths.dms.core.dao;

/**
 * Import thu vien ho tro
 * */
import java.util.List;

import ths.dms.core.entities.EquipRole;
import ths.dms.core.entities.EquipRoleDetail;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.vo.EquipProposalBorrowVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Danh cho tat ca cac lien quan den Bien ban thiet bi DAO
 * 
 * @author hunglm16
 * @since December 14,2014
 * */
public interface EquipStockPermissionDAO {
	/**
	 * Lay danh sach Quan ly quyen kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */	
	List<EquipProposalBorrowVO> searchListStockPermissionVOByFilter(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter)throws DataAccessException;
	/**
	 * Lay danh sach kho thiet bi theo don vi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	List<EquipProposalBorrowVO> searchStockByFilter(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter)throws DataAccessException;
	/**
	 * Kiem tra xem ma quyen kho thiet bi da ton tai chua
	 *  
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	EquipRole getEquipRoleByCodeEx(String code, ActiveType status)throws DataAccessException;
	/**
	 * Tao moi quyen kho thiet bi
	 *  
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	EquipRole createStockPermission(EquipRole eq, LogInfoVO logInfoVO) throws DataAccessException;
	/**
	 * lay quyen kho thiet bi theo Id
	 *  
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	EquipRole getStockPermissionById(Long id) throws DataAccessException;
	/**
	 * Cap nhat quyen kho thiet bi
	 *  
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	EquipRole saveStockPermission(EquipRole eq, LogInfoVO logInfoVO) throws DataAccessException;
	/**
	 * Xoa quyen kho thiet bi
	 *  
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	void deleteStockPermission(EquipRole eq) throws DataAccessException;
	/**
	 *Lay danh sach kho thiet bi theo id ma quyen
	 *  
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	List<EquipProposalBorrowVO> getListtEquipStockByEquipRoleId(KPaging<EquipProposalBorrowVO> kPaging, Long id)throws DataAccessException;
	/**
	 * Tim kiem Danh sach kho
	 * 
	 * @return the string
	 * @author hoanv25
	 * @since March 09,2015
	 */
	List<EquipProposalBorrowVO> searchListStockPermissionVOAdd(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter)throws DataAccessException;
	/**
	 * lay danh sach kho thiet bi theo Id
	 *  
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	EquipStock getEquipStockById(Long id)throws DataAccessException;
	/**
	 * Tao moi danh sach thiet bi detail theo Id
	 *  
	 * @author hoanv25
	 * @return 
	 * @since March 09,2015
	 * */
	EquipRoleDetail createStockPermission(EquipRoleDetail eqr)throws DataAccessException;
	/**
	 * lay danh sach ma quyen detail theo Id
	 *  
	 * @author hoanv25
	 * @return 
	 * @since March 09,2015
	 * */
	List<EquipRoleDetail> getListEquipRoleDetailByEquipRoleId(Long id)throws DataAccessException;
	/**
	 * lay EquipRoleDetail theo Id
	 *  
	 * @author hoanv25
	 * @return 
	 * @since March 09,2015
	 * */
	EquipRoleDetail getEquipRoleDetailById(Long id)throws DataAccessException;
	/**
	 * Xoa EquipRoleDetail theo Id
	 *  
	 * @author hoanv25
	 * @return 
	 * @since March 09,2015
	 * */
	void deleteEquipRoleDetail(EquipRoleDetail eqr)throws DataAccessException;
	/**
	 * Lay ma quyen detail kho thiet bi theo Id
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	EquipRoleDetail getListEquipRoleDetailById(Long id)throws DataAccessException;
	/**
	 * Lay danh sach theo id kho va id role
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	List<EquipProposalBorrowVO> getListEquipRoleDetailByStockIdAndRoleId(Long idcheck, Long id)throws DataAccessException;
	/**
	 * update detail kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	EquipRoleDetail updateEquipRoleDetail(EquipRoleDetail eqrd, LogInfoVO logInfoVO)throws DataAccessException;
	/**
	 * create detail kho thiet bi
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	EquipRoleDetail createEquipRoleDetail(EquipRoleDetail eRDetailNew)throws DataAccessException;
	
	
	/***
	 * @author vuongmq
	 * @date Mar 23,2015
	 * @description lay danh sach them cho equip_role left join equip_role_detail de xuat excel quan lý quyen kho thiet bi
	 */
	List<EquipProposalBorrowVO> searchListStockPermissionVOByFilterExport(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws DataAccessException;
	/**
	 * Tim kiem kho cap cha
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	List<EquipProposalBorrowVO> searchLstAddStockId(Long id)throws DataAccessException;
	/**
	 * Tim kiem kho cap con
	 * 
	 * @author hoanv25
	 * @since March 09,2015
	 * */
	List<EquipProposalBorrowVO> searchLstParenIdAddStockId(Long id)throws DataAccessException;
	/**
	 * Lay danh sach kho de copy
	 * 
	 * @author hoanv25
	 * @since March 26,2015
	 */
	List<EquipProposalBorrowVO> getListEquipRoleDetailByEquipRoleCode(Long id)throws DataAccessException;

	/**
	 * Check id equip role da duoc gan quyen chua
	 * 
	 * @param: id
	 * @return: lst id
	 * @author hoanv25
	 * @since March 26,2015
	 * @throws BusinessException
	 */
	List<EquipProposalBorrowVO> listEquipRoleById(Long id) throws DataAccessException;

	/**
	 * Ham lay shop_id cua shop cha nhap vao
	 * 
	 * @param: id
	 * @return: lst id
	 * @author hoanv25
	 * @since April 14,2015
	 * @throws BusinessException
	 */
	List<EquipProposalBorrowVO> searchCheckShopId(Long id) throws DataAccessException;
}

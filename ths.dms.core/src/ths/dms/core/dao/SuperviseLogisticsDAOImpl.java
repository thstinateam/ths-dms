package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.RoutingCar;
import ths.dms.core.entities.RoutingCarCust;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.CarFilter;
import ths.dms.core.entities.vo.CarVO;
import ths.dms.core.entities.vo.LatLngVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class SuperviseLogisticsDAOImpl implements SuperviseLogisticsDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Autowired
	private CommonDAO commonDAO;
	
	@Override
	public void createListCustomerArea(List<LatLngVO> lst, LogInfoVO logInfo)throws DataAccessException  {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		for(LatLngVO temp:lst){
			sql = new StringBuilder();
			params = new ArrayList<Object>();
			sql.append(" insert into customer_area(customer_area_id,shop_id,customer_id,lat,lng,angle,create_time) values ");
			sql.append(" (customer_area_seq.nextval,?,?,?,?,?,sysdate) ");
			params.add(temp.getShopId());
			params.add(temp.getCustomerId());
			params.add(temp.getLat());
			params.add(temp.getLng());
			params.add(temp.getGoc());
			repo.executeSQLQuery(sql.toString(), params);
		}
	}

	@Override
	public void deleteCustomerArea(Long shopId, LogInfoVO logInfo) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" delete from customer_area where shop_id=? ");
		params.add(shopId);
		repo.executeSQLQuery(sql.toString(), params);
	}
	
	@Override
	public List<CarVO> getListShopCar(Long parentShopId)
			throws DataAccessException {
		if (null == parentShopId ) {
			return new ArrayList<CarVO>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
			sql.append(" with lstMien as  (select shop_id from shop where status=1 and parent_shop_id=?), ");
			params.add(parentShopId);
			sql.append("  tInfo as (select m.shop_id mienId, v.shop_id vungId, s.shop_id nppId from ( ");
			sql.append("     select car_position_LOG_ID, car_id,  shop_id ");
			sql.append("     from ( SELECT car_position_LOG_ID, car_id, shop_id,is_warning, ");
			sql.append("             ROW_NUMBER() OVER(PARTITION BY car_id,shop_id ORDER BY car_position_LOG_ID DESC) RN ");
			sql.append("             FROM car_position_log ");
			sql.append("             WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date ");
			sql.append("           ) where rn = 1 AND is_warning=1 ");
			sql.append("       )t  ");
			sql.append(" join shop s on s.shop_id = t.shop_id ");
			sql.append(" join shop v on v.shop_id = s.parent_shop_id ");
			sql.append(" join shop m on m.shop_id = v.parent_shop_id) ");
			sql.append(" select shop_id as shopId,shop_code as shopCode,shop_name as shopName,lat,lng, ");
			sql.append(" (select object_type from channel_type where channel_type_id=s.shop_type_id) as shopType, ");
			sql.append(" (select count(1) from car where  shop_id in (select shop_id from shop "); 
			sql.append("     where status=1 start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id )) as countCar, "); 
			sql.append(" (select count(1) from tInfo where vungId= s.shop_id or mienId = s.shop_id) countWarning, ");
			sql.append(" (SELECT count(1) FROM (select distinct car_id,shop_id from routing_position_car_log ");
			sql.append(" 		   WHERE is_or=1 and trunc(create_date)=trunc(sysdate)) t where shop_id IN ");
			sql.append(" 		       (SELECT shop_id FROM shop WHERE status=1 START WITH shop_id = s.shop_id "); 
			sql.append(" 		        CONNECT BY PRIOR shop_id = parent_shop_id)) AS countRouting ");
			sql.append("  from shop s ");   
			sql.append("  where (s.parent_shop_id=? or s.parent_shop_id in (select shop_id from lstMien)) and s.status=1 ");
			params.add(parentShopId);

		final String[] fieldNames = new String[] {
				"shopId","shopCode","shopName",
				"lat", "lng", "shopType",
				"countCar", "countWarning","countRouting"
				 };

		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.LONG,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER
				};
		return repo.getListByQueryAndScalar(CarVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<CarVO> getListShopCarOneNode(Long parentShopId)
			throws DataAccessException {
		if (null == parentShopId) {
			return new ArrayList<CarVO>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with   tInfo as (select m.shop_id mienId, v.shop_id vungId, s.shop_id nppId from ( ");
		sql.append(" 	    select car_position_LOG_ID, car_id,  shop_id ");
		sql.append(" 	    from ( SELECT car_position_LOG_ID, car_id, shop_id,is_warning, ");
		sql.append(" 	            ROW_NUMBER() OVER(PARTITION BY car_id,shop_id ORDER BY car_position_LOG_ID DESC) RN ");
		sql.append(" 	            FROM car_position_log ");
		sql.append(" 	            WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date ");
		sql.append(" 	          ) where rn = 1 AND is_warning=1 ");
		sql.append(" 	      )t  ");
		sql.append(" 	join shop s on s.shop_id = t.shop_id ");
		sql.append(" 	join shop v on v.shop_id = s.parent_shop_id ");
		sql.append(" 	join shop m on m.shop_id = v.parent_shop_id) ");
		sql.append(" 	select shop_id as shopId,shop_code as shopCode,shop_name as shopName,lat,lng, ");
		sql.append(" 	(select object_type from channel_type where channel_type_id=s.shop_type_id) as shopType, ");
		sql.append(" 	(select count(1) from car where  shop_id in (select shop_id from shop "); 
		sql.append(" 	    where status=1 start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id )) as countCar, "); 
		sql.append(" 	(select count(1) from tInfo where  nppId = s.shop_id or vungId= s.shop_id or mienId = s.shop_id) countWarning, ");
		sql.append(" (SELECT count(1) FROM (select distinct car_id,shop_id from routing_position_car_log ");
		sql.append(" 		   WHERE is_or=1 and trunc(create_date)=trunc(sysdate)) t where shop_id IN ");
		sql.append(" 		       (SELECT shop_id FROM shop WHERE status=1 START WITH shop_id = s.shop_id "); 
		sql.append(" 		        CONNECT BY PRIOR shop_id = parent_shop_id)) AS countRouting ");
		sql.append(" 	 from shop s ");   
		sql.append(" 	 where s.parent_shop_id=? and s.status=1 ");
		params.add(parentShopId);
		final String[] fieldNames = new String[] {
				"shopId","shopCode","shopName", "lat", "lng", 
				"shopType","countCar", "countWarning",
				"countRouting"
				 };

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.LONG,StandardBasicTypes.STRING,StandardBasicTypes.STRING, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, 
		StandardBasicTypes.INTEGER,	StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
		StandardBasicTypes.INTEGER,
		};
		return repo.getListByQueryAndScalar(CarVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<Shop> getListAncestor(Long parentShopId, String shopCode,
			String shopName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShop as  ");
		sql.append(" ( select shop_id from shop start with shop_id =? connect by prior shop_id =parent_shop_id), ");
		params.add(parentShopId);
		sql.append(" lstSearch as( ");
		sql.append(" select * from shop start with shop_id = "); 
		sql.append("     (select shop_id from shop where status=1 and ( 0=1 ");
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" or shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" or upper(shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shopName.toUpperCase()));
		}
		sql.append("   )  and shop_id in ( select shop_id from lstShop)  and rownum=1) ");
		sql.append("   connect by prior parent_shop_id= shop_id  ");
		sql.append(" ) ");
		sql.append(" select * from lstSearch lst1 join lstShop lst2 on lst1.shop_id=lst2.shop_id ");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	@Override
	public List<CarVO> getListCar(Long parentShopId)
			throws DataAccessException {
		if (null == parentShopId ) {
			return new ArrayList<CarVO>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" with lstCar as  (select * from car where shop_id=?), ");
		params.add(parentShopId);
		sql.append(" tInfo as (  select * ");
		sql.append(" 	    from ( SELECT car_position_LOG_ID, car_id, shop_id,create_date,lat,lng,is_warning, ");
		sql.append(" 	            ROW_NUMBER() OVER(PARTITION BY car_id,shop_id ORDER BY car_position_LOG_ID DESC) RN ");
		sql.append(" 	            FROM car_position_log ");
		sql.append(" 	            WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date and shop_id=? ");
		params.add(parentShopId);
		sql.append(" 	          ) where rn = 1 ) ");
		sql.append(" 	 select  t.shop_id as shopId,t.car_id as carId, "); 
		sql.append(" 	 to_char(i.create_date,'HH24:MI') as createDate,  ");
		sql.append(" 	 i.is_warning as isWarning,  ");
		sql.append(" 	 i.lat,i.lng, t.car_number as carNumber, ");
		sql.append(" 	 (select short_code from customer where customer_id=t.customer_id) as customerCode, "); 
		sql.append(" 	 (select customer_name from customer where customer_id=t.customer_id) as customerName,  ");
		sql.append(" 	 (select address from customer where customer_id=t.customer_id) as address  ");
		sql.append(" 	 from lstCar t join tInfo i on t.car_id=i.car_id ");
		
		final String[] fieldNames = new String[] {
				"shopId","carId", "minTemp", "maxTemp",
				"createDate","isWarning","position",
				"lat", "lng", "carCode","imei",
				"productYear","warranty", 
				 "customerCode",
				"customerName","address"
				 };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,StandardBasicTypes.LONG,StandardBasicTypes.FLOAT,StandardBasicTypes.FLOAT,
				StandardBasicTypes.STRING,StandardBasicTypes.INTEGER,StandardBasicTypes.FLOAT,
				StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(CarVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<CarVO> getListCarKP(CarFilter filter) throws DataAccessException {
		if (null == filter.getParentShopId()) {
			return new ArrayList<CarVO>();
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder withSql = new StringBuilder();
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		if(filter.getDeliveryDate()==null){
			filter.setDeliveryDate(commonDAO.getSysDate());
		}
		
		withSql.append(" with lstShop as ( ");
		withSql.append("     select shop_id from shop start with shop_id in (-1");
		if(filter.getParentShopId()!=null){
			withSql.append(",?");
			params.add(filter.getParentShopId());
		}
		withSql.append(" ) connect by prior shop_id = parent_shop_id ");
		withSql.append(" ), ");
		withSql.append(" lstCar as  ( ");
		withSql.append("     select * from car where status=1 and shop_id in (select shop_id from lstShop) ");
		if(!StringUtility.isNullOrEmpty(filter.getCarNumber())){
			withSql.append("  and car_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCarNumber().toUpperCase()));
		}
		withSql.append(" ), ");
		withSql.append(" lstRouting as ( ");
		withSql.append("     select rcc.*, rc.car_id, rc.staff_id,rc.shop_id,rc.delivery_date ");
		withSql.append("     from routing_car_cust rcc  ");
		withSql.append("     join routing_car rc on rc.routing_car_id=rcc.routing_car_id ");
		withSql.append("     where trunc(rc.delivery_date)=trunc(?) and rc.status=1 ");
		params.add(filter.getDeliveryDate());
		withSql.append("     and rc.car_id in (select car_id from lstCar) ");
		withSql.append(" ) ");
		sql.append(withSql.toString());
		sql.append(" select ");
		sql.append(" npp.shop_code shopCode, ");
		sql.append(" (select car_number from lstCar where car_id=rpl.car_id) carNumber, ");
		sql.append(" (select (select staff_code||'-'||staff_name from staff where staff_id=lr.staff_id) ");
		sql.append("     from lstRouting lr where lr.car_id=rpl.car_id and rownum=1) staffCode, ");
		sql.append(" (select short_code||'-'||customer_name from customer where customer_id=rpl.customer_id) customerCode, ");
		sql.append(" to_char(rpl.create_date,'HH24:MI') as createTime, ");
		sql.append(" rpl.distance ");
		fromSql.append(" from routing_position_car_log rpl ");
		fromSql.append(" join shop npp on npp.shop_id=rpl.shop_id ");
		fromSql.append(" where rpl.car_id in (select car_id from lstCar) ");
		fromSql.append(" and trunc(rpl.create_date)=trunc(?) and rpl.is_or=1 ");
		params.add(filter.getDeliveryDate());
		sql.append(fromSql.toString());
		sql.append(" order by shopCode,rpl.create_date,carNumber ");
		
		final String[] fieldNames = new String[] {
				"shopCode","carNumber","staffCode",
				"customerCode","createTime","distance"
				 };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.INTEGER
				};
		
		if(null == filter.getkPaging()) {
			return repo.getListByQueryAndScalar(CarVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(CarVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	
	@Override
	public List<CarVO> getListCarVungKP(CarFilter filter) throws DataAccessException {
		if (null == filter.getParentShopId()) {
			return new ArrayList<CarVO>();
		}
		if(filter.getDeliveryDate()==null){
			filter.setDeliveryDate(new Date());
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder withSql = new StringBuilder();
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		
		
		sql.append(" with lstShop  as ( ");
		sql.append("     select shop_id from shop start with shop_id = ? ");
		params.add(filter.getParentShopId());
		sql.append("           connect by prior shop_id = parent_shop_id ");
		sql.append(" ),  ");
		sql.append(" lstCar as  (  ");
		sql.append("     select c.* from car c where c.status=1 and c.shop_id in (select * from lstShop) ");
		if(!StringUtility.isNullOrEmpty(filter.getCarNumber())){
			sql.append("  				and c.car_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCarNumber().toUpperCase()));
		}
		sql.append(" ),  ");
		sql.append(" tInfo as (  ");
		sql.append("     select *  from (   ");
		sql.append("          SELECT car_position_LOG_ID, car_id, shop_id,distance, lat,lng,create_date,is_warning,customer_id, ");
		sql.append("           ROW_NUMBER() OVER(PARTITION BY car_id,shop_id ORDER BY car_position_LOG_ID DESC) RN   ");
		sql.append("           FROM car_position_log lg  ");
		sql.append("           WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date ");   
		sql.append("           and shop_id in (select * from lstShop)  ");
		sql.append("     ) where rn = 1 and is_warning=1  ");
		sql.append(" ) ");
		sql.append(" select ");  
		sql.append(" (select shop_code from shop where shop_id=lst.shop_id) shopCode, ");
		sql.append(" (select car_number from car where car_id=lst.car_id) carNumber, ");
		sql.append(" to_char(t.create_date,'HH24:MI') as createTime, ");
		sql.append(" (select short_code||'-'||customer_name from customer where customer_id=t.customer_id) customerCode, "); 
		sql.append(" to_char((select (select staff_code || ' - '|| staff_name from staff where staff_id=rc.staff_id)  ");
		sql.append("     from routing_car rc where status=1 and trunc(delivery_date)=trunc(sysdate)  ");
		sql.append("     and lst.car_id=car_id and rownum=1)) as staffCode ");
		sql.append(" from lstCar lst  ");
		sql.append(" join tInfo t on lst.car_id=t.car_id "); 
		sql.append(" order by shopCode,carNumber ");
		
		final String[] fieldNames = new String[] {
				"shopCode","carNumber","createTime",
				"customerCode","staffCode"
				 };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING
				};
		
		
		countSql.append(" SELECT COUNT(1) AS COUNT from ( ");
		countSql.append(sql.toString());
		countSql.append(" )");
		
		if(null == filter.getkPaging()) {
			return repo.getListByQueryAndScalar(CarVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(CarVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	
	@Override
	public List<CarVO> getListCustShop(Long parentShopId) throws DataAccessException {
		if (null == parentShopId ) {
			return new ArrayList<CarVO>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShop as (select shop_id from shop where status=1 start with shop_id = ? ");
		params.add(parentShopId);
		sql.append("     connect by prior shop_id = parent_shop_id)");
		sql.append(" select customer_id as customerId, short_code as shortCode,customer_code as customerCode, ");
		sql.append(" customer_name as customerName, address as address, shop_id as shopId, ");
		sql.append(" lat,lng from customer  ");
		sql.append(" where status=1 and shop_id in (select shop_id from lstShop) ");
		sql.append(" and nvl(lat,-1)!=-1  ");
		sql.append(" and nvl(lng,-1)!=-1 ");
//		sql.append(" and 9.223049<lat  and lat<11.675387 ");
//		sql.append(" and  104.300596<lng  and lng<108.535826 "); 
		sql.append(" order by shop_id,customer_id ");
		
		final String[] fieldNames = new String[] {
				"customerId","shortCode", "customerCode",
				"customerName","address","shopId",
				"lat", "lng"
				 };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.LONG,
				StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT };
		return repo.getListByQueryAndScalar(CarVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<CarVO> getListCarPosition(Long parentShopId,Long maxLogId) throws DataAccessException {
		if (null == parentShopId ) {
			return new ArrayList<CarVO>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select cl.car_position_log_id id,cl.car_id carId,cl.shop_id shopId,cl.lat,cl.lng,is_warning isWarning, ");
		sql.append(" cl.distance,to_char(cl.create_date,'HH24:MI') createDate, ");
		sql.append(" (select car_number from car where car_id=cl.car_id) as carNumber, ");
		sql.append(" (select shop_code from shop where shop_id=cl.shop_id) as shopCode, ");
		sql.append(" (select (select staff_code || ' - '|| staff_name from staff where staff_id=rc.staff_id) ");
		sql.append(" 	    from routing_car rc where rc.status=1 and trunc(rc.delivery_date)=trunc(sysdate) ");
		sql.append(" 	    and cl.car_id=rc.car_id and rownum=1) staffCode, ");
		sql.append(" (select count(1) from routing_car_cust rcc join routing_car rc on rcc.routing_car_id=rc.routing_car_id ");
		sql.append(" 		where trunc(rc.delivery_date)=trunc(sysdate) and rc.car_id=cl.car_id) tongSoDiemGiao, ");
		sql.append(" (select count(1) from routing_position_car_log where trunc(create_date)=trunc(sysdate) ");
		sql.append(" and car_id=cl.car_id) diemDaGiao, ");
		sql.append(" (select count(1) from routing_position_car_log where trunc(create_date)=trunc(sysdate) ");
		sql.append(" and is_or=0 and car_id=cl.car_id) diemDaGiaoTrongTuyen, ");
		sql.append(" cl.FUEL_PERCENT fuelPercent,cl.gps_speed gpsSpeed ");
		sql.append(" from car_position_log cl");
		sql.append(" where cl.shop_id=? ");
		params.add(parentShopId);
		sql.append(" and TRUNC(sysdate)<=cl.create_date AND TRUNC(sysdate)+1 >cl.create_date ");
		if(maxLogId!=null){
			sql.append(" and cl.car_position_log_id > ? ");
			params.add(maxLogId);
		}
		sql.append(" order by cl.car_id,cl.create_date desc ");
		
		final String[] fieldNames = new String[] {
				"id","carId","shopId",
				"lat","lng","isWarning",
				"createDate","carNumber","shopCode",
				"staffCode","tongSoDiemGiao","diemDaGiao",
				"fuelPercent","gpsSpeed","diemDaGiaoTrongTuyen"
				 };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,StandardBasicTypes.LONG,StandardBasicTypes.LONG,
				StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT,StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER
				};
		return repo.getListByQueryAndScalar(CarVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<CarVO> getListRoutingCustPosition(CarFilter filter) throws DataAccessException {
		if (null == filter.getShopId()) {
			return new ArrayList<CarVO>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select rc.shop_id shopId,c.customer_id customerId,rc.car_id carId,rcc.seq,c.lat,c.lng, ");
		sql.append(" (c.short_code||' - '||c.customer_name||', '||c.address) customerCode ");
		sql.append(" from routing_car_cust rcc  ");
		sql.append(" join routing_car rc on rcc.routing_car_id=rc.routing_car_id ");
		sql.append(" join customer c on rcc.customer_id=c.customer_id ");
		sql.append(" where trunc(rc.delivery_date)=trunc(sysdate) ");
		sql.append(" and rc.shop_id=? ");
		params.add(filter.getShopId());
		sql.append(" order by carId,seq");
		final String[] fieldNames = new String[] {
				"shopId","customerId","carId","seq",
				"lat","lng","customerCode"
				 };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,StandardBasicTypes.LONG,StandardBasicTypes.LONG,StandardBasicTypes.INTEGER,
				StandardBasicTypes.FLOAT,StandardBasicTypes.FLOAT, StandardBasicTypes.STRING
				};
		return repo.getListByQueryAndScalar(CarVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<CarVO> getListRoutingCustStatus(CarFilter filter) throws DataAccessException {
		if (null == filter.getShopId()) {
			return new ArrayList<CarVO>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select customer_id customerId,car_id carId ");
		sql.append(" from routing_position_car_log where shop_id=?  ");
		params.add(filter.getShopId());
		sql.append(" and trunc(create_date)=trunc(sysdate) and is_or=0 ");
		final String[] fieldNames = new String[] {
				"customerId","carId"
				 };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,StandardBasicTypes.LONG
				};
		return repo.getListByQueryAndScalar(CarVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<LatLngVO> getListCustomerArea(Long parentShopId) throws DataAccessException {
		if (null == parentShopId ) {
			return new ArrayList<LatLngVO>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShop as (select shop_id from shop start with shop_id=? connect by prior shop_id=parent_shop_id)");
		params.add(parentShopId);
		sql.append(" select customer_id customerId,shop_id shopId,lat,lng ");
		sql.append(" from customer_area where shop_id in (select shop_id from lstShop) ");
		
		final String[] fieldNames = new String[] {
				"customerId","shopId","lat","lng",
				 };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,StandardBasicTypes.LONG,
				StandardBasicTypes.DOUBLE, StandardBasicTypes.DOUBLE};
		return repo.getListByQueryAndScalar(LatLngVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<LatLngVO> getListCustomerByShop(CarFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select customer_id customerId, shop_id shopId, lat, lng ");
		sql.append(" from customer where status=1 ");
		sql.append(" and nvl(lat,-1)!=-1 ");
		sql.append(" and nvl(lng,-1)!=-1 ");
		if(filter.getShopId()!=null){
			sql.append(" and shop_id =? ");
			params.add(filter.getShopId());
		}
		final String[] fieldNames = new String[] {
				"customerId","shopId","lat","lng"};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,StandardBasicTypes.LONG,
				StandardBasicTypes.DOUBLE, StandardBasicTypes.DOUBLE};
		return repo.getListByQueryAndScalar(LatLngVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	
	@Override
	public RoutingCar createRoutingCar(RoutingCar rcar, LogInfoVO logInfo)throws DataAccessException  {
		if (rcar == null) {
			throw new IllegalArgumentException("routingcar");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		rcar.setCreateDate(commonDAO.getSysDate());
		if (logInfo != null){
			rcar.setCreateUser(logInfo.getStaffCode());
		}
		repo.create(rcar);
		return repo.getEntityById(RoutingCar.class, rcar.getId());
	}
	
	@Override
	public RoutingCarCust createRoutingCarCust(RoutingCarCust rcar, LogInfoVO logInfo)throws DataAccessException  {
		if (rcar == null) {
			throw new IllegalArgumentException("RoutingCarCust");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		rcar.setCreateDate(commonDAO.getSysDate());
		if (logInfo != null){
			rcar.setCreateUser(logInfo.getStaffCode());
		}
		repo.create(rcar);
		return repo.getEntityById(RoutingCarCust.class, rcar.getId());
	}
	
	@Override
	public RoutingCar getRoutingCarById(long id) throws DataAccessException {
		return repo.getEntityById(RoutingCar.class, id);
	}
	
	@Override
	public RoutingCarCust getRoutingCarCustById(long id) throws DataAccessException {
		return repo.getEntityById(RoutingCarCust.class, id);
	}
	
	@Override
	public void updateRoutingCar(RoutingCar rcar, LogInfoVO logInfo) throws DataAccessException {
		if (rcar == null) {
			throw new IllegalArgumentException("RoutingCar");
		}
		rcar.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			rcar.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(rcar);
	}
	
	@Override
	public void updateRoutingCarCust(RoutingCarCust rcar, LogInfoVO logInfo) throws DataAccessException {
		if (rcar == null) {
			throw new IllegalArgumentException("RoutingCar");
		}
		rcar.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			rcar.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(rcar);
	}
	
	@Override
	public List<CarVO> getListCustomerByRouting(CarFilter filter)
			throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		
		sql.append(" with lstRouting as ( ");
		sql.append("   select rcc.routing_car_cust_id id,rcc.customer_id,rcc.seq ");
		sql.append("   from routing_car_cust rcc ");
		sql.append("   join routing_car rcar on rcc.routing_car_id=rcar.routing_car_id ");
		sql.append("   where 1=1 and rcar.status=1 ");
		if(filter.getCarId()!=null){
			sql.append("   and rcar.car_id=? ");
			params.add(filter.getCarId());
		}
		if(filter.getDeliveryDate()!=null){
			sql.append("   and rcar.delivery_date>=trunc(?) and rcar.delivery_date<trunc(?)+1 ");
			params.add(filter.getDeliveryDate());
			params.add(filter.getDeliveryDate());
		}
		sql.append(" ), ");
		sql.append(" lstSO as ( ");
		sql.append(" select null id,customer_id,0 seq from ( ");
		sql.append("   select distinct customer_id ");
		sql.append("   from sale_order so where type=1 and approved=1 and order_type in ('IN','TT') ");
		if(filter.getCarId()!=null){
			sql.append("   and so.car_id=? ");
			params.add(filter.getCarId());
		}
		if(filter.getDeliveryDate()!=null){
			sql.append("   and so.delivery_date>=trunc(?) and so.delivery_date<trunc(?)+1 ");
			params.add(filter.getDeliveryDate());
			params.add(filter.getDeliveryDate());
		}
		sql.append("   and so.customer_id not in (select customer_id from lstRouting) ");
		sql.append(" )) ");
		sql.append(" select t.id,t.customer_id customerId,t.seq, ");
		sql.append(" (select short_code from customer where customer_id=t.customer_id) shortCode, ");
		sql.append(" (select customer_name from customer where customer_id=t.customer_id) customerName, ");
		sql.append(" (select address from customer where customer_id=t.customer_id) address, ");
		sql.append(" (select lat from customer where customer_id=t.customer_id) lat, ");
		sql.append(" (select lng from customer where customer_id=t.customer_id) lng ");
		sql.append(" from ( ");
		sql.append("   select * from lstRouting ");
		sql.append("   union all ");
		sql.append("   select * from lstSO ");
		sql.append(" ) t ");
		sql.append(" order by shortCode ");
		
		final String[] fieldNames = new String[] {
				"id","customerId","seq",
				"shortCode", "customerName", "address",
				"lat","lng"
				 };
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,StandardBasicTypes.LONG,StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.FLOAT,StandardBasicTypes.FLOAT
				};
		
		if(null == filter.getkPaging()) {
			return repo.getListByQueryAndScalar(CarVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			countSql.append("select count(1) as count  from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(CarVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	
	@Override
	public List<CarVO> getListCustomerBySaleOrder(CarFilter filter)
			throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		
		sql.append(" select  distinct(so.customer_id) customerId, ");
		sql.append(" (select short_code from customer where customer_id=so.customer_id) shortCode, ");
		sql.append(" (select customer_name from customer where customer_id=so.customer_id) customerName, ");
		sql.append(" (select address from customer where customer_id=so.customer_id) address, ");
		sql.append(" (select lat from customer where customer_id=so.customer_id) lat, ");
		sql.append(" (select lng from customer where customer_id=so.customer_id) lng ");
		sql.append(" from sale_order so ");
		sql.append(" where 1=1 ");
		if(filter.getDeliveryDate()!=null){
			sql.append(" and so.delivery_date>=trunc(?) and so.delivery_date<trunc(?)+1 ");
			params.add(filter.getDeliveryDate());
			params.add(filter.getDeliveryDate());
		}
		if(filter.getCarId()!=null){
			sql.append(" and so.car_id=? ");
			params.add(filter.getCarId());
		}
		sql.append(" order by shortCode ");
		
		
		final String[] fieldNames = new String[] {
				"customerId","shortCode", "customerName", "address",
				"lat","lng"
				 };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.FLOAT,StandardBasicTypes.FLOAT
				};
		
		if(null == filter.getkPaging()) {
			return repo.getListByQueryAndScalar(CarVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(CarVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	@Override
	public List<CarVO> getListRouting(CarFilter filter)
			throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		if(filter.getFromDate()==null){
			filter.setFromDate(commonDAO.getSysDate());
		}
		
		sql.append(" with lstShop as ( ");
		sql.append("     select shop_id from shop start with shop_id in (-1");
		if(filter.getLstShopId()!=null){
			for(int i=0;i<filter.getLstShopId().size();i++){
				sql.append(",?");
				params.add(filter.getLstShopId().get(i));
			}
		}
		sql.append(" ) connect by prior shop_id = parent_shop_id ");
		sql.append(" ), ");
		sql.append(" lstCar as  ( ");
		sql.append("     select * from car where status=1 and shop_id in (select shop_id from lstShop) ");
		if(!StringUtility.isNullOrEmpty(filter.getCarNumber())){
			sql.append("  and car_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCarNumber().toUpperCase()));
		}
		sql.append(" ), ");
		sql.append(" lstRouting as ( ");
		sql.append("     select rcc.*, rc.car_id, rc.staff_id,rc.shop_id,rc.delivery_date ");
		sql.append("     from routing_car_cust rcc  ");
		sql.append("     join routing_car rc on rc.routing_car_id=rcc.routing_car_id ");
		sql.append("     where rc.status=1 ");
		if(filter.getFromDate()!=null){
			sql.append(" and rc.delivery_date>=trunc(?) ");
			params.add(filter.getFromDate());
		}
		if(filter.getToDate()!=null){
			sql.append(" and rc.delivery_date<trunc(?)+1 ");
			params.add(filter.getToDate());
		}
		sql.append("     and rc.car_id in (select car_id from lstCar) ");
		sql.append(" ) ");
		sql.append(" select (mien.shop_code||'-'||mien.shop_name) mien, ");
		sql.append(" (vung.shop_code||'-'||vung.shop_name) vung, ");
		sql.append(" (npp.shop_code||'-'||npp.shop_name) shopCode, ");
		sql.append(" (select car_number from lstCar where car_id=rpl.car_id) carNumber, ");
		sql.append(" (select (select staff_code||'-'||staff_name from staff where staff_id=lr.staff_id) ");
		sql.append("     from lstRouting lr where lr.car_id=rpl.car_id and rownum=1) staffCode, ");
		sql.append(" (select short_code||'-'||customer_name from customer where customer_id=rpl.customer_id) customerCode, ");
		sql.append(" (select address from customer where customer_id=rpl.customer_id) address, ");
		sql.append(" (case when rpl.is_or=0 then 'Trong tuyến' else 'Ngoại tuyến' end) isOr, ");
		sql.append(" rpl.seq,to_char(rpl.create_date,'dd/MM/yyyy HH24:MI') as createTime, ");
		sql.append(" (select lr.seq from lstRouting lr ");
		sql.append("      where lr.customer_id=rpl.customer_id and lr.car_id=rpl.car_id and rownum=1) seqSet ");
		sql.append(" from routing_position_car_log rpl ");
		sql.append(" join shop npp on npp.shop_id=rpl.shop_id ");
		sql.append(" join shop vung on vung.shop_id=npp.parent_shop_id ");
		sql.append(" join shop mien on mien.shop_id=vung.parent_shop_id ");
		sql.append(" where rpl.car_id in (select car_id from lstCar) ");
		if(filter.getFromDate()!=null){
			sql.append(" and rpl.create_date>=trunc(?) ");
			params.add(filter.getFromDate());
		}
		if(filter.getToDate()!=null){
			sql.append(" and rpl.create_date<trunc(?)+1 ");
			params.add(filter.getToDate());
		}
		sql.append(" order by mien,vung,shopCode,rpl.create_date,carNumber ");
		
		final String[] fieldNames = new String[] {
				"mien","vung","shopCode",
				"carNumber","staffCode","customerCode",
				"address","isOr","seq",
				"createTime","seqSet"
				 };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING,StandardBasicTypes.INTEGER
				};
		
		if(null == filter.getkPaging()) {
			return repo.getListByQueryAndScalar(CarVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(CarVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	
	@Override
	public Boolean checkDriverHasCar(Long idRouting,Long staffId,Date deliveryDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from routing_car where status=1 ");
		sql.append(" and trunc(delivery_date)=trunc(?) and staff_id=? "); 
		params.add(deliveryDate);
		params.add(staffId);
		if(idRouting!=null){
			sql.append(" and routing_car_id!=? ");
			params.add(idRouting);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}
	
	public List<CarVO> getListRoutingExist(CarFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" with lstShop as (  ");
		sql.append("     select shop_id from shop start with shop_id in (-1 ");
		if(filter.getLstShopId()!=null){
			for(int i=0;i<filter.getLstShopId().size();i++){
				sql.append(",?");
				params.add(filter.getLstShopId().get(i));
			}
		}
		sql.append("     ) connect by prior shop_id = parent_shop_id ");
		sql.append(" ), ");
		sql.append(" lstCar as ( ");
		sql.append("     select car_id from car c where c.status=1 and c.shop_id in (select shop_id from lstShop) ");
		if(!StringUtility.isNullOrEmpty(filter.getCarNumber())){
			sql.append("  and c.car_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCarNumber().toUpperCase()));
		}
		sql.append(" ), ");
		sql.append(" lstReal as ( ");
		sql.append("     select customer_id,car_id ");
		sql.append("     from routing_position_car_log ");
		sql.append("     where 1=1 ");
//		if(filter.getDeliveryDate()!=null){
//			sql.append(" and trunc(create_date)=trunc(?) ");
//			params.add(filter.getDeliveryDate());
//		}
		if(filter.getFromDate()!=null){
			sql.append(" and create_date>=trunc(?) ");
			params.add(filter.getFromDate());
		}
		if(filter.getToDate()!=null){
			sql.append(" and create_date<trunc(?)+1 ");
			params.add(filter.getToDate());
		}
		sql.append("     and car_id in (select car_id from lstCar) ");
		sql.append(" ), ");
		sql.append(" lstRouting as ( ");
		sql.append("     select rcc.routing_car_cust_id id,rcc.customer_id,rc.car_id, ");
		sql.append("     rc.shop_id,rc.staff_id,rc.delivery_date ");
		sql.append("     from routing_car_cust rcc  ");
		sql.append("     join routing_car rc on rcc.routing_car_id=rc.routing_car_id ");
		sql.append("     where 1=1 ");
//		if(filter.getDeliveryDate()!=null){
//			sql.append(" and trunc(rc.delivery_date)=trunc(?) ");
//			params.add(filter.getDeliveryDate());
//		}
		if(filter.getFromDate()!=null){
			sql.append(" and rc.delivery_date>=trunc(?) ");
			params.add(filter.getFromDate());
		}
		if(filter.getToDate()!=null){
			sql.append(" and rc.delivery_date<trunc(?)+1 ");
			params.add(filter.getToDate());
		}
		sql.append("     and rc.car_id in (select car_id from lstCar) ");
		sql.append(" ), ");
		sql.append(" lstJoin as ( ");
		sql.append("     select id ");
		sql.append("     from lstReal real   ");
		sql.append("     join lstRouting routing on real.customer_id=routing.customer_id and real.car_id=routing.car_id ");
		sql.append(" ) ");

		sql.append(" select  ");
		sql.append(" (select shop_code||'-'||shop_name from shop where shop_id=lr.shop_id) shopCode, ");
		sql.append(" (select car_number from car where car_id=lr.car_id) carNumber, ");
		sql.append(" (select staff_code||'-'||staff_name from staff where staff_id=lr.staff_id) driverCode, ");
		sql.append(" (select short_code||'-'||customer_name from customer where customer_id=lr.customer_id) customerCode, ");
		sql.append(" (select (select staff_code||'-'||staff_name  ");
		sql.append("     from staff where staff_id=so.delivery_id)   ");
		sql.append("     from sale_order so where trunc(so.delivery_date)=trunc(lr.delivery_date)  ");
		sql.append("     and so.car_id=lr.car_id and so.customer_id=lr.customer_id and rownum=1) staffCode, ");
		sql.append(" to_char(lr.delivery_date ,'dd/MM/yyyy') createDate ");
		sql.append(" from lstRouting lr where id not in (select id from lstJoin) ");
		sql.append(" order by shopCode,createDate,carNumber,customerCode ");
		
				
		final String[] fieldNames = new String[] {
				"shopCode","carNumber","driverCode",
				"customerCode","staffCode","createDate"
				 };

		final Type[] fieldTypes = new Type[] { 
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING
			};
		
		if(null == filter.getkPaging()) {
			return repo.getListByQueryAndScalar(CarVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(CarVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	
	@Override
	public List<RoutingCar> getRoutingCarByFilter(CarFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select rc.* from routing_car rc where rc.status=1 ");
		if(filter.getCarId()!=null){
			sql.append(" and rc.car_id=? ");
			params.add(filter.getCarId());
		}
		if(filter.getDeliveryDate()!=null){
			sql.append(" and trunc(rc.delivery_date)=trunc(?) ");
			params.add(filter.getDeliveryDate());
		}
		
		if (filter.getkPaging() == null)
			return repo.getListBySQL(RoutingCar.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(RoutingCar.class, sql.toString(), params, filter.getkPRoutingCar());
	}
	
	@Override
	public List<CarVO> getListRoutingExistKP(CarFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" WITH lstShop AS ");
		sql.append("   (SELECT shop_id FROM shop START WITH shop_id IN (-1  ");
		if(filter.getLstShopId()!=null){
			for(int i=0;i<filter.getLstShopId().size();i++){
				sql.append(",?");
				params.add(filter.getLstShopId().get(i));
			}
		}
		sql.append("   ) CONNECT BY PRIOR shop_id = parent_shop_id ");
		sql.append(" ), ");
		sql.append(" lstCar AS ");
		sql.append("   (SELECT car_id  FROM car c ");
		sql.append("    WHERE c.status=1 AND c.shop_id IN (SELECT shop_id  FROM lstShop) ");
		if(!StringUtility.isNullOrEmpty(filter.getCarNumber())){
			sql.append("  and c.car_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCarNumber().toUpperCase()));
		}
		sql.append(" ), ");
		sql.append(" lstReal AS ");
		sql.append("   (SELECT customer_id, car_id ");
		sql.append("    FROM routing_position_car_log ");
		sql.append("    WHERE 1=1 ");
		if(filter.getDeliveryDate()!=null){
			sql.append("      AND trunc(create_date)=trunc(?) ");
			params.add(filter.getDeliveryDate());
		}
		sql.append("      AND car_id IN (SELECT car_id FROM lstCar) ");
		sql.append(" ), ");
		sql.append(" lstRouting AS ");
		sql.append("   (SELECT rcc.routing_car_cust_id id,rcc.customer_id,rc.car_id, rc.shop_id,rc.staff_id,rc.delivery_date ");
		sql.append("    FROM routing_car_cust rcc ");
		sql.append("    JOIN routing_car rc ON rcc.routing_car_id=rc.routing_car_id ");
		sql.append("    WHERE 1=1 ");
		if(filter.getDeliveryDate()!=null){
			sql.append("      AND trunc(rc.delivery_date)=trunc(?) ");
			params.add(filter.getDeliveryDate());
		}
		sql.append("      AND rc.car_id IN (SELECT car_id FROM lstCar) ");
		sql.append(" ), ");
		sql.append(" lstJoin AS ");
		sql.append("   (SELECT id FROM lstReal real ");
		sql.append("    JOIN lstRouting routing ON real.customer_id=routing.customer_id ");
		sql.append("    AND real.car_id=routing.car_id ");
		sql.append(" ) ");
		sql.append(" select t.shopCode,t.carNumber,t.driverCode,t.countWarning, ");
		sql.append(" (select LISTAGG(staffCode, ', ') WITHIN GROUP (ORDER BY staffCode) staffCode from "); 
		sql.append("     (SELECT distinct (SELECT staff_code FROM staff WHERE staff_id=so.delivery_id) staffCode, ");
		sql.append("         car_id,delivery_date ");
		sql.append("          FROM sale_order so ");
		sql.append("          WHERE 1=1 ");
		if(filter.getDeliveryDate()!=null){
			sql.append("          and trunc(so.delivery_date)=trunc(?) ");
			params.add(filter.getDeliveryDate());
		}
		sql.append("          and so.car_id in (select car_id from lstCar) ");
		sql.append("     ) so1  ");
		sql.append("     where so1.car_id=t.car_id ");
		sql.append("     and trunc(so1.delivery_date)=trunc(t.delivery_date) ");
		sql.append(" ) staffCode ");
		sql.append(" from (SELECT ");
		sql.append("           (SELECT shop_code||'-'||shop_name FROM shop WHERE shop_id=lr.shop_id) shopCode, ");
		sql.append("            lr.car_id , ");
		sql.append("            lr.delivery_date, ");
		sql.append("           (SELECT car_number FROM car WHERE car_id=lr.car_id) carNumber, ");
		sql.append("           (SELECT staff_code||'-'||staff_name FROM staff WHERE staff_id=lr.staff_id) driverCode, ");
		sql.append("           count(1) countWarning ");
		sql.append("         FROM lstRouting lr ");
		sql.append("         WHERE id NOT IN (SELECT id FROM lstJoin) ");
		sql.append("         group by lr.shop_id,lr.car_id,lr.staff_id,lr.delivery_date ");
		sql.append(" ) t ");
		sql.append(" ORDER BY shopCode,carNumber ");
		
				
		final String[] fieldNames = new String[] {
				"shopCode","carNumber","driverCode",
				"countWarning","staffCode",
				 };

		final Type[] fieldTypes = new Type[] { 
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.INTEGER,StandardBasicTypes.STRING
			};
		
		if(null == filter.getkPaging()) {
			return repo.getListByQueryAndScalar(CarVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(CarVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	@Override
	public List<CarVO> getBCVT11(CarFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH lstShop AS ");
		sql.append("   (SELECT shop_id ");
		sql.append("    FROM shop START WITH shop_id IN (-1  ");
		if(filter.getLstShopId()!=null){
			for(int i=0;i<filter.getLstShopId().size();i++){
				sql.append(",?");
				params.add(filter.getLstShopId().get(i));
			}
		}
		sql.append("    ) CONNECT BY ");
		sql.append("    PRIOR shop_id = parent_shop_id), ");
		sql.append(" lstCar AS ");
		sql.append("   (SELECT c.*  FROM car c WHERE c.status=1 ");
		sql.append("         AND c.shop_id IN (SELECT shop_id FROM lstShop)) ");
		
		sql.append(" select  ");
		sql.append(" (select shop_code||'-'||shop_name from shop where shop_id=tmp.shop_id) shopCode, ");
		sql.append(" (select shop_code||'-'||shop_name from shop where shop_id in  ");
		sql.append("     (select parent_shop_id from shop s where shop_id in  ");
		sql.append("     (select parent_shop_id from shop where shop_id=tmp.shop_id) and rownum=1)  ");
		sql.append("     and rownum=1) mien, ");
		sql.append("     tmp.car_id carId, ");
		sql.append(" (select car_number from car where car_id=tmp.car_id) carNumber,  ");
		sql.append(" to_char(tmp.create_date,'dd/MM/yyyy') AS createDate,  ");
		sql.append(" to_char(tmp.create_date,'HH24:MI') AS createTime, ");
		sql.append("   (SELECT short_code||' - '||customer_name  FROM customer WHERE customer_id=tmp.customer_id) customerCode, ");
		sql.append("   (SELECT address FROM customer WHERE customer_id=tmp.customer_id) address,  ");
		sql.append("   tmp.distance, ");
		sql.append("   (SELECT (SELECT staff_code || ' - '|| staff_name FROM staff WHERE staff_id=rc.staff_id) ");
		sql.append("    FROM routing_car rc ");
		sql.append("    WHERE status=1 ");
		sql.append("      AND trunc(delivery_date)=trunc(tmp.create_date) ");
		sql.append("      AND tmp.car_id=car_id AND rownum=1) staffCode, ");
		sql.append(" tmp.is_warning isWarning ");
		sql.append(" from  ");
		sql.append(" ( ");
		sql.append("   select car_id,shop_id, create_date, is_warning ,customer_id,distance, ");
		sql.append("   row_number() over ( ");
		sql.append("     PARTITION  by car_id,shop_id ");
		sql.append("     order by create_date, CAR_POSITION_LOG_ID ");
		sql.append("   ) as x ");
		sql.append("   from CAR_POSITION_LOG  where 1=1 ");
		if(filter.getFromDate()!=null){
			sql.append("       AND TRUNC(?)<=create_date ");
			params.add(filter.getFromDate());
		}
		if(filter.getToDate()!=null){
			sql.append(" 	   AND TRUNC(?)+1 >create_date ");
			params.add(filter.getToDate());
		}
		sql.append("   and car_id in (select car_id from lstCar) ");
		sql.append(" ) tmp ");
		sql.append(" left join  ");
		sql.append(" ( ");
		sql.append("   select car_id,shop_id, create_date, is_warning , ");
		sql.append("   row_number() over ( ");
		sql.append("     PARTITION  by car_id,shop_id ");
		sql.append("     order by create_date, CAR_POSITION_LOG_ID ");
		sql.append("   ) as x ");
		sql.append("   from CAR_POSITION_LOG where 1=1 ");
		if(filter.getFromDate()!=null){
			sql.append("       AND TRUNC(?)<=create_date ");
			params.add(filter.getFromDate());
		}
		if(filter.getToDate()!=null){
			sql.append(" 	   AND TRUNC(?)+1 >create_date ");
			params.add(filter.getToDate());
		}
		sql.append("   and car_id in (select car_id from lstCar) ");
		sql.append(" ) tmp1 on tmp.x = tmp1.x +1  and tmp.car_id = tmp1.car_id ");
		sql.append(" where ( ");
		sql.append(" (tmp.IS_WARNING = 1 and tmp1.IS_WARNING = 0)  ");
		sql.append(" or (tmp.IS_WARNING = 0 and tmp1.IS_WARNING = 1)  ");
		sql.append(" or tmp.x = 1 ");
		sql.append(" ) ");
		sql.append(" ORDER BY mien, shopCode,carNumber , tmp.create_date  ");
		
		final String[] fieldNames = new String[] {
				"shopCode","mien","carId",
				"carNumber","createDate","createTime",
				"customerCode","address","distance",
				"staffCode","isWarning"
				 };

		final Type[] fieldTypes = new Type[] { 
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.LONG,
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.INTEGER,
			StandardBasicTypes.STRING,StandardBasicTypes.INTEGER
			};
		return repo.getListByQueryAndScalar(CarVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<CarVO> getBCVT12(CarFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH lstShop AS ");
		sql.append("   (SELECT shop_id ");
		sql.append("    FROM shop START WITH shop_id IN (-1  ");
		if(filter.getLstShopId()!=null){
			for(int i=0;i<filter.getLstShopId().size();i++){
				sql.append(",?");
				params.add(filter.getLstShopId().get(i));
			}
		}
		sql.append("    ) CONNECT BY PRIOR shop_id = parent_shop_id), ");
		sql.append(" lstCar AS ");
		sql.append("   (SELECT c.*  FROM car c WHERE c.status=1 AND c.shop_id IN (SELECT shop_id FROM lstShop)) ");
		sql.append(" select  ");
		sql.append(" (select shop_code||'-'||shop_name from shop where shop_id=tmp.shop_id) shopCode, ");
		sql.append(" (select shop_code||'-'||shop_name from shop where shop_id in  ");
		sql.append("     (select parent_shop_id from shop s where shop_id in  ");
		sql.append("     (select parent_shop_id from shop where shop_id=tmp.shop_id) and rownum=1)  ");
		sql.append("     and rownum=1) mien, ");
		sql.append("     tmp.car_id carId,tmp.create_date time, ");
		sql.append(" (select car_number from car where car_id=tmp.car_id) carNumber,  ");
		sql.append(" to_char(tmp.create_date,'dd/MM/yyyy') AS createDate,  ");
		sql.append(" to_char(tmp.create_date,'HH24:MI') AS createTime, ");
		sql.append(" (SELECT short_code||' - '||customer_name  FROM customer WHERE customer_id=nvl(tmp.customer_id,tmp1.customer_id)) customerCode, ");
		sql.append(" tmp.distance,nvl(tmp.gps_speed,tmp1.gps_speed) gpsSpeed, ");
		sql.append(" (SELECT (SELECT staff_code || ' - '|| staff_name FROM staff WHERE staff_id=rc.staff_id) ");
		sql.append("    FROM routing_car rc WHERE status=1  ");
		sql.append("      AND trunc(delivery_date)=trunc(tmp.create_date) ");
		sql.append("        AND tmp.car_id=car_id AND rownum=1) staffCode, ");
		sql.append(" (select count(1) from routing_car_cust rcc  ");
		sql.append("       join routing_car rc on rcc.routing_car_id=rc.routing_car_id ");
		sql.append("       where trunc(rc.delivery_date)=trunc(tmp.create_date)  ");
		sql.append("       and rc.car_id=tmp.car_id and rcc.customer_id=tmp.customer_id and rownum=1) isOr, ");
		sql.append(" (select count(1) from routing_car_cust rcc  ");
		sql.append("       join routing_car rc on rcc.routing_car_id=rc.routing_car_id ");
		sql.append("       where trunc(rc.delivery_date)=trunc(tmp.create_date)  ");
		sql.append("       and rc.car_id=tmp.car_id ) tongSoDiemGiao ");
		sql.append(" from  ");
		sql.append(" ( ");
		sql.append("   select car_id,shop_id, create_date,gps_speed ,customer_id,distance, ");
		sql.append("   row_number() over ( ");
		sql.append("     PARTITION  by car_id,shop_id ");
		sql.append("     order by create_date, CAR_POSITION_LOG_ID ");
		sql.append("   ) as x ");
		sql.append("   from CAR_POSITION_LOG where 1=1 "); 
		if(filter.getFromDate()!=null){
			sql.append("       AND TRUNC(?)<=create_date ");
			params.add(filter.getFromDate());
		}
		if(filter.getToDate()!=null){
			sql.append(" 	   AND TRUNC(?)+1 >create_date ");
			params.add(filter.getToDate());
		}
		sql.append("   and car_id in (select car_id from lstCar) ");
		sql.append(" ) tmp ");
		sql.append(" left join "); 
		sql.append(" ( ");
		sql.append("   select car_id,shop_id, create_date,gps_speed ,customer_id, ");
		sql.append("   row_number() over ( ");
		sql.append("     PARTITION  by car_id,shop_id ");
		sql.append("     order by create_date, CAR_POSITION_LOG_ID ");
		sql.append("   ) as x ");
		sql.append("   from CAR_POSITION_LOG where 1=1 "); 
		if(filter.getFromDate()!=null){
			sql.append("       AND TRUNC(?)<=create_date ");
			params.add(filter.getFromDate());
		}
		if(filter.getToDate()!=null){
			sql.append(" 	   AND TRUNC(?)+1 >create_date ");
			params.add(filter.getToDate());
		}
		sql.append("   and car_id in (select car_id from lstCar) ");
		sql.append(" ) tmp1 on tmp.x = tmp1.x +1  and tmp.car_id = tmp1.car_id ");
		sql.append(" where ( ");
		sql.append(" (tmp.gps_speed >0 and tmp1.gps_speed = 0) "); 
		sql.append(" or (tmp.gps_speed = 0 and tmp1.gps_speed >0) "); 
		sql.append(" or tmp.x = 1 ");
		sql.append(" ) ");
		sql.append(" ORDER BY mien, shopCode, carNumber,tmp.create_date ");
		
		final String[] fieldNames = new String[] {
				"shopCode","mien","carId","time",
				"carNumber","createDate","createTime",
				"customerCode","distance","gpsSpeed",
				"staffCode","isOr","tongSoDiemGiao"
				 };

		final Type[] fieldTypes = new Type[] { 
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.LONG,StandardBasicTypes.TIMESTAMP,
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER,
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.INTEGER
			};
		return repo.getListByQueryAndScalar(CarVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<CarVO> getBCVT13(CarFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH lstShop AS ");
		sql.append("   (SELECT shop_id ");
		sql.append("    FROM shop START WITH shop_id IN (-1  ");
		if(filter.getLstShopId()!=null){
			for(int i=0;i<filter.getLstShopId().size();i++){
				sql.append(",?");
				params.add(filter.getLstShopId().get(i));
			}
		}
		sql.append("    ) CONNECT BY PRIOR shop_id = parent_shop_id), ");
		sql.append(" lstCar AS ");
		sql.append("  	(SELECT c.*  FROM car c WHERE c.status=1 AND c.shop_id IN (SELECT shop_id FROM lstShop)) ");
		
		sql.append(" select  ");
		sql.append(" (select shop_code||'-'||shop_name from shop where shop_id=t.shop_id) shopCode, ");
		sql.append(" (select shop_code||'-'||shop_name from shop where shop_id in ");
		sql.append("     (select parent_shop_id from shop s where shop_id in ");
		sql.append("       (select parent_shop_id from shop where shop_id=t.shop_id) and rownum=1) ");
		sql.append("       and rownum=1) mien, ");
		sql.append(" (select car_number from car where car_id=t.car_id) carNumber, ");
		sql.append(" (SELECT (SELECT staff_code || ' - '|| staff_name FROM staff WHERE staff_id=rc.staff_id) "); 
		sql.append("     FROM routing_car rc  WHERE status=1  ");
		sql.append(" 		      AND trunc(delivery_date)=trunc(t.create_date) "); 
		sql.append(" 		      AND t.car_id=car_id AND rownum=1) staffCode,  ");
		sql.append(" to_char(t.create_date,'dd/MM/yyyy') as createDate, ");
		sql.append(" to_char(t.create_date,'HH24:MI') as createTime, ");
		sql.append(" to_char(t.lastCreateDate,'dd/MM/yyyy') as lastCreateDate, ");
		sql.append(" to_char(t.lastCreateDate,'HH24:MI') as lastCreateTime, ");
		sql.append(" t.distance,t.lastDistance, ");
		sql.append(" (SELECT short_code||' - '||customer_name  FROM customer WHERE customer_id=t.customer_id) customerCode, ");
		sql.append(" (SELECT short_code||' - '||customer_name  FROM customer WHERE customer_id=t.lastCustomerId) lastCustomerCode, ");
		sql.append(" (SELECT address  FROM customer WHERE customer_id=t.customer_id) address, ");
		sql.append(" (SELECT address  FROM customer WHERE customer_id=t.lastCustomerId) lastAddress ");
		sql.append(" from ( ");
		sql.append("   select cl.car_id, cl.create_date,cl.customer_id,cl.shop_id,distance, ");
		sql.append("   lag(create_date) over (partition by car_id order by create_date) lastCreateDate, ");
		sql.append("   lag(customer_id) over (partition by car_id order by create_date) lastCustomerId, ");
		sql.append("   lag(distance) over (partition by car_id order by create_date) lastDistance, ");
		sql.append("   24*60*(cl.create_date-lag(create_date) over (partition by car_id order by create_date)) diff "); 
		sql.append("   from car_position_log cl  ");
		sql.append("   where 1=1 ");
		if(filter.getFromDate()!=null){
			sql.append("   and cl.create_date>=trunc(?) ");
			params.add(filter.getFromDate());
		}
		if(filter.getToDate()!=null){
			sql.append("   and cl.create_date<trunc(?)+1 ");
			params.add(filter.getToDate());
		}
		sql.append("   and cl.car_id in (select car_id from lstCar) ");
		sql.append("   ) t  ");
		sql.append(" where diff > 30 ");
		sql.append(" order by mien,shopCode,car_id, create_date ");
		final String[] fieldNames = new String[] {
				"shopCode","mien","carNumber",
				"staffCode","createDate","createTime",
				"lastCreateDate","lastCreateTime","distance",
				"lastDistance","customerCode","lastCustomerCode",
				"address","lastAddress"
				 };

		final Type[] fieldTypes = new Type[] { 
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.INTEGER,
			StandardBasicTypes.INTEGER,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,StandardBasicTypes.STRING
			};
		return repo.getListByQueryAndScalar(CarVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<CarVO> getBCVT14(CarFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" with lstShop  as ( ");
		sql.append("   select shop_id from shop start with shop_id in (-1");
		if(filter.getLstShopId()!=null){
			for(int i=0;i<filter.getLstShopId().size();i++){
				sql.append(",?");
				params.add(filter.getLstShopId().get(i));
			}
		}	
		sql.append("   ) connect by prior shop_id = parent_shop_id ");
		sql.append(" ), ");
		sql.append(" lstCar as ( ");
		sql.append("     select * from car c where c.status=1 and c.shop_id in (select shop_id from lstShop) ");
		if(!StringUtility.isNullOrEmpty(filter.getCarNumber())){
			sql.append("  and c.car_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCarNumber().toUpperCase()));
		}
		sql.append(" ), ");
		sql.append(" lstCarPosition as ( ");
		sql.append("    select car_position_LOG_ID, car_id,  shop_id, lat,lng,customer_id,distance ");
		sql.append("    from ( SELECT car_position_LOG_ID, car_id, shop_id,is_warning, lat,lng,customer_id,distance, ");
		sql.append("            ROW_NUMBER() OVER(PARTITION BY car_id,shop_id ORDER BY car_position_LOG_ID DESC) RN "); 
		sql.append("            FROM car_position_log "); 
		sql.append("            WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date "); 
		sql.append("            and car_id in (select car_id from lstCar) ");
		sql.append("          ) where rn = 1 and shop_id in (select shop_id from lstShop) ");
		sql.append(" ) ");
		sql.append(" select m.shop_code mien,s.shop_code shopCode, ");
		sql.append(" (select car_number from car where car_id=lc.car_id) carNumber, ");
		sql.append(" (select (select staff_code||'-'||staff_name from staff where staff_id=rc.staff_id) ");
		sql.append("      from routing_car rc where rc.car_id=lc.car_id and trunc(rc.delivery_date)=trunc(sysdate) and rownum=1) staffCode, ");
		sql.append(" (select short_code||'-'||customer_code from customer where customer_id=lc.customer_id) customerCode, ");
		sql.append(" (select address from customer where customer_id=lc.customer_id) address, ");
		sql.append(" lc.lat,lc.lng,lc.distance ");
		sql.append(" from lstCarPosition lc  ");
		sql.append(" join shop s on s.shop_id = lc.shop_id ");
		sql.append(" join shop v on v.shop_id = s.parent_shop_id ");
		sql.append(" join shop m on m.shop_id = v.parent_shop_id ");
		sql.append(" order by mien,shopCode,carNumber ");
				
		final String[] fieldNames = new String[] {
				"mien","shopCode","carNumber",
				"staffCode","customerCode","address",
				"lat","lng","distance"
				 };

		final Type[] fieldTypes = new Type[] { 
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.FLOAT,StandardBasicTypes.FLOAT,StandardBasicTypes.INTEGER
			};
		return repo.getListByQueryAndScalar(CarVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<CarVO> getBCVT15(CarFilter filter) throws DataAccessException {
		return this.getListRouting(filter);//ham chung
	}
	
	@Override
	public List<CarVO> getBCVT16(CarFilter filter) throws DataAccessException {
		return this.getListRoutingExist(filter);//ham chung
	}
}

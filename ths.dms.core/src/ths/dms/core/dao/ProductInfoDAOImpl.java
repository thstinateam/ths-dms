package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductInfoMapType;
import ths.dms.core.entities.enumtype.ProductInfoObjectType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.CatalogFilter;
import ths.dms.core.entities.vo.CatalogVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProductInfoVO;
import ths.dms.core.entities.vo.ProductInfoVOEx;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class ProductInfoDAOImpl implements ProductInfoDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public ProductInfo createProductInfo(ProductInfo productInfo,
			LogInfoVO logInfo) throws DataAccessException {
		if (productInfo == null) {
			throw new IllegalArgumentException("productInfo");
		}
		if (productInfo.getProductInfoCode() != null)
			productInfo.setProductInfoCode(productInfo.getProductInfoCode().toUpperCase());
		productInfo = repo.create(productInfo);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, productInfo,logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(ProductInfo.class, productInfo.getId());
	}

	@Override
	public void deleteProductInfo(ProductInfo productInfo, LogInfoVO logInfo)
			throws DataAccessException {
		if (productInfo == null) {
			throw new IllegalArgumentException("productInfo");
		}
		repo.delete(productInfo);
		try {
			actionGeneralLogDAO.createActionGeneralLog(productInfo, null,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public ProductInfo getProductInfoById(long id) throws DataAccessException {
		return repo.getEntityById(ProductInfo.class, id);
	}

	@Override
	public void updateProductInfo(ProductInfo productInfo, LogInfoVO logInfo)
			throws DataAccessException {
		if (productInfo == null) {
			throw new IllegalArgumentException("productInfo");
		}
		if (productInfo.getProductInfoCode() != null)
			productInfo.setProductInfoCode(productInfo.getProductInfoCode().toUpperCase());
		ProductInfo temp = this.getProductInfoById(productInfo.getId());
		repo.update(productInfo);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, productInfo,logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	
	@Override
	public ProductInfo getProductInfoByCode(String code, ProductType type, String catCode, Boolean isActiveTypeRunning)
			throws DataAccessException {
		if(ProductType.SUB_CAT.equals(type) || ProductType.SECOND_SUB_CAT.equals(type)) {
			ProductInfo cat = this.getProductInfoByWithNotSubCat(catCode, ProductType.CAT, true );
			if(cat == null) {
				throw new IllegalArgumentException("cat is not exists or not running");
			}
			if(ProductType.SUB_CAT.equals(type))  
				return this.getProductInfoWithSubCat(cat.getId(), code, type, ProductInfoMapType.SUB_CAT, isActiveTypeRunning);
			else 
				return this.getProductInfoWithSubCat(cat.getId(), code, type, ProductInfoMapType.SECOND_SUB_CAT, isActiveTypeRunning);
		}  else {
			return this.getProductInfoByWithNotSubCat(code, type, isActiveTypeRunning);
		}
	}
	
	@Override
	public ProductInfo getProductInfoByWithNotSubCat(String code, ProductType type, Boolean isActiveTypeRunning)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from PRODUCT_INFO where 1 =1");
		List<Object> params = new ArrayList<Object>();
		if (code != null) {
			sql.append("and product_info_code=?");
			params.add(code.toUpperCase());
		}
		sql.append(" and type=?");
		params.add(type.getValue());
		if(Boolean.TRUE.equals(isActiveTypeRunning)) {
			sql.append(" AND status  = ? ");
			params.add(ActiveType.RUNNING.getValue());
		} else {
			sql.append(" AND status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		ProductInfo pi = repo.getEntityBySQL(ProductInfo.class, sql.toString(), params);
		return pi;
	}
	
	public ProductInfo getProductInfoWithSubCat(Long catId, String code, ProductType productType, ProductInfoMapType productInfoMapType,  Boolean isActiveTypeRunning)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT pi.* ");
		sql.append(" FROM PRODUCT_INFO pi JOIN product_info_map pim ON pi.product_info_id = pim.to_object_id ");
		sql.append(" WHERE pi.type = ? ");
		params.add(productType.getValue());
		sql.append(" AND pim.from_object_id = ? ");
		params.add(catId);
		sql.append(" AND pi.product_info_code =? ");
		params.add(code.toUpperCase());
		sql.append(" AND pim.object_type =? ");
		params.add(productInfoMapType.getValue());
		if(Boolean.TRUE.equals(isActiveTypeRunning)) {
			sql.append(" AND pi.status  = ? ");
			params.add(ActiveType.RUNNING.getValue());
		} else {
			sql.append(" AND pi.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		ProductInfo pi = repo.getEntityBySQL(ProductInfo.class, sql.toString(), params);
		return pi;
	}
	
	public List<ProductInfo> getListProductInfoFromProduct(KPaging<ProductInfo> kPaging,String desc,
			ActiveType status, ProductType type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select distinct pi.* from PRODUCT p join PRODUCT_INFO pi on p.sub_cat_id=pi.product_info_id where p.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (!StringUtility.isNullOrEmpty(desc)) {
			sql.append(" and lower(description) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(desc.toLowerCase()));
		}
		if (status != null) {
			sql.append(" and pi.status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and pi.status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (type != null) {
			sql.append(" and pi.type=?");
			params.add(type.getValue());
		}
		sql.append(" order by product_info_code");
		if (kPaging == null)
			return repo.getListBySQL(ProductInfo.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ProductInfo.class,
					sql.toString(), params, kPaging);
	}
	@Override
	public List<ProductInfo> getListProductInfo(KPaging<ProductInfo> kPaging,
			String productInfoCode, String productInfoName, String description,
			ActiveType status, ProductType type, Boolean isExceptZCat,String sort,String order) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PRODUCT_INFO where 1 = 1");

		if (!StringUtility.isNullOrEmpty(productInfoCode)) {
			sql.append(" and product_info_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(productInfoCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(productInfoName)) {
			sql.append(" and lower(UNICODE2ENGLISH(product_info_name)) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(productInfoName.toLowerCase())));
		}
		if (!StringUtility.isNullOrEmpty(description)) {
			sql.append(" and lower(description) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(description.toLowerCase()));
		}
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (type != null) {
			sql.append(" and type=?");
			params.add(type.getValue());
		}
		if(isExceptZCat != null && isExceptZCat){
			sql.append(" AND NOT EXISTS");
			sql.append(" (SELECT 1");
			sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT'  and ap.status = 1");
			sql.append(" AND ap.ap_param_code  = PRODUCT_INFO.product_info_code");
			sql.append(" )");
		}
		//phuocdh2 modified
		sql.append(" order by product_info_code ");
		if (kPaging == null) {
			return repo.getListBySQL(ProductInfo.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(ProductInfo.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public List<ProductInfo> getListProductInfoForStock(KPaging<ProductInfo> kPaging,
			String productInfoCode, String productInfoName, String desc,
			ActiveType status, ProductType type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PRODUCT_INFO where product_info_code in ('A', 'B', 'C', 'D', 'E', 'F') ");

		if (!StringUtility.isNullOrEmpty(productInfoCode)) {
			sql.append(" and product_info_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(productInfoCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(productInfoName)) {
			sql.append(" and lower(product_info_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(productInfoName.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(desc)) {
			sql.append(" and lower(description) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(desc.toLowerCase()));
		}
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (type != null) {
			sql.append(" and type=?");
			params.add(type.getValue());
		}
		sql.append(" AND NOT EXISTS");
		sql.append(" (SELECT 1");
		sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT'  and ap.status = 1");
		sql.append(" AND ap.ap_param_code  = product_info_code");
		sql.append(" )");
		sql.append(" order by product_info_code");
		if (kPaging == null)
			return repo.getListBySQL(ProductInfo.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ProductInfo.class,
					sql.toString(), params, kPaging);
	}

	@Override
	public List<ProductInfo> getListSubCatNotInDisplayGroup(
			KPaging<ProductInfo> kPaging, Long displayGroupId,
			String subCatCode, String subCatName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PRODUCT_INFO pi where status = ? and type = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ProductType.SUB_CAT.getValue());

		//TODO: check again
//		if (displayGroupId != null) {
//			sql.append(" and product_info_id not in (select sub_cat_id from display_group_detail where display_group_id=? and status != ?)");
//			params.add(displayGroupId);
//			params.add(ActiveType.DELETED.getValue());
//		}

		if (!StringUtility.isNullOrEmpty(subCatCode)) {
			sql.append(" and product_info_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(subCatCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(subCatName)) {
			sql.append(" and upper(product_info_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(subCatName.toUpperCase()));
		}

		sql.append(" order by product_info_code");
		if (kPaging == null)
			return repo.getListBySQL(ProductInfo.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ProductInfo.class,
					sql.toString(), params, kPaging);
	}

	@Override
	public Boolean isUsingByOthers(long productInfoId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select count(1) as count from product_info  ");
		sql.append(" where exists (select 1 from product where (cat_id=? or packing_id=? or FLAVOUR_ID=? or SUB_CAT_ID=?) and status != ?) ");
		params.add(productInfoId);
		params.add(productInfoId);
		params.add(productInfoId);
		params.add(productInfoId);
		params.add(ActiveType.DELETED.getValue());
		sql.append("       or exists (select 1 from product_level where cat_id=? or sub_cat_id=? and status != ?) ");
		params.add(productInfoId);
		params.add(productInfoId);
		params.add(ActiveType.DELETED.getValue());
		sql.append("       or exists (select 1 from sale_level_cat where cat_id=? and status != ?) ");
		params.add(productInfoId);
		params.add(ActiveType.DELETED.getValue());
		sql.append("       or exists (select 1 from shop_product where cat_id=? and status != ?) ");
		params.add(productInfoId);
		params.add(ActiveType.DELETED.getValue());
		sql.append("       or exists (select 1 from staff_sale_cat where cat_id=? ) ");
		params.add(productInfoId);
		sql.append("       or exists (select 1 from product where (brand_id=? or cat_id=? or flavour_id=? or packing_id=? or sub_cat_id=?) and status != ?) ");
		params.add(productInfoId);
		params.add(productInfoId);
		params.add(productInfoId);
		params.add(productInfoId);
		params.add(productInfoId);
		params.add(ActiveType.DELETED.getValue());
		
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public List<ProductInfo> getListProductInfoOrderByName(ProductType type)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PRODUCT_INFO where 1 = 1");

		if (type != null) {
			sql.append(" and type = ?");
			params.add(type.getValue());
		}
		sql.append(" order by NLSSORT(PRODUCT_INFO_NAME,'NLS_SORT=vietnamese')");

		return repo.getListBySQL(ProductInfo.class, sql.toString(), params);
	}

	@Override
	public List<ProductInfoVO> getListProductInfoVO()
			throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql=new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT pi.PRODUCT_INFO_ID as idProductInfoVO, ");
		sql.append(" PI.PRODUCT_INFO_CODE as codeProductInfoVO ");
		sql.append(" FROM SALE_LEVEL_CAT sc,PRODUCT_INFO pi ");
		sql.append(" WHERE PI.PRODUCT_INFO_ID=SC.CAT_ID ");
		sql.append(" AND PI.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND SC.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" GROUP BY PI.PRODUCT_INFO_CODE,pi.PRODUCT_INFO_ID ");
		sql.append(" ORDER BY PI.PRODUCT_INFO_CODE ");
		 String[] fieldNames = { 
					"idProductInfoVO",//0
					"codeProductInfoVO"// 1
					};

		 Type[] fieldTypes = {
				 	StandardBasicTypes.LONG, // 0
					StandardBasicTypes.STRING // 1
			};
		
		return repo.getListByQueryAndScalar(ProductInfoVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public Boolean isEquimentCat(String catCode)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT count(1) as count ");
		sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT'  and ap.status = 1");
		sql.append(" AND upper(ap.ap_param_code)  = ?");
		params.add(catCode.toUpperCase());
		int res = repo.countBySQL(sql.toString(), params);
		return res==1?true:false;
	}
	
	/*****************************	Nganh hang con phu****************************************/
	/**
	 * Tim kiem danh sach nganh hang con phu
	 * @author thongnm
	 */
	@Override
	public List<ProductInfoVOEx> getListProductInfoOfSecondSubCat(KPaging<ProductInfoVOEx> kPaging, String catCode, 
			String subCatCode, String subCatName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT info.PRODUCT_INFO_ID productInfoId, info.PRODUCT_INFO_NAME productInfoName, info.PRODUCT_INFO_CODE productInfoCode, ");
		sql.append(" info.status status, info.description description, info.type type, ");
		sql.append(" (SELECT pi.product_info_code ");
		sql.append(" FROM product_info_map m JOIN product_info pi ON pi.product_info_id = m.FROM_OBJECT_ID ");
		sql.append(" WHERE m.object_type = 17 and m.status = 1 and M.TO_OBJECT_ID = info.PRODUCT_INFO_ID and rownum = 1 ");
		sql.append(" ) parentCode ");
		sql.append(" FROM product_info info ");
		sql.append(" WHERE 1 =1 ");
		sql.append(" AND info.type =7 ");
		sql.append(" AND info.status = 1 ");
		sql.append(" AND NOT EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and ap.status = 1 ");
		sql.append(" AND ap.ap_param_code = info.product_info_code ");
		sql.append(" ) ");
		
		if (!StringUtility.isNullOrEmpty(catCode)) {
			sql.append(" AND exists (SELECT 1 ");
			sql.append(" FROM product_info_map m ");
			sql.append(" JOIN product_info pi ");
			sql.append(" ON pi.product_info_id = m.FROM_OBJECT_ID ");
			sql.append(" WHERE m.object_type = 17 ");
			sql.append(" AND m.status = 1 ");
			sql.append(" AND M.TO_OBJECT_ID = info.PRODUCT_INFO_ID ");
			sql.append(" AND upper(pi.product_info_code) like ? ESCAPE '/'");
			sql.append(" AND rownum = 1) ");
			params.add(StringUtility.toOracleSearchLike(catCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(subCatCode)) {
			sql.append(" and lower(info.PRODUCT_INFO_CODE) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(subCatCode.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(subCatName)) {
			sql.append(" and lower(info.PRODUCT_INFO_NAME) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(subCatName.toLowerCase()));
		}
		sql.append(" order by info.PRODUCT_INFO_CODE asc");

		String[] fieldNames = {
				"productInfoId", 
				"productInfoName",
				"productInfoCode",
				"status", 
				"description",
				"type",
				"parentCode"
				};
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING,StandardBasicTypes.INTEGER, StandardBasicTypes.STRING
		};	
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from("); 
		countSql.append(sql.toString());
		countSql.append(")");
		if (kPaging == null){
			 return repo.getListByQueryAndScalar(ProductInfoVOEx.class, fieldNames,fieldTypes, sql.toString(), params);
		}else{
			return repo.getListByQueryAndScalarPaginated(ProductInfoVOEx.class, fieldNames, fieldTypes,sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}

	@Override
	public List<ProductInfo> getListSubCat(ActiveType status, ProductType type,
			Long from_object) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from PRODUCT_INFO where 1=1");

		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		} 
		if (type != null) {
			sql.append(" and type=?");
			params.add(type.getValue());
		}
		sql.append(" and PRODUCT_INFO_ID in (select to_object_id from PRODUCT_INFO_MAP where object_type=12 ");
		if(from_object != null && from_object > 0){
			sql.append(" and from_object_id=? ");
			params.add(from_object);
		}
		/*else {
			sql.append(" and from_object_id=-1 "); // khong lay ra sub_cat nao het
		}*/
		sql.append(" ) ");
		sql.append(" order by product_info_code");		
		return repo.getListBySQL(ProductInfo.class, sql.toString(), params);		
	}
	
	@Override
	public List<ProductInfo> getListSubCatByListCat(ActiveType status, ProductType type, List<Long> lstObjectId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from PRODUCT_INFO where 1=1");

		if (status != null) {
			sql.append(" and status = ?");
			params.add(status.getValue());
		}
		if (type != null) {
			sql.append(" and type = ?");
			params.add(type.getValue());
		}
		sql.append(" and PRODUCT_INFO_ID in (select to_object_id from PRODUCT_INFO_MAP where object_type = 12 ");
		if(lstObjectId != null && lstObjectId.size() > 0){
			sql.append(" and from_object_id IN (-1 ");
			for (Long objectId : lstObjectId) {
				sql.append(", ?");
				params.add(objectId);
			}
			sql.append(" ) ");
		}
		sql.append(" ) ");
		sql.append(" order by product_info_code");		
		return repo.getListBySQL(ProductInfo.class, sql.toString(), params);		
	}

	@Override
	public List<ProductInfo> getListSubCat(ActiveType status, ProductType type, ProductInfoMapType mapType,
			Long from_object) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from PRODUCT_INFO where 1=1");

		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		} 
		if (type != null) {
			sql.append(" and type=?");
			params.add(type.getValue());
		}
		sql.append(" and PRODUCT_INFO_ID in (select to_object_id from PRODUCT_INFO_MAP where object_type=? ");
		params.add(mapType.getValue());
		if(from_object != null){
			sql.append(" and from_object_id=?");
			params.add(from_object);
		}		
		sql.append(" ) ");
		sql.append(" order by product_info_code");		
		return repo.getListBySQL(ProductInfo.class, sql.toString(), params);		
	}
	
	@Override
	public List<ProductInfo> getListProductInfoByFilter(BasicFilter<ProductInfo> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_info where 1 = 1 ");
		if (filter.getType() != null) {
			sql.append(" and type = ? ");
			params.add(filter.getType());
		}
		if (!StringUtility.isNullOrEmpty(filter.getTextG())){
			sql.append(" and lower(product_info_code) not in (?) ");
			params.add(filter.getTextG().trim().toLowerCase());
		}
		
		if (filter.getkPaging() == null)
			return repo.getListBySQL(ProductInfo.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ProductInfo.class, sql.toString(), params, filter.getkPaging());
	}
	
	@Override
	public List<ProductInfo> getListProductInfoStock(KPaging<ProductInfo> kPaging, String productInfoCode, String productInfoName, String description, ActiveType status, ProductType type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PRODUCT_INFO where 1 = 1");

		if (!StringUtility.isNullOrEmpty(productInfoCode)) {
			sql.append(" and product_info_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(productInfoCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(productInfoName)) {
			sql.append(" and lower(UNICODE2ENGLISH(product_info_name)) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(productInfoName.toLowerCase())));
		}
		if (!StringUtility.isNullOrEmpty(description)) {
			sql.append(" and lower(description) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(description.toLowerCase()));
		}
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (type != null) {
			sql.append(" and type=?");
			params.add(type.getValue());
		}
		sql.append(" order by product_info_code");
		if (kPaging == null)
			return repo.getListBySQL(ProductInfo.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ProductInfo.class, sql.toString(), params, kPaging);
	}

	/**
	 * @author vuongmq
	 * @date 15/01/2015	
	 */
	@Override
	public List<ProductInfo> getListProductInfoEx(KPaging<ProductInfo> kPaging,
			String productInfoCode, String productInfoName, ActiveType status,
			ProductType type, List<ProductInfoObjectType> objTypes,
			boolean orderByCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PRODUCT_INFO where 1 = 1");

		if (!StringUtility.isNullOrEmpty(productInfoCode)) {
			sql.append(" and upper(product_info_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(productInfoCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(productInfoName)) {
			sql.append(" and upper(product_info_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(productInfoName.toUpperCase()));
		}
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (type != null) {
			sql.append(" and type=?");
			params.add(type.getValue());
			/*if (type == ProductType.CAT && objTypes != null && objTypes.size() > 0) {
				sql.append(" and object_type in(?");
				params.add(objTypes.get(0).getValue());
				for (int i = 1; i < objTypes.size(); i++) {
					sql.append(", ?");
					params.add(objTypes.get(i).getValue());
				}
				sql.append(")");
			}*/
		}
		
		if (orderByCode) {
			sql.append(" order by product_info_code");
		} else {
			sql.append(" order by product_info_name");
		}
		if (kPaging == null) {
			return repo.getListBySQL(ProductInfo.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(ProductInfo.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<ProductInfo> getListProductInfoTypeStock(KPaging<ProductInfo> kPaging, ActiveType producttype) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_info ");
		sql.append(" where STATUS = 1 ");	
		if (producttype != null) {
			sql.append(" and type = ?");
			params.add(producttype.getValue());
		}
		sql.append(" order by PRODUCT_INFO_CODE");
		if (kPaging == null)
			return repo.getListBySQL(ProductInfo.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ProductInfo.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<ApParam> getListShopProductInfoType(Object object, ActiveType producttype) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from ap_param where ");
		sql.append(" type ='STAFF_SALE_TYPE' ");
		sql.append(" and status = 1 ");
		sql.append(" order by ap_param_code ");
		return repo.getListBySQL(ApParam.class, sql.toString(), params);
	}

	@Override
	public ProductInfo getProductInfoByCodeCat(String code, ProductType type, Boolean isActiveTypeRunning) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with res as ");
		sql.append(" ( ");
		sql.append(" SELECT pim.FROM_OBJECT_ID ");
		sql.append(" FROM PRODUCT_INFO pi ");
		sql.append(" JOIN product_info_map pim ");
		sql.append(" ON pi.product_info_id = pim.to_object_id ");
		sql.append(" WHERE pi.type in (1,2) ");
		if (code != null) {
			sql.append(" AND pi.product_info_code = ? ");
			params.add(code.toUpperCase());
		}
		sql.append(" AND pim.object_type = 12 ");
		sql.append(" AND pi.status = 1 ");
		sql.append(" ) ");
		sql.append(" SELECT distinct pi.* ");
		sql.append(" FROM PRODUCT_INFO pi ");
		sql.append(" join res r on r.FROM_OBJECT_ID = pi.product_info_id ");
		sql.append(" WHERE pi.status = 1 ");
		sql.append(" AND type = 1 ");
		ProductInfo pi = repo.getEntityBySQL(ProductInfo.class, sql.toString(), params);
		return pi;
	}
	
	@Override
	public List<ProductInfo> getListSubCatByCatId(Long catId, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * ");
		sql.append(" FROM product_info ");
		sql.append(" WHERE product_info_id IN ");
		sql.append(" ( SELECT to_object_id  ");
		sql.append(" FROM product_info_map  ");
		sql.append(" WHERE FROM_OBJECT_ID = ? ");
		params.add(catId);
		sql.append(" ) ");
		if (status != null) {
			sql.append(" and status = ? ");
			params.add(status.getValue());
		} else {
			sql.append(" and status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		return repo.getListBySQL(ProductInfo.class, sql.toString(), params);
	}
	
	@Override
	public List<CatalogVO> getListCatalogByFilter(CatalogFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder query = new StringBuilder();
		StringBuilder sqlCount = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		if (ApParamType.UOM.getValue().equals(filter.getType()) || ApParamType.NGAY_THANG_NAM.getValue().equals(filter.getType())
				|| ApParamType.FEEDBACK_TYPE.getValue().equals(filter.getType()) || ApParamType.CUSTOMER_SALE_POSITION.getValue().equals(filter.getType())
				|| ApParamType.STAFF_SALE_TYPE.getValue().equals(filter.getType()) || ApParamType.FOCUS_PRODUCT_TYPE.getValue().equals(filter.getType())
				|| ApParamType.PO_REASON_INVOICE.getValue().equals(filter.getType()) || ApParamType.ORDER_PIRITY.getValue().equals(filter.getType())) {
			sql.append(" select ap_param_id id ");
			sql.append(" , ap_param_code code ");
			sql.append(" , ap_param_name name ");
			sql.append(" , description description ");
			sql.append(" , type ");
			sql.append(" , null staffTypeId ");
			sql.append(" , null value ");
			sql.append(" , null parentId ");
			sql.append(" , null parentName ");
			sql.append(" from ap_param ");
			sql.append(" where type = ? ");
			params.add(filter.getType());
			sql.append(" and status = ? ");
			params.add(ActiveType.RUNNING.getValue());

			if (!StringUtility.isNullOrEmpty(filter.getText())) {
				sql.append(" and ( ");
				sql.append(" upper(ap_param_code)like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getText().toUpperCase()));
				sql.append(" or upper(ap_param_name)like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getText().toUpperCase()));
				sql.append(" ) ");
			}
			query.append(sql);
			query.append(" order by ap_param_code ");
		} else if (ApParamType.STAFF_TYPE_SHOW_SUP.getValue().equals(filter.getType())) {
			sql.append(" select ap.ap_param_id id ");
			sql.append(" , ap.ap_param_code code ");
			sql.append(" , ap.ap_param_name name ");
			sql.append(" , ap.description description ");
			sql.append(" , ap.type ");
			sql.append(" , ap.value staffTypeId ");
			sql.append(" , stt.name value ");
			sql.append(" , null parentId ");
			sql.append(" , null parentName ");
			sql.append(" from ap_param ap ");
			sql.append(" join staff_type stt on ap.value = stt.staff_type_id ");
			sql.append(" where ap.type = ? ");
			params.add(filter.getType());
			sql.append(" and ap.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" and stt.status = ? ");
			params.add(ActiveType.RUNNING.getValue());

			if (!StringUtility.isNullOrEmpty(filter.getText())) {
				sql.append(" and ( ");
				sql.append(" upper(ap_param_code)like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getText().toUpperCase()));
				sql.append(" or upper(ap_param_name)like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getText().toUpperCase()));
				sql.append(" ) ");
			}
			query.append(sql);
			query.append(" order by ap_param_code ");
		} else if (ProductType.CAT.getValue().toString().equals(filter.getType()) || ProductType.BRAND.getValue().toString().equals(filter.getType()) 
				|| ProductType.FLAVOUR.getValue().toString().equals(filter.getType()) || ProductType.PACKING.getValue().toString().equals(filter.getType())) {
			sql.append(" select product_info_id id ");
			sql.append(" , product_info_code code ");
			sql.append(" , product_info_name name ");
			sql.append(" , description description ");
			sql.append(" , type ");
			sql.append(" , null staffTypeId ");
			sql.append(" , null value ");
			sql.append(" , null parentId ");
			sql.append(" , null parentName ");
			sql.append(" from product_info ");
			sql.append(" where type = ? ");
			params.add(filter.getType());
			sql.append(" and status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			
			if (!StringUtility.isNullOrEmpty(filter.getText())) {
				sql.append(" and ( ");
				sql.append(" upper(product_info_code)like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getText().toUpperCase()));
				sql.append(" or upper(product_info_name)like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getText().toUpperCase()));
				sql.append(" ) ");
			}
			query.append(sql);
			query.append(" order by product_info_code ");
		} else if (ProductType.SUB_CAT.getValue().toString().equals(filter.getType())) {
			sql.append(" Select picon.Product_Info_Id id ");
			sql.append(" , picon.Product_Info_Code code ");
			sql.append(" , picon.Product_Info_Name name ");
			sql.append(" , picon.Description description ");
			sql.append(" , picon.type");
			sql.append(" , null staffTypeId ");
			sql.append(" , null value ");
			sql.append(" , picha.Product_Info_Id parentId ");
			sql.append(" , picha.Product_Info_Name parentName ");
			sql.append(" from ( ");
			sql.append(" Select pi.Product_Info_Id idCon, ");
			sql.append(" (Select pim.From_Object_Id From Product_Info_Map pim Where pim.To_Object_Id = pi.Product_Info_Id) idCha ");
			sql.append(" From Product_Info pi ");
			sql.append(" Where pi.Type = ? ");
			params.add(filter.getType());
			sql.append(" and pi.Status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" ) t ");
			sql.append(" Left Join Product_Info picon On picon.Product_Info_Id = t.idcon ");
			sql.append(" LEFT Join Product_Info picha On picha.PRODUCT_INFO_ID = t.idCha ");

			if (!StringUtility.isNullOrEmpty(filter.getText())) {
				sql.append(" where upper(picon.product_info_code)like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getText().toUpperCase()));
				sql.append(" or upper(picon.product_info_name)like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getText().toUpperCase()));
			}
			query.append(sql);
			query.append(" order by picon.product_info_code ");
		} 

		sqlCount.append(" select count(*) count from( ");
		sqlCount.append(sql);
		sqlCount.append(" )");

		String[] fieldNames = new String[] { "id", "code", "name", "description", "parentId", "parentName", "type", "staffTypeId", "value"};
		Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING
			};
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(CatalogVO.class, fieldNames, fieldTypes, query.toString(), sqlCount.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(CatalogVO.class, fieldNames, fieldTypes, query.toString(), params);
		}
	}
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.PoAutoGroup;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoAutoGroupFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupVO;
import ths.dms.core.entities.vo.ProductInfoVOEx;
import ths.dms.core.exceptions.DataAccessException;

public interface PoAutoGroupDAO {

	PoAutoGroup createPoAutoGroup(PoAutoGroup poAutoGroup) throws DataAccessException;

	void deletePoAutoGroup(PoAutoGroup poAutoGroup) throws DataAccessException;

	void updatePoAutoGroup(PoAutoGroup poAutoGroup) throws DataAccessException;

	PoAutoGroup getPoAutoGroupById(Long id) throws DataAccessException;
	
	List<PoAutoGroupVO> getPoAutoGroupVO(
			PoAutoGroupFilter filter, KPaging<PoAutoGroupVO> kPaging)
			throws DataAccessException;
	
	List<ProductInfoVOEx> getListProductInfoCanBeAdded(
			int poAutoGroupId, String name, String code, boolean isSubCat, KPaging<ProductInfoVOEx> kPaging)
			throws DataAccessException;

	List<Product> getListProductCanBeAdded(
			int poAutoGroupId, String name, String code, KPaging<Product> kPaging)
			throws DataAccessException;

	Boolean isExistsInfoOfPoAutoGroup(PoAutoGroup poAutoGroup)
			throws DataAccessException;

	PoAutoGroup getPoAutoGroupByCode(String groupCode)
			throws DataAccessException;

	List<PoAutoGroupVO> getPoAutoGroupVOEx(PoAutoGroupFilter filter,
			KPaging<PoAutoGroupVO> kPaging) throws DataAccessException;

	List<ProductInfoVOEx> getListProductInfoCanBeAddedNPP(int poAutoGroupId,
			String name, String code, boolean isSubCat,
			KPaging<ProductInfoVOEx> kPaging) throws DataAccessException;
}

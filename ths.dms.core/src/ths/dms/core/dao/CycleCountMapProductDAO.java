package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.CycleCountMapProduct;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.MapProductFilter;
import ths.dms.core.entities.vo.CycleCountDiffVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.MapProductVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.core.entities.vo.rpt.RptCountStoreVO;
import ths.dms.core.exceptions.DataAccessException;

public interface CycleCountMapProductDAO {

	void deleteCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct) throws DataAccessException;
	
	CycleCountMapProduct getCycleCountMapProductById(long id) throws DataAccessException;

	List<CycleCountMapProduct> getListCycleCountMapProductByCycleCountId(
			KPaging<CycleCountMapProduct> kPaging, long cycleCountId)
			throws DataAccessException;

	Boolean checkIfRecordExist(long cycleCountId, String productCode, Long id)
			throws DataAccessException;

	List<CycleCountMapProduct> getListCycleCountMapProduct(
			KPaging<CycleCountMapProduct> kPaging, Long cycleCountId,
			String productCode, String productName, Long catId, Long subCatId,
			Integer minQuantity, Integer maxQuantity,
			Boolean isOnlyGetDifferentQuantity) throws DataAccessException;

	Integer getMaxStockCardNumber(Long cycleCountId) throws DataAccessException;

	/**
	 * @author thanhnguyen
	 * @param cycleCountCode
	 * @param shopCode
	 * @return
	 */
	List<RptCountStoreVO> getListReportCountStore(String cycleCountCode,
			String shopCode) throws DataAccessException;

	/**
	 * @author hungnm
	 * @param cycleCountId
	 * @return
	 * @throws DataAccessException
	 */
	Boolean checkIfAnyProductCounted(Long cycleCountId)
			throws DataAccessException;

	/**
	 * @author hungnm
	 * @param id
	 * @return
	 * @throws DataAccessException
	 */
	CycleCountMapProduct getCycleCountMapProductByIdForUpdate(long id)
			throws DataAccessException;

	void updateCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct,
			Boolean isUpdateQuantityBfrCoun, Boolean isNewProduct) throws DataAccessException;

	Integer getTotalStockCardNumber(Long cycleCountId)
			throws DataAccessException;

	void updateListCycleCountMapProduct(List<CycleCountMapProduct> lstCycleCountMapProduct, LogInfoVO log)throws DataAccessException;

	/**
	 * @author hungnm
	 * @param cycleCountId
	 * @param productId
	 * @return
	 * @throws DataAccessException
	 */
	CycleCountMapProduct getCycleCountMapProduct(Long cycleCountId,
			Long productId) throws DataAccessException;

	Boolean createListCycleCountMapProduct(CycleCount cycleCount,List<CycleCountMapProduct> lstCycleCountMapProduct,
			Boolean isCreatingAll, LogInfoVO log) throws DataAccessException;

	CycleCountMapProduct createCycleCountMapProduct(CycleCountMapProduct cycleCountMapProduct,Boolean isUpdateQuantityBfrCount, LogInfoVO logInfo)
			throws DataAccessException;

	
	/**
	 * Lay ds chenh lech kho va thuc te cua kiem ke
	 * 
	 * @author lacnv1
	 * @since Nov 04, 2014
	 */
	List<CycleCountDiffVO> getListCycleCountDiff(long cycleCountId) throws DataAccessException;

	/**
	 * Danh sach san pham bo sung nhap kiem ke
	 * @author hunglm16
	 * @param kPaging
	 * @param status
	 * @param cycleCountId
	 * @param lstProductId
	 * @param lstProductIdExcep
	 * @param productCode
	 * @param productName
	 * @return
	 * @throws DataAccessException
	 * @since 10/11/2015
	 */
	List<ProductVO> searchProductNotInCycleProductMap(KPaging<ProductVO> kPaging, Integer status, Long cycleCountId, List<Long> lstProductId, List<Long> lstProductIdExcep, String productCode, String productName) throws DataAccessException;

	/**
	 * Thong tin the kiem kho
	 * @author hunglm16
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @since 11/11/2015
	 */
	List<MapProductVO> searchCycleCountMapProductByFilter(MapProductFilter<MapProductVO> filter) throws DataAccessException;

	/**
	 * Lay the kiem kho va bo sung them san pham
	 * @author hunglm16
	 * @param filter
	 * @param lstProductId
	 * @return Danh sach the kiem kho tam
	 * @throws DataAccessException
	 * @since 12/11/2015
	 */
	List<MapProductVO> getCycleCountMapProductByProductInsertTemp(MapProductFilter<MapProductVO> filter, List<Long> lstProductId) throws DataAccessException;

}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.EquipStockAdjustForm;
import ths.dms.core.entities.EquipStockAdjustFormDtl;
import ths.dms.core.entities.filter.EquipStockAdjustFormFilter;
import ths.dms.core.entities.vo.EquipStockAdjustFormDtlVO;
import ths.dms.core.entities.vo.EquipStockAdjustFormVO;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Description: class DAO quan ly nhap kho dieu chinh
 * @author Datpv4
 * @since July, 05,2015
 * */
public interface EquipStockAdjustFormDAO {
	/**
     * @author Datpv4
     * @date July, 02 2015
     * @description get list EquipStockAdjustForm
     */
	List<EquipStockAdjustFormVO> getListEquipStockAdjustForm(EquipStockAdjustFormFilter filter) throws DataAccessException;
	/**
     * @author Datpv4
     * @date July, 02 2015
     * @description get list EquipStockAdjustFormDtl by Filter
     */
	List<EquipStockAdjustFormDtlVO> getEquipStockAdjustFormDtlFilter(EquipStockAdjustFormFilter filter) throws DataAccessException;
	
	/**
     * @author Datpv4
     * @date July, 02 2015
     * @description get list get EquipStockAdjustForm by Id
     */
	EquipStockAdjustForm getEquipStockAdjustFormByID(Long equipmentRepairPayFormId) throws DataAccessException;
	
	
	/**
     * @author Datpv4
     * @date July, 02 2015
     * @description get list EquipStockAdjustFormDetail by id
     */
	List<EquipStockAdjustFormDtl> getListEquipStockAdjustFormDetailById(Long id) throws DataAccessException;
	
	/**
     * @author Datpv4
     * @date July, 02 2015
     * @description get list EquipStockAdjustForm by lisId
     */
	List<EquipStockAdjustForm>  getListEquipStockAdjustFormByListId(List<Long> lstId)  throws DataAccessException;

	EquipStockAdjustFormDtl getEquipStockAdjustFormDetailByID(Long equipStockAdjustFormDetailId) throws DataAccessException;
}

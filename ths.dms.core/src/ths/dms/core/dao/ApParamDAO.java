package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.vo.ApParamVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface ApParamDAO {

	ApParam createApParam(ApParam apParam, LogInfoVO logInfo) throws DataAccessException;

	void deleteApParam(ApParam apParam, LogInfoVO logInfo) throws DataAccessException;

	void updateApParam(ApParam apParam, LogInfoVO logInfo) throws DataAccessException;
	
	ApParam getApParamById(long id) throws DataAccessException;

	ApParam getApParamByCode(String code, ApParamType type) throws DataAccessException;
	
	ApParam getApParamByCodeX(String code, ApParamType type, ActiveType status) throws DataAccessException;

	List<ApParam> getListApParamByType(ApParamType type, ActiveType status)
			throws DataAccessException;

	ApParam getApParamByCode(String code, ActiveType status)
			throws DataAccessException;

	ApParam getApParamByCondition(String apName, String apCode,
			ApParamType type, ActiveType status) throws DataAccessException;
	
	/**
	 * Lay danh sach ApParam theo dieu kien loc ApParamFilter
	 * @author tulv2
	 * @since 09.17.2014
	 * */
	List<ApParam> getListApParamByFilter(ApParamFilter filter)  throws DataAccessException;
	
	/**
	 * Lay danh sach ApParam (cho man hinh quan ly ap_param)
	 * 
	 * @author lacnv1
	 * @since Oct 13, 2014
	 * */
	List<ApParamVO> getListApParamByFilter2(ApParamFilter filter)  throws DataAccessException;
	
	/**
	 * Tao moi ap_param (truong hop type khong co dinh nghia trong enum)
	 * 
	 * @author lacnv1
	 * @since Oct 13, 2014
	 */
	ApParam createApParamEx(ApParam apParam, LogInfoVO logInfo) throws DataAccessException;
	
	/**
	 * Cap nhat ap_param (truong hop type khong co dinh nghia trong enum)
	 * 
	 * @author lacnv1
	 * @since Oct 13, 2014
	 */
	ApParam updateApParamEx(ApParam apParam, LogInfoVO logInfo) throws DataAccessException;
	
	/**
	 * @author vuongmq, cpy habeco
	 * @date 15/01/2015
	 * @description danh sach uom
	 */
	List<ApParam> getListUoms(ApParamType type) throws DataAccessException;

	List<ApParam> getListActiveType(ApParamType type, ActiveType... activeTypes)	throws DataAccessException;
}

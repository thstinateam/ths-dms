package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SalePlanVersion;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class SalePlanVersionDAOImpl implements SalePlanVersionDAO {

	@Autowired
	private IRepository repo;

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SalePlanDAO#createSalePlan(ths.dms.core.entities.SalePlan)
	 */
	@Override
	public SalePlanVersion createSalePlanVersion(SalePlanVersion salePlanVersion)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (salePlanVersion == null) {
			throw new IllegalArgumentException("salePlan");
		}
		repo.create(salePlanVersion);
		return repo.getEntityById(SalePlanVersion.class, salePlanVersion.getId());
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SalePlanDAO#createSalePlan(java.util.List)
	 */
	@Override
	public void createSalePlanVersion(List<SalePlanVersion> listSalePlanVersion)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (listSalePlanVersion == null) {
			return;
		}
		repo.create(listSalePlanVersion);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SalePlanDAO#deleteSalePlan(ths.dms.core.entities.SalePlan)
	 */
	@Override
	public void deleteSalePlanVersion(SalePlanVersion salePlanVersion) throws DataAccessException {
		// TODO Auto-generated method stub
		if (salePlanVersion == null) {
			throw new IllegalArgumentException("salePlanVersion");
		}
		repo.delete(salePlanVersion);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SalePlanDAO#updateSalePlan(ths.dms.core.entities.SalePlan)
	 */
	@Override
	public void updateSalePlanVersion(SalePlanVersion salePlanVersion) throws DataAccessException {
		// TODO Auto-generated method stub
		if (salePlanVersion == null) {
			throw new IllegalArgumentException("salePlanVersion");
		}
		repo.update(salePlanVersion);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SalePlanDAO#getSalePlanById(long)
	 */
	@Override
	public SalePlanVersion getSalePlanVersionById(long id) throws DataAccessException {
		// TODO Auto-generated method stub
		return repo.getEntityById(SalePlanVersion.class, id);
	}
	
	@Override
	public Integer getMaxVersionBySalePlanSersionId(long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select max(VERSION) from SALE_PLAN_VERSION where 1 = 1");
		sql.append(" and SALE_PLAN_ID = ?");
		params.add(id);
		BigDecimal value = (BigDecimal) repo.getObjectByQuery(sql.toString(), params);
		return value == null ? 0 : value.intValue() ;
	}

}

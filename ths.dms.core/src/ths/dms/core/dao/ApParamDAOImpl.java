package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.vo.ApParamVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class ApParamDAOImpl implements ApParamDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public ApParam createApParam(ApParam apParam, LogInfoVO logInfo) throws DataAccessException {
		if (apParam == null) {
			throw new IllegalArgumentException("apParam");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (apParam.getApParamCode() != null)
			apParam.setApParamCode(apParam.getApParamCode().toUpperCase());
		repo.create(apParam);
		apParam = repo.getEntityById(ApParam.class, apParam.getId());
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, apParam, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return apParam;
	}

	@Override
	public void deleteApParam(ApParam apParam, LogInfoVO logInfo) throws DataAccessException {
		if (apParam == null) {
			throw new IllegalArgumentException("apParam");
		}
		repo.delete(apParam);
		try {
			actionGeneralLogDAO.createActionGeneralLog(apParam, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public ApParam getApParamById(long id) throws DataAccessException {
		return repo.getEntityById(ApParam.class, id);
	}

	@Override
	public void updateApParam(ApParam apParam, LogInfoVO logInfo) throws DataAccessException {
		if (apParam == null) {
			throw new IllegalArgumentException("apParam");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (apParam.getApParamCode() != null)
			apParam.setApParamCode(apParam.getApParamCode().toUpperCase());
		ApParam temp = this.getApParamById(apParam.getId());
		repo.update(apParam);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, apParam, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public List<ApParam> getListApParamByType(ApParamType type, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from ap_param where 1=1 ");
		if (type != null) {
			sql.append(" and type = ? ");
			params.add(type.getValue());
		}
		if (status != null) {
			sql.append(" and status = ? ");
			params.add(status.getValue());
		}
		if (ApParamType.NGAY_THANG_NAM.equals(type)) {
			sql.append(" order by value ");
		} else if (ApParamType.PRODUCT_SUBCAT_MAP.equals(type)) {
			sql.append(" order by ap_param_id ");
		} else if (ApParamType.DMS_CORE_ATTR.equals(type)) {
			sql.append(" order by ap_param_name ");
		} else if (ApParamType.FOCUS_PRODUCT_TYPE.equals(type)) {
			sql.append(" order by lpad(value, 10) desc");
		} else {
			sql.append(" order by ap_param_code  ");
		}
		return repo.getListBySQL(ApParam.class, sql.toString(), params);
	}

	@Override
	public ApParam getApParamByCode(String code, ApParamType type)
			throws DataAccessException {
		code = code.toUpperCase();
		StringBuilder sql = new StringBuilder();
		sql.append("select * from ap_param where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and ap_param_code=? ");
			params.add(code.toUpperCase());
		}
		if (type != null) {
			sql.append(" and type=? ");
			params.add(type.getValue());
		}
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(ApParam.class, sql.toString(), params);
	}
	
	@Override
	public ApParam getApParamByCodeX(String code, ApParamType type, ActiveType status) throws DataAccessException {
		code = code.toUpperCase();
		StringBuilder sql = new StringBuilder();
		sql.append("select * from ap_param where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and ap_param_code=? ");
			params.add(code.toUpperCase());
		}
		if (type != null) {
			sql.append(" and type=? ");
			params.add(type.getValue());
		}
		if (status != null) {
			sql.append(" and status = ?");
			params.add(status.getValue());
		} else {
			sql.append(" and status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		return repo.getEntityBySQL(ApParam.class, sql.toString(), params);
	}

	@Override
	public ApParam getApParamByCode(String code, ActiveType status)
			throws DataAccessException {
		code = code.toUpperCase();
		StringBuilder sql = new StringBuilder();

		sql.append("select * from AP_PARAM where 1=1");
		List<Object> params = new ArrayList<Object>();

		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and AP_PARAM_CODE = ?");
			params.add(code.toUpperCase());
		}

		if (status != null) {
			sql.append(" and STATUS = ?");
			params.add(status.getValue());
		}
		
		return repo.getEntityBySQL(ApParam.class, sql.toString(), params);
	}
	
	@Override
	public ApParam getApParamByCondition(String apName, String apCode,
			ApParamType type, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();

		sql.append("select * from AP_PARAM where 1=1");
		List<Object> params = new ArrayList<Object>();

		if (!StringUtility.isNullOrEmpty(apName)) {
			sql.append(" and AP_PARAM_NAME like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(apName));
		}
		
		if (!StringUtility.isNullOrEmpty(apCode)) {
			sql.append(" and AP_PARAM_CODE = ?");
			params.add(apCode.toUpperCase());
		}
		
		if (type != null) {
			sql.append(" and TYPE = ?");
			params.add(type.getValue());
		}

		if (status != null) {
			sql.append(" and STATUS = ?");
			params.add(status.getValue());
		} else{
			sql.append("and STATUS != ?");
			params.add(ActiveType.DELETED.getValue());
		}

		return repo.getEntityBySQL(ApParam.class, sql.toString(), params);
	}
	
	@Override
	public List<ApParam> getListApParamByFilter(ApParamFilter filter)  throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from ap_param where 1=1");

		if (!StringUtility.isNullOrEmpty(filter.getApParamCode())) {
			sql.append(" and upper(ap_param_code) = ?");
			params.add(filter.getApParamCode().toUpperCase());
		}
		
		if (filter.getLstApCode() != null && filter.getLstApCode().size() > 0) {
			sql.append(" and ap_param_code IN ( ");
			int n = filter.getLstApCode().size();
			for (int i = 0; i < n; i++){
				if (i < n - 1){
					sql.append(" ?, ");
					params.add(filter.getLstApCode().get(i).toUpperCase());
				} else {
					sql.append(" ? ");
					params.add(filter.getLstApCode().get(i).toUpperCase());
				}
			}
			sql.append(" ) ");
		}

		if (!StringUtility.isNullOrEmpty(filter.getDescription())) {
			sql.append(" and upper(description) = ?");
			params.add(filter.getDescription().toUpperCase());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getTypeStr())) {
			sql.append(" and upper(type) = ?");
			params.add(filter.getTypeStr().toUpperCase());
		}

		if (null != filter.getStatus()) {
			sql.append(" and status = ?");
			params.add(filter.getStatus().getValue());
		}
		
		if (null != filter.getType()) {
			sql.append(" and type = ?");
			params.add(filter.getType().getValue());
		}
		
		if(null != filter.getValue()){
			sql.append(" and upper(value) =  ?");
			params.add(filter.getValue().toUpperCase());
		}
		
		sql.append(" order by ap_param_code ");
		if(filter.getkPaging()!=null){
			return repo.getListBySQLPaginated(ApParam.class, sql.toString(), params,filter.getkPaging());
		}else{
			return repo.getListBySQL(ApParam.class, sql.toString(), params);
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<ApParamVO> getListApParamByFilter2(ApParamFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select ap.ap_param_id as id, ap.ap_param_code as apParamCode,");
		sql.append(" ap.ap_param_name as apParamName, ap.description,");
		sql.append(" ap.value, ap.type, ap.status, ");
		sql.append(" (select ap_param_name from ap_param where type ='STATUS_TYPE' and value = ap.status) as statusStr ");
		fromSql.append(" from ap_param ap where 1=1");

		String s = null;
		if (!StringUtility.isNullOrEmpty(filter.getApParamCode())) {
			fromSql.append(" and upper(ap.ap_param_code) like ? escape '/'");
			s = filter.getApParamCode().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getApParamName())) {
			fromSql.append(" and unicode2english(lower(ap.ap_param_name)) like ? escape '/'");
			s = filter.getApParamName().toLowerCase();
			s = Unicode2English.codau2khongdau(s);
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}

		if (!StringUtility.isNullOrEmpty(filter.getDescription())) {
			fromSql.append(" and unicode2english(lower(ap.description)) like ? escape '/'");
			s = filter.getDescription().toLowerCase();
			s = Unicode2English.codau2khongdau(s);
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getTypeStr())) {
			fromSql.append(" and upper(ap.type) like ? escape '/'");
			s = filter.getTypeStr().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}

		if (null != filter.getStatus()) {
			fromSql.append(" and ap.status = ?");
			params.add(filter.getStatus().getValue());
		} else {
			fromSql.append(" and ap.status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		if (null != filter.getType()) {
			fromSql.append(" and ap.type = ?");
			params.add(filter.getType().getValue());
		}
		
		if(null != filter.getValue()){
			fromSql.append(" and upper(ap.value) like  ? escape '/'");
			s = filter.getValue().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		sql.append(fromSql.toString());
		
		String[] fieldNames = new String[] {
				"id",
				"apParamCode", "apParamName",
				"description", "value", "type",
				"status", "statusStr"
		};
		
		String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			sql.append(" order by ").append(sort).append(" ");
			String order = StringUtility.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				sql.append(order);
			}
		} else {
			sql.append(" order by ap.ap_param_code, ap.ap_param_name");
		}
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING
		};
		
		if (filter.getPagingVO() == null) {
			fromSql = null;
			List<ApParamVO> lst = repo.getListByQueryAndScalar(ApParamVO.class, fieldNames, fieldTypes, sql.toString(), params);
			return lst;
		}
		StringBuilder countSql = new StringBuilder("select count(1) as count");
		countSql.append(fromSql.toString());
		fromSql = null;
		List<ApParamVO> lst = repo.getListByQueryAndScalarPaginated(ApParamVO.class, fieldNames, fieldTypes,
				sql.toString(), countSql.toString(), params, params, filter.getPagingVO());
		return lst;
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public ApParam createApParamEx(ApParam apParam, LogInfoVO logInfo) throws DataAccessException {
		if (apParam == null) {
			throw new IllegalArgumentException("apParam");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (apParam.getApParamCode() != null) {
			apParam.setApParamCode(apParam.getApParamCode().toUpperCase());
		}
		String typeStr = apParam.getTypeStr();
		repo.create(apParam);
		apParam = repo.getEntityById(ApParam.class, apParam.getId());
		if (apParam != null && apParam.getType() == null && !StringUtility.isNullOrEmpty(typeStr)) {
			String sql = "update ap_param set type = ? where ap_param_id = ?";
			List<Object> params = new ArrayList<Object>();
			params.add(typeStr.toUpperCase());
			params.add(apParam.getId());
			repo.executeSQLQuery(sql, params);
		}
		/*try {
			actionGeneralLogDAO.createActionGeneralLog(null, apParam, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}*/
		return apParam;
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public ApParam updateApParamEx(ApParam apParam, LogInfoVO logInfo) throws DataAccessException {
		if (apParam == null) {
			throw new IllegalArgumentException("apParam");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (apParam.getApParamCode() != null) {
			apParam.setApParamCode(apParam.getApParamCode().toUpperCase());
		}
		String typeStr = apParam.getTypeStr();
		repo.update(apParam);
		if (apParam != null && apParam.getType() == null && !StringUtility.isNullOrEmpty(typeStr)) {
			String sql = "update ap_param set type = ? where ap_param_id = ?";
			List<Object> params = new ArrayList<Object>();
			params.add(typeStr.toUpperCase());
			params.add(apParam.getId());
			repo.executeSQLQuery(sql, params);
		}
		/*try {
			actionGeneralLogDAO.createActionGeneralLog(null, apParam, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}*/
		return apParam;
	}

	/**
	 * @author vuongmq
	 * @date 15/01/2015
	 * @description danh sach uom
	 */
	@Override
	public List<ApParam> getListUoms(ApParamType type)
			throws DataAccessException {
		if (type == null) {
			throw new IllegalArgumentException("invalid parameter");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from ap_param where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		
		if (type != null) {
			sql.append(" and type=?");
			params.add(type.getValue());
		} 
		/*if (type.getValue() == ApParamType.UOM1.getValue()) {
			sql.append(" and type=?");
			params.add(ApParamType.UOM1.getValue());
		} else if (type.getValue() == ApParamType.UOM2.getValue()) {
			sql.append(" and type in (?, ?)");
			params.add(ApParamType.UOM1.getValue());
			params.add(ApParamType.UOM2.getValue());
		}*/
		sql.append(" order by ap_param_name");

		return repo.getListBySQL(ApParam.class, sql.toString(), params);
	}
	
	/**
	 * @author vuongmq
	 * @date 20/01/2015
	 * @description danh sach status theo tham so
	 */
	@Override
	public List<ApParam> getListActiveType(ApParamType type, ActiveType... activeTypes)
			throws DataAccessException {
		if (type == null) {
			throw new IllegalArgumentException("invalid parameter");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from ap_param where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (type != null) {
			sql.append(" and type=? ");
			params.add(type.getValue());
		}
		if (activeTypes != null && activeTypes.length > 0) {
			sql.append(" and value in ('-5' "); // de tranh truong hop activeTypes khong co gi giong ActiveType
			for(ActiveType act :activeTypes){
				if(ActiveType.RUNNING.equals(act) || ActiveType.STOPPED.equals(act) || ActiveType.WAITING.equals(act) || ActiveType.DELETED.equals(act)){
					sql.append(", ?");
					params.add(act.getValue().toString());
				}
			}
			sql.append(" )");
		} else {
			sql.append(" and value in (?, ?)");
			params.add(ActiveType.RUNNING.getValue().toString());
			params.add(ActiveType.STOPPED.getValue().toString());
		}
		sql.append(" order by value desc ");
		return repo.getListBySQL(ApParam.class, sql.toString(), params);
	}

}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.SaleOrderPromoLot;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class SaleOrderLotDAOImpl implements SaleOrderLotDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	CommonDAO commonDAO;

	@Override
	public SaleOrderLot createSaleOrderLot(SaleOrderLot saleOrderLot) throws DataAccessException {
		if (saleOrderLot == null) {
			throw new IllegalArgumentException("saleOrderLot");
		}
		repo.create(saleOrderLot);
		return repo.getEntityById(SaleOrderLot.class, saleOrderLot.getId());
	}

	@Override
	public void deleteSaleOrderLot(SaleOrderLot saleOrderLot) throws DataAccessException {
		if (saleOrderLot == null) {
			throw new IllegalArgumentException("saleOrderLot");
		}
		repo.delete(saleOrderLot);
	}

	@Override
	public SaleOrderLot getSaleOrderLotById(long id) throws DataAccessException {
		return repo.getEntityById(SaleOrderLot.class, id);
	}

	@Override
	public void updateSaleOrderLot(SaleOrderLot saleOrderLot) throws DataAccessException {
		if (saleOrderLot == null) {
			throw new IllegalArgumentException("saleOrderLot");
		}
		saleOrderLot.setUpdateDate(commonDAO.getSysDate());
		repo.update(saleOrderLot);
	}

	@Override
	public void createSaleOrderLot(List<SaleOrderLot> lstSaleOrderLot) throws DataAccessException {
		if (lstSaleOrderLot == null) {
			return;
		}
		repo.create(lstSaleOrderLot);
	}

	@Override
	public void createSaleOrderPromoLot(List<SaleOrderPromoLot> lstSaleOrderPromoLot) throws DataAccessException {
		if (lstSaleOrderPromoLot == null) {
			return;
		}
		repo.create(lstSaleOrderPromoLot);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SaleOrderLotDAO#getListSaleOrderLotBySaleOrderDetailId
	 * (long)
	 */
	@Override
	public List<SaleOrderLot> getListSaleOrderLotBySaleOrderDetailId(long saleOrderDetailId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_lot");
		sql.append(" where sale_order_detail_id = ?");
		params.add(saleOrderDetailId);
		return repo.getListBySQL(SaleOrderLot.class, sql.toString(), params);
	}

	@Override
	public List<SaleOrderPromoLot> getListSaleOrderPromoLotBySaleOrderId(long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_promo_lot");
		sql.append(" where sale_order_id = ?");
		params.add(saleOrderId);
		return repo.getListBySQL(SaleOrderPromoLot.class, sql.toString(), params);
	}
	
	@Override
	public List<SaleOrderPromoLot> getListSaleOrderPromoLotByListSaleOrderDetailId(List<SaleOrderDetail> lstDetail) throws DataAccessException {
		if (lstDetail == null || lstDetail.size() == 0) {
			return new ArrayList<SaleOrderPromoLot>();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_promo_lot");
		sql.append(" where sale_order_detail_id in (-1 ");
		for (SaleOrderDetail detail : lstDetail) {
			sql.append(",?");
			params.add(detail.getId());
		}
		sql.append(") ");
		return repo.getListBySQL(SaleOrderPromoLot.class, sql.toString(), params);
	}

	@Override
	public List<SaleOrderLot> getListSaleOrderLotBySaleOrderDetail(KPaging<SaleOrderLot> kPaging, long saleOrderDetailId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_lot where sale_order_detail_id = ?");
		params.add(saleOrderDetailId);
		if (kPaging == null)
			return repo.getListBySQL(SaleOrderLot.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(SaleOrderLot.class, sql.toString(), params, kPaging);
	}

	@Override
	public Integer sumQuatitySaleOrderLotByCondition(Long saleOrderId, Long saleOrderDetailId, String lot) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select NVL(SUM(quantity), 0) as count from sale_order_lot");
		sql.append(" where 1 = 1");
		if (saleOrderId != null) {
			sql.append(" and sale_order_id = ?");
			params.add(saleOrderId);
		}
		if (saleOrderDetailId != null) {
			sql.append(" and sale_order_detail_id = ?");
			params.add(saleOrderDetailId);
		}
		if (!StringUtility.isNullOrEmpty(lot)) {
			sql.append(" and lot = ?");
			params.add(lot);
		}
		return repo.countBySQL(sql.toString(), params);
	}

	@Override
	public Boolean checkProductExceptLotExpire(Long productId, Long shopId, Integer quant) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT ( ");
		sql.append("   (SELECT NVL(SUM(available_quantity), 0) ");
		sql.append("   FROM product_lot pl ");
		sql.append("   WHERE object_id =? ");
		params.add(shopId);
		sql.append("   AND object_type =? ");
		params.add(StockObjectType.SHOP.getValue());
		sql.append("   AND product_id  = ? ");
		params.add(productId);
		sql.append("   ) - ");
		sql.append("   (SELECT NVL(SUM(available_quantity), 0) ");
		sql.append("   FROM product_lot pl ");
		sql.append("   WHERE object_id                  = ? ");
		params.add(shopId);
		sql.append("   AND object_type                  = ? ");
		params.add(StockObjectType.SHOP.getValue());
		sql.append("   AND product_id                   = ? ");
		params.add(productId);
		sql.append("   AND expiry_date - TRUNC(sysdate) < ");
		sql.append("     (SELECT value ");
		sql.append("     FROM ap_param ");
		sql.append("     WHERE status      = 1 ");
		sql.append("     AND ap_param_code = 'NUM_DAY_LOT_EXPIRE' ");
		sql.append("     ) ");
		sql.append("   )) count ");
		sql.append(" FROM dual ");

		int c = repo.countBySQL(sql.toString(), params);
		return c >= quant ? true : false;
	}
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.EquipStockTotal;
import ths.dms.core.entities.EquipStockTransForm;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.vo.EquipmentStockDeliveryVO;
import ths.dms.core.exceptions.DataAccessException;
/**
 * EquipmentPeriodDAO
 * 
 * @author nhutnn
 * @since 15/12/2014
 * @description: Lop DAO quan ly kho
 */
public interface EquipmentStockTotalDAO {
	
	/**
	 * Lay kho thiet bi
	 * @author nhutnn
	 * @since 16/12/2014
	 * @param stockId
	 * @param stockType
	 * @return
	 * @throws DataAccessException
	 */
	EquipStockTotal getEquipStockTotalById(Long stockId, EquipStockTotalType stockType, Long equipGroupId) throws DataAccessException;
	
	/**
	 * Chinh sua kho thiet bi
	 * @author nhutnn
	 * @since 16/12/2014
	 * @param eqDeliveryRecord
	 * @throws DataAccessException
	 */
	void updateEquipmentStockTotal(EquipStockTotal eqDeliveryRecord) throws DataAccessException;
	
	/**
	 * Lay danh sach kho thiet bi
	 * @author phuongvm
	 * @since 17/12/2014
	 * @param stockId
	 * @param stockType
	 * @param equipGroupId
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipStockTotal> getListEquipStockTotal(Long stockId, EquipStockTotalType stockType, Long equipGroupId) throws DataAccessException;
	
	/**
	 * Tao kho
	 * @author nhutnn
	 * @since 18/12/2014
	 * @param equipStockTotal
	 * @return
	 * @throws DataAccessException
	 */
	EquipStockTotal createEquipStockTotal(EquipStockTotal equipStockTotal) throws DataAccessException;
	
	/**
	 * Lay danh sach kho thiet bi
	 * @author nhutnn
	 * @since 19/12/2014
	 */
	List<EquipmentStockDeliveryVO> getListEquipStockTotalVOByfilter(EquipmentFilter<EquipmentStockDeliveryVO> filter) throws DataAccessException;
	/**
	 * Lay bien ban theo Id
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	EquipStockTransForm getEquipStockTransFormById(Long idRecord) throws DataAccessException;
	
	/**
	 * Lay bien ban theo Id va shop
	 * @author hoanv25
	 * @since 25/12/2014
	 */
	EquipStockTransForm getEquipStockTransFormById(Long idRecord, Long shopId) throws DataAccessException;

}

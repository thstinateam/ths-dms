package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.PoVnmLot;
import ths.dms.core.exceptions.DataAccessException;

public interface PoVnmLotDAO {

	PoVnmLot createPoVnmLot(PoVnmLot poVnmLot) throws DataAccessException;

	void deletePoVnmLot(PoVnmLot poVnmLot) throws DataAccessException;

	void updatePoVnmLot(PoVnmLot poVnmLot) throws DataAccessException;
	
	PoVnmLot getPoVnmLotById(long id) throws DataAccessException;

	List<PoVnmLot> getListPoVnmLotByPoVnmId(Long poVnmId) throws DataAccessException;
	
	PoVnmLot getPoVnmLotByPoVnmIdandProductId(Long poVnmId, Long productId) throws DataAccessException;

	List<PoVnmLot> getListPoVnmLotByPoVnmDetailId(Long poVnmDetailId, Long productId) throws DataAccessException;

	/**
	 * updateListPoVnmLot
	 * @author hieunq1
	 * @param listPoVnmLot
	 * @throws DataAccessException
	 */
	void updateListPoVnmLot(List<PoVnmLot> listPoVnmLot)
			throws DataAccessException;

	/**
	 * createListPoVnmLot
	 * @author hieunq1
	 * @param listPoVnmLot
	 * @throws DataAccessException
	 */
	void createListPoVnmLot(List<PoVnmLot> listPoVnmLot)
			throws DataAccessException;

	/**
	 * getPoVnmLotByIdForUpdate
	 * @author hieunq1
	 * @param id
	 * @return
	 * @throws DataAccessException
	 */
	PoVnmLot getPoVnmLotByIdForUpdate(long id) throws DataAccessException;
}

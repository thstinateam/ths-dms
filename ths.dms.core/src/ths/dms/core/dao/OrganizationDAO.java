package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Organization;
import ths.dms.core.entities.ShopType;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrganizationFilter;
import ths.dms.core.entities.enumtype.OrganizationUnitTypeFilter;
import ths.dms.core.entities.filter.OrganizationSystemFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.OrganizationUnitTypeVO;
import ths.dms.core.entities.vo.OrganizationVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.entities.StaffType;


/**
 * The Interface OrganizationDAO.
 * @author vuongmq
 * @date 11/02/2015
 */
public interface OrganizationDAO {
	/**BEGIN VUONGMQ*/
	Organization getOrganizationById(Long id) throws DataAccessException;
	
	List<Organization> getListOrganization(OrganizationSystemFilter<Organization> filter) throws DataAccessException;
	
	List<Organization> getListOrganizationParent(OrganizationSystemFilter<Organization> filter) throws DataAccessException;
	
	OrganizationVO getOrganizationVOById(Long Id) throws DataAccessException;
	
	List<OrganizationVO> getListOrganizationSystemVO(OrganizationSystemFilter<OrganizationVO> filter) throws DataAccessException;
	
	OrganizationVO getOrganizationVOByParent() throws DataAccessException;
	
	List<OrganizationVO> getListOrganizationSystemVOShop(OrganizationSystemFilter<OrganizationVO> filter) throws DataAccessException;
	
	List<OrganizationVO> getListOrganizationSystemVOStaff(OrganizationSystemFilter<OrganizationVO> filter) throws DataAccessException;
	
	List<StaffType> getListStaffType(OrganizationSystemFilter<StaffType> filter) throws DataAccessException;
	
	/**END VUONGMQ*/
	
	/**BEGIN PHUOCDH2 **/
	Organization createOrganization(Organization organization, LogInfoVO logInfo) throws DataAccessException ;
	
	
	void deleteOrganization(Organization organization, LogInfoVO logInfo) throws DataAccessException;
	
	Organization getOrganizationById(long id) throws DataAccessException ;
		
	void updateOrganization(Organization organization, LogInfoVO logInfo) throws DataAccessException ;
		
	List<OrganizationUnitTypeVO> getListOrganizationUnitType(OrganizationUnitTypeFilter filter,KPaging<OrganizationUnitTypeVO> kPaging)throws DataAccessException;
	
	StaffType getStaffTypeById(Long id) throws DataAccessException;
	
	Integer getCountByPrefixShop(String prefix) throws DataAccessException ;
	
    Integer getCountByPrefixStaff(String prefix) throws DataAccessException;
    
    /**
	 * kiem tra loai don vi, chuc vu nay da co tren node cha hay chua 
	 * @author phuocdh2
	 * @since 03/05/2015
	 * @description : nhan vao loai don vi ,chuc vu va kiem tra loai don vi chuc vu nay da co tren node cha chua
	 */
	List<Organization> getListOrgExistParentNodeByFilter(OrganizationFilter filter)throws DataAccessException;
	/**
	 * kiem tra loai don vi, chuc vu nay da co tren node cha hay chua 
	 * @author phuocdh2
	 * @since 03/05/2015
	 * @description : nhan vao loai don vi ,chuc vu va kiem tra loai don vi chuc vu nay da co tren node cha chua
	 */
	List<Organization> getListOrgExistSubNodeByFilter(OrganizationFilter filter)throws DataAccessException;
	/** END PHUOCDH2 **/
	/**
	 * get shop type by id
	 * @param id
	 * @return shop type
	 * @createDate 12/02/2015
	 * @author liemtpt
	 * @description:  lay shop type by id
	 */
	ShopType getShopTypeById(Long id) throws DataAccessException;
	/**
	 * get list organization by filter
	 * @param Organization filter
	 * @return the list organization
	 * @createDate 12/02/2015
	 * @author liemtpt
	 * @description:  lay danh sach organization
	 */
	List<Organization> getListOrganizationByFilter(OrganizationFilter filter)
			throws DataAccessException;
	/**
	 * get max node ordinal
	 * @param organizationId
	 * @createDate 12/02/2015
	 * @author liemtpt
	 * @description:  lay thu tu node_ordinal cao nhat
	 * vuongmq update lay so thu tu cao nhat trong bang organization
	 */
	Integer getMaxNodeOrdinal() throws DataAccessException;
	/**
	 * get list organization by id for context menu
	 * @param organizationId
	 * @createDate 26/02/2015
	 * @author liemtpt
	 * @description:  lay danh sach organization tu orgid cho context menu cua cay don vi
	 */
	List<OrganizationVO> getListOrganizationById4ContextMenu(Long orgId)
			throws DataAccessException;
	/**
	 * get list staff exits staff type id in organization
	 * @param organizationId
	 * @createDate 05/03/2015
	 * @author liemtpt
	 * @description: lay danh sach nhan vien co ton tai loai chuc vu cua nhan vien dang duoc chon 
	 */

	List<Staff> getListStaffExitsStaffTypeIdInOrganization(Long orgId, Long staffId) throws DataAccessException;
	/**
	 * Lay organization tuong ung voi cac dieu kien trong staff_type, shop_type
	 * @author phuocdh2
	 * @since 03/26/2015
	 * @description : nhan vao staff_code, staff_name,staff.status tra ra organization ung voi staff_name  do  
	 */
	Organization getOrgJoinTypeByFilter(OrganizationFilter filter) throws DataAccessException ;
	/**
	 * Lay StaffType tuong ung voi cac dieu kien 
	 * @author phuocdh2
	 * @since 03/26/2015
	 * @description : nhan vao staff_code,staff_type , lay ra StaffType
	 */
	StaffType getStaffTypeByCondition(OrganizationFilter filter) throws DataAccessException ;
	/**
	 * Lay ShopType tuong ung voi cac dieu kien 
	 * @author phuocdh2
	 * @since 03/26/2015
	 * @description : nhan vao shop_code,shop_type , lay ra ShopType
	 */
	ShopType getShopTypeByCondition(OrganizationFilter filter) throws DataAccessException;

	/**
	 * @author tungmt
	 * @since 9/6/2015
	 * Check 2 org co phai cha con nhau ko
	 */
	Boolean checkParentOrg(OrganizationFilter filter)
			throws DataAccessException;
}

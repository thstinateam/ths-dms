package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PayReceived;
import ths.dms.core.entities.PayReceivedTemp;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ReceiptType;
import ths.dms.core.entities.filter.PayReceivedFilter;
import ths.dms.core.entities.vo.CustomerDebitVO;
import ths.dms.core.entities.vo.PayReceivedVO;
import ths.core.entities.vo.rpt.RptCustomerPayReceivedVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class PayReceivedDAOImpl implements PayReceivedDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public PayReceived createPayReceived(PayReceived payReceived) throws DataAccessException {
		if (payReceived == null) {
			throw new IllegalArgumentException("payReceived");
		}
		if (payReceived.getPayReceivedNumber() != null)
			payReceived.setPayReceivedNumber(payReceived.getPayReceivedNumber().toUpperCase());
		repo.create(payReceived);
		return repo.getEntityById(PayReceived.class, payReceived.getId());
	}

	@Override
	public void deletePayReceived(PayReceived payReceived) throws DataAccessException {
		if (payReceived == null) {
			throw new IllegalArgumentException("payReceived");
		}
		repo.delete(payReceived);
	}

	@Override
	public PayReceived getPayReceivedById(long id) throws DataAccessException {
		return repo.getEntityById(PayReceived.class, id);
	}
	
	@Override
	public void updatePayReceived(PayReceived payReceived) throws DataAccessException {
		if (payReceived == null) {
			throw new IllegalArgumentException("payReceived");
		}
		if (payReceived.getPayReceivedNumber() != null)
			payReceived.setPayReceivedNumber(payReceived.getPayReceivedNumber().toUpperCase());
		payReceived.setUpdateDate(commonDAO.getSysDate());
		repo.update(payReceived);
	}
	
	@Override
	public void updatePayReceivedWithoutUpdateDate(PayReceived payReceived) throws DataAccessException {
		if (payReceived == null) {
			throw new IllegalArgumentException("payReceived");
		}
		if (payReceived.getPayReceivedNumber() != null)
			payReceived.setPayReceivedNumber(payReceived.getPayReceivedNumber().toUpperCase());
		repo.update(payReceived);
	}
	
	@Override
	public List<RptCustomerPayReceivedVO> getListRptCustomerPayReceivedVO(
			KPaging<RptCustomerPayReceivedVO> kPaging, Long shopId, Date fromPayDate,
			Date toPayDate, Long customerId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" select c.customer_code as customerCode,");
		sql.append("     c.customer_name as customerName,");
		sql.append("     so.order_date as orderDate,");
		sql.append("     so.order_number as orderNumber,");
		sql.append("     dd.pay_date as payDate,");
		sql.append("     dd.payment_type as paymentType,");
		sql.append("     pr.pay_received_number as payReceivedNumber,");
		sql.append("     d.total as total,");
		sql.append("     d.remain as remain,");
		sql.append("     s.staff_code cashierCode,");
		sql.append("     s.staff_name cashierName");

		fromSql.append(" from sale_order so join debit_detail d on so.sale_order_id = d.from_object_id");
		fromSql.append("     join payment_detail dd on d.debit_detail_id = dd.debit_detail_id");
		fromSql.append("     join debit dg on d.debit_id = dg.debit_id and dg.owner_type = ?");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		fromSql.append("     join pay_received pr on pr.pay_received_id = dd.pay_received_id");
		fromSql.append("     join customer c on so.customer_id = c.customer_id");
		fromSql.append("     left join staff s on so.cashier_id = s.staff_id");
		fromSql.append(" where 1 = 1");
		if (shopId != null) {
			fromSql.append("   and so.shop_id = ?");
			params.add(shopId);
		}
		if (fromPayDate != null) {
			fromSql.append("   and dd.pay_date >= trunc(?)");
			params.add(fromPayDate);
		}
		if (toPayDate != null) {
			fromSql.append("   and dd.pay_date < trunc(?) + 1");
			params.add(toPayDate);
		}
		if (customerId != null) {
			fromSql.append("   and c.customer_id = ?");
			params.add(customerId);
		}

		sql.append(fromSql);
		sql.append(" order by customerCode");
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count");
		countSql.append(fromSql);

		String[] fieldNames = { "customerCode", // 0
				"customerName", // 1
				"orderDate", // 2
				"orderNumber", // 3
				"payDate", // 4
				"paymentType", // 5
				"payReceivedNumber", // 6
				"total", // 7
				"remain", // 8
				"cashierCode", // 9
				"cashierName", // 10
		};

		Type[] fieldTypes = { StandardBasicTypes.STRING, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.DATE, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.DATE, // 4
				StandardBasicTypes.INTEGER, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.BIG_DECIMAL, // 7
				StandardBasicTypes.BIG_DECIMAL, // 8
				StandardBasicTypes.STRING, // 9
				StandardBasicTypes.STRING, // 10
		};

		if (kPaging == null)
			return repo.getListByQueryAndScalar(RptCustomerPayReceivedVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(
					RptCustomerPayReceivedVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params,
					kPaging);
	}

	@Override
	public Integer getCountOfPayReceivedInSysdate() throws DataAccessException {
		String sql = "SELECT COUNT(1) COUNT FROM pay_received WHERE create_date >= TRUNC(sysdate)";
		ArrayList<Object> params = new ArrayList<Object>();
		return repo.countBySQL(sql, params);
	}
	
	@Override
	public PayReceived getPayReceivedByNumber(String payreceivedNumber, Long shopId) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(payreceivedNumber)) {
			return null;
		}
		StringBuilder sql =  new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		
		sql.append("SELECT * from pay_received WHERE PAY_RECEIVED_NUMBER = ?");
		params.add(payreceivedNumber.toUpperCase());
		
		if (shopId != null) {
			sql.append(" and shop_id = ?");
			params.add(shopId);
		}
		
		return repo.getEntityBySQL(PayReceived.class, sql.toString(), params);
	}

	@Override
	public String getPayReceivedStringSugguest(Date lockDate) throws DataAccessException {		
		ArrayList<Object> params = new ArrayList<Object>();
		String sql = "SELECT ('CT' || to_char(?, 'ddMMyy') || COUNT(1)) FROM pay_received WHERE create_date >= TRUNC(?) and create_date < trunc(?) + 1";
		params.add(lockDate);
		params.add(lockDate);
		params.add(lockDate);
		return (String) repo.getObjectByQuery(sql, params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public PayReceivedTemp clonePayReceivedToTemp(PayReceived payReceived) throws DataAccessException {
		PayReceivedTemp p = new PayReceivedTemp();

		p.setPayReceivedNumber(payReceived.getPayReceivedNumber());
		p.setAmount(payReceived.getAmount());
		p.setCreateDate(payReceived.getCreateDate());
		p.setPaymentType(payReceived.getPaymentType());
		p.setShop(payReceived.getShop());
		//p.setCustomer(payReceived.getCustomer());
		p.setReceiptType(payReceived.getReceiptType());
		p.setCreateUser(payReceived.getCreateUser());
		p.setType(payReceived.getType());
		p.setCreateDateSys(payReceived.getCreateDateSys());
		p.setBank(payReceived.getBank());
		p.setPaymentStatus(payReceived.getPaymentStatus());
		p.setPayerName(payReceived.getPayerName());
		p.setPayerAddress(payReceived.getPayerAddress());
		p.setPaymentReason(payReceived.getPaymentReason());
		p.setApproved(1);
		p.setUpdateDate(payReceived.getUpdateDate());
		p.setUpdateUser(payReceived.getUpdateUser());
		p = repo.create(p);
		return p;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PayReceivedVO> getListPayReceivedVO(PayReceivedFilter<PayReceivedVO> filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter null");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select pr.pay_received_number as payReceivedNumber,");
		sql.append(" pr.pay_received_id as payId, ");
		sql.append(" pr.receipt_type as receiptType,");
		sql.append(" decode((select count(1) from payment_detail pd where pd.customer_id is not null and pd.pay_received_id = pr.pay_received_id), 0, 1, 3) as ownerType,");
		sql.append(" to_char(pr.create_date, 'dd/mm/yyyy') as createDateStr,");
		sql.append(" nvl(pr.amount, 0) as amount, pr.payment_status as status,");
		sql.append(" (select bank_name from bank where bank_id = pr.bank_id) as bankName ");
		
		StringBuilder fSql = new StringBuilder();
		fSql.append(" from pay_received pr");
		fSql.append(" where 1=1");
		
		if(filter.getShopId() != null){
			fSql.append(" and pr.shop_id = ? ");
			params.add(filter.getShopId());
		}
		
		String s = null;
		if (!StringUtility.isNullOrEmpty(filter.getPayReceivedNumber())) {
			fSql.append(" and pr.pay_received_number like ? escape '/' ");
			s = filter.getPayReceivedNumber();
			s = s.toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
				
		if (DebitOwnerType.SHOP.getValue().equals(filter.getOwnerType())) {
			fSql.append(" and (select count(1) from payment_detail pd where pd.status =1 and pd.customer_id is not null and pd.pay_received_id = pr.pay_received_id) = 0");
		} else if (DebitOwnerType.CUSTOMER.getValue().equals(filter.getOwnerType())) {
			fSql.append(" and (select count(1) from payment_detail pd where pd.status =1 and pd.customer_id is not null and pd.pay_received_id = pr.pay_received_id) > 0 ");
		}
		
		if (filter.getReceiptType() != null) {
			fSql.append(" and pr.receipt_type = ?");
			params.add(filter.getReceiptType());
		} else {
			fSql.append(" and pr.receipt_type in (?, ?)");
			params.add(ReceiptType.RECEIVED.getValue());
			params.add(ReceiptType.PAID.getValue());
		}
		
		if (filter.getStatus() != null) {
			fSql.append(" and pr.payment_status = ? and pr.payment_status is not null ");
			params.add(filter.getStatus());
		}
		fSql.append(" and pr.create_date is not null ");
		if (filter.getFromDate() != null) {
			fSql.append(" and pr.create_date >= trunc(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			fSql.append(" and pr.create_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		
		if (filter.getFromAmount() != null) {
			fSql.append(" and amount >= ?");
			params.add(filter.getFromAmount());
		}
		if (filter.getToAmount() != null) {
			fSql.append(" and amount <= ?");
			params.add(filter.getToAmount());
		}
		
		sql.append(fSql.toString());
		//sql.append(" group by pr.pay_received_number, receipt_type, decode(pr.customer_id, null, 1, 3), to_char(pr.create_date, 'dd/mm/yyyy'), pr.bank_id");
		
		String[] fieldNames = {
				"payReceivedNumber",
				"payId",
				"receiptType",
				"ownerType",
				"createDateStr",
				"amount",
				"status",
				"bankName"
		};
		Type[] fieldTypes = {
				StandardBasicTypes.STRING,
				StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING
		};
		
		List<PayReceivedVO> lst = null;
		if (filter.getPaging() == null) {
			sql.append(" order by ownerType desc, receiptType, payReceivedNumber ");
			lst = repo.getListByQueryAndScalar(PayReceivedVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			sql.append(" order by ownerType desc, receiptType, payReceivedNumber ");
			
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count");
			countSql.append(fSql.toString());
			lst = repo.getListByQueryAndScalarPaginated(PayReceivedVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params, filter.getPaging());
		}
		return lst;
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public PayReceivedVO getPayReceivedVO(PayReceivedFilter<PayReceivedVO> filter) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(filter.getPayReceivedNumber())) {
			throw new IllegalArgumentException("payReceivedNumber null or empty");
		}
		String payReceivedNumber = filter.getPayReceivedNumber().toUpperCase();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select pr.pay_received_number as payReceivedNumber, ");
		sql.append(" pr.pay_received_id as payId, ");
		sql.append(" pr.receipt_type as receiptType, ");
		sql.append(" decode((select count(1) from payment_detail pd where pd.customer_id is not null and pd.pay_received_id = pr.pay_received_id), 0, 1, 3) as ownerType, ");
		sql.append(" nvl((select sum(pd.amount) from payment_detail pd where pd.pay_received_id = pr.pay_received_id and pd.status = ?), 0) as amount, ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" pr.payer_name as payerName, ");
		sql.append(" pr.payer_address as payerAddress, ");
		sql.append(" pr.payment_reason as payingReason, ");
		sql.append(" pr.payment_status as status, ");
		sql.append(" nvl((select sum(pd.discount) from payment_detail pd where pd.pay_received_id = pr.pay_received_id and pd.status = ?), 0) as discount ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" from pay_received pr ");		
		sql.append(" where pay_received_number = ? ");
		params.add(payReceivedNumber);
		if(filter.getPayId() != null){
			sql.append(" and pr.pay_received_id = ? ");
			params.add(filter.getPayId());
		}
		if(filter.getShopId() != null){
			sql.append(" and pr.shop_id = ? ");
			params.add(filter.getShopId());
		}
		//sql.append(" GROUP BY pr.pay_received_number,  pr.receipt_type,  decode((select count(1) from payment_detail pd where pd.customer_id is not null and pd.pay_received_id = pr.pay_received_id), 0, 1, 3),  pr.payer_name,  pr.payer_address,  pr.payment_reason,  pr.payment_status ");
		
		String[] fieldNames = {
				"payReceivedNumber",
				"payId",
				"receiptType",
				"ownerType",
				"amount",
				"payerName",
				"payerAddress",
				"payingReason",
				"status",
				"discount"
		};
		
		Type[] fieldTypes = {
				StandardBasicTypes.STRING,
				StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL
		};
		
		List<PayReceivedVO> lst = repo.getListByQueryAndScalar(PayReceivedVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (lst != null && lst.size() > 0) {
			return lst.get(0);
		}
		return null;
	}


	@Override
	public PayReceivedTemp getPayReceivedTempByNumber(String code, Long shopId) throws DataAccessException {
		// TODO Auto-generated method stub
		String sql = "SELECT * from pay_received_temp WHERE PAY_RECEIVED_NUMBER = ? ";
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		if(shopId != null){
			sql += " and shop_id = ? ";
			params.add(shopId);
		}
		return repo.getEntityBySQL(PayReceivedTemp.class, sql, params);
	}

	@Override
	public void updatePayReceivedTemp(PayReceivedTemp payReceivedTemp) throws DataAccessException {
		// TODO Auto-generated method stub
		if (payReceivedTemp == null) {
			throw new IllegalArgumentException("payReceivedTemp");
		}
		if (payReceivedTemp.getPayReceivedNumber() != null)
			payReceivedTemp.setPayReceivedNumber(payReceivedTemp.getPayReceivedNumber().toUpperCase());
		payReceivedTemp.setUpdateDate(commonDAO.getSysDate());
		repo.update(payReceivedTemp);
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public PayReceivedVO getPayReceivedVOForExport(long payId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select pr.receipt_type as receiptType,");
		sql.append(" pr.pay_received_number as payReceivedNumber,");
		sql.append(" pr.payer_name as payerName,");
		sql.append(" pr.payer_address as payerAddress,");
		sql.append(" pr.payment_reason as payingReason,");
		sql.append(" nvl(sum(pd.amount), 0) as amount,");
		sql.append(" nvl(sum(pd.discount), 0) as discount,");
		sql.append(" (select listagg(from_object_number, ', ') within group (order by from_object_number)");
		sql.append(" from debit_detail dd");
		sql.append(" where exists (select 1 from payment_detail");
		sql.append(" where debit_detail_id = dd.debit_detail_id and status = ? and pay_received_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(payId);
		sql.append(" )");
		sql.append(" ) as fromObjectNumbers,");
		sql.append(" (select nvl(sum(remain), 0)");
		sql.append(" from debit_detail dd");
		sql.append(" where exists (select 1 from payment_detail");
		sql.append(" where debit_detail_id = dd.debit_detail_id and status = ? and pay_received_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(payId);
		sql.append(" )");
		sql.append(" ) as remain,");
		sql.append(" pr.payment_status as status");
		sql.append(" from pay_received pr");
		sql.append(" join payment_detail pd on (pd.pay_received_id = pr.pay_received_id and pd.status = ?)");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" where pr.pay_received_id = ?");
		params.add(payId);
		sql.append(" group by pr.receipt_type, pr.pay_received_number, pr.payer_name, pr.payer_address, pr.payment_reason, pr.payment_status");
		
		String[] fieldNames = {
				"receiptType",
				"payReceivedNumber",
				"payerName",
				"payerAddress",
				"payingReason",
				"amount",
				"discount",
				"fromObjectNumbers",
				"remain",
				"status"
		};
		
		Type[] fieldTypes = {
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER
		};
		
		List<PayReceivedVO> lst = repo.getListByQueryAndScalar(PayReceivedVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (lst != null && lst.size() > 0) {
			return lst.get(0);
		}
		return null;
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<CustomerDebitVO> getCustomerDebitOfPayReceived(long shopId, long payId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select d.debit_detail_id as debitId,");
		sql.append(" c.short_code as shortCode,");
		sql.append(" c.customer_name as customerName,");
		sql.append(" d.from_object_number as orderNumber,");
		sql.append(" d.debit_date as orderDate,");
		sql.append(" d.total as total,");
		sql.append(" d.total_pay as totalPay,");
		sql.append(" d.remain as remain,");
		sql.append(" nvl(d.discount_amount, 0) as discountAmount,");
		sql.append(" (select staff_code from staff where staff_id = so.cashier_id) as nvttCodeName,");
		sql.append(" (select staff_code from staff where staff_id = so.delivery_id) as nvghCodeName,");
		sql.append(" (select staff_code from staff where staff_id = so.staff_id) as nvbhCodeName,");
		sql.append(" (select staff_name from staff where staff_id = so.cashier_id) as nvttName,");
		sql.append(" (select staff_name from staff where staff_id = so.delivery_id) as nvghName,");
		sql.append(" (select staff_name from staff where staff_id = so.staff_id) as nvbhName,");
		sql.append(" pd.amount as payAmt,");
		sql.append(" pd.discount as discountAmt");
		sql.append(" from debit_detail d join debit dg on (dg.debit_id = d.debit_id and dg.object_type = ?)");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		sql.append(" join customer c on (c.customer_id = dg.object_id and c.shop_id = ?)");
		params.add(shopId);
		sql.append(" join payment_detail pd on (pd.debit_detail_id = d.debit_detail_id and pd.pay_received_id = ?)");
		params.add(payId);
		sql.append(" left join sale_order so on (d.from_object_id = so.sale_order_id)");
		
		final String[] fieldNames = { 
				"debitId",// 0
				"shortCode",// 1
				"customerName",// 2
				"orderNumber",// 3
				"orderDate" ,// 4
				"total",// 5
				"totalPay",// 6
				"remain",// 7
				"discountAmount",// 8
				"nvttCodeName",// 9
				"nvghCodeName",// 10
				"nvbhCodeName",// 11
				"nvttName",
				"nvghName",
				"nvbhName",
				"payAmt",
				"discountAmt"
		};
		
		final Type[] fieldTypes = { 
				StandardBasicTypes.LONG,//0
				StandardBasicTypes.STRING,//1
				StandardBasicTypes.STRING,//2
				StandardBasicTypes.STRING,//3
				StandardBasicTypes.TIMESTAMP,//4
				StandardBasicTypes.BIG_DECIMAL,//5
				StandardBasicTypes.BIG_DECIMAL,//6
				StandardBasicTypes.BIG_DECIMAL,//7
				StandardBasicTypes.BIG_DECIMAL,//8
				StandardBasicTypes.STRING,//9
				StandardBasicTypes.STRING,//10
				StandardBasicTypes.STRING,//11
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL
		};
		
		List<CustomerDebitVO> lst = repo.getListByQueryAndScalar(CustomerDebitVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	@Override
	public Integer checkOwnerTypeCustomerPayReceived(Long payReceivedId) throws DataAccessException {
		// TODO Auto-generated method stub
		if (payReceivedId == null) {
			throw new IllegalArgumentException("payReceivedId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select count(pd.payment_detail_id) as count from payment_detail pd where pd.status = 1 and pd.customer_id is not null and pd.pay_received_id = ? ");
		params.add(payReceivedId);
		return repo.countBySQL(sql.toString(), params);
	}

	@Override
	public PayReceived getPayReceivedByFilter(PayReceivedFilter<PayReceivedVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * from pay_received pr WHERE 1=1 ");
		ArrayList<Object> params = new ArrayList<Object>();
		if(!StringUtility.isNullOrEmpty(filter.getPayReceivedNumber())){
			sql.append(" and pr.pay_received_number = ? ");
			params.add(filter.getPayReceivedNumber().toUpperCase());
		}
		if(filter.getPayId() != null){
			sql.append(" and pr.pay_received_id = ? ");
			params.add(filter.getPayId());
		}
		if(filter.getShopId() != null){
			sql.append(" and pr.shop_id = ? ");
			params.add(filter.getShopId());
		}		
		return repo.getEntityBySQL(PayReceived.class, sql.toString(), params);
		
	}
}

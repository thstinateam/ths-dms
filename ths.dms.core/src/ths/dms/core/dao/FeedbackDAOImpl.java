/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Feedback;
import ths.dms.core.entities.FeedbackStaff;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.filter.FeedbackFilter;
import ths.dms.core.entities.vo.FeedbackVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.Constant;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

/**
 * FeedbackDAOImpl
 * @author vuongmq
 * @since 11/11/2015 
 */
public class FeedbackDAOImpl implements FeedbackDAO {
	
	@Autowired
	private IRepository repo;

	@Override
	public List<Feedback> getListFeedbackByFilter(FeedbackFilter filter) throws DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FeedbackStaff> getListFeedbackStaffByFilter(FeedbackFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException(" FeedbackFilter is null ");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from feedback_staff where 1 = 1 ");
		if (filter.getLstId() != null) {
			String paramFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getLstId(), "feedback_staff_id");
			sql.append(paramFix);
			/*sql.append(" and feedback_staff_id in (-1 ");
			for (int i = 0, sz = filter.getLstId().size(); i < sz; i++) {
				sql.append(",? ");
				params.add(filter.getLstId().get(i));
			}
			sql.append(" ) ");*/
		}
		return repo.getListBySQL(FeedbackStaff.class, sql.toString(), params);
	}
	
	@Override
	public FeedbackVO getFeedBackVOByFilter(FeedbackFilter filter) throws DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FeedbackVO> getListFeedbackVOByFilter(FeedbackFilter filter) throws DataAccessException {
		if (filter == null || filter.getStaffRootId() == null || filter.getRoleId() == null || filter.getShopRootId() == null) {
			throw new IllegalArgumentException(" StaffRootId is null or RoleId is null or ShopRootId is null ");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select fb.feedback_id as feedbackId, ");
		sql.append(" fbs.feedback_staff_id as id, "); // chinh
		sql.append(" st.staff_id as staffId, ");
		sql.append(" st.staff_code as staffCode, ");
		sql.append(" st.staff_name as staffName, ");
		//sql.append(" listagg(to_char(cu.short_code || ' - ' || cu.customer_name), ', ') within group (order by cu.short_code) as customerName, ");
		sql.append(" listagg((case when cu.short_code is not null ");
		sql.append(" then to_char(cu.short_code || ' - ' || cu.customer_name) ");
		sql.append(" else '' end ");
		sql.append(" ), ', ') within group (order by cu.short_code) as customerName, ");
		sql.append(" ap.ap_param_code as typeId, ");
		sql.append(" ap.ap_param_name as typeName, ");
		sql.append(" fb.content as content, ");
		sql.append(" fb.status as status, ");
		sql.append(" fbs.result as result, ");
		sql.append(" cr.staff_id as createUserId, ");
		sql.append(" cr.staff_code as createCode, ");
		sql.append(" cr.staff_name as createName, ");
		sql.append(" to_char(fb.remind_date, 'dd/mm/yyyy hh24:mi') as remindDateStr, ");
		sql.append(" to_char(fbs.done_date, 'dd/mm/yyyy hh24:mi') as doneDateStr, ");
		sql.append(" (select count(*) as count from media_item where object_type = ? and object_id = fb.feedback_id) as item ");
		params.add(MediaObjectType.IMAGE_FEEDBACK.getValue());
		sql.append(" from feedback fb ");
		sql.append(" join feedback_staff fbs on fb.feedback_id = fbs.feedback_id and fb.status = 1 ");
		sql.append(" join ap_param ap on ap.ap_param_code = fb.type and ap.status = 1 and ap.type like ? ");
		params.add(ApParamType.FEEDBACK_TYPE.getValue());
		sql.append(" join (select * from  table (F_GET_LIST_CHILD_STAFF_INHERIT(?, sysdate, ?, ?, null))  ");
		sql.append(" 		union select ?, ? ,'' from dual");
		sql.append("     ) tmp on tmp.number_key_id = fbs.staff_id");
		params.add(filter.getStaffRootId());
		params.add(filter.getRoleId());
		params.add(filter.getShopRootId());
		// union voi chinh user dang nhap
		params.add(filter.getStaffRootId());
		params.add(filter.getShopRootId());
		sql.append(" join staff st on st.staff_id = fbs.staff_id ");
		sql.append(" join staff cr on cr.staff_id = fb.create_user_id ");
		sql.append(" left join feedback_staff_customer fbsc on fbsc.feedback_staff_id = fbs.feedback_staff_id ");
		sql.append(" left join customer cu on cu.customer_id = fbsc.customer_id ");
		sql.append(" where 1 = 1 ");
		if (filter.getShopId() != null) {
			sql.append(" and st.shop_id in  ");
			sql.append(" (select shop_id ");
			sql.append(" from shop ");
			sql.append(" start with shop_id = ? ");
			sql.append(" connect by prior shop_id = parent_shop_id) ");
			params.add(filter.getShopId());
		}
		if (filter.getCreateUserId() != null) {
			sql.append(" and cr.staff_id = ? ");
			params.add(filter.getCreateUserId());
		}
		if (filter.getStaffId() != null) {
			sql.append(" and fbs.staff_id = ? ");
			params.add(filter.getStaffId());
		}
		if (filter.getFromDate() != null) {
			sql.append(" and fb.remind_date >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and fb.remind_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if (!StringUtility.isNullOrEmpty(filter.getTypeParam())) {
			sql.append(" and ap.ap_param_code like ? ");
			params.add(filter.getTypeParam());
		}
		if (filter.getStatus() != null) {
			sql.append(" and fbs.result = ? ");
			params.add(filter.getStatus());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and (cu.short_code like ? escape '/' ");
			sql.append(" or lower(cu.name_text) like ? escape '/' ");
			sql.append(" or unicode2english(cu.address) like ? escape '/' ");
			sql.append(" ) ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCustomerCode()).toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCustomerCode()).toLowerCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCustomerCode()).toLowerCase()));
		}
		sql.append(" group by fb.feedback_id , ");
		sql.append(" fbs.feedback_staff_id, ");
		sql.append(" st.staff_id, ");
		sql.append(" st.staff_code, ");
		sql.append(" st.staff_name, ");
		sql.append(" ap.ap_param_code, ");
		sql.append(" ap.ap_param_name, ");
		sql.append(" fb.content, ");
		sql.append(" fb.status, ");
		sql.append(" fbs.result, ");
		sql.append(" cr.staff_id, ");
		sql.append(" cr.staff_code, ");
		sql.append(" cr.staff_name, ");
		sql.append(" fb.remind_date, ");
		sql.append(" fbs.done_date ");
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from ( ");
		countSql.append(sql);
		countSql.append(" )");
		sql.append(" order by fbs.result asc, ");
		sql.append(" fb.remind_date ");
		String[] fieldNames = { 
				"feedbackId", //1
				"id", //2
				"staffId",  //3
				"staffCode", //4
				"staffName", //5
				"customerName",  //6
				"typeId",  //7
				"typeName", //8
				"content", //9
				"status", //10
				"result", //11
				"createUserId", //12
				"createCode", //13
				"createName", //14
				"remindDateStr", //15
				"doneDateStr", //16
				"item", //17
		};
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG,  //1
				StandardBasicTypes.LONG, //2
				StandardBasicTypes.LONG, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.STRING, //7
				StandardBasicTypes.STRING, //8
				StandardBasicTypes.STRING, //9
				StandardBasicTypes.INTEGER, //10
				StandardBasicTypes.INTEGER, //11
				StandardBasicTypes.LONG, //12
				StandardBasicTypes.STRING, //13
				StandardBasicTypes.STRING, //14
				StandardBasicTypes.STRING, //15
				StandardBasicTypes.STRING, //16
				StandardBasicTypes.INTEGER, //17
		};
		if (filter.getkPagingVO() != null) {
			return repo.getListByQueryAndScalarPaginated(FeedbackVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVO());
		}
		return repo.getListByQueryAndScalar(FeedbackVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<FeedbackVO> getListFeedbackIdCheckByFilter(FeedbackFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException(" FeedbackFilter is null ");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (Constant.FEEDBACK_CREATE_STAFF_ID.equals(filter.getFlagGetStaffId())) {
			sql.append(" select feedback_id as feedbackId from feedback where create_user_id = ? ");
			params.add(filter.getCreateUserId());
		} else if (Constant.FEEDBACK_OWNER_STAFF_ID.equals(filter.getFlagGetStaffId())) {
			sql.append(" select feedback_id as feedbackId from feedback_staff where staff_id = ? ");
			params.add(filter.getCreateUserId());
		} else if (Constant.FEEDBACK_CREATE_AND_OWNER_STAFF_ID.equals(filter.getFlagGetStaffId()) || filter.getFlagGetStaffId() == null) {
			sql.append(" select feedback_id as feedbackId from feedback where create_user_id = ? ");
			sql.append(" union ");
			sql.append(" select feedback_id as feedbackId from feedback_staff where staff_id = ? ");
			params.add(filter.getCreateUserId());
			params.add(filter.getCreateUserId());
		} 
		String[] fieldNames = { 
				"feedbackId", //1
		};
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG,  //1
		};
		return repo.getListByQueryAndScalar(FeedbackVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
}

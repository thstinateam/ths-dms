package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.StDisplayProgram;
import ths.dms.core.entities.StDisplayProgramExclusion;
import ths.dms.core.entities.enumtype.DisplayProgramExclusionFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface DisplayProgramExclusionDAO {

	StDisplayProgramExclusion createDisplayProgramExclusion(
			StDisplayProgramExclusion displayProgramExclusion, LogInfoVO logInfo)
			throws DataAccessException;

	void deleteDisplayProgramExclusion(
			StDisplayProgramExclusion displayProgramExclusion, LogInfoVO logInfo)
			throws DataAccessException;

	void updateDisplayProgramExclusion(
			StDisplayProgramExclusion displayProgramExclusion, LogInfoVO logInfo)
			throws DataAccessException;

	StDisplayProgramExclusion getDisplayProgramExclusionById(long id)
			throws DataAccessException;
	
	List<StDisplayProgramExclusion> getDisplayProgramExclusionByDP(
			KPaging<StDisplayProgramExclusion> kPaging,
			StDisplayProgram displayProgram) throws DataAccessException;
	
	StDisplayProgramExclusion getDisplayProgramExclusionByDisplayAndExclusion(long displayProgramId, long exclusionProgramId)
			throws DataAccessException;
	
//	void updateRemoveListDisplayProgramExclusionIn(Long displayProgramId,
//			List<Long> listDisplayProgramExclusionId)
//			throws DataAccessException;

	List<StDisplayProgramExclusion> getListDisplayProgramExclusion(
			KPaging<StDisplayProgramExclusion> kPaging,
			DisplayProgramExclusionFilter filter, StaffRole staffRole)
			throws DataAccessException;
}

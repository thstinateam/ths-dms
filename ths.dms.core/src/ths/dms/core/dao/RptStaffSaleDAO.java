package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.List;

import ths.dms.core.entities.RptStaffSale;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.filter.RptStaffSaleFilter;
import ths.dms.core.entities.vo.AmountAndAmountPlanVO;
import ths.dms.core.entities.vo.AmountPlanCustomerVO;
import ths.dms.core.entities.vo.CustomerSaleVO;
import ths.dms.core.entities.vo.NVBHSaleVO;
import ths.dms.core.entities.vo.SupervisorSaleVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface RptStaffSaleDAO {

	RptStaffSale createRptStaffSale(RptStaffSale rptStaffSale) throws DataAccessException;

	void deleteRptStaffSale(RptStaffSale rptStaffSale) throws DataAccessException;

	void updateRptStaffSale(RptStaffSale rptStaffSale) throws DataAccessException;
	
	RptStaffSale getRptStaffSaleById(long id) throws DataAccessException;
	
	BigDecimal getDayAmountPlanByNVGS(Long nvgsId) throws DataAccessException; 
	
	BigDecimal getDayAmountByNVGS(Long nvgsId) throws DataAccessException;

	BigDecimal getDayAmountPlanByNVBH(Long nvbhId) throws DataAccessException;

	BigDecimal getDayAmountByNVBH(Long nvbhId) throws DataAccessException;

	BigDecimal getDayAmountPlanByTBHV(Long tbhvId) throws DataAccessException;

	BigDecimal getDayAmountByTBHV(Long tbhvId) throws DataAccessException;

	SupervisorSaleVO getDayAmountAndDayAmountPlanByNVGS(Long nvgs)
			throws DataAccessException;
	
	BigDecimal getMonthAmountPlanByStaffType(StaffObjectType roleType,Staff staff) throws DataAccessException;
	BigDecimal getMonthAmountByStaffType(StaffObjectType roleType,Staff staff) throws DataAccessException;

	AmountAndAmountPlanVO getAmountAndAmountPlanByStaff(Staff staff, String type) throws DataAccessException;

	/**
	 * Doanh so bang tong hop RPT_STAFF_SALE ung voi supervise  truyen vao
	 * @author vuongmq
	 * @param staff
	 * @param filter
	 * @return AmountAndAmountPlanVO
	 * @throws DataAccessException
	 * @since 19/08/2015
	 */
	AmountAndAmountPlanVO getAmountAndAmountPlanByStaffSupervise(Staff staff, RptStaffSaleFilter filter) throws DataAccessException;

	/**
	 * Lay khach hang cua NVBH cho bang tong hop RPT_STAFF_SALE_DETAIL
	 * @author vuongmq
	 * @param filter
	 * @return List<AmountPlanCustomerVO>
	 * @throws DataAccessException
	 * @since 19/08/2015
	 */
	List<AmountPlanCustomerVO> getAmountPlanCustomerByStaff(RptStaffSaleFilter filter) throws DataAccessException;

	/***
	 * Lay doanh so ngay, doanh so Luy ke, cua NVBH ung voi DS KH 
	 * @author vuongmq
	 * @param kPaging
	 * @param nvbhId
	 * @return List<CustomerSaleVO>
	 * @throws DataAccessException
	 * @since 20/08/2015
	 */
	List<CustomerSaleVO> getListCustomerSaleVO(KPaging<CustomerSaleVO> kPaging, Long nvbhId, String type) throws DataAccessException;

	/**
	 * Lay doanh so ngay, doanh so Luy ke, cua NVGS ung voi DS NVBH 
	 * @author vuongmq
	 * @param kPaging
	 * @param filter
	 * @return List<NVBHSaleVO>
	 * @throws BusinessException
	 * @since 21/08/2015
	 */
	List<NVBHSaleVO> getListNVBHSaleVO(KPaging<NVBHSaleVO> kPaging,	RptStaffSaleFilter filter) throws DataAccessException;
	
}

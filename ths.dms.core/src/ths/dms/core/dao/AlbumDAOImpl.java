package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.MtMediaAlbum;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class AlbumDAOImpl implements AlbumDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Override
	public MtMediaAlbum createMtMediaAlbum(MtMediaAlbum mtMediaAlbum, LogInfoVO logInfo) throws DataAccessException {
		if (mtMediaAlbum == null) {
			throw new IllegalArgumentException("mtMediaAlbum");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.create(mtMediaAlbum);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, mtMediaAlbum, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(MtMediaAlbum.class, mtMediaAlbum.getId());
	}

	@Override
	public MtMediaAlbum getMtMediaAlbumById(Long id) throws DataAccessException {
		return repo.getEntityById(MtMediaAlbum.class, id);
	}
	@Override
	public MtMediaAlbum getMtMediaAlbumByCode(String code) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select al.* from MT_MEDIA_ALBUM al where 1 = 1");
		sql.append(" and al.MT_MEDIA_ALBUM_CODE = ? ");
		params.add(code);
		return repo.getFirstBySQL(MtMediaAlbum.class, sql.toString(), params);
	}
	
	@Override
	public void updateMtMediaAlbum(MtMediaAlbum mtMediaAlbum, LogInfoVO logInfo) throws DataAccessException {
		if (mtMediaAlbum == null) {
			throw new IllegalArgumentException("mtMediaAlbum");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		MtMediaAlbum temp = this.getMtMediaAlbumById(mtMediaAlbum.getId());
		repo.update(mtMediaAlbum);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, mtMediaAlbum, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
}

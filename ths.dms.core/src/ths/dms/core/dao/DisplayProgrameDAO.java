package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Product;
import ths.dms.core.entities.StDisplayPdGroup;
import ths.dms.core.entities.StDisplayPdGroupDtl;
import ths.dms.core.entities.StDisplayProgram;
import ths.dms.core.entities.StDisplayProgramVNM;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.DisplayProgramFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StDisplayPdGroupFilter;
import ths.dms.core.entities.enumtype.StDisplayProgramFilter;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.enumtype.StaffRoleType;
import ths.dms.core.entities.filter.ProductFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface DisplayProgrameDAO {
	
	/** 
	 * @author tientv */
	StDisplayProgram getStDisplayProgramByCode(String code) throws DataAccessException;
	/** 
	 * @author tientv */
	String checkUpdateDisplayProgramForActive(Long stDisplayProgramId) throws DataAccessException;
	/** 
	 * @author tientv */
	void updateDisplayProgram(StDisplayProgram stDisplayProgram, LogInfoVO logInfo) throws DataAccessException;
	/**
	 * Copy CTTB
	 * @author tientv
	 */
	Integer checkExistsProductDisplayGr(Long dpId,DisplayProductGroupType type) throws DataAccessException;
	/** 
	 * @author tientv */
	StDisplayProgram createDisplayProgram(StDisplayProgram stDisplayProgram, LogInfoVO logInfo) throws DataAccessException;
	
	StDisplayProgram getStDisplayProgramById(long id) throws DataAccessException;
	
	List<StDisplayProgram> getListDisplayProgramSysdateEX(List<Long> shopId, Date month)throws DataAccessException;

	Boolean checkDisplayProgrameInShop(Long displayProgrameId, Long shopId, Date month) throws DataAccessException;

	Boolean checkDisplayProgrameInShopForCustomer(Long displayProgrameId,Long shopIdOfCustomer, Date month) throws DataAccessException;
	
	
	/**************************** ****************************************/
	/**
	 * Search CTTB 2013
	 * @author thachnn
	 */
	List<StDisplayProgram> getListDisplayProgram(KPaging<StDisplayProgram> kPaging,StDisplayProgramFilter filter, 
			StaffRoleType staffRole)throws DataAccessException;
	
	StDisplayProgram getStDisplayProgramById(Long id) throws DataAccessException;
	/**
	 * Danh sach them moi CTTB Loai tru
	 * @author loctt
	 * @since 17sep2013
	 */
	List<StDisplayProgram> getListDisplayProgramForExclusion(KPaging<StDisplayProgram> kPaging, DisplayProgramFilter filter) throws DataAccessException;
	
	/**
	 * Search SPTB 2013
	 * @author thachnn
	 */
	List<StDisplayPdGroup> getListDisplayGroup(KPaging<StDisplayPdGroup> kPaging, StDisplayPdGroupFilter filter,
			StaffRoleType staffRole) throws DataAccessException;
	List<StDisplayPdGroupDtl> getListDisplayGroupDetail(KPaging<StDisplayPdGroupDtl> kPaging,  Long stDisplayPdGroupId,
			StaffRoleType staffRole) throws DataAccessException;
	void updateStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup, LogInfoVO logInfo) 
								throws DataAccessException;
	StDisplayPdGroup getStDisplayPdGroupByCode(Long stDisplayProgramId,DisplayProductGroupType type, 
			String stDisplayPdGroupCode) throws DataAccessException;
	StDisplayPdGroup createStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup, LogInfoVO logInfo)throws DataAccessException;
	void deleteStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup) throws DataAccessException;
	
	/**
	 * 19/9/2013
	 * @author lochp
	 * san pham doanh so CTTB
	 */
	List<StDisplayPdGroup> getListDisplayProgrameAmount(KPaging<StDisplayPdGroup> kPaging, StDisplayPdGroupFilter filter,
			StaffRole staffRole) throws DataAccessException;
	List<StDisplayPdGroupDtl> getListDisplayProgrameAmountProductDetail(KPaging<StDisplayPdGroupDtl> kPaging, 
			StDisplayPdGroupFilter filter, StaffRole staffRole)
			throws DataAccessException;
//	void updateStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup) throws DataAccessException;
//	StDisplayPdGroup createStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup) throws DataAccessException;
//	void deleteStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup) throws DataAccessException;
	/**
	 * @author thachnn
	 */
	List<Product> getListProduct(KPaging<Product> kPaging, 
			Long displayProgramId,String code, String name, DisplayProductGroupType type) throws DataAccessException;
	List<StDisplayProgram> getListDisplayProgramByShop(
			KPaging<StDisplayProgram> kPaging, Long shopId,
			String displayProgramCode, String displayProgramName)
			throws DataAccessException;
	
	List<StDisplayProgram> getListDisplayProgramByYear(KPaging<StDisplayProgram> kPaging, Long shopId, Integer year,
			String code, String name) throws DataAccessException;
	List<StDisplayProgram> getListDisplayProgramByListId(Long shopId, List<Long> lstId, Boolean flag, Integer year, String code,
			String name) throws DataAccessException;
	
	List<StDisplayProgramVNM> getListDisplayProgramInTowDate(KPaging<StDisplayProgramVNM> kPaging, Long shopId, Date fDate, Date tDate, String code, String name) throws DataAccessException;
	
	StDisplayProgramVNM getDisplayProgramByCode(String code) throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.StaffSaleCat;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface StaffSaleCatDAO {

	StaffSaleCat createStaffSaleCat(StaffSaleCat staffSaleCat, LogInfoVO logInfo) throws DataAccessException;

	void deleteStaffSaleCat(StaffSaleCat staffSaleCat, LogInfoVO logInfo) throws DataAccessException;

	void updateStaffSaleCat(StaffSaleCat staffSaleCat, LogInfoVO logInfo) throws DataAccessException;
	
	StaffSaleCat getStaffSaleCatById(long id) throws DataAccessException;

	List<StaffSaleCat> getListStaffSaleCat(KPaging<StaffSaleCat> kPaging,
			Long staffId) throws DataAccessException;

	Boolean checkIfRecordExist(long staffId, Long catId, Long id)
			throws DataAccessException;

	StaffSaleCat getStaffSaleCat(Long staffId, Long productInfoId)
			throws DataAccessException;
}

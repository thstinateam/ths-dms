package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.DisplayDetailVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class DisplayProgrameDetailDAOImpl implements DisplayProgrameDetailDAO {

	@Autowired
	private IRepository repo;

//	@Override
//	public DisplayProgrameDetail createDisplayProgrameDetail(DisplayProgrameDetail displayProgrameDetail) throws DataAccessException {
//		if (displayProgrameDetail == null) {
//			throw new IllegalArgumentException("displayProgrameDetail");
//		}
//		repo.create(displayProgrameDetail);
//		return repo.getEntityById(DisplayProgrameDetail.class, displayProgrameDetail.getDisplayProgrameDetailId());
//	}
//
//	@Override
//	public void deleteDisplayProgrameDetail(DisplayProgrameDetail displayProgrameDetail) throws DataAccessException {
//		if (displayProgrameDetail == null) {
//			throw new IllegalArgumentException("displayProgrameDetail");
//		}
//		repo.delete(displayProgrameDetail);
//	}
//
//	@Override
//	public DisplayProgrameDetail getDisplayProgrameDetailById(long id) throws DataAccessException {
//		return repo.getEntityById(DisplayProgrameDetail.class, id);
//	}
//	
//	@Override
//	public void updateDisplayProgrameDetail(DisplayProgrameDetail displayProgrameDetail) throws DataAccessException {
//		if (displayProgrameDetail == null) {
//			throw new IllegalArgumentException("displayProgrameDetail");
//		}
//		repo.update(displayProgrameDetail);
//	}

	@Override
	public List<DisplayDetailVO> getListCTTBbyId(KPaging<DisplayDetailVO> kPaging,Long shopId, Long idCTTB,Date date)
			throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId");
		}
		// TODO Auto-generated method stub
		StringBuilder sql=new StringBuilder();
		StringBuilder countsql=new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH lstShop AS ");
		sql.append(" (SELECT DISTINCT shop_id FROM shop START WITH shop_id = ? CONNECT BY PRIOR shop_id = parent_shop_id ");
		params.add(shopId);
		sql.append(" ) ");
		
		sql.append(" SELECT ");
		sql.append(" DP.DISPLAY_PROGRAME_DETAIL_ID AS idDisplayDetail, ");
		sql.append(" (SELECT CONCAT(CONCAT(C.DISPLAY_PROGRAME_CODE,'-'),C.DISPLAY_PROGRAME_NAME) FROM DISPLAY_PROGRAME C WHERE C.DISPLAY_PROGRAME_ID=DP.DISPLAY_PROGRAME_ID ) AS CTTB, ");
		sql.append(" (SELECT CONCAT(CONCAT(PR.PRODUCT_CODE,'-'),PR.PRODUCT_NAME) FROM PRODUCT PR WHERE PR.PRODUCT_ID=DP.PRODUCT_ID)AS nameProduct, ");
		sql.append(" (SELECT C.DISPLAY_PROGRAME_CODE FROM DISPLAY_PROGRAME C WHERE C.DISPLAY_PROGRAME_ID=DP.DISPLAY_PROGRAME_ID ) AS displayCode, ");
		sql.append(" (SELECT PR.PRODUCT_CODE FROM PRODUCT PR WHERE PR.PRODUCT_ID=DP.PRODUCT_ID)AS productCode, ");
		sql.append(" DP.QUANTITY AS countCTTB, ");
		sql.append(" DIS.DISPLAY_PROGRAME_ID, ");
		sql.append(" (case ");
		sql.append(" WHEN (trunc(DIS.FROM_DATE)<=trunc(SYSDATE) AND trunc(DIS.TO_DATE)>=trunc(SYSDATE)) THEN 1 ");
		sql.append(" ELSE 0 ");
		sql.append(" END)as isTrue ");
		sql.append(" FROM DISPLAY_PROGRAME_DETAIL DP,DISPLAY_PROGRAME DIS ");
		sql.append(" WHERE DIS.DISPLAY_PROGRAME_ID=DP.DISPLAY_PROGRAME_ID AND DIS.STATUS=1 ");
		
		if(idCTTB!=null){
			sql.append(" AND DP.DISPLAY_PROGRAME_ID =? ");
			params.add(idCTTB);
		}
		if(date!=null){
			sql.append(" AND TRUNC(DIS.from_date,'MM') <= TRUNC(?,'MM') ");
			params.add(date);
			sql.append(" AND (DIS.to_date is null or TRUNC(DIS.to_date,'MM') >= TRUNC(?,'MM')) ");
			params.add(date);
		}
		sql.append(" and DP.quantity > 0");
		sql.append(" and EXISTS (select 1 from display_programe_shop_map ds ");
		sql.append(" where DIS.display_programe_id = ds.display_programe_id and ds.status =1 ");
		sql.append(" and ds.shop_id in (SELECT shop_id FROM lstShop) ");
		sql.append(" ) ");
		
		countsql.append(" SELECT count(*) as count from( ");
		countsql.append(sql);
		countsql.append(" ) ");
		
		sql.append(" ORDER BY CTTB,nameProduct ");
		
		 String[] fieldNames = { 
					"idDisplayDetail",//1
					"CTTB",//2
					"nameProduct",//3
					"displayCode",//4
					"productCode",//5
					"countCTTB",// 6
					"isTrue"//7
					};

		Type[] fieldTypes = { 
					StandardBasicTypes.LONG, // 1
					StandardBasicTypes.STRING, // 2
					StandardBasicTypes.STRING,//3
					StandardBasicTypes.STRING, // 4
					StandardBasicTypes.STRING, // 5
					StandardBasicTypes.STRING, // 6
					StandardBasicTypes.INTEGER//7
					};
		if(kPaging!=null){
			return repo.getListByQueryAndScalarPaginated(DisplayDetailVO.class, fieldNames, fieldTypes, sql.toString(), countsql.toString(), params, params, kPaging);
		}else{
			return repo.getListByQueryAndScalar(DisplayDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

//	@Override
//	public DisplayProgrameDetail getDisplayProgrameDetail(
//			DisplayProgrameDetail dpPro) throws DataAccessException {
//		// TODO Auto-generated method stub
//		StringBuilder sql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		 
//		sql.append(" SELECT * FROM DISPLAY_PROGRAME_DETAIL dp ");
//		sql.append(" WHERE 1=1 ");
//		sql.append(" AND DP.DISPLAY_PROGRAME_ID=? ");
//		params.add(dpPro.getDisplayPrograme().getDisplayProgrameId());
//		sql.append(" AND DP.PRODUCT_ID=? ");
//		params.add(dpPro.getProduct().getProductId());
//		return repo.getEntityBySQL(DisplayProgrameDetail.class, sql.toString(), params);
//	}
}

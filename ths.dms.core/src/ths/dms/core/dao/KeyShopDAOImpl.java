package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.KS;
import ths.dms.core.entities.KSCusProductReward;
import ths.dms.core.entities.KSCustomer;
import ths.dms.core.entities.KSCustomerLevel;
import ths.dms.core.entities.KSLevel;
import ths.dms.core.entities.KSProduct;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.RewardType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.KeyShopFilter;
import ths.dms.core.entities.filter.SaleOrderPromoDetailFilter;
import ths.dms.core.entities.vo.AllocateShopVO;
import ths.dms.core.entities.vo.KSCusProductRewardDoneVO;
import ths.dms.core.entities.vo.KeyShopCustomerHistoryVO;
import ths.dms.core.entities.vo.KeyShopVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.TotalRewardMoneyVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class KeyShopDAOImpl implements KeyShopDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	private CommonDAO commonDAO;

	private String getColumnSort(String sortField, String [] arrTableName) {
		if (!StringUtility.isNullOrEmpty(sortField) && arrTableName != null && arrTableName.length > 0) {
			for (int i = 0, sz = arrTableName.length; i < sz; i++) {
				String fieldName = arrTableName[i];
				if (!StringUtility.isNullOrEmpty(fieldName) && fieldName.toLowerCase().equals(sortField.trim().toLowerCase())) {
					return sortField;
				}
			}
		}
		return null;
	}
	
	private String getOrderBy(String order) {
		if (!StringUtility.isNullOrEmpty(order)) {
			if (ths.dms.core.common.utils.Constant.SORT_ASC.equals(order.trim().toLowerCase()) || ths.dms.core.common.utils.Constant.SORT_DESC.equals(order.trim().toLowerCase())) {
				return order;
			}
		}
		return null;
	}
	
	@Override
	public KS createKS(KS ks, LogInfoVO logInfo) throws DataAccessException {
		if (ks == null) {
			throw new IllegalArgumentException("ks");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (ks.getKsCode() != null)
			ks.setKsCode(ks.getKsCode().toUpperCase());
		if (ks.getName() != null)
			ks.setNameText(Unicode2English.codau2khongdau(ks.getName()).toUpperCase());
		ks.setCreateDate(commonDAO.getSysDate());
		ks.setCreateUser(logInfo.getStaffCode());
		repo.create(ks);
		ks = repo.getEntityById(KS.class, ks.getId());
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, ks, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return ks;
	}

	@Override
	public void deleteKS(KS ks, LogInfoVO logInfo) throws DataAccessException {
		if (ks == null) {
			throw new IllegalArgumentException("ks");
		}
		repo.delete(ks);
		try {
			actionGeneralLogDAO.createActionGeneralLog(ks, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public KS getKSById(long id) throws DataAccessException {
		return repo.getEntityById(KS.class, id);
	}
	
	@Override
	public KSLevel getKSLevelById(long id) throws DataAccessException {
		return repo.getEntityById(KSLevel.class, id);
	}

	@Override
	public void updateKS(KS ks, LogInfoVO logInfo) throws DataAccessException {
		if (ks == null) {
			throw new IllegalArgumentException("ks");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (ks.getKsCode() != null)
			ks.setKsCode(ks.getKsCode().toUpperCase());
		if (ks.getName() != null)
			ks.setNameText(Unicode2English.codau2khongdau(ks.getName()).toUpperCase());
		ks.setUpdateDate(commonDAO.getSysDate());
		ks.setUpdateUser(logInfo.getStaffCode());
		KS temp = this.getKSById(ks.getId());
		repo.update(ks);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, ks, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public KS getKSByCode(String code)
			throws DataAccessException {
		code = code.toUpperCase();
		StringBuilder sql = new StringBuilder();
		sql.append("select * from ks where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and ks_code=? ");
			params.add(code.toUpperCase());
		}
		sql.append(" and status != ?");
		params.add(ActiveType.DELETED.getValue());
		return repo.getEntityBySQL(KS.class, sql.toString(), params);
	}
	
	@Override
	public KSLevel getKSLevelByCode(String levelCode, String ksCode)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from ks_level where 1=1 ");
		if (!StringUtility.isNullOrEmpty(levelCode)) {
			sql.append(" and ks_level_code=? ");
			params.add(levelCode.toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(ksCode)) {
			sql.append(" and ks_id in (select ks_id from ks where status != ? and ks_code = ?)");
			params.add(ActiveType.DELETED.getValue());
			params.add(ksCode.toUpperCase());
		}
		
		sql.append(" and status != ?");
		params.add(ActiveType.DELETED.getValue());
		return repo.getEntityBySQL(KSLevel.class, sql.toString(), params);
	}
	
	@Override
	public List<KeyShopVO> getListKeyShopVOByFilter(KeyShopFilter filter) throws DataAccessException {
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("getShopId is null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH r_ks_shopmap AS ( ");
		sql.append(" select shop_id from shop sh ");
		sql.append(" where sh.status = ? ");
		sql.append(" start with sh.shop_id = ? connect by prior sh.shop_id = sh.parent_shop_id ");
		sql.append(" union all ");
		sql.append(" select shop_id from shop sh ");
		sql.append(" where sh.status = ? ");
		sql.append(" start with sh.shop_id = ? connect by sh.shop_id = prior sh.parent_shop_id) ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(filter.getShopId());
		params.add(ActiveType.RUNNING.getValue());
		params.add(filter.getShopId());
		sql.append(" select distinct ks.ks_id ksId, ks.ks_code ksCode, ks.name ksName, ks.status, ks.description, ");
		sql.append(" to_char((case when fc.num > 9 then to_char(fc.num) else '0'||to_char(fc.num) end)||'/'||(to_char(fc.year,'yyyy'))) fromCycle, ");
		sql.append(" to_char((case when tc.num > 9 then to_char(tc.num) else '0'||to_char(tc.num) end)||'/'||(to_char(tc.year,'yyyy'))) toCycle, ");
		sql.append(" Extract(year from fc.year) fromCycleYear, ");
		sql.append(" Extract(year from tc.year) toCycleYear, ");
		sql.append(" ks.PROGRAM_TYPE ksType, ks.MIN_PHOTO_NUM minPhoto, ks.from_cycle_id fromCycleId, ks.to_cycle_id toCycleId ");
		sql.append(" from ks ");
		sql.append(" join cycle fc on fc.cycle_id = ks.from_cycle_id ");
		sql.append(" join cycle tc on tc.cycle_id = ks.to_cycle_id ");
		/*sql.append(" join ( select distinct rp.ks_id from r_ks_shopmap rp ");
		
		if (StringUtility.isNullOrEmpty(filter.getStrLstShopId()) && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on rp.shop_id = ogr.number_key_id  ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getStrLstShopId())) {
			sql.append(" where 1=1 ");
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrLstShopId(), "rp.shop_id");
			sql.append(paramsFix);
		}
		sql.append(" ) ksm on ks.ks_id = ksm.ks_id ");*/
		sql.append(" left join ks_shop_map ksm on ks.ks_id = ksm.ks_id ");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCodeAndName())) {
			String temp = StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCodeAndName().toLowerCase()));
			sql.append(" and (ks.ks_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCodeAndName().toUpperCase()));
			sql.append(" 		or lower(ks.name_text) like ? ESCAPE '/' )");
			params.add(temp);
		}
		if (filter.getStatus() != null) {
			sql.append(" and ks.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and ks.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getYear() != null) {
			if (filter.getNum() != null) {
				sql.append(" and (to_char(fc.year,'yyyy') < ? or (to_char(fc.year,'yyyy') = ? and fc.num <= ?))");
				sql.append(" and (to_char(tc.year,'yyyy') > ? or (to_char(tc.year,'yyyy') = ? and tc.num >= ?))");
				params.add(filter.getYear().toString());
				params.add(filter.getYear().toString());
				params.add(filter.getNum());
				params.add(filter.getYear().toString());
				params.add(filter.getYear().toString());
				params.add(filter.getNum());
			} else {
				sql.append(" and to_char(fc.year,'yyyy') <= ? and ? <= to_char(tc.year,'yyyy') ");
				params.add(filter.getYear().toString());
				params.add(filter.getYear().toString());
			}
		}
		if (filter.getKsType() != null) {
			sql.append(" and ks.PROGRAM_TYPE = ? ");
			params.add(filter.getKsType());
		}
		sql.append(" and (ksm.shop_id in (select  * from r_ks_shopmap)  ");
		sql.append("    or (ksm.shop_id is null and upper(ks.create_user) = (select upper(staff_code) from staff where staff_id = ?))");
		sql.append("  ) ");
		params.add(filter.getStaffRootId());
		countSql.append(" select count(1) count from (").append(sql.toString()).append(")");
		
		final String [] fieldNames = new String [] {"ksId", "ksCode", "ksName", 
				"status", "description", "fromCycle",
				"toCycle", "fromCycleYear",
				"toCycleYear", "ksType", "minPhoto",
				"fromCycleId", "toCycleId"};
		String sort = this.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			sql.append(" order by ").append(sort).append(" ");
			String order = this.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				sql.append(order);
			}
		} else {
			sql.append(" order by ksCode ");
		}
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG, StandardBasicTypes.LONG
				};
		if (filter.getkPagingVO() == null) {
			return repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		return repo.getListByQueryAndScalarPaginated(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVO());

	}
	
	@Override
	public Boolean checkExistsCustomerNotInKSCycle(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select kc.ks_customer_id ksCustomerId from ks_customer kc ");
		sql.append(" join cycle c on c.cycle_id = kc.cycle_id ");
		sql.append(" where kc.status != ? ");
		params.add(ActiveType.DELETED.getValue());
		if (filter.getKsId() != null) {
			sql.append(" and kc.ks_id = ? ");
			params.add(filter.getKsId());
		}
		if (filter.getBeginDate() != null && filter.getEndDate() != null) {
			sql.append(" and (c.begin_date < trunc(?) or c.end_date >= trunc(?) + 1)");
			params.add(filter.getBeginDate());
			params.add(filter.getEndDate());
		}
		final String[] fieldNames = new String[] {  "ksCustomerId" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG };
		List<KeyShopVO> lst = repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst != null && lst.size() > 0 ? true : false;

	}
	
	@Override
	public Boolean checkExistsCustomerNotInKSLevel(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select kcl.ks_customer_level_id ksCustomerLevelId from ks_customer_level kcl ");
		sql.append(" where kcl.status != ? ");
		params.add(ActiveType.DELETED.getValue());
		if (filter.getKsLevelId() != null) {
			sql.append(" and kcl.KS_LEVEL_ID = ? ");
			params.add(filter.getKsLevelId());
		}
		final String[] fieldNames = new String[] {  "ksCustomerLevelId" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG };
		List<KeyShopVO> lst = repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst != null && lst.size() > 0 ? true : false;

	}
	
	@Override
	public Boolean checkExistsRewardInKS(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select kc.ks_customer_id ksCustomerId from ks_customer kc ");
		sql.append(" where kc.status != ? and kc.reward_type is not null ");
		params.add(ActiveType.DELETED.getValue());
		if (filter.getKsId() != null) {
			sql.append(" and kc.ks_id = ? ");
			params.add(filter.getKsId());
		}
		final String[] fieldNames = new String[] {  "ksCustomerId" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG };
		List<KeyShopVO> lst = repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst != null && lst.size() > 0 ? true : false;

	}
	@Override
	public KSCustomer getKSCustomerById(long id) throws DataAccessException {
		return repo.getEntityById(KSCustomer.class, id);
	}
	
	@Override
	public List<KSProduct> getListKSProductByFilter(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select kp.* from ks_product kp where 1=1 ");
		if (filter.getStatus() != null) {
			sql.append(" and kp.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and kp.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getProductId() != null) {
			sql.append(" and kp.product_id = ? ");
			params.add(filter.getProductId());
		}
		if (filter.getKsId() != null) {
			sql.append(" and kp.ks_id = ? ");
			params.add(filter.getKsId());
		}
		if (filter.getkPagingP() == null) {
			return repo.getListBySQL(KSProduct.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(KSProduct.class, sql.toString(), params, filter.getkPagingP());
	}
	
	@Override
	public List<AllocateShopVO> getListAllocateShop(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT ksm.KS_ID ksId, s2.shop_code areaCode, s1.shop_code zoneCode, s.shop_code shopCode, s.shop_name shopName ");
		sql.append(" FROM KS_SHOP_MAP ksm ");
		sql.append(" JOIN SHOP s on ksm.SHOP_ID = s.SHOP_ID ");
		if (StringUtility.isNullOrEmpty(filter.getStrLstShopId()) && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s.shop_id = ogr.number_key_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		sql.append(" JOIN SHOP_TYPE st ON st.SHOP_TYPE_ID = s.SHOP_TYPE_ID ");
		sql.append(" LEFT JOIN SHOP s1 ON s.PARENT_SHOP_ID = s1.SHOP_ID ");
		sql.append(" LEFT JOIN SHOP s2 ON s1.PARENT_SHOP_ID    = s2.SHOP_ID ");
		sql.append(" WHERE 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getStrLstShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrLstShopId(), "s.SHOP_ID");
			sql.append(paramsFix);
		}
		sql.append(" AND st.SPECIFIC_TYPE = ? ");
		params.add(ShopSpecificType.NPP.getValue());
		if (filter.getKsId() != null) {
			sql.append(" AND ksm.KS_ID = ? ");
			params.add(filter.getKsId());
		}
		if (filter.getLstKsId() != null) {
			sql.append(" and ksm.KS_ID in (-1 ");
			for (Long id : filter.getLstKsId()) {
				sql.append(", ?");
				params.add(id);
			}
			sql.append(" ) ");
		}
		sql.append(" AND ksm.STATUS = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   CONNECT BY prior s.PARENT_SHOP_ID = s.SHOP_ID ");
		sql.append(" ORDER BY ksId, areaCode, zoneCode, shopCode ");
		
		final String[] fieldNames = new String[] {"ksId", "areaCode", "zoneCode", "shopCode", "shopName"};
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING
		};
		return repo.getListByQueryAndScalar(AllocateShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<KSCustomer> getListKSCustomerByFilter(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select kc.* from ks_customer kc where 1=1 ");
		if (filter.getStatus() != null) {
			sql.append(" and kc.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and kc.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getCustomerId() != null) {
			sql.append(" and kc.customer_id = ? ");
			params.add(filter.getCustomerId());
		}
		if (filter.getKsId() != null) {
			sql.append(" and kc.ks_id = ? ");
			params.add(filter.getKsId());
		}
		if (filter.getCycleId() != null) {
			sql.append(" and kc.cycle_id = ? ");
			params.add(filter.getCycleId());
		}
		if (filter.getShopId() != null) {
			sql.append(" and kc.shop_id = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getkPagingC() == null) {
			return repo.getListBySQL(KSCustomer.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(KSCustomer.class, sql.toString(), params, filter.getkPagingC());
	}
	
	@Override
	public List<KSCustomerLevel> getListKSCustomerLevelByFilter(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select kcl.* from ks_customer_level kcl where 1=1 ");
		if (filter.getStatus() != null) {
			sql.append(" and kcl.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and kcl.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getKsCustomerId() != null) {
			sql.append(" and kcl.ks_customer_id = ? ");
			params.add(filter.getKsCustomerId());
		}
		if (filter.getKsLevelId() != null) {
			sql.append(" and kcl.ks_level_id = ? ");
			params.add(filter.getKsLevelId());
		}
		if (filter.getkPagingCL() == null) {
			return repo.getListBySQL(KSCustomerLevel.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(KSCustomerLevel.class, sql.toString(), params, filter.getkPagingCL());
	}
	
	@Override
	public List<KSCusProductReward> getListKSCusProductRewardByFilter(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select kcpr.* from ks_cus_product_reward kcpr where 1=1 ");
		if (filter.getStatus() != null) {
			sql.append(" and kcpr.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and kcpr.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getKsCustomerId() != null) {
			sql.append(" and kcpr.ks_customer_id = ? ");
			params.add(filter.getKsCustomerId());
		}
		if (filter.getLstKsCustomerId() != null) {
			sql.append(" and kcpr.KS_CUSTOMER_ID in (-1 ");
			for (int i = 0, n = filter.getLstKsCustomerId().size(); i < n; i++) {
				sql.append(", ?");
				params.add(filter.getLstKsCustomerId().get(i));
			}
			sql.append(" ) ");
		}
		if (filter.getProductId() != null) {
			sql.append(" and kcpr.product_id = ? ");
			params.add(filter.getProductId());
		}
		if (filter.getkPagingCPR() == null) {
			return repo.getListBySQL(KSCusProductReward.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(KSCusProductReward.class, sql.toString(), params, filter.getkPagingCPR());
	}
	
	@Override
	public List<Shop> getListShopForKSByFilter(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select s.* from shop s where 1=1 ");
		if (filter.getStatus() != null) {
			sql.append(" and s.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and s.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getShopId() != null) {
			sql.append(" and s.shop_id = ? ");
			params.add(filter.getShopId());
		}
		sql.append(" and s.shop_id in (select shop_id from shop start with shop_id in (select shop_id from ks_shop_map where status = 1 ");
		if (filter.getKsId() != null) {
			sql.append(" and ks_id = ? ");
			params.add(filter.getKsId());
		}
		sql.append(" ) connect by prior shop_id = parent_shop_id and status = 1) ");
		
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	@Override
	public void removeShopMapByShop(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (filter.getShopId() == null || filter.getKsId() == null) {
			return;
		}

		sql.append("update ks_shop_map set update_user=?, update_date=sysdate,status=? ");
		params.add(filter.getUpdateUser());
		params.add(ActiveType.DELETED.getValue());
		sql.append(" where ks_id = ? ");
		params.add(filter.getKsId());
		if (Boolean.TRUE.equals(filter.getIsGetChild())) {
			sql.append(" and shop_id in (select shop_id from shop start with parent_shop_id = ? connect by prior shop_id=parent_shop_id) ");
			params.add(filter.getShopId());
		} else {//xoa luon shop
			sql.append(" and shop_id in (select shop_id from shop start with shop_id = ? connect by prior parent_shop_id=shop_id) ");
			params.add(filter.getShopId());
		}
		repo.executeSQLQuery(sql.toString(), params);
	}
	
	@Override
	public Boolean checkKSCustomerExistsShop(KeyShopFilter filter) throws DataAccessException {
		if (filter.getShopId() == null || filter.getKsId() == null) {
			throw new DataAccessException("shopId & ksId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with lstParent as ( ");
		sql.append("     select shop_id from shop start with shop_id = ? connect by prior parent_shop_id=shop_id ");
		params.add(filter.getShopId());
		sql.append(" ), ");
		sql.append(" lstShopMap as ( ");
		sql.append("     select shop_id from ks_shop_map ksm where status = 1");
		sql.append(" 	 and ks_id = ? ");
		params.add(filter.getKsId());
		sql.append("     and shop_id in (select shop_id from lstParent) and rownum=1 ");
		sql.append(" ), ");
		sql.append(" lstChildParent as ( ");
		sql.append("     select distinct shop_id from shop where status = 1 start with parent_shop_id = (select shop_id from lstShopMap) ");
		sql.append("     connect by prior shop_id=parent_shop_id ");
		sql.append(" ), ");
		sql.append(" lstChild as ( ");
		sql.append("     select distinct shop_id from shop start with parent_shop_id = ? connect by prior shop_id=parent_shop_id ");
		params.add(filter.getShopId());
		sql.append(" ), ");
		sql.append(" lstShop as ( ");
		sql.append("     select * from ( ");
		sql.append("         select shop_id from lstChildParent minus select shop_id from lstChild ");
		sql.append("     ) where shop_id != ? ");
		params.add(filter.getShopId());
		sql.append(" ) ");
		sql.append(" select ks_customer_id ksCustomerId from ks_customer kc "); 
		sql.append(" where ks_id = ? and kc.shop_id in (select shop_id from lstShop) ");
		params.add(filter.getKsId());
		final String[] fieldNames = new String[] {  "ksCustomerId" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG };
		List<KeyShopVO> lst = repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst != null && lst.size() > 0 ? true : false;
	}
	
	@Override
	public Boolean checkKSCustomerExistsShopDel(KeyShopFilter filter) throws DataAccessException {
		if (filter.getShopId() == null || filter.getKsId() == null) {
			throw new DataAccessException("shopId & ksId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH lstChildShop AS");
//		sql.append("     (SELECT shop_id  FROM shop  Where status=1  START WITH shop_id = ? CONNECT BY prior shop_id = parent_shop_id");
		sql.append("     (SELECT shop_id  FROM shop  Where status=1  START WITH shop_id = ? CONNECT BY prior parent_shop_id = shop_id");
		params.add(filter.getShopId());
		sql.append(" ), ");
		sql.append(" lstShopMap AS ");
		sql.append("     (SELECT shop_id  FROM ks_shop_map ksm  WHERE status = 1  AND ks_id    = ? ");
		params.add(filter.getKsId());
		sql.append("  AND shop_id IN    (SELECT shop_id FROM lstChildShop    )  AND rownum=1  ) ");

		sql.append("  , lstChildShopMap AS ");
		sql.append("    (SELECT shop_id  FROM shop  WHERE status  =1");
		sql.append("    START WITH shop_id  IN (select shop_id from lstChildShop) ");
//		sql.append("   CONNECT BY prior shop_id=parent_shop_id  )");
		sql.append("   CONNECT BY prior parent_shop_id = shop_id  )");
		sql.append("  SELECT ks_customer_id ksCustomerId FROM ks_customer kc WHERE ks_id     = ?");
		params.add(filter.getKsId());
		sql.append("  AND kc.shop_id IN  (SELECT shop_id FROM lstChildShopMap  ) and kc.status IN (1)");

		final String[] fieldNames = new String[] { "ksCustomerId" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG };
		List<KeyShopVO> lst = repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst != null && lst.size() > 0 ? true : false;
	}
	
	@Override
	public List<KeyShopVO> getListKSProductVOByFilter(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select ");
		if (filter.getKsId() != null || filter.getLstKsId() != null) {
			sql.append(" ksp.ks_id ");
		} else {
			sql.append(" 0 ");
		}
		sql.append(" as ksId, p.product_id id, p.product_id productId, p.product_code productCode, p.product_name productName, ");
		sql.append(" (select product_info_name from product_info where product_info_id=cat_id) cat, ");
		sql.append(" (select product_info_name from product_info where product_info_id=sub_cat_id) subCat, ");
		sql.append(" (select product_info_name from product_info where product_info_id=brand_id) brand, ");
		sql.append(" (select product_info_name from product_info where product_info_id=flavour_id) flavour, ");
		sql.append(" (select product_info_name from product_info where product_info_id=packing_id) packing ");
		sql.append(" from product p ");
		if (filter.getKsId() != null || filter.getLstKsId() != null) {
			sql.append(" Join ks_product ksp on p.product_id = ksp.product_id ");
		}
		sql.append(" where p.status != ? ");
		params.add(ActiveType.DELETED.getValue());
		if (filter.getStatus() != null) {
			sql.append(" and p.status = ? ");
			params.add(filter.getStatus());
		}
		if (filter.getKsId() != null) {
			sql.append(" and ksp.ks_id = ? and ksp.status != ? ");
			params.add(filter.getKsId());
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getLstKsId() != null) {
			sql.append(" and ksp.ks_id in (-1 ");
			for (Long id : filter.getLstKsId()) {
				sql.append(",?");
				params.add(id);
			}
			sql.append(" ) ");
			sql.append(" and ksp.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getExceptKSId() != null) {
			sql.append(" and p.product_id not in (select product_id from ks_product where ks_id = ? and status != ?) ");
			params.add(filter.getExceptKSId());
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCodeAndName())) {
			sql.append(" and (p.product_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCodeAndName().toUpperCase()));
			String temp = StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCodeAndName().toLowerCase()));
			sql.append(" or UNICODE2ENGLISH(p.product_name) like ? ESCAPE '/' )");
			params.add(temp);
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and p.product_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			String temp = StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getName().toLowerCase()));
			sql.append(" and UNICODE2ENGLISH(p.product_name) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (filter.getLstProductInfoId() != null) {
			sql.append(" and p.cat_id in (-1 ");
			for (Long id : filter.getLstProductInfoId()) {
				sql.append(",?");
				params.add(id);
			}
			sql.append(" ) ");
		}
		if (filter.getLstProductInfoChildId() != null) {
			sql.append(" and p.sub_cat_id in (-1 ");
			for (Long id : filter.getLstProductInfoChildId()) {
				sql.append(",?");
				params.add(id);
			}
			sql.append(" ) ");
		}

		countSql.append(" select count(1) count from (").append(sql.toString()).append(")");

		final String [] fieldNames = new String [] {"ksId", "id", "productId", "productCode", 
				"productName", "cat", "subCat",
				"brand", "flavour", "packing"};
		String sort = this.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			sql.append(" order by ").append(sort).append(" ");
			String order = this.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				sql.append(order);
			}
		} else {
			sql.append(" order by productCode ");
		}
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				};
		if (filter.getkPagingVO() == null)
			return repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVO());

	}
	
	@Override
	public List<KeyShopVO> getListKSLevelVOByFilter(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select kl.ks_id ksId, kl.ks_level_id ksLevelId, kl.ks_level_code ksLevelCode, kl.name ksLevelName, ");
		sql.append(" kl.amount, kl.quantity ");
		sql.append(" from ks_level kl ");
		sql.append(" where kl.status != ? ");
		params.add(ActiveType.DELETED.getValue());
		if (filter.getKsId() != null) {
			sql.append(" and kl.ks_id = ? ");
			params.add(filter.getKsId());
		}
		if (filter.getLstKsId() != null) {
			sql.append(" and kl.ks_id in (-1 ");
			for (Long id : filter.getLstKsId()) {
				sql.append(",?");
				params.add(id);
			}
			sql.append(" ) ");
		}
		
		countSql.append(" select count(1) count from (").append(sql.toString()).append(")");
		
		final String [] fieldNames = new String [] {"ksId", "ksLevelId", "ksLevelCode", "ksLevelName", 
				"amount", "quantity"};
		String sort = this.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			sql.append(" order by ").append(sort).append(" ");
			String order = this.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				sql.append(order);
			}
		} else {
			sql.append(" order by ksLevelCode ");
		}
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER
				};
		if (filter.getkPagingVO() == null)
			return repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVO());

	}
	
	@Override
	public List<KeyShopVO> getListKSCustomerLevelVOByFilter(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" Select ksLevelId, ksLevelCode, ksLevelName, amount, quantity, multiplier");
		sql.append(" from  ");
		sql.append(" (SELECT kl.ks_level_id ksLevelId, kl.ks_level_code ksLevelCode, kl.name ksLevelName, kl.amount, nvl(kl.quantity,0) quantity  ");
		sql.append(" FROM ks_level kl WHERE 1=1");
		if (filter.getKsId() != null) {
			sql.append(" and kl.ks_id = ? ");
			params.add(filter.getKsId());
		}
		sql.append(" and  kl.status <> ?) tempLevel  ");
		params.add(ActiveType.DELETED.getValue());
		sql.append(" left join (select ks_level_id, multiplier from ks_customer_level kscl  ");
		sql.append(" join  ks_customer ksc on (kscl.ks_customer_id = ksc.ks_customer_id and ksc.status in (1,2,3)) and ksc.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("  where 1 = 1  ");
		
		if (filter.getCustomerId() != null) {
			sql.append(" and ksc.customer_id=? ");
			params.add(filter.getCustomerId());
		} else {
			sql.append(" and ksc.customer_id=? ");
			params.add("-1");
		}
		if (filter.getCycleId() != null) {
			sql.append(" and ksc.cycle_id = ? ");
			params.add(filter.getCycleId());
		}
		sql.append("  and kscl.status in (1) ) tmpMulti ");
		sql.append("  on (tempLevel.ksLevelId=tmpMulti.ks_level_id) ");
		sql.append("  ORDER BY ksLevelCode  ");
		
		countSql.append(" select count(1) count from (").append(sql.toString()).append(")");
		final String[] fieldNames = new String[] { 
				"ksLevelId", "ksLevelCode", "ksLevelName", 
				"amount", "quantity", "multiplier"
				}; 
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER
				};
		if (filter.getkPagingVO() == null)
			return repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVO());

	}
	
	@Override
	public List<KeyShopVO> getListKSCustomerVOByFilter(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select  ");
		if (Boolean.TRUE.equals(filter.getIsGetParentShop())) {
			sql.append("  s2.SHOP_CODE areaCode, s1.SHOP_CODE zoneCode, ");
		} else {
			sql.append("  '' areaCode, '' zoneCode, ");
		}
		sql.append("  ksc.ks_customer_id ksCustomerId, s.shop_code shopCode, s.SHOP_NAME shopName, c.short_code customerCode,  ");
		sql.append("  c.customer_name customerName, aa.province_name provinceName, aa.district_name districtName, aa.precinct_name precinctName, c.street, c.housenumber, ksc.cycle_id cycleId, ksc.reward_type rewardType, ksc.customer_id customerId,");
		sql.append("  c.address as address, ");
		sql.append("  (select listagg(CAST(tmpCode AS VARCHAR2(2000)), ' + ') within group (Order by ks_customer_id, amount desc) ");
		sql.append("  from (select tmpLevel.ks_customer_id, amount, (count || tmpLevel.ks_level_code) tmpCode ");
		sql.append("  from ( ");
		sql.append("  select kscl.ks_customer_id,ksl.ks_level_code, ksl.amount , SUM(multiplier) count ");
		sql.append("  from ks_customer_level kscl ");
		sql.append("  inner join ks_level ksl on kscl.ks_level_id=ksl.ks_level_id ");
		sql.append("  where kscl.status=1 ");
		sql.append("  group by ksl.ks_level_code, kscl.ks_customer_id,ksl.amount ");
		sql.append("  ) tmpLevel) cl ");
		sql.append("  where cl.ks_customer_id=ksc.ks_customer_id) levelCustomer, ");
		sql.append("  ksc.status status ");
		sql.append("  from ks_customer ksc ");
		sql.append("  join shop s on ksc.shop_id = s.shop_id ");
		
		if (!StringUtility.isNullOrEmpty(filter.getStrLstShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrLstShopId(), "s.shop_id");
			sql.append(paramsFix);
		}
		
		if (StringUtility.isNullOrEmpty(filter.getStrLstShopId()) && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			sql.append("  join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s.shop_id = ogr.number_key_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		
		if (Boolean.TRUE.equals(filter.getIsGetParentShop())) {
			sql.append("  LEFT JOIN shop s1 ON s.parent_shop_id = s1.shop_id ");
			sql.append("  LEFT JOIN shop s2 ON s1.parent_shop_id = s2.shop_id ");
		}
		sql.append("  inner join customer c on ksc.customer_id=c.customer_id ");
		sql.append("  left join area aa on c.area_id = aa.area_id ");
		sql.append("  join cycle tc on tc.cycle_id = ksc.cycle_id ");
		sql.append("  where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCodeAndName())) {
			String temp = StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCodeAndName().toUpperCase()));
			sql.append(" and (c.customer_code like ? ESCAPE '/'");
			params.add(temp);
			sql.append(" 		or upper(c.name_text) like ? ESCAPE '/' ");
			params.add(temp);
			sql.append(" 		or upper(UNICODE2ENGLISH(c.address)) like ? ESCAPE '/' )");
			params.add(temp);
		}
		if (filter.getStatus() != null) {
			sql.append(" and ksc.status = ? ");
			params.add(filter.getStatus());
		}
		else sql.append(" and ksc.status in (1,2,3) ");
		if (filter.getYear() != null) {
			if (filter.getNum() != null) {
				sql.append(" and (to_char(tc.year,'yyyy') = ? and tc.num = ?)");
				params.add(filter.getYear().toString());
				params.add(filter.getNum());
			} else {
				sql.append(" and ? = to_char(tc.year,'yyyy') ");
				params.add(filter.getYear().toString());
			}
		}
		if (filter.getShopId() != null) {
			sql.append(" and ksc.shop_id in (select shop_id from shop START WITH shop_id = ? ");
			sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id) ");
			params.add(filter.getShopId());
		}
		if (filter.getKsId() != null) {
			sql.append(" and ksc.ks_id = ? ");
			params.add(filter.getKsId());
		}
		countSql.append(" select count(1) count from (").append(sql.toString()).append(")");
		final String[] fieldNames = new String[] { 
				"areaCode", "zoneCode",
				"ksCustomerId","shopCode", "shopName", "customerCode", "customerName", 
				"provinceName", "districtName", "precinctName", "street", "housenumber",
				"address", 
				"levelCustomer","status", "cycleId", 
				"rewardType", "customerId"
				}; 
		String sort = this.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			sql.append(" order by ").append(sort).append(" ");
			String order = this.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				sql.append(order);
			}
		} else {
			sql.append(" order by shopCode ");
		}
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER, StandardBasicTypes.LONG
				};
		if (filter.getkPagingVO() == null)
			return repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVO());

	}
	
	@Override
	public List<KeyShopVO> getListKSRewardVOByFilter(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select  ");
		if (Boolean.TRUE.equals(filter.getIsGetParentShop())) {
			sql.append("  s2.SHOP_CODE areaCode, s1.SHOP_CODE zoneCode, ");
		} else {
			sql.append("  '' areaCode, '' zoneCode, ");
		}
		sql.append("  ksc.ks_customer_id id, s.shop_code shopCode, s.SHOP_NAME shopName, c.short_code customerCode,  ");
		sql.append("  c.customer_name customerName,c.address address, ksc.cycle_id cycleId, ksc.customer_id customerId,");
		sql.append("  (select listagg(CAST(tmpCode AS VARCHAR2(2000)), ' + ') within group (Order by ks_customer_id, amount desc) ");
		sql.append("  from (select tmpLevel.ks_customer_id, amount, (count || tmpLevel.ks_level_code) tmpCode ");
		sql.append("  from ( ");
		sql.append("  select kscl.ks_customer_id,ksl.ks_level_code, ksl.amount , SUM(multiplier) count ");
		sql.append("  from ks_customer_level kscl ");
		sql.append("  inner join ks_level ksl on kscl.ks_level_id=ksl.ks_level_id ");
		sql.append("  where kscl.status=1 ");
		sql.append("  group by ksl.ks_level_code, kscl.ks_customer_id,ksl.amount ");
		sql.append("  ) tmpLevel) cl ");
		sql.append("  where cl.ks_customer_id=ksc.ks_customer_id) levelCustomer, ");
		
		sql.append("  nvl(mi.quantity_target,0) as quantityTarget,");
		sql.append("  nvl(mi.amount_target,0) as amountTarget,  ");
		sql.append("  nvl(mo.quantity,0) as quantity,  ");
		sql.append("  nvl(mo.amount,0) as amount,  ");
		sql.append("  (CASE nvl(mi.quantity_target,0) WHEN 0 THEN (CASE nvl(mo.quantity,0) WHEN 0 THEN 0 ELSE 100 END) ELSE ROUND(100*nvl(mo.quantity,0)/nvl(mi.quantity_target,0),2)  ");
		sql.append("  END) percentQuantity,  ");
		sql.append("  (CASE nvl(mi.amount_target,0) WHEN 0 THEN (CASE nvl(mo.amount,0) WHEN 0 THEN 0 ELSE 100 END) ELSE ROUND(100*nvl(mo.amount,0)/nvl(mi.amount_target,0),2) END)   ");
		sql.append("  percentAmount,  nvl(mo.result,5) result, ksc.reward_type rewardType, ksc.total_reward_money totalReward,  ");
		sql.append("  ksc.total_reward_money_done totalRewardDone,(nvl(ksc.total_reward_money,0)-nvl(ksc.total_reward_money_done,0))   ");
		sql.append("  payReward,  ");
		sql.append("  (select listagg(CAST((ksr.product_num || ' ' || p.product_code || ' (' || p.product_name || ')') AS  ");
		sql.append("  VARCHAR2(2000)),' + ')  ");
		sql.append("  within GROUP (  ORDER BY ksr.ks_customer_id, ksr.product_id DESC)  ");
		sql.append("  from ks_cus_product_reward ksr  ");
		sql.append("  left join product p  ");
		sql.append("  on (ksr.product_id=p.product_id and p.status=1)  ");
		sql.append("  where ksr.status = 1 and ksr.ks_customer_id=ksc.ks_customer_id  ");
		sql.append("  and  ksr.product_num>0) productReward,  ");
		sql.append("  (select listagg(CAST((ksr.product_num_done || ' ' || p.product_code || ' (' || p.product_name || ')') AS  ");
		sql.append("   VARCHAR2(2000)),' + ')  ");
		sql.append("  within GROUP (  ORDER BY ksr.ks_customer_id, ksr.product_id DESC)  ");
		sql.append("  from ks_cus_product_reward ksr  ");
		sql.append("  left join product p  ");
		sql.append("  on (ksr.product_id=p.product_id and p.status=1)  ");
		sql.append("  where ksr.status = 1 and ksr.ks_customer_id=ksc.ks_customer_id  ");
		sql.append("  and ksr.product_num_done>0) productRewardDone,  ");
		sql.append("  ksc.amount_discount amountDisCount,  ");
		sql.append("  ksc.over_amount_discount overAmountDiscount,  ");
		sql.append("  ksc.display_money displayMoney,  ");
		sql.append("  ksc.support_money supportMoney,  ");
		sql.append("  ksc.odd_discount oddDiscount,  ");
		sql.append("  ksc.add_money addMoney  ");
		sql.append("  FROM ks_customer ksc  ");
		sql.append("  join cycle tc on tc.cycle_id = ksc.cycle_id ");
		sql.append("  JOIN shop s  ");
		sql.append("  ON ksc.shop_id=s.shop_id  ");
		
		if (!StringUtility.isNullOrEmpty(filter.getStrLstShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrLstShopId(), "s.shop_id");
			sql.append(paramsFix);
		}
		
		if (StringUtility.isNullOrEmpty(filter.getStrLstShopId()) && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			sql.append("  join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s.shop_id = ogr.number_key_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		if (Boolean.TRUE.equals(filter.getIsGetParentShop())) {
			sql.append("  LEFT JOIN shop s1 ON s.parent_shop_id = s1.shop_id ");
			sql.append("  LEFT JOIN shop s2 ON s1.parent_shop_id = s2.shop_id ");
		}
		sql.append("  INNER JOIN customer c  ");
		sql.append("  ON ksc.customer_id=c.customer_id  ");
		sql.append("  LEFT JOIN rpt_ks_mobile mo  ");
		sql.append("  ON (ksc.ks_id=mo.ks_id and ksc.customer_id=mo.customer_id  ");
		sql.append("  and ksc.shop_id=mo.shop_id and ksc.cycle_id=mo.cycle_id)  ");
		sql.append("  LEFT JOIN (select ksl.ks_customer_id,  sum(nvl(ksl.multiplier * kl.amount,0)) amount_target, ");
		sql.append("  sum(nvl(ksl.multiplier * kl.quantity,0)) quantity_target");
		sql.append("  from ks_customer_level ksl ");
		sql.append("  inner join ks_level kl on (ksl.ks_level_id=kl.ks_level_id and kl.status=1) ");
		sql.append("  Where ksl.status=1 ");
		sql.append("  group by ksl.ks_customer_id) mi  ");
		sql.append("  on (ksc.ks_customer_id=mi.ks_customer_id) ");
		
		sql.append("  where 1=1 and ksc.status = 1");		
		if (!StringUtility.isNullOrEmpty(filter.getCodeAndName())) {
			String temp = StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCodeAndName().toLowerCase()));
			sql.append(" and (lower(c.customer_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCodeAndName().toLowerCase()));
			sql.append(" 		or lower(c.name_text) like ? ESCAPE '/' ");
			params.add(temp);
			sql.append(" 		or lower(c.address) like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCodeAndName().toLowerCase()));
		}
		
		if (filter.getStatus() != null) {
			if (filter.getStatus() == -1) {
				sql.append(" and ksc.reward_type is null ");
			}
			else
			{
				sql.append(" and ksc.reward_type = ? ");
				params.add(filter.getStatus());
			}
		}
		if (filter.getResult() != null) {
			if (filter.getResult()==5)
			{
				sql.append(" and (mo.result = ? or mo.result is null) ");
				params.add(filter.getResult());
			}
			else
			{
				sql.append(" and mo.result = ? ");
				params.add(filter.getResult());
			}
		}
		else sql.append(" and ksc.status in (1,2,3) ");
		if (filter.getYear() != null) {
			if (filter.getNum() != null) {
				sql.append(" and (to_char(tc.year,'yyyy') = ? and tc.num = ?)");
				params.add(filter.getYear().toString());
				params.add(filter.getNum());
			} else {
				sql.append(" and ? = to_char(tc.year,'yyyy') ");
				params.add(filter.getYear().toString());
			}
		}
		if (filter.getShopId() != null) {
			sql.append(" and ksc.shop_id in (select shop_id from shop START WITH shop_id = ? ");
			sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id) ");
			params.add(filter.getShopId());
		}
		if (filter.getKsId() != null) {
			sql.append(" and ksc.ks_id = ? ");
			params.add(filter.getKsId());
		}
		countSql.append(" select count(1) count from (").append(sql.toString()).append(")");
		
		final String[] fieldNames = new String[] { 
				"areaCode", "zoneCode",
				"id","shopCode", "shopName", "customerCode", "customerName", 
				"address", "cycleId", "levelCustomer", "quantityTarget",
				"amountTarget", "quantity", "amount", "percentQuantity",
				"percentAmount", "result", "rewardType",
				"totalReward", "totalRewardDone", "payReward",
				"productReward", "productRewardDone", "amountDiscount", "overAmountDiscount",
				"displayMoney", "supportMoney", "oddDiscount", "addMoney"
				}; 
		String sort = this.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			sql.append(" order by ").append(sort).append(" ");
			String order = this.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				sql.append(order);
			}
		} else {
			sql.append(" order by shopCode ");
		}
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL
				};
		if (filter.getkPagingVO() == null)
			return repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVO());

	}
	
	@Override
	public List<KeyShopVO> getListKSVOForRewardOrderByFilter(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with lstProduct as ( ");
		sql.append("     select kc.ks_id ksId, (select ks_code from ks where ks_id = kc.ks_id) ksCode, ");
		sql.append("     p.product_id productId, p.product_code productCode, p.product_name productName, p.convfact, ");
		sql.append("     nvl(sum(product_num),0) total, nvl(sum(product_num_done),0) totalDone ");
		sql.append("     from ks_customer kc ");
		sql.append("     join ks_cus_product_reward kcpr on kcpr.ks_customer_id = kc.ks_customer_id ");
		sql.append("     join product p on kcpr.product_id = p.product_id ");
		sql.append("     where kc.status = ? and kcpr.status = ? and kc.reward_type in (?,?) ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		params.add(RewardType.MO_KHOA_CHUA_TRA.getValue());
		params.add(RewardType.DA_TRA_MOT_PHAN.getValue());
		sql.append("     and kc.customer_id = ? ");
		params.add(filter.getCustomerId());
		sql.append("     group by kc.ks_id,kc.customer_id, p.product_id, p.product_code, p.product_name, p.convfact ");
		sql.append("     order by ksCode,productCode ");
		sql.append(" ) ");
		sql.append(" ,lstMoney as ( ");
		sql.append("     select kc.ks_id ksId, (select ks_code from ks where ks_id = kc.ks_id) ksCode, ");
		sql.append("     null productId, null productCode, null productName, 0 convfact, ");
		sql.append("     nvl(sum(total_reward_money),0) total, nvl(sum(total_reward_money_done),0) totalDone ");
		sql.append("     from ks_customer kc ");
		sql.append("     where kc.status = ? and kc.reward_type in (?,?) ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(RewardType.MO_KHOA_CHUA_TRA.getValue());
		params.add(RewardType.DA_TRA_MOT_PHAN.getValue());
		sql.append("     and kc.customer_id = ? ");
		params.add(filter.getCustomerId());
		sql.append("     group by kc.ks_id, kc.customer_id ");
		sql.append("     order by ksCode ");
		sql.append(" ) ");
		sql.append(" select * from lstProduct ");
		sql.append(" union all ");
		sql.append(" select * from lstMoney ");
		countSql.append(" select count(1) count from (").append(sql.toString()).append(")");
		
		final String[] fieldNames = new String[] { 
				"ksId","ksCode", "productId", 
				"productCode", "productName", "convfact",
				"total", "totalDone"
				}; 
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL
				};
		if (filter.getkPagingVO() == null)
			return repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVO());

	}
	
	@Override
	public List<KeyShopVO> getListKeyShopVOForViewOrderByFilter(KeyShopFilter filter) throws DataAccessException {
		final String KS = "KS";
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH keyShopCustomer AS ");
		sql.append("   (SELECT so.CUSTOMER_ID, ");
		sql.append("     sopd.PROGRAM_CODE, ");
		sql.append("     SUM(sopd.DISCOUNT_AMOUNT) DiscountAmount ");
		sql.append("   FROM SALE_ORDER so ");
		sql.append("   JOIN SALE_ORDER_PROMO_DETAIL sopd ON so.SALE_ORDER_ID = sopd.SALE_ORDER_ID ");
		sql.append("   WHERE 1 = 1 ");
		if (filter.getSaleOrderId() != null) {
			sql.append("   and so.SALE_ORDER_ID        = ? ");
			params.add(filter.getSaleOrderId());
		}
		sql.append("   and sopd.PROGRAM_TYPE       = ? ");
		params.add(ProgramType.KEY_SHOP.getValue());
		sql.append("   and sopd.PROGRAME_TYPE_CODE = ? ");
		params.add(KS);
		sql.append("   GROUP BY so.CUSTOMER_ID, sopd.PROGRAM_CODE ");
		sql.append("   ) ");
		sql.append("   SELECT ks.KS_ID ksId, ");
		sql.append("     ks.ks_code ksCode, ");
		sql.append("     null productId, ");
		sql.append("     null productCode, ");
		sql.append("     null productName, ");
		sql.append("     0 convfact, ");
		sql.append("     c.DiscountAmount quantityAmount, ");
		sql.append("     0 quantityRetail, ");
		sql.append("     0 quantityPackage, ");
		sql.append("     null warehouseId, ");
		sql.append("     null warehouseName, ");
		sql.append("     0 availableQuantity, ");
		sql.append("     NVL(SUM(ksc.TOTAL_REWARD_MONEY),0) total, ");
		sql.append("     NVL(SUM(ksc.TOTAL_REWARD_MONEY_DONE),0) totalDone ");
		sql.append("   FROM keyShopCustomer c ");
		sql.append("   JOIN KS ks ON ks.KS_CODE = c.PROGRAM_CODE ");
		sql.append("   JOIN KS_CUSTOMER ksc ON ks.KS_ID = ksc.KS_ID AND c.CUSTOMER_ID = ksc.CUSTOMER_ID ");
		sql.append("   WHERE 1              = 1 ");
		sql.append("   AND ksc.STATUS       = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   GROUP BY ks.KS_ID, ks.ks_code, c.DiscountAmount ");
		sql.append("   UNION   ");
		sql.append("   SELECT ks.KS_ID ksId, ");
		sql.append("     ks.ks_code ksCode, ");
		sql.append("     sol.PRODUCT_ID productId, ");
		sql.append("     p.PRODUCT_CODE productCode, ");
		sql.append("     p.PRODUCT_NAME productName, ");
		sql.append("     p.convfact convfact, ");
		sql.append("     sol.QUANTITY quantityAmount, ");
		sql.append("     NVL(sol.QUANTITY_RETAIL,0) quantityRetail, ");
		sql.append("     NVL(sol.QUANTITY_PACKAGE,0) quantityPackage, ");
		sql.append("     sol.WAREHOUSE_ID warehouseId, ");
		sql.append("     w.WAREHOUSE_NAME warehouseName, ");
		sql.append("     st.AVAILABLE_QUANTITY availableQuantity, ");
		sql.append("     NVL(SUM(kcp.PRODUCT_NUM),0) total, ");
		sql.append("     NVL(SUM(kcp.PRODUCT_NUM_DONE),0) totalDone ");
		sql.append("   FROM SALE_ORDER so ");
		sql.append("   join SALE_ORDER_DETAIL sod on so.SALE_ORDER_ID = sod.SALE_ORDER_ID ");
		sql.append("   join SALE_ORDER_LOT sol on sod.SALE_ORDER_DETAIL_ID = sol.SALE_ORDER_DETAIL_ID ");
		sql.append("   join PRODUCT p on sol.PRODUCT_ID = p.PRODUCT_ID ");
		sql.append("   left join warehouse w on sol.WAREHOUSE_ID = w.WAREHOUSE_ID ");
		sql.append("   left join STOCK_TOTAL st on sol.WAREHOUSE_ID = st.WAREHOUSE_ID and sol.PRODUCT_ID = st.PRODUCT_ID and st.OBJECT_TYPE = ? ");
		params.add(StockObjectType.SHOP.getValue());
		sql.append("   		and sol.SHOP_ID = st.OBJECT_ID ");
		sql.append("   join KS ks on ks.KS_CODE = sod.PROGRAM_CODE ");
		sql.append("   join KS_CUSTOMER ksc on ks.KS_ID = ksc.KS_ID and so.CUSTOMER_ID = ksc.CUSTOMER_ID ");
		sql.append("   join KS_CUS_PRODUCT_REWARD kcp on ks.KS_ID = kcp.KS_ID and ksc.KS_CUSTOMER_ID = kcp.KS_CUSTOMER_ID and sol.PRODUCT_ID = kcp.PRODUCT_ID ");
		sql.append("   WHERE 1 = 1 ");
		if (filter.getSaleOrderId() != null) {
			sql.append("   and so.SALE_ORDER_ID = ? ");
			params.add(filter.getSaleOrderId());
		}
		sql.append("   and sod.PROGRAM_TYPE = ? ");
		params.add(ProgramType.KEY_SHOP.getValue());
		sql.append("   and sod.PROGRAME_TYPE_CODE = ? ");
		params.add(KS);
		sql.append("   and ksc.STATUS = ?  ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   GROUP BY ks.KS_ID, ks.ks_code, sol.PRODUCT_ID, p.PRODUCT_CODE, p.PRODUCT_NAME, p.convfact, ");
		sql.append("     sol.QUANTITY, sol.QUANTITY_RETAIL, sol.QUANTITY_PACKAGE, ");
		sql.append("     sol.WAREHOUSE_ID, w.WAREHOUSE_NAME, st.AVAILABLE_QUANTITY ");
		sql.append("   ORDER BY productCode, productName, warehouseName ");

		final String[] fieldNames = new String[] { 
				"ksId", "ksCode",
				"productId", "productCode", "productName",
				"convfact", "quantityAmount",
				"quantityRetail", "quantityPackage", "warehouseId", 
				"warehouseName", "availableQuantity", "total", "totalDone" 
				};
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL
				};
		return repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<KSCustomer> getListKSCustomerForReward(KeyShopFilter filter) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(filter.getKsCode())) {
			return new ArrayList<KSCustomer>();
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT kc.* ");
		sql.append(" FROM KS k  ");
		sql.append(" JOIN KS_CUSTOMER kc ON k.ks_id = kc.KS_ID ");
		sql.append(" JOIN CYCLE c on kc.CYCLE_ID = c.CYCLE_ID ");
		sql.append(" WHERE k.KS_CODE = ? ");
		params.add(filter.getKsCode().toUpperCase());
		if (Boolean.TRUE.equals(filter.getIsReturn())) {
			sql.append(" AND kc.REWARD_TYPE in (?, ?) ");
			params.add(RewardType.DA_TRA_TOAN_BO.getValue());
			params.add(RewardType.DA_TRA_MOT_PHAN.getValue());
			sql.append(" AND NVL(kc.TOTAL_REWARD_MONEY_DONE, 0) > 0 ");
		} else if (Boolean.FALSE.equals(filter.getIsReturn())) {
			sql.append(" AND kc.REWARD_TYPE in (?, ?) ");
			params.add(RewardType.MO_KHOA_CHUA_TRA.getValue());
			params.add(RewardType.DA_TRA_MOT_PHAN.getValue());
			sql.append(" AND NVL(kc.TOTAL_REWARD_MONEY_DONE, 0) < kc.TOTAL_REWARD_MONEY ");
		}
		if (filter.getCustomerId() != null) {
			sql.append(" AND kc.CUSTOMER_ID = ? ");
			params.add(filter.getCustomerId());
		}
		sql.append(" ORDER BY c.BEGIN_DATE ");
		return repo.getListBySQL(KSCustomer.class, sql.toString(), params);
	}
	
	@Override
	public List<KSCusProductReward> getListKSCusProductRewardForReward(KeyShopFilter keyShopFilter) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(keyShopFilter.getKsCode())) {
			return new ArrayList<KSCusProductReward>();
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT kcp.* ");
		sql.append(" FROM KS k  ");
		sql.append(" JOIN KS_CUSTOMER kc ON k.ks_id = kc.KS_ID ");
		sql.append(" JOIN CYCLE c on kc.CYCLE_ID = c.CYCLE_ID ");
		sql.append(" JOIN KS_CUS_PRODUCT_REWARD kcp on kc.KS_CUSTOMER_ID = kcp.KS_CUSTOMER_ID ");
		sql.append(" WHERE k.KS_CODE = ? ");
		params.add(keyShopFilter.getKsCode().toUpperCase());
		if (Boolean.TRUE.equals(keyShopFilter.getIsReturn())) {
			sql.append(" AND kc.REWARD_TYPE in (?, ?) ");
			params.add(RewardType.DA_TRA_TOAN_BO.getValue());
			params.add(RewardType.DA_TRA_MOT_PHAN.getValue());
			sql.append(" AND NVL(kcp.PRODUCT_NUM_DONE, 0) > 0 ");
		} else if (Boolean.FALSE.equals(keyShopFilter.getIsReturn())) {
			sql.append(" AND kc.REWARD_TYPE in (?, ?) ");
			params.add(RewardType.MO_KHOA_CHUA_TRA.getValue());
			params.add(RewardType.DA_TRA_MOT_PHAN.getValue());
			sql.append(" AND NVL(kcp.PRODUCT_NUM_DONE, 0) < kcp.PRODUCT_NUM ");
		}
		
		if (keyShopFilter.getCustomerId() != null) {
			sql.append(" AND kc.CUSTOMER_ID = ? ");
			params.add(keyShopFilter.getCustomerId());
		}
		if (keyShopFilter.getProductId() != null) {
			sql.append(" AND kcp.PRODUCT_ID = ? ");
			params.add(keyShopFilter.getProductId());
		}
		sql.append(" ORDER BY c.NUM, c.BEGIN_DATE ");
		return repo.getListBySQL(KSCusProductReward.class, sql.toString(), params);
	}
	
	@Override
	public List<KSCusProductRewardDoneVO> getListKSCusProductRewardDone(List<Long> lstKSCustomerId) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT KS_CUSTOMER_ID ksCustomerId, ");
		sql.append("  CASE WHEN SUM(PRODUCT_NUM) = SUM(PRODUCT_NUM_DONE) THEN 1 ELSE 0 END isDone, ");
		sql.append("  SUM(PRODUCT_NUM_DONE) totalProductNumDone");
		sql.append(" FROM KS_CUS_PRODUCT_REWARD ");
		sql.append(" WHERE KS_CUSTOMER_ID in (-1 ");
		for (int i = 0, n = lstKSCustomerId.size(); i < n; i++) {
			sql.append(", ?");
			params.add(lstKSCustomerId.get(i));
		}
		sql.append(" ) ");
		sql.append(" GROUP BY KS_CUSTOMER_ID ");
		final String[] fieldNames = new String[] { "ksCustomerId", "isDone", "totalProductNumDone" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.BOOLEAN, StandardBasicTypes.BIG_DECIMAL };
		return repo.getListByQueryAndScalar(KSCusProductRewardDoneVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<TotalRewardMoneyVO> getListTotalRewardMoneyVO(SaleOrderPromoDetailFilter filter) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ks.KS_CODE KSCode, ");
		sql.append("   SUM(TOTAL_REWARD_MONEY) totalRewardMoney, ");
		sql.append("   SUM(TOTAL_REWARD_MONEY_DONE) totalRewardMoneyDone ");
		sql.append(" FROM KS ks ");
		sql.append(" JOIN KS_CUSTOMER ksc ON ks.KS_ID       = ksc.KS_ID ");
		sql.append(" WHERE ks.KS_CODE IN ");
		sql.append("   (SELECT PROGRAM_CODE ");
		sql.append("   FROM SALE_ORDER_PROMO_DETAIL ");
		sql.append("   WHERE 1 = 1 ");
		if (filter.getSaleOrderId() != null) {
			sql.append(" AND SALE_ORDER_ID = ? ");
			params.add(filter.getSaleOrderId());
		}
		if (filter.getShopId() != null) {
			sql.append(" AND SHOP_ID = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getProgramType() != null) {
			sql.append(" AND PROGRAM_TYPE = ? ");
			params.add(filter.getProgramType().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getProgrameTypeCode())) {
			sql.append(" AND PROGRAME_TYPE_CODE = ? ");
			params.add(filter.getProgrameTypeCode());
		}
		sql.append("   ) ");
		if (filter.getCustomerId() != null) {
			sql.append(" AND ksc.CUSTOMER_ID = ? ");
			params.add(filter.getCustomerId());
		}

		sql.append(" GROUP BY ks.KS_CODE ");

		final String[] fieldNames = new String[] { "ksCode", "totalRewardMoney", "totalRewardMoneyDone" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(TotalRewardMoneyVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<KeyShopVO> getInfoKSForOrder(KeyShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select ks.ks_id ksId, ks.ks_code ksCode, ks.name ksName, ks.description, ");
		sql.append(" (select num || '/' || to_char(year,'yyyy') from cycle where cycle_id = ks.from_cycle_id) fromCycle, ");
		sql.append(" (select num || '/' || to_char(year,'yyyy') from cycle where cycle_id = ks.to_cycle_id) toCycle, ");
		sql.append(" (select LISTAGG(to_char(p.product_code) ||' - '|| to_char(p.product_name), ', ') WITHIN GROUP (ORDER BY product_code) ");
		sql.append("   	from product p WHERE p.product_id in (select product_id from ks_product where ks_id = ks.ks_id)) productCode ");
		sql.append(" FROM ks where 1=1 ");
		sql.append(" and ks.ks_id = ? ");
		params.add(filter.getKsId());
		final String[] fieldNames = new String[] { 
				"ksId", "ksCode", "ksName", "description", 
				"fromCycle","toCycle", "productCode" 
				}; 
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING
				};
		return repo.getListByQueryAndScalar(KeyShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<KeyShopCustomerHistoryVO> getKSRegistedHistory(KPaging<KeyShopCustomerHistoryVO> kPaging, Long ksId, Long cycleId, Long ksLevelId, Long customerId, Date updateDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select  ");
		sql.append(" KS.NAME as keyShopName ");
		sql.append(" , cc.CYCLE_NAME as cycleName ");
		sql.append(" , kl.NAME as levelName ");
		sql.append(" , hs.UPDATE_USER as userName ");
		sql.append(" , to_char(hs.UPDATE_DATE, 'dd/MM/yyyy hh24:mi:ss') as createDateStr ");
		sql.append(" , hs.multiplyer ");
		sql.append(" from KS_REGISTED_HIS hs ");
		sql.append(" join KS on hs.KS_ID = KS.KS_ID ");
		sql.append(" join CYCLE cc on hs.CYCLE_ID = cc.CYCLE_ID ");
		sql.append(" join KS_LEVEL kl on kl.KS_LEVEL_ID = hs.KS_LEVEL_ID ");
		sql.append(" where 1 = 1 ");
		if (customerId != null) {
			sql.append(" and hs.customer_id = ? ");
			params.add(customerId);
		}
		if (ksId != null) {
			sql.append(" and hs.KS_ID = ? ");
			params.add(ksId);
		}
		if (cycleId != null) {
			sql.append(" and hs.CYCLE_ID = ? ");
			params.add(cycleId);
		}
		if (ksLevelId != null) {
			sql.append(" and hs.KS_LEVEL_ID = ? ");
			params.add(ksLevelId);
		}
		if (updateDate != null) {
			sql.append(" and hs.UPDATE_DATE >= trunc(?) ");
			params.add(updateDate);
			sql.append(" and hs.UPDATE_DATE < trunc(?) + 1 ");
			params.add(updateDate);
		}
		sql.append(" order by cc.CYCLE_NAME, hs.UPDATE_DATE desc  ");
		countSql.append(" select count(1) count from (").append(sql.toString()).append(")");
		
		final String[] fieldNames = new String[] { 
				"keyShopName" //1
				, "cycleName" //2
				, "levelName" //3
				, "userName" // 4
				, "createDateStr" // 5
				, "multiplyer" //6
		};
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.STRING //1
				, StandardBasicTypes.STRING //2
				, StandardBasicTypes.STRING //3
				, StandardBasicTypes.STRING //4
				, StandardBasicTypes.STRING //5
				, StandardBasicTypes.BIG_DECIMAL //6
		};
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(KeyShopCustomerHistoryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		return repo.getListByQueryAndScalarPaginated(KeyShopCustomerHistoryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}
}
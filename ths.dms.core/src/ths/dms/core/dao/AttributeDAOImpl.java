package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.record.formula.functions.T;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Attribute;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeFilter;
import ths.dms.core.entities.enumtype.AttributeVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.TableHasAttribute;
import ths.dms.core.entities.vo.DynamicAttributeVO;
import ths.dms.core.exceptions.DataAccessException;
/*import test.ths.dms.common.TestUtils;*/

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class AttributeDAOImpl implements AttributeDAO {

	@Autowired
	private IRepository repo;

	@Override
	public Attribute createAttribute(Attribute attribute)
			throws DataAccessException {
		if (attribute == null) {
			throw new IllegalArgumentException("attribute");
		}
		repo.create(attribute);
		return repo.getEntityById(Attribute.class, attribute.getId());
	}

	@Override
	public void deleteAttribute(Attribute attribute)
			throws DataAccessException {
		if (attribute == null) {
			throw new IllegalArgumentException("attribute");
		}
		repo.delete(attribute);
	}

	@Override
	public Attribute getAttributeById(long id) throws DataAccessException {
		return repo.getEntityById(Attribute.class, id);
	}

	@Override
	public void updateAttribute(Attribute attribute)
			throws DataAccessException {
		if (attribute == null) {
			throw new IllegalArgumentException("attribute");
		}
		if (!StringUtility.isNullOrEmpty(attribute.getAttributeCode())) {
			attribute.setAttributeCode(attribute.getAttributeCode()
					.toUpperCase());
		}

		repo.update(attribute);
	}

	@Override
	public Attribute getAttributeByCode(String tableName, String code)
			throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from ATTRIBUTE where ATTRIBUTE_CODE=? and STATUS!=? "
				+ " and TABLE_NAME = ? ";
		List<Object> params = new ArrayList<Object>();

		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		params.add(tableName);

		Attribute attribute = repo.getEntityBySQL(Attribute.class, sql, params);
		return attribute;
	}
	
	@Override
	public List<Attribute> getListAttribute(KPaging<Attribute> kPaging,
			TableHasAttribute table, String attributeCode,
			String attributeName, AttributeColumnType columnType,
			ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from customer_attribute where 1 = 1 ");

//		sql.append(" and status = ? ");
//		params.add(table.getTableName());

		if (!StringUtility.isNullOrEmpty(attributeCode)) {
			sql.append(" and lower(attribute_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(attributeCode
					.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(attributeName)) {
			sql.append(" and lower(attribute_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(attributeName
					.toLowerCase()));
		}
		
		if (null != columnType) {
			sql.append(" and column_type = ? ");
			params.add(columnType.getValue());
		}
		
		if (status != null) {
			sql.append(" and status=? ");
			params.add(status.getValue());
		} else {
			sql.append(" and status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		sql.append(" order by code");
		
		if (kPaging == null)
			return repo.getListBySQL(Attribute.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Attribute.class, sql.toString(),
					params, kPaging);
	}

	@Override
	public List<AttributeVO> getListAttributeVO(AttributeFilter filter, KPaging<AttributeVO> kPaging) throws DataAccessException {
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		selectSql.append("SELECT attr.attribute_id attributeId,");
		selectSql.append("   attr.attribute_code attributeCode,");
		selectSql.append("   attr.attribute_name attributeName,");
		selectSql.append("   attr.value_type valueType,");
		selectSql.append("   attr.status status,");
		selectSql.append("   attr.note note,");
		selectSql.append(" attr.table_name tableName");
		fromSql.append(" FROM attribute attr  ");
		fromSql.append("WHERE 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(filter.getAttributeCode())) {
			fromSql.append(" and upper(attr.attribute_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getAttributeCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getAttributeName())) {
			fromSql.append(" and upper(attr.attribute_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getAttributeName().toUpperCase()));
		}
		if(filter.getLstValueType() != null && filter.getLstValueType().size() > 0){
			Boolean isFirst = true;
			fromSql.append(" and attr.value_type in(");
			for (AttributeColumnType child : filter.getLstValueType()) {
				if(isFirst){
					isFirst = false;
					fromSql.append("?");
				}else{
					fromSql.append(",?");
				}
				params.add(child.getValue());
			}
			fromSql.append(" )");
		}
		if(filter.getListObject()!=null && !filter.getListObject().isEmpty()){
			if(filter.getListObject().contains("-1")){ // tim tat ca
				// do nothing
			}else{
				fromSql.append(" and table_name in (");
				String[] tmps = filter.getListObject().split(",");
				Boolean isFirst = true;
				for (String str : tmps) {
					if(isFirst){
						isFirst = false;
						fromSql.append("?");
					}else{
						fromSql.append(",?");
					}
					params.add(str);
				}
				fromSql.append(" )");
			}
		}else{
			fromSql.append(" and table_name in (?)");
			params.add("CUSTOMER");
		}
		if(filter.getStatus() != null){
			fromSql.append(" and attr.status=?");
			params.add(filter.getStatus().getValue());
		}
		selectSql.append(fromSql.toString());
		selectSql.append(" order by attr.attribute_code, attr.attribute_id");
		
		countSql.append("select count(attr.attribute_id) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"attributeId", "attributeCode", "attributeName", 
				"valueType", "status", "note","tableName"
				};

		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING
		};	
		List<AttributeVO> lst =  null;
		if (kPaging == null){
			lst =  repo.getListByQueryAndScalar(
					AttributeVO.class, fieldNames,
					fieldTypes, selectSql.toString(), params);
		}else{
			lst = repo.getListByQueryAndScalarPaginated(AttributeVO.class, fieldNames, fieldTypes,
					selectSql.toString(), countSql.toString(), params, params, kPaging);
		}
		return lst;
	}

	@Override
	public Boolean isUsingByOther(String attributeCode)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) as count from attribute where ATTRIBUTE_CODE = ? and status != -1");
		params.add(attributeCode.toUpperCase());
		int res = repo.countBySQL(sql.toString(), params);
		return res==1?true:false;
	}

	@Override
	public <T> List<DynamicAttributeVO> getDynamicAttribute(Class<T> clazz, ActiveType status) throws DataAccessException {
		if (clazz == null) {
			throw new IllegalArgumentException("object type is required");
		}
		
		Boolean hasDynamicAttributeClass = Customer.class.isAssignableFrom(clazz) 
										|| Product.class.isAssignableFrom(clazz)
										|| Staff.class.isAssignableFrom(clazz);
		if (!hasDynamicAttributeClass) {
			throw new IllegalArgumentException("object type don't have dynamic attribute.");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select a.code attributeCode, a.name attributeName, a.type attributeType ");
		sql.append(" , a.display_order displayOrder, ae.code attributeDetailCode, ae.value attributeDetailName ");
		sql.append(" , a.data_length attibuteValueMaxLength, a.min_value attributeMinValue, a.max_value attributeMaxValue, a.mandatory attributeMandatoy ");
		
		if (status == null) {
			status = ActiveType.RUNNING;
		}
		if (Customer.class.isAssignableFrom(clazz)) {
			sql.append(" , a.customer_attribute_id attributeId, ae.customer_attribute_enum_id attributeDetailId ");
			sql.append(" from customer_attribute a ");
			sql.append(" left join customer_attribute_enum ae ");
			sql.append(" on ae.customer_attribute_id = a.customer_attribute_id and ae.status = ? ");
			params.add(status.getValue());
		} else if (Product.class.isAssignableFrom(clazz)) {
			sql.append(" , a.product_attribute_id attributeId, ae.product_attribute_enum_id attributeDetailId ");
			sql.append(" from product_attribute a ");
			sql.append(" left join product_attribute_enum ae ");
			sql.append(" on ae.product_attribute_id = a.product_attribute_id and ae.status = ? ");
			params.add(status.getValue());
		} else if (Staff.class.isAssignableFrom(clazz)) {
			sql.append(" , a.staff_attribute_id attributeId, ae.staff_attribute_enum_id attributeDetailId ");
			sql.append(" from staff_attribute a ");
			sql.append(" left join staff_attribute_enum ae ");
			sql.append(" on ae.staff_attribute_id = a.staff_attribute_id and ae.status = ? ");
			params.add(status.getValue());
		}
		sql.append(" where a.status = ? ");
		params.add(status.getValue());
		sql.append(" order by a.display_order, NLSSORT(a.name, 'NLS_SORT=vietnamese'), ae.value ");
		
		String[] fieldNames = {
			"attributeId",
			"attributeCode", "attributeName",
			"attributeType", "displayOrder",
			"attributeDetailId",
			"attributeDetailCode", "attributeDetailName",
			"attibuteValueMaxLength",
			"attributeMinValue", "attributeMaxValue",
			"attributeMandatoy"
		};

		Type[] fieldTypes = { 
			StandardBasicTypes.LONG,
			StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
			StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
			StandardBasicTypes.LONG,
			StandardBasicTypes.STRING, StandardBasicTypes.STRING,
			StandardBasicTypes.INTEGER,
			StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
			StandardBasicTypes.INTEGER
		};
		
		return repo.getListByQueryAndScalar(DynamicAttributeVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

}

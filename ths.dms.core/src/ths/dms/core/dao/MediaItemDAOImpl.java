package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import test.ths.dms.common.TestUtils;
import ths.dms.core.common.utils.LogUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.OfficeDocShopMap;
import ths.dms.core.entities.OfficeDocument;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.enumtype.MediaType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.filter.FeedbackFilter;
import ths.dms.core.entities.filter.ImageFilter;
import ths.dms.core.entities.filter.OfficeDocumentFilter;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.MediaItemDetailVO;
import ths.dms.core.entities.vo.MediaItemVO;
import ths.dms.core.exceptions.DataAccessException;

public class MediaItemDAOImpl implements MediaItemDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Override
	public MediaItem createMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws DataAccessException {
		if (mediaItem == null) {
			throw new IllegalArgumentException("mediaItem");
		}
//		mediaItem.setCreateUser(logInfo.getStaffCode());
		repo.create(mediaItem);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, mediaItem, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(MediaItem.class, mediaItem.getId());
	}

	@Override
	public void deleteMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws DataAccessException {
		if (mediaItem == null) {
			throw new IllegalArgumentException("mediaItem");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(mediaItem);
		try {
			actionGeneralLogDAO.createActionGeneralLog(mediaItem, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public MediaItem getMediaItemById(long id) throws DataAccessException {
		return repo.getEntityById(MediaItem.class, id);
	}
	
	@Override
	public MediaItem updateMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws DataAccessException {
		if (mediaItem == null) {
			throw new IllegalArgumentException("mediaItem");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		MediaItem temp = this.getMediaItemById(mediaItem.getId());

//		product.setUpdateDate(commonDAO.getSysDate());
//		product.setUpdateUser(logInfo.getStaffCode());
		
		repo.update(mediaItem);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, mediaItem, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return this.getMediaItemById(mediaItem.getId());
	}

	@Override
	public List<MediaItem> getListMediaItemByObjectIdAndObjectType(KPaging<MediaItem> kPaging, Long objectId, MediaObjectType objectType) throws DataAccessException {
		if (objectId == null) {
			throw new IllegalArgumentException("objectId is null");
		}
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		
		//old vuongmq
		sql.append(" select * from media_item ");
		sql.append(" where status = 1 ");
		if(objectId != null){
			sql.append(" and object_id = ?");
			params.add(objectId);
		}
		if(objectType != null){
			sql.append(" and object_type = ?");
			params.add(objectType.getValue());
		}
		if(objectType != null && MediaObjectType.IMAGE_PRODUCT.equals(objectType)){
			sql.append(" order by media_type desc, create_date desc");
		} else {
			sql.append(" order by media_item_id");
		}
		
		/* vuongmq
		 * sql.append(" SELECT * ");
		sql.append("  FROM ");
		sql.append("  (SELECT mi.* ");
		sql.append("  FROM media_item mi ");
		sql.append(" WHERE (mi.object_id = ? ");
		params.add(objectId);
		sql.append(" AND mi.object_type  = 3 ");
		sql.append(" AND mi.status = 1");
		sql.append(" AND mi.media_type   = 0) ");
		sql.append(" UNION ");
		sql.append(" SELECT mi.* ");
		sql.append(" FROM media m ");
		sql.append(" JOIN media_item mi ");
		sql.append(" ON (m.media_item_id = mi.media_item_id and m.status = 1 and mi.status = 1) ");
		sql.append(" JOIN media_map mm ");
		sql.append(" ON (mm.media_id      = m.media_id) ");
		sql.append(" WHERE mi.object_type = 7 ");
		sql.append(" AND mm.object_type   = 1 ");
		sql.append(" AND mm.status        = 1 ");
		sql.append(" AND mm.object_id     = ? ");
		params.add(objectId);
		sql.append(" ) ");
		sql.append(" ORDER BY media_item_id");*/
		
		
		
		if (kPaging == null){
			return repo.getListBySQL(MediaItem.class, sql.toString(), params);
		}			
		else{
			return repo.getListBySQLPaginated(MediaItem.class, sql.toString(), params, kPaging);
		}			
	}

	@Override
	public List<MediaItem> getListMediaItemByMediaType(MediaType mediaType) throws DataAccessException {
		if (mediaType == null) {
			throw new IllegalArgumentException("mediaType is null");
		}
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" select * from media_item");
		sql.append(" where media_type = ?");
		params.add(mediaType.getValue());		
		sql.append(" order by media_item_id");
		return repo.getListBySQL(MediaItem.class, sql.toString(), params);
	}
	
//	@Override
//	public List<MediaItem> getListMediaItemByProduct(Long productId) throws DataAccessException {
//		if (productId == null) {
//			throw new IllegalArgumentException("productId is null");
//		}
//		StringBuilder sql = new StringBuilder();
//		ArrayList<Object> params = new ArrayList<Object>();
//		sql.append(" select * from media_item");
//		sql.append(" where object_id = ?");
//		params.add(productId);
//		sql.append(" order by media_item_id");
//		return repo.getListBySQL(MediaItem.class, sql.toString(), params);
//	}
	
	@Override
	public MediaItem checkIsExistsVideoOfProduct(Long productId)throws DataAccessException {	
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from media_item ");
		sql.append(" where object_id=? and media_type=1 and object_type=3 ");
		params.add(productId);
		return repo.getEntityBySQL(MediaItem.class, sql.toString(), params);
	}
	
	//Image SangTN
	@Override
	public MediaItemVO getMediaItemVOById(Long id) throws DataAccessException {
		String sql = "select media_item_id as id, url as url, THUMB_URL as thumbUrl from MEDIA_ITEM where media_item_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		
		String[] fieldNames = { 
			"id", 
			"url", 
			"thumbUrl"
		};

		Type[] fieldTypes = { 
			StandardBasicTypes.LONG,
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING
		};
		//return repo.getEntityBySQL(MediaItemVO.class, sql.toString(), params);
		List<MediaItemVO> result = repo.getListByQueryAndScalar(MediaItemVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return result != null && result.size() > 0 ? result.get(0) : null;
	}
	
	@Override
	public List<ImageVO> getListImageVO(ImageFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();

		StringBuilder sqlTemp = new StringBuilder("");
		sql.append(" WITH ");
		if (filter.getLstShop() != null && filter.getLstShop().size() > 0) {
			sql.append(" lstShop AS ");
			sql.append(" ( SELECT DISTINCT PS.SHOP_ID ");
			sql.append(" FROM SHOP ps ");
			sql.append(" START WITH ps.SHOP_ID  in ( ");
			for (int i = 0; i < filter.getLstShop().size(); i++) {
				if (i == 0)
					sql.append(" ? ");
				else
					sql.append(" ,? ");
				params.add(filter.getLstShop().get(i));
			}
			sql.append(" ) CONNECT BY PRIOR ps.SHOP_ID = ps.PARENT_SHOP_ID ");
			sql.append(" ), ");
		}
		if (!StringUtility.isNullOrEmpty(filter.getLevel())) {
			sql.append(" levelTmp as (SELECT regexp_substr(?,'[^,]+', 1, LEVEL) as code ");
			sql.append("  FROM dual CONNECT BY regexp_substr(?, '[^,]+', 1, LEVEL) IS NOT NULL), ");
			params.add(filter.getLevel().toUpperCase());
			params.add(filter.getLevel().toUpperCase());
		}
		sql.append(" lstTmp as ( ");
		sql.append(" SELECT ");
		sql.append(" MI.* ");
		sql.append(" FROM ");
		sql.append(" MEDIA_ITEM mi ");
		if (filter.getObjectType() != null && filter.getObjectType() == 4) {
			if (!StringUtility.isNullOrEmpty(filter.getLevel())) {
				sql.append(" JOIN DISPLAY_CUSTOMER_MAP cm ON MI.OBJECT_ID = CM.CUSTOMER_ID AND cm.status=1 ");
				sql.append(" JOIN display_program_LEVEL dpl ON cm.display_program_level_id=dpl.display_program_level_id and dpl.status=1 ");
				sql.append(" JOIN display_staff_map dsm on dsm.display_staff_map_id = cm.display_staff_map_id and dsm.status=1 ");
				sql.append(" AND dsm.display_program_id = mi.display_program_id ");
				sql.append(" AND trunc(cm.from_date)<=trunc(mi.create_date) and (cm.to_date is null or cm.to_date>=trunc(mi.create_date)) ");
				sql.append(" AND upper(dpl.level_code) in (SELECT * from levelTmp) ");
			}
		}
		sql.append(" JOIN STAFF st on st.staff_id=mi.staff_id AND st.staff_type_id in (select staff_type_id from staff where staff_type_id in (select staff_type_id from staff_type where specific_type in (1,2))) ");
		sql.append(" where 1=1 ");
		sql.append(" AND MI.STATUS=1 ");
		sql.append(" AND MI.MEDIA_TYPE=0 ");
		//sql.append(" AND MI.TYPE=1 ");
		sql.append(" AND TRUNC(MI.CREATE_DATE)>=trunc(ADD_MONTHS(trunc(SYSDATE,'MM'),-2)) ");

		if (filter.getIdCustomer() != null) {
			sql.append(" AND MI.OBJECT_ID =? ");
			params.add(filter.getIdCustomer());
		}
		if (null != filter.getObjectType()) {
			sql.append(" AND MI.OBJECT_TYPE =? ");
			params.add(filter.getObjectType());
		}
		if (null != filter.getStaffIdGroup()) {
			sql.append(" AND MI.STAFF_ID =? ");
			params.add(filter.getStaffIdGroup());
		}

		if (filter.getLstShop() != null && filter.getLstShop().size() > 0) {
			sql.append(" AND MI.SHOP_ID IN ");
			sql.append(" ( SELECT SHOP_ID FROM lstShop ");
			sql.append(" ) ");
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopIdListStr())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getShopIdListStr(), "MI.SHOP_ID");
			sql.append(paramsFix);
		}
		if (filter.getKey() != null && filter.getKey() != -1) {

			sql.append(" AND EXISTS ");
			sql.append(" ( ");
			sql.append(" select 1 from routing r join routing_customer rc on r.routing_id = rc.routing_id ");
			if (filter.getLstStaff() != null && !filter.getLstStaff().isEmpty()) {
				sql.append(" join visit_plan vp on vp.routing_id = r.routing_id ");
			}
			sql.append(" WHERE MI.OBJECT_ID=rc.CUSTOMER_ID ");
			if (filter.getLstStaff() != null && !filter.getLstStaff().isEmpty()) {
				sql.append(" AND MI.STAFF_ID=VP.STAFF_ID ");
				sql.append(" and vp.status = 1 ");
				sql.append(" AND TRUNC(vp.from_date) <= TRUNC(sysdate,'dd') ");
				sql.append(" AND ((TRUNC(vp.to_date)  >= TRUNC(sysdate,'dd')  or vp.to_date is null )) ");

			}
			sql.append(" and r.status = 1 and rc.status = 1 ");
			sql.append(" AND MI.shop_id=r.Shop_id  ");
			sql.append(" AND MI.routing_id= r.routing_id  ");
			sql.append(" AND rc.start_date <= TRUNC(sysdate,'dd') ");
			sql.append(" AND (rc.end_date  >= TRUNC(ADD_MONTHS(TRUNC(SYSDATE,'MM'),-2)) ");
			sql.append(" OR  rc.end_date           IS NULL) ");

			switch (filter.getKey()) {
			case 2:
				sqlTemp.append(" AND RC.MONDAY=1 ");
				break;
			case 3:
				sqlTemp.append(" AND RC.TUESDAY=1 ");
				break;
			case 4:
				sqlTemp.append(" AND RC.WEDNESDAY=1 ");
				break;
			case 5:
				sqlTemp.append(" AND RC.THURSDAY=1 ");
				break;
			case 6:
				sqlTemp.append(" AND RC.FRIDAY=1 ");
				break;
			case 7:
				sqlTemp.append(" AND RC.SATURDAY=1 ");
				break;
			case 8:
				sqlTemp.append(" AND RC.SUNDAY=1 ");
				break;
			}
			sql.append(sqlTemp);
			sql.append(" ) ");

		}

		if (!StringUtility.isNullOrEmpty(filter.getCodeCustomer()) || !StringUtility.isNullOrEmpty(filter.getShortCode()) || !StringUtility.isNullOrEmpty(filter.getNameCustomer())) {
			sql.append(" AND MI.OBJECT_ID IN ");
			sql.append(" ( ");
			sql.append(" SELECT cus.CUSTOMER_ID FROM CUSTOMER cus ");
			sql.append(" WHERE 1=1 ");

			if (!StringUtility.isNullOrEmpty(filter.getCodeCustomer())) {
				sql.append(" AND cus.CUSTOMER_CODE LIKE ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getCodeCustomer().toUpperCase().trim()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
				sql.append(" AND cus.short_code LIKE ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getShortCode().toUpperCase().trim()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getNameCustomer())) {
				sql.append(" AND lower(cus.name_text) LIKE ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getNameCustomer().toLowerCase())));
			}
			sql.append(" ) ");
		}
		if (filter.getLstStaff() != null && !filter.getLstStaff().isEmpty()) {
			sql.append(" AND MI.STAFF_ID IN ( ");
			for (int i = 0; i < filter.getLstStaff().size(); i++) {
				if (i == 0) {
					sql.append(" ? ");
				} else {
					sql.append(" ,? ");
				}
				params.add(filter.getLstStaff().get(i));
			}
			sql.append(" ) ");
		}
		if (filter.getFromDate() != null) {
			sql.append(" AND TRUNC(MI.CREATE_DATE)>=TRUNC(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" AND TRUNC(MI.CREATE_DATE)<=TRUNC(?) ");
			params.add(filter.getToDate());
		}

		if (filter.getObjectType() != null && filter.getObjectType() == 4 && filter.getLstDisplayPrograme() != null && !filter.getLstDisplayPrograme().isEmpty()) {
			sql.append(" AND MI.DISPLAY_PROGRAM_ID IN (");
			for (int i = 0; i < filter.getLstDisplayPrograme().size(); i++) {
				if (i == 0) {
					sql.append(" ? ");
				} else {
					sql.append(" ,? ");
				}
				params.add(filter.getLstDisplayPrograme().get(i));
			}
			sql.append(" ) ");
		}

		sql.append(" ) ");
		sql.append(" SELECT MEI.media_item_id AS id, ");
		sql.append(" MEI.URL AS urlImage, ");
		sql.append(" MEI.THUMB_URL AS urlThum, ");
		sql.append(" MEI.CREATE_DATE AS createDate, ");
		sql.append(" MEI.LAT AS lat, ");
		sql.append(" MEI.LNG AS lng, ");
		sql.append(" MEI.OBJECT_ID AS customerId, ");
		sql.append(" (SELECT TRIM(CUS.SHORT_CODE) FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=MEI.OBJECT_ID) AS customerCode, ");
		sql.append(" (SELECT CUS.CUSTOMER_NAME FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=MEI.OBJECT_ID ) AS customerName, ");
		sql.append(" (SELECT CUS.STREET FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=MEI.OBJECT_ID) AS street, ");
		sql.append(" (SELECT CUS.HOUSENUMBER FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=MEI.OBJECT_ID ) AS housenumber, ");
		sql.append(" (SELECT ST.STAFF_ID FROM STAFF ST WHERE ST.STAFF_ID=MEI.STAFF_ID ) AS staffId, ");
		sql.append(" (SELECT ST.STAFF_CODE FROM STAFF ST WHERE ST.STAFF_ID=MEI.STAFF_ID ) AS staffCode, ");
		sql.append(" (SELECT st.staff_name FROM STAFF ST WHERE ST.STAFF_ID=MEI.STAFF_ID  ) AS staffName, ");
		sql.append(" (SELECT SH.SHOP_NAME FROM SHOP sh WHERE SH.SHOP_ID=MEI.SHOP_ID) as shopName, ");
		sql.append(" (SELECT SH.SHOP_CODE FROM SHOP sh WHERE SH.SHOP_ID=MEI.SHOP_ID) as shopCode, ");
		sql.append(" (SELECT KS_CODE ||' - '|| name from ks where KS_ID=mei.display_program_id ) as displayProgrameCode, ");
		sql.append("	cus.lat as custLat, ");
		sql.append("	cus.lng as custLng");
		sql.append(" FROM lstTmp MEI ");
		sql.append(" JOIN Customer cus on mei.object_id=cus.customer_id and cus.status=1 ");
		sql.append(" WHERE 1=1 ");

		String[] fieldNames = { "id",//1
				"urlImage",//2
				"urlThum",//3
				"createDate",//4
				//"result",
				"lat",//5
				"lng",//6
				//"monthSeq",
				"customerId",//7
				"customerCode",//8
				"customerName",//9
				"street",//10
				"housenumber",//11
				"staffId", "staffCode",//12
				"staffName",//13
				"shopName",//14
				"shopCode",//15
				"displayProgrameCode",
				//	"levelCode",
				"custLat", "custLng" };

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.TIMESTAMP,// 4
				//StandardBasicTypes.INTEGER,
				StandardBasicTypes.FLOAT, // 5
				StandardBasicTypes.FLOAT, // 6
				//StandardBasicTypes.INTEGER, // 6
				StandardBasicTypes.LONG, // 7
				StandardBasicTypes.STRING, // 8
				StandardBasicTypes.STRING, // 9
				StandardBasicTypes.STRING, // 10
				StandardBasicTypes.STRING, // 11
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, // 12
				StandardBasicTypes.STRING, // 13
				StandardBasicTypes.STRING, // 14
				StandardBasicTypes.STRING, // 15
				StandardBasicTypes.STRING,
				//	StandardBasicTypes.STRING,
				StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT };

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from (").append(sql).append(" ) ");

		sql.append(" ORDER BY createDate DESC,customerCode,customerName ");
		System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(ImageVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public List<ImageVO> getListImageVOMT(ImageFilter filter)
			throws DataAccessException {		
		StringBuilder sql = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();
		
		StringBuilder sqlTemp = new StringBuilder("");
		sqlTemp.append(" and vp.active = 1 ");
		sqlTemp.append(" and trunc(vp.start_date) <= trunc(sysdate,'dd') ");
		sqlTemp.append(" and (trunc(vp.end_date) >= trunc(sysdate,'dd') or vp.end_date is null) ");
		sql.append(" WITH ");
		if(filter.getLstShop()!=null && filter.getLstShop().size()>0){
			sql.append(" lstShop AS ");
			sql.append(" ( SELECT DISTINCT PS.SHOP_ID ");
			sql.append(" FROM SHOP ps ");
			sql.append(" START WITH ps.SHOP_ID  in ( ");
			for(int i=0;i<filter.getLstShop().size();i++){
				if(i==0) sql.append(" ? ");
				else sql.append(" ,? ");
				params.add(filter.getLstShop().get(i));
			}
			sql.append(" ) CONNECT BY PRIOR ps.SHOP_ID = ps.PARENT_SHOP_ID ");
			sql.append(" ), ");
		}		
		sql.append(" lstTmp as ( ");
		sql.append(" SELECT ");
		sql.append(" MI.* ");
		sql.append(" FROM ");
		sql.append(" MEDIA_ITEM mi ");

		sql.append(" JOIN STAFF st on st.staff_id=mi.staff_id AND st.staff_type_id in (select staff_type_id from staff where staff_type_id in (select staff_type_id from staff_type where specific_type in (1,2))) ");
		sql.append(" where 1=1 ");
		sql.append(" AND MI.STATUS=1 ");
		sql.append(" AND MI.MEDIA_TYPE=0 ");
		//sql.append(" AND MI.TYPE=1 ");
		sql.append(" AND TRUNC(MI.CREATE_DATE)>=trunc(ADD_MONTHS(trunc(SYSDATE,'MM'),-2)) ");
		
		if(filter.getIdCustomer()!=null){
			sql.append(" AND MI.OBJECT_ID =? ");
			params.add(filter.getIdCustomer());
		}		
		//Trung bay 1, 11 - Diem ban 2, 12
		if(null != filter.getLstObjectType() && filter.getLstObjectType().size() > 0){
			sql.append(" AND MI.OBJECT_TYPE IN ( ");
			for(int i = 0; i< filter.getLstObjectType().size(); i++){
				if(i ==0){
					sql.append(" ? ");					
				}else{
					sql.append(", ? ");
				}
				params.add(filter.getLstObjectType().get(i));
			}
			sql.append(" ) ");
		}
		
		if(null!=filter.getStaffIdGroup()){
			sql.append(" AND MI.STAFF_ID =? ");
			params.add(filter.getStaffIdGroup());
		}
		
		if(filter.getLstShop()!=null && filter.getLstShop().size()>0){
			 sql.append(" AND ST.SHOP_ID IN ");
			 sql.append(" ( SELECT SHOP_ID FROM lstShop ");
			 sql.append(" ) ");
		}
		if(filter.getKey()!=null&&filter.getKey()!=-1){
			 
			 sql.append(" AND EXISTS ");
			 sql.append(" ( ");
			 sql.append(" SELECT 1 FROM VISIT_PLAN vp ");
			 sql.append(" WHERE MI.OBJECT_ID=vp.CUSTOMER_ID ");
			 if(filter.getLstStaff()!=null&&!filter.getLstStaff().isEmpty()){
				 sql.append(" AND MI.STAFF_ID=VP.STAFF_ID ");
			 }
			 sql.append(" AND MI.shop_id=VP.Shop_id ");
			 
			 switch (filter.getKey()) {
			case 2:
				sqlTemp.append(" AND VP.MONDAY=1 ");
				break;
			case 3:
				sqlTemp.append(" AND VP.TUESDAY=1 ");
				break;
			case 4:
				sqlTemp.append(" AND VP.WEDNESDAY=1 ");
				break;
			case 5:
				sqlTemp.append(" AND VP.THURSDAY=1 ");
				break;
			case 6:
				sqlTemp.append(" AND VP.FRIDAY=1 ");
				break;
			case 7:
				sqlTemp.append(" AND VP.SATURDAY=1 ");
				break;
			case 8:
				sqlTemp.append(" AND VP.SUNDAY=1 ");
				break;
			}
			 sql.append(sqlTemp);
			 sql.append(" ) ");
			 
		 }
		 
		 if(!StringUtility.isNullOrEmpty(filter.getCodeCustomer())
				 || !StringUtility.isNullOrEmpty(filter.getShortCode())
				 ||!StringUtility.isNullOrEmpty(filter.getNameCustomer())){
			sql.append(" AND MI.OBJECT_ID IN ");
			sql.append(" ( ");
			sql.append(" SELECT cus.CUSTOMER_ID FROM CUSTOMER cus ");
			sql.append(" WHERE 1=1 ");
			
			if(!StringUtility.isNullOrEmpty(filter.getCodeCustomer())){
				sql.append(" AND upper(cus.CUSTOMER_CODE) LIKE ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getCodeCustomer().toUpperCase()));
			}
			if( !StringUtility.isNullOrEmpty(filter.getShortCode())){
//				sql.append(" AND upper(substr(cus.CUSTOMER_CODE,0,3)) LIKE ? ESCAPE '/' ");
//				params.add(StringUtility.toOracleSearchLike(filter.getShortCode().toUpperCase()));
				sql.append(" AND upper(cus.CUSTOMER_CODE) LIKE ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getShortCode().toUpperCase()));
			}
			if(!StringUtility.isNullOrEmpty(filter.getNameCustomer())){
				sql.append(" AND (upper(cus.CUSTOMER_NAME) LIKE ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getNameCustomer().toUpperCase()));
				sql.append(" OR upper(cus.ADDRESS) LIKE ? ESCAPE '/' )");
				params.add(StringUtility.toOracleSearchLike(filter.getNameCustomer().toUpperCase()));
			}
			sql.append(" ) ");
		}
		if(filter.getLstStaff()!=null&&!filter.getLstStaff().isEmpty()){
			sql.append(" AND MI.STAFF_ID IN ( ");
			for (int i = 0; i <filter.getLstStaff().size(); i++) {
				if(i==0){
					sql.append(" ? ");
				}else{
					sql.append(" ,? ");
				}
				params.add(filter.getLstStaff().get(i));
			}
			sql.append(" ) ");
		}
		
		if(filter.getFromDate()!=null){
			 sql.append(" AND TRUNC(MI.CREATE_DATE)>=TRUNC(?) ");
			 params.add(filter.getFromDate());
		}
		if(filter.getToDate()!=null){
			 sql.append(" AND TRUNC(MI.CREATE_DATE)<=TRUNC(?) ");
			 params.add(filter.getToDate());
		}
		
		sql.append(" ) ");
		sql.append(" SELECT MEI.media_item_id AS id, ");
		sql.append(" MEI.URL AS urlImage, ");
		sql.append(" MEI.THUMB_URL AS urlThum, ");
		sql.append(" MEI.CREATE_DATE AS createDate, ");
		//sql.append(" MEI.RESULT AS result, ");
		sql.append(" MEI.LAT AS lat, ");
		sql.append(" MEI.LNG AS lng, ");
		//sql.append(" MEI.Month_Seq AS monthSeq, ");
		sql.append(" MEI.OBJECT_ID AS customerId, ");
		sql.append(" MEI.OBJECT_TYPE as objectType, ");		
		//sql.append(" (SELECT SUBSTR(TRIM(CUS.CUSTOMER_CODE),0,3) FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=MEI.OBJECT_ID) AS customerCode, ");
		sql.append(" (SELECT CUS.SHORT_CODE FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=MEI.OBJECT_ID) AS customerCode, ");
		sql.append(" (SELECT CUS.CUSTOMER_NAME FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=MEI.OBJECT_ID ) AS customerName, ");
		sql.append(" (SELECT CUS.STREET FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=MEI.OBJECT_ID) AS street, ");
		sql.append(" (SELECT CUS.HOUSENUMBER FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=MEI.OBJECT_ID ) AS housenumber, ");
		sql.append(" (SELECT ST.STAFF_ID FROM STAFF ST WHERE ST.STAFF_ID=MEI.STAFF_ID ) AS staffId, ");
		sql.append(" (SELECT ST.STAFF_CODE FROM STAFF ST WHERE ST.STAFF_ID=MEI.STAFF_ID ) AS staffCode, ");
		sql.append(" (SELECT st.staff_name FROM STAFF ST WHERE ST.STAFF_ID=MEI.STAFF_ID  ) AS staffName, ");
//		sql.append(" (SELECT SH.SHOP_NAME FROM SHOP sh WHERE SH.SHOP_ID=MEI.SHOP_ID) as shopName, ");
//		sql.append(" (SELECT SH.SHOP_CODE FROM SHOP sh WHERE SH.SHOP_ID=MEI.SHOP_ID) as shopCode, ");
		sql.append(" (SELECT SH.SHOP_NAME FROM SHOP sh WHERE SH.SHOP_ID=CUS.SHOP_ID) as shopName, ");
		sql.append(" (SELECT SH.SHOP_CODE FROM SHOP sh WHERE SH.SHOP_ID=CUS.SHOP_ID) as shopCode, ");
		sql.append(" (SELECT display_program_code from display_program where display_program_id=mei.display_program_id ) as displayProgrameCode, ");
		sql.append(" (SELECT dpl.level_code FROM DISPLAY_CUSTOMER_MAP sdcm JOIN display_program_level dpl ");
		sql.append("	ON sdcm.display_program_level_id = dpl.display_program_level_id and dpl.status=1 ");
		sql.append("	join display_staff_map dsm on dsm.display_staff_map_id = sdcm.display_staff_map_id and dsm.status=1 ");
		sql.append("	WHERE MEI.OBJECT_ID =  sdcm.CUSTOMER_ID and sdcm.status=1 and dpl.status=1 ");
		sql.append("	AND TRUNC(sdcm.from_date) <=  TRUNC(mei.create_date )AND (sdcm.to_date IS NULL ");
		sql.append("	OR trunc(sdcm.to_date) >= TRUNC(mei.create_date))  ");
		sql.append("	and dsm.display_program_id = mei.display_program_id ");
		sql.append("	and rownum = 1 ) AS levelCode, ");
		sql.append("	cus.lat as custLat, ");
		sql.append("	cus.lng as custLng");
		sql.append(" FROM lstTmp MEI ");
		sql.append(" JOIN Customer cus on mei.object_id=cus.customer_id and cus.status=1 ");
		sql.append(" WHERE 1=1 ");
		
		 String[] fieldNames = { 
					"id",//1
					"urlImage",//2
					"urlThum",//3
					"createDate",//4
					"result",
					"lat",//5
					"lng",//6
					"monthSeq",
					"customerId",//7
					"objectType",
					"customerCode",//8
					"customerName",//9
					"street",//10
					"housenumber",//11
					"staffId",
					"staffCode",//12
					"staffName",//13
					"shopName",//14
					"shopCode",//15
					"displayProgrameCode",
					"levelCode",
					"custLat",
					"custLng"
					};

		Type[] fieldTypes = { 
					StandardBasicTypes.LONG, // 1
					StandardBasicTypes.STRING, // 2
					StandardBasicTypes.STRING, // 3
					StandardBasicTypes.TIMESTAMP,// 4
					StandardBasicTypes.INTEGER,
					StandardBasicTypes.FLOAT, // 5
					StandardBasicTypes.FLOAT, // 6
					StandardBasicTypes.INTEGER, // 6
					StandardBasicTypes.LONG, // 7
					StandardBasicTypes.INTEGER,
					StandardBasicTypes.STRING, // 8
					StandardBasicTypes.STRING, // 9
					StandardBasicTypes.STRING, // 10
					StandardBasicTypes.STRING, // 11
					StandardBasicTypes.LONG,
					StandardBasicTypes.STRING, // 12
					StandardBasicTypes.STRING, // 13
					StandardBasicTypes.STRING, // 14
					StandardBasicTypes.STRING, // 15
					StandardBasicTypes.STRING,
					StandardBasicTypes.STRING,
					StandardBasicTypes.FLOAT,
					StandardBasicTypes.FLOAT
					};
		
		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from (").append(sql).append(" ) ");
		
		sql.append(" ORDER BY createDate DESC,customerCode,customerName ");
		
		if(filter.getkPaging()!=null){
			return repo.getListByQueryAndScalarPaginated(ImageVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}else{
			return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public List<ImageVO> getListImageVOGroup(ImageFilter filter)
			throws DataAccessException {		
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlTemp = new StringBuilder();
//		 sqlTemp.append(" and vp.active = 1 ");
//		 sqlTemp.append(" and trunc(vp.start_date) <= trunc(sysdate,'dd') ");
//		 sqlTemp.append(" and (trunc(vp.end_date) >= trunc(sysdate,'dd') or vp.end_date is null) ");
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		sql.append(" WITH ");
		if(filter.getLstShop()!=null && filter.getLstShop().size()>0){
			sql.append(" lstShop AS ");
			sql.append(" ( SELECT DISTINCT PS.SHOP_ID ");
			sql.append(" FROM SHOP ps ");
			sql.append(" START WITH ps.SHOP_ID  in ( ");
			for(int i=0;i<filter.getLstShop().size();i++){
				if(i==0) sql.append(" ? ");
				else sql.append(" ,? ");
				params.add(filter.getLstShop().get(i));
			}
			sql.append(" ) CONNECT BY PRIOR ps.SHOP_ID = ps.PARENT_SHOP_ID ");
			sql.append(" ), ");
		}
		if(!StringUtility.isNullOrEmpty(filter.getLevel())){
			 sql.append(" levelTmp as (SELECT regexp_substr(?,'[^,]+', 1, LEVEL) as code ");
			 sql.append("  FROM dual CONNECT BY regexp_substr(?, '[^,]+', 1, LEVEL) IS NOT NULL), ");
			 params.add(filter.getLevel().toUpperCase());
			params.add(filter.getLevel().toUpperCase());
		 }
		
		sql.append(" lstTmp as ( ");
		sql.append(" SELECT ");
		sql.append(" MI.* ");
		sql.append(" FROM ");
		sql.append(" MEDIA_ITEM mi");
		if(filter.getObjectType()!=null && filter.getObjectType()==4 ){
			if(!StringUtility.isNullOrEmpty(filter.getLevel())){
				sql.append(" JOIN DISPLAY_CUSTOMER_MAP cm ON MI.OBJECT_ID = CM.CUSTOMER_ID AND cm.status=1 ");
				sql.append(" JOIN display_program_LEVEL dpl ON cm.display_program_level_id=dpl.display_program_level_id and dpl.status=1 ");
				sql.append(" JOIN display_staff_map dsm on dsm.display_staff_map_id = cm.display_staff_map_id and dsm.status=1 ");
				sql.append(" AND dsm.display_program_id = mi.display_program_id ");
				sql.append(" AND trunc(cm.from_date)<=trunc(mi.create_date) and (cm.to_date is null or cm.to_date>=trunc(mi.create_date)) ");
				sql.append(" AND upper(dpl.level_code) in (SELECT * from levelTmp) ");
			}
		}
		sql.append(" JOIN STAFF st on st.staff_id=mi.staff_id AND st.staff_type_id in (select staff_type_id from staff where staff_type_id in (select staff_type_id from staff_type where specific_type in (1,2))) ");
		sql.append(" Where 1=1 ");
		sql.append(" AND MI.STATUS=1 ");
		sql.append(" AND MI.MEDIA_TYPE=0 ");
		//sql.append(" AND MI.TYPE=1 ");
		if(filter.getObjectType()!=null){
			sql.append(" AND MI.OBJECT_TYPE=? ");
			params.add(filter.getObjectType());
		}
		sql.append(" AND TRUNC(MI.CREATE_DATE)>=trunc(ADD_MONTHS(trunc(SYSDATE,'MM'),-2)) ");
		if(filter.getMonthSeq()!=null && filter.getMonthSeq().size()>0){
			sql.append(" AND MI.MONTH_SEQ IN ( ");
			for(int i=0;i<filter.getMonthSeq().size();i++){
				if(i==0) sql.append(" ? ");
				else sql.append(" ,? ");
				params.add(filter.getMonthSeq().get(i));
			}
			sql.append(" ) ");
		}else if(Boolean.TRUE.equals(filter.getLastSeq())){
			sql.append(" AND MI.media_item_id in (select id from media_item where trunc(create_date) in ");
			sql.append(" (select max(trunc(create_date)) from media_item where display_program_id=MI.display_program_id and object_id=mi.object_id)) ");
		}
		if(filter.getLstShop()!=null && filter.getLstShop().size()>0){
			 sql.append(" AND ST.SHOP_ID IN ");
			 sql.append(" ( SELECT SHOP_ID FROM lstShop ");
			 sql.append(" ) ");
		 }
		if(filter.getKey()!=null&&filter.getKey()!=-1){
			 
			 sql.append(" AND EXISTS ");
			 sql.append(" ( ");
			 sql.append(" select 1 from routing r join routing_customer rc on r.routing_id = rc.routing_id ");
			 if(filter.getLstStaff()!=null&&!filter.getLstStaff().isEmpty()){
				 sql.append(" join visit_plan vp on vp.routing_id = r.routing_id ");
			 }
			 sql.append(" WHERE MI.OBJECT_ID=rc.CUSTOMER_ID ");
			 if(filter.getLstStaff()!=null&&!filter.getLstStaff().isEmpty()){
				 sql.append(" AND MI.STAFF_ID=VP.STAFF_ID ");
				 sql.append(" and vp.status = 1 ");
				 sql.append(" AND TRUNC(vp.from_date) <= TRUNC(sysdate,'dd') ");
				sql.append(" AND ((TRUNC(vp.to_date)  >= TRUNC(sysdate,'dd')  or vp.to_date is null )) ");

			 }
			 sql.append(" and r.status = 1 and rc.status = 1 ");
			 sql.append(" AND MI.shop_id=r.Shop_id  ");
			 sql.append(" AND rc.start_date <= TRUNC(sysdate,'dd') ");
			 sql.append(" AND (rc.end_date  >= TRUNC(ADD_MONTHS(TRUNC(SYSDATE,'MM'),-2)) ");
			 sql.append(" OR  rc.end_date           IS NULL) ");

			 
			 switch (filter.getKey()) {
			case 2:
				sqlTemp.append(" AND RC.MONDAY=1 ");
				break;
			case 3:
				sqlTemp.append(" AND RC.TUESDAY=1 ");
				break;
			case 4:
				sqlTemp.append(" AND RC.WEDNESDAY=1 ");
				break;
			case 5:
				sqlTemp.append(" AND RC.THURSDAY=1 ");
				break;
			case 6:
				sqlTemp.append(" AND RC.FRIDAY=1 ");
				break;
			case 7:
				sqlTemp.append(" AND RC.SATURDAY=1 ");
				break;
			case 8:
				sqlTemp.append(" AND RC.SUNDAY=1 ");
				break;
			}
			 sql.append(sqlTemp);
			 sql.append(" ) ");
			 
		 }
//		 if(filter.getKey()!=null&&filter.getKey()!=-1){
//			 
//			 sql.append(" AND EXISTS ");
//			 sql.append(" ( ");
//			 sql.append(" SELECT 1 FROM VISIT_PLAN vp ");
//			 sql.append(" WHERE MI.OBJECT_ID=vp.CUSTOMER_ID ");
//			 if(filter.getLstStaff()!=null&&!filter.getLstStaff().isEmpty()){
//				 sql.append(" AND MI.STAFF_ID=VP.STAFF_ID ");
//			 }
//			 sql.append(" AND MI.shop_id=VP.Shop_id ");
//			 
//			 switch (filter.getKey()) {
//			case 2:
//				sqlTemp.append(" AND VP.MONDAY=1 ");
//				break;
//			case 3:
//				sqlTemp.append(" AND VP.TUESDAY=1 ");
//				break;
//			case 4:
//				sqlTemp.append(" AND VP.WEDNESDAY=1 ");
//				break;
//			case 5:
//				sqlTemp.append(" AND VP.THURSDAY=1 ");
//				break;
//			case 6:
//				sqlTemp.append(" AND VP.FRIDAY=1 ");
//				break;
//			case 7:
//				sqlTemp.append(" AND VP.SATURDAY=1 ");
//				break;
//			case 8:
//				sqlTemp.append(" AND VP.SUNDAY=1 ");
//				break;
//			}
//			 sql.append(sqlTemp);
//			 sql.append(" ) ");
//			 
//		 }
		 
		 if(!StringUtility.isNullOrEmpty(filter.getCodeCustomer())
				 || !StringUtility.isNullOrEmpty(filter.getShortCode())
				 ||!StringUtility.isNullOrEmpty(filter.getNameCustomer())){
				sql.append(" AND MI.OBJECT_ID IN ");
				sql.append(" ( ");
				sql.append(" SELECT cus.CUSTOMER_ID FROM CUSTOMER cus ");
				sql.append(" WHERE 1=1 ");
				
				if(!StringUtility.isNullOrEmpty(filter.getCodeCustomer())){
					sql.append(" AND upper(cus.CUSTOMER_CODE) LIKE ? ESCAPE '/' ");
					params.add(StringUtility.toOracleSearchLike(filter.getCodeCustomer().toUpperCase()));
				}
				if( !StringUtility.isNullOrEmpty(filter.getShortCode())){
					sql.append(" AND upper(substr(cus.CUSTOMER_CODE,0,3)) LIKE ? ESCAPE '/' ");
					params.add(StringUtility.toOracleSearchLike(filter.getShortCode().toUpperCase()));
				}
				if(!StringUtility.isNullOrEmpty(filter.getNameCustomer())){
					sql.append(" AND (upper(cus.CUSTOMER_NAME) LIKE ? ESCAPE '/' ");
					params.add(StringUtility.toOracleSearchLike(filter.getNameCustomer().toUpperCase()));
					sql.append(" OR upper(cus.ADDRESS) LIKE ? ESCAPE '/' )");
					params.add(StringUtility.toOracleSearchLike(filter.getNameCustomer().toUpperCase()));
				}
				sql.append(" ) ");
			}
		 if(filter.getLstStaff()!=null&&!filter.getLstStaff().isEmpty()){
				sql.append(" AND MI.STAFF_ID IN ( ");
				for (int i = 0; i <filter.getLstStaff().size(); i++) {
					if(i==0){
						sql.append(" ? ");
					}else{
						sql.append(" ,? ");
					}
					params.add(filter.getLstStaff().get(i));
				}
				sql.append(" ) ");
			}
		 if(filter.getFromDate()!=null){
			 sql.append(" AND TRUNC(MI.CREATE_DATE)>=TRUNC(?) ");
			 params.add(filter.getFromDate());
		 }
		 if(filter.getToDate()!=null){
			 sql.append(" AND TRUNC(MI.CREATE_DATE)<=TRUNC(?) ");
			 params.add(filter.getToDate());
		 }
		
		 if(filter.getObjectType()!=null && filter.getObjectType()==4 && 
				 filter.getLstDisplayPrograme()!=null&&!filter.getLstDisplayPrograme().isEmpty()){
			 sql.append(" AND MI.DISPLAY_PROGRAM_ID IN (");
			 for (int i = 0; i < filter.getLstDisplayPrograme().size(); i++) {
				if(i==0){
					 sql.append(" ? ");
				}else{
					 sql.append(" ,? ");
				}
				params.add(filter.getLstDisplayPrograme().get(i));
			 }
			 sql.append(" ) ");
		 }
		sql.append(" ) ");
		sql.append(" SELECT ");
		sql.append(" Me.media_item_id id, ");
		sql.append(" ME.url as urlImage, ");
		sql.append(" ME.thumb_url as urlThum, ");
		sql.append(" ME.OBJECT_ID as customerId, ");
		sql.append(" ( ");
		sql.append(" SELECT CUS.SHORT_CODE FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=ME.OBJECT_ID ");
		sql.append(" ) as customerCode, ");
		sql.append(" ( ");
		sql.append(" SELECT CUS.CUSTOMER_NAME FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=ME.OBJECT_ID ");
		sql.append(" )as customerName, ");
		sql.append(" (SELECT ST.STAFF_ID FROM STAFF st WHERE ST.STAFF_ID=ME.STAFF_ID) as staffId, ");
		sql.append(" (SELECT ST.STAFF_CODE FROM STAFF st WHERE ST.STAFF_ID=ME.STAFF_ID) as staffCode, ");
		sql.append(" (SELECT st.staff_name FROM STAFF st WHERE ST.STAFF_ID=ME.STAFF_ID) as staffName, ");
		sql.append(" ME.DISPLAY_PROGRAM_ID as displayProgrameId, ");
		sql.append(" (SELECT SH.SHOP_NAME FROM SHOP sh WHERE SH.SHOP_ID=ME.SHOP_ID) as shopName, ");
		sql.append(" (SELECT SH.SHOP_CODE FROM SHOP sh WHERE SH.SHOP_ID=ME.SHOP_ID) as shopCode, ");
		if(filter.getIsGroupByStaff()!=null && filter.getIsGroupByStaff()){
			sql.append(" (SELECT COUNT(*) FROM lstTmp L1 WHERE L1.STAFF_ID=ME.STAFF_ID) AS countImg ");
		}else{
			sql.append(" (SELECT COUNT(*) FROM lstTmp L1 WHERE L1.OBJECT_ID=ME.OBJECT_ID) AS countImg ");
		}
		sql.append(" FROM lstTmp ME ");
		sql.append(" JOIN Customer cus on me.object_id=cus.customer_id and cus.status=1 ");
		sql.append(" WHERE Me.media_item_id= ");
		if(filter.getIsGroupByStaff()!=null && filter.getIsGroupByStaff()){
			sql.append(" ( SELECT MAX(L2.media_item_id) FROM lstTmp L2 WHERE L2.STAFF_ID=ME.STAFF_ID) ");
			sql.append(" GROUP BY ME.STAFF_ID,ME.OBJECT_ID,ME.shop_id,ME.url,ME.thumb_url,Me.media_item_id,ME.DISPLAY_PROGRAM_ID ");
		}else{
			sql.append(" ( SELECT MAX(L2.media_item_id) FROM lstTmp L2 WHERE L2.OBJECT_ID=ME.OBJECT_ID) ");
			sql.append(" GROUP BY ME.OBJECT_ID,ME.STAFF_ID,ME.shop_id,ME.url,ME.thumb_url,Me.media_item_id,ME.DISPLAY_PROGRAM_ID ");
		}
		
		 String[] fieldNames = { 
					"id",//1
					"urlImage",//2
					"urlThum",//3
					"customerId",//4
					"customerCode",//5
					"customerName",//6
					"staffId",
					"staffCode",
					"staffName",
					"displayProgrameId",//7
					"shopName",
					"shopCode",
					"countImg",//8
					};

		Type[] fieldTypes = { 
					StandardBasicTypes.LONG, // 1
					StandardBasicTypes.STRING, // 2
					StandardBasicTypes.STRING, // 3
					StandardBasicTypes.LONG,// 4
					StandardBasicTypes.STRING, // 5
					StandardBasicTypes.STRING, // 6
					StandardBasicTypes.LONG,
					StandardBasicTypes.STRING,
					StandardBasicTypes.STRING,
					StandardBasicTypes.LONG, // 7
					StandardBasicTypes.STRING,
					StandardBasicTypes.STRING,
					StandardBasicTypes.INTEGER // 8
					};
		 
		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from (").append(sql).append(" ) ");
		
		if(filter.getIsGroupByStaff()!=null && filter.getIsGroupByStaff()){
			sql.append(" ORDER BY staffCode ");
		}else{
			sql.append(" ORDER BY shopCode,customerCode,customerName ");
		}
		System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));
		if(filter.getkPaging()!=null){
			return repo.getListByQueryAndScalarPaginated(ImageVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}else{
			return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public List<ImageVO> getListImageVOGroupMT(ImageFilter filter)
			throws DataAccessException {		
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlTemp = new StringBuilder();
		 sqlTemp.append(" and vp.active = 1 ");
		 sqlTemp.append(" and trunc(vp.start_date) <= trunc(sysdate,'dd') ");
		 sqlTemp.append(" and (trunc(vp.end_date) >= trunc(sysdate,'dd') or vp.end_date is null) ");
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		sql.append(" WITH ");
		if(filter.getLstShop()!=null && filter.getLstShop().size()>0){
			sql.append(" lstShop AS ");
			sql.append(" ( SELECT DISTINCT PS.SHOP_ID ");
			sql.append(" FROM SHOP ps ");
			sql.append(" START WITH ps.SHOP_ID  in ( ");
			for(int i=0;i<filter.getLstShop().size();i++){
				if(i==0) sql.append(" ? ");
				else sql.append(" ,? ");
				params.add(filter.getLstShop().get(i));
			}
			sql.append(" ) CONNECT BY PRIOR ps.SHOP_ID = ps.PARENT_SHOP_ID ");
			sql.append(" ), ");
		}
		
		sql.append(" lstTmp as ( ");
		sql.append(" SELECT ");
		sql.append(" MI.* ");
		sql.append(" FROM ");
		sql.append(" MEDIA_ITEM mi");
		sql.append(" JOIN STAFF st on st.staff_id=mi.staff_id and AND st.staff_type_id in (select staff_type_id from staff where staff_type_id in (select staff_type_id from staff_type where specific_type in (1,2))) ");
		sql.append(" Where 1=1 ");
		sql.append(" AND MI.STATUS=1 ");
		sql.append(" AND MI.MEDIA_TYPE=0 ");
		//sql.append(" AND MI.TYPE=1 ");
		
		if(null != filter.getLstObjectType() && filter.getLstObjectType().size() > 0){
			sql.append(" AND MI.OBJECT_TYPE IN ( ");
			for(int i = 0; i< filter.getLstObjectType().size(); i++){
				if(i ==0){
					sql.append(" ? ");					
				}else{
					sql.append(", ? ");
				}
				params.add(filter.getLstObjectType().get(i));
			}
			sql.append(" ) ");
		}
		
		sql.append(" AND TRUNC(MI.CREATE_DATE)>=trunc(ADD_MONTHS(trunc(SYSDATE,'MM'),-2)) ");
		
		if(filter.getLstShop()!=null && filter.getLstShop().size()>0){
			 sql.append(" AND ST.SHOP_ID IN ");
			 sql.append(" ( SELECT SHOP_ID FROM lstShop ");
			 sql.append(" ) ");
		 }
		 
		 if(filter.getKey()!=null&&filter.getKey()!=-1){
			 
			 sql.append(" AND EXISTS ");
			 sql.append(" ( ");
			 sql.append(" SELECT 1 FROM VISIT_PLAN vp ");
			 sql.append(" WHERE MI.OBJECT_ID=vp.CUSTOMER_ID ");
			 if(filter.getLstStaff()!=null&&!filter.getLstStaff().isEmpty()){
				 sql.append(" AND MI.STAFF_ID=VP.STAFF_ID ");
			 }
			 sql.append(" AND MI.shop_id=VP.Shop_id ");
			 
			 switch (filter.getKey()) {
			case 2:
				sqlTemp.append(" AND VP.MONDAY=1 ");
				break;
			case 3:
				sqlTemp.append(" AND VP.TUESDAY=1 ");
				break;
			case 4:
				sqlTemp.append(" AND VP.WEDNESDAY=1 ");
				break;
			case 5:
				sqlTemp.append(" AND VP.THURSDAY=1 ");
				break;
			case 6:
				sqlTemp.append(" AND VP.FRIDAY=1 ");
				break;
			case 7:
				sqlTemp.append(" AND VP.SATURDAY=1 ");
				break;
			case 8:
				sqlTemp.append(" AND VP.SUNDAY=1 ");
				break;
			}
			 sql.append(sqlTemp);
			 sql.append(" ) ");
			 
		 }
		 
		 if(!StringUtility.isNullOrEmpty(filter.getCodeCustomer())
				 || !StringUtility.isNullOrEmpty(filter.getShortCode())
				 ||!StringUtility.isNullOrEmpty(filter.getNameCustomer())){
				sql.append(" AND MI.OBJECT_ID IN ");
				sql.append(" ( ");
				sql.append(" SELECT cus.CUSTOMER_ID FROM CUSTOMER cus ");
				sql.append(" WHERE 1=1 ");
				
				if(!StringUtility.isNullOrEmpty(filter.getCodeCustomer())){
					sql.append(" AND upper(cus.CUSTOMER_CODE) LIKE ? ESCAPE '/' ");
					params.add(StringUtility.toOracleSearchLike(filter.getCodeCustomer().toUpperCase()));
				}
				if( !StringUtility.isNullOrEmpty(filter.getShortCode())){
					sql.append(" AND upper(substr(cus.CUSTOMER_CODE,0,3)) LIKE ? ESCAPE '/' ");
					params.add(StringUtility.toOracleSearchLike(filter.getShortCode().toUpperCase()));
				}
				if(!StringUtility.isNullOrEmpty(filter.getNameCustomer())){
					sql.append(" AND (upper(cus.CUSTOMER_NAME) LIKE ? ESCAPE '/' ");
					params.add(StringUtility.toOracleSearchLike(filter.getNameCustomer().toUpperCase()));
					sql.append(" OR upper(cus.ADDRESS) LIKE ? ESCAPE '/' )");
					params.add(StringUtility.toOracleSearchLike(filter.getNameCustomer().toUpperCase()));
				}
				sql.append(" ) ");
			}
		 if(filter.getLstStaff()!=null&&!filter.getLstStaff().isEmpty()){
				sql.append(" AND MI.STAFF_ID IN ( ");
				for (int i = 0; i <filter.getLstStaff().size(); i++) {
					if(i==0){
						sql.append(" ? ");
					}else{
						sql.append(" ,? ");
					}
					params.add(filter.getLstStaff().get(i));
				}
				sql.append(" ) ");
			}
		 
		 if(filter.getFromDate()!=null){
			 sql.append(" AND TRUNC(MI.CREATE_DATE)>=TRUNC(?) ");
			 params.add(filter.getFromDate());
		 }
		 if(filter.getToDate()!=null){
			 sql.append(" AND TRUNC(MI.CREATE_DATE)<=TRUNC(?) ");
			 params.add(filter.getToDate());
		 }
		

		sql.append(" ) ");
		sql.append(" SELECT ");
		sql.append(" ME.id, ");
		sql.append(" ME.url as urlImage, ");
		sql.append(" ME.thumb_url as urlThum, ");
		sql.append(" ME.OBJECT_ID as customerId, ");
		sql.append(" ME.OBJECT_TYPE as objectType, ");
		sql.append(" ( ");
		//sql.append(" SELECT SUBSTR(TRIM(CUS.CUSTOMER_CODE),0,3) FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=ME.OBJECT_ID ");
		sql.append(" SELECT CUS.SHORT_CODE FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=ME.OBJECT_ID ");
		sql.append(" ) as customerCode, ");
		sql.append(" ( ");
		sql.append(" SELECT CUS.CUSTOMER_NAME FROM CUSTOMER CUS WHERE CUS.CUSTOMER_ID=ME.OBJECT_ID ");
		sql.append(" )as customerName, ");
		sql.append(" (SELECT ST.STAFF_ID FROM STAFF st WHERE ST.STAFF_ID=ME.STAFF_ID) as staffId, ");
		sql.append(" (SELECT ST.STAFF_CODE FROM STAFF st WHERE ST.STAFF_ID=ME.STAFF_ID) as staffCode, ");
		sql.append(" (SELECT st.staff_name FROM STAFF st WHERE ST.STAFF_ID=ME.STAFF_ID) as staffName, ");
		sql.append(" ME.DISPLAY_PROGRAM_ID as displayProgrameId, ");
		sql.append(" (SELECT SH.SHOP_NAME FROM SHOP sh WHERE SH.SHOP_ID=CUS.SHOP_ID) as shopName, ");
		sql.append(" (SELECT SH.SHOP_CODE FROM SHOP sh WHERE SH.SHOP_ID=CUS.SHOP_ID) as shopCode, ");
		if(filter.getIsGroupByStaff()!=null && filter.getIsGroupByStaff()){
			sql.append(" (SELECT COUNT(*) FROM lstTmp L1 WHERE L1.STAFF_ID=ME.STAFF_ID) AS countImg ");
		}else{
			sql.append(" (SELECT COUNT(*) FROM lstTmp L1 WHERE L1.OBJECT_ID=ME.OBJECT_ID) AS countImg ");
		}
		sql.append(" FROM lstTmp ME ");
		sql.append(" JOIN Customer cus on me.object_id=cus.customer_id and cus.status=1 ");
		sql.append(" WHERE ME.id= ");
		if(filter.getIsGroupByStaff()!=null && filter.getIsGroupByStaff()){
			sql.append(" ( SELECT MAX(L2.id) FROM lstTmp L2 WHERE L2.STAFF_ID=ME.STAFF_ID) ");
			sql.append(" GROUP BY ME.STAFF_ID,ME.OBJECT_ID,ME.OBJECT_TYPE,CUS.shop_id,ME.url,ME.thumb_url,ME.id,ME.DISPLAY_PROGRAM_ID ");
		}else{
			sql.append(" ( SELECT MAX(L2.id) FROM lstTmp L2 WHERE L2.OBJECT_ID=ME.OBJECT_ID) ");
			sql.append(" GROUP BY ME.OBJECT_ID,ME.STAFF_ID,CUS.shop_id,ME.url,ME.thumb_url,ME.id,ME.DISPLAY_PROGRAM_ID,ME.OBJECT_TYPE ");
		}
		
		 String[] fieldNames = { 
					"id",//1
					"urlImage",//2
					"urlThum",//3
					"customerId",//4
					"objectType",
					"customerCode",//5
					"customerName",//6
					"staffId",
					"staffCode",
					"staffName",
					"displayProgrameId",//7
					"shopName",
					"shopCode",
					"countImg",//8
					};

		Type[] fieldTypes = { 
					StandardBasicTypes.LONG, // 1
					StandardBasicTypes.STRING, // 2
					StandardBasicTypes.STRING, // 3
					StandardBasicTypes.LONG,// 4
					StandardBasicTypes.INTEGER,
					StandardBasicTypes.STRING, // 5
					StandardBasicTypes.STRING, // 6
					StandardBasicTypes.LONG,
					StandardBasicTypes.STRING,
					StandardBasicTypes.STRING,
					StandardBasicTypes.LONG, // 7
					StandardBasicTypes.STRING,
					StandardBasicTypes.STRING,
					StandardBasicTypes.INTEGER // 8
					};
		 
		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from (").append(sql).append(" ) ");
		
		if(filter.getIsGroupByStaff()!=null && filter.getIsGroupByStaff()){
			sql.append(" ORDER BY staffCode ");
		}else{
			sql.append(" ORDER BY shopCode,customerCode,customerName ");
		}
		
		if(filter.getkPaging()!=null){
			return repo.getListByQueryAndScalarPaginated(ImageVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}else{
			return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public List<MediaItemDetailVO> getListMediaItemShopVO(
			List<Long> lstCustomerId, Date fromDate, Date toDate,
			String displayProgramCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		
		sql.append(" select fin.id, fin.shopCode, fin.shopName, fin.staffCode, fin.staffName, ");
		sql.append(" fin.customerCode, fin.customerName, fin.customerAddress, fin.displayProgramCode, fin.url,");
		sql.append(" fin.create_date createDate,");
		sql.append(" fin.month_seq monthSeq,");
		sql.append(" (fin.shopCode || '_' || fin.customerCode || '_' || fin.displayProgramCode || '_' || fin.staffCode || '_' || to_char(fin.create_date,'yyyymmddhhmiss')) imageName");
		sql.append(" from(");
		sql.append(" SELECT MI.media_item_id,");
		sql.append("   s.shop_code shopCode,");
		sql.append("   s.shop_code shopName,");
		sql.append("   st.staff_code staffCode,");
		sql.append("   st.staff_code staffName,");
		sql.append("   (SELECT customer_code FROM customer WHERE customer_id = mi.object_id");
		sql.append("   ) customerCode,");
		sql.append("   (SELECT customer_name FROM customer WHERE customer_id = mi.object_id");
		sql.append("   ) customerName,");
		sql.append("   (SELECT address FROM customer WHERE customer_id = mi.object_id");
		sql.append("   ) customerAddress,");
		sql.append("    dp.display_programe_code displayProgramCode,");
		sql.append("   mi.url,");
		sql.append("   mi.create_date,");
		sql.append("   mi.month_seq");
		sql.append(" FROM media_item mi");
		sql.append(" JOIN shop s ON s.shop_id = mi.shop_id");
		sql.append(" JOIN staff st ON st.staff_id            = mi.staff_id");
		sql.append(" join display_programe dp on dp.display_program_id = mi.display_program_id");
		sql.append(" WHERE mi.object_type      = 4");
		
		if(!StringUtility.isNullOrEmpty(displayProgramCode)) {
			sql.append(" AND dp.display_programe_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(displayProgramCode));
		}
		sql.append(" AND mi.create_date        >= TRUNC(?)");
		params.add(fromDate);
		sql.append(" AND mi.create_date        <= TRUNC(?) + 1");
		params.add(toDate);
		if(lstCustomerId != null &&  lstCustomerId.size() > 0){
			StringBuilder tmp = new StringBuilder();
			Boolean isFirst = true;
			for (Long child : lstCustomerId) {
				if(isFirst){
					isFirst = false;
					tmp.append(child);
				}else{
					tmp.append("," + child);
				}
			}
			sql.append(" AND mi.object_id in(");
			sql.append(tmp.toString() + ")");
		}
		
		sql.append(" ) fin");
		sql.append(" order by fin.shopCode, fin.customerCode, fin.displayProgramCode, createDate");

		
		String[] fieldNames = { 
				"id", "shopCode", "shopName", "staffCode", "staffName", 
				"customerCode", "customerName","customerAddress", "monthSeq",
				"displayProgramCode", "url", "createDate", "imageName"
		};

		Type[] fieldTypes = { 
			StandardBasicTypes.LONG,StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
			StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.DATE, StandardBasicTypes.STRING
		};
		
		return repo.getListByQueryAndScalar(MediaItemDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public MediaItem updateMediaItem(MediaItem mediaItem) throws DataAccessException {
		if (mediaItem == null) {
			throw new IllegalArgumentException("mediaItem");
		}
		
		repo.update(mediaItem);
		return this.getMediaItemById(mediaItem.getId());
	}

	/** --------------------------------Begin Quan ly cong van----------------------------- **/
	/**
	 * Danh sach cong van
	 * @author vuongmq
	 */
	@Override
	public List<OfficeDocument> getListOfficeDoc(KPaging<OfficeDocument> kPaging, OfficeDocumentFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (!StaffObjectType.NHVNM.equals(filter.getRoleType())) {
			if (filter.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			sql.append("with lstshop as (select distinct shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
			params.add(filter.getShopId());
		}
		/*if (filter.getShopId() == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		sql.append("with lstshop as (select distinct shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
		params.add(filter.getShopId());*/
		
		sql.append("select * from office_document dp ");
		sql.append(" where 1=1 and dp.status = 1 ");
		if(filter.getType()!=null){
			sql.append(" and type = ? ");
			params.add(filter.getType().getValue());
		}
		if(!StringUtility.isNullOrEmpty(filter.getDocumentCode())) {
			sql.append(" AND upper(DOC_CODE) like ? escape '/' ");
			//params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDocumentCode().toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getDocumentCode().toUpperCase())));
		}
		if(!StringUtility.isNullOrEmpty(filter.getDocumentName())) {
			sql.append(" AND upper(DOC_NAME) like ? escape '/' ");
			//params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDocumentName().toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getDocumentName().toUpperCase())));
		}
		if (filter.getFromDate() != null) {
			sql.append(" and ((to_date is not null and to_date >= trunc(?)) or to_date is null)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and ((from_date is not null and from_date < trunc(?) + 1) or from_date is null)");
			params.add(filter.getToDate());
		}
		if (!StaffObjectType.NHVNM.equals(filter.getRoleType())) {
			sql.append(" and exists(select 1 from office_doc_shop_map dsm where dsm.office_document_id = dp.office_document_id  and dsm.status = 1 and exists (select 1 from lstshop where dsm.shop_id = shop_id))");
		}
		sql.append(" order by FROM_DATE DESC, TO_DATE DESC, DOC_CODE ASC");
		String s = TestUtils.addParamToQuery(sql.toString(), params.toArray());
		System.out.println("*******" + s);
		if (kPaging == null)
			return repo.getListBySQL(OfficeDocument.class, sql.toString(),params);
		else
			return repo.getListBySQLPaginated(OfficeDocument.class,sql.toString(), params, kPaging);
	}
	
	@Override
	public OfficeDocument getOfficeDocumentById(Long id)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select DISTINCT * from office_document ");
		sql.append(" where 1=1 ");
		sql.append(" and office_document_id = ? ");
		params.add(id);
		return repo.getEntityBySQL(OfficeDocument.class, sql.toString(),params);
	}

	@Override
	public Integer insertOffDocShopMap(OfficeDocShopMap docShopMap)	throws DataAccessException {
		Integer kq = 1;
		try{
			if (docShopMap == null) {
				throw new IllegalArgumentException("docShopMap is null");
			}
			repo.create(docShopMap);
		}catch(Exception ex){ 
			kq = 0;
			LogUtility.logError(ex, ex.getMessage());
		}
		return kq;
	}

	@Override
	public List<OfficeDocShopMap> getListOffShopMapToDocumentId(Long documentID)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT DISTINCT * FROM office_doc_shop_map ");
		sql.append(" WHERE   status = 1  AND   ");
		sql.append(" office_document_id = ?   ");
		params.add(documentID);
		return repo.getListBySQL(OfficeDocShopMap.class, sql.toString(),params);
	}
	
	@Override
	public OfficeDocShopMap getListOffShopMapToDocIdShopID(Long documentID, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT DISTINCT * FROM office_doc_shop_map ");
		sql.append(" where status = 1 AND ");
		sql.append(" office_document_id = ? ");
		params.add(documentID);
		sql.append(" AND shop_id = ? ");
		params.add(shopId);
		return repo.getFirstBySQL(OfficeDocShopMap.class, sql.toString(), params);
	}

	@Override
	public Integer updateOffDocShopMap(OfficeDocShopMap docShopMap)	throws DataAccessException {
		Integer kq = 1;
		try {
			repo.update(docShopMap);
		}catch (Exception e) {
			kq = 0;
			LogUtility.logError(e, e.getMessage());
		}
		return kq;
		
	}
	
	@Override
	public OfficeDocument getOfficeDocumentByCode(String documentCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select DISTINCT * from office_document ");
		sql.append(" where 1=1 and status = 1 ");
		sql.append(" and DOC_CODE = ? ");
		params.add(documentCode);
		return repo.getEntityBySQL(OfficeDocument.class, sql.toString(),params);
	}

	@Override
	public OfficeDocument createDocument(OfficeDocument document)
			throws DataAccessException {
		if(document == null) {
			throw new IllegalArgumentException (" document is null");
		}
		repo.create(document);
		return repo.getEntityById(OfficeDocument.class, document.getId());
	}

	@Override
	public MediaItem createMediaItem(MediaItem mediaItem)
			throws DataAccessException {
		if(mediaItem == null) {
			throw new IllegalArgumentException(" mediaItem is null ");
		}
		repo.create(mediaItem);
		return repo.getEntityById(MediaItem.class, mediaItem.getId());
	}

	@Override
	public MediaItem getMediaItemByDocumentID(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select DISTINCT * from MEDIA_ITEM ");
		sql.append(" where 1 = 1 and status = 1 ");
		sql.append(" and OBJECT_ID = ? ");
		params.add(id);
		return repo.getEntityBySQL(MediaItem.class, sql.toString(), params);
	}
	
	@Override
	public MediaItem getMediaItemByDocumentID (Long id, Integer object_type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select DISTINCT * from MEDIA_ITEM ");
		sql.append(" where 1=1 and status =1 ");
		sql.append(" and OBJECT_ID = ? ");
		params.add(id);
		sql.append(" and OBJECT_TYPE = ? ");
		params.add(object_type);
		return repo.getEntityBySQL(MediaItem.class, sql.toString(), params);
	}
	
	@Override
	public Integer deleteOfficeDocument(OfficeDocument officeDocument) throws DataAccessException {
		Integer kq = 1;
		try {
			repo.update(officeDocument);
		} catch (Exception e) {
			kq = 0;
			LogUtility.logError(e, e.getMessage());
		}
		return kq;
	}
	
	/** --------------------------------End Quan ly cong van----------------------------- **/
	
	@Override
	public List<MediaItem> getListMediaItemFeedbackFilter (FeedbackFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from media_item ");
		sql.append(" where status = 1 ");
		if (filter.getType() != null) {
			sql.append(" and object_type = ? ");
			params.add(filter.getType());
		}
		if (filter.getFeedbackId() != null) {
			sql.append(" and object_id = ? ");
			params.add(filter.getFeedbackId());
		}
		sql.append(" order by create_date ");
		return repo.getListBySQL(MediaItem.class, sql.toString(), params);
	}
}

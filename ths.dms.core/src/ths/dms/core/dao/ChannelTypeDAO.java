package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.List;

import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.ChannelTypeVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface ChannelTypeDAO {

	ChannelType createChannelType(ChannelType channelType, LogInfoVO logInfo) throws DataAccessException;

	void deleteChannelType(ChannelType channelType, LogInfoVO logInfo) throws DataAccessException;

	void updateChannelType(ChannelType channelType, LogInfoVO logInfo) throws DataAccessException;
	
	ChannelType getChannelTypeById(long id) throws DataAccessException;

	ChannelType getChannelTypeByCode(String code, ChannelTypeType type)
			throws DataAccessException;
	
	ChannelType getChannelTypeByName(String name, ChannelTypeType type)
			throws DataAccessException;

	Boolean isUsingByOthers(long channelTypeId, ChannelTypeType type)
			throws DataAccessException;

	List<ChannelType> getListDescendantChannelType(
			KPaging<ChannelType> kPaging, Long parentId, ChannelTypeType type)
			throws DataAccessException;

	Boolean isAllChildStoped(long channelTypeId, ChannelTypeType type)
			throws DataAccessException;

	List<ChannelType> getListChannelType(ChannelTypeFilter filter, KPaging<ChannelType> kPaging)
			throws DataAccessException;

	List<ChannelType> getListCustomerTypeNotInPromotionProgram(
			KPaging<ChannelType> kPaging, Long promotionShopMapId)
			throws DataAccessException;

	void updateDescendantChannelType(Long parentId, Integer objectType)
			throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param channelCode
	 * @param type
	 * @return
	 * @throws DataAccessException 
	 */
	Boolean checkExsitedChildChannelType(String channelCode,
			ChannelTypeType type) throws DataAccessException;

	Boolean checkChannelTypeHasAnyChild(Long parentId) throws DataAccessException;

	
	/**
	 * Gets the list descendant channel type order by id.
	 *
	 * @param kPaging the k paging
	 * @param parentId the parent id
	 * @param type the type
	 * @return the list descendant channel type order by id
	 * @throws DataAccessException the data access exception
	 */
	List<ChannelType> getListDescendantChannelTypeOrderById(
			KPaging<ChannelType> kPaging, Long parentId, ChannelTypeType type)
			throws DataAccessException;

	Boolean isUsingByOtherRunningItem(long channelTypeId, ChannelTypeType type)
			throws DataAccessException;

	Boolean checkAncestor(Long parentId, Long channelTypeId)
			throws DataAccessException;

	List<ChannelType> getListSubChannelType(Long parentId,
			ChannelTypeType type, Boolean isOrderByName)
			throws DataAccessException;

	List<ChannelType> getListChannelTypeOrderById(KPaging<ChannelType> kPaging,
			String channelTypeCode, String channelTypeName, Long parentId,
			ChannelTypeType type, ActiveType status, Integer isEdit,
			BigDecimal minSku, BigDecimal minSaleAmount, Integer objectType,
			Boolean isGetOneChildLevel) throws DataAccessException;

	List<ChannelType> getListChannelTypeOrderById(KPaging<ChannelType> kPaging,
			String channelTypeCode, String channelTypeName, Long parentId,
			ChannelTypeType type, ActiveType status, Integer isEdit,
			Integer objectType) throws DataAccessException;
	
	List<ChannelTypeVO> getListChannelTypeVO() throws DataAccessException;
	
	List<ChannelTypeVO> getListChannelTypeVOSaleMT() throws DataAccessException;
	
	//TUNGTT
	List<ChannelTypeVO> getListChannelTypeCustomerCardVO() throws DataAccessException;
	
	List<ChannelTypeVO> getListChannelTypeVOAlreadySet(KPaging<ChannelTypeVO> kPaging, long promotionProgramId) throws DataAccessException;
	
	//TUNGTT
	List<ChannelTypeVO> getListChannelTypeVOCustomerCardAlreadySet(KPaging<ChannelTypeVO> kPaging, long promotionProgramId) throws DataAccessException;
	
	List<ChannelTypeVO> getSaleMTListChannelTypeVOAlreadySet(KPaging<ChannelTypeVO> kPaging, long promotionProgramId) throws DataAccessException;

	/**
	 * Lay danh sach ChannelType theo Type va Danh sach ObjectType
	 * 
	 * @author hunglm16
	 * @since December 18,2014
	 * @description ObjectType = null thi lay tat ca cac ChannelType theo Type
	 * */
	List<ChannelType> getListChannelTypeByTypeAndArrObjType(Integer type, Integer[] objectType) throws DataAccessException;
	
	/**
	 * Lay danh sach ChannelType theo Type
	 * 
	 * @author vuongbd
	 * @since 06/05/2015
	 * */
	List<ChannelType> getListChannelTypeByType(Integer type) throws DataAccessException;
}

package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SalePlan;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.PlanType;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SalePlanOwnerType;
import ths.dms.core.entities.vo.DistributeSalePlanVO;
import ths.dms.core.entities.vo.SalePlanVO;
import ths.dms.core.entities.vo.SumSalePlanVO;
import ths.core.entities.vo.rpt.RptRealTotalSalePlanVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class SalePlanDAOImpl implements SalePlanDAO {

	@Autowired
	private IRepository repo;
	@Autowired
	CommonDAO commonDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SalePlanDAO#createSalePlan(ths.dms.core.entities
	 * .SalePlan)
	 */
	@Override
	public SalePlan createSalePlan(SalePlan salePlan) throws DataAccessException {
		if (salePlan == null) {
			throw new IllegalArgumentException("salePlan");
		}
		salePlan.setCreateDate(commonDAO.getSysDate());
		repo.create(salePlan);
		return repo.getEntityById(SalePlan.class, salePlan.getId());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.dao.SalePlanDAO#createSalePlan(java.util.List)
	 */
	@Override
	public void createSalePlan(List<SalePlan> listSalePlan) throws DataAccessException {
		if (listSalePlan == null) {
			return;
		}
		Date sysDate = commonDAO.getSysDate();
		for (int i = 0; i < listSalePlan.size(); i++) {
			listSalePlan.get(i).setCreateDate(sysDate);
		}

		repo.create(listSalePlan);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SalePlanDAO#deleteSalePlan(ths.dms.core.entities
	 * .SalePlan)
	 */
	@Override
	public void deleteSalePlan(SalePlan salePlan) throws DataAccessException {

		if (salePlan == null) {
			throw new IllegalArgumentException("salePlan");
		}
		repo.delete(salePlan);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SalePlanDAO#deleteSalePlan(ths.dms.core.entities
	 * .SalePlan)
	 */
	@Override
	public void deleteSalePlanById(long id) throws DataAccessException {
		SalePlan salePlan = getSalePlanById(id);
		repo.delete(salePlan);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SalePlanDAO#updateSalePlan(ths.dms.core.entities
	 * .SalePlan)
	 */
	@Override
	public void updateSalePlan(SalePlan salePlan) throws DataAccessException {

		if (salePlan == null) {
			throw new IllegalArgumentException("salePlan");
		}
		repo.update(salePlan);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.dao.SalePlanDAO#getSalePlanById(long)
	 */
	@Override
	public SalePlan getSalePlanById(long id) throws DataAccessException {

		return repo.getEntityById(SalePlan.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.dao.SalePlanDAO#getListSalePlan(java.lang.Long,
	 * java.lang.Long, java.lang.Integer, java.lang.Integer,
	 * ths.dms.core.entities.enumtype.PlanType)
	 */
	@Override
	public List<DistributeSalePlanVO> getListSalePlanWithCondition(Long shopId, Long ownerId, String productCode, Integer num, Integer year, List<String> lstCateCode, PlanType type, KPaging<DistributeSalePlanVO> kPaging) throws DataAccessException {

		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (num == null) {
			throw new IllegalArgumentException("month is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}		
		if (type == null) {
			throw new IllegalArgumentException("type is null");
		}
		StringBuilder fromSql = new StringBuilder();
		StringBuilder sql = new StringBuilder();
		StringBuilder withSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		withSql.append(" WITH sp AS");
		withSql.append("   (SELECT sp.* ");
		withSql.append("   FROM sale_plan sp ");
		withSql.append("   JOIN cycle c on sp.cycle_id = c.cycle_id ");
		withSql.append("   WHERE sp.OBJECT_TYPE = ? ");
		params.add(SalePlanOwnerType.SHOP.getValue());
		withSql.append("   AND sp.object_id = ?");
		params.add(shopId);
		withSql.append("   AND sp.type          = ?");
		params.add(type.getValue());
		withSql.append("   AND c.num = ?");
		params.add(num);
		withSql.append("   AND Extract(year from c.year) = ?");
		params.add(year);
		withSql.append("   )");
		
		withSql.append(" ,lstShop AS");
		withSql.append(" (SELECT shop_id , level AS lv  FROM shop s1  WHERE status = 1 START WITH s1.shop_id = ?");
		params.add(shopId);
		withSql.append(" CONNECT BY prior parent_shop_id = shop_id  ) ,");
		withSql.append(" prc AS  (SELECT product_id, customer_id, customer_type_id, shop_id, shop_type_id, from_date, to_date , price_id");
		withSql.append(" FROM price prc  WHERE 1 = 1 AND prc.status = 1  AND prc.from_date <= sysdate AND (prc.to_date  >= sysdate");
		withSql.append("  OR prc.to_date    IS NULL)  AND (shop_id      IS NULL  OR shop_id        IN");
		withSql.append("  (SELECT shop_id FROM lstShop    ))  ) ,");
		withSql.append("  prcX AS  (SELECT product_id, customer_id, customer_type_id, prc.shop_id, shop_type_id, from_date, to_date , price_id, lstShop.lv,");
		withSql.append("  rank() over(PARTITION BY product_id ORDER BY lstShop.lv) rank_last");
		withSql.append(" FROM prc  JOIN lstShop  ON prc.shop_id              = lstShop.shop_id");
		withSql.append(" WHERE prc.customer_type_id IS NULL  ) ,");
		withSql.append("  prc_rec AS  (SELECT product_id, shop_id, lv, rank_last, from_date, to_date , price_id, customer_id, customer_type_id, shop_type_id");
		withSql.append("  FROM prcX  WHERE rank_last = 1  ) ,");
		withSql.append("  result AS  (SELECT product_id, customer_id, customer_type_id, shop_id, shop_type_id, from_date, to_date , price_id");
		withSql.append("  FROM prc_rec  UNION ALL  SELECT product_id, customer_id, customer_type_id, shop_id,");
		withSql.append("  shop_type_id, from_date, to_date , price_id FROM prc");
		withSql.append(" WHERE customer_type_id IS NULL  AND shop_id            IS NULL  AND shop_type_id        =");
		withSql.append(" (SELECT sh.shop_type_id FROM shop sh WHERE sh.shop_id=prc.shop_id    )  ) ,");
		withSql.append("  resultAll AS  (SELECT result.product_id,    MAX(    CASE      WHEN result.customer_id     IS NULL");
		withSql.append(" AND result.customer_type_id IS NULL AND result.shop_id   IS NOT NULL  AND result.shop_type_id  IS NULL  THEN price_id END ) pr_shop_id,");
		withSql.append("  MAX(CASE WHEN result.customer_id IS NULL  AND result.customer_type_id IS NULL  AND result.shop_id IS NULL AND result.shop_type_id =");
		withSql.append(" (SELECT sh.shop_type_id FROM shop sh WHERE sh.shop_id=result.shop_id        )");
		withSql.append(" THEN price_id    END ) pr_shop_type  FROM result");
		withSql.append(" GROUP BY product_id  ) ,");
		withSql.append("  priceTbl AS  (SELECT product_id,    (    CASE      WHEN pr_shop_id IS NOT NULL");
		withSql.append(" THEN pr_shop_id      WHEN pr_shop_type IS NOT NULL      THEN pr_shop_type      ELSE -1");
		withSql.append("  END ) price_id  FROM resultALL  ) ,");
		withSql.append(" priceTmp AS  (SELECT product_id,    price_id,    (    CASE      WHEN price_id>0");
		withSql.append(" THEN        (SELECT pt.price FROM price pt WHERE pt.price_id=priceTbl.price_id        )");
		withSql.append("  ELSE 0    END) price  FROM priceTbl  )");
		
		sql.append(withSql.toString());
		
		sql.append(" SELECT sp.sale_plan_id salePlanId,");
		sql.append("   pro.product_id productId,");
		sql.append("   pro.product_code productCode,");
		sql.append("   pro.product_name productName,");
		sql.append("   pro.convfact convfact,");
//		sql.append("   (case when sp.sale_plan_id is not null then Extract(month from sp.month_date) || '/' || Extract(year from sp.month_date) else '' end) spMonth,");
		//sql.append("   NVL(sp.quantity,0) quantity,");
		sql.append("   sp.quantity quantity,"); // lacnv1 - nhung thang chua co de null
		sql.append("   NVL(sp.quantity_assigned,0) quantityAssign,");
		sql.append("   NVL(pri.price,0) price,");
		sql.append("   sp.amount amount, sh.shop_code as shopCode "); //NVL(sp.amount,0) amount, sh.shop_code as shopCode
		
		fromSql.append(" FROM product pro");
		fromSql.append(" LEFT JOIN sp ON sp.product_id = pro.product_id");
		fromSql.append(" LEFT JOIN shop sh on sh.shop_id = sp.object_id  and sp.object_type=1 ");
		//fromSql.append(" JOIN price pri ON pri.product_id  = pro.product_id");
		fromSql.append(" left join priceTmp pri ON pri.product_id  = pro.product_id");

		fromSql.append(" JOIN product_info info ON info.product_info_id = pro.cat_id");
		fromSql.append(" WHERE pro.status   = ?");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" AND NOT EXISTS (SELECT 1");
		fromSql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and ap.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" AND ap.ap_param_code  = info.product_info_code");
		fromSql.append(" )");
		//fromSql.append(" AND pri.from_date    <= TRUNC(sysdate)");
		//fromSql.append(" AND (pri.to_date     >= TRUNC(sysdate) OR pri.to_date          IS NULL)");
		//fromSql.append(" AND pri.status     = 1");
		
		if (!StringUtility.isNullOrEmpty(productCode)) {
			fromSql.append(" AND UPPER(pro.product_code) LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(productCode).toUpperCase());
		}
		if (lstCateCode != null && lstCateCode.size() > 0) {
			Boolean isFirst = true;
			fromSql.append(" AND UPPER(info.product_info_code) in(");
			for (String child : lstCateCode) {
				if (isFirst) {
					isFirst = false;
					fromSql.append(" ?");
				} else {
					fromSql.append(", ?");
				}
				params.add(child);
			}
			fromSql.append(" )");
		}
		
		sql.append(fromSql.toString());

		sql.append(" order by productCode");

		String[] fieldNames = new String[] {//
		"salePlanId", "productId", "productCode", "productName", "convfact", "quantity", "quantityAssign", "price", "amount", "shopCode" };
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING };
		if (kPaging != null) {
			//count sql
			StringBuilder countSql = new StringBuilder();
			countSql.append(withSql.toString());
			countSql.append(" select count(*) as count ");
			countSql.append(fromSql.toString());
			return repo.getListByQueryAndScalarPaginated(DistributeSalePlanVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
		return repo.getListByQueryAndScalar(DistributeSalePlanVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<SalePlanVO> getListSalePlanVO(Long shopId, String staffCode, List<String> lstCatCode, String productCode, Integer num, Integer year, Long gsnppId) throws DataAccessException {

		if (num == null) {
			throw new IllegalArgumentException("num of period is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}
		if (staffCode != null) {
			staffCode = staffCode.toUpperCase();
		}
		if (productCode != null) {
			productCode = productCode.toUpperCase();
		}
		if (lstCatCode != null && lstCatCode.size() == 0)
			lstCatCode = null;

		StringBuilder selectSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		//Lay gia theo do uu tien
		selectSql.append(" with lstShop as (select shop_id ,level as lv from shop s1 where status = 1");
		selectSql.append(" start with s1.shop_id = ?");	
		params.add(shopId);
		selectSql.append(" connect by prior parent_shop_id = shop_id) ");
		selectSql.append(" ,prc as ( ");
		selectSql.append(" SELECT	product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		selectSql.append(" FROM	 price	prc WHERE 1 = 1 AND prc.status = 1 ");
		selectSql.append(" and prc.from_date <= sysdate and (prc.to_date >= sysdate or prc.to_date is null) ");
		selectSql.append(" and (shop_id is null or shop_id in (select shop_id from lstShop))) ");
		selectSql.append(" ,prcX as ( ");
		selectSql.append("  select product_id,customer_id,customer_type_id,prc.shop_id,shop_type_id, from_date, to_date ");
		selectSql.append(" ,price_id,lstShop.lv, rank() over(PARTITION BY product_id ORDER BY lstShop.lv) rank_last  ");
		selectSql.append(" from prc join lstShop on prc.shop_id = lstShop.shop_id where prc.customer_type_id is null ) ");
		selectSql.append(" ,prc_rec as( ");
		selectSql.append(" select  product_id,shop_id,lv, rank_last, from_date, to_date ,price_id,customer_id,customer_type_id,shop_type_id ");
		selectSql.append(" from prcX where rank_last = 1) ");
		selectSql.append(" ,result as ( ");
		selectSql.append(" select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		selectSql.append(" from prc_rec union all ");
		selectSql.append(" select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		selectSql.append(" from prc where customer_type_id is null and shop_id is null ");
		selectSql.append(" and shop_type_id = (select sh.shop_type_id from shop sh where sh.shop_id=prc.shop_id)) ");
		selectSql.append(" ,resultAll as ( ");
		selectSql.append(" SELECT result.product_id, max( ");
		selectSql.append(" case when result.customer_id is null and result.customer_type_id is null ");
		selectSql.append(" and result.shop_id is not null and result.shop_type_id is null then price_id end ");
		selectSql.append(" ) pr_shop_id, max( ");
		selectSql.append(" case when result.customer_id is null  and result.customer_type_id is null ");
		selectSql.append(" and result.shop_id  is null and result.shop_type_id = ");
		selectSql.append(" (select sh.shop_type_id from shop sh where sh.shop_id=result.shop_id) ");
		selectSql.append(" then price_id end ) pr_shop_type FROM result  group by product_id) ");
		selectSql.append(" ,priceTbl as (select product_id, (case when pr_shop_id is not null then pr_shop_id ");
		selectSql.append(" when pr_shop_type is not null then pr_shop_type  ");
		selectSql.append(" else -1 end ) price_id from resultALL)");	
		selectSql.append(" ,priceTmp as ( ");
		selectSql.append("  select product_id,price_id, (case when price_id>0 then (select pt.price from price pt where pt.price_id=priceTbl.price_id) ");
		selectSql.append("  else 0 end) price from priceTbl) ");
		selectSql.append(" SELECT a.staff_code staffCode, p.product_code productCode, p.product_name productName, ");
		selectSql.append("  a.quantityNVBH staffQuantity,a.amount amount,  p.price price,  (a.quantityNVBH * p.price) amount,  a.quantity_assigned shopQuantityAssign,  a.quantityNPP shopQuantity, ");
		selectSql.append(" (a.quantityNPP - a.quantity_assigned) shopLeftQuantity,  a.sale_plan_id salePlanId,  a.staff_id staffId,  a.shop_code shopCode,  p.product_id productId, a.cycle_id cycleId ");
		selectSql.append(" FROM (SELECT p1.product_id product_id , product_code , product_name, cat_id, nvl(pr.price,0) price ");
		selectSql.append(" FROM product p1  left JOIN priceTmp pr  ON p1.product_id =pr.product_id  WHERE p1.status  =1 ) p");
		selectSql.append(" LEFT JOIN  (SELECT DECODE(x.product_id,NULL,y.product_id,x.product_id) product_id ,    y.object_id object_id, ");
		selectSql.append(" x.quantity_assigned quantity_assigned,    x.quantity quantityNPP, ");
		selectSql.append(" y.quantity quantityNVBH, y.amount amount,    y.staff_Code staff_code ,    x.sale_plan_id,    y.staff_id,    y.shop_code, x.cycle_id ");
		selectSql.append(" FROM    (SELECT product_id,      quantity_assigned,      quantity, ");
		selectSql.append(" sale_plan_id, sp1.cycle_id ");
		selectSql.append(" FROM sale_plan sp1 JOIN cycle c on sp1.cycle_id = c.cycle_id  WHERE sp1.object_type = ? ");
		params.add(SalePlanOwnerType.SALEMAN.getValue());
		selectSql.append(" AND sp1.type          = 2 ");
		selectSql.append(" AND sp1.object_id     = (select staff_id from staff where staff_code=?) ");
		params.add(staffCode);
		selectSql.append(" AND c.num = ?  ");
		params.add(num);
		selectSql.append(" AND Extract(year from c.year) = ?  ");
		params.add(year);
		selectSql.append(" )x  ");
		selectSql.append(" full join  ");
		selectSql.append(" (SELECT product_id , object_id, quantity , amount, s.staff_code staff_code, s.staff_id, sh.shop_code, quantity_assigned ");
		selectSql.append(" FROM sale_plan sp2 JOIN cycle c on sp2.cycle_id = c.cycle_id JOIN staff s ON sp2.object_id=s.staff_id JOIN shop sh ON s.shop_id         = sh.shop_id");
		selectSql.append("  where sp2.object_type = ? ");
		params.add(SalePlanOwnerType.SALEMAN.getValue());
		selectSql.append(" and sp2.type = 2 ");
		selectSql.append("         and s.shop_id = ? ");
		params.add(shopId);
		//? check staff owner
		//selectSql.append("		   and s.staff_id in (select staff_id from staff where status = 1 and staff_owner_id = ?) ");
		//params.add(gsnppId);
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			selectSql.append("         and s.staff_code = ? ");
			params.add(staffCode.toUpperCase());
		}
		selectSql.append(" AND c.num = ?  ");
		params.add(num);
		selectSql.append(" AND Extract(year from c.year) = ?  ");
		params.add(year);
		selectSql.append("         )y ");
		selectSql.append("      on x.product_id = y.product_id ");
		selectSql.append(" ) a ");
		selectSql.append(" on p.product_id=a.product_id ");
		selectSql.append(" join product_info pi on p.cat_id=pi.product_info_id ");
		selectSql.append(" where 1=1 ");
		//		selectSql.append(" and pi.status=1 ");

		if (lstCatCode != null) {
			selectSql.append(" AND (1=0");
			for (String catCode : lstCatCode) {
				selectSql.append("	or pi.product_info_code = ?");
				params.add(catCode.toUpperCase());
			}
			selectSql.append(")");
		}
		selectSql.append(" AND NOT EXISTS");
		selectSql.append(" (SELECT 1");
		selectSql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and ap.status = 1");
		selectSql.append(" AND ap.ap_param_code  = pi.product_info_code");
		selectSql.append(" )");

		if (!StringUtility.isNullOrEmpty(productCode)) {
			selectSql.append(" and p.product_code like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(productCode).toUpperCase());
		}
		selectSql.append(" order by p.product_code ");
		selectSql.append("  ");
		selectSql.append("  ");

		String[] fieldNames = new String[] {//
				"salePlanId",//1
				"shopCode",//2
				"staffId",//3
				"staffCode",//4
				"productId",//5
				"productCode",//6
				"productName",//7
				"staffQuantity",//8
				"price",//9
				"amount", //10
				"shopQuantity",//11
				"shopQuantityAssign",//12
				"shopLeftQuantity",//13
				"cycleId"//14
		};
		Type[] fieldTypes = new Type[] {//
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.LONG,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.LONG,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.INTEGER,// 8
				StandardBasicTypes.BIG_DECIMAL,// 9
				StandardBasicTypes.BIG_DECIMAL,// 10
				StandardBasicTypes.INTEGER,// 11
				StandardBasicTypes.INTEGER,// 12
				StandardBasicTypes.INTEGER,// 13
				StandardBasicTypes.LONG// 14
		};
		return repo.getListByQueryAndScalar(SalePlanVO.class, fieldNames, fieldTypes, selectSql.toString(), params);
	}

	@Override
	public List<SalePlanVO> getListSalePlanVO4KT(Long shopId, String staffCode, List<String> lstCatCode, String productCode, Integer month, Integer year) throws DataAccessException {

		if (month == null) {
			throw new IllegalArgumentException("month is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}
		if (staffCode != null) {
			staffCode = staffCode.toUpperCase();
		}
		if (productCode != null) {
			productCode = productCode.toUpperCase();
		}
		if (lstCatCode != null && lstCatCode.size() == 0)
			lstCatCode = null;

		StringBuilder selectSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		//Lay gia theo do uu tien
		selectSql.append(" with priceTmp1 as ( ");
		selectSql.append(" 		  select price_id, price, price_not_vat, product_id, orderNo ");
		selectSql.append(" 		  from price pr ");
		selectSql.append(" 		  join (select shop_id, (case ct.object_type when 3 then 1 when 11 then 1 ");
		selectSql.append(" 		   when 2 then 2 when 10 then 2 when 1 then 3 when 9 then 3 when 13 then 3 ");
		selectSql.append(" 		   when 0 then 4 when 8 then 4 when 12 then 4 when 14 then 5 else 10 end) as orderNo ");
		selectSql.append(" 		    from shop s ");
		selectSql.append(" 		    join channel_type ct on (ct.channel_type_id = s.shop_type_id) ");
		selectSql.append(" 		    where s.status = 1 ");
		selectSql.append(" 		    start with shop_id = ? connect by prior parent_shop_id = shop_id ");
		params.add(shopId);
		selectSql.append(" 		  ) sh on (sh.shop_id = pr.shop_id) ");
		selectSql.append(" 		  where status = 1 ");
		selectSql.append(" 		    and from_date < trunc(sysdate) + 1 ");
		selectSql.append(" 		    and (to_date >= trunc(sysdate) or to_date is null) ");
		selectSql.append(" 	    and customer_type_id is null    ");
		selectSql.append(" 		union all ");
		selectSql.append(" 		  select price_id, price, price_not_vat, product_id, (case when shop_channel is not null then 6 else 7 end) as orderNo ");
		selectSql.append(" 		  from price pr ");
		selectSql.append(" 		  where status = 1 ");
		selectSql.append(" 		    and from_date < trunc(sysdate) + 1 ");
		selectSql.append(" 	    and (to_date >= trunc(sysdate) or to_date is null) ");
		selectSql.append(" 	    and customer_type_id is null ");
		selectSql.append(" 		    and shop_id is null ");
		selectSql.append(" 		    and (shop_channel in (select shop_channel from shop where shop_id = ?) or shop_channel is null) ");
		params.add(shopId);
		selectSql.append(" 		), ");
		selectSql.append(" 		priceTmp as ( ");
		selectSql.append(" 		  select price_id, price, price_not_vat, product_id ");
		selectSql.append(" 		  from priceTmp1 pr1 ");
		selectSql.append(" 		  where orderNo <= (select min(orderNo) from priceTmp1 where product_id = pr1.product_id) ");
		selectSql.append(" ) ");

		selectSql.append(" select a.staff_code staffCode,  ");
		selectSql.append("        p.product_code productCode,  ");
		selectSql.append("        p.product_name productName,  ");
		selectSql.append("        a.month month, ");
		selectSql.append("        a.year year, ");
		selectSql.append("        a.quantityNVBH staffQuantity, ");
		selectSql.append("        p.price price, ");
		selectSql.append("        (a.quantityNVBH * p.price) amount, ");
		selectSql.append("        a.quantity_assign shopQuantityAssign, ");
		selectSql.append("        a.quantityNPP shopQuantity, ");
		selectSql.append("        (a.quantityNPP - a.quantity_assign) shopLeftQuantity, ");
		selectSql.append("        a.sale_plan_id salePlanId, ");
		selectSql.append("        a.staff_id staffId, ");
		selectSql.append("        a.shop_code shopCode, ");
		selectSql.append("        p.product_id productId ");
		selectSql.append(" from  ");
		selectSql.append("  (select p1.product_id product_id , product_code , product_name, cat_id, pr.price price ");
		selectSql.append("          from product p1 join priceTmp pr on p1.product_id =pr.product_id  ");
		selectSql.append("         where p1.status =1 ");
//		selectSql.append("         and pr.status=1 ");
//		selectSql.append("         and pr.from_date <= trunc(sysdate)  ");
//		selectSql.append("         and (pr.to_date >= trunc(sysdate) or pr.to_date is null)  ");
		selectSql.append("     ) p ");
		selectSql.append(" left join  ");
		selectSql.append(" (select DECODE(x.product_id,NULL,y.product_id,x.product_id) product_id , y.object_id object_id, x.quantity_assign quantity_assign, x.quantity quantityNPP, DECODE(x.month,NULL,y.month,x.month) month, DECODE(x.year,NULL,y.year,x.year) year, y.quantity quantityNVBH, y.staff_Code staff_code ");
		selectSql.append("       , x.sale_plan_id, y.staff_id, y.shop_code ");
		selectSql.append(" from (select product_id, quantity_assign, quantity, month, YEAR, sale_plan_id ");
		selectSql.append("          from sale_plan sp1  ");
		selectSql.append("          where sp1.object_type =1 and sp1.type=2 ");
		selectSql.append("          and sp1.object_id= ?  ");
		params.add(shopId);
		selectSql.append("          and sp1.month = ?  ");
		params.add(month);
		selectSql.append("          and sp1.year = ?  ");
		params.add(year);
		selectSql.append("           )x  ");
		selectSql.append("      full join  ");
		selectSql.append("         (select product_id , object_id,quantity , s.staff_code staff_code, s.staff_id, sh.shop_code, quantity_assign, MONTH, YEAR ");
		selectSql.append("         from sale_plan sp2 join staff s on sp2.object_id=s.staff_id join shop sh on s.shop_id = sh.shop_id ");
		selectSql.append("         where sp2.object_type=2 and sp2.type=2 ");
		selectSql.append("         and s.shop_id = ? ");
		params.add(shopId);
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			selectSql.append("         and s.staff_code = ? ");
			params.add(staffCode.toUpperCase());
		}
		selectSql.append("         and sp2.month = ?  ");
		params.add(month);
		selectSql.append("         and sp2.year = ?  ");
		params.add(year);
		selectSql.append("         )y ");
		selectSql.append("      on x.product_id = y.product_id ");
		selectSql.append(" ) a ");
		selectSql.append(" on p.product_id=a.product_id ");
		selectSql.append(" join product_info pi on p.cat_id=pi.product_info_id ");
		selectSql.append(" where 1=1 ");
		//		selectSql.append(" and pi.status=1 ");

		if (lstCatCode != null) {
			selectSql.append(" AND (1=0");
			for (String catCode : lstCatCode) {
				selectSql.append("	or pi.product_info_code = ?");
				params.add(catCode.toUpperCase());
			}
			selectSql.append(")");
		}
		selectSql.append(" AND NOT EXISTS");
		selectSql.append(" (SELECT 1");
		selectSql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and ap.status = 1");
		selectSql.append(" AND ap.ap_param_code  = pi.product_info_code");
		selectSql.append(" )");

		if (!StringUtility.isNullOrEmpty(productCode)) {
			selectSql.append(" and p.product_code like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(productCode).toUpperCase());
		}
		selectSql.append(" order by p.product_code ");
		selectSql.append("  ");
		selectSql.append("  ");

		String[] fieldNames = new String[] {//
		"salePlanId",//1
				"shopCode",//2
				"staffId",//3
				"staffCode",//4
				"productId",//5
				"productCode",//6
				"productName",//7
				"month",//8
				"year",//9
				"staffQuantity",//10
				"price",//11
				"amount", //
				"shopQuantity",//12
				"shopQuantityAssign",//13
				"shopLeftQuantity"//14
		};
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.LONG,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.LONG,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.INTEGER,// 8
				StandardBasicTypes.INTEGER,// 9
				StandardBasicTypes.INTEGER,// 10
				StandardBasicTypes.BIG_DECIMAL,// 11
				StandardBasicTypes.BIG_DECIMAL,// 11
				StandardBasicTypes.INTEGER,// 12
				StandardBasicTypes.INTEGER,// 13
				StandardBasicTypes.INTEGER,// 14
		};
		return repo.getListByQueryAndScalar(SalePlanVO.class, fieldNames, fieldTypes, selectSql.toString(), params);
	}

	@Override
	public SalePlan getSalePlanOfVNM(Long ownerId, Long productId, SalePlanOwnerType ownerType, PlanType planType, Integer num, Integer year) throws DataAccessException {
		if (ownerId == null) {
			throw new IllegalArgumentException("ownerId is null");
		}
		if (productId == null) {
			throw new IllegalArgumentException("productId is null");
		}
		if (ownerType == null) {
			throw new IllegalArgumentException("ownerType is null");
		}
		if (planType == null) {
			throw new IllegalArgumentException("planType is null");
		}
		if (num == null) {
			throw new IllegalArgumentException("month is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select cp.* from SALE_PLAN cp join cycle c on cp.cycle_id = c.cycle_id where 1 = 1");
		sql.append(" and cp.object_id = ?");
		params.add(ownerId);
		sql.append(" and cp.object_type = ?");
		params.add(ownerType.getValue());
		sql.append(" and cp.product_id = ?");
		params.add(productId);
		sql.append(" and cp.type = ?");
		params.add(planType.getValue());
		sql.append(" and c.num = ?");
		params.add(num);
		sql.append(" and Extract(year from c.year) = ?");
		params.add(year);
		return repo.getFirstBySQL(SalePlan.class, sql.toString(), params);
	}

	@Override
	public SumSalePlanVO getSumSalePlan(Long ownerId, Long productId, SalePlanOwnerType shopType, PlanType planType, Integer month, Integer year) throws DataAccessException {
		if (ownerId == null) {
			throw new IllegalArgumentException("ownerId is null");
		}
		if (month == null) {
			throw new IllegalArgumentException("month is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}
		Calendar cal = Calendar.getInstance();
		cal.set(year, 01, 01);
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sum(sp.quantity) totalSalePlanQty,");
		sql.append(" sum(sp.quantity_assigned) assignSalePlanQty,");
		sql.append(" sum(sp.amount) totalSalePlanAmt,");
		sql.append(" sum(sp.amount_assigned) assignSalePlanAmt");
		sql.append(" from SALE_PLAN sp join CYCLE c on sp.cycle_id = c.cycle_id  where 1 = 1");
		sql.append(" and sp.object_id = ?");
		params.add(ownerId);
		sql.append(" and sp.object_type = ?");
		params.add(shopType.getValue());
		if (productId != null) {
			sql.append(" and sp.product_id = ?");
			params.add(productId);
		}
		sql.append(" and sp.type = ?");
		params.add(planType.getValue());
		sql.append(" and c.num = ?");
		params.add(month);
		sql.append(" and c.year = ?");
		params.add(cal.getTime());
		sql.append(" and sp.quantity > 0");
		sql.append(" group by sp.object_id");

		String[] fieldNames = new String[] { "totalSalePlanQty", "assignSalePlanQty", "totalSalePlanAmt", "assignSalePlanAmt" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL };
		List<SumSalePlanVO> tmp = repo.getListByQueryAndScalar(SumSalePlanVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (tmp == null || tmp.size() == 0) {
			return null;
		}
		return tmp.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SalePlanDAO#getListRealTotalSalePlan(java.lang.Long,
	 * java.lang.Integer, java.lang.Integer, java.lang.Long)
	 */
	@Override
	public List<RptRealTotalSalePlanVO> getListRptRealTotalSalePlan(Long shopId, Integer month, Integer year, String staffCode) throws DataAccessException {
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (month == null) {
			throw new IllegalArgumentException("month is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT tb1.product_id AS productId,");
		sql.append(" tb1.quantity        AS planQuantity,");
		sql.append(" tb1.amount          AS planAmount,");
		sql.append(" tb1.product_code    AS productCode,");
		sql.append(" tb1.product_name    AS productName,");
		sql.append(" tb2.is_free_item    AS isFreeItem,");
		sql.append(" tb2.quantity        AS realQuantity,");
		sql.append(" tb2.amount          AS realAmount,");
		sql.append(" tb2.price           AS priceValue,");
		sql.append(" tb1.staff_code      AS staffCode,");
		sql.append(" tb1.staff_name      AS staffName");
		sql.append(" FROM");
		sql.append(" (SELECT sp.quantity,");
		sql.append(" sp.amount, p.product_id, p.product_code, p.product_name, s.staff_code, s.staff_name");
		sql.append(" FROM sale_plan sp, product p, staff s");
		sql.append(" WHERE object_id IN");
		sql.append(" (SELECT staff_id FROM staff WHERE shop_id = ?)");
		sql.append(" AND sp.object_id   = s.staff_id AND sp.month = ? AND sp.year = ?");
		params.add(shopId);
		params.add(month);
		params.add(year);
		if (staffCode != null) {
			sql.append(" AND lower(s.staff_code) = lower(?)");
			params.add(staffCode);
		}
		sql.append(" AND sp.owner_type = ? AND p.product_id  = sp.product_id ORDER BY p.product_code");
		params.add(SalePlanOwnerType.SALEMAN.getValue());
		sql.append(" ) tb1");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT so.order_date, sod.is_free_item, sod.quantity,");
		sql.append(" sod.amount, sod.price, sod.product_id");
		sql.append(" FROM sale_order so, sale_order_detail sod");
		sql.append(" WHERE so.sale_order_id = sod.sale_order_id AND so.order_type = ? AND so.approved = ?");
		params.add(OrderType.IN.getValue());
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append(" AND so.shop_id = ?");
		sql.append(" AND so.order_date >= trunc(?, 'MONTH')");
		sql.append(" AND so.order_date < trunc(?, 'MONTH') + interval '1' month");
		params.add(shopId);
		Calendar date = Calendar.getInstance();
		date.set(year, month - 1, 1);
		params.add(date.getTime());
		params.add(date.getTime());
		sql.append(" ) tb2 ON tb1.product_id = tb2.product_id");
		final String[] fieldNames = new String[] { //
		"productId",// 1
				"planQuantity", // 2
				"planAmount", // 3
				"productCode", // 3
				"productName",//4
				"isFreeItem", // 5
				"realQuantity", // 6
				"realAmount", // 7
				"priceValue", // 8
				"staffCode", // 9
				"staffName", // 10
		};

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.LONG,// 1
				StandardBasicTypes.INTEGER,// 2
				StandardBasicTypes.BIG_DECIMAL,// 3
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.INTEGER,// 6
				StandardBasicTypes.BIG_DECIMAL,// 7
				StandardBasicTypes.FLOAT,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.STRING,// 10
		};
		return repo.getListByQueryAndScalar(RptRealTotalSalePlanVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<SalePlanVO> getListSalePlanVOProductOfShop(KPaging<SalePlanVO> kPaging, Long shopId, String staffCode, String catCode, String productCode, String productName, Integer month, Integer year, Integer fromQty, Integer toQty)
			throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (month == null) {
			throw new IllegalArgumentException("month is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}
		if (staffCode != null) {
			staffCode = staffCode.toUpperCase();
		}
		if (catCode != null) {
			catCode = catCode.toUpperCase();
		}
		if (productCode != null) {
			productCode = productCode.toUpperCase();
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlSelect = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		List<Object> countParams = new ArrayList<Object>();

		countSql.append(" SELECT count(1) as count ");

		sqlSelect.append(" SELECT sale_plan_staff_tmp.salePlanId      AS salePlanId,");
		sqlSelect.append("   sale_plan_staff_tmp.staffId              AS staffId,");
		sqlSelect.append("   sale_plan_staff_tmp.staffCode            AS staffCode,");
		sqlSelect.append("   pd.product_id                            AS productId,");
		sqlSelect.append("   pd.product_id                            AS id,");//LamNH
		sqlSelect.append("   pd.product_code                          AS productCode,");
		sqlSelect.append("   pd.product_name                          AS productName,");
		sqlSelect.append("   pif.product_info_code                    AS productInfoCode,");
		sqlSelect.append("   pif.product_info_name                    AS productInfoName,");
		sqlSelect.append("   ?                                        AS month,");
		params.add(month);
		sqlSelect.append("   ?                                        AS year,");
		params.add(year);
		sqlSelect.append("   NVL(sale_plan_staff_tmp.quantity, 0)     AS staffQuantity,");
		sqlSelect.append("   NVL(sale_plan_staff_tmp.price, pr.price) AS price,");
		sqlSelect.append("   NVL(sale_plan_staff_tmp.quantity, 0) * NVL(sale_plan_staff_tmp.price, pr.price) AS amount,");
		sqlSelect.append("   NVL(sale_plan_shop_tmp.quantity, 0)      AS shopQuantity,");
		sqlSelect.append("   NVL(sale_plan_shop_tmp.quantityAssign, 0) AS shopQuantityAssign,");
		sqlSelect.append("   NVL(sale_plan_shop_tmp.leftQuantity, 0)   AS shopLeftQuantity");

		sql.append(" FROM product pd");
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT pd.product_id                              AS productId,");
		sql.append("   	 pd.product_id                                    AS id,");//LamNH
		sql.append("     NVL(sl.quantity, 0)                              AS quantity,");
		sql.append("     NVL(sl.quantity_assign, 0)                       AS quantityAssign,");
		sql.append("     NVL(sl.quantity, 0) - NVL(sl.quantity_assign, 0) AS leftQuantity");
		sql.append("   FROM sale_plan sl,");
		sql.append("     product pd");
		sql.append("   WHERE 1           = 1");
		sql.append("   AND sl.object_type = ?");
		params.add(SalePlanOwnerType.SHOP.getValue());
		countParams.add(SalePlanOwnerType.SHOP.getValue());
		sql.append("   AND sl.object_id   = ?");
		params.add(shopId);
		countParams.add(shopId);
		sql.append("   AND sl.product_id = pd.product_id");
		sql.append("   AND sl.month      = ?");
		params.add(month);
		countParams.add(month);
		sql.append("   AND sl.year       = ?");
		params.add(year);
		countParams.add(year);
		sql.append("   ) sale_plan_shop_tmp");
		sql.append(" ON (sale_plan_shop_tmp.productId = pd.product_id)");
		sql.append(" JOIN price pr");
		sql.append(" ON (pr.status = ? and pd.product_id        = pr.product_id");
		params.add(ActiveType.RUNNING.getValue());
		countParams.add(ActiveType.RUNNING.getValue());
		sql.append(" AND pr.from_date < TRUNC(sysdate) + 1");
		sql.append(" AND (pr.to_date         IS NULL");
		sql.append(" OR pr.to_date    >= TRUNC(sysdate)))");
		sql.append(" JOIN product_info pif");
		if (!StringUtility.isNullOrEmpty(catCode)) {
			sql.append(" ON (pd.cat_id = pif.product_info_id AND pif.product_info_code = ?)");
			params.add(catCode);
			countParams.add(catCode);
		} else {
			sql.append(" ON (pd.cat_id = pif.product_info_id)");
		}
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT sl.sale_plan_id AS salePlanId,");
		sql.append("     s.staff_id            AS staffId,");
		sql.append("     s.staff_code          AS staffCode,");
		sql.append("     sl.product_id         AS productId,");
		sql.append("     sl.product_id         AS id,");//LamNH
		sql.append("     sl.quantity           AS quantity,");
		sql.append("     sl.price              AS price");
		sql.append("   FROM sale_plan sl,");
		sql.append("     staff s");
		sql.append("   WHERE 1           = 1");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append("   AND UPPER(s.staff_code)     = ?");
			params.add(staffCode);
			countParams.add(staffCode);
		} else {
			sql.append("   AND 1 = 2");
		}
		sql.append("   AND s.status      = 1");
		sql.append("   AND sl.object_type = ?");
		params.add(SalePlanOwnerType.SALEMAN.getValue());
		countParams.add(SalePlanOwnerType.SALEMAN.getValue());
		sql.append("   AND sl.object_id   = s.staff_id");
		sql.append("   AND sl.month      = ?");
		params.add(month);
		countParams.add(month);
		sql.append("   AND sl.year       = ?");
		params.add(year);
		countParams.add(year);
		sql.append("   ) sale_plan_staff_tmp");
		sql.append(" ON (sale_plan_staff_tmp.productId = pd.product_id)");
		sql.append(" WHERE pd.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		countParams.add(ActiveType.RUNNING.getValue());
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" AND UPPER(pd.product_code) LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(productCode).toUpperCase());
			countParams.add(StringUtility.toOracleSearchLike(productCode).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" AND UPPER(pd.product_name) LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(productName).toUpperCase());
			countParams.add(StringUtility.toOracleSearchLike(productName).toUpperCase());
		}
		if (fromQty != null) {
			sql.append(" AND sale_plan_shop_tmp.leftQuantity >= ?");
			params.add(fromQty);
			countParams.add(fromQty);
		}
		if (toQty != null) {
			sql.append(" AND sale_plan_shop_tmp.leftQuantity <= ?");
			params.add(toQty);
			countParams.add(toQty);
		}
		sql.append(" ORDER BY pd.product_code");
		// count sql
		sqlSelect.append(sql);
		countSql.append(sql);

		String[] fieldNames = new String[] {//
		"salePlanId",// 1
				"staffId",// 3
				"staffCode",// 4
				"productId",// 5
				"id",//LamNH
				"productCode",// 6
				"productName",// 7
				"productInfoCode",// 6
				"productInfoName",// 6
				"month",// 8
				"year",// 9
				"staffQuantity",// 10
				"price",// 11
				"amount",// 11
				"shopQuantity",// 12
				"shopQuantityAssign",// 13
				"shopLeftQuantity"// 14
		};
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.LONG,// 1
				StandardBasicTypes.LONG,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.LONG,// 5
				StandardBasicTypes.LONG,// LamNH
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.INTEGER,// 8
				StandardBasicTypes.INTEGER,// 9
				StandardBasicTypes.INTEGER,// 10
				StandardBasicTypes.BIG_DECIMAL,// 11
				StandardBasicTypes.BIG_DECIMAL,// 11
				StandardBasicTypes.INTEGER,// 12
				StandardBasicTypes.INTEGER,// 13
				StandardBasicTypes.INTEGER,// 14
		};
		if (kPaging != null)
			return repo.getListByQueryAndScalarPaginated(SalePlanVO.class, fieldNames, fieldTypes, sqlSelect.toString(), countSql.toString(), params, countParams, kPaging);
		else
			return repo.getListByQueryAndScalar(SalePlanVO.class, fieldNames, fieldTypes, sqlSelect.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SalePlanDAO#getListSalePlanVOProductOfParentShop
	 * (ths.dms.core.entities.enumtype.KPaging, java.lang.Long,
	 * java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer,
	 * java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<SalePlanVO> getListSalePlanVOProductOfParentShop(KPaging<SalePlanVO> kPaging, Long shopId, String productCode, String productName, Integer month, Integer year, Integer fromQty, Integer toQty) throws DataAccessException {

		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (month == null) {
			throw new IllegalArgumentException("month is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}
		if (productCode != null) {
			productCode = productCode.toUpperCase();
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlSelect = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		List<Object> countParams = new ArrayList<Object>();

		countSql.append(" SELECT count(1) as count ");

		sqlSelect.append(" SELECT sale_plan_staff_tmp.salePlanId      AS salePlanId,");
		sqlSelect.append("   pd.product_id                            AS productId,");
		sqlSelect.append("   pd.product_id                            AS id,");//LamNH
		sqlSelect.append("   pd.product_code                          AS productCode,");
		sqlSelect.append("   pd.product_name                          AS productName,");
		sqlSelect.append("   pif.product_info_code                    AS productInfoCode,");
		sqlSelect.append("   pif.product_info_name                    AS productInfoName,");
		sqlSelect.append("   ?                                        AS month,");
		params.add(month);
		sqlSelect.append("   ?                                        AS year,");
		params.add(year);
		sqlSelect.append("   NVL(sale_plan_staff_tmp.quantity, 0)     AS staffQuantity,");
		sqlSelect.append("   NVL(sale_plan_staff_tmp.price, pr.price) AS price,");
		sqlSelect.append("   NVL(sale_plan_staff_tmp.quantity, 0) * NVL(sale_plan_staff_tmp.price, pr.price) AS amount,");
		sqlSelect.append("   sale_plan_shop_tmp.quantity              AS shopQuantity,");
		sqlSelect.append("   sale_plan_shop_tmp.quantityAssign        AS shopQuantityAssign,");
		sqlSelect.append("   sale_plan_shop_tmp.leftQuantity          AS shopLeftQuantity");

		sql.append(" FROM product pd");
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT pd.product_id                              AS productId,");
		sql.append("     pd.product_id                                    AS id,");//LamNH
		sql.append("     NVL(sl.quantity, 0)                              AS quantity,");
		sql.append("     NVL(sl.quantity_assign, 0)                       AS quantityAssign,");
		sql.append("     NVL(sl.quantity, 0) - NVL(sl.quantity_assign, 0) AS leftQuantity");
		sql.append("   FROM sale_plan sl,");
		sql.append("     product pd");
		sql.append("   WHERE 1           = 1");
		sql.append("   AND sl.object_type = ?");
		params.add(SalePlanOwnerType.SHOP.getValue());
		countParams.add(SalePlanOwnerType.SHOP.getValue());
		sql.append("   AND sl.object_id   = (select parent_shop_id from shop where shop_id = ?)");
		params.add(shopId);
		countParams.add(shopId);
		sql.append("   AND sl.product_id = pd.product_id");
		sql.append("   AND sl.month      = ?");
		params.add(month);
		countParams.add(month);
		sql.append("   AND sl.year       = ?");
		params.add(year);
		countParams.add(year);
		sql.append("   ) sale_plan_shop_tmp");
		sql.append(" ON (sale_plan_shop_tmp.productId = pd.product_id)");
		sql.append(" JOIN price pr");
		sql.append(" ON (pd.product_id        = pr.product_id");
		sql.append(" AND pr.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		countParams.add(ActiveType.RUNNING.getValue());
		sql.append(" AND pr.from_date < TRUNC(sysdate) + 1");
		sql.append(" AND (pr.to_date         IS NULL");
		sql.append(" OR pr.to_date    >= TRUNC(sysdate)))");
		sql.append(" JOIN product_info pif");
		sql.append(" ON (pd.cat_id = pif.product_info_id)");
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT sl.sale_plan_id AS salePlanId,");
		sql.append("     sl.product_id         AS productId,");
		sql.append("     sl.product_id         AS id,");//LamNH
		sql.append("     sl.quantity           AS quantity,");
		sql.append("     sl.price              AS price");
		sql.append("   FROM sale_plan sl ");
		sql.append("   WHERE 1           = 1");
		sql.append("   AND sl.object_type = ?");
		params.add(SalePlanOwnerType.SHOP.getValue());
		countParams.add(SalePlanOwnerType.SHOP.getValue());
		sql.append("   AND sl.object_id   = ?");
		params.add(shopId);
		countParams.add(shopId);
		sql.append("   AND sl.month      = ?");
		params.add(month);
		countParams.add(month);
		sql.append("   AND sl.year       = ?");
		params.add(year);
		countParams.add(year);
		sql.append("   ) sale_plan_staff_tmp");
		sql.append(" ON (sale_plan_staff_tmp.productId = pd.product_id)");
		sql.append(" WHERE pd.status = 1");
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" AND UPPER(pd.product_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(productCode).toUpperCase());
			countParams.add(StringUtility.toOracleSearchLike(productCode).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" AND UPPER(pd.product_name) LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(productName).toUpperCase());
			countParams.add(StringUtility.toOracleSearchLike(productName).toUpperCase());
		}
		if (fromQty != null) {
			sql.append(" AND sale_plan_shop_tmp.leftQuantity >= ?");
			params.add(fromQty);
			countParams.add(fromQty);
		}
		if (toQty != null) {
			sql.append(" AND sale_plan_shop_tmp.leftQuantity <= ?");
			params.add(toQty);
			countParams.add(toQty);
		}
		sql.append(" ORDER BY pd.product_code");
		// count sql
		sqlSelect.append(sql);
		countSql.append(sql);

		String[] fieldNames = new String[] {//
		"salePlanId",// 1
				"productId",// 2
				"id",// LamNH
				"productCode",// 3
				"productName",// 4
				"productInfoCode",// 5
				"productInfoName",// 6
				"month",// 7
				"year",// 8
				"staffQuantity",// 9
				"price",// 10
				"amount",// 11
				"shopQuantity",// 12
				"shopQuantityAssign",// 13
				"shopLeftQuantity"// 14
		};
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.LONG,// 1
				StandardBasicTypes.LONG,// 2
				StandardBasicTypes.LONG,// LamNH
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.INTEGER,// 8
				StandardBasicTypes.INTEGER,// 9
				StandardBasicTypes.BIG_DECIMAL,// 10
				StandardBasicTypes.BIG_DECIMAL,// 11
				StandardBasicTypes.INTEGER,// 12
				StandardBasicTypes.INTEGER,// 13
				StandardBasicTypes.INTEGER,// 14
		};
		if (kPaging != null)
			return repo.getListByQueryAndScalarPaginated(SalePlanVO.class, fieldNames, fieldTypes, sqlSelect.toString(), countSql.toString(), params, countParams, kPaging);
		else
			return repo.getListByQueryAndScalar(SalePlanVO.class, fieldNames, fieldTypes, sqlSelect.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SalePlanDAO#getSalePlanByCondition(java.lang.Long,
	 * java.lang.String, java.lang.Integer, java.lang.Integer,
	 * ths.dms.core.entities.enumtype.PlanType)
	 */
	@Override
	public SalePlan getSalePlanByCondition(Long ownerId, String catCode, Integer month, Integer year, PlanType type) throws DataAccessException {

		if (ownerId == null) {
			throw new IllegalArgumentException("ownerId is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sp.* from sale_plan sp");
		if (!StringUtility.isNullOrEmpty(catCode)) {
			sql.append(", product_info pif");
		}
		sql.append(" where sp.object_id = ? and sp.object_type = ?");
		params.add(ownerId);
		params.add(SalePlanOwnerType.SHOP.getValue());
		sql.append(" and sp.year = ?");
		params.add(year);
		if (!StringUtility.isNullOrEmpty(catCode)) {
			sql.append(" and pif.product_info_id = sp.cat_id");
			sql.append(" and lower(pif.product_info_code) = ?");
			params.add(catCode.toLowerCase());
		}
		if (month != null) {
			sql.append(" and sp.month = ?");
			params.add(month);
		}
		if (type != null) {
			sql.append(" and sp.type = ?");
			params.add(type.getValue());
		}
		return repo.getEntityBySQL(SalePlan.class, sql.toString(), params);
	}

	@Override
	public Boolean checkSalePlan(String objectCode, SalePlanOwnerType objectType, String productCode, Integer month, Integer year) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from sale_plan sp where 1 = 1");
		if (objectType != null) {
			sql.append(" and object_type = ? ");
			params.add(objectType.getValue());
		}
		if (!StringUtility.isNullOrEmpty(objectCode)) {
			if (SalePlanOwnerType.SHOP.equals(objectType)) {
				sql.append(" AND exists (select 1 from shop where status != ? and shop_code = ?)");
				params.add(ActiveType.DELETED.getValue());
				params.add(objectCode.toUpperCase());
			} else if (SalePlanOwnerType.SALEMAN.equals(objectType)) {
				sql.append(" AND exists (select 1 from staff where status != ? and staff_code = ?)");
				params.add(ActiveType.DELETED.getValue());
				params.add(objectCode.toUpperCase());
			}
		}
		if (month != null) {
			sql.append(" and month = ? ");
			params.add(month);
		}

		if (year != null) {
			sql.append(" and year = ? ");
			params.add(year);
		}

		if (!StringUtility.isNullOrEmpty(productCode)) {

		}

		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public List<DistributeSalePlanVO> getListSalePlanWithNoCondition(KPaging<DistributeSalePlanVO> kPaging, String productCode, String productName) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		StringBuilder withSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		withSql.append("with priceTmp1 as (");
		withSql.append(" select price_id, price, product_id,");
		withSql.append(" (row_number() over (partition by product_id order by product_id)) as rowNo");
		withSql.append(" from price");
		withSql.append(" where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		withSql.append(" and from_date < trunc(sysdate) + 1");
		withSql.append(" and (to_date >= trunc(sysdate) or to_date is null)");
		withSql.append(" ),");
		withSql.append(" priceTmp as (");
		withSql.append(" select price_id, price, product_id");
		withSql.append(" from priceTmp1");
		withSql.append(" where rowNo = 1");
		withSql.append(" )");
		
		sql.append(withSql.toString());
		
		sql.append(" SELECT pro.product_id as productId,");
		sql.append("   pro.product_code as productCode,");
		sql.append("   pro.product_name as productName,");
		sql.append("   pro.convfact as convfact,");
		sql.append("   pri.price as price");
		
		fromSql.append(" FROM product pro");
		//fromSql.append(" JOIN price pri ON (pri.product_id    = pro.product_id");
		//fromSql.append(" AND pri.from_date    <= TRUNC(sysdate)");
		//fromSql.append(" AND (pri.to_date     >= TRUNC(sysdate) OR pri.to_date          IS NULL)");
		//fromSql.append(" AND pri.status       = 1");
		//fromSql.append(")");
		fromSql.append(" join priceTmp pri on (pri.product_id = pro.product_id)");
		fromSql.append("  join product_info pi on pi.product_info_id = pro.cat_id");
		fromSql.append(" WHERE 1 = 1");
		fromSql.append(" AND pro.status       = ?");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" AND NOT EXISTS");
		fromSql.append(" (SELECT 1");
		fromSql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and ap.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" AND ap.ap_param_code  = pi.product_info_code");
		fromSql.append(" )");

		if (!StringUtility.isNullOrEmpty(productCode)) {
			fromSql.append(" and pro.product_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(productName)) {
			fromSql.append(" and upper(pro.product_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(productCode.toUpperCase()));
		}

		sql.append(fromSql.toString());
		sql.append(" order by productCode");

		String[] fieldNames = new String[] {//
		"productId", "productCode", "productName", "convfact", "price" };
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL };
		if (kPaging != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(withSql.toString());
			countSql.append(" select count(1) as count");
			countSql.append(fromSql.toString());
			return repo.getListByQueryAndScalarPaginated(DistributeSalePlanVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
		return repo.getListByQueryAndScalar(DistributeSalePlanVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<SalePlan> getListSalePlan(KPaging<SalePlan> paging, String staffCode, List<String> lstCatCode, String productCode, Integer month, Integer year) throws DataAccessException {

		if (lstCatCode != null && lstCatCode.size() == 0)
			lstCatCode = null;

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select kk.* from (select * from sale_plan sp where sp.object_type = 2 ");

		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and exists (select 1 from staff where staff_id = sp.object_id and status = 1 and staff_code like ? escape '/')");
			params.add(StringUtility.toOracleSearchLike(staffCode));
		}

		if (lstCatCode != null) {
			sql.append(" AND exists (select 1 from product_info where product_info_id = sp.cat_id and (1=0");
			for (String catCode : lstCatCode) {
				sql.append("	or product_info_code = ?");
				params.add(catCode.toUpperCase());
			}
			sql.append(") )");
		}

		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and exists (select 1 from product p where product_id = sp.product_id and p.product_code like ? escape '/' )");
			params.add(StringUtility.toOracleSearchLike(productCode).toUpperCase());
		}

		if (month != null) {
			sql.append(" and Extract(month from sp.month_date) = ?");
			params.add(month);
		}

		if (year != null) {
			sql.append(" and Extract(year from sp.month_date) = ?");
			params.add(year);
		}
		sql.append(" ) kk join product pr on kk.product_id = pr.product_id and pr.status =1 order by pr.product_code");

		if (paging == null)
			return repo.getListBySQL(SalePlan.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(SalePlan.class, sql.toString(), params, paging);
	}

	@Override
	public SalePlan getSalePlan(String staffCode, String productCode, Integer num, Integer year) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(staffCode) || StringUtility.isNullOrEmpty(productCode) || num == null || year == null) {
			throw new IllegalArgumentException("IllegalArgumentException");
		}		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sp.* from sale_plan sp join cycle c on sp.cycle_id = c.cycle_id where sp.object_type = ? ");
		params.add(SalePlanOwnerType.SALEMAN.getValue());
		sql.append("and sp.object_id = (select staff_id from staff where status != -1 and staff_code = ?) ");
		params.add(staffCode.toUpperCase());
		sql.append("   and sp.product_id in (select product_id from product where status != -1 and product_code = ?) ");
		params.add(productCode.toUpperCase());
		sql.append("   and c.num = ? ");
		params.add(num);
		sql.append("   and Extract(year from c.year) = ? ");
		params.add(year);
		return repo.getFirstBySQL(SalePlan.class, sql.toString(), params);
	}

	@Override
	public List<SalePlanVO> getListSalePlanVOForLoadPage(KPaging<SalePlanVO> paging, Long shopId, Integer month, Integer year) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		//Lay gia theo do uu tien
		sql.append(" with priceTmp1 as ( ");
		sql.append(" 		  select price_id, price, price_not_vat, product_id, orderNo ");
		sql.append(" 		  from price pr ");
		sql.append(" 		  join (select shop_id, (case ct.object_type when 3 then 1 when 11 then 1 ");
		sql.append(" 		   when 2 then 2 when 10 then 2 when 1 then 3 when 9 then 3 when 13 then 3 ");
		sql.append(" 		   when 0 then 4 when 8 then 4 when 12 then 4 when 14 then 5 else 10 end) as orderNo ");
		sql.append(" 		    from shop s ");
		sql.append(" 		    join channel_type ct on (ct.channel_type_id = s.shop_type_id) ");
		sql.append(" 		    where s.status = 1 ");
		sql.append(" 		    start with shop_id = ? connect by prior parent_shop_id = shop_id ");
		params.add(shopId);
		sql.append(" 		  ) sh on (sh.shop_id = pr.shop_id) ");
		sql.append(" 		  where status = 1 ");
		sql.append(" 		    and from_date < trunc(sysdate) + 1 ");
		sql.append(" 		    and (to_date >= trunc(sysdate) or to_date is null) ");
		sql.append(" 	    and customer_type_id is null    ");
		sql.append(" 		union all ");
		sql.append(" 		  select price_id, price, price_not_vat, product_id, (case when shop_channel is not null then 6 else 7 end) as orderNo ");
		sql.append(" 		  from price pr ");
		sql.append(" 		  where status = 1 ");
		sql.append(" 		    and from_date < trunc(sysdate) + 1 ");
		sql.append(" 	    and (to_date >= trunc(sysdate) or to_date is null) ");
		sql.append(" 	    and customer_type_id is null ");
		sql.append(" 		    and shop_id is null ");
		sql.append(" 		    and (shop_channel in (select shop_channel from shop where shop_id = ?) or shop_channel is null) ");
		params.add(shopId);
		sql.append(" 		), ");
		sql.append(" 		priceTmp as ( ");
		sql.append(" 		  select price_id, price, price_not_vat, product_id ");
		sql.append(" 		  from priceTmp1 pr1 ");
		sql.append(" 		  where orderNo <= (select min(orderNo) from priceTmp1 where product_id = pr1.product_id) ");
		sql.append(" ) ");

		sql.append(" select p.product_id productId, product_code productCode,product_name productName, pr.price, sp.quantity_assign shopQuantityAssign, sp.quantity shopQuantity");
		sql.append(" from product p join priceTmp pr on p.product_id = pr.product_id ");
		sql.append(" left join sale_plan sp on sp.product_id=p.product_id and sp.object_type=1  ");
		sql.append(" and sp.object_id = ? ");
		params.add(shopId);
		sql.append(" and month = ? ");
		params.add(month);
		sql.append(" and year = ? ");
		params.add(year);
		sql.append(" join product_info pi on pi.product_info_id = p.cat_id  ");
		sql.append(" where 1=1 ");
		sql.append(" and p.status =1 ");
		//sql.append(" and pr.status =1 ");
		//sql.append(" and pr.from_date < trunc(sysdate) +1 ");
		//sql.append(" and (pr.to_date >= trunc(sysdate) or to_date is null) ");
		sql.append(" and pi.product_info_code not in (select ap_param_code from ap_param where status = 1 and type = 'EQUIPMENT_CAT') ");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from (");
		countSql.append(sql);
		countSql.append(" )");

		sql.append(" order by p.product_code asc ");

		String[] fieldNames = { "productId", "productCode", "productName", "price", "shopQuantityAssign", "shopQuantity" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };

		if (paging == null)
			return repo.getListByQueryAndScalar(SalePlanVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(SalePlanVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, paging);
	}

	@Override
	public Integer getVersion(Long ownerId, Long productId, SalePlanOwnerType ownerType, PlanType planType, Integer month, Integer year) throws DataAccessException {
		if (ownerId == null) {
			throw new IllegalArgumentException("ownerId is null");
		}
		if (productId == null) {
			throw new IllegalArgumentException("productId is null");
		}
		if (ownerType == null) {
			throw new IllegalArgumentException("ownerType is null");
		}
		if (planType == null) {
			throw new IllegalArgumentException("planType is null");
		}
		if (month == null) {
			throw new IllegalArgumentException("month is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select max(version) from sale_plan_version where sale_plan_id = spn.sale_plan_id) as version ");
		sql.append(" from SALE_PLAN spn where 1 = 1");
		sql.append(" and spn.object_id = ?");
		params.add(ownerId);
		sql.append(" and spn.object_type = ?");
		params.add(ownerType.getValue());
		sql.append(" and spn.product_id = ?");
		params.add(productId);
		sql.append(" and spn.type = ?");
		params.add(planType.getValue());
		sql.append(" and spn.month = ?");
		params.add(month);
		sql.append(" and spn.year = ?");
		params.add(year);
		BigDecimal tmp = (BigDecimal) repo.getObjectByQuery(sql.toString(), params);
		if (tmp != null) {
			return tmp.intValue();
		}
		return null;
	}

}

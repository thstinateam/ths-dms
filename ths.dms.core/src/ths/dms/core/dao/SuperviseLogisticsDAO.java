package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.RoutingCar;
import ths.dms.core.entities.RoutingCarCust;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.CarFilter;
import ths.dms.core.entities.vo.CarVO;
import ths.dms.core.entities.vo.LatLngVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

/**
 * 
 * @author tungmt
 * @since 15/4/2014 
 * @description: quan ly xe giao hang cho vnm
 */
public interface SuperviseLogisticsDAO {
	/**
	 * 
	 * @author tungmt
	 * @since 15/4/2014 
	 * @description: 
	 */
	void createListCustomerArea(List<LatLngVO> lst, LogInfoVO logInfo) throws DataAccessException, BusinessException;
	void deleteCustomerArea(Long shopId, LogInfoVO logInfo) throws DataAccessException;
	
	List<CarVO> getListShopCar(Long parentShopId) throws DataAccessException;
	List<CarVO> getListCar(Long parentShopId) throws DataAccessException;
	List<CarVO> getListShopCarOneNode(Long parentShopId) throws DataAccessException;
	List<Shop> getListAncestor(Long parentShopId,String shopCode,String shopName) throws DataAccessException;
	List<CarVO> getListCarKP(CarFilter filter) throws DataAccessException;//lan tuyen
	List<CarVO> getListCarVungKP(CarFilter filter) throws DataAccessException;//lan vung
	List<CarVO> getListRoutingExistKP(CarFilter filter) throws DataAccessException;
	
	List<CarVO> getListCustShop(Long parentShopId) throws DataAccessException;
	List<CarVO> getListCarPosition(Long parentShopId,Long maxLogId) throws DataAccessException;
	List<CarVO> getListRoutingCustPosition(CarFilter filter) throws DataAccessException;
	List<CarVO> getListRoutingCustStatus(CarFilter filter) throws DataAccessException;
	List<LatLngVO> getListCustomerArea(Long parentShopId) throws DataAccessException;
	
	List<LatLngVO> getListCustomerByShop(CarFilter filter) throws DataAccessException;
	
	RoutingCar getRoutingCarById(long id) throws DataAccessException;
	RoutingCarCust getRoutingCarCustById(long id) throws DataAccessException;
	RoutingCar createRoutingCar(RoutingCar rcar, LogInfoVO logInfo) throws DataAccessException;
	RoutingCarCust createRoutingCarCust(RoutingCarCust rcar, LogInfoVO logInfo) throws DataAccessException;
	void updateRoutingCar(RoutingCar rcar, LogInfoVO logInfo) throws DataAccessException;
	void updateRoutingCarCust(RoutingCarCust rcar, LogInfoVO logInfo) throws DataAccessException;
	List<CarVO> getListCustomerByRouting(CarFilter filter) throws DataAccessException;
	List<CarVO> getListCustomerBySaleOrder(CarFilter filter) throws DataAccessException;
	List<CarVO> getListRouting(CarFilter filter) throws DataAccessException;
	Boolean checkDriverHasCar(Long idRouting,Long staffId,Date deliveryDate) throws DataAccessException;
	
	List<RoutingCar> getRoutingCarByFilter(CarFilter filter) throws DataAccessException;
	
	List<CarVO> getBCVT11(CarFilter filter) throws DataAccessException;
	List<CarVO> getBCVT12(CarFilter filter) throws DataAccessException;
	List<CarVO> getBCVT13(CarFilter filter) throws DataAccessException;
	List<CarVO> getBCVT14(CarFilter filter) throws DataAccessException;
	List<CarVO> getBCVT15(CarFilter filter) throws DataAccessException;
	List<CarVO> getBCVT16(CarFilter filter) throws DataAccessException;
}

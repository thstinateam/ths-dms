package ths.dms.core.dao;

import java.util.Date;

import ths.dms.core.entities.RptDebitMonth;
import ths.dms.core.entities.enumtype.RptDebitMonthObjectType;
import ths.dms.core.exceptions.DataAccessException;

public interface RptDebitMonthDAO {

	RptDebitMonth createRptDebitMonth(RptDebitMonth rptDebitMonth) throws DataAccessException;

	void deleteRptDebitMonth(RptDebitMonth rptDebitMonth) throws DataAccessException;

	void updateRptDebitMonth(RptDebitMonth rptDebitMonth) throws DataAccessException;
	
	RptDebitMonth getRptDebitMonthById(long id) throws DataAccessException;

	RptDebitMonth getRptDebitMonthByOwner(Long ownerId, RptDebitMonthObjectType ownerType, Date date)
			throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.GroupStaffPayrollDetail;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class GroupStaffPayrollDetailDAOImpl implements
		GroupStaffPayrollDetailDAO {
	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public GroupStaffPayrollDetail createGroupStaffPayrollDetail(
			GroupStaffPayrollDetail groupStaffPayrollDetail, LogInfoVO logInfo)
			throws DataAccessException {
		if (groupStaffPayrollDetail == null) {
			throw new IllegalArgumentException(
					ExceptionCode.GROUP_STAFF_PAYROLL_DETAIL_INSTANCE_IS_NULL);
		}
		if (logInfo == null) {
			throw new IllegalArgumentException(ExceptionCode.LOG_INFO_IS_NULL);
		}

		repo.create(groupStaffPayrollDetail);

		groupStaffPayrollDetail = repo.getEntityById(
				GroupStaffPayrollDetail.class, groupStaffPayrollDetail.getId());

		try {
			actionGeneralLogDAO.createActionGeneralLog(null,
					groupStaffPayrollDetail, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}

		return groupStaffPayrollDetail;
	}

	@Override
	public void deleteGroupStaffPayrollDetail(
			GroupStaffPayrollDetail groupStaffPayrollDetail, LogInfoVO logInfo)
			throws DataAccessException {
		if (groupStaffPayrollDetail == null) {
			throw new IllegalArgumentException(
					ExceptionCode.GROUP_STAFF_PAYROLL_DETAIL_INSTANCE_IS_NULL);
		}

		repo.delete(groupStaffPayrollDetail);

		try {
			actionGeneralLogDAO.createActionGeneralLog(groupStaffPayrollDetail,
					null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public void updateGroupStaffPayrollDetail(
			GroupStaffPayrollDetail groupStaffPayrollDetail, LogInfoVO logInfo)
			throws DataAccessException {
		if (groupStaffPayrollDetail == null) {
			throw new IllegalArgumentException(
					ExceptionCode.GROUP_STAFF_PAYROLL_DETAIL_INSTANCE_IS_NULL);
		}
		if (logInfo == null) {
			throw new IllegalArgumentException(ExceptionCode.LOG_INFO_IS_NULL);
		}

		GroupStaffPayrollDetail temp = this
				.getGroupStaffPayrollDetailById(groupStaffPayrollDetail.getId());

		repo.update(groupStaffPayrollDetail);

		try {
			actionGeneralLogDAO.createActionGeneralLog(temp,
					groupStaffPayrollDetail, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public GroupStaffPayrollDetail getGroupStaffPayrollDetailById(long id)
			throws DataAccessException {
		return repo.getEntityById(GroupStaffPayrollDetail.class, id);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.GroupStaffPayrollDetailDAO#getListGroupStaffPayrollDetail(java.lang.Long)
	 */
	@Override
	public List<GroupStaffPayrollDetail> getListGroupStaffPayrollDetail(KPaging<GroupStaffPayrollDetail> kPaging,
			Long groupStaffPayrollId) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select gspd.* from GROUP_STAFF_PAYROLL_DETAIL gspd, STAFF s where gspd.GROUP_STAFF_PAYROLL_ID = ?");
		params.add(groupStaffPayrollId);
		sql.append(" and gspd.STAFF_ID = s.STAFF_ID and s.STATUS = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (null == kPaging) {
			return repo.getListBySQL(GroupStaffPayrollDetail.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(GroupStaffPayrollDetail.class, sql.toString(),
					params, kPaging);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.GroupStaffPayrollDetailDAO#getListStaffWithOutGroupStaffPayroll(java.lang.Long, java.lang.String, java.lang.String)
	 */
	@Override
	public List<Staff> getListStaffWithOutGroupStaffPayroll(KPaging<Staff> kPaging, 
			Long groupStaffPayrollId, String staffCode, String staffName,
			Long staffTypeId, Long shopId) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *");
		sql.append(" FROM staff WHERE 1 = 1");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and upper(STAFF_CODE) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and upper(STAFF_NAME) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (groupStaffPayrollId != null) {
			sql.append(" AND staff_id NOT IN");
			sql.append(" (SELECT staff_id FROM group_staff_payroll_detail WHERE GROUP_STAFF_PAYROLL_ID = ?)");
			params.add(groupStaffPayrollId);
		}
		sql.append(" AND status        = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (staffTypeId != null) {
			sql.append(" AND staff_type_id = ?");
			params.add(staffTypeId);
		}
		if (shopId != null) {
			sql.append(" AND shop_id      IN");
			sql.append(" (SELECT shop_id");
			sql.append(" FROM shop");
			sql.append(" START WITH shop_id              = ?");
			params.add(shopId);
			sql.append(" CONNECT BY prior parent_shop_id = shop_id)");
			
		}
		sql.append(" order by STAFF_CODE");
		if (null == kPaging) {
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(Staff.class, sql.toString(),
					params, kPaging);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.GroupStaffPayrollDetailDAO#getGroupStaffPayrollDetail(java.lang.Long, java.lang.Long)
	 */
	@Override
	public GroupStaffPayrollDetail getGroupStaffPayrollDetail(
			Long groupStaffPayrollId, Long staffId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from GROUP_STAFF_PAYROLL_DETAIL  where GROUP_STAFF_PAYROLL_ID = ?");
		params.add(groupStaffPayrollId);
		sql.append(" AND STAFF_ID = ?");
		params.add(staffId);
		return repo.getEntityBySQL(GroupStaffPayrollDetail.class, sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.StDisplayProgramLevel;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface DisplayProgrameLevelDAO {
	/**
	 * 
	 * @param kPaging
	 * @param displayProgramId
	 * @param levelCode
	 * @param status
	 * @return
	 * @throws DataAccessException
	 * @author loctt
	 * @since 18sep2013
	 */
	List<StDisplayProgramLevel> getListDisplayProgramLevel(
			KPaging<StDisplayProgramLevel> kPaging, Long displayProgramId,
			String levelCode, ActiveType status)
			throws DataAccessException;
	/**
	 * @author loctt
	 * @since 19sep2013
	 * @param id
	 * @return
	 * @throws DataAccessException
	 */
	StDisplayProgramLevel getDisplayProgramLevelById(long id) throws DataAccessException;
	/**
	 * @author loctt
	 * @since 19sep2013
	 * @param displayProgramLevel
	 * @param logInfo
	 * @throws DataAccessException
	 */
	void updateDisplayProgramLevel(StDisplayProgramLevel displayProgramLevel, LogInfoVO logInfo) throws DataAccessException;
	/**
	 * @author loctt
	 * @since 19sep2013
	 * @param displayProgramLevel
	 * @param logInfo
	 * @return
	 * @throws DataAccessException
	 */
	StDisplayProgramLevel createDisplayProgramLevel(StDisplayProgramLevel displayProgramLevel, LogInfoVO logInfo) throws DataAccessException;
	/**
	 * @author loctt
	 * @since 19sep2013
	 * @param displayProgramId
	 * @param levelCode
	 * @param status
	 * @return
	 * @throws DataAccessException
	 */
	StDisplayProgramLevel getDisplayProgramLevelByLevelCode(Long displayProgramId, String levelCode, 
			ActiveType status) throws DataAccessException;
}

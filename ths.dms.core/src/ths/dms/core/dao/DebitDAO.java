package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Debit;
import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.DebitPaymentType;
import ths.dms.core.entities.enumtype.DebitType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.DebitFilter;
import ths.dms.core.entities.vo.DebitCustomerVO;
import ths.core.entities.vo.rpt.RptBCTCNDetailVO;
import ths.core.entities.vo.rpt.RptDebitPaymentVO;
import ths.dms.core.exceptions.DataAccessException;

public interface DebitDAO {

	Debit createDebit(Debit debit) throws DataAccessException;

	void deleteDebit(Debit debit) throws DataAccessException;

	void updateDebit(Debit debit) throws DataAccessException;
	
	void updateDebit(List<Debit> debit) throws DataAccessException;

	Debit getDebitById(long id) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param ownerId
	 * @param ownerType
	 * @return
	 * @throws DataAccessException
	 */
	Debit getDebit(Long ownerId, DebitOwnerType ownerType)
			throws DataAccessException;

	Debit getDebitForUpdate(Long ownerId, DebitOwnerType ownerType)
			throws DataAccessException;

	/**
	 * Xem cong no khach hang Module 3.1.6.4
	 * 
	 * @param customerId
	 * @param customerCode
	 * @param customerName
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<DebitCustomerVO> getDebitCustomer(Long shopId, Long customerId,
			String customerCode, String customerName, Date fromDate, Date toDate, DebitPaymentType type)
			throws DataAccessException;

	Date getLongestDebtSaleOrder(Long ownerId, DebitOwnerType ownerType)
			throws DataAccessException;

	/**
	 * Lay thong tin cong no cua mot khach hang
	 * 
	 * @param shopId
	 * @param customerId
	 * @return
	 * @throws DataAccessException
	 * @author thuattq
	 */
	DebitDetail getLastDebitDetailHaveRemain(Long ownerId, DebitOwnerType ownerType)
			throws DataAccessException;
	
	List<RptDebitPaymentVO> getListDebitPayment(Date fromDate, Date toDate, Long shopId, 
			List<Long> staffIds, List<Long> customerIds, Integer type) throws DataAccessException;

	List<RptBCTCNDetailVO> getRptBCTCNDetailVO(Long shopId, List<Long> lstCustomerId,
			List<Long> lstStaffId, StockObjectType objectType) throws DataAccessException;
	
	/**
	 * Lay tong cong no
	 * 
	 * @author lacnv1
	 * @since Mar 26, 2014
	 */
	BigDecimal getTotalDebit(Long ownerId, DebitOwnerType ownerType) throws DataAccessException;
	
	/**
	 * Xem cong no khach hang Module 3.1.6.4
	 * 
	 * @author lacnv1
	 * @since Mar 27, 2014
	 * @see getDebitCustomer
	 */
	List<DebitCustomerVO> getListDebitCustomerVO(DebitFilter<DebitCustomerVO> filter) throws DataAccessException;
	
	BigDecimal getCurrentDebit(Long objectId, DebitOwnerType ownerType, DebitType debitType) throws DataAccessException;
	
	List<Debit> getListDebitByFilter(SoFilter filter) throws DataAccessException;
}

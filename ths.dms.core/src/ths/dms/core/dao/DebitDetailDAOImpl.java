package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.DebitDetailTemp;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.enumtype.DebitDetailType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.filter.DebitFilter;
import ths.dms.core.entities.vo.CustomerDebitVO;
import ths.dms.core.entities.vo.DebitShopVO;
import ths.core.entities.vo.rpt.RptDebtOfCustomerDataVO;
import ths.core.entities.vo.rpt.RptLimitDebitVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class DebitDetailDAOImpl implements DebitDetailDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public DebitDetail createDebitDetail(DebitDetail debitDetail) throws DataAccessException {
		if (debitDetail == null) {
			throw new IllegalArgumentException("debit");
		}
		repo.create(debitDetail);
		return repo.getEntityById(DebitDetail.class, debitDetail.getId());
	}

	@Override
	public void deleteDebitDetail(DebitDetail debitDetail) throws DataAccessException {
		if (debitDetail == null) {
			throw new IllegalArgumentException("debitDetail");
		}
		repo.delete(debitDetail);
	}

	@Override
	public DebitDetail getDebitDetailById(long id) throws DataAccessException {
		return repo.getEntityById(DebitDetail.class, id);
	}
	
	@Override
	public DebitDetail getDebitDetailByIdForUpdate(long id) throws DataAccessException {
		String sql = "select * from debit_detail where debit_detail_id = ? for update";
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		return repo.getEntityBySQL(DebitDetail.class, sql, params);
	}
	
	@Override
	public void updateDebitDetail(DebitDetail debitDetail) throws DataAccessException {
		if (debitDetail == null) {
			throw new IllegalArgumentException("debitDetail");
		}
		debitDetail.setUpdateDate(commonDAO.getSysDate());
		repo.update(debitDetail);
	}
	
	@Override
	public void updateDebitDetail(List<DebitDetail> debitDetail) throws DataAccessException {
		if (debitDetail == null) {
			throw new IllegalArgumentException("debitDetail");
		}
		for(DebitDetail dd:debitDetail){
			dd.setUpdateDate(commonDAO.getSysDate());
			repo.update(dd);
		}
	}
	
	
	@Override
	public List<DebitDetail> getListPositiveDebitDetail(KPaging<DebitDetail> kPaging, 
			String shortCode,
			Long fromObjectId, DebitOwnerType type, BigDecimal fromAmount, BigDecimal toAmount,
			Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from debit_detail d join debit dg on dg.debit_id = d.debit_id where 1 = 1");
		
//		if (!StringUtility.isNullOrEmpty(staffCode)) {
//			sql.append(" and exists (select 1 from staff s where s.staff_id=so.staff_id and staff_code=?)");
//			params.add(staffCode.toUpperCase());
//		}
		if (type != null) {
			sql.append(" and dg.object_type = ?");
			params.add(type.getValue());
		}
		if (!StringUtility.isNullOrEmpty(shortCode) && type == DebitOwnerType.CUSTOMER) {
			sql.append(" and exists (select 1 from customer c where c.customer_id= dg.object_id and short_code like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
		}
		if (shopId != null && type == DebitOwnerType.CUSTOMER) {
			sql.append(" and exists (select 1 from customer c where c.customer_id = dg.object_id and c.shop_id = ? ESCAPE '/' )");
			params.add(shopId);
		}
		if (shopId != null && type == DebitOwnerType.SHOP) {
			sql.append(" and dg.object_id = ?");
			params.add(shopId);
		}
		if (fromObjectId != null) {
			sql.append(" and d.from_object_id = ?");
			params.add(fromObjectId);
		}

		if (fromAmount != null) {
			sql.append(" and d.amount >= ?");
			params.add(fromAmount);
		}
		if (toAmount != null) {
			sql.append(" and d.amount <= ?");
			params.add(toAmount);
		}
		sql.append(" and d.remain > 0");
		
		sql.append(" order by CREATE_DATE desc");
		
		if (kPaging == null)
			return repo.getListBySQL(DebitDetail.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(DebitDetail.class, sql.toString(), params, kPaging);
	}

	/** vuongmq, 19 September, 2014 lay danh sach Phieu chi NPP-VNM, phieu thu VNM-NPP*/
	@Override
	public List<DebitShopVO> getListDebitShopVO(Long shopId, PoType poType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/** modify by lacnv1 - 11.04.2014 - Lay them phieu tang no */
		/*sql.append(" select dd.debit_detail_id debitId, po_vnm.invoice_number invoiceNumber, shop.shop_code shopCode,");
		//sql.append(" po_vnm.po_co_number poCoNumber, po_vnm.po_vnm_date createDate,");
		sql.append(" po_vnm.po_co_number poCoNumber, dd.debit_date createDate,"); // lacnv1
		sql.append(" dd.total total, dd.remain remain");
		sql.append(" from debit, debit_detail dd, po_vnm, shop");
		sql.append(" where 1 = 1");
		sql.append(" and debit.debit_id = dd.debit_id");
		if(poType != null) {
			sql.append(" and po_vnm.type = ? ");
			params.add(poType.getValue());
		}
		if (poType != null && poType == PoType.RETURNED_SALES_ORDER) {
			sql.append(" and debit.object_type = ? and dd.remain < 0");
			params.add(DebitOwnerType.SHOP.getValue());
		} else if (poType != null && poType == PoType.PO_CONFIRM) {
			sql.append(" and debit.object_type = ? and dd.remain > 0");
			params.add(DebitOwnerType.SHOP.getValue());
		} else {
			sql.append(" and debit.object_type = ? and dd.remain <> 0");
			params.add(DebitOwnerType.SHOP.getValue());
		}
		sql.append(" and po_vnm.status = ?");
		params.add(PoVNMStatus.IMPORTED.getValue());
		sql.append(" and dd.from_object_id = po_vnm.po_vnm_id");
		sql.append(" and debit.object_id = shop.shop_id");
		if (shopId != null) {
			sql.append(" and debit.object_id = ?");
			params.add(shopId);
		}
		sql.append(" order by createDate");*/
		sql.append("select dd.debit_detail_id as debitId,");
		sql.append(" (case when po.invoice_number is null then dd.invoice_number else po.invoice_number end) as invoiceNumber,");
		sql.append(" (select shop_code from shop where shop_id = db.object_id) as shopCode,");
		sql.append(" (case when po.po_co_number is null then dd.from_object_number else po.po_co_number end) as poCoNumber,");
		sql.append(" dd.debit_date as createDate,");
		sql.append(" dd.total as total,");
		sql.append(" dd.remain as remain, ");
		sql.append(" dd.amount as amount, ");
		sql.append(" dd.discount as discount ");
		sql.append(" from debit_detail dd");
		sql.append(" join debit db on (db.debit_id = dd.debit_id and db.object_type = ?");
		params.add(DebitOwnerType.SHOP.getValue());
		if (shopId != null) {
			sql.append(" and db.object_id = ?");
			params.add(shopId);
		}
		sql.append(")");
		sql.append(" left join po_vnm po on (po.po_vnm_id = dd.from_object_id");
		if(poType != null) {
			sql.append(" and po.type = ?");
			params.add(poType.getValue());
		}
		sql.append(" and po.status = ?)");
		params.add(PoVNMStatus.IMPORTED.getValue());
		sql.append(" where 1 = 1");
		sql.append(" and dd.shop_id =? "); // check them debit_detail (shopId)
		params.add(shopId);
		if (poType != null && poType == PoType.RETURNED_SALES_ORDER) {
			sql.append(" and dd.remain < 0");
		} else if (poType != null && poType == PoType.PO_CONFIRM) {
			sql.append(" and dd.remain > 0");
		} else {
			sql.append(" and dd.remain <> 0");
		}
		sql.append(" order by createDate");
		final String[] fieldNames = new String[] { "debitId", "invoiceNumber", "shopCode"
				, "poCoNumber", "createDate", "total", "remain", "amount", "discount"};
		
		final Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING
				, StandardBasicTypes.STRING, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.BIG_DECIMAL
				, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL};
		return repo.getListByQueryAndScalar(DebitShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public boolean updateDebtForShop(long debitId, BigDecimal amount) {
		return false;
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.DebitDAO#getDebitForUpdatePo(java.lang.Long, java.lang.Long)
	 */
	@Override
	public DebitDetail getDebitDetailForUpdatePo(Long PoVnmId, Long shopId, Long debitDetailId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from debit_detail dd ");
		sql.append(" where dd.debit_detail_id = ? and from_object_id = ? ");
		params.add(debitDetailId);
		params.add(PoVnmId);
		sql.append(" and dd.remain > 0 ");
		sql.append(" and exists (select 1 from po_vnm p where p.shop_id= ? and p.type = 2 and p.status = 2 and po_vnm_id = dd.from_object_id) ");
		params.add(shopId);
		sql.append(" and exists (select 1 from debit where object_id = ? and object_type=1 and debit_id = dd.debit_id) ");
		params.add(shopId);
		return repo.getEntityBySQL(DebitDetail.class, sql.toString(), params);
	}
	
	@Override
	public DebitDetail getDebitDetailForUpdatePoVNM(Long PoVnmId, Long debitId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from debit_detail dd ");
		sql.append(" where 1 = 1 ");
		if (debitId != null) {
			sql.append(" and dd.debit_id = ? ");
			params.add(debitId);
		}
		if (PoVnmId != null) {
			sql.append(" and dd.from_object_id = ? ");
			params.add(PoVnmId);
		}
		return repo.getEntityBySQL(DebitDetail.class, sql.toString(), params);
	}

//	@Override
//	public List<Debit> getListDiffZeroDebitDetail(KPaging<Debit> kPaging, 
//			String shortCode,
//			Long fromObjectId, DebitGeneralOwnerType type, BigDecimal fromAmount, BigDecimal toAmount,
//			Long shopId) throws DataAccessException {
//		StringBuilder sql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		sql.append(" select * from debit so where 1=1");
//		
////		if (!StringUtility.isNullOrEmpty(staffCode)) {
////			sql.append(" and exists (select 1 from staff s where s.staff_id=so.staff_id and staff_code=?)");
////			params.add(staffCode.toUpperCase());
////		}
//		
//		if (!StringUtility.isNullOrEmpty(shortCode)) {
//			sql.append(" and exists (select 1 from customer c where c.customer_id=so.customer_id and short_code like ?)");
//			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
//		}
//		if (shopId != null) {
//			sql.append(" and shop_id=?");
//			params.add(shopId);
//		}
//		if (fromObjectId != null) {
//			sql.append(" and from_object_id=?");
//			params.add(fromObjectId);
//		}
//		if (type != null) {
//			sql.append(" and type=?");
//			params.add(type.getValue());
//		}
//		if (fromAmount != null) {
//			sql.append(" and amount>=?");
//			params.add(fromAmount);
//		}
//		if (toAmount != null) {
//			sql.append(" and amount<=?");
//			params.add(toAmount);
//		}
//		sql.append(" and remain != 0");
//		
//		sql.append(" order by CREATE_DATE desc");
//		
//		if (kPaging == null)
//			return repo.getListBySQL(Debit.class, sql.toString(), params);
//		else
//			return repo.getListBySQLPaginated(Debit.class, sql.toString(), params, kPaging);
//	}
	
	
	/**
	 * 
	 * @modify by lacnv1 - them id NVBH
	 * @modify date Mar 26, 2014
	 */
	@Override
	public List<CustomerDebitVO> getListCustomerDebitVO(Long shopId, String shortCode, String orderNumber, BigDecimal fromAmount,
			BigDecimal toAmount, Boolean isReveived, Date fromDate, Date toDate, Long nvghId, Long nvttId, Long nvbhId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT d.debit_detail_id AS debitId,");
		sql.append("   c.short_code    AS shortCode,");
		sql.append("   c.customer_name    AS customerName,");
		//sql.append("   so.order_number AS orderNumber,");
		//sql.append(" (case when so.order_number is null then d.from_object_number else so.order_number end) as orderNumber,");
		sql.append(" d.from_object_number as orderNumber,");
		//sql.append("   so.order_date   AS orderDate,");
		sql.append("   d.debit_date   AS orderDate,"); //
		sql.append("   d.total         AS total,");
		sql.append("   d.total_pay     AS totalPay,");
		sql.append("   d.remain        AS remain,");
		sql.append(" nvl(d.discount_amount, 0) as discountAmount,");
		sql.append("   (select staff_code from staff where staff_id = so.cashier_id) AS nvttCodeName,");
		sql.append("   (select staff_code from staff where staff_id = so.delivery_id) AS nvghCodeName,");
		sql.append("   (select staff_code from staff where staff_id = so.staff_id) AS nvbhCodeName,");
		sql.append("   (select staff_name from staff where staff_id = so.cashier_id) AS nvttName,");
		sql.append("   (select staff_name from staff where staff_id = so.delivery_id) AS nvghName,");
		sql.append("   (select staff_name from staff where staff_id = so.staff_id) AS nvbhName");
		sql.append(" FROM debit_detail d JOIN debit dg ON (dg.debit_id = d.debit_id AND dg.object_type = ?)");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		sql.append(" JOIN customer c ON (c.customer_id = dg.object_id AND c.shop_id    = ?)");
		params.add(shopId);
		sql.append(" left JOIN sale_order so ON (d.from_object_id = so.sale_order_id)");
		sql.append(" WHERE 1 =1");
		if (isReveived != null) { // check them dieu kien tong no va con no phai cung dau
			if (Boolean.TRUE.equals(isReveived)) {
				sql.append(" AND d.remain     > 0 and d.total > 0");
			} else {
				sql.append(" AND d.remain     < 0 and d.total < 0");
			}
		} else {
			sql.append(" AND d.remain     != 0 and d.remain * d.total > 0");
		}
		if (!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append(" AND UPPER(c.short_code) = ? ");
			params.add(shortCode.toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(orderNumber)) {//tungMT fix tim kiem like
			sql.append(" AND (UPPER(so.order_number) LIKE ? ESCAPE '/' ");
			sql.append(" or upper(d.from_object_number) like ? escape '/')");
			String s = StringUtility.toOracleSearchLike(orderNumber.toUpperCase());
			params.add(s);
			params.add(s);
		}
		if (fromAmount != null) {
			sql.append(" AND ABS(d.remain)     >= ?");
			params.add(fromAmount);
		}
		if (toAmount != null) {
			sql.append(" AND ABS(d.remain)     <= ?");
			params.add(toAmount);
		}
		if (fromDate != null) {
			sql.append(" AND d.debit_date >= trunc(?) ");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND d.debit_date < trunc(?) + 1 ");
			params.add(toDate);
		}
		if (nvghId != null) {
			sql.append(" AND so.delivery_id = ? ");
			params.add(nvghId);
		}
		if (nvttId != null) {
			sql.append(" AND so.cashier_id = ? ");
			params.add(nvttId);
		}
		if (nvbhId != null) {
			sql.append(" AND so.staff_id = ? ");
			params.add(nvbhId);
		}
		
		sql.append(" ORDER BY orderDate, debitId");
		
		final String[] fieldNames = new String[] { 
				"debitId",// 0
				"shortCode",// 1
				"customerName",// 2
				"orderNumber",// 3
				"orderDate" ,// 4
				"total",// 5
				"totalPay",// 6
				"remain",// 7
				"discountAmount",// 8
				"nvttCodeName",// 9
				"nvghCodeName",// 10
				"nvbhCodeName",// 11
				"nvttName",
				"nvghName",
				"nvbhName"
		};
		
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,//0
				StandardBasicTypes.STRING,//1
				StandardBasicTypes.STRING,//2
				StandardBasicTypes.STRING,//3
				StandardBasicTypes.TIMESTAMP,//4
				StandardBasicTypes.BIG_DECIMAL,//5
				StandardBasicTypes.BIG_DECIMAL,//6
				StandardBasicTypes.BIG_DECIMAL,//7
				StandardBasicTypes.BIG_DECIMAL,//8
				StandardBasicTypes.STRING,//9
				StandardBasicTypes.STRING,//10
				StandardBasicTypes.STRING,//11
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING
		};
		
		return repo.getListByQueryAndScalar(CustomerDebitVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/**
	 * 
	 * @author lacnv1
	 */
	@Override
	public List<CustomerDebitVO> getListCustomerDebitVO(DebitFilter<CustomerDebitVO> filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("fillter null");
		}
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("shopId is null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT d.debit_detail_id AS debitId,");
		sql.append("   c.short_code    AS shortCode,");
		sql.append("   c.customer_name    AS customerName,");
		sql.append(" d.from_object_id as orderId,");
		sql.append(" d.from_object_number as orderNumber,");		
		sql.append("   d.debit_date   AS orderDate,");
		sql.append("   d.total         AS total,");
		sql.append("   d.total_pay     AS totalPay,");
		sql.append("   d.remain        AS remain,");
		sql.append(" nvl(d.discount_amount, 0) as discountAmount,");
		sql.append("   (select staff_code from staff where staff_id = so.cashier_id) as nvttCodeName,");
		sql.append("   (select staff_code from staff where staff_id = so.delivery_id) as nvghCodeName,");
		sql.append("   (select staff_code from staff where staff_id = so.staff_id) as nvbhCodeName,");
		sql.append("   (select staff_name from staff where staff_id = so.cashier_id) as nvttName,");
		sql.append("   (select staff_name from staff where staff_id = so.delivery_id) as nvghName,");
		sql.append("   (select staff_name from staff where staff_id = so.staff_id) as nvbhName");
		sql.append(" from debit_detail d");
		sql.append(" join debit dg on (dg.debit_id = d.debit_id and dg.object_type = ?)");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		sql.append(" join customer c on (c.customer_id = dg.object_id AND c.shop_id    = ?)");
		params.add(filter.getShopId());
		sql.append(" left join sale_order so on (d.from_object_id = so.sale_order_id)");
		sql.append(" where 1 = 1");
		
		if (filter.getIsReveived() != null) {
			if (Boolean.TRUE.equals(filter.getIsReveived())) {
				sql.append(" and d.remain > 0 and d.total > 0");
			} else {
				sql.append(" and d.remain < 0 and d.total < 0");
			}
		} else {
			sql.append(" and d.remain * d.total > 0");
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and upper(c.short_code) = ? ");
			params.add(filter.getShortCode().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and upper(d.from_object_number) like ? escape '/'");
			String s = StringUtility.toOracleSearchLike(filter.getOrderNumber().toUpperCase());
			params.add(s);
			params.add(s);
		}
		
		if (filter.getFromAmount() != null) {
			sql.append(" and abs(d.remain)     >= ?");
			params.add(filter.getFromAmount());
		}
		if (filter.getToAmount() != null) {
			sql.append(" and abs(d.remain)     <= ?");
			params.add(filter.getToAmount());
		}
		
		if (filter.getFromDate() != null) {
			sql.append(" and d.debit_date >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and d.debit_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		
		if (filter.getDeliveryId() != null) {
			sql.append(" and so.delivery_id = ? ");
			params.add(filter.getDeliveryId());
		}
		if (filter.getCashierId() != null) {
			sql.append(" and so.cashier_id = ? ");
			params.add(filter.getCashierId());
		}
		if (filter.getStaffId() != null) {
			sql.append(" and so.staff_id = ? ");
			params.add(filter.getStaffId());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getLstOrderNumbers())) {
			String s = filter.getLstOrderNumbers();
			s = s.replaceAll(";", ",");
			String[] str = s.split(",");
			sql.append(" and upper(d.from_object_number) in ('-1'");
			for (String s1 : str) {
				sql.append(",?");
				params.add(s1.trim().toUpperCase());
			}
			sql.append(")");
		}
		
		if (filter.getLstDebitDetailId() != null && !filter.getLstDebitDetailId().isEmpty()) {
			sql.append(" and d.debit_detail_id in (-1");
			for (int i = 0, sz = filter.getLstDebitDetailId().size(); i < sz; i++) {
				sql.append(",?");
				params.add(filter.getLstDebitDetailId().get(i));
			}
			sql.append(" ) ");
		}

		sql.append(" order by orderDate, debitId");

		final String[] fieldNames = new String[] {
				"debitId",// 0
				"shortCode",// 1
				"customerName",// 2
				"orderId",
				"orderNumber",// 3
				"orderDate",// 4
				"total",// 5
				"totalPay",// 6
				"remain",// 7
				"discountAmount",// 8
				"nvttCodeName",// 9
				"nvghCodeName",// 10
				"nvbhCodeName",// 11
				"nvttName", 
				"nvghName",
				"nvbhName"
		};

		final Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.TIMESTAMP,// 4
				StandardBasicTypes.BIG_DECIMAL,// 5
				StandardBasicTypes.BIG_DECIMAL,// 6
				StandardBasicTypes.BIG_DECIMAL,// 7
				StandardBasicTypes.BIG_DECIMAL,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.STRING,// 11
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING
		};

		return repo.getListByQueryAndScalar(CustomerDebitVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	//phuongvm
	@Override
	public List<CustomerDebitVO> getListCustomerDebitVOEx(Long shopId, String shortCode, String orderNumber, BigDecimal fromAmount,
			BigDecimal toAmount, Boolean isReveived, Date fromDate, Date toDate, Long nvghId, Long nvttId, Long nvbhId,List<Long> lstDebitId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT d.debit_detail_id AS debitId,");
		sql.append("   c.short_code    AS shortCode,");
		sql.append("   c.customer_name    AS customerName,");
		//sql.append("   so.order_number AS orderNumber,");
		sql.append(" (case when so.order_number is null then d.from_object_number else so.order_number end) as orderNumber,");
		//sql.append("   so.order_date   AS orderDate,");
		sql.append("   d.debit_date   AS orderDate,"); //
		sql.append("   d.total         AS total,");
		sql.append("   d.total_pay     AS totalPay,");
		sql.append("   d.remain        AS remain,");
		sql.append(" nvl(d.discount_amount, 0) as discountAmount,");
		sql.append("   (select staff_name from staff where staff_id = so.cashier_id) AS nvttName, ");
		sql.append("   (select staff_name from staff where staff_id = so.delivery_id) AS nvghName, ");
		sql.append("   (select staff_name from staff where staff_id = so.staff_id) AS nvbhName, ");
		sql.append("   (select staff_code from staff where staff_id = so.cashier_id) AS nvttCodeName, ");
		sql.append("   (select staff_code from staff where staff_id = so.delivery_id) AS nvghCodeName, ");
		sql.append("   (select staff_code from staff where staff_id = so.staff_id) AS nvbhCodeName ");
		sql.append(" FROM debit_detail d JOIN debit dg ON (dg.debit_id = d.debit_id AND dg.object_type = ?)");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		sql.append(" JOIN customer c ON (c.customer_id = dg.object_id AND c.shop_id    = ?)");
		params.add(shopId);
		sql.append(" left JOIN sale_order so ON (d.from_object_id = so.sale_order_id)");
		sql.append(" WHERE 1 =1");
		if (isReveived != null) {
			if (Boolean.TRUE.equals(isReveived)) {
				sql.append(" AND d.remain     > 0");
			} else {
				sql.append(" AND d.remain     < 0");
			}
		} else {
			sql.append(" AND d.remain     != 0");
		}
		if(lstDebitId!=null && !lstDebitId.isEmpty()){
			sql.append(" AND d.debit_detail_id in(");
			for(int i=0;i<lstDebitId.size();i++){
				Long id = lstDebitId.get(i);
				sql.append("?");
				params.add(id);
				if(i!=lstDebitId.size()-1){
					sql.append(",");
				}
			}
			sql.append(")");
			
		}
		if (!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append(" AND UPPER(c.short_code) = ? ");
			params.add(shortCode.toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(orderNumber)) {//tungMT fix tim kiem like
			sql.append(" AND (UPPER(so.order_number) LIKE ? ESCAPE '/' ");
			sql.append(" or upper(d.from_object_number) like ? escape '/')");
			String s = StringUtility.toOracleSearchLike(orderNumber.toUpperCase());
			params.add(s);
			params.add(s);
		}
		if (fromAmount != null) {
			sql.append(" AND ABS(d.remain)     >= ?");
			params.add(fromAmount);
		}
		if (toAmount != null) {
			sql.append(" AND ABS(d.remain)     <= ?");
			params.add(toAmount);
		}
		if (fromDate != null) {
			sql.append(" AND d.debit_date >= trunc(?) ");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND d.debit_date < trunc(?) + 1 ");
			params.add(toDate);
		}
		if (nvghId != null) {
			sql.append(" AND so.delivery_id = ? ");
			params.add(nvghId);
		}
		if (nvttId != null) {
			sql.append(" AND so.cashier_id = ? ");
			params.add(nvttId);
		}
		if (nvbhId != null) {
			sql.append(" AND so.staff_id = ? ");
			params.add(nvbhId);
		}
		
		sql.append(" ORDER BY orderDate, debitId");
		
		final String[] fieldNames = new String[] { 
				"debitId",// 0
				"shortCode",// 1
				"customerName",// 2
				"orderNumber",// 3
				"orderDate" ,// 4
				"total",// 5
				"totalPay",// 6
				"remain",// 7
				"discountAmount",// 8
				"nvttName",// 9
				"nvghName",// 10
				"nvbhName",// 11
				"nvttCodeName",// 12
				"nvghCodeName",// 13
				"nvbhCodeName"// 14
		};
		
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,//0
				StandardBasicTypes.STRING,//1
				StandardBasicTypes.STRING,//2
				StandardBasicTypes.STRING,//3
				StandardBasicTypes.TIMESTAMP,//4
				StandardBasicTypes.BIG_DECIMAL,//5
				StandardBasicTypes.BIG_DECIMAL,//6
				StandardBasicTypes.BIG_DECIMAL,//7
				StandardBasicTypes.BIG_DECIMAL,//8
				StandardBasicTypes.STRING,//9
				StandardBasicTypes.STRING,//10
				StandardBasicTypes.STRING,//11
				StandardBasicTypes.STRING,//12
				StandardBasicTypes.STRING,//13
				StandardBasicTypes.STRING//14
		};
		
		return repo.getListByQueryAndScalar(CustomerDebitVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/**
	 * @author hunglm16
	 * @modify date MAY 18, 2014
	 * @description Lay danh sach don hang voi danh sach DebitDetail
	 */
	@Override
	public List<SaleOrder> getListSaleOderByListDebitdetailId(List<Long> lstFromObject) throws DataAccessException {
		if (lstFromObject == null || lstFromObject.size()==0) {
			throw new IllegalArgumentException("lstFromObject is null");
		}
		final int SIZE = 900;
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * FROM sale_order so");
		sql.append(" where 1=1");
		sql.append(" AND (so.sale_order_id in(-1");
		if (lstFromObject != null && lstFromObject.size() > 0) {
			Long soId = null;
			int n = 0;
			for (int i = 0, sz = lstFromObject.size(); i < sz; i++) {
				soId = lstFromObject.get(i);
				if (n < SIZE) {
					sql.append(",?");
					params.add(soId);
					n++;
				} else {
					sql.append(") or so.sale_order_id in (-1,?");
					params.add(soId);
					n = 1;
				}
			}
		}
		sql.append("))");
		return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
	}
	
	
	
	@Override
	public List<RptDebtOfCustomerDataVO> getDataForReportDebtOfCustomer(
			Long shopId, List<Long> customerIds, Date reportDate)
			throws DataAccessException {
		if (null == shopId) {
			throw new IllegalArgumentException("Shop id is invalidate");
		}

		if (null == reportDate) {
			throw new IllegalArgumentException("Report date is invalidate");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT CUS.CUSTOMER_ID AS customerId, CUS.CUSTOMER_CODE AS customerCode, CUS.CUSTOMER_NAME AS customerName, ");
		sql.append("     SO.ORDER_DATE AS customerDate, SO.ORDER_NUMBER AS orderNumber, SO.ORDER_TYPE AS orderType, ");
		sql.append("     SO.ORDER_DATE + CUS.MAX_DEBIT_DATE AS duaDate, ");
		sql.append("     CUS.MAX_DEBIT_DATE maxDebitDate, ");
		sql.append("     DEB.REMAIN AS remain, ");
		sql.append("     DECODE(SIGN(TRUNC(?) - (TRUNC(SO.ORDER_DATE) + NVL(CUS.MAX_DEBIT_DATE, 0))), 1, DEB.REMAIN, 0) AS overdueDebt, ");
		sql.append("     DECODE(SIGN(TRUNC(?) - (TRUNC(SO.ORDER_DATE) + NVL(CUS.MAX_DEBIT_DATE, 0))), 0, DEB.REMAIN, 0) AS dueDebt, ");
		sql.append("     DECODE(SIGN(TRUNC(?) - (TRUNC(SO.ORDER_DATE) + NVL(CUS.MAX_DEBIT_DATE, 0))), -1, DEB.REMAIN, 0) AS undueDebt, ");
		sql.append("     STA1.STAFF_CODE AS staffCode, STA1.STAFF_NAME AS staffName, ");
		sql.append("     STA2.STAFF_CODE AS deliveryCode, STA2.STAFF_NAME AS deliveryName ");
		sql.append(" FROM CUSTOMER CUS, DEBIT_detail DEB, SALE_ORDER SO, STAFF STA1, STAFF STA2 ");
		sql.append(" WHERE CUS.CUSTOMER_ID = DEB.CUSTOMER_ID ");
		sql.append("     AND DEB.FROM_OBJECT_ID = SO.SALE_ORDER_ID ");
		sql.append("     AND SO.STAFF_ID = STA1.STAFF_ID ");
		sql.append("     AND SO.DELIVERY_ID = STA2.STAFF_ID ");
		sql.append("     AND DEB.SHOP_ID = ? ");
		sql.append("     AND DEB.TYPE = 0 ");
		sql.append("     AND DEB.REMAIN <> 0 ");

		params.add(reportDate);
		params.add(reportDate);
		params.add(reportDate);

		params.add(shopId);

		if (null != customerIds && customerIds.size() > 0) {
			sql.append("     AND CUS.CUSTOMER_ID IN (");

			for (int i = 0; i < customerIds.size(); i++) {
				if (i < customerIds.size() - 1) {
					sql.append(" ?,");
				} else {
					sql.append(" ?");
				}

				params.add(customerIds.get(i));
			}

			sql.append(") ");
		}

		sql.append(" ORDER BY CUS.CUSTOMER_CODE, SO.ORDER_NUMBER ");

		String[] fieldNames = { //
		"customerId",// 1
				"customerCode", // 1'
				"customerName",// 2
				"customerDate",// 3
				"orderNumber",// 4
				"orderType",// 5
				"duaDate",// 6
				"maxDebitDate",// 7
				"remain",// 8
				"overdueDebt",// 9
				"dueDebt",// 10
				"undueDebt",// 11
				"staffCode",// 12
				"staffName",// 13
				"deliveryCode",// 14
				"deliveryName"// 15
		};

		Type[] fieldTypes = { //
		StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING, // 1'
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.DATE, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.DATE, // 6
				StandardBasicTypes.INTEGER, // 7
				StandardBasicTypes.BIG_DECIMAL, // 8
				StandardBasicTypes.BIG_DECIMAL, // 9
				StandardBasicTypes.BIG_DECIMAL, // 10
				StandardBasicTypes.BIG_DECIMAL, // 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.STRING, // 13
				StandardBasicTypes.STRING, // 14
				StandardBasicTypes.STRING, // 15

		};

		return repo.getListByQueryAndScalar(RptDebtOfCustomerDataVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.DebitDAO#getListLimitDebitDetail(java.lang.Long, java.util.Date, java.lang.String)
	 */
	@Override
	public List<RptLimitDebitVO> getListLimitDebitDetail(Long shopId, Date date,
			String customerCode) throws DataAccessException {
		
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (date == null) {
			throw new IllegalArgumentException("date is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT tb1.order_type AS orderType,");
		sql.append(" tb1.order_date      AS orderDate,");
		sql.append(" tb1.short_code      AS shortCode,");
		sql.append(" tb1.customer_code   AS customerCode,");
		sql.append(" tb1.customer_name   AS customerName,");
		sql.append(" tb1.address         AS address,");
		sql.append(" tb1.max_debit_date  AS maxDebitDate,");
		sql.append(" tb1.total           AS total,");
		sql.append(" tb1.staff_code      AS staffCode,");
		sql.append(" tb1.staff_name      AS staffName,");
		sql.append(" tb1.remain          AS remain,");
		sql.append(" tb2.staff_code      AS deliveryCode,");
		sql.append(" tb2.staff_name      AS deliveryName");
		sql.append(" FROM");
		sql.append(" (SELECT so.order_type,");
		sql.append(" so.order_date,");
		sql.append(" c.short_code,");
		sql.append(" c.customer_code,");
		sql.append(" c.customer_name,");
		sql.append(" so.delivery_id,");
		sql.append(" c.address,");
		sql.append(" c.max_debit_date,");
		sql.append(" d.total,");
		sql.append(" s.staff_name,");
		sql.append(" s.staff_code,");
		sql.append(" d.remain");
		sql.append(" FROM sale_order so,");
		sql.append(" customer c,");
		sql.append(" debit_detail d,");
		sql.append(" debit dg,");
		sql.append(" staff s");
		sql.append(" WHERE so.staff_id                         = s.staff_id");
		sql.append(" AND d.from_object_id                      = so.sale_order_id");
		sql.append(" AND dg.debit_id   			   	   = d.debit_id");
		sql.append(" AND dg.object_type                         = ?");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		sql.append(" AND so.customer_id                        = c.customer_id");
		sql.append(" AND so.order_date < TRUNC(?) - c.max_debit_date");
		params.add(date);
		sql.append(" AND so.shop_id                            = ?");
		params.add(shopId);
		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append(" AND lower(c.customer_code) = ?");
			params.add(customerCode);
		}
		sql.append(" ORDER BY c.customer_code");
		sql.append(" ) tb1");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT staff_id, staff_code, staff_name FROM staff");
		sql.append(" ) tb2");
		sql.append(" ON tb1.delivery_id = tb2.staff_id");
		
		final String[] fieldNames = new String[] { 
				"orderType",//1
				"orderDate",//2
				"shortCode",//3
				"customerCode" ,//4
				"customerName",//5
				"address",//6
				"maxDebitDate",//7
				"total",//8
				"staffCode",//9
				"staffName",//10
				"remain",//11
				"deliveryCode",//12
				"deliveryCode",//13
				};
		
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,//
				StandardBasicTypes.STRING,//1
				StandardBasicTypes.TIMESTAMP,//2
				StandardBasicTypes.STRING,//3
				StandardBasicTypes.STRING,//4
				StandardBasicTypes.STRING,//5
				StandardBasicTypes.STRING,//6
				StandardBasicTypes.INTEGER,//7
				StandardBasicTypes.BIG_DECIMAL,//8
				StandardBasicTypes.STRING,//9
				StandardBasicTypes.STRING,//10
				StandardBasicTypes.BIG_DECIMAL,//11
				StandardBasicTypes.STRING,//12
				StandardBasicTypes.STRING,//13
		};
		return repo.getListByQueryAndScalar(RptLimitDebitVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public BigDecimal getCustomerTotalDebitDetail(Long shopId,
			String staffCode, String custName, String shortCode, String orderNumber, BigDecimal fromAmount,
			BigDecimal toAmount, Boolean isReveived, Long deliveryId, Long cashierId,
			Date fromDate, Date toDate) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT SUM(NVL(d.remain, 0)) sum");
		sql.append(" FROM debit_detail d");
		sql.append(" JOIN debit dg");
		sql.append(" ON (d.debit_id = dg.debit_id AND dg.object_type = ?)");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		sql.append(" JOIN customer c");
		sql.append(" ON (dg.object_id = c.customer_id AND c.shop_id    = ?)");
		params.add(shopId);
		sql.append(" left JOIN sale_order so");
		sql.append(" ON (d.from_object_id = so.sale_order_id)");
		sql.append(" left JOIN staff s");
		sql.append(" ON (so.staff_id  = s.staff_id)");
		sql.append(" WHERE 1          =1");
		if (isReveived != null) {
			if (Boolean.TRUE.equals(isReveived)) {
				sql.append(" AND d.remain     > 0");
			} else {
				sql.append(" AND d.remain     < 0");
			}
		} else {
			sql.append(" AND d.remain     != 0");
		}
		if(null != deliveryId) {
			sql.append(" AND so.delivery_id = ? ");
			params.add(deliveryId);
		}
		if(null != cashierId) {
			sql.append(" AND so.cashier_id = ? ");
			params.add(cashierId);
		}
		if (fromDate != null) {
			sql.append(" AND d.debit_date >= trunc(?) ");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND d.debit_date <= trunc(?) + 1 ");
			params.add(toDate);
		}
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" AND UPPER(s.staff_code) = ?");
			params.add(staffCode.toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append(" AND UPPER(c.short_code) = ?");
			params.add(shortCode.toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(custName)) {
			sql.append(" AND UPPER(c.customer_name) LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(custName.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(orderNumber)) {
			sql.append(" AND UPPER(so.order_number) = ?");
			params.add(orderNumber.toUpperCase());
		}
		if (fromAmount != null) {
			sql.append(" AND d.remain     >= ?");
			params.add(fromAmount);
		}
		if (toAmount != null) {
			sql.append(" AND d.remain     <= ?");
			params.add(toAmount);
		}
		BigDecimal ret = (BigDecimal) repo.getObjectByQuery(sql.toString(), params);
		return ret == null ? BigDecimal.valueOf(0) : ret;
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<DebitShopVO> getDebitVNMOfShop(Long shopId, Long typeInvoice) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("invalid id");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select dd.debit_detail_id as debitId,");
		sql.append(" dd.invoice_number as invoiceNumber,");
		sql.append(" (select shop_code from shop where shop_id=?) as shopCode,");
		params.add(shopId);
		sql.append(" dd.from_object_number as poCoNumber,");
		sql.append(" dd.debit_date as createDate,");
		sql.append(" dd.total as total, dd.remain as remain, dd.amount as amount, dd.discount as discount ");
		sql.append(" from debit_detail dd");
		sql.append(" where dd.debit_id in (select debit_id from debit db where db.object_type=? and db.object_id=?)");
		params.add(DebitOwnerType.SHOP.getValue());
		params.add(shopId);
		sql.append(" and dd.shop_id = ? "); // check them debit_detail(shop_id)
		params.add(shopId);
		if(typeInvoice == 2){
			/** typeInvoice == 2 phieu chi */
			sql.append(" and dd.type in (?, ?) and dd.remain > 0");
			params.add(DebitDetailType.NHAP_VNM.getValue());
			params.add(DebitDetailType.TANG_NO_VNM.getValue());
		} else {
			/** typeInvoice == 3 phieu thu */
			sql.append(" and dd.type in (?, ?) and dd.remain < 0");
			params.add(DebitDetailType.TRA_VNM.getValue());
			params.add(DebitDetailType.GIAM_NO_VNM.getValue());
		}
		
		sql.append(" order by createDate, invoiceNumber, poCoNumber");
		
		final String[] fieldNames = new String[] {
				"debitId", "invoiceNumber", "shopCode",
				"poCoNumber", "createDate", "total",
				"remain", "amount", "discount",
		};
		
		final Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL
		};
		
		return repo.getListByQueryAndScalar(DebitShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<DebitShopVO> getDebitVNMOfShopAndId(Long shopId, List<Long> listDebitDetailId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("invalid id");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select dd.debit_detail_id as debitId,");
		sql.append(" dd.invoice_number as invoiceNumber,");
		sql.append(" (select shop_code from shop where shop_id=?) as shopCode,");
		params.add(shopId);
		sql.append(" dd.from_object_number as poCoNumber,");
		sql.append(" dd.debit_date as createDate,");
		sql.append(" dd.amount as amount, dd.discount as discount,");
		sql.append(" dd.total as total, dd.remain as remain");
		sql.append(" from debit_detail dd");
		sql.append(" where dd.debit_id in (select debit_id from debit db where db.object_type=? and db.object_id=?)");
		params.add(DebitOwnerType.SHOP.getValue());
		params.add(shopId);
		sql.append(" and dd.type in (?, ?) and dd.remain > 0");
		params.add(DebitDetailType.NHAP_VNM.getValue());
		params.add(DebitDetailType.TANG_NO_VNM.getValue());
		if(listDebitDetailId != null && listDebitDetailId.size() >0){
			sql.append(" and dd.debit_detail_id in (?");
			params.add(listDebitDetailId.get(0));
			for(int i=1, size = listDebitDetailId.size();i< size;i++){
				sql.append(",?");
				params.add(listDebitDetailId.get(i));
			}
			sql.append(") ");
		}
		sql.append(" order by createDate, invoiceNumber, poCoNumber");
		
		final String[] fieldNames = new String[] {
				"debitId", "invoiceNumber", "shopCode",
				"poCoNumber", "createDate", "amount",
				"discount", "total", "remain"
		};
		
		final Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL
		};
		
		return repo.getListByQueryAndScalar(DebitShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public DebitDetail getDebitDetailBySaleOrderId(long id)
			throws DataAccessException {
		String sql = "select * from debit_detail where FROM_OBJECT_ID =  ?";
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		return repo.getEntityBySQL(DebitDetail.class, sql, params);
	}
	
	@Override
	public DebitDetail getDebitDetailByFromObjectId(long fromObjectId) throws DataAccessException {
		String sql = "select * from debit_detail where from_object_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(fromObjectId);
		return repo.getFirstBySQL(DebitDetail.class, sql, params);
	}
	
	@Override
	public List<DebitDetail> getListDebitDetailByFilter(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from debit_detail dd where 1 = 1");
		if (filter.getShopId() != null) {
			sql.append(" and dd.shop_id=?");
			params.add(filter.getShopId());
		}
		if (filter.getLstSaleOrderId() != null) {
			sql.append(" and dd.from_object_id in (-1");
			for (Long id : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(id);
			}
			sql.append(")");
		}
		if (filter.getLstId() != null) {
			sql.append(" and dd.debit_detail_id in (-1");
			for (Long id : filter.getLstId()) {
				sql.append(", ?");
				params.add(id);
			}
			sql.append(")");
		}
		return repo.getListBySQL(DebitDetail.class, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public boolean existsDebitNumber(long shopId, String number) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(number)) {
			throw new IllegalArgumentException("invalid number");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select count(1) as count from");
		sql.append(" (");
		sql.append(" select 1 from debit_detail dd");
		sql.append(" where upper(from_object_number) = ?");
		params.add(number.toUpperCase());
		sql.append(" and exists (select 1 from debit db");
		sql.append(" join shop s on (s.shop_id = db.object_id)");
		sql.append(" where debit_id = dd.debit_id");
		sql.append(" and object_type = ? and s.shop_id = ?)");
		params.add(DebitOwnerType.SHOP.getValue());
		params.add(shopId);
		sql.append(" union all");
		sql.append(" select 1 from debit_detail dd");
		sql.append(" where upper(from_object_number) = ?");
		params.add(number.toUpperCase());
		sql.append(" and exists (select 1 from debit db");
		sql.append(" join customer c on (c.customer_id = db.object_id)");
		sql.append(" where debit_id = dd.debit_id");
		sql.append(" and object_type = ? and c.shop_id = ?)");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		params.add(shopId);
		sql.append(" )");
		
		Integer n = repo.countBySQL(sql.toString(), params);
		return (n > 0);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public void cloneDebitDetailToTemp(DebitDetail debitDetail) throws DataAccessException {
		DebitDetailTemp temp = new DebitDetailTemp();
		temp.setFromObjectId(debitDetail.getFromObjectId());
		temp.setCreateDate(debitDetail.getCreateDate());
		temp.setAmount(debitDetail.getAmount());
		temp.setDiscount(debitDetail.getDiscount());
		temp.setTotal(debitDetail.getTotal());
		temp.setTotalPay(debitDetail.getTotalPay());
		temp.setRemain(debitDetail.getRemain());
		temp.setCreateUser(debitDetail.getCreateUser());
		temp.setDebit(debitDetail.getDebit());
		temp.setFromObjectNumber(debitDetail.getFromObjectNumber());
		temp.setType(debitDetail.getType());
		temp.setDebitDate(debitDetail.getDebitDate());
		temp.setDiscountAmount(debitDetail.getDiscountAmount());
		temp.setShop(debitDetail.getShop());
		temp.setCustomer(debitDetail.getCustomer());
		temp.setStaff(debitDetail.getStaff());
		temp.setOrderType(debitDetail.getOrderType());
		temp.setOrderDate(debitDetail.getOrderDate());
		temp.setReturnDate(debitDetail.getReturnDate());
		
		repo.create(temp);
	}

	/**
	 * 
	 */
	@Override
	public void updateOrderNumberForDebitDetailTemp(SaleOrder so) throws DataAccessException {
		if (so == null) {
			return;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from debit_detail_temp where type in (4, 5) and from_object_id = ?");
		params.add(so.getId());
		
		DebitDetailTemp temp = repo.getEntityBySQL(DebitDetailTemp.class, sql.toString(), params);
		
		if (temp != null) {
			temp.setFromObjectNumber(so.getOrderNumber());
		}
		repo.update(temp);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public DebitDetail getDebitDetailByObjectNumber(long shopId, String fromObjectNumber, Date pDate) throws DataAccessException {
		if (fromObjectNumber == null) {
			fromObjectNumber = "";
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from debit_detail");
		sql.append(" where exists (select 1 from debit where object_type = ?)");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		sql.append(" and shop_id = ?");
		params.add(shopId);
		sql.append(" and upper(from_object_number) = ?");
		params.add(fromObjectNumber.toUpperCase());
		sql.append(" and remain * total > 0");
		
		if (pDate != null) {
			sql.append(" and debit_date >= trunc(?) and debit_date < trunc(?)+1");
			params.add(pDate);
			params.add(pDate);
		}
		
		return repo.getEntityBySQL(DebitDetail.class, sql.toString(), params);
	}
}

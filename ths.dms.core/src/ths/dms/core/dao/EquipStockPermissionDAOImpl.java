package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.EquipEvictionFormDtl;
import ths.dms.core.entities.EquipRole;
import ths.dms.core.entities.EquipRoleDetail;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.vo.EquipProposalBorrowVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ShopViewParentVO;
import ths.dms.core.entities.vo.StockTransLotVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

/**
 * Tang xu ly tuong tac DB
 * 
 * @author hoanv25
 * @since March 09,2015
 * @description Dung cho Quan Ly quyen kho thiet bi
 * */
public class EquipStockPermissionDAOImpl implements EquipStockPermissionDAO {
	@Autowired
	private IRepository repo;

	@Autowired
	private CommonDAO commonDAO;	

	@Override
	public List<EquipProposalBorrowVO> searchListStockPermissionVOByFilter(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct er.equip_role_id as id, er.code as code, er.name as name, er.description as description , er.status as status from equip_role er ");
		sql.append(" where 1=1 ");
		if (filter.getStatus() != null) {
			sql.append("  and er.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append("  and er.status <> -1 ");
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(er.code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			sql.append(" and lower(er.name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getName().trim().toLowerCase()));
		}
		sql.append(" ORDER BY code asc ");
		String[] fieldNames = { "id", "code", "name", "description", "status" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}	
	}

	/***
	 * @author vuongmq
	 * @date Mar 23,2015
	 * @description lay danh sach them cho equip_role left join equip_role_detail de xuat excel quan lý quyen kho thiet bi
	 */
	@Override
	public List<EquipProposalBorrowVO> searchListStockPermissionVOByFilterExport(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT er.equip_role_id AS id, ");
		sql.append(" er.code AS code, ");
		sql.append(" er.name AS name, ");
		sql.append(" er.description AS description, ");
		sql.append(" er.status AS status, ");
		sql.append(" erd.equip_stock_id as idStock, ");
		sql.append(" (select es.code from equip_stock es where es.equip_stock_id = erd.equip_stock_id) as stockCode, ");
		sql.append(" (select es.name from equip_stock es where es.equip_stock_id = erd.equip_stock_id) as stockName, ");
		sql.append(" erd.is_under as isUnder ");
		sql.append(" FROM equip_role er ");
		sql.append(" left join equip_role_detail erd on er.equip_role_id = erd.equip_role_id ");
		sql.append(" where 1=1 ");
		if (filter.getStatus() != null) {
			sql.append("  and er.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append("  and er.status <> -1 ");
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(er.code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			sql.append(" and lower(er.name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getName().trim().toLowerCase()));
		}
		sql.append(" ORDER BY code asc ");
		String[] fieldNames = { "id", "code", "name", "description", "status", "idStock", "stockCode", "stockName", "isUnder" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	@Override
	public List<EquipProposalBorrowVO> searchStockByFilter(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
//		sql.append(" select es.equip_stock_id as id, (es.code || ' - '|| es.name) as stock, es.code as stockCode, es.name as stockName, s.shop_code as shopCode, s.shop_name as shopName, es.SHOP_ID as objectId,  s.shop_type_id as ch, ");
		sql.append(" select es.equip_stock_id as id, (es.code || ' - '|| es.name) as stock, es.code as stockCode, es.name as stockName, s.shop_code as shopCode, s.shop_name as shopName, es.SHOP_ID as objectId,  st.SPECIFIC_TYPE as ch, ");
//		sql.append(" (case when ");
//		sql.append(" ( select channel_type_id from channel_type where ");
//		sql.append(" type =1 and channel_type_id in (select shop_type_id from shop where shop_id = es.shop_id) ");
//		sql.append(" ) <> (select channel_type_id from channel_type where ");
//		sql.append(" type =1 and object_type = 3 ) ");
//		sql.append(" then 2 ");
//		sql.append(" else 0 ");
//		sql.append(" end) ");
//		sql.append(" as isUnder ");
		sql.append("( ");
		sql.append("  CASE ");
		sql.append("    WHEN st.SPECIFIC_TYPE is null or st.SPECIFIC_TYPE != 1 ");
		sql.append("    THEN 2 ");
		sql.append("    ELSE 0 ");
		sql.append("  END) AS isUnder");
		sql.append(" from equip_stock es");
		sql.append(" join shop s on s.shop_id = es.SHOP_ID ");
		sql.append(" join shop_type st on st.shop_type_id = s.shop_type_id and st.status = 1 ");
		sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s.shop_id = ogr.number_key_id ");
		params.add(filter.getStaffRootId());
		params.add(filter.getRoleId());
		params.add(filter.getShopRootId());
		
		sql.append(" where 1=1 ");
		sql.append(" and es.status =1 ");
		sql.append(" and s.status = 1");
		/*sql.append(" and es.SHOP_ID in (select shop_id from shop start with shop_id = ? ");
		params.add(filter.getShopId());
		sql.append(" connect by prior shop_id =parent_shop_id) ");*/
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and lower(shop_code) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getShopCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and lower(s.name_text) like ? escape '/'");
	//		sql.append(" and (EXISTS(SELECT shop_id FROM shop where status = 1 START WITH lower(shop_name) like ? ESCAPE '/' CONNECT BY PRIOR shop_id = parent_shop_id UNION ALL SELECT shop_id FROM shop where status = 1 START WITH lower(shop_name) like ? ESCAPE '/' CONNECT BY PRIOR parent_shop_id = shop_id)");
	//		params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopName().trim().toLowerCase()));
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopName().trim().toLowerCase()));			
		}
		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			sql.append(" and lower(es.code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getStockName())) {
			sql.append(" and lower(es.name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockName().trim().toLowerCase()));
		}
		if (filter.getLstEquipId() != null && filter.getLstEquipId().size() > 0) {
			sql.append(" and es.equip_stock_id not in (-1");
			List<Long> lstP = filter.getLstEquipId();
			for (Long pId : lstP) {
				sql.append(",?");
				params.add(pId);
			}
			sql.append(")");
		}
		if (filter.getLstIdStock() != null && filter.getLstIdStock().size() > 0) {
			sql.append(" and es.SHOP_ID not in (-1");
			List<Long> lstP = filter.getLstIdStock();
			for (Long pId : lstP) {
				sql.append(",?");
				params.add(pId);
			}
			sql.append(")");
		}
		sql.append(" ORDER BY shopCode, stockCode  DESC ");
		String[] fieldNames = { "id", "stock", "shopCode", "shopName", "stockCode", "stockName", "objectId", "isUnder", "ch" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public EquipRole getEquipRoleByCodeEx(String code, ActiveType status) throws DataAccessException {
		code = code.toUpperCase();
		StringBuilder sql = new StringBuilder();
		sql.append("select * from equip_role where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and code =? ");
			params.add(code.toUpperCase());
		}	
			sql.append(" and status <> -1 ");		
		return repo.getEntityBySQL(EquipRole.class, sql.toString(), params);
	}

	@Override
	public EquipRole createStockPermission(EquipRole eq, LogInfoVO logInfoVO) throws DataAccessException {
		if (eq == null) {
			throw new IllegalArgumentException("eq");
		}
		if (logInfoVO == null) {
			throw new IllegalArgumentException("logInfoVO is null");
		}
		if (eq.getCode() != null) {
			eq.setCode(eq.getCode().toUpperCase());
		}	
		eq.setCreateDate(commonDAO.getSysDate());
		repo.create(eq);		
		return eq;
	}

	@Override
	public EquipRole getStockPermissionById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipRole.class, id);
	}

	@Override
	public EquipRole saveStockPermission(EquipRole eq, LogInfoVO logInfoVO) throws DataAccessException {
		if (eq == null) {
			throw new IllegalArgumentException("eq");
		}
		if (logInfoVO == null) {
			throw new IllegalArgumentException("logInfoVO is null");
		}
		if (eq.getCode() != null) {
			eq.setCode(eq.getCode().toUpperCase());
		}		
		eq.setUpdateDate(commonDAO.getSysDate());
		repo.update(eq);		
		return eq;
	}

	@Override
	public void deleteStockPermission(EquipRole eq) throws DataAccessException {
		if (eq == null) {
			throw new IllegalArgumentException("eq");
		}
		repo.delete(eq);	
	}

	@Override
	public List<EquipProposalBorrowVO> getListtEquipStockByEquipRoleId(KPaging<EquipProposalBorrowVO> kPaging, Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct er.code as code, erd.is_under as isUnder, es.code as stockCode, (s.shop_code || ' - '||s.shop_name) as shopCode from equip_role er ");
		sql.append(" join equip_role_detail erd on erd.equip_role_id = er.equip_role_id");
		sql.append(" join equip_stock es on es.equip_stock_id = erd.EQUIP_STOCK_ID ");
		sql.append(" join shop s on s.shop_id = es.SHOP_ID ");
		sql.append(" where 1=1 ");
		//sql.append(" and er.status =1 ");
		if (id != null) {
			sql.append(" and er.equip_role_id = ?");
			params.add(id);
		}
		String[] fieldNames = { "stockCode", "shopCode", "isUnder", "code" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<EquipProposalBorrowVO> searchListStockPermissionVOAdd(EquipProposalBorrowFilter<EquipProposalBorrowVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/*** idStock: id cua detail; id: id cua kho dong detail */
		sql.append(" select  erd.EQUIP_ROLE_DETAIL_ID as idStock , es.EQUIP_STOCK_ID as id, (s.shop_code || ' - '||s.shop_name) as shopCode, es.code as stockCode, erd.is_under as isUnder, s.shop_type_id as ch from equip_role er ");
		sql.append(" join equip_role_detail erd on erd.equip_role_id = er.equip_role_id");
		sql.append(" join equip_stock es on erd.EQUIP_STOCK_ID =es.equip_stock_id ");
		sql.append(" join shop s on s.shop_id = es.SHOP_ID ");
		sql.append(" where 1=1 ");
		//	sql.append(" and er.status =1 ");
		if (filter.getId() != null) {
			sql.append(" and er.equip_role_id = ? ");
			params.add(filter.getId());
		}
		sql.append(" ORDER BY shopCode, stockCode ");
		String[] fieldNames = { "id", "idStock", "shopCode", "stockCode", "isUnder", "ch" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public EquipStock getEquipStockById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipStock.class, id);
	}

	@Override
	public EquipRoleDetail createStockPermission(EquipRoleDetail eqr) throws DataAccessException {
		if (eqr == null) {
			throw new IllegalArgumentException("eqr is null");
		}
		return repo.create(eqr);
	}

	@Override
	public List<EquipRoleDetail> getListEquipRoleDetailByEquipRoleId(Long id) throws DataAccessException {
		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_role_detail ");
		sql.append(" where 1=1 ");
		sql.append(" and equip_role_id =? ");
		params.add(id);
		return repo.getListBySQL(EquipRoleDetail.class, sql.toString(), params);
	}
	@Override
	public EquipRoleDetail getEquipRoleDetailById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipRoleDetail.class, id);
	}

	@Override
	public void deleteEquipRoleDetail(EquipRoleDetail eqr) throws DataAccessException {
		if (eqr == null) {
			throw new IllegalArgumentException("eqr is null");
		}
		repo.delete(eqr);
	}

	@Override
	public EquipRoleDetail getListEquipRoleDetailById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipRoleDetail.class, id);
	}

	@Override
	public List<EquipProposalBorrowVO> getListEquipRoleDetailByStockIdAndRoleId(Long idcheck, Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select  equip_role_detail_id as id, is_under as isUnder from equip_role_detail ");
		sql.append(" where 1= 1 ");
		if (id != null) {
			sql.append(" and equip_role_id = ? ");
			params.add(id);
		}
		if (idcheck != null) {
			sql.append(" and EQUIP_STOCK_ID = ? ");
			params.add(idcheck);
		}
		String[] fieldNames = { "id", "isUnder" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		return repo.getListByQueryAndScalar(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public EquipRoleDetail updateEquipRoleDetail(EquipRoleDetail eqrd, LogInfoVO logInfoVO) throws DataAccessException {
		if (eqrd == null) {
			throw new IllegalArgumentException("eqrd");
		}
		if (logInfoVO == null) {
			throw new IllegalArgumentException("logInfoVO is null");
		}		
		eqrd.setUpdateDate(commonDAO.getSysDate());
		repo.update(eqrd);		
		return eqrd;
	}

	@Override
	public EquipRoleDetail createEquipRoleDetail(EquipRoleDetail eRDetailNew) throws DataAccessException {
		if (eRDetailNew == null) {
			throw new IllegalArgumentException("eRDetailNew");
		}
		eRDetailNew.setCreateDate(commonDAO.getSysDate());
		repo.create(eRDetailNew);		
		return eRDetailNew;
	}

	@Override
	public List<EquipProposalBorrowVO> searchLstAddStockId(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select shop_id as id from shop ");		
		sql.append(" where shop_id <> ? ");
		params.add(id);
		sql.append(" start with shop_id = ? ");
		params.add(id);
		sql.append(" connect by prior shop_id = parent_shop_id ");
		String[] fieldNames = { "id"};
		Type[] fieldTypes = { StandardBasicTypes.LONG};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		return repo.getListByQueryAndScalar(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), params);	
	}

	@Override
	public List<EquipProposalBorrowVO> searchLstParenIdAddStockId(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select shop_id as id from shop ");		
		sql.append(" where shop_id <> ? ");
		params.add(id);
		sql.append(" start with shop_id = ? ");
		params.add(id);
		sql.append(" connect by prior shop_id = parent_shop_id ");
		String[] fieldNames = { "id"};
		Type[] fieldTypes = { StandardBasicTypes.LONG};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		return repo.getListByQueryAndScalar(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	@Override
	public List<EquipProposalBorrowVO> getListEquipRoleDetailByEquipRoleCode(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select equip_role_detail_id as id from equip_role_detail where 1=1");
		List<Object> params = new ArrayList<Object>();
		if (id != null) {
			sql.append("and equip_role_id = ? ");
			params.add(id);
		}				
		String[] fieldNames = { "id"};
		Type[] fieldTypes = { StandardBasicTypes.LONG};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		return repo.getListByQueryAndScalar(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipProposalBorrowVO> listEquipRoleById(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select equip_role_user_id as id from equip_role_user where 1= 1 and status = 1");
		List<Object> params = new ArrayList<Object>();
		if (id != null) {
			sql.append("and equip_role_id = ? ");
			params.add(id);
		}				
		String[] fieldNames = { "id"};
		Type[] fieldTypes = { StandardBasicTypes.LONG};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		return repo.getListByQueryAndScalar(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipProposalBorrowVO> searchCheckShopId(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT shop_id as id");
		sql.append(" FROM shop ");
		sql.append(" WHERE status = 1 ");
		if (id != null) {
		sql.append(" START WITH shop_id = ? ");
		params.add(id);
		}
		sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id ");
		String[] fieldNames = new String[] {
				"id"
		};
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG				
		};
		List<EquipProposalBorrowVO> lst = repo.getListByQueryAndScalar(EquipProposalBorrowVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
}

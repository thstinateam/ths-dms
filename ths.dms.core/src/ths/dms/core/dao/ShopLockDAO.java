package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.ShopLock;
import ths.dms.core.entities.ShopLockReleasedLog;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.filter.StaffFilter;
import ths.dms.core.entities.vo.ShopLockLogVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.DataAccessException;


public interface ShopLockDAO {
	ShopLock createShopLock(ShopLock shopLock) throws DataAccessException;
	void updateShopLock(ShopLock shopLock, Long shopId) throws DataAccessException;
	boolean isShopLocked(Long shopId) throws DataAccessException;
	boolean isShopLockedByCode(String shopCode) throws DataAccessException;
	/**
	 * Lay ds nhan vien ban hang DP
	 * 
	 * @author hoanv25
	 * @since Dec 28, 2014
	 */
	List<StaffVO> getListStockVOFilter(StaffFilter filter) throws DataAccessException;
	/**
	 * Lay ngay chot theo npp
	 * 
	 * @author lacnv1
	 * @since Mar 10, 2014
	 */
	Date getLockedDay(Long shopId) throws DataAccessException;
	
	/**
	 * Lay ngay chot theo npp
	 * 
	 * @author lacnv1
	 * @since Apr 2, 2014
	 */
	Date getApplicationDate(Long shopId) throws DataAccessException;
	
	/**
	 * Lấy ngày chốt = ngày đã chốt + 1
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 */
	Date getNextLockedDay(Long shopId) throws DataAccessException;
	
	boolean isStockTransLocked(Long shopId, Date lockDate) throws DataAccessException;
	
	ShopLock getShopLockVanSale(Long shopId, Date lockDate) throws DataAccessException;
	
	void updateShopLockVansale(ShopLock shopLock) throws DataAccessException;
	List<ShopLock> getListShopLock(Long shopId, Date fromDate) throws DataAccessException;
	void deleteShopLock(List<ShopLock> lst) throws DataAccessException;
	Date getMinLockDate(Long shopId) throws DataAccessException;
	
	/**
	 * @author tungmt
	 * @since 23/06/2015
	 * @param filter
	 * @return danh sach shop con va ngay chot cho man hinh chốt ngay 
	 * @throws DataAccessException
	 */
	List<ShopVO> getListChildShopForCloseDay(ShopFilter filter)
			throws DataAccessException;
	/**
	 * Lay bang lich su cap nhat ngay chot
	 * @author hunglm16
	 * @param shopId
	 * @param lockDate
	 * @return Don vi va cac tinh trang ghi nhan
	 * @throws DataAccessException
	 * @since 01/09/2015 
	 */
	List<ShopLockLogVO> getShopLockLogVOByShopAndLockDate(Long shopId, Date lockDate) throws DataAccessException;
	
	/**
	 * Xu ly getShopLockShopAndLockDate
	 * @author vuongmq
	 * @param shopId
	 * @param lockDate
	 * @return ShopLock
	 * @throws DataAccessException
	 * @since Apr 14, 2016
	*/
	ShopLock getShopLockShopAndLockDate(Long shopId, Date lockDate) throws DataAccessException;
	
	/**
	 * Xu ly getShopLockReleaseLogShopAndReleaseDate
	 * @author vuongmq
	 * @param shopId
	 * @param releaseDate
	 * @return ShopLockReleasedLog
	 * @throws DataAccessException
	 * @since Apr 14, 2016
	 */
	ShopLockReleasedLog getShopLockReleaseLogShopAndReleaseDate(Long shopId, Date releaseDate) throws DataAccessException;
}

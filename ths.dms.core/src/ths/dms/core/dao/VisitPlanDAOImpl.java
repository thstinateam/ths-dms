package ths.dms.core.dao;

import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Routing;
import ths.dms.core.entities.VisitPlan;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.SpParam;
import ths.dms.core.entities.filter.RoutingGeneralFilter;
import ths.dms.core.entities.filter.SupFilter;
import ths.dms.core.entities.filter.VisitPlanFilter;
import ths.dms.core.entities.vo.RoutingTreeVO;
import ths.dms.core.entities.vo.RoutingVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class VisitPlanDAOImpl implements VisitPlanDAO {

	@Autowired
	private IRepository repo;

	@Override
	public VisitPlan createVisitPlan(VisitPlan visitPlan) throws DataAccessException {
		if (visitPlan == null) {
			throw new IllegalArgumentException("visitPlan");
		}
		repo.create(visitPlan);
		return repo.getEntityById(VisitPlan.class, visitPlan.getId());
	}

	@Override
	public void deleteVisitPlan(VisitPlan visitPlan) throws DataAccessException {
		if (visitPlan == null) {
			throw new IllegalArgumentException("visitPlan");
		}
		repo.delete(visitPlan);
	}

	@Override
	public VisitPlan getVisitPlanById(Long id) throws DataAccessException {
		return repo.getEntityById(VisitPlan.class, id);
	}

	@Override
	public void updateVisitPlan(VisitPlan visitPlan) throws DataAccessException {
		if (visitPlan == null) {
			throw new IllegalArgumentException("visitPlan");
		}

		repo.update(visitPlan);
	}

	@Override
	public List<RoutingVO> exportTuyenBySelect(Long shopId, String routingCode, String routingName, String staffCode, Date sysdate, List<Long> lstRoutingId) throws DataAccessException {
		//		if (shopId == null) {
		//			throw new IllegalArgumentException("shopId");
		//		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT DISTINCT  ");
		sql.append("   s.shop_code               AS shopCode, ");
		sql.append("   rt.routing_code           AS routingCode, ");
		sql.append("   rt.routing_name           AS routingName, ");
		sql.append("   tb1.staff_code            AS staffCode, ");
		sql.append("   cus.short_code            AS customerShortCode, ");
		sql.append("   cus.customer_name         AS customerName, ");
		sql.append("   cus.address               AS customerAddress, ");
		sql.append("   ( CASE WHEN rcus.monday = 1 THEN 'x' ELSE '' END) AS monDayStr, ");
		sql.append("   ( CASE WHEN rcus.tuesday = 1 THEN 'x' ELSE '' END) AS tuesDayStr, ");
		sql.append("   ( CASE WHEN rcus.wednesday = 1 THEN 'x' ELSE '' END) AS wednesDayStr, ");
		sql.append("   ( CASE WHEN rcus.thursday = 1 THEN 'x' ELSE '' END) AS thursDayStr, ");
		sql.append("   ( CASE WHEN rcus.friday = 1 THEN 'x' ELSE '' END) AS firtDayStr, ");
		sql.append("   ( CASE WHEN rcus.saturday = 1 THEN 'x' ELSE '' END) AS saturDayStr, ");
		sql.append("   ( CASE WHEN rcus.sunday = 1 THEN 'x' ELSE '' END) AS sunDayStr, ");
		sql.append("   ( CASE WHEN rcus.week1 = 1 THEN 'x' ELSE '' END) AS week1Str, ");
		sql.append("   ( CASE WHEN rcus.week2 = 1 THEN 'x' ELSE '' END) AS week2Str, ");
		sql.append("   ( CASE WHEN rcus.week3 = 1 THEN 'x' ELSE '' END) AS week3Str, ");
		sql.append("   ( CASE WHEN rcus.week4 = 1 THEN 'x' ELSE '' END) AS week4Str, ");
		sql.append("  rcus.seq2 AS seq2,   ");
		sql.append("  rcus.seq3 AS seq3,   ");
		sql.append("  rcus.seq4 AS seq4,   ");
		sql.append("  rcus.seq5 AS seq5,   ");
		sql.append("  rcus.seq6 AS seq6,   ");
		sql.append("  rcus.seq7 AS seq7,   ");
		sql.append("  rcus.seq8  AS seq8,   ");
		sql.append(" (CASE WHEN rcus.status = 1 THEN 'x' ELSE '' END) AS statusRoutingCustomer,   ");
		sql.append("   rcus.week_interval as weekInterval, ");
		sql.append("   to_char(rcus.start_date, 'dd/MM/yyyy') as startDateStr, ");
		sql.append("   to_char(rcus.end_date, 'dd/MM/yyyy') as endDateStr, ");
		sql.append("   to_char(rcus.update_date, 'dd/MM/yyyy')   AS updateDateStr, ");
		sql.append("   rcus.FREQUENCY   AS tansuat ");
		sql.append(" FROM routing rt ");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" JOIN ");
		} else {
			sql.append(" LEFT JOIN ");
		}
		sql.append(" (select vp.routing_id, st.staff_code from visit_plan vp join staff st on vp.staff_id = st.staff_id  ");
		if (shopId != null) {
			sql.append(" and vp.shop_id = ? ");
			params.add(shopId);
		}
		sql.append("   AND vp.status = 1  and st.status = 1  ");

		sql.append("   AND (vp.from_date < TRUNC(sysdate) + 1 AND (vp.to_date IS NULL OR (TRUNC(sysdate) <= vp.to_date)))  ");
		sql.append("   ) tb1 ON rt.routing_id = tb1.routing_id  ");

		sql.append(" JOIN shop s ON rt.shop_id = s.shop_id ");
		sql.append(" JOIN routing_customer rcus ON rt.routing_id = rcus.routing_id  ");
		sql.append(" JOIN customer cus ON rcus.customer_id = cus.customer_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" and (rcus.start_date < TRUNC(sysdate) + 1 AND (rcus.end_date   IS NULL OR (TRUNC(sysdate) < rcus.end_date + 1))) ");
		sql.append(" and cus.status = 1 ");
		sql.append(" and rcus.status <> -1 ");
		if (shopId != null) {
			sql.append(" AND rt.shop_id = ? ");
			params.add(shopId);
		}

		if (!StringUtility.isNullOrEmpty(routingCode)) {
			sql.append(" and rt.routing_code like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(routingCode.trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(routingName)) {
			sql.append(" and unicode2english(rt.routing_name) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(routingName).trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and tb1.staff_code like ? escape '/'   ");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.trim().toUpperCase()));
		}

		if (lstRoutingId != null && lstRoutingId.size() > 0) {
			sql.append(" and rt.routing_id in (-1   ");
			for (Long routingId : lstRoutingId) {
				sql.append(", ? ");
				params.add(routingId);
			}
			sql.append("   ) ");
		}
		sql.append(" ORDER BY statusRoutingCustomer, rt.routing_code, cus.short_code, startDateStr, endDateStr ");

		String[] fieldNames = new String[] {//
				"shopCode", "routingCode", "routingName",
				"staffCode", "customerShortCode", "customerName",
				"customerAddress", "monDayStr", "tuesDayStr",
				"wednesDayStr", "thursDayStr", "firtDayStr",
				"saturDayStr", "sunDayStr", "week1Str",
				"week2Str", "week3Str", "week4Str", 
				"seq2", "seq3", "seq4", 
				"seq5", "seq6", "seq7", 
				"seq8", "statusRoutingCustomer", "weekInterval", 
				"startDateStr", "endDateStr", "updateDateStr",
				"tansuat" };
		Type[] fieldTypes = new Type[] {//
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(RoutingVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/**
	 * @author hunglm16
	 * @since April 18, 2014
	 * @description Lay danh sach tuyen trong shop
	 */
	@Override
	public List<Routing> getListRoutingShopId(Long shopId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from routing ");
		sql.append(" where status != -1 ");
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		sql.append(" order by routing_code ");
		return repo.getListBySQL(Routing.class, sql.toString(), params);
	}

	/**
	 * @author hunglm16
	 * @since MAY 17, 2014
	 * @description Lay danh sach tuyen trong shop by VO
	 */
	@Override
	public List<RoutingTreeVO> getListRoutingShopIdVO(Long shopId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select id,  routingCode, routingName, shopId, status, max(isStatus) as isStatus from (  ");
		sql.append(" select r.routing_id as id, r.routing_code as routingCode, r.routing_name as routingName,  ");
		sql.append(" r.shop_id as shopId, r.status as status, ");
		sql.append(" (case ");
		sql.append(" when vp.routing_id is null then 0 ");
		sql.append(" when vp.to_date is null or (vp.to_date >= TRUNC(sysdate) and vp.from_date <= vp.to_date) then 1 ");
		sql.append(" else 0 end) isStatus ");
		sql.append(" from routing r left join visit_plan vp on r.routing_id = vp.routing_id and vp.status = 1 ");
		if (shopId != null) {
			sql.append(" and vp.shop_id = ? ");
			params.add(shopId);
		}
		sql.append(" where  r.status != ? ");//r.status = 1 ");
		params.add(ActiveType.DELETED.getValue());
		if (shopId != null) {
			sql.append(" and r.shop_id = ? ");
			params.add(shopId);
		}
		sql.append(" ) ");
		sql.append(" group by id,  routingCode, routingName, shopId, status ");
		sql.append(" order by routingCode ");

		String[] fieldNames = new String[] {//
		"id", "routingCode", "routingName", "shopId", "status", "isStatus" };
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER};

		return repo.getListByQueryAndScalar(RoutingTreeVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.VisitPlanDAO#getListVisitPlanByShopId(java.lang.
	 * Long)
	 */
	@Override
	public List<RoutingVO> getListVisitPlanByShopId(KPaging<RoutingVO> kPaging, Long shopId, Long ownerId, ActiveType status, String customerCode, String staffCode, String routingCode, String routingName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select tb2.visit_plan_id as visitPlanId,");
		sql.append(" tb1.routing_id   as routingId,");
		sql.append(" tb1.routing_code as routingCode,");
		sql.append(" tb1.routing_name as routingName,");
		sql.append(" tb1.shop_code    as shopCode,");
		sql.append(" tb1.status   as status,");
		sql.append(" tb2.staff_id     as staff_id,");
		sql.append(" tb2.staff_code   as staffCode,");
		sql.append(" tb2.staff_name   as staffName");
		sql.append(" from ((");
		sql.append("select rt.routing_id, rt.routing_code, rt.routing_name, sh.shop_code, rt.create_date,rt.status from routing rt, shop sh");
		if (shopId != null) {
			sql.append(" where rt.shop_id = ?");
			params.add(shopId);
		} else {
			sql.append(" where rt.shop_id in (select distinct s.shop_id from staff sf, shop s ");
			sql.append(" where sf.shop_id = s.shop_id and sf.staff_owner_id = ?)");
			params.add(ownerId);
		}
		sql.append(" and rt.shop_id = sh.shop_id");
		//		sql.append(" and vp.from_date <= trunc(sysdate) and vp.to_date >= trunc(sysdate)");
		if (status != null) {
			sql.append(" and rt.status = ?");
			params.add(status.getValue());
		} else {
			sql.append(" and rt.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(routingCode)) {
			sql.append(" and rt.routing_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(routingCode.trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(routingName)) {
			sql.append(" and unicode2english(rt.routing_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(routingName).trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append(" AND rt.routing_id IN");
			sql.append(" (SELECT DISTINCT routing_id");
			sql.append("  FROM routing_customer rc,");
			sql.append(" customer c");
			sql.append(" WHERE rc.customer_id = c.customer_id");
			sql.append(" AND c.customer_code LIKE ?)");
			params.add(StringUtility.toOracleSearchLike(customerCode.trim().toUpperCase()));
		}
		sql.append(" )");
		sql.append(" tb1");
		sql.append(" left join");
		sql.append(" (select s.staff_id, s.staff_code, s.staff_name, vp.visit_plan_id, vp.routing_id");
		sql.append(" from visit_plan vp, staff s");
		sql.append(" where vp.staff_id = s.staff_id and s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and vp.from_date <= trunc(sysdate) and (vp.to_date is null or vp.to_date >= trunc(sysdate)) and vp.status = 1) tb2");
		sql.append(" on tb1.routing_id = tb2.routing_id)");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(", staff s");
			sql.append(" where tb2.staff_id = s.staff_id and s.staff_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(staffCode.trim().toUpperCase()));
		}
		sql.append(" order by tb1.status desc, tb1.routing_code, tb1.create_date desc");
		// count sql
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from (");
		countSql.append(sql.toString());
		countSql.append(" )");
		String[] fieldNames = new String[] {//
		"visitPlanId", "routingId", "staffCode", "staffName", "routingCode", "routingName", "shopCode", "status" };
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };
		if (kPaging != null)
			return repo.getListByQueryAndScalarPaginated(RoutingVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		else
			return repo.getListByQueryAndScalar(RoutingVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RoutingVO> searchListRoutingVOByFilter(RoutingGeneralFilter<RoutingVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select tb2.visit_plan_id as visitPlanId,");
		sql.append(" tb1.routing_id   as routingId,");
		sql.append(" tb1.routing_code as routingCode,");
		sql.append(" tb1.routing_name as routingName,");
		sql.append(" tb1.shop_code    as shopCode,");
		sql.append(" tb1.status   as status,");
		sql.append(" tb2.staff_id     as staff_id,");
		sql.append(" tb2.staff_code   as staffCode,");
		sql.append(" tb2.staff_name   as staffName");
		sql.append(" from ((");
		sql.append("select rt.routing_id, rt.routing_code, rt.routing_name, sh.shop_code, rt.create_date,rt.status from routing rt, shop sh");
		sql.append(" where 1 = 1 ");
		if (filter.getShopId() != null) {
			sql.append(" and rt.shop_id = ?");
			params.add(filter.getShopId());
		}
		sql.append(" and rt.shop_id = sh.shop_id");
		if (filter.getStatus() != null) {
			sql.append(" and rt.status = ?");
			params.add(filter.getStatus());
		} else {
			sql.append(" and rt.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getRoutingCode())) {
			sql.append(" and rt.routing_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getRoutingCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getRoutingName())) {
			sql.append(" and unicode2english(rt.routing_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getRoutingName()).toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" AND rt.routing_id IN");
			sql.append(" (SELECT DISTINCT routing_id");
			sql.append("  FROM routing_customer rc,");
			sql.append(" customer c");
			sql.append(" WHERE rc.customer_id = c.customer_id");
			sql.append(" AND c.customer_code LIKE ?)");
			params.add(StringUtility.toOracleSearchLike(filter.getShortCode().toUpperCase()));
		}
		sql.append(" )");
		sql.append(" tb1");
		sql.append(" left join");
		sql.append(" (select s.staff_id, s.staff_code, s.staff_name, vp.visit_plan_id, vp.routing_id");
		sql.append(" from visit_plan vp, staff s");
		sql.append(" where vp.staff_id = s.staff_id and s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and vp.from_date <= trunc(sysdate) and (vp.to_date is null or vp.to_date >= trunc(sysdate)) and vp.status = 1) tb2");
		sql.append(" on tb1.routing_id = tb2.routing_id)");
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(", staff s");
			sql.append(" where tb2.staff_id = s.staff_id and s.staff_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getStaffCode().toUpperCase()));
		}
		sql.append(" order by tb1.status desc,upper(tb1.routing_code), tb1.create_date desc");
		// count sql
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from (");
		countSql.append(sql.toString());
		countSql.append(" )");
		String[] fieldNames = new String[] { "visitPlanId", "routingId", "staffCode", "staffName", "routingCode", "routingName", "shopCode", "status" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER };
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(RoutingVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(RoutingVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public Boolean checkRoutingErrDelete(Long routingId, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select COUNT(1) as count from visit_plan ");
		sql.append(" where  1 = 1 ");
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		sql.append(" and from_date < trunc(sysdate) + 1 ");
		sql.append(" and (to_date is null or to_date >= trunc(sysdate)) ");
		sql.append(" and status = 1 ");
		if (routingId != null) {
			sql.append(" and routing_id = ? ");
			params.add(routingId);
		}
		int count = repo.countBySQL(sql.toString(), params);
		if (count > 0) {
			return false;
		}
		return true;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.VisitPlanDAO#getListVisitPlanByRoutingId(java.lang
	 * .Long)
	 */
	@Override
	public List<VisitPlan> getListVisitPlanByRoutingId(SupFilter<VisitPlan> filter) throws DataAccessException {
		if (filter.getRoutingId() == null) {
			throw new IllegalArgumentException("routingId");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from visit_plan where routing_id = ? and status = 1");
		params.add(filter.getRoutingId());
		if (!StringUtility.isNullOrEmpty(filter.getStrStaffId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrStaffId(), "staff_id");
			sql.append(paramsFix);

		}
		sql.append(" order by from_date desc");

		if (filter.getkPaging() != null)
			return repo.getListBySQLPaginated(VisitPlan.class, sql.toString(), params, filter.getkPaging());
		else
			return repo.getListBySQL(VisitPlan.class, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.dao.VisitPlanDAO#getVisitPlan(java.lang.Long,
	 * java.util.Date, java.util.Date)
	 */
	@Override
	public VisitPlan getVisitPlan(Long routingId, Date fromDate, Date toDate) throws DataAccessException {
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return null;

		if (routingId == null) {
			throw new IllegalArgumentException("routingId is null");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from visit_plan where routing_id = ?");
		params.add(routingId);
		if (toDate != null) {
			sql.append(" and ((from_date >= trunc(?) and from_date < trunc(?) + 1) or (to_date >= trunc(?) and to_date < trunc(?) + 1) or (from_date < trunc(?) + 1 and to_date > trunc(?)))");
			params.add(fromDate);
			params.add(toDate);
			params.add(fromDate);
			params.add(toDate);
			params.add(fromDate);
			params.add(toDate);
		} else {
			sql.append(" and (from_date >= trunc(?) or (from_date < trunc(?) + 1 and to_date > trunc(?)))");
			params.add(fromDate);
			params.add(fromDate);
			params.add(fromDate);
		}
		return repo.getEntityBySQL(VisitPlan.class, sql.toString(), params);
	}

	@Override
	public VisitPlan getVisitPlanMultiChoce(VisitPlanFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from visit_plan  ");
		sql.append(" where 1 = 1 ");
		if (filter.getStaffId() != null) {
			sql.append(" and staff_id = ? ");
			params.add(filter.getStaffId());
		}
		if (filter.getRoutingId() != null) {
			sql.append(" and routing_id = ? ");
			params.add(filter.getRoutingId());
		} else if (filter.getExceptRouteId() != null) {
			sql.append(" and routing_id != ? ");
			params.add(filter.getExceptRouteId());
		}
		if (filter.getShopId() != null) {
			sql.append(" and shop_id = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getStatus() != null) {
			sql.append(" and status = ? ");
			params.add(filter.getStatus());
		}
		if (filter.getFromDate() != null) {
			sql.append(" and (to_date is null or trunc(?) <= to_date) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and from_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		return repo.getEntityBySQL(VisitPlan.class, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.dao.VisitPlanDAO#checkExitVisitPlan(java.lang.Long,
	 * java.lang.Long, java.lang.Long, java.util.Date, java.util.Date)
	 */
	@Override
	public Boolean checkExitVisitPlan(Long routingId, Long visitPlanId, Long staffId, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(*) as count from (");
		sql.append(" select * from visit_plan vp where vp.routing_id = ? and vp.visit_plan_id <> ? and vp.status = ? and exists (select 1 from routing where status = 1 and routing_id = vp.routing_id)");
		params.add(routingId);
		params.add(visitPlanId);
		params.add(ActiveType.RUNNING.getValue());
		if (toDate != null) {
			sql.append(" and ((vp.from_date >= trunc(?) and vp.from_date < trunc(?) + 1) or (vp.to_date >= trunc(?) and vp.to_date < trunc(?) + 1) or (vp.from_date < trunc(?) + 1 and vp.to_date > trunc(?)))");
			params.add(fromDate);
			params.add(toDate);
			params.add(fromDate);
			params.add(toDate);
			params.add(fromDate);
			params.add(toDate);
		} else {
			sql.append(" and (vp.from_date >= trunc(?) or (vp.from_date < trunc(?) + 1 and vp.to_date > trunc(?)))");
			params.add(fromDate);
			params.add(fromDate);
			params.add(fromDate);
		}
		sql.append(" union");
		sql.append(" select * from visit_plan vp where vp.routing_id <> ? and vp.staff_id = ? and vp.status = ? and exists (select 1 from routing where status = 1 and routing_id = vp.routing_id)");
		params.add(routingId);
		params.add(staffId);
		params.add(ActiveType.RUNNING.getValue());
		if (toDate != null) {
			sql.append(" and ((vp.from_date >= trunc(?) and vp.from_date < trunc(?) + 1) or (vp.to_date >= trunc(?) and vp.to_date < trunc(?) + 1) or (vp.from_date < trunc(?) + 1 and vp.to_date > trunc(?))))");
			params.add(fromDate);
			params.add(toDate);
			params.add(fromDate);
			params.add(toDate);
			params.add(fromDate);
			params.add(toDate);
		} else {
			sql.append(" and (vp.from_date >= trunc(?) or (vp.from_date < trunc(?) + 1 and vp.to_date > trunc(?))))");
			params.add(fromDate);
			params.add(fromDate);
			params.add(fromDate);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public List<VisitPlan> checkExitVisitPlanInShop(Long shopId, Long routingId, Long visitPlanId, Long staffId, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM visit_plan ");
		sql.append(" where status = 1 ");
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		if (visitPlanId != null) {
			sql.append(" and visit_plan_id <> ? ");
			params.add(visitPlanId);
		}
		if (staffId != null && routingId != null) {
			sql.append(" and ((staff_id = ? and routing_id <> ?) or (staff_id <> ? and routing_id = ?) or (staff_id = ? AND routing_id     = ?)) ");
			params.add(staffId);
			params.add(routingId);
			params.add(staffId);
			params.add(routingId);
			params.add(staffId);
			params.add(routingId);
		}
		if (fromDate != null && toDate != null) {
			sql.append(" and ((from_date <= trunc(?) and (to_date is null or to_date>=trunc(?)))  or (from_date <= trunc(?) and (to_date is null or to_date>=trunc(?)))) ");
			params.add(fromDate);
			params.add(fromDate);
			params.add(toDate);
			params.add(toDate);
		} else if (fromDate != null) {
			sql.append(" and(from_date >= trunc(?) or (from_date <= trunc(?) and (to_date is null or to_date>=trunc(?)))) ");
			params.add(fromDate);
			params.add(fromDate);
			params.add(fromDate);
		}
		return repo.getListBySQL(VisitPlan.class, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.VisitPlanDAO#getListVisitPlanForManagerPlan(com.
	 * viettel.core.entities.enumtype.KPaging, java.lang.Long, java.lang.Long,
	 * java.util.Date, ths.dms.core.entities.enumtype.ActiveType)
	 */
	@Override
	public List<VisitPlan> getListVisitPlanForManagerPlan(KPaging<VisitPlan> kPaging, Long shopId, Long staffId, Date date, ActiveType type) throws DataAccessException {
		// TODO Auto-generated method stub
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (staffId == null) {
			throw new IllegalArgumentException("staffId is null");
		}
		if (date == null) {
			throw new IllegalArgumentException("date is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select vp.* from visit_plan vp, customer c, staff s");
		sql.append(" where vp.shop_id = ?");
		params.add(shopId);
		sql.append(" and vp.staff_id = ?");
		params.add(staffId);
		sql.append(" and vp.active = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and vp.staff_id = s.staff_id");
		sql.append(" and s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and trunc(vp.start_date, 'month') <= trunc(?) + 1");
		params.add(date);
		sql.append(" and (vp.end_date >= trunc(?) or vp.end_date is null)");
		params.add(date);
		sql.append(" and c.customer_id = vp.customer_id");
		sql.append(" and c.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" order by c.customer_code");
		if (kPaging != null)
			return repo.getListBySQLPaginated(VisitPlan.class, sql.toString(), params, kPaging);
		else
			return repo.getListBySQL(VisitPlan.class, sql.toString(), params);
	}

	@Override
	public void updateVisitPlanStatusToStop(Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("update visit_plan set to_date = sysdate where status = 1 and staff_id = ? and from_date <= trunc(sysdate) and (to_date is null or to_date >= trunc(sysdate)) ");
		if (staffId != null) {
			params.add(staffId);
		}
		repo.executeSQLQuery(sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.VisitPlanDAO#offVisitPlanWithRoutingId(java.lang
	 * .Long)
	 */
	@Override
	public void offVisitPlanWithRoutingId(Long routingId, String updateUser, Date updateDate) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("update visit_plan set status = ?, update_user = ?, update_date = ? where routing_id = ?");
		params.add(ActiveType.DELETED.getValue());
		params.add(updateUser);
		params.add(updateDate);
		params.add(routingId);
		repo.executeSQLQuery(sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.VisitPlanDAO#checkVisitPlanWithSP(java.lang.Long,
	 * java.util.Date, java.util.Date)
	 */
	@Override
	public Boolean checkVisitPlanWithSP(Long routingId, Date fromDate, Date toDate) throws DataAccessException {
		// TODO Auto-generated method stub
		Integer count = 0;
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String tungay = df.format(fromDate);
		String denngay = "";
		if (toDate != null) {
			denngay = df.format(toDate);
		}
		List<SpParam> inParams = new ArrayList<SpParam>();
		List<SpParam> outParams = new ArrayList<SpParam>();
		inParams.add(new SpParam(2, Types.VARCHAR, tungay));
		inParams.add(new SpParam(3, Types.VARCHAR, denngay));
		inParams.add(new SpParam(4, Types.NUMERIC, routingId));
		outParams.add(new SpParam(1, Types.NUMERIC, null));
		repo.executeSP("PKG_PAID_DISPLAY_PROGRAM.CHECK_VISIT_PLAN", inParams, outParams);
		for (SpParam out : outParams) {
			count = (Integer) out.getValue();
		}

		return count > 1 ? true : false;
	}

	@Override
	public List<VisitPlan> getListVisitPlanByFuture(Long staffId, Long routingId, Long shopId, Date sysdate, Integer flag) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from visit_plan ");
		sql.append(" where 1 = 1 ");
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		if (flag == 0 && routingId != null && staffId != null) {
			sql.append(" and routing_id = ? ");
			params.add(routingId);
			sql.append(" and staff_id = ? ");
			params.add(staffId);
		}
		if (routingId != null && flag == 2) {
			sql.append(" and routing_id = ? ");
			params.add(routingId);
			sql.append(" and staff_id <> ? ");
			params.add(staffId);
		}
		if (staffId != null && flag == 1) {
			sql.append(" and staff_id = ? ");
			params.add(staffId);
			sql.append(" and routing_id <> ? ");
			params.add(routingId);
		}

		if (sysdate != null) {
			sql.append(" and trunc(sysdate) <= from_date ");
		}
		sql.append(" and status = 1 ");
		return repo.getListBySQL(VisitPlan.class, sql.toString(), params);
	}

	@Override
	public VisitPlan getListVisitPlanByCurrent(Long staffId, Long routingId, Long shopId, Date sysdate, Integer flag) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from visit_plan ");
		sql.append(" where 1 = 1 ");
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		if (flag == 0 && routingId != null && staffId != null) {
			sql.append(" and routing_id = ? ");
			params.add(routingId);
			sql.append(" and staff_id = ? ");
			params.add(staffId);
		}
		if (routingId != null && flag == 2) {
			sql.append(" and routing_id = ? ");
			params.add(routingId);
			sql.append(" and staff_id <> ? ");
			params.add(staffId);
		}
		if (staffId != null && flag == 1) {
			sql.append(" and staff_id = ? ");
			params.add(staffId);
			sql.append(" and routing_id <> ? ");
			params.add(routingId);
		}
		sql.append(" and from_date <= trunc(?) and (to_date is null or trunc(?) < to_date + 1) ");
		params.add(sysdate);
		params.add(sysdate);
		return repo.getEntityBySQL(VisitPlan.class, sql.toString(), params);
	}

	@Override
	public List<VisitPlan> getListVisitPlanErrByStartDate(Long staffId, Long routingId, Date fDate, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select * from visit_plan  ");
		sql.append(" where status = 1  ");
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
			sql.append(" and routing_id in(select routing_id from routing where shop_id = ?) ");
			params.add(shopId);
		}
		if (staffId != null) {
			sql.append(" and staff_id  = ?  ");
			params.add(staffId);
		}
		if (routingId != null) {
			sql.append(" and routing_id = ?  ");
			params.add(routingId);
		}
		if (fDate != null) {
			sql.append(" and from_date < trunc(?) and (to_date is null or to_date > trunc(?) - 1)  ");
			params.add(fDate);
			params.add(fDate);
		} else {
			sql.append(" and from_date > trunc(sysdate) ");
		}
		sql.append(" order by from_date ");
		return repo.getListBySQL(VisitPlan.class, sql.toString(), params);
	}

	@Override
	public List<VisitPlan> getListVisitPlanInCurrent(Long staffId, Long routingId, Long shopId, Date fDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from visit_plan  ");
		sql.append(" where status = 1  ");
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		if (staffId != null) {
			sql.append(" and staff_id  = ?  ");
			params.add(staffId);
		}
		if (routingId != null) {
			sql.append(" and routing_id = ?  ");
			params.add(routingId);
		}
		if (fDate != null) {
			sql.append(" and from_date <= trunc(?) and (to_date is null or to_date > trunc(?) + 1)  ");
			params.add(fDate);
		} else {
			sql.append(" and from_date <= trunc(sysdate) and (to_date is null or to_date > trunc(sysdate) + 1)  ");
		}
		return repo.getListBySQL(VisitPlan.class, sql.toString(), params);
	}

	@Override
	public VisitPlan getVisitPlanWitFDateMin(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with r1 as(  ");
		sql.append(" select visit_plan_id as visit_plan_id, min(from_date) as from_date from visit_plan  ");
		sql.append(" where status = 1 and from_date > trunc(sysdate) + 1  ");
		sql.append(" group by (visit_plan_id)  ");
		sql.append(" )  ");
		sql.append(" select * from visit_plan where visit_plan_id in (select visit_plan_id from r1)  ");
		return repo.getEntityBySQL(VisitPlan.class, sql.toString(), params);
	}

	/**
	 * @author hunglm16
	 * @since April 13, 2014
	 * @description Lay cac tuyen trong trong Shop voi NVBH
	 * */
	@Override
	public List<VisitPlan> getListVisitPlanByStaffId(Long staffId, Long shopId, int flagTime) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from visit_plan ");
		sql.append(" where 1 = 1 ");
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		if (staffId != null) {
			sql.append(" and staff_id = ?  ");
			params.add(staffId);
		}
		if (flagTime == 0) {//Qua Khu
			sql.append(" and trunc(sysdate) > to_date ");
		} else if (flagTime == 1) {//hien tai
			sql.append(" and from_date <= trunc(sysdate) and (to_date is null or trunc(sysdate) <= to_date) ");
		} else if (flagTime == 2) {//Tuong lai
			sql.append(" and trunc(sysdate) <= from_date ");
		}
		sql.append(" and status in (0, 1) ");
		return repo.getListBySQL(VisitPlan.class, sql.toString(), params);
	}

	/**
	 * @author hunglm16
	 * @since April 14, 2014
	 * @description Lay visitplan voi day du cac tieu chi
	 * */
	@Override
	public VisitPlan getVisitPlanByFullId(Long staffId, Long routingId, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from visit_plan ");
		sql.append(" where status = 1 ");
		sql.append(" and from_date > trunc(sysdate) - 1 ");
		sql.append(" and from_date < trunc(sysdate) + 1 ");
		if (routingId != null) {
			sql.append(" and routing_id = ? ");
			params.add(routingId);
		}
		if (staffId != null) {
			sql.append(" and staff_id = ? ");
			params.add(staffId);
		}
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		return repo.getFirstBySQL(VisitPlan.class, sql.toString(), params);
	}

	/**
	 * @author hunglm16
	 * @since April 14, 2014
	 * @description Lay visitplan by Control Import
	 * */
	@Override
	public List<VisitPlan> getListVisitPlanByImport(VisitPlan visitPlan, Boolean flag) throws DataAccessException {
		if (visitPlan == null || flag == null) {
			throw new IllegalArgumentException("Shop Or Flag is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from visit_plan ");
		if (!flag) {
			sql.append(" where visit_plan_id in ( ");
			sql.append(" select visit_plan_id from ( ");
			sql.append(" ( select * from visit_plan vp1 ");
			sql.append("   where vp1.status = 1 and vp1.shop_id = ? and vp1.staff_id = ? and vp1.routing_id <> ? ");
			params.add(visitPlan.getShop().getId());
			params.add(visitPlan.getStaff().getId());
			params.add(visitPlan.getRouting().getId());
			if (visitPlan.getToDate() != null) {
				sql.append(" 	and ((vp1.from_date <= TRUNC(?) and (vp1.to_date is null or vp1.to_date >= TRUNC(?)))");
				params.add(visitPlan.getToDate());
				params.add(visitPlan.getToDate());
				sql.append(" 		or (vp1.from_date <= TRUNC(?) and (vp1.to_date is null or vp1.to_date >= TRUNC(?)))");
				params.add(visitPlan.getFromDate());
				params.add(visitPlan.getFromDate());
				sql.append(" 		or (vp1.from_date <= TRUNC(?) and (vp1.to_date is null or vp1.to_date >= TRUNC(?)))");
				params.add(visitPlan.getFromDate());
				params.add(visitPlan.getToDate());
				sql.append(" 		or (vp1.from_date >= TRUNC(?) and vp1.to_date <= TRUNC(?))");
				params.add(visitPlan.getFromDate());
				params.add(visitPlan.getToDate());
				sql.append(" 		) ");
				/*
				 * sql.append(
				 * " and ((vp1.from_date <= trunc(?) and vp1.to_date >= TRUNC(?)) or (vp1.from_date <= trunc(?) and (vp1.to_date is null or vp1.to_date >= TRUNC(?)))) "
				 * ); params.add(visitPlan.getFromDate());
				 * params.add(visitPlan.getFromDate());
				 * params.add(visitPlan.getToDate());
				 * params.add(visitPlan.getToDate());
				 */
			} else {
				sql.append("   and (vp1.to_date   IS NULL OR vp1.to_date     >= TRUNC(sysdate)) ");
			}
			sql.append(" ) union ( ");
			sql.append("   select * from visit_plan vp2 ");
			sql.append("   where vp2.status = 1 and vp2.shop_id = ? and vp2.staff_id <> ? and vp2.routing_id = ? ");
			params.add(visitPlan.getShop().getId());
			params.add(visitPlan.getStaff().getId());
			params.add(visitPlan.getRouting().getId());
			if (visitPlan.getToDate() != null) {
				sql.append(" 	and ((vp2.from_date <= TRUNC(?) and (vp2.to_date is null or vp2.to_date >= TRUNC(?)))");
				params.add(visitPlan.getToDate());
				params.add(visitPlan.getToDate());
				sql.append(" 		or (vp2.from_date <= TRUNC(?) and (vp2.to_date is null or vp2.to_date >= TRUNC(?)))");
				params.add(visitPlan.getFromDate());
				params.add(visitPlan.getFromDate());
				sql.append(" 		or (vp2.from_date <= TRUNC(?) and (vp2.to_date is null or vp2.to_date >= TRUNC(?)))");
				params.add(visitPlan.getFromDate());
				params.add(visitPlan.getToDate());
				sql.append(" 		or (vp2.from_date >= TRUNC(?) and vp2.to_date <= TRUNC(?))");
				params.add(visitPlan.getFromDate());
				params.add(visitPlan.getToDate());
				sql.append(" 		) ");

				/*
				 * sql.append(
				 * " and ((vp2.from_date <= trunc(?) and vp2.to_date >= TRUNC(?)) or (vp2.from_date <= trunc(?) and (vp2.to_date is null or vp2.to_date >= TRUNC(?)))) "
				 * ); params.add(visitPlan.getFromDate());
				 * params.add(visitPlan.getFromDate());
				 * params.add(visitPlan.getToDate());
				 * params.add(visitPlan.getToDate());
				 */
			} else {
				sql.append("   and (vp2.to_date   IS NULL OR vp2.to_date     >= TRUNC(sysdate)) ");
			}
			sql.append(" ))) ");
		} else {
			sql.append("   where status = 1 and shop_id = ? and staff_id = ? and routing_id = ? ");
			params.add(visitPlan.getShop().getId());
			params.add(visitPlan.getStaff().getId());
			params.add(visitPlan.getRouting().getId());
			sql.append("   and (to_date is null or to_date  >= TRUNC(sysdate)) ");
		}
		return repo.getListBySQL(VisitPlan.class, sql.toString(), params);
	}

	/**
	 * @author hunglm16
	 * @since May 12, 2014
	 * @description Lay visitplan by Control Import
	 * */
	@Override
	public List<VisitPlan> getListVisitPlanByForm(VisitPlan visitPlan, Boolean flag) throws DataAccessException {
		if (visitPlan == null || flag == null) {
			throw new IllegalArgumentException("Shop Or Flag is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from visit_plan ");
		if (!flag) {
			sql.append(" where visit_plan_id in ( ");
			sql.append(" select visit_plan_id from ( ");
			sql.append(" ( select * from visit_plan vp1 ");
			sql.append("   where vp1.status = 1 and vp1.shop_id = ? and vp1.staff_id = ? and vp1.routing_id <> ? ");
			params.add(visitPlan.getShop().getId());
			params.add(visitPlan.getStaff().getId());
			params.add(visitPlan.getRouting().getId());
			if (visitPlan.getToDate() != null) {
				sql.append(" and vp1.from_date <= ? and (vp1.to_date is null or vp1.to_date >= ?)");
				params.add(visitPlan.getToDate());
				params.add(visitPlan.getFromDate());
			} else {
				if (visitPlan.getFromDate() != null) {
					sql.append("   and (vp1.to_date   IS NULL OR vp1.to_date     >= TRUNC(?)) ");
					params.add(visitPlan.getFromDate());
				} else {
					sql.append("   and (vp1.to_date   IS NULL OR vp1.to_date     >= TRUNC(sysdate)) ");
				}
			}
			sql.append(" ) union ( ");
			sql.append("   select * from visit_plan vp2 ");
			sql.append("   where vp2.status = 1 and vp2.shop_id = ? and vp2.routing_id = ? ");
			params.add(visitPlan.getShop().getId());
			params.add(visitPlan.getRouting().getId());
			if (visitPlan.getToDate() != null) {
				sql.append(" and vp2.from_date <= ? and (vp2.to_date is null or vp2.to_date >= ?)");
				params.add(visitPlan.getToDate());
				params.add(visitPlan.getFromDate());
			} else {
				if (visitPlan.getFromDate() != null) {
					sql.append("   and (vp2.to_date   IS NULL OR vp2.to_date     >= TRUNC(?)) ");
					params.add(visitPlan.getFromDate());
				} else {
					sql.append("   and (vp2.to_date   IS NULL OR vp2.to_date     >= TRUNC(sysdate)) ");
				}
			}
			sql.append(" ))) ");
		} else {
			sql.append("   where status = 1 and shop_id = ? and staff_id = ? and routing_id = ? ");
			params.add(visitPlan.getShop().getId());
			params.add(visitPlan.getStaff().getId());
			params.add(visitPlan.getRouting().getId());
			sql.append("   and (to_date is null or to_date  >= TRUNC(sysdate)) ");
		}
		return repo.getListBySQL(VisitPlan.class, sql.toString(), params);
	}

	/**
	 * @author hunglm16
	 * @since April 13, 2014
	 * @description Lay cac tuyen trong trong Shop voi NVBH
	 * */
	@Override
	public List<VisitPlan> getListVisitPlanByRoutingIDAndFlag(Long routingId, Long shopId, int flagTime) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from visit_plan ");
		sql.append(" where 1 = 1 ");
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		if (routingId != null) {
			sql.append(" and routing_id = ?  ");
			params.add(routingId);
		}
		if (flagTime == 0) {//Qua Khu
			sql.append(" and trunc(sysdate) > to_date ");
		} else if (flagTime == 1) {//hien tai
			sql.append(" and from_date <= trunc(sysdate) and (to_date is null or trunc(sysdate) <= to_date) ");
		} else if (flagTime == 2) {//Tuong lai
			sql.append(" and trunc(sysdate) <= from_date ");
		}
		sql.append(" and status in (0, 1) ");
		return repo.getListBySQL(VisitPlan.class, sql.toString(), params);
	}

	@Override
	public void deleteVisitPlanStatusToStop(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("delete  from visit_plan where status = 1 and staff_id = ? and from_date > trunc(sysdate) ");
		if (id != null) {
			params.add(id);
		}
		repo.executeSQLQuery(sql.toString(), params);
	}
	
}

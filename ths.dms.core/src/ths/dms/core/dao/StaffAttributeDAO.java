package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.StaffAttribute;
import ths.dms.core.entities.StaffAttributeDetail;
import ths.dms.core.entities.StaffAttributeEnum;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumFilter;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.StaffAttributeDetailVO;
import ths.dms.core.entities.filter.StaffAttributeFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.StaffAttributeEnumVO;
import ths.dms.core.entities.vo.StaffAttributeVO;
import ths.dms.core.exceptions.DataAccessException;
// TODO: Auto-generated Javadoc

/**
 * The Interface StaffAttributeDAO.
 *
 * @author liemtpt
 * @since 21/01/2015
 */
public interface StaffAttributeDAO {

	/**
	 * Creates the staff attribute.
	 *
	 * @param staffAttribute the staff attribute
	 * @param logInfo the log info
	 * @return the staff attribute
	 * @throws DataAccessException the data access exception
	 */
	StaffAttribute createStaffAttribute(StaffAttribute staffAttribute,
			LogInfoVO logInfo) throws DataAccessException;

	/**
	 * Delete staff attribute.
	 *
	 * @param staffAttribute the staff attribute
	 * @throws DataAccessException the data access exception
	 */
	void deleteStaffAttribute(StaffAttribute staffAttribute) throws DataAccessException;

	/**
	 * Update staff attribute.
	 *
	 * @param staffAttribute the staff attribute
	 * @param logInfo the log info
	 * @throws DataAccessException the data access exception
	 */
	void updateStaffAttribute(StaffAttribute staffAttribute, LogInfoVO logInfo)
	throws DataAccessException;
	
	/**
	 * Gets the staff attribute by id.
	 *
	 * @param id the id
	 * @return the staff attribute by id
	 * @throws DataAccessException the data access exception
	 */
	StaffAttribute getStaffAttributeById(long id) throws DataAccessException;
	
	/**
	 * Gets the staff attribute by code.
	 *
	 * @param code the code
	 * @return the staff attribute by code
	 * @throws DataAccessException the data access exception
	 */
	StaffAttribute getStaffAttributeByCode(String code) throws DataAccessException;
   
   /**
    * **
    * Danh sach thuoc tinh dong cho nhan vien.
    *
    * @param filter the filter
    * @param kPaging the k paging
    * @return list staff attribute vo
    * @throws DataAccessException the data access exception
    * @author liemtpt
    */
	List<AttributeDynamicVO> getListStaffAttributeVO(AttributeDynamicFilter filter,
			KPaging<AttributeDynamicVO> kPaging) throws DataAccessException;
	
	/**
	 * **
	 * Danh sach gia tri thuoc tinh khi chon mot va chon nhieu.
	 *
	 * @param filter the filter
	 * @param kPaging the k paging
	 * @return list staff attribute enum vo
	 * @throws DataAccessException the data access exception
	 * @author liemtpt
	 */
	List<AttributeEnumVO> getListStaffAttributeEnumVO(AttributeEnumFilter filter,
		KPaging<AttributeEnumVO> kPaging) throws DataAccessException;

	/**
	 * Gets the staff attribute enum by code.
	 *
	 * @param code the code
	 * @return the staff attribute enum by code
	 * @throws DataAccessException the data access exception
	 */
	StaffAttributeEnum getStaffAttributeEnumByCode(String code)
			throws DataAccessException;

	/**
	 * Update staff attribute enum.
	 *
	 * @param staffAttributeEnum the staff attribute enum
	 * @param logInfo the log info
	 * @throws DataAccessException the data access exception
	 */
	void updateStaffAttributeEnum(StaffAttributeEnum staffAttributeEnum,
			LogInfoVO logInfo) throws DataAccessException;

	/**
	 * Creates the staff attribute enum.
	 *
	 * @param staffAttributeEnum the staff attribute enum
	 * @param logInfo the log info
	 * @return the staff attribute enum
	 * @throws DataAccessException the data access exception
	 */
	StaffAttributeEnum createStaffAttributeEnum(
			StaffAttributeEnum staffAttributeEnum, LogInfoVO logInfo)
			throws DataAccessException;

	/**
	 * Delete staff attribute enum.
	 *
	 * @param staffAttribute the staff attribute
	 * @throws DataAccessException the data access exception
	 */
	void deleteStaffAttributeEnum(StaffAttributeEnum staffAttribute)
			throws DataAccessException;

	/**
	 * Gets the staff attribute enum by id.
	 *
	 * @param id the id
	 * @return the staff attribute enum by id
	 * @throws DataAccessException the data access exception
	 */
	StaffAttributeEnum getStaffAttributeEnumById(long id)
			throws DataAccessException;

	/**
	 * Gets the list staff attribute detail by enum id.
	 *
	 * @param id the id
	 * @return the list staff attribute detail by enum id
	 * @throws DataAccessException the data access exception
	 */
	List<StaffAttributeDetail> getListStaffAttributeDetailByEnumId(Long id)
			throws DataAccessException;

	/**
	 * Update staff attribute detail.
	 *
	 * @param staffAttributeDetail the staff attribute detail
	 * @param logInfo the log info
	 * @throws DataAccessException the data access exception
	 */
	void updateStaffAttributeDetail(StaffAttributeDetail staffAttributeDetail,
			LogInfoVO logInfo) throws DataAccessException;

	/****VUONGMQ; 26/02/2014; danh sach thuoc tinh dong */
	List<StaffAttributeVO> getListStaffAttributeVOFilter(StaffAttributeFilter filter) throws DataAccessException;

	List<StaffAttributeEnumVO> getListStaffAttributeEnumVOFilter(StaffAttributeFilter filter) throws DataAccessException;

	List<StaffAttributeDetailVO> getListStaffAttributeDetailVOFilter(StaffAttributeFilter filter) throws DataAccessException;
	/**** END VUONGMQ; 26/02/2014; danh sach thuoc tinh dong */
	
	/****VUONGMQ; 27/02/2014; lay de cap nhat thong tin thuoc tinh dong  */
	/**** chua khai bao o Mgr*/
	StaffAttributeDetail getStaffAttributeDetailByFilter(Long staffAtt,	Long staffId, Long staffAttEnum) throws DataAccessException;

	List<StaffAttributeDetail> getStaffAttributeDetailByFilter(Long staffAtt, Long staffId) throws DataAccessException;
	/**** END VUONGMQ; 27/02/2014; lay de cap nhat thong tin thuoc tinh dong  */



}

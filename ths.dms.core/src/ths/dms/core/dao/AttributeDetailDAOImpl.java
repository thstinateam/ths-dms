package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.AttributeDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeDetailFilter;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class AttributeDetailDAOImpl implements AttributeDetailDAO {
	@Override
	public List<AttributeDetailVO> getListAttributeDetailByAttributeId(KPaging<AttributeDetailVO> kPaging, Long id, ActiveType status) throws DataAccessException{
		StringBuilder sql = new StringBuilder();
//		sql.append("select ad.attribute_detail_id attributeId, ad.attribute_detail_id id, ad.attribute_detail_name name,ad.attribute_detail_code code from attribute_detail ad where ad.attribute_id = ?");
		sql.append(" SELECT ad.customer_attribute_id attributeId, ");
//		sql.append("   ad.CUSTOMER_ATTRIBUTE_DETAIL_ID id, ");
		sql.append("   ae.CUSTOMER_ATTRIBUTE_ENUM_ID enumId, ");
		sql.append("   ae.VALUE name, ");
		sql.append("   ae.CODE code ");
//		sql.append(" FROM CUSTOMER_ATTRIBUTE_DETAIL ad ");
		sql.append(" FROM customer_attribute ad");
		sql.append(" JOIN CUSTOMER_ATTRIBUTE_ENUM ae ON ad.customer_attribute_id = ae.customer_attribute_id");
		sql.append(" WHERE ad.customer_attribute_id   = ? ");
//		StringBuilder coutsql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		if (status != null) {
			sql.append(" and ad.status=?");
			params.add(status.getValue());
		}
		 sql.append(" ORDER BY code ");
		String[] fieldNames = { "attributeId", // 0
//				"id",
				"enumId",
				"code", // 
				"name"
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING // 2
		};
//		coutsql.append(" select count(*) count from ( ");
//		coutsql.append(sql);
//		coutsql.append(" ) ");
//		if (kPaging == null)
//			return repo.getListBySQL(AttributeDetailVO.class, sql.toString(), params);
//		else
//			return repo.getListByQueryAndScalarPaginated(AttributeDetailVO.class, fieldNames, fieldTypes, sql.toString(), coutsql.toString(), params, params, kPaging);
		return repo.getListByQueryAndScalar(AttributeDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	@Autowired
	private IRepository repo;
	
	@Autowired
	CommonDAO commonDAO;

	@Override
	public AttributeDetail createAttributeDetail(AttributeDetail attributeDetail) throws DataAccessException {
		if (attributeDetail == null) {
			throw new IllegalArgumentException("attributeDetail");
		}
		repo.create(attributeDetail);
		return repo.getEntityById(AttributeDetail.class, attributeDetail.getId());
	}
	
	

	@Override
	public void deleteAttributeDetail(AttributeDetail attributeDetail) throws DataAccessException {
		if (attributeDetail == null) {
			throw new IllegalArgumentException("attributeDetail");
		}		
		repo.delete(attributeDetail);
	}

	@Override
	public AttributeDetail getAttributeDetailById(long id) throws DataAccessException {
		return repo.getEntityById(AttributeDetail.class, id);
	}
	
	@Override
	public void updateAttributeDetail(AttributeDetail attributeDetail) throws DataAccessException {
		if (attributeDetail == null) {
			throw new IllegalArgumentException("attributeDetail");
		}
		repo.update(attributeDetail);
	}
	

	
	@Override
	public AttributeDetail getAttributeDetailByCode(Long attributeId, String attributeDetailCode) throws DataAccessException  {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from attribute_detail ad where ATTRIBUTE_ID = ?");
		params.add(attributeId);
		if (!StringUtility.isNullOrEmpty(attributeDetailCode)) {
			sql.append(" and lower(ATTRIBUTE_DETAIL_CODE) = ?");
			params.add(attributeDetailCode.toLowerCase());
		}
		
		return repo.getEntityBySQL(AttributeDetail.class, sql.toString(), params);
	}

	@Override
	public void deleteListAttributeDetailByAttribute(Long attributeId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("delete attribute_detail where attribute_id = ?");
		params.add(attributeId);

		repo.executeSQLQuery(sql.toString(), params);
	}

	
	@Override
	public List<AttributeDetailVO> getListAttributeDetailVO(AttributeDetailFilter filter,
			KPaging<AttributeDetailVO> kPaging) throws DataAccessException {
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		selectSql.append("SELECT ad.CUSTOMER_ATTRIBUTE_ID attributeId,");
		selectSql.append("   ad.CUSTOMER_ATTRIBUTE_DETAIL_ID id,");
		selectSql.append(" ad.customer_attribute_enum_id enumId, ");
//		selectSql.append("   ad.attribute_detail_code code,");
		selectSql.append("   ad.value value,");
		selectSql.append("   ad.status status");
		fromSql.append(" FROM customer_attribute_detail ad, customer_attribute ca  ");
		fromSql.append(" WHERE 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		if (filter.getAttributeId() != null) {
			fromSql.append(" and ad.customer_attribute_id = ? ");
			params.add(filter.getAttributeId());
		}
		if (filter.getCustomerID() != null) {
			fromSql.append(" and ad.customer_id = ? ");
			params.add(filter.getCustomerID());
		}
//		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
//			fromSql.append(" and upper(ad.customer_attribute_detail_code) like ? ESCAPE '/' ");
//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
//		}
//		if (!StringUtility.isNullOrEmpty(filter.getName())) {
//			fromSql.append(" and upper(ad.attribute_detail_name) like ? ESCAPE '/' ");
//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getName().toUpperCase()));
//		}
		if(filter.getStatus() != null){
			fromSql.append(" and ad.status = ? ");
			params.add(filter.getStatus().getValue());
		}
		fromSql.append(" and ca.customer_attribute_id = ad.customer_attribute_id ");
		fromSql.append(" and ca.status = 1 ");
		selectSql.append(fromSql.toString());
		selectSql.append(" order by ad.CUSTOMER_ATTRIBUTE_DETAIL_ID");
		
		countSql.append("select count(ad.CUSTOMER_ATTRIBUTE_DETAIL_ID) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"attributeId", "id", "enumId", "value", "status"
				};

		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER
		};	
		List<AttributeDetailVO> lst =  null;
		if (kPaging == null){
			lst =  repo.getListByQueryAndScalar(
					AttributeDetailVO.class, fieldNames,
					fieldTypes, selectSql.toString(), params);
		}else{
			lst = repo.getListByQueryAndScalarPaginated(AttributeDetailVO.class, fieldNames, fieldTypes,
					selectSql.toString(), countSql.toString(), params, params, kPaging);
		}
		return lst;
	}



	@Override
	public Boolean isExistAttributeDetailCode(Long attributeId,
			String attributeDetailCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) as count from attribute_detail where ATTRIBUTE_ID = ? and attribute_detail_code = ? and status != -1");
		params.add(attributeId);
		params.add(attributeDetailCode.toUpperCase());
		int res = repo.countBySQL(sql.toString(), params);
		return res==1?true:false;
	}
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Media;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.MediaFilter;
import ths.dms.core.exceptions.DataAccessException;

public interface MediaDAO {

	Media createMedia(Media media) throws DataAccessException;

	void deleteMedia(Media media) throws DataAccessException;

	Media getMediaById(long id) throws DataAccessException;

	Media updateMedia(Media media) throws DataAccessException;

	List<Media> getListMedia(MediaFilter filter) throws DataAccessException;

	List<Product> getListProductByMedia(KPaging<Product> kPaging, Long mediaId,
			Integer status) throws DataAccessException;

	Media getMediaByCode(String mediaCode) throws DataAccessException;

}

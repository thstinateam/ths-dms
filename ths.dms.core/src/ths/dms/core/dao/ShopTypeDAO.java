package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.ShopType;
import ths.dms.core.exceptions.DataAccessException;


public interface ShopTypeDAO {
	ShopType getShopTypeByCode(String code) throws DataAccessException;
	List<ShopType> getListShopTypeByType(List<Integer> status) throws DataAccessException;
}
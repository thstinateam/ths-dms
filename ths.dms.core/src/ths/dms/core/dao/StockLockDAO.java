package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.StockLock;
import ths.dms.core.exceptions.DataAccessException;

public interface StockLockDAO {
	
	StockLock getStockLock(Long shopId, Long staffId) throws DataAccessException;

	List<StockLock> getListStockLockByListStaffId(List<Long> lstStaffId, Long shopId) throws DataAccessException;

	StockLock getStockLockByStaffCode(String staffCode, Long shopId) throws DataAccessException;
	
	/**
	 * Tim kiem danh sach kho nhan vien
	 * @author hoanv25
	 * @param filter
	 */
	StockLock getStockLockByFilter(StockLock filter) throws DataAccessException;
}

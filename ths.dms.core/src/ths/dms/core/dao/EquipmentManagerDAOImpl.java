package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.EquipCategory;
import ths.dms.core.entities.EquipDeliveryRecDtl;
import ths.dms.core.entities.EquipDeliveryRecord;
import ths.dms.core.entities.EquipEvictionForm;
import ths.dms.core.entities.EquipEvictionFormDtl;
import ths.dms.core.entities.EquipFormHistory;
import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipGroupProduct;
import ths.dms.core.entities.EquipHistory;
import ths.dms.core.entities.EquipImportRecord;
import ths.dms.core.entities.EquipItem;
import ths.dms.core.entities.EquipItemConfig;
import ths.dms.core.entities.EquipItemConfigDtl;
import ths.dms.core.entities.EquipLend;
import ths.dms.core.entities.EquipLendDetail;
import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.EquipProvider;
import ths.dms.core.entities.EquipRepairForm;
import ths.dms.core.entities.EquipRepairFormDtl;
import ths.dms.core.entities.EquipRepairPayForm;
import ths.dms.core.entities.EquipRepairPayFormDtl;
import ths.dms.core.entities.EquipRole;
import ths.dms.core.entities.EquipRoleUser;
import ths.dms.core.entities.EquipSalePlan;
import ths.dms.core.entities.EquipStatisticCustomer;
import ths.dms.core.entities.EquipStatisticRecDtl;
import ths.dms.core.entities.EquipStock;
import ths.dms.core.entities.EquipStockTotal;
import ths.dms.core.entities.EquipStockTransForm;
import ths.dms.core.entities.EquipStockTransFormDtl;
import ths.dms.core.entities.EquipSuggestEvictionDTL;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamEquipType;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.EquipGFilter;
import ths.dms.core.entities.enumtype.EquipObjectType;
import ths.dms.core.entities.enumtype.EquipPeriodType;
import ths.dms.core.entities.enumtype.EquipStockEquipFilter;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeStatus;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUsageStatus;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.EquipmentRepairFormFilter;
import ths.dms.core.entities.enumtype.IsCheckType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PerFormStatus;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.filter.EquipItemConfigDtlFilter;
import ths.dms.core.entities.filter.EquipItemConfigFilter;
import ths.dms.core.entities.filter.EquipItemFilter;
import ths.dms.core.entities.filter.EquipPermissionFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipRepairFilter;
import ths.dms.core.entities.filter.EquipRoleFilter;
import ths.dms.core.entities.filter.EquipStaffFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.filter.EquipmentEvictionFilter;
import ths.dms.core.entities.filter.EquipmentGroupProductFilter;
import ths.dms.core.entities.filter.EquipmentSalePlaneFilter;
import ths.dms.core.entities.vo.EquipItemConfigDtlVO;
import ths.dms.core.entities.vo.EquipItemConfigVO;
import ths.dms.core.entities.vo.EquipItemVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipRepairFormVO;
import ths.dms.core.entities.vo.EquipRepairFormVOList;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentDeliveryPrintVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentEvictionVO;
import ths.dms.core.entities.vo.EquipmentExVO;
import ths.dms.core.entities.vo.EquipmentGroupProductVO;
import ths.dms.core.entities.vo.EquipmentGroupVO;
import ths.dms.core.entities.vo.EquipmentManagerVO;
import ths.dms.core.entities.vo.EquipmentPermissionVO;
import ths.dms.core.entities.vo.EquipmentRecordDeliveryVO;
import ths.dms.core.entities.vo.EquipmentRepairFormDtlVO;
import ths.dms.core.entities.vo.EquipmentRepairFormVO;
import ths.dms.core.entities.vo.EquipmentStaffVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProductTreeVO;
import ths.dms.core.entities.vo.ShopViewParentVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

/**
 * Tang xu ly tuong tac DB
 * 
 * @author hunglm16
 * @since December 14,2014
 * @description Dung chung cho Quan Ly Thiet Bi
 * */
public class EquipmentManagerDAOImpl implements EquipmentManagerDAO {
	@Autowired
	private IRepository repo;

	@Autowired
	private CommonDAO commonDAO; 

	@Override
	public Equipment createEquipment(Equipment equipment) throws DataAccessException {
		if (equipment == null) {
			throw new IllegalArgumentException("equipment is null");
		}
		if (!StringUtility.isNullOrEmpty(equipment.getCode())) {
			equipment.setCode(equipment.getCode().toUpperCase());
		}
		//Them moi thiet bi
		return repo.create(equipment);
	}

	@Override
	public EquipStockTotal createEquipStockTotal(EquipStockTotal equipStockTotal) throws DataAccessException {
		if (equipStockTotal == null) {
			throw new IllegalArgumentException("equipment is null");
		}
		equipStockTotal.setCreateDate(commonDAO.getSysDate());
		return repo.create(equipStockTotal);
	}

	@Override
	public void updateEquipStockTotal(EquipStockTotal equipStockTotal) throws DataAccessException {
		if (equipStockTotal == null) {
			throw new IllegalArgumentException("EquipStockTotal is null");
		}
		equipStockTotal.setUpdateDate(commonDAO.getSysDate());
		repo.update(equipStockTotal);
	}

	@Override
	public EquipStockTransForm createEquipStockTransForm(EquipStockTransForm equipStockTransForm) throws DataAccessException {
		if (equipStockTransForm == null) {
			throw new IllegalArgumentException("equipStockTransForm is null");
		}
		if (!StringUtility.isNullOrEmpty(equipStockTransForm.getCode())) {
			equipStockTransForm.setCode(equipStockTransForm.getCode().trim().toUpperCase());
		}
//		equipStockTransForm.setCreateDate(commonDAO.getSysDate());
		return repo.create(equipStockTransForm);
	}

	@Override
	public void updateEquipment(Equipment equipment) throws DataAccessException {
		if (equipment == null) {
			throw new IllegalArgumentException("equipment is null");
		}
		if (!StringUtility.isNullOrEmpty(equipment.getCode())) {
			equipment.setCode(equipment.getCode().trim().toUpperCase());
		}
		repo.update(equipment);
	}

	@Override
	public EquipCategory getEquipCategoryById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipCategory.class, id);
	}

	@Override
	public EquipCategory getEquipCategoryByCode(String code,ActiveType status) throws DataAccessException {
		// TODO Auto-generated method stub
		if (code == null) {
			throw new IllegalArgumentException("code is null");
		}
		String sql = "select * from EQUIP_CATEGORY where code =?  ";
		List<Object> params = new ArrayList<Object>();
		params.add(code.trim().toUpperCase());
		if(status != null){
			sql +="and status = ?";
			params.add(status.getValue());
		}else{
			sql +="and status != ?";
			params.add(ActiveType.DELETED.getValue());
		}
		return repo.getEntityBySQL(EquipCategory.class, sql, params);
	}
	
	@Override
	public EquipItem getEquipItemById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipItem.class, id);
	}

	@Override
	public EquipItem getEquipItemByCode(String code) throws DataAccessException {
		if (code == null) {
			throw new IllegalArgumentException("code is null");
		}
		String sql = "select * from equip_item where code = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.trim().toUpperCase());
		EquipItem rs = repo.getEntityBySQL(EquipItem.class, sql, params);
		return rs;
	}
	@Override
	public EquipItem getEquipItemByCode(String code,ActiveType status) throws DataAccessException {
		String sql = "select * from equip_item where 1 = 1 ";
		List<Object> params = new ArrayList<Object>();
		if(code!= null){
			sql += " and code = ? ";
			params.add(code.trim().toUpperCase());
		}
		if(status != null){
			sql +=" and status = ? ";
			params.add(status.getValue());
		}else{ 
			sql +=" and status != ? ";
			params.add(ActiveType.DELETED.getValue());
		}
		EquipItem rs = repo.getEntityBySQL(EquipItem.class, sql, params);
		return rs;
	}
	@Override
	public EquipProvider getEquipProviderById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipProvider.class, id);
	}

	@Override
	public EquipProvider getEquipProviderByCode(String code) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from equip_provider where lower(code) = ? and status != ? ";
		List<Object> params = new ArrayList<Object>();
		params.add(code.trim().toLowerCase());
		params.add(ActiveType.DELETED.getValue());
		EquipProvider rs = repo.getEntityBySQL(EquipProvider.class, sql, params);
		return rs;
	}

	@Override
	public List<EquipmentVO> getListEquipmentByFilterNew(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		if (filter == null || filter.getLstShopId() == null || filter.getLstShopId().isEmpty()) {
			throw new IllegalArgumentException("List Shop Is Null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("	with rshop as (  ");
		sql.append("	  select shop_id, shop_code, shop_name from shop ");
		sql.append("	  start with shop_id in (? ");
		params.add(filter.getLstShopId().get(0));
		for (int i = 1, size = filter.getLstShopId().size(); i < size; i++) {
			sql.append(" ,? ");
			params.add(filter.getLstShopId().get(i));
		}
		sql.append("	  ) and status in (0, 1) connect by prior shop_id = parent_shop_id  ");
		sql.append("	), ");
		sql.append("	rEquip AS ( ");
		sql.append("	  SELECT  ");
		sql.append("	    eq.code, ");
		sql.append("	    apr.value as healthStatus, ");
		sql.append("	    eq.equip_group_id, ");
		sql.append("	    eq.equip_id, ");
		sql.append("	    eq.health_status, ");
		sql.append("	    eq.price, ");
		sql.append("	    eq.serial, ");
		sql.append("	    eq.status, ");
		sql.append("	    eq.stock_id, ");
		sql.append("	    eq.stock_type, ");
		sql.append("	    eq.trade_status, ");
		sql.append("	    eq.trade_type, ");
		sql.append("	    eq.usage_status, ");
		sql.append("	    eq.equip_provider_id, ");
		sql.append("	    eq.warranty_expired_date, ");
		sql.append("	    eq.manufacturing_year, ");
		sql.append("	    eq.equip_import_record_id, ");
		sql.append("	    eq.stock_code, ");
		sql.append("	    eq.stock_name, ");
		sql.append("	    eq.first_date_in_use, ");
		sql.append("	    eq.equip_id       AS equipId , ");
		sql.append("	    eg.code AS equipGroupCode , ");
		sql.append("	    eg.equip_group_id AS equipGroupId , ");
		sql.append("	    eg.name equipGroupName , ");
		sql.append("	    ec.name              AS equipCategoryName, ");
		sql.append("	 ec.EQUIP_CATEGORY_ID, ");
		sql.append("	    eg.brand_name        AS equipBrandName , ");
		sql.append("	    ep.name              AS equipProviderName , ");
		sql.append("	    ep.equip_provider_id AS equipProviderId , ");
		sql.append("	    eg.capacity_to, ");
		sql.append("	    eg.capacity_from, ");
		sql.append("	    cus.short_code    AS shortCode , ");
		sql.append("	    cus.customer_name AS customerName , ");
		sql.append("	    cus.address       AS customerAddress , ");
		sql.append("	    cus.name_text, ");
		sql.append("	    sh.shop_code AS shopCode, ");
		sql.append("	    sh.shop_name AS shopName, ");
		sql.append("	    sh.shop_id   AS shopId, ");
		sql.append("	    ec.code      AS equipCategoryCode ");
		sql.append("	  FROM equipment eq JOIN customer cus ON eq.stock_type = 2 AND eq.stock_id  = cus.customer_id AND cus.status  IN (0, 1) ");
		if (!StringUtility.isNullOrEmpty(filter.getStockName())) {
			sql.append(" and cus.name_text like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getStockName().trim().toUpperCase())));
		}
		
		sql.append("	  JOIN rshop sh ON cus.shop_id = sh.shop_id ");
		sql.append("	  JOIN equip_group eg ON eq.equip_group_id = eg.equip_group_id ");
		sql.append("	  JOIN equip_provider ep ON eq.equip_provider_id = ep.equip_provider_id ");
		sql.append("	  JOIN equip_category ec ON eg.equip_category_id = ec.equip_category_id ");
		sql.append("	  LEFT JOIN ap_param apr on apr.ap_param_code = eq.health_status AND apr.type LIKE 'EQUIP_CONDITION' AND apr.status = 1 ");
		sql.append("	  WHERE 1 = 1 ");
		sql.append("	  AND eg.status           = 1 ");
		sql.append("	  AND ep.status           = 1 ");
		sql.append("	  AND ec.status           = 1 ");
		sql.append("	  AND eq.status           <> -1 ");
		
		sql.append("	  UNION ALL ");
		sql.append("	  SELECT ");
		sql.append("	    eq.code, ");
		sql.append("	    apr.value as healthStatus, ");
		sql.append("	    eq.equip_group_id, ");
		sql.append("	    eq.equip_id, ");
		sql.append("	    eq.health_status, ");
		sql.append("	    eq.price, ");
		sql.append("	    eq.serial, ");
		sql.append("	    eq.status, ");
		sql.append("	    eq.stock_id, ");
		sql.append("	    eq.stock_type, ");
		sql.append("	    eq.trade_status, ");
		sql.append("	    eq.trade_type, ");
		sql.append("	    eq.usage_status, ");
		sql.append("	    eq.equip_provider_id, ");
		sql.append("	    eq.warranty_expired_date, ");
		sql.append("	    eq.manufacturing_year, ");
		sql.append("	    eq.equip_import_record_id, ");
		sql.append("	    eq.stock_code, ");
		sql.append("	    eq.stock_name, ");
		sql.append("	    eq.first_date_in_use, ");
		sql.append("	    eq.equip_id       AS equipId , ");
		sql.append("	    eg.code AS equipGroupCode , ");
		sql.append("	    eg.equip_group_id AS equipGroupId , ");
		sql.append("	    eg.name equipGroupName , ");		
		sql.append("	    ec.name              AS equipCategoryName, ");
		sql.append("	 ec.EQUIP_CATEGORY_ID, ");
		sql.append("	    eg.brand_name        AS equipBrandName , ");
		sql.append("	    ep.name              AS equipProviderName , ");
		sql.append("	    ep.equip_provider_id AS equipProviderId , ");
		sql.append("	    eg.capacity_to, ");
		sql.append("	    eg.capacity_from, ");
		sql.append("	    NULL AS shortCode , ");
		sql.append("	    NULL AS customerName , ");
		sql.append("	    NULL AS customerAddress , ");
		sql.append("	    NULL name_text, ");
		sql.append("	    sh.shop_code AS shopCode, ");
		sql.append("	    sh.shop_name AS shopName, ");
		sql.append("	    sh.shop_id   AS shopId, ");
		sql.append("	    ec.code      AS equipCategoryCode ");
		sql.append("	  FROM equipment eq  ");
//		sql.append("	  JOIN rshop sh ON eq.stock_type = 1 AND eq.stock_id  = sh.shop_id ");
		//tamvnm: sua cau sql join sai
		sql.append("	  JOIN equip_stock es ON eq.stock_type = ? AND eq.stock_id = es.equip_stock_id ");
		params.add(EquipStockTotalType.KHO.getValue());
		
		if (!StringUtility.isNullOrEmpty(filter.getStockName())) {
			sql.append(" and es.name like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockName().trim()));
		}
		
		sql.append("	  JOIN rshop sh ON es.shop_id  = sh.shop_id ");
		
		
		sql.append("	  JOIN equip_group eg ON eq.equip_group_id = eg.equip_group_id ");
		sql.append("	  JOIN equip_provider ep ON eq.equip_provider_id = ep.equip_provider_id ");
		sql.append("	  JOIN equip_category ec ON eg.equip_category_id = ec.equip_category_id ");
		sql.append("	  LEFT JOIN ap_param apr on apr.ap_param_code = eq.health_status AND apr.type LIKE 'EQUIP_CONDITION' AND apr.status = 1 ");
		sql.append("	  WHERE 1 = 1 ");
		sql.append("	  AND eg.status           = 1 ");
		sql.append("	  AND ep.status           = 1 ");
		sql.append("	  AND ec.status           = 1 ");
		sql.append("	  AND eq.status           <> -1 ");
		sql.append("	  ) ");
		sql.append("	select ");
		sql.append("	eq.code AS code ,");
		sql.append("	  eq.equipGroupId , ");
		sql.append("	  eq.equip_id AS equipId , ");
		sql.append("	  (SELECT apr.value ");
		sql.append("	  FROM ap_param apr ");
		sql.append("	  WHERE apr.ap_param_code = eq.health_status ");
		sql.append("	  AND apr.type LIKE 'EQUIP_CONDITION'  ");
		sql.append("	  AND apr.status = 1 ");
		sql.append("	  ) AS healthStatus , ");
		sql.append("	  eq.price  AS price ,  ");
		sql.append("	  eq.serial AS serial , ");
		sql.append("	  eq.status AS status , ");
		sql.append("	  eq.stock_id  AS stockId , ");
		sql.append("	  eq.stock_type AS stockType , ");
		sql.append("	  eq.trade_status AS tradeStatus ,  ");
		sql.append("	  eq.trade_type AS tradeType , ");
		sql.append("	  eq.usage_status AS usageStatus ,  ");
		sql.append("	  eq.equipGroupName , ");
		sql.append("	  eq.equipCategoryName ,");
		sql.append("	  eq.equipBrandName , ");
		sql.append("	  eq.equipProviderName , ");
		sql.append("	  eq.equipProviderId ,  ");
		sql.append(" (CASE ");
		sql.append("  WHEN eq.capacity_from IS NULL ");
		sql.append("  AND ");
		sql.append("  eq.capacity_to IS NULL THEN ");
		sql.append("  '' ");
		sql.append(" WHEN eq.capacity_from IS NULL THEN ");
		sql.append("  '<= ' ||to_char(eq.capacity_to) ");
		sql.append(" WHEN eq.capacity_to IS NULL THEN ");
		sql.append("  '>= ' ||to_char(eq.capacity_from) ");
		sql.append(" WHEN eq.capacity_to <> eq.capacity_from THEN ");
		sql.append("  to_char(eq.capacity_from) || ' - ' || to_char(eq.capacity_to) ");
		sql.append("  ELSE to_char(eq.capacity_from) ");
		sql.append(" END) AS capacitystr ,");
		sql.append("	  decode(eq.usage_status, 0, 'Đã mất', 1, 'Đã thanh lý', 2, 'Đang ở kho', 3, 'Đang sử dụng', 4, 'Đang sửa chữa', '') as usageStatusStr , ");
		sql.append("	  TO_CHAR(eq.warranty_expired_date, 'dd/MM/yyyy') AS warrantyExpiredDateStr ,");
		sql.append("	  eq.manufacturing_year AS manufacturingYear ,  ");
		sql.append("	  eq.equip_import_record_id equipImportRecordId ,  ");
		sql.append("	  eq.stock_code stockCode , ");
		sql.append("	  eq.stock_name stockName , ");
		sql.append("	  eq.equipGroupCode , ");
		sql.append("	  NULL equipImportRecordCode , ");
		sql.append("	  TO_CHAR(eq.first_date_in_use,'dd/mm/yyyy') createDateStr ,  ");
		sql.append("	  eq.shortCode , ");
		sql.append("	  eq.customerName , ");
		sql.append("	  eq.customerAddress , ");
		sql.append("	  (SELECT COUNT(*) dem ");
		sql.append("	  FROM EQUIP_ATTACH_FILE ");
		sql.append("	  WHERE OBJECT_ID = eq.equip_id  ");
		sql.append("	  AND OBJECT_TYPE = 9 ");
		sql.append("	  ) countFile, ");
		sql.append("	  eq.equipCategoryCode AS equipCategoryCode ");
		sql.append("	FROM rEquip eq ");
		sql.append("    join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on eq.shopId = ogr.number_key_id  ");
		params.add(filter.getStaffRootId());
		params.add(filter.getRoleId());
		params.add(filter.getShopRootId());
		
		sql.append(" WHERE 1            = 1 ");
		if (filter.getStatus() != null) {
			sql.append("  and eq.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append("  and eq.status <> -1 ");
		}
		if (filter.getEquipGroupId() != null) {
			sql.append("  and eq.equip_group_id = ? ");
			params.add(filter.getEquipGroupId());
		}
		if (filter.getEquipCategoryId() != null) {
			sql.append("  and eq.equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		if (filter.getEquipProviderId() != null) {
			sql.append("  and eq.equip_provider_id = ? ");
			params.add(filter.getEquipProviderId());
		}
		if (filter.getTradeType() != null) {
			sql.append("  and eq.trade_type = ? ");
			params.add(filter.getTradeType());
		}
		if (filter.getTradeStatus() != null) {
			sql.append("  and eq.trade_status = ? ");
			params.add(filter.getTradeStatus());
		}
		if (filter.getUsageStatus() != null) {
			sql.append("  and eq.usage_status = ? ");
			params.add(filter.getUsageStatus());
		}
		if (filter.getStockType() != null) {
			sql.append("  and eq.stock_type = ? ");
			params.add(filter.getStockType());
		}
		if (filter.getStockId() != null) {
			sql.append("  and eq.stock_id = ? ");
			params.add(filter.getStockId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and eq.code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toUpperCase()));
		}
		if (filter.getFromDate() != null && filter.getToDate() != null) {
			sql.append(" and eq.first_date_in_use is not null ");
			sql.append(" and eq.first_date_in_use >= trunc(?) ");
			params.add(filter.getFromDate());
			sql.append(" and eq.first_date_in_use < trunc(?) + 1 ");
			params.add(filter.getToDate());
		} else if (filter.getFromDate() != null) {
			sql.append(" and (eq.first_date_in_use is null or ");
			sql.append(" eq.first_date_in_use >= trunc(?)) ");
			params.add(filter.getFromDate());
		} else if (filter.getToDate() != null) {
			sql.append(" and (eq.first_date_in_use is null or eq.first_date_in_use < trunc(?) + 1 ) ");
			params.add(filter.getToDate());
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
			sql.append(" and lower(eq.serial) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getHealthStatus())) {
			sql.append(" and lower(eq.health_status) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getHealthStatus().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			sql.append(" and eq.stock_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().trim().toUpperCase()));
		}
//		if (!StringUtility.isNullOrEmpty(filter.getStockName())) {
//			sql.append(" and lower(eq.stock_name) like ? ESCAPE '/'");
//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockName().trim().toLowerCase()));
//		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and eq.shortCode like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().trim().toUpperCase()));
		}
		String namTextCus = "";
		if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
			namTextCus = Unicode2English.codau2khongdau(filter.getCustomerName());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerAddress())) {
			namTextCus = namTextCus.trim() + " " + Unicode2English.codau2khongdau(filter.getCustomerAddress());
		}
		if (namTextCus.length() > 0) {
			sql.append(" and lower(eq.name_text) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(namTextCus.trim().toLowerCase()));
		}
		sql.append("  order by eq.first_date_in_use desc, eq.code ");
		String[] fieldNames = { "equipId", 
				"code", 
				"equipGroupId", 
				"equipProviderId", 
				"healthStatus", 
				"price", 
				"serial", 
				"status", 
				"stockCode", 
				"tradeStatus", 
				"tradeType", 
				"usageStatus", 
				"equipGroupName",				
				"equipCategoryName", 
				"equipBrandName",
				"equipProviderName", 
				"capacityStr", 
				"equipGroupCode", 
				"usageStatusStr", 
				"warrantyExpiredDateStr", 
				"manufacturingYear", 
				"stockType", 
				"equipImportRecordId", 
				"equipImportRecordCode", 
				"createDateStr", 
				"stockName", 
				"stockId", 
				"countFile",
				"shortCode", 
				"customerName", 
				"customerAddress",
				"equipCategoryCode" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER
				, StandardBasicTypes.STRING
				, StandardBasicTypes.STRING
				, StandardBasicTypes.STRING
				, StandardBasicTypes.STRING};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	
	@Override
	public List<EquipmentVO> getListEquipmentByFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select distinct");
		sql.append(" eq.code as code ");
		sql.append(" ,eg.equip_group_id as equipGroupId ");
		sql.append(" ,eq.equip_id as equipId ");
		sql.append(" ,ep.equip_provider_id as equipProviderId ");
		sql.append(" ,apr.value as healthStatus ");
		sql.append(" ,eq.price as price ");
		sql.append(" ,eq.serial as serial ");
		sql.append(" ,eq.status as status ");
		sql.append(" ,eq.stock_id as stockId ");
		sql.append(" ,eq.stock_type as stockType ");
		sql.append(" ,eq.trade_status as tradeStatus ");
		sql.append(" ,eq.trade_type as tradeType ");
		sql.append(" ,eq.usage_status as usageStatus ");
		sql.append(" ,eg.name equipGroupName ");
		sql.append(" ,ec.name as equipCategoryName ");
		sql.append(" ,eg.brand_name as equipBrandName ");
		sql.append(" ,ep.name as equipProviderName ");
		sql.append(" ,(case when eg.capacity_from is null and eg.capacity_to is null then '' when eg.capacity_from is null then '<'||to_char(eg.capacity_to) when eg.capacity_to is null then '>'||to_char(eg.capacity_from)  ");
		sql.append("   when eg.capacity_to <> eg.capacity_from then to_char(eg.capacity_from) || ' - '|| to_char(eg.capacity_to) else to_char(eg.capacity_from) end) as capacityStr  ");
		sql.append(" ,eg.code as equipGroupCode ");
		sql.append(" ,(case when eq.usage_status = 0 then 'Đã mất' when eq.usage_status = 1 then 'Đã thanh lý' when eq.usage_status = 2 then 'Đang ở kho'  ");
		sql.append("    when eq.usage_status = 3 then 'Đang sử dụng' when eq.usage_status = 4 then 'Đang sửa chữa' else '' end) as usageStatusStr ");

		sql.append(" ,to_char(eq.warranty_expired_date, 'dd/MM/yyyy') as warrantyExpiredDateStr ");
		sql.append(" ,eq.manufacturing_year as manufacturingYear ");
		/***liemtpt: begin modified */
		sql.append(" ,eq.equip_import_record_id equipImportRecordId ");
		sql.append(" ,eir.code equipImportRecordCode ");
		sql.append(" ,to_char(eir.create_date,'dd/mm/yyyy') createDateStr ");
		sql.append(" ,eq.stock_code stockCode ");
		sql.append(" ,eq.stock_name stockName ");
		sql.append(" ,es.shop_id shopId ");
		sql.append(" ,s.shop_code shopCode ");
		sql.append(" ,c.customer_name  customerName ");
		sql.append(" ,c.short_code  shortCode ");
		/***liemtpt: end modified */
		sql.append(" FROM equipment eq ");
		sql.append(" join equip_group eg on eq.equip_group_id = eg.equip_group_id ");
		sql.append(" JOIN ap_param apr ON apr.ap_param_code = eq.health_status and apr.type LIKE 'EQUIP_CONDITION' and apr.status = 1 ");
		sql.append(" join equip_provider ep on eq.equip_provider_id = ep.equip_provider_id ");
		sql.append(" join equip_category ec on eg.equip_category_id = ec.equip_category_id ");
		/***liemtpt: begin modified */
		sql.append(" join equip_import_record eir on eir.equip_import_record_id=eq.equip_import_record_id ");
		sql.append(" LEFT JOIN equip_stock es ON ES.equip_stock_id = eq.stock_id ");
		sql.append(" LEFT JOIN CUSTOMER c ON c.customer_id = eq.stock_id and c.status=1 ");
		sql.append(" LEFT JOIN SHOP s on s.shop_id=c.shop_id and s.shop_id=es.shop_id and s.status=1 ");
		/***liemtpt: end modified */
		sql.append("  where 1 = 1 and eq.stock_type in (1,2) ");
		sql.append("  AND eq.status = 1 and eg.status = 1 and ep.status = 1 ");
		if (filter.getStatus() != null) {
			sql.append("  and eq.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append("  and eq.status <> -1 ");
		}
		if (filter.getEquipGroupId() != null) {
			sql.append("  and eq.equip_group_id = ? ");
			params.add(filter.getEquipGroupId());
		}
		if (filter.getEquipCategoryId() != null) {
			sql.append("  and eg.equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		if (filter.getEquipProviderId() != null) {
			sql.append("  and eq.equip_provider_id = ? ");
			params.add(filter.getEquipProviderId());
		}
		if (filter.getTradeType() != null) {
			sql.append("  and eq.trade_type = ? ");
			params.add(filter.getTradeType());
		}
		if (filter.getTradeStatus() != null) {
			sql.append("  and eq.trade_status = ? ");
			params.add(filter.getTradeStatus());
		}
		if (filter.getUsageStatus() != null) {
			sql.append("  and eq.usage_status = ? ");
			params.add(filter.getUsageStatus());
		}
		if (filter.getStockType() != null) {
			sql.append("  and eq.stock_type = ? ");
			params.add(filter.getStockType());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and eq.code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toUpperCase()));
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getEquipImportRecordCode())) {
			sql.append(" and eir.code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipImportRecordCode().trim().toUpperCase()));
		}
		if (filter.getFromDate() != null) {
			sql.append(" and trunc(eir.create_date) >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and trunc(eir.create_date) < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
			sql.append(" and lower(eq.serial) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getHealthStatus())) {
			sql.append(" and lower(eq.health_status) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getHealthStatus().trim().toLowerCase()));
		}
		sql.append("  order by eir.code, eq.code ");
		String[] fieldNames = { "equipId", "code", "equipGroupId", "equipProviderId", "healthStatus", "price", "serial", "status", "stockCode", "tradeStatus", "tradeType", "usageStatus", "equipGroupName", "equipCategoryName", "equipBrandName",
				"equipProviderName", "capacityStr", "equipGroupCode", "usageStatusStr", "warrantyExpiredDateStr", "manufacturingYear", "stockType" 
				,"equipImportRecordId","equipImportRecordCode","createDateStr","stockName","shopCode","customerName","shortCode","stockId","shopId"};
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER
				,StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING
				,StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER 
				,StandardBasicTypes.LONG,StandardBasicTypes.STRING,StandardBasicTypes.STRING, StandardBasicTypes.STRING,StandardBasicTypes.STRING, StandardBasicTypes.STRING,StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.LONG
		};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	/**
	 * Lay danh sach kho theo phan quyen
	 * 
	 * @author hunglm16
	 * @since May 26, 2015
	 * */
	@Override
	public List<EquipStockVO> getListEquipStockVOByFilter(EquipStockFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("with rdta AS ( ");
		sql.append(" SELECT es.equip_stock_id AS equipStockId, ");
		sql.append(" 	        es.code                 AS equipStockCode, ");
		sql.append(" es.name                 AS equipStockName, ");
		sql.append(" es.shop_id              AS shopId, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName, ");
		sql.append(" erd.is_under            AS isUnder ");
		sql.append(" FROM EQUIP_ROLE_USER eru ");
		sql.append(" JOIN EQUIP_ROLE er ON eru.EQUIP_ROLE_ID = er.EQUIP_ROLE_ID ");
		sql.append(" JOIN EQUIP_ROLE_DETAIL erd ON er.EQUIP_ROLE_ID = erd.EQUIP_ROLE_ID ");
		sql.append(" JOIN EQUIP_STOCK es ON erd.EQUIP_STOCK_ID = es.EQUIP_STOCK_ID ");
		sql.append(" JOIN shop sh ON es.shop_id = sh.shop_id ");
		sql.append(" WHERE eru.status      = 1 ");
		sql.append(" AND er.status         = 1 ");
		sql.append(" AND es.status         = 1 ");
		if (filter.getStaffRoot() != null) {
			sql.append(" AND eru.user_id = ? ");
			params.add(filter.getStaffRoot());
		}
		if (filter.getShopRoot() != null) {
			sql.append(" AND es.shop_id       IN ");
			sql.append("  (SELECT shop_id ");
			sql.append("  FROM shop ");
			sql.append("  WHERE status               = 1 ");
			sql.append("   START WITH shop_id       = ? ");
			params.add(filter.getShopRoot());
			sql.append("   AND status                 = 1 ");
			sql.append("      CONNECT BY prior shop_id = parent_shop_id ");
			sql.append("  ) ");
		}
		sql.append(" ), ");
		sql.append(" rStockFull as ( ");
		sql.append(" SELECT DISTINCT equipStockId, ");
		sql.append(" equipStockCode, ");
		sql.append(" equipStockName, ");
		sql.append(" shopCode, ");
		sql.append(" shopName, ");
		sql.append(" shopId ");
		sql.append(" FROM ( ");
		sql.append(" (SELECT es.equip_stock_id AS equipStockId, ");
		sql.append(" es.code                 AS equipStockCode, ");
		sql.append(" es.name                 AS equipStockName, ");
		sql.append(" es.shop_id              AS shopId, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName, ");
		sql.append(" 1            AS isUnder ");
		sql.append(" FROM (SELECT * FROM SHOP  ");
		sql.append(" WHERE 1                  = 1  ");
		sql.append(" START WITH shop_id IN  ");
		sql.append("  (SELECT shopId FROM rdta WHERE isUnder = 1) and status = 1  ");
		sql.append("  CONNECT BY prior shop_id = parent_shop_id) sh  ");
		sql.append(" JOIN EQUIP_STOCK es ON sh.shop_id = es.shop_id  ");
		sql.append("  WHERE es.status            = 1  ");
		if (filter.getStaffRoot() != null) {
			sql.append(" MINUS ( ");
			sql.append(" SELECT es.equip_stock_id AS equipStockId, ");
			sql.append(" es.code                 AS equipStockCode, ");
			sql.append(" es.name                 AS equipStockName, ");
			sql.append(" es.shop_id              AS shopId, ");
			sql.append(" sh.shop_code as shopCode, ");
			sql.append(" sh.shop_name as shopName, ");
			sql.append(" 1            AS isUnder ");
			sql.append(" FROM SHOP sh ");
			sql.append(" JOIN EQUIP_STOCK es ON sh.shop_id = es.shop_id  ");
			sql.append(" Where 1 = 1 ");
			sql.append(" AND sh.shop_id IN (SELECT shopId FROM rdta WHERE isUnder = 1) ");
			sql.append(" AND es.equip_stock_id NOT IN (SELECT equipStockId FROM rdta) ");
			sql.append(" ) ");
		}
		sql.append(" ) ");
		sql.append(" UNION ");
		sql.append(" (SELECT * FROM rdta WHERE isUnder = 0 ");
		sql.append(" ) ) ORDER BY equipStockCode ");
		sql.append(" ) ");
			    
		sql.append(" select equipStockId,  equipStockCode as code,  equipStockName as name,  shopCode, shopName, ");
		sql.append("   (shopCode ||' - '|| shopName) codeNameShop, shopId  ");
		sql.append(" from rStockFull where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and lower(shopCode) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and lower(shopName) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopName().trim().toLowerCase()));
		}
		sql.append(" order by shopCode, code ");

		String[] fieldNames = { "code", "name", "shopId", "shopCode", "shopName", "equipStockId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipStockVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipStockVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public List<EquipStockVO> getListEquipStockVOByFilterForStockTrans(EquipStockFilter filter) throws DataAccessException {
		//Su dung chung voi cac chuc nang giao nhan hop dong
		return getListEquipStockVOByRole(filter);
//		StringBuilder sql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		sql.append("WITH  ");
//		sql.append(" tbequipstocktmp AS ");
//		sql.append(" (SELECT * from equip_stock where status = 1 ");
//		sql.append(" )");
//		sql.append("  SELECT distinct tbtmp.code code ");
//		sql.append("  ,tbtmp.name name ");
//		sql.append(" ,sh.shop_id shopId ");
//		sql.append(" ,sh.shop_code shopCode ");
//		sql.append(" ,sh.shop_name shopName ");
//		sql.append(" ,tbtmp.equip_stock_id equipStockId ");
//		sql.append(" FROM shop sh ");
//		sql.append(" JOIN tbequipstocktmp tbtmp ON tbtmp.shop_id = sh.shop_id ");
//		sql.append(" WHERE 1         = 1 ");
//		sql.append(" AND sh.status   = 1 ");
//		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
//			sql.append(" and sh.shop_code like ? ESCAPE '/'");
//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toUpperCase()));
//		}
//		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
//			sql.append(" and lower(sh.shop_name) like ? ESCAPE '/'");
//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopName().trim().toLowerCase()));
//		}
//		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
//			sql.append(" and tbtmp.code like ? ESCAPE '/'");
//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toUpperCase()));
//		}
//		sql.append(" order by sh.shop_code, tbtmp.code ");
//		String[] fieldNames = { "code", "name", "shopId", "shopCode", "shopName", "equipStockId" };
//		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG };
//		StringBuilder countSql = new StringBuilder();
//		countSql.append(" select count(1) as count from ( ");
//		countSql.append(sql);
//		countSql.append(" ) ");
//		if (filter.getkPaging() != null) {
//			return repo.getListByQueryAndScalarPaginated(EquipStockVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
//		} else {
//			return repo.getListByQueryAndScalar(EquipStockVO.class, fieldNames, fieldTypes, sql.toString(), params);
//		}
	}
	
	@Override
	public List<EquipmentVO> getListEquipmentGeneral(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (filter.getType() != null && filter.getType() != -1) {
			if (filter.getType() == 1) {
				sql.append("select distinct ecy.equip_category_id as id,  ecy.code as code, ecy.name as name, ");
				sql.append(" '1' as type,null as typeHMSC,null as warranty, ");
				sql.append(" ecy.status as status ");
				sql.append(" from equip_category ecy ");
				sql.append("where 1=1 ");
				if (filter.getStatus() != null && filter.getStatus() != -1) {
					sql.append("and status = ? ");
					params.add(filter.getStatus());
				}
				if (!StringUtility.isNullOrEmpty(filter.getCode())) {
					sql.append(" and lower(code) like ? ESCAPE '/'");
					params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
				}
				if (!StringUtility.isNullOrEmpty(filter.getName())) {
					sql.append(" and lower(name) like ? ESCAPE '/'");
					params.add(StringUtility.toOracleSearchLikeSuffix(filter.getName().trim().toLowerCase()));
				}
			} else if (filter.getType() == 2) {
				sql.append(" select epd.equip_provider_id as id, epd.code as code, epd.name as name, ");
				sql.append(" '2' as type,null as typeHMSC,null as warranty, ");
				sql.append(" epd.status as status ");
				sql.append(" from equip_provider epd ");
				sql.append("where 1=1 ");
				if (filter.getStatus() != null && filter.getStatus() != -1) {
					sql.append("and status = ? ");
					params.add(filter.getStatus());
				}
				if (!StringUtility.isNullOrEmpty(filter.getCode())) {
					sql.append(" and lower(code) like ? ESCAPE '/'");
					params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
				}
				if (!StringUtility.isNullOrEmpty(filter.getName())) {
					sql.append(" and lower(name) like ? ESCAPE '/'");
					params.add(StringUtility.toOracleSearchLikeSuffix(filter.getName().trim().toLowerCase()));
				}
			} else if (filter.getType() == 4) {
				sql.append(" select eim.equip_item_id as id, eim.code as code, eim.name as name, ");
				sql.append(" '4' as type,eim.type as typeHMSC,eim.warranty as warranty, ");
				sql.append(" eim.status as status ");
				sql.append(" from equip_item eim ");
				sql.append("where 1=1 ");
				if (filter.getStatus() != null && filter.getStatus() != -1) {
					sql.append("and status = ? ");
					params.add(filter.getStatus());
				}
				if (!StringUtility.isNullOrEmpty(filter.getCode())) {
					sql.append(" and lower(code) like ? ESCAPE '/'");
					params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
				}
				if (!StringUtility.isNullOrEmpty(filter.getName())) {
					sql.append(" and lower(name) like ? ESCAPE '/'");
					params.add(StringUtility.toOracleSearchLikeSuffix(filter.getName().trim().toLowerCase()));
				}
			}
		} else {
			sql.append("select distinct ecy.equip_category_id as id,  ecy.code as code, ecy.name as name, ");
			sql.append(" '1' as type,null as typeHMSC,null as warranty, ");
			sql.append(" ecy.status as status ");
			sql.append(" from equip_category ecy ");
			sql.append("where 1=1 ");
			if (filter.getStatus() != null && filter.getStatus() != -1) {
				sql.append("and status = ? ");
				params.add(filter.getStatus());
			}
			if (!StringUtility.isNullOrEmpty(filter.getCode())) {
				sql.append(" and lower(code) like ? ESCAPE '/'");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getName())) {
				sql.append(" and lower(name) like ? ESCAPE '/'");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getName().trim().toLowerCase()));
			}
			sql.append(" union ");
			sql.append(" select distinct epd.equip_provider_id as id, epd.code as code, epd.name as name, ");
			sql.append(" '2' as type,null as typeHMSC,null as warranty, ");
			sql.append(" epd.status as status ");
			sql.append(" from equip_provider epd ");
			sql.append("where 1=1 ");
			if (filter.getStatus() != null && filter.getStatus() != -1) {
				sql.append(" and status = ? ");
				params.add(filter.getStatus());
			}
			if (!StringUtility.isNullOrEmpty(filter.getCode())) {
				sql.append(" and lower(code) like ? ESCAPE '/'");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getName())) {
				sql.append(" and lower(name) like ? ESCAPE '/'");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getName().trim().toLowerCase()));
			}
			sql.append(" union ");
			sql.append(" select distinct eim.equip_item_id as id, eim.code as code, eim.name as name, ");
			sql.append(" '4' as type,eim.type as typeHMSC,eim.warranty as warranty, ");
			sql.append(" eim.status as status ");
			sql.append(" from equip_item eim ");
			sql.append("where 1=1 ");
			if (filter.getStatus() != null && filter.getStatus() != -1) {
				sql.append(" and status = ? ");
				params.add(filter.getStatus());
			}
			if (!StringUtility.isNullOrEmpty(filter.getCode())) {
				sql.append(" and lower(code) like ? ESCAPE '/'");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getName())) {
				sql.append(" and lower(name) like ? ESCAPE '/'");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getName().trim().toLowerCase()));
			}
		}
		/*
		 * sql.append(" where 1=1 "); if(filter.getStatus() !=null &&
		 * filter.getStatus() != -1){ sql.append(" and status = ? ");
		 * params.add(filter.getStatus()); }
		 */
		sql.append(" ORDER BY code , name DESC ");
		String[] fieldNames = { "id", "code", "name", "type","typeHMSC","warranty", "status" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<EquipPeriod> getListEquipPeriodByFilter(EquipmentFilter<EquipPeriod> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select distinct * from equip_period where 1 = 1");

		if (filter.getYearManufacture() != null) {
			sql.append(" and  extract( year from from_date) = ? ");
			params.add(filter.getYearManufacture());
		}
		if (filter.getFromDate() != null) {
			sql.append(" and to_date >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and from_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if (filter.getStatus() != null) {
			sql.append(" and status = ? ");
			params.add(filter.getStatus());
		}
		if (filter.getIdExcepted()!=null) {
			sql.append(" and EQUIP_PERIOD_ID not in (?) ");
			params.add(filter.getIdExcepted());
		}
		if (filter.getId()!=null) {
			sql.append(" and equip_period_id = ? ");
			params.add(filter.getId());
		}
		if (Boolean.TRUE.equals(filter.getFirstRow())) {
			sql.append(" order by equip_period_id desc ");
		} else {
			sql.append(" order by code desc ");
		}
		if (Boolean.TRUE.equals(filter.getFirstRow())) {
			StringBuilder rownum = new StringBuilder();
			rownum.append(" select * from ( ");
			rownum.append(sql.toString());
			rownum.append(" ) frm where rownum = 1");

		}
	
		
		if (filter.getkPaging() != null) {
			return repo.getListBySQLPaginated(EquipPeriod.class, sql.toString(), params, filter.getkPaging());
		}
		return repo.getListBySQL(EquipPeriod.class, sql.toString(), params);

	}
	
	@Override
	public List<EquipPeriod> getListEquipPeriodClosedByFromDate(EquipmentFilter<EquipPeriod> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select distinct * from equip_period where 1 = 1");

		if (filter.getFromDate() != null) {
			sql.append(" and from_date > trunc(?) ");
			params.add(filter.getFromDate());
		}
		sql.append(" and status = ? ");
		params.add(EquipPeriodType.CLOSED.getValue());
		return repo.getListBySQL(EquipPeriod.class, sql.toString(), params);

	}

	@Override
	public EquipStockTotal getFirtEquipStockTotalByFilter(EquipStockEquipFilter<EquipStockTotal> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select distinct * ");
		sql.append(" FROM equip_stock_total ");
		sql.append(" WHERE 1            = 1 ");
		if (filter.getEquipGroupId() != null) {
			sql.append(" AND equip_group_id = ? ");
			params.add(filter.getEquipGroupId());
		}
		if (filter.getStockId() != null) {
			sql.append(" AND stock_id       = ? ");
			params.add(filter.getStockId());
		}
		if (filter.getStockType() != null) {
			sql.append(" AND stock_type     = ?  ");
			params.add(filter.getStockType());
		}
		sql.append(" order by create_date desc ");
		return repo.getFirstBySQL(EquipStockTotal.class, sql.toString(), params);

	}

	@Override
	public EquipProvider createEquipProviderManagerCatalog(EquipProvider equipprovider, LogInfoVO logInfoVO) throws DataAccessException {
		if (equipprovider == null) {
			throw new IllegalArgumentException("equipprovider");
		}
		if (logInfoVO == null) {
			throw new IllegalArgumentException("logInfoVO is null");
		}
		if (equipprovider.getCode() != null) {
			equipprovider.setCode(equipprovider.getCode().toUpperCase());
		}
		equipprovider.setCreateDate(commonDAO.getSysDate());
		repo.create(equipprovider);
		return equipprovider;
	}

	@Override
	public EquipItem createEquipItemManagerCatalog(EquipItem equipItem, LogInfoVO logInfoVO) throws DataAccessException {
		if (equipItem == null) {
			throw new IllegalArgumentException("equipItem");
		}
		if (logInfoVO == null) {
			throw new IllegalArgumentException("logInfoVO is null");
		}
		if (equipItem.getCode() != null) {
			equipItem.setCode(equipItem.getCode().toUpperCase());
		}
		equipItem.setCreateDate(commonDAO.getSysDate());
		repo.create(equipItem);
		return equipItem;
	}

	@Override
	public EquipCategory updateEquipCategoryManagerCatalog(EquipCategory equipCategory, LogInfoVO logInfoVO) throws DataAccessException {
		if (equipCategory == null) {
			throw new IllegalArgumentException("equipCategory");
		}
		if (logInfoVO == null) {
			throw new IllegalArgumentException("logInfoVO is null");
		}
		if (equipCategory.getCode() != null) {
			equipCategory.setCode(equipCategory.getCode().toUpperCase());
		}
		equipCategory.setUpdateDate(commonDAO.getSysDate());
		repo.update(equipCategory);
		return equipCategory;
	}

	@Override
	public EquipProvider updateEquipProviderManagerCatalog(EquipProvider equipProvider, LogInfoVO logInfoVO) throws DataAccessException {
		if (equipProvider == null) {
			throw new IllegalArgumentException("equipProvider");
		}
		if (logInfoVO == null) {
			throw new IllegalArgumentException("logInfoVO is null");
		}
		if (equipProvider.getCode() != null) {
			equipProvider.setCode(equipProvider.getCode().toUpperCase());
		}
		equipProvider.setUpdateDate(commonDAO.getSysDate());
		repo.update(equipProvider);
		return equipProvider;
	}

	@Override
	public EquipItem updateEquipItemManagerCatalog(EquipItem equipItem, LogInfoVO logInfoVO) throws DataAccessException {
		if (equipItem == null) {
			throw new IllegalArgumentException("equipItem");
		}
		if (logInfoVO == null) {
			throw new IllegalArgumentException("logInfoVO is null");
		}
		if (equipItem.getCode() != null) {
			equipItem.setCode(equipItem.getCode().toUpperCase());
		}
		equipItem.setUpdateDate(commonDAO.getSysDate());
		repo.update(equipItem);
		return equipItem;
	}

	// phuongvm
	@Override
	public EquipEvictionForm createEquipEvictionForm(EquipEvictionForm equipEvictionForm) throws DataAccessException {
		if (equipEvictionForm == null) {
			throw new IllegalArgumentException("equipEvictionForm is null");
		}
		if (!StringUtility.isNullOrEmpty(equipEvictionForm.getCode())) {
			equipEvictionForm.setCode(equipEvictionForm.getCode().trim().toUpperCase());
		}
		return repo.create(equipEvictionForm);
	}

	// phuongvm
	@Override
	public EquipEvictionFormDtl createEquipEvictionFormDtl(EquipEvictionFormDtl equipEvictionFormDtl) throws DataAccessException {
		if (equipEvictionFormDtl == null) {
			throw new IllegalArgumentException("equipEvictionFormDtl is null");
		}
		return repo.create(equipEvictionFormDtl);
	}

	@Override
	public EquipEvictionForm getEquipEvictionFormById(long id) throws DataAccessException {
		return repo.getEntityById(EquipEvictionForm.class, id);
	}

	@Override
	public List<EquipEvictionFormDtl> getEquipEvictionFormDtlByEquipEvictionFormId(Long equipEvictionFormId) throws DataAccessException {
		if (equipEvictionFormId == null) {
			throw new IllegalArgumentException("equipEvictionFormId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_eviction_form_dtl where equip_eviction_form_id = ? ");
		params.add(equipEvictionFormId);
		return repo.getListBySQL(EquipEvictionFormDtl.class, sql.toString(), params);
	}

	// phuongvm
	@Override
	public List<EquipmentExVO> getListEquipmentDeliveryByCustomerId(Long customerId) throws DataAccessException {
		if (customerId == null) {
			throw new IllegalArgumentException("customerId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		//		sql.append("select distinct drd.equip_id as id,dr.contract_number as soHopDong, ");
		//		sql.append("   ec.name as loaiThietBi,eg.name as nhomThietBi,e.code as maThietBi,e.serial as soSeri,1 as soLuong, ");
		//		sql.append("   ap.ap_param_name as tinhTrangThietBi ");
		//		sql.append("   from  equip_delivery_record dr ");
		//		sql.append("   join equip_delivery_rec_dtl drd on drd.equip_delivery_record_id = dr.equip_delivery_record_id ");
		//		sql.append("   join equipment e on drd.equip_id = e.equip_id");
		//		sql.append("   join equip_group eg on eg.equip_group_id = e.equip_group_id");
		//		sql.append("   join equip_category ec on ec.equip_category_id = eg.equip_category_id ");
		//		sql.append("   join ap_param ap on ap.ap_param_code = e.health_status ");
		//		sql.append(" where 1=1 and dr.customer_id = ? and dr.record_status = 2 and e.usage_status = 3 and e.trade_status = 0");
		//		params.add(customerId);
		//		sql.append(" and dr.create_date = (select max(create_date) from equip_delivery_record where customer_id = ? and record_status = 2)");
		//		params.add(customerId);
		//		sql.append(" order by e.code");

		sql.append("select distinct e.equip_id as id, ");
		sql.append("   redr.contract_number soHopDong, ");
		sql.append("   ec.name as loaiThietBi,(eg.code||' - '||eg.name) as nhomThietBi,e.code as maThietBi,e.serial as soSeri,1 as soLuong, ");
		sql.append("   ap.value as tinhTrangThietBi");
		sql.append("   from equipment e  ");
		sql.append("   join equip_group eg on eg.equip_group_id = e.equip_group_id");
		sql.append("   join equip_category ec on ec.equip_category_id = eg.equip_category_id  ");
		sql.append("   join ap_param ap on ap.ap_param_code = e.health_status  ");
		sql.append("   left join ( ");
		sql.append("   select * from (  ");
		sql.append("    select dtl.equip_id ,dtl.equip_delivery_record_id ,hr.contract_number, hr.record_status ");
		sql.append("    ,row_number() over (partition by dtl.equip_id order by hr.create_date desc) as rn ");
		sql.append("    from equip_delivery_record hr join equip_delivery_rec_dtl dtl on hr.equip_delivery_record_id = dtl.equip_delivery_record_id ");
		sql.append("    where 1 = 1 ");
		sql.append("    and hr.record_status = ?");
		params.add(StatusRecordsEquip.APPROVED.getValue());
		sql.append("    ) dta");
		sql.append("    where dta.rn = 1 order by dta.equip_id ");
		sql.append("   ) redr on e.equip_id = redr.equip_id  ");
		sql.append("    where 1=1  and e.usage_status = ? and e.trade_status = ? ");
		params.add(EquipUsageStatus.IS_USED.getValue());
		params.add(EquipTradeStatus.NOT_TRADE.getValue());
		sql.append("    and stock_type = ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append("    and stock_id = ? ");
		params.add(customerId);
		sql.append(" order by e.code");

		String[] fieldNames = new String[] { "id", "soHopDong", "loaiThietBi", "nhomThietBi", "maThietBi", "soSeri", "soLuong", "tinhTrangThietBi" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(EquipmentExVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	// phuongvm
	@Override
	public List<EquipmentExVO> getListAllEquipmentDeliveryByCustomerId(Long customerId, List<Long> lstEquipId) throws DataAccessException {
		if (customerId == null) {
			throw new IllegalArgumentException("customerId is null");
		}
		//		StringBuilder sql = new StringBuilder();
		//		List<Object> params = new ArrayList<Object>();
		//		sql.append(" select drd.equip_id as id,dr.contract_number as soHopDong, ");
		//		sql.append("   ec.name as loaiThietBi,eg.name as nhomThietBi,e.code as maThietBi,e.serial as soSeri,1 as soLuong, ");
		//		sql.append("   ap.ap_param_name as tinhTrangThietBi ");
		//		sql.append("   from  equip_delivery_record dr ");
		//		sql.append("   join equip_delivery_rec_dtl drd on drd.equip_delivery_record_id = dr.equip_delivery_record_id ");
		//		sql.append("   join equipment e on drd.equip_id = e.equip_id");
		//		sql.append("   join equip_group eg on eg.equip_group_id = e.equip_group_id");
		//		sql.append("   join equip_category ec on ec.equip_category_id = eg.equip_category_id ");
		//		sql.append("   join ap_param ap on ap.ap_param_code = e.health_status ");
		//		sql.append(" where 1=1 and dr.customer_id = ? and dr.record_status = 2 and e.usage_status = 3 and e.trade_status = 0");
		//		params.add(customerId);
		//		sql.append(" and dr.create_date = (select max(create_date) from equip_delivery_record where customer_id = ? and record_status = 2)");
		//		params.add(customerId);
		//		sql.append(" union ");
		//		sql.append(" select drd.equip_id as id,dr.contract_number as soHopDong, ");
		//		sql.append("   ec.name as loaiThietBi,eg.name as nhomThietBi,e.code as maThietBi,e.serial as soSeri,1 as soLuong, ");
		//		sql.append("   ap.ap_param_name as tinhTrangThietBi ");
		//		sql.append("   from  equip_delivery_record dr ");
		//		sql.append("   join equip_delivery_rec_dtl drd on drd.equip_delivery_record_id = dr.equip_delivery_record_id ");
		//		sql.append("   join equipment e on drd.equip_id = e.equip_id");
		//		sql.append("   join equip_group eg on eg.equip_group_id = e.equip_group_id");
		//		sql.append("   join equip_category ec on ec.equip_category_id = eg.equip_category_id ");
		//		sql.append("   join ap_param ap on ap.ap_param_code = e.health_status ");
		//		sql.append(" where 1=1 and dr.customer_id = ? and dr.record_status = 2 and e.usage_status = 3");
		//		params.add(customerId);
		//		sql.append(" and dr.create_date = (select max(create_date) from equip_delivery_record where customer_id = ? and record_status = 2)");
		//		params.add(customerId);
		//		if (lstEquipId != null && lstEquipId.size() > 0) {
		//			sql.append(" and e.equip_id in (");
		//			for (Long id : lstEquipId) {
		//				if (id != null) {
		//					sql.append("?,");
		//					params.add(id);
		//				}
		//			}
		//			sql.append("?)");
		//			params.add(-1L);
		//		}
		//		sql.append(" order by maThietBi ");

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select distinct e.equip_id as id, ");
		sql.append("   redr.contract_number soHopDong, ");
		sql.append("   ec.name as loaiThietBi,(eg.code||' - '||eg.name) as nhomThietBi,e.code as maThietBi,e.serial as soSeri,1 as soLuong, ");
		sql.append("   ap.value as tinhTrangThietBi, e.MANUFACTURING_YEAR as manufacturingYear, redr.equip_delivery_record_id as equipDeliveryId ");
		sql.append("   from equipment e  ");
		sql.append("   join equip_group eg on eg.equip_group_id = e.equip_group_id");
		sql.append("   join equip_category ec on ec.equip_category_id = eg.equip_category_id  ");
		sql.append("   join ap_param ap on ap.ap_param_code = e.health_status  ");
		sql.append("   left join ( ");
		sql.append("   select * from (  ");
		sql.append("    select dtl.equip_id ,dtl.equip_delivery_record_id ,hr.contract_number, hr.record_status ");
		sql.append("    ,row_number() over (partition by dtl.equip_id order by hr.create_date desc) as rn ");
		sql.append("    from equip_delivery_record hr join equip_delivery_rec_dtl dtl on hr.equip_delivery_record_id = dtl.equip_delivery_record_id ");
		sql.append("    where 1 = 1 ");
		sql.append("    and hr.record_status = ?");
		params.add(StatusRecordsEquip.APPROVED.getValue());
		sql.append("    ) dta");
		sql.append("    where dta.rn = 1 order by dta.equip_id ");
		sql.append("   ) redr on e.equip_id = redr.equip_id  ");
		sql.append("    where 1=1  and e.usage_status = ? and e.trade_status = ? ");
		params.add(EquipUsageStatus.IS_USED.getValue());
		params.add(EquipTradeStatus.NOT_TRADE.getValue());
		sql.append("    and stock_type = ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append("    and stock_id = ? ");
		params.add(customerId);
		sql.append(" union ");
		sql.append("select distinct e.equip_id as id, ");
		sql.append("   redr.contract_number soHopDong, ");
		sql.append("   ec.name as loaiThietBi,(eg.code||' - '||eg.name) as nhomThietBi,e.code as maThietBi,e.serial as soSeri,1 as soLuong, ");
		sql.append("   ap.value as tinhTrangThietBi, e.MANUFACTURING_YEAR as manufacturingYear,  redr.equip_delivery_record_id AS equipDeliveryId  ");
		sql.append("   from equipment e  ");
		sql.append("   join equip_group eg on eg.equip_group_id = e.equip_group_id");
		sql.append("   join equip_category ec on ec.equip_category_id = eg.equip_category_id  ");
		sql.append("   join ap_param ap on ap.ap_param_code = e.health_status  ");
		sql.append("   left join ( ");
		sql.append("   select * from (  ");
		sql.append("    select dtl.equip_id ,dtl.equip_delivery_record_id ,hr.contract_number, hr.record_status ");
		sql.append("    ,row_number() over (partition by dtl.equip_id order by hr.create_date desc) as rn ");
		sql.append("    from equip_delivery_record hr join equip_delivery_rec_dtl dtl on hr.equip_delivery_record_id = dtl.equip_delivery_record_id ");
		sql.append("    where 1 = 1 ");
		sql.append("    and hr.record_status = ?");
		params.add(StatusRecordsEquip.APPROVED.getValue());
		sql.append("    ) dta");
		sql.append("    where dta.rn = 1 order by dta.equip_id ");
		sql.append("   ) redr on e.equip_id = redr.equip_id  ");
		sql.append("    where 1=1  and e.usage_status = ?  ");
		params.add(EquipUsageStatus.IS_USED.getValue());
		sql.append("    and stock_type = ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append("    and stock_id = ? ");
		params.add(customerId);
		if (lstEquipId != null && lstEquipId.size() > 0) {
			sql.append(" and e.equip_id in (");
			for (Long id : lstEquipId) {
				if (id != null) {
					sql.append("?,");
					params.add(id);
				}
			}
			sql.append("?)");
			params.add(-1L);
		}
		sql.append(" order by maThietBi ");

		String[] fieldNames = new String[] { "id", "soHopDong", "loaiThietBi", "nhomThietBi", "maThietBi", "soSeri", "soLuong", "tinhTrangThietBi", "manufacturingYear", "equipDeliveryId" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG };
		return repo.getListByQueryAndScalar(EquipmentExVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public void updateEquipEvictionForm(EquipEvictionForm equipEvictionForm) throws DataAccessException {
		if (equipEvictionForm == null) {
			throw new IllegalArgumentException("equipEvictionForm is null");
		}
		repo.update(equipEvictionForm);
	}

	@Override
	public List<EquipmentEvictionVO> getListEquipmentEviction(EquipmentEvictionFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT DISTINCT eef.equip_eviction_form_id             AS id, ");
//		sql.append("                To_char(eef.create_date, 'dd/mm/yyyy') AS ngay, ");
		//tamvnm: thay doi theo ngay lap
		sql.append("                To_char(eef.create_form_date, 'dd/mm/yyyy') AS ngay, ");
		sql.append("                eef.code                               maPhieu, ");
		sql.append("                s.shop_code                            maNPP, ");
		sql.append("                s.shop_name                            tenNPP, ");
		sql.append("                c.short_code                           maKH, ");
		sql.append("                c.customer_name                        AS tenKH, ");
		sql.append("                c.address                              AS diaChi, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN eef.form_status = 4 THEN 'Hủy' ");
		sql.append("                    WHEN eef.form_status = 2 THEN 'Đã duyệt' ");
		sql.append("                    ELSE 'Dự thảo' ");
		sql.append("                  END )                                trangThaiBienBan, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN eef.action_status = 2 THEN 'Đã nhận' ");
		sql.append("                    WHEN eef.action_status = 1 THEN 'Đã gửi' ");
		sql.append("                    ELSE 'Chưa gửi' ");
		sql.append("                  END )                                trangThaiGiaoNhan, ");
		sql.append("                Ese.code                               AS ");
		sql.append("                equipSugEvictionCode, ");
		sql.append("                eef.note, ");
		sql.append("                (select count(*) from Equip_Attach_File where object_type = 5 and object_id = eef.equip_eviction_form_id) as numberFile ");
		sql.append("FROM   equip_eviction_form eef ");
		sql.append("       LEFT JOIN equip_eviction_form_dtl eefd ");
		sql.append("         ON eefd.equip_eviction_form_id = eef.equip_eviction_form_id ");
		sql.append("       LEFT JOIN equipment e ");
		sql.append("         ON eefd.equip_id = e.equip_id ");
		sql.append("       JOIN customer c ");
		sql.append("         ON c.customer_id = eef.customer_id ");
		sql.append("       JOIN shop s ");
		sql.append("         ON s.shop_id = eef.shop_id ");
		sql.append("       LEFT JOIN equip_suggest_eviction ese ");
		sql.append("              ON Ese.equip_suggest_eviction_id = Eef.equip_suggest_eviction_id ");
		sql.append("WHERE  1 = 1");
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and (upper(c.short_code) like ? ESCAPE '/' or c.name_text like ? ESCAPE '/')");
			String customerCode = Unicode2English.codau2khongdau(filter.getCustomerCode().trim().toUpperCase());
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
		}
		if (!StringUtility.isNullOrEmpty(filter.getEuqipCode())) {
			sql.append(" and upper(e.code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEuqipCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeri())) {
			sql.append(" and upper(e.serial) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeri().toUpperCase()));
		}
		if (filter.getStatusAction() != null) {
			sql.append(" and eef.action_status = ?");
			params.add(filter.getStatusAction());
		}
		if (filter.getStatusForm() != null) {
			sql.append(" and eef.form_status = ?");
			params.add(filter.getStatusForm());
		}
//		if (filter.getFromDate() != null) {
//			sql.append(" and eef.create_date >= TRUNC(?) ");
//			params.add(filter.getFromDate());
//		}
//		if (filter.getToDate() != null) {
//			sql.append(" and eef.create_date < TRUNC(?)+1 ");
//			params.add(filter.getToDate());
//		}
		//tamvnm:13/07/2015 - thay doi tim kiem theo ngay lap
		if (filter.getFromDate() != null) {
			sql.append(" and eef.create_form_date >= TRUNC(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and eef.create_form_date < TRUNC(?)+1 ");
			params.add(filter.getToDate());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getEquipSugEvictionCode())) {
			sql.append(" AND ese.code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipSugEvictionCode().toUpperCase()));
		}
		
		if (filter.getLstShopId() != null && filter.getLstShopId().size() > 0) {
			sql.append(" and s.shop_id in (");
			sql.append(" select shop_id from shop start with shop_id in (");
			for (Long id : filter.getLstShopId()) {
				if (id != null) {
					sql.append("?,");
					params.add(id);
				}
			}
			sql.append("?) connect by prior shop_id = parent_shop_id)");
			params.add(-1L);
		}

//		sql.append(" order by eef.code desc ");
		sql.append(" order by ngay desc, eef.code desc ");
		
		String[] fieldNames = new String[] { 
				"id", 							// 1
				"ngay", 						// 2
				"maPhieu", 						// 3
				"maNPP",						// 4
				"tenNPP", 						// 5
				"maKH", 						// 6
				"tenKH", 						// 7
				"diaChi", 						// 8
				"trangThaiBienBan",				// 9
				"trangThaiGiaoNhan",			// 10
				"equipSugEvictionCode",			// 11
				"note",
				"numberFile"
				};
		Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,		// 1
				StandardBasicTypes.STRING, 		// 2
				StandardBasicTypes.STRING,		// 3
				StandardBasicTypes.STRING,		// 4
				StandardBasicTypes.STRING, 		// 5
				StandardBasicTypes.STRING,		// 6
				StandardBasicTypes.STRING, 		// 7
				StandardBasicTypes.STRING,		// 8
				StandardBasicTypes.STRING, 		// 9
				StandardBasicTypes.STRING,		// 10
				StandardBasicTypes.STRING,		// 11
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER
				};
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipmentEvictionVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(EquipmentEvictionVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	// phuongvm

	@Override
	public List<EquipmentEvictionVO> getHistory(Long idRecord, EquipTradeType recordType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ");
		sql.append(" to_char(act_date,'dd/mm/yyyy') as ngay, ");
	sql.append(" (case when delivery_status = 2 then 'Đã nhận' when delivery_status = 1 then 'Đã gửi' else 'Chưa gửi' end ) trangThaiGiaoNhan ");
		sql.append(" from ( ");
		sql.append(" 	select  ");
		sql.append(" 	record_id, ");
		sql.append(" 	act_date, ");
		sql.append(" 	delivery_status, ");
		sql.append(" 	row_number() over (partition by record_id, delivery_status order by act_date) stt ");
		sql.append(" 	from equip_form_history ");
		sql.append(" 	where 1=1 ");
		if (recordType != null) {
			sql.append(" and record_type  = ?");
			params.add(recordType.getValue());
		}
		if (idRecord != null && idRecord > 0) {
			sql.append(" and record_id  = ?");
			params.add(idRecord);
		}
		sql.append(" ) ");
		sql.append(" where stt = 1 ");
		sql.append(" order by act_date desc ");

		/*
		 * sql.append(" select  to_char(act_date,'dd/mm/yyyy') as ngay, ");
		 * sql.append(
		 * "   (case when delivery_status = 2 then '?��???�ƒ?�£ nh?�¡?�º?�­n' when delivery_status = 1 then '?��???�ƒ?�£ g?�¡?�»?�­i' else 'Ch?�†?�°a g?�¡?�»?�­i' end ) trangThaiGiaoNhan "
		 * ); sql.append("  from equip_form_history ");
		 * sql.append(" where 1=1"); if (idRecord != null && idRecord > 0) {
		 * sql.append(" and record_id  = ?"); params.add(idRecord); } if
		 * (recordType != null) { sql.append(" and record_type  = ?");
		 * params.add(recordType.getValue()); }
		 * sql.append(" order by act_date desc ");
		 */

		String[] fieldNames = new String[] { "ngay", "trangThaiGiaoNhan" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(EquipmentEvictionVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipRecordVO> getHistoryEquipRecordByFilter(EquipRecordFilter<EquipRecordVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select  to_char(act_date,'dd/mm/yyyy') as actDateStr, ");
		sql.append("  (case when delivery_status = 0 then 'Chưa gửi' when delivery_status = 1 then 'Đã gửi'  when delivery_status = 2 then 'Đã nhận'  else '' end ) deliveryStatusStr, ");
		sql.append("  delivery_status as deliveryStatus ");
		sql.append("  from equip_form_history ");
		sql.append(" where 1 = 1");
		if (filter.getEquipId() != null) {
			sql.append(" and record_id  = ?");
			params.add(filter.getEquipId());
		}
		if (filter.getRecordType() != null) {
			sql.append(" and record_type  = ?");
			params.add(filter.getRecordType());
		}
		sql.append(" order by act_date desc, deliveryStatusStr asc ");

		String[] fieldNames = new String[] { "actDateStr", "deliveryStatusStr", "deliveryStatus" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };
		if (filter.getkPaging() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" Select count(*) as count from ( ").append(sql).append(" ) ");
			return repo.getListByQueryAndScalarPaginated(EquipRecordVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
		return repo.getListByQueryAndScalar(EquipRecordVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public EquipCategory createEquipCategoryManagerCatalog(EquipCategory equipcategory, LogInfoVO logInfoVO) throws DataAccessException {
		if (equipcategory == null) {
			throw new IllegalArgumentException("equipcategory");
		}
		if (logInfoVO == null) {
			throw new IllegalArgumentException("logInfoVO is null");
		}
		if (equipcategory.getCode() != null) {
			equipcategory.setCode(equipcategory.getCode().toUpperCase());
		}
		equipcategory.setCreateDate(commonDAO.getSysDate());
		repo.create(equipcategory);
		return equipcategory;
	}

	@Override
	public List<EquipmentDeliveryVO> getListEquipmentDeliveryVOByFilter(EquipmentFilter<EquipmentDeliveryVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder whereSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select DISTINCT eq.equip_id as equipmentId, ");
		sql.append(" eq.code as equipmentCode, ");
		sql.append(" eq.serial as seriNumber, ");
		//sql.append(" (case when eq.health_status is not null then (select min(value) from ap_param ap where ap.status = 1 and ap.ap_param_code like eq.health_status and type like 'EQUIP_CONDITION')else '' end) as healthStatus, ");
		sql.append(" (SELECT ap.value ");
		sql.append(" FROM   ap_param ap ");
		sql.append(" WHERE  ap.status = 1 ");
		sql.append(" AND    ap.ap_param_code LIKE eq.health_status ");
		sql.append(" AND    type LIKE 'EQUIP_CONDITION' ) as healthstatus,");
		sql.append(" (case ");
		sql.append(" when eq.stock_type = 2 then (select cus.customer_code from customer cus where cus.customer_id = eq.stock_id and cus.status = 1) ");
		sql.append(" else (select es.code from equip_stock es where es.equip_stock_id = eq.stock_id and es.status = 1) end)as stockCode, ");
		sql.append(" (case ");
		sql.append(" when eq.stock_type = 2 then (select cus.customer_name from customer cus where cus.customer_id = eq.stock_id and cus.status = 1) ");
		sql.append(" else (select es.name from equip_stock es where es.equip_stock_id = eq.stock_id and es.status = 1) end)as stockName, ");
		sql.append(" ec.name as typeEquipment, ");
		sql.append(" eg.code as groupEquipmentCode, ");
		sql.append(" eg.name as groupEquipmentName, ");
		sql.append(" ( CASE ");
		sql.append(" WHEN eg.capacity_from IS NULL ");
		sql.append(" AND eg.capacity_to IS NULL ");
		sql.append(" THEN '' ");
		sql.append(" WHEN eg.capacity_from IS NULL ");
		sql.append(" THEN '<=' ");
		sql.append(" ||TO_CHAR(eg.capacity_to) ");
		sql.append(" WHEN eg.capacity_to IS NULL ");
		sql.append(" THEN '>=' ");
		sql.append(" ||TO_CHAR(eg.capacity_from) ");
		sql.append(" WHEN eg.capacity_to <> eg.capacity_from ");
		sql.append(" THEN TO_CHAR(eg.capacity_from) ");
		sql.append(" || ' - ' ");
		sql.append(" || TO_CHAR(eg.capacity_to) ");
		sql.append(" ELSE TO_CHAR(eg.capacity_from) ");
		sql.append(" END) AS capacity, ");
		sql.append(" eg.brand_name as equipmentBrand, ");
		sql.append(" ep.name as equipmentProvider, ");
		sql.append(" ep.code as equipmentProviderCode, ");
		sql.append(" eq.manufacturing_year as yearManufacture, ");
		sql.append(" (select to_char(eq.FIRST_DATE_IN_USE, 'yyyy') from dual) as firstYearInUse, ");
		sql.append(" eq.PRICE as price, ");
		if (filter.getIdRecordDelivery() != null) {
			sql.append(" edrdt.content AS contentDelivery, ");
		} else {
			sql.append(" null AS contentDelivery, ");
		}
		
		if (filter.getIdRecordDelivery() != null) {
			sql.append(" edrdt.depreciation as depreciation ");
		} else {
			sql.append(" null as depreciation ");
		}
		sql.append(" from equipment eq ");
		sql.append(" join equip_group eg on eg.equip_group_id = eq.equip_group_id and eg.status = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getEquipLendCode())) {
			sql.append(" JOIN equip_lend_detail eld ");
			sql.append(" ON eld.EQUIP_CATEGORY_ID = eg.EQUIP_CATEGORY_ID ");
			sql.append(" JOIN equip_lend el ");
			sql.append(" ON eld.equip_lend_id = el.equip_lend_id AND el.code = ? ");
			sql.append(" AND el.status = 1 AND eld.status = 1 ");
			params.add(filter.getEquipLendCode());
		}
		
		sql.append(" join equip_category ec on eg.equip_category_id = ec.equip_category_id  and ec.status = 1 ");
		//sql.append(" join equip_brand eb on eb.equip_brand_id = eg.equip_brand_id ");
		sql.append(" join equip_provider ep on ep.equip_provider_id = eq.equip_provider_id and ep.status = 1 ");
		if (filter.getIdRecordDelivery() != null) {
			sql.append(" join equip_delivery_rec_dtl edrdt on edrdt.equip_id = eq.equip_id ");
		}
		sql.append(" where 1=1 ");
		if (filter.getLstEquipAdd() != null && filter.getLstEquipAdd().size() > 0) {
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
				code += ",?";
			}
			sql.append(" and eq.code not in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
				params.add(filter.getLstEquipAdd().get(i));
			}
		}
		
		if (filter.getLstEquipmentCode() != null && filter.getLstEquipmentCode().size() > 0) {
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipmentCode().size(); i++) {
				code += ",?";
			}
			sql.append(" and eq.code not in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipmentCode().size(); i++) {
				params.add(filter.getLstEquipmentCode().get(i));
			}
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			whereSql.append(" and upper(eq.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
			whereSql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
		}
		if (filter.getEquipCategoryId() != null) {
			whereSql.append(" and ec.equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		if (filter.getEquipGroupId() != null) {
			whereSql.append(" and eg.equip_group_id = ? ");
			params.add(filter.getEquipGroupId());
		}
		if (filter.getEquipProviderId() != null) {
			whereSql.append(" and ep.equip_provider_id = ? ");
			params.add(filter.getEquipProviderId());
		}
		if (filter.getYearManufacture() != null) {
			whereSql.append(" and eq.manufacturing_year = ? ");
			params.add(filter.getYearManufacture());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			whereSql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
		}
		if (filter.getStockType() != null) {
			whereSql.append(" and eq.stock_type = ? ");
			params.add(filter.getStockType());
		} else if (filter.getIdRecordDelivery() == null) {
			whereSql.append(" and eq.stock_type != ? ");
			params.add(EquipStockTotalType.KHO_KH.getValue());
		}
		if (filter.getStatusEquip() != null) {
			whereSql.append(" and eq.status = ? ");
			params.add(filter.getStatusEquip());
		} else {
			whereSql.append(" and eq.status != ? ");
			params.add(StatusType.DELETED.getValue());
		}
		if (filter.getTradeStatus() != null) {
			whereSql.append(" and eq.trade_status = ?");
			params.add(filter.getTradeStatus());
		}
		if (filter.getUsageStatus() != null) {
			whereSql.append(" and eq.usage_status = ?");
			params.add(filter.getUsageStatus());
		}
		if (filter.getIdRecordDelivery() != null) {
			whereSql.append(" and edrdt.equip_delivery_rec_dtl_id in ");
			whereSql.append(" (select edrd.equip_delivery_rec_dtl_id from equip_delivery_rec_dtl edrd ");
			whereSql.append(" join equip_delivery_record edr on edr.equip_delivery_record_id = edrd.equip_delivery_record_id ");
			whereSql.append(" where 1=1 ");
			whereSql.append(" and edr.equip_delivery_record_id = ?) ");
			params.add(filter.getIdRecordDelivery());
		}
		
		
		
		String sqlTemp = sql.toString();
		sql.append(whereSql);
		// them cac thiet bi xoa tam tren danh sach
		if (filter.getLstEquipDelete() != null && filter.getLstEquipDelete().size() > 0) {
			sql.append(" union ");
			sql.append(sqlTemp);
			if (filter.getLstEquipAdd() != null && filter.getLstEquipAdd().size() > 0) {
				for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
					params.add(filter.getLstEquipAdd().get(i));
				}
			}
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipDelete().size(); i++) {
				code += ",?";
			}
			sql.append(" and eq.code in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipDelete().size(); i++) {
				params.add(filter.getLstEquipDelete().get(i));
			}
			if (!StringUtility.isNullOrEmpty(filter.getCode())) {
				sql.append(" and upper(eq.code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
				sql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
			}
			if (filter.getEquipCategoryId() != null) {
				sql.append(" and ec.equip_category_id = ? ");
				params.add(filter.getEquipCategoryId());
			}
			if (filter.getEquipGroupId() != null) {
				sql.append(" and eg.equip_group_id = ? ");
				params.add(filter.getEquipGroupId());
			}
			if (filter.getEquipProviderId() != null) {
				sql.append(" and ep.equip_provider_id = ? ");
				params.add(filter.getEquipProviderId());
			}
			if (filter.getYearManufacture() != null) {
				sql.append(" and eq.manufacturing_year = ? ");
				params.add(filter.getYearManufacture());
			}
			if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
				sql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
			}
			if (filter.getStatusEquip() != null) {
				sql.append(" and eq.status = ? ");
				params.add(filter.getStatusEquip());
			} else {
				sql.append(" and eq.status != ? ");
				params.add(StatusType.DELETED.getValue());
			}
			if (filter.getUsageStatus() != null) {
				sql.append(" and eq.usage_status = ?");
				params.add(filter.getUsageStatus());
			}
		}
		sql.append(" ORDER BY equipmentCode ASC  ");

		String[] fieldNames = { "equipmentId",// 0
				"equipmentCode",// 1
				"seriNumber",// 2
				"healthStatus",// 3
				"stockCode",// 4
				"stockName",// 5
				"typeEquipment",// 6
				"groupEquipmentCode",// 7
				"groupEquipmentName",// 8
				"capacity",// 9
				"equipmentBrand",// 10
				"equipmentProvider",// 11
				"equipmentProviderCode",
				"yearManufacture",// 12
				"contentDelivery",// 13
				"firstYearInUse",	//14
				"price",				//15
				"depreciation"
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.STRING,// 11
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.STRING, // 13
				StandardBasicTypes.STRING,	//14
				StandardBasicTypes.BIG_DECIMAL, //15
				StandardBasicTypes.INTEGER
		};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	/**
	 * Tim kiem danh sach thiet bi bien ban sua chua, 
	 * lay theo phan quyen; popup thiet bi
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	@Override
	public List<EquipmentDeliveryVO> getListEquipmentPopUp(EquipmentFilter<EquipmentDeliveryVO> filter) throws DataAccessException {
		if (filter.getStaffRoot() == null || filter.getShopRoot() == null) {
			throw new IllegalArgumentException("staffRoot or shopRoot not id");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder whereSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH t_kho_under AS ");
		sql.append(" (SELECT es.equip_stock_id AS id, ");
		sql.append(" es.shop_id as shopId, ");
		sql.append(" es.code AS code, ");
		sql.append(" es.name AS name ");
		sql.append(" FROM equip_role_user eru ");
		sql.append(" JOIN equip_role_detail erd ON eru.equip_role_id = erd.equip_role_id ");
		sql.append(" JOIN equip_stock es ON es.equip_stock_id = erd.equip_stock_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND es.status = 1 ");
		sql.append(" AND erd.is_under = 1 "); /** co quan ly don vi cap duoi: 1*/
		sql.append(" AND eru.status = 1 ");
		sql.append(" AND eru.user_id = ? ");
		params.add(filter.getStaffRoot());
		sql.append(" ), ");
		sql.append(" stock_under as ( ");
		sql.append(" select es.equip_stock_id as id, ");
		sql.append(" es.shop_id as shopId, ");
		sql.append(" es.code as code, ");
		sql.append(" es.name as name ");
		sql.append(" from equip_stock es ");
		sql.append(" where 1=1 ");
		sql.append(" AND es.status = 1 ");
		sql.append(" and es.shop_id in ( ");
		sql.append(" select sh.shop_id ");
		sql.append(" FROM shop sh ");
		sql.append(" WHERE sh.status = 1 ");
		sql.append(" and sh.shop_id not in (SELECT shopId FROM t_kho_under) ");    // vuongmq; 25/05/2015; tru all kho cua shop t_kho_under
		sql.append(" START WITH sh.shop_id IN (SELECT shopId FROM t_kho_under) ");
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sql.append("   ) ");
		sql.append(" union ");
		sql.append(" (select * from t_kho_under) ");
		sql.append(" ), ");
		sql.append(" t_kho_nounder as ");
		sql.append(" (SELECT es.equip_stock_id as id, ");
		sql.append(" es.shop_id as shopId, ");
		sql.append(" es.code as code, ");
		sql.append(" es.name as name ");
		sql.append(" FROM equip_role_user eru ");
		sql.append(" JOIN equip_role_detail erd ON eru.equip_role_id = erd.equip_role_id ");
		sql.append(" JOIN equip_stock es ON es.equip_stock_id = erd.equip_stock_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND es.status = 1 ");
		sql.append(" AND erd.is_under = 0 "); /** khong quan ly don vi cap duoi: 0*/
		sql.append(" AND eru.status = 1 ");
		sql.append(" AND eru.user_id = ? ");
		params.add(filter.getStaffRoot());
		sql.append(" ), ");
		sql.append(" lstStockShopTmp AS ");
		sql.append(" ( SELECT * FROM stock_under ");
		sql.append(" UNION ");
		sql.append(" SELECT * FROM t_kho_nounder ");
		sql.append(" ), ");
		sql.append(" lstStockShop AS ");
		sql.append(" ( SELECT * FROM lstStockShopTmp t where t.shopId in ( ");
	    sql.append(" select sh.shop_id from shop sh ");
		sql.append(" where sh.status = 1 ");
		sql.append(" START WITH sh.shop_id IN (?) ");
		params.add(filter.getShopRoot());
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sql.append(" ) ");
		sql.append(" ), ");
		/**end danh sach kho Don vi ; dung kho DV la: lstStockShop */
		sql.append(" lstshopCUS as ( ");
		sql.append(" select shopId from ( ");
		sql.append(" select shop_id as shopId from shop sh ");
		sql.append(" START WITH sh.shop_id IN (SELECT shopId FROM t_kho_under) ");
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sql.append(" UNION ");
		sql.append(" select shopId from t_kho_nounder ) ");
		sql.append(" where shopId in ( ");
		sql.append(" select sh.shop_id from shop sh ");
		sql.append(" where 1=1 ");
		sql.append(" and sh.status = 1 ");
		sql.append(" START WITH sh.shop_id IN (?) ");
		params.add(filter.getShopRoot());
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id) ");
		sql.append(" ), ");
		sql.append(" t_kho_customer as ");
		sql.append(" ( select c.customer_id as id, ");
		sql.append(" sh.shop_code||c.short_code as code, ");
		sql.append(" c.customer_name as name ");
		sql.append(" from customer c ");
		sql.append(" join shop sh on sh.shop_id = c.shop_id ");
		sql.append(" where c.status = 1 ");
		sql.append(" and c.shop_id in (select tc.shopId from lstshopCUS tc) ");
		sql.append(" ) ");
		/**end danh sach kho Customer ; dung kho customer la: t_kho_customer; lay all customer cua kho Qly, lay theo ShopId */
		/**** select VO ***/
		sql.append(" SELECT DISTINCT eq.equip_id AS equipmentId, ");
		sql.append(" eq.code AS equipmentCode, ");
		sql.append(" eq.serial AS seriNumber, ");
		sql.append(" ( ");
		sql.append(" CASE ");
		sql.append(" WHEN eq.health_status IS NOT NULL ");
		sql.append(" THEN ");
		sql.append(" (SELECT to_char(MIN(value)) ");
		sql.append(" FROM ap_param ap ");
		sql.append(" WHERE ap.status = 1 ");
		sql.append(" AND ap.ap_param_code LIKE eq.health_status ");
		sql.append(" AND type LIKE 'EQUIP_CONDITION' ");
		sql.append(" ) ");
		sql.append(" ELSE '' ");
		sql.append(" END) AS healthStatus, ");
		sql.append(" eq.stock_code AS stockCode, ");
		/* sql.append(" ( CASE ");
		sql.append(" WHEN eq.stock_type = 1 ");
		sql.append(" THEN (SELECT ts.name from ");
		sql.append(" lstStockShop ts where ts.id = eq.stock_id ");
		sql.append(" ) ELSE ");
		sql.append(" (SELECT ts.name from ");
		sql.append(" t_kho_customer ts where ts.id = eq.stock_id ");
		sql.append(" ) ");
		sql.append(" END) AS stockName, "); */
		
		sql.append(" eq.stock_name AS stockName, ");
		
		/*** end select stockName */
		//sql.append(" eq.stock_type as khotype, ");
		sql.append(" ec.name AS typeEquipment, ");
		sql.append(" eg.code AS groupEquipmentCode, ");
		sql.append(" eg.name AS groupEquipmentName, ");
		sql.append(" ( ");
		sql.append(" CASE ");
		sql.append(" WHEN eg.capacity_from IS NULL ");
		sql.append(" AND eg.capacity_to IS NULL ");
		sql.append(" THEN '' ");
		sql.append(" WHEN eg.capacity_from IS NULL ");
		sql.append(" THEN '<=' ");
		sql.append(" ||TO_CHAR(eg.capacity_to) ");
		sql.append(" WHEN eg.capacity_to IS NULL ");
		sql.append(" THEN '>=' ");
		sql.append(" ||TO_CHAR(eg.capacity_from) ");
		sql.append(" WHEN eg.capacity_to <> eg.capacity_from ");
		sql.append(" THEN TO_CHAR(eg.capacity_from) ");
		sql.append(" || ' - ' ");
		sql.append(" || TO_CHAR(eg.capacity_to) ");
		sql.append(" ELSE TO_CHAR(eg.capacity_from) ");
		sql.append(" END) AS capacity, ");
		sql.append(" eg.brand_name AS equipmentBrand, ");
		sql.append(" ep.name AS equipmentProvider, ");
		sql.append(" eq.manufacturing_year AS yearManufacture, ");
		sql.append(" TO_CHAR(eq.warranty_Expired_Date,'dd/mm/yyyy') AS warrantyExpiredDate, ");
		/** vuongmq; 10/06/2015; lay so lan sua chua thiet bi cho chinh xac; Defect 227832; */
		//sql.append(" (SELECT COUNT(1)+1 ");
		sql.append(" (SELECT nvl(max(erf.repair_count),0)+1  ");
		sql.append(" FROM equip_repair_form erf ");
		sql.append(" WHERE erf.equip_id = eq.equip_id ");
		sql.append(" AND erf.status = ? ");
		params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
		sql.append(" ) repairCount, ");
		if (filter.getIdRecordDelivery() != null) {
			sql.append(" edrdt.content AS contentDelivery ");
		} else {
			sql.append(" null AS contentDelivery ");
		}
		/*** from cac table*/
		sql.append(" FROM (  ");
		
		sql.append(" select eq.equip_id, eq.code, eq.serial, eq.health_status, eq.stock_code, eq.stock_id ");
		sql.append(" , eq.manufacturing_year, eq.warranty_Expired_Date, eq.trade_type, eq.status, eq.trade_status ");
		sql.append(" , eq.usage_status, eq.equip_group_id, eq.equip_provider_id, t.name as stock_name ");
		sql.append(" from equipment eq ");
		sql.append(" join lstStockShop t on t.id = eq.stock_id and eq.stock_type = 1 ");
		sql.append(" union all ");
		sql.append(" select eq.equip_id, eq.code, eq.serial, eq.health_status, eq.stock_code, eq.stock_id ");
		sql.append(" , eq.manufacturing_year, eq.warranty_Expired_Date, eq.trade_type, eq.status, eq.trade_status ");
		sql.append(" , eq.usage_status, eq.equip_group_id, eq.equip_provider_id, tmp.name as stock_name ");
		sql.append(" from equipment eq ");
		sql.append(" join t_kho_customer tmp on tmp.id = eq.stock_id and eq.stock_type = 2 ");
		
		sql.append(" ) eq ");
		sql.append(" JOIN equip_group eg ON eg.equip_group_id = eq.equip_group_id ");
		sql.append(" JOIN equip_category ec ON eg.equip_category_id = ec.equip_category_id ");
		sql.append(" join equip_provider ep on ep.equip_provider_id = eq.equip_provider_id ");
		if (filter.getIdRecordDelivery() != null) {
			sql.append(" join equip_delivery_rec_dtl edrdt on edrdt.equip_id = eq.equip_id ");
		}
		/** begin cac dieu kien where macdinh */
		sql.append(" WHERE 1 =1 ");
		sql.append(" AND ec.status = 1 ");
		sql.append(" AND eg.status = 1 ");
		sql.append(" AND ep.status = 1 ");
		sql.append(" AND eq.trade_type IS NULL ");
		 
		/* sql.append(" and ( (eq.stock_id IN (select t.id from lstStockShop t) and eq.stock_type = ?) ");
		params.add(EquipStockTotalType.KHO.getValue()); 
		sql.append(" or (eq.stock_id IN (select tmp.id from t_kho_customer tmp) and eq.stock_type = ?) ");
		params.add(EquipStockTotalType.KHO_KH.getValue()); 
		sql.append(" ) "); */
		
		/** end cac dieu kien where macdinh */
		if (filter.getLstEquipAdd() != null) {
		    String code = "'-1'";
		for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
			code += ",?";
		}
		sql.append(" and eq.code not in ( " + code + " ) ");
	       for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
	              params.add(filter.getLstEquipAdd().get(i));
	       }
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
	       whereSql.append(" and eq.code like ? ESCAPE '/' ");
	       params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
	        whereSql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
	        params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCategoryCode())) {
	        whereSql.append(" and upper(ec.code) like ? ESCAPE '/' ");
	        params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCategoryCode().toUpperCase()));
		}
		if (filter.getEquipCategoryId() != null) {
	        whereSql.append(" and ec.equip_category_id = ? ");
	        params.add(filter.getEquipCategoryId());
		} 
		if (!StringUtility.isNullOrEmpty(filter.getGroupCode())) {
	        whereSql.append(" and upper(eg.code) like ? ESCAPE '/' ");
	        params.add(StringUtility.toOracleSearchLikeSuffix(filter.getGroupCode().toUpperCase()));
		}
		if (filter.getEquipGroupId() != null) {
	        whereSql.append(" and eg.equip_group_id = ? ");
	        params.add(filter.getEquipGroupId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getProviderCode())) {
	        whereSql.append(" and upper(ep.code) like ? ESCAPE '/' ");
	        params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProviderCode().toUpperCase()));
		}
		if (filter.getEquipProviderId() != null) {
	        whereSql.append(" and ep.equip_provider_id = ? ");
	        params.add(filter.getEquipProviderId());
		}
		if (filter.getYearManufacture() != null) {
	        whereSql.append(" and eq.manufacturing_year = ? ");
	        params.add(filter.getYearManufacture());
		} 
		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
	        whereSql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
	        params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
		}
		if (filter.getStockType() != null) {
	        whereSql.append(" and eq.stock_type = ? ");
	        params.add(filter.getStockType());
		} else if (filter.getIdRecordDelivery() == null) {
		    /*whereSql.append(" and eq.stock_type != ? ");
			params.add(EquipStockTotalType.KHO_KH.getValue());*/
		}
		if (filter.getStatusEquip() != null) {
	        whereSql.append(" and eq.status = ? ");
	        params.add(filter.getStatusEquip());
		} else {
	        whereSql.append(" and eq.status != ? ");
	        params.add(StatusType.DELETED.getValue());
		}
		if (filter.getTradeStatus() != null) {
	        whereSql.append(" and eq.trade_status = ?");
	        params.add(filter.getTradeStatus());
		}
		//if (filter.getUsageStatus() != null) {
		whereSql.append(" and eq.usage_status in (?,?)");
		params.add(EquipUsageStatus.IS_USED.getValue());
		params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
		//}
		if (filter.getIdRecordDelivery() != null) {
	        whereSql.append(" and edrdt.equip_delivery_rec_dtl_id in ");
			whereSql.append(" (select edrd.equip_delivery_rec_dtl_id from equip_delivery_rec_dtl edrd ");
			whereSql.append(" join equip_delivery_record edr on edr.equip_delivery_record_id = edrd.equip_delivery_record_id ");
			whereSql.append(" where 1=1 ");
			whereSql.append(" and edr.equip_delivery_record_id = ?) ");
	        params.add(filter.getIdRecordDelivery());
		}
		String sqlTemp = sql.toString();
		sql.append(whereSql);
		//////////**** them cac thiet bi xoa tam tren danh sach ********//////
		if (filter.getLstEquipDelete() != null) {
	        sql.append(" union ");
	        sql.append(sqlTemp);
			if (filter.getLstEquipAdd() != null) {
			      for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
			             params.add(filter.getLstEquipAdd().get(i));
			      }
			}
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipDelete().size(); i++) {
			    code += ",?";
			}
			sql.append(" and eq.code in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipDelete().size(); i++) {
			    params.add(filter.getLstEquipDelete().get(i));
			}
			if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			    sql.append(" and upper(eq.code) like ? ESCAPE '/' ");
			    params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
			    sql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
			    params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getCategoryCode())) {
			    sql.append(" and upper(ec.code) like ? ESCAPE '/' ");
			    params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCategoryCode().toUpperCase()));
			}
			if (filter.getEquipCategoryId() != null) {
			    sql.append(" and ec.equip_category_id = ? ");
			    params.add(filter.getEquipCategoryId());
			}
			if (!StringUtility.isNullOrEmpty(filter.getGroupCode())) {
			    sql.append(" and upper(eg.code) like ? ESCAPE '/' ");
			    params.add(StringUtility.toOracleSearchLikeSuffix(filter.getGroupCode().toUpperCase()));
			}
			if (filter.getEquipGroupId() != null) {
			     sql.append(" and eg.equip_group_id = ? ");
			     params.add(filter.getEquipGroupId());
			}
			if (!StringUtility.isNullOrEmpty(filter.getProviderCode())) {
			    sql.append(" and upper(ep.code) like ? ESCAPE '/' ");
			    params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProviderCode().toUpperCase()));
			}
			if (filter.getEquipProviderId() != null) {
			    sql.append(" and ep.equip_provider_id = ? ");
			    params.add(filter.getEquipProviderId());
			}
			if (filter.getYearManufacture() != null) {
			    sql.append(" and eq.manufacturing_year = ? ");
			    params.add(filter.getYearManufacture());
			}
			if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			    sql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
			    params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
			}
			if (filter.getStatusEquip() != null) {
			    sql.append(" and eq.status = ? ");
			    params.add(filter.getStatusEquip());
			} else {
			    sql.append(" and eq.status != ? ");
			    params.add(StatusType.DELETED.getValue());
			}
		}
		/**vuongmq; 16/04/2015; lay danh sach thiet bi het han; 01/07/2015; lay luon het han bao hanh*/
		//sql.append(" and eq.warranty_Expired_Date < sysdate ");
		sql.append(" ORDER BY equipmentCode ASC  ");
		String[] fieldNames = { "equipmentId",// 0
			  "equipmentCode",// 1
			  "seriNumber",// 2
			  "healthStatus",// 3
			  "stockCode",// 4
			  "stockName",// 5
			  "typeEquipment",// 6
			  "groupEquipmentCode",// 7
			  "groupEquipmentName",// 8
			  "capacity",// 9
			  "equipmentBrand",// 10
			  "equipmentProvider",// 11
			  "yearManufacture",// 12
			  "warrantyExpiredDate", // 12.1 /** 06/04/2015; vuongmq; ngay het han bh*/
			  "repairCount",// 13
			  "contentDelivery",// 14
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
			  StandardBasicTypes.STRING,// 1
			  StandardBasicTypes.STRING,// 2
			  StandardBasicTypes.STRING,// 3
			  StandardBasicTypes.STRING,// 4
			  StandardBasicTypes.STRING,// 5
			  StandardBasicTypes.STRING,// 6
			  StandardBasicTypes.STRING,// 7
			  StandardBasicTypes.STRING,// 8
			  StandardBasicTypes.STRING,// 9
			  StandardBasicTypes.STRING,// 10
			  StandardBasicTypes.STRING,// 11
			  StandardBasicTypes.STRING, // 12
			  StandardBasicTypes.STRING, // 12.1
			  StandardBasicTypes.INTEGER,//13
		     StandardBasicTypes.INTEGER // 14
		};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");
		if (filter.getkPaging() != null) {
            return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	    } else {
	        return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
	    }

	}
	
	/**vuongmq; 22/04/2015; lay danh sach thiet bi het han
	 * chua xai: xai ham nay de lay popup: getListEquipmentPopUp
	 * */
	@Override
	public List<EquipmentDeliveryVO> getListEquipmentDeliveryVOByFilterEx(EquipmentFilter<EquipmentDeliveryVO> filter) throws DataAccessException {
		if (filter.getStaffRoot() == null) {
			throw new IllegalArgumentException("staffRoot not id");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder whereSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with ");
		sql.append(" shopView as (");
		sql.append(" select distinct shop_id");
		sql.append(" from shop s");
		sql.append(" where status = 1");
		if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			sql.append(" start with shop_id IN(SELECT regexp_substr(?,'[^,]+', 1, level) FROM dual");
			sql.append(" CONNECT BY regexp_substr(?, '[^,]+', 1, level) IS NOT NULL)");
			sql.append(" connect by prior shop_id=parent_shop_id");
			params.add(filter.getStrListShopId().trim());
			params.add(filter.getStrListShopId().trim());
		}
		sql.append(" ),");
		sql.append(" customerView as ( ");
		sql.append(" select customer_id from customer ");
		sql.append(" where shop_id in (select * from shopView) and status = 1");
		sql.append(" )");

		sql.append(" select DISTINCT eq.equip_id as equipmentId, ");
		sql.append(" eq.code as equipmentCode, ");
		sql.append(" eq.serial as seriNumber, ");
		sql.append(" (case when eq.health_status is not null then (select min(value) from ap_param ap where ap.status = 1 and ap.ap_param_code like eq.health_status and type like 'EQUIP_CONDITION')else '' end) as healthStatus, ");
		sql.append(" (case ");
		sql.append(" when eq.stock_type = 3 then (select s.shop_code || cus.short_code from customer cus join shop s on s.shop_id = cus.shop_id where cus.customer_id = eq.stock_id and cus.status = 1) ");
		sql.append(" else (select sh.shop_code from shop sh where (sh.shop_code = eq.stock_code) and sh.status = 1) end)as stockCode, ");
		sql.append(" (case ");
		sql.append(" when eq.stock_type = 3 then (select cus.customer_name from customer cus where cus.customer_id = eq.stock_id and cus.status = 1) ");
		sql.append(" else (select sh.shop_name from shop sh where (sh.shop_code = eq.stock_code) and sh.status = 1) end)as stockName, ");
		sql.append(" ec.name as typeEquipment, ");
		sql.append(" eg.code as groupEquipmentCode, ");
		sql.append(" eg.name as groupEquipmentName, ");
		sql.append(" ( CASE ");
		sql.append(" WHEN eg.capacity_from IS NULL ");
		sql.append(" AND eg.capacity_to IS NULL ");
		sql.append(" THEN '' ");
		sql.append(" WHEN eg.capacity_from IS NULL ");
		sql.append(" THEN '<' ");
		sql.append(" ||TO_CHAR(eg.capacity_to) ");
		sql.append(" WHEN eg.capacity_to IS NULL ");
		sql.append(" THEN '>' ");
		sql.append(" ||TO_CHAR(eg.capacity_from) ");
		sql.append(" WHEN eg.capacity_to <> eg.capacity_from ");
		sql.append(" THEN TO_CHAR(eg.capacity_from) ");
		sql.append(" || ' - ' ");
		sql.append(" || TO_CHAR(eg.capacity_to) ");
		sql.append(" ELSE TO_CHAR(eg.capacity_from) ");
		sql.append(" END) AS capacity, ");
		sql.append(" eg.brand_name as equipmentBrand, ");
		sql.append(" ep.name as equipmentProvider, ");
		sql.append(" eq.manufacturing_year as yearManufacture, ");
		sql.append(" to_char(eq.warranty_Expired_Date,'dd/mm/yyyy') as warrantyExpiredDate, "); /** 06/04/2015; vuongmq; ngay het han bh*/
		/** vuongmq; 10/06/2015; lay so lan sua chua thiet bi cho chinh xac; Defect 227832; */
		//sql.append(" (select count(1)+1 from equip_repair_form erf where erf.equip_id = eq.equip_id and erf.status = ?) repairCount, ");
		sql.append(" (SELECT nvl(max(erf.repair_count),0)+1  from equip_repair_form erf where erf.equip_id = eq.equip_id and erf.status = ?) repairCount, ");
		params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
		if (filter.getIdRecordDelivery() != null) {
			sql.append(" edrdt.content AS contentDelivery ");
		} else {
			sql.append(" null AS contentDelivery ");
		}
		sql.append(" from equipment eq ");
		sql.append(" join equip_group eg on eg.equip_group_id = eq.equip_group_id ");
		sql.append(" join equip_category ec on eg.equip_category_id = ec.equip_category_id ");
		//sql.append(" join equip_brand eb on eb.equip_brand_id = eg.equip_brand_id ");
		sql.append(" join equip_provider ep on ep.equip_provider_id = eq.equip_provider_id ");
		if (filter.getIdRecordDelivery() != null) {
			sql.append(" join equip_delivery_rec_dtl edrdt on edrdt.equip_id = eq.equip_id ");
		}
		sql.append(" where 1=1 and ec.status = 1 and eg.status = 1 and ep.status = 1 and eq.trade_type is null ");
		sql.append(" and (eq.stock_id in (select * from customerView) or eq.stock_id in (select * from shopView))");
		if (filter.getLstEquipAdd() != null) {
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
				code += ",?";
			}
			sql.append(" and eq.code not in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
				params.add(filter.getLstEquipAdd().get(i));
			}
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			whereSql.append(" and upper(eq.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
			whereSql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCategoryCode())) {
			whereSql.append(" and upper(ec.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCategoryCode().toUpperCase()));
		}
		if (filter.getEquipCategoryId() != null) {
			whereSql.append(" and ec.equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getGroupCode())) {
			whereSql.append(" and upper(eg.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getGroupCode().toUpperCase()));
		}
		if (filter.getEquipGroupId() != null) {
			whereSql.append(" and eg.equip_group_id = ? ");
			params.add(filter.getEquipGroupId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getProviderCode())) {
			whereSql.append(" and upper(ep.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProviderCode().toUpperCase()));
		}
		if (filter.getEquipProviderId() != null) {
			whereSql.append(" and ep.equip_provider_id = ? ");
			params.add(filter.getEquipProviderId());
		}
		if (filter.getYearManufacture() != null) {
			whereSql.append(" and eq.manufacturing_year = ? ");
			params.add(filter.getYearManufacture());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			whereSql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
		}
		if (filter.getStockType() != null) {
			whereSql.append(" and eq.stock_type = ? ");
			params.add(filter.getStockType());
		} else if (filter.getIdRecordDelivery() == null) {
			//			whereSql.append(" and eq.stock_type != ? ");
			//			params.add(EquipStockTotalType.KHO_KH.getValue());
		}
		if (filter.getStatusEquip() != null) {
			whereSql.append(" and eq.status = ? ");
			params.add(filter.getStatusEquip());
		} else {
			whereSql.append(" and eq.status != ? ");
			params.add(StatusType.DELETED.getValue());
		}
		if (filter.getTradeStatus() != null) {
			whereSql.append(" and eq.trade_status = ?");
			params.add(filter.getTradeStatus());
		}
		//		if (filter.getUsageStatus() != null) {
		whereSql.append(" and eq.usage_status in (?,?)");
		params.add(EquipUsageStatus.IS_USED.getValue());
		params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
		//		}
		if (filter.getIdRecordDelivery() != null) {
			whereSql.append(" and edrdt.equip_delivery_rec_dtl_id in ");
			whereSql.append(" (select edrd.equip_delivery_rec_dtl_id from equip_delivery_rec_dtl edrd ");
			whereSql.append(" join equip_delivery_record edr on edr.equip_delivery_record_id = edrd.equip_delivery_record_id ");
			whereSql.append(" where 1=1 ");
			whereSql.append(" and edr.equip_delivery_record_id = ?) ");
			params.add(filter.getIdRecordDelivery());
		}
		String sqlTemp = sql.toString();
		sql.append(whereSql);
		// them cac thiet bi xoa tam tren danh sach
		if (filter.getLstEquipDelete() != null) {
			sql.append(" union ");
			sql.append(sqlTemp);
			if (filter.getLstEquipAdd() != null) {
				for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
					params.add(filter.getLstEquipAdd().get(i));
				}
			}
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipDelete().size(); i++) {
				code += ",?";
			}
			sql.append(" and eq.code in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipDelete().size(); i++) {
				params.add(filter.getLstEquipDelete().get(i));
			}
			if (!StringUtility.isNullOrEmpty(filter.getCode())) {
				sql.append(" and upper(eq.code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
				sql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getCategoryCode())) {
				sql.append(" and upper(ec.code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCategoryCode().toUpperCase()));
			}
			if (filter.getEquipCategoryId() != null) {
				sql.append(" and ec.equip_category_id = ? ");
				params.add(filter.getEquipCategoryId());
			}
			if (!StringUtility.isNullOrEmpty(filter.getGroupCode())) {
				sql.append(" and upper(eg.code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getGroupCode().toUpperCase()));
			}
			if (filter.getEquipGroupId() != null) {
				sql.append(" and eg.equip_group_id = ? ");
				params.add(filter.getEquipGroupId());
			}
			if (!StringUtility.isNullOrEmpty(filter.getProviderCode())) {
				sql.append(" and upper(ep.code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProviderCode().toUpperCase()));
			}
			if (filter.getEquipProviderId() != null) {
				sql.append(" and ep.equip_provider_id = ? ");
				params.add(filter.getEquipProviderId());
			}
			if (filter.getYearManufacture() != null) {
				sql.append(" and eq.manufacturing_year = ? ");
				params.add(filter.getYearManufacture());
			}
			if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
				sql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
			}
			if (filter.getStatusEquip() != null) {
				sql.append(" and eq.status = ? ");
				params.add(filter.getStatusEquip());
			} else {
				sql.append(" and eq.status != ? ");
				params.add(StatusType.DELETED.getValue());
			}
		}
		/**vuongmq; 16/04/2015; lay danh sach thiet bi het han; 01/07/2015; lay luon het han bao hanh*/
		//sql.append(" and eq.warranty_Expired_Date < sysdate ");
		sql.append(" ORDER BY equipmentCode ASC  ");
		String[] fieldNames = { "equipmentId",// 0
				"equipmentCode",// 1
				"seriNumber",// 2
				"healthStatus",// 3
				"stockCode",// 4
				"stockName",// 5
				"typeEquipment",// 6
				"groupEquipmentCode",// 7
				"groupEquipmentName",// 8
				"capacity",// 9
				"equipmentBrand",// 10
				"equipmentProvider",// 11
				"yearManufacture",// 12
				"warrantyExpiredDate", // 12.1 /** 06/04/2015; vuongmq; ngay het han bh*/
				"repairCount",// 13
				"contentDelivery",// 14
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.STRING,// 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.STRING, // 12.1
				StandardBasicTypes.INTEGER,//13
				StandardBasicTypes.INTEGER // 14
		};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	/**
	 * Lay equipment Entity by code an status
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	@Override
	public Equipment getEquipmentByCodeAndStatus(String code, ActiveType status) throws DataAccessException {
		if (code == null) {
			throw new IllegalArgumentException("code is null");
		}
		StringBuffer  sql = new StringBuffer();
		List<Object> params = new ArrayList<Object>();	
		sql.append("SELECT  eq.* ");
		sql.append("FROM   equipment eq ");
		sql.append("WHERE  1 = 1 ");
		sql.append("       AND upper(eq.code) = ? ");
		params.add(code.toUpperCase());
		if (status != null) {
			sql.append("       AND eq.status = ? ");
			params.add(status.getValue());
		} else {
			sql.append("       AND eq.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		return repo.getEntityBySQL(Equipment.class, sql.toString(), params);
	}
	
	/**
	 * Lay equipment Entity by code theo filter
	 * @author vuongmq
	 * @since 17/04/2015
	 */
	@Override
	public Equipment getEquipmentByCodeFilter(EquipmentFilter<Equipment> filter) throws DataAccessException {
		if (filter.getStaffRoot() == null || filter.getShopRoot() == null) {
			throw new IllegalArgumentException("staffRoot or shopRoot not id");
		}
		StringBuffer  sql = new StringBuffer();
		List<Object> params = new ArrayList<Object>();	
		sql.append(" WITH t_kho_under AS ");
		sql.append(" (SELECT es.equip_stock_id AS id, ");
		sql.append(" es.shop_id as shopId, ");
		sql.append(" es.code AS code, ");
		sql.append(" es.name AS name ");
		sql.append(" FROM equip_role_user eru ");
		sql.append(" JOIN equip_role_detail erd ON eru.equip_role_id = erd.equip_role_id ");
		sql.append(" JOIN equip_stock es ON es.equip_stock_id = erd.equip_stock_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND es.status = 1 ");
		sql.append(" AND erd.is_under = 1 "); /** co quan ly don vi cap duoi: 1*/
		sql.append(" AND eru.status = 1 ");
		sql.append(" AND eru.user_id = ? ");
		params.add(filter.getStaffRoot());
		sql.append(" ), ");
		sql.append(" stock_under as ( ");
		sql.append(" select es.equip_stock_id as id, ");
		sql.append(" es.shop_id as shopId, ");
		sql.append(" es.code as code, ");
		sql.append(" es.name as name ");
		sql.append(" from equip_stock es ");
		sql.append(" where 1=1 ");
		sql.append(" AND es.status = 1 ");
		sql.append(" and es.shop_id in ( ");
		sql.append(" select sh.shop_id ");
		sql.append(" FROM shop sh ");
		sql.append(" WHERE sh.status = 1 ");
		sql.append(" and sh.shop_id not in (SELECT shopId FROM t_kho_under) ");	 // vuongmq; 25/05/2015; tru all kho cua shop t_kho_under
		sql.append(" START WITH sh.shop_id IN (SELECT shopId FROM t_kho_under) ");
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sql.append("   ) ");
		sql.append(" union ");
		sql.append(" (select * from t_kho_under) ");
		sql.append(" ), ");
		sql.append(" t_kho_nounder as ");
		sql.append(" (SELECT es.equip_stock_id as id, ");
		sql.append(" es.shop_id as shopId, ");
		sql.append(" es.code as code, ");
		sql.append(" es.name as name ");
		sql.append(" FROM equip_role_user eru ");
		sql.append(" JOIN equip_role_detail erd ON eru.equip_role_id = erd.equip_role_id ");
		sql.append(" JOIN equip_stock es ON es.equip_stock_id = erd.equip_stock_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND es.status = 1 ");
		sql.append(" AND erd.is_under = 0 "); /** khong quan ly don vi cap duoi: 0*/
		sql.append(" AND eru.status = 1 ");
		sql.append(" AND eru.user_id = ? ");
		params.add(filter.getStaffRoot());
		sql.append(" ), ");
		/**end danh sach kho Don vi tmp */
		sql.append(" lstStockShopTmp AS ");
		sql.append(" ( SELECT * FROM stock_under ");
		sql.append(" UNION ");
		sql.append(" SELECT * FROM t_kho_nounder ");
		sql.append(" ), ");
		sql.append(" lstStockShop AS ");
		sql.append(" ( SELECT * FROM lstStockShopTmp t where t.shopId in ( ");
		sql.append(" select sh.shop_id from shop sh ");
		sql.append(" where sh.status = 1 ");
		sql.append(" START WITH sh.shop_id IN (?) ");
		params.add(filter.getShopRoot());
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sql.append(" ) ");
		sql.append(" ), ");
		/**end danh sach kho Don vi ; dung kho DV la: lstStockShop */
		sql.append(" lstshopCUS as ( ");
		sql.append(" select shopId from ( ");
		sql.append(" select shop_id as shopId from shop sh ");
		sql.append(" START WITH sh.shop_id IN (SELECT shopId FROM t_kho_under) ");
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sql.append(" UNION ");
		sql.append(" select shopId from t_kho_nounder ) ");
		sql.append(" where shopId in ( ");
		sql.append(" select sh.shop_id from shop sh ");
		sql.append(" where 1=1 ");
		sql.append(" and sh.status = 1 ");
		sql.append(" START WITH sh.shop_id IN (?) ");
		params.add(filter.getShopRoot());
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id) ");
		sql.append(" ), ");
		sql.append(" t_kho_customer as ");
		sql.append(" ( select c.customer_id as id, ");
		sql.append(" sh.shop_code||c.short_code as code, ");
		sql.append(" c.customer_name as name ");
		sql.append(" from customer c ");
		sql.append(" join shop sh on sh.shop_id = c.shop_id ");
		sql.append(" where c.status = 1 ");
		sql.append(" and c.shop_id in (select tc.shopId from lstshopCUS tc) ");
		sql.append(" ) ");
		/**end danh sach kho Customer ; dung kho customer la: t_kho_customer; lay all customer cua kho Qly, lay theo ShopId */

		/**** select Entities Equipment ***/
		sql.append("SELECT  eq.* ");
		sql.append("FROM   equipment eq ");
		/** begin cac dieu kien where macdinh */
		sql.append(" WHERE 1 =1 ");
		sql.append(" and ( (eq.stock_id IN (select t.id from lstStockShop t) and eq.stock_type = ?) ");
		params.add(EquipStockTotalType.KHO.getValue()); /** kho cua don vi; id equip_stock*/
		sql.append(" or (eq.stock_id IN (select tmp.id from t_kho_customer tmp) and eq.stock_type = ?) ");
		params.add(EquipStockTotalType.KHO_KH.getValue()); /** kho cua kh*/
		sql.append(" ) ");
		/** end cac dieu kien where macdinh */
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append("       AND eq.code = ? ");
			params.add(filter.getCode().toUpperCase());
		}
		if (filter.getStockType() != null) {
			sql.append(" and eq.stock_type = ? ");
			params.add(filter.getStockType());
		}
		if (filter.getStatusEquip() != null) {
			sql.append(" and eq.status = ? ");
			params.add(filter.getStatusEquip());
		} else {
			sql.append(" and eq.status != ? ");
			params.add(StatusType.DELETED.getValue());
		}
		if (filter.getTradeStatus() != null) {
			sql.append(" and eq.trade_status = ?");
			params.add(filter.getTradeStatus());
		}
		if (filter.getTradeType() != null) {
			sql.append(" AND eq.trade_type = ? ");
			params.add(filter.getTradeType());
		}
		if (Boolean.TRUE.equals(filter.isFlagNotUsageStatus())) {
			// vuongmq; 03/07/2015; truong hop import excel phieu sua chua thi: True, khong can lay trang thai su dung va o kho
			// cac truong hop khac lay thiet bi trang thai su dung va o kho
		} else {
			if (filter.getTradeType() != null) {
				sql.append(" AND eq.trade_type IS NULL ");
			}
			sql.append(" and eq.usage_status in (?,?)");
			params.add(EquipUsageStatus.IS_USED.getValue());
			params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
		}
		return repo.getEntityBySQL(Equipment.class, sql.toString(), params);
	}
	
	/**
	 * @author vuongmq
	 * @date 11/05/2015
	 * Lay thiet bi theo ma code; dung import bao mat
	 */
	@Override
	public Equipment getEquipmentByCode(String code) throws DataAccessException {
		if (code == null) {
			throw new IllegalArgumentException("code is null");
		}
		StringBuffer  sql = new StringBuffer();
		List<Object> params = new ArrayList<Object>();	
		
//		sql.append("SELECT  eq.equip_id           AS equipmentId, ");
//		sql.append("                eq.code               AS equipmentCode, ");
//		sql.append("                eq.serial             AS seriNumber, ");
//		sql.append("                ( CASE ");
//		sql.append("                    WHEN eq.health_status IS NOT NULL THEN ");
//		sql.append("                    (SELECT Min(value) ");
//		sql.append("                     FROM   ap_param ap ");
//		sql.append("                     WHERE  ap.status = 1 ");
//		sql.append("                            AND ap.ap_param_code LIKE ");
//		sql.append("                                eq.health_status ");
//		sql.append("                            AND ");
//		sql.append("                    type LIKE 'EQUIP_CONDITION' ");
//		sql.append("                    ) ");
//		sql.append("                    ELSE '' ");
//		sql.append("                  END )               AS healthStatus, ");
//		sql.append("                ( CASE ");
//		sql.append("                    WHEN eq.stock_type = 2 THEN (SELECT cus.customer_code ");
//		sql.append("                                                 FROM   customer cus ");
//		sql.append("                                                 WHERE ");
//		sql.append("                    cus.customer_id = eq.stock_id ");
//		sql.append("                    AND cus.status = 1) ");
//		sql.append("                    ELSE (SELECT es.code ");
//		sql.append("                          FROM   equip_stock es ");
//		sql.append("                          WHERE  es.equip_stock_id = eq.stock_id ");
//		sql.append("                                 AND es.status = 1) ");
//		sql.append("                  END )               AS stockCode, ");
//		sql.append("                ( CASE ");
//		sql.append("                    WHEN eq.stock_type = 2 THEN (SELECT cus.customer_name ");
//		sql.append("                                                 FROM   customer cus ");
//		sql.append("                                                 WHERE ");
//		sql.append("                    cus.customer_id = eq.stock_id ");
//		sql.append("                    AND cus.status = 1) ");
//		sql.append("                    ELSE (SELECT es.NAME ");
//		sql.append("                          FROM   equip_stock es ");
//		sql.append("                          WHERE  es.equip_stock_id = eq.stock_id ");
//		sql.append("                                 AND es.status = 1) ");
//		sql.append("                  END )               AS stockName, ");
//		sql.append("                ec.NAME               AS typeEquipment, ");
//		sql.append("                eg.code               AS groupEquipmentCode, ");
//		sql.append("                eg.NAME               AS groupEquipmentName, ");
//		sql.append("                ( CASE ");
//		sql.append("                    WHEN eg.capacity_from IS NULL ");
//		sql.append("                         AND eg.capacity_to IS NULL THEN '' ");
//		sql.append("                    WHEN eg.capacity_from IS NULL THEN '<' ");
//		sql.append("                                                       || ");
//		sql.append("                    To_char(eg.capacity_to) ");
//		sql.append("                    WHEN eg.capacity_to IS NULL THEN '>' ");
//		sql.append("                                                     || ");
//		sql.append("                    To_char(eg.capacity_from) ");
//		sql.append("                    WHEN eg.capacity_to <> eg.capacity_from THEN ");
//		sql.append("                    To_char(eg.capacity_from) ");
//		sql.append("                    || ' - ' ");
//		sql.append("                    || To_char(eg.capacity_to) ");
//		sql.append("                    ELSE To_char(eg.capacity_from) ");
//		sql.append("                  END )               AS capacity, ");
//		sql.append("                eg.brand_name         AS equipmentBrand, ");
//		sql.append("                ep.NAME               AS equipmentProvider, ");
//		sql.append("                eq.manufacturing_year AS yearManufacture, ");
//		sql.append("                (SELECT To_char(eq.first_date_in_use, 'yyyy') ");
//		sql.append("                 FROM   dual)         AS firstYearInUse, ");
//		sql.append("                eq.price              AS price, ");
//		sql.append("                NULL                  AS contentDelivery, ");
//		sql.append("                NULL                  AS depreciation ");
		sql.append("SELECT  eq.* ");
		sql.append("FROM   equipment eq ");
		sql.append("       JOIN equip_group eg ");
		sql.append("         ON eg.equip_group_id = eq.equip_group_id ");
		sql.append("       JOIN equip_category ec ");
		sql.append("         ON eg.equip_category_id = ec.equip_category_id ");
		sql.append("       JOIN equip_provider ep ");
		sql.append("         ON ep.equip_provider_id = eq.equip_provider_id ");
		sql.append("WHERE  1 = 1 ");
		sql.append("       AND upper(eq.code) = ? ");
		params.add(code.toUpperCase().trim());
		
		/*sql.append("       AND eq.stock_type != ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());*/
		
		/*sql.append("       AND eq.status = ? ");
		params.add(ActiveType.RUNNING.getValue());*/
		
//		sql.append("       AND eq.trade_status = ? ");
//		params.add(EquipTradeStatus.TRADING.getValue());
		
		/*sql.append("       AND eq.usage_status = ? ");
		params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());*/
//		sql.append("ORDER  BY equipmentcode ASC");
		return repo.getEntityBySQL(Equipment.class, sql.toString(), params);
	}

	@Override
	public EquipGroup getEquipGroupById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipGroup.class, id);
	}

	@Override
	public EquipGroup getEquipGroupByCode(String code) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from equip_group where lower(code) = ? and status != ? ";
		List<Object> params = new ArrayList<Object>();
		params.add(code.trim().toLowerCase());
		params.add(ActiveType.DELETED.getValue());
		EquipGroup rs = repo.getEntityBySQL(EquipGroup.class, sql, params);
		return rs;
	}

	@Override
	public Equipment getEquipmentById(Long id) throws DataAccessException {
		return repo.getEntityById(Equipment.class, id);
	}
	
	@Override
	public Equipment getEquipmentById(Long equipmentId, Long shopId) throws DataAccessException {
		if (equipmentId == null) {
			throw new IllegalArgumentException("equipmentId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select e.* ");
		sql.append(" from equipment e ");
		sql.append(" where e.equip_id = ? ");
		params.add(equipmentId);
		if (shopId != null) {
			sql.append(" AND ( ");
			sql.append("  (e.stock_type = 1 and e.stock_id IN ");
			sql.append(" (SELECT equip_stock_id ");
			sql.append(" FROM equip_stock ");
			sql.append(" WHERE status = 1 ");
			sql.append(" and shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append(" ) ");
			params.add(shopId);
			/** end kho NPP */
			sql.append(" or  (e.stock_type = 2 and e.stock_id IN ");
			sql.append(" (SELECT cu.customer_id ");
			sql.append(" FROM customer cu ");
			sql.append(" WHERE  ");
			sql.append(" cu.shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append("  ");
			sql.append(" ) ");
			/** end kho KH */
			sql.append(" ) "); // end And
			params.add(shopId);
		}
		return repo.getEntityBySQL(Equipment.class, sql.toString(), params);
	}

	@Override
	public String getEquipImportRecordCodeGenetated() throws DataAccessException {
		String sql = "select (select max(code) as maxCode from equip_import_record) as maxCode from dual";
		String maxCode = (String) repo.getObjectByQuery(sql, null);
		if (!StringUtility.isNullOrEmpty(maxCode)) {
			String maxCodeNew = maxCode.replaceAll("CM", "").trim();
			try {
				Integer numberMaxCode = Integer.valueOf(maxCodeNew);
				numberMaxCode++;
				String kq = "00000000" + String.valueOf(numberMaxCode);
				kq = kq.substring(kq.length() - 8).trim();
				kq = "CM" + kq.trim();
				return kq.trim();
			} catch (Exception e) {
				throw new IllegalArgumentException("error data in by code not format");
			}
		}
		return "CM00000001";
	}

	@Override
	public String getMaxEquipImportRecordCode() throws DataAccessException {
		String sql = "select (select max(code) as maxCode from equip_import_record) as maxCode from dual";
		String maxCode = (String) repo.getObjectByQuery(sql, null);
		if (!StringUtility.isNullOrEmpty(maxCode)) {
			return maxCode;
		}
		return "CM00000001";
	}

	@Override
	public String getMaxEquipmentCodeByEquipGroupId(Long equipGroupId) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "select max(code) as maxCode from equipment where 1 = 1 and equip_group_id = ? ";
		params.add(equipGroupId);
		String maxCode = (String) repo.getObjectByQuery(sql, params);
		if (!StringUtility.isNullOrEmpty(maxCode)) {
			return maxCode;
		}
		return "";
	}
	
	@Override
	public String getMaxEquipmentCodeByEquipCategoryId(Long equipCategoryId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT Max(e.code) AS maxCode ");
		sql.append(" FROM   equipment e ");
		sql.append(" JOIN equip_group eg ");
		sql.append(" ON E.equip_group_id = Eg.equip_group_id ");
		sql.append(" WHERE  1 = 1 ");
		sql.append(" AND Eg.equip_category_id = ?");
		params.add(equipCategoryId);
		
		String maxCode = (String) repo.getObjectByQuery(sql.toString(), params);
		if (!StringUtility.isNullOrEmpty(maxCode)) {
			return maxCode;
		}
		return "";
	}

	@Override
	public String getMaxEquipmentCode(String prefix) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "select (select max(code) as maxCode from equipment where 1 = 1 ";
		if (!StringUtility.isNullOrEmpty(prefix)) {
			sql = sql + " and code like ? ";
			params.add(StringUtility.toOracleSearchLikePrefix(prefix.trim()));
		}
		sql = sql + " ) as maxCode from dual ";
		String maxCode = (String) repo.getObjectByQuery(sql, params);
		if (!StringUtility.isNullOrEmpty(maxCode)) {
			return maxCode;
		}
		return "";
	}

	@Override
	public List<EquipmentVO> getListEquipmentBrand(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct eb.equip_brand_id as id, eb.name ");
		sql.append(" from equip_brand eb ");
		sql.append(" where 1 = 1 and eb.status =1 ");
		sql.append(" ORDER BY id ");
		String[] fieldNames = { "id",// 0
				"name"// 1 
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
		};
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipmentVO> getListEquipmentCategory(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
//		sql.append(" select distinct equip_category_id as id,code as code, name as name from equip_category ");
//		sql.append(" where 1 = 1 and status =1 ");
//		if(!StringUtility.isNullOrEmpty(filter.getEquipCategoryCode())){
//			sql.append(" and code =? ");
//			params.add(filter.getEquipCategoryCode().toUpperCase());
//		}
//		sql.append(" ORDER BY id ");
		sql.append("SELECT DISTINCT ec.equip_category_id AS id, ");
		sql.append("                ec.code              AS code, ");
		sql.append("                ec.NAME              AS NAME ");
//		if (filter.getEquipLendCode() != null && !filter.getEquipLendCode().isEmpty()) {
//			sql.append("FROM   equip_lend el ");
//			sql.append("       JOIN equip_lend_detail eld ");
//			sql.append("         ON el.equip_lend_id = eld.equip_lend_id ");
//			sql.append("       JOIN equip_category ec ");
//			sql.append("         ON eld.equip_category_id = ec.equip_category_id ");
//		} else {
//			sql.append("FROM   equip_category ec  ");
//		}
		sql.append("FROM   equip_category ec  ");

		sql.append("WHERE  1 = 1 ");
		sql.append("       AND ec.status = 1 ");
//		if (filter.getEquipLendCode() != null && !filter.getEquipLendCode().isEmpty()) {
//			sql.append("       AND el.status = 1 ");
//			sql.append("       AND eld.status = 1 ");
//			sql.append("       AND el.code = ? ");
//			params.add(filter.getEquipLendCode());
//		}
		if (filter.getEquipLendCode() != null && !filter.getEquipLendCode().isEmpty()) {
			sql.append(" AND ec.equip_category_id IN ");
			sql.append(" ( ");
			sql.append("       SELECT equip_category_id ");
			sql.append("       FROM   equip_lend_detail ");
			sql.append("       WHERE  equip_lend_id IN ");
			sql.append("              ( ");
			sql.append("                     SELECT equip_lend_id ");
			sql.append("                     FROM   equip_lend ");
			sql.append("                     WHERE  code = ?) and status = 1");
			params.add(filter.getEquipLendCode());
			if (filter.getCustomerId() != null) {
				sql.append(" and customer_id = ? ");
				params.add(filter.getCustomerId());
			}
			sql.append(" ) ");
		}
		sql.append("ORDER  BY ec.code");

		String[] fieldNames = { "id", "code", "name" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipCategory> getListEquipmentCategoryByFilter(EquipmentFilter<EquipCategory> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct * from equip_category ");
		sql.append(" where 1 = 1 ");
		if (filter.getIsCheckStatus() == null || filter.getIsCheckStatus().equals(Boolean.TRUE)) {
			if (filter.getStatus() != null) {
				sql.append(" and status = ? ");
				params.add(filter.getStatus());
			} else {
				sql.append(" and status <> -1 ");
			}
		}
		
		if (filter.getEquipCategoryId() != null) {
			sql.append(" and  equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		sql.append(" order by code ");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(EquipCategory.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(EquipCategory.class, sql.toString(), params, filter.getkPaging());
		}
	}

	@Override
	public List<EquipProvider> getListEquipProviderByFilter(EquipmentFilter<EquipProvider> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_provider ");
		sql.append(" where 1 = 1 ");
		if (filter.getStatus() != null) {
			sql.append(" and status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and status <> -1 ");
		}
		sql.append(" order by code ");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(EquipProvider.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(EquipProvider.class, sql.toString(), params, filter.getkPaging());
		}
	}

	@Override
	public EquipFormHistory createEquipmentHistoryRecord(EquipFormHistory equipFormHistory) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipFormHistory == null) {
			throw new IllegalArgumentException("equipFormHistory is null");
		}
		return repo.create(equipFormHistory);
	}
	
	/**
	 * get list customer by shop
	 * @author phut
	 */
	@Override
	public List<Customer> getListCustomerByShop(Long recordId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from CUSTOMER cu where cu.status = ? and cu.shop_id IN (");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("select UNIT_ID from EQUIP_STATISTIC_UNIT where EQUIP_STATISTIC_RECORD_ID = ?");
		sql.append(")");
		params.add(recordId);
		return repo.getListBySQL(Customer.class, sql.toString(), params);
	}
	
	/**
	 * Lay danh sach khach hang theo Don vi va nhom san pham
	 * 
	 * @author hunglm16
	 * @since June 15, 2015
	 */
	@Override
	public List<Customer> getListCustomerByEquipStatisticRecordId(Long recordId) throws DataAccessException {
		if (recordId == null) {
			throw new IllegalArgumentException("recordId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with ");
		sql.append(" equipTemp as ( ");
		sql.append(" select e.stock_id ");
		sql.append(" from equipment e ");
		sql.append(" join EQUIP_STATISTIC_GROUP eg on eg.EQUIP_STATISTIC_RECORD_ID = ? and e.equip_group_id = eg.object_id and eg.OBJECT_TYPE = ? ");
		params.add(recordId);
		params.add(EquipObjectType.GROUP.getValue());
		sql.append(" where e.stock_type = ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append(" union ");
		sql.append(" select e.stock_id ");
		sql.append(" from equipment e ");
		sql.append(" join EQUIP_STATISTIC_GROUP eg on eg.EQUIP_STATISTIC_RECORD_ID = ? and e.equip_id = eg.object_id and eg.OBJECT_TYPE = ? ");
		params.add(recordId);
		params.add(EquipObjectType.EQUIP.getValue());
		sql.append(" where e.stock_type = ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append(" ) ");
		sql.append(" select DISTINCT cu.* ");
		sql.append(" from CUSTOMER cu ");
		sql.append(" join EQUIP_STATISTIC_UNIT un on un.EQUIP_STATISTIC_RECORD_ID = ? and un.UNIT_ID = cu.shop_id ");
		params.add(recordId);
//		sql.append(" where cu.status = ? ");
//		params.add(ActiveType.RUNNING.getValue());
		sql.append(" where 1=1 ");
		sql.append(" and cu.CUSTOMER_ID not in (select CUSTOMER_ID from EQUIP_STATISTIC_CUSTOMER where status <> ? and EQUIP_STATISTIC_RECORD_ID = ?) ");
		params.add(ActiveType.DELETED.getValue());
		params.add(recordId);
		sql.append(" and cu.CUSTOMER_ID in (select stock_id from equipTemp) ");
		return repo.getListBySQL(Customer.class, sql.toString(), params);
	}
	
	/**
	 * Lay danh sach khach hang theo Import tring kiem ke thiet bi
	 * 
	 * @author hunglm16
	 * @since June 15, 2015
	 */
	@Override
	public List<EquipStatisticCustomer> getListCusInImportEquipStatisticCus(Long recordId, Integer status) throws DataAccessException {
		if (recordId == null) {
			throw new IllegalArgumentException("recordId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select *   ");
		sql.append(" from EQUIP_STATISTIC_CUSTOMER ");
		sql.append(" where EQUIP_STATISTIC_RECORD_ID = ?  ");
		params.add(recordId);
		if (status != null) {
			sql.append(" and status = ? ");
			params.add(status);
		}
		return repo.getListBySQL(EquipStatisticCustomer.class, sql.toString(), params);
	}
	
	/**
	 * get list customer join in record
	 * @author phut
	 */
	@Override
	public Map<Long, EquipStatisticCustomer> getListExistsCustomer(Long recordId, List<Customer> list) throws DataAccessException {
		Map<Long, EquipStatisticCustomer> map = new HashMap<Long, EquipStatisticCustomer>();
		int dev = list.size() / 900;
		for(int d = 0; d < dev; d++) {
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("select * from EQUIP_STATISTIC_CUSTOMER where EQUIP_STATISTIC_RECORD_ID = ? ");
			params.add(recordId);
			sql.append("and CUSTOMER_ID IN (");
			boolean first = true;
			for(int i = d * 900; i < (d + 1) * 900 && i < list.size(); i++) {
				if(first) {
					sql.append("?");
					params.add(list.get(i).getId());
					first = false;
				} else {
					sql.append(", ?");
					params.add(list.get(i).getId());
				}
			}
			sql.append(")");
			List<EquipStatisticCustomer> listResult = repo.getListBySQL(EquipStatisticCustomer.class, sql.toString(), params);
			for(EquipStatisticCustomer en : listResult) {
				map.put(en.getCustomer().getId(), en);
			}
		}
		return map;
	}
	
	/**
	 * Lay danh sach kh theo u ke va CT HTTM
	 * 
	 * @author hunglm16
	 * @sin June 15,2015
	 */
	@Override
	public List<Customer> getListCustomerByEquipSttRecIdbyCTHTTM(Long recordId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select distinct cu.* from customer cu  ");
		sql.append(" join sale_order so on cu.customer_id = so.customer_id  ");
		sql.append(" join sale_order_detail sod on so.sale_order_id = sod.sale_order_id ");
		sql.append(" join PROMOTION_PROGRAM pp on sod.PROGRAM_CODE = pp.PROMOTION_PROGRAM_CODE ");
		sql.append(" join EQUIP_STATISTIC_RECORD esr on esr.promotion_code = sod.PROGRAM_CODE and esr.promotion_code = pp.PROMOTION_PROGRAM_CODE and EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(recordId);
		sql.append(" join EQUIP_STATISTIC_UNIT esu on esu.UNIT_ID = cu.shop_id and esu.EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(recordId);
		sql.append(" join EQUIP_STATISTIC_GROUP esg on esu.EQUIP_STATISTIC_RECORD_ID = esg.EQUIP_STATISTIC_RECORD_ID and sod.product_id = esg.OBJECT_ID ");
		sql.append(" where 1 = 1  ");
		sql.append(" and pp.from_date < so.ORDER_DATE + 1 ");
		sql.append(" and (pp.to_date is null or (so.ORDER_DATE < pp.to_date + 1)) ");
//		sql.append(" and cu.status = 1  ");
		sql.append(" and so.type = 1 ");
		sql.append(" and so.approved = ? ");
		params.add(ApprovalStatus.APPROVED.getValue());
		sql.append(" and so.order_date < sysdate ");
		sql.append(" and esg.OBJECT_TYPE = 2 ");
		sql.append(" and cu.CUSTOMER_ID not in (select CUSTOMER_ID from EQUIP_STATISTIC_CUSTOMER where status <> ? and  EQUIP_STATISTIC_RECORD_ID = ?) ");
		params.add(ActiveType.DELETED.getValue());
		params.add(recordId);
		return repo.getListBySQL(Customer.class, sql.toString(), params);
	}
	
	/**
	 * get list customer join in ct httm
	 */
	@Override
	public List<Customer> getListCustomerBuTCHTTM(Long recordId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select distinct cu.* from customer cu inner join sale_order so on cu.customer_id = so.customer_id inner join sale_order_detail sod on so.sale_order_id = sod.sale_order_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and cu.status = ? and sod.PROGRAM_CODE = (select promotion_code from EQUIP_STATISTIC_RECORD where EQUIP_STATISTIC_RECORD_ID = ?) and so.type = 1 and cu.shop_id IN (");
		params.add(ActiveType.RUNNING.getValue());
		params.add(recordId);
		sql.append("select UNIT_ID from EQUIP_STATISTIC_UNIT where EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
		sql.append(")");
		return repo.getListBySQL(Customer.class, sql.toString(), params);
	}

	@Override
	public EquipHistory createEquipHistory(EquipHistory equipHistory) throws DataAccessException {
		if (equipHistory == null) {
			throw new IllegalArgumentException("equipHistory is null");
		}
		return repo.create(equipHistory);
	}

	@Override
	public List<EquipmentRepairFormVO> searchListEquipmentRepairFormVOByFilter(EquipmentRepairFormFilter<EquipmentRepairFormVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select distinct  ");
		sql.append(" erf. equip_id as equipId ");
		sql.append(" , erf.stock_id as stockId ");
		sql.append(" , erf.equip_period_id as equipPeriodId ");
		sql.append(" , erfdt.equip_repair_form_dtl_id as equipRepairFormDtlId ");
		sql.append(" , ei.equip_item_id as equipItemId ");
		sql.append(" , ei.code as equipItemCode ");
		sql.append(" , ei.name as equipItemName ");
		sql.append(" , erf.status as status ");
		sql.append(" , erfdt.warranty as warranty ");
		sql.append(" , erfdt.material_price as materialPrice ");
		sql.append(" , erfdt.worker_price as workerPrice ");
		sql.append(" , erfdt.quantity as quantity ");
		sql.append(" , erfdt.total_amount as totalAmount ");
		sql.append(" , erfdt.repair_count as repairCount ");
		sql.append(" , to_char(erf.complete_date, 'dd/MM/yyyy') as completeDateStr ");
		sql.append(" from equip_repair_form erf ");
		sql.append(" join equip_repair_form_dtl erfdt on erf.equip_repair_form_id = erfdt.equip_repair_form_id ");
		sql.append(" join equip_item ei on erfdt.equip_item_id = ei.equip_item_id ");
		sql.append("  where 1 = 1 ");
		if (filter.getStatus() != null) {
			sql.append("  and erf.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append("  and erf.status <> -1 ");
		}
		if (filter.getEquipId() != null) {
			sql.append("  and erf.equip_id = ? ");
			params.add(filter.getEquipId());
		}
		sql.append(" order by completeDateStr desc, ei.name  ");

		String[] fieldNames = { "equipId", "stockId", "equipPeriodId", "equipRepairFormDtlId", "equipItemId", "equipItemCode", "equipItemName", "status", "warranty", "materialPrice", "workerPrice", "quantity", "totalAmount", "repairCount",
				"completeDateStr" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentRepairFormVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentRepairFormVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<EquipmentVO> searchEquipmentGroupVObyFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT DISTINCT eg.equip_group_id    AS id, ");
		sql.append("                eg.equip_category_id AS equipCategoryId, ");
		sql.append("               (select ec.NAME from equip_category ec where ec.EQUIP_CATEGORY_ID = eg.EQUIP_CATEGORY_ID) as equipCategoryName, ");
		sql.append("                eg.code              AS code, ");
		sql.append("                eg.NAME              AS NAME, ");
		sql.append("                eg.code || ' - ' || eg.name    AS codeName, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN capacity_from IS NULL ");
		sql.append("                         AND capacity_to IS NULL THEN '' ");
		sql.append("                    WHEN capacity_from IS NULL THEN '<=' ");//Datpv4 13/07/2015, them <=
		sql.append("                                                    || To_char(capacity_to) ");
		sql.append("                    WHEN capacity_to IS NULL THEN '>=' ");//Datpv4 13/07/2015, them >=
		sql.append("                                                  || To_char(capacity_from) ");
		sql.append("                    WHEN capacity_to <> capacity_from THEN ");
		sql.append("                    To_char(capacity_from) ");
		sql.append("                    || ' - ' ");
		sql.append("                    || To_char(capacity_to) ");
		sql.append("                    ELSE To_char(capacity_from) ");
		sql.append("                  END )              AS capacityStr, ");
		sql.append("                eg.status            AS status, ");
		sql.append("                eg.name            AS equipCategoryName, ");
		sql.append("                eg.BRAND_NAME            AS brandName ");//Datpv4 08/07/2015, bo sung brandName
		sql.append(" FROM   equip_group eg ");
		sql.append(" JOIN Equip_Category ec on Ec.Equip_Category_Id = Eg.Equip_Category_Id and ec.status = 1 ");
//		if (filter.getEquipLendCode() != null && !filter.getEquipLendCode().isEmpty()) {
//			sql.append("       JOIN equip_lend_detail eld ");
//			sql.append("         ON eld.equip_category_id = eg.equip_category_id ");
//			sql.append("       JOIN equip_lend el ");
//			sql.append("         ON el.equip_lend_id = eld.equip_lend_id ");
//		}
		sql.append("WHERE  1 = 1 ");
		if (filter.getStatus() != null && filter.getStatus() != -1) {
			sql.append("       AND eg.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append("       AND eg.status <> -1 ");
		}
		if (filter.getEquipLendCode() != null && !filter.getEquipLendCode().isEmpty()) {
			sql.append(" AND ec.equip_category_id IN ");
			sql.append(" ( ");
			sql.append("       SELECT equip_category_id ");
			sql.append("       FROM   equip_lend_detail ");
			sql.append("       WHERE  equip_lend_id IN ");
			sql.append("              ( ");
			sql.append("                     SELECT equip_lend_id ");
			sql.append("                     FROM   equip_lend ");
			sql.append("                     WHERE  code = ?) and status = 1");
			params.add(filter.getEquipLendCode());
			if (filter.getCustomerId() != null) {
				sql.append(" and customer_id = ? ");
				params.add(filter.getCustomerId());
			}
			sql.append(" ) ");
		}
		if (filter.getEquipCategoryId() != null) {
			sql.append(" and eg.equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		//Them tham so tim kiem neu can thiet
		
		String[] fieldNames = { "id", "code", "name", "codeName", "capacityStr", "status",  "equipCategoryName" , "brandName"};
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING ,StandardBasicTypes.STRING };
		
		if (filter.getkPaging() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from ( ");
			countSql.append(sql);
			countSql.append(" ) ");
			sql.append(" ORDER BY eg.code");
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			sql.append(" ORDER BY eg.code");
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<EquipmentVO> searchEquipmentGroup(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select DISTINCT g.code ||' - '|| g.name groupEquip,g.code code, g.name name, c.name typeGroup, g.brand_name brandName,g.capacity_from capacityFrom, g.capacity_to capacityTo, ");
//		sql.append(" (case when g.capacity_from is null and g.capacity_to is null then '' when g.capacity_from is null then '<'||to_char(g.capacity_to) when g.capacity_to is null then '>'||to_char(g.capacity_from) ");
//		sql.append(" when g.capacity_to <> g.capacity_from then to_char(g.capacity_from) || ' - '|| to_char(g.capacity_to) else to_char(g.capacity_from) end) as capacity, ");
		sql.append("CASE ");
		sql.append("    WHEN g.capacity_from IS NULL ");
		sql.append("    AND g.capacity_to    IS NULL ");
		sql.append("    THEN '' ");
		sql.append("    WHEN g.capacity_from IS NULL ");
		sql.append("    THEN '<=' ");
		sql.append("      ||TO_CHAR(g.capacity_to) ");
		sql.append("    WHEN g.capacity_to IS NULL ");
		sql.append("    THEN '>=' ");
		sql.append("      ||TO_CHAR(g.capacity_from) ");
		sql.append("    WHEN g.capacity_to <> g.capacity_from ");
		sql.append("    THEN TO_CHAR(g.capacity_from) ");
		sql.append("      || ' - ' ");
		sql.append("      || TO_CHAR(g.capacity_to) ");
		sql.append("    ELSE TO_CHAR(g.capacity_from) ");
		sql.append("  END                  AS capacity,");
		sql.append(" g.equip_group_id id, ");
		sql.append(" es.quantity AS totalQuantity,  ");
		sql.append(" es.quantity AS quantity, ");
		sql.append(" g.status status ");
		sql.append(" from equip_group g ");
		sql.append(" left join equip_stock_total es on es.equip_group_id = g.equip_group_id ");
		if (ActiveType.RUNNING.getValue().equals(filter.getFlagStockCom())) {
			sql.append(" and es.stock_type = 0 ");
		}
		sql.append(" join EQUIP_CATEGORY c on g.equip_category_id = c.equip_category_id ");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and g.code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			sql.append(" and lower(g.name_text) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getName()).trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getBrand())) {
			sql.append(" and lower(g.brand_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getBrand().trim().toLowerCase()));
		}
		if (filter.getFromCapacity() != null) {
			sql.append(" and g.capacity_from >= ? ");
			params.add(filter.getFromCapacity());
		}
		if (filter.getToCapacity() != null) {
			sql.append(" and  g.capacity_to <=? ");
			params.add(filter.getToCapacity());
		}
		if (filter.getType() != null && filter.getType() != -1) {
			sql.append(" and  g.equip_category_id = ? ");
			params.add(filter.getType());
		}
		if (filter.getStatus() != null && filter.getStatus() != -1) {
			sql.append(" and g.status = ? ");
			params.add(filter.getStatus());
		}
		//	sql.append(" GROUP BY g.code, g.name, c.name, g.capacity_from, g.capacity_to , g.brand_name , g.status, g.equip_group_id ");
		sql.append(" order by groupEquip ");
		String[] fieldNames = { 
				"groupEquip"
				,"code"
				,"name"
				, "typeGroup"
				, "capacity"
				, "brandName"
				,"capacityFrom"
				,"capacityTo"
				, "status"
				, "id"
				, "totalQuantity"
				, "quantity"
//				,"fromMonth"
//				,"toMonth"
//				,"customerType"
//				,"amount"
				};

		Type[] fieldTypes = { 
				StandardBasicTypes.STRING
				,StandardBasicTypes.STRING
				,StandardBasicTypes.STRING
				, StandardBasicTypes.STRING
				, StandardBasicTypes.STRING
				, StandardBasicTypes.STRING
				,StandardBasicTypes.STRING
				,StandardBasicTypes.STRING
				, StandardBasicTypes.INTEGER
				, StandardBasicTypes.LONG
				, StandardBasicTypes.INTEGER
				, StandardBasicTypes.INTEGER
//				, StandardBasicTypes.INTEGER
//				, StandardBasicTypes.INTEGER
//				, StandardBasicTypes.INTEGER
//				, StandardBasicTypes.INTEGER
				};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	/*@Override
	public List<EquipmentVO> searchEquipmentGroup(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select DISTINCT g.code ||' - '|| g.name groupEquip, c.name typeGroup, g.brand_name brandName, ");
		sql.append(" (case when g.capacity_from is null and g.capacity_to is null then '' when g.capacity_from is null then '<'||to_char(g.capacity_to) when g.capacity_to is null then '>'||to_char(g.capacity_from) ");
		sql.append(" when g.capacity_to <> g.capacity_from then to_char(g.capacity_from) || ' - '|| to_char(g.capacity_to) else to_char(g.capacity_from) end) as capacity, ");
		sql.append(" g.equip_group_id id, ");
		sql.append(" (select sum(es.quantity) FROM equip_stock_total es WHERE g.equip_group_id = es.equip_group_id ");
		sql.append(" ) AS totalQuantity, ");
		sql.append(" (select sum(es.quantity) FROM equip_stock_total es WHERE g.equip_group_id = es.equip_group_id and es.stock_type = 1 ");
		sql.append(" ) AS quantity, ");
		sql.append(" g.status status ");
		sql.append(" from equip_group g ");
		sql.append(" left join equip_stock_total es on es.equip_group_id = g.equip_group_id ");
		sql.append(" join EQUIP_CATEGORY c on g.equip_category_id = c.equip_category_id ");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(g.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			sql.append(" and lower(g.name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getName().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getBrand())) {
			sql.append(" and lower(g.brand_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getBrand().trim().toLowerCase()));
		}
		if (filter.getFromCapacity() != null) {
			sql.append("and g.capacity_from >= ?");
			params.add(filter.getFromCapacity());
		}
		if (filter.getToCapacity() != null) {
			sql.append("and  g.capacity_to <=?");
			params.add(filter.getToCapacity());
		}
		if (filter.getType() != null && filter.getType() != -1) {
			sql.append(" and  g.equip_category_id = ? ");
			params.add(filter.getType());
		}
		if (filter.getStatus() != null && filter.getStatus() != -1) {
			sql.append(" and g.status = ? ");
			params.add(filter.getStatus());
		}
		//	sql.append(" GROUP BY g.code, g.name, c.name, g.capacity_from, g.capacity_to , g.brand_name , g.status, g.equip_group_id ");
		sql.append(" order by groupEquip ");
		String[] fieldNames = { "groupEquip", "typeGroup", "capacity", "brandName", "status", "id", "totalQuantity", "quantity" };

		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}*/
	@Override
	public List<EquipmentVO> getListEquipmentProvider(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select equip_provider_id as id,code as code, name as name, code || ' - ' || name as codeName from equip_provider ");
		sql.append(" where 1=1 ");
		if (filter.getStatus() != null) {
			sql.append(" and status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and status != ? ");
			params.add(0);
		}
		sql.append(" ORDER BY code desc, name desc ");
		String[] fieldNames = { "id",// 0
				"code", "name", "codeName"// 1 
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING // 1
		};
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public EquipGroup createEquipmentGroup(EquipGroup equipGroup) throws DataAccessException {
		if (equipGroup == null) {
			throw new IllegalArgumentException("equipGroup is null");
		}
		if (!StringUtility.isNullOrEmpty(equipGroup.getCode())) {
			equipGroup.setCode(equipGroup.getCode().trim().toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(equipGroup.getName())) {
			equipGroup.setNameText(Unicode2English.codau2khongdau(equipGroup.getName()).trim().toLowerCase());
		}
		equipGroup.setCreateDate(commonDAO.getSysDate());
		return repo.create(equipGroup);
	}

	@Override
	public EquipImportRecord createEquipImportRecord(EquipImportRecord equipImportRecord) throws DataAccessException {
		Date sysDate = commonDAO.getSysDate();
		if (equipImportRecord == null) {
			throw new IllegalArgumentException("Equipment is null");
		}
		if (!StringUtility.isNullOrEmpty(equipImportRecord.getCode())) {
			equipImportRecord.setCode(equipImportRecord.getCode().trim().toUpperCase());
		}
		equipImportRecord.setCreateDate(sysDate);
		return repo.create(equipImportRecord);
	}

	@Override
	public EquipSalePlan createEquipSalePlan(EquipSalePlan equipSalePlan) throws DataAccessException {
		if (equipSalePlan == null) {
			throw new IllegalArgumentException("equipSalePlan is null");
		}

		Date sysDate = commonDAO.getSysDate();
		equipSalePlan.setCreateDate(sysDate);

		return repo.create(equipSalePlan);
	}

	@Override
	public EquipSalePlan getEquipSalePlanById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipSalePlan.class, id);
	}

	@Override
	public EquipGroup updateEquipGroup(EquipGroup equipGroup, LogInfoVO logInfoVO) throws DataAccessException {
		if (equipGroup == null) {
			throw new IllegalArgumentException("equipGroup is null");
		}
		// code cua group: ten loai + ma code group nhap vao
		if (!StringUtility.isNullOrEmpty(equipGroup.getCode())) {
			equipGroup.setCode(equipGroup.getCode().trim().toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(equipGroup.getName())) {
			equipGroup.setNameText(Unicode2English.codau2khongdau(equipGroup.getName()).trim().toLowerCase());
		}
		equipGroup.setUpdateDate(commonDAO.getSysDate());
		repo.update(equipGroup);
		return equipGroup;
	}

	@Override
	public EquipSalePlan updateEquipSalePlan(EquipSalePlan equipSalePlan, LogInfoVO logInfoVO) throws DataAccessException {
		if (equipSalePlan == null) {
			throw new IllegalArgumentException("equipSalePlan is null");
		}
		equipSalePlan.setUpdateDate(commonDAO.getSysDate());
		repo.update(equipSalePlan);
		return equipSalePlan;
	}

	@Override
	public List<EquipmentVO> getEquipFlan(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
//		sql.append(" select distinct from_month as fromMonth, to_month as toMonth, customer_type as customerType, amount as aMount, ");
//		sql.append(" equip_sale_plan_id as id, ");
//		sql.append(" equip_group_id as equipGroupId, ");
//		sql.append(" case when customer_type=0 then 'Chưa xác định' ");
//		sql.append(" when customer_type =1 then 'Thành thị' ");
//		sql.append(" when customer_type =2 then 'Nông thôn' end as customerTypeName ");
//		sql.append(" from equip_sale_plan ");
//		sql.append(" where 1=1 ");
		sql.append("select distinct ");
		sql.append(" esp.equip_sale_plan_id as id, ");
		sql.append(" esp.equip_group_id as equipGroupId, ");
		sql.append(" esp.from_month as fromMonth, ");
		sql.append(" esp.to_month as toMonth, ");
		sql.append(" esp.amount as aMount, ");
		sql.append(" ct.CHANNEL_TYPE_ID as customerTypeId, ");
		sql.append(" ct.CHANNEL_TYPE_CODE as customerTypeCode, ");
		sql.append(" ct.CHANNEL_TYPE_NAME as customerTypeName ");
		sql.append(" from equip_sale_plan esp ");
		sql.append(" left join channel_type ct on ct.CHANNEL_TYPE_ID = esp.CUSTOMER_TYPE_ID and ct.type = ? ");
		params.add(ChannelTypeType.CUSTOMER.getValue());
		sql.append(" where 1=1");
		if (filter.getId() != null && filter.getId() > 0) {
			sql.append(" and equip_group_id =? ");
			params.add(filter.getId());
		}
		String[] fieldNames = { 
			"id", 				// 1
			"equipGroupId", 	// 2
			"fromMonth", 		// 3
			"toMonth", 			// 4
			"amount", 			// 5
			"customerTypeId",	// 6
			"customerTypeCode",	// 7
			"customerTypeName"	// 8
		};
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, 		// 1
				StandardBasicTypes.LONG, 		// 2
				StandardBasicTypes.INTEGER, 	// 3
				StandardBasicTypes.INTEGER, 	// 4
				StandardBasicTypes.BIG_DECIMAL, // 5
				StandardBasicTypes.LONG,		// 6
				StandardBasicTypes.STRING,		// 7
				StandardBasicTypes.STRING		// 8
		};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		sql.append(" order by equip_sale_plan_id ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public List<EquipSalePlan> getListEquipSalePlanByEquipGroup(Long equipGroupId) throws DataAccessException {
		if (equipGroupId == null) {
			throw new IllegalArgumentException("equipGroupId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_sale_plan ");
		sql.append(" where 1 = 1 ");
		sql.append(" and equip_group_id = ? ");
		params.add(equipGroupId);
		return repo.getListBySQL(EquipSalePlan.class, sql.toString(), params);
	}

	@Override
	public EquipSalePlan deleteEquipSalePlan(EquipSalePlan equipSalePlan, LogInfoVO logInfoVO) throws DataAccessException {
		if (equipSalePlan == null) {
			throw new IllegalArgumentException("equipSalePlan");
		}
		repo.delete(equipSalePlan);
		return equipSalePlan;
	}

	@Override
	public void deleteEquipEvictionFormDtl(EquipEvictionFormDtl equipEvictionFormDtl) throws DataAccessException {
		if (equipEvictionFormDtl == null) {
			throw new IllegalArgumentException("equipEvictionFormDtl");
		}
		repo.delete(equipEvictionFormDtl);
	}

	@Override
	public List<EquipmentVO> getListEquipStock(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		sql.append(" select  ");
		sql.append(" shop_id as shopId, ");
		sql.append(" shop_code ||' - '||shop_name kho, ");
		sql.append(" shop_code as shopCode, ");
		sql.append(" shop_name as shopName, ");
		sql.append(" status as status, ");
		sql.append(" parent_shop_id as parentId ");
		sql.append(" from shop sh ");
		sql.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and lower(s.shop_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and lower(s.shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getShopName().trim().toLowerCase())));
		}
		countSql.append("  select count(*) as count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");

		final String[] fieldNames = new String[] { "shopId", "kho", "shopCode", "shopName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };

		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	@Override
	public List<EquipmentVO> searchEquipmentStockChange(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		if (StringUtility.isNullOrEmpty(filter.getToStock()) && StringUtility.isNullOrEmpty(filter.getFromStock())) {
			sql.append("WITH rdta ");
			sql.append("     AS (SELECT es.equip_stock_id AS equipStockId, ");
			sql.append("                es.code           AS equipStockCode, ");
			sql.append("                es.name           AS equipStockName, ");
			sql.append("                es.shop_id        AS shopId, ");
			sql.append("                sh.shop_code      AS shopCode, ");
			sql.append("                sh.shop_name      AS shopName, ");
			sql.append("                erd.is_under      AS isUnder ");
			sql.append("         FROM   equip_role_user eru ");
			sql.append("                join equip_role er ");
			sql.append("                  ON eru.equip_role_id = er.equip_role_id ");
			sql.append("                join equip_role_detail erd ");
			sql.append("                  ON er.equip_role_id = erd.equip_role_id ");
			sql.append("                join equip_stock es ");
			sql.append("                  ON erd.equip_stock_id = es.equip_stock_id ");
			sql.append("                join shop sh ");
			sql.append("                  ON es.shop_id = sh.shop_id ");
			sql.append("         WHERE  eru.status = 1 ");
			sql.append("                AND er.status = 1 ");
			sql.append("                AND es.status = 1 ");
			sql.append("                AND eru.user_id = ?), ");
			params.add(filter.getStaffRoot());
			sql.append("     rstockfull ");
			sql.append("     AS (SELECT DISTINCT equipstockid, ");
			sql.append("                         equipstockcode, ");
			sql.append("                         equipstockname, ");
			sql.append("                         shopcode, ");
			sql.append("                         shopname, ");
			sql.append("                         shopid ");
			sql.append("         FROM   ((SELECT es.equip_stock_id AS equipStockId, ");
			sql.append("                         es.code           AS equipStockCode, ");
			sql.append("                         es.name           AS equipStockName, ");
			sql.append("                         es.shop_id        AS shopId, ");
			sql.append("                         sh.shop_code      AS shopCode, ");
			sql.append("                         sh.shop_name      AS shopName, ");
			sql.append("                         1                 AS isUnder ");
			sql.append("                  FROM   (SELECT * ");
			sql.append("                          FROM   shop ");
			sql.append("                          WHERE  1 = 1 ");
			sql.append("                          START WITH shop_id IN (SELECT shopid ");
			sql.append("                                                 FROM   rdta ");
			sql.append("                                                 WHERE  isunder = 1) ");
			sql.append("                                     AND status = 1 ");
			sql.append("                          CONNECT BY PRIOR shop_id = parent_shop_id) sh ");
			sql.append("                         join equip_stock es ");
			sql.append("                           ON sh.shop_id = es.shop_id ");
			sql.append("                  WHERE  es.status = 1 ");
			sql.append("                  MINUS ");
			sql.append("                  (SELECT es.equip_stock_id AS equipStockId, ");
			sql.append("                          es.code           AS equipStockCode, ");
			sql.append("                          es.name           AS equipStockName, ");
			sql.append("                          es.shop_id        AS shopId, ");
			sql.append("                          sh.shop_code      AS shopCode, ");
			sql.append("                          sh.shop_name      AS shopName, ");
			sql.append("                          1                 AS isUnder ");
			sql.append("                   FROM   shop sh ");
			sql.append("                          join equip_stock es ");
			sql.append("                            ON sh.shop_id = es.shop_id ");
			sql.append("                   WHERE  1 = 1 ");
			sql.append("                          AND sh.shop_id IN (SELECT shopid ");
			sql.append("                                             FROM   rdta ");
			sql.append("                                             WHERE  isunder = 1) ");
			sql.append("                          AND es.equip_stock_id NOT IN (SELECT equipstockid ");
			sql.append("                                                        FROM   rdta))) ");
			sql.append("                 UNION ");
			sql.append("                 (SELECT * ");
			sql.append("                  FROM   rdta ");
			sql.append("                  WHERE  isunder = 0)) ");
			sql.append("         ORDER  BY equipstockcode) ");
			sql.append("         , rshop as ( ");
			sql.append("           select es.equip_stock_id ");
			sql.append("           from equip_stock es ");
			sql.append("           join ( ");
			sql.append("           select shop_id ");
			sql.append("           from shop	 ");
			sql.append("           START WITH shop_id IN (?)	 ");
			params.add(filter.getShopRoot());
			sql.append("           CONNECT BY PRIOR shop_id = parent_shop_id) r on r.shop_id = es.shop_id )	  ");
		}
		
		sql.append(" select  ");
		sql.append(" est.equip_stock_trans_form_id as id, ");
		sql.append(" est.code as code, ");
		sql.append(" est.note as note, ");
		sql.append(" to_char(est.create_date,'dd/mm/yyyy') as creatDate, ");
		sql.append(" to_char(est.create_form_date,'dd/mm/yyyy') as createFormDate, ");
		sql.append(" est.status as status, ");
		sql.append(" est.perform_status as performStatus, ");
		sql.append(" wm_concat(distinct TO_CHAR(' '|| from_st.code ||'('|| fsh.shop_name ||')')) fromStock, ");
		sql.append(" wm_concat(distinct TO_CHAR(' '|| to_st.code ||'('|| tsh.shop_name ||')')) toStock, ");
		sql.append("(select url from equip_attach_file where ");
		sql.append("	object_id = est.equip_stock_trans_form_id and rownum = 1 and object_type = ?) as url "); // lay them Url de kiem tra tap tin
		params.add(EquipTradeType.WAREHOUSE_TRANSFER.getValue());
		
		sql.append(" from equip_stock_trans_form est ");
		sql.append(" join equip_stock_trans_form_dtl dt on est.equip_stock_trans_form_id = dt.equip_stock_trans_form_id ");
		sql.append(" JOIN equip_stock to_st ON to_st.equip_stock_id = dt.to_stock_id  ");
		sql.append(" JOIN shop tsh on tsh.shop_id = to_st.shop_id");
		sql.append(" JOIN equip_stock from_st ON from_st.equip_stock_id = dt.from_stock_id ");
		sql.append(" JOIN shop fsh on fsh.shop_id = from_st.shop_id ");
		sql.append(" where 1=1 ");
		if (filter.getShopRoot() != null) {
			sql.append(" and dt.from_stock_id IN (SELECT equip_stock_id from equip_stock  where shop_id in (  SELECT shop_id");
			sql.append("  FROM shop WHERE status               = 1  START WITH shop_id      IN (?) AND status                 = 1");
			sql.append("   CONNECT BY prior shop_id = parent_shop_id) )");
			params.add(filter.getShopRoot());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and est.code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getToStock())) {
			sql.append(" and (est.equip_stock_trans_form_id in ( ");
			sql.append("  select f.equip_stock_trans_form_id from equip_stock_trans_form f join equip_stock_trans_form_dtl fdt on f.equip_stock_trans_form_id = fdt.equip_stock_trans_form_id ");
			sql.append("  where fdt.to_stock_id in ( SELECT equip_stock_id from equip_stock where code like ?  ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getToStock().trim().toUpperCase()));
			sql.append(" ) ");
			sql.append(" or tsh.shop_code like ?  ESCAPE '/' or upper(tsh.name_text) like ?  ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getToStock().trim().toUpperCase()));
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getToStock().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getFromStock())) {
			sql.append(" and (est.equip_stock_trans_form_id in ( ");
			sql.append("  select f.equip_stock_trans_form_id from equip_stock_trans_form f join equip_stock_trans_form_dtl fdt on f.equip_stock_trans_form_id = fdt.equip_stock_trans_form_id ");
			sql.append("  where fdt.from_stock_id in ( SELECT equip_stock_id from equip_stock where code like ?  ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getFromStock().trim().toUpperCase()));
			sql.append(" ) ");
			sql.append(" or fsh.shop_code like ?  ESCAPE '/' or upper(fsh.name_text) like ?  ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getFromStock().trim().toUpperCase()));
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getFromStock().trim().toUpperCase()));
		}
		
		if (StringUtility.isNullOrEmpty(filter.getToStock()) && StringUtility.isNullOrEmpty(filter.getFromStock())) {
			sql.append(" and est.equip_stock_trans_form_id in ( ");
			sql.append("  select f.equip_stock_trans_form_id from equip_stock_trans_form f join equip_stock_trans_form_dtl fdt on f.equip_stock_trans_form_id = fdt.equip_stock_trans_form_id ");
			sql.append("  where (fdt.from_stock_id in ( SELECT s.equipStockId FROM   rstockfull s where 1 = 1 AND s.equipStockId in (select equip_stock_id from rshop) )");
			sql.append("  or fdt.to_stock_id in ( SELECT s.equipStockId FROM   rstockfull s where 1 = 1 AND s.equipStockId in (select equip_stock_id from rshop) ))");
			sql.append(" ) ");
		}
		
		if (filter.getToDate() != null) {
			sql.append("  and est.create_date < trunc(?) +1 ");
			params.add(filter.getToDate());
		}
		if (filter.getFromDate() != null) {
			sql.append("  and est.create_date >= trunc(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getCreateToDate() != null) {
			sql.append("  and est.create_form_date < trunc(?) +1 ");
			params.add(filter.getCreateToDate());
		}
		if (filter.getCreateFromDate() != null) {
			sql.append("  and est.create_form_date >= trunc(?)");
			params.add(filter.getCreateFromDate());
		}
		if (filter.getStatus() != null) {
			sql.append(" and est.status = ? ");
			params.add(filter.getStatus());
		}
		if (filter.getStatusPerform() != null) {
			sql.append(" and est.perform_status = ? ");
			params.add(filter.getStatusPerform());
		}
		String[] fieldNames = { "id", "code", "note", "creatDate", "createFormDate", "status", "performStatus", "fromStock", "toStock", "url" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
	
		

		sql.append(" group by est.equip_stock_trans_form_id,est.code, EST.NOTE, est.create_form_date, est.create_date, est.status, est.perform_status ");
		sql.append(" order by est.code desc");
		


		if (filter.getkPaging() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from ( ");
			countSql.append(sql);
			countSql.append(" ) ");
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public List<EquipmentVO> searchEquipmentStockChangeExcel(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select DISTINCT ");
		sql.append(" est.equip_stock_trans_form_id as id, ");
		sql.append(" est.code as code, ");
		sql.append(" est.note as note, ");
		sql.append(" to_char(est.create_form_date,'dd/mm/yyyy') as creatDate, ");
		sql.append(" est.status as status, ");
		sql.append(" est.perform_status as performStatus, ");
		sql.append(" e.code                                      AS equipCode, ");
		sql.append(" st.code                                     AS stockCode ");
		sql.append(" from equip_stock_trans_form est ");
		sql.append(" join equip_stock_trans_form_dtl dt on dt.equip_stock_trans_form_id = est.equip_stock_trans_form_id ");
		sql.append(" join equipment e on dt.equip_id = e.equip_id ");
		sql.append(" join equip_stock st on st.equip_stock_id = dt.to_stock_id ");
		sql.append(" where 1=1 ");
		if (filter.getShopRoot() != null) {
			sql.append(" and dt.from_stock_id IN (SELECT equip_stock_id from equip_stock  where shop_id in (  SELECT shop_id");
			sql.append("  FROM shop WHERE status               = 1  START WITH shop_id      IN (?) AND status                 = 1");
			sql.append("   CONNECT BY prior shop_id = parent_shop_id) )");
			params.add(filter.getShopRoot());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and est.code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getToStock())) {
			sql.append(" and est.equip_stock_trans_form_id in ( ");
			sql.append("  select f.equip_stock_trans_form_id from equip_stock_trans_form f join equip_stock_trans_form_dtl fdt on f.equip_stock_trans_form_id = fdt.equip_stock_trans_form_id ");
			sql.append("  where fdt.to_stock_id in ( SELECT equip_stock_id from equip_stock where code like ?  ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getToStock().trim().toUpperCase()));
			sql.append(" ) ");
		}
		if (!StringUtility.isNullOrEmpty(filter.getFromStock())) {
			sql.append(" and est.equip_stock_trans_form_id in ( ");
			sql.append("  select f.equip_stock_trans_form_id from equip_stock_trans_form f join equip_stock_trans_form_dtl fdt on f.equip_stock_trans_form_id = fdt.equip_stock_trans_form_id ");
			sql.append("  where fdt.from_stock_id in ( SELECT equip_stock_id from equip_stock where code like ?  ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getFromStock().trim().toUpperCase()));
			sql.append(" ) ");
		}
		if (filter.getToDate() != null) {
			sql.append("  and est.create_date < trunc(?) +1 ");
			params.add(filter.getToDate());
		}
		if (filter.getFromDate() != null) {
			sql.append("  and est.create_date >= trunc(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getCreateToDate() != null) {
			sql.append("  and est.create_form_date < trunc(?) +1 ");
			params.add(filter.getCreateToDate());
		}
		if (filter.getCreateFromDate() != null) {
			sql.append("  and est.create_form_date >= trunc(?)");
			params.add(filter.getCreateFromDate());
		}
		if (filter.getStatus() != null) {
			sql.append(" and est.status = ? ");
			params.add(filter.getStatus());
		}
		if (filter.getStatusPerform() != null) {
			sql.append(" and est.perform_status = ? ");
			params.add(filter.getStatusPerform());
		}
		sql.append(" order by est.code DESC ");
		
		String[] fieldNames = { "id", "code", "note", "creatDate", "status","performStatus","equipCode","stockCode" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipmentDeliveryVO> getListEquipmentStockVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder whereSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH res AS ");
		sql.append("  (SELECT es.equip_stock_id AS equipStockId, ");
		sql.append("  es.code                 AS equipStockCode,");
		sql.append("  es.name                 AS equipStockName,");
		sql.append("  es.shop_id              AS shopId,");
		sql.append("  erd.is_under            AS isUnder");
		sql.append("  FROM EQUIP_ROLE_USER eru");
		sql.append("  JOIN EQUIP_ROLE er");
		sql.append("  ON eru.EQUIP_ROLE_ID = er.EQUIP_ROLE_ID");
		sql.append("  JOIN EQUIP_ROLE_DETAIL erd");
		sql.append("  ON er.EQUIP_ROLE_ID = erd.EQUIP_ROLE_ID");
		sql.append("  JOIN EQUIP_STOCK es");
		sql.append("  ON erd.EQUIP_STOCK_ID = es.EQUIP_STOCK_ID and es.status = 1");
		sql.append("  WHERE eru.status      = 1");
		sql.append("  AND er.status         = 1");
		sql.append("  AND es.shop_id       IN");
		sql.append("    (SELECT shop_id");
		sql.append("    FROM shop");
		sql.append("    WHERE status               = 1");
		if (equipmentFilter.getShopRoot() != null) {
			sql.append("        START WITH shop_id       = ?");
			params.add(equipmentFilter.getShopRoot());
			sql.append("  		AND status                 = 1");
			sql.append("      CONNECT BY prior shop_id = parent_shop_id");
		}
		sql.append("    )");
		if (equipmentFilter.getStaffRoot() != null) {
			sql.append("  AND eru.user_id = ?");
			params.add(equipmentFilter.getStaffRoot());
		}
		sql.append("    )");
		sql.append("  ,");
		sql.append("  stock as ( ");
		sql.append("  SELECT DISTINCT equipStockId, equipStockCode,equipStockName,shopId");
		sql.append("  FROM (");
		sql.append("    (SELECT es.equip_stock_id AS equipStockId,");
		sql.append("     es.code                 AS equipStockCode,");
		sql.append("      es.name                 AS equipStockName,");
		sql.append("      es.shop_id              AS shopId,");
		sql.append("      1            AS isUnder");
//		sql.append("  	  FROM SHOP sh ");
		sql.append("  	  FROM (");
		sql.append("  	        select * from shop ");
		sql.append("  	        where 1=1");
		sql.append("  	       start with shop_id in (select shopId from res where isUnder = 1) and status = 1 ");
		sql.append("  	       connect by prior shop_id = parent_shop_id  ");
		sql.append("  	        ) sh ");
		sql.append("      JOIN EQUIP_STOCK es ON sh.shop_id = es.shop_id");
		sql.append("   Where 1 = 1 and es.status = 1 AND sh.status = 1 ");
//		sql.append("  AND sh.shop_id      IN");
//		sql.append("    (SELECT shop_id FROM shop WHERE status               = 1");
//		if (equipmentFilter.getShopRoot() != null) {
//			sql.append("    START WITH shop_id       = ?");
//			params.add(equipmentFilter.getShopRoot());
//			sql.append("    AND status                 = 1 CONNECT BY prior shop_id = parent_shop_id");
//		}
//		sql.append("  )");
//		sql.append("   START WITH sh.shop_id IN");
//		sql.append("  (SELECT shopId FROM res WHERE isUnder = 1) ");
//		sql.append("   AND sh.status                 = 1 CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sql.append("   MINUS (");
		sql.append("    SELECT es.equip_stock_id AS equipStockId,");
		sql.append("  es.code                 AS equipStockCode,");
		sql.append("  es.name                 AS equipStockName,");
		sql.append("  es.shop_id              AS shopId,");
		sql.append("  1            AS isUnder");
		sql.append("  FROM SHOP sh");
		sql.append("  JOIN EQUIP_STOCK es ON sh.shop_id = es.shop_id ");
		sql.append(" Where 1 = 1  and es.status = 1 ");
		sql.append(" AND sh.shop_id IN (SELECT shopId FROM res WHERE isUnder = 1) ");
		sql.append(" AND es.equip_stock_id NOT IN (SELECT equipStockId FROM res WHERE isUnder = 1) ");
//		sql.append("  AND es.equip_stock_id NOT IN (SELECT equipStockId FROM res)");
		sql.append("   )");
		sql.append("   )");
		sql.append("  UNION");
		sql.append("  	(SELECT * FROM res WHERE isUnder = 0  ) ) ORDER BY equipStockCode");
		sql.append("   ),");
		sql.append("  equipstock as (  ");
		sql.append("   SELECT s.equipStockId id,s.equipStockCode code, s.equipStockName name");
		sql.append("  from stock s");
		sql.append("    )");

		sql.append(" select DISTINCT eq.equip_id as equipmentId, ");
		sql.append(" eq.code as equipmentCode, ");
		sql.append(" eq.serial as seriNumber, ");
		sql.append(" (case when eq.health_status is not null then (select min(value) from ap_param ap where ap.status = 1 and ap.ap_param_code like eq.health_status and type like 'EQUIP_CONDITION')else null end) as healthStatus, ");
		/*
		 * sql.append(
		 * " (select sh.shop_code from shop sh where sh.equip_stock_code = eq.stock_code and sh.status = 1) as stockCode, "
		 * ); sql.append(
		 * " (select sh.shop_name from shop sh where sh.equip_stock_code = eq.stock_code and sh.status = 1) as stockName, "
		 * );
		 */
		sql.append(" sh.code as stockCode, sh.name as stockName, ");
		sql.append(" ec.name as typeEquipment, ");
		sql.append("(select code from equip_stock where equip_stock_id = eq.stock_id)||' - '|| (select name from equip_stock where equip_stock_id = eq.stock_id) stock,");
		sql.append(" eg.code as groupEquipmentCode, ");
		sql.append(" eg.name as groupEquipmentName, ");
		sql.append(" (case when eg.capacity_from is null and eg.capacity_to is null then '' when eg.capacity_from is null then '<'||to_char(eg.capacity_to) when eg.capacity_to is null then '>'||to_char(eg.capacity_from) ");
		sql.append(" when eg.capacity_to <> eg.capacity_from then to_char(eg.capacity_from) || ' - '|| to_char(eg.capacity_to) else to_char(eg.capacity_from) end) as capacity, ");
		sql.append(" eg.brand_name as equipmentBrand, ep.name as equipmentProvider, ");
		sql.append(" eq.manufacturing_year as year");
		sql.append(" from equipment eq ");
		sql.append(" join equipstock sh on sh.id = eq.stock_id");
		sql.append(" join equip_group eg on eg.equip_group_id = eq.equip_group_id and eg.status = 1");
		sql.append(" join equip_category ec on eg.equip_category_id = ec.equip_category_id ");
		sql.append(" join equip_provider ep on ep.equip_provider_id = eq.equip_provider_id ");
		sql.append(" where 1=1 ");
		sql.append(" and eq.usage_status = 2 ");
		sql.append("and trade_type is null");

		if (equipmentFilter.getLstEquipId() != null && equipmentFilter.getLstEquipId().size() > 0) {
			sql.append(" and eq.equip_id not in (-1");
			List<Long> lstP = equipmentFilter.getLstEquipId();
			for (Long pId : lstP) {
				sql.append(",?");
				params.add(pId);
			}
			sql.append(")");
		}
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getCode())) {
			whereSql.append(" and upper(eq.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipmentFilter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getSeriNumber())) {
			whereSql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipmentFilter.getSeriNumber().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getCategoryCode())) {
			whereSql.append(" and upper(ec.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipmentFilter.getCategoryCode().toUpperCase()));
		}
		if (equipmentFilter.getEquipCategoryId() != null) {
			whereSql.append(" and ec.equip_category_id = ? ");
			params.add(equipmentFilter.getEquipCategoryId());
		}
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getGroupCode())) {
			whereSql.append(" and upper(eg.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipmentFilter.getGroupCode().toUpperCase()));
		}
		if (equipmentFilter.getEquipGroupId() != null) {
			whereSql.append(" and eg.equip_group_id = ? ");
			params.add(equipmentFilter.getEquipGroupId());
		}
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getProviderCode())) {
			whereSql.append(" and upper(ep.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipmentFilter.getProviderCode().toUpperCase()));
		}
		if (equipmentFilter.getEquipProviderId() != null) {
			whereSql.append(" and ep.equip_provider_id = ? ");
			params.add(equipmentFilter.getEquipProviderId());
		}
		if (equipmentFilter.getYearManufacture() != null) {
			whereSql.append(" and eq.manufacturing_year = ? ");
			params.add(equipmentFilter.getYearManufacture());
		}
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getStockCode())) {
			whereSql.append(" AND eq.stock_id in (select equip_stock_id from equip_stock where code like ? ESCAPE '/') ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipmentFilter.getStockCode().toUpperCase()));
		}
		if (equipmentFilter.getStockType() != null) {
			whereSql.append(" and eq.stock_type = ? ");
			params.add(equipmentFilter.getStockType());
		} else if (equipmentFilter.getIdRecordDelivery() == null) {
			whereSql.append(" and eq.stock_type != ? ");
			params.add(EquipStockTotalType.KHO_KH.getValue());
		}
		if (equipmentFilter.getStatusEquip() != null) {
			whereSql.append(" and eq.status = ?");
			params.add(equipmentFilter.getStatusEquip());
		} else {
			whereSql.append(" and eq.status = ?");
			params.add(ActiveType.RUNNING.getValue());
		}
		if (equipmentFilter.getTradeStatus() != null) {
			whereSql.append(" and eq.trade_status = ?");
			params.add(equipmentFilter.getTradeStatus());
		}
		if (equipmentFilter.getUsageStatus() != null) {
			whereSql.append(" and eq.usage_status = ?");
			params.add(equipmentFilter.getUsageStatus());
		}
		String sqlTemp = sql.toString();
		sql.append(whereSql);
//		if (equipmentFilter.getLstEquipDelete() != null) {
//			sql.append(" union ");
//			sql.append(sqlTemp);
//			if (equipmentFilter.getLstEquipAdd() != null) {
//				for (int i = 0; i < equipmentFilter.getLstEquipAdd().size(); i++) {
//					params.add(equipmentFilter.getLstEquipAdd().get(i));
//				}
//			}
//			String code = "'-1'";
//			for (int i = 0; i < equipmentFilter.getLstEquipDelete().size(); i++) {
//				code += ",?";
//			}
//			sql.append(" and eq.code in ( " + code + " ) ");
//			for (int i = 0; i < equipmentFilter.getLstEquipDelete().size(); i++) {
//				params.add(equipmentFilter.getLstEquipDelete().get(i));
//			}
//		}
		if (equipmentFilter.getLstEquipDelete() != null) {
			sql.append(" union ");
			sql.append(" select DISTINCT eq.equip_id as equipmentId, ");
			sql.append(" eq.code as equipmentCode, ");
			sql.append(" eq.serial as seriNumber, ");
			sql.append(" (case when eq.health_status is not null then (select min(value) from ap_param ap where ap.status = 1 and ap.ap_param_code like eq.health_status and type like 'EQUIP_CONDITION')else '' end) as healthStatus, ");
			sql.append(" sh.code as stockCode, sh.name as stockName, ");
			sql.append(" ec.name as typeEquipment, ");
			sql.append("(select code from equip_stock where equip_stock_id = eq.stock_id)||' - '|| (select name from equip_stock where equip_stock_id = eq.stock_id) stock,");
			sql.append(" eg.code as groupEquipmentCode, ");
			sql.append(" eg.name as groupEquipmentName, ");
			sql.append(" (case when eg.capacity_from is null and eg.capacity_to is null then '' when eg.capacity_from is null then '<'||to_char(eg.capacity_to) when eg.capacity_to is null then '>'||to_char(eg.capacity_from) ");
			sql.append(" when eg.capacity_to <> eg.capacity_from then to_char(eg.capacity_from) || ' - '|| to_char(eg.capacity_to) else to_char(eg.capacity_from) end) as capacity, ");
			sql.append(" eg.brand_name as equipmentBrand, ep.name as equipmentProvider, ");
			sql.append(" eq.manufacturing_year as year");
			sql.append(" from equipment eq ");
			sql.append(" join equipstock sh on sh.id = eq.stock_id");
			sql.append(" join equip_group eg on eg.equip_group_id = eq.equip_group_id and eg.status = 1");
			sql.append(" join equip_category ec on eg.equip_category_id = ec.equip_category_id ");
			sql.append(" join equip_provider ep on ep.equip_provider_id = eq.equip_provider_id ");
			sql.append(" where 1=1 ");
			sql.append(" and eq.usage_status = 2 ");
			String code = "'-1'";
			for (int i = 0; i < equipmentFilter.getLstEquipDelete().size(); i++) {
				code += ",?";
			}
			sql.append(" and eq.code in ( " + code + " ) ");
			for (int i = 0; i < equipmentFilter.getLstEquipDelete().size(); i++) {
				params.add(equipmentFilter.getLstEquipDelete().get(i));
			}
		}
		sql.append(" ORDER BY equipmentCode ASC  ");

		String[] fieldNames = { "equipmentId",// 0
				"equipmentCode",// 1
				"seriNumber",// 2
				"healthStatus",// 3
				"stockCode",// 4
				"stockName",// 5
				"typeEquipment",// 6
				"groupEquipmentCode",// 7
				"groupEquipmentName",// 8
				"capacity",// 9
				"equipmentBrand",// 10
				"equipmentProvider",// 11		
				"stock",// 12
				"year",// 13
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.STRING,// 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.INTEGER // 13
		};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");

		if (equipmentFilter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, equipmentFilter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<EquipmentVO> getListEquipToStock(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		sql.append(" select  ");
		sql.append(" shop_id as shopId, ");
		sql.append(" shop_code ||' - '||shop_name kho, ");
		sql.append(" shop_code as shopCode, ");
		sql.append(" shop_name as shopName, ");
		sql.append(" status as status, ");
		sql.append(" parent_shop_id as parentId ");
		sql.append(" from shop sh ");
		sql.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and lower(s.shop_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and lower(s.shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getShopName().trim().toLowerCase())));
		}
		countSql.append("  select count(*) as count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");

		final String[] fieldNames = new String[] { "shopId", "kho", "shopCode", "shopName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };

		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	@Override
	public List<EquipmentVO> getListEquipGroupById(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select eq.equip_group_id as id , eq.code as code, eq.name as name, eq.status as status from equip_group eq");
		sql.append(" where 1=1 ");
		if (filter.getId() != null && filter.getId() > 0) {
			sql.append("and eq.equip_category_id  =? ");
			params.add(filter.getId());
		}
		final String[] fieldNames = new String[] { "id", "code", "name", "status" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipmentVO> listEquipCategoryCode(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select  code from equip_category ");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(code) like ? ");
			params.add(filter.getCode().trim().toLowerCase());
		}
		final String[] fieldNames = new String[] { "code", };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipmentVO> listEquipEquipProvider(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select  code from equip_provider ");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(code) like ? ");
			params.add(filter.getCode().trim().toLowerCase());
		}
		final String[] fieldNames = new String[] { "code", };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipmentVO> listEquipBrand(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select  code from equip_brand ");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(code) like ? ");
			params.add(filter.getCode().trim().toLowerCase());
		}
		final String[] fieldNames = new String[] { "code", };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipmentVO> listEquipItem(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select  code from equip_item ");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(code) like ? ");
			params.add(filter.getCode().trim().toLowerCase());
		}
		final String[] fieldNames = new String[] { "code", };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipmentVO> getListEquipGroupByIdEquipProvider(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select eq.equip_id as id , eq.code as code, eq.status as status from equipment eq ");
		sql.append(" where 1=1 ");
		if (filter.getId() != null && filter.getId() > 0) {
			sql.append(" and eq.equip_provider_id = ? ");
			params.add(filter.getId());
		}
		final String[] fieldNames = new String[] { "id", "code", "status" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipmentVO> listEquipItemById(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select er.equip_repair_form_dtl_id as id, er.equip_repair_form_id as equiprepair, er.equip_item_id as equipitem from equip_repair_form_dtl er ");
		sql.append(" where 1=1 ");
		if (filter.getId() != null && filter.getId() > 0) {
			sql.append(" and er.equip_item_id = ? ");
			params.add(filter.getId());
		}
		final String[] fieldNames = new String[] { "id", "equiprepair", "equipitem" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/**
	 * lay doanh so binh quan theo thang cua khach hang sua chua thang, nganh hang (cau hinh) gan nhat theo rpt_sale_in_month
	 * @author vuongmq
	 * @since 14/04/2015
	 * @return
	 */
	@Override
	public Object getDoanhSoEquipRepair(EquipRepairFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select NVL(sum(rptm.amount),0) as amount ");
		sql.append(" from rpt_sale_in_month rptm ");
		sql.append(" join product pd on pd.product_id = rptm.product_id ");
		sql.append(" join product_info pi on pd.cat_id = pi.product_info_id ");
		sql.append(" where 1= 1 ");
		sql.append(" and rptm.status = 1 ");
		if (filter.getCustomerId() != null) {
			sql.append(" and rptm.customer_id = ? ");
			params.add(filter.getCustomerId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getNganhHangStr())) {
			sql.append(" AND (? is null ");
			sql.append(" or pi.product_info_code in (SELECT trim(regexp_substr(?,'[^,]+', 1, level)) ");
			sql.append(" 				from dual CONNECT BY regexp_substr(?, '[^,]+', 1, level) IS NOT NULL ");
			sql.append(" 					) ");
			sql.append(" ) ");
			params.add(filter.getNganhHangStr());
			params.add(filter.getNganhHangStr());
			params.add(filter.getNganhHangStr());
		}
		if (filter.getSoThang() != null) {
			sql.append(" and EXTRACT(MONTH from rptm.month) >= (select EXTRACT( MONTH FROM sysdate) - ? from dual) ");
			params.add(filter.getSoThang());
			sql.append(" and EXTRACT(MONTH from rptm.month) < (select EXTRACT( MONTH FROM sysdate) from dual) ");
		}
		return repo.getObjectByQuery(sql.toString(), params);
		//return repo.getEntityBySQL(EquipItemConfigDtl.class, sql.toString(), params);
	}
	
	/**
	 * Lay stock code - name ben EquipStock voi stock_id; cua kho don vi
	 * @author vuongmq
	 * @since 23/04/2015
	 * @return
	 */
	@Override
	public EquipStock getEquipStockByFilter(EquipStockEquipFilter<EquipStock> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from Equip_Stock est ");
		sql.append(" where 1 = 1 ");
		if (filter.getStatus() != null) {
			sql.append(" and est.status = ? ");
			params.add(filter.getStatus());
		}
		if (filter.getStockId() != null) {
			sql.append(" and est.Equip_Stock_id = ? ");
			params.add(filter.getStockId());
		}
		return repo.getEntityBySQL(EquipStock.class, sql.toString(), params);
	}
	
	/**
	 * danh sach hang muc cau hinh
	 * @author vuongmq
	 * @since 14/04/2015
	 * @return
	 */
	@Override
	public EquipItemConfigDtl getEquipItemConfigDtl(EquipItemFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_item_config_dtl ec ");
		sql.append(" where 1 = 1 ");
		if (filter.getStatus() != null) {
			sql.append(" and ec.status = ? ");
			params.add(filter.getStatus().getValue());
		}
		if (filter.getEquipId() != null) {
			sql.append(" and ec.equip_item_id = ? ");
			params.add(filter.getEquipId());
		}
		return repo.getEntityBySQL(EquipItemConfigDtl.class, sql.toString(), params);
	}
	
	/***
	 * @author vuongmq
	 * @date 22/04/2015
	 * lay danh sach theo in; giong giong nhu getListEquipRepair; nhung them mot so cot
	 * them cot: doanhSoTB, tenNPP, tenMien
	 */
	@Override
	public List<EquipRepairFormVOList> getListEquipRepairPrint(EquipRepairFilter filter) throws DataAccessException {
		if (filter.getShopRoot() == null) {
			throw new IllegalArgumentException("shopRoot not id");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/**** select VO ***/
		sql.append(" SELECT f.equip_repair_form_id id, ");
		sql.append(" f.equip_repair_form_code AS maPhieu, ");
		sql.append(" ec.name AS loaiThietBi, ");
		//sql.append(" (eg.code || '-' || eg.name) AS nhomThietBi, ");
		sql.append(" eg.name AS nhomThietBi, "); // vuongmq; 28/05/2015; chi lay ten
		sql.append(" e.code AS maThietBi, ");
		sql.append(" e.serial AS soSeri, ");
		/** end soseri */
		sql.append(" (CASE ");
		sql.append(" WHEN f.stock_type = 1 ");
		sql.append(" THEN ");
		sql.append(" (SELECT es.code || ' - ' || es.name ");
		sql.append(" FROM equip_stock es ");
		sql.append(" WHERE es.equip_stock_id = f.stock_id ");
		sql.append(" ) ");
		sql.append(" ELSE ");
		sql.append(" (SELECT sh.shop_code ||c.short_code || '-' || c.customer_name ");
		sql.append(" FROM customer c ");
		sql.append(" JOIN shop sh ON sh.shop_id = c.shop_id ");
		sql.append(" WHERE c.customer_id = f.stock_id ");
		sql.append(" ) ");
		sql.append(" END ) AS kho, ");
		/** end Kho*/
		sql.append(" TO_CHAR(f.create_date,'dd/mm/yyyy') ngayTao, ");
		sql.append(" f.status trangThai, ");
		sql.append(" f.condition AS tinhTrangHuHong, ");
		sql.append(" f.reason lyDoDeNghi, ");
		sql.append(" f.total_amount tongTien, ");
		sql.append(" f.reject_reason lyDoTuChoi, ");
//		sql.append(" DECODE(ep.status,2,1,0) AS kyMo , ");
		/*sql.append(" ( case when exists (  select 1 ");
		sql.append("                       from equip_repair_pay_form erpfm ");
		sql.append("                       where erpfm.equip_repair_form_id = f.equip_repair_form_id ");
		sql.append("                       and exists (select 1 from equip_period epd where epd.equip_period_id = erpfm.equip_period_id and epd.status = 2) ");
		sql.append("                 ) ");
		sql.append("     then 1 ");
		sql.append("     else 0 ");
		sql.append("     end ");
		sql.append("   ) as paymentPeriodOpen, ");
		sql.append("   (case when exists (select 1 from equip_repair_pay_form pf where pf.equip_repair_form_id = f.equip_repair_form_id and f.status = ?) then 1 else 0 end) as daThanhToan, ");
		params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());*/
		sql.append(" f.payment_status AS daThanhToan, ");
		/** end thanhToan*/
		sql.append(" f.amount AS doanhSoTB, ");
		/** vuongmq; 28/05/2015; begin cap nhat lay them */
		sql.append(" NVL(f.worker_price,0) as giaNhanCong, ");
		/** end giaNhanCong */
		//sql.append(" to_char(e.first_date_in_use,'dd/mm/yyyy') as capNam, ");
		sql.append(" to_char(e.first_date_in_use,'yyyy') as capNam, ");
		/** end capNam */
		sql.append(" (CASE WHEN f.stock_type = 1 ");
		sql.append(" THEN null ");
		sql.append(" ELSE ");
		sql.append(" (SELECT c.short_code ");
		sql.append(" FROM customer c ");
		sql.append(" WHERE c.customer_id = f.stock_id ) ");
		sql.append(" END ) AS maKH, ");
		/** end maKH */
		sql.append(" (CASE WHEN f.stock_type = 1 ");
		sql.append(" THEN null ");
		sql.append(" ELSE ");
		sql.append(" (SELECT c.customer_name ");
		sql.append(" FROM customer c ");
		sql.append(" WHERE c.customer_id = f.stock_id ) ");
		sql.append(" END ) AS khachHang, ");
		/** end khachHang */
		sql.append(" (CASE WHEN f.stock_type = 1 ");
		sql.append(" THEN ");
		sql.append(" (SELECT sh.address ");
		sql.append(" FROM shop sh ");
		sql.append(" WHERE sh.shop_id IN ");
		sql.append(" (SELECT est.shop_id ");
		sql.append(" FROM equip_stock est ");
		sql.append(" WHERE est.equip_stock_id = f.stock_id ");
		sql.append(" ) ");
		sql.append(" ) ");
		sql.append(" ELSE ");
		sql.append(" (SELECT c.address ");
		sql.append(" FROM customer c ");
		sql.append(" WHERE c.customer_id = f.stock_id ) ");
		sql.append(" END ) AS diaChi, ");
		/** end diaChi */
		sql.append(" (CASE WHEN f.stock_type = 1 ");
		sql.append(" THEN ");
		sql.append(" (SELECT (CASE WHEN sh.mobiphone IS NOT NULL AND sh.phone IS NOT NULL THEN sh.mobiphone || ' - ' || sh.phone ");
		sql.append(" WHEN sh.mobiphone IS NOT NULL AND sh.phone IS NULL THEN sh.mobiphone ");
		sql.append(" WHEN sh.mobiphone IS NULL AND sh.phone IS NOT NULL THEN sh.phone ");
		sql.append(" else null ");
		sql.append(" END) ");
		sql.append(" FROM shop sh ");
		sql.append(" WHERE sh.shop_id IN ");
		sql.append(" (SELECT est.shop_id ");
		sql.append(" FROM equip_stock est ");
		sql.append(" WHERE est.equip_stock_id = f.stock_id ");
		sql.append(" ) ");
		sql.append(" ) ");
		sql.append(" ELSE ");
		sql.append(" (SELECT ");
		sql.append(" (CASE WHEN c.mobiphone IS NOT NULL AND c.phone IS NOT NULL THEN c.mobiphone || ' - ' || c.phone ");
		sql.append(" WHEN c.mobiphone IS NOT NULL AND c.phone IS NULL THEN c.mobiphone ");
		sql.append(" WHEN c.mobiphone IS NULL AND c.phone IS NOT NULL THEN c.phone ");
		sql.append(" else null ");
		sql.append(" END) ");
		sql.append(" FROM customer c ");
		sql.append(" WHERE c.customer_id = f.stock_id ) ");
		sql.append(" END ) AS dienThoai, ");
		/** end dienThoai */
		sql.append(" (CASE WHEN f.stock_type = 1 ");
		sql.append(" THEN null ");
		sql.append(" else ");
		sql.append(" (SELECT contract_number ");
		sql.append(" FROM ");
		sql.append(" (SELECT dtl.equip_id , ");
		sql.append(" dtl.equip_delivery_record_id , ");
		sql.append(" hr.contract_number , ");
		sql.append(" hr.record_status , ");
		sql.append(" row_number() over (partition BY dtl.equip_id order by hr.create_date DESC) AS rn ");
		sql.append(" FROM equip_delivery_record hr ");
		sql.append(" JOIN equip_delivery_rec_dtl dtl ");
		sql.append(" ON hr.equip_delivery_record_id = dtl.equip_delivery_record_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" ) dta ");
		sql.append(" WHERE 1 =1 ");
		sql.append(" and dta.rn = 1 ");
		sql.append(" and dta.equip_id = f.equip_id ) ");
		sql.append(" END ) as contractNumber, ");
		/** end contractNumber */
		/** vuongmq; 28/05/2015; end cap nhat lay them */
		sql.append(" ( CASE ");
		sql.append(" WHEN f.stock_type = 1 ");
		sql.append(" THEN ( ");
		sql.append(" select to_char(sh.shop_name) from shop sh ");
		sql.append(" where sh.shop_id in ");
		sql.append(" (SELECT est.shop_id from equip_stock est where est.equip_stock_id = f.stock_id) ");
		sql.append(" ) ELSE ");
		sql.append(" (select to_char(s.shop_name) ");
		sql.append(" from shop s ");
		sql.append(" where s.shop_id in (SELECT c.shop_id from customer c where c.customer_id = f.stock_id) ");
		sql.append(" ) ");
		sql.append(" END ) AS tenNPP, ");
		sql.append(" ( CASE ");
		sql.append(" WHEN f.stock_type = 1 ");
		sql.append(" THEN ( ");
		sql.append(" select to_char(sh.shop_name) from shop sh ");
		sql.append(" where sh.shop_type_id = (SELECT channel_type_id ");
		sql.append(" FROM channel_type ");
		sql.append(" WHERE status = 1 ");
		sql.append(" AND type = 1 ");
		sql.append(" AND object_type = 1) ");
		sql.append(" START WITH shop_id in (SELECT est.shop_id from equip_stock est where est.equip_stock_id = f.stock_id) ");
		sql.append(" CONNECT BY PRIOR parent_shop_id = shop_id ");
		sql.append(" ) ELSE ");
		sql.append(" (select to_char(s.shop_name) ");
		sql.append(" from shop s ");
		sql.append(" where s.shop_type_id = (SELECT channel_type_id ");
		sql.append(" FROM channel_type ");
		sql.append(" WHERE status = 1 ");
		sql.append(" AND type = 1 ");
		sql.append(" AND object_type = 1) ");
		sql.append(" START WITH shop_id in (SELECT c.shop_id from customer c where c.customer_id = f.stock_id) ");
		sql.append(" CONNECT BY PRIOR parent_shop_id = shop_id ");
		sql.append(" ) ");
		sql.append(" END ) AS tenMien ");
		/*** from table **/
		sql.append("  from  equip_repair_form f");
		sql.append("   join equipment e on f.equip_id = e.equip_id  ");
		sql.append("   join equip_group eg on eg.equip_group_id = e.equip_group_id");
		sql.append("   join equip_category ec on ec.equip_category_id = eg.equip_category_id ");
//		sql.append("   join equip_period ep on ep.equip_period_id = f.equip_period_id  ");
		sql.append(" where 1=1");
		if (!StringUtility.isNullOrEmpty(filter.getFormCode())) {
			sql.append(" and f.equip_repair_form_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getFormCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getEuqipCode())) {
			sql.append(" and e.code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEuqipCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeri())) {
			sql.append(" and upper(e.serial) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeri().toUpperCase()));
		}
		if (filter.getFromDate() != null) {
			sql.append(" and f.create_date >=trunc(?)  ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and f.create_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if (filter.getStatus() != null) {
			sql.append(" and f.status =  ?");
			params.add(filter.getStatus());
		}
		if (filter.getStatusPayment() != null && filter.getStatusPayment() > -1) {
			/*if (filter.getStatusPayment().intValue() == 1) {//da thanh toan
				sql.append(" and  exists (select 1 from equip_repair_pay_form pf where pf.equip_repair_form_id = f.equip_repair_form_id and f.status = ?)");
			} else {
				sql.append(" and not exists (select 1 from equip_repair_pay_form pf where pf.equip_repair_form_id = f.equip_repair_form_id) and f.status = ?");
			}
			params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());*/
			sql.append(" and f.payment_status = ? ");
			params.add(filter.getStatusPayment());
		}
		if (filter.getEquipCategoryId() != null && filter.getEquipCategoryId() > -1) {
			sql.append(" and ec.equip_category_id = ?");
			params.add(filter.getEquipCategoryId());
		}
		
		if (filter.getEquipGroupId() != null && filter.getEquipGroupId() > -1) {
			sql.append(" and eg.equip_group_id = ?");
			params.add(filter.getEquipGroupId());
		}
		if (filter.getEquipRepairId() != null) {
			sql.append(" and f.equip_repair_form_id = ? ");
			params.add(filter.getEquipRepairId());
		}
		if (filter.getLstId() != null) {
			sql.append(" and f.equip_repair_form_id in (-1 ");
			for (Long id : filter.getLstId()) {
				if (id != null) {
					sql.append(" ,?");
					params.add(id);
				}
			}
			sql.append(" )");
		}
		/** vuongmq; 28/05/2015; truong hop in khong can su dung dieu kien nay cung duoc*/
		sql.append(" AND ( "); /** begin and*/
		sql.append("   (f.stock_id IN (SELECT equip_stock_id from equip_stock where shop_id in ( ");
		sql.append(" SELECT shop_id ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status = 1 ");
		sql.append(" START WITH shop_id IN (?) ");
		params.add(filter.getShopRoot());
		sql.append(" AND status = 1 ");
		sql.append(" CONNECT BY prior shop_id = parent_shop_id) ");
		sql.append(" ) ");
		sql.append(" AND f.STOCK_TYPE = 1) ");
		/** end kho NPP */
		sql.append(" OR ( f.STOCK_ID IN (SELECT cus.customer_id ");
		sql.append(" FROM customer cus ");
		sql.append(" WHERE cus.shop_id IN ");
		sql.append(" (SELECT shop_id ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status = 1 ");
		sql.append(" START WITH shop_id IN (?) ");
		params.add(filter.getShopRoot());
		sql.append(" AND status = 1 ");
		sql.append(" CONNECT BY prior shop_id = parent_shop_id) ");
		sql.append(" ) ");
		sql.append(" AND f.STOCK_TYPE = 2) ");
		/** end kho KH */
		sql.append(" ) "); /** end and*/
		sql.append(" order by f.equip_repair_form_code desc,e.code");

		String[] fieldNames = new String[] { "id", //1
				"maPhieu", "loaiThietBi", "nhomThietBi", "maThietBi", "soSeri", "kho", "ngayTao",//8 
				"trangThai", "tinhTrangHuHong", "lyDoDeNghi", "tongTien", //12
				"lyDoTuChoi",
				/*"kyMo",*/ 
				/*"paymentPeriodOpen",*/ 
				"daThanhToan", //16
				"doanhSoTB", // 17
				"giaNhanCong", //17.1
				"capNam", //18
				"maKH", //19
				"khachHang", //20
				"diaChi", //21
				"dienThoai", //22
				"contractNumber", //23
				"tenNPP", "tenMien"	}; // 25
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,//8 
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, //12
				StandardBasicTypes.STRING, 
				/*StandardBasicTypes.INTEGER,*/
				/*StandardBasicTypes.INTEGER,*/ 
				StandardBasicTypes.INTEGER, //16
				StandardBasicTypes.BIG_DECIMAL,  //17
				StandardBasicTypes.BIG_DECIMAL,  //17.1
				StandardBasicTypes.STRING, //18
				StandardBasicTypes.STRING, //19
				StandardBasicTypes.STRING, //20
				StandardBasicTypes.STRING, //21
				StandardBasicTypes.STRING, //22
				StandardBasicTypes.STRING, //23
				StandardBasicTypes.STRING, StandardBasicTypes.STRING}; //25
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipRepairFormVOList.class, fieldNames, fieldTypes, sql.toString().replace("  ", " "), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString().replace("  ", " "));
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(EquipRepairFormVOList.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVOlist());
		}
	}
	
	/***
	 * @author vuongmq
	 * @date 22/04/2015
	 * ----cap nhat: lay lai theo phan quyen nhieu kho
	 * cap nhat: 25/05/2015 lay theo phan quyen CMS; khong lay theo quuyen kho khi search
	 * dung chung search Ql sua chua va export sua chua
	 */
	@Override
	public List<EquipRepairFormVO> getListEquipRepair(EquipRepairFilter filter) throws DataAccessException {
		if (filter.getShopRoot() == null) {
			throw new IllegalArgumentException("shopRoot not id");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/**** select VO ***/
		sql.append(" SELECT f.equip_repair_form_id id, ");
		sql.append(" f.equip_repair_form_code AS maPhieu, ");
		sql.append(" ec.name AS loaiThietBi, ");
		sql.append(" (eg.code || '-' || eg.name) AS nhomThietBi, ");
		sql.append(" e.code AS maThietBi, ");
		sql.append(" e.serial AS soSeri, ");
		/** end soseri */
		sql.append(" (CASE ");
		sql.append(" WHEN f.stock_type = 1 ");
		sql.append(" THEN ");
		sql.append(" (SELECT es.code || ' - ' || es.name ");
		sql.append(" FROM equip_stock es ");
		sql.append(" WHERE es.equip_stock_id = f.stock_id ");
		sql.append(" ) ");
		sql.append(" ELSE ");
		sql.append(" (SELECT sh.shop_code ||c.short_code || '-' || c.customer_name ");
		sql.append(" FROM customer c ");
		sql.append(" JOIN shop sh ON sh.shop_id = c.shop_id ");
		sql.append(" WHERE c.customer_id = f.stock_id ");
		sql.append(" ) ");
		sql.append(" END ) AS kho, ");
		/** end Kho*/
//		sql.append(" TO_CHAR(f.create_date,'dd/mm/yyyy') ngayTao, ");
		//tamvnm: thay doi thanh ngay tao
		sql.append(" TO_CHAR(f.create_form_date,'dd/mm/yyyy') ngayTao, ");
		sql.append(" f.status trangThai, ");
		sql.append(" f.condition AS tinhTrangHuHong, ");
		sql.append(" f.reason lyDoDeNghi, ");
		sql.append(" f.total_amount tongTien, ");
		sql.append(" f.reject_reason lyDoTuChoi, ");
//		sql.append(" DECODE(ep.status,2,1,0) AS kyMo , ");
		/*sql.append(" ( case when exists (  select 1 ");
		sql.append("                       from equip_repair_pay_form erpfm ");
		sql.append("                       where erpfm.equip_repair_form_id = f.equip_repair_form_id ");
		sql.append("                       and exists (select 1 from equip_period epd where epd.equip_period_id = erpfm.equip_period_id and epd.status = 2) ");
		sql.append("                 ) ");
		sql.append("     then 1 ");
		sql.append("     else 0 ");
		sql.append("     end ");
		sql.append("   ) as paymentPeriodOpen, ");
		sql.append("   (case when exists (select 1 from equip_repair_pay_form pf where pf.equip_repair_form_id = f.equip_repair_form_id and f.status = ?) then 1 else 0 end) as daThanhToan ");
		params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());*/
		sql.append(" f.payment_status AS daThanhToan, ");
		sql.append(" f.CREATE_FORM_DATE AS createFormDate, ");
		sql.append(" f.note as note, ");
		sql.append(" (select eqat.url from equip_attach_file eqat where eqat.object_id = f.equip_repair_form_id and rownum = 1 and eqat.object_type = 2) as url ");
		/** from table */
		sql.append("  from  equip_repair_form f");
		sql.append("   join equipment e on f.equip_id = e.equip_id  ");
		sql.append("   join equip_group eg on eg.equip_group_id = e.equip_group_id");
		sql.append("   join equip_category ec on ec.equip_category_id = eg.equip_category_id ");
//		sql.append("   join equip_period ep on ep.equip_period_id = f.equip_period_id  ");
		sql.append(" where 1=1");
		if (!StringUtility.isNullOrEmpty(filter.getFormCode())) {
			sql.append(" and f.equip_repair_form_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getFormCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getEuqipCode())) {
			sql.append(" and e.code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEuqipCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeri())) {
			sql.append(" and upper(e.serial) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeri().toUpperCase()));
		}
//		if (filter.getFromDate() != null) {
//			sql.append(" and f.create_date >=trunc(?)  ");
//			params.add(filter.getFromDate());
//		}
//		if (filter.getToDate() != null) {
//			sql.append(" and f.create_date < trunc(?) + 1 ");
//			params.add(filter.getToDate());
//		}
		//tamvnm: 13/07/2015 - Thay doi tim kiem theo ngay lap
		if (filter.getFromDate() != null) {
			sql.append(" and f.create_form_date >=trunc(?)  ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and f.create_form_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if (filter.getStatus() != null) {
			sql.append(" and f.status =  ?");
			params.add(filter.getStatus());
		}
		if (filter.getStatusPayment() != null && filter.getStatusPayment() > -1) {
			/*if (filter.getStatusPayment().intValue() == 1) {//da thanh toan
				sql.append(" and  exists (select 1 from equip_repair_pay_form pf where pf.equip_repair_form_id = f.equip_repair_form_id and f.status = ?)");
			} else {
				sql.append(" and not exists (select 1 from equip_repair_pay_form pf where pf.equip_repair_form_id = f.equip_repair_form_id) and f.status = ?");
			}
			params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());*/
			sql.append(" and f.payment_status = ? ");
			params.add(filter.getStatusPayment());
		}
		if (filter.getEquipCategoryId() != null && filter.getEquipCategoryId() > -1) {
			sql.append(" and ec.equip_category_id = ?");
			params.add(filter.getEquipCategoryId());
		}
		//tamvnm: thay doi thanh autoCompleteCombobox: -2 la truong hop tim group ko co trong BD
		if (filter.getEquipGroupId() != null && (filter.getEquipGroupId() > -1 || filter.getEquipGroupId().longValue() == -2)) {
			sql.append(" and eg.equip_group_id = ?");
			params.add(filter.getEquipGroupId());
		}
		if (filter.getEquipRepairId() != null) {
			sql.append(" and f.equip_repair_form_id = ? ");
			params.add(filter.getEquipRepairId());
		}
		if (filter.getLstId() != null) {
			sql.append(" and f.equip_repair_form_id in (-1 ");
			for (Long id : filter.getLstId()) {
				if (id != null) {
					sql.append(" ,?");
					params.add(id);
				}
			}
			sql.append(" )");
		}
		sql.append(" AND ( "); /** begin and*/
		sql.append("   (f.stock_id IN (SELECT equip_stock_id from equip_stock where shop_id in ( ");
		sql.append(" SELECT shop_id ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status = 1 ");
		if (StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "shop_id");
			sql.append(paramsFix);
		}
		sql.append(" START WITH shop_id IN (?) ");
		params.add(filter.getShopRoot());
		sql.append(" AND status = 1 ");
		sql.append(" CONNECT BY prior shop_id = parent_shop_id) ");
		sql.append(" ) ");
		sql.append(" AND f.STOCK_TYPE = 1) ");
		/** end kho NPP */
		sql.append(" OR ( f.STOCK_ID IN (SELECT cus.customer_id ");
		sql.append(" FROM customer cus ");
		sql.append(" WHERE cus.shop_id IN ");
		sql.append(" (SELECT shop_id ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status = 1 ");
		if (StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "shop_id");
			sql.append(paramsFix);
		}
		sql.append(" START WITH shop_id IN (?) ");
		params.add(filter.getShopRoot());
		sql.append(" AND status = 1 ");
		sql.append(" CONNECT BY prior shop_id = parent_shop_id) ");
		sql.append(" ) ");
		sql.append(" AND f.STOCK_TYPE = 2) ");
		/** end kho KH */
		sql.append(" ) "); /** end and*/
		
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql.toString().replace("  ", " "));
		countSql.append(")");
		
		sql.append(" order by f.equip_repair_form_code desc, e.code ");

		String[] fieldNames = new String[] { "id", //1
				"maPhieu", "loaiThietBi", "nhomThietBi", "maThietBi", "soSeri", "kho", "ngayTao",//8 
				"trangThai", "tinhTrangHuHong", "lyDoDeNghi", "tongTien", //12
				"lyDoTuChoi", 
				/*"kyMo",*/ 
				/*"paymentPeriodOpen",*/ 
				"daThanhToan", "createFormDate", "note",
				"url" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,//8 
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, //12
				StandardBasicTypes.STRING, 
				/*StandardBasicTypes.INTEGER,*/ 
				/*StandardBasicTypes.INTEGER,*/ 
				StandardBasicTypes.INTEGER, StandardBasicTypes.DATE, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING };
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipRepairFormVO.class, fieldNames, fieldTypes, sql.toString().replace("  ", " "), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(EquipRepairFormVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}


	/**
	 * Lay danh sach chi tiet hang muc tren man hinh cap nhat phieu sua chua; man hinh pe duyet sua chua
	 * @author vuongmq
	 * @since 18/04/2015
	 * @return
	 */
	@Override
	public List<EquipRepairFormDtl> getListEquipRepairDtlByEquipRepairId(EquipRepairFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_repair_form_dtl ");
		sql.append(" where 1 = 1 ");
		if (filter.getEquipRepairId() != null) {
			sql.append(" and equip_repair_form_id = ? ");
			params.add(filter.getEquipRepairId());
		}
		sql.append(" order by equip_repair_form_dtl_id ");
		if (filter.getkPagingDetail() == null) {
			return repo.getListBySQL(EquipRepairFormDtl.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(EquipRepairFormDtl.class, sql.toString(), params, filter.getkPagingDetail());
		}
	}
	
	/**
	 * Lay danh sach phieu sua chua; dung cho tao moi ds chi tiet cua phieu thanh toan
	 * @author vuongmq
	 * @since 24/06/2015
	 * @return
	 */
	@Override
	public List<EquipRepairForm> getListEquipRepairFormFilter(EquipRepairFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_repair_form f ");
		sql.append(" where 1 = 1 ");
		if (filter.getEquipRepairId() != null) {
			sql.append(" and f.equip_repair_form_id = ? ");
			params.add(filter.getEquipRepairId());
		}
		if (filter.getLstId() != null) {
			sql.append(" and f.equip_repair_form_id in (-1 ");
			for (Long id : filter.getLstId()) {
				if (id != null) {
					sql.append(" ,?");
					params.add(id);
				}
			}
			sql.append(" )");
		}
		sql.append(" order by f.equip_repair_form_code  ");
		if (filter.getkPagingRepairForm() == null) {
			return repo.getListBySQL(EquipRepairForm.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(EquipRepairForm.class, sql.toString(), params, filter.getkPagingRepairForm());
		}
	}
	//////////////////// Begin VUONGMQ Bao mat; 11/05/2015 ////////////
	/***
	 * @author vuongmq
	 * @date 11/05/2015
	 * lay danh sach kho cap tren cua Thiet bi ung voi tung loai 1: NPP; 2: KH
	 */
	@Override
	public List<EquipStock> getListEquipStockParentByFilter(EquipStockEquipFilter<EquipStock> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_stock where shop_id in ( ");
		sql.append(" select s.shop_id from shop s ");
		sql.append(" where s.status = 1 ");
		sql.append(" and s.shop_type_id = (select channel_type_id from channel_type where status = 1 and type = 1 and object_type = 1) ");
		if (filter.getStockType() != null && EquipStockTotalType.KHO.getValue().equals(filter.getStockType())
			&& filter.getStockId() != null) {
			sql.append(" START WITH s.shop_id in (select shop_id from equip_stock where equip_stock_id = ?) ");
			sql.append(" CONNECT BY PRIOR s.parent_shop_id = s.shop_id ");
			params.add(filter.getStockId());
		}
		if (filter.getStockType() != null && EquipStockTotalType.KHO_KH.getValue().equals(filter.getStockType())
				&& filter.getStockId() != null) {
				sql.append(" START WITH s.shop_id in (SELECT shop_id from customer where customer_id = ?) ");
				sql.append(" CONNECT BY PRIOR s.parent_shop_id = s.shop_id ");
				params.add(filter.getStockId());
			}
		sql.append(" ) ");
		sql.append(" and ordinal = 1 "); // lay theo thu tu la 1 uu tien
		sql.append(" ORDER by equip_stock_id ");
		return repo.getListBySQL(EquipStock.class, sql.toString(), params);
	}
	
	/***
	 * @author vuongmq
	 * @date 11/05/2015
	 * lay danh s?�¡ch kho cap tren cua Thiet bi ung voi tung loai 1: NPP; 2: KH ; IF getListEquipStockParentByFilter() KHONG CO DU LIEU
	 */
	@Override
	public List<EquipStock> getListEquipStockParentSameLevelByFilter(EquipStockEquipFilter<EquipStock> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_stock where shop_id in ( ");
		sql.append(" select s.shop_id from shop s ");
		sql.append(" where s.status = 1 ");
		sql.append(" and s.shop_type_id = (select channel_type_id from channel_type where status = 1 and type = 1 and object_type = 1) ");
		sql.append(" ) ");
		///sql.append(" and ordinal = 1 "); // lay theo thu tu la 1 uu tien
		sql.append(" ORDER by ordinal, equip_stock_id ");
		return repo.getListBySQL(EquipStock.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipCategory> getListCategory() throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_category where status = 1 order by name");
		return repo.getListBySQL(EquipCategory.class, sql.toString(), params);
	}
	
	
	/***
	 * @author vuongmq
	 * Lay danh sach hop dong giao nhan moi nhat cua thiet bi
	 * @date 20/05/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public EquipDeliveryRecord getEquipDeliveryRecordNewOfEquipmentByFilter(EquipmentFilter<EquipDeliveryRecord> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT r.* FROM equip_delivery_record r ");
		sql.append(" where r.equip_delivery_record_id in ");
		sql.append(" (SELECT dta.equip_delivery_record_id ");
		sql.append(" FROM ");
		sql.append("   (SELECT dtl.equip_id , ");
		sql.append(" dtl.equip_delivery_record_id , ");
		sql.append(" hr.contract_number , ");
		sql.append(" hr.record_status , ");
		sql.append(" row_number() over (partition BY dtl.equip_id order by hr.create_date DESC) AS rn ");
		sql.append(" FROM equip_delivery_record hr ");
		sql.append(" JOIN equip_delivery_rec_dtl dtl ");
		sql.append(" ON hr.equip_delivery_record_id = dtl.equip_delivery_record_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" ) dta ");
		sql.append(" WHERE dta.rn = 1 ");
		if (filter.getEquipId() != null) {
			sql.append(" and dta.equip_id = ? ");
			params.add(filter.getEquipId());
		}
		sql.append(" ) ");
		return repo.getEntityBySQL(EquipDeliveryRecord.class, sql.toString(), params);
	}
	//////////////////// End VUONGMQ Bao mat; 11/05/2015 ////////////
	
	@Override
	public List<EquipmentVO> getlistEquipGroup(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select  code from equip_group ");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(code) = ? ");
			params.add(filter.getCode().trim().toLowerCase());
		}
		final String[] fieldNames = new String[] { "code", };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public EquipRepairForm createEquipRepairForm(EquipRepairForm equipRepairForm) throws DataAccessException {
		if (equipRepairForm == null) {
			throw new IllegalArgumentException("equipRepairForm is null");
		}
		if (equipRepairForm.getEquipRepairFormCode() != null) {
			equipRepairForm.setEquipRepairFormCode(equipRepairForm.getEquipRepairFormCode().toUpperCase());
		}
		return repo.create(equipRepairForm);
	}

	@Override
	public EquipRepairFormDtl createEquipRepairFormDtl(EquipRepairFormDtl equipRepairFormDtl) throws DataAccessException {
		if (equipRepairFormDtl == null) {
			throw new IllegalArgumentException("equipRepairFormDtl is null");
		}
		return repo.create(equipRepairFormDtl);
	}

	@Override
	public EquipRepairForm getEquipRepairFormById(long id) throws DataAccessException {
		return repo.getEntityById(EquipRepairForm.class, id);
	}

	@Override
	public EquipRepairForm updateEquipRepairForm(EquipRepairForm equipRepairForm) throws DataAccessException {
		if (equipRepairForm == null) {
			throw new IllegalArgumentException("equipRepairForm is null");
		}
		if (equipRepairForm.getEquipRepairFormCode() != null) {
			equipRepairForm.setEquipRepairFormCode(equipRepairForm.getEquipRepairFormCode().toUpperCase());
		}
		repo.update(equipRepairForm);
		return equipRepairForm;
	}

	/***
	 * @author vuongmq
	 * @date 22/04/2015
	 * cap nhat: lay lai theo phan quyen nhieu kho
	 * danh sach phieu sua chua
	 */
	@Override
	public List<EquipRepairFormVO> getListEquipRepairEx(EquipRepairFilter filter) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			throw new IllegalArgumentException("StrListShopId() is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/** select VO **/
		sql.append(" SELECT f.equip_repair_form_id id, ");
		sql.append(" (CASE WHEN f.stock_type = 1 ");
		sql.append(" THEN ");
		sql.append(" (SELECT es.code || ' - ' || es.name ");
		sql.append(" FROM equip_stock es ");
		sql.append(" WHERE es.equip_stock_id = f.stock_id ");
		sql.append(" ) ");
		sql.append(" ELSE ");
		sql.append(" (SELECT sh.shop_code ||c.short_code || '-' || c.customer_name ");
		sql.append(" FROM customer c ");
		sql.append(" JOIN shop sh ON sh.shop_id = c.shop_id ");
		sql.append(" WHERE c.customer_id = f.stock_id ) ");
		sql.append(" END ) AS donVi, ");
		sql.append(" f.equip_repair_form_code AS maPhieu, ");
		sql.append(" ec.name AS loaiThietBi, ");
		sql.append(" (eg.code ||'-' ||eg.name) AS nhomThietBi, ");
		sql.append(" e.code AS maThietBi, ");
		sql.append(" e.serial AS soSeri, ");
//		sql.append(" TO_CHAR(f.create_date,'dd/mm/yyyy') ngayTao, ");
		//TAMVNM: thay doi thanh ngay tao
		sql.append(" TO_CHAR(f.create_form_date,'dd/mm/yyyy') ngayTao, ");
		sql.append(" ( CASE WHEN f.stock_type = 2 ");
		sql.append(" THEN ");
		sql.append(" (SELECT (cus.short_code || '-' || cus.customer_name) ");
		sql.append(" FROM customer cus ");
		sql.append(" WHERE cus.customer_id = f.stock_id ");
		sql.append(" ) ");
		sql.append(" ELSE NULL ");
		sql.append(" END ) khachHang, ");
		sql.append(" ( CASE WHEN f.stock_type = 2 ");
		sql.append(" THEN ");
		sql.append(" (SELECT address ");
		sql.append(" FROM customer ");
		sql.append(" WHERE customer.customer_id = f.stock_id ) ");
		sql.append(" ELSE ");
		sql.append(" (SELECT address ");
		sql.append(" FROM shop sh ");
		sql.append(" WHERE sh.shop_id = (SELECT es.shop_id ");
		sql.append(" FROM equip_stock es ");
		sql.append(" WHERE es.equip_stock_id = f.stock_id) ");
		sql.append(" ) ");
		sql.append(" END ) diaChi, ");
		sql.append(" f.condition AS tinhTrangHuHong, ");
		sql.append(" f.reason lyDoDeNghi, ");
		sql.append(" f.total_amount tongTien, ");
		sql.append(" f.reject_reason lyDoTuChoi, ");
		sql.append(" f.note as note, ");
		sql.append(" (select eqat.url from equip_attach_file eqat where eqat.object_id = f.equip_repair_form_id and rownum = 1 and eqat.object_type = 2) as url ");
		/** lay table from **/
		sql.append(" from  equip_repair_form f");
		sql.append(" join equipment e on f.equip_id = e.equip_id ");
		sql.append(" join equip_group eg on eg.equip_group_id = e.equip_group_id");
		sql.append(" join equip_category ec on ec.equip_category_id = eg.equip_category_id");
		sql.append(" where 1=1 "); 
		sql.append(" and f.status = 1 ");/** trang thai phieu sua chua cho duyet*/
		if (!StringUtility.isNullOrEmpty(filter.getFormCode())) {
			sql.append(" and f.equip_repair_form_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getFormCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getEuqipCode())) {
			sql.append(" and upper(e.code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEuqipCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeri())) {
			sql.append(" and upper(e.serial) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeri().toUpperCase()));
		}
//		if (filter.getFromDate() != null) {
//			sql.append(" and f.create_date >=trunc(?)  ");
//			params.add(filter.getFromDate());
//		}
//		if (filter.getToDate() != null) {
//			sql.append(" and f.create_date < trunc(?) + 1 ");
//			params.add(filter.getToDate());
//		}
		//tamvnm: 13/07/2015 - thay doi tim kiem theo ngay lap
		if (filter.getFromDate() != null) {
			sql.append(" and f.create_form_date >=trunc(?)  ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and f.create_form_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		/** lay stock_type all**/
		if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			sql.append(" AND ( (f.stock_id IN ");
			sql.append(" (SELECT equip_stock_id ");
			sql.append(" FROM equip_stock ");
			sql.append(" WHERE shop_id IN ");
			sql.append(" (SELECT sh.shop_id ");
			sql.append(" FROM shop sh ");
			sql.append(" where sh.status = 1 ");
			sql.append(" START WITH sh.shop_id IN ");
			sql.append(" (SELECT regexp_substr(?,'[^,]+', 1, level) ");
			sql.append(" FROM dual ");
			sql.append(" CONNECT BY regexp_substr(?, '[^,]+', 1, level) IS NOT NULL ) ");
			sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ) ");
			sql.append(" ) ");
			params.add(filter.getStrListShopId().trim());
			params.add(filter.getStrListShopId().trim());
			sql.append(" and f.stock_type = 1 ");
			sql.append(" ) ");
			/*** end kho NPP */
			sql.append(" OR (f.stock_id IN ");
			sql.append(" (SELECT cus.customer_id ");
			sql.append(" FROM customer cus ");
			sql.append(" WHERE cus.shop_id IN ");
			sql.append(" (SELECT sh.shop_id ");
			sql.append(" FROM shop sh ");
			sql.append(" where sh.status = 1 ");
			sql.append(" START WITH sh.shop_id IN ");
			sql.append(" (SELECT regexp_substr(?,'[^,]+', 1, level) ");
			sql.append(" FROM dual ");
			sql.append(" CONNECT BY regexp_substr(?, '[^,]+', 1, level) IS NOT NULL ) ");
			sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ) ");
			sql.append(" ) ");
			params.add(filter.getStrListShopId().trim());
			params.add(filter.getStrListShopId().trim());
			sql.append(" AND f.stock_type = 2) ");
			sql.append(" ) ");
			/*** end kho KH */
		}
		/** seach danh sach kho cua KH theo shop Id; connect by lay theo shop*/
		if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())
				&& (!StringUtility.isNullOrEmpty(filter.getCustomerCode()) || !StringUtility.isNullOrEmpty(filter.getCustomerName()) ) ) {
			/** --------- tim kiem MAKH va ten KH */
			sql.append(" AND (f.stock_id IN ");
				sql.append(" (SELECT cus.customer_id ");
				sql.append(" FROM customer cus ");
				sql.append(" WHERE cus.shop_id IN ");
				sql.append(" (SELECT sh.shop_id ");
				sql.append(" FROM shop sh ");
				sql.append(" where sh.status = 1 ");
				sql.append(" START WITH sh.shop_id IN ");
				sql.append(" (SELECT regexp_substr(?,'[^,]+', 1, level) ");
				sql.append(" FROM dual ");
				sql.append(" CONNECT BY regexp_substr(?, '[^,]+', 1, level) IS NOT NULL ) ");
				sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ) ");
				params.add(filter.getStrListShopId().trim());
				params.add(filter.getStrListShopId().trim());
				if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
					sql.append(" and upper(cus.short_code) like ? ESCAPE '/' ");
					params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().toUpperCase()));
				}
				if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
					sql.append(" and upper(cus.name_text) like ? ESCAPE '/' ");
					params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getCustomerName().toUpperCase())));
				}
				sql.append(" ) ");
			sql.append(" AND f.stock_type = 2) ");
		}
		
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql.toString());
		countSql.append(")");
		
		sql.append(" order by donVi, maPhieu desc ");

		String[] fieldNames = new String[] { "id", //1
				"donVi", "maPhieu", "loaiThietBi", "nhomThietBi", "maThietBi", "soSeri", "ngayTao",//8 
				"khachHang", "diaChi", "tinhTrangHuHong", "lyDoDeNghi", "tongTien", //12
				"lyDoTuChoi", "note",
				"url" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,//8 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, //12
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING };
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipRepairFormVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(EquipRepairFormVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	@Override
	public List<EquipmentVO> getListEquipSalePlan(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select equip_sale_plan_id as id from equip_sale_plan ");
		sql.append(" where 1=1 ");
		if (filter.getId() != null && filter.getId() > 0) {
			sql.append(" and equip_group_id = ? ");
			params.add(filter.getId());
		}
		final String[] fieldNames = new String[] { "id" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipSalePlan> getListEquipSalePlanByFilter(EquipmentSalePlaneFilter<EquipSalePlan> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct * from equip_sale_plan ");
		sql.append(" where 1 = 1 ");
		if (filter.getEquipGroupId() != null) {
			sql.append(" and equip_group_id = ? ");
			params.add(filter.getEquipGroupId());
		}
		sql.append(" order by equip_sale_plan_id ");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(EquipSalePlan.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(EquipSalePlan.class, sql.toString(), params, filter.getkPaging());
		}
	}
	
	@Override
	public EquipSalePlan createEquipmentSalePlane(EquipSalePlan eqs) throws DataAccessException {
		if (eqs == null) {
			throw new IllegalArgumentException("eqs is null");
		}
		eqs.setCreateDate(commonDAO.getSysDate());
		return repo.create(eqs);

	}

	@Override
	public EquipSalePlan getEquipSalePlaneById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipSalePlan.class, id);
	}

	@Override
	public EquipSalePlan updateEquipmentSalePlane(EquipSalePlan eqs) throws DataAccessException {
		if (eqs == null) {
			throw new IllegalArgumentException("eqs is null");
		}
		repo.update(eqs);
		return eqs;
	}

	@Override
	public List<EquipItemVO> getListEquipItem(EquipItemFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ei.equip_item_id id, ei.name, ");
		/** vuongmq; 10/06/2015; lay so lan sua chua thiet bi cho chinh xac; Defect 227832; */
		//sql.append(" (SELECT COUNT(1)+1 ");
		sql.append(" (SELECT nvl(max(dt.repair_count),0)+1 ");  
		sql.append(" FROM equip_repair_form_dtl dt");
		sql.append(" join equip_repair_form f on f.equip_repair_form_id = dt.equip_repair_form_id");
		sql.append(" WHERE dt.equip_item_id = ei.equip_item_id and f.status = ?");
		params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
		if (filter.getEquipId() != null && filter.getEquipId() > 0) {
			sql.append(" and f.equip_id = ?");
			params.add(filter.getEquipId());
		}
		sql.append(" ) repairCount");
		sql.append(" from equip_item ei ");
		sql.append(" where ei.status = 1");
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			sql.append(" and upper(ei.name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getName().toUpperCase()));
		}
		if (filter.getLstEquipItemIdExcept() != null && filter.getLstEquipItemIdExcept().size() > 0) {
			sql.append(" and ei.equip_item_id not in (-1 ");
			for (Long id : filter.getLstEquipItemIdExcept()) {
				sql.append(",? ");
				params.add(id);
			}
			sql.append(" ) ");
		}
		final String[] fieldNames = new String[] { "id", "name", "repairCount" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };

		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipItemVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(EquipItemVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	
	/***
	 * @author vuongmq
	 * @date 06/04/2015
	 * lay danh sach hang muc popup lap phieu yeu cau sua chua
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public List<EquipItemVO> getListEquipItemPopUpRepair(EquipItemFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with a as ( ");
		sql.append(" select g.CAPACITY_FROM as dungTichTu, g.CAPACITY_TO as dungTichDen from EQUIP_GROUP g ");
		sql.append(" where g.EQUIP_GROUP_ID = ");
		sql.append(" (select e.EQUIP_GROUP_ID from equipment e where 1 = 1 ");
		if (filter.getEquipId() != null && filter.getEquipId() > 0) {
			sql.append(" and e.equip_id = ? ");
			params.add(filter.getEquipId());
		} else {
			sql.append(" and e.equip_id = 0 ");
		}
		sql.append(" and rownum = 1) ");
		sql.append(" ), ");
		/**end bang a*/
		sql.append(" b as ( ");
		sql.append(" SELECT itd.equip_item_id id, ");
		sql.append(" ei.code, ");
		sql.append(" ei.name, ");
		if (filter.getEquipId() != null && filter.getEquipId() > 0) {
			//sql.append(" (SELECT COUNT(1)+1 ");
			/** vuongmq; 10/06/2015; lay so lan sua chua thiet bi cho chinh xac; Defect 227832*/
			sql.append(" (SELECT nvl(max(dt.repair_count),0)+1 ");
			sql.append(" FROM equip_repair_form_dtl dt ");
			sql.append(" JOIN equip_repair_form f ON f.equip_repair_form_id = dt.equip_repair_form_id ");
			sql.append(" WHERE dt.equip_item_id = ei.equip_item_id ");
			sql.append(" AND f.status = ? ");
			params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
			sql.append(" AND f.equip_id = ? ");
			params.add(filter.getEquipId());
			sql.append(" ) repairCount, ");
		} else {
			sql.append(" 1 repairCount, ");
		}
		sql.append(" nvl(ei.type,0) as type, ");
		sql.append(" nvl(ei.warranty,0) as warranty, ");
		sql.append(" itd.from_worker_price as fromWorkerPrice, ");
		sql.append(" itd.to_worker_price as toWorkerPrice, ");
		sql.append(" itd.from_material_price as fromMaterialPrice, ");
		sql.append(" itd.to_material_price as toMaterialPrice, ");
		sql.append(" eic.FROM_CAPACITY as fromCapacity, ");
		sql.append(" eic.TO_CAPACITY as toCapacity, ");
		sql.append(" itd.equip_item_config_dtl_id as IdDetail ");
		sql.append(" FROM equip_item_config eic ");
		sql.append(" left join equip_item_config_dtl itd on itd.equip_item_config_id =eic.equip_item_config_id ");
		sql.append(" left join equip_item ei on ei.equip_item_id = itd.equip_item_id ");
		sql.append(" where ei.status = 1 ");
		sql.append(" and eic.status = 1 ");
		sql.append(" and ( ");
		/*** --dungTichTu != null && dungTichDen != null */
		sql.append(" ( (select dungTichTu from a) is not null and (select dungTichDen from a) is not null ");
		sql.append(" and (eic.TO_CAPACITY is not null and eic.FROM_CAPACITY is not null ");
		sql.append(" and (select dungTichTu from a) <= eic.TO_CAPACITY ");
		sql.append(" and (select dungTichDen from a) >= eic.FROM_CAPACITY ) ");
		sql.append(" or( eic.FROM_CAPACITY is not null and eic.FROM_CAPACITY <= (select dungTichDen from a) and eic.FROM_CAPACITY is null) ");
		sql.append(" or( eic.TO_CAPACITY is not null and eic.TO_CAPACITY >= (select dungTichTu from a) and eic.FROM_CAPACITY is null) ");
		sql.append(" ) ");
		/*** --dungTichTu != null && dungTichDen == null */
		/*sql.append(" or ( (select dungTichTu from a) is not null and (select dungTichDen from a) is null ");
		sql.append(" and eic.FROM_CAPACITY is not null and eic.FROM_CAPACITY >= (select dungTichTu from a) ");
		sql.append(" or eic.TO_CAPACITY is null ");
		sql.append(" ) ");*/
		sql.append(" OR ( (SELECT dungTichTu FROM a) IS NOT NULL AND (SELECT dungTichDen FROM a) IS NULL ");
		sql.append(" AND (eic.FROM_CAPACITY IS NOT NULL AND eic.FROM_CAPACITY >= (SELECT dungTichTu FROM a ) AND eic.TO_CAPACITY IS NULL ) ");
		sql.append(" OR (eic.TO_CAPACITY IS NOT NULL AND eic.TO_CAPACITY >= (SELECT dungTichTu FROM a) AND eic.FROM_CAPACITY IS NULL) ");
		sql.append(" ) ");
		/*** ---dungTichTu == null && dungTichDen != null */
		sql.append(" or ( (select dungTichTu from a) is null and (select dungTichDen from a) is not null ");
		sql.append(" and (eic.TO_CAPACITY is not null and eic.TO_CAPACITY <= (select dungTichDen from a) and eic.FROM_CAPACITY is null ) ");
		sql.append(" or (eic.FROM_CAPACITY is not null and eic.FROM_CAPACITY <= (select dungTichDen from a) and eic.TO_CAPACITY is null) ");
		sql.append(" ) ");
		sql.append(" ) ");
		/** --v?� loại trừ những hạng mục của thiết bị đang trong th�?i gian c?�n bảo h?�nh
		*-- tuong duong voi: lay nhung hang muc het thoi gian bao hanh */
		sql.append(" and ei.equip_item_id not in ( ");
		sql.append(" SELECT dt.equip_item_id ");
		sql.append(" FROM equip_repair_form_dtl dt ");
		sql.append(" JOIN equip_repair_form f ON f.equip_repair_form_id = dt.equip_repair_form_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND f.status = ? ");
		params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
		if (filter.getEquipId() != null) {
			sql.append(" AND f.equip_id = ? ");
			params.add(filter.getEquipId());
		} else {
			sql.append(" AND f.equip_id = 0 ");
		}
		sql.append(" and ( ( dt.warranty_expired_date is not null and dt.warranty_expired_date >= sysdate) ");
		sql.append(" or dt.warranty_expired_date is null ");
		sql.append(" ) ");
		sql.append(" ) ");
		/** end dieu kien*/
		sql.append(" order by fromCapacity, toCapacity ");
		sql.append(" ), ");
		/**end bang b*/
		sql.append(" tmp1 as ( ");
		sql.append(" select id, code, name, repairCount,type, warranty, fromWorkerPrice, toWorkerPrice, fromMaterialPrice, toMaterialPrice, fromCapacity, toCapacity ");
		sql.append(" from ( ");
		sql.append(" select id, code, name, repairCount,type,warranty,fromWorkerPrice, toWorkerPrice, fromMaterialPrice, toMaterialPrice, fromCapacity,toCapacity, ");
		sql.append(" RANK() OVER (PARTITION BY id order by idDetail) rank ");
		sql.append(" from b ");
		sql.append(" )tmp ");
		sql.append(" where tmp.rank = 1 ");
		sql.append(" order by name ");
		sql.append(" ), ");
		/**end bang tmp1 */
		sql.append(" tmp2 as ( ");
		sql.append(" Select ei.equip_item_id as id, ");
		sql.append(" ei.code, ");
		sql.append(" ei.name, ");
		if (filter.getEquipId() != null && filter.getEquipId() > 0) {
			//sql.append(" (SELECT COUNT(1)+1 ");
			/** vuongmq; 10/06/2015; lay so lan sua chua thiet bi cho chinh xac; Defect 227832*/
			sql.append(" (SELECT nvl(max(dt.repair_count),0)+1 ");
			sql.append(" FROM equip_repair_form_dtl dt ");
			sql.append(" JOIN equip_repair_form f ON f.equip_repair_form_id = dt.equip_repair_form_id ");
			sql.append(" WHERE dt.equip_item_id = ei.equip_item_id ");
			sql.append(" AND f.status = ? ");
			params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
			sql.append(" AND f.equip_id = ? ");
			params.add(filter.getEquipId());
			sql.append(" ) repairCount, ");
		} else {
			sql.append(" 1 repairCount, ");
		}
		sql.append(" nvl(ei.type,0) as type, ");
		sql.append(" nvl(ei.warranty,0) as warranty, ");
		sql.append(" null fromWorkerPrice, ");
		sql.append(" null toWorkerPrice, ");
		sql.append(" null fromMaterialPrice, ");
		sql.append(" null toMaterialPrice, ");
		sql.append(" null fromCapacity, ");
		sql.append(" null toCapacity ");
		sql.append(" from EQUIP_ITEM ei ");
		sql.append(" where ei.STATUS=1 ");
			/** --v?� loại trừ những hạng mục của thiết bị đang trong th�?i gian c?�n bảo h?�nh
		*-- tuong duong voi: lay nhung hang muc het thoi gian bao hanh */
		sql.append(" and ei.equip_item_id not in ( ");
		sql.append(" SELECT dt.equip_item_id ");
		sql.append(" FROM equip_repair_form_dtl dt ");
		sql.append(" JOIN equip_repair_form f ON f.equip_repair_form_id = dt.equip_repair_form_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND f.status = ? ");
		params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
		if (filter.getEquipId() != null) {
			sql.append(" AND f.equip_id = ? ");
			params.add(filter.getEquipId());
		} else {
			sql.append(" AND f.equip_id = 0 ");
		}
		sql.append(" and ( ( dt.warranty_expired_date is not null and dt.warranty_expired_date >= sysdate) ");
		sql.append(" or dt.warranty_expired_date is null ");
		sql.append(" ) ");
		sql.append(" ) ");
		sql.append(" and ei.equip_item_id not in ( select id from tmp1) ");
		sql.append(" order by name ");
		sql.append(" ), ");
		/**end bang tmp2 */
		sql.append(" tmp3 as ( ");
		sql.append(" select * from tmp1 ");
		sql.append(" union ");
		sql.append(" select * from tmp2 ");
		sql.append(" ) ");
		/**end bang tmp3 */
		/** select bang tmp3 */
		sql.append(" select * from tmp3 t ");
		sql.append(" where 1 = 1 ");
		
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			sql.append(" and (t.code like ? ESCAPE '/'  ");
			sql.append(" or unicode2English(t.name) like ? ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getName()).trim().toUpperCase()));
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getName()).trim().toLowerCase()));
		}
		if (filter.getLstEquipItemIdExcept() != null && filter.getLstEquipItemIdExcept().size() > 0) {
			//id: la?�  equip_item_id
			sql.append(" and t.id not in (-1 ");
			for (Long id : filter.getLstEquipItemIdExcept()) {
				sql.append(",? ");
				params.add(id);
			}
			sql.append(" ) ");
		}
		/** cai nay lay luc cap nhat phieu sua chua tra ve sanh sach detail hang muc*/
		if (filter.getEpuipItemId() != null) {
			//id: la?�  equip_item_id
			sql.append(" and t.id = ? ");
			params.add(filter.getEpuipItemId());
		}
		sql.append(" order by code, name ");
		final String[] fieldNames = new String[] { "id", "code", "name", "repairCount",
													"type","warranty", 
													"fromWorkerPrice", "toWorkerPrice",
													"fromMaterialPrice","toMaterialPrice",
													"fromCapacity", "toCapacity"
													};

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				 								StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				 								StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
				 								StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
				 								StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL};

		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipItemVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(EquipItemVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	/**
	 * Lay equipment Entity by code theo filter
	 * @author vuongmq
	 * @since 17/04/2015
	 * lay thiet bi cua chuc nang bao mat
	 */
	@Override
	public List<EquipmentVO> searchEquipByEquipDeliveryRecordNew(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		if (filter.getStaffRoot() == null || filter.getShopId() == null) {
			throw new IllegalArgumentException("staffRoot or shopId is null");
		}
		StringBuilder sqlKhoNPP = new StringBuilder();
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/** * staffRoot va?�  shopCode; shopId luon luon ca */
		sqlKhoNPP.append(" with t_kho_under as ");
		sqlKhoNPP.append(" (SELECT es.equip_stock_id AS id, "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" es.shop_id AS shopId, "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" es.code AS code, "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" es.name AS name "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" FROM equip_role_user eru ");
		sqlKhoNPP.append(" JOIN equip_role_detail erd ");
		sqlKhoNPP.append(" ON eru.equip_role_id = erd.equip_role_id ");
		sqlKhoNPP.append(" JOIN equip_stock es ");
		sqlKhoNPP.append(" ON es.equip_stock_id = erd.equip_stock_id ");
		sqlKhoNPP.append(" WHERE 1 = 1 ");
		sqlKhoNPP.append(" AND es.status = 1 ");
		sqlKhoNPP.append(" AND erd.is_under = 1 ");
		sqlKhoNPP.append(" AND eru.status = 1 ");
		sqlKhoNPP.append(" AND eru.user_id = ? ");
		sqlKhoNPP.append(" ), ");
		params.add(filter.getStaffRoot());
		sqlKhoNPP.append(" stock_under AS ");
		sqlKhoNPP.append(" (SELECT equip_stock_id AS id, ");
		sqlKhoNPP.append(" es.shop_id as shopId, ");
		sqlKhoNPP.append(" es.code AS code, ");
		sqlKhoNPP.append(" es.name AS name ");
		sqlKhoNPP.append(" FROM equip_stock es ");
		sqlKhoNPP.append(" WHERE 1 =1 ");
		sqlKhoNPP.append(" AND es.status = 1 ");
		sqlKhoNPP.append(" AND es.shop_id IN ");
		sqlKhoNPP.append(" (SELECT sh.shop_id ");
		sqlKhoNPP.append(" FROM shop sh ");
		sqlKhoNPP.append(" WHERE sh.status = 1 ");
		sqlKhoNPP.append(" and sh.shop_id not in (SELECT shopId FROM t_kho_under) "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" START WITH sh.shop_id IN (SELECT shop_id FROM t_kho_under) ");
		sqlKhoNPP.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sqlKhoNPP.append(" ) ");
		sqlKhoNPP.append(" union "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" (SELECT * FROM t_kho_under) "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" ), ");
		sqlKhoNPP.append(" t_kho_nounder AS ");
		sqlKhoNPP.append(" (SELECT es.equip_stock_id AS id, ");
		sqlKhoNPP.append(" es.shop_id AS shopId, ");
		sqlKhoNPP.append(" es.code AS code, ");
		sqlKhoNPP.append(" es.name AS name ");
		sqlKhoNPP.append(" FROM equip_role_user eru ");
		sqlKhoNPP.append(" JOIN equip_role_detail erd ");
		sqlKhoNPP.append(" ON eru.equip_role_id = erd.equip_role_id ");
		sqlKhoNPP.append(" JOIN equip_stock es ");
		sqlKhoNPP.append(" ON es.equip_stock_id = erd.equip_stock_id ");
		sqlKhoNPP.append(" WHERE 1 = 1 ");
		sqlKhoNPP.append(" AND es.status = 1 ");
		sqlKhoNPP.append(" AND erd.is_under = 0 ");
		sqlKhoNPP.append(" AND eru.status = 1 ");
		sqlKhoNPP.append(" AND eru.user_id = ? ");
		sqlKhoNPP.append(" ) , ");
		params.add(filter.getStaffRoot());
		sqlKhoNPP.append(" lstStockShop AS ");
		sqlKhoNPP.append(" (SELECT id, code, name, shopId FROM stock_under ");
		sqlKhoNPP.append(" UNION ");
		sqlKhoNPP.append(" SELECT id, code, name, shopId FROM t_kho_nounder ");
		sqlKhoNPP.append(" ) , ");
		sqlKhoNPP.append(" lstStockShopView as ");
		sqlKhoNPP.append(" (select * from lstStockShop where shopId in ");
		sqlKhoNPP.append("    (select s.shop_id from Shop s where s.shop_id in (?) ) ");
		sqlKhoNPP.append(" ) ");
		params.add(filter.getShopId());
		/** end danh sach kho cho NPP */
		/**bao mat:  su dung kho NPP: 1; kho KH: 2 */
		if (filter.getStockType() != null) {
			if (EquipStockTotalType.KHO.getValue().equals(filter.getStockType())) {
				sql.append(sqlKhoNPP);
			} else {
				// kho KH khong xai sqlKhoNPP
				params = new ArrayList<Object>();
			}
		}
		sql.append(" select distinct ");
		sql.append(" eq.equip_id as equipId ");
		sql.append(" , eq.code as equipCode ");
		sql.append(" , eq.serial as serial ");
		sql.append(" , eq.price as price ");
		if (filter.getEquipLostId() != null) {
			// truong hop cap nhat
			sql.append(" , (select price_Actually from equip_lost_record where equip_lost_record_id = ?) as priceActually ");
			params.add(filter.getEquipLostId());
		} else {
			//truong hop them moi
			sql.append(" , null as priceActually ");
		}
		sql.append(" , eq.stock_id as stockId ");
		sql.append(" , eq.stock_code as stockCode ");
		sql.append(" , eq.stock_type as stockType ");
		sql.append(" , redr.equip_delivery_record_id as equipDeliveryRecordId ");
		sql.append(" , redr.contract_number as contractNumber ");
		sql.append(" , eg.equip_group_id as equipGroupId ");
		sql.append(" , eg.code as equipGroupCode ");
		sql.append(" , eg.name as equipGroupName ");
		sql.append(" , ec.equip_category_id as equipCategoryId ");
		sql.append(" , ec.code as equipCategoryCode ");
		sql.append(" , ec.name as equipCategoryName ");
		sql.append(" , eq.manufacturing_year as manufacturingYear ");
		sql.append(" , eq.liquidation_status as liquidationStatus ");
		sql.append(" , to_char(eq.first_date_in_use, 'yyyy') as firstDateInUseStr ");
		/** vuongmq; 27/05/2015; kiem tra de biet thay doi phai bao mat cua mobile hay khong*/
		if (filter.getEquipLostId() != null) {
			sql.append(" , (select m.code from equip_lost_mobile_rec m where rownum = 1 and m.equip_lost_rec_id = ?) as codeMobile ");
			params.add(filter.getEquipLostId());
		} else {
			sql.append(" , null as codeMobile ");
		}
		/** from table */
		sql.append(" from equipment eq left join ( ");
		sql.append(" select * from ( ");
		sql.append(" 	select dtl.equip_id ,dtl.equip_delivery_record_id ,hr.contract_number  ");
		sql.append("   , hr.record_status ");
		sql.append("   ,row_number() over (partition by dtl.equip_id order by hr.create_date desc) as rn ");
		sql.append(" 	from equip_delivery_record hr join equip_delivery_rec_dtl dtl on hr.equip_delivery_record_id = dtl.equip_delivery_record_id ");
		sql.append("   where 1 = 1 ");
		if (filter.getRecordStatus() != null) {
			sql.append("   and hr.record_status = ? ");
			params.add(filter.getRecordStatus());
		}
		sql.append(" ) dta ");
		sql.append(" where dta.rn = 1 order by dta.equip_id ");
		sql.append(" ) redr on eq.equip_id = redr.equip_id ");
		sql.append(" join equip_group eg on eq.equip_group_id = eg.equip_group_id ");
		sql.append(" join equip_category ec on ec.equip_category_id = eg.equip_category_id ");
		sql.append(" where 1 = 1  ");
		/** vuongmq; 06/05/2015; cap nhat */
		/** vuongmq; 08/05/2015; chi lay TB dang o kho: 1 voi loai kho: NPP; dang su dung: 2 voi loai kho la KH
		* va giao dich Binh thuong: 0
		*/
		if (filter.getStockType() != null) {
			sql.append(" and eq.stock_type = ? ");
			params.add(filter.getStockType());
			if (EquipStockTotalType.KHO.getValue().equals(filter.getStockType())) {
				// bao mat NPP
				//sql.append(" and eq.stock_id IN (select es.equip_stock_id from equip_stock es where es.shop_id = ?) ");
				//params.add(filter.getShopId());
				sql.append(" and eq.stock_id IN (select id from lstStockShopView)  ");
				
				sql.append(" and eq.usage_status in (?) ");
				params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
			} else {
				// bao mat KH
				sql.append(" and eq.stock_id IN (?) ");
				params.add(filter.getStockId());
				
				//vuongmq; 27/05/2015; 0: bao mat wweb; 1: bao mat mobile khong can kiem tra usage_status; tr?�°?�»?ng hop nay kiem tra khi thiet bi ben mobile KH cung phai la 3: dang su dung
				/*if (filter.getFlagMobileLost() != null && filter.getFlagMobileLost().equals(ActiveType.STOPPED.getValue())) {
					sql.append(" and eq.usage_status in (?) ");
					params.add(EquipUsageStatus.IS_USED.getValue()); 
				}*/
				sql.append(" and eq.usage_status in (?) ");
				params.add(EquipUsageStatus.IS_USED.getValue()); 
				
			}
		}
		/*if (filter.getStockId() != null) {
			sql.append(" and eq.stock_id = ? ");
			params.add(filter.getStockId());
		}*/
		if (filter.getTradeStatus() != null) {
			sql.append(" and eq.trade_status = ? ");
			params.add(filter.getTradeStatus());
		}
		if (filter.getEquipId() != null) {
			sql.append(" and eq.equip_id = ? ");
			params.add(filter.getEquipId());
		}
		if (filter.getFlagUpdate() != null && filter.getFlagUpdate() == 0) {
			sql.append(" and eq.liquidation_status is null ");
		}
		if (filter.getLstEquipId() != null) {
			sql.append(" and eq.equip_id in ( ? ");
			params.add(filter.getLstEquipId().get(0));
			for (int i = 1, size = filter.getLstEquipId().size(); i < size; i++) {
				sql.append(" , ? ");
				params.add(filter.getLstEquipId().get(i));
			}
			sql.append(" ) ");
		}
		if (filter.getEqDeliveryRecId() != null) {
			sql.append(" and eq.equip_id in (select equip_id from equip_lost_record where equip_delivery_record_id =?) ");
			params.add(filter.getEqDeliveryRecId());
		}
		if (filter.getEquipLostId() != null) {
			sql.append(" and eq.equip_id in (select equip_id from equip_lost_record where equip_lost_record_id =?) ");
			params.add(filter.getEquipLostId());
		}
		/** vuongmq; 06/05/2015; khong su dung nua*/
		/*if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			sql.append(" and lower(eq.stock_code) like ? ");
			params.add(filter.getStockCode().toLowerCase().trim());
		}*/
		if (filter.getStatus() != null) {
			sql.append(" and eq.status = ? ");
			params.add(filter.getStatus());
		}
		if (!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			sql.append(" and eq.code = ? ");
			params.add(filter.getEquipCode().toUpperCase().trim());
		}
		sql.append(" order by eq.code ");
		String[] fieldNames = { "equipId", "stockId", "equipDeliveryRecordId", "stockType", 
				"price", "priceActually",
				"equipCode", "serial", "stockCode",
				"equipGroupId", "equipCategoryId", 
				"equipGroupCode", "equipGroupName",
				"equipCategoryCode", "equipCategoryName",
				"manufacturingYear", "contractNumber", "firstDateInUseStr",
				"codeMobile", "liquidationStatus"};
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<Equipment> getListEquipmentEntitesByFilter(EquipmentFilter<Equipment> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from equipment et ");
		sql.append(" where 1 = 1 ");
		if (filter.getLstEquipId() != null && !filter.getLstEquipId().isEmpty()) {
			sql.append(" and et.equip_id in (?   ");
			params.add(filter.getLstEquipId().get(0));
			for (int i = 1, size = filter.getLstEquipId().size(); i < size; i++) {
				sql.append(" ,?  ");
				params.add(filter.getLstEquipId().get(i));
			}
			sql.append(" ) ");
		}

		if (filter.getStatus() != null) {
			sql.append(" and et.status = ? ");
			params.add(filter.getStatus());
		}

		if (filter.getkPaging() == null) {
			return repo.getListBySQL(Equipment.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(Equipment.class, sql.toString(), params, filter.getkPaging());
	}

	@Override
	public List<EquipDeliveryRecord> getListEquipDeliveryRecordEntitesByFilter(EquipmentFilter<EquipDeliveryRecord> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_delivery_record where 1 = 1 ");
		if (filter.getLstId() != null && !filter.getLstId().isEmpty()) {
			sql.append(" and equip_delivery_record_id in (?   ");
			params.add(filter.getLstId().get(0));
			for (int i = 1, size = filter.getLstId().size(); i < size; i++) {
				sql.append(" ,?  ");
				params.add(filter.getLstId().get(i));
			}
			sql.append(" ) ");
		}
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(EquipDeliveryRecord.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(EquipDeliveryRecord.class, sql.toString(), params, filter.getkPaging());
	}

	@Override
	public EquipSalePlan deleteEquipmentSalePlan(EquipSalePlan eqs) throws DataAccessException {
		if (eqs == null) {
			throw new IllegalArgumentException("eqs is null");
		}
		repo.delete(eqs);
		return eqs;
	}

	@Override
	public EquipStockTransForm updateEquipStockTransForm(EquipStockTransForm e) throws DataAccessException {
		if (e == null) {
			throw new IllegalArgumentException("e is null");
		}
		repo.update(e);
		return e;
	}
	
	@Override
	public EquipStockTransFormDtl updateEquipStockTransFormDtl(EquipStockTransFormDtl e) throws DataAccessException {
		if (e == null) {
			throw new IllegalArgumentException("e is null");
		}
		repo.update(e);
		return e;
	}

	@Override
	public EquipStockTransForm getEquipStockTransForm(Long id) throws DataAccessException {
		return repo.getEntityById(EquipStockTransForm.class, id);
	}

	@Override
	public Shop getListEquipStockTransForm(Long fromStockId) throws DataAccessException {
		return repo.getEntityById(Shop.class, fromStockId);
	}

	@Override
	public Shop getListToStockTransForm(Long toStockId) throws DataAccessException {
		return repo.getEntityById(Shop.class, toStockId);
	}

	@Override
	public String getCodeStockTransForm() throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select max(code) as maxCode from equip_stock_trans_form");
		String code = (String) repo.getObjectByQuery(sql.toString(), params);
		if (code == null) {
			return "CK00000001";
		}
		code = code.substring(code.length() - 8);
		code = "00000000" + (Long.parseLong(code.toString()) + 1);
		code = code.substring(code.length() - 8);
		code = "CK" + code;
		return code;
	}

	@Override
	public EquipStockTransForm createEquipmentStockTransForm(EquipStockTransForm equipStockTransForm) throws DataAccessException {
		if (equipStockTransForm == null) {
			throw new IllegalArgumentException("equipStockTransForm is null");
		}
		if (!StringUtility.isNullOrEmpty(equipStockTransForm.getCode())) {
			equipStockTransForm.setCode(equipStockTransForm.getCode().toUpperCase());
		}
		equipStockTransForm.setCreateDate(commonDAO.getSysDate());
//		equipStockTransForm.setCreateFormDate(commonDAO.getSysDate());
		return repo.create(equipStockTransForm);
	}

	@Override
	public EquipStockTransFormDtl createEquipStockTransFormDtl(EquipStockTransFormDtl ed) throws DataAccessException {
		if (ed == null) {
			throw new IllegalArgumentException("ed is null");
		}
//		ed.setCreateDate(commonDAO.getSysDate());
		return repo.create(ed);
	}

	@Override
	public List<EquipmentGroupVO> searchEquipmentGroupVO(EquipmentGroupProductFilter<EquipmentGroupVO> equipmentGroupFilter) throws DataAccessException {
		if (equipmentGroupFilter == null) {
			throw new IllegalArgumentException("equipmentGroupFilter must not be NULL.");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ");
		sql.append(" 	equip_group_id id, ");
		sql.append(" 	code code, ");
		sql.append(" 	name name, ");
		sql.append(" 	status status ");
		sql.append(" from equip_group egp ");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(equipmentGroupFilter.getEquipmentGroupCode())) {
			sql.append(" and egp.code like ? escape '/' ");
			String searchCode = StringUtility.toOracleSearchLikeSuffix(equipmentGroupFilter.getEquipmentGroupCode());
			params.add(searchCode.trim().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(equipmentGroupFilter.getEquipmentGroupName())) {
			sql.append(" and lower(egp.name_text) like ? escape '/' ");
			String searchName = StringUtility.toOracleSearchLikeSuffix(equipmentGroupFilter.getEquipmentGroupName());
			params.add(Unicode2English.codau2khongdau(searchName.trim().toLowerCase()));
		}

		if (equipmentGroupFilter.getEquipmentGroupStatus() != null) {
			sql.append(" and egp.status = ? ");
			params.add(equipmentGroupFilter.getEquipmentGroupStatus().getValue());
		} else {
			sql.append(" and egp.status in (0, 1) ");
		}

		String[] fieldNames = new String[] { "id", "code", "name", "status" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };

		if (equipmentGroupFilter.getkPaging() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count ");
			countSql.append(" from ( ");
			countSql.append(sql.toString());
			countSql.append(" ) ");

			sql.append(" order by egp.code ");
			return repo.getListByQueryAndScalarPaginated(EquipmentGroupVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, equipmentGroupFilter.getkPaging());
		}
		sql.append(" order by egp.code ");
		return repo.getListByQueryAndScalar(EquipmentGroupVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipmentGroupVO> getListEquipmentGroupVOForExport(EquipmentGroupProductFilter<EquipmentGroupVO> equipmentGroupFilter) throws DataAccessException {
		if (equipmentGroupFilter == null) {
			throw new IllegalArgumentException("equipmentGroupFilter must not be NULL.");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT eg.code                     AS code, ");
		sql.append("       p.product_code                       AS productCode, ");
		sql.append("       To_char(Egp.from_date, 'dd/mm/yyyy') AS fromDate, ");
		sql.append("       To_char(egp.to_date, 'dd/mm/yyyy')   AS toDate ");
		sql.append("FROM   equip_group eg ");
		sql.append("       LEFT JOIN equip_group_product egp ");
		sql.append("         ON Egp.equip_group_id = Eg.equip_group_id and egp.status = 1 ");
		sql.append("       LEFT JOIN product p ");
		sql.append("         ON P.product_id = Egp.product_id ");
		sql.append("WHERE  1 = 1 ");

		if (!StringUtility.isNullOrEmpty(equipmentGroupFilter.getEquipmentGroupCode())) {
			sql.append(" and eg.code like ? escape '/' ");
			String searchCode = StringUtility.toOracleSearchLikeSuffix(equipmentGroupFilter.getEquipmentGroupCode());
			params.add(searchCode.trim().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(equipmentGroupFilter.getEquipmentGroupName())) {
			sql.append(" and lower(eg.name_text) like ? escape '/' ");
			String searchName = StringUtility.toOracleSearchLikeSuffix(equipmentGroupFilter.getEquipmentGroupName());
			params.add(Unicode2English.codau2khongdau(searchName.trim().toLowerCase()));
		}

		if (equipmentGroupFilter.getEquipmentGroupStatus() != null) {
			sql.append(" and eg.status = ? ");
			params.add(equipmentGroupFilter.getEquipmentGroupStatus().getValue());
		} else {
			sql.append(" and eg.status in (0, 1) ");
		}

		String[] fieldNames = new String[] { "code", "productCode", "fromDate", "toDate" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		
		sql.append("ORDER  BY eg.code, p.product_code,  Egp.from_date");
		return repo.getListByQueryAndScalar(EquipmentGroupVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<EquipmentGroupProductVO> retrieveProductsInEquipmentGroup(String equipmentGroupCode, ActiveType equipmentGroupProductStatus, KPaging<EquipmentGroupProductVO> kPaging) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(equipmentGroupCode)) {
			throw new IllegalArgumentException("equipmentGroupCode must not be NULL.");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ");
		sql.append(" 	equip_group_product_id equipGroupProductId, ");
		sql.append(" 	egp.equip_group_id equipmentGroupId, ");
		sql.append(" 	pt.product_code productCode, ");
		sql.append(" 	pt.product_name productName, ");
		sql.append(" 	to_char(egpt.from_date, 'dd/MM/yyyy') fromDateStr, ");
		sql.append(" 	to_char(egpt.to_Date, 'dd/MM/yyyy') toDateStr, ");
		sql.append(" 	egpt.from_date fromDate, ");
		sql.append(" 	egpt.to_Date toDate, ");
		sql.append(" (case when egpt.to_Date is not null and trunc(egpt.to_Date) < trunc(sysdate) + 1 then 0 else 1 end) flagSys ");
		sql.append(" from equip_group_product egpt ");
		sql.append(" join equip_group egp on egpt.equip_group_id = egp.equip_group_id and egp.status in (0, 1) ");
		sql.append(" join product pt on egpt.product_id = pt.product_id  ");
		sql.append(" where 1 = 1 ");
		sql.append(" and pt.status in (0, 1) ");
		sql.append(" and egp.code = ? ");
		params.add(equipmentGroupCode.trim().toUpperCase());
		if (equipmentGroupProductStatus != null) {
			sql.append(" and egpt.status = ? ");
			params.add(equipmentGroupProductStatus.getValue());
		} else {
			sql.append(" and egpt.status in (0, 1) ");
		}

		String[] fieldNames = new String[] { "equipGroupProductId", "equipmentGroupId", "productCode", "productName", "fromDateStr", "toDateStr", "fromDate", "toDate", "flagSys" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.DATE, StandardBasicTypes.DATE, StandardBasicTypes.INTEGER };

		if (kPaging != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count ");
			countSql.append(" from ( ");
			countSql.append(sql.toString());
			countSql.append(" ) ");

			sql.append(" order by pt.product_code, egpt.from_date ");
			return repo.getListByQueryAndScalarPaginated(EquipmentGroupProductVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}

		sql.append(" order by pt.product_code, egpt.from_date ");
		return repo.getListByQueryAndScalar(EquipmentGroupProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public EquipGroupProduct retrieveEquipmentGroupProduct(Long equipmentGroupId, Long productId, ActiveType equipmentGroupProductStatus) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from equip_group_product egpt ");
		sql.append(" where 1=1 ");
		sql.append(" and egpt.equip_group_id = ? ");
		params.add(equipmentGroupId);
		sql.append(" and exists (select 1 from equip_group egp where egp.equip_group_id = egpt.equip_group_id and egp.status = ?) ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and egpt.product_id = ? ");
		params.add(productId);
		sql.append(" and exists (select 1 from product pt where pt.product_id = egpt.product_id and pt.status = ?) ");
		params.add(ActiveType.RUNNING.getValue());
		if (equipmentGroupProductStatus != null) {
			sql.append(" and egpt.status = ? ");
			params.add(equipmentGroupProductStatus.getValue());
		}
		return repo.getEntityBySQL(EquipGroupProduct.class, sql.toString(), params);
	}

	@Override
	public EquipGroupProduct retrieveEquipmentGroupProduct(String equipmentGroupCode, String productCode, ActiveType equipmentGroupProductStatus) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from equip_group_product egpt ");
		sql.append(" where 1=1 ");
		sql.append(" and exists (select equip_group_id from equip_group egp where egp.code = ? and egp.status = ? and egp.equip_group_id = egpt.equip_group_id) ");
		params.add(equipmentGroupCode.trim().toUpperCase());
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and exists (select 1 from product pt where pt.status = ? and pt.product_id = egpt.product_id and pt.product_code = ?) ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(productCode.trim().toUpperCase());
		if (equipmentGroupProductStatus != null) {
			sql.append(" and egpt.status = ? ");
			params.add(equipmentGroupProductStatus.getValue());
		}
		return repo.getEntityBySQL(EquipGroupProduct.class, sql.toString(), params);
	}

	@Override
	public void retrieveProductsAndCategories(String productCode, String productName, Long equipmentGroupId, Boolean exceptProductsInZCategory, List<ProductTreeVO> outProducts, List<ProductTreeVO> outCategories, List<ProductTreeVO> outSubCategories)
			throws DataAccessException {
		StringBuilder prdSql = new StringBuilder();
		StringBuilder catSql = new StringBuilder();
		StringBuilder subSql = new StringBuilder();

		List<Object> params = new ArrayList<Object>();

		prdSql.append(" select product_id id, product_code code, (product_code || ' - ' || product_name) name, sub_cat_id parentId, convfact convfact from product p where p.status = 1");
		catSql.append(" select distinct cat_id id, '' code, (select product_info_code || ' - ' || product_info_name from product_info where product_info_id = p.cat_id) name, null parentId, null convfact from product p where status = 1 and cat_id is not null ");
		subSql.append(" select distinct sub_cat_id id, '' code, (select product_info_code || ' - ' || product_info_name from product_info where product_info_id = p.sub_cat_id) name, cat_id parentId, null convfact from product p where status = 1 and sub_cat_id is not null ");

		if (!StringUtility.isNullOrEmpty(productCode)) {
			prdSql.append(" and product_code like ? escape '/'");
			catSql.append(" and product_code like ? escape '/'");
			subSql.append(" and product_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(productCode.trim().toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(productName)) {
			prdSql.append(" and upper(product_name) like ? escape '/'");
			catSql.append(" and upper(product_name) like ? escape '/'");
			subSql.append(" and upper(product_name) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(productName.trim().toUpperCase()));
		}
		if (equipmentGroupId != null) {
			prdSql.append(" and not exists (select 1 from equip_group_product egpt where egpt.equip_group_id = ? and egpt.product_id = p.product_id and egpt.status = ?)");
			catSql.append(" and not exists (select 1 from equip_group_product egpt where egpt.equip_group_id = ? and egpt.product_id = p.product_id and egpt.status = ?)");
			subSql.append(" and not exists (select 1 from equip_group_product egpt where egpt.equip_group_id = ? and egpt.product_id = p.product_id and egpt.status = ?)");
			params.add(equipmentGroupId);
			params.add(ActiveType.RUNNING.getValue());
		}

		if (exceptProductsInZCategory) {
			prdSql.append(" and not exists (select 1 from product_info where product_info_id = p.cat_id and product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
			catSql.append(" and not exists (select 1 from product_info where product_info_id = p.cat_id and product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
			subSql.append(" and not exists (select 1 from product_info where product_info_id = p.cat_id and product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
		}

		prdSql.append(" order by name");
		catSql.append(" order by name");
		subSql.append(" order by name");

		String[] fieldNames = { "id", "code", "name", "parentId", "convfact" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };

		List<ProductTreeVO> products = repo.getListByQueryAndScalar(ProductTreeVO.class, fieldNames, fieldTypes, prdSql.toString(), params);
		outProducts.addAll(products);
		List<ProductTreeVO> categories = repo.getListByQueryAndScalar(ProductTreeVO.class, fieldNames, fieldTypes, catSql.toString(), params);
		outCategories.addAll(categories);
		List<ProductTreeVO> subCategories = repo.getListByQueryAndScalar(ProductTreeVO.class, fieldNames, fieldTypes, subSql.toString(), params);
		outSubCategories.addAll(subCategories);

	}

	@Override
	public List<EquipmentVO> getEquipStockTransFormDtlByEquipId(Long equipmentId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select equip_stock_trans_form_dtl_id as id from equip_stock_trans_form_dtl ");
		sql.append(" where 1=1 ");
		if (equipmentId != null && equipmentId > 0) {
			sql.append(" and equip_id = ? ");
			params.add(equipmentId);
		}
		final String[] fieldNames = new String[] { "id" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public void deleteEquipStockTransFormDtl(EquipStockTransFormDtl id) throws DataAccessException {
		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		repo.delete(id);
	}

//	@Override
//	public List<EquipmentDeliveryVO> getListEquipmentequipmentStockTransFormVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws DataAccessException {
//		StringBuilder sql = new StringBuilder();
//		StringBuilder whereSql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		sql.append(" select est.equip_stock_trans_form_id as idStockRecord, esf.equip_stock_trans_form_dtl_id as idStockRecordDtl,ec.name as typeEquipment, (eg.code || ' - ' || eg.name) as groupEquipmentCode, ");
//		sql.append(" (case when eg.capacity_from is null and eg.capacity_to is null then '' when eg.capacity_from is null then '<'||to_char(eg.capacity_to) when eg.capacity_to is null then '>'||to_char(eg.capacity_from) ");
//		sql.append(" when eg.capacity_to <> eg.capacity_from then to_char(eg.capacity_from) || ' - '|| to_char(eg.capacity_to) else to_char(eg.capacity_from) end) as capacity, ");
//		sql.append(" eq.code as equipmentCode, eq.serial as seriNumber, ");
//		sql.append(" (case when eq.health_status is not null then (select min(value) from ap_param ap where ap.status = 1 and ap.ap_param_code like eq.health_status and type like 'EQUIP_CONDITION')else '' end) as healthStatus ");
//		sql.append(" from equip_stock_trans_form est ");
//		sql.append(" join equip_stock_trans_form_dtl esf on est.equip_stock_trans_form_id = esf.equip_stock_trans_form_id ");
//		sql.append(" join equipment eq on eq.equip_id = esf.equip_id ");
//		sql.append(" join equip_group eg on eg.equip_group_id = eq.equip_group_id ");
//		sql.append(" join equip_category ec on eg.equip_category_id = ec.equip_category_id ");
//		sql.append(" where 1=1 ");
//		if (equipmentFilter.getEquipStockTransFormId() != null) {
//			sql.append(" and esf.equip_stock_trans_form_id = ? ");
//			params.add(equipmentFilter.getEquipStockTransFormId());
//		}
//		if (!StringUtility.isNullOrEmpty(equipmentFilter.getCode())) {
//			sql.append(" and est.code = ? ");
//			params.add(equipmentFilter.getCode().trim().toUpperCase());
//		}
//		if (equipmentFilter.getFromStockId() != null) {
//			sql.append(" and est.from_stock_id = ? ");
//			params.add(equipmentFilter.getFromStockId());
//		}
//		sql.append(" order by equipmentCode ");
//		String[] fieldNames = { "idStockRecord",// 0
//				"idStockRecordDtl",// 1
//				"typeEquipment",// 2
//				"groupEquipmentCode",// 3				
//				"capacity",// 4			
//				"seriNumber",// 5				
//				"healthStatus",// 6				
//				"equipmentCode",// 7				
//		};
//		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
//				StandardBasicTypes.LONG,// 1
//				StandardBasicTypes.STRING,// 2
//				StandardBasicTypes.STRING,// 3
//				StandardBasicTypes.STRING,// 4
//				StandardBasicTypes.STRING,// 5
//				StandardBasicTypes.STRING,// 6
//				StandardBasicTypes.STRING,// 7				
//		};
//		StringBuilder countSql = new StringBuilder();
//		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");
//
//		if (equipmentFilter.getkPaging() != null) {
//			return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, equipmentFilter.getkPaging());
//		} else {
//			return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
//		}
//	}
	@Override
	public List<EquipmentDeliveryVO> getListEquipmentequipmentStockTransFormVOByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select est.equip_stock_trans_form_id as idStockRecord, esf.equip_stock_trans_form_dtl_id as idStockRecordDtl,ec.name as typeEquipment, (eg.code || ' - ' || eg.name) as groupEquipmentCode, ");
		sql.append(" (case when eg.capacity_from is null and eg.capacity_to is null then '' when eg.capacity_from is null then '<'||to_char(eg.capacity_to) when eg.capacity_to is null then '>'||to_char(eg.capacity_from) ");
		sql.append(" when eg.capacity_to <> eg.capacity_from then to_char(eg.capacity_from) || ' - '|| to_char(eg.capacity_to) else to_char(eg.capacity_from) end) as capacity, ");
		sql.append(" eq.code as equipmentCode, eq.serial as seriNumber, ");
		sql.append(" (select fromStock.code from equip_stock fromStock where fromStock.equip_stock_id = esf.from_stock_id) stock, ");
		sql.append(" (select toStock.code from equip_stock toStock where toStock.equip_stock_id = esf.to_stock_id) toStock, ");
		sql.append(" (case when eq.health_status is not null then (select min(value) from ap_param ap where ap.status = 1 and ap.ap_param_code like eq.health_status and type like 'EQUIP_CONDITION')else null end) as healthStatus, ");
		sql.append("  esf.perform_status performStatus");
		sql.append(" from equip_stock_trans_form est ");
		sql.append(" join equip_stock_trans_form_dtl esf on est.equip_stock_trans_form_id = esf.equip_stock_trans_form_id ");
		sql.append(" join equipment eq on eq.equip_id = esf.equip_id ");
		sql.append(" join equip_group eg on eg.equip_group_id = eq.equip_group_id ");
		sql.append(" join equip_category ec on eg.equip_category_id = ec.equip_category_id ");
		sql.append(" where 1=1 ");
		if (equipmentFilter.getEquipStockTransFormId() != null) {
			sql.append(" and esf.equip_stock_trans_form_id = ? ");
			params.add(equipmentFilter.getEquipStockTransFormId());
		}
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getCode())) {
			sql.append(" and est.code = ? ");
			params.add(equipmentFilter.getCode().trim().toUpperCase());
		}
		if (equipmentFilter.getFromStockId() != null) {
			sql.append(" and est.from_stock_id = ? ");
			params.add(equipmentFilter.getFromStockId());
		}
		sql.append(" order by equipmentCode ");
		String[] fieldNames = { "idStockRecord",// 0
				"idStockRecordDtl",// 1
				"typeEquipment",// 2
				"groupEquipmentCode",// 3				
				"capacity",// 4			
				"seriNumber",// 5				
				"healthStatus",// 6				
				"equipmentCode",// 7				
				"stock",// 8				
				"toStock",// 9				
				"performStatus",// 10
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7				
				StandardBasicTypes.STRING,// 8				
				StandardBasicTypes.STRING,// 9				
				StandardBasicTypes.INTEGER,// 10		
		};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");
		
		if (equipmentFilter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, equipmentFilter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public EquipStockTransForm updateEquipmentStockTransForm(EquipStockTransForm equipStockTransForm) throws DataAccessException {
		if (equipStockTransForm == null) {
			throw new IllegalArgumentException("equipStockTransForm is null");
		}
//		equipStockTransForm.setUpdateDate(commonDAO.getSysDate());
		repo.update(equipStockTransForm);
		return equipStockTransForm;
	}

	@Override
	public List<EquipmentDeliveryVO> getListEquipmentStockTransByFilter(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		//	StringBuilder whereSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ec.name as typeEquipment, eg.code as groupEquipmentCode, eg.name as groupEquipmentName, ");
		sql.append(" (case when eg.capacity_from is null and eg.capacity_to is null then '' when eg.capacity_from is null then '<'||to_char(eg.capacity_to) when eg.capacity_to is null then '>'||to_char(eg.capacity_from) ");
		sql.append(" when eg.capacity_to <> eg.capacity_from then to_char(eg.capacity_from) || ' - '|| to_char(eg.capacity_to) else to_char(eg.capacity_from) end) as capacity, ");
		sql.append(" eq.code as equipmentCode, eq.serial as seriNumber, ");
		sql.append(" eq.equip_id as equipmentId, ");
		sql.append(" (select fromStock.code from equip_stock fromStock where fromStock.equip_stock_id = eq.stock_id) stock, ");
		sql.append(" (case when eq.health_status is not null then (select min(value) from ap_param ap where ap.status = 1 and ap.ap_param_code like eq.health_status and type like 'EQUIP_CONDITION')else null end) as healthStatus ");
		sql.append(" from equipment eq ");
		sql.append(" join equip_group eg on eg.equip_group_id = eq.equip_group_id ");
		sql.append(" join equip_category ec on eg.equip_category_id = ec.equip_category_id ");
		sql.append(" where 1=1 and eq.status = 1 ");
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getStockCode())) {
			sql.append(" and upper(eq.stock_code) like ? ESCAPE '/'");
			params.add(equipmentFilter.getStockCode().toUpperCase());
		}
		if (equipmentFilter.getTradeStatus() != null) {
			sql.append(" and eq.trade_status = ?");
			params.add(equipmentFilter.getTradeStatus());
		}
		if (equipmentFilter.getLstEquipId() != null && equipmentFilter.getLstEquipId().size() > 0) {
			sql.append(" and eq.equip_id not in (-1");
			List<Long> lstP = equipmentFilter.getLstEquipId();
			for (Long pId : lstP) {
				sql.append(",?");
				params.add(pId);
			}
			sql.append(")");
		}
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getCode())) {
			sql.append(" and upper(eq.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipmentFilter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getSeriNumber())) {
			sql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
			params.add(equipmentFilter.getSeriNumber().trim().toUpperCase());
		}
		String[] fieldNames = { "equipmentCode",// 0
				"typeEquipment",// 1				
				"groupEquipmentCode",// 2				
				"groupEquipmentName",// 3				
				"capacity",// 4							
				"seriNumber",// 5				
				"healthStatus",// 6	
				"equipmentId",//7
				"stock",//8
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6		
				StandardBasicTypes.LONG, //7
				StandardBasicTypes.STRING, //8
		};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");
		if (equipmentFilter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, equipmentFilter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<EquipRepairFormDtl> getEquipRepairFormDtlByEquipRepairFormId(Long equipRepairFormId) throws DataAccessException {
		if (equipRepairFormId == null) {
			throw new IllegalArgumentException("equipRepairFormId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_repair_form_dtl where equip_repair_form_id = ? ");
		params.add(equipRepairFormId);
		return repo.getListBySQL(EquipRepairFormDtl.class, sql.toString(), params);
	}

	@Override
	public void deleteEquipRepairFormDtl(EquipRepairFormDtl equipRepairFormDtl) throws DataAccessException {
		if (equipRepairFormDtl == null) {
			throw new IllegalArgumentException("equipRepairFormDtl");
		}
		repo.delete(equipRepairFormDtl);
	}

	/***
	 * Lay EquipRepairPayFormBy Code
	 * @author vuongmq
	 * @date 19/04/2015
	 * @return EquipRepairPayForm
	 */
	@Override
	public EquipRepairPayForm getEquipRepairPayFormByCode(String code) throws DataAccessException {
		if (code == null) {
			throw new IllegalArgumentException("EquipRepairPayForm code is null");
		}
		String sql = "select * from equip_repair_pay_form where code = ? ";
		List<Object> params = new ArrayList<Object>();
		params.add(code.trim().toUpperCase());
		EquipRepairPayForm rs = repo.getEntityBySQL(EquipRepairPayForm.class, sql, params);
		return rs;
	}
	
	@Override
	public EquipRepairPayForm retrieveEquipmentRepairPaymentRecord(String equipmentRepairPaymentRecord) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_repair_pay_form where code like ? ESCAPE '/' ");
		params.add(StringUtility.toOracleSearchLikeSuffix(equipmentRepairPaymentRecord.trim().toUpperCase()));

		return repo.getEntityBySQL(EquipRepairPayForm.class, sql.toString(), params);
	}

	@Override
	public List<EquipRepairPayFormDtl> retrieveEquipmentRepairPaymentDetails(Long equipmentRepairFormId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from equip_repair_pay_form_dtl erpfdl ");
		sql.append(" where 1=1 ");
		sql.append(" and exists ( ");
		sql.append(" 	select 1 ");
		sql.append(" 	from equip_repair_pay_form erpfm ");
		sql.append(" 	where erpfm.equip_repair_form_id = ? ");
		params.add(equipmentRepairFormId);
		sql.append(" 	and erpfdl.equip_repair_pay_form_id = erpfm.equip_repair_pay_form_id ");
		sql.append(" ) ");
		return repo.getListBySQL(EquipRepairPayFormDtl.class, sql.toString(), params);
	}
	
	/**
	 * Lay danh sach chi tiet cua phieu thanh toan;
	 * @author vuongmq
	 * @since 24/06/2015
	 * @return
	 */
	@Override
	public List<EquipRepairPayFormDtl> getEquipmentRepairPaymentDtlFilter(EquipRepairFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_repair_pay_form_dtl erpfdl ");
		sql.append(" where 1=1 ");
		if (filter.getId() != null) {
			sql.append(" and erpfdl.equip_repair_pay_form_id = ? ");
			params.add(filter.getId());
		}
		if (filter.getEquipRepairId() != null) {
			sql.append(" and erpfdl.equip_repair_form_id = ? ");
			params.add(filter.getEquipRepairId());
		}
		return repo.getListBySQL(EquipRepairPayFormDtl.class, sql.toString(), params);
	}
	
	@Override
	public EquipRepairPayForm retrieveEquipmentRepairPayment(Long equipmentRepairFormId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		//sql.append(" select * from equip_repair_pay_form where equip_repair_form_id = ? ");
		sql.append(" select * from equip_repair_pay_form where equip_repair_pay_form_id = ? ");
		params.add(equipmentRepairFormId);
		return repo.getEntityBySQL(EquipRepairPayForm.class, sql.toString(), params);
	}

	@Override
	public List<EquipmentDeliveryVO> getCheckFromStockEquipment(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select equip_id as equipmentId, (select fromStock.code from equip_stock fromStock where fromStock.equip_stock_id = equipment.stock_id) stock from equipment where 1=1 ");
		sql.append(" and trade_status =0 ");
		sql.append(" and status =1 ");
		sql.append(" and usage_status = 2 ");
		sql.append(" and stock_type =? ");
		params.add(EquipStockTotalType.KHO.getValue());
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getStockCode())) {
			sql.append(" and upper(stock_code) like ? ESCAPE '/'");
			params.add(equipmentFilter.getStockCode().trim().toUpperCase());
		}
		String[] fieldNames = { "stock",// 0				
				"equipmentId",//1
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING,// 1		
				StandardBasicTypes.LONG, //0
		};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");
		if (equipmentFilter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, equipmentFilter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	/**
	 * get list customer by shop
	 * @author phut
	 */
	@Override
	public List<Customer> retrieveCustomersOfShopsInEquipmentStatisticRecord(Long equipmentStatisticRecordId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from customer c ");
		sql.append(" where c.customer_id IN (select CUSTOMER_ID from EQUIP_STATISTIC_CUSTOMER where EQUIP_STATISTIC_RECORD_ID = ?)");
		params.add(equipmentStatisticRecordId);
		return repo.getListBySQL(Customer.class, sql.toString(), params);
	}
	
	/**
	 * get list shop join record
	 */
	@Override
	public List<Shop> retrieveShopsOfShopsInEquipmentStatisticRecord(Long equipmentStatisticRecordId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from shop sh ");
		sql.append(" where sh.shop_id IN (select UNIT_ID from EQUIP_STATISTIC_UNIT where EQUIP_STATISTIC_RECORD_ID = ?)");
		params.add(equipmentStatisticRecordId);
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<EquipStatisticRecDtl> getEquipmentStatisticRecordDetails(Long equipmentStatisticRecordId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from equip_statistic_rec_dtl esrtl ");
		sql.append(" where 1=1 ");
		sql.append(" and esrtl.equip_statistic_record_id = ? ");
		params.add(equipmentStatisticRecordId);
		return repo.getListBySQL(EquipStatisticRecDtl.class, sql.toString(), params);
	}

	@Override
	public Equipment getEquipmentById(Equipment equip) throws DataAccessException {
		return repo.getEntityById(Equipment.class, equip);
	}

	@Override
	public List<EquipmentDeliveryVO> searchRecordStockChangeByEquipment(EquipmentFilter<EquipmentDeliveryVO> equipmentFilter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select DISTINCT eq.equip_id as equipmentId, ");
		sql.append(" eq.code as equipmentCode, ");
		sql.append(" eq.serial as seriNumber, ");
		sql.append(" (case when eq.health_status is not null then (select min(value) from ap_param ap where ap.status = 1 and ap.ap_param_code like eq.health_status and type like 'EQUIP_CONDITION')else '' end) as healthStatus, ");
		sql.append(" (select sh.shop_code from shop sh where sh.equip_stock_code = eq.stock_code and sh.status = 1) as stockCode, ");
		sql.append(" (select sh.shop_name from shop sh where sh.equip_stock_code = eq.stock_code and sh.status = 1) as stockName, ");
		sql.append(" ec.name as typeEquipment, ");
		sql.append(" eg.code as groupEquipmentCode, ");
		sql.append(" eg.name as groupEquipmentName, ");
		sql.append(" (case when eg.capacity_from is null and eg.capacity_to is null then '' when eg.capacity_from is null then '<'||to_char(eg.capacity_to) when eg.capacity_to is null then '>'||to_char(eg.capacity_from) ");
		sql.append(" when eg.capacity_to <> eg.capacity_from then to_char(eg.capacity_from) || ' - '|| to_char(eg.capacity_to) else to_char(eg.capacity_from) end) as capacity, ");
		sql.append(" eg.brand_name as equipmentBrand ");
		sql.append(" from equipment eq ");
		sql.append(" left join equip_group eg on eg.equip_group_id = eq.equip_group_id ");
		sql.append(" left join equip_category ec on eg.equip_category_id = ec.equip_category_id ");
		sql.append(" left join equip_provider ep on ep.equip_provider_id = eq.equip_provider_id ");
		sql.append(" where 1=1 ");
		sql.append(" and eq.equip_id = ? ");
		params.add(equipmentFilter.getEquipId());
		String[] fieldNames = { "equipmentId",// 0
				"equipmentCode",// 1
				"seriNumber",// 2
				"healthStatus",// 3
				"stockCode",// 4
				"stockName",// 5
				"typeEquipment",// 6
				"groupEquipmentCode",// 7
				"groupEquipmentName",// 8
				"capacity",// 9
				"equipmentBrand",// 10			
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.STRING,// 10			
		};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");

		if (equipmentFilter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, equipmentFilter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public BigDecimal getRepairCountEquip(Long equipId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/** vuongmq; 10/06/2015; lay so lan sua chua thiet bi cho chinh xac; Defect 227832; */
		//sql.append(" select count(1)+1 from equip_repair_form f join equipment e on e.equip_id = f.equip_id");
		sql.append(" SELECT nvl(max(f.repair_count),0)+1 from equip_repair_form f join equipment e on e.equip_id = f.equip_id");
		sql.append(" where f.status = ? and f.equip_id = ?");
		params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
		params.add(equipId);
		return (BigDecimal) repo.getObjectByQuery(sql.toString(), params);
	}

	@Override
	public BigDecimal getRepairCountEquipItem(Long equipItemId, Long equipId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/** vuongmq; 10/06/2015; lay so lan sua chua thiet bi cho chinh xac; Defect 227832; */
		//sql.append(" select count(1)+1 from equip_repair_form f join equip_repair_form_dtl dt on dt.equip_repair_form_id = f.equip_repair_form_id");
		sql.append(" SELECT nvl(max(dt.repair_count),0)+1 from equip_repair_form f join equip_repair_form_dtl dt on dt.equip_repair_form_id = f.equip_repair_form_id");
		sql.append(" where f.status = ? and dt.equip_item_id = ? and f.equip_id = ?");
		params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
		params.add(equipItemId);
		params.add(equipId);
		return (BigDecimal) repo.getObjectByQuery(sql.toString(), params);
	}

	/**
	 * Lay danh sach bien ban thu hoi va thanh ly cho export
	 * 
	 * @author tamvnm
	 * @since 12/01/2015
	 * @param equipmentFilter
	 * @throws BusinessException
	 */
	@Override
	public List<EquipmentEvictionVO> getListEquipmentEvictionVOForExport(Long evictionId) throws DataAccessException {
		StringBuffer sql = new StringBuffer();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT data.shop_code        AS maNPP, ");
		sql.append("       data.shop_name       AS tenNPP, ");
		sql.append("       data.short_code       AS maKH, ");
		sql.append("       data.create_form_date AS createFormDate, ");
		sql.append("       data.from_representor AS ngDaiDienKhachHang, ");
		sql.append("       data.code             AS maThietBi, ");
		sql.append("       data.to_representator AS ngDaiDienThuHoi, ");
		sql.append("       khocodeshop.code      AS shopStockcode, ");
		sql.append("       data.reason           AS lyDo, ");
		sql.append("       data.form_status      AS recordStatus, ");
		sql.append("       data.action_status    AS deliveryStatus, ");
		sql.append("       data.note, ");
		sql.append("       data.maphieu          AS maPhieu ");
		sql.append("FROM   (SELECT evf.code AS maPhieu, ");
		sql.append("               s.shop_code, ");
		sql.append("               s.shop_name, ");
		sql.append("               c.short_code, ");
		sql.append("               Evf.create_form_date, ");
		sql.append("               evf.from_representor, ");
		sql.append("               e.code, ");
		sql.append("               evf.to_representator, ");
		sql.append("               evf.to_stock_id, ");
		sql.append("               evf.to_stock_type, ");
		sql.append("               evf.reason, ");
		sql.append("               evf.form_status, ");
		sql.append("               evf.action_status, ");
		sql.append("               evf.note ");
		sql.append("        FROM   equip_eviction_form evf, ");
		sql.append("               equip_eviction_form_dtl evfd, ");
		sql.append("               customer c, ");
		sql.append("               shop s, ");
		sql.append("               equipment e ");
		sql.append("        WHERE  evf.equip_eviction_form_id = ? ");
		params.add(evictionId);
		sql.append("               AND c.status = 1 ");
		sql.append("               AND e.status = 1 ");
		sql.append("               AND s.status = 1 ");
		sql.append("               AND evf.shop_id = s.shop_id ");
		sql.append("               AND evf.equip_eviction_form_id = evfd.equip_eviction_form_id ");
		sql.append("               AND evfd.equip_id = e.equip_id ");
		sql.append("               AND evf.customer_id = c.customer_id) data ");
		sql.append("       LEFT JOIN (SELECT equip_stock_id, ");
		sql.append("                         code ");
		sql.append("                  FROM   equip_stock) khocodeshop ");
		sql.append("              ON khocodeshop.equip_stock_id = data.to_stock_id ");
		sql.append("                 AND data.to_stock_type IN ( 1, 2 ) ");


		String[] fieldNames = { 
				"maNPP", 				// 0
				"tenNPP", 				// 1
				"maKH", 				// 2
				"createFormDate",		// 3
				"ngDaiDienKhachHang", 	// 4
				"maThietBi", 			// 5
				"ngDaiDienThuHoi", 		// 6
				"shopStockcode", 		// 7
				"lyDo",					// 8
				"recordStatus", 	// 9
				"deliveryStatus",	// 10
				"note",
				"maPhieu" 				// 11
				
		};

		Type[] fieldTypes = {
				StandardBasicTypes.STRING, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.DATE,   // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.STRING, // 7
				StandardBasicTypes.STRING, // 8
				StandardBasicTypes.INTEGER, // 9
				StandardBasicTypes.INTEGER, // 10
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING // 11
		};

		return repo.getListByQueryAndScalar(EquipmentEvictionVO.class, fieldNames, fieldTypes, sql.toString(), params);

	}

	/**
	 * Lay danh sach bien ban thu hoi va thanh ly cho in
	 * 
	 * @author phuongvm
	 * @since 16/01/2015
	 * @param evictionId
	 * @throws BusinessException
	 */
	@Override
	public List<EquipmentEvictionVO> getListEquipmentEvictionVOForPrint(Long evictionId) throws DataAccessException {
		StringBuffer sql = new StringBuffer();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT dt.equip_eviction_form_dtl_id                                      AS id, ");
		sql.append("       c.customer_name                                                    AS ");
		sql.append("       tenKH, ");
		sql.append("       c.short_code                                                       AS ");
		sql.append("       maKH, ");
		sql.append("        c.customer_id                      as customerId, ");
//		sql.append("       c.phone ");
//		sql.append("       || '/' ");
//		sql.append("       || c.mobiphone                                                     AS ");
//		sql.append("       dienThoai, ");
		sql.append("       decode(c.phone, null, c.mobiphone, c.phone) AS dienThoai, ");
		sql.append("       c.address                                                          AS ");
		sql.append("       diaChi, ");
		sql.append("       f.from_representor                                                 AS ");
		sql.append("       ngDaiDienKhachHang, ");
		sql.append("       (SELECT ap.value ");
		sql.append("        FROM   equip_param ap ");
		sql.append("        WHERE  Ap.equip_param_code = f.reason ");
		sql.append("               AND Ap.type = ?)                                           AS ");
		params.add(ApParamEquipType.EQUIP_EVICTION_REASON_TYPE.getValue());
		sql.append("       lyDo, ");
//		sql.append("       c1.customer_name                                                   AS ");
//		sql.append("       tenKHNew, ");
//		sql.append("       c1.short_code                                                      AS ");
//		sql.append("       maKHNew, ");
//		sql.append("       c1.address                                                         AS ");
//		sql.append("       diaChiNew, ");
		sql.append("       (SELECT rc.contract_number ");
		sql.append("        FROM   equip_delivery_record rc ");
		sql.append("        WHERE  rc.equip_delivery_record_id = dt.equip_delivery_record_id) AS ");
		sql.append("       soHopDong, ");
		sql.append("       ec.NAME                                                            AS ");
		sql.append("       loaiTB, ");
		sql.append("       eg.NAME                                                            AS ");
		sql.append("       tenTB, ");
		sql.append("       e.serial                                                           AS ");
		sql.append("       soSeri, ");
		sql.append("       (SELECT value ");
		sql.append("        FROM   ap_param ");
		sql.append("        WHERE  ap_param_code = dt.health_status)                          AS ");
		sql.append("       tinhTrang, ");
		sql.append("       dt.total                                                           AS ");
		sql.append("       soLuong, ");
//		sql.append("       f.current_postion                                                  AS ");
//		sql.append("       vitri, ");
		sql.append("       f.to_representator                                                 AS ");
		sql.append("       ngDaiDienThuHoi ");
		sql.append("FROM   equip_eviction_form f ");
		sql.append("       JOIN equip_eviction_form_dtl dt ");
		sql.append("         ON dt.equip_eviction_form_id = f.equip_eviction_form_id ");
		sql.append("       JOIN equipment e ");
		sql.append("         ON e.equip_id = dt.equip_id ");
		sql.append("       JOIN equip_group eg ");
		sql.append("         ON eg.equip_group_id = e.equip_group_id ");
		sql.append("       JOIN equip_category ec ");
		sql.append("         ON ec.equip_category_id = eg.equip_category_id ");
		sql.append("       JOIN customer c ");
		sql.append("         ON c.customer_id = f.customer_id ");
//		sql.append("       LEFT JOIN customer c1 ");
//		sql.append("              ON c1.customer_id = f.new_customer_id ");
		sql.append("       JOIN shop s ");
		sql.append("         ON s.shop_id = f.shop_id ");
		sql.append("WHERE  f.equip_eviction_form_id = ?");
		params.add(evictionId);

		String[] fieldNames = { "id", // 1
				"tenKH", // 2
				"customerId",
				"maKH", // 3
				"dienThoai", // 4
				"diaChi", // 5
				"ngDaiDienKhachHang", // 6
				"lyDo", // 7
//				"tenKHNew", // 8
//				"maKHNew", // 9	
//				"diaChiNew", // 10
				"soHopDong", // 11
				"loaiTB", // 12
				"tenTB", // 13
				"soSeri", // 14							
				"tinhTrang", // 15
				"soLuong", // 16
//				"viTri", // 17
				"ngDaiDienThuHoi", // 18
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.STRING, // 7
//				StandardBasicTypes.STRING, // 8
//				StandardBasicTypes.STRING, // 9
//				StandardBasicTypes.STRING, // 10
				StandardBasicTypes.STRING, // 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.STRING, // 13
				StandardBasicTypes.STRING, // 14
				StandardBasicTypes.STRING, // 15
				StandardBasicTypes.INTEGER, // 16
//				StandardBasicTypes.INTEGER, // 17
				StandardBasicTypes.STRING,

		};

		return repo.getListByQueryAndScalar(EquipmentEvictionVO.class, fieldNames, fieldTypes, sql.toString(), params);

	}

	@Override
	public List<EquipmentVO> getEquipStockTransFormDtlByIdEquip(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select equip_id as id from equip_stock_trans_form_dtl");
		sql.append(" where 1=1 ");
		if (filter.getId() != null && filter.getId() > 0) {
			sql.append(" and equip_stock_trans_form_id = ? ");
			params.add(filter.getId());
		}
		final String[] fieldNames = new String[] { "id" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}


	/** vuongmq; 16/03/2015; lay theo code*/
	@Override
	public Integer getEquipStockOrdinalMaxByShop(Long id) 	throws DataAccessException {
		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		String sql = "select max(s.ordinal) from equip_stock s where s.shop_id = ? ";
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		Object value = repo.getObjectByQuery( sql.toString(), params);
		if(value != null){
			return (Integer) value;
		} else {
			return 0;
		}
	}

	@Override
	public List<EquipStock> getEquipStockByShopAndOrdinal(Long id, Integer stt) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_stock s where 1 = 1 ");
		if(id != null){
			sql.append(" and s.shop_id = ? ");
			params.add(id);
		}
		if(stt != null){
			sql.append(" and s.ordinal = ? ");
			params.add(stt);
		}
		return repo.getListBySQL(EquipStock.class, sql.toString(), params);
	}
	@Override
	public EquipStock getEquipStockbyCode(String code) throws DataAccessException {
		if (code == null) {
			throw new IllegalArgumentException("code is null");
		}
		String sql = "select * from equip_stock where upper(code)=? and status != ? ";
		List<Object> params = new ArrayList<Object>();
		params.add(code.trim().toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		return repo.getEntityBySQL(EquipStock.class, sql, params);
	}
	@Override
	public List<EquipStock> getListEquipmentStockByFilter(EquipmentFilter<EquipStock> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select es.* from equip_stock es ");
		sql.append(" join shop s on es.shop_id = s.shop_id ");
		sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s.shop_id = ogr.number_key_id ");
		params.add(filter.getStaffRootId());
		params.add(filter.getRoleId());
		params.add(filter.getShopRootId());
		
		sql.append(" where 1=1 ");
		if(!StringUtility.isNullOrEmpty(filter.getCode())){
			sql.append(" and upper(es.code) like ? ESCAPE '/'  ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCode()).toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getName())){
			sql.append(" and unicode2english(es.name) like ? ESCAPE '/'  ");
			/** function trong DB: unicode2english(es.name) tu chuyen co dau thanh khong dau va lower*/
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getName()).toLowerCase()));
		}
		if(filter.getStatus() != null && filter.getStatus() != -1){
			sql.append(" and es.status = ?  ");
			params.add(filter.getStatus());
		} else {
			/** lay tat ca cac trang thai status != -1*/
			sql.append(" and es.status != -1 ");
		}
		/** vi lay entities nen search theo dk shopCode thi nhu the nay*/
		/*if(filter.getShopCode() != null){
			sql.append(" and es.shop_id in ( ");
			sql.append(" select s.shop_id from shop s where upper(s.shop_code) like ? ESCAPE '/' ");
			sql.append(" ) ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getShopCode()).toUpperCase()));
		}*/
		if(filter.getShopCode() != null){
			sql.append(" and upper(s.shop_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getShopCode()).toUpperCase()));
		}
		if(filter.getFromDate() != null){
			sql.append(" and es.create_date >= trunc(?)  ");
			params.add(filter.getFromDate());
		}
		if(filter.getToDate() != null){
			sql.append(" and es.create_date < trunc(?) + 1  ");
			params.add(filter.getToDate());
		}
		//sql.append(" order by es.create_date desc,  s.shop_code, es.ordinal, es.code ");
		sql.append(" order by s.shop_code, es.ordinal, es.code ");
		if(filter.getkPaging() != null){
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(*) as count  from ( ");
			countSql.append(sql);
			countSql.append(" ) ");
			return repo.getListBySQLPaginated(EquipStock.class, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListBySQL(EquipStock.class, sql.toString(), params);
		}
	}
	/**
	 * cay don vi view
	 * 
	 * @author hoanv25
	 * @date Mar 19,2015
	 * */
	@Override
	public List<ShopViewParentVO> searchShopTreeCustomer(Long pShopId, String shopCode, String shopName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		if (StringUtility.isNullOrEmpty(shopCode) && StringUtility.isNullOrEmpty(shopName)) {
			sql.append("select s.shop_id as id, s.parent_shop_id as parentId,");
			sql.append(" s.shop_code as shopCode, s.shop_name as shopName,");
			sql.append(" (select count(1) from channel_type where channel_type_id = s.shop_type_id and status = ? and object_type in (?, ?)) as isNPP");
			params.add(ActiveType.RUNNING.getValue());
			params.add(ShopObjectType.NPP.getValue());
			params.add(ShopObjectType.NPP_KA.getValue());
			sql.append(" from shop s");
			sql.append(" where 1 = 1");
			sql.append(" and shop_id in (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id = ? ");
			sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id) ");
			params.add(pShopId);
			sql.append(" and status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" order by shopCode, shopName");
		} else {
			sql.append("with lstShopTmp as (");
			sql.append(" select shop_id from shop");
			sql.append(" where status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" start with shop_id = ?");
			params.add(pShopId);
			sql.append(" connect by prior shop_id = parent_shop_id");
			sql.append(" ),");
			sql.append(" lstDVTmp as (");
			sql.append(" select distinct s.shop_id as id, s.parent_shop_id as parentId,");
			sql.append(" s.shop_code as shopCode, s.shop_name as shopName,");
			sql.append(" (select object_type from channel_type where channel_type_id = s.shop_type_id and status = ?) as shopType");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" from shop s");
			sql.append(" where shop_id in (select shop_id from lstShopTmp)");
			sql.append(" start with 1=1");
			
			if (!StringUtility.isNullOrEmpty(shopCode)) {
				sql.append(" and lower(shop_code) like ? escape '/'");
				String s = shopCode.toLowerCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (!StringUtility.isNullOrEmpty(shopName)) {
				sql.append(" and lower(shop_name) like ? escape '/'");
				String s = shopName.toLowerCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			
			sql.append(" connect by prior parent_shop_id = shop_id");
			sql.append(" ),");
			sql.append(" lstDV as (");
			sql.append(" select id, parentId, shopCode, shopName,");
			sql.append(" (case when shopType = ? then 0");
			params.add(ShopObjectType.VNM.getValue());
			sql.append(" when shopType in (?, ?, ?) then 1");
			params.add(ShopObjectType.GT.getValue());
			params.add(ShopObjectType.KA.getValue());
			params.add(ShopObjectType.ST.getValue());
			sql.append(" when shopType in (?, ?, ?) then 2");
			params.add(ShopObjectType.MIEN.getValue());
			params.add(ShopObjectType.MIEN_KA.getValue());
			params.add(ShopObjectType.MIEN_ST.getValue());
			sql.append(" when shopType in (?, ?) then 3");
			params.add(ShopObjectType.VUNG.getValue());
			params.add(ShopObjectType.VUNG_KA.getValue());
			sql.append(" when shopType in (?, ?) then 4");
			params.add(ShopObjectType.NPP.getValue());
			params.add(ShopObjectType.NPP_KA.getValue());
			sql.append(" end) as orderNo");
			sql.append(" from lstDVTmp");
			sql.append(" )");
			sql.append(" select id, parentId, shopCode, shopName,");
			sql.append(" (case orderNo when 4 then 1 else 0 end) as isNPP");
			sql.append(" from lstDV");
			sql.append(" order by isNPP, orderNo, shopCode, shopName");
		}
		
		String[] fieldNames = new String[] {
				"id",
				"parentId",
				"shopCode",
				"shopName",
				"isNPP"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER
		};
		
		List<ShopViewParentVO> lst = repo.getListByQueryAndScalar(ShopViewParentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
	/**
	 * cay don vi view
	 * 
	 * @author vuongmq
	 * @date Mar 19,2015
	 * */
	@Override
	public List<ShopViewParentVO> searchShopTreeView(Long pShopId, String shopCode, String shopName, EquipmentFilter<ShopViewParentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		if (StringUtility.isNullOrEmpty(shopCode) && StringUtility.isNullOrEmpty(shopName)) {
			sql.append("select s.shop_id as id, s.parent_shop_id as parentId,");
			sql.append(" s.shop_code as shopCode, s.shop_name as shopName,");
//			sql.append(" (select count(1) from channel_type where channel_type_id = s.shop_type_id and status = ? and object_type in (?, ?)) as isNPP");
			sql.append("(SELECT COUNT(1) ");
			sql.append("  FROM shop_type ");
			sql.append("  WHERE shop_type_id = s.shop_type_id ");
			sql.append("  AND status            = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("  AND SPECIFIC_TYPE = ? ");
			params.add(ShopSpecificType.NPP.getValue());
			sql.append("  ) AS isNPP");
			sql.append(" from shop s");
			sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s.shop_id = ogr.number_key_id  ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
			
			sql.append(" where parent_shop_id = ?");
			params.add(pShopId);
			sql.append(" and status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" order by shopCode, shopName");
		} else {
			sql.append("with lstShopTmp as (");
			sql.append(" select shop_id from shop");
			sql.append(" where status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" start with shop_id = ?");
			params.add(pShopId);
			sql.append(" connect by prior shop_id = parent_shop_id");
			sql.append(" ),");
			sql.append(" lstDVTmp as (");
			sql.append(" select distinct s.shop_id as id, s.parent_shop_id as parentId,");
			sql.append(" s.shop_code as shopCode, s.shop_name as shopName,");
//			sql.append(" (select object_type from channel_type where channel_type_id = s.shop_type_id and status = ?) as shopType");
//			params.add(ActiveType.RUNNING.getValue());
			sql.append("(SELECT COUNT(1) ");
			sql.append("  FROM shop_type ");
			sql.append("  WHERE shop_type_id = s.shop_type_id ");
			sql.append("  AND status            = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("  AND SPECIFIC_TYPE = ? ");
			params.add(ShopSpecificType.NPP.getValue());
			sql.append("  ) AS isNPP");
			
			sql.append(" from shop s");
			sql.append(" where shop_id in (select shop_id from lstShopTmp)");
			sql.append(" start with 1=1");
			
			if (!StringUtility.isNullOrEmpty(shopCode)) {
				sql.append(" and lower(shop_code) like ? escape '/'");
				String s = shopCode.toLowerCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (!StringUtility.isNullOrEmpty(shopName)) {
				sql.append(" and lower(shop_name) like ? escape '/'");
				String s = shopName.toLowerCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			
			sql.append(" connect by prior parent_shop_id = shop_id");
			sql.append(" ),");
			sql.append(" lstDV as (");
			sql.append(" select id, parentId, shopCode, shopName,");
			sql.append(" isNPP ");
//			sql.append(" (case when shopType = ? then 0");
//			params.add(ShopObjectType.VNM.getValue());
//			sql.append(" when shopType in (?, ?, ?) then 1");
//			params.add(ShopObjectType.GT.getValue());
//			params.add(ShopObjectType.KA.getValue());
//			params.add(ShopObjectType.ST.getValue());
//			sql.append(" when shopType in (?, ?, ?) then 2");
//			params.add(ShopObjectType.MIEN.getValue());
//			params.add(ShopObjectType.MIEN_KA.getValue());
//			params.add(ShopObjectType.MIEN_ST.getValue());
//			sql.append(" when shopType in (?, ?) then 3");
//			params.add(ShopObjectType.VUNG.getValue());
//			params.add(ShopObjectType.VUNG_KA.getValue());
//			sql.append(" when shopType in (?, ?) then 4");
//			params.add(ShopObjectType.NPP.getValue());
//			params.add(ShopObjectType.NPP_KA.getValue());
//			sql.append(" end) as orderNo");
			sql.append(" from lstDVTmp");
			sql.append(" )");
			sql.append(" select id, parentId, shopCode, shopName,");
//			sql.append(" (case orderNo when 4 then 1 else 0 end) as isNPP");
			sql.append(" isNPP");
			sql.append(" from lstDV");
			sql.append(" order by isNPP, shopCode, shopName");
		}
		
		String[] fieldNames = new String[] {
				"id",
				"parentId",
				"shopCode",
				"shopName",
				"isNPP"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER
		};
		
		List<ShopViewParentVO> lst = repo.getListByQueryAndScalar(ShopViewParentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
	@Override
	public Equipment getEquipmentByEquipId(Long equipId) throws DataAccessException {
		return repo.getEntityById(Equipment.class, equipId);
	}

	@Override
	public List<Equipment> retrieveRemovedEquipmentFromStockTransRecord(Long equipStockTransFormId, List<Long> addEquipmentIds) throws DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}

	/***
	 * @author vuongmq
	 * @date 20/04/2015
	 * lay equip_repair_form_dtl theo filter, 
	 * lay hang muc con thoi gian bao hanh
	 */
	@Override
	public EquipRepairFormDtl getEquipRepairFormDtlByFilter(EquipItemFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select erfd.* ");
		sql.append(" from equip_repair_form erf ");
		sql.append(" join equip_repair_form_dtl erfd on erfd.equip_repair_form_id = erf.equip_repair_form_id ");
		sql.append(" where 1 = 1 ");
		if (filter.getEquipId() != null) {
			sql.append(" and erf.equip_id = ? ");
			params.add(filter.getEquipId());
		}
		if (filter.getEpuipItemId() != null) {
			sql.append(" and erfd.equip_item_id = ? ");
			params.add(filter.getEpuipItemId());
		}
		/** lay hang muc con time bao hanh; cai nay dung import*/
		if (filter.isFlagWarrantyExpriedDate() && filter.getEquipId() != null) {
			//sql.append(" and erfd.warranty_expired_date >= sysdate ");
			sql.append(" and erfd.equip_item_id in ( ");
			sql.append(" SELECT dt.equip_item_id ");
			sql.append(" FROM equip_repair_form_dtl dt ");
			sql.append(" JOIN equip_repair_form f ON f.equip_repair_form_id = dt.equip_repair_form_id ");
			sql.append(" WHERE 1 = 1 ");
			sql.append(" AND f.status = ? ");
			params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
			if (filter.getEquipId() != null) {
				sql.append(" AND f.equip_id = ? ");
				params.add(filter.getEquipId());
			}
			sql.append(" and ( ( dt.warranty_expired_date is not null and dt.warranty_expired_date >= sysdate) ");
			sql.append(" or dt.warranty_expired_date is null ");
			sql.append(" ) ");
			sql.append(" ) ");
		}
		/** lay hang muc sua chua, theo status cua sua chua*/
		if (filter.getStatusRepair() != null) {
			sql.append(" and erf.status = ? ");
			params.add(filter.getStatusRepair().getValue());
		}
		sql.append(" order by erfd.repair_count desc "); // lay lan sua moi nhat
		return repo.getEntityBySQL(EquipRepairFormDtl.class, sql.toString(), params);
	}
	/***
	 * @author vuongmq
	 * @date 20/04/2015
	 * cap nhat them cac cot: 
	 * private String ngayBatDauBaoHanh; //
	 * private String ngayHetHanBaoHanh; //
	 */
	/** dung: exportRepairRecord; printRepair */
	@Override
	public List<EquipmentRepairFormDtlVO> getListEquipRepairDtlVOByEquipRepairId(EquipRepairFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT erfd.EQUIP_ITEM_ID AS id, ");
		sql.append(" (SELECT ei.code ");
		sql.append(" FROM equip_item ei ");
		sql.append(" WHERE ei.equip_item_id = erfd.equip_item_id ");
		sql.append(" ) AS hangMuc, ");
		sql.append(" (SELECT ei.name ");
		sql.append(" FROM equip_item ei ");
		sql.append(" WHERE ei.equip_item_id = erfd.equip_item_id ");
		sql.append(" ) AS tenHangMuc, "); // vuongmq; 28/05/2015; lay them tenHangMuc
		sql.append(" to_char(erfd.WARRANTY_DATE,'dd/mm/yyyy') as ngayBatDauBaoHanh, ");
		sql.append(" erfd.WARRANTY AS baoHanh, ");
		sql.append(" to_char(erfd.WARRANTY_EXPIRED_DATE, 'dd/mm/yyyy') as ngayHetHanBaoHanh, ");
		sql.append(" erfd.MATERIAL_PRICE AS donGiaVatTu, ");
		sql.append(" erfd.WORKER_PRICE AS donGiaNhanCong, ");
		sql.append(" erfd.QUANTITY AS soLuong ");
		sql.append(" FROM equip_repair_form_dtl erfd ");
		sql.append(" WHERE 1 = 1 ");
		if (filter.getEquipRepairId() != null) {
			sql.append(" and erfd.equip_repair_form_id = ? ");
			params.add(filter.getEquipRepairId());
		}
		sql.append(" ORDER BY hangMuc ");
		String[] fieldNames = { "id", // 1
				"hangMuc", // 2
				"tenHangMuc", // 2.0
				"ngayBatDauBaoHanh", // 2.1
				"baoHanh", // 3
				"ngayHetHanBaoHanh", //3.1
				"donGiaVatTu", // 4
				"donGiaNhanCong", // 5
				"soLuong", // 6
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 2.0
				StandardBasicTypes.STRING, // 2.1
				StandardBasicTypes.LONG, // 3
				StandardBasicTypes.STRING, // 3.1
				StandardBasicTypes.BIG_DECIMAL, // 4
				StandardBasicTypes.BIG_DECIMAL, // 5
				StandardBasicTypes.INTEGER, // 6
		};

		return repo.getListByQueryAndScalar(EquipmentRepairFormDtlVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public EquipStockTransFormDtl getEquipStockTransFormDtlById(Long id) throws DataAccessException {
		// TODO Auto-generated method stub
		return repo.getEntityById(EquipStockTransFormDtl.class, id);
	}

	/***
	 * @author vuongmq
	 * @date 25/04/2015
	 * cap nhat  lay kho popup cua Thiet bi, theo phan quyen
	 */
	@Override
	public List<EquipStockVO> getListStockByFilter(EquipStockFilter filter) throws DataAccessException {
		if (filter.getStaffRoot() == null || filter.getShopRoot() == null) {
			throw new IllegalArgumentException("staffRoot not id");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/*sql.append(" with ");
		sql.append(" lstShop as (");
		sql.append("  select distinct shop_id");
		sql.append("        from shop s");
		sql.append("        where s.status = 1 ");
		sql.append("        and exists (select 1 from channel_type where type = 1 and object_type in (14,3) and status = 1 and channel_type_id = s.shop_type_id)");
		if (filter.getShopRoot() != null && filter.getShopRoot() > 0) {
			sql.append("        and s.shop_id in (select s1.shop_id from shop s1 where s1.status != -1 start with s1.shop_id = ?  connect by prior s1.shop_id = s1.parent_shop_id)");
			params.add(filter.getShopRoot());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("        and upper(s.shop_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append("        and upper(s.shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopName().toUpperCase()));
		}
		sql.append(" ),");
		sql.append(" shopView as (");
		sql.append(" select distinct shop_id");
		sql.append("        from shop s");
		sql.append("        where s.status = 1 ");
		sql.append("        and exists (select 1 from channel_type where type = 1 and object_type in (14,3) and status = 1 and channel_type_id = s.shop_type_id)");
		sql.append("        start with shop_id IN( select * from lstShop)");
		sql.append("        connect by prior shop_id=parent_shop_id");
		sql.append(" ),");
		sql.append(" customerView as ( ");
		sql.append("  select s.shop_code as shopCode,s.shop_name as shopName,c.short_code as customerCode,c.customer_name as customerName,c.address as address,(s.shop_code ||c.short_code) as stockCode ");
		sql.append("  from customer c ");
		sql.append("  join shop s on c.shop_id = s.shop_id");
		sql.append("  where 1=1  and c.status = 1");
		sql.append("  and s.shop_id in (select * from shopView )");
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append("        and upper(c.short_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerNameOrAdrress())) {
			sql.append("        and ( upper(c.address) like ? ESCAPE '/' or upper(c.name_text) like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerNameOrAdrress().toUpperCase()));
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getCustomerNameOrAdrress().toUpperCase())));
		}
		sql.append(" )");
		sql.append(" select c.shopCode,c.shopName,c.customerCode,c.customerName,c.address,c.stockCode");
		sql.append(" from customerView c");
		if (StringUtility.isNullOrEmpty(filter.getCustomerCode()) && StringUtility.isNullOrEmpty(filter.getCustomerNameOrAdrress())) {
			sql.append(" union");
			sql.append(" select shop_code as shopCode,shop_name as shopName,null customerCode,null customerName,null address,shop_code as stockCode");
			sql.append(" from shop");
			sql.append(" where shop_id in (select * from shopView)");
		}
		sql.append(" order by stockCode");*/
		
		sql.append(" WITH t_kho_under AS ");
		sql.append(" (SELECT es.equip_stock_id AS id, ");
		sql.append(" es.shop_id as shopId, ");
		sql.append(" (select sh.shop_code from shop sh where sh.status = 1 and sh.shop_id = es.shop_id) as shopCode, ");
		sql.append(" (select sh.shop_name from shop sh where sh.status = 1 and sh.shop_id = es.shop_id) as shopName, ");
		sql.append(" null as customerCode, ");
		sql.append(" null as customerName, ");
		sql.append(" null as address, ");
		sql.append(" es.code as stockCode ");
		sql.append(" FROM equip_role_user eru ");
		sql.append(" JOIN equip_role_detail erd ON eru.equip_role_id = erd.equip_role_id ");
		sql.append(" JOIN equip_stock es ON es.equip_stock_id = erd.equip_stock_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND es.status = 1 ");
		sql.append(" AND eru.status = 1 ");
		sql.append(" AND erd.is_under = 1  "); /** co quan ly don vi cap duoi: 1*/
		sql.append(" AND eru.user_id = ? ");
		params.add(filter.getStaffRoot());
		sql.append(" ), ");
		sql.append(" stock_under as ( ");
		sql.append(" select es.equip_stock_id as id, ");
		sql.append(" es.shop_id as shopId, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName, ");
		sql.append(" null as customerCode, ");
		sql.append(" null as customerName, ");
		sql.append(" null as address, ");
		sql.append(" es.code as stockCode ");
		sql.append(" from equip_stock es ");
		sql.append(" join shop sh on sh.shop_id = es.shop_id ");
		sql.append(" where 1=1 ");
		sql.append(" and es.status = 1 ");
		sql.append(" and sh.status = 1 ");
		sql.append(" and es.shop_id in ( ");
		sql.append(" select sh.shop_id ");
		sql.append(" FROM shop sh ");
		sql.append(" WHERE sh.status = 1 ");
		sql.append(" and sh.shop_id not in (SELECT shopId FROM t_kho_under) ");
		sql.append(" START WITH sh.shop_id IN (SELECT shopId FROM t_kho_under) ");
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sql.append(" ) ");
		sql.append(" union ");
		sql.append(" (select * from t_kho_under) ");
		sql.append(" ), ");
		sql.append(" t_kho_nounder as ");
		sql.append(" (SELECT es.equip_stock_id as id, ");
		sql.append(" es.shop_id as shopId, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName, ");
		sql.append(" null as customerCode, ");
		sql.append(" null as customerName, ");
		sql.append(" null as address, ");
		sql.append(" es.code as stockCode ");
		sql.append(" FROM equip_role_user eru ");
		sql.append(" JOIN equip_role_detail erd ON eru.equip_role_id = erd.equip_role_id ");
		sql.append(" JOIN equip_stock es ON es.equip_stock_id = erd.equip_stock_id ");
		sql.append(" join shop sh on sh.shop_id = es. shop_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND es.status = 1 ");
		sql.append(" AND eru.status = 1 ");
		sql.append(" AND erd.is_under = 0  ");/** khong quan ly don vi cap duoi: 0*/
		sql.append(" AND eru.user_id = ? ");
		params.add(filter.getStaffRoot());
		sql.append(" ), ");
		sql.append(" lstStockShopTmp AS ");
		sql.append(" ( SELECT * FROM stock_under ");
		sql.append(" UNION ");
		sql.append(" SELECT * FROM t_kho_nounder ");
		sql.append(" ), ");
		sql.append(" lstStockShop AS ");
		sql.append(" ( SELECT * FROM lstStockShopTmp t where t.shopId in ( ");
		sql.append(" select sh.shop_id from shop sh ");
		sql.append(" where sh.status = 1 ");
		sql.append(" START WITH sh.shop_id IN (?) ");
		params.add(filter.getShopRoot());
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sql.append("  ) ");
		sql.append(" ), ");
		/** end danh sach kho cua shop; lstStockShop: nay lay trong phan quyen CMS shopRoot*/
		sql.append(" lstshopCUS as ( ");
		sql.append(" select shopId from ( ");
		sql.append(" select shop_id as shopId from shop sh ");
		sql.append(" START WITH sh.shop_id IN (SELECT shopId FROM t_kho_under) ");
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sql.append(" UNION ");
		sql.append(" select shopId from t_kho_nounder ) ");
		sql.append(" where shopId in ( ");
		sql.append(" select sh.shop_id from shop sh ");
		sql.append(" where 1=1 ");
		sql.append(" and sh.status = 1 ");
		sql.append(" START WITH sh.shop_id IN (?) ");
		params.add(filter.getShopRoot());
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id) ");
		sql.append(" ), ");
		sql.append(" t_kho_customer as ");
		sql.append(" ( select c.customer_id as id, ");
		sql.append(" sh.shop_id as shopId, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName, ");
		sql.append(" c.short_code as customerCode, ");
		sql.append(" c.customer_name as customerName, ");
		sql.append(" c.address as address, ");
		sql.append(" sh.shop_code || c.short_code as stockCode ");
		sql.append(" from customer c ");
		sql.append(" join shop sh on sh.shop_id = c.shop_id ");
		sql.append(" where c.status = 1 ");
		sql.append(" and c.shop_id in (select tc.shopId from lstshopCUS tc) ");
		sql.append(" ), ");
		/** end danh sach kho cua customer */
		sql.append(" lstStockShopAndCustomer as ");
		sql.append(" (select id, shopId, shopCode, shopName, customerCode, customerName, address, stockCode from lstStockShop ");
		sql.append(" union ");
		sql.append(" select id, shopId, shopCode, shopName, customerCode, customerName, address, stockCode from t_kho_customer ");
		sql.append(" ) ");
		/** select VO */
		sql.append(" select * from lstStockShopAndCustomer t ");
		sql.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("        and t.shopCode like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			// ham trong DB function: unicode2english(customer_name) -> khong dau lower
			sql.append("        and unicode2english(t.shopName) like ? ESCAPE '/' ");
			//params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopName().toLowerCase()));
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getShopName()).toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append("        and t.customerCode like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerNameOrAdrress())) {
			// ham trong DB function: unicode2english(customer_name) -> khong dau lower
			sql.append("        and ( unicode2english(t.customerName) like ? ESCAPE '/' or unicode2english(t.address) like ? ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getCustomerNameOrAdrress().toLowerCase())));
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getCustomerNameOrAdrress().toLowerCase())));
		}
		sql.append(" order by shopCode");
		String[] fieldNames = { "shopCode",// 0
				"shopName",// 1
				"customerCode",// 2
				"customerName",// 3
				"address",// 4
				"stockCode"// 5
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING // 5
		};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipStockVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipStockVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public String getContractNumberByFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select equip_id as equipId, equip_delivery_record_id as equipDeliveryRecordId, contract_number as contractNumber ");
		sql.append("  from ( select dtl.equip_id , hr.equip_delivery_record_id, hr.contract_number  ");
		sql.append(" , hr.record_status ,row_number() over (partition by dtl.equip_id order by hr.create_date desc) as rn  ");
		sql.append(" from equip_delivery_record hr join equip_delivery_rec_dtl dtl on hr.equip_delivery_record_id = dtl.equip_delivery_record_id ");
		sql.append(" where 1 = 1  ");
		if (filter.getRecordStatus() != null) {
			sql.append("   and hr.record_status = ?  ");
			params.add(filter.getRecordStatus());
		}
		sql.append(" ) dta  ");
		sql.append(" where dta.rn = 1   ");
		if (filter.getEquipId() != null) {
			sql.append(" and dta.equip_id = ? ");
			params.add(filter.getEquipId());
		}
		if (filter.getEqDeliveryRecId() != null) {
			sql.append(" and dta.equip_delivery_record_id = ? ");
			params.add(filter.getEqDeliveryRecId());
		}
		sql.append(" order by dta.equip_delivery_record_id desc  ");
		String[] fieldNames = { "shopCode",// 0
				"equipId",// 1
				"equipDeliveryRecordId",// 2
				"contractNumber" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING };
		List<EquipmentVO> lstData = repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (lstData != null && !lstData.isEmpty() && lstData.get(0) != null && !StringUtility.isNullOrEmpty(lstData.get(0).getContractNumber())) {
			return lstData.get(0).getContractNumber().trim();
		}
		return "";
	}
	
	@Override
	public List<EquipmentVO> searchContractNumberByFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select equip_id as equipId, equip_delivery_record_id as equipDeliveryRecordId, contract_number as contractNumber ");
		sql.append("  from ( select dtl.equip_id , hr.equip_delivery_record_id, hr.contract_number  ");
		sql.append(" , hr.record_status ,row_number() over (partition by dtl.equip_id order by hr.create_date desc) as rn  ");
		sql.append(" from equip_delivery_record hr join equip_delivery_rec_dtl dtl on hr.equip_delivery_record_id = dtl.equip_delivery_record_id ");
		sql.append(" where 1 = 1  ");
		if (filter.getRecordStatus() != null) {
			sql.append("   and hr.record_status = ?  ");
			params.add(filter.getRecordStatus());
		}
		sql.append(" ) dta  ");
		sql.append(" where dta.rn = 1   ");
		if (filter.getEquipId() != null) {
			sql.append(" and dta.equip_id = ? ");
			params.add(filter.getEquipId());
		}
		if (filter.getEqDeliveryRecId() != null) {
			sql.append(" and dta.equip_delivery_record_id = ? ");
			params.add(filter.getEqDeliveryRecId());
		}
		sql.append(" order by dta.equip_delivery_record_id desc  ");
		String[] fieldNames = { "shopCode",// 0
				"equipId",// 1
				"equipDeliveryRecordId",// 2
				"contractNumber" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING };
		
		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");
		
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<EquipmentVO> getListCheckEquipGroupById(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select e.equip_id as id, e.status as status from equipment e");
		sql.append(" where 1=1 and e.status =1 ");
		if (filter.getId() != null && filter.getId() > 0) {
			sql.append("and e.equip_group_id =?");
			params.add(filter.getId());
		}
		final String[] fieldNames = new String[] { "id", "status" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipmentVO> getListCheckEquipGroupProductByIdEquipGroup(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ep.equip_group_product_id as id, ep.status as status from equip_group_product ep");
		sql.append(" where 1=1 and ep.status =1 ");
		if (filter.getId() != null && filter.getId() > 0) {
			sql.append("and ep.equip_group_id =?");
			params.add(filter.getId());
		}
		final String[] fieldNames = new String[] { "id", "status" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/**
	 * Tao moi Quyen nguoi dung
	 * 
	 * @author tamvnm
	 * @since 24/03/2015
	 * @param 
	 * @throws DataAccessException
	 */
	@Override
	public EquipRoleUser createEquipRoleUser(EquipRoleUser roleUser) throws DataAccessException {
		if (roleUser == null) {
			throw new IllegalArgumentException(" roleUser is null ");
		}
		repo.create(roleUser);
		return repo.getEntityById(EquipRoleUser.class, roleUser.getId());
	}

	/**
	 * Lay danh sach staff cho phan quyen - Thiet bi
	 * 
	 * @author tamvnm
	 * @since 24/03/2015
	 * @param 
	 * @throws DataAccessException
	 */
	@Override
	public List<EquipmentStaffVO> getStaffForPermission(EquipStaffFilter filter) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
//		sql.append("WITH rparentstaff ");
//		sql.append("     AS (SELECT psm.staff_id AS staff_id ");
//		sql.append("         FROM   parent_staff_map psm ");
//		sql.append("         WHERE  psm.status = 1 ");
//		sql.append("                AND psm.staff_id NOT IN (SELECT staff_id ");
//		sql.append("                                         FROM   exception_user_access ");
//		sql.append("                                         WHERE  status = 1 ");
//		sql.append("                                                AND ? = parent_staff_id) ");
//		params.add(filter.getStaffId());
//		sql.append("         START WITH psm.parent_staff_id = ? ");
//		params.add(filter.getStaffId());
//		sql.append("         CONNECT BY PRIOR psm.staff_id = psm.parent_staff_id), ");
//		sql.append("     mapcmswithstaff ");
//		sql.append("     AS (SELECT s1.staff_id ");
//		sql.append("         FROM   staff s1 ");
//		sql.append("                left join rparentstaff rs ");
//		sql.append("                       ON rs.staff_id = s1.staff_id ");
//		sql.append("         WHERE  1 = 1 ");
//		sql.append("                AND shop_id IN (SELECT shop_id ");
//		sql.append("                                FROM   shop ");
//		sql.append("                                WHERE  status = 1 ");
//		sql.append("                                START WITH shop_id = ? ");
//		params.add(filter.getShopId());
//		sql.append("                                CONNECT BY PRIOR shop_id = parent_shop_id) ");
//		sql.append("                AND s1.staff_id NOT IN (SELECT staff_id ");
//		sql.append("                                        FROM   exception_user_access ");
//		sql.append("                                        WHERE  status = 1 ");
//		sql.append("                                               AND ? = parent_staff_id) ");
//		params.add(filter.getStaffId());
//		sql.append("                AND ( ? = 0 ");
//		params.add(filter.getFlagCMS());
//		sql.append("                       OR ( rs.staff_id IS NOT NULL ) )) ");
//		sql.append(" select distinct st.staff_code as staffCode, ");
//		sql.append(" er.code as roleCode, ");
//		sql.append(" eru.status as status ");
//		sql.append(" from staff st ");
//		sql.append(" JOIN mapcmswithstaff ms on st.staff_id = ms.staff_id ");
//		sql.append(" left join equip_role_user eru on st.staff_id = eru.user_id ");
//		sql.append(" left join equip_role er on er.equip_role_id = eru.equip_role_id ");
//		sql.append(" where 1 = 1 ");
		
		/*@modified: tamvnm
		 *@discrpition: Giap phap thay doi: Khong lay theo phan quyen
		 *@since: 02/04/2015
		 */
		sql.append("SELECT DISTINCT s.staff_id   AS id, ");
		sql.append("                s.staff_code AS staffCode, ");
		sql.append("                s.staff_name AS staffName, ");
		sql.append("                sh.shop_code AS shopCode, ");
		sql.append("                sh.shop_name AS shopName, ");
		sql.append("                s.status     AS status ");
		sql.append("FROM   staff s ");
		sql.append("       JOIN shop sh ");
		sql.append("         ON sh.shop_id = s.shop_id ");
		if(filter.getLstShopId() != null && !filter.getLstShopId().isEmpty()) {
			String paramsListShopId = StringUtility.getParamsIdFixBugTooLongParams(filter.getLstShopId(), "sh.shop_id");
			sql.append(paramsListShopId);
		}
		
		
		if (StringUtility.isNullOrEmpty(filter.getStrListUserId()) && filter.getShopId() != null && filter.getRoleId() != null && filter.getUserId() != null) {
			sql.append(" JOIN table (F_GET_LIST_CHILD_STAFF_INHERIT(?, sysdate, ?, ?, ?)) on s.staff_id = number_key_id and s.shop_id = number_shop_id ");
			params.add(filter.getUserId());
			params.add(filter.getRoleId());
			params.add(filter.getShopId());
			params.add(StaffSpecificType.STAFF.getValue());
		}
		
		
		sql.append("WHERE  ");
		if (filter.getPersonalStatus() != null) {
			if (filter.getPersonalStatus() != -1) {
				sql.append("          s.status = ? ");
				params.add(filter.getPersonalStatus());
			} else {
				// lay tat ca khong lay delete
				sql.append("         s.status <> -1 ");
			}
		} else {
			sql.append("      		  	 s.status = 1 ");
		}
//		ql.append(" and sh.shop_type_id not in ( select Channel_type_id from channel_type where 1=1 and type = 1 and object_type in (4,5,6,7)) ");
		
		if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "s.staff_id");
			sql.append(paramsFix);
		}
		
		if(!StringUtility.isNullOrEmpty(filter.getPersonalCode())) {
			sql.append("        and  upper(s.staff_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getPersonalCode().toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getPersonalName())) {
			sql.append("        and  upper(s.name_text) like ? ESCAPE '/' ");
			params.add(Unicode2English.codau2khongdau((StringUtility.toOracleSearchLikeSuffix(filter.getPersonalName().toUpperCase()))));
		}
		
		sql.append("ORDER  BY s.staff_code");

		final String[] fieldNames = new String[] {
				"id", 
				"staffCode",
				"staffName",
				"shopCode",
				"shopName",
				"status"
		};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER
				
		};
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipmentStaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(EquipmentStaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	/**
	 * Lay danh sach staff cho phan quyen - Thiet bi
	 * Note: lay danh sach staff giong nhu getStaffForPermission o tren; cong them roleCode quyen vao cho nhan vien nua
	 * @author vuongmq
	 * @date 26/03/2015
	 * @param 
	 * @throws DataAccessException
	 */
	@Override
	public List<EquipmentStaffVO> getStaffForPermissionExportExcel(EquipStaffFilter filter) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		/*sql.append("WITH rparentstaff ");
		sql.append("     AS (SELECT psm.staff_id AS staff_id ");
		sql.append("         FROM   parent_staff_map psm ");
		sql.append("         WHERE  psm.status = 1 ");
		sql.append("                AND psm.staff_id NOT IN (SELECT staff_id ");
		sql.append("                                         FROM   exception_user_access ");
		sql.append("                                         WHERE  status = 1 ");
		sql.append("                                                AND ? = parent_staff_id) ");
		params.add(filter.getStaffId());
		sql.append("         START WITH psm.parent_staff_id = ? ");
		params.add(filter.getStaffId());
		sql.append("         CONNECT BY PRIOR psm.staff_id = psm.parent_staff_id), ");
		sql.append("     mapcmswithstaff ");
		sql.append("     AS (SELECT s1.staff_id ");
		sql.append("         FROM   staff s1 ");
		sql.append("                left join rparentstaff rs ");
		sql.append("                       ON rs.staff_id = s1.staff_id ");
		sql.append("         WHERE  1 = 1 ");
		sql.append("                AND shop_id IN (SELECT shop_id ");
		sql.append("                                FROM   shop ");
		sql.append("                                WHERE  status = 1 ");
		sql.append("                                START WITH shop_id = ? ");
		params.add(filter.getShopId());
		sql.append("                                CONNECT BY PRIOR shop_id = parent_shop_id) ");
		sql.append("                AND s1.staff_id NOT IN (SELECT staff_id ");
		sql.append("                                        FROM   exception_user_access ");
		sql.append("                                        WHERE  status = 1 ");
		sql.append("                                               AND ? = parent_staff_id) ");
		params.add(filter.getStaffId());
		sql.append("                AND ( ? = 0 ");
		params.add(filter.getFlagCMS());
		sql.append("                       OR ( rs.staff_id IS NOT NULL ) )) ");
		sql.append(" select distinct st.staff_code as staffCode, ");
		sql.append(" er.code as roleCode, ");
		sql.append(" eru.status as status ");
		sql.append(" from staff st ");
		sql.append(" JOIN mapcmswithstaff ms on st.staff_id = ms.staff_id ");
		sql.append(" left join equip_role_user eru on st.staff_id = eru.user_id ");
		sql.append(" left join equip_role er on er.equip_role_id = eru.equip_role_id ");
		sql.append(" where 1 = 1 ");*/
		sql.append(" select distinct st.staff_code as staffCode, ");
		sql.append(" er.code as roleCode, ");
		sql.append(" eru.status as status ");
		sql.append(" from staff st ");
		sql.append(" left join equip_role_user eru on st.staff_id = eru.user_id ");
		sql.append(" left join equip_role er on er.equip_role_id = eru.equip_role_id ");
		if (StringUtility.isNullOrEmpty(filter.getStrListUserId()) && filter.getShopId() != null && filter.getRoleId() != null && filter.getUserId() != null) {
			sql.append(" JOIN table (F_GET_LIST_CHILD_STAFF_INHERIT(?, sysdate, ?, ?, ?)) on st.staff_id = number_key_id and st.shop_id = number_shop_id ");
			params.add(filter.getUserId());
			params.add(filter.getRoleId());
			params.add(filter.getShopId());
			params.add(StaffSpecificType.STAFF.getValue());
		}
		
		
		sql.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "st.staff_id");
			sql.append(paramsFix);
		}
		if(!StringUtility.isNullOrEmpty(filter.getPersonalCode())) {
			sql.append("        and  upper(st.staff_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getPersonalCode().toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getPersonalName())) {
			sql.append("        and  upper(st.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getPersonalName())).toUpperCase());
		}
		if(filter.getPersonalStatus() != null) {
			if(filter.getPersonalStatus() != -1) {
				sql.append("        and  st.status = ? ");
				params.add(filter.getPersonalStatus());
			} else {
				//lay tat ca khong lay delete
				sql.append("        and  st.status <> -1 ");
			}
		}
		sql.append(" order by staffCode, roleCode ");

		final String[] fieldNames = new String[] {
				"staffCode",
				"roleCode",
				"status"
		};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER
				
		};
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipmentStaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(EquipmentStaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	/**
	 * Lay danh sach quyen - Gan Quyen Cho Nguoi Dung
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param 
	 * @throws BusinessException
	 */
	@Override
	public List<EquipmentPermissionVO> getPermissionByFilter(EquipPermissionFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		//sql.append("SELECT er.* ");
		sql.append("SELECT er.EQUIP_ROLE_ID as id, ");
		sql.append(" 		er.CODE as roleCode, ");
		sql.append(" 		er.NAME as roleName, ");
		sql.append(" 		er.DESCRIPTION as discription, ");
		sql.append(" 		ers.status as status, ");
		sql.append(" 		ers.EQUIP_ROLE_USER_ID as equipRoleUserId ");
		sql.append("FROM   equip_role er ");
		sql.append("       JOIN (SELECT * ");
		sql.append("             FROM   equip_role_user ");
		sql.append("             WHERE  user_id = ? ");
		params.add(filter.getStaffId());
		
		if(filter.getPermissionStatus() != null){
			if(filter.getPermissionStatus() != -1) {
				sql.append("            AND status = ? ");
				params.add(filter.getPermissionStatus());
			}
		}
		sql.append("             ) ers ");
		sql.append("         ON er.equip_role_id = ers.equip_role_id ");
		
		if(!StringUtility.isNullOrEmpty(filter.getPermissionCode())) {
			sql.append("            AND er.code LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getPermissionCode().toUpperCase()));
		}
		
		if(!StringUtility.isNullOrEmpty(filter.getPermissionName())) {
			sql.append("            AND unicode2english(er.name) LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getPermissionName()).trim().toLowerCase()));
		}
		
		sql.append("         order by er.code ");
		final String[] fieldNames = new String[] {
				"id", 
				"roleCode",
				"roleName",
				"discription",
				"status",
				"equipRoleUserId"
		};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG
				
		};
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipmentPermissionVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(EquipmentPermissionVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	@Override
	public List<EquipItemConfig> getListCheckExistCrossing(EquipItemConfigFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_item_config ");
		sql.append(" where 1 = 1  ");
		if(filter.getFromCapacity() != null){
			sql.append(" and ( ");
			sql.append(" ((to_capacity >= ? or to_capacity is null) ");
			params.add(filter.getFromCapacity());
			if(filter.getToCapacity()!= null){
				sql.append(" and (from_capacity <= ?) ");
				params.add(filter.getToCapacity());
			}
			sql.append(" ) ");
			
			sql.append(" or (to_capacity is null ");
			if(filter.getToCapacity() != null){
				sql.append(" and from_capacity <= ? ");
				params.add(filter.getToCapacity());
			}
			sql.append(" ) ");
			sql.append(")");
		}
		return repo.getListBySQL(EquipItemConfig.class, sql.toString(), params);
	}
	@Override
	public EquipItemConfig getEquipItemConfigByFilter(EquipItemConfigFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_item_config ");
		sql.append(" where 1 = 1  and status = 1 ");
		if(filter.getFromCapacity() != null){
			sql.append(" and from_capacity = ? ");
			params.add(filter.getFromCapacity());
		}else{
			sql.append(" and from_capacity is null ");
		}
		if(filter.getToCapacity() != null){
			sql.append(" and to_capacity = ? ");
			params.add(filter.getToCapacity());
		}else{
			sql.append(" and to_capacity is null ");
		}
		return repo.getEntityBySQL(EquipItemConfig.class, sql.toString(), params);
	}
	@Override
	public List<EquipItemConfigDtl> getListEquipItemConfigDtlByFilter(EquipItemConfigDtlFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select eicd.* ");
		sql.append(" from equip_item_config_dtl eicd ");
		if(filter.getEquipItemCode()!= null){
			sql.append(" join equip_item ei on ei.equip_item_id = eicd.equip_item_id and ei.code = ? ");
			params.add(filter.getEquipItemCode().toUpperCase());
		}
		sql.append(" where 1 = 1  ");
		if(filter.getEquipItemConfigId()!= null){
			sql.append(" and eicd.equip_item_config_id = ? ");
			params.add(filter.getEquipItemConfigId());
		}
		sql.append(" order by eicd.equip_item_id ");
		return repo.getListBySQL(EquipItemConfigDtl.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipItemConfigVO> getListEquipItemConfigByFilter(EquipItemConfigFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select eic.equip_item_config_id id ");
		sql.append(" ,eic.code  equipItemConfigCode ");
		sql.append(" ,(CASE ");
		sql.append(" WHEN eic.from_capacity IS NULL   AND eic.to_capacity IS NULL  THEN ''  ");
		sql.append(" WHEN eic.from_capacity IS NULL   THEN '=< '||TO_CHAR(eic.to_capacity) ");
		sql.append(" WHEN eic.to_capacity IS NULL    THEN '>= ' ||TO_CHAR(eic.from_capacity) ");
		sql.append(" WHEN eic.to_capacity <> eic.from_capacity   THEN TO_CHAR(eic.from_capacity)|| ' - '|| TO_CHAR(eic.to_capacity) ");
		sql.append(" ELSE TO_CHAR(eic.from_capacity) ");
		sql.append(" END)    AS capacity ");
		sql.append(" ,eic.from_capacity fromCapacity ");
		sql.append(" ,eic.to_capacity toCapacity ");
		sql.append(" ,eic.status status ");
		sql.append(" from EQUIP_ITEM_CONFIG eic ");
		sql.append(" where 1 = 1 ");
		if (filter.getStatus() != null) {
			sql.append(" and eic.status =? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and eic.status <> - 1 ");
		}
		if (filter.getToCapacity() != null && filter.getFromCapacity() != null) {
			sql.append(" and ( ");
			sql.append(" (eic.from_capacity is null and eic.to_capacity is not null and eic.to_capacity >= ?) ");
			params.add(filter.getFromCapacity());
			sql.append(" or (eic.from_capacity is not null and eic.to_capacity is null and eic.from_capacity < ?) ");
			params.add(filter.getToCapacity() + 1);
			sql.append(" or (eic.from_capacity < ? and eic.to_capacity >= ?)  ");
			params.add(filter.getFromCapacity() + 1);
			params.add(filter.getFromCapacity());
			sql.append(" or (eic.from_capacity >= ? and eic.from_capacity < ?)  ");
			params.add(filter.getFromCapacity());
			params.add(filter.getToCapacity() + 1);
			sql.append(" or (eic.to_capacity >= ? and eic.to_capacity < ?)  ");
			params.add(filter.getFromCapacity());
			params.add(filter.getToCapacity() + 1);
			sql.append(" ) ");			
		} else if (filter.getToCapacity() != null) {
			// Truong hop From is null, To not null
			sql.append(" and ( ");
			sql.append(" eic.from_capacity is null ");
			sql.append(" or (eic.from_capacity < ?) ");
			params.add(filter.getToCapacity() + 1);
			sql.append(" ) ");
		} else if (filter.getFromCapacity() != null) {
			// Truong hop From not null, To is null
			sql.append(" and ( ");
			sql.append(" eic.to_capacity is null or (eic.to_capacity >= ?) ");
			params.add(filter.getFromCapacity());
			sql.append(" ) ");
		}
		sql.append(" order by eic.code");
		final String[] fieldNames = new String[] { "id" //1
				, "equipItemConfigCode" //2
				, "capacity" //3
				, "fromCapacity" //4
				, "toCapacity" //5
				, "status" //6
		};

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //2 
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.INTEGER, //4
				StandardBasicTypes.INTEGER, //5
				StandardBasicTypes.INTEGER //6
		};
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql.toString());
		countSql.append(")");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipItemConfigVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipItemConfigVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public List<EquipItemConfigVO> exportListEquipItemConfigByFilter(EquipItemConfigFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("   WITH dta AS ");
		sql.append("     (SELECT rc.equip_item_config_id, ");
		sql.append("      rc.code AS eqItemConfig, ");
		sql.append("       rc.from_capacity, ");
		sql.append("       rc.to_capacity, ");
		sql.append("       (case when rcd.equip_item_config_id is not null then (select code from equip_item where equip_item_id = rcd.equip_item_id) else '' end) as code, ");
		sql.append("      (case when rcd.equip_item_config_id is not null then to_char((select name from equip_item where equip_item_id = rcd.equip_item_id)) else '' end) as name, ");
		sql.append("  (case when rc.status = 1 then 'Hoạt động' when rc.status = 0 then 'Tạm ngưng' when rc.status = -1 then 'Đã xóa' else '' end) as eqItemConfStatusStr, ");
		sql.append("      rcd.from_material_price, ");
		sql.append("      rcd.to_material_price, ");
		sql.append("      rcd.from_worker_price, ");
		sql.append("       rcd.to_worker_price ");
		sql.append("     FROM equip_item_config rc ");
		sql.append("     LEFT JOIN equip_item_config_dtl rcd ON rc.equip_item_config_id = rcd.equip_item_config_id ");
		sql.append("   where 1 = 1 ");
		if (filter.getStatus() != null) {
			sql.append(" and rc.status =? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and rc.status != - 1 ");
		}
		if (filter.getToCapacity() != null && filter.getFromCapacity() != null) {
			sql.append(" and ( ");
			sql.append(" (rc.from_capacity is null and rc.to_capacity is not null and rc.to_capacity >= ?) ");
			params.add(filter.getFromCapacity());
			sql.append(" or (rc.from_capacity is not null and rc.to_capacity is null and rc.from_capacity < ?) ");
			params.add(filter.getToCapacity() + 1);
			sql.append(" or (rc.from_capacity < ? and rc.to_capacity >= ?)  ");
			params.add(filter.getFromCapacity() + 1);
			params.add(filter.getFromCapacity());
			sql.append(" or (rc.from_capacity >= ? and rc.from_capacity < ?)  ");
			params.add(filter.getFromCapacity());
			params.add(filter.getToCapacity() + 1);
			sql.append(" or (rc.to_capacity >= ? and rc.to_capacity < ?)  ");
			params.add(filter.getFromCapacity());
			params.add(filter.getToCapacity() + 1);
			sql.append(" ) ");			
		} else if (filter.getToCapacity() != null) {
			// Truong hop From is null, To not null
			sql.append(" and ( ");
			sql.append(" rc.from_capacity is null ");
			sql.append(" or (rc.from_capacity < ?) ");
			params.add(filter.getToCapacity() + 1);
			sql.append(" ) ");
		} else if (filter.getFromCapacity() != null) {
			// Truong hop From not null, To is null
			sql.append(" and ( ");
			sql.append(" rc.to_capacity is null or (rc.to_capacity >= ?) ");
			params.add(filter.getFromCapacity());
			sql.append(" ) ");
		}
		sql.append("   order by eqItemConfig, code ) ");
		
		sql.append("  select equipItemConfigId");
		sql.append("  ,(case when rn = 1 then eqItemConfig ELSE '' END) equipItemConfigCode ");
		sql.append("  ,(case when rn = 1 then eqItemConfStatusStr ELSE '' END) eqItemConfStatusStr ");
		sql.append("  ,(case when rn = 1 then fromCapacity else null end) fromCapacity ");
		sql.append("  ,(case when rn = 1 then toCapacity else null end) toCapacity ");
		sql.append("  , equipItemCode, equipItemName, fromMaterialPrice, toMaterialPrice, fromWorkerPrice, toWorkerPrice ");
		sql.append("  from (select equip_item_config_id as equipItemConfigId, eqItemConfig, eqItemConfStatusStr, from_capacity as fromCapacity, to_capacity as toCapacity, code as equipItemCode, name as equipItemName, "); 
		sql.append("  from_material_price as fromMaterialPrice, to_material_price as toMaterialPrice, from_worker_price as fromWorkerPrice, ");
		sql.append("  to_worker_price as toWorkerPrice, row_number() over (partition by equip_item_config_id order by eqItemConfig, code) rn ");
		sql.append("  from dta)");

		final String[] fieldNames = new String[] { 
				"equipItemConfigId",//1
				"equipItemConfigCode",//2
				"eqItemConfStatusStr",//3
				"fromCapacity",//4
				"toCapacity",//5
				"equipItemCode",//6
				"equipItemName",//7
				"fromMaterialPrice",//8
				"toMaterialPrice",//9
				"fromWorkerPrice",//10
				"toWorkerPrice"//11
		};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.INTEGER, //4 
				StandardBasicTypes.INTEGER, //5
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.STRING, //7
				StandardBasicTypes.BIG_DECIMAL, //8
				StandardBasicTypes.BIG_DECIMAL, //9
				StandardBasicTypes.BIG_DECIMAL, //10
				StandardBasicTypes.BIG_DECIMAL //11
		};
		return repo.getListByQueryAndScalar(EquipItemConfigVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<EquipItemConfigDtlVO> getListEquipItemConfigDetailByFilter(EquipItemConfigDtlFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select eic.equip_item_config_id id ");
		sql.append(" ,eic.code  equipItemConfigCode ");
		sql.append(" ,ei.code equipItemCode ");
		sql.append(" ,ei.name equipItemName ");
		sql.append(" ,eicd.FROM_MATERIAL_PRICE fromMaterialPrice ");
		sql.append(" ,eicd.TO_MATERIAL_PRICE toMaterialPrice ");
		sql.append(" ,eicd.FROM_WORKER_PRICE fromWorkerPrice ");
		sql.append(" ,eicd.TO_WORKER_PRICE toWorkerPrice ");
		sql.append(" from EQUIP_ITEM_CONFIG eic ");
		sql.append(" join EQUIP_ITEM ei on 1 = 1 and ei.status = 1 ");
		sql.append(" left join EQUIP_ITEM_CONFIG_dtl eicd on eic.EQUIP_ITEM_CONFIG_id = eicd.EQUIP_ITEM_CONFIG_id ");
		sql.append(" and ei.EQUIP_ITEM_id = eicd.EQUIP_ITEM_id and eicd.status = 1 ");
		sql.append(" where 1=1 ");
		
		sql.append(" AND eic.status = 1 ");
		
		if(filter.getEquipItemConfigId()!= null){
			sql.append(" and eic.equip_item_config_id =? ");
			params.add(filter.getEquipItemConfigId());
		}
		sql.append(" order by eic.code,ei.code");
		final String[] fieldNames = new String[] {
				"id"  //1
				,"equipItemConfigCode" //2
				,"equipItemCode" //3
				,"equipItemName" //4
				,"fromMaterialPrice" //5
				,"toMaterialPrice" //6
				,"fromWorkerPrice" //7
				,"toWorkerPrice" //8
		};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,  //1
				StandardBasicTypes.STRING, //2 
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.BIG_DECIMAL, //5
				StandardBasicTypes.BIG_DECIMAL, //6
				StandardBasicTypes.BIG_DECIMAL, //7
				StandardBasicTypes.BIG_DECIMAL, //8
//				StandardBasicTypes.STRING, //9
//				StandardBasicTypes.BIG_DECIMAL, //10
//				StandardBasicTypes.BIG_DECIMAL, //11
//				StandardBasicTypes.INTEGER //12
		};
		/*StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql.toString());
		countSql.append(")");*/
		return repo.getListByQueryAndScalar(EquipItemConfigDtlVO.class, fieldNames, fieldTypes, sql.toString(), params);
		/*if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipItemConfigDtlVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipItemConfigDtlVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}*/
	}
	
	@Override
	public List<EquipItemConfigDtlVO> getEquipItemInDetailByEquipItemConfig(Long equipItemConfigId) throws DataAccessException {
		if (equipItemConfigId == null) {
			throw new IllegalArgumentException("equipItemConfigId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with dta as ( ");
		sql.append(" 		   select dt.equip_item_id, dt.equip_item_config_dtl_id, dt.from_material_price, dt.to_material_price, dt.from_worker_price, dt.to_worker_price ");
		sql.append(" 		  from equip_item_config rs join equip_item_config_dtl dt on rs.equip_item_config_id = dt.equip_item_config_id ");
		sql.append(" 		  where rs.equip_item_config_id = ? ");
		params.add(equipItemConfigId);
		sql.append(" 		) ");
		sql.append(" 		select ei.equip_item_id as equipItemId ");
		sql.append(" 	,(case  when ei.equip_item_id in (select equip_item_id from dta) then (select max(equip_item_config_dtl_id) from dta where equip_item_id = ei.equip_item_id) ");
		sql.append(" 	 else null end) as id ");
		sql.append(" 		,ei.code as equipItemCode ");
		sql.append(" 		,ei.name as equipItemName ");
		sql.append(" 		,(case  ");
		sql.append(" 		when ei.equip_item_id in (select equip_item_id from dta) then (select max(from_material_price) from dta where equip_item_id = ei.equip_item_id) ");
		sql.append(" 		else null ");
		sql.append(" 		end) as fromMaterialPrice ");
		sql.append(" 		,(case  ");
		sql.append(" 		when ei.equip_item_id in (select equip_item_id from dta) then (select max(to_material_price) from dta where equip_item_id = ei.equip_item_id) ");
		sql.append(" 		else null ");
		sql.append(" 		end) as toMaterialPrice ");
		sql.append(" 		,(case  ");
		sql.append(" 		when ei.equip_item_id in (select equip_item_id from dta) then (select max(from_worker_price) from dta where equip_item_id = ei.equip_item_id) ");
		sql.append(" 		else null ");
		sql.append(" 		end) as fromWorkerPrice ");
		sql.append(" 		,(case  ");
		sql.append(" 		when ei.equip_item_id in (select equip_item_id from dta) then (select max(to_worker_price) from dta where equip_item_id = ei.equip_item_id) ");
		sql.append(" 		else null ");
		sql.append(" 		end) as toWorkerPrice ");
		sql.append(" 		from equip_item ei ");
		sql.append(" 		where status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   order by equipItemCode ");
		
		final String[] fieldNames = new String[] {
				"equipItemId"
				,"id"  //1
				,"equipItemCode" //3
				,"equipItemName" //4
				,"fromMaterialPrice" //5
				,"toMaterialPrice" //6
				,"fromWorkerPrice" //7
				,"toWorkerPrice" //8
		};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,  //0
				StandardBasicTypes.LONG,  //1
				StandardBasicTypes.STRING, //2 
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.BIG_DECIMAL, //5
				StandardBasicTypes.BIG_DECIMAL, //6
				StandardBasicTypes.BIG_DECIMAL, //7
				StandardBasicTypes.BIG_DECIMAL, //8
		};
		return repo.getListByQueryAndScalar(EquipItemConfigDtlVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public String getCodeEquipItemConfig() throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select max(code) as maxCode from equip_item_config");
		String code = (String) repo.getObjectByQuery(sql.toString(), params);
		if (code == null) {
			return "DMHM0001";
		}
		code = code.substring(code.length() - 4);
		code = "0000" + (Long.parseLong(code.toString()) + 1);
		code = code.substring(code.length() - 4);
		code = "DMHM" + code;
		return code;
	}
	
	/**
	 * Lay danh sach quyen - popup search
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param 
	 * @throws BusinessException
	 */
	@Override
	public List<EquipRole> searchListEquipRoleByFilter(EquipRoleFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("SELECT * ");
		sql.append("FROM   equip_role eqr ");
		sql.append("WHERE  eqr.equip_role_id NOT IN (SELECT er.equip_role_id ");
		sql.append("                                 FROM   equip_role er ");
		sql.append("                                        JOIN (SELECT * ");
		sql.append("                                              FROM   equip_role_user ");
		sql.append("                                              WHERE  user_id = ?) ers ");
		params.add(filter.getStaffId());
		sql.append("                                          ON er.equip_role_id = ");
		sql.append("                                             ers.equip_role_id) ");
		sql.append("       AND eqr.status = ? ");
		params.add(filter.getStatusRole());
		
		if(!StringUtility.isNullOrEmpty(filter.getRoleCode())) {
			sql.append("            AND Upper(eqr.code) LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getRoleCode().toUpperCase()));
		}
		
		if(!StringUtility.isNullOrEmpty(filter.getRoleName())) {
			sql.append("            AND Upper(eqr.name) LIKE ? ESCAPE '/' ");
			params.add(Unicode2English.codau2khongdau(StringUtility.toOracleSearchLikeSuffix(filter.getRoleName().toUpperCase())));
		}
		
		sql.append("         order by eqr.code ");
		
		if(filter.getkPaging() != null){
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(*) as count  from ( ");
			countSql.append(sql);
			countSql.append(" ) ");
			return repo.getListBySQLPaginated(EquipRole.class, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListBySQL(EquipRole.class, sql.toString(), params);
		}
	}

	/**
	 * Lay Entities EquipRoleUser ung voi filter
	 * @author vuongmq
	 * @since 27/03/2015
	 */
	@Override
	public EquipRoleUser getEquipEquipRoleUserByFilter(EquipPermissionFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_role_user eru ");
		sql.append(" where 1 =1 ");
		if(filter.getStaffId() != null){
			sql.append(" and eru.user_id = ? ");
			params.add(filter.getStaffId());
		}
		if(filter.getRoleId() != null){
			sql.append(" and eru.equip_role_id = ? ");
			params.add(filter.getRoleId());
		}
		if(filter.getStatus() != null){
			sql.append(" and eru.status = ? ");
			params.add(filter.getStatus().getValue());			
		} else {
			sql.append(" and eru.status <> -1 ");
		}
		sql.append(" order by eru.equip_role_user_id ");
		return repo.getEntityBySQL(EquipRoleUser.class, sql.toString(), params);
	}
	
	/**
	 * Lay EquipRole
	 * 
	 * @author tamvnm
	 * @since 25/03/2015
	 * @param  id
	 * @throws BusinessException
	 */
	@Override
	public EquipRole getEquipRoleById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipRole.class, id);
	}
	
	@Override
	public EquipStock getEquipStockById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipStock.class, id);
	}
	
	@Override
	public Boolean checkExistStockTranFormDtlOnGoing(Long stockTranFormId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from equip_stock_trans_form_dtl where equip_stock_trans_form_id = ? and perform_status = ?");
		params.add(stockTranFormId);
		params.add(PerFormStatus.ONGOING.getValue());
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	
	@Override
	public Boolean checkExistStockTranFormDtlByEquip(Long stockTranFormId, Long equipId) throws DataAccessException {
		if (stockTranFormId == null) {
			throw new IllegalArgumentException("stockTranFormId is null");
		}
		if (equipId == null) {
			throw new IllegalArgumentException("equipId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from equip_stock_trans_form_dtl where equip_stock_trans_form_id = ? and equip_id = ?");
		params.add(stockTranFormId);
		params.add(equipId);
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	
	@Override
	public List<EquipStockTransFormDtl> getListEquipStockTransFormDtlOnGoing(Long stockTranFormId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * ");
		sql.append("FROM   equip_stock_trans_form_dtl ");
		sql.append("where equip_stock_trans_form_id = ? and perform_status = ?");
		params.add(stockTranFormId);
		params.add(PerFormStatus.ONGOING.getValue());
		return repo.getListBySQL(EquipStockTransFormDtl.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipStockTransFormDtl> getListEquipStockTransFormDtlById(Long stockTranFormId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * ");
		sql.append("FROM   equip_stock_trans_form_dtl ");
		sql.append("where equip_stock_trans_form_id = ? ");
		params.add(stockTranFormId);
		return repo.getListBySQL(EquipStockTransFormDtl.class, sql.toString(), params);
	}

	@Override
	public List<EquipLendDetail> getListEquipmentLendDetailByLendId(
			Long equipLendId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM   equip_lend_detail eld WHERE  status = 1        AND equip_lend_id = ?");
		params.add(equipLendId);
		return repo.getListBySQL(EquipLendDetail.class, sql.toString(), params);
	}

	@Override
	public List<EquipDeliveryRecord> getListDeliveryRecordByFilter(
			EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_delivery_record edr where edr.contract_number = ? ");
		params.add(filter.getNumberContract());
		return repo.getListBySQL(EquipDeliveryRecord.class, sql.toString(), params);
	}

	@Override
	public List<EquipmentRecordDeliveryVO> getListDeliveryRecordForExport(EquipmentFilter<EquipmentRecordDeliveryVO> equipmentFilter) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT  distinct edr.code                                    AS recordCode, ");
		sql.append("       edr.contract_number                         AS numberContract, ");
		sql.append("       (SELECT s.shop_code ");
		sql.append("        FROM   shop s ");
		sql.append("        WHERE  s.shop_id = edr.to_object_id)       AS shopCode, ");
		sql.append("        sf.staff_code   AS staffCode,  ");
		sql.append("       edr.contract_create_date                    AS contractCreateDate, ");
		sql.append("       e.code                                      AS equipmentCode, ");
		sql.append("       e.serial                                    AS seriNumber, ");
		sql.append("       edrd.content                                AS content, ");
		sql.append("       edrd.depreciation                           AS depreciation, ");
		sql.append("       (SELECT c.short_code ");
		sql.append("        FROM   customer c ");
		sql.append("        WHERE  c.customer_id = edr.customer_id)    AS customerCode, ");
		sql.append("       edr.to_idno                                 AS idNO, ");
		sql.append("       edr.to_idno_place                           AS idNOPlace, ");
		sql.append("       edr.to_idno_date                            AS idNODate, ");
		sql.append("       edr.to_business_license                     AS toBusinessLicense, ");
		sql.append("       edr.to_business_place                       AS toBusinessPlace, ");
		sql.append("       edr.to_business_date                        AS toBusinessDate, ");
		sql.append("       edr.to_object_addres                        AS toObjectAddress, ");
		sql.append("       edr.ADDRESS		                           AS soNha, ");
		sql.append("       edr.STREET                                  AS duong, ");
		sql.append("       edr.WARD_NAME                               AS phuong, ");
		sql.append("       edr.DISTRICT_NAME                           AS quan, ");
		sql.append("       edr.PROVINCE_NAME                           AS tp, ");
		sql.append("       edr.to_representative                       AS toRepresentative, ");
		sql.append("       edr.to_position                             AS toPosition, ");
		sql.append("       edr.freezer                                 AS freezer, ");
		sql.append("       edr.refrigerator                            AS refrigerator, ");
		sql.append("       edr.note, ");
		sql.append("       eg.NAME                                     AS equipGroup, ");
		sql.append("       eg.code                                     AS equipGroupCode, ");
		sql.append("       ep.NAME                                     AS equipProvider, ");
		sql.append("       e.price                                     AS price, ");
		sql.append("       (SELECT Ap.value ");
		sql.append("        FROM   ap_param ap ");
		sql.append("        WHERE  Ap.ap_param_code = E.health_status) AS health, ");
		sql.append("       e.manufacturing_year                        AS manufacturingYear, ");
		sql.append("       edr.create_form_date                         AS createFormDate, ");
		sql.append("       edr.TO_PERMANENT_ADDRESS                         AS toPermanentAddress ");
		sql.append("FROM   equip_delivery_record edr ");
		sql.append("       left JOIN equip_delivery_rec_dtl edrd ");
		sql.append("         ON edr.equip_delivery_record_id = edrd.equip_delivery_record_id ");
		sql.append("       JOIN staff sf on sf.staff_id = edr.staff_id and sf.status = 1 ");
		sql.append("       JOIN customer cus ON cus.customer_id = edr.customer_id ");
		sql.append("       JOIN equipment e ON e.equip_id = edrd.equip_id ");
		sql.append("       LEFT JOIN equip_lend el on el.equip_lend_id = edr.equip_lend_id ");
		sql.append("       join (SELECT shop_id, shop_code, shop_name ");
		sql.append("             FROM   shop ");
		sql.append("             WHERE  status = 1 ");
		sql.append("             START WITH shop_code = ? ");
		params.add(equipmentFilter.getCurShopCode());
		sql.append("             CONNECT BY PRIOR shop_id = parent_shop_id ");
		sql.append("                             ) lstShop ");
		sql.append("         ON lstShop.shop_id = cus.shop_id ");
		sql.append("       LEFT JOIN equip_group eg ");
		sql.append("         ON Eg.equip_group_id = E.equip_group_id ");
		sql.append("            AND Eg.status = 1 ");
		sql.append("       LEFT JOIN equip_provider ep ");
		sql.append("         ON Ep.equip_provider_id = E.equip_provider_id ");
		sql.append("            AND Ep.status = 1 ");
		sql.append("WHERE  1 = 1 ");
		
		if (equipmentFilter.getLstIdRecord() != null) {
			sql.append(" AND  edr.equip_delivery_record_id IN (-1 ");
			for (int i = 0, isize = equipmentFilter.getLstIdRecord().size(); i < isize; i++) {
					sql.append(" , ? ");
					params.add(equipmentFilter.getLstIdRecord().get(i));
			}
			sql.append(") ");
		}
		
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getStaffCode())) {
			sql.append(" and upper(sf.staff_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipmentFilter.getStaffCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getCustomerCode())) {
			sql.append(" and (upper(cus.short_code) like ? ESCAPE '/' or upper(cus.name_text) like ? ESCAPE '/') ");
			String customerCode = Unicode2English.codau2khongdau(equipmentFilter.getCustomerCode().trim().toUpperCase());
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
		}
//		if (!StringUtility.isNullOrEmpty(equipmentFilter.getCustomerName())) {
//			sql.append(" and upper(cus.name_text) like ? ESCAPE '/' ");
//			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(equipmentFilter.getCustomerName().toUpperCase())));
//		}
//		if (filter.getFromDate() != null) {
//			sql.append(" and (edr.CREATE_DATE is not null and edr.CREATE_DATE >= trunc(?))");
//			params.add(filter.getFromDate());
//		}
//		if (filter.getToDate() != null) {
//			sql.append(" and ((edr.CREATE_DATE is not null and edr.CREATE_DATE < trunc(?)+1))");
//			params.add(filter.getToDate());
//		}
		//tamvnm: 13/07/2015 - Thay doi tim kiem theo ngay lap
		if (equipmentFilter.getFromDate() != null) {
			sql.append(" and (edr.CREATE_FORM_DATE is not null and edr.CREATE_FORM_DATE >= trunc(?))");
			params.add(equipmentFilter.getFromDate());
		}
		if (equipmentFilter.getToDate() != null) {
			sql.append(" and ((edr.CREATE_FORM_DATE is not null and edr.CREATE_FORM_DATE < trunc(?)+1))");
			params.add(equipmentFilter.getToDate());
		}
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getNumberContract())) {
			sql.append(" and upper(edr.contract_number) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipmentFilter.getNumberContract().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getRecordCode())) {
			sql.append(" and upper(edr.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipmentFilter.getRecordCode().toUpperCase()));
		}
		
		if (!StringUtility.isNullOrEmpty(equipmentFilter.getShopCode())) {
			sql.append("AND ");
			sql.append("( ");
			sql.append("  ? IS NULL ");
			params.add(equipmentFilter.getShopCode().toUpperCase());
			sql.append("  OR ");
			sql.append("  ? = '' ");
			params.add(equipmentFilter.getShopCode().toUpperCase());
			sql.append("  OR ");
			sql.append("  upper(lstshop.shop_code) IN  (select shop_code ");
			sql.append("  from shop ");
			sql.append("   start with shop_code in ");		
			sql.append("  ( ");
			sql.append("         SELECT upper(regexp_substr(?,'[^,]+', 1, level)) ");
			params.add(equipmentFilter.getShopCode().toUpperCase());
			sql.append("         FROM   dual connect BY regexp_substr(?, '[^,]+', 1, level) IS NOT NULL ) ");
			sql.append("  connect by prior shop_id = parent_shop_id) ");
			params.add(equipmentFilter.getShopCode().toUpperCase());
			sql.append(")");
			
		}
		
		if (equipmentFilter.getFromContractDate() != null) {
			sql.append(" and (edr.CONTRACT_CREATE_DATE is not null and edr.CONTRACT_CREATE_DATE >= trunc(?))");
			params.add(equipmentFilter.getFromContractDate());
		}
		if (equipmentFilter.getToContractDate() != null) {
			sql.append(" and ((edr.CONTRACT_CREATE_DATE is not null and edr.CONTRACT_CREATE_DATE < trunc(?)+1))");
			params.add(equipmentFilter.getToContractDate());
		}
		
		if (equipmentFilter.getStatusRecord() != null) {
			sql.append(" and edr.record_status = ? ");
			params.add(equipmentFilter.getStatusRecord());
		}
		if (equipmentFilter.getStatusDelivery() != null) {
			sql.append(" and edr.delivery_status = ? ");
			params.add(equipmentFilter.getStatusDelivery());
		}
		
		if (equipmentFilter.getStatusPrint() != null) {
			if(equipmentFilter.getStatusPrint() == 0) {
				sql.append(" AND edr.print_user is null ");
			} else {
				sql.append(" AND edr.print_user is not null ");
			}
		}
		// update by nhutnn, 10/07/2015
		if (equipmentFilter.getEquipLendId() != null) {
			sql.append(" and edr.EQUIP_LEND_ID = ? ");
			params.add(equipmentFilter.getEquipLendId());
		}
		if (equipmentFilter.getEquipLendCode() != null && !equipmentFilter.getEquipLendCode().isEmpty()) {
			sql.append("  and el.code like ? ESCAPE '/'  ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipmentFilter.getEquipLendCode().toUpperCase()));
		}
		
		//tamvnm: them dieu kien loc theo ma thiet bi
		if (equipmentFilter.getEquipCode() != null) {
			sql.append(" and e.code  like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipmentFilter.getEquipCode().toUpperCase()));
		}
		
		
		sql.append("ORDER  BY edr.code desc ");
		final String[] fieldNames = new String[] {
				"recordCode",			// 0
				"numberContract",		// 1
				"shopCode",				// 2
				"staffCode",			// 3
				"contractCreateDate",	// 4
				"equipmentCode",		// 5
				"seriNumber",			// 6
				"content",				// 7
				"depreciation",			// 8
				"customerCode",			// 9
				"idNO",					// 10
				"idNOPlace",			// 11
				"idNODate",				// 12
				"toBusinessLicense",	// 13
				"toBusinessPlace",		// 14
				"toBusinessDate",		// 15
				"toObjectAddress",		// 16
				"soNha",
				"duong",
				"phuong",
				"quan",
				"tp",
				"toRepresentative",		// 17
				"toPosition",			// 18
				"freezer",				// 19
				"refrigerator",			// 20
				"note",
				"equipGroup",			// 21
				"equipGroupCode",			
				"equipProvider",		// 22
				"price",				// 23
				"health",				// 24
				"manufacturingYear",	// 25
				"createFormDate",		// 26
				"toPermanentAddress"		// 27
		};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.STRING,			// 0
				StandardBasicTypes.STRING,			// 1
				StandardBasicTypes.STRING,			// 2
				StandardBasicTypes.STRING,			// 3
				StandardBasicTypes.DATE,			// 4
				StandardBasicTypes.STRING,			// 5
				StandardBasicTypes.STRING,			// 6
				StandardBasicTypes.STRING,			// 7
				StandardBasicTypes.INTEGER,			// 8
				StandardBasicTypes.STRING,			// 9
				StandardBasicTypes.STRING,			// 10
				StandardBasicTypes.STRING,			// 11
				StandardBasicTypes.DATE,			// 12
				StandardBasicTypes.STRING,			// 13
				StandardBasicTypes.STRING,			// 14
				StandardBasicTypes.DATE,			// 15
				StandardBasicTypes.STRING,			// 16
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,			// 17
				StandardBasicTypes.STRING,			// 18
				StandardBasicTypes.BIG_DECIMAL,		// 19
				StandardBasicTypes.BIG_DECIMAL,		// 20
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,			// 21
				StandardBasicTypes.STRING,			
				StandardBasicTypes.STRING,			// 22
				StandardBasicTypes.BIG_DECIMAL,		// 23
				StandardBasicTypes.STRING,			// 24
				StandardBasicTypes.INTEGER,			// 25
				StandardBasicTypes.DATE,				// 26
				StandardBasicTypes.STRING
		};

		return repo.getListByQueryAndScalar(EquipmentRecordDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public EquipLend getEquipLendById(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * ");
		sql.append(" FROM   equip_lend ");
		sql.append(" WHERE  equip_lend_id IN ");
		sql.append("    ( SELECT el.equip_lend_id ");
		sql.append("      FROM   equip_lend el ");
		sql.append("      JOIN equip_lend_detail eld ");
		sql.append("      ON el.equip_lend_id = Eld.equip_lend_id ");
		sql.append("      JOIN (SELECT * ");
		sql.append("            FROM   (SELECT d.*, Row_number () OVER (partition BY d.customer_id ORDER BY d.create_date DESC) stt ");
		sql.append("                    FROM   equip_delivery_record d ");
		sql.append("                    WHERE  equip_lend_id = ?) row1 ");
		params.add(id);
		sql.append("            WHERE  row1.stt = 1) edr ");
		sql.append("      ON Edr.equip_lend_id = El.equip_lend_id AND Edr.Customer_Id = Eld.Customer_Id ");
		sql.append("      WHERE  el.equip_lend_id = ? ");
		params.add(id);
		sql.append("      AND Edr.record_status IN ( ?, ? ) ");
		params.add(StatusRecordsEquip.APPROVED.getValue());
		params.add(StatusRecordsEquip.CANCELLATION.getValue());
		sql.append("      GROUP  BY el.equip_lend_id ");
		sql.append("      HAVING Count (*) = (SELECT Count (*) ");
		sql.append("     					  FROM   equip_lend_detail ");
		sql.append("    					  WHERE  equip_lend_id = ?) ");
		params.add(id);
		sql.append("    )");
		return repo.getEntityBySQL(EquipLend.class, sql.toString(), params);
	}

	@Override
	public List<EquipmentDeliveryPrintVO> getListEquipDeliveryRecordForPrint(
			List<Long> lstId) throws DataAccessException {
		if (lstId == null) {
			throw new IllegalArgumentException(" lstDeliveryRECId is null ");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT edr.equip_delivery_record_id ");
		sql.append("AS ");
		sql.append("id, ");
//		sql.append("       (SELECT sp.shop_name ");
//		sql.append("        FROM   shop sp ");
//		sql.append("        WHERE  sp.shop_id IN (SELECT sp.parent_shop_id ");
//		sql.append("                              FROM   shop sp ");
//		sql.append("                              WHERE  sp.shop_id = s.parent_shop_id)) ");
//		sql.append("       AS mien, ");
//		sql.append("       (SELECT s1.shop_name ");
//		sql.append("        FROM   shop s1 ");
//		sql.append("        WHERE  s1.shop_id = (SELECT sp.parent_shop_id ");
//		sql.append("                             FROM   shop sp ");
//		sql.append("                             WHERE  sp.shop_id IN (SELECT sp.parent_shop_id ");
//		sql.append("                                                   FROM   shop sp ");
//		sql.append("                                                   WHERE ");
//		sql.append("                                    sp.shop_id = s.parent_shop_id ");
//		sql.append("                                                  ))) ");
//		sql.append("       AS kenh, ");sdfsdfsdf
//		sql.append(" (SELECT sh.shop_name ");
//		sql.append(" FROM   shop sh ");
//		sql.append(" JOIN   channel_type cn ");
//		sql.append(" ON     sh.shop_type_id = cn.channel_type_id ");
//		sql.append(" AND    cn.type = 1 ");
//		sql.append(" WHERE  1 = 1 ");
//		sql.append(" AND    cn.status = 1 ");
//		sql.append(" AND    Decode_islevel_vnm(cn.object_type) = 3 start WITH sh.shop_id = s.shop_id ");
//		sql.append(" AND    sh.status = 1 connect BY sh.shop_id = prior sh.parent_shop_id ) AS mien, ");
//		sql.append("      ( SELECT sh.shop_name ");
//		sql.append("       FROM   shop sh ");
//		sql.append("       JOIN   channel_type cn ");
//		sql.append("       ON     sh.shop_type_id = cn.channel_type_id ");
//		sql.append("       AND    cn.type = 1 ");
//		sql.append("       WHERE  1 = 1 ");
//		sql.append("       AND    cn.status = 1 ");
//		sql.append("       AND    decode_islevel_vnm(cn.object_type) = 2 start WITH sh.shop_id = s.shop_id ");
//		sql.append("       AND    sh.status = 1 connect BY sh.shop_id = prior sh.parent_shop_id ) AS kenh,");
		sql.append("       C.short_code            AS customerCode, ");
		sql.append("       C.customer_name            AS customerName, ");
		sql.append("       Edr.to_representative      AS representative, ");
		sql.append("       Edr.to_phone               AS toPhone, ");
//		sql.append("       C.housenumber              AS houseNumber, ");
//		sql.append("       C.street                   AS street, ");
		sql.append("       edr.ADDRESS              	  AS houseNumber, ");
		sql.append("       edr.STREET                   AS street, ");
		sql.append("       s.shop_name                AS shopName, ");
		sql.append("       s.shop_id                  AS shopId, ");
		sql.append("       s.shop_code                AS shopCode, ");
//		sql.append("       (SELECT A1.area_name ");
//		sql.append("        FROM   area a1 ");
//		sql.append("        WHERE  a1.area_id = c.area_id) ");
//		sql.append("        AS ward, ");
		sql.append("       edr.WARD_NAME 			 AS ward, ");
//		sql.append("       (SELECT A2.area_name ");
//		sql.append("        FROM   area a2 ");
//		sql.append("        WHERE  a2.area_id = (SELECT A1.parent_area_id ");
//		sql.append("                             FROM   area a1 ");
//		sql.append("                             WHERE  a1.area_id = c.area_id)) ");
//		sql.append("       AS district, ");
		sql.append("       edr.DISTRICT_NAME         AS district, ");
//		sql.append("       (SELECT a3.area_name ");
//		sql.append("        FROM   area a3 ");
//		sql.append("        WHERE  a3.area_id = (SELECT A2.parent_area_id ");
//		sql.append("                             FROM   area a2 ");
//		sql.append("                             WHERE  a2.area_id = (SELECT A1.parent_area_id ");
//		sql.append("                                                  FROM   area a1 ");
//		sql.append("                                                  WHERE  a1.area_id = c.area_id) ");
//		sql.append("                            )) AS ");
//		sql.append("       city, ");
		sql.append("       edr.PROVINCE_NAME    as  city, ");
//		sql.append("       (SELECT Count (*) ");
//		sql.append("        FROM   equip_delivery_rec_dtl ");
//		sql.append("        WHERE  equip_delivery_record_id = edr.equip_delivery_record_id) ");
		//tamvnm: so luong luon la 1
		sql.append("      1 AS numberEquip, ");
		sql.append("       (SELECT Eg.BRAND_NAME ");
		sql.append("        FROM   equip_group eg ");
		sql.append("        WHERE  eg.equip_group_id = e.equip_group_id) ");
		sql.append("       AS equipGroup, ");
		sql.append("       (SELECT Ec.NAME ");
		sql.append("        FROM   equip_group eg, ");
		sql.append("               equip_category ec ");
		sql.append("        WHERE  eg.equip_category_id = ec.equip_category_id ");
		sql.append("               AND eg.equip_group_id = e.equip_group_id) ");
		sql.append("       AS equipCategory, ");
		sql.append("      (select ap.value from Ap_Param ap where AP_PARAM_CODE = Edrd.health_status )  AS healthStatus, ");
		sql.append("      (select to_char(eld.TIME_LEND, 'dd/mm/yyyy') from dual)            AS thoiGianNhan, ");
		sql.append("       E.serial                     AS seri, ");
		sql.append("       st.staff_name                AS staffName, ");
//		sql.append("       Decode(st.phone, NULL, st.mobilephone, ");
//		sql.append("                        st.phone) ");
		sql.append("       (case when st.phone is not null and st.mobilephone is not null then st.phone || '/' || st.mobilephone ");
		sql.append("        when st.phone is not null and st.mobilephone is null then st.phone  ");
		sql.append("        when st.phone is null and st.mobilephone is not null then st.mobilephone  end) ");
		sql.append("       AS staffPhone ");
		sql.append("FROM   equip_delivery_record edr ");
		sql.append("       JOIN equip_delivery_rec_dtl edrd ");
		sql.append("         ON edr.equip_delivery_record_id = edrd.equip_delivery_record_id ");
		sql.append("       JOIN customer c ");
		sql.append("         ON edr.customer_id = c.customer_id ");
		sql.append("       JOIN shop s ");
		sql.append("         ON s.shop_id = edr.to_object_id ");
		sql.append("       JOIN equipment e ");
		sql.append("         ON e.equip_id = edrd.equip_id ");
		sql.append("       JOIN staff st ");
		sql.append("         ON st.staff_id = edr.staff_id ");
		sql.append("       JOIN equip_group eg ");
		sql.append("         ON Eg.Equip_Group_Id = E.Equip_Group_Id ");
		sql.append("        JOIN Equip_Category ec  ");
		sql.append("         ON Ec.Equip_Category_Id = Eg.Equip_Category_Id ");
		sql.append("       LEFT JOIN equip_lend_detail eld ");
		sql.append("         ON eld.equip_lend_id = edr.equip_lend_id AND Ec.Equip_Category_Id = eld.Equip_Category_Id ");
		sql.append("WHERE  Edr.equip_delivery_record_id IN ( ");
		
		for (int i = 0; i < lstId.size(); i++) {
			if (i < lstId.size() - 1) {
				sql.append("?, ");
				params.add(lstId.get(i));
			} else {
				sql.append("?");
				params.add(lstId.get(i));
			}
		}
		sql.append(") ");
		sql.append("ORDER  BY s.shop_code, customercode");
		final String[] fieldNames = new String[] {
				"id",				// 1
//				"mien",				// 2
//				"kenh",				// 3
				"customerCode",		// 4 
				"customerName",		// 5
				"representative",  	// 6
				"toPhone",			// 7
				"houseNumber",		// 8
				"street",			// 9
				"shopId",
				"shopName",
				"shopCode",
				"ward",  			// 10
				"district",			// 11
				"city",				// 12
				"numberEquip",		// 13
				"healthStatus",		// 14
				"equipGroup",		// 15
				"equipCategory",	// 16
				"thoiGianNhan",		// 17
				"seri",				// 18
				"staffName",		// 19
				"staffPhone"		// 20
		};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,			// 1
//				StandardBasicTypes.STRING,			// 2
//				StandardBasicTypes.STRING,			// 3
				StandardBasicTypes.STRING,			// 4
				StandardBasicTypes.STRING,			// 5
				StandardBasicTypes.STRING,			// 6
				StandardBasicTypes.STRING,			// 7
				StandardBasicTypes.STRING,			// 8
				StandardBasicTypes.STRING,			// 9
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,			// 10
				StandardBasicTypes.STRING,			// 11
				StandardBasicTypes.STRING,			// 12
				StandardBasicTypes.INTEGER,			// 13
				StandardBasicTypes.STRING,			// 14
				StandardBasicTypes.STRING,			// 15
				StandardBasicTypes.STRING,			// 16
				StandardBasicTypes.STRING,			// 17
				StandardBasicTypes.STRING,			// 18
				StandardBasicTypes.STRING,			// 19
				StandardBasicTypes.STRING			// 20
		};

		return repo.getListByQueryAndScalar(EquipmentDeliveryPrintVO.class, fieldNames, fieldTypes, sql.toString(), params);
	
	}
	
	/**
	 * Kiem tra tinh chung chom Dung tich
	 * 
	 * @author hunglm16
	 * @since May 04,2015
	 * */
	@Override
	public Integer countIntersectionCapacity(Integer fCapacity, Integer tCapacity, Integer status, Long...longsException) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(*) as dem from equip_item_config ");
		sql.append(" where 1 = 1 ");
		if (status != null) {
			sql.append(" and status = ? ");
			params.add(status);
		} else {
			sql.append(" and status > ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (longsException != null && longsException.length > 0) {
			sql.append(" and equip_item_config_id not in ( ? ");
			params.add(longsException[0]);
			for (int i = 1, size = longsException.length; i < size; i++) {
				sql.append(" , ");
				params.add(longsException[i]);
			}
			sql.append(" ) ");
		}
		if (fCapacity != null && tCapacity != null) {
			sql.append(" and ( ");
			sql.append(" (from_capacity is null and to_capacity is not null and to_capacity >= ?) ");
			params.add(fCapacity);
			sql.append(" or (from_capacity is not null and to_capacity is null and from_capacity < ?) ");
			params.add(tCapacity + 1);
			sql.append(" or (from_capacity < ? and to_capacity >= ?)  ");
			params.add(fCapacity + 1);
			params.add(fCapacity);
			sql.append(" or (from_capacity >= ? and from_capacity < ?)  ");
			params.add(fCapacity);
			params.add(tCapacity + 1);
			sql.append(" or (to_capacity >= ? and to_capacity < ?)  ");
			params.add(fCapacity);
			params.add(tCapacity + 1);
			sql.append(" ) ");			
		} else if (tCapacity != null) {
			// Truong hop From is null, To not null
			sql.append(" and ( ");
			sql.append(" from_capacity is null ");
			sql.append(" or (from_capacity < ?) ");
			params.add(tCapacity + 1);
			sql.append(" ) ");
		} else if (fCapacity != null) {
			// Truong hop From not null, To is null
			sql.append(" and ( ");
			sql.append(" to_capacity is null or (to_capacity >= ?) ");
			params.add(fCapacity);
			sql.append(" ) ");
		}
		BigDecimal kq = (BigDecimal) repo.getObjectByQuery(sql.toString(), params);
		if (kq != null) {
			return kq.intValue();
		}
		return 0;
	}
	
	@Override
	public EquipItemConfig getEquipItemConfigByFromAndToCapacity(Integer fCapacity, Integer tCapacity, Integer status) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_item_config where 1 = 1 ");
		if (status != null) {
			sql.append(" and status = ? ");
			params.add(status);
		} else {
			sql.append(" and status > ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (fCapacity != null && tCapacity != null) {
			sql.append(" and from_capacity = ? ");
			sql.append(" and to_capacity = ? ");
			params.add(fCapacity);
			params.add(tCapacity);
		} else if (tCapacity != null) {
			// Truong hop From is null, To not null
			sql.append(" and from_capacity is null ");
			sql.append(" and to_capacity = ? ");
			params.add(tCapacity);
		} else if (fCapacity != null) {
			// Truong hop From not null, To is null
			sql.append(" and from_capacity = ? ");
			sql.append(" and to_capacity is null ");
			params.add(fCapacity);
		} else {
			sql.append(" and from_capacity is null ");
			sql.append(" and to_capacity is null ");
		}
		return repo.getEntityBySQL(EquipItemConfig.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipItemConfigDtl> getListEquipItemConfigDtlByEquipItemConfig(Long equipItemConfigId, Integer status) throws DataAccessException {
		if (equipItemConfigId == null) {
			throw new IllegalArgumentException("equipItemConfigId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from equip_item_config_dtl ");
		sql.append(" where 1 = 1  ");
		sql.append(" and equip_item_config_id = ? ");
		params.add(equipItemConfigId);
		if (status != null) {
			sql.append(" and status = ? ");
			params.add(status);
		} else {
			sql.append(" and status = ? ");
			params.add(ActiveType.RUNNING.getValue());
		}
		return repo.getListBySQL(EquipItemConfigDtl.class, sql.toString(), params);
	}

	@Override
	public List<EquipSuggestEvictionDTL> getListEquipSugEvictionDTL(Long id,
			Long shopId) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * ");
		sql.append(" FROM Equip_Suggest_Eviction_Dtl esed ");
		sql.append(" WHERE Esed.Equip_Suggest_Eviction_Id = ? ");
		params.add(id);
		sql.append(" AND Esed.Shop_Id                     = ?  ");
		params.add(shopId);
		return repo.getListBySQL(EquipSuggestEvictionDTL.class, sql.toString(), params);
	}

	@Override
	public List<EquipStockVO> getEquipStockCMSbyCode(EquipStockFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("WITH  ");
		sql.append(" t_shop_role_under1 as( ");
		sql.append(" select es.shop_id ");
		sql.append(" FROM equip_role er ");
		sql.append(" JOIN equip_role_detail erd      ON er.equip_role_id =erd.equip_role_id ");
		sql.append(" JOIN equip_role_user eru      ON eru.equip_role_id = er.equip_role_id ");
		sql.append(" join equip_stock es on es.Equip_stock_id=erd.Equip_stock_id ");
		sql.append(" WHERE 1              = 1 ");
		sql.append(" AND er.status        = 1 and es.status=1 ");
		sql.append("  AND erd.is_under     = ? ");
		params.add(IsCheckType.YES.getValue());
		sql.append(" AND eru.user_id      = ?");
		params.add(filter.getStaffRoot());
		sql.append(" )");
		sql.append(" ,t_shop_role_under0 as ( ");
		sql.append(" select es.shop_id ");
		sql.append(" FROM equip_role er ");
		sql.append(" JOIN equip_role_detail erd  ON er.equip_role_id =erd.equip_role_id ");
		sql.append(" JOIN equip_role_user eru      ON eru.equip_role_id = er.equip_role_id ");
		sql.append(" join equip_stock es on es.Equip_stock_id=erd.Equip_stock_id ");
		sql.append(" join shop s on s.shop_id = es.shop_id ");
		sql.append(" WHERE er.status      =1 ");
		sql.append(" and s.status         =1 and es.status=1 ");
		sql.append(" AND erd.is_under     =? ");
		params.add(IsCheckType.NO.getValue());
		sql.append("  AND eru.user_id      =? ");
		params.add(filter.getStaffRoot());
		sql.append(" ) ");
		sql.append(" ,t_shop as ( ");
		sql.append(" SELECT shop_id ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status          = 1 ");
		sql.append(" START WITH shop_id IN(select shop_id from t_shop_role_under1) ");
		sql.append(" CONNECT BY prior shop_id=parent_shop_id ");
		sql.append(" ) ");
		sql.append(" ,tbequipstocktmp AS(");
		sql.append(" SELECT es.equip_stock_id, es.name, es.code,s.shop_id, 1 stock_type ");
		sql.append(" FROM equip_stock es ");
		sql.append(" JOIN( ");
		sql.append(" select shop_id from t_shop ");
		sql.append(" UNION ALL ");
		sql.append(" SELECT shop_id from t_shop_role_under0 ");
		sql.append("  ) s ON s.shop_id = es.shop_id ");
		sql.append(" ) ");
		sql.append("  SELECT distinct tbtmp.code code ");
		sql.append("  ,tbtmp.equip_stock_id as equipStockId ");
		sql.append("  ,tbtmp.name name ");
		sql.append(" ,sh.shop_id shopId ");
		sql.append(" ,sh.shop_code shopCode ");
		sql.append(" ,sh.shop_name shopName ");
		sql.append(" ,tbtmp.equip_stock_id equipStockId ");
		sql.append(" FROM shop sh ");
		sql.append(" JOIN tbequipstocktmp tbtmp ON tbtmp.shop_id = sh.shop_id ");
		sql.append(" WHERE 1         = 1 ");
		sql.append(" AND sh.status   = 1 ");
//		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
//			sql.append(" and sh.shop_code like ? ESCAPE '/'");
//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toUpperCase()));
//		}
//		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
//			sql.append(" and lower(sh.shop_name) like ? ESCAPE '/'");
//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopName().trim().toLowerCase()));
//		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and tbtmp.code = ? ");
			params.add(filter.getShopCode().trim().toUpperCase());
		}
		sql.append(" order by sh.shop_code, tbtmp.code ");
		String[] fieldNames = { "code", "equipStockId", "name", "shopId","shopCode","shopName","equipStockId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.LONG,StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipStockVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipStockVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<EquipmentVO> getListEquipmentPeriod(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select distinct ep.equip_period_id as id, ep.code as code, ep.name as name, to_char(ep.from_date,'mm/yyyy') || ' - '|| to_char(ep.to_date,'mm/yyyy') as period, ep.status as status from equip_period ep where 1 = 1 and ep.status <> 1");
		if (filter.getYearManufacture() != null) {
			sql.append(" and  trunc(to_char(from_date, 'yyyy')) = ? ");
			params.add(filter.getYearManufacture());
		} else {
			sql.append(" and  trunc(to_char(from_date, 'yyyy')) = trunc(to_char(sysdate, 'yyyy')) ");
		}
		sql.append("ORDER  BY period");
		
		String[] fieldNames = { "id", "code", "name", "period", "status" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public EquipPeriod getListEquipPeriodById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipPeriod.class, id);
	}

	@Override
	public List<EquipmentVO> getListEquipmentPeriodChangeYear(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();		
		List<Object> params = new ArrayList<Object>();
		//sql.append("select distinct ep.equip_period_id as id,ep.name as name, ep.code as code, to_char(ep.from_date,'mm/yyyy') || ' - '|| to_char(ep.to_date,'mm/yyyy') as period, ep.status as status from equip_period ep where 1 = 1 and ep.status <> 1 ");
		/** Vuongmq; 16/06/2015; khong dung distinct tren con that */
		sql.append("select ep.equip_period_id as id,ep.name as name, ep.code as code, to_char(ep.from_date,'mm/yyyy') || ' - '|| to_char(ep.to_date,'mm/yyyy') as period, ep.status as status from equip_period ep where 1 = 1 and ep.status <> 1 ");
		if (filter.getYearManufacture() != null) {
			sql.append(" and  trunc(to_char(from_date, 'yyyy')) = ? ");
			params.add(filter.getYearManufacture());
		} else {
			sql.append(" and  trunc(to_char(from_date, 'yyyy')) = trunc(to_char(sysdate, 'yyyy')) ");
		}
		sql.append("ORDER  BY ep.from_date");		
		String[] fieldNames = { "id","name", "code", "period", "status" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public Equipment getEquipmentByIdAndBlock(EquipItemFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * ");
		sql.append("FROM   equipment eq ");
		sql.append("WHERE  eq.status = ? ");
		params.add(filter.getStatus().getValue());
		sql.append("       AND Eq.usage_status = ? ");
		params.add(filter.getUsageStatus());
		sql.append("       AND Eq.trade_status = ? ");
		params.add(filter.getTradeStatus());
		sql.append("       AND Eq.equip_id = ? ");
		params.add(filter.getEquipId());
//		sql.append("FOR UPDATE");
		
		return repo.getEntityBySQL(Equipment.class, sql.toString(), params);
	}

	@Override
	public List<EquipmentVO> getListCategoryCount(Long statisticRecord) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct ec.equip_category_id as id, TO_CHAR(ec.equip_category_id) as code, ec.name as name from equip_category ec ");
		sql.append(" join equip_group eg on eg.equip_category_id = ec.equip_category_id ");
		sql.append(" where 1 = 1 and ec.status <> -1");
		sql.append(" order by code ");
		String[] fieldNames = { "id", "name", "code" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipmentGroupVO> getListEquipGroupForFilter(EquipGFilter<EquipmentGroupVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select EQUIP_GROUP_ID as id,  code, name from EQUIP_GROUP ");
		sql.append(" where 1 = 1 ");
		if (filter.getEquipGroupStatus() != null) {
			sql.append(" and status = ? ");
			params.add(filter.getEquipGroupStatus());
		} else {
			sql.append(" and status = ? ");
			params.add(ActiveType.RUNNING.getValue());
		}
		if (filter.getEquipCategoryId() != null) {
			sql.append(" and equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		sql.append(" order by code ");
		final String[] fieldNames = new String[] { "id", "code", "name" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(EquipmentGroupVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<EquipmentGroupVO> getListEquipGroupForFilterByRPT(EquipGFilter<EquipmentGroupVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select EQUIP_GROUP_ID as id,  code, name from EQUIP_GROUP ");
		sql.append(" where 1 = 1 ");
		
		if (filter.getLstStatus() != null && filter.getLstStatus().size() > 0) {
			sql.append(" and status in (-1111 ");
			for (int i = 0, isize = filter.getLstStatus().size(); i < isize; i++) {
				sql.append(" , ? ");
				params.add(filter.getLstStatus().get(i));
			}
			sql.append(" ) ");
		}
		
		if (filter.getEquipGroupStatus() != null) {
			sql.append(" and status = ? ");
			params.add(filter.getEquipGroupStatus());
		}
		if (filter.getEquipCategoryId() != null) {
			sql.append(" and equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		sql.append(" order by code ");
		final String[] fieldNames = new String[] { "id", "code", "name" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(EquipmentGroupVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipDeliveryRecDtl> getListDeliveryDetailByRecordId(Long equipDeliveryId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from Equip_Delivery_Rec_Dtl where Equip_Delivery_Record_Id = ? ");
		params.add(equipDeliveryId);
		return repo.getListBySQL(EquipDeliveryRecDtl.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipGroupProduct> getListEquipGroupProductByFilter(EquipmentGroupProductFilter<EquipGroupProduct> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("SELECT egp.* ");
		sql.append(" FROM equip_group_product egp ");
		sql.append(" JOIN product prd ON egp.product_id = prd.product_id ");
		sql.append(" JOIN equip_group eg ON egp.equip_group_id = eg.equip_group_id ");
		sql.append(" WHERE 1               = 1 ");
		sql.append(" AND prd.status        in(0, 1) and eg.status in (0, 1) ");
		if (filter.getStatus() != null) {
			sql.append(" AND egp.status        = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" AND egp.status        <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getEquipGroupProductIds() != null && !filter.getEquipGroupProductIds().isEmpty()) {
			sql.append(" AND egp.EQUIP_GROUP_PRODUCT_ID IN (?  ");
			params.add(filter.getEquipGroupProductIds().get(0));
			for (int i = 1, size = filter.getEquipGroupProductIds().size(); i < size; i++) {
				sql.append(" ,?  ");
				params.add(filter.getEquipGroupProductIds().get(i));
			}
			sql.append(" )  ");
		}
		if (filter.getEquipGroupCodes() != null && !filter.getEquipGroupCodes().isEmpty()) {
			sql.append(" AND eg.CODE  IN (?  ");
			params.add(filter.getEquipGroupCodes().get(0));
			for (int i = 1, size = filter.getEquipGroupCodes().size(); i < size; i++) {
				sql.append(" ,?  ");
				params.add(filter.getEquipGroupCodes().get(i));
			}
			sql.append(" )  ");
		}
		if (filter.getProductCodes() != null && !filter.getProductCodes().isEmpty()) {
			sql.append(" AND prd.PRODUCT_CODE   IN (? ");
			params.add(filter.getProductCodes().get(0));
			for (int i = 1, size = filter.getProductCodes().size(); i < size; i++) {
				sql.append(" ,?  ");
				params.add(filter.getProductCodes().get(i));
			}
			sql.append(" )  ");
		}
		if (filter.getkPaging() != null) {
			return repo.getListBySQLPaginated(EquipGroupProduct.class, sql.toString(), params, filter.getkPaging());
		}
		return repo.getListBySQL(EquipGroupProduct.class, sql.toString(), params);
	}

	@Override
	public List<EquipmentVO> getListEquipmentStatisticPeriod(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select equip_statistic_record_id as id, code as code, name as name, ");
		sql.append(" to_char(es.from_date, 'dd/mm/yyyy') as fromdate, ");
		sql.append(" to_char(es.to_date, 'dd/mm/yyyy') as toDate");
		sql.append(" from equip_statistic_record es ");
		sql.append(" where 1 = 1 ");
		if (filter.getLstRecordStatus() != null) {
			sql.append(" and es.record_status in (-1 ");
			for (int i = 0, sz = filter.getLstRecordStatus().size(); i < sz; i++) {
				sql.append(", ? ");
				params.add(filter.getLstRecordStatus().get(i));
			}
			sql.append(")");
		}
		if (filter.getPeriodId() != null) {
			sql.append(" and es.equip_priod_id = ? ");
			params.add(filter.getPeriodId());
		}
		if (filter.getShopId() != null) {
			sql.append(" and es.shop_id in ");
			sql.append("  (SELECT shop_id ");
			sql.append("  FROM shop ");
			sql.append("   START WITH shop_id       = ? ");
			params.add(filter.getShopId());
			sql.append("   CONNECT BY prior shop_id = parent_shop_id ");
			sql.append("  ) ");
		}

		String[] fieldNames = { "id", "name", "code", 
								"fromDate", "toDate"};
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
									StandardBasicTypes.STRING, StandardBasicTypes.STRING};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");
		
		if (!StringUtility.isNullOrEmpty(filter.getOrderStatistic())) {
			sql.append(" order by es.from_date desc, es.to_date ");
		} else {
			sql.append(" order by code, name");
		}

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public int checkFromDateToDateInEquipGroupProduct(Long equipGroupId, String productCode, String fDateStr, String tDateStr, int flag) throws DataAccessException {
		if (equipGroupId == null) {
			throw new IllegalArgumentException("equipGroupId is null");
		}
		if (StringUtility.isNullOrEmpty(fDateStr)) {
			throw new IllegalArgumentException("fDateStr is null or emty");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select COUNT(*) as dem  ");
		sql.append(" from EQUIP_GROUP_PRODUCT  ");
		sql.append(" where status = 1 ");
		sql.append(" and EQUIP_GROUP_ID = ? ");
		params.add(equipGroupId);
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and PRODUCT_ID in (select product_id from product where product_code in (?)) ");
			params.add(productCode);
		}
		if (flag == 1) {
			if (!StringUtility.isNullOrEmpty(tDateStr)) {
				sql.append(" and ((from_date is null and to_date is null) ");
				sql.append(" 		or ((from_date is not null and from_date >= to_date(?, 'dd/MM/yyyy') and from_date < to_date(?, 'dd/MM/yyyy') + 1) ");
				params.add(fDateStr);
				params.add(tDateStr);
				sql.append(" 		or (to_date is not null and to_date >= to_date(?, 'dd/MM/yyyy') and to_date < to_date(?, 'dd/MM/yyyy') + 1) ");
				params.add(fDateStr);
				params.add(tDateStr);
				sql.append(" )) ");
			} else {
				sql.append(" and ((from_date is null and to_date is null) or (to_date is not null and to_date >= to_date(?, 'dd/MM/yyyy'))) ");
				params.add(fDateStr);
			}
		} else if (flag == 2) {
			//xet th to_date is null
			sql.append(" and to_date is null and to_date(?, 'dd/MM/yyyy') >= trunc(from_date) ");
			params.add(fDateStr);
		} else {
			return -1;
		}
		BigDecimal kq = (BigDecimal) repo.getObjectByQuery(sql.toString(), params);
		if (kq != null) {
			return kq.intValue();
		}
		return 0;
	}
	
	@Override
	public EquipGroupProduct getPrdInEquipGroupPrdByFlagToDate(Long equipGroupId, String productCode, Date fromDate, Boolean flagToDateIsNull) throws DataAccessException {
		if (equipGroupId == null) {
			throw new IllegalArgumentException("equipGroupId is null");
		}
		if (StringUtility.isNullOrEmpty(productCode)) {
			throw new IllegalArgumentException("equipGroupId is null");
		}
		if (flagToDateIsNull == null) {
			throw new IllegalArgumentException("flagToDateIsNull is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select egp.*  ");
		sql.append(" from EQUIP_GROUP_PRODUCT egp join product p on egp.product_id = p.product_id  ");
		sql.append(" where egp.status = ?  ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and egp.EQUIP_GROUP_ID = ? ");
		params.add(equipGroupId);
		sql.append(" and upper(p.product_code) like ? ESCAPE '/' ");
//		params.add(productCode);
		params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase()));
		if (flagToDateIsNull != null && flagToDateIsNull) {
			if (fromDate == null) {
				throw new IllegalArgumentException("fromDate true -> fromDate is null");
			}
			sql.append(" and to_date is null ");
			sql.append(" and from_date is not null and from_date <= trunc(?)");
			params.add(fromDate);
		}
		return repo.getEntityBySQL(EquipGroupProduct.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipGroupProduct> getPrdInEquipGroupPrdByFdateForDel(Long equipGroupId, String productCode, Date fromDate, Boolean flagNextSysdate) throws DataAccessException {
		if (equipGroupId == null) {
			throw new IllegalArgumentException("equipGroupId is null");
		}
		if (StringUtility.isNullOrEmpty(productCode)) {
			throw new IllegalArgumentException("equipGroupId is null");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select egp.*  ");
		sql.append(" from EQUIP_GROUP_PRODUCT egp join product p on egp.product_id = p.product_id  ");
		sql.append(" where egp.status = ?  ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and egp.EQUIP_GROUP_ID = ? ");
		params.add(equipGroupId);
		sql.append(" and p.product_code in (?) ");
		params.add(productCode);
		sql.append(" and from_date is not null and from_date >= trunc(?)");
		params.add(fromDate);
		if (flagNextSysdate != null && flagNextSysdate) {
			sql.append(" and (to_date is null or (trunc(sysdate) + 1 <= from_date)) ");
		}
		return repo.getListBySQL(EquipGroupProduct.class, sql.toString(), params);
	}

	@Override
	public List<EquipGroupProduct> getPrdInEquipGroupPrdByFdateForUpdateToDate(Long equipGroupId, String productCode, Date fromDate) throws DataAccessException {
		if (equipGroupId == null) {
			throw new IllegalArgumentException("equipGroupId is null");
		}
		if (StringUtility.isNullOrEmpty(productCode)) {
			throw new IllegalArgumentException("equipGroupId is null");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select egp.*  ");
		sql.append(" from EQUIP_GROUP_PRODUCT egp join product p on egp.product_id = p.product_id  ");
		sql.append(" where egp.status = ?  ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and egp.EQUIP_GROUP_ID = ? ");
		params.add(equipGroupId);
		sql.append(" and p.product_code in (?) ");
		params.add(productCode);
		sql.append(" and from_date is not null and from_date < trunc(?)");
		params.add(fromDate);
		sql.append(" and (to_date is null or to_date >= trunc(?))");
		params.add(fromDate);
		return repo.getListBySQL(EquipGroupProduct.class, sql.toString(), params);
	}

	@Override
	public List<EquipmentDeliveryVO> getListEquipmentByRole(
			EquipmentFilter<EquipmentDeliveryVO> filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder whereSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("WITH rdta ");
		sql.append("     AS (SELECT es.equip_stock_id AS equipStockId, ");
		sql.append("                es.code           AS equipStockCode, ");
		sql.append("                es.name           AS equipStockName, ");
		sql.append("                es.shop_id        AS shopId, ");
		sql.append("                sh.shop_code      AS shopCode, ");
		sql.append("                sh.shop_name      AS shopName, ");
		sql.append("                erd.is_under      AS isUnder ");
		sql.append("         FROM   equip_role_user eru ");
		sql.append("                join equip_role er ");
		sql.append("                  ON eru.equip_role_id = er.equip_role_id ");
		sql.append("                join equip_role_detail erd ");
		sql.append("                  ON er.equip_role_id = erd.equip_role_id ");
		sql.append("                join equip_stock es ");
		sql.append("                  ON erd.equip_stock_id = es.equip_stock_id ");
		sql.append("                join shop sh ");
		sql.append("                  ON es.shop_id = sh.shop_id ");
		sql.append("         WHERE  eru.status = 1 ");
		sql.append("                AND er.status = 1 ");
		sql.append("                AND es.status = 1 ");
		sql.append("                AND eru.user_id = ?), ");
		params.add(filter.getStaffRoot());
		
		sql.append("     rstockfull ");
		sql.append("     AS (SELECT DISTINCT equipstockid, ");
		sql.append("                         equipstockcode, ");
		sql.append("                         equipstockname, ");
		sql.append("                         shopcode, ");
		sql.append("                         shopname, ");
		sql.append("                         shopid ");
		sql.append("         FROM   ((SELECT es.equip_stock_id AS equipStockId, ");
		sql.append("                         es.code           AS equipStockCode, ");
		sql.append("                         es.name           AS equipStockName, ");
		sql.append("                         es.shop_id        AS shopId, ");
		sql.append("                         sh.shop_code      AS shopCode, ");
		sql.append("                         sh.shop_name      AS shopName, ");
		sql.append("                         1                 AS isUnder ");
		sql.append("                  FROM   (SELECT * ");
		sql.append("                          FROM   shop ");
		sql.append("                          WHERE  1 = 1 ");
		sql.append("                          START WITH shop_id IN (SELECT shopid ");
		sql.append("                                                 FROM   rdta ");
		sql.append("                                                 WHERE  isunder = 1) ");
		sql.append("                                     AND status = 1 ");
		sql.append("                          CONNECT BY PRIOR shop_id = parent_shop_id) sh ");
		sql.append("                         join equip_stock es ");
		sql.append("                           ON sh.shop_id = es.shop_id ");
		sql.append("                  WHERE  es.status = 1 ");
		sql.append("                  MINUS ");
		sql.append("                  (SELECT es.equip_stock_id AS equipStockId, ");
		sql.append("                          es.code           AS equipStockCode, ");
		sql.append("                          es.name           AS equipStockName, ");
		sql.append("                          es.shop_id        AS shopId, ");
		sql.append("                          sh.shop_code      AS shopCode, ");
		sql.append("                          sh.shop_name      AS shopName, ");
		sql.append("                          1                 AS isUnder ");
		sql.append("                   FROM   shop sh ");
		sql.append("                          join equip_stock es ");
		sql.append("                            ON sh.shop_id = es.shop_id ");
		sql.append("                   WHERE  1 = 1 ");
		sql.append("                          AND sh.shop_id IN (SELECT shopid ");
		sql.append("                                             FROM   rdta ");
		sql.append("                                             WHERE  isunder = 1) ");
		sql.append("                          AND es.equip_stock_id NOT IN (SELECT equipstockid ");
		sql.append("                                                        FROM   rdta))) ");
		sql.append("                 UNION ");
		sql.append("                 (SELECT * ");
		sql.append("                  FROM   rdta ");
		sql.append("                  WHERE  isunder = 0)) ");
		sql.append("         ORDER  BY equipstockcode) ");
		sql.append("		,rshop as ( ");
		sql.append("  		select es.equip_stock_id ");
		sql.append("  		from equip_stock es ");
		sql.append("  		join ( ");
		sql.append("    	select shop_id ");
		sql.append("    	from shop ");
		sql.append("    	START WITH shop_id IN (?) ");
		params.add(filter.getShopRoot());
		sql.append("    	CONNECT BY PRIOR shop_id = parent_shop_id) r on r.shop_id = es.shop_id )");
		sql.append("SELECT DISTINCT eq.equip_id                AS equipmentId, ");
		sql.append("                eq.code                    AS equipmentCode, ");
		sql.append("                eq.serial                  AS seriNumber, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN eq.health_status IS NOT NULL THEN ");
		sql.append("                    (SELECT Min(value) ");
		sql.append("                     FROM   ap_param ap ");
		sql.append("                     WHERE  ap.status = 1 ");
		sql.append("                            AND ap.ap_param_code LIKE ");
		sql.append("                                eq.health_status ");
		sql.append("                            AND ");
		sql.append("                    TYPE LIKE 'EQUIP_CONDITION' ");
		sql.append("                    ) ");
		sql.append("                    ELSE null ");
		sql.append("                  END )                    AS healthStatus, ");
		sql.append("   (SELECT ap.ap_param_code ");
		sql.append("    FROM ap_param ap ");
		sql.append("    WHERE ap.status = 1 ");
		sql.append("    AND ap.ap_param_code LIKE eq.health_status ");
		sql.append("    AND TYPE LIKE 'EQUIP_CONDITION' ");
		sql.append("    ) AS healthCode,   ");
		sql.append("                (SELECT es.code ");
		sql.append("                 FROM   equip_stock es ");
		sql.append("                 WHERE  es.equip_stock_id = eq.stock_id ");
		sql.append("                        AND es.status = 1) AS stockCode, ");
		sql.append("                (SELECT es.name ");
		sql.append("                 FROM   equip_stock es ");
		sql.append("                 WHERE  es.equip_stock_id = eq.stock_id ");
		sql.append("                        AND es.status = 1) AS stockName, ");
		sql.append("                ec.name                    AS typeEquipment, ");
		sql.append("                eg.code                    AS groupEquipmentCode, ");
		sql.append("                eg.name                    AS groupEquipmentName, ");
		sql.append("                ( CASE ");
		sql.append("                    WHEN eg.capacity_from IS NULL ");
		sql.append("                         AND eg.capacity_to IS NULL THEN '' ");
		sql.append("                    WHEN eg.capacity_from IS NULL THEN ' <= ' ");
		sql.append("                                                       ||To_char(eg.capacity_to) ");
		sql.append("                    WHEN eg.capacity_to IS NULL THEN ' >= ' ");
		sql.append("                                                     ||To_char(eg.capacity_from) ");
		sql.append("                    WHEN eg.capacity_to <> eg.capacity_from THEN ");
		sql.append("                    To_char(eg.capacity_from) ");
		sql.append("                    || ' - ' ");
		sql.append("                    || To_char(eg.capacity_to) ");
		sql.append("                    ELSE To_char(eg.capacity_from) ");
		sql.append("                  END )                    AS capacity, ");
		sql.append("                eg.brand_name              AS equipmentBrand, ");
		sql.append("                ep.name                    AS equipmentProvider, ");
		sql.append("                ep.code                    AS equipmentProviderCode, ");
		sql.append("                eq.manufacturing_year      AS yearManufacture, ");
		sql.append("                (SELECT To_char(eq.first_date_in_use, 'yyyy') ");
		sql.append("                 FROM   dual)              AS firstYearInUse, ");
		sql.append("                eq.price                   AS price, ");
		sql.append("                NULL                       AS contentDelivery, ");
		sql.append("                NULL                       AS depreciation ");
		sql.append("FROM   equipment eq ");
		sql.append("       join equip_group eg ");
		sql.append("         ON eg.equip_group_id = eq.equip_group_id ");
		sql.append("            AND eg.status = 1 ");
		sql.append("       join equip_category ec ");
		sql.append("         ON eg.equip_category_id = ec.equip_category_id ");
		sql.append("            AND ec.status = 1 ");
		sql.append("       join equip_provider ep ");
		sql.append("         ON ep.equip_provider_id = eq.equip_provider_id ");
		sql.append("            AND ep.status = 1 ");
		sql.append("       join rstockfull s ");
		sql.append("         ON s.equipstockid = eq.stock_id ");
//		if (!StringUtility.isNullOrEmpty(filter.getEquipLendCode())) {
//			sql.append(" JOIN equip_lend_detail eld ");
//			sql.append(" ON eld.EQUIP_CATEGORY_ID = eg.EQUIP_CATEGORY_ID ");
//			sql.append(" JOIN equip_lend el ");
//			sql.append(" ON eld.equip_lend_id = el.equip_lend_id AND el.code = ? ");
//			sql.append(" AND el.status = 1 AND eld.status = 1 ");
//			params.add(filter.getEquipLendCode());
//		}
		sql.append("WHERE  1 = 1 ");
		sql.append(" and s.equipStockId in (select equip_stock_id from rshop) ");

		if (!StringUtility.isNullOrEmpty(filter.getEquipLendCode())) {
			sql.append(" AND ec.equip_category_id IN ");
			sql.append(" ( ");
			sql.append("       SELECT equip_category_id ");
			sql.append("       FROM   equip_lend_detail ");
			sql.append("       WHERE  equip_lend_id IN ");
			sql.append("              ( ");
			sql.append("                     SELECT equip_lend_id ");
			sql.append("                     FROM   equip_lend ");
			sql.append("                     WHERE  code = ?) and status = 1 ");
			params.add(filter.getEquipLendCode());
			if (filter.getCustomerId() != null) {
				sql.append(" and customer_id = ? ");
				params.add(filter.getCustomerId());
			}
			sql.append(" ) ");
		}
		
		if (filter.getLstEquipAdd() != null && filter.getLstEquipAdd().size() > 0) {
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
				code += ",?";
			}
			sql.append(" and eq.code not in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
				params.add(filter.getLstEquipAdd().get(i));
			}
		}
		
		if (filter.getLstEquipmentCode() != null && filter.getLstEquipmentCode().size() > 0) {
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipmentCode().size(); i++) {
				code += ",?";
			}
			sql.append(" and eq.code not in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipmentCode().size(); i++) {
				params.add(filter.getLstEquipmentCode().get(i));
			}
		}
		
		//tim kiem bang trong dieu kien so sanh
		if (!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			whereSql.append(" and upper(eq.code) = ? ");
			params.add(filter.getEquipCode());
		}
		
		//tim kiem like code tren dialog
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			whereSql.append(" and upper(eq.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
			whereSql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
		}
		if (filter.getEquipCategoryId() != null) {
			whereSql.append(" and ec.equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		if (filter.getEquipGroupId() != null) {
			whereSql.append(" and eg.equip_group_id = ? ");
			params.add(filter.getEquipGroupId());
		}
		if (filter.getEquipProviderId() != null) {
			whereSql.append(" and ep.equip_provider_id = ? ");
			params.add(filter.getEquipProviderId());
		}
		if (filter.getYearManufacture() != null) {
			whereSql.append(" and eq.manufacturing_year = ? ");
			params.add(filter.getYearManufacture());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			whereSql.append(" and s.equipStockCode like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
		}
		if (filter.getStockType() != null) {
			whereSql.append(" and eq.stock_type = ? ");
			params.add(filter.getStockType());
		} else if (filter.getIdRecordDelivery() == null) {
			whereSql.append(" and eq.stock_type != ? ");
			params.add(EquipStockTotalType.KHO_KH.getValue());
		}
		if (filter.getStatusEquip() != null) {
			whereSql.append(" and eq.status = ? ");
			params.add(filter.getStatusEquip());
		} else {
			whereSql.append(" and eq.status != ? ");
			params.add(StatusType.DELETED.getValue());
		}
		if (filter.getTradeStatus() != null) {
			whereSql.append(" and eq.trade_status = ?");
			params.add(filter.getTradeStatus());
		}
		if (filter.getUsageStatus() != null) {
			whereSql.append(" and eq.usage_status = ?");
			params.add(filter.getUsageStatus());
		}	
		
//		String sqlTemp = sql.toString();
		sql.append(whereSql);
//		// them cac thiet bi xoa tam tren danh sach
//		if (filter.getLstEquipDelete() != null && filter.getLstEquipDelete().size() > 0) {
//			sql.append(" union ");
//			sql.append(sqlTemp);
//			if (filter.getLstEquipAdd() != null && filter.getLstEquipAdd().size() > 0) {
//				for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
//					params.add(filter.getLstEquipAdd().get(i));
//				}
//			}
//			String code = "'-1'";
//			for (int i = 0; i < filter.getLstEquipDelete().size(); i++) {
//				code += ",?";
//			}
//			sql.append(" and eq.code in ( " + code + " ) ");
//			for (int i = 0; i < filter.getLstEquipDelete().size(); i++) {
//				params.add(filter.getLstEquipDelete().get(i));
//			}
//			if (!StringUtility.isNullOrEmpty(filter.getCode())) {
//				sql.append(" and upper(eq.code) like ? ESCAPE '/' ");
//				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
//			}
//			if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
//				sql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
//				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
//			}
//			if (filter.getEquipCategoryId() != null) {
//				sql.append(" and ec.equip_category_id = ? ");
//				params.add(filter.getEquipCategoryId());
//			}
//			if (filter.getEquipGroupId() != null) {
//				sql.append(" and eg.equip_group_id = ? ");
//				params.add(filter.getEquipGroupId());
//			}
//			if (filter.getEquipProviderId() != null) {
//				sql.append(" and ep.equip_provider_id = ? ");
//				params.add(filter.getEquipProviderId());
//			}
//			if (filter.getYearManufacture() != null) {
//				sql.append(" and eq.manufacturing_year = ? ");
//				params.add(filter.getYearManufacture());
//			}
//			if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
//				sql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
//				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
//			}
//			if (filter.getStatusEquip() != null) {
//				sql.append(" and eq.status = ? ");
//				params.add(filter.getStatusEquip());
//			} else {
//				sql.append(" and eq.status != ? ");
//				params.add(StatusType.DELETED.getValue());
//			}
//			if (filter.getUsageStatus() != null) {
//				sql.append(" and eq.usage_status = ?");
//				params.add(filter.getUsageStatus());
//			}
//		}
		sql.append(" ORDER BY equipmentCode ASC  ");

		String[] fieldNames = { "equipmentId",// 0
				"equipmentCode",// 1
				"seriNumber",// 2
				"healthStatus",// 3
				"healthCode",	//3.5
				"stockCode",// 4
				"stockName",// 5
				"typeEquipment",// 6
				"groupEquipmentCode",// 7
				"groupEquipmentName",// 8
				"capacity",// 9
				"equipmentBrand",// 10
				"equipmentProvider",// 11
				"equipmentProviderCode",
				"yearManufacture",// 12
				"contentDelivery",// 13
				"firstYearInUse",	//14
				"price",				//15
				"depreciation"
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,//3.5
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.STRING,// 11
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.INTEGER, // 13
				StandardBasicTypes.STRING,	//14
				StandardBasicTypes.BIG_DECIMAL, //15
				StandardBasicTypes.INTEGER
		};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<EquipStockVO> getListEquipStockVOByRole(EquipStockFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("WITH rdta ");
		sql.append("     AS (SELECT es.equip_stock_id AS equipStockId, ");
		sql.append("                es.code           AS equipStockCode, ");
		sql.append("                es.name           AS equipStockName, ");
		sql.append("                es.shop_id        AS shopId, ");
		sql.append("                sh.shop_code      AS shopCode, ");
		sql.append("                sh.shop_name      AS shopName, ");
		sql.append("                erd.is_under      AS isUnder ");
		sql.append("         FROM   equip_role_user eru ");
		sql.append("                join equip_role er ");
		sql.append("                  ON eru.equip_role_id = er.equip_role_id ");
		sql.append("                join equip_role_detail erd ");
		sql.append("                  ON er.equip_role_id = erd.equip_role_id ");
		sql.append("                join equip_stock es ");
		sql.append("                  ON erd.equip_stock_id = es.equip_stock_id ");
		sql.append("                join shop sh ");
		sql.append("                  ON es.shop_id = sh.shop_id ");
		sql.append("         WHERE  eru.status = 1 ");
		sql.append("                AND er.status = 1 ");
		sql.append("                AND es.status = 1 ");
		sql.append("                AND eru.user_id = ?), ");
		params.add(filter.getStaffRoot());
		sql.append("     rstockfull ");
		sql.append("     AS (SELECT DISTINCT equipstockid, ");
		sql.append("                         equipstockcode, ");
		sql.append("                         equipstockname, ");
		sql.append("                         shopcode, ");
		sql.append("                         shopname, ");
		sql.append("                         shopid ");
		sql.append("         FROM   ((SELECT es.equip_stock_id AS equipStockId, ");
		sql.append("                         es.code           AS equipStockCode, ");
		sql.append("                         es.name           AS equipStockName, ");
		sql.append("                         es.shop_id        AS shopId, ");
		sql.append("                         sh.shop_code      AS shopCode, ");
		sql.append("                         sh.shop_name      AS shopName, ");
		sql.append("                         1                 AS isUnder ");
		sql.append("                  FROM   (SELECT * ");
		sql.append("                          FROM   shop ");
		sql.append("                          WHERE  1 = 1 ");
		sql.append("                          START WITH shop_id IN (SELECT shopid ");
		sql.append("                                                 FROM   rdta ");
		sql.append("                                                 WHERE  isunder = 1) ");
		sql.append("                                     AND status = 1 ");
		sql.append("                          CONNECT BY PRIOR shop_id = parent_shop_id) sh ");
		sql.append("                         join equip_stock es ");
		sql.append("                           ON sh.shop_id = es.shop_id ");
		sql.append("                  WHERE  es.status = 1 ");
		sql.append("                  MINUS ");
		sql.append("                  (SELECT es.equip_stock_id AS equipStockId, ");
		sql.append("                          es.code           AS equipStockCode, ");
		sql.append("                          es.name           AS equipStockName, ");
		sql.append("                          es.shop_id        AS shopId, ");
		sql.append("                          sh.shop_code      AS shopCode, ");
		sql.append("                          sh.shop_name      AS shopName, ");
		sql.append("                          1                 AS isUnder ");
		sql.append("                   FROM   shop sh ");
		sql.append("                          join equip_stock es ");
		sql.append("                            ON sh.shop_id = es.shop_id ");
		sql.append("                   WHERE  1 = 1 ");
		sql.append("                          AND sh.shop_id IN (SELECT shopid ");
		sql.append("                                             FROM   rdta ");
		sql.append("                                             WHERE  isunder = 1) ");
		sql.append("                          AND es.equip_stock_id NOT IN (SELECT equipstockid ");
		sql.append("                                                        FROM   rdta))) ");
		sql.append("                 UNION ");
		sql.append("                 (SELECT * ");
		sql.append("                  FROM   rdta ");
		sql.append("                  WHERE  isunder = 0)) ");
		sql.append("         ORDER  BY equipstockcode) ");
		sql.append("         , rshop as ( ");
		sql.append("           select es.equip_stock_id ");
		sql.append("           from equip_stock es ");
		sql.append("           join ( ");
		sql.append("           select shop_id ");
		sql.append("           from shop	 ");
		sql.append("           START WITH shop_id IN (?)	 ");
		params.add(filter.getShopRoot());
		sql.append("           CONNECT BY PRIOR shop_id = parent_shop_id) r on r.shop_id = es.shop_id )	  ");
		sql.append("         		 ");
		sql.append("SELECT s.equipStockId, ");
		sql.append("     s.equipStockCode, ");
		sql.append("     s.equipStockName, ");
		sql.append("     s.equipStockCode as code, ");
		sql.append("     s.equipStockName as name, ");
		sql.append("     s.equipStockId as id, ");
		sql.append("     sh.shop_id as shopId, ");
		sql.append("     sh.shop_code as shopCode, ");
		sql.append("     sh.SHOP_NAME as shopName ");
		
		sql.append("FROM   rstockfull s JOIN shop sh on s.shopId = sh.shop_id where 1 = 1 ");
		sql.append(" AND s.equipStockId in (select equip_stock_id from rshop) ");

		if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "s.shopId");
			sql.append(paramsFix);
		}
		if (!StringUtility.isNullOrEmpty(filter.getToShopCode())) {
			sql.append(" and sh.shop_code = ? ");
			params.add(filter.getToShopCode().toUpperCase());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and sh.shop_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and sh.NAME_TEXT like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getShopName().trim().toUpperCase())));	
			
		}
		if (!StringUtility.isNullOrEmpty(filter.getEquipStockCode())) {
			/*sql.append(" and s.equipStockCode = ? ");
			params.add(filter.getEquipStockCode().toString()); //Datpv4 modified lowerCase 14/07/2015 
			*/		
			sql.append(" and lower(s.equipStockCode) = ? ");
			params.add(filter.getEquipStockCode().toString().toLowerCase()); //Datpv4 modified lowerCase 14/07/2015 
		}

		String[] fieldNames = { 
			"equipStockCode", 
			"equipStockName", 
			"code", 
			"name", 
			"id", 
			"shopId", 
			"shopCode", 
			"shopName", 
			"equipStockId" 
		};
		Type[] fieldTypes = { 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.LONG, 
			StandardBasicTypes.LONG, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.LONG 
		};

		if (filter.getkPaging() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from ( ");
			countSql.append(sql);
			countSql.append(" ) ");
			sql.append(" order by s.equipStockCode, sh.shop_code ");
			return repo.getListByQueryAndScalarPaginated(EquipStockVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			sql.append(" order by s.equipStockCode, sh.shop_code ");
			return repo.getListByQueryAndScalar(EquipStockVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	/**
	 * dem so luong kho theo phan quyen kho
	 * 
	 * @author hunglm16
	 * @since May 26, 2015
	 * 
	 * @param staffRootId: id Nhan vien quyen thay the
	 * @param shopRootId: id don vi tuong tac he thong
	 * @param stockCode: ma kho
	 * */
	@Override
	public int countStockEquipForPermisionCSM(Long staffRootId, Long shopRootId, String stockCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with rdta AS ( ");
		sql.append(" SELECT es.equip_stock_id AS equipStockId, ");
		sql.append(" es.code                 AS equipStockCode, ");
		sql.append(" es.name                 AS equipStockName, ");
		sql.append(" es.shop_id              AS shopId, ");
		sql.append(" erd.is_under            AS isUnder ");
		sql.append(" FROM EQUIP_ROLE_USER eru  ");
		sql.append(" JOIN EQUIP_ROLE er ON eru.EQUIP_ROLE_ID = er.EQUIP_ROLE_ID  ");
		sql.append(" JOIN EQUIP_ROLE_DETAIL erd ON er.EQUIP_ROLE_ID = erd.EQUIP_ROLE_ID ");
		sql.append(" JOIN EQUIP_STOCK es ON erd.EQUIP_STOCK_ID = es.EQUIP_STOCK_ID ");
		sql.append(" WHERE eru.status      = 1 ");
		sql.append(" AND er.status         = 1 ");
		sql.append(" AND es.status         = 1 ");
		if (shopRootId != null) {
			sql.append(" AND es.shop_id       IN ");
			sql.append(" (SELECT shop_id FROM shop WHERE status  = 1 ");
			sql.append("  START WITH shop_id       = ? AND status = 1 ");
			params.add(shopRootId);
			sql.append("  CONNECT BY prior shop_id = parent_shop_id) ");
		}	
		if (staffRootId != null) {
			sql.append(" AND eru.user_id = ? ");
			params.add(staffRootId);
		}
		sql.append(" ), ");
		sql.append(" rStockFull as ( ");
		sql.append(" SELECT DISTINCT equipStockId, equipStockCode, equipStockName ");
		sql.append(" FROM ( ");
		sql.append(" (SELECT es.equip_stock_id AS equipStockId, ");
		sql.append(" es.code                 AS equipStockCode, ");
		sql.append(" es.name                 AS equipStockName, ");
		sql.append(" es.shop_id              AS shopId, ");
		sql.append(" 1            AS isUnder ");
		
//		sql.append("       FROM SHOP sh ");
//		sql.append("     JOIN EQUIP_STOCK es ON sh.shop_id = es.shop_id ");
//		sql.append("      Where 1 = 1 and es.status = 1 ");
//		sql.append(" 	        START WITH sh.shop_id IN ");
//		sql.append(" 	        (SELECT shopId FROM rdta WHERE isUnder = 1) ");
//		sql.append("       AND sh.status                 = 1 CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		
		sql.append(" FROM (SELECT shop_id FROM SHOP ");
		sql.append("         WHERE 1                  = 1 ");
		sql.append("       START WITH shop_id IN ");
		sql.append(" 	        (SELECT shopId FROM rdta WHERE isUnder = 1) ");
		sql.append("       and status                 = 1 ");
		sql.append("       CONNECT BY prior shop_id = parent_shop_id) sh ");
		sql.append("       JOIN EQUIP_STOCK es ON sh.shop_id = es.shop_id ");
		sql.append("       WHERE es.status            = 1 ");
		
		sql.append(" 	    MINUS ( ");
		sql.append(" 	        SELECT es.equip_stock_id AS equipStockId, ");
		sql.append(" 	        es.code                 AS equipStockCode, ");
		sql.append("         es.name                 AS equipStockName, ");
		sql.append(" 	        es.shop_id              AS shopId, ");
		sql.append(" 	        1            AS isUnder ");
		sql.append("       FROM SHOP sh ");
		sql.append("       JOIN EQUIP_STOCK es ON sh.shop_id = es.shop_id  ");
		sql.append("       Where 1 = 1 ");
		sql.append("       AND sh.shop_id IN (SELECT shopId FROM rdta WHERE isUnder = 1) ");
		sql.append("       AND es.equip_stock_id NOT IN (SELECT equipStockId FROM rdta)) ");
		sql.append("     )   ");
		sql.append("     UNION (SELECT * FROM rdta WHERE isUnder = 0)  ");
		sql.append(" 	    )  ");
		sql.append("     ) ");
		sql.append(" 	    select count(*) dem from rStockFull where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(stockCode)) {
			sql.append(" and lower(equipStockCode) like ? ESCAPE '/'");
			params.add(stockCode.toLowerCase().trim());
		}
		BigDecimal kq = (BigDecimal) repo.getObjectByQuery(sql.toString(), params);
		if (kq != null) {
			return kq.intValue();
		}
		return 0;
	}
	
	/**
	 * @author phuongvm
	 * @date 26/05/2015
	 * Lay thiet bi theo ma code theo phan quyen kho
	 */
	@Override
	public Equipment getEquipmentByCodeHasPermission(String code,Long staffRootId,Long shopRootId) throws DataAccessException {
		if (code == null) {
			throw new IllegalArgumentException("code is null");
		}
		if (staffRootId == null) {
			throw new IllegalArgumentException("staffRootId is null");
		}
		if (shopRootId == null) {
			throw new IllegalArgumentException("shopRootId is null");
		}
		StringBuffer  sql = new StringBuffer();
		List<Object> params = new ArrayList<Object>();	
		sql.append(" WITH res AS ");
		sql.append("  (SELECT es.equip_stock_id AS equipStockId, ");
		sql.append("  es.code                 AS equipStockCode,");
		sql.append("  es.name                 AS equipStockName,");
		sql.append("  es.shop_id              AS shopId,");
		sql.append("  erd.is_under            AS isUnder");
		sql.append("  FROM EQUIP_ROLE_USER eru");
		sql.append("  JOIN EQUIP_ROLE er");
		sql.append("  ON eru.EQUIP_ROLE_ID = er.EQUIP_ROLE_ID");
		sql.append("  JOIN EQUIP_ROLE_DETAIL erd");
		sql.append("  ON er.EQUIP_ROLE_ID = erd.EQUIP_ROLE_ID");
		sql.append("  JOIN EQUIP_STOCK es");
		sql.append("  ON erd.EQUIP_STOCK_ID = es.EQUIP_STOCK_ID and es.status = 1");
		sql.append("  WHERE eru.status      = 1");
		sql.append("  AND er.status         = 1");
		sql.append("  AND es.shop_id       IN");
		sql.append("    (SELECT shop_id");
		sql.append("    FROM shop");
		sql.append("    WHERE status               = 1");
		if (shopRootId != null) {
			sql.append("        START WITH shop_id       = ?");
			params.add(shopRootId);
			sql.append("  		AND status                 = 1");
			sql.append("      CONNECT BY prior shop_id = parent_shop_id");
		}
		sql.append("    )");
		if (staffRootId != null) {
			sql.append("  AND eru.user_id = ?");
			params.add(staffRootId);
		}
		sql.append("    )");
		sql.append("  ,");
		sql.append("  stock as ( ");
		sql.append("  SELECT DISTINCT equipStockId, equipStockCode,equipStockName,shopId");
		sql.append("  FROM (");
		sql.append("    (SELECT es.equip_stock_id AS equipStockId,");
		sql.append("     es.code                 AS equipStockCode,");
		sql.append("      es.name                 AS equipStockName,");
		sql.append("      es.shop_id              AS shopId,");
		sql.append("      1            AS isUnder");
		sql.append("  	  FROM SHOP sh ");
		sql.append("      JOIN EQUIP_STOCK es ON sh.shop_id = es.shop_id");
		sql.append("   Where 1 = 1 and es.status = 1");
		sql.append("  AND sh.shop_id      IN");
		sql.append("    (SELECT shop_id FROM shop WHERE status               = 1");
		if (shopRootId != null) {
			sql.append("    START WITH shop_id       = ?");
			params.add(shopRootId);
			sql.append("    AND status                 = 1 CONNECT BY prior shop_id = parent_shop_id");
		}
		sql.append("  )");
		sql.append("   START WITH sh.shop_id IN");
		sql.append("  (SELECT shopId FROM res WHERE isUnder = 1) ");
		sql.append("   AND sh.status                 = 1 CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sql.append("   MINUS (");
		sql.append("    SELECT es.equip_stock_id AS equipStockId,");
		sql.append("  es.code                 AS equipStockCode,");
		sql.append("  es.name                 AS equipStockName,");
		sql.append("  es.shop_id              AS shopId,");
		sql.append("  1            AS isUnder");
		sql.append("  FROM SHOP sh");
		sql.append("  JOIN EQUIP_STOCK es ON sh.shop_id = es.shop_id ");
		sql.append(" Where 1 = 1 ");
		sql.append(" AND sh.shop_id IN (SELECT shopId FROM res WHERE isUnder = 1) ");
		sql.append("  AND es.equip_stock_id NOT IN (SELECT equipStockId FROM res)");
		sql.append("   )");
		sql.append("   )");
		sql.append("  UNION");
		sql.append("  	(SELECT * FROM res WHERE isUnder = 0  ) ) ORDER BY equipStockCode");
		sql.append("   ),");
		sql.append("  equipstock as (  ");
		sql.append("   SELECT s.equipStockId id,s.equipStockCode code, s.equipStockName name");
		sql.append("  from stock s");
		sql.append("    )");
		
		sql.append("SELECT  eq.* ");
		sql.append("FROM   equipment eq ");
		sql.append("       JOIN equip_group eg ");
		sql.append("         ON eg.equip_group_id = eq.equip_group_id ");
		sql.append("       JOIN equip_category ec ");
		sql.append("         ON eg.equip_category_id = ec.equip_category_id ");
		sql.append("       JOIN equip_provider ep ");
		sql.append("         ON ep.equip_provider_id = eq.equip_provider_id ");
		sql.append("WHERE  1 = 1 And eq.stock_id in (select id from equipstock)");
		sql.append("       AND eq.code = ? ");
		params.add(code.toUpperCase().trim());
		
		
		return repo.getEntityBySQL(Equipment.class, sql.toString(), params);
	}

	@Override
	public List<EquipSuggestEvictionDTL> getListSuggestDetailByRecordId(Long equipSuggestId, Integer status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from Equip_Suggest_Eviction_Dtl where Equip_Suggest_Eviction_Id = ? ");
		params.add(equipSuggestId);
		if (status != null) {
			sql.append(" and  DELIVERY_STATUS = ? ");
			params.add(status);
		}
		return repo.getListBySQL(EquipSuggestEvictionDTL.class, sql.toString(), params);
	}

	@Override
	public List<EquipmentManagerVO> getListEquipmentVOByShopInCMS(EquipmentFilter<EquipmentManagerVO> filter) throws DataAccessException {
		if (filter.getShopRoot() == null) {
			throw new IllegalArgumentException("shopRoot is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with rShop as ( ");
		sql.append("   select * from shop where status = 1 start with shop_id = ? and status = 1 connect by prior shop_id = PARENT_SHOP_ID ");
		params.add(filter.getShopRoot());
		sql.append(" ) ");
		sql.append(" select eq.equip_id as equipmentId, ");
		sql.append(" eq.code as equipmentCode, ");
		sql.append(" eq.serial as seriNumber, ");
		sql.append(" (case when eq.health_status is not null then (select min(value) from ap_param ap where ap.status = 1 and ap.ap_param_code like eq.health_status and type like 'EQUIP_CONDITION')else '' end) as healthStatus, ");
		sql.append(" eq.stock_code as stockCode, ");
		sql.append(" eq.stock_name as stockName, ");
		sql.append(" ec.name as typeEquipment, ");
		sql.append(" eg.code as groupEquipmentCode, ");
		sql.append(" eg.name as groupEquipmentName, ");
		sql.append(" ( CASE WHEN eg.capacity_from IS NULL AND eg.capacity_to IS NULL THEN ''  ");
		sql.append(" WHEN eg.capacity_from IS NULL THEN '<' ||TO_CHAR(eg.capacity_to) ");
		sql.append(" WHEN eg.capacity_to IS NULL THEN '>' ||TO_CHAR(eg.capacity_from) ");
		sql.append(" WHEN eg.capacity_to <> eg.capacity_from THEN TO_CHAR(eg.capacity_from) || ' - ' || TO_CHAR(eg.capacity_to) ");
		sql.append(" ELSE TO_CHAR(eg.capacity_from) END) AS capacity, ");
		sql.append(" eg.brand_name as equipmentBrand, ");
		sql.append(" ep.name as equipmentProvider, ");
		sql.append(" eq.manufacturing_year as yearManufacture, ");
		sql.append(" eq.price as price ");
		sql.append(" from EQUIPMENT eq join EQUIP_STOCK et ON eq.STOCK_ID = et.EQUIP_STOCK_ID ");
		sql.append(" join EQUIP_GROUP eg on eq.EQUIP_GROUP_ID = eg.EQUIP_GROUP_ID ");
		sql.append(" join EQUIP_CATEGORY ec on eg.EQUIP_CATEGORY_ID = ec.EQUIP_CATEGORY_ID ");
		sql.append(" join EQUIP_PROVIDER ep on ep.EQUIP_PROVIDER_ID = eq.EQUIP_PROVIDER_ID ");
		sql.append(" join (select * from rshop) sh on sh.shop_id = et.shop_id ");
		sql.append(" where eq.status = 1 and et.STATUS = 1 and eg.STATUS = 1 and ec.STATUS = 1 and ep.STATUS = 1 ");
		if (filter.getStockType() != null) {
			sql.append(" and eq.STOCK_TYPE = ? ");
			params.add(filter.getStockType());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and eq.code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
			sql.append(" and lower(eq.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toLowerCase()));
		}
		if (filter.getEquipCategoryId() != null) {
			sql.append(" and ec.equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		if (filter.getEquipGroupId() != null) {
			sql.append(" and eg.equip_group_id = ? ");
			params.add(filter.getEquipGroupId());
		}
		if (filter.getEquipProviderId() != null) {
			sql.append(" and ep.equip_provider_id = ? ");
			params.add(filter.getEquipProviderId());
		}
		if (filter.getYearManufacture() != null) {
			sql.append(" and eq.manufacturing_year = ? ");
			params.add(filter.getYearManufacture());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			sql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
		}
		if (filter.getStatusEquip() != null) {
			sql.append(" and eq.status = ? ");
			params.add(filter.getStatusEquip());
		} else {
			sql.append(" and eq.status != ? ");
			params.add(StatusType.DELETED.getValue());
		}
		if (filter.getTradeStatus() != null) {
			sql.append(" and eq.trade_status = ? ");
			params.add(filter.getTradeStatus());
			if (EquipTradeStatus.NOT_TRADE.getValue().equals(filter.getTradeStatus())) {
				sql.append(" and eq.TRADE_TYPE is null ");
			}
		}
		if (filter.getUsageStatus() != null) {
			sql.append(" and eq.usage_status = ?");
			params.add(filter.getUsageStatus());
		}
		String sqlTemp = sql.toString();
		sql.append(" ORDER BY equipmentCode ASC  ");

		String[] fieldNames = { "equipmentId",// 0
				"equipmentCode",// 1
				"seriNumber",// 2
				"healthStatus",// 3
				"stockCode",// 4
				"stockName",// 5
				"typeEquipment",// 6
				"groupEquipmentCode",// 7
				"groupEquipmentName",// 8
				"capacity",// 9
				"equipmentBrand",// 10
				"equipmentProvider",// 11
				"yearManufacture",// 12
				"price",// 13
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.STRING,// 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.BIG_DECIMAL, // 13
		};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sqlTemp).append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentManagerVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentManagerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public List<EquipmentManagerVO> getListEquipmentVOByShopInCMSEx(EquipmentFilter<EquipmentManagerVO> filter) throws DataAccessException {
		if (filter.getShopRoot() == null) {
			throw new IllegalArgumentException("shopRoot is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with ");
		if (filter.getId() != null) {
			sql.append(" shopRecord as ( ");
			sql.append(" SELECT shop_id FROM shop WHERE status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("   START WITH shop_id IN (SELECT UNIT_ID FROM EQUIP_STATISTIC_UNIT where STATUS = ? AND EQUIP_STATISTIC_RECORD_ID = ?) ");
			params.add(ActiveType.RUNNING.getValue());
			params.add(filter.getId());
			sql.append("    AND status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("   CONNECT BY prior shop_id = PARENT_SHOP_ID ");
			sql.append(" ), ");
		}
		sql.append(" rShop as ( ");
		sql.append("   select * from shop where status = ?  ");
		params.add(ActiveType.RUNNING.getValue());
		if(!StringUtility.isNullOrEmpty(filter.getShopCode())){
			sql.append(" and shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().toUpperCase()));
		}
		sql.append("  start with shop_id = ? and status = ? connect by prior shop_id = PARENT_SHOP_ID ");
		params.add(filter.getShopRoot());
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ) ");
		sql.append(" select eq.equip_id as equipmentId, ");
		sql.append(" eq.code as equipmentCode, ");
		sql.append(" eq.serial as seriNumber, ");
		sql.append(" (case when eq.health_status is not null then (select min(value) from ap_param ap where ap.status = 1 and ap.ap_param_code like eq.health_status and type like 'EQUIP_CONDITION')else null end) as healthStatus, ");
		sql.append(" eq.stock_code as stockCode, ");
		sql.append(" eq.stock_name as stockName, ");
		sql.append(" ec.name as typeEquipment, ");
		sql.append(" eg.code as groupEquipmentCode, ");
		sql.append(" eg.name as groupEquipmentName, ");
		sql.append(" ( CASE WHEN eg.capacity_from IS NULL AND eg.capacity_to IS NULL THEN ''  ");
		sql.append(" WHEN eg.capacity_from IS NULL THEN '<=' ||TO_CHAR(eg.capacity_to) ");
		sql.append(" WHEN eg.capacity_to IS NULL THEN '>=' ||TO_CHAR(eg.capacity_from) ");
		sql.append(" WHEN eg.capacity_to <> eg.capacity_from THEN TO_CHAR(eg.capacity_from) || ' - ' || TO_CHAR(eg.capacity_to) ");
		sql.append(" ELSE TO_CHAR(eg.capacity_from) END) AS capacity, ");
		sql.append(" eg.brand_name as equipmentBrand, ");
		sql.append(" ep.name as equipmentProvider, ");
		sql.append(" eq.manufacturing_year as yearManufacture, ");
		sql.append(" eq.price as price ");
		sql.append(" ,c.short_code shortCode ");
		sql.append(" ,c.customer_name customerName ");
		sql.append(" ,c.address ");
		sql.append(" ,case when c.customer_id is not null then( select shop_code from shop where shop_id = c.shop_id ) else (select shop_code from shop where shop_id = et.shop_id) end shopCode ");
		sql.append(" from EQUIPMENT eq left join EQUIP_STOCK et ON eq.STOCK_ID = et.EQUIP_STOCK_ID AND eq.STOCK_TYPE   = ?");
		params.add(EquipStockTotalType.KHO.getValue());
		sql.append(" join customer c on c.customer_id = eq.stock_id and eq.stock_type = ?");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		if (filter.getId() != null) {
			sql.append(" join shopRecord sr on c.shop_id = sr.shop_id ");
		}
		sql.append(" join EQUIP_GROUP eg on eq.EQUIP_GROUP_ID = eg.EQUIP_GROUP_ID ");
		sql.append(" join EQUIP_CATEGORY ec on eg.EQUIP_CATEGORY_ID = ec.EQUIP_CATEGORY_ID ");
		sql.append(" join EQUIP_PROVIDER ep on ep.EQUIP_PROVIDER_ID = eq.EQUIP_PROVIDER_ID ");
		sql.append(" ");
		sql.append(" where eq.status = 1 and eg.STATUS = 1 and ec.STATUS = 1 and ep.STATUS = 1 AND eq.usage_status in (?,?,?) and( (eq.stock_type  = 1 and et.shop_id in (select shop_id from rShop) ) or (eq.stock_type = 2 and c.shop_id in (select shop_id from rShop )) )");
		params.add(EquipUsageStatus.IS_USED.getValue());
		params.add(EquipUsageStatus.SHOWING_REPAIR.getValue());
		params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and eq.code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
			sql.append(" and lower(eq.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toLowerCase()));
		}
		if (filter.getEquipCategoryId() != null) {
			sql.append(" and ec.equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		if (filter.getEquipGroupId() != null) {
			sql.append(" and eg.equip_group_id = ? ");
			params.add(filter.getEquipGroupId());
		}
		if (filter.getEquipProviderId() != null) {
			sql.append(" and ep.equip_provider_id = ? ");
			params.add(filter.getEquipProviderId());
		}
		if (filter.getYearManufacture() != null) {
			sql.append(" and eq.manufacturing_year = ? ");
			params.add(filter.getYearManufacture());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			sql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
		}
//		if (filter.getStatusEquip() != null) {
//			sql.append(" and eq.status = ? ");
//			params.add(filter.getStatusEquip());
//		} else {
//			sql.append(" and eq.status != ? ");
//			params.add(StatusType.DELETED.getValue());
//		}
		if (filter.getTradeStatus() != null) {
			sql.append(" and eq.trade_status = ? ");
			params.add(filter.getTradeStatus());
			if (EquipTradeStatus.NOT_TRADE.getValue().equals(filter.getTradeStatus())) {
				sql.append(" and eq.TRADE_TYPE is null ");
			}
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and (upper(c.short_code) like ? ESCAPE '/' or c.name_text like ? ESCAPE '/')");
			String customer = filter.getCustomerCode().trim().toUpperCase();
			params.add(StringUtility.toOracleSearchLikeSuffix(customer));
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(customer)));
		}
//		if (filter.getUsageStatus() != null) {
//			sql.append(" and eq.usage_status = ?");
//			params.add(filter.getUsageStatus());
//		}else {
//			sql.append(" and eq.usage_status not in (?,?)");
//			params.add(EquipUsageStatus.LIQUIDATED.getValue());
//			params.add(EquipUsageStatus.LOST.getValue());
//		}
		if(filter.getId()!=null){
			sql.append(" and eq.equip_id not in (select OBJECT_ID from EQUIP_STATISTIC_GROUP where EQUIP_STATISTIC_RECORD_ID = ? and OBJECT_TYPE = ?)");
			params.add(filter.getId());
			params.add(EquipObjectType.EQUIP.getValue());
		}
		String sqlTemp = sql.toString();
		sql.append(" ORDER BY equipmentCode ASC  ");

		String[] fieldNames = { "equipmentId",// 0
				"equipmentCode",// 1
				"seriNumber",// 2
				"healthStatus",// 3
				"stockCode",// 4
				"stockName",// 5
				"typeEquipment",// 6
				"groupEquipmentCode",// 7
				"groupEquipmentName",// 8
				"capacity",// 9
				"equipmentBrand",// 10
				"equipmentProvider",// 11
				"yearManufacture",// 12
				"price",// 13
				"shopCode",
				"shortCode",
				"customerName",
				"address",
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.STRING,// 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.BIG_DECIMAL, // 13
				StandardBasicTypes.STRING, // 14
				StandardBasicTypes.STRING, // 15
				StandardBasicTypes.STRING, // 16
				StandardBasicTypes.STRING, // 17
		};
		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sqlTemp).append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentManagerVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentManagerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<EquipHistory> getListEquipHistoryByEquipId(Long equipId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_history where Status = 1 and Equip_Id = ? ");
		params.add(equipId);
		return repo.getListBySQL(EquipHistory.class, sql.toString(), params);
	}

	@Override
	public List<EquipPeriod> getListEquipPeriodByDate(Date d) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * ");
		sql.append("FROM   equip_period ");
		sql.append("WHERE  status = 2 ");
		sql.append("       AND from_date <= ? ");
		params.add(d);
		sql.append("       AND to_date >= ? ");
		params.add(d);
		sql.append("ORDER  BY create_date DESC");
		return repo.getListBySQL(EquipPeriod.class, sql.toString(), params);
	}

	@Override
	public List<EquipmentRecordDeliveryVO> getEquipmentRecordDeliveryVOByFilter(EquipmentFilter<EquipmentRecordDeliveryVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select DISTINCT edr.code as recordCode, ");
		sql.append(" edr.UPDATE_DATE, ");
		sql.append(" edr.TO_REPRESENTATIVE as toRepresentative, ");
		sql.append(" edr.relation as toRelation, ");
		sql.append(" edr.address as toHouseNumber, ");
		sql.append(" edr.street as toStreet, ");
		sql.append(" edr.Province_Name as toProvinceName, ");
		sql.append(" edr.District_Name as toDistrictName, ");
		sql.append(" edr.Ward_Name as toWardName ");
		sql.append(" from EQUIP_DELIVERY_RECORD edr ");
		sql.append(" join customer cs on cs.CUSTOMER_ID = edr.CUSTOMER_ID ");
		sql.append(" join EQUIP_DELIVERY_REC_DTL edrdt on edrdt.EQUIP_DELIVERY_RECORD_ID = edr.EQUIP_DELIVERY_RECORD_ID ");
		sql.append(" join equipment eq on eq.equip_id = edrdt.equip_id ");
		sql.append(" where 1=1 ");
		if(!StringUtility.isNullOrEmpty(filter.getCustomerCode())){
			sql.append(" and cs.short_code = ? ");
			params.add(filter.getCustomerCode());
		}
		if(!StringUtility.isNullOrEmpty(filter.getEquipCode())){
			sql.append(" and eq.code = ? ");
			params.add(filter.getEquipCode());
		}
		if(!StringUtility.isNullOrEmpty(filter.getShopCode())){
			sql.append(" and cs.shop_id in (select s.shop_id from shop s where s.status = ? and s.shop_code = ?) ");
			params.add(ActiveType.RUNNING.getValue());
			params.add(filter.getShopCode());
		}
		sql.append(" order by edr.code desc, edr.UPDATE_DATE desc ");
		
		final String[] fieldNames = new String[] {
				"recordCode",			// 0
				"toRepresentative",		// 1
				"toRelation",				// 2
				"toHouseNumber",			// 3
				"toStreet",	// 4
				"toProvinceName",		// 5
				"toDistrictName",			// 6
				"toWardName",				// 7
		};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.STRING,			// 0
				StandardBasicTypes.STRING,			// 1
				StandardBasicTypes.STRING,			// 2
				StandardBasicTypes.STRING,			// 3
				StandardBasicTypes.STRING,			// 4
				StandardBasicTypes.STRING,			// 5
				StandardBasicTypes.STRING,			// 6
				StandardBasicTypes.STRING,			// 7				
		};

		return repo.getListByQueryAndScalar(EquipmentRecordDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public EquipFormHistory getEquipFormHistory(Long recordId) throws DataAccessException {
		// TODO Auto-generated method stub
		List<Object> params = new ArrayList<Object>();
		StringBuilder  varname1 = new StringBuilder();
		varname1.append("SELECT * ");
		varname1.append("FROM   equip_form_history ");
		varname1.append("WHERE  equip_form_history_id = (SELECT Max(equip_form_history_id) ");
		varname1.append("                                FROM   equip_form_history ");
		varname1.append("                                WHERE  1 = 1 ");
		varname1.append("                                       AND record_id = ?)");
		params.add(recordId);
		return repo.getEntityBySQL(EquipFormHistory.class, varname1.toString(), params);
	}
}

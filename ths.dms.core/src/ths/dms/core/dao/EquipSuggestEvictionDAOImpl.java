package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.EquipSuggestEviction;
import ths.dms.core.entities.EquipSuggestEvictionDTL;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.filter.EquipSuggestEvictionFilter;
import ths.dms.core.entities.vo.EquipSuggestEvictionDetailVO;
import ths.dms.core.entities.vo.EquipmentSuggestEvictionVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

/**
 * Tang xu ly tuong tac DB
 * 
 * @author nhutnn
 * @since 14/07/2015
 * @description Dung cho Quan Ly de nghi thu hoi thiet bi
 * */
public class EquipSuggestEvictionDAOImpl implements EquipSuggestEvictionDAO {
	@Autowired
	private IRepository repo;

	@Override
	public List<EquipmentSuggestEvictionVO> searchListEquipSuggestEvictionVOByFilter(EquipSuggestEvictionFilter<EquipmentSuggestEvictionVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		if (filter == null) {
			throw new IllegalArgumentException("filter must not be null");
		}
		if (filter.getLstShopId() == null || filter.getLstShopId().size() == 0) {
			throw new IllegalArgumentException("filter.getLstShopId() == null || filter.getLstShopId().size() == 0");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShopTemp as ( ");
		sql.append(" select s.shop_id, s.shop_code, s.shop_name ");
		sql.append(" from shop s ");
		//tamvnm: 09/09/2015 bo check status shop, kh
//		sql.append(" where s.status = 1 ");
		sql.append(" start WITH s.shop_id in (?");
		params.add(filter.getLstShopId().get(0));
		for (int i = 1, sz = filter.getLstShopId().size(); i < sz; i++) {
			sql.append(",?");
			params.add(filter.getLstShopId().get(i));
		}
		sql.append(")");
		sql.append(" CONNECT by PRIOR s.shop_id = s.parent_shop_id ");
		sql.append(" ) ");
		sql.append(" select ese.equip_suggest_eviction_id id, ");
		sql.append(" ese.code code, ");
		sql.append(" ese.note as note, ");
		sql.append(" lst.shop_code shopCode, ");
		sql.append(" lst.shop_name shopName, ");
		sql.append(" ese.status, ");
		sql.append(" to_char(ese.create_form_date, 'dd/mm/yyyy') createFormDate, ");
		sql.append(" ese.delivery_status statusDelivery ");
		sql.append(" from equip_suggest_eviction ese ");
		sql.append(" join lstShopTemp lst on lst.shop_id = ese.shop_id ");
		sql.append(" where 1=1 ");

		if (filter.getStatus() != null) {
			sql.append(" and ese.status = ? ");
			params.add(filter.getStatus());
		} else {
			if (filter.getLstStatus() != null) {
				sql.append(" and ese.status in (?");
				params.add(filter.getLstStatus().get(0));
				for (int i = 1, sz = filter.getLstStatus().size(); i < sz; i++) {
					sql.append(",?");
					params.add(filter.getLstStatus().get(i));
				}
				sql.append(") ");
			} else {
				sql.append(" and ese.status <> ? ");
				params.add(ActiveType.DELETED.getValue());
			}
		}

		if (filter.getDeliveryStatus() != null) {
			sql.append(" and ese.DELIVERY_STATUS = ? ");
			params.add(filter.getDeliveryStatus());
		} 

		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and ese.code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toUpperCase()));
		}

		if (filter.getFromDate() != null) {
			sql.append(" and ese.CREATE_FORM_DATE >= trunc(?) ");
			params.add(filter.getFromDate());
		}

		if (filter.getToDate() != null) {
			sql.append(" and ese.CREATE_FORM_DATE < trunc(?)+1 ");
			params.add(filter.getToDate());
		}

		String[] fieldNames = { "id", //1
				"code", //2
				"note",
				"shopCode", //3 
				"shopName", //4 
				"status", //7
				"createFormDate", //8  
				"statusDelivery" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.INTEGER, //7
				StandardBasicTypes.STRING, //8
				StandardBasicTypes.INTEGER };

		if (filter.getkPaging() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from ( ");
			countSql.append(sql);
			countSql.append(" ) ");
			sql.append(" ORDER BY lst.shop_code, ese.code, ese.create_form_date ");
			return repo.getListByQueryAndScalarPaginated(EquipmentSuggestEvictionVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			sql.append(" ORDER BY lst.shop_code, ese.code, ese.create_form_date ");
			return repo.getListByQueryAndScalar(EquipmentSuggestEvictionVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public EquipSuggestEviction getEquipSuggestEvictionByCode(String code) throws DataAccessException {
		// TODO Auto-generated method stub
		if (code == null) {
			throw new IllegalArgumentException("code == null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_lend where code = ? ");
		params.add(code.toUpperCase().trim());
		return repo.getEntityBySQL(EquipSuggestEviction.class, sql.toString(), params);
	}

	@Override
	public List<EquipSuggestEvictionDetailVO> getListEquipSuggestEvictionDetailVOByFilter(EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		if (filter == null) {
			throw new IllegalArgumentException("filter must not be null");
		}
		if (filter.getLstId() == null || filter.getLstId().size() == 0) {
			throw new IllegalArgumentException("filter.getLstId() == null || filter.getLstId().size() == 0");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT esedt.equip_suggest_eviction_id as equipSuggestEvictionId, ");
		sql.append(" esedt.equip_suggest_eviction_dtl_id as id, ");
		sql.append(" ese.code as equipSuggestEvictionCode, ");
		sql.append(" ese.note as note, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName, ");
		sql.append(" esedt.REGION as shopCodeMien, ");
		sql.append(" esedt.CHANNEL as shopCodeKenh, ");
		sql.append(" cs.customer_id as customerId, ");
		sql.append(" cs.short_code as customerCode, ");
		sql.append(" esedt.customer_code as customerCodeLong, ");
		sql.append(" esedt.CUSTOMER_NAME as customerName, ");
		sql.append(" esedt.REPRESENTATIVE, ");
		sql.append(" esedt.RELATION, ");
		sql.append(" esedt.PHONE, ");
		sql.append(" esedt.MOBILE, ");
		sql.append(" esedt.HOUSE_NUMBER as ADDRESS, ");
		sql.append(" esedt.STREET, ");
		sql.append(" esedt.WARD_NAME as ward, ");
		sql.append(" esedt.DISTRICT_NAME as district, ");
		sql.append(" esedt.PROVINCE_NAME as province, ");
		sql.append(" ec.name as equipCategory, ");
		sql.append(" (case when eg.capacity_to is not null and eg.capacity_from is null ");
		sql.append(" then '<=' || eg.capacity_to || 'L' ");
		sql.append(" when eg.capacity_to is null and eg.capacity_from is not null ");
		sql.append(" then '>=' || eg.capacity_from || 'L' ");
		sql.append(" when eg.capacity_to = eg.capacity_from ");
		sql.append(" then eg.capacity_to || 'L' ");
		sql.append(" when eg.capacity_to is not null and eg.capacity_from is not null ");
		sql.append(" then eg.capacity_from || ' - ' || eg.capacity_to || 'L' ");
		sql.append(" else '' ");
		sql.append(" end) ");
		sql.append(" as capacity, ");
		sql.append(" ec.code as equipCategoryCode, ");
		sql.append(" ap.AP_PARAM_NAME as healthStatus, ");
		sql.append(" ap.AP_PARAM_CODE as healthStatusCode, ");
		sql.append(" TO_CHAR(esedt.SUGGEST_DATE,'dd/mm/yyyy') as timeEviction, ");
		sql.append(" esedt.SUGGEST_REASON as reason, ");
		sql.append(" esedt.EQUIP_CODE as equipCode, ");
		sql.append(" esedt.EQUIP_ID as equipId, ");
		sql.append(" esedt.SERIAL as equipSeri, ");
		sql.append(" eg.BRAND_NAME as equipBrand, ");
		sql.append(" eg.NAME as equipGroup, ");
		sql.append(" esedt.staff_name as staffName, ");
		sql.append(" esedt.staff_code as staffCode, ");
		sql.append(" st.phone as staffPhone, ");
		sql.append(" st.mobilephone as staffMobile, ");
		sql.append(" esedt.object_type stockTypeValue ");
		sql.append(" FROM equip_suggest_eviction_dtl esedt ");
		sql.append(" JOIN customer cs ");
		sql.append(" ON cs.customer_id = esedt.customer_id ");
		sql.append(" JOIN shop sh ");
		sql.append(" ON sh.shop_id = esedt.shop_id ");
		sql.append(" JOIN EQUIP_CATEGORY ec ");
		sql.append(" ON esedt.EQUIP_CATEGORY_ID = ec.EQUIP_CATEGORY_ID ");
		sql.append(" JOIN equip_suggest_eviction ese ");
		sql.append(" ON ese.equip_suggest_eviction_id = esedt.equip_suggest_eviction_id ");
		sql.append(" JOIN staff st ");
		sql.append(" ON esedt.staff_id = st.staff_id ");
		sql.append(" JOIN equipment eq ");
		sql.append(" ON eq.equip_id = esedt.equip_id ");
		sql.append(" JOIN EQUIP_GROUP eg ");
		sql.append(" ON eg.EQUIP_GROUP_ID = eq.EQUIP_GROUP_ID ");
		sql.append(" JOIN AP_PARAM ap ");
		sql.append(" ON ap.AP_PARAM_CODE = esedt.HEALTH_STATUS ");
		sql.append(" AND ap.TYPE = 'EQUIP_CONDITION' ");
		sql.append(" AND ap.status = 1 ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" and esedt.equip_suggest_eviction_id in (? ");
		params.add(filter.getLstId().get(0));
		for (int i = 1, sz = filter.getLstId().size(); i < sz; i++) {
			sql.append(",?");
			params.add(filter.getLstId().get(i));
		}
		sql.append(" ) ");
		if (!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			sql.append(" and upper(esedt.EQUIP_CODE) = ? ");
			params.add(filter.getEquipCode().toUpperCase().trim());
		}
		String[] fieldNames = { "equipSuggestEvictionId", // 1
								"id", //2
								"equipSuggestEvictionCode", //3
								"note",
								"shopCode", //4
								"shopName", //5
								"shopCodeMien", //4
								"shopCodeKenh", //5	
								"customerId", 
								"customerCode", //6
								"customerCodeLong", //7
								"customerName", //8
								"representative", //9
								"relation", //10
								"phone", //11
								"mobile", //12
								"address", //13
								"street", //14
								"ward", //16
								"district", //18
								"province", //20
								"equipCategory", //21
								"capacity", //22
								"equipCategoryCode", //23
								"healthStatus", //24
								"healthStatusCode", //25
								"timeEviction", //26
								"reason", //27
								"equipCode", //28
								"equipId", //29
								"equipSeri", //30
								"equipBrand", //31
								"equipGroup", //31
								"staffName", //32
								"staffCode", //32
								"staffPhone",// 33
								"staffMobile",// 33
								"stockTypeValue" };
		Type[] fieldTypes = { 	StandardBasicTypes.LONG, //1
								StandardBasicTypes.LONG, //2
								StandardBasicTypes.STRING, //3
								StandardBasicTypes.STRING,
								StandardBasicTypes.STRING, //4
								StandardBasicTypes.STRING, //5
								StandardBasicTypes.STRING, //4
								StandardBasicTypes.STRING, //5
								StandardBasicTypes.LONG, 
								StandardBasicTypes.STRING, //6
								StandardBasicTypes.STRING, //7
								StandardBasicTypes.STRING, //8
								StandardBasicTypes.STRING, //9
								StandardBasicTypes.STRING, //10
								StandardBasicTypes.STRING, //11
								StandardBasicTypes.STRING, //12
								StandardBasicTypes.STRING, //13
								StandardBasicTypes.STRING, //14
								StandardBasicTypes.STRING, //16
								StandardBasicTypes.STRING, //18
								StandardBasicTypes.STRING, //20
								StandardBasicTypes.STRING, //21
								StandardBasicTypes.STRING, //22
								StandardBasicTypes.STRING, //23
								StandardBasicTypes.STRING, //24
								StandardBasicTypes.STRING, //25
								StandardBasicTypes.STRING, //26
								StandardBasicTypes.STRING, //27
								StandardBasicTypes.STRING, //28
								StandardBasicTypes.LONG, //29
								StandardBasicTypes.STRING, //30
								StandardBasicTypes.STRING, //31
								StandardBasicTypes.STRING, //31
								StandardBasicTypes.STRING, //32
								StandardBasicTypes.STRING, //32
								StandardBasicTypes.STRING, //33
								StandardBasicTypes.STRING, //33
								StandardBasicTypes.INTEGER };

		sql.append(" ORDER BY ese.code, esedt.customer_code ");
		return repo.getListByQueryAndScalar(EquipSuggestEvictionDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public String getCodeEquipSuggestEviction() throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select max(code) as maxCode from equip_suggest_eviction ");
		String code = (String) repo.getObjectByQuery(sql.toString(), params);
		if (code == null) {
			return "DNTH00000001";
		}
		code = code.substring(code.length() - 8);
		code = "00000000" + (Long.parseLong(code.toString()) + 1);
		code = code.substring(code.length() - 8);
		code = "DNTH" + code;
		return code;
	}

	@Override
	public List<EquipSuggestEvictionDTL> getListEquipSuggestEvictionDetailByFilter(EquipSuggestEvictionFilter<EquipSuggestEvictionDTL> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		if (filter == null) {
			throw new IllegalArgumentException("filter must not be null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from equip_suggest_eviction_dtl eldt ");
		sql.append(" where 1=1 ");
		if (filter.getLstId() != null && filter.getLstId().size() > 0) {
			sql.append(" and eldt.equip_suggest_eviction_DTL_ID in (? ");
			params.add(filter.getLstId().get(0));
			for (int i = 1, sz = filter.getLstId().size(); i < sz; i++) {
				sql.append(",?");
				params.add(filter.getLstId().get(i));
			}
			sql.append(" ) ");
		}
		if (filter.getLstObjectId() != null && filter.getLstObjectId().size() > 0) {
			sql.append(" and eldt.equip_suggest_eviction_ID in (? ");
			params.add(filter.getLstObjectId().get(0));
			for (int i = 1, sz = filter.getLstObjectId().size(); i < sz; i++) {
				sql.append(",?");
				params.add(filter.getLstObjectId().get(i));
			}
			sql.append(" ) ");
		}
		sql.append(" ORDER BY eldt.equip_suggest_eviction_ID, eldt.customer_id ");
		return repo.getListBySQL(EquipSuggestEvictionDTL.class, sql.toString(), params);
	}

	@Override
	public List<EquipSuggestEviction> getListEquipSuggestEvictionByFilter(EquipSuggestEvictionFilter<EquipSuggestEviction> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		if (filter == null) {
			throw new IllegalArgumentException("filter must not be null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from equip_suggest_eviction el ");
		sql.append(" where 1=1 ");
		if (filter.getStatus() != null) {
			sql.append(" and el.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and el.status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getLstId() != null && !filter.getLstId().isEmpty()) {
			sql.append(" and el.equip_suggest_eviction_ID in (? ");
			params.add(filter.getLstId().get(0));
			for (int i = 1, sz = filter.getLstId().size(); i < sz; i++) {
				sql.append(",?");
				params.add(filter.getLstId().get(i));
			}
			sql.append(" ) ");
		}
		sql.append(" ORDER BY el.equip_suggest_eviction_id ");
		return repo.getListBySQL(EquipSuggestEviction.class, sql.toString(), params);
	}

}

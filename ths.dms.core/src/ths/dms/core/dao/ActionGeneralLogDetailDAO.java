package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.ActionAuditDetail;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

public interface ActionGeneralLogDetailDAO {

	ActionAuditDetail createActionGeneralLogDetail(ActionAuditDetail actionGeneralLogDetail) throws DataAccessException;

	void deleteActionGeneralLogDetail(ActionAuditDetail actionGeneralLogDetail) throws DataAccessException;

	void updateActionGeneralLogDetail(ActionAuditDetail actionGeneralLogDetail) throws DataAccessException;
	
	ActionAuditDetail getActionGeneralLogDetailById(long id) throws DataAccessException;

	List<ActionAuditDetail> getListActionGeneralLogDetail(
			KPaging<ActionAuditDetail> kPaging, Long actionGeneralLogId)
			throws DataAccessException;
}

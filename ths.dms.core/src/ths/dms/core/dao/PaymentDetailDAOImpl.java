package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.DebitDetailTemp;
import ths.dms.core.entities.PayReceivedTemp;
import ths.dms.core.entities.PaymentDetail;
import ths.dms.core.entities.PaymentDetailTemp;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.PaymentStatus;
import ths.dms.core.entities.enumtype.ReceiptType;
import ths.dms.core.entities.filter.PayReceivedFilter;
import ths.dms.core.entities.vo.PayDetailVO;
import ths.core.entities.vo.rpt.RptPayReceived1VO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class PaymentDetailDAOImpl implements PaymentDetailDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public PaymentDetail createPaymentDetail(PaymentDetail paymentDetail) throws DataAccessException {
		if (paymentDetail == null) {
			throw new IllegalArgumentException("paymentDetail");
		}
		repo.create(paymentDetail);
		return repo.getEntityById(PaymentDetail.class, paymentDetail.getId());
	}

	@Override
	public void deletePaymentDetail(PaymentDetail paymentDetail) throws DataAccessException {
		if (paymentDetail == null) {
			throw new IllegalArgumentException("paymentDetail");
		}
		repo.delete(paymentDetail);
	}

	@Override
	public PaymentDetail getPaymentDetailById(long id) throws DataAccessException {
		return repo.getEntityById(PaymentDetail.class, id);
	}
	
	@Override
	public void updatePaymentDetail(PaymentDetail paymentDetail) throws DataAccessException {
		if (paymentDetail == null) {
			throw new IllegalArgumentException("paymentDetail");
		}
		paymentDetail.setUpdateDate(commonDAO.getSysDate());
		repo.update(paymentDetail);
	}
	
	@Override
	public void createListPaymentDetail(List<PaymentDetail> lstPaymentDetail) throws DataAccessException {
		if (lstPaymentDetail == null) {
			throw new IllegalArgumentException("paymentDetail");
		}
		repo.create(lstPaymentDetail);
	}
	
	@Override
	public List<RptPayReceived1VO> getListRptPayReceived1VO (
			Long shopId, String shortCode, Date fromDate, Date toDate) throws DataAccessException {
		
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptPayReceived1VO>();
		
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		if (toDate == null) {
			throw new IllegalArgumentException("toDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT c.short_code      AS shortCode,");
		sql.append("   c.customer_name        AS customerName,");
		sql.append("   pr.pay_received_number AS payReceivedNumber,");
		sql.append("   dd.pay_date            AS payDate,");
		sql.append("   d.amount               AS amount");
		sql.append(" FROM payment_detail dd,");
		sql.append("   debit_detail d,");
		sql.append("   debit dg,");
		sql.append("   pay_received pr,");
		sql.append("   sale_order so,");
		sql.append("   customer c");
		sql.append(" WHERE dd.debit_detail_id         = d.debit_detail_id");
		sql.append(" AND dg.debit_id   = d.debit_id");
		sql.append(" AND dd.pay_received_id    = pr.pay_received_id");
		sql.append(" AND d.from_object_id      = so.sale_order_id");
		sql.append(" AND dg.object_id           = c.customer_id");
		sql.append(" AND dg.object_type         = ?");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		sql.append(" AND pr.receipt_type       = ?");
		params.add(ReceiptType.RECEIVED.getValue());
		sql.append(" AND pr.create_date        >= TRUNC(?)");
		params.add(fromDate);
		sql.append(" AND pr.create_date        < TRUNC(?) + 1");
		params.add(toDate);
		sql.append(" AND c.shop_id             = ?");
		params.add(shopId);
		if (!StringUtility.isNullOrEmpty(shortCode)) {
			shortCode = shortCode.toUpperCase();
			sql.append(" AND UPPER(c.short_code)   = ?");
			params.add(shortCode);
		}
		sql.append(" ORDER BY c.customer_code,");
		sql.append("   dd.pay_date");
		
		String[] fieldNames = { "shortCode", // 1
				"customerName", // 2
				"payReceivedNumber",// 3
				"payDate",// 4
				"amount"// 5
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.DATE,// 4
				StandardBasicTypes.BIG_DECIMAL// 5
		};
		return repo.getListByQueryAndScalar(RptPayReceived1VO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	public Integer getTimeOfPay(Long debitDetailId) throws DataAccessException {
		if (debitDetailId == null) {
			throw new IllegalArgumentException("invalid id");
		}
		List<Object> params = new ArrayList<Object>();
		String sql = "select count(1) as count from payment_detail where debit_detail_id = ?";
		params.add(debitDetailId);
		
		return repo.countBySQL(sql, params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public void createListPaymentDetailAndCloneToTemp(List<PaymentDetail> lstPaymentDetail, PayReceivedTemp payTemp) throws DataAccessException {
		if (lstPaymentDetail == null) {
			return;
		}
		lstPaymentDetail = repo.create(lstPaymentDetail);
		
		cloneListPaymentDetailToTemp(lstPaymentDetail, payTemp);
	}
	
	@Override
	public void cloneListPaymentDetailToTemp(List<PaymentDetail> lstPaymentDetail, PayReceivedTemp payTemp) throws DataAccessException {
		Date sysdate = commonDAO.getSysDate();
		
		StringBuilder sql = new StringBuilder();
		sql.append("select * from debit_detail_temp where type in (4, 5) and from_object_id = ?");
		List<Object> params = null;
		
		PaymentDetailTemp pdTmp = null;
		DebitDetailTemp ddTmp = null;
		for (PaymentDetail pd : lstPaymentDetail) {
			pdTmp = new PaymentDetailTemp();
			pdTmp.setPayReceived(payTemp);
			pdTmp.setAmount(pd.getAmount());
			pdTmp.setPaymentType(pd.getPaymentType());
			pdTmp.setStatus(pd.getStatus());
			pdTmp.setPayDate(pd.getPayDate());
			pdTmp.setCreateUser(pd.getCreateUser());
			pdTmp.setCreateDate(pd.getCreateDate());
			pdTmp.setCustomer(pd.getCustomer());
			pdTmp.setShop(pd.getShop());
			pdTmp.setPaymentStatus(pd.getPaymentStatus());
			if (pd.getDebitDetail() != null && pd.getDebitDetail().getFromObjectId() != null) {
				pdTmp.setForObjectId(pd.getDebitDetail().getFromObjectId());
			}
			pdTmp.setDiscount(pd.getDiscount());
			pdTmp.setTime(pd.getTime());
			pdTmp.setStaff(pd.getStaff());
			
			repo.create(pdTmp);
			
			// cap nhat debit_detail_temp
			if (pd.getDebitDetail() != null && pd.getDebitDetail().getFromObjectId() != null) {
				params = new ArrayList<Object>();
				params.add(pd.getDebitDetail().getFromObjectId());
				
				ddTmp = repo.getEntityBySQL(DebitDetailTemp.class, sql.toString(), params);
				
				if (ddTmp != null) {
					ddTmp.setUpdateDate(sysdate);
					ddTmp.setUpdateUser(payTemp.getCreateUser());
					if (ddTmp.getRemain() == null) {
						ddTmp.setRemain(BigDecimal.ZERO);
					}
					ddTmp.setRemain(ddTmp.getRemain().subtract(pd.getAmount()));
					if (ddTmp.getTotalPay() == null) {
						ddTmp.setTotalPay(BigDecimal.ZERO);
					}
					ddTmp.setTotalPay(ddTmp.getTotalPay().add(pd.getAmount()));
					if (pd.getDiscount() != null) {
						ddTmp.setRemain(ddTmp.getRemain().subtract(pd.getDiscount()));
						if (ddTmp.getDiscountAmount() == null) {
							ddTmp.setDiscountAmount(BigDecimal.ZERO);
						}
						ddTmp.setDiscountAmount(ddTmp.getDiscountAmount().add(pd.getDiscount()));
					}
					repo.update(ddTmp);
				}
			}
		}
	}

	@Override
	public List<PayDetailVO> getListPaymentDetailVO(PayReceivedFilter<PayDetailVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		if(filter.getPayId() == null){
			throw new IllegalArgumentException("getPayId is null");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select pd.payment_detail_id as paymentDetailId, ");
		sql.append(" cs.short_code as customerCode, ");
		sql.append(" dd.from_object_number as fromObjectNumber, ");
		sql.append(" so.SALE_ORDER_ID as orderId, ");
		sql.append(" to_char(pd.create_date, 'dd/mm/yyyy') as createDateStr, ");
		sql.append(" (select staff_code||' - '||staff_name from staff where staff_id = so.delivery_id) AS deliveryCode, ");
		sql.append(" (select staff_code from staff where staff_id = so.cashier_id) AS cashierCode, ");
		sql.append(" (select staff_name from staff where staff_id = so.cashier_id) AS cashierName, ");
		sql.append(" (select staff_code||' - '||staff_name from staff where staff_id = so.staff_id) AS staffCode, ");
		sql.append(" nvl(dd.total, 0) as total, ");
		sql.append(" nvl(pd.discount, 0) as discount, ");
		sql.append(" nvl(pd.amount, 0) as amount, ");
		sql.append(" (select bank_name from bank where bank_id = pr.bank_id) as bankName ");
		sql.append(" from payment_detail pd ");
		//sql.append(" join customer cs on cs.customer_id = pd.customer_id and cs.status = ? ");
		//params.add(ActiveType.RUNNING.getValue());
		sql.append(" join customer cs on cs.customer_id = pd.customer_id");
		sql.append(" join debit_detail dd on dd.debit_detail_id = pd.debit_detail_id ");
		sql.append(" join pay_received pr on pd.pay_received_id = pr.pay_received_id ");
		sql.append(" left join sale_order so ON dd.from_object_id = so.sale_order_id ");
		sql.append(" where 1=1 ");
		sql.append(" and pr.pay_received_id = ? ");
		params.add(filter.getPayId());
		if(filter.getShopId() != null){
			sql.append(" and pd.shop_id = ? ");
			params.add(filter.getShopId());
		}
		sql.append(" and pd.status = ? ");
		params.add(ActiveType.RUNNING.getValue());		
		if (filter.getLstId() != null && !filter.getLstId().isEmpty()) {
			sql.append(" and pd.payment_detail_id in (-1 ");
			for (int i = 0, sz = filter.getLstId().size(); i < sz; i++) {
				sql.append(",?");
				params.add(filter.getLstId().get(i));
			}
			sql.append(" ) ");
		}
		
		String[] fieldNames = { 
				"paymentDetailId", // 1
				"customerCode", // 2
				"fromObjectNumber",// 3
				"orderId",// 3
				"createDateStr",// 4
				"deliveryCode",// 5
				"cashierCode",// 6
				"cashierName",// 6
				"staffCode",// 7
				"total",// 8
				"discount",// 9
				"amount",// 10
				"bankName",// 11
		};
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.LONG,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.BIG_DECIMAL,// 8
				StandardBasicTypes.BIG_DECIMAL,// 9
				StandardBasicTypes.BIG_DECIMAL,// 10
				StandardBasicTypes.STRING// 11
		};
		
		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(1) as count from ( ").append(sql).append(" ) ");
		
		sql.append(" order by customerCode, fromObjectNumber ");
		if(filter.getPaging() == null){			
			return repo.getListByQueryAndScalar(PayDetailVO.class,fieldNames, fieldTypes, sql.toString(), params);
		}else{
			return repo.getListByQueryAndScalarPaginated(PayDetailVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getPaging());
		}
	}

	@Override
	public List<PaymentDetail> getListPaymentDetailByIdPayReceive(Long payReceiveId) throws DataAccessException {
		// TODO Auto-generated method stub
		if(payReceiveId == null){
			throw new IllegalArgumentException("payReceiveId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select * ");
		sql.append(" from payment_detail pd ");
		sql.append(" where pd.pay_received_id = ? ");
		params.add(payReceiveId);
		sql.append(" and pd.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		
		return repo.getListBySQL(PaymentDetail.class, sql.toString(), params);
	}

	@Override
	public List<PaymentDetailTemp> getListPaymentDetailTempByIdPayReceive(Long payReceiveTempId) throws DataAccessException {
		// TODO Auto-generated method stub
		if(payReceiveTempId == null){
			throw new IllegalArgumentException("payReceiveTempId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select * ");
		sql.append(" from payment_detail_temp pd ");
		sql.append(" where pd.pay_received_id = ? ");
		params.add(payReceiveTempId);
		sql.append(" and pd.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		
		return repo.getListBySQL(PaymentDetailTemp.class, sql.toString(), params);
	}

	@Override
	public void updatePaymentDetailTemp(PaymentDetailTemp paymentDetailTemp) throws DataAccessException {
		// TODO Auto-generated method stub
		if (paymentDetailTemp == null) {
			throw new IllegalArgumentException("paymentDetailTemp");
		}
		paymentDetailTemp.setUpdateDate(commonDAO.getSysDate());
		repo.update(paymentDetailTemp);
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public BigDecimal getPaymentAmountOfDebitDetail(long debitDetailId, PaymentStatus paymentStatus) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select sum(nvl(pd.amount, 0) + nvl(discount, 0)) as amount");
		sql.append(" from payment_detail pd");
		sql.append(" join pay_received pr on (pr.pay_received_id = pd.pay_received_id)");
		sql.append(" where pd.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and pd.debit_detail_id = ?");
		params.add(debitDetailId);
		
		sql.append(" and pr.receipt_type not in (?, ?)");
		params.add(ReceiptType.RECEIVED_REVERSE.getValue());
		params.add(ReceiptType.PAID_REVERSE.getValue());
		
		if (paymentStatus == null) {
			sql.append(" and pd.payment_status in (?, ?)");
			params.add(PaymentStatus.NOT_PAID_YET.getValue());
			params.add(PaymentStatus.PAID.getValue());
		} else {
			sql.append(" and pd.payment_status in (?)");
			params.add(paymentStatus.getValue());
		}
		
		Object obj = repo.getObjectByQuery(sql.toString(), params);
		
		if (obj != null) {
			if (obj instanceof BigDecimal) {
				return (BigDecimal)obj;
			}
			return new BigDecimal(obj.toString());
		}
		
		return BigDecimal.ZERO;
	}

}

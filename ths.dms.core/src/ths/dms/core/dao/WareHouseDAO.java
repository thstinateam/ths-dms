package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface WareHouseDAO {
	/**
	 * Lay danh sach kho theo dieu kien
	 * @author tientv11
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<Warehouse> getListWarehouseByFilter(StockTotalFilter filter) throws DataAccessException;
	
	/**
	 * Lay kho cua NPP theo ma
	 * @author tientv11
	 * @param code
	 * @return
	 * @throws BusinessException
	 */
	Warehouse getWarehouseByCode (String code,Long shopId) throws DataAccessException;

	Warehouse getWarehouseById(Long id) throws DataAccessException;
	/**
	 * Lay danh sach kho kiem ke thiet bi theo dieu kien 
	 * @author phuongvm
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<Warehouse> getListWarehouseByFilterChecking(StockTotalFilter filter) throws DataAccessException;
}

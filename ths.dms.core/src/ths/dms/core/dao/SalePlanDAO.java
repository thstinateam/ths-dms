package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.SalePlan;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PlanType;
import ths.dms.core.entities.enumtype.SalePlanOwnerType;
import ths.dms.core.entities.vo.DistributeSalePlanVO;
import ths.dms.core.entities.vo.SalePlanVO;
import ths.dms.core.entities.vo.SumSalePlanVO;
import ths.core.entities.vo.rpt.RptRealTotalSalePlanVO;
import ths.dms.core.exceptions.DataAccessException;


public interface SalePlanDAO {
	
	SalePlan createSalePlan(SalePlan salePlan) throws DataAccessException;
	
	void createSalePlan(List<SalePlan> listSalePlan) throws DataAccessException;

	void deleteSalePlan(SalePlan salePlan) throws DataAccessException;

	void updateSalePlan(SalePlan salePlan) throws DataAccessException;
	
	SalePlan getSalePlanById(long id) throws DataAccessException;
	
	List<DistributeSalePlanVO> getListSalePlanWithCondition(Long shopId, Long ownerId, String productCode,
			Integer month, Integer year, List<String> lstCateCode, PlanType type, KPaging<DistributeSalePlanVO> kPaging) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param id
	 * @throws DataAccessException
	 */
	void deleteSalePlanById(long id) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param ownerId
	 * @param productId
	 * @param shopType
	 * @param planType
	 * @param month
	 * @param year
	 * @return
	 * @throws DataAccessException
	 */
	SalePlan getSalePlanOfVNM(Long ownerId, Long productId, SalePlanOwnerType ownerType,
			PlanType planType, Integer month, Integer year) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param ownerId
	 * @param productId
	 * @param shopType
	 * @param planType
	 * @param month
	 * @param year
	 * @return
	 * @throws DataAccessException
	 */
	SumSalePlanVO getSumSalePlan(Long ownerId, Long productId,
			SalePlanOwnerType shopType, PlanType planType, Integer month, Integer year)
			throws DataAccessException;
	
	List<RptRealTotalSalePlanVO> getListRptRealTotalSalePlan(Long shopId,
			Integer month, Integer year, String staffCode) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param kPaging
	 * @param shopId
	 * @param staffCode
	 * @param catCode
	 * @param productCode
	 * @param num
	 * @param year
	 * @return
	 * @throws DataAccessException
	 */
	List<SalePlanVO> getListSalePlanVOProductOfShop(
			KPaging<SalePlanVO> kPaging, Long shopId, String staffCode,
			String catCode, String productCode,String productName, Integer num, Integer year, Integer fromQty, Integer toQty)
			throws DataAccessException;
	/**
	 * lay danh sach sku them vao o phan phan bo chi tieu thang
	 * @author thanhnguyen
	 * @param kPaging
	 * @param shopId - id cua don vi dang phan bo
	 * @param productCode
	 * @param productName
	 * @param month
	 * @param year
	 * @param fromQty
	 * @param toQty
	 * @return
	 * @throws DataAccessException
	 */
	List<SalePlanVO> getListSalePlanVOProductOfParentShop(
			KPaging<SalePlanVO> kPaging, Long shopId, String productCode,
			String productName, Integer month, Integer year, Integer fromQty,
			Integer toQty) throws DataAccessException;
//	/**
//	 * lay danh sach sku them vao o phan phan bo chi tieu thang - phan ko dua vao parent - new
//	 * @author thanhnguyen
//	 * @param kPaging
//	 * @param shopId
//	 * @param productCode
//	 * @param productName
//	 * @param month
//	 * @param year
//	 * @param fromQty
//	 * @param toQty
//	 * @return
//	 * @throws DataAccessException
//	 */
//	List<SalePlanVO> getListSalePlanVOProductNew(
//			KPaging<SalePlanVO> kPaging, Long shopId, String productCode,
//			String productName, Integer month, Integer year, Integer fromQty,
//			Integer toQty) throws DataAccessException;

	/**
	*  lay sale plan theo dieu kien
	*  @author: thanhnn8
	*  @param shopId
	*  @param catCode
	*  @param month
	*  @param year
	*  @param type
	*  @return
	*  @return: SalePlan
	*  @throws:
	*/
	SalePlan getSalePlanByCondition(Long ownerId, String catCode, Integer month,
			Integer year, PlanType type) throws DataAccessException;

	/**
	 * @author HUNGNM
	 * @param objectCode
	 * @param objectType
	 * @param month
	 * @param year
	 * @return
	 * @throws DataAccessException
	 */
	Boolean checkSalePlan(String objectCode, SalePlanOwnerType objectType,
			String productCode, Integer month, Integer year)
			throws DataAccessException;

	List<DistributeSalePlanVO> getListSalePlanWithNoCondition(
			KPaging<DistributeSalePlanVO> kPaging,String productCode,String productName) throws DataAccessException;

	List<SalePlanVO> getListSalePlanVO(Long shopId, String staffCode,
			List<String> lstCatCode, String productCode, Integer month,
			Integer year, Long gsnppId) throws DataAccessException;


	List<SalePlan> getListSalePlan(KPaging<SalePlan> paging, String staffCode,
			List<String> lstCatCode, String productCode, Integer month,
			Integer year) throws DataAccessException;

	SalePlan getSalePlan(String staffCode, String productCode, Integer num,
			Integer year) throws DataAccessException;

	List<SalePlanVO> getListSalePlanVOForLoadPage(KPaging<SalePlanVO> paging,
			Long shopId, Integer month, Integer year)
			throws DataAccessException;

	List<SalePlanVO> getListSalePlanVO4KT(Long shopId, String staffCode,
			List<String> lstCatCode, String productCode, Integer month,
			Integer year) throws DataAccessException;
	
	/**
	 * 	 * Lay version xuat file xml PO Auto
	 * @author tungtt21
	 * @param ownerId
	 * @param productId
	 * @param ownerType
	 * @param planType
	 * @param month
	 * @param year
	 * @return
	 * @throws DataAccessException
	 */
	Integer getVersion(Long ownerId, Long productId, SalePlanOwnerType ownerType,
			PlanType planType, Integer month, Integer year) throws DataAccessException;
}

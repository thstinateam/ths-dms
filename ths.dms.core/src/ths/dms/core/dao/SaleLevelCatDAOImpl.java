package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SaleLevelCat;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.entities.vo.SaleCatLevelVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class SaleLevelCatDAOImpl implements SaleLevelCatDAO {

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public SaleLevelCat createSaleLevelCat(SaleLevelCat saleLevelCat, LogInfoVO logInfo) throws DataAccessException {
		if (saleLevelCat == null) {
			throw new IllegalArgumentException("saleLevelCat");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (saleLevelCat.getCat() != null) {
			if (checkIfRecordExist(saleLevelCat.getCat().getId(), saleLevelCat.getSaleLevelCode(), null))
				return null;
		} else
			return null;

		saleLevelCat.setCreateUser(logInfo.getStaffCode());
		repo.create(saleLevelCat);

		try {
			actionGeneralLogDAO.createActionGeneralLog(null, saleLevelCat, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(SaleLevelCat.class, saleLevelCat.getId());
	}

	@Override
	public void deleteSaleLevelCat(SaleLevelCat saleLevelCat, LogInfoVO logInfo) throws DataAccessException {
		if (saleLevelCat == null) {
			throw new IllegalArgumentException("saleLevelCat");
		}
		repo.delete(saleLevelCat);
		try {
			actionGeneralLogDAO.createActionGeneralLog(saleLevelCat, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public SaleLevelCat getSaleLevelCatById(long id) throws DataAccessException {
		return repo.getEntityById(SaleLevelCat.class, id);
	}

	@Override
	public void updateSaleLevelCat(SaleLevelCat saleLevelCat, LogInfoVO logInfo) throws DataAccessException {
		if (saleLevelCat == null) {
			throw new IllegalArgumentException("saleLevelCat");
		}
		if (logInfo != null) {
			saleLevelCat.setUpdateDate(commonDAO.getSysDate());
			saleLevelCat.setUpdateUser(logInfo.getStaffCode());
		}

		if (saleLevelCat.getCat() != null) {
			if (checkIfRecordExist(saleLevelCat.getCat().getId(), saleLevelCat.getSaleLevelCode(), saleLevelCat.getId()))
				return;
		} else
			return;
		SaleLevelCat temp = this.getSaleLevelCatById(saleLevelCat.getId());
		repo.update(saleLevelCat);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, saleLevelCat, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Boolean checkIfRecordExist(long catId, String saleLevel, Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(1) as count from sale_level_cat p ");
		sql.append(" where cat_id=? and sale_level_code = ? and status != ?");

		List<Object> params = new ArrayList<Object>();
		params.add(catId);
		params.add(saleLevel.trim().toUpperCase());
		params.add(ActiveType.DELETED.getValue());

		if (id != null) {
			sql.append(" and sale_level_cat_id != ?");
			params.add(id);
		}

		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public List<SaleLevelCat> getListSaleLevelCat(KPaging<SaleLevelCat> kPaging, Long catId, String catName, String saleLevel, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select sc.* from sale_level_cat sc join product_info s on s.product_info_id=sc.cat_id where 1 = 1");
		if (!StringUtility.isNullOrEmpty(catName)) {
			sql.append("	and unicode2english(s.product_info_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(catName).trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(saleLevel)) {
			sql.append(" and sc.sale_level_code = ? ");
			params.add(saleLevel.toUpperCase());
		}
		if (catId != null) {
			sql.append(" and sc.cat_id=?");
			params.add(catId);
		}
		if (status != null) {
			sql.append(" and sc.status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and sc.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" order by sc.sale_level_name, sc.sale_level_cat_id ");
		if (kPaging == null)
			return repo.getListBySQL(SaleLevelCat.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(SaleLevelCat.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<String> getListSaleLevel(Long catId, String catName, String saleLevel, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select distinct sc.sale_level_code as productCode from sale_level_cat sc join product_info s on s.product_info_id=sc.cat_id where 1 = 1");
		if (!StringUtility.isNullOrEmpty(catName)) {
			sql.append("	and unicode2english(s.product_info_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(catName).trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(saleLevel)) {
			sql.append(" and sc.sale_level_code = ? ");
			params.add(saleLevel.trim().toUpperCase());
		}
		if (catId != null) {
			sql.append(" and sc.cat_id=?");
			params.add(catId);
		}
		if (status != null) {
			sql.append(" and sc.status=?");
			params.add(status.getValue());
		}
		sql.append(" order by sc.sale_level_code");

		String[] fieldNames = { "productCode" };
		Type[] fieldTypes = { StandardBasicTypes.STRING };
		List<ProductVO> lst = repo.getListByQueryAndScalar(ProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
		List<String> rs = new ArrayList<String>();
		for (ProductVO vo : lst) {
			rs.add(vo.getProductCode());
		}
		return rs;
	}

	@Override
	public SaleLevelCat getSaleLevelCatByCatAndSaleLevel(Long catId, String saleLevel) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from sale_level_cat sc where 1 = 1");
		if (catId != null) {
			sql.append(" and cat_id=?");
			params.add(catId);
		}
		if (!StringUtility.isNullOrEmpty(saleLevel)) {
			sql.append(" and sale_level_code = ? ");
			params.add(saleLevel.trim().toUpperCase());
		}

		return repo.getEntityBySQL(SaleLevelCat.class, sql.toString(), params);
	}

	@Override
	public List<SaleLevelCat> getListSaleLevelCatByProductInfo(KPaging<SaleLevelCat> kPaging, List<String> listProInfoCode, String saleLevelCode, String saleLevelName, BigDecimal fromAmount, BigDecimal toAmount, ActiveType status
			,String sort,String order)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("SELECT sc.* FROM sale_level_cat sc join product_info s on s.product_info_id = sc.cat_id where 1 = 1 and s.status = 1 ");

		if (null != listProInfoCode && listProInfoCode.size() > 0) {
			sql.append(" and s.product_info_code in (");
			for (int i = 0; i < listProInfoCode.size(); i++) {
				if (i < listProInfoCode.size() - 1) {
					sql.append("?, ");
				} else {
					sql.append(" ?) ");
				}
				params.add(listProInfoCode.get(i));
			}
		}

		if (!StringUtility.isNullOrEmpty(saleLevelName)) {
			sql.append("	and unicode2english(sc.sale_level_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(saleLevelName).trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(saleLevelCode)) {
			sql.append("    and sc.sale_level_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(saleLevelCode.toUpperCase()));
		}
		if (status != null) {
			sql.append(" and sc.status=?");
			params.add(status.getValue());
		}
		if (fromAmount != null) {
			sql.append(" and sc.from_amount > ?");
			params.add(fromAmount.subtract(BigDecimal.ONE) );
		}
		if (toAmount != null) {
			sql.append(" and sc.to_amount<?");
			params.add(toAmount.add(BigDecimal.ONE));
		}

		sql.append(" and s.type = 1");
		String[] arrColumnAction = new String[] { "s.product_info_code", "sc.sale_level_code",
				"sc.sale_level_name", "nvl(sc.from_amount,0)", "nvl(sc.to_amount,0)", "sc.status" };
		sort = StringUtility.getColumnSort(sort, arrColumnAction);
		if (!StringUtility.isNullOrEmpty(sort)) {
			sql.append(" order by ").append(sort).append(" ");
			order = StringUtility.getOrderBy(order);
			if (!StringUtility.isNullOrEmpty(order)) {
				sql.append(order);
			}
		} else {
			sql.append(" order by s.product_info_code, sc.sale_level_code, sc.sale_level_name, sc.sale_level_cat_id ");
		}
		if (kPaging == null) {
			return repo.getListBySQL(SaleLevelCat.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(SaleLevelCat.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<SaleCatLevelVO> getListSaleCatLevelVOByIdPro(Long id) throws DataAccessException {
		// TODO Auto-generated method stub
		if (id == null) {
			throw new DataAccessException("idProductInfo not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT SC.SALE_LEVEL_CAT_ID as idSaleCatLevel, ");
		sql.append(" SC.SALE_LEVEL_CODE as codeSaleCatLevel, ");
		sql.append(" SC.SALE_LEVEL_NAME as nameSaleCatLevel ");
		sql.append(" FROM SALE_LEVEL_CAT SC ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND SC.CAT_ID=? ");
		params.add(id);
		sql.append(" AND SC.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ORDER BY codeSaleCatLevel ");

		String[] fieldNames = { "idSaleCatLevel", // 0
				"codeSaleCatLevel", // 1
				"nameSaleCatLevel" // 2
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING // 2
		};

		return repo.getListByQueryAndScalar(SaleCatLevelVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<SaleCatLevelVO> getListSaleCatLevelVOByIdProAlreadySet(KPaging<SaleCatLevelVO> kPaging, long promotionProgramId) throws DataAccessException {
		// TODO Auto-generated method stub

		StringBuilder sql = new StringBuilder();
		StringBuilder countsql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT CAT.SALE_LEVEL_CAT_ID idSaleCatLevel, ");
		sql.append(" CAT.SALE_LEVEL_CODE codeSaleCatLevel, ");
		sql.append(" CAT.SALE_LEVEL_NAME nameSaleCatLevel ");
		sql.append(" FROM SALE_LEVEL_CAT cat ");
		sql.append(" WHERE CAT.SALE_LEVEL_CAT_ID IN ( ");
		sql.append(" SELECT PCADT.OBJECT_ID FROM MT_PROMOTION_CUST_ATTR pca,MT_PROMOTION_CUST_ATTR_DETAIL pcadt ");
		sql.append(" WHERE PCA.PROMOTION_CUST_ATTR_ID=PCADT.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND PCA.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND pcadt.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND PCA.PROMOTION_PROGRAM_ID=? ");
		params.add(promotionProgramId);
		sql.append(" AND PCA.OBJECT_TYPE=3) ");

		String[] fieldNames = { "idSaleCatLevel", // 0
				"codeSaleCatLevel", // 1
				"nameSaleCatLevel" // 2
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING // 2
		};

		countsql.append(" select count(*) count from ( ").append(sql).append(" ) ");
		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(SaleCatLevelVO.class, fieldNames, fieldTypes, sql.toString(), countsql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(SaleCatLevelVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<SaleCatLevelVO> getListSaleCatLevelVOByIdProAlreadySetSO(KPaging<SaleCatLevelVO> kPaging, long promotionProgramId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		StringBuilder countsql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT CAT.SALE_LEVEL_CAT_ID idSaleCatLevel, ");
		sql.append(" CAT.SALE_LEVEL_CODE codeSaleCatLevel, ");
		sql.append(" CAT.SALE_LEVEL_NAME nameSaleCatLevel ");
		sql.append(" FROM SALE_LEVEL_CAT cat ");
		sql.append(" WHERE CAT.SALE_LEVEL_CAT_ID IN ( ");
		sql.append(" SELECT PCADT.OBJECT_ID FROM PROMOTION_CUST_ATTR pca,PROMOTION_CUST_ATTR_DETAIL pcadt ");
		sql.append(" WHERE PCA.PROMOTION_CUST_ATTR_ID=PCADT.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND PCA.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND pcadt.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND PCA.PROMOTION_PROGRAM_ID=? ");
		params.add(promotionProgramId);
		sql.append(" AND PCA.OBJECT_TYPE=3) ");

		String[] fieldNames = { "idSaleCatLevel", // 0
				"codeSaleCatLevel", // 1
				"nameSaleCatLevel" // 2
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING // 2
		};

		countsql.append(" select count(*) count from ( ").append(sql).append(" ) ");
		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(SaleCatLevelVO.class, fieldNames, fieldTypes, sql.toString(), countsql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(SaleCatLevelVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
}

package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Routing;
import ths.dms.core.entities.VisitPlan;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.RoutingGeneralFilter;
import ths.dms.core.entities.filter.SupFilter;
import ths.dms.core.entities.filter.VisitPlanFilter;
import ths.dms.core.entities.vo.RoutingTreeVO;
import ths.dms.core.entities.vo.RoutingVO;
import ths.dms.core.exceptions.DataAccessException;

public interface VisitPlanDAO {

	VisitPlan createVisitPlan(VisitPlan visitPlan) throws DataAccessException;

	void deleteVisitPlan(VisitPlan visitPlan) throws DataAccessException;

	void updateVisitPlan(VisitPlan visitPlan) throws DataAccessException;
	
	VisitPlan getVisitPlanById(Long id) throws DataAccessException;
	
	List<RoutingVO> getListVisitPlanByShopId(KPaging<RoutingVO> kPaging,
			Long shopId, Long ownerId, ActiveType status, String customerCode,
			String staffCode, String routingCode, String routingName)
			throws DataAccessException;
	
	List<VisitPlan> getListVisitPlanByRoutingId(SupFilter<VisitPlan> filter) throws DataAccessException;
	VisitPlan getVisitPlan(Long routingId, Date fromDate, Date toDate) throws DataAccessException;
	
	Boolean checkExitVisitPlan(Long routingId, Long visitPlanId, Long staffId, Date fromDate, Date toDate) throws DataAccessException;
	
	List<VisitPlan> getListVisitPlanForManagerPlan(KPaging<VisitPlan> kPaging,
			Long shopId, Long staffId, Date date, ActiveType type)
			throws DataAccessException;

	void updateVisitPlanStatusToStop(Long staffId) throws DataAccessException;
	
	void offVisitPlanWithRoutingId(Long routingId, String updateUser, Date updateDate) throws DataAccessException;
	
	Boolean checkVisitPlanWithSP(Long routingId, Date fromDate, Date toDate) throws DataAccessException;

	VisitPlan getVisitPlanMultiChoce(VisitPlanFilter filter) throws DataAccessException;

	List<VisitPlan> getListVisitPlanByFuture(Long staffId, Long routingId, Long shopId, Date sysdate, Integer flag) throws DataAccessException;

	VisitPlan getListVisitPlanByCurrent(Long staffId, Long routingId, Long shopId, Date sysdate, Integer flag) throws DataAccessException;

	List<VisitPlan> getListVisitPlanErrByStartDate(Long staffId, Long routingId, Date fDate, Long shopId) throws DataAccessException;

	List<VisitPlan> getListVisitPlanInCurrent(Long staffId, Long routingId, Long shopId, Date fDate) throws DataAccessException;

	VisitPlan getVisitPlanWitFDateMin(Long shopId) throws DataAccessException;

	Boolean checkRoutingErrDelete(Long routingId, Long shopId) throws DataAccessException;

	List<VisitPlan> getListVisitPlanByStaffId(Long staffId, Long shopId, int flagTime) throws DataAccessException;

	VisitPlan getVisitPlanByFullId(Long staffId, Long routingId, Long shopId) throws DataAccessException;

	List<VisitPlan> getListVisitPlanByRoutingIDAndFlag(Long routingId, Long shopId, int flagTime) throws DataAccessException;

	List<Routing> getListRoutingShopId(Long shopId) throws DataAccessException;

	List<RoutingVO> exportTuyenBySelect(Long shopId, String routingCode, String routingName, String staffCode, Date sysdate, List<Long> lstRoutingId) throws DataAccessException;

	List<VisitPlan> checkExitVisitPlanInShop(Long shopId, Long routingId, Long visitPlanId, Long staffId, Date fromDate, Date toDate) throws DataAccessException;

	List<VisitPlan> getListVisitPlanByImport(VisitPlan visitPlan, Boolean flag) throws DataAccessException;

	List<VisitPlan> getListVisitPlanByForm(VisitPlan visitPlan, Boolean flag) throws DataAccessException;

	List<RoutingTreeVO> getListRoutingShopIdVO(Long shopId) throws DataAccessException;

	/**
	 * Tim kiem tuyen
	 * @author hunglm16
	 * @description October 6,2014
	 * */
	List<RoutingVO> searchListRoutingVOByFilter(RoutingGeneralFilter<RoutingVO> filter) throws DataAccessException;

	/**
	 * Xoa tuyen du thua
	 * @author hoanv25
	 * @since August 14,2015
	 * @description 
	 * */
	void deleteVisitPlanStatusToStop(Long id) throws DataAccessException;

}

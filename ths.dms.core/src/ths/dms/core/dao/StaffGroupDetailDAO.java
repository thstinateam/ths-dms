package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffGroupDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface StaffGroupDetailDAO {

	StaffGroupDetail createStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws DataAccessException;

	void deleteStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws DataAccessException;

	void updateStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws DataAccessException;
	
	StaffGroupDetail getStaffGroupDetailById(long id) throws DataAccessException;

	List<StaffGroupDetail> getListStaffGroupDetail(KPaging<StaffGroupDetail> kPaging,
			Long staffGroupId, ActiveType activeType) throws DataAccessException;

	List<Staff> getListStaffInStaffGroup(KPaging<Staff> paging,
			Long staffGroupId, ActiveType activeType)
			throws DataAccessException;

	List<StaffGroupDetail> getListStaffGroupDetailByStaff(Long staffId)
			throws DataAccessException;
}

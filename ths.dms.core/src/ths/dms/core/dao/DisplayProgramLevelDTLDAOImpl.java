package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StDisplayPlAmtDtl;
import ths.dms.core.entities.StDisplayPlDpDtl;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.DisplayProgrameExVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class DisplayProgramLevelDTLDAOImpl implements DisplayProgramLevelDTLDAO {

	@Autowired
	private IRepository repo;
	
//	@Autowired
//	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Autowired
	private CommonDAO commonDAO;

	@Override
	public StDisplayPlAmtDtl createDisplayProgramLevelDTL(StDisplayPlAmtDtl displayProgramLevelDTL, LogInfoVO logInfo) throws DataAccessException {
		if (displayProgramLevelDTL == null) {
			throw new IllegalArgumentException("displayProgramLevelDTL is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.create(displayProgramLevelDTL);
//		try {
//			actionGeneralLogDAO.createActionGeneralLog(null, displayProgramLevelDTL, logInfo);
//		}
//		catch (Exception ex) {
//			throw new DataAccessException(ex);
//		}
		return repo.getEntityById(StDisplayPlAmtDtl.class, displayProgramLevelDTL.getId());
	}

	@Override
	public void deleteDisplayProgramLevelDTL(StDisplayPlAmtDtl displayProgramLevelDTL, LogInfoVO logInfo) throws DataAccessException {
		if (displayProgramLevelDTL == null) {
			throw new IllegalArgumentException("displayProgramLevelDTL");
		}
		repo.delete(displayProgramLevelDTL);
//		try {
//			actionGeneralLogDAO.createActionGeneralLog(displayProgramLevelDTL, null, logInfo);
//		}
//		catch (Exception ex) {
//			throw new DataAccessException(ex);
//		}
	}

	@Override
	public StDisplayPlAmtDtl getDisplayProgramLevelDTLById(long id) throws DataAccessException {
		return repo.getEntityById(StDisplayPlAmtDtl.class, id);
	}
	
	@Override
	public void updateDisplayProgramLevelDTL(StDisplayPlAmtDtl displayProgramLevelDTL, LogInfoVO logInfo) throws DataAccessException {
		if (displayProgramLevelDTL == null) {
			throw new IllegalArgumentException("displayProgramLevelDTL is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		StDisplayPlAmtDtl temp = this.getDisplayProgramLevelDTLById(displayProgramLevelDTL.getId());
		temp = temp.clone();
		
		displayProgramLevelDTL.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null) displayProgramLevelDTL.setUpdateUser(logInfo.getStaffCode());
		repo.update(displayProgramLevelDTL);
//		try {
//			actionGeneralLogDAO.createActionGeneralLog(temp, displayProgramLevelDTL, logInfo);
//		}
//		catch (Exception ex) {
//			throw new DataAccessException(ex);
//		}
	}
	
	/**
	 * @edit by tientv11
	 */
	@Override
	public List<StDisplayPlAmtDtl> getListDisplayProgramLevelDTL(
			KPaging<StDisplayPlAmtDtl> kPaging, Long displayProgramLevelDTLId, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM DISPLAY_PL_AMT_DTL WHERE 1=1 ");
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		}
		if (displayProgramLevelDTLId != null) {
			sql.append(" and DISPLAY_PROGRAM_LEVEL_ID=?");
			params.add(displayProgramLevelDTLId);
		}
		if (kPaging == null)
			return repo.getListBySQL(StDisplayPlAmtDtl.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StDisplayPlAmtDtl.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public List<DisplayProgrameExVO> getListDisplayProgramLevelDetail(Long displayProgramId,String levelCode,DisplayProductGroupType type)
			throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH tmp1 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM display_product_group ");
		sql.append(" WHERE display_program_id = ? ");
		params.add(displayProgramId);
		sql.append(" AND status = 1 ");
		sql.append(" AND display_product_group.type = ? ");
		params.add(type.getValue());
		sql.append(" ), ");
		sql.append(" tmp2 AS ");
		sql.append(" (SELECT dplt.* ");
		sql.append(" FROM display_program_level_dtl dplt, display_program_level dpl ");
		sql.append(" WHERE display_program_id = 352 ");
		sql.append(" AND dplt.display_program_level_id = dpl.display_program_level_id ");
		sql.append(" AND dplt.status = 1 ");
		sql.append(" AND dpl.status = 1 ");
		sql.append(" AND dpl.level_code = ? ");
		params.add(levelCode.toUpperCase());
		sql.append(" ) ");
		sql.append(" SELECT tmp1.display_product_group_id AS displayProductGroupId, ");
		sql.append(" tmp2.display_program_level_dtl_id AS displayProgramLevelDTLId, ");
		sql.append(" tmp1.display_product_group_code AS displayProductGroupCode , ");
		sql.append(" tmp1.display_product_group_name AS displayProductGroupName, ");
		sql.append(" tmp2.max, ");
		sql.append(" tmp2.min, ");
		sql.append(" tmp2.percent ");
		sql.append(" FROM tmp1 LEFT JOIN tmp2 ");
		sql.append(" ON tmp1.display_product_group_id = tmp2.display_product_group_id; ");
		
		String[] fieldNames = { 
				"displayProductGroupId", // 0
				"displayProgramLevelDTLId", // 1
				"displayProductGroupCode", // 2
				"displayProductGroupName", // 3
				"max", // 4
				"min", // 5
				"precent", // 6
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.BIG_DECIMAL, // 4
				StandardBasicTypes.BIG_DECIMAL, // 5
				StandardBasicTypes.FLOAT, // 6
		};
		return repo.getListByQueryAndScalar(DisplayProgrameExVO.class, fieldNames,fieldTypes, sql.toString(), params);
	}
	
	/**
	 * @author loctt
	 * @since 20sep2013
	 * sua ten table va column
	 */
	@Override
	public List<StDisplayPlAmtDtl> getListDisplayProgramLevelDetailByLevelId(Long levelId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM display_pl_amt_dtl WHERE 1=1 and status != -1");
		if (levelId != null) {
			sql.append(" and display_program_level_id=?");
			params.add(levelId);
		}
		return repo.getListBySQL(StDisplayPlAmtDtl.class, sql.toString(), params);
	}
	/**
	 * @author loctt
	 * @since 25sep2013
	 * cho san pham chuong trinh trung bay
	 */
	@Override
	public List<StDisplayPlDpDtl> getListDisplayProductLevelDetailByLevelId(Long levelId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT sd.* FROM display_pl_dp_dtl sd,  display_product_group_dtl sl,  display_product_group sg WHERE sl.DISPLAY_PRODUCT_GROUP_DTL_ID = sd.DISPLAY_PRODUCT_GROUP_DTL_ID AND sl.display_product_group_id       = sg.display_product_group_id");

		if (levelId != null) {
			sql.append(" and sd.display_program_level_id =?");
			params.add(levelId);
		}
		return repo.getListBySQL(StDisplayPlDpDtl.class, sql.toString(), params);
	}
}

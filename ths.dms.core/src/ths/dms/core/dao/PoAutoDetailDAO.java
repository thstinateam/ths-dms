package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.PoAutoDetail;
import ths.dms.core.entities.enumtype.KPaging;
import ths.core.entities.vo.rpt.RptBookProductVinamilkVO;
import ths.dms.core.exceptions.DataAccessException;

public interface PoAutoDetailDAO {

	PoAutoDetail createPoAutoDetail(PoAutoDetail poAutoDetail)
			throws DataAccessException;

	void deletePoAutoDetail(PoAutoDetail poAutoDetail)
			throws DataAccessException;

	void updatePoAutoDetail(PoAutoDetail poAutoDetail)
			throws DataAccessException;

	PoAutoDetail getPoAutoDetailById(int id) throws DataAccessException;

	List<PoAutoDetail> getListPoAutoDetail(KPaging<PoAutoDetail> kPaging,
			Long poAutoId) throws DataAccessException;
	
	/**
	 * lay danh sach bao cao dat hang cua vinamilk
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptBookProductVinamilkVO> getListRptBookProduct(Long shopId, Date fromDate,
			Date toDate) throws DataAccessException;
}

package ths.dms.core.dao;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ProductInfoMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ProductInfoMapType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class ProductInfoMapDAOImpl implements ProductInfoMapDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Autowired
	private CommonDAO commonDAO;

	@Override
	public ProductInfoMap createProductInfoMap(ProductInfoMap productInfoMap,LogInfoVO logInfo) throws DataAccessException {
		if (productInfoMap == null) {
			throw new IllegalArgumentException("productInfoMap");
		}
		productInfoMap.setCreateUser(logInfo.getStaffCode());
		productInfoMap.setCreateDate(commonDAO.getSysDate());
		productInfoMap = repo.create(productInfoMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, productInfoMap,logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(ProductInfoMap.class, productInfoMap.getId());
	}

	@Override
	public void deleteProductInfoMap(ProductInfoMap productInfoMap, LogInfoVO logInfo)
			throws DataAccessException {
		if (productInfoMap == null) {
			throw new IllegalArgumentException("productInfoMap");
		}
		repo.delete(productInfoMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(productInfoMap, null,logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public ProductInfoMap getProductInfoMapById(long id) throws DataAccessException {
		return repo.getEntityById(ProductInfoMap.class, id);
	}

	@Override
	public void updateProductInfoMap(ProductInfoMap productInfoMap, LogInfoVO logInfo)
			throws DataAccessException {
		if (productInfoMap == null) {
			throw new IllegalArgumentException("productInfoMap");
		}
		ProductInfoMap temp = this.getProductInfoMapById(productInfoMap.getId());
		productInfoMap.setUpdateUser(logInfo.getStaffCode());
		productInfoMap.setUpdateDate(commonDAO.getSysDate());
		repo.update(productInfoMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, productInfoMap,logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public ProductInfoMap getProductInfoMap(Long catId, Long subCatId, ProductInfoMapType productInfoMapType)
			throws DataAccessException {
		if (catId == null) {
			throw new IllegalArgumentException("getProductInfoMap:catId is null");
		}
		if (subCatId == null) {
			throw new IllegalArgumentException("getProductInfoMap:subCatId is null");
		}
		if (productInfoMapType == null) {
			throw new IllegalArgumentException("getProductInfoMap:productInfoMapType is null");
		}
		String sql = "select * from product_info_map where from_object_id = ? and to_object_id=? and object_type = ? and status =?";
		List<Object> params = new ArrayList<Object>();
		params.add(catId);
		params.add(subCatId);
		params.add(productInfoMapType.getValue());
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(ProductInfoMap.class, sql, params);
	}
	
	@Override
	public ProductInfoMap getProductInfoMapByToId(long toId, ProductInfoMapType productInfoMapType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_info_map where to_object_id = ? and object_type = ? ");
		params.add(toId);
		if (productInfoMapType != null) {
			params.add(productInfoMapType.getValue());
		}
		return repo.getEntityBySQL(ProductInfoMap.class, sql.toString(), params);
	}
	
}

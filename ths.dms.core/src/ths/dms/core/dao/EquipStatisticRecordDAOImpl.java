package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.EquipStatisticCustomer;
import ths.dms.core.entities.EquipStatisticGroup;
import ths.dms.core.entities.EquipStatisticRecord;
import ths.dms.core.entities.EquipStatisticStaff;
import ths.dms.core.entities.EquipStatisticUnit;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.EquipObjectType;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.EquipmentStatisticRecordStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipStatisticFilter;
import ths.dms.core.entities.vo.EquipRecordShopVO;
import ths.dms.core.entities.vo.EquipStatisticCustomerVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.StatisticCheckingVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class EquipStatisticRecordDAOImpl implements EquipStatisticRecordDAO{
	@Autowired
	private IRepository repo;
	
	@Override
	public EquipStatisticRecord createEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord) throws DataAccessException {
		return repo.create(equipStatisticRecord);
	}
	
	@Override
	public void updateEquipStatisticRecord(EquipStatisticRecord equipStatisticRecord) throws DataAccessException {
		repo.update(equipStatisticRecord);
	}
	
	@Override
	public EquipStatisticRecord getEquipStatisticRecordById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipStatisticRecord.class, id);
	}
	
	/**
	 * get equip_statistic_group by id
	 * @author phut
	 */
	@Override
	public EquipStatisticGroup getEquipStatisticGroupById(Long recordId, Long id, EquipObjectType type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from EQUIP_STATISTIC_GROUP where 1 = 1 ");
		sql.append(" and EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(recordId);
		sql.append(" and OBJECT_ID = ? and OBJECT_TYPE = ?");
		params.add(id);
		params.add(type.getValue());
		return repo.getEntityBySQL(EquipStatisticGroup.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipStatisticGroup> getListEquipStatisticGroup(EquipStatisticFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select r.* from EQUIP_STATISTIC_GROUP r where 1 = 1 ");
		if (filter.getRecordId() != null) {
			sql.append(" AND r.EQUIP_STATISTIC_RECORD_ID = ? ");
			params.add(filter.getRecordId());
		}
		if (filter.getObjectType() != null) {
			sql.append(" AND r.OBJECT_TYPE = ? ");
			params.add(filter.getObjectType());
		}
		if (filter.getStatus() != null) {
			sql.append(" AND r.STATUS = ? ");
			params.add(filter.getStatus());
		}
		if (filter.getObjectId() != null) {
			sql.append(" AND r.OBJECT_ID = ? ");
			params.add(filter.getObjectId());
		}
		return repo.getListBySQL(EquipStatisticGroup.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipStatisticGroup> getListEquipStatisticGroupByShop(EquipStatisticFilter filter) throws DataAccessException {
		if (filter.getRecordId() == null) {
			throw new IllegalArgumentException("recordId is null");
		}
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH shopRecord AS ");
		sql.append(" (SELECT shop_id ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" START WITH shop_id = ? ");
		params.add(filter.getShopId());
		sql.append(" AND status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" CONNECT BY prior shop_id = parent_shop_id ");
		sql.append(" ) ");
		sql.append(" SELECT esg.* ");
		sql.append(" FROM EQUIP_STATISTIC_GROUP esg ");
		sql.append(" JOIN EQUIPMENT eq ON esg.OBJECT_ID = eq.EQUIP_ID ");
		sql.append(" JOIN CUSTOMER c ON c.customer_id = eq.stock_id AND eq.stock_type = ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append(" JOIN shopRecord s on c.shop_id = s.shop_id ");
		sql.append(" WHERE esg.STATUS = ? AND esg.OBJECT_TYPE = ? AND EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(EquipObjectType.EQUIP.getValue());
		params.add(filter.getRecordId());
		return repo.getListBySQL(EquipStatisticGroup.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipStatisticRecord> getListEquipStatisticRecord(EquipRecordFilter<EquipStatisticRecord> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select r.* from EQUIP_STATISTIC_RECORD r where 1 = 1 ");
		if (filter.getFromDate() != null) {
			sql.append("and r.TO_DATE >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append("and r.FROM_DATE < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append("and r.code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			sql.append(" and lower(UNICODE2ENGLISH(r.name)) like ? ESCAPE '/' ");
			String n = Unicode2English.codau2khongdau(filter.getName());
			params.add(StringUtility.toOracleSearchLikeSuffix(n.trim().toLowerCase()));
		}
		if (filter.getStatus() != null && filter.getStatus() != -1) {
			sql.append(" and r.RECORD_STATUS = ? ");
			params.add(filter.getStatus());
		}

		if (filter.getLstShopId() != null && filter.getLstShopId().size() > 0) {
			sql.append(" and r.shop_id in (");
			sql.append(" select shop_id from shop where status = ? start with shop_id in (");
			params.add(ActiveType.RUNNING.getValue());
			for (Long id : filter.getLstShopId()) {
				if (id != null) {
					sql.append("?,");
					params.add(id);
				}
			}
			sql.append("-1) connect by prior shop_id = parent_shop_id)");
		}
		if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "r.shop_id");
			sql.append(paramsFix);
		}
		String sqlCount = "select count(1) as count from (" + sql.toString() + ")";
		sql.append(" order by r.FROM_DATE desc ");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(EquipStatisticRecord.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(EquipStatisticRecord.class, sql.toString(), sqlCount, params, params, filter.getkPaging());
		}
	}
	
	@Override 
	public List<EquipStatisticGroup> createListEquipStatisticGroup(List<EquipStatisticGroup> list) throws DataAccessException {
		return repo.create(list);
	}
	
	/**
	 * get list shop join in record
	 * @author phut
	 */
	@Override
	public List<EquipRecordShopVO> getListShopOfRecord(Long recordId, List<ShopSpecificType> lstShopSpecificType, String shopCode, String shopName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
//		sql.append("WITH lstNPP AS (SELECT psm.UNIT_ID AS id,(SELECT shop_code FROM shop WHERE shop_id = psm.UNIT_ID) AS shopCode, ");
//		sql.append("(SELECT shop_name FROM shop WHERE shop_id = psm.UNIT_ID) AS shopName, ");
//		sql.append("(SELECT name_text FROM shop WHERE shop_id = psm.UNIT_ID) AS nameText ");
		sql.append("WITH lstNPP AS (SELECT psm.UNIT_ID AS id,s.shop_code AS shopCode, s.shop_name AS shopName,s.name_text AS nameText ,s.status AS status ");
		sql.append("FROM EQUIP_STATISTIC_UNIT psm join shop s on s.shop_id = psm.UNIT_ID ");
		sql.append("WHERE EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(recordId);
		if(!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append("AND lower(s.shop_code) LIKE ? ESCAPE '/' ");
			String s = shopCode.trim().toLowerCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if(!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and lower(s.name_text) like ? escape '/' ");
			String s = Unicode2English.codau2khongdau(shopName.trim());
			s = StringUtility.toOracleSearchLike(s.toLowerCase());
			params.add(s);
		}
		sql.append(" ), ");
		sql.append("lstDV AS(SELECT DISTINCT s.parent_shop_id AS parentId, s.shop_id AS id, s.shop_code AS shopCode, s.shop_name AS shopName, s.name_text AS nameText ,s.status AS status, ");
		sql.append(" level AS orderNo, ct.SPECIFIC_TYPE ");
		sql.append("FROM shop s JOIN shop_type ct ON (ct.shop_type_id = s.shop_type_id) ");
		if (lstShopSpecificType != null && lstShopSpecificType.size() > 0) {
			sql.append("where ct.SPECIFIC_TYPE IN (");
			String lstType = "-1";
			for (int i = 0, size = lstShopSpecificType.size(); i < size; i++) {
				lstType += ", ?";
				params.add(lstShopSpecificType.get(i).getValue());
			}
			sql.append(lstType);
			sql.append(") ");
		}
		sql.append("START WITH shop_id  IN (SELECT id FROM lstNPP) ");
		sql.append("CONNECT BY prior parent_shop_id = shop_id) ");
		sql.append("SELECT s.parentId, s.id, s.shopCode, s.shopName, s.nameText,s.status , (CASE s.SPECIFIC_TYPE WHEN ? THEN 1 ELSE 0 END) AS isNPP, (case when npp.id is not null then 1 else 0 end) isCheck ");
		params.add(ShopSpecificType.NPP.getValue());
		sql.append("FROM lstDV s LEFT JOIN lstNPP npp ON (s.id = npp.id) ");
		sql.append("WHERE 1  = 1 ");		
		sql.append(" ORDER BY isNPP, orderNo desc, shopCode, shopName");
		
		String[] fieldNames = new String[] {
				"id",
				"parentId",
				"shopCode",
				"shopName",
				"nameText",
				"status",
				"isNPP",
				"isCheck"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER
		};
		
		return repo.getListByQueryAndScalar(EquipRecordShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	
	/**
	 * get list customer 4 record
	 * @author phut
	 */
	@Override
	public List<EquipStatisticCustomerVO> searchCustomer4Record(KPaging<EquipStatisticCustomerVO> kPaging, Long statisId, String shortCode, String customerName, Integer status, Integer statusCus) throws DataAccessException {
//		EquipStatisticRecord record = repo.getEntityById(EquipStatisticRecord.class, statisId);
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" with ");
		sql.append(" StaffEquip as (");
		sql.append("   select e.stock_id,st.staff_id,st.staff_code,st.staff_name,e.code");
		sql.append("  		  from equipment e");
		sql.append("    join equip_statistic_group gr on gr.object_type = 3 and gr.object_id = e.equip_id and gr.EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(statisId);
		sql.append("    left join equip_statistic_staff est on est.EQUIP_STATISTIC_RECORD_ID = ? and e.equip_id = est.equip_id");
		params.add(statisId);
		sql.append("    left join staff st on st.staff_id = est.staff_id ");
		sql.append("    where e.status = 1 and e.stock_type = 2");
		sql.append("    union");
		sql.append("    select e.stock_id,st.staff_id,st.staff_code,st.staff_name,e.code");
		sql.append("   from equipment e");
		sql.append("    join equip_group g on g.equip_group_id = e.equip_group_id");
		sql.append("    join equip_statistic_group gr on gr.object_type = 1 and gr.object_id = g.equip_group_id and gr.EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(statisId);
		sql.append("    left join equip_statistic_staff est on est.EQUIP_STATISTIC_RECORD_ID = ? and e.equip_id = est.equip_id");
		params.add(statisId);
		sql.append("    left join staff st on st.staff_id = est.staff_id ");
		sql.append("    where e.status = 1 and e.stock_type = 2");
		sql.append("  )");
		
		sql.append("select esc.EQUIP_STATISTIC_CUSTOMER_ID id, sh.shop_code shopCode, c.short_code shortCode, c.customer_name customerName, c.housenumber houseNumber, c.street street,c.address, temp.staff_code staffCode,temp.staff_name as staffName, c.status statusCus,temp.code equipCode");
		sql.append(" from EQUIP_STATISTIC_CUSTOMER esc");
		sql.append(" inner join CUSTOMER c on esc.CUSTOMER_ID = c.CUSTOMER_ID ");
		sql.append(" inner join shop sh on c.shop_id = sh.shop_id ");
		sql.append(" left join StaffEquip temp on temp.stock_id = c.customer_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and esc.EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(statisId);
		if (statusCus != null) {
			sql.append(" and c.status = ? ");
			params.add(statusCus);
		}
		if(!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append(" and c.short_code LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shortCode.toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(customerName)) {
			sql.append(" and (upper(c.NAME_TEXT) like ? escape '/' or  upper(c.address) like ? escape '/')");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(customerName.trim()).toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(customerName.trim().toUpperCase()));
		}
		if(status != null) {//status = 0 du thao => import, 1 => load all
			sql.append(" and esc.status = ? ");
			params.add(status);
		}
		if(status != null && status == 0) {
			sql.append(" and esc.type = ? ");
			params.add(2);//import excel
		}
		
		sql.append(" order by sh.shop_code,c.short_code,temp.staff_code,temp.code");
		
		String[] fieldNames = new String[] {
				"id",
				"shopCode",
				"shortCode",
				"customerName",
				"houseNumber",
				"street",
				"address",
				"staffCode",
				"staffName",
				"statusCus",
				"equipCode"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING
		};
		
		if(kPaging == null) {
			return repo.getListByQueryAndScalar(EquipStatisticCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			String sqlCount = "select count(1) as count from (" + sql.toString() + ")";
			return repo.getListByQueryAndScalarPaginated(EquipStatisticCustomerVO.class, fieldNames, fieldTypes, sql.toString(), sqlCount, params, params, kPaging);
		}
	}
	
	@Override
	public List<EquipStatisticCustomerVO> searchCustomer4RecordEquipGroup(KPaging<EquipStatisticCustomerVO> kPaging, Long statisId, String shortCode, String customerName, Integer statusCus) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" with ");
		sql.append(" StaffEquip as (");
		sql.append("   select e.stock_id,st.staff_id,st.staff_code,st.staff_name,e.code");
		sql.append("  		  from equipment e");
		sql.append("    join equip_statistic_group gr on gr.object_type = 3 and gr.object_id = e.equip_id and gr.EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(statisId);
		sql.append("    left join equip_statistic_staff est on est.EQUIP_STATISTIC_RECORD_ID = ? and e.equip_id = est.equip_id");
		params.add(statisId);
		sql.append("    left join staff st on st.staff_id = est.staff_id ");
		sql.append("    where e.status = 1 and e.stock_type = 2");
		sql.append("    union");
		sql.append("    select e.stock_id,st.staff_id,st.staff_code,st.staff_name,e.code");
		sql.append("   from equipment e");
		sql.append("    join equip_group g on g.equip_group_id = e.equip_group_id");
		sql.append("    join equip_statistic_group gr on gr.object_type = 1 and gr.object_id = g.equip_group_id and gr.EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(statisId);
		sql.append("    left join equip_statistic_staff est on est.EQUIP_STATISTIC_RECORD_ID = ? and e.equip_id = est.equip_id");
		params.add(statisId);
		sql.append("    left join staff st on st.staff_id = est.staff_id ");
		sql.append("    where e.status = 1 and e.stock_type = 2");
		sql.append("  )");
		sql.append("    ,custemp as ( ");
		sql.append("select esc.EQUIP_STATISTIC_CUSTOMER_ID id, sh.shop_code shopCode, c.short_code shortCode, c.customer_name customerName, c.housenumber houseNumber, c.street street,c.address,c.NAME_TEXT, temp.staff_code staffCode,temp.staff_name as staffName,c.status statusCus,temp.code equipCode");
		sql.append(" from EQUIP_STATISTIC_CUSTOMER esc");
		sql.append(" inner join CUSTOMER c on esc.CUSTOMER_ID = c.CUSTOMER_ID ");
		sql.append(" inner join shop sh on c.shop_id = sh.shop_id ");
		sql.append(" left join StaffEquip temp on temp.stock_id = c.customer_id ");
		sql.append(" where 1 = 1 and esc.status = 0 ");
		sql.append(" and esc.EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(statisId);
		
		sql.append("    union");
		
		sql.append(" SELECT ");
		sql.append(" DISTINCT -1 id,");
		sql.append(" sh.shop_code shopCode,");
		sql.append(" cu.short_code shortCode,");
		sql.append("  cu.customer_name customerName,");
		sql.append("  cu.housenumber houseNumber,");
		sql.append("   cu.street street,");
		sql.append("   cu.address,");
		sql.append("   cu.NAME_TEXT,");
		sql.append("  temp.staff_code staffCode,");
		sql.append("   temp.staff_name staffName,cu.status statusCus,temp.code equipCode");
		sql.append(" FROM CUSTOMER cu");
		sql.append(" JOIN EQUIP_STATISTIC_UNIT un");
		sql.append(" ON un.EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(statisId);
		sql.append(" AND un.UNIT_ID                  = cu.shop_id");
//		sql.append(" JOIN EQUIP_STATISTIC_GROUP esg");
//		sql.append(" ON un.EQUIP_STATISTIC_RECORD_ID = esg.EQUIP_STATISTIC_RECORD_ID");
		sql.append(" JOIN EQUIP_STOCK_TOTAL est");
		sql.append(" ON est.STOCK_TYPE       = 2");
		sql.append(" AND est.stock_id        = cu.CUSTOMER_ID");
		sql.append(" AND est.QUANTITY        > 0");
		sql.append(" INNER JOIN shop sh ON cu.shop_id                      = sh.shop_id");
		sql.append(" join StaffEquip temp on temp.stock_id = cu.customer_id ");
		sql.append(" WHERE 1         = 1");
//		sql.append(" AND esg.OBJECT_TYPE     in (1,3)");
		sql.append("  AND cu.CUSTOMER_ID NOT IN (SELECT CUSTOMER_ID FROM EQUIP_STATISTIC_CUSTOMER WHERE status <> -1 AND EQUIP_STATISTIC_RECORD_ID = ? )");
		params.add(statisId);
		 
		sql.append("  )");
		sql.append(" select * from custemp c1 where 1=1 ");
		
		if(!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append(" and c1.shortCode LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shortCode.toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(customerName)) {
			sql.append(" and (upper(c1.NAME_TEXT) like ? escape '/' or  upper(c1.address) like ? escape '/')");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(customerName.trim()).toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(customerName.trim().toUpperCase()));
		}
		if (statusCus != null) {
			sql.append(" and c1.statusCus = ?");
			params.add(statusCus);
		}
		
		sql.append(" order by shopCode,shortCode,staffCode,equipCode");
		
		String[] fieldNames = new String[] {
				"id",
				"shopCode",
				"shortCode",
				"customerName",
				"houseNumber",
				"street",
				"address",
				"staffCode",
				"staffName",
				"statusCus",
				"equipCode"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING
		};
		
		if(kPaging == null) {
			return repo.getListByQueryAndScalar(EquipStatisticCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			String sqlCount = "select count(1) as count from (" + sql.toString() + ")";
			return repo.getListByQueryAndScalarPaginated(EquipStatisticCustomerVO.class, fieldNames, fieldTypes, sql.toString(), sqlCount, params, params, kPaging);
		}
	}
	
	/**
	 * check shop in record
	 * @author phut
	 */
	@Override
	public Integer checkShopInRecordUnit(Long recordId, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(EQUIP_STATISTIC_UNIT_ID) as count from EQUIP_STATISTIC_UNIT where EQUIP_STATISTIC_RECORD_ID = ? and UNIT_ID = ? and STATUS = ?");
		params.add(recordId);
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.countBySQL(sql.toString(), params);
	}
	
	/**
	 * search shop 4 join record
	 * @author phut
	 */
	@Override
	public List<EquipRecordShopVO> searchShopForRecord(Long recordId, Long pShopId, String shopCode, String shopName, Integer isLevel)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		if (StringUtility.isNullOrEmpty(shopCode) && StringUtility.isNullOrEmpty(shopName)) {
			sql.append("SELECT s.shop_id AS id, s.parent_shop_id AS parentId, s.shop_code AS shopCode, s.shop_name AS shopName, ");
			sql.append("(SELECT COUNT(1) FROM channel_type WHERE channel_type_id = s.shop_type_id AND status = 1 AND object_type IN (?, ?)) AS isNPP, ");
			params.add(ShopObjectType.NPP.getValue());
			params.add(ShopObjectType.NPP_KA.getValue());
			sql.append(" (select count(EQUIP_STATISTIC_UNIT_ID) from EQUIP_STATISTIC_UNIT where EQUIP_STATISTIC_RECORD_ID = ? and UNIT_ID = s.shop_id and status = 1) as isCheck ");
			params.add(recordId);
			sql.append(" FROM shop s WHERE parent_shop_id = ? ");
			params.add(pShopId);
			//sql.append("and shop_id not in (SELECT unit_id FROM EQUIP_STATISTIC_UNIT WHERE EQUIP_STATISTIC_RECORD_ID = ? AND rownum = 1 AND unit_id IN ");
			//params.add(recordId);
			//sql.append("(SELECT shop_id FROM shop START WITH shop_id = s.shop_id CONNECT BY prior shop_id = parent_shop_id)) ");
			sql.append("AND status = 1 ORDER BY shopCode, shopName");
		} else {
			sql.append("WITH lstShopTmp AS ");
			sql.append("(SELECT shop_id FROM shop WHERE status = 1 START WITH shop_id = ? CONNECT BY prior shop_id = parent_shop_id), ");
			params.add(pShopId);
			sql.append("lstDVTmp AS ");
			sql.append("(SELECT DISTINCT s.shop_id AS id, s.parent_shop_id AS parentId, s.shop_code AS shopCode, s.shop_name AS shopName, ");
			sql.append("(SELECT object_type FROM channel_type WHERE channel_type_id = s.shop_type_id AND status = 1) AS shopType ");
			sql.append("FROM shop s WHERE shop_id IN (SELECT shop_id FROM lstShopTmp) START WITH 1=1 ");
			if(!StringUtility.isNullOrEmpty(shopCode)) {
				sql.append("AND lower(shop_code) LIKE ? ESCAPE '/' ");
				String s = shopCode.trim().toLowerCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if(!StringUtility.isNullOrEmpty(shopName)) {
				sql.append(" and lower(name_text) like ? escape '/'");
				String s = Unicode2English.codau2khongdau(shopName.trim());
				s = StringUtility.toOracleSearchLike(s.toLowerCase());
				params.add(s);
			}
			sql.append("CONNECT BY prior parent_shop_id = shop_id ");
			sql.append("), ");
			sql.append("lstDV AS (SELECT id, parentId, shopCode, shopName, ");
			sql.append("(CASE WHEN shopType = ? THEN 0 ");
			params.add(ShopObjectType.VNM.getValue());
			sql.append("WHEN shopType IN (?, ?, ?) THEN 1 ");
			params.add(ShopObjectType.GT.getValue());
			params.add(ShopObjectType.KA.getValue());
			params.add(ShopObjectType.ST.getValue());
			sql.append("WHEN shopType IN (?, ?, ?) THEN 2 ");
			params.add(ShopObjectType.MIEN.getValue());
			params.add(ShopObjectType.MIEN_KA.getValue());
			params.add(ShopObjectType.MIEN_ST.getValue());
			sql.append("WHEN shopType IN (?, ?) THEN 3 ");
			params.add(ShopObjectType.VUNG.getValue());
			params.add(ShopObjectType.VUNG_KA.getValue());
			sql.append("WHEN shopType IN (?, ?) THEN 4 END) AS orderNo ");
			params.add(ShopObjectType.NPP.getValue());
			params.add(ShopObjectType.NPP_KA.getValue());
			sql.append("FROM lstDVTmp) ");
			sql.append("SELECT id, parentId, shopCode, shopName, (CASE orderNo WHEN 4 THEN 1 ELSE 0 END) AS isNPP, ");
			sql.append("(select count(EQUIP_STATISTIC_UNIT_ID) from EQUIP_STATISTIC_UNIT where EQUIP_STATISTIC_RECORD_ID = ? and UNIT_ID = id and status = 1) as isCheck ");
			params.add(recordId);
			sql.append(" FROM lstDV ");
			sql.append("ORDER BY isNPP, orderNo, shopCode, shopName");
		};
		
		String[] fieldNames = new String[] {
				"id",
				"parentId",
				"shopCode",
				"shopName",
				"isNPP",
				"isCheck"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER
		};
		
		List<EquipRecordShopVO> lst = repo.getListByQueryAndScalar(EquipRecordShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
	
	@Override
	public List<EquipRecordShopVO> getListShopInRecord(long recordId, List<Long> lstShopId, boolean shopMapOnly) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("with lstShopTmp as (");
		sql.append(" select unit_id as shop_id");
		sql.append(" from EQUIP_STATISTIC_UNIT ");
		sql.append(" where EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
		sql.append(" )");
		sql.append(" select distinct shop_id as id, shop_code as shopCode, shop_name as shopName");
		sql.append(" from shop");
		sql.append(" where 1=1");
		if (lstShopId != null && lstShopId.size() > 0) {
			sql.append(" and shop_id in (-1");
			for (Long id : lstShopId) {
				sql.append(",?");
				params.add(id);
			}
			sql.append(")");
		}
		if (shopMapOnly) {
			sql.append(" and shop_id in (select shop_id from lstShopTmp)");
		} else {
			sql.append(" start with shop_id in (select shop_id from lstShopTmp)");
			sql.append(" connect by prior parent_shop_id = shop_id");
		}
		
		String[] fieldNames = new String[] {
				"id",
				"shopCode",
				"shopName",
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
		};
		
		List<EquipRecordShopVO> lst = repo.getListByQueryAndScalar(EquipRecordShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
	
	/**
	 * insert list statistic customer
	 * @author phut
	 */
	@Override
	public void insertListEquipStatisticCustomer(List<EquipStatisticCustomer> list) throws DataAccessException {
		repo.create(list);
	}
	
	/**
	 * get map exist customer in record
	 * @author phut
	 */
	@Override
	public Map<Long, EquipStatisticCustomer> getListExistsCustomer(List<EquipStatisticCustomer> list) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from EQUIP_STATISTIC_CUSTOMER where EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(list.get(0).getEquipStatisticRecord().getId());
		sql.append("and CUSTOMER_ID IN (?");
		params.add(list.get(0).getCustomer().getId());
		for(int i = 1 ; i < list.size(); i++) {
			sql.append(", ?");
			params.add(list.get(i).getCustomer().getId());
		}
		sql.append(") and STATUS != ?");
		params.add(-1);
		List<EquipStatisticCustomer> listResult = repo.getListBySQL(EquipStatisticCustomer.class, sql.toString(), params);
		Map<Long, EquipStatisticCustomer> map = new HashMap<Long, EquipStatisticCustomer>();
		for(EquipStatisticCustomer en : listResult) {
			map.put(en.getCustomer().getId(), en);
		}
		return map;
	}
	
	/**
	 * check khach hang thuộc kiem ke
	 * @author phuongvm
	 */
	@Override
	public Boolean checkCustomerInEquipStatisCus(Long recordId, Long customerId) throws DataAccessException {
		if (null == recordId) {
			throw new IllegalArgumentException("recordId is null");
		}
		if (null == customerId) {
			throw new IllegalArgumentException("customerId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from EQUIP_STATISTIC_CUSTOMER where EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
		sql.append(" and CUSTOMER_ID = ?");
		params.add(customerId);
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}
	
	@Override
	public EquipStatisticUnit createEquipStatisticUnit(EquipStatisticUnit equipStatisticUnit, LogInfoVO logInfo) throws DataAccessException {
		return repo.create(equipStatisticUnit);
	}
	
	@Override
	public void deleteEquipStatisticUnit(EquipStatisticUnit equipStatisticUnit) throws DataAccessException {
		repo.delete(equipStatisticUnit);
	}
	
	@Override
	public void deleteEquipStatisticGroup(EquipStatisticGroup equipStatisticGroup) throws DataAccessException {
		repo.delete(equipStatisticGroup);
	}
	
	@Override
	public void deleteEquipStatisticGroup(List<EquipStatisticGroup> lstEquipStatisticGroup) throws DataAccessException {
		if (lstEquipStatisticGroup == null || lstEquipStatisticGroup.size() == 0) {
			return;
		}
		for (EquipStatisticGroup esg : lstEquipStatisticGroup) {
			repo.delete(esg);
		}
	}
	
	/**
	 * get list equip_statistic_unit in record
	 * @author phut
	 */
	@Override
	public List<EquipStatisticUnit> getListEquipStatisticUnitChildShopMapWithShopAndRecord(Long shopId, Long recordId, Boolean isGetChild) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if(isGetChild) {
			sql.append(" select * from EQUIP_STATISTIC_UNIT psm where unit_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
		} else {
			sql.append(" select * from EQUIP_STATISTIC_UNIT psm where unit_id  = ?");
		}
		params.add(shopId);
		sql.append(" and EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
		return repo.getListBySQL(EquipStatisticUnit.class, sql.toString(), params);
	}
	
	/**
	 * 
	 * Xu ly danh sach EquipStatisticUnit shop cha theo shopId va recordId
	 * @author trietptm
	 * @param lstShopId
	 * @param recordId
	 * @return List<EquipStatisticUnit>
	 * @throws DataAccessException
	 * @since Mar 9, 2016
	 */
	@Override
	public List<EquipStatisticUnit> getListEquipStatisticUnitParentShop(List<Long> lstShopId, long recordId) throws DataAccessException {
		if (lstShopId == null || lstShopId.size() == 0) {
			throw new IllegalArgumentException("shopId is null or empty");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from EQUIP_STATISTIC_UNIT psm where unit_id in (select shop_id from shop start with shop_id in (-1 ");
		for(int i = 0 ; i < lstShopId.size(); i++) {
			sql.append(", ?");
			params.add(lstShopId.get(i));
		}
		sql.append(" ) ");
		sql.append(" connect by prior parent_shop_id = shop_id)");
		sql.append(" and unit_id not in (-1 ");
		for(int i = 0 ; i < lstShopId.size(); i++) {
			sql.append(", ?");
			params.add(lstShopId.get(i));
		}
		sql.append(" ) ");
		sql.append(" and EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
		return repo.getListBySQL(EquipStatisticUnit.class, sql.toString(), params);
	}

	@Override
	public List<StatisticCheckingVO> getListEquipmentStatisticCheckingVOForExport(
			Long idRecord) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT esrd.equip_statistic_rec_dtl_id AS detailId, ");
		sql.append("       s.shop_code                     AS shopCode, ");
		sql.append("       s.shop_name                     AS shopName, ");
		sql.append("       c.short_code                    AS shortCode, ");
		sql.append("       c.customer_name                 AS customerName, ");
		sql.append("       c.address                       AS address, ");
		sql.append("       eg.NAME                         AS equipNameGroup, ");
		sql.append("       eg.CODE                         AS equipCodeGroup, ");
		sql.append("       e.code                          AS equipCode, ");
		sql.append("       e.serial                        AS seri, ");
		sql.append("       esrd.status                     AS status ");
		sql.append("FROM   equip_statistic_record esr, ");
		sql.append("       equip_statistic_rec_dtl esrd, ");
		sql.append("       equipment e, ");
		sql.append("       equip_group eg, ");
		sql.append("       customer c, ");
		sql.append("       shop s ");
		sql.append("WHERE  esr.equip_statistic_record_id = ? ");
		params.add(idRecord);
		sql.append("       AND c.status = 1 ");
		sql.append("       AND s.status = 1 ");
		sql.append("       AND eg.status = 1 ");
		sql.append("       AND e.status = 1 ");
		sql.append("       AND esr.equip_statistic_record_id = esrd.equip_statistic_record_id ");
		sql.append("       AND esrd.shop_id = s.shop_id ");
		sql.append("       AND esrd.customer_id = c.customer_id ");
		sql.append("       AND esrd.equip_id = e.equip_id ");
		sql.append("       AND e.equip_group_id = eg.equip_group_id ");
		sql.append("ORDER  BY s.shop_code, c.short_code, eg.CODE, e.code");
		
		String[] fieldNames = new String[] {
				"detailId",				// 1
				"shopCode",				// 2
				"shopName",				// 3
				"shortCode",			// 4
				"customerName",			// 5
				"address",				// 6
				"equipNameGroup",		// 7
				"equipCodeGroup",		// 8
				"equipCode",			// 9
				"seri",					// 10
				"status",				// 11
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,	//1
				StandardBasicTypes.STRING,	//2
				StandardBasicTypes.STRING,	//3
				StandardBasicTypes.STRING,	//4
				StandardBasicTypes.STRING,	//5
				StandardBasicTypes.STRING,	//6
				StandardBasicTypes.STRING,	//7
				StandardBasicTypes.STRING,	//8
				StandardBasicTypes.STRING,	//9
				StandardBasicTypes.STRING,	//10
				StandardBasicTypes.INTEGER,	//11
		};
		
		return repo.getListByQueryAndScalar(StatisticCheckingVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public EquipStatisticRecord getEquipStatisticRecordByCode(String code) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from EQUIP_STATISTIC_RECORD where 1 = 1 ");
		sql.append(" and CODE = ? ");
		params.add(code.toUpperCase());
		return repo.getEntityBySQL(EquipStatisticRecord.class, sql.toString(), params);
	}
	
	/**
	 * check thiết bi thuộc đơn vị và nhóm thiết bị của kiểm kê
	 * @author phuongvm
	 */
	@Override
	public Boolean checkEquipInUnitAndGroup(Long recordId, Long equipId) throws DataAccessException {
		if (null == equipId) {
			throw new IllegalArgumentException("equipId is null");
		}
		if (null == recordId) {
			throw new IllegalArgumentException("recordId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from equipment where equip_id = ?");
		params.add(equipId);
		sql.append(" and( (equip_group_id in (select object_id from equip_statistic_group where equip_statistic_record_id = ? and status = 1 and object_type = 1)) ");
		params.add(recordId);
		sql.append(" or (equip_id in (select object_id from equip_statistic_group where equip_statistic_record_id = ? and status = 1 and object_type = 3)) )");
		params.add(recordId);
		sql.append(" and stock_id in (select equip_stock_id from equip_stock where shop_id in   (SELECT unit_id as shop_id");
		sql.append(" FROM equip_statistic_unit");
		sql.append("  WHERE EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
		sql.append(" ))");
		
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}
	
	@Override
	public Boolean checkEquipInUnit(Long recordId, Long equipId) throws DataAccessException {
		if (null == equipId) {
			throw new IllegalArgumentException("equipId is null");
		}
		if (null == recordId) {
			throw new IllegalArgumentException("recordId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from equipment where equip_id = ?");
		params.add(equipId);
//		sql.append(" and ( stock_id in (select equip_stock_id from equip_stock where shop_id in   (SELECT unit_id as shop_id");
//		sql.append(" FROM equip_statistic_unit");
//		sql.append("  WHERE EQUIP_STATISTIC_RECORD_ID = ?");
//		params.add(recordId);
//		sql.append(" ))");
//		sql.append(" or stock_id in (select customer_id from customer where shop_id in   (SELECT unit_id as shop_id");
//		sql.append(" FROM equip_statistic_unit");
//		sql.append("  WHERE EQUIP_STATISTIC_RECORD_ID = ?");
//		params.add(recordId);
//		sql.append(" )))");
		sql.append(" and ( equip_group_id in (select object_id from equip_statistic_group where object_type = 1 and equip_statistic_record_id = ? and status = 1)");
		params.add(recordId);
		sql.append("     or equip_id in (select object_id from equip_statistic_group where object_type = 3 and equip_statistic_record_id = ? and status = 1))");
		params.add(recordId);
		
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public Shop getShopInEquip(Long stockId) throws DataAccessException {
		if (null == stockId) {
			throw new IllegalArgumentException("stockId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from shop where shop_id in (select shop_id from equip_stock where equip_stock_id = ?) ");
		params.add(stockId);
		return repo.getEntityBySQL(Shop.class, sql.toString(), params);
	}
	
	/**
	 * check don vi co thuoc danh sach don vi kiem ke
	 * @author phuongvm
	 */
	@Override
	public Boolean checkExistInEquipStatisticUnit(Long recordId, Long shopId) throws DataAccessException {
		if (null == shopId) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (null == recordId) {
			throw new IllegalArgumentException("recordId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from equip_statistic_unit where equip_statistic_record_id = ? and unit_id = ?");
		params.add(recordId);
		params.add(shopId);
		
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}
	/**
	 * check u ke/nhom thiet bi co thuoc danh sach nhom kiem ke
	 * @author phuongvm
	 */
	@Override
	public Boolean checkExistInEquipStatisticGroup(Long recordId, Long objectId) throws DataAccessException {
		if (null == objectId) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (null == recordId) {
			throw new IllegalArgumentException("recordId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from equip_statistic_group where equip_statistic_record_id = ? and object_id = ? and status = 1");
		params.add(recordId);
		params.add(objectId);
		
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}
	
	@Override
	public List<EquipStatisticCustomer> getListEquipStatisticCustomer(Long recordId) throws DataAccessException {
		if (null == recordId) {
			throw new IllegalArgumentException("recordId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_statistic_customer where equip_statistic_record_id = ? and status = 0 ");
		params.add(recordId);
		return repo.getListBySQL(EquipStatisticCustomer.class, sql.toString(), params);
	}

	@Override
	public List<EquipRecordShopVO> getListShopOfRecordExport(Long recordId, List<ShopObjectType> lstShopObjectType, String shopCode, String shopName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT psm.UNIT_ID AS id, ");
		sql.append(" s.shop_code AS shopCode, ");
		sql.append(" s.shop_name AS shopName, ");
		sql.append(" s.name_text AS nameText , ");
		sql.append(" s.status AS status, ");
		sql.append(" PSM.STATUS ");
		sql.append(" FROM EQUIP_STATISTIC_UNIT psm ");
		sql.append(" JOIN shop s ");
		sql.append(" ON s.shop_id = psm.UNIT_ID ");
		sql.append(" WHERE s.status = 1 and psm.status = 1 and psm.EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
		if(!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append("AND lower(s.shop_code) LIKE ? ESCAPE '/' ");
			String s = shopCode.trim().toLowerCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if(!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and lower(s.name_text) like ? escape '/' ");
			String s = Unicode2English.codau2khongdau(shopName.trim());
			s = StringUtility.toOracleSearchLike(s.toLowerCase());
			params.add(s);
		}		
		sql.append(" ORDER BY shopCode, shopName");
		
		String[] fieldNames = new String[] {
				"id",
				"shopCode",
				"shopName",
				"nameText",
				"status",
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER
		};
		
		return repo.getListByQueryAndScalar(EquipRecordShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<EquipStatisticUnit> getListStatisticUnitChild(Long recordId,Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_statistic_unit where 1 = 1 ");
		if (recordId != null) {
			sql.append(" and equip_statistic_record_id = ? ");
			params.add(recordId);
		}
		if (shopId != null) {
			sql.append(" and unit_id in (select shop_id from shop  ");
			sql.append("  start with shop_id = ?  connect by parent_shop_id = prior shop_id) ");
			params.add(shopId);
		}
		return repo.getListBySQL(EquipStatisticUnit.class, sql.toString(), params);
	}
	
	@Override
	public List<Shop> getListShopForEquipStatisticUnit(Long shopId,List<Long> lstShopIdExcept) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from shop where status = 1 ");
		if(lstShopIdExcept!=null && !lstShopIdExcept.isEmpty()){
			sql.append(" and shop_id not in (-1 ");
			for(int i = 0 ; i < lstShopIdExcept.size(); i++) {
				sql.append(", ?");
				params.add(lstShopIdExcept.get(i));
			}
			sql.append(" ) ");
		}
		if (shopId != null) {
			sql.append("  start with shop_id = ?  connect by parent_shop_id = prior shop_id ");
			params.add(shopId);
		}

		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	@Override
	public Boolean checkStaffInEquipStatisStaff(Long recordId, Long staffId,Long equipId) throws DataAccessException {
		if (null == recordId) {
			throw new IllegalArgumentException("recordId is null");
		}
		if (null == staffId) {
			throw new IllegalArgumentException("staffId is null");
		}
		if (null == equipId) {
			throw new IllegalArgumentException("equipId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from EQUIP_STATISTIC_STAFF where EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
//		sql.append(" and staff_id = ?");
//		params.add(staffId);
		sql.append(" and equip_id = ?");
		params.add(equipId);
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}
	
	@Override
	public List<EquipStatisticStaff> getListEquipStatisticStaffNotBelong(Long recordId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_statistic_staff  ");
		sql.append(" where equip_statistic_record_id = ? and equip_id not in ( ");
		params.add(recordId);
		sql.append(" select e.equip_id ");
		sql.append(" from (select e.* ");
		sql.append("  	   from equipment e");
		sql.append("       join equip_statistic_group gr on gr.object_id = e.equip_group_id and e.status = 1 and gr.object_type = 1 ");
//		params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
//		params.add(EquipUsageStatus.IS_USED.getValue());
//		params.add(EquipUsageStatus.SHOWING_REPAIR.getValue());
		sql.append("       where gr.equip_statistic_record_id = ? ");
		params.add(recordId);
		sql.append(" 	   union ");
		sql.append("       select e.* ");
		sql.append("       from equipment e ");
		sql.append("       join equip_statistic_group gr on gr.object_id = e.equip_id and e.status = 1  and gr.object_type = 3 ");
//		params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
//		params.add(EquipUsageStatus.IS_USED.getValue());
//		params.add(EquipUsageStatus.SHOWING_REPAIR.getValue());
		sql.append("       where gr.equip_statistic_record_id = ? ");
		params.add(recordId);
		sql.append("      ) e ");
//		sql.append("  where (e.stock_type = 2 and e.stock_id in (select customer_id from customer where shop_id in (select unit_id from equip_statistic_unit where equip_statistic_record_id = ?)  ) ) ");
//		params.add(recordId);
//		sql.append(" or (e.stock_type = 1 and e.stock_id in (select equip_stock_id from equip_stock where shop_id in (select unit_id from equip_statistic_unit where equip_statistic_record_id = ?) ) ) ");
//		params.add(recordId);
		sql.append(" ) ");
		return repo.getListBySQL(EquipStatisticStaff.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipmentVO> getListChecking(Date fromDate, Date toDate, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select rc.code,rc.equip_statistic_record_id id from equip_statistic_record rc ");
		sql.append(" where rc.record_status in (?,?) ");
		params.add(EquipmentStatisticRecordStatus.RUNNING.getValue());
		params.add(EquipmentStatisticRecordStatus.DONE.getValue());
		if (toDate != null) {
			sql.append(" and rc.from_date < trunc(?) + 1");
			params.add(toDate);
		}
		if (fromDate != null) {
			sql.append(" and (rc.to_date is null or rc.to_date >= trunc(?))  ");
			params.add(fromDate);
		}
		if (shopId != null) {
			sql.append(" and rc.shop_id in ");
			sql.append("  (SELECT shop_id ");
			sql.append("  FROM shop ");
			sql.append("   START WITH shop_id       = ? ");
			params.add(shopId);
			sql.append("   CONNECT BY prior shop_id = parent_shop_id ");
			sql.append("  ) ");
		}
		sql.append(" ORDER BY rc.code");

		String[] fieldNames = new String[] { "id", "code", };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, };

		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public void deleteEquipStatisticStaff(Long recordId, Long staffId, Long equipId) throws DataAccessException {
		if (null == recordId) {
			throw new IllegalArgumentException("recordId is null");
		}
//		if (null == staffId) {
//			throw new IllegalArgumentException("staffId is null");
//		}
		if (null == equipId) {
			throw new IllegalArgumentException("equipId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" delete from EQUIP_STATISTIC_STAFF where EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
		if (staffId != null) {
			sql.append(" and staff_id = ?");
			params.add(staffId);
		}
		sql.append(" and equip_id = ?");
		params.add(equipId);
		repo.executeSQLQuery(sql.toString(), params);
	}
	
	@Override
	public List<EquipmentVO> getListEquipStaffByStatisticFilter(EquipmentFilter<EquipmentVO> filter) throws DataAccessException {
		if (null == filter.getId()) {
			throw new IllegalArgumentException("filter.getId() is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select e.code equipCode, ");
		sql.append("       (select staff_code from staff where staff_id = est.staff_id) staffCode ");
		sql.append(" from EQUIP_STATISTIC_GROUP gr ");
		sql.append(" join equipment e on e.equip_id = gr.object_id ");
		sql.append(" left join EQUIP_STATISTIC_STAFF est on est.EQUIP_STATISTIC_RECORD_ID = gr.EQUIP_STATISTIC_RECORD_ID and est.equip_id = gr.object_id ");
		sql.append(" where gr.object_type = ? and gr.EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(EquipObjectType.EQUIP.getValue());
		params.add(filter.getId());
		if (!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			sql.append(" and e.code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getEquipGroupName())) {
			sql.append(" and e.equip_group_id in ( select equip_group_id from equip_group where upper(name_text) like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getEquipGroupName()).trim().toUpperCase()));
		}
		sql.append(" ORDER BY e.code");

		String[] fieldNames = new String[] { "equipCode", "staffCode", };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.STRING, };

		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public EquipStatisticStaff getEquipStatisticStaff(Long recordId, Long staffId, Long equipId) throws DataAccessException {
		if (null == recordId) {
			throw new IllegalArgumentException("recordId is null");
		}
//		if (null == staffId) {
//			throw new IllegalArgumentException("staffId is null");
//		}
		if (null == equipId) {
			throw new IllegalArgumentException("equipId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from EQUIP_STATISTIC_STAFF where EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
		if (staffId != null) {
			sql.append(" and staff_id = ?");
			params.add(staffId);
		}
		sql.append(" and equip_id = ?");
		params.add(equipId);
		return repo.getFirstBySQL(EquipStatisticStaff.class, sql.toString(), params);
	}
	
	@Override
	public Boolean isStaffBelongShop(long staffId, long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from shop  start with shop_id in (");
		sql.append(" select shop_id from ORG_ACCESS where status = ? and PERMISSION_ID in ( select PERMISSION_ID from ROLE_PERMISSION_MAP");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" where status = ? and role_id in (select role_id from role_user where USER_ID = ? and status = ?)");
		params.add(ActiveType.RUNNING.getValue());
		params.add(staffId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" )");
		sql.append(" )");
		sql.append(" CONNECT by prior shop_id = shop.PARENT_SHOP_ID");
		int hasPermissionData = repo.countBySQL(sql.toString(), params);
		int c = 0;
		sql = new StringBuilder();
		params = new ArrayList<Object>();
		if (hasPermissionData > 0) {
			sql.append(" select count(1) as count from shop where shop_id = ? start with shop_id in (");
			params.add(shopId);
			sql.append(" select shop_id from ORG_ACCESS where status = ? and PERMISSION_ID in ( select PERMISSION_ID from ROLE_PERMISSION_MAP");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" where status = ? and role_id in (select role_id from role_user where USER_ID = ? and status = ?)");
			params.add(ActiveType.RUNNING.getValue());
			params.add(staffId);
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" )");
			sql.append(" )");
			sql.append(" CONNECT by prior shop_id = shop.PARENT_SHOP_ID");
			c = repo.countBySQL(sql.toString(), params);
		} else {
			sql.append(" select count(*) as count from staff where staff_id = ? and shop_id = ?");
			params.add(staffId);
			params.add(shopId);
			c = repo.countBySQL(sql.toString(), params);
		}
		return c == 0 ? false : true;
	}
}

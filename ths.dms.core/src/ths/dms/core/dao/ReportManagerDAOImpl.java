package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Report;
import ths.dms.core.entities.ReportShopMap;
import ths.dms.core.entities.ReportUrl;
import ths.dms.core.entities.enumtype.DefaultType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ObjectReportType;
import ths.dms.core.entities.filter.ReportFilter;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class ReportManagerDAOImpl implements ReportManagerDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	/** --------------------------------Begin Quan ly cong van----------------------------- **/
	
	@Override
	public Report createReport(Report report) throws DataAccessException {
		// TODO Auto-generated method stub
		if (report == null) {
			throw new IllegalArgumentException("report");
		}
		return repo.create(report);
	}

	@Override
	public Report getReportById(Long id) throws DataAccessException {
		// TODO Auto-generated method stub
		if(id == null){
			throw new IllegalArgumentException("id report");
		}
		return repo.getEntityById(Report.class, id);
	}
	
	@Override
	public void updateReport(Report report) throws DataAccessException {
		// TODO Auto-generated method stub
		if(report == null){
			throw new IllegalArgumentException("report");
		}
		repo.update(report);
	}
	
	@Override
	public void deleteReport(Report report) throws DataAccessException {
		// TODO Auto-generated method stub
		if (report == null) {
			throw new IllegalArgumentException("report");
		}
		repo.delete(report);
	}
	
	@Override
	public Report getReportByCode(String code) throws DataAccessException {
		// TODO Auto-generated method stub
		if (code == null) {
			throw new IllegalArgumentException("report code");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from report where report_code = ?");
		params.add(code.toUpperCase());
		return repo.getEntityBySQL(Report.class, sql.toString(), params);
	}

	@Override
	public ReportUrl createReportUrl(ReportUrl reportUrl)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (reportUrl == null) {
			throw new IllegalArgumentException("reportUrl");
		}
		return repo.create(reportUrl);
	}

	@Override
	public ReportUrl getReportUrlById(Long id) throws DataAccessException {
		// TODO Auto-generated method stub
		if(id == null){
			throw new IllegalArgumentException("id reportUrl");
		}
		return repo.getEntityById(ReportUrl.class, id);
	}
	
	@Override
	public void updateReportUrl(ReportUrl reportUrl) throws DataAccessException {
		// TODO Auto-generated method stub
		if(reportUrl == null){
			throw new IllegalArgumentException("report");
		}
		repo.update(reportUrl);
	}
	
	@Override
	public void deleteReportUrl(ReportUrl reportUrl)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (reportUrl == null) {
			throw new IllegalArgumentException("reportUrl");
		}
		repo.delete(reportUrl);
	}
	// lay nay la theo VNM goc
	@Override
	public List<ReportUrl> getListReportUrlByReportId(Long reportId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (reportId == null) {
			throw new IllegalArgumentException("reportId");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ru.* from Report_url ru ");
		sql.append(" where 1 = 1 ");
		sql.append(" and ru.object_id = ? ");
		params.add(reportId);
		return repo.getListBySQL(ReportUrl.class, sql.toString(), params);
	}
	
	// lay nay la theo VNM goc
	@Override
	public List<ReportUrl> getListReportUrlByNPP(Long reportId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (reportId == null) {
			throw new IllegalArgumentException("reportId");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ru.* from Report_url ru ");
		sql.append(" JOIN report_shop_map rs ON rs.report_shop_map_id    = ru.object_id ");
		sql.append(" WHERE rs.report_id   =  ? ");
		params.add(reportId);
		sql.append(" and object_type = ? ");
		params.add(ObjectReportType.NPP.getValue());
		return repo.getListBySQL(ReportUrl.class, sql.toString(), params);
	}
		
	@Override
	public ReportShopMap createReportShopMap(ReportShopMap reportShopMap)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (reportShopMap == null) {
			throw new IllegalArgumentException("reportShopMap");
		}
		return repo.create(reportShopMap);
	}

	@Override
	public ReportShopMap getReportShopMapById(Long id)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if(id == null){
			throw new IllegalArgumentException("id ReportShopMap");
		}
		return repo.getEntityById(ReportShopMap.class, id);
	}
	@Override
	public ReportShopMap getReportShopMapByShopIdReportId(Long shopId, Long reportId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if(shopId == null){
			throw new IllegalArgumentException("id shopId");
		}
		if(reportId == null){
			throw new IllegalArgumentException("id reportId");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select rs.* from Report_shop_map rs ");
		sql.append(" where 1 = 1 ");
		sql.append(" and rs.shop_id = ? ");
		params.add(shopId);
		sql.append(" and rs.report_id = ? ");
		params.add(reportId);
		return repo.getEntityBySQL(ReportShopMap.class, sql.toString(), params);
	}
	
	@Override
	public void updateReportShopMap(ReportShopMap reportShopMap)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if(reportShopMap == null){
			throw new IllegalArgumentException("reportShopMap");
		}
		repo.update(reportShopMap);
	}
	
	@Override
	public void deleteReportShopMap(ReportShopMap reportShopMap)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (reportShopMap == null) {
			throw new IllegalArgumentException("reportShopMap");
		}
		repo.delete(reportShopMap);
	}
	
	@Override
	public List<ReportShopMap> getListReportShopMapByReportId(Long reportId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (reportId == null) {
			throw new IllegalArgumentException("reportId");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from report_shop_map where report_id = ?");
		params.add(reportId);
		return repo.getListBySQL(ReportShopMap.class, sql.toString(), params);
	}
	
	/**
	 * Danh sach cong van
	 * @author vuongmq
	 */
	@Override
	public List<Report> getListReport(KPaging<Report> kPaging, ReportFilter filter) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if(filter.getShopId() != null) {
			sql.append(" with shopTemp as  ");
			sql.append(" (  ");
			sql.append(" SELECT shop_id  ");
			sql.append(" FROM shop  ");
			sql.append("       START WITH shop_id       = ?  ");
			params.add(filter.getShopId());
			sql.append("    CONNECT BY PRIOR shop_id = parent_shop_id  ");
			sql.append(" )  ");
		}
		/*if(filter.getRoleType().getValue().equals(StaffObjectType.NHVNM)){
			sql.append(" select r.* from Report r ");
		}*/
		sql.append(" select r.* from Report r ");
		if(filter.getShopId() != null) {
			sql.append(" join report_shop_map rs on r.report_id = rs.report_id ");
			sql.append(" and rs.shop_id  in (select shop_id from shopTemp) ");
		}
		sql.append(" where 1 = 1 ");
		if(!StringUtility.isNullOrEmpty(filter.getReportCode())) {
			sql.append(" and upper(r.report_code) like ? escape '/' ");
			String s = filter.getReportCode().toUpperCase();
			s = Unicode2English.codau2khongdau(s);
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if(!StringUtility.isNullOrEmpty(filter.getReportName())) {
			sql.append(" and unicode2english(lower(r.report_name)) like ? escape '/' ");
			String s = filter.getReportName().toLowerCase();
			s = Unicode2English.codau2khongdau(s);
			s = Unicode2English.codau2khongdau(s);
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		sql.append(" order by r.report_code, r.report_name ");
		if (kPaging == null)
			return repo.getListBySQL(Report.class, sql.toString(),params);
		else
			return repo.getListBySQLPaginated(Report.class,sql.toString(), params, kPaging);
	}
	
	@Override
	public List<ReportUrl> getListReportUrl(KPaging<ReportUrl> kPaging, ReportFilter filter) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ru.* from Report_url ru ");
		if(filter.getTypeReport().equals(ObjectReportType.VNM)){
			sql.append(" where 1 = 1 ");
			if(filter.getReportId() != null) {
				sql.append(" and ru.object_id = ? ");
				params.add(filter.getReportId());
			}
			sql.append(" and ru.object_type = ? ");
			params.add(ObjectReportType.VNM.getValue());
		} else {
			if(filter.getShopId() != null) {
				sql.append(" join report_shop_map rs ON rs.report_shop_map_id = ru.object_id ");
				sql.append(" where rs.shop_id = ? ");
				params.add(filter.getShopId());
			}
			if(filter.getReportId() != null) {
				sql.append(" and rs.report_id = ? ");
				params.add(filter.getReportId());
			}
			sql.append(" and ru.object_type = ? ");
			params.add(ObjectReportType.NPP.getValue());
		}
		sql.append(" order by ru.url ");
		if (kPaging == null)
			return repo.getListBySQL(ReportUrl.class, sql.toString(),params);
		else
			return repo.getListBySQLPaginated(ReportUrl.class,sql.toString(), params, kPaging);
	}

	// end vuongmq
	@Override
	public ReportUrl getReportFileName(Long shopId, String maBC) throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from  ");
		sql.append(" (   ");
		sql.append("   select ru.* ");
		sql.append("   from report r ");
		sql.append("     join report_shop_map rsm on r.report_id = rsm.report_id ");
		sql.append("     join report_url ru on rsm.report_shop_map_id = ru.object_id ");
		sql.append("   where 1=1 ");
		sql.append("     and ru.object_type = 1 ");
		sql.append("     and ru.is_default = 1 ");
		sql.append(" 	 and upper(r.report_code) = ? ");
		params.add(maBC.trim().toUpperCase());
		sql.append("     and rsm.shop_id = ? ");
		params.add(shopId);
		    
		sql.append("   union all ");
		  
		sql.append("   select ru.* ");
		sql.append("   from report r ");
		sql.append("     join report_url ru on r.report_id = ru.object_id ");
		sql.append("   where 1=1 ");
		sql.append("     and ru.object_type = 0 ");
		sql.append("     and ru.is_default = 1 ");
		sql.append(" 	 and upper(r.report_code) = ? ");
		params.add(maBC.trim().toUpperCase());
		sql.append(" ) ");
		sql.append(" where rownum = 1 ");
		sql.append(" order by object_type desc ");
		
		return repo.getEntityBySQL(ReportUrl.class, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public ReportUrl getDefaultReportUrl(ReportFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select ru.* from report_url ru");
		if(ObjectReportType.VNM.equals(filter.getTypeReport())){
			sql.append(" where 1 = 1");
			if(filter.getReportId() != null) {
				sql.append(" and ru.object_id = ?");
				params.add(filter.getReportId());
			}
			sql.append(" and ru.object_type = ?");
			params.add(ObjectReportType.VNM.getValue());
		} else {
			if(filter.getShopId() != null) {
				sql.append(" join report_shop_map rs ON rs.report_shop_map_id = ru.object_id");
				sql.append(" where rs.shop_id = ?");
				params.add(filter.getShopId());
			}
			if(filter.getReportId() != null) {
				sql.append(" and rs.report_id = ?");
				params.add(filter.getReportId());
			}
			sql.append(" and ru.object_type = ?");
			params.add(ObjectReportType.NPP.getValue());
		}
		sql.append(" and ru.is_default = ?"); // lay file mac dinh
		params.add(DefaultType.DEFAULT.getValue());
		
		return repo.getEntityBySQL(ReportUrl.class, sql.toString(),params);
	}
}

package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.DisplayShopMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StDisplayProgram;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.RelationType;
import ths.dms.core.entities.filter.ImageFilter;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.exceptions.DataAccessException;

public interface DisplayShopMapDAO {

	DisplayShopMap createDisplayShopMap(DisplayShopMap displayShopMap,
			LogInfoVO logInfo) throws DataAccessException;

	void deleteDisplayShopMap(DisplayShopMap displayShopMap, LogInfoVO logInfo)
			throws DataAccessException;

	void updateDisplayShopMap(DisplayShopMap displayShopMap, LogInfoVO logInfo)
			throws DataAccessException;

	DisplayShopMap getDisplayShopMapById(long id) throws DataAccessException;

	Boolean checkIfRecordExist(long displayProgrameId, String shopCode,
			Long displayShopMapId) throws DataAccessException;

	List<DisplayShopMap> getListDisplayShopMapByDisplayProgramId(
			KPaging<DisplayShopMap> kPaging, Long displayProgramId,
			String shopCode, String shopName, ActiveType status,
			Long parentShopId) throws DataAccessException;

	Boolean createListDisplayShopMap(Long displayProgramId,
			List<Long> lstShopId, ActiveType status, LogInfoVO logInfo)
			throws DataAccessException;

	void createDisplayShopMapByLst(List<DisplayShopMap> lstDisplayShopMap, ActiveType status, LogInfoVO logInfo)
			throws DataAccessException;
	
	DisplayShopMap getDisplayShopMap(long displayProgramId, Long shopId,
			Long displayShopMapId) throws DataAccessException;

	List<DisplayShopMap> getListDisplayShopMapByDp(
			KPaging<DisplayShopMap> kPaging, String displayProgramCode,
			String displayProgramName, RelationType relation,
			ActiveType status, Date fromDate, Date toDate)
			throws DataAccessException;

	Boolean checkIfAncestorInDp(Long shopId, Long displayProgramId)
			throws DataAccessException;
	/*****************************	THONGNM ****************************************/
	/**
	 * Danh sach don vi trong CTTB khi tim kiem voi shopCode va shopName
	 * @author thongnm
	 */
	List<Shop> getListShopInDisplayProgram(Long displayProgramId,String shopCode, String shopName, 
			Boolean isJoin) throws DataAccessException;
	List<Shop> getListShopActiveInDisplayProgram(Long displayProgramId,String shopCode,
			String shopName) throws DataAccessException;
	
	List<ProgrameExtentVO> getListProgrameExtentVOEx(List<Shop> lstShop,
			Long displayProgramId) throws DataAccessException;

	List<DisplayShopMap> getListChildDisplayShopMap(Long shopId,Long displayProgramId, 
			Boolean exceptShopCheck)throws DataAccessException;
	/**
	 * Kiem tra ton tai don vi con tham gia trong CTTB
	 * @author thongnm
	 */
	
	Boolean checkExistChildDisplayShopMap(Long shopId, Long displayProgramId,
			Boolean exceptShopCheck) throws DataAccessException;
	/**
	 * Kiem tra ton tai don vi tham gia trong CTTB
	 * @author thongnm
	 */
	Boolean checkExistRecordDisplayShopMap(Long shopId, Long displayProgramId)throws DataAccessException;

	List<ImageVO> getListCTTB(ImageFilter filter) throws DataAccessException;

	List<ImageVO> getListCTTBByNPP(Long id) throws DataAccessException;

	List<ImageVO> getListCTTBByListShop(List<Long> lstShopId)
			throws DataAccessException;
	
	List<StDisplayProgram> getListDisplayProgramByShopAndYear(KPaging<StDisplayProgram> kPaging, Long shopID, Integer year,String code,String name)throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.StDisplayStaffMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface StDisplayStaffMapDAO {
	StDisplayStaffMap createDisplayStaffMap(StDisplayStaffMap displayStaffMap, LogInfoVO logInfo) throws DataAccessException;

	void deleteDisplayStaffMap(StDisplayStaffMap displayStaffMap, LogInfoVO logInfo) throws DataAccessException;

	void updateDisplayStaffMap(StDisplayStaffMap displayStaffMap, LogInfoVO logInfo) throws DataAccessException;
	
	StDisplayStaffMap getDisplayStaffMapById(long id) throws DataAccessException;

	Boolean isUsingByOthers(long displayStaffMapId) throws DataAccessException;

	Boolean createDisplayStaffMap(long displayProgramId, String shopCode, Long staffTypeId,
			String staffCode, LogInfoVO logInfo) throws DataAccessException;

	Boolean checkIfRecordExist(long displayProgramId, long staffId, Long id)
			throws DataAccessException;

	StDisplayStaffMap getDisplayStaffMapByIdForUpdate(long id)
			throws DataAccessException;

	List<StDisplayStaffMap> getListDisplayStaffMap(KPaging<StDisplayStaffMap> kPaging, Long displayProgramId,
			ActiveType status, List<Long> lstShopId, String month, boolean checkMap, long parentStaffId) throws DataAccessException;
	
	StDisplayStaffMap getDisplayStaffMap(Long dpId,Long staffId,Date month) throws DataAccessException;
	
	Integer checkShopOfStaffJoinDisplayProgram(Long displayProgramId,Long staffId) throws DataAccessException;
}

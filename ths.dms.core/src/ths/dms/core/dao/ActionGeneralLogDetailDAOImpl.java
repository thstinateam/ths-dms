package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ActionAuditDetail;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class ActionGeneralLogDetailDAOImpl implements ActionGeneralLogDetailDAO {

	@Autowired
	private IRepository repo;

	@Override
	public List<ActionAuditDetail> getListActionGeneralLogDetail(KPaging<ActionAuditDetail> kPaging, 
				Long actionGeneralLogId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select * from action_audit_detail where 1=1");
		if (actionGeneralLogId != null) {
			sql.append(" and action_audit_id = ?");
			params.add(actionGeneralLogId);
		}
		sql.append(" order by action_date desc, action_audit_detail_id");
		if (kPaging != null)
			return repo.getListBySQLPaginated(ActionAuditDetail.class, sql.toString(), params, kPaging);
		else
			return repo.getListBySQL(ActionAuditDetail.class, sql.toString(), params);
	}
	
	@Override
	public ActionAuditDetail createActionGeneralLogDetail(ActionAuditDetail actionGeneralLogDetail) throws DataAccessException {
		if (actionGeneralLogDetail == null) {
			throw new IllegalArgumentException("actionGeneralLogDetail");
		}
		repo.create(actionGeneralLogDetail);
		return repo.getEntityById(ActionAuditDetail.class, actionGeneralLogDetail.getId());
	}

	@Override
	public void deleteActionGeneralLogDetail(ActionAuditDetail actionGeneralLogDetail) throws DataAccessException {
		if (actionGeneralLogDetail == null) {
			throw new IllegalArgumentException("actionGeneralLogDetail");
		}
		repo.delete(actionGeneralLogDetail);
	}

	@Override
	public ActionAuditDetail getActionGeneralLogDetailById(long id) throws DataAccessException {
		return repo.getEntityById(ActionAuditDetail.class, id);
	}
	
	@Override
	public void updateActionGeneralLogDetail(ActionAuditDetail actionGeneralLogDetail) throws DataAccessException {
		if (actionGeneralLogDetail == null) {
			throw new IllegalArgumentException("actionGeneralLogDetail");
		}
		repo.update(actionGeneralLogDetail);
	}
}

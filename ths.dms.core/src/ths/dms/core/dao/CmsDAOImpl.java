package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.CmsFilter;
import ths.dms.core.entities.enumtype.PermissionType;
import ths.dms.core.entities.vo.CmsVO;
import ths.dms.core.entities.vo.RoleVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class CmsDAOImpl implements CmsDAO {

	@Autowired
	private IRepository repo;

	@Override
	public CmsVO getApplicationVOByCode(String code) throws DataAccessException {
		code = code.trim().toUpperCase();
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct app_id as appId, app_code as appCode, app_name as appName from application where status != -1 and app_code = ?");
		params.add(code);
		final String[] fieldNames = new String[] { "appId", "appCode", "appName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalarFirst(CmsVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<CmsVO> getListFormControlByRole(CmsFilter filter) throws DataAccessException {
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("getUserId is not null");
		}
		if (filter.getRoleId() == null) {
			throw new IllegalArgumentException("getRoleId is not null");
		}
		if (filter.getAppId() == null) {
			throw new IllegalArgumentException("getAppId is not null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with r1 as ( ");
		sql.append(" select distinct ");
		sql.append(" fr.form_id as formId ");
		sql.append(" ,fr.form_code as formCode ");
		sql.append(" ,fr.form_name as formName ");
		sql.append(" ,fr.url as url ");
		sql.append(" , NVL(fr.parent_form_id, 0) AS parentFormId ");
		sql.append(" , NVL(pm.is_full_privilege, 0) as fullPrivilege ");
		sql.append(" , (select NVL(control_id, 0) from control where control_id = fa.control_id) as controlId ");
		sql.append(" , (select control_code from control where control_id = fa.control_id) as controlCode ");
		sql.append(" , fa.show_status as status ");
		sql.append(" , fr.order_number as orderNumber ");
		sql.append(" , fr.type AS formType ");
		sql.append(" , NVL(fr.specific, 0) AS specific ");
		sql.append(" from function_access fa  ");
		sql.append(" join permission pm on fa.permission_id = pm.permission_id ");
		sql.append(" join form fr on fa.form_id = fr.form_id ");
		sql.append(" where fa.status = 1 and fr.subsystem = 1 and  pm.permission_id in ( ");
		sql.append("   select rpm.permission_id from role_permission_map rpm WHERE rpm.status = 1 and rpm.role_id in ( ");
		sql.append("     select role_id from role_user ru where ru.user_id = ? and ru.role_id = ? and ru.status = 1 ");
		params.add(filter.getUserId());
		params.add(filter.getRoleId());
		sql.append("   ) ");
		sql.append(" ) ");
		sql.append(" and pm.app_id = ? ");
		params.add(filter.getAppId());
		sql.append(" and pm.status = 1 and fa.status = 1 and fr.status = 1 AND pm.permission_type = ? ");
		params.add(PermissionType.FUNCTION.getValue());
		sql.append(" ) ");
		sql.append(" SELECT  formId, formCode, formName, formType, url, max(fullPrivilege) as fullPrivilege,  parentFormId,  controlId,  controlCode,  status,  orderNumber, specific");
		sql.append(" FROM r1 ");
		sql.append(" group by formId, formCode, formType, formName, url,  parentFormId,  controlId,  controlCode,  status,  orderNumber, specific");
		sql.append(" ORDER BY orderNumber, formCode");
		
		final String[] fieldNames = new String[] { "formId", "formCode", "formName", "formType", "url", "parentFormId", "fullPrivilege", "controlId", "controlCode", "status", "orderNumber", "specific" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };

		return repo.getListByQueryAndScalar(CmsVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<CmsVO> getListFormInRoleByFilter(CmsFilter filter) throws DataAccessException {
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("getUserId is not null");
		}
		if (filter.getRoleId() == null) {
			throw new IllegalArgumentException("getRoleId is not null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with r1 as ( select fa.form_id from function_access fa where fa.status = 1 ");
		sql.append(" 		  and fa.permission_id in (select permission_id from permission where status = 1 ");
		if (filter.getType() != null) {
			sql.append(" and permission_type = ? ");
			params.add(filter.getType());
		} else {
			sql.append(" and permission_type in (?, ?) ");
			params.add(PermissionType.FUNCTION.getValue());
			params.add(PermissionType.REPORT.getValue());
		}
		sql.append(" 		  and permission_id in (select prm.permission_id from role_permission_map prm where prm.status = 1 and prm.role_id in (  ");
		sql.append(" 		  select ru.role_id from role_user ru where ru.status = 1   ");
		if (filter.getRoleId() != null) {
			sql.append(" 		  and ru.role_id = ?   ");
			params.add(filter.getRoleId());
		}
		sql.append(" 		  and ru.user_id = ? ))))  ");
		params.add(filter.getUserId());
		sql.append(" select  ");
		sql.append(" fr.form_id as formId, fr.form_code as formCode, fr.form_name as formName, NVL(fr.parent_form_id, 0) as parentFormId ");
		sql.append(" ,fr.type as type, fr.url as url ");
		sql.append(" from form fr ");
		sql.append(" where fr.status = 1 and fr.subsystem = 1 ");
		if (filter.getAppId() != null) {
			sql.append("  and fr.app_id = ? ");
			params.add(filter.getAppId());
		}
		sql.append(" and  fr.form_id in (select * from r1) ");
		if (filter.getParentId() != null) {
			sql.append(" start with fr.form_id = ? and fr.status = 1  connect by prior fr.form_id = fr.parent_form_id ");
			params.add(filter.getParentId());
		}
		sql.append(" order by parentFormId, fr.order_number, fr.form_code ");

		final String[] fieldNames = new String[] { "formId", "formCode", "formName", "parentFormId", "type", "url" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(CmsVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<CmsVO> getListFormInRoleForTreeByFilter(CmsFilter filter) throws DataAccessException {
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("getUserId is not null");
		}
		if (filter.getRoleId() == null) {
			throw new IllegalArgumentException("getRoleId is not null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with r1 as ( select fa.form_id from function_access fa where fa.status = 1 ");
		sql.append(" 		  and fa.permission_id in (select permission_id from permission where status = 1 ");
		if (filter.getType() != null) {
			sql.append(" and permission_type = ? ");
			params.add(filter.getType());
		} else {
			sql.append(" and permission_type in (?, ?) ");
			params.add(PermissionType.FUNCTION.getValue());
			params.add(PermissionType.REPORT.getValue());
		}
		sql.append(" 		  and permission_id in (select prm.permission_id from role_permission_map prm where prm.status = 1 and prm.role_id in (  ");
		sql.append(" 		  select ru.role_id from role_user ru where ru.status = 1   ");
		if (filter.getRoleId() != null) {
			sql.append(" 		  and ru.role_id = ?   ");
			params.add(filter.getRoleId());
		}
		sql.append(" 		  and ru.user_id = ? )))),  ");
		params.add(filter.getUserId());

		sql.append(" r2 as ( select fa.form_id from function_access fa where fa.status = 1 ");
		sql.append(" 		  and fa.permission_id in (select permission_id from permission where status = 1 ");
		if (filter.getType() != null) {
			sql.append(" and permission_type <> ? ");
			params.add(filter.getType());
		} else {
			sql.append(" and permission_type in (?, ?) ");
			params.add(PermissionType.FUNCTION.getValue());
			params.add(PermissionType.REPORT.getValue());
		}
		sql.append(" 		  and permission_id in (select prm.permission_id from role_permission_map prm where prm.status = 1 and prm.role_id in (  ");
		sql.append(" 		  select ru.role_id from role_user ru where ru.status = 1   ");
		if (filter.getRoleId() != null) {
			sql.append(" 		  and ru.role_id = ?   ");
			params.add(filter.getRoleId());
		}
		sql.append(" 		  and ru.user_id = ? ))))  ");
		params.add(filter.getUserId());
		
		sql.append(" select distinct   ");
		sql.append(" formId, formCode, formName, parentFormId , type, url, order_number as orderNumber,  ");
		sql.append(" max(isLevel) as isLevel  ");
		sql.append(" from (  ");
		sql.append(" SELECT fr.form_id           AS formId,  ");
		sql.append("   fr.form_code              AS formCode,  ");
		sql.append("   fr.form_name              AS formName,  ");
		sql.append("   NVL(fr.parent_form_id, 0) AS parentFormId ,  ");
		sql.append("   fr.type                   AS type,  ");
		sql.append("   fr.url                    AS url,  ");
		sql.append("   fr.order_number,  ");
		sql.append("   level as isLevel  ");
		sql.append(" FROM  form fr  ");
		sql.append(" WHERE fr.status  = 1  ");
		sql.append(" AND fr.subsystem = 1  ");
		if (filter.getAppId() != null) {
			sql.append("  and fr.app_id = ? ");
			params.add(filter.getAppId());
		}
		sql.append(" and fr.form_id not in (SELECT * FROM r2) ");
		if (filter.getParentId() != null) {
			sql.append(" start with fr.form_id = ? and fr.status = 1  connect by prior fr.form_id = fr.parent_form_id) ");
			params.add(filter.getParentId());
		} else {
			sql.append(" start with fr.form_id  IN (SELECT * FROM r1) and fr.form_id not in (SELECT * FROM r2) connect by fr.form_id = prior fr.parent_form_id )  ");
		}
		sql.append(" group by formId, formCode, formName, parentFormId , type, url, order_number  ");
		if (filter.getParentId() != null) {
			sql.append(" ORDER BY isLevel, order_number, formCode  ");
		} else {
			sql.append(" ORDER BY isLevel desc, order_number, formCode  ");
		}
		final String[] fieldNames = new String[] { "formId", "formCode", "formName", "parentFormId", "type", "url", "orderNumber" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };

		return repo.getListByQueryAndScalar(CmsVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<CmsVO> getListUrl(CmsFilter filter) throws DataAccessException {
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("UserId is not null");
		}
		if (filter.getAppId() == null) {
			throw new IllegalArgumentException("AppCode is not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select url from form where status = 1 and app_id = ? and url is not null and subsystem = 1 ");
		params.add(filter.getAppId());
		sql.append(" and form_id in (");
		sql.append("   select form_id from function_access fa where fa.status = 1 and fa.control_id is null");
		sql.append("   and fa.permission_id in (");
		sql.append("     select rpm.permission_id from role_permission_map rpm where rpm.status = 1");
		sql.append("     and rpm.role_id in (");
		sql.append("       SELECT ru.role_id FROM role_user ru where status =1 and ru.user_id = ? ");
		params.add(filter.getUserId());
		if (filter.getRoleId() != null) {
			sql.append(" and ru.role_id = ? ");
			params.add(filter.getRoleId());
		}
		params.add(filter.getRoleId());
		sql.append("     )))");

		final String[] fieldNames = new String[] { "url" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(CmsVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ShopVO> getListOrgAccess(CmsFilter filter) throws DataAccessException {
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("UserId is not null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select s.shop_id as shopId, s.shop_code as shopCode, s.shop_name as shopName, s.parent_shop_id as parentId ");
		sql.append(" , (select cn.object_type from channel_type cn where cn.status = 1 and cn.channel_type_id = s.shop_type_id) as objectType ");
		sql.append(" , decode_islevel_vnm((select cn.object_type from channel_type cn where cn.status = 1 and cn.channel_type_id = s.shop_type_id)) as isLevel ");
		sql.append(" from shop s ");
		sql.append(" where s.status = 1 and s.shop_id in ( ");
		sql.append("   select org.shop_id from org_access org where org.status = 1 ");
		sql.append("   and org.permission_id in ( ");
		sql.append("     select rpm.permission_id from role_permission_map rpm where rpm.status = 1 ");
		sql.append("     and rpm.role_id in ( ");
		sql.append("       select ru.role_id from role_user ru where ru.status = 1 ");
		sql.append("       and ru.user_id = ?  ");
		params.add(filter.getUserId());
		if (filter.getRoleId() != null) {
			sql.append("       and ru.role_id = ?  ");
			params.add(filter.getRoleId());
		}
		sql.append("     ))) ");
		sql.append(" order by isLevel asc ");

		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "parentId", "isLevel", "objectType" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ShopVO> getListShopInOrgAccess(CmsFilter filter) throws DataAccessException {
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("UserId is not null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select s.shop_id as shopId, s.shop_code as shopCode, s.shop_name as shopName, s.parent_shop_id as parentId ");
		sql.append(" , (select object_type from channel_type where status = 1 and channel_type_id = s.shop_type_id) as objectType ");
		sql.append(" , decode_islevel_vnm((select cn.object_type from channel_type cn where cn.status = 1 and cn.channel_type_id = shop_type_id)) as isLevel ");
		sql.append(" from shop s ");
		sql.append(" where s.status = 1 and s.shop_id in ( ");
		sql.append("   select org.shop_id from org_access org where org.status = 1 ");
		sql.append("   and org.permission_id in ( ");
		sql.append("     select rpm.permission_id from role_permission_map rpm where rpm.status = 1 ");
		sql.append("     and rpm.role_id in ( ");
		sql.append("       select ru.role_id from role_user ru where ru.status = 1 ");
		sql.append("       and ru.user_id = ?  ");
		params.add(filter.getUserId());
		if (filter.getRoleId() != null) {
			sql.append("       and ru.role_id = ?  ");
			params.add(filter.getRoleId());
		}
		sql.append("     ))) ");
		sql.append(" order by s.shop_code asc ");

		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "parentId", "objectType", "isLevel" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ShopVO> getFullShopInOrgAccessByFilter(CmsFilter filter) throws DataAccessException {
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("UserId is not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select distinct shop_id as shopId, shop_code as shopCode, shop_name as shopName, parent_shop_id as parentId, shop_channel as shopChannel ");
		sql.append(" , decode_islevel_vnm((select cn.object_type from channel_type cn where cn.status = 1 and cn.channel_type_id = shop_type_id)) as isLevel ");
		sql.append(" , (select object_type from channel_type where status = 1 and channel_type_id = shop_type_id) as objectType ");
		sql.append(" from shop where status = 1 ");
		if (filter.getLstId() != null) {
			sql.append(" start with shop_id in (-1 ");
			for (Long shopId : filter.getLstId()) {
				sql.append(" ,?  ");
				params.add(shopId);
			}
			sql.append(" ) ");
		} else {
			sql.append(" start with shop_id in ( ");
			sql.append("   select org.shop_id from org_access org where org.status = 1 and org.permission_id in ( ");
			sql.append("     select rpm.permission_id from role_permission_map rpm where rpm.status = 1 and rpm.role_id ");
			sql.append("     in (select ru.role_id from role_user ru where ru.status = 1 and ru.user_id = ? ");
			params.add(filter.getUserId());
			if (filter.getRoleId() != null) {
				sql.append(" and ru.role_id = ?  ");
				params.add(filter.getRoleId());
			}
			sql.append(" ))) ");
		}
		sql.append(" and status = 1 connect by prior shop_id = parent_shop_id ");
		sql.append(" order by isLevel, shop_code asc ");

		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "parentId", "shopChannel", "isLevel", "objectType" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}//thangok

	@Override
	public List<CmsVO> getListShopInRoleByUser(CmsFilter filter) throws DataAccessException {
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("UserId is not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select s.shop_id as shopId, s.shop_code as shopCode, s.shop_name as shopName  ");
		sql.append(" from role r join role_permission_map rp on r.role_id = rp.role_id ");
		sql.append(" join permission p on rp.permission_id = p.permission_id  ");
		sql.append(" join org_access org on p.permission_id = org.permission_id ");
		sql.append(" join shop s on org.shop_id = s.shop_id ");
		sql.append(" where  1 = 1 ");
		if (filter.getRoleId() == null) {
			sql.append(" and r.role_id = ? ");
			params.add(filter.getRoleId());
			sql.append(" and r.role_id in (select role_id from role_user where user_id = ? and role_id = ? and status = ?) ");
			params.add(filter.getUserId());
			params.add(filter.getRoleId());
			params.add(ActiveType.RUNNING.getValue());
		}
		if (filter.getLstId() != null && filter.getLstId().size() > 0) {
			sql.append(" and s.shop_id in (-1 ");
			for (Long id : filter.getLstId()) {
				sql.append(" ,? ");
				params.add(id);
			}
			sql.append(" ) ");
		}
		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(CmsVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<CmsVO> getListRoleByUser(CmsFilter filter) throws DataAccessException {
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("UserId is not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT r.role_id AS roleId,  ");
		sql.append("   r.role_code    AS roleCode,  ");
		sql.append("   r.role_name    AS roleName,  ");
		sql.append("   (case when ru.inherit_user_priv is null then ? else ru.inherit_user_priv end) as inheritUserPriv  ");
		params.add(filter.getUserId());
		sql.append(" FROM role r join role_user ru on r.role_id = ru.role_id  ");
		sql.append(" WHERE 1        = 1  ");
		sql.append(" AND r.status   = 1  ");
		sql.append(" AND ru.user_id = ?  ");
		params.add(filter.getUserId());
		if (filter.getAppId() != null && filter.getAppId() > 0) {
			sql.append(" and r.app_id = ?  ");
			params.add(filter.getAppId());
		}
		
		final String[] fieldNames = new String[] { "roleId", "roleCode", "roleName", "inheritUserPriv"};

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		return repo.getListByQueryAndScalar(CmsVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ShopVO> getListShopByRole(CmsFilter filter) throws DataAccessException {
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("UserId is not null");
		}
		if (filter.getRoleId() == null) {
			throw new IllegalArgumentException("AppCode is not null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select s.shop_id as shopId, s.shop_code as shopCode, s.shop_name as shopName, s.parent_shop_id as parentId ");
		sql.append(" , decode_islevel_vnm((select cn.object_type from channel_type cn where cn.status = 1 and cn.channel_type_id = s.shop_type_id)) as isLevel ");
		sql.append(" from shop s ");
		sql.append(" where s.status = 1 and s.shop_id in ( ");
		sql.append("   select org.shop_id from org_access org where org.status = 1 ");
		sql.append("   and org.permission_id in ( ");
		sql.append("     select rpm.permission_id from role_permission_map rpm where rpm.status = 1 ");
		sql.append("     and rpm.role_id in ( ");
		sql.append("       select ru.role_id from role_user ru where ru.status = 1 ");
		sql.append("       and ru.user_id = ?  ");
		params.add(filter.getUserId());
		sql.append("       and ru.role_id = ? ");
		params.add(filter.getRoleId());
		sql.append("     ))) ");
		sql.append(" order by isLevel, shopCode asc ");

		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "parentId", "isLevel" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ShopVO> getListShopByListParentId(CmsFilter filter) throws DataAccessException {
		if (filter.getLstId() == null) {
			throw new IllegalArgumentException("getLstId is not null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select distinct s.shop_id as shopId, s.shop_code as shopCode , s.shop_name as shopName, s.parent_shop_id as parentId ");
		sql.append(" from shop s ");
		sql.append(" where s.status = 1 ");
		sql.append(" start with s.shop_id in (-1");
		for (Long id : filter.getLstId()) {
			sql.append(" ,? ");
			params.add(id);
		}
		sql.append(" )");
		sql.append(" connect by prior shop_id = s.parent_shop_id ");
		sql.append(" order by shopName ");

		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "parentId" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<StaffVO> getListStaffByUserWithRole(CmsFilter filter) throws DataAccessException {
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("UserId is not null");
		}
		/**
		 * Nghiep vu dac biet, khong thay doi hay set lai cac apparam trong ham
		 * nay @author hunglm16 @since September 24,2014
		 **/
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select  ");
		sql.append(" psm.parent_staff_map_id as parentStaffMapId ");
		sql.append(" ,psm.staff_id as staffId ");
		sql.append(" ,st.staff_code as staffCode ");
		sql.append(" ,st.staff_name as staffName ");
		sql.append(" ,psm.parent_staff_id as parentStaffId ");
		sql.append(" ,st.shop_id as shopId ");
		sql.append(" ,(select object_type from channel_type where status = 1 and channel_type_id = st.staff_type_id) as objectType ");
		sql.append(" ,level as isLevel ");
		sql.append(" from parent_staff_map psm join staff st on psm.staff_id = st.staff_id ");
		sql.append(" where psm.status = 1 and st.status = 1 ");
		sql.append(" and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and parent_staff_id = ?) ");
		params.add(filter.getUserId());
		sql.append(" start with psm.parent_staff_id = ? and psm.status = 1 connect by prior psm.staff_id = psm.parent_staff_id ");
		params.add(filter.getUserId());

		final String[] fieldNames = new String[] { "parentStaffMapId", "staffId", "staffCode", "staffName", "parentStaffId", "shopId", "objectType", "isLevel" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public int checkShopInCMSByFilter (CmsFilter filter) throws DataAccessException {
		if (filter == null || filter.getShopId() == null || (filter.getShopRootId() == null && filter.getStaffRootId() == null)) {
			return 0;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (filter.getShopRootId() != null) {
			sql.append(" select count (*) as countShop from shop where status = 1 ");
			sql.append(" and shop_id = ? ");
			params.add(filter.getShopId());
			sql.append(" start with shop_id = ? and status = 1 connect by prior shop_id = parent_shop_id ");
			params.add(filter.getShopRootId());
		} else if (filter.getStaffRootId() != null) {
			sql.append(" with  rOrgAccess as ( ");
			sql.append("   select shop_id from role_user ru join role_permission_map pm on ru.role_id = pm.role_id join org_access org on pm.permission_id = org.permission_id ");
			sql.append("   where ru.status = 1 and pm.status = 1 and org.status = 1 and ru.user_id = ? ) ");
			params.add(filter.getStaffRootId());
			sql.append(" select count (*) as countShop  ");
			sql.append(" from shop sh ");
			sql.append(" where sh.status = 1 ");
			sql.append(" and sh.shop_id = ? ");
			params.add(filter.getShopId());
			sql.append(" start with sh.shop_id in (select * from rOrgAccess) and sh.status = 1 connect by prior sh.shop_id= sh.parent_shop_id ");
		} else {
			return 0;
		}
		BigDecimal kq = (BigDecimal) repo.getObjectByQuery(sql.toString(), params);
		if (kq != null) {
			return kq.intValue();
		}
		return 0;
	}
	
	@Override
	public List<RoleVO> getListOwnerRoleShopVOByStaff(Long staffId) throws DataAccessException {

		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT RE.ROLE_ID AS roleId, RE.ROLE_CODE AS roleCode, RE.ROLE_NAME AS roleName, RUR.INHERIT_USER_PRIV AS inheritUserId, ");
		sql.append(" 		SF.SHOP_ID AS inheritStaffShopId, STE.SPECIFIC_TYPE AS inheritStaffSpecificType, STE.NAME AS inheritStaffTypeName ");
		sql.append(" FROM ROLE RE, ROLE_USER RUR, STAFF SF, STAFF_TYPE STE ");
		sql.append(" WHERE RUR.STATUS = 1 ");
		sql.append(" AND RE.STATUS = 1 ");
		sql.append(" AND RUR.USER_ID = ? ");
		sql.append(" AND RE.ROLE_ID = RUR.ROLE_ID ");
		sql.append(" AND SF.STAFF_ID = RUR.INHERIT_USER_PRIV ");
		sql.append(" AND SF.STAFF_TYPE_ID = STE.STAFF_TYPE_ID ");

		params.add(staffId);
		final String[] fieldNames = new String[] { "roleId", "roleCode", "roleName", "inheritUserId", "inheritStaffShopId", "inheritStaffSpecificType", "inheritStaffTypeName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG,
				StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING };
		List<RoleVO> roleVOs = new ArrayList<RoleVO>();

		roleVOs = repo.getListByQueryAndScalar(RoleVO.class, fieldNames, fieldTypes, sql.toString(), params);

		for (RoleVO roleVO : roleVOs) {
			roleVO.setListShop(this.getListOwnerShopVOByRole(roleVO.getRoleId(), staffId));
		}

		return roleVOs;
	}
	
	@Override
	public List<ShopVO> getListOwnerShopVOByRole(Long roleId, Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT sh.shop_id AS shopId, ");
		sql.append(" sh.shop_name AS shopName, ");
		sql.append(" sh.shop_code AS shopCode, ");
		sql.append(" sh.address AS address, ");
		sql.append(" sh.lng AS lng, ");
		sql.append(" sh.lat AS lat, ");
		sql.append(" sh.parent_shop_id parentId, ");
		sql.append(" (select specific_type from shop_type where shop_type_id = sh.shop_type_id) objectType ");
		sql.append(" FROM shop sh ");
		sql.append(" WHERE sh.shop_id IN ");
		sql.append(" (SELECT SHOP_ID ");
		sql.append(" FROM ORG_ACCESS OAS ");
		sql.append(" JOIN ROLE_PERMISSION_MAP RPMP ");
		sql.append(" ON OAS.PERMISSION_ID = RPMP.PERMISSION_ID ");
		sql.append(" WHERE OAS.STATUS = 1 ");
		sql.append(" AND RPMP.STATUS = 1 ");
		sql.append(" AND RPMP.ROLE_ID = ? ");
		params.add(roleId);
		sql.append(" MINUS ");
		
		sql.append(" SELECT SHOP_ID ");
		sql.append(" FROM ORG_ACCESS_EXCEPT OAS ");
		sql.append(" JOIN ROLE_PERMISSION_MAP RPMP ");
		sql.append(" ON OAS.PERMISSION_ID = RPMP.PERMISSION_ID ");
		sql.append(" WHERE OAS.STATUS = 1 ");
		sql.append(" AND RPMP.STATUS = 1 ");
		sql.append(" AND RPMP.ROLE_ID = ? ");
		params.add(roleId);
		sql.append(" ) ");
		sql.append(" AND sh.status = 1 ");

		final String[] fieldNames = new String[] { "shopId", "shopName", "shopCode", "address", "lng", "lat", "parentId", "objectType" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };

		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<ShopVO> getListShopVOByStaffVO(CmsFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH ORG_TEMP AS ");
		sql.append(" 	(SELECT IS_MANAGE ");
		sql.append(" 	FROM ORGANIZATION ");
		sql.append(" 	WHERE ORGANIZATION_ID = (SELECT ORGANIZATION_ID FROM STAFF WHERE STAFF_ID = ?) ");
		params.add(filter.getStaffRootId());
		sql.append(" 	) ");
		sql.append(" SELECT sh.shop_id AS shopId, ");
		sql.append(" sh.shop_name AS shopName, ");
		sql.append(" sh.shop_code AS shopCode, ");
		sql.append(" sh.address AS address, ");
		sql.append(" sh.lng AS lng, ");
		sql.append(" sh.lat AS lat, ");
		sql.append(" sh.parent_shop_id parentId, ");
		sql.append(" (select specific_type from shop_type where shop_type_id = sh.shop_type_id) objectType ");
		sql.append(" FROM SHOP sh ");
		sql.append(" WHERE STATUS = 1 ");
		sql.append(" AND ( ");
		sql.append(" 		1 = (SELECT IS_MANAGE FROM ORG_TEMP) AND sh.SHOP_ID = ? ");
		params.add(filter.getShopId());
		sql.append(" 		OR ");
		sql.append(" 		1 <> (SELECT IS_MANAGE FROM ORG_TEMP) AND sh.SHOP_ID IN (SELECT SHOP_ID FROM SHOP S_TEMP WHERE S_TEMP.STATUS = 1 AND S_TEMP.PARENT_SHOP_ID = ?) ");
		params.add(filter.getShopId());
		sql.append(" 	 ) ");
		
		final String[] fieldNames = new String[] { "shopId", "shopName", "shopCode", "address", "lng", "lat", "parentId", "objectType" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT,StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		List<ShopVO> shopVOs = new ArrayList<ShopVO>();

		shopVOs = repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		
		if (shopVOs != null && shopVOs.size() > 0) {
			return shopVOs;
		}
		
		sql = new StringBuilder();
		params.clear();
		
		sql.append(" SELECT sh.shop_id AS shopId, ");
		sql.append(" sh.shop_name AS shopName, ");
		sql.append(" sh.shop_code AS shopCode, ");
		sql.append(" sh.address AS address, ");
		sql.append(" sh.lng AS lng, ");
		sql.append(" sh.lat AS lat, ");
		sql.append(" sh.parent_shop_id parentId, ");
		sql.append(" (select specific_type from shop_type where shop_type_id = sh.shop_type_id) objectType ");
		sql.append(" FROM SHOP sh ");
		sql.append(" WHERE STATUS = 1 ");
		sql.append(" AND shop_id = ? ");
		params.add(filter.getShopId());

		shopVOs = repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		
		return shopVOs;
	}
	
	@Override
	public List<CmsVO> getListChildStaffInherit(CmsFilter filter) throws DataAccessException {
		if (filter.getStaffRootId() == null || filter.getRoleId() == null || filter.getShopRootId() == null) {
			throw new IllegalArgumentException("filter is not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select number_key_id as staffId from table (F_GET_LIST_CHILD_STAFF_INHERIT(?,sysdate,?,?,null))");
		params.add(filter.getStaffRootId());
		params.add(filter.getRoleId());
		params.add(filter.getShopRootId());
		final String[] fieldNames = new String[] { "staffId" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG };
		return repo.getListByQueryAndScalar(CmsVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<CmsVO> getListChildShopInherit(CmsFilter filter) throws DataAccessException {
		if (filter.getStaffRootId() == null || filter.getRoleId() == null || filter.getShopRootId() == null) {
			throw new IllegalArgumentException("filter is not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select number_key_id as shopId from table (F_GET_LIST_CHILD_SHOP_INHERIT(?,sysdate,?,?)) ");
		params.add(filter.getStaffRootId());
		params.add(filter.getRoleId());
		params.add(filter.getShopRootId());
		final String[] fieldNames = new String[] { "shopId" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG };
		return repo.getListByQueryAndScalar(CmsVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<ShopVO> getFullShopOwnerByFilter(CmsFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with lstId as (select number_key_id as shopId from table (F_GET_LIST_CHILD_SHOP_INHERIT(?,sysdate,?,?))) ");
		params.add(filter.getStaffRootId());
		params.add(filter.getRoleId());
		params.add(filter.getShopRootId());
		sql.append(" select distinct shop_id as shopId, shop_code as shopCode, shop_name as shopName, parent_shop_id as parentId, ");
		sql.append(" (select specific_type from shop_type where shop_type_id = s.shop_type_id) objectType ");
		sql.append(" from shop s where s.shop_id in (select shopId from lstId) ");

		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "parentId", "objectType" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public CmsVO getFormCmsVoByUrl(String url, Integer permissionType, Long roleId, Long userId, Long inheritStaffId) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(url)) {
			throw new IllegalArgumentException("url is null");
		}
		if (roleId == null) {
			throw new IllegalArgumentException("roleId is null");
		}
		if (userId == null) {
			throw new IllegalArgumentException("userId is null");
		}
		if (inheritStaffId == null) {
			throw new IllegalArgumentException("inheritStaffId is null");
		}
		if (permissionType == null) {
			throw new IllegalArgumentException("permissionType is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ps.permission_id permissionId, f.form_id formId, f.form_code as formCode, f.form_name as formName ");
		sql.append(" from role_user ru  ");
		sql.append(" join role r on r.role_id = ru.role_id and r.status = 1 ");
		sql.append(" join role_permission_map rpm on r.role_id = rpm.role_id and rpm.status = 1 ");
		sql.append(" join permission ps on ps.permission_id = rpm.permission_id and ps.status = 1 ");
		sql.append(" join function_access fa on fa.permission_id = ps.permission_id and fa.status = 1 and fa.control_id is null ");
		sql.append(" join form f on fa.form_id = f.form_id and f.status = 1 and f.url is not null ");
		sql.append(" where ru.status = 1  ");
		sql.append(" and ru.role_id = ?  ");
		params.add(roleId);
		sql.append(" and ru.user_id = ? ");
		params.add(userId);
		sql.append(" and ps.permission_type = ? ");
		params.add(permissionType);
		sql.append(" and f.url = ? ");
		sql.append(" and f.TYPE in (1, 2) ");
		params.add(url);
		
		final String[] fieldNames = new String[] { "permissionId", "formId", "formCode", "formName"};

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING};
		return repo.getListByQueryAndScalarFirst(CmsVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
}

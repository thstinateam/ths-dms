package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.SaleOrderPromoLot;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface SaleOrderLotDAO {

	SaleOrderLot createSaleOrderLot(SaleOrderLot salesOrderLot) throws DataAccessException;

	void createSaleOrderLot(List<SaleOrderLot> lstSaleOrderLot) throws DataAccessException;

	/**
	 * Tao sale_order_promo_lot cho don hang
	 * @author tungmt
	 * @version 1.0
	 * @param filter
	 * @return Luu thong tin promolot
	 * @exception BusinessException
	 * @see
	 * @since 18/8/2015
	 * @serial
	 */
	void createSaleOrderPromoLot(List<SaleOrderPromoLot> lstSaleOrderPromoLot) throws DataAccessException;

	void deleteSaleOrderLot(SaleOrderLot salesOrderLot) throws DataAccessException;

	void updateSaleOrderLot(SaleOrderLot salesOrderLot) throws DataAccessException;

	SaleOrderLot getSaleOrderLotById(long id) throws DataAccessException;

	List<SaleOrderLot> getListSaleOrderLotBySaleOrderDetailId(long saleOrderDetailId) throws DataAccessException;

	/**
	 * @author tungmt
	 * @version
	 * @param saleOrderId - lay saleOrderpromolot theo orderId
	 * @return danh sach saleOrderpromolot
	 * @exception DataAccessException
	 * @see
	 * @since 8/8/2015
	 * @serial
	 */
	List<SaleOrderPromoLot> getListSaleOrderPromoLotBySaleOrderId(long saleOrderId) throws DataAccessException;
	
	/**
	 * @author tungmt
	 * @version
	 * @param lstDetail - lay saleOrderpromolot theo lst saleOrderDetail
	 * @return danh sach saleOrderpromolot
	 * @exception DataAccessException
	 * @see
	 * @since 8/8/2015
	 * @serial
	 */
	List<SaleOrderPromoLot> getListSaleOrderPromoLotByListSaleOrderDetailId(List<SaleOrderDetail> lstDetail) throws DataAccessException;

	List<SaleOrderLot> getListSaleOrderLotBySaleOrderDetail(KPaging<SaleOrderLot> kPaging, long saleOrderDetailId) throws DataAccessException;

	/**
	 * Gets the sale order lot by condition.
	 * 
	 * @author phut
	 * @param saleOrderId
	 *            the sale order id
	 * @param saleOrderDetailId
	 *            the sale order detail id
	 * @param lot
	 *            the lot
	 * @return the sale order lot by condition
	 * @throws DataAccessException
	 *             the data access exception
	 */
	Integer sumQuatitySaleOrderLotByCondition(Long saleOrderId, Long saleOrderDetailId, String lot) throws DataAccessException;

	Boolean checkProductExceptLotExpire(Long productId, Long shopId, Integer quant) throws DataAccessException;

}

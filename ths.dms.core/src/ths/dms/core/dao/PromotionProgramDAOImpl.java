package ths.dms.core.dao;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.GroupLevel;
import ths.dms.core.entities.GroupLevelDetail;
import ths.dms.core.entities.GroupMapping;
import ths.dms.core.entities.PPConvert;
import ths.dms.core.entities.PPConvertDetail;
import ths.dms.core.entities.Price;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductGroup;
import ths.dms.core.entities.PromotionCustomerMap;
import ths.dms.core.entities.PromotionProductOpen;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.PromotionStaffMap;
import ths.dms.core.entities.RptAccumulativePromotionProgram;
import ths.dms.core.entities.RptAccumulativePromotionProgramDetail;
import ths.dms.core.entities.RptCTTLPay;
import ths.dms.core.entities.RptCTTLPayDetail;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.SaleOrderPromotion;
import ths.dms.core.entities.StockTotal;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.CommercialSupportType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.ProductGroupType;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.PromotionCustomerMapType;
import ths.dms.core.entities.enumtype.PromotionProgramFilter;
import ths.dms.core.entities.enumtype.PromotionType;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.SpParam;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.ValueType;
import ths.dms.core.entities.enumtype.WarehouseType;
import ths.dms.core.entities.filter.CalcPromotionFilter;
import ths.dms.core.entities.filter.CalculatePromotionFilter;
import ths.dms.core.entities.filter.PromotionMapBasicFilter;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.entities.vo.CommercialSupportCodeVO;
import ths.dms.core.entities.vo.CommercialSupportVO;
import ths.dms.core.entities.vo.ExMapping;
import ths.dms.core.entities.vo.GroupLevelDetailVO;
import ths.dms.core.entities.vo.GroupLevelVO;
import ths.dms.core.entities.vo.LevelMappingVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.NewLevelMapping;
import ths.dms.core.entities.vo.NewProductGroupVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PPConvertDetailVO;
import ths.dms.core.entities.vo.PPConvertVO;
import ths.dms.core.entities.vo.ProductGroupVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.entities.vo.PromoProductConvVO;
import ths.dms.core.entities.vo.PromotionCustAttrVO;
import ths.dms.core.entities.vo.PromotionProductOpenVO;
import ths.dms.core.entities.vo.PromotionProductsVO;
import ths.dms.core.entities.vo.PromotionProgramVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderPromotionRemainVO;
import ths.dms.core.entities.vo.SaleOrderVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.SubLevelMapping;
import ths.dms.core.entities.vo.ZV03View;
import ths.dms.core.entities.vo.ZV07View;
import ths.core.entities.vo.rpt.RptPromotionDetailDataVO;
import ths.core.entities.vo.rpt.RptPromotionProgramDetailVO;
import ths.dms.core.exceptions.DataAccessException;
//import org.apache.jasper.tagplugins.jstl.core.ForEach;

import ths.dms.core.common.utils.Constant;
import ths.dms.core.common.utils.LogUtility;
import ths.dms.core.common.utils.PromotionCalc;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class PromotionProgramDAOImpl implements PromotionProgramDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Autowired
	CommonDAO commonDAO;

	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	StockTotalDAO stockTotalDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	PriceDAO priceDAO;
	
	@Autowired
	SaleOrderLotDAO saleOrderDAO;

	@Override
	public PromotionProgram createPromotionProgram(
			PromotionProgram promotionProgram, LogInfoVO logInfo)
			throws DataAccessException {
		if (promotionProgram == null) {
			throw new IllegalArgumentException("promotionProgram");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (promotionProgram.getPromotionProgramCode() != null)
			promotionProgram.setPromotionProgramCode(promotionProgram
					.getPromotionProgramCode().toUpperCase());
		promotionProgram.setCreateUser(logInfo.getStaffCode());
		repo.create(promotionProgram);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, promotionProgram,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(PromotionProgram.class,
				promotionProgram.getId());
	}
	
	@Override
	public ProductGroup createProductGroup(ProductGroup productGroup) throws DataAccessException {
		productGroup = repo.create(productGroup);
		return repo.getEntityById(ProductGroup.class, productGroup.getId());
	}
	
	@Override
	public GroupLevel createGroupLevel(GroupLevel groupLevel) throws DataAccessException {
		groupLevel = repo.create(groupLevel);
		return repo.getEntityById(GroupLevel.class, groupLevel.getId());
	}
	
	@Override
	public GroupLevelDetail getGroupLevelDetailById(Long id) throws DataAccessException {
		return repo.getEntityById(GroupLevelDetail.class, id);
	}
	
	@Override
	public GroupLevelDetail createGroupLevelDetail(GroupLevelDetail groupLevelDetail) throws DataAccessException {
		groupLevelDetail = repo.create(groupLevelDetail);
		return repo.getEntityById(GroupLevelDetail.class, groupLevelDetail.getId());
	}
	
	@Override
	public GroupMapping createGroupMapping(GroupMapping groupMapping) throws DataAccessException {
		groupMapping = repo.create(groupMapping);
		return repo.getEntityById(GroupMapping.class, groupMapping.getId());
	}

	@Override
	public void deletePromotionProgram(PromotionProgram promotionProgram,
			LogInfoVO logInfo) throws DataAccessException {
		if (promotionProgram == null) {
			throw new IllegalArgumentException("promotionProgram");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(promotionProgram);
		try {
			actionGeneralLogDAO.createActionGeneralLog(promotionProgram, null,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public PromotionProgram getPromotionProgramById(long id)
			throws DataAccessException {
		return repo.getEntityById(PromotionProgram.class, id);
	}

	@Override
	public void updatePromotionProgram(PromotionProgram promotionProgram,
			LogInfoVO logInfo) throws DataAccessException {
		if (promotionProgram == null) {
			throw new IllegalArgumentException("promotionProgram");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (promotionProgram.getPromotionProgramCode() != null) {
			promotionProgram.setPromotionProgramCode(promotionProgram
					.getPromotionProgramCode().toUpperCase());
		}
		PromotionProgram temp = this.getPromotionProgramById(promotionProgram
				.getId());

		promotionProgram.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null)
			promotionProgram.setUpdateUser(logInfo.getStaffCode());
		if (!temp.getType().equals(promotionProgram.getType())) {
			this.deleteAllPromotionProgramDetail(promotionProgram.getId());
		}
		repo.update(promotionProgram);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, promotionProgram,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public void updateProductGroup(ProductGroup productGroup) throws DataAccessException {
		repo.update(productGroup);
	}
	
	@Override
	public void updateGroupLevel(GroupLevel groupLevel) throws DataAccessException {
		repo.update(groupLevel);
	}
	
	@Override
	public void updateGroupLevelDetail(GroupLevelDetail groupLevelDetail) throws DataAccessException {
		repo.update(groupLevelDetail);
	}
	
	@Override
	public void updateGroupMapping(GroupMapping groupMapping) throws DataAccessException {
		repo.update(groupMapping);
	}

	@Override
	public PromotionProgram getPromotionProgramByCode(String code)
			throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from promotion_program where upper(PROMOTION_PROGRAM_CODE)=? and status!=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		return repo.getEntityBySQL(PromotionProgram.class, sql, params);
	}
	
	@Override
	public List<GroupLevel> getListLevelNotInMapping(Long promotionId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("with listSaleGroupId as (select product_group_id from product_group where promotion_program_id = ? and group_type = ?), ");
		params.add(promotionId);
		params.add(ProductGroupType.MUA.getValue());
		sql.append("listFreeGroupId as (select product_group_id from product_group where promotion_program_id = ? and group_type = ?) ");
		params.add(promotionId);
		params.add(ProductGroupType.KM.getValue());
		sql.append("select * from group_level ");
		sql.append("where product_group_id IN (select * from listSaleGroupId) ");
		sql.append("and group_level_id not in (select sale_group_level_id  from group_mapping where sale_group_id in (select * from listSaleGroupId)) ");
		sql.append("union ");
		sql.append("select * from group_level ");
		sql.append("where product_group_id IN (select * from listFreeGroupId) ");
		sql.append("and group_level_id not in (select promo_group_level_id  from group_mapping where sale_group_id in (select * from listFreeGroupId))");
		
		return repo.getListBySQL(GroupLevel.class, sql.toString(), params);
	}
	
	@Override
	public PromotionProgram checkProductExistInOrPromotion(Long promotionId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("WITH listProduct AS \n");
		sql.append("  (SELECT gld.product_id FROM group_level_detail gld WHERE gld.status = 1 AND gld.group_level_id IN \n");
		sql.append("    (SELECT group_level_id FROM group_level gl WHERE gl.status = 1 AND gl.has_product = 1 AND gl.product_group_id IN \n");
		sql.append("      (SELECT product_group_id FROM product_group WHERE status = 1 AND GROUP_TYPE = 1 AND promotion_program_id = ? \n");
		params.add(promotionId);
		sql.append("      ) \n");
		sql.append("    ) \n");
		sql.append("  ), \n");
		sql.append("  listShopId AS \n");
		sql.append("  (SELECT shop_id FROM shop START WITH shop_id IN \n");
		sql.append("    (SELECT shop_id FROM promotion_shop_map psm WHERE psm.status = 1 AND psm.promotion_program_id = ? \n");
		params.add(promotionId);
		sql.append("    ) CONNECT BY PRIOR shop_id = parent_shop_id \n");
		sql.append("  union ");
		sql.append("  	SELECT shop_id FROM shop START WITH shop_id IN \n");
		sql.append("    (SELECT shop_id FROM promotion_shop_map psm WHERE psm.status = 1 AND psm.promotion_program_id = ? \n");
		params.add(promotionId);
		sql.append("    ) CONNECT BY PRIOR parent_shop_id = shop_id \n");
		sql.append("  ) , \n");
		sql.append("  listAttr AS \n");
		sql.append("  (SELECT PROMOTION_CUST_ATTR_ID attrId, OBJECT_TYPE objectType, OBJECT_ID objectId, FROM_VALUE fromValue, TO_VALUE toValue FROM PROMOTION_CUST_ATTR WHERE PROMOTION_PROGRAM_ID = ? AND status = 1 \n");
		params.add(promotionId);
		sql.append("  ), \n");
		sql.append("  listAttrDtl AS \n");
		sql.append("  ( SELECT OBJECT_TYPE, OBJECT_ID FROM PROMOTION_CUST_ATTR_DETAIL WHERE status = 1 AND PROMOTION_CUST_ATTR_ID IN \n");
		sql.append("    (SELECT attrId FROM listAttr \n");
		sql.append("    ) \n");
		sql.append("  ) , \n");
		sql.append("  listPromoExistsA AS \n");
		sql.append("  (SELECT promotion_program_id FROM promotion_program pp WHERE pp.status = 1 AND pp.promotion_program_id != ? AND pp.status = 1 AND pp.promotion_program_id IN \n");
		params.add(promotionId);
		sql.append("    (SELECT pg.promotion_program_id FROM product_group pg WHERE pg.status = 1 AND pg.group_type = 1 AND pg.product_group_id IN \n");
		sql.append("      (SELECT gl.product_group_id FROM group_level gl WHERE gl.status = 1 AND gl.group_level_id IN \n");
		sql.append("        (SELECT gld.group_level_id FROM group_level_detail gld WHERE gld.status = 1 AND gld.product_id IN \n");
		sql.append("          (SELECT product_id FROM listProduct \n");
		sql.append("          ) \n");
		sql.append("        ) \n");
		sql.append("      ) \n");
		sql.append("    ) \n");
		sql.append("  ) , \n");
		sql.append("  listShopExistsTmpA AS \n");
		sql.append("  (SELECT shop_id, promotion_program_id FROM promotion_shop_map psm WHERE psm.status = 1 AND psm.promotion_program_id IN \n");
		sql.append("    (SELECT promotion_program_id FROM listPromoExistsA \n");
		sql.append("    ) AND psm.shop_id IN \n");
		sql.append("    (SELECT shop_id FROM listShopId \n");
		sql.append("    ) \n");
		sql.append("  ) , \n");
		sql.append("  listPromoExists AS \n");
		sql.append("  (SELECT DISTINCT promotion_program_id FROM listShopExistsTmpA \n");
		
		sql.append("     where 1=1 ");
		/*chi lay nhung CTKM giao ngay voi CTKM dang check*/
		sql.append("     and exists ( ");
		sql.append("       select 1 ");
		sql.append("       from promotion_program pp2 ");
		sql.append("       where 1=1 ");
		sql.append("       and pp2.promotion_program_id = listShopExistsTmpA.promotion_program_id ");
		sql.append("       and pp2.status = 1 ");
		sql.append("       and not ( ");
		sql.append("         (pp2.to_date is not null and pp2.to_date < (select from_date from promotion_program where promotion_program_id = ?)) ");
		params.add(promotionId);
		sql.append("         or ( ");
		sql.append("             exists (select 1 from promotion_program where promotion_program_id = ? and to_date is not null) ");
		params.add(promotionId);
		sql.append("               and pp2.from_date > (select to_date from promotion_program where promotion_program_id = ?) ");
		params.add(promotionId);
		sql.append("           ) ");
		sql.append("       ) ");
		sql.append("     ) ");
		
		sql.append("  ), \n");
		sql.append("  listShopExistsTmp AS \n");
		sql.append("  (SELECT DISTINCT shop_id FROM listShopExistsTmpA \n");
		sql.append("  ), \n");
		sql.append("  listShopExists AS \n");
		sql.append("  (SELECT shop_id FROM shop START WITH shop_id IN \n");
		sql.append("    (SELECT shop_id FROM listShopExistsTmp \n");
		sql.append("    ) CONNECT BY PRIOR shop_id = parent_shop_id \n");
		sql.append("  ) , \n");
		sql.append("  listAttrExists AS \n");
		sql.append("  (SELECT PROMOTION_CUST_ATTR_ID attrId, OBJECT_TYPE objectType, OBJECT_ID objectId, FROM_VALUE fromValue, TO_VALUE toValue FROM PROMOTION_CUST_ATTR WHERE status = 1 AND PROMOTION_PROGRAM_ID IN \n");
		sql.append("    (SELECT promotion_program_id FROM listPromoExists \n");
		sql.append("    ) \n");
		sql.append("  ), \n");
		sql.append("  listAttrDtlExists AS \n");
		sql.append("  (SELECT OBJECT_TYPE objectType, OBJECT_ID objectId FROM PROMOTION_CUST_ATTR_DETAIL WHERE status = 1 AND PROMOTION_CUST_ATTR_ID IN \n");
		sql.append("    (SELECT attrId FROM listAttrExists \n");
		sql.append("    ) \n");
		sql.append("  ), \n");
		sql.append("  listAttrIntersect AS \n");
		sql.append("  ( SELECT objectType, objectId, fromValue,toValue FROM listAttr \n");
		sql.append("  INTERSECT \n");
		sql.append("  SELECT objectType, objectId, fromValue,toValue FROM listAttrExists \n");
		sql.append("  ) \n");
		sql.append("SELECT CASE WHEN NOT EXISTS \n");
		sql.append("      (SELECT * FROM listShopId \n");
		sql.append("      INTERSECT \n");
		sql.append("      SELECT * FROM listShopExists \n");
		sql.append("      ) THEN 0 WHEN EXISTS \n");
		sql.append("      (SELECT * FROM listShopId \n");
		sql.append("      INTERSECT \n");
		sql.append("      SELECT * FROM listShopExists \n");
		sql.append("      ) AND EXISTS \n");
		sql.append("      (SELECT * FROM listAttr \n");
		sql.append("      ) AND EXISTS \n");
		sql.append("      (SELECT * FROM listAttrExists \n");
		sql.append("      ) AND EXISTS \n");
		sql.append("      (SELECT * FROM listAttrIntersect \n");
		sql.append("      ) AND EXISTS \n");
		sql.append("      ( SELECT * FROM listAttrIntersect WHERE (objectType = 2 OR objectType = 3) AND NOT EXISTS \n");
		sql.append("        (SELECT * FROM listAttrDtl \n");
		sql.append("        INTERSECT \n");
		sql.append("        SELECT * FROM listAttrDtlExists \n");
		sql.append("        ) \n");
		sql.append("      ) THEN 0 ELSE \n");
		sql.append("      (SELECT promotion_program_id FROM listPromoExists WHERE rownum = 1 \n");
		sql.append("      ) END FROM dual");
		
		Object __promotionExistsId = repo.getObjectByQuery(sql.toString().replaceAll("  ", " "), params);
		
		Long promotionExistsId = __promotionExistsId == null ? 0 : ((Number) __promotionExistsId).longValue();
		
		if(promotionExistsId == null || promotionExistsId == 0) {
			return null;
		} else {
			return this.getPromotionProgramById(promotionExistsId);
		}
	}
	
	@Override
	public ProductGroup getProductGroupByCode(String productGroup, ProductGroupType groupType, Long promotionProgramId) throws DataAccessException {
		productGroup = productGroup.toUpperCase();
		String sql = "select * from product_group where product_group_code = ? and group_type = ? and promotion_program_id = ? and status = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(productGroup);
		params.add(groupType.getValue());
		params.add(promotionProgramId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(ProductGroup.class, sql, params);
	}
	
	@Override
	public GroupLevel getGroupLevelByMaxAmount(Long groupProductId, BigDecimal maxAmount) throws DataAccessException {
		String sql = "select * from group_level where max_amount = ? and product_group_id = ? and status = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(maxAmount);
		params.add(groupProductId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(GroupLevel.class, sql, params);
	}
	
	@Override
	public GroupLevel getGroupLevelByPercent(Long groupProductId, Float percent) throws DataAccessException {
		String sql = "select * from group_level where promotion_percent = ? and product_group_id = ? and status = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(percent);
		params.add(groupProductId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(GroupLevel.class, sql, params);
	}
	
	@Override
	public List<ProductGroup> getListProductGroupByPromotionId(Long promotionId, ProductGroupType groupType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from product_group pg where pg.promotion_program_id = ? and status = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		if(groupType != null) {
			sql.append(" and group_type = ?");
			params.add(groupType.getValue());
		}
		sql.append("ORDER BY PG.ORDER_NUMBER ");
		return repo.getListBySQL(ProductGroup.class, sql.toString(), params);
	}
	
	@Override
	public List<NewProductGroupVO> getListNewProductGroupByPromotionId(Long promotionId) throws DataAccessException {
		if (promotionId == null) {
			throw new IllegalArgumentException("promotionId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select mua.promotion_program_id promotionId, mua.product_group_id groupMuaId, km.product_group_id groupKMId, mua.product_group_code groupMuaCode, km.product_group_code groupKMCode, mua.product_group_name groupMuaName, km.product_group_name groupKMName, mua.min_quantity minQuantityMua, mua.min_amount minAmountMua, km.max_quantity maxQuantityKM, km.max_amount maxAmountKM, mua.order_number stt, mua.MULTIPLE multiple, mua.RECURSIVE recursive ");
		sql.append("from promotion_program pp inner join product_group mua on pp.promotion_program_id = mua.promotion_program_id and mua.group_type = ? ");
		params.add(ProductGroupType.MUA.getValue());
		sql.append("inner join product_group km on pp.promotion_program_id = km.promotion_program_id and mua.product_group_code = km.product_group_code and km.group_type = ? ");
		params.add(ProductGroupType.KM.getValue());
		sql.append("where mua.promotion_program_id = ? and mua.status = 1 and km.promotion_program_id = ? and km.status = 1 ");
		params.add(promotionId);
		params.add(promotionId);
		sql.append("order by groupMuaCode");

		String[] fieldNames = { "promotionId", // 0
				"groupMuaId", // 1
				"groupKMId", // 2
				"groupMuaCode", // 3
				"groupKMCode", // 4
				"groupMuaName", // 5
				"groupKMName", // 6
				"minQuantityMua", // 7
				"minAmountMua", // 8
				"maxQuantityKM", // 9
				"maxAmountKM", // 10
				"stt", // 11
				"multiple", "recursive" };

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.LONG, // 1
				StandardBasicTypes.LONG, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.INTEGER, // 7
				StandardBasicTypes.BIG_DECIMAL, // 8
				StandardBasicTypes.INTEGER, // 9
				StandardBasicTypes.BIG_DECIMAL, // 10
				StandardBasicTypes.INTEGER, // 11
				StandardBasicTypes.INTEGER,//12
				StandardBasicTypes.INTEGER //13
		};

		return repo.getListByQueryAndScalar(NewProductGroupVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/**
	 * Lay product_group join level de kiem tra them has_product
	 * @author tientv11
	 * @param promotionId
	 * @param groupType
	 * @param hasProduct
	 * @return
	 * @throws DataAccessException
	 */
	private List<ProductGroup> getListProductGroupByPromotionIdJoinLevel(Long promotionId, ProductGroupType groupType, Integer hasProduct ) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct pg.* ");
		sql.append(" from product_group pg ");
		sql.append(" join group_level gl on pg.product_group_id = gl.product_group_id and gl.parent_group_level_id is not null");
		sql.append(" where pg.promotion_program_id = ? ");
		params.add(promotionId);
		sql.append(" and pg.status = 1 and gl.status = 1 ");
		
		if(groupType != null) {
			sql.append(" and pg.group_type = ? ");
			params.add(groupType.getValue());
		}
		
		if(hasProduct != null){
			sql.append(" and gl.has_product = ? ");
			params.add(hasProduct);
		}		
		//sql.append(" order by pg.order_number ");
		sql.append(" order by pg.product_group_code ");
		return repo.getListBySQL(ProductGroup.class, sql.toString(), params);
	}
	
	@Override
	public List<ProductGroupVO> getListProductGroupVO(Long promotionId, ProductGroupType groupType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select product_group_id id, product_group_code productGroupCode, product_group_name productGroupName, ");
		sql.append("group_type groupType, min_quantity minQuantity, max_quantity maxQuantity, ");
		sql.append("min_amount minAmount, max_amount maxAmount, order_number orderNumber ");
		sql.append("from product_group ");
		sql.append("where promotion_program_id = ? and status = ? ");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		if(groupType != null) {
			sql.append("and group_type = ?");
			params.add(groupType.getValue());
		}
		
		final String[] fieldNames = new String[] { "id", "productGroupCode", "productGroupName", 
				"groupType", "minQuantity", "maxQuantity", 
				"minAmount", "maxAmount", "orderNumber"};
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER};
		List<ProductGroupVO> listResult = repo.getListByQueryAndScalar(ProductGroupVO.class, fieldNames, fieldTypes, sql.toString(), params);
		for(ProductGroupVO vo : listResult) {
			vo.setListLevel(this.getListGroupLevelVO(vo.getId()));
		}
		return listResult;
	}
	
	@Override
	public List<GroupLevel> getListGroupLevelByGroupId(Long groupId) throws DataAccessException {
		String sql = "select * from group_level where product_group_id = ? and status = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(groupId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getListBySQL(GroupLevel.class, sql, params);
	}
	
	@Override
	public List<GroupLevel> getGroupLevelsOfProductGroup(long groupId, int isPromotionProduct) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT GL.GROUP_LEVEL_ID, GL.ORDER_NUMBER, GL.MIN_QUANTITY, GL.MAX_QUANTITY ");
		sql.append("	   , GL.MIN_AMOUNT, GL.MAX_AMOUNT, GL.HAS_PRODUCT ");
		sql.append("	   , GLD.GROUP_LEVEL_DETAIL_ID, GLD.PRODUCT_ID, GLD.VALUE_TYPE, GLD.VALUE, GLD.IS_REQUIRED ");
		sql.append("FROM GROUP_LEVEL GL ");
		sql.append("	LEFT JOIN GROUP_LEVEL_DETAIL GLD ON GL.GROUP_LEVEL_ID = GLD.GROUP_LEVEL_ID");
		sql.append("			AND GLD.STATUS = 1 ");
		sql.append("	WHERE 1 = 1 ");
		sql.append("			AND GL.PRODUCT_GROUP_ID = ? ");
		params.add(groupId);
		sql.append("			AND GL.HAS_PRODUCT = ? ");
		params.add(isPromotionProduct);
		sql.append("			AND GL.STATUS = 1 ");
		sql.append("	ORDER BY GL.ORDER_NUMBER, GLD.IS_REQUIRED DESC ");
		
		return repo.getListBySQL(GroupLevel.class, sql.toString(), params);
		
	}
	
	
	public List<GroupLevelVO> getGroupLevelVOOfProductGroup(long groupId, int isPromotionProduct) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT GL.GROUP_LEVEL_ID id, GL.ORDER_NUMBER orderNumber, ");
		sql.append(" 		GL.MIN_QUANTITY as minQuantity, GL.MAX_QUANTITY maxQuantity, ");
		sql.append("  		GL.MIN_AMOUNT minAmount, GL.MAX_AMOUNT maxAmount, GL.HAS_PRODUCT hasProduct, ");
		sql.append(" 		GLD.GROUP_LEVEL_DETAIL_ID groupLevelDetailId, GLD.PRODUCT_ID productId, ");
		sql.append(" 		GLD.VALUE_TYPE valueType, GLD.VALUE value, GLD.IS_REQUIRED isRequired ");
		sql.append(" FROM GROUP_LEVEL GL ");
		sql.append(" LEFT JOIN GROUP_LEVEL_DETAIL GLD ON GL.GROUP_LEVEL_ID = GLD.GROUP_LEVEL_ID ");
		sql.append(" AND GLD.STATUS = 1 ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND GL.PRODUCT_GROUP_ID = ? ");
		params.add(groupId);
		sql.append("  ");
		sql.append(" AND GL.HAS_PRODUCT = ? ");
		params.add(isPromotionProduct);
		sql.append("  ");
		sql.append(" AND GL.STATUS = 1 ");
		sql.append(" ORDER BY GL.ORDER_NUMBER, GLD.IS_REQUIRED DESC ");
		final String[] fieldNames = new String[] { "id", "orderNumber", 
				"minQuantity","maxQuantity",
				"minAmount", "maxAmount", "hasProduct",
				"groupLevelDetailId","productId",
				"valueType", "value", "isRequired"
			};
		
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER, 
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, 
				StandardBasicTypes.INTEGER,StandardBasicTypes.BIG_DECIMAL,StandardBasicTypes.INTEGER
			};
		List<GroupLevelVO> groupLevelVOs = repo.getListByQueryAndScalar(GroupLevelVO.class, fieldNames, fieldTypes, sql.toString(), params);
		
		List<GroupLevelVO> result = new ArrayList<GroupLevelVO>();
		
		for(GroupLevelVO levelVO : groupLevelVOs){
			if(isPromotionProduct == SaleOrderDetailVO.ownerTypePromoOrder || 
					(levelVO.getGroupLevelDetailId() !=null && levelVO.getGroupLevelDetailId() > 0L)){
				
				boolean isExist = false;
				for(GroupLevelVO dto: result) {
					if(dto.getId().equals(levelVO.getId()) && dto.getOrderNumber().equals(levelVO.getOrderNumber())) {
						isExist = true;
						break;
					}
				}
				if(!isExist) {
					result.add(levelVO);
				}				
			}
		}
		return result;
	}
	
	@Override
	public List<GroupLevelVO> getListGroupLevelVO(Long groupId) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append("select GROUP_LEVEL_ID id, GROUP_TEXT groupText, ORDER_NUMBER orderNumber, MIN_QUANTITY minQuantity, MAX_QUANTITY maxQuantity, MIN_AMOUNT minAmount, MAX_AMOUNT maxAmount, PROMOTION_PERCENT promotionPercent ");
		sql.append("from GROUP_LEVEL ");
		sql.append("where product_group_id = ? and status = ? order by orderNumber desc");
		params.add(groupId);
		params.add(ActiveType.RUNNING.getValue());
		final String[] fieldNames = new String[] { "id", "groupText", "orderNumber", "minQuantity", "maxQuantity", "minAmount", "maxAmount", "promotionPercent"};

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.FLOAT};
		List<GroupLevelVO> listResult = repo.getListByQueryAndScalar(GroupLevelVO.class, fieldNames, fieldTypes, sql.toString(), params);
		for(GroupLevelVO level : listResult) {
			List<GroupLevelDetailVO> listLevelDetail = this.getListGroupLevelDetailVO(level.getId());
			level.setDetail(listLevelDetail);
		}
		return listResult;
	}
	
	@Override
	public GroupLevel getGroupLevelByLeveCode(Long groupId, String levelCode, Integer orderNumber) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from group_level where product_group_id = ? and status = 1 and PARENT_GROUP_LEVEL_ID is null ");
		params.add(groupId);
		if(!StringUtility.isNullOrEmpty(levelCode)) {
			sql.append(" and group_level_code = ? ");
			params.add(levelCode);
		}
		if(null != orderNumber) {
			sql.append(" and ORDER_NUMBER = ? ");
			params.add(orderNumber);
		}
		return repo.getEntityBySQL(GroupLevel.class, sql.toString(), params);
	}
	
	@Override
	public List<GroupLevelDetail> getListGroupLevelDetailByLevelId(Long levelId) throws DataAccessException {
		String sql = "select * from group_level_detail gld where gld.group_level_id = ? and gld.status = ? ";
		sql += " ORDER BY gld.IS_REQUIRED DESC, gld.product_id ";
		List<Object> params = new ArrayList<Object>();
		params.add(levelId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getListBySQL(GroupLevelDetail.class, sql, params);
	}
	
	@Override
	public List<GroupLevelDetailVO> getListGroupLevelDetailVO(Long levelId) throws DataAccessException {
		String sql = "select gld.GROUP_LEVEL_DETAIL_ID id, gld.GROUP_LEVEL_ID groupLevelId, p.PRODUCT_ID productId, ";
		sql += " p.PRODUCT_CODE productCode, p.PRODUCT_NAME productName, p.convfact, gld.VALUE_TYPE valueType, ";
		sql += " gld.VALUE value, gld.IS_REQUIRED isRequired from group_level_detail gld ";
		sql += " inner join product p on gld.product_id = p.product_id where gld.group_level_id = ? and gld.status = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(levelId);
		params.add(ActiveType.RUNNING.getValue());
		final String[] fieldNames = new String[] { 
				"id", "groupLevelId", "productId", 
				"productCode", "productName", "convfact", 
				"valueType", "value", "isRequired"};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER};
		return repo.getListByQueryAndScalar(GroupLevelDetailVO.class, fieldNames, fieldTypes, sql, params);
	}
	
	@Override
	public List<SubLevelMapping> getListGroupLevelDetailVOEx(Long levelId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select gld.group_level_id levelId, gld.group_level_detail_id levelDetailId, p.product_id productId, p.product_code productCode, p.product_name productName, ");
		sql.append("case when gld.value_type = 1 then gld.value else 0 end quantity, case when gld.value_type = 2 then gld.value else 0 end amount, is_required isRequired ");
		sql.append("from group_level_detail gld inner join product p on gld.product_id = p.product_id and gld.status = 1 and p.status = 1 ");
		sql.append("where gld.group_level_id = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(levelId);
		final String[] fieldNames = new String[] { "levelId", //1
				"levelDetailId", //2
				"productId", //3
				"productCode", //4
				"productName", //5
				"quantity", //6
				"amount", //7
				"isRequired"};//8

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,//1
				StandardBasicTypes.LONG, //2
				StandardBasicTypes.LONG, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.STRING,//5
				StandardBasicTypes.INTEGER,//6
				StandardBasicTypes.BIG_DECIMAL,//7
				StandardBasicTypes.INTEGER};//8
		return repo.getListByQueryAndScalar(SubLevelMapping.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<GroupMapping> getlistMappingLevel(Long groupMuaId, Long groupKMId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from group_mapping where sale_group_id = ? and promo_group_id = ? and status = ?");
		params.add(groupMuaId);
		params.add(groupKMId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getListBySQL(GroupMapping.class, sql.toString(), params);
	}
	
	@Override
	public ObjectVO<NewLevelMapping> getListMappingLevel(Long groupMuaId, Long groupKMId, Integer fromLevel, Integer toLevel) throws DataAccessException {
		List<NewLevelMapping> listResult;
		StringBuilder sql = new StringBuilder();

		StringBuilder sqlCount = new StringBuilder();

		List<Object> params = new ArrayList<Object>();

		List<Object> paramCounts = new ArrayList<Object>();
		sql.append("select gm.group_mapping_id mapId, glmua.group_level_code levelCode, glmua.order_number stt, glmua.group_level_id levelMuaId, ");
		sql.append("glkm.group_level_id levelKMId, gm.sale_group_id groupMuaId, gm.promo_group_id groupKMId ");
		sql.append("from group_level glmua inner join group_mapping gm on glmua.group_level_id = gm.sale_group_level_id and glmua.status = 1 and gm.status = 1 ");
		sql.append("inner join group_level glkm on glkm.group_level_id = gm.promo_group_level_id and glkm.status = 1 ");
		sql.append("where gm.sale_group_id = ? and gm.promo_group_id = ? ");
		params.add(groupMuaId);
		params.add(groupKMId);
		if (null != fromLevel) {
			sql.append(" and glmua.order_number >= ? ");
			params.add(fromLevel);
		}
		if (null != toLevel) {
			sql.append(" and glmua.order_number <= ? ");
			params.add(toLevel);
		}
		sql.append("order by stt, levelCode");

		sqlCount.append("select count(1) as count from (");
		sqlCount.append("select gm.group_mapping_id mapId, glmua.group_level_code levelCode, glmua.order_number stt, glmua.group_level_id levelMuaId, ");
		sqlCount.append("glkm.group_level_id levelKMId, gm.sale_group_id groupMuaId, gm.promo_group_id groupKMId ");
		sqlCount.append("from group_level glmua inner join group_mapping gm on glmua.group_level_id = gm.sale_group_level_id and glmua.status = 1 and gm.status = 1 ");
		sqlCount.append("inner join group_level glkm on glkm.group_level_id = gm.promo_group_level_id and glkm.status = 1 ");
		sqlCount.append("where gm.sale_group_id = ? and gm.promo_group_id = ? ");
		paramCounts.add(groupMuaId);
		paramCounts.add(groupKMId);
		sqlCount.append(")");

		final String[] fieldNames = new String[] { "mapId", //1
				"levelCode", //2
				"stt", //3
				"levelMuaId", //4
				"levelKMId", //5
				"groupMuaId", //6
				"groupKMId" };//7

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,//1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.INTEGER, //3
				StandardBasicTypes.LONG, //4
				StandardBasicTypes.LONG, //5
				StandardBasicTypes.LONG,//6
				StandardBasicTypes.LONG };//7
		listResult = repo.getListByQueryAndScalar(NewLevelMapping.class, fieldNames, fieldTypes, sql.toString(), params);
		for (NewLevelMapping map : listResult) {
			map.setListExLevelMua(this.getListSubLevelByMapping(map.getLevelMuaId()));
			map.setListExLevelKM(this.getListSubLevelByMapping(map.getLevelKMId()));
		}

		Integer total = repo.countBySQL(sqlCount.toString(), paramCounts);

		ObjectVO<NewLevelMapping> result = new ObjectVO<NewLevelMapping>();

		result.setLstObject(listResult);
		KPaging<NewLevelMapping> kPaging = new KPaging<NewLevelMapping>();
		kPaging.setMaxResult(total);
		result.setkPaging(kPaging);
		return result;
	}
	
	@Override
	public List<ExMapping> getListSubLevelByMapping(Long levelId) throws DataAccessException {
		List<ExMapping> listResult;
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select group_level_id levelId, parent_group_level_id parentId, group_text textCode, min_quantity minQuantityMua, min_amount minAmountMua, PROMOTION_PERCENT percent, max_quantity maxQuantityKM, max_amount maxAmountKM, promotion_percent percent ");
		sql.append("from group_level ");
		sql.append("where parent_group_level_id = ? and status = 1");
		params.add(levelId);
		
		final String[] fieldNames = new String[] { "levelId", //1
				"parentId", //2
				"textCode", //3
				"minQuantityMua", //4
				"minAmountMua", //5
				"percent",
				"maxQuantityKM", //6
				"maxAmountKM"};//7

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,//1
				StandardBasicTypes.LONG, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.INTEGER, //4
				StandardBasicTypes.BIG_DECIMAL, //5
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER,//6
				StandardBasicTypes.BIG_DECIMAL};//7
		
		listResult = repo.getListByQueryAndScalar(ExMapping.class, fieldNames, fieldTypes, sql.toString(), params);
		for(ExMapping ex : listResult) {
			ex.setListSubLevel(this.getListGroupLevelDetailVOEx(ex.getLevelId()));
		}
		return listResult;
	}
	
	@Override
	public List<GroupLevel> getListSubLevelByParentId(Long parentLevelId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from group_level where status = ? and PARENT_GROUP_LEVEL_ID = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(parentLevelId);
		return repo.getListBySQL(GroupLevel.class, sql.toString(), params);
	}
	
	@Override
	public List<GroupMapping> getListGroupMappingBySaleGroupId(Long saleGroupId) throws DataAccessException {
		String sql = "select * from group_mapping where sale_group_id = ? and status = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(saleGroupId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getListBySQL(GroupMapping.class, sql, params);
	}
	
	@Override
	public List<GroupMapping> getListGroupMappingByPromotionGroupId(Long promotionGroupId) throws DataAccessException {
		String sql = "select * from group_mapping where promo_group_id = ? and status = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(promotionGroupId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getListBySQL(GroupMapping.class, sql, params);
	}
	
	@Override
	public List<LevelMappingVO> getListLevelMappingByGroupId(Long promotionId, Long groupMuaId, Long groupKMId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select gm.group_mapping_id mappingId, glmua.group_level_id idLevelMua, pgmua.product_group_code groupMuaCode, ");
		sql.append("pgmua.product_group_name groupMuaName, glmua.order_number orderLevelMua, glmua.group_text groupMuaText, ");
		sql.append("glmua.min_quantity minQuantityMua, glmua.min_amount minAmountMua, ");
		sql.append("glkm.group_level_id idLevelKM, pgkm.product_group_code groupKMCode, pgkm.product_group_name groupKMName, glkm.group_text groupKMText, ");
		sql.append("glkm.order_number orderLevelKM, glkm.max_quantity maxQuantityKM, glkm.max_amount maxAmountKM, glkm.promotion_percent percentKM ");
		sql.append("from group_mapping gm ");
		sql.append("inner join product_group pgmua on gm.sale_group_id = pgmua.product_group_id ");
		sql.append("inner join product_group pgkm on gm.promo_group_id = pgkm.product_group_id ");
		sql.append("inner join group_level glmua on gm.sale_group_level_id = glmua.group_level_id ");
		sql.append("inner join group_level glkm on gm.promo_group_level_id = glkm.group_level_id ");
		sql.append("where 1 = 1 ");
		sql.append("and pgmua.promotion_program_id = ? ");
		params.add(promotionId);
		sql.append("and pgkm.promotion_program_id = ? ");
		params.add(promotionId);
		sql.append("and pgmua.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("and pgkm.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("and glmua.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("and glkm.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("and gm.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		if(groupMuaId != null) {
			sql.append("and pgmua.product_group_id = ? ");
			params.add(groupMuaId);
		}
		if(groupKMId != null) {
			sql.append("and pgkm.product_group_id = ? ");
			params.add(groupKMId);
		}
		sql.append("order by groupMuaCode, orderLevelMua, groupKMCode, orderLevelKM");
		
		final String[] fieldNames = new String[] {"mappingId", "idLevelMua", "groupMuaCode", 
				"groupMuaName", "orderLevelMua", "groupMuaText",
				"minQuantityMua", "minAmountMua", 
				"idLevelKM", "groupKMCode", "groupKMName", "groupKMText", 
				"orderLevelKM", "maxQuantityKM", "maxAmountKM", "percentKM"};

		final Type[] fieldTypes = new Type[] {StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.FLOAT};
		
		return repo.getListByQueryAndScalar(LevelMappingVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<GroupMapping> getListGroupMappingByLevelId(Long levelMuaId, Long levelKMId) throws DataAccessException {
		String sql = "select * from group_mapping where 1 = 1";
		List<Object> params = new ArrayList<Object>();
		if(levelMuaId != null) {
			sql += " and sale_group_level_id = ? ";
			params.add(levelMuaId);
		}
		if(levelKMId != null) {
			sql += " and promo_group_level_id = ? ";
			params.add(levelKMId);
		}
		sql += " and status = 1";
		return repo.getListBySQL(GroupMapping.class, sql, params);
	}
	
	@Override
	public GroupMapping getGroupMappingByGroupCodeAndOrder(Long promotionId, String groupMuaCode, Integer orderLevelMua, String groupKMCode, Integer orderLevelKM) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select gm.* from group_mapping gm inner join group_level muagl on gm.sale_group_level_id = muagl.group_level_id ");
		sql.append("inner join group_level kmgl on gm.promo_group_level_id = kmgl.group_level_id ");
		sql.append("inner join product_group muapg on gm.sale_group_id = muapg.product_group_id ");
		sql.append("inner join product_group kmpg on gm.promo_group_id = kmpg.product_group_id ");
		sql.append("where muapg.product_group_code = ? and muagl.order_number = ? and kmpg.product_group_code = ? and kmgl.order_number = ? ");
		params.add(groupMuaCode);
		params.add(orderLevelMua);
		params.add(groupKMCode);
		params.add(orderLevelKM);
		sql.append("and muapg.promotion_program_id = ? and kmpg.promotion_program_id = ? ");
		params.add(promotionId);
		params.add(promotionId);
		sql.append("and muapg.status = ? and kmpg.status = ? and gm.status = ? and muagl.status = ? and kmgl.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(GroupMapping.class, sql.toString(), params);
	}
	
	@Override
	public List<GroupMapping> getGroupMappingByGroupCodeAndOrderMua(Long promotionId, String groupMuaCode, Integer orderLevelMua) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select gm.* from group_mapping gm inner join group_level muagl on gm.sale_group_level_id = muagl.group_level_id ");
		sql.append("inner join product_group muapg on gm.sale_group_id = muapg.product_group_id ");
		sql.append("where muapg.product_group_code = ? and muagl.order_number = ? ");
		params.add(groupMuaCode);
		params.add(orderLevelMua);
		sql.append("and muapg.promotion_program_id = ? ");
		params.add(promotionId);
		sql.append("and muapg.status = ? and gm.status = ? and muagl.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		return repo.getListBySQL(GroupMapping.class, sql.toString(), params);
	}
	
	@Override
	public GroupMapping getGroupMappingById(Long id) throws DataAccessException {
		return repo.getEntityById(GroupMapping.class, id);
	}
	
	@Override
	public Integer getMaxOrderNumberOfGroupLevel(Long groupProductId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select max(ORDER_NUMBER) as count from GROUP_LEVEL where PRODUCT_GROUP_ID = ? and PARENT_GROUP_LEVEL_ID is null and STATUS = 1 ");
		params.add(groupProductId);
		return repo.countBySQL(sql.toString(), params);
	}
	
	@Override
	public GroupLevel getGroupLevelByOrder(Integer order, Long productGroupId) throws DataAccessException {
		String sql = "select * from group_level where product_group_id = ? and order_number = ? and status = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(productGroupId);
		params.add(order);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(GroupLevel.class, sql, params);
	}
	
	@Override
	public ProductGroup getProductGroup(Long groupId) throws DataAccessException {
		return repo.getEntityById(ProductGroup.class, groupId);
	}
	
	@Override
	public void deleteProductGroup(ProductGroup productGroup) throws DataAccessException {
		repo.delete(productGroup);
	}
	
	@Override
	public List<Product> getListProductInSaleLevel(Long promotionId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select  distinct p.* from GROUP_LEVEL_DETAIL gld inner join product p on gld.PRODUCT_ID = p.product_id ");
		sql.append(" where gld.status = ? and gld.GROUP_LEVEL_ID IN ( ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("select GROUP_LEVEL_ID from GROUP_LEVEL where status = ? and PRODUCT_GROUP_ID IN ( ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("select PRODUCT_GROUP_ID from PRODUCT_GROUP where GROUP_TYPE = ? and PROMOTION_PROGRAM_ID = ? and status = ? ");
		params.add(ProductGroupType.MUA.getValue());
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(") ");
		sql.append(") ");
		return repo.getListBySQL(Product.class, sql.toString(), params);
	}
	
	@Override
	public List<Boolean> checkIfCommercialSupportExists(
			List<CommercialSupportCodeVO> lstCommercialSupportCodeVO,
			Date orderDate, Long saleOrderId) throws DataAccessException {
		List<String> lstSql = new ArrayList<String>();
		List<List<Object>> lstParams = new ArrayList<List<Object>>();
		for (CommercialSupportCodeVO vo : lstCommercialSupportCodeVO) {
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();

			if (CommercialSupportType.PROMOTION_PROGRAM.equals(vo.getType())) {
				sql.append(" from (");
				sql.append(" select promotion_program_code as code, promotion_program_name as name ");
				sql.append(" from promotion_program pp where status = 1 and pp.type in ('ZM', 'ZD', 'ZH', 'ZT')");
				// sql.append(" where type not like 'ZV%'");
				sql.append("   and promotion_program_code=? ");
				params.add(vo.getCode().toUpperCase());

				if (saleOrderId != null) {
					sql.append(" and (promotion_program_code in (select program_code from sale_order_detail where sale_order_id = ? and program_type != 2)");
					params.add(saleOrderId);
				} else {
					sql.append(" and (1=0");
				}

				sql.append("   or  (  ");

				sql.append(" EXISTS ");
				sql.append("   (SELECT 1 ");
				sql.append("   FROM promotion_shop_map psm ");
				sql.append("   WHERE psm.promotion_program_id = pp.promotion_program_id and status = 1");
				//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
				sql.append("   AND psm.from_date < TRUNC(?) + 1  ");
				sql.append("   AND (psm.to_date    >= TRUNC(?) or psm.to_date is null) ");
				params.add(orderDate);
				params.add(orderDate);
				//end
				sql.append("   AND (psm.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id)) ");
				params.add(vo.getShopId());
				
				//TODO vuongmq 13/01/2015; comment khong kiem tra CTKM cho NVBH, KH
				/*sql.append("    and  ");
				sql.append("       ( ");
				sql.append("         (not exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1) and ((psm.quantity_max is null or NVL(psm.quantity_received, 0) < psm.quantity_max)) )");
				sql.append("         or ");
				sql.append("         exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and customer_id = ? and ((quantity_max is null or NVL(quantity_received, 0) < quantity_max))) ");
				params.add(vo.getCustomerId());
				sql.append("       ) ");*/
				sql.append("   ) ");
				
				
				/*sql.append(" AND  ");
				sql.append("   ( ");
				sql.append("     NOT EXISTS (SELECT 1 FROM PROMOTION_CUST_ATTR pa WHERE pa.promotion_program_id = pp.promotion_program_id and status = 1) ");
				sql.append("     OR  ");

				// Kiem tra thuoc tinh KH
				sql.append(" ( ");
				sql.append(" EXISTS (SELECT * ");
				sql.append(" FROM PROMOTION_CUST_ATTR pa ");
				sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
				sql.append(" AND status = 1) ");
				sql.append(" AND ( ");
				sql.append(" NOT EXISTS ");
				sql.append(" ( ");
				sql.append(" (SELECT * ");
				sql.append(" FROM PROMOTION_CUST_ATTR pa ");
				sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
				sql.append(" AND status = 1) ");
				sql.append(" minus ");
				sql.append(" (SELECT * ");
				sql.append(" FROM PROMOTION_CUST_ATTR pa ");
				sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
				sql.append(" AND status = 1 ");
				sql.append(" AND ( (object_type = 1 ");
				sql.append(" AND ( ( EXISTS ");
				sql.append(" (SELECT 1 ");
				sql.append(" FROM attribute ");
				sql.append(" WHERE value_type = 1 ");
				sql.append(" AND attribute_id = pa.object_id ");
				sql.append(" AND status = 1 ");
				sql.append(" ) ");
				sql.append(" AND EXISTS ");
				sql.append(" (SELECT * ");
				sql.append(" FROM attribute_value ");
				sql.append(" WHERE attribute_id = pa.object_id ");
				sql.append(" AND status = 1 ");
				sql.append(" AND table_name = 'CUSTOMER' ");
				sql.append(" AND TABLE_ID = ? ");
				params.add(vo.getCustomerId());
				sql.append(" AND STATUS = 1 ");
				sql.append(" AND upper(VALUE) = upper(PA.FROM_VALUE) ");
				sql.append(" ) ) ");
				sql.append(" OR ( EXISTS ");
				sql.append(" (SELECT 1 ");
				sql.append(" FROM attribute ");
				sql.append(" WHERE value_type = 2 ");
				sql.append(" AND attribute_id = pa.object_id ");
				sql.append(" AND status = 1 ");
				sql.append(" ) ");
				sql.append(" AND EXISTS ");
				sql.append(" (SELECT * ");
				sql.append(" FROM attribute_value ");
				sql.append(" WHERE attribute_id = pa.object_id ");
				sql.append(" AND table_name = 'CUSTOMER' ");
				sql.append(" AND TABLE_ID = ? ");
				params.add(vo.getCustomerId());
				sql.append(" AND STATUS = 1 ");
				sql.append(" AND to_number(VALUE) >= to_number(PA.FROM_VALUE) ");
				sql.append(" AND to_number(value) <= to_number(pa.to_value) ");
				sql.append(" ) ) ");
				sql.append(" OR ( EXISTS ");
				sql.append(" (SELECT 1 ");
				sql.append(" FROM attribute ");
				sql.append(" WHERE value_type = 3 ");
				sql.append(" AND attribute_id = pa.object_id ");
				sql.append(" AND status = 1 ");
				sql.append(" ) ");
				sql.append(" AND EXISTS ");
				sql.append(" (SELECT * ");
				sql.append(" FROM attribute_value ");
				sql.append(" WHERE attribute_id = pa.object_id ");
				sql.append(" AND table_name = 'CUSTOMER' ");
				sql.append(" AND TABLE_ID = ? ");
				params.add(vo.getCustomerId());
				sql.append(" AND STATUS = 1 ");
				sql.append(" AND (to_date(VALUE, 'yyyy-MM-dd') >= to_date(PA.FROM_VALUE, 'yyyy-MM-dd') ");
				sql.append(" OR pa.from_value IS NULL) ");
				sql.append(" AND (to_date(value, 'yyyy-MM-dd') <= to_date(pa.to_value, 'yyyy-MM-dd') ");
				sql.append(" OR pa.to_value IS NULL) ");
				sql.append(" ) ) ");
				sql.append(" OR ( EXISTS ");
				sql.append(" (SELECT 1 ");
				sql.append(" FROM attribute ");
				sql.append(" WHERE value_type IN (4,5) ");
				sql.append(" AND attribute_id = pa.object_id ");
				sql.append(" AND status = 1 ");
				sql.append(" ) ");
				sql.append(" AND EXISTS ");
				sql.append(" (SELECT * ");
				sql.append(" FROM attribute_value av ");
				sql.append(" WHERE attribute_id = pa.object_id ");
				sql.append(" AND table_name = 'CUSTOMER' ");
				sql.append(" AND TABLE_ID = ? ");
				params.add(vo.getCustomerId());
				sql.append(" AND STATUS = 1 ");

				// Kiem tra thuoc tinh mutil select
				sql.append(" and exists (select 1 from attribute_value_detail avd join promotion_cust_attr_detail pcd on avd.attribute_detail_id = pcd.object_id ");
				sql.append("             where avd.attribute_value_id = av.attribute_value_id and pcd.promotion_cust_attr_id = pa.promotion_cust_attr_id and pcd.status = 1 and avd.status = 1)");

				// sql.append(" AND EXISTS ( SELECT * ");
				// sql.append(" FROM promotion_cust_attr_detail pcd ");
				// sql.append(" WHERE 1 = 1 ");
				// sql.append(" AND pcd.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
				// sql.append(" AND pcd.status = 1) ");
				// sql.append(" AND NOT EXISTS( ");
				// sql.append(" ( SELECT * ");
				// sql.append(" FROM promotion_cust_attr_detail pcd ");
				// sql.append(" WHERE 1 = 1 ");
				// sql.append(" AND pcd.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
				// sql.append(" AND pcd.status = 1) ");
				// sql.append(" minus ");
				// sql.append(" (SELECT * ");
				// sql.append(" FROM promotion_cust_attr_detail pcd ");
				// sql.append(" WHERE 1 = 1 ");
				// sql.append(" AND pcd.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
				// sql.append(" AND pcd.status = 1 ");
				// sql.append(" AND EXISTS ( ");
				// sql.append(" select 1 from attribute_value_detail avd ");
				// sql.append(" where avd.attribute_value_id = av.attribute_value_id ");
				// sql.append(" AND avd.attribute_detail_id = pcd.object_id ");
				// sql.append(" AND avd.status = 1 ");
				// sql.append(" )) ");
				// sql.append(" ) ");
				// Kiem tra thuoc tinh mutil select
				sql.append(" ) ) ) ) ");
				sql.append(" OR (object_type = 2 ");
				sql.append(" AND EXISTS ");
				sql.append(" (SELECT 1 ");
				sql.append(" FROM PROMOTION_CUST_ATTR_DETAIL ");
				sql.append(" WHERE status = 1 ");
				sql.append(" AND PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
				sql.append(" AND OBJECT_ID = ");
				sql.append(" (SELECT CHANNEL_TYPE_ID FROM CUSTOMER WHERE CUSTOMER_ID = ? AND status = 1 ");
				params.add(vo.getCustomerId());
				sql.append(" ) ");
				sql.append(" ) ) ");
				sql.append(" OR (object_type = 3 ");
				sql.append(" AND EXISTS ");
				sql.append(" (SELECT 1 ");
				sql.append(" FROM PROMOTION_CUST_ATTR_DETAIL ");
				sql.append(" WHERE status = 1 ");
				sql.append(" AND PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
				sql.append(" AND OBJECT_ID IN ");
				sql.append(" (SELECT SALE_LEVEL_CAT_ID FROM CUSTOMER_CAT_LEVEL WHERE CUSTOMER_ID = ? ");
				params.add(vo.getCustomerId());
				sql.append(" AND status = 1 ");
				sql.append(" ) ");
				sql.append(" ) ) ) ");
				sql.append(" ) ");
				sql.append(" ) ");
				sql.append(" ) ");
				sql.append(" ) ");
				// Kiem tra thuoc tinh KH
				sql.append("   ) ");*/
				
				
				sql.append(" and (not exists ");
				sql.append(" 	(select 1 ");
				sql.append(" 	 from PROMOTION_CUST_ATTR pa ");
				sql.append(" 	 where pa.promotion_program_id = pp.promotion_program_id ");
				sql.append(" 	  and status = 1) ");
				sql.append("    or (exists ");
				sql.append(" 		(select 1 ");
				sql.append(" 		 from PROMOTION_CUST_ATTR pa ");
				sql.append(" 		 where pa.promotion_program_id = pp.promotion_program_id ");
				sql.append(" 		  and status = 1) ");
				sql.append(" 	   and (not exists ( ");
				sql.append(" 						 (select * ");
				sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
				sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
				sql.append(" 						  and status = 1) ");
				sql.append(" 						 minus ");
				sql.append(" 						 (select * ");
				sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
				sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
				sql.append(" 						   and status = 1 ");
				sql.append(" 						   and ( ");
				sql.append(" 						   		(object_type = 1 ");
				sql.append(" 								 and ( ");
				sql.append(" 								 	(exists ");
				sql.append(" 										(select 1 ");
				sql.append(" 										 from customer_attribute ");
				sql.append(" 										 where type = 1 ");
				sql.append(" 										  and customer_attribute_id = pa.object_id ");
				sql.append(" 										  and status = 1) ");
				sql.append(" 										and exists ( ");
				sql.append(" 											select 1 ");
				sql.append(" 											from customer_attribute_detail ");
				sql.append(" 											where customer_id = ? ");
				params.add(vo.getCustomerId());
				sql.append(" 											and status = 1 ");
				sql.append(" 											and customer_attribute_id = pa.object_id ");
				sql.append(" 											and upper(value) = upper(pa.from_value) ");
				sql.append(" 										) ");
				sql.append(" 									) ");
				sql.append(" 									  or (exists ");
				sql.append(" 										   (select 1 ");
				sql.append(" 										    from customer_attribute ");
				sql.append(" 										    where type = 2 ");
				sql.append(" 										     and customer_attribute_id = pa.object_id ");
				sql.append(" 										     and status = 1) ");
				sql.append(" 									  		and exists ( ");
				sql.append(" 									  			select 1 ");
				sql.append(" 												from customer_attribute_detail ");
				sql.append(" 												where customer_id = ? ");
				params.add(vo.getCustomerId());
				sql.append(" 												and status = 1 ");
				sql.append(" 												and customer_attribute_id = pa.object_id ");
				sql.append(" 												and to_number(value) >= to_number(pa.from_value) ");
				sql.append(" 												and to_number(value) <= to_number(pa.to_value) ");
				sql.append(" 									  		) ");
				sql.append(" 										) ");
				sql.append(" 									  or (exists ");
				sql.append(" 										   (select 1 ");
				sql.append(" 										    from customer_attribute ");
				sql.append(" 										    where type = 3 ");
				sql.append(" 										     and customer_attribute_id = pa.object_id ");
				sql.append(" 										     and status = 1) ");
				sql.append(" 									  		and exists ( ");
				sql.append(" 									  			select 1 ");
				sql.append(" 												from customer_attribute_detail ");
				sql.append(" 												where customer_id = ? ");
				params.add(vo.getCustomerId());
				sql.append(" 												and status = 1 ");
				sql.append(" 												and customer_attribute_id = pa.object_id ");
				sql.append(" 												and (to_date(value, 'yyyy-mm-dd') >= to_date(pa.from_value, 'yyyy-mm-dd') or pa.from_value is null) ");
				sql.append(" 												and (to_date(value, 'yyyy-mm-dd') <= to_date(pa.to_value, 'yyyy-mm-dd') or pa.to_value is null) ");
				sql.append(" 									  		) ");
				sql.append(" 										) ");
				sql.append(" 									  or (exists ");
				sql.append(" 										   (select 1 ");
				sql.append(" 										    from customer_attribute ");
				sql.append(" 										    where type in (4, 5) ");
				sql.append(" 										     and customer_attribute_id = pa.object_id ");
				sql.append(" 										     and status = 1) ");
				sql.append(" 									  		and exists ( ");
				sql.append(" 									  			select 1 ");
				sql.append(" 												from customer_attribute_detail cad ");
				sql.append(" 												join promotion_cust_attr_detail pcad  ");
				sql.append(" 													on pcad.object_type = 1  ");
				sql.append(" 													and pcad.status = 1 ");
				sql.append(" 													and cad.customer_attribute_enum_id = pcad.object_id ");
				sql.append(" 												where cad.customer_id = ? ");
				params.add(vo.getCustomerId());
				sql.append(" 												and cad.status = 1 ");
				sql.append(" 												and pcad.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
				sql.append(" 												and cad.customer_attribute_id = pa.object_id ");
				sql.append(" 									  		) ");
				sql.append(" 										) ");
				sql.append(" 									) ");
				sql.append(" 								) ");
				sql.append(" 								or (object_type = 2 ");
				sql.append(" 									and exists ");
				sql.append(" 									 (select 1 ");
				sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
				sql.append(" 									  where status = 1 ");
				sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
				sql.append(" 									   and OBJECT_ID = ");
				sql.append(" 										(select CHANNEL_TYPE_ID ");
				sql.append(" 										 from CUSTOMER ");
				sql.append(" 										 where CUSTOMER_ID = ? ");
				params.add(vo.getCustomerId());
				sql.append(" 										  and status = 1))) ");
				sql.append(" 								or (object_type = 3 ");
				sql.append(" 									and exists ");
				sql.append(" 									 (select 1 ");
				sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
				sql.append(" 									  where status = 1 ");
				sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
				sql.append(" 									   and OBJECT_ID in ");
				sql.append(" 										(select SALE_LEVEL_CAT_ID ");
				sql.append(" 										 from CUSTOMER_CAT_LEVEL ");
				sql.append(" 										 where CUSTOMER_ID = ? ");
				params.add(vo.getCustomerId());
				sql.append(" 										  and status = 1))) ");
				sql.append(" 							) ");
				sql.append(" 						) ");
				sql.append(" 				) ");
				sql.append(" 			) ");
				sql.append(" 	) ");
				sql.append(" ) ");
				

				sql.append(" ) ");

				sql.append(" ) ");

				sql.append("   and from_date < trunc(?) + 1 and (to_date >= trunc(?) or to_date is null) and status=?");
				params.add(orderDate);
				params.add(orderDate);
				params.add(ActiveType.RUNNING.getValue());
				sql.append(" )");

			} else if (CommercialSupportType.DISPLAY_PROGRAM.equals(vo
					.getType())) {
				sql.append(" from (");
				sql.append(" select display_program_code as code, display_program_name as name ");
				sql.append(" from DISPLAY_PROGRAM_VNM dp ");
				sql.append(" where display_program_code = ? ");
				params.add(vo.getCode().toUpperCase());
//				sql.append(" and exists (select 1 from display_customer_map dcm ");
//				sql.append("             where exists(select 1 from display_staff_map dsm  ");
//				sql.append("               			  where dsm.display_staff_map_id=dcm.display_staff_map_id ");
//				sql.append("               			  and dp.display_program_id=dsm.display_program_id and dsm.status = 1)");
//				sql.append("               and dcm.customer_id=? and dcm.status = 1");
//				params.add(vo.getCustomerId());
//				// sql.append("			   and dcm.from_date < trunc(?) + 1 and (trunc(?) <= dcm.to_date or dcm.to_date is null) ");
//				// params.add(orderDate);
//				// params.add(orderDate);
//				sql.append(" )");
				//TUNGTT
				sql.append(" and dp.display_program_vnm_id in (select display_program_id from display_pro_shop_map_vnm dsm where dsm.status = 1 and shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id))");
				params.add(vo.getShopId());
				 sql.append(" and from_date < trunc(?) + 1 and (add_months(trunc(?, 'MM'), -2) <= to_date or to_date is null)");
				 params.add(orderDate);
				 params.add(orderDate);
				sql.append(" and status=?");
				params.add(ActiveType.RUNNING.getValue());
				sql.append(" )");
			} else {
				// sql.append(" from (");
				// sql.append(" select promotion_program_code as code, promotion_program_name as name ");
				// sql.append(" from promotion_program pp ");
				// sql.append(" where type not like 'ZV%'");
				// sql.append("   and promotion_program_code=? ");
				// params.add(vo.getCode().toUpperCase());
				// sql.append("   and exists (select 1 from promotion_shop_map psm  ");
				// sql.append("               where pp.promotion_program_id=psm.promotion_program_id ");
				// sql.append("                 and psm.status = ? and (psm.quantity_max is null or NVL(psm.quantity_received,0) < psm.quantity_max) ");
				// params.add(ActiveType.RUNNING.getValue());
				// sql.append("	and psm.shop_id in (select shop_id from shop where status != -1 start with shop_id = ? connect by prior parent_shop_id = shop_id)");
				// params.add(vo.getShopId());
				// sql.append("   and ((object_apply = ?  )");
				// // params.add(PromotionObjectApply.SHOP.getValue());
				//
				// sql.append(" 	or (object_apply = ? ");
				// // params.add(PromotionObjectApply.CUSTOMER.getValue());
				// sql.append("	and exists (select 1 from promotion_customer_map pcm ");
				// sql.append("		where pcm.promotion_shop_map_id = psm.promotion_shop_map_id ");
				// sql.append("		and pcm.customer_id = ? and pcm.type = ?  ");
				// params.add(vo.getCustomerId());
				// params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER.getValue());
				// sql.append("		and pcm.status = ?");
				// params.add(ActiveType.RUNNING.getValue());
				// sql.append("		and (pcm.quantity_max is null or NVL(pcm.quantity_received, 0) < pcm.quantity_max))) ");
				//
				// sql.append(" 	or (object_apply = ? ");
				// //
				// params.add(PromotionObjectApply.SHOP_AND_CUSTOMER_TYPE.getValue());
				// sql.append("	and exists (select 1 from promotion_customer_map pcm ");
				// sql.append("		where pcm.promotion_shop_map_id = psm.promotion_shop_map_id ");
				// sql.append("		and pcm.customer_type_id = ? and pcm.type = ?  ");
				// Customer cust =
				// customerDAO.getCustomerById(vo.getCustomerId());
				// if (cust == null || cust.getChannelType() == null)
				// params.add(-1);
				// else
				// params.add(cust.getChannelType().getId());
				// params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER_TYPE.getValue());
				// sql.append("		and pcm.status = ?");
				// params.add(ActiveType.RUNNING.getValue());
				// sql.append("		and (pcm.quantity_max is null or NVL(pcm.quantity_received, 0) < pcm.quantity_max)))) ");
				// sql.append(" )");
				//
				// sql.append("   and from_date < trunc(?) + 1 and (to_date >= trunc(?) or to_date is null) and status=1");
				// params.add(orderDate);
				// params.add(orderDate);
				// sql.append(" union ");
				// sql.append(" select display_program_code as code, display_program_name as name ");
				// sql.append(" from display_program dp ");
				// sql.append(" where display_program_code=? ");
				// params.add(vo.getCode().toUpperCase());
				// sql.append("   and exists (select 1 from display_customer_map dcm ");
				// sql.append("               where exists(select 1 from display_staff_map dsm where dsm.display_staff_map_id=dcm.display_staff_map_id and dp.display_program_id=dsm.display_program_id) ");
				// sql.append("                 and dcm.customer_id=? and dcm.status = ?) ");
				// params.add(vo.getCustomerId());
				// params.add(ActiveType.RUNNING.getValue());
				// sql.append("   and from_date < trunc(?) + 1 and (to_date >= trunc(?) or to_date is null) and status=?");
				// params.add(orderDate);
				// params.add(orderDate);
				// params.add(ActiveType.RUNNING.getValue());
				// sql.append(" )");
				return null;
			}

			lstSql.add(sql.toString().replaceAll("  ", " "));
			lstParams.add(params);
		}
		return repo.checkExistBySQL(lstSql, lstParams);
	}

	@Override
	public List<CommercialSupportVO> getListCommercialSupport(
			KPaging<CommercialSupportVO> kPaging, Long shopId, Long customerId,
			Date orderDate, Long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		StringBuilder countSql = new StringBuilder();

		countSql.append(" select count(1) as count from (");

		sql.append(" select promotion_program_code as code, promotion_program_name as name, ");
		sql.append(" type as promotionType, 1 as type, to_char(description) as description, from_date as fromDate");
		sql.append(" from promotion_program pp ");

		sql.append(" WHERE pp.status   = 1 and pp.type in ('ZM', 'ZD', 'ZH', 'ZT')");
		sql.append(" AND pp.from_date < TRUNC(?) + 1 ");
		sql.append(" AND (pp.to_date    >= TRUNC(?) or pp.to_date is null)");
		params.add(orderDate);
		params.add(orderDate);
		sql.append(" AND EXISTS ");
		sql.append("   (SELECT 1 ");
		sql.append("   FROM promotion_shop_map psm ");
		sql.append("   WHERE psm.promotion_program_id = pp.promotion_program_id and status = 1");
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append(" AND ( psm.from_date is null  OR");
		sql.append("  ( psm.from_date < TRUNC(?) + 1  ");
		sql.append("   AND (psm.to_date    >= TRUNC(?) or psm.to_date is null) ");
		params.add(orderDate);
		params.add(orderDate);
		sql.append("  )  )");
		//end
		sql.append("   AND (psm.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id)) ");
		params.add(shopId);
		//TODO vuongmq 13/01/2015; comment khong kiem tra CTKM cho NVBH, KH
		/*sql.append("    and  ");
		sql.append("       ( ");
		sql.append("         (not exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1) and ((psm.quantity_max is null or NVL(psm.quantity_received, 0) < psm.quantity_max)) )");
		sql.append("         or ");
		sql.append("         exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and customer_id = ? and ((quantity_max is null or NVL(quantity_received, 0) < quantity_max))) ");
		params.add(customerId);
		sql.append("       ) ");*/
		sql.append("   ) ");
		
		
		/*sql.append(" AND  ");
		sql.append("   ( ");
		sql.append("     NOT EXISTS (SELECT 1 FROM PROMOTION_CUST_ATTR pa WHERE pa.promotion_program_id = pp.promotion_program_id and status = 1) ");
		sql.append("     OR  ");

		// Kiem tra thuoc tinh KH
		sql.append(" ( ");
		sql.append(" EXISTS (SELECT * FROM PROMOTION_CUST_ATTR pa WHERE pa.promotion_program_id = pp.promotion_program_id AND status = 1)");
		sql.append(" AND ( ");
		sql.append(" NOT EXISTS ");
		sql.append(" ( ");
		sql.append(" (SELECT * ");
		sql.append(" FROM PROMOTION_CUST_ATTR pa ");
		sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" AND status = 1) ");
		sql.append(" minus ");
		sql.append(" (SELECT * ");
		sql.append(" FROM PROMOTION_CUST_ATTR pa ");
		sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" AND status = 1 ");
		sql.append(" AND ( (object_type = 1 ");
		sql.append(" AND ( ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type = 1 ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		sql.append(" AND upper(VALUE) = upper(PA.FROM_VALUE) ");
		sql.append(" ) ) ");
		sql.append(" OR ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type = 2 ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		sql.append(" AND to_number(VALUE) >= to_number(PA.FROM_VALUE) ");
		sql.append(" AND to_number(value) <= to_number(pa.to_value) ");
		sql.append(" ) ) ");
		sql.append(" OR ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type = 3 ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		sql.append(" AND (to_date(VALUE, 'yyyy-MM-dd') >= to_date(PA.FROM_VALUE, 'yyyy-MM-dd') ");
		sql.append(" OR pa.from_value IS NULL) ");
		sql.append(" AND (to_date(value, 'yyyy-MM-dd') <= to_date(pa.to_value, 'yyyy-MM-dd') ");
		sql.append(" OR pa.to_value IS NULL) ");
		sql.append(" ) ) ");
		sql.append(" OR ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type IN (4,5) ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value av ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		// -----------------Kiem tra thuoc tinh mutil select
		sql.append(" and exists (select 1 from attribute_value_detail avd join promotion_cust_attr_detail pcd on avd.attribute_detail_id = pcd.object_id ");
		sql.append("             where avd.attribute_value_id = av.attribute_value_id and pcd.promotion_cust_attr_id = pa.promotion_cust_attr_id and pcd.status = 1 and avd.status = 1)");

		// sql.append(" AND EXISTS ( SELECT * ");
		// sql.append(" FROM promotion_cust_attr_detail pcd ");
		// sql.append(" WHERE 1 = 1 ");
		// sql.append(" AND pcd.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
		// sql.append(" AND pcd.status = 1) ");
		// sql.append(" AND NOT EXISTS( ");
		// sql.append(" ( SELECT * ");
		// sql.append(" FROM promotion_cust_attr_detail pcd ");
		// sql.append(" WHERE 1 = 1 ");
		// sql.append(" AND pcd.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
		// sql.append(" AND pcd.status = 1) ");
		// sql.append(" minus ");
		// sql.append(" (SELECT * ");
		// sql.append(" FROM promotion_cust_attr_detail pcd ");
		// sql.append(" WHERE 1 = 1 ");
		// sql.append(" AND pcd.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
		// sql.append(" AND pcd.status = 1 ");
		// sql.append(" AND EXISTS ( ");
		// sql.append(" select 1 from attribute_value_detail avd ");
		// sql.append(" where avd.attribute_value_id = av.attribute_value_id ");
		// sql.append(" AND avd.attribute_detail_id = pcd.object_id ");
		// sql.append(" AND avd.status = 1 ");
		// sql.append(" )) ");
		// sql.append(" ) ");
		// -----------------Kiem tra thuoc tinh mutil select
		sql.append(" ) ) ) ) ");
		sql.append(" OR (object_type = 2 ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" WHERE status = 1 ");
		sql.append(" AND PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND OBJECT_ID = ");
		sql.append(" (SELECT CHANNEL_TYPE_ID FROM CUSTOMER WHERE CUSTOMER_ID = ? AND status = 1 ");
		params.add(customerId);
		sql.append(" ) ");
		sql.append(" ) ) ");
		sql.append(" OR (object_type = 3 ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" WHERE status = 1 ");
		sql.append(" AND PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND OBJECT_ID IN ");
		sql.append(" (SELECT SALE_LEVEL_CAT_ID FROM CUSTOMER_CAT_LEVEL WHERE CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" ) ) ) ");
		sql.append(" ) ");
		sql.append(" ) ");
		sql.append(" ) ");
		sql.append(" ) ");*/
		// Kiem tra thuoc tinh KH
		
		sql.append(" and (not exists ");
		sql.append(" 	(select 1 ");
		sql.append(" 	 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 	 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 	  and status = 1) ");
		sql.append("    or (exists ");
		sql.append(" 		(select 1 ");
		sql.append(" 		 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 		 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 		  and status = 1) ");
		sql.append(" 	   and (not exists ( ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						  and status = 1) ");
		sql.append(" 						 minus ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						   and status = 1 ");
		sql.append(" 						   and ( ");
		sql.append(" 						   		(object_type = 1 ");
		sql.append(" 								 and ( ");
		sql.append(" 								 	(exists ");
		sql.append(" 										(select 1 ");
		sql.append(" 										 from customer_attribute ");
		sql.append(" 										 where type = 1 ");
		sql.append(" 										  and customer_attribute_id = pa.object_id ");
		sql.append(" 										  and status = 1) ");
		sql.append(" 										and exists ( ");
		sql.append(" 											select 1 ");
		sql.append(" 											from customer_attribute_detail ");
		sql.append(" 											where customer_id = ? ");
		params.add(customerId);
		sql.append(" 											and status = 1 ");
		sql.append(" 											and customer_attribute_id = pa.object_id ");
		sql.append(" 											and upper(value) = upper(pa.from_value) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 2 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and to_number(value) >= to_number(pa.from_value) ");
		sql.append(" 												and to_number(value) <= to_number(pa.to_value) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 3 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') >= to_date(pa.from_value, 'yyyy-mm-dd') or pa.from_value is null) ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') <= to_date(pa.to_value, 'yyyy-mm-dd') or pa.to_value is null) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type in (4, 5) ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail cad ");
		sql.append(" 												join promotion_cust_attr_detail pcad  ");
		sql.append(" 													on pcad.object_type = 1  ");
		sql.append(" 													and pcad.status = 1 ");
		sql.append(" 													and cad.customer_attribute_enum_id = pcad.object_id ");
		sql.append(" 												where cad.customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and cad.status = 1 ");
		sql.append(" 												and pcad.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
		sql.append(" 												and cad.customer_attribute_id = pa.object_id ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 								) ");
		sql.append(" 								or (object_type = 2 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID = ");
		sql.append(" 										(select CHANNEL_TYPE_ID ");
		sql.append(" 										 from CUSTOMER ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 								or (object_type = 3 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID in ");
		sql.append(" 										(select SALE_LEVEL_CAT_ID ");
		sql.append(" 										 from CUSTOMER_CAT_LEVEL ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 							) ");
		sql.append(" 						) ");
		sql.append(" 				) ");
		sql.append(" 			) ");
		sql.append(" 	) ");
		sql.append(" ) ");

		
		//sql.append("   ) ");

//		sql.append(" union  ");
//		sql.append(" select display_program_code as code, display_program_name as name, '' as promotionType, 2 as type, to_char('') as description, from_date as fromDate ");
//		sql.append(" from display_program_vnm dp "); //tungtt
//		sql.append(" where 1=1");
//		//TUNTT
////		sql.append(" where exists (select 1 from display_customer_map dcm ");
////		sql.append("               where exists(select 1 from display_staff_map dsm  ");
////		sql.append("               				where dsm.display_staff_map_id=dcm.display_staff_map_id ");
////		sql.append("               				and dp.display_program_vnm_id=dsm.display_program_id and dsm.status = 1)");
////		sql.append("               and dcm.customer_id=? and dcm.status = 1");
////		sql.append("			   and dcm.from_date < trunc(?) + 1 and (trunc(?) <= dcm.to_date or dcm.to_date is null) ");
////		params.add(customerId);
////		params.add(orderDate);
////		params.add(orderDate);
////		sql.append(" )");
//		sql.append(" and dp.display_program_vnm_id in (select display_program_id from display_pro_shop_map_vnm dsm where dsm.status = 1 and from_date < trunc(?) + 1 and (add_months(trunc(?, 'MM'), -2) <= to_date or to_date is null) and shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id))");
//		params.add(orderDate);
//		params.add(orderDate);
//		params.add(shopId);
//		sql.append(" and from_date < trunc(?) + 1 and (add_months(trunc(?, 'MM'), -2) <= to_date or to_date is null) and status=?");
//		params.add(orderDate);
//		params.add(orderDate);
//		params.add(ActiveType.RUNNING.getValue());

		countSql.append(sql.toString());
		countSql.append(" )");

		StringBuilder temp = new StringBuilder();
		temp.append("select * from (");
		temp.append(sql.toString());
		temp.append(") order by code");

		sql = temp;

		final String[] fieldNames = new String[] { "code", "name", "type",
				"description", "promotionType" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };

		if (kPaging != null)
			return repo.getListByQueryAndScalarPaginated(
					CommercialSupportVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params,
					kPaging);
		else
			return repo.getListByQueryAndScalar(CommercialSupportVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
	}

	
	@Override
	public List<CommercialSupportVO> getCommercialSupport(
			KPaging<CommercialSupportVO> kPaging, Long shopId, Long customerId,
			Date orderDate, Long saleOrderId,String commercialSupportCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		StringBuilder countSql = new StringBuilder();

		countSql.append(" select count(1) as count from (");

		sql.append(" select promotion_program_code as code, promotion_program_name as name, ");
		sql.append(" type as promotionType, 1 as type, to_char(description) as description, from_date as fromDate");
		sql.append(" from promotion_program pp ");

		sql.append(" WHERE pp.status   = 1 and pp.type in ('ZM', 'ZD', 'ZH', 'ZT')");
		sql.append(" AND pp.from_date < TRUNC(?) + 1 ");
		sql.append(" AND (pp.to_date    >= TRUNC(?) or pp.to_date is null)");
		params.add(orderDate);
		params.add(orderDate);
		if (!StringUtility.isNullOrEmpty(commercialSupportCode)) {
			sql.append(" AND upper(pp.promotion_program_code) = ? ");
			params.add(commercialSupportCode.toUpperCase());
		}
		sql.append(" AND EXISTS ");
		sql.append("   (SELECT 1 ");
		sql.append("   FROM promotion_shop_map psm ");
		sql.append("   WHERE psm.promotion_program_id = pp.promotion_program_id and status = 1");
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append(" AND ( psm.from_date is null  OR");
		sql.append("  ( psm.from_date < TRUNC(?) + 1  ");
		sql.append("   AND (psm.to_date    >= TRUNC(?) or psm.to_date is null) ");
		params.add(orderDate);
		params.add(orderDate);
		sql.append("  )  )");
		//end
		sql.append("   AND (psm.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id)) ");
		params.add(shopId);
		
		//TODO vuongmq 13/01/2015; comment khong kiem tra CTKM cho NVBH, KH
		/*sql.append("    and  ");
		sql.append("       ( ");
		sql.append("         (not exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1) and ((psm.quantity_max is null or NVL(psm.quantity_received, 0) < psm.quantity_max)) )");
		sql.append("         or ");
		sql.append("         exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and customer_id = ? and ((quantity_max is null or NVL(quantity_received, 0) < quantity_max))) ");
		params.add(customerId);
		sql.append("       ) ");*/
		sql.append("   ) ");
		
		
		/*sql.append(" AND  ");
		sql.append("   ( ");
		sql.append("     NOT EXISTS (SELECT 1 FROM PROMOTION_CUST_ATTR pa WHERE pa.promotion_program_id = pp.promotion_program_id and status = 1) ");
		sql.append("     OR  ");

		// Kiem tra thuoc tinh KH
		sql.append(" ( ");
		sql.append(" EXISTS (SELECT * FROM PROMOTION_CUST_ATTR pa WHERE pa.promotion_program_id = pp.promotion_program_id AND status = 1)");
		sql.append(" AND ( ");
		sql.append(" NOT EXISTS ");
		sql.append(" ( ");
		sql.append(" (SELECT * ");
		sql.append(" FROM PROMOTION_CUST_ATTR pa ");
		sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" AND status = 1) ");
		sql.append(" minus ");
		sql.append(" (SELECT * ");
		sql.append(" FROM PROMOTION_CUST_ATTR pa ");
		sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" AND status = 1 ");
		sql.append(" AND ( (object_type = 1 ");
		sql.append(" AND ( ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type = 1 ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		sql.append(" AND upper(VALUE) = upper(PA.FROM_VALUE) ");
		sql.append(" ) ) ");
		sql.append(" OR ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type = 2 ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		sql.append(" AND to_number(VALUE) >= to_number(PA.FROM_VALUE) ");
		sql.append(" AND to_number(value) <= to_number(pa.to_value) ");
		sql.append(" ) ) ");
		sql.append(" OR ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type = 3 ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		sql.append(" AND (to_date(VALUE, 'yyyy-MM-dd') >= to_date(PA.FROM_VALUE, 'yyyy-MM-dd') ");
		sql.append(" OR pa.from_value IS NULL) ");
		sql.append(" AND (to_date(value, 'yyyy-MM-dd') <= to_date(pa.to_value, 'yyyy-MM-dd') ");
		sql.append(" OR pa.to_value IS NULL) ");
		sql.append(" ) ) ");
		sql.append(" OR ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type IN (4,5) ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value av ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		// -----------------Kiem tra thuoc tinh mutil select
		sql.append(" and exists (select 1 from attribute_value_detail avd join promotion_cust_attr_detail pcd on avd.attribute_detail_id = pcd.object_id ");
		sql.append("             where avd.attribute_value_id = av.attribute_value_id and pcd.promotion_cust_attr_id = pa.promotion_cust_attr_id and pcd.status = 1 and avd.status = 1)");

		sql.append(" ) ) ) ) ");
		sql.append(" OR (object_type = 2 ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" WHERE status = 1 ");
		sql.append(" AND PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND OBJECT_ID = ");
		sql.append(" (SELECT CHANNEL_TYPE_ID FROM CUSTOMER WHERE CUSTOMER_ID = ? AND status = 1 ");
		params.add(customerId);
		sql.append(" ) ");
		sql.append(" ) ) ");
		sql.append(" OR (object_type = 3 ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" WHERE status = 1 ");
		sql.append(" AND PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND OBJECT_ID IN ");
		sql.append(" (SELECT SALE_LEVEL_CAT_ID FROM CUSTOMER_CAT_LEVEL WHERE CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" ) ) ) ");
		sql.append(" ) ");
		sql.append(" ) ");
		sql.append(" ) ");
		sql.append(" ) ");
		// Kiem tra thuoc tinh KH
		sql.append("   ) ");*/
		
		sql.append(" and (not exists ");
		sql.append(" 	(select 1 ");
		sql.append(" 	 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 	 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 	  and status = 1) ");
		sql.append("    or (exists ");
		sql.append(" 		(select 1 ");
		sql.append(" 		 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 		 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 		  and status = 1) ");
		sql.append(" 	   and (not exists ( ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						  and status = 1) ");
		sql.append(" 						 minus ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						   and status = 1 ");
		sql.append(" 						   and ( ");
		sql.append(" 						   		(object_type = 1 ");
		sql.append(" 								 and ( ");
		sql.append(" 								 	(exists ");
		sql.append(" 										(select 1 ");
		sql.append(" 										 from customer_attribute ");
		sql.append(" 										 where type = 1 ");
		sql.append(" 										  and customer_attribute_id = pa.object_id ");
		sql.append(" 										  and status = 1) ");
		sql.append(" 										and exists ( ");
		sql.append(" 											select 1 ");
		sql.append(" 											from customer_attribute_detail ");
		sql.append(" 											where customer_id = ? ");
		params.add(customerId);
		sql.append(" 											and status = 1 ");
		sql.append(" 											and customer_attribute_id = pa.object_id ");
		sql.append(" 											and upper(value) = upper(pa.from_value) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 2 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and to_number(value) >= to_number(pa.from_value) ");
		sql.append(" 												and to_number(value) <= to_number(pa.to_value) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 3 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') >= to_date(pa.from_value, 'yyyy-mm-dd') or pa.from_value is null) ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') <= to_date(pa.to_value, 'yyyy-mm-dd') or pa.to_value is null) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type in (4, 5) ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail cad ");
		sql.append(" 												join promotion_cust_attr_detail pcad  ");
		sql.append(" 													on pcad.object_type = 1  ");
		sql.append(" 													and pcad.status = 1 ");
		sql.append(" 													and cad.customer_attribute_enum_id = pcad.object_id ");
		sql.append(" 												where cad.customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and cad.status = 1 ");
		sql.append(" 												and pcad.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
		sql.append(" 												and cad.customer_attribute_id = pa.object_id ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 								) ");
		sql.append(" 								or (object_type = 2 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID = ");
		sql.append(" 										(select CHANNEL_TYPE_ID ");
		sql.append(" 										 from CUSTOMER ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 								or (object_type = 3 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID in ");
		sql.append(" 										(select SALE_LEVEL_CAT_ID ");
		sql.append(" 										 from CUSTOMER_CAT_LEVEL ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 							) ");
		sql.append(" 						) ");
		sql.append(" 				) ");
		sql.append(" 			) ");
		sql.append(" 	) ");
		sql.append(" ) ");
		

		sql.append(" union  ");
		sql.append(" select display_program_code as code, display_program_name as name, '' as promotionType, 2 as type, to_char('') as description, from_date as fromDate ");
		sql.append(" from display_program_vnm dp "); //tungtt
		sql.append(" where 1=1");
		sql.append(" and dp.display_program_vnm_id in (select display_program_id from display_pro_shop_map_vnm dsm where dsm.status = 1 and from_date < trunc(?) + 1 and (add_months(trunc(?, 'MM'), -2) <= to_date or to_date is null) and shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id))");
		params.add(orderDate);
		params.add(orderDate);
		params.add(shopId);
		sql.append(" and dp.display_program_code = ? ");
		params.add(commercialSupportCode);
		sql.append(" and from_date < trunc(?) + 1 and (add_months(trunc(?, 'MM') , -2) <= to_date or to_date is null) and status=?");
		params.add(orderDate);
		params.add(orderDate);
		params.add(ActiveType.RUNNING.getValue());

		countSql.append(sql.toString());
		countSql.append(" )");

		StringBuilder temp = new StringBuilder();
		temp.append("select * from (");
		temp.append(sql.toString());
		temp.append(") order by code");

		sql = temp;

		final String[] fieldNames = new String[] { "code", "name", "type",
				"description", "promotionType" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };

		if (kPaging != null)
			return repo.getListByQueryAndScalarPaginated(
					CommercialSupportVO.class, fieldNames, fieldTypes,
					sql.toString().replaceAll("  ", " "), countSql.toString().replaceAll("  ", " "), params, params,
					kPaging);
		else
			return repo.getListByQueryAndScalar(CommercialSupportVO.class,
					fieldNames, fieldTypes, sql.toString().replaceAll("  ", " "), params);
	}
	
	
	
	@Override
	public List<PromotionProgram> getListPromotionProgram(PromotionProgramFilter filter) throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate())) {
			return new ArrayList<PromotionProgram>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from promotion_program p where 1=1 ");

		if (filter.getStatus() != null) {
			sql.append(" and status=?");
			params.add(filter.getStatus().getValue());
		} else if (filter.getLstStatus() != null && filter.getLstStatus().size() > 0) {
			sql.append(" and status in (? ");
			params.add(filter.getLstStatus().get(0));
			for (int i = 1; i < filter.getLstStatus().size(); i++) {
				sql.append(",?");
				params.add(filter.getLstStatus().get(i));
			}
			sql.append(" ) ");
		} else {
			sql.append(" and status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}

		if (!StringUtility.isNullOrEmpty(filter.getPpCode())) {
			sql.append(" and promotion_program_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getPpCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getPpName())) {
			sql.append(" and unicode2english(promotion_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getPpName().toLowerCase())));
		}

		if (filter.getLstType() != null && filter.getLstType().size() > 0 && !StringUtility.isNullOrEmpty(filter.getLstType().get(0))) {
			sql.append(" and upper(type) in (''");
			for (String type : filter.getLstType())
				if (!StringUtility.isNullOrEmpty(type)) {
					sql.append(",?");
					params.add(type.toUpperCase());
				}
			sql.append(")");
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and (exists (");
			sql.append(" select 1 from promotion_shop_map m where m.promotion_program_id = p.promotion_program_id and m.status = 1 ");
			sql.append(" and m.shop_id in  ");
			sql.append(" (SELECT shop_id FROM shop where status = 1 ");
			sql.append(" START WITH shop_code = ? CONNECT BY PRIOR shop_id = parent_shop_id ");
			params.add(filter.getShopCode().toUpperCase());
			sql.append(" UNION ALL SELECT shop_id FROM shop where status = 1 START WITH shop_code = ? CONNECT BY PRIOR parent_shop_id = shop_id");
			params.add(filter.getShopCode().toUpperCase());
			sql.append(" )");
			if (filter.getFromDate() != null) {
				sql.append(" and (m.to_date is null or m.to_date >= trunc(?))");
				params.add(filter.getFromDate());
			}
			if (filter.getToDate() != null) {
				sql.append(" and (m.from_date is null or m.from_date < trunc(?) + 1)");
				params.add(filter.getToDate());
			}
			if (filter.getFlag() && filter.getToDate() != null) {
				sql.append(" and m.to_date < trunc(?) + 1");
				params.add(filter.getToDate());
			}
			sql.append("  )");
			sql.append(" OR (not exists (select 1 from promotion_shop_map m where m.promotion_program_id = p.promotion_program_id and m.status=1) and (1=0 ");
			if (!StringUtility.isNullOrEmpty(filter.getCreateUser())) {
				sql.append(" or p.CREATE_USER = ?");
				params.add(filter.getCreateUser());
			}
			if (Boolean.TRUE.equals(filter.getIsVNM())) {
				sql.append(" OR 1=1 ");
			}
			sql.append(")))");
			if (Boolean.FALSE.equals(filter.getIsAutoPromotion())) {
				sql.append(" and p.type in (select ap_param_code from ap_param where status = 1 and type = 'PROMOTION_MANUAL')");
			} else {
				sql.append(" and p.type in (select ap_param_code from ap_param where status = 1 and type = 'PROMOTION')");
			}
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerTypeCode())) {
			sql.append(" and exists (select 1 from promotion_customer_map pcm ");
			sql.append("	join promotion_shop_map psm on pcm.promotion_shop_map_id = psm.promotion_shop_map_id ");
			sql.append("	join channel_type ct on ct.channel_type_id = pcm.customer_type_id ");
			sql.append("	where pcm.type = ? and ct.channel_type_code like ? ");
			sql.append("    and psm.promotion_program_id = p.promotion_program_id and psm.status != ? and pcm.status != ?");
			params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER_TYPE.getValue());
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerTypeCode().toUpperCase()));
			params.add(ActiveType.DELETED.getValue());
			params.add(ActiveType.DELETED.getValue());
			if (filter.getFromDate() != null) {
				sql.append(" and (psm.to_date is null or psm.to_date >= trunc(?))");
				params.add(filter.getFromDate());
			}
			if (filter.getToDate() != null) {
				sql.append(" and (psm.from_date is null or psm.from_date < trunc(?) + 1)");
				params.add(filter.getToDate());
			}
			if (filter.getFlag() && filter.getToDate() != null) {
				sql.append(" and psm.to_date < trunc(?) + 1");
				params.add(filter.getToDate());
			}
			sql.append("             ) ");
		}
		if (filter.getFromDate() != null) {
			sql.append(" and (to_date is null or to_date >= trunc(?))");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and (from_date is null or from_date < trunc(?) + 1)");
			params.add(filter.getToDate());
		}
		if (filter.getFlag() && filter.getToDate() != null) {
			sql.append(" and to_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		if (filter.getIsAutoPromotion() != null) {
			if (filter.getIsAutoPromotion().equals(true)) {
				sql.append(" and type like 'ZV%'");
			} else {
				sql.append(" and type not like 'ZV%'");
			}
		}
		if (!StringUtility.isNullOrEmpty(filter.getPromotionName())) {
			sql.append(" and lower(promotion_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getPromotionName().toLowerCase()));// @PhuT: chinh lai the nay moi dung
		}
		sql.append(" and exists (select 1 from ap_param a where a.ap_param_code=p.type and a.status=1)");
		sql.append(" order by from_date desc, promotion_program_code");

		if (filter.getkPaging() == null) {
			return repo.getListBySQL(PromotionProgram.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(PromotionProgram.class, sql.toString(), params, filter.getkPaging());
	}
	
	@Override
	public List<PromotionProgram> getListDefaultPromotionProgram(PromotionProgramFilter filter) throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate())) {
			return new ArrayList<PromotionProgram>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from promotion_program p where 1=1 ");

		if (filter.getStatus() != null) {
			sql.append(" and status=?");
			params.add(filter.getStatus().getValue());
		} else if (filter.getLstStatus() != null && filter.getLstStatus().size() > 0) {
			sql.append(" and status in (? ");
			params.add(filter.getLstStatus().get(0));
			for (int i = 1; i < filter.getLstStatus().size(); i++) {
				sql.append(",?");
				params.add(filter.getLstStatus().get(i));
			}
			sql.append(" ) ");
		} else {
			sql.append(" and status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}

		if (!StringUtility.isNullOrEmpty(filter.getPpCode())) {
			sql.append(" and promotion_program_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getPpCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getPpName())) {
			sql.append(" and unicode2english(promotion_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getPpName().toLowerCase())));
		}

		if (filter.getLstType() != null && filter.getLstType().size() > 0 && !StringUtility.isNullOrEmpty(filter.getLstType().get(0))) {
			sql.append(" and upper(type) in (''");
			for (String type : filter.getLstType())
				if (!StringUtility.isNullOrEmpty(type)) {
					sql.append(",?");
					params.add(type.toUpperCase());
				}
			sql.append(")");
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and (exists (");
			sql.append(" select 1 from promotion_shop_map m where m.promotion_program_id = p.promotion_program_id and m.status = 1 ");
			sql.append(" and m.shop_id in  ");
			sql.append(" (SELECT shop_id FROM shop where status = 1 ");
			sql.append(" START WITH shop_code = ? CONNECT BY PRIOR shop_id = parent_shop_id ");
			params.add(filter.getShopCode().toUpperCase());
			sql.append(" UNION ALL SELECT shop_id FROM shop where status = 1 START WITH shop_code = ? CONNECT BY PRIOR parent_shop_id = shop_id");
			params.add(filter.getShopCode().toUpperCase());
			sql.append(" )");
			if (filter.getFromDate() != null) {
				sql.append(" and (m.to_date is null or m.to_date >= trunc(?))");
				params.add(filter.getFromDate());
			}
			if (filter.getToDate() != null) {
				sql.append(" and (m.from_date is null or m.from_date < trunc(?) + 1)");
				params.add(filter.getToDate());
			}
			if (filter.getFlag() && filter.getToDate() != null) {
				sql.append(" and m.to_date < trunc(?) + 1");
				params.add(filter.getToDate());
			}
			sql.append("  )");
			sql.append(" OR (not exists (select 1 from promotion_shop_map m where m.promotion_program_id = p.promotion_program_id and m.status=1) and (1=0 ");
			if (!StringUtility.isNullOrEmpty(filter.getCreateUser())) {
				sql.append(" or p.CREATE_USER = ?");
				params.add(filter.getCreateUser());
			}
			if (Boolean.TRUE.equals(filter.getIsVNM())) {
				sql.append(" OR 1=1 ");
			}
			sql.append(")))");
			if (Boolean.FALSE.equals(filter.getIsAutoPromotion())) {
				sql.append(" and p.type in (select ap_param_code from ap_param where status = 1 and type = 'PROMOTION_MANUAL')");
			} else {
				sql.append(" and p.type in (select ap_param_code from ap_param where status = 1 and type = 'PROMOTION')");
			}
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerTypeCode())) {
			sql.append(" and exists (select 1 from promotion_customer_map pcm ");
			sql.append("	join promotion_shop_map psm on pcm.promotion_shop_map_id = psm.promotion_shop_map_id ");
			sql.append("	join channel_type ct on ct.channel_type_id = pcm.customer_type_id ");
			sql.append("	where pcm.type = ? and ct.channel_type_code like ? ");
			sql.append("    and psm.promotion_program_id = p.promotion_program_id and psm.status != ? and pcm.status != ?");
			params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER_TYPE.getValue());
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerTypeCode().toUpperCase()));
			params.add(ActiveType.DELETED.getValue());
			params.add(ActiveType.DELETED.getValue());
			if (filter.getFromDate() != null) {
				sql.append(" and (psm.to_date is null or psm.to_date >= trunc(?))");
				params.add(filter.getFromDate());
			}
			if (filter.getToDate() != null) {
				sql.append(" and (psm.from_date is null or psm.from_date < trunc(?) + 1)");
				params.add(filter.getToDate());
			}
			if (filter.getFlag() && filter.getToDate() != null) {
				sql.append(" and psm.to_date < trunc(?) + 1");
				params.add(filter.getToDate());
			}
			sql.append("             ) ");
		}
		if (filter.getFromDate() != null) {
			sql.append(" and (to_date is null or to_date >= trunc(?))");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and (from_date is null or from_date < trunc(?) + 1)");
			params.add(filter.getToDate());
		}
		if (filter.getFlag() && filter.getToDate() != null) {
			sql.append(" and to_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		if (filter.getIsAutoPromotion() != null) {
			if (filter.getIsAutoPromotion().equals(true)) {
				sql.append(" and (type like 'ZV04' or  type like 'ZV19')");
			} else {
				sql.append(" and type not like 'ZV%'");
			}
		}
		if(filter.getIsDefaultDiscountType() != null){
			sql.append(" and is_default_discount_type = ?");
			params.add(filter.getIsDefaultDiscountType());
		}
		if(filter.getCalPriceVat() != null && filter.getCalPriceVat() != -1){
			sql.append(" and cal_price_vat = ?");
			params.add(filter.getCalPriceVat());
		}
		if (!StringUtility.isNullOrEmpty(filter.getPromotionName())) {
			sql.append(" and lower(promotion_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getPromotionName().toLowerCase()));// @PhuT: chinh lai the nay moi dung
		}
		sql.append(" and exists (select 1 from ap_param a where a.ap_param_code=p.type and a.status=1)");
		sql.append(" order by from_date desc, promotion_program_code");

		if (filter.getkPaging() == null) {
			return repo.getListBySQL(PromotionProgram.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(PromotionProgram.class, sql.toString(), params, filter.getkPaging());
	}

	@Override
	public List<PromotionProgram> getPromotionProgramByProductAndShopAndCustomer(
			long productId, long shopId, long customerId, Date orderDate,
			Long saleOrderId, Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		Customer customer = customerDAO.getCustomerById(customerId);
		if (customer == null) {
			return null;
		}
		sql.append("  SELECT pp.*  ");
		sql.append("  FROM promotion_program pp  ");
		sql.append("  WHERE (pp.status   = 1 and pp.type not in ('ZM', 'ZD', 'ZH', 'ZT', 'ZV23') ");
		sql.append("  AND pp.from_date < TRUNC(?) + 1  ");
		sql.append("  AND (pp.to_date    >= TRUNC(?) or pp.to_date is null)");
		params.add(orderDate);
		params.add(orderDate);
		sql.append(" and exists ");
		sql.append(" ( ");
		sql.append(" 	select 1 from product_group pg ");
		sql.append(" 	where pg.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 	and pg.group_type =1 ");
		sql.append(" 	and pg.status = 1 ");
		sql.append(" 	and exists ");
		sql.append(" 		( ");
		sql.append(" 			select 1 from group_level gl ");
		sql.append(" 			where gl.product_group_id = pg.product_group_id ");
		sql.append(" 			and gl.has_product = 1 and gl.status = 1 ");
		sql.append(" 			and exists ");
		sql.append(" 				( ");
		sql.append(" 					select 1 from group_level_detail gld ");
		sql.append(" 					where gl.GROUP_LEVEL_ID  = GLD.GROUP_LEVEL_ID ");
		sql.append(" 					and GLD.product_id          = ? ");
		params.add(productId);
		sql.append(" 					AND gld.STATUS               = 1 ");
		sql.append(" 				) ");
		sql.append(" 		) ");
		sql.append(" ) ");
		sql.append("  AND EXISTS  ");
		sql.append("    (SELECT 1  ");
		sql.append("    FROM promotion_shop_map psm  ");
		sql.append("    WHERE psm.promotion_program_id = pp.promotion_program_id and status = 1 ");
		sql.append("    AND (psm.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id)  ) ");
		params.add(shopId);
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("    AND psm.from_date < TRUNC(?) + 1  ");
		sql.append("    AND (psm.to_date    >= TRUNC(?) or psm.to_date is null) ");
		params.add(orderDate);
		params.add(orderDate);
		sql.append("    AND ( (quantity_max IS NULL OR NVL(quantity_received_total, 0) < quantity_max) ) ");
		//end
		
		//TODO vuongmq 13/01/2015; comment khong kiem tra CTKM cho NVBH, KH
		//Kiem tra KM ap dung cho KH
		/*sql.append("    AND  ");
		sql.append("       ( ");
		sql.append("         (not exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1) and ((psm.quantity_max is null or NVL(psm.quantity_received_total, 0) < psm.quantity_max)) )");
		sql.append("         or ");
		sql.append("         exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status  = 1 and customer_id = ? and ((quantity_max is null or NVL(quantity_received_total, 0) < quantity_max)) ");
		params.add(customerId);
		sql.append("       ) ");
		sql.append("    ) ");
		//End kiem tra KM ap dung cho KH
		//Kiem tra KM ap dung cho NVBH
		sql.append(" AND (  ");
		sql.append("         (not exists (select 1 from promotion_staff_map where promotion_shop_map_id = psm.promotion_shop_map_id and shop_id = ? and status = 1) and ((psm.quantity_max is null or NVL(psm.quantity_received_total, 0) < psm.quantity_max)) )");
		sql.append("         or ");
		sql.append("         exists (select 1 from promotion_staff_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and staff_id = ? and shop_id = ? and ((quantity_max is null or NVL(quantity_received_total, 0) < quantity_max))) ");
		params.add(shopId);
		params.add(staffId);
		params.add(shopId);
		sql.append(" 	 ) ");*/
		//end Kiem tra KM ap dung cho NVBH
		
		// Kiem tra thuoc tinh KH
		sql.append(" and (not exists ");
		sql.append(" 	(select 1 ");
		sql.append(" 	 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 	 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 	  and status = 1) ");
		sql.append("    or (exists ");
		sql.append(" 		(select 1 ");
		sql.append(" 		 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 		 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 		  and status = 1) ");
		sql.append(" 	   and (not exists ( ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						  and status = 1) ");
		sql.append(" 						 minus ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						   and status = 1 ");
		sql.append(" 						   and ( ");
		sql.append(" 						   		(object_type = 1 ");
		sql.append(" 								 and ( ");
		sql.append(" 								 	(exists ");
		sql.append(" 										(select 1 ");
		sql.append(" 										 from customer_attribute ");
		sql.append(" 										 where type = 1 ");
		sql.append(" 										  and customer_attribute_id = pa.object_id ");
		sql.append(" 										  and status = 1) ");
		sql.append(" 										and exists ( ");
		sql.append(" 											select 1 ");
		sql.append(" 											from customer_attribute_detail ");
		sql.append(" 											where customer_id = ? ");
		params.add(customerId);
		sql.append(" 											and status = 1 ");
		sql.append(" 											and customer_attribute_id = pa.object_id ");
		sql.append(" 											and upper(value) = upper(pa.from_value) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 2 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and to_number(value) >= to_number(pa.from_value) ");
		sql.append(" 												and to_number(value) <= to_number(pa.to_value) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 3 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') >= to_date(pa.from_value, 'yyyy-mm-dd') or pa.from_value is null) ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') <= to_date(pa.to_value, 'yyyy-mm-dd') or pa.to_value is null) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type in (4, 5) ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail cad ");
		sql.append(" 												join promotion_cust_attr_detail pcad  ");
		sql.append(" 													on pcad.object_type = 1  ");
		sql.append(" 													and pcad.status = 1 ");
		sql.append(" 													and cad.customer_attribute_enum_id = pcad.object_id ");
		sql.append(" 												where cad.customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and cad.status = 1 ");
		sql.append(" 												and pcad.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
		sql.append(" 												and cad.customer_attribute_id = pa.object_id ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 								) ");
		sql.append(" 								or (object_type = 2 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID = ");
		sql.append(" 										(select CHANNEL_TYPE_ID ");
		sql.append(" 										 from CUSTOMER ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 								or (object_type = 3 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID in ");
		sql.append(" 										(select SALE_LEVEL_CAT_ID ");
		sql.append(" 										 from CUSTOMER_CAT_LEVEL ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 							) ");
		sql.append(" 						) ");
		sql.append(" 				) ");
		sql.append(" 			) ");
		sql.append(" 	) ");
		sql.append(" ) ");
		
		
		sql.append("    )  ");
		sql.append(")");
		if (saleOrderId != null) {
			sql.append(" or (pp.promotion_program_code in (select program_code from sale_order_detail sod ");
			sql.append("	where sod.sale_order_id = ? and sod.program_type != 2 and order_date >= trunc(?) and order_date < trunc(?) + 1)"); // khac cham trung bay
			sql.append("	and pp.type not in ('ZV19', 'ZV20', 'ZV21'))"); // khac cham trung bay
			params.add(saleOrderId);
			params.add(orderDate);
			params.add(orderDate);
		}
		return repo.getListBySQL(PromotionProgram.class, sql.toString().replaceAll("  ", " "),params);
	}

	private void deleteAllPromotionProgramDetail(Long promotionProgramId)
			throws DataAccessException {
		String sql = "delete from promotion_program_detail where promotion_program_id=?";
		List<Object> params = new ArrayList<Object>();
		params.add(promotionProgramId);
		repo.executeSQLQuery(sql, params);
	}

	@Override
	public List<PromotionCustAttrVO> getListPromotionCustAttrVOCanBeSet(
			KPaging<PromotionCustAttrVO> kPaging, long promotionProgramId)
			throws DataAccessException {

		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		selectSql.append("SELECT *");
		selectSql.append(" FROM");
		selectSql
				.append("   (SELECT customer_attribute_id objectId, 1 AS objectType, TO_CHAR(name) name ");
		selectSql.append("   FROM customer_attribute");
		selectSql.append("   WHERE type <> 6 ");
		selectSql.append("   AND status       = 1");
		selectSql.append("   ORDER BY code");
		selectSql.append("   )");
		selectSql.append(" UNION ALL");
		selectSql
				.append(" SELECT 0 objectId, 2 AS objectType, TO_CHAR('Loại khách hàng') name FROM dual");
		selectSql.append(" UNION ALL");
		selectSql
				.append(" SELECT 0 objectId, 3 AS objectType, TO_CHAR('Mức doanh số') name FROM dual");
		selectSql.append(" MINUS");
		selectSql
				.append(" SELECT nvl(object_id,0) objectId, object_type AS objectType, (");
		selectSql.append("   CASE");
		selectSql.append("     WHEN object_type = 1");
		selectSql
				.append("     then (SELECT TO_CHAR(name) FROM customer_attribute WHERE customer_attribute_id = object_id) ");
		selectSql.append("     WHEN object_type = 2");
		selectSql.append("     then TO_CHAR('Loại khách hàng')");
		selectSql.append("     WHEN object_type = 3");
		selectSql.append("     then TO_CHAR('Mức doanh số')");
		selectSql.append("   END)        AS name");
		selectSql.append(" FROM promotion_cust_attr");
		selectSql.append(" WHERE promotion_program_id = ? and status = 1");
		selectSql.append(" order by name");
		params.add(promotionProgramId);

		countSql.append("select count(1) as count from(");
		countSql.append(selectSql.toString());
		countSql.append(")");
		String[] fieldNames = { "objectId", "objectType", "name" };

		Type[] fieldTypes = { StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING };
		List<PromotionCustAttrVO> lst = null;
		if (kPaging == null) {
			lst = repo.getListByQueryAndScalar(PromotionCustAttrVO.class,
					fieldNames, fieldTypes, selectSql.toString(), params);
		} else {
			lst = repo.getListByQueryAndScalarPaginated(
					PromotionCustAttrVO.class, fieldNames, fieldTypes,
					selectSql.toString(), countSql.toString(), params, params,
					kPaging);
		}
		return lst;

	}

	@Override
	public List<PromotionCustAttrVO> getListPromotionCustAttrVOAlreadySet(
			KPaging<PromotionCustAttrVO> kPaging, long promotionProgramId)
			throws DataAccessException {

		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		selectSql
				.append(" SELECT object_id objectId, object_type AS objectType, (");
		selectSql.append("   CASE");
		selectSql.append("     WHEN object_type = 1");
		selectSql
				.append("     then (select TO_CHAR(name) from customer_attribute where status =1 and  customer_attribute_id = object_id)");
		selectSql.append("     WHEN object_type = 2");
		selectSql.append("     then TO_CHAR('Loại khách hàng')");
		selectSql.append("     WHEN object_type = 3");
		selectSql.append("     then TO_CHAR('Mức doanh số')");
		selectSql.append("   END)        AS name");
		selectSql.append(" FROM promotion_cust_attr");
		selectSql.append(" WHERE promotion_program_id = ? and status = 1");
		selectSql.append(" order by seq");
		params.add(promotionProgramId);

		countSql.append("select count(1) as count from(");
		countSql.append(selectSql.toString());
		countSql.append(")");
		String[] fieldNames = { "objectId", "objectType", "name" };

		Type[] fieldTypes = { StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING };
		List<PromotionCustAttrVO> lst = null;
		if (kPaging == null) {
			lst = repo.getListByQueryAndScalar(PromotionCustAttrVO.class,
					fieldNames, fieldTypes, selectSql.toString(), params);
		} else {
			lst = repo.getListByQueryAndScalarPaginated(
					PromotionCustAttrVO.class, fieldNames, fieldTypes,
					selectSql.toString(), countSql.toString(), params, params,
					kPaging);
		}
		return lst;

	}

	@Override
	public List<RptPromotionProgramDetailVO> getListRptPromotionProgramDetail(
			KPaging<RptPromotionProgramDetailVO> kPaging, Date fromDate,
			Date toDate, Long shopId, Long staffId) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptPromotionProgramDetailVO>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" select pp.promotion_program_code as promotionProgramCode,");
		sql.append("       pp.promotion_program_name as promotionProgramName,");
		sql.append("       s.staff_code as staffCode,");
		sql.append("       s.staff_name as staffName,");
		sql.append("       p.product_code as productCode,");
		sql.append("       p.product_name as productName,");
		sql.append("       sod.quantity as quantity,");
		sql.append("       (select sod.quantity*price from price pr where pr.product_id = p.product_id and (pr.from_date is null or trunc(pr.from_date) <= so.order_date) and (pr.to_date is null or trunc(pr.to_date) + 1 > so.order_date) and pr.status != -1) as freeProductAmount");

		fromSql.append(" from promotion_program pp ");
		fromSql.append(" 	join sale_order_detail sod on sod.is_free_item = 1");
		fromSql.append(" 		and sod.program_type in (?, ?)");
		params.add(ProgramType.AUTO_PROM.getValue());
		params.add(ProgramType.MANUAL_PROM.getValue());
		fromSql.append(" 		and sod.program_code = pp.promotion_program_code");
		fromSql.append(" 	join sale_order so on so.sale_order_id = sod.sale_order_id");
		fromSql.append(" 		and so.approved = ? and so.order_type = ?");
		params.add(SaleOrderStatus.APPROVED.getValue());
		params.add(OrderType.IN.getValue());
		if (shopId != null) {
			fromSql.append(" 		and so.shop_id = ?");
			params.add(shopId);
		}
		fromSql.append(" 	join staff s on so.staff_id = s.staff_id ");
		if (staffId != null) {
			fromSql.append(" 		and s.staff_id = ?");
			params.add(staffId);
		}
		fromSql.append("   join product p on p.product_id = sod.product_id");
		fromSql.append(" where 1 = 1");
		if (toDate != null) {
			fromSql.append("	and pp.from_date <= trunc(?) + 1");
			params.add(toDate);
		}
		if (fromDate != null) {
			fromSql.append(" 	and pp.to_date >= trunc(?)");
			params.add(fromDate);
		}
		fromSql.append("   ");
		fromSql.append("   ");
		fromSql.append(" 	");
		sql.append(fromSql);
		sql.append(" 	");
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count");
		countSql.append(fromSql);

		String[] fieldNames = { "promotionProgramCode", // 0
				"promotionProgramName", // 1
				"staffCode", // 2
				"staffName", // 3
				"productCode", // 4
				"productName", // 5
				"quantity", // 6
				"freeProductAmount", // 7
		};

		Type[] fieldTypes = { StandardBasicTypes.STRING, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.INTEGER, // 6
				StandardBasicTypes.BIG_DECIMAL, // 7
		};

		if (kPaging == null)
			return repo.getListByQueryAndScalar(
					RptPromotionProgramDetailVO.class, fieldNames, fieldTypes,
					sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(
					RptPromotionProgramDetailVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params,
					kPaging);
	}

	@Override
	public List<RptPromotionDetailDataVO> getDataForPromotionDetailReport(
			Long shopId, List<Long> staffIds, Date fromDate, Date toDate)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptPromotionDetailDataVO>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT PRO.PROMOTION_PROGRAM_CODE promotionCode, PRO.PROMOTION_PROGRAM_NAME promotionName, PRO.ORDER_DATE orderDate, PRO.SALE_ORDER_ID orderId,  ");
		sql.append("     PRO.PRODUCT_ID productId, PRO.PRODUCT_CODE productCode, PRO.PRODUCT_NAME productName, PRO.IS_FREE_ITEM isFreeItem,  ");
		sql.append("     PRO.CUSTOMER_CODE customerCode, PRO.CHANNEL_TYPE_NAME channelName, PRO.TYPE promotionType, ");
		sql.append("     PRO.STAFF_CODE staffCode, PRO.STAFF_NAME staffName, ");
		sql.append("     NVL(PRO.QUANTITY, 0) AS productQuantity, ");
		sql.append("     NVL(PRO.AMOUNT, 0) AS productAmount ");
		sql.append(" FROM (SELECT DISTINCT PROM.PROMOTION_PROGRAM_ID, PROM.PROMOTION_PROGRAM_CODE, PROM.TYPE, PROM.PROMOTION_PROGRAM_NAME ");
		sql.append("     FROM PROMOTION_PROGRAM PROM, PROMOTION_PROGRAM_DETAIL PROM_D ");
		sql.append("     WHERE PROM.PROMOTION_PROGRAM_ID = PROM_D.PROMOTION_PROGRAM_ID ");
		sql.append("         AND PROM.FROM_DATE >= TRUNC(?) ");
		sql.append("         AND (PROM.TO_DATE <= TRUNC(?) OR PROM.TO_DATE IS NULL)) PRO ");

		params.add(fromDate);
		params.add(toDate);

		sql.append(" JOIN (SELECT DISTINCT SO.SALE_ORDER_ID, SO.ORDER_DATE, SO_D.PROGRAM_CODE ");
		sql.append("     FROM SALE_ORDER SO, SALE_ORDER_DETAIL SO_D ");
		sql.append("     WHERE SO.SALE_ORDER_ID = SO_D.SALE_ORDER_ID ");
		sql.append("         AND SO.APPROVED = ? ");
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append("         AND SO.ORDER_TYPE = ? ");
		params.add(OrderType.IN.getValue());
		sql.append("         AND SO.SHOP_ID = ? ");
		sql.append("         AND SO.ORDER_DATE BETWEEN TRUNC(?) AND TRUNC(?) ");

		params.add(shopId);
		params.add(fromDate);
		params.add(toDate);

		if (null != staffIds && staffIds.size() > 0) {
			sql.append("         AND SO.STAFF_ID IN (");

			for (int i = 0; i < staffIds.size(); i++) {
				if (i < staffIds.size() - 1) {
					sql.append("?,");
				} else {
					sql.append("?");
				}

				params.add(staffIds.get(i));
			}

			sql.append(") ");
		}

		sql.append("         AND SO_D.IS_FREE_ITEM = 1 AND SO_D.PROGRAM_TYPE IN (0, 1))FREE_PRO  ");
		sql.append(" ON PRO.PROMOTION_PROGRAM_CODE = FREE_PRO.PROGRAM_CODE ");
		sql.append(" JOIN (SELECT SO.SALE_ORDER_ID, SO.ORDER_DATE,  ");
		sql.append("         SO_D.PRODUCT_ID, SO_D.IS_FREE_ITEM, SO_D.QUANTITY, SO_D.AMOUNT,  ");
		sql.append("         PRO.PRODUCT_CODE, PRO.PRODUCT_NAME,  ");
		sql.append("         CUS.CUSTOMER_CODE, CHAN.CHANNEL_TYPE_NAME, ");
		sql.append("         STA.STAFF_CODE, STA.STAFF_NAME ");
		sql.append("     FROM SALE_ORDER SO, SALE_ORDER_DETAIL SO_D, PRODUCT PRO, CUSTOMER CUS, CHANNEL_TYPE CHAN, STAFF STA ");
		sql.append("     WHERE SO.SALE_ORDER_ID = SO_D.SALE_ORDER_ID ");
		sql.append("         AND SO.APPROVED = ? ");
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append("         AND SO.ORDER_TYPE = ? ");
		params.add(OrderType.IN.getValue());
		sql.append("         AND SO.SHOP_ID = ? ");
		sql.append("         AND SO.ORDER_DATE BETWEEN TRUNC(?) AND TRUNC(?) ");

		params.add(shopId);
		params.add(fromDate);
		params.add(toDate);

		if (null != staffIds && staffIds.size() > 0) {
			sql.append("         AND SO.STAFF_ID IN (");

			for (int i = 0; i < staffIds.size(); i++) {
				if (i < staffIds.size() - 1) {
					sql.append("?,");
				} else {
					sql.append("?");
				}

				params.add(staffIds.get(i));
			}

			sql.append(") ");
		}

		sql.append("         AND PRO.PRODUCT_ID = SO_D.PRODUCT_ID ");
		sql.append("         AND SO.CUSTOMER_ID = CUS.CUSTOMER_ID ");
		sql.append("         AND CUS.CHANNEL_TYPE_ID = CHAN.CHANNEL_TYPE_ID ");
		sql.append("         AND CHAN.TYPE = ? ");
		params.add(ChannelTypeType.CUSTOMER.getValue());
		sql.append("         AND SO.STAFF_ID = STA.STAFF_ID ");
		sql.append("         AND SO_D.IS_FREE_ITEM IN (0, 1))PRO  ");
		sql.append(" ON FREE_PRO.SALE_ORDER_ID = PRO.SALE_ORDER_ID ");
		sql.append("         AND ((PRO.IS_FREE_ITEM <> 1  ");
		sql.append("                     AND EXISTS (SELECT 1  ");
		sql.append("                         FROM PROMOTION_PROGRAM PROM, PROMOTION_PROGRAM_DETAIL PROM_D   ");
		sql.append("                         WHERE    PROM.PROMOTION_PROGRAM_ID = PROM_D.PROMOTION_PROGRAM_ID ");
		sql.append("                             AND PROM.PROMOTION_PROGRAM_CODE =  FREE_PRO.PROGRAM_CODE   ");
		sql.append("                             AND PRO.PRODUCT_ID = PROM_D.PRODUCT_ID )) ");
		sql.append("                 OR PRO.IS_FREE_ITEM = 1) ");
		sql.append(" ORDER BY promotionCode ASC, orderDate DESC, orderId ASC,  isFreeItem ASC ");

		String[] fieldNames = { "promotionCode",// 1
				"promotionName",// 2
				"orderDate",// 3
				"orderId",// 4
				"productId",// 5
				"productCode",// 6
				"productName",// 7
				"isFreeItem",// 8
				"customerCode",// 9
				"channelName",// 10
				"promotionType",// 10'
				"staffCode",// 11
				"staffName",// 12
				"productQuantity",// 13
				"productAmount"// 14
		};

		Type[] fieldTypes = { StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.DATE, // 3
				StandardBasicTypes.LONG, // 4
				StandardBasicTypes.LONG, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.STRING, // 7
				StandardBasicTypes.INTEGER, // 8
				StandardBasicTypes.STRING, // 9
				StandardBasicTypes.STRING, // 10
				StandardBasicTypes.STRING, // 10'
				StandardBasicTypes.STRING, // 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.LONG, // 13
				StandardBasicTypes.BIG_DECIMAL, // 14
		};

		return repo.getListByQueryAndScalar(RptPromotionDetailDataVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public Boolean checkProductInOtherPromotionProgram(String productCode,
			Long promotionProgramId, Long promotionProgramDetailId)
			throws DataAccessException {
		if (productCode == null || promotionProgramId == null) {
			throw new IllegalArgumentException(
					"productId or promotionProgramId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		PromotionProgram pg = this.getPromotionProgramById(promotionProgramId);

		sql.append(" select count(1) as count from promotion_program pp ");
		sql.append(" 	join promotion_program_detail ppd on pp.promotion_program_id = ppd.promotion_program_id");
		sql.append("	join product p on p.product_id = ppd.product_id");
		sql.append("	where p.product_code = ? and pp.status = ?");
		params.add(productCode.toUpperCase());
		params.add(ActiveType.RUNNING.getValue());
		if (pg.getFromDate() != null) {
			sql.append("	and ((pp.to_date is not null and pp.to_date >= trunc(?)) or pp.to_date is null) ");
			params.add(pg.getFromDate());
		}
		if (pg.getToDate() != null) {
			sql.append("	and ((from_date is not null and from_date < trunc(?) + 1) or from_date is null)");
			params.add(pg.getToDate());
		}
		if (promotionProgramDetailId != null) {
			sql.append("	and promotion_program_detail_id != ?");
			params.add(promotionProgramDetailId);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public Boolean checkProductInOtherPromotionProgram(String productCode,
			Date fromDate, Date toDate, Long promotionProgramId)
			throws DataAccessException {
		if (productCode == null) {
			throw new IllegalArgumentException(
					"productId or promotionProgramId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select count(1) as count from promotion_program pp ");
		sql.append(" 	join promotion_program_detail ppd on pp.promotion_program_id = ppd.promotion_program_id");
		sql.append("	join product p on p.product_id = ppd.product_id");
		sql.append("	where p.product_code = ? and pp.status = ?");
		params.add(productCode.toUpperCase());
		params.add(ActiveType.RUNNING.getValue());
		if (fromDate != null) {
			sql.append("	and ((pp.to_date is not null and pp.to_date >= trunc(?)) or pp.to_date is null) ");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("	and ((from_date is not null and from_date < trunc(?) + 1) or from_date is null)");
			params.add(toDate);
		}
		if (promotionProgramId != null) {
			sql.append("	and pp.promotion_program_id != ?");
			params.add(promotionProgramId);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public Boolean checkExistProductTimeShopInActive(Long promotionProgramId,
			String startDate, String endDate) throws DataAccessException {
		if (promotionProgramId == null || startDate == null
				|| startDate.length() == 0) {
			throw new IllegalArgumentException(
					"productId or promotionProgramId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select count(1) as count from promotion_program_detail ppd1, promotion_shop_map psm1 ");
		sql.append(" where  1 = 1  ");
		sql.append(" and ppd1.promotion_program_id = ?  ");
		params.add(promotionProgramId);
		sql.append(" and ppd1.promotion_program_id = psm1.promotion_program_id ");
		sql.append(" and psm1.status = 1 ");
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append(" and psm1.from_date < TRUNC(sysdate) + 1  ");
		sql.append(" and (psm1.to_date  >= TRUNC(sysdate) or psm1.to_date is null) ");
		//end
		sql.append(" and exists ( ");
		sql.append(" select ppd.* from promotion_program pp  join promotion_program_detail ppd  on ");
		sql.append(" pp.promotion_program_id = ppd.promotion_program_id  ");
		sql.append(" where 1 = 1 ");
		sql.append(" and pp.status = 1 and pp.promotion_program_id <> ? ");
		params.add(promotionProgramId);
		sql.append(" and ppd1.product_id = ppd.product_id ");

		// check duplicate timing
		if (endDate == null || endDate.length() == 0) {
			sql.append(" and (pp.to_date is null ");
			sql.append(" or (pp.to_date is not null and (pp.from_date <= TRUNC(to_date(?, 'DD/MM/YYYY')) and pp.to_date >= TRUNC(to_date(?, 'DD/MM/YYYY')))) ");
			sql.append(" or (pp.to_date is not null and pp.from_date >= TRUNC(to_date(?, 'DD/MM/YYYY')))) ");
			params.add(startDate);
			params.add(startDate);
			params.add(startDate);
		} else {
			sql.append(" and ((pp.to_date is null and pp.from_date <= TRUNC(to_date(?, 'DD/MM/YYYY'))) ");
			sql.append(" or (pp.to_date is not null and pp.from_date <= TRUNC(to_date(?, 'DD/MM/YYYY')) and pp.to_date >= TRUNC(to_date(?, 'DD/MM/YYYY'))) ");
			sql.append(" or (pp.to_date is not null and pp.from_date >= TRUNC(to_date(?, 'DD/MM/YYYY')) and pp.from_date <= TRUNC(to_date(?, 'DD/MM/YYYY')))) ");
			params.add(endDate);
			params.add(startDate);
			params.add(startDate);
			params.add(startDate);
			params.add(endDate);
		}

		sql.append(" and exists ( ");
		sql.append(" select 1 from promotion_program pp2  join promotion_shop_map psm2 on pp2.promotion_program_id = psm2.promotion_program_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and pp2.status = 1 ");
		sql.append(" and psm2.status = 1 ");
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append(" and psm2.from_date < TRUNC(sysdate) + 1  ");
		sql.append(" and (psm2.to_date  >= TRUNC(sysdate) or psm2.to_date is null) ");
		//end
		sql.append(" and pp2.promotion_program_id <> ? ");
		params.add(promotionProgramId);
		sql.append(" AND psm2.shop_id IN (select shop_id from shop start with shop_id = psm1.shop_id connect by prior parent_shop_id = shop_id union select shop_id from shop start with shop_id = psm1.shop_id connect by prior shop_id = parent_shop_id) ");
		sql.append(" and pp.promotion_program_id = pp2.promotion_program_id ");
		sql.append(" )) ");
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public List<PromotionProgram> getCheckQuantityPromotionShopMap(
			Long saleOrderId) throws DataAccessException {
		if (saleOrderId == null) {
			throw new IllegalArgumentException("saleOrderId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT pp.*");
		sql.append(" FROM sale_order so,");
		sql.append(" sale_order_detail sod,");
		sql.append(" promotion_shop_map psm,");
		sql.append(" promotion_program pp");
		sql.append(" WHERE so.sale_order_id      = sod.sale_order_id");
		sql.append(" AND so.sale_order_id 		 = ?");
		params.add(saleOrderId);
		sql.append(" AND sod.program_code        = pp.promotion_program_code");
		sql.append(" AND sod.program_type        = ?");
		params.add(ProgramType.AUTO_PROM.getValue());
		sql.append(" AND sod.shop_id             = psm.shop_id");
		sql.append(" AND pp.promotion_program_id = psm.promotion_program_id");
		sql.append(" AND psm.status              = ?");
		params.add(ActiveType.RUNNING.getValue());
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append(" AND psm.from_date < TRUNC(sysdate) + 1  ");
		sql.append(" AND (psm.to_date  >= TRUNC(sysdate) or psm.to_date is null) ");
		//end
		sql.append(" AND psm.quantity_max       <= psm.quantity_received");
		return repo.getListBySQL(PromotionProgram.class, sql.toString(), params);
	}

	// ------------promotion view----------------------------
	@Override
	public List<ZV03View> getZV03ViewHeader(KPaging<ZV03View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT d.product_id productId, ");
		sql.append("        (select product_code from product where product_id = d.product_id) productCode, ");
		sql.append("        (select product_name from product where product_id = d.product_id) productName, ");
		sql.append("        d.sale_qty quantity,  ");
		sql.append("        listagg(f.product_code || '(' || to_char(free_qty) || ')', ',') within GROUP (ORDER BY f.product_code) ");
		sql.append("               freeProductCodeStr, ");
		sql.append("        listagg(d.promotion_program_detail_id, ',') within GROUP (ORDER BY d.promotion_program_detail_id) ");
		sql.append("               pgDetailIdStr, ");
		sql.append("        listagg(to_char(f.product_name), ',') within GROUP (ORDER BY f.product_code) ");
		sql.append("               freeProductNameStr ");
		sql.append(" FROM promotion_program_detail d join product f on d.free_product_id = f.product_id ");
		sql.append(" WHERE d.promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" GROUP BY d.product_id, ");
		sql.append("   d.sale_qty ");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from  ");
		countSql.append(" (SELECT 1 FROM promotion_program_detail WHERE promotion_program_id = ? group by product_id, sale_qty) ");

		String[] fieldNames = { "productId", "productCode", "quantity",
				"productName", "freeProductCodeStr", "pgDetailIdStr",
				"freeProductNameStr" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV03View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV03View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV03View> getZV03ViewDetail(KPaging<ZV03View> paging,
			Long promotionProgramId, Long productId, Integer quant)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select p.product_id productId, p.product_code productCode, p.product_name productName, d.free_qty quantity, d.promotion_program_detail_id pgDetailId from promotion_program_detail d join product p on p.product_id = d.free_product_id ");
		sql.append("	where promotion_program_id = ? and d.product_id = ? and sale_qty = ?");
		params.add(promotionProgramId);
		params.add(productId);
		params.add(quant);

		countSql.append(" select count(1) count from promotion_program_detail where promotion_program_id = ? and product_id = ? and sale_qty = ?");

		String[] fieldNames = { "productId", "productCode", "quantity",
				"productName", "pgDetailId" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV03View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV03View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV03View> getZV06ViewHeader(KPaging<ZV03View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT d.product_id productId, ");
		sql.append("        (select product_code from product where product_id = d.product_id) productCode, ");
		sql.append("        (select product_name from product where product_id = d.product_id) productName, ");
		sql.append("        d.sale_amt amount,  ");
		sql.append("        listagg(f.product_code || '(' || to_char(free_qty) || ')', ',') within GROUP (ORDER BY f.product_code) ");
		sql.append("               freeProductCodeStr, ");
		sql.append("        listagg(d.promotion_program_detail_id, ',') within GROUP (ORDER BY d.promotion_program_detail_id) ");
		sql.append("               pgDetailIdStr , ");
		sql.append("        listagg(to_char(f.product_name), ',') within GROUP (ORDER BY f.product_code) ");
		sql.append("               freeProductNameStr ");
		sql.append(" FROM promotion_program_detail d join product f on d.free_product_id = f.product_id ");
		sql.append(" WHERE d.promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" GROUP BY d.product_id, ");
		sql.append("   d.sale_amt ");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from  ");
		countSql.append(" (SELECT 1 FROM promotion_program_detail WHERE promotion_program_id = ? group by product_id, sale_amt) ");

		String[] fieldNames = { "productId", "productCode", "amount",
				"productName", "freeProductCodeStr", "pgDetailIdStr",
				"freeProductNameStr" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV03View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV03View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV03View> getZV06ViewDetail(KPaging<ZV03View> paging,
			Long promotionProgramId, Long productId, Long amount)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select p.product_id productId, p.product_code productCode, p.product_name productName, d.free_qty quantity, d.promotion_program_detail_id pgDetailId from promotion_program_detail d join product p on p.product_id = d.free_product_id ");
		sql.append("	where promotion_program_id = ? and d.product_id = ? and sale_amt = ?");
		params.add(promotionProgramId);
		params.add(productId);
		params.add(amount);

		countSql.append(" select count(1) count from promotion_program_detail where promotion_program_id = ? and product_id = ? and sale_qty = ?");

		String[] fieldNames = { "productId", "productCode", "quantity",
				"productName", "pgDetailId" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV03View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV03View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV07ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT sale_qty quantity, disc_per discountPercent  ");
		sql.append(" 		,listagg(p.product_code || case when d.required = 1 then '*' end, ', ') within GROUP (ORDER BY p.product_code) productCodeStr ");
		sql.append("        ,listagg(d.promotion_program_detail_id, ',') within GROUP (ORDER BY d.promotion_program_detail_id) ");
		sql.append("               pgDetailIdStr ");
		sql.append(" 		,listagg(to_char(p.product_name), ', ') within GROUP (ORDER BY p.product_code) productNameStr ");
		sql.append(" FROM promotion_program_detail d ");
		sql.append(" JOIN product p ");
		sql.append(" ON d.product_id            = p.product_id ");
		sql.append(" WHERE promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" group by sale_qty, disc_per ");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from  ");
		countSql.append(" (SELECT 1 FROM promotion_program_detail WHERE promotion_program_id = ? group by sale_qty, disc_per) ");

		String[] fieldNames = { "quantity", "discountPercent",
				"productCodeStr", "pgDetailIdStr", "productNameStr" };
		Type[] fieldTypes = { StandardBasicTypes.INTEGER,
				StandardBasicTypes.FLOAT, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV07ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, Float discountPercent, Integer quantity)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.product_id) productName  ");
		sql.append("       ,d.required, d.promotion_program_detail_id pgDetailId, d.product_id productId ");
		sql.append(" from promotion_program_detail d where promotion_program_id = ? and sale_qty = ? and disc_per = ? ");
		params.add(promotionProgramId);
		params.add(quantity);
		params.add(discountPercent);

		countSql.append(" select count(1) count from promotion_program_detail d where promotion_program_id = ? and sale_qty = ? and disc_per = ?");

		String[] fieldNames = { "productCode", "productName", "required",
				"pgDetailId", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV08ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT sale_qty quantity, disc_amt discountAmount  ");
		sql.append(" 		,listagg(p.product_code || case when d.required = 1 then '*' end, ', ') within GROUP (ORDER BY p.product_code) productCodeStr ");
		sql.append("        ,listagg(d.promotion_program_detail_id, ',') within GROUP (ORDER BY d.promotion_program_detail_id) ");
		sql.append("               pgDetailIdStr ");
		sql.append(" 		,listagg(to_char(p.product_code), ', ') within GROUP (ORDER BY p.product_code) productNameStr ");
		sql.append(" FROM promotion_program_detail d ");
		sql.append(" JOIN product p ");
		sql.append(" ON d.product_id            = p.product_id ");
		sql.append(" WHERE promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" group by sale_qty, disc_amt ");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from  ");
		countSql.append(" (SELECT 1 FROM promotion_program_detail WHERE promotion_program_id = ? group by sale_qty, disc_amt) ");

		String[] fieldNames = { "quantity", "discountAmount", "productCodeStr",
				"pgDetailIdStr", "productNameStr" };
		Type[] fieldTypes = { StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV08ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal discountAmount, Integer quantity)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.product_id) productName  ");
		sql.append("       ,d.required, d.promotion_program_detail_id pgDetailId, d.product_id productId ");
		sql.append(" from promotion_program_detail d where promotion_program_id = ? and sale_qty = ? and disc_amt = ? ");
		params.add(promotionProgramId);
		params.add(quantity);
		params.add(discountAmount);

		countSql.append(" select count(1) count from promotion_program_detail d where promotion_program_id = ? and sale_qty = ? and disc_amt = ?");

		String[] fieldNames = { "productCode", "productName", "required",
				"pgDetailId", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV09ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH detail as (SELECT pr.product_code sale_prd,pr.product_name sale_prdname, fpr.product_code free_prd, fpr.product_name free_prdname, required, sale_qty, free_qty, promotion_program_detail_id ");
		sql.append(" FROM promotion_program_detail d join product pr on d.product_id = pr.product_id ");
		sql.append("     join product fpr on d.free_product_id = fpr.product_id ");
		sql.append(" WHERE promotion_program_id = ?), ");
		params.add(promotionProgramId);
		sql.append(" prd as (select distinct sale_prd, sale_prdname, sale_qty, required from detail), ");
		sql.append(" free as (select distinct free_prd, free_prdname, free_qty, sale_qty from detail) ");
		// listagg(p.product_code || case when d.required = 1 then '*' end, ',
		// ') within GROUP (ORDER BY p.product_code) productCodeStr
		sql.append(" select (select listagg(sale_prd || case when required = 1 then '*' end, ', ') within group (order by sale_prd) from prd where prd.sale_qty = detail.sale_qty) productCodeStr ");
		sql.append("       ,(select listagg(to_char(sale_prdname) , ', ') within group (order by sale_prd) from prd where prd.sale_qty = detail.sale_qty) productNameStr ");
		sql.append("       ,(select listagg(free_prd || '(' || free_qty || ')', ', ') within group (order by free_prd) from free where free.sale_qty = detail.sale_qty) freeProductCodeStr ");
		sql.append("       ,(select listagg(to_char(free_prdname) , ', ') within group (order by free_prd) from free where free.sale_qty = detail.sale_qty) freeProductNameStr ");
		sql.append("       , listagg(promotion_program_detail_id || ',') within group (order by promotion_program_detail_id) pgDetailIdStr ");
		sql.append("       , sale_qty quantity ");
		sql.append(" from detail group by sale_qty ");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" SELECT count(1) count FROM (select count(1) count from promotion_program_detail WHERE promotion_program_id = ? group by sale_qty) ");

		String[] fieldNames = { "productCodeStr", "productNameStr", "quantity",
				"freeProductCodeStr", "freeProductNameStr", "pgDetailIdStr" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV09ViewSaleProductDetail(
			KPaging<ZV07View> paging, Long promotionProgramId, Integer quantity)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.product_id) productName ");
		sql.append("       ,listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr ");
		sql.append("       ,required, d.product_id productId ");
		sql.append(" from promotion_program_detail d where promotion_program_id = ? and sale_qty = ? group by product_id, required ");
		params.add(promotionProgramId);
		params.add(quantity);

		countSql.append(" select count(distinct product_id) count from promotion_program_detail d where promotion_program_id = ? and sale_qty = ?");

		String[] fieldNames = { "productCode", "productName", "required",
				"pgDetailIdStr", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV09ViewFreeProductDetail(
			KPaging<ZV07View> paging, Long promotionProgramId, Integer quantity)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select product_code from product where product_id = d.free_product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.free_product_id) productName ");
		sql.append("       ,listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr ");
		sql.append("       ,free_qty quantity, d.free_product_id productId ");
		sql.append(" from promotion_program_detail d where promotion_program_id = ? and sale_qty = ? group by free_product_id, free_qty ");
		params.add(promotionProgramId);
		params.add(quantity);

		countSql.append(" select count(distinct free_product_id) count from promotion_program_detail d where promotion_program_id = ? and sale_qty = ?");

		String[] fieldNames = { "productCode", "productName", "quantity",
				"pgDetailIdStr", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV10ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT listagg(product_code || ");
		sql.append("   CASE WHEN required = 1 THEN '*' END, ', ') within GROUP (ORDER BY product_code) productCodeStr, ");
		sql.append("   sale_amt amount, ");
		sql.append("   listagg(to_char(product_name), ',') within group (order by product_code) productNameStr, ");
		sql.append("   listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr, ");
		sql.append("   disc_per discountPercent ");
		sql.append(" FROM promotion_program_detail d ");
		sql.append(" JOIN product p ");
		sql.append(" ON d.product_id            = p.product_id ");
		sql.append(" WHERE promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" GROUP BY sale_amt, ");
		sql.append("   disc_per ");

		countSql.append(" SELECT count(1) count FROM (select count(1) count from promotion_program_detail WHERE promotion_program_id = ? group by sale_amt, disc_per) ");

		String[] fieldNames = { "productCodeStr", "amount", "productNameStr",
				"discountPercent", "pgDetailIdStr" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING,
				StandardBasicTypes.FLOAT, StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV10ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal amount)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.product_id) productName ");
		sql.append("       ,listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr ");
		sql.append("       ,required, d.product_id productId ");
		sql.append(" from promotion_program_detail d where promotion_program_id = ? and sale_amt = ? group by product_id, required ");
		params.add(promotionProgramId);
		params.add(amount);

		countSql.append(" select count(distinct product_id) count from promotion_program_detail d where promotion_program_id = ? and sale_amt = ?");

		String[] fieldNames = { "productCode", "productName", "required",
				"pgDetailIdStr", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV11ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT listagg(product_code || ");
		sql.append("   CASE WHEN required = 1 THEN '*' END, ', ') within GROUP (ORDER BY product_code) productCodeStr, ");
		sql.append("   sale_amt amount, ");
		sql.append("   listagg(to_char(product_name), ',') within group (order by product_code) productNameStr, ");
		sql.append("   listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr, ");
		sql.append("   disc_amt discountAmount ");
		sql.append(" FROM promotion_program_detail d ");
		sql.append(" JOIN product p ");
		sql.append(" ON d.product_id            = p.product_id ");
		sql.append(" WHERE promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" GROUP BY sale_amt, ");
		sql.append("   disc_amt ");

		countSql.append(" SELECT count(1) count FROM (select count(1) count from promotion_program_detail WHERE promotion_program_id = ? group by sale_amt, disc_amt) ");

		String[] fieldNames = { "productCodeStr", "amount", "productNameStr",
				"discountAmount", "pgDetailIdStr" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV11ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal amount)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.product_id) productName ");
		sql.append("       ,listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr ");
		sql.append("       ,required, d.product_id productId ");
		sql.append(" from promotion_program_detail d where promotion_program_id = ? and sale_amt = ? group by product_id, required ");
		params.add(promotionProgramId);
		params.add(amount);

		countSql.append(" select count(distinct product_id) count from promotion_program_detail d where promotion_program_id = ? and sale_amt = ?");

		String[] fieldNames = { "productCode", "productName", "required",
				"pgDetailIdStr", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV12ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH detail as (SELECT pr.product_code sale_prd, fpr.product_code free_prd,pr.product_name sale_prdname, fpr.product_name free_prdname, required, sale_amt, free_qty, promotion_program_detail_id ");
		sql.append(" FROM promotion_program_detail d join product pr on d.product_id = pr.product_id ");
		sql.append("     join product fpr on d.free_product_id = fpr.product_id ");
		sql.append(" WHERE promotion_program_id = ?), ");
		params.add(promotionProgramId);
		sql.append(" prd as (select distinct sale_prd,sale_prdname, sale_amt, required from detail), ");
		sql.append(" free as (select distinct free_prd,free_prdname, free_qty, sale_amt from detail) ");
		sql.append(" select (select listagg(sale_prd || case when required = 1 then '*' end, ', ') within group (order by sale_prd) from prd where prd.sale_amt = detail.sale_amt) productCodeStr ");
		sql.append("       ,(select listagg(to_char(sale_prdname), ', ') within group (order by sale_prd) from prd where prd.sale_amt = detail.sale_amt) productNameStr");
		sql.append("       ,(select listagg(free_prd || '(' || free_qty || ')', ', ') within group (order by free_prd) from free where free.sale_amt = detail.sale_amt) freeProductCodeStr ");
		sql.append("       ,(select listagg(to_char(free_prdname), ', ') within group (order by free_prd) from free where free.sale_amt = detail.sale_amt) freeProductNameStr ");
		sql.append("       , listagg(promotion_program_detail_id || ',') within group (order by promotion_program_detail_id) pgDetailIdStr ");
		sql.append("       , sale_amt amount ");
		sql.append(" from detail group by sale_amt ");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" SELECT count(1) count FROM (select count(1) count from promotion_program_detail WHERE promotion_program_id = ? group by sale_amt) ");

		String[] fieldNames = { "productCodeStr", "productNameStr", "amount",
				"freeProductCodeStr", "freeProductNameStr", "pgDetailIdStr" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV12ViewSaleProductDetail(
			KPaging<ZV07View> paging, Long promotionProgramId, BigDecimal amount)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.product_id) productName ");
		sql.append("       ,listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr ");
		sql.append("       ,required, d.product_id productId ");
		sql.append(" from promotion_program_detail d where promotion_program_id = ? and sale_amt = ? group by product_id, required ");
		params.add(promotionProgramId);
		params.add(amount);

		countSql.append(" select count(distinct product_id) count from promotion_program_detail d where promotion_program_id = ? and sale_amt = ?");

		String[] fieldNames = { "productCode", "productName", "required",
				"pgDetailIdStr", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV12ViewFreeProductDetail(
			KPaging<ZV07View> paging, Long promotionProgramId, BigDecimal amount)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select product_code from product where product_id = d.free_product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.free_product_id) productName ");
		sql.append("       ,listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr ");
		sql.append("       ,free_qty quantity, d.free_product_id productId ");
		sql.append(" from promotion_program_detail d where promotion_program_id = ? and sale_amt = ? group by free_product_id, free_qty ");
		params.add(promotionProgramId);
		params.add(amount);

		countSql.append(" select count(distinct free_product_id) count from promotion_program_detail d where promotion_program_id = ? and sale_amt = ?");

		String[] fieldNames = { "productCode", "productName", "quantity",
				"pgDetailIdStr", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV13ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT listagg(product_code || ");
		sql.append("   '(' || to_char(sale_qty) || ')', ', ') within GROUP (ORDER BY product_code) productCodeStr, ");
		sql.append("   listagg(to_char(product_name), ',') within group (order by product_code) productNameStr, ");
		sql.append("   listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr, ");
		sql.append("   disc_per discountPercent ");
		sql.append(" FROM promotion_program_detail d ");
		sql.append(" JOIN product p ");
		sql.append(" ON d.product_id            = p.product_id ");
		sql.append(" WHERE promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" GROUP BY ");
		sql.append("   disc_per ");

		countSql.append(" SELECT count(1) count FROM (select count(1) count from promotion_program_detail WHERE promotion_program_id = ? group by disc_per) ");

		String[] fieldNames = { "productCodeStr", "productNameStr",
				"discountPercent", "pgDetailIdStr" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.FLOAT,
				StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV13ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, Float discountPercent)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.product_id) productName ");
		sql.append("       ,listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr ");
		sql.append("       ,sale_qty quantity, d.product_id productId ");
		sql.append(" from promotion_program_detail d where promotion_program_id = ? and disc_per = ? group by product_id, sale_qty ");
		params.add(promotionProgramId);
		params.add(discountPercent);

		countSql.append(" select count(distinct product_id) count from promotion_program_detail d where promotion_program_id = ? and disc_per = ?");

		String[] fieldNames = { "productCode", "productName", "quantity",
				"pgDetailIdStr", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV14ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT listagg(product_code || ");
		sql.append("   '(' || to_char(sale_qty) || ')', ', ') within GROUP (ORDER BY product_code) productCodeStr, ");
		sql.append("   listagg(to_char(product_name), ',') within group (order by product_code) productNameStr, ");
		sql.append("   listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr, ");
		sql.append("   disc_amt discountAmount ");
		sql.append(" FROM promotion_program_detail d ");
		sql.append(" JOIN product p ");
		sql.append(" ON d.product_id            = p.product_id ");
		sql.append(" WHERE promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" GROUP BY ");
		sql.append("   disc_amt ");

		countSql.append(" SELECT count(1) count FROM (select count(1) count from promotion_program_detail WHERE promotion_program_id = ? group by disc_amt) ");

		String[] fieldNames = { "productCodeStr", "productNameStr",
				"discountAmount", "pgDetailIdStr" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV14ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal discountAmount)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.product_id) productName ");
		sql.append("       ,listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr ");
		sql.append("       ,sale_qty quantity, d.product_id productId ");
		sql.append(" from promotion_program_detail d where promotion_program_id = ? and disc_amt = ? group by product_id, sale_qty ");
		params.add(promotionProgramId);
		params.add(discountAmount);

		countSql.append(" select count(distinct product_id) count from promotion_program_detail d where promotion_program_id = ? and disc_amt = ?");

		String[] fieldNames = { "productCode", "productName", "quantity",
				"pgDetailIdStr", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV15ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select freeProductCodeStr,freeProductNameStr ");
		sql.append("       ,listagg(productCode || case when required = 1 then '*' end || '(' || sale_qty || ')', ', ') within group (order by productCode) productCodeStr ");
		sql.append("   	   ,listagg(to_char(productName), ',') within group (order by productCode) productNameStr ");
		sql.append("       ,listagg(programDetailIdStr, ',') within group (order by programDetailIdStr) pgDetailIdStr ");
		sql.append(" from  ");
		sql.append(" ( ");
		sql.append("   select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.product_id) productName  ");
		sql.append("       ,required, sale_qty  ");
		sql.append("       ,listagg(f.product_code || '(' || d.free_qty || ')', ', ') within group (order by f.product_code) freeProductCodeStr ");
		sql.append("       ,listagg(to_char(f.product_name) , ', ') within group (order by f.product_code) freeProductNameStr ");
		sql.append("       ,listagg(d.promotion_program_detail_id, ',') within group (order by d.promotion_program_detail_id) programDetailIdStr ");
		sql.append("   from promotion_program_detail d join product f on d.free_product_id = f.product_id ");
		sql.append("   where promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append("   group by d.product_id, d.required, sale_qty ");
		sql.append(" ) ");
		sql.append(" group by freeProductCodeStr,freeProductNameStr ");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" SELECT count(1) count FROM ( ");
		countSql.append(sql);
		countSql.append(")");

		String[] fieldNames = { "productCodeStr", "productNameStr",
				"freeProductCodeStr", "freeProductNameStr", "pgDetailIdStr" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV15ViewSaleProductDetail(
			KPaging<ZV07View> paging, Long promotionProgramId,
			String freeProductCodeStr) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select productCode, productName, productId, quantity, programDetailIdStr pgDetailIdStr ");
		sql.append(" from ");
		sql.append(" ( ");
		sql.append("     select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("         ,(select product_name from product where product_id = d.product_id) productName ");
		sql.append("         ,d.product_id productId ");
		sql.append("         ,sale_qty quantity ");
		sql.append("         ,listagg(f.product_code || '(' || d.free_qty || ')', ', ') within group (order by f.product_code) freeProductCodeStr ");
		sql.append("         ,listagg(d.promotion_program_detail_id, ',') within group (order by d.promotion_program_detail_id) programDetailIdStr ");
		sql.append("     from promotion_program_detail d join product f on d.free_product_id = f.product_id ");
		sql.append("     where promotion_program_id = ? ");
		sql.append("     group by d.product_id, sale_qty ");
		sql.append(" ) ");
		sql.append(" where freeProductCodeStr = ? ");
		params.add(promotionProgramId);
		params.add(freeProductCodeStr);

		countSql.append(" select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");

		String[] fieldNames = { "productCode", "productName", "pgDetailIdStr",
				"productId", "quantity" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV15ViewFreeProductDetail(
			KPaging<ZV07View> paging, Long promotionProgramId,
			String productCodeStr) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select productCode, productName, productId, quantity, programDetailIdStr pgDetailIdStr ");
		sql.append(" from ");
		sql.append(" ( ");
		sql.append("     select (select product_code from product where product_id = d.free_product_id) productCode ");
		sql.append("         ,(select product_name from product where product_id = d.free_product_id) productName ");
		sql.append("         ,d.free_product_id productId ");
		sql.append("         ,free_qty quantity ");
		sql.append("         ,listagg(p.product_code || '(' || d.sale_qty || ')', ', ') within group (order by p.product_code) productCodeStr ");
		sql.append("         ,listagg(d.promotion_program_detail_id, ',') within group (order by d.promotion_program_detail_id) programDetailIdStr ");
		sql.append("     from promotion_program_detail d join product p on d.product_id = p.product_id ");
		sql.append("     where promotion_program_id = ? ");
		sql.append("     group by d.free_product_id, free_qty ");
		sql.append(" ) ");
		sql.append(" where productCodeStr = ? ");
		params.add(promotionProgramId);
		params.add(productCodeStr);

		countSql.append(" select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");

		String[] fieldNames = { "productCode", "productName", "quantity",
				"pgDetailIdStr", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV16ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT listagg(product_code || ");
		sql.append("   '(' || to_char(sale_amt) || ')', ', ') within GROUP (ORDER BY product_code) productCodeStr, ");
		sql.append("   listagg(to_char(product_name), ',') within group (order by product_code) productNameStr, ");
		sql.append("   listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr, ");
		sql.append("   disc_per discountPercent ");
		sql.append(" FROM promotion_program_detail d ");
		sql.append(" JOIN product p ");
		sql.append(" ON d.product_id            = p.product_id ");
		sql.append(" WHERE promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" GROUP BY ");
		sql.append("   disc_per ");

		countSql.append(" SELECT count(1) count FROM (select count(1) count from promotion_program_detail WHERE promotion_program_id = ? group by disc_per) ");

		String[] fieldNames = { "productCodeStr", "productNameStr",
				"discountPercent", "pgDetailIdStr" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.FLOAT,
				StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV16ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, Float discountPercent)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.product_id) productName ");
		sql.append("       ,listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr ");
		sql.append("       ,sale_amt amount, d.product_id productId ");
		sql.append(" from promotion_program_detail d where promotion_program_id = ? and disc_per = ? group by product_id, sale_amt ");
		params.add(promotionProgramId);
		params.add(discountPercent);

		countSql.append(" select count(distinct product_id) count from promotion_program_detail d where promotion_program_id = ? and disc_per = ?");

		String[] fieldNames = { "productCode", "productName", "amount",
				"pgDetailIdStr", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV17ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT listagg(product_code || ");
		sql.append("   '(' || to_char(sale_amt) || ')', ', ') within GROUP (ORDER BY product_code) productCodeStr, ");
		sql.append("   listagg(to_char(product_name), ',') within group (order by product_code) productNameStr, ");
		sql.append("   listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr, ");
		sql.append("   disc_amt discountAmount ");
		sql.append(" FROM promotion_program_detail d ");
		sql.append(" JOIN product p ");
		sql.append(" ON d.product_id            = p.product_id ");
		sql.append(" WHERE promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" GROUP BY ");
		sql.append("   disc_amt ");

		countSql.append(" SELECT count(1) count FROM (select count(1) count from promotion_program_detail WHERE promotion_program_id = ? group by disc_amt) ");

		String[] fieldNames = { "productCodeStr", "productNameStr",
				"discountAmount", "pgDetailIdStr" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV17ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal discountAmount)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.product_id) productName ");
		sql.append("       ,listagg(promotion_program_detail_id, ',') within group (order by promotion_program_detail_id) pgDetailIdStr ");
		sql.append("       ,sale_amt amount, d.product_id productId ");
		sql.append(" from promotion_program_detail d where promotion_program_id = ? and disc_amt = ? group by product_id, sale_amt ");
		params.add(promotionProgramId);
		params.add(discountAmount);

		countSql.append(" select count(distinct product_id) count from promotion_program_detail d where promotion_program_id = ? and disc_amt = ?");

		String[] fieldNames = { "productCode", "productName", "amount",
				"pgDetailIdStr", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV18ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select freeProductCodeStr ,freeProductNameStr");
		sql.append("       ,listagg(productCode || case when required = 1 then '*' end || '(' || sale_amt || ')', ', ') within group (order by productCode) productCodeStr ");
		sql.append("       ,listagg(to_char(productName), ', ') within group (order by productCode) productNameStr ");
		sql.append("       ,listagg(programDetailIdStr, ',') within group (order by programDetailIdStr) pgDetailIdStr ");
		sql.append(" from  ");
		sql.append(" ( ");
		sql.append("   select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("       ,(select product_name from product where product_id = d.product_id) productName  ");
		sql.append("       ,required,sale_amt  ");
		sql.append("       ,listagg(f.product_code || '(' || d.free_qty || ')', ', ') within group (order by f.product_code) freeProductCodeStr ");
		sql.append("       ,listagg(to_char(f.product_name), ', ') within group (order by f.product_code) freeProductNameStr ");
		sql.append("       ,listagg(d.promotion_program_detail_id, ',') within group (order by d.promotion_program_detail_id) programDetailIdStr ");
		sql.append("   from promotion_program_detail d join product f on d.free_product_id = f.product_id ");
		sql.append("   where promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append("   group by d.product_id, required, sale_amt ");
		sql.append(" ) ");
		sql.append(" group by freeProductCodeStr,freeProductNameStr ");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" SELECT count(1) count FROM ( ");
		countSql.append(sql);
		countSql.append(")");

		String[] fieldNames = { "productCodeStr", "productNameStr",
				"freeProductCodeStr", "freeProductNameStr", "pgDetailIdStr" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV18ViewSaleProductDetail(
			KPaging<ZV07View> paging, Long promotionProgramId,
			String freeProductCodeStr) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select productCode, productName, productId, amount, programDetailIdStr pgDetailIdStr ");
		sql.append(" from ");
		sql.append(" ( ");
		sql.append("     select (select product_code from product where product_id = d.product_id) productCode ");
		sql.append("         ,(select product_name from product where product_id = d.product_id) productName ");
		sql.append("         ,d.product_id productId ");
		sql.append("         ,sale_amt amount ");
		sql.append("         ,listagg(f.product_code || '(' || d.free_qty || ')', ', ') within group (order by f.product_code) freeProductCodeStr ");
		sql.append("         ,listagg(d.promotion_program_detail_id, ',') within group (order by d.promotion_program_detail_id) programDetailIdStr ");
		sql.append("     from promotion_program_detail d join product f on d.free_product_id = f.product_id ");
		sql.append("     where promotion_program_id = ? ");
		sql.append("     group by d.product_id, sale_amt ");
		sql.append(" ) ");
		sql.append(" where freeProductCodeStr = ? ");
		params.add(promotionProgramId);
		params.add(freeProductCodeStr);

		countSql.append(" select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");

		String[] fieldNames = { "productCode", "productName", "pgDetailIdStr",
				"productId", "amount" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV18ViewFreeProductDetail(
			KPaging<ZV07View> paging, Long promotionProgramId,
			String productCodeStr) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select productCode, productName, productId, quantity, programDetailIdStr pgDetailIdStr ");
		sql.append(" from ");
		sql.append(" ( ");
		sql.append("     select (select product_code from product where product_id = d.free_product_id) productCode ");
		sql.append("         ,(select product_name from product where product_id = d.free_product_id) productName ");
		sql.append("         ,d.free_product_id productId ");
		sql.append("         ,free_qty quantity ");
		sql.append("         ,listagg(p.product_code || '(' || d.sale_qty || ')', ', ') within group (order by p.product_code) productCodeStr ");
		sql.append("         ,listagg(d.promotion_program_detail_id, ',') within group (order by d.promotion_program_detail_id) programDetailIdStr ");
		sql.append("     from promotion_program_detail d join product p on d.product_id = p.product_id ");
		sql.append("     where promotion_program_id = ? ");
		sql.append("     group by d.free_product_id, free_qty ");
		sql.append(" ) ");
		sql.append(" where productCodeStr = ? ");
		params.add(promotionProgramId);
		params.add(productCodeStr);

		countSql.append(" select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");

		String[] fieldNames = { "productCode", "productName", "quantity",
				"pgDetailIdStr", "productId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV21ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT  ");
		sql.append("        d.sale_amt amount,  ");
		sql.append("        listagg(f.product_code || '(' || to_char(free_qty) || ')', ',') within GROUP (ORDER BY f.product_code) ");
		sql.append("               freeProductCodeStr, ");
		sql.append("        listagg(to_char(f.product_name), ',') within GROUP (ORDER BY f.product_code) ");
		sql.append("               freeProductNameStr, ");
		sql.append("        listagg(d.promotion_program_detail_id, ',') within GROUP (ORDER BY d.promotion_program_detail_id) ");
		sql.append("               pgDetailIdStr ");
		sql.append(" FROM promotion_program_detail d join product f on d.free_product_id = f.product_id ");
		sql.append(" WHERE d.promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" GROUP BY d.sale_amt ");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from  ");
		countSql.append(" (SELECT 1 FROM promotion_program_detail WHERE promotion_program_id = ? group by sale_amt) ");

		String[] fieldNames = { "amount", "freeProductCodeStr",
				"freeProductNameStr", "pgDetailIdStr" };
		Type[] fieldTypes = { StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV21ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal amount)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select p.product_id productId, p.product_code productCode, p.product_name productName, d.free_qty quantity, d.promotion_program_detail_id pgDetailId ");
		sql.append(" from promotion_program_detail d join product p on p.product_id = d.free_product_id ");
		sql.append("	where promotion_program_id = ? and sale_amt = ?");
		params.add(promotionProgramId);
		params.add(amount);

		countSql.append(" select count(1) count from promotion_program_detail where promotion_program_id = ? and sale_amt = ?");

		String[] fieldNames = { "productId", "productCode", "quantity",
				"productName", "pgDetailId" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging == null)
			return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ZV07View.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, paging);
	}

	@Override
	public List<ZV07View> getZV01ViewHeader(Long promotionProgramId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT d.product_id productId, ");
		sql.append("        (select product_code from product where product_id = d.product_id) productCode, ");
		sql.append("        (select product_name from product where product_id = d.product_id) productName, ");
		sql.append("        d.sale_qty quantity,  ");
		sql.append("        d.disc_per discountPercent  ");
		sql.append(" FROM promotion_program_detail d join product f on d.product_id = f.product_id ");
		sql.append(" WHERE d.promotion_program_id = ? ");
		params.add(promotionProgramId);

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from  ");
		countSql.append(" (SELECT 1 FROM promotion_program_detail WHERE promotion_program_id = ? order by product_id, sale_qty) ");

		String[] fieldNames = { "productId", "productCode", "productName",
				"quantity", "discountPercent" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT };
		return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ZV07View> getZV02ViewHeader(Long promotionProgramId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT d.product_id productId, ");
		sql.append("        (select product_code from product where product_id = d.product_id) productCode, ");
		sql.append("        (select product_name from product where product_id = d.product_id) productName, ");
		sql.append("        d.sale_qty quantity,  ");
		sql.append("        d.disc_amt discountAmount  ");
		sql.append(" FROM promotion_program_detail d join product f on d.product_id = f.product_id ");
		sql.append(" WHERE d.promotion_program_id = ? ");
		params.add(promotionProgramId);

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from  ");
		countSql.append(" (SELECT 1 FROM promotion_program_detail WHERE promotion_program_id = ? order by product_id, sale_qty) ");

		String[] fieldNames = { "productId", "productCode", "productName",
				"quantity", "discountAmount" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL };
		return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ZV07View> getZV04ViewHeader(Long promotionProgramId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT d.product_id productId, ");
		sql.append("        (select product_code from product where product_id = d.product_id) productCode, ");
		sql.append("        (select product_name from product where product_id = d.product_id) productName, ");
		sql.append("        d.sale_amt amount,  ");
		sql.append("        d.disc_per discountPercent  ");
		sql.append(" FROM promotion_program_detail d join product f on d.product_id = f.product_id ");
		sql.append(" WHERE d.promotion_program_id = ? ");
		params.add(promotionProgramId);

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from  ");
		countSql.append(" (SELECT 1 FROM promotion_program_detail WHERE promotion_program_id = ? order by product_id, sale_amt) ");

		String[] fieldNames = { "productId", "productCode", "productName",
				"amount", "discountPercent" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.FLOAT };
		return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ZV07View> getZV05ViewHeader(Long promotionProgramId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT d.product_id productId, ");
		sql.append("        (select product_code from product where product_id = d.product_id) productCode, ");
		sql.append("        (select product_name from product where product_id = d.product_id) productName, ");
		sql.append("        d.sale_amt amount,  ");
		sql.append("        d.disc_amt discountAmount  ");
		sql.append(" FROM promotion_program_detail d join product f on d.product_id = f.product_id ");
		sql.append(" WHERE d.promotion_program_id = ? ");
		params.add(promotionProgramId);

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from  ");
		countSql.append(" (SELECT 1 FROM promotion_program_detail WHERE promotion_program_id = ? order by product_id, sale_amt) ");

		String[] fieldNames = { "productId", "productCode", "productName",
				"amount", "discountAmount" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL };
		return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ZV07View> getZV19ViewHeader(Long promotionProgramId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT d.sale_amt amount,  ");
		sql.append("        d.disc_per discountPercent  ");
		sql.append(" FROM promotion_program_detail d");
		sql.append(" WHERE d.promotion_program_id = ? ");
		params.add(promotionProgramId);

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from  ");
		countSql.append(" (SELECT 1 FROM promotion_program_detail WHERE promotion_program_id = ? order by sale_amt) ");

		String[] fieldNames = { "amount", "discountPercent" };
		Type[] fieldTypes = { StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.FLOAT };
		return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ZV07View> getZV20ViewHeader(Long promotionProgramId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT d.sale_amt amount,  ");
		sql.append("        d.disc_amt discountAmount  ");
		sql.append(" FROM promotion_program_detail d");
		sql.append(" WHERE d.promotion_program_id = ? ");
		params.add(promotionProgramId);

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from  ");
		countSql.append(" (SELECT 1 FROM promotion_program_detail WHERE promotion_program_id = ? order by sale_amt) ");

		String[] fieldNames = { "amount", "discountAmount" };
		Type[] fieldTypes = { StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL };
		return repo.getListByQueryAndScalar(ZV07View.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}

	@Override
	public List<PromotionProgram> getListPromotionForOrder(Long shopId,
			Long customerId, Date orderDate, Long saleOrderId, Long staffId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		
		sql.append("select pp.* from promotion_program pp ");
		sql.append(" where pp.promotion_program_id in ( SELECT PP.promotion_program_id promotionProgramId ");
		sql.append(" FROM   promotion_program pp,  ");
		sql.append("       promotion_shop_map psm ");
//		sql.append("       LEFT JOIN promotion_customer_map pcm ON pcm.promotion_shop_map_id = psm.promotion_shop_map_id ");
		sql.append(" WHERE  1 = 1 ");
		sql.append("       AND psm.shop_id IN ( select shop_id from shop where status = 1 START WITH shop_id = ? CONNECT BY PRIOR parent_shop_id = shop_id ");
		params.add(shopId);
		
		sql.append(") ");
		sql.append("       AND PP.STATUS = 1 AND psm.status = 1 and pp.type in ('ZV19', 'ZV20', 'ZV21') ");
		sql.append("       AND psm.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 	   AND pp.from_date < TRUNC(?) + 1 ");
		sql.append(" 	   AND (pp.to_date    >= TRUNC(?) or pp.to_date is null) ");
		params.add(orderDate);
		params.add(orderDate);
		sql.append("       AND psm.from_date < TRUNC(?) + 1  ");
		sql.append("       AND (psm.to_date    >= TRUNC(?) OR psm.to_date is null)  AND ( (psm.quantity_max IS NULL OR NVL(psm.quantity_received_total, 0) < psm.quantity_max) ) ");
		params.add(orderDate);
		params.add(orderDate);
		
		//TODO vuongmq 13/01/2015; comment khong kiem tra CTKM cho NVBH, KH
		// Kiem tra xem co ap dung toi muc KH ko?
		/*sql.append(" AND ( ");
		sql.append(" NOT EXISTS ( SELECT 1 ");
		sql.append(" 			 FROM promotion_customer_map pcmm ");
		sql.append(" 			 WHERE pcmm.promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and ((pcmm.quantity_max is null or NVL(pcmm.quantity_received_total, 0) < pcmm.quantity_max))) ");
		sql.append(" OR EXISTS ( SELECT 1 ");
		sql.append(" 			 FROM promotion_customer_map pcmm ");
		sql.append(" 			 WHERE promotion_shop_map_id = psm.promotion_shop_map_id AND status = 1 AND customer_id = ? and ((pcmm.quantity_max is null or NVL(pcmm.quantity_received_total, 0) < pcmm.quantity_max)) ) ");
		sql.append(" 	) ");
		params.add(customerId);
		
		// Kiem tra xem co ap dung toi muc NV ko?
		sql.append(" AND ( ");
		sql.append(" NOT EXISTS ( SELECT 1 ");
		sql.append(" 			 FROM promotion_staff_map pstm ");
		sql.append(" 			 WHERE pstm.promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and ((pstm.quantity_max is null or NVL(pstm.quantity_received_total, 0) < pstm.quantity_max))) ");
		sql.append(" OR EXISTS ( SELECT 1 ");
		sql.append(" 			 FROM promotion_staff_map pstm ");
		sql.append(" 			 WHERE promotion_shop_map_id = psm.promotion_shop_map_id AND status = 1 AND staff_id = ? and ((pstm.quantity_max is null or NVL(pstm.quantity_received_total, 0) < pstm.quantity_max)) ) ");
		sql.append(" 	) ");
		params.add(staffId);*/
		
		//Nhom sp ban & khong co sp
		sql.append(" AND EXISTS ( ");
		sql.append(" 				SELECT 1 FROM product_group pg WHERE pg.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 				AND pg.status = 1 AND pg.group_type = 1 ");
		sql.append(" 				AND exists ( ");
		sql.append(" 								SELECT 1 FROM group_level gl WHERE gl.product_group_id = pg.product_group_id ");
		sql.append(" 								AND gl.has_product = 0 AND gl.status = 1 ");
		sql.append(" 							) ");
		sql.append(" 			) " );
		
		
		// Kiem tra thuoc tinh KH
		/*sql.append(" AND  ");
		sql.append("   ( ");
		sql.append("     NOT EXISTS (SELECT 1 FROM PROMOTION_CUST_ATTR pa WHERE pa.promotion_program_id = pp.promotion_program_id and status = 1) ");
		sql.append("     OR  ");
		sql.append(" ( ");
		sql.append(" EXISTS (SELECT * FROM PROMOTION_CUST_ATTR pa WHERE pa.promotion_program_id = pp.promotion_program_id AND status = 1)");
		sql.append(" AND ( ");
		sql.append(" NOT EXISTS ");
		sql.append(" ( ");
		sql.append(" (SELECT * ");
		sql.append(" FROM PROMOTION_CUST_ATTR pa ");
		sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" AND status = 1) ");
		sql.append(" minus ");
		sql.append(" (SELECT * ");
		sql.append(" FROM PROMOTION_CUST_ATTR pa ");
		sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" AND status = 1 ");
		sql.append(" AND ( (object_type = 1 ");
		sql.append(" AND ( ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type = 1 ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		sql.append(" AND upper(VALUE) = upper(PA.FROM_VALUE) ");
		sql.append(" ) ) ");
		sql.append(" OR ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type = 2 ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		sql.append(" AND to_number(VALUE) >= to_number(PA.FROM_VALUE) ");
		sql.append(" AND to_number(value) <= to_number(pa.to_value) ");
		sql.append(" ) ) ");
		sql.append(" OR ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type = 3 ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		sql.append(" AND (to_date(VALUE, 'yyyy-MM-dd') >= to_date(PA.FROM_VALUE, 'yyyy-MM-dd') ");
		sql.append(" OR pa.from_value IS NULL) ");
		sql.append(" AND (to_date(value, 'yyyy-MM-dd') <= to_date(pa.to_value, 'yyyy-MM-dd') ");
		sql.append(" OR pa.to_value IS NULL) ");
		sql.append(" ) ) ");
		sql.append(" OR ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type IN (4,5) ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value av ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		// -----------------Kiem tra thuoc tinh mutil select
		sql.append(" and exists (select 1 from attribute_value_detail avd join promotion_cust_attr_detail pcd on avd.attribute_detail_id = pcd.object_id ");
		sql.append("             where avd.attribute_value_id = av.attribute_value_id and pcd.promotion_cust_attr_id = pa.promotion_cust_attr_id and pcd.status = 1 and avd.status = 1)");
		// -----------------Kiem tra thuoc tinh mutil select
		sql.append(" ) ) ) ) ");
		sql.append(" OR (object_type = 2 ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" WHERE status = 1 ");
		sql.append(" AND PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND OBJECT_ID = ");
		sql.append(" (SELECT CHANNEL_TYPE_ID FROM CUSTOMER WHERE CUSTOMER_ID = ? AND status = 1 ");
		params.add(customerId);
		sql.append(" ) ");
		sql.append(" ) ) ");
		sql.append(" OR (object_type = 3 ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" WHERE status = 1 ");
		sql.append(" AND PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND OBJECT_ID IN ");
		sql.append(" (SELECT SALE_LEVEL_CAT_ID FROM CUSTOMER_CAT_LEVEL WHERE CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" ) ) ) ");
		sql.append(" ) ");
		sql.append(" ) ");
		sql.append(" ) ");
		sql.append(" ) ");
		// end Kiem tra thuoc tinh KH
		sql.append("   ) ");*/
		
		sql.append(" and (not exists ");
		sql.append(" 	(select 1 ");
		sql.append(" 	 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 	 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 	  and status = 1) ");
		sql.append("    or (exists ");
		sql.append(" 		(select 1 ");
		sql.append(" 		 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 		 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 		  and status = 1) ");
		sql.append(" 	   and (not exists ( ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						  and status = 1) ");
		sql.append(" 						 minus ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						   and status = 1 ");
		sql.append(" 						   and ( ");
		sql.append(" 						   		(object_type = 1 ");
		sql.append(" 								 and ( ");
		sql.append(" 								 	(exists ");
		sql.append(" 										(select 1 ");
		sql.append(" 										 from customer_attribute ");
		sql.append(" 										 where type = 1 ");
		sql.append(" 										  and customer_attribute_id = pa.object_id ");
		sql.append(" 										  and status = 1) ");
		sql.append(" 										and exists ( ");
		sql.append(" 											select 1 ");
		sql.append(" 											from customer_attribute_detail ");
		sql.append(" 											where customer_id = ? ");
		params.add(customerId);
		sql.append(" 											and status = 1 ");
		sql.append(" 											and customer_attribute_id = pa.object_id ");
		sql.append(" 											and upper(value) = upper(pa.from_value) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 2 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and to_number(value) >= to_number(pa.from_value) ");
		sql.append(" 												and to_number(value) <= to_number(pa.to_value) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 3 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') >= to_date(pa.from_value, 'yyyy-mm-dd') or pa.from_value is null) ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') <= to_date(pa.to_value, 'yyyy-mm-dd') or pa.to_value is null) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type in (4, 5) ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail cad ");
		sql.append(" 												join promotion_cust_attr_detail pcad  ");
		sql.append(" 													on pcad.object_type = 1  ");
		sql.append(" 													and pcad.status = 1 ");
		sql.append(" 													and cad.customer_attribute_enum_id = pcad.object_id ");
		sql.append(" 												where cad.customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and cad.status = 1 ");
		sql.append(" 												and pcad.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
		sql.append(" 												and cad.customer_attribute_id = pa.object_id ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 								) ");
		sql.append(" 								or (object_type = 2 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID = ");
		sql.append(" 										(select CHANNEL_TYPE_ID ");
		sql.append(" 										 from CUSTOMER ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 								or (object_type = 3 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID in ");
		sql.append(" 										(select SALE_LEVEL_CAT_ID ");
		sql.append(" 										 from CUSTOMER_CAT_LEVEL ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 							) ");
		sql.append(" 						) ");
		sql.append(" 				) ");
		sql.append(" 			) ");
		sql.append(" 	) ");
		sql.append(" ) ");
		
		
		sql.append("   ) ");
		return repo.getListBySQL(PromotionProgram.class, sql.toString().replaceAll("  ", " "), params);
		
	}

	@Override
	public List<PromotionProgram> getListPromotionProgramByCustomerType(
			Long customerTypeId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		params.add(customerTypeId);
		sql.append(" select * from promotion_program pp where exists (SELECT 1 FROM PROMOTION_CUST_ATTR pa WHERE pa.promotion_program_id = pp.promotion_program_id");
		sql.append("  and object_type = 2 AND EXISTS (SELECT 1 FROM PROMOTION_CUST_ATTR_DETAIL WHERE PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append("                   AND OBJECT_ID = ?) ");
		sql.append(")");
		// sql.append(" and from_date < trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null)");
		sql.append(" and (status = 1 and (to_date is null or to_date >= trunc(sysdate)))");

		return repo
				.getListBySQL(PromotionProgram.class, sql.toString(), params);
	}

	public static void main(String[] args) {
		System.out.println(Long.MAX_VALUE);
	}

	@Override
	public PromotionProgram getPromotionProgramByCodeByType(String code,
			ApParamType type) throws DataAccessException {
		code = code.toUpperCase();
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from promotion_program where PROMOTION_PROGRAM_CODE=? and status!=?");
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		if (ApParamType.PROMOTION.equals(type)) {
			sql.append(" and type like 'ZV%'");
		} else {
			sql.append(" and type not like 'ZV%'");
		}
		return repo.getEntityBySQL(PromotionProgram.class, sql.toString(),
				params);
	}
	
	@Override
	public void clearPromotionProgram(PromotionProgram promotionProgram,
			LogInfoVO logInfo) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		//Lay danh sach product group theo promotion
		sql.append(" select product_group_id id from product_group where promotion_program_id=? ");
		params.add(promotionProgram.getId());
		String[] fieldNames = new String[] { "id" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG };
		List<BasicVO> lstProductGroup=repo.getListByQueryAndScalar(BasicVO.class, fieldNames, fieldTypes, sql.toString(), params);
		
		if(lstProductGroup!=null && lstProductGroup.size()>0){
			sql = new StringBuilder();
			params = new ArrayList<Object>();
			//Lay danh sach group level theo product group
			sql.append(" select group_level_id id from group_level where product_group_id in (-1");
			for(BasicVO vo:lstProductGroup){
				sql.append(",?");
				params.add(vo.getId());
			}
			sql.append(" ) ");
			List<BasicVO> lstGroupLevel=repo.getListByQueryAndScalar(BasicVO.class, fieldNames, fieldTypes, sql.toString(), params);
			
			if(lstGroupLevel!=null && lstGroupLevel.size()>0){
				//Xoa group level detail theo group level
				sql = new StringBuilder();
				params = new ArrayList<Object>();
				sql.append(" delete group_level_detail where group_level_id in (-1");
				for(BasicVO vo:lstGroupLevel){
					sql.append(",?");
					params.add(vo.getId());
				}
				sql.append(" ) ");
				repo.executeSQLQuery(sql.toString(), params);
			}
			
			//Xoa group level theo product group
			sql = new StringBuilder();
			params = new ArrayList<Object>();
			sql.append("delete group_level where product_group_id in (-1");
			for(BasicVO vo:lstProductGroup){
				sql.append(",?");
				params.add(vo.getId());
			}
			sql.append(" ) ");
			repo.executeSQLQuery(sql.toString(), params);
			
			//Xoa group mapping theo product group
			sql = new StringBuilder();
			params = new ArrayList<Object>();
			sql.append("delete group_mapping where (SALE_GROUP_ID in (-1");
			for(BasicVO vo:lstProductGroup){
				sql.append(",?");
				params.add(vo.getId());
			}
			sql.append(" ) or PROMO_GROUP_ID in (-1");
			for(BasicVO vo:lstProductGroup){
				sql.append(",?");
				params.add(vo.getId());
			}
			sql.append(" ))");
			
			repo.executeSQLQuery(sql.toString(), params);
			
			//Xoa productGroup
			sql = new StringBuilder();
			params = new ArrayList<Object>();
			sql.append(" delete product_group where promotion_program_id=? ");
			params.add(promotionProgram.getId());
			repo.executeSQLQuery(sql.toString(), params);
		}
	}
	
	@Override
	public GroupMapping getGroupMappingByProductGroupAndLevel(Long productGroupId, Long groupLevelId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from group_mapping where sale_group_id = ? and sale_group_level_id = ? and status = ? ");
		List<Object> params = new ArrayList<Object>();
		params.add(productGroupId);
		params.add(groupLevelId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(GroupMapping.class, sql.toString(),params);
	}
	
	@Override
	public GroupLevel getGroupLevel(Long productGroupId, Long groupLevelId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from group_level where product_group_id = ? and group_level_id = ? and status = ? ");
		List<Object> params = new ArrayList<Object>();
		params.add(productGroupId);
		params.add(groupLevelId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(GroupLevel.class, sql.toString(),params);
	}
	
	@Override
	public GroupLevel getGroupLevelById(Long groupLevelId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from group_level where group_level_id = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(groupLevelId);
		return repo.getEntityBySQL(GroupLevel.class, sql.toString(),params);
	}
	
	@Override
	public GroupLevelDetail getGroupLevelDetailByLevelIdProductId(Long groupLevelId, Long productId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from group_level_detail where group_level_id = ? and product_id = ?");
		params.add(groupLevelId);
		params.add(productId);
		return repo.getEntityBySQL(GroupLevelDetail.class, sql.toString(), params);
	}
	
	/**
	 *Tinh KM don hang
	 * @author: nhanlt6
	 * @return: void
	 * @param orderAmount
	 * @param sortListProductSale
	 * @param sortListOutPut
	 * @param listProductPromotionsale
	 * @param shopId
	 * @param keyList
	 * @param customerId
	 * @param customerTypeId
	 * @param orderDate
	 * @param saleOrderId
	 */
	@Override
	public PromotionProductsVO calPromotionForOrder(Map<Long, List<SaleOrderLot>> mapSaleOrderLot,BigDecimal orderAmount,
			SortedMap<Long, SaleOrderDetailVO> sortListProductSale,
			SortedMap<Long, List<SaleOrderDetailVO>> sortListOutPut,
			List<SaleOrderDetailVO> listProductPromotionsale,
			Long shopId,Long keyList,Long customerId,Long customerTypeId,Long staffId,Date orderDate, Long saleOrderId,SaleOrderVO orderVO) {
		// Calculate total (amount - discount) of order
		// long disCount = 0;
		PromotionProductsVO vo = new PromotionProductsVO();
		try {
			// B2: tinh KM cho don hang
			List<PromotionProgram> listPromotionOrder = this.getListPromotionForOrder(shopId, customerId, orderDate,saleOrderId,staffId);
			ArrayList<SaleOrderDetailVO> listPromotionForOrderChange = new ArrayList<SaleOrderDetailVO>();
			ArrayList<SaleOrderDetailVO> listOrderProductPromotion = new ArrayList<SaleOrderDetailVO>();
			boolean hasOrderPromotion = false;
			// B2: tinh KM cho don hang
			for (int i = 0, size = listPromotionOrder.size(); i < size; i++) {
				// lay ds CTKM tuong ung 
				PromotionProgram promotionProgram = listPromotionOrder.get(i);
				if (promotionProgram != null) {
					CalcPromotionFilter calcPromotionFilter = new CalcPromotionFilter();
					calcPromotionFilter.setAmtOrder(orderAmount);
					calcPromotionFilter.setLstPromotionProEntity(Arrays.asList(promotionProgram));
					calcPromotionFilter.setSortListProductSales(sortListProductSale);
					calcPromotionFilter.setListProductPromotionsale(listOrderProductPromotion);
					calcPromotionFilter.setKeyList(keyList);
					calcPromotionFilter.setSortListOutPut(sortListOutPut);
					calcPromotionFilter.setShopId(shopId);
					calcPromotionFilter.setPromotionType(SaleOrderDetailVO.ownerTypePromoOrder);
					calcPromotionFilter.setOrderVO(orderVO);
					calcPromotionFilter.setCustomerId(customerId);
					calcPromotionFilter.setStaffId(staffId);
					calcPromotionFilter.setOrderDate(orderDate);
					calcPromotionFilter.setMapCTKMProduct(null);
					this.calcPromotion(calcPromotionFilter);
				}
				//Huong duoc KM cua 1 program -> co the huong duoc nhieu group
				//Them vao ds doi hang
				if(listOrderProductPromotion.size() > 0) {
					//Kiem tra neu list KM gom nhieu CTKM khac nhau thi moi add vao list SPKM doi
//					boolean isCheck = false;
//					for (SaleOrderDetailVO sod : listOrderProductPromotion) {
//						for (int j = 0;j<listOrderProductPromotion.size();j++) {
//							if (sod.getType() != listOrderProductPromotion.get(j).getType()) {
//								isCheck = true;
//								break;
//							}
//						}
//					}
//					if (isCheck) {
					if(listOrderProductPromotion.size() > 0) {
						listPromotionForOrderChange.addAll(listOrderProductPromotion);
					}
					
					if(!hasOrderPromotion) {
						hasOrderPromotion = true;
						listProductPromotionsale.addAll(listOrderProductPromotion);
						//Lay ds so suat cua KM don hang dau tien vao ds so suat
						vo.productQuantityReceivedList.addAll(vo.orderQuantityReceivedList);
					}
					listOrderProductPromotion.clear();
				}
			}			
			
			if (listPromotionForOrderChange.size() > 0) {
				// neu co doi CTKM cho don hang
				vo.setListPromotionForOrderChange(listPromotionForOrderChange);
			}
			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return vo;
	}
	
	private PromotionProductsVO calPromotionForOrder(Map<Long, List<SaleOrderLot>> mapSaleOrderLot,BigDecimal orderAmount,
			SortedMap<Long, SaleOrderDetailVO> sortListProductSale,
			SortedMap<Long, List<SaleOrderDetailVO>> sortListOutPut,
			List<SaleOrderDetailVO> listProductPromotionsale,
			Long shopId,Long keyList,Long customerId,Long customerTypeId,Long staffId,Date orderDate, Long saleOrderId,SaleOrderVO orderVO,
			String lstProductIds, String lstQuantities, String lstAmounts) {
		// Calculate total (amount - discount) of order
		// long disCount = 0;
		PromotionProductsVO vo = new PromotionProductsVO();
		vo.setListOpenPromotionForOrder(new ArrayList<SaleOrderDetailVO>());
		try {
			// B2: tinh KM cho don hang
			List<PromotionProgram> listPromotionOrder = this.getListPromotionForOrder(shopId, customerId, orderDate,saleOrderId, staffId,
					lstProductIds, lstQuantities, lstAmounts);
			ArrayList<SaleOrderDetailVO> listPromotionForOrderChange = new ArrayList<SaleOrderDetailVO>();
			ArrayList<SaleOrderDetailVO> listOrderProductPromotion = new ArrayList<SaleOrderDetailVO>();
			boolean hasOrderPromotion = false;
			// B2: tinh KM cho don hang
			for (int i = 0, size = listPromotionOrder.size(); i < size; i++) {
				// lay ds CTKM tuong ung 
				PromotionProgram promotionProgram = listPromotionOrder.get(i);
				if (promotionProgram != null) {
					boolean b = this.checkPromotionProgramHasPortion(promotionProgram.getId(), shopId, customerId, staffId, orderDate);
					if (b) {
						promotionProgram.setHasPortion(1);
					} else {
						promotionProgram.setHasPortion(0);
					}
					CalcPromotionFilter calcPromotionFilter = new CalcPromotionFilter();
					calcPromotionFilter.setAmtOrder(orderAmount);
					calcPromotionFilter.setLstPromotionProEntity(Arrays.asList(promotionProgram));
					calcPromotionFilter.setSortListProductSales(sortListProductSale);
					calcPromotionFilter.setListProductPromotionsale(listOrderProductPromotion);
					calcPromotionFilter.setKeyList(keyList);
					calcPromotionFilter.setSortListOutPut(sortListOutPut);
					calcPromotionFilter.setShopId(shopId);
					calcPromotionFilter.setPromotionType(SaleOrderDetailVO.ownerTypePromoOrder);
					calcPromotionFilter.setOrderVO(orderVO);
					calcPromotionFilter.setCustomerId(customerId);
					calcPromotionFilter.setStaffId(staffId);
					calcPromotionFilter.setOrderDate(orderDate);
					calcPromotionFilter.setMapCTKMProduct(null);
					this.calcPromotion(calcPromotionFilter);
					if (sortListOutPut.size() > 0) {
						keyList = sortListOutPut.lastKey();
					}
				}
				//Huong duoc KM cua 1 program -> co the huong duoc nhieu group
				//Them vao ds doi hang
				if(listOrderProductPromotion.size() > 0) {
					//Kiem tra neu list KM gom nhieu CTKM khac nhau thi moi add vao list SPKM doi
//					boolean isCheck = false;
//					for (SaleOrderDetailVO sod : listOrderProductPromotion) {
//						for (int j = 0;j<listOrderProductPromotion.size();j++) {
//							if (sod.getType() != listOrderProductPromotion.get(j).getType()) {
//								isCheck = true;
//								break;
//							}
//						}
//					}

					/*if (PromotionType.ZV24.getValue().equals(promotionProgram.getType())) {
						vo.getListOpenPromotionForOrder().addAll(listOrderProductPromotion);
					} else {*/
						listPromotionForOrderChange.addAll(listOrderProductPromotion);
					//}
					
					if(!hasOrderPromotion && !PromotionType.ZV24.getValue().equals(promotionProgram.getType())) {
						hasOrderPromotion = true;
						listProductPromotionsale.addAll(listOrderProductPromotion);
						//Lay ds so suat cua KM don hang dau tien vao ds so suat
						vo.productQuantityReceivedList.addAll(vo.orderQuantityReceivedList);
					}
					listOrderProductPromotion.clear();
				}
			}			
			
			if (listPromotionForOrderChange.size() > 0) {
				// neu co doi CTKM cho don hang
				vo.setListPromotionForOrderChange(listPromotionForOrderChange);
			}
			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		return vo;
	}
	
	
	/**
	 * Update thong tin cho 1 sp KM
	 * @author: nhanlt6
	 * @return: void
	 * @param promotionProduct
	 * @param productPromotionInfo
	 */
	private void updatePromotionProductInfo(
			SaleOrderDetailVO promotionProduct,
			StockTotalVO productPromotionInfo) {
		promotionProduct.setStock(productPromotionInfo.getStockQuantity());
		promotionProduct.setAvailableQuantity(productPromotionInfo.getAvailableQuantity());
		promotionProduct.getSaleOrderDetail().setTotalWeight(
				BigDecimal.valueOf(productPromotionInfo.getGrossWeight()
				* promotionProduct.getSaleOrderDetail().getQuantity()));
	}
	
	/**
	 * Lay ds san pham khuyen mai tu san pham ban
	 * @author: NhanLT6
	 * @param calculatePromotionFilter
	 * @return PromotionProductsVO
	 */	
	@Override
	public PromotionProductsVO calculatePromotionProducts2(CalculatePromotionFilter calculatePromotionFilter) {
		Map<Long, List<SaleOrderLot>> mapSaleOrderLot = calculatePromotionFilter.getMapSaleOrderLot();
		List<SaleOrderDetailVO> lstSalesOrderDetail = calculatePromotionFilter.getLstSalesOrderDetail();
		long shopId = calculatePromotionFilter.getShopId();
		Integer shopChannel = calculatePromotionFilter.getShopChannel();
		Long staffId = calculatePromotionFilter.getStaffId();
		long customerId = calculatePromotionFilter.getCustomerId();
		long customerTypeId = calculatePromotionFilter.getCustomerTypeId();
		Date orderDate = calculatePromotionFilter.getOrderDate();
		Long saleOrderId = calculatePromotionFilter.getSaleOrderId();
		SaleOrderVO orderVO = calculatePromotionFilter.getOrderVO();
		if (null == shopChannel) {
			shopChannel = 0;
		}
		PromotionProductsVO result = new PromotionProductsVO();
		// ds khuyen mai tra ve
		SortedMap<Long, List<SaleOrderDetailVO>> sortListOutPut = new TreeMap<Long, List<SaleOrderDetailVO>>();
		
		// Sap xep cac san pham ban theo product id
		SortedMap<Long, SaleOrderDetailVO> sortListProductSale = new TreeMap<Long, SaleOrderDetailVO>();
		
		//Sap xep danh sach SP ban theo ma SP
//		SortedMap<String, SaleOrderDetailVO> sortlistProductSaleByCode = new TreeMap<String, SaleOrderDetailVO>();
		
		BigDecimal totalAmount = BigDecimal.ZERO;
		Integer totalAvailableQuantity = 0;
		List<SaleOrderLot> lstLot;
		String lstProductIds = "";
		String lstQuantities = "";
		String lstAmounts = "";
		for (int i = 0, size = lstSalesOrderDetail.size(); i < size; i++) {
			SaleOrderDetailVO detail = lstSalesOrderDetail.get(i);
			//Reset sp chua co KM
			detail.getSaleOrderDetail().setHasPromotion(false);
			
			SaleOrderDetailVO orderDetail = new SaleOrderDetailVO();
			SaleOrderDetail orderDTO = new SaleOrderDetail();
			orderDetail.setSaleOrderDetail(orderDTO);
			//reset gia tri cua discountAmount va discountPercent
			detail.getSaleOrderDetail().setDiscountAmount(null);
			detail.getSaleOrderDetail().setDiscountPercent(null);
			
			orderDTO.setQuantity(detail.getSaleOrderDetail().getQuantity());
			orderDTO.setProduct(detail.getSaleOrderDetail().getProduct());
			orderDTO.setPrice(detail.getSaleOrderDetail().getPrice());
			orderDTO.setPriceValue(detail.getSaleOrderDetail().getPriceValue());
			orderDTO.setAmount(detail.getSaleOrderDetail().getAmount());
			orderDTO.setProgramCode(detail.getSaleOrderDetail().getProgramCode());
			totalAmount = totalAmount.add(detail.getSaleOrderDetail().getAmount());
			sortListProductSale.put(Long.valueOf(detail.getSaleOrderDetail().getProduct().getId()), orderDetail);
			
			lstProductIds = lstProductIds + "," + detail.getSaleOrderDetail().getProduct().getId();
			lstQuantities = lstQuantities + "," + detail.getSaleOrderDetail().getQuantity();
			lstAmounts = lstAmounts + "," + detail.getSaleOrderDetail().getAmount();
		}
		
		lstProductIds = lstProductIds.replaceFirst(",", "");
		lstQuantities = lstQuantities.replaceFirst(",", "");
		lstAmounts = lstAmounts.replaceFirst(",", "");

		ArrayList<SaleOrderDetailVO> listProductPromotionsale = new ArrayList<SaleOrderDetailVO>();

		Long keyList = Long.valueOf(100);
		try {
			// Tinh KM cho tung sp
			SaleOrderDetailVO detail = null;
//			Map<Long, Integer> mapProductPromo = new HashMap<Long, Integer>();//luu lai nhung product nao da check km (1 san pham nhieu chuong trinh)
			Map<Long, List<Long>> mapCTKMProduct = new HashMap<Long, List<Long>>();//luu lai cac sp da xet km theo CTKM
			while (sortListProductSale.size() > 0) {
				// Reset price & quantiy
				for (SaleOrderDetailVO sortDetail : sortListProductSale.values()) {
					for (int i = 0, size = lstSalesOrderDetail.size(); i < size; i++) {
						detail = lstSalesOrderDetail.get(i);
						if (sortDetail.getSaleOrderDetail().getProduct().getId().equals(detail.getSaleOrderDetail().getProduct().getId())) {
							sortDetail.getSaleOrderDetail().setQuantity(detail.getSaleOrderDetail().getQuantity());
							sortDetail.getSaleOrderDetail().setPrice(detail.getSaleOrderDetail().getPrice());
							sortDetail.getSaleOrderDetail().setPrice(detail.getSaleOrderDetail().getPrice());
							sortDetail.getSaleOrderDetail().setAmount(detail.getSaleOrderDetail().getAmount());
							break;
						}
					}
				}

				Long key = sortListProductSale.firstKey();

				SaleOrderDetailVO buyProducts = sortListProductSale.get(key);
				Long productId = buyProducts.getSaleOrderDetail().getProduct().getId();

				List<PromotionProgram> lstPromotionProgram = this.getPromotionProgramByProductAndShopAndCustomer(productId, shopId, 
						customerId, orderDate, saleOrderId, staffId, lstProductIds, lstQuantities, lstAmounts);
				/** Tinh khuyen mai */
				if (lstPromotionProgram != null && lstPromotionProgram.size() > 0) {
					for (int i = lstPromotionProgram.size() - 1 ; i >= 0 ; i--) {
						List<Long> lstProductId = mapCTKMProduct.get(lstPromotionProgram.get(i).getId());
						if (lstProductId != null && lstProductId.size() > 0) {
							for (Long id : lstProductId) {
								if (id.equals(productId)) {
									lstPromotionProgram.remove(i);
									break;
								}
							}
						}
					}
					String lstProgramCode = "";
					for (PromotionProgram promotionProgram : lstPromotionProgram) {
						lstProgramCode += lstProgramCode.equals("") ? promotionProgram.getPromotionProgramCode() : ", " + promotionProgram.getPromotionProgramCode();
						boolean b = this.checkPromotionProgramHasPortion(promotionProgram.getId(), shopId, customerId, staffId, orderDate);
						if (b) {
							promotionProgram.setHasPortion(1);
						} else {
							promotionProgram.setHasPortion(0);
						}
						for (SaleOrderDetailVO vo : lstSalesOrderDetail) {
							if (productId.equals(vo.getSaleOrderDetail().getProduct().getId())) {
								vo.getSaleOrderDetail().setProgramCode(lstProgramCode);
							}
						}
					}
					CalcPromotionFilter calcPromotionFilter = new CalcPromotionFilter();
					calcPromotionFilter.setAmtOrder(totalAmount);
					calcPromotionFilter.setLstPromotionProEntity(lstPromotionProgram);
					calcPromotionFilter.setSortListProductSales(sortListProductSale);
					calcPromotionFilter.setListProductPromotionsale(listProductPromotionsale);
					calcPromotionFilter.setKeyList(keyList);
					calcPromotionFilter.setSortListOutPut(sortListOutPut);
					calcPromotionFilter.setShopId(shopId);
					calcPromotionFilter.setPromotionType(SaleOrderDetailVO.ownerTypePromoProduct);
					calcPromotionFilter.setOrderVO(orderVO);
					calcPromotionFilter.setCustomerId(customerId);
					calcPromotionFilter.setStaffId(staffId);
					calcPromotionFilter.setOrderDate(orderDate);
					calcPromotionFilter.setMapCTKMProduct(mapCTKMProduct);
					calcPromotionFilter.setIsDividualWarehouse(calculatePromotionFilter.getIsDividualWarehouse());
					calcPromotion(calcPromotionFilter);

					if (sortListOutPut.size() > 0) {
						keyList = sortListOutPut.lastKey();
					}
				}
				sortListProductSale.remove(key);
			}
			// tinh KM cho don hang
			PromotionProductsVO voPromotionProduct = calPromotionForOrder(mapSaleOrderLot,totalAmount, 
					sortListProductSale,sortListOutPut, listProductPromotionsale, shopId, 
					keyList, customerId, customerTypeId,staffId, orderDate, saleOrderId,orderVO,
					lstProductIds, lstQuantities, lstAmounts);

			// Lay thong tin cho ds sp KM hien thi
			ArrayList<SaleOrderDetailVO> listProductSalePromotion = new ArrayList<SaleOrderDetailVO>();
			ArrayList<SaleOrderDetailVO> listOrderSalePromotion = new ArrayList<SaleOrderDetailVO>();
			ArrayList<SaleOrderDetailVO> listProductSalePromotionMissing = new ArrayList<SaleOrderDetailVO>();

			if (listProductPromotionsale != null && listProductPromotionsale.size() > 0) {
				//Map<Long, List<StockTotal>> mapWarehouse = new HashMap<Long, List<StockTotal>>();
				for (SaleOrderDetailVO promotionProduct : listProductPromotionsale) {
					//Neu co san pham KM thi moi gan lai
					if (promotionProduct.getPromoProduct() != null && promotionProduct.getPromoProduct().getId() > 0) {
						promotionProduct.getSaleOrderDetail().setProduct(promotionProduct.getPromoProduct());
						promotionProduct.getSaleOrderDetail().setProduct(promotionProduct.getPromoProduct());
						// Chi lay sp KM co so luong KM > 0
						if (promotionProduct.getSaleOrderDetail().getQuantity() > 0) {

							List<StockTotalVO> productPromotionInfo = productDAO.getPromotionProductInfo(promotionProduct.getPromoProduct().getId(),
									shopId,shopChannel,customerId,StockObjectType.SHOP.getValue(),orderDate);
							if (productPromotionInfo != null && productPromotionInfo.size() > 0) {
								updatePromotionProductInfo(promotionProduct, productPromotionInfo.get(0));
//								// Sp khong co gia' hieu luc hoac gia' <= 0
//								if (productDAO.checkProductInZ(promotionProduct.getSaleOrderDetail().getProduct().getProductCode())) {
//									if (promotionProduct.getSaleOrderDetail().getPriceValue().compareTo(BigDecimal.ZERO) <= 0) {
//										listProductSalePromotionMissing.add(promotionProduct);
//									}
//								}								
							}
						} else {
							continue;
						}
						
						//Tach kho cho SPKM
						lstLot = new ArrayList<SaleOrderLot>();
						if (promotionProduct.getPromoProduct() != null) {
							totalAvailableQuantity = 0;
							//Load danh sach SPKM dc huong theo Kho
							//List<StockTotal> lst = mapWarehouse.get(promotionProduct.getPromoProduct().getId());
							//if (lst == null) {
							StockTotalFilter stockTotalFilter = new StockTotalFilter();
							stockTotalFilter.setProductId(promotionProduct.getPromoProduct().getId());
							stockTotalFilter.setOwnerType(StockObjectType.SHOP);
							stockTotalFilter.setOwnerId(shopId);
							if (calculatePromotionFilter.getIsDividualWarehouse() == null || calculatePromotionFilter.getIsDividualWarehouse()) {
								stockTotalFilter.setWarehouseType(WarehouseType.PROMOTION);
							} else {
								stockTotalFilter.setOrderByType(WarehouseType.PROMOTION);
							}
							List<StockTotal> lst = stockTotalDAO.getListStockTotalByProductAndOwner(stockTotalFilter);
								//mapWarehouse.put(promotionProduct.getPromoProduct().getId(), lst);
							//}
							if (lst != null && lst.size() > 0) {
								totalAvailableQuantity = promotionProduct.getSaleOrderDetail().getQuantity();
								//Tach kho theo stock_Total va warehouse Id
								for (StockTotal stt : lst) {
									SaleOrderLot lot = new SaleOrderLot();
									lot.setProduct(promotionProduct.getSaleOrderDetail().getProduct());
									lot.setWarehouse(stt.getWarehouse());
									lot.setStockTotal(stt);
									if (stt.getAvailableQuantity() <= 0) {
										continue;
									} else {
										if (promotionProduct.getPromoProduct() != null && totalAvailableQuantity > stt.getAvailableQuantity()) {
											lot.setQuantity(stt.getAvailableQuantity());
											//stt.setAvailableQuantity(0);
											totalAvailableQuantity = totalAvailableQuantity - stt.getAvailableQuantity();
											lstLot.add(lot);
										} else if (promotionProduct.getPromoProduct() != null && totalAvailableQuantity <= stt.getAvailableQuantity()) {
											lot.setQuantity(totalAvailableQuantity);
											//stt.setAvailableQuantity(stt.getAvailableQuantity() - totalAvailableQuantity);
											totalAvailableQuantity = 0;
											lstLot.add(lot);
											break;
										}
									}
								} 
								if (totalAvailableQuantity > 0) {
									if (lstLot.size() > 0) {
										lstLot.get(lstLot.size()-1).setQuantity(lstLot.get(lstLot.size()-1).getQuantity()+totalAvailableQuantity);
									} else {
										StockTotal stt = lst.get(0);
										SaleOrderLot lot = new SaleOrderLot();
										lot.setProduct(promotionProduct.getSaleOrderDetail().getProduct());
										lot.setWarehouse(stt.getWarehouse());
										lot.setStockTotal(stt);
										lot.setQuantity(totalAvailableQuantity);
//										/*
//										 * Truong hop sau khi tach kho ma so luong ton kho < so luong KM thi 
//										 * lay gia tri ton kho lam so luong KM duoc huong
//										 * @Modified by NhanLT6 - 19/11/2014
//										 * */
//										lot.setQuantity(stt.getAvailableQuantity());
										totalAvailableQuantity = 0;
										lstLot.add(lot);
									}
								}
							} else {
								SaleOrderLot lot = new SaleOrderLot();
								lot.setSaleOrderDetail(promotionProduct.getSaleOrderDetail());
								lstLot.add(lot);
							}
						} else {
							SaleOrderLot lot = new SaleOrderLot();
							lot.setSaleOrderDetail(promotionProduct.getSaleOrderDetail());
							lstLot.add(lot);
						}
						promotionProduct.setSaleOrderLot(lstLot);
					} else if (promotionProduct.getListPromotionForPromo21() != null
							&& promotionProduct.getListPromotionForPromo21().size() > 0) {
						for (SaleOrderDetailVO promotionProductZV21 : promotionProduct.getListPromotionForPromo21()) {
							//Neu co san pham KM thi moi gan lai
							if (promotionProductZV21.getPromoProduct() != null && promotionProductZV21.getPromoProduct().getId() > 0) {
								promotionProductZV21.getSaleOrderDetail().setProduct(promotionProductZV21.getPromoProduct());
								// Chi lay sp KM co so luong KM > 0
								if (promotionProductZV21.getSaleOrderDetail().getQuantity() > 0) {
									List<StockTotalVO> productPromotionInfo = productDAO.getPromotionProductInfo(promotionProductZV21.getPromoProduct().getId(),
											shopId,shopChannel,customerId,StockObjectType.SHOP.getValue(),orderDate);
									if (productPromotionInfo != null && productPromotionInfo.size() > 0) {
										updatePromotionProductInfo(promotionProductZV21, productPromotionInfo.get(0));
										// Sp khong co gia' hieu luc hoac gia' <= 0
										if (productDAO.checkProductInZ(promotionProductZV21.getSaleOrderDetail().getProduct().getProductCode())) {
											//if (promotionProductZV21.getSaleOrderDetail().getPriceValue().compareTo(BigDecimal.ZERO) <= 0) {
											if (promotionProductZV21.getSaleOrderDetail().getPriceValue().signum() < 1) {
												listProductSalePromotionMissing.add(promotionProductZV21);
											}											
										}
									}
								} else {
									continue;
								}
								
								//Tach kho cho sp km
								totalAvailableQuantity = 0;
								StockTotalFilter stockTotalFilter = new StockTotalFilter();
								stockTotalFilter.setProductId(promotionProductZV21.getPromoProduct().getId());
								stockTotalFilter.setOwnerType(StockObjectType.SHOP);
								stockTotalFilter.setOwnerId(shopId);
								if (calculatePromotionFilter.getIsDividualWarehouse() == null || calculatePromotionFilter.getIsDividualWarehouse()) {
									stockTotalFilter.setWarehouseType(WarehouseType.PROMOTION);
								} else {
									stockTotalFilter.setOrderByType(WarehouseType.PROMOTION);
								}
								List<StockTotal> lst = stockTotalDAO.getListStockTotalByProductAndOwner(stockTotalFilter);
								if (lst != null && lst.size() > 0) {
									lstLot = new ArrayList<SaleOrderLot>();
									totalAvailableQuantity = promotionProductZV21.getSaleOrderDetail().getQuantity();
									for (StockTotal stt : lst) {
										SaleOrderLot lot = new SaleOrderLot();
										lot.setProduct(promotionProductZV21.getSaleOrderDetail().getProduct());
										lot.setWarehouse(stt.getWarehouse());
										lot.setStockTotal(stt);
										if (stt.getAvailableQuantity() <= 0) {
											continue;
										} else if (promotionProductZV21.getPromoProduct() != null && 
												totalAvailableQuantity > stt.getAvailableQuantity()) {
											lot.setQuantity(stt.getAvailableQuantity());
											totalAvailableQuantity = totalAvailableQuantity - stt.getAvailableQuantity();
											lstLot.add(lot);
										} else if (promotionProductZV21.getPromoProduct() != null && 
												totalAvailableQuantity <= stt.getAvailableQuantity()) {
											lot.setQuantity(totalAvailableQuantity);
											totalAvailableQuantity = 0;
											lstLot.add(lot);
											break;
										} 
									}
									if (totalAvailableQuantity > 0) {
										if (lstLot.size() > 0) {
											lstLot.get(lstLot.size()-1).setQuantity(lstLot.get(lstLot.size()-1).getQuantity()+totalAvailableQuantity);
										} else {
											StockTotal stt = lst.get(0);
											SaleOrderLot lot = new SaleOrderLot();
											lot.setProduct(promotionProduct.getSaleOrderDetail().getProduct());
											lot.setWarehouse(stt.getWarehouse());
											lot.setStockTotal(stt);
											lot.setQuantity(totalAvailableQuantity);
											/*
											 * Truong hop sau khi tach kho ma so luong ton kho < so luong KM thi 
											 * lay gia tri ton kho lam so luong KM duoc huong
											 * @Modified by NhanLT6 - 19/11/2014
											 * */
//											lot.setQuantity(stt.getAvailableQuantity());
											totalAvailableQuantity = 0;
											lstLot.add(lot);
										}
									}
									promotionProductZV21.setSaleOrderLot(lstLot);
								}
							} 
						}
					} 

					// Sap xep theo programe code
					// CTKM cho sp
					if (SaleOrderDetailVO.PROMOTION_FOR_PRODUCT == promotionProduct.getPromotionType()) {
						int i = 0;
						int size = listProductSalePromotion.size();
						for (i = 0; i < size; i++) {
							SaleOrderDetailVO promotionTemp = listProductSalePromotion.get(i);
							// Lon hon
							if (promotionProduct.getSaleOrderDetail() != null && promotionProduct.getSaleOrderDetail().getProgramCode()
									.compareToIgnoreCase(promotionTemp.getSaleOrderDetail().getProgramCode()) < 0) {
								break;
							}
						}
						listProductSalePromotion.add(i, promotionProduct);
					} else {// CTKM cho don hang
						int i = 0;
						int size = listOrderSalePromotion.size();
						for (i = 0; i < size; i++) {
							SaleOrderDetailVO promotionTemp = listOrderSalePromotion.get(i);
							// Lon hon
							if (promotionProduct.getSaleOrderDetail().getProgramCode()
									.compareToIgnoreCase(promotionTemp.getSaleOrderDetail().getProgramCode()) < 0) {
								break;
							}
						}

						listOrderSalePromotion.add(i, promotionProduct);
					}
				}
				// Add list promotion of order at the end of list promotion product
				listProductSalePromotion.addAll(listOrderSalePromotion);
			}

			// Ds san pham hien thi
			sortListOutPut.put(Long.valueOf(1), listProductSalePromotion);

			// Ds san pham ko co trong ton kho or ko co trong kho cua sp hien thi
			sortListOutPut.put(Long.valueOf(10), listProductSalePromotionMissing);

			// Ds sp ko co trong ton kho or ko co trong kho cua sp doi
			listProductSalePromotionMissing = new ArrayList<SaleOrderDetailVO>();

			if (sortListOutPut.size() > 2) {
				Iterator<Long> it = sortListOutPut.keySet().iterator();
				it.next();// sp hien thi
				it.next();// sp hien thi bi loi

				while (it.hasNext()) {
					Long md = it.next();
					List<SaleOrderDetailVO> listProductChange = sortListOutPut.get(md);
					listProductSalePromotion = new ArrayList<SaleOrderDetailVO>();

					for (SaleOrderDetailVO promotionProduct : listProductChange) {
						// Neu co san pham KM thi moi gan lai
						if (promotionProduct.getPromoProduct() != null 
								&&promotionProduct.getPromoProduct().getId() > 0) {
							promotionProduct.getSaleOrderDetail().setProduct(promotionProduct.getPromoProduct());
							promotionProduct.getSaleOrderDetail().setProduct(promotionProduct.getPromoProduct());
							// Chi lay sp KM co so luong KM > 0
							if (promotionProduct.getSaleOrderDetail().getQuantity() > 0) {
								
								List<StockTotalVO> productPromotionInfo = productDAO.getPromotionProductInfo(promotionProduct.getPromoProduct().getId(),
										shopId,shopChannel,customerId,StockObjectType.SHOP.getValue(),orderDate);
								
								if (productPromotionInfo != null && productPromotionInfo.size() > 0) {
									updatePromotionProductInfo(promotionProduct, productPromotionInfo.get(0));

									// Sp khong co gia' hieu luc hoac gia' <= 0
//									if (productDAO.checkProductInZ(promotionProduct.getSaleOrderDetail().getProduct().getProductCode())) {
//										if (promotionProduct.getSaleOrderDetail().getPriceValue().compareTo(BigDecimal.ZERO) <= 0) {
//											listProductSalePromotionMissing.add(promotionProduct);
//										}										
//									}
								}
							} else {
								continue;
							}
						}
						listProductSalePromotion.add(promotionProduct);
					}
					sortListOutPut.put(md, listProductSalePromotion);
				}
				// Ds san pham ko co trong ton kho or ko co trong kho cua sp doi
				// hang
				sortListOutPut.put(Long.valueOf(11), listProductSalePromotionMissing);
			}

			// Get information for product of zv21
			if (voPromotionProduct.getListPromotionForOrderChange() != null
					&& voPromotionProduct.getListPromotionForOrderChange().size() > 0) {
				for (SaleOrderDetailVO promotionPrograme : voPromotionProduct.getListPromotionForOrderChange()) {
					if (promotionPrograme.getListPromotionForPromo21() != null
							&& promotionPrograme.getListPromotionForPromo21().size() > 0) {
						for (SaleOrderDetailVO promotionProduct : promotionPrograme.getListPromotionForPromo21()) {
							// Neu co san pham KM thi moi gan lai
							if (promotionProduct.getPromoProduct().getId() > 0) {
								promotionProduct.getSaleOrderDetail().setProduct(promotionProduct.getPromoProduct());
								promotionProduct.getSaleOrderDetail().setProduct(promotionProduct.getPromoProduct());
								// Chi lay sp KM co so luong KM > 0
								if (promotionProduct.getSaleOrderDetail().getQuantity() > 0) {

									List<StockTotalVO> productPromotionInfo = productDAO.getPromotionProductInfo(promotionProduct.getPromoProduct().getId(),
											shopId,shopChannel,customerId,StockObjectType.SHOP.getValue(),orderDate);

									if (productPromotionInfo != null) {
										updatePromotionProductInfo(promotionProduct, productPromotionInfo.get(0));

										// Sp khong co gia' hieu luc hoac gia' <= 0
										if (!productDAO.checkProductInZ(promotionProduct.getSaleOrderDetail().getProduct().getProductCode())) {
											if (promotionProduct.getSaleOrderDetail().getPriceValue().compareTo(BigDecimal.ZERO) <= 0) {
												listProductSalePromotionMissing.add(promotionProduct);
											}
										}
									}
								} else {
									continue;
								}
								
								//Tach kho cho sp km
								totalAvailableQuantity = 0;
								StockTotalFilter stockTotalFilter = new StockTotalFilter();
								stockTotalFilter.setProductId(promotionProduct.getPromoProduct().getId());
								stockTotalFilter.setOwnerType(StockObjectType.SHOP);
								stockTotalFilter.setOwnerId(shopId);
								if (calculatePromotionFilter.getIsDividualWarehouse() == null || calculatePromotionFilter.getIsDividualWarehouse()) {
									stockTotalFilter.setWarehouseType(WarehouseType.PROMOTION);
								} else {
									stockTotalFilter.setOrderByType(WarehouseType.PROMOTION);
								}
								List<StockTotal> lst = stockTotalDAO.getListStockTotalByProductAndOwner(stockTotalFilter);
								if (lst != null && lst.size() > 0 && lst.get(0) != null) {
									lstLot = new ArrayList<SaleOrderLot>();
									totalAvailableQuantity = promotionProduct.getSaleOrderDetail().getQuantity();
									for (StockTotal stt : lst) {
										SaleOrderLot lot = new SaleOrderLot();
										lot.setProduct(promotionProduct.getSaleOrderDetail().getProduct());
										lot.setWarehouse(stt.getWarehouse());
										lot.setStockTotal(stt);
										if (stt.getAvailableQuantity() <= 0) {
											continue;
										} else if (promotionProduct.getPromoProduct() != null && 
												totalAvailableQuantity > stt.getAvailableQuantity()) {
											lot.setQuantity(stt.getAvailableQuantity());
											totalAvailableQuantity = totalAvailableQuantity - stt.getAvailableQuantity();
											lstLot.add(lot);
										} else if (promotionProduct.getPromoProduct() != null && 
												totalAvailableQuantity <= stt.getAvailableQuantity()) {
											lot.setQuantity(totalAvailableQuantity);
											totalAvailableQuantity = 0;
											lstLot.add(lot);
											break;
										} 
									}
									if (totalAvailableQuantity > 0) {
										if (lstLot.size() > 0) {
											lstLot.get(lstLot.size()-1).setQuantity(lstLot.get(lstLot.size()-1).getQuantity()+totalAvailableQuantity);
										} else {
											StockTotal stt = lst.get(0);
											SaleOrderLot lot = new SaleOrderLot();
											lot.setProduct(promotionProduct.getSaleOrderDetail().getProduct());
											lot.setWarehouse(stt.getWarehouse());
											lot.setStockTotal(stt);
											lot.setQuantity(totalAvailableQuantity);
											/*
											 * Truong hop sau khi tach kho ma so luong ton kho < so luong KM thi 
											 * lay gia tri ton kho lam so luong KM duoc huong
											 * @Modified by NhanLT6 - 19/11/2014
											 * */
//											lot.setQuantity(stt.getAvailableQuantity());
											totalAvailableQuantity = 0;
											lstLot.add(lot);
										}
									}
									promotionProduct.setSaleOrderLot(lstLot);
								}
							} 
						}
					}
				}
			}
			
			
			//Luu danh sach do~i KM
			result.setListPromotionForOrderChange(voPromotionProduct.getListPromotionForOrderChange());
			result.setListOpenPromotionForOrder(voPromotionProduct.getListOpenPromotionForOrder());
			
			// Lay ton kho cho sp ban tuong ung voi loai don hang
			/** @author tientv11 kiem tra */
			for (SaleOrderDetailVO product : lstSalesOrderDetail) {
				List<StockTotalVO> productPromotionInfo = productDAO.getPromotionProductInfo(product.getProduct().getId(),
						shopId,shopChannel,customerId,StockObjectType.SHOP.getValue(),orderDate);
				if (productPromotionInfo != null) {
					product.setStock(productPromotionInfo.get(0).getQuantity());
					product.setAvailableQuantity(productPromotionInfo.get(0).getAvailableQuantity());
				}
			}

			// Lay ton kho cho sp KM KHONG PHAI TU DONG tuong ung voi loai don hang
			for (SaleOrderDetailVO product : listProductPromotionsale) {
				if (product.getSaleOrderDetail() != null && !ProgramType.AUTO_PROM.equals(product.getSaleOrderDetail().getProgramType())) {
					List<StockTotalVO> productPromotionInfo = productDAO.getPromotionProductInfo(product.getProduct().getId(),
							shopId,shopChannel,customerId,StockObjectType.SHOP.getValue(),orderDate);
					if (productPromotionInfo != null) {
						product.setStock(productPromotionInfo.get(0).getQuantity());
						product.setAvailableQuantity(productPromotionInfo.get(0).getAvailableQuantity());
					}
				}
			}
			
			//Tinh % discount, discount amount
			listProductSalePromotion = (ArrayList<SaleOrderDetailVO>) sortListOutPut.get(Long.valueOf(1));
			ArrayList<SaleOrderDetailVO> listPercentPromotion = new ArrayList<SaleOrderDetailVO>();
			//Reset gia tri
			for (SaleOrderDetailVO product : lstSalesOrderDetail) {
				product.getSaleOrderDetail().setDiscountPercent(0F);
				product.getSaleOrderDetail().setDiscountAmount(BigDecimal.ZERO);
			}
			
			//Xoa het cac phan tu trong list sp mua sau khi tinh KM
			for (SaleOrderDetailVO product : lstSalesOrderDetail) {
				if (product.getListPromoDetail() == null) {
					product.setListPromoDetail(new ArrayList<SaleOrderPromoDetail>());
				} else{
					product.getListPromoDetail().clear();
				}
			}

//			for (SaleOrderDetailVO vdp : lstSalesOrderDetail) {
//				sortlistProductSaleByCode.put(String.valueOf(vdp.getSaleOrderDetail().getProduct().getProductCode()), vdp);
//			}

			Integer isOwner;
			BigDecimal totalDiscountLot;
			for (SaleOrderDetailVO promotion : listProductSalePromotion) {
				if (SaleOrderDetailVO.PROMOTION_FOR_PRODUCT == promotion.getPromotionType() || 
						SaleOrderDetailVO.PROMOTION_FOR_ORDER == promotion.getPromotionType()) {
					//Xu ly cho cac KM % cua sp
					if (SaleOrderDetailVO.FREE_PERCENT == promotion.getType() ||
							//Moi xu ly cho KM mua Amount tang Amount: ZV05, ZV11, ZV17
							SaleOrderDetailVO.FREE_PRICE == promotion.getType()) {
						
						BigDecimal totalDiscount = BigDecimal.ZERO;
						BigDecimal discountAmount = BigDecimal.ZERO;
						BigDecimal sochia = BigDecimal.ZERO;
						BigDecimal fixDiscountAmount = BigDecimal.ZERO;
						if (promotion.getListBuyProduct().size() > 0) {
							for (GroupLevelDetail levelDetail : promotion.getListBuyProduct()) {
								for (SaleOrderDetailVO product : lstSalesOrderDetail) {
									if (product.getSaleOrderDetail().getProduct().getId().equals(levelDetail.getProduct().getId())) {
//										try {
//											product.getSaleOrderDetail().setProgramCode(levelDetail.getGroupLevel().getProductGroup().getPromotionProgram().getPromotionProgramCode());
//										} catch (Exception e1) {
//											// pass through
//										}
										totalDiscountLot = BigDecimal.ZERO;
										product.getSaleOrderDetail().setHasPromotion(true);
										/** Tinh tren tong san pham */
										product.getSaleOrderDetail().setDiscountPercent(promotion.getSaleOrderDetail().getDiscountPercent());
										BigDecimal amount = BigDecimal.ZERO;
										if (promotion.getIsBundle()) {
											amount = levelDetail.getValue();
										} else {
											amount = product.getSaleOrderDetail().getAmount();
										}
										sochia = amount.multiply(new BigDecimal((promotion.getSaleOrderDetail().getDiscountPercent()* PromotionCalc.ROUND_PERCENT)));
										//discountAmount = new BigDecimal(Math.round(sochia.doubleValue()/ PromotionCalc.ROUND_NO_PERCENT));
										discountAmount = new BigDecimal(Math.floor(sochia.doubleValue()/ PromotionCalc.ROUND_NO_PERCENT));
										
										//Tinh toan de lam tron, tong so tien <= tong tien KM
										fixDiscountAmount = promotion.getSaleOrderDetail().getDiscountAmount();
										if(discountAmount.compareTo(fixDiscountAmount) > 0) {
											discountAmount = fixDiscountAmount;
										}
										if(totalDiscount.add(discountAmount).compareTo(fixDiscountAmount)>0) {
											discountAmount = fixDiscountAmount.subtract(totalDiscount);
										} else {
											if(promotion.getListBuyProduct().indexOf(levelDetail) == promotion.getListBuyProduct().size() - 1) {
												discountAmount = fixDiscountAmount.subtract(totalDiscount);
											}
										}
										totalDiscount = totalDiscount.add(discountAmount);
										
										if (product.getSaleOrderDetail().getDiscountAmount() != null) {
											product.getSaleOrderDetail().setDiscountAmount(product.getSaleOrderDetail().getDiscountAmount().add(discountAmount));
										} else {
											product.getSaleOrderDetail().setDiscountAmount(discountAmount);
										}
										if (SaleOrderDetailVO.PROMOTION_FOR_ORDER != promotion.getPromotionType()) {
//											product.getSaleOrderDetail().setProgramCode(promotion.getSaleOrderDetail().getProgramCode());
											product.setPromotionType(promotion.getPromotionType());
											product.setType(promotion.getType());
										}
										
										listPercentPromotion.add(promotion);
										
										//promo detail sau khi tinh KM lai
										SaleOrderPromoDetail promoDetail = new SaleOrderPromoDetail();
										promoDetail.setDiscountAmount(discountAmount);
										promoDetail.setDiscountPercent(promotion.getSaleOrderDetail().getDiscountPercent());
										if (BigDecimal.ZERO.equals(discountAmount)) { //if (discountAmount.compareTo(BigDecimal.ZERO) == 0) {
											promoDetail.setDiscountPercent(0F);
										}
										//1. Là khuyến mãi của chính sp, 0. Là khuyến mãi do chia đều (từ đơn hàng, nhóm)
										isOwner = (promotion.getNumBuyProductInGroup() == 1) ? 1 : 0; 
										promoDetail.setIsOwner(isOwner);
										promoDetail.setMaxAmountFree(discountAmount);
										promoDetail.setProgramCode(promotion.getSaleOrderDetail().getProgramCode());
										
										if (promotion.getSaleOrderDetail() != null) {
											promoDetail.setProductGroupId(promotion.getSaleOrderDetail().getProductGroupId());
											try {
												//promoDetail.setProductGroup(promotion.getSaleOrderDetail().getProductGroup());
												ProductGroup tmpProductGroup = this.getProductGroup(promotion.getSaleOrderDetail().getProductGroupId());
												promoDetail.setProductGroup(tmpProductGroup);
											} catch (Exception e) {
												// pass through
											}
											
											promoDetail.setGroupLevelId(promotion.getSaleOrderDetail().getGroupLevelId());
											try {
												GroupLevel tmpGroupLevel = this.getGroupLevelById(promotion.getSaleOrderDetail().getGroupLevelId());
												//promoDetail.setGroupLevel(promotion.getSaleOrderDetail().getGroupLevel());
												promoDetail.setGroupLevel(tmpGroupLevel);
											} catch (Exception e) {
												// pass through
											}											
										}
										//KM 
										promoDetail.setProgramType(promotion.getSaleOrderDetail().getProgramType()); 
										promoDetail.setProgrameTypeCode(promotion.getSaleOrderDetail().getProgrameTypeCode());
										product.getListPromoDetail().add(promoDetail);
//										product.setLevelPromo(promotion.getSaleOrderDetail().getLevelPromo());
										BigDecimal remainAmount =  discountAmount;
										/** Phan bo khuyen mai tren tung kho */
										for (SaleOrderLot lot : product.getSaleOrderLot()) {
											lot.setHasPromotion(1);
											BigDecimal amountLot = lot.getPriceValue().multiply(new BigDecimal(lot.getQuantity()));
											if (promotion.getIsBundle()) {
												//Tinh toan de lam tron, tong so tien <= tong tien KM
												//Neu sp co nhieu kho thi phan bo dong tien theo thu tu uu tien kho sao cho tong thanh tien - CKMH cua tung kho
												//khong duoc be hon 0 , neu = 0 thi phan bo tien xuong kho tiep theo
												//TungMT fix * - khong can quan tam chiet khau vuot so tien mua
//												if (amountLot.compareTo(remainAmount) > 0) {
												BigDecimal discountAmountLot = BigDecimal.ZERO;
												sochia = new BigDecimal(lot.getQuantity()).divide(new BigDecimal(product.getSaleOrderDetail().getQuantity()), 20, RoundingMode.HALF_UP);
												discountAmountLot = sochia.multiply(discountAmount).setScale(0, RoundingMode.HALF_UP);
													if (lot.getDiscountAmount() != null) {
														lot.setDiscountAmount(lot.getDiscountAmount().add(discountAmountLot));
													} else {
														lot.setDiscountAmount(discountAmountLot);
													}
													lot.setDiscountPercent(product.getSaleOrderDetail().getDiscountPercent());
//													break;
//												} else if (amountLot.compareTo(remainAmount) <= 0) {
//													remainAmount = remainAmount.subtract(amountLot);
//													if (lot.getDiscountAmount() != null) {
//														lot.setDiscountAmount(lot.getDiscountAmount().add(amountLot));
//													} else {
//														lot.setDiscountAmount(amountLot);
//													}
//												}
												lot.setDiscountPercent(product.getSaleOrderDetail().getDiscountPercent());
											} else {
												BigDecimal discountAmountLot = BigDecimal.ZERO;
												if (SaleOrderDetailVO.FREE_PRICE == promotion.getType()) {
													sochia = amountLot.divide(amount, 2, RoundingMode.HALF_UP);
													discountAmountLot = new BigDecimal(Math.round(sochia.multiply(discountAmount).doubleValue()));
												} else {
													sochia = amountLot.multiply(new BigDecimal(promotion.getSaleOrderDetail().getDiscountPercent()));
													discountAmountLot = sochia.divide(new BigDecimal(100), 0, RoundingMode.HALF_UP);
												}
												
												BigDecimal fixDiscountAmountLot = discountAmount;
												if(discountAmountLot.compareTo(fixDiscountAmountLot) > 0) {
													discountAmountLot = fixDiscountAmountLot;
												}
												if(totalDiscountLot.add(discountAmountLot).compareTo(fixDiscountAmountLot)>0) {
													discountAmountLot = fixDiscountAmountLot.subtract(totalDiscountLot);
												} else {
													if(product.getSaleOrderLot().indexOf(lot) == product.getSaleOrderLot().size() - 1) {
														discountAmountLot = fixDiscountAmountLot.subtract(totalDiscountLot);
													}
												}
												totalDiscountLot = totalDiscountLot.add(discountAmountLot);
												if (lot.getDiscountAmount() != null) {
													lot.setDiscountAmount(lot.getDiscountAmount().add(discountAmountLot));
												} else {
													lot.setDiscountAmount(discountAmountLot);
												}
												lot.setDiscountPercent(product.getSaleOrderDetail().getDiscountPercent());
												lot.setLevelPromo(product.getLevelPromo());
											}
										}
										break;
									}
								}
							}
						} else {
							//KM don hang
							for (SaleOrderDetailVO product : lstSalesOrderDetail) {
								if (product.getSaleOrderDetail().getDiscountPercent() != null) {
									product.getSaleOrderDetail().setDiscountPercent(product.getSaleOrderDetail().getDiscountPercent()
											+ promotion.getSaleOrderDetail().getDiscountPercent());
								} else {
									product.getSaleOrderDetail().setDiscountPercent(promotion.getSaleOrderDetail().getDiscountPercent());
								}
								sochia = (product.getSaleOrderDetail().getAmount().multiply(new BigDecimal((promotion.getSaleOrderDetail().getDiscountPercent() * PromotionCalc.ROUND_PERCENT))));
								discountAmount = new BigDecimal(Math.round(sochia.doubleValue()/ PromotionCalc.ROUND_NO_PERCENT));
								
								//Tinh toan de lam tron, tong so tien <= tong tien KM
								fixDiscountAmount = promotion.getSaleOrderDetail().getDiscountAmount();
								if(discountAmount.compareTo(fixDiscountAmount) > 0) {
									discountAmount = fixDiscountAmount;
								}
								if(totalDiscount.add(discountAmount).compareTo(fixDiscountAmount) > 0) {
									discountAmount = fixDiscountAmount.subtract(totalDiscount);
								} else {
									if(lstSalesOrderDetail.indexOf(product) == lstSalesOrderDetail.size() - 1) {
										discountAmount = fixDiscountAmount.subtract(totalDiscount);
									}
								}
								totalDiscount = totalDiscount.add(discountAmount);
								if (product.getSaleOrderDetail().getDiscountAmount() != null) {
									product.getSaleOrderDetail().setDiscountAmount(product.getSaleOrderDetail().getDiscountAmount().add(discountAmount));
								} else {
									product.getSaleOrderDetail().setDiscountAmount(discountAmount);
								}
								product.setLevelPromo(promotion.getLevelPromo());
								//promo detail sau khi tinh KM lai
								SaleOrderPromoDetail promoDetail = new SaleOrderPromoDetail();
								promoDetail.setDiscountAmount(discountAmount);
								promoDetail.setDiscountPercent(promotion.getSaleOrderDetail().getDiscountPercent());
								if (promotion.getSaleOrderDetail() != null) {
									promoDetail.setProductGroupId(promotion.getSaleOrderDetail().getProductGroupId());
									try {
										//promoDetail.setProductGroup(promotion.getSaleOrderDetail().getProductGroup());
										ProductGroup tmpProductGroup = this.getProductGroup(promotion.getSaleOrderDetail().getProductGroupId());
										promoDetail.setProductGroup(tmpProductGroup);
									} catch (Exception e) {
										// pass through
									}
									
									promoDetail.setGroupLevelId(promotion.getSaleOrderDetail().getGroupLevelId());
									try {
										GroupLevel tmpGroupLevel = this.getGroupLevelById(promotion.getSaleOrderDetail().getGroupLevelId());
										//promoDetail.setGroupLevel(promotion.getSaleOrderDetail().getGroupLevel());
										promoDetail.setGroupLevel(tmpGroupLevel);
									} catch (Exception e) {
										// pass through
									}
								}
								//KM đơn hang : isOwner : 2
								promoDetail.setIsOwner(2);
								promoDetail.setMaxAmountFree(discountAmount);
								promoDetail.setProgramCode(promotion.getSaleOrderDetail().getProgramCode());
								promoDetail.setSaleOrderDetail(product.getSaleOrderDetail());
								//KM 
								promoDetail.setProgramType(promotion.getSaleOrderDetail().getProgramType()); 
								promoDetail.setProgrameTypeCode(promotion.getSaleOrderDetail().getProgrameTypeCode());
//								product.getListPromoDetail().add(promoDetail);
								if (SaleOrderDetailVO.PROMOTION_FOR_PRODUCT == promotion.getPromotionType()) {
									listPercentPromotion.add(promotion);
								} /*else if (SaleOrderDetailVO.PROMOTION_FOR_ORDER == promotion.getPromotionType()) {
									
								}*/
//								/** Phan bo khuyen mai tren tung kho */
//								for (SaleOrderLot lot: product.getSaleOrderLot()) {
//									
//									BigDecimal amountLot = lot.getPrice().multiply(new BigDecimal(lot.getQuantity()));
//									sochia = amountLot.multiply(new BigDecimal((promotion.getSaleOrderDetail().getDiscountPercent()* PromotionCalc.ROUND_PERCENT)));
//									
//									discountAmount = new BigDecimal(Math.round(sochia.doubleValue()/ PromotionCalc.ROUND_NO_PERCENT));
//									lot.setDiscountAmount(discountAmount);
//									lot.setDiscountPercent(product.getSaleOrderDetail().getDiscountPercent());;
//								}
							}
						}
					} else {
						if (promotion.getListBuyProduct() != null && promotion.getListBuyProduct().size() > 0) {
							//Cap nhat nhung sp ban dat duoc KM de luu programCode
							for(GroupLevelDetail levelDetail : promotion.getListBuyProduct()) {
								for (SaleOrderDetailVO product : lstSalesOrderDetail) {
									if(product.getSaleOrderDetail().getProduct().getId().equals(levelDetail.getProduct().getId())) {
//										try {
//											product.getSaleOrderDetail().setProgramCode(levelDetail.getGroupLevel().getProductGroup().getPromotionProgram().getPromotionProgramCode());
//										} catch (Exception e1) {
//											// pass through
//										}
										product.getSaleOrderDetail().setHasPromotion(true);
										for (SaleOrderLot lot: product.getSaleOrderLot()) {
											lot.setHasPromotion(1);
										}
									}
								}
							}
						}
					}
				}
			}
			
			if (result.getListPromotionForOrderChange() != null) {
				//Tinh KM don hang cho doi CTKM
				List<SaleOrderDetailVO> lstSalesOrderDetailForOrder = new ArrayList<SaleOrderDetailVO>();
				for (SaleOrderDetailVO product : lstSalesOrderDetail) {
					lstSalesOrderDetailForOrder.add(product);
				}
				//Tinh gia tri cho sale promo detail cho doi CTKM don hang
				for (SaleOrderDetailVO promotion : result.getListPromotionForOrderChange()) {
					if (SaleOrderDetailVO.PROMOTION_FOR_ORDER == promotion.getPromotionType()) {
						//Xu ly cho cac KM % cua don hang doi CTKM
						if (SaleOrderDetailVO.FREE_PERCENT == promotion.getType() || SaleOrderDetailVO.FREE_PRICE == promotion.getType()) {
							BigDecimal totalDiscount = BigDecimal.ZERO;
							/*if(promotion.getListBuyProduct().size() > 0) {
							} else {*/
								//KM don hang                                 
								for (SaleOrderDetailVO product : lstSalesOrderDetail) {
									
									BigDecimal sochia = (product.getSaleOrderDetail().getAmount().multiply(new BigDecimal((promotion.getSaleOrderDetail().getDiscountPercent() * PromotionCalc.ROUND_PERCENT))));
									BigDecimal discountAmount = new BigDecimal(Math.round(sochia.doubleValue()/ PromotionCalc.ROUND_NO_PERCENT));
									
									//Tinh toan de lam tron, tong so tien <= tong tien KM
									BigDecimal fixDiscountAmount = promotion.getSaleOrderDetail().getDiscountAmount();
									if(discountAmount.compareTo(fixDiscountAmount) > 0) {
										discountAmount = fixDiscountAmount;
									}
									if(totalDiscount.add(discountAmount).compareTo(fixDiscountAmount) > 0) {
										discountAmount = fixDiscountAmount.subtract(totalDiscount);
									} else {
										if(lstSalesOrderDetail.indexOf(product) == lstSalesOrderDetail.size() - 1) {
											discountAmount = fixDiscountAmount.subtract(totalDiscount);
										}
									}
									totalDiscount = totalDiscount.add(discountAmount);
									
									//promo detail sau khi tinh KM lai
									SaleOrderPromoDetail promoDetail = new SaleOrderPromoDetail();
									promoDetail.setDiscountAmount(discountAmount);
									promoDetail.setDiscountPercent(promotion.getSaleOrderDetail().getDiscountPercent());
									//KM đơn hang : isOwner : 2
									promoDetail.setIsOwner(2);
									promoDetail.setMaxAmountFree(discountAmount);
									promoDetail.setProgramCode(promotion.getSaleOrderDetail().getProgramCode());
									//KM 
									promoDetail.setProgramType(promotion.getSaleOrderDetail().getProgramType()); 
									promoDetail.setProgrameTypeCode(promotion.getSaleOrderDetail().getProgrameTypeCode());
									promotion.getListPromoDetail().add(promoDetail);
									promoDetail.setSaleOrderDetail(product.getSaleOrderDetail());
									if (promotion.getSaleOrderDetail() != null) {
										promoDetail.setProductGroupId(promotion.getSaleOrderDetail().getProductGroupId());
										try {
											//promoDetail.setProductGroup(promotion.getSaleOrderDetail().getProductGroup());
											ProductGroup tmpProductGroup = this.getProductGroup(promotion.getSaleOrderDetail().getProductGroupId());
											promoDetail.setProductGroup(tmpProductGroup);
										} catch (Exception e) {
											// pass through
										}
										
										promoDetail.setGroupLevelId(promotion.getSaleOrderDetail().getGroupLevelId());
										try {
											GroupLevel tmpGroupLevel = this.getGroupLevelById(promotion.getSaleOrderDetail().getGroupLevelId());
											//promoDetail.setGroupLevel(promotion.getSaleOrderDetail().getGroupLevel());
											promoDetail.setGroupLevel(tmpGroupLevel);
										} catch (Exception e) {
											// pass through
										}											
									}
									product.getListPromoDetail().add(promoDetail);
								}
							//}
						} 
					} else if (SaleOrderDetailVO.PROMOTION_FOR_ZV21 == promotion.getPromotionType()) {
						 if (promotion.getListPromotionForPromo21() != null	&& promotion.getListPromotionForPromo21().size() > 0) {
								for (SaleOrderDetailVO promotionProductZV21 : promotion.getListPromotionForPromo21()) {
									//Neu co san pham KM thi moi gan lai
									if (promotionProductZV21.getPromoProduct() != null && promotionProductZV21.getPromoProduct().getId() > 0) {
										promotionProductZV21.getSaleOrderDetail().setProduct(promotionProductZV21.getPromoProduct());
										// Chi lay sp KM co so luong KM > 0
										if (promotionProductZV21.getSaleOrderDetail().getQuantity() > 0) {
											List<StockTotalVO> productPromotionInfo = productDAO.getPromotionProductInfo(promotionProductZV21.getPromoProduct().getId(),
													shopId,shopChannel,customerId,StockObjectType.SHOP.getValue(),orderDate);
											if (productPromotionInfo != null && productPromotionInfo.size() > 0) {
												updatePromotionProductInfo(promotionProductZV21, productPromotionInfo.get(0));
												// Sp khong co gia' hieu luc hoac gia' <= 0
												if (!productDAO.checkProductInZ(promotionProductZV21.getSaleOrderDetail().getProduct().getProductCode())) {
													if (promotionProductZV21.getSaleOrderDetail().getPriceValue().compareTo(BigDecimal.ZERO) <= 0) {
														listProductSalePromotionMissing.add(promotionProductZV21);
													}
												}
											}
										} else {
											continue;
										}
										//Tach kho cho sp km
										totalAvailableQuantity = 0;
										StockTotalFilter stockTotalFilter = new StockTotalFilter();
										stockTotalFilter.setProductId(promotionProductZV21.getPromoProduct().getId());
										stockTotalFilter.setOwnerType(StockObjectType.SHOP);
										stockTotalFilter.setOwnerId(shopId);
										if (calculatePromotionFilter.getIsDividualWarehouse() == null || calculatePromotionFilter.getIsDividualWarehouse()) {
											stockTotalFilter.setWarehouseType(WarehouseType.PROMOTION);
										} else {
											stockTotalFilter.setOrderByType(WarehouseType.PROMOTION);
										}
										List<StockTotal> lst = stockTotalDAO.getListStockTotalByProductAndOwner(stockTotalFilter);
										if (lst != null && lst.size() > 0) {
											lstLot = new ArrayList<SaleOrderLot>();
											totalAvailableQuantity = promotionProductZV21.getSaleOrderDetail().getQuantity();
											for (StockTotal stt : lst) {
												SaleOrderLot lot = new SaleOrderLot();
												lot.setProduct(promotionProductZV21.getSaleOrderDetail().getProduct());
												lot.setWarehouse(stt.getWarehouse());
												lot.setStockTotal(stt);
												if (stt.getAvailableQuantity() <= 0) {
													continue;
												} else if (promotionProductZV21.getPromoProduct() != null && 
														totalAvailableQuantity > stt.getAvailableQuantity()) {
													lot.setQuantity(stt.getAvailableQuantity());
													totalAvailableQuantity = totalAvailableQuantity - stt.getAvailableQuantity();
													lstLot.add(lot);
												} else if (promotionProductZV21.getPromoProduct() != null && 
														totalAvailableQuantity <= stt.getAvailableQuantity()) {
													lot.setQuantity(totalAvailableQuantity);
													totalAvailableQuantity = 0;
													lstLot.add(lot);
													break;
												}
											}
											if (totalAvailableQuantity > 0) {
												if (lstLot.size() > 0) {
													lstLot.get(lstLot.size()-1).setQuantity(lstLot.get(lstLot.size()-1).getQuantity()+totalAvailableQuantity);
												} else {
													StockTotal stt = lst.get(0);
													SaleOrderLot lot = new SaleOrderLot();
													lot.setProduct(promotionProductZV21.getSaleOrderDetail().getProduct());
													lot.setWarehouse(stt.getWarehouse());
													lot.setStockTotal(stt);
													lot.setQuantity(totalAvailableQuantity);
													/*
													 * Truong hop sau khi tach kho ma so luong ton kho < so luong KM thi 
													 * lay gia tri ton kho lam so luong KM duoc huong
													 * @Modified by NhanLT6 - 19/11/2014
													 * */
//													lot.setQuantity(stt.getAvailableQuantity());
													totalAvailableQuantity = 0;
													lstLot.add(lot);
												}
											}
										promotionProductZV21.setSaleOrderLot(lstLot);
									}
								} 
							}
						} 
					}
					promotion.setListSaleOrderPromotionForOrder(lstSalesOrderDetailForOrder);
				}
			}
			
			//Xoa sp KM phan tram
			listProductSalePromotion.removeAll(listPercentPromotion);
			
			//Ham kiem tra so suat, so luong, so tien
			//orderDTO.checkQuantityReceivedFull();
			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		result.setSortListOutPut(sortListOutPut);
		result.setListProductPromotionSale(sortListOutPut.get(1L));
		if (sortListOutPut.get(100L) != null && sortListOutPut.get(100L).size() > 0) {
			result.setListQuantityReceivedOrder(sortListOutPut.get(100L).get(0).getPromotionQuantityReceivedList());
		}
		//		List<SaleOrderDetailVO> lstTmp = new ArrayList<SaleOrderDetailVO>();
//		for (SaleOrderDetailVO vvv : sortlistProductSaleByCode.values()) {
//			lstTmp.add(vvv);
//		}
		result.setListSaleProductReceivePromo(lstSalesOrderDetail);

		result.orderQuantityReceivedList = orderVO.orderQuantityReceivedList;
		result.productQuantityReceivedList = orderVO.productQuantityReceivedList;
		
		return result;
	}
	
	
	public Integer checkPromotionHasQuantityReceiveOrNot(Long promotionProgramId, Long shopId, Long customerId, 
			Long staffId, Date orderDate) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * ");
		sql.append(" FROM promotion_shop_map psm ");
		sql.append(" WHERE psm.promotion_program_id = ? and status = 1 ");
		params.add(promotionProgramId);
		sql.append(" AND (psm.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id)  ) ");
		sql.append(" AND psm.from_date < TRUNC(?) + 1 ");
		sql.append(" AND (psm.to_date    >= TRUNC(?) or psm.to_date is null) ");
		params.add(shopId);
		params.add(orderDate);
		params.add(orderDate);
		sql.append(" AND ( quantity_max IS NOT NULL ");
		sql.append(" OR ");
		sql.append(" ( ");
		sql.append(" exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status  = 1 ");
		sql.append(" and customer_id = ? and quantity_max is NOT null ");
		params.add(customerId);
		sql.append(" ) ");
		sql.append(" ) ");
		sql.append(" OR ( ");
		sql.append(" exists (select 1 from promotion_staff_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and staff_id = ? ");
		params.add(staffId);
		sql.append(" and quantity_max is not null ");
		sql.append(" ) ");
		sql.append(" ) ); ");
		return repo.countBySQL(sql.toString(), params);
	}
	
	private SortedMap<Long, SaleOrderDetailVO> cloneSortMap(SortedMap<Long, SaleOrderDetailVO> sortOld) {
		SortedMap<Long, SaleOrderDetailVO> sortNew = new TreeMap<Long, SaleOrderDetailVO>();
		for (Entry<Long, SaleOrderDetailVO> entry : sortOld.entrySet()) {
			sortNew.put(entry.getKey(), SerializationUtils.clone(entry.getValue()));
		}
		return sortNew;
	}
	/**
	 * Tinh KM cho don hang
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:  
	 * @param calcPromotionFilter
	 * @updated : 01/10/2014
	 */
	@Override
	public SaleOrderDetailVO calcPromotion(CalcPromotionFilter calcPromotionFilter) throws DataAccessException  {
		BigDecimal amtOrder = calcPromotionFilter.getAmtOrder();
		List<PromotionProgram> lstPromotionProEntity = calcPromotionFilter.getLstPromotionProEntity();
		SortedMap<Long, SaleOrderDetailVO> sortListProductSales = calcPromotionFilter.getSortListProductSales();
		List<SaleOrderDetailVO> listProductPromotionsale = calcPromotionFilter.getListProductPromotionsale();
		Long keyList = calcPromotionFilter.getKeyList();
		SortedMap<Long, List<SaleOrderDetailVO>> sortListOutPut = calcPromotionFilter.getSortListOutPut();
		Long shopId = calcPromotionFilter.getShopId();
		int promotionType = calcPromotionFilter.getPromotionType();
		SaleOrderVO orderVO = calcPromotionFilter.getOrderVO();
		Long customerId = calcPromotionFilter.getCustomerId();
		Long staffId = calcPromotionFilter.getStaffId();
		Date orderDate = calcPromotionFilter.getOrderDate();
		Map<Long, List<Long>> mapCTKMProduct = calcPromotionFilter.getMapCTKMProduct();
		SaleOrderDetailVO vo = new SaleOrderDetailVO();
		try {
//			int checkQuantityReceived = this.checkPromotionHasQuantityReceiveOrNot(promotionProEntity.getId(), shopId, 
//									customerId, staffId, orderDate);
			Customer customer = customerDAO.getCustomerById(customerId);
			Long customerTypeId = null;
			if (customer.getChannelType() != null) {
				customerTypeId = customer.getChannelType().getId();
			}
			List<Long> calculatedKeyList = new ArrayList<Long>();
			for (PromotionProgram promotionProEntity:lstPromotionProEntity) {
				//TODO: Lay gia tri so suat, so luong, so tien trong db so sanh
				SaleOrderPromotionRemainVO<Integer> quantityRemainVO = this.getQuantityRemainOfPromotionProgramAndType(promotionProEntity.getId(), shopId, staffId, customerId, orderDate);
				Integer quantityRemain = quantityRemainVO != null ? quantityRemainVO.getRemain() : null; 
				SaleOrderPromotionRemainVO<Integer> numRemainVO = this.getNumRemainOfPromotionProgramAndType(promotionProEntity.getPromotionProgramCode(), shopId, staffId, customerId, orderDate);
				Integer numRemain = numRemainVO != null ? numRemainVO.getRemain() : null; 
				SaleOrderPromotionRemainVO<BigDecimal> amountRemainVO = this.getAmountRemainOfPromotionProgramAndType(promotionProEntity.getPromotionProgramCode(), shopId, staffId, customerId, orderDate);
				BigDecimal amountRemain = amountRemainVO != null ? amountRemainVO.getRemain() : null;
				/** Lay ds product group order theo thu tu uu tien */
				SortedMap<Long, SaleOrderDetailVO> sortListProductSale = this.cloneSortMap(sortListProductSales);
				List<ProductGroup> productGroupList = this.getListProductGroupByPromotionIdJoinLevel(promotionProEntity.getId(), ProductGroupType.MUA,promotionType);
				
				//Danh sach cac san pham tham gia 1 CTKM
				boolean hasPromotionGroup = false;
				for (ProductGroup group : productGroupList) {
					int totalQuantity = 0;
					BigDecimal totalAmount = BigDecimal.ZERO;
					//int totalQuantityFix = 0;
					BigDecimal totalAmountFix = BigDecimal.ZERO;
					BigDecimal totalAmountOrder = amtOrder;
					boolean hasPromotionLevel = false;
					
					//List<GroupLevelVO> groupLevelList = this.getGroupLevelVOOfProductGroup(group.getId(),promotionType);
					List<GroupLevelVO> groupLevelList = this.getChildrenGroupLevelVOOfProductGroup(group.getId(), promotionType);
					
					//Ds cac san pham cua 1 group
	//				List<Long> listIdProductOfGroup = new ArrayList<Long>();
					
					//Chon 1 muc bat ki de tim ra cac san pham ban cua cung 1 nhom duoc KM
					GroupLevelVO groupLevelItem = groupLevelList.get(0);
					
					/**
					 * @param hasProduct
					 * 0: KM tinh tren don hang, 
					 * 1: KM tinh tren san pham
					 */
					if(groupLevelItem.getHasProduct() == SaleOrderDetailVO.ownerTypePromoProduct) {
						
						List<GroupLevelDetail> lst = this.getListGroupLevelDetailByLevelId(groupLevelItem.getId()); 
						//Tinh tong tien, tong so luong tham gia group
						for(GroupLevelDetail levelDetailTmp : lst) {
							//Tim xem co mua san pham nhu yeu cau khong?
							Long key = Long.valueOf(levelDetailTmp.getProduct().getId());
							
							SaleOrderDetailVO p = sortListProductSale.get(key);
							
							if (p != null) {
								totalQuantity += p.getSaleOrderDetail().getQuantity();
								totalAmount = totalAmount.add(p.getSaleOrderDetail().getAmount());
								
								//De tinh %, tien tren sp ban
								//totalQuantityFix += p.getSaleOrderDetail().getQuantity();
								totalAmountFix = totalAmountFix.add(p.getSaleOrderDetail().getAmount());
								
								// Them vao de tinh chia deu tien, % cho cac sp thuoc 1 nhom
	//							listIdProductOfGroup.add(key);
								
								//San pham thuoc group -> thuoc 1 CTKM
								calculatedKeyList.add(key);
								//Neu khong tack lo, tack kho thi dung break
	//							continue;
							}
						}
					} else {
						//Tinh ton tien, tong so luong cua ca don hang
						totalAmount = totalAmountOrder;
						totalAmountFix = totalAmountOrder;
						
	//					for (SaleOrderDetailVO product : sortListProductSale.values()) {
	//						listIdProductOfGroup.add(Long.valueOf(product.getSaleOrderDetail().getProduct().getId()));
	//					}
					}
					if(group.getOrder() != null && group.getOrder() > 0 && hasPromotionGroup) {
						continue;
					}
					
					//Dat dieu kien mua so luong/so tien thoi thieu cua nhom thi moi duoc tinh KM tren nhom
					if((group.getMinQuantity() != null && group.getMinQuantity() > 0 && totalQuantity >= group.getMinQuantity()) 
							|| (group.getMinAmount() != null && group.getMinAmount().signum() > 0/*group.getMinAmount().compareTo(BigDecimal.ZERO) > 0*/ && totalAmount.compareTo(group.getMinAmount()) >= 0) 
							|| (group.getMinQuantity() != null && group.getMinQuantity() == 0 && group.getMinAmount() != null && BigDecimal.ZERO.equals(group.getMinAmount())/*group.getMinAmount().compareTo(BigDecimal.ZERO) == 0*/)
						) {
						
						//Duyet danh sach cac muc
						for (GroupLevelVO level : groupLevelList) {
							//KM dang bundle
							boolean isBundle = true;
							BigDecimal totalAmountFixBundle = BigDecimal.ZERO;
							ArrayList<GroupLevelDetail> listBuyProduct = new ArrayList<GroupLevelDetail>();
							
							//Dat dieu kien mua so luong/so tien thoi thieu cua muc thi moi duoc tinh tren muc
							if((level.getMinQuantity() != null && level.getMinQuantity() > 0 && totalQuantity >= level.getMinQuantity()) 
									|| (level.getMinAmount() != null && level.getMinAmount().signum() > 0/*level.getMinAmount().compareTo(BigDecimal.ZERO) > 0*/ && totalAmount.compareTo(level.getMinAmount()) >= 0) 
									|| (level.getMinQuantity() != null && level.getMinQuantity() == 0 && level.getMinAmount() != null && BigDecimal.ZERO.equals(level.getMinAmount())/*level.getMinAmount().compareTo(BigDecimal.ZERO) == 0*/)
								) {
								
								//Duoc huong KM hay ko?
								boolean checkPro = true;
								keyList++;
								
								List<GroupLevelDetail> lstTmp = this.getListGroupLevelDetailByLevelId(level.getId());
								if (lstTmp != null && lstTmp.size() == 0) {
									isBundle = false;
								}
								//Ds san pham cua tung muc
								for (GroupLevelDetail levelDetailTmp : lstTmp) {
									
									//Tim xem co mua san pham nhu yeu cau khong?
									Long key = levelDetailTmp.getProduct().getId();
									SaleOrderDetailVO p = sortListProductSale.get(key);
									
									//Yeu cau phai mua nhung khong mua thi khong duoc KM
									if(levelDetailTmp.getIsRequired() == 1 && (p == null || p.getSaleOrderDetail().getQuantity() == 0)) {
										checkPro = false;
										break;
									}
									
									//Yeu cau mua voi so luong, so tien ma khong mua du thi khong duoc KM
									if(levelDetailTmp.getIsRequired() == 1 && levelDetailTmp.getValue() != null && levelDetailTmp.getValue().compareTo(BigDecimal.ZERO) > 0) {
										long value = 0;
										if(ValueType.QUANTITY.equals(levelDetailTmp.getValueType())) { //levelDetailTmp.getValueType() == ValueType.QUANTITY
											value = p.getSaleOrderDetail().getQuantity();
										
										} else if(ValueType.AMOUNT.equals(levelDetailTmp.getValueType())) { //levelDetailTmp.getValueType() == ValueType.AMOUNT
											value = p.getSaleOrderDetail().getAmount().longValue();
										}
										
										if(levelDetailTmp.getValue() != null && value < levelDetailTmp.getValue().longValue()) {
											checkPro = false;
											break;
										}
									} else {
										//Co 1 sp ko bat buoc thi la ko phai la KM Bundle
										isBundle = false;
									}
								}
								
								int n = -1;
								
								//Tinh KM neu thoa man dk KM
								if(checkPro) {
									/**
									 * Tim ra boi so -> lay boi so nho nhat 
									 * 	cua tung san pham lam boi so chung cho muc
									 */								
									boolean hasRequireValue = false;
									for (GroupLevelDetail levelDetailTmp2 : lstTmp) {
										//Tim xem co mua san pham nhu yeu cau khong?
										Long key = levelDetailTmp2.getProduct().getId();
										SaleOrderDetailVO p = sortListProductSale.get(key);
										
										//San pham yeu cau ban thi phai thoa man so luong/ so tien qui dinh
										if (p != null) {
											long buyValue = 0;
											BigDecimal requireValue = BigDecimal.ZERO;
											if (ValueType.QUANTITY.equals(levelDetailTmp2.getValueType())) { //levelDetailTmp2.getValueType() == ValueType.QUANTITY
												buyValue = p.getSaleOrderDetail().getQuantity();
											
											} else if (ValueType.AMOUNT.equals(levelDetailTmp2.getValueType())) { //levelDetailTmp2.getValueType() == ValueType.AMOUNT
												buyValue = p.getSaleOrderDetail().getAmount().longValue();
											}
											
											if (levelDetailTmp2.getValue() != null) {
												requireValue = levelDetailTmp2.getValue();
											}
											
											//Lay boi so nho nhat cua tung san pham
											if(levelDetailTmp2.getIsRequired() == 1 && requireValue.signum() > 0/*requireValue.compareTo(BigDecimal.ZERO) > 0*/) {
												if (n > buyValue / requireValue.intValue() || n < 0) {
													n = (int) (buyValue / requireValue.intValue());
												}
												//Co yeu cau mua voi so luong bao nhieu
												hasRequireValue = true;
											}
										}
									}
									
									/**
									 * Co tinh boi so tren muc sau khi tinh boi so tren tung san pham
									 * Kiem tra xem KM theo so luong hay so tien 
									**/
									long buyValue = 0;
									long requireValue = 0;
									if(level.getMinQuantity() != null && level.getMinQuantity() > 0) {
										buyValue = totalQuantity;
										requireValue = level.getMinQuantity();
									
									} else if (level.getMinAmount() != null && level.getMinAmount().signum() > 0/*level.getMinAmount().compareTo(BigDecimal.ZERO) > 0*/){
										buyValue = totalAmount.longValue();
										requireValue = level.getMinAmount().longValue();
									}
									
									if(requireValue > 0) {
										if (n > buyValue / requireValue || n < 0 || !hasRequireValue) {
											n = (int) (buyValue / requireValue);
										}
									}
									
									// Khi ko luy ke
									if (group.getMultiple() != null && group.getMultiple() == 0) {
										n = 1;
									}
									
									// TODO: vuongmq; 08/01/2015 lay dc n; kiem tra so suat
									// lay so suat con lai trong shop_map...: n1, so suat max: m
									// if (m != null and n > n1) then n = n1;
									int quantityMax = n;
									Integer exceedObject = null; 
									Integer exceedUnit = null;
									Integer flagPromoQuantity = null;
									if (quantityRemain != null && n > quantityRemain) {
										n = quantityRemain;
										exceedUnit = Constant.SALE_ORDER_PROMOTION_UNIT_QUANTITY;
										exceedObject = quantityRemainVO.getTypeObject();
										if (n == 0) {
											flagPromoQuantity = Constant.DAT_KHONG_NHAN_KM;
										} else {
											flagPromoQuantity = Constant.DAT_NHAN_MOT_PHAN_KM;
										}
									}
									if (quantityRemain != null) {
										quantityRemain = quantityRemain - n;
									}
									//Ds san pham cua tung muc
									for (GroupLevelDetail levelDetailTmp3 : lstTmp) {
										//Tim xem co mua san pham nhu yeu cau khong?
										Long key = levelDetailTmp3.getProduct().getId();
										SaleOrderDetailVO product = sortListProductSale.get(key);
										
										if (product != null) {
											Integer val = product.getSaleOrderDetail().getQuantity();
											BigDecimal valAmount  = product.getSaleOrderDetail().getAmount();
											
											if(levelDetailTmp3.getIsRequired() == 1 && levelDetailTmp3.getValue() != null 
													&& levelDetailTmp3.getValue().signum() > 0/*levelDetailTmp3.getValue().compareTo(BigDecimal.ZERO) > 0*/) {
												
												BigDecimal amount = BigDecimal.ZERO;
												if (levelDetailTmp3.getValue() != null) {
													if (ValueType.QUANTITY.equals(levelDetailTmp3.getValueType())) { //levelDetailTmp3.getValueType() == ValueType.QUANTITY
														product.getSaleOrderDetail().setQuantity(val - n * levelDetailTmp3.getValue().intValue());
														
														//Cap nhat amount mua
														amount = levelDetailTmp3.getValue().multiply(product.getSaleOrderDetail().getPriceValue()).multiply(new BigDecimal(n));
														levelDetailTmp3.setValue(amount);
													
													} else if (ValueType.AMOUNT.equals(levelDetailTmp3.getValueType())) { //levelDetailTmp3.getValueType() == ValueType.AMOUNT
														amount = levelDetailTmp3.getValue().multiply(new BigDecimal(n));
														valAmount = valAmount.subtract(amount);
														product.getSaleOrderDetail().setAmount(valAmount);
														levelDetailTmp3.setValue(amount);
													}
													//Tong amount tren muc cua Bundle
													totalAmountFixBundle = totalAmountFixBundle.add(amount);
												}											
											}
											listBuyProduct.add(levelDetailTmp3);
										}
									}
									
									// Tru tren muc
									if(level.getMinQuantity() != null && level.getMinQuantity() > 0) {
										totalQuantity -= n * level.getMinQuantity();
									
									} else if (level.getMinAmount() != null && level.getMinAmount().signum() > 0/*level.getMinAmount().compareTo(BigDecimal.ZERO) > 0*/){
										totalAmount = totalAmount.subtract(level.getMinAmount().multiply(new BigDecimal(n)));
									}
									// TODO: vuongmq; 11/01/2015; luu so luong, so tien dau tien tinh KM
									BigDecimal numReceivedMax = BigDecimal.ZERO;
									BigDecimal numReceived = BigDecimal.ZERO;
									BigDecimal amountReceivedMax = BigDecimal.ZERO;
									BigDecimal amountReceived = BigDecimal.ZERO;
									//Lay ra thong tin KM ung voi group va level
									//GroupMapping mapping = this.getGroupMappingByProductGroupAndLevel(group.getId(), level.getId());
									GroupMapping mapping = this.getGroupMappingByProductGroupAndChildLevel(group.getId(), level.getId());
									//Co ton tai 1 mapping
									if(mapping != null && mapping.getPromotionGroup().getId() > 0 && mapping.getPromotionGroupLevel().getId() > 0) {
										hasPromotionGroup = true;
										hasPromotionLevel = true;
										
										ProductGroup promoGroup = mapping.getPromotionGroup();//this.getProductGroup(mapping.getPromotionGroup().getId());
										//GroupLevel promoGroupLevel = this.getGroupLevel(mapping.getPromotionGroup().getId(), mapping.getPromotionGroupLevel().getId());
										GroupLevel promoGroupLevel = this.getGroupLevelByProductGroupAndParentGroupLevel(mapping.getPromotionGroup().getId(), mapping.getPromotionGroupLevel().getId());
										if (promoGroupLevel == null) {
											continue;
										}
										//List<GroupLevelDetail> promoDetailList = this.getListGroupLevelDetailByLevelId(mapping.getPromotionGroupLevel().getId());
										List<GroupLevelDetail> promoDetailList = this.getListGroupLevelDetailByLevelId(promoGroupLevel.getId());
										
										//KM san pham
										if(promoGroupLevel.getHasProduct() != null && promoGroupLevel.getHasProduct() == 1) {
											List<SaleOrderDetailVO> requiredList = new ArrayList<SaleOrderDetailVO>();
											List<SaleOrderDetailVO> optionList = new ArrayList<SaleOrderDetailVO>();
											// TODO: vuongmq; 11/01/2015; tinh tong sl KM
											Integer tongSlgKM = 0;
											if (numRemain != null) {
												Integer minSlgDoi = 0;
												for (GroupLevelDetail promoDetail : promoDetailList) {
													Integer slgMuc = promoDetail.getValue() != null ? promoDetail.getValue().intValue() : 0;
													if (promoDetail.getIsRequired() != null && 1 == promoDetail.getIsRequired()) {
														tongSlgKM = tongSlgKM + slgMuc;
													} else if (minSlgDoi == 0 || (slgMuc != 0 && minSlgDoi > slgMuc)) {
														minSlgDoi = slgMuc;
													}
												}
												tongSlgKM = tongSlgKM + minSlgDoi;
												if (tongSlgKM * n > numRemain) {
													int nCu = n;
													n = numRemain / tongSlgKM;
													numReceived = new BigDecimal(numRemain);
													exceedUnit = Constant.SALE_ORDER_PROMOTION_UNIT_NUM;
													exceedObject = numRemainVO.getTypeObject();
													if (n == 0) {
														flagPromoQuantity = Constant.DAT_KHONG_NHAN_KM;
													} else {
														flagPromoQuantity = Constant.DAT_NHAN_MOT_PHAN_KM;
													}
													if (level.getMinQuantity() != null && level.getMinQuantity() > 0) {
														totalQuantity += (nCu - n) * level.getMinQuantity();

													} else if (level.getMinAmount() != null && level.getMinAmount().signum() > 0) {
														totalAmount = totalAmount.add(level.getMinAmount().multiply(new BigDecimal(nCu - n)));
													}
												}
												tongSlgKM = (tongSlgKM - minSlgDoi) * n;
											}
											
											for(GroupLevelDetail promoDetail : promoDetailList) {
												SaleOrderDetailVO detailView = new SaleOrderDetailVO();
												SaleOrderDetail selectedOrderDetail = new SaleOrderDetail();
												detailView.setSaleOrderDetail(selectedOrderDetail);
												detailView.setProductGroupId(promoGroup.getId());
												if (PromotionType.ZV24.getValue().equalsIgnoreCase(promotionProEntity.getType())) {
													detailView.setOpenPromo(1);
												}
												detailView.setHasPortion(promotionProEntity.getHasPortion());
												
	//											selectedOrderDetail.productId = productSalePro.orderDetailDTO.productId;
												 // mat hang khuyen mai
												//KM san pham cho don hang
												if(level.getHasProduct() == 0) {
													detailView.setPromotionType(SaleOrderDetailVO.PROMOTION_FOR_ORDER_TYPE_PRODUCT);
												}
												selectedOrderDetail.setProgrameTypeCode(promotionProEntity.getType());
												selectedOrderDetail.setIsFreeItem(1);
												selectedOrderDetail.setProgramCode(promotionProEntity.getPromotionProgramCode());
												selectedOrderDetail.setProgramType(ProgramType.AUTO_PROM);
												selectedOrderDetail.setOrderNumber(level.getOrderNumber());
												//selectedOrderDetail.setLevelPromo(promoGroupLevel.getOrder());
												selectedOrderDetail.setLevelPromo(mapping.getPromotionGroupLevel().getOrder());
												//selectedOrderDetail.setGroupLevel(promoGroupLevel);
												selectedOrderDetail.setGroupLevel(mapping.getPromotionGroupLevel());
												//selectedOrderDetail.setGroupLevelId(promoGroupLevel.getId());
												selectedOrderDetail.setGroupLevelId(mapping.getPromotionGroupLevel().getId());
												selectedOrderDetail.setProductGroup(promoGroup);
												selectedOrderDetail.setProductGroupId(promoGroup.getId());
												if(promoDetail.getValueType().equals(ValueType.QUANTITY)) {
	//												selectedOrderDetail.productId = listPromotionProgramDetail.get(j).productId;
													Price price = null;
													if (customerTypeId == null) {
														price =  priceDAO.getPriceByProductAndShopId(promoDetail.getProduct().getId(), shopId);
													} else {
														price =  priceDAO.getProductPriceForCustomer(promoDetail.getProduct().getId(), shopId, customerTypeId);
													}
													selectedOrderDetail.setPrice(price);
													if (price != null) {
														selectedOrderDetail.setPriceValue(price.getPrice());
													} else {
														selectedOrderDetail.setPriceValue(BigDecimal.ZERO);
													}
													
													selectedOrderDetail.setDiscountAmount(BigDecimal.ZERO);
													// TODO: vuongmq; 08/01/2015 
													// sl = (int)promoDetail.getValue().intValue()*n
													// soluongconlai = ..., max...
													// if sl > soluongconlai then chinh lai so suat
													// tinh lai nhung gia tri lien quan den n: totalQuantity, totalAmount, ....
													// for requiredList [optionList]
													// nhung doi tuong cung muc selectedOrderDetail.groupLevelId -> tinh lai so luong khuyen mai theo so suat moi
													
//													int slKM = (int)promoDetail.getValue().intValue() * n;
//													if (numRemain != null && slKM > numRemain) {
//														int nCu = n;
//														n = numRemain / promoDetail.getValue().intValue();
//														exceedUnit = Constant.SALE_ORDER_PROMOTION_UNIT_NUM;
//														exceedObject = numRemainVO.getTypeObject();
//														if (n == 0) {
//															flagPromoQuantity = Constant.DAT_KHONG_NHAN_KM;
//														} else {
//															flagPromoQuantity = Constant.DAT_NHAN_MOT_PHAN_KM;
//														}
//														if (level.getMinQuantity() != null && level.getMinQuantity() > 0) {
//															totalQuantity += (nCu - n) * level.getMinQuantity();
//														
//														} else if (level.getMinAmount() != null && level.getMinAmount().signum() > 0/*level.getMinAmount().compareTo(BigDecimal.ZERO) > 0*/){
//															totalAmount = totalAmount.add(level.getMinAmount().multiply(new BigDecimal(nCu - n)));
//														}
//														for (SaleOrderDetailVO detailVO : requiredList) {
//															if (detailVO.getSaleOrderDetail().getGroupLevelId().equals(selectedOrderDetail.getGroupLevelId())) {
//																int slMoi = detailVO.getSaleOrderDetail().getQuantity();
//																slMoi = (slMoi / nCu) * n;
//																detailVO.getSaleOrderDetail().setQuantity(slMoi);
//																detailVO.getSaleOrderDetail().setMaxQuantityFree(slMoi);
//															}
//														}
//														for (SaleOrderDetailVO detailVO : optionList) {
//															if (detailVO.getSaleOrderDetail().getGroupLevelId().equals(selectedOrderDetail.getGroupLevelId())) {
//																int slMoi = detailVO.getSaleOrderDetail().getQuantity();
//																slMoi = (slMoi / nCu) * n;
//																detailVO.getSaleOrderDetail().setQuantity(slMoi);
//																detailVO.getSaleOrderDetail().setMaxQuantityFree(slMoi);
//															}
//														}
//													}
													//selectedOrderDetail.setQuantity((int)promoDetail.getValue().intValue() * n);
													selectedOrderDetail.setQuantity((int)promoDetail.getValue().intValue());
													selectedOrderDetail.setMaxQuantityFree(selectedOrderDetail.getQuantity());
													selectedOrderDetail.setMaxQuantityFreeTotal((int)promoDetail.getValue().intValue() * quantityMax);
													detailView.setIsEdited(promotionProEntity.getIsEdited());
													detailView.setPromoProduct(promoDetail.getProduct());
													detailView.setType(SaleOrderDetailVO.FREE_PRODUCT);
													detailView.setTypeName("KM hàng");
													detailView.setLevelPromo(promoGroupLevel.getOrder());
													detailView.setListBuyProduct(listBuyProduct);
													
													//numReceived = numReceived.add(new BigDecimal(selectedOrderDetail.getQuantity()));
													numReceivedMax = numReceivedMax.add(new BigDecimal(selectedOrderDetail.getMaxQuantityFreeTotal()));
												}
												//Sp KM co bat buoc phai tra, sp tuy cho thi cho phep doi hang
												if(promoDetail.getIsRequired() != null && 1 == promoDetail.getIsRequired()) {
													requiredList.add(detailView);
												} else {
													optionList.add(detailView);
												}
											}
											if (!optionList.isEmpty()) {
												Integer numRemainOfPromotionProgram = this.getNumRemainOfPromotionProgram(promotionProEntity.getPromotionProgramCode(), shopId, staffId, customerId, orderDate);
												adjustPromotionProductQuantity(optionList, n, numRemainOfPromotionProgram);
													//numReceived = new BigDecimal(adjustPromotionProductQuantity(optionList, n, numRemainOfPromotionProgram));
											}
											if (!requiredList.isEmpty()) {
												Integer numRemainOfPromotionProgram = this.getNumRemainOfPromotionProgram(promotionProEntity.getPromotionProgramCode(), shopId, staffId, customerId, orderDate);
												adjustPromotionProductQuantity(requiredList, n, numRemainOfPromotionProgram);
													//numReceived = new BigDecimal(adjustPromotionProductQuantity(optionList, n, numRemainOfPromotionProgram));
											}
											
											if(optionList.size() > 0) {
												//Neu chi co 1 sp tuy chon thi coi nhu duoc huong bat buoc luon
												if(optionList.size() == 1) {
													requiredList.addAll(optionList);
													optionList.clear();
												} else {
													//Tao key list
													for(SaleOrderDetailVO optionProduct : optionList) {
														optionProduct.setChangeProduct(1);
														optionProduct.setKeyList(keyList);
													}
													//Lay 1 sp trong ds tuy chon dua vao ds KM hien thi
													/*
													 * chon san pham co stock_total.
													 * Neu nhu ko co san pham nao co stock_total thi chon 1 san pham dau tien
													 * @modified by tuannd20
													 * @date 19/11/2014
													 */
													SaleOrderDetailVO saleOrderDetailVOTmp = null;
													int idx = -1;
													int kkk = -1;
													for (SaleOrderDetailVO sodVo : optionList) {
														kkk++;
														if (saleOrderDetailVOTmp == null && numRemain != null && numRemain > tongSlgKM + sodVo.getSaleOrderDetail().getQuantity()) {
															saleOrderDetailVOTmp = sodVo;
															idx = kkk;
														}
														Long tmpProductId = sodVo.getPromoProduct() != null ? sodVo.getPromoProduct().getId() : (sodVo.getProduct() != null ? sodVo.getProduct().getId() : 0);
														StockTotalFilter stockTotalFilter = new StockTotalFilter();
														stockTotalFilter.setProductId(tmpProductId);
														stockTotalFilter.setOwnerType(StockObjectType.SHOP);
														stockTotalFilter.setOwnerId(shopId);
														if (calcPromotionFilter.getIsDividualWarehouse() == null || calcPromotionFilter.getIsDividualWarehouse()) {
															stockTotalFilter.setWarehouseType(WarehouseType.PROMOTION);
														} else {
															stockTotalFilter.setOrderByType(WarehouseType.PROMOTION);
														}
														List<StockTotal> lstProductStockTotal = stockTotalDAO.getListStockTotalByProductAndOwner(stockTotalFilter);
														if (lstProductStockTotal != null && lstProductStockTotal.size() > 0) {
															requiredList.add(sodVo);
															break;
														}
													}
													if (idx != -1 && saleOrderDetailVOTmp != null) {
														numRemain = numRemain - tongSlgKM - saleOrderDetailVOTmp.getSaleOrderDetail().getQuantity();
														optionList.remove(idx);
														optionList.add(0, saleOrderDetailVOTmp);
													}
													if (requiredList.size() == 0) {
														requiredList.add(optionList.get(0));
													}
												}
											}
											
											//KM san pham ZV01 - ZV18
											if(level.getHasProduct() == 1) {
												listProductPromotionsale.addAll(requiredList);
												sortListOutPut.put(keyList, optionList);
											
											} else {
												//KM don hang ZV19 - 21
												SaleOrderDetailVO detailView = null;
												if(requiredList.size() > 0) {
													SaleOrderDetail productSaleDTO = new SaleOrderDetail();
													detailView = new SaleOrderDetailVO();
													detailView.setPromotionType(SaleOrderDetailVO.PROMOTION_FOR_ZV21);
													detailView.setSaleOrderDetail(productSaleDTO);
													detailView.setProductGroupId(promoGroup.getId());
													if (PromotionType.ZV24.getValue().equalsIgnoreCase(promotionProEntity.getType())) {
														detailView.setOpenPromo(1);
													}
													detailView.setHasPortion(promotionProEntity.getHasPortion());
	
													productSaleDTO.setProgramCode(promotionProEntity.getPromotionProgramCode());
													productSaleDTO.setProgramType(ProgramType.AUTO_PROM);
													productSaleDTO.setProgrameTypeCode("ZV21");
													productSaleDTO.setIsFreeItem(1); // mat hang khuyen mai
													productSaleDTO.setPriceValue(BigDecimal.ZERO);
													productSaleDTO.setDiscountAmount(BigDecimal.ZERO);// amountPromo = 0;
													productSaleDTO.setMaxQuantityFree(productSaleDTO.getQuantity());
													productSaleDTO.setMaxQuantityFreeTotal(productSaleDTO.getMaxQuantityFreeTotal()); /** vuongmq; 08/01/2015; hien thi maxQuantityFreeTotal so luong tinh khuyen mai*/
													//productSaleDTO.setLevelPromo(promoGroupLevel.getOrder());
													productSaleDTO.setLevelPromo(mapping.getPromotionGroupLevel().getOrder());
													//productSaleDTO.setGroupLevel(promoGroupLevel);
													productSaleDTO.setGroupLevel(mapping.getPromotionGroupLevel());
													//productSaleDTO.setGroupLevelId(promoGroupLevel.getId());
													productSaleDTO.setGroupLevelId(mapping.getPromotionGroupLevel().getId());
													productSaleDTO.setProductGroup(promoGroup);
													productSaleDTO.setProductGroupId(promoGroup.getId());
													detailView.setIsEdited(promotionProEntity.getIsEdited());
													detailView.setType(SaleOrderDetailVO.FREE_PRODUCT);
													detailView.setTypeName("KM hàng");
													detailView.setListPromotionForPromo21(requiredList);
													
	//												if (promotionDescription.length() > 0){
	//													detailView.promotionDescription = String.valueOf(promotionDescription.toString());
	//												}
													listProductPromotionsale.add(detailView);
													sortListOutPut.put(keyList, optionList);
												}
											}
										//KM tien, %
										} else {
											SaleOrderDetailVO detailView = new SaleOrderDetailVO();
											SaleOrderDetail selectedOrderDetail = new SaleOrderDetail();
											detailView.setSaleOrderDetail(selectedOrderDetail);
											detailView.setProductGroupId(promoGroup.getId());
											if (PromotionType.ZV24.getValue().equalsIgnoreCase(promotionProEntity.getType())) {
												detailView.setOpenPromo(1);
											}
											detailView.setHasPortion(promotionProEntity.getHasPortion());
											 // mat hang khuyen mai
											if(level.getHasProduct() == 0) {
												detailView.setPromotionType(SaleOrderDetailVO.PROMOTION_FOR_ORDER);
											}
											selectedOrderDetail.setIsFreeItem(1);
											selectedOrderDetail.setProgramCode(promotionProEntity.getPromotionProgramCode());
											selectedOrderDetail.setProgramType(ProgramType.AUTO_PROM);
											selectedOrderDetail.setProgrameTypeCode(promotionProEntity.getType());
											//selectedOrderDetail.setLevelPromo(promoGroupLevel.getOrder());
											selectedOrderDetail.setLevelPromo(mapping.getPromotionGroupLevel().getOrder());
											//selectedOrderDetail.setGroupLevel(promoGroupLevel);
											selectedOrderDetail.setGroupLevel(mapping.getPromotionGroupLevel());
											//selectedOrderDetail.setGroupLevelId(promoGroupLevel.getId());
											selectedOrderDetail.setGroupLevelId(mapping.getPromotionGroupLevel().getId());
											selectedOrderDetail.setProductGroup(promoGroup);
											selectedOrderDetail.setProductGroupId(promoGroup.getId());
											detailView.setNumBuyProductInGroup(lstTmp.size());
											detailView.setIsEdited(promotionProEntity.getIsEdited());
											detailView.setListBuyProduct(listBuyProduct);
											detailView.setIsBundle(isBundle);
											
											//KM tien
											if (promoGroupLevel.getMaxAmount() != null && promoGroupLevel.getMaxAmount().signum() > 0/*promoGroupLevel.getMaxAmount().compareTo(BigDecimal.ZERO) > 0*/) {
												// TODO: vuongmq; 08/01/2015 
												// sotien = promoGroupLevel.getMaxAmount().multiply(new BigDecimal(n))
												// sotienconlai = ...
												// if sotien > sotienconlai then chinh lai so suat
												// tinh lai nhung gia tri lien quan den n: totalQuantity, totalAmount, ....
												// for listProductPromotionsale(detailView)
												// if detailView.getType == SaleOrderDetailVO.FREE_PRICE
												// nhung doi tuong cung muc selectedOrderDetail.groupLevelId -> tinh lai so tien khuyen mai theo so suat moi
												
												BigDecimal discountAmount = promoGroupLevel.getMaxAmount().multiply(new BigDecimal(n));
												if (amountRemain != null && discountAmount.compareTo(amountRemain) > 0) {
													BigDecimal nCu = new BigDecimal(n);
													n = amountRemain.divide(promoGroupLevel.getMaxAmount(), RoundingMode.DOWN).intValue();
													exceedUnit = Constant.SALE_ORDER_PROMOTION_UNIT_AMOUNT;
													exceedObject = amountRemainVO.getTypeObject();
													if (n == 0) {
														flagPromoQuantity = Constant.DAT_KHONG_NHAN_KM;
													} else {
														flagPromoQuantity = Constant.DAT_NHAN_MOT_PHAN_KM;
													}
													if (level.getMinQuantity() != null && level.getMinQuantity() > 0) {
														totalQuantity += (nCu.intValue() - n) * level.getMinQuantity();
													
													} else if (level.getMinAmount() != null && level.getMinAmount().signum() > 0/*level.getMinAmount().compareTo(BigDecimal.ZERO) > 0*/){
														totalAmount = totalAmount.add(level.getMinAmount().multiply(new BigDecimal(nCu.intValue() - n)));
													}
													for (SaleOrderDetailVO detailVO : listProductPromotionsale) {
														if (detailVO.getSaleOrderDetail().getGroupLevelId().equals(selectedOrderDetail.getGroupLevelId())) {
															BigDecimal discount = detailVO.getSaleOrderDetail().getDiscountAmount();
															discount = (discount.divide(nCu, RoundingMode.DOWN)).multiply(new BigDecimal(n));
															detailVO.getSaleOrderDetail().setDiscountAmount(discount);
															detailVO.getSaleOrderDetail().setMaxAmountFree(discount);
														}
													}
												}
												
												selectedOrderDetail.setDiscountAmount(promoGroupLevel.getMaxAmount().multiply(new BigDecimal(n)));
												selectedOrderDetail.setMaxAmountFree(selectedOrderDetail.getDiscountAmount());
												
												amountReceived = amountReceived.add(selectedOrderDetail.getDiscountAmount());
												amountReceivedMax = amountReceivedMax.add(promoGroupLevel.getMaxAmount().multiply(new BigDecimal(quantityMax)));
												BigDecimal totalAmountBuy = BigDecimal.ZERO;
												if (isBundle) {
													totalAmountBuy = totalAmountFixBundle;
												} else {
													totalAmountBuy = totalAmountFix;
												}
												
												if (totalAmountBuy.equals(BigDecimal.ZERO)) {
													selectedOrderDetail.setDiscountPercent(0.0f); // tulv2 truong hop khuyen mai tay khong nhat thiet phai co san pham ban
												} else {
													selectedOrderDetail.setDiscountPercent((float)(Math.round(selectedOrderDetail.getDiscountAmount()
															.multiply(new BigDecimal(PromotionCalc.ROUND_NO_PERCENT))
															.divide(totalAmountBuy,2, RoundingMode.HALF_UP).doubleValue())/ (double)PromotionCalc.ROUND_PERCENT));
												}
												selectedOrderDetail.setQuantity(0);
												detailView.setType(SaleOrderDetailVO.FREE_PRICE);
												detailView.setTypeName("KM tiền");
											
											} else if (promoGroupLevel.getPercent() > 0) {
												selectedOrderDetail.setDiscountPercent(promoGroupLevel.getPercent());											
												BigDecimal totalDiscount = BigDecimal.ZERO;
												BigDecimal sochia = BigDecimal.ZERO;
												BigDecimal sobichia = BigDecimal.ZERO;
												if(isBundle) {
													BigDecimal total = BigDecimal.ZERO;
													for(GroupLevelDetail levelDetail: listBuyProduct) {
														sochia = levelDetail.getValue().multiply(new BigDecimal(selectedOrderDetail.getDiscountPercent() * PromotionCalc.ROUND_PERCENT));
														sobichia = new BigDecimal(PromotionCalc.ROUND_NO_PERCENT);
														//total = new BigDecimal(Math.round(sochia.divide(sobichia).doubleValue()));
														total = sochia.divide(sobichia, 0, RoundingMode.DOWN);
														totalDiscount = totalDiscount.add(total);
													}
												} else {
													sochia = totalAmountFix.multiply( new BigDecimal(selectedOrderDetail.getDiscountPercent() * PromotionCalc.ROUND_PERCENT));
													sobichia = new BigDecimal(PromotionCalc.ROUND_NO_PERCENT);
													//totalDiscount = new BigDecimal(Math.round(sochia.divide(sobichia).doubleValue()));
													totalDiscount = sochia.divide(sobichia, 0, RoundingMode.DOWN);
												}
												// TODO: vuongmq; 08/01/2015 
												// max.. = totalDiscount
												// totalDiscount > so tien con lai -> totalDiscount = gan bang so tien con lai
												amountReceivedMax = amountReceivedMax.add(totalDiscount); // gia tri tien lan dau tien tinh 
												if (amountRemain != null && totalDiscount.compareTo(amountRemain) > 0) {
													totalDiscount = amountRemain;
													exceedUnit = Constant.SALE_ORDER_PROMOTION_UNIT_AMOUNT;
													exceedObject = amountRemainVO.getTypeObject();
													flagPromoQuantity = Constant.DAT_NHAN_MOT_PHAN_KM;
												}
												selectedOrderDetail.setDiscountAmount(totalDiscount);
												selectedOrderDetail.setMaxAmountFree(selectedOrderDetail.getDiscountAmount()); 
												amountReceived = amountReceived.add(selectedOrderDetail.getDiscountAmount());
												
												detailView.setType(SaleOrderDetailVO.FREE_PERCENT);
												detailView.setTypeName("KM %");
											}
	//										detailView.setListIdProductOfGroup(listIdProductOfGroup);
											
											listProductPromotionsale.add(detailView);
										}
										
										//Tinh so suat
										List<SaleOrderPromotion> quantityReceivedList;
										if(level.getHasProduct() == SaleOrderDetailVO.ownerTypePromoProduct) {
											quantityReceivedList = orderVO.productQuantityReceivedList;
										} else {
											quantityReceivedList = orderVO.orderQuantityReceivedList;
										}
										boolean isExist = false;
										if (quantityReceivedList.size() > 0) {
											for(SaleOrderPromotion orderPromotion: quantityReceivedList) {
												if(orderPromotion.getPromotionProgram() != null
														&& orderPromotion.getPromotionProgram().getId().equals(promotionProEntity.getId()) 
														/*&& orderPromotion.getProductGroup() != null && mapping.getPromotionGroup() != null
														&& orderPromotion.getProductGroup().getId().equals(mapping.getPromotionGroup().getId())*/
														&& orderPromotion.getProductGroupId() != null && mapping.getPromotionGroup() != null
														&& orderPromotion.getProductGroupId().equals(mapping.getPromotionGroup().getId())
														/*&& orderPromotion.getGroupLevel() != null && mapping.getPromotionGroupLevel() != null
														&& orderPromotion.getGroupLevel().getId().equals(mapping.getPromotionGroupLevel().getId())*/
														&& orderPromotion.getGroupLevelId() != null && mapping.getPromotionGroupLevel() != null
														&& orderPromotion.getGroupLevelId().equals(mapping.getPromotionGroupLevel().getId())
														
														//&& orderPromotion.getGroupLevel().getId().equals(promoGroupLevel.getId())
														) {
													isExist = true;
	//												if (promotionProEntity.getIsEdited().equals(SaleOrderDetailVO.PORTION_EDIT) 
	//														|| (promotionProEntity.getIsEdited().equals(SaleOrderDetailVO.PORTION_AND_QUANTITY_NOT_EDIT)
	//															&& checkQuantityReceived > 0)) {
														orderPromotion.setQuantityReceived(orderPromotion.getQuantityReceived()+n);
	//												}
													break;
												}
											}
										} 
										if (!isExist) {
											SaleOrderPromotion saleOrderPromotion = new SaleOrderPromotion();
											saleOrderPromotion.setPromotionProgram(promotionProEntity);
											saleOrderPromotion.setPromotionCode(promotionProEntity.getPromotionProgramCode());
											saleOrderPromotion.setPromotionName(promotionProEntity.getPromotionProgramName());
											saleOrderPromotion.setProductGroup(mapping.getPromotionGroup());
											if (mapping.getPromotionGroup() != null) {
												saleOrderPromotion.setProductGroupId(mapping.getPromotionGroup().getId());
											}
											saleOrderPromotion.setGroupLevel(mapping.getPromotionGroupLevel());
											if (mapping.getPromotionGroupLevel() != null) {
												saleOrderPromotion.setGroupLevelId(mapping.getPromotionGroupLevel().getId());
												saleOrderPromotion.setPromotionLevel(mapping.getPromotionGroupLevel().getOrder());	// dev
											}
	
											//saleOrderPromotion.setGroupLevel(promoGroupLevel);
											//saleOrderPromotion.setPromotionLevel(promoGroupLevel.getOrder());
											
											/*
											 * Kiem tra neu CTKM duoc khai bao cho phep sua doi so suat KM 
											 * hoac khong duoc sua so suat KM va ca so luong KM nhung co ap dung so suat KM ( checkQuantityReceived > 0 )
											 * thi moi luu thong tin so suat KM vao sale order promotion
											 * */
	//										if (promotionProEntity.getIsEdited().equals(SaleOrderDetailVO.PORTION_EDIT) 
	//												|| (promotionProEntity.getIsEdited().equals(SaleOrderDetailVO.PORTION_AND_QUANTITY_NOT_EDIT)
	//													&& checkQuantityReceived > 0)) {
												saleOrderPromotion.setQuantityReceived(n);
												saleOrderPromotion.setMaxQuantityReceived(n);
												// TODO: Vuongmq 11/01/2015; luu so suat, so luong, so tien; lan dau tinh KM
												saleOrderPromotion.setMaxQuantityReceivedTotal(quantityMax);
												saleOrderPromotion.setMaxNumReceivedTotal(numReceivedMax);
												saleOrderPromotion.setMaxAmountReceivedTotal(amountReceivedMax);
												//TODO: Vuongmq 11/01/2015; luu so suat, so luong, so tien; sau khi tinh KM
												saleOrderPromotion.setNumReceived(numReceived);
												saleOrderPromotion.setMaxNumReceived(numReceived);
												saleOrderPromotion.setAmountReceived(amountReceived);
												saleOrderPromotion.setMaxAmountReceived(amountReceived);
												//TODO: Vuongmq 11/01/2015; luu cac gia tri thay doi
												saleOrderPromotion.setExceedObject(exceedObject);
												saleOrderPromotion.setExceedUnit(exceedUnit);
												saleOrderPromotion.setFlagPromoQuantity(flagPromoQuantity);
	//										}
											if (SaleOrderDetailVO.ownerTypePromoOrder == level.getHasProduct()) {
													saleOrderPromotion.setOrderPromo(1);
											}
											quantityReceivedList.add(saleOrderPromotion);
										}
									}
								} // end checkPro
							}
							
							//Khong toi uu
							if(group.getRecursive() != null && group.getRecursive().equals(0) && hasPromotionLevel) {
								break;
							}
						}
					}
				}
				//Neu group co thu tu va duoc huong KM thi dung lai
				/*if(group.getOrder() != null && group.getOrder() > 0 && hasPromotion) {
					break;
				}*/
				
				//Luu lai cac sp da tinh km theo tung CTKM
				if (mapCTKMProduct != null) {
					mapCTKMProduct.put(promotionProEntity.getId(), calculatedKeyList);
				}
				//Loai bo cac sp da tinh KM trong cac nhom
				for(Long key : calculatedKeyList) {
					sortListProductSale.remove(key);
				}
			}
//			vo.setListPromotionForPromo21(listProductPromotionsale);
			
		} catch (Exception ex) {
			LogUtility.logError(ex, ex.getMessage());
		}
		return vo;
	}
	
	private Integer adjustPromotionProductQuantity(List<SaleOrderDetailVO> promotionProducts, int receivableQuota, Integer remainReceivableQuantity) {
		Integer totalProductQuantityReceived = 0;
		for (SaleOrderDetailVO saleOrderDetailVO : promotionProducts) {
			int quota = receivableQuota;
			SaleOrderDetail saleOrderDetail = saleOrderDetailVO.getSaleOrderDetail();
			while (remainReceivableQuantity != null && quota > 0 && saleOrderDetail.getQuantity() * quota > remainReceivableQuantity) {
				quota--;
			}
			saleOrderDetail.setQuantity(saleOrderDetail.getQuantity() * quota);
			saleOrderDetail.setPromotionQuotation(quota);
			totalProductQuantityReceived += saleOrderDetail.getQuantity();
		}
		return totalProductQuantityReceived;
	}

	@Override
	public int getBasePromotiveProductQuantityForEditingSaleOrder(
			Long saleOrderId, String productCode, String programCode,
			Integer gotPromoLevel) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select gld.value as count ");
		sql.append(" from group_level gl ");
		sql.append(" join group_level_detail gld on gl.group_level_id = gld.group_level_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and gl.status = 1 ");
		sql.append(" and gl.product_group_id = ( ");
		sql.append(" 	select product_group_id ");
		sql.append(" 	from sale_order_promotion sop ");
		sql.append(" 	where 1 = 1 ");
		sql.append(" 	and sop.sale_order_id = ? ");
		params.add(saleOrderId);
		sql.append(" 	and sop.promotion_program_code = ? ");
		params.add(programCode);
		sql.append(" 	and sop.promotion_level = ? ");
		params.add(gotPromoLevel);
		sql.append(" 	and rownum = 1 ");
		sql.append(" ) ");
		sql.append(" and gl.order_number = ? ");
		params.add(gotPromoLevel);
		sql.append(" and exists (select 1 from product_group where product_group_id = gl.product_group_id and group_type = 2) ");
		sql.append(" and gld.product_id = (select product_id from product where product_code = ? and status = 1) ");
		params.add(productCode);
		sql.append(" and rownum = 1 ");
		return repo.countBySQL(sql.toString(), params);
	}
	
	@Override
	public int getBasePromotiveProductQuantityForEditingSaleOrder2(
			Long saleOrderId, String productCode, String programCode,
			Long promoLevelId, Integer gotPromoLevel) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select gld.value as count ");
		sql.append(" from group_level gl ");
		sql.append(" join group_level_detail gld on gl.group_level_id = gld.group_level_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and gl.status = 1 ");
		sql.append(" and gl.product_group_id = ( ");
		sql.append(" 	select product_group_id ");
		sql.append(" 	from sale_order_promotion sop ");
		sql.append(" 	where 1 = 1 ");
		sql.append(" 	and sop.sale_order_id = ? ");
		params.add(saleOrderId);
		sql.append(" 	and sop.promotion_program_code = ? ");
		params.add(programCode);
		/*sql.append(" 	and sop.promotion_level = ? ");
		params.add(gotPromoLevel);*/
		sql.append(" and sop.group_level_id = ?");
		params.add(promoLevelId);
		sql.append(" 	and rownum = 1 ");
		sql.append(" ) ");
		//sql.append(" and gl.order_number = ? ");
		//params.add(gotPromoLevel);
		sql.append(" and gl.parent_group_level_id = ?");
		params.add(promoLevelId);
		sql.append(" and exists (select 1 from product_group where product_group_id = gl.product_group_id and group_type = 2) ");
		sql.append(" and gld.product_id = (select product_id from product where product_code = ? and status = 1) ");
		params.add(productCode);
		sql.append(" and rownum = 1 ");
		return repo.countBySQL(sql.toString(), params);
	}

	@Override
	public List<SaleOrderDetailVOEx> getPromotiveProductForEditingSaleOrder(Long shopId, 
			Long saleOrderId, String productCode, String programCode,
			Integer gotPromoLevel) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ");
		sql.append(" 	p.product_id productId, ");
		sql.append(" 	st.warehouse_id warehouseId, ");
		sql.append(" 	st.quantity stockQuantity, ");
		if (saleOrderId == null) {
			sql.append(" 	st.available_quantity availableQuantity, ");			
		} else {
			sql.append(" 	st.available_quantity + ");
			sql.append(" 	(	select sum(quantity) ");
			sql.append(" 		from sale_order_detail ");
			sql.append(" 		where 1=1 ");
			sql.append(" 		and sale_order_id = ? ");
			params.add(saleOrderId);
			sql.append(" 		and product_id = p.product_id ");
			//sql.append(" 		and product_id = (select product_id from product where product_code = ?) ");
			//params.add(productCode);
			sql.append(" 	) ");
			sql.append(" 	as availableQuantity, ");
		}
		sql.append(" 	p.convfact convfact, ");
		sql.append(" 	p.check_lot checkLot, ");
		sql.append(" 	p.product_code productCode, ");
		sql.append(" 	p.product_name productName, ");
		sql.append(" 	gld.value quantity ");
		sql.append(" from group_level gl ");
		sql.append(" join group_level_detail gld on gl.group_level_id = gld.group_level_id ");
		//sql.append(" join stock_total st on gld.product_id = st.product_id and st.object_id = ? ");
		/*
		 * chi lay 1 kho mac dinh dau tien
		 */
		sql.append(" join	(select ");
		sql.append(" 			st2.*, ");
		sql.append(" 			row_number() over ( partition by st2.product_id  ");
		sql.append(" 								order by (select seq from warehouse where status = 1 and warehouse_id = st2.warehouse_id)) ");
		sql.append(" 			as rn ");
		sql.append(" 		from stock_total st2 ");
		sql.append(" 		where st2.status = 1 ");
		sql.append(" 		and st2.object_id = ? ");
		params.add(shopId);
		sql.append(" ) st on gld.product_id = st.product_id and st.rn = 1 ");
		
		sql.append(" join product p on st.product_id = p.product_id and p.status = 1 ");
		sql.append(" where 1 = 1 ");
		sql.append(" and gl.status = 1 and gld.status = 1 ");
		sql.append(" and gl.product_group_id = ( ");
		sql.append(" 	select product_group_id ");
		sql.append(" 	from sale_order_promotion sop ");
		sql.append(" 	where 1 = 1 ");
		sql.append(" 	and sop.sale_order_id = ? ");
		params.add(saleOrderId);
		sql.append(" 	and sop.promotion_program_code = ? ");
		params.add(programCode);
		sql.append(" 	and sop.promotion_level = ?	");
		params.add(gotPromoLevel);
		sql.append(" 	and rownum = 1 ");
		sql.append(" ) ");
		sql.append(" and gl.order_number = ? ");
		params.add(gotPromoLevel);
		sql.append(" and exists (select 1 from warehouse where warehouse_id = st.warehouse_id and status = 1) ");
		sql.append(" and exists (select 1 from product_group where product_group_id = gl.product_group_id and group_type = 2) ");
		sql.append(" order by p.product_code ");
		
		final String[] fieldNames = new String[] {
				"productId",
				"warehouseId",
				"stockQuantity",
				"availableQuantity",
				"convfact",
				"checkLot",
				"productCode",
				"productName",
				"quantity",
		};
		final Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
		};
		return repo.getListByQueryAndScalar(SaleOrderDetailVOEx.class, fieldNames, fieldTypes, sql.toString().replaceAll("  ", " "), params);
	}
	
	@Override
	public List<SaleOrderDetailVOEx> getPromotiveProductForEditingSaleOrder2(Long shopId, 
			Long saleOrderId, String productCode, String programCode,
			Long promoLevelId, Integer gotPromoLevel) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("with lstStockTotalTmp as (");
		sql.append(" select st.stock_total_id, st.warehouse_id, st.object_id, st.object_type, st.product_id,");
		sql.append(" st.quantity, st.approved_quantity");
		if (saleOrderId == null) {
			sql.append(", st.available_quantity");
		} else {
			sql.append(", (st.available_quantity + (select nvl(sum(quantity), 0) from sale_order_lot where sale_order_id = ?");
			params.add(saleOrderId);
			sql.append(" and product_id = st.product_id and warehouse_id = st.warehouse_id)) as available_quantity");
		}
		sql.append(" from stock_total st");
		sql.append(" where st.status = 1");
		sql.append(" and st.object_type = 1 and st.object_id = ?");
		params.add(shopId);
		if (saleOrderId != null) {
			sql.append(" and product_id in (select product_id from sale_order_detail where sale_order_id = ?)");
			params.add(saleOrderId);
		}
		sql.append("),");
		
		sql.append(" lstStockTotal as (");
		sql.append("select stock_total_id, warehouse_id, product_id,");
		sql.append(" quantity, available_quantity,");
		sql.append(" row_number() over (partition by st.product_id");
		sql.append(" order by (select seq from warehouse where status = 1 and warehouse_id = st.warehouse_id)) as rn");
		sql.append(" from lstStockTotalTmp st");
		sql.append(" where available_quantity > 0");//sai
		sql.append(")");
		
		sql.append(" select ");
		sql.append(" 	p.product_id productId, ");
		sql.append(" 	st.warehouse_id warehouseId, ");
		sql.append(" 	st.quantity stockQuantity, ");
		/*if (saleOrderId == null) {
			sql.append(" 	st.available_quantity availableQuantity, ");			
		} else {
			sql.append(" 	st.available_quantity + ");
			sql.append(" 	(	select nvl(sum(quantity), 0) ");
			sql.append(" 		from sale_order_lot ");
			sql.append(" 		where 1=1 ");
			sql.append(" 		and sale_order_id = ? ");
			params.add(saleOrderId);
			sql.append(" 		and product_id = p.product_id ");
			//sql.append(" 		and product_id = (select product_id from product where product_code = ?) ");
			//params.add(productCode);
			sql.append(" 	) ");
			sql.append(" 	as availableQuantity, ");
		}*/
		sql.append(" 	st.available_quantity availableQuantity, ");
		sql.append(" 	p.convfact convfact, ");
		sql.append(" 	p.check_lot checkLot, ");
		sql.append(" 	p.product_code productCode, ");
		sql.append(" 	p.product_name productName, ");
		sql.append(" 	gld.value quantity ");
		sql.append(" from group_level gl ");
		sql.append(" join group_level_detail gld on gl.group_level_id = gld.group_level_id ");
		//sql.append(" join stock_total st on gld.product_id = st.product_id and st.object_id = ? ");
		/*
		 * chi lay 1 kho mac dinh dau tien
		 */
		/*sql.append(" join	(select ");
		sql.append(" 			st2.*, ");
		sql.append(" 			row_number() over ( partition by st2.product_id  ");
		sql.append(" 								order by (select seq from warehouse where status = 1 and warehouse_id = st2.warehouse_id)) ");
		sql.append(" 			as rn ");
		sql.append(" 		from stock_total st2 ");
		sql.append(" 		where st2.status = 1 ");
		sql.append(" 		and st2.object_id = ? ");
		params.add(shopId);
		sql.append(" ) st on gld.product_id = st.product_id and st.rn = 1 ");*/
		sql.append(" left join lstStockTotal st on (st.product_id = gld.product_id and st.rn = 1");
		sql.append(" and exists (select 1 from warehouse where warehouse_id = st.warehouse_id and status = 1)");
		sql.append(")");
		
		sql.append(" join product p on (p.product_id = gld.product_id and p.status = 1)");
		sql.append(" where 1 = 1 ");
		sql.append(" and gl.status = 1 and gld.status = 1 ");
		sql.append(" and gl.product_group_id = ( ");
		sql.append(" 	select product_group_id ");
		sql.append(" 	from sale_order_promotion sop ");
		sql.append(" 	where 1 = 1 ");
		sql.append(" 	and sop.sale_order_id = ? ");
		params.add(saleOrderId);
		sql.append(" 	and sop.promotion_program_code = ? ");
		params.add(programCode);
		/*sql.append(" 	and sop.promotion_level = ?	");
		params.add(gotPromoLevel);*/
		sql.append(" and sop.group_level_id = ?");
		params.add(promoLevelId);
		sql.append(" 	and rownum = 1 ");
		sql.append(" ) ");
		//sql.append(" and gl.order_number = ? ");
		//params.add(gotPromoLevel);
		sql.append(" and gl.parent_group_level_id = ?");
		params.add(promoLevelId);
		//sql.append(" and exists (select 1 from warehouse where warehouse_id = st.warehouse_id and status = 1) ");
		sql.append(" and exists (select 1 from product_group where product_group_id = gl.product_group_id and group_type = 2) ");
		sql.append(" order by p.product_code ");
		
		final String[] fieldNames = new String[] {
				"productId",
				"warehouseId",
				"stockQuantity",
				"availableQuantity",
				"convfact",
				"checkLot",
				"productCode",
				"productName",
				"quantity",
		};
		final Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
		};
		return repo.getListByQueryAndScalar(SaleOrderDetailVOEx.class, fieldNames, fieldTypes, sql.toString().replaceAll("  ", " "), params);
	}
	
	@Override
	public List<PromotionProductOpenVO> listPromotionProductOpenVO(Long promotionId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select ppo.PROMOTION_PRODUCT_OPEN_ID id, ppo.PROMOTION_PROGRAM_ID promotionId, p.product_code productCode, p.product_name productName, ppo.QUANTITY quantity, ppo.AMOUNT amount ");
		sql.append(" from PROMOTION_PRODUCT_OPEN ppo inner join product p on ppo.product_id = p.product_id ");
		sql.append(" where ppo.PROMOTION_PROGRAM_ID = ? and ppo.status = ? and p.status = ?");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		
		String[] fieldNames = new String[] {
				"id",
				"promotionId",
				"productCode",
				"productName",
				"quantity",
				"amount"
		};
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL
		};
		
		return repo.getListByQueryAndScalar(PromotionProductOpenVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public PromotionProductOpen getPromotionProductOpen(Long id) throws DataAccessException {
		return repo.getEntityById(PromotionProductOpen.class, id);
	}
	
	@Override
	public PromotionProductOpen createPromotionProductOpen(PromotionProductOpen promotionProductOpen) throws DataAccessException {
		return repo.create(promotionProductOpen);
	}
	
	@Override
	public void updatePromotionProductOpen(PromotionProductOpen promotionProductOpen) throws DataAccessException {
		repo.update(promotionProductOpen);
	}
	
	@Override
	public List<PPConvertVO> listPromotionProductConvertVO(Long promotionId, String name) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select PROMOTION_PRODUCT_CONVERT_ID id, PROMOTION_PRODUCT_CONV_NAME name, PROMOTION_PROGRAM_ID promotionId from PROMOTION_PRODUCT_CONVERT where PROMOTION_PROGRAM_ID = ? and status = ? ");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		if(!StringUtility.isNullOrEmpty(name)) {
			sql.append(" and PROMOTION_PRODUCT_CONV_NAME = ? ");
			params.add(name);
		}
		String[] fieldNames = new String[] {
				"id",
				"name",
				"promotionId"
		};
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.LONG
		};
		
		List<PPConvertVO> result = repo.getListByQueryAndScalar(PPConvertVO.class, fieldNames, fieldTypes, sql.toString(), params);
		for(PPConvertVO vo : result) {
			sql = new StringBuilder();
			params = new ArrayList<Object>();
			sql.append("select ppcd.PROMOTION_PRODUCT_CONV_DTL_ID id, ppcd.PROMOTION_PRODUCT_CONVERT_ID convertId, p.product_code productCode, p.product_name productName, ppcd.IS_SOURCE_PRODUCT isSourceProduct, ppcd.FACTOR factor ");
			sql.append("from PROMOTION_PRODUCT_CONV_DTL ppcd inner join product p on ppcd.PRODUCT_ID = p.PRODUCT_ID and ppcd.status = ? and p.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			params.add(ActiveType.RUNNING.getValue());
			sql.append("where ppcd.PROMOTION_PRODUCT_CONVERT_ID = ?");
			params.add(vo.getId());
			
			fieldNames = new String[] {
					"id",
					"convertId",
					"productCode",
					"productName",
					"isSourceProduct",
					"factor"
			};
			fieldTypes = new Type[] {
					StandardBasicTypes.LONG,
					StandardBasicTypes.LONG,
					StandardBasicTypes.STRING,
					StandardBasicTypes.STRING,
					StandardBasicTypes.INTEGER,
					StandardBasicTypes.FLOAT
			};
			
			vo.setListDetail(repo.getListByQueryAndScalar(PPConvertDetailVO.class, fieldNames, fieldTypes, sql.toString(), params));
		}
		return result;
	}
	
	@Override
	public PPConvert getPPConvertById(Long id) throws DataAccessException {
		return repo.getEntityById(PPConvert.class, id);
	}
	
	@Override
	public PPConvert createPPConvert(PPConvert ppConvert) throws DataAccessException {
		return repo.create(ppConvert);
	}
	
	@Override
	public void updatePPConvert(PPConvert ppConvert) throws DataAccessException {
		repo.update(ppConvert);
	}
	
	@Override
	public PPConvertDetail getPPConvertDetailById(Long id) throws DataAccessException {
		return repo.getEntityById(PPConvertDetail.class, id);
	}
	
	@Override
	public PPConvertDetail createPPConvertDetail(PPConvertDetail ppConvertDetail) throws DataAccessException {
		return repo.create(ppConvertDetail);
	}
	
	@Override
	public void updatePPConvertDetail(PPConvertDetail ppConvertDetail) throws DataAccessException {
		repo.update(ppConvertDetail);
	}
	
	/**
	 * Lay ds group_level theo product_group
	 * 
	 * @author lacnv1
	 * @since Nov 17, 2014
	 */
	private List<GroupLevelVO> getChildrenGroupLevelVOOfProductGroup(long groupId, int isPromotionProduct) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT GL.GROUP_LEVEL_ID id, ");
		//sql.append(" GL.ORDER_NUMBER orderNumber,");
		sql.append(" (case when GL.ORDER_NUMBER is not null then GL.ORDER_NUMBER");
		sql.append(" else (select order_number from group_level where group_level_id = gl.parent_group_level_id) end) as orderNumber,");
		sql.append(" 		GL.MIN_QUANTITY as minQuantity, GL.MAX_QUANTITY maxQuantity, ");
		sql.append("  		GL.MIN_AMOUNT minAmount, GL.MAX_AMOUNT maxAmount, GL.HAS_PRODUCT hasProduct, ");
		sql.append(" 		GLD.GROUP_LEVEL_DETAIL_ID groupLevelDetailId, GLD.PRODUCT_ID productId, ");
		sql.append(" 		GLD.VALUE_TYPE valueType, GLD.VALUE value, GLD.IS_REQUIRED isRequired ");
		sql.append(" FROM GROUP_LEVEL GL ");
		sql.append(" LEFT JOIN GROUP_LEVEL_DETAIL GLD ON GL.GROUP_LEVEL_ID = GLD.GROUP_LEVEL_ID ");
		sql.append(" AND GLD.STATUS = 1 ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND GL.PRODUCT_GROUP_ID = ? ");
		params.add(groupId);
		//sql.append("  ");
		sql.append(" AND GL.HAS_PRODUCT = ? ");
		params.add(isPromotionProduct);
		//sql.append("  ");
		sql.append(" AND GL.STATUS = 1 ");
		
		sql.append(" and gl.parent_group_level_id is not null");
		
		sql.append(" ORDER BY orderNumber, GLD.IS_REQUIRED DESC ");
		
		final String[] fieldNames = new String[] { "id", "orderNumber", 
				"minQuantity","maxQuantity",
				"minAmount", "maxAmount", "hasProduct",
				"groupLevelDetailId","productId",
				"valueType", "value", "isRequired"
		};
		
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER, 
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, 
				StandardBasicTypes.INTEGER,StandardBasicTypes.BIG_DECIMAL,StandardBasicTypes.INTEGER
		};
		
		List<GroupLevelVO> groupLevelVOs = repo.getListByQueryAndScalar(GroupLevelVO.class, fieldNames, fieldTypes, sql.toString(), params);
		
		List<GroupLevelVO> result = new ArrayList<GroupLevelVO>();
		
		for(GroupLevelVO levelVO : groupLevelVOs){
			if(isPromotionProduct == SaleOrderDetailVO.ownerTypePromoOrder || 
					(levelVO.getGroupLevelDetailId() !=null && levelVO.getGroupLevelDetailId() > 0L)){
				
				boolean isExist = false;
				for(GroupLevelVO dto: result) {
					if(dto.getId().equals(levelVO.getId()) && dto.getOrderNumber().equals(levelVO.getOrderNumber())) {
						isExist = true;
						break;
					}
				}
				if(!isExist) {
					result.add(levelVO);
				}				
			}
		}
		return result;
	}
	
	/**
	 * Lay group_mapping theo product_group va group_level (cap con)
	 * 
	 * @author lacnv1
	 * @since Nov 17, 2014
	 */
	private GroupMapping getGroupMappingByProductGroupAndChildLevel(long productGroupId, long groupLevelId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from group_mapping");
		sql.append(" where sale_group_id = ?");
		params.add(productGroupId);
		sql.append(" and sale_group_level_id = (select parent_group_level_id from group_level where group_level_id = ?)");
		params.add(groupLevelId);
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		
		return repo.getEntityBySQL(GroupMapping.class, sql.toString(),params);
	}
	
	/**
	 * Lay ds group_level theo product_group va group_level cha
	 * 
	 * @author lacnv1
	 * @since Nov 17, 2014
	 */
	private GroupLevel getGroupLevelByProductGroupAndParentGroupLevel(long productGroupId, long parentLevelId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select * from group_level");
		sql.append(" where product_group_id = ?");
		params.add(productGroupId);
		sql.append(" and parent_group_level_id = ?");
		params.add(parentLevelId);
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		
		return repo.getEntityBySQL(GroupLevel.class, sql.toString(),params);
	}

	@Override
	public List<PromotionProgram> getAccumulativePromotionProgramByProductAndShopAndCustomer(
			Long shopId, Long customerId, Long staffId, Date orderDate) throws DataAccessException {
		if (shopId == null || staffId == null || customerId == null) {
			throw new DataAccessException("Invalid arguments (shop_id, staff_id, customer_id must not be null.)");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("  with lstShopParent as ( ");
		sql.append("  	select shop_id from shop where status=1 start with shop_id = ? connect by prior parent_shop_id=shop_id ");
		params.add(shopId);
		sql.append("  ) ");
		sql.append(" select pp.* ");
		sql.append(" from rpt_cttl rpt ");
		sql.append(" join promotion_program pp on rpt.promotion_program_id = pp.promotion_program_id and pp.status = 1");
		sql.append(" where 1 = 1 ");
		sql.append(" and rpt.shop_id = ? ");
		params.add(shopId);
		sql.append(" and rpt.customer_id = ? ");
		params.add(customerId);
		sql.append(" and exists (select 1 from promotion_shop_map psm where status=1 and promotion_program_id = pp.promotion_program_id ");
		sql.append("  			and shop_id in (select shop_id from lstShopParent)");
		
		//TODO vuongmq 13/01/2015; comment khong kiem tra CTKM cho NVBH, KH
		/*sql.append(" and (not exists (select 1 from promotion_staff_map where status=1 and promotion_shop_map_id = psm.promotion_shop_map_id) ");
		sql.append("     or exists (select 1 from promotion_staff_map where status=1 and promotion_shop_map_id = psm.promotion_shop_map_id and staff_id=?)) ");
		params.add(staffId);
		sql.append(" and (not exists (select 1 from promotion_customer_map where status=1 and promotion_shop_map_id = psm.promotion_shop_map_id) ");
		sql.append(" 	 or exists (select 1 from promotion_customer_map where status=1 and promotion_shop_map_id = psm.promotion_shop_map_id and customer_id=?)) ");
		params.add(customerId);*/
		sql.append(" ) ");
		// Kiem tra thuoc tinh KH
		sql.append(" AND   ");
		sql.append("    (  ");
		sql.append("      NOT EXISTS (SELECT 1 FROM PROMOTION_CUST_ATTR pa WHERE pa.promotion_program_id = pp.promotion_program_id and status = 1)  ");
		sql.append("      OR   ");
		sql.append(" ( ");
		sql.append(" EXISTS (SELECT * FROM PROMOTION_CUST_ATTR pa WHERE pa.promotion_program_id = pp.promotion_program_id AND status = 1)");
		sql.append(" AND ( ");
		sql.append(" NOT EXISTS ");
		sql.append(" ( ");
		sql.append(" (SELECT * ");
		sql.append(" FROM PROMOTION_CUST_ATTR pa ");
		sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" AND status = 1) ");
		sql.append(" minus ");
		sql.append(" (SELECT * ");
		sql.append(" FROM PROMOTION_CUST_ATTR pa ");
		sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" AND status = 1 ");
		sql.append(" AND ( (object_type = 1 ");
		sql.append(" AND ( ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type = 1 ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		sql.append(" AND upper(VALUE) = upper(PA.FROM_VALUE) ");
		sql.append(" ) ) ");
		sql.append(" OR ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type = 2 ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		sql.append(" AND to_number(VALUE) >= to_number(PA.FROM_VALUE) ");
		sql.append(" AND to_number(value) <= to_number(pa.to_value) ");
		sql.append(" ) ) ");
		sql.append(" OR ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type = 3 ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		sql.append(" AND (to_date(VALUE, 'yyyy-MM-dd') >= to_date(PA.FROM_VALUE, 'yyyy-MM-dd') ");
		sql.append(" OR pa.from_value IS NULL) ");
		sql.append(" AND (to_date(value, 'yyyy-MM-dd') <= to_date(pa.to_value, 'yyyy-MM-dd') ");
		sql.append(" OR pa.to_value IS NULL) ");
		sql.append(" ) ) ");
		sql.append(" OR ( EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM attribute ");
		sql.append(" WHERE value_type IN (4,5) ");
		sql.append(" AND attribute_id = pa.object_id ");
		sql.append(" AND status = 1 ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM attribute_value av ");
		sql.append(" WHERE attribute_id = pa.object_id ");
		sql.append(" AND table_name = 'CUSTOMER' ");
		sql.append(" AND TABLE_ID = ? ");
		params.add(customerId);
		sql.append(" AND STATUS = 1 ");
		// -----------------Kiem tra thuoc tinh mutil select
		sql.append(" and exists (select 1 from attribute_value_detail avd join promotion_cust_attr_detail pcd on avd.attribute_detail_id = pcd.object_id ");
		sql.append("             where avd.attribute_value_id = av.attribute_value_id and pcd.promotion_cust_attr_id = pa.promotion_cust_attr_id and pcd.status = 1 and avd.status = 1)");
		// -----------------Kiem tra thuoc tinh mutil select
		sql.append(" ) ) ) ) ");
		sql.append(" OR (object_type = 2 ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" WHERE status = 1 ");
		sql.append(" AND PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND OBJECT_ID = ");
		sql.append(" (SELECT CHANNEL_TYPE_ID FROM CUSTOMER WHERE CUSTOMER_ID = ? AND status = 1 ");
		params.add(customerId);
		sql.append(" ) ");
		sql.append(" ) ) ");
		sql.append(" OR (object_type = 3 ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" WHERE status = 1 ");
		sql.append(" AND PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND OBJECT_ID IN ");
		sql.append(" (SELECT SALE_LEVEL_CAT_ID FROM CUSTOMER_CAT_LEVEL WHERE CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" AND status = 1 ");
		sql.append(" ))))))))) ");
		// End Kiem tra thuoc tinh KH
		sql.append(" order by pp.promotion_program_code ");
		return repo.getListBySQL(PromotionProgram.class, sql.toString(), params);
	}

	@Override
	public List<GroupMapping> getPromotionProgramParentLevelStructures(Long promotionProgramId, Long buyGroupId) throws DataAccessException {
		// TODO: order by sale_group.order_number desc, sale_group_level.order_number desc
		if (promotionProgramId == null || buyGroupId == null) {
			throw new DataAccessException("Invalid arguments (promotion_program_id, buy_group_id must not be null.)");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from group_mapping gm ");
		sql.append(" where 1 = 1 ");
		sql.append(" and gm.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and gm.sale_group_id = ? ");
		params.add(buyGroupId);
		sql.append(" order by (select order_number from product_group where product_group_id = gm.sale_group_id) desc, ");
		sql.append(" 		(select order_number from group_level where group_level_id = gm.sale_group_level_id) desc, ");
		sql.append(" 		(select has_product from group_level where group_level_id = gm.promo_group_level_id) desc ");
		
		return repo.getListBySQL(GroupMapping.class, sql.toString(), params);
	}

	@Override
	public List<ProductGroup> getPromotionProgramStructure(Long promotionProgramId, ProductGroupType groupType)
			throws DataAccessException {
		if (promotionProgramId == null) {
			throw new DataAccessException("Invalid arguments (promotion_program_id must not be null.)");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from product_group pg ");
		sql.append(" where 1 = 1 ");
		sql.append(" and pg.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and pg.promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" and exists (select 1 from promotion_program where promotion_program_id = pg.promotion_program_id and status = ?) ");
		params.add(ActiveType.RUNNING.getValue());
		if (groupType != null) {
			sql.append(" and pg.group_type = ? ");
			params.add(groupType.getValue());
		}
		
		return repo.getListBySQL(ProductGroup.class, sql.toString(), params);
	}	
	
	@Override
	public Integer checkGroupLevelMuaValue(Long groupId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select case when (minAmount is null or minAmount = 0) and minQuantity > 0 then 1 when (minQuantity is null or minQuantity = 0) and minAmount > 0 then 2 else 0 end as count from ");
		sql.append("(select sum(minAmount) minAmount, sum(minQuantity) minQuantity from ");
		sql.append("(select NVL(MIN_AMOUNT, 0) minAmount, NVL(MIN_QUANTITY, 0) minQuantity FROM GROUP_LEVEL WHERE PRODUCT_GROUP_ID = ? AND status = 1 ");
		params.add(groupId);
		sql.append("UNION ");
		sql.append("select case when VALUE_TYPE = 2 then nvl(value, 0) else 0 end minAmount, case when VALUE_TYPE = 1 then nvl(value, 0) else 0 end minQuantity ");
		sql.append("from group_level_detail where status = 1 and group_level_id IN (select group_level_id from group_level where product_group_id = ?)))");
		params.add(groupId);
		return repo.countBySQL(sql.toString(), params);
	}
	
	@Override
	public Map<Long, List<SubLevelMapping>> getListMapDetail(Long parentId, ProductGroupType type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select gld.group_level_id levelId, gld.group_level_detail_id levelDetailId, p.product_id productId, p.product_code productCode, p.product_name productName, ");
		sql.append("case when gld.value_type = 1 then gld.value else 0 end quantity, case when gld.value_type = 2 then gld.value else 0 end amount, is_required isRequired ");
		sql.append("from group_level_detail gld inner join product p on gld.PRODUCT_ID = p.PRODUCT_ID and gld.status = 1 ");
		sql.append("where gld.GROUP_LEVEL_ID IN ( ");
		sql.append("select group_level_id from group_level where PARENT_GROUP_LEVEL_ID = ? and status = ?");
		params.add(parentId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(")");
		
		final String[] fieldNames = new String[] { "levelId", //1
				"levelDetailId", //2
				"productId", //3
				"productCode", //4
				"productName", //5
				"quantity", //6
				"amount", //7
				"isRequired"};//8

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,//1
				StandardBasicTypes.LONG, //2
				StandardBasicTypes.LONG, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.STRING,//5
				StandardBasicTypes.INTEGER,//6
				StandardBasicTypes.BIG_DECIMAL,//7
				StandardBasicTypes.INTEGER};//8
		List<SubLevelMapping> result = repo.getListByQueryAndScalar(SubLevelMapping.class, fieldNames, fieldTypes, sql.toString(), params);
		Map<Long, List<SubLevelMapping>> map = new HashMap<Long, List<SubLevelMapping>>();
		for(SubLevelMapping subLevel : result) {
			if(map.get(subLevel.getLevelId()) == null) {
				List<SubLevelMapping> list = new ArrayList<SubLevelMapping>();
				list.add(subLevel);
				map.put(subLevel.getLevelId(), list);
			} else {
				List<SubLevelMapping> list = map.get(subLevel.getLevelId());
				list.add(subLevel);
				map.put(subLevel.getLevelId(), list);
			}
		}
		return map;
	}
	
	private List<PromotionProgram> getPromotionProgramByProductAndShopAndCustomer(long productId, long shopId, long customerId, Date orderDate,
			Long saleOrderId, Long staffId, String lstProductIds, String lstQuantities, String lstAmounts) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		Customer customer = customerDAO.getCustomerById(customerId);
		if (customer == null) {
			return null;
		}
		sql.append("  SELECT pp.*  ");
		sql.append("  FROM promotion_program pp  ");
		sql.append("  WHERE (pp.status   = 1 and pp.type not in ('ZM', 'ZD', 'ZH', 'ZT', 'ZV23') ");
		sql.append("  AND pp.from_date < TRUNC(?) + 1  ");
		sql.append("  AND (pp.to_date    >= TRUNC(?) or pp.to_date is null)");
		params.add(orderDate);
		params.add(orderDate);
		sql.append(" and exists ");
		sql.append(" ( ");
		sql.append(" 	select 1 from product_group pg ");
		sql.append(" 	where pg.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 	and pg.group_type =1 ");
		sql.append(" 	and pg.status = 1 ");
		sql.append(" 	and exists ");
		sql.append(" 		( ");
		sql.append(" 			select 1 from group_level gl ");
		sql.append(" 			where gl.product_group_id = pg.product_group_id ");
		sql.append(" 			and gl.has_product = 1 and gl.status = 1 ");
		sql.append(" 			and exists ");
		sql.append(" 				( ");
		sql.append(" 					select 1 from group_level_detail gld ");
		sql.append(" 					where gl.GROUP_LEVEL_ID  = GLD.GROUP_LEVEL_ID ");
		sql.append(" 					and GLD.product_id          = ? ");
		params.add(productId);
		sql.append(" 					AND gld.STATUS               = 1 ");
		sql.append(" 				) ");
		sql.append(" 		) ");
		sql.append(" ) ");
		sql.append("  AND EXISTS  ");
		sql.append("    (SELECT 1  ");
		sql.append("    FROM promotion_shop_map psm  ");
		sql.append("    WHERE psm.promotion_program_id = pp.promotion_program_id and status = 1 ");
		sql.append("    AND (psm.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id)  ) ");
		params.add(shopId);
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("    AND psm.from_date < TRUNC(?) + 1  ");
		sql.append("    AND (psm.to_date    >= TRUNC(?) or psm.to_date is null) ");
		params.add(orderDate);
		params.add(orderDate);
		sql.append("    AND ( (quantity_max IS NULL OR NVL(quantity_received_total, 0) < quantity_max) ) ");
		//end
		
		//TODO vuongmq 13/01/2015; comment khong kiem tra CTKM cho NVBH, KH
		//Kiem tra KM ap dung cho KH
		/*sql.append("    AND  ");
		sql.append("       ( ");
		sql.append("         (not exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1) and ((psm.quantity_max is null or NVL(psm.quantity_received_total, 0) < psm.quantity_max)) )");
		sql.append("         or ");
		sql.append("         exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status  = 1 and customer_id = ? and ((quantity_max is null or NVL(quantity_received_total, 0) < quantity_max)) ");
		params.add(customerId);
		sql.append("       ) ");
		sql.append("    ) ");
		//End kiem tra KM ap dung cho KH
		//Kiem tra KM ap dung cho NVBH
		sql.append(" AND (  ");
		sql.append("         (not exists (select 1 from promotion_staff_map where promotion_shop_map_id = psm.promotion_shop_map_id and shop_id = ? and status = 1) and ((psm.quantity_max is null or NVL(psm.quantity_received_total, 0) < psm.quantity_max)) )");
		sql.append("         or ");
		sql.append("         exists (select 1 from promotion_staff_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and staff_id = ? and shop_id = ? and ((quantity_max is null or NVL(quantity_received_total, 0) < quantity_max))) ");
		params.add(shopId);
		params.add(staffId);
		params.add(shopId);
		sql.append(" 	 ) ");*/
		
		sql.append(" and (not exists ");
		sql.append(" 	(select 1 ");
		sql.append(" 	 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 	 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 	  and status = 1) ");
		sql.append("    or (exists ");
		sql.append(" 		(select 1 ");
		sql.append(" 		 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 		 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 		  and status = 1) ");
		sql.append(" 	   and (not exists ( ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						  and status = 1) ");
		sql.append(" 						 minus ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						   and status = 1 ");
		sql.append(" 						   and ( ");
		sql.append(" 						   		(object_type = 1 ");
		sql.append(" 								 and ( ");
		sql.append(" 								 	(exists ");
		sql.append(" 										(select 1 ");
		sql.append(" 										 from customer_attribute ");
		sql.append(" 										 where type = 1 ");
		sql.append(" 										  and customer_attribute_id = pa.object_id ");
		sql.append(" 										  and status = 1) ");
		sql.append(" 										and exists ( ");
		sql.append(" 											select 1 ");
		sql.append(" 											from customer_attribute_detail ");
		sql.append(" 											where customer_id = ? ");
		params.add(customerId);
		sql.append(" 											and status = 1 ");
		sql.append(" 											and customer_attribute_id = pa.object_id ");
		sql.append(" 											and upper(value) = upper(pa.from_value) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 2 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and to_number(value) >= to_number(pa.from_value) ");
		sql.append(" 												and to_number(value) <= to_number(pa.to_value) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 3 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') >= to_date(pa.from_value, 'yyyy-mm-dd') or pa.from_value is null) ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') <= to_date(pa.to_value, 'yyyy-mm-dd') or pa.to_value is null) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type in (4, 5) ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail cad ");
		sql.append(" 												join promotion_cust_attr_detail pcad  ");
		sql.append(" 													on pcad.object_type = 1  ");
		sql.append(" 													and pcad.status = 1 ");
		sql.append(" 													and cad.customer_attribute_enum_id = pcad.object_id ");
		sql.append(" 												where cad.customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and cad.status = 1 ");
		sql.append(" 												and pcad.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
		sql.append(" 												and cad.customer_attribute_id = pa.object_id ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 								) ");
		sql.append(" 								or (object_type = 2 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID = ");
		sql.append(" 										(select CHANNEL_TYPE_ID ");
		sql.append(" 										 from CUSTOMER ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 								or (object_type = 3 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID in ");
		sql.append(" 										(select SALE_LEVEL_CAT_ID ");
		sql.append(" 										 from CUSTOMER_CAT_LEVEL ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 							) ");
		sql.append(" 						) ");
		sql.append(" 				) ");
		sql.append(" 			) ");
		sql.append(" 	) ");
		sql.append(" ) ");
		
		sql.append("    )  ");
		sql.append(")");
		return repo.getListBySQL(PromotionProgram.class, sql.toString().replaceAll("  ", " "), params);
	}
	
	private List<PromotionProgram> getListPromotionForOrder(Long shopId,
			Long customerId, Date orderDate, Long saleOrderId, Long staffId, String lstProductIds, String lstQuantities, String lstAmounts)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select pp.* from promotion_program pp ");
		sql.append(" where pp.promotion_program_id in ( SELECT PP.promotion_program_id promotionProgramId ");
		sql.append(" FROM   promotion_program pp,  ");
		sql.append("       promotion_shop_map psm ");
//		sql.append("       LEFT JOIN promotion_customer_map pcm ON pcm.promotion_shop_map_id = psm.promotion_shop_map_id ");
		sql.append(" WHERE  1 = 1 ");
		sql.append("       AND psm.shop_id IN ( select shop_id from shop where status = 1 START WITH shop_id = ? CONNECT BY PRIOR parent_shop_id = shop_id ");
		params.add(shopId);
		
		sql.append(") ");
		sql.append("       AND PP.STATUS = 1 AND psm.status = 1 and pp.type in ('ZV19', 'ZV20', 'ZV21', 'ZV24') ");
		sql.append("       AND psm.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 	   AND pp.from_date < TRUNC(?) + 1 ");
		sql.append(" 	   AND (pp.to_date    >= TRUNC(?) or pp.to_date is null) ");
		params.add(orderDate);
		params.add(orderDate);
		sql.append("       AND psm.from_date < TRUNC(?) + 1  ");
		sql.append("       AND (psm.to_date    >= TRUNC(?) OR psm.to_date is null)  AND ( (psm.quantity_max IS NULL OR NVL(psm.quantity_received_total, 0) < psm.quantity_max) ) ");
		params.add(orderDate);
		params.add(orderDate);
		
		// lacnv1 - check them dieu kien KM mo moi
//		if (saleOrderId == null) {
//			sql.append(" and 1 = f_check_promotion_open_product(pp.promotion_program_id, pp.quanti_month_new_open, pp.type,");
//			sql.append("         ?, ?, ?, ?)");
//			params.add(customerId);
//			params.add(lstProductIds);
//			params.add(lstQuantities);
//			params.add(lstAmounts);
//		} else {
//			sql.append(" and 1 = f_check_promo_open_product2(pp.promotion_program_id, pp.quanti_month_new_open, pp.type,");
//			sql.append("         ?, ?, ?, ?, ?)");
//			params.add(customerId);
//			params.add(lstProductIds);
//			params.add(lstQuantities);
//			params.add(lstAmounts);
//			params.add(saleOrderId);
//		}
		// --
		
		//TODO vuongmq 13/01/2015; comment khong kiem tra CTKM cho NVBH, KH
		// Kiem tra xem co ap dung toi muc KH ko?
		/*sql.append(" AND ( ");
		sql.append(" NOT EXISTS ( SELECT 1 ");
		sql.append(" 			 FROM promotion_customer_map pcmm ");
		sql.append(" 			 WHERE pcmm.promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and ((pcmm.quantity_max is null or NVL(pcmm.quantity_received_total, 0) < pcmm.quantity_max))) ");
		sql.append(" OR EXISTS ( SELECT 1 ");
		sql.append(" 			 FROM promotion_customer_map pcmm ");
		sql.append(" 			 WHERE promotion_shop_map_id = psm.promotion_shop_map_id AND status = 1 AND customer_id = ? and ((pcmm.quantity_max is null or NVL(pcmm.quantity_received_total, 0) < pcmm.quantity_max)) ) ");
		sql.append(" 	) ");
		params.add(customerId);
		
		// Kiem tra xem co ap dung toi muc NV ko?
		sql.append(" AND ( ");
		sql.append(" NOT EXISTS ( SELECT 1 ");
		sql.append(" 			 FROM promotion_staff_map pstm ");
		sql.append(" 			 WHERE pstm.promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and ((pstm.quantity_max is null or NVL(pstm.quantity_received_total, 0) < pstm.quantity_max))) ");
		sql.append(" OR EXISTS ( SELECT 1 ");
		sql.append(" 			 FROM promotion_staff_map pstm ");
		sql.append(" 			 WHERE promotion_shop_map_id = psm.promotion_shop_map_id AND status = 1 AND staff_id = ? and ((pstm.quantity_max is null or NVL(pstm.quantity_received_total, 0) < pstm.quantity_max)) ) ");
		sql.append(" 	) ");
		params.add(staffId);*/
		
		//Nhom sp ban & khong co sp
		sql.append(" AND EXISTS ( ");
		sql.append(" 				SELECT 1 FROM product_group pg WHERE pg.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 				AND pg.status = 1 AND pg.group_type = 1 ");
		sql.append(" 				AND exists ( ");
		sql.append(" 								SELECT 1 FROM group_level gl WHERE gl.product_group_id = pg.product_group_id ");
		sql.append(" 								AND gl.has_product = 0 AND gl.status = 1 ");
		sql.append(" 							) ");
		sql.append(" 			) " );
		
		// Kiem tra thuoc tinh KH
		sql.append(" and (not exists ");
		sql.append(" 	(select 1 ");
		sql.append(" 	 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 	 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 	  and status = 1) ");
		sql.append("    or (exists ");
		sql.append(" 		(select 1 ");
		sql.append(" 		 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 		 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 		  and status = 1) ");
		sql.append(" 	   and (not exists ( ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						  and status = 1) ");
		sql.append(" 						 minus ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						   and status = 1 ");
		sql.append(" 						   and ( ");
		sql.append(" 						   		(object_type = 1 ");
		sql.append(" 								 and ( ");
		sql.append(" 								 	(exists ");
		sql.append(" 										(select 1 ");
		sql.append(" 										 from customer_attribute ");
		sql.append(" 										 where type = 1 ");
		sql.append(" 										  and customer_attribute_id = pa.object_id ");
		sql.append(" 										  and status = 1) ");
		sql.append(" 										and exists ( ");
		sql.append(" 											select 1 ");
		sql.append(" 											from customer_attribute_detail ");
		sql.append(" 											where customer_id = ? ");
		params.add(customerId);
		sql.append(" 											and status = 1 ");
		sql.append(" 											and customer_attribute_id = pa.object_id ");
		sql.append(" 											and upper(value) = upper(pa.from_value) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 2 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and to_number(value) >= to_number(pa.from_value) ");
		sql.append(" 												and to_number(value) <= to_number(pa.to_value) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 3 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') >= to_date(pa.from_value, 'yyyy-mm-dd') or pa.from_value is null) ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') <= to_date(pa.to_value, 'yyyy-mm-dd') or pa.to_value is null) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type in (4, 5) ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail cad ");
		sql.append(" 												join promotion_cust_attr_detail pcad  ");
		sql.append(" 													on pcad.object_type = 1  ");
		sql.append(" 													and pcad.status = 1 ");
		sql.append(" 													and cad.customer_attribute_enum_id = pcad.object_id ");
		sql.append(" 												where cad.customer_id = ? ");
		params.add(customerId);
		sql.append(" 												and cad.status = 1 ");
		sql.append(" 												and pcad.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
		sql.append(" 												and cad.customer_attribute_id = pa.object_id ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 								) ");
		sql.append(" 								or (object_type = 2 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID = ");
		sql.append(" 										(select CHANNEL_TYPE_ID ");
		sql.append(" 										 from CUSTOMER ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 								or (object_type = 3 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID in ");
		sql.append(" 										(select SALE_LEVEL_CAT_ID ");
		sql.append(" 										 from CUSTOMER_CAT_LEVEL ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(customerId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 							) ");
		sql.append(" 						) ");
		sql.append(" 				) ");
		sql.append(" 			) ");
		sql.append(" 	) ");
		sql.append(" ) ");
		
		sql.append("   ) ");
		sql.append(" order by promotion_program_code");
		
		return repo.getListBySQL(PromotionProgram.class, sql.toString().replaceAll("  ", " "), params);
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<CommercialSupportVO> getListCommercialSupportOfSaleOrder(long saleOrderId, KPaging<CommercialSupportVO> paging)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT pp.promotion_program_code code, ");
		sql.append(" pp.promotion_program_name name,  ");
		sql.append(" pp.type promotionType, ");
		sql.append(" 1 type ");
		sql.append(" FROM PROMOTION_PROGRAM pp ");
		sql.append(" JOIN SALE_ORDER_DETAIL sod ON pp.PROMOTION_PROGRAM_CODE = sod.PROGRAM_CODE ");
		sql.append(" WHERE sod.sale_order_id     = ? ");
		params.add(saleOrderId);
		sql.append(" AND sod.is_free_item        = 1 ");
		sql.append(" AND sod.program_type       <> 0  ");
		
		String[] fieldNames = {
				"code","name",
				"promotionType", "type",
		};
		
		Type[] fieldTypes = {
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER
		};
		
		if (paging == null) {
			return repo.getListByQueryAndScalar(CommercialSupportVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		StringBuilder cSql = new StringBuilder();
		cSql.append("select count(1) as count from (").append(sql.toString()).append(")");
		return repo.getListByQueryAndScalarPaginated(CommercialSupportVO.class, fieldNames, fieldTypes, sql.toString(), cSql.toString(), params, params, paging);
	}

	@Override
	public RptAccumulativePromotionProgram getRptAccumulativePP(Long shopId, Long promotionProgramId, Long customerId, Date orderDate) throws DataAccessException {
		if (shopId == null || promotionProgramId == null || customerId == null) {
			throw new DataAccessException("Invalid arguments (shop_id, promotion_program_id, customer_id must not be null.)");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select rpt.* ");
		sql.append(" from rpt_cttl rpt ");
		sql.append(" where 1 = 1 ");
		sql.append(" and rpt.shop_id = ? ");
		params.add(shopId);
		sql.append(" and rpt.customer_id = ? ");
		params.add(customerId);
		sql.append(" and rpt.promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" and exists (select 1 from promotion_program where promotion_program_id = ? and status = ?) ");
		params.add(promotionProgramId);
		params.add(ActiveType.RUNNING.getValue());
		
		return repo.getEntityBySQL(RptAccumulativePromotionProgram.class, sql.toString(), params);
	}

	@Override
	public List<RptAccumulativePromotionProgramDetail> getRptAccumulativePPDetail(
			Long rptAccumulativePPId) throws DataAccessException {
		if (rptAccumulativePPId == null) {
			throw new DataAccessException("Invalid arguments (rptAccumulativePPId must not be null.)");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from rpt_cttl_detail rd ");
		sql.append(" where 1 = 1 ");
		sql.append(" and rd.rpt_cttl_id = ? ");
		params.add(rptAccumulativePPId);
		
		return repo.getListBySQL(RptAccumulativePromotionProgramDetail.class, sql.toString(), params);
	}
	
	@Override
	public List<RptCTTLPayDetail> getListRptCTTLPayDetail(Long rptPayId, Long shopId, Long promotionProgramId, Long customerId, Date orderDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select rdp.* from Rpt_cttl_pay rp ");
		sql.append(" join rpt_cttl_detail_pay rdp on rp.rpt_cttl_pay_id = rdp.rpt_cttl_pay_id ");
		sql.append(" where 1=1 ");
		if(orderDate!=null){
			sql.append(" and rp.create_date >= trunc(?)  ");
			params.add(orderDate);
		}
		if(rptPayId!=null){
			sql.append(" and rp.rpt_cttl_pay_id = ?  ");
			params.add(rptPayId);
		}
		if(promotionProgramId!=null){
			sql.append(" and rp.PROMOTION_PROGRAM_ID = ? ");
			params.add(promotionProgramId);
		}
		if(shopId!=null){
			sql.append(" and rp.SHOP_ID = ? ");
			params.add(shopId);
		}
		if(customerId!=null){
			sql.append(" and rp.customer_id = ? ");
			params.add(customerId);
		}
		sql.append(" and rp.APPROVED in (0,1) ");
		
		return repo.getListBySQL(RptCTTLPayDetail.class, sql.toString(), params);
	}
	
	@Override
	public List<RptCTTLPay> getLastRptCTTLPayByCustomer(Long shopId, Long promotionProgramId, Long customerId, Date orderDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from Rpt_cttl_pay rp ");
		sql.append(" where rp.create_date >= trunc(?) ");
		params.add(orderDate);
		sql.append(" and rp.APPROVED in (0,1,2) ");
		if(promotionProgramId!=null){
			sql.append(" and rp.PROMOTION_PROGRAM_ID = ? ");
			params.add(promotionProgramId);
		}
		if(shopId!=null){
			sql.append(" and rp.SHOP_ID = ? ");
			params.add(shopId);
		}
		if(customerId!=null){
			sql.append(" and rp.customer_id = ? ");
			params.add(customerId);
		}
		sql.append(" order by rp.amount_promotion desc");
		
		return repo.getListBySQL(RptCTTLPay.class, sql.toString(), params);
	}
	
	@Override
	public List<RptCTTLPay> getListRptCTTLPay(SaleOrderFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select rp.* from Rpt_cttl_pay rp");
		sql.append(" where 1=1");
		if(filter.getSaleOrderId()!=null){
			sql.append(" and rp.sale_order_id = ?");
			params.add(filter.getSaleOrderId());
		}
		if (filter.getProgramId() != null) {
			sql.append(" and rp.promotion_program_id = ?");
			params.add(filter.getProgramId());
		}
		return repo.getListBySQL(RptCTTLPay.class, sql.toString(), params);
	}
	
	
	@Override
	public RptCTTLPay createRptCTTLPay( RptCTTLPay rptCTTLPay, LogInfoVO logInfo) throws DataAccessException {
		if (rptCTTLPay == null) {
			throw new IllegalArgumentException("rptCTTLPay");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.create(rptCTTLPay);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, rptCTTLPay,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(RptCTTLPay.class,
				rptCTTLPay.getId());
	}
	
	@Override
	public RptCTTLPay getRptCTTLPayById(long id)
			throws DataAccessException {
		return repo.getEntityById(RptCTTLPay.class, id);
	}
	
	@Override
	public RptCTTLPayDetail createRptCTTLPayDetail( RptCTTLPayDetail rptCTTLPayDetail, LogInfoVO logInfo) throws DataAccessException {
		if (rptCTTLPayDetail == null) {
			throw new IllegalArgumentException("RptCTTLPayDetail");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.create(rptCTTLPayDetail);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, rptCTTLPayDetail,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(RptCTTLPayDetail.class,
				rptCTTLPayDetail.getId());
	}
	
	@Override
	public RptCTTLPayDetail getRptCTTLPayDetailById(long id)
			throws DataAccessException {
		return repo.getEntityById(RptCTTLPayDetail.class, id);
	}

	@Override
	public int countSaleOrderDetailHasInvalidPromotionGroupLevel(Long saleOrderId) throws DataAccessException {
		if (saleOrderId == null) {
			throw new DataAccessException("saleOrderId must not be null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count ");
		sql.append(" from sale_order_detail sod ");
		sql.append(" where 1 = 1 ");
		sql.append(" and sod.sale_order_id = ? ");
		params.add(saleOrderId);
		sql.append(" and sod.group_level_id is null ");
		sql.append(" and not exists ( ");
		sql.append("     select 1 ");
		sql.append("     from group_level ");
		sql.append("     where sod.group_level_id = group_level_id ");
		sql.append("     and status = 1 ");
		sql.append(" ) ");
		
		return repo.countBySQL(sql.toString(), params);
	}

	@Override
	public List<String> getChangedStructurePromotionProgramInSaleOrder(
			Long saleOrderId) throws DataAccessException {
		if (saleOrderId == null) {
			throw new DataAccessException("saleOrderId must not be null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct(sod.program_code) ");
		sql.append(" from sale_order_detail sod ");
		sql.append(" where 1 = 1 ");
		sql.append(" and sod.sale_order_id = ? ");
		params.add(saleOrderId);
		sql.append(" and sod.is_free_item = 1 ");
		sql.append(" and ( ");
		sql.append("     not exists (select 1 from group_level where group_level_id = sod.group_level_id) ");
		sql.append("     or not exists (select 1 from product_group where product_group_id = sod.product_group_id) ");
		sql.append(" ) ");
		sql.append(" and instr(sod.programe_type_code, 'ZV') >= 1 ");
		
		final String[] fieldNames = new String[] {"program_code"};
		final Type[] fieldTypes = new Type[] {StandardBasicTypes.STRING};
		
		return repo.getListByQueryAndScalar(fieldNames, fieldTypes, sql.toString(), params, null, null);
	}

	@Override
	public List<OrderProductVO> getSaleOrdersHasChangedStructurePromotionProgram(
			List<Long> saleOrderIds) throws DataAccessException {
		if (saleOrderIds == null || saleOrderIds.size() == 0) {
			return null;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();		
		sql.append(" select ");
		sql.append(" 	sale_order_id saleOrderId, ");
		sql.append(" 	listagg(program_code, ', ') within group (order by program_code) promotionProgramCode ");
		sql.append(" from ( ");
		sql.append(" 	select distinct ");
		sql.append(" 		sod.sale_order_id, ");
		sql.append(" 		sod.program_code ");
		sql.append(" 	from sale_order_detail sod ");
		sql.append(" 	where 1 = 1 ");
		/*
		 * add sale_order_id params
		 */
		sql.append(" 	and sod.sale_order_id in (-1 ");
		for (Long saleOrderId : saleOrderIds) {
			sql.append(" , ?");
			params.add(saleOrderId);
		}
		sql.append(" ) ");
		
		sql.append(" 	and sod.is_free_item = 1 ");
		sql.append(" 	and sod.program_code is not null ");
		sql.append(" 	and ( ");
		sql.append(" 		not exists (select 1 from group_level where group_level_id = sod.group_level_id) ");
		sql.append(" 		or not exists (select 1 from product_group where product_group_id = sod.product_group_id) ");
		sql.append(" 	) ");
		sql.append(" 	and instr(sod.programe_type_code, 'ZV') >= 1 ");
		sql.append(" ) ");
		sql.append(" group by sale_order_id ");
		
		String[] fieldNames = new String[] { "saleOrderId", "promotionProgramCode"};
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING};
		
		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<OrderProductVO> getSaleOrdersHasChangedStructurePromotionProgram(SoFilter soFilter) throws DataAccessException {
		if (soFilter == null) {
			return null;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();		
		sql.append(" select ");
		sql.append(" 	sale_order_id saleOrderId, ");
		sql.append(" 	listagg(program_code, ', ') within group (order by program_code) promotionProgramCode ");
		sql.append(" from ( ");
		sql.append(" 	select distinct ");
		sql.append(" 		sod.sale_order_id, ");
		sql.append(" 		sod.program_code ");
		sql.append(" 	from sale_order_detail sod ");
		sql.append(" 	where 1 = 1 ");
		/*
		 * add sale_order_id params
		 */
		if ((soFilter.getLstSaleOrderId() != null && soFilter.getLstSaleOrderId().size() > 0)
			|| soFilter.getLockDay() != null
			|| soFilter.getShopId() != null) {
			sql.append(" 	and sod.sale_order_id in ( ");
			sql.append(" 		select so.sale_order_id ");
			sql.append(" 		from sale_order so ");
			sql.append(" 		where 1 = 1 ");
			if (soFilter.getLstSaleOrderId() != null) {// tinh tong so luong cho 1 lst don hang
				sql.append(" and so.sale_order_id in (-1");
				for (Long orderId : soFilter.getLstSaleOrderId()) {
					sql.append(",?");
					params.add(orderId);
				}
				sql.append(") ");
			}
			if (soFilter.getLockDay() != null) {
				sql.append(" and so.order_date>=trunc(?) and so.order_date<trunc(?)+1 ");
				params.add(soFilter.getFromDate() != null ? soFilter.getFromDate() : soFilter.getLockDay());
				params.add(soFilter.getLockDay());				
			}
			if (soFilter.getShopId() != null) {
				sql.append(" and so.shop_id=? ");
				params.add(soFilter.getShopId());				
			}
			sql.append(" ) ");
		}
		
		sql.append(" 	and sod.is_free_item = 1 ");
		sql.append(" 	and sod.program_code is not null ");
		sql.append(" 	and ( ");
		sql.append(" 		not exists (select 1 from group_level where group_level_id = sod.group_level_id) ");
		sql.append(" 		or not exists (select 1 from product_group where product_group_id = sod.product_group_id) ");
		sql.append(" 	) ");
		sql.append(" 	and instr(sod.programe_type_code, 'ZV') >= 1 ");
		sql.append(" ) ");
		sql.append(" group by sale_order_id ");
		
		String[] fieldNames = new String[] { "saleOrderId", "promotionProgramCode"};
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING};
		
		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}


	/**
	 * @author lacnv1
	 */
	@Override
	public List<RptCTTLPayDetail> getRptCTTLPayDetailByRptPayId(long rptPayId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from rpt_cttl_detail_pay where rpt_cttl_pay_id = ?");
		params.add(rptPayId);
		
		List<RptCTTLPayDetail> lst = repo.getListBySQL(RptCTTLPayDetail.class, sql.toString(), params);
		return lst;
	}
	
	/**
	 * Lay ds quy doi san pham CTKM
	 * 
	 * @author tungmt
	 * @since 1/1/2014
	 */
	@Override
	public List<PromoProductConvVO> getListConvertPromotionProgram(Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstConvert as ( ");
		sql.append(" select pcd.*,pc.promotion_program_id from PROMOTION_PRODUCT_CONV_DTL pcd ");
		sql.append(" join PROMOTION_PRODUCT_CONVERT pc on pc.PROMOTION_PRODUCT_CONVERT_ID = pcd.PROMOTION_PRODUCT_CONVERT_ID ");
		sql.append(" where pc.status=1 ");
		if(promotionProgramId!=null){
			sql.append(" and pc.PROMOTION_PROGRAM_ID = ? ");
			params.add(promotionProgramId);
		}
		sql.append(" ), ");
		sql.append(" lstIsSource1 as ( ");
		sql.append(" select * from lstConvert where is_source_product=1 ");
		sql.append(" ), ");
		sql.append(" lstIsSource0 as ( ");
		sql.append(" select * from lstConvert where is_source_product=0 ");
		sql.append(" ) ");
		sql.append(" select i1.PROMOTION_PRODUCT_CONVERT_ID promotionProductConvertId, i1.promotion_program_id promotionProgramId, ");
		sql.append(" i1.PRODUCT_ID productId1, i0.PRODUCT_ID productId0, i1.factor factor1, i0.factor factor0 ");
		sql.append(" from lstIsSource1 i1 ");
		sql.append(" join lstIsSource0 i0 on i1.PROMOTION_PRODUCT_CONVERT_ID = i0.PROMOTION_PRODUCT_CONVERT_ID ");
		sql.append(" order by i1.PROMOTION_PRODUCT_CONVERT_ID, i0.product_id ");
		
		final String[] fieldNames = new String[] { 
				"promotionProductConvertId","promotionProgramId",
				"productId1", "productId0","factor1","factor0"
				
		};
		
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,StandardBasicTypes.LONG, 
				StandardBasicTypes.LONG,StandardBasicTypes.LONG,  StandardBasicTypes.BIG_DECIMAL,StandardBasicTypes.BIG_DECIMAL 
				
		};
		
		return repo.getListByQueryAndScalar(PromoProductConvVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<GroupLevelDetail> getListGroupLevelDetailByPromoGroupAndPromoGroupLevel(long promoGroupId, long promoGroupLevelId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from group_level_detail");
		sql.append(" where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and group_level_id in (");
		sql.append(" select group_level_id from group_level");
		sql.append(" where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and parent_group_level_id in (");
		sql.append(" select sale_group_level_id from group_mapping");
		sql.append(" where promo_group_id = ? and promo_group_level_id = ?");
		params.add(promoGroupId);
		params.add(promoGroupLevelId);
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" )");
		sql.append(" )");
		
		List<GroupLevelDetail> lst = repo.getListBySQL(GroupLevelDetail.class, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionProgram> getPromotionProgramsZV24InSaleOrder(long orderId, long customerId,
			Date orderDate, List<Long> lstPassOrderId) throws DataAccessException {
		if (orderDate == null) {
			orderDate = commonDAO.getSysDate();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select pp.* from sale_order_detail sod");
		sql.append(" join promotion_program pp on (pp.promotion_program_code = sod.join_program_code and pp.type = ?)");
		params.add(PromotionType.ZV24.getValue());
		sql.append(" where sale_order_id = ?");
		params.add(orderId);
		sql.append(" and is_free_item = 0");
		sql.append(" and exists (");
		sql.append(" select 1 from promotion_product_open ppo");
		sql.append(" where promotion_program_id = pp.promotion_program_id");
		sql.append(" and product_id in (");
		sql.append(" select product_id from sale_order_detail");
		sql.append(" where sale_order_id = ?");
		params.add(orderId);
		sql.append(" and is_free_item = 0");
		sql.append(" and (ppo.quantity is null or quantity >= ppo.quantity)");
		sql.append(" and (ppo.amount is null or amount >= ppo.amount)");
		sql.append(" )");
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and not exists (");
		sql.append(" select 1 from sale_order_detail sod");
		sql.append(" where order_date >= add_months(trunc(?), -1*pp.quanti_month_new_open)");
		params.add(orderDate);
		sql.append(" and is_free_item = 0 and product_id = ppo.product_id");
		sql.append(" and exists (");
		sql.append(" select 1 from sale_order so1");
		sql.append(" where sale_order_id = sod.sale_order_id and customer_id = ?");
		params.add(customerId);
		sql.append(" and type = ?");
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		if (lstPassOrderId == null || lstPassOrderId.size() == 0) {
			sql.append(" and approved = ?");
			params.add(SaleOrderStatus.APPROVED.getValue());
		} else {
			sql.append(" and (approved = ?");
			params.add(SaleOrderStatus.APPROVED.getValue());
			sql.append(" or sale_order_id in (-1");
			for (Long idt : lstPassOrderId) {
				sql.append(",?");
				params.add(idt);
			}
			sql.append("))");
		}
		sql.append(" )");
		sql.append(" )");
		sql.append(" )");
		sql.append(" and not exists (");
		sql.append(" select 1 from sale_order_promotion");
		sql.append(" where sale_order_id = ?");
		params.add(orderId);
		sql.append(" and promotion_program_code = sod.join_program_code");
		sql.append(" )");
		
		List<PromotionProgram> lst = repo.getListBySQL(PromotionProgram.class, sql.toString(), params);
		return lst;
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public boolean checkPromotionProgramHasPortion(long promotionId, long shopId, long customerId, long staffId, Date lockDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select nvl(psm.quantity_max, nvl(decode(psm.is_quantity_max_edit, 1, 1, null), nvl(pcm.quantity_max, nvl(pstm.quantity_max, -1))))");
		sql.append(" from promotion_shop_map psm");
		sql.append(" left join promotion_customer_map pcm on (");
		sql.append(" pcm.promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and pcm.customer_id = ? and pcm.status = ?");
		params.add(customerId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" )");
		sql.append(" left join promotion_staff_map pstm on (");
		sql.append(" pstm.promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and pstm.staff_id = ? and pstm.status = ?");
		params.add(staffId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" )");
		sql.append(" where promotion_program_id = ? and psm.status = ?");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		if (lockDate == null) {
			sql.append(" and psm.from_date < trunc(sysdate) + 1 and (psm.to_date is null or psm.to_date >= trunc(sysdate))");
		} else {
			sql.append(" and psm.from_date < trunc(?) + 1 and (psm.to_date is null or psm.to_date >= trunc(?))");
			params.add(lockDate);
			params.add(lockDate);
		}
		sql.append(" and psm.shop_id in (select shop_id from shop where status = ? start with shop_id = ? connect by prior parent_shop_id = shop_id)");
		params.add(ActiveType.RUNNING.getValue());
		params.add(shopId);
		
		Object o = repo.getObjectByQuery(sql.toString(), params);
		
		if (o != null) {
			if (o instanceof BigDecimal && ((BigDecimal)o).signum() >= 0) {
				return true;
			}
			
			Integer n = Integer.valueOf(o.toString());
			if (n >= 0) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public Integer getQuantityRemainOfPromotionProgram(long promotionId, long shopId, long staffId, long customerId,
			Date lockDate) throws DataAccessException {
		if (lockDate == null) {
			lockDate = commonDAO.getSysDate();
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select least(nvl(remain, nvl(remain1, remain2)), nvl(remain1, nvl(remain, remain2)),");
		sql.append(" nvl(remain2, nvl(remain, remain1))) as remain");
		sql.append(" from (");
		sql.append(" select (case when quantity_max is not null then nvl(quantity_max, 0) - nvl(quantity_received_total, 0)");
		sql.append(" else null end) as remain,");
		sql.append(" (select quantity_max - nvl(quantity_received_total, 0) from promotion_customer_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and customer_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(customerId);
		sql.append(" and quantity_max is not null) as remain1,");
		sql.append(" (select quantity_max - nvl(quantity_received_total, 0) from promotion_staff_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and staff_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(staffId);
		sql.append(" and quantity_max is not null) as remain2");
		sql.append(" from promotion_shop_map psm");
		sql.append(" where promotion_program_id = ?");
		params.add(promotionId);
		sql.append(" and shop_id = ? and status = ? and from_date < trunc(?)+1");
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		params.add(lockDate);
		sql.append(" and (to_date is null or to_date >= trunc(?))");
		params.add(lockDate);
		sql.append(" )");
		
		Object obj = repo.getObjectByQuery(sql.toString(), params);
		
		if (obj == null) {
			return null;
		}
		
		if (obj instanceof BigDecimal) {
			return ((BigDecimal) obj).intValue();
		}
		
		return Integer.valueOf(obj.toString());
	}
	
	@Override
	public BigDecimal getAmountRemainOfPromotionProgram(String promotionCode, long shopId, long staffId, long customerId,
			Date lockDate) throws DataAccessException {
		if (lockDate == null) {
			lockDate = commonDAO.getSysDate();
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select least(nvl(remain, nvl(remain1, remain2)), nvl(remain1, nvl(remain, remain2)),");
		sql.append(" nvl(remain2, nvl(remain, remain1))) as remain");
		sql.append(" from (");
		sql.append(" select (case when amount_max is not null then nvl(amount_max, 0) - nvl(amount_received_total, 0)");
		sql.append(" else null end) as remain,");
		sql.append(" (select amount_max - nvl(amount_received_total, 0) from promotion_customer_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and customer_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(customerId);
		sql.append(" and amount_max is not null) as remain1,");
		sql.append(" (select amount_max - nvl(amount_received_total, 0) from promotion_staff_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and staff_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(staffId);
		sql.append(" and amount_max is not null) as remain2");
		sql.append(" from promotion_shop_map psm");
		sql.append(" join promotion_program pp on psm.promotion_program_id = pp.promotion_program_id");
		sql.append(" where pp.promotion_program_code = ?");
		params.add(promotionCode);
		sql.append(" and psm.shop_id = ? and psm.status = ? and psm.from_date < trunc(?)+1");
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		params.add(lockDate);
		sql.append(" and (psm.to_date is null or psm.to_date >= trunc(?))");
		params.add(lockDate);
		sql.append(" )");
		
		Object obj = repo.getObjectByQuery(sql.toString(), params);
		
		if (obj == null) {
			return null;
		}
		
		return (BigDecimal) obj;
	}
	
	@Override
	public BigDecimal getAmountRemainApprovedOfPromotionProgram(String promotionCode, long shopId, long staffId, long customerId,
			Date lockDate) throws DataAccessException {
		if (lockDate == null) {
			lockDate = commonDAO.getSysDate();
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select least(nvl(remain, nvl(remain1, remain2)), nvl(remain1, nvl(remain, remain2)),");
		sql.append(" nvl(remain2, nvl(remain, remain1))) as remain");
		sql.append(" from (");
		sql.append(" select (case when amount_max is not null then nvl(amount_max, 0) - nvl(amount_received, 0)");
		sql.append(" else null end) as remain,");
		sql.append(" (select amount_max - nvl(amount_received_total, 0) from promotion_customer_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and customer_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(customerId);
		sql.append(" and amount_max is not null) as remain1,");
		sql.append(" (select amount_max - nvl(amount_received_total, 0) from promotion_staff_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and staff_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(staffId);
		sql.append(" and amount_max is not null) as remain2");
		sql.append(" from promotion_shop_map psm");
		sql.append(" join promotion_program pp on psm.promotion_program_id = pp.promotion_program_id");
		sql.append(" where pp.promotion_program_code = ?");
		params.add(promotionCode);
		sql.append(" and psm.shop_id = ? and psm.status = ? and psm.from_date < trunc(?)+1");
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		params.add(lockDate);
		sql.append(" and (psm.to_date is null or psm.to_date >= trunc(?))");
		params.add(lockDate);
		sql.append(" )");
		
		Object obj = repo.getObjectByQuery(sql.toString(), params);
		
		if (obj == null) {
			return null;
		}
		
		return (BigDecimal) obj;
	}

	
	@Override
	public Integer getNumRemainOfPromotionProgram(String promotionCode, long shopId, long staffId, long customerId,
			Date lockDate) throws DataAccessException {
		if (lockDate == null) {
			lockDate = commonDAO.getSysDate();
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select least(nvl(remain, nvl(remain1, remain2)), nvl(remain1, nvl(remain, remain2)),");
		sql.append(" nvl(remain2, nvl(remain, remain1))) as remain");
		sql.append(" from (");
		sql.append(" select (case when num_max is not null then nvl(num_max, 0) - nvl(num_received_total, 0)");
		sql.append(" else null end) as remain,");
		sql.append(" (select num_max - nvl(num_received_total, 0) from promotion_customer_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and customer_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(customerId);
		sql.append(" and num_max is not null) as remain1,");
		sql.append(" (select num_max - nvl(num_received_total, 0) from promotion_staff_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and staff_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(staffId);
		sql.append(" and num_max is not null) as remain2");
		sql.append(" from promotion_shop_map psm");
		sql.append(" join promotion_program pp on psm.promotion_program_id = pp.promotion_program_id");
		sql.append(" join ( ");
		sql.append(" 	select * ");
		sql.append(" 	from shop start with shop_id = ? ");
		params.add(shopId);
		sql.append(" 	connect by prior parent_shop_id = shop_id ");
		sql.append(" ) s on psm.shop_id = s.shop_id ");
		sql.append(" where pp.promotion_program_code = ?");
		params.add(promotionCode);
		sql.append(" and psm.status = ? and psm.from_date < trunc(?)+1");
		params.add(ActiveType.RUNNING.getValue());
		params.add(lockDate);
		sql.append(" and (psm.to_date is null or psm.to_date >= trunc(?))");
		params.add(lockDate);
		sql.append(" )");
		
		Object obj = repo.getObjectByQuery(sql.toString(), params);
		
		if (obj == null) {
			return null;
		}
		
		if (obj instanceof BigDecimal) {
			return ((BigDecimal) obj).intValue();
		}
		
		return Integer.valueOf(obj.toString());
	}
	
	
	@Override
	public Integer getNumRemainApprovedOfPromotionProgram(String promotionCode, long shopId, long staffId, long customerId,
			Date lockDate) throws DataAccessException {
		if (lockDate == null) {
			lockDate = commonDAO.getSysDate();
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select least(nvl(remain, nvl(remain1, remain2)), nvl(remain1, nvl(remain, remain2)),");
		sql.append(" nvl(remain2, nvl(remain, remain1))) as remain");
		sql.append(" from (");
		sql.append(" select (case when num_max is not null then nvl(num_max, 0) - nvl(num_received, 0)");
		sql.append(" else null end) as remain,");
		sql.append(" (select num_max - nvl(num_received_total, 0) from promotion_customer_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and customer_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(customerId);
		sql.append(" and num_max is not null) as remain1,");
		sql.append(" (select num_max - nvl(num_received_total, 0) from promotion_staff_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and staff_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(staffId);
		sql.append(" and num_max is not null) as remain2");
		sql.append(" from promotion_shop_map psm");
		sql.append(" join promotion_program pp on psm.promotion_program_id = pp.promotion_program_id");
		sql.append(" join ( ");
		sql.append(" 	select * ");
		sql.append(" 	from shop start with shop_id = ? ");
		params.add(shopId);
		sql.append(" 	connect by prior parent_shop_id = shop_id ");
		sql.append(" ) s on psm.shop_id = s.shop_id ");
		sql.append(" where pp.promotion_program_code = ?");
		params.add(promotionCode);
		sql.append(" and psm.status = ? and psm.from_date < trunc(?)+1");
		params.add(ActiveType.RUNNING.getValue());
		params.add(lockDate);
		sql.append(" and (psm.to_date is null or psm.to_date >= trunc(?))");
		params.add(lockDate);
		sql.append(" )");
		
		Object obj = repo.getObjectByQuery(sql.toString(), params);
		
		if (obj == null) {
			return null;
		}
		
		if (obj instanceof BigDecimal) {
			return ((BigDecimal) obj).intValue();
		}
		
		return Integer.valueOf(obj.toString());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public SaleOrderPromotionRemainVO<Integer> getQuantityRemainOfPromotionProgramAndType(long promotionId, long shopId, long staffId, long customerId,			Date lockDate) throws DataAccessException {
		if (lockDate == null) {
			lockDate = commonDAO.getSysDate();
		}
		SaleOrderPromotionRemainVO<Integer> saleOrderPromotionRemainVO = null;
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH shopTmp AS ( ");
		sql.append(" SELECT shop_id ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status               = 1 ");
		sql.append(" START WITH shop_id       = ? ");
		params.add(shopId);
		sql.append(" CONNECT BY prior parent_shop_id = shop_id ");
		sql.append(" ) ");
		sql.append(" select (case when quantity_max is not null then nvl(quantity_max, 0) - nvl(quantity_received_total, 0)");
		sql.append(" else null end) as remain,");
		sql.append(" (select quantity_max - nvl(quantity_received_total, 0) from promotion_customer_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and customer_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(customerId);
		sql.append(" and quantity_max is not null) as remain1,");
		sql.append(" (select quantity_max - nvl(quantity_received_total, 0) from promotion_staff_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and staff_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(staffId);
		sql.append(" and quantity_max is not null) as remain2");
		sql.append(" from promotion_shop_map psm");
		sql.append(" where promotion_program_id = ?");
		params.add(promotionId);
		sql.append(" and shop_id IN (SELECT shop_id FROM shopTmp) and status = ? and from_date < trunc(?)+1");
		params.add(ActiveType.RUNNING.getValue());
		params.add(lockDate);
		sql.append(" and (to_date is null or to_date >= trunc(?))");
		params.add(lockDate);
		final String[] fieldNames = new String[] { "remain", "remain1", "remain2"};
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER};
		List<SaleOrderPromotionRemainVO> lst = repo.getListByQueryAndScalar(SaleOrderPromotionRemainVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (lst != null && !lst.isEmpty()) {
			saleOrderPromotionRemainVO = lst.get(0);
			Integer remain = saleOrderPromotionRemainVO.getRemain();
			Integer remain1 = saleOrderPromotionRemainVO.getRemain1();
			Integer remain2 = saleOrderPromotionRemainVO.getRemain2();
			Integer type = null;
			Integer min = null;
			if (remain != null) {
				type = Constant.SALE_ORDER_PROMOTION_OBJECT_SHOP;
				min = remain;
			}
			if (remain1 != null && (min == null || remain1.intValue() < min.intValue())) {
				type = Constant.SALE_ORDER_PROMOTION_OBJECT_CUSTOMER;
				min = remain1;
			}
			if (remain2 != null && (min == null || remain2.intValue() < min.intValue())) {
				type = Constant.SALE_ORDER_PROMOTION_OBJECT_STAFF;
				min = remain2;
			}
			saleOrderPromotionRemainVO.setRemain(min);
			saleOrderPromotionRemainVO.setTypeObject(type);
		}
		return saleOrderPromotionRemainVO;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public SaleOrderPromotionRemainVO<BigDecimal> getAmountRemainOfPromotionProgramAndType(String promotionCode, long shopId, long staffId, long customerId,
			Date lockDate) throws DataAccessException {
		if (lockDate == null) {
			lockDate = commonDAO.getSysDate();
		}
		SaleOrderPromotionRemainVO<BigDecimal> saleOrderPromotionRemainVO = null;
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH shopTmp AS ( ");
		sql.append(" SELECT shop_id ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status               = 1 ");
		sql.append(" START WITH shop_id       = ? ");
		params.add(shopId);
		sql.append(" CONNECT BY prior parent_shop_id = shop_id ");
		sql.append(" ) ");
		sql.append(" select (case when amount_max is not null then nvl(amount_max, 0) - nvl(amount_received_total, 0)");
		sql.append(" else null end) as remain,");
		sql.append(" (select amount_max - nvl(amount_received_total, 0) from promotion_customer_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and customer_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(customerId);
		sql.append(" and amount_max is not null) as remain1,");
		sql.append(" (select amount_max - nvl(amount_received_total, 0) from promotion_staff_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and staff_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(staffId);
		sql.append(" and amount_max is not null) as remain2");
		sql.append(" from promotion_shop_map psm");
		sql.append(" join promotion_program pp on psm.promotion_program_id = pp.promotion_program_id");
		sql.append(" where pp.promotion_program_code = ?");
		params.add(promotionCode);
		sql.append(" and psm.shop_id IN (SELECT shop_id FROM shopTmp) and psm.status = ? and psm.from_date < trunc(?)+1");
		params.add(ActiveType.RUNNING.getValue());
		params.add(lockDate);
		sql.append(" and (psm.to_date is null or psm.to_date >= trunc(?))");
		params.add(lockDate);
		final String[] fieldNames = new String[] { "remain", "remain1", "remain2"};
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL};
		List<SaleOrderPromotionRemainVO> lst = repo.getListByQueryAndScalar(SaleOrderPromotionRemainVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (lst != null && !lst.isEmpty()) {
			saleOrderPromotionRemainVO = lst.get(0);
			BigDecimal remain = saleOrderPromotionRemainVO.getRemain();
			BigDecimal remain1 = saleOrderPromotionRemainVO.getRemain1();
			BigDecimal remain2 = saleOrderPromotionRemainVO.getRemain2();
			Integer type = null;
			BigDecimal min = null;
			if (remain != null) {
				type = Constant.SALE_ORDER_PROMOTION_OBJECT_SHOP;
				min = remain;
			}
			if (remain1 != null && (min == null || remain1.compareTo(min) < 0)) {
				type = Constant.SALE_ORDER_PROMOTION_OBJECT_CUSTOMER;
				min = remain1;
			}
			if (remain2 != null && (min == null || remain2.compareTo(min) < 0)) {
				type = Constant.SALE_ORDER_PROMOTION_OBJECT_STAFF;
				min = remain2;
			}
			saleOrderPromotionRemainVO.setRemain(min);
			saleOrderPromotionRemainVO.setTypeObject(type);
		}
		return saleOrderPromotionRemainVO;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public SaleOrderPromotionRemainVO<Integer> getNumRemainOfPromotionProgramAndType(String promotionCode, long shopId, long staffId, long customerId,
			Date lockDate) throws DataAccessException {
		if (lockDate == null) {
			lockDate = commonDAO.getSysDate();
		}
		SaleOrderPromotionRemainVO<Integer> saleOrderPromotionRemainVO = null;
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH shopTmp AS ( ");
		sql.append(" SELECT shop_id ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status               = 1 ");
		sql.append(" START WITH shop_id       = ? ");
		params.add(shopId);
		sql.append(" CONNECT BY prior parent_shop_id = shop_id ");
		sql.append(" ) ");
		sql.append(" select (case when num_max is not null then nvl(num_max, 0) - nvl(num_received_total, 0)");
		sql.append(" else null end) as remain,");
		sql.append(" (select num_max - nvl(num_received_total, 0) from promotion_customer_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and customer_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(customerId);
		sql.append(" and num_max is not null) as remain1,");
		sql.append(" (select num_max - nvl(num_received_total, 0) from promotion_staff_map");
		sql.append(" where promotion_shop_map_id = psm.promotion_shop_map_id");
		sql.append(" and status = ? and staff_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(staffId);
		sql.append(" and num_max is not null) as remain2");
		sql.append(" from promotion_shop_map psm");
		sql.append(" join promotion_program pp on psm.promotion_program_id = pp.promotion_program_id");
		sql.append(" where pp.promotion_program_code = ?");
		params.add(promotionCode);
		sql.append(" and psm.shop_id IN (SELECT shop_id FROM shopTmp) and psm.status = ? and psm.from_date < trunc(?)+1");
		params.add(ActiveType.RUNNING.getValue());
		params.add(lockDate);
		sql.append(" and (psm.to_date is null or psm.to_date >= trunc(?))");
		params.add(lockDate);
		final String[] fieldNames = new String[] { "remain", "remain1", "remain2"};
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER};
		List<SaleOrderPromotionRemainVO> lst = repo.getListByQueryAndScalar(SaleOrderPromotionRemainVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (lst != null && !lst.isEmpty()) {
			saleOrderPromotionRemainVO = lst.get(0);
			Integer remain = saleOrderPromotionRemainVO.getRemain();
			Integer remain1 = saleOrderPromotionRemainVO.getRemain1();
			Integer remain2 = saleOrderPromotionRemainVO.getRemain2();
			Integer type = null;
			Integer min = null;
			if (remain != null) {
				type = Constant.SALE_ORDER_PROMOTION_OBJECT_SHOP;
				min = remain;
			}
			if (remain1 != null && (min == null || remain1.intValue() < min.intValue())) {
				type = Constant.SALE_ORDER_PROMOTION_OBJECT_CUSTOMER;
				min = remain1;
			}
			if (remain2 != null && (min == null || remain2.intValue() < min.intValue())) {
				type = Constant.SALE_ORDER_PROMOTION_OBJECT_STAFF;
				min = remain2;
			}
			saleOrderPromotionRemainVO.setRemain(min);
			saleOrderPromotionRemainVO.setTypeObject(type);
		}
		return saleOrderPromotionRemainVO;
	}
	
	/**
	 * @author tungmt
	 */
	@Override
	public List<ProgrameExtentVO> getIdAllTableProgram(long promotionId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" with lstProductGroup as ( ");
		sql.append(" 	    select product_group_id, to_char(update_date,'dd/MM/yyyy HH24:mi:ss') update_date from product_group where promotion_program_id = ? ");
		params.add(promotionId);
		sql.append(" 	    and status = 1 order by product_group_id ");
		sql.append(" 	), ");
		sql.append(" 	lstGroupLevel as ( ");
		sql.append(" 	    select group_level_id, to_char(update_date,'dd/MM/yyyy HH24:mi:ss') update_date from group_level where product_group_id in (select product_group_id from lstProductGroup) ");
		sql.append(" 	    and status = 1 order by group_level_id ");
		sql.append(" 	), ");
		sql.append(" 	lstGroupLevelDetail as ( ");
		sql.append(" 	    select group_level_detail_id, to_char(update_date,'dd/MM/yyyy HH24:mi:ss') update_date from group_level_detail where group_level_id in (select group_level_id from lstGroupLevel) ");
		sql.append(" 	    and status = 1 order by group_level_detail_id ");
		sql.append(" 	), ");
		sql.append(" 	lstGroupMap as ( ");
		sql.append(" 	    select group_mapping_id, to_char(update_date,'dd/MM/yyyy HH24:mi:ss') update_date from group_mapping where (sale_group_id in (select product_group_id from lstProductGroup) ");
		sql.append(" 	          or promo_group_id in (select product_group_id from lstProductGroup)) ");
		sql.append(" 	    and status = 1 order by group_mapping_id ");
		sql.append(" 	) ");
		sql.append(" 	select product_group_id id, update_date updateDate from lstProductGroup ");
		sql.append(" 	union all ");
		sql.append(" 	select group_level_id id, update_date updateDate from lstGroupLevel ");
		sql.append(" 	union all ");
		sql.append(" 	select group_level_detail_id id, update_date updateDate from lstGroupLevelDetail ");
		sql.append(" 	union all ");
		sql.append(" 	select group_mapping_id id, update_date updateDate from lstGroupMap ");
		
		String[] fieldNames = { "id", "updateDate" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING };
		
		List<ProgrameExtentVO> lst = repo.getListByQueryAndScalar(ProgrameExtentVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
	
	@Override
	public List<GroupLevelDetail> getListDuplicateLevelDetail(long productGroupId, String lstProductIds, String lstValues, Long exceptGroupLevelId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("with lstProductIdTmp as (");
		sql.append(" select trim(regexp_substr(?, '[^,]+', 1, level)) as productId, rownum as r_num");
		params.add(lstProductIds);
		sql.append(" from dual");
		sql.append(" connect by regexp_substr(?, '[^,]+', 1, level) is not null");
		params.add(lstProductIds);
		sql.append("),");
		sql.append(" lstValueTmp as (");
		sql.append(" select trim(regexp_substr(?, '[^,]+', 1, level)) as quantity, rownum as r_num");
		params.add(lstValues);
		sql.append(" from dual");
		sql.append(" connect by regexp_substr(?, '[^,]+', 1, level) is not null");
		params.add(lstValues);
		sql.append(")");
		sql.append(" select *");
		sql.append(" from group_level_detail gld");
		sql.append(" join lstProductIdTmp p on (p.productId = gld.product_id)");
		sql.append(" join lstValueTmp v on (v.r_num = p.r_num and gld.value = v.quantity)");
		sql.append(" where group_level_id in (");
		sql.append(" select group_level_id");
		sql.append(" from group_level");
		sql.append(" where product_group_id = ?");
		params.add(productGroupId);
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(")");
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());

		if (exceptGroupLevelId != null) {
			sql.append(" and group_level_id <> ?");
			params.add(exceptGroupLevelId);
		}

		List<GroupLevelDetail> lst = repo.getListBySQL(GroupLevelDetail.class, sql.toString(), params);
		return lst;
	}
	
	@Override
	public List<GroupLevel> getListDuplicateLevelWithMinValue(long productGroupId, Integer minQuantity, BigDecimal minAmount, Long exceptGroupLevelId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select *");
		sql.append(" from group_level");
		sql.append(" where product_group_id = ?");
		params.add(productGroupId);
		if (minQuantity != null) {
			sql.append(" and min_quantity = ?");
			params.add(minQuantity);
		}
		if (minAmount != null) {
			sql.append(" and min_amount = ?");
			params.add(minAmount);
		}
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());

		if (exceptGroupLevelId != null) {
			sql.append(" and group_level_id <> ?");
			params.add(exceptGroupLevelId);
		}

		List<GroupLevel> lst = repo.getListBySQL(GroupLevel.class, sql.toString(), params);
		return lst;
	}
	
	@Override
	public List<Product> getListDuplicateProductInProgram(long promotionId, long curProductGroupId, String lstProductId) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(lstProductId)) {
			return null;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select *");
		sql.append(" from product");
		sql.append(" where product_id in (");
		sql.append(" select product_id");
		sql.append(" from group_level_detail");
		sql.append(" where group_level_id in (");
		sql.append(" select group_level_id from group_level");
		sql.append(" where product_group_id in (");
		sql.append(" select product_group_id");
		sql.append(" from product_group");
		sql.append(" where promotion_program_id = ? and group_type = ?");
		params.add(promotionId);
		params.add(ProductGroupType.MUA.getValue());
		sql.append(" AND status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and product_group_id <> ?");
		params.add(curProductGroupId);
		sql.append(")");
		sql.append(" and parent_group_level_id is not null");
		sql.append(" AND status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(")");
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" intersect");
		sql.append(" select to_number(trim(regexp_substr(?, '[^,]+', 1, level))) as product_id");
		params.add(lstProductId);
		sql.append(" from dual");
		sql.append(" connect by regexp_substr(?, '[^,]+', 1, level) is not null");
		params.add(lstProductId);
		sql.append(")");

		List<Product> lst = repo.getListBySQL(Product.class, sql.toString(), params);
		return lst;
	}
	
	@Override
	public void updateGroupLevelOrderNumber(long promotionId, long productGroupId, LogInfoVO logInfo) throws DataAccessException {
		List<SpParam> inParams = new ArrayList<SpParam>();
		List<SpParam> outParams = new ArrayList<SpParam>();

		int idx = 1;
		inParams.add(new SpParam(idx++, java.sql.Types.NUMERIC, promotionId));
		inParams.add(new SpParam(idx++, java.sql.Types.NUMERIC, productGroupId));
		repo.executeSP("updateGroupLevelOrderNumber", inParams, outParams);
	}
	
	@Override
	public List<PromotionProgramVO> getListVOPromotionProgramByShop(Integer status, Long shopId, Long staffRootId, Long roleId, Long shopRootId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (staffRootId == null) {
			throw new IllegalArgumentException("staffRootId is null");
		}
		if (roleId == null) {
			throw new IllegalArgumentException("roleId is null");
		}
		if (shopRootId == null) {
			throw new IllegalArgumentException("shopRootId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with tmpshop as ( ");
		sql.append(" select ogr.number_key_id from table (f_get_list_child_shop_inherit(?, sysdate, ?, ?)) ogr ");
		sql.append(" where ogr.number_key_id = ? ");
		sql.append(" ) ");
		params.add(staffRootId);
		params.add(roleId);
		params.add(shopRootId);
		params.add(shopId);
		sql.append("select DISTINCT * from (   ");
		sql.append(" select  ");
		sql.append(" pp.PROMOTION_PROGRAM_ID as id ");
		sql.append(" , pp.PROMOTION_PROGRAM_CODE as promotionProgramCode ");
		sql.append(" , pp.PROMOTION_PROGRAM_NAME as promotionProgramName ");
		sql.append(" , pp.TYPE as type ");
		sql.append(" , pp.from_date as fDate ");
		sql.append(" , pp.to_date as tDate ");
		sql.append(" from promotion_program pp ");
		sql.append(" join ( ");
		sql.append("   select psm.PROMOTION_PROGRAM_ID , psm.shop_id, psm.status ");
		sql.append("   from promotion_shop_map psm ");
		sql.append("   join ( ");
		sql.append("     select shop_id, status from shop ");
		sql.append("     start WITH shop_id in (select number_key_id from tmpshop) connect by prior shop_id = parent_shop_id ");
		sql.append("     union ");
		sql.append("     select shop_id, status from shop ");
		sql.append("     start WITH shop_id in (select number_key_id from tmpshop) connect by shop_id = prior parent_shop_id ");
		sql.append("   ) rsh on psm.shop_id = rsh.shop_id and rsh.status in (?, ?) ");
		params.add(ActiveType.STOPPED.getValue());
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ) r2 on pp.promotion_program_id = r2.promotion_program_id ");
		/*sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on r2.shop_id = ogr.number_key_id ");
		params.add(staffRootId);
		params.add(roleId);
		params.add(shopRootId);*/
		sql.append(" where r2.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		if (status != null) {
			sql.append(" and pp.status = ? ");
			params.add(status);
		} else {
			sql.append(" and status in (?, ?) ");
			params.add(ActiveType.STOPPED.getValue());
			params.add(ActiveType.RUNNING.getValue());
		}
		sql.append(" ) ");
		sql.append(" order by fDate desc, tDate desc, promotionProgramCode asc ");
		final String[] fieldNames = new String[] { "id", "promotionProgramCode", "promotionProgramName", "type" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(PromotionProgramVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public PromotionShopMap getPromotionShopMapFilter(PromotionMapBasicFilter<PromotionShopMap> filter) throws DataAccessException {
		if (filter == null || filter.getId() == null) {
			throw new IllegalArgumentException("filter is null && filter.getId() is null");
		}
		if (filter.getOrderDate() == null) {
			throw new IllegalArgumentException("filter.getOrderDate() is null");
		}
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("filter.getShopId() is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * ");
		sql.append(" from promotion_shop_map psm ");
		sql.append(" WHERE psm.promotion_program_id = ? ");
		params.add(filter.getId());
		sql.append(" AND status = 1 ");
		sql.append(" AND (psm.shop_id IN ");
		sql.append(" (SELECT shop_id ");
		sql.append(" FROM shop ");
		sql.append(" where status = 1 ");
		sql.append(" START WITH shop_id = ? ");
		sql.append(" CONNECT BY prior parent_shop_id = shop_id ");
		sql.append(" ) ) ");
		params.add(filter.getShopId());
		sql.append(" AND psm.from_date < TRUNC(?) + 1 ");
		sql.append(" and (psm.to_date >= trunc(?) ");
		sql.append(" or psm.to_date is null) ");
		params.add(filter.getOrderDate());
		params.add(filter.getOrderDate());
		sql.append(" and ( (quantity_max is null ");
		sql.append(" OR NVL(quantity_received_total, 0) < quantity_max) ) ");
		List<PromotionShopMap> lst = repo.getListBySQL(PromotionShopMap.class, sql.toString(), params);
		if (lst != null && !lst.isEmpty()) {
			return lst.get(0);
		}
		return null;
	}
	
	@Override
	public PromotionStaffMap getPromotionStaffMapFilter(PromotionMapBasicFilter<PromotionStaffMap> filter) throws DataAccessException {
		if (filter == null || filter.getId() == null) {
			throw new IllegalArgumentException("filter is null && filter.getId() is null");
		}
		if (filter.getOrderDate() == null) {
			throw new IllegalArgumentException("filter.getOrderDate() is null");
		}
		if (filter.getShopId() == null || filter.getStaffId() == null) {
			throw new IllegalArgumentException("filter.getShopId() is null || filter.getStaffId() is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with pShopMap as ( SELECT psm.promotion_shop_map_id ");
		sql.append(" from promotion_shop_map psm ");
		sql.append(" where psm.promotion_program_id = ? ");
		params.add(filter.getId());
		sql.append(" AND status = 1 ");
		sql.append(" AND (psm.shop_id IN ");
		sql.append(" (SELECT shop_id ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status = 1 ");
		sql.append(" START WITH shop_id = ? ");
		sql.append(" CONNECT BY prior parent_shop_id = shop_id ");
		sql.append(" ) ) ");
		params.add(filter.getShopId());
		sql.append(" AND psm.from_date < TRUNC(?) + 1 ");
		sql.append(" and (psm.to_date >= trunc(?) ");
		sql.append(" or psm.to_date is null) ");
		params.add(filter.getOrderDate());
		params.add(filter.getOrderDate());
		sql.append(" and ( (quantity_max is null ");
		sql.append(" or nvl(quantity_received_total, 0) < quantity_max) ) ");
		sql.append(" ) ");
		sql.append(" SELECT * ");
		sql.append(" from promotion_staff_map ");
		sql.append(" WHERE promotion_shop_map_id in (select promotion_shop_map_id from pShopMap where rownum = 1) ");
		sql.append(" AND status = 1 ");
		sql.append(" and staff_id = ? ");
		params.add(filter.getStaffId());
		sql.append(" AND ((quantity_max IS NULL ");
		sql.append(" or nvl(quantity_received_total, 0) < quantity_max)) ");
		List<PromotionStaffMap> lst = repo.getListBySQL(PromotionStaffMap.class, sql.toString(), params);
		if (lst != null && !lst.isEmpty()) {
			return lst.get(0);
		}
		return null;
	}
	
	@Override
	public PromotionCustomerMap getPromotionCustomerMapFilter(PromotionMapBasicFilter<PromotionCustomerMap> filter) throws DataAccessException {
		if (filter == null || filter.getId() == null) {
			throw new IllegalArgumentException("filter is null && filter.getId() is null");
		}
		if (filter.getOrderDate() == null) {
			throw new IllegalArgumentException("filter.getOrderDate() is null");
		}
		if (filter.getShopId() == null || filter.getCustomerId() == null) {
			throw new IllegalArgumentException("filter.getShopId() is null || filter.getCustomerId() is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with pShopMap as ( SELECT psm.promotion_shop_map_id ");
		sql.append(" from promotion_shop_map psm ");
		sql.append(" WHERE psm.promotion_program_id = ? ");
		params.add(filter.getId());
		sql.append(" and status = 1 ");
		sql.append(" AND (psm.shop_id IN ");
		sql.append(" (SELECT shop_id ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status = 1 ");
		sql.append(" START WITH shop_id = ? ");
		sql.append(" CONNECT BY prior parent_shop_id = shop_id ");
		sql.append(" ) ) ");
		params.add(filter.getShopId());
		sql.append(" AND psm.from_date < TRUNC(?) + 1 ");
		sql.append(" and (psm.to_date >= trunc(?) ");
		sql.append(" or psm.to_date is null) ");
		params.add(filter.getOrderDate());
		params.add(filter.getOrderDate());
		sql.append(" and ( (quantity_max is null ");
		sql.append(" or nvl(quantity_received_total, 0) < quantity_max) ) ");
		sql.append(" ) ");
		sql.append(" SELECT * ");
		sql.append(" from promotion_customer_map ");
		sql.append(" WHERE promotion_shop_map_id in (select promotion_shop_map_id from pShopMap where rownum = 1) ");
		sql.append(" AND status = 1 ");
		sql.append(" AND customer_id = ? ");
		params.add(filter.getCustomerId());
		sql.append(" AND ((quantity_max IS NULL ");
		sql.append(" or nvl(quantity_received_total, 0) < quantity_max)) ");
		List<PromotionCustomerMap> lst = repo.getListBySQL(PromotionCustomerMap.class, sql.toString(), params);
		if (lst != null && !lst.isEmpty()) {
			return lst.get(0);
		}
		return null;
	}
}

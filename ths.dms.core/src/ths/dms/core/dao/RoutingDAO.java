package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Routing;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

public interface RoutingDAO {

	Routing createRouting(Routing routing) throws DataAccessException;

	void deleteRouting(Routing routing) throws DataAccessException;

	void updateRouting(Routing routing) throws DataAccessException;
	
	Routing getRoutingById(Long id) throws DataAccessException;
	
	Routing getRoutingByCode(Long shopId, String code) throws DataAccessException;
	
	List<Routing> getListRouting(KPaging<Routing> kPaging, Long parentShopId, Long shopId, Long superId, String routingCode, String routingName) throws DataAccessException;

	/**
	*  Mo ta chuc nang cua ham
	*  @author: thanhnn
	*  @param superId
	*  @param routingCode
	*  @param routingName
	*  @return
	*  @return: List<Routing>
	*  @throws:
	*/
	List<Routing> getListRoutingBySuper(KPaging<Routing> kPaging, Long superId, String routingCode, String routingName) throws DataAccessException;

	Routing getRoutingMultiChoice(Long routingId, Long shopId, String routingCode, Integer status) throws DataAccessException;
	
}

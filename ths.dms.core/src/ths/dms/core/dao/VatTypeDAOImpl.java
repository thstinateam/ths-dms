package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.VatType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class VatTypeDAOImpl implements VatTypeDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public VatType createVatType(VatType vatType, LogInfoVO logInfo) throws DataAccessException {
		if (vatType == null) {
			throw new IllegalArgumentException("vatType");
		}
		if (vatType.getVatTypeCode() != null)
			vatType.setVatTypeCode(vatType.getVatTypeCode().toUpperCase());
		repo.create(vatType);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, vatType, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(VatType.class, vatType.getId());
	}

	@Override
	public void deleteVatType(VatType vatType, LogInfoVO logInfo) throws DataAccessException {
		if (vatType == null) {
			throw new IllegalArgumentException("vatType");
		}
		repo.delete(vatType);
		try {
			actionGeneralLogDAO.createActionGeneralLog(vatType, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public VatType getVatTypeById(long id) throws DataAccessException {
		return repo.getEntityById(VatType.class, id);
	}
	
	@Override
	public void updateVatType(VatType vatType, LogInfoVO logInfo) throws DataAccessException {
		if (vatType == null) {
			throw new IllegalArgumentException("vatType");
		}
		if (vatType.getVatTypeCode() != null)
			vatType.setVatTypeCode(vatType.getVatTypeCode().toUpperCase());
		VatType temp = this.getVatTypeById(vatType.getId());
		repo.update(vatType);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, vatType, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public VatType getVatTypeByCode(String code) throws DataAccessException{
		code = code.toUpperCase();
		String sql = "select * from VAT_TYPE where vat_type_code=? and status!=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		VatType rs = repo.getEntityBySQL(VatType.class, sql, params);
		return rs;
	}
	
	@Override
	public List<VatType> getListVatType(KPaging<VatType> kPaging, String vatTypeCode,
							String vatTypeName, String desc, ActiveType status, Float value) 
									throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from VAT_TYPE where 1 = 1");
		if (!StringUtility.isNullOrEmpty(vatTypeCode)) {
			sql.append(" and vat_type_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(vatTypeCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(vatTypeName)) {
			String temp = StringUtility.toOracleSearchLike(vatTypeName.toLowerCase());
			sql.append(" and lower(vat_type_name) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (!StringUtility.isNullOrEmpty(desc)) {
			String temp = StringUtility.toOracleSearchLike(desc.toLowerCase());
			sql.append(" and lower(description) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (value != null) {
			sql.append(" and value=?");
			params.add(value);
		}
		sql.append(" order by VAT_TYPE_CODE asc");
		if (kPaging == null)
			return repo.getListBySQL(VatType.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(VatType.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public Boolean isUsingByOthers(long vatTypeId) throws DataAccessException {
//		String sql = "select count(1) as count from Product where vat_type_id=? and status != ?";
//		List<Object> params = new ArrayList<Object>();
//		params.add(vatTypeId);
//		params.add(ActiveType.DELETED.getValue());
//		int c = repo.countBySQL(sql, params);
//		return c == 0? false : true;
		return false;
	}
}

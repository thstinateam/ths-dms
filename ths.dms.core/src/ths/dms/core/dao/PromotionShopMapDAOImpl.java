package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PromotionShopMapFilter;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PromotionShopMapVO;
import ths.dms.core.entities.vo.PromotionShopVO;
import ths.dms.core.entities.vo.QuantityVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class PromotionShopMapDAOImpl implements PromotionShopMapDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private PromotionProgramDAO promotionProgramDAO;

	@Autowired
	private ShopDAO shopDAO;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;

	@Override
	public PromotionShopMap createPromotionShopMap(PromotionShopMap promotionShopMap, LogInfoVO logInfo) throws DataAccessException {
		if (promotionShopMap == null) {
			throw new IllegalArgumentException("promotionShopMap");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		promotionShopMap.setCreateUser(logInfo.getStaffCode());
		try {
			repo.create(promotionShopMap);
			actionGeneralLogDAO.createActionGeneralLog(null, promotionShopMap, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(PromotionShopMap.class, promotionShopMap.getId());
	}

	@Override
	public PromotionShopMap createOnePromotionShopMap(PromotionShopMap promotionShopMap, LogInfoVO logInfo) throws DataAccessException {
		if (promotionShopMap == null) {
			throw new IllegalArgumentException("promotionShopMap");
		}
		Boolean b = this.checkExistChildPromotionShopMap(promotionShopMap.getShop().getId(), promotionShopMap.getPromotionProgram().getId(), false);
		if (b) {
			return null;
		} else {
			repo.create(promotionShopMap);
			List<PromotionShopMap> lst = this.getListParentPromotionShopMap(promotionShopMap.getShop().getId(), promotionShopMap.getPromotionProgram().getId());
			for (PromotionShopMap psm : lst) {
				psm.setStatus(ActiveType.DELETED);
				this.updatePromotionShopMap(psm, logInfo);
			}
			try {
				actionGeneralLogDAO.createActionGeneralLog(null, promotionShopMap, logInfo);
			} catch (Exception ex) {
				throw new DataAccessException(ex);
			}
			return repo.getEntityById(PromotionShopMap.class, promotionShopMap.getId());
		}
	}

	@Override
	public Boolean checkExistChildPromotionShopMap(Long shopId, Long promotionProgramId, Boolean exceptShopCheck) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) as count from promotion_shop_map psm where status = 1 and promotion_program_id = ?");
		params.add(promotionProgramId);
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("   and psm.from_date < TRUNC(sysdate) + 1  ");
		sql.append("   and (psm.to_date is null or psm.to_date  >= TRUNC(sysdate)) ");
		//end
		sql.append(" and psm.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
		if (Boolean.TRUE.equals(exceptShopCheck)) {
			sql.append(" and psm.shop_id != ?");
			params.add(shopId);
		}
		params.add(shopId);
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	private List<PromotionShopMap> getListParentPromotionShopMap(Long shopId, Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from promotion_shop_map psm where status = 1 and promotion_program_id = ? ");
		params.add(promotionProgramId);
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("   and psm.from_date < TRUNC(sysdate) + 1  ");
		sql.append("   and (psm.to_date  >= TRUNC(sysdate) or psm.to_date is null) ");
		//end
		sql.append(" and psm.shop_id != ? and psm.shop_id in (select shop_id from shop start with shop_id = ? connect by prior parent_shop_id = shop_id)");
		params.add(shopId);
		params.add(shopId);
		return repo.getListBySQL(PromotionShopMap.class, sql.toString(), params);
	}

	@Override
	public void updatePromotionShopMap(PromotionShopMap promotionShopMap, LogInfoVO logInfo) throws DataAccessException {
		if (promotionShopMap == null) {
			throw new IllegalArgumentException("promotionShopMap");
		}

		if (promotionShopMap.getShop() != null && promotionShopMap.getStatus() == ActiveType.RUNNING) {
			if (checkIfRecordExist(promotionShopMap.getPromotionProgram().getId(), promotionShopMap.getShop().getShopCode(), promotionShopMap.getId()))
				return;
		}
		PromotionShopMap temp = this.getPromotionShopMapById(promotionShopMap.getId());
		//for log purpose
		temp = temp.clone();
		promotionShopMap.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null) {
			promotionShopMap.setUpdateUser(logInfo.getStaffCode());
			if (logInfo.getIp() == null || StringUtility.isNullOrEmpty(logInfo.getStaffCode()))
				logInfo = null;
		}
		repo.update(promotionShopMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, promotionShopMap, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Boolean checkIfRecordExist(long promotionProgramId, String shopCode, Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(1) as count from promotion_shop_map p ");
		sql.append(" where promotion_program_id=? and exists (select 1 from shop s where p.shop_id=s.shop_id and shop_code=?) and status!=?");

		List<Object> params = new ArrayList<Object>();
		params.add(promotionProgramId);
		params.add(shopCode.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("   and p.from_date < TRUNC(sysdate) + 1  ");
		sql.append("   and (p.to_date  >= TRUNC(sysdate) or p.to_date is null) ");
		//end
		if (id != null) {
			sql.append(" and promotion_shop_map_id != ?");
			params.add(id);
		}

		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public void deletePromotionShopMap(PromotionShopMap promotionShopMap, LogInfoVO logInfo) throws DataAccessException {
		if (promotionShopMap == null) {
			throw new IllegalArgumentException("promotionShopMap");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(promotionShopMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(promotionShopMap, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public PromotionShopMap getPromotionShopMapById(long id) throws DataAccessException {
		return repo.getEntityById(PromotionShopMap.class, id);
	}

	@Override
	public PromotionShopMap getPromotionShopMap(Long shopId, Long promotionProgramId) throws DataAccessException {
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		//String sql = "select * from promotion_shop_map psm where shop_id = ? and promotion_program_id = ? and status != ? and psm.from_date < TRUNC(sysdate) + 1 and (psm.to_date  >= TRUNC(sysdate) or psm.to_date is null)";
		String sql = "select * from promotion_shop_map psm where shop_id = ? and promotion_program_id = ? and status <> ?";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);
		params.add(promotionProgramId);
		params.add(ActiveType.DELETED.getValue());
		return repo.getEntityBySQL(PromotionShopMap.class, sql, params);
	}
	
	@Override
	public boolean checkExistPromotionShopMapByListShop(String strListShopId, Long promotionProgramId) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder("select count(1) count from promotion_shop_map psm where promotion_program_id = ? and status <> ? ");
		params.add(promotionProgramId);
		params.add(ActiveType.DELETED.getValue());
		String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(strListShopId, "shop_id");
		sql.append(paramsFix);
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public List<PromotionShopMap> getListPromotionShopMap(KPaging<PromotionShopMap> kPaging, Long promotionProgramId, String shopCode, String shopName, Integer shopQuantity, ActiveType status, Long parentShopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from promotion_shop_map psm join shop s on psm.shop_id = s.shop_id where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		if (status != null) {
			sql.append(" and psm.status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and psm.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}

		if (promotionProgramId != null) {
			sql.append(" and psm.promotion_program_id=?");
			params.add(promotionProgramId);
		}

		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and s.shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and UNICODE2ENGLISH(s.shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shopName.toLowerCase()));
		}
		if (shopQuantity != null) {
			sql.append(" and psm.QUANTITY_MAX = ?");
			params.add(shopQuantity);
		}
		if (parentShopId != null) {
			sql.append(" and psm.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
			params.add(parentShopId);
		}
		sql.append(" order by s.shop_code");
		if (kPaging == null)
			return repo.getListBySQL(PromotionShopMap.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(PromotionShopMap.class, sql.toString(), params, kPaging);
	}

	@Override
	public PromotionShopMap checkPromotionShopMap(Long promotionProgramId, String shopCode, ActiveType status, Date lockDay) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from promotion_shop_map psm where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and status != ?");
			params.add(ActiveType.DELETED.getValue());
		}

		if (promotionProgramId != null) {
			sql.append(" and promotion_program_id=?");
			params.add(promotionProgramId);
		} else
			sql.append(" and promotion_program_id is null");

		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and psm.shop_id in (select shop_id from shop s where status != -1 start with s.shop_code = ? connect by prior parent_shop_id = shop_id)");
			params.add(shopCode.toUpperCase());
		}
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("     AND psm.from_date < TRUNC(?) + 1  ");
		sql.append("     AND (psm.to_date    >= TRUNC(?) or psm.to_date is null) ");
		params.add(lockDay);
		params.add(lockDay);
		//end
		return repo.getEntityBySQL(PromotionShopMap.class, sql.toString(), params);
	}

	@Override
	public Boolean checkCTHMShopMap(String promotionProgramCode, Long shopId, ActiveType status, Date lockDay) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select psm.PROMOTION_SHOP_MAP_ID from promotion_shop_map psm inner join promotion_program pp on psm.PROMOTION_PROGRAM_ID = pp.PROMOTION_PROGRAM_ID where 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		if (status != null) {
			sql.append(" and psm.status = ?");
			params.add(status.getValue());
		} else {
			sql.append(" and psm.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}

		if (promotionProgramCode != null) {
			sql.append(" and pp.PROMOTION_PROGRAM_CODE = ?");
			params.add(promotionProgramCode);
		}

		if (shopId != null) {
			sql.append(" and psm.shop_id in (select shop_id from shop s where status != -1 start with s.shop_id = ? connect by prior parent_shop_id = shop_id)");
			params.add(shopId);
		}
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("     AND psm.from_date < TRUNC(?) + 1  ");
		sql.append("     AND (psm.to_date    >= TRUNC(?) or psm.to_date is null) ");
		params.add(lockDay);
		params.add(lockDay);
		sql.append(" UNION ALL ");
		sql.append("select dpsm.DISPLAY_PRO_SHOP_MAP_VNM_ID from DISPLAY_PRO_SHOP_MAP_VNM dpsm inner join display_program_vnm dp on dpsm.DISPLAY_PROGRAM_ID = dp.DISPLAY_PROGRAM_VNM_ID where 1 = 1 ");
		if (status != null) {
			sql.append(" and dpsm.STATUS = ?");
			params.add(status.getValue());
		} else {
			sql.append(" and dpsm.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}

		if (promotionProgramCode != null) {
			sql.append(" and dp.DISPLAY_PROGRAM_CODE = ?");
			params.add(promotionProgramCode);
		}
		if (shopId != null) {
			sql.append(" and dpsm.shop_id IN (select shop_id from shop s where status != -1 start with s.shop_id = ? connect by prior parent_shop_id = shop_id)");
			params.add(shopId);
		}
		sql.append(" and dpsm.from_date < trunc(?) + 1 ");
		sql.append(" and (dpsm.to_date >= add_months(trunc(?, 'MM'), -2) or dpsm.to_date is null) ");
		params.add(lockDay);
		params.add(lockDay);
		//end
		Object obj = repo.getObjectByQuery(sql.toString(), params);
		return obj != null ? true : false;
	}

	@Override
	public List<PromotionShopMap> getListPromotionShopMapForUpdate(KPaging<PromotionShopMap> kPaging, Long promotionProgramId, String shopCode, String shopName, Integer shopQuantity, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from promotion_shop_map psm where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		}
		if (promotionProgramId != null) {
			sql.append(" and promotion_program_id=?");
			params.add(promotionProgramId);
		} else
			sql.append(" and promotion_program_id is null");

		if (shopQuantity != null) {
			sql.append(" and QUANTITY_MAX=?");
			params.add(shopQuantity);
		}
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and psm.shop_id in (select shop_id from shop s where status != -1 start with s.shop_code = ? connect by prior parent_shop_id = shop_id)");
			params.add(shopCode.toUpperCase());
		}
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("     AND psm.from_date < TRUNC(sysdate) + 1  ");
		sql.append("     AND (psm.to_date    >= TRUNC(sysdate) or psm.to_date is null) ");
		//end
		sql.append(" for update");
		if (kPaging == null)
			return repo.getListBySQL(PromotionShopMap.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(PromotionShopMap.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<PromotionShopMap> getListPromotionShopMapWithListPromotionId(List<Long> listPromotionProgramId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select psm.* from promotion_shop_map psm, promotion_program pp, shop s where 1 = 1 ");
		sql.append(" and psm.promotion_program_id = pp.promotion_program_id");
		sql.append(" and psm.shop_id = s.shop_id");
		if (listPromotionProgramId != null) {
			sql.append(" and pp.promotion_program_id in (");
			for (int i = 0, size = listPromotionProgramId.size(); i < size; i++) {
				sql.append("?");
				if (i < size - 1) {
					sql.append(", ");
				}
				params.add(listPromotionProgramId.get(i));
			}
			sql.append(")");
		}
		sql.append(" and psm.status <> ?");
		params.add(ActiveType.DELETED.getValue());
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append(" AND psm.from_date < TRUNC(sysdate) + 1  ");
		sql.append(" AND (psm.to_date    >= TRUNC(sysdate) or psm.to_date is null) ");
		//end
		sql.append(" order by pp.from_date desc, s.shop_code");
		return repo.getListBySQL(PromotionShopMap.class, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.dao.PromotionShopMapDAO#
	 * getListPromotionShopMapWithShopAndPromotionProgram(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public List<PromotionShopMap> getListPromotionShopMapWithShopAndPromotionProgram(Long shopId, Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from promotion_shop_map psm where status = 1 and shop_id in (select shop_id from shop start with shop_id = ? connect by prior parent_shop_id = shop_id)");
		//		sql.append(" and shop_id <> ?");
		//		params.add(shopId);
		params.add(shopId);
		sql.append(" and promotion_program_id = ?");
		params.add(promotionProgramId);
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("     AND psm.from_date < TRUNC(sysdate) + 1  ");
		sql.append("     AND (psm.to_date    >= TRUNC(sysdate) or psm.to_date is null) ");
		//end
		return repo.getListBySQL(PromotionShopMap.class, sql.toString(), params);
	}

	@Override
	public List<PromotionShopMap> getListPromotionChildShopMapWithShopAndPromotionProgram(Long shopId, Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from promotion_shop_map psm where status = 1 and shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
		//		sql.append(" and shop_id <> ?");
		//		params.add(shopId);
		params.add(shopId);
		sql.append(" and promotion_program_id = ?");
		params.add(promotionProgramId);
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		//sql.append(" AND psm.from_date < TRUNC(sysdate) + 1  ");
		//sql.append(" AND (psm.to_date    >= TRUNC(sysdate) or psm.to_date is null) ");
		//end
		return repo.getListBySQL(PromotionShopMap.class, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.PromotionShopMapDAO#checkExitsPromotionWithParentShop
	 * (java.lang.Long, java.lang.String)
	 */
	@Override
	public Boolean checkExitsPromotionWithParentShop(Long promotionProgramid, String shopCode) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT COUNT(1) AS COUNT");
		sql.append(" FROM promotion_shop_map psm");
		sql.append(" WHERE promotion_program_id = ?");
		params.add(promotionProgramid);
		sql.append(" AND status = ?");
		params.add(ActiveType.RUNNING.getValue());
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append(" AND psm.from_date < TRUNC(sysdate) + 1  ");
		sql.append(" AND (psm.to_date    >= TRUNC(sysdate) or psm.to_date is null) ");
		//end
		sql.append(" AND shop_id               IN");
		sql.append(" (SELECT shop_id");
		sql.append(" FROM shop");
		sql.append(" START WITH shop_code      = ? ");
		params.add(shopCode);
		sql.append(" CONNECT BY prior parent_shop_id = shop_id)");
		sql.append(" AND shop_id <> (select shop_id from shop where shop_code = ?)");
		params.add(shopCode);
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public List<Shop> getListShopInPromotionProgram(Long promotionProgramId, String shopCode, String shopName, Integer quantity) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct * from shop s where s.status = 1 start with shop_id in ");
		sql.append("	(select ps.shop_id from promotion_shop_map ps join shop sh on ps.shop_id = sh.shop_id where ps.status = 1 and promotion_program_id = ?");
		params.add(promotionProgramId);
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and sh.shop_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(shopCode).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and UNICODE2ENGLISH(sh.shop_name) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(shopName).toUpperCase());
		}
		if (quantity != null) {
			sql.append(" and quantity_max = ?");
			params.add(quantity);
		}
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		//sql.append("     AND ps.from_date < TRUNC(sysdate) + 1  ");
		//sql.append("     AND (ps.to_date    >= TRUNC(sysdate) or ps.to_date is null) ");
		//end
		sql.append("	) ");
		sql.append(" connect by prior parent_shop_id = shop_id");
		sql.append(" order by s.shop_code");

		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<QuantityVO> getListQuantityVOEx(List<Shop> lstShop, Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT shop_id shopId, ");
		sql.append("   (");
		sql.append("	select case object_type when 3 then 1 else 0 end from channel_type where channel_type_id = shop_type_id");
		sql.append("   ) isNPP, ");
		sql.append("   (");
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("	select count(1) from shop ss where exists (select 1 from promotion_shop_map psm where psm.shop_id = ss.shop_id and psm.status = 1  AND psm.from_date < TRUNC(sysdate) + 1  AND (psm.to_date    >= TRUNC(sysdate) or psm.to_date is null) and promotion_program_id = ? ) START WITH ss.shop_id = s.shop_id CONNECT BY prior ss.shop_id = ss.parent_shop_id");
		params.add(promotionProgramId);
		sql.append("   ) isJoin ");
		sql.append(" FROM shop s ");
		sql.append(" WHERE 1=1 ");
		
		String paramFix = StringUtility.getParamsIdFixBugTooLongParamsLstShop(lstShop, "shop_id");
		sql.append(paramFix);

		String[] fieldNames = { "shopId", "isNPP", "isJoin" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(QuantityVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<QuantityVO> getListQuantityVO(List<Shop> lstShop, Long promotionProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT shop_id shopId, ");

		sql.append(" (case when exists(select 1 FROM promotion_shop_map psm ");
		sql.append(" WHERE promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" AND status = 1 ");
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		//sql.append(" AND psm.from_date < TRUNC(sysdate) + 1  ");
		//sql.append(" AND (psm.to_date    >= TRUNC(sysdate) or psm.to_date is null) ");
		//end
		sql.append(" AND shop_id IN ");
		sql.append(" (select shop_id from shop start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id) ");
		sql.append(" and quantity_max is null ");
		sql.append(" ) then null ");
		sql.append(" else ");
		sql.append(" (SELECT SUM(quantity_max) ");
		sql.append(" FROM promotion_shop_map psm ");
		sql.append(" WHERE promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" AND status = 1 ");
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		//sql.append(" AND psm.from_date < TRUNC(sysdate) + 1  ");
		//sql.append(" AND (psm.to_date    >= TRUNC(sysdate) or psm.to_date is null) ");
		//end
		sql.append(" AND shop_id IN ");
		sql.append(" (select shop_id from shop start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id) ");
		sql.append(" ) ");
		sql.append(" end) quantityMax, ");

		sql.append("   ( ");
		/**
		 * SangTN old: sql.append(
		 * "     SELECT promotion_shop_map_id FROM promotion_shop_map psm1 WHERE promotion_program_id = ? and status = 1 and shop_id = s.shop_id  "
		 * ); return error: single-row subquery returns more than one row
		 */
		sql.append("     SELECT max(promotion_shop_map_id) FROM promotion_shop_map psm1 WHERE promotion_program_id = ? and status = 1 and shop_id = s.shop_id   ");

		params.add(promotionProgramId);
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		//sql.append("     AND psm1.from_date < TRUNC(sysdate) + 1  ");
		//sql.append("     AND (psm1.to_date    >= TRUNC(sysdate) or psm1.to_date is null) ");
		//end
		sql.append("   GROUP BY shop_id ) promotionShopMapId, ");
		sql.append("   ( ");
		sql.append("     SELECT SUM(nvl(quantity_received, 0)) FROM promotion_shop_map psm2 WHERE promotion_program_id = ? and status = 1 and shop_id in  ");
		params.add(promotionProgramId);
		sql.append("           (select shop_id from shop start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id) ");
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		//sql.append("     AND psm2.from_date < TRUNC(sysdate) + 1  ");
		//sql.append("     AND (psm2.to_date    >= TRUNC(sysdate) or psm2.to_date is null) ");
		//end
		sql.append("   ) quantityReceived, ");
		sql.append("   (");
		sql.append("	select case object_type when 3 then 1 else 0 end from channel_type where channel_type_id = shop_type_id");
		sql.append("   ) isNPP ");
		sql.append(" FROM shop s ");
		sql.append(" WHERE 1=1 ");
		
		String paramFix = StringUtility.getParamsIdFixBugTooLongParamsLstShop(lstShop, "shop_id");
		sql.append(paramFix);
		
		//2.5
		String[] fieldNames = { "shopId", "quantityMax", "quantityReceived", "isNPP", "promotionShopMapId" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG };
		return repo.getListByQueryAndScalar(QuantityVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public Boolean isExistChildJoinProgram(String shopCode, Long promotionProgramId, Boolean exceptShopCheck) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from promotion_shop_map f where status = 1");
		if (promotionProgramId != null) {
			sql.append(" and promotion_program_id = ? ");
			params.add(promotionProgramId);
		}
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("     AND f.from_date < TRUNC(sysdate) + 1  ");
		sql.append("     AND (f.to_date    >= TRUNC(sysdate) or f.to_date is null) ");
		//end
		sql.append(" and f.shop_id in (select shop_id from shop start with shop_code = ? connect by prior shop_id = parent_shop_id)");
		params.add(shopCode.toUpperCase());
		if (Boolean.TRUE.equals(exceptShopCheck)) {
			sql.append(" and not exists (select 1 from shop where shop_code = ? and shop_id = f.shop_id)");
			params.add(shopCode);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public List<PromotionShopMapVO> getListPromotionShopMapVO(PromotionShopMapFilter filter, KPaging<PromotionShopMapVO> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT psm.promotion_shop_map_id id, ");
		sql.append(" s.shop_code shopCode, ");
		sql.append(" s.shop_name shopName, ");
		sql.append(" (SELECT short_code FROM customer WHERE customer_id = pcm.customer_id) shortCode, ");
		sql.append(" (SELECT customer_code FROM customer WHERE customer_id = pcm.customer_id) customerCode, ");
		sql.append(" psm.quantity_max quantityMaxShop, ");
		sql.append(" pcm.quantity_max quantityMaxCus, ");
		sql.append(" psm.NUM_MAX as numMax, ");
		sql.append(" psm.AMOUNT_MAX as amountMax ");
		sql.append(" FROM promotion_shop_map psm JOIN shop s ON s.shop_id = psm.shop_id ");
		sql.append(" LEFT JOIN promotion_customer_map pcm ON pcm.promotion_shop_map_id = psm.promotion_shop_map_id and pcm.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" WHERE 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and s.shop_code like ? escape '/' ");
			String s = filter.getShopCode().toUpperCase();
			s = StringUtility.toOracleSearchLikeSuffix(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and lower(s.shop_name) like ? escape '/' ");
			String s = filter.getShopName().toLowerCase();
			s = StringUtility.toOracleSearchLikeSuffix(s);
			params.add(s);
		}
		if (filter.getProgramId() != null) {
			sql.append(" and psm.promotion_program_id = ? ");
			params.add(filter.getProgramId());
		}
		if (filter.getStatus() != null) {
			sql.append(" and psm.status=?");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and psm.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getQuantityMax() != null) {
			sql.append(" and psm.quantity_max = ? ");
			params.add(filter.getQuantityMax());
		}

		if (filter.getParentShopId() != null) {
			sql.append(" and psm.shop_id in (");
			sql.append("select shop_id from shop where status = 1 start with shop_id = ? connect by prior shop_id = parent_shop_id");
			sql.append(")");
			params.add(filter.getParentShopId());
		}

		String[] fieldNames = { 
				"id",//1 
				"shopCode",//2 
				"shopName",//3
				"shortCode",//4
				"customerCode",//5 
				"quantityMaxShop",//6 
				"quantityMaxCus",//7
				"numMax",//8
				"amountMax"//9
				};
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL };
		if (kPaging == null) {
			sql.append(" order by shopCode, shortCode");
			return repo.getListByQueryAndScalar(PromotionShopMapVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from( ");
		countSql.append(sql.toString());
		countSql.append(")");
		sql.append(" order by shopCode, shortCode");
		return repo.getListByQueryAndScalarPaginated(PromotionShopMapVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}
	
	@Override
	public List<PromotionShopMapVO> getPromotionShopMapVOByFilter(PromotionShopMapFilter filter, KPaging<PromotionShopMapVO> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT psm.promotion_shop_map_id id, ");
		sql.append(" s.shop_code shopCode, ");
		sql.append(" s.shop_name shopName, ");
		sql.append(" psm.quantity_max quantityMaxShop, ");
		sql.append(" psm.NUM_MAX as numMax, ");
		sql.append(" psm.AMOUNT_MAX as amountMax ");
		sql.append(" FROM promotion_shop_map psm JOIN shop s ON s.shop_id = psm.shop_id ");
		sql.append(" WHERE 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and s.shop_code like ? escape '/' ");
			String s = filter.getShopCode().toUpperCase();
			s = StringUtility.toOracleSearchLikeSuffix(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and UNICODE2ENGLISH(s.shop_name) like ? escape '/' ");
			String s = filter.getShopName().toLowerCase();
			s = StringUtility.toOracleSearchLikeSuffix(s);
			params.add(s);
		}
		if (filter.getProgramId() != null) {
			sql.append(" and psm.promotion_program_id = ? ");
			params.add(filter.getProgramId());
		}
		if (filter.getStatus() != null) {
			sql.append(" and psm.status=?");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and psm.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getQuantityMax() != null) {
			sql.append(" and psm.quantity_max = ? ");
			params.add(filter.getQuantityMax());
		}

		if (filter.getParentShopId() != null) {
			sql.append(" and s.shop_id in (");
			sql.append("select shop_id from shop where status = 1 start with shop_id = ? connect by prior shop_id = parent_shop_id");
			sql.append(")");
			params.add(filter.getParentShopId());
		}
		String[] fieldNames = { 
				"id",//1 
				"shopCode",//2 
				"shopName",//3
				"quantityMaxShop",//4
				"numMax",//5
				"amountMax"//6
				};
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.BIG_DECIMAL,//4 
				StandardBasicTypes.BIG_DECIMAL, //5
				StandardBasicTypes.BIG_DECIMAL //6
		};
		if (kPaging == null) {
			sql.append(" order by shopCode");
			return repo.getListByQueryAndScalar(PromotionShopMapVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from( ");
		countSql.append(sql.toString());
		countSql.append(")");
		sql.append(" order by shopCode");
		return repo.getListByQueryAndScalarPaginated(PromotionShopMapVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionShopVO> getShopTreeInPromotionProgram(long shopId, long promotionId, String shopCode, String shopName, Integer quantity) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("with");

		if (!StringUtility.isNullOrEmpty(shopCode) || !StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" lstShopTmp as (");
			sql.append("select shop_id from shop where 1=1");
			/*
			 * sql.append(" select distinct shop_id"); sql.append(" from shop");
			 * sql.append(" start with 1=1");
			 */
			if (!StringUtility.isNullOrEmpty(shopCode)) {
				sql.append(" and lower(shop_code) like ? escape '/'");
				shopCode = shopCode.toLowerCase();
				shopCode = StringUtility.toOracleSearchLike(shopCode);
				params.add(shopCode);
			}
			if (!StringUtility.isNullOrEmpty(shopName)) {
				sql.append(" and lower(name_text) like ? escape '/'");
				shopName = shopName.toLowerCase();
				shopName = Unicode2English.codau2khongdau(shopName);
				shopName = StringUtility.toOracleSearchLike(shopName);
				params.add(shopName);
			}
			/* sql.append(" connect by prior shop_id = parent_shop_id"); */
			sql.append(" ),");
		}

		sql.append(" lstNPP as (");
		sql.append(" select psm.shop_id as id,");
		sql.append(" (select shop_code from shop where shop_id = psm.shop_id) as shopCode,");
		sql.append(" (select shop_name from shop where shop_id = psm.shop_id) as shopName,");
//		sql.append(" psm.quantity_max as quantity, nvl(psm.quantity_received, 0) as receivedQtt,");
		sql.append(" psm.quantity_max as quantity, nvl(psm.QUANTITY_RECEIVED_TOTAL, 0) as receivedQtt,");
		sql.append("  psm.amount_max              AS amountMax,");
//		sql.append("    NVL(psm.amount_received, 0) AS receivedAmt,");
		sql.append("    NVL(psm.AMOUNT_RECEIVED_TOTAL, 0) AS receivedAmt,");
		sql.append("     psm.num_max              AS numMax,");
//		sql.append("     NVL(psm.num_received, 0) AS receivedNum,");
		sql.append("     NVL(psm.NUM_RECEIVED_TOTAL, 0) AS receivedNum,");
		sql.append(" case when psm.IS_QUANTITY_MAX_EDIT is null then 0 else psm.IS_QUANTITY_MAX_EDIT end isEdit");
		sql.append(" from promotion_shop_map psm");
		sql.append(" where promotion_program_id = ?");
		params.add(promotionId);
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (quantity != null) {
			sql.append(" and psm.quantity_max = ?");
			params.add(quantity);
		}
		if (!StringUtility.isNullOrEmpty(shopCode) || !StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and psm.shop_id in (select shop_id from lstShopTmp)");
		}
		sql.append(" ),");
		
		sql.append("   lstSub AS ");
		sql.append("   (SELECT shop_id ");
		sql.append("     FROM shop ");
		sql.append("     WHERE status               = 1 and shop_id <> ? ");
		params.add(shopId);
		sql.append("       START WITH shop_id     = ? ");
		params.add(shopId);
		sql.append("       CONNECT BY prior parent_shop_id = shop_id ");
		sql.append("   ),");
		
		sql.append(" lstDV as (");
		sql.append(" select distinct parent_shop_id as parentId, shop_id as id, shop_code as shopCode, shop_name as shopName,");
		sql.append("(select count(1) from promotion_shop_map where promotion_program_id = ?");
		params.add(promotionId);
		sql.append(" and quantity_max is null and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and shop_id in (select shop_id from shop start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id)) as qtt,");
		sql.append("   (SELECT COUNT(1)  ");
		sql.append("   FROM promotion_shop_map  ");
		sql.append("   WHERE promotion_program_id = ?  ");
		sql.append("   AND amount_max          IS NULL  ");
		sql.append("   AND status                 = ?  ");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   AND shop_id               IN  ");
		sql.append("    (SELECT shop_id  ");
		sql.append("    FROM shop  ");
		sql.append("      START WITH shop_id       = s.shop_id  ");
		sql.append("       CONNECT BY prior shop_id = parent_shop_id  ");
		sql.append("    )  ");
		sql.append("   ) AS amt,  ");
		sql.append("   (SELECT COUNT(1)  ");
		sql.append("   FROM promotion_shop_map  ");
		sql.append("   WHERE promotion_program_id = ?  ");
		sql.append("   AND num_max          IS NULL  ");
		sql.append("   AND status                 = ?  ");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   AND shop_id               IN  ");
		sql.append("    (SELECT shop_id  ");
		sql.append("    FROM shop  ");
		sql.append("      START WITH shop_id       = s.shop_id  ");
		sql.append("      CONNECT BY prior shop_id = parent_shop_id  ");
		sql.append("     )  ");
		sql.append("   ) AS num,  ");
		
		sql.append(" (case when ct.specific_type = ? then 1 ");
		params.add(ShopSpecificType.NPP.getValue());
		sql.append(" else 0 end) as isNPP, ");
		sql.append(" level lv");
		sql.append(" from shop s");
		sql.append(" join shop_type ct on (ct.shop_type_id = s.shop_type_id) ");
		sql.append(" where shop_id not in (SELECT shop_id FROM lstSub) ");
		sql.append(" start with shop_id in (select id from lstNPP)");
		sql.append(" connect by prior parent_shop_id = shop_id");
		sql.append(" )");
		sql.append(" select s.parentId, s.id, s.shopCode, s.shopName,");
		sql.append(" (CASE s.isNPP WHEN 1 THEN npp.quantity ");
		sql.append(" ELSE (SELECT SUM(quantity_max) FROM promotion_shop_map WHERE promotion_program_id = ? AND status = ? ");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND shop_id IN (SELECT shop_id FROM shop START WITH shop_id = s.id CONNECT BY prior shop_id = parent_shop_id)) END ) AS quantity, ");
		sql.append("(case s.isNPP when 1 then npp.receivedQtt else");
//		sql.append(" (select sum(quantity_received) from promotion_shop_map where promotion_program_id = ? and status = ?");
		sql.append(" (select sum(QUANTITY_RECEIVED_TOTAL) from promotion_shop_map where promotion_program_id = ? and status = ?");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and shop_id in (select shop_id from shop start with shop_id = s.id connect by prior shop_id = parent_shop_id)) end");
		sql.append(" ) as receivedQtt,");
		sql.append(" s.isNPP,");
		sql.append(" ( ");
		sql.append(" 		  CASE s.isNPP ");
		sql.append(" 		    WHEN 1 ");
		sql.append(" 		    THEN npp.isEdit ");
		sql.append(" 		    ELSE 0 ");
		sql.append(" 		  END) AS isEdit, ");
		sql.append(" 		  ( ");
		sql.append(" 		      CASE s.isNPP ");
		sql.append(" 		        WHEN 1 ");
		sql.append(" 		        THEN npp.amountMax ");
		sql.append(" 		        ELSE ");
		sql.append(" 		          (SELECT SUM(amount_max) ");
		sql.append(" 		          FROM promotion_shop_map ");
		sql.append(" 		          WHERE promotion_program_id = ? ");
		sql.append(" 		          AND status                 = ? ");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" 		          AND shop_id               IN ");
		sql.append(" 		            (SELECT shop_id ");
		sql.append(" 		            FROM shop ");
		sql.append("               START WITH shop_id       = s.id ");
		sql.append(" 	              CONNECT BY prior shop_id = parent_shop_id ");
		sql.append(" 	            ) ");
		sql.append(" 	          ) ");
		sql.append(" 	  END) AS amountMax, ");
		sql.append(" 	   ( ");
		sql.append(" 	  CASE s.isNPP ");
		sql.append(" 	    WHEN 1 ");
		sql.append(" 	    THEN npp.receivedAmt ");
		sql.append(" 	    ELSE ");
		sql.append(" 	      (SELECT SUM(AMOUNT_RECEIVED_TOTAL) ");
		sql.append(" 	      FROM promotion_shop_map ");
		sql.append(" 	      WHERE promotion_program_id = ? ");
		sql.append(" 	      AND status                 = ? ");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" 	      AND shop_id               IN ");
		sql.append(" 	        (SELECT shop_id ");
		sql.append(" 		        FROM shop ");
		sql.append(" 		          START WITH shop_id       = s.id ");
		sql.append(" 		          CONNECT BY prior shop_id = parent_shop_id ");
		sql.append(" 	        ) ");
		sql.append(" 	      ) ");
		sql.append(" 	  END ) AS receivedAmt, ");
		sql.append(" 	  ( ");
		sql.append(" 		      CASE s.isNPP ");
		sql.append(" 		        WHEN 1 ");
		sql.append(" 		        THEN npp.numMax ");
		sql.append(" 		        ELSE ");
		sql.append(" 		          (SELECT SUM(num_max) ");
		sql.append(" 		          FROM promotion_shop_map ");
		sql.append(" 		          WHERE promotion_program_id = ? ");
		sql.append(" 		          AND status                 = ? ");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" 		          AND shop_id               IN ");
		sql.append(" 		            (SELECT shop_id ");
		sql.append(" 		            FROM shop ");
		sql.append(" 		              START WITH shop_id       = s.id ");
		sql.append(" 	              CONNECT BY prior shop_id = parent_shop_id ");
		sql.append(" 	            ) ");
		sql.append(" 		          ) ");
		sql.append(" 		  END) AS numMax , ");
		sql.append(" 		   ( ");
		sql.append(" 		  CASE s.isNPP ");
		sql.append(" 		    WHEN 1 ");
		sql.append(" 		    THEN npp.receivedNum ");
		sql.append(" 	    ELSE ");
		sql.append(" 	      (SELECT SUM(NUM_RECEIVED_TOTAL) ");
		sql.append(" 	      FROM promotion_shop_map ");
		sql.append(" 	      WHERE promotion_program_id = ? ");
		sql.append(" 	      AND status                 = ? ");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" 	      AND shop_id               IN ");
		sql.append(" 	        (SELECT shop_id ");
		sql.append("         FROM shop ");
		sql.append("         START WITH shop_id       = s.id ");
		sql.append("          CONNECT BY prior shop_id = parent_shop_id ");
		sql.append(" 	        ) ");
		sql.append(" 	      ) ");
		sql.append(" 	  END ) AS receivedNum, ");
		sql.append(" (case s.isNPP when 1 then npp.isEdit else 0 end) as isEdit");
		sql.append(" from lstDV s");
		sql.append(" left join lstNPP npp on (s.id = npp.id)");
		sql.append(" where 1 = 1");
		sql.append(" order by s.lv desc, isNPP, shopCode, shopName");

		String[] fieldNames = new String[] { 
				"id", //1 
				"parentId",//2 
				"shopCode", //3
				"shopName", //4
				"quantity", //5
				"receivedQtt", //6
				"isNPP", //7
				"amountMax", //8 
				"receivedAmt", //9
				"numMax", //10
				"receivedNum", //11 
				"isEdit" //12
		};

		Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, //1
				StandardBasicTypes.LONG, //2 
				StandardBasicTypes.STRING,//3 
				StandardBasicTypes.STRING,//4 
				StandardBasicTypes.BIG_DECIMAL, //5 
				StandardBasicTypes.BIG_DECIMAL, //6 
				StandardBasicTypes.INTEGER, //7
				StandardBasicTypes.BIG_DECIMAL, //8 
				StandardBasicTypes.BIG_DECIMAL, //9
				StandardBasicTypes.BIG_DECIMAL, //10
				StandardBasicTypes.BIG_DECIMAL, //11
				StandardBasicTypes.INTEGER //12 
		};

		List<PromotionShopVO> lst = repo.getListByQueryAndScalar(PromotionShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	/**
	 * Van giu nguyen nghiep vu cua ham getShopTreeInPromotionProgram Cai tien
	 * cach so sanh giam bot case when: Improvements - cai tien Them dieu kien
	 * thoa man phan quyen CMS
	 * 
	 * @author hunglm16
	 * @since March 24,2015
	 * */
	@Override
	public List<PromotionShopVO> searchShopTreeInPromotionProgramImpr(PromotionShopMapFilter filter) throws DataAccessException {
		//long promotionId, String shopCode, String shopName, Integer quantity) throws DataAccessException {
		if (filter.getPromotionId() == null ) {
			throw new IllegalArgumentException("PromotionId is not null");
		}
		if (filter.getShopRootId() == null ) {
			throw new IllegalArgumentException("ShopRootId is not null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("WITH lstNPP AS (SELECT psm.shop_id AS id,  ");
		sql.append(" rShop.shop_code AS shopCode, ");
		sql.append(" rShop.shop_name AS shopName, ");
		sql.append(" psm.quantity_max AS quantity, ");
		sql.append(" NVL(psm.quantity_received, 0) AS receivedQtt, ");
		sql.append(" (CASE WHEN psm.IS_QUANTITY_MAX_EDIT IS NULL THEN 0 ELSE psm.IS_QUANTITY_MAX_EDIT END) isEdit ");
		sql.append(" FROM promotion_shop_map psm  ");
		sql.append(" join (select shop_id, shop_code, shop_name from shop where status = 1 start with shop_id = ? and status = 1  ");
		params.add(filter.getShopRootId());
		sql.append(" connect by prior shop_id = parent_shop_id) rShop on psm.shop_id = rShop.shop_id ");
		sql.append(" WHERE psm.promotion_program_id = ? ");
		params.add(filter.getPromotionId());
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and lower(rShop.shop_code) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getShopCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and UNICODE2ENGLISH(rShop.shop_name) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getShopName().trim().toLowerCase()));
		}
		if (filter.getQuantity() != null) {
			sql.append(" and psm.quantity_max = ?");
			params.add(filter.getQuantity());
		}
		sql.append(" AND psm.status = 1 ), ");
		sql.append(" lstDV AS (SELECT DISTINCT parent_shop_id AS parentId, shop_id AS id, shop_code AS shopCode, ");
		sql.append(" shop_name AS shopName,");
		sql.append(" (SELECT COUNT(1) FROM promotion_shop_map WHERE promotion_program_id = ? ");
		params.add(filter.getPromotionId());
		sql.append(" AND quantity_max IS NULL AND status = 1 ");
		sql.append(" AND shop_id IN (SELECT shop_id FROM shop START WITH shop_id = s.shop_id  and s.status = 1 CONNECT BY prior shop_id = parent_shop_id)) AS qtt, ");
		sql.append("  decode_islevel_vnm(ct.object_type) - 1 as orderNo ");
		sql.append(" FROM shop s JOIN channel_type ct  ON (ct.channel_type_id = s.shop_type_id) ");
		sql.append(" START WITH s.shop_id  IN (SELECT id FROM lstNPP) and s.status = 1 CONNECT BY prior s.parent_shop_id = s.shop_id) ");
		sql.append(" SELECT s.parentId, s.id, s.shopCode, s.shopName, (s.orderNo + 1) as isLevel, ");
		sql.append(" (CASE WHEN qtt > 0 THEN NULL ");
		sql.append("  ELSE (CASE s.orderNo WHEN 4 THEN npp.quantity ELSE (SELECT SUM(quantity_max) FROM promotion_shop_map WHERE promotion_program_id = ? AND status = 1  ");
		params.add(filter.getPromotionId());
		sql.append("  AND shop_id IN (SELECT shop_id FROM shop START WITH shop_id = s.id CONNECT BY prior shop_id = parent_shop_id))END ) END) AS quantity, ");
		sql.append(" (CASE s.orderNo WHEN 4 THEN npp.receivedQtt ELSE (SELECT SUM(quantity_received) FROM promotion_shop_map  ");
		sql.append(" WHERE promotion_program_id = ? ");
		params.add(filter.getPromotionId());
		sql.append(" AND status = 1 AND shop_id IN (SELECT shop_id FROM shop START WITH shop_id = s.id and status = 1 CONNECT BY prior shop_id = parent_shop_id)) END ) AS receivedQtt, ");
		sql.append(" (CASE s.orderNo WHEN 4 THEN 1 ELSE 0 END) AS isNPP, ");
		sql.append(" (CASE s.orderNo WHEN 4 THEN npp.isEdit ELSE 0 END) AS isEdit ");
		sql.append(" FROM lstDV s LEFT JOIN lstNPP npp ");
		sql.append(" ON (s.id = npp.id) WHERE 1  = 1 ");
		if (filter.getIsLevel() != null) {
			sql.append(" and s.orderNo + 2 > ? ");
			params.add(filter.getIsLevel());
		}
		sql.append(" ORDER BY isNPP, isLevel, shopCode, shopName ");

		String[] fieldNames = new String[] { "id", "parentId", "shopCode", "shopName", "quantity", "receivedQtt", "isNPP", "isEdit", "isLevel" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };

		List<PromotionShopVO> lst = repo.getListByQueryAndScalar(PromotionShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	@Override
	public List<PromotionShopVO> searchShopForPromotionProgram(long promotionId, long pShopId, String shopCode, String shopName, Long staffRootId, Long roleId, Long shopRootId) throws DataAccessException {
		if (staffRootId == null || roleId == null || shopRootId == null) {
			throw new IllegalArgumentException("cms paramater is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		if (StringUtility.isNullOrEmpty(shopCode) && StringUtility.isNullOrEmpty(shopName)) {
			sql.append("select s.shop_id as id, s.parent_shop_id as parentId,");
			sql.append(" s.shop_code as shopCode, s.shop_name as shopName,");
			sql.append(" (select count(1) from promotion_shop_map where promotion_program_id = ?");
			params.add(promotionId);
			sql.append(" and status = ? and rownum = 1");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" and shop_id in (select shop_id from shop start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id)");
			sql.append(" ) as isExists,");
			sql.append(" (select count(1) from shop_type where shop_type_id = s.shop_type_id and status = ? and specific_type = ? ) as isNPP");
			params.add(ActiveType.RUNNING.getValue());
			params.add(ShopSpecificType.NPP.getValue());
			sql.append(" from shop s ");
			sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s.shop_id = ogr.number_key_id ");
			params.add(staffRootId);
			params.add(roleId);
			params.add(shopRootId);
			sql.append(" where parent_shop_id = ?");
			params.add(pShopId);
			sql.append(" and status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" order by shopCode, shopName");
		} else {
			sql.append("with lstShopTmp as (");
			sql.append(" select shop_id, level lv from shop");
			sql.append(" where status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" start with shop_id = ?");
			params.add(pShopId);
			sql.append(" connect by prior shop_id = parent_shop_id");
			sql.append(" ),");
			sql.append(" lstDVTmp as (");
			sql.append(" select distinct s.shop_id as id, s.parent_shop_id as parentId,");
			sql.append(" s.shop_code as shopCode, s.shop_name as shopName,");
			sql.append(" (select count(1) from promotion_shop_map where promotion_program_id = ?");
			params.add(promotionId);
			sql.append(" and status = ? and rownum = 1");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" and shop_id in (select shop_id from shop start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id)");
			sql.append(" ) as isExists,");
			sql.append(" (select specific_type from shop_type where shop_type_id = s.shop_type_id and status = ?) as specific_type, ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" ls.lv");
			sql.append(" from shop s");
			sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s.shop_id = ogr.number_key_id ");
			params.add(staffRootId);
			params.add(roleId);
			params.add(shopRootId);
			sql.append(" join lstShopTmp ls on s.shop_id = ls.shop_id ");
			sql.append(" where 1 = 1 ");
			sql.append(" start with 1=1");

			if (!StringUtility.isNullOrEmpty(shopCode)) {
				sql.append(" and lower(shop_code) like ? escape '/'");
				String s = shopCode.toLowerCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (!StringUtility.isNullOrEmpty(shopName)) {
				sql.append(" and lower(name_text) like ? escape '/'");
				String s = shopName.toLowerCase();
				s = Unicode2English.codau2khongdau(s);
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}

			sql.append(" connect by prior s.parent_shop_id = s.shop_id");
			sql.append(" ),");
			sql.append(" lstDV as (");
			sql.append(" select id, parentId, shopCode, shopName,");
			sql.append(" isExists,");
//			sql.append(" (case when shopType = ? then 0");
//			params.add(ShopObjectType.VNM.getValue());
//			sql.append(" when shopType in (?, ?, ?) then 1");
//			params.add(ShopObjectType.GT.getValue());
//			params.add(ShopObjectType.KA.getValue());
//			params.add(ShopObjectType.ST.getValue());
//			sql.append(" when shopType in (?, ?, ?) then 2");
//			params.add(ShopObjectType.MIEN.getValue());
//			params.add(ShopObjectType.MIEN_KA.getValue());
//			params.add(ShopObjectType.MIEN_ST.getValue());
//			sql.append(" when shopType in (?, ?) then 3");
//			params.add(ShopObjectType.VUNG.getValue());
//			params.add(ShopObjectType.VUNG_KA.getValue());
//			sql.append(" when shopType in (?, ?) then 4");
//			params.add(ShopObjectType.NPP.getValue());
//			params.add(ShopObjectType.NPP_KA.getValue());
			sql.append(" (case when specific_type = ? then 1 ");
			params.add(ShopSpecificType.NPP.getValue());
			sql.append(" else 0 ");
			sql.append(" end) as isNPP, ");
			sql.append(" lv");
			sql.append(" from lstDVTmp");
			sql.append(" )");
			sql.append(" select id, parentId, shopCode, shopName, isExists,");
			sql.append(" isNPP");
			sql.append(" from lstDV");
			sql.append(" order by lv, isNPP, shopCode, shopName");
		}

		String[] fieldNames = new String[] { "id", "parentId", "shopCode", "shopName", "isExists", "isNPP" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };

		List<PromotionShopVO> lst = repo.getListByQueryAndScalar(PromotionShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionShopVO> getListShopInPromotion(long promotionId, List<Long> lstShopId, boolean shopMapOnly) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("with lstShopTmp as  (");
		sql.append(" select shop_id");
		sql.append(" from promotion_shop_map");
		sql.append(" where promotion_program_id = ? and status = ?");
		params.add(promotionId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" )");
		sql.append(" select distinct shop_id as id, shop_code as shopCode, shop_name as shopName");
		sql.append(" from shop");
		sql.append(" where 1=1");
		
		if (lstShopId != null && lstShopId.size() > 0) {
			String paramFix = StringUtility.getParamsIdFixBugTooLongParams(lstShopId, "shop_id");
			sql.append(paramFix);
		}
		
		if (shopMapOnly) {
			sql.append(" and shop_id in (select shop_id from lstShopTmp)");
		} else {
			sql.append(" start with shop_id in (select shop_id from lstShopTmp)");
			sql.append(" connect by prior parent_shop_id = shop_id");
		}

		String[] fieldNames = new String[] { "id", "shopCode", "shopName", };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, };

		List<PromotionShopVO> lst = repo.getListByQueryAndScalar(PromotionShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public PromotionShopMap getPromotionParentShopMap(long promotionId, long shopId, Integer status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from promotion_shop_map psm where promotion_program_id = ?");
		params.add(promotionId);

		sql.append(" and shop_id in (select shop_id from shop where status = ? start with shop_id = ? connect by prior parent_shop_id = shop_id)");
		params.add(ActiveType.RUNNING.getValue());
		params.add(shopId);

		if (status != null) {
			sql.append(" and status = ?");
			params.add(status);
		} else {
			sql.append(" and status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}

		return repo.getEntityBySQL(PromotionShopMap.class, sql.toString(), params);
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionShopMap> getPromotionChildrenShopMap(long promotionId, long shopId, Integer status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from promotion_shop_map psm where promotion_program_id = ?");
		params.add(promotionId);

		sql.append(" and shop_id in (select shop_id from shop where status = ? start with shop_id = ? connect by prior shop_id = parent_shop_id and status = ?)");
		params.add(ActiveType.RUNNING.getValue());
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());

		if (status != null) {
			sql.append(" and status = ?");
			params.add(status);
		} else {
			sql.append(" and status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}

		List<PromotionShopMap> lst = repo.getListBySQL(PromotionShopMap.class, sql.toString(), params);
		return lst;
	}
	
	@Override
	public List<PromotionShopMap> getPromotionShopMapIncludeParentShop(long promotionProgramId, long shopId, Integer status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" WITH shopTmp AS ");
		sql.append(" (SELECT s.shop_id, level lv ");
		sql.append(" FROM shop s ");
		sql.append(" WHERE s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" START WITH s.shop_id = ? ");
		params.add(shopId);
		sql.append(" CONNECT BY prior s.parent_shop_id = s.shop_id) ");
		
		sql.append("SELECT psm.* FROM promotion_shop_map psm ");
		sql.append(" JOIN shopTmp s ON psm.shop_id = s.shop_id ");
		sql.append(" WHERE psm.promotion_program_id = ? ");
		params.add(promotionProgramId);
		if (status != null) {
			sql.append(" AND psm.status = ?");
			params.add(status);
		} else {
			sql.append(" AND psm.status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" ORDER BY s.lv ");
		List<PromotionShopMap> lst = repo.getListBySQL(PromotionShopMap.class, sql.toString(), params);
		return lst;
	}
	
	@Override
	public List<PromotionShopMap> getPromotionShopMapByCustomer(List<Long> lstId, Long promotionId) throws DataAccessException {
		if (lstId == null || promotionId == null) {
			return new ArrayList<PromotionShopMap>();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT DISTINCT psm.* ");
		sql.append(" FROM PROMOTION_SHOP_MAP psm ");
		sql.append(" JOIN Customer c ON psm.SHOP_ID = c.SHOP_ID ");
		sql.append(" WHERE PROMOTION_PROGRAM_ID = ? ");
		params.add(promotionId);
		sql.append(" AND c.CUSTOMER_ID IN (-1 ");
		for (int i = 0, n = lstId.size(); i < n; i++) {
			Long id = lstId.get(i);
			if (id != null) {
				sql.append(", ?");
				params.add(id);
			}
		}
		sql.append(" ) ");
		List<PromotionShopMap> lst = repo.getListBySQL(PromotionShopMap.class, sql.toString(), params);
		return lst;
	}

	@Override
	public Long getPromotionShopMapFromFunction(Long shopID, Long promotionProgramID) throws DataAccessException {
		// TODO Auto-generated method stub
		if (shopID == null || promotionProgramID == null) {
			throw new IllegalArgumentException("param is not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select F_GET_PROMOTION_SHOP_MAP_ID(?, ?) from dual ");
		params.add(shopID);
		params.add(promotionProgramID);
		Object proShopID = repo.getObjectByQuery(sql.toString(), params);
		Long promotionShopMapId = Long.parseLong(proShopID.toString());
		return promotionShopMapId;
	}
}
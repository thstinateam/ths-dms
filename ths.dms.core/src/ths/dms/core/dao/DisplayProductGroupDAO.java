package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.StDisplayPdGroup;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface DisplayProductGroupDAO {

	StDisplayPdGroup createDisplayProductGroup(StDisplayPdGroup displayProductGroup, LogInfoVO logInfo) throws DataAccessException;

	void deleteDisplayProductGroup(StDisplayPdGroup displayProductGroup, LogInfoVO logInfo) throws DataAccessException;

	void updateDisplayProductGroup(StDisplayPdGroup displayProductGroup, LogInfoVO logInfo) throws DataAccessException;
	
	StDisplayPdGroup getDisplayProductGroupById(long id) throws DataAccessException;

	List<StDisplayPdGroup> getListDisplayProductGroup(KPaging<StDisplayPdGroup> kPaging, Long displayProgramId,
			DisplayProductGroupType type, ActiveType status,Boolean isAll) throws DataAccessException;

	StDisplayPdGroup getDisplayProductGroupByCode(Long displayProgramId,
			DisplayProductGroupType type, String displayProductGroupCode) throws DataAccessException;
	
//	DisplayProductGroup getDisplayProductGroupByCode(String code, Long displayProgramId, ActiveType activeType) throws DataAccessException;
//
//	Boolean isUsingByOthers(long DisplayProductGroupId) throws DataAccessException;
//	List<DisplayProductGroup> getListDisplayProductGroup(KPaging<DisplayProductGroup> kPaging,
//			Long displayProgramId, String DisplayProductGroupCode,
//			String DisplayProductGroupName, Float percentMin, Float percentMax,
//			ActiveType status) throws DataAccessException;
//	
//	/**
//	 * Kiem tra mot san pham da ton tai trong DISPLAY_GROUP cua mot DisplayProgram
//	 * @author thuattq
//	 */
//	DisplayProductGroup checkIfProductExistsInDisplayProductGroup(Long productId,
//			Long displayProgrameId, ActiveType DisplayProductGroupActiveType)
//			throws DataAccessException;
//
//	/**
//	 * @author thuattq
//	 */
//	DisplayProductGroup getDisplayProductGroupByCodeAndDP(String code,
//			String displayProgramCode, ActiveType activeType)
//			throws DataAccessException;
/**
 * @author loctt
 * @since 25sep2013
 */
	List<StDisplayPdGroup> getListLevelProductGroup(KPaging<StDisplayPdGroup> kPaging, Long displayProgramId,
			DisplayProductGroupType type, ActiveType status,Long levelId) throws DataAccessException;
	
}

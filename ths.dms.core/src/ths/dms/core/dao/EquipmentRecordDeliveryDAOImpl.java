package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.EquipDeliveryRecDtl;
import ths.dms.core.entities.EquipDeliveryRecord;
import ths.dms.core.entities.EquipStockTransFormDtl;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.vo.EquipmentRecordDeliveryVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class EquipmentRecordDeliveryDAOImpl implements EquipmentRecordDeliveryDAO {
	@Autowired
	private IRepository repo;

	@Override
	public EquipDeliveryRecord createEquipmentRecordDelivery(EquipDeliveryRecord equipDeliveryRecord) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipDeliveryRecord == null) {
			throw new IllegalArgumentException("equipDeliveryRecord is null");
		}
		return repo.create(equipDeliveryRecord);
	}

	@Override
	public void updateEquipmentRecordDelivery(EquipDeliveryRecord equipDeliveryRecord) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipDeliveryRecord == null) {
			throw new IllegalArgumentException("equipDeliveryRecord is null");
		}
		repo.update(equipDeliveryRecord);
	}

	@Override
	public EquipDeliveryRecDtl createEquipmentRecordDeliveryDetail(EquipDeliveryRecDtl equipDeliveryRecDtl) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipDeliveryRecDtl == null) {
			throw new IllegalArgumentException("equipDeliveryRecord is null");
		}
		return repo.create(equipDeliveryRecDtl);
	}

	@Override
	public void deleteEquipmentRecordDeliveryDetail(EquipDeliveryRecDtl equipDeliveryRecDtl) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipDeliveryRecDtl == null) {
			throw new IllegalArgumentException("equipDeliveryRecDtl is null");
		}
		repo.delete(equipDeliveryRecDtl);
	}
	
	
	/**
	 * Search danh sach bien ban giao nhan va hop dong
	 * 
	 * @author ...
	 * @modified tamvnm 
	 * @since March 31,2015
	 */
	@Override
	public List<EquipmentRecordDeliveryVO> getListEquipmentRecordDeliveryVOByFilter(EquipmentFilter<EquipmentRecordDeliveryVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("SELECT distinct edr.equip_delivery_record_id                    AS idRecord, ");
		sql.append("       edr.code         AS recordCode, ");
		sql.append("       sf.staff_code    AS staffCode, ");
		sql.append("       sf.staff_name    AS staffName, ");
		sql.append("       (SELECT ct.channel_type_name ");
		sql.append("        FROM   channel_type ct ");
		sql.append("        WHERE  ct.channel_type_id = sf.staff_type_id)  AS staffType, ");
		sql.append("       cus.short_code   AS customerCode, ");
		sql.append("       cus.customer_name                               AS customerName, ");
		sql.append("       cus.address      AS customerAddress, ");
		sql.append("       lstShop.shop_code                               AS shopCode, ");
		sql.append("       lstShop.shop_name                               AS shopName, ");
//		sql.append("       To_char(edr.create_date, 'dd/mm/yyyy')          AS createDate, ");
		//tamvnm: thay doi thanh ngay lap
		sql.append("       To_char(edr.create_form_date, 'dd/mm/yyyy')          AS createDate, ");
		sql.append("       edr.contract_number                             AS numberContract, ");
		sql.append("       To_char(edr.contract_create_date, 'dd/mm/yyyy') AS dateContract, ");
		sql.append("       edr.record_status                               AS statusRecord, ");
		sql.append("       edr.delivery_status                             AS statusDelivery, ");
		sql.append("       edr.print_user   AS printUser, ");
		sql.append("       Decode(edr.print_user, NULL, 0, ");
		sql.append("                              1)                       AS statusPrint, ");
		sql.append("       el.code          AS equipLendCode,   ");
		sql.append("       edr.note          AS note,   ");
		sql.append("       (select count(*) from Equip_Attach_File where object_type = 0 and object_id = edr.equip_delivery_record_id) as numberFile   ");
		
		sql.append("FROM   equip_delivery_record edr ");
		sql.append("       left join staff sf ");
		sql.append("         ON sf.staff_id = edr.staff_id and sf.STATUS = 1 ");
		sql.append("       join customer cus ");
//		sql.append("         ON cus.customer_id = edr.customer_id and cus.STATUS = 1 ");
		sql.append("         ON cus.customer_id = edr.customer_id ");
		sql.append("       join (SELECT * ");
		sql.append("             FROM   shop ");
		sql.append("             WHERE  status = 1 ");
		sql.append("             START WITH shop_code = ? ");
		params.add(filter.getCurShopCode());
		sql.append("             CONNECT BY PRIOR shop_id = parent_shop_id ");
		sql.append("                             ) lstShop ");
		sql.append("         ON lstShop.shop_id = cus.shop_id ");
		sql.append("      LEFT JOIN equip_lend el   ");
		sql.append("      ON el.EQUIP_LEND_ID = edr.EQUIP_LEND_ID  ");
		//tamvnm: them dieu kien loc theo ma thiet bi
		sql.append("     LEFT JOIN Equip_Delivery_Rec_Dtl edrd on Edrd.Equip_Delivery_Record_Id = edr.Equip_Delivery_Record_Id   ");
		sql.append("     LEFT JOIN equipment e on e.equip_id = edrd.equip_id   ");
		
		sql.append("WHERE  1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and upper(sf.staff_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and (upper(cus.short_code) like ? ESCAPE '/' or upper(cus.name_text) like ? ESCAPE '/')");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().toUpperCase()));
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().toUpperCase()));
		}
//		if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
//			sql.append(" and upper(cus.name_text) like ? ESCAPE '/' ");
//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerName().toUpperCase()));
//		}
//		if (filter.getFromDate() != null) {
//			sql.append(" and (edr.CREATE_DATE is not null and edr.CREATE_DATE >= trunc(?))");
//			params.add(filter.getFromDate());
//		}
//		if (filter.getToDate() != null) {
//			sql.append(" and ((edr.CREATE_DATE is not null and edr.CREATE_DATE < trunc(?)+1))");
//			params.add(filter.getToDate());
//		}
		//tamvnm: 13/07/2015 - Thay doi tim kiem theo ngay lap
		if (filter.getFromDate() != null) {
			sql.append(" and (edr.CREATE_FORM_DATE is not null and edr.CREATE_FORM_DATE >= trunc(?))");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and ((edr.CREATE_FORM_DATE is not null and edr.CREATE_FORM_DATE < trunc(?)+1))");
			params.add(filter.getToDate());
		}
		if (!StringUtility.isNullOrEmpty(filter.getNumberContract())) {
			sql.append(" and upper(edr.contract_number) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getNumberContract().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getRecordCode())) {
			sql.append(" and upper(edr.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getRecordCode().toUpperCase()));
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("AND ");
			sql.append("( ");
			sql.append("  ? IS NULL ");
			params.add(filter.getShopCode().toUpperCase());
			sql.append("  OR ");
			sql.append("  ? = '' ");
			params.add(filter.getShopCode().toUpperCase());
			sql.append("  OR ");
			sql.append("  upper(lstshop.shop_code) IN  (select shop_code ");
			sql.append("  from shop ");
			sql.append("   start with shop_code in ");		
			sql.append("  ( ");
			sql.append("         SELECT upper(regexp_substr(?,'[^,]+', 1, level)) ");
			params.add(filter.getShopCode().toUpperCase());
			sql.append("         FROM   dual connect BY regexp_substr(?, '[^,]+', 1, level) IS NOT NULL ) ");
			sql.append("  connect by prior shop_id = parent_shop_id) ");
			params.add(filter.getShopCode().toUpperCase());
			sql.append(")");
			
		}
		
		if (filter.getFromContractDate() != null) {
			sql.append(" and (edr.CONTRACT_CREATE_DATE is not null and edr.CONTRACT_CREATE_DATE >= trunc(?))");
			params.add(filter.getFromContractDate());
		}
		if (filter.getToContractDate() != null) {
			sql.append(" and ((edr.CONTRACT_CREATE_DATE is not null and edr.CONTRACT_CREATE_DATE < trunc(?)+1))");
			params.add(filter.getToContractDate());
		}
		
		if (filter.getStatusRecord() != null) {
			sql.append(" and edr.record_status = ? ");
			params.add(filter.getStatusRecord());
		}
		if (filter.getStatusDelivery() != null) {
			sql.append(" and edr.delivery_status = ? ");
			params.add(filter.getStatusDelivery());
		}
		
		if (filter.getStatusPrint() != null) {
			if(filter.getStatusPrint() == 0) {
				sql.append(" AND edr.print_user is null ");
			} else {
				sql.append(" AND edr.print_user is not null ");
			}
		}
		// update by nhutnn, 10/07/2015
		if (filter.getEquipLendId() != null) {
			sql.append(" and edr.EQUIP_LEND_ID = ? ");
			params.add(filter.getEquipLendId());
		}
		if (filter.getEquipLendCode() != null && !filter.getEquipLendCode().isEmpty()) {
			sql.append("  and el.code like ? ESCAPE '/'  ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipLendCode().toUpperCase()));
		}
		
		//tamvnm: them dieu kien loc theo ma thiet bi
		if (filter.getEquipCode() != null) {
			sql.append(" and e.code  = ? ");
			params.add(filter.getEquipCode().toUpperCase());
		}
		
		String[] fieldNames = { 
				"idRecord",			// 0
				"recordCode",		// 1
				"staffCode",		// 2
				"staffName",		// 3
				"staffType",		// 4
				"customerCode",		// 5
				"customerName",		// 6
				"customerAddress",	// 7
				"createDate",		// 8
				"shopCode",			// 9
				"shopName",			// 10
				"numberContract",	// 11
				"dateContract",		// 12
//				"attachFilesName",	// 13
//				"urlAttachFiles",	// 14
				"statusRecord",		// 15
				"statusDelivery",	// 16
				"statusPrint",		// 17
				"equipLendCode",	// 18
				"note",
				"numberFile"		// 19
		};

		Type[] fieldTypes = { 
				StandardBasicTypes.LONG,	// 0
				StandardBasicTypes.STRING,	// 1
				StandardBasicTypes.STRING,	// 2
				StandardBasicTypes.STRING,	// 3
				StandardBasicTypes.STRING,	// 4
				StandardBasicTypes.STRING,	// 5
				StandardBasicTypes.STRING,	// 6
				StandardBasicTypes.STRING,	// 7
				StandardBasicTypes.STRING,	// 8
				StandardBasicTypes.STRING,	// 9
				StandardBasicTypes.STRING,	// 10
				StandardBasicTypes.STRING,	// 11
//				StandardBasicTypes.STRING,	// 12
//				StandardBasicTypes.STRING,	// 13
				StandardBasicTypes.STRING,	// 14
				StandardBasicTypes.INTEGER, // 15
				StandardBasicTypes.INTEGER, // 16
				StandardBasicTypes.INTEGER, // 17
				StandardBasicTypes.STRING, 	// 18
				StandardBasicTypes.STRING, 	
				StandardBasicTypes.INTEGER	// 19
		};

		if (filter.getkPaging() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");
			sql.append(" order by edr.code Desc ");
			return repo.getListByQueryAndScalarPaginated(EquipmentRecordDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			sql.append(" order by edr.code Desc ");
			return repo.getListByQueryAndScalar(EquipmentRecordDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public String getCodeRecordDelivery() throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select max(code) as maxCode from equip_delivery_record");
		String code = (String)repo.getObjectByQuery(sql.toString(), params);
		if(code == null){
			return "GN00000001";
		}
		code = code.substring(code.length() - 8);
		code = "00000000" + (Long.parseLong(code.toString()) + 1);
		code = code.substring(code.length() - 8);
		code = "GN" + code;
		return code;
	}
	
	@Override
	public String getCodeEviction() throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select max(code) from Equip_Eviction_Form ");
		String code = (String)repo.getObjectByQuery(sql.toString(), params);
		if(code == null){
			return "TH00000001";
		}
		code = code.substring(code.length() - 8);
		code = "00000000" + (Long.parseLong(code.toString()) + 1);
		code = code.substring(code.length() - 8);
		code = "TH" + code;
		return code;
	}

	@Override
	public EquipmentRecordDeliveryVO getEquipmentRecordDeliveryVOById(Long idRecord) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("SELECT edr.equip_delivery_record_id  AS idRecord, ");
		sql.append("        edr.code                       AS recordCode, ");
		sql.append("        edr.to_permanent_address AS toPermanentAddress, ");
		sql.append("        edr.from_object_name           AS fromShopName, ");
		sql.append("        edr.from_object_address        AS fromShopAddess, ");
		sql.append("        edr.from_object_phone          AS fromPhone, ");
		sql.append("        edr.from_fax                   AS fromFax, ");
		sql.append("        edr.from_representative        AS fromRepresentative, ");
		sql.append("        edr.from_position              AS fromPosition, ");
		sql.append("        cus.short_code                 AS toCustomerCode, ");
		sql.append("        cus.customer_name              AS toCustomerName, ");
		sql.append("        edr.to_object_addres           AS toObjectAddress, ");
		sql.append("        edr.to_address                 AS toAddress, ");
		sql.append("        edr.to_phone                   AS toPhone, ");
		sql.append("        edr.to_business_license        AS toBusinessLicense, ");
		sql.append("        edr.to_business_place          AS toBusinessPlace, ");
		sql.append("        edr.to_business_date           AS toBusinessDate, ");
		sql.append("        edr.to_representative          AS toRepresentative, ");
		sql.append("        edr.to_position                AS toPosition, ");
		sql.append("        edr.to_idno                    AS idNO, ");
		sql.append("        edr.to_idno_place              AS idNOPlace, ");
		sql.append("        edr.to_idno_date               AS idNODate, ");
		sql.append("        edr.contract_create_date       AS contractDate, ");
		sql.append("        edr.contract_number            AS numberContract, ");
		sql.append("        edr.record_status              AS statusRecord, ");
		sql.append("        edr.delivery_status            AS statusDelivery, ");
//		sql.append("        (SELECT sp.shop_name ");
//		sql.append("         FROM   shop sp ");
//		sql.append("         WHERE  sp.shop_id IN (SELECT sp.parent_shop_id ");
//		sql.append("                               FROM   shop sp ");
//		sql.append("                               WHERE  sp.shop_id = s.parent_shop_id)) AS mien, ");
//		sql.append("        (SELECT s1.shop_name ");
//		sql.append("         FROM   shop s1 ");
//		sql.append("         WHERE  s1.shop_id = (SELECT sp.parent_shop_id ");
//		sql.append("                              FROM   shop sp ");
//		sql.append("                              WHERE  sp.shop_id IN (SELECT sp.parent_shop_id ");
//		sql.append("                     FROM   shop sp ");
//		sql.append("                     WHERE ");
//		sql.append("      sp.shop_id = s.parent_shop_id ");
//		sql.append("                    )))                AS kenh, ");
		sql.append(" (SELECT sh.shop_name ");
		sql.append(" FROM   shop sh ");
		sql.append(" JOIN   channel_type cn ");
		sql.append(" ON     sh.shop_type_id = cn.channel_type_id ");
		sql.append(" AND    cn.type = 1 ");
		sql.append(" WHERE  1 = 1 ");
		sql.append(" AND    cn.status = 1 ");
		sql.append(" AND    Decode_islevel_vnm(cn.object_type) = 3 start WITH sh.shop_id = s.shop_id ");
		sql.append(" AND    sh.status = 1 connect BY sh.shop_id = prior sh.parent_shop_id ) AS mien, ");
		sql.append("      ( SELECT sh.shop_name ");
		sql.append("       FROM   shop sh ");
		sql.append("       JOIN   channel_type cn ");
		sql.append("       ON     sh.shop_type_id = cn.channel_type_id ");
		sql.append("       AND    cn.type = 1 ");
		sql.append("       WHERE  1 = 1 ");
		sql.append("       AND    cn.status = 1 ");
		sql.append("       AND    decode_islevel_vnm(cn.object_type) = ? start WITH sh.shop_id = s.shop_id ");
		params.add(ShopObjectType.VUNG.getValue());
		sql.append("       AND    sh.status = 1 connect BY sh.shop_id = prior sh.parent_shop_id ) AS kenh,");
		
		sql.append("      (select staff_name from staff st where edr.staff_id = st.staff_id) as staffName  ");
		sql.append(" FROM   equip_delivery_record edr ");
		sql.append("        JOIN customer cus ");
		sql.append("          ON cus.customer_id = edr.customer_id ");
		sql.append("        JOIN shop s ");
		sql.append("          ON s.shop_id = Edr.to_object_id ");
		sql.append(" WHERE  1 = 1 ");
		if (idRecord != null) {
			sql.append("       AND edr.equip_delivery_record_id = ?");
			params.add(idRecord);
		}

		String[] fieldNames = { 
				"idRecord",					// 1
				"recordCode",				// 2
				"toPermanentAddress",
				"fromShopName",				// 3
				"fromShopAddess",			// 4
				"fromPhone",				// 5
				"fromFax",					// 6
				"fromRepresentative",		// 7
				"fromPosition",				// 8
				"toCustomerCode",			// 9
				"toCustomerName",			// 10
				"toObjectAddress",			// 11
				"toAddress",				// 12
				"toPhone",					// 13
				"toBusinessLicense",		// 14
				"toBusinessPlace",			// 15
				"toBusinessDate",			// 16
				"toRepresentative",			// 17
				"toPosition",				// 18
				"idNO",						// 19
				"idNOPlace",				// 20
				"idNODate",					// 21
				"contractDate",				// 22
				"numberContract",			// 23
				"statusRecord",				// 24
				"statusDelivery",			// 25
				"mien",
				"kenh",
				"staffName"
		};

		Type[] fieldTypes = { 
				StandardBasicTypes.LONG,	// 1
				StandardBasicTypes.STRING,	// 2
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,	// 3
				StandardBasicTypes.STRING,	// 4
				StandardBasicTypes.STRING,	// 5
				StandardBasicTypes.STRING,	// 6
				StandardBasicTypes.STRING,	// 7
				StandardBasicTypes.STRING,	// 8
				StandardBasicTypes.STRING,	// 9
				StandardBasicTypes.STRING,	// 10
				StandardBasicTypes.STRING,	// 11
				StandardBasicTypes.STRING,	// 12
				StandardBasicTypes.STRING,	// 13
				StandardBasicTypes.STRING,	// 14
				StandardBasicTypes.STRING,	// 15
				StandardBasicTypes.DATE,	// 16
				StandardBasicTypes.STRING,	// 17
				StandardBasicTypes.STRING,	// 18
				StandardBasicTypes.STRING,	// 19
				StandardBasicTypes.STRING,	// 20
				StandardBasicTypes.DATE,	// 21
				StandardBasicTypes.DATE,	// 22
				StandardBasicTypes.STRING,	// 23
				StandardBasicTypes.INTEGER,	// 24
				StandardBasicTypes.INTEGER,	// 25
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING
				
		};

		return repo.getListByQueryAndScalarFirst(EquipmentRecordDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public EquipDeliveryRecord getEquipDeliveryRecordById(Long idRecord) throws DataAccessException {
		// TODO Auto-generated method stub
		return repo.getEntityById(EquipDeliveryRecord.class, idRecord);
	}

	@Override
	public List<EquipDeliveryRecDtl> getListEquipDeliveryRecDtlByEquipDeliveryRecId(Long idRecord) throws DataAccessException {
		// TODO Auto-generated method stub
		if (idRecord == null) {
			throw new IllegalArgumentException("equipRecordDeliveryId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_delivery_rec_dtl where equip_delivery_record_id = ? ");
		params.add(idRecord);
		return repo.getListBySQL(EquipDeliveryRecDtl.class, sql.toString(), params);
	}

	@Override
	public EquipDeliveryRecDtl getEquipDeliveryRecDtlByFilter(EquipmentFilter<EquipDeliveryRecDtl> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select edrd.* ");
		sql.append(" from equip_delivery_rec_dtl edrd ");
		sql.append(" join equipment eq on eq.equip_id = edrd.equip_id ");
		sql.append(" where 1=1 ");
		if(filter.getIdRecordDelivery() != null){
			sql.append(" and edrd.equip_delivery_record_id = ? ");
			params.add(filter.getIdRecordDelivery());
		}
		if(filter.getId() != null){
			sql.append(" and eq.equip_id = ? ");
			params.add(filter.getId());
		}
		if (filter.getIdRecordDelivery() != null) {
			sql.append(" and edrd.EQUIP_DELIVERY_RECORD_ID = ? ");
			params.add(filter.getIdRecordDelivery());
		}
		if(!StringUtility.isNullOrEmpty(filter.getCode())){
			sql.append(" and eq.code = ? ");
			params.add(filter.getCode().toUpperCase());
		}
		if(!StringUtility.isNullOrEmpty(filter.getSeriNumber())){
			sql.append(" and eq.serial = ? ");
			params.add(filter.getSeriNumber().toUpperCase());
		}
		return repo.getEntityBySQL(EquipDeliveryRecDtl.class, sql.toString(), params);
	}

	@Override
	public List<EquipStockTransFormDtl> getListEquipStockTransFormDtlById(Long id) throws DataAccessException {
		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_stock_trans_form_dtl where equip_stock_trans_form_id = ? ");		
		params.add(id);
		return repo.getListBySQL(EquipStockTransFormDtl.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipmentRecordDeliveryVO> getListRecordDeliveryVOForEquipLendByFilter(EquipmentFilter<EquipmentRecordDeliveryVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		if (filter == null) {
			throw new IllegalArgumentException("filter == null");
		}
		if (StringUtility.isNullOrEmpty(filter.getCurShopCode())) {
			throw new IllegalArgumentException("filter.getCurShopCode() == null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH lstDeliveryRecord AS ");
		sql.append(" (SELECT edr.equip_delivery_record_id AS idRecord, ");
		sql.append(" edr.code AS recordCode, ");
		sql.append(" cus.CUSTOMER_ID AS customerId, ");
		sql.append(" cus.short_code AS customerCode, ");
		sql.append(" cus.customer_name AS customerName, ");
		sql.append(" cus.address AS customerAddress, ");
		sql.append(" lstShop.shop_code AS shopCode, ");
		sql.append(" lstShop.shop_name AS shopName, ");
		sql.append(" TO_CHAR(edr.create_date, 'dd/mm/yyyy') AS createDate, ");
		sql.append(" edr.record_status AS statusRecord, ");
		sql.append(" edr.delivery_status AS statusDelivery, ");
		sql.append(" edr.print_user AS printUser, ");
		sql.append(" DECODE(edr.print_user, NULL, 0, 1) AS statusPrint, ");
		sql.append(" el.code AS equipLendCode, ");
		sql.append(" Row_Number () Over (Partition BY cus.short_code, edr.record_status Order By edr.create_date DESC) AS stt, ");
		sql.append(" DECODE(edr.record_status, ?,0 ,1) num ");
		params.add(StatusRecordsEquip.CANCELLATION.getValue());
		sql.append(" FROM equip_delivery_record edr ");
		sql.append(" JOIN customer cus ");
		sql.append(" ON cus.customer_id = edr.customer_id ");
		sql.append(" AND cus.STATUS = 1 ");
		sql.append(" JOIN ");
		sql.append(" (SELECT * ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status = 1 ");
		sql.append(" START WITH shop_code = ? ");
		params.add(filter.getCurShopCode());
		sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id ");
		sql.append(" ) lstShop ");
		sql.append(" ON lstShop.shop_id = cus.shop_id ");
		sql.append(" LEFT JOIN equip_lend el ");
		sql.append(" ON el.EQUIP_LEND_ID = edr.EQUIP_LEND_ID ");
		sql.append(" WHERE 1 = 1 ");
		if (filter.getEquipLendId() != null) {
			sql.append(" AND edr.EQUIP_LEND_ID = ? ");
			params.add(filter.getEquipLendId());
		}
		sql.append(" ) ");
		sql.append(" SELECT * ");
		sql.append(" FROM lstDeliveryRecord ");
		sql.append(" WHERE customerCode in (SELECT customerCode FROM lstDeliveryRecord ldr ");
		sql.append(" WHERE ldr.stt = 1 ");
		sql.append(" GROUP BY customerCode ");
		sql.append(" HAVING SUM(ldr.num) = 0) ");
		sql.append(" and stt = 1 ");
		
		String[] fieldNames = { "idRecord", // 0
								"recordCode", // 1
								"customerId", // 5
								"customerCode", // 5
								"customerName", // 6
								"customerAddress", // 7
								"createDate", // 8
								"shopCode", // 9
								"shopName", // 10				
								"statusRecord", // 15
								"statusDelivery", // 16
								"statusPrint", // 17
								"equipLendCode" // 18
							};

		Type[] fieldTypes = { 	StandardBasicTypes.LONG, // 0
								StandardBasicTypes.STRING, // 1
								StandardBasicTypes.LONG, // 5
								StandardBasicTypes.STRING, // 5
								StandardBasicTypes.STRING, // 6
								StandardBasicTypes.STRING, // 7
								StandardBasicTypes.STRING, // 8
								StandardBasicTypes.STRING, // 9
								StandardBasicTypes.STRING, // 10
								StandardBasicTypes.INTEGER, // 15
								StandardBasicTypes.INTEGER, // 16
								StandardBasicTypes.INTEGER, // 17
								StandardBasicTypes.STRING // 18
							};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");
		sql.append(" ORDER BY recordCode, customerCode ");

		return repo.getListByQueryAndScalar(EquipmentRecordDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/**
	 * Lay giam sat trong bien ban giao nhan gan nhat
	 * 
	 * @author hunglm16
	 * @param [equipId]: id Thiet bi
	 * @param [customerId]: id Khach Hang
	 * @since August 19, 2015
	 * */
	@Override
	public Staff getStaffInEquipDelivRecordApproved(Long equipId, Long customerId) throws DataAccessException {
		if (equipId == null) {
			throw new IllegalArgumentException("Id equipId Is Null");
		}
		if (customerId == null) {
			throw new IllegalArgumentException("Id customerId Is Null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT distinct edr.* ");
		sql.append(" FROM equip_delivery_record edr  ");
		sql.append(" join equip_delivery_rec_dtl edd on edr.equip_delivery_record_id = edd.equip_delivery_record_id ");
		sql.append(" where edr.create_form_date is not null ");
		sql.append(" and edr.customer_id = ? ");
		params.add(customerId);
		sql.append(" and edd.equip_id = ? ");
		params.add(equipId);
		sql.append(" and edr.record_status = ? ");
		params.add(StatusRecordsEquip.APPROVED.getValue());
		sql.append(" and edr.staff_id is not null ");
		sql.append(" order by edr.create_form_date desc, edr.create_date desc ");
		List<EquipDeliveryRecord> res = repo.getListBySQL(EquipDeliveryRecord.class, sql.toString(), params);
		if (res != null && !res.isEmpty()) {
			return res.get(0).getStaff();//Lay gia tri dau tien lay duoc
		}
		return null;
		
	}
}

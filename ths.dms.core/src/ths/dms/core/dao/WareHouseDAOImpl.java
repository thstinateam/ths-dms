package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.filter.StockTotalFilter;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class WareHouseDAOImpl implements WareHouseDAO {
	
	@Autowired
	private IRepository repo;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public Warehouse getWarehouseById(Long id) throws DataAccessException {
		return repo.getEntityById(Warehouse.class, id);
	}
	
	@Override
	public List<Warehouse> getListWarehouseByFilter(StockTotalFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from warehouse wh where 1 = 1 ");
		if (filter.getOwnerId() != null) {
			sql.append(" and wh.shop_id = ? ");
			params.add(filter.getOwnerId());
		}
		if (filter.getWarehouseType() != null) {
			sql.append(" and wh.warehouse_type = ? ");
			params.add(filter.getWarehouseType().getValue());
		}
		if (filter.getStatus() != null) {
			sql.append(" and wh.status = ? ");
			params.add(filter.getStatus());
		}
		sql.append(" order by seq ");
		return repo.getListBySQL(Warehouse.class, sql.toString(), params);
	}
	@Override
	public Warehouse getWarehouseByCode(String code,Long shopId) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from warehouse where warehouse_code = ? and shop_id  = ? ";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(shopId);
		return repo.getEntityBySQL(Warehouse.class, sql, params);
	}
	
	@Override
	public List<Warehouse> getListWarehouseByFilterChecking(StockTotalFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select  wh.* ");
		sql.append(" from EQUIP_STATISTIC_UNIT esu");
		sql.append(" join warehouse wh on esu.UNIT_id = wh.shop_id");
		sql.append(" join shop sh on sh.shop_id = esu.UNIT_id");
		sql.append(" where 1=1 and wh.status =1 and esu.status =1");
		if(filter.getIdChecking()!=null){
			sql.append(" and esu.EQUIP_STATISTIC_RECORD_ID = ?  ");
			params.add(filter.getIdChecking());
		}
		if(filter.getOwnerId() != null){
			sql.append("and esu.UNIT_id in (select shop_id ");
			sql.append(" from shop");
			sql.append(" where 1=1");
			sql.append(" and status = 1 ");
			sql.append(" start with shop_id = ? connect by prior shop_id = parent_shop_id )");
			params.add(filter.getOwnerId());
		}
		sql.append(" order by warehouse_code ");
		return repo.getListBySQL(Warehouse.class, sql.toString(), params);
	}
}

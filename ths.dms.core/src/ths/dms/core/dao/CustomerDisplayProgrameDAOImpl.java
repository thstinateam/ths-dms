package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.CustomerDisplayPrograme;
import ths.dms.core.entities.StDisplayProgramLevel;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.CustomerDisplayProgrameVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class CustomerDisplayProgrameDAOImpl implements CustomerDisplayProgrameDAO {

	@Autowired
	private IRepository repo;

	@Override
	public CustomerDisplayPrograme createCustomerDisplayPrograme(CustomerDisplayPrograme customerDisplayPrograme) throws DataAccessException {
		if (customerDisplayPrograme == null) {
			throw new IllegalArgumentException("customerDisplayPrograme");
		}
		repo.create(customerDisplayPrograme);
		return repo.getEntityById(CustomerDisplayPrograme.class, customerDisplayPrograme.getId());
	}

	@Override
	public void deleteCustomerDisplayPrograme(CustomerDisplayPrograme customerDisplayPrograme) throws DataAccessException {
		if (customerDisplayPrograme == null) {
			throw new IllegalArgumentException("customerDisplayPrograme");
		}
		repo.delete(customerDisplayPrograme);
	}

	@Override
	public CustomerDisplayPrograme getCustomerDisplayProgrameById(long id) throws DataAccessException {
		return repo.getEntityById(CustomerDisplayPrograme.class, id);
	}
	
	@Override
	public void updateCustomerDisplayPrograme(CustomerDisplayPrograme customerDisplayPrograme) throws DataAccessException {
		if (customerDisplayPrograme == null) {
			throw new IllegalArgumentException("customerDisplayPrograme");
		}
		repo.update(customerDisplayPrograme);
	}

	@Override
	public CustomerDisplayPrograme getCustomerDisplayPrograme(
			CustomerDisplayPrograme cd,Boolean checkAll) throws DataAccessException {
		// TODO Auto-generated method stub
		if (cd == null) {
			throw new IllegalArgumentException("CustomerDisplayPrograme not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM DISPLAY_CUSTOMER_MAP cd ");
		sql.append(" WHERE CD.STATUS=1 ");
		sql.append(" AND cd.CUSTOMER_ID=? ");
		params.add(cd.getCustomer().getId());
		if(cd.getFromDate()!=null){
			sql.append("and trunc(cd.from_date,'MM') = trunc(?,'MM') ");
			params.add(cd.getFromDate());
		}		
		sql.append(" AND CD.DISPLAY_PROGRAM_CODE=? ");
		params.add(cd.getDisplayProgramCode());
		if(checkAll){
			sql.append(" AND CD.STAFF_ID=? ");
			params.add(cd.getStaff().getId());
			sql.append(" AND CD.LEVEL_CODE=? ");
			params.add(cd.getLevelCode());
		}
		return repo.getEntityBySQL(CustomerDisplayPrograme.class, sql.toString(), params);
	}
	
	@Override
	public List<Customer> getListCustomerInLevel(KPaging<Customer> kPaging, String displayProgramCode, String levelCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" Select c.*");
		sql.append(" from CUSTOMER_DISPLAY_PROGRAME cdp inner join CUSTOMER c on CDP.CUSTOMER_ID = c.CUSTOMER_ID");
		if(!StringUtility.isNullOrEmpty(displayProgramCode)) {
			sql.append(" Where CDP.display_programe_code = ?");
			params.add(displayProgramCode);
		}
		if(!StringUtility.isNullOrEmpty(levelCode)) {
			sql.append(" and CDP.level_code = ? ");
			params.add(levelCode);
		}
		if(kPaging != null) {
			return repo.getListBySQLPaginated(Customer.class, sql.toString(), params, kPaging);
		} else {
			return repo.getListBySQL(Customer.class, sql.toString(), params);
		}
	}
	
	@Override
	public List<CustomerDisplayProgrameVO> getListCustomerDisplayPrograme(
			KPaging<CustomerDisplayProgrameVO> kPaging, Long shopId,
			Long id, String month)
			throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlFrom = new StringBuilder();
		StringBuilder sqlCount = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
//		sql.append(" select cdp.st_display_customer_map_id as id,");
//		sql.append(" dp.display_program_code as displayProgramCode,");
//		sql.append(" dpl.level_code as levelCode,");
//		sql.append(" s.shop_code as shopCode,");
//		sql.append(" (st.staff_code || '-' || st.name) as staffName,");
//		sql.append(" (st.name) as staffNameEx,");
//		sql.append(" st.staff_code as staffCode,");
//		sql.append(" substr(c.customer_code,0,3) as customerCode,");
//		sql.append(" (substr(c.customer_code,0,3) || '-' || c.customer_name) as customerName,");
//		sql.append(" c.customer_name as customerNameEx,");
//		sql.append(" c.housenumber as houseNumber,");
//		sql.append(" c.street as street,");
//		sql.append(" to_char(cdp.from_date,'DD/MM/YYYY') as fromDate,");
//		sql.append(" to_char(cdp.update_date,'DD/MM/YYYY') as updateDate,");
//		sql.append(" to_char(cdp.from_date,'MM/YYYY') as month");
//		sqlFrom.append(" from st_display_customer_map cdp join st_display_staff_map stm on stm.st_display_staff_map_id = cdp.st_display_staff_map_id ");
//		sqlFrom.append(" join st_display_prog_level dpl on dpl.st_display_prog_level_id = cdp.st_display_prog_level_id ");
//		sqlFrom.append(" join st_display_program stdp on stdp.st_display_program_id = dpl.st_display_program_id ");
//		sqlFrom.append(" join st_display_program stdp on stdp.st_display_program_id = stm.st_display_program_id ");
//		sqlFrom.append(" join customer c on c.customer_id = cdp.customer_id");
//		sqlFrom.append(" join staff st on st.staff_id = cdp.staff_id");
//		sqlFrom.append(" join shop s on s.shop_id = st.shop_id");
//		sqlFrom.append(" where 1=1");
//		if(!StringUtility.isNullOrEmpty(displayProgrameCode)){
//			sqlFrom.append(" and dp.display_program_code=?");
//			params.add(displayProgrameCode);
//		}
//		sqlFrom.append(" and cdp.status = 1 and dp.status =1");
//		sqlFrom.append(" and trunc(dp.from_date,'MM')<= trunc(to_date('01'|| ?,'DD/MM/YYYY'))");
//		sqlFrom.append(" and (trunc(dp.to_date,'MM')>= trunc(to_date('01'|| ?,'DD/MM/YYYY'))or dp.to_date is null)");
//		sqlFrom.append(" and trunc(cdp.from_date,'MM')= trunc(to_date('01'|| ?,'DD/MM/YYYY'))");
//		sqlFrom.append(" and s.shop_id in (select s1.shop_id from shop s1 start with s1.shop_id = ? connect by prior s1.shop_id = s1.parent_shop_id)");
//		sqlFrom.append(" order by cdp.display_program_code, cdp.level_code, s.shop_code, st.staff_code, c.customer_code");		
//		sql.append(sqlFrom.toString());
//		sqlCount.append(" select count(1) as count").append(sqlFrom.toString());		
//		params.add(month);
//		params.add(month);
//		params.add(month);
//		params.add(shopId);		
		
		sql.append(" SELECT dcm.display_customer_map_id AS id, ");
		sql.append(" st.display_program_code AS displayProgramCode, ");
		sql.append(" dpl.level_code AS levelCode, ");
		sql.append(" sh.shop_code AS shopCode, ");
		sql.append(" (sf.staff_code ");
		sql.append(" || '-' ");
		sql.append(" || sf.staff_name) AS staffName, ");
		sql.append(" (sf.staff_name) AS staffNameEx, ");
		sql.append(" sf.staff_code AS staffCode, ");
		sql.append(" SUBSTR(c.customer_code,0,3) AS customerCode, ");
		sql.append(" (SUBSTR(c.customer_code,0,3) ");
		sql.append(" || '-' ");
		sql.append(" || c.customer_name) AS customerName, ");
		sql.append(" c.customer_name AS customerNameEx, ");
		sql.append(" c.housenumber AS houseNumber, ");
		sql.append(" c.street AS street, ");
		sql.append(" TO_CHAR(dcm.from_date,'MM/YYYY') AS fromDate, ");
		sql.append(" TO_CHAR(dcm.update_date,'DD/MM/YYYY') AS updateDate, ");
		sql.append(" TO_CHAR(dcm.create_date,'DD/MM/YYYY') AS oneMONTH ");
		sql.append(" FROM display_program st ");
		sql.append(" JOIN display_staff_map dsm ");
		sql.append(" ON st.display_program_id = dsm.display_program_id ");
		if(id != null){
			sql.append(" AND st.display_program_id = ? ");
			params.add(id);
		}

		//sql.append(" AND st.status = 1 ");
		sql.append(" AND TRUNC(st.from_date,'MM')<= TRUNC(to_date('01'|| ?,'DD/MM/YYYY')) ");
		sql.append(" AND (TRUNC(st.to_date,'MM') >= TRUNC(to_date('01'|| ?,'DD/MM/YYYY')) ");
		sql.append(" OR st.to_date IS NULL) ");
		sql.append(" JOIN display_customer_map dcm ");
		sql.append(" ON dcm.display_staff_map_id = dsm.display_staff_map_id ");
		sql.append(" AND TRUNC(dcm.from_date,'MM') = TRUNC(to_date('01'|| ?,'DD/MM/YYYY')) ");
		sql.append(" AND dcm.status = 1 ");
		params.add(month);
		params.add(month);
		params.add(month);
		sql.append(" JOIN display_program_level dpl ");
		sql.append(" ON dpl.display_program_level_id = dcm.display_program_level_id ");
		sql.append(" JOIN staff sf ");
		sql.append(" ON sf.staff_id = dsm.staff_id ");
		sql.append(" AND sf.status = 1 ");
		sql.append(" JOIN shop sh ");
		sql.append(" ON sf.shop_id = sh.shop_id ");
		sql.append(" JOIN customer c ");
		sql.append(" ON dcm.customer_id = c.customer_id ");
		sql.append(" AND c.status = 1 ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND sh.shop_id IN ");
		sql.append(" (SELECT s1.shop_id ");
		sql.append(" FROM shop s1 ");
		sql.append(" START WITH s1.shop_id = ? ");
		sql.append(" CONNECT BY prior s1.shop_id = s1.parent_shop_id ");
		sql.append(" ) ");
		sql.append(" ORDER BY st.display_program_code, ");
		sql.append(" dpl.level_code, ");
		sql.append(" sh.shop_code, ");
		sql.append(" sf.staff_code, ");
		sql.append(" c.customer_code ");
		sqlCount.append(" select count(1) as count from (").append(sql.toString());
		sqlCount.append(" )");

		params.add(shopId);
		
		String[] fieldNames = { "id", 
				"displayProgramCode", 
				"levelCode", 
				"shopCode", 
				"staffName",
				"staffNameEx",
				"staffCode",
				"customerCode",
				"customerName",
				"customerNameEx",
				"houseNumber",
				"street",
				"fromDate",
				"updateDate",
				"oneMonth"
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING
				 
		};
//		if(kPaging != null) {
//			return repo.getListBySQLPaginated(CustomerDisplayProgrameVO.class, sql.toString(), params, kPaging);
//		} else {
//			return repo.getListBySQL(CustomerDisplayProgrameVO.class, sql.toString(), params);
//		}
		if (kPaging == null){
			return repo.getListByQueryAndScalar(CustomerDisplayProgrameVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
		} else{
			return repo.getListByQueryAndScalarPaginated(
					CustomerDisplayProgrameVO.class, fieldNames, fieldTypes,
					sql.toString(), sqlCount.toString(), params, params,
					kPaging);
		}
		
	}
	
	@Override
	public List<CustomerDisplayProgrameVO> getListCustomerDisplayProgrameEX(
			KPaging<CustomerDisplayProgrameVO> kPaging, List<Long> shopId,
			Long id, String month, boolean checkMap, long parentStaffId)
			throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		//StringBuilder sqlFrom = new StringBuilder();
		StringBuilder sqlCount = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
	
		
		sql.append(" SELECT dcm.display_customer_map_id AS id, ");
		sql.append(" st.display_program_code AS displayProgramCode, ");
		sql.append(" dpl.level_code AS levelCode, ");
		sql.append(" sh.shop_code AS shopCode, ");
		sql.append(" (sf.staff_code ");
		sql.append(" || '-' ");
		sql.append(" || sf.staff_name) AS staffName, ");
		sql.append(" (sf.staff_name) AS staffNameEx, ");
		sql.append(" sf.staff_code AS staffCode, ");
		sql.append(" SUBSTR(c.customer_code,0,3) AS customerCode, ");
		sql.append(" (SUBSTR(c.customer_code,0,3) ");
		sql.append(" || '-' ");
		sql.append(" || c.customer_name) AS customerName, ");
		sql.append(" c.customer_name AS customerNameEx, ");
		sql.append(" c.housenumber AS houseNumber, ");
		sql.append(" c.street AS street, ");
		sql.append(" TO_CHAR(dcm.from_date,'MM/YYYY') AS fromDate, ");
		sql.append(" TO_CHAR(dcm.update_date,'DD/MM/YYYY') AS updateDate, ");
		sql.append(" TO_CHAR(dcm.create_date,'DD/MM/YYYY') AS oneMONTH ");
		sql.append(" FROM display_program st ");
		sql.append(" JOIN display_staff_map dsm ");
		sql.append(" ON st.display_program_id = dsm.display_program_id ");
		if(id != null){
			sql.append(" AND st.display_program_id = ? ");
			params.add(id);
		}

		//sql.append(" AND st.status = 1 ");
		sql.append(" AND TRUNC(st.from_date,'MM')<= TRUNC(to_date('01'|| ?,'DD/MM/YYYY')) ");
		sql.append(" AND (TRUNC(st.to_date,'MM') >= TRUNC(to_date('01'|| ?,'DD/MM/YYYY')) ");
		sql.append(" OR st.to_date IS NULL) ");
		sql.append(" JOIN display_customer_map dcm ");
		sql.append(" ON dcm.display_staff_map_id = dsm.display_staff_map_id ");
		sql.append(" AND TRUNC(dcm.from_date,'MM') = TRUNC(to_date('01'|| ?,'DD/MM/YYYY')) ");
		sql.append(" AND dcm.status = 1 ");
		params.add(month);
		params.add(month);
		params.add(month);
		sql.append(" JOIN display_program_level dpl ");
		sql.append(" ON dpl.display_program_level_id = dcm.display_program_level_id ");
		sql.append(" JOIN staff sf ");
		sql.append(" ON sf.staff_id = dsm.staff_id ");
		sql.append(" AND sf.status = 1 ");
		sql.append(" JOIN shop sh ");
		sql.append(" ON sf.shop_id = sh.shop_id ");
		sql.append(" JOIN customer c ");
		sql.append(" ON dcm.customer_id = c.customer_id ");
		sql.append(" AND c.status = 1 ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND sh.shop_id IN ");
		sql.append(" (SELECT s1.shop_id ");
		sql.append(" FROM shop s1 ");
		sql.append(" START WITH s1.shop_id in ('' ");
		for(int i=0;i<shopId.size();i++){
			sql.append(" ,? ");
			params.add(shopId.get(i));
		}
		sql.append(" ) ");
		sql.append(" CONNECT BY prior s1.shop_id = s1.parent_shop_id ");
		sql.append(" ) ");
		
		if (checkMap) {
			sql.append("and sf.staff_id in (");
			sql.append(" select psm.staff_id from parent_staff_map psm");
			sql.append(" where psm.status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" start with psm.parent_staff_id = ? and psm.status = ? connect by prior psm.staff_id = psm.parent_staff_id");
			params.add(parentStaffId);
			params.add(ActiveType.RUNNING.getValue());
			sql.append(")");
		}
		sql.append(" and not exists (select 1 from exception_user_access where status = ? and parent_staff_id = ? and staff_id = sf.staff_id)");
		params.add(ActiveType.RUNNING.getValue());
		params.add(parentStaffId);
		
		sql.append(" ORDER BY st.display_program_code, ");
		sql.append(" dpl.level_code, ");
		sql.append(" sh.shop_code, ");
		sql.append(" sf.staff_code, ");
		sql.append(" c.customer_code ");
		sqlCount.append(" select count(1) as count from (").append(sql.toString());
		sqlCount.append(" )");
		
		String[] fieldNames = { "id", 
				"displayProgramCode", 
				"levelCode", 
				"shopCode", 
				"staffName",
				"staffNameEx",
				"staffCode",
				"customerCode",
				"customerName",
				"customerNameEx",
				"houseNumber",
				"street",
				"fromDate",
				"updateDate",
				"oneMonth"
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING
				 
		};

		if (kPaging == null){
			return repo.getListByQueryAndScalar(CustomerDisplayProgrameVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
		} else{
			return repo.getListByQueryAndScalarPaginated(
					CustomerDisplayProgrameVO.class, fieldNames, fieldTypes,
					sql.toString(), sqlCount.toString(), params, params,
					kPaging);
		}
		
	}

	@Override
	public void deleteListCustomerDisplayPrograme(
			List<Long> lstCustomerDisplayPrograme) throws DataAccessException {
		// TODO Auto-generated method stub
		if(lstCustomerDisplayPrograme==null){
			throw new IllegalArgumentException("lstCustomerDisplayPrograme not null");
		}else{
			if(lstCustomerDisplayPrograme.isEmpty()){
				throw new IllegalArgumentException("lstCustomerDisplayPrograme not empty");
			}
		}
		
		Integer count=lstCustomerDisplayPrograme.size()/900;
		count=count+1;
		for(int j=0;j<count;j++){
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append(" DELETE FROM DISPLAY_CUSTOMER_MAP cp ");
			
			sql.append(" WHERE CP.DISPLAY_CUSTOMER_MAP_ID in ( ");
			Boolean isFirt=true;
			for(int i=900*j;i<900*(j+1);i++){
				if(isFirt){
					sql.append(" ? ");
					isFirt=false;
				}else{
					sql.append(" ,? ");
				}
				params.add(lstCustomerDisplayPrograme.get(i));
				
				if(i>=(lstCustomerDisplayPrograme.size()-1)){
					break;
				}
			}
			sql.append(" ) ");
			repo.executeSQLQuery(sql.toString(), params);
		}
	}
	
	@Override
	public int countShopConnectBy(Long shopId)throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(*) from shop where shop_type = 1 ");
		sql.append("start with shop_id = ? ");
		sql.append("connect by prior shop_id = parent_shop_id");
		params.add(shopId);
		// int c =Integer.valueOf(repo.getObjectByQuery(sql.toString(), params).toString());
		BigDecimal value = (BigDecimal)repo.getObjectByQuery(sql.toString(), params); 
		return value.intValue();
	}
	
	@Override
	public StDisplayProgramLevel checkLevelCodeExists(Long id,String levelCode)throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from display_program_level ");
		sql.append("where display_program_id = ? ");
		sql.append("and level_code =  ?");
		params.add(id);
		params.add(levelCode);
//		BigDecimal value = (BigDecimal)repo.getObjectByQuery(sql.toString(), params); 
//		return (value.intValue() == 0 ) ? false : true;
		return repo.getEntityBySQL(StDisplayProgramLevel.class, sql.toString(), params);
	}
	
	@Override
	public CustomerDisplayPrograme checkExistDisplayCustomer(Long id,Long customerId,Long staffId,String levelCode,String month)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT dcm.*");
		sql.append(" FROM display_program st ");
		sql.append(" JOIN display_staff_map dsm ");
		sql.append(" ON st.display_program_id = dsm.display_program_id ");
		sql.append(" AND st.display_program_id = ? ");
		params.add(id);
		sql.append(" AND st.status = 1 ");
		sql.append(" AND TRUNC(st.from_date,'MM')<= TRUNC(to_date(?,'DD/MM/YYYY')) ");
		sql.append(" AND (TRUNC(st.to_date,'MM') >= TRUNC(to_date(?,'DD/MM/YYYY')) ");
		sql.append(" OR st.to_date IS NULL) ");
		sql.append(" JOIN display_customer_map dcm ");
		sql.append(" ON dcm.display_staff_map_id = dsm.display_staff_map_id ");
		sql.append(" AND TRUNC(dcm.from_date,'MM') = TRUNC(to_date(?,'DD/MM/YYYY')) ");
		sql.append(" AND dcm.status = 1 ");
		params.add(month);
		params.add(month);
		params.add(month);
		sql.append(" JOIN display_program_level dpl ");
		sql.append(" ON dpl.display_program_level_id = dcm.display_program_level_id ");
		if(!StringUtility.isNullOrEmpty(levelCode)){
			sql.append(" and dpl.level_code = ? ");
			params.add(levelCode.toUpperCase());
		}
		if(staffId != null){
			sql.append(" JOIN staff sf ");
			sql.append(" ON sf.staff_id = dsm.staff_id ");
			sql.append(" AND sf.status = 1 and sf.staff_id = ? ");
			params.add(staffId);
		}

		
		sql.append(" JOIN customer c ");
		sql.append(" ON dcm.customer_id = c.customer_id ");
		sql.append(" AND c.status = 1 ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" and c.customer_id = ? ");
		params.add(customerId);
		sql.append("");

		
		return repo.getEntityBySQL(CustomerDisplayPrograme.class, sql.toString(), params);
	}
}

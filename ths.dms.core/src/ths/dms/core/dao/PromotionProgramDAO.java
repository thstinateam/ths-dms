package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import ths.dms.core.entities.GroupLevel;
import ths.dms.core.entities.GroupLevelDetail;
import ths.dms.core.entities.GroupMapping;
import ths.dms.core.entities.PPConvert;
import ths.dms.core.entities.PPConvertDetail;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductGroup;
import ths.dms.core.entities.PromotionCustomerMap;
import ths.dms.core.entities.PromotionProductOpen;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.PromotionStaffMap;
import ths.dms.core.entities.RptAccumulativePromotionProgram;
import ths.dms.core.entities.RptAccumulativePromotionProgramDetail;
import ths.dms.core.entities.RptCTTLPay;
import ths.dms.core.entities.RptCTTLPayDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductGroupType;
import ths.dms.core.entities.enumtype.PromotionProgramFilter;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.filter.CalcPromotionFilter;
import ths.dms.core.entities.filter.CalculatePromotionFilter;
import ths.dms.core.entities.filter.PromotionMapBasicFilter;
import ths.dms.core.entities.vo.CommercialSupportCodeVO;
import ths.dms.core.entities.vo.CommercialSupportVO;
import ths.dms.core.entities.vo.ExMapping;
import ths.dms.core.entities.vo.GroupLevelDetailVO;
import ths.dms.core.entities.vo.GroupLevelVO;
import ths.dms.core.entities.vo.LevelMappingVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.NewLevelMapping;
import ths.dms.core.entities.vo.NewProductGroupVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PPConvertVO;
import ths.dms.core.entities.vo.ProductGroupVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.entities.vo.PromoProductConvVO;
import ths.dms.core.entities.vo.PromotionCustAttrVO;
import ths.dms.core.entities.vo.PromotionProductOpenVO;
import ths.dms.core.entities.vo.PromotionProductsVO;
import ths.dms.core.entities.vo.PromotionProgramVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderPromotionRemainVO;
import ths.dms.core.entities.vo.SaleOrderVO;
import ths.dms.core.entities.vo.SubLevelMapping;
import ths.dms.core.entities.vo.ZV03View;
import ths.dms.core.entities.vo.ZV07View;
import ths.core.entities.vo.rpt.RptPromotionDetailDataVO;
import ths.core.entities.vo.rpt.RptPromotionProgramDetailVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface PromotionProgramDAO {

	PromotionProgram createPromotionProgram(PromotionProgram promotionProgram, LogInfoVO logInfo) throws DataAccessException;

	void deletePromotionProgram(PromotionProgram promotionProgram, LogInfoVO logInfo) throws DataAccessException;

	void updatePromotionProgram(PromotionProgram promotionProgram, LogInfoVO logInfo) throws DataAccessException;
	
	PromotionProgram getPromotionProgramById(long id) throws DataAccessException;

	PromotionProgram getPromotionProgramByCode(String code)
			throws DataAccessException;

	/**
	 * @author hungnm
	 * @param kPaging
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @param staffId
	 * @return
	 * @throws DataAccessException
	 */
	List<RptPromotionProgramDetailVO> getListRptPromotionProgramDetail(
			KPaging<RptPromotionProgramDetailVO> kPaging, Date fromDate,
			Date toDate, Long shopId, Long staffId) throws DataAccessException;
	
	/**
	 * Lay du lieu cho bao cao chi tiet khuyen mai
	 * 
	 * @param shopId Id shop
	 * @param staffIds List nhan vien BH
	 * @param fromDate Ngay bat dau chuong trinh khuyen mai
	 * @param toDate Ngay ket thuc chuong trinh khuyen mai
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<RptPromotionDetailDataVO> getDataForPromotionDetailReport(
			Long shopId, List<Long> staffIds, Date fromDate, Date toDate)
			throws DataAccessException;

	List<PromotionCustAttrVO> getListPromotionCustAttrVOCanBeSet(
			KPaging<PromotionCustAttrVO> kPaging, long promotionProgramId) throws DataAccessException;
	
	List<PromotionCustAttrVO> getListPromotionCustAttrVOAlreadySet(
			KPaging<PromotionCustAttrVO> kPaging, long promotionProgramId) throws DataAccessException;
	
	/**
	 * @author hungnm
	 * @param productId
	 * @param promotionProgramId
	 * @return
	 * @throws DataAccessException
	 */
	Boolean checkProductInOtherPromotionProgram(String productCode,
			Long promotionProgramId, Long promotionProgramDetailId)
			throws DataAccessException;

	Boolean checkProductInOtherPromotionProgram(String productCode,
			Date fromDate, Date toDate, Long promotionProgramId)
			throws DataAccessException;
	/**
	 * @author lochp
	 * @param promotionProgramId
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws DataAccessException
	 * function: checkExistProductTimeShopInActive
	 */
	Boolean checkExistProductTimeShopInActive(Long promotionProgramId, String startDate, String endDate) 
			throws DataAccessException;
	
	List<PromotionProgram> getCheckQuantityPromotionShopMap(Long saleOrderId)
			throws DataAccessException;

	List<PromotionProgram> getPromotionProgramByProductAndShopAndCustomer(
			long productId, long shopId, long customerId, Date orderDate,
			Long saleOrderId, Long staffId) throws DataAccessException;

	List<CommercialSupportVO> getListCommercialSupport(
			KPaging<CommercialSupportVO> kPaging, Long shopId, Long customerId,
			Date orderDate, Long saleOrderId) throws DataAccessException;
	
	List<CommercialSupportVO> getCommercialSupport(
			KPaging<CommercialSupportVO> kPaging, Long shopId, Long customerId,
			Date orderDate, Long saleOrderId, String commercialSupportCode) throws DataAccessException;

	List<Boolean> checkIfCommercialSupportExists(
			List<CommercialSupportCodeVO> lstCommercialSupportCodeVO,
			Date orderDate, Long saleOrderId) throws DataAccessException;

	List<ZV03View> getZV03ViewHeader(KPaging<ZV03View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV03View> getZV03ViewDetail(KPaging<ZV03View> paging,
			Long promotionProgramId, Long productId, Integer quant) throws DataAccessException;

	List<ZV03View> getZV06ViewHeader(KPaging<ZV03View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV03View> getZV06ViewDetail(KPaging<ZV03View> paging,
			Long promotionProgramId, Long productId, Long amount)
			throws DataAccessException;

	List<ZV07View> getZV07ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV07ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, Float discountPercent, Integer quantity)
			throws DataAccessException;

	List<ZV07View> getZV08ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV08ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal discountAmount, Integer quantity)
			throws DataAccessException;

	List<ZV07View> getZV09ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV09ViewSaleProductDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, Integer quantity)
			throws DataAccessException;

	List<ZV07View> getZV09ViewFreeProductDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, Integer quantity)
			throws DataAccessException;

	List<ZV07View> getZV10ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV10ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal amount)
			throws DataAccessException;

	List<ZV07View> getZV11ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV11ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal amount)
			throws DataAccessException;

	List<ZV07View> getZV12ViewFreeProductDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal amount)
			throws DataAccessException;

	List<ZV07View> getZV12ViewSaleProductDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal amount)
			throws DataAccessException;

	List<ZV07View> getZV12ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV13ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV13ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, Float discountPercent)
			throws DataAccessException;

	List<ZV07View> getZV14ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV14ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal discountAmount)
			throws DataAccessException;

	List<ZV07View> getZV15ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV15ViewSaleProductDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, String freeProductCodeStr)
			throws DataAccessException;

	List<ZV07View> getZV15ViewFreeProductDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, String productCodeStr)
			throws DataAccessException;

	List<ZV07View> getZV16ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV16ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, Float discountPercent)
			throws DataAccessException;

	List<ZV07View> getZV17ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV17ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal discountAmount)
			throws DataAccessException;

	List<ZV07View> getZV18ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV18ViewSaleProductDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, String freeProductCodeStr)
			throws DataAccessException;

	List<ZV07View> getZV18ViewFreeProductDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, String productCodeStr)
			throws DataAccessException;

	List<ZV07View> getZV21ViewHeader(KPaging<ZV07View> paging,
			Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV21ViewDetail(KPaging<ZV07View> paging,
			Long promotionProgramId, BigDecimal amount) throws DataAccessException;

	List<ZV07View> getZV01ViewHeader(Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV02ViewHeader(Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV04ViewHeader(Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV05ViewHeader(Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV19ViewHeader(Long promotionProgramId) throws DataAccessException;

	List<ZV07View> getZV20ViewHeader(Long promotionProgramId) throws DataAccessException;

	List<PromotionProgram> getListPromotionForOrder(Long shopId,
			Long customerId, Date orderDate, Long saleOrderId, Long staffId)
			throws DataAccessException;

	List<PromotionProgram> getListPromotionProgram(PromotionProgramFilter filter)
			throws DataAccessException;
	
	List<PromotionProgram> getListDefaultPromotionProgram(PromotionProgramFilter filter)
			throws DataAccessException;

	PromotionProgram getPromotionProgramByCodeByType(String code,
			ApParamType type)throws DataAccessException;

	List<PromotionProgram> getListPromotionProgramByCustomerType(
			Long customerTypeId) throws DataAccessException;

	ProductGroup createProductGroup(ProductGroup productGroup)
			throws DataAccessException;

	ProductGroup getProductGroupByCode(String productGroup, ProductGroupType groupType,
			Long promotionProgramId) throws DataAccessException;

	GroupLevel createGroupLevel(GroupLevel groupLevel)
			throws DataAccessException;

	GroupMapping createGroupMapping(GroupMapping groupMapping)
			throws DataAccessException;

	GroupLevelDetail createGroupLevelDetail(GroupLevelDetail groupLevelDetail)
			throws DataAccessException;

	void updateProductGroup(ProductGroup productGroup)
			throws DataAccessException;

	void updateGroupLevel(GroupLevel groupLevel) throws DataAccessException;

	void updateGroupLevelDetail(GroupLevelDetail groupLevelDetail)
			throws DataAccessException;

	void updateGroupMapping(GroupMapping groupMapping)
			throws DataAccessException;

	/*ProductGroup getProductGroupByMaxAmount(BigDecimal maxAmount,
			Long promotionProgramId) throws DataAccessException;*/

	List<ProductGroup> getListProductGroupByPromotionId(Long promotionId, ProductGroupType groupType)
			throws DataAccessException;

	List<GroupLevel> getListGroupLevelByGroupId(Long groupId)
			throws DataAccessException;

	List<GroupLevelDetail> getListGroupLevelDetailByLevelId(Long levelId)
			throws DataAccessException;

	List<GroupMapping> getListGroupMappingBySaleGroupId(Long saleGroupId)
			throws DataAccessException;

	List<GroupMapping> getListGroupMappingByPromotionGroupId(
			Long promotionGroupId) throws DataAccessException;
	
	GroupMapping getGroupMappingByProductGroupAndLevel(Long productGroupId, Long groupLevelId) throws DataAccessException;

	/*ProductGroup getProductGroupByPercent(Float percent, Long promotionProgramId)
			throws DataAccessException;*/

	GroupLevel getGroupLevelByMaxAmount(Long groupProductId,
			BigDecimal maxAmount) throws DataAccessException;

	GroupLevel getGroupLevelByPercent(Long groupProductId, Float percent)
			throws DataAccessException;
	
	GroupLevel getGroupLevel(Long productGroupId, Long groupLevelId) throws DataAccessException;

	ProductGroup getProductGroup(Long groupId) throws DataAccessException;

	void deleteProductGroup(ProductGroup productGroup)
			throws DataAccessException;

	List<GroupLevelVO> getListGroupLevelVO(Long groupId)
			throws DataAccessException;

	List<GroupLevelDetailVO> getListGroupLevelDetailVO(Long levelId)
			throws DataAccessException;

	/**
	 *Tinh KM don hang
	 * @author: nhanlt6
	 * @return: void
	 * @param orderAmount
	 * @param sortListProductSale
	 * @param sortListOutPut
	 * @param listProductPromotionsale
	 * @param shopId
	 * @param keyList
	 * @param customerId
	 * @param customerTypeId
	 * @param orderDate
	 * @param saleOrderId
	 * @return: PromotionProductsVO
	 * @since 11/09/2014
	 */
	PromotionProductsVO calPromotionForOrder(
			Map<Long, List<SaleOrderLot>> mapSaleOrderLot,BigDecimal orderAmount,
			SortedMap<Long, SaleOrderDetailVO> sortListProductSale,
			SortedMap<Long, List<SaleOrderDetailVO>> sortListOutPut,
			List<SaleOrderDetailVO> listProductPromotionsale,
			Long shopId,Long keyList,Long customerId,Long customerTypeId,Long staffId, Date orderDate, Long saleOrderId,
			SaleOrderVO orderVO) throws DataAccessException;
	
	/**
	 * Lay ds san pham khuyen mai tu san pham ban
	 * @author: NhanLT6
	 * @param calculatePromotionFilter
	 * @return PromotionProductsVO
	 * @since 11/09/2014
	 */
	PromotionProductsVO calculatePromotionProducts2(CalculatePromotionFilter calculatePromotionFilter) throws DataAccessException;
	
	/**
	 * Tinh KM cho don hang
	 * @author: dungnt19
	 * @since: 1.0
	 * @return: void
	 * @throws:  
	 * @param calcPromotionFilter
	 * @updated : 01/10/2014
	 */
	SaleOrderDetailVO calcPromotion(CalcPromotionFilter calcPromotionFilter) throws DataAccessException;
	
	void clearPromotionProgram(PromotionProgram promotionProgram, LogInfoVO logInfo) throws DataAccessException;
	
	/**
	 * Lay ds muc cua 1 nhom
	 * @author: nhanlt6
	 * @since: 18/09/2014
	 * @param groupId
	 * @return
	 */
	List<GroupLevel> getGroupLevelsOfProductGroup(long groupId, int isPromotionProduct) throws DataAccessException;

	List<LevelMappingVO> getListLevelMappingByGroupId(Long promotionId, Long groupMuaId, Long groupKMId) throws DataAccessException;

	List<ProductGroupVO> getListProductGroupVO(Long promotionId,
			ProductGroupType groupType) throws DataAccessException;

	GroupMapping getGroupMappingByGroupCodeAndOrder(Long promotionId, String groupMuaCode,
			Integer orderLevelMua, String groupKMCode, Integer orderLevelKM)
			throws DataAccessException;

	GroupMapping getGroupMappingById(Long id) throws DataAccessException;

	GroupLevel getGroupLevelByOrder(Integer order, Long productGroupId)
			throws DataAccessException;

	List<GroupMapping> getGroupMappingByGroupCodeAndOrderMua(Long promotionId,
			String groupMuaCode, Integer orderLevelMua)
			throws DataAccessException;

	GroupLevel getGroupLevelById(Long groupLevelId) throws DataAccessException;

	GroupLevelDetail getGroupLevelDetailByLevelIdProductId(Long groupLevelId,
			Long productId) throws DataAccessException;

	List<GroupMapping> getListGroupMappingByLevelId(Long levelMuaId,
			Long levelKMId) throws DataAccessException;

	GroupLevelDetail getGroupLevelDetailById(Long id)
			throws DataAccessException;
	
	/**
	 * get base quantity of promotive product for changing promotive product when editing sale order
	 * @author tuannd20
	 * @param saleOrderId
	 * @param productCode
	 * @param programCode
	 * @param gotPromoLevel
	 * @return
	 * @throws DataAccessException
	 */
	int getBasePromotiveProductQuantityForEditingSaleOrder(Long saleOrderId, String productCode, String programCode, Integer gotPromoLevel) throws DataAccessException;
	int getBasePromotiveProductQuantityForEditingSaleOrder2(Long saleOrderId, String productCode, String programCode, Long promoLevelId, Integer gotPromoLevel) throws DataAccessException;
	
	/**
	 * get promotive product in same group of checking product
	 * @author tuannd20
	 * @param shopId
	 * @param saleOrderId
	 * @param productCode
	 * @param programCode
	 * @param gotPromoLevel
	 * @return
	 * @throws DataAccessException
	 * @date 10/10/2014
	 */
	List<SaleOrderDetailVOEx> getPromotiveProductForEditingSaleOrder(Long shopId, Long saleOrderId, String productCode, String programCode, Integer gotPromoLevel) throws DataAccessException;
	List<SaleOrderDetailVOEx> getPromotiveProductForEditingSaleOrder2(Long shopId, Long saleOrderId, String productCode, String programCode, Long promoLevelId, Integer gotPromoLevel) throws DataAccessException;

	List<GroupLevel> getListLevelNotInMapping(Long promotionId)
			throws DataAccessException;

	PromotionProgram checkProductExistInOrPromotion(Long promotionId)
			throws DataAccessException;
	
	List<NewProductGroupVO> getListNewProductGroupByPromotionId(Long promotionId)
	throws DataAccessException;
	
	List<SubLevelMapping> getListGroupLevelDetailVOEx(Long levelId)
		throws DataAccessException;
	
	List<ExMapping> getListSubLevelByMapping(Long levelId)
		throws DataAccessException;
	
	ObjectVO<NewLevelMapping> getListMappingLevel(Long groupMuaId, Long groupKMId, Integer fromLevel, Integer toLevel)
		throws DataAccessException;
	
	GroupLevel getGroupLevelByLeveCode(Long groupId, String levelCode, Integer orderNumber)
		throws DataAccessException;
	
	List<GroupLevel> getListSubLevelByParentId(Long parentLevelId)
		throws DataAccessException;
	
	/**
	 * lay ds tat ca cac CTKM tich luy ma KH tham gia
	 * @author tuannd20
	 * @param shopId
	 * @param customerId
	 * @param staffId
	 * @param orderDate
	 * @return
	 * @throws DataAccessException
	 * @date 10/12/2014
	 */
	List<PromotionProgram> getAccumulativePromotionProgramByProductAndShopAndCustomer(Long shopId, Long customerId, Long staffId, Date orderDate) throws DataAccessException;
	
	/**
	 * lay thong tin cau truc khai bao CTKM theo loai nhom mua/nhom KM
	 * @author tuannd20
	 * @param promotionProgramId
	 * @param groupType
	 * @return
	 * @throws DataAccessException
	 * @date 10/12/2014
	 */
	List<ProductGroup> getPromotionProgramStructure(Long promotionProgramId, ProductGroupType groupType) throws DataAccessException;
	
	/**
	 * lay cau truc mapping giua (nhom mua, nhom KM) trong khai bao CTKM
	 * @author tuannd20
	 * @param promotionProgramId
	 * @param buyGroupId
	 * @return
	 * @throws DataAccessException
	 * @date 10/12/2014
	 */
	List<GroupMapping> getPromotionProgramParentLevelStructures(Long promotionProgramId, Long buyGroupId) throws DataAccessException;

	List<PPConvertVO> listPromotionProductConvertVO(Long promotionId, String name)
			throws DataAccessException;

	PPConvert getPPConvertById(Long id) throws DataAccessException;

	PPConvertDetail getPPConvertDetailById(Long id) throws DataAccessException;

	PPConvert createPPConvert(PPConvert ppConvert) throws DataAccessException;

	void updatePPConvert(PPConvert ppConvert) throws DataAccessException;

	PPConvertDetail createPPConvertDetail(PPConvertDetail ppConvertDetail)
			throws DataAccessException;

	void updatePPConvertDetail(PPConvertDetail ppConvertDetail)
			throws DataAccessException;

	List<PromotionProductOpenVO> listPromotionProductOpenVO(Long promotionId)
			throws DataAccessException;

	PromotionProductOpen getPromotionProductOpen(Long id)
			throws DataAccessException;

	PromotionProductOpen createPromotionProductOpen(
			PromotionProductOpen promotionProductOpen)
			throws DataAccessException;

	void updatePromotionProductOpen(PromotionProductOpen promotionProductOpen)
			throws DataAccessException;

	List<GroupMapping> getlistMappingLevel(Long groupMuaId, Long groupKMId)
			throws DataAccessException;

	Map<Long, List<SubLevelMapping>> getListMapDetail(Long parentId,
			ProductGroupType type) throws DataAccessException;
	
	/**
	 * Lay danh sach CTHTTM co trong don hang
	 * 
	 * @author lacnv1
	 * @since Dec 10, 2014
	 */
	List<CommercialSupportVO> getListCommercialSupportOfSaleOrder(long saleOrderId, KPaging<CommercialSupportVO> paging) throws DataAccessException;

	/**
	 * lay thong tin CTTL tu bang tong hop
	 * @author tuannd20
	 * @param shopId
	 * @param promotionProgramId
	 * @param customerId
	 * @param orderDate
	 * @return
	 * @date 10/12/2014
	 */
	RptAccumulativePromotionProgram getRptAccumulativePP(Long shopId, Long promotionProgramId, Long customerId, Date orderDate) throws DataAccessException;

	List<RptAccumulativePromotionProgramDetail> getRptAccumulativePPDetail(Long rptAccumulativePPId) throws DataAccessException;
	
	/**
	 * lay thong tin CTTL pay detail
	 * @author tungmt
	 * @param shopId
	 * @param promotionProgramId
	 * @param customerId
	 * @param orderDate
	 * @return
	 * @date 10/12/2014
	 */
	List<RptCTTLPayDetail> getListRptCTTLPayDetail(Long rptPayId, Long shopId, Long promotionProgramId, Long customerId, Date orderDate) throws DataAccessException;
	
	/**
	 * lay thong tin CTTL pay cuoi cung
	 * @author tungmt
	 * @param shopId
	 * @param promotionProgramId
	 * @param customerId
	 * @param orderDate
	 * @return
	 * @date 10/12/2014
	 */
	List<RptCTTLPay> getLastRptCTTLPayByCustomer(Long shopId, Long promotionProgramId, Long customerId, Date orderDate) throws DataAccessException;
	
	/**
	 * lay thong tin CTTL pay
	 * @author tungmt
	 * @return
	 * @date 27/12/2014
	 */
	List<RptCTTLPay> getListRptCTTLPay(SaleOrderFilter filter) throws DataAccessException;
	
	RptCTTLPay createRptCTTLPay(RptCTTLPay rptCTTLPay, LogInfoVO logInfo) throws DataAccessException;
	RptCTTLPay getRptCTTLPayById(long id) throws DataAccessException;
	
	RptCTTLPayDetail createRptCTTLPayDetail(RptCTTLPayDetail rptCTTLPayDetail, LogInfoVO logInfo) throws DataAccessException;
	RptCTTLPayDetail getRptCTTLPayDetailById(long id) throws DataAccessException;
	
	/**
	 * tinh so dong sale_order_detail co cau truc KM ko hop le
	 * @author tuannd20
	 * @param saleOrderId
	 * @return so dong sale_order_detail trong DH co cau truc KM ko hop le
	 * @throws DataAccessException
	 * @date 20/12/2014
	 */
	int countSaleOrderDetailHasInvalidPromotionGroupLevel(Long saleOrderId) throws DataAccessException;
	
	/**
	 * lay danh sach CTKM ma DH duoc huong, da thay doi cau truc
	 * @author tuannd20
	 * @param saleOrderId
	 * @return
	 * @throws DataAccessException
	 * @date 22/12/2014
	 */
	List<String> getChangedStructurePromotionProgramInSaleOrder(Long saleOrderId) throws DataAccessException;
	
	/**
	 * lay danh sach cac DH co CTKM bi thay doi co cau.
	 * OrderProductVO.promotionProgramCode: cac CTKM thay doi co cau cua DH, cach nhau bang dau ','
	 * @author tuannd20
	 * @param saleOrderIds
	 * @return danh sach cac DH thay doi co cau
	 * @throws BusinessException
	 * @date 23/12/2014
	 */
	List<OrderProductVO> getSaleOrdersHasChangedStructurePromotionProgram(List<Long> saleOrderIds) throws DataAccessException;
	
	/**
	 * lay danh sach cac DH co CTKM bi thay doi co cau.
	 * OrderProductVO.promotionProgramCode: cac CTKM thay doi co cau cua DH, cach nhau bang dau ','
	 * @author tuannd20
	 * @param soFilter
	 * @return danh sach cac DH thay doi co cau
	 * @throws DataAccessException
	 * @date 23/12/2014
	 */
	List<OrderProductVO> getSaleOrdersHasChangedStructurePromotionProgram(SoFilter soFilter) throws DataAccessException;

	List<Product> getListProductInSaleLevel(Long promotionId)
			throws DataAccessException;
	
	/**
	 * Lay danh sach rpt_cttl_pay_detail theo rpt_cttl_pay_id
	 * 
	 * @author lacnv1
	 * @since Jan 01, 2015
	 */
	List<RptCTTLPayDetail> getRptCTTLPayDetailByRptPayId(long rptPayId) throws DataAccessException;
	
	/**
	 * lay danh sach danh sach quy doi CTKM
	 * @author tungmt
	 * @return danh sach quy doi CTKM
	 * @throws DataAccessException
	 * @date 1/1/2014
	 */
	List<PromoProductConvVO> getListConvertPromotionProgram(Long promotionProgramId) throws DataAccessException;
	
	/**
	 * Lay danh sach group_level_detail cua nhom sp ban khi biet product_group_id, group_level_id (cha) cua nhom khuyen mai
	 * 
	 * @author lacnv1
	 * @since Jan 06, 2015
	 */
	List<GroupLevelDetail> getListGroupLevelDetailByPromoGroupAndPromoGroupLevel(long promoGroupId, long promoGroupLevelId) throws DataAccessException;
	
	/**
	 * Lay danh sach CTKM mo moi co trong don hang
	 * Voi dieu kien: don hang co sp cau cau ZV24, co sp mo moi chua duoc tra trong cac don da duyet, chua tra mo moi cho don hien tai
	 * 
	 * @author lacnv1
	 * @since Jan 10, 2015
	 */
	List<PromotionProgram> getPromotionProgramsZV24InSaleOrder(long orderId, long customerId,
			Date orderDate, List<Long> lstPassOrderId) throws DataAccessException;
	
	/**
	 * Kiem tra xem CTKM co thuoc loai co so suat hay khong
	 * Tra ve: true: co so suat, nguoc lai: false
	 * 
	 * @author lacnv1
	 * @since Jan 28, 2015
	 */
	boolean checkPromotionProgramHasPortion(long promotionId, long shopId, long customerId, long staffId, Date lockDate) throws DataAccessException;
	
	Integer getMaxOrderNumberOfGroupLevel(Long groupProductId)
			throws DataAccessException;

	Integer checkGroupLevelMuaValue(Long groupId) throws DataAccessException;
	
	/**
	 * Lay so suat con lai cua CTKM
	 * 
	 * @author lacnv1
	 * @return so suat con lai
	 * @throws DataAccessException
	 * @since Feb 10, 2015
	 */
	Integer getQuantityRemainOfPromotionProgram(long promotionId, long shopId, long staffId, long customerId, Date lockDate) throws DataAccessException;
	
	/**
	 * Lay so tien tra con lai cua CTKM
	 * @author trietptm
	 * @param promotionCode
	 * @param shopId
	 * @param staffId
	 * @param customerId
	 * @param lockDate
	 * @return so suat con lai
	 * @throws DataAccessException
	 * @since Feb 04, 2015
	 */
	BigDecimal getAmountRemainOfPromotionProgram(String promotionCode, long shopId, long staffId, long customerId, Date lockDate) throws DataAccessException;
	
	/**
	 * Lay so luong tra con lai cua CTKM
	 * @author trietptm
	 * @param promotionCode
	 * @param shopId
	 * @param staffId
	 * @param customerId
	 * @param lockDate
	 * @return so suat con lai
	 * @throws DataAccessException
	 * @since Sep 04, 2015
	 */
	Integer getNumRemainOfPromotionProgram(String promotionCode, long shopId, long staffId, long customerId, Date lockDate) throws DataAccessException;

	List<ProgrameExtentVO> getIdAllTableProgram(long promotionId) throws DataAccessException;
	
	/**
	 * Lay danh sach group_level bi trung trong nhom
	 * @author hunglm16 
	 * @param productGroupId
	 * @param lstProductIds
	 * @param lstValues
	 * @param exceptGroupLevelId
	 * @return danh sach group_level
	 * @throws DataAccessException
	 * @since September 08, 2015
	 */
	List<GroupLevelDetail> getListDuplicateLevelDetail(long productGroupId, String lstProductIds, String lstValues, Long exceptGroupLevelId) throws DataAccessException;
	
	/**
	 * Lay danh sach group_level bi trung trong nhom
	 * 
	 * @author hunglm16
	 * @param productGroupId - id nhom mua
	 * @param minQuantity - so luong toi thieu cua muc
	 * @param minAmount - so tien toi thieu cua muc
	 * @param exceptGroupLevelId - id group_level khong can kiem tra
	 * @return danh sach group_level
	 * @throws DataAccessException
	 * @since September 08, 2015
	 */
	List<GroupLevel> getListDuplicateLevelWithMinValue(long productGroupId, Integer minQuantity, BigDecimal minAmount, Long exceptGroupLevelId) throws DataAccessException;
	
	/**
	 * Lay danh sach nhung san pham co trong group curProductGroupId dong thoi co trong nhung product_group khac cua CTKM
	 *  
	 * @author hunglm16
	 * @param promotionId : id CTKM
	 * @param curProductGroupId : id product_group dang xet
	 * @param lstProductId : danh sach san pham cua nhom moi (chuoi, cach nhau dau phay)
	 * @return List<Product> : danh sach san pham thoa man yeu cau
	 * @throws DataAccessException
	 * @since September 08, 2015
	 */
	List<Product> getListDuplicateProductInProgram(long promotionId, long curProductGroupId, String lstProductId) throws DataAccessException;
	
	/**
	 * Cap nhat thu tu muc cho nhom san pham ban
	 * 
	 * @author hunglm16
	 * @version
	 * @param promotionId: id CTKM can cap nhat
	 * @param productGroupId: nhom san pham ban can cap nhat
	 * @param logInfo : thong tin dang nhap
	 * @throws DataAccessException
	 * @since September 08, 2015
	 */
	void updateGroupLevelOrderNumber(long promotionId, long productGroupId, LogInfoVO logInfo) throws DataAccessException;

	/**
	 * Lay danh sach CTKM theo don vi phan quyen
	 * 
	 * @author hunglm16
	 * @param status
	 * @param shopId
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return
	 * @throws DataAccessException
	 * @since 19/09/2015
	 */
	List<PromotionProgramVO> getListVOPromotionProgramByShop(Integer status, Long shopId, Long staffRootId, Long roleId, Long shopRootId) throws DataAccessException;

	/**
	 * Xu ly getQuantityRemainOfPromotionProgramAndType
	 * @author vuongmq
	 * @param promotionId
	 * @param shopId
	 * @param staffId
	 * @param customerId
	 * @param lockDate
	 * @return SaleOrderPromotionRemainVO<Integer>
	 * @throws DataAccessException
	 * @since Jan 11, 2016
	*/
	SaleOrderPromotionRemainVO<Integer> getQuantityRemainOfPromotionProgramAndType(long promotionId, long shopId, long staffId, long customerId, Date lockDate) throws DataAccessException;

	/**
	 * Xu ly getAmountRemainOfPromotionProgramAndType
	 * @author vuongmq
	 * @param promotionCode
	 * @param shopId
	 * @param staffId
	 * @param customerId
	 * @param lockDate
	 * @return SaleOrderPromotionRemainVO<BigDecimal>
	 * @throws DataAccessException
	 * @since Jan 11, 2016
	*/
	SaleOrderPromotionRemainVO<BigDecimal> getAmountRemainOfPromotionProgramAndType(String promotionCode, long shopId, long staffId, long customerId, Date lockDate) throws DataAccessException;

	/**
	 * Xu ly getNumRemainOfPromotionProgramAndType
	 * @author vuongmq
	 * @param promotionCode
	 * @param shopId
	 * @param staffId
	 * @param customerId
	 * @param lockDate
	 * @return SaleOrderPromotionRemainVO<Integer>
	 * @throws DataAccessException
	 * @since Jan 11, 2016
	*/
	SaleOrderPromotionRemainVO<Integer> getNumRemainOfPromotionProgramAndType(String promotionCode, long shopId, long staffId, long customerId, Date lockDate) throws DataAccessException;

	/**
	 * Xu ly getPromotionShopMapFilter
	 * @author vuongmq
	 * @param filter
	 * @return PromotionShopMap
	 * @throws DataAccessException
	 * @since Jan 11, 2016
	*/
	PromotionShopMap getPromotionShopMapFilter(PromotionMapBasicFilter<PromotionShopMap> filter) throws DataAccessException;

	/**
	 * Xu ly getPromotionStaffMapFilter
	 * @author vuongmq
	 * @param filter
	 * @return PromotionStaffMap
	 * @throws DataAccessException
	 * @since Jan 11, 2016
	*/
	PromotionStaffMap getPromotionStaffMapFilter(PromotionMapBasicFilter<PromotionStaffMap> filter) throws DataAccessException;

	/**
	 * Xu ly getPromotionCustomerMapFilter
	 * @author vuongmq
	 * @param filter
	 * @return PromotionCustomerMap
	 * @throws DataAccessException
	 * @since Jan 11, 2016
	*/
	PromotionCustomerMap getPromotionCustomerMapFilter(PromotionMapBasicFilter<PromotionCustomerMap> filter) throws DataAccessException;

	/**
	 * 
	 * Xu ly getAmountRemainApprovedOfPromotionProgram
	 * @author vuongmq
	 * @param promotionCode
	 * @param shopId
	 * @param staffId
	 * @param customerId
	 * @param lockDate
	 * @return BigDecimal
	 * @throws DataAccessException
	 * @since Apr 1, 2016
	 */
	BigDecimal getAmountRemainApprovedOfPromotionProgram(String promotionCode, long shopId, long staffId, long customerId, Date lockDate) throws DataAccessException;

	/**
	 * 
	 * Xu ly getNumRemainApprovedOfPromotionProgram
	 * @author vuongmq
	 * @param promotionCode
	 * @param shopId
	 * @param staffId
	 * @param customerId
	 * @param lockDate
	 * @return Integer
	 * @throws DataAccessException
	 * @since Apr 1, 2016
	 */
	Integer getNumRemainApprovedOfPromotionProgram(String promotionCode, long shopId, long staffId, long customerId, Date lockDate) throws DataAccessException;
}
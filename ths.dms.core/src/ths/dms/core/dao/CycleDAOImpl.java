package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.filter.CycleFilter;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class CycleDAOImpl implements CycleDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Override
	public Cycle createCycle(Cycle cycle, LogInfoVO logInfo) throws DataAccessException {
		if (cycle == null) {
			throw new IllegalArgumentException("cycle");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.create(cycle);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, cycle, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(Cycle.class, cycle.getId());
	}

	@Override
	public void deleteCycle(Cycle cycle, LogInfoVO logInfo) throws DataAccessException {
		if (cycle == null) {
			throw new IllegalArgumentException("cycle");
		}
		repo.delete(cycle);
		try {
			actionGeneralLogDAO.createActionGeneralLog(cycle, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public void updateCycle(Cycle cycle, LogInfoVO logInfo) throws DataAccessException {
		if (cycle == null) {
			throw new IllegalArgumentException("cycle");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		Cycle temp = this.getCycleById(cycle.getId());
		repo.update(cycle);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, cycle, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Cycle getCycleById(long id) throws DataAccessException {
		return repo.getEntityById(Cycle.class, id);
	}

	@Override
	public Cycle getCycleByCurrentSysdate() throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from cycle where begin_date <= trunc(sysdate) and (end_date is null or trunc(sysdate) < end_date + 1) ");
		return repo.getEntityBySQL(Cycle.class, sql.toString(), params);
	}
	
	@Override
	public Cycle getCycleByDate(Date date) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from cycle where begin_date <= trunc(?) and end_date >= trunc(?)");
//		sql.append("select * from cycle where sysdate >= begin_date and sysdate < trunc(end_date) + 1 ");
		params.add(date);
		params.add(date);
		return repo.getEntityBySQL(Cycle.class, sql.toString(), params);
	}

	@Override
	public Cycle getCycleByYearAndNum(int year, int num) throws DataAccessException {		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from cycle where Extract(year from year) = ? and num = ? ");
		params.add(year);
		params.add(num);
		return repo.getEntityBySQL(Cycle.class, sql.toString(), params);
	}
	
	@Override
	public List<Integer> getListYearOfAllCycle() throws DataAccessException {		
		List<Integer> lstRs = new ArrayList<Integer>();
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT DISTINCT Extract(year from year) seq FROM cycle");
		final String[] fieldNames = new String[] { "seq" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.INTEGER };
		List<BasicVO> lstData = repo.getListByQueryAndScalar(BasicVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (lstData != null && !lstData.isEmpty()) {
			for (BasicVO item : lstData) {
				lstRs.add(item.getSeq());
			}
		}
		return lstRs;
	}
	
	@Override
	public List<CycleVO> getListCycleByFilter(CycleFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select c.cycle_id cycleId, c.cycle_code cycleCode, c.cycle_name cycleName, c.num, to_char(c.year,'yyyy') year, ");
		sql.append(" c.begin_date beginDate, c.end_date endDate, ");
		sql.append(" to_char(c.begin_date,'dd/MM/yyyy') beginDateStr, to_char(c.end_date,'dd/MM/yyyy') endDateStr ");
		sql.append(" from cycle c where c.status = 1 ");
		if (filter.getLessYear() != null) {
			sql.append(" and c.begin_date >= add_months(sysdate,?) ");
			params.add(filter.getLessYear()*-12 - 1);
		}
		if (filter.getMoreYear() != null) {
			sql.append(" and c.end_date <= add_months(sysdate,?) ");
			params.add(filter.getMoreYear()*12 + 1);
		}
		if (filter.getYear() != null) {
//			sql.append(" and to_char(c.year) = ? ");
//			params.add(filter.getYear().toString());
			sql.append(" and extract(year from c.year) = ? ");
			params.add(filter.getYear());
		}
		if (filter.getNum() != null) {
			sql.append(" and c.num = ? ");
			params.add(filter.getNum());
		}
		if (filter.getBeginDate() != null) {
			sql.append(" and c.begin_date >= ?");
			params.add(filter.getBeginDate());
		}
		if (filter.getEndDate() != null) {
			sql.append(" and c.begin_date <= ?");
			params.add(filter.getEndDate());
		}
		
		sql.append(" order by c.year, c.num ");
		String[] fieldNames = new String[] {
			"cycleId", "cycleCode", "cycleName",
			"num", "year", "beginDate", "endDate",
			"beginDateStr", "endDateStr"
		};
		Type[] fieldTypes = new Type[] {
			StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
			StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.TIMESTAMP,
			StandardBasicTypes.STRING, StandardBasicTypes.STRING
		};
		return repo.getListByQueryAndScalar(CycleVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<CycleVO> getListKPIByCycle(Long cycleId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select k.kpi_id as cycleId, k.criteria as cycleCode, k.ORDER_INDEX as cycleName from kpi k ");
		sql.append(" join rpt_kpi_cycle rkc on rkc.kpi_id = k.kpi_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and k.status =1 ");
		if (cycleId != null) {
			sql.append(" and rkc.cycle_id=?");
			params.add(cycleId);
		}
		sql.append(" group by k.kpi_id, k.criteria, k.ORDER_INDEX ");
		sql.append(" order by k.kpi_id, k.ORDER_INDEX ");
		String[] fieldNames = new String[] { "cycleId", "cycleCode", "cycleName" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(CycleVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<CycleVO> getListKeyShopByCycle(Long cycleId, List<Long> lstKeyShopId, String fromDate, String toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct k.ks_id as cycleId, k.ks_code || ' - ' || k.name as cycleCode, k.name as cycleName from ks k ");
		sql.append(" join rpt_ks_day rkd on rkd.ks_id = k.ks_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and status = 1 ");
		if (cycleId != null) {
			sql.append(" and rkd.cycle_id=?");
			params.add(cycleId);
		}
		if (lstKeyShopId != null && lstKeyShopId.size() > 0) {
			String paramFix = StringUtility.getParamsIdFixBugTooLongParams(lstKeyShopId, "rkd.ks_id");
			sql.append(paramFix);
		}
		sql.append(" order by k.ks_id ");
		String[] fieldNames = new String[] { "cycleId", "cycleCode", "cycleName" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(CycleVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public Cycle getCycleByFilter(CycleFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from cycle where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCycleCode())) {
			sql.append(" and cycle_code = ? ");
			params.add(filter.getCycleCode());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCycleName())) {
			sql.append(" and unicode2english(cycle_name) = ? ");
			params.add(Unicode2English.codau2khongdau(filter.getCycleName()).trim().toLowerCase());
		}
		if (filter.getStatus() != null) {
			sql.append(" and status = ? ");
			params.add(filter.getStatus().getValue());
		}
		return repo.getEntityBySQL(Cycle.class, sql.toString(), params);
	}
	
	@Override
	public Cycle getCycleLast(Long cycleId) throws DataAccessException {
		if (cycleId == null) {
			throw new IllegalArgumentException("cycleId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select cycle.* from cycle ");
		sql.append(" join (SELECT cycle_id, ROWNUM AS SEED_INDEX ");
		sql.append(" from cycle ");
		sql.append(" where cycle_id < ? ");
		params.add(cycleId);
		sql.append(" order by cycle_id desc ");
		sql.append(" ) cyclelast ");
		sql.append(" on cycle.cycle_id = cyclelast.cycle_id ");
		sql.append(" where cyclelast.seed_index = 1 ");
		return repo.getEntityBySQL(Cycle.class, sql.toString(), params);
	}
}

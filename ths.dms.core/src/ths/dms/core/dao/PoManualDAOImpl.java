/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Po;
import ths.dms.core.entities.PoDetail;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApprovalStepPo;
import ths.dms.core.entities.enumtype.PoManualType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.PoManualFilter;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PoManualVO;
import ths.dms.core.entities.vo.PoManualVODetail;
import ths.dms.core.entities.vo.PoVnmManualVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

/**
 * Mo ta class PoManualDAOImpl.java
 * @author vuongmq
 * @since Dec 7, 2015
 */
public class PoManualDAOImpl implements PoManualDAO {

	@Autowired
	private IRepository repo;

	@Override
	public Po createPo(Po po) throws DataAccessException {
		if (po == null) {
			throw new IllegalArgumentException("po create is null");
		}
		repo.create(po);
		return repo.getEntityById(Po.class, po.getId());
	}
	
	@Override
	public void createListPo(List<Po> listPo) throws DataAccessException {
		if (listPo == null) {
			throw new IllegalArgumentException("listPo create is null");
		}
		repo.create(listPo);
	}

	@Override
	public void deletePo(Po po) throws DataAccessException {
		if (po == null) {
			throw new IllegalArgumentException("po delete is null");
		}
		repo.delete(po);
	}

	@Override
	public Po getPoById(long id) throws DataAccessException {
		return repo.getEntityById(Po.class, id);
	}
	
	@Override
	public void updatePo(Po po) throws DataAccessException {
		if (po == null) {
			throw new IllegalArgumentException("po update is null");
		}
		repo.update(po);
	}
	
	@Override
	public void updateListPo(List<Po> listPo) throws DataAccessException {
		if (listPo == null) {
			throw new IllegalArgumentException("listPo update is null");
		}
		repo.update(listPo);
	}

	@Override
	public Po getPoByPoNumber(String poNumber) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(poNumber)) {
			throw new IllegalArgumentException("poNumber is null");
		}
		String sql = "select * from po where po_number = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(poNumber);
		return repo.getEntityBySQL(Po.class, sql.toString(), params);
	}
	
	@Override
	public List<PoDetail> getListPoDetailByPoId(Long poId) throws DataAccessException {
		if (poId == null) {
			throw new IllegalArgumentException("poId is null");
		}
		String sql = "select * from po_detail where po_id = ? order by po_line_number ";
		List<Object> params = new ArrayList<Object>();
		params.add(poId);
		return repo.getListBySQL(PoDetail.class, sql, params);
	}

	@Override
	public PoDetail getPoDetailByPoDetailIdAndProductId(Long poDetailId) throws DataAccessException {
		if (poDetailId == null) {
			throw new IllegalArgumentException("poDetailId is null");
		}
		String sql = "select * from po_detail where po_detail_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(poDetailId);
		return repo.getEntityBySQL(PoDetail.class, sql.toString(), params);
	}
	
	@Override
	public List<OrderProductVO> getListOrderProductVO(PoManualFilter<OrderProductVO> filter) throws DataAccessException {
		if (filter == null || filter.getShopId() == null) {
			throw new IllegalArgumentException("filter is null or filter.getShopId() is null");
		}
		if (filter.getWarehouseId() == null) {
			throw new IllegalArgumentException("filter.getWarehouseId() is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with priceTmp as ( ");
		sql.append(" select shop_id, shop_name, ");
		sql.append(" price_sale_in_id, product_id, ");
		sql.append(" package_price, retail_price ");
		sql.append(" from ( ");
		sql.append(" select sh.shop_id, sh.shop_code, sh.shop_name, sh.parent_shop_id, ");
		sql.append(" ps.price_sale_in_id, ps.product_id, ");
		sql.append(" ps.package_price, ps.retail_price, ");
		sql.append(" row_number() over (partition by ps.product_id order by lvl) as rn ");
		sql.append(" from ( ");
		sql.append(" select shop_id, shop_code, shop_name, parent_shop_id, level as lvl from shop ");
		sql.append(" where status = 1 ");
		sql.append(" start with shop_id = ? ");
		sql.append(" connect by prior parent_shop_id = shop_id ");
		params.add(filter.getShopId());
		sql.append(" ) sh left join price_sale_in ps on sh.shop_id = ps.shop_id and ps.status = 1 ");
		sql.append(" AND from_date < TRUNC(sysdate) + 1 ");
		sql.append(" and (to_date is null ");
		sql.append(" or to_date >= trunc(sysdate)) ");
		sql.append(" ) ");
		sql.append(" where product_id is not null and rn = 1 ");
		sql.append(" order by product_id ");
		sql.append(" ) ");
		//end priceTmp
		sql.append(" , stockTmp as ( ");
		sql.append(" select stock.product_id, ");
		sql.append(" stock.available_quantity as available_quantity, ");
		sql.append(" stock.quantity as quantity, ");
		sql.append(" stock.warehouse_id as warehouseId ");
		sql.append(" from stock_total stock ");
		sql.append(" where status = 1 and shop_id = ? ");
		sql.append(" and object_type = ? ");
		sql.append(" and warehouse_id = ? ");
		sql.append(" ) ");
		params.add(filter.getShopId());
		params.add(StockObjectType.SHOP.getValue());
		params.add(filter.getWarehouseId());
		//end stockTmp
		sql.append(" select p.product_id as productId, ");
		sql.append(" p.product_code as productCode, ");
		sql.append(" p.product_name as productName, ");
		sql.append(" p.convfact as convfact, ");
		sql.append(" priceTmp.price_sale_in_id as priceSaleInId, ");
		sql.append(" NVL(priceTmp.package_price, 0) as packagePrice, ");
		sql.append(" NVL(priceTmp.retail_price, 0) as price, ");
		sql.append(" p.gross_weight as grossWeight, ");
		sql.append(" p.net_weight as netWeight, ");
		sql.append(" NVL(stocktmp.available_quantity, 0) as availableQuantity, ");
		sql.append(" NVL(stockTmp.quantity, 0) as quantity ");
		sql.append(" from product p ");
		sql.append(" left join priceTmp on priceTmp.product_id = p.product_id ");
		sql.append(" left join stocktmp on stockTmp.product_id = p.product_id ");
		sql.append(" where p.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		if (filter.getProductId() != null) {
			sql.append(" and p.product_id = ? ");
			params.add(filter.getProductId());
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		sql.append(" order by productCode ");
		String[] fieldNames = new String[] { 
				"productId", // 1
				"productCode",// 2
				"productName", // 3
				"convfact", //4
				"priceSaleInId", // 5
				"packagePrice",//6
				"price", // 7
				"grossWeight", //8
				"netWeight", //9
				"availableQuantity", // 10 
				"quantity", //11
				};
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.INTEGER,// 4
				StandardBasicTypes.LONG,// 5
				StandardBasicTypes.BIG_DECIMAL,// 6
				StandardBasicTypes.BIG_DECIMAL,// 7
				StandardBasicTypes.FLOAT,// 8
				StandardBasicTypes.FLOAT,// 9
				StandardBasicTypes.INTEGER,// 10
				StandardBasicTypes.INTEGER,// 11
				};

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public List<PoVnm> getListPoVNMByFilter(PoVnmFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from po_vnm where 1 = 1");
		if (filter.getObjectType() != null) {
			sql.append(" and object_type = ?");
			params.add(filter.getObjectType().getValue());
		}
		if (filter.getPoType() != null) {
			sql.append(" and type = ?");
			params.add(filter.getPoType().getValue());
		}
		if (filter.getPoStatus() != null) {
			sql.append(" and status = ?");
			params.add(filter.getPoStatus().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and sale_order_number like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getOrderNumber().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getPoAutoNumber())) {
			sql.append(" and po_auto_number like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getPoAutoNumber().toUpperCase()));
		}
		if (filter.getFromDate() != null) {
			sql.append(" and po_vnm_date >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and po_vnm_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		sql.append(" order by po_co_number");
		return repo.getListBySQL(PoVnm.class, sql.toString(), params);
	}
	
	@Override
	public List<PoVnmManualVO> getListPoVNMManualByFilter(PoVnmFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select povnm.po_vnm_id as id, ");
		sql.append(" povnm.shop_id as shopId, ");
		sql.append(" povnm.from_po_vnm_id as fromId, ");
		sql.append(" fromvnm.sale_order_number as orderNumber, ");
		sql.append(" povnm.sale_order_number as asn, ");
		sql.append(" to_char(povnm.po_vnm_date, 'dd/mm/yyyy') as poDateStr ");
		sql.append(" from po_vnm povnm ");
		sql.append(" join po_vnm fromvnm on fromvnm.po_vnm_id = povnm.from_po_vnm_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and fromvnm.type = ? ");
		params.add(PoType.PO_CUSTOMER_SERVICE.getValue());
		sql.append(" and povnm.po_vnm_id not in (select distinct ref_asn_id from po where po_type = ? and ref_asn_id is not null) ");
		params.add(PoManualType.PO_RETURN.getValue());
		if (filter.getShopId() != null) {
			sql.append(" and povnm.shop_id = ?");
			params.add(filter.getShopId());
		}
		if (filter.getObjectType() != null) {
			sql.append(" and povnm.object_type = ?");
			params.add(filter.getObjectType().getValue());
		}
		if (filter.getPoStatus() != null) {
			sql.append(" and povnm.status = ? ");
			params.add(filter.getPoStatus().getValue());
		}
		if (filter.getPoType() != null) {
			sql.append(" and povnm.type = ? ");
			params.add(filter.getPoType().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and fromvnm.sale_order_number like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getOrderNumber().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getPoAutoNumber())) { // asn
			sql.append(" and povnm.sale_order_number like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getPoAutoNumber().toUpperCase()));
		}
		if (filter.getFromDate() != null) {
			sql.append(" and povnm.po_vnm_date >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and povnm.po_vnm_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		sql.append(" order by ordernumber, asn ");
		String[] fieldNames = new String[] { 
				"id", // 1
				"shopId", // 1.1
				"fromId",// 2
				"orderNumber", // 3
				"asn", //4
				"poDateStr", // 5
				};
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.LONG,// 1.1
				StandardBasicTypes.LONG,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				};
		if (filter.getkPagingManualVO() != null) {
			return repo.getListByQueryAndScalarPaginated(PoVnmManualVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingManualVO());
		}
		return repo.getListByQueryAndScalar(PoVnmManualVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<OrderProductVO> getListPoVNMDetailByFilter(PoVnmFilter filter) throws DataAccessException {
		if (filter == null || filter.getPoVnmId() == null) {
			throw new IllegalArgumentException("filter is null or filter.getPoVnmId() is null");
		}
		if (filter.getWarehouseId() == null) {
			throw new IllegalArgumentException("filter.getWarehouseId() is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with stockTmp as ( ");
		sql.append(" select stock.product_id, ");
		sql.append(" stock.available_quantity as available_quantity, ");
		sql.append(" stock.quantity as quantity, ");
		sql.append(" stock.warehouse_id as warehouseId ");
		sql.append(" from stock_total stock ");
		sql.append(" where status = 1 and shop_id = ? ");
		sql.append(" and object_type = ? ");
		sql.append(" and warehouse_id = ? ");
		sql.append(" ) ");
		params.add(filter.getShopId());
		params.add(StockObjectType.SHOP.getValue());
		params.add(filter.getWarehouseId());
		//end stockTmp
		sql.append(" select p.product_id      AS productId,");
		sql.append("   p.product_code      	AS productCode,");
		sql.append("   p.product_name           AS productName,");
		sql.append("   p.convfact               AS convfact,");
		sql.append("   null        AS priceSaleInId,");
		sql.append("   NVL(pvd.price_package, 0)           AS packagePrice,");
		sql.append("   NVL(pvd.price, 0)                AS price,");
		sql.append("   p.gross_weight as grossWeight, ");
		sql.append("   p.net_weight as netWeight, ");
		sql.append("   NVL(stockTmp.available_quantity, 0) as availableQuantity, ");
		sql.append("   NVL(stockTmp.quantity, 0) as quantity, ");
		sql.append("   sum(pvd.quantity) as oldQuantity "); //quantity
		sql.append(" from po_vnm_detail pvd ");
		sql.append("   join product p on p.product_id = pvd.product_id");
		sql.append("   left join stockTmp on stockTmp.product_id = p.product_id ");
		sql.append(" where pvd.po_vnm_id = ? ");
		params.add(filter.getPoVnmId());
		if (filter.getProductId() != null) {
			sql.append(" and p.product_id = ? ");
			params.add(filter.getProductId());
		}
		sql.append(" group by p.product_id, ");
		sql.append(" p.product_code, ");
		sql.append(" p.product_name, ");
		sql.append(" p.convfact, ");
		sql.append(" null, ");
		sql.append(" pvd.price_package, ");
		sql.append(" pvd.price, ");
		sql.append(" p.gross_weight, ");
		sql.append(" p.net_weight , ");
		sql.append(" stockTmp.available_quantity, ");
		sql.append(" stockTmp.quantity ");
		sql.append(" order by p.product_code ");
		String[] fieldNames = new String[] { 
				"productId", // 1
				"productCode",// 2
				"productName", // 3
				"convfact", //4
				"priceSaleInId", // 5
				"packagePrice",//6
				"price", // 7
				"grossWeight", //8
				"netWeight", //9
				"availableQuantity", // 10 
				"quantity", //11
				"oldQuantity", // 12
				};
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.INTEGER,// 4
				StandardBasicTypes.LONG,// 5
				StandardBasicTypes.BIG_DECIMAL,// 6
				StandardBasicTypes.BIG_DECIMAL,// 7
				StandardBasicTypes.FLOAT,// 8
				StandardBasicTypes.FLOAT,// 9
				StandardBasicTypes.INTEGER,// 10
				StandardBasicTypes.INTEGER,// 11
				StandardBasicTypes.INTEGER,// 12
				};
		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<OrderProductVO> getListPoDetailEditByFilter(PoManualFilter<OrderProductVO> filter) throws DataAccessException {
		if (filter == null || filter.getId() == null) {
			throw new IllegalArgumentException("filter is null or filter.getId() is null");
		}
		if (filter.getWarehouseId() == null) {
			throw new IllegalArgumentException("filter.getWarehouseId() is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with stockTmp AS ");
		sql.append(" (select stock.product_id, ");
		sql.append(" stock.available_quantity AS available_quantity, ");
		sql.append(" stock.quantity AS quantity, ");
		sql.append(" stock.warehouse_id AS warehouseId ");
		sql.append(" from stock_total stock ");
		sql.append(" where status = 1 ");
		sql.append(" and shop_id = ? ");
		sql.append(" and object_type = ? ");
		sql.append(" and warehouse_id = ? ");
		sql.append(" ) ");
		params.add(filter.getShopId());
		params.add(StockObjectType.SHOP.getValue());
		params.add(filter.getWarehouseId());
		//end stockTmp;
		// select VO
		if (PoManualType.PO_IMPORT.getValue().equals(filter.getType())) {
			sql.append(" select p.product_id AS productId, ");
			sql.append(" p.product_code AS productcode, ");
			sql.append(" p.product_name AS productName, ");
			sql.append(" pod.convfact AS convfact, ");
			sql.append(" pod.price_sale_in_id AS priceSaleInId, ");
			sql.append(" pod.package_price AS packagePrice, ");
			sql.append(" pod.retail_price AS price, ");
			sql.append(" p.gross_weight AS grossWeight, ");
			sql.append(" p.net_weight as netweight, ");
			sql.append(" stocktmp.available_quantity AS availableQuantity, ");
			sql.append(" stockTmp.quantity AS quantity, ");
			sql.append(" null AS oldQuantity, "); // don dat se null
			sql.append(" pod.package_quantity AS packageQuantity, "); // vi tao don dat, don tra: thung 
			sql.append(" pod.retail_quantity AS retailQuantity, "); // vi tao don dat, don tra: le 
			sql.append(" pod.quantity AS poDetailQuantity, "); // vi tao don dat, don tra: so luong
			sql.append(" pod.po_line_number AS poLineNumber, "); // vi tao don dat, don tra: po_line_number 
			sql.append(" pod.amount AS amount ");
			sql.append(" from po_detail pod ");
			sql.append(" join product p on p.product_id = pod.product_id ");
			sql.append(" left join stockTmp on stockTmp.product_id = p.product_id");
		} else if (PoManualType.PO_RETURN.getValue().equals(filter.getType())) {
			sql.append(" , pvnmd as ( ");
			sql.append(" select pvd.product_id, ");
			sql.append(" 	sum(pvd.quantity) as quantity ");
			sql.append(" from po_vnm_detail pvd ");
			sql.append(" where pvd.po_vnm_id = ? ");
			params.add(filter.getRefAsnId());
			sql.append(" group by pvd.product_id )");
			// end pvnmd
			sql.append(" select p.product_id AS productId, ");
			sql.append(" p.product_code as productcode, ");
			sql.append(" p.product_name AS productName, ");
			sql.append(" pod.convfact AS convfact, ");
			sql.append(" pod.price_sale_in_id AS priceSaleInId, "); // don tra se null
			sql.append(" pod.package_price AS packagePrice, ");
			sql.append(" pod.retail_price AS price, ");
			sql.append(" p.gross_weight AS grossWeight, ");
			sql.append(" p.net_weight as netweight, ");
			sql.append(" stockTmp.available_quantity AS availableQuantity, ");
			sql.append(" stockTmp.quantity AS quantity, ");
			sql.append(" pvnmd.quantity AS oldQuantity, ");
			sql.append(" pod.package_quantity AS packageQuantity, "); // vi tao don dat, don tra: thung 
			sql.append(" pod.retail_quantity AS retailQuantity, "); // vi tao don dat, don tra: le 
			sql.append(" pod.quantity AS poDetailQuantity, "); // vi tao don dat, don tra: so luong
			sql.append(" pod.po_line_number AS poLineNumber, "); // vi tao don dat, don tra: po_line_number 
			sql.append(" pod.amount AS amount ");
			sql.append(" from po_detail pod ");
			sql.append(" join product p on p.product_id = pod.product_id ");
			sql.append(" left join stockTmp on stockTmp.product_id = p.product_id");
			sql.append(" left join pvnmd on pvnmd.product_id = p.product_id ");
		}
		sql.append(" where pod.po_id = ? ");
		params.add(filter.getId());
		if (filter.getProductId() != null) {
			sql.append(" and p.product_id = ? ");
			params.add(filter.getProductId());
		}
		sql.append(" order by pod.po_line_number ");
		String[] fieldNames = new String[] { 
				"productId", // 1
				"productCode",// 2
				"productName", // 3
				"convfact", //4
				"priceSaleInId", // 5
				"packagePrice",//6
				"price", // 7
				"grossWeight", //8
				"netWeight", //9
				"availableQuantity", // 10 
				"quantity", //11
				"oldQuantity", // 12
				"packageQuantity", // 13
				"retailQuantity", // 14
				"poDetailQuantity", // 15
				"poLineNumber", // 16
				"amount", // 17
				};
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.INTEGER,// 4
				StandardBasicTypes.LONG,// 5
				StandardBasicTypes.BIG_DECIMAL,// 6
				StandardBasicTypes.BIG_DECIMAL,// 7
				StandardBasicTypes.FLOAT,// 8
				StandardBasicTypes.FLOAT,// 9
				StandardBasicTypes.INTEGER,// 10
				StandardBasicTypes.INTEGER,// 11
				StandardBasicTypes.INTEGER,// 12
				StandardBasicTypes.INTEGER,// 13
				StandardBasicTypes.INTEGER,// 14
				StandardBasicTypes.INTEGER,// 15
				StandardBasicTypes.INTEGER,// 16
				StandardBasicTypes.BIG_DECIMAL,// 17
				};
		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<PoManualVO> getListPoManualVOByFilter(PoManualFilter<PoManualVO> filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ");
		sql.append(" po.po_id as id, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName, ");
		sql.append(" po.po_number as poNumber, ");
		sql.append(" to_char(po.po_date, 'dd/mm/yyyy') as poDateStr, ");
		sql.append(" po.po_type as type, ");
		sql.append(" po.approved_step as approvedStep, ");
		sql.append(" po.status as status, ");
		sql.append(" po.amount as amount ");
		sql.append(" from po ");
		sql.append(" join shop sh on sh.shop_id = po.shop_id ");
		sql.append(" where 1 = 1 ");
		if (filter.getShopId() != null) {
			sql.append(" and sh.shop_id in (select shop_id from shop ");
			sql.append(" where status = 1 ");
			sql.append(" start with shop_id = ? ");
			sql.append(" connect by prior shop_id = parent_shop_id) ");
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getPoNumber())) {
			sql.append(" and po.po_number like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getPoNumber().toUpperCase()));
		}
		if (filter.getType() != null) {
			sql.append(" and po.po_type = ? ");
			params.add(filter.getType());
		}
		if (filter.getApprovedStep() != null) {
			if (ApprovalStepPo.DESTROY.getValue().equals(filter.getApprovedStep())) {
				//status = 0, approve_step = 1
				sql.append(" and po.approved_step = ? ");
				params.add(ApprovalStepPo.NEW.getValue());
				sql.append(" and po.status = ? ");
				params.add(ActiveType.STOPPED.getValue());
			} else if (ApprovalStepPo.REJECTED.getValue().equals(filter.getApprovedStep())) {
				// status = 0, approve_step = 2
				sql.append(" and po.approved_step = ? ");
				params.add(ApprovalStepPo.WAITING.getValue());
				sql.append(" and po.status = ? ");
				params.add(ActiveType.STOPPED.getValue());
			} else if (ApprovalStepPo.COMPLETED.getValue().equals(filter.getApprovedStep())) {
				// status = 1, approve_step = 4 
				sql.append(" and po.approved_step = ? ");
				params.add(ApprovalStepPo.COMPLETED.getValue());
				sql.append(" and po.status = ? ");
				params.add(ActiveType.RUNNING.getValue());
			} else if (ApprovalStepPo.APPROVED.getValue().equals(filter.getApprovedStep())) {
				// status = 1, approve_step = 3
				sql.append(" and po.approved_step = ? ");
				params.add(ApprovalStepPo.APPROVED.getValue());
				sql.append(" and po.status = ? ");
				params.add(ActiveType.RUNNING.getValue());
			} else if (ApprovalStepPo.WAITING.getValue().equals(filter.getApprovedStep())) {
				// status = 1, approve_step = 2 
				sql.append(" and po.approved_step = ? ");
				params.add(ApprovalStepPo.WAITING.getValue());
				sql.append(" and po.status = ? ");
				params.add(ActiveType.RUNNING.getValue());
			} else if (ApprovalStepPo.NEW.getValue().equals(filter.getApprovedStep())) {
				// status = 1, approve_step = 1 
				sql.append(" and po.approved_step = ? ");
				params.add(ApprovalStepPo.NEW.getValue());
				sql.append(" and po.status = ? ");
				params.add(ActiveType.RUNNING.getValue());
			}
		}
		if (filter.getFromDate() != null) {
			sql.append(" and po.po_date >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and po.po_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "sh.shop_id");
			sql.append(paramsFix);
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		sql.append(" order by po.status desc, po.approved_step, po.po_date desc, po.po_number desc ");
		String[] fieldNames = new String[] { 
				"id", // 1
				"shopCode",// 2
				"shopName", // 3
				"poNumber", //4
				"poDateStr", // 5
				"type", // 6
				"approvedStep", // 7
				"status", // 8
				"amount", // 9
				};
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.INTEGER,// 6
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.INTEGER,// 8
				StandardBasicTypes.BIG_DECIMAL,// 9
				};
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(PoManualVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
		return repo.getListByQueryAndScalar(PoManualVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<PoManualVODetail> getListPoManualDetailVOByFilter(PoManualFilter<PoManualVODetail> filter) throws DataAccessException {
		if (filter == null || filter.getId() == null) {
			throw new IllegalArgumentException("filter is null or filter.getId() poId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select pod.po_detail_id as id, ");
		sql.append(" pod.price_sale_in_id as priceSaleInId, ");
		sql.append(" p.product_id as productId, ");
		sql.append(" p.product_code as productCode, ");
		sql.append(" p.product_name as productName, ");
		sql.append(" pod.package_price as packagePrice, ");
		sql.append(" pod.retail_price as price, ");
		sql.append(" pod.package_quantity as packageQuantity, ");
		sql.append(" pod.retail_quantity as retailQuantity, ");
		sql.append(" pod.quantity as quantity, ");
		sql.append(" pod.convfact as convfact, ");
		sql.append(" pod.po_line_number AS poLineNumber, "); 
		sql.append(" pod.amount as amount ");
		sql.append(" from po_detail pod ");
		sql.append(" join product p on p.product_id = pod.product_id ");
		sql.append(" where pod.po_id = ? ");
		params.add(filter.getId());
		if (filter.getProductId() != null) {
			sql.append(" and pod.product_id = ? ");
			params.add(filter.getProductId());
		}
		sql.append(" order by pod.po_line_number ");
		String[] fieldNames = new String[] { 
				"id", // 1
				"priceSaleInId", // 2
				"productId", // 3
				"productCode",// 4
				"productName", // 5
				"packagePrice",//6
				"price",//7 ; gia le
				"packageQuantity", // 8
				"retailQuantity", //9
				"quantity", //10
				"convfact", //11
				"poLineNumber", //12
				"amount", //13
				};
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.LONG,// 2
				StandardBasicTypes.LONG,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.BIG_DECIMAL,// 6
				StandardBasicTypes.BIG_DECIMAL,// 7
				StandardBasicTypes.INTEGER,// 8
				StandardBasicTypes.INTEGER,// 9
				StandardBasicTypes.INTEGER,// 10
				StandardBasicTypes.INTEGER,// 11
				StandardBasicTypes.INTEGER,// 12
				StandardBasicTypes.BIG_DECIMAL,// 13
				};
		return repo.getListByQueryAndScalar(PoManualVODetail.class, fieldNames, fieldTypes, sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.CycleCountMapProduct;
import ths.dms.core.entities.CycleCountResult;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.CycleCountResultVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.core.entities.vo.rpt.RptCycleCountDiff3VO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class CycleCountResultDAOImpl implements CycleCountResultDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	ProductLotDAO productLotDAO;
	
	@Autowired
	CycleCountMapProductDAO cycleCountMapProductDAO;
	
	@Override
	public CycleCountResult getCycleCountResult(Long cycleCountId, Long productId, String lot) throws DataAccessException {
		if (cycleCountId == null || productId == null || StringUtility.isNullOrEmpty(lot)) {
			throw new IllegalArgumentException("params cannot null");
		}
		String sql = "select * from cycle_count_result where cycle_count_id = ? and product_id = ? and upper(lot) = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(cycleCountId);
		params.add(productId);
		params.add(lot.toUpperCase());
		return repo.getEntityBySQL(CycleCountResult.class, sql, params);
	}
	
	@Override
	public CycleCountResult createCycleCountResult(CycleCountResult cycleCountResult) throws DataAccessException {
		if (cycleCountResult == null) {
			throw new IllegalArgumentException("cycleCountResult");
		}
		repo.create(cycleCountResult);				
		
		return repo.getEntityById(CycleCountResult.class, cycleCountResult.getId());
	}
	
//	@Override
//	public CycleCountResult createCycleCountResult(CycleCountResult cycleCountResult) throws DataAccessException {
//		if (cycleCountResult == null) {
//			throw new IllegalArgumentException("cycleCountResult");
//		}
//		CycleCountMapProduct cc = cycleCountMapProductDAO.getCycleCountMapProductByIdForUpdate(cycleCountResult.getCycleCountMapProduct().getId());  
//		if (cc != null) {
//			cycleCountResult.setCycleCount(cc.getCycleCount());
//			cycleCountResult.setProduct(cc.getProduct());
//		}
//		if (cycleCountResult.getLot() != null) {
//			cycleCountResult.setLot(cycleCountResult.getLot().toUpperCase());
//			if (checkIfRecordExist(cycleCountResult.getCycleCountMapProduct().getId(), cycleCountResult.getLot(), null))
//				return null;
//		}
////		CycleCountResult ccr = this.getCycleCountResult(cycleCountResult.getLot(), cycleCountResult.getCycleCountMapProduct().getId());
////		if (ccr != null) {
////			ccr.setQuantityCounted(cycleCountResult.getQuantityCounted());
////			repo.update(ccr);
////			return ccr;
////		}
//
//		ProductLot pl = productLotDAO.getProductLotByCodeAndOwnerAndProductForUpdate(cycleCountResult.getLot(), cc.getCycleCount().getId(), StockObjectType.SHOP, cc.getProduct().getId());
//		if (pl == null)
//			throw new IllegalArgumentException("pl == null");
//		
//		Integer quantityBfrCount = pl.getQuantity();
//		cc.setQuantityBeforeCount(cc.getQuantityBeforeCount() + quantityBfrCount);
//		cc.setQuantityCounted(cc.getQuantityCounted() + cycleCountResult.getQuantityCounted());
//		cycleCountMapProductDAO.updateCycleCountMapProduct(cc, false, false);
//		
//		cycleCountResult.setQuantityBeforeCount(quantityBfrCount);
//		
//		repo.create(cycleCountResult);
//		
//		return repo.getEntityById(CycleCountResult.class, cycleCountResult.getId());
//	}

	@Override
	public void deleteCycleCountResult(CycleCountResult cycleCountResult) throws DataAccessException {
		if (cycleCountResult == null) {
			throw new IllegalArgumentException("cycleCountResult");
		}
		//cap nhat so luong dem va so luong chot
		CycleCountMapProduct cc = cycleCountMapProductDAO.getCycleCountMapProductByIdForUpdate(cycleCountResult.getCycleCountMapProduct().getId());
		cc.setQuantityBeforeCount(cc.getQuantityBeforeCount() - cycleCountResult.getQuantityBeforeCount());
		cc.setQuantityCounted(cc.getQuantityCounted() - cycleCountResult.getQuantityCounted());
		cycleCountMapProductDAO.updateCycleCountMapProduct(cc, false, false);
		
		repo.delete(cycleCountResult);
	}
	
	@Override
	public void deleteListCycleCountResult(List<CycleCountResult> lstCycleCountResult, LogInfoVO log) throws DataAccessException {
		if (lstCycleCountResult == null) {
			throw new IllegalArgumentException("lstCycleCountResult");
		}
		for (CycleCountResult cycleCountResult: lstCycleCountResult) {
			//cap nhat so luong dem va so luong chot
			CycleCountMapProduct cc = cycleCountMapProductDAO.getCycleCountMapProductByIdForUpdate(cycleCountResult.getCycleCountMapProduct().getId());
			cc.setQuantityBeforeCount(cc.getQuantityBeforeCount() - cycleCountResult.getQuantityBeforeCount());
			cc.setQuantityCounted(cc.getQuantityCounted() - cycleCountResult.getQuantityCounted());
			cc.setUpdateDate(commonDAO.getSysDate());
			cc.setUpdateUser(log.getStaffCode());
			cycleCountMapProductDAO.updateCycleCountMapProduct(cc, false, false);
		}
		repo.delete(lstCycleCountResult);
	}

	@Override
	public CycleCountResult getCycleCountResultById(long id) throws DataAccessException {
		return repo.getEntityById(CycleCountResult.class, id);
	}
	
	@Override
	public void updateCycleCountResult(CycleCountResult cycleCountResult, Boolean isUpdateQuantityBfrCount) throws DataAccessException {
		if (cycleCountResult == null) {
			throw new IllegalArgumentException("cycleCountResult");
		}
		CycleCountMapProduct cc = cycleCountMapProductDAO.getCycleCountMapProductByIdForUpdate(cycleCountResult.getCycleCountMapProduct().getId());  
		if (cc != null) {
			cycleCountResult.setCycleCount(cc.getCycleCount());
			cycleCountResult.setProduct(cc.getProduct());
		}
		if (cycleCountResult.getLot() != null) {
			cycleCountResult.setLot(cycleCountResult.getLot().toUpperCase());
			if (checkIfRecordExist(cycleCountResult.getCycleCountMapProduct().getId(), cycleCountResult.getLot(), cycleCountResult.getId()))
				return;
		}
		cycleCountResult.setUpdateDate(commonDAO.getSysDate());
		//cap nhat so luong dem va so luong chot
		CycleCountResult ccr = this.getCycleCountResultById(cycleCountResult.getId());
		if (Boolean.TRUE.equals(isUpdateQuantityBfrCount)) {
			ProductLot pl = productLotDAO.getProductLotByCodeAndOwnerAndProductForUpdate(cycleCountResult.getLot(), cc.getCycleCount().getId(), StockObjectType.SHOP, cc.getProduct().getId());
			if (pl == null)
				throw new IllegalArgumentException("pl == null");
			cycleCountResult.setQuantityBeforeCount(pl.getQuantity());
		}
		cc.setQuantityBeforeCount(cc.getQuantityBeforeCount() - ccr.getQuantityBeforeCount() + cycleCountResult.getQuantityBeforeCount());
		cc.setQuantityCounted(cc.getQuantityCounted() - ccr.getQuantityCounted() + cycleCountResult.getQuantityCounted());
		cycleCountMapProductDAO.updateCycleCountMapProduct(cc, false, false);
		
		repo.update(cycleCountResult);
	}
	
	@Override
	public void updateListCycleCountResult(List<CycleCountResultVO> lstCycleCountResultVO) throws DataAccessException {
		if (lstCycleCountResultVO == null) {
			throw new IllegalArgumentException("cycleCountResult");
		}
		List<CycleCountResult> lstUpdated = new ArrayList<CycleCountResult>();
		for (CycleCountResultVO cycleCountResultVO: lstCycleCountResultVO) {
			CycleCountResult cycleCountResult = cycleCountResultVO.getCycleCountResult();
			Boolean isUpdateQuantityBfrCount = cycleCountResultVO.getIsUpdateQuantBfrCount();
			CycleCountMapProduct cc = cycleCountResult.getCycleCountMapProduct();  
			if (cc != null) {
				cycleCountResult.setCycleCount(cc.getCycleCount());
				cycleCountResult.setProduct(cc.getProduct());
			}
			if (cycleCountResult.getLot() != null) {
				cycleCountResult.setLot(cycleCountResult.getLot().toUpperCase());
				if (checkIfRecordExist(cycleCountResult.getCycleCountMapProduct().getId(), cycleCountResult.getLot(), cycleCountResult.getId()))
					continue;
			}
			cycleCountResult.setUpdateDate(commonDAO.getSysDate());
			CycleCountResult ccr = this.getCycleCountResultById(cycleCountResult.getId());
			if (Boolean.TRUE.equals(isUpdateQuantityBfrCount)) {
				ProductLot pl = productLotDAO.getProductLotByCodeAndOwnerAndProductForUpdate(cycleCountResult.getLot(), cc.getCycleCount().getId(), StockObjectType.SHOP, cc.getProduct().getId());
				if (pl == null)
					throw new IllegalArgumentException("pl == null");
				cycleCountResult.setQuantityBeforeCount(pl.getQuantity());
			}
			cc.setQuantityBeforeCount(cc.getQuantityBeforeCount() - ccr.getQuantityBeforeCount() + cycleCountResult.getQuantityBeforeCount());
			cc.setQuantityCounted(cc.getQuantityCounted() - ccr.getQuantityCounted() + cycleCountResult.getQuantityCounted());
			cycleCountMapProductDAO.updateCycleCountMapProduct(cc, false, false);
			lstUpdated.add(cycleCountResult);
		}
		
		repo.update(lstUpdated);
	}
	
	@Override
	public void createListCycleCountResult(List<CycleCountResult> lstCycleCountResult) throws DataAccessException {
		if (lstCycleCountResult != null && lstCycleCountResult.size() > 0) {
			repo.create(lstCycleCountResult);
		}
	}
	
	@Override
	public void createListCycleCountResult(List<CycleCountResult> lstCycleCountResult, LogInfoVO log, Boolean isNewProduct) throws DataAccessException {
		if (lstCycleCountResult == null) {
			throw new IllegalArgumentException("cycleCountResult");
		}
		List<CycleCountResult> lstUpdated = new ArrayList<CycleCountResult>();
		List<CycleCountResult> lstCreated = new ArrayList<CycleCountResult>();

		Map<Long, Integer> quantityCountedMap = new TreeMap<Long, Integer>();
		Map<Long, Integer> quantityBeforeCountMap = new TreeMap<Long, Integer>();
		
		for (CycleCountResult cycleCountResult: lstCycleCountResult) {
			CycleCountMapProduct cc = cycleCountResult.getCycleCountMapProduct();  
			if (cc != null) {
				cycleCountResult.setCycleCount(cc.getCycleCount());
				cycleCountResult.setProduct(cc.getProduct());
			}
			if (cycleCountResult.getLot() != null) {
				cycleCountResult.setLot(cycleCountResult.getLot().toUpperCase());
				/*if (checkIfRecordExist(cycleCountResult.getCycleCountMapProduct().getId(), cycleCountResult.getLot(), null))
					continue;*/
			}
			
			CycleCountResult ccr = this.getCycleCountResult(cycleCountResult.getLot(), cycleCountResult.getCycleCountMapProduct().getId());
			if (ccr != null) {
				quantityCountedMap.put(ccr.getId(), ccr.getQuantityCounted());
				quantityBeforeCountMap.put(ccr.getId(), ccr.getQuantityBeforeCount());
				
				ccr.setQuantityCounted(cycleCountResult.getQuantityCounted());
				ccr.setQuantityBeforeCount(cycleCountResult.getQuantityBeforeCount());
				ccr.setCountDate(cycleCountResult.getCountDate());
				ccr.setUpdateDate(commonDAO.getSysDate());
				ccr.setUpdateUser(log.getStaffCode());
				lstUpdated.add(ccr);
			}
			else{
				cycleCountResult.setCreateDate(commonDAO.getSysDate());
				cycleCountResult.setCreateUser(log.getStaffCode());
				lstCreated.add(cycleCountResult);
			}
				
		}
		//9
		for (CycleCountResult cycleCountResult: lstCreated) {
			CycleCountMapProduct cc = cycleCountResult.getCycleCountMapProduct();
			cc.setQuantityBeforeCount(cc.getQuantityBeforeCount() + cycleCountResult.getQuantityBeforeCount());
			cc.setQuantityCounted(cc.getQuantityCounted() + cycleCountResult.getQuantityCounted());
			if(Boolean.FALSE.equals(isNewProduct)) {
				cc.setUpdateUser(log.getStaffCode());
			}
			cycleCountMapProductDAO.updateCycleCountMapProduct(cc, false, isNewProduct);
		}
		for (CycleCountResult cycleCountResult: lstUpdated) {
			CycleCountMapProduct cc = cycleCountResult.getCycleCountMapProduct();
			cc.setQuantityBeforeCount(cc.getQuantityBeforeCount() - quantityBeforeCountMap.get(cycleCountResult.getId()) + cycleCountResult.getQuantityBeforeCount());
			cc.setQuantityCounted(cc.getQuantityCounted() - quantityCountedMap.get(cycleCountResult.getId()) + cycleCountResult.getQuantityCounted());
			if(Boolean.FALSE.equals(isNewProduct)) {
				cc.setUpdateUser(log.getStaffCode());
			}
			cycleCountMapProductDAO.updateCycleCountMapProduct(cc, false, isNewProduct);
		}
		repo.create(lstCreated);
		repo.update(lstUpdated);
	}
	
	@Override
	public Boolean checkIfRecordExist(long cycleCountMapProductId, String lotCode, Long id) throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		/*sql.append("select count(1) as count from cycle_count_result where cycle_count_result_id=? and lot=?"); */
		sql.append("select count(1) as count from cycle_count_result where cycle_count_map_product_id=? and lot=?"); //Edit:tientv11
		List<Object> params = new ArrayList<Object>();
		params.add(cycleCountMapProductId);
		params.add(lotCode.toUpperCase());
		if (id != null) {
			sql.append("	and cycle_count_result_id!=?");
			params.add(id);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}
	
	@Override
	public List<CycleCountResult> getListCycleCount(KPaging<CycleCountResult> kPaging, 
			Long cycleCountMapProductId) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from cycle_count_result c where 1=1 ");
		if (cycleCountMapProductId != null) {
			sql.append(" and cycle_count_map_product_id=?");
			params.add(cycleCountMapProductId);
		}
		sql.append(" order by cycle_count_result_id asc");
		if (kPaging != null)
			return repo.getListBySQLPaginated(CycleCountResult.class, sql.toString(), params, kPaging);
		else
			return repo.getListBySQL(CycleCountResult.class, sql.toString(), params);
	}
	
	private CycleCountResult getCycleCountResult(String lot, Long cycleCountMapProductId) throws DataAccessException {
		String sql = "select * from cycle_count_result where lot=? and cycle_count_map_product_id=?";
		List<Object> params = new ArrayList<Object>();
		params.add(lot);
		params.add(cycleCountMapProductId);
		return repo.getEntityBySQL(CycleCountResult.class, sql, params);
	}
	
	@Override
	public List<RptCycleCountDiff3VO> getListRptCycleCountDiff3VO(Long shopId,
			Date fromDate, Date toDate, String cycleCountCode) throws DataAccessException {
		
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptCycleCountDiff3VO>();
		
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *");
		sql.append(" FROM");
		sql.append("   (SELECT cc.cycle_count_code                                                   AS cycleCountCode,");
		sql.append("     ccmp.stock_card_number                                                      AS stockCardNumber,");
		sql.append("     p.product_code                                                              AS productCode,");
		sql.append("     ccr.lot                                                                     AS lot,");
		sql.append("     ccmp.CREATE_DATE                                                               AS startDate,");
		sql.append("     NVL(ccr.quantity_counted,0)                                                 AS quantityCounted,");
		sql.append("     NVL(ccr.quantity_before_count ,0)                                           AS quantityBeforeCount,");
		sql.append("     pr.price                                                                    AS price,");
		sql.append("     NVL(ccr.quantity_before_count,0)  - NVL(ccr.quantity_counted ,0)            AS quantityDiff,");
		sql.append("     (NVL(ccr.quantity_before_count,0) - NVL(ccr.quantity_counted,0)) * pr.price AS amountDiff,");
		sql.append("	 p.check_lot checkLot");
		sql.append("   FROM ");
		sql.append("     cycle_count_map_product ccmp left join cycle_count_result ccr on ccr.cycle_count_map_product_id = ccmp.cycle_count_map_product_id,");

		sql.append("     cycle_count cc,");
		sql.append("     product p,");
		sql.append("     price pr");
		sql.append("   WHERE pr.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   AND ccmp.cycle_count_id              = cc.cycle_count_id");
		sql.append("   AND p.product_id                     = ccmp.product_id");
		sql.append("   AND p.product_id                     = pr.product_id");
		sql.append("   AND p.check_lot                      = 1");
		sql.append("   AND pr.from_date                     < TRUNC(sysdate) + 1");
		sql.append("   AND (pr.to_date                     IS NULL");
		sql.append("   OR pr.to_date                       >= TRUNC(sysdate))");
		if (fromDate != null) {
			sql.append("   AND cc.start_date                   >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("   AND cc.start_date                   <= TRUNC(?) + 1");
			params.add(toDate);
		}
		sql.append("   AND cc.shop_id                       = ?");
		params.add(shopId);
		if (!StringUtility.isNullOrEmpty(cycleCountCode)) {
			sql.append("   AND cc.cycle_count_code                       = ?");
			params.add(cycleCountCode.toUpperCase());
		}
		
		//update: HUNGNM
//		sql.append("   UNION");
//		sql.append("   SELECT cc.cycle_count_code                                                    AS cycleCountCode,");
//		sql.append("     ccmp.stock_card_number                                                      AS stockCardNumber,");
//		sql.append("     p.product_code                                                              AS productCode,");
//		sql.append("     pl.lot                                                                      AS lot,");
//		sql.append("     cc.start_date                                                               AS startDate,");
//		sql.append("     NULL						                                                 AS quantityCounted,");
//		sql.append("     NULL								                                         AS quantityBeforeCount,");
//		sql.append("     pr.price                                                                    AS price,");
//		sql.append("     NULL        															     AS quantityDiff,");
//		sql.append("     NULL																		 AS amountDiff");
//		sql.append("   FROM product_lot pl,");
//		sql.append("     cycle_count_map_product ccmp,");
//		sql.append("     cycle_count cc,");
//		sql.append("     product p,");
//		sql.append("     price pr");
//		sql.append("   WHERE pl.product_id = ccmp.product_id");
//		sql.append("   AND ccmp.cycle_count_id              = cc.cycle_count_id");
//		sql.append("   AND p.product_id                     = ccmp.product_id");
//		sql.append("   AND p.product_id                     = pr.product_id");
//		sql.append("   AND p.check_lot                      = 1");
//		
//		sql.append("   AND pl.lot not in ( ");
//		
//		
//		sql.append("   SELECT ccr.lot");
//		sql.append("   FROM cycle_count_result ccr,");
//		sql.append("     cycle_count_map_product ccmp,");
//		sql.append("     cycle_count cc,");
//		sql.append("     product p,");
//		sql.append("     price pr");
//		sql.append("   WHERE ccr.cycle_count_map_product_id = ccmp.cycle_count_map_product_id and pr.status = ?");
//		params.add(ActiveType.RUNNING.getValue());
//		sql.append("   AND ccmp.cycle_count_id              = cc.cycle_count_id");
//		sql.append("   AND p.product_id                     = ccmp.product_id");
//		sql.append("   AND p.product_id                     = pr.product_id");
//		sql.append("   AND p.check_lot                      = 1");
//		sql.append("   AND pr.from_date                     < TRUNC(sysdate) + 1");
//		sql.append("   AND (pr.to_date                     IS NULL");
//		sql.append("   OR pr.to_date                       >= TRUNC(sysdate))");
//		if (fromDate != null) {
//			sql.append("   AND cc.start_date                   >= TRUNC(?)");
//			params.add(fromDate);
//		}
//		if (toDate != null) {
//			sql.append("   AND cc.start_date                   <= TRUNC(?) + 1");
//			params.add(toDate);
//		}
//		sql.append("   AND cc.shop_id                       = ?");
//		params.add(shopId);
//		if (!StringUtility.isNullOrEmpty(cycleCountCode)) {
//			sql.append("   AND cc.cycle_count_code                       = ?");
//			params.add(cycleCountCode.toUpperCase());
//		}
//		
//		sql.append(" )");
//		
//		sql.append("   AND pr.status = ? and pr.from_date                     < TRUNC(sysdate) + 1");
//		params.add(ActiveType.RUNNING.getValue());
//		sql.append("   AND (pr.to_date                     IS NULL");
//		sql.append("   OR pr.to_date                       >= TRUNC(sysdate))");
//		if (fromDate != null) {
//			sql.append("   AND cc.start_date                   >= TRUNC(?)");
//			params.add(fromDate);
//		}
//		if (toDate != null) {
//			sql.append("   AND cc.start_date                   <= TRUNC(?) + 1");
//			params.add(toDate);
//		}
//		sql.append("   AND cc.shop_id                       = ?");
//		params.add(shopId);
//		sql.append("   AND pl.owner_id = ?");
//		params.add(shopId);
//		sql.append("   AND pl.owner_type = ?");
//		params.add(StockObjectType.SHOP.getValue());
//		
//		if (!StringUtility.isNullOrEmpty(cycleCountCode)) {
//			sql.append("   AND cc.cycle_count_code                       = ?");
//			params.add(cycleCountCode.toUpperCase());
//		}
		//end update
		
		sql.append("   UNION");
		sql.append("   SELECT cc.cycle_count_code                                                      AS cycleCountCode,");
		sql.append("     ccmp.stock_card_number                                                        AS stockCardNumber,");
		sql.append("     p.product_code                                                                AS productCode,");
		sql.append("     NULL                                                                          AS lot,");
		sql.append("     ccmp.CREATE_DATE                                                                 AS startDate,");
		sql.append("     NVL(ccmp.quantity_counted,0)                                                  AS quantityCounted,");
		sql.append("     NVL(ccmp.quantity_before_count,0)                                             AS quantityBeforeCount,");
		sql.append("     pr.price                                                                      AS price,");
		sql.append("     NVL(ccmp.quantity_before_count,0)  - NVL(ccmp.quantity_counted,0)             AS quantityDiff,");
		sql.append("     (NVL(ccmp.quantity_before_count,0) - NVL(ccmp.quantity_counted,0)) * pr.price AS amountDiff,");
		sql.append("	 p.check_lot checkLot");
		sql.append("   FROM cycle_count_map_product ccmp,");
		sql.append("     cycle_count cc,");
		sql.append("     product p,");
		sql.append("     price pr");
		sql.append("   WHERE ccmp.cycle_count_id            = cc.cycle_count_id and pr.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   AND p.product_id                     = ccmp.product_id");
		sql.append("   AND p.product_id                     = pr.product_id");
		sql.append("   AND p.check_lot                      = 0");
		sql.append("   AND pr.from_date             < TRUNC(sysdate) + 1");
		sql.append("   AND (pr.to_date                     IS NULL");
		sql.append("   OR pr.to_date                >= TRUNC(sysdate))");
		if (fromDate != null) {
			sql.append("   AND cc.start_date                   >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("   AND cc.start_date                   < TRUNC(?) + 1");
			params.add(toDate);
		}
		sql.append("   AND cc.shop_id                       = ?");
		params.add(shopId);
		if (!StringUtility.isNullOrEmpty(cycleCountCode)) {
			sql.append("   AND cc.cycle_count_code                       = ?");
			params.add(cycleCountCode.toUpperCase());
		}
		sql.append("   )");
		sql.append(" ORDER BY cycleCountCode,");
		sql.append("   stockCardNumber,");
		sql.append("   productCode,");
		sql.append("   lot");
		
		String[] fieldNames = { "cycleCountCode",// 1
				"stockCardNumber",
				"productCode",// 2
				"lot",// 3
				"startDate",// 4
				"quantityCounted",// 5
				"quantityBeforeCount",// 6
				"price",// 7
				"quantityDiff",// 8
				"amountDiff",// 9
				"checkLot"
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.TIMESTAMP,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.INTEGER,// 6
				StandardBasicTypes.BIG_DECIMAL, // 7
				StandardBasicTypes.INTEGER, // 8
				StandardBasicTypes.BIG_DECIMAL, // 9
				StandardBasicTypes.INTEGER
		};
		return repo.getListByQueryAndScalar(RptCycleCountDiff3VO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/**
	 * @author trietptm
	 */
	@Override
	public List<RptCycleCountDiff3VO> getListRptCycleCountDiff3VO(long cycleCountId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("   SELECT p.PRODUCT_CODE productCode, ");
		sql.append("         p.PRODUCT_NAME productName, ");
		sql.append("   		 cc.CYCLE_COUNT_CODE cycleCountCode, ");
		sql.append("  		 cc.START_DATE startDate, ");
		sql.append("         ccmp.QUANTITY_BEFORE_COUNT quantityBeforeCount, ");
		sql.append("         ccmp.QUANTITY_COUNTED quantityCounted, ");
		sql.append("         NVL(psd.PRICE, 0) price, ");
		sql.append("         (ccmp.QUANTITY_BEFORE_COUNT - NVL(ccmp.QUANTITY_COUNTED, 0)) quantityDiff, ");
		sql.append("         (ccmp.QUANTITY_BEFORE_COUNT - NVL(ccmp.QUANTITY_COUNTED, 0)) * NVL(psd.PRICE, 0) amountDiff, ");
		sql.append("		 p.convfact convfact ");
		sql.append("   FROM CYCLE_COUNT cc  ");
		sql.append("   JOIN CYCLE_COUNT_MAP_PRODUCT ccmp on cc.CYCLE_COUNT_ID = ccmp.CYCLE_COUNT_ID ");
		sql.append("   JOIN PRODUCT p on ccmp.PRODUCT_ID = p.PRODUCT_ID ");
		sql.append("   LEFT join PRICE_SHOP_DEDUCED psd on ccmp.PRODUCT_ID = psd.PRODUCT_ID and cc.SHOP_ID = psd.SHOP_ID ");
		sql.append("                                   AND psd.FROM_DATE < cc.START_DATE + 1 ");
		sql.append("                                   AND (psd.TO_DATE >= cc.START_DATE OR psd.TO_DATE IS NULL) ");
		sql.append("   WHERE cc.cycle_count_id = ? ");
		params.add(cycleCountId);
		sql.append("   ORDER BY p.order_index, p.product_code asc ");
		
		String fieldNames[] = {
				"cycleCountCode",
				"productCode",
				"productName",
				"startDate",
				"quantityCounted",
				"quantityBeforeCount",
				"price",
				"quantityDiff",
				"amountDiff",
				"convfact"
		};
		
		Type fieldTypes[] = {
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.TIMESTAMP,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER
		};
		
		return repo.getListByQueryAndScalar(RptCycleCountDiff3VO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<CycleCountResult> getListCycleCountResultEx(KPaging<CycleCountResult> kPaging, 
			Long cycleCountMapProductId, Long ownerId, StockObjectType ownerType, Long productId) 
					throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		
		selectSql.append(" SELECT * FROM ( ");
		sql.append(" SELECT pl.lot AS lot, ");
		sql.append("   pl.quantity AS quantityBeforeCount, ");
		sql.append("   0           AS quantityCounted ");
		sql.append(" FROM product_lot pl ");
		sql.append(" WHERE pl.object_id = ? and pl.quantity > 0 ");
		params.add(ownerId);
		sql.append(" AND pl.object_type = ? ");
		params.add(ownerType.getValue());
		sql.append(" AND pl.status     = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND pl.product_id = ? ");
		params.add(productId);
		
		if (cycleCountMapProductId != null) {
			sql.append(" AND NOT EXISTS ");
			sql.append("   (SELECT 1 ");
			sql.append("   FROM cycle_count_result ccr ");
			sql.append("   WHERE ccr.cycle_count_map_product_id = ? ");
			params.add(cycleCountMapProductId);
			sql.append("   AND ccr.lot                          = pl.lot ");
			sql.append("   ) ");
			sql.append(" UNION ");
			sql.append(" SELECT ccr.lot              AS lot, ");
			sql.append("   ccr.quantity_before_count AS quantityBeforeCount, ");
			sql.append("   ccr.quantity_counted      AS quantityCounted ");
			sql.append(" FROM cycle_count_result ccr ");
			sql.append(" WHERE ccr.cycle_count_map_product_id = ?  ");
			params.add(cycleCountMapProductId);
		}
		
		selectSql.append(sql);
		selectSql.append(" ) ORDER BY lot");
		
		List<Object> countParams = new ArrayList<Object>();
		countSql.append(" SELECT count(1) as count ");
		countSql.append(" FROM product_lot pl ");
		countSql.append(" WHERE pl.object_id = ? and pl.quantity > 0 ");
		countParams.add(ownerId);
		countSql.append(" AND pl.object_type = ? ");
		countParams.add(ownerType.getValue());
		countSql.append(" AND pl.status     = ? ");
		countParams.add(ActiveType.RUNNING.getValue());
		countSql.append(" AND pl.product_id = ? ");
		countParams.add(productId);
		
		String[] fieldNames = {"lot", "quantityBeforeCount", "quantityCounted"};
		Type[] fieldTypes = {StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER};
		if (kPaging != null)
			return repo.getListByQueryAndScalarPaginated(CycleCountResult.class, fieldNames, fieldTypes, selectSql.toString(), countSql.toString(), params, countParams, kPaging);
		else
			return repo.getListByQueryAndScalar(CycleCountResult.class, fieldNames, fieldTypes, selectSql.toString(), params);
	}
}

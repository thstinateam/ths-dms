package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.filter.CycleFilter;
import ths.dms.core.entities.vo.CycleVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface CycleDAO {

	Cycle createCycle(Cycle cycle, LogInfoVO logInfo) throws DataAccessException;

	void deleteCycle(Cycle cycle, LogInfoVO logInfo) throws DataAccessException;

	void updateCycle(Cycle cycle, LogInfoVO logInfo) throws DataAccessException;
	
	Cycle getCycleById(long id) throws DataAccessException;
	
	Cycle getCycleByDate(Date date) throws DataAccessException;

	Cycle getCycleByYearAndNum(int year, int num) throws DataAccessException;

	List<Integer> getListYearOfAllCycle() throws DataAccessException;

	/**
	 * @author tungmt
	 * @since 24/6/2015
	 * @param filter
	 * @return Danh sach cycle theo filter truyen vao
	 * @throws DataAccessException
	 */
	List<CycleVO> getListCycleByFilter(CycleFilter filter)
			throws DataAccessException;

	/**
	 * @author hoanv25
	 * @since July 24/2015
	 * @param cycleId
	 * @return Danh sach Kpi theo cycleId truyen vao
	 * @throws DataAccessException
	 */
	List<CycleVO> getListKPIByCycle(Long cycleId) throws DataAccessException;

	/**
	 * @author hoanv25
	 * @since July 24/2015
	 * @param cycleId
	 * @param lstKeyShopId
	 * @param fromDate
	 * @param toDate
	 * @return Danh sach Kpi theo cycleId truyen vao
	 * @throws DataAccessException
	 */
	List<CycleVO> getListKeyShopByCycle(Long cycleId, List<Long> lstKeyShopId, String fromDate, String toDate) throws DataAccessException;

	/**
	 * Lay chu ky hien tai
	 * @author hunglm16
	 * @return
	 * @throws DataAccessException
	 * @since 19/09/2015
	 */
	Cycle getCycleByCurrentSysdate() throws DataAccessException;

	/**
	 * @author vuongmq
	 * @param filter
	 * @return Cycle
	 * @throws DataAccessException
	 * @since 23/10/2015
	 */
	Cycle getCycleByFilter(CycleFilter filter) throws DataAccessException;

	/**
	 * Lay cycle sau cycle Id truyen vao
	 * @author vuongmq
	 * @param cycleId
	 * @return Cycle
	 * @throws DataAccessException
	 * @since 12/11/2015
	 */
	Cycle getCycleLast(Long cycleId) throws DataAccessException;
}

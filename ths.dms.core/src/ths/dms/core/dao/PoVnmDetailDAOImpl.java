package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PoVnmDetail;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.entities.vo.PoVnmDetailVO;
import ths.core.entities.vo.rpt.RptInputProductVinamilkVO;
import ths.core.entities.vo.rpt.RptPoCfrmFromDvkh2VO;
import ths.core.entities.vo.rpt.RptProductPoVnmDetail2VO;
import ths.core.entities.vo.rpt.Rpt_PO_CTTTVO;
import ths.core.entities.vo.rpt.Rpt_PO_CTTT_MAPPINGVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.GroupUtility;
import ths.dms.core.dao.repo.IRepository;

public class PoVnmDetailDAOImpl implements PoVnmDetailDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ProductDAO productDAO;
	
	@Override
	public PoVnmDetail createPoVnmDetail(PoVnmDetail poVnmDetail) throws DataAccessException {
		if (poVnmDetail == null) {
			throw new IllegalArgumentException("poVnmDetail");
		}
		repo.create(poVnmDetail);
		return repo.getEntityById(PoVnmDetail.class, poVnmDetail.getId());
	}

	@Override
	public void deletePoVnmDetail(PoVnmDetail poVnmDetail) throws DataAccessException {
		if (poVnmDetail == null) {
			throw new IllegalArgumentException("poVnmDetail");
		}
		repo.delete(poVnmDetail);
	}

	@Override
	public PoVnmDetail getPoVnmDetailById(long id) throws DataAccessException {
		return repo.getEntityById(PoVnmDetail.class, id);
	}
	
	@Override
	public PoVnmDetail getPoVnmDetailByIdForUpdate(long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM_DETAIL where PO_VNM_DETAIL_ID = ? for update");
		params.add(id);
		return repo.getFirstBySQL(PoVnmDetail.class, sql.toString(), params);
	}
	
	@Override
	public void updatePoVnmDetail(PoVnmDetail poVnmDetail) throws DataAccessException {
		if (poVnmDetail == null) {
			throw new IllegalArgumentException("poVnmDetail");
		}
		repo.update(poVnmDetail);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.PoVnmDetailDAO#getPoVnmDetailForUpdatePoVnm(java.lang.String)
	 */
	@Override
	public List<PoVnmDetail> getListPoVnmDetailByPoVnmId(KPaging<PoVnmDetail> kPaging, Long poVnmId)
			throws DataAccessException {
		if (poVnmId == null) {
			throw new IllegalArgumentException("poVnmId is not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select pvd.* from PO_VNM_DETAIL pvd left join product p on pvd.product_id = p.product_id where 1 = 1");
		sql.append(" and pvd.PO_VNM_ID = ?");
		params.add(poVnmId);
		sql.append(" order by p.product_code");
		
		String countSql = "select count(1) count from po_vnm_detail where po_vnm_id = ?";
		
		if (kPaging == null)
			return repo.getListBySQL(PoVnmDetail.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(PoVnmDetail.class, sql.toString(), countSql, params, params, kPaging);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.PoVnmDetailDAO#getListPoVnmDetailVO(java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<PoVnmDetailVO> getListPoVnmDetailVO(Long poVnmId, Long shopId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (poVnmId == null) {
			throw new IllegalArgumentException("poVnmId is not null");
		}
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select temp.*, st.quantity as quantityStore from");
		sql.append(" ((select p.product_id as productId ,p.product_code as productCode, p.product_name as productName, p.convfact as convfact, pvd.quantity as quantity, pvd.price as price, pvd.amount as amount, null as lot");
		sql.append(" from po_vnm_detail pvd, product p, po_vnm pv where pvd.product_id = p.product_id and p.check_lot = 0 and pv.po_vnm_id = pvd.po_vnm_id and pv.po_vnm_id = ?)");
		sql.append(" union");
		sql.append(" (select pd.product_id as productId, pd.product_code as productCode, pd.product_name as productName, pd.convfact as convfact, pvl.quantity as quantity, pvl.price as price, pvl.amount as amount, pvll.lot as lot");
		sql.append(" from po_vnm_lot pvl, product pd, po_vnm_lot pvll");
		sql.append(" where pvll.product_id = pd.product_id and pvl.po_vnm_detail_id");
		sql.append(" in (select pvd.po_vnm_detail_id from po_vnm_detail pvd, product p, po_vnm pv");
		sql.append(" where pvd.product_id = p.product_id and p.check_lot = 1 and pv.po_vnm_id = pvd.po_vnm_id and pv.po_vnm_id = ?) and pd.product_id = pvl.product_id)) temp,");
		sql.append(" stock_total st");
		sql.append(" where st.product_id = temp.productId and st.object_type = ?");
		sql.append(" and st.object_id = ?");
		params.add(poVnmId);
		params.add(poVnmId);
		params.add(StockObjectType.SHOP.getValue());
		params.add(shopId);
		
		final String[] fieldNames = new String[] { //
				"productId",// 1
				"productCode", // 2
				"productName", // 3
				"convfact",//4
				"quantity", // 5
				"price", // 6
				"amount", // 7
				"quantityStore", // 9
				"lot", //10
				};

		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.INTEGER,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.BIG_DECIMAL,// 6
				StandardBasicTypes.BIG_DECIMAL,// 7
				StandardBasicTypes.INTEGER,// 9
				StandardBasicTypes.STRING,// 10
		};
		return repo.getListByQueryAndScalar(PoVnmDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<PoVnmDetail> getListPoVnmDetailByPoVnmId(Long poVnmId, Long shopId,Long productId)
			throws DataAccessException {
		if (poVnmId == null) {
			throw new IllegalArgumentException("poVnmId is not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select pvd.* from PO_VNM_DETAIL pvd join warehouse w on w.warehouse_id = pvd.warehouse_id where 1 = 1");
		sql.append(" and pvd.PO_VNM_ID = ?");
		params.add(poVnmId);
		if(productId!=null && productId>0){
			sql.append(" and pvd.product_id = ?");
			params.add(productId);
		}
		sql.append(" order by w.seq ");
	    return repo.getListBySQL(PoVnmDetail.class, sql.toString(), params);

	}
	
	// phuongvm dung xoa nha
	@Override
	public List<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmId(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId)
			throws DataAccessException {
		if (poVnmId == null) {
			throw new IllegalArgumentException("poVnmId is null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT ");
		sql.append(" temp.poVnmDetailLotId AS poVnmDetailLotId,");
		sql.append(" temp.productId AS productId,");
		sql.append(" temp.productCode AS productCode,");
		sql.append(" temp.productName AS productName,");
		sql.append(" temp.checkLot     AS checkLot,");
		sql.append(" temp.quantity AS quantity,");
		sql.append(" temp.quantityPay AS quantityPay,");
		sql.append(" temp.convfact AS convfact,");
		sql.append(" temp.price AS price,");
		sql.append(" temp.amount AS amount,");
		sql.append(" temp.lot AS lot,");
		sql.append(" temp.productType as productType,");
		sql.append(" case ");
		sql.append("	when temp.lot = '' then NVL(st.quantity, 0)");
		sql.append("	when temp.lot is NULL then NVL(st.quantity, 0)");
//		sql.append("	else NVL(pl.quantity, 0) end");	
		sql.append("	end");	
		sql.append("  AS quantityStock,");
		sql.append(" NVL(st.quantity, 0) AS quantityStockTotal,");
		sql.append(" w.warehouse_name as wareHouseName, ");
		sql.append(" temp.lineSaleOrder as lineSaleOrder, ");
		sql.append(" temp.linePo as linePo");
	    
		fromSql.append(" FROM (");
		
		fromSql.append(" (SELECT pvd.po_vnm_detail_id AS poVnmDetailLotId,");
		fromSql.append(" pd.product_id       AS productId,");
		fromSql.append(" pd.product_code     AS productCode,");
		fromSql.append(" pd.product_name     AS productName,");
		fromSql.append(" pd.CHECK_LOT     AS checkLot,");
		fromSql.append(" pvd.quantity        AS quantity,");
		fromSql.append(" pvd.quantity_pay    AS quantityPay,");
		fromSql.append(" pd.convfact         AS convfact,");
		fromSql.append(" pvd.price           AS price,");
		fromSql.append(" pvd.amount          AS amount,");
		fromSql.append(" ''                  AS lot,");
		fromSql.append(" pvd.product_type as productType,");
		fromSql.append(" pvd.warehouse_id    as warehouse_id,");
		fromSql.append(" pv.shop_id          AS shopId, ");
		fromSql.append(" pvd.so_line_number as lineSaleOrder, ");
		fromSql.append(" pvd.po_line_number as linePo ");
		fromSql.append(" FROM po_vnm_detail pvd,");
		fromSql.append(" product pd,");
		fromSql.append(" po_vnm pv");
		fromSql.append(" WHERE pvd.product_id = pd.product_id");
		fromSql.append(" AND pd.check_lot     = 0");
		fromSql.append(" AND pv.po_vnm_id     = pvd.po_vnm_id");
		fromSql.append(" AND pv.po_vnm_id     = ?");
		params.add(poVnmId);
		fromSql.append(" )");
		
//		fromSql.append(" UNION");
//		
//		fromSql.append(" (SELECT pvl.po_vnm_lot_id AS poVnmDetailLotId,");
//		fromSql.append(" pd.product_id       AS productId,");
//		fromSql.append(" pd.product_code     AS productCode,");
//		fromSql.append(" pd.product_name     AS productName,");
//		fromSql.append(" pd.CHECK_LOT     AS checkLot,");
//		fromSql.append(" pvl.quantity        AS quantity,");
//		fromSql.append(" pvl.quantity_pay    AS quantityPay,");
//		fromSql.append(" pd.convfact         AS convfact,");
//		fromSql.append(" pvl.price           AS price,");
//		fromSql.append(" pvl.amount          AS amount,");
//		fromSql.append(" pvl.lot             AS lot,");
//		fromSql.append(" pv.shop_id          AS shopId");
//		fromSql.append(" FROM po_vnm_lot pvl,");
//		fromSql.append(" product pd,");
//		fromSql.append(" po_vnm_detail pvd,");
//		fromSql.append(" po_vnm pv");
//		fromSql.append(" WHERE pvd.product_id = pd.product_id");
//		fromSql.append(" AND pvl.product_id   = pd.product_id");
//		fromSql.append(" AND pv.po_vnm_id     = pvd.po_vnm_id");
//		fromSql.append(" AND pd.check_lot     = 1");
//		fromSql.append(" AND pvl.po_vnm_id = pv.po_vnm_id");
//		fromSql.append(" AND pvd.po_vnm_id    = ?");
//		params.add(poVnmId);
//		fromSql.append(" )");
		fromSql.append("  )temp");
		
		fromSql.append(" left JOIN stock_total st");
		fromSql.append(" ON (temp.productId  = st.product_id");
		fromSql.append(" AND st.object_type   = ?");
		params.add(StockObjectType.SHOP.getValue());
		fromSql.append(" AND st.object_id     = temp.shopId and st.warehouse_id = temp.warehouse_id)");
//		sql.append(" AND temp.lot        is null)");
		
//		fromSql.append(" LEFT JOIN product_lot pl");
//		fromSql.append(" ON (temp.productId  = pl.product_id");
//		fromSql.append(" AND pl.object_type   = ?");
//		params.add(StockObjectType.SHOP.getValue());
//		fromSql.append(" AND pl.object_id     = temp.shopId");
//		fromSql.append(" AND temp.lot        is not null");
//		fromSql.append(" AND temp.lot        = pl.lot)");
		fromSql.append(" left join warehouse w  on st.warehouse_id = w.warehouse_id");
		sql.append(fromSql.toString());
		sql.append(" ORDER BY temp.lineSaleOrder ");
		
		countSql.append("select count(*) count ");
		countSql.append(fromSql.toString());
		
		
		final String[] fieldNames = new String[] { //
				"poVnmDetailLotId", "productId", "productCode", "productName",
				"checkLot","quantity", "quantityPay", "convfact", 
				"price", "amount", "lot", "productType", "quantityStock",
				"quantityStockTotal","wareHouseName",
				"lineSaleOrder", "linePo"
				};

		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER
		};
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}else{
			return repo.getListByQueryAndScalarPaginated(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}
	
	// phuongvm
	@Override
	public List<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmIdEx(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId,Long shopId,Date lockDate)
			throws DataAccessException {
		if (poVnmId == null) {
			throw new IllegalArgumentException("poVnmId is null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT ");
		sql.append(" NVL((select po_vnm_detail_id from po_vnm_detail where po_vnm_id = ?");
		params.add(poVnmId);
		sql.append(" and product_id = p.product_id and warehouse_id = w.warehouse_id),-1) as poVnmDetailLotId,");
		sql.append(" p.product_id as productId,");
		sql.append(" p.product_code productCode,");
		sql.append(" p.product_name productName,");
		sql.append(" 0 checkLot,");
		sql.append(" NVL((select quantity from po_vnm_detail where po_vnm_id = ?");
		params.add(poVnmId);
		sql.append(" and product_id = p.product_id and warehouse_id = w.warehouse_id),0) quantity,");
		sql.append(" NVL((select quantity_pay from po_vnm_detail where po_vnm_id = ?");
		params.add(poVnmId);
		sql.append(" and product_id = p.product_id and warehouse_id = w.warehouse_id),0) quantityPay,");
		sql.append(" p.convfact                AS convfact,");
		
		/*
		 * Commented by NhanLT - 14/11/2014 - Khi load gia thi phai lay distinct gia chung cua SP tu po_vnm ma khong quan tam kho
		 * sql.append("    and warehouse_id = w.warehouse_id), ");
		 */
		sql.append(" NVL((select distinct(price) from po_vnm_detail where po_vnm_id = ?");
		params.add(poVnmId);
		sql.append("	and product_id = p.product_id ), ");
		
		sql.append(" NVL((select pr.price");
		sql.append("	  from (");
		sql.append("	select dta.*, row_number() over ( partition by dta.product_id order by dta.lver asc) as rnum");
		sql.append("	from ( select mpe.*,");
		sql.append("	(case ");
		sql.append("	 when mpe.shop_channel is null");
		sql.append("		and mpe.shop_id = ?");
		params.add(shopId);
		sql.append("		and customer_type_id is null");
		sql.append("	then 1  ");
		sql.append("	when mpe.shop_channel is null");
		sql.append("	and mpe.shop_id in (select shop_id from shop where status = 1 and shop_type_id = 10");
		sql.append("	start with shop_id = ? connect by prior parent_shop_id = shop_id)");
		params.add(shopId);
		sql.append("	and customer_type_id is null ");
		sql.append("	then 2");
		sql.append("	when mpe.shop_channel is null");
		sql.append("	and mpe.shop_id in (select shop_id from shop where status = 1 and shop_type_id = 9");
		sql.append("	 start with shop_id = ? connect by prior parent_shop_id = shop_id)");
		params.add(shopId);
		sql.append("	and mpe.customer_type_id is null");
		sql.append("	then 3");
		sql.append("	when mpe.shop_channel = (select shop_channel from shop where shop_id =?)");
		params.add(shopId);
		sql.append("	 and mpe.shop_id is null");
		sql.append("	and mpe.customer_type_id is null");
		sql.append("	then 4");
		sql.append("	when mpe.shop_channel is null");
		sql.append("	and mpe.shop_id is null");
		sql.append("	and mpe.customer_type_id is null");
		sql.append("	then 5");
		sql.append("	else 0 end ) lver from price mpe where 1=1 and status=1 and from_date <= trunc(?) and (to_date >= trunc(?) or to_date is null) ) dta where lver != 0  ) pr where pr.rnum = 1 and pr.product_id = p.product_id),0) ) price,");
		sql.append("	NVL((select amount from po_vnm_detail where po_vnm_id = ?");
		params.add(lockDate);
		params.add(lockDate);
		params.add(poVnmId);
		sql.append("	and product_id = p.product_id and warehouse_id = w.warehouse_id),0) amount,");	
		sql.append("  '' lot,");
		sql.append(" st.quantity quantityStock,");
		sql.append(" st.quantity quantityStockTotal,");
		sql.append(" w.warehouse_name wareHouseName,");
		sql.append(" w.warehouse_id wareHouseId");
		
		fromSql.append(" FROM stock_total st");
		
		fromSql.append(" join warehouse w on st.warehouse_id = w.warehouse_id");
		fromSql.append(" join product p on p.product_id = st.product_id");
		fromSql.append(" where st.object_type = 1 and st.object_id = ?");
		params.add(shopId);
		fromSql.append(" and p.product_id in");
		fromSql.append(" (select pvd.product_id from po_vnm_detail pvd where pvd.product_id = p.product_id and pvd.po_vnm_id = ?)");
		params.add(poVnmId);
		sql.append(fromSql.toString());
		sql.append(" order by p.product_code, w.seq");
		
		countSql.append("select count(*) count ");
		countSql.append(fromSql.toString());
		
		
		final String[] fieldNames = new String[] { //
				"poVnmDetailLotId", "productId", "productCode", "productName",
				"checkLot","quantity", "quantityPay", "convfact", 
				"price", "amount", "lot", "quantityStock",
				"quantityStockTotal","wareHouseName","wareHouseId"
				};

		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING,StandardBasicTypes.LONG
		};
		if(kPaging == null){
			return repo.getListByQueryAndScalar(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}else{
			return repo.getListByQueryAndScalarPaginated(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}
	// vuongmq, cai nay join voi bang warehouse, khong lay id tu bang stock_total,
	// cu vuongmq 10/08/2015; khong su dung
	/*@Override
	public List<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmIdStockIn(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId)
			throws DataAccessException {
		if (poVnmId == null) {
			throw new IllegalArgumentException("poVnmId is null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT ");
		sql.append(" temp.poVnmDetailLotId AS poVnmDetailLotId,");
		sql.append(" temp.productId AS productId,");
		sql.append(" temp.productCode AS productCode,");
		sql.append(" temp.productName AS productName,");
		sql.append(" temp.checkLot     AS checkLot,");
		sql.append(" temp.quantity AS quantity,");
		sql.append(" temp.quantityPay AS quantityPay,");
		sql.append(" temp.convfact AS convfact,");
		sql.append(" temp.price AS price,");
		sql.append(" temp.amount AS amount,");
		sql.append(" temp.lot AS lot,");
		sql.append(" case ");
		sql.append("	when temp.lot = '' then NVL(st.quantity, 0)");
		sql.append("	when temp.lot is NULL then NVL(st.quantity, 0)");
//		sql.append("	else NVL(pl.quantity, 0) end");	
		sql.append("	end");	
		sql.append("  AS quantityStock,");
		sql.append(" NVL(st.quantity, 0) AS quantityStockTotal,");
		sql.append(" null AS quantityStock,");
		sql.append(" null AS quantityStockTotal,");
		sql.append(" w.warehouse_name as wareHouseName");
		
		fromSql.append(" FROM (");
		
		fromSql.append(" (SELECT pvd.po_vnm_detail_id AS poVnmDetailLotId,");
		fromSql.append(" pd.product_id       AS productId,");
		fromSql.append(" pd.product_code     AS productCode,");
		fromSql.append(" pd.product_name     AS productName,");
		fromSql.append(" pd.CHECK_LOT     AS checkLot,");
		fromSql.append(" pvd.quantity        AS quantity,");
		fromSql.append(" pvd.quantity_pay    AS quantityPay,");
		fromSql.append(" pd.convfact         AS convfact,");
		fromSql.append(" pvd.price           AS price,");
		fromSql.append(" pvd.amount          AS amount,");
		fromSql.append(" ''                  AS lot,");
		fromSql.append(" pvd.warehouse_id    as warehouse_id,");
		fromSql.append(" pv.shop_id          AS shopId");
		fromSql.append(" FROM po_vnm_detail pvd,");
		fromSql.append(" product pd,");
		fromSql.append(" po_vnm pv");
		fromSql.append(" WHERE pvd.product_id = pd.product_id");
		fromSql.append(" AND pd.check_lot     = 0");
		fromSql.append(" AND pv.po_vnm_id     = pvd.po_vnm_id");
		fromSql.append(" AND pv.po_vnm_id     = ?");
		params.add(poVnmId);
		fromSql.append(" )");
		fromSql.append("  )temp");
		fromSql.append(" join warehouse w  on temp.warehouse_id = w.warehouse_id");
		sql.append(fromSql.toString());
		sql.append(" ORDER BY temp.productCode, w.seq");
		
		countSql.append("select count(*) count ");
		countSql.append(fromSql.toString());
		
		
		final String[] fieldNames = new String[] { //
				"poVnmDetailLotId", "productId", "productCode", "productName",
				"checkLot","quantity", "quantityPay", "convfact", 
				"price", "amount", "lot", "quantityStock",
				"quantityStockTotal","wareHouseName"
				};

		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING
		};
		if(kPaging == null){
			return repo.getListByQueryAndScalar(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}else{
			return repo.getListByQueryAndScalarPaginated(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}*/
	@Override
	public List<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmIdStockIn(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId)
			throws DataAccessException {
		if (poVnmId == null) {
			throw new IllegalArgumentException("poVnmId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT pvd.po_vnm_detail_id AS poVnmDetailLotId, ");
		sql.append(" pd.product_id AS productId, ");
		sql.append(" pd.product_code AS productCode, ");
		sql.append(" pd.product_name AS productName, ");
		sql.append(" pvd.product_type AS productType, ");
		sql.append(" pvd.quantity AS quantity, ");
		sql.append(" pvd.quantity_pay AS quantityPay, ");
		sql.append(" pd.convfact AS convfact, ");
		sql.append(" pvd.price AS price, ");
		sql.append(" pvd.amount AS amount, ");
		sql.append(" pvd.quantity_received AS quantityReceived, ");
		sql.append(" pvd.warehouse_id AS wareHouseId, ");
		sql.append(" pv.shop_id AS shopId, ");
		sql.append(" pvd.price_package AS packagePrice, ");
		sql.append(" pvd.so_line_number AS lineSaleOrder, ");
		sql.append(" pvd.po_line_number AS linePo, ");
		sql.append(" st.quantity as quantityStock ");
		sql.append(" FROM po_vnm_detail pvd ");
		sql.append(" join po_vnm pv on pv.po_vnm_id = pvd.po_vnm_id ");
		sql.append(" join product pd on pd.product_id = pvd.product_id ");
		sql.append(" left join stock_total st on (st.product_id = pd.product_id and st.object_type = 1 ");
		sql.append(" and st.object_id = pv.shop_id and st.warehouse_id = pvd.warehouse_id) ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND pv.po_vnm_id = ? ");
		params.add(poVnmId);
		
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(*) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		
		sql.append(" ORDER BY lineSaleOrder ");
		
		final String[] fieldNames = new String[] { //
				"poVnmDetailLotId", "productId", "productCode", "productName",
				"productType","quantity", "quantityPay", "convfact", 
				"price", "amount", "quantityReceived",
				"wareHouseId","shopId",
				"packagePrice", "lineSaleOrder", "linePo",
				"quantityStock",
				};

		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG, StandardBasicTypes.LONG,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER
		};
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}
	
	@Override
	public List<PoVnmDetailLotVO> getListFullPoVnmDetailLotVOByPoVnmId(KPaging<PoVnmDetailLotVO> kPaging, List<Long> lstPoVnmId) throws DataAccessException {
		if (lstPoVnmId == null) {
			throw new IllegalArgumentException("List poVnmId is null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/*
		fromSql.append(" SELECT ");
		fromSql.append(" temp.productId AS productId, ");
		fromSql.append(" temp.productCode AS productCode, ");
		fromSql.append(" temp.productName AS productName, ");
		fromSql.append(" SUM(temp.quantity) AS quantity, ");
		fromSql.append(" SUM(temp.quantityPay) AS quantityPay, ");
		fromSql.append(" temp.convfact AS convfact, ");
		fromSql.append(" temp.price AS price, ");
		fromSql.append(" SUM(temp.quantity * temp.price) AS amount, ");
		fromSql.append(" SUM(NVL(st.quantity, 0)) AS quantityStockTotal ");
		fromSql.append(" FROM ( ");
		fromSql.append(" SELECT pvd.po_vnm_detail_id AS poVnmDetailLotId, ");
		fromSql.append(" pd.product_id AS productId, ");
		fromSql.append(" pd.product_code AS productCode, ");
		fromSql.append(" pd.product_name AS productName, ");
		fromSql.append(" pvd.quantity AS quantity, ");
		fromSql.append(" pvd.quantity_pay AS quantityPay, ");
		fromSql.append(" pd.convfact AS convfact, ");
		fromSql.append(" pvd.price AS price, ");
		fromSql.append(" pvd.amount AS amount, ");
		fromSql.append(" pv.shop_id AS shopId ");
		fromSql.append(" FROM po_vnm_detail pvd, ");
		fromSql.append(" product pd, ");
		fromSql.append(" po_vnm pv ");
		fromSql.append(" WHERE pvd.product_id = pd.product_id ");
		fromSql.append(" AND pd.check_lot = 0 ");
		fromSql.append(" AND pv.po_vnm_id = pvd.po_vnm_id ");
//		fromSql.append(" AND pv.po_vnm_id in(202, 245, 267) ");
		fromSql.append(" AND pv.po_vnm_id in(-1 ");
		for(int i = 0; i< lstPoVnmId.size(); i++){
			fromSql.append(" ,?");
			params.add(lstPoVnmId.get(i));
		}
		fromSql.append(" ) ");
		fromSql.append(" ) temp ");
		fromSql.append(" LEFT JOIN stock_total st ");
		fromSql.append(" ON (temp.productId = st.product_id ");
		fromSql.append(" AND st.object_type = ? ");
		params.add(StockObjectType.SHOP.getValue());
		fromSql.append(" AND st.object_id = temp.shopId) ");
		fromSql.append(" GROUP BY productId, productCode, productName, price, convfact ");
		sql.append(fromSql.toString());
		sql.append(" ORDER BY temp.productCode");
		*/
		fromSql.append(" SELECT distinct ");
		fromSql.append(" pd.product_id AS productId, ");
		fromSql.append(" pd.product_code AS productCode, ");
		fromSql.append(" pd.product_name AS productName, ");
		fromSql.append(" pvd.quantity AS quantity, ");
		fromSql.append(" pd.convfact AS convfact, ");
		//fromSql.append(" pr.price AS price, ");
		//fromSql.append(" (pvd.quantity * pr.price) AS amount ");
		fromSql.append(" pvd.price AS price, ");
		fromSql.append(" pvd.amount AS amount, ");
		fromSql.append(" pvd.product_type AS productType, ");
		fromSql.append(" pvd.so_line_number as lineSaleOrder, ");
		fromSql.append(" pvd.po_line_number as linePo ");
		/** From Table **/
		fromSql.append(" FROM ");
		fromSql.append(" po_vnm_detail pvd join product pd on pvd.product_id = pd.product_id ");
		//fromSql.append(" join price pr on pd.product_id = pr.product_id ");
		//fromSql.append(" and pr.from_date<trunc(sysdate)+1 and(pr.to_date is null OR pr.to_date >= trunc(sysdate)) and pr.status = 1 ");
		fromSql.append(" WHERE pvd.product_id = pd.product_id ");
		fromSql.append(" AND pvd.po_vnm_id in(-1 ");
		for (int i = 0, sz = lstPoVnmId.size(); i < sz; i++) {
			fromSql.append(" ,?");
			params.add(lstPoVnmId.get(i));
		}
		fromSql.append(" ) ");
		sql.append(fromSql.toString());
		sql.append(" ORDER BY pvd.so_line_number ");
		
		
		countSql.append("select count(*) count from( ");
		countSql.append(fromSql.toString());
		countSql.append(") ");
		final String[] fieldNames = new String[] { //
				"productId", "productCode", "productName","quantity", "convfact", 
				"price", "amount", "productType",
				"lineSaleOrder", "linePo"
				};
		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER
		};
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}
	
	@Override
	public List<PoVnmDetailLotVO> getPoConfirmDetail(KPaging<PoVnmDetailLotVO> kPaging, Long poDVKHId, Long poConfirmId)
			throws DataAccessException {
		if (poDVKHId == null) {
			throw new IllegalArgumentException("poVnmId is null");
		}
		if(poConfirmId == null){
			PoVnmFilter filter = new PoVnmFilter();
			filter.setPoVnmId(poDVKHId);
			return productDAO.getListProductForPoConfirm(kPaging, filter);
		}else{
			StringBuilder sql = new StringBuilder();
			StringBuilder countSql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			
			sql.append(" WITH poconfirm AS");
			sql.append("   (SELECT pr.product_id,");
			sql.append("     SUM(pod.quantity) quantity");
			sql.append("   FROM po_vnm po");
			sql.append("   JOIN po_vnm_detail pod");
			sql.append("   ON po.po_vnm_id = pod.po_vnm_id");
			sql.append("   JOIN product pr");
			sql.append("   ON pr.product_id         = pod.product_id");
			sql.append("   WHERE po.type            = 2");
			sql.append("   AND po.object_type       = 0");
			sql.append("   AND po.po_vnm_id        != ?");
			params.add(poConfirmId);
			sql.append("   AND po.SALE_ORDER_NUMBER =");
			sql.append("     (SELECT SALE_ORDER_NUMBER FROM po_vnm WHERE po_vnm_id = ?");
			params.add(poDVKHId);
			sql.append("     )");
			sql.append("   GROUP BY pr.product_id");
			sql.append("   )");
			sql.append("   select tmp.*,podvkh.totalQuantity quantityStockTotal, podvkh.quantity quantityStock from");
			sql.append(" (SELECT pr.product_id productId,");
			sql.append("   pr.product_code productCode,");
			sql.append("   pr.product_Name productName,");
			sql.append("   pr.check_lot checkLot,");
			sql.append("   pr.convfact convfact,");
			sql.append("   pvd.quantity quantity,");
			sql.append("   pvd.price price,");
			sql.append("   pvd.amount amount,");
			sql.append("   '' lot");
			sql.append(" FROM po_vnm_detail pvd");
			sql.append(" JOIN product pr");
			sql.append(" ON pvd.product_id = pr.product_id");
			sql.append(" WHERE pvd.po_vnm_id   = ?");
			params.add(poConfirmId);
			sql.append(" and pr.check_lot = 0");
			sql.append(" union ");
			sql.append(" SELECT pr.product_id productId,");
			sql.append("   pr.product_code productCode,");
			sql.append("   pr.product_Name productName,");
			sql.append("   pr.check_lot checkLot,");
			sql.append("   pr.convfact convfact,");
			sql.append("   pvd.quantity quantity,");
			sql.append("   pvd.price price,");
			sql.append("   pvd.amount amount,");
			sql.append("   lt.lot lot");
			sql.append(" FROM po_vnm_detail pvd");
			sql.append(" join po_vnm_lot lt on lt.po_vnm_detail_id = pvd.po_vnm_detail_id");
			sql.append(" JOIN product pr");
			sql.append(" ON pvd.product_id = pr.product_id");
			sql.append(" WHERE pvd.po_vnm_id   = ?");
			params.add(poConfirmId);
			sql.append(" and pr.check_lot = 1) tmp");
			sql.append(" join(");
			sql.append(" SELECT productId,");
			sql.append("   productCode ,");
			sql.append("   productName,");
			sql.append("   checkLot,");
			sql.append("   convfact,");
			sql.append("   totalQuantity,");
			sql.append("   (totalQuantity - usedQuantity) quantity");
			sql.append(" FROM");
			sql.append("   ( SELECT DISTINCT pr.product_id productId,");
			sql.append("     pr.product_code productCode ,");
			sql.append("     pr.product_name productName,");
			sql.append("     pr.check_lot checkLot,");
			sql.append("     pr.convfact,");
			sql.append("     pod.quantity totalQuantity,");
			sql.append("     NVL(");
			sql.append("     (SELECT quantity FROM poconfirm WHERE product_id = pr.product_id");
			sql.append("     ),0) usedQuantity");
			sql.append("   FROM product pr");
			sql.append("   JOIN po_vnm_detail pod");
			sql.append("   ON pr.product_id = pod.product_id");
			sql.append("   JOIN po_vnm po");
			sql.append("   ON po.po_vnm_id   = pod.po_vnm_id");
			sql.append("   WHERE pr.status   = 1");
			sql.append("   AND pod.po_vnm_id = ?");
			params.add(poDVKHId);
			sql.append("   )");
			sql.append(" WHERE totalQuantity - usedQuantity > 0");
			sql.append(" ORDER BY productCode) podvkh on podvkh.productid = tmp.productid");
			
			
			countSql.append("select count(*) count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			
			
			final String[] fieldNames = new String[] { //
					"productId", "productCode", "productName",
					"checkLot", "convfact", "quantity", "price", 
					"amount", "lot", "quantityStock", "quantityStockTotal"
					};
	
			final Type[] fieldTypes = new Type[] { //
					StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
					StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL,
					StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER
			};
			if(kPaging == null){
				return repo.getListByQueryAndScalar(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
			}else{
				return repo.getListByQueryAndScalarPaginated(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
			}
		}
	}
	
	@Override
	public void createListPoVnmDetail(List<PoVnmDetail> listPoVnmDetail) throws DataAccessException {
		if (listPoVnmDetail == null) {
			throw new IllegalArgumentException("listPoVnmDetail");
		}
		repo.create(listPoVnmDetail);
	}
	
	@Override
	public void updateListPoVnmDetail(List<PoVnmDetail> listPoVnmDetail) throws DataAccessException {
		if (listPoVnmDetail == null) {
			throw new IllegalArgumentException("listPoVnmDetail");
		}
		repo.update(listPoVnmDetail);
	}

	@Override
	public List<RptProductPoVnmDetail2VO> getListRptProductPoVnmDetailVO(Long shopId) throws DataAccessException {
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT p.product_code      AS productCode,");
		sql.append("   p.product_name           AS productName,");
		sql.append("   pv.invoice_number        AS invoiceNumber,");
		sql.append("   pv.po_vnm_date           AS poVnmDate,");
		sql.append("   p.convfact               AS convfact,");
		sql.append("   pvd.price                AS price,");
		sql.append("   pvd.quantity             AS quantity,");
		sql.append("   pvd.quantity * pvd.price AS amount,");
		sql.append("   0                        AS discount,");
		sql.append("   0                        AS taxAmount,");
		sql.append("   pvd.quantity * pvd.price AS totalAmount");
		sql.append(" FROM po_vnm_detail pvd,");
		sql.append("   po_vnm pv,");
		sql.append("   po_auto pa,");
		sql.append("   product p");
		sql.append(" WHERE pvd.po_vnm_id   = pv.po_vnm_id");
		sql.append(" AND pv.po_auto_number = pa.po_auto_number");
		sql.append(" AND pa.status         = ?");
		params.add(ApprovalStatus.APPROVED.getValue());
		sql.append(" AND pv.type           = ?");
		params.add(PoType.PO_CONFIRM.getValue());
		sql.append(" AND pvd.product_id    = p.product_id");
		sql.append(" AND pv.shop_id        = ?");
		params.add(shopId);
		sql.append(" ORDER BY p.product_code,");
		sql.append("   pv.invoice_number");
		String[] fieldNames = new String[] {//
				"productCode",//1
				"productName",//2
				"invoiceNumber",//3
				"poVnmDate",//4
				"convfact",//5
				"price",//6
				"quantity",//7
				"amount",//8
				"discount",//9
				"taxAmount",//10
				"totalAmount",//11
		};
		Type[] fieldTypes = new Type[] {//
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.TIMESTAMP,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.BIG_DECIMAL,// 6
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.BIG_DECIMAL,// 8
				StandardBasicTypes.BIG_DECIMAL,// 9
				StandardBasicTypes.BIG_DECIMAL,// 10
				StandardBasicTypes.BIG_DECIMAL,// 11
		};
		return repo.getListByQueryAndScalar(RptProductPoVnmDetail2VO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RptPoCfrmFromDvkh2VO> getListRptPoCfrmFromDvkh2VO(Long shopId, Date fromDate,
			Date toDate) throws DataAccessException {
		
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptPoCfrmFromDvkh2VO>();
		
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		if (toDate == null) {
			throw new IllegalArgumentException("toDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT pv_dvkh.sale_order_number                      AS saleOrderNumber,");
		sql.append("   pv_dvkh.product_code                                AS dvkhProductCode,");
		sql.append("   NVL(pv_dvkh.quantity, 0)                            AS dvkhQty,");
		sql.append("   pv_dvkh.price                                       AS price,");
		sql.append("   NVL(pv_giao.quantity, 0)                            AS deliveryQty,");
		sql.append("   pv_dvkh.create_date                                 AS importDate,");
		sql.append("   pv_nhap.product_code                                AS importProductCode,");
		sql.append("   NVL(pv_nhap.quantity, 0)                            AS importQty,");
		sql.append("   NVL(pv_dvkh.quantity, 0) - NVL(pv_nhap.quantity, 0) AS waitQty");
		sql.append(" FROM");
		sql.append("   (SELECT pv.sale_order_number AS sale_order_number,");
		sql.append("     p.product_code             AS product_code,");
		sql.append("     SUM(pvd.quantity)          AS quantity,");
		sql.append("     pvd.price                  AS price,");
		sql.append("     pv.create_date");
		sql.append("   FROM po_vnm pv,");
		sql.append("     po_vnm_detail pvd,");
		sql.append("     product p");
		sql.append("   WHERE pv.type             = ? and pv.object_type = 1");
		sql.append("   AND pv.status      in( 0,1,2)");
		params.add(PoType.PO_CUSTOMER_SERVICE.getValue());
		sql.append("   AND pv.po_vnm_id          = pvd.po_vnm_id");
		sql.append("   AND pv.po_vnm_date >= TRUNC(?)");
		params.add(fromDate);
		sql.append("   AND pv.po_vnm_date < (TRUNC(?) + 1)");
		params.add(toDate);
		sql.append("   AND pv.shop_id            = ?");
		params.add(shopId);
		sql.append("   AND pvd.product_id        = p.product_id");
		sql.append("   GROUP BY pv.sale_order_number, p.product_code, pvd.price, pv.create_date");
		sql.append("   ORDER BY sale_order_number,");
		sql.append("     product_code");
		sql.append("   ) pv_dvkh");
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT pv.sale_order_number AS sale_order_number,");
		sql.append("     p.product_code             AS product_code,");
		sql.append("     SUM(pvd.quantity)          AS quantity");
		sql.append("   FROM po_vnm pv,");
		sql.append("     po_vnm_detail pvd,");
		sql.append("     product p");
		sql.append("   WHERE pv.type      = ? and pv.object_type = 1");
		params.add(PoType.PO_CONFIRM.getValue());
		sql.append("   AND pv.status      = ?");
		params.add(PoVNMStatus.IMPORTED.getValue());
		sql.append("   AND pv.po_vnm_id   = pvd.po_vnm_id");
		sql.append("   AND pvd.product_id = p.product_id");
		sql.append("   AND pv.shop_id     = ?");
		params.add(shopId);
		sql.append("   GROUP BY pv.sale_order_number,");
		sql.append("     p.product_code");
		sql.append("   ) pv_nhap");
		sql.append(" ON (pv_dvkh.sale_order_number = pv_nhap.sale_order_number and pv_dvkh.product_code = pv_nhap.product_code )");
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT pv.sale_order_number AS sale_order_number,");
		sql.append("     p.product_code             AS product_code,");
		sql.append("     SUM(pvd.quantity)          AS quantity");
		sql.append("   FROM po_vnm pv,");
		sql.append("     po_vnm_detail pvd,");
		sql.append("     product p");
		sql.append("   WHERE pv.type      = ? and pv.object_type = 1");
		params.add(PoType.PO_CONFIRM.getValue());
		sql.append("   AND pv.status      = ?");
		params.add(PoVNMStatus.NOT_IMPORT.getValue());
		sql.append("   AND pv.po_vnm_id   = pvd.po_vnm_id");
		sql.append("   AND pvd.product_id = p.product_id");
		sql.append("   AND pv.shop_id     = ?");
		params.add(shopId);
		sql.append("   GROUP BY pv.sale_order_number,");
		sql.append("     p.product_code");
		sql.append("   ) pv_giao");
		sql.append(" ON (pv_giao.sale_order_number = pv_nhap.sale_order_number and pv_giao.product_code = pv_nhap.product_code)");
		String[] fieldNames = new String[] {
				"saleOrderNumber", "dvkhProductCode", "dvkhQty", "price",
				"deliveryQty","importDate", "importProductCode", "importQty",
				"waitQty"
		};
		Type[] fieldTypes = new Type[] {//
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER,StandardBasicTypes.TIMESTAMP, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER
		};
		/*System.out.println(TestUtil.addParamToQuery(sql.toString(), params));*/
		return repo.getListByQueryAndScalar(RptPoCfrmFromDvkh2VO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.PoVnmDetailDAO#getListRptImportProduct(ths.dms.core.entities.enumtype.KPaging, java.lang.Long, java.util.Date, java.util.Date)
	 */
	@Override
	public List<RptInputProductVinamilkVO> getListRptImportProduct(Long shopId, Date fromDate,
			Date toDate) throws DataAccessException {
		
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptInputProductVinamilkVO>();
		
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		if (toDate == null) {
			throw new IllegalArgumentException("toDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT po.sale_order_number AS saleOrderNumber,");
		sql.append(" po.po_auto_number         AS poAutoNumber,");
		sql.append(" po.po_vnm_date            AS poVnmDate,");
		sql.append(" pa.po_auto_date           AS poAutoDate,");
		sql.append(" po.po_co_number           AS poCoNumber,");
		sql.append(" po.invoice_number         AS invoiceNumber,");
		sql.append(" po.import_date            AS importDate,");
		sql.append(" po.status                 AS status,");
		sql.append(" po.quantity               AS quantity,");
		sql.append(" pod.price                 AS priceValue,");
		sql.append(" p.product_code            AS productCode,");
		sql.append(" p.product_name            AS productName,");
		sql.append(" p.uom1                    AS uom1,");
		sql.append(" po.amount                 AS amount");
		sql.append(" FROM po_vnm po,");
		sql.append(" po_vnm_detail pod,");
		sql.append(" product p,");
		sql.append(" po_auto pa");
		sql.append(" WHERE po.po_vnm_id    = pod.po_vnm_id");
		sql.append(" AND pod.product_id    = p.product_id");
		sql.append(" AND pa.po_auto_number = po.po_auto_number");
		sql.append(" AND po.type           = ?");
		params.add(PoType.PO_CONFIRM.getValue());
		sql.append(" and po.shop_id = ?");
		params.add(shopId);
		if (fromDate != null) {
			sql.append(" and po.po_vnm_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and po.po_vnm_date < (trunc(?) + 1)");
			params.add(toDate);
		}
		sql.append(" order by p.product_code");
		final String[] fieldNames = new String[] { //
				"saleOrderNumber",// 1
				"poAutoNumber", // 2
				"poVnmDate", // 3
				"poAutoDate", // 3
				"poCoNumber",//4
				"invoiceNumber", // 5
				"importDate", // 6
				"status", // 7
				"quantity", // 8
				"priceValue", // 9
				"productCode", // 10
				"productName", // 11
				"uom1", // 12
				"amount", //13
				};

		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.TIMESTAMP,// 3
				StandardBasicTypes.TIMESTAMP,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.TIMESTAMP,// 6
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.INTEGER,// 8
				StandardBasicTypes.FLOAT,// 9
				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.STRING,// 11
				StandardBasicTypes.STRING,// 12
				StandardBasicTypes.BIG_DECIMAL,// 13
		};
		return repo.getListByQueryAndScalar(RptInputProductVinamilkVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.PoVnmDetailDAO#getPoVnmDetail(java.lang.Long, java.lang.Long)
	 */
	@Override
	public PoVnmDetail getPoVnmDetail(Long poVnmId, Long productId, Integer productType) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM_DETAIL where 1 = 1");
		if (poVnmId != null) {
			sql.append(" and PO_VNM_ID = ?");
			params.add(poVnmId);
		}
		if (productId != null) {
			sql.append(" and PRODUCT_ID = ?");
			params.add(productId);
		}
		if (productType != null) {
			sql.append(" and PRODUCT_TYPE = ?");
			params.add(productType);
		}
		sql.append(" order by po_vnm_detail_id desc");
		return repo.getEntityBySQL(PoVnmDetail.class, sql.toString(), params);
	}
	
	@Override
	public List<PoVnmDetail> getListPoVnmDetailByPoVnmId(Long poVnmId)
			throws DataAccessException {
		if (poVnmId == null) {
			throw new IllegalArgumentException("poVnmId is null");
		}
		String sql = "select * from po_vnm_detail where po_vnm_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(poVnmId);
		return repo.getListBySQL(PoVnmDetail.class, sql, params);
	}
	
	@Override
	public List<Rpt_PO_CTTTVO> getPOCTTT(Long shopId, int typePerson, String codePerson, String fromDate, String toDate)
	 throws DataAccessException
	{

		List<Rpt_PO_CTTT_MAPPINGVO> lstData = null;
		List<Rpt_PO_CTTTVO> lstGroup = new ArrayList<Rpt_PO_CTTTVO>();
		
		List<Object> params = new ArrayList<Object>();
		
		params.add("SHOPID");
		params.add(shopId);
		params.add(StandardBasicTypes.LONG);

		params.add("TYPEPERSON");
		params.add(typePerson);
		params.add(StandardBasicTypes.INTEGER);

		params.add("CODEPERSON");
		params.add(codePerson);
		params.add(StandardBasicTypes.STRING);

		params.add("FROMDATE");
		params.add(fromDate);
		params.add(StandardBasicTypes.STRING);
		
		params.add("TODATE");
		params.add(toDate);
		params.add(StandardBasicTypes.STRING);
		
		try
		{
			lstData =  repo.getListByNamedQuery(Rpt_PO_CTTT_MAPPINGVO.class, "PKG_SHOP_REPORT.BAOCAO_PO_TTCKH", params);
			//group theo nhan vien ban hang
			int iSize = lstData.size();
			if(iSize > 0)
			{
				Map<String, List<Object>> map = new HashMap<String, List<Object>>();
				if(typePerson == 0){
					map = GroupUtility.collectionToHash(lstData, "ma_kh");
				}else{if(typePerson == 1){
					map = GroupUtility.collectionToHash(lstData, "nvtt");
				}}
				Iterator iter = map.entrySet().iterator();
				while (iter.hasNext()) {
					Map.Entry mEntry = (Map.Entry) iter.next();
					Rpt_PO_CTTTVO dataConverting = new Rpt_PO_CTTTVO((List<Rpt_PO_CTTT_MAPPINGVO>)mEntry.getValue(), typePerson);
					lstGroup.add(dataConverting);
				}
			}
			
		}
		catch(Exception ex)
		{
			throw new DataAccessException(ex);
		}
		
		return lstGroup;
	}
}

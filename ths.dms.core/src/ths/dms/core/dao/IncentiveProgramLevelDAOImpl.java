package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.IncentiveProgramLevel;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.IncentiveProgramLevelFilter;
import ths.dms.core.entities.enumtype.IncentiveProgramLevelVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class IncentiveProgramLevelDAOImpl implements IncentiveProgramLevelDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public IncentiveProgramLevel createIncentiveProgramLevel(IncentiveProgramLevel incentiveProgramLevel, LogInfoVO logInfo) throws DataAccessException {
		if (incentiveProgramLevel == null) {
			throw new IllegalArgumentException("incentiveProgramLevel");
		}
		repo.create(incentiveProgramLevel);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, incentiveProgramLevel, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(IncentiveProgramLevel.class, incentiveProgramLevel.getId());
	}

	@Override
	public void deleteIncentiveProgramLevel(IncentiveProgramLevel incentiveProgramLevel, LogInfoVO logInfo) throws DataAccessException {
		if (incentiveProgramLevel == null) {
			throw new IllegalArgumentException("incentiveProgramLevel");
		}
		repo.delete(incentiveProgramLevel);
		try {
			actionGeneralLogDAO.createActionGeneralLog(incentiveProgramLevel, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public IncentiveProgramLevel getIncentiveProgramLevelById(long id) throws DataAccessException {
		return repo.getEntityById(IncentiveProgramLevel.class, id);
	}
	
	@Override
	public void updateIncentiveProgramLevel(IncentiveProgramLevel incentiveProgramLevel, LogInfoVO logInfo) throws DataAccessException {
		if (incentiveProgramLevel == null) {
			throw new IllegalArgumentException("incentiveProgramLevel");
		}
		IncentiveProgramLevel temp = this.getIncentiveProgramLevelById(incentiveProgramLevel.getId());
		temp = temp.clone();
		incentiveProgramLevel.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			incentiveProgramLevel.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(incentiveProgramLevel);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, incentiveProgramLevel, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public List<IncentiveProgramLevel> getListIncentiveProgramLevel(
			Long incentiveProgramId, ActiveType status,
			KPaging<IncentiveProgramLevel> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from incentive_program_level where 1 = 1");
		
		if (status != null) {
			sql.append(" and status = ?");
			params.add(status.getValue());
		}else {
			sql.append(" and status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		if (incentiveProgramId != null) {
			sql.append(" and incentive_program_id = ?");
			params.add(incentiveProgramId);
		}
		
		sql.append(" order by incentive_program_level_id desc");
		ObjectVO<IncentiveProgramLevel> res = new ObjectVO<IncentiveProgramLevel>();
		if (kPaging == null){
			List<IncentiveProgramLevel> lst = repo.getListBySQL(IncentiveProgramLevel.class, sql.toString(), params);
			return lst;
		}else{
			List<IncentiveProgramLevel> lst = repo.getListBySQLPaginated(IncentiveProgramLevel.class, sql.toString(), params, kPaging);
			return lst;
		}
	}

	@Override
	public List<IncentiveProgramLevelVO> getListIncentiveProgramLevelVO(IncentiveProgramLevelFilter filter,
			KPaging<IncentiveProgramLevelVO> kPaging)
			throws DataAccessException {
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();

		selectSql.append("SELECT lv.incentive_program_level_id id, ");
		selectSql.append("  pr.incentive_program_id programId, ");
		selectSql.append("  pr.incentive_program_code programCode, ");
		selectSql.append("  pr.incentive_program_name programName, ");
		selectSql.append("  lv.level_code levelCode, ");
		selectSql.append("  lv.amount amount, ");
		selectSql.append("  lv.free_amount freeAmount, ");
		selectSql.append("  lv.free_product_id freeProductId, ");
		selectSql.append("  (select p.product_name from product p where p.product_id = lv.free_product_id) freeProductName, ");
		selectSql.append("  (select p.product_code from product p where p.product_id = lv.free_product_id) freeProductCode, ");
		selectSql.append("  lv.free_quantity freeQuantity, ");
		selectSql.append("  lv.status status ");
		
		fromSql.append("FROM incentive_program_level lv ");
		fromSql.append("JOIN incentive_program pr ");
		fromSql.append("ON lv.incentive_program_id = pr.incentive_program_id ");
		fromSql.append("WHERE 1                    = 1 ");

		List<Object> params = new ArrayList<Object>();
		if (filter.getProgramId() != null) {
			fromSql.append(" and pr.incentive_program_id = ? ");
			params.add(filter.getProgramId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getLevelCode())) {
			fromSql.append(" and lower(lv.level_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getLevelCode().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getLevelCodeEqual())) {
			fromSql.append(" and lower(lv.level_code) = ? ");
			params.add(filter.getLevelCodeEqual().toLowerCase());
		}
		if(filter.getAmount() != null){
			fromSql.append(" and lv.amount=?");
			params.add(filter.getAmount());
		}
		if(filter.getFreeAmount() != null){
			fromSql.append(" and lv.free_amount=?");
			params.add(filter.getFreeAmount());
		}
		if(filter.getFreeProductId() != null){
			fromSql.append(" and lv.free_product_id=?");
			params.add(filter.getFreeProductId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getFreeProductCode())) {
			fromSql.append(" and exists(select 1 from product p where p.product_id = lv.free_product_id and lower(p.product_code) like ? ESCAPE '/')");
			params.add(StringUtility.toOracleSearchLike(filter.getFreeProductCode().toLowerCase()));
		}
		if (filter.getFreeQuantity() != null) {
			fromSql.append(" and lv.free_quantity=?");
			params.add(filter.getFreeQuantity());
		}
		if (filter.getStatus() != null) {
			fromSql.append(" and lv.status=?");
			params.add(filter.getStatus().getValue());
		}else {
			fromSql.append(" and lv.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		selectSql.append(fromSql.toString());
		selectSql.append(" order by lv.CREATE_DATE, pr.INCENTIVE_PROGRAM_ID");
		
		countSql.append("select count(lv.incentive_program_id) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"id", "programId", "programCode", "programName", 
				"levelCode", "amount", "freeAmount","freeProductId", 
				"freeProductName", "freeProductCode", "freeQuantity", "status"
				};

		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER
		};	
		ObjectVO<IncentiveProgramLevelVO> res = new ObjectVO<IncentiveProgramLevelVO>();
		if (kPaging == null){
			List<IncentiveProgramLevelVO> lst = repo.getListByQueryAndScalar(
					IncentiveProgramLevelVO.class, fieldNames,
					fieldTypes, selectSql.toString(), params);
			return lst;
		}else{
			List<IncentiveProgramLevelVO> lst = repo.getListByQueryAndScalarPaginated(IncentiveProgramLevelVO.class, fieldNames, fieldTypes,
					selectSql.toString(), countSql.toString(), params, params, kPaging);
			return lst;
		}
	}

	@Override
	public IncentiveProgramLevel getIncentiveProgramLevelByIncentiveProCode(
			Long incentiveProgramId, String incentiveProCode, String levelCode, ActiveType activeType)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select dpl.* from incentive_program dp, incentive_program_level dpl where dp.incentive_program_id = dpl.incentive_program_id ");

		if(incentiveProgramId != null){
			sql.append(" and dp.incentive_program_id = ? ");
			params.add(incentiveProgramId);
		}
		if (!StringUtility.isNullOrEmpty(incentiveProCode)) {
			sql.append(" and lower(dp.incentive_program_code) = ? ");
			params.add(incentiveProCode.toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(levelCode)) {
			sql.append(" and lower(dpl.level_code) = ? ");
			params.add(levelCode.toLowerCase());
		}
		if (null != activeType) {
			sql.append(" and dpl.status = ? ");
			params.add(activeType.getValue());
		} else {
			sql.append(" and dpl.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}

		return repo.getEntityBySQL(IncentiveProgramLevel.class, sql.toString(),
				params);
	}
}

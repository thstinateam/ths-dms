package ths.dms.core.dao;

/**
 * Import thu vien ho tro
 * */
import java.util.List;

import ths.dms.core.entities.EquipSuggestEviction;
import ths.dms.core.entities.EquipSuggestEvictionDTL;
import ths.dms.core.entities.filter.EquipSuggestEvictionFilter;
import ths.dms.core.entities.vo.EquipSuggestEvictionDetailVO;
import ths.dms.core.entities.vo.EquipmentSuggestEvictionVO;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Danh cho tat ca cac lien quan den Bien ban thu hoi thiet bi DAO
 * 
 * @author nhutnn
 * @since 14/07/2015
 * */
public interface EquipSuggestEvictionDAO {
	/**
	 * Lay danh sach bien ban de nghi thu hoi
	 * 
	 * @author nhutnn
	 * @since 14/07/2015
	 * */
	List<EquipmentSuggestEvictionVO> searchListEquipSuggestEvictionVOByFilter(EquipSuggestEvictionFilter<EquipmentSuggestEvictionVO> filter) throws DataAccessException;
	
	/**
	 * Lay equip lend bang id
	 * @author nhutnn
	 * @since 03/07/2015
	 * @param code
	 * @return
	 * @throws DataAccessException
	 */
	EquipSuggestEviction getEquipSuggestEvictionByCode(String code) throws DataAccessException;		
		
	/**
	 * Lay danh sach chi tiet bien ban VO
	 * 
	 * @author nhutnn
	 * @since 20/07/2015
	 * */
	List<EquipSuggestEvictionDetailVO> getListEquipSuggestEvictionDetailVOByFilter(EquipSuggestEvictionFilter<EquipSuggestEvictionDetailVO> filter)throws DataAccessException;
	
	/**
	 * Lay ma bien ban
	 * @author nhutnn
	 * @since 06/07/2015
	 * @return
	 * @throws DataAccessException
	 */
	String getCodeEquipSuggestEviction() throws DataAccessException;
	
	/**
	 * Lay danh sach chi tiet bien ban Entity
	 * 
	 * @author nhutnn
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @since 08/07/2015
	 */
	List<EquipSuggestEvictionDTL> getListEquipSuggestEvictionDetailByFilter(EquipSuggestEvictionFilter<EquipSuggestEvictionDTL> filter)throws DataAccessException;

	/**
	 * Lay danh sach bien ban
	 * 
	 * @author nhutnn
	 * @since 15/07/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipSuggestEviction> getListEquipSuggestEvictionByFilter(EquipSuggestEvictionFilter<EquipSuggestEviction> filter) throws DataAccessException;
}

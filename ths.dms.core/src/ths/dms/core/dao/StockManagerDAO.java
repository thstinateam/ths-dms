package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.StockGeneralFilter;
import ths.dms.core.entities.vo.StockGeneralVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.WarehouseVO;
import ths.dms.core.exceptions.DataAccessException;

public interface StockManagerDAO {
	Warehouse createWarehouse(Warehouse warehouse) throws DataAccessException;
	
	/**
	 * tao kho
	 * @author tuannd20
	 * @param warehouse
	 * @param sysdate
	 * @return
	 * @throws DataAccessException
	 * @since 13/01/2016
	 */
	Warehouse createWarehouse(Warehouse warehouse, Date sysdate) throws DataAccessException;
	
	void updateWarehouse(Warehouse warehouse) throws DataAccessException;
	List<WarehouseVO> getListWarehouseVOByFilter(BasicFilter<WarehouseVO> filter) throws DataAccessException;
	Boolean checkIfRecordWarehouseExist(BasicFilter<WarehouseVO> filter) throws DataAccessException;
	Warehouse getWarehouseById(long id) throws DataAccessException;
	List<Warehouse> getListWarehouseByFilter(BasicFilter<Warehouse> filter) throws DataAccessException;
	Boolean checkIfRecordWarehouseInStockTotalExist(BasicFilter<WarehouseVO> filter) throws DataAccessException;
	/** 
	 * Ham load tat ca kho cua sp
	 * @author nhanlt6
	 * @date 29/08/2014
	 * @param productId
	 * */
	List<StockTotalVO> getListStockWarehouseByProductId(BasicFilter<StockTotalVO> filter) throws DataAccessException;
	
	/** 
	 * Ham load tat ca kho cua sp theo list productId
	 * @author tungmt
	 * @date 16/12/2014
	 * @param 
	 * */
	List<StockTotalVO> getListStockWarehouseByListProduct(BasicFilter<StockTotalVO> filter) throws DataAccessException;
	
	/**
	 * Lay ds sp + kho theo NVBH
	 * 
	 * @author lacnv1
	 * @since Sep 23, 2014
	 */
	List<StockTotalVO> getListStockTotalVOBySaler(BasicFilter<StockTotalVO> filter) throws DataAccessException;
	/**
	 * @author hunglm16
	 * @since November 8,2014
	 * @description search update information output warehouse
	 */
	List<StockGeneralVO> searchUpdInformationOutputWarehouse(StockGeneralFilter<StockGeneralVO> filter) throws DataAccessException;
	/**
	 * Tim kiem san pham co trong kho ung voi Don vi Xac dinh
	 * 
	 * @author hunglm16
	 * @since October 31,2014
	 * */
	List<StockGeneralVO> searchProductInWareHouse(StockGeneralFilter<StockGeneralVO> filter) throws DataAccessException;
	/**
	 * Tim kiem kho ung voi Don vi Xac dinh
	 * 
	 * @author hunglm16
	 * @since December 16,2014
	 * */
	List<Warehouse> searchWarehouseByShop(StockGeneralFilter<Warehouse> filter) throws DataAccessException;
	
	/**
	 * Tim kiem danh sach stocktransdetail theo filter
	 * 
	 * @author tungmt
	 * @since 30/01/2015
	 * */
	List<StockTotalVO> getListStockTransDetail(BasicFilter<StockTotalVO> filter) throws DataAccessException;
	List<StockTotalVO> getListStockTransDetailForDC(BasicFilter<StockTotalVO> filter) throws DataAccessException;
}

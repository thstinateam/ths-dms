package ths.dms.core.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.AttributeValue;
import ths.dms.core.exceptions.DataAccessException;

public interface AttributeValueDAO {

	AttributeValue getValueOfObject(Long objId, Long attributeId)
			throws DataAccessException;

	AttributeValue createAttributeValue(AttributeValue attributeValue)
			throws DataAccessException;

	void updateAttributeValue(AttributeValue attributeValue)
			throws DataAccessException;

	
}

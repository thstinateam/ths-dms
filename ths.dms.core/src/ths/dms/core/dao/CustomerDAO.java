package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ParamsDAOGetListCustomer;
import ths.dms.core.entities.filter.CustomerFilter;
import ths.dms.core.entities.vo.Customer4SaleVO;
import ths.dms.core.entities.vo.CustomerExportVO;
import ths.dms.core.entities.vo.CustomerGTVO;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.DuplicatedCustomerVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.core.entities.vo.rpt.RptCustomerByProductCatDataVO;
import ths.core.entities.vo.rpt.RptCustomerByRoutingDataVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface CustomerDAO {

	Customer createCustomer(Customer customer, LogInfoVO logInfo) throws DataAccessException, BusinessException;

	void deleteCustomer(Customer customer, LogInfoVO logInfo) throws DataAccessException;

	void updateCustomer(Customer customer, LogInfoVO logInfo) throws DataAccessException;
	
	Customer getCustomerById(long id) throws DataAccessException;

	Customer getCustomerByCode(String code) throws DataAccessException;
	Customer getCustomerByShortCode(String code) throws DataAccessException;
	Customer getCustomerByCodeAndShop(String shortCode, Long shopId) throws DataAccessException;

	Boolean isUsingByOthers(long customerId) throws DataAccessException;

	Customer getCustomerForUpdate(Long id) throws DataAccessException;

	Boolean checkIfCustomerExists(String email, String mobiphone,String phone,
			String numberIdentity, Long id, String fax) throws DataAccessException;
	
	/**
	 * Lay du lieu cho Bao cao Khach hang theo nhom san pham
	 * 
	 * @param shopId
	 * @param cusIds
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<RptCustomerByProductCatDataVO> getDataForCusByProCatReport(
			Long shopId, List<Long> cusIds, Date fromDate, Date toDate)
			throws DataAccessException;

	CustomerVO getCustomerVO(Long customerId) throws DataAccessException;


	/**
	 * lay danh sach khach hang duoc quan ly boi shopCode tuong ung (khong lay de qui xuong duoi)
	 * @author thanhnguyen
	 * @param kPaging
	 * @param shortCode
	 * @param customerName
	 * @param shopCode
	 * @return
	 * @throws DataAccessException
	 */
	List<Customer> getListCustomerForOnlyShop(KPaging<Customer> kPaging, String shortCode, String customerName, String shopCode
			,List<Long> lstExceptionCus) throws DataAccessException;

	String test() throws DataAccessException;

	/**
	 * @author hungnm
	 * @param parentShopId
	 * @param promotionShopMapId
	 * @return
	 * @throws DataAccessException
	 */
	List<Customer> getListCustomerNotInPromotionProgram(
			KPaging<Customer> kPaging, Long parentShopId,
			Long promotionShopMapId, String shortCode, String customerName, String address) throws DataAccessException;
	/**
	 * @author hungnm
	 * @param parentShopId
	 * @param displayStaffMapId
	 * @param customerCode
	 * @param customerName
	 * @return
	 * @throws DataAccessException
	 */
	List<Customer> getListCustomerNotInDisplayProgram(Long parentShopId,
			Long displayStaffMapId, String customerCode, String customerName,
			Integer limit) throws DataAccessException;

	String generateCodeFromId(Integer id) throws BusinessException;

	List<Customer> getListCustomer(ParamsDAOGetListCustomer funcParams)
			throws DataAccessException;

	List<Customer> getListCustomerByShopCode(KPaging<Customer> kPaging,
			String shopCode) throws DataAccessException;

	List<Customer> getListCustomer(Long parentShopId, String customerCode,
			String customerName, Integer limit) throws DataAccessException;

	List<Customer> getListCustomerEx(Long parentShopId, String customerCode,
			String customerName, Integer limit) throws DataAccessException;

	List<Customer> getListCustomerByShopId(Long shopId,
			ParamsDAOGetListCustomer funcParams) throws DataAccessException;
	
	List<CustomerVO> getListCustomerByShopFilter(CustomerFilter<CustomerVO> filter) throws DataAccessException;

	void updateDb() throws DataAccessException;

	List<RptCustomerByRoutingDataVO> getRptCustomerByRouting(Long shopId,
			List<Long> listStaffId, List<Long> listCustomerId,
			String customerName) throws DataAccessException;
	List<CustomerVO> getListCustomerVO(Long shopId, String staffCode,
			String customerCode, String customerName, boolean isGroupByStaff,Integer status,
			Long parentStaffId, boolean checkMap) throws DataAccessException;

	List<Customer> getListCustomerByStaffRouting(KPaging<Customer> kPaging,
			Long shopId, String customerCode,
			String customerName, Long staffId, 
			ActiveType status) throws DataAccessException;

	List<CustomerGTVO> getListCustomerGTVOByGroupTransfer(
			KPaging<CustomerGTVO> kPaging, Long shopId,
			String groupTransferCode, String groupTransferName,
			String staffTransferCode, String shortCode, ActiveType status,
			String shopCode, Boolean check, String customerName)
			throws DataAccessException;

	List<Customer> searchCustomerVOForDSKH(KPaging<Customer> kPaging,
			String shopCode, String staffCode, String customerCode,
			String customerName) throws DataAccessException;

	CustomerVO getCustomerVisitting(Long staffId) throws DataAccessException;

	List<CustomerVO> getListCustomerBySaleStaffHasVisitPlan(Long staffId, Date checkDate) throws DataAccessException;

	Customer getCustomerMultiChoine(Long customerId, Long shopId, String shortCode, Integer status) throws DataAccessException;

	List<Customer4SaleVO> getListCustomer4CreateOrder(Long shopId)
			throws DataAccessException;

	List<Customer> getListCustomerByListID(List<Long> listId)throws DataAccessException;
	
	List<Customer> getListCustomerByShortCode(Long shopId, String shortCode) throws DataAccessException;

	/**
	 * @author sangtn
	 * @since 08-05-2014
	 * @description Get customer by full code and status 
	 * @note Extend from getCustomerByCode
	 */
	Customer getCustomerByCodeEx(String code, ActiveType status)
			throws DataAccessException;
	
	/**
	 * Cap nhat channel_type cua KH ve null
	 * 
	 * @author lacnv1
	 * @since Jun 16, 2014
	 */
	void removeTypeOfCustomer(long typeId) throws DataAccessException;

	/**
	 * Tim kiem khach hang theo phan quyen CMS 
	 * 
	 * @author hunglm16
	 * @description filter.getLstShopId() khong duoc de trong
	 * */
	List<CustomerVO> searchListCustomerVOByFilter(CustomerFilter<CustomerVO> filter) throws DataAccessException;
	/**
	 * Tim kiem khach hang theo phan quyen CMS ket hop Tuyen ban hang
	 * 
	 * @author hunglm16
	 * @description filter.getLstShopId() va filter.getStaffRootId khong duoc de trong
	 * */
	List<CustomerVO> searchListCustomerVOForRoutingByFilter(CustomerFilter<CustomerVO> filter) throws DataAccessException;

	boolean existsCode(Long cusID, Long shopId, String code) throws DataAccessException;

	/**
	 * Lay danh sach khach hang trung dia chi voi khach hang customerId
	 * @author tuannd20
	 * @param customerId id cua khach hang dang kiem tra
	 * @return danh sach khach hang trung dia chi voi khach hang customerId
	 * @throws DataAccessException
	 * @since 28/08/2015
	 */
	List<DuplicatedCustomerVO> getDuplicatedCustomers(Long customerId) throws DataAccessException;
	
	/**
	 * Tim kiem khach hang voi thong tin thuoc tinh dong
	 * @author tuannd20
	 * @param param tham so tim kiem
	 * @return danh sach khach hang kem theo thong tin thuoc tinh dong
	 * @throws BusinessException
	 * @since 04/09/2015
	 */
	List<CustomerExportVO> getCustomerWithDynamicAttributeInfo(ParamsDAOGetListCustomer param) throws DataAccessException;
	
	/**
	 * (M) add shop_id
	 * @author tuannd20
	 * @param shopId
	 * @param staffId
	 * @return
	 * @throws BusinessException
	 * @date 05/07/2014
	 */
	List<CustomerVO> getListCustomerBySaleStaffHasVisitPlanWithFirstSaleOrder(Long shopId, Long staffId, Integer cfIsCustomerWaitingToApproved, Date checkDate) throws DataAccessException;
	
	/**
	 * Lay thong tin tuyen, nguoi tao khach hang
	 * @author tuannd20
	 * @param customerCodes danh sach khach hang lay thong tin
	 * @return danh sach khach hang voi thong tin tuyen tham gia & nguoi tao khach hang
	 * @throws DataAccessException
	 * @since 12/10/2015
	 */
	List<CustomerVO> getCustomerRoutingsInfo(List<String> customerCodes) throws DataAccessException;

	/**
	 * Lay danh sach customer popup feedback
	 * @author vuongmq
	 * @param filter
	 * @return List<CustomerVO> 
	 * @throws DataAccessException
	 * @since 18/11/2015
	 */
	List<CustomerVO> getCustomerVOFeedback(CustomerFilter<CustomerVO> filter) throws DataAccessException;

	/**
	 * Lay danh sach customer popup feedback theo routing cua staff
	 * @author vuongmq
	 * @param filter
	 * @return List<CustomerVO>
	 * @throws DataAccessException
	 * @since 18/11/2015
	 */
	List<CustomerVO> getCustomerVORoutingFeedback(CustomerFilter<CustomerVO> filter) throws DataAccessException;
	
	/**
	 * Lay danh sach KH bang filter
	 * @author nhutnn
	 * @since 22/07/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<CustomerVO> getListCustomerFullVOByFilter(CustomerFilter<CustomerVO> filter) throws DataAccessException;
	
}

package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoObjectType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.vo.PoCSVO;
import ths.dms.core.entities.vo.PoConfirmVO;
import ths.dms.core.entities.vo.PoSecVO;
import ths.dms.core.entities.vo.PoVnmStockInVO;
import ths.core.entities.vo.rpt.RptPoAutoDataVO;
import ths.core.entities.vo.rpt.RptPoStatusTrackingVO;
import ths.core.entities.vo.rpt.RptPoVnmDebitComparison2VO;
import ths.core.entities.vo.rpt.RptPoVnmVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class PoVnmDAOImpl implements PoVnmDAO {

	@Autowired
	private IRepository repo;

	@Override
	public PoVnm createPoVnm(PoVnm poVnm) throws DataAccessException {
		if (poVnm == null) {
			throw new IllegalArgumentException("poVnm");
		}
		repo.create(poVnm);
		return repo.getEntityById(PoVnm.class, poVnm.getId());
	}

	@Override
	public void deletePoVnm(PoVnm poVnm) throws DataAccessException {
		if (poVnm == null) {
			throw new IllegalArgumentException("poVnm");
		}
		repo.delete(poVnm);
	}

	@Override
	public void updatePoVnm(PoVnm poVnm) throws DataAccessException {
		if (poVnm == null) {
			throw new IllegalArgumentException("poVnm");
		}
		repo.update(poVnm);
	}

	@Override
	public List<PoVnm> getListPoVnm(PoVnmFilter filter, KPaging<PoVnm> kPaging) throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate())) {
			return new ArrayList<PoVnm>();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM p join shop s on p.shop_id = s.shop_id ");
		sql.append(" where 1 = 1");
		//TUNGTT khong cho nhap poCofrim cua PO DVKH dang bi treo
		//sql.append(" and (select count(*) from PO_VNM where type = 1 and status = 4 and sale_order_number = p.sale_order_number) = 0");
		//
		if (filter.getObjectType() != null) {
			sql.append(" and p.object_type = ?");
			params.add(filter.getObjectType().getValue());
		} else {
			sql.append(" and p.object_type = 1");
		}
		if (null != filter.getPoType()) {
			sql.append(" and p.TYPE = ?");
			params.add(filter.getPoType().getValue());
		}
		if (null != filter.getLstPoVnmType() && filter.getLstPoVnmType().size() > 0) {
			sql.append(" and p.TYPE in (?");
			params.add(filter.getLstPoVnmType().get(0));
			for (int i = 1, sz = filter.getLstPoVnmType().size(); i < sz; i++) {
				sql.append(",?");
				params.add(filter.getLstPoVnmType().get(i));
			}
			sql.append(")");
		}
		if (filter.getShopId() != null) {
			if (filter.getHasFindChildShop()) {
				sql.append(" and p.SHOP_ID IN (SELECT SHOP_ID FROM SHOP START WITH SHOP_ID = ? CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID)");	
			} else {
				sql.append(" and p.SHOP_ID = ?");
			}
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getPoAutoNumber())) {
			if (filter.getIsLikePoNumber() != null && filter.getIsLikePoNumber()) {
				sql.append(" and p.PO_CO_NUMBER like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getPoAutoNumber().toUpperCase()));
			} else {
				sql.append(" and p.PO_CO_NUMBER = ? ");
				params.add(filter.getPoAutoNumber().toUpperCase());
			}
		}
		//NHAP HANG
		if (filter.getFromDate() != null) {
			sql.append(" and p.po_vnm_date >= trunc(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and p.po_vnm_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		//TRA HANG
		if (filter.getImportFromDate() != null) {
			sql.append(" and p.import_date >= trunc(?)");
			params.add(filter.getImportFromDate());
		}
		if (filter.getImportToDate() != null) {
			sql.append(" and p.import_date < trunc(?) + 1");
			params.add(filter.getImportToDate());
		}
		if (filter.getPoStatus() != null) {
			if (filter.getPoStatus().getValue() == 0) { // chua nhap
				sql.append(" and p.STATUS = 0 AND p.po_vnm_date >= trunc(?) and p.po_vnm_date < trunc(?) + 1 ");
				params.add(filter.getfDate());
				params.add(filter.gettDate());
			} else if (filter.getPoStatus().getValue() == 2) { //da nhap
				sql.append(" and p.STATUS = 2 AND p.import_date >= trunc(?) and p.import_date < trunc(?) + 1 ");
				params.add(filter.getfDate());
				params.add(filter.gettDate());
			} else if (filter.getPoStatus().getValue() == 3) {
				sql.append(" and p.STATUS = 3 AND p.cancel_date >= trunc(?) and p.cancel_date < trunc(?) + 1 ");
				params.add(filter.getfDate());
				params.add(filter.gettDate());
			}
		} else {
			sql.append(" and( (p.STATUS = 0 AND p.po_vnm_date >= trunc(?) and p.po_vnm_date < trunc(?) + 1 ) ");
			params.add(filter.getfDate());
			params.add(filter.gettDate());
			sql.append(" or (p.STATUS = 2 AND p.import_date >= trunc(?) and p.import_date < trunc(?) + 1 ) ");
			params.add(filter.getfDate());
			params.add(filter.gettDate());
			sql.append(" or (p.STATUS = 3 AND p.cancel_date >= trunc(?) and p.cancel_date < trunc(?) + 1) ) ");
			params.add(filter.getfDate());
			params.add(filter.gettDate());
		}
		if (!StringUtility.isNullOrEmpty(filter.getInvoiceNumber())) {
			sql.append(" and p.INVOICE_NUMBER like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getInvoiceNumber().toUpperCase()));
		}
		sql.append(" order by SHOP_CODE, INVOICE_NUMBER desc");
		if (kPaging == null) {
			return repo.getListBySQL(PoVnm.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(PoVnm.class, sql.toString(), params, kPaging);
	}
	
	
	/**
	 * @author vuongmq
	 * @date 07/08/2015
	 * @description lay danh sach po confirm theo filter
	 * Dung cho Lay danh sach Po confirm dieu chinh hoa don
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	@Override
	public List<PoVnm> getListPoConfirmByFilter(PoVnmFilter filter) throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate())) {
			return new ArrayList<PoVnm>();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM p join shop s on p.shop_id = s.shop_id ");
		sql.append(" where 1 = 1");
		//TUNGTT khong cho nhap poCofrim cua PO DVKH dang bi treo
		//sql.append(" and (select count(*) from PO_VNM where type = 1 and status = 4 and sale_order_number = p.sale_order_number) = 0");
		//
		if (filter.getObjectType() != null) {
			sql.append(" and p.object_type = ?");
			params.add(filter.getObjectType().getValue());
		} else {
			sql.append(" and p.object_type = 1");
		}
		if (null != filter.getPoType()) {
			sql.append(" and p.TYPE = ?");
			params.add(filter.getPoType().getValue());
		}
		if (null != filter.getLstPoVnmType() && filter.getLstPoVnmType().size() > 0) {
			sql.append(" and p.TYPE in (?");
			params.add(filter.getLstPoVnmType().get(0));
			for(int i = 1, sz = filter.getLstPoVnmType().size(); i < sz; i++){
				sql.append(",?");
				params.add(filter.getLstPoVnmType().get(i));
			}
			sql.append(")");
		}
		if (filter.getShopId() != null) {
			if (filter.getHasFindChildShop()) {
				sql.append(" and p.SHOP_ID IN (SELECT SHOP_ID FROM SHOP START WITH SHOP_ID = ? CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID)");	
			} else {
				sql.append(" and p.SHOP_ID = ?");
			}
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getPoAutoNumber())) {
			if (filter.getIsLikePoNumber() != null && filter.getIsLikePoNumber()) {
				sql.append(" and upper(p.PO_CO_NUMBER) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getPoAutoNumber().toUpperCase()));
			} else {
				sql.append(" and upper(p.PO_CO_NUMBER) = ? ");
				params.add(filter.getPoAutoNumber().toUpperCase());
			}
		}
		//NHAP HANG
		if (filter.getFromDate() != null) {
			sql.append(" and p.po_vnm_date >= trunc(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and p.po_vnm_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		//TRA HANG
		if (filter.getImportFromDate() != null) {
			sql.append(" and p.import_date >= trunc(?)");
			params.add(filter.getImportFromDate());
		}
		if (filter.getImportToDate() != null) {
			sql.append(" and p.import_date < trunc(?) + 1");
			params.add(filter.getImportToDate());
		}
		if (filter.getPoStatus() != null) {
			sql.append(" and p.STATUS = ? ");
			params.add(filter.getPoStatus().getValue());
		}
		if (null != filter.getLstPoVnmStatus() && filter.getLstPoVnmStatus().size() > 0) {
			sql.append(" and p.STATUS in (?");
			params.add(filter.getLstPoVnmStatus().get(0));
			for (int i = 1, sz = filter.getLstPoVnmStatus().size(); i < sz; i++) {
				sql.append(",?");
				params.add(filter.getLstPoVnmStatus().get(i));
			}
			sql.append(")");
		}
		if (!StringUtility.isNullOrEmpty(filter.getInvoiceNumber())) {
			sql.append(" and upper(p.INVOICE_NUMBER) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getInvoiceNumber().toUpperCase()));
		}
		sql.append(" order by SHOP_CODE, INVOICE_NUMBER desc");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(PoVnm.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(PoVnm.class, sql.toString(), params, filter.getkPaging());
	}
	
	@Override
	public List<PoVnmStockInVO> getListPoConfirmPoVnmByFilter(PoVnmFilter filter) throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate())) {
			return new ArrayList<PoVnmStockInVO>();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select p.po_vnm_id as id, ");
		sql.append(" po.sale_order_number as saleOrderNumber, ");
		sql.append(" p.sale_order_number as poCoNumber, ");
		sql.append(" p.invoice_number as invoiceNumber, ");
		sql.append(" p.po_auto_number as poAutoNumber, ");
		sql.append(" p.type as type, ");
		sql.append(" p.status as status, ");
		sql.append(" to_char(p.request_date, 'dd/mm/yyyy') as requestedDateStr, ");
		sql.append(" prctmp.receivedDateStr as receivedDateStr, ");
		sql.append(" to_char(p.po_vnm_date, 'dd/mm/yyyy') as poVnmDateStr, ");
		sql.append(" p.amount as amount, ");
		sql.append(" p.discount as discount ");
		/** from table*/
		sql.append(" from po_vnm p ");
		sql.append(" left join po_vnm po on po.po_vnm_id = p.from_po_vnm_id ");
		sql.append(" left join ( ");
		sql.append(" select po_vnm_id, max(receivedDateStr) as receivedDateStr from ( ");
		sql.append(" select prc.po_vnm_detail_received_id, ");
		sql.append(" prc.po_vnm_detail_id, ");
		sql.append(" prc.po_vnm_id, ");
		sql.append(" to_char(prc.received_date, 'dd/mm/yyyy') as receiveddatestr, ");
		sql.append(" row_number() over (partition by prc.po_vnm_detail_id order by prc.received_date desc) as rn ");
		sql.append(" from po_vnm_detail_received prc ");
		sql.append(" join po_vnm on po_vnm.po_vnm_id = prc.po_vnm_id ");
		sql.append(" ) where 1 = 1 ");
		sql.append(" and rn = 1 ");
		sql.append(" group by po_vnm_id) prctmp on prctmp.po_vnm_id = p.po_vnm_id ");
		/** where */
		sql.append(" where 1 = 1");
		if (filter.getObjectType() != null) {
			sql.append(" and p.object_type = ?");
			params.add(filter.getObjectType().getValue());
		} else {
			sql.append(" and p.object_type = 1");
		}
		if (null != filter.getPoType()) {
			sql.append(" and p.type = ?");
			params.add(filter.getPoType().getValue());
		}
		if (null != filter.getLstPoVnmType() && filter.getLstPoVnmType().size() > 0) {
			sql.append(" and p.type in (?");
			params.add(filter.getLstPoVnmType().get(0));
			for (int i = 1, sz = filter.getLstPoVnmType().size(); i < sz; i++) {
				sql.append(",?");
				params.add(filter.getLstPoVnmType().get(i));
			}
			sql.append(")");
		}
		if (filter.getShopId() != null) {
			if (filter.getHasFindChildShop()) {
				sql.append(" and p.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
			} else {
				sql.append(" and p.shop_id = ?");
			}
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getPoCoNumber())) {
			if (filter.getIsLikePoNumber() != null && filter.getIsLikePoNumber()) {
				sql.append(" and p.sale_order_number like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getPoCoNumber().toUpperCase()));
			} else {
				sql.append(" and p.sale_order_number) = ? ");
				params.add(filter.getPoCoNumber().toUpperCase());
			}
		}
		//NHAP HANG
		if (filter.getFromDate() != null) {
			sql.append(" and p.po_vnm_date >= trunc(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and p.po_vnm_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		//TRA HANG
		if (filter.getImportFromDate() != null) {
			sql.append(" and p.import_date >= trunc(?)");
			params.add(filter.getImportFromDate());
		}
		if (filter.getImportToDate() != null) {
			sql.append(" and p.import_date < trunc(?) + 1");
			params.add(filter.getImportToDate());
		}
		if (filter.getPoStatus() != null) {
			sql.append(" and p.status = ? ");
			params.add(filter.getPoStatus().getValue());
		}
		if (null != filter.getLstPoVnmStatus() && filter.getLstPoVnmStatus().size() > 0) {
			sql.append(" and p.status in (?");
			params.add(filter.getLstPoVnmStatus().get(0));
			for (int i = 1, sz = filter.getLstPoVnmStatus().size(); i < sz; i++) {
				sql.append(",?");
				params.add(filter.getLstPoVnmStatus().get(i));
			}
			sql.append(")");
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			// lay tu FROM_PO_VNM_ID (sale_order_number)
			sql.append(" and po.sale_order_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getPoAutoNumber())) {
			sql.append(" and p.po_auto_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getPoAutoNumber().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getInvoiceNumber())) {
			sql.append(" and p.invoice_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getInvoiceNumber().toUpperCase()));
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from (");
		countSql.append(sql).append(" ) ");
		
		sql.append(" order by p.po_vnm_date desc, saleOrderNumber ");
		final String[] fieldNames = new String[] { //
				"id",// 1
				"saleOrderNumber",// 2
				"poCoNumber",// 3
				"invoiceNumber",// 4
				"poAutoNumber",// 5
				"type",// 6
				"status", // 6.1
				"requestedDateStr",// 7
				"receivedDateStr",// 8
				"poVnmDateStr", // 9
				"amount", // 10
				"discount"// 11
		};

		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.INTEGER,// 6
				StandardBasicTypes.INTEGER,// 6.1
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.BIG_DECIMAL,// 10
				StandardBasicTypes.BIG_DECIMAL,// 11

		};
		if (null == filter.getkPagingPoStockInVO()) {
			return repo.getListByQueryAndScalar(PoVnmStockInVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		return repo.getListByQueryAndScalarPaginated(PoVnmStockInVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingPoStockInVO());
	}
	
	/*@Override
	public List<PoVnm> getListPoVnm(KPaging<PoVnm> kPaging, PoType poType,
			Long shopId, String poAutoNumber, Date fromDate, Date toDate,
			PoVNMStatus poStatus, String invoiceNumb, boolean hasFindChildShop, String shortField, boolean isLikePoNumber)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<PoVnm>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM p join shop s on p.shop_id = s.shop_id where p.object_type = 1");
		if (null != poType) {
			sql.append(" and p.TYPE = ?");
			params.add(poType.getValue());
		}
		if (shopId != null) {
			if(hasFindChildShop){
				sql.append(" and p.SHOP_ID IN (SELECT SHOP_ID FROM SHOP START WITH SHOP_ID = ? CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID)");	
			}else{
				sql.append(" and p.SHOP_ID = ?");	
			}
			
			params.add(shopId);
		}
//		if (!StringUtility.isNullOrEmpty(poAutoNumber)) {
//			if(isLikePoNumber == true){
//				sql.append(" and PO_CO_NUMBER like ? ESCAPE '/' ");
//				params.add(StringUtility.toOracleSearchLikeSuffix(poAutoNumber.toUpperCase()));
//			} else{
//				sql.append(" and lower(PO_CO_NUMBER) = ?");
//				params.add(poAutoNumber.toLowerCase());
//			}
//		}
		if (!StringUtility.isNullOrEmpty(poAutoNumber)) {
			sql.append(" and p.PO_CO_NUMBER like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(poAutoNumber.toUpperCase()));
		}
		if (fromDate != null) {
			sql.append(" and p.PO_VNM_DATE >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and p.PO_VNM_DATE < trunc(?) + 1");
			params.add(toDate);
		}
		if (null != poStatus) {
			sql.append(" and p.STATUS = ?");
			params.add(poStatus.getValue());
		}
		if (!StringUtility.isNullOrEmpty(invoiceNumb)) {
			sql.append(" and p.INVOICE_NUMBER like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(invoiceNumb.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shortField)) {
			sql.append(" order by " + shortField);
		}
		if (kPaging == null)
			return repo.getListBySQL(PoVnm.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(PoVnm.class, sql.toString(),
					params, kPaging);
	}
	
	
	@Override
	public List<PoVnm> getListPoVnmLike(KPaging<PoVnm> kPaging, PoType poType,
			Long shopId, String poAutoNumber, Date fromDate, Date toDate,
			PoVNMStatus poStatus, String invoiceNumb, boolean hasFindChildShop, String shortField)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<PoVnm>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM where object_type = 1");
		if (null != poType) {
			sql.append(" and TYPE = ?");
			params.add(poType.getValue());
		}
		if (shopId != null && shopId > 0) {
			if(hasFindChildShop){
				sql.append(" and SHOP_ID IN (SELECT SHOP_ID FROM SHOP START WITH SHOP_ID = ? CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID)");	
			}else{
				sql.append(" and SHOP_ID = ?");	
			}
			
			params.add(shopId);
		}
		if (!StringUtility.isNullOrEmpty(poAutoNumber)) {
			sql.append(" and lower(PO_CO_NUMBER) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(poAutoNumber.toLowerCase()));
		}
		if (fromDate != null) {
			sql.append(" and PO_VNM_DATE >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and PO_VNM_DATE < trunc(?) + 1");
			params.add(toDate);
		}
		if (null != poStatus) {
			sql.append(" and STATUS = ?");
			params.add(poStatus.getValue());
		}
		if (!StringUtility.isNullOrEmpty(invoiceNumb)) {
			sql.append(" and lower(INVOICE_NUMBER) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(invoiceNumb.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(shortField)) {
			sql.append(" order by " + shortField);
		}
		if (kPaging == null)
			return repo.getListBySQL(PoVnm.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(PoVnm.class, sql.toString(),
					params, kPaging);
	}
*/
	@Override
	public PoVnm getPoVnmById(Long poVnmId) throws DataAccessException {
		return repo.getEntityById(PoVnm.class, poVnmId);
	}
	
	@Override
	public PoVnm getPoVnmByIdForUpdate(Long poVnmId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from po_vnm where po_vnm_id = ? for update");
		params.add(poVnmId);
		return repo.getFirstBySQL(PoVnm.class, sql.toString(), params);
	}
	
	@Override
	public List<PoVnm> getListPoVnmDVKHForUpdate(Long shopId, String orderNumber,
			PoType objectType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from po_vnm where shop_id = ? and SALE_ORDER_NUMBER = ? and TYPE = ? for update");
		params.add(shopId);
		params.add(orderNumber);
		params.add(objectType.getValue());
		
		return repo.getListBySQL(PoVnm.class, sql.toString(), params);
	}

	@Override
	public List<PoVnm> getListPoConfirmByPoCS(KPaging<PoVnm> kPaging, String poDVKHNumber, PoObjectType objectType, PoType poType)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM where ");
		if (objectType != null) {
			sql.append(" object_type = ?");
			params.add(objectType.getValue());
		} else {
			sql.append(" object_type = 1");
		}
		if (poType != null) {
			if (PoType.PO_CUSTOMER_SERVICE.equals(poType)) { //1
				sql.append(" and TYPE = ?");
				params.add(PoType.PO_CONFIRM.getValue()); // 2
			} else if (PoType.PO_CUSTOMER_SERVICE_RETURN.equals(poType)) { //4
				sql.append(" and TYPE = ?");
				params.add(PoType.RETURNED_SALES_ORDER.getValue()); // 3
			}
		}
		sql.append(" and SALE_ORDER_NUMBER = ?");
		params.add(poDVKHNumber);
		sql.append(" order by PO_CO_NUMBER asc");
		if (kPaging == null) {
			return repo.getListBySQL(PoVnm.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(PoVnm.class, sql.toString(), params, kPaging);
		}
	}
	
	@Override
	public List<PoVnm> getListPoConfirmByPoCSFromPoId(KPaging<PoVnm> kPaging, Long poVnmId, PoObjectType objectType, PoType poType)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from po_vnm where ");
		if (objectType != null) {
			sql.append(" object_type = ?");
			params.add(objectType.getValue());
		} else {
			sql.append(" object_type = 1");
		}
		if (poType != null) {
			if (PoType.PO_CUSTOMER_SERVICE.equals(poType)) { //1
				sql.append(" and type = ?");
				params.add(PoType.PO_CONFIRM.getValue()); // 2
			} else if (PoType.PO_CUSTOMER_SERVICE_RETURN.equals(poType)) { //4
				sql.append(" and type = ?");
				params.add(PoType.RETURNED_SALES_ORDER.getValue()); // 3
			}
		}
		sql.append(" and from_po_vnm_id = ?");
		params.add(poVnmId);
		sql.append(" order by sale_order_number desc");
		if (kPaging == null) {
			return repo.getListBySQL(PoVnm.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(PoVnm.class, sql.toString(), params, kPaging);
		}
	}

	@Override
	public boolean checkExistedPoVnmByPoCoNumber(String poCoNumber)
			throws DataAccessException {
		if (StringUtility.isNullOrEmpty(poCoNumber)) {
			throw new DataAccessException("poConfNumber is null or empty");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM where object_type = 1");
		sql.append(" and PO_CO_NUMBER = ?");
		params.add(poCoNumber);
		return repo.getListBySQL(PoVnm.class, sql.toString(), params) == null ? false
				: true;
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.PoVnmDAO#getListPoDVKH(java.lang.String)
	 */
	@Override
	public PoVnm getPoDVKH(String poAutoNumber)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM where object_type = 1");

		sql.append(" and TYPE = ?");
		params.add(PoType.PO_CUSTOMER_SERVICE.getValue());

		sql.append(" and PO_AUTO_NUMBER = ?");
		params.add(poAutoNumber);

		return repo.getEntityBySQL(PoVnm.class, sql.toString(), params);
	}
	
	@Override
	public PoVnm getPoDVKHBySaleOrderNumber(PoVnmFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM where 1 = 1 ");
		if (filter.getObjectType() != null) {
			sql.append(" and object_type = ? ");
			params.add(filter.getObjectType().getValue());
		}
		if (filter.getPoType() != null) {
			sql.append(" and TYPE = ? ");
			params.add(filter.getPoType().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and SALE_ORDER_NUMBER = ? ");
			params.add(filter.getOrderNumber());
		}
		if (filter.getPoStatus() != null) {
			sql.append(" and status = ? ");
			params.add(filter.getPoStatus().getValue());
		}
		if (filter.getPoVnmId() != null) {
			sql.append(" and po_vnm_id = ? ");
			params.add(filter.getPoVnmId());
		}
		if (filter.getFromPoVnmId() != null) {
			// lay don goc sale order ung voi from_po_vnm_id cua asn
			sql.append(" and po_vnm_id = ? ");
			params.add(filter.getFromPoVnmId());
		}
		if (filter.getShopId() != null) {
			sql.append(" and shop_id = ? ");
			params.add(filter.getShopId());
		}
		return repo.getEntityBySQL(PoVnm.class, sql.toString(), params);
	}

	@Override
	public PoVnm getPoVnmByCode(String code) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM where 1 = 1");

		sql.append(" and PO_CO_NUMBER = ? and status!=?");
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());

		return repo.getEntityBySQL(PoVnm.class, sql.toString(), params);
	}

	@Override
	public List<PoVnm> getListPoDVKHByShop(Long shopId, Date fromDate,
			Date toDate, Long poVnmId) throws DataAccessException{

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<PoVnm>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * ");
		sql.append(" from po_vnm");
		sql.append(" where 1 = 1 ");
		sql.append(" and object_type = 1 ");
		/*sql.append(" where object_type = 1 and type = ?");
		params.add(PoType.PO_CUSTOMER_SERVICE.getValue());*/
		if (fromDate != null) {
			sql.append(" and PO_VNM_DATE >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null){
			sql.append("  and PO_VNM_DATE < (trunc(?) + 1)");
			params.add(toDate);
		}
		if(shopId != null && shopId > 0){
			sql.append(" and shop_id = ?");
			params.add(shopId);
		}
		if (poVnmId != null) {
			sql.append(" and po_vnm_id = ?");
			params.add(poVnmId);
		}
		
		sql.append(" order by po_vnm_id desc");

		return repo.getListBySQL(PoVnm.class, sql.toString(), params);
	}

	/***
	 * @author vuongmq
	 * @date 04/08/2015
	 * @description: lay danh sach PO confirm cua PODV, customize lai truyen vao filter
	 * ham cu: getListPoConfirmForReport(Long shopId, Long poDVKHId)
	 */
	@Override
	public List<PoConfirmVO> getListPoConfirmForReport(PoVnmFilter filter) throws DataAccessException {
		if (filter.getPoVnmId() == null) {
			throw new DataAccessException("PO_VNM_ID is null or empty");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
//		sql.append(" SELECT po.product_id productId, ");
//		sql.append("        PRO.product_code productCode, ");
//		sql.append("        PRO.product_name productName, ");
//		sql.append("        nvl(po.quantity, 0) bookedQuantity, ");
//		sql.append("        po.price productPrice, ");
//		sql.append("        po.create_date importDate, ");
//		sql.append("        nvl(po.quantity, 0) - nvl(poImport.importQuantity, 0) notImportQuantity, ");
//		sql.append("                              nvl(poImport.importQuantity, 0) importPoQuantity, ");
//		sql.append("                              nvl(poNotImport.suspendQuantity, 0) suspendPoDVKHQuantity ");
//		sql.append(" FROM ");
//		sql.append("   (SELECT podvkh.PO_AUTO_NUMBER, ");
//		sql.append("           podvkh_d.product_id, ");
//		sql.append("           podvkh_d.quantity, ");
//		sql.append("           podvkh_d.price, ");
//		sql.append("           podvkh.create_date ");
//		sql.append("    FROM po_vnm podvkh ");
//		sql.append("    LEFT JOIN po_vnm_detail podvkh_d ON podvkh.po_vnm_id = podvkh_d.po_vnm_id ");
//		sql.append("    WHERE podvkh.object_type = 1 and podvkh.po_vnm_id = ? ");
//		params.add(poDVKHId);
//		sql.append("      AND podvkh.TYPE = ? ");
//		params.add(PoType.PO_CUSTOMER_SERVICE.getValue());
//		sql.append("      AND podvkh.shop_id = ?) po ");
//		params.add(shopId);
//		sql.append(" LEFT JOIN ");
//		sql.append("   (SELECT podvkh.PO_AUTO_NUMBER, podvkh_d.PRODUCT_ID, SUM (podvkh_d.quantity) AS importQuantity ");
//		sql.append("    FROM po_vnm podvkh, ");
//		sql.append("         po_vnm_detail podvkh_d ");
//		sql.append("    WHERE  podvkh.object_type = 1 and podvkh.po_vnm_id = podvkh_d.po_vnm_id ");
//		sql.append("      AND podvkh.TYPE = ? ");
//		params.add(PoType.PO_CONFIRM.getValue());
//		sql.append("      AND podvkh.status = ? ");
//		params.add(PoVNMStatus.IMPORTED.getValue());
//		sql.append("      AND podvkh.shop_id = ? ");
//		params.add(shopId);
//		sql.append("    GROUP BY podvkh_d.PRODUCT_ID, ");
//		sql.append("             podvkh.PO_AUTO_NUMBER) poImport ON po.PO_AUTO_NUMBER = poImport.PO_AUTO_NUMBER ");
//		sql.append(" AND po.product_id = poImport.product_id ");
//		sql.append(" LEFT JOIN ");
//		sql.append("   (SELECT podvkh.PO_AUTO_NUMBER, podvkh_d.PRODUCT_ID, SUM (podvkh_d.quantity) AS suspendQuantity ");
//		sql.append("    FROM po_vnm podvkh, ");
//		sql.append("         po_vnm_detail podvkh_d ");
//		sql.append("    WHERE  podvkh.object_type = 1 and podvkh.po_vnm_id = podvkh_d.po_vnm_id ");
//		sql.append("      AND podvkh.TYPE = ? ");
//		params.add(PoType.PO_CONFIRM.getValue());
//		sql.append("      AND podvkh.status = ? ");
//		params.add(PoVNMStatus.NOT_IMPORT.getValue());
//		sql.append("      AND podvkh.shop_id = ? ");
//		params.add(shopId);
//		sql.append("    GROUP BY podvkh_d.PRODUCT_ID, ");
//		sql.append("             podvkh.PO_AUTO_NUMBER) poNotImport ON po.PO_AUTO_NUMBER = poNotImport.PO_AUTO_NUMBER ");
//		sql.append(" AND po.product_id = poNotImport.product_id ");
//		sql.append(" LEFT JOIN PRODUCT PRO ON po.product_id = pro.product_id ");
//		sql.append(" ORDER BY productCode ");
		
		
		//THONGNM
		sql.append(" SELECT po.product_id productId, ");
		sql.append("        PRO.product_code productCode, ");
		sql.append("        PRO.product_name productName, ");
		//sql.append("        nvl(po.quantity, 0) bookedQuantity, ");
		sql.append("        abs(NVL(po.quantity, 0)) bookedQuantity, ");
		sql.append("        po.price productPrice, ");
		sql.append("        po.create_date importDate, ");
		sql.append("        po.product_type productType, "); // vuongmq; 04/08/2015; them productType cho sp Po_vnm_detail
		sql.append("        nvl(poImport.importQuantity, 0) importPoQuantity, ");
		sql.append("        NVL(poNotImport.notImportQuantity, 0) notImportQuantity, ");
		//sql.append("        NVL(po.quantity, 0) - NVL(poImport.importQuantity, 0) suspendPoDVKHQuantity ");
		sql.append("        abs(NVL(po.quantity, 0)) - NVL(poImport.importQuantity, 0) suspendPoDVKHQuantity ");
		/** from table */
		sql.append(" FROM ");
		sql.append("   (SELECT podvkh.SALE_ORDER_NUMBER, ");
		sql.append("           podvkh_d.product_id, ");
		sql.append("           podvkh_d.quantity, ");
		sql.append("           podvkh_d.price, ");
		sql.append("           podvkh_d.product_type, "); // vuongmq; 04/08/2015; them productType cho sp Po_vnm_detail
		sql.append("           podvkh.create_date ");
		sql.append("    FROM po_vnm podvkh ");
		sql.append("    LEFT JOIN po_vnm_detail podvkh_d ON podvkh.po_vnm_id = podvkh_d.po_vnm_id ");
		sql.append("    WHERE podvkh.object_type = 1 and podvkh.po_vnm_id = ? ");
		params.add(filter.getPoVnmId());
		if (PoType.PO_CUSTOMER_SERVICE.equals(filter.getPoType())) {
			sql.append(" AND podvkh.TYPE = ? ");
			params.add(PoType.PO_CUSTOMER_SERVICE.getValue());
		} else if (PoType.PO_CUSTOMER_SERVICE_RETURN.equals(filter.getPoType())) {
			sql.append(" AND podvkh.TYPE = ? ");
			params.add(PoType.PO_CUSTOMER_SERVICE_RETURN.getValue());
		}
		if (filter.getShopId() != null) {
			sql.append(" AND podvkh.shop_id = ? ");
			params.add(filter.getShopId());
		}
		sql.append(" ) po ");
		sql.append(" LEFT JOIN ");
		sql.append("   (SELECT podvkh.SALE_ORDER_NUMBER, podvkh_d.PRODUCT_ID, podvkh_d.PRODUCT_TYPE, " );
		//sql.append("  SUM (podvkh_d.quantity) AS importQuantity ");
		//sql.append("  SUM (podvkh_d.quantity_received) AS importQuantity ");
		sql.append("  SUM (abs(podvkh_d.quantity_received)) AS importQuantity ");
		sql.append("    FROM po_vnm podvkh, ");
		sql.append("         po_vnm_detail podvkh_d ");
		sql.append("    WHERE  podvkh.object_type = 1 and podvkh.po_vnm_id = podvkh_d.po_vnm_id ");
		//sql.append("      AND podvkh.status = ? ");
		//params.add(PoVNMStatus.IMPORTED.getValue());
		sql.append("      AND podvkh.status in (?, ?) ");
		params.add(PoVNMStatus.IMPORTING.getValue());
		params.add(PoVNMStatus.IMPORTED.getValue());
		/** PO confirm voi tung loai don, don nhap or don tra ; Po confirm nhap: 2; Po confirm tra: 3 */
		if (PoType.PO_CUSTOMER_SERVICE.equals(filter.getPoType())) { // 1
			sql.append(" AND podvkh.TYPE = ? ");
			params.add(PoType.PO_CONFIRM.getValue()); // 2
		} else if (PoType.PO_CUSTOMER_SERVICE_RETURN.equals(filter.getPoType())) { //4
			sql.append(" AND podvkh.TYPE = ? ");
			params.add(PoType.RETURNED_SALES_ORDER.getValue()); //3
		}
		/*sql.append("      AND podvkh.TYPE = ? ");
		params.add(PoType.PO_CONFIRM.getValue());*/
		if (filter.getShopId() != null) {
			sql.append(" AND podvkh.shop_id = ? ");
			params.add(filter.getShopId());
		}
		sql.append("    GROUP BY podvkh_d.PRODUCT_ID, ");
		sql.append("     podvkh.SALE_ORDER_NUMBER, podvkh_d.PRODUCT_TYPE) ");
		sql.append(" poImport ON po.SALE_ORDER_NUMBER = poImport.SALE_ORDER_NUMBER ");
		sql.append(" AND po.product_id = poImport.product_id ");
		sql.append(" AND po.product_type = poImport.product_type "); // them vao product_type
		sql.append(" LEFT JOIN ");
		sql.append("   (SELECT podvkh.SALE_ORDER_NUMBER, podvkh_d.PRODUCT_ID, podvkh_d.PRODUCT_TYPE, ");
		//sql.append(" SUM (podvkh_d.quantity) AS notImportQuantity ");
		//sql.append(" SUM (podvkh_d.quantity)- SUM (nvl(podvkh_d.quantity_received,0)) AS notImportQuantity ");
		sql.append(" SUM(abs(NVL(podvkh_d.quantity,0)))- SUM (abs(nvl(podvkh_d.quantity_received,0))) AS notImportQuantity ");
		sql.append("    FROM po_vnm podvkh, ");
		sql.append("         po_vnm_detail podvkh_d ");
		sql.append("    WHERE  podvkh.object_type = 1 and podvkh.po_vnm_id = podvkh_d.po_vnm_id ");
		//sql.append("      AND podvkh.status = ? ");
		//params.add(PoVNMStatus.NOT_IMPORT.getValue());
		sql.append("      AND podvkh.status in (?, ?) ");
		params.add(PoVNMStatus.NOT_IMPORT.getValue());
		params.add(PoVNMStatus.IMPORTING.getValue());
		/** PO confirm voi tung loai don, don nhap or don tra ; Po confirm nhap: 2; Po confirm tra: 3 */
		if (PoType.PO_CUSTOMER_SERVICE.equals(filter.getPoType())) { // 1
			sql.append(" AND podvkh.TYPE = ? ");
			params.add(PoType.PO_CONFIRM.getValue()); // 2
		} else if (PoType.PO_CUSTOMER_SERVICE_RETURN.equals(filter.getPoType())) { //4
			sql.append(" AND podvkh.TYPE = ? ");
			params.add(PoType.RETURNED_SALES_ORDER.getValue()); //3
		}
		/*sql.append("      AND podvkh.TYPE = ? ");
		params.add(PoType.PO_CONFIRM.getValue());*/
		if (filter.getShopId() != null) {
			sql.append(" AND podvkh.shop_id = ? ");
			params.add(filter.getShopId());
		}
		sql.append("    GROUP BY podvkh_d.PRODUCT_ID, ");
		sql.append("    podvkh.SALE_ORDER_NUMBER,  podvkh_d.PRODUCT_TYPE) ");
		sql.append("    poNotImport ON po.SALE_ORDER_NUMBER = poNotImport.SALE_ORDER_NUMBER ");
		sql.append(" AND po.product_id = poNotImport.product_id ");
		sql.append(" AND po.product_type = poNotImport.product_type "); // them vao product_type
		sql.append(" LEFT JOIN PRODUCT PRO ON po.product_id = pro.product_id ");
		sql.append(" ORDER BY po.sale_order_number desc, PRO.product_code ");
		
		final String[] fieldNames = new String[] { //
				"productId",// 1
				"productCode",// 2
				"productName",// 3
				"bookedQuantity",// 4
				"productPrice",// 5
				"importDate",// 6
				"productType", // 6.1
				"notImportQuantity",// 7
				"importPoQuantity",// 8
				"suspendPoDVKHQuantity"// 9
		};

		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.INTEGER,// 4
				StandardBasicTypes.BIG_DECIMAL,// 5
				StandardBasicTypes.TIMESTAMP,// 6
				StandardBasicTypes.INTEGER,// 6.1
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.INTEGER,// 8
				StandardBasicTypes.INTEGER,// 9

		};
		
		return repo.getListByQueryAndScalar(PoConfirmVO.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}

	@Override
	/**
	 * @edit: tientv  
	 */
	public List<PoCSVO> getListPoCSVO(Long shopId, String orderNumber, String poCoNumber,
			Date fromDate, Date toDate, PoVNMStatus poStatus,
			boolean hasFindChildShop) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<PoCSVO>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("SELECT pocs.PO_VNM_ID AS poVNMId, pocs.AMOUNT AS amount, pocs.SALE_ORDER_NUMBER AS saleOrderNumber,");
		sql.append(" pocs.AMOUNT_RECEIVED AS amountReceived, pocs.QUANTITY AS quantity,");
		sql.append(" pocs.STATUS AS status, pocs.PO_VNM_DATE AS poVNMDate,");
		
		sql.append(" NVL( ");
		sql.append(" (select SUM (pvd.quantity) ");
		sql.append(" from po_vnm pv join po_vnm_detail pvd ON pv.po_vnm_id = pvd.po_vnm_id ");
		sql.append(" where pv.sale_order_number = pocs.sale_order_number ");
		sql.append(" and pv.status = ? ");
		sql.append(" and pv.type = ? ");
		sql.append(" ), 0) AS quantityReceived ");
		
		params.add(PoVNMStatus.IMPORTED.getValue());
		params.add(PoType.PO_CONFIRM.getValue());
		sql.append(" FROM po_vnm pocs, shop s");
		// sql.append(" WHERE pocs.TYPE = ? AND pocs.STATUS not in (?, ?)");
		sql.append(" WHERE pocs.object_type = 1 and pocs.TYPE = ? AND pocs.STATUS <> ?");
		params.add(PoType.PO_CUSTOMER_SERVICE.getValue());
		params.add(PoVNMStatus.PENDING.getValue());
		sql.append(" AND pocs.SHOP_ID = s.SHOP_ID");

		if (null != shopId && shopId > 0) {
			if (hasFindChildShop) {
				sql.append(" AND pocs.SHOP_ID IN (SELECT SHOP_ID FROM SHOP START WITH SHOP_ID = ? CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID)");
			} else {
				sql.append(" and pocs.SHOP_ID = ?");
			}

			params.add(shopId);
		}

		if (!StringUtility.isNullOrEmpty(orderNumber)) {
			sql.append(" and pocs.SALE_ORDER_NUMBER like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(orderNumber.toUpperCase()));
		}
		
		if (!StringUtility.isNullOrEmpty(poCoNumber)) {
			sql.append(" and exists ");
			sql.append(" (select 1 ");
			sql.append(" from po_vnm ");
			sql.append(" where type =2 ");
			sql.append(" and po_co_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(poCoNumber.toUpperCase()));
			sql.append(" and pocs.sale_order_number= sale_order_number ");
			sql.append(" ) ");
		}

		if (null != poStatus) {
			sql.append(" and pocs.STATUS = ?");
			params.add(poStatus.getValue());
		}

		if (null != fromDate) {
			sql.append(" and pocs.PO_VNM_DATE >= trunc(?)");
			params.add(fromDate);
		}

		if (null != toDate) {
			sql.append(" and pocs.PO_VNM_DATE < (trunc(?) + 1)");
			params.add(toDate);
		}

		sql.append(" order by pocs.PO_VNM_DATE, s.SHOP_CODE, pocs.SALE_ORDER_NUMBER");

		final String[] fieldNames = new String[] { //
		"poVNMId",// 1
				"amount", // 2
				"amountReceived",// 3
				"quantity", // 4
				"status", // 5
				"poVNMDate", // 6
				"quantityReceived",// 7
				"saleOrderNumber" };

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.LONG,// 1
				StandardBasicTypes.BIG_DECIMAL,// 2
				StandardBasicTypes.BIG_DECIMAL,// 3
				StandardBasicTypes.INTEGER,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.TIMESTAMP,// 6
				StandardBasicTypes.INTEGER, // 7
				StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(PoCSVO.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RptPoVnmDebitComparison2VO> getListRptPoVnmDebitComparison2VO(
			Long shopId, Date fromDate, Date toDate) throws DataAccessException {
		
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptPoVnmDebitComparison2VO>();
		
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		if (toDate == null) {
			throw new IllegalArgumentException("toDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT po_vnm_date AS poVnmDate,");
		sql.append("   type             AS type,");
		sql.append("   status           AS status,");
		sql.append("   po_co_number     AS poCoNumber,");
		sql.append("   invoice_number   AS invoiceNumber,");
		sql.append("   NVL(total, 0)    AS total");
		sql.append(" FROM po_vnm");
		sql.append(" WHERE object_type = 1 and (type = ?");
		params.add(PoType.PO_CONFIRM.getValue());
		sql.append(" AND status  = ?)");
		params.add(PoVNMStatus.IMPORTED.getValue());
		sql.append(" OR (type    = ?");
		params.add(PoType.RETURNED_SALES_ORDER.getValue());
		sql.append(" AND status  = ?)");
		params.add(PoVNMStatus.IMPORTED.getValue());
		sql.append(" AND shop_id = ?");
		params.add(shopId);
		sql.append(" AND po_vnm_date >= TRUNC(?)");
		params.add(fromDate);
		sql.append(" AND po_vnm_date < (TRUNC(?) + 1)");
		params.add(toDate);
		sql.append(" ORDER BY po_vnm_date");
		final String[] fieldNames = new String[] { //
		"poVnmDate",// 1
				"type", // 2
				"status",// 3
				"poCoNumber", // 4
				"invoiceNumber", // 5
				"total" // 6
		};

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.TIMESTAMP,// 1
				StandardBasicTypes.INTEGER,// 2
				StandardBasicTypes.INTEGER,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.BIG_DECIMAL // 6
		};

		return repo.getListByQueryAndScalar(RptPoVnmDebitComparison2VO.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}
	
	/**
	 * @author hungnm
	 */
	@Override
	public List<RptPoVnmVO> getListRptPoVnmVO(KPaging<RptPoVnmVO> kPaging, Date fromDate,
			Date toDate, Long shopId) throws DataAccessException {
		
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptPoVnmVO>();
		
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select po_co_number as poCoNumber, po_vnm_date as poVnmDate,");
		sql.append(" 	total, (amount+amount*0.1)*0.1 as vatAmount");
		
		countSql.append("select count(1) as count ");
		
		fromSql.append(" from po_vnm where object_type = 1");
		if (fromDate != null) {
			fromSql.append(" and po_vnm_date >= trunc(?)");
			params.add(fromDate);
		}
		
		if (toDate != null) {
			fromSql.append(" and po_vnm_date < (trunc(?) + 1)");
			params.add(toDate);
		}
		
		if (shopId != null) {
			fromSql.append(" and shop_id=?");
			params.add(shopId);
		}
		
		sql.append(fromSql);
		sql.append(" order by po_co_number");
		
		String[] fieldNames = {"poCoNumber", "poVnmDate", "total", "vatAmount"};
		Type[] fieldTypes = {StandardBasicTypes.STRING, StandardBasicTypes.DATE, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL};
		if (kPaging == null) 
			return repo.getListByQueryAndScalar(RptPoVnmVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else 
			return repo.getListByQueryAndScalarPaginated(RptPoVnmVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}
	
	@Override
	public List<RptPoStatusTrackingVO> getListRptPoStatusTrackingVO(Date fromDate, Date toDate,
			Long shopId) throws DataAccessException {
		
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptPoStatusTrackingVO>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select p.product_code as productCode, ");
		sql.append(" 	p.product_name as productName,");
		sql.append(" 	pvd.price as price, ");
		sql.append(" 	p.convfact as convfact, ");
		sql.append("   	pv.po_auto_number as poAutoNumber, ");
		sql.append(" 	pvd.quantity as poQuantity, ");
		sql.append(" 	(select sum(pvd1.quantity) from po_vnm pv1 join po_vnm_detail pvd1 on pv1.po_vnm_id = pvd1.po_vnm_id");
		sql.append("		where pv1.SALE_ORDER_NUMBER = pv.SALE_ORDER_NUMBER and pvd1.product_id = pvd.product_id and type = 2 and status =2 and pv1.object_type = 1");
		sql.append("		) receivedQuantity,");
		sql.append(" 	(select sum(pvd1.quantity) from po_vnm pv1 join po_vnm_detail pvd1 on pv1.po_vnm_id = pvd1.po_vnm_id");
		sql.append("		where pv1.po_auto_number  = pv.po_auto_number  and pvd1.product_id = pvd.product_id and type = 3  and pv1.object_type = 1");
		sql.append("		) returnedQuantity,");
		sql.append(" 	(select sum(pvd1.price * pvd1.quantity) from po_vnm pv1 join po_vnm_detail pvd1 on pv1.po_vnm_id = pvd1.po_vnm_id");
		sql.append("		where pv1.po_auto_number  = pv.po_auto_number and pvd1.product_id = pvd.product_id and type = 3  and pv1.object_type = 1");
		sql.append("		) returnedAmount,");
		sql.append(" 	pv.po_vnm_Date as poAutoDate,");
		sql.append(" 	(pvd.price * pvd.quantity) as poAmount, ");
		sql.append(" 	pv.status as status, ");
		sql.append(" 	pv.type as type");
		
		sql.append(" from po_vnm pv join po_vnm_detail pvd on pv.po_vnm_id = pvd.po_vnm_id ");
		sql.append("       join product p on p.product_id=pvd.product_id ");
		
		sql.append(" where pv.object_type = 1 and type = 1 ");
//		params.add(ActiveType.RUNNING.getValue());
		if (fromDate != null) {
			sql.append("     and pv.po_vnm_date >= trunc(?) ");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("     and pv.po_vnm_date < (trunc(?) + 1) ");
			params.add(toDate);
		}
		if (shopId != null) {
			sql.append("	and pv.shop_id = ?");
			params.add(shopId);
		}
		sql.append(" order by pv.po_vnm_date desc, pv.po_auto_number, p.product_code");
		
		String[] fieldNames = {
				"productCode", "productName", "price", "convfact",
				"poAutoNumber", "poQuantity", "poAmount", "status",
				"type", "poAutoDate", 
				//"receivedQuantity",
				"returnedQuantity",
				"returnedAmount"};
		Type[] fieldTypes = {
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.DATE, 
				//StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, 
				StandardBasicTypes.BIG_DECIMAL};
		/*System.out.println(TestUtil.addParamToQuery(sql.toString(), params));*/
		return repo.getListByQueryAndScalar(RptPoStatusTrackingVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RptPoAutoDataVO> getListPoAutoForReport(List<Long> shopIds,
			Date fromDate, Date toDate) throws DataAccessException {
		
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptPoAutoDataVO>();
		
		if (fromDate == null) {
			throw new IllegalArgumentException("From date is invalidate");
		}
		if (toDate == null) {
			throw new IllegalArgumentException("To date is invalidate");
		}
		if (fromDate.compareTo(toDate) > 0) {
			throw new IllegalArgumentException("From date can't after to date");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT SHO.SHOP_ID AS shopId, SHO.SHOP_CODE AS shopCode, SHO.SHOP_NAME AS shopName, ");
		sql.append("    PO.PO_AUTO_DATE AS poAutoDate, PO.PO_AUTO_NUMBER AS poAutoNumber, ");
		sql.append("    PRO.PRODUCT_ID AS productId, PRO.PRODUCT_CODE AS productCode, PRO.PRODUCT_NAME AS productName, PRO.CONVFACT AS convfact, PRO.UOM1 AS productUnit, ");
		sql.append("    PO_D.QUANTITY AS quantity, PO_D.PRICE AS price ");
		sql.append(" FROM SHOP SHO, PO_AUTO PO, PO_AUTO_DETAIL PO_D, PRODUCT PRO ");
		sql.append(" WHERE PO.PO_AUTO_DATE >= trunc(?) AND PO.PO_AUTO_DATE < (trunc(?) + 1) ");

		params.add(fromDate);
		params.add(toDate);

		if (null != shopIds && shopIds.size() > 0) {
			String paramFix = StringUtility.getParamsIdFixBugTooLongParams(shopIds, "SHO.SHOP_ID");
			sql.append(paramFix);
		}

		sql.append(") ");
		sql.append("    AND SHO.SHOP_ID = PO.SHOP_ID ");
		sql.append("    AND PO.STATUS = ? ");
		params.add(ApprovalStatus.APPROVED.getValue());
		sql.append("    AND PO.PO_AUTO_ID = PO_D.PO_AUTO_ID ");
		sql.append("    AND PO_D.PRODUCT_ID = PRO.PRODUCT_ID ");
		sql.append(" ORDER BY SHO.SHOP_CODE ASC, PO.PO_AUTO_DATE ASC, PO.PO_AUTO_NUMBER ASC, PRO.PRODUCT_CODE ASC ");

		final String[] fieldNames = new String[] { //
		"shopId", // 1
				"shopCode", // 2
				"shopName", // 3
				"poAutoDate", // 4
				"poAutoNumber", // 5
				"productId", // 6
				"productCode", // 7
				"productName", // 8
				"convfact", // 9
				"productUnit", // 10
				"quantity", // 11
				"price" // 12
		};

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.DATE,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.LONG, // 6
				StandardBasicTypes.STRING, // 7
				StandardBasicTypes.STRING, // 8
				StandardBasicTypes.LONG, // 9
				StandardBasicTypes.STRING, // 10
				StandardBasicTypes.LONG, // 11
				StandardBasicTypes.FLOAT // 12
		};

		return repo.getListByQueryAndScalar(RptPoAutoDataVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	
	@Override
	public List<PoVnm> getListPoConfirmByPoAutoNumber(String poAutoNumber,
			PoVNMStatus status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM where object_type = 1");

		sql.append(" and TYPE = ?");
		params.add(PoType.PO_CONFIRM.getValue());
		
		if (status != null) {
		sql.append(" and STATUS = ?");
		params.add(status.getValue());
		}

		sql.append(" and PO_AUTO_NUMBER = ?");
		params.add(poAutoNumber);

		sql.append(" order by PO_CO_NUMBER desc");

		return repo.getListBySQL(PoVnm.class, sql.toString(), params);
	}
	
	@Override
	public List<PoVnm> getListPoConfirmBySaleOrderNumber(PoVnmFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_VNM where 1 = 1");
		if (filter.getObjectType() != null) {
			sql.append(" and object_type = ?");
			params.add(filter.getObjectType().getValue());
		}
		if (filter.getPoType() != null) {
			sql.append(" and TYPE = ?");
			params.add(filter.getPoType().getValue());
		}
		if (filter.getPoStatus() != null) {
			sql.append(" and STATUS = ?");
			params.add(filter.getPoStatus().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and SALE_ORDER_NUMBER = ?");
			params.add(filter.getOrderNumber());
		}
		if (filter.getFromPoVnmId() != null) {
			sql.append(" and from_po_vnm_id = ? ");
			params.add(filter.getFromPoVnmId());
		}
		sql.append(" order by PO_CO_NUMBER desc");
		return repo.getListBySQL(PoVnm.class, sql.toString(), params);
	}
	
	@Override
	public void updatePoVnm(List<PoVnm> listPoVnm)
			throws DataAccessException {
		if (listPoVnm == null) {
			throw new IllegalArgumentException("poVnm");
		}
		repo.update(listPoVnm);
	}
	
	@Override
	public void deletePoVnmConfirmDetail(PoVnm poConfirm) throws DataAccessException {
		//String sql ="delete po_vnm where po_vnm_id = ?";
		String sql ="";
		List<Object> params = new ArrayList<Object>();
		params.add(poConfirm.getId());
		//repo.executeSQLQuery(sql, params);
		sql ="delete po_vnm_detail where po_vnm_id = ?";
		repo.executeSQLQuery(sql, params);
		sql ="delete po_vnm_lot where po_vnm_id = ?";
		repo.executeSQLQuery(sql, params);
	}
	
	@Override
	public Integer checkProductInPoDVKH(Long poDvkhId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) count from ( ");
		sql.append(" SELECT product_id, ");
		sql.append("   SUM(quantity) quant ");
		sql.append(" FROM ");
		sql.append("   (SELECT product_id, ");
		sql.append("     SUM(quantity) quantity ");
		sql.append("   FROM po_vnm_detail ");
		sql.append("   WHERE po_vnm_id IN ");
		sql.append("     (SELECT po_vnm_id ");
		sql.append("     FROM po_vnm ");
		sql.append("     WHERE object_type            = 0 ");
		sql.append("     AND type                     = 2 ");
		sql.append("     AND upper(sale_order_number) = ");
		sql.append("       (SELECT upper(sale_order_number) FROM po_vnm WHERE po_vnm_id = ? ");
		params.add(poDvkhId);
		sql.append("       ) ");
		sql.append("     ) ");
		sql.append("   GROUP BY product_id ");
		sql.append("   UNION ");
		sql.append("   SELECT product_id, ");
		sql.append("     SUM(-quantity) quantity ");
		sql.append("   FROM po_vnm_detail ");
		sql.append("   WHERE po_vnm_id = ? ");
		params.add(poDvkhId);
		sql.append("   GROUP BY product_id ");
		sql.append("   ) ");
		sql.append(" GROUP BY product_id ");
		sql.append(" ) ");
		sql.append(" where quant != 0 ");
		int c1 = repo.countBySQL(sql.toString(), params);
		
		sql = new StringBuilder();
		params = new ArrayList<Object>();
		
		sql.append(" SELECT COUNT(1) COUNT ");
		sql.append(" FROM po_vnm ");
		sql.append(" WHERE object_type            = 0 ");
		sql.append(" AND type                     = 2 ");
		sql.append(" AND upper(sale_order_number) = ");
		sql.append("   (SELECT upper(sale_order_number) FROM po_vnm WHERE po_vnm_id = ? ");
		params.add(poDvkhId);
		sql.append("   ) ");
		sql.append(" AND (dis_status != 1 ");
		sql.append(" OR dis_status   IS NULL) ");
		
		int c2 = repo.countBySQL(sql.toString(), params);
		if (c1 > 0 && c2 > 0)
			return 1;
		if (c1 > 0 && c2 == 0)
			return 2;
		if (c1 == 0 && c2 > 0)
			return 3;
		if (c1 == 0 && c2 == 0)
			return 4;
		return null;
	}

	@Override
	public Long getPoVnmSeq() throws DataAccessException {
		String sql = "select po_vnm_seq.nextval count from dual";
		BigDecimal b = repo.countBySQLReturnBigDecimal(sql, new ArrayList<Object>());
		return b.longValue();
	}
	
	@Override
	public List<PoSecVO> getListPoSecVO(KPaging<PoSecVO> paging,Long shopId, String poConfirmNumber,
			Date fromDate, Date toDate, Integer type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/** modify by lacnv1 - 11.04.2014 - cap nhat dac ta cong no */
		/*sql.append(" select p.PO_CO_NUMBER poConfirmNumber ");
		sql.append("       ,p.po_vnm_date poVnmDate ");
		sql.append("       ,p.total ");
		sql.append("       ,p.po_vnm_id as poVnmId ");		
		sql.append("       ,dd.remain ");
		sql.append("       ,dd.total_pay totalPay ");
		sql.append("       ,dd.debit_detail_id debitDetailId ");
		sql.append(" from PO_VNM p join debit_detail dd on p.po_vnm_id = dd.from_object_id join debit d on dd.debit_id =d.debit_id ");
		sql.append(" where p.shop_id=? and p.type = 2 and p.status = 2 and dd.remain > 0 and d.object_type=1  ");
		params.add(shopId);
		if (fromDate != null) {
			sql.append("   and p.po_vnm_date >= trunc(?) ");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("   and p.po_vnm_date < trunc(?) + 1 ");
			params.add(toDate);
		}
		if (!StringUtility.isNullOrEmpty(poConfirmNumber)) {
			sql.append("   and p.PO_CO_NUMBER like ? ");
			params.add(StringUtility.toOracleSearchLike(poConfirmNumber.toUpperCase()));
		}
		sql.append(" order by p.PO_CO_NUMBER ");*/
		
		sql.append("select (case when dd.from_object_number is null then po.po_co_number else dd.from_object_number end) as poConfirmNumber,");
		sql.append(" dd.debit_date as poVnmDate,");
		sql.append(" dd.amount as amount,");
		sql.append(" dd.discount as discount,");
		sql.append(" dd.total as total,");
		sql.append(" po.po_vnm_id as poVnmId,");
		sql.append(" dd.remain as remain,");
		sql.append(" dd.total_pay as totalPay,");
		sql.append(" dd.debit_detail_id as debitDetailId");
		sql.append(" from debit_detail dd");
		sql.append(" join debit db on (db.debit_id = dd.debit_id and db.object_type = 1 and db.object_id = ?)");
		params.add(shopId);
		sql.append(" left join po_vnm po on (po.po_vnm_id = dd.from_object_id and po.type = 2 and po.status = 2)");
		sql.append(" where 1 = 1");
		if (type != null) {
			if (type == 0) {
				sql.append(" and dd.remain > 0");
			} else {
				sql.append(" and dd.remain < 0");
			}
		} else {
			sql.append(" and dd.remain <> 0");
		}
		if (fromDate != null) {
			sql.append(" and dd.debit_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and dd.debit_date < trunc(?) + 1");
			params.add(toDate);
		}
		if (!StringUtility.isNullOrEmpty(poConfirmNumber)) {
			sql.append(" and (upper(po.po_co_number) like ? escape '/'");
			sql.append(" or upper(dd.from_object_number) like ? escape '/')");
			String s = StringUtility.toOracleSearchLike(poConfirmNumber.toUpperCase());
			params.add(s);
			params.add(s);
		}
		sql.append(" order by poConfirmNumber, poVnmDate");
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from (");
		countSql.append(sql.toString());
		countSql.append(" )");
		String[] fieldNames = {"poConfirmNumber", "poVnmDate", "amount", "discount", 
								"total", "poVnmId","remain", "totalPay", "debitDetailId"};
		Type[] fieldTypes = {StandardBasicTypes.STRING, StandardBasicTypes.DATE, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG};
		if (paging != null) {
			return repo.getListByQueryAndScalarPaginated(PoSecVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, paging);
		} else {
			return repo.getListByQueryAndScalar(PoSecVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<PoCSVO> getListPoCSVO(Long shopId, String orderNumber, String poCoNumber, Date fromDate, Date toDate,
			List<Integer> lstPoStatus, boolean hasFindChildShop) throws DataAccessException {
		if (fromDate != null && toDate != null && fromDate.after(toDate)) {
			return new ArrayList<PoCSVO>();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT pocs.PO_VNM_ID AS poVNMId, pocs.AMOUNT AS amount, pocs.SALE_ORDER_NUMBER AS saleOrderNumber,");
		sql.append(" pocs.AMOUNT_RECEIVED AS amountReceived, pocs.QUANTITY AS quantity,");
		sql.append(" pocs.STATUS AS status, pocs.PO_VNM_DATE AS poVNMDate,");
		sql.append(" NVL( ");
		sql.append(" (select SUM (pvd.quantity) ");
		sql.append(" from po_vnm pv join po_vnm_detail pvd ON pv.po_vnm_id = pvd.po_vnm_id ");
		sql.append(" where pv.sale_order_number = pocs.sale_order_number ");
		sql.append(" and pv.status = ? ");
		sql.append(" and pv.type = ? ");
		sql.append(" ), 0) AS quantityReceived ");
		params.add(PoVNMStatus.IMPORTED.getValue());
		params.add(PoType.PO_CONFIRM.getValue());
		sql.append(" FROM po_vnm pocs, shop s");
		// sql.append(" WHERE pocs.TYPE = ? AND pocs.STATUS not in (?, ?)");
		sql.append(" WHERE pocs.object_type = 1 and pocs.TYPE = ? AND pocs.STATUS <> ?");
		params.add(PoType.PO_CUSTOMER_SERVICE.getValue());
		params.add(PoVNMStatus.PENDING.getValue());
		sql.append(" AND pocs.SHOP_ID = s.SHOP_ID");
		if (null != shopId && shopId > 0) {
			if (hasFindChildShop) {
				sql.append(" AND pocs.SHOP_ID IN (SELECT SHOP_ID FROM SHOP START WITH SHOP_ID = ? CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID)");
			} else {
				sql.append(" and pocs.SHOP_ID = ?");
			}
			params.add(shopId);
		}
		if (!StringUtility.isNullOrEmpty(orderNumber)) {
			sql.append(" and pocs.SALE_ORDER_NUMBER like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(orderNumber.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(poCoNumber)) {
			sql.append(" and exists ");
			sql.append(" (select 1 ");
			sql.append(" from po_vnm ");
			sql.append(" where type =2 ");
			sql.append(" and po_co_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(poCoNumber.toUpperCase()));
			sql.append(" and pocs.sale_order_number= sale_order_number ");
			sql.append(" ) ");
		}
		if (null != lstPoStatus && lstPoStatus.size()>0) {
			sql.append(" and pocs.STATUS in (?");
			params.add(lstPoStatus.get(0));
			for(int i = 1; i < lstPoStatus.size(); i++){
				sql.append(",?");
				params.add(lstPoStatus.get(i));
			}
			sql.append(")");
		}
		if (null != fromDate) {
			sql.append(" and pocs.PO_VNM_DATE >= trunc(?)");
			params.add(fromDate);
		}
		if (null != toDate) {
			sql.append(" and pocs.PO_VNM_DATE < (trunc(?) + 1)");
			params.add(toDate);
		}
		sql.append(" order by pocs.PO_VNM_DATE, s.SHOP_CODE, pocs.SALE_ORDER_NUMBER");
		final String[] fieldNames = new String[] { //
		"poVNMId",// 1
				"amount", // 2
				"amountReceived",// 3
				"quantity", // 4
				"status", // 5
				"poVNMDate", // 6
				"quantityReceived",// 7
				"saleOrderNumber" };
		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.LONG,// 1
				StandardBasicTypes.BIG_DECIMAL,// 2
				StandardBasicTypes.BIG_DECIMAL,// 3
				StandardBasicTypes.INTEGER,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.TIMESTAMP,// 6
				StandardBasicTypes.INTEGER, // 7
				StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(PoCSVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/***
	 * @author vuongmq
	 * @date 03/08/2015
	 * @description lay danh sach POVNM don nhap, don tra
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	/** vuongmq; 03/08/2015; Danh sach PoCSVO filter
	 * lay tu: getListPoCSVO(Long shopId, String orderNumber, String poCoNumber, Date fromDate, Date toDate,
			List<Integer> lstPoStatus, boolean hasFindChildShop)
	 * */
	@Override
	public List<PoCSVO> getListPoCSVOFilter(PoVnmFilter filter) throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate())) {
			return new ArrayList<PoCSVO>();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT pocs.PO_VNM_ID AS poVNMId, ");
		sql.append(" s.shop_code as shopCode, s.shop_name as shopName, ");
		sql.append(" pocs.AMOUNT AS amount, pocs.SALE_ORDER_NUMBER AS saleOrderNumber,");
		sql.append(" pocs.AMOUNT_RECEIVED AS amountReceived, pocs.QUANTITY AS quantity,");
		sql.append(" pocs.STATUS AS status, pocs.PO_VNM_DATE AS poVNMDate,");
		sql.append(" pocs.type,");
		sql.append(" ( case when pocs.type = 1 then ");
		sql.append(" NVL( ");
		sql.append(" (select SUM (pvd.quantity_received) ");
		sql.append(" from po_vnm pv join po_vnm_detail pvd ON pv.po_vnm_id = pvd.po_vnm_id ");
		sql.append(" where pv.from_po_vnm_id = pocs.po_vnm_id ");
		sql.append(" and pv.status in (?, ?) ");
		params.add(PoVNMStatus.IMPORTING.getValue());
		params.add(PoVNMStatus.IMPORTED.getValue());
		sql.append(" and pv.type = ? ");
		params.add(PoType.PO_CONFIRM.getValue()); // 2
		sql.append(" ), 0) ");
		sql.append(" when pocs.type = 4 then ");
		sql.append(" NVL( ");
		sql.append(" (select SUM (pvd.quantity_received) ");
		sql.append(" from po_vnm pv join po_vnm_detail pvd ON pv.po_vnm_id = pvd.po_vnm_id ");
		sql.append(" where pv.from_po_vnm_id = pocs.po_vnm_id ");
		sql.append(" and pv.status in (?, ?) ");
		params.add(PoVNMStatus.IMPORTING.getValue());
		params.add(PoVNMStatus.IMPORTED.getValue());
		sql.append(" and pv.type = ? ");
		params.add(PoType.RETURNED_SALES_ORDER.getValue()); // 3
		sql.append(" ), 0) ");
		sql.append(" end ) AS quantityReceived, ");
		sql.append(" pocs.po_auto_number AS poAutoNumber, ");
		sql.append(" pocs.discount AS discount, ");
		sql.append(" pocs.total AS total ");
		/** From table */
		sql.append(" FROM po_vnm pocs, shop s");
		//sql.append(" WHERE pocs.object_type = 1 and pocs.TYPE = ? AND pocs.STATUS <> ?");
		sql.append(" WHERE pocs.object_type = 1 and pocs.TYPE in (?, ?) AND pocs.STATUS <> ?");
		params.add(PoType.PO_CUSTOMER_SERVICE.getValue()); // 1
		params.add(PoType.PO_CUSTOMER_SERVICE_RETURN.getValue()); // 4
		params.add(PoVNMStatus.PENDING.getValue());
		sql.append(" AND pocs.SHOP_ID = s.SHOP_ID");
		if (null != filter.getShopId() && filter.getShopId() > 0) {
			if (filter.getHasFindChildShop()) {
				sql.append(" AND pocs.SHOP_ID IN (SELECT SHOP_ID FROM SHOP START WITH SHOP_ID = ? CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID)");
			} else {
				sql.append(" and pocs.SHOP_ID = ?");
			}

			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and pocs.sale_order_number like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getOrderNumber().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getPoAutoNumber())) {
			sql.append(" and pocs.po_auto_number like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getPoAutoNumber().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getPoCoNumber())) {
			sql.append(" and exists ");
			sql.append(" (select 1 ");
			sql.append(" from po_vnm ");
			sql.append(" where 1=1 ");
			//sql.append(" and type =2 ");
			sql.append(" and po_co_number like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getPoCoNumber().toUpperCase()));
			sql.append(" and pocs.sale_order_number= sale_order_number ");
			sql.append(" ) ");
		}
		if (null != filter.getLstPoVnmStatus() && filter.getLstPoVnmStatus().size()>0) {
			sql.append(" and pocs.STATUS in (?");
			params.add(filter.getLstPoVnmStatus().get(0));
			for(int i = 1, sz = filter.getLstPoVnmStatus().size(); i < sz; i++){
				sql.append(",?");
				params.add(filter.getLstPoVnmStatus().get(i));
			}
			sql.append(")");
		}
		if (null != filter.getPoType()) {
			sql.append(" and pocs.type = ? ");
			params.add(filter.getPoType().getValue());
		}
		if (null != filter.getWarehouseId() && filter.getWarehouseId().intValue() > ActiveType.DELETED.getValue().intValue()) {
			sql.append(" and pocs.warehouse_id = ? ");
			params.add(filter.getWarehouseId());
		}
		if (null != filter.getFromDate()) {
			sql.append(" and pocs.PO_VNM_DATE >= trunc(?)");
			params.add(filter.getFromDate());
		}
		if (null != filter.getToDate()) {
			sql.append(" and pocs.PO_VNM_DATE < (trunc(?) + 1)");
			params.add(filter.getToDate());
		}
		sql.append(" order by pocs.po_vnm_date desc, pocs.sale_order_number, s.shop_code");
		final String[] fieldNames = new String[] { //
				"poVNMId",// 1
				"shopCode",// 1.1
				"shopName",// 1.2
				"amount", // 2
				"amountReceived",// 3
				"quantity", // 4
				"status", // 5
				"poVNMDate", // 6
				"type", // 6.1
				"quantityReceived",// 7
				"saleOrderNumber", // 8
				"poAutoNumber", //9
				"discount", //10
				"total" //11
				};
		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 1.1
				StandardBasicTypes.STRING,// 1.2
				StandardBasicTypes.BIG_DECIMAL,// 2
				StandardBasicTypes.BIG_DECIMAL,// 3
				StandardBasicTypes.INTEGER,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.TIMESTAMP,// 6
				StandardBasicTypes.INTEGER, // 6.1
				StandardBasicTypes.INTEGER, // 7
				StandardBasicTypes.STRING, //8
				StandardBasicTypes.STRING,  //9
				StandardBasicTypes.BIG_DECIMAL, //10 
				StandardBasicTypes.BIG_DECIMAL, //11
				};
		return repo.getListByQueryAndScalar(PoCSVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import test.ths.dms.common.TestUtils;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

import ths.dms.core.entities.Media;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.MediaFilter;
import ths.dms.core.exceptions.DataAccessException;

public class MediaDAOImpl implements MediaDAO {
	@Autowired
	private IRepository repo;	
	
	@Override
	public Media createMedia(Media media) throws DataAccessException {
		if (media == null) {
			throw new IllegalArgumentException("Media");
		}
		repo.create(media);
		return repo.getEntityById(Media.class, media.getMediaId());
	}

	@Override
	public void deleteMedia(Media media) throws DataAccessException {
		if (media == null) {
			throw new IllegalArgumentException("Media");
		}
		repo.delete(media);
	}

	@Override
	public Media getMediaById(long id) throws DataAccessException {
		return repo.getEntityById(Media.class, id);
	}
	
	@Override
	public Media updateMedia(Media media) throws DataAccessException {
		if (media == null) {
			throw new IllegalArgumentException("Media");
		}
		
		repo.update(media);
		return this.getMediaById(media.getMediaId());
	}
	
	@Override
	public Media getMediaByCode(String mediaCode) throws DataAccessException {		
		if(StringUtility.isNullOrEmpty(mediaCode)){
			return null;
		}
		String sql = "select * from media where upper(media_code) = upper(?)";
		List<Object> params = new ArrayList<Object>();
		params.add(mediaCode);		
		return repo.getEntityBySQL(Media.class, sql, params);
	}
	
	@Override
	public List<Media> getListMedia(MediaFilter filter)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		if(null == filter){
			sql.append(" select * ");
//			sql.append("   select md.media_id as mediaId   "); 
//			sql.append("   , md.media_code as mediaCode   "); 
//			sql.append("   , md.media_name as mediaName   "); 
//			sql.append("   , md.status as status   "); 
//			sql.append("   , md.type as type   "); 
//			sql.append("   , md.media_item_id as mediaItemId   "); 
//			sql.append("   , md.create_date as createDate   "); 
//			sql.append("   , md.update_date as updateDate   "); 
			sql.append("   from media md   "); 
			sql.append("   where 1 = 1   ");
		}else{
			sql.append(" select * ");
//			sql.append("   select md.media_id as mediaId   "); 
//			sql.append("   , md.media_code as mediaCode   "); 
//			sql.append("   , md.media_name as mediaName   "); 
//			sql.append("   , md.status as status   "); 
//			sql.append("   , md.type as type   "); 
//			sql.append("   , md.media_item_id as mediaItemId   "); 
//			sql.append("   , md.create_date as createDate   "); 
//			sql.append("   , md.update_date as updateDate   "); 
			sql.append("   from media md   "); 
			sql.append("   where 1 = 1   ");
			if(null != filter.getStatus()){
				if (filter.getStatus().equals(-1)){
					sql.append("   and status in (0, 1)  ");
				} else {
					sql.append("   and status = ?   ");
					params.add(filter.getStatus());
				}
			}else{
				sql.append("   and status = 1   ");
			}
			
			if(null != filter.getType()){
				sql.append("   and type=?   ");
				params.add(filter.getType());
			}		
			
			if(null != filter.getMediaCode()){
				sql.append("   and upper(md.media_code) like upper(?)   ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getMediaCode().toUpperCase())));
			}
			
			if(null != filter.getMediaName()){
				//sql.append("   and upper(md.media_name) like upper(?)	ESCAPE '/' ");
				// Tulv2 update 18.10.2014 thuc hien tim kiem khong dau.
				sql.append(" and upper(UNICODE2ENGLISH(media_name)) like upper(?)	ESCAPE '/'");
				params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getMediaName()).toUpperCase()));
			}
			 
			if(null != filter.getCreateFromDate()){
				sql.append("   and trunc(md.create_date) >= trunc(?)    ");
				params.add(filter.getCreateFromDate());				
			}
			if(null != filter.getCreateToDate()){
				sql.append("   and trunc(md.create_date) <= trunc(?)   ");
				params.add(filter.getCreateToDate());
			}
			if(null != filter.getUpdateFromDate()){
				sql.append("   and trunc(md.update_date) >= trunc(?)    ");
				params.add(filter.getUpdateFromDate());
			}
			if(null != filter.getUpdateToDate()){
				sql.append("   and trunc(md.update_date) <= trunc(?) ");
				params.add(filter.getUpdateToDate());
			}			
			
			sql.append("   order by md.media_code   ");	 	
			//System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));
		}
		if(null != filter && null != filter.getkPaging()){
			return repo.getListBySQLPaginated(Media.class, sql.toString(), params, filter.getkPaging());			
		}		
		else{
			return repo.getListBySQL(Media.class, sql.toString(), params);			
		}
	}
	
	@Override
	public List<Product> getListProductByMedia(KPaging<Product> kPaging, Long mediaId, Integer status ) throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();		
		sql.append("   select distinct p.*    "); 
		sql.append("   from product p   "); 
		sql.append("   where 1 = 1   "); 
		if(null != status){
			sql.append("   and p.status = ?   ");
			params.add(status);
		}else{
			sql.append("   and p.status = 1   ");
		}
		sql.append("   and p.product_id in (   "); 
		sql.append("      select object_id   "); 
		sql.append("      from media md join media_map mm on md.media_id = mm.media_id   "); 
		sql.append("      where 1 = 1   ");
		sql.append("   and mm.status = 1   ");
		if(null != mediaId){
			sql.append("                 and md.media_id = ?   ");
			params.add(mediaId);
		}
		 
		sql.append("                 )");
		sql.append("   order by p.product_code   "); 
		 
		if(null != kPaging){
			return repo.getListBySQLPaginated(Product.class, sql.toString(), params, kPaging);
		}else{
			return repo.getListBySQL(Product.class, sql.toString(), params);
		}			
	}
	
}

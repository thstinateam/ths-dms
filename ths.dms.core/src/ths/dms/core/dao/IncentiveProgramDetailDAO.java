package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.IncentiveProgramDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.IncentiveProgramDetailFilter;
import ths.dms.core.entities.enumtype.IncentiveProgramDetailVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface IncentiveProgramDetailDAO {

	IncentiveProgramDetail createIncentiveProgramDetail(IncentiveProgramDetail incentiveProgramDetail, LogInfoVO logInfo) throws DataAccessException;

	void deleteIncentiveProgramDetail(IncentiveProgramDetail incentiveProgramDetail, LogInfoVO logInfo) throws DataAccessException;

	void updateIncentiveProgramDetail(IncentiveProgramDetail incentiveProgramDetail, LogInfoVO logInfo) throws DataAccessException;
	
	IncentiveProgramDetail getIncentiveProgramDetailById(long id) throws DataAccessException;

	List<IncentiveProgramDetail> getListIncentiveProgramDetail(Long incentiveProgramId, ActiveType status,
			KPaging<IncentiveProgramDetail> kPaging)
			throws DataAccessException;

	List<IncentiveProgramDetailVO> getListIncentiveProgramDetailVO(
			IncentiveProgramDetailFilter filter,
			KPaging<IncentiveProgramDetailVO> kPaging)
			throws DataAccessException;

	Boolean isExistProduct(Long incentiveProgramId, Long productId, Date fromDate, Date toDate,  Long exceptedId)
			throws DataAccessException;
	
	Boolean isExistCategory(Long incentiveProgramId, Long catId, Date fromDate, Date toDate)
			throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.PromotionCustAttrDetail;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface PromotionCustAttrDetailDAO {

	PromotionCustAttrDetail createPromotionCustAttrDetail(PromotionCustAttrDetail detail, LogInfoVO logInfo) throws DataAccessException;

	void deletePromotionCustAttrDetail(PromotionCustAttrDetail detail, LogInfoVO logInfo) throws DataAccessException;

	void updatePromotionCustAttrDetail(PromotionCustAttrDetail detail, LogInfoVO logInfo) throws DataAccessException;
	
	PromotionCustAttrDetail getPromotionCustAttrDetailById(long id) throws DataAccessException;
	
	List<PromotionCustAttrDetail> getListPromotionCustAttrDetail(long id) throws DataAccessException;

	PromotionCustAttrDetail getPromotionCustAttrDetailbyObjectType_and_ObjectId(Long promotionCustAttrId, Long objectType, Long objectId) 
			throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class StaffDAONewImpl implements StaffDAONew {
	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	private CommonDAO commonDAO;

	@Override
	public Staff createStaff(Staff staff, LogInfoVO logInfo) throws DataAccessException {
		if (staff == null) {
			throw new IllegalArgumentException("staff");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (staff.getStaffCode() != null)
			staff.setStaffCode(staff.getStaffCode().toUpperCase());
		if (staff.getSaleTypeCode() != null)
			staff.setSaleTypeCode(staff.getSaleTypeCode().toUpperCase());
		staff.setCreateUser(logInfo.getStaffCode());
		repo.create(staff);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, staff, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(Staff.class, staff.getId());
	}

	@Override
	public void deleteStaff(Staff staff, LogInfoVO logInfo) throws DataAccessException {
		if (staff == null) {
			throw new IllegalArgumentException("staff");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(staff);
		try {
			actionGeneralLogDAO.createActionGeneralLog(staff, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Staff getStaffById(long id) throws DataAccessException {
		return repo.getEntityById(Staff.class, id);
	}

	@Override
	public void updateStaff(Staff staff, LogInfoVO logInfo) throws DataAccessException {
		if (staff == null) {
			throw new IllegalArgumentException("staff");
		}
		if (staff.getStaffCode() != null)
			staff.setStaffCode(staff.getStaffCode().toUpperCase());
		if (staff.getSaleTypeCode() != null)
			staff.setSaleTypeCode(staff.getSaleTypeCode().toUpperCase());
		Staff temp = this.getStaffById(staff.getId());

		staff.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null)
			staff.setUpdateUser(logInfo.getStaffCode());
		temp = temp.clone();
		repo.update(staff);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, staff, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Staff getStaffByCode(String code) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from STAFF where staff_code = ? and status != ? ";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		Staff rs = repo.getEntityBySQL(Staff.class, sql, params);
		return rs;
	}

	@Override
	public Staff getStaffLogin(String code, String password) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from STAFF where staff_code=? and password = ? and status = ? ";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(password);
		params.add(ActiveType.RUNNING.getValue());
		Staff rs = repo.getEntityBySQL(Staff.class, sql, params);
		return rs;
	}

	@Override
	public List<StaffVO> searchListStaffVOByFilter(StaffPrsmFilter<StaffVO> filter) throws DataAccessException {
		if (filter.getLstShopId() == null || filter.getLstShopId().size() == 0) {
			throw new IllegalArgumentException("filter.getLstShopId()");
		}
		if (filter.getInheritUserPriv() == null) {
			throw new IllegalArgumentException("getInheritUserPriv is not null");
		}
		if (filter.getIsLstChildStaffRoot() == null) {
			throw new IllegalArgumentException("getIsLstChildStaffRoot is not null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with Rparentstaff as ( ");
		sql.append("  select psm.staff_id as staff_id, psm.parent_staff_id as parent_staff_id from parent_staff_map psm where psm.status = 1  ");
		//Tham so la danh sach nhung Object_id
		/*if (filter.getLstObjectType() != null && !filter.getLstObjectType().isEmpty()) {
			sql.append(" and  psm.staff_id in (select staff_id from staff where staff_type_id in (select staff_type_id from staff_type where status = 1 and SPECIFIC_TYPE in (-1  ");
			for (Integer value : filter.getLstObjectType()) {
				sql.append("  , ? ");
				params.add(value);
			}
			sql.append(" )))");
		}*/
		if (filter.getLstSpecType() != null && !filter.getLstSpecType().isEmpty()) {
			sql.append(" and  psm.staff_id in (select staff_id from staff where staff_type_id in (select staff_type_id from staff_type where status = 1 and SPECIFIC_TYPE in (-1  ");
			for (Integer value : filter.getLstSpecType()) {
				sql.append("  , ? ");
				params.add(value);
			}
			sql.append(" )))");
		}
		
		sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and psm.staff_id = parent_staff_id)  ");
		sql.append("  start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id)  ");
		params.add(filter.getInheritUserPriv());

		sql.append(" select distinct ");
		sql.append(" st.staff_id as id ");
		sql.append(" ,st.staff_code as staffCode ");
		sql.append(" ,st.staff_name as staffName ");
		sql.append(" ,st.address as address ");
		sql.append(" ,st.shop_id as shopId ");
		sql.append(" ,(select shop_code from shop where shop_id = st.shop_id ) as shopCode ");
		sql.append(" ,(select shop_name from shop where shop_id = st.shop_id ) as shopName ");
		///Co the bo sung them cac truong select con thieu
		sql.append(" from staff st left join Rparentstaff rs on rs.staff_id = st.staff_id ");
		if (StringUtility.isNullOrEmpty(filter.getStaffIdListStr()) && filter.getShopId() != null && filter.getRoleId() != null && filter.getUserId() != null) {
			sql.append(" JOIN table (F_GET_LIST_CHILD_STAFF_INHERIT(?, sysdate, ?, ?, ?)) on st.staff_id = number_key_id and st.shop_id = number_shop_id ");
			params.add(filter.getUserId());
			params.add(filter.getRoleId());
			params.add(filter.getShopId());
			params.add(StaffSpecificType.STAFF.getValue());
		}
		sql.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getStaffIdListStr())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStaffIdListStr(), "st.staff_id");
			sql.append(paramsFix);
		}
		if (filter.getIsLstChildStaffRoot()) {
			sql.append(" and rs.staff_id is not null ");
		}
		if (filter.getIsGetChildShop() == null || filter.getIsGetChildShop()) {
			sql.append(" and st.shop_id in (select shop_id from shop where status = 1 start with shop_id in (-1  ");
			for (Long value : filter.getLstShopId()) {
				sql.append("  , ? ");
				params.add(value);
			}
			sql.append(" ) ");
			sql.append(" connect by prior shop_id = parent_shop_id ) ");
		} else {
			String paramFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getLstShopId(), "st.shop_id");
			sql.append(paramFix);
		}
		if (filter.getStatus() != null) {
			sql.append(" and st.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and st.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and st.staff_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffName())) {
			sql.append(" and lower(st.name_text) like ? ESCAPE '/' ");
			String nameTmp = Unicode2English.codau2khongdau(filter.getStaffName().trim());
			params.add(StringUtility.toOracleSearchLikeSuffix(nameTmp.toLowerCase()));

		}
		if (!StringUtility.isNullOrEmpty(filter.getAddress())) {
			sql.append(" and unicode2english(st.address) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getAddress().trim()).toLowerCase()));
		}
		if(filter.getIsGetChildShop() == null || filter.getIsGetChildShop()){
			if (filter.getShopId() != null) {
				sql.append(" and st.shop_id in (select shop_id from shop where status = ? start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
				params.add(ActiveType.RUNNING.getValue());
				params.add(filter.getShopId());
			}
		} else {
			if (filter.getShopId() != null) {
				sql.append(" and st.shop_id = ? ");
				params.add(filter.getShopId());
			}
		}
		if (filter.getObjectType() != null) {
//			if (StaffObjectType.NVBHANDNVVS.getValue().equals(filter.getObjectType())) {
//				sql.append(" and st.staff_type_id in (select channel_type_id from channel_type where type = ? and object_type in (?, ?)) ");
//				params.add(ChannelTypeType.STAFF.getValue());
//				params.add(StaffObjectType.NVBH.getValue());
//				params.add(StaffObjectType.NVVS.getValue());
//			} else {
//				sql.append(" and st.staff_type_id in (select channel_type_id from channel_type where type = ? and object_type in (?)) ");
//				params.add(ChannelTypeType.STAFF.getValue());
//				params.add(filter.getObjectType());
//			}
			sql.append(" and st.staff_type_id in (select staff_type_id from staff_type where status = 1 and SPECIFIC_TYPE = ? ) ");
			params.add(filter.getObjectType());
		}
		if (filter.getLstObjectType() != null && filter.getLstObjectType().size() > 0) { // lacnv1: them tham so ds object_type
			sql.append(" and st.staff_type_id in (select staff_type_id from staff_type where status = 1 and SPECIFIC_TYPE in (-1 ");
//			params.add(ChannelTypeType.STAFF.getValue());
			for (Integer t : filter.getLstObjectType()) {
				sql.append(" ,? ");
				params.add(t);
			}
			sql.append(" )) ");
		}
		
		//trungtm6 
		if (filter.getLstSpecType() != null && filter.getLstSpecType().size() > 0) { 
			sql.append(" and st.staff_type_id in (select staff_type_id from staff_type where status = 1 and SPECIFIC_TYPE in (-1 ");
			for (Integer t : filter.getLstSpecType()) {
				sql.append(" ,? ");
				params.add(t);
			}
			sql.append(" )) ");
		}

		sql.append("   and st.staff_id not in (select staff_id from exception_user_access where status = 1  and parent_staff_id = ?) ");
		params.add(filter.getInheritUserPriv());
		if (filter.getParentStaffId() != null) {
			/** Ham check phan quyen CMS voi Staff parent **/
			sql.append(" and st.staff_id  in ( ");
			sql.append(" (SELECT psm.staff_id  AS staff_id ");
			sql.append(" FROM parent_staff_map psm ");
			sql.append(" WHERE psm.status      = 1 ");
			sql.append(" AND psm.staff_id NOT IN (SELECT staff_id FROM exception_user_access WHERE status = 1 AND  psm.staff_id = parent_staff_id) ");
			sql.append(" START WITH psm.parent_staff_id = ? ");
			params.add(filter.getParentStaffId());
			sql.append(" CONNECT BY prior psm.staff_id  = psm.parent_staff_id ) ");
		}

		if (filter.getLstParentStaffCode() != null && !filter.getLstParentStaffCode().isEmpty()) {
			/** Ham check phan quyen CMS voi Staff by ParentStaffId **/
			sql.append(" and st.staff_id  in ( ");
			sql.append("   select psm.staff_id as staffId from parent_staff_map psm ");
			sql.append("   where psm.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("   and psm.staff_id not in (select staff_id from exception_user_access where status = ?  and  psm.staff_id = parent_staff_id) ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" start with psm.parent_staff_id in ");
			sql.append(" (select staff_id as parent_staff_id from staff where lower(staff_code) in ('-1'  ");
			for (String value : filter.getLstParentStaffCode()) {
				sql.append("  , ? ");
				params.add(value.trim().toLowerCase());
			}
			sql.append(" ))  ");
			sql.append(" connect by prior psm.staff_id = psm.parent_staff_id) ");
		}

		if (filter.getLstParentStaffId() != null && !filter.getLstParentStaffId().isEmpty()) {
			/** Ham check phan quyen CMS voi Staff by ParentStaffId **/
			sql.append(" and st.staff_id  in ( ");
			sql.append("   select psm.staff_id as staffId from parent_staff_map psm ");
			sql.append("   where psm.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("   and psm.staff_id not in (select staff_id from exception_user_access where status = ?  and  psm.staff_id = parent_staff_id) ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" start with psm.parent_staff_id in (-1 ");
			for (Long value : filter.getLstId()) {
				sql.append("  , ? ");
				params.add(value);
			}
			sql.append(" ) ");
			sql.append(" connect by prior psm.staff_id = psm.parent_staff_id) ");
		}

		if (filter.getLstCode() != null && !filter.getLstCode().isEmpty()) {
			/** Ham check phan quyen CMS voi Staff by listParentStaffCode **/
			sql.append(" and st.staff_id in (select staff_id from staff where lower(staff_code) in ('-1'  ");
			for (String value : filter.getLstCode()) {
				sql.append("  , ? ");
				params.add(value.trim().toLowerCase());
			}
			sql.append(" ))  ");
		}
		if (filter.getLstId() != null && !filter.getLstId().isEmpty()) {
			/** Ham check phan quyen CMS voi Staff by listParentStaffCode **/
			sql.append(" and psm.staff_id  in (-1  ");
			for (Long value : filter.getLstId()) {
				sql.append("  , ? ");
				params.add(value);
			}
			sql.append(" ) ");
		}

		//Append Chuoi From and Where
		sql.append(" order by staffCode ");
		//Count total
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		final String[] fieldNames = new String[] { "id", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}
	
	@Override
	public List<StaffVO> searchListStaffVOByShop(StaffPrsmFilter<StaffVO> filter) throws DataAccessException {
		if (filter.getLstShopId() == null || filter.getLstShopId().size() == 0) {
			throw new IllegalArgumentException("filter.getLstShopId()");
		}
		if (filter.getInheritUserPriv() == null) {
			throw new IllegalArgumentException("getInheritUserPriv is not null");
		}
		if (filter.getIsLstChildStaffRoot() == null) {
			throw new IllegalArgumentException("getIsLstChildStaffRoot is not null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" With lstShop as ");
		sql.append(" ( ");
		sql.append(" SELECT shop_id,shop_type_id ");
		sql.append(" FROM shop ");
		
		sql.append(" WHERE status               = 1 ");

		if (filter.getIsGetChildShop() == null || filter.getIsGetChildShop()) {
			sql.append(" and shop_id in (select shop_id from shop where status = 1 start with shop_id in (-1  ");
			for (Long value : filter.getLstShopId()) {
				sql.append("  , ? ");
				params.add(value);
			}
			sql.append(" ) ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id ) ");
		} else {
			String paramFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getLstShopId(), "shop_id");
			sql.append(paramFix);
		}
		sql.append(" ) ");
		sql.append(" , lstNPP as ");
		sql.append(" ( ");
		sql.append(" select shop_id from lstShop ");
		sql.append(" where exists(select 1 from shop_type st where st.shop_type_id=shop_type_id and st.specific_type=1) ");
		sql.append(" ) ");
		sql.append(" select DISTINCT stf.staff_id AS staffId , ");
		sql.append(" stf.staff_code             AS staffCode , ");
		sql.append(" stf.staff_name             AS staffName , ");
		sql.append(" stf.address                AS address , ");
		sql.append(" stf.shop_id                AS shopId , ");
		sql.append(" (SELECT shop_code FROM shop WHERE shop_id = stf.shop_id  ) AS shopCode ,");
		sql.append(" (SELECT shop_name FROM shop WHERE shop_id = stf.shop_id  ) AS shopName");
		sql.append(" from staff stf");
		//fix loi qua 1000 tham so nv
		if (filter != null && StringUtility.isNullOrEmpty(filter.getStaffIdListStr()) && filter.getUserId() != null && filter.getRoleId() != null && filter.getShopId() != null) {
			sql.append(" JOIN TABLE (F_GET_LIST_CHILD_STAFF_INHERIT(?, sysdate, ?, ?, NULL)) tmp on tmp.number_key_id = stf.staff_id  ");
			params.add(filter.getUserId());
            params.add(filter.getRoleId());
            params.add(filter.getShopId());

		}
		sql.append(" where stf.shop_id in (select shop_id from lstNPP)");
		sql.append(" AND stf.status =1");
		sql.append(" AND stf.staff_type_id in (select staff_type_id from staff_type where specific_type in (1,2))");
    	
		if (!StringUtility.isNullOrEmpty(filter.getStaffIdListStr())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStaffIdListStr(), "stf.staff_id");
			sql.append(paramsFix);
		}
		sql.append(" ORDER BY trim(staffCode)");

//		if (filter.getStatus() != null) {
//			sql.append(" and st.status = ? ");
//			params.add(filter.getStatus());
//		} else {
//			sql.append(" and st.status != ? ");
//			params.add(ActiveType.DELETED.getValue());
//		}
		
		final String[] fieldNames = new String[] { "staffId", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}

	@Override
	public List<StaffVO> getListStaffVOByFilter(StaffPrsmFilter<StaffVO> filter) throws DataAccessException {
		if (filter.getInheritUserPriv() == null) {
			return new ArrayList<StaffVO>();
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();
		sql.append(" select  ");
		sql.append(" st.staff_id as id ");
		sql.append(" ,st.staff_code as staffCode ");
		sql.append(" ,st.staff_name as staffName ");
		sql.append(" ,st.address as address ");
		sql.append(" ,st.shop_id as shopId ");
		sql.append(" ,(select shop_code from shop where shop_id = st.shop_id ) as shopCode ");
		sql.append(" ,(select shop_name from shop where shop_id = st.shop_id ) as shopName ");
		///Co the bo sung them cac truong select con thieu
		fromSql.append(" from staff st ");
		fromSql.append(" where 1 = 1 ");
		if (filter.getStatus() != null) {
			fromSql.append(" and st.status = ? ");
			params.add(filter.getStatus());
		} else {
			fromSql.append(" and st.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			fromSql.append(" and lower(st.staff_code) like ?  ");
			params.add(filter.getStaffCode().trim().toLowerCase());
		}
		
		String paramFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getLstShopId(), "st.shop_id");
		fromSql.append(paramFix);		
		
		fromSql.append(" ) ");
		fromSql.append("   and st.staff_id not in (select staff_id from exception_user_access where status = 1  and parent_staff_id = ?) ");
		params.add(filter.getInheritUserPriv());
		if (filter.getParentStaffId() != null) {
			/** Ham check phan quyen CMS voi Staff by ParentStaffId **/
			fromSql.append(" and st.staff_id  in ( ");
			fromSql.append("   select psm.staff_id as staffId from parent_staff_map psm ");
			fromSql.append("   where psm.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			fromSql.append("   and psm.staff_id not in (select staff_id from exception_user_access where status = ?  and staff_id = parent_staff_id) ");
			params.add(ActiveType.RUNNING.getValue());
			fromSql.append(" start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id) ");
			params.add(filter.getParentStaffId());
		}
		if (filter.getLstParentStaffCode() != null && !filter.getLstParentStaffCode().isEmpty()) {
			/** Ham check phan quyen CMS voi Staff by ParentStaffId **/
			fromSql.append(" and st.staff_id  in ( ");
			fromSql.append("   select psm.staff_id as staffId from parent_staff_map psm ");
			fromSql.append("   where psm.status = 1 ");
			fromSql.append("   and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id) ");
			fromSql.append(" start with psm.parent_staff_id in ");
			fromSql.append(" (select staff_id as parent_staff_id from staff where lower(staff_code) in ('-1'  ");
			for (String value : filter.getLstCode()) {
				fromSql.append("  , ? ");
				params.add(value.trim().toLowerCase());
			}
			fromSql.append(" ))  ");
			fromSql.append(" connect by prior psm.staff_id = psm.parent_staff_id) ");
		}
		if (filter.getLstParentStaffId() != null && !filter.getLstParentStaffId().isEmpty()) {
			/** Ham check phan quyen CMS voi Staff by ParentStaffId **/
			fromSql.append(" and staff_id  in ( ");
			fromSql.append("   select psm.staff_id as staffId from parent_staff_map psm ");
			fromSql.append("   where psm.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			fromSql.append("   and psm.staff_id not in (select staff_id from exception_user_access where status = ?  and staff_id = parent_staff_id) ");
			params.add(ActiveType.RUNNING.getValue());
			fromSql.append(" start with psm.parent_staff_id in (-1 ");
			for (Long value : filter.getLstId()) {
				fromSql.append("  , ? ");
				params.add(value);
			}
			fromSql.append(" ) ");
			fromSql.append(" connect by prior psm.staff_id = psm.parent_staff_id) ");
			params.add(filter.getParentStaffId());
		}

		if (filter.getLstCode() != null && !filter.getLstParentStaffCode().isEmpty()) {
			/** Ham check phan quyen CMS voi Staff by listParentStaffCode **/
			fromSql.append(" and st.staff_id in (select staff_id from staff where lower(staff_code) in ('-1'  ");
			for (String value : filter.getLstCode()) {
				fromSql.append("  , ? ");
				params.add(value.trim().toLowerCase());
			}
			fromSql.append(" ))  ");
		}

		if (filter.getLstId() != null && !filter.getLstParentStaffId().isEmpty()) {
			/** Ham check phan quyen CMS voi Staff by listParentStaffCode **/
			fromSql.append(" and st.staff_id  in (-1  ");
			for (Long value : filter.getLstId()) {
				fromSql.append("  , ? ");
				params.add(value);
			}
			fromSql.append(" ) ");
		}
		///Co the bo sung them dieu kien lay con thieu

		//Append Chuoi From and Where
		sql.append(fromSql.toString());
		sql.append(" order by staffCode ");
		//Count total
		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());
		final String[] fieldNames = new String[] { "id", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	@Override
	public List<StaffVO> getListStaffVOByShopId(BasicFilter<StaffVO> filter) throws DataAccessException {
		if (filter.getInheritUserPriv() == null) {
			throw new IllegalArgumentException("getInheritUserPriv is not null");
		}
		if (filter.getArrLongS() == null) {
			throw new IllegalArgumentException("getArrLongS() is not null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();
		sql.append(" select  ");
		sql.append(" st.staff_id as id ");
		sql.append(" ,st.staff_code as staffCode ");
		sql.append(" ,st.staff_name as staffName ");
		sql.append(" ,st.address as address ");
		sql.append(" ,st.shop_id as shopId ");
		sql.append(" ,(select shop_code from shop where shop_id = st.shop_id ) as shopCode ");
		sql.append(" ,(select shop_name from shop where shop_id = st.shop_id ) as shopName ");
		///Co the bo sung them cac truong select con thieu
		fromSql.append(" from staff st ");
		fromSql.append(" where 1 = 1 ");
		fromSql.append("   and st.staff_id not in (select staff_id from exception_user_access where status = 1  and parent_staff_id = ?) ");
		params.add(filter.getInheritUserPriv());
		if (filter.getParentId() != null) {
			fromSql.append("   and st.staff_id not in (select staff_id from exception_user_access where status = 1  and parent_staff_id = ?) ");
			params.add(filter.getParentId());
			sql.append(" and st.staff_id in ( select psm.staff_id as staff_id from parent_staff_map psm where psm.status = 1 ");
			sql.append(" and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id) ");
			sql.append(" start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id) ");
			params.add(filter.getParentId());
		}
		if (filter.getStatus() != null) {
			fromSql.append(" and st.status = ? ");
			params.add(filter.getStatus());
		} else {
			fromSql.append(" and st.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			fromSql.append(" and lower(st.staff_code) like ?  ");
			params.add(filter.getCode().trim().toLowerCase());
		}

		/** Ham check phan quyen CMS voi Staff by ParentStaffId **/
		fromSql.append(" and st.shop_id in (-1  ");
		for (Long value : filter.getArrLongS()) {
			fromSql.append("  , ? ");
			params.add(value);
		}
		fromSql.append(" ) ");
		if (filter.getArrCode() != null && !filter.getArrCode().isEmpty()) {
			/** Ham check phan quyen CMS voi Staff by listParentStaffCode **/
			fromSql.append(" and st.staff_id in (select staff_id from staff where lower(staff_code) in ('-1'  ");
			for (String value : filter.getArrCode()) {
				fromSql.append("  , ? ");
				params.add(value.trim().toLowerCase());
			}
			fromSql.append(" ))  ");
		}

		if (filter.getArrlongG() != null && !filter.getArrlongG().isEmpty()) {
			/** Ham check phan quyen CMS voi Staff by listParentStaffCode **/
			fromSql.append(" and st.staff_id  in (-1  ");
			for (Long value : filter.getArrlongG()) {
				fromSql.append("  , ? ");
				params.add(value);
			}
			fromSql.append(" ) ");
		}
		///Co the bo sung them dieu kien lay con thieu

		//Append Chuoi From and Where
		sql.append(fromSql.toString());
		sql.append(" order by staffCode ");
		//Count total
		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());
		final String[] fieldNames = new String[] { "id", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	@Override
	public List<StaffVO> getListStaffVOAndChilrentByShopId(BasicFilter<StaffVO> filter) throws DataAccessException {
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("UserId is not null");
		}
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("filter.getShopId() is not null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" select  ");
		sql.append(" st.staff_id as id ");
		sql.append(" ,st.staff_code as staffCode ");
		sql.append(" ,st.staff_name as staffName ");
		sql.append(" ,st.address as address ");
		sql.append(" ,st.shop_id as shopId ");
		sql.append(" ,(select shop_code from shop where shop_id = st.shop_id ) as shopCode ");
		sql.append(" ,(select shop_name from shop where shop_id = st.shop_id ) as shopName ");
		///Co the bo sung them cac truong select con thieu
		fromSql.append(" from staff st ");
		fromSql.append(" where 1 = 1 ");
		fromSql.append(" and st.shop_id in (-1  ");
		for (Long value : filter.getArrLongS()) {
			fromSql.append("  , ? ");
			params.add(value);
		}
		fromSql.append(" ) ");
		if (filter.getStatus() != null) {
			fromSql.append(" and st.status = ? ");
			params.add(filter.getStatus());
		} else {
			fromSql.append(" and st.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			fromSql.append(" and lower(st.staff_code) like ?  ");
			params.add(filter.getCode().trim().toLowerCase());
		}

		/** Ham check phan quyen CMS voi Staff by ParentStaffId **/
		if (filter.getArrCode() != null && !filter.getArrCode().isEmpty()) {
			/** Ham check phan quyen CMS voi Staff by listParentStaffCode **/
			fromSql.append(" and st.staff_id in (select staff_id from parent_staff_map where status = 1 start with staff_id in (select staff_id from staff where lower(staff_code) in ('-1'  ");
			for (String value : filter.getArrCode()) {
				fromSql.append("  , ? ");
				params.add(value.trim().toLowerCase());
			}
			fromSql.append(" )) and status = 1  connect by prior staff_id = parent_staff_id )  ");

		}

		if (filter.getArrlongG() != null && !filter.getArrlongG().isEmpty()) {
			/** Ham check phan quyen CMS voi Staff by listParentStaffCode **/
			fromSql.append(" and st.staff_id  in (-1  ");
			for (Long value : filter.getArrlongG()) {
				fromSql.append("  , ? ");
				params.add(value);
			}
			fromSql.append(" ) ");
		}
		///Co the bo sung them dieu kien lay con thieu

		//Append Chuoi From and Where
		sql.append(fromSql.toString());
		sql.append(" order by staffCode ");
		//Count total
		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());
		final String[] fieldNames = new String[] { "id", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	@Override
	public List<StaffVO> getListStaffVOFullChilrentByCMS(StaffPrsmFilter<StaffVO> filter) throws DataAccessException {
		if (filter.getInheritUserPriv() == null) {
			throw new IllegalArgumentException("getInheritUserPriv is null");
		}
		if (filter.getIsLstChildStaffRoot() == null) {
			throw new IllegalArgumentException("getIsLstChildStaffRoot() is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with Rparentstaff as ( ");
		sql.append("  select psm.staff_id as staff_id, psm.parent_staff_id as parent_staff_id from parent_staff_map psm where psm.status = 1  ");
		//Tham so la danh sach nhung Object_id
		if (filter.getLstObjectType() != null && !filter.getLstObjectType().isEmpty()) {
			sql.append(" and  psm.staff_id in (select staff_id from staff where staff_type_id in (select staff_type_id from staff_type where specific_type in (-1  ");
			for (Integer value : filter.getLstObjectType()) {
				sql.append("  , ? ");
				params.add(value);
			}
			sql.append(" )))");
		}
		sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id)  ");
		sql.append("  start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id)  ");
		params.add(filter.getInheritUserPriv());

		sql.append(" select distinct  ");
		sql.append(" st.staff_id as id ");
		sql.append(" ,st.staff_code as staffCode ");
		sql.append(" ,st.staff_name as staffName ");
		sql.append(" ,st.address as address ");
		sql.append(" ,st.shop_id as shopId ");
		sql.append(" ,(select shop_code from shop where shop_id = st.shop_id ) as shopCode ");
		sql.append(" ,(select shop_name from shop where shop_id = st.shop_id ) as shopName ");

		sql.append(" from staff st left join Rparentstaff rs on rs.staff_id = st.staff_id   ");
		sql.append(" where 1 = 1  ");
		if (filter.getIsLstChildStaffRoot()) {
			sql.append(" and rs.staff_id is not null  ");
		}
		//Tham so tinh trang
		if (filter.getStatus() != null) {
			sql.append(" and st.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and st.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		//Tham so staff Code
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and lower(st.staff_code) like ?  ");
			params.add(filter.getStaffCode().trim().toLowerCase());
		}
		//Tham so la Danh sach Staff Code
		if (filter.getLstCode() != null && !filter.getLstCode().isEmpty()) {
			sql.append(" and lower(st.staff_code) in ('-1'  ");
			for (String value : filter.getLstCode()) {
				sql.append("  , ? ");
				params.add(value);
			}
			sql.append(" )");
		}
		//Tham so la danh sach nhung Object_id
		if (filter.getLstObjectType() != null && !filter.getLstObjectType().isEmpty()) {
			sql.append(" and  st.staff_type_id in (select staff_type_id from staff_type where specific_type in (-1  ");
			for (Integer value : filter.getLstObjectType()) {
				sql.append("  , ? ");
				params.add(value);
			}
			sql.append(" ))");
		}
		//Tham so la danh sach shop
		if (filter.getLstShopId() != null && !filter.getLstShopId().isEmpty()) {
			String paramFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getLstShopId(), "st.shop_id");
			sql.append(paramFix);
		}
		//Tham so la danh sach parent staff Id
		if (filter.getLstParentStaffId() != null && !filter.getLstParentStaffId().isEmpty()) {
			sql.append(" and rs.staff_id is not null  ");
			sql.append(" and rs.staff_id in ( ");
			sql.append("  select rps.staff_id from Rparentstaff rps start with rps.parent_staff_id in (-1 ");
			for (Long value : filter.getLstParentStaffId()) {
				sql.append("  , ? ");
				params.add(value);
			}
			sql.append(" ) connect by prior rps.staff_id = rps.parent_staff_id) ");

		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffIdListStr())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStaffIdListStr(), "st.staff_id");
			sql.append(paramsFix);
		}

		sql.append(" order by staffCode, staffName");

		final String[] fieldNames = new String[] { "id", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public Staff getStaffByFilterWithCMS(BasicFilter<Staff> filter) throws DataAccessException {
		if (filter.getInheritUserPriv() == null) {
			return new Staff();
		}
		if (filter.getIntFlag() == null) {
			return new Staff();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from STAFF where 1 = 1 ");
		sql.append("   and staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = ? ) ");
		params.add(filter.getInheritUserPriv());
		if (filter.getParentId() != null) {
			sql.append("   and staff_id not in (select staff_id from exception_user_access where status = 1  and parent_staff_id = ?) ");
			params.add(filter.getParentId());
			sql.append(" and staff_id in ( select psm.staff_id as staff_id from parent_staff_map psm where psm.status = 1 ");
			sql.append(" and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id) ");
			sql.append(" start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id) ");
			params.add(filter.getParentId());
		}

		if (filter.getIntFlag() != 0) {
			sql.append(" and staff_id in ( select psm.staff_id as staff_id from parent_staff_map psm where psm.status = 1 ");
			sql.append(" and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id) ");
			sql.append(" start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id) ");
			params.add(filter.getInheritUserPriv());
		}

		if (filter.getStatus() != null) {
			sql.append(" and status = ?  ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and status = ?  ");
			params.add(ActiveType.RUNNING.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(staff_code) like ?  ");
			params.add(filter.getCode().trim().toLowerCase());
		}
		if (filter.getId() != null) {
			sql.append(" and staff_id = ?  ");
			params.add(filter.getId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(staff_code) like ?  ");
			params.add(filter.getCode().trim().toLowerCase());
		}
		if (filter.getShopId() != null) {
			sql.append(" and shop_id = ?  ");
			params.add(filter.getShopId());
		}
		sql.append(" and staff_type_id in ");
		if (filter.getObjectType() != null) {
			sql.append(" (select channel_type_id from channel_type where status = 1 and type = 2 and object_type = ? ) ");
			params.add(filter.getId());
		} else {
			sql.append(" (select channel_type_id from channel_type where status = 1 and type = 2 )");
		}
		Staff rs = repo.getEntityBySQL(Staff.class, sql.toString(), params);
		return rs;
	}

	@Override
	public boolean checkStaffInCMSByFilterWithCMS(BasicFilter<BasicVO> filter) throws DataAccessException {
		if (filter.getInheritUserPriv() == null) {
			throw new IllegalArgumentException("filter.getInheritUserPriv() is not null");
		}
		if (filter.getShopRootId() == null) {
			throw new IllegalArgumentException("getShopRootId() is not null ");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with Rparentstaff as ( ");
		sql.append("     select psm.staff_id as staff_id from parent_staff_map psm where psm.status = 1 ");
		sql.append("     and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and psm.staff_id = parent_staff_id) ");
		sql.append("     start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id ) ");
		params.add(filter.getInheritUserPriv());
		sql.append(" select count(1) as count from staff st left join Rparentstaff rs on rs.staff_id = st.staff_id  ");
		sql.append("     where 1 = 1 ");
		if (filter.getFlagCMS() != null && filter.getFlagCMS()) {
			sql.append("     and rs.staff_id is not null ");
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append("     and st.staff_code in (?) ");
			params.add(filter.getCode());
		}
		sql.append("     and st.staff_id not in (select staff_id from exception_user_access where status = 1  and st.staff_id = parent_staff_id) ");
		sql.append("     and st.shop_id in ( select shop_id from shop WHERE status = 1 start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
		params.add(filter.getShopRootId());
		if (filter.getStatus() != null) {
			sql.append(" and st.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and st.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getId() != null) {
			sql.append(" and st.staff_id = ? ");
			params.add(filter.getId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(st.staff_code) like ?  ");
			params.add(filter.getCode().trim().toLowerCase());
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public boolean checkParentChildrentByDoubleStaffId(Long parentStaffId, Long childStaffId) throws DataAccessException {
		if (parentStaffId == null) {
			return false;
		}
		if (childStaffId == null) {
			return false;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from parent_staff_map where status = ? and parent_staff_id = ? and staff_id = ? ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(parentStaffId);
		params.add(childStaffId);
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
//	public List<StaffVO> getListGSByStaffRootWithNVBH(StaffPrsmFilter<StaffVO> filter) throws DataAccessException {
//		if (filter.getInheritUserPriv() == null) {
//			throw new IllegalArgumentException("getInheritUserPriv is not null");
//		}
//		if (filter.getShopId() == null) {
//			throw new IllegalArgumentException("getShopId is not null");
//		}
//		StringBuilder sql = new StringBuilder();
//		StringBuilder countSql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//
//		sql.append(" with  Rparentstaff as ( ");
//		sql.append("  select psm.staff_id as staff_id from parent_staff_map psm where psm.status = 1 ");
//		sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and psm.staff_id = parent_staff_id) ");
//		sql.append("  start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id), ");
//		params.add(filter.getInheritUserPriv());
//		sql.append("  MapCMSWithStaffNVBH as ( ");
//		sql.append("  select st.staff_id from staff st join channel_type cn on st.staff_type_id = cn.channel_type_id left join Rparentstaff rs on rs.staff_id = st.staff_id ");
//		sql.append(" where 1 = 1 ");
//		sql.append("  and st.shop_id in (select shop_id from shop WHERE status = 1 start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
//		params.add(filter.getShopId());
//		sql.append("  and cn.status = 1 and cn.type = 2 and cn.object_type in (1, 2) ");
//		sql.append("  ), ");
//		sql.append("  MapCMSWithStaffGS as ( ");
//		sql.append("  select distinct psm.parent_staff_id as staff_id from parent_staff_map psm join ");
//		sql.append("  staff st on psm.parent_staff_id = st.staff_id join channel_type cn on st.staff_type_id = cn.channel_type_id ");
//		sql.append("  where psm.status = 1 and cn.status = 1 and cn.type = 2 and cn.object_type in (5) ");
//		sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id) ");
//		sql.append("  and psm.staff_id in (select * from MapCMSWithStaffNVBH)) ");
//
//		sql.append(" select  ");
//		sql.append(" st.staff_id as id ");
//		sql.append(" ,st.staff_code as staffCode ");
//		sql.append(" ,st.staff_name as staffName ");
//		sql.append(" ,st.address as address ");
//		sql.append(" ,st.shop_id as shopId ");
//		sql.append(" ,(select shop_code from shop where shop_id = st.shop_id ) as shopCode ");
//		sql.append(" ,(select shop_name from shop where shop_id = st.shop_id ) as shopName ");
//		sql.append(" from staff st ");
//		sql.append(" where st.staff_id in(select * from MapCMSWithStaffGS) ");
//		//Append Chuoi From and Where
//		sql.append(" order by st.staff_code ");
//		//Count total
//		countSql.append(" select count(1) count from ( ");
//		countSql.append(sql.toString());
//		countSql.append(" ) ");
//		final String[] fieldNames = new String[] { "id", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName" };
//
//		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
//		if (filter.getkPaging() == null) {
//			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
//		} else {
//			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
//		}
//	}
	public List<StaffVO> getListGSByStaffRootWithNVBH(StaffPrsmFilter<StaffVO> filter) throws DataAccessException {
		if (filter.getInheritUserPriv() == null) {
			throw new IllegalArgumentException("getInheritUserPriv is not null");
		}
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("getShopId is not null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with  Rparentstaff as ( ");
		sql.append("  select psm.staff_id as staff_id from parent_staff_map psm where psm.status = 1 ");
		sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and parent_staff_id = ?) ");
		params.add(filter.getInheritUserPriv());
		sql.append("  start with psm.parent_staff_id = ? and psm.status = 1 connect by prior psm.staff_id = psm.parent_staff_id), ");
		params.add(filter.getInheritUserPriv());
		sql.append("  MapCMSWithStaffNVBH as ( ");
		sql.append("  select st.staff_id from staff st join staff_type cn on st.staff_type_id = cn.staff_type_id left join Rparentstaff rs on rs.staff_id = st.staff_id ");
		sql.append(" where 1 = 1 ");
		sql.append("  and st.shop_id in (select shop_id from shop WHERE 1= 1 ");
		if(Boolean.TRUE.equals(filter.getIsGetShopStopped())){
			sql.append(" and status in (?, ?) ");
			params.add(ActiveType.RUNNING.getValue());
			params.add(ActiveType.STOPPED.getValue());
		}else {
			sql.append(" and status = ? ");
			params.add(ActiveType.RUNNING.getValue());
		}			
		sql.append(" start with shop_id = ? ");
		params.add(filter.getShopId());
		if(Boolean.TRUE.equals(filter.getIsGetShopStopped())){
			sql.append(" and status in (?, ?) ");
			params.add(ActiveType.RUNNING.getValue());
			params.add(ActiveType.STOPPED.getValue());
		}else {
			sql.append(" and status = ? ");
			params.add(ActiveType.RUNNING.getValue());
		}
		sql.append("  connect by prior shop_id = parent_shop_id) ");
		sql.append("  and cn.status = 1 and cn.specific_type in (1) ");
		sql.append("  ), ");
		sql.append("  MapCMSWithStaffGS as ( ");
		sql.append("  select distinct psm.parent_staff_id as staff_id from parent_staff_map psm join ");
		sql.append("  staff st on psm.parent_staff_id = st.staff_id join staff_type cn on st.staff_type_id = cn.staff_type_id ");
		sql.append("  where psm.status = 1 and cn.status = 1 and cn.specific_type in (2 ");
//		if (filter.getGska() != null) {
//			sql.append(" , ? ");
//			params.add(filter.getGska());
//		}
		if (filter.getGsmt() != null) {
			sql.append(" , ? ");
			params.add(filter.getGsmt());
		}
		
		sql.append(" ) ");
		sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and parent_staff_id = ?) ");
		params.add(filter.getInheritUserPriv());
		sql.append("  and psm.staff_id in (select * from MapCMSWithStaffNVBH)) ");

		sql.append(" select  ");
		sql.append(" st.staff_id as id ");
		sql.append(" ,st.staff_code as staffCode ");
		sql.append(" ,st.staff_name as staffName ");
		sql.append(" ,st.address as address ");
		sql.append(" ,st.shop_id as shopId ");
		sql.append(" ,st.phone ");
		sql.append(" ,st.mobilephone mobilePhone");
		sql.append(" ,(select shop_code from shop where shop_id = st.shop_id ) as shopCode ");
		sql.append(" ,(select shop_name from shop where shop_id = st.shop_id ) as shopName ");
		sql.append(" from staff st ");
		sql.append(" where st.staff_id in(select * from MapCMSWithStaffGS) ");
		/** fix: status = 1 cho staff; 01/04/2015*/
		if(filter.getStatus() != null){
			sql.append(" and st.status = ? ");
			params.add(filter.getStatus());
		}
		// Append Chuoi From and Where
		sql.append(" order by st.staff_code ");
		// Count total
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		final String[] fieldNames = new String[] { "id", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName", "phone", "mobilePhone" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	@Override
//	public List<StaffVO> getListGSByStaffRootWithInParentStaffMap(StaffPrsmFilter<StaffVO> filter) throws DataAccessException {
//		if (filter.getInheritUserPriv() == null) {
//			throw new IllegalArgumentException("getInheritUserPriv is not null");
//		}
//		if (filter.getShopId() == null) {
//			throw new IllegalArgumentException("getShopId is not null");
//		}
//		StringBuilder sql = new StringBuilder();
//		StringBuilder countSql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//
//		sql.append(" with  Rparentstaff as ( ");
//		sql.append("  select psm.staff_id as staff_id from parent_staff_map psm where psm.status = 1 ");
//		sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and psm.staff_id = parent_staff_id) ");
//		sql.append("  start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id), ");
//		params.add(filter.getInheritUserPriv());
//		sql.append("  MapCMSWithStaffNVBH as ( ");
//		sql.append("  select st.staff_id from staff st join channel_type cn on st.staff_type_id = cn.channel_type_id left join Rparentstaff rs on rs.staff_id = st.staff_id ");
//		sql.append(" where 1 = 1 ");
//		sql.append("  and st.shop_id in (select shop_id from shop WHERE status = 1 start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
//		params.add(filter.getShopId());
//		sql.append("  and cn.status = 1 and cn.type = 2 and cn.object_type in (1, 2) ");
//		sql.append("  ), ");
//		sql.append("  MapCMSWithStaffGS as ( ");
//		sql.append("  select distinct psm.parent_staff_id as staff_id from parent_staff_map psm join ");
//		sql.append("  staff st on psm.parent_staff_id = st.staff_id join channel_type cn on st.staff_type_id = cn.channel_type_id ");
//		sql.append("  where psm.status = 1 and cn.status = 1 and cn.type = 2 and cn.object_type in (5) ");
//		sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id) ");
//		sql.append("  and psm.staff_id in (select * from MapCMSWithStaffNVBH) ) ");
//		sql.append(" select  ");
//		sql.append(" st.staff_id as id ");
//		sql.append(" ,st.staff_code as staffCode ");
//		sql.append(" ,st.staff_name as staffName ");
//		sql.append(" ,st.address as address ");
//		sql.append(" ,st.shop_id as shopId ");
//		sql.append(" ,(select shop_code from shop where shop_id = st.shop_id ) as shopCode ");
//		sql.append(" ,(select shop_name from shop where shop_id = st.shop_id ) as shopName ");
//		sql.append(" from staff st ");
//		sql.append(" where st.staff_id in(select * from MapCMSWithStaffGS) ");
//		//Append Chuoi From and Where
//		sql.append(" order by st.staff_code ");
//		//Count total
//		countSql.append(" select count(1) count from ( ");
//		countSql.append(sql.toString());
//		countSql.append(" ) ");
//		final String[] fieldNames = new String[] { "id", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName" };
//
//		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
//		if (filter.getkPaging() == null) {
//			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
//		} else {
//			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
//		}
//	}
	public List<StaffVO> getListGSByStaffRootWithInParentStaffMap(StaffPrsmFilter<StaffVO> filter) throws DataAccessException {
		if (filter.getInheritUserPriv() == null) {
			throw new IllegalArgumentException("getInheritUserPriv is not null");
		}
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("getShopId is not null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with  Rparentstaff as ( ");
		sql.append("  (select psm.staff_id as staff_id from parent_staff_map psm where psm.status = 1 ");
		sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and parent_staff_id = ?) ");
		sql.append("  start with psm.parent_staff_id = ? and psm.status = 1  connect by prior psm.staff_id = psm.parent_staff_id) union (SELECT staff_id FROM staff st WHERE st.staff_id = ?)), ");
		params.add(filter.getInheritUserPriv());
		params.add(filter.getInheritUserPriv());
		params.add(filter.getInheritUserPriv());
		sql.append("  MapCMSWithStaffNVBH as ( ");
		sql.append("  select st.staff_id from staff st join staff_type cn on st.staff_type_id = cn.staff_type_id left join Rparentstaff rs on rs.staff_id = st.staff_id ");
		sql.append(" where 1 = 1 ");
		sql.append("  and st.shop_id in (select shop_id from shop WHERE status = 1 start with shop_id = ? and status = 1  connect by prior shop_id = parent_shop_id) ");
		params.add(filter.getShopId());
		sql.append("  and cn.status = 1 and cn.specific_type in (1) ");
		sql.append("  ), ");
		sql.append("  MapCMSWithStaffGS as ( ");
		sql.append("  select distinct psm.parent_staff_id as staff_id from parent_staff_map psm join ");
		sql.append("  staff st on psm.parent_staff_id = st.staff_id join staff_type cn on st.staff_type_id = cn.staff_type_id ");
		sql.append("  where psm.status = 1 and cn.status = 1 and cn.specific_type in (2) ");
		sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and parent_staff_id = ?) ");
		params.add(filter.getInheritUserPriv());
		sql.append("  and psm.staff_id in (select * from MapCMSWithStaffNVBH) ) ");
		sql.append(" select  ");
		sql.append(" st.staff_id as id ");
		sql.append(" ,st.staff_code as staffCode ");
		sql.append(" ,st.staff_name as staffName ");
		sql.append(" ,st.address as address ");
		sql.append(" ,st.shop_id as shopId ");
		sql.append(" ,st.phone ");
		sql.append(" ,st.MOBILEPHONE mobilePhone");
		sql.append(" ,(select shop_code from shop where shop_id = st.shop_id ) as shopCode ");
		sql.append(" ,(select shop_name from shop where shop_id = st.shop_id ) as shopName ");
		sql.append(" from staff st ");
		sql.append(" where st.staff_id in(select * from MapCMSWithStaffGS) ");
		if (filter.getIsLstChildStaffRoot() != null && filter.getIsLstChildStaffRoot()) {
			sql.append("  and st.staff_id in (select * from Rparentstaff) ");
		}
//		sql.append("  and st.shop_id in (select shop_id from shop WHERE status = 1 start with shop_id = ? and status = 1 connect by prior shop_id = parent_shop_id) ");
//		params.add(filter.getShopId());
		/** fix: status = 1 cho staff; 01/04/2015*/
		if(filter.getStatus() != null){
			sql.append(" and st.status = ? ");
			params.add(filter.getStatus());
		}
		// Append Chuoi From and Where
		sql.append(" order by st.staff_code ");
		// Count total
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		final String[] fieldNames = new String[] { "id", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName", "phone", "mobilePhone" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	
	@Override
	public List<StaffVO> getListGSByStaffRootKAMT(StaffPrsmFilter<StaffVO> filter) throws DataAccessException {
		if (filter.getInheritUserPriv() == null) {
			throw new IllegalArgumentException("getInheritUserPriv is not null");
		}
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("getShopId is not null");
		}
		
		StringBuffer sql = new StringBuffer();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("	SELECT	");
		sql.append("	    st.staff_id AS id ,	");
		sql.append("	    st.staff_code    AS staffCode ,	");
		sql.append("	    st.staff_name    AS staffName ,	");
		sql.append("	    st.address       AS address ,	");
		sql.append("	    st.shop_id       AS shopId ,	");
		sql.append("	    st.phone,	");
		sql.append("	    st.mobilephone mobilePhone,	");
		sql.append("	    (SELECT	");
		sql.append("	        shop_code	");
		sql.append("	    FROM	");
		sql.append("	        shop	");
		sql.append("	    WHERE	");
		sql.append("	        shop_id = st.shop_id    ) AS shopCode ,	");
		sql.append("	    (SELECT	");
		sql.append("	        shop_name	");
		sql.append("	    FROM	");
		sql.append("	        shop	");
		sql.append("	    WHERE	");
		sql.append("	        shop_id = st.shop_id    ) AS shopName	");
		sql.append("	FROM	");
		sql.append("	    staff st	");
		sql.append("	JOIN	");
		sql.append("	    staff_type cn	");
		sql.append("	        ON st.staff_type_id = cn.staff_type_id	");
		sql.append("	JOIN	");
		sql.append("	    MAP_USER_SHOP MUS	");
		sql.append("	        ON st.staff_id = mus.user_id	");
		sql.append("	WHERE	");
		sql.append("	    st.status = 1	");
		sql.append("	    and mus.from_date < trunc(sysdate) + 1 ");
		sql.append("	    and (mus.to_date is null or mus.to_date >= trunc(sysdate)) ");
		sql.append("	    AND cn.status = 1	");
		if (filter.getLstObjectType() != null && filter.getLstObjectType().size() > 0) {
			sql.append(" and cn.specific_type in (-1  ");
			for (int i = 0, isize = filter.getLstObjectType().size(); i < isize; i++) {
				Integer objectType = filter.getLstObjectType().get(i);
				if (objectType != null) {
					sql.append(", ?  ");
					params.add(objectType);
				}

			}
			sql.append(" ) ");
		}
		sql.append("	    AND mus.shop_id in (	");
		sql.append("	        SELECT	");
		sql.append("	            s1.shop_id	");
		sql.append("	        FROM	");
		sql.append("	            shop s1	");
		sql.append("	        WHERE	");
		sql.append("	            1 = 1	");
		sql.append("	            AND s1.status = 1  START WITH s1.SHOP_ID = ?  CONNECT	");
		sql.append("	        BY	");
		sql.append("	            PRIOR s1.shop_id = s1.parent_shop_id	");
		sql.append("	    )     ");
		params.add(filter.getShopId());
		
		// Count total
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");

		sql.append(" order by staffCode ");
		final String[] fieldNames = new String[] { "id", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName", "phone", "mobilePhone" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
//	@Override
//	public List<StaffVO> getListGSByStaffRootKAMT(StaffPrsmFilter<StaffVO> filter) throws DataAccessException {
//		if (filter.getInheritUserPriv() == null) {
//			throw new IllegalArgumentException("getInheritUserPriv is not null");
//		}
//		if (filter.getShopId() == null) {
//			throw new IllegalArgumentException("getShopId is not null");
//		}
//		StringBuilder sql = new StringBuilder();
//		StringBuilder countSql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		
//		sql.append("SELECT st.staff_id AS id , ");
//		sql.append("   st.staff_code    AS staffCode , ");
//		sql.append("   st.staff_name    AS staffName , ");
//		sql.append("   st.address       AS address , ");
//		sql.append("   st.shop_id       AS shopId , ");
//		sql.append("   st.phone, ");
//		sql.append("   st.mobilephone mobilePhone, ");
//		sql.append("   (SELECT shop_code FROM shop WHERE shop_id = st.shop_id ");
//		sql.append("   ) AS shopCode , ");
//		sql.append("   (SELECT shop_name FROM shop WHERE shop_id = st.shop_id ");
//		sql.append("   ) AS shopName ");
//		sql.append(" FROM staff st ");
//		sql.append(" join staff_type cn on st.staff_type_id = cn.staff_type_id ");
//		sql.append(" where st.status = 1 and cn.status = 1 ");
////		sql.append(" and cn.type = 2 ");
//		if (filter.getLstObjectType() != null && filter.getLstObjectType().size() > 0) {
//			sql.append(" and cn.specific_type in (-1  ");
//			for (int i = 0, isize = filter.getLstObjectType().size(); i < isize; i++) {
//				Integer objectType = filter.getLstObjectType().get(i);
//				if (objectType != null) {
//					sql.append(", ?  ");
//					params.add(objectType);
//				}
//				
//			}
//			sql.append(" ) ");
//		}
//		
//		sql.append(" and st.staff_id not in (select staff_id from exception_user_access where status = 1 and ? = parent_staff_id) ");
//		params.add(filter.getInheritUserPriv());
//		sql.append(" and staff_id in ( ");
//		sql.append("   select ru.user_id from role_user ru where ru.status = 1 ");
//		sql.append("   and ru.role_id in ( ");
//		sql.append("     select r.role_id from role_permission_map rpm join role r on rpm.role_id = r.role_id where rpm.status =  1 and r.status = 1 ");
//		sql.append("     and rpm.permission_id in ( ");
//		sql.append("       select ps.permission_id from permission ps join org_access org on org.permission_id = ps.permission_id ");
//		sql.append("       where org.status = 1 and ps.status = 1 ");
//		sql.append(" AND org.shop_id in ( ");
//		sql.append(" select s1.shop_id ");
//		sql.append(" from shop s1 ");
//		sql.append(" where 1 = 1 ");
//		if(Boolean.TRUE.equals(filter.getIsGetShopStopped())){
//			sql.append(" and s1.status in (?, ?) ");
//			params.add(ActiveType.RUNNING.getValue());
//			params.add(ActiveType.STOPPED.getValue());
//		}else {
//			sql.append(" and s1.status = ? ");
//			params.add(ActiveType.RUNNING.getValue());
//		}
//		sql.append(" START WITH s1.SHOP_ID = ? ");
//		params.add(filter.getShopId());
//		sql.append(" CONNECT BY PRIOR s1.shop_id = s1.parent_shop_id) ");
//		sql.append("     ) ");
//		sql.append("   ) ");
//		sql.append(" ) ");
//		sql.append(" union ");
//		sql.append(" SELECT st.staff_id AS id , ");
//		sql.append("   st.staff_code    AS staffCode , ");
//		sql.append("   st.staff_name    AS staffName , ");
//		sql.append("   st.address       AS address , ");
//		sql.append("   st.shop_id       AS shopId , ");
//		sql.append("   st.phone, ");
//		sql.append("   st.mobilephone mobilePhone, ");
//		sql.append("   (SELECT shop_code FROM shop WHERE shop_id = st.shop_id ");
//		sql.append("   ) AS shopCode , ");
//		sql.append("   (SELECT shop_name FROM shop WHERE shop_id = st.shop_id ");
//		sql.append("   ) AS shopName ");
//		sql.append(" FROM staff st ");
//		sql.append(" join staff_type cn on st.staff_type_id = cn.staff_type_id ");
//		sql.append(" where 1=1 ");
//		sql.append(" and st.status = 1 ");
////		sql.append(" and cn.type = 2 ");
//		if (filter.getLstObjectType() != null && filter.getLstObjectType().size() > 0) {
//			sql.append(" and cn.specific_type in (-1  ");
//			for (int i = 0, isize = filter.getLstObjectType().size(); i < isize; i++) {
//				Integer objectType = filter.getLstObjectType().get(i);
//				if (objectType != null) {
//					sql.append(", ?  ");
//					params.add(objectType);
//				}
//				
//			}
//			sql.append(" ) ");
//		}
//		sql.append(" and st.staff_id not in (select staff_id from exception_user_access where status = 1 and ? = parent_staff_id)");
//		params.add(filter.getInheritUserPriv());
//		sql.append(" AND st.shop_id in ( ");
//		sql.append(" select s1.shop_id ");
//		sql.append(" from shop s1 ");
//		sql.append(" where 1 = 1 ");
//		if(Boolean.TRUE.equals(filter.getIsGetShopStopped())){
//			sql.append(" and s1.status in (?, ?) ");
//			params.add(ActiveType.RUNNING.getValue());
//			params.add(ActiveType.STOPPED.getValue());
//		}else {
//			sql.append(" and s1.status = ? ");
//			params.add(ActiveType.RUNNING.getValue());
//		}
//		sql.append(" START WITH s1.SHOP_ID = ? ");
//		params.add(filter.getShopId());
//		sql.append(" CONNECT BY PRIOR s1.shop_id = s1.parent_shop_id) ");
//		
//		// Count total
//		countSql.append(" select count(1) count from ( ");
//		countSql.append(sql.toString());
//		countSql.append(" ) ");
//		
//		sql.append(" order by staffCode ");
//		final String[] fieldNames = new String[] { "id", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName", "phone", "mobilePhone" };
//		
//		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
//				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
//		if (filter.getkPaging() == null) {
//			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
//		} else {
//			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
//		}
//	}
	
	@Override
	public List<StaffVO> getListGSByFilter(StaffPrsmFilter<StaffVO> filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null");
		}
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("filter.getUserId() is null");
		}
		if (filter.getRoleId() == null) {
			throw new IllegalArgumentException("filter.getRoleId() is null");
		}
		StringBuffer sql = new StringBuffer();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("	SELECT	");
		sql.append("	    st.staff_id AS id ,	");
		sql.append("	    st.staff_code    AS staffCode ,	");
		sql.append("	    st.staff_name    AS staffName ,	");
		sql.append("	    st.address       AS address ,	");
		sql.append("	    st.shop_id       AS shopId ,	");
		sql.append("	    st.phone,	");
		sql.append("	    st.mobilephone mobilePhone,	");
		sql.append("	    sh.shop_code AS shopCode ,	");
		sql.append("	    sh.shop_name AS shopName	");
		sql.append(" from map_user_staff mus ");
		sql.append(" join staff st on st.staff_id = mus.staff_id ");
		sql.append(" join shop sh on sh.shop_id = st.shop_id ");
		sql.append(" where mus.from_date < trunc(sysdate) + 1 ");
		sql.append(" and (mus.to_date is null or mus.to_date >= trunc(sysdate)) ");
		sql.append(" and mus.user_id = ? ");
		params.add(filter.getUserId());
		sql.append(" and mus.role_id = ? ");
		params.add(filter.getRoleId());
		if (filter.getStatus() != null) {
			sql.append(" and st.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and st.status in (?, ?) ");
			params.add(ActiveType.RUNNING.getValue());
			params.add(ActiveType.STOPPED.getValue());
		}
		if (filter.getLstObjectType() != null && filter.getLstObjectType().size() > 0) {
			sql.append(" and mus.specific_type in (-1  ");
			for (int i = 0, isize = filter.getLstObjectType().size(); i < isize; i++) {
				Integer objectType = filter.getLstObjectType().get(i);
				if (objectType != null) {
					sql.append(", ?  ");
					params.add(objectType);
				}

			}
			sql.append(" ) ");
		}
		if (filter.getStaffId() != null) {
			sql.append(" and st.staff_id = ? ");
			params.add(filter.getStaffId());
		}
		if (filter.getShopId() != null) {
			sql.append(" and sh.shop_id in ( ");
			sql.append(" select shop_id from shop sh ");
			sql.append(" where sh.status = 1 ");
			sql.append(" start with sh.shop_id = ? ");
			sql.append(" connect by prior sh.shop_id = sh.parent_shop_id) ");
			params.add(filter.getShopId());
		}
		//union Giam sat current
		sql.append(" union ");
		sql.append(" ( ");
		sql.append(" SELECT st.staff_id AS id , ");
		sql.append(" st.staff_code AS staffCode , ");
		sql.append(" st.staff_name AS staffName , ");
		sql.append(" st.address AS address , ");
		sql.append(" st.shop_id AS shopId , ");
		sql.append(" st.phone, ");
		sql.append(" st.mobilephone mobilePhone, ");
		sql.append(" sh.shop_code AS shopCode , ");
		sql.append(" sh.shop_name as shopname ");
		sql.append(" from staff st ");
		sql.append(" join shop sh on sh.shop_id = st.shop_id ");
		sql.append(" join staff_type stype on stype.staff_type_id = st.staff_type_id ");
		sql.append(" where st.staff_id = ? ");
		params.add(filter.getUserId());
		sql.append(" and sh.shop_id = ? ");
		params.add(filter.getShopId());
		if (filter.getLstObjectType() != null && filter.getLstObjectType().size() > 0) {
			sql.append(" and stype.specific_type in (-1  ");
			for (int i = 0, isize = filter.getLstObjectType().size(); i < isize; i++) {
				Integer objectType = filter.getLstObjectType().get(i);
				if (objectType != null) {
					sql.append(", ?  ");
					params.add(objectType);
				}

			}
			sql.append(" ) ");
		}
		sql.append(" ) ");
		
		// Count total
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");

		sql.append(" order by staffCode ");
		final String[] fieldNames = new String[] { "id", "staffCode", "staffName", "address", "shopId", "shopCode", "shopName", "phone", "mobilePhone" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
}
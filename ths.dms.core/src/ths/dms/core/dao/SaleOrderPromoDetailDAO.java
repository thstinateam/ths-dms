package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.SaleOrderPromotion;
import ths.dms.core.entities.filter.SaleOrderPromoDetailFilter;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderPromotionDetailVO;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Truy xuat du lieu bang sale_order_promo_detail
 * 
 * @author lacnv1
 * @since Sep 15, 2014
 */
public interface SaleOrderPromoDetailDAO {

	/**
	 * Tao ds sale_order_promo_detail
	 * 
	 * @author lacnv1
	 * @since Sep 15, 2014
	 */
	void createListSaleOrderPromoDetail(List<SaleOrderPromoDetail> lstPromo) throws DataAccessException;
	
	/**
	 * Lay ds sale_order_promo_detail theo sale_order_detail_id
	 * 
	 * @author lacnv1
	 * @since Sep 15, 2014
	 */
	List<SaleOrderPromoDetail> getListSaleOrderPromoByDetailId(long soDetailId) throws DataAccessException;
	
	/**
	 * Xoa sale_order_promo_detail
	 * 
	 * @author lacnv1
	 * @since Sep 30, 2014
	 */
	void deleteSaleOrderPromoDetail(SaleOrderPromoDetail promo) throws DataAccessException;
	
	/**
	 * Lay ds sale_order_promotion theo sale_order_id
	 * 
	 * @author lacnv1
	 * @since Oct 06, 2014
	 */
	List<SaleOrderPromotion> getListSaleOrderPromotionByOrderId(long saleOrderId) throws DataAccessException;
	
	/**
	 * Tao ds sale_order_promotion
	 * 
	 * @author lacnv1
	 * @since Oct 06, 2014
	 */
	void createListSaleOrderPromotion(List<SaleOrderPromotion> lstPromo) throws DataAccessException;
	
	/**
	 * Xoa sale_order_promotion
	 * 
	 * @author lacnv1
	 * @since Oct 06, 2014
	 */
	void deleteSaleOrderPromotion(SaleOrderPromotion promo) throws DataAccessException;
	
	/**
	 *Get list sale order promo detail theo sale order va type
	 * 
	 * @author nhanlt6
	 * @since Oct 22, 2014
	 */
	List<SaleOrderDetailVOEx> getListSaleOrderPromoDetailForOrder(Long orderId, Integer promotionType) throws DataAccessException;

	/**
	 * @author trietptm
	 * @param filter
	 * @return	danh sach SaleOrderPromotionDetailVO theo filter
	 * @throws DataAccessException
	 * @since Aug 10, 2015
	 */
	List<SaleOrderPromotionDetailVO> getListSaleOrderPromoByFilter(SaleOrderPromoDetailFilter filter) throws DataAccessException;

	/**
	 * @author trietptm
	 * @param filter
	 * @return danh sach SaleOrderPromoDetail theo filter
	 * @throws DataAccessException
	 * @since Aug 21, 2015
	 */
	List<SaleOrderPromoDetail> getListSaleOrderPromoDetailByFilter(SaleOrderPromoDetailFilter filter) throws DataAccessException;
}
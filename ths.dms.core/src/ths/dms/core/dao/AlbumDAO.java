package ths.dms.core.dao;

import ths.dms.core.entities.MtMediaAlbum;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface AlbumDAO {

	MtMediaAlbum createMtMediaAlbum(MtMediaAlbum mtMediaAlbum, LogInfoVO logInfo) throws DataAccessException;

	MtMediaAlbum getMtMediaAlbumById(Long id) throws DataAccessException;

	void updateMtMediaAlbum(MtMediaAlbum mtMediaAlbum, LogInfoVO logInfo) throws DataAccessException;

	MtMediaAlbum getMtMediaAlbumByCode(String code) throws DataAccessException;

}

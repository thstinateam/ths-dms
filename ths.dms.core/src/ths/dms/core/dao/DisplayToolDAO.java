package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.DisplayTool;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.DisplayToolCustomerFilter;
import ths.dms.core.entities.filter.DisplayToolFilter;
import ths.dms.core.entities.vo.DisplayToolCustomerVO;
import ths.dms.core.entities.vo.DisplayToolVO;
import ths.dms.core.entities.vo.StaffSimpleVO;
import ths.dms.core.exceptions.DataAccessException;

public interface DisplayToolDAO {

	DisplayTool createDisplayTool(DisplayTool displayTool) throws DataAccessException;

	void deleteDisplayTool(DisplayTool displayTool) throws DataAccessException;

	void updateDisplayTool(DisplayTool displayTool) throws DataAccessException;
	
	DisplayTool getDisplayToolById(long id) throws DataAccessException;

	List<DisplayToolVO> getListDisplayTool(DisplayToolFilter filter) throws DataAccessException;

	List<Product> getListProductOfDisplayTool(KPaging<Product> kPaging, Long displayToolId) throws DataAccessException;

	List<DisplayToolVO> getListDisplayToolOfProduct(DisplayToolFilter filter) throws DataAccessException;

	List<DisplayToolCustomerVO> getListDisplayToolCustomerVo(KPaging<DisplayToolCustomerVO> kPaging,DisplayToolCustomerFilter filter) throws DataAccessException;
	
	List<DisplayTool> checkListDisplayTool(List<DisplayTool> list)throws DataAccessException;
	
	void createListDisplayTool(List<DisplayTool> list)throws DataAccessException;
	
	public void deleteListDisplayTools(List<Long> listId)	throws DataAccessException;
	
	Boolean checkDateMonthToDay(Long shopId,Long staffId,String month) throws DataAccessException;
	
	List<DisplayTool> getListDisplayTool(KPaging<DisplayTool> kPaging,DisplayToolFilter filter) throws DataAccessException;

	/* Sao chep nhieu khach hang muon tu cua nhieu nhan vien */
	List<StaffSimpleVO> getListDisplayToolStaffId(DisplayToolFilter filter) throws DataAccessException;
	
}

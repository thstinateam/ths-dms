package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Bank;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.BankFilter;
import ths.dms.core.entities.vo.BankVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface BankDAO {

	Bank createBank(Bank bank, LogInfoVO logInfo) throws DataAccessException;

	void deleteBank(Bank bank, LogInfoVO logInfo) throws DataAccessException;

	void updateBank(Bank bank, LogInfoVO logInfo) throws DataAccessException;
	
	Bank getBankById(long id) throws DataAccessException;

	Bank getBankByCode(String codeBank) throws DataAccessException;

	List<Bank> getListBankByShop(Long shopId, ActiveType status) throws DataAccessException;

	List<Bank> getListBankByShopAscBankName(Long shopId) throws DataAccessException;
	
	Bank getBankByCodeAndShop(Long shopId, String Code)
			throws DataAccessException;
	
	List<BankVO> getListBank(KPaging<BankVO> kPaging, BankFilter filter) throws DataAccessException;

}

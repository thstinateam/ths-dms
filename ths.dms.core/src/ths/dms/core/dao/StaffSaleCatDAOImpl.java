package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StaffSaleCat;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class StaffSaleCatDAOImpl implements StaffSaleCatDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Override
	public StaffSaleCat createStaffSaleCat(StaffSaleCat staffSaleCat, LogInfoVO logInfo) throws DataAccessException {
		if (staffSaleCat == null) {
			throw new IllegalArgumentException("staffSaleCat");
		}
		if (staffSaleCat.getCat() != null && staffSaleCat.getStaff() != null) {
			if (checkIfRecordExist(staffSaleCat.getStaff().getId(), staffSaleCat.getCat().getId(), null))
				return null;
		}
		else 
			return null;
		repo.create(staffSaleCat);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, staffSaleCat, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(StaffSaleCat.class, staffSaleCat.getId());
	}

	@Override
	public void deleteStaffSaleCat(StaffSaleCat staffSaleCat, LogInfoVO logInfo) throws DataAccessException {
		if (staffSaleCat == null) {
			throw new IllegalArgumentException("staffSaleCat");
		}
		repo.delete(staffSaleCat);
		try {
			actionGeneralLogDAO.createActionGeneralLog(staffSaleCat, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public StaffSaleCat getStaffSaleCatById(long id) throws DataAccessException {
		return repo.getEntityById(StaffSaleCat.class, id);
	}
	
	@Override
	public void updateStaffSaleCat(StaffSaleCat staffSaleCat, LogInfoVO logInfo) throws DataAccessException {
		if (staffSaleCat == null) {
			throw new IllegalArgumentException("staffSaleCat");
		}
		if (staffSaleCat.getCat() != null && staffSaleCat.getStaff() != null) {
			if (checkIfRecordExist(staffSaleCat.getStaff().getId(), staffSaleCat.getCat().getId(), staffSaleCat.getId()))
				return ;
		}
		else 
			return ;
		StaffSaleCat temp = this.getStaffSaleCatById(staffSaleCat.getId());
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, staffSaleCat, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		repo.update(staffSaleCat);
	}
	
	@Override
	public List<StaffSaleCat> getListStaffSaleCat(KPaging<StaffSaleCat> kPaging, Long staffId)
				throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		sql.append("select ss.* from staff_sale_cat ss join product_info pi on ss.cat_id = pi.product_info_id where 1 = 1");
		List<Object> params = new ArrayList<Object>();
		if (staffId != null) {
			sql.append(" and staff_id=?");
			params.add(staffId);
		}
		sql.append(" order by pi.product_info_code");
		if (kPaging == null)
			return repo.getListBySQL(StaffSaleCat.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StaffSaleCat.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public Boolean checkIfRecordExist(long staffId, Long catId, Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(1) as count from staff_sale_cat p ");
		sql.append(" where staff_id=? and cat_id=?");
		
		List<Object> params = new ArrayList<Object>();
		params.add(staffId);
		params.add(catId);
		
		if (id != null) {
			sql.append(" and staff_sale_cat_id != ?");
			params.add(id);
		}
		
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true; 
	}
	
	@Override
	public StaffSaleCat getStaffSaleCat(Long staffId, Long productInfoId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from staff_sale_cat p ");
		sql.append(" where staff_id=? and cat_id=?");
		
		List<Object> params = new ArrayList<Object>();
		params.add(staffId);
		params.add(productInfoId);
		return repo.getEntityBySQL(StaffSaleCat.class, sql.toString(), params);
	}
	
}

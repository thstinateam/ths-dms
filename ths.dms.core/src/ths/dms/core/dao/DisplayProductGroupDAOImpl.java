package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StDisplayPdGroup;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class DisplayProductGroupDAOImpl implements DisplayProductGroupDAO {

	@Autowired
	private IRepository repo;
	
//	@Autowired
//	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public StDisplayPdGroup createDisplayProductGroup(StDisplayPdGroup displayProductGroup, LogInfoVO logInfo) throws DataAccessException {
		if (displayProductGroup == null) {
			throw new IllegalArgumentException("displayProductGroup is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (displayProductGroup.getDisplayProductGroupCode() != null)
			displayProductGroup.setDisplayProductGroupCode(displayProductGroup.getDisplayProductGroupCode().toUpperCase());
		displayProductGroup.setCreateUser(logInfo.getStaffCode());
		repo.create(displayProductGroup);
//		try {
//			actionGeneralLogDAO.createActionGeneralLog(null, displayProductGroup, logInfo);
//		}
//		catch (Exception ex) {
//			throw new DataAccessException(ex);
//		}
		return repo.getEntityById(StDisplayPdGroup.class, displayProductGroup.getId());
	}

	@Override
	public void deleteDisplayProductGroup(StDisplayPdGroup displayProductGroup, LogInfoVO logInfo) throws DataAccessException {
		if (displayProductGroup == null) {
			throw new IllegalArgumentException("displayProductGroup is null");
		}
		repo.delete(displayProductGroup);
//		try {
//			actionGeneralLogDAO.createActionGeneralLog(displayProductGroup, null, logInfo);
//		}
//		catch (Exception ex) {
//			throw new DataAccessException(ex);
//		}
	}

	@Override
	public StDisplayPdGroup getDisplayProductGroupById(long id) throws DataAccessException {
		return repo.getEntityById(StDisplayPdGroup.class, id);
	}
	
	@Override
	public void updateDisplayProductGroup(StDisplayPdGroup displayProductGroup, LogInfoVO logInfo) throws DataAccessException {
		if (displayProductGroup == null) {
			throw new IllegalArgumentException("displayProductGroup is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (displayProductGroup.getDisplayProductGroupCode() != null)
			displayProductGroup.setDisplayProductGroupCode(displayProductGroup.getDisplayProductGroupCode().toUpperCase());
		StDisplayPdGroup temp = this.getDisplayProductGroupById(displayProductGroup.getId());
		displayProductGroup.setUpdateDate(commonDAO.getSysDate());
		displayProductGroup.setUpdateUser(logInfo.getStaffCode());
		temp = temp.clone();
		repo.update(displayProductGroup);
	}
	
	@Override
	public StDisplayPdGroup getDisplayProductGroupByCode(Long displayProgramId,DisplayProductGroupType type, String displayProductGroupCode) throws DataAccessException{
		if (displayProgramId == null) {
			throw new IllegalArgumentException("displayProgramId is null");
		}
		if (type == null) {
			throw new IllegalArgumentException("type is null");
		}
		if (StringUtility.isNullOrEmpty(displayProductGroupCode)) {
			throw new IllegalArgumentException("displayProductGroupCode is null");
		}
		displayProductGroupCode = displayProductGroupCode.toUpperCase();
		String sql = "select * from display_product_group where display_program_id = ? and type = ? and display_dp_group_code =? and status =? ";
		List<Object> params = new ArrayList<Object>();
		params.add(displayProgramId);
		params.add(type.getValue());
		params.add(displayProductGroupCode.toUpperCase());
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(StDisplayPdGroup.class, sql, params);
	}
	
	@Override
	public List<StDisplayPdGroup> getListDisplayProductGroup( KPaging<StDisplayPdGroup> kPaging, 
			Long displayProgramId,DisplayProductGroupType type , ActiveType status,Boolean isAll) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from display_product_group where 1 = 1 ");
		if (status != null) {
			sql.append(" and status = ?");
			params.add(status.getValue());
		}
		if(type != null) {
			sql.append(" and type = ?");
			params.add(type.getValue());
		}else if(isAll!=null && isAll) {
			sql.append(" and type in (1,2,3,4) ");			
		}else if(isAll!=null && !isAll){
			sql.append(" and type in (1,2) ");
		}
		if (displayProgramId != null) {
			sql.append(" and display_program_id = ?");
			params.add(displayProgramId);
		}
		sql.append(" order by display_dp_group_code");
		if (kPaging == null)
			return repo.getListBySQL(StDisplayPdGroup.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StDisplayPdGroup.class, sql.toString(), params, kPaging);
	}
	
	/**
	 * @author loctt
	 * @since 25sep 2013
	 */
	@Override
	public List<StDisplayPdGroup> getListLevelProductGroup( KPaging<StDisplayPdGroup> kPaging, 
			Long displayProgramId,DisplayProductGroupType type , ActiveType status,Long levelId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (type == DisplayProductGroupType.CB) {
			sql.append(" select distinct sg.* FROM display_product_group sg, display_product_group_dtl sgl ");
			sql.append(" LEFT JOIN display_pl_dp_dtl sd ON sgl.display_product_group_dtl_id = sd.display_product_group_dtl_id ");
			sql.append(" where sg.display_product_group_id = sgl.display_product_group_id ");
			if (status != null) {
				sql.append(" and sg.status = ?");
				params.add(status.getValue());
			}
				sql.append(" and sg.type = ?");
				params.add(DisplayProductGroupType.CB.getValue());
			
		} else {
			sql.append("SELECT distinct sg.* FROM display_product_group sg LEFT JOIN DISPLAY_PL_AMT_DTL dpad ON sg.display_product_group_id = dpad.display_product_group_id  ");
			if(levelId!=null && levelId>0){
				sql.append(" and dpad.display_program_level_id = ?");
				params.add(levelId);
			}
			sql.append(" WHERE (sg.type = 1 or sg.type = 2 or sg.type = 3)");
			if (status != null) {
				sql.append(" and sg.status = ?");
				params.add(status.getValue());
			}
			
		}		
		if (displayProgramId != null) {
			sql.append(" and sg.display_program_id = ?");
			params.add(displayProgramId);
		}
		if (kPaging == null)
			return repo.getListBySQL(StDisplayPdGroup.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StDisplayPdGroup.class, sql.toString(), params, kPaging);
	}
}
package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.ProductLotVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class ProductLotDAOImpl implements ProductLotDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private CommonDAO commonDAO;
	@Autowired
	private ApParamDAO apParamDAO;
	@Autowired
	private ShopLockDAO shopLockDAO;
	
	@Override
	public ProductLot createProductLot(ProductLot productLot)
			throws DataAccessException {
		if (productLot == null) {
			throw new IllegalArgumentException("productLot");
		}
		repo.create(productLot);
		return repo.getEntityById(ProductLot.class, productLot.getId());
	}
	
	@Override
	public void createListProductLot(List<ProductLot> listProductLot)
			throws DataAccessException {
		if (listProductLot == null) {
			throw new IllegalArgumentException("listProductLot");
		}
		repo.create(listProductLot);
	}

	@Override
	public void deleteProductLot(ProductLot productLot)
			throws DataAccessException {
		if (productLot == null) {
			throw new IllegalArgumentException("productLot");
		}
		repo.delete(productLot);
	}

	@Override
	public ProductLot getProductLotById(long id) throws DataAccessException {
		return repo.getEntityById(ProductLot.class, id);
	}

	@Override
	public void updateProductLot(ProductLot productLot)
			throws DataAccessException {
		if (productLot == null) {
			throw new IllegalArgumentException("productLot");
		}
		repo.update(productLot);
	}
	
	@Override
	public void updateListProductLot(List<ProductLot> listProductLot)
			throws DataAccessException {
		if (listProductLot == null) {
			throw new IllegalArgumentException("listProductLot");
		}
		repo.update(listProductLot);
	}

	@Override
	public Boolean isUsingByOthers(Long productLotId)
			throws DataAccessException {
//		StringBuilder sql = new StringBuilder();
//		sql.append(" select count(1) as count from product_lot  ");
//		sql.append(" where  ");
//		sql.append("   exists (select 1 from stock_trans_lot where product_lot_id=?) ");
//		List<Object> params = new ArrayList<Object>();
//		params.add(productLotId);
//		int c = repo.countBySQL(sql.toString(), params);
//		return c == 0 ? false : true;
		return false;
	}

	@Override
	public ProductLot getProductLotByCodeAndOwnerAndProduct(String lot,
			long ownerId, StockObjectType ownerType, long productId)
			throws DataAccessException {
		lot = lot.toUpperCase();
		String sql = "select * from product_lot where lot=? and object_id=? and object_type=? and product_id=?";
		List<Object> params = new ArrayList<Object>();
		params.add(lot);
		params.add(ownerId);
		params.add(ownerType.getValue());
		params.add(productId);
		return repo.getEntityBySQL(ProductLot.class, sql, params);
	}
	
	@Override
	public ProductLot getProductLotByCodeAndOwnerAndProductForUpdate(String code,
			long ownerId, StockObjectType ownerType, long productId)
			throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from product_lot where lot=? and object_id=? and object_type=? and product_id=? for update";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ownerId);
		params.add(ownerType.getValue());
		params.add(productId);
		return repo.getEntityBySQL(ProductLot.class, sql, params);
	}

	@Override
	/** update value = ap_param_name @tientv*/
	public List<ProductLot> getProductLotByProductAndOwner(long productId,
			long ownerId, StockObjectType ownerType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from product_lot where product_id=? and object_id=? and object_type=? and status = ?");
		sql.append(" and quantity > 0");
		sql.append(" and (expiry_date - trunc(sysdate) >= (select value from ap_param where ap_param_code = 'NUM_DAY_LOT_EXPIRE' and status = 1)");
		sql.append(" 	or exists (select 1 from ap_param where ap_param_code = 'NUM_DAY_LOT_EXPIRE' and status = 1 and value < 0))");
		sql.append(" order by expiry_date ");
		List<Object> params = new ArrayList<Object>();
		params.add(productId);
		params.add(ownerId);
		params.add(ownerType.getValue());
		params.add(ActiveType.RUNNING.getValue());//PhuT
		return repo.getListBySQL(ProductLot.class, sql.toString(), params);
	}
	
	@Override
	public List<ProductLot> getProductLotByProductAndOwnerEx(long productId,
			long ownerId, StockObjectType ownerType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from product_lot where product_id=? and object_id=? and object_type=? and status = ?");
	//	sql.append(" and quantity > 0 "); //loctt - Oct 9, 2013, fix 0011293
		sql.append(" 	order by expiry_date ");
		List<Object> params = new ArrayList<Object>();
		params.add(productId);
		params.add(ownerId);
		params.add(ownerType.getValue());
		params.add(ActiveType.RUNNING.getValue());//PhuT
		return repo.getListBySQL(ProductLot.class, sql.toString(), params);
	}

	@Override
	/** update value = ap_param_name @tientv*/
	public List<ProductLot> getProductLotByProductAndOwnerForUpdate(long productId,
			long ownerId, StockObjectType ownerType, Integer fifo) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		ApParam ap = apParamDAO.getApParamByCode("NUM_DAY_LOT_EXPIRE", ActiveType.RUNNING);
		Integer delta = null;
		if (ap != null && Integer.parseInt(ap.getValue()) != -1) 
			delta = Integer.parseInt(ap.getValue());
		sql.append(" select * from product_lot where product_id=? and object_id=? and object_type=?");
		sql.append(" and quantity > 0 and status=?");
		params.add(productId);
		params.add(ownerId);
		params.add(ownerType.getValue());
		params.add(ActiveType.RUNNING.getValue());
		if (delta != null) {
			sql.append(" and expiry_date >= trunc(sysdate) + ? ");
			params.add(delta);
		}
		if (fifo != null && fifo == 2) {
			sql.append(" order by expiry_date desc for update");
		} else {
			sql.append(" order by expiry_date for update");
		}
		return repo.getListBySQL(ProductLot.class, sql.toString(), params);
	}
	
	@Override
	/** update value = ap_param_name @tientv*/
	public List<ProductLot> getProductLotByProductAndOwnerForUpdate(long productId,
			long ownerId, StockObjectType ownerType, String orderBy) throws DataAccessException {
		ApParam ap = apParamDAO.getApParamByCode("NUM_DAY_LOT_EXPIRE", ActiveType.RUNNING);
		Integer delta = null;
		if (ap != null && Integer.parseInt(ap.getValue()) != -1) 
			delta = Integer.parseInt(ap.getValue());
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_lot where product_id=? and object_id=? and object_type=?");
		params.add(productId);
		params.add(ownerId);
		params.add(ownerType.getValue());
//		sql.append(" and available_quantity > 0 and status=? ");
		sql.append(" and quantity > 0 and status=? ");
		params.add(ActiveType.RUNNING.getValue());
		if (delta != null) {
			sql.append(" and trunc(expiry_date) - trunc(sysdate) >= ? ");
			params.add(delta);
		}
		sql.append(" order by expiry_date ");
		if (orderBy != null)
			sql.append(orderBy);
		sql.append(" for update");
		return repo.getListBySQL(ProductLot.class, sql.toString(), params);
	}
	
	@Override
	/** update value = ap_param_name @tientv*/
	public List<ProductLot> getListProductLot(KPaging<ProductLot> kPaging,
			Long ownerId, StockObjectType ownerType, Integer quantity,
			ActiveType status, Long productId, Boolean checkApparam) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_lot where 1 = 1");
		if (ownerId != null) {
			sql.append(" and object_id = ?");
			params.add(ownerId);
		}
		if (ownerType != null) {
			sql.append(" and object_type = ?");
			params.add(ownerType.getValue());
		}
		if (status != null) {
			sql.append(" and status = ?");
			params.add(status.getValue());
		}
		if (quantity != null) {
			sql.append(" and quantity > ?");
			params.add(quantity);
		}
		if (null != productId) {
			sql.append(" and product_id = ?");
			params.add(productId);
		}
		if (checkApparam != null && checkApparam == true) {
			ApParam ap = apParamDAO.getApParamByCode("NUM_DAY_LOT_EXPIRE", ActiveType.RUNNING);
			Integer delta = null;
			if (ap != null && Integer.parseInt(ap.getValue()) != -1) 
				delta = Integer.parseInt(ap.getValue());
			if (delta != null) {
				sql.append(" and trunc(expiry_date) - trunc(sysdate) >= ? ");
				params.add(delta);
			}
		}
		sql.append(" order by expiry_date desc");
		if (kPaging != null) {
			return repo.getListBySQLPaginated(ProductLot.class, sql.toString(),
					params, kPaging);
		} else {
			return repo.getListBySQL(ProductLot.class, sql.toString(), params);
		}
	}
	
	@Override
	/** update value = ap_param_name @tientv*/
	public List<ProductLot> getListProductLotForUpdate(KPaging<ProductLot> kPaging,
			Long ownerId, StockObjectType ownerType, Integer quantity,
			ActiveType status, Long productId, Boolean checkApparam) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_lot where 1 = 1");
		if (ownerId != null) {
			sql.append(" and object_id = ?");
			params.add(ownerId);
		}
		if (ownerType != null) {
			sql.append(" and object_type = ?");
			params.add(ownerType.getValue());
		}
		if (status != null) {
			sql.append(" and status = ?");
			params.add(status.getValue());
		}
		if (quantity != null) {
			sql.append(" and quantity > ?");
			params.add(quantity);
		}
		if (null != productId) {
			sql.append(" and product_id = ?");
			params.add(productId);
		}
		if (checkApparam != null && checkApparam == true) {
			ApParam ap = apParamDAO.getApParamByCode("NUM_DAY_LOT_EXPIRE", ActiveType.RUNNING);
			Integer delta = null;
			if (ap != null && Integer.parseInt(ap.getValue()) != -1) 
				delta = Integer.parseInt(ap.getValue());
			if (delta != null) {
				sql.append(" and trunc(expiry_date) - trunc(sysdate) >= ? ");
				params.add(delta);
			}
		}
		sql.append(" order by expiry_date for update");
		if (kPaging != null) {
			return repo.getListBySQLPaginated(ProductLot.class, sql.toString(),
					params, kPaging);
		} else {
			return repo.getListBySQL(ProductLot.class, sql.toString(), params);
		}
	}
	
	@Override
	public List<ProductLotVO> getListProductInStock(KPaging<ProductLotVO> kPaging,Long shopId,
			Long ownerId, StockObjectType ownerType, Integer quantity,
			ActiveType status) throws DataAccessException {
		/* tientv: update owner_id->object_id,owner_type->object_type*/
		Date appDate = commonDAO.getSysDate();
		if(shopId!=null){
			appDate = shopLockDAO.getApplicationDate(shopId);
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select pl.lot lot, pl.quantity quantity, pl.available_quantity availableQuantity,");
		sql.append(" pd.product_id productId, pd.product_code productCode, pd.product_name productName,");
		sql.append(" pd.net_weight netWeight, pd.gross_weight grossWeight, pd.convfact convfact, pr.price price");
		sql.append(" from product_lot pl, product pd, price pr, stock_total st");
		sql.append(" where 1 = 1");
		sql.append(" and pl.object_id = st.object_id and pl.object_type = st.object_type and pl.product_id = st.product_id and st.quantity > 0");
		sql.append(" and pl.product_id = pd.product_id");
		sql.append(" and pl.product_id = pr.product_id");
		sql.append(" and pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?))");
		params.add(appDate);
		params.add(appDate);
		sql.append(" and pd.check_lot = 1 and pr.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and pl.expiry_date >= trunc(sysdate)");//lacnv1 - 06.03.2014
		
		if (ownerId != null) {
			sql.append(" and pl.object_id = ?");
			params.add(ownerId);
		}
		if (ownerType != null) {
			sql.append(" and pl.object_type = ?");
			params.add(ownerType.getValue());
		}
		if (status != null) {
			sql.append(" and pl.status = ?");
			params.add(status.getValue());
		}
		if (quantity != null) {
			sql.append(" and pl.quantity > ?");
			params.add(quantity);
		}
		
		sql.append(" union");
		sql.append(" select '' as lot, sum(pl.quantity) quantity, sum(pl.available_quantity) availableQuantity,");
		sql.append(" pd.product_id productId, pd.product_code productCode, pd.product_name productName,");
		sql.append(" pd.net_weight netWeight, pd.gross_weight grossWeight, pd.convfact convfact, pr.price price");
		sql.append(" from stock_total pl, product pd, price pr");
		sql.append(" where 1 = 1");
		sql.append(" and pl.product_id = pd.product_id");
		sql.append(" and pl.product_id = pr.product_id");
		sql.append(" and pr.from_date < trunc(sysdate) + 1 and (pr.to_date is null or pr.to_date >= trunc(sysdate))");
		sql.append(" and pd.check_lot = 0 and pr.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		
		if (ownerId != null) {
			sql.append(" and pl.object_id = ?");
			params.add(ownerId);
		}
		if (ownerType != null) {
			sql.append(" and pl.object_type in (4,?)");
			params.add(ownerType.getValue());
		}
//		trong stock_total khong co status
//		if (status != null) {
//			sql.append(" and pl.status = ?");
//			params.add(status.getValue());
//		}
		if (quantity != null) {
			sql.append(" and pl.quantity > ?");
			params.add(quantity);
		}
		sql.append(" GROUP BY pd.product_id, pd.product_code, pd.product_name, pd.net_weight, pd.gross_weight, pd.convfact, pr.price");
		sql.append(" order by productCode, lot desc ");
		

		
		
		final String[] fieldNames = new String[] { "lot", "quantity", "availableQuantity"
				, "productId", "productCode", "productName", "netWeight", "grossWeight", "convfact", "price"};
		
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER
				, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING
				, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL};
		
		if (kPaging != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(" )");
			return repo.getListByQueryAndScalarPaginated(ProductLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(ProductLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	@Override
	public List<ProductLotVO> getListProductLotVO(KPaging<ProductLotVO> kPaging,
			Long ownerId, StockObjectType ownerType, Integer quantity,
			ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select pl.product_lot_id id, pl.lot lot, pl.quantity quantity,");
		sql.append(" pd.product_id productId, pd.product_code productCode, pd.product_name productName,");
		sql.append(" pd.net_weight netWeight, pd.gross_weight grossWeight, pd.convfact convfact, pr.price price");
		sql.append(" from product_lot pl, product pd, price pr");
		sql.append(" where 1 = 1");
		sql.append(" and pl.product_id = pd.product_id");
		sql.append(" and pl.product_id = pr.product_id");
		sql.append(" and pr.from_date < trunc(sysdate) + 1 and (pr.to_date is null or pr.to_date >= trunc(sysdate))");
		if (ownerId != null) {
			sql.append(" and pl.object_id = ?");
			params.add(ownerId);
		}
		if (ownerType != null) {
			sql.append(" and pl.object_type = ?");
			params.add(ownerType.getValue());
		}
		if (status != null) {
			sql.append(" and pl.status = ?");
			params.add(status.getValue());
		}
		if (quantity != null) {
			sql.append(" and pl.quantity > ?");
			params.add(quantity);
		}
		sql.append(" order by pl.product_lot_id desc ");
		

		
		
		final String[] fieldNames = new String[] { "id", "lot", "quantity"
				, "productId", "productCode", "productName", "netWeight", "grossWeight", "convfact", "price"};
		
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER
				, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING
				, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL};
		
		if (kPaging != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(" )");
			return repo.getListByQueryAndScalarPaginated(ProductLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(ProductLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public boolean checkQuantityInProductLot(long shopId, long productId,
			int quantity, String lot) throws DataAccessException {
		if (lot == null) {
			throw new IllegalArgumentException("lot is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) count from product_lot pl"); 
		sql.append(" where pl.object_id = ? and pl.object_type = ?");
		params.add(shopId);
		params.add(StockObjectType.SHOP.getValue());
		sql.append(" and pl.product_id = ?");
		params.add(productId);		
		sql.append(" and pl.quantity > 0 and pl.quantity >= ?");
		params.add(quantity);
		sql.append(" and pl.lot = ?");
		params.add(lot);
		return repo.countBySQL(sql.toString(), params) > 0 ? true : false;
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.ProductLotDAO#getProductLogForUpdatePo(long)
	 */
	@Override
	public ProductLot getProductLogForUpdatePo(long poVnmId)
			throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select pl.* from product_lot pl, po_vnm_lot pvl");
		sql.append(" where pvl.po_vnm_id = ?");
		sql.append(" and pvl.product_id = pl.product_id and pvl.lot = pl.lot for update");
		params.add(poVnmId);
		return repo.getEntityBySQL(ProductLot.class, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.ProductLotDAO#getProductLotByPoVnmId(java.lang.Long)
	 */
	@Override
	public ProductLot getProductLotByProductIdAndLot(Long productId, String lot)
			throws DataAccessException {
		
		if (productId == null) {
			throw new IllegalArgumentException("productId is null");
		}
		if (lot == null) {
			throw new IllegalArgumentException("lot is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_lot");
		sql.append(" where product_id = ? and lot = ?");
		params.add(productId);
		params.add(lot);
		return repo.getEntityBySQL(ProductLot.class, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.ProductLotDAO#updateQuantityForProductLot(java.lang.Long, int)
	 */
	@Override
	public void updateQuantityAndAvaiableQuantityForProductLot(Long productLotId, Integer quantity)
			throws DataAccessException {
		
		if (productLotId == null) {
			throw new IllegalArgumentException("productLotId is null");
		}
		if (quantity == null) {
			throw new IllegalArgumentException("quantity is null");
		}
		StringBuilder sql = new StringBuilder();
		sql.append(" update product_lot set quantity = quantity + ?, available_quantity = available_quantity + ? where product_lot_id = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(quantity);
		params.add(quantity);
		params.add(productLotId);
		repo.executeSQLQuery(sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.ProductLotDAO#getListProductForVanSale(ths.dms.core.entities.enumtype.KPaging, java.lang.Long, ths.dms.core.entities.enumtype.StockObjectType)
	 */
	@Override
	public List<ProductLotVO> getListProductForVanSale(
			KPaging<ProductLotVO> kPaging, Long ownerId,
			StockObjectType ownerType) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT tb1.product_id as productId, tb1.product_code as productCode, tb1.product_name as productName, tb1.net_weight as netWeight, tb1.gross_weight as grossWeight");
		sql.append(" , tb1.convfact as convfact, tb1.price as price, tb2.quantity as quantity, tb2.available_quantity as availableQuantity");
		sql.append(" FROM");
		sql.append(" (SELECT DISTINCT (std.product_id),");
		sql.append(" p.product_name,");
		sql.append(" p.product_code,");
		sql.append(" p.net_weight,");
		sql.append(" p.gross_weight,");
		sql.append(" p.convfact,");
		sql.append(" std.price");
		sql.append(" FROM stock_trans st,");
		sql.append(" stock_trans_detail std,");
		sql.append(" product p");
		sql.append(" WHERE st.stock_trans_id  = std.stock_trans_id");
		sql.append(" AND std.product_id       = p.product_id");
		sql.append(" AND st.to_owner_id       = ?");
		params.add(ownerId);
		sql.append(" AND st.to_owner_type     = ?");
		params.add(ownerType.getValue());
		sql.append(" AND st.stock_trans_date >= TRUNC(sysdate)");
		sql.append(" ORDER BY p.product_code");
		sql.append(" ) tb1");
		sql.append(" JOIN");
		sql.append(" (SELECT quantity, product_id, available_quantity");
		sql.append(" FROM stock_total");
		sql.append(" WHERE object_id = ?");
		params.add(ownerId);
		sql.append(" AND object_type = ?");
		params.add(ownerType.getValue());
		sql.append(" ) tb2");
		sql.append(" ON tb1.product_id = tb2.product_id");
		
		
		final String[] fieldNames = new String[] { "quantity", "availableQuantity"
				, "productId", "productCode", "productName", "netWeight", "grossWeight", "convfact", "price"};
		
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER
				, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING
				, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL};
		
		if (kPaging != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(" )");
			return repo.getListByQueryAndScalarPaginated(ProductLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(ProductLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.ProductLotDAO#getListProductVanSale(ths.dms.core.entities.enumtype.KPaging, java.lang.String, java.lang.String, java.lang.Long, ths.dms.core.entities.enumtype.StockObjectType, java.lang.Integer, ths.dms.core.entities.enumtype.ActiveType)
	 */
	@Override
	public List<ProductLotVO> getListProductVanSale(
			KPaging<ProductLotVO> kPaging, String productCode,
			String productName, Long ownerId, StockObjectType ownerType,
			Integer quantity, ActiveType status) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select pl.lot lot, pl.quantity quantity, pl.available_quantity availableQuantity,");
		sql.append(" pd.product_id productId, pd.product_code productCode, pd.product_name productName,");
		sql.append(" pd.net_weight netWeight, pd.gross_weight grossWeight, pd.convfact convfact, pr.price price");
		sql.append(" from product_lot pl, product pd, price pr");
		sql.append(" where 1 = 1");
		sql.append(" and pl.product_id = pd.product_id");
		sql.append(" and pl.product_id = pr.product_id");
		sql.append(" and pr.from_date < trunc(sysdate) + 1 and (pr.to_date is null or pr.to_date >= trunc(sysdate))");
		sql.append(" and pd.check_lot = 1 and pr.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		
		if (ownerId != null) {
			sql.append(" and pl.object_id = ?");
			params.add(ownerId);
		}
		if (ownerType != null) {
			sql.append(" and pl.object_type = ?");
			params.add(ownerType.getValue());
		}
		if (status != null) {
			sql.append(" and pl.status = ?");
			params.add(status.getValue());
		}
		if (quantity != null) {
			sql.append(" and pl.quantity > ?");
			params.add(quantity);
		}
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and upper(pd.product_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(productCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" and upper(pd.product_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(productName.toUpperCase()));
		}
		sql.append(" order by pd.product_code ");
		

		
		
		final String[] fieldNames = new String[] { "lot", "quantity", "availableQuantity"
				, "productId", "productCode", "productName", "netWeight", "grossWeight", "convfact", "price"};
		
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER
				, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING
				, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL};
		
		if (kPaging != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(" )");
			return repo.getListByQueryAndScalarPaginated(ProductLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(ProductLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.ProductLotDAO#getListProductLotForInstock(java.lang.Long, ths.dms.core.entities.enumtype.StockObjectType, java.lang.Integer)
	 */
	@Override
	public List<ProductLotVO> getListProductLotForInstock(Long shopId,Long objectId,
			StockObjectType objectType, Integer fromQuantity, Integer productId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		Date appDate = commonDAO.getSysDate();
		if(shopId!=null){
			appDate = shopLockDAO.getApplicationDate(shopId);
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT p.product_id     AS productId,");
		sql.append(" p.product_code        AS productCode,");
		sql.append(" p.product_name        AS productName,");
		sql.append(" pl.lot                AS lot,");
		sql.append(" pr.price               AS price,");
		sql.append(" p.net_weight          AS netWeight,");
		sql.append(" p.gross_weight        AS grossWeight,");
		sql.append(" p.convfact            AS convfact,");
		sql.append(" pl.quantity           AS quantity,");
		sql.append(" pl.available_quantity AS availableQuantity");
		sql.append(" FROM product_lot pl,");
		sql.append(" product p, price pr");
		sql.append(" WHERE pl.product_id = p.product_id");
		sql.append(" AND pl.object_id    = ?");
		params.add(objectId);
		sql.append(" AND pl.object_type  = ?");
		params.add(objectType.getValue());
		sql.append(" AND p.status        = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (fromQuantity != null) {
			sql.append(" AND pl.quantity     > ?");
			params.add(fromQuantity);
		}
		sql.append(" AND pr.product_id = p.product_id");
		sql.append(" AND pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?))");
		params.add(appDate);
		params.add(appDate);
		sql.append(" AND pr.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND p.check_lot = 1");
		sql.append(" AND p.product_id = ?");
		params.add(productId);
		sql.append(" ORDER BY pl.expiry_date DESC");
		final String[] fieldNames = new String[] { "lot", "quantity", "availableQuantity"
				, "productId", "productCode", "productName", "netWeight", "grossWeight", "convfact", "price"};
		
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER
				, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING
				, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL};
		
		return repo.getListByQueryAndScalar(ProductLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
}

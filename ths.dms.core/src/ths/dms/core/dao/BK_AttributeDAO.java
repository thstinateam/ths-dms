package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Attribute;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.TableHasAttribute;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface BK_AttributeDAO {

	Attribute createAttribute(Attribute attribute, LogInfoVO logInfo) throws DataAccessException;

	void deleteAttribute(Attribute attribute, LogInfoVO logInfo) throws DataAccessException;

	void updateAttribute(Attribute attribute, LogInfoVO logInfo) throws DataAccessException;
	
	Attribute getAttributeById(long id) throws DataAccessException;

	List<Attribute> getListAttribute(KPaging<Attribute> kPaging,
			TableHasAttribute table, String attributeCode,
			String attributeName, AttributeColumnType columnType,
			ActiveType status) throws DataAccessException;

	Attribute getAttributeByCode(TableHasAttribute table, String code)
			throws DataAccessException;
	
	/**
	 * Xoa gia tri thuoc tinh
	 * 
	 * @param listAttribute
	 * @throws DataAccessException
	 * @author thuattq
	 */
	void deleteAttributeValue(List<Attribute> listAttribute)
			throws DataAccessException;
	
	Boolean isUsingByOthers(Attribute attr) throws DataAccessException;
	
	/**
	 * Import Attribute data
	 * 
	 * @param table
	 * @param listActiveAttribute
	 * @param data
	 * @throws BusinessException
	 * @author thuattq
	 */
	/*void importAttribute(TableHasAttribute table,
			List<Attribute> listActiveAttribute, List<List<String>> data,
			LogInfoVO logInfo) throws DataAccessException;*/
	
	/**
	 * Export attribute data
	 * 
	 * @param table
	 * @param listActiveAttribute
	 * @return
	 * @throws BusinessException
	 * @author thuattq
	 */
	List<List<String>> exportAttribute(TableHasAttribute table,
			List<Attribute> listActiveAttribute) throws DataAccessException;

	/**
	 * Kiem tra gia tri khoa truyen vao, co ton tai khong
	 * 
	 * @param table
	 * @param data
	 * @return
	 * @throws BusinessException
	 */
	boolean checkKeyIsExists(TableHasAttribute table,
			List<String> listValueKey) throws DataAccessException;
	
	/**
	 * Kiem tra danh sach cac ma attribute truyen vao, ma nao chua duoc khai bao
	 * 
	 * @param table
	 * @param data
	 * @return
	 * @throws BusinessException
	 */
	boolean checkAttributeIsExists(TableHasAttribute table,
			String attributeCode) throws DataAccessException;
}

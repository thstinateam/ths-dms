package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.PoAuto;
import ths.dms.core.entities.PoAutoGroupShopMap;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.PoAuto01VO;
import ths.dms.core.exceptions.DataAccessException;

public interface PoAutoDAO {

	PoAuto createPoAuto(PoAuto poAuto) throws DataAccessException;

	void deletePoAuto(PoAuto poAuto) throws DataAccessException;

	void updatePoAuto(PoAuto poAuto) throws DataAccessException;

	void updatePoAuto(List<PoAuto> listPoAuto) throws DataAccessException;

	PoAuto getPoAutoById(Long id) throws DataAccessException;
	PoAutoGroupShopMap getPoAutoGroupShopMapById(Long id) throws DataAccessException;

	List<PoAuto> getListPoVnm(KPaging<PoAuto> kPaging, Long shopId,
			String poAutoNumber, Date fromDate, Date toDate,
			ApprovalStatus status) throws DataAccessException;

	/**
	 * lay danh sach po auto phuc vu cho mang hinh quan ly po_auto
	 * 
	 * @param shopId
	 * @param poAutoNumber
	 * @param fromDate
	 * @param toDate
	 * @param status
	 * @param hasFindChildShop true: lay tat ca shop con, false: chi lay shop truyen vao
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<PoAuto01VO> getListPoAuto01(Long shopId, String poAutoNumber,
			Date fromDate, Date toDate, ApprovalStatus status,
			boolean hasFindChildShop) throws DataAccessException;
	
	List<PoAuto01VO> getListPoAutoForNCC(KPaging<PoAuto01VO> kPaging,Long shopId, String poAutoNumber,
			Date fromDate, Date toDate, List<ApprovalStatus> listStatus,
			boolean hasFindChildShop) throws DataAccessException;

	PoAuto getPoAutoByIdForUpdate(Long id) throws DataAccessException;
}

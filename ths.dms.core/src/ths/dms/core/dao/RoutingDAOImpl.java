package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Routing;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.business.CommonMgr;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class RoutingDAOImpl implements RoutingDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	ShopDAO shopDAO;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public Routing createRouting(Routing routing) throws DataAccessException {
		if (routing == null) {
			throw new IllegalArgumentException("routing");
		}
		routing.setCreateDate(commonDAO.getSysDate());
		repo.create(routing);
		return repo.getEntityById(Routing.class, routing.getId());
	}

	@Override
	public void deleteRouting(Routing routing) throws DataAccessException {
		if (routing == null) {
			throw new IllegalArgumentException("routing");
		}
		repo.delete(routing);
	}

	@Override
	public Routing getRoutingById(Long id) throws DataAccessException {
		return repo.getEntityById(Routing.class, id);
	}
	
	@Override
	public void updateRouting(Routing routing) throws DataAccessException {
		if (routing == null) {
			throw new IllegalArgumentException("routing");
		}
		routing.setUpdateDate(commonDAO.getSysDate());
		repo.update(routing);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.RoutingDAO#getListRouting()
	 */
	@Override
	public List<Routing> getListRouting(KPaging<Routing> kPaging, Long parentShopId, Long shopId, Long superId, String routingCode, String routingName) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from routing where 1 = 1");
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (shopId != null) {
			sql.append(" and shop_id = ?");
			params.add(shopId);
		} else {
			if (parentShopId != null) {
				sql.append(" and shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
				params.add(parentShopId);
			} else if (superId != null){
				sql.append(" and shop_id in (select distinct(s.shop_id) from shop s, staff st");
				sql.append(" where s.shop_id = st.shop_id and st.staff_owner_id = ?)");
				params.add(superId);
			} else {
				throw new IllegalArgumentException("all params id is null");
			}
		}
		if (!StringUtility.isNullOrEmpty(routingCode)) {
			sql.append(" and lower(routing_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(routingCode.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(routingName)) {
			sql.append(" and lower(routing_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(routingName.toLowerCase()));
		}
		sql.append(" order by upper(routing_code), create_date desc");
		if (kPaging != null)
			return repo.getListBySQLPaginated(Routing.class, sql.toString(), params, kPaging);
		else
			return repo.getListBySQL(Routing.class, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.RoutingDAO#getRoutingByCode(java.lang.String)
	 */
	@Override
	public Routing getRoutingByCode(Long shopId, String code) throws DataAccessException {
		if (code == null) {
			throw new IllegalArgumentException("code is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from routing where shop_id = ? and upper(routing_code) = ? and status =1");
		params.add(shopId);
		params.add(code.toUpperCase());
		return repo.getEntityBySQL(Routing.class, sql.toString(), params);
	}

	@Override
	public Routing getRoutingMultiChoice(Long routingId, Long shopId, String routingCode, Integer status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from routing  where 1 = 1 ");
		if(routingId != null){
			sql.append("and routing_id = ? ");
			params.add(routingId);
		}
		if(shopId != null){
			sql.append("and shop_id = ?  ");
			params.add(shopId);
		}
		if(!StringUtility.isNullOrEmpty(routingCode)){
			sql.append("and upper(routing_code) = ?  ");
			params.add(routingCode.trim().toUpperCase());
		}
		if(status != null){
			sql.append("and status = ? ");
			params.add(status);
		}else{
			sql.append("and status in (0, 1)");
		}
		return repo.getEntityBySQL(Routing.class, sql.toString(), params);
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.core.dao.RoutingDAO#getListRoutingBySuper(java.lang.Long, java.lang.String, java.lang.String)
	 */
	@Override
	public List<Routing> getListRoutingBySuper(KPaging<Routing> kPaging, Long superId,
			String routingCode, String routingName) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from routing where 1 = 1");
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and shop_id in (select distinct(s.shop_id) from shop s, staff st");
		sql.append(" where s.shop_id = st.shop_id and st.staff_owner_id = ?)");
		params.add(superId);
		if (!StringUtility.isNullOrEmpty(routingCode)) {
			sql.append(" and lower(routing_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(routingCode
					.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(routingName)) {
			sql.append(" and lower(routing_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(routingName
					.toLowerCase()));
		}
		sql.append(" order by upper(routing_code), create_date desc");
		if (kPaging != null)
			return repo.getListBySQLPaginated(Routing.class, sql.toString(),
					params, kPaging);
		else
			return repo.getListBySQL(Routing.class, sql.toString(), params);
	}
	
}

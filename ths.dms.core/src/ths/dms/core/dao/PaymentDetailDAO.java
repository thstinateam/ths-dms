package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.PayReceivedTemp;
import ths.dms.core.entities.PaymentDetail;
import ths.dms.core.entities.PaymentDetailTemp;
import ths.dms.core.entities.filter.PayReceivedFilter;
import ths.dms.core.entities.vo.PayDetailVO;
import ths.dms.core.entities.enumtype.PaymentStatus;
import ths.core.entities.vo.rpt.RptPayReceived1VO;
import ths.dms.core.exceptions.DataAccessException;

public interface PaymentDetailDAO {

	PaymentDetail createPaymentDetail(PaymentDetail paymentDetail) throws DataAccessException;

	void deletePaymentDetail(PaymentDetail paymentDetail) throws DataAccessException;

	void updatePaymentDetail(PaymentDetail paymentDetail) throws DataAccessException;
	
	PaymentDetail getPaymentDetailById(long id) throws DataAccessException;

	void createListPaymentDetail(List<PaymentDetail> lstPaymentDetail)
			throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @param custId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptPayReceived1VO> getListRptPayReceived1VO(Long shopId, String shortCode,
			Date fromDate, Date toDate) throws DataAccessException;

	/**
	 * Lay so lan da tra cua debit_detail
	 * 
	 * @author lacnv1
	 * @since Apr 18, 2014
	 */
	Integer getTimeOfPay(Long debitDetailId) throws DataAccessException;
	
	/**
	 * Tao 1 ds cac payment_detail dong thoi chuyen sang payment_detail_temp
	 * 
	 * @author lacnv1
	 * @since Nov 20, 2014
	 */
	void createListPaymentDetailAndCloneToTemp(List<PaymentDetail> lstPaymentDetail, PayReceivedTemp payReceived) throws DataAccessException;
	
	/**
	 * lay danh sach chi tiet chung tu
	 * @author nhutnn
	 * @since 23/03/2015
	 */
	List<PayDetailVO> getListPaymentDetailVO(PayReceivedFilter<PayDetailVO> filter) throws DataAccessException;
	
	/**
	 * lay danh sach chi tiet chung tu
	 * @author nhutnn
	 * @since 24/03/2015
	 */
	List<PaymentDetail> getListPaymentDetailByIdPayReceive(Long payReceiveId) throws DataAccessException;
	
	/**
	 * clone 
	 * @author nhutnn
	 * @since 24/03/2015
	 */
	void cloneListPaymentDetailToTemp(List<PaymentDetail> lstPaymentDetail, PayReceivedTemp payTemp) throws DataAccessException;
	
	/**
	 * lay list payment_detail_temp
	 * @author nhutnn
	 * @since 24/03/2015
	 */
	List<PaymentDetailTemp> getListPaymentDetailTempByIdPayReceive(Long payReceiveId) throws DataAccessException;
	void updatePaymentDetailTemp(PaymentDetailTemp paymentDetailTemp) throws DataAccessException;
	
	/**
	 * Lay tong tien thanh toan cho debit_detail
	 * 
	 * @author lacnv1
	 * @since Mar 23, 2015
	 */
	BigDecimal getPaymentAmountOfDebitDetail(long debitDetailId, PaymentStatus paymentStatus) throws DataAccessException;
}

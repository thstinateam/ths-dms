package ths.dms.core.dao;

import ths.dms.core.entities.IncentiveProgram;
import ths.dms.core.entities.enumtype.IncentiveProgramFilter;
import ths.dms.core.entities.enumtype.IncentiveProgramVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.DataAccessException;

public interface IncentiveProgramDAO {

	IncentiveProgram createIncentiveProgram(IncentiveProgram incentiveProgram,
			LogInfoVO logInfo) throws DataAccessException;

	void deleteIncentiveProgram(IncentiveProgram incentiveProgram,
			LogInfoVO logInfo) throws DataAccessException;

	void updateIncentiveProgram(IncentiveProgram incentiveProgram,
			LogInfoVO logInfo) throws DataAccessException;

	IncentiveProgram getIncentiveProgramById(long id)
			throws DataAccessException;

	ObjectVO<IncentiveProgramVO> getListIncentiveProgramVO(
			IncentiveProgramFilter filter, KPaging<IncentiveProgramVO> kPaging)
			throws DataAccessException;

	IncentiveProgram getIncentiveProgramByCode(String code)
			throws DataAccessException;

	Boolean isDuplicateProductByIncentiveProgram(
			IncentiveProgram incentiveProgram) throws DataAccessException;
}
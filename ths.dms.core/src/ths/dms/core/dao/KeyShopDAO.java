package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.KS;
import ths.dms.core.entities.KSCusProductReward;
import ths.dms.core.entities.KSCustomer;
import ths.dms.core.entities.KSCustomerLevel;
import ths.dms.core.entities.KSLevel;
import ths.dms.core.entities.KSProduct;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.KeyShopFilter;
import ths.dms.core.entities.filter.SaleOrderPromoDetailFilter;
import ths.dms.core.entities.vo.AllocateShopVO;
import ths.dms.core.entities.vo.KSCusProductRewardDoneVO;
import ths.dms.core.entities.vo.KeyShopCustomerHistoryVO;
import ths.dms.core.entities.vo.KeyShopVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.TotalRewardMoneyVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface KeyShopDAO {

	KS createKS(KS ks, LogInfoVO logInfo) throws DataAccessException;

	void deleteKS(KS ks, LogInfoVO logInfo) throws DataAccessException;

	void updateKS(KS ks, LogInfoVO logInfo) throws DataAccessException;
	
	KS getKSById(long id) throws DataAccessException;
	
	KSLevel getKSLevelById(long id) throws DataAccessException;

	KS getKSByCode(String code) throws DataAccessException;
	
	KSLevel getKSLevelByCode(String levelCode, String ksCode)
			throws DataAccessException;
	
	/**
	 * @author tungmt
	 * @since 01/07/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<KeyShopVO> getListKeyShopVOByFilter(KeyShopFilter filter) throws DataAccessException;

	Boolean checkExistsCustomerNotInKSCycle(KeyShopFilter filter) throws DataAccessException;

	Boolean checkExistsCustomerNotInKSLevel(KeyShopFilter filter) throws DataAccessException;
	
	Boolean checkExistsRewardInKS(KeyShopFilter filter) throws DataAccessException;

	List<KSProduct> getListKSProductByFilter(KeyShopFilter filter) throws DataAccessException;

	List<KSCustomer> getListKSCustomerByFilter(KeyShopFilter filter) throws DataAccessException;

	List<KSCustomerLevel> getListKSCustomerLevelByFilter(KeyShopFilter filter) throws DataAccessException;

	List<KSCusProductReward> getListKSCusProductRewardByFilter( KeyShopFilter filter) throws DataAccessException;

	List<Shop> getListShopForKSByFilter(KeyShopFilter filter) throws DataAccessException;

	void removeShopMapByShop(KeyShopFilter filter) throws DataAccessException;

	Boolean checkKSCustomerExistsShop(KeyShopFilter filter) throws DataAccessException;
	
	Boolean checkKSCustomerExistsShopDel(KeyShopFilter filter) throws DataAccessException;

	List<KeyShopVO> getListKSProductVOByFilter(KeyShopFilter filter) throws DataAccessException;

	List<KeyShopVO> getListKSLevelVOByFilter(KeyShopFilter filter) throws DataAccessException;
	
	List<KeyShopVO> getListKSCustomerVOByFilter(KeyShopFilter filter) throws DataAccessException;
	
	List<KeyShopVO> getListKSRewardVOByFilter(KeyShopFilter filter) throws DataAccessException;
	
	List<KeyShopVO> getListKSCustomerLevelVOByFilter(KeyShopFilter filter) throws DataAccessException;

	/**
	 * @author tungmt
	 * @since
	 * @param filter
	 * @return Danh sach tra thuong cho don hang (bao gom ca tien va san pham)
	 * @throws DataAccessException
	 */
	List<KeyShopVO> getListKSVOForRewardOrderByFilter(KeyShopFilter filter) throws DataAccessException;
	
	KSCustomer getKSCustomerById(long id) throws DataAccessException;

	/**
	 * @author trietptm
	 * @param filter
	 * @return Danh sach don vi phan keyshop
	 * @throws DataAccessException
	 * @since Aug 01, 2015
	 */
	List<AllocateShopVO> getListAllocateShop(KeyShopFilter filter) throws DataAccessException;

	/**
	 * danh sach KSCustomer - de tra thuong/tra hang
	 * @author trietptm
	 * @param filter
	 * @return danh sach KSCustomer 
	 * @throws DataAccessException
	 * @since Oct 16, 2015
	 */
	List<KSCustomer> getListKSCustomerForReward(KeyShopFilter filter) throws DataAccessException;

	/** Lay danh sach tra thuong san pham keyshop - de tra thuong/tra hang
	 * @author trietptm
	 * @param keyShopFilter
	 * @return danh sach KSCusProductReward 
	 * @throws DataAccessException
	 * @since Oct 16, 2015
	 */
	List<KSCusProductReward> getListKSCusProductRewardForReward(KeyShopFilter keyShopFilter) throws DataAccessException;

	/**
	 * @author trietptm
	 * @param lstKSCustomerId
	 * @return danh sach KSCusProductRewardDoneVO de kiem tra da tra toan bo san pham cho khach hang hay chua
	 * @throws DataAccessException
	 */
	List<KSCusProductRewardDoneVO> getListKSCusProductRewardDone(List<Long> lstKSCustomerId) throws DataAccessException;

	/**
	 * @author trietptm
	 * @param filter
	 * @return danh sach TotalRewardMoneyVO de check tra thuong tien
	 * @throws DataAccessException
	 */
	List<TotalRewardMoneyVO> getListTotalRewardMoneyVO(SaleOrderPromoDetailFilter filter) throws DataAccessException;

	/**
	 * @author tungmt
	 * @version 1.0
	 * @param filter
	 * @return Thong tin KS cho don hang, bao gom danh sach san pham cua ks
	 * @exception BusinessException
	 * @see
	 * @since 12/8/2015
	 * @serial
	 */
	List<KeyShopVO> getInfoKSForOrder(KeyShopFilter filter) throws DataAccessException;

	/**
	 * @author trietptm
	 * @param filter
	 * @return Thong tin KS cho don hang
	 * @throws DataAccessException
	 * @since Aug 17, 2015
	 */
	List<KeyShopVO> getListKeyShopVOForViewOrderByFilter(KeyShopFilter filter) throws DataAccessException;

	/**
	 * Lich su cap nhat cua khach hang - CTHTTM
	 * @author hunglm16
	 * @param kPaging
	 * @param ksId
	 * @param cycleId
	 * @param ksLevelId
	 * @param customerId
	 * @param updateDate
	 * @return
	 * @throws DataAccessException
	 * @since 24/11/2015
	 */
	List<KeyShopCustomerHistoryVO> getKSRegistedHistory(KPaging<KeyShopCustomerHistoryVO> kPaging, Long ksId, Long cycleId, Long ksLevelId, Long customerId, Date updateDate) throws DataAccessException;
}

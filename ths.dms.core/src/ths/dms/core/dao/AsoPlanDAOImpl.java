/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.AsoPlan;
import ths.dms.core.entities.enumtype.AsoPlanType;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.vo.AsoPlanVO;
import ths.dms.core.entities.vo.RoutingVO;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.CommonFilter;

import ths.dms.core.dao.repo.IRepository;

/**
 * AsoPlanDAOImpl phan bo Aso
 * @author vuongmq
 * @since 19/10/2015 
 */
public class AsoPlanDAOImpl implements AsoPlanDAO {
	
	@Autowired
	private IRepository repo;

	@Override
	public List<AsoPlan> getListAsoByFilter(CommonFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from aso_plan aso ");
		sql.append(" where 1 = 1 ");
		
		if (filter.getPlanType() != null) {
			sql.append(" and aso.object_type = ? ");
			params.add(filter.getPlanType());
		}
		if (filter.getShopId() != null) {
			sql.append(" and aso.shop_id = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getCycleId() != null) {
			sql.append(" and aso.cycle_id = ? ");
			params.add(filter.getCycleId());
		}
		if (filter.getRoutingId() != null) {
			sql.append(" and aso.routing_id = ? ");
			params.add(filter.getRoutingId());
		}
		if (filter.getObjectId() != null) {
			sql.append(" and aso.object_id = ? ");
			params.add(filter.getObjectId());
		}
		if (Boolean.TRUE.equals(filter.isNotIn())) {
			if (filter.getLstId() != null && filter.getLstId().size() > 0) {
				sql.append(" and aso.object_id not in (-1 ");
				for (int i = 0, sz = filter.getLstId().size(); i < sz; i++) {
					sql.append(", ? ");
					params.add(filter.getLstId().get(i));
				}
				sql.append(" )");
			}
		} else {
			if (filter.getLstId() != null && filter.getLstId().size() > 0) {
				sql.append(" and aso.object_id in (-1 ");
				for (int i = 0, sz = filter.getLstId().size(); i < sz; i++) {
					sql.append(", ? ");
					params.add(filter.getLstId().get(i));
				}
				sql.append(" )");
			}
		}
		return repo.getListBySQL(AsoPlan.class, sql.toString(), params);
	}
	
	@Override
	public AsoPlanVO getAsoPlanVOByFilter(CommonFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select aso.shop_id as shopId, ");
		sql.append(" sh.shop_code AS shopCode, ");
		sql.append(" sh.shop_name AS shopName, ");
		sql.append(" aso.routing_id AS routingId, ");
		sql.append(" rt.routing_code as routingCode, ");
		sql.append(" rt.routing_name as routingName, ");
		sql.append(" aso.aso_plan_id as id, ");
		sql.append(" aso.object_type as objectType, ");
		sql.append(" aso.object_id as objectId, ");
		sql.append(" aso.plan as plan ");
		sql.append(" FROM aso_plan aso ");
		sql.append(" JOIN shop sh ");
		sql.append(" ON sh.shop_id = aso.shop_id ");
		sql.append(" JOIN routing rt ");
		sql.append(" on rt.routing_id = aso.routing_id ");
		sql.append(" where 1 = 1 ");
		if (filter.getPlanType() != null) {
			sql.append(" and aso.object_type = ? ");
			params.add(filter.getPlanType());
		}
		if (filter.getShopId() != null) {
			sql.append(" and aso.shop_id = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getCycleId() != null) {
			sql.append(" and aso.cycle_id = ? ");
			params.add(filter.getCycleId());
		}
		if (filter.getRoutingId() != null) {
			sql.append(" and aso.routing_id = ? ");
			params.add(filter.getRoutingId());
		}
		if (filter.getId() != null) {
			sql.append(" and aso.aso_plan_id = ? ");
			params.add(filter.getId());
		}
		String[] fieldNames = { 
				"shopId", //1
				"shopCode", //2
				"shopName",  //3
				"routingId", //7
				"routingCode", //8
				"routingName",  //9
				"id",  //10
				"objectType", //11
				"objectId", //12
				"plan", //15
		};
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG,  //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.LONG, //7
				StandardBasicTypes.STRING, //8
				StandardBasicTypes.STRING, //9
				StandardBasicTypes.LONG, //10
				StandardBasicTypes.INTEGER, //11
				StandardBasicTypes.LONG, //12
				StandardBasicTypes.BIG_DECIMAL, //15
		};
		return (AsoPlanVO) repo.getListByQueryAndScalar(AsoPlanVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	@Override
	public List<AsoPlanVO> getListAsoPlanVOByFilter(CommonFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ");
		if (filter.getCycleId() != null) {
			sql.append(" (select cycle_name from cycle where cycle_id = ?) as cycleName, ");
			sql.append(" (select extract(year from year) from cycle where cycle_id = ?) as year, ");
			params.add(filter.getCycleId());
			params.add(filter.getCycleId());
		} else {
			sql.append(" null as cycleName, ");
			sql.append(" null as year, ");
		}
		if (filter.getShopId() != null) {
			sql.append(" (select shop_id from shop where shop_id = ?) as shopId, ");
			sql.append(" (select shop_code from shop where shop_id = ?) as shopCode, ");
			sql.append(" (select shop_name from shop where shop_id = ?) as shopName, ");
			params.add(filter.getShopId());
			params.add(filter.getShopId());
			params.add(filter.getShopId());
		} else {
			sql.append(" null as shopId, ");
			sql.append(" null as shopCode, ");
			sql.append(" null as shopName, ");
		}
		if (filter.getStaffId() != null) {
			sql.append(" (select staff_id from staff where staff_id = ?) as staffId, ");
			sql.append(" (select staff_code from staff where staff_id = ?) as staffCode, ");
			sql.append(" (select staff_name from staff where staff_id = ?) as staffName, ");
			params.add(filter.getStaffId());
			params.add(filter.getStaffId());
			params.add(filter.getStaffId());
		} else {
			sql.append(" null as staffId, ");
			sql.append(" null as staffCode, ");
			sql.append(" null as staffName, ");
		}
		if (filter.getRoutingId() != null) {
			sql.append(" (select routing_id from routing where routing_id = ?) as routingId, ");
			sql.append(" (select routing_code from routing where routing_id = ?) as routingCode, ");
			sql.append(" (select routing_name from routing where routing_id = ?) as routingName, ");
			params.add(filter.getRoutingId());
			params.add(filter.getRoutingId());
			params.add(filter.getRoutingId());
		} else {
			sql.append(" null as routingId, ");
			sql.append(" null as routingCode, ");
			sql.append(" null as routingName, ");
		}
		
		StringBuilder sqlAnd = new StringBuilder();
		if (filter.getPlanType() != null) {
			sqlAnd.append(" and aso.object_type = ? ");
			params.add(filter.getPlanType());
		}
		if (filter.getCycleId() != null) {
			sqlAnd.append(" and aso.cycle_id = ? ");
			params.add(filter.getCycleId());
		}
		if (filter.getShopId() != null) {
			sqlAnd.append(" and aso.shop_id = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getRoutingId() != null) {
			sqlAnd.append(" and aso.routing_id = ? ");
			params.add(filter.getRoutingId());
		}
		if (AsoPlanType.SKU.getValue().equals(filter.getPlanType())) {
			sql.append(" aso.aso_plan_id as id, ");
			sql.append(" aso.object_type as objectType, ");
			sql.append(" p.product_id AS objectId, ");
			sql.append(" p.product_code as code, ");
			sql.append(" p.product_name as name, ");
			sql.append(" aso.plan as plan ");
			sql.append(" from product p ");
			sql.append(" left join aso_plan aso ");
			sql.append(" on aso.object_id = p.product_id ");
			if (sqlAnd.length() > 0) {
				sql.append(sqlAnd.toString());
			}
			sql.append(" where p.status = 1 ");
		} else {
			sql.append(" aso.aso_plan_id as id, ");
			sql.append(" aso.object_type as objectType, ");
			sql.append(" pi.product_info_id as objectId, ");
			sql.append(" pi.product_info_code as code, ");
			sql.append(" pi.product_info_name as name, ");
			sql.append(" aso.plan as plan ");
			sql.append(" from product_info pi ");
			sql.append(" left join aso_plan aso ");
			sql.append(" on aso.object_id = pi.product_info_id ");
			if (sqlAnd.length() > 0) {
				sql.append(sqlAnd.toString());
			}
			sql.append(" where pi.status = 1 ");
			sql.append(" and pi.type = ? ");
			params.add(ProductType.SUB_CAT.getValue());
		}
		sql.append(" order by shopCode, staffCode, routingCode, code ");
		String[] fieldNames = { 
				"cycleName",
				"year",
				"shopId", //1
				"shopCode", //2
				"shopName",  //3
				"staffId", //4
				"staffCode", //5
				"staffName", //6
				"routingId", //7
				"routingCode", //8
				"routingName",  //9
				"id",  //10
				"objectType", //11
				"objectId", //12
				"code", //13
				"name",  //14
				"plan", //15
		};
		Type[] fieldTypes = {
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG,  //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.LONG, //4
				StandardBasicTypes.STRING,  //5
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.LONG, //7
				StandardBasicTypes.STRING, //8
				StandardBasicTypes.STRING, //9
				StandardBasicTypes.LONG, //10
				StandardBasicTypes.INTEGER, //11
				StandardBasicTypes.LONG, //12
				StandardBasicTypes.STRING, //13
				StandardBasicTypes.STRING, //14
				StandardBasicTypes.BIG_DECIMAL, //15
		};
		return repo.getListByQueryAndScalar(AsoPlanVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<RoutingVO> getRoutingVOByFilter(CommonFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select tmp.staff_id as staffId, ");
		sql.append(" tmp.staff_code as staffCode, ");
		sql.append(" tmp.staff_name as staffName, ");
		sql.append(" rt.routing_id as routingId, ");
		sql.append(" rt.routing_code as routingCode, ");
		sql.append(" rt.routing_name as routingName ");
		sql.append(" from ( ");
		sql.append(" select distinct st.staff_id, ");
		sql.append(" st.staff_code, ");
		sql.append(" st.staff_name, ");
		sql.append(" vp.routing_id ");
		sql.append(" from staff st ");
		sql.append(" left join visit_plan vp on st.staff_id = vp.staff_id ");
		sql.append(" and vp.from_date < trunc(sysdate) + 1 ");
		sql.append(" and ( vp.to_date >= trunc (sysdate) ");
		sql.append(" or vp.to_date is null) ");
		sql.append(" and vp.status in (0, 1) ");
		sql.append(" where 1 = 1 ");
		if (filter.getStaffId() != null) {
			sql.append(" and st.staff_id = ? ");
			params.add(filter.getStaffId());
		}
		sql.append(" ) tmp ");
		sql.append(" join routing rt on rt.routing_id = tmp.routing_id ");
		sql.append(" where 1 = 1 ");
		if (filter.getRoutingId() != null) {
			sql.append(" and rt.routing_id = ? ");
			params.add(filter.getRoutingId());
		}
		String[] fieldNames = { 
				"staffId", //4
				"staffCode", //5
				"staffName", //6
				"routingId", //7
				"routingCode", //8
				"routingName",  //9
		};
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, //4
				StandardBasicTypes.STRING,  //5
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.LONG, //7
				StandardBasicTypes.STRING, //8
				StandardBasicTypes.STRING, //9
		};
		return repo.getListByQueryAndScalar(RoutingVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.AttributeDetail;
import ths.dms.core.entities.CustomerAttributeDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeDetailFilter;
import ths.dms.core.entities.enumtype.AttributeDetailVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.TableHasAttribute;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface AttributeDetailDAO {
	List<AttributeDetailVO> getListAttributeDetailByAttributeId(
			KPaging<AttributeDetailVO> kPaging, Long id, ActiveType status)
			throws DataAccessException;
	AttributeDetail createAttributeDetail(AttributeDetail attributeDetail) throws DataAccessException;

	void deleteAttributeDetail(AttributeDetail attributeDetail) throws DataAccessException;
	
	void deleteListAttributeDetailByAttribute(Long attributeId) throws DataAccessException;

	void updateAttributeDetail(AttributeDetail attributeDetail) throws DataAccessException;
	
	AttributeDetail getAttributeDetailById(long id) throws DataAccessException;

	AttributeDetail getAttributeDetailByCode(Long attributeId,
			String attributeDetailCode) throws DataAccessException;

	List<AttributeDetailVO> getListAttributeDetailVO(
			AttributeDetailFilter filter, KPaging<AttributeDetailVO> kPaging)
			throws DataAccessException;

	Boolean isExistAttributeDetailCode(Long attributeId, String attributeDetailCode) throws DataAccessException;
}

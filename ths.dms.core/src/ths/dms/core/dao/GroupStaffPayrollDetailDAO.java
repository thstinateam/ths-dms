package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.GroupStaffPayrollDetail;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface GroupStaffPayrollDetailDAO {

	GroupStaffPayrollDetail createGroupStaffPayrollDetail(
			GroupStaffPayrollDetail groupStaffPayrollDetail, LogInfoVO logInfo)
			throws DataAccessException;

	void deleteGroupStaffPayrollDetail(
			GroupStaffPayrollDetail groupStaffPayrollDetail, LogInfoVO logInfo)
			throws DataAccessException;

	void updateGroupStaffPayrollDetail(
			GroupStaffPayrollDetail groupStaffPayrollDetail, LogInfoVO logInfo)
			throws DataAccessException;

	GroupStaffPayrollDetail getGroupStaffPayrollDetailById(long id)
			throws DataAccessException;
	
	List<GroupStaffPayrollDetail> getListGroupStaffPayrollDetail(KPaging<GroupStaffPayrollDetail> kPaging,
			Long groupStaffPayrollId) throws DataAccessException;
	
	List<Staff> getListStaffWithOutGroupStaffPayroll(KPaging<Staff> kPaging, Long groupStaffPayrollId,
			String staffCode, String staffName, Long staffTypeId, Long shopId) throws DataAccessException;
	
	GroupStaffPayrollDetail getGroupStaffPayrollDetail(
			Long groupStaffPayrollId, Long staffId) throws DataAccessException;
}

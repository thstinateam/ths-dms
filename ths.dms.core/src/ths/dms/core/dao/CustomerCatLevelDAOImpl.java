package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.CustomerCatLevel;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ParamDAOGetCustomerCatLevelImportVO;
import ths.dms.core.entities.vo.CustomerCatLevelImportVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class CustomerCatLevelDAOImpl implements CustomerCatLevelDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public CustomerCatLevel createCustomerCatLevel(CustomerCatLevel customerCatLevel, LogInfoVO logInfo) throws DataAccessException {
		if (customerCatLevel == null) {
			throw new IllegalArgumentException("customerCatLevel");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (customerCatLevel.getCustomer() != null && customerCatLevel.getSaleLevelCat() != null) {
			if (checkIfRecordExist(customerCatLevel.getCustomer().getId(), customerCatLevel.getSaleLevelCat().getId(), null)) {
				return null;
			}
		}
		else
			return null;
		repo.create(customerCatLevel);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, customerCatLevel, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(CustomerCatLevel.class, customerCatLevel.getId());
	}

	@Override
	public void deleteCustomerCatLevel(CustomerCatLevel customerCatLevel, LogInfoVO logInfo) throws DataAccessException {
		if (customerCatLevel == null) {
			throw new IllegalArgumentException("customerCatLevel");
		}
		repo.delete(customerCatLevel);
		try {
			actionGeneralLogDAO.createActionGeneralLog(customerCatLevel, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public CustomerCatLevel getCustomerCatLevelById(long id) throws DataAccessException {
		return repo.getEntityById(CustomerCatLevel.class, id);
	}
	
	@Override
	public void updateCustomerCatLevel(CustomerCatLevel customerCatLevel, LogInfoVO logInfo) throws DataAccessException {
		if (customerCatLevel == null) {
			throw new IllegalArgumentException("customerCatLevel");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (customerCatLevel.getCustomer() != null && customerCatLevel.getSaleLevelCat() != null) {
			if (checkIfRecordExist(customerCatLevel.getCustomer().getId(), customerCatLevel.getSaleLevelCat().getId(), customerCatLevel.getId())) {
				return ;
			}
		}
		else
			return ;
		CustomerCatLevel temp = this.getCustomerCatLevelById(customerCatLevel.getId());
		repo.update(customerCatLevel);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, customerCatLevel, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public List<CustomerCatLevel> getListCustomerCatLevel(KPaging<CustomerCatLevel> kPaging,
			Long customerId, Long saleLevelCatId, Boolean isOrderByName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from customer_cat_level ccl join sale_level_cat slc on ccl.sale_level_cat_id = slc.sale_level_cat_id ");
		sql.append(" join product_info pi on pi.product_info_id = slc.cat_id where 1=1");
		List<Object> params = new ArrayList<Object>();
		if (customerId != null) {
			sql.append(" and ccl.customer_id=?");
			params.add(customerId);
		}
		if (saleLevelCatId != null) {
			sql.append(" and ccl.sale_level_cat_id=?");
			params.add(saleLevelCatId);
		}
		if (Boolean.TRUE.equals(isOrderByName))
			sql.append(" order by NLSSORT(product_info_name,'NLS_SORT=vietnamese')");
		else
			sql.append(" order by customer_cat_level_id desc");
		if (kPaging == null)
			return repo.getListBySQL(CustomerCatLevel.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(CustomerCatLevel.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public Boolean checkIfRecordExist(long customerId, long saleLevelCatId, Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(1) as count from customer_cat_level c ");
		sql.append(" where customer_id=? and sale_level_cat_id=?");
		
		List<Object> params = new ArrayList<Object>();
		params.add(customerId);
		params.add(saleLevelCatId);
		
		if (id != null) {
			sql.append(" and customer_cat_level_id != ?");
			params.add(id);
		}
		
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true; 
	}
	
	@Override
	public CustomerCatLevel getCustomerCatLevel(Long customerId, Long catId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from customer_cat_level ccl join sale_level_cat slc on ccl.sale_level_cat_id = slc.sale_level_cat_id");
		sql.append(" where ccl.customer_id = ? and slc.cat_id = ?");
		params.add(customerId);
		params.add(catId);
		return repo.getFirstBySQL(CustomerCatLevel.class, sql.toString(), params);
	}

	@Override
	public List<CustomerCatLevelImportVO> getCustomerCatLevelImportVO(
			ParamDAOGetCustomerCatLevelImportVO funcParams)
			throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("  SELECT C.CUSTOMER_CODE AS customerCode, ");
		sql.append("          PI.PRODUCT_INFO_NAME AS productInfoName, ");
		sql.append("          SLC.SALE_LEVEL_CODE AS saleLevel, ");
		sql.append("          S.SHOP_CODE AS shopCode");
		
		fromSql.append("     FROM SHOP S, ");
		fromSql.append("          CUSTOMER C, ");
		fromSql.append("          CUSTOMER_CAT_LEVEL CCL, ");
		fromSql.append("          SALE_LEVEL_CAT SLC, ");
		fromSql.append("          PRODUCT_INFO PI ");
		fromSql.append("    WHERE S.SHOP_ID = C.SHOP_ID ");
		fromSql.append("          AND C.CUSTOMER_ID = CCL.CUSTOMER_ID ");
		fromSql.append("          AND CCL.SALE_LEVEL_CAT_ID = SLC.SALE_LEVEL_CAT_ID ");
		fromSql.append("          AND SLC.CAT_ID = PI.PRODUCT_INFO_ID ");
		// sql.append("          AND PI.STATUS = 1 ");
		// sql.append("          AND PI.TYPE = 1 ");
		// sql.append("          AND SLC.STATUS = 1 ");

		if (!StringUtility.isNullOrEmpty(funcParams.getShortCode())) {
			fromSql.append(" AND C.SHORT_CODE LIKE ? ESCAPE '/' ");

			params.add(StringUtility.toOracleSearchLikeSuffix(funcParams
					.getShortCode().toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getCustomerName())) {
			fromSql.append(" AND C.NAME_TEXT LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English
					.codau2khongdau(funcParams.getCustomerName().toUpperCase())));
		}

		if (funcParams.getLstAreaCode() != null
				&& funcParams.getLstAreaCode().size() > 0) {
			String str = " AND (";

			for (String areaCode : funcParams.getLstAreaCode()) {
				str += " EXISTS (SELECT 1 FROM AREA A WHERE A.AREA_ID = C.AREA_ID AND AREA_CODE = ?) OR ";
				params.add(areaCode);
			}

			str = str.substring(0, str.length() - 4);
			str += ")";

			fromSql.append(str);
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getPhone())) {
			fromSql.append(" AND LOWER(C.PHONE) LIKE ? ESCAPE '/' ");

			params.add(StringUtility.toOracleSearchLike(funcParams.getPhone()
					.toLowerCase()));
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getMobiPhone())) {
			fromSql.append(" AND LOWER(C.MOBIPHONE) LIKE ? ESCAPE '/' ");

			params.add(StringUtility.toOracleSearchLike(funcParams
					.getMobiPhone().toLowerCase()));
		}

		if (funcParams.getStatus() != null) {
			fromSql.append(" AND C.STATUS = ?");

			params.add(funcParams.getStatus().getValue());
		}

		if (funcParams.getCustomerTypeId() != null) {
			fromSql.append(" AND C.CHANNEL_TYPE_ID = ?");

			params.add(funcParams.getCustomerTypeId());
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getRegion())) {
			fromSql.append(" AND LOWER(C.REGION) LIKE ? ESCAPE '/' ");

			params.add(StringUtility.toOracleSearchLike(funcParams.getRegion()
					.toLowerCase()));
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getLoyalty())) {
			fromSql.append(" AND LOWER(C.LOYALTY) LIKE ?");

			params.add(StringUtility.toOracleSearchLike(funcParams.getLoyalty()
					.toLowerCase()));
		}

		if (funcParams.getGroupTransferId() != null) {
			fromSql.append(" AND C.GROUP_TRANSFER_ID = ?");

			params.add(funcParams.getGroupTransferId());
		}

		if (funcParams.getCashierId() != null) {
			fromSql.append(" AND C.CASHIER_STAFF_ID = ?");

			params.add(funcParams.getCashierId());
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getAddress())) {
			fromSql.append(" AND LOWER(C.ADDRESS) LIKE ? ESCAPE '/' ");

			params.add(StringUtility.toOracleSearchLike(funcParams.getAddress()
					.toLowerCase()));
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getStreet())) {
			fromSql.append(" AND LOWER(C.STREET) LIKE ? ESCAPE '/' ");

			params.add(StringUtility.toOracleSearchLike(funcParams.getStreet()
					.toLowerCase()));
		}

		if (funcParams.getShopId() != null) {
			fromSql.append(" AND (C.SHOP_ID IN (SELECT SHOP_ID FROM SHOP ");
			fromSql.append(" 	START WITH PARENT_SHOP_ID = ? AND STATUS = 1");
			fromSql.append(" 	CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID) OR C.SHOP_ID = ?)");
			params.add(funcParams.getShopId());
			params.add(funcParams.getShopId());
		} else {
			fromSql.append(" AND (C.SHOP_ID IN (SELECT SHOP_ID FROM SHOP ");
			fromSql.append(" 	START WITH PARENT_SHOP_ID IS NULL AND STATUS = 1");
			fromSql.append(" 	CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID))");
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getDirectShopCode())) {
			fromSql.append(" AND EXISTS (SELECT 1 FROM SHOP S WHERE C.SHOP_ID = S.SHOP_ID AND UPPER(S.SHOP_CODE) = ?)");
			params.add(funcParams.getDirectShopCode().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getShopCodeLike())) {
			fromSql.append(" AND EXISTS (SELECT 1 FROM SHOP S WHERE C.SHOP_ID = S.SHOP_ID AND S.SHOP_CODE LIKE ?)");
			params.add(StringUtility.toOracleSearchLikeSuffix(funcParams
					.getShopCodeLike().toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getDisplay())) {
			fromSql.append(" AND UPPER(C.DISPLAY) = ?");
			params.add(funcParams.getDisplay().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getLocation())) {
			fromSql.append(" AND UPPER(C.LOCATION) = ?");
			params.add(funcParams.getLocation().toUpperCase());
		}

		if (funcParams.getNotInGroupTransferId() != null) {
			fromSql.append(" AND (C.GROUP_TRANSFER_ID != ? OR C.GROUP_TRANSFER_ID IS NULL)");
			params.add(funcParams.getNotInGroupTransferId());
		}

		sql.append(fromSql.toString());
		sql.append("   ORDER BY CUSTOMERCODE, PRODUCTINFONAME, SALELEVEL ");
		
		StringBuilder countSql = new StringBuilder(" select count(1) as count ");
		countSql.append(fromSql);

		final String[] fieldNames = new String[] { //
		"customerCode",// 1
				"productInfoName",// 2
				"saleLevel",// 3
				"shopCode"// 4
		};

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING // 4
		};

		if (null == funcParams.getkPaging()) {
			return repo.getListByQueryAndScalar(CustomerCatLevelImportVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(
					CustomerCatLevelImportVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params,
					funcParams.getkPaging());
		}
	}
}

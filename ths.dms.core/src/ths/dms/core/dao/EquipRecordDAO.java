package ths.dms.core.dao;

/**
 * Import thu vien ho tro
 * */
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipLiquidationFormDtl;
import ths.dms.core.entities.EquipLostMobileRec;
import ths.dms.core.entities.EquipLostRecord;
import ths.dms.core.entities.EquipStatisticRecDtl;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipStatisticFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.vo.EquipLostMobileRecVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipStatisticRecordDetailVO;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentEvictionVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.StatisticCheckingVO;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Danh cho tat ca cac lien quan den Bien ban thiet bi DAO
 * 
 * @author hunglm16
 * @since December 14,2014
 * */
public interface EquipRecordDAO {
	/**
	 * Them moi bien thong tin ban Bao Mat
	 * 
	 * @author hunglm16
	 * @since December 31,2014
	 * */
	EquipLostRecord createEquipLostRecord(EquipLostRecord equipLostRecord) throws DataAccessException;

	/**
	 * Cap nhat bien thong tin ban Bao Mat
	 * 
	 * @author hunglm16
	 * @since January 01,2014
	 * */
	void updateEquipLostRecord(EquipLostRecord equipLostRecord) throws DataAccessException;

	/**
	 * Tim kiem Bien Ban Bao Mat
	 * 
	 * @author hunglm16
	 * @since December 31,2014
	 * */
	List<EquipRecordVO> searchListEquipLostRecordVOByFilter(EquipRecordFilter<EquipRecordVO> filter) throws DataAccessException;

	/**
	 * Tim kiem hinh anh cho cac bien ban thiet bi
	 * 
	 * @author hunglm16
	 * @since December 31,2014
	 * */
	List<EquipAttachFile> searchEquipAttachFileByFilter(BasicFilter<EquipAttachFile> filter) throws DataAccessException;

	/**
	 * Tim kiem Bien ban thiet bi Entites
	 * 
	 * @author hunglm16
	 * @since December 31,2014
	 * */
	List<EquipLostRecord> searchEquipLostRecordByFilter(EquipRecordFilter<EquipLostRecord> filter) throws DataAccessException;

	/**
	 * Lay Bien Ban Bao Mat Entites
	 * 
	 * @author hunglm16
	 * @since December 31,2014
	 * @param id
	 * */
	EquipLostRecord getEquipLostRecordById(Long id) throws DataAccessException;
	
	/**
	 * Lay Bien Ban Bao Mat Entites thuoc shopId
	 * @author dungnt19
	 * @param id
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 */
	EquipLostRecord getEquipLostRecordById(Long id, Long shopId) throws DataAccessException;

	/**
	 * CHi tiet thiet bi cho bao mat
	 * 
	 * @author tientv11
	 * @param id
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipmentVO> getListEquipmentVOByEquiId(Long id) throws DataAccessException;

	/**
	 * Lay danh sach thiet bi
	 * 
	 * @author nhutnn
	 * @since 06/01/2015
	 * @param kPaging
	 * @param equipmentFilter
	 * @throws DataAccessException
	 */
	List<EquipmentDeliveryVO> getListEquipmentLiquidationVOByFilter(EquipmentFilter<EquipmentDeliveryVO> filter) throws DataAccessException;

	/***
	 * @author vuongmq
	 * @date 08/07/2015
	 * Lay danh sach thiet bi pop up cua thanh ly tai san
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipmentDeliveryVO> getListEquipmentLiquidationPopup(EquipmentFilter<EquipmentDeliveryVO> filter) throws DataAccessException;
	
	/**
	 * Lay danh sach thiet bi bao mat tu may tinh bang
	 * 
	 * @author tientv11
	 * @since 07/01/2015
	 * @param equipmentFilter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipLostMobileRecVO> getListEquipLostMobileRecByFilter(EquipmentFilter<EquipLostMobileRecVO> equipmentFilter) throws DataAccessException;

	/**
	 * Lay Max Code Bien Ban Bao Mat Theo Tien To
	 * 
	 * @author hunglm16
	 * @since January 08,2014
	 * */
	String getMaxCodeEquipLostRecord(String perfix) throws DataAccessException;

	/**
	 * Lay Bien Ban Bao Mat tu Mobile Entites
	 * 
	 * @author hunglm16
	 * @since January 08,2014
	 * @param id
	 * */
	EquipLostMobileRec getEquipLostMobileRecById(Long id) throws DataAccessException;
	EquipLostMobileRec getEquipLostMobileRecById(Long id, Long shopId) throws DataAccessException;

	/**
	 * Cap nhat Bien Ban Bao Mat tu Mobile Entites
	 * 
	 * @author hunglm16
	 * @since January 08,2014
	 * */
	void updateEquipLostMobileRec(EquipLostMobileRec equipLostMobileRec) throws DataAccessException;

	EquipAttachFile getEquipAttachFileById(Long id) throws DataAccessException;

	EquipStatisticRecDtl getEquipStatisticRecDtlById(Long id) throws DataAccessException;

	/**
	 * In Bien Ban Bao Mat
	 * 
	 * @author hunglm16
	 * @since January 15,2014
	 * */
	List<EquipRecordVO> printfEquipLostRecordVOByFilter(EquipRecordFilter<EquipRecordVO> filter) throws DataAccessException;

	
	/**
	 * Dem so so dong trong Bien ban bao mat tu Mobile theo Filter
	 * 
	 * @author hunglm16
	 * @since January 19,2014
	 * */
	Integer countEquipLostMobileRecByFilter(EquipRecordFilter<EquipRecordVO> filter) throws DataAccessException;
	/**
	 * Lay danh sach Bien ban bao mat tu Mobile theo Filter
	 * 
	 * @author hunglm16
	 * @since January 19,2014
	 * */
	List<EquipLostMobileRec> searchEquipRecordFilter(EquipRecordFilter<EquipLostMobileRec> filter) throws DataAccessException;
	
	/**
	 * Them moi Tap tin vao EquipAttachFile
	 * 
	 * @author hunglm16
	 * @since Feb 03,2014
	 * */
	EquipAttachFile createEquipAttachFile(EquipAttachFile equipAttFile) throws DataAccessException;

	/**
	 * Lay danh sach tap tin EquipAttachFile
	 * 
	 * @author hunglm16
	 * @since Feb 03,2014
	 * */
	List<FileVO> searchListFileVOByFilter(EquipRecordFilter<FileVO> filter) throws DataAccessException;
	/**
	 * Xoa tap tin EquipAttachFile
	 * 
	 * @author hunglm16
	 * @since Feb 03,2014
	 * */
	void deletEquipAttachFile(EquipAttachFile equipAttachFile) throws DataAccessException;

	/**
	 * get list detail. if not exists in equip_statistic_rec_dtl => insert new
	 * @author phut
	 */
	List<StatisticCheckingVO> getListDetailGroupSoLan(KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId, Long equipGroupId, String equipCode, String seri, Long stockId, Integer stepCheck)
			throws DataAccessException;

	/**
	 * get list detail. if not exists in equip_statistic_rec_dtl => insert new
	 * @author phut
	 */
	List<StatisticCheckingVO> getListDetailShelfSoLan(
			KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId, Long productId, Long stockId, Integer stepCheck)
			throws DataAccessException;

	/**
	 * get list detail. if not exists in equip_statistic_rec_dtl => insert new
	 * @author phut
	 */
	List<EquipStatisticRecDtl> getListEquipStatisticRecDetail(
			KPaging<EquipStatisticRecDtl> kPaging, Long recordId, Long shopId,
			Long stockId, Long objectId) throws DataAccessException;

	/**
	 * get list detail. if not exists in equip_statistic_rec_dtl => insert new
	 * @author phut
	 */
	List<StatisticCheckingVO> getListDetailShelfTuyen(
			KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId,
			Long productId, Long stockId, Date checkDate)
			throws DataAccessException;

	/**
	 * get list detail. if not exists in equip_statistic_rec_dtl => insert new
	 * @author phut
	 */
	List<StatisticCheckingVO> getListDetailGroupTuyen(
			KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId,
			Long equipGroupId, String equipCode, String seri, Long stockId,
			Date checkDate) throws DataAccessException;
	/**
	 * Lay danh sach thong tin kiem ke u ke
	 * 
	 * @author phuongvm
	 * @since 22/04/2015
	 * */
	List<StatisticCheckingVO> getListDetailShelf(EquipStatisticFilter filter) throws DataAccessException;
	/**
	 * Lay danh sach thong tin kiem ke nhom thiet bi
	 * 
	 * @author phuongvm
	 * @since 23/04/2015
	 * */
	List<StatisticCheckingVO> getListDetailGroup(EquipStatisticFilter filter) throws DataAccessException;
	/**
	 * Lay chi tiet kiem ke
	 * 
	 * @author phuongvm
	 * @since 27/04/2015
	 * */
	EquipStatisticRecDtl geEquipStatisticRecDtlByFilter(EquipStatisticFilter filter) throws DataAccessException;

	/**
	 * Tim kiem danh sach thiet bi bien ban sua chua, 
	 * lay theo phan quyen; popup thiet bi
	 * @author vuongmq
	 * @date 04/05/2015
	 */
	List<EquipmentDeliveryVO> getListEquipmentLostPopUp(EquipmentFilter<EquipmentDeliveryVO> filter) throws DataAccessException;

	/***
	 * @author vuongmq
	 * @date 04/05/2015
	 * cap nhat  lay kho popup cua Thiet bi, theo phan quyen
	 */
	List<EquipStockVO> getListStockLostByFilter(EquipStockFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach bien ban thu hoi da huy tu bien ban de nghi thu hoi
	 * @author nhutnn
	 * @since 27/07/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipmentEvictionVO> getListFormEvictionVOForEquipSuggestEvictionByFilter(EquipmentFilter<EquipmentEvictionVO> filter) throws DataAccessException;
	
	/**
	 * Lay bien ban bao mat moi nhat dua vao thiet bi
	 * 
	 * @author phuongvm
	 * @param equipId
	 * @return EquipRecordVO
	 * @throws DataAccessException
	 * @since 20/11/2015
	 */
	EquipRecordVO getLostRecordNewestByEquipId(Long equipId) throws DataAccessException;
	
	/**
	 * Lay danh sach chi tiet bien ban thanh ly dua vao id bien ban
	 * 
	 * @author phuongvm
	 * @param recordId
	 * @return danh sach chi tiet bien ban thanh ly
	 * @throws DataAccessException
	 * @since 20/11/2015
	 */
	List<EquipLiquidationFormDtl> getListEquipLiquidationFormDtlByRecordId(Long recordId) throws DataAccessException;
	
	/**
	 * Kiem tra ton tai so hop dong cua bien ban giao nhan
	 * 
	 * @author phuongvm
	 * @param contractNumber
	 * @return true: ton tai, false: ko ton tai
	 * @throws DataAccessException
	 * @since 21/12/2015
	 */
	Boolean isExistContractNumInDeliveryRecord(String contractNumber) throws DataAccessException;

	/**
	 * lay thong tin so lan kiem ke
	 * Xu ly getListStatisticTime
	 * @author trietptm
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @since Mar 11, 2016
	*/
	List<EquipStatisticRecordDetailVO> getListStatisticTime(EquipStatisticFilter filter) throws DataAccessException;
}

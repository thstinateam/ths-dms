package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedMap;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ActionLog;
import ths.dms.core.entities.ParentStaffMap;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffGroupDetail;
import ths.dms.core.entities.StaffPositionLog;
import ths.dms.core.entities.StaffSaleCat;
import ths.dms.core.entities.StaffType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffFilterNew;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.filter.ImageFilter;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.filter.StaffTypeFilter;
import ths.dms.core.entities.filter.SupFilter;
import ths.dms.core.entities.vo.AmountPlanStaffDetailVO;
import ths.dms.core.entities.vo.AmountPlanStaffVO;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.NVBHSaleVO;
import ths.dms.core.entities.vo.ParentStaffMapVO;
import ths.dms.core.entities.vo.ShopTreeVO;
import ths.dms.core.entities.vo.StaffComboxVO;
import ths.dms.core.entities.vo.StaffExportVO;
import ths.dms.core.entities.vo.StaffGroupDetailVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.entities.vo.StaffPositionVOEx;
import ths.dms.core.entities.vo.StaffSimpleVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.SupervisorSaleVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.core.entities.vo.rpt.RptCustomerNotVisitOfSalesVO;
import ths.core.entities.vo.rpt.RptExImStockOfStaffDataVO;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;
import ths.dms.core.filter.UnitFilter;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class StaffDAOImpl implements StaffDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	private CommonDAO commonDAO;

	@Autowired
	private SaleDayDAO saleDayDAO;

	@Autowired
	private ExceptionDayDAO exceptionDayDAO;

	@Override
	public Staff createStaff(Staff staff, LogInfoVO logInfo) throws DataAccessException {
		if (staff == null) {
			throw new IllegalArgumentException("staff");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (staff.getStaffCode() != null)
			staff.setStaffCode(staff.getStaffCode().toUpperCase());
		if (staff.getSaleTypeCode() != null)
			staff.setSaleTypeCode(staff.getSaleTypeCode().toUpperCase());
		if(staff.getStaffName()!=null){
			staff.setNameText(Unicode2English.codau2khongdau(staff.getStaffName()).toUpperCase());
		}
		staff.setCreateUser(logInfo.getStaffCode());
		staff.setCreateDate(commonDAO.getSysDate());
		repo.create(staff);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, staff, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(Staff.class, staff.getId());
	}

	@Override
	public void deleteStaff(Staff staff, LogInfoVO logInfo) throws DataAccessException {
		if (staff == null) {
			throw new IllegalArgumentException("staff");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(staff);
		try {
			actionGeneralLogDAO.createActionGeneralLog(staff, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Staff getStaffById(long id) throws DataAccessException {
		return repo.getEntityById(Staff.class, id);
	}

	@Override
	public ParentStaffMap getParentStaffMapById(long id) throws DataAccessException {
		return repo.getEntityById(ParentStaffMap.class, id);
	}

	@Override
	public void updateStaff(Staff staff, LogInfoVO logInfo) throws DataAccessException {
		if (staff == null) {
			throw new IllegalArgumentException("staff");
		}
		if (staff.getStaffCode() != null)
			staff.setStaffCode(staff.getStaffCode().toUpperCase());
		if (staff.getSaleTypeCode() != null)
			staff.setSaleTypeCode(staff.getSaleTypeCode().toUpperCase());
		if(staff.getStaffName()!=null){
			staff.setNameText(Unicode2English.codau2khongdau(staff.getStaffName()).toUpperCase());
		}
		Staff temp = this.getStaffById(staff.getId());

		staff.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null)
			staff.setUpdateUser(logInfo.getStaffCode());
		temp = temp.clone();
		repo.update(staff);
		/***liemtpt: tam thoi dong source ben duoi lai vi bi loi null pointer ***/
		/*try {
			actionGeneralLogDAO.createActionGeneralLog(temp, staff, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}*/
	}

	@Override
	public ParentStaffMap createParentStaffMap(ParentStaffMap psm, LogInfoVO logInfo) throws DataAccessException {
		if (psm == null) {
			throw new IllegalArgumentException("ParentStaffMap");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		psm.setCreateUser(logInfo.getStaffCode());
		psm.setCreateDate(commonDAO.getSysDate());
		repo.create(psm);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, psm, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(ParentStaffMap.class, psm.getId());
	}

	@Override
	public void deleteParentStaffMap(ParentStaffMap psm, LogInfoVO logInfo) throws DataAccessException {
		if (psm == null) {
			throw new IllegalArgumentException("ParentStaffMap");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(psm);
		try {
			actionGeneralLogDAO.createActionGeneralLog(psm, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public void updateParentStaffMap(ParentStaffMap psm, LogInfoVO logInfo) throws DataAccessException {
		if (psm == null) {
			throw new IllegalArgumentException("ParentStaffMap");
		}
		ParentStaffMap temp = this.getParentStaffMapById(psm.getId());

		psm.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null)
			psm.setUpdateUser(logInfo.getStaffCode());
		temp = temp.clone();
		repo.update(psm);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, psm, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public void deleteParentByStaff(Long staffId,List<Long> lstParentId,  LogInfoVO logInfo) throws DataAccessException {
		try {
			if (staffId == null) {
				throw new IllegalArgumentException("staffId is null");
			}
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append(" update parent_staff_map set status=0,to_date=?, update_date=?, update_user=? ");
			params.add(commonDAO.getSysDate());
			params.add(commonDAO.getSysDate());
			params.add(logInfo.getStaffCode());
			sql.append("        where staff_id = ?");
			params.add(staffId);
			if(lstParentId!=null && lstParentId.size()>0){
				sql.append(" and parent_staff_id not in (-1");
				for(Long id: lstParentId){
					sql.append(",?");
					params.add(id);
				}
				sql.append(")");
			}
			repo.executeSQLQuery(sql.toString(), params);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Staff getStaffByCode(String code) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from STAFF where lower(staff_code) = ? and status != ?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.trim().toLowerCase());
		params.add(ActiveType.DELETED.getValue());
		Staff rs = repo.getEntityBySQL(Staff.class, sql, params);
		return rs;
	}

	@Override
	public Staff getStaffLogin(String code, String password) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from STAFF where lower(staff_code)=? and password=? and status=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.trim().toLowerCase());
		params.add(password);
		params.add(ActiveType.RUNNING.getValue());
		Staff rs = repo.getEntityBySQL(Staff.class, sql, params);
		return rs;
	}

	@Override
	public Staff getStaffByInfoBasic(String code, Long shopId, ActiveType status) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from STAFF where staff_code=? and shop_id=? and status=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(shopId);
		params.add(status.getValue());
		Staff rs = repo.getEntityBySQL(Staff.class, sql, params);
		return rs;
	}

	/**
	 * @author hunglm16
	 * @modify date MAY 18, 2014
	 * @description Lay danh sach Nhan vien voi Danh sach Ma Code
	 */
	//	@Override
	//	public List<SaleOrder> getListStaffByListStaffCode(List<String> lstStaffCode) throws DataAccessException {
	//		if (lstStaffCode == null || lstStaffCode.size()==0) {
	//			throw new IllegalArgumentException("lstDebitdetailId is null");
	//		}
	//		StringBuilder sql = new StringBuilder();
	//		List<Object> params = new ArrayList<Object>();
	//		sql.append(" select * from STAFF ");
	//		sql.append(" where 1 = 1 ");
	//		sql.append(" and staff_code in ('-1' ");
	//		for(int i=0; i<lstStaffCode.size(); i++){
	//			sql.append("  ,? ");
	//			params.add(lstStaffCode.get(i));
	//		}
	//		sql.append("  ) ");
	//		return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
	//	}

	@Override
	public List<Staff> getListStaff(StaffFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select st.* from STAFF st join shop s on st.shop_id = s.shop_id where (s.status = 1");

		if (!StringUtility.isNullOrEmpty(filter.getStaffOwnerCode())) {
			sql.append(" and exists (select 1 from staff pr where pr.staff_id = st.staff_owner_id and pr.staff_code = ?)");
			params.add(filter.getStaffOwnerCode().toUpperCase());
		}

		if (filter.getShopId() != null) {
			sql.append(" and st.shop_id = ? ");
			params.add(filter.getShopId());
		}

		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and st.staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffName())) {
			String temp = StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getStaffName().toLowerCase()));
			sql.append(" and lower(st.name_text) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (!StringUtility.isNullOrEmpty(filter.getMobilePhoneNum())) {
			String temp = StringUtility.toOracleSearchLike(filter.getMobilePhoneNum().toLowerCase());
			sql.append(" and lower(st.MOBILEPHONE) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (filter.getStatus() != null) {
			sql.append(" and st.status=?");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and st.status<>?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		
		//Nutifood join to StaffType
		/*if (filter.getStaffTypeCode() != null) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status != ? and st.STAFF_TYPE_ID=ct.channel_type_id and ct.channel_type_code=?)");
			params.add(ActiveType.DELETED.getValue());
			params.add(filter.getStaffTypeCode());
		}
		if (filter.getChannelTypeType() != null) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status = 1 and st.STAFF_TYPE_ID=ct.channel_type_id and ct.type=?)");
			params.add(filter.getChannelTypeType().getValue());
		}
		if (filter.getLstChannelObjectType() != null && filter.getLstChannelObjectType().size() > 0) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status = 1 and st.STAFF_TYPE_ID = ct.channel_type_id and (");
			for (Integer channelObjectType : filter.getLstChannelObjectType()) {
				if (channelObjectType != null) {
					sql.append(" ct.object_type=? or ");
					params.add(channelObjectType);
				}
			}
			sql.append("1=0))");
		}*/
		
		//longnh15 add: check phan quyen
		 if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
				String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "st.staff_id");
				sql.append(paramsFix);
		}
		//end
		 
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			if (Boolean.TRUE.equals(filter.getIsGetShopOnly())) {
				sql.append(" and s.shop_code = ? and s.status = ?");
				params.add(filter.getShopCode().toUpperCase());
				params.add(ActiveType.RUNNING.getValue());
			} else if (Boolean.TRUE.equals(filter.getApplyUnitTree())) {
				sql.append(" and staff_id in (select STAFF_ID from staff_group_detail sgd join staff_group sg on sgd.staff_group_id = sg.staff_group_id where sg.status = 1 and sgd.status = 1 and sg.shop_id in (select shop_id from shop start with shop_id = (select shop_id from shop where shop_code =?  and status = 1) connect by prior shop_id = parent_shop_id))");
				params.add(filter.getShopCode().toUpperCase());
			} else {
				sql.append(" and ( st.shop_id in (select shop_id from shop start with shop_code=? and status=? connect by prior shop_id=parent_shop_id and status=?)");
				params.add(filter.getShopCode().toUpperCase());
				params.add(ActiveType.RUNNING.getValue());
				params.add(ActiveType.RUNNING.getValue());
				//sql.append(" 	or STAFF_ID IN (select STAFF_ID from staff_group_detail sgd join staff_group sg on sgd.staff_group_id = sg.staff_group_id where sg.status = 1 and sgd.status = 1 and sg.shop_id in (select shop_id from shop start with shop_id = (select shop_id from shop where shop_code =?  and status = 1) connect by prior shop_id = parent_shop_id))");
				//params.add(filter.getShopCode().toUpperCase());
				sql.append(")");
			}
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			String temp = StringUtility.toOracleSearchLike(filter.getShopName().toUpperCase());
			sql.append(" and st.shop_id in (select shop_id from shop start with upper(shop_name) like ? and status=? connect by prior shop_id=parent_shop_id and status=?)");
			params.add(temp);
			params.add(ActiveType.RUNNING.getValue());
			params.add(ActiveType.RUNNING.getValue());
		}

		//Nutifood join to StaffType 
		/*if (filter.getStaffType() != null) {
			sql.append(" and exists (select 1 from staff_type sty where sty.status = ? and st.STAFF_TYPE_ID = sty.STAFF_TYPE_ID and sty.SPECIFIC_TYPE = ?");
			params.add(ActiveType.RUNNING.getValue());
			params.add(filter.getStaffType().getValue());
			sql.append(" and ct.type=?)");
			params.add(ChannelTypeType.STAFF.getValue());
		}*/

		if (filter.getSpecType() != null){
			sql.append(" and exists (select 1 from staff_type sty where sty.status = 1 and st.STAFF_TYPE_ID = sty.STAFF_TYPE_ID and sty.SPECIFIC_TYPE = ? ) ");
			params.add(filter.getSpecType().getValue());
		}
		if (filter.getLstSpecType() != null) {
			sql.append(" and exists (select 1 from staff_type sty where sty.status = 1 and st.STAFF_TYPE_ID = sty.STAFF_TYPE_ID and sty.SPECIFIC_TYPE IN (");
			int n = filter.getLstSpecType().size();
			for (int i = 0; i < n; i++){
				if (i < n - 1){
					sql.append(" ?, ");
					params.add(filter.getLstSpecType().get(i).getValue());
				} else {
					sql.append(" ? ");
					params.add(filter.getLstSpecType().get(i).getValue());
				}
			}
			sql.append(" 			) ");
			sql.append(" ) ");
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getStrSpecType())) {
			sql.append(" and exists (select 1 from staff_type sty where sty.status = 1 and st.STAFF_TYPE_ID = sty.STAFF_TYPE_ID and sty.SPECIFIC_TYPE IN ( ").append(filter.getStrSpecType());
			sql.append(" ) ");
			sql.append(" ) ");
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getSaleTypeCode())) {
			sql.append(" and st.sale_type_code =? ");
			params.add(filter.getSaleTypeCode().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopCodeStaff())) {
			sql.append(" and exists (select 1 from shop s where s.shop_id = st.shop_id and s.status = ? and s.shop_code like ? ESCAPE '/' )");
			params.add(ActiveType.RUNNING.getValue());
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCodeStaff().toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopNameStaff())) {
			sql.append(" and exists (select 1 from shop s where s.shop_id = st.shop_id and s.status = ? and upper(s.shop_name) like ? ESCAPE '/' )");
			params.add(ActiveType.RUNNING.getValue());
			params.add(StringUtility.toOracleSearchLike(filter.getShopNameStaff().toUpperCase()));
		}
		if (Boolean.TRUE.equals(filter.getNotInStaffGroup())) {
			sql.append(" and not exists (select 1 from staff_group_detail sgd join staff_group sg ON sgd.staff_group_id = sg.staff_group_id");
			sql.append("			where sg.status =1 and sgd.status =1 and sgd.staff_id = st.staff_id)");
		}
		if (filter.getStaffGroupId() != null) {
			sql.append(" and exists (select 1 from staff_group_detail where status = 1 and staff_id = st.staff_id and staff_group_id = ?)");
			params.add(filter.getStaffGroupId());
		}

		if (Boolean.TRUE.equals(filter.getIsInAnyStaffGroup())) {
			sql.append(" and exists (select 1 from staff_group_detail where status = 1 and staff_id = st.staff_id)");
		}

		if (filter.getLstExceptId() != null && filter.getLstExceptId().size() > 0) {
			sql.append(" and st.staff_id not in (-1");
			for (Long stId : filter.getLstExceptId()) {
				sql.append(",?");
				params.add(stId);
			}
			sql.append(")");
		}

		sql.append(")");

		if (filter.getLstMandatoryId() != null && filter.getLstMandatoryId().size() > 0) {
			sql.append(" or st.staff_id in (-1");
			for (Long stId : filter.getLstMandatoryId()) {
				sql.append(",?");
				params.add(stId);
			}
			sql.append(")");
		}

		sql.append(" order by staff_code asc");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, filter.getkPaging());
	}
	
	@Override
	public List<Staff> getListStaffNew(StaffFilterNew filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select st.* from STAFF st join shop s on st.shop_id = s.shop_id where (s.status = 1");

		if (!StringUtility.isNullOrEmpty(filter.getStaffOwnerCode())) {
			sql.append(" and exists (select 1 from staff pr where pr.staff_id = st.staff_owner_id and pr.staff_code = ?)");
			params.add(filter.getStaffOwnerCode().toUpperCase());
		}

		if (filter.getShopId() != null) {
			sql.append(" and st.shop_id = ? ");
			params.add(filter.getShopId());
		}

		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and st.staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffName())) {
			String temp = StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getStaffName().toLowerCase()));
			sql.append(" and lower(st.name_text) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (!StringUtility.isNullOrEmpty(filter.getMobilePhoneNum())) {
			String temp = StringUtility.toOracleSearchLike(filter.getMobilePhoneNum().toLowerCase());
			sql.append(" and lower(st.MOBILEPHONE) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (filter.getStatus() != null) {
			sql.append(" and st.status=?");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and st.status<>?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getStaffTypeCode() != null) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status != ? and st.STAFF_TYPE_ID=ct.channel_type_id and ct.channel_type_code=?)");
			params.add(ActiveType.DELETED.getValue());
			params.add(filter.getStaffTypeCode());
		}
		if (filter.getChannelTypeType() != null) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status = 1 and st.STAFF_TYPE_ID=ct.channel_type_id and ct.type=?)");
			params.add(filter.getChannelTypeType().getValue());
		}
		if (filter.getLstChannelObjectType() != null && filter.getLstChannelObjectType().size() > 0) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status = 1 and st.STAFF_TYPE_ID = ct.channel_type_id and (");
			for (Integer channelObjectType : filter.getLstChannelObjectType()) {
				if (channelObjectType != null) {
					sql.append(" ct.object_type=? or ");
					params.add(channelObjectType);
				}
			}
			sql.append("1=0))");
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			if (Boolean.TRUE.equals(filter.getIsGetShopOnly())) {
				sql.append(" and s.shop_code = ? and s.status = ?");
				params.add(filter.getShopCode().toUpperCase());
				params.add(ActiveType.RUNNING.getValue());
			} else if (Boolean.TRUE.equals(filter.getApplyUnitTree())) {
				sql.append(" and staff_id in (select STAFF_ID from staff_group_detail sgd join staff_group sg on sgd.staff_group_id = sg.staff_group_id where sg.status = 1 and sgd.status = 1 and sg.shop_id in (select shop_id from shop start with shop_id = (select shop_id from shop where shop_code =?  and status = 1) connect by prior shop_id = parent_shop_id))");
				params.add(filter.getShopCode().toUpperCase());
			} else {
				sql.append(" and ( st.shop_id in (select shop_id from shop start with shop_code=? and status=? connect by prior shop_id=parent_shop_id and status=?)");
				params.add(filter.getShopCode().toUpperCase());
				params.add(ActiveType.RUNNING.getValue());
				params.add(ActiveType.RUNNING.getValue());
				//sql.append(" 	or STAFF_ID IN (select STAFF_ID from staff_group_detail sgd join staff_group sg on sgd.staff_group_id = sg.staff_group_id where sg.status = 1 and sgd.status = 1 and sg.shop_id in (select shop_id from shop start with shop_id = (select shop_id from shop where shop_code =?  and status = 1) connect by prior shop_id = parent_shop_id))");
				//params.add(filter.getShopCode().toUpperCase());
				sql.append(")");
			}
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			String temp = StringUtility.toOracleSearchLike(filter.getShopName().toUpperCase());
			sql.append(" and st.shop_id in (select shop_id from shop start with upper(shop_name) like ? and status=? connect by prior shop_id=parent_shop_id and status=?)");
			params.add(temp);
			params.add(ActiveType.RUNNING.getValue());
			params.add(ActiveType.RUNNING.getValue());
		}

		if (filter.getStaffType() != null) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status = ? and st.STAFF_TYPE_ID=ct.channel_type_id and ct.object_type IN (-1"); 
			params.add(ActiveType.RUNNING.getValue());
			for(int i = 0; i < filter.getStaffType().length; i++) {
				sql.append(",?");
				params.add(filter.getStaffType()[i].getValue());
			}
			sql.append(")");
//			if(StaffObjectType.NVBH.equals(staffType)){
//				sql.append(" or ct.object_type = ?)");
//				params.add(StaffObjectType.NVVS.getValue());
//			}
//			else {
//				sql.append(" )");
//			}
			sql.append(" and ct.type=?)");
			params.add(ChannelTypeType.STAFF.getValue());
		}
		/** vuongmq; Oct 13, 2014, Danh sach Object type Staff*/
		if (filter.getLstStaffType() != null) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status = 1 and st.STAFF_TYPE_ID = ct.channel_type_id and (");
			for (Integer staffType : filter.getLstStaffType()) {
				if (staffType != null) {
					sql.append(" ct.object_type=? or ");
					params.add(staffType);
				}
			}
			sql.append(" 1=0 ) ");
			sql.append(" and ct.type=? )");
			params.add(ChannelTypeType.STAFF.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getSaleTypeCode())) {
			sql.append(" and st.sale_type_code =? ");
			params.add(filter.getSaleTypeCode().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopCodeStaff())) {
			sql.append(" and exists (select 1 from shop s where s.shop_id = st.shop_id and s.status = ? and s.shop_code like ? ESCAPE '/' )");
			params.add(ActiveType.RUNNING.getValue());
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCodeStaff().toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopNameStaff())) {
			sql.append(" and exists (select 1 from shop s where s.shop_id = st.shop_id and s.status = ? and upper(s.shop_name) like ? ESCAPE '/' )");
			params.add(ActiveType.RUNNING.getValue());
			params.add(StringUtility.toOracleSearchLike(filter.getShopNameStaff().toUpperCase()));
		}
		if (Boolean.TRUE.equals(filter.getNotInStaffGroup())) {
			sql.append(" and not exists (select 1 from staff_group_detail sgd join staff_group sg ON sgd.staff_group_id = sg.staff_group_id");
			sql.append("			where sg.status =1 and sgd.status =1 and sgd.staff_id = st.staff_id)");
		}
		if (filter.getStaffGroupId() != null) {
			sql.append(" and exists (select 1 from staff_group_detail where status = 1 and staff_id = st.staff_id and staff_group_id = ?)");
			params.add(filter.getStaffGroupId());
		}

		if (Boolean.TRUE.equals(filter.getIsInAnyStaffGroup())) {
			sql.append(" and exists (select 1 from staff_group_detail where status = 1 and staff_id = st.staff_id)");
		}

		if (filter.getLstExceptId() != null && filter.getLstExceptId().size() > 0) {
			sql.append(" and st.staff_id not in (-1");
			for (Long stId : filter.getLstExceptId()) {
				sql.append(",?");
				params.add(stId);
			}
			sql.append(")");
		}

		sql.append(")");

		if (filter.getLstMandatoryId() != null && filter.getLstMandatoryId().size() > 0) {
			sql.append(" or st.staff_id in (-1");
			for (Long stId : filter.getLstMandatoryId()) {
				sql.append(",?");
				params.add(stId);
			}
			sql.append(")");
		}

		sql.append(" order by staff_code asc");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, filter.getkPaging());
	}

	@Override
	public List<Staff> getListStaffForExportExcel(StaffFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select st.* from STAFF st join shop s on st.shop_id = s.shop_id where (s.status = 1");

		if (!StringUtility.isNullOrEmpty(filter.getStaffOwnerCode())) {
			sql.append(" and exists (select 1 from staff pr where pr.staff_id = st.staff_owner_id and pr.staff_code = ?)");
			params.add(filter.getStaffOwnerCode().toUpperCase());
		}

		if (filter.getShopId() != null) {
			sql.append(" and st.shop_id = ? ");
			params.add(filter.getShopId());
		}

		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and st.staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffName())) {
			String temp = StringUtility.toOracleSearchLike(filter.getStaffName().toLowerCase());
			sql.append(" and lower(st.staff_name) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (!StringUtility.isNullOrEmpty(filter.getMobilePhoneNum())) {
			String temp = StringUtility.toOracleSearchLike(filter.getMobilePhoneNum().toLowerCase());
			sql.append(" and lower(st.MOBILEPHONE) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (filter.getStatus() != null) {
			sql.append(" and st.status=?");
			params.add(filter.getStatus().getValue());
		}
		if (filter.getStaffTypeCode() != null) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status != ? and st.STAFF_TYPE_ID=ct.channel_type_id and ct.channel_type_code=?)");
			params.add(ActiveType.DELETED.getValue());
			params.add(filter.getStaffTypeCode());
		}
		if (filter.getChannelTypeType() != null) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status = 1 and st.STAFF_TYPE_ID=ct.channel_type_id and ct.type=?)");
			params.add(filter.getChannelTypeType().getValue());
		}
		if (filter.getLstChannelObjectType() != null && filter.getLstChannelObjectType().size() > 0) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status = 1 and st.STAFF_TYPE_ID = ct.channel_type_id and (");
			for (Integer channelObjectType : filter.getLstChannelObjectType()) {
				if (channelObjectType != null) {
					sql.append(" ct.object_type=? or ");
					params.add(channelObjectType);
				}
			}
			sql.append("1=0))");
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			if (Boolean.TRUE.equals(filter.getIsGetShopOnly())) {
				sql.append(" and s.shop_code = ? and s.status = ?");
				params.add(filter.getShopCode().toUpperCase());
				params.add(ActiveType.RUNNING.getValue());
			} else if (Boolean.TRUE.equals(filter.getApplyUnitTree())) {
				sql.append(" and staff_id in (select STAFF_ID from staff_group_detail sgd join staff_group sg on sgd.staff_group_id = sg.staff_group_id where sg.status = 1 and sgd.status = 1 and sg.shop_id in (select shop_id from shop start with shop_id = (select shop_id from shop where shop_code =?  and status = 1) connect by prior shop_id = parent_shop_id))");
				params.add(filter.getShopCode().toUpperCase());
			} else {
				sql.append(" and ( st.shop_id in (select shop_id from shop start with shop_code=? and status=? connect by prior shop_id=parent_shop_id and status=?)");
				params.add(filter.getShopCode().toUpperCase());
				params.add(ActiveType.RUNNING.getValue());
				params.add(ActiveType.RUNNING.getValue());
				sql.append(" 	or STAFF_ID IN (select STAFF_ID from staff_group_detail sgd join staff_group sg on sgd.staff_group_id = sg.staff_group_id where sg.status = 1 and sgd.status = 1 and sg.shop_id in (select shop_id from shop start with shop_id = (select shop_id from shop where shop_code =?  and status = 1) connect by prior shop_id = parent_shop_id)))");
				params.add(filter.getShopCode().toUpperCase());
			}
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			String temp = StringUtility.toOracleSearchLike(filter.getShopName().toUpperCase());
			sql.append(" and st.shop_id in (select shop_id from shop start with upper(shop_name) like ? and status=? connect by prior shop_id=parent_shop_id and status=?)");
			params.add(temp);
			params.add(ActiveType.RUNNING.getValue());
			params.add(ActiveType.RUNNING.getValue());
		}

		if (filter.getStaffType() != null) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status = ? and st.STAFF_TYPE_ID=ct.channel_type_id and ct.object_type=?");
			params.add(ActiveType.RUNNING.getValue());
			params.add(filter.getStaffType().getValue());
			//			if(StaffObjectType.NVBH.equals(staffType)){
			//				sql.append(" or ct.object_type = ?)");
			//				params.add(StaffObjectType.NVVS.getValue());
			//			}
			//			else {
			//				sql.append(" )");
			//			}
			sql.append(" and ct.type=?)");
			params.add(ChannelTypeType.STAFF.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getSaleTypeCode())) {
			sql.append(" and st.sale_type_code =? ");
			params.add(filter.getSaleTypeCode().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopCodeStaff())) {
			sql.append(" and exists (select 1 from shop s where s.shop_id = st.shop_id and s.status = ? and s.shop_code like ? ESCAPE '/' )");
			params.add(ActiveType.RUNNING.getValue());
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCodeStaff().toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopNameStaff())) {
			sql.append(" and exists (select 1 from shop s where s.shop_id = st.shop_id and s.status = ? and upper(s.shop_name) like ? ESCAPE '/' )");
			params.add(ActiveType.RUNNING.getValue());
			params.add(StringUtility.toOracleSearchLike(filter.getShopNameStaff().toUpperCase()));
		}
		if (Boolean.TRUE.equals(filter.getNotInStaffGroup())) {
			sql.append(" and not exists (select 1 from staff_group_detail sgd join staff_group sg ON sgd.staff_group_id = sg.staff_group_id");
			sql.append("			where sg.status =1 and sgd.status =1 and sgd.staff_id = st.staff_id)");
		}
		if (filter.getStaffGroupId() != null) {
			sql.append(" and exists (select 1 from staff_group_detail where status = 1 and staff_id = st.staff_id and staff_group_id = ?)");
			params.add(filter.getStaffGroupId());
		}

		if (Boolean.TRUE.equals(filter.getIsInAnyStaffGroup())) {
			sql.append(" and exists (select 1 from staff_group_detail where status = 1 and staff_id = st.staff_id)");
		}

		if (filter.getLstExceptId() != null && filter.getLstExceptId().size() > 0) {
			sql.append(" and st.staff_id not in (-1");
			for (Long stId : filter.getLstExceptId()) {
				sql.append(",?");
				params.add(stId);
			}
			sql.append(")");
		}

		sql.append(")");

		if (filter.getLstMandatoryId() != null && filter.getLstMandatoryId().size() > 0) {
			sql.append(" or st.staff_id in (-1");
			for (Long stId : filter.getLstMandatoryId()) {
				sql.append(",?");
				params.add(stId);
			}
			sql.append(")");
		}

		if (StaffObjectType.NVGH.equals(filter.getStaffType()))
			sql.append(" order by s.shop_code, staff_code");
		else
			sql.append(" order by s.shop_code,staff_code asc");
		if (filter.getkPaging() == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, filter.getkPaging());
	}

	@Override
	public List<StaffGroupDetail> getListStaffGroupDetail(StaffFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT sgd.* ");
		sql.append(" FROM staff st ");
		sql.append(" JOIN staff_group_detail sgd ");
		sql.append(" ON sgd.staff_id = st.staff_id ");
		sql.append(" JOIN staff_group sg ");
		sql.append(" ON sg.staff_group_id = sgd.staff_group_id ");
		sql.append(" JOIN shop sh ");
		sql.append(" ON sh.shop_id = sg.shop_id ");
		sql.append(" JOIN channel_type ct ");
		sql.append(" ON ct.channel_type_id = st.staff_type_id ");
		sql.append(" WHERE st.status =1 ");
		sql.append(" AND sgd.status =1 ");
		sql.append(" AND sg.status = 1");
		sql.append(" AND ct.type = 2 ");
		sql.append(" AND sh.status =1 ");

		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and st.staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffName())) {
			String temp = StringUtility.toOracleSearchLike(filter.getStaffName().toLowerCase());
			sql.append(" and lower(st.staff_name) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			if (Boolean.TRUE.equals(filter.getIsGetShopOnly())) {
				sql.append(" and s.shop_code = ? and s.status = ?");
				params.add(filter.getShopCode().toUpperCase());
				params.add(ActiveType.RUNNING.getValue());
			} else {
				sql.append(" and ( st.shop_id in (select shop_id from shop start with shop_code=? and status=1 connect by prior shop_id=parent_shop_id and status=1))");
				params.add(filter.getShopCode().toUpperCase());
			}
		}
		sql.append(" order by sh.shop_code,st.staff_code,st.staff_name,ct.channel_type_id ");

		if (filter.getSgdPaging() == null)
			return repo.getListBySQL(StaffGroupDetail.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StaffGroupDetail.class, sql.toString(), params, filter.getSgdPaging());
	}

	//	@Override
	//	public List<StaffGroupDetail> getListStaffGroupDetail(StaffFilter filter) throws DataAccessException{
	//		StringBuilder sql = new StringBuilder();
	//		List<Object> params = new ArrayList<Object>();
	//		sql.append("select sgd.* from STAFF st join shop s on st.shop_id = s.shop_id join staff_group_detail sgd on st.staff_id = sgd.staff_id and sgd.status = 1 and exists (select 1 from staff_group sg where sg.stafF_group_id = sgd.staff_group_id and sg.status = 1) where (s.status = 1");
	//		
	//		if (!StringUtility.isNullOrEmpty(filter.getStaffOwnerCode())) {
	//			sql.append(" and exists (select 1 from staff pr where pr.staff_id = st.staff_owner_id and pr.staff_code = ?)");
	//			params.add(filter.getStaffOwnerCode().toUpperCase());
	//		}
	//		
	//		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
	//			sql.append(" and st.staff_code like ? ESCAPE '/'");
	//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().toUpperCase()));
	//		}
	//		if (!StringUtility.isNullOrEmpty(filter.getStaffName())) {
	//			String temp = StringUtility.toOracleSearchLike(filter.getStaffName().toLowerCase());
	//			sql.append(" and lower(st.staff_name) like ? ESCAPE '/'");
	//			params.add(temp);
	//		}
	//		if (!StringUtility.isNullOrEmpty(filter.getMobilePhoneNum())) {
	//			String temp = StringUtility.toOracleSearchLike(filter.getMobilePhoneNum().toLowerCase());
	//			sql.append(" and lower(st.MOBILEPHONE) like ? ESCAPE '/'");
	//			params.add(temp);
	//		}
	//		if (filter.getStatus() != null) {
	//			sql.append(" and st.status=?");
	//			params.add(filter.getStatus().getValue());
	//		}
	//		if (filter.getStaffTypeCode() != null) {
	//			sql.append(" and exists (select 1 from channel_type ct where ct.status != ? and st.STAFF_TYPE_ID=ct.channel_type_id and ct.channel_type_code=?)");
	//			params.add(ActiveType.DELETED.getValue());
	//			params.add(filter.getStaffTypeCode());
	//		}
	//		if (filter.getChannelTypeType() != null) {
	//			sql.append(" and exists (select 1 from channel_type ct where ct.status = 1 and st.STAFF_TYPE_ID=ct.channel_type_id and ct.type=?)");
	//			params.add(filter.getChannelTypeType().getValue());
	//		}
	//		if (filter.getLstChannelObjectType() != null && filter.getLstChannelObjectType().size() > 0) {
	//			sql.append(" and exists (select 1 from channel_type ct where ct.status = 1 and st.STAFF_TYPE_ID = ct.channel_type_id and (");
	//			for (Integer channelObjectType: filter.getLstChannelObjectType()) {
	//				if (channelObjectType != null) {
	//					 sql.append(" ct.object_type=? or ");
	//					 params.add(channelObjectType);
	//				}
	//			}
	//			sql.append("1=0))");
	//		}
	//		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
	//			if (Boolean.TRUE.equals(filter.getIsGetShopOnly())) {
	//				sql.append(" and s.shop_code = ? and s.status = ?");
	//				params.add(filter.getShopCode().toUpperCase());
	//				params.add(ActiveType.RUNNING.getValue());
	//			}
	//			else {
	//				sql.append(" and ( st.shop_id in (select shop_id from shop start with shop_code=? and status=? connect by prior shop_id=parent_shop_id and status=?)");
	//				params.add(filter.getShopCode().toUpperCase());
	//				params.add(ActiveType.RUNNING.getValue());
	//				params.add(ActiveType.RUNNING.getValue());
	//				sql.append(" 	or st.STAFF_ID IN (select STAFF_ID from staff_group_detail sgd join staff_group sg on sgd.staff_group_id = sg.staff_group_id where sg.status = 1 and sgd.status = 1 and sg.shop_id in (select shop_id from shop start with shop_id = (select shop_id from shop where shop_code =?  and status = 1) connect by prior shop_id = parent_shop_id)))");
	//				params.add(filter.getShopCode().toUpperCase());
	//			}
	//		}
	//		
	//		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
	//			String temp = StringUtility.toOracleSearchLike(filter.getShopName().toUpperCase());
	//			sql.append(" and st.shop_id in (select shop_id from shop start with upper(shop_name) like ? and status=? connect by prior shop_id=parent_shop_id and status=?)");
	//			params.add(temp);
	//			params.add(ActiveType.RUNNING.getValue());
	//			params.add(ActiveType.RUNNING.getValue());
	//		}
	//		
	//		if (filter.getStaffType() != null) {
	//			sql.append(" and exists (select 1 from channel_type ct where ct.status = ? and st.STAFF_TYPE_ID=ct.channel_type_id and ct.object_type=?"); 
	//			params.add(ActiveType.RUNNING.getValue());
	//			params.add(filter.getStaffType().getValue());
	////			if(StaffObjectType.NVBH.equals(staffType)){
	////				sql.append(" or ct.object_type = ?)");
	////				params.add(StaffObjectType.NVVS.getValue());
	////			}
	////			else {
	////				sql.append(" )");
	////			}
	//			sql.append(" and ct.type=?)");
	//			params.add(ChannelTypeType.STAFF.getValue());
	//		}
	//		if (!StringUtility.isNullOrEmpty(filter.getSaleTypeCode())) {
	//			sql.append(" and st.sale_type_code =? ");
	//			params.add(filter.getSaleTypeCode().toUpperCase());
	//		}
	//		
	//		if (!StringUtility.isNullOrEmpty(filter.getShopCodeStaff())) {
	//			sql.append(" and exists (select 1 from shop s where s.shop_id = st.shop_id and s.status = ? and s.shop_code like ? ESCAPE '/' )");
	//			params.add(ActiveType.RUNNING.getValue());
	//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCodeStaff().toUpperCase()));
	//		}
	//		
	//		if (!StringUtility.isNullOrEmpty(filter.getShopNameStaff())) {
	//			sql.append(" and exists (select 1 from shop s where s.shop_id = st.shop_id and s.status = ? and upper(s.shop_name) like ? ESCAPE '/' )");
	//			params.add(ActiveType.RUNNING.getValue());
	//			params.add(StringUtility.toOracleSearchLike(filter.getShopNameStaff().toUpperCase()));
	//		}
	//		if (Boolean.TRUE.equals(filter.getNotInStaffGroup())) {
	//			sql.append(" and not exists (select 1 from staff_group_detail sgd join staff_group sg ON sgd.staff_group_id = sg.staff_group_id");
	//			sql.append("			where sg.status =1 and sgd.status =1 and sgd.staff_id = st.staff_id)");
	//		}
	//		if (filter.getStaffGroupId() != null) {
	//			sql.append(" and exists (select 1 from staff_group_detail where status = 1 and staff_id = st.staff_id and staff_group_id = ?)");
	//			params.add(filter.getStaffGroupId());
	//		}
	//		
	//		if (Boolean.TRUE.equals(filter.getIsInAnyStaffGroup())) {
	//			sql.append(" and exists (select 1 from staff_group_detail where status = 1 and staff_id = st.staff_id)");
	//		}
	//		
	//		if (filter.getLstExceptId() != null && filter.getLstExceptId().size() > 0) {
	//			sql.append(" and st.staff_id not in (-1");
	//			for (Long stId: filter.getLstExceptId()) {
	//				sql.append(",?");
	//				params.add(stId);
	//			}
	//			sql.append(")");
	//		}
	//		
	//		sql.append(")");
	//		
	//		if (filter.getLstMandatoryId() != null && filter.getLstMandatoryId().size() > 0) {
	//			sql.append(" or st.staff_id in (-1");
	//			for (Long stId: filter.getLstMandatoryId()) {
	//				sql.append(",?");
	//				params.add(stId);
	//			}
	//			sql.append(")");
	//		}
	//		
	//		
	//		sql.append(" order by shop_code, staff_code");
	//		if (filter.getSgdPaging() == null)
	//			return repo.getListBySQL(StaffGroupDetail.class, sql.toString(), params);
	//		else
	//			return repo.getListBySQLPaginated(StaffGroupDetail.class, sql.toString(), params, filter.getSgdPaging());
	//	}

	@Override
	public Boolean isUsingByOthers(long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from staff ");
		sql.append(" where ");
		sql.append("   exists (select 1 from Customer where CASHIER_STAFF_ID=? and status != ?) ");
		params.add(staffId);
		params.add(ActiveType.DELETED.getValue());
		//		sql.append("   or exists (select 1 from group_transfer where staff_id=? and status != ?) ");
		//		params.add(staffId);
		//		params.add(ActiveType.DELETED.getValue());
		sql.append("   or exists (select 1 from staff where staff_owner_id=? and status != ?) ");
		params.add(staffId);
		params.add(ActiveType.DELETED.getValue());
		//		sql.append("   or exists (select 1 from display_customer_score where staff_id=?) ");
		//		params.add(staffId);
		sql.append("   or exists (select 1 from display_staff_map where staff_id=? and status != ?) ");
		params.add(staffId);
		params.add(ActiveType.DELETED.getValue());
		//		sql.append("   or exists (select 1 from stock_total where OWNER_ID=? and OWNER_TYPE=?)");
		//		params.add(staffId);
		//		params.add(StockObjectType.STAFF.getValue());
		//		sql.append("   or exists (select 1 from stock_trans where (FROM_OWNER_ID=? and FROM_OWNER_TYPE=?) or (TO_OWNER_ID=? and TO_OWNER_TYPE=?) or (staff_id=?))");
		//		params.add(staffId);
		//		params.add(StockObjectType.STAFF.getValue());
		//		params.add(staffId);
		//		params.add(StockObjectType.STAFF.getValue());
		//		params.add(staffId);
		//		sql.append("   or exists (select 1 from product_lot where OWNER_ID=? and OWNER_TYPE=?)");
		//		params.add(staffId);
		//		params.add(StockObjectType.STAFF.getValue());
		//		sql.append("   or exists (select 1 from staff_sale_cat where staff_id=?) ");
		//		params.add(staffId);
		//		sql.append("   or exists (select 1 from po_auto where staff_id=?) ");
		//		params.add(staffId);
		//		sql.append("   or exists (select 1 from sale_order_detail where staff_id=?) ");
		//		params.add(staffId);
		//		sql.append("   or exists (select 1 from sale_order_lot where staff_id=?) ");
		//		params.add(staffId);
		sql.append("   or exists (select 1 from sale_order where staff_id=?) ");
		params.add(staffId);

		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public List<Staff> getListStaffByOwnerId(Long ownerId) throws DataAccessException {
		String sql = "select * from STAFF where STAFF_OWNER_ID = ? AND status = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(ownerId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getListBySQL(Staff.class, sql, params);
	}

	@Override
	public List<Staff> getListStaffNotManagedByOwnerId(KPaging<Staff> kPaging, Long ownerId, String shopCode, String shopName, String staffCode, String staffName, Long parentShopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();//phut: sua thanh (st.STAFF_OWNER_ID != ? OR st.STAFF_OWNER_ID is null) moi dung
		sql.append(" select * from STAFF st join shop sh on st.shop_id = sh.shop_id join channel_type ct on ct.channel_type_id = st.staff_type_id");
		sql.append(" where (st.STAFF_OWNER_ID != ? OR st.STAFF_OWNER_ID is null) AND st.status = ? and sh.status = ? and ct.status = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(ownerId);
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());

		sql.append(" and ct.type = ? and ct.object_type = ?");
		params.add(ChannelTypeType.STAFF.getValue());
		params.add(StaffObjectType.NVBH.getValue());

		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and sh.shop_code like ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and upper(sh.shop_name) like ?");
			params.add(StringUtility.toOracleSearchLike(shopName.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and st.staff_code like ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and upper(st.staff_name) like ?");
			params.add(StringUtility.toOracleSearchLike(staffName.toUpperCase()));
		}
		if (parentShopId != null) {
			sql.append(" and st.shop_id in (select sh.shop_id from shop sh start with sh.shop_id = ? connect by prior shop_id = parent_shop_id)");
			params.add(parentShopId);
		}

		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<Staff> getListStaffByOwnerId(KPaging<Staff> kPaging, Long ownerId, String shopCode, String shopName, String staffCode, String staffName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from STAFF st join shop sh on st.shop_id = sh.shop_id where st.STAFF_OWNER_ID = ? AND st.status = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(ownerId);
		params.add(ActiveType.RUNNING.getValue());

		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and sh.shop_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and upper(sh.shop_name) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(shopName.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and st.staff_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and upper(st.staff_name) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(staffName.toUpperCase()));
		}
		sql.append(" order by st.staff_code");
		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<Staff> getListNVBHInTuyen(KPaging<Staff> kPaging, Long gsnppId, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select st.* from STAFF st join shop sh on st.shop_id = sh.shop_id ");
		sql.append(" where 1 = 1 ");
		if (shopId != null) {
			sql.append(" and sh.shop_id=?  ");
			params.add(shopId);
		}
		if (gsnppId != null) {
			sql.append(" and st.STAFF_ID in (  ");
			sql.append("  select psm.staff_id as staff_id from parent_staff_map psm where psm.status = 1  ");
			sql.append("     and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id)  ");
			sql.append("     start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id  ");
			params.add(gsnppId);
			sql.append(" )  ");
		}
		sql.append(" and st.status = 1 and sh.status = 1 ");
		sql.append(" and exists (select 1 from channel_type ct where ct.status = ? and st.STAFF_TYPE_ID=ct.channel_type_id and ct.object_type in (?, ?)");
		params.add(ActiveType.RUNNING.getValue());
		params.add(StaffObjectType.NVBH.getValue());
		params.add(StaffObjectType.NVVS.getValue());
		sql.append(" and ct.type=?)");
		params.add(ChannelTypeType.STAFF.getValue());
		sql.append(" order by st.staff_code");
		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<Staff> getListNVBHInTuyenNew(KPaging<Staff> kPaging, Long gsnppId, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("  select s.* from staff s join staff_type st on s.staff_type_id = st.STAFF_TYPE_ID   ");
		sql.append("  where 1 = 1   ");
		sql.append("  and s.status = 1  ");
		if (shopId != null) {
			sql.append("  and s.shop_id = ?  ");
			params.add(shopId);
		}
		//tungmt dmscore chua apply phan quyen
//		if (gsnppId != null) {
//			sql.append(" and s.staff_id not in (select staff_id from exception_user_access where status = 1 and  parent_staff_id = ?) ");
//			params.add(gsnppId);
//			sql.append(" and s.staff_id in (select staff_id from parent_staff_map where status = 1 start with parent_staff_id = ? connect by prior staff_id=parent_staff_id) ");
//			params.add(gsnppId);
//		}
		sql.append("  and st.SPECIFIC_TYPE  = ?  ");
		params.add(StaffSpecificType.STAFF.getValue());
		sql.append("  and st.status = 1  ");
		sql.append(" order by s.staff_code");
		if (kPaging == null) {
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
		}
	}

	@Override
	public List<Staff> getListNVBHForDisplayProgram(KPaging<Staff> kPaging, Long shopId, Long displayProgramId, String code, String name) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from STAFF st join shop sh on st.shop_id = sh.shop_id where sh.shop_id = ? AND st.status = 1 AND sh.status = 1");
		params.add(shopId);
		sql.append(" and exists (select 1 from channel_type ct where ct.status = 1 and ct.type=?  and st.STAFF_TYPE_ID=ct.channel_type_id and (ct.object_type=? or ct.object_type = ?))");
		params.add(ChannelTypeType.STAFF.getValue());
		params.add(StaffObjectType.NVBH.getValue());
		params.add(StaffObjectType.NVVS.getValue());
		sql.append(" and not exists (select 1 from display_staff_map where status = 1 and display_program_id = ? and staff_id = st.staff_id) ");
		params.add(displayProgramId);
		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and st.staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(code.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(name)) {
			String temp = StringUtility.toOracleSearchLike(name.toLowerCase());
			sql.append(" and lower(st.staff_name) like ? ESCAPE '/'");
			params.add(temp);
		}
		sql.append(" order by st.staff_code");
		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<Staff> getListStaffByShopId(KPaging<Staff> kPaging, Long shopId, ActiveType staffActiveType, List<StaffObjectType> channelType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT s.* ");
		sql.append(" FROM staff s JOIN channel_type ct ON (s.staff_type_id = ct.channel_type_id) ");
		sql.append(" WHERE 1 = 1 ");

		if (null != shopId) {
			sql.append(" 	AND s.shop_id = ?");
			params.add(shopId);
		}

		sql.append(" 	AND s.status = ?");
		if (null != staffActiveType) {
			params.add(staffActiveType.getValue());
		} else {
			params.add(ActiveType.RUNNING.getValue());
		}

		if (null != channelType && channelType.size() > 0) {
			sql.append(" 	AND ct.OBJECT_TYPE in (");

			for (int i = 0; i < channelType.size(); i++) {
				if (i < channelType.size() - 1) {
					sql.append("?, ");
				} else {
					sql.append("?)");
				}

				params.add(channelType.get(i).getValue());
			}
		}

		sql.append(" 	ORDER BY s.staff_code");

		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);

		//		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public List<Staff> getListStaffByShopId(KPaging<Staff> kPaging, String staffCode, String staffName, Long shopId, ActiveType staffActiveType, List<StaffObjectType> channelType, Boolean isHasChild, List<Long> listStaffOwnerIds)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT s.* ");
		sql.append(" FROM staff s JOIN channel_type ct ON (s.staff_type_id = ct.channel_type_id) ");
		sql.append(" WHERE 1 = 1 AND ct.status = 1");

		if (null != shopId) {
			if (Boolean.TRUE.equals(isHasChild)) {
				sql.append(" AND s.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
			} else {
				sql.append(" 	AND s.shop_id = ?");
			}
			params.add(shopId);
		}

		sql.append(" 	AND s.status = ?");
		if (null != staffActiveType) {
			params.add(staffActiveType.getValue());
		} else {
			params.add(ActiveType.RUNNING.getValue());
		}

		if (listStaffOwnerIds != null && listStaffOwnerIds.size() > 0) {
			sql.append(" AND s.staff_owner_id in (");
			for (Long staffOwnerId : listStaffOwnerIds) {
				sql.append(" ?, ");
				params.add(staffOwnerId);
			}
			sql.append(" -1)");
		}

		if (staffCode != null && !staffCode.equals("")) {
			sql.append(" and s.staff_code like ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}

		if (staffName != null && !staffName.equals("")) {
			sql.append(" and upper(s.staff_name) like ?");
			params.add(StringUtility.toOracleSearchLike(staffName.toUpperCase()));
		}
		sql.append(" 	AND ct.status = 1");
		if (null != channelType && channelType.size() > 0) {
			sql.append(" 	AND ct.OBJECT_TYPE in (");

			for (int i = 0; i < channelType.size(); i++) {
				if (i < channelType.size() - 1) {
					sql.append("?, ");
				} else {
					sql.append("?)");
				}

				params.add(channelType.get(i).getValue());
			}
		}

		sql.append(" 	ORDER BY s.staff_code");

		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);

		//		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public List<Staff> getListStaffByShopCode(KPaging<Staff> kPaging, String shopCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *");
		sql.append(" FROM staff s JOIN shop sh on s.shop_id = sh.shop_id");
		sql.append(" WHERE sh.shop_code = ?");
		params.add(shopCode.toUpperCase());
		sql.append(" AND s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	/**
	 * 
	 * @author sangtn
	 * @see Cho chức năng quản lý hình ảnh
	 * @since 25/01/2014
	 */
	@Override
	public List<Staff> getListStaffByShop(List<Long> lstShopId, ActiveType staffActiveType, List<StaffObjectType> objectTypes) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT s.* ");
		sql.append(" FROM staff s JOIN channel_type ct ON (s.staff_type_id = ct.channel_type_id) ");
		sql.append(" WHERE 1 = 1 ");
		if (lstShopId != null && lstShopId.size() > 0) {
			sql.append(" and shop_id in (select shop_id from shop where status=1 START WITH shop_id in ( ");
			for (int i = 0; i < lstShopId.size(); i++) {
				if (i == 0) {
					sql.append("?");
				} else {
					sql.append(",?");
				}
				params.add(lstShopId.get(i));
			}
			sql.append(" ) CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID) ");
		}

		sql.append(" 	AND s.status = ?");
		if (null != staffActiveType) {
			params.add(staffActiveType.getValue());
		} else {
			params.add(ActiveType.RUNNING.getValue());
		}

		if (null != objectTypes && objectTypes.size() > 0) {
			sql.append(" 	AND ct.OBJECT_TYPE in (");
			for (int i = 0; i < objectTypes.size(); i++) {
				if (i < objectTypes.size() - 1) {
					sql.append("?, ");
				} else {
					sql.append("?)");
				}
				params.add(objectTypes.get(i).getValue());
			}
		}
		sql.append(" 	ORDER BY s.staff_code");
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	/**
	 * @author sangtn
	 * @since 28-03-2014
	 * @description Get list staff for VO
	 * @note Expand from getListStaffByShop function
	 */
	@Override
	public List<StaffSimpleVO> getListStaffByShopCombo(List<Long> lstShopId, ActiveType staffActiveType, List<StaffObjectType> objectTypes) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT s.staff_id staffId, s.staff_code staffCode, s.staff_name staffName ");
		sql.append(" FROM staff s JOIN channel_type ct ON (s.staff_type_id = ct.channel_type_id) ");
		sql.append(" WHERE 1 = 1 ");
		if (lstShopId != null && lstShopId.size() > 0) {
			sql.append(" and shop_id in (select shop_id from shop where status=1 START WITH shop_id in ( ");
			for (int i = 0; i < lstShopId.size(); i++) {
				if (i == 0) {
					sql.append("?");
				} else {
					sql.append(",?");
				}
				params.add(lstShopId.get(i));
			}
			sql.append(" ) CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID) ");
		}

		sql.append(" 	AND s.status = ?");
		if (null != staffActiveType) {
			params.add(staffActiveType.getValue());
		} else {
			params.add(ActiveType.RUNNING.getValue());
		}

		if (null != objectTypes && objectTypes.size() > 0) {
			sql.append(" 	AND ct.OBJECT_TYPE in (");
			for (int i = 0; i < objectTypes.size(); i++) {
				if (i < objectTypes.size() - 1) {
					sql.append("?, ");
				} else {
					sql.append("?)");
				}
				params.add(objectTypes.get(i).getValue());
			}
		}
		sql.append(" 	ORDER BY s.staff_code");

		final String[] fieldNames = new String[] { "staffId", "staffCode", "staffName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(StaffSimpleVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<Staff> getListStaffByShopIdWithRunning(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from staff st where st.shop_id = ? ");
		params.add(shopId);
		sql.append(" and st.status = 1 ");
		sql.append(" 	ORDER BY st.staff_code");
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public List<Staff> getStaffMultilChoice(Long staffId, String staffCode, Long shopId, Integer status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from staff st where 1=1 ");
		if (staffId != null) {
			sql.append(" and st.staff_id = ? ");
			params.add(staffId);
		}
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and upper(st.staff_code) = ? ");
			params.add(staffCode.trim().toUpperCase());
		}
		if (shopId != null) {
			sql.append(" and st.shop_id = ? ");
			params.add(shopId);
		}
		if (status != null) {
			sql.append(" and st.status != ? ");
			params.add(status);
		} else {
			sql.append(" and st.status != -1 ");
		}
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public Staff getStaffByRoutingCustomer(Long customerId) throws DataAccessException {
		if (customerId == null)
			throw new IllegalArgumentException("customerId is null");
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT s.*");
		sql.append(" FROM ROUTING_CUSTOMER a,");
		sql.append("   visit_plan b,");
		sql.append("   routing c,");
		sql.append("   staff s");
		sql.append(" WHERE 1          =1");
		sql.append(" AND b.routing_id = a.routing_id");
		sql.append(" AND c.routing_id = a.routing_id");
		sql.append(" AND b.staff_id   = s.staff_id");
		sql.append(" and (");
		sql.append("   (a.sunday = 1 and to_number(to_char(trunc(sysdate), 'd')) = 1)");
		sql.append("   or (a.monday = 1 and to_number(to_char(trunc(sysdate), 'd')) = 2)");
		sql.append("   or (a.tuesday = 1 and to_number(to_char(trunc(sysdate), 'd')) = 3)");
		sql.append("   or (a.wednesday = 1 and to_number(to_char(trunc(sysdate), 'd')) = 4)");
		sql.append("   or (a.thursday = 1 and to_number(to_char(trunc(sysdate), 'd')) = 5)");
		sql.append("   or (a.friday = 1 and to_number(to_char(trunc(sysdate), 'd')) = 6)");
		sql.append("   or (a.saturday = 1 and to_number(to_char(trunc(sysdate), 'd')) = 7)");
		sql.append(" )");
		sql.append(" AND a.status                                                  = 1");
		sql.append(" AND b.status                                                  = 1");
		sql.append(" AND s.status                                                  = 1");
		sql.append(" AND c.status                                                  = 1");
		if (customerId != null) {
			sql.append(" AND a.customer_id                                             = ?");
			params.add(customerId);
			sql.append(" AND c.shop_id                                                 = (select shop_id from customer where customer_id = ?)");
			params.add(customerId);
		}

		sql.append(" AND b.from_date                                              <= TRUNC(sysdate)");
		sql.append(" AND (b.to_date                                               >= TRUNC(sysdate)");
		sql.append(" OR b.to_date                                                 IS NULL)");
		sql.append(" AND mod(TO_CHAR(sysdate,'iw')- a.start_week, a.week_interval) = 0");
		return repo.getEntityBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public List<Staff> getListStaffByRoutingCustomer(Long customerId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct s.* from routing_customer rc join visit_plan vp on vp.routing_id=rc.routing_id ");
		sql.append("       join staff s on s.staff_id=vp.staff_id ");
		sql.append(" where rc.customer_id=? ");//and rc.routing_id= to_number(TO_char(sysdate,'D')) ");
		sql.append("     and vp.from_date < trunc(sysdate) + 1 and vp.to_date >= trunc(sysdate) ");
		sql.append("     and vp.status=? ");
		params.add(customerId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public Boolean checkIfStaffExists(String email, String mobiphone, String idno, Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(*) as count from staff where 1=1");
		if (!StringUtility.isNullOrEmpty(email)) {
			sql.append(" and lower(email)=?");
			params.add(email.toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(mobiphone)) {
			sql.append(" and lower(MOBILEPHONE)=?");
			params.add(mobiphone.toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(idno)) {
			sql.append(" and lower(idno)=?");
			params.add(idno.toLowerCase());
		}
		if (id != null) {
			sql.append(" and staff_id != ?");
			params.add(id);
		}

		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public List<Staff> getListSupervisorByShop(KPaging<Staff> kPaging, String staffCode, String staffName, String childShopCode) throws DataAccessException {
		return this.getListSupervisorByShop(kPaging, staffCode, staffName, childShopCode, null);
	}

	@Override
	public List<Staff> getListSupervisorByShop(KPaging<Staff> kPaging, String staffCode, String staffName, String childShopCode, Boolean isGetUpperShop) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select *  ");
		sql.append(" from staff s join channel_type c on s.staff_type_id=c.channel_type_id  ");
		sql.append(" where object_type=? and c.type = 2 and c.status = ? ");
		params.add(StaffObjectType.NVGS.getValue());
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   and shop_id in (select shop_id from shop where status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("                   start with shop_code=? ");
		params.add(childShopCode);
		if (Boolean.TRUE.equals(isGetUpperShop)) {
			sql.append("                   connect by prior parent_shop_id = shop_id) ");
		} else
			sql.append("                   connect by prior shop_id = parent_shop_id) ");//PhuT sua

		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			String temp = StringUtility.toOracleSearchLike(staffName.toLowerCase());
			sql.append(" and lower(staff_name) like ? ESCAPE '/'");
			params.add(temp);
		}

		sql.append(" and s.status=?");
		params.add(ActiveType.RUNNING.getValue());

		sql.append(" order by staff_code asc");
		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<Staff> getListStaffNotInDisplayProgram(Long parentShopId, Long displayProgramId, String staffCode, String staffName, Integer limit, Boolean isGetChildOnly) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select *  ");
		sql.append(" from staff s ");
		sql.append(" where s.status != ? and s.staff_id not in (select staff_id from display_staff_map ds where ds.display_program_id = ? and ds.status != ?)  ");
		params.add(ActiveType.DELETED.getValue());
		params.add(displayProgramId);
		params.add(ActiveType.DELETED.getValue());

		// thuattq add: chi hien thi staff cua shop tham gia chuong trinh trung bay
		sql.append(" and s.shop_id in (select shop_id from shop start with shop_id in (select shop_id from display_shop_map where display_program_id = ? and status != ?) connect by prior shop_id = parent_shop_id) ");

		params.add(displayProgramId);
		params.add(ActiveType.DELETED.getValue());

		if (Boolean.TRUE.equals(isGetChildOnly)) {
			if (null != parentShopId) {
				sql.append(" and shop_id = ?");
				params.add(parentShopId);
			}
		} else {
			sql.append("   and shop_id in (select shop_id from shop ");
			if (parentShopId == null)
				sql.append("                   start with parent_shop_id is null ");
			else {
				sql.append("                   start with shop_id=? ");
				params.add(parentShopId);
			}
			sql.append("                   connect by prior shop_id=parent_shop_id) ");
		}

		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and lower(staff_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(staffName.toLowerCase()));
		}

		sql.append(" and s.status=?");
		params.add(ActiveType.RUNNING.getValue());

		if (limit != null) {
			sql.append(" and rownum <= ?");
			params.add(limit);
		}

		sql.append(" order by staff_code asc");
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public Staff getStaffNotInDisplayProgram(Long parentShopId, Long displayProgramId, String staffCode, String staffName, Integer limit, Boolean isGetChildOnly) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from (");
		sql.append(" select *  ");
		sql.append(" from staff s ");
		sql.append(" where s.status != ? and s.staff_id not in (select staff_id from display_staff_map ds where ds.display_program_id = ? and ds.status != ?)  ");
		params.add(ActiveType.DELETED.getValue());
		params.add(displayProgramId);
		params.add(ActiveType.DELETED.getValue());

		// thuattq add: chi hien thi staff cua shop tham gia chuong trinh trung bay
		sql.append(" and s.shop_id in (select distinct shop_id from shop start with shop_id in (select distinct shop_id from display_shop_map where display_program_id = ? and status != ?) connect by prior shop_id = parent_shop_id) ");
		params.add(displayProgramId);
		params.add(ActiveType.DELETED.getValue());

		if (Boolean.TRUE.equals(isGetChildOnly)) {
			sql.append(" and shop_id = ?");
			params.add(parentShopId);
		} else {
			sql.append("   and shop_id in (select shop_id from shop ");
			if (parentShopId == null)
				sql.append("                   start with parent_shop_id is null ");
			else {
				sql.append("                   start with shop_id=? ");
				params.add(parentShopId);
			}
			sql.append("                   connect by prior shop_id=parent_shop_id) ");
		}

		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and lower(staff_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(staffName.toLowerCase()));
		}

		sql.append(" and s.status=?");
		params.add(ActiveType.RUNNING.getValue());

		sql.append(" order by staff_code asc");
		sql.append(") where rownum = 1");
		// params.add(limit);

		return repo.getFirstBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public List<Staff> getSaleStaffByDate(RoutingCustomerFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.* ");
		sql.append(" from ROUTING_CUSTOMER a, visit_plan b, routing c, staff s, staff_type st");
		if (filter.getShopId() != null && filter.getRoleId() != null && filter.getUserId() != null) {
			sql.append(" , TABLE (F_GET_LIST_CHILD_STAFF_INHERIT(?, sysdate, ?, ?, NULL)) tmp ");
			params.add(filter.getUserId());
			params.add(filter.getRoleId());
			params.add(filter.getShopId());
		}
		sql.append(" where 1=1 ");
		if (filter.getShopId() != null && filter.getRoleId() != null && filter.getUserId() != null) {
			sql.append(" AND tmp.number_key_id = s.staff_id ");
		}
		sql.append(" and b.routing_id = a.routing_id ");
		sql.append(" and c.routing_id = a.routing_id ");
		sql.append(" and b.staff_id = s.staff_id ");
		sql.append(" and s.staff_type_id = st.staff_type_id ");
		sql.append(" and a.status = 1 ");
		sql.append(" and b.status = 1 ");
		sql.append(" and s.status = 1 ");
		sql.append(" and st.SPECIFIC_TYPE = ? ");
		params.add(StaffSpecificType.STAFF.getValue());
		sql.append(" and c.status = 1 ");
		sql.append(" and a.customer_id = ? ");
		params.add(filter.getCustomerId());
		sql.append(" and c.shop_id = ? ");
		params.add(filter.getShopId());
		sql.append(" and b.from_date < trunc(?) + 1 and (b.to_date >= trunc(?) or b.to_date is null) ");
		sql.append(" and a.start_date < trunc(?) + 1 and (a.end_date >= trunc(?) or a.end_date is null) ");
		params.add(filter.getOrderDate());
		params.add(filter.getOrderDate());
		params.add(filter.getOrderDate());
		params.add(filter.getOrderDate());
		//		sql.append(" and mod(to_char(?,'iw')- a.start_week, a.week_interval) = 0 ");
		//sql.append(" and (to_char(?,'iw')- a.start_week) >= 0 ");
		//params.add(orderDate);

		//sql.append("and mod((trunc(?,'iw') - trunc(to_date(a.start_date,'dd/mm/yyyy'),'iw')) /7, a.week_interval) = 0 ");
		//sql.append("and (trunc(?,'iw') - trunc(to_date(a.start_date,'dd/mm/yyyy'),'iw')) >= 0");

//		sql.append(" and (nvl(a.week1, 0) + nvl(a.week2, 0) + nvl(a.week3, 0) + nvl(a.week4, 0)) > 0  ");
//		sql.append(" and ( ");
//		sql.append("    ((mod(floor((trunc(?,  'iw') - trunc(a.start_date, 'iw')) / 7), 4) + 1) = 1 and a.week1 = 1)  ");
//		sql.append("    or ((mod(floor((trunc(?, 'iw') - trunc(a.start_date, 'iw')) / 7), 4) + 1) = 2 and a.week2 = 1)  ");
//		sql.append("    or ((mod(floor((trunc(?, 'iw') - trunc(a.start_date, 'iw')) / 7), 4) + 1) = 3 and a.week3 = 1)  ");
//		sql.append("    or ((mod(floor((trunc(?, 'iw') - trunc(a.start_date, 'iw')) / 7), 4) + 1) = 4 and a.week4 = 1)  ");
//		sql.append(" )  ");
//		params.add(orderDate);
//		params.add(orderDate);
//		params.add(orderDate);
//		params.add(orderDate);
		sql.append(" order by s.staff_code ");
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	
	@Override
	public Staff getSaleStaffByDateForCreateOrder(Long customerId, Long shopId, Date orderDate, Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.* ");
		sql.append(" from ROUTING_CUSTOMER a, visit_plan b, routing c, staff s, staff_type st");
		sql.append(" where 1=1 ");
		sql.append(" and b.routing_id = a.routing_id ");
		sql.append(" and c.routing_id = a.routing_id ");
		sql.append(" and b.staff_id = s.staff_id ");
		sql.append(" and s.staff_type_id=st.staff_type_id");
		sql.append(" and a.status = 1 ");
		sql.append(" and b.status = 1 ");
		sql.append(" and s.status = 1 ");
		sql.append(" and st.specific_type in (?)");
		params.add(StaffSpecificType.STAFF.getValue());
		sql.append(" and c.status = 1 ");
		sql.append(" and a.customer_id = ? ");
		params.add(customerId);
		sql.append(" and c.shop_id = ? ");
		params.add(shopId);
		sql.append(" and b.from_date < trunc(?) + 1 and (b.to_date >= trunc(?) or b.to_date is null) ");
		sql.append(" and a.start_date < trunc(?) + 1 and (a.end_date >= trunc(?) or a.end_date is null) ");
		params.add(orderDate);
		params.add(orderDate);
		params.add(orderDate);
		params.add(orderDate);
		
		sql.append(" and (nvl(a.week1, 0) + nvl(a.week2, 0) + nvl(a.week3, 0) + nvl(a.week4, 0)) > 0  ");
//		sql.append(" and ( ");
//		sql.append("    ((mod(floor((trunc(?,  'iw') - trunc(a.start_date, 'iw')) / 7), 4) + 1) = 1 and a.week1 = 1)  ");
//		sql.append("    or ((mod(floor((trunc(?, 'iw') - trunc(a.start_date, 'iw')) / 7), 4) + 1) = 2 and a.week2 = 1)  ");
//		sql.append("    or ((mod(floor((trunc(?, 'iw') - trunc(a.start_date, 'iw')) / 7), 4) + 1) = 3 and a.week3 = 1)  ");
//		sql.append("    or ((mod(floor((trunc(?, 'iw') - trunc(a.start_date, 'iw')) / 7), 4) + 1) = 4 and a.week4 = 1)  ");
//		sql.append(" )  ");
//
//		params.add(orderDate);
//		params.add(orderDate);
//		params.add(orderDate);
//		params.add(orderDate);
		
		sql.append(" AND ( ( ( MOD( ( ( floor((TRUNC(?,'dd') - TRUNC(a.start_date,'dd')) / 7)) + ( ");
		params.add(orderDate);
		sql.append("   CASE ");
		sql.append("     WHEN (mod(TRUNC(?,'dd')- TRUNC(a.start_date,'dd'), 7) > 0) ");
		params.add(orderDate);
		sql.append("     AND ( (CASE WHEN TO_CHAR(?,'D') = '1' THEN 8 ELSE TRUNC(TO_CHAR(?, 'D')) END )  ");
		params.add(orderDate);
		params.add(orderDate);
		sql.append("       < ( CASE WHEN TO_CHAR( a.start_date,'D') = '1' THEN 8 ELSE TRUNC(TO_CHAR( a.start_date,'D')) END ) ) ");
		sql.append("     THEN 1 ELSE 0 ");
		sql.append("   END) ), 4) + 1 ) = 1 ");
		sql.append(" AND week1         = 1 ) ");
		sql.append(" OR ( ( MOD( ( ( floor((TRUNC(?,'dd') - TRUNC(a.start_date,'dd')) / 7)) + ( ");
		params.add(orderDate);
		sql.append("   CASE ");
		sql.append("     WHEN (mod(TRUNC(?,'dd')- TRUNC(a.start_date,'dd'), 7) > 0) ");
		params.add(orderDate);
		sql.append("     AND ( (CASE WHEN TO_CHAR(?,'D') = '1' THEN 8 ELSE TRUNC(TO_CHAR(?, 'D')) END )  ");
		params.add(orderDate);
		params.add(orderDate);
		sql.append("     < (CASE WHEN TO_CHAR( a.start_date,'D') = '1' THEN 8 ELSE TRUNC(TO_CHAR( a.start_date,'D')) END ) ) ");
		sql.append("     THEN 1 ELSE 0 ");
		sql.append("   END) ), 4) + 1 ) = 2 ");
		sql.append(" AND week2         = 1) ");
		sql.append(" OR ( ( MOD( ( ( floor((TRUNC(?,'dd') - TRUNC(a.start_date,'dd')) / 7)) + ( ");
		params.add(orderDate);
		sql.append("   CASE WHEN (mod(TRUNC(?,'dd')- TRUNC(a.start_date,'dd'), 7) > 0) ");
		params.add(orderDate);
		sql.append("     AND ( (CASE WHEN TO_CHAR(?,'D') = '1' THEN 8 ELSE TRUNC(TO_CHAR(?, 'D')) END )  ");
		params.add(orderDate);
		params.add(orderDate);
		sql.append("     < (CASE WHEN TO_CHAR( a.start_date,'D') = '1' THEN 8 ELSE TRUNC(TO_CHAR( a.start_date,'D')) END ) ) ");
		sql.append("     THEN 1 ELSE 0 ");
		sql.append("   END) ), 4) + 1 ) = 3 ");
		sql.append(" AND week3         = 1) ");
		sql.append(" OR ( ( MOD( ( ( floor((TRUNC(?,'dd') - TRUNC(a.start_date,'dd')) / 7)) + ( ");
		params.add(orderDate);
		sql.append("   CASE WHEN (mod(TRUNC(?,'dd')- TRUNC(a.start_date,'dd'), 7) > 0) ");
		params.add(orderDate);
		sql.append("     AND ( (CASE WHEN TO_CHAR(?,'D') = '1' THEN 8 ELSE TRUNC(TO_CHAR(?, 'D')) END )  ");
		params.add(orderDate);
		params.add(orderDate);
		sql.append("     < (CASE WHEN TO_CHAR( a.start_date,'D') = '1' THEN 8 ELSE TRUNC(TO_CHAR( a.start_date,'D')) END ) ) ");
		sql.append("     THEN 1 ELSE 0 ");
		sql.append("   END) ), 4) + 1 ) = 4 ");
		sql.append(" AND week4         = 1) ) ");
		
		sql.append(" AND (CASE ");
		sql.append("   WHEN TO_CHAR(?, 'd') = 1 THEN a.SUNDAY ");
		params.add(orderDate);
		sql.append("   WHEN TO_CHAR(?, 'd') = 2 THEN a.MONDAY ");
		params.add(orderDate);
		sql.append("   WHEN TO_CHAR(?, 'd') = 3 THEN a.TUESDAY ");
		params.add(orderDate);
		sql.append("   WHEN TO_CHAR(?, 'd') = 4 THEN a.WEDNESDAY ");
		params.add(orderDate);
		sql.append("   WHEN TO_CHAR(?, 'd') = 5 THEN a.THURSDAY ");
		params.add(orderDate);
		sql.append("   WHEN TO_CHAR(?, 'd') = 6 THEN a.FRIDAY ");
		params.add(orderDate);
		sql.append("   WHEN TO_CHAR(?, 'd') = 7 THEN a.SATURDAY ");
		params.add(orderDate);
		sql.append("   END ) = 1 ");
		
		if (staffId != null && staffId > 0) {
			sql.append(" and s.staff_id = ? ");
			params.add(staffId);
		}
		return repo.getFirstBySQL(Staff.class, sql.toString(), params);
	}
	
	
	@Override
	public List<Staff> getListCashierByShop(KPaging<Staff> kPaging, String shopCode, ActiveType status, Boolean isOrderByCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from staff s join channel_type ct on s.staff_type_id = ct.channel_type_id where ct.type = ? and ct.object_type = ? and ct.status != ?");
		params.add(ChannelTypeType.STAFF.getValue());
		params.add(StaffObjectType.NVTT.getValue());
		params.add(ActiveType.DELETED.getValue());
		if (status != null) {
			sql.append(" and s.status = ?");
			params.add(status.getValue());
		} else {
			sql.append(" and s.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}

		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and exists (select 1 from shop sh where sh.shop_id = s.shop_id and sh.shop_code = ?)");
			params.add(shopCode.toUpperCase());
		}
		if (Boolean.TRUE.equals(isOrderByCode)) {
			sql.append(" order by s.staff_code");
		} else {
			sql.append(" order by s.staff_name");
		}
		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	@Override
	public Boolean checkIfStaffManageAnySaleMan(Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from staff s join channel_type c on s.staff_type_id = c.channel_type_id");
		sql.append("   where s.staff_owner_id = ? and c.type = ? and c.object_type = ?");
		params.add(staffId);
		params.add(ChannelTypeType.STAFF.getValue());
		params.add(StaffObjectType.NVBH.getValue());
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public void updateOwnerIdToNull(Long staffOwnerId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" update staff set staff_owner_id = null ");
		sql.append("        where staff_owner_id = ?");
		params.add(staffOwnerId);
		repo.executeSQLQuery(sql.toString(), params);
	}

	@Override
	public void updateSaleManagerToNull(Long staffOwnerId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" update staff set staff_owner_id = null ");
		sql.append("	where staff_id in (select s.staff_id from staff s join channel_type c on s.staff_type_id = c.channel_type_id");
		sql.append("        where s.staff_owner_id = ? and c.type = ? and c.object_type = ?)");
		params.add(staffOwnerId);
		params.add(ChannelTypeType.STAFF.getValue());
		params.add(StaffObjectType.NVBH.getValue());
		repo.executeSQLQuery(sql.toString(), params);
	}

	@Override
	public void updateSaleManager(Long staffOwnerId, List<Long> lstStaffId) throws DataAccessException {
		if (lstStaffId == null || lstStaffId.size() == 0)
			throw new IllegalArgumentException("lstStaffId");
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" update staff set staff_owner_id = ? ");
		params.add(staffOwnerId);
		sql.append("	where staff_id in (-1");
		for (Long staffId : lstStaffId) {
			sql.append(",?");
			params.add(staffId);
		}
		sql.append(")");
		repo.executeSQLQuery(sql.toString(), params);
	}

	@Override
	public List<Staff> getListStaffByOwner(String staffOwnerCode, String staffCode, String staffName, StaffObjectType staffObjectType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.* from staff s join staff os on (os.staff_id = s.staff_owner_id) join channel_type c on s.staff_type_id = c.channel_type_id");
		sql.append("   where 1 = 1");
		if (!StringUtility.isNullOrEmpty(staffOwnerCode)) {
			sql.append("   and upper(os.staff_code) = ?");
			params.add(staffOwnerCode.toUpperCase());

		}
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and upper(s.staff_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and upper(s.staff_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(staffName.toUpperCase()));
		}
		if (staffObjectType != null) {
			sql.append("   and c.type = ? and c.object_type = ?");
			params.add(ChannelTypeType.STAFF.getValue());
			params.add(staffObjectType.getValue());
		}
		sql.append(" order by s.staff_code");
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public Boolean checkStaffInShopOldFamily(String staffCode, String shopCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from staff s where s.staff_code = ?");
		sql.append(" 	and s.shop_id in (select shop_id from shop sh start with sh.shop_code = ?");
		sql.append("		connect by prior parent_shop_id = shop_id)");
		params.add(staffCode.toUpperCase());
		params.add(shopCode.toUpperCase());
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public String generateStaffCode() throws DataAccessException {
		String sql = "select staff_code_seq.nextval as count from dual";
		List<Object> params = new ArrayList<Object>();
		Integer c = repo.countBySQL(sql, params);
		String code = "000000000" + c.toString();
		code = code.substring(code.length() - 10);
		return code;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.StaffDAO#checkExitStaffManagerBy(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public Boolean checkExitStaffManagerBy(String shopCode, String superCode) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT COUNT(1) as count");
		sql.append(" FROM shop s,");
		sql.append(" staff st,");
		sql.append(" staff super");
		sql.append(" WHERE s.shop_code     = ?");
		params.add(shopCode);
		sql.append(" AND s.shop_id         = st.shop_id");
		sql.append(" AND s.status          = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND st.status         = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND st.staff_owner_id = super.staff_id");
		sql.append(" AND super.staff_code  = ?");
		params.add(superCode);
		sql.append(" AND super.status      = ?");
		params.add(ActiveType.RUNNING.getValue());
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public List<RptExImStockOfStaffDataVO> getRptExImStockOfStaff(Long shopId, List<Long> listStaffId, Date fromDate, Date toDate) throws DataAccessException {
		if (null == shopId) {
			return new ArrayList<RptExImStockOfStaffDataVO>();
		}
		if (null == listStaffId || listStaffId.size() <= 0) {
			return new ArrayList<RptExImStockOfStaffDataVO>();
		}
		if (fromDate != null && toDate != null && fromDate.after(toDate)) {
			return new ArrayList<RptExImStockOfStaffDataVO>();
		}

		StringBuilder findStaffId = new StringBuilder(" ( ");

		for (int i = 0; i < listStaffId.size(); i++) {
			if (i < listStaffId.size() - 1) {
				findStaffId.append("?, ");
			} else {
				findStaffId.append("?) ");
			}
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH temp ");
		sql.append("         AS (SELECT std.stock_trans_detail_id, std.product_id, std.price, std.quantity, s.staff_id ");
		sql.append("               FROM stock_trans st, stock_trans_detail std, staff s ");
		sql.append("              WHERE     st.stock_trans_id = std.stock_trans_id ");
		sql.append("                    AND st.from_owner_type = 1 ");
		sql.append("                    AND st.to_owner_type = 2 ");
		if (null != fromDate) {
			sql.append("                    AND st.STOCK_TRANS_DATE >= TRUNC (?) ");
			params.add(fromDate);
		}

		if (null != toDate) {
			sql.append("                    AND st.STOCK_TRANS_DATE < TRUNC (?) + 1 ");
			params.add(toDate);
		}

		sql.append("                    AND s.staff_id = st.to_owner_id ");
		sql.append("                    AND s.staff_id IN ");
		sql.append(findStaffId);
		params.addAll(listStaffId);

		sql.append("                    AND st.shop_id = ?) ");
		params.add(shopId);

		sql.append(" select st.staff_code as staffCode, st.staff_name as staffName, st.staff_id as staffId, ");
		sql.append("     pr.product_code as productCode, pr.product_name as productName,  ");
		sql.append("     nvl(data.price, 0) as price,  ");
		sql.append("     nvl(data.export_quantity, 0) as exportQuantity, nvl(data.export_quantity * data.price, 0) as exportAmount,   ");
		sql.append("     nvl(data.sale_quantity, 0) as sellQuantity, nvl(data.sale_quantity * data.price, 0) as sellAmount, ");
		sql.append("     nvl(data.sale_free_quantity, 0) as promoteQuantity, nvl(data.sale_free_quantity * data.price, 0) as promoteAmount, ");
		sql.append("     (NVL (data.export_quantity, 0) - NVL (data.sale_quantity, 0) - NVL (data.sale_free_quantity, 0)) AS stockQuantity, ");
		sql.append("     (NVL (data.export_quantity, 0) - NVL (data.sale_quantity, 0) - NVL (data.sale_free_quantity, 0)) * NVL (data.price, 0) AS stockAmount ");
		sql.append(" from( ");
		sql.append("     select nvl(stock.staff_id, nvl(sale.staff_id, sale_free.staff_id)) as staff_id,  ");
		sql.append("         nvl(stock.product_id, nvl(sale.product_id, sale_free.product_id)) as product_id, ");
		sql.append("         stock.price,  ");
		sql.append("         stock.stock_quantity as export_quantity,   ");
		sql.append("         sale.sale_quantity as sale_quantity,  ");
		sql.append("         sale_free.sale_free_quantity as sale_free_quantity ");
		sql.append("     from ( ");
		sql.append("         SELECT item.product_id, item.price, sum(temp.quantity) as stock_quantity, temp.staff_id ");
		sql.append("         from(select * ");
		sql.append("             FROM temp ");
		sql.append("             WHERE stock_trans_detail_id IN ( ");
		sql.append("                 SELECT MAX (stock_trans_detail_id) ");
		sql.append("                 FROM temp ");
		sql.append("                 GROUP BY product_id)) item ");
		sql.append("         join temp ");
		sql.append("         on item.product_id = temp.product_id ");
		sql.append("         group by item.product_id, item.price, temp.staff_id) stock ");
		sql.append("     left join( ");
		sql.append("         select sod.product_id, sum(sod.quantity) as sale_quantity, s.staff_id ");
		sql.append("         from sale_order so, sale_order_detail sod, staff s ");
		sql.append("         where so.sale_order_id = sod.sale_order_id ");
		sql.append("             and so.order_type = 'SO' ");
		sql.append("             and so.type = 1 ");
		if (null != fromDate) {
			sql.append("             and so.order_date >= trunc(?) ");
			params.add(fromDate);
		}

		if (null != toDate) {
			sql.append("             and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}

		sql.append("             and sod.is_free_item = 0 ");
		sql.append("             and s.staff_id in ");
		sql.append(findStaffId);
		params.addAll(listStaffId);

		sql.append("             and s.staff_id = so.staff_id ");
		sql.append("             and so.shop_id = ? ");
		params.add(shopId);

		sql.append("         group by sod.product_id, s.staff_id) sale ");
		sql.append("     on stock.product_id = sale.product_id and stock.staff_id = sale.staff_id ");
		sql.append("     left join( ");
		sql.append("         select sod.product_id, sum(sod.quantity) as sale_free_quantity, s.staff_id ");
		sql.append("         from sale_order so, sale_order_detail sod, staff s ");
		sql.append("         where so.sale_order_id = sod.sale_order_id ");
		sql.append("             and so.order_type = 'SO' ");
		sql.append("             and so.type = 1 ");
		if (null != fromDate) {
			sql.append("             and so.order_date >= trunc(?) ");
			params.add(fromDate);
		}
		if (null != toDate) {
			sql.append("             and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append("             and sod.is_free_item = 1 ");
		sql.append("             and so.staff_id = s.staff_id ");
		sql.append("             and s.staff_id in ");
		sql.append(findStaffId);
		params.addAll(listStaffId);

		sql.append("             and so.shop_id = ? ");
		params.add(shopId);

		sql.append("         group by sod.product_id, s.staff_id) sale_free ");
		sql.append("     on stock.product_id = sale_free.product_id and stock.staff_id = sale_free.staff_id ");
		sql.append(" ) data ");
		sql.append(" join staff st on st.staff_id = data.staff_id ");
		sql.append(" join product pr on pr.product_id = data.product_id ");
		sql.append(" order by staffCode, productCode ");

		final String[] fieldNames = new String[] { "staffCode", "staffName", "staffId", "productCode", "productName", "price", "exportQuantity", "exportAmount", "sellQuantity", "sellAmount", "promoteQuantity", "promoteAmount", "stockQuantity",
				"stockAmount" };

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL };

		// System.out.print(TestUtils.addParamToQuery( sql.toString(), params.toArray()));

		return repo.getListByQueryAndScalar(RptExImStockOfStaffDataVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public Boolean checkStaffPhoneExists(String phoneNum) throws DataAccessException {
		String sql = "select count(1) as count from staff where upper(mobilephone) = upper(?) and status != -1";
		List<Object> params = new ArrayList<Object>();
		params.add(phoneNum);
		int c = repo.countBySQL(sql, params);
		return c > 0 ? true : false;
	}

	@Override
	public List<RptCustomerNotVisitOfSalesVO> getListRptKHKGTTT_NVBH(List<Long> listShopId, List<Long> listNVGSId, List<Long> listNVBHId, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder findShopId = new StringBuilder(" ( ");
		StringBuilder findStaffId = new StringBuilder(" ( ");
		StringBuilder findOwnerStaffId = new StringBuilder(" ( ");

		if (null == listShopId || listShopId.size() <= 0) {
			throw new IllegalArgumentException(ExceptionCode.LIST_SHOP_ID_IS_NULL);
		}
		if (null == fromDate || null == toDate) {
			throw new IllegalArgumentException(ExceptionCode.DATE_TIME_IS_NULL);
		}
		if (fromDate.after(toDate)) {
			throw new IllegalArgumentException(ExceptionCode.BEGIN_TIME_AFTER_END_TIME);
		}

		for (int i = 0; i < listShopId.size(); i++) {
			if (i < listShopId.size() - 1) {
				findShopId.append("?, ");
			} else {
				findShopId.append("?) ");
			}
		}
		if (null != listNVBHId && listNVBHId.size() > 0) {
			for (int i = 0; i < listNVBHId.size(); i++) {
				if (i < listNVBHId.size() - 1) {
					findStaffId.append("?, ");
				} else {
					findStaffId.append("?) ");
				}
			}
		}

		if (null != listNVGSId && listNVGSId.size() > 0) {
			for (int i = 0; i < listNVGSId.size(); i++) {
				if (i < listNVGSId.size() - 1) {
					findOwnerStaffId.append("?, ");
				} else {
					findOwnerStaffId.append("?) ");
				}
			}
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with temp as( ");
		sql.append("     select  ");
		sql.append("         (select shop_code from shop sh, channel_type ct where sh.shop_type_id = ct.channel_type_id  and ct.object_type = 1 and rownum = 1 start with sh.shop_id = shop_temp.shop_id connect by prior parent_shop_id = shop_id) as area, ");
		sql.append("         (select shop_code from shop sh, channel_type ct where sh.shop_type_id = ct.channel_type_id  and ct.object_type = 2 and rownum = 1 start with sh.shop_id = shop_temp.shop_id connect by prior parent_shop_id = shop_id) as location, ");
		sql.append("         shop_temp.shop_id, ");
		sql.append("         shop_temp.shop_code, ");
		sql.append("         shop_temp.shop_name, ");
		sql.append("         nvgs.staff_id as ownerid, ");
		sql.append("         nvgs.staff_code as ownercode, ");
		sql.append("         nvgs.staff_name as ownerName, ");
		sql.append("         nvbh.staff_id as staffid, ");
		sql.append("         nvbh.staff_code as staffcode, ");
		sql.append("         nvbh.staff_name as staffName ");
		sql.append("     from ( ");
		sql.append("         select sh.shop_id, sh.parent_shop_id, sh.shop_code, sh.shop_name, ct.object_type ");
		sql.append("         from shop sh, channel_type ct ");
		sql.append("         where sh.shop_type_id = ct.channel_type_id ");
		sql.append("             and ct.object_type = 3 ");
		sql.append("         start with sh.shop_id in ");
		sql.append(findShopId);
		params.addAll(listShopId);

		sql.append("         connect by prior shop_id = parent_shop_id) shop_temp ");
		sql.append("     join ( ");
		sql.append("         select st.* ");
		sql.append("         from staff st, channel_type ct ");
		sql.append("         where st.staff_type_id = ct.channel_type_id ");
		sql.append("             and ct.object_type = 1 ");
		if (null != listNVBHId && listNVBHId.size() > 0) {
			sql.append("             and st.staff_id in ");
			sql.append(findStaffId);

			params.addAll(listNVBHId);
		}

		sql.append("     ) nvbh on nvbh.shop_id = shop_temp.shop_id ");
		sql.append("     join ( ");
		sql.append("         select st.* ");
		sql.append("         from staff st, channel_type ct ");
		sql.append("         where st.staff_type_id = ct.channel_type_id ");
		sql.append("             and ct.object_type = 5 ");
		if (null != listNVGSId && listNVGSId.size() > 0) {
			sql.append("             and st.staff_id in ");
			sql.append(findOwnerStaffId);

			params.addAll(listNVGSId);
		}

		sql.append("     ) nvgs on nvbh.staff_owner_id = nvgs.staff_id), ");
		sql.append(" temp2 as( ");
		sql.append("     select vp.staff_id, rc.customer_id, dates.date_name ");
		sql.append("     from visit_plan vp, routing r, routing_customer rc, ( ");
		sql.append("         select (trunc(?) + level - 1) as date_name ");
		params.add(fromDate);

		sql.append("         from dual ");
		sql.append("         connect by level <= trunc(?) - trunc(?) + 1 ");
		params.add(toDate);
		params.add(fromDate);

		sql.append("         order by date_name) dates ");
		sql.append("     where vp.routing_id = r.routing_id ");
		sql.append("         and r.routing_id = rc.routing_id ");
		sql.append("         and vp.status = 1 and r.status = 1 and rc.status = 1 ");
		sql.append("         and ((to_char (dates.date_name, 'D') = 2 and rc.monday = 1) ");
		sql.append("            or (to_char (dates.date_name, 'D') = 3 and rc.tuesday = 1) ");
		sql.append("            or (to_char (dates.date_name, 'D') = 4 and rc.wednesday = 1) ");
		sql.append("            or (to_char (dates.date_name, 'D') = 5 and rc.thursday = 1) ");
		sql.append("            or (to_char (dates.date_name, 'D') = 6 and rc.friday = 1) ");
		sql.append("            or (to_char (dates.date_name, 'D') = 7 and rc.saturday = 1)) ");

		sql.append("         AND (TO_CHAR (trunc(?), 'iw') - rc.START_WEEK) >= 0 ");
		params.add(fromDate);

		sql.append("         AND MOD ((TO_CHAR (trunc(?), 'iw') - rc.START_WEEK), rc.WEEK_INTERVAL) = 0 ");
		params.add(fromDate);

		sql.append("         AND vp.from_date <= trunc(?) + 1 ");
		params.add(fromDate);

		sql.append("         AND (vp.TO_DATE >= trunc(?) OR vp.TO_DATE IS NULL) ");
		params.add(toDate);

		sql.append("         and vp.staff_id in (select staffid from temp) ");
		sql.append("     minus ");
		sql.append("     select staff_id, customer_id, trunc(start_time) date_name ");
		sql.append("     from action_log ");
		sql.append("     where 1 = 1 ");
		sql.append("         and start_time >= trunc(?) ");
		params.add(fromDate);

		sql.append("         and start_time < trunc(?) + 1 ");
		params.add(toDate);

		sql.append("         and object_type = 0 ");
		sql.append("         and is_or in (0, 1) ");
		sql.append("         and staff_id in (select staffid from temp) ");
		sql.append(" ) ");
		sql.append(" select  nv.area as areaCode, ");
		sql.append("     nv.location as locationCode, ");
		sql.append("     nv.shop_code as shopCode, ");
		sql.append("     nv.shop_name as shopName, ");
		sql.append("     nv.ownerName as staffOwnerName, ");
		sql.append("     nv.staffCode as staffCode, ");
		sql.append("     nv.staffName as staffName, ");
		sql.append("     data.date_name as visitDay, ");
		sql.append("     TO_CHAR (data.date_name, 'D') as visitDate, ");
		sql.append("     c.short_code as customerCode,  ");
		sql.append("     c.customer_name as customerName, ");
		sql.append("     c.housenumber || ' ' || c.address as customerAddress, ");
		sql.append("     1 orderNumber ");
		sql.append(" from temp nv, temp2 data, customer c  ");
		sql.append(" where nv.staffId = data.staff_id ");
		sql.append("     and data.customer_id = c.customer_id ");
		sql.append("     and c.status = 1 ");
		sql.append(" union ");
		sql.append(" select  nv.area as areaCode, ");
		sql.append("     nv.location as locationCode, ");
		sql.append("     nv.shop_code as shopCode, ");
		sql.append("     nv.shop_name as shopName, ");
		sql.append("     nv.ownerName as staffOwnerName, ");
		sql.append("     nv.staffCode as staffCode, ");
		sql.append("     nv.staffName as staffName, ");
		sql.append("     null as visitDay, ");
		sql.append("     null as visitDate, ");
		sql.append("     count(1) || '' as customerCode,  ");
		sql.append("     null as customerName, ");
		sql.append("     null as customerAddress, ");
		sql.append("     2 orderNumber ");
		sql.append(" from temp nv, temp2 data, customer c  ");
		sql.append(" where nv.staffId = data.staff_id ");
		sql.append("     and data.customer_id = c.customer_id ");
		sql.append("     and c.status = 1 ");
		sql.append(" GROUP BY ROLLUP (nv.area, nv.location, nv.shop_code, nv.shop_name, nv.ownerName, nv.staffCode, nv.staffName) ");
		sql.append(" having grouping_id(nv.area, nv.location, nv.shop_code, nv.shop_name, nv.ownerName, nv.staffCode, nv.staffName) not in(1, 15) ");
		sql.append(" order by areaCode, locationCode, shopCode, shopName, staffCode, visitDay, orderNumber ");

		final String[] fieldNames = new String[] { "areaCode", "locationCode", "shopCode", "shopName", "staffOwnerName", "staffCode", "staffName", "visitDate", "visitDay", "customerCode", "customerName", "customerAddress" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.DATE, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };

		System.out.println(sql.toString());
		return repo.getListByQueryAndScalar(RptCustomerNotVisitOfSalesVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.StaffDAO#getListStaffForSuperVisorWithCondition(
	 * ths.dms.core.entities.enumtype.KPaging, java.lang.String,
	 * java.lang.String, java.lang.String,
	 * ths.dms.core.entities.enumtype.ChannelTypeType, java.util.List,
	 * java.lang.Boolean)
	 */
	@Override
	public List<StaffVO> getListStaffForSuperVisorWithCondition(KPaging<StaffVO> kPaging, String staffCode, String staffName, String shopCode, ActiveType status, ChannelTypeType type, List<StaffObjectType> listObjectType, Boolean isHasChild)
			throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select st.staff_id as staffId, st.staff_code as staffCode, st.staff_name as staffName, s.shop_code as shopCode, s.shop_id as shopId,");
		sql.append(" (select channel_type_code from channel_type where channel_type_id = st.staff_type_id) as channelTypeCode,");
		sql.append(" (select channel_type_name from channel_type where channel_type_id = st.staff_type_id) as channelTypeName,");
		sql.append(" (select object_type from channel_type where channel_type_id = st.staff_type_id) as objectType,");
		sql.append(" (select count(1) as count from staff_position_log where staff_id = st.staff_id and create_date >= trunc(sysdate)) as hasPosition");
		sql.append("  from STAFF st join shop s on st.shop_id = s.shop_id ");
		sql.append(" where 1 = 1");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and st.staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			String temp = StringUtility.toOracleSearchLike(staffName.toLowerCase());
			sql.append(" and lower(st.staff_name) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (status != null) {
			sql.append(" and st.status=?");
			params.add(status.getValue());
		}
		if (type != null) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status = ? and st.STAFF_TYPE_ID=ct.channel_type_id and ct.type=?)");
			params.add(ActiveType.RUNNING.getValue());
			params.add(type.getValue());
		}
		if (listObjectType != null && listObjectType.size() > 0) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status = ? and st.STAFF_TYPE_ID = ct.channel_type_id and (");
			params.add(ActiveType.RUNNING.getValue());
			for (StaffObjectType channelObjectType : listObjectType) {
				if (channelObjectType != null) {
					sql.append(" ct.object_type=? or ");
					params.add(channelObjectType.getValue());
				}
			}
			sql.append("1=0))");
		}
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			if (Boolean.TRUE.equals(isHasChild)) {
				sql.append(" and st.shop_id in (select shop_id from shop start with shop_code=? connect by prior shop_id = parent_shop_id and status=?)");
				params.add(shopCode.toUpperCase());
				params.add(ActiveType.RUNNING.getValue());
			} else {
				sql.append(" and s.shop_code = ? and s.status = ?");
				params.add(shopCode.toUpperCase());
				params.add(ActiveType.RUNNING.getValue());
			}
		}

		sql.append(" order by shop_code, staff_code asc");

		String[] fieldNames = new String[] {//
		"shopCode",// 1
				"shopId",// 1
				"channelTypeCode",// 2
				"channelTypeName",// 3
				"staffId",// 4
				"staffCode",// 5
				"staffName",// 6
				"objectType",// 7
				"hasPosition",// 8
		};
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.STRING,// 1
				StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.LONG,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.INTEGER,// 8
		};
		if (kPaging != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.StaffDAO#getListSupervisorAllowShop(ths.dms.
	 * core.entities.enumtype.KPaging, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.Boolean)
	 */
	@Override
	public List<Staff> getListSupervisorAllowShop(KPaging<Staff> kPaging, String staffCode, String staffName, String shopCode, Boolean isHasChild) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.*  ");
		sql.append(" from staff s join channel_type c on s.staff_type_id=c.channel_type_id  ");
		sql.append(" where c.object_type=? and c.status = ? and c.type = ?");
		params.add(StaffObjectType.NVGS.getValue());
		params.add(ActiveType.RUNNING.getValue());
		params.add(ChannelTypeType.STAFF.getValue());
		sql.append(" and s.staff_id in (select distinct(staff_owner_id) from staff where 1 = 1");
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (Boolean.TRUE.equals(isHasChild)) {
			sql.append("   and shop_id in (select shop_id from shop where status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("                   start with shop_code=? ");
			params.add(shopCode);
			sql.append("                   connect by prior shop_id = parent_shop_id)) ");
		} else {
			sql.append(" and shop_id  = (select shop_id from shop where shop_code = ?))");
			params.add(shopCode);
		}

		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			String temp = StringUtility.toOracleSearchLike(staffName.toLowerCase());
			sql.append(" and lower(staff_name) like ? ESCAPE '/'");
			params.add(temp);
		}

		sql.append(" and s.status=?");
		params.add(ActiveType.RUNNING.getValue());

		sql.append(" order by staff_code asc");
		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.StaffDAO#getListStaffAllowOwnerAndShop(com.viettel
	 * .core.entities.enumtype.KPaging, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.Boolean)
	 */
	@Override
	public List<Staff> getListStaffAllowOwnerAndShop(KPaging<Staff> kPaging, String staffOwnerCode, String staffCode, String staffName, String shopCode, Boolean isHasChild) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.*  ");
		sql.append(" from staff s join channel_type c on s.staff_type_id=c.channel_type_id  ");
		sql.append(" where object_type=? and c.status = ? ");
		params.add(StaffObjectType.NVBH.getValue());
		params.add(ActiveType.RUNNING.getValue());
		if (Boolean.TRUE.equals(isHasChild)) {
			sql.append("   and s.shop_id in (select shop_id from shop where status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("                   start with shop_code=? ");
			params.add(shopCode);
			sql.append("                   connect by prior shop_id = parent_shop_id)");
		} else {
			sql.append(" and s.shop_id = (select shop_id from shop where shop_code = ?)");
			params.add(shopCode);
		}
		if (!StringUtility.isNullOrEmpty(staffOwnerCode)) {
			sql.append(" and s.staff_owner_id = (select staff_id from staff where staff_code = ?)");
			params.add(staffOwnerCode);
		}
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			String temp = StringUtility.toOracleSearchLike(staffName.toLowerCase());
			sql.append(" and lower(staff_name) like ? ESCAPE '/'");
			params.add(temp);
		}

		sql.append(" and s.status=?");
		params.add(ActiveType.RUNNING.getValue());

		sql.append(" order by staff_code asc");
		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<Staff> getListNVGSNew(KPaging<Staff> kPaging, String staffCode, String staffName, Long vungId, Long shopId, List<Long> lstExceptId, SortedMap<Long, List<Long>> deletedStaffByVung) throws DataAccessException {
		List<Long> mandatoryId = new ArrayList<Long>();
		if (vungId == null) {
			throw new IllegalArgumentException("vungId is null");
		}
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (lstExceptId == null) {
			lstExceptId = new ArrayList<Long>();
		}
		if (deletedStaffByVung != null) {
			for (Long vId : deletedStaffByVung.keySet()) {
				Set<Long> setStaffId = new HashSet<Long>(deletedStaffByVung.get(vId));
				if (vungId != vId) {
					//kiem tra xem nhan vien do da duoc xoa het trong vung hay chua, neu chua thi dua vao lstExceptId
					for (Long staffId : setStaffId) {
						Integer count = this.countNumNVGSInVung(staffId);
						if (count > Collections.frequency(deletedStaffByVung.get(vId), staffId)) {
							lstExceptId.add(staffId);
						} else
							mandatoryId.add(staffId);
					}
				} else {
					for (Long staffId : setStaffId) {
						StaffGroupDetail sgd = this.getSGDByStaff(staffId);
						if (sgd.getStaffGroup().getShop().getId().equals(shopId)) {
							mandatoryId.add(staffId);
						}
					}
				}
			}
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from staff st");
		sql.append(" where ((status = 1 and st.shop_id = ?");
		params.add(vungId);
		sql.append(" and staff_type_id in (select channel_type_id from channel_type where status = 1 and type = 2 and object_type = 5)");

		sql.append(" and not exists (select 1 from staff_group_detail sgd , staff_group sg, shop s ");
		sql.append(" where s.shop_id = sg.shop_id ");
		sql.append(" and sgd.staff_group_id = sg.staff_group_id ");
		sql.append(" and sgd.staff_id = st.staff_id ");
		sql.append(" and sg.status = 1 ");
		sql.append(" and sgd.status = 1 ");
		sql.append(" and sg.shop_id = ? ");
		sql.append(" and s.parent_shop_id = ?) ");
		params.add(shopId);
		params.add(vungId);

		sql.append(" and ( ");
		sql.append(" exists (select 1 from staff_group_detail sgd , staff_group sg, shop s ");
		sql.append(" where s.shop_id = sg.shop_id ");
		sql.append(" and sgd.staff_group_id = sg.staff_group_id ");
		sql.append(" and sgd.staff_id = st.staff_id ");
		sql.append(" and sg.status = 1 ");
		sql.append(" and sgd.status = 1 ");
		sql.append(" and sg.shop_id !=? ");
		params.add(shopId);

		sql.append(" and s.parent_shop_id = ?) ");
		params.add(vungId);
		sql.append(" or not exists (select 1 from staff_group_detail sgd  join staff_group sg ON sgd.staff_group_id = sg.staff_group_id where sg.status =1 and sgd.status =1 and sgd.staff_id = st.staff_id) ");
		//sql.append(" not exists (select 1 from staff_group_detail sgd  join staff_group sg ON sgd.staff_group_id = sg.staff_group_id where sg.status =1 and sgd.status =1 and sgd.staff_id = st.staff_id) ");
		sql.append(" )");
		if (lstExceptId != null && lstExceptId.size() > 0) {
			sql.append(" and st.staff_id not in (-1");
			for (Long stId : lstExceptId) {
				sql.append(",?");
				params.add(stId);
			}
			sql.append(")");
		}
		sql.append(")");

		if (mandatoryId != null && mandatoryId.size() > 0) {
			sql.append(" or st.staff_id in (-1");
			for (Long stId : mandatoryId) {
				sql.append(",?");
				params.add(stId);
			}
			sql.append(")");
		}

		sql.append(")");

		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and lower(staff_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffName.toLowerCase()));
		}

		sql.append(" order by staff_code");

		if (kPaging == null) {
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<Staff> getListNVGS(KPaging<Staff> kPaging, String staffCode, String staffName, Long shopId, List<String> listTBHVCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from staff");
		sql.append(" where 1=1");
		sql.append(" and status = 1");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and staff_name like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffName.toUpperCase()));
		}
		sql.append(" and staff_type_id in (select channel_type_id from channel_type where status = 1 and type = 2 and object_type = 5)");
		sql.append(" and shop_id in(select shop_id from shop where status = 1");
		if (listTBHVCode == null || listTBHVCode.size() == 0) {
			sql.append(" start with shop_id = ?");
			params.add(shopId);
			sql.append(" connect by prior shop_id = parent_shop_id)");
			sql.append(" order by staff_code, staff_name");
		} else {
			sql.append(" start with shop_id in (");
			for (int i = 0; i < listTBHVCode.size(); i++) {
				sql.append("(select shop_id from staff where upper(staff_code) = upper(");
				sql.append("'");
				sql.append(listTBHVCode.get(i));
				sql.append("'))");
				if (i != listTBHVCode.size() - 1)
					sql.append(",");
			}
			sql.append(")");
			//sql.append(" connect by prior shop_id = parent_shop_id)");
			sql.append(" connect by prior parent_shop_id = shop_id)");
			sql.append(" order by staff_code, staff_name");
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");

		// System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));

		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	//SangTN
	@Override
	public List<Staff> getListNVGS2(KPaging<Staff> kPaging, Long shopId, String lstStaffCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("   SELECT DISTINCT st_owner.*   ");
		sql.append("   FROM staff st_owner   ");
		sql.append("   JOIN staff st   ");
		sql.append("   ON (st_owner.staff_id       = st.staff_owner_id   ");
		if (shopId != null) {
			sql.append("   AND st.shop_id              = ?   ");
			params.add(shopId);
		}

		if (!StringUtility.isNullOrEmpty(lstStaffCode)) {
			String[] temp = lstStaffCode.split(",");
			sql.append(" 	and ( 	");
			for (int i = 0; i < temp.length; i++) {
				String staffCode = temp[i].trim();
				if (null != staffCode) {
					sql.append(" st.staff_code            =?  or   ");
					params.add(staffCode);
				}
			}
			sql.append(" 1=0 )");
		}
		sql.append(")");

		sql.append("   WHERE 1                     = 1   ");
		sql.append("   AND st_owner.staff_type_id IN   ");
		sql.append("     (SELECT channel_type_id   ");
		sql.append("     FROM channel_type   ");
		sql.append("     WHERE status    = 1   ");
		sql.append("     AND type        = 2   ");
		sql.append("     AND object_type = 5   ");
		sql.append("     )   ");

		sql.append(" order by st_owner.staff_code, st_owner.staff_name ");

		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");

		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	@Override
	public List<Staff> getListNVGS3(KPaging<Staff> kPaging, String strListShopId, String staffCode, String staffName, String lstStaffCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(strListShopId)) {
			sql.append("  with shopTemp as ( SELECT regexp_substr(?,'[^,]+', 1, level) as shopId ");
			sql.append("   FROM dual ");
			sql.append("   CONNECT BY regexp_substr(?, '[^,]+', 1, level) IS NOT NULL  ");
			sql.append("  ) ");
			params.add(strListShopId);
			params.add(strListShopId);
		}
		sql.append("   SELECT DISTINCT st_owner.*   ");
		sql.append("   FROM staff st_owner   ");
		sql.append("   JOIN staff st   ");
		sql.append("   ON (st_owner.staff_id       = st.staff_owner_id   ");
		sql.append("   AND st.shop_id              in (   ");
		sql.append(" (SELECT shop_id FROM shop START WITH shop_id IN ");
		sql.append(" ( SELECT shopId FROM shopTemp ) ");
		sql.append(" CONNECT BY SHOP_ID = PARENT_SHOP_ID ");
		sql.append(" ) ) ");
		if (!StringUtility.isNullOrEmpty(lstStaffCode)) {
			String[] temp = lstStaffCode.split(",");
			sql.append(" 	and ( 	");
			for (int i = 0; i < temp.length; i++) {
				String stCode = temp[i].trim();
				//if(null != staffCode){
				if (null != stCode) {
					sql.append(" st_owner.staff_code            =?  or   ");
					params.add(stCode);
				}
			}
			sql.append(" 1=0 )");
		}
		sql.append(")");

		sql.append("   WHERE 1                     = 1   ");
		sql.append("   AND st_owner.status         = 1   ");
		sql.append("   AND st_owner.staff_type_id IN   ");
		sql.append("     (SELECT channel_type_id   ");
		sql.append("     FROM channel_type   ");
		sql.append("     WHERE status    = 1   ");
		sql.append("     AND type        = 2   ");
		sql.append("     AND object_type = 5   ");
		sql.append("     )   ");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and st_owner.staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and upper(st_owner.staff_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffName.toUpperCase()));
		}

		sql.append(" order by st_owner.staff_code, st_owner.staff_name ");

		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");
		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	@Override
	public List<Staff> getListNVGS4(KPaging<Staff> kPaging, String strListShopId, String staffCode, String staffName) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(strListShopId)) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH shopTemp AS ");
		sql.append("   (  ");
		sql.append("     SELECT shop_id  ");
		sql.append("     FROM shop ");
		sql.append("       START WITH shop_id in  ");
		sql.append("       ( ");
		sql.append("         SELECT regexp_substr(?,'[^,]+', 1, level) AS shopId ");
		sql.append("         FROM dual ");
		sql.append("           CONNECT BY regexp_substr(?, '[^,]+', 1, level) IS NOT NULL ");
		sql.append("       ) ");
		sql.append("       CONNECT BY PRIOR shop_id = parent_shop_id ");
		sql.append("   )  ");
		params.add(strListShopId);
		params.add(strListShopId);
		sql.append("   SELECT DISTINCT st_owner.*   ");
		sql.append("   FROM staff st_owner   ");
		sql.append("   JOIN staff st   ");
		sql.append("   ON (st_owner.staff_id       = st.staff_owner_id   ");
		sql.append("   AND st.shop_id       IN ( select shop_id from shopTemp) )   ");
		sql.append("   WHERE 1                     = 1   ");
		sql.append("   AND st_owner.status         = 1   ");
		sql.append("   AND st_owner.staff_type_id IN   ");
		sql.append("     (SELECT channel_type_id   ");
		sql.append("     FROM channel_type   ");
		sql.append("     WHERE status    = 1   ");
		sql.append("     AND type        = 2   ");
		sql.append("     AND object_type = 5   ");
		sql.append("     )   ");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and st_owner.staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and upper(st_owner.staff_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffName.toUpperCase()));
		}

		sql.append(" order by st_owner.staff_code, st_owner.staff_name ");

		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");
		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	@Override
	public List<Staff> getListNVGSByTBHVId(KPaging<Staff> kPaging, Long tbhvId) throws DataAccessException {
		if (tbhvId == null) {
			throw new IllegalArgumentException("tbhvId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from staff st ");
		sql.append(" where 1=1 ");
		sql.append(" and status = 1 ");
		sql.append(" and staff_type_id in (select channel_type_id from channel_type where status = 1 and type = 2 and object_type = 5) ");
		sql.append(" and staff_owner_id = ? ");
		params.add(tbhvId);
		sql.append(" and exists (select 1 from staff_group_detail where status = 1 and staff_id = st.staff_id) ");
		sql.append(" order by staff_code");

		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<Staff> getListNVGH(KPaging<Staff> kPaging, Long shopId, String customerShortCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select distinct s.* ");
		sql.append(" from customer c join group_transfer gt on c.group_transfer_id = gt.group_transfer_id and c.shop_id = gt.shop_id ");
		sql.append(" join staff s on s.staff_id = gt.staff_id and s.shop_id = gt.shop_id ");
		sql.append(" where c.status = 1 and gt.status = 1 and s.status = 1  ");
		if (!StringUtility.isNullOrEmpty(customerShortCode)) {
			sql.append(" and c.short_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(customerShortCode.toUpperCase()));
		}
		if (shopId != null) {
			sql.append(" AND c.SHOP_ID=? ");
			params.add(shopId);
		}

		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");

		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	@Override
	public List<Staff> getListNVBH(KPaging<Staff> kPaging, String staffCode, String staffName, List<String> lstStaffOwnerCode, Long shopId, Boolean isGetChildShop, List<String> lstTBHVCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		boolean check = false;
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.* from staff s");
		sql.append(" where status = 1 and staff_type_id in");
		sql.append(" (select channel_type_id from channel_type where status = 1 and type = 2 and object_type in(1,2))");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and upper(staff_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffName.toUpperCase()));
		}
		if (lstStaffOwnerCode != null && lstStaffOwnerCode.size() != 0 && lstStaffOwnerCode.get(0) != null) {
			sql.append(" and (");
			for (String staffOwnerCode : lstStaffOwnerCode) {
				if (!StringUtility.isNullOrEmpty(staffOwnerCode)) {
					sql.append(" exists (select 1 from staff where staff_id = s.staff_owner_id and upper(staff_code) like ? ESCAPE '/') or ");
					params.add(StringUtility.toOracleSearchLikeSuffix(staffOwnerCode.toUpperCase()));
				}
			}
			sql.append(" 0 = 1)");
		} else { // neu' co' truyen vao lstTBHV se~ lay' tat ca cac con
			if (lstTBHVCode != null && lstTBHVCode.size() != 0) {
				sql.append(" and shop_id in ");
				sql.append(" (select shop_id from shop where status = 1 ");
				sql.append(" start with shop_id in (");
				for (int i = 0; i < lstTBHVCode.size(); i++) {
					String staffTBHVCode = lstTBHVCode.get(i);
					if (!StringUtility.isNullOrEmpty(staffTBHVCode)) {
						sql.append("(select shop_id from staff where upper(staff_code) = upper(? ");
						params.add(staffTBHVCode.toUpperCase().trim());
						sql.append("))");
						if (i != lstTBHVCode.size() - 1) {
							sql.append(",");
						}
					}
				}
				sql.append(")");
				sql.append(" connect by prior shop_id = parent_shop_id)");
				check = true;
			}
		}
		if (!check) {
			if (isGetChildShop != null && Boolean.TRUE.equals(isGetChildShop)) {
				sql.append(" and shop_id in ");
				sql.append(" (select shop_id from shop where status = 1 ");
				sql.append(" start with shop_id = ?");
				params.add(shopId);
				sql.append(" connect by prior shop_id = parent_shop_id)");
			} else {
				sql.append(" and shop_id = ?");
				params.add(shopId);
			}
		}
		sql.append(" order by staff_code");

		// System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));

		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<StaffComboxVO> getListNVBHisShop(KPaging<StaffComboxVO> kPaging, String staffCode, String staffName, List<String> lstStaffOwnerCode, Long shopId, Boolean isGetChildShop, List<String> lstTBHVCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		boolean check = false;
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.staff_id as id, s.staff_code as staffCode, s.staff_name as staffName from staff s");
		sql.append(" where status = 1 and staff_type_id in");
		sql.append(" (select channel_type_id from channel_type where status = 1 and type = 2 and object_type in(1,2))");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and upper(staff_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffName.toUpperCase()));
		}
		if (lstStaffOwnerCode != null && lstStaffOwnerCode.size() != 0 && lstStaffOwnerCode.get(0) != null) {
			sql.append(" and (");
			for (String staffOwnerCode : lstStaffOwnerCode) {
				if (!StringUtility.isNullOrEmpty(staffOwnerCode)) {
					sql.append(" exists (select 1 from staff where staff_id = s.staff_owner_id and upper(staff_code) like ? ESCAPE '/') and shop_id = ? or ");
					params.add(StringUtility.toOracleSearchLikeSuffix(staffOwnerCode.toUpperCase()));
					params.add(shopId);
				}
			}
			sql.append(" 0 = 1)");
		} else { // neu' co' truyen vao lstTBHV se~ lay' tat ca cac con
			if (lstTBHVCode != null && lstTBHVCode.size() != 0) {
				sql.append(" and shop_id in ");
				sql.append(" (select shop_id from shop where status = 1 ");
				sql.append(" start with shop_id in (");
				for (int i = 0; i < lstTBHVCode.size(); i++) {
					String staffTBHVCode = lstTBHVCode.get(i);
					if (!StringUtility.isNullOrEmpty(staffTBHVCode)) {
						sql.append("(select shop_id from staff where upper(staff_code) = upper(? ");
						params.add(staffTBHVCode.toUpperCase().trim());
						sql.append("))");
						if (i != lstTBHVCode.size() - 1) {
							sql.append(",");
						}
					}
				}
				sql.append(")");
				sql.append(" connect by prior shop_id = parent_shop_id)");
				check = true;
			}
		}
		if (!check) {
			if (isGetChildShop != null && Boolean.TRUE.equals(isGetChildShop)) {
				sql.append(" and shop_id in ");
				sql.append(" (select shop_id from shop where status = 1 ");
				sql.append(" start with shop_id = ?");
				params.add(shopId);
				sql.append(" connect by prior shop_id = parent_shop_id)");
			} else {
				sql.append(" and shop_id = ?");
				params.add(shopId);
			}
		}
		sql.append(" order by staff_code");

		String[] fieldNames = new String[] { "id", //1
				"staffCode", //2
				"staffName", //3 
		};
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
		};
		// System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));

		if (kPaging == null)
			//return repo.getListBySQL(Staff.class, sql.toString(), params);
			return repo.getListByQueryAndScalar(StaffComboxVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else {
			//return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(StaffComboxVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}

	/*
	 * @Override public List<Staff> getListNVBHisShop(KPaging<Staff> kPaging,
	 * String staffCode, String staffName, List<String> lstStaffOwnerCode, Long
	 * shopId) throws DataAccessException { StringBuilder sql = new
	 * StringBuilder(); List<Object> params = new ArrayList<Object>();
	 * sql.append(" select s.* from staff s");
	 * sql.append(" where status = 1 and staff_type_id in"); sql.append(
	 * " (select channel_type_id from channel_type where status = 1 and type = 2 and object_type in(1,2))"
	 * ); if (!StringUtility.isNullOrEmpty(staffCode)) {
	 * sql.append(" and staff_code like ? ESCAPE '/'");
	 * params.add(StringUtility.
	 * toOracleSearchLikeSuffix(staffCode.toUpperCase())); } if
	 * (!StringUtility.isNullOrEmpty(staffName)) {
	 * sql.append(" and upper(staff_name) like ? ESCAPE '/'");
	 * params.add(StringUtility
	 * .toOracleSearchLikeSuffix(staffName.toUpperCase())); } if(shopId!=null){
	 * sql.append(" and  shop_id = ? "); params.add(shopId); } if
	 * (lstStaffOwnerCode != null && lstStaffOwnerCode.size() != 0 &&
	 * lstStaffOwnerCode.get(0) != null) { sql.append(" and ("); for (String
	 * staffOwnerCode: lstStaffOwnerCode) { if
	 * (!StringUtility.isNullOrEmpty(staffOwnerCode)) { sql.append(
	 * " exists (select 1 from staff where staff_id = s.staff_owner_id and upper(staff_code) like ? ESCAPE '/') or "
	 * );
	 * params.add(StringUtility.toOracleSearchLikeSuffix(staffOwnerCode.toUpperCase
	 * ())); } } sql.append(" 0 = 1)"); } sql.append(" order by staff_code"); //
	 * System.out.println(TestUtils.addParamToQuery(sql.toString(),
	 * params.toArray())); if (kPaging == null) return
	 * repo.getListBySQL(Staff.class, sql.toString(), params); else return
	 * repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	 * }
	 */

	@Override
	public List<Staff> getListNVBH2(KPaging<Staff> kPaging, String staffCode, String staffName, List<String> lstStaffOwnerCode, String strListShopId, Boolean isGetChildShop, List<String> lstTBHVCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		boolean check = false;
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.* from staff s");
		sql.append(" where status = 1 and staff_type_id in");
		sql.append(" (select channel_type_id from channel_type where status = 1 and type = 2 and object_type in(1,2))");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and upper(staff_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffName.toUpperCase()));
		}
		if (lstStaffOwnerCode != null && lstStaffOwnerCode.size() != 0 && lstStaffOwnerCode.get(0) != null) {
			sql.append(" and (");
			for (String staffOwnerCode : lstStaffOwnerCode) {
				if (!StringUtility.isNullOrEmpty(staffOwnerCode)) {
					sql.append(" exists (select 1 from staff where staff_id = s.staff_owner_id and upper(staff_code) like ? ESCAPE '/') or ");
					params.add(StringUtility.toOracleSearchLikeSuffix(staffOwnerCode.toUpperCase()));
				}
			}
			sql.append(" 0 = 1)");
		} else { // neu' co' truyen vao lstTBHV se~ lay' tat ca cac con
			if (lstTBHVCode != null && lstTBHVCode.size() != 0) {
				sql.append(" and shop_id in ");
				sql.append(" (select shop_id from shop where status = 1 ");
				sql.append(" start with shop_id in (");
				for (int i = 0; i < lstTBHVCode.size(); i++) {
					String staffTBHVCode = lstTBHVCode.get(i);
					if (!StringUtility.isNullOrEmpty(staffTBHVCode)) {
						sql.append("(select shop_id from staff where upper(staff_code) = upper(? ");
						params.add(staffTBHVCode.toUpperCase().trim());
						sql.append("))");
						if (i != lstTBHVCode.size() - 1) {
							sql.append(",");
						}
					}
				}
				sql.append(")");
				sql.append(" connect by prior shop_id = parent_shop_id)");
				check = true;
			}
		}
		if (!check) {
			if (isGetChildShop != null && Boolean.TRUE.equals(isGetChildShop)) {
				sql.append(" and shop_id in ");
				sql.append(" (select shop_id from shop where status = 1 ");
				sql.append(" start with shop_id in (");
				sql.append(" select regexp_substr(?,'[^,]+', 1, level) ");
				sql.append(" from dual connect by regexp_substr(?, '[^,]+', 1, level) is not null ");
				sql.append(" ) ");
				params.add(strListShopId);
				params.add(strListShopId);
				sql.append(" connect by prior shop_id = parent_shop_id)");
			} else {
				sql.append(" and shop_id in (");
				sql.append(" select regexp_substr(?,'[^,]+', 1, level) ");
				sql.append(" from dual connect by regexp_substr(?, '[^,]+', 1, level) is not null ");
				sql.append(" ) ");
				params.add(strListShopId);
				params.add(strListShopId);
			}
		}
		sql.append(" order by staff_code");

		// System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));

		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.StaffDAO#getListStaffExceptListStaff(com.viettel
	 * .core.entities.enumtype.KPaging, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String,
	 * ths.dms.core.entities.enumtype.ActiveType, java.lang.String,
	 * ths.dms.core.entities.enumtype.StaffObjectType,
	 * ths.dms.core.entities.enumtype.ChannelTypeType, java.util.List,
	 * java.lang.String, java.lang.String, java.lang.Boolean, java.lang.String,
	 * java.lang.String, java.lang.String, java.util.List)
	 */
	@Override
	public List<Staff> getListStaffExceptListStaff(KPaging<Staff> kPaging, String staffCode, String staffName, ActiveType status, String shopCode, Long staffTypeId, List<Long> exceptStaffIds) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select st.* from STAFF st join shop s on st.shop_id = s.shop_id where 1 = 1");

		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and st.staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			String temp = StringUtility.toOracleSearchLike(staffName.toLowerCase());
			sql.append(" and lower(st.staff_name) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			String temp = StringUtility.toOracleSearchLike(shopCode.toLowerCase());
			sql.append(" and lower(s.shop_code) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (status != null) {
			sql.append(" and st.status=?");
			params.add(status.getValue());
		}
		if (staffTypeId != null) {
			sql.append(" and st.staff_type_id = ?");
			params.add(staffTypeId);
		}
		if (exceptStaffIds != null && exceptStaffIds.size() > 0) {
			sql.append(" and st.staff_id not in (");
			for (Long staffId : exceptStaffIds) {
				sql.append("?, ");
				params.add(staffId);
			}
			sql.append("0)");
		}

		sql.append(" order by s.shop_code, st.staff_code asc");
		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<Staff> getListStaffForAbsentReport(KPaging<Staff> kPaging, String staffCode, String staffName, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *");
		sql.append(" FROM staff");
		sql.append(" WHERE status       = 1");
		sql.append(" AND staff_type_id IN");
		sql.append("   (SELECT channel_type_id");
		sql.append("   FROM channel_type");
		sql.append("   WHERE status     = 1");
		sql.append("   AND type         = 2");
		sql.append("   AND object_type IN(1,2,5,7)");
		sql.append("   )");
		if (shopId != null) {
			sql.append(" AND shop_id IN");
			sql.append("   (SELECT shop_id");
			sql.append("   FROM shop");
			sql.append("     START WITH shop_id       = ?");
			sql.append("     CONNECT BY prior shop_id = parent_shop_id");
			sql.append("   )");
			params.add(shopId);
		}

		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			sql.append(" and upper(staff_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffName.toUpperCase()));
		}

		sql.append(" ORDER BY staff_code");

		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");

		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.StaffDAO#getListPreAndVanStaffHasChild(com.viettel
	 * .core.entities.enumtype.KPaging, java.lang.String, java.lang.String,
	 * java.util.List, ths.dms.core.entities.enumtype.ActiveType,
	 * java.util.List, java.lang.Boolean, java.util.List)
	 */
	@Override
	public List<Staff> getListPreAndVanStaffHasChild(KPaging<Staff> kPaging, String staffCode, String staffName, List<Long> shopIds, ActiveType staffActiveType, List<StaffObjectType> channelType, Boolean isHasChild, List<Long> listStaffOwnerIds)
			throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT s.* ");
		sql.append(" FROM staff s JOIN channel_type ct ON ct.status = 1 and (s.staff_type_id = ct.channel_type_id) ");
		sql.append(" WHERE 1 = 1 ");

		if (null != shopIds && shopIds.size() > 0) {
			if (Boolean.TRUE.equals(isHasChild)) {
				sql.append(" AND s.shop_id in (select shop_id from shop start with shop_id in ( ");
				for (Long shopId : shopIds) {
					sql.append(" ?,");
					params.add(shopId);
				}
				sql.append(" -1)");
				sql.append(" connect by prior shop_id = parent_shop_id and status = ?)");
				params.add(ActiveType.RUNNING.getValue());
			} else {
				sql.append(" 	AND s.shop_id in (");
				for (Long shopId : shopIds) {
					sql.append(" ?,");
					params.add(shopId);
				}
				sql.append(" -1)");
			}
		}

		sql.append(" 	AND s.status = ?");
		if (null != staffActiveType) {
			params.add(staffActiveType.getValue());
		} else {
			params.add(ActiveType.RUNNING.getValue());
		}

		if (listStaffOwnerIds != null && listStaffOwnerIds.size() > 0) {
			sql.append(" AND s.staff_owner_id in (");
			for (Long staffOwnerId : listStaffOwnerIds) {
				sql.append(" ?, ");
				params.add(staffOwnerId);
			}
			sql.append(" -1)");
		}

		if (staffCode != null && !staffCode.equals("")) {
			sql.append(" and s.staff_code like ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}

		if (staffName != null && !staffName.equals("")) {
			sql.append(" and upper(s.staff_name) like ?");
			params.add(StringUtility.toOracleSearchLike(staffName.toUpperCase()));
		}

		if (null != channelType && channelType.size() > 0) {
			sql.append(" 	AND ct.OBJECT_TYPE in (");

			for (int i = 0; i < channelType.size(); i++) {
				if (i < channelType.size() - 1) {
					sql.append("?, ");
				} else {
					sql.append("?)");
				}

				params.add(channelType.get(i).getValue());
			}
		}

		sql.append(" 	ORDER BY s.staff_code");

		if (kPaging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
	}

	//--------------------------
	@Override
	public Staff getNVGSInGroup(Long staffGroupId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from staff st where status = 1 ");
		sql.append("	and exists (select 1 from staff_group_detail where status = 1 and staff_id = st.staff_id and staff_group_id = ?)");
		params.add(staffGroupId);
		sql.append("	and exists (select 1 from channel_type where channel_type_id = staff_type_id and object_type = ?)");
		params.add(StaffObjectType.NVGS.getValue());
		return repo.getFirstBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public Staff getTBHVOfShop(Long vungId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from staff st where status = 1 ");
		sql.append("	and exists (select 1 from staff_group_detail sgd join staff_group sg on sgd.staff_group_id = sg.staff_group_id");
		sql.append(" 			where staff_id = st.staff_id and sgd.status = 1 and sg.status = 1 and sg.shop_id = ?)");
		params.add(vungId);
		sql.append("	and exists (select 1 from channel_type where channel_type_id = staff_type_id and object_type = ?)");
		params.add(StaffObjectType.TBHV.getValue());
		return repo.getFirstBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public Staff getTBHMOfShop(Long mienId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from staff st where status = 1 ");
		sql.append("	and exists (select 1 from staff_group_detail sgd join staff_group sg on sgd.staff_group_id = sg.staff_group_id");
		sql.append(" 			where  sgd.status = 1 and sg.status = 1  and staff_id = st.staff_id and sg.shop_id = ?)");
		params.add(mienId);
		sql.append("	and exists (select 1 from channel_type where channel_type_id = staff_type_id and object_type = ?)");
		params.add(StaffObjectType.TBHM.getValue());
		return repo.getFirstBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public List<Staff> getListNVBHInGroup(Long staffGroupId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from staff st where status = 1 ");
		sql.append("	and exists (select 1 from staff_group_detail where status = 1 and staff_id = st.staff_id and staff_group_id = ?)");
		params.add(staffGroupId);
		sql.append("	and exists (select 1 from channel_type where channel_type_id = staff_type_id and object_type in (?,?))");
		params.add(StaffObjectType.NVBH.getValue());
		params.add(StaffObjectType.NVVS.getValue());
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public List<Staff> getListNVGSInVung(Long vungId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from staff st where status = 1 ");
		sql.append("	and exists (select 1 from staff_group_detail sgd join staff_group sg on sgd.staff_group_id = sg.staff_group_id ");
		sql.append("		where staff_id = st.staff_id and sg.status = 1 and sgd.status = 1 and shop_id in (select shop_id from shop where parent_shop_id = ?))");
		params.add(vungId);
		sql.append("	and exists (select 1 from channel_type where channel_type_id = staff_type_id and object_type = ?)");
		params.add(StaffObjectType.NVGS.getValue());
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public List<Staff> getListTBHVInMien(Long mienId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from staff st where status = 1 ");
		sql.append("	and exists (select 1 from staff_group_detail sgd join staff_group sg on sgd.staff_group_id = sg.staff_group_id ");
		sql.append("		where  sgd.status = 1 and sg.status = 1  and staff_id = st.staff_id and shop_id in (select shop_id from shop where parent_shop_id = ?))");
		params.add(mienId);
		sql.append("	and exists (select 1 from channel_type where channel_type_id = staff_type_id and object_type = ?)");
		params.add(StaffObjectType.TBHV.getValue());
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public List<ShopTreeVO> getListShopManagedByStaff(Long staffId, KPaging<ShopTreeVO> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT distinct gr.shop_id id,");
		sql.append("   (SELECT shop_code FROM shop WHERE shop_id = gr.shop_id");
		sql.append("   ) shopCode,");
		sql.append("   (SELECT shop_name FROM shop WHERE shop_id = gr.shop_id");
		sql.append("   ) shopName");
		sql.append(" FROM staff_group gr");
		sql.append(" JOIN staff_group_detail dt");
		sql.append(" ON gr.staff_group_id = dt.staff_group_id");
		sql.append(" where 1 = 1 and gr.status = 1");
		if (staffId != null) {
			sql.append(" and dt.staff_id = ?");
			params.add(staffId);
		}
		sql.append(" order by shopCode, id asc");

		String[] fieldNames = new String[] { "id", "shopCode", "shopName" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (kPaging != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(ShopTreeVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(ShopTreeVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public Boolean checkShopManagedByNVGS(Long gsnppId, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH lstGS AS ");
		sql.append(" (SELECT st.staff_id ");
		sql.append(" FROM STAFF st ");
		sql.append(" INNER JOIN channel_type ct ");
		sql.append(" ON st.staff_type_id = ct.channel_type_id ");
		sql.append(" AND ct.object_type = 5 ");
		sql.append(" AND ct.type = 2 ");
		sql.append(" AND ct.status = 1 ");
		sql.append(" AND st.staff_id = ? ");
		params.add(gsnppId);
		sql.append(" ), ");
		sql.append(" lstShop as ( ");
		sql.append(" SELECT DISTINCT gr.shop_id ");
		sql.append(" FROM staff_group gr ");
		sql.append(" JOIN staff_group_detail dt ");
		sql.append(" ON gr.staff_group_id = dt.staff_group_id ");
		sql.append(" WHERE gr.status = 1 ");
		sql.append(" and gr.shop_id = ? ");
		sql.append(" AND exists (select 1 from lstGS where staff_id = dt.staff_id)) ");
		sql.append(" select count(1) count from lstShop ");
		params.add(shopId);
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public List<SupervisorSaleVO> getListSupervisorSaleVO(KPaging<SupervisorSaleVO> kPaging, Long tbhvId, Long shopId) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder("");
		sql.append(" with lstShop as ( ");
		sql.append(" select s.* from shop s ");
		sql.append(" where s.status=1 start with s.shop_id = ? connect by prior shop_id=parent_shop_id ");
		params.add(shopId);
		sql.append(" ) ,");
		sql.append(" lstStaff as ( ");
		sql.append(" select s.* from staff s ");
		sql.append(" where s.staff_id  in ( ");
		sql.append(" select psm.staff_id as staffId ");
		sql.append(" from parent_staff_map psm ");
		sql.append(" where psm.status = 1 ");
		sql.append(" and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id) ");
		sql.append(" start with psm.parent_staff_id = ? AND PSM.STATUS = 1 connect by prior psm.staff_id = psm.parent_staff_id ");
		params.add(tbhvId);
		sql.append(" ) and s.shop_id in (select shop_id from lstShop) ), ");
		sql.append(" lstStaffOneNode as ( ");
		sql.append(" select s.* from lstStaff s where s.staff_id in ");
		sql.append(" (select staff_id from parent_staff_map where parent_staff_id=?) ");
		params.add(tbhvId);
		sql.append(" ), ");
		sql.append(" lstRootNV as ( ");
		sql.append(" SELECT DISTINCT CONNECT_BY_ROOT(PSMP.PARENT_STAFF_ID) AS ROOT_ID, ");
		sql.append(" PSMP.STAFF_ID FROM PARENT_STAFF_MAP PSMP ");
		sql.append(" JOIN lstStaff SF ON PSMP.STAFF_ID = SF.STAFF_ID AND SF.STATUS = 1 ");
		sql.append(" WHERE PSMP.STATUS = 1 AND STAFF_TYPE_ID IN ");
		sql.append(" (SELECT CHANNEL_TYPE_ID FROM CHANNEL_TYPE WHERE TYPE = 2 AND OBJECT_TYPE IN (1, 2) AND STATUS = 1) ");
		sql.append(" START WITH PSMP.PARENT_STAFF_ID IN (select staff_id from lstStaffOneNode) ");
		sql.append(" AND PSMP.STATUS = 1 ");
		sql.append(" CONNECT BY PRIOR PSMP.STAFF_ID = PSMP.PARENT_STAFF_ID ");
		sql.append(" ), ");
		sql.append(" lstSaleNV as ( ");
		sql.append(" select distinct rpt.staff_id,rpt.shop_id,rpt.month_amount_plan,rpt.month_amount,rpt.day_amount_plan,rpt.day_amount, ");
		sql.append(" rpt.day_cust,rpt.day_cust_order_plan,rpt.day_cust_order,rpt.create_date,rpt.DAY_APPROVED_AMOUNT ");
		sql.append(" from lstStaff s ");
		sql.append(" join channel_type ct on ct.channel_type_id=s.staff_type_id and ct.object_type in (1,2) ");
		sql.append(" join rpt_staff_sale rpt on rpt.staff_id = s.staff_id ");
		sql.append(" ) ");
		sql.append(" select s.staff_id staffId,s.staff_code staffcode, s.staff_name staffName, s.shop_id shopId, ");
		sql.append(" (select shop_code from shop where shop_id=s.shop_id) shopCode, ");
		sql.append(" (select shop_name from shop where shop_id=s.shop_id) shopName, ");
		sql.append(" (SELECT NVL(SUM(RPT.DAY_AMOUNT_PLAN),0) AS ");
		sql.append(" FROM lstSaleNV rpt ");
		sql.append(" join lstRootNV root on rpt.staff_id=root.staff_id ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND rpt.CREATE_DATE   >= TRUNC(SYSDATE,'MM') ");
		sql.append(" AND rpt.CREATE_DATE      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		sql.append(" and s.staff_id=root.root_id ");
		sql.append(" )AS dayAmountPlan, ");
		sql.append(" (SELECT NVL(SUM(rpt.DAY_AMOUNT),0) AS ");
		sql.append(" FROM lstSaleNV rpt ");
		sql.append(" join lstRootNV root on rpt.staff_id=root.staff_id ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND rpt.CREATE_DATE   >= TRUNC(SYSDATE,'MM') ");
		sql.append(" AND rpt.CREATE_DATE      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		sql.append(" and s.staff_id=root.root_id ");
		sql.append(" ) AS dayAmount, ");
		sql.append(" (SELECT NVL(SUM(rpt.DAY_APPROVED_AMOUNT),0) AS ");
		sql.append(" FROM lstSaleNV rpt ");
		sql.append(" join lstRootNV root on rpt.staff_id=root.staff_id ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND rpt.CREATE_DATE   >= TRUNC(SYSDATE,'MM') ");
		sql.append(" AND rpt.CREATE_DATE      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		sql.append(" and s.staff_id=root.root_id ");
		sql.append(" ) AS dayAmountApproved ");
		sql.append(" from staff s ");
		sql.append(" where s.status=1 ");
		sql.append(" and s.staff_id in (select staff_id from parent_staff_map where parent_staff_id=?) ");
		params.add(tbhvId);
		sql.append(" order by shopCode,s.staff_code ");
		//		sql.append(" SELECT ");
		//		sql.append(" s.SHOP_ID as shopId, ");
		//		sql.append(" s.SHOP_CODE as shopCode, ");
		//		sql.append(" s.SHOP_NAME as shopName, ");
		//		sql.append(" f.STAFF_ID as staffId, ");
		//		sql.append(" f.STAFF_NAME as staffName, ");
		//		sql.append(" f.STAFF_CODE as staffCode ");
		//		sql.append(" , ");
		//		sql.append(" (SELECT SUM(nvl(RPT.DAY_AMOUNT_PLAN,0)) AS ");
		//		sql.append(" FROM RPT_STAFF_SALE rpt inner join staff st on  rpt.staff_id = st.staff_id and st.status = 1 ");
		//		sql.append(" WHERE ");
		//		sql.append(" TRUNC(rpt.CREATE_DATE,'YYYY')=TRUNC(SYSDATE, 'YYYY') AND TRUNC(rpt.CREATE_DATE,'MM')=TRUNC(SYSDATE, 'MM') ");
		//		sql.append(" AND ");
		//		sql.append(" RPT.PARENT_STAFF_ID = f.staff_id ");
		//		sql.append(" )as dayAmountPlan, ");
		//		sql.append(" (SELECT NVL(SUM(rpt.DAY_AMOUNT),0) AS ");
		//		sql.append(" FROM RPT_STAFF_SALE rpt inner join staff st on rpt.staff_id = st.staff_id and st.status = 1 ");
		//		sql.append(" WHERE ");
		//		sql.append(" TRUNC(rpt.CREATE_DATE,'YYYY')=TRUNC(SYSDATE, 'YYYY') AND TRUNC(rpt.CREATE_DATE,'MM')=TRUNC(SYSDATE, 'MM') ");
		//		sql.append(" AND ");
		//		sql.append(" RPT.PARENT_STAFF_ID = f.staff_id ");
		//		sql.append(" ) AS dayAmount, ");
		//		sql.append(" (SELECT NVL(SUM(rpt.DAY_APPROVED_AMOUNT),0) AS ");
		//		sql.append(" FROM RPT_STAFF_SALE rpt inner join staff st on rpt.staff_id = st.staff_id and st.status = 1 ");
		//		sql.append(" WHERE ");
		//		sql.append(" TRUNC(rpt.CREATE_DATE,'YYYY')=TRUNC(SYSDATE, 'YYYY') AND TRUNC(rpt.CREATE_DATE,'MM')=TRUNC(SYSDATE, 'MM') ");
		//		sql.append(" AND ");
		//		sql.append(" RPT.PARENT_STAFF_ID = f.staff_id ");
		//		sql.append(" ) AS dayAmountApproved ");
		//		sql.append(" FROM STAFF f INNER JOIN SHOP s ON F.SHOP_ID = S.SHOP_ID INNER JOIN channel_type ct on f.staff_type_id = ct.channel_type_id and ct.object_type = 5 and ct.type = 2 and ct.status = 1 ");
		//		sql.append(" WHERE f.STAFF_OWNER_ID = ? AND F.STATUS=1 ");
		//		params.add(tbhvId);
		//		sql.append(" order by s.shop_code, f.staff_name");
		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "staffId", "staffCode", "staffName", "dayAmountPlan", "dayAmount", "dayAmountApproved" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL };

		if (null == kPaging) {
			return repo.getListByQueryAndScalar(SupervisorSaleVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(SupervisorSaleVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}

	@Override
	public List<StaffPositionVO> getListStaffPositionWithTraining(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT DISTINCT NV.STAFF_ID AS STAFFID,NV.STAFF_CODE AS STAFFCODE, NV.STAFF_NAME AS STAFFNAME,");
		sql.append(" gs.staff_id AS staffOwnerId,gs.staff_code AS staffOwnerCode, gs.STAFF_NAME AS staffOwnerName");
		sql.append(" FROM training_plan tp");
		sql.append(" JOIN training_plan_detail tpd");
		sql.append(" ON tp.TRAINING_PLAN_ID = tpd.training_plan_id");
		sql.append(" JOIN staff nv");
		sql.append(" ON tpd.staff_id = nv.staff_id");
		sql.append(" JOIN staff gs");
		sql.append(" ON tp.staff_id = gs.staff_id");
		sql.append(" JOIN staff_position_log sp");
		sql.append(" ON sp.staff_id      = tpd.staff_id");
		sql.append(" WHERE 1             =1");
		sql.append(" AND tp.status       =1");
		sql.append(" AND nv.status       =1");
		sql.append(" AND GS.STATUS       = 1");
		sql.append(" AND TRUNC(tp.month) = TRUNC(sysdate,'mm')");
		sql.append(" AND tp.shop_id                    IN");
		sql.append(" (SELECT shop_id");
		sql.append(" FROM shop");
		sql.append(" START WITH shop_id       = ?");
		params.add(shopId);
		sql.append(" CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID");
		sql.append(" )");
		sql.append(" AND TRUNC(tpd.training_date) = TRUNC(sysdate)");
		sql.append(" AND TPD.STATUS                             IN (2,1)");
		sql.append(" AND TRUNC(sp.create_date)    = TRUNC(sysdate)");

		final String[] fieldNames = new String[] { "staffId", //1
				"staffCode", //2
				"staffName", //3
				"staffOwnerId",//4
				"staffOwnerCode",//5
				"staffOwnerName"//6
		};

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.LONG, //4
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.STRING };//6
		return repo.getListByQueryAndScalar(StaffPositionVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<StaffPositionVO> getListStaffPosition(Long shopId, Long staffId, Boolean isVNM, Boolean isMien, Boolean isVung, Boolean isNPP, Boolean oneNode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String[] fieldNames = null;
		Type[] fieldTypes = null;

		if (Boolean.TRUE.equals(oneNode)) {
			if (staffId != null && staffId != 0) {//lay ra danh sach nv co staff_owner_id = staffId + 
				sql.append(" SELECT c_ST.STAFF_ID AS staffId, ");
				sql.append(" c_ST.STAFF_CODE AS staffCode, ");
				sql.append(" c_ST.STAFF_NAME AS staffName, ");
				sql.append(" TB.LAT AS lat, ");
				sql.append(" TB.lng AS lng, ");
				sql.append(" c_ST.SHOP_ID AS shopId, ");
				sql.append(" C_SH.SHOP_CODE AS shopCode, ");
				sql.append(" C_SH.SHOP_NAME AS shopName, ");
				sql.append(" ct.OBJECT_TYPE as roleType, ");
				sql.append(" TB.ACCURACY as accuracy, ");
				sql.append(" TB.CREATE_DATE as createTime, ");
				sql.append(" (SELECT COUNT(*) FROM Staff s2 WHERE s2.staff_owner_id= c_ST.STAFF_ID) as countStaff ");
				sql.append(" FROM STAFF c_st INNER JOIN CHANNEL_TYPE ct on C_ST.STAFF_TYPE_ID = CT.CHANNEL_TYPE_ID inner join shop c_sh on c_st.shop_id = c_sh.shop_id ");
				sql.append(" LEFT JOIN ");
				sql.append(" (SELECT spl.* FROM staff_position_log spl  ");
				sql.append(" WHERE spl.create_date >= trunc(sysdate) ");
				sql.append(" AND spl.STAFF_POSITION_LOG_ID =");
				sql.append(" (select max(STAFF_POSITION_LOG_ID) from staff_position_log sp2 where sp2.staff_id = spl.staff_id and sp2.create_date >= trunc(sysdate))");
				sql.append(" ) tb");
				sql.append(" ON c_st.STAFF_ID=TB.STAFF_ID ");
				sql.append(" WHERE ct.type=2 and ct.status=1 and c_st.STATUS = 1 ");
				sql.append(" AND c_st.STAFF_OWNER_ID = ? ");
				params.add(staffId);
				sql.append(" and c_st.shop_id IN (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
				params.add(shopId);
				sql.append(" order by c_ST.STAFF_NAME ");

				fieldNames = new String[] { "staffId", //1
						"staffCode", //2
						"staffName", //3
						"lat",//4
						"lng",//5
						"shopId",//6
						"shopCode",//7
						"shopName",//7
						"roleType", //8			
						"accuracy",//9
						"createTime",//10
						"countStaff"//11
				};

				fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
						StandardBasicTypes.STRING, //2
						StandardBasicTypes.STRING, //3
						StandardBasicTypes.FLOAT, //4
						StandardBasicTypes.FLOAT, //5
						StandardBasicTypes.LONG, //6
						StandardBasicTypes.STRING, //7
						StandardBasicTypes.STRING, //7
						StandardBasicTypes.INTEGER, //8	
						StandardBasicTypes.FLOAT,//9
						StandardBasicTypes.TIMESTAMP,//10
						StandardBasicTypes.INTEGER //11
				};

			} else {
				if (Boolean.TRUE.equals(isVNM)) {//lay danh sach gdm
					sql.append("SELECT ST.STAFF_ID AS STAFFID, ST.STAFF_CODE    AS STAFFCODE, ST.STAFF_NAME    AS STAFFNAME, CT.OBJECT_TYPE   AS ROLETYPE FROM STAFF ST INNER JOIN CHANNEL_TYPE CT ON ST.STAFF_TYPE_ID  = CT.CHANNEL_TYPE_ID INNER JOIN SHOP S ON ST.SHOP_ID = S.SHOP_ID WHERE CT.OBJECT_TYPE = ? AND CT.TYPE = 2 AND CT.STATUS = 1 AND S.STATUS = ? AND S.PARENT_SHOP_ID = ? and st.status = 1 and s.SHOP_TYPE_ID = ? ORDER BY ST.STAFF_NAME");
					params.add(StaffObjectType.TBHM.getValue());
					params.add(ActiveType.RUNNING.getValue());
					params.add(shopId);
					params.add(9);//id channel type cua MIEN trong bang channel_type
					fieldNames = new String[] { "staffId", //1
							"staffCode", //2
							"staffName", //3
							"roleType" //4
					};

					fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
							StandardBasicTypes.STRING, //2
							StandardBasicTypes.STRING,//3
							StandardBasicTypes.INTEGER //4
					};

				} else if (Boolean.TRUE.equals(isMien)) {//lay danh sach tbhv. shop chon la mien
					sql.append("SELECT C_ST.STAFF_ID AS STAFFID, C_ST.STAFF_CODE AS STAFFCODE, C_ST.STAFF_NAME AS STAFFNAME, TB.LAT AS LAT, TB.LNG AS LNG, C_ST.SHOP_ID AS SHOPID, s.shop_code AS shopCode, s.shop_name AS shopName, ct.OBJECT_TYPE AS roleType, TB.ACCURACY AS accuracy, TB.CREATE_DATE AS createTime, (SELECT COUNT(*) FROM Staff s2 WHERE s2.staff_owner_id= c_ST.STAFF_ID) AS countStaff ");
					sql.append("FROM STAFF c_st ");
					sql.append("INNER JOIN CHANNEL_TYPE ct ");
					sql.append("ON C_ST.STAFF_TYPE_ID = CT.CHANNEL_TYPE_ID ");
					sql.append("inner join shop s on c_st.shop_id = s.shop_id ");
					sql.append("LEFT JOIN ");
					sql.append("(SELECT SPL.* FROM STAFF_POSITION_LOG SPL ");
					sql.append("WHERE spl.create_date        >= TRUNC(sysdate) ");
					sql.append("AND spl.STAFF_POSITION_LOG_ID = ");
					sql.append("(SELECT MAX(STAFF_POSITION_LOG_ID) ");
					sql.append("FROM staff_position_log sp2 ");
					sql.append("WHERE sp2.staff_id   = spl.staff_id ");
					sql.append("AND sp2.create_date >= TRUNC(sysdate) ");
					sql.append(") ");
					sql.append(") tb ");
					sql.append("ON c_st.STAFF_ID        =TB.STAFF_ID ");
					sql.append("WHERE ct.type           =2 ");
					sql.append("AND ct.status           =1 ");
					sql.append("AND C_ST.STATUS         = 1 ");
					sql.append("AND c_st.STAFF_OWNER_ID IN (select staff_id from staff gdm inner join channel_type ct1 on gdm.staff_type_id = ct1.channel_type_id and ct1.status = 1 and ct1.type = 2 and ct1.object_type = ? where gdm.shop_id = ?) ");
					sql.append("ORDER BY c_ST.STAFF_NAME ");

					params.add(StaffObjectType.TBHM.getValue());
					params.add(shopId);

					fieldNames = new String[] { "staffId", //1
							"staffCode", //2
							"staffName", //3
							"lat",//4
							"lng",//5
							"shopId",//6
							"shopCode",//7
							"shopName",//7
							"roleType", //8			
							"accuracy",//9
							"createTime",//10
							"countStaff"//11
					};

					fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
							StandardBasicTypes.STRING, //2
							StandardBasicTypes.STRING, //3
							StandardBasicTypes.FLOAT, //4
							StandardBasicTypes.FLOAT, //5
							StandardBasicTypes.LONG, //6
							StandardBasicTypes.STRING, //7
							StandardBasicTypes.STRING, //7
							StandardBasicTypes.INTEGER, //8	
							StandardBasicTypes.FLOAT,//9
							StandardBasicTypes.TIMESTAMP,//10
							StandardBasicTypes.INTEGER //11
					};
				} else if (Boolean.TRUE.equals(isVung)) {//lay ra danh sach gs. shop chon la vung
					sql.append("SELECT C_ST.STAFF_ID AS STAFFID, C_ST.STAFF_CODE AS STAFFCODE, C_ST.STAFF_NAME AS STAFFNAME, TB.LAT AS LAT, TB.LNG AS LNG, C_ST.SHOP_ID AS SHOPID, s.shop_code AS shopCode,s.shop_name AS shopName, ct.OBJECT_TYPE AS roleType, TB.ACCURACY AS accuracy, TB.CREATE_DATE AS createTime, (SELECT COUNT(*) FROM Staff s2 WHERE s2.staff_owner_id= c_ST.STAFF_ID) AS countStaff ");
					sql.append("FROM STAFF c_st ");
					sql.append("INNER JOIN CHANNEL_TYPE ct ");
					sql.append("ON C_ST.STAFF_TYPE_ID = CT.CHANNEL_TYPE_ID ");
					sql.append("inner join shop s on c_st.shop_id = s.shop_id ");
					sql.append("LEFT JOIN ");
					sql.append("(SELECT SPL.* FROM STAFF_POSITION_LOG SPL ");
					sql.append("WHERE spl.create_date        >= TRUNC(sysdate) ");
					sql.append("AND spl.STAFF_POSITION_LOG_ID = ");
					sql.append("(SELECT MAX(STAFF_POSITION_LOG_ID) ");
					sql.append("FROM staff_position_log sp2 ");
					sql.append("WHERE sp2.staff_id   = spl.staff_id ");
					sql.append("AND sp2.create_date >= TRUNC(sysdate) ");
					sql.append(") ");
					sql.append(") tb ");
					sql.append("ON c_st.STAFF_ID        =TB.STAFF_ID ");
					sql.append("WHERE ct.type           =2 ");
					sql.append("AND ct.status           =1 ");
					sql.append("AND C_ST.STATUS         = 1 ");
					sql.append("AND c_st.STAFF_OWNER_ID IN (select staff_id from staff gdm inner join channel_type ct1 on gdm.staff_type_id = ct1.channel_type_id and ct1.status = 1 and ct1.type = 2 and ct1.object_type = ? where gdm.shop_id = ?) ");
					sql.append("ORDER BY c_ST.STAFF_NAME ");

					params.add(StaffObjectType.TBHV.getValue());
					params.add(shopId);

					fieldNames = new String[] { "staffId", //1
							"staffCode", //2
							"staffName", //3
							"lat",//4
							"lng",//5
							"shopId",//6
							"shopCode",//7
							"shopName",//7
							"roleType", //8			
							"accuracy",//9
							"createTime",//10
							"countStaff"//11
					};

					fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
							StandardBasicTypes.STRING, //2
							StandardBasicTypes.STRING, //3
							StandardBasicTypes.FLOAT, //4
							StandardBasicTypes.FLOAT, //5
							StandardBasicTypes.LONG, //6
							StandardBasicTypes.STRING, //7
							StandardBasicTypes.STRING, //7
							StandardBasicTypes.INTEGER, //8	
							StandardBasicTypes.FLOAT,//9
							StandardBasicTypes.TIMESTAMP,//10
							StandardBasicTypes.INTEGER //11
					};
				} else if (Boolean.TRUE.equals(isNPP)) {//lay ra danh sach gs nvbh cua npp do. shop chọn la npp
					sql.append("SELECT C_ST.STAFF_ID AS STAFFID, C_ST.STAFF_CODE AS STAFFCODE, C_ST.STAFF_NAME AS STAFFNAME, TB.LAT AS LAT, TB.LNG AS LNG, C_ST.SHOP_ID AS SHOPID, s.shop_code AS shopCode,s.shop_name AS shopName, ct.OBJECT_TYPE AS roleType, TB.ACCURACY AS accuracy, TB.CREATE_DATE AS createTime, (SELECT COUNT(*) FROM Staff s2 WHERE s2.staff_owner_id= c_ST.STAFF_ID) AS countStaff ");
					sql.append("FROM STAFF c_st ");
					sql.append("INNER JOIN CHANNEL_TYPE ct ");
					sql.append("ON C_ST.STAFF_TYPE_ID = CT.CHANNEL_TYPE_ID ");
					sql.append("inner join shop s on c_st.shop_id = s.shop_id ");
					sql.append("LEFT JOIN ");
					sql.append("(SELECT SPL.* FROM STAFF_POSITION_LOG SPL ");
					sql.append("WHERE spl.create_date        >= TRUNC(sysdate) ");
					sql.append("AND spl.STAFF_POSITION_LOG_ID = ");
					sql.append("(SELECT MAX(STAFF_POSITION_LOG_ID) ");
					sql.append("FROM staff_position_log sp2 ");
					sql.append("WHERE sp2.staff_id   = spl.staff_id ");
					sql.append("AND sp2.create_date >= TRUNC(sysdate) ");
					sql.append(") ");
					sql.append(") tb ");
					sql.append("ON c_st.STAFF_ID        =TB.STAFF_ID ");
					sql.append("WHERE ct.type           =2 and ct.object_type = ? ");
					params.add(StaffObjectType.NVGS.getValue());
					sql.append("AND ct.status           =1 ");
					sql.append("AND C_ST.STATUS         = 1 ");
					sql.append("AND c_st.STAFF_ID IN (select staff_owner_id from staff nvbh inner join channel_type ct1 on nvbh.staff_type_id = ct1.channel_type_id and ct1.status = 1 and ct1.type = 2 and ct1.object_type IN (?, ?) where nvbh.shop_id = ? and nvbh.status = 1) ");
					sql.append("ORDER BY c_ST.STAFF_NAME ");

					params.add(StaffObjectType.NVBH.getValue());
					params.add(StaffObjectType.NVVS.getValue());
					params.add(shopId);

					fieldNames = new String[] { "staffId", //1
							"staffCode", //2
							"staffName", //3
							"lat",//4
							"lng",//5
							"shopId",//6
							"shopCode",//7
							"shopName",//7
							"roleType", //8			
							"accuracy",//9
							"createTime",//10
							"countStaff"//11
					};

					fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
							StandardBasicTypes.STRING, //2
							StandardBasicTypes.STRING, //3
							StandardBasicTypes.FLOAT, //4
							StandardBasicTypes.FLOAT, //5
							StandardBasicTypes.LONG, //6
							StandardBasicTypes.STRING, //7
							StandardBasicTypes.STRING, //7
							StandardBasicTypes.INTEGER, //8	
							StandardBasicTypes.FLOAT,//9
							StandardBasicTypes.TIMESTAMP,//10
							StandardBasicTypes.INTEGER //11
					};
				}
			}
		} else {
			if (Boolean.TRUE.equals(isVNM) || Boolean.TRUE.equals(isMien) || Boolean.TRUE.equals(isVung)) {
				sql.append("SELECT C_ST.STAFF_ID AS STAFFID, C_ST.STAFF_CODE AS STAFFCODE, C_ST.STAFF_NAME AS STAFFNAME, c_st.STAFF_OWNER_ID as staffOwnerId, TB.LAT AS LAT, TB.LNG AS LNG, C_ST.SHOP_ID AS SHOPID, s.shop_code AS shopCode,s.shop_name AS shopName, ct.OBJECT_TYPE AS roleType, TB.ACCURACY AS accuracy, TB.CREATE_DATE AS createTime, (SELECT COUNT(*) FROM Staff s2 WHERE s2.staff_owner_id= c_ST.STAFF_ID) AS countStaff ");
				sql.append("FROM STAFF c_st ");
				sql.append("INNER JOIN CHANNEL_TYPE ct ");
				sql.append("ON C_ST.STAFF_TYPE_ID = CT.CHANNEL_TYPE_ID ");
				sql.append("inner join shop s on c_st.shop_id = s.shop_id ");
				sql.append("LEFT JOIN ");
				sql.append("(SELECT SPL.* FROM STAFF_POSITION_LOG SPL ");
				sql.append("WHERE spl.create_date        >= TRUNC(sysdate) ");
				sql.append("AND spl.STAFF_POSITION_LOG_ID = ");
				sql.append("(SELECT MAX(STAFF_POSITION_LOG_ID) ");
				sql.append("FROM staff_position_log sp2 ");
				sql.append("WHERE sp2.staff_id   = spl.staff_id ");
				sql.append("AND sp2.create_date >= TRUNC(sysdate) ");
				sql.append(") ");
				sql.append(") tb ");
				sql.append("ON c_st.STAFF_ID        =TB.STAFF_ID ");
				sql.append("WHERE ct.type           =2 ");
				sql.append("AND ct.status           =1 ");
				sql.append("AND C_ST.STATUS         = 1 ");
				sql.append("AND c_st.STAFF_OWNER_ID IN (select staff_id from staff where status = 1 and staff.shop_id in (select shop_id from shop  where status = 1 start with shop_id = ? connect by prior shop_id = parent_shop_id)) ");
				params.add(shopId);
				if (isVNM || isMien) {
					sql.append("UNION ");
					sql.append("SELECT gdm.STAFF_ID AS STAFFID, gdm.STAFF_CODE AS STAFFCODE, gdm.STAFF_NAME AS STAFFNAME, gdm.STAFF_OWNER_ID as staffOwnerId, NULL AS LAT, null AS LNG, gdm.SHOP_ID AS SHOPID, s.shop_code AS shopCode, s.shop_name AS shopName, ct.OBJECT_TYPE AS roleType, null AS accuracy, null AS createTime, (SELECT COUNT (*) FROM Staff s2 WHERE s2.staff_owner_id = gdm.STAFF_ID ) AS countStaff ");
					sql.append("FROM staff gdm inner join CHANNEL_TYPE ct on gdm.staff_type_id = ct.channel_type_id inner join shop s on gdm.shop_id = s.shop_id ");
					sql.append("where ct.status = 1 and ct.type = 2 and ct.object_type = ? and (s.parent_shop_id = ? or gdm.shop_id = ?) order by STAFFNAME");
					params.add(StaffObjectType.TBHM.getValue());
					params.add(shopId);
					params.add(shopId);
				} else if (isVung) {
					sql.append("UNION ");
					sql.append("SELECT tbhv.STAFF_ID AS STAFFID, tbhv.STAFF_CODE AS STAFFCODE, tbhv.STAFF_NAME AS STAFFNAME, tbhv.STAFF_OWNER_ID as staffOwnerId, NULL AS LAT, null AS LNG, tbhv.SHOP_ID AS SHOPID, s.shop_code AS shopCode, s.shop_name AS shopName, ct.OBJECT_TYPE AS roleType, null AS accuracy, null AS createTime, (SELECT COUNT (*) FROM Staff s2 WHERE s2.staff_owner_id = tbhv.STAFF_ID ) AS countStaff ");
					sql.append("FROM staff tbhv inner join CHANNEL_TYPE ct on tbhv.staff_type_id = ct.channel_type_id inner join shop s on tbhv.shop_id = s.shop_id ");
					sql.append("where ct.status = 1 and ct.type = 2 and ct.object_type = ? and tbhv.shop_id = ? order by STAFFNAME");
					params.add(StaffObjectType.TBHV.getValue());
					params.add(shopId);
				}

				fieldNames = new String[] { "staffId", //1
						"staffCode", //2
						"staffName", //3
						"staffOwnerId", "lat",//4
						"lng",//5
						"shopId",//6
						"shopCode",//7
						"shopName",//7
						"roleType", //8			
						"accuracy",//9
						"createTime",//10
						"countStaff"//11
				};

				fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
						StandardBasicTypes.STRING, //2
						StandardBasicTypes.STRING, //3
						StandardBasicTypes.LONG, StandardBasicTypes.FLOAT, //4
						StandardBasicTypes.FLOAT, //5
						StandardBasicTypes.LONG, //6
						StandardBasicTypes.STRING, //7
						StandardBasicTypes.STRING, //7
						StandardBasicTypes.INTEGER, //8	
						StandardBasicTypes.FLOAT,//9
						StandardBasicTypes.TIMESTAMP,//10
						StandardBasicTypes.INTEGER //11
				};
			} else if (Boolean.TRUE.equals(isNPP)) {
				sql.append("SELECT C_ST.STAFF_ID AS STAFFID, C_ST.STAFF_CODE AS STAFFCODE, C_ST.STAFF_NAME AS STAFFNAME, c_st.STAFF_OWNER_ID as staffOwnerId, TB.LAT AS LAT, TB.LNG AS LNG, C_ST.SHOP_ID AS SHOPID, s.shop_code AS shopCode, s.shop_name AS shopName, ct.OBJECT_TYPE AS roleType, TB.ACCURACY AS accuracy, TB.CREATE_DATE AS createTime, (SELECT COUNT(*) FROM Staff s2 WHERE s2.staff_owner_id= c_ST.STAFF_ID) AS countStaff ");
				sql.append("FROM STAFF c_st ");
				sql.append("INNER JOIN CHANNEL_TYPE ct ");
				sql.append("ON C_ST.STAFF_TYPE_ID = CT.CHANNEL_TYPE_ID ");
				sql.append("inner join shop s on c_st.shop_id = s.shop_id ");
				sql.append("LEFT JOIN ");
				sql.append("(SELECT SPL.* FROM STAFF_POSITION_LOG SPL ");
				sql.append("WHERE spl.create_date        >= TRUNC(sysdate) ");
				sql.append("AND spl.STAFF_POSITION_LOG_ID = ");
				sql.append("(SELECT MAX(STAFF_POSITION_LOG_ID) ");
				sql.append("FROM staff_position_log sp2 ");
				sql.append("WHERE sp2.staff_id   = spl.staff_id ");
				sql.append("AND sp2.create_date >= TRUNC(sysdate) ");
				sql.append(") ");
				sql.append(") tb ");
				sql.append("ON c_st.STAFF_ID        =TB.STAFF_ID ");
				sql.append("WHERE ct.type           =2 ");
				sql.append("AND ct.status           =1 ");
				sql.append("AND C_ST.STATUS         = 1 ");
				sql.append("AND c_st.STAFF_ID IN (SELECT STAFF_OWNER_ID FROM STAFF NVBH INNER JOIN CHANNEL_TYPE CT1 ON NVBH.STAFF_TYPE_ID = CT1.CHANNEL_TYPE_ID AND CT1.status = 1 and ct1.type = 2 and CT1.OBJECT_TYPE   IN (?, ?) WHERE NVBH.SHOP_ID    = ? UNION SELECT STAFF_ID FROM STAFF NVBH INNER JOIN channel_type ct1 ON nvbh.staff_type_id = ct1.channel_type_id and ct1.status = 1 and ct1.type = 2 AND CT1.OBJECT_TYPE   IN (?, ?) WHERE NVBH.SHOP_ID    = ?) ");
				sql.append("ORDER BY c_ST.STAFF_NAME ");

				params.add(StaffObjectType.NVBH.getValue());
				params.add(StaffObjectType.NVVS.getValue());
				params.add(shopId);

				params.add(StaffObjectType.NVBH.getValue());
				params.add(StaffObjectType.NVVS.getValue());
				params.add(shopId);

				fieldNames = new String[] { "staffId", //1
						"staffCode", //2
						"staffName", //3
						"staffOwnerId", "lat",//4
						"lng",//5
						"shopId",//6
						"shopCode",//7
						"shopName",//7
						"roleType", //8			
						"accuracy",//9
						"createTime",//10
						"countStaff"//11
				};

				fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
						StandardBasicTypes.STRING, //2
						StandardBasicTypes.STRING, //3
						StandardBasicTypes.LONG, StandardBasicTypes.FLOAT, //4
						StandardBasicTypes.FLOAT, //5
						StandardBasicTypes.LONG, //6
						StandardBasicTypes.STRING, //7
						StandardBasicTypes.STRING, //7
						StandardBasicTypes.INTEGER, //8	
						StandardBasicTypes.FLOAT,//9
						StandardBasicTypes.TIMESTAMP,//10
						StandardBasicTypes.INTEGER //11
				};
			}
		}

		return repo.getListByQueryAndScalar(StaffPositionVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<StaffPositionVO> getListStaffPosition(Long shopId, Long staffOwnerId, StaffObjectType roleType, Boolean oneNode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String[] fieldNames = null;
		Type[] fieldTypes = null;
		if (oneNode != null && oneNode == true) {
			if (StaffObjectType.NHVNM.equals(roleType) && shopId == null) {
				sql.append("SELECT S.STAFF_ID AS staffId, S.STAFF_CODE AS staffCode, S.STAFF_NAME AS staffName, CT.OBJECT_TYPE AS roleType, S.SHOP_ID shopId, SH.SHOP_CODE shopCode, ");
				sql.append("(SELECT COUNT (1) FROM staff WHERE status = 1 AND staff_owner_id = s.staff_id) countStaff ");
				sql.append("FROM STAFF S INNER JOIN CHANNEL_TYPE CT ON S.STAFF_TYPE_ID = CT.CHANNEL_TYPE_ID INNER JOIN SHOP SH ON S.SHOP_ID = SH.SHOP_ID and sh.status = ? ");
				params.add(ActiveType.RUNNING.getValue());
				sql.append("WHERE CT.OBJECT_TYPE = ? and CT.STATUS = ? and s.status=? and CT.TYPE = ? ");
				sql.append("ORDER BY s.STAFF_NAME");
				params.add(StaffObjectType.TBHM.getValue());
				params.add(ActiveType.RUNNING.getValue());
				params.add(ActiveType.RUNNING.getValue());
				params.add(ChannelTypeType.STAFF.getValue());
				fieldNames = new String[] { "staffId", "staffCode", "staffName", "roleType", "shopId", "shopCode", "countStaff" };

				fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
						StandardBasicTypes.STRING, //2
						StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER }; //3	

			} else {
				sql.append("select c_st.staff_id as staffid, c_st.staff_code as staffcode, c_st.staff_name as staffname, TB.LAT AS lat, TB.lng AS lng, c_ST.SHOP_ID AS shopId, ");
				sql.append("(SELECT C_SH.SHOP_CODE FROM SHOP c_sh WHERE C_st.SHOP_ID=C_SH.SHOP_ID and c_sh.status=1 ) as shopcode, ");
				sql.append("ct.OBJECT_TYPE AS roleType, ");
				sql.append("TB.ACCURACY    AS accuracy, ");
				sql.append("TB.CREATE_DATE AS createTime, ");
				sql.append("(SELECT COUNT(*) FROM Staff s2 WHERE s2.staff_owner_id= c_ST.STAFF_ID) as countstaff ");
				sql.append("FROM STAFF c_st inner join channel_type ct on c_st.staff_type_id = ct.channel_type_id and ct.status = ? and ct.type = ? ");
				params.add(ActiveType.RUNNING.getValue());
				params.add(ChannelTypeType.STAFF.getValue());
				sql.append(" INNER JOIN SHOP SHt ON SHt.SHOP_ID = c_st.SHOP_ID and SHt.status = 1 ");
				//begin fixing bug #10363
				sql.append(" inner join (select distinct g_d.staff_id from staff_group_detail g_d ");
				sql.append(" where not exists (select 1 ");
				sql.append(" from staff_group_detail ds inner join staff s on ds.staff_id = s.staff_id ");
				sql.append(" inner join shop ss on ss.shop_id=s.shop_id and ss.status<>1 ");
				sql.append(" where ds.staff_group_id=g_d.staff_group_id )) s_g_d on s_g_d.staff_id=c_st.staff_id ");
				//end
				sql.append("LEFT JOIN ");
				sql.append("(SELECT spl.* ");
				sql.append("FROM staff_position_log spl ");
				sql.append("where spl.create_date >= trunc(sysdate) ");
				sql.append("AND spl.staff_position_log_id = ");
				sql.append("(SELECT MAX(staff_position_log_id) ");
				sql.append("FROM staff_position_log sp2 ");
				sql.append("WHERE sp2.staff_id   = spl.staff_id ");
				sql.append("AND sp2.create_date >= TRUNC(sysdate) ");
				sql.append(") ");
				sql.append(") tb ");
				sql.append("ON c_st.STAFF_ID    =TB.STAFF_ID ");
				sql.append("where c_st.status   = ? ");
				params.add(ActiveType.RUNNING.getValue());
				if (shopId != null) {
					sql.append(" AND (ct.object_type  = ? or ct.object_type = ?) ");
					params.add(StaffObjectType.NVBH.getValue());
					params.add(StaffObjectType.NVVS.getValue());
					if (StaffObjectType.KTNPP.equals(roleType)) {
						sql.append("AND C_ST.SHOP_ID =? ");
						params.add(shopId);
					} else {
						sql.append("AND C_ST.SHOP_ID   IN ");
						sql.append("(SELECT W_SH.SHOP_ID ");
						sql.append("from shop w_sh ");
						sql.append("START WITH w_sh.shop_id       = ? ");
						params.add(shopId);
						sql.append("CONNECT BY prior w_sh.shop_id = w_sh.parent_shop_id and status = 1");
						sql.append(") ");
					}
				}
				if (staffOwnerId != null) {
					sql.append(" AND c_st.STAFF_OWNER_ID = ? ");
					params.add(staffOwnerId);
				}
				sql.append("ORDER BY c_ST.staff_name ");

				fieldNames = new String[] { "staffId", //1
						"staffCode", //2
						"staffName", //3
						"lat",//4
						"lng",//5
						"shopId",//6
						"shopCode",//7
						"roleType", "accuracy", "createTime", "countStaff" };//8

				fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
						StandardBasicTypes.STRING, //2
						StandardBasicTypes.STRING, //3
						StandardBasicTypes.FLOAT, //4
						StandardBasicTypes.FLOAT, //4
						StandardBasicTypes.LONG, //4
						StandardBasicTypes.STRING, //4
						StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.INTEGER };//5
			}
		} else {
			sql.append("with tmp as (select max(staff_position_log_id) staff_position_log_id, staff_id from staff_position_log where create_date >= trunc(sysdate) group by staff_id) ");
			sql.append("SELECT st.staff_id  AS staffId, st.staff_code as staffcode, st.staff_name AS staffName, st.shop_id AS shopId, st.staff_owner_id AS staffOwnerId, ");
			sql.append("(SELECT s.shop_code FROM shop s WHERE s.shop_id = st.shop_id) AS shopCode, ");
			sql.append("(select s.shop_name from shop s where s.shop_id = st.shop_id) as shopname, ");
			sql.append("ct.object_type AS roleType, spl.lat AS lat, spl.lng AS lng, spl.accuracy AS accuracy, spl.create_date as createtime ");
			sql.append("from staff st inner join channel_type ct on st.staff_type_id = ct.channel_type_id and ct.type = ? and ct.status = ? ");
			params.add(ChannelTypeType.STAFF.getValue());
			params.add(ActiveType.RUNNING.getValue());
			sql.append("left join (select * from staff_position_log spl WHERE spl.create_date >= TRUNC(sysdate) AND spl.staff_position_log_id IN ( SELECT staff_position_log_id FROM tmp)) spl ");
			sql.append("ON spl.staff_id   = st.staff_id ");
			sql.append("where st.status   = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("and st.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id and status = 1)  ");
			params.add(shopId);
			sql.append("order by st.staff_name");

			fieldNames = new String[] { "staffId", "staffCode", "staffName", "lat", "lng", "shopId", "shopName", "staffOwnerId", "shopCode", "roleType", "accuracy", "createTime" };

			fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.LONG,
					StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT, StandardBasicTypes.TIMESTAMP };//5
		}
		//System.out.print(TestUtils.addParamToQuery( sql.toString(), params.toArray()));
		return repo.getListByQueryAndScalar(StaffPositionVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	//old-function
	/*@Override
	public List<StaffPositionVO> getListStaffPosition(SupFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String[] fieldNames = null;
		Type[] fieldTypes = null;
		if (Boolean.TRUE.equals(filter.getOneNode())) {
			sql.append(" with lstShop as ( ");
			sql.append(" 	select s.* from shop s ");
			sql.append(" 	where s.status=1 start with s.shop_id = ? connect by prior shop_id=parent_shop_id ");
			params.add(filter.getShopId());
			sql.append(" ), ");
			sql.append(" lstStaff as ( ");
			if(StaffObjectType.KTNPP.getValue().equals(filter.getRoleType())){
				sql.append(" select s.* from staff s join  channel_type ct on ct.channel_type_id = s.staff_type_id ");
				sql.append(" where s.status=1 and ct.status=1 and ct.object_type in (1,2) ");
				sql.append(" and s.shop_id in (select shop_id from lstShop) ");
			}else{
				sql.append(" select s.* from staff s where s.status = 1 and s.staff_id in ");
				sql.append(" (select staff_id from parent_staff_map where status=1 and parent_staff_id=?) ");
				params.add(filter.getStaffId());
				sql.append(" and s.shop_id in (select shop_id from lstShop) ");
			}
			sql.append(" ), ");
			sql.append(" lstPos as ( ");
			sql.append(" select * from ( SELECT spl.*, ROW_NUMBER() OVER(PARTITION BY spl.staff_id ORDER BY spl.create_date DESC) RN ");
			sql.append(" FROM staff_position_log spl ");
			sql.append(" WHERE spl.create_date < trunc(sysdate)+1 ");
			sql.append(" and spl.create_date >= trunc(sysdate) ");
			sql.append(" ) where rn = 1 ");
			sql.append(" ) ");
			sql.append(" select s.staff_id as staffId, s.staff_code as staffCode, s.staff_name as staffName, ");
			sql.append(" p.LAT AS lat, p.lng AS lng, s.SHOP_ID AS shopId, ");
			sql.append(" (SELECT SHOP_CODE FROM SHOP WHERE s.SHOP_ID=shop_id and status=1 ) as shopcode, ");
			sql.append(" ct.OBJECT_TYPE AS roleType, ");
			sql.append(" p.ACCURACY    AS accuracy, ");
			sql.append(" p.CREATE_DATE AS createTime, ");
			sql.append(" (SELECT COUNT(*) FROM parent_staff_map psm WHERE s.staff_id=psm.parent_staff_id) as countstaff ");
			sql.append(" from lstStaff s left join lstPos p on s.staff_id=p.staff_id ");
			sql.append(" join channel_type ct on s.staff_type_id = ct.channel_type_id and ct.status = 1 and ct.type=? ");
			params.add(ChannelTypeType.STAFF.getValue());
			sql.append(" order by s.staff_code ");
			fieldNames = new String[] { "staffId", "staffCode", "staffName", "lat", "lng", "shopId", "shopCode", "roleType", "accuracy", "createTime", "countStaff" };

			fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
					StandardBasicTypes.FLOAT, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.INTEGER };
		} else {
			sql.append(" with lstShop as ( ");
			sql.append(" 	select s.* from shop s ");
			sql.append(" 	where s.status=1 start with s.shop_id = ? connect by prior shop_id=parent_shop_id ");
			params.add(filter.getShopId());
			sql.append(" ), ");
			sql.append(" lstStaff as ( ");
			if(StaffObjectType.KTNPP.getValue().equals(filter.getRoleType())){
				sql.append(" select s.* from staff s join  channel_type ct on ct.channel_type_id = s.staff_type_id ");
				sql.append(" where s.status=1 and ct.status=1 and ct.object_type in (1,2) ");
				sql.append(" and s.shop_id in (select shop_id from lstShop) ");
			}else{
				sql.append(" select s.* from staff s where s.status = 1 and s.staff_id in ");
				sql.append(" (select staff_id from parent_staff_map psm where psm.status = 1 start with parent_staff_id = ? ");
				params.add(filter.getStaffId());
				sql.append(" CONNECT BY prior psm.staff_id = psm.parent_staff_id ) ");
				sql.append(" and s.status = 1 and s.shop_id in (select shop_id from lstShop) ");
			}
			sql.append(" ), ");
			sql.append(" lstPos as ( ");
			sql.append(" select * from ( SELECT spl.*, ROW_NUMBER() OVER(PARTITION BY spl.staff_id ORDER BY spl.create_date DESC) RN ");
			sql.append(" FROM staff_position_log spl ");
			sql.append(" WHERE spl.create_date < trunc(sysdate)+1 ");
			sql.append(" and spl.create_date >= trunc(sysdate) ");
			sql.append(" ) where rn = 1 ");
			sql.append(" ) ");
			sql.append(" select s.staff_id as staffId, s.staff_code as staffCode, s.staff_name as staffName, ");
			sql.append(" p.LAT AS lat, p.lng AS lng, s.SHOP_ID AS shopId, ");
			sql.append(" (SELECT SHOP_CODE FROM SHOP WHERE s.SHOP_ID=shop_id and status=1 ) as shopCode, ");
			sql.append(" (SELECT SHOP_NAME FROM SHOP WHERE s.SHOP_ID=shop_id and status=1 ) as shopName, ");
			sql.append(" ct.OBJECT_TYPE AS roleType, ");
			sql.append(" p.ACCURACY    AS accuracy, ");
			sql.append(" p.CREATE_DATE AS createTime, ");
			sql.append(" (select listagg(parent_staff_id, ',') within GROUP (ORDER BY null) ");
			sql.append(" 		from parent_staff_map where staff_id=s.staff_id) lstStaffOwnerIdStr ");
			sql.append(" from lstStaff s left join lstPos p on s.staff_id=p.staff_id ");
			sql.append(" join channel_type ct on s.staff_type_id = ct.channel_type_id and ct.status = 1  and ct.type=? ");
			params.add(ChannelTypeType.STAFF.getValue());

			fieldNames = new String[] { 
					"staffId", "staffCode", "staffName", 
					"lat", "lng", "shopId", 
					"shopName", "shopCode", "roleType",
					"accuracy", "createTime", "lstStaffOwnerIdStr"
					};

			fieldTypes = new Type[] { 
					StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
					StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.LONG, 
					StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
					StandardBasicTypes.FLOAT, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.STRING };//5
		}
		//System.out.print(TestUtils.addParamToQuery( sql.toString(), params.toArray()));
		return repo.getListByQueryAndScalar(StaffPositionVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}*/
	
	
	//trungtm6 modify on 21/08/2015
	@Override
	public List<StaffPositionVO> getListStaffPosition(SupFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String[] fieldNames = null;
		Type[] fieldTypes = null;
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException(" shopId is null ");
		}

		if (filter.getChildShop() != null && filter.getChildShop()) {
			sql.append(" with lstShop as ( ");
			sql.append(" select * ");
			sql.append(" from shop sh ");
			sql.append(" start with shop_id = ? and status = ? ");
			params.add(filter.getShopId());
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" connect by prior shop_id = parent_shop_id and status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" ) ");

			sql.append(" ,lstStaff as ( ");
			sql.append(" select st.staff_id, st.staff_code, st.staff_name, sty.SPECIFIC_TYPE, st.shop_id,   ");

			sql.append(" ( (SELECT LISTAGG(parent_shop_id, ',') WITHIN GROUP ( ");
			sql.append(" ORDER BY level DESC) \"lstParentId\" ");
			sql.append(" FROM shop ");
			sql.append(" START WITH shop_id = st.shop_id ");
			sql.append(" CONNECT BY prior parent_shop_id = shop_id) || ',' || to_char(st.shop_id) ) lstParentShopId  ");
			sql.append(" , sty.icon_url ");
			sql.append(" , st.status ");
			sql.append(" from staff st ");
			if (StringUtility.isNullOrEmpty(filter.getStrStaffId()) && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
				sql.append(" join table (F_GET_LIST_CHILD_STAFF_INHERIT(?, sysdate, ?, ?, null)) prs on st.staff_id = prs.number_key_id ");
				params.add(filter.getStaffRootId());
				params.add(filter.getRoleId());
				params.add(filter.getShopRootId());
			}
			
			sql.append(" join staff_type sty on st.staff_type_id = sty.staff_type_id and sty.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" where st.shop_id IN ( select shop_id from lstShop ) ");
			sql.append(" and st.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			
			if (!StringUtility.isNullOrEmpty(filter.getStrStaffId())) {
				String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrStaffId(), "st.staff_id");
				sql.append(paramsFix);
			}
			
			if (!StringUtility.isNullOrEmpty(filter.getStrStaffTypeId())) {
				sql.append(" and sty.staff_type_id IN ( ");
				sql.append(filter.getStrStaffTypeId());
				sql.append(" ) ");
			}

			if (!StringUtility.isNullOrEmpty(filter.getStrSpecType())) {
				sql.append(" and sty.SPECIFIC_TYPE IN ( ");
				sql.append(filter.getStrSpecType());
				sql.append(" ) ");
			}

			sql.append(" ) ");
		} else {
			sql.append(" with lstStaff as ( ");
			sql.append(" select st.staff_id, st.staff_code, st.staff_name, sty.SPECIFIC_TYPE, st.shop_id, '' lstParentShopId, sty.icon_url ");
			sql.append(" , st.status ");
			sql.append(" from staff st ");
			sql.append(" join table (F_GET_LIST_CHILD_STAFF_INHERIT(?, sysdate, ?, ?, null)) prs on st.staff_id = prs.number_key_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
			sql.append(" join staff_type sty on st.staff_type_id = sty.staff_type_id and sty.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" where st.shop_id = ? ");
			params.add(filter.getShopId());
			sql.append(" and st.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			if (!StringUtility.isNullOrEmpty(filter.getStrStaffTypeId())) {
				sql.append(" and sty.staff_type_id IN ( ");
				sql.append(filter.getStrStaffTypeId());
				sql.append(" ) ");
			}
			if (!StringUtility.isNullOrEmpty(filter.getStrSpecType())) {
				sql.append(" and sty.SPECIFIC_TYPE IN ( ");
				sql.append(filter.getStrSpecType());
				sql.append(" ) ");
			}
			sql.append(" ) ");
		}
		sql.append(" ,lstPos as ( ");
		sql.append(" select * from ( SELECT spl.*, ROW_NUMBER() OVER(PARTITION BY spl.staff_id ORDER BY spl.create_date DESC) RN ");
		sql.append(" FROM staff_position_log spl ");
		sql.append(" WHERE spl.create_date < trunc(sysdate)+1 ");
		sql.append(" and spl.create_date >= trunc(sysdate) ");
		sql.append(" ) where rn = 1 ");
		sql.append(" ) ");
		sql.append(" select s.staff_id as staffId, s.staff_code as staffCode, s.staff_name as staffName, ");
		sql.append(" p.LAT AS lat, p.lng AS lng, s.SHOP_ID AS shopId, ");
		sql.append(" (SELECT SHOP_CODE FROM SHOP WHERE s.SHOP_ID=shop_id and status=1 ) as shopcode, ");
		sql.append(" (SELECT SHOP_NAME FROM SHOP WHERE s.SHOP_ID=shop_id and status=1 ) as shopName, ");
		sql.append(" s.SPECIFIC_TYPE AS roleType, ");
		sql.append(" p.ACCURACY    AS accuracy, ");
		sql.append(" p.CREATE_DATE AS createTime, ");
		sql.append(" 0 as countstaff, ");
		sql.append(" to_char(p.create_date, 'HH24:Mi:SS') hhmm, ");
		sql.append(" (s.staff_code || ' - ' || s.staff_name) text, ");
		sql.append(" s.lstParentShopId ");
		sql.append(" ,s.icon_url iconUrl ");
		sql.append(" ,s.status as status ");
		sql.append(" from lstStaff s left join lstPos p on s.staff_id=p.staff_id ");
		//sql.append(" order by s.staff_id ");
		sql.append(" order by s.SPECIFIC_TYPE DESC, s.staff_code ");
		fieldNames = new String[] { 
				"staffId" //1
				, "staffCode" //2
				, "staffName" //3
				, "lat" //4
				, "lng" //5
				, "shopId" //6
				, "shopCode" //7
				, "shopName" //8
				, "roleType" //9
				, "accuracy" //10
				, "createTime" //11
				, "countStaff" //12
				, "hhmm" //13
				, "text" //14
				, "lstParentShopId" //15
				, "iconUrl" //16
				, "status" //17
		};
		fieldTypes = new Type[] { 
				StandardBasicTypes.LONG //1
				, StandardBasicTypes.STRING //2
				, StandardBasicTypes.STRING //3
				, StandardBasicTypes.FLOAT //4
				, StandardBasicTypes.FLOAT //5
				, StandardBasicTypes.LONG //6
				, StandardBasicTypes.STRING //7
				, StandardBasicTypes.STRING //8
				, StandardBasicTypes.INTEGER //9
				, StandardBasicTypes.FLOAT //10
				, StandardBasicTypes.TIMESTAMP //11
				, StandardBasicTypes.INTEGER //12
				, StandardBasicTypes.STRING //13
				, StandardBasicTypes.STRING //14
				, StandardBasicTypes.STRING //15
				, StandardBasicTypes.STRING //16
				, StandardBasicTypes.INTEGER //17
		};
		return repo.getListByQueryAndScalar(StaffPositionVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<ParentStaffMapVO> getListParentStaffMapVO(SupFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShop as ( ");
		sql.append(" 	select s.* from shop s ");
		sql.append(" 	where s.status=1 start with s.shop_id = ? connect by prior shop_id=parent_shop_id ");
		params.add(filter.getShopId());
		sql.append(" ), ");
		sql.append(" lstStaff as ( ");
		sql.append(" select s.* from staff s where s.status = 1 and s.staff_id in ");
		sql.append(" (select staff_id from parent_staff_map psm start with parent_staff_id = ? ");
		params.add(filter.getStaffId());
		sql.append(" CONNECT BY prior psm.staff_id = psm.parent_staff_id and status = 1) ");
		sql.append(" and s.shop_id in (select shop_id from lstShop) ");
		sql.append(" ) ");
		sql.append(" select (psm.staff_id||'-'||psm.parent_staff_id) parentStaffStr from parent_staff_map psm ");
		sql.append(" where psm.status=1 ");
		sql.append(" and psm.from_date<trunc(?)+1 ");
		sql.append(" and (psm.to_date is null or psm.to_date>=trunc(?)) ");
		params.add(filter.getDateTime());
		params.add(filter.getDateTime());
		sql.append(" and psm.staff_id in (select staff_id from lstStaff) ");

		String[] fieldNames = new String[] { 
					"parentStaffStr"
			};

		Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.STRING 
			};

		return repo.getListByQueryAndScalar(ParentStaffMapVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public StaffPositionVO getStaffPosition(Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();

		sql.append("SELECT st.staff_id AS staffId, st.staff_code AS staffcode, st.staff_name AS staffName, st.shop_id AS shopId, SH.SHOP_CODE AS shopCode, SH.SHOP_NAME AS shopname, ct.object_type AS roleType, spl.lat AS lat, spl.lng AS lng, spl.accuracy AS accuracy, spl.create_date AS createtime ");
		sql.append("FROM staff st inner join shop sh on ST.shop_id = SH.shop_id and SH.STATUS = ?  ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("INNER JOIN channel_type ct ON st.staff_type_id = ct.channel_type_id ");
		sql.append("AND ct. TYPE = ? ");
		params.add(2);
		sql.append("AND ct.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("LEFT JOIN (SELECT * FROM staff_position_log spl WHERE spl.create_date >= TRUNC (SYSDATE) AND spl.staff_position_log_id IN  ");
		sql.append("(SELECT MAX (staff_position_log_id) staff_position_log_id FROM staff_position_log WHERE create_date >= TRUNC (SYSDATE) and staff_id = ?)) ");
		params.add(staffId);
		sql.append("spl ON spl.staff_id = st.staff_id ");
		sql.append("WHERE st.status = ? AND st.staff_id = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(staffId);

		String[] fieldNames = new String[] { "staffId", "staffCode", "staffName", "lat", "lng", "shopId", "shopName", "shopCode", "roleType", "accuracy", "createTime" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.LONG, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT, StandardBasicTypes.TIMESTAMP };//5

		List<StaffPositionVO> result = repo.getListByQueryAndScalar(StaffPositionVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (result != null && result.size() > 0) {
			return result.get(0);
		} else {
			return null;
		}
	}

	@Override
	public AmountPlanStaffVO getAmountPlanOfStaff(StaffObjectType type, Staff staff,Long shopId) throws DataAccessException {
		if (type.equals(StaffObjectType.NVGS)) {
			return this.getAmountPlanOfStaffEx(staff,shopId);
		}
		return this.getAmountPlanOfStaffEx1(staff,shopId);
	}

	//for gdm
	public AmountPlanStaffVO getAmountPlanOfStaffEx1(Staff staff,Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShop as ( ");
		sql.append(" select s.* from shop s ");
		sql.append(" where s.status=1 start with s.shop_id = ? connect by prior shop_id=parent_shop_id ");
		params.add(shopId);
		sql.append(" ), ");
		sql.append(" lstStaff as ( ");
		sql.append(" select s.* from staff s ");
		sql.append(" where s.staff_id  in ( ");
		sql.append(" select psm.staff_id as staffId ");
		sql.append(" from parent_staff_map psm ");
		sql.append(" where psm.status = 1 ");
		sql.append(" and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id) ");
		sql.append(" start with psm.parent_staff_id = ? AND PSM.STATUS = 1 connect by prior psm.staff_id = psm.parent_staff_id ");
		params.add(staff.getId());
		sql.append(" ) and s.shop_id in (select shop_id from lstShop)), ");
		sql.append(" lstStaffOneNode as ( ");
		sql.append(" select s.* from lstStaff s where s.staff_id in ");
		sql.append(" (select staff_id from parent_staff_map where parent_staff_id=?) ");
		params.add(staff.getId());
		sql.append(" ), ");
		sql.append(" lstRootNV as ( ");
		sql.append(" SELECT DISTINCT CONNECT_BY_ROOT(PSMP.PARENT_STAFF_ID) AS ROOT_ID, ");
		sql.append(" PSMP.STAFF_ID FROM PARENT_STAFF_MAP PSMP ");
		sql.append(" JOIN lstStaff SF ON PSMP.STAFF_ID = SF.STAFF_ID AND SF.STATUS = 1 ");
		sql.append(" WHERE PSMP.STATUS = 1 AND STAFF_TYPE_ID IN ");
		sql.append(" (SELECT CHANNEL_TYPE_ID FROM CHANNEL_TYPE WHERE TYPE = 2 AND OBJECT_TYPE IN (1, 2) AND STATUS = 1) ");
		sql.append(" START WITH PSMP.PARENT_STAFF_ID IN (select staff_id from lstStaffOneNode) ");
		sql.append(" AND PSMP.STATUS = 1 ");
		sql.append(" CONNECT BY PRIOR PSMP.STAFF_ID = PSMP.PARENT_STAFF_ID ");
		sql.append(" ), ");
		sql.append(" lstSaleNV as ( ");
		sql.append(" select distinct rpt.staff_id,rpt.shop_id,rpt.month_amount_plan,rpt.month_amount,rpt.day_amount_plan,rpt.day_amount, ");
		sql.append(" rpt.day_cust,rpt.day_cust_order_plan,rpt.day_cust_order,rpt.create_date,rpt.DAY_APPROVED_AMOUNT,rpt.MONTH_APPROVED_AMOUNT ");
		sql.append(" from lstStaff s ");
		sql.append(" join channel_type ct on ct.channel_type_id=s.staff_type_id and ct.object_type in (1,2) ");
		sql.append(" join rpt_staff_sale rpt on rpt.staff_id = s.staff_id ");
		sql.append(" where 1=1 ");
		sql.append(" AND rpt.CREATE_DATE >= TRUNC(SYSDATE,'MM') ");
		sql.append(" AND rpt.CREATE_DATE < add_months(TRUNC(SYSDATE,'mm'),1) ");
		sql.append(" ), ");
		sql.append(" lstResult as ( ");
		sql.append(" select s.staff_id staffId, s.staff_code staffCode, s.staff_name staffName,s.shop_id shopId, ");
		sql.append(" (select shop_code from shop where shop_id=s.shop_id) shopCode, ");
		sql.append(" (select shop_name from shop where shop_id=s.shop_id) shopName, ");
		sql.append(" (select  NVL(round(sum(rpt.month_amount)/1000), 0) ");
		sql.append(" FROM lstSaleNV rpt ");
		sql.append(" join lstRootNV root on rpt.staff_id=root.staff_id ");
		sql.append(" WHERE 1=1 ");
		sql.append(" and s.staff_id=root.root_id ");
		sql.append(" ) monthAmount, ");
		sql.append(" (select  NVL(round(sum(rpt.month_amount_plan)/1000), 0) ");
		sql.append(" FROM lstSaleNV rpt ");
		sql.append(" join lstRootNV root on rpt.staff_id=root.staff_id ");
		sql.append(" WHERE 1=1 ");
		sql.append(" and s.staff_id=root.root_id ");
		sql.append(" ) monthAmountPlan, ");
		sql.append(" (select  NVL(round(sum(rpt.MONTH_APPROVED_AMOUNT)/1000), 0) ");
		sql.append(" FROM lstSaleNV rpt ");
		sql.append(" join lstRootNV root on rpt.staff_id=root.staff_id ");
		sql.append(" WHERE 1=1 ");
		sql.append(" and s.staff_id=root.root_id ");
		sql.append(" ) monthApprovedAmount ");
		sql.append(" from lstStaffOneNode s ");
		sql.append(" ) ");
		sql.append(" select staffId, staffCode,staffName, ");
		sql.append(" shopId, shopCode, shopName, ");
		sql.append(" monthAmount,monthAmountPlan,monthApprovedAmount, ");
		sql.append(" (monthAmountPlan-monthAmount) exist, ");
		sql.append(" (case monthamountplan when 0 then ");
		sql.append(" case monthamount when 0 then 0 else 100 end ");
		sql.append(" else NVL(floor((monthamount  / monthamountplan) * 100), 0) end) AS progress ");
		sql.append(" from lstResult ");
		sql.append(" order by staffCode ");
		//		sql.append("select s.shop_id                                                       as shopId,");
		//		sql.append("s.shop_code                                                          as shopCode, ");
		//		sql.append("s.shop_name                                                          as shopName, ");
		//		sql.append("tbhv.staff_id                                                           as staffId, ");
		//		sql.append("tbhv.staff_code                                                         as staffCode, ");
		//		sql.append("tbhv.staff_name                                                         as staffName, ");
		//		sql.append("NVL(round(sum(nvgs.monthamount)        /1000), 0)                              as monthAmount, ");
		//		sql.append("NVL(round(sum(nvgs.monthamountplan)    /1000), 0)                              as monthAmountPlan, ");
		//		sql.append("NVL(ROUND(SUM(nvgs.monthApprovedAmount) / 1000), 0) AS monthApprovedAmount, ");
		//		sql.append("NVL(round(sum(nvgs.exist)              /1000), 0)                              as exist, ");
		//		//sql.append("nvl(floor ((sum (nvgs.monthamount) / sum (nvgs.monthamountplan)) * 100), 0) as progress ");
		//		sql.append("  (case SUM(nvgs.monthamountplan) when 0 then  ");
		//		sql.append(" case SUM(nvgs.monthamount) when 0 then 0 else 100 end ");
		//		sql.append(" else NVL(floor ((SUM (nvgs.monthamount)  / SUM (nvgs.monthamountplan)) * 100), 0) end) AS progress ");
		//		sql.append("from staff tbhv inner join shop s on tbhv.shop_id = s.shop_id left join (SELECT sh.shop_id AS shopId, ");
		//		sql.append("sh.shop_code     AS shopCode, ");
		//		sql.append("sh.shop_name     AS shopName, ");
		//		sql.append("st.staff_id      as staffid, ");
		//		sql.append("st.staff_owner_id as staffOwnerId, ");
		//		sql.append("st.staff_code    AS staffCode, ");
		//		sql.append("st.staff_name    AS staffName, ");
		//		sql.append("NVL(rpt.month_amount, 0) monthAmount, ");
		//		sql.append("NVL (rpt.month_amount_plan, 0)                            AS monthAmountPlan, ");
		//		sql.append(" NVL (rpt.MONTH_APPROVED_AMOUNT,0) monthApprovedAmount, ");
		//		sql.append("NVL (rpt.month_amount_plan, 0) - NVL(rpt.month_amount, 0) AS exist ");
		//		sql.append("FROM rpt_staff_sale rpt, ");
		//		sql.append("staff nvbh, ");
		//		sql.append("staff st, ");
		//		sql.append("shop sh ");
		//		sql.append("WHERE rpt.staff_id                = nvbh.staff_id ");
		//		sql.append("AND sh.shop_id                    = nvbh.shop_id ");
		//		sql.append("AND nvbh.staff_owner_id           = st.staff_id ");
		//		sql.append("and trunc (rpt.create_date, 'mm') = trunc (sysdate, 'mm') ");
		//		sql.append("  and st.staff_owner_id             IN (select staff_id from staff where staff_owner_id = ?) ");
		//		params.add(staff.getId());
		//		sql.append(") nvgs on nvgs.staffOwnerId = tbhv.staff_id ");
		//		sql.append(" where TBHV.STAFF_OWNER_ID = ? ");
		//		params.add(staff.getId());
		//		sql.append("group by s.shop_id, s.shop_code, s.shop_name, tbhv.staff_id, tbhv.staff_code, tbhv.staff_name ");

		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "staffId", "staffCode", "staffName", "monthAmount", "monthAmountPlan", "monthApprovedAmount", "exist", "progress" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER };
		AmountPlanStaffVO vo = new AmountPlanStaffVO();
		List<AmountPlanStaffDetailVO> detail = repo.getListByQueryAndScalar(AmountPlanStaffDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);

		if (detail != null && detail.size() > 0) {
			for (AmountPlanStaffDetailVO child : detail) {
				if (child.getExist().compareTo(BigDecimal.ZERO) < 0) {
					child.setExist(BigDecimal.ZERO);
				}

				if (child.getMonthAmount() != null && child.getMonthAmountPlan() != null) {
					if (child.getMonthAmountPlan().compareTo(BigDecimal.ZERO) == 0) {
						if (child.getMonthAmount().compareTo(BigDecimal.ZERO) == 0) {
							child.setProgress(0);
						} else {
							child.setProgress(100);
						}
					}
				}
			}
		}

		vo.setDsBHKH(saleDayDAO.getSalesMonthByYear());
		vo.setSnBHDQ(exceptionDayDAO.getExceptionDayForSupervise());
		if (vo.getDsBHKH() == null || vo.getDsBHKH() == 0) {
			vo.setTienDoChuan(0);
		} else {
			vo.setTienDoChuan((Integer) (vo.getSnBHDQ() * 100 / vo.getDsBHKH()));
		}

		vo.setDetail(detail);
		return vo;
	}

	//for GSNPP
	public AmountPlanStaffVO getAmountPlanOfStaffEx(Staff staff,Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShop as ( ");
		sql.append(" select s.* from shop s ");
		sql.append(" where s.status=1 start with s.shop_id = ? connect by prior shop_id=parent_shop_id ");
		params.add(shopId);
		sql.append(" ), ");
		sql.append(" lstStaff as ( ");
		sql.append(" select s.* from staff s ");
		sql.append(" where s.staff_id  in ( ");
		sql.append(" select psm.staff_id as staffId ");
		sql.append(" from parent_staff_map psm ");
		sql.append(" where psm.status = 1 ");
		sql.append(" and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id) ");
		sql.append(" start with psm.parent_staff_id = ? AND PSM.STATUS = 1 connect by prior psm.staff_id = psm.parent_staff_id ");
		params.add(staff.getId());
		sql.append(" ) and s.shop_id in (select shop_id from lstShop) ), ");
		sql.append(" lstStaffOneNode as ( ");
		sql.append(" select s.* from lstStaff s where s.staff_id in ");
		sql.append(" (select staff_id from parent_staff_map where parent_staff_id=?) ");
		params.add(staff.getId());
		sql.append(" ), ");
		sql.append(" lstSaleNV as ( ");
		sql.append(" select distinct rpt.staff_id,rpt.shop_id,rpt.month_amount_plan,rpt.month_amount,rpt.day_amount_plan,rpt.day_amount, ");
		sql.append(" rpt.day_cust,rpt.day_cust_order_plan,rpt.day_cust_order,rpt.create_date,rpt.DAY_APPROVED_AMOUNT,rpt.MONTH_APPROVED_AMOUNT ");
		sql.append(" from lstStaff s ");
		sql.append(" join channel_type ct on ct.channel_type_id=s.staff_type_id and ct.object_type in (1,2) ");
		sql.append(" join rpt_staff_sale rpt on rpt.staff_id = s.staff_id ");
		sql.append(" where 1=1 ");
		sql.append(" AND rpt.CREATE_DATE >= TRUNC(SYSDATE,'MM') ");
		sql.append(" AND rpt.CREATE_DATE < add_months(TRUNC(SYSDATE,'mm'),1) ");
		sql.append(" ) ");
		sql.append(" select rpt.shop_id AS shopId, ");
		sql.append(" s.shop_code     AS shopCode, ");
		sql.append(" s.shop_name     AS shopName, ");
		sql.append(" st.staff_id     AS staffId, ");
		sql.append(" st.staff_code   AS staffCode, ");
		sql.append(" st.staff_name         AS staffName, ");
		sql.append(" ROUND(NVL(rpt.month_amount_plan,0)/1000,0) AS monthAmountPlan, ");
		sql.append(" ROUND(NVL(rpt.month_amount,0)     /1000,0) AS monthAmount, ");
		sql.append(" ROUND(NVL(rpt.MONTH_APPROVED_AMOUNT,0)/1000,0) monthApprovedAmount, ");
		sql.append(" (CASE NVL(rpt.month_amount_plan,0) WHEN 0 THEN  ");
		sql.append(" case NVL(rpt.month_amount,0) when 0 then 0 else 100 end ");
		sql.append(" ELSE floor((NVL(rpt.month_amount,0)/NVL(rpt.month_amount_plan,0))*100) ");
		sql.append(" END)                                                     AS progress, ");
		sql.append(" ROUND((NVL(rpt.month_amount_plan,0) - NVL(rpt.month_amount,0))/1000,0) AS exist ");
		sql.append(" from lstStaffOneNode st ");
		sql.append(" inner JOIN shop s ON (s.shop_id = st.shop_id) ");
		sql.append(" left join lstSaleNV rpt on rpt.staff_id=st.staff_id ");
		sql.append(" order by shopCode,staffCode ");
		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "staffId", "staffCode", "staffName", "monthAmountPlan", "monthAmount", "monthApprovedAmount", "progress", "exist" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL };
		AmountPlanStaffVO vo = new AmountPlanStaffVO();
		List<AmountPlanStaffDetailVO> detail = repo.getListByQueryAndScalar(AmountPlanStaffDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);

		if (detail != null && detail.size() > 0) {
			for (AmountPlanStaffDetailVO child : detail) {
				if (child.getExist().compareTo(BigDecimal.ZERO) < 0) {
					child.setExist(BigDecimal.ZERO);
				}
				if (child.getMonthAmount() != null && child.getMonthAmountPlan() != null) {
					if (child.getMonthAmountPlan().compareTo(BigDecimal.ZERO) == 0) {
						if (child.getMonthAmount().compareTo(BigDecimal.ZERO) == 0) {
							child.setProgress(0);
						} else {
							child.setProgress(100);
						}
					}
				}
			}
		}

		vo.setDsBHKH(saleDayDAO.getSalesMonthByYear());
		vo.setSnBHDQ(exceptionDayDAO.getExceptionDayForSupervise());
		if (vo.getDsBHKH() == null || vo.getDsBHKH() == 0) {
			vo.setTienDoChuan(0);
		} else {
			vo.setTienDoChuan((Integer) (vo.getSnBHDQ() * 100 / vo.getDsBHKH()));
		}

		vo.setDetail(detail);
		return vo;
	}

	private Integer countNumNVGSInVung(Long nvgsId) throws DataAccessException {
		int c = 0;
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(*) count from staff_group_detail d where staff_id = ? and status = 1 and exists (select 1 from staff_group where staff_group_id = d.staff_group_id and status = 1)");
		params.add(nvgsId);
		c = repo.countBySQL(sql.toString(), params);
		return c;
	}

	private StaffGroupDetail getSGDByStaff(Long staffId) throws DataAccessException {
		String sql = "select * from staff_group_detail sgd where staff_id = ? and status = 1 and exists (select 1 from staff_group where status = 1 and staff_group_id = sgd.staff_group_id)";
		List<Object> params = new ArrayList<Object>();
		params.add(staffId);
		return repo.getFirstBySQL(StaffGroupDetail.class, sql, params);

	}

	@Override
	public Staff getStaffOwnerOfShop(Long shopId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shop is null");
		}
		String sql = "select st.* from STAFF_GROUP_DETAIL sgd inner join staff st on ST.staff_id = SGD.STAFF_ID where st.status = ? and sgd.status = 1 and SGD.STAFF_GROUP_ID in (Select STAFF_GROUP_ID from staff_group where shop_id = ? and status = ?)";
		List<Object> params = new ArrayList<Object>();
		params.add(ActiveType.RUNNING.getValue());
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(Staff.class, sql, params);
	}

	@Override
	public List<StaffPositionLog> getListStaffPosition2(Long userId, Boolean getTbhv, Boolean getGsnpp, Boolean getNvbh) throws DataAccessException {
		/*StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		Staff staff = this.getStaffById(userId);

		sql.append(" WITH listStaff AS ");
		sql.append("   (SELECT staff_id ");
		sql.append("   FROM staff ");
		sql.append("   WHERE status = 1 ");
		sql.append("   AND shop_id IN ");
		sql.append("     (SELECT shop_id ");
		sql.append("     FROM shop ");
		sql.append("     WHERE status               = 1 ");
		sql.append("       START WITH shop_id       = ? ");
		params.add(staff.getShop().getId());
		sql.append("       CONNECT BY prior shop_id = parent_shop_id ");
		sql.append("     ) ");
		sql.append("   ) ");
		sql.append(" SELECT * FROM staff_position_log s WHERE staff_id IN ");
		sql.append("   (SELECT * FROM listStaff) ");
		sql.append(" AND CREATE_DATE = (SELECT MAX(CREATE_DATE) FROM STAFF_POSITION_LOG WHERE STAFF_ID = S.STAFF_ID) ");

		switch (staff.getStaffType().getObjectType()) {
		case 6:
			//giam doc mien
			if (Boolean.TRUE.equals(getTbhv) && !Boolean.TRUE.equals(getGsnpp) && !Boolean.TRUE.equals(getNvbh)) {
				//load tat ca vi tri truong ban hang vung cua tat ca npp
				sql.append(" and exists (select 1 from channel_type where channel_type_id = (select staff_type_id from staff where staff_id = s.staff_id) and object_type = 7 )");
			} else if (Boolean.TRUE.equals(getTbhv) && Boolean.TRUE.equals(getGsnpp) && !Boolean.TRUE.equals(getNvbh)) {
				//load tat ca vi tri truong ban hang vung, gs npp cua tat ca npp
				sql.append(" and exists (select 1 from channel_type where channel_type_id = (select staff_type_id from staff where staff_id = s.staff_id) and object_type in (7,5) )");
			}
			break;
		case 7:
			//truong ban hang vung
			//load tat ca vi tri gs npp
			sql.append(" and exists (select 1 from channel_type where channel_type_id = (select staff_type_id from staff where staff_id = s.staff_id) and object_type = 5 )");
			break;
		case 5:
			//giam sat nha phan phoi
			//load tat ca vi tri nvbh
			sql.append(" and exists (select 1 from channel_type where channel_type_id = (select staff_type_id from staff where staff_id = s.staff_id) and object_type in (1,2) )");
			break;
		}
		return repo.getListBySQL(StaffPositionLog.class, sql.toString(), params);*/
		return null;
	}

	@Override
	public StaffPositionLog getLatLngOfStaff(Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder(
				"SELECT spl.* FROM staff_position_log spl WHERE spl.create_date >= TRUNC (SYSDATE) AND spl.STAFF_POSITION_LOG_ID = (SELECT MAX (STAFF_POSITION_LOG_ID) FROM staff_position_log sp2 WHERE sp2.staff_id = spl.staff_id AND sp2.create_date >= TRUNC (SYSDATE)) AND STAFF_ID = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(staffId);
		return repo.getEntityBySQL(StaffPositionLog.class, sql.toString(), params);
	}

	@Override
	public TreeVO<StaffPositionVO> searchStaff(SupFilter filterSearch) throws DataAccessException {
		/*TreeVO<StaffPositionVO> res = new TreeVO<StaffPositionVO>();
		res.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
		Long gdmId = 0l;
		Long tbhvId = 0l;
		Long gsId = 0l;
		Long nvbhId = 0l;
		Long boldId = 0l;
		SupFilter filter = new SupFilter();
		filter.setOneNode(true);
		filter.setStaffId(filterSearch.getLoginStaff().getId());
		filter.setShopId(filterSearch.getShopId());
		List<Staff> lstStaff = getListStaffAncestor(filterSearch);
		if (lstStaff != null && lstStaff.size() > 0) {
			for (Staff staff : lstStaff) {
				if (gdmId.equals(0l) && StaffObjectType.TBHM.getValue().equals(staff.getStaffType().getObjectType())) {
					gdmId = staff.getId();
					if (tbhvId.equals(0l) && gsId.equals(0l) && nvbhId.equals(0l)) {
						boldId = gdmId;
					}
				} else if (tbhvId.equals(0l) && StaffObjectType.TBHV.getValue().equals(staff.getStaffType().getObjectType())) {
					tbhvId = staff.getId();
					if (gsId.equals(0l) && nvbhId.equals(0l)) {
						boldId = tbhvId;
					}
				} else if (gsId.equals(0l) && StaffObjectType.NVGS.getValue().equals(staff.getStaffType().getObjectType())) {
					gsId = staff.getId();
					if (nvbhId.equals(0l)) {
						boldId = gsId;
					}
				} else if (nvbhId.equals(0l) && (StaffObjectType.NVBH.getValue().equals(staff.getStaffType().getObjectType()) || StaffObjectType.NVVS.getValue().equals(staff.getStaffType().getObjectType()))) {
					nvbhId = staff.getId();
					boldId = nvbhId;
				}
			}
			if (StaffObjectType.NHVNM.getValue().equals(filterSearch.getLoginStaff().getStaffType().getObjectType())) {
				List<StaffPositionVO> lstGDM = getListStaffPosition(filter);
				for (StaffPositionVO child : lstGDM) {
					child.setStaffOwnerId(filter.getStaffId());
					TreeVO<StaffPositionVO> gdm = new TreeVO<StaffPositionVO>();
					gdm.setObject(child);
					if (gdmId.equals(child.getStaffId())) {
						if (gdmId.equals(boldId)) {
							child.setIsBold(true);
						}
						filter.setStaffId(gdmId);
						List<StaffPositionVO> lstTBHV = getListStaffPosition(filter);
						gdm.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
						for (StaffPositionVO tv : lstTBHV) {
							tv.setStaffOwnerId(filter.getStaffId());
							TreeVO<StaffPositionVO> tbhv = new TreeVO<StaffPositionVO>();
							tbhv.setObject(tv);
							gdm.getListChildren().add(tbhv);
							if (tbhvId.equals(tv.getStaffId())) {
								if (tbhvId.equals(boldId)) {
									tv.setIsBold(true);
								}
								filter.setStaffId(tbhvId);
								List<StaffPositionVO> lstGS = getListStaffPosition(filter);
								tbhv.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
								for (StaffPositionVO gs : lstGS) {
									gs.setStaffOwnerId(filter.getStaffId());
									TreeVO<StaffPositionVO> gsvo = new TreeVO<StaffPositionVO>();
									gsvo.setObject(gs);
									tbhv.getListChildren().add(gsvo);
									if (gsId.equals(gs.getStaffId())) {
										if (gsId.equals(boldId)) {
											gs.setIsBold(true);
										}
										filter.setStaffId(gsId);
										List<StaffPositionVO> lstNV = getListStaffPosition(filter);
										gsvo.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
										for (StaffPositionVO nv : lstNV) {
											nv.setStaffOwnerId(filter.getStaffId());
											TreeVO<StaffPositionVO> nvbh = new TreeVO<StaffPositionVO>();
											nvbh.setObject(nv);
											gsvo.getListChildren().add(nvbh);
											if (nvbhId.equals(nv.getStaffId())) {
												if (nvbhId.equals(boldId)) {
													nv.setIsBold(true);
												}
											}
										}
									}
								}
							}
						}
					}
					res.getListChildren().add(gdm);
				}
			} else if (StaffObjectType.TBHM.getValue().equals(filterSearch.getLoginStaff().getStaffType().getObjectType())) {
				List<StaffPositionVO> lstTBHV = getListStaffPosition(filter);//getListStaffPosition(null, filterSearch.getLoginStaff().getId(), null, true);
				for (StaffPositionVO tv : lstTBHV) {
					tv.setStaffOwnerId(filter.getStaffId());
					TreeVO<StaffPositionVO> tbhv = new TreeVO<StaffPositionVO>();
					tbhv.setObject(tv);
					res.getListChildren().add(tbhv);
					if (tbhvId.equals(tv.getStaffId())) {
						if (tbhvId.equals(boldId)) {
							tv.setIsBold(true);
						}
						filter.setStaffId(tbhvId);
						List<StaffPositionVO> lstGS = getListStaffPosition(filter);
						tbhv.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
						for (StaffPositionVO gs : lstGS) {
							gs.setStaffOwnerId(filter.getStaffId());
							TreeVO<StaffPositionVO> gsvo = new TreeVO<StaffPositionVO>();
							gsvo.setObject(gs);
							tbhv.getListChildren().add(gsvo);
							if (gsId.equals(gs.getStaffId())) {
								if (gsId.equals(boldId)) {
									gs.setIsBold(true);
								}
								filter.setStaffId(gsId);
								List<StaffPositionVO> lstNV = getListStaffPosition(filter);
								gsvo.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
								for (StaffPositionVO nv : lstNV) {
									nv.setStaffOwnerId(filter.getStaffId());
									TreeVO<StaffPositionVO> nvbh = new TreeVO<StaffPositionVO>();
									nvbh.setObject(nv);
									gsvo.getListChildren().add(nvbh);
									if (nvbhId.equals(nv.getStaffId())) {
										if (nvbhId.equals(boldId)) {
											nv.setIsBold(true);
										}
									}
								}
							}
						}
					}
				}
			} else if (StaffObjectType.TBHV.getValue().equals(filterSearch.getLoginStaff().getStaffType().getObjectType())) {
				List<StaffPositionVO> lstGS = getListStaffPosition(filter);//getListStaffPosition(null, filterSearch.getLoginStaff().getId(), null, true);
				for (StaffPositionVO gs : lstGS) {
					gs.setStaffOwnerId(filter.getStaffId());
					TreeVO<StaffPositionVO> gsvo = new TreeVO<StaffPositionVO>();
					gsvo.setObject(gs);
					res.getListChildren().add(gsvo);
					if (gsId.equals(gs.getStaffId())) {
						if (gsId.equals(boldId)) {
							gs.setIsBold(true);
						}
						filter.setStaffId(gsId);
						List<StaffPositionVO> lstNV = getListStaffPosition(filter);
						gsvo.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
						for (StaffPositionVO nv : lstNV) {
							nv.setStaffOwnerId(filter.getStaffId());
							TreeVO<StaffPositionVO> nvbh = new TreeVO<StaffPositionVO>();
							nvbh.setObject(nv);
							gsvo.getListChildren().add(nvbh);
							if (nvbhId.equals(nv.getStaffId())) {
								if (nvbhId.equals(boldId)) {
									nv.setIsBold(true);
								}
							}
						}
					}
				}
			} else if (StaffObjectType.NVGS.getValue().equals(filterSearch.getLoginStaff().getStaffType().getObjectType())) {
				List<StaffPositionVO> lstNV = getListStaffPosition(filter);//getListStaffPosition(null, filterSearch.getLoginStaff().getId(), null, true);
				for (StaffPositionVO nv : lstNV) {
					nv.setStaffOwnerId(filter.getStaffId());
					TreeVO<StaffPositionVO> nvbh = new TreeVO<StaffPositionVO>();
					nvbh.setObject(nv);
					if (nvbhId.equals(nv.getStaffId())) {
						if (nvbhId.equals(boldId)) {
							nv.setIsBold(true);
						}
					}
					res.getListChildren().add(nvbh);
				}
			}
		}
		return res;*/
		return null;
	}

	private List<Staff> getListStaffAncestor(SupFilter filter) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		
		sql.append(" with lstShop as ( ");
		sql.append(" select s.* from shop s where s.status=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and s.shop_code = ?");
			params.add(filter.getShopCode().toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and lower(s.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getShopName().trim().toLowerCase())));
		}
		sql.append(" start with s.shop_id = ? connect by prior shop_id=parent_shop_id ");
		params.add(filter.getShopId());
		sql.append(" ), ");
		sql.append(" lstStaff as ( ");
		sql.append(" 	select * from ( ");
		sql.append(" 		select s.*, ct.object_type ");
		sql.append(" 		from staff s ");
		sql.append(" 		join channel_type ct on s.staff_type_id = ct.channel_type_id and ct.status=1 and ct.type=? ");
		params.add(ChannelTypeType.STAFF.getValue());
		sql.append(" 		where s.status=1 and s.shop_id in (select shop_id from lstShop) ");
		sql.append(" 		and s.staff_id in ( ");
		sql.append(" 		select staff_id from parent_staff_map where status=1 start with parent_staff_id=? connect by prior staff_id=parent_staff_id ");
		params.add(filter.getLoginStaff().getId());
		sql.append(" 		) ");
		sql.append(" 		order by ct.object_type desc, staff_code");
		sql.append(" 	) where rownum=1 ");
		sql.append(" ) ");
		
		sql.append(" select s.* from staff s ");
		sql.append(" join channel_type ct on s.staff_type_id = ct.channel_type_id and ct.status=1 and ct.type=? ");
		params.add(ChannelTypeType.STAFF.getValue());
		sql.append(" where staff_id in (select distinct parent_staff_id from parent_staff_map ");
		sql.append(" where status=1 start with staff_id in (select staff_id from lstStaff) connect by prior parent_staff_id=staff_id) ");
		sql.append(" union all ");
		sql.append(" select * from staff where staff_id = (select staff_id from lstStaff where rownum=1) ");
		
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public List<NVBHSaleVO> getListTBHMSaleVO(KPaging<NVBHSaleVO> kPaging, Long gdmId, Long shopId) throws DataAccessException {
		if (null == gdmId) {
			throw new IllegalArgumentException("gdmId can not null");
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder("");
		sql.append(" with lstShop as ( ");
		sql.append(" select s.* from shop s ");
		sql.append(" where s.status=1 start with s.shop_id = ? connect by prior shop_id=parent_shop_id ");
		params.add(shopId);
		sql.append(" ) ,");
		sql.append(" lstStaff as ( ");
		sql.append(" select s.* from staff s ");
		sql.append(" where s.staff_id  in ( ");
		sql.append(" select psm.staff_id as staffId ");
		sql.append(" from parent_staff_map psm ");
		sql.append(" where psm.status = 1 ");
		sql.append(" and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id) ");
		sql.append(" start with psm.parent_staff_id = ? AND PSM.STATUS = 1 connect by prior psm.staff_id = psm.parent_staff_id ");
		params.add(gdmId);
		sql.append(" ) and s.shop_id in (select shop_id from lstShop) ), ");
		sql.append(" lstStaffOneNode as ( ");
		sql.append(" select s.* from lstStaff s where s.staff_id in ");
		sql.append(" (select staff_id from parent_staff_map where parent_staff_id=?) ");
		params.add(gdmId);
		sql.append(" ), ");
		sql.append(" lstRootNV as ( ");
		sql.append(" SELECT DISTINCT CONNECT_BY_ROOT(PSMP.PARENT_STAFF_ID) AS ROOT_ID, ");
		sql.append(" PSMP.STAFF_ID FROM PARENT_STAFF_MAP PSMP ");
		sql.append(" JOIN STAFF SF ON PSMP.STAFF_ID = SF.STAFF_ID AND SF.STATUS = 1 ");
		sql.append(" WHERE PSMP.STATUS = 1 AND STAFF_TYPE_ID IN ");
		sql.append(" (SELECT CHANNEL_TYPE_ID FROM CHANNEL_TYPE WHERE TYPE = 2 AND OBJECT_TYPE IN (1, 2) AND STATUS = 1) ");
		sql.append(" START WITH PSMP.PARENT_STAFF_ID IN (select staff_id from lstStaffOneNode) ");
		sql.append(" AND PSMP.STATUS = 1 ");
		sql.append(" CONNECT BY PRIOR PSMP.STAFF_ID = PSMP.PARENT_STAFF_ID ");
		sql.append(" ), ");
		sql.append(" lstSaleNV as ( ");
		sql.append(" select distinct rpt.staff_id,rpt.shop_id,rpt.month_amount_plan,rpt.month_amount,rpt.day_amount_plan,rpt.day_amount, ");
		sql.append(" rpt.day_cust,rpt.day_cust_order_plan,rpt.day_cust_order,rpt.create_date,rpt.DAY_APPROVED_AMOUNT ");
		sql.append(" from lstStaff s ");
		sql.append(" join channel_type ct on ct.channel_type_id=s.staff_type_id and ct.object_type in (1,2) ");
		sql.append(" join rpt_staff_sale rpt on rpt.staff_id = s.staff_id ");
		sql.append(" ) ");
		sql.append(" select s.staff_id staffId,s.staff_code staffcode, s.staff_name staffName, ");
		sql.append(" s.shop_id shopid, (select shop_code from shop where shop_id=s.shop_id) shopCode, ");
		sql.append(" (SELECT NVL(SUM(rpt.day_cust_order_plan),0) AS donhang_kh ");
		sql.append(" FROM lstSaleNV rpt ");
		sql.append(" join lstRootNV root on rpt.staff_id=root.staff_id ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND rpt.CREATE_DATE   >= TRUNC(SYSDATE,'MM') ");
		sql.append(" AND rpt.CREATE_DATE      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		sql.append(" and s.staff_id=root.root_id ");
		sql.append(" ) AS dhKH, ");
		sql.append(" (SELECT NVL(SUM(rpt.day_cust_order),0) donhang_th ");
		sql.append(" FROM lstSaleNV rpt ");
		sql.append(" join lstRootNV root on rpt.staff_id=root.staff_id ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND rpt.CREATE_DATE   >= TRUNC(SYSDATE,'MM') ");
		sql.append(" AND rpt.CREATE_DATE      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		sql.append(" and s.staff_id=root.root_id ");
		sql.append(" )AS dhTH, ");
		sql.append(" (SELECT NVL(ROUND(SUM(rpt.DAY_AMOUNT_PLAN)/1000),0) ");
		sql.append(" FROM lstSaleNV rpt ");
		sql.append(" join lstRootNV root on rpt.staff_id=root.staff_id ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND rpt.CREATE_DATE   >= TRUNC(SYSDATE,'MM') ");
		sql.append(" AND rpt.CREATE_DATE      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		sql.append(" and s.staff_id=root.root_id ");
		sql.append(" )AS dsKH, ");
		sql.append(" (SELECT NVL(ROUND(SUM(rpt.DAY_AMOUNT)/1000),0) ");
		sql.append(" FROM lstSaleNV rpt ");
		sql.append(" join lstRootNV root on rpt.staff_id=root.staff_id ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND rpt.CREATE_DATE   >= TRUNC(SYSDATE,'MM') ");
		sql.append(" AND rpt.CREATE_DATE      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		sql.append(" and s.staff_id=root.root_id ");
		sql.append(" )AS dsTH, ");
		sql.append(" (SELECT NVL(ROUND(SUM(rpt.DAY_APPROVED_AMOUNT)/1000),0) ");
		sql.append(" FROM lstSaleNV rpt ");
		sql.append(" join lstRootNV root on rpt.staff_id=root.staff_id ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND rpt.CREATE_DATE   >= TRUNC(SYSDATE,'MM') ");
		sql.append(" AND rpt.CREATE_DATE      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		sql.append(" and s.staff_id=root.root_id ");
		sql.append(" )AS dsDuyet ");
		sql.append(" from staff s ");
		sql.append(" where s.status=1 ");
		sql.append(" and s.staff_id in (select staff_id from parent_staff_map where parent_staff_id=?) ");
		params.add(gdmId);
		sql.append(" order by s.staff_code ");
		//		sql.append("WITH lstGS AS ");
		//		sql.append("  (SELECT * ");
		//		sql.append("  FROM staff");
		//		sql.append("  WHERE status        = 1 ");
		//		sql.append("  and staff_owner_id in ");
		//		sql.append("    ( SELECT staff_id FROM staff WHERE staff_owner_id = ? AND status = 1 ");
		//		params.add(gdmId);
		//		sql.append("    ) ");
		//		sql.append("  ) ");
		//		sql.append("SELECT f.STAFF_ID AS staffId, ");
		//		sql.append("  f.staff_code    as staffcode, ");
		//		sql.append("  f.staff_name          AS staffName, ");
		//		sql.append("  f.shop_id       as shopid, ");
		//		sql.append("  s.shop_code     as shopCode, ");
		//		sql.append("  ( ");
		//		sql.append("    select sum(rpt.day_cust_order_plan) as   ");
		//		sql.append("    from rpt_staff_sale rpt ");
		//		sql.append("      join staff nvbh on rpt.staff_id = nvbh.staff_id ");
		//		sql.append("      join staff gs on gs.staff_id = rpt.parent_staff_id ");
		//		sql.append("     where nvbh.status = 1 ");
		//		sql.append("      and gs.status = 1 ");
		//		sql.append("      and trunc(rpt.create_date,'mm') = trunc(sysdate,'mm') ");
		//		sql.append("      and gs.staff_owner_id = f.staff_id ");
		//		sql.append("  ) AS dhKH, ");
		//		sql.append("  ( ");
		//		sql.append("    select sum(rpt.day_cust_order) donhang_th ");
		//		sql.append("    from rpt_staff_sale rpt ");
		//		sql.append("      join staff nvbh on rpt.staff_id = nvbh.staff_id ");
		//		sql.append("      join staff gs on gs.staff_id = rpt.parent_staff_id ");
		//		sql.append("    where nvbh.status = 1 ");
		//		sql.append("       and gs.status = 1 ");
		//		sql.append("      and trunc(rpt.create_date,'mm') = trunc(sysdate,'mm') ");
		//		sql.append("      and gs.staff_owner_id = f.staff_id ");
		//		sql.append("  )AS dhTH, ");
		//		sql.append("  (SELECT ROUND(SUM(NVL(rpt.DAY_AMOUNT_PLAN,0))/1000) ");
		//		sql.append("  FROM RPT_STAFF_SALE rpt ");
		//		sql.append("  WHERE rpt.CREATE_DATE   >= TRUNC(SYSDATE,'MM') ");
		//		sql.append("  AND rpt.CREATE_DATE      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		//		sql.append("  AND RPT.PARENT_STAFF_ID IN ");
		//		sql.append("    (SELECT staff_id FROM lstGS WHERE staff_owner_id = f.staff_id ");
		//		sql.append("    ) ");
		//		sql.append("  AND EXISTS ");
		//		sql.append("    (SELECT 1 FROM staff s WHERE s.status = 1 AND s.staff_id = rpt.staff_id ");
		//		sql.append("    ) ");
		//		sql.append("  )AS dsKH, ");
		//		sql.append("  (SELECT ROUND(SUM(NVL(rpt.DAY_AMOUNT,0))/1000) ");
		//		sql.append("  FROM RPT_STAFF_SALE rpt ");
		//		sql.append("  WHERE rpt.CREATE_DATE   >= TRUNC(SYSDATE,'MM') ");
		//		sql.append("  AND rpt.CREATE_DATE      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		//		sql.append("  AND RPT.PARENT_STAFF_ID IN ");
		//		sql.append("    (SELECT staff_id FROM lstGS WHERE staff_owner_id = f.staff_id ");
		//		sql.append("    ) ");
		//		sql.append("  AND EXISTS ");
		//		sql.append("    (SELECT 1 FROM staff s WHERE s.status = 1 AND s.staff_id = rpt.staff_id ");
		//		sql.append("    ) ");
		//		sql.append("  )AS dsTH, ");
		//		sql.append("  (SELECT ROUND(SUM(NVL(rpt.DAY_APPROVED_AMOUNT,0))/1000) ");
		//		sql.append("  FROM RPT_STAFF_SALE rpt ");
		//		sql.append("  WHERE rpt.CREATE_DATE   >= TRUNC(SYSDATE,'MM') ");
		//		sql.append("  AND rpt.CREATE_DATE      < add_months(TRUNC(SYSDATE,'mm'),1) ");
		//		sql.append("  AND RPT.PARENT_STAFF_ID IN ");
		//		sql.append("    (SELECT staff_id FROM lstGS WHERE staff_owner_id = f.staff_id ");
		//		sql.append("    ) ");
		//		sql.append("  AND EXISTS ");
		//		sql.append("    (SELECT 1 FROM staff s WHERE s.status = 1 AND s.staff_id = rpt.staff_id ");
		//		sql.append("    ) ");
		//		sql.append("  )AS dsDuyet ");
		//		sql.append("from staff f ");
		//		sql.append("inner join shop s on f.shop_id = s.shop_id and s.status = 1 ");
		//		sql.append("inner join channel_type ct on f.staff_type_id = ct.channel_type_id and ct.type = 2 ");
		//		sql.append("where f.status      =1 ");
		//		sql.append("AND ct.object_type     = 7 ");
		//		sql.append("AND f.STAFF_OWNER_ID=? ");
		//		params.add(gdmId);
		//		sql.append("ORDER BY f.staff_code ");

		final String[] fieldNames = new String[] { "staffId", "staffCode", "staffName", "shopID", "shopCode", "dhKH", "dhTH", "dsKH", "dsTH", "dsDuyet" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL };

		if (null == kPaging) {
			return repo.getListByQueryAndScalar(NVBHSaleVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(NVBHSaleVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}

	/**
	 * @author thachnn
	 * @param code
	 * @param parentId
	 *            des : kiem tra code staff co thuoc quan ly user dang nhap
	 */
	@Override
	public Staff getStaffByCodeOfUserLogin(String code, Long parentId) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		code = code.toUpperCase();
		sql.append("select st.* from staff st where upper(st.staff_code)=? ");
		params.add(code.toUpperCase());

		sql.append(" and st.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
		params.add(parentId);

		Staff rs = repo.getEntityBySQL(Staff.class, sql.toString(), params);
		return rs;
	}

	@Override
	public List<Staff> getListStaffByShop(KPaging<Staff> kPaging, List<Long> lstShopId, String staffName, String staffCode, ActiveType staffActiveType, List<StaffObjectType> objectTypes) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT s.* ");
		sql.append(" FROM staff s JOIN channel_type ct ON (s.staff_type_id = ct.channel_type_id) ");
		sql.append(" WHERE 1 = 1 ");
		if (lstShopId != null && lstShopId.size() > 0) {
			sql.append(" and shop_id in (select shop_id from shop where status=1 START WITH shop_id in ( ");
			for (int i = 0; i < lstShopId.size(); i++) {
				if (i == 0) {
					sql.append("?");
				} else {
					sql.append(",?");
				}
				params.add(lstShopId.get(i));
			}
			sql.append(" ) CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID) ");
		}
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and s.staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffName)) {
			String temp = StringUtility.toOracleSearchLike(staffName.toLowerCase());
			sql.append(" and lower(s.staff_name) like ? ESCAPE '/'");
			params.add(temp);
		}
		sql.append(" 	AND s.status = ?");
		if (null != staffActiveType) {
			params.add(staffActiveType.getValue());
		} else {
			params.add(ActiveType.RUNNING.getValue());
		}

		if (null != objectTypes && objectTypes.size() > 0) {
			sql.append(" 	AND ct.OBJECT_TYPE in (");
			for (int i = 0; i < objectTypes.size(); i++) {
				if (i < objectTypes.size() - 1) {
					sql.append("?, ");
				} else {
					sql.append("?)");
				}
				params.add(objectTypes.get(i).getValue());
			}
		}
		sql.append(" 	ORDER BY s.staff_code");
		if (kPaging != null) {
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, kPaging);
		}
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public List<StaffPositionLog> showDirectionStaff(Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from staff_position_log tpd ");
		sql.append("where TRUNC(tpd.create_date) = TRUNC(sysdate) and staff_id=? order by staff_position_log_id asc ");
		params.add(staffId);
		return repo.getListBySQL(StaffPositionLog.class, sql.toString(), params);
	}
	
	/**
	 * @author trietptm
	 */
	@Override
	public List<StaffPositionLog> showDirectionStaff(Long staffId, Date checkDate, int numPoint, String fromTime, String toTime) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH lst AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM staff_position_log tpd ");
		sql.append(" WHERE tpd.create_date >= TRUNC(?) ");
		params.add(checkDate);
		sql.append(" AND tpd.create_date < TRUNC(?) + 1 ");
		params.add(checkDate);
		sql.append(" AND staff_id =? ");
		params.add(staffId);
		sql.append(" AND shop_id = ");
		sql.append(" (SELECT shop_id FROM staff WHERE staff_id=? ");
		sql.append(" ) ");
		params.add(staffId);
		//		sql.append(" ORDER BY dbms_random.value ");
		sql.append(" ), ");
		sql.append(" row1 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 7 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 8 ");
		sql.append(" ), ");
		sql.append(" row2 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 8 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 9 ");
		sql.append(" ), ");
		sql.append(" row3 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 9 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 10 ");
		sql.append(" ), ");
		sql.append(" row4 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 10 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 11 ");
		sql.append(" ), ");
		sql.append(" row5 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 11 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 12 ");
		sql.append(" ), ");
		sql.append(" row6 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 12 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 13 ");
		sql.append(" ), ");
		sql.append(" row7 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 13 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 14 ");
		sql.append(" ), ");
		sql.append(" row8 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 14 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 15 ");
		sql.append(" ), ");
		sql.append(" row9 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 15 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 16 ");
		sql.append(" ), ");
		sql.append(" row10 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 16 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 17 ");
		sql.append(" ), ");
		sql.append(" row11 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 17 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 18 ");
		sql.append(" ), ");
		sql.append(" row12 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 18 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 19 ");
		sql.append(" ), ");
		sql.append(" row13 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 19 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 20 ");
		sql.append(" ), ");
		sql.append(" row14 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 20 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 21 ");
		sql.append(" ), ");
		sql.append(" row15 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 21 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 22 ");
		sql.append(" ), ");
		sql.append(" row16 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 22 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 23 ");
		sql.append(" ), ");
		sql.append(" row17 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 23 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 24 ");
		sql.append(" ), ");
		sql.append(" row18 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 0 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 1 ");
		sql.append(" ), ");
		sql.append(" row19 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 1 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 2 ");
		sql.append(" ), ");
		sql.append(" row20 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 2 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 3 ");
		sql.append(" ), ");
		sql.append(" row21 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 3 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 4 ");
		sql.append(" ), ");
		sql.append(" row22 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 4 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 5 ");
		sql.append(" ), ");
		sql.append(" row23 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 5 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 6 ");
		sql.append(" ), ");
		sql.append(" row24 AS ");
		sql.append(" (SELECT * ");
		sql.append(" FROM lst ");
		sql.append(" WHERE ");
		sql.append(" ROWNUM <= ? AND ");
		params.add(numPoint);
		sql.append(" TO_NUMBER(TO_CHAR(create_date, 'HH24')) >= 6 ");
		sql.append(" AND TO_NUMBER(TO_CHAR(create_date, 'HH24')) < 7 ");
		sql.append(" ) ");
		sql.append(" , listTemp as (     ");
		sql.append(" select * from row1 ");
		sql.append(" UNION all ");
		sql.append(" select * from row2 ");
		sql.append(" UNION all ");
		sql.append(" select * from row3 ");
		sql.append(" UNION all ");
		sql.append(" select * from row4 ");
		sql.append(" UNION all ");
		sql.append(" select * from row5 ");
		sql.append(" UNION all ");
		sql.append(" select * from row6 ");
		sql.append(" UNION all ");
		sql.append(" select * from row7 ");
		sql.append(" UNION all ");
		sql.append(" select * from row8 ");
		sql.append(" UNION all ");
		sql.append(" select * from row9 ");
		sql.append(" UNION all ");
		sql.append(" select * from row10 ");
		sql.append(" UNION all ");
		sql.append(" select * from row11 ");
		sql.append(" UNION all ");
		sql.append(" select * from row12 ");
		sql.append(" UNION all ");
		sql.append(" select * from row13 ");
		sql.append(" UNION all ");
		sql.append(" select * from row14 ");
		sql.append(" UNION all ");
		sql.append(" select * from row15 ");
		sql.append(" UNION all ");
		sql.append(" select * from row16 ");
		sql.append(" UNION all ");
		sql.append(" select * from row17 ");
		sql.append(" UNION all ");
		sql.append(" select * from row18 ");
		sql.append(" UNION all ");
		sql.append(" select * from row19 ");
		sql.append(" UNION all ");
		sql.append(" select * from row20 ");
		sql.append(" UNION all ");
		sql.append(" select * from row21 ");
		sql.append(" UNION all ");
		sql.append(" select * from row22 ");
		sql.append(" UNION all ");
		sql.append(" select * from row23 ");
		sql.append(" UNION all ");
		sql.append(" select * from row24 ");
		sql.append(" )          ");
		sql.append(" select * from listTemp ");
		if (!StringUtility.isNullOrEmpty(fromTime) && !StringUtility.isNullOrEmpty(toTime)) {
			sql.append(" where TO_CHAR(create_date, 'HH24:MI') >= ? ");
			params.add(fromTime);
			sql.append(" AND TO_CHAR(create_date, 'HH24:MI') <= ? ");
			params.add(toTime);
			sql.append(" order by create_date  ");
		}
		List<StaffPositionLog> result = repo.getListBySQL(StaffPositionLog.class, sql.toString(), params);
		return result;
	}
	
	@Override
	public List<ActionLog> getListActionLog(Long staffId, Date checkDate, String fromTime, String toTime) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * ");
		sql.append(" FROM action_log al ");
		sql.append(" JOIN staff st ");
		sql.append(" ON st.staff_id = al.staff_id ");
//		sql.append(" AND st.shop_id = ? ");
//		params.add(shopId);
		sql.append(" JOIN customer cus ");
		sql.append(" ON cus.customer_id = al.customer_id ");
//		sql.append(" AND cus.shop_id = ? ");
//		params.add(shopId);
//		sql.append(" LEFT JOIN staff gs ");
//		sql.append(" ON gs.staff_id = st.staff_owner_id ");
//		sql.append(" AND gs.shop_id = ? ");
//		params.add(shopId);
//		sql.append(" LEFT JOIN channel_type ct ");
//		sql.append(" ON ct.status = 1 ");
//		sql.append(" AND ct.type = 2 ");
//		sql.append(" AND ct.object_type = 2 ");
//		sql.append(" AND gs.staff_type_id = ct.channel_type_id ");
//		sql.append(" AND ct.shop_id = ? ");
//		params.add(shopId);
		sql.append(" LEFT JOIN routing r ");
		sql.append(" ON r.routing_id = al.routing_id ");
		sql.append(" WHERE al.STAFF_ID = ? ");
		params.add(staffId);
//		sql.append(" and al.shop_id = ? ");
//		params.add(shopId);
		sql.append(" AND al.START_TIME >= TRUNC(?) ");
		params.add(checkDate);
		sql.append(" AND al.START_TIME < TRUNC(?) + 1 ");
		params.add(checkDate);
		sql.append(" AND al.OBJECT_TYPE in (0,1) ");
		if (!StringUtility.isNullOrEmpty(fromTime) && !StringUtility.isNullOrEmpty(toTime)) {
			sql.append(" AND TO_CHAR(al.START_TIME, 'HH24:MI') >= ? ");
			params.add(fromTime);
			sql.append(" AND TO_CHAR(al.START_TIME, 'HH24:MI') <= ? ");
			params.add(toTime);
		}
		sql.append(" order by al.START_TIME asc ");
		List<ActionLog> result = repo.getListBySQL(ActionLog.class, sql.toString(), params);
		return result;
	}

	@Override
	public Staff getStaffByCodeAndShopId(String staffCode, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
//		staffCode = staffCode.toUpperCase();
		/*sql.append(" select * from STAFF where staff_code like ? and status != ? ");
		params.add(staffCode.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		sql.append(" and shop_id in (SELECT shop_id FROM shop where status = 1 start with shop_id = ? and status = 1 connect by prior shop_id = parent_shop_id) ");
		params.add(shopId);
		sql.append(" and staff_type_id in");
		sql.append(" (select channel_type_id from channel_type where status = 1 and type = 2)");*/
		
		sql.append(" select * from STAFF where staff_code = ? and status = ? ");
		params.add(staffCode.toUpperCase());
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and shop_id in (SELECT shop_id FROM shop where status = 1 start with shop_id = ? and status = 1 connect by prior shop_id = parent_shop_id) ");
		params.add(shopId);

		Staff rs = repo.getEntityBySQL(Staff.class, sql.toString(), params);
		return rs;
	}
	
	@Override
	public Staff getStaffByCodeAndListShop(String staffCode, String lstShopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from STAFF where staff_code = ? and status = ? ");
		params.add(staffCode.toUpperCase());
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and shop_id in (SELECT shop_id FROM shop where status = 1 start with shop_id In ( ").append(lstShopId);
		sql.append(" ) and status = 1 connect by prior shop_id = parent_shop_id) ");

		Staff rs = repo.getEntityBySQL(Staff.class, sql.toString(), params);
		return rs;
	}

	/** SangTN: Update Giam sat lo trinh cua nhan vien ban hang */
	/** Cho chon ngay */
	@Override
	public List<StaffPositionVO> getListStaffPositionOptionsDate(SupFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String[] fieldNames = null;
		Type[] fieldTypes = null;
		sql.append(" with lstShop as ( ");
		sql.append(" 	select s.* from shop s ");
		sql.append(" 	where s.status=1 start with s.shop_id = ? connect by prior shop_id=parent_shop_id ");
		params.add(filter.getShopId());
		sql.append(" ), ");
		sql.append(" lstStaff as ( ");
		if(StaffObjectType.KTNPP.getValue().equals(filter.getRoleType())){
			sql.append(" select s.* from staff s join  channel_type ct on ct.channel_type_id = s.staff_type_id ");
			sql.append(" where s.status=1 and ct.status=1 and ct.object_type in (1,2) ");
			sql.append(" and s.shop_id in (select shop_id from lstShop) ");
		}else{
			sql.append(" select s.* from staff s where s.status = 1 and s.staff_id in ");
			sql.append(" (select staff_id from parent_staff_map where status=1 and parent_staff_id=?) ");
			params.add(filter.getStaffId());
			sql.append(" and s.shop_id in (select shop_id from lstShop) ");
		}
		sql.append(" ), ");
		sql.append(" lstPos as ( ");
		sql.append(" select * from ( SELECT spl.*, ROW_NUMBER() OVER(PARTITION BY spl.staff_id ORDER BY spl.create_date DESC) RN ");
		sql.append(" FROM staff_position_log spl ");
		sql.append(" WHERE spl.create_date < trunc(?)+1 ");
		sql.append(" and spl.create_date >= trunc(?) ");
		params.add(filter.getDateTime());
		params.add(filter.getDateTime());
		sql.append(" ) where rn = 1 ");
		sql.append(" ), ");
		sql.append(" lstTraining as ( ");
		sql.append(" 	SELECT s.staff_id staff_id, tp.training_date ");
		sql.append(" 	FROM lstStaff s  JOIN channel_type ct ON (s.staff_type_id = ct.channel_type_id ) ");
		sql.append(" 	LEFT JOIN ");
		sql.append(" 	(SELECT tp.staff_id gsnpp_id, tpd.staff_id nvbh_id , tpd.training_date ");
		sql.append(" 		FROM training_plan tp ");
		sql.append(" 		JOIN training_plan_detail tpd ON tp.training_plan_id = tpd.training_plan_id ");
		sql.append(" 		AND tpd.training_date >=  TRUNC(?) ");
		sql.append(" 		AND tpd.training_date  <  TRUNC(?) + 1 ");
		params.add(filter.getDateTime());
		params.add(filter.getDateTime());
		sql.append(" 	) tp ON ((tp.nvbh_id   = s.staff_id AND ct.object_type      IN (1,2)) ");
		sql.append(" 		OR (tp.gsnpp_id          = s.staff_id AND ct.object_type      IN (5))) ");
		sql.append(" ) ");
		sql.append(" select s.staff_id as staffId, s.staff_code as staffCode, s.staff_name as staffName, ");
		sql.append(" p.LAT AS lat, p.lng AS lng, s.SHOP_ID AS shopId, ");
		sql.append(" (SELECT SHOP_CODE FROM SHOP WHERE s.SHOP_ID=shop_id and status=1 ) as shopCode, ");
		sql.append(" (SELECT SHOP_NAME FROM SHOP WHERE s.SHOP_ID=shop_id and status=1 ) as shopName, ");
		sql.append(" (select case when count(1)>0 then 1 else 0 end from lstTraining where s.staff_id=staff_id and training_date is not null) isHaveTraining, ");
		sql.append(" ct.OBJECT_TYPE AS roleType, ");
		sql.append(" p.ACCURACY    AS accuracy, ");
		sql.append(" p.CREATE_DATE AS createTime ");
		sql.append(" from lstStaff s left join lstPos p on s.staff_id=p.staff_id ");
		sql.append(" join channel_type ct on s.staff_type_id = ct.channel_type_id  and ct.type=? and ct.status=1");
		params.add(ChannelTypeType.STAFF.getValue());
		sql.append(" order by s.staff_code ");

		fieldNames = new String[] { 
				"staffId", "staffCode","staffName",
				 "lat", "lng", "shopId",
				 "shopCode", "shopName","isHaveTraining",
				"roleType", "accuracy", "createTime",
				 };
		fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING,StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT, StandardBasicTypes.TIMESTAMP };
		return repo.getListByQueryAndScalar(StaffPositionVO.class, fieldNames, fieldTypes, sql.toString(), params);
//		StringBuilder sql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		String[] fieldNames = null;
//		Type[] fieldTypes = null;
//		if (oneNode != null && oneNode == true) {
//			if (StaffRoleType.VNM.equals(roleType) && shopId == null) {
//				sql.append(" select staff_id as staffId, " + "? as staffOwnerId, " + "staff_code as staffCode, " + "staff_name as staffName, " + "ct.object_type as roleType,  " + "shop_id shopId, "
//						+ "(select shop_code from shop where shop_id = s.shop_id) shopCode," + "(select shop_name from shop where shop_id = s.shop_id) shopName, "
//						+ "(select count(1) from staff where status = 1 and staff_owner_id = s.staff_id) countStaff");
//				params.add(staffOwnerId);
//				sql.append(" from staff s JOIN channel_type ct on (ct.channel_type_id = s.staff_type_id and ct.status = 1) " + "where ct.object_type = ? ");
//				sql.append(" order by staff_code");
//				params.add(StaffRoleType.GDM.getValue());
//				fieldNames = new String[] { "staffId", "staffOwnerId", "staffCode", "staffName", "roleType", "shopId", "shopCode", "shopName", "countStaff" };
//
//				fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
//						StandardBasicTypes.LONG, //1
//						StandardBasicTypes.STRING, //2
//						StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER }; //3	
//
//			} else {
//				sql.append(" SELECT c_ST.STAFF_ID  AS staffId, ");
//				sql.append("   c_ST.staff_owner_id AS staffOwnerId, ");
//				sql.append("   c_ST.STAFF_CODE     AS staffCode, ");
//				sql.append("  c_ST.STAFF_NAME     AS staffName, ");
//				sql.append("  TB.LAT              AS lat, ");
//				sql.append("   TB.lng              AS lng, ");
//				sql.append("   c_ST.SHOP_ID        AS shopId, ");
//				sql.append("   ( ");
//				sql.append("  CASE ");
//				sql.append("    WHEN staff_train.training_date IS NULL ");
//				sql.append("    THEN 0 ");
//				sql.append("   ELSE 1 ");
//				sql.append("  END) isHaveTraining, ");
//				sql.append("   (SELECT C_SH.SHOP_CODE FROM SHOP c_sh WHERE C_st.SHOP_ID=C_SH.SHOP_ID ");
//				sql.append("   ) AS shopCode, ");
//				sql.append("   (SELECT C_SHn.SHOP_Name FROM SHOP c_shn WHERE C_st.SHOP_ID=C_SHn.SHOP_ID ");
//				sql.append("  ) AS shopName, ");
//				sql.append("   (SELECT ct.object_type ");
//				sql.append("  FROM channel_type ct ");
//				sql.append("  WHERE ct.channel_type_id = C_st.staff_type_id ");
//				sql.append("   )              AS roleType, ");
//				sql.append("  TB.ACCURACY    AS accuracy, ");
//				sql.append("  TB.CREATE_DATE AS createTime, ");
//				sql.append("  (SELECT COUNT(*) FROM Staff s2 WHERE s2.staff_owner_id= c_ST.STAFF_ID ");
//				sql.append("  ) AS countStaff ");
//				sql.append(" FROM STAFF c_st  LEFT JOIN ");
//				sql.append("  (SELECT spl.* ");
//				sql.append("  FROM staff_position_log spl ");
//				sql.append("  WHERE spl.create_date        >=  TRUNC(?) ");
//				sql.append("  AND spl.create_date           <  TRUNC(?) + 1 ");
//				params.add(dateTime);
//				params.add(dateTime);
//				sql.append("  AND spl.staff_position_log_id = ");
//				sql.append("     (SELECT MAX(staff_position_log_id) ");
//				sql.append("    FROM staff_position_log sp2 ");
//				sql.append("    WHERE sp2.staff_id   = spl.staff_id ");
//				sql.append("    AND sp2.create_date >=  TRUNC(?) ");
//				sql.append("    AND sp2.create_date  <  TRUNC(?) + 1 ");
//				params.add(dateTime);
//				params.add(dateTime);
//				sql.append("    ) ");
//				sql.append("   ) tb ");
//				sql.append(" ON c_st.STAFF_ID=TB.STAFF_ID ");
//				sql.append(" LEFT JOIN ");
//				sql.append("   (SELECT s.staff_id staff_id, tp.training_date ");
//				sql.append("     FROM staff s ");
//				sql.append(" JOIN channel_type ct ");
//				sql.append(" ON (s.staff_type_id = ct.channel_type_id ");
//				sql.append(" AND ct.status       = 1) ");
//				sql.append(" LEFT JOIN ");
//				sql.append("  (SELECT tp.staff_id gsnpp_id, ");
//				sql.append("     tpd.staff_id nvbh_id , ");
//				sql.append("     tpd.training_date ");
//				sql.append("   FROM training_plan tp ");
//				sql.append("   JOIN training_plan_detail tpd ");
//				sql.append("   ON tp.training_plan_id = tpd.training_plan_id ");
//				sql.append("   AND tpd.training_date >=  TRUNC(?) ");
//				sql.append("   AND tpd.training_date  <  TRUNC(?) + 1 ");
//				params.add(dateTime);
//				params.add(dateTime);
//				sql.append("   ) tp ON ((tp.nvbh_id   = s.staff_id ");
//				sql.append(" AND ct.object_type      IN (1,2)) ");
//				sql.append(" OR (tp.gsnpp_id          = s.staff_id ");
//				sql.append(" AND ct.object_type      IN (5))) ");
//				sql.append("   ) staff_train ON c_st.staff_id = staff_train.staff_id ");
//				sql.append(" WHERE c_st.STATUS                = 1 ");
//				if (shopId != null) {
//					sql.append(" AND C_ST.SHOP_ID IN ");
//					sql.append("   (SELECT W_SH.SHOP_ID ");
//					sql.append("  FROM SHOP w_sh ");
//					sql.append("    START WITH w_sh.shop_id       = ? ");
//					params.add(shopId);
//					sql.append("    CONNECT BY prior w_sh.shop_id = w_sh.parent_shop_id ");
//					sql.append("   ) ");
//				}
//				sql.append(" order by c_ST.STAFF_NAME ");
//
//				fieldNames = new String[] { "staffId", //1
//						"staffOwnerId", "staffCode", //2
//						"staffName", //3
//						"lat",//4
//						"lng",//5
//						"shopId",//6
//						"shopCode",//7
//						"shopName",//7
//						"roleType", "accuracy", "createTime", "countStaff", "isHaveTraining" };//8
//
//				fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
//						StandardBasicTypes.LONG, StandardBasicTypes.STRING, //2
//						StandardBasicTypes.STRING, //3
//						StandardBasicTypes.FLOAT, //4
//						StandardBasicTypes.FLOAT, //4
//						StandardBasicTypes.LONG, //4
//						StandardBasicTypes.STRING, //4
//						StandardBasicTypes.STRING, //4
//						StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };//5
//			}
//		} else {
//			sql.append(" WITH tmp AS ");
//			sql.append(" (SELECT MAX(id) id, ");
//			sql.append(" staff_id ");
//			sql.append(" FROM staff_position_log ");
//			//sql.append(" WHERE create_date >= TRUNC(sysdate) ");
//			sql.append(" WHERE create_date >= TRUNC(?) ");
//			sql.append(" AND create_date < TRUNC(?) + 1 ");
//			params.add(dateTime);
//			params.add(dateTime);
//			sql.append(" GROUP BY staff_id ");
//			sql.append(" ) ");
//			sql.append(" SELECT st.staff_id AS staffId, ");
//			sql.append(" st.staff_code    AS staffCode, ");
//			sql.append(" st.name          AS staffName, ");
//			sql.append(" st.shop_id       AS shopId, ");
//			sql.append(" case when st.staff_owner_id is null and st.role_type = 5 then ? else st.staff_owner_id end AS staffOwnerId, ");
//			params.add(staffOwnerId);
//			sql.append(" (SELECT s.shop_code FROM shop s WHERE s.shop_id = st.shop_id ");
//			sql.append(" )               AS shopCode, ");
//			sql.append(" (SELECT s.shop_name FROM shop s WHERE s.shop_id = st.shop_id ");
//			sql.append(" )               AS shopName, ");
//			sql.append(" st.role_type    AS roleType, ");
//			sql.append(" spl.lat         AS lat, ");
//			sql.append(" spl.lng         AS lng, ");
//			sql.append(" spl.accuracy    AS accuracy, ");
//			sql.append(" spl.create_date AS createTime ");
//			//sql.append(" FROM ( select * from staff_position_log spl where spl.create_date >= TRUNC(sysdate) and spl.id IN ( ");
//			sql.append(" FROM ( select * from staff_position_log spl where spl.create_date >= TRUNC(?) and spl.create_date < TRUNC(?)+1 and spl.id IN ( ");
//			params.add(dateTime);
//			params.add(dateTime);
//			sql.append(" SELECT id FROM tmp ");
//			sql.append(" ) ) spl ");
//			sql.append(" RIGHT JOIN staff st ");
//			sql.append(" ON spl.staff_id        = st.staff_id ");
//			if (StaffRoleType.NVTT.equals(roleType)) {
//				sql.append(" WHERE st.status     = 1 and st.role_type = 1 ");
//			} else {
//				sql.append(" WHERE st.status     = 1 and st.role_type != 4 ");
//			}
//			if (shopId != null) {
//				sql.append(" AND st.shop_id   IN ");
//				sql.append(" (SELECT shop_id ");
//				sql.append(" FROM shop ");
//				sql.append(" START WITH shop_id       = ? ");
//				sql.append(" CONNECT BY prior shop_id = parent_shop_id ");
//				sql.append(" ) ");
//				params.add(shopId);
//			}
//			if (staffOwnerId != null) {
//				sql.append(" AND (st.STAFF_OWNER_ID = ? or st.STAFF_ID = ?) ");
//				params.add(staffOwnerId);
//				params.add(staffOwnerId);
//			}
//
//			fieldNames = new String[] { "staffId", "staffOwnerId", "staffCode", "staffName", "lat", "lng", "shopId", "shopName", "staffOwnerId", "shopCode", "roleType", "accuracy", "createTime" };
//
//			fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.LONG, StandardBasicTypes.STRING,
//					StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT, StandardBasicTypes.TIMESTAMP };//5
//		}
//
//		System.out.print(TestUtils.addParamToQuery(sql.toString(), params.toArray()));
//		return repo.getListByQueryAndScalar(StaffPositionVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<StaffPositionVOEx> getStaffPositionEx(SupFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String[] fieldNames = null;
		Type[] fieldTypes = null;
		sql.append(" with lstStaff as ( ");
		if(StaffObjectType.KTNPP.getValue().equals(filter.getRoleType())){
			sql.append(" select s.* from staff s join channel_type ct on ct.channel_type_id=s.staff_type_id ");
			sql.append(" where ct.object_type in (1,2) and s.shop_id = ? ");
			params.add(filter.getShopId());
		}else{
			sql.append(" select s.* from staff s ");
			sql.append(" where s.staff_id  in ( ");
			sql.append(" select psm.staff_id as staffId ");
			sql.append(" from parent_staff_map psm ");
			sql.append(" where psm.status = 1 ");
			sql.append(" and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id) ");
			sql.append(" start with psm.parent_staff_id = ? AND PSM.STATUS = 1 connect by prior psm.staff_id = psm.parent_staff_id) ");
			params.add(filter.getLoginStaffId());
		}
		sql.append(" ), ");
		sql.append(" tmp AS ");
		sql.append(" (SELECT staff_position_log_id, ");
		sql.append(" staff_id ");
		sql.append(" FROM staff_position_log ");
		sql.append(" WHERE create_date >= TRUNC(?) ");
		sql.append(" AND create_date < TRUNC(?) + 1");
		params.add(filter.getDateTime());
		params.add(filter.getDateTime());
		sql.append(" AND staff_id = ? ");
		params.add(filter.getStaffId());

		sql.append(" ) ");
		sql.append(" SELECT st.staff_id AS staffId, ");
		sql.append(" st.staff_code    AS staffCode, ");
		sql.append(" st.staff_name          AS staffName, ");
		sql.append(" st.shop_id       AS shopId, ");
		sql.append(" (SELECT s.shop_code FROM shop s WHERE s.shop_id = st.shop_id ");
		sql.append(" )               AS shopCode, ");
		sql.append(" (SELECT s.shop_name FROM shop s WHERE s.shop_id = st.shop_id ");
		sql.append(" )               AS shopName, ");
		sql.append(" (select ct.object_type from channel_type ct where ct.status=1 and ct.type=2 and ct.channel_type_id = st.staff_type_id)    AS roleType, ");
		sql.append(" spl.lat         AS lat, ");
		sql.append(" spl.lng         AS lng, ");
		sql.append(" spl.accuracy    AS accuracy, ");
		sql.append(" 0    AS isOr, ");
		sql.append(" spl.create_date AS createTime ");

		sql.append(", ''             AS ordinalVisit ");
		sql.append(", 0     		 AS cusLat");
		sql.append(", 0     		 AS cusLng");
		sql.append(", to_char(' ')    AS customerCode,  to_char(' ') AS customerName ");
		sql.append(", to_char(' ') AS address,  ' ' AS mobilephone, ' ' AS phone  ");

		sql.append(" FROM ( select * from staff_position_log spl " + "where spl.create_date >= TRUNC(?) and spl.create_date < TRUNC(?) + 1 " + "and spl.staff_position_log_id IN ( SELECT staff_position_log_id FROM tmp)");
		params.add(filter.getDateTime());
		params.add(filter.getDateTime());

		sql.append(" and spl.staff_id IN (SELECT staff_id FROM tmp) ");
		sql.append("  ) spl ");
		sql.append(" JOIN lstStaff st ");
		sql.append(" ON spl.staff_id        = st.staff_id ");
		sql.append(" WHERE st.status     = 1 AND st.staff_type_id not in (select channel_type_id from channel_type where object_type = 4 and status =1 and type=2) ");

		sql.append(" union all ");

		sql.append(" SELECT st.staff_id AS staffId, ");
		sql.append(" st.staff_code    AS staffCode, ");
		sql.append(" st.staff_name          AS staffName, ");
		sql.append(" st.shop_id       AS shopId, ");
		sql.append(" (SELECT s.shop_code FROM shop s WHERE s.shop_id = st.shop_id ");
		sql.append(" )               AS shopCode, ");
		sql.append(" (SELECT s.shop_name FROM shop s WHERE s.shop_id = st.shop_id ");
		sql.append(" )               AS shopName, ");
		sql.append("  (select ct.object_type from channel_type ct where ct.status=1 and ct.type=2 and ct.channel_type_id = st.staff_type_id)    AS roleType, ");
		sql.append(" al.lat         AS lat, ");
		sql.append(" al.lng         AS lng, ");
		sql.append(" 0    AS accuracy, ");
		sql.append(" al.is_or    AS isOr, ");
		sql.append(" al.start_time AS createTime ");

		sql.append(", ''              AS ordinalVisit ");
		sql.append(", cu.lat     AS cusLat");
		sql.append(",  cu.lng     AS cusLng");
		sql.append(", to_char(substr(cu.customer_code, 0, 3))    AS customerCode, to_char(cu.customer_name) AS customerName ");
		sql.append(", to_char(cu.housenumber  || ','  || cu.street) AS address,  cu.mobiphone, cu.phone ");
		sql.append("  FROM    action_log al ");
		sql.append(" JOIN lstStaff st ");
		sql.append(" ON al.staff_id        = st.staff_id  and al.is_or = 0");
		sql.append(" JOIN customer cu on al.customer_id = cu.customer_id ");

		sql.append(" WHERE st.status     = 1 AND st.staff_type_id not in (select channel_type_id from channel_type where object_type = 4 and status =1 and type=2) AND al.object_type in (0, 1) AND al.start_time >= TRUNC(?) "
				+ "AND al.start_time < TRUNC(?) + 1 AND st.staff_id = ?");
		params.add(filter.getDateTime());
		params.add(filter.getDateTime());
		params.add(filter.getStaffId());

		sql.append(" ORDER BY staffId, createTime ");

		fieldNames = new String[] { 
				"staffId", "staffCode", "staffName", 
				"lat", "lng", "shopId",
				"shopName", "shopCode", "roleType",
				"accuracy", "isOr", "createTime", 
				"ordinalVisit", "cusLat", "cusLng",
				"customerCode", "customerName", "address", 
				"mobilephone", "phone" };

		fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.FLOAT, StandardBasicTypes.INTEGER, StandardBasicTypes.TIMESTAMP, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
//		System.out.print(TestUtils.addParamToQuery(sql.toString(), params.toArray()));
		return repo.getListByQueryAndScalar(StaffPositionVOEx.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public TreeVO<StaffPositionVO> searchStaffEx(SupFilter filterSearch) throws DataAccessException {
		/*TreeVO<StaffPositionVO> res = new TreeVO<StaffPositionVO>();
		res.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
		Long gdmId = 0l;
		Long tbhvId = 0l;
		Long gsId = 0l;
		Long nvbhId = 0l;
		Long boldId = 0l;
		List<Staff> lstStaff = getListStaffAncestor(filterSearch);
		SupFilter filter = new SupFilter();
		filter.setOneNode(true);
		filter.setStaffId(filterSearch.getLoginStaff().getId());
		filter.setDateTime(filterSearch.getDateTime());
		filter.setShopId(filterSearch.getShopId());
		if (lstStaff != null && lstStaff.size() > 0) {
			for (Staff staff : lstStaff) {
				if (gdmId.equals(0l) && StaffObjectType.TBHM.getValue().equals(staff.getStaffType().getObjectType())) {
					gdmId = staff.getId();
					if (tbhvId.equals(0l) && gsId.equals(0l) && nvbhId.equals(0l)) {
						boldId = gdmId;
					}
				} else if (tbhvId.equals(0l) && StaffObjectType.TBHV.getValue().equals(staff.getStaffType().getObjectType())) {
					tbhvId = staff.getId();
					if (gsId.equals(0l) && nvbhId.equals(0l)) {
						boldId = tbhvId;
					}
				} else if (gsId.equals(0l) && StaffObjectType.NVGS.getValue().equals(staff.getStaffType().getObjectType())) {
					gsId = staff.getId();
					if (nvbhId.equals(0l)) {
						boldId = gsId;
					}
				} else if (nvbhId.equals(0l) && (StaffObjectType.NVBH.getValue().equals(staff.getStaffType().getObjectType()) || StaffObjectType.NVVS.getValue().equals(staff.getStaffType().getObjectType()))) {
					nvbhId = staff.getId();
					boldId = nvbhId;
				}
			}
			if (StaffRoleType.VNM.getValue().equals(filterSearch.getLoginStaff().getStaffType().getObjectType())) {
				List<StaffPositionVO> lstGDM = getListStaffPositionOptionsDate(filter);
				for (StaffPositionVO child : lstGDM) {
					child.setStaffOwnerId(filter.getStaffId());
					TreeVO<StaffPositionVO> gdm = new TreeVO<StaffPositionVO>();
					gdm.setObject(child);
					if (gdmId.equals(child.getStaffId())) {
						if (gdmId.equals(boldId)) {
							child.setIsBold(true);
						}
						filter.setStaffId(gdmId);
						List<StaffPositionVO> lstTBHV = getListStaffPositionOptionsDate(filter);
						gdm.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
						for (StaffPositionVO tv : lstTBHV) {
							tv.setStaffOwnerId(filter.getStaffId());
							TreeVO<StaffPositionVO> tbhv = new TreeVO<StaffPositionVO>();
							tbhv.setObject(tv);
							gdm.getListChildren().add(tbhv);
							if (tbhvId.equals(tv.getStaffId())) {
								if (tbhvId.equals(boldId)) {
									tv.setIsBold(true);
								}
								filter.setStaffId(tbhvId);
								List<StaffPositionVO> lstGS = getListStaffPositionOptionsDate(filter);
								tbhv.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
								for (StaffPositionVO gs : lstGS) {
									gs.setStaffOwnerId(filter.getStaffId());
									TreeVO<StaffPositionVO> gsvo = new TreeVO<StaffPositionVO>();
									gsvo.setObject(gs);
									tbhv.getListChildren().add(gsvo);
									if (gsId.equals(gs.getStaffId())) {
										if (gsId.equals(boldId)) {
											gs.setIsBold(true);
										}
										filter.setStaffId(gsId);
										List<StaffPositionVO> lstNV = getListStaffPositionOptionsDate(filter);
										gsvo.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
										for (StaffPositionVO nv : lstNV) {
											nv.setStaffOwnerId(filter.getStaffId());
											TreeVO<StaffPositionVO> nvbh = new TreeVO<StaffPositionVO>();
											nvbh.setObject(nv);
											gsvo.getListChildren().add(nvbh);
											if (nvbhId.equals(nv.getStaffId())) {
												if (nvbhId.equals(boldId)) {
													nv.setIsBold(true);
												}
											}
										}
									}
								}
							}
						}
					}
					res.getListChildren().add(gdm);
				}
			} else if (StaffRoleType.GDM.getValue().equals(filterSearch.getLoginStaff().getStaffType().getObjectType())) {
				List<StaffPositionVO> lstTBHV = getListStaffPositionOptionsDate(filter);
				for (StaffPositionVO tv : lstTBHV) {
					tv.setStaffOwnerId(filter.getStaffId());
					TreeVO<StaffPositionVO> tbhv = new TreeVO<StaffPositionVO>();
					tbhv.setObject(tv);
					res.getListChildren().add(tbhv);
					if (tbhvId.equals(tv.getStaffId())) {
						if (tbhvId.equals(boldId)) {
							tv.setIsBold(true);
						}
						filter.setStaffId(tbhvId);
						List<StaffPositionVO> lstGS = getListStaffPositionOptionsDate(filter);
						tbhv.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
						for (StaffPositionVO gs : lstGS) {
							gs.setStaffOwnerId(filter.getStaffId());
							TreeVO<StaffPositionVO> gsvo = new TreeVO<StaffPositionVO>();
							gsvo.setObject(gs);
							tbhv.getListChildren().add(gsvo);
							if (gsId.equals(gs.getStaffId())) {
								if (gsId.equals(boldId)) {
									gs.setIsBold(true);
								}
								filter.setStaffId(gsId);
								List<StaffPositionVO> lstNV = getListStaffPositionOptionsDate(filter);
								gsvo.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
								for (StaffPositionVO nv : lstNV) {
									nv.setStaffOwnerId(filter.getStaffId());
									TreeVO<StaffPositionVO> nvbh = new TreeVO<StaffPositionVO>();
									nvbh.setObject(nv);
									gsvo.getListChildren().add(nvbh);
								}
							}
						}
					}
				}
			} else if (StaffRoleType.TBHV.getValue().equals(filterSearch.getLoginStaff().getStaffType().getObjectType())) {
				List<StaffPositionVO> lstGS = getListStaffPositionOptionsDate(filter);
				for (StaffPositionVO gs : lstGS) {
					gs.setStaffOwnerId(filter.getStaffId());
					TreeVO<StaffPositionVO> gsvo = new TreeVO<StaffPositionVO>();
					gsvo.setObject(gs);
					res.getListChildren().add(gsvo);
					if (gsId.equals(gs.getStaffId())) {
						if (gsId.equals(boldId)) {
							gs.setIsBold(true);
						}
						filter.setStaffId(gsId);
						List<StaffPositionVO> lstNV = getListStaffPositionOptionsDate(filter);
						gsvo.setListChildren(new ArrayList<TreeVO<StaffPositionVO>>());
						for (StaffPositionVO nv : lstNV) {
							nv.setStaffOwnerId(filter.getStaffId());
							TreeVO<StaffPositionVO> nvbh = new TreeVO<StaffPositionVO>();
							nvbh.setObject(nv);
							gsvo.getListChildren().add(nvbh);
						}
					}
				}
			} else if (StaffRoleType.GSNPP.getValue().equals(filterSearch.getLoginStaff().getStaffType().getObjectType())) {
				List<StaffPositionVO> lstNV = getListStaffPositionOptionsDate(filter);
				for (StaffPositionVO nv : lstNV) {
					nv.setStaffOwnerId(filter.getStaffId());
					TreeVO<StaffPositionVO> nvbh = new TreeVO<StaffPositionVO>();
					nvbh.setObject(nv);
					res.getListChildren().add(nvbh);
				}
			}
		}
		return res;*/
		return null;
	}

	@Override
	public Staff getStaffByCodeAndShopId1(String staffCode, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		staffCode = staffCode.toUpperCase();
		sql.append(" select * from STAFF where staff_code=? and status!=? and shop_id=? ");
		List<Object> params = new ArrayList<Object>();
		params.add(staffCode.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		params.add(shopId);

		Staff rs = repo.getEntityBySQL(Staff.class, sql.toString(), params);
		return rs;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public void offVisitPlanOfStaff(long staffId, long shopId, Date updateToDate, String userCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("update visit_plan");
		sql.append(" set status = ?, to_date = ?, update_date = sysdate, update_user = ?");
		params.add(ActiveType.STOPPED.getValue());
		params.add(updateToDate);
		params.add(userCode);
		sql.append(" where staff_id = ? and shop_id = ? and status in (?, ?)");
		params.add(staffId);
		params.add(shopId);
		params.add(ActiveType.STOPPED.getValue());
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and (to_date is null or to_date >= trunc(?))");
		params.add(updateToDate);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<Staff> getListStaffPopup(StaffFilter filter) throws DataAccessException {
		if (filter == null || filter.getStaffType() == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (StaffObjectType.NVGS.equals(filter.getStaffType())) {
			sql.append("select st.* from staff st");
			sql.append(" where st.shop_id = (select parent_shop_id from shop where shop_id = ?) and st.status = ?");
			params.add(filter.getShopId());
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" and exists (select 1 from channel_type ct where ct.status = ? and ct.type = ?");
			params.add(ActiveType.RUNNING.getValue());
			params.add(ChannelTypeType.STAFF.getValue());
			sql.append(" and ct.object_type = ? and ct.channel_type_id = st.staff_type_id)");
			params.add(filter.getStaffType().getValue());
		} else if (StaffObjectType.NVBH.equals(filter.getStaffType()) || StaffObjectType.NVVS.equals(filter.getStaffType())) {
			sql.append("select st.* from staff st");
			sql.append(" where st.shop_id = ? and st.status = ?");
			params.add(filter.getShopId());
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" and exists (select 1 from channel_type ct where ct.status = ? and ct.type = ?");
			params.add(ActiveType.RUNNING.getValue());
			params.add(ChannelTypeType.STAFF.getValue());
			sql.append(" and ct.object_type in (?,?) and ct.channel_type_id = st.staff_type_id)");
			params.add(StaffObjectType.NVBH.getValue());
			params.add(StaffObjectType.NVVS.getValue());
			sql.append(" and not exists (select 1 from staff_group_detail gd where gd.status = ? and gd.staff_id = st.staff_id)");
			params.add(ActiveType.RUNNING.getValue());
		} else if (StaffObjectType.TBHM.equals(filter.getStaffType()) || StaffObjectType.TBHV.equals(filter.getStaffType())) {
			sql.append("select st.* from staff st");
			sql.append(" where st.status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" and exists (select 1 from channel_type ct where ct.status = ? and ct.type = ?");
			params.add(ActiveType.RUNNING.getValue());
			params.add(ChannelTypeType.STAFF.getValue());
			sql.append(" and ct.object_type = ? and ct.channel_type_id = st.staff_type_id)");
			params.add(filter.getStaffType().getValue());
			sql.append(" and not exists (select 1 from staff_group_detail gd where gd.status = ? and gd.staff_id = st.staff_id)");
			params.add(ActiveType.RUNNING.getValue());
		}

		if (StringUtility.isNullOrEmpty(sql.toString())) {
			return null;
		}
		sql.append(" order by st.staff_code");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, filter.getkPaging());
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<Staff> getListStaffOfShop(StaffFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("with lstShopId as (");
		sql.append(" select shop_id from shop");
		sql.append(" where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" start with shop_id = ?");
		params.add(filter.getShopId());
		sql.append(" connect by prior shop_id = parent_shop_id");
		sql.append(" )");
		sql.append(" select * from staff st");
		if (filter.getStatus() == null) {
			sql.append(" where st.status <> ?");
			params.add(ActiveType.DELETED.getValue());
		} else {
			sql.append(" where st.status = ?");
			params.add(filter.getStatus().getValue());
		}
		sql.append(" and st.shop_id in (select shop_id from lstShopId)");
		sql.append(" and exists (select 1 from channel_type ct where ct.channel_type_id = st.staff_type_id and ct.type = ? and ct.status = ?)");
		params.add(ChannelTypeType.STAFF.getValue());
		params.add(ActiveType.RUNNING.getValue());

		String s = filter.getStaffCode();
		if (!StringUtility.isNullOrEmpty(s)) {
			s = s.toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			sql.append(" and st.staff_code like ? escape '/'");
			params.add(s);
		}
		s = filter.getStaffName();
		if (!StringUtility.isNullOrEmpty(s)) {
			// tulv2 update 15.10.2014 tim kiem theo truong name_text
			String tmpNameText = Unicode2English.codau2khongdau(s);
			tmpNameText = tmpNameText.toLowerCase();
			tmpNameText = StringUtility.toOracleSearchLike(tmpNameText);
			sql.append(" and lower(st.name_text) like ? escape '/'");
			params.add(tmpNameText);
		}
		sql.append(" order by st.staff_code, st.staff_name");

		if (filter.getkPaging() == null) {
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, filter.getkPaging());
	}

	/**
	 * @author lacnv1
	 * @modified phuocdh2 chinh sua export danh sach nhan vien theo mau moi
	 */
	@Override
	public List<StaffExportVO> getListExportStaffOfShop(StaffFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("with lstShopId as (");
		sql.append(" select shop_id from shop");
		sql.append(" where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" start with shop_id = ?");
		params.add(filter.getShopId());
		sql.append(" connect by prior shop_id = parent_shop_id");
		sql.append(" )");
		sql.append(" select (select shop_code from shop where shop.shop_id = st.shop_id) as shopCode,");
		sql.append(" (select shop_name from shop where shop.shop_id = st.shop_id) as shopName,");//
		sql.append(" (select province_name from area where area.area_id = st.area_id) || ' - ' || ");
		sql.append(" (select district_name from area where area.area_id = st.area_id) || ' - ' || ");
		sql.append(" (select precinct_name from area where area.area_id = st.area_id) as areaName,");
		sql.append(" st.staff_code as staffCode, st.staff_name as staffName, st.gender as gender,");
		sql.append(" to_char(st.start_working_day, 'dd/mm/yyyy') as startDate,");
		sql.append(" st.education as education, st.position as position, st.idno as idNumber,");
		sql.append(" to_char(st.idno_date, 'dd/mm/yyyy') as idDate, st.idno_place as idPlace,");
		sql.append(" st.housenumber as houseNumber, st.street as street, st.address as address,");
		sql.append(" (select province from area where area.area_id = st.area_id) as provinceCode,");
		sql.append(" (select province_name from area where area.area_id = st.area_id) as provinceName,");
		sql.append(" (select district from area where area.area_id = st.area_id) as districtCode,");
		sql.append(" (select district_name from area where area.area_id = st.area_id) as districtName,");
		sql.append(" (select precinct from area where area.area_id = st.area_id) as precinctCode,");
		sql.append(" (select precinct_name from area where area.area_id = st.area_id) as precinctName,");
		sql.append(" st.mobilephone as mobile, st.phone as phone, st.email as email,");
		sql.append(" (select channel_type_name from channel_type ct where ct.channel_type_id = st.staff_type_id) as staffType,");
		sql.append(" st.sale_type_code as saleTypeCode, st.sale_group as saleGroup, st.status as status");

		fromSql.append(" from staff st");
		if (filter.getStatus() == null) {
			fromSql.append(" where st.status <> ?");
			params.add(ActiveType.DELETED.getValue());
		} else {
			fromSql.append(" where st.status = ?");
			params.add(filter.getStatus().getValue());
		}
		fromSql.append(" and st.shop_id in (select shop_id from lstShopId)");
		fromSql.append(" and exists (select 1 from staff_type  ste where ste.staff_type_id = st.staff_type_id and ste.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if(filter.getStaffTypeName()!=null){
			fromSql.append(" and ste.name like ? escape '/'");
			params.add(filter.getStaffTypeName());
		}
		fromSql.append(" )");

		String s = filter.getStaffCode();
		if (!StringUtility.isNullOrEmpty(s)) {
			s = s.toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			fromSql.append(" and st.staff_code like ? escape '/'");
			params.add(s);
		}
		s = filter.getStaffName();
		if (!StringUtility.isNullOrEmpty(s)) {
			s = s.trim().toLowerCase();
			s = StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(s));
			fromSql.append(" and lower(st.name_text) like ? escape '/'");
			params.add(s);
		}
		// add them shopName va areaName
		String[] fieldNames = new String[] { "shopCode","shopName","areaName", "staffCode", "staffName", "gender", "startDate", "education", "position", "idNumber", "idDate", "idPlace", "houseNumber", "street", "address", "provinceCode", "provinceName", "districtCode",
				"districtName", "precinctCode", "precinctName", "mobile", "phone", "email", "staffType", "saleTypeCode", "saleGroup", "status" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };

		sql.append(fromSql.toString());
		sql.append(" order by shopCode, staffCode, staffName");
		if (filter.getExPaging() == null) {
			return repo.getListByQueryAndScalar(StaffExportVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append("with lstShopId as (");
		countSql.append(" select shop_id from shop");
		countSql.append(" where status = ?");
		countSql.append(" start with shop_id = ?");
		countSql.append(" connect by prior shop_id = parent_shop_id");
		countSql.append(" ) select count(1) as count");
		countSql.append(fromSql.toString());
		return repo.getListByQueryAndScalarPaginated(StaffExportVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getExPaging());
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<StaffGroupDetailVO> getListStaffGroupOfUnitTree(StaffFilter filter, KPaging<StaffGroupDetailVO> paging) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("with lstShopId as (");
		sql.append(" select shop_id from shop");
		sql.append(" where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" start with shop_id = ?");
		params.add(filter.getShopId());
		sql.append(" connect by prior shop_id = parent_shop_id");
		sql.append(" )");

		sql.append(" select s.shop_id as shopId, s.shop_code as shopCode, g.staff_group_id as groupId,");
		sql.append(" gd.staff_group_detail_id as groupDetailId, st.staff_id as staffId,");
		sql.append(" st.staff_code as staffCode, st.staff_name as staffName, st.mobilephone as mobile,");
		sql.append(" ct.channel_type_code as staffType");

		fromSql.append(" from staff_group_detail gd");
		fromSql.append(" join staff st on (st.staff_id = gd.staff_id and st.status = ?)");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" join staff_group g on (g.staff_group_id = gd.staff_group_id and g.status = ?)");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" join shop s on (s.shop_id = g.shop_id)");
		fromSql.append(" join channel_type ct on (ct.channel_type_id = st.staff_type_id and ct.type = ? and ct.status = ?)");
		params.add(ChannelTypeType.STAFF.getValue());
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" where gd.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" and s.shop_id in (select shop_id from lstShopId)");

		String s = filter.getStaffCode();
		if (!StringUtility.isNullOrEmpty(s)) {
			s = s.toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			fromSql.append(" and st.staff_code like ? escape '/'");
			params.add(s);
		}
		s = filter.getStaffName();
		if (!StringUtility.isNullOrEmpty(s)) {
			s = s.toLowerCase();
			s = StringUtility.toOracleSearchLike(s);
			fromSql.append(" and lower(st.staff_name) like ? escape '/'");
			params.add(s);
		}

		sql.append(fromSql.toString());
		sql.append(" order by shopCode, staffCode, staffName, staffType");

		String[] fieldNames = new String[] { "shopId", "shopCode", "groupId", "groupDetailId", "staffId", "staffCode", "staffName", "mobile", "staffType" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING };

		if (paging == null) {
			return repo.getListByQueryAndScalar(StaffGroupDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append("with lstShopId as (");
		countSql.append(" select shop_id from shop");
		countSql.append(" where status = ?");
		countSql.append(" start with shop_id = ?");
		countSql.append(" connect by prior shop_id = parent_shop_id");
		countSql.append(" ) select count(1) as count");
		countSql.append(fromSql.toString());
		return repo.getListByQueryAndScalarPaginated(StaffGroupDetailVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, paging);
	}

	@Override
	public List<Staff> getListStaffYMamagerStaffX(Long staffIdY, Long staffIdX) throws DataAccessException {
		StringBuffer sql = new StringBuffer();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *");
		sql.append(" FROM staff");
		sql.append(" WHERE staff_id IN");
		sql.append(" (SELECT staff_id");
		sql.append(" FROM PARENT_STAFF_MAP");
		sql.append(" WHERE PARENT_STAFF_ID = ?");
		params.add(staffIdY);
		sql.append(" AND staff_id          = ?");
		params.add(staffIdX);
		sql.append(" AND TO_DATE          IS NULL");
		sql.append(" AND status            = ?)");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND staff_id NOT IN");
		sql.append("   (SELECT staff_id");
		sql.append("   FROM EXCEPTION_USER_ACCESS");
		sql.append("   WHERE EXCEPTION_USER_ACCESS.PARENT_STAFF_ID = ?");
		params.add(staffIdY);
		sql.append("   AND staff_id                               =?");
		params.add(staffIdX);
		sql.append(" AND status            = ?)");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND STATUS = ?");
		params.add(ActiveType.RUNNING.getValue());
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}

	@Override
	public List<StaffVO> getListStaffVO(StaffFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.staff_id staffId, s.staff_code staffCode, s.staff_name staffName ");
		sql.append(" from staff s ");
		if (StringUtility.isNullOrEmpty(filter.getStrListShopId()) && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s.shop_id = ogr.number_key_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		if (StringUtility.isNullOrEmpty(filter.getStrListUserId()) && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			sql.append(" join table (F_GET_LIST_CHILD_STAFF_INHERIT(?, sysdate, ?, ?, null)) prs on s.staff_id = prs.number_key_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		sql.append(" join staff_type st on s.staff_type_id = st.staff_type_id and st.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" where 1=1 ");
		if (filter.getStatus() != null) {
			sql.append(" and s.status = ?");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and s.status<>-1 ");
		}
		if (Boolean.TRUE.equals(filter.getIsGetParent()) && filter.getStaffId() != null) {
			sql.append(" and s.staff_id in (select parent_staff_id from parent_staff_map where staff_id=? and status=1) ");
			params.add(filter.getStaffId());
		}
		if (Boolean.TRUE.equals(filter.getIsGetAllChild()) && filter.getStaffId() != null) {
			sql.append(" and s.staff_id in (select staff_id from parent_staff_map where status=1 start with parent_staff_id in (?) connect by prior staff_id=parent_staff_id)");
			params.add(filter.getStaffId());
		}
		if (filter.getSpecType() != null) {
			sql.append(" and st.SPECIFIC_TYPE = ?  ");
			params.add(filter.getSpecType().getValue());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "s.staff_id");
			sql.append(paramsFix);
		} else if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "s.shop_id");
			sql.append(paramsFix);
		}
		
		if (filter.getAllStaffChildShop() != null && filter.getAllStaffChildShop()) {
			if (filter.getShopId() != null) {
				sql.append(" and s.shop_id IN ( ");
				sql.append(" select shop_id ");
				sql.append(" from shop sh ");
				sql.append(" start with shop_id = ? and status = ? ");
				sql.append(" connect by prior shop_id = parent_shop_id and status = ? ");
				params.add(filter.getShopId());
				params.add(ActiveType.RUNNING.getValue());
				params.add(ActiveType.RUNNING.getValue());
			sql.append(" ) ");
			}
		} else if (filter.getShopId() != null) {
			sql.append(" and s.shop_id = ?");
			params.add(filter.getShopId());
		}
		
		
		sql.append(" order by s.staff_code asc");

		String[] fieldNames = new String[] {//
		"staffId", "staffCode", "staffName" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getStaffVOPaging() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getStaffVOPaging());
		} else {
			return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<Staff> getListStaffByShop(StaffFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.* from staff s ");
		if (Boolean.TRUE.equals(filter.getIsStaffTypeId())) { 
			sql.append(" join organization org on org.organization_id = s.organization_id and org.status = 1 ");
		}
		sql.append(" where 1=1 ");
		if (filter.getStatus() != null) {
			sql.append(" and s.status=? ");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and s.status<>? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getShopId() != null) {
			sql.append(" and s.shop_id=?");
			params.add(filter.getShopId());
		}
		if (Boolean.TRUE.equals(filter.getIsStaffTypeId())) {
			sql.append(" order by org.node_ordinal,s.staff_name ");
		}
		return repo.getListBySQL(Staff.class, sql.toString(), params);
	}
	
	public void stopParentStaffMap(Staff staff, LogInfoVO logInfo) throws DataAccessException {
		try {
			if (staff == null) {
				throw new IllegalArgumentException("staffId is null");
			}
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append(" update parent_staff_map set status=0, to_date=?, update_date=?, update_user=? ");
			params.add(commonDAO.getSysDate());
			params.add(commonDAO.getSysDate());
			params.add(logInfo.getStaffCode());
			sql.append(" where staff_id = ? or parent_staff_id=?");
			params.add(staff.getId());
			params.add(staff.getId());
			repo.executeSQLQuery(sql.toString(), params);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public List<ParentStaffMap> getListParentStaffMap(StaffFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select psm.* from parent_staff_map psm where 1=1 ");
		if (filter.getStatus() != null) {
			sql.append(" and psm.status=? ");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and psm.status<>? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if(filter.getStaffId()!=null && Boolean.TRUE.equals(filter.getIsGetAll())){
			sql.append(" and (psm.staff_id=? or psm.parent_staff_id=?) ");
			params.add(filter.getStaffId());
			params.add(filter.getStaffId());
		}

		return repo.getListBySQL(ParentStaffMap.class, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<Staff> getListStaffLockedStock(StaffFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select st.* ");
		sql.append(" from staff st ");
		sql.append(" join shop s on st.shop_id = s.shop_id");
		sql.append(" where 1 = 1 and st.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (filter.getShopId() != null) {
			sql.append(" and s.shop_id = ?");
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and s.shop_code = ? ");
			params.add(filter.getShopCode().toUpperCase());
		}
		if (filter.getSpecType() != null) {
			sql.append(" and exists (select 1 from staff_type where SPECIFIC_TYPE = ? and status = ? and staff_type_id = st.staff_type_id )");
			params.add(filter.getSpecType().getValue());
			params.add(ActiveType.RUNNING.getValue());
		} else if (filter.getLstSpecType() != null && filter.getLstSpecType().size() > 0) {
			sql.append(" and exists (select 1 from staff_type where status = ? and staff_type_id = st.staff_type_id");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" and SPECIFIC_TYPE in (-1");
			for (StaffSpecificType t : filter.getLstSpecType()) {
				sql.append(", ?");
				params.add(t.getValue());
			}
			sql.append("))");
		}
//		sql.append(" and exists (select 1 from stock_lock where van_lock = 1 and staff_id = st.staff_id and create_date >= trunc(sysdate) and create_date < trunc(sysdate) + 1)");
		sql.append(" and exists (select 1 from stock_lock where van_lock = 1 and staff_id = st.staff_id)");
		
		sql.append(" order by st.staff_code, st.staff_name");
		
		if (filter.getkPaging() == null) {
			List<Staff> lst = repo.getListBySQL(Staff.class, sql.toString(), params);
			return lst;
		}
		List<Staff> lst = repo.getListBySQLPaginated(Staff.class, sql.toString(), params, filter.getkPaging());
		return lst;
	}

	@Override
	public List<StaffVO> findStaffVOBy(KPaging<StaffVO> kPaging,
			UnitFilter unitFilter) throws DataAccessException {
		if (unitFilter == null) {
			throw new IllegalArgumentException("unitFilter must not null");
		}
		if (unitFilter.getStaffRootId() == null || unitFilter.getRoleId() == null || unitFilter.getShopRootId() == null) {
			throw new IllegalArgumentException("priviledge data is null");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select ");
		sql.append(" 	s.staff_id staffId, ");
		sql.append(" 	s.staff_code staffCode, ");
		sql.append(" 	s.staff_name staffName, ");
		sql.append(" 	s.status status, ");
		sql.append("	(select shop_name from shop where shop_id = s.shop_id) shopName, ");
		sql.append(" 	(select name from staff_type where staff_type_id = s.staff_type_id) staffType, ");
		sql.append("	phone, ");
		sql.append("	mobilePhone, ");
		sql.append("	s.shop_id shopId ");
		sql.append(" from staff s ");
		if (ActiveType.RUNNING == unitFilter.getStatus()) {
			sql.append(" join ( ");
		} else {
			sql.append(" left join ( ");
		}
		sql.append("     select number_key_id as staff_id from table (F_GET_LIST_CHILD_STAFF_INHERIT(?, sysdate, ?, ?, null)) ");
		sql.append(" ) privStaff on s.staff_id = privStaff.staff_id ");
		params.add(unitFilter.getStaffRootId());
		params.add(unitFilter.getRoleId());
		params.add(unitFilter.getShopRootId());
		sql.append(" where 1=1 ");
		if (unitFilter.getStatus() != null) {
			sql.append(" and s.status = ? ");
			params.add(unitFilter.getStatus().getValue());
		} else {
			sql.append(" and ((s.status = 1 and privStaff.staff_id is not null) or s.status = 0)");
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getUnitCode())) {
			sql.append(" and s.staff_code like ? ");
			params.add(StringUtility.toOracleSearchLike(unitFilter.getUnitCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getUnitName())) {
			sql.append(" and upper(s.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(unitFilter.getUnitName().trim().toUpperCase())));
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getManageUnitCode())) {
			sql.append(" and exists ( ");
			sql.append(" 	select 1 ");
			sql.append(" 	from shop ");
			sql.append(" 	where 1=1 ");
			sql.append(" 	and shop_id = s.shop_id ");
			sql.append(" 	and status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" 	start with shop_code = ? ");
			params.add(unitFilter.getManageUnitCode().trim().toUpperCase());
			sql.append(" 	connect by prior shop_id = parent_shop_id ");
			sql.append(" ) ");
		}
		
			sql.append(" and exists ( ");
			sql.append(" 	select 1 ");
			sql.append(" 	from staff_type ste ");
			sql.append(" 	where 1=1 ");
			sql.append(" 	and s.staff_type_id = ste.staff_type_id ");
			sql.append(" 	and ste.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			if (!StringUtility.isNullOrEmpty(unitFilter.getUnitType())) {
				sql.append(" 	and ste.name = ? ");
				params.add(unitFilter.getUnitType());
			}
			sql.append(" ) ");
		
		StringBuilder countSql = new StringBuilder("select count(1) as count from (");
		countSql.append(sql);
		countSql.append(")");
		
		if (!StringUtility.isNullOrEmpty(unitFilter.getSortField()) && !StringUtility.isNullOrEmpty(unitFilter.getSortOrder())) {
			//sql.append(" order by s." + unitFilter.transformSortFieldToDBTableColumn() + " " + unitFilter.getSortOrder());
			String sortField = null;
			switch (unitFilter.getSortField()) {
			case "staffCode":
				sortField = "staffCode";
				break;
			case "staffName":
				sortField = "staffName";
				break;
			case "phoneMobile":
				sortField = "phone";
				break;
			case "status":
				sortField = "status";
				break;
			case "phone":
				sortField = "phone";
				break;
			case "mobilePhone":
				sortField = "mobilephone";
				break;
			case "shopName":
				sortField = "shopName";
				break;
			case "staffType":
				sortField = "staffType";
				break;
			default:
				sortField = null;
				break;
			}
			
			if (sortField != null) {
				if (sortField.equals("shop_name")) {
					sql.append(" order by (select shop_name from shop where shop_id = s.shop_id) ").append(unitFilter.getSortOrder());
				} else if (sortField.equals("staff_type")) {
					sql.append(" order by (select name from staff_type where staff_type_id = s.staff_type_id) ").append(unitFilter.getSortOrder());
				} else {
					sql.append(" order by ").append(sortField).append(" ").append(unitFilter.getSortOrder());
				}
			}
		}
		
		final String[] fieldNames = new String[] {
			"staffId",
			"staffCode",
			"staffName",	// 3
			"status",
			"shopName",
			"staffType",	// 6
			"phone",
			"mobilePhone",
			"shopId"		// 9
		};

		final Type[] fieldTypes = new Type[] { 
			StandardBasicTypes.LONG, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING, //3 
			StandardBasicTypes.INTEGER, 
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING, //6
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.LONG
		};
		
		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
		return repo.getListBySQL(StaffVO.class, sql.toString(), params);
	}

	@Override
	public List<ImageVO> getListGSByNPP(Long shopId, ImageFilter filter) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("method:getListCTTBByNPP: id is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH lstshop AS ");
		sql.append(" ( ");
		sql.append(" SELECT DISTINCT shop_id, shop_code ");
		sql.append(" FROM shop ");
		sql.append(" where status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" START WITH shop_id = ? and status = ? ");
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" CONNECT BY prior shop_id = parent_shop_id ");
		sql.append(" union all ");
		sql.append(" (SELECT DISTINCT shop_id, shop_code ");
		sql.append(" FROM shop ");
		sql.append(" where status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and shop_id <> ? ");
		params.add(shopId);
		sql.append(" START WITH shop_id = ? and status = ? ");
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" CONNECT BY prior parent_shop_id = shop_id) ");
		sql.append(" ) ");
		sql.append(" select distinct s.staff_id as gsId, s.staff_code as gsCode, s.staff_name as gsName from map_user_staff mus ");
		sql.append(" join staff s on s.staff_id = mus.user_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and mus.USER_SPECIFIC_TYPE = 2 ");
		sql.append(" and mus.shop_id in (select shop_id from lstshop) ");				
		if (!StringUtility.isNullOrEmpty(filter.getShopIdListStr())) {
			 String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getShopIdListStr(), "mus.shop_id");
			 sql.append(paramsFix);
		}
		sql.append(" order by gsId, gsCode, gsName ");
		
		String[] fieldNames = { "gsId",//1
								"gsCode",//2
								"gsName"//3
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 1
							  StandardBasicTypes.STRING, //2
							  StandardBasicTypes.STRING //2
		};
		return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ImageVO> getListNVBHByNPP(Long shopId, ImageFilter filter) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("method:getListCTTBByNPP: id is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH lstshop AS ");
		sql.append(" ( ");
		sql.append(" SELECT DISTINCT shop_id, shop_code ");
		sql.append(" FROM shop ");
		sql.append(" where status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" START WITH shop_id = ? and status = ? ");
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" CONNECT BY prior shop_id = parent_shop_id ");
		sql.append(" union all ");
		sql.append(" (SELECT DISTINCT shop_id, shop_code ");
		sql.append(" FROM shop ");
		sql.append(" where status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and shop_id <> ? ");
		params.add(shopId);
		sql.append(" START WITH shop_id = ? and status = ? ");
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" CONNECT BY prior parent_shop_id = shop_id) ");
		sql.append(" ) ");
		sql.append(" select distinct s.staff_id as nvbhId, s.staff_code as nvbhCode, s.staff_name as nvbhName from staff s  ");
		sql.append(" join STAFF_TYPE st  on s.STAFF_TYPE_ID = st.STAFF_TYPE_ID ");
		sql.append(" where 1 = 1 ");
		sql.append(" and st.SPECIFIC_TYPE = 1 ");
		sql.append(" and s.shop_id in (select shop_id from lstshop) ");				
		if (!StringUtility.isNullOrEmpty(filter.getShopIdListStr())) {
			 String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getShopIdListStr(), "s.shop_id");
			 sql.append(paramsFix);
		}
		sql.append(" order by nvbhId, nvbhCode, nvbhName ");
		
		String[] fieldNames = { "nvbhId",//1
								"nvbhCode",//2
								"nvbhName"//3
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 1
							  StandardBasicTypes.STRING, //2
							  StandardBasicTypes.STRING //2
		};
		return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<StaffVO> getListGsById(List<Long> lstGsId) throws DataAccessException {		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select staff_name as gsName from staff where 1 = 1 and staff_id in (");	
		for(int i=0;i<lstGsId.size();i++){
			if(i==0) sql.append(" ? ");
			else sql.append(" ,? ");
			params.add(lstGsId.get(i));
		}		
		sql.append(" )");	
		sql.append(" order by gsName ");		
		String[] fieldNames = {
								"gsName"//3
		};

		Type[] fieldTypes = { 
							  StandardBasicTypes.STRING
		};
		return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ImageVO> getListNVBHByNPPShop(List<Long> lstShopId, ImageFilter filter) throws DataAccessException {
		if (lstShopId == null || lstShopId.size() <= 0) {
			throw new IllegalArgumentException("method:getListNVBHByNPPShop: id is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH lstshop AS ");
		sql.append(" ( ");
		sql.append(" SELECT DISTINCT shop_id, shop_code ");
		sql.append(" FROM shop ");
		sql.append(" where status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" START WITH shop_id in (");
		if(lstShopId!=null && lstShopId.size()>0){
			 for(int i=0;i<lstShopId.size();i++){
				 if(i==0) sql.append("?");
				 else sql.append(",?");
				 params.add(lstShopId.get(i));
			 }
		 }
	//	params.add(shopId);
		sql.append(" ) and status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" CONNECT BY prior shop_id = parent_shop_id ");
		sql.append(" union all ");
		sql.append(" (SELECT DISTINCT shop_id, shop_code ");
		sql.append(" FROM shop ");
		sql.append(" where status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and shop_id not in ( ");
		if(lstShopId!=null && lstShopId.size()>0){
			 for(int i=0;i<lstShopId.size();i++){
				 if(i==0) sql.append("?");
				 else sql.append(",?");
				 params.add(lstShopId.get(i));
			 }
		 }
	//	params.add(shopId);
		sql.append(")");
		sql.append(" START WITH shop_id in ( ");
		if(lstShopId!=null && lstShopId.size()>0){
			 for(int i=0;i<lstShopId.size();i++){
				 if(i==0) sql.append("?");
				 else sql.append(",?");
				 params.add(lstShopId.get(i));
			 }
		 }
		sql.append(" ) and status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" CONNECT BY prior parent_shop_id = shop_id) ");
		sql.append(" ) ");
		sql.append(" select distinct s.staff_id as nvbhId, s.staff_code as nvbhCode, s.staff_name as nvbhName from staff s  ");
		sql.append(" join STAFF_TYPE st  on s.STAFF_TYPE_ID = st.STAFF_TYPE_ID ");
		sql.append(" where 1 = 1 ");
		sql.append(" and st.SPECIFIC_TYPE = 1 ");
		sql.append(" and s.shop_id in (select shop_id from lstshop) ");				
		if (!StringUtility.isNullOrEmpty(filter.getShopIdListStr())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getShopIdListStr(), "s.shop_id");
			sql.append(paramsFix);
		}
		sql.append(" order by nvbhId, nvbhCode, nvbhName ");
		
		String[] fieldNames = { "nvbhId",//1
								"nvbhCode",//2
								"nvbhName"//3
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 1
							  StandardBasicTypes.STRING, //2
							  StandardBasicTypes.STRING //2
		};
		return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<StaffVO> listEquipCategoryCode(StaffFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select staff_id as staffId from staff_sale_cat where  1 = 1");	
		if (filter.getCatId() != null) {
			sql.append(" and cat_id = ?");
			 params.add(filter.getCatId());
		}
		if (filter.getStaffId() != null) {
			sql.append(" and staff_id = ? ");
			 params.add(filter.getStaffId());
		}
		final String[] fieldNames =   { "staffId", };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, };
		return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<StaffVO> listCheckSubStaff(StaffSaleCat filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select staff_id as staffId from staff_sale_cat ");
		sql.append(" where 1 = 1 ");
		if (filter.getCat().getId() != null) {
			sql.append(" and cat_id = ? ");
			params.add(filter.getCat().getId());
		}
		if (filter.getStaff() != null) {
			sql.append(" and staff_id = ? ");
			params.add(filter.getStaff().getId());
		}
		final String[] fieldNames = new String[] { "staffId", };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, };
		return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<StaffVO> listCheckDelSubStaff(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select staff_id as staffId, cat_id as catId, staff_sale_cat_id as id from staff_sale_cat ");
		sql.append(" where 1 = 1 ");
		if (id != null) {
			sql.append(" and staff_id = ? ");
			params.add(id);
		}
		final String[] fieldNames = new String[] { "staffId",
				"catId",
				"id",
				};
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG };
		return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<StaffExportVO> getListStaffWithDynamicAttribute(StaffFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null.");
		}
		
		if (filter.getStaffRootId() == null || filter.getRoleId() == null || filter.getShopRootId() == null) {
			throw new IllegalArgumentException("priviledge data is null");
		}
		
		StringBuilder sql = new StringBuilder();
		sql.append("call TUANND.STAFF_DYNAMIC_ATTRIBUTE(?, :shopRootId, :roleId, :staffRootId, :shopId, :staffStatus, :staffTypeName, :staffCode, :staffName)");
		
		List<Object> params = new ArrayList<Object>();
		int paramIndex = 2;
		params.add(paramIndex++);
		params.add(filter.getStaffRootId());
		params.add(java.sql.Types.NUMERIC);
		
		params.add(paramIndex++);
		params.add(filter.getRoleId());
		params.add(java.sql.Types.NUMERIC);
		
		params.add(paramIndex++);
		params.add(filter.getShopRootId());
		params.add(java.sql.Types.NUMERIC);
		
		params.add(paramIndex++);
		params.add(filter.getShopId());
		params.add(java.sql.Types.NUMERIC);
		
		params.add(paramIndex++);
		params.add(filter.getStatus() != null ? filter.getStatus().getValue() : null);
		params.add(java.sql.Types.INTEGER);
		
		params.add(paramIndex++);
		params.add(filter.getStaffTypeName());
		params.add(java.sql.Types.VARCHAR);
		
		params.add(paramIndex++);
		params.add(StringUtility.isNullOrEmpty(filter.getStaffCode()) ? null : filter.getStaffCode().toUpperCase());
		params.add(java.sql.Types.VARCHAR);
		
		params.add(paramIndex++);
		params.add(StringUtility.isNullOrEmpty(filter.getStaffName()) ? null : Unicode2English.codau2khongdau(filter.getStaffName().toUpperCase()));
		params.add(java.sql.Types.VARCHAR);
		
		return repo.getListByQueryDynamicFromPackage(StaffExportVO.class, sql.toString(), params, null);
	}
	
	/**
	 * @author trietptm
	 */
	@Override
	public List<CustomerVO> getListCustomerHasVisitPlan(Long shopId, Long staffId, Integer isCustomerWaiting, Date checkDate, String fromTime, String toTime) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with saleOrder as (                ");
		sql.append("  	select so.*               ");
		sql.append("    from sale_order so             ");
		sql.append("    where so.order_source = ? and  so.staff_id = ?             ");
		/** Select saleorder from tablet */
		params.add(SaleOrderSource.TABLET.getValue());
		params.add(staffId);
		sql.append("          and so.shop_id = ?       ");
		params.add(shopId);
		sql.append("          and so.order_date >= trunc(?) and so.order_date < trunc(?) + 1       ");
		params.add(checkDate);
		params.add(checkDate);
		sql.append("          and so.order_date = ( ");
		sql.append("  				select min(s.order_date)               ");
		sql.append("    			from sale_order s             ");
		sql.append("    			where  s.order_source = ? and s.staff_id = so.staff_id             ");
		params.add(SaleOrderSource.TABLET.getValue());
		sql.append("          			and s.shop_id = so.shop_id and s.customer_id = so.customer_id       ");
		sql.append("          			and s.order_date >= trunc(?) and s.order_date < trunc(?) + 1       ");
		params.add(checkDate);
		params.add(checkDate);
		sql.append(" 		)                ");
		sql.append(" )                ");
		sql.append(" select * from ( ");
		sql.append(" SELECT distinct (c.customer_id) AS id, ");
		sql.append(" c.customer_code AS customerCode, ");
		sql.append(" c.customer_name AS customerName, ");
		sql.append(" c.ADDRESS AS address, ");
		sql.append(" c.MOBIPHONE AS mobiphone, ");
		sql.append(" so.total as amount, ");
		sql.append(" (CASE TO_CHAR((?),'D') ");
		params.add(checkDate);
		sql.append(" WHEN '2' THEN rc.SEQ2 ");
		sql.append(" WHEN '3' THEN rc.SEQ3 ");
		sql.append(" WHEN '4' THEN rc.SEQ4 ");
		sql.append(" WHEN '5' THEN rc.SEQ5 ");
		sql.append(" WHEN '6' THEN rc.SEQ6 ");
		sql.append(" WHEN '7' THEN rc.SEQ7 ");
		sql.append(" WHEN '1' THEN rc.SEQ8 ");
		sql.append(" ELSE NULL ");
		sql.append(" END ) AS ordinalVisit, al.object_type as objectType, al.start_time as startTime, al.end_time as endTime, c.lat, c.lng,al.is_or as isOr, ");
		sql.append(" (select count(customer_id) as count from action_log al ");
		sql.append(" where START_TIME >= TRUNC(?) ");
		params.add(checkDate);
		sql.append(" and START_TIME < TRUNC(?) + 1 ");
		params.add(checkDate);
		if (!StringUtility.isNullOrEmpty(fromTime) && !StringUtility.isNullOrEmpty(toTime)) {
			sql.append(" AND TO_CHAR(START_TIME, 'HH24:MI') >= ? ");
			params.add(fromTime);
			sql.append(" AND TO_CHAR(START_TIME, 'HH24:MI') <= ? ");
			params.add(toTime);
		}
		sql.append(" and (object_type=4) and customer_id = rc.customer_id and staff_id = vp.staff_id )AS count ");
		sql.append(" FROM customer c ");
		sql.append(" JOIN ROUTING_CUSTOMER rc ON rc.customer_id = c.customer_id ");
		sql.append(" join ROUTING r on r.ROUTING_ID = rc.ROUTING_ID and r.status in (0,1) ");
		sql.append(" join VISIT_PLAN vp on vp.ROUTING_ID = r.ROUTING_ID ");
		sql.append(" left join action_log al on vp.staff_id = al.staff_id ");
		sql.append(" and al.customer_id = rc.customer_id AND al.start_time >= TRUNC(?) ");
		params.add(checkDate);
		sql.append(" and al.start_time < TRUNC(?) + 1 ");
		params.add(checkDate);
		if (!StringUtility.isNullOrEmpty(fromTime) && !StringUtility.isNullOrEmpty(toTime)) {
			sql.append(" AND TO_CHAR(al.start_time, 'HH24:MI') >= ? ");
			params.add(fromTime);
			sql.append(" AND TO_CHAR(al.start_time, 'HH24:MI') < ? ");
			params.add(toTime);
		}
		sql.append(" left join saleOrder so on so.shop_id = c.shop_id and so.staff_id = vp.staff_id and so.customer_id = c.customer_id             ");
		sql.append(" WHERE 1=1 ");
		/**
		 * lochp fix bug vẽ lộ trình của KH đã bị off
		 */
		if (isCustomerWaiting == null) {
			sql.append(" and c.status in (1, 0)  ");
		} else {
			if (isCustomerWaiting <= 0) {
				sql.append(" and c.status in (0, 1, 2)  ");
			} else {
				sql.append(" and c.status in (0, 1)  ");
			}
		}
		sql.append(" AND vp.status in (0,1) AND rc.status in (0,1) ");
		sql.append(" and c.shop_id = ? and rc.shop_id= ? and r.shop_id = ? and vp.shop_id = ? and (al.shop_id is null or al.shop_id = ?)  ");
		params.add(shopId);
		params.add(shopId);
		params.add(shopId);
		params.add(shopId);
		params.add(shopId);
		sql.append(" AND( ( rc.WEEK1 IS NULL ");
		sql.append(" AND rc.WEEK2 IS NULL ");
		sql.append(" AND rc.WEEK3 IS NULL ");
		sql.append(" AND rc.WEEK4 IS NULL) ");
		sql.append(" OR ( ( MOD( ( ( floor((TRUNC(?,'dd') - TRUNC(rc.start_date,'dd'))/7)) + ( ");
		params.add(checkDate);
		sql.append(" CASE ");
		sql.append(" WHEN (mod(TRUNC(?,'dd')- TRUNC(rc.start_date,'dd'),7) > 0) ");
		params.add(checkDate);
		sql.append(" AND ( ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR(?,'D') = '1' ");
		params.add(checkDate);
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR(?, 'D')) ");
		params.add(checkDate);
		sql.append(" END ) < ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR( rc.start_date,'D') = '1' ");
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR( rc.start_date,'D')) ");
		sql.append(" END ) ) ");
		sql.append(" THEN 1 ");
		sql.append(" ELSE 0 ");
		sql.append(" END) ),4) + 1 ) = 1 ");
		sql.append(" AND week1 =1 ) ");
		sql.append(" OR ( ( MOD( ( ( floor((TRUNC(?,'dd') - TRUNC(rc.start_date,'dd'))/7)) + ( ");
		params.add(checkDate);
		sql.append(" CASE ");
		sql.append(" WHEN (mod(TRUNC(?,'dd')- TRUNC(rc.start_date,'dd'),7) > 0) ");
		params.add(checkDate);
		sql.append(" AND ( ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR(?,'D') = '1' ");
		params.add(checkDate);
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR(?, 'D')) ");
		params.add(checkDate);
		sql.append(" END ) < ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR( rc.start_date,'D') = '1' ");
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR( rc.start_date,'D')) ");
		sql.append(" END ) ) ");
		sql.append(" THEN 1 ");
		sql.append(" ELSE 0 ");
		sql.append(" END) ),4) + 1 ) = 2 ");
		sql.append(" AND week2 =1) ");
		sql.append(" OR ( ( MOD( ( ( floor((TRUNC(?,'dd') - TRUNC(rc.start_date,'dd'))/7)) + ( ");
		params.add(checkDate);
		sql.append(" CASE ");
		sql.append(" WHEN (mod(TRUNC(?,'dd')- TRUNC(rc.start_date,'dd'),7) > 0) ");
		params.add(checkDate);
		sql.append(" AND ( ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR(?,'D') = '1' ");
		params.add(checkDate);
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR(?, 'D')) ");
		params.add(checkDate);
		sql.append(" END ) < ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR( rc.start_date,'D') = '1' ");
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR( rc.start_date,'D')) ");
		sql.append(" END ) ) ");
		sql.append(" THEN 1 ");
		sql.append(" ELSE 0 ");
		sql.append(" END) ),4) + 1 ) = 3 ");
		sql.append(" AND week3 =1) ");
		sql.append(" OR ( ( MOD( ( ( floor((TRUNC(?,'dd') - TRUNC(rc.start_date,'dd'))/7)) + ( ");
		params.add(checkDate);
		sql.append(" CASE ");
		sql.append(" WHEN (mod(TRUNC(?,'dd')- TRUNC(rc.start_date,'dd'),7) > 0) ");
		params.add(checkDate);
		sql.append(" AND ( ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR(?,'D') = '1' ");
		params.add(checkDate);
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR(?, 'D')) ");
		params.add(checkDate);
		sql.append(" END ) < ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR( rc.start_date,'D') = '1' ");
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR( rc.start_date,'D')) ");
		sql.append(" END ) ) ");
		sql.append(" THEN 1 ");
		sql.append(" ELSE 0 ");
		sql.append(" END) ),4) + 1 ) = 4 ");
		sql.append(" AND week4 =1) ) ");
		sql.append(" and vp.staff_id = ? ");
		params.add(staffId);
		sql.append(" and trunc(vp.from_date) <= trunc(?) ");
		sql.append(" and ( trunc(vp.to_date) >= trunc(?) or vp.to_date is null) ");
		sql.append(" and trunc(rc.start_date) <= trunc(?) ");
		sql.append(" and ( trunc(rc.end_date) >= trunc(?) or rc.end_date is null) ");
		params.add(checkDate);
		params.add(checkDate);
		params.add(checkDate);
		params.add(checkDate);
		sql.append(" AND ( (TO_CHAR((?),'D')=1 AND SUNDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =2 AND MONDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =3 AND TUESDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =4 AND WEDNESDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =5 AND THURSDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =6 AND FRIDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =7 AND SATURDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D')=1 AND SUNDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =2 AND MONDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =3 AND TUESDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =4 AND WEDNESDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =5 AND THURSDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =6 AND FRIDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =7 AND SATURDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" ) ");
		sql.append(" order by al.start_time ) ");
		sql.append(" where objecttype is null or objecttype <> 4 ");
		sql.append("  ");

		final String[] fieldNames = new String[] { "id", //1
				"customerCode",//2
				"customerName",//3
				"address",//6				
				"mobiphone",//7
				"amount",//10
				"ordinalVisit",//11
				"objectType",//12
				"startTime", "endTime",//13
				"lat",//14
				"lng",//15
				"isOr",//16
				"count" };//17
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.BIG_DECIMAL,//10
				StandardBasicTypes.INTEGER, //11
				StandardBasicTypes.INTEGER, //12
				StandardBasicTypes.TIMESTAMP,//13 
				StandardBasicTypes.TIMESTAMP,//13 
				StandardBasicTypes.STRING, //14
				StandardBasicTypes.STRING,//15
				StandardBasicTypes.INTEGER,//16
				StandardBasicTypes.INTEGER };//17
		List<CustomerVO> vos = repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return vos;
	}
	
	@Override
	public List<StaffVO> getListStaffVOInherit(KPaging<StaffVO> kPaging, UnitFilter unitFilter) throws DataAccessException {
		if (unitFilter == null) {
			throw new IllegalArgumentException("unitFilter must not null");
		}
		if (unitFilter.getStaffRootId() == null || unitFilter.getRoleId() == null || unitFilter.getShopRootId() == null) {
			throw new IllegalArgumentException("priviledge data is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select ");
		sql.append(" 	s.staff_id staffId, ");
		sql.append(" 	s.staff_code staffCode, ");
		sql.append(" 	s.staff_name staffName, ");
		sql.append("	s.shop_id shopId ");
		sql.append(" from staff s ");
		if (Boolean.TRUE.equals(unitFilter.getIsCurrentUser())) {
			sql.append(" join ( ");
			sql.append("    select number_key_id as staff_id from table (f_get_list_child_staff_inherit(?, sysdate, ?, ?, null)) ");
			sql.append("     union ");
			sql.append("    select ? from dual ");
			sql.append(" ) privStaff on s.staff_id = privStaff.staff_id ");
			params.add(unitFilter.getStaffRootId());
			params.add(unitFilter.getRoleId());
			params.add(unitFilter.getShopRootId());
			// them current user hien tai
			params.add(unitFilter.getStaffRootId());
		} else {
			sql.append(" join ( ");
			sql.append("     select number_key_id as staff_id from table (F_GET_LIST_CHILD_STAFF_INHERIT(?, sysdate, ?, ?, null)) ");
			sql.append(" ) privStaff on s.staff_id = privStaff.staff_id ");
			params.add(unitFilter.getStaffRootId());
			params.add(unitFilter.getRoleId());
			params.add(unitFilter.getShopRootId());
		}
		sql.append(" where 1=1 ");
		if (unitFilter.getStatus() != null) {
			sql.append(" and s.status = ? ");
			params.add(unitFilter.getStatus().getValue());
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getUnitCode())) {
			sql.append(" and s.staff_code like ? ");
			params.add(StringUtility.toOracleSearchLike(unitFilter.getUnitCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getUnitName())) {
			sql.append(" and lower(s.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(unitFilter.getUnitName().trim().toLowerCase())));
		}
		if (unitFilter.getManageUnitId() != null) {
			sql.append(" and exists ( ");
			sql.append(" 	select 1 ");
			sql.append(" 	from shop ");
			sql.append(" 	where 1=1 ");
			sql.append(" 	and shop_id = s.shop_id ");
			sql.append(" 	and status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" 	start with shop_id = ? ");
			params.add(unitFilter.getManageUnitId());
			sql.append(" 	connect by prior shop_id = parent_shop_id ");
			sql.append(" ) ");
		}
		if (unitFilter.getLstStaffType() != null) {
			sql.append(" and exists ( ");
			sql.append(" 	select 1 ");
			sql.append(" 	from staff_type ste ");
			sql.append(" 	where 1=1 ");
			sql.append(" 	and s.staff_type_id = ste.staff_type_id ");
			sql.append(" 	and ste.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" 	and ste.specific_type in (? ");
			params.add(unitFilter.getLstStaffType().get(0));
			for (int i = 1, sz = unitFilter.getLstStaffType().size(); i < sz; i++) {
				sql.append(",? ");
				params.add(unitFilter.getLstStaffType().get(i));
			}
			sql.append(" )");
			sql.append(") ");
		}
		StringBuilder countSql = new StringBuilder("select count(1) as count from (");
		countSql.append(sql);
		countSql.append(")");
		sql.append(" order by staffCode");
		final String[] fieldNames = new String[] {
			"staffId",
			"staffCode",
			"staffName",
			"shopId"
		};

		final Type[] fieldTypes = new Type[] { 
			StandardBasicTypes.LONG, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.LONG
		};
		
		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
		return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<StaffType> getListAllStaffType(StaffTypeFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter must not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select stt.* from staff_type stt ");
		sql.append(" where 1 = 1 ");
		if (filter.getId() != null) {
			sql.append(" and stt.staff_type_id = ? ");
			params.add(filter.getId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			sql.append(" and unicode2english(stt.name) = ? ");
			params.add(Unicode2English.codau2khongdau(filter.getName().trim()).toLowerCase());
		}
		if (filter.getStatus() != null) {
			sql.append(" and stt.status = ? ");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and stt.status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" order by stt.name ");
		return repo.getListBySQL(StaffType.class, sql.toString(), params);
	}
	
	@Override
	public List<StaffType> getListStaffType(UnitFilter unitFilter) throws DataAccessException {
		if (unitFilter == null) {
			throw new IllegalArgumentException("unitFilter must not null");
		}
		if (unitFilter.getStaffRootId() == null || unitFilter.getRoleId() == null || unitFilter.getShopRootId() == null) {
			throw new IllegalArgumentException("priviledge data is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct stype.* from staff st ");
		if (Boolean.TRUE.equals(unitFilter.getIsCurrentUser())) {
			sql.append(" join (select * from table (f_get_list_child_staff_inherit(?, sysdate, ?, ?, null))  ");
			sql.append(" 		union select ?, ? ,'' from dual");
			sql.append("     ) tmp on tmp.number_key_id = st.staff_id");
			params.add(unitFilter.getStaffRootId());
			params.add(unitFilter.getRoleId());
			params.add(unitFilter.getShopRootId());
			// union voi chinh user dang nhap
			params.add(unitFilter.getStaffRootId());
			params.add(unitFilter.getShopRootId());
		} else {
			sql.append(" join table (f_get_list_child_staff_inherit(?, sysdate, ?, ?, null)) tmp on tmp.number_key_id = st.staff_id ");
			params.add(unitFilter.getStaffRootId());
			params.add(unitFilter.getRoleId());
			params.add(unitFilter.getShopRootId());
		}
		sql.append(" join staff_type stype on stype.staff_type_id = st.staff_type_id ");
		if (unitFilter.getManageUnitId() != null) {
			sql.append(" join (SELECT shop_id ");
			sql.append(" from shop ");
			sql.append(" start with shop_id = ? ");
			sql.append(" connect by prior shop_id = parent_shop_id ");
			sql.append(" ) sh on sh.shop_id = st.shop_id ");
			params.add(unitFilter.getManageUnitId());
		}
		sql.append(" order by stype.name ");
		return repo.getListBySQL(StaffType.class, sql.toString(), params);
	}
	
	@Override
	public List<StaffVO> getListStaffVOFeedbackByFilter(StaffFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter must not null");
		}
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (filter.getStaffRootId() == null || filter.getRoleId() == null || filter.getShopRootId() == null) {
			throw new IllegalArgumentException("priviledge data is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select st.staff_id as id, ");
		sql.append(" st.staff_code as staffCode, ");
		sql.append(" st.staff_name as staffName, ");
		sql.append(" stype.specific_type as objectType, ");
		sql.append(" sh.shop_id as shopId, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName ");
		sql.append(" from staff st ");
		if (Boolean.TRUE.equals(filter.getIsCurrentUser())) {
			sql.append(" join (select * from table (f_get_list_child_staff_inherit(?, sysdate, ?, ?, null))  ");
			sql.append(" 		union select ?, ? ,'' from dual");
			sql.append("     ) tmp on tmp.number_key_id = st.staff_id");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
			// union voi chinh user dang nhap
			params.add(filter.getStaffRootId());
			params.add(filter.getShopRootId());
		} else {
			sql.append(" join table (f_get_list_child_staff_inherit(?, sysdate, ?, ?, null)) tmp on tmp.number_key_id = st.staff_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		sql.append(" join (SELECT shop_id, shop_code, shop_name ");
		sql.append(" from shop ");
		sql.append(" start with shop_id = ? ");
		params.add(filter.getShopId());
		sql.append(" connect by prior shop_id = parent_shop_id ");
		sql.append(" ) sh on sh.shop_id = st.shop_id ");
		sql.append(" join staff_type stype on stype.staff_type_id = st.staff_type_id ");
		sql.append(" where st.status = 1 ");
		if (filter.getTypeId() != null) {
			sql.append(" and stype.staff_type_id = ? ");
			params.add(filter.getTypeId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and (st.staff_code like ? escape '/' ");
			sql.append("  or lower(st.name_text) like ? escape '/' ) ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getStaffCode().trim()).toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getStaffCode().trim()).toLowerCase()));
		}
		if (filter.getLstNotInId() != null && filter.getLstNotInId().size() > 0) {
			sql.append(" and st.staff_id not in (-1 ");
			for (int i = 0, sz = filter.getLstNotInId().size(); i < sz; i++) {
				sql.append(", ? ");
				params.add(filter.getLstNotInId().get(i));
			}
			sql.append(" ) ");
		}
		StringBuilder countSql = new StringBuilder("select count(1) as count from (");
		countSql.append(sql);
		countSql.append(")");
		sql.append(" order by shopcode, staffcode ");
		final String[] fieldNames = new String[] {
			"id",
			"staffCode",
			"staffName",
			"objectType",
			"shopId",
			"shopCode",
			"shopName",
		};

		final Type[] fieldTypes = new Type[] { 
			StandardBasicTypes.LONG, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING,
			StandardBasicTypes.INTEGER,
			StandardBasicTypes.LONG,
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING,
		};
		
		if (filter.getStaffVOPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getStaffVOPaging());
		}
		return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<StaffVO> getListStaffVOUserMapStaffByFilter(StaffFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter must not null");
		}
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (filter.getStaffRootId() == null || filter.getRoleId() == null || filter.getShopRootId() == null) {
			throw new IllegalArgumentException("priviledge data is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select st.staff_id as id, ");
		sql.append(" st.staff_code as staffCode, ");
		sql.append(" st.staff_name as staffName, ");
		sql.append(" stype.specific_type as objectType, ");
		sql.append(" sh.shop_id as shopId, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName ");
		sql.append(" from staff st ");
		sql.append(" join shop sh on sh.shop_id = st.shop_id ");
		sql.append(" join staff_type stype on stype.staff_type_id = st.staff_type_id ");
		sql.append(" where st.status = 1 ");
		if (filter.getStaffId() != null) {
			sql.append(" and st.staff_id in ( ");
			sql.append(" select distinct mus.user_id from map_user_staff mus ");
			sql.append(" where mus.from_date < trunc(sysdate) + 1 ");
			sql.append(" and (mus.to_date is null or mus.to_date >= trunc(sysdate)) ");
			sql.append(" and mus.staff_id = ? ");
			sql.append(" ) ");
			params.add(filter.getStaffId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and (st.staff_code like ? escape '/' ");
			sql.append("  or lower(st.name_text) like ? escape '/' ) ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getStaffCode().trim()).toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getStaffCode().trim()).toLowerCase()));
		}
		if (filter.getLstNotInId() != null && filter.getLstNotInId().size() > 0) {
			sql.append(" and st.staff_id not in (-1 ");
			for (int i = 0, sz = filter.getLstNotInId().size(); i < sz; i++) {
				sql.append(", ? ");
				params.add(filter.getLstNotInId().get(i));
			}
			sql.append(" ) ");
		}
		StringBuilder countSql = new StringBuilder("select count(1) as count from (");
		countSql.append(sql);
		countSql.append(")");
		sql.append(" order by shopCode, staffCode ");
		final String[] fieldNames = new String[] {
			"id",
			"staffCode",
			"staffName",
			"objectType",
			"shopId",
			"shopCode",
			"shopName",
		};

		final Type[] fieldTypes = new Type[] { 
			StandardBasicTypes.LONG, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING,
			StandardBasicTypes.INTEGER,
			StandardBasicTypes.LONG,
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING,
		};
		
		if (filter.getStaffVOPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(StaffVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getStaffVOPaging());
		}
		return repo.getListByQueryAndScalar(StaffVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
}
package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeFilter;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PromotionCustomerMapType;
import ths.dms.core.entities.vo.ChannelTypeVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class ChannelTypeDAOImpl implements ChannelTypeDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public ChannelType createChannelType(ChannelType channelType, LogInfoVO logInfo) throws DataAccessException {
		if (channelType == null) {
			throw new IllegalArgumentException("channelType");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo");
		}
		if (channelType.getChannelTypeCode() != null)
			channelType.setChannelTypeCode(channelType.getChannelTypeCode().toUpperCase());
		repo.create(channelType);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, channelType, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(ChannelType.class, channelType.getId());
	}

	@Override
	public void deleteChannelType(ChannelType channelType, LogInfoVO logInfo) throws DataAccessException {
		if (channelType == null) {
			throw new IllegalArgumentException("channelType");
		}
		repo.delete(channelType);
		try {
			actionGeneralLogDAO.createActionGeneralLog(channelType, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public ChannelType getChannelTypeById(long id) throws DataAccessException {
		return repo.getEntityById(ChannelType.class, id);
	}

	@Override
	public void updateChannelType(ChannelType channelType, LogInfoVO logInfo) throws DataAccessException {
		if (channelType == null) {
			throw new IllegalArgumentException("channelType");
		}
		if (channelType.getChannelTypeCode() != null)
			channelType.setChannelTypeCode(channelType.getChannelTypeCode().toUpperCase());

		ChannelType temp = this.getChannelTypeById(channelType.getId());

		ChannelType temp1 = new ChannelType();
		temp1.setChannelTypeCode(temp.getChannelTypeCode());
		temp1.setChannelTypeName(temp.getChannelTypeName());
		temp1.setId(temp.getId());
		temp1.setIsEdit(temp.getIsEdit());
		temp1.setObjectType(temp.getObjectType());
		temp1.setParentChannelType(temp.getParentChannelType());
		temp1.setSaleAmount(temp.getSaleAmount());
		temp1.setSku(temp.getSku());
		temp1.setStatus(temp.getStatus());
		temp1.setType(temp.getType());
		temp = temp1;

		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, channelType, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		repo.update(channelType);
	}

	@Override
	public ChannelType getChannelTypeByCode(String code, ChannelTypeType type) throws DataAccessException {
		String sql = "select * from CHANNEL_TYPE where channel_type_code=? and type=? and status != ?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(type.getValue());
		params.add(ActiveType.DELETED.getValue());
		ChannelType pi = repo.getEntityBySQL(ChannelType.class, sql, params);
		return pi;
	}

	@Override
	public List<ChannelType> getListChannelTypeOrderById(KPaging<ChannelType> kPaging, String channelTypeCode, String channelTypeName, Long parentId, ChannelTypeType type, ActiveType status, Integer isEdit, Integer objectType)
			throws DataAccessException {
		return this.getListChannelTypeOrderById(kPaging, channelTypeCode, channelTypeName, parentId, type, status, isEdit, null, null, objectType, null);
	}

	@Override
	public List<ChannelType> getListChannelType(ChannelTypeFilter filter, KPaging<ChannelType> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select a.* from CHANNEL_TYPE a ");
		sql.append("  left join  channel_type ct on  a.parent_channel_type_id = ct.channel_type_id where 1 = 1  ");
		if (!StringUtility.isNullOrEmpty(filter.getChannelTypeCode())) {
			sql.append(" and a.channel_type_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getChannelTypeCode().toUpperCase().trim()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getChannelTypeName())) {
			sql.append(" and unicode2english(a.channel_type_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getChannelTypeName().toLowerCase().trim())));
		}
		if (filter.getParentId() != null) {
			if (Boolean.TRUE.equals(filter.getIsGetOneChildLevel())) {
				sql.append(" and (a.parent_channel_type_id=?");
			} else {
				if (Boolean.TRUE.equals(filter.getIsGetParentAndChild())) {
					sql.append(" and (a.channel_type_id in (select channel_type_id from channel_type start with channel_type_id = ? connect by prior channel_type_id = parent_channel_type_id)");
				} else {
					sql.append(" and (a.channel_type_id in (select channel_type_id from channel_type where channel_type_id != ? start with channel_type_id = ? connect by prior channel_type_id = parent_channel_type_id)");
					params.add(filter.getParentId());
				}
			}
			if (Boolean.TRUE.equals(filter.getIsGetParent())) {
				sql.append(" or channel_type_id = ?)");
				params.add(filter.getParentId());
			} else {
				sql.append(")");
			}

			params.add(filter.getParentId());
		}
		if (filter.getType() != null) {
			sql.append(" and a.type=?");
			params.add(filter.getType().getValue());
		}
		if (filter.getStatus() != null) {
			sql.append(" and a.status=?");
			params.add(filter.getStatus().getValue());

		} else {
			sql.append(" and a.status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getIsEdit() != null) {
			sql.append(" and a.is_edit=?");
			params.add(filter.getIsEdit());
		}
		if (filter.getSku() != null) {
			if (!BigDecimal.valueOf(-1).equals(filter.getSku())) {
				sql.append(" and a.sku = ?");
				params.add(filter.getSku());
			} else
				sql.append(" and a.status is not null");
		}

		if (filter.getSaleAmount() != null) {
			if (!BigDecimal.valueOf(-1).equals(filter.getSaleAmount())) {
				sql.append(" and a.sale_amount = ?");
				params.add(filter.getSaleAmount());
			} else
				sql.append(" and status is not null");
		}

		if (filter.getObjectType() != null) {
			sql.append(" and a.object_type = ?");
			params.add(filter.getObjectType());
		}
		if (filter.getLstObjectType() != null && filter.getLstObjectType().size() > 0 && filter.getLstObjectType().get(0) != null) {
			sql.append(" and (1=0 ");
			for (Integer ot : filter.getLstObjectType()) {
				sql.append(" or a.object_type = ?");
				params.add(ot);
			}
			sql.append(" )");
		}

		if (filter.getIsOrderByChannelTypeName() != null && filter.getIsOrderByChannelTypeName()) {
			sql.append(" order by a.channel_type_name");
		} else if (!StringUtility.isNullOrEmpty(filter.getSort()) && !StringUtility.isNullOrEmpty(filter.getOrder())) {
			sql.append(" order by ");
			if ( "parentChannelType".equals(filter.getSort()) ) {
				sql.append(" ct.channel_type_code ");
			} else if ( "channelTypeCode".equals(filter.getSort()) ) {
				sql.append(" a.channel_type_code ");
			} else if ( "channelTypeName".equals(filter.getSort()) ) {
				sql.append(" a.channel_type_name ");
			} else if ( "sku".equals(filter.getSort()) ) {
				sql.append(" NVL(a.sku,0) ");
			} else if ( "saleAmount".equals(filter.getSort()) ) {
				sql.append(" NVL(a.sale_amount,0) ");
			} else if ( "status".equals(filter.getSort()) ) {
				sql.append(" a.status ");
			}
			
			if ("DESC".equals(filter.getOrder().toUpperCase())) {
				sql.append(" desc ");
			}
		} else {
			sql.append(" order by a.channel_type_code");
		}
		if (kPaging == null)
			return repo.getListBySQL(ChannelType.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ChannelType.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<ChannelType> getListChannelTypeOrderById(KPaging<ChannelType> kPaging, String channelTypeCode, String channelTypeName, Long parentId, ChannelTypeType type, ActiveType status, Integer isEdit, BigDecimal minSku,
			BigDecimal minSaleAmount, Integer objectType, Boolean isGetOneChildLevel) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from CHANNEL_TYPE where 1 = 1");

		if (!StringUtility.isNullOrEmpty(channelTypeCode)) {
			sql.append(" and channel_type_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(channelTypeCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(channelTypeName)) {
			sql.append(" and lower(channel_type_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(channelTypeName.toLowerCase()));
		}
		if (parentId != null) {
			if (Boolean.TRUE.equals(isGetOneChildLevel)) {
				sql.append(" and parent_channel_type_id=?");
			} else {
				sql.append(" and channel_type_id in (select channel_type_id from channel_type where channel_type_id != ? start with channel_type_id = ? connect by prior channel_type_id = parent_channel_type_id)");
				params.add(parentId);
			}
			params.add(parentId);
		}
		if (type != null) {
			sql.append(" and type=?");
			params.add(type.getValue());
		}
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and status!=?");
			params.add(ActiveType.DELETED.getValue());
		}

		if (isEdit != null) {
			sql.append(" and is_edit=?");
			params.add(isEdit);
		}
		if (minSku != null) {
			if (!BigDecimal.valueOf(-1).equals(minSku)) {
				sql.append(" and sku = ?");
				params.add(minSku);
			} else
				sql.append(" and status_sku is not null");
		}

		if (minSaleAmount != null) {
			if (!BigDecimal.valueOf(-1).equals(minSaleAmount)) {
				sql.append(" and sale_amount >= ?");
				params.add(minSaleAmount);
			} else
				sql.append(" and status_amount is not null");
		}
		if (objectType != null) {
			sql.append(" and object_type = ?");
			params.add(objectType);
		}

		sql.append(" order by CHANNEL_TYPE_ID");
		if (kPaging == null)
			return repo.getListBySQL(ChannelType.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ChannelType.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<ChannelType> getListSubChannelType(Long parentId, ChannelTypeType type, Boolean isOrderByName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from channel_type where status=? ");
		List<Object> params = new ArrayList<Object>();
		params.add(ActiveType.RUNNING.getValue());
		if (parentId != null) {
			sql.append(" and parent_channel_type_id=?");
			params.add(parentId);
		} else {
			sql.append(" and parent_channel_type_id is null");
		}
		if (type != null) {
			sql.append("  and type=?");
			params.add(type.getValue());
		}
		if (Boolean.TRUE.equals(isOrderByName)) {
			sql.append(" order by NLSSORT(channel_type_name,'NLS_SORT=vietnamese')");
		} else {
			sql.append(" order by channel_type_code");
		}
		return repo.getListBySQL(ChannelType.class, sql.toString(), params);
	}

	@Override
	public Boolean isUsingByOthers(long channelTypeId, ChannelTypeType type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("	select count(1) as count from channel_type where exists (select 1 from channel_type where parent_channel_type_id=? and status != ?)");
		if (type == ChannelTypeType.CUSTOMER) {
			sql.append(" OR exists (select 1 from Customer where CHANNEL_TYPE_ID=? and status != ?)");
			params.add(channelTypeId);
			params.add(ActiveType.DELETED.getValue());
		} else if (type == ChannelTypeType.CAR) {
			sql.append(" or exists (select 1 from Car where label_id=? and status != ?)");

			params.add(channelTypeId);
			params.add(ActiveType.DELETED.getValue());
		}
		if (type == ChannelTypeType.SHOP) {
			sql.append(" or exists (select 1 from shop where shop_type_ID=? and status != ?)");

			params.add(channelTypeId);
			params.add(ActiveType.DELETED.getValue());
		}

		params.add(channelTypeId);
		params.add(ActiveType.DELETED.getValue());
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public Boolean isUsingByOtherRunningItem(long channelTypeId, ChannelTypeType type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("	select count(1) as count from channel_type where 1 = 0");
		if (type == ChannelTypeType.CUSTOMER) {
			sql.append(" OR exists (select 1 from Customer where CHANNEL_TYPE_ID=? and status = ?)");
			params.add(channelTypeId);
			params.add(ActiveType.RUNNING.getValue());
		}
		//		else if (type == ChannelTypeType.STAFF) {
		//			sql.append(" or exists (select 1 as count from Staff where STAFF_TYPE_ID=? and status = ?)");
		//			
		//			params.add(channelTypeId);
		//			params.add(ActiveType.RUNNING.getValue());
		//		} else if (type == ChannelTypeType.SALE_MAN) {
		//			sql.append(" or exists (select 1 from Staff where STAFF_SALE_ID=? and status = ?)");
		//			
		//			params.add(channelTypeId);
		//			params.add(ActiveType.RUNNING.getValue());
		//		}
		else if (type == ChannelTypeType.CAR) {
			sql.append(" or exists (select 1 from Car where label_id=? and status = ?)");

			params.add(channelTypeId);
			params.add(ActiveType.RUNNING.getValue());
		}
		if (type == ChannelTypeType.SHOP) {
			sql.append(" or exists (select 1 from shop where shop_type_ID=? and status = ?)");

			params.add(channelTypeId);
			params.add(ActiveType.RUNNING.getValue());
		}

		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public Boolean isAllChildStoped(long channelTypeId, ChannelTypeType type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select count(1) as count from channel_type ");
		sql.append(" start with parent_channel_type_id=? and type=? and status=?");
		params.add(channelTypeId);
		params.add(type.getValue());
		params.add(ActiveType.RUNNING.getValue());

		sql.append(" connect by prior channel_type_id=parent_channel_type_id");

		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? false : true;
	}

	@Override
	public List<ChannelType> getListDescendantChannelType(KPaging<ChannelType> kPaging, Long parentId, ChannelTypeType type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from ( select * from channel_type ");
		sql.append(" where type=? and status = ?");
		params.add(type.getValue());
		params.add(ActiveType.RUNNING.getValue());

		if (parentId == null) {
			sql.append(" start with parent_channel_type_id is null");
		} else {
			sql.append(" start with parent_channel_type_id=?");
			params.add(parentId);
		}
		sql.append(" connect by prior channel_type_id=parent_channel_type_id ");
		sql.append(" union  ");
		sql.append(" select * from channel_type  ");

		if (parentId == null) {
			sql.append(" where channel_type_id is null and type=? and status = ?) ");
			params.add(type.getValue());
			params.add(ActiveType.RUNNING.getValue());
		} else {
			sql.append(" where channel_type_id=? and type=? and status = ?) ");
			params.add(parentId);
			params.add(type.getValue());
			params.add(ActiveType.RUNNING.getValue());
		}

		sql.append(" order by channel_type_code");
		if (kPaging == null)
			return repo.getListBySQL(ChannelType.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ChannelType.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<ChannelType> getListDescendantChannelTypeOrderById(KPaging<ChannelType> kPaging, Long parentId, ChannelTypeType type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from ( select * from channel_type ");
		sql.append(" where type=? and status = ?");
		params.add(type.getValue());
		params.add(ActiveType.RUNNING.getValue());

		if (parentId == null) {
			sql.append(" start with parent_channel_type_id is null");
		} else {
			sql.append(" start with parent_channel_type_id=?");
			params.add(parentId);
		}
		sql.append(" connect by prior channel_type_id=parent_channel_type_id ");
		sql.append(" union  ");
		sql.append(" select * from channel_type  ");

		if (parentId == null) {
			sql.append(" where channel_type_id is null and type=? and status = ?) ");
			params.add(type.getValue());
			params.add(ActiveType.RUNNING.getValue());
		} else {
			sql.append(" where channel_type_id=? and type=? and status = ?) ");
			params.add(parentId);
			params.add(type.getValue());
			params.add(ActiveType.RUNNING.getValue());
		}

		sql.append(" order by channel_type_id");
		if (kPaging == null)
			return repo.getListBySQL(ChannelType.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ChannelType.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<ChannelType> getListCustomerTypeNotInPromotionProgram(KPaging<ChannelType> kPaging, Long promotionShopMapId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select *  ");
		sql.append(" from channel_type s ");
		sql.append(" where s.channel_type_id not in (select customer_type_id from promotion_customer_map ds where ds.promotion_shop_map_id = ? ");
		sql.append(" and ds.type = ? and ds.status != ?)");
		params.add(promotionShopMapId);
		params.add(PromotionCustomerMapType.APPLY_TO_CUSTOMER_TYPE.getValue());
		params.add(ActiveType.DELETED.getValue());
		sql.append(" and s.status=? and s.type = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ChannelTypeType.CUSTOMER.getValue());

		sql.append(" order by NLSSORT(channel_type_name,'NLS_SORT=vietnamese')");
		if (kPaging == null)
			return repo.getListBySQL(ChannelType.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ChannelType.class, sql.toString(), params, kPaging);
	}

	@Override
	public void updateDescendantChannelType(Long parentId, Integer objectType) throws DataAccessException {
		String sql = null;
		List<Object> params = new ArrayList<Object>();
		if (objectType != null) {
			sql = "update channel_type set object_type = ? where channel_type_id in (select channel_type_id from channel_type start with channel_type_id = ? connect by prior channel_type_id = parent_channel_type_id)";
			params.add(objectType);
			params.add(parentId);

		} else {
			sql = "update channel_type set object_type = null where channel_type_id in (select channel_type_id from channel_type start with channel_type_id = ? connect by prior channel_type_id = parent_channel_type_id)";
			params.add(parentId);
		}
		repo.executeSQLQuery(sql, params);
	}

	@Override
	public Boolean checkExsitedChildChannelType(String channelCode, ChannelTypeType type) throws DataAccessException {
		StringBuilder countHql = new StringBuilder();
		List<Object> countParams = new ArrayList<Object>();
		countHql.append("select count(1) as count from channel_type where parent_channel_type_id in (select channel_type_id from channel_type where channel_type_code = ? and type = ? and status != ?)");
		countParams.add(channelCode);
		countParams.add(type.getValue());
		countParams.add(ActiveType.DELETED.getValue());
		Integer c = repo.countByHQL(countHql.toString(), countParams);
		if (c == null || c == 0) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public Boolean checkChannelTypeHasAnyChild(Long parentId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select count(1) as count from channel_type where status != ? ");
		List<Object> params = new ArrayList<Object>();
		params.add(ActiveType.DELETED.getValue());
		if (parentId != null) {
			sql.append(" and parent_channel_type_id=?");
			params.add(parentId);
		} else {
			sql.append(" and parent_channel_type_id is null");
		}
		sql.append(" order by channel_type_code");
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public Boolean checkAncestor(Long parentId, Long channelTypeId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select count(1) as count from channel_type where channel_type_id=?");
		params.add(channelTypeId);
		sql.append(" start with channel_type_id=?");
		params.add(parentId);

		sql.append(" connect by prior channel_type_id=parent_channel_type_id");

		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public List<ChannelTypeVO> getListChannelTypeVO() throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT ");
		sql.append(" CHN.CHANNEL_TYPE_ID as idChannelType, ");
		sql.append(" CHN.CHANNEL_TYPE_NAME as nameChannelType, ");
		sql.append(" CHN.CHANNEL_TYPE_CODE as codeChannelType ");
		sql.append(" FROM CHANNEL_TYPE CHN ");
		sql.append(" WHERE CHN.TYPE=3 "); // lacnv1 - 12.03.2014
		//sql.append(" WHERE CHN.TYPE=10 "); //LocTT1 - Dec19, 2013: change from 3 to 10
		sql.append(" AND CHN.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ORDER BY CHN.CHANNEL_TYPE_CODE ");

		String[] fieldNames = { "idChannelType", // 0
				"nameChannelType", // 1
				"codeChannelType" // 2
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING // 2
		};
		return repo.getListByQueryAndScalar(ChannelTypeVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ChannelTypeVO> getListChannelTypeVOSaleMT() throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT ");
		sql.append(" CHN.CHANNEL_TYPE_ID as idChannelType, ");
		sql.append(" CHN.CHANNEL_TYPE_NAME as nameChannelType, ");
		sql.append(" CHN.CHANNEL_TYPE_CODE as codeChannelType ");
		sql.append(" FROM CHANNEL_TYPE CHN ");
		sql.append(" WHERE CHN.TYPE=10 "); //LocTT1 - Dec19, 2013: change from 3 to 10
		sql.append(" AND CHN.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ORDER BY CHN.CHANNEL_TYPE_CODE ");

		String[] fieldNames = { "idChannelType", // 0
				"nameChannelType", // 1
				"codeChannelType" // 2
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING // 2
		};
		return repo.getListByQueryAndScalar(ChannelTypeVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	//TUNGTT
	@Override
	public List<ChannelTypeVO> getListChannelTypeCustomerCardVO() throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT ");
		sql.append(" CHN.CHANNEL_TYPE_ID as idChannelType, ");
		sql.append(" CHN.CHANNEL_TYPE_NAME as nameChannelType, ");
		sql.append(" CHN.CHANNEL_TYPE_CODE as codeChannelType ");
		sql.append(" FROM CHANNEL_TYPE CHN ");
		//		sql.append(" WHERE CHN.TYPE=3 ");
		sql.append(" WHERE CHN.TYPE=11 "); //LocTT1 - Dec19, 2013: change from 3 to 10
		sql.append(" AND CHN.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ORDER BY CHN.CHANNEL_TYPE_CODE ");

		String[] fieldNames = { "idChannelType", // 0
				"nameChannelType", // 1
				"codeChannelType" // 2
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING // 2
		};
		return repo.getListByQueryAndScalar(ChannelTypeVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ChannelTypeVO> getListChannelTypeVOAlreadySet(KPaging<ChannelTypeVO> kPaging, long promotionProgramId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		StringBuilder coutsql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT CHT.CHANNEL_TYPE_ID idChannelType, ");
		sql.append(" CHT.CHANNEL_TYPE_CODE codeChannelType, ");
		sql.append(" CHT.CHANNEL_TYPE_NAME nameChannelType ");
		sql.append(" FROM CHANNEL_TYPE cht ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND CHT.STATUS=1 ");
		sql.append(" AND CHT.CHANNEL_TYPE_ID IN ");
		sql.append(" ( ");
		sql.append(" SELECT PCADT.OBJECT_ID FROM PROMOTION_CUST_ATTR pca,PROMOTION_CUST_ATTR_DETAIL pcadt ");
		sql.append(" WHERE PCA.PROMOTION_CUST_ATTR_ID=PCADT.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND PCA.OBJECT_TYPE=2 ");
		sql.append(" AND pca.PROMOTION_PROGRAM_ID=? ");
		params.add(promotionProgramId);
		sql.append(" AND pca.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND pcadt.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ) ");

		String[] fieldNames = { "idChannelType", // 0
				"codeChannelType", // 1
				"nameChannelType" // 2
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING // 2
		};

		coutsql.append(" select count(*) count from ( ");
		coutsql.append(sql);
		coutsql.append(" ) ");

		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(ChannelTypeVO.class, fieldNames, fieldTypes, sql.toString(), coutsql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(ChannelTypeVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	//dandt - getListChannelTypeVOAlreadySet for SaleMT
	@Override
	public List<ChannelTypeVO> getSaleMTListChannelTypeVOAlreadySet(KPaging<ChannelTypeVO> kPaging, long promotionProgramId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		StringBuilder coutsql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT CHT.CHANNEL_TYPE_ID idChannelType, ");
		sql.append(" CHT.CHANNEL_TYPE_CODE codeChannelType, ");
		sql.append(" CHT.CHANNEL_TYPE_NAME nameChannelType ");
		sql.append(" FROM CHANNEL_TYPE cht ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND CHT.STATUS=1 ");
		sql.append(" AND CHT.CHANNEL_TYPE_ID IN ");
		sql.append(" ( ");
		sql.append(" SELECT PCADT.OBJECT_ID FROM MT_PROMOTION_CUST_ATTR pca,MT_PROMOTION_CUST_ATTR_DETAIL pcadt ");
		sql.append(" WHERE PCA.PROMOTION_CUST_ATTR_ID=PCADT.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND PCA.OBJECT_TYPE=2 ");
		sql.append(" AND pca.PROMOTION_PROGRAM_ID=? ");
		params.add(promotionProgramId);
		sql.append(" AND pca.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND pcadt.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ) ");

		String[] fieldNames = { "idChannelType", // 0
				"codeChannelType", // 1
				"nameChannelType" // 2
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING // 2
		};

		coutsql.append(" select count(*) count from ( ");
		coutsql.append(sql);
		coutsql.append(" ) ");

		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(ChannelTypeVO.class, fieldNames, fieldTypes, sql.toString(), coutsql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(ChannelTypeVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<ChannelTypeVO> getListChannelTypeVOCustomerCardAlreadySet(KPaging<ChannelTypeVO> kPaging, long promotionProgramId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		StringBuilder coutsql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT CHT.CHANNEL_TYPE_ID idChannelType, ");
		sql.append(" CHT.CHANNEL_TYPE_CODE codeChannelType, ");
		sql.append(" CHT.CHANNEL_TYPE_NAME nameChannelType ");
		sql.append(" FROM CHANNEL_TYPE cht ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND CHT.STATUS=1 ");
		sql.append(" AND CHT.CHANNEL_TYPE_ID IN ");
		sql.append(" ( ");
		sql.append(" SELECT PCADT.OBJECT_ID FROM MT_PROMOTION_CUST_ATTR pca,MT_PROMOTION_CUST_ATTR_DETAIL pcadt ");
		sql.append(" WHERE PCA.PROMOTION_CUST_ATTR_ID=PCADT.PROMOTION_CUST_ATTR_ID ");
		sql.append(" AND PCA.OBJECT_TYPE=4 ");
		sql.append(" AND pca.PROMOTION_PROGRAM_ID=? ");
		params.add(promotionProgramId);
		sql.append(" AND pca.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND pcadt.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ) ");

		String[] fieldNames = { "idChannelType", // 0
				"codeChannelType", // 1
				"nameChannelType" // 2
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING // 2
		};

		coutsql.append(" select count(*) count from ( ");
		coutsql.append(sql);
		coutsql.append(" ) ");

		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(ChannelTypeVO.class, fieldNames, fieldTypes, sql.toString(), coutsql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(ChannelTypeVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<ChannelType> getListChannelTypeByTypeAndArrObjType(Integer type, Integer... objectType) throws DataAccessException {
		if (type == null) {
			throw new IllegalArgumentException("type null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from channel_type where 1 = 1 ");
		sql.append(" and type = ? ");
		params.add(type);
		if (objectType != null && objectType.length > 0) {
			sql.append(" and object_type in (-1 ");
			for (Integer obj: objectType) {
				sql.append(" ,? ");
				params.add(obj);
			}
			sql.append(" ) ");
		}
		return repo.getListBySQL(ChannelType.class, sql.toString(), params);
	}

	@Override
	public List<ChannelType> getListChannelTypeByType(Integer type)
			throws DataAccessException {
		if (type == null) {
			throw new IllegalArgumentException("type null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from channel_type where 1 = 1 and status = 1");
		sql.append(" and type = ? ");
		params.add(type);		
		return repo.getListBySQL(ChannelType.class, sql.toString(), params);
	}

	@Override
	public ChannelType getChannelTypeByName(String name, ChannelTypeType type)
			throws DataAccessException {
		String sql = "select * from CHANNEL_TYPE where upper(channel_type_name)=upper(?) and type=? and status != ?";
		List<Object> params = new ArrayList<Object>();
		params.add(name.toUpperCase());
		params.add(type.getValue());
		params.add(ActiveType.DELETED.getValue());
		ChannelType pi = repo.getEntityBySQL(ChannelType.class, sql, params);
		return pi;
	}
}

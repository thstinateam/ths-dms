package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ProductIntroduction;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class ProductIntroductionDAOImpl implements ProductIntroductionDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Override
	public ProductIntroduction createProductIntroduction(ProductIntroduction productIntroduction, LogInfoVO logInfo) throws DataAccessException {
		if (productIntroduction == null) {
			throw new IllegalArgumentException("productIntroduction");
		}
//		productIntroduction.setCreateUser(logInfo.getStaffCode());
		repo.create(productIntroduction);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, productIntroduction, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(ProductIntroduction.class, productIntroduction.getId());
	}

	@Override
	public void deleteProductIntroduction(ProductIntroduction productIntroduction, LogInfoVO logInfo) throws DataAccessException {
		if (productIntroduction == null) {
			throw new IllegalArgumentException("productIntroduction");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(productIntroduction);
		try {
			actionGeneralLogDAO.createActionGeneralLog(productIntroduction, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public ProductIntroduction getProductIntroductionById(long id) throws DataAccessException {
		return repo.getEntityById(ProductIntroduction.class, id);
	}
	
	@Override
	public void updateProductIntroduction(ProductIntroduction productIntroduction, LogInfoVO logInfo) throws DataAccessException {
		if (productIntroduction == null) {
			throw new IllegalArgumentException("productIntroduction");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		ProductIntroduction temp = this.getProductIntroductionById(productIntroduction.getId());

//		product.setUpdateDate(commonDAO.getSysDate());
//		product.setUpdateUser(logInfo.getStaffCode());
		repo.update(productIntroduction);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, productIntroduction, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public ProductIntroduction getProductIntroductionByProduct(Long productId)
		throws DataAccessException {
		String sql = "select * from product_introduction where product_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(productId);
		return repo.getEntityBySQL(ProductIntroduction.class, sql, params);
	}
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Warehouse;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.StockGeneralFilter;
import ths.dms.core.entities.vo.StockGeneralVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.WarehouseVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class StockManagerDAOImpl implements StockManagerDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private CommonDAO commonDAO;

	@Override
	public Warehouse createWarehouse(Warehouse warehouse) throws DataAccessException {
		return createWarehouse(warehouse, commonDAO.getSysDate());
	}
	
	@Override
	public Warehouse createWarehouse(Warehouse warehouse, Date sysdate) throws DataAccessException {
		if (warehouse == null) {
			throw new IllegalArgumentException("warehouse is null");
		}
		if (!StringUtility.isNullOrEmpty(warehouse.getWarehouseCode())) {
			warehouse.setWarehouseCode(warehouse.getWarehouseCode().trim().toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(warehouse.getWarehouseName())) {
			warehouse.setWarehouseNameText(Unicode2English.codau2khongdau(warehouse.getWarehouseName().trim().toLowerCase()));
		}
		warehouse.setCreateDate(sysdate);
		return repo.create(warehouse);
	}

	@Override
	public void updateWarehouse(Warehouse warehouse) throws DataAccessException {
		if (warehouse == null) {
			throw new IllegalArgumentException("warehouse is null");
		}
		if (!StringUtility.isNullOrEmpty(warehouse.getWarehouseCode())) {
			warehouse.setWarehouseCode(warehouse.getWarehouseCode().trim().toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(warehouse.getWarehouseName())) {
			warehouse.setWarehouseNameText(Unicode2English.codau2khongdau(warehouse.getWarehouseName().trim().toLowerCase()));
		}
		warehouse.setUpdateDate(commonDAO.getSysDate());
		repo.update(warehouse);
	}

	@Override
	public Warehouse getWarehouseById(long id) throws DataAccessException {
		Warehouse kq = repo.getEntityById(Warehouse.class, id);
		if (kq != null && !ActiveType.DELETED.getValue().equals(kq.getStatus().getValue())) {
			return kq;
		}
		return new Warehouse();
	}

	@Override
	public Boolean checkIfRecordWarehouseExist(BasicFilter<WarehouseVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) as count from warehouse ");
		sql.append(" where status !=-1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(warehouse_code) = ? ");
			params.add(filter.getCode().trim().toLowerCase());
		}
		if (filter.getLongG() != null) {
			sql.append(" and shop_id = ? ");
			params.add(filter.getLongG());
		}
		if (filter.getSeq() != null) {
			sql.append(" and seq = ? ");
			params.add(filter.getSeq());
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public Boolean checkIfRecordWarehouseInStockTotalExist(BasicFilter<WarehouseVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) as count from stock_total ");
		sql.append(" where status !=-1 ");
		if (filter.getId() != null) {
			sql.append(" and warehouse_id = ? ");
			params.add(filter.getId());
		}
		if (filter.getLongG() != null) {
			sql.append(" and product_id = ? ");
			params.add(filter.getLongG());
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public List<Warehouse> getListWarehouseByFilter(BasicFilter<Warehouse> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select * ");
		fromSql.append(" from warehouse wh ");
		if (filter != null && StringUtility.isNullOrEmpty(filter.getStrShopId()) && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			fromSql.append(" join table (f_get_list_child_shop_inherit(?, sysdate, ?, ?)) tmp on tmp.number_key_id = wh.shop_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		fromSql.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getStrShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrShopId(), "wh.shop_id");
			fromSql.append(paramsFix);
		}
		if (filter.getStatus() != null) {
			fromSql.append(" and wh.status = ? ");
			params.add(filter.getStatus());
		} else {
			fromSql.append(" and wh.status != -1 ");
		}
		if (filter.getSeq() != null && filter.getSeq() > 0) {
			fromSql.append(" and wh.seq = ? ");
			params.add(filter.getSeq());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			fromSql.append(" and lower(wh.warehouse_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			fromSql.append(" and lower(wh.warehouse_name_text) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getName().trim().toLowerCase())));
		}
		if (!StringUtility.isNullOrEmpty(filter.getDescription())) {
			fromSql.append(" and lower(wh.description) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDescription().trim().toLowerCase()));
		}
		if (filter.getWarehouseType() != null) {
			fromSql.append(" and wh.warehouse_type = ? ");
			params.add(filter.getWarehouseType());
		}
		if (filter.getFromDate() != null) {
			fromSql.append(" and trunc(wh.create_date) >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			fromSql.append(" and trunc(wh.create_date) < trunc(?) ");
			params.add(filter.getToDate());
		}
		if (filter.getLongG() != null) {
			fromSql.append(" and wh.shop_id = ? ");
			params.add(filter.getLongG());
			fromSql.append(" and wh.shop_id in (select sw.shop_id from shop sw where sw.status = 1 start with sw.shop_id = ? connect by prior sw.shop_id = sw.parent_shop_id) ");
			params.add(filter.getLongG());
		}
		if (filter.getArrlongG() != null && !filter.getArrlongG().isEmpty()) {
			fromSql.append(" and wh.shop_id in (-1, ");
			for (Long shopIdG : filter.getArrlongG()) {
				fromSql.append(" ,? ");
				params.add(shopIdG);
			}
			fromSql.append(" ) ");
			fromSql.append(" and wh.shop_id in (select sw.shop_id from shop sw where sw.status = 1 start with sw.shop_id in(-1 ");
			for (Long shopIdG : filter.getArrlongG()) {
				fromSql.append(" ,? ");
				params.add(shopIdG);
			}
			fromSql.append(" ) and wh.shop_id in (select sw.shop_id from shop sw where sw.status = 1 start with sw.shop_id = ? connect by prior sw.shop_id = sw.parent_shop_id) ");
			params.add(filter.getLongG());
		}
		sql.append(fromSql.toString());

		if (filter.getIntG() != null && filter.getIntG().equals(1)) {
			sql.append(" order by wh.warehouse_code ");
		} else if (filter.getIntG() != null && filter.getIntG().equals(2)) {
			sql.append(" order by wh.shop_id, wh.warehouse_code ");
		}
		//Count total
		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(Warehouse.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(Warehouse.class, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}

	@Override
	public List<WarehouseVO> getListWarehouseVOByFilter(BasicFilter<WarehouseVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();
		sql.append(" select  ");
		sql.append(" wh.warehouse_id as warehouseId, ");
		sql.append(" wh.warehouse_code as warehouseCode, ");
		sql.append(" wh.warehouse_name as warehouseName, ");
		sql.append(" wh.seq as seq, ");
		sql.append(" wh.status as status, ");
		sql.append(" to_char(wh.create_date, 'dd/MM/yyyy') as createDateStr, ");
		sql.append(" wh.description as description, ");
		sql.append(" wh.warehouse_type as warehouseType, ");
//		sql.append(" (select app.ap_param_name from ap_param app where app.ap_param_code = wh.warehouse_type) as warehouseTypeN, ");
		sql.append("  wh.shop_id as shopId, ");
		sql.append(" (select s.shop_code from shop s where s.status = 1 and s.shop_id = wh.shop_id) as shopCode, ");
		sql.append(" (select s.shop_name from shop s where s.status = 1 and s.shop_id = wh.shop_id) as shopName, ");
		sql.append(" (select s1.shop_code from shop s1 where s1.status = 1 and s1.shop_id = (select s.parent_shop_id from shop s where s.status = 1 and s.shop_id = wh.shop_id)) as shopCodeV, ");
		sql.append(" (select s2.shop_code from shop s2 where s2.status = 1  and s2.shop_id = (select s1.parent_shop_id from shop s1 where s1.status = 1  ");
		sql.append("   and s1.shop_id = (select s.parent_shop_id from shop s where s.status = 1 and s.shop_id = wh.shop_id))) as shopCodeM ");
		fromSql.append(" from warehouse wh ");
		if (filter != null && StringUtility.isNullOrEmpty(filter.getStrShopId()) && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			fromSql.append(" join table (f_get_list_child_shop_inherit(?, sysdate, ?, ?)) tmp on tmp.number_key_id = wh.shop_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		fromSql.append(" where 1 = 1  ");
		
		if (!StringUtility.isNullOrEmpty(filter.getStrShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrShopId(), "wh.shop_id");
			fromSql.append(paramsFix);
		}
		
		if (Boolean.TRUE.equals(filter.getIsExistStock())) {
			fromSql.append(" and EXISTS (SELECT 1 FROM STOCK_TOTAL st WHERE st.WAREHOUSE_ID = wh.WAREHOUSE_ID) ");
		}
		if (filter.getStatus() != null) {
			fromSql.append(" and wh.status = ? ");
			params.add(filter.getStatus());
		} else {
			fromSql.append(" and wh.status != -1 ");
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			fromSql.append(" and lower(wh.warehouse_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			fromSql.append(" and lower(wh.warehouse_name_text) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getName().trim().toLowerCase())));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCodeAndName())) {
			fromSql.append(" and (lower(wh.warehouse_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCodeAndName().trim().toLowerCase()));
			fromSql.append(" or lower(wh.warehouse_name_text) like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCodeAndName().trim().toLowerCase())));
		}
		if (!StringUtility.isNullOrEmpty(filter.getDescription())) {
			fromSql.append(" and lower(wh.description) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDescription().trim().toLowerCase()));
		}
		if (filter.getWarehouseType() != null) {
			fromSql.append(" and wh.warehouse_type = ?");
			params.add(filter.getWarehouseType());
		}
		if (filter.getFromDate() != null) {
			fromSql.append(" and trunc(wh.create_date) >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			fromSql.append(" and trunc(wh.create_date) < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if (filter.getLongG() != null) {
			fromSql.append(" and wh.shop_id in (select sw.shop_id from shop sw where sw.status = 1 start with sw.shop_id = ? connect by prior sw.shop_id = sw.parent_shop_id) ");
			params.add(filter.getLongG());
		}
		//Append Chuoi From and Where
		sql.append(fromSql.toString());
		
		//Count total
		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());
		final String[] fieldNames = new String[] { 
				"warehouseId", "warehouseCode", "warehouseName", "seq", 
				"status", "createDateStr", "description", "warehouseType", //"warehouseTypeN", 
				"shopId", "shopCode", "shopName", "shopCodeV", 
				"shopCodeM" };
		
		String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			sql.append(" order by ").append(sort).append(" ");
			String order = StringUtility.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				sql.append(order);
			}
		} else {
			sql.append(" order by warehouseCode, shopCodeM, shopCodeV, seq, shopCode, status, createDateStr ");
		}
		
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING };
		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(WarehouseVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(WarehouseVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}

	@Override
	public List<StockTotalVO> getListStockWarehouseByProductId(BasicFilter<StockTotalVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();
		sql.append(" select  ");
		sql.append(" wh.warehouse_id as warehouseId, ");
		sql.append(" wh.seq as seq, ");
		sql.append(" st.stock_total_id as stockTotalId, ");
		sql.append(" st.product_id as productId, ");
		sql.append(" st.available_quantity as availableQuantity, ");
		sql.append(" st.quantity as stockQuantity ");
		fromSql.append(" from warehouse stock_total st join warehouse wh on st.warehouse_id = wh.warehouse_id and st.object_id = wh.shop_id ");
		fromSql.append(" where 1 = 1 and st.status = 1 and wh.status = 1 ");
		if (filter.getLongG() != null) {
			sql.append(" and st.product_id = ? ");
			params.add(filter.getLongG());
		}
		if (filter.getId() != null) {
			sql.append(" and st.object_id = ? ");
			params.add(filter.getId());
		}
		sql.append(fromSql);
		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());
		final String[] fieldNames = new String[] { "warehouseId", "seq", "stockTotalId", "productId", "availableQuantity", "stockQuantity" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}
	
	
	@Override
	public List<StockTotalVO> getListStockWarehouseByListProduct(BasicFilter<StockTotalVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select  ");
		sql.append(" wh.warehouse_id as warehouseId, ");
		sql.append(" wh.warehouse_code as warehouseCode, ");
		sql.append(" wh.warehouse_name as warehouseName, ");
		sql.append(" wh.seq as seq, ");
		sql.append(" st.stock_total_id as stockTotalId, ");
		sql.append(" st.product_id as productId, ");
		sql.append(" st.available_quantity as availableQuantity, ");
		sql.append(" st.quantity as stockQuantity ");
		sql.append(" from warehouse stock_total st ");
		sql.append(" join warehouse wh on st.warehouse_id = wh.warehouse_id and st.object_id = wh.shop_id ");
		sql.append(" where 1 = 1 and st.status = 1 and wh.status = 1 ");
		if (filter.getArrLongS() != null) {
			sql.append(" and st.product_id in (-1 ");
			for(Long i:filter.getArrLongS()){
				sql.append(",?");
				params.add(i);
			}
			sql.append(") ");
		}
		if (filter.getId() != null) {
			sql.append(" and st.object_id = ? ");
			params.add(filter.getId());
		}
		sql.append(" order by wh.seq");
		final String[] fieldNames = new String[] { 
				"warehouseId", "seq", "stockTotalId", 
				"productId", "availableQuantity", "stockQuantity" 
				};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, 
				StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER 
				};
		return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<StockTotalVO> getListStockTotalVOBySaler(BasicFilter<StockTotalVO> filter) throws DataAccessException {
		if (filter == null || filter.getShopId() == null || filter.getLongG() == null) {
			throw new IllegalArgumentException("invalid parameters");
		}

		StringBuilder sql = new StringBuilder();
		StringBuilder withSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		Long staffId = filter.getLongG();

		withSql.append(" with ");
//		withSql.append(" with lstShop as (");
//		withSql.append(" select shop_id, level as orderNo from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id and status = 1 ");
//		params.add(filter.getShopId());
//		withSql.append(" ),");
//		withSql.append(" lstPrice as (");
//		withSql.append(" select 1 orderNo, price_id, product_id, customer_id, customer_type_id, shop_id, shop_type_id, vat, price, package_price");
//		withSql.append(" from price");
//		withSql.append(" where status = 1 and customer_id is not null");
//		withSql.append(" union all");
//		withSql.append(" select 2 orderNo, price_id, product_id, customer_id, customer_type_id, shop_id, shop_type_id, vat, price, package_price");
//		withSql.append(" from price");
//		withSql.append(" where status = 1 and customer_id is null and customer_type_id is not null");
//		withSql.append(" and shop_id = ?");
//		params.add(filter.getShopId());
//		withSql.append(" union all");
//		withSql.append(" select 3 orderNo, price_id, product_id, customer_id, customer_type_id, shop_id, shop_type_id, vat, price, package_price");
//		withSql.append(" from price");
//		withSql.append(" where status = 1 and customer_id is null and customer_type_id is not null");
//		withSql.append(" and shop_id is null and shop_type_id = (select shop_type_id from shop where shop_id=?)");
//		params.add(filter.getShopId());
//		
//		withSql.append(" union all");
//		withSql.append(" select 4 orderNo, price_id, product_id, customer_id, customer_type_id, shop_id, shop_type_id, vat, price, package_price");
//		withSql.append(" from price");
//		withSql.append(" where status = 1 and customer_id is null and customer_type_id is not null");
//		withSql.append(" and shop_id is null and shop_type_id is null");
//		withSql.append(" union all");
//		withSql.append(" select 5 orderNo, price_id, product_id, customer_id, customer_type_id, shop_id, shop_type_id, vat, price, package_price");
//		withSql.append(" from price");
//		withSql.append(" where status = 1 and customer_id is null and customer_type_id is null");
//		withSql.append(" and shop_id = ?");
//		params.add(filter.getShopId());
//		withSql.append(" union all");
//		withSql.append(" select 6 orderNo, price_id, product_id, customer_id, customer_type_id, shop_id, shop_type_id, vat, price, package_price");
//		withSql.append(" from price");
//		withSql.append(" where status = 1 and customer_id is null and customer_type_id is null");
//		withSql.append(" and shop_id is null and shop_type_id = (select shop_type_id from shop where shop_id=?)");
//		params.add(filter.getShopId());
//		withSql.append(" union all");
//		withSql.append(" select (s.orderNo + 100) orderNo, pr.price_id, pr.product_id, pr.customer_id, pr.customer_type_id, pr.shop_id, pr.shop_type_id, pr.vat, price, package_price");
//		withSql.append(" from price pr join lstShop s on pr.shop_id = s.shop_id");
//		withSql.append(" where pr.status = 1 and pr.customer_id is null and pr.customer_type_id is null");
//		withSql.append(" and pr.shop_id != ?");
//		params.add(filter.getShopId());
//		withSql.append(" ),");
		withSql.append(" priceTmp as (");
		withSql.append(" select lp.product_id, lp.price_id,lp.price");
		withSql.append(" from price_shop_deduced lp");
		withSql.append(" where 1 = 1 ");
		withSql.append(" and lp.shop_id = ? ");
		params.add(filter.getShopId());
		withSql.append(" and lp.from_date <= trunc(sysdate) and (lp.to_date >= trunc(sysdate) or lp.to_date is null) ");
//		withSql.append(" inner join (select Min(orderNo) orderNo,product_id from lstPrice group by product_id) mlp ");
//		withSql.append(" on (lp.orderNo=mlp.orderNo and lp.product_id=mlp.product_id)");
		withSql.append(" ),");
		withSql.append(" products as (");
		withSql.append(" select product_id, product_code, product_name,");
		withSql.append(" volumn, net_weight, gross_weight, uom1, uom2, convfact");
		withSql.append(" from product");
		withSql.append(" where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		withSql.append(" and (sub_cat_id in (");
		withSql.append(" select cat_id from staff_sale_cat where staff_id = ?");
		params.add(staffId);
		withSql.append(" ) or not exists (select 1 from staff_sale_cat where staff_id = ?))");
		params.add(staffId);
		withSql.append(" )");

		sql.append(withSql.toString());
		sql.append(" select p.product_id as productId, p.product_code as productCode,");
		sql.append(" p.product_name as productName,");
		sql.append(" p.gross_weight as grossWeight, p.uom1, p.convfact,");
		sql.append(" wh.warehouse_id as warehouseId, wh.warehouse_name as warehouseName,");
		sql.append(" st.available_quantity as availableQuantity, wh.seq, nvl(pr.price, 0) price ");

		StringBuilder fromSql = new StringBuilder();
		fromSql.append(" from products p");
		fromSql.append(" join stock_total st on (st.product_id = p.product_id and object_type = ? and object_id = ? and st.status=?)");
		params.add(StockObjectType.SHOP.getValue());
		params.add(filter.getShopId());
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" join warehouse wh on (wh.warehouse_id = st.warehouse_id and wh.status = ?)");
		params.add(ActiveType.RUNNING.getValue());
		fromSql.append(" left join priceTmp pr on (pr.product_id = p.product_id)");
		fromSql.append(" where 1 = 1");

		if (filter.getIntG() != null && 1 == filter.getIntG()) {
			fromSql.append(" and st.available_quantity > 0");
		}

		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			fromSql.append(" and lower(p.product_code) like ? escape '/'");
			String s = filter.getCode().trim();
			s = s.toLowerCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}

		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			fromSql.append(" and lower(p.product_name) like ? escape '/'");
			String s = filter.getName().trim();
			s = s.toLowerCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}

		sql.append(fromSql.toString());
		sql.append(" order by productCode, seq");

		String[] fieldNames = new String[] { "productId", "productCode", "productName", "grossWeight", "uom1", "convfact", "warehouseId", "warehouseName", "availableQuantity", "seq", "price" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.FLOAT, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL };

		if (filter.getkPaging() == null) {
			withSql = null;
			fromSql = null;
			List<StockTotalVO> lst = repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
			return lst;
		}

		StringBuilder countSql = new StringBuilder();
		countSql.append(withSql.toString());
		countSql.append(" select count(1) as count");
		countSql.append(fromSql.toString());
		withSql = null;
		fromSql = null;
		List<StockTotalVO> lst = repo.getListByQueryAndScalarPaginated(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		return lst;
	}

	@Override
	public List<Warehouse> searchWarehouseByShop(StockGeneralFilter<Warehouse> filter) throws DataAccessException {
		if (filter == null || filter.getShopId() == null) {
			throw new IllegalArgumentException("invalid parameters or shop undefined");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select wh.* ");
		sql.append("  from warehouse wh join shop sh on wh.shop_id = sh.shop_id ");
		sql.append("  where 1 = 1  ");
		if (filter.getStatusWareHouse()!=null) {
			sql.append("  and wh.status = ? ");
			params.add(filter.getStatusWareHouse());
		} else {
			sql.append("  and wh.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getStatusShop()!=null) {
			sql.append("  and sh.status = ? ");
			params.add(filter.getStatusShop());
		}
		if (!StringUtility.isNullOrEmpty(filter.getWareHouseCode())) {
			sql.append("  and lower(wh.warehouse_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getWareHouseCode().toLowerCase().trim()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getWareHouseName())) {
			sql.append("  and lower(wh.warehouse_name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getWareHouseName().toLowerCase().trim()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("  and lower(sh.shop_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getShopCode().toLowerCase().trim()));
		}
		if (filter.getShopChannel() != null) {
			sql.append("  and sh.shop_channel = ? ");
			params.add(filter.getShopChannel());
		}
		sql.append("  and wh.shop_id in (select shop_id from shop where status = 1 ");
		if (filter.getShopChannel() != null) {
			sql.append("  and shop_channel = ? ");
			params.add(filter.getShopChannel());
		}
		sql.append("  start with shop_id = ? and status = 1 connect by prior shop_id = parent_shop_id) ");
		params.add(filter.getShopId());
		sql.append(" order by sh.shop_code, wh.seq ");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(Warehouse.class, sql.toString(), params);
		}else{
			return repo.getListBySQLPaginated(Warehouse.class, sql.toString(), params, filter.getkPaging());
		}
	}
	
	//TODO Danh cho cap nhat phieu xuat kho kiem van chuyen noi bo
	/**
	 * @author hunglm16
	 * @since November 8,2014
	 * @description search update information output warehouse
	 */
	@Override
	public List<StockGeneralVO> searchUpdInformationOutputWarehouse(StockGeneralFilter<StockGeneralVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(filter.getTransType()) && (filter.DP.equals(filter.getTransType()) || filter.DC.equals(filter.getTransType()))) {
			sql.append("  select to_char(st.stock_trans_date,'dd/mm/yyyy') as stockTransDateStr,  ");
			sql.append("    st.stock_trans_code as stockTransCode, ");
			sql.append("    null as stockIssueNumber, ");
			sql.append("    decode(st.trans_type,'DC','Điều chuyển','DCT','Điều chuyển tăng','DCG','Điều chuyển giảm','DP','Đơn bán' ) issueType, ");
			sql.append("    null as about, ");
			sql.append("    (select staff_code from staff where staff_id = st.staff_id) as staffText, ");
			sql.append("    null as contractNumber,  ");
			sql.append("    null as carNumber, ");
			sql.append("    null as warehouseOutPutText, ");
			sql.append("    null as warehouseInputText, ");
			sql.append("    st.total_amount as totalAmount, ");
			sql.append("    st.approved as approved, ");
			sql.append("    st.stock_trans_id as isIdAttr, ");
			sql.append("    std.stock_trans_detail_id as isIdDetail ");
			sql.append(" from stock_trans st join stock_trans_detail std on st.stock_trans_id = std.stock_trans_id ");
			sql.append(" where 1 = 1  ");
//			if (filter.getIsPrint() != null && filter.ISPRINT.equals(filter.getIsPrint())) {
//				sql.append(" 	and st.stock_trans_code in (  ");
//				sql.append(" 	select ORDER_NUMBER  ");
//				sql.append(" 	from stock_issue ");
//				sql.append(" 	where status = 1) ");
//			}
			if (filter.getShopId() != null) {
				sql.append(" and st.shop_id = ?  ");
				params.add(filter.getShopId());
			}
			if (filter.getApproved() != null) {
				sql.append(" and st.approved = ? ");
				params.add(filter.getApproved());
			}
			if (filter.getApprovedStep() != null) {
				sql.append(" and st.approved_step = ? ");
				params.add(filter.getApprovedStep());
			}
			if (!StringUtility.isNullOrEmpty(filter.getTransType())) {
				sql.append(" and lower(st.trans_type) like ? ");
				params.add(filter.getTransType().trim().toLowerCase());
			}
			//sql.append(" and st.stock_trans_code like 'DC000010' ");
			if (filter.getFromDate() != null) {
				sql.append(" and trunc(st.stock_trans_date) >= trunc(?) ");
				params.add(filter.getFromDate());
			}
			if (filter.getToDate() != null) {
				sql.append(" and trunc(st.stock_trans_date) < trunc(?) + 1 ");
				params.add(filter.getToDate());
			}
			if (filter.getStaffId() != null) {
				sql.append(" and st.staff_id = ? ");
				params.add(filter.getStaffId());
			}
			if (!StringUtility.isNullOrEmpty(filter.getStockTransCode())) {
				sql.append(" and lower(st.stock_trans_code) like lower(?) ");
				params.add(filter.getStockTransCode().trim().toLowerCase());
			}
		} else if (!StringUtility.isNullOrEmpty(filter.getTransType()) && filter.POVNM.equals(filter.getTransType())) {
			sql.append(" select to_char(po.po_vnm_date, 'dd/mm/yyyy') as stockTransDateStr,   ");
			sql.append(" po.po_co_number as stockTransCode,  ");
			sql.append(" null as stockIssueNumber,  ");
			sql.append(" 'Trả hàng' as issueType,  ");
			sql.append(" null as about,  ");
			sql.append(" '' as staffText,  ");
			sql.append(" null as contractNumber,   ");
			sql.append(" null as carNumber,  ");
			sql.append(" null as warehouseOutPutText,  ");
			sql.append(" null as warehouseInputText,  ");
			sql.append(" po.total as totalAmount,  ");
			sql.append(" null as approved,  ");
			sql.append(" po.po_vnm_id as isIdAttr, ");
			sql.append(" pod.po_vnm_detail_id as isIdDetail ");
			sql.append(" from po_vnm po join po_vnm_detail pod on po.po_vnm_id = pod.po_vnm_id ");
			sql.append(" where 1 = 1 ");
			sql.append(" and po.status = 2  ");
			sql.append(" and po.type = 3  ");
			if (filter.getIsPrint() != null && filter.ISPRINT.equals(filter.getIsPrint())) {
				sql.append(" 	and po.po_co_number in (  ");
				sql.append(" 	select ORDER_NUMBER  ");
				sql.append(" 	from stock_issue ");
				sql.append(" 	where status = 1) ");
			}
			if (filter.getShopId() != null) {
				sql.append(" and po.shop_id = ?  ");
				params.add(filter.getShopId());
			}
			if (filter.getFromDate() != null) {
				sql.append(" and trunc(po.po_vnm_date) >= trunc(?) ");
				params.add(filter.getFromDate());
			}
			if (filter.getToDate() != null) {
				sql.append(" and trunc(po.po_vnm_date) < trunc(?) + 1 ");
				params.add(filter.getToDate());
			}
			if (!StringUtility.isNullOrEmpty(filter.getStockTransCode())) {
				sql.append(" and lower(po.po_co_number) like lower(?) ");
				params.add(filter.getStockTransCode().trim().toLowerCase());
			}
		} else {
			//Tham so khong thoa man
			return new ArrayList<StockGeneralVO>();
		}
		sql.append(" order by stockTransCode  ");
		String[] fieldNames = new String[] { "stockTransDateStr", "stockTransCode", "stockIssueNumber", "issueType", "about", "contractNumber", "carNumber", "warehouseOutPutText", "warehouseInputText", "totalAmount", "approved", "isIdAttr", "isIdDetail" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.LONG};

		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StockGeneralVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(StockGeneralVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	/**
	 * @author hunglm16
	 */
	@Override
	public List<StockGeneralVO> searchProductInWareHouse(StockGeneralFilter<StockGeneralVO> filter) throws DataAccessException {
		if (filter == null || filter.getShopId() == null) {
			throw new IllegalArgumentException("filter.getShopId() is null ");
		}

		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		/*sql.append(" with priceTmp1 as ( ");
		sql.append(" 		select price_id, price, price_not_vat, product_id, orderNo ");
		sql.append(" 		  from price pr join (select shop_id, (case ct.object_type when 3 then 1 when 11 then 1 when 2 then 2  ");
		sql.append(" when 10 then 2 when 1 then 3 when 9 then 3 when 13 then 3 when 0 then 4 when 8 then 4 when 12 then 4 when 14 then 5 else 10 end) as orderNo ");
		sql.append(" from shop s join channel_type ct on (ct.channel_type_id = s.shop_type_id) ");
		sql.append(" where s.status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id ");
		params.add(filter.getShopId());
		sql.append(" ) sh on (sh.shop_id = pr.shop_id) ");
		sql.append(" where status = 1 and from_date < trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null) and customer_type_id is null ");
		sql.append(" union all ");
		sql.append(" select price_id, price, price_not_vat, product_id, (case when shop_channel is not null then 6 else 7 end) as orderNo ");
		sql.append(" from price pr where status = 1 and from_date < trunc(sysdate) + 1 ");
		sql.append(" and (to_date >= trunc(sysdate) or to_date is null) and customer_type_id is null ");
		sql.append(" and shop_id is null ");
		//sql.append(" and (shop_channel in (select shop_channel from shop where shop_id = ?) or shop_channel is null)");
		sql.append(" and shop_channel in (select shop_channel from shop where shop_id = ?)");
		params.add(filter.getShopId());
		sql.append(" ), ");
		sql.append(" priceTmp as (select price_id, price, price_not_vat, product_id, row_number() over (partition by product_id order by orderNo) as r_num from priceTmp1 pr1) ");
		sql.append(" select distinct st.stock_total_id as stockTotalId, pd.product_id as productId,  pd.product_code as productCode ");
		sql.append(" , pd.product_name as productName, pd.convfact as convfact, wh.warehouse_id as warehouseId, st.available_quantity as availableQuantity, prt.price as price, pd.gross_weight as grossWeight ");
		sql.append(" from warehouse wh join stock_total st on wh.warehouse_id = st.warehouse_id join product pd on st.product_id = pd.product_id left join priceTmp prt on prt.product_id = pd.product_id and prt.r_num = 1 ");
		sql.append(" where st.object_id = ? and st.object_type = 1 and wh.shop_id = ? and wh.status = 1 and st.status = 1 and pd.status = 1 ");
		params.add(filter.getShopId());
		params.add(filter.getShopId());
		sql.append(" and (prt.price is null and pd.cat_id in (select product_info_id from product_info where type = 1 and product_info_code in ( select ap_param_code from ap_param where type like 'EQUIPMENT_CAT')) ");
		sql.append(" or prt.price is not null) ");*/
		
		sql.append("with tmp as (");
		sql.append(" select distinct st.stock_total_id as stockTotalId,");
		sql.append(" st.product_id,");
		sql.append(" wh.warehouse_id                 as warehouseId,");
		sql.append(" st.available_quantity           as availableQuantity,");
		sql.append(" psd.price as price");
		sql.append(" from warehouse wh");
		sql.append(" join stock_total st on wh.warehouse_id = st.warehouse_id");
		sql.append(" LEFT join PRICE_SHOP_DEDUCED psd on st.PRODUCT_ID = psd.PRODUCT_ID and psd.SHOP_ID = ? ");
		params.add(filter.getShopId());
		sql.append("                                 AND psd.FROM_DATE < TRUNC(SYSDATE) + 1 ");
		sql.append("                                 AND (psd.TO_DATE >= TRUNC(SYSDATE) OR psd.TO_DATE IS NULL) ");
		sql.append(" where st.object_id = ?");
		sql.append(" and st.object_type = 1");
		sql.append(" and wh.shop_id     = ?");
		params.add(filter.getShopId());
		params.add(filter.getShopId());
		sql.append(" and wh.status      = 1");
		sql.append(" and st.status      = 1");
		if (filter.getWareHouseId() != null) {
			sql.append(" and wh.warehouse_id = ? ");
			params.add(filter.getWareHouseId());
		}
		sql.append(" )");
		sql.append(" select tmp.stockTotalId, tmp.product_id as productId,");
		sql.append(" pd.product_code                 as productCode ,");
		sql.append(" pd.product_name                 as productName,");
		sql.append(" pd.convfact                     as convfact,");
		sql.append(" tmp.warehouseId, tmp.availableQuantity,");
		sql.append(" decode(tmp.price, -1, null, tmp.price) as price,");
		sql.append(" pd.gross_weight                 as grossWeight");
		sql.append(" from tmp");
		sql.append(" join product pd on (pd.product_id = tmp.product_id and pd.status = 1)");
		/*sql.append(" where ((tmp.price = -1");
		sql.append(" and pd.cat_id in (");
		sql.append(" select product_info_id");
		sql.append(" from product_info");
		sql.append(" where type = 1");
		sql.append(" and product_info_code in ( select ap_param_code from ap_param where type like 'EQUIPMENT_CAT')");
		sql.append(" )");
		sql.append(" )");
		sql.append(" or tmp.price <> -1)");*/
		sql.append(" where 1=1");
		
		/*if (filter.getWareHouseId() != null) {
			sql.append(" and wh.warehouse_id = ? ");
			params.add(filter.getWareHouseId());
		}*/
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			sql.append(" and lower(pd.product_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProductCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			sql.append(" and lower(pd.name_text) like ? ESCAPE '/' ");
			String nameTmp = Unicode2English.codau2khongdau(filter.getProductName().trim());
			params.add(StringUtility.toOracleSearchLikeSuffix(nameTmp.toLowerCase()));

		}
		//Bo sung dieu kien tim kiem neu thay can thiet
		if (filter.getLstExcId() != null && !filter.getLstExcId().isEmpty()) {
			sql.append(" and pd.product_id not in ( -1 ");
			for (Long idExc : filter.getLstExcId()) {
				sql.append(" ,? ");
				params.add(idExc);
			}
			sql.append(" ) ");
		}

		if (filter.getLstId() != null && !filter.getLstId().isEmpty()) {
			sql.append(" and pd.product_id in ( -1 ");
			for (Long idExc : filter.getLstId()) {
				sql.append(" ,? ");
				params.add(idExc);
			}
			sql.append(" ) ");
		}

		//Count total
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		
		sql.append(" order by productCode ");

		String[] fieldNames = new String[] { "stockTotalId", "productId", "productCode", "productName", "convfact", "warehouseId", "availableQuantity", "price", "grossWeight" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL };

		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StockGeneralVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(StockGeneralVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	
	@Override
	public List<StockTotalVO> getListStockTransDetail(BasicFilter<StockTotalVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstWH as ( ");
		sql.append(" select * from warehouse where status = 1 and shop_id = ? ");
		params.add(filter.getShopId());
		sql.append(" ) ");
		sql.append(" select std.product_id productId, ");
		sql.append(" (select product_code from product where product_id = std.product_id) productCode, ");
		sql.append(" (select product_name from product where product_id = std.product_id) productName, ");
		sql.append(" (select convfact from product where product_id = std.product_id) convfact, ");
		sql.append(" (select warehouse_name from lstWH where warehouse_id = st.warehouse_id) warehouseName, ");
		sql.append(" (select seq from lstWH where warehouse_id = st.warehouse_id) seq, ");
		sql.append(" st.available_quantity availableQuantity, ");
		sql.append(" stl.quantity, std.price, ");
		sql.append(" (std.price*stl.quantity) amount ");
		sql.append(" from stock_trans_detail std ");
		sql.append(" join stock_total st on st.product_id = std.product_id and st.object_type=1 and st.object_id=? ");
		params.add(filter.getShopId());
		sql.append(" join stock_trans_lot stl on stl.stock_trans_detail_id = std.stock_trans_detail_id ");
		if(filter.getFromId() != null){
			sql.append(" and stl.from_owner_type=? and stl.from_owner_id = st.warehouse_id ");
			params.add(filter.getFromId());
		}
		if(filter.getToId() != null){
			sql.append(" and stl.to_owner_type=? and stl.to_owner_id = st.warehouse_id ");
			params.add(filter.getToId());
		}
		sql.append(" where std.stock_trans_id=? ");
		params.add(filter.getId());
		sql.append(" order by productCode,seq ");
		//Count total
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(") ");
		String[] fieldNames = new String[] { 
				"productId","productCode", "productName",
				"convfact", "warehouseName", 
				"seq", "availableQuantity", "quantity", 
				"price", "amount" };

		Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL };

		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	
	@Override
	public List<StockTotalVO> getListStockTransDetailForDC(BasicFilter<StockTotalVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstWH as ( ");
		sql.append(" select * from warehouse where status = 1 and shop_id = ? ");
		params.add(filter.getShopId());
		sql.append(" ) ");
		sql.append(" select std.product_id productId, ");
		sql.append(" (select product_code from product where product_id = std.product_id) productCode, ");
		sql.append(" (select product_name from product where product_id = std.product_id) productName, ");
		sql.append(" (select convfact from product where product_id = std.product_id) convfact, ");
		if(filter.getFromId() != null){
			sql.append(" (select warehouse_name from lstWH where warehouse_id = stl.from_owner_id) warehouseName, ");
		}
		if(filter.getToId() != null){
			sql.append(" (select warehouse_name from lstWH where warehouse_id = stl.to_owner_id) warehouseName, ");
		}
		sql.append(" stl.quantity, std.price, ");
		sql.append(" (std.price*stl.quantity) amount ");
		sql.append(" from stock_trans_detail std ");
		sql.append(" join stock_trans_lot stl on stl.stock_trans_detail_id = std.stock_trans_detail_id ");
		sql.append(" where std.stock_trans_id=? ");
		params.add(filter.getId());
		sql.append(" order by productCode ");
		//Count total
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(") ");
		String[] fieldNames = new String[] { 
				"productId","productCode", "productName",
				"convfact", "warehouseName", "quantity", 
				"price", "amount" };

		Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL };

		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
}

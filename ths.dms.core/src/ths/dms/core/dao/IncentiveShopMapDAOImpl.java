package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.IncentiveShopMap;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.IncentiveShopMapFilter;
import ths.dms.core.entities.enumtype.IncentiveShopMapVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.entities.vo.QuantityVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class IncentiveShopMapDAOImpl implements IncentiveShopMapDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	private CommonDAO commonDAO;

	@Override
	public IncentiveShopMap createIncentiveShopMap(IncentiveShopMap incentiveShopMap, LogInfoVO logInfo) throws DataAccessException {
		if (incentiveShopMap == null) {
			throw new IllegalArgumentException("incentiveShopMap");
		}
//		Boolean b = this.checkIfAncestorInIp(incentiveShopMap.getShop().getId(), incentiveShopMap.getIncentiveProgram().getId());
//		if (b) 
//			incentiveShopMap.setStatus(ActiveType.DELETED);
		
		Boolean b = this.checkExistChildIncentiveShopMap(incentiveShopMap.getShop().getId(), incentiveShopMap.getIncentiveProgram().getId(), false);
		if (b) return null;
		else {
			incentiveShopMap.setCreateUser(logInfo.getStaffCode());
			repo.create(incentiveShopMap);
			List<IncentiveShopMap> lst = this.getListParentIncentiveShopMap(incentiveShopMap.getShop().getId(), incentiveShopMap.getIncentiveProgram().getId());
			for (IncentiveShopMap fsm: lst) {
				fsm.setStatus(ActiveType.DELETED);
				this.updateIncentiveShopMap(fsm, logInfo);
			}
			try {
				actionGeneralLogDAO.createActionGeneralLog(null, incentiveShopMap, logInfo);
			}
			catch (Exception ex) {
				throw new DataAccessException(ex);
			}
			return repo.getEntityById(IncentiveShopMap.class, incentiveShopMap.getId());
		}
	}

	@Override
	public void deleteIncentiveShopMap(IncentiveShopMap incentiveShopMap, LogInfoVO logInfo) throws DataAccessException {
		if (incentiveShopMap == null) {
			throw new IllegalArgumentException("incentiveShopMap");
		}
		repo.delete(incentiveShopMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(incentiveShopMap, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public IncentiveShopMap getIncentiveShopMapById(long id) throws DataAccessException {
		return repo.getEntityById(IncentiveShopMap.class, id);
	}
	
	@Override
	public void updateIncentiveShopMap(IncentiveShopMap incentiveShopMap, LogInfoVO logInfo) throws DataAccessException {
		if (incentiveShopMap == null) {
			throw new IllegalArgumentException("incentiveShopMap");
		}
		IncentiveShopMap temp = this.getIncentiveShopMapById(incentiveShopMap.getId());
		//for log purpose
		temp = temp.clone();
		incentiveShopMap.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null) {
			incentiveShopMap.setUpdateUser(logInfo.getStaffCode());
			if (logInfo.getIp() == null || StringUtility.isNullOrEmpty(logInfo.getStaffCode())) logInfo = null;
		}
		
		repo.update(incentiveShopMap);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, incentiveShopMap, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public List<IncentiveShopMap> getListIncentiveShopMap(
			Long incentiveProgramId, ActiveType status,
			KPaging<IncentiveShopMap> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from incentive_shop_map where 1 = 1");
		
		if (status != null) {
			sql.append(" and status = ?");
			params.add(status.getValue());
		}else {
			sql.append(" and status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (incentiveProgramId != null) {
			sql.append(" and incentive_program_id = ?");
			params.add(incentiveProgramId);
		}
		
		sql.append(" order by incentive_shop_map_id desc");
		if (kPaging == null){
			return repo.getListBySQL(IncentiveShopMap.class, sql.toString(), params);
		}else{
			return repo.getListBySQLPaginated(IncentiveShopMap.class, sql.toString(), params, kPaging);
		}
	}

	@Override
	public List<IncentiveShopMapVO> getListIncentiveShopMapVO(
			IncentiveShopMapFilter filter, KPaging<IncentiveShopMapVO> kPaging)
			throws DataAccessException {
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();

		selectSql.append("SELECT sm.incentive_shop_map_id id, ");
		selectSql.append("  s.shop_code shopCode, ");
		selectSql.append("  s.shop_name shopName, ");
		selectSql.append("  sm.status status ");
		fromSql.append("FROM incentive_shop_map sm ");
		fromSql.append("JOIN incentive_program pr ");
		fromSql.append("ON pr.incentive_program_id = sm.incentive_program_id ");
		fromSql.append("JOIN shop s ");
		fromSql.append("ON s.shop_id = sm.shop_id ");
		fromSql.append("WHERE 1                    = 1 ");

		List<Object> params = new ArrayList<Object>();
		if (filter.getProgramId() != null) {
			fromSql.append(" and pr.incentive_program_id = ? ");
			params.add(filter.getProgramId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			fromSql.append(" and lower(s.shop_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getShopCode().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			fromSql.append(" and lower(s.shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getShopName().toLowerCase()));
		}
		if (filter.getStatus() != null) {
			fromSql.append(" and sm.status=?");
			params.add(filter.getStatus().getValue());
		}else {
			fromSql.append(" and sm.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getParentShopId() != null) {
			fromSql.append(" and s.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
			params.add(filter.getParentShopId());
		}
		selectSql.append(fromSql.toString());
		selectSql.append(" order by s.shop_code, sm.incentive_shop_map_id");
		
		countSql.append("select count(sm.incentive_shop_map_id) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"id", "shopCode", "shopName", "status"
				};

		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER
		};	
		if (kPaging == null){
			List<IncentiveShopMapVO> lst = repo.getListByQueryAndScalar(
					IncentiveShopMapVO.class, fieldNames,
					fieldTypes, selectSql.toString(), params);
			return lst;
		}else{
			List<IncentiveShopMapVO> lst = repo.getListByQueryAndScalarPaginated(IncentiveShopMapVO.class, fieldNames, fieldTypes,
					selectSql.toString(), countSql.toString(), params, params, kPaging);
			return lst;
		}
	}

	@Override
	public List<Shop> getListShopNotJoinIncentiveProgram(
			Long incentiveProgramId, KPaging<Shop> kPaging)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT s.* ");
		sql.append("FROM shop s ");
		sql.append("WHERE s.shop_id NOT IN ");
		sql.append("  (SELECT shop_id ");
		sql.append("  FROM v_shop_tree ");
		sql.append("  WHERE root_id IN ");
		sql.append("    (SELECT sm.shop_id ");
		sql.append("    FROM incentive_program pr ");
		sql.append("    JOIN incentive_shop_map sm ");
		sql.append("    ON sm.incentive_program_id    = pr.incentive_program_id ");
		sql.append("    WHERE 1 = 1 ");
		if (incentiveProgramId != null) {
			sql.append(" and pr.incentive_program_id = ?");
			params.add(incentiveProgramId);
		}
		sql.append("    AND sm.status                != ? ");
		params.add(ActiveType.DELETED.getValue());
		sql.append("    ) ");
		sql.append("  ) ");
		sql.append("  order by s.shop_id  ");
		if (kPaging == null){
			List<Shop> lst = repo.getListBySQL(Shop.class, sql.toString(), params);
			return lst;
		}else{
			List<Shop> lst = repo.getListBySQLPaginated(Shop.class, sql.toString(), params, kPaging);
			return lst;
		}
		
	}

	@Override
	public Boolean isExistShop(Long incentiveProgramId, Long shopId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("SELECT count(1) as count ");
		sql.append("FROM incentive_shop_map dt ");
		sql.append("JOIN incentive_program pr ");
		sql.append("ON pr.incentive_program_id = dt.incentive_program_Id ");
		sql.append("WHERE 1                    = 1 ");
		if(incentiveProgramId != null){
			sql.append(" AND pr.incentive_program_id = ? ");
			params.add(incentiveProgramId);
		}
		if(shopId != null){
			sql.append(" AND dt.shop_id = ? ");
			params.add(shopId);
		}
		sql.append(" and dt.status != ?");
		params.add(ActiveType.DELETED.getValue());
		
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}

	@Override
	public List<IncentiveShopMap> getListAncestorIncentiveShopMap(
			Long incentiveProgramId, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from incentive_shop_map sm where 1 = 1");
		
		sql.append(" and sm.status != ?");
		params.add(ActiveType.DELETED.getValue());
		if (incentiveProgramId != null) {
			sql.append(" and sm.incentive_program_id = ?");
			params.add(incentiveProgramId);
		}
		if(shopId != null){
			sql.append(" and  sm.shop_id in ( SELECT SHOP_ID FROM SHOP ");
			sql.append(" where STATUS = 1");
			sql.append(" 	START WITH SHOP_ID = ?"); 
			sql.append(" 	CONNECT BY PRIOR PARENT_SHOP_ID = SHOP_ID)");
			params.add(shopId);
		}
		sql.append(" order by sm.incentive_shop_map_id desc");
		List<IncentiveShopMap> lst = repo.getListBySQL(IncentiveShopMap.class, sql.toString(), params);
		return lst;
	}
	
//	private List<IncentiveShopMap> getListIncentiveShopMapByParentShop(Long parentShopId, Long incentiveProgramId) throws DataAccessException {
//		StringBuilder sql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		sql.append("select * from incentive_shop_map f where status = 1 and incentive_program_id = ? ");
//		params.add(incentiveProgramId);
//		sql.append(" and f.shop_id != ? and f.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
//		params.add(parentShopId);
//		params.add(parentShopId);
//		return repo.getListBySQL(IncentiveShopMap.class, sql.toString(), params);
//	}
	
	private List<IncentiveShopMap> getListParentIncentiveShopMap(Long shopId, Long incentiveProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from incentive_shop_map f where status = 1 and incentive_program_id = ? ");
		params.add(incentiveProgramId);
		sql.append(" and f.shop_id != ? and f.shop_id in (select shop_id from shop start with shop_id = ? connect by prior parent_shop_id = shop_id)");
		params.add(shopId);
		params.add(shopId);
		return repo.getListBySQL(IncentiveShopMap.class, sql.toString(), params);
	}
	
	@Override
	public Boolean checkExistChildIncentiveShopMap(Long shopId, Long incentiveProgramId, Boolean exceptShopCheck) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from incentive_shop_map f where status = 1 and incentive_program_id = ?");
		params.add(incentiveProgramId);
		sql.append(" and f.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
		if(Boolean.TRUE.equals(exceptShopCheck)) {
			sql.append(" and f.shop_id != ?");
			params.add(shopId);
		}
		params.add(shopId);
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	@Override
	public Boolean checkIfAncestorInIp(Long shopId, Long incentiveProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from incentive_shop_map f where status = 1 and incentive_program_id = ? ");
		params.add(incentiveProgramId);
		sql.append(" and f.shop_id in (select shop_id from shop start with shop_id = ? connect by prior parent_shop_id = shop_id)");
		params.add(shopId);
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	
	@Override
	public Boolean isExistChildJoinProgram(String shopCode, Long incentiveProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from incentive_shop_map f where status = 1");
		if(incentiveProgramId != null){
			sql.append(" and incentive_program_id = ? ");
			params.add(incentiveProgramId);
		}
		sql.append(" and f.shop_id in (select shop_id from shop start with shop_code = ? connect by prior shop_id = parent_shop_id)");
		params.add(shopCode.toUpperCase());
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	
	@Override
	public List<Shop> getListShopInIncentiveProgram(Long incentiveProgramId, Long shopId,
			String shopCode, String shopName, Boolean isJoin)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select distinct * from shop s start with shop_id in (");
		//sql.append("	(select ps.shop_id from incentive_shop_map ps join shop sh on ps.shop_id = sh.shop_id where ps.status = 1 and incentive_program_id = ?");
		
		if (Boolean.TRUE.equals(isJoin)) {
			sql.append(" select sh.shop_id from shop sh where sh.shop_id in (select shop_id from incentive_shop_map where incentive_program_id = ? and status = 1)");
		} else {
			sql.append(" select sh.shop_id from shop sh where sh.shop_id not in (select shop_id from incentive_shop_map where incentive_program_id = ? and status = 1)");
		}
		
		params.add(incentiveProgramId);
		
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and sh.shop_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(shopCode).toUpperCase());
		}
		
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and upper(sh.shop_name) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(shopName).toUpperCase());
		}
		if (shopId != null) {
			sql.append(" and sh.shop_id = ? ");
			params.add(shopId);
		}
		
		sql.append("	) ");
		sql.append(" connect by prior parent_shop_id = shop_id and s.status = 1");

		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<ProgrameExtentVO> getListProgrameExtentVOEx(List<Shop> lstShop, Long incentiveProgramId) throws DataAccessException {
//		StringBuilder sql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		
//		sql.append(" SELECT shop_id shopId, ");
//		sql.append("   (");
//		sql.append("	select case object_type when 3 then 1 else 0 end from channel_type where channel_type_id = shop_type_id");
//		sql.append("   ) isNPP, ");
//		
//		sql.append("   (");
//		sql.append("	select count(1) from incentive_shop_map where incentive_program_id = ? and shop_id = s.shop_id and status = 1");
//		params.add(incentiveProgramId);
//		sql.append("   ) isJoin, ");
//		
//		sql.append(" (");
//		sql.append("	select count(1) from shop ss where exists (select 1 from incentive_shop_map psm where psm.shop_id = ss.shop_id and psm.status = 1 and incentive_program_id = ?) START WITH ss.shop_id = s.shop_id CONNECT BY prior ss.shop_id = ss.parent_shop_id");
//		params.add(incentiveProgramId);
//		sql.append(" ) as isChildJoin ");
//		
//		sql.append(" FROM shop s ");
//		sql.append(" WHERE status = 1 AND shop_id IN  (-1");
//		
//		for (Shop s: lstShop) {
//			sql.append(",?");
//			params.add(s.getId());
//		}
//		sql.append(")");
//		
//		String[] fieldNames = {"shopId", "isNPP", "isJoin", "isChildJoin"};
//		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER};
//		return repo.getListByQueryAndScalar(QuantityVO.class, fieldNames, fieldTypes, sql.toString(), params);
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT shop_id shopId, ");
		sql.append("   (");
		sql.append("	select case object_type when 3 then 1 else 0 end from channel_type where channel_type_id = shop_type_id");
		sql.append("   ) isNPP, ");
		sql.append("   (");
		sql.append("	select count(1) from shop ss where exists (select 1 from incentive_shop_map psm where psm.shop_id = ss.shop_id and psm.status = 1 and incentive_program_id = ?) START WITH ss.shop_id = s.shop_id CONNECT BY prior ss.shop_id = ss.parent_shop_id");
		params.add(incentiveProgramId);
		sql.append("   ) isJoin ");
		sql.append(" FROM shop s ");
		sql.append(" WHERE 1=1 ");
		
		String paramFix = StringUtility.getParamsIdFixBugTooLongParamsLstShop(lstShop, "shop_id");
		sql.append(paramFix);
		
		String[] fieldNames = {"shopId", "isNPP", "isJoin"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER};
		return repo.getListByQueryAndScalar(ProgrameExtentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<IncentiveShopMap> getListIncentiveChildShopMapWithShopAndIncentiveProgram(
			Long shopId, Long incentiveProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from incentive_shop_map where status = 1 and shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
//		sql.append(" and shop_id <> ?");
//		params.add(shopId);
		params.add(shopId);
		sql.append(" and incentive_program_id = ?");
		params.add(incentiveProgramId);
		return repo.getListBySQL(IncentiveShopMap.class, sql.toString(), params);
	}
	
	@Override
	public List<QuantityVO> getListQuantityVOEx(List<Shop> lstShop, Long incentiveProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT shop_id shopId, ");
		sql.append("   (");
		sql.append("	select case object_type when 3 then 1 else 0 end from channel_type where channel_type_id = shop_type_id");
		sql.append("   ) isNPP, ");
		sql.append("   (");
		sql.append("	select count(1) from shop ss where exists (select 1 from incentive_shop_map psm where psm.shop_id = ss.shop_id and psm.status = 1 and incentive_program_id = ?) START WITH ss.shop_id = s.shop_id CONNECT BY prior ss.shop_id = ss.parent_shop_id");
		params.add(incentiveProgramId);
		sql.append("   ) isJoin ");
		sql.append(" FROM shop s ");
		sql.append(" WHERE 1=1 ");
		
		String paramFix = StringUtility.getParamsIdFixBugTooLongParamsLstShop(lstShop, "shop_id");
		sql.append(paramFix);
		
		String[] fieldNames = {"shopId", "isNPP", "isJoin"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER};
		return repo.getListByQueryAndScalar(QuantityVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.EquipLend;
import ths.dms.core.entities.EquipLendDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.filter.EquipProposalBorrowFilter;
import ths.dms.core.entities.vo.EquipBorrowVO;
import ths.dms.core.entities.vo.EquipLendDetailVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

/**
 * Tang xu ly tuong tac DB
 * 
 * @author hoanv25
 * @since March 09,2015
 * @description Dung cho Quan Ly yeu cau muon thiet bi
 * */
public class EquipProposalBorrowDAOImpl implements EquipProposalBorrowDAO {
	@Autowired
	private IRepository repo;

	@Override
	public List<EquipBorrowVO> searchListProposalBorrowVOByFilter(EquipProposalBorrowFilter<EquipBorrowVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		if (filter == null) {
			throw new IllegalArgumentException("filter must not be null");
		}
		if (filter.getLstShopId() == null || filter.getLstShopId().size() == 0) {
			throw new IllegalArgumentException("filter.getLstShopId() == null || filter.getLstShopId().size() == 0");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShopTemp as ( ");
		sql.append(" select s.shop_id, s.shop_code, s.shop_name ");
		sql.append(" from shop s ");
		sql.append(" where s.status = 1 ");
		sql.append(" start WITH s.shop_id in (?");
		params.add(filter.getLstShopId().get(0));
		for (int i = 1, sz = filter.getLstShopId().size(); i < sz; i++) {
			sql.append(",?");
			params.add(filter.getLstShopId().get(i));
		}
		sql.append(")");
		sql.append(" CONNECT by PRIOR s.shop_id = s.parent_shop_id ");
		sql.append(" ) ");
		sql.append(" select el.equip_lend_id id, ");
		sql.append(" el.code code, ");
		sql.append(" el.note as note, ");
		sql.append(" lst.shop_code shopCode, ");
		sql.append(" lst.shop_name shopName, ");
		sql.append(" st.staff_code staffCode, ");
		sql.append(" st.staff_name staffName, ");
		sql.append(" el.status, ");
		sql.append(" to_char(el.create_form_date, 'dd/mm/yyyy') createFormDate, ");
		sql.append(" el.delivery_status statusDelivery ");
		sql.append(" from equip_lend el ");
		sql.append(" left join staff st on st.staff_id = el.user_id ");
		//		sql.append(" and st.STAFF_TYPE_ID in (select c.channel_type_id from CHANNEL_TYPE c where c.status = 1 and c.type = 2 and c.object_type = 5) ");
		sql.append(" join lstShopTemp lst on lst.shop_id = el.shop_id ");
		sql.append(" where 1=1 ");

		if (filter.getStatus() != null) {
			sql.append(" and el.status = ? ");
			params.add(filter.getStatus());
		} else {
			if (filter.getLstStatus() != null) {
				sql.append(" and el.status in (?");
				params.add(filter.getLstStatus().get(0));
				for (int i = 1, sz = filter.getLstStatus().size(); i < sz; i++) {
					sql.append(",?");
					params.add(filter.getLstStatus().get(i));
				}
				sql.append(") ");
			} else {
				sql.append(" and el.status <> ? ");
				params.add(ActiveType.DELETED.getValue());
			}			
		}

		if (filter.getDeliveryStatus() != null) {
			sql.append(" and el.DELIVERY_STATUS = ? ");
			params.add(filter.getDeliveryStatus());
		} else {
			sql.append(" and el.DELIVERY_STATUS <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}

		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and el.code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(filter.getSuperviseCode())) {
			sql.append(" and st.staff_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSuperviseCode().trim().toUpperCase()));
		}

		if (filter.getFromDate() != null) {
			sql.append(" and el.CREATE_FORM_DATE >= trunc(?) ");
			params.add(filter.getFromDate());
		}

		if (filter.getToDate() != null) {
			sql.append(" and el.CREATE_FORM_DATE < trunc(?)+1 ");
			params.add(filter.getToDate());
		}

		String[] fieldNames = { "id", //1
				"code", //2
				"note",
				"shopCode", //3 
				"shopName", //4 
				"staffCode", //5
				"staffName", //6
				"status", //7
				"createFormDate", //8  
				"statusDelivery" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.INTEGER, //7
				StandardBasicTypes.STRING, //8
				StandardBasicTypes.INTEGER };

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");

		sql.append(" ORDER BY lst.shop_code, el.code, el.create_form_date ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipBorrowVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipBorrowVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public EquipLend getEquipLendByCode(String code) throws DataAccessException {
		// TODO Auto-generated method stub
		if (code == null) {
			throw new IllegalArgumentException("code == null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_lend where code = ? ");
		params.add(code.toUpperCase().trim());
		return repo.getEntityBySQL(EquipLend.class, sql.toString(), params);
	}

	@Override
	public void updateEquipLend(EquipLend equipLend) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipLend == null) {
			throw new IllegalArgumentException("equipLend == null");
		}
		repo.update(equipLend);
	}

	@Override
	public EquipLend createEquipLend(EquipLend equipLend) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipLend == null) {
			throw new IllegalArgumentException("equipLend == null");
		}
		return repo.create(equipLend);
	}

	@Override
	public EquipLendDetail createEquipLendDetail(EquipLendDetail equipLendDetail) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipLendDetail == null) {
			throw new IllegalArgumentException("equipLendDetail == null");
		}
		return repo.create(equipLendDetail);
	}

	@Override
	public List<EquipLendDetailVO> getListEquipLendDetailVOByFilter(EquipProposalBorrowFilter<EquipBorrowVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		if (filter == null) {
			throw new IllegalArgumentException("filter must not be null");
		}
		if (filter.getLstId() == null || filter.getLstId().size() == 0) {
			throw new IllegalArgumentException("filter.getLstId() == null || filter.getLstId().size() == 0");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select eldt.EQUIP_LEND_ID equipLendId, ");
		sql.append(" eldt.EQUIP_LEND_DETAIL_ID id, ");
		sql.append(" el.code equipLendCode, ");
		sql.append(" el.note as note, ");
		sql.append(" sh.shop_code shopCode, ");
		sql.append(" sh.shop_name shopName, ");
		/*sql.append(" vung.shop_code shopCodeVung, "); // neu la kenh ST se ko co shopCodeKenh (do kenh ST ko co NPP)
		sql.append(" mien.shop_code shopCodeMien, ");
		sql.append(" kenh.shop_code shopCodeKenh, ");*/
		sql.append(" cs.short_code customerCode, ");
		sql.append(" cs.customer_code customerCodeLong, ");
		sql.append(" cs.customer_name customerName, ");
		sql.append(" eldt.REPRESENTATIVE, ");
		sql.append(" eldt.RELATION, ");
		sql.append(" eldt.PHONE, ");
		sql.append(" eldt.ADDRESS, ");
		sql.append(" eldt.STREET, ");
		sql.append(" phuong.area_code wardCode, ");
		sql.append(" eldt.WARD_NAME ward, ");
		sql.append(" quan.area_code districtCode, ");
		sql.append(" eldt.DISTRICT_NAME district, ");
		sql.append(" tinh.area_code provinceCode, ");
		sql.append(" eldt.PROVINCE_NAME province, ");
		sql.append(" eldt.IDNO, ");
		sql.append(" eldt.IDNO_PLACE idnoPlace, ");
		sql.append(" to_char(eldt.IDNO_DATE,'dd/mm/yyyy') idnoDate, ");
		sql.append(" eldt.BUSINESS_NO businessNo, ");
		sql.append(" eldt.BUSINESS_NO_PLACE businessNoPlace, ");
		sql.append(" to_char(eldt.BUSINESS_NO_DATE,'dd/mm/yyyy') businessNoDate, ");
		sql.append(" eldt.CUSTOMER_ADDRESS shopAddress, ");
		sql.append(" eldt.quantity, ");		
		sql.append(" ec.name equipCategory, ");
		sql.append(" eldt.capacity capacity, ");
		sql.append(" ec.code equipCategoryCode, ");
		sql.append(" ap.AP_PARAM_NAME healthStatus, ");
		sql.append(" ap.AP_PARAM_CODE healthStatusCode, ");
		sql.append(" TO_CHAR(eldt.TIME_LEND,'dd/mm/yyyy') timeLend, ");
		sql.append(" eldt.object_type stockTypeValue, ");
		sql.append(" eldt.AMOUNT, ");
		sql.append(" st.staff_code staffCode, ");
		sql.append(" st.staff_name staffName, ");
		sql.append(" st.PHONE staffPhone, ");
		sql.append(" st.mobilephone staffMobile ");
		sql.append(" from EQUIP_LEND_DETAIL eldt ");
		sql.append(" join customer cs on cs.customer_id = eldt.customer_id ");
		sql.append(" join area tinh on tinh.area_id = eldt.PROVINCE_ID ");
		sql.append(" join area quan on quan.area_id = eldt.DISTRICT_ID ");
		sql.append(" join area phuong on phuong.area_id = eldt.WARD_ID ");
		sql.append(" join shop sh on sh.shop_id = cs.shop_id ");
		/*sql.append(" join shop vung on sh.parent_shop_id = vung.shop_id ");
		sql.append(" left join shop mien on vung.parent_shop_id = mien.SHOP_ID ");
		sql.append(" left join shop kenh on mien.PARENT_SHOP_ID = kenh.SHOP_ID ");*/
		sql.append(" join EQUIP_CATEGORY ec on eldt.EQUIP_CATEGORY_ID = ec.EQUIP_CATEGORY_ID ");
		sql.append(" join EQUIP_LEND el on el.EQUIP_LEND_ID = eldt.EQUIP_LEND_ID ");
		sql.append(" left join staff st on el.user_id = st.staff_id ");
		sql.append(" join AP_PARAM ap on ap.AP_PARAM_CODE = eldt.HEALTH_STATUS and ap.TYPE = ? and ap.status = ? ");
		params.add(ApParamType.EQUIP_CONDITION.getValue());
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" where 1=1 ");
		sql.append(" and eldt.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and eldt.EQUIP_LEND_ID in (? ");
		params.add(filter.getLstId().get(0));
		for (int i = 1, sz = filter.getLstId().size(); i < sz; i++) {
			sql.append(",?");
			params.add(filter.getLstId().get(i));
		}
		sql.append(" ) ");
		String[] fieldNames = { 
				"id", //0
				"equipLendId", //1
				"equipLendCode", 
				"note",
				"shopCode", //2
				"shopName", //3
				/*"shopCodeVung", 
				"shopCodeMien", //2
				"shopCodeKenh", //3 */
				"customerCode", //4
				"customerCodeLong",
				"customerName", //5
				"representative", //6
				"relation", //7
				"phone", //8
				"address", //9
				"street", //10
				"wardCode", //11
				"ward", //11
				"districtCode", //12
				"district", //12
				"provinceCode", //13
				"province", //13
				"idNo", //14
				"idNoPlace", //15
				"idNoDate", //16
				"businessNo", //17
				"businessNoPlace", //18
				"businessNoDate", //19
				"shopAddress", 
				"quantity", 
				"equipCategory", //20
				"capacity",
				"equipCategoryCode", //20
				"healthStatus", 
				"healthStatusCode", 
				"timeLend", //21
				"stockTypeValue", //22
				"amount", //23
				"staffCode", //24
				"staffName", //24.1
				"staffMobile",
				"staffPhone" };
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, //0
				StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3 
				/*StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3*/
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.STRING, //7
				StandardBasicTypes.STRING, //8
				StandardBasicTypes.STRING, //9
				StandardBasicTypes.STRING, //10
				StandardBasicTypes.STRING, //11
				StandardBasicTypes.STRING, //11
				StandardBasicTypes.STRING, //12
				StandardBasicTypes.STRING, //12
				StandardBasicTypes.STRING, //13
				StandardBasicTypes.STRING, //13
				StandardBasicTypes.STRING, //14
				StandardBasicTypes.STRING, //15
				StandardBasicTypes.STRING, //16
				StandardBasicTypes.STRING, //17
				StandardBasicTypes.STRING, //18
				StandardBasicTypes.STRING, //19
				StandardBasicTypes.STRING, 
				StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.STRING, //20
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, //20
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, //21
				StandardBasicTypes.INTEGER, //22
				StandardBasicTypes.BIG_DECIMAL, //23
				StandardBasicTypes.STRING, //24
				StandardBasicTypes.STRING, //24.1
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING };

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");

		sql.append(" ORDER BY el.code, cs.customer_code ");
		return repo.getListByQueryAndScalar(EquipLendDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public String getCodeEquipLend() throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select max(code) as maxCode from equip_lend ");
		String code = (String) repo.getObjectByQuery(sql.toString(), params);
		if (code == null) {
			return "DNM00000001";
		}
		code = code.substring(code.length() - 8);
		code = "00000000" + (Long.parseLong(code.toString()) + 1);
		code = code.substring(code.length() - 8);
		code = "DNM" + code;
		return code;
	}

	@Override
	public void updateEquipLendDetail(EquipLendDetail equipLendDetail) throws DataAccessException {
		// TODO Auto-generated method stub
		if (equipLendDetail == null) {
			throw new IllegalArgumentException("equipLendDetail == null");
		}
		repo.update(equipLendDetail);
	}

	@Override
	public List<EquipLendDetail> getListEquipLendDetailByFilter(EquipProposalBorrowFilter<EquipBorrowVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		if (filter == null) {
			throw new IllegalArgumentException("filter must not be null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from EQUIP_LEND_DETAIL eldt ");
		sql.append(" where 1=1 ");
		sql.append(" and eldt.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		if (filter.getLstId() != null && filter.getLstId().size() > 0) {
			sql.append(" and eldt.EQUIP_LEND_DETAIL_ID in (? ");
			params.add(filter.getLstId().get(0));
			for (int i = 1, sz = filter.getLstId().size(); i < sz; i++) {
				sql.append(",?");
				params.add(filter.getLstId().get(i));
			}
			sql.append(" ) ");
		}
		if (filter.getLstObjectId() != null && filter.getLstObjectId().size() > 0) {
			sql.append(" and eldt.EQUIP_LEND_ID in (? ");
			params.add(filter.getLstObjectId().get(0));
			for (int i = 1, sz = filter.getLstObjectId().size(); i < sz; i++) {
				sql.append(",?");
				params.add(filter.getLstObjectId().get(i));
			}
			sql.append(" ) ");
		}
		sql.append(" ORDER BY eldt.EQUIP_LEND_ID, eldt.customer_id ");
		return repo.getListBySQL(EquipLendDetail.class, sql.toString(), params);
	}
	
	@Override
	public List<EquipLend> getListEquipLendByFilter(EquipProposalBorrowFilter<EquipBorrowVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		if (filter == null) {
			throw new IllegalArgumentException("filter must not be null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from EQUIP_LEND el ");
		sql.append(" where 1=1 ");
		if (filter.getRecordStatus() != null){
			sql.append(" and el.status = ? ");
			params.add(filter.getRecordStatus());
		} else {
			sql.append(" and el.status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getLstId() != null && !filter.getLstId().isEmpty()){
			sql.append(" and el.EQUIP_LEND_ID in (? ");
			params.add(filter.getLstId().get(0));
			for (int i = 1, sz = filter.getLstId().size(); i < sz; i++) {
				sql.append(",?");
				params.add(filter.getLstId().get(i));
			}
			sql.append(" ) ");
		}
		sql.append(" ORDER BY el.equip_lend_id ");
		return repo.getListBySQL(EquipLend.class, sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.DisplayShopMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.StDisplayProgram;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.RelationType;
import ths.dms.core.entities.filter.ImageFilter;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class DisplayShopMapDAOImpl implements DisplayShopMapDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	CommonDAO commonDAO;
	
//	@Autowired
//	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	DisplayProgrameDAO displayProgramDAO;

	@Autowired
	ShopDAO shopDAO;

	@Override
	public DisplayShopMap createDisplayShopMap(DisplayShopMap displayShopMap,LogInfoVO logInfo) throws DataAccessException {
		if (displayShopMap == null) {
			throw new IllegalArgumentException(ExceptionCode.DISPLAY_SHOP_MAP_INSTANCE_IS_NULL);
		}
		if (logInfo == null) {
			throw new IllegalArgumentException(ExceptionCode.LOG_INFO_IS_NULL);
		}

		Boolean b = this.checkExistChildDisplayShopMap(displayShopMap.getShop().getId(), displayShopMap.getDisplayProgram().getId(), false);
		if (b) return null;
		else {
			displayShopMap.setCreateUser(logInfo.getStaffCode());
			displayShopMap = repo.create(displayShopMap);
			List<DisplayShopMap> lst = this.getListParentDisplayShopMap(displayShopMap.getShop().getId(), displayShopMap.getDisplayProgram().getId());
			for (DisplayShopMap dsm: lst) {
				dsm.setStatus(ActiveType.DELETED);
				this.updateDisplayShopMap(dsm, logInfo);
			}
			try {
				//actionGeneralLogDAO.createActionGeneralLog(null, displayShopMap, logInfo);
			}
			catch (Exception ex) {
				throw new DataAccessException(ex);
			}
			return repo.getEntityById(DisplayShopMap.class, displayShopMap.getId());
		}
	}

	@Override
	public void deleteDisplayShopMap(DisplayShopMap displayShopMap,
			LogInfoVO logInfo) throws DataAccessException {
		if (displayShopMap == null) {
			throw new IllegalArgumentException(ExceptionCode.DISPLAY_SHOP_MAP_INSTANCE_IS_NULL);
		}
		if (logInfo == null) {
			throw new IllegalArgumentException(ExceptionCode.LOG_INFO_IS_NULL);
		}
		repo.delete(displayShopMap);
		try {
			//actionGeneralLogDAO.createActionGeneralLog(displayShopMap, null,logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public DisplayShopMap getDisplayShopMapById(long id) throws DataAccessException {
		return repo.getEntityById(DisplayShopMap.class, id);
	}

	@Override
	public void updateDisplayShopMap(DisplayShopMap displayShopMap,
			LogInfoVO logInfo) throws DataAccessException {
		if (displayShopMap == null) {
			throw new IllegalArgumentException(ExceptionCode.DISPLAY_SHOP_MAP_INSTANCE_IS_NULL);
		}
		if (logInfo == null) {
			throw new IllegalArgumentException(ExceptionCode.LOG_INFO_IS_NULL);
		}
		DisplayShopMap temp = this.getDisplayShopMapById(displayShopMap.getId());
		//for log purpose
		temp = temp.clone();
		displayShopMap.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null) {
			displayShopMap.setUpdateUser(logInfo.getStaffCode());
			if (logInfo.getIp() == null || StringUtility.isNullOrEmpty(logInfo.getStaffCode())) logInfo = null;
		}
		
		repo.update(displayShopMap);
		try {
			//actionGeneralLogDAO.createActionGeneralLog(temp, displayShopMap,logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public List<DisplayShopMap> getListDisplayShopMapByDisplayProgramId(
			KPaging<DisplayShopMap> kPaging, Long displayProgramId,
			String shopCode, String shopName, ActiveType status,
			Long parentShopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select f.* from display_shop_map f join shop s on f.shop_id = s.shop_id where 1=1 ");

		if (status != null) {
			sql.append(" and f.status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and f.status!=?");
			params.add(ActiveType.DELETED.getValue());// sua lai ko delete
		}

		if (displayProgramId != null) {
			sql.append(" and f.display_program_id=?");
			params.add(displayProgramId);
		} else
			sql.append(" and f.display_program_id is null");

		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and s.shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode
					.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and lower(s.shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shopName.toLowerCase()));
		}

		if (parentShopId != null) {
			sql.append(" and f.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
			params.add(parentShopId);
		}

		sql.append(" order by s.shop_code asc");

		if (kPaging == null)
			return repo.getListBySQL(DisplayShopMap.class, sql.toString(),
					params);
		else
			return repo.getListBySQLPaginated(DisplayShopMap.class,
					sql.toString(), params, kPaging);
	}
	
	@Override
	public Boolean checkIfRecordExist(long displayProgrameId, String shopCode,
			Long displayShopMapId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		shopCode = shopCode.toUpperCase();
		sql.append(" select count(1) as count from display_shop_map m where display_program_id=? ");
		sql.append("  and exists (select 1 from shop s where m.shop_id=s.shop_id and shop_code=?) and status!=?");
		List<Object> params = new ArrayList<Object>();
		params.add(displayProgrameId);
		params.add(shopCode);
		params.add(ActiveType.DELETED.getValue());

		if (displayShopMapId != null) {
			sql.append(" and display_shop_map_id!=?");
			params.add(displayShopMapId);
		}

		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}
	
	@Override
	public Boolean createListDisplayShopMap(Long displayProgramId,
			List<Long> lstShopId, ActiveType status, LogInfoVO logInfo)
			throws DataAccessException {
		if (displayProgramId == null) {
			throw new IllegalArgumentException("focusProgramId is null");
		}
		List<DisplayShopMap> lstDiplayShopMap = new ArrayList<DisplayShopMap>();
		List<DisplayShopMap> lstDisplayShopMapUpdate = new ArrayList<DisplayShopMap>();
		StDisplayProgram pg = this.displayProgramDAO
				.getStDisplayProgramById(displayProgramId);
		if (pg == null) {
			throw new IllegalArgumentException("focusProgram is null");
		}

		List<Shop> lstShop = new ArrayList<Shop>();
		List<Long> lstShopId1 = new ArrayList<Long>();

		for (Long shopId : lstShopId) {
			if (shopId == null)
				continue;
			Shop s = shopDAO.getShopById(shopId);
			if (s != null
					&& !lstShopId1.contains(s.getId())) {
				lstShop.add(s);
				lstShopId1.add(s.getId());
			}
		}

		for (Shop s : lstShop) {
			if (s == null) {
				throw new IllegalArgumentException("shop is null");
			}
			DisplayShopMap m = this.getDisplayShopMap(displayProgramId,
					s.getId(), null);

			// for log purpose
			if (m != null) {
				m = m.clone();
			}

			if (m == null) {
				if (lstShopId.contains(s.getId())) {
					m = new DisplayShopMap();
					m.setDisplayProgram(pg);
					m.setShop(s);
					m.setStatus(ActiveType.RUNNING);
					m.setCreateUser(logInfo.getStaffCode());
					lstDiplayShopMap.add(m);
				}
			} else if (status != null) {
				m.setStatus(status);
				lstDisplayShopMapUpdate.add(m);
			}
		}
		if (lstDiplayShopMap.size() > 0) {
			for (DisplayShopMap fsm : lstDiplayShopMap) {
				fsm = this.createDisplayShopMap(fsm, logInfo);
			}
		}
		if (lstDisplayShopMapUpdate.size() > 0) {
			for (DisplayShopMap fsm : lstDisplayShopMapUpdate) {
				this.updateDisplayShopMap(fsm, logInfo);
			}
		}
		if (lstDiplayShopMap.size() == 0 && lstDisplayShopMapUpdate.size() == 0)
			return false;
		return true;
	}

	// ------------------------
	private Shop getShopInLevel1(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select *");
		sql.append(" from shop s");
		sql.append(" where exists (select 1 from shop pr where pr.shop_id = s.parent_shop_id and pr.parent_shop_id is null)");
		sql.append(" start with s.shop_id = ? connect by prior s.parent_shop_id = s.shop_id");
		params.add(shopId);

		return repo.getEntityBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public DisplayShopMap getDisplayShopMap(long displayProgramId, Long shopId,
			Long displayShopMapId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from display_shop_map m where display_program_id = ? and m.shop_id = ? and status != ?");
		List<Object> params = new ArrayList<Object>();
		params.add(displayProgramId);
		params.add(shopId);
		params.add(ActiveType.DELETED.getValue());
		if (displayShopMapId != null) {
			sql.append(" and focus_shop_map_id!=?");
			params.add(displayShopMapId);
		}
		return repo.getFirstBySQL(DisplayShopMap.class, sql.toString(), params);
	}

	@Override
	public void createDisplayShopMapByLst(
			List<DisplayShopMap> lstDisplayShopMap, ActiveType status, LogInfoVO logInfo)
			throws DataAccessException {
		Map<Long, List<Long>> dpMap = new HashMap<Long, List<Long>>();
		if(lstDisplayShopMap != null && lstDisplayShopMap.size() > 0){
			for (DisplayShopMap child : lstDisplayShopMap) {
				if(child.getDisplayProgram().getId() != null && child.getShop().getId() != null){
					if(dpMap.get(child.getDisplayProgram().getId()) != null){
						List<Long> lstTemp = dpMap.get(child.getDisplayProgram().getId());
						lstTemp.add(child.getShop().getId());
					}else{
						List<Long> lstTemp = new ArrayList<Long>();
						lstTemp.add(child.getShop().getId());
						dpMap.put(child.getDisplayProgram().getId(), lstTemp);
					}
				}
			}
			if(!dpMap.isEmpty()){
				for (Long dpId : dpMap.keySet()) {
					createListDisplayShopMap(dpId, dpMap.get(dpId), status, logInfo);
				}
			}
		}
	}
	
	@Override
	public List<DisplayShopMap> getListDisplayShopMapByDp(KPaging<DisplayShopMap> kPaging, 
			String displayProgramCode, String displayProgramName,
			RelationType relation, ActiveType status, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select dsm.* from display_shop_map dsm join display_program dg on dsm.display_program_id = dg.display_program_id ");
		sql.append(" 	join shop s on s.shop_id = dsm.shop_id");
		sql.append("	where dsm.status != -1");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(displayProgramCode)) {
			sql.append(" and upper(dg.display_program_code) like ? ESCAPE '/' ");
			params.add(StringUtility
					.toOracleSearchLikeSuffix(displayProgramCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(displayProgramName)) {
			sql.append(" and lower(dg.display_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(displayProgramName
					.toLowerCase()));
		}
		if (relation != null) {
			sql.append(" and dg.relation=?");
			params.add(relation.getValue());
		}
		if (status != null) {
			sql.append(" and dg.status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and dg.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (fromDate != null) {
			sql.append(" and ((dg.to_date is not null and dg.to_date >= trunc(?)) or dg.to_date is null)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and ((dg.from_date is not null and dg.from_date < trunc(?) + 1) or dg.from_date is null)");
			params.add(toDate);
		}
		sql.append(" order by dg.display_program_code, s.shop_code ");
		if (kPaging == null)
			return repo.getListBySQL(DisplayShopMap.class, sql.toString(),
					params);
		else
			return repo.getListBySQLPaginated(DisplayShopMap.class,
					sql.toString(), params, kPaging);
	}
	
	@Override
	public Boolean checkIfAncestorInDp(Long shopId, Long displayProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from display_shop_map f where status != -1 and display_program_id = ? ");
		params.add(displayProgramId);
		sql.append(" and f.shop_id in (select shop_id from shop start with shop_id = ? connect by prior parent_shop_id = shop_id)");
		params.add(shopId);
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	
	/*****************************	THONGNM ****************************************/
	@Override
	public List<Shop> getListShopInDisplayProgram(Long displayProgramId,String shopCode, String shopName, Boolean isJoin)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct * from shop s start with shop_id in (");
		if (Boolean.TRUE.equals(isJoin)) {
			sql.append(" select sh.shop_id from shop sh where sh.shop_id in (select shop_id from display_shop_map where display_program_id = ? and status = 1)");
		} else {
			sql.append(" select sh.shop_id from shop sh where sh.shop_id not in (select shop_id from display_shop_map where display_program_id = ? and status = 1)");
		}
		params.add(displayProgramId);
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and sh.shop_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(shopCode).toUpperCase());
		}
		
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and upper(sh.shop_name) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(shopName).toUpperCase());
		}
		sql.append("	) ");
		sql.append(" connect by prior parent_shop_id = shop_id");
		sql.append(" order by shop_code ");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	@Override
	public List<Shop> getListShopActiveInDisplayProgram(Long displayProgramId,String shopCode, String shopName)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct * from shop s where  exists ( select 1 from display_shop_map where display_program_id = ? and status =1 and shop_id = s.shop_id)");
		params.add(displayProgramId);
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and s.shop_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(shopCode).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and upper(s.shop_name) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(shopName).toUpperCase());
		}
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	@Override
	public List<ProgrameExtentVO> getListProgrameExtentVOEx(List<Shop> lstShop, Long displayProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT s.shop_id as shopId, ct.object_type as isNPP ,");
		sql.append(" (");
		sql.append("    SELECT COUNT(1) ");
		sql.append("    FROM shop ss ");
		sql.append("    WHERE EXISTS ");
		sql.append("      (SELECT 1 ");
		sql.append("       FROM display_shop_map dsm ");
		sql.append("       WHERE dsm.shop_id = ss.shop_id ");
		sql.append("       AND dsm.status = 1 ");
		sql.append("       AND display_program_id = ? )");
		params.add(displayProgramId);
		sql.append("    START WITH ss.shop_id = s.shop_id CONNECT BY PRIOR ss.shop_id = ss.parent_shop_id");
		sql.append(" ) isJoin ");
		sql.append(" FROM shop s ");
		sql.append(" join channel_type ct on ct.channel_type_id = s.shop_type_id ");
		sql.append(" WHERE 1=1 ");
		
		String paramFix = StringUtility.getParamsIdFixBugTooLongParamsLstShop(lstShop, "shop_id");
		sql.append(paramFix);
		
		String[] fieldNames = {"shopId", "isNPP", "isJoin"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER};
		return repo.getListByQueryAndScalar(ProgrameExtentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	private List<DisplayShopMap> getListParentDisplayShopMap(Long shopId, Long incentiveProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from display_shop_map f where status = 1 and display_program_id = ? ");
		params.add(incentiveProgramId);
		sql.append(" and f.shop_id != ? and f.shop_id in (select shop_id from shop start with shop_id = ? connect by prior parent_shop_id = shop_id)");
		params.add(shopId);
		params.add(shopId);
		return repo.getListBySQL(DisplayShopMap.class, sql.toString(), params);
	}
	@Override
	public List<DisplayShopMap> getListChildDisplayShopMap(Long shopId, Long displayProgramId,  Boolean exceptShopCheck) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from display_shop_map f where status = 1 and display_program_id = ?");
		params.add(displayProgramId);
		sql.append(" and f.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
		params.add(shopId);
		if(Boolean.TRUE.equals(exceptShopCheck)) {
			sql.append(" and f.shop_id != ?");
			params.add(shopId);
		}
		return repo.getListBySQL(DisplayShopMap.class, sql.toString(), params);
	}
	@Override
	public Boolean checkExistChildDisplayShopMap(Long shopId, Long displayProgramId, Boolean exceptShopCheck) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShop as (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
		params.add(shopId);
		sql.append("select count(1) count from display_shop_map f where status = 1 and display_program_id = ?");
		params.add(displayProgramId);
		sql.append(" and f.shop_id in (select shop_id from lstShop)");
		if(Boolean.TRUE.equals(exceptShopCheck)) {
			sql.append(" and f.shop_id != ?");
			params.add(shopId);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	
	@Override
	public Boolean checkExistRecordDisplayShopMap(Long shopId, Long displayProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from display_shop_map f where status = 1 and display_program_id = ? and f.shop_id = ?");
		params.add(displayProgramId);
		params.add(shopId);
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	
	
	@Override
	public List<ImageVO> getListCTTB(ImageFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		StringBuilder sqlTemp = new StringBuilder("");
		sql.append(" WITH ");
		if (filter.getLstShop() != null && !filter.getLstShop().isEmpty()) {
			sql.append(" lstShop AS ");
			sql.append(" ( SELECT DISTINCT PS.SHOP_ID ");
			sql.append(" FROM SHOP ps ");
			sql.append(" START WITH ps.SHOP_ID  in ( ");
			sql.append(" ? ");
			params.add(filter.getLstShop().get(0));
			for (int i = 1; i < filter.getLstShop().size(); i++) {
				sql.append(" ,? ");
				params.add(filter.getLstShop().get(i));
			}
			sql.append(" ) CONNECT BY PRIOR ps.SHOP_ID = ps.PARENT_SHOP_ID ");
			sql.append(" ), ");
		}
		if (!StringUtility.isNullOrEmpty(filter.getLevel())) {
			sql.append(" lstLevel as (SELECT regexp_substr(?,'[^,]+', 1, level) FROM dual ");
			sql.append("    CONNECT BY regexp_substr(?, '[^,]+', 1, level) IS NOT NULL), ");
			params.add(filter.getLevel().toUpperCase());
			params.add(filter.getLevel().toUpperCase());

		}
		sql.append(" lstTmp as ( ");
		sql.append(" SELECT ");
		sql.append(" MI.* ");
		sql.append(" FROM ");
		sql.append(" MEDIA_ITEM mi");
		sql.append(" JOIN STAFF st on st.staff_id=mi.staff_id AND st.staff_type_id in (select staff_type_id from staff where staff_type_id in (select staff_type_id from staff_type where specific_type in (1,2))) ");
		sql.append("  ");
		sql.append(" AND MI.STATUS=1 ");
		sql.append(" AND MI.MEDIA_TYPE=0 ");
		sql.append(" AND MI.CREATE_DATE>=trunc(ADD_MONTHS(trunc(SYSDATE,'MM'),-2)) ");
		sql.append(" AND MI.DISPLAY_PROGRAM_ID IS NOT NULL ");
		if (null != filter.getObjectType()) {
			sql.append(" AND MI.OBJECT_TYPE =? ");
			params.add(filter.getObjectType());
		}
		if (filter.getLstShop() != null) {
			sql.append(" AND MI.SHOP_ID IN ");
			sql.append(" ( SELECT SHOP_ID FROM lstShop ");
			sql.append(" ) ");
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopIdListStr())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getShopIdListStr(), "MI.SHOP_ID");
			sql.append(paramsFix);
		}
		if (filter.getKey() != null && filter.getKey() != -1) {
			switch (filter.getKey()) {
			case 2:
				sqlTemp.append(" AND rc.MONDAY=1 ");
				break;
			case 3:
				sqlTemp.append(" AND rc.TUESDAY=1 ");
				break;
			case 4:
				sqlTemp.append(" AND rc.WEDNESDAY=1 ");
				break;
			case 5:
				sqlTemp.append(" AND rc.THURSDAY=1 ");
				break;
			case 6:
				sqlTemp.append(" AND rc.FRIDAY=1 ");
				break;
			case 7:
				sqlTemp.append(" AND rc.SATURDAY=1 ");
				break;
			case 8:
				sqlTemp.append(" AND rc.SUNDAY=1 ");
				break;
			}
			sql.append(" AND EXISTS ");
			sql.append(" ( ");
			sql.append(" SELECT 1 FROM VISIT_PLAN vp inner join routing r on vp.routing_id = r.routing_id and r.status = 1 inner join routing_customer rc on vp.routing_id = rc.routing_id and rc.status in (0,1) AND rc.start_date <= TRUNC(sysdate,'dd') AND (rc.end_date  >= TRUNC(ADD_MONTHS(TRUNC(SYSDATE,'MM'),-2)) OR rc.end_date           IS NULL) ");
			sql.append(sqlTemp);
			sql.append(" WHERE MI.OBJECT_ID=rc.CUSTOMER_ID ");
			if (filter.getLstStaff() != null && !filter.getLstStaff().isEmpty()) {
				sql.append(" AND MI.STAFF_ID = VP.STAFF_ID ");
			}
			sql.append(" AND MI.shop_id=VP.Shop_id ");
			sql.append(" AND MI.routing_id= r.routing_id ");
			sql.append(" and vp.status in (0, 1)");
			sql.append(" ) ");
		}

		if (!StringUtility.isNullOrEmpty(filter.getCodeCustomer()) || !StringUtility.isNullOrEmpty(filter.getShortCode()) || !StringUtility.isNullOrEmpty(filter.getNameCustomer())) {
			sql.append(" AND MI.OBJECT_ID IN ");
			sql.append(" ( ");
			sql.append(" SELECT cus.CUSTOMER_ID FROM CUSTOMER cus ");
			sql.append(" WHERE 1 = 1 ");

			if (!StringUtility.isNullOrEmpty(filter.getCodeCustomer())) {
				sql.append(" AND cus.CUSTOMER_CODE LIKE ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getCodeCustomer().toUpperCase().trim()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
				sql.append(" AND cus.short_code LIKE ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(filter.getShortCode().toUpperCase().trim()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getNameCustomer())) {
				sql.append(" AND lower(cus.name_text) LIKE ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getNameCustomer().toLowerCase())));
			}
			sql.append(" ) ");
		} 
		if (filter.getLstStaff() != null && !filter.getLstStaff().isEmpty()) {
			sql.append(" AND MI.STAFF_ID IN ( ");
			for (int i = 0; i < filter.getLstStaff().size(); i++) {
				if (i == 0) {
					sql.append(" ? ");
				} else {
					sql.append(" ,? ");
				}
				params.add(filter.getLstStaff().get(i));
			}
			sql.append(" ) ");
		}
		if (filter.getFromDate() != null) {
			sql.append(" AND MI.CREATE_DATE>=TRUNC(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" AND MI.CREATE_DATE<TRUNC(?)+1 ");
			params.add(filter.getToDate());
		}
		if (!StringUtility.isNullOrEmpty(filter.getLevel())) {
			sql.append(" JOIN DISPLAY_CUSTOMER_MAP cm ON MI.OBJECT_ID = CM.CUSTOMER_ID AND cm.status=1 ");
			sql.append(" JOIN display_program_level dpl ON cm.display_program_level_id=dpl.display_program_level_id and dpl.status=1 ");
			sql.append(" JOIN display_staff_map dsm ON dsm.display_staff_map_id = cm.display_staff_map_id and dsm.status=1 ");
			sql.append(" AND dSM.DISPLAY_PROGRAM_ID = mi.DISPLAY_PROGRAM_ID ");
			sql.append(" AND trunc(cm.from_date)<=trunc(mi.create_date) and (cm.to_date is null or cm.to_date>=trunc(mi.create_date)) ");
			sql.append(" AND upper(dpl.level_code) in (SELECT * from lstLevel) ");
		}
		sql.append(" Where 1=1 ");
		if (filter.getLstDisplayPrograme() != null && !filter.getLstDisplayPrograme().isEmpty()) {
			sql.append(" AND MI.DISPLAY_PROGRAM_ID IN (");
			for (int i = 0; i < filter.getLstDisplayPrograme().size(); i++) {
				if (i == 0) {
					sql.append(" ? ");
				} else {
					sql.append(" ,? ");
				}
				params.add(filter.getLstDisplayPrograme().get(i));
			}
			sql.append(" ) ");
		}
		sql.append(" ) ");
		sql.append(" SELECT ");
		sql.append(" DP.KS_ID as displayProgrameId, ");
		sql.append(" (select KS_CODE||' - '|| name from ks where KS_ID=DP.KS_ID) as displayProgrameCode, ");
		sql.append(" MEI.MEDIA_ITEM_ID as id,");
		sql.append(" MEI.url AS urlImage, ");
		sql.append(" MEI.thumb_url AS urlThum, ");
		sql.append(" (SELECT COUNT(*) FROM lstTmp WHERE LSTTMP.DISPLAY_PROGRAM_ID=DP.KS_ID) as countImg ");
		sql.append(" FROM KS_SHOP_MAP dp,lstTmp mei,customer cus ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND cus.customer_id = mei.object_id ");
		sql.append(" AND DP.KS_ID=MEI.DISPLAY_PROGRAM_ID ");
		sql.append(" AND DP.STATUS = 1 ");
		sql.append(" AND MEI.MEDIA_ITEM_ID= ");
		sql.append(" ( ");
		sql.append(" SELECT L1.MEDIA_ITEM_ID FROM lstTmp l1 WHERE l1.DISPLAY_PROGRAM_ID=DP.KS_ID AND ROWNUM=1 ");
		sql.append(" AND l1.CREATE_DATE=( ");
		sql.append(" SELECT max(l2.CREATE_DATE) FROM lstTmp l2 WHERE L2.DISPLAY_PROGRAM_ID=DP.KS_ID ");
		sql.append(" ) ");
		sql.append(" ) ");
		sql.append(" order by displayprogramecode ");

		String[] fieldNames = { "displayProgrameId",//2
				"displayProgrameCode",//3
				"id",//4
				"urlImage",//4
				"urlThum",//5
				"countImg"//6
		};

		Type[] fieldTypes = {

		StandardBasicTypes.LONG, // 2
				StandardBasicTypes.STRING,//3
				StandardBasicTypes.LONG, // 2
				StandardBasicTypes.STRING,//4
				StandardBasicTypes.STRING,//5
				StandardBasicTypes.INTEGER // 6
		};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from (").append(sql).append(" ) ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(ImageVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	
	//SangTN
	@Override
	public List<ImageVO> getListCTTBByNPP(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct k.ks_id displayProgrameId, k.ks_code displayProgrameCode, k.name displayProgrameName, ");
		sql.append(" k.FROM_CYCLE_ID fromCycle, k.TO_CYCLE_ID toCycle, cc.end_date as createDate");
		sql.append(" from ks_shop_map ksm ");
		sql.append(" join ks k on k.ks_id = ksm.ks_id ");
		sql.append(" join cycle cc on cc.cycle_id = k.TO_CYCLE_ID ");
		sql.append(" where ksm.status = 1 ");
		sql.append(" and k.PROGRAM_TYPE = 1 ");
		sql.append(" and cc.end_date >= sysdate ");
		sql.append(" AND ksm.SHOP_ID IN ");
		sql.append(" ( SELECT DISTINCT PS.SHOP_ID FROM SHOP ps ");
		sql.append(" START WITH ps.SHOP_ID=? ");
		if (id != null) {
			params.add(id);
		}
		sql.append(" CONNECT BY PRIOR ps.SHOP_ID = ps.PARENT_SHOP_ID ");
		sql.append(" ) ");
		sql.append(" AND EXISTS ");
		sql.append(" (SELECT 1 FROM ks k WHERE k.status =1 AND k.KS_ID = ksm.KS_ID) ");

		String[] fieldNames = { "displayProgrameId",//1
				"displayProgrameCode",//2
				"displayProgrameName",//3
				"fromCycle", "toCycle", "createDate" };

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING,//2
				StandardBasicTypes.STRING,//3
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.DATE };
		sql.append(" ORDER BY cc.end_date ");
		return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	
	//SangTN
	@Override
	public List<ImageVO> getListCTTBByListShop(List<Long> lstShopId) throws DataAccessException {
		 StringBuilder sql = new StringBuilder();
		 List<Object> params = new ArrayList<Object>();
		 
		 sql.append(" SELECT DISTINCT ksm.KS_SHOP_MAP_ID AS displayProgrameId, ");
		 sql.append(" ( SELECT ks_code FROM ks WHERE ks_id = ksm.ks_id ) AS displayProgrameCode , ");
		 sql.append(" ( SELECT name FROM ks WHERE ks_id = ksm.ks_id ) AS displayProgrameName, ");
		 sql.append(" ( SELECT FROM_CYCLE_ID FROM ks WHERE ks_id = ksm.ks_id) as fromCycle, ");
		 sql.append(" ( SELECT TO_CYCLE_ID FROM ks WHERE ks_id = ksm.ks_id) as toCycle ");
		 sql.append(" FROM KS_SHOP_MAP ksm ");
		 sql.append(" WHERE ksm.STATUS =1 ");
		 sql.append(" AND ksm.SHOP_ID IN ");
		 sql.append(" ( ");
		 sql.append(" ( SELECT DISTINCT PS.SHOP_ID FROM SHOP ps ");
		 sql.append(" START WITH ps.SHOP_ID in ( ");
		 if(lstShopId!=null && lstShopId.size()>0){
			 for(int i=0;i<lstShopId.size();i++){
				 if(i==0) sql.append("?");
				 else sql.append(",?");
				 params.add(lstShopId.get(i));
			 }
		 }
		 sql.append(" ) CONNECT BY PRIOR ps.SHOP_ID = ps.PARENT_SHOP_ID ");
		 sql.append(" )) ");
		 sql.append(" AND EXISTS ");
		 sql.append(" ( ");		 
		 sql.append(" (SELECT 1 FROM ks k WHERE k.status =1 AND k.KS_ID = ksm.KS_ID)) ");				 
		 String[] fieldNames = { 
					"displayProgrameId",//1
					"displayProgrameCode",//2
					"displayProgrameName",//3
					"fromCycle",
					"toCycle"
					};

		Type[] fieldTypes = { 
					StandardBasicTypes.LONG, // 1
					StandardBasicTypes.STRING,//2
					StandardBasicTypes.STRING,//3
					StandardBasicTypes.INTEGER,
					StandardBasicTypes.INTEGER
					};
		
		sql.append(" ORDER BY toCycle desc ");				
		return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<StDisplayProgram> getListDisplayProgramByShopAndYear(KPaging<StDisplayProgram> kPaging,
			Long shopID, Integer year, String code, String name) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String endDate = "31/12/"+year;
		String beginDate = "01/01/"+year;
		sql.append(" select * from display_program dp");
		 sql.append(" join display_shop_map dsm");
		 sql.append(" on dsm.display_program_id = dp.display_program_id");
		 sql.append(" where ");
		 sql.append(" dsm.shop_id in(select shop_id from shop where status = 1 start with shop_id = ? connect by prior PARENT_SHOP_ID = shop_id)");
		 params.add(shopID);
		 sql.append(" and trunc(dp.from_date) <= trunc(to_date(?,'dd/mm/yyyy'))");
		 params.add(endDate);
		 sql.append(" and (trunc(dp.to_date) >= trunc(to_date(?,'dd/mm/yyyy')) or dp.to_date is null)");
		 params.add(beginDate);
		 sql.append(" and dp.status = 1");
		 sql.append(" and dsm.status =1");
		 if(!StringUtility.isNullOrEmpty(code)) {
				sql.append("and lower(dp.display_program_code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(code.toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(name)) {
			sql.append("and lower(dp.display_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(name.toLowerCase()));
		}
		 if(kPaging!=null){
			 return repo.getListBySQLPaginated(StDisplayProgram.class, sql.toString(), params, kPaging);
		 }else{
			 return repo.getListBySQL(StDisplayProgram.class, sql.toString(), params);
		 }
	}

}

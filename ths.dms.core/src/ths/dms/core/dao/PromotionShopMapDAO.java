package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PromotionShopMapFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PromotionShopMapVO;
import ths.dms.core.entities.vo.PromotionShopVO;
import ths.dms.core.entities.vo.QuantityVO;
import ths.dms.core.exceptions.DataAccessException;

public interface PromotionShopMapDAO {

	PromotionShopMap createPromotionShopMap(PromotionShopMap promotionShopMap, LogInfoVO logInfo) throws DataAccessException;

	void deletePromotionShopMap(PromotionShopMap promotionShopMap, LogInfoVO logInfo) throws DataAccessException;

	void updatePromotionShopMap(PromotionShopMap promotionShopMap, LogInfoVO logInfo) throws DataAccessException;
	
	PromotionShopMap getPromotionShopMapById(long id) throws DataAccessException;

	Boolean checkIfRecordExist(long promotionProgramId, String shopCode, Long id)
			throws DataAccessException;

	List<PromotionShopMap> getListPromotionShopMapForUpdate(
			KPaging<PromotionShopMap> kPaging, Long promotionProgramId,
			String shopCode, String shopName, Integer shopQuantity,
			ActiveType status) throws DataAccessException;

	PromotionShopMap getPromotionShopMap(Long shopId, Long promotionProgramId)
			throws DataAccessException;

	List<PromotionShopMap> getListPromotionShopMap(
			KPaging<PromotionShopMap> kPaging, Long promotionProgramId,
			String shopCode, String shopName, Integer shopQuantity,
			ActiveType status, Long parentShopId) throws DataAccessException;

	PromotionShopMap checkPromotionShopMap(Long promotionProgramId, String shopCode,
			ActiveType status,Date lockDay) throws DataAccessException;

	/**
	*  Mo ta chuc nang cua ham
	*  @author: thanhnn
	*  @param listLong
	*  @return
	*  @return: List<PromotionShopMap>
	*  @throws:
	*/
	List<PromotionShopMap> getListPromotionShopMapWithListPromotionId(
			List<Long> listPromotionProgramId) throws DataAccessException;
	
	List<PromotionShopMap> getListPromotionShopMapWithShopAndPromotionProgram(
			Long shopId, Long promotionProgramId) throws DataAccessException;
	
	Boolean checkExitsPromotionWithParentShop(Long promotionProgramid,
			String shopCode) throws DataAccessException;

	List<Shop> getListShopInPromotionProgram(Long promotionProgramId,
			String shopCode, String shopName, Integer quantity)
			throws DataAccessException;

	List<QuantityVO> getListQuantityVO(List<Shop> lstShop,
			Long promotionProgramId) throws DataAccessException;

	List<QuantityVO> getListQuantityVOEx(List<Shop> lstShop,
			Long promotionProgramId) throws DataAccessException;

	List<PromotionShopMap> getListPromotionChildShopMapWithShopAndPromotionProgram(
			Long shopId, Long promotionProgramId) throws DataAccessException;

	Boolean isExistChildJoinProgram(String shopCode, Long promotionProgramId, Boolean exceptShopCheck)
			throws DataAccessException;

	PromotionShopMap createOnePromotionShopMap(PromotionShopMap promotionShopMap, LogInfoVO logInfo)
			throws DataAccessException;

	/**
	*  Kiem tra ton tai PromotionShopMap doi voi don vi con cua don vi nhap vao 
	*  @author: thongnm1
	*/
	Boolean checkExistChildPromotionShopMap(Long shopId,Long promotionProgramId, Boolean exceptShopCheck)
			throws DataAccessException;

	List<PromotionShopMapVO> getListPromotionShopMapVO(PromotionShopMapFilter filter, KPaging<PromotionShopMapVO> kPaging) throws DataAccessException;

	Boolean checkCTHMShopMap(String promotionProgramCode, Long shopId,
			ActiveType status, Date lockDay) throws DataAccessException;
	
	/**
	 * Lay danh sach don vi cho CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 15, 2014
	 */
	List<PromotionShopVO> getShopTreeInPromotionProgram(long shopId, long promotionId, String shopCode, String shopName, Integer quantity) throws DataAccessException;
	
	/**
	 * Kiem tra xem don vi+don vi con co ton tai trong CTKM chua
	 * 
	 * @author lacnv1
	 * @since Aug 20, 2014
	 */
	List<PromotionShopVO> getListShopInPromotion(long promotionId, List<Long> lstShopId, boolean shopMapOnly) throws DataAccessException;
	
	/**
	 * Lay shop_map con cua shop_id
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	PromotionShopMap getPromotionParentShopMap(long promotionId, long shopId, Integer status) throws DataAccessException;
	
	/**
	 * Lay promotion_shop_map cua shop con chau
	 * 
	 * @param promotionId - id CTKM
	 * @param shopId - id cua shop can lay promotion_shop_map cua shop con chau
	 * @param status - trang thai cua promotion_shop_map can lay, neu de null thi lay khac DELETE
	 * 
	 * @author lacnv1
	 * @since Apr 13, 2015
	 */
	List<PromotionShopMap> getPromotionChildrenShopMap(long promotionId, long shopId, Integer status) throws DataAccessException;
	
	/**
	 * Tim kiem don vi trong CTKM 
	 * 
	 * @param promotionId, shopCode, shopName, quantity, isLevel
	 * @author hunglm16
	 * @since March 24,2015
	 * */
	List<PromotionShopVO> searchShopTreeInPromotionProgramImpr(PromotionShopMapFilter filter) throws DataAccessException;

	/**
	 * kiem tra ton tai PromotionShopMap theo phan quyen
	 * 
	 * @param strListShopId
	 * @param promotionProgramId
	 * @return
	 * @throws DataAccessException
	 */
	boolean checkExistPromotionShopMapByListShop(String strListShopId, Long promotionProgramId) throws DataAccessException;

	/**
	 * 
	 * @param lstId
	 * @param promotionId
	 * @return danh sach PromotionShopMap theo id khach hang
	 * @throws DataAccessException
	 * @author trietptm
	 * @since Aug 03, 2015
	 */
	List<PromotionShopMap> getPromotionShopMapByCustomer(List<Long> lstId, Long promotionId) throws DataAccessException;

	/**
	 * Lay danh sach don vi tham gia CTKM
	 * @author hunglm16
	 * @param filter
	 * @param kPaging
	 * @return
	 * @throws DataAccessException
	 * @since 25/09/2015
	 */
	List<PromotionShopMapVO> getPromotionShopMapVOByFilter(PromotionShopMapFilter filter, KPaging<PromotionShopMapVO> kPaging) throws DataAccessException;

	/**
	 * Lay danh sach don vi popup them don vi vao CTKM
	 * @author hunglm16
	 * @param promotionId
	 * @param pShopId
	 * @param shopCode
	 * @param shopName
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return
	 * @throws DataAccessException
	 * @since 26/11/2015
	 */
	List<PromotionShopVO> searchShopForPromotionProgram(long promotionId, long pShopId, String shopCode, String shopName, Long staffRootId, Long roleId, Long shopRootId) throws DataAccessException;
	
	/**
	 * 
	 * Xu ly lay promotionShopMapID tu function db
	 * @author dungdq3
	 * @param shopID
	 * @param promotionProgramID
	 * @return Long: promotionShopMapID
	 * @throws DataAccessException
	 * @since Jan 26, 2016
	 */
	Long getPromotionShopMapFromFunction(Long shopID, Long promotionProgramID) throws DataAccessException;

	/**
	 * lay ds PromotionShopMap gom ca shop cha 
	 * @author trietptm
	 * @param promotionProgramId
	 * @param shopId
	 * @param status
	 * @return List<PromotionShopMap>
	 * @throws DataAccessException
	 * @since Feb 19, 2016
	*/
	List<PromotionShopMap> getPromotionShopMapIncludeParentShop(long promotionProgramId, long shopId, Integer status) throws DataAccessException;
}
package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.PromotionCustAttr;
import ths.dms.core.entities.PromotionCustAttrDetail;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PromotionCustAttVO2;
import ths.dms.core.exceptions.DataAccessException;

public interface PromotionCustAttrDAO {

	PromotionCustAttr createPromotionCustAttr(PromotionCustAttr promotionCustAttr, LogInfoVO logInfo) throws DataAccessException;

	void deletePromotionCustAttr(PromotionCustAttr promotionCustAttr, LogInfoVO logInfo) throws DataAccessException;

	void updatePromotionCustAttr(PromotionCustAttr promotionCustAttr, LogInfoVO logInfo) throws DataAccessException;
	
	PromotionCustAttr getPromotionCustAttrById(long id) throws DataAccessException;
	
	List<PromotionCustAttVO2> getListPromotionCustAttVOValue(long promotionProgramId) throws DataAccessException;
	
	List<PromotionCustAttVO2> getListPromotionCustAttVOValueDetail(long promotionProgramId) throws DataAccessException;
	
	void createOrUpdatePromotionCustAttDynamicVO(PromotionCustAttr proCusAtt)throws DataAccessException;
	
	void createOrUpdatePromotionCustAttDynamicVO(PromotionCustAttr proCusAtt,List<PromotionCustAttrDetail> proCusAttDetail,LogInfoVO logInfo)throws DataAccessException;

	PromotionCustAttr getPromotionCustAttrByPromotion(long promotionId,
			Integer objectType, Long objectId) throws DataAccessException;

	void deletePromotionCustAttrInfo(Long promotionProgramId,
			List<Long> lstExceptAttrId, LogInfoVO logInfo) throws DataAccessException;

	List<PromotionCustAttr> getLstPromotionCustAttrByPP(Long promotionProgramId)
			throws DataAccessException;
}

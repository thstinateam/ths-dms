package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.StaffPositionLog;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.entities.vo.SuperVisorInfomationVO;
import ths.dms.core.exceptions.DataAccessException;

public interface StaffPositionLogDAO {

	StaffPositionLog createStaffPositionLog(StaffPositionLog staffPositionLog,
			LogInfoVO logInfo) throws DataAccessException;

	void deleteStaffPositionLog(StaffPositionLog staffPositionLog,
			LogInfoVO logInfo) throws DataAccessException;

	void updateStaffPositionLog(StaffPositionLog staffPositionLog,
			LogInfoVO logInfo) throws DataAccessException;

	StaffPositionLog getStaffPositionLogById(long id)
			throws DataAccessException;

	/**
	*  Mo ta chuc nang cua ham
	*  @author: thanhnn
	*  @param parentStaffId
	*  @return
	*  @return: List<SuperVisorInfomationVO>
	*  @throws:
	*/
	List<SuperVisorInfomationVO> getInfomationOfParentSuper(Long parentStaffId)
	throws DataAccessException;
	
	List<StaffPositionVO> getListStaffPosition(Long ownerId,
			StaffObjectType objectType) throws DataAccessException;
	
	List<StaffPositionVO> getListSuperPosition(Long staffId) throws DataAccessException;

	/**
	*  Mo ta chuc nang cua ham
	*  @author: thanhnn
	*  @param superStaffId
	*  @return
	*  @return: List<SuperVisorInfomationVO>
	*  @throws:
	*/
	List<SuperVisorInfomationVO> getInfomationOfSuper(Long superStaffId) throws DataAccessException;

	/**
	*  Mo ta chuc nang cua ham
	*  @author: thanhnn
	*  @param staffId
	*  @return
	*  @return: SuperVisorInfomationVO
	*  @throws:
	*/
	List<SuperVisorInfomationVO> getInfomationOfStaff(Long staffId) throws DataAccessException;

	/**
	 * Lay staff position (co them wifi)
	 * @author vuongmq
	 * @param staffId
	 * @param shopId
	 * @return StaffPositionLog
	 * @throws DataAccessException
	 * @since 17/08/2015
	 */
	StaffPositionLog getNewestStaffPositionLog(Long staffId, Long shopId) throws DataAccessException;
	
	/**
	 * Lay vi tri cuoi cung
	 * @author trietptm
	 * @param staffId
	 * @param date
	 * @param fromTime
	 * @param toTime
	 * @return
	 * @throws DataAccessException
	 */
	StaffPositionLog getLastStaffPositionLog(Long staffId, Date date, String fromTime, String toTime) throws DataAccessException;
}
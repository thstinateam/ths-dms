package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Product;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.StDisplayPdGroup;
import ths.dms.core.entities.StDisplayPdGroupDtl;
import ths.dms.core.entities.StDisplayProgram;
import ths.dms.core.entities.StDisplayProgramVNM;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.DisplayProgramFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StDisplayPdGroupFilter;
import ths.dms.core.entities.enumtype.StDisplayProgramFilter;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.enumtype.StaffRoleType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class DisplayProgrameDAOImpl implements DisplayProgrameDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private CommonDAO commonDAO;

	@Override
	public Integer checkExistsProductDisplayGr(Long dpId, DisplayProductGroupType type) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(*) as count");
		sql.append(" from display_product_group sdpg ");
		sql.append(" where type=? and status=1 and display_program_id = ?");		
		params.add(type.getValue());
		params.add(dpId);
		sql.append(" and exists(select 1 from display_product_group_dtl sdpgdtl");
		sql.append(" where sdpg.display_product_group_id = sdpgdtl.display_product_group_id and status = 1)");		
		return repo.countBySQL(sql.toString(), params);
	}

	@Override
	public StDisplayProgram getStDisplayProgramById(long id)
			throws DataAccessException {
		return repo.getEntityById(StDisplayProgram.class, id);
	}

	
	@Override
	public List<StDisplayProgram> getListDisplayProgramSysdateEX(List<Long> shopId,
			Date month) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException(
					"getListDisplayProgramSysdate-shopId is null");
		}
		if (month == null) {
			throw new IllegalArgumentException(
					"getListDisplayProgramSysdate-month is null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH lstShop AS ");
		sql.append(" (SELECT DISTINCT shop_id FROM shop START WITH shop_id in('' ");

		for(int i=0;i<shopId.size();i++){
			sql.append(" ,? ");
			params.add(shopId.get(i));
		}
		sql.append(" ) ");
		sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id ");
		sql.append(" ) ");
		sql.append(" select dp.* ");
		sql.append(" from display_program dp ");
		sql.append(" where dp.status = 1 ");
		sql.append(" and trunc(dp.from_date,'MONTH') <= trunc(?) ");
		params.add(month);
		sql.append(" and (trunc(?) <= trunc(dp.to_date,'MONTH') ");
		params.add(month);
		sql.append(" or dp.to_date is null) ");
		sql.append(" and EXISTS (select 1 from display_shop_map ds ");
		sql.append(" where dp.display_program_id = ds.display_program_id and ds.status =1 ");
		sql.append(" and ds.shop_id in (SELECT shop_id FROM lstShop) ");
		sql.append(" ) ");
		sql.append(" order by dp.display_program_code ");
		return repo.getListBySQL(StDisplayProgram.class, sql.toString(), params);
	}

	@Override
	public Boolean checkDisplayProgrameInShop(Long displayProgrameId,
			Long shopId, Date month) throws DataAccessException {
		if (displayProgrameId == null) {
			throw new IllegalArgumentException(
					"checkDisplayProgrameInShop-displayProgrameId is null");
		}
		if (shopId == null) {
			throw new IllegalArgumentException(
					"checkDisplayProgrameInShop-shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH lstShop AS ");
		sql.append(" (SELECT DISTINCT shop_id FROM shop START WITH shop_id = ? CONNECT BY PRIOR shop_id = parent_shop_id ");
		params.add(shopId);
		sql.append(" ) ");
		sql.append("select count(*) as count from (");
		sql.append(" select dp.* ");
		sql.append(" from display_program dp ");
		sql.append(" where dp.status = 1 ");
		sql.append(" and dp.display_program_id = ? ");
		params.add(displayProgrameId);
		sql.append(" and trunc(dp.from_date,'MONTH') <= trunc(?) ");
		params.add(month);
		sql.append(" and (trunc(?) <= trunc(dp.to_date,'MONTH') ");
		params.add(month);
		sql.append(" or dp.to_date is null) ");
		sql.append(" and EXISTS (select 1 from display_shop_map ds ");
		sql.append(" where dp.display_program_id = ds.display_program_id and ds.status =1 ");
		sql.append(" and ds.shop_id in (SELECT shop_id FROM lstShop) ");
		sql.append(" )) ");
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public Boolean checkDisplayProgrameInShopForCustomer(
			Long displayProgrameId, Long shopIdOfCustomer, Date month)
			throws DataAccessException {
		if (displayProgrameId == null) {
			throw new IllegalArgumentException(
					"checkDisplayProgrameInShopForCustomer-displayProgrameId is null");
		}
		if (shopIdOfCustomer == null) {
			throw new IllegalArgumentException(
					"checkDisplayProgrameInShopForCustomer-shopIdOfCustomer is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(*) as count from (");
		sql.append(" select dp.* ");
		sql.append(" from display_program dp ");
		sql.append(" where dp.status = 1 ");
		sql.append(" and dp.display_program_id = ? ");
		params.add(displayProgrameId);
		sql.append(" and trunc(dp.from_date,'MONTH') <= trunc(?) ");
		params.add(month);
		sql.append(" and (trunc(?) <= trunc(dp.to_date,'MONTH') ");
		params.add(month);
		sql.append(" or dp.to_date is null) ");
		sql.append(" and EXISTS (select 1 from display_shop_map ds ");
		sql.append(" where dp.display_program_id = ds.display_program_id and ds.status =1 ");
		sql.append(" and ds.shop_id = ? ");
		params.add(shopIdOfCustomer);
		sql.append(" )) ");
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	/**
	 * Search CTTB
	 * 
	 * @author thachnn9
	 */
	@Override
	public List<StDisplayProgram> getListDisplayProgram(
			KPaging<StDisplayProgram> kPaging, StDisplayProgramFilter filter,
			StaffRoleType staffRole) throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null
				&& filter.getFromDate().after(filter.getToDate()))
			return new ArrayList<StDisplayProgram>();
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
			
		if (!StaffRoleType.VNM.equals(staffRole)) {
			if (filter.getShopId() == null) {
				throw new IllegalArgumentException("shopId is null");
			}
			sql.append("with lstshop as (select distinct shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
			params.add(filter.getShopId());
		}

		sql.append("select * from display_program dp where 1=1");
		if (!StringUtility.isNullOrEmpty(filter.getDisplayProgramCode())) {
			sql.append(" and upper(display_program_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDisplayProgramCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getDisplayProgramName())) {
			sql.append(" and lower(display_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getDisplayProgramName().toLowerCase()));
		}
		if (filter.getStatus() != null) {
			sql.append(" and status=?");
			params.add(filter.getStatus().getValue());
		}else{
			if(!StaffRoleType.VNM.equals(staffRole)){
				sql.append(" and status in (0,1)");
			}
		}
		if (filter.getFromDate() != null) {
			sql.append(" and ((to_date is not null and to_date >= trunc(?)) or to_date is null)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and ((from_date is not null and from_date < trunc(?) + 1) or from_date is null)");
			params.add(filter.getToDate());
		}
		if (!StaffRoleType.VNM.equals(staffRole)) {
			sql.append(" and exists(select 1 from display_shop_map dsm where dsm.display_program_id = dp.display_program_id  and dsm.status = 1 and exists (select 1 from lstshop where dsm.shop_id = shop_id))");
		}
		sql.append(" order by from_date desc, to_date desc, display_program_code asc ");
		if (kPaging == null)
			return repo.getListBySQL(StDisplayProgram.class, sql.toString(),
					params);
		else
			return repo.getListBySQLPaginated(StDisplayProgram.class,
					sql.toString(), params, kPaging);
	}

	// loctt
	@Override
	public StDisplayProgram getStDisplayProgramById(Long id)
			throws DataAccessException {
		return repo.getEntityById(StDisplayProgram.class, id);
	}

	/**
	 * Danh sach them moi CTTB Loai tru
	 * 
	 * @author loctt
	 * @since 17sep2013
	 */

	@Override
	public List<StDisplayProgram> getListDisplayProgramForExclusion(
			KPaging<StDisplayProgram> kPaging, DisplayProgramFilter filter)
			throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null
				&& filter.getFromDate().after(filter.getToDate()))
			return new ArrayList<StDisplayProgram>();
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		if (filter.getDisplayProgramId() == null) {
			throw new IllegalArgumentException("displayProgramId is null");
		}
		sql.append(" select * from display_program where 1=1");
		if (!StringUtility.isNullOrEmpty(filter.getDisplayProgramCode())) {
			sql.append(" and upper(display_program_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter
					.getDisplayProgramCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getDisplayProgramName())) {
			sql.append(" and lower(display_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter
					.getDisplayProgramName().toLowerCase()));
		}
		sql.append(" and (status=? or status = ?)");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.WAITING.getValue());
		// sql.append(" and from_date < trunc(sysdate) + 1");
		sql.append(" and (to_date >= trunc(sysdate) or to_date is null)");
//loctt1 sep17
		sql.append(" and display_program_id not in (select display_p_exclusion_id from display_program_exclusion where display_program_id = ? and status =?) ");
		params.add(filter.getDisplayProgramId());
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and display_program_id != ?");
		params.add(filter.getDisplayProgramId());
		sql.append(" order by display_program_code ");
		if (kPaging == null)
			return repo.getListBySQL(StDisplayProgram.class, sql.toString(),
					params);
		else
			return repo.getListBySQLPaginated(StDisplayProgram.class,
					sql.toString(), params, kPaging);
	}
	
	/**
	 * @author thachnn
	 */
	@Override
	 public List<StDisplayPdGroup> getListDisplayGroup(KPaging<StDisplayPdGroup> kPaging, StDisplayPdGroupFilter filter,
			 StaffRoleType staffRole) throws DataAccessException{
		
		/*if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate()))
			return new ArrayList<StDisplayProgram>();*/
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from display_product_group dp ");
		if(filter.getType()!= null){
			sql.append(" where type=?");
			params.add(filter.getType().getValue());
		}
		
		if(filter.getId()!= null){
			sql.append(" and display_program_id=?");
			params.add(filter.getId());
		}
		sql.append(" order by type asc ");

		if (kPaging == null)
			return repo.getListBySQL(StDisplayPdGroup.class, sql.toString(),params);
		else
			return repo.getListBySQLPaginated(StDisplayPdGroup.class,sql.toString(), params, kPaging);
	}
	
	/**
	 * @author thachnn
	 */
	@Override
	 public List<StDisplayPdGroupDtl> getListDisplayGroupDetail(KPaging<StDisplayPdGroupDtl> kPaging, Long stDisplayPdGroupId,
			 StaffRoleType staffRole) throws DataAccessException{
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select dpg.* from display_product_group_dtl dpg join product p on p.product_id = dpg.product_id where 1=1 and dpg.status != -1");
		if(stDisplayPdGroupId != null){
			sql.append(" and dpg.display_product_group_id = ? ");
			params.add(stDisplayPdGroupId);
		}
		sql.append(" order by p.product_code");
		if (kPaging == null)
			return repo.getListBySQL(StDisplayPdGroupDtl.class, sql.toString(),params);
		else
			return repo.getListBySQLPaginated(StDisplayPdGroupDtl.class,sql.toString(), params, kPaging);
	}
	/**
	 * @author thachnn
	 */
	@Override
	public void updateStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup, LogInfoVO logInfo) 
	throws DataAccessException{
		try{
		if (stDisplayPdGroup == null) {
			throw new IllegalArgumentException("displayProductGroup is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (stDisplayPdGroup.getDisplayProductGroupCode() != null)
			stDisplayPdGroup.setDisplayProductGroupCode(stDisplayPdGroup.getDisplayProductGroupCode().toUpperCase());
		stDisplayPdGroup.setUpdateDate((Date) repo.getObjectByQuery("SELECT SYSDATE FROM DUAL", null));
		stDisplayPdGroup.setUpdateUser(logInfo.getStaffCode());
		repo.update(stDisplayPdGroup);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public StDisplayProgram getStDisplayProgramByCode(String code)	throws DataAccessException {		
		code = code.toUpperCase();
		String sql = "select * from DISPLAY_PROGRAM where display_program_code=? and status!=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		return repo.getEntityBySQL(StDisplayProgram.class, sql, params);
	}

	@Override
	public String checkUpdateDisplayProgramForActive(Long stDisplayProgramId)throws DataAccessException {		
		return null;
	}

	@Override
	public void updateDisplayProgram(StDisplayProgram stDisplayProgram,
			LogInfoVO logInfo) throws DataAccessException {
		if (stDisplayProgram == null) {
			throw new IllegalArgumentException("displayProgram");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (stDisplayProgram.getDisplayProgramCode() != null)
			stDisplayProgram.setDisplayProgramCode(stDisplayProgram.getDisplayProgramCode().toUpperCase());
		StDisplayProgram temp = this.getStDisplayProgramById(stDisplayProgram.getId());
		stDisplayProgram.setUpdateDate(commonDAO.getSysDate());
		stDisplayProgram.setUpdateUser(logInfo.getStaffCode());
		temp = temp.clone();
		repo.update(stDisplayProgram);		
	}

	@Override
	public StDisplayProgram createDisplayProgram(StDisplayProgram stDisplayProgram, LogInfoVO logInfo)throws DataAccessException {
		if (stDisplayProgram == null) {
			throw new IllegalArgumentException("displayProgram");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (stDisplayProgram.getDisplayProgramCode() != null)
			stDisplayProgram.setDisplayProgramCode(stDisplayProgram.getDisplayProgramCode().toUpperCase());
		stDisplayProgram.setCreateUser(logInfo.getStaffCode());
		repo.create(stDisplayProgram);		
		return repo.getEntityById(StDisplayProgram.class, stDisplayProgram.getId());
	}
	
	@Override
	public StDisplayPdGroup getStDisplayPdGroupByCode(Long stDisplayProgramId,DisplayProductGroupType type, 
			String stDisplayPdGroupCode) throws DataAccessException{
		if (stDisplayProgramId == null) {
			throw new IllegalArgumentException("stDisplayProgramId is null");
		}
		if (type == null) {
			throw new IllegalArgumentException("type is null");
		}
		if (StringUtility.isNullOrEmpty(stDisplayPdGroupCode)) {
			throw new IllegalArgumentException("stDisplayPdGroupCode is null");
		}
		stDisplayPdGroupCode = stDisplayPdGroupCode.toUpperCase();
		String sql = "select * from display_product_group where display_program_id = ? and type = ? and display_dp_group_code =? and status =? ";
		List<Object> params = new ArrayList<Object>();
		params.add(stDisplayProgramId);
		params.add(type.getValue());
		params.add(stDisplayPdGroupCode.toUpperCase());
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(StDisplayPdGroup.class, sql, params);
	}
	
	@Override
	public StDisplayPdGroup createStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup, LogInfoVO logInfo)throws DataAccessException{
		if (stDisplayPdGroup == null) {
			throw new IllegalArgumentException("displayProductGroup is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (stDisplayPdGroup.getDisplayProductGroupCode() != null)
			stDisplayPdGroup.setDisplayProductGroupCode(stDisplayPdGroup.getDisplayProductGroupCode().toUpperCase());
		stDisplayPdGroup.setCreateUser(logInfo.getStaffCode());
		stDisplayPdGroup = repo.create(stDisplayPdGroup);
		/*try {
			actionGeneralLogDAO.createActionGeneralLog(null, displayProductGroup, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}*/
		return stDisplayPdGroup;//repo.getEntityById(StDisplayPdGroup.class, stDisplayPdGroup.getId());
	}
	
	@Override
	public void deleteStDisplayPdGroup(StDisplayPdGroup stDisplayPdGroup) throws DataAccessException{
		try{
			repo.delete(stDisplayPdGroup);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	/*
	 * author: lochp
	 * date: 19/09/2013
	 * getlistDisplayGroup order by type, DISPLAY_PD_GROUP_CODE ASC
	 * cap nhat ham ngay 23/9/2013
	 */
	@Override
	 public List<StDisplayPdGroup> getListDisplayProgrameAmount(KPaging<StDisplayPdGroup> kPaging, StDisplayPdGroupFilter filter,
			StaffRole staffRole) throws DataAccessException{
		
		/*if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate()))
			return new ArrayList<StDisplayProgram>();*/
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from display_product_group dp ");
		if (filter.getDisplayProductGroupCode() != null || filter.getId()!= null)
			sql.append(" where status != -1 and type != 4 ");
		if (filter.getDisplayProductGroupCode() != null){
			sql.append(" and display_dp_group_code = ?");
			params.add(filter.getDisplayProductGroupCode());
		}
		if(filter.getId()!= null){
			sql.append(" and display_program_id=?");
			params.add(filter.getId());
		}
		
		if(filter.getType()!= null){
			sql.append(" and  type=?");
			params.add(filter.getType().getValue());
		}

		sql.append(" order by type,DISPLAY_DP_GROUP_CODE asc ");

		if (kPaging == null)
			return repo.getListBySQL(StDisplayPdGroup.class, sql.toString(),params);
		else
			return repo.getListBySQLPaginated(StDisplayPdGroup.class,sql.toString(), params, kPaging);
	}

	
	

	/*
	 * author: lochp
	 * 20/09/2013
	 * hien thi chi tiet san pham cua 1 nhom
	 * (non-Javadoc)
	 * @see ths.dms.core.dao.DisplayProgrameDAO#getListDisplayProgrameAmountProductDetail(ths.dms.core.entities.enumtype.StDisplayPdGroupFilter, ths.dms.core.entities.enumtype.StaffRole)
	 */
	@Override
	public List<StDisplayPdGroupDtl> getListDisplayProgrameAmountProductDetail(KPaging<StDisplayPdGroupDtl> kPaging, 
			StDisplayPdGroupFilter filter, StaffRole staffRole)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (filter.getId() != null){
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
//			sql.append("select dtl.* from product  where product.product_id in " +
//					" (select product_id from st_display_pd_group_dtl " +
//					" where st_display_pd_group_id = ? and status != -1) " +
//					" order by product_id asc");
//			
			sql.append("select dtl.* from product p, display_product_group_dtl dtl where p.product_id " +
					"= dtl.product_id and dtl.display_product_group_id = ? and dtl.status = 1 " +
					"order by dtl.product_id asc");
			params.add(filter.getId());
			if (kPaging == null){
				return repo.getListBySQL(StDisplayPdGroupDtl.class, sql.toString(), params);
			}else
				return repo.getListBySQLPaginated(StDisplayPdGroupDtl.class, sql.toString(), params, kPaging);
		}
		return null;
	}

	/**
	 * @author thachnn
	 */
	@Override
	public List<Product> getListProduct(KPaging<Product> kPaging, 
			Long displayProgramId,String code, String name, DisplayProductGroupType type) throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("SELECT p.* FROM product p JOIN price pr ON p.product_id=pr.product_id AND pr.status=1 ");
		sql.append("AND trunc(sysdate)>=trunc(from_date) AND (trunc(sysdate)<=trunc(to_date) OR to_date IS NULL) WHERE 1=1 ");
		if(!StringUtility.isNullOrEmpty(code)){
			sql.append("AND p.product_code like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(code.toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(name)){
			sql.append("AND upper(p.product_name_text) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(name.toUpperCase())));
		}
		sql.append("AND p.status =1 AND p.product_id NOT IN ");
		sql.append("(SELECT dl.product_id FROM display_product_group pd JOIN display_product_group_dtl dl ON pd.display_product_group_id = dl.display_product_group_id ");
		//sql.append("join product_info pi on pi.product_info_id = p.cat_id ");
		sql.append("WHERE 1=1 AND pd.status = 1 AND dl.status = 1 AND pd.type = 4 ");
		sql.append("AND pd.display_program_id = ?) ");
		params.add(displayProgramId);
		sql.append("ORDER BY p.product_code ASC");
		

		if (kPaging == null)
			return repo.getListBySQL(Product.class, sql.toString(),params);
		else
			return repo.getListBySQLPaginated(Product.class,sql.toString(), params, kPaging);
	}
	
	@Override
	public List<StDisplayProgram> getListDisplayProgramByShop(KPaging<StDisplayProgram> kPaging, Long shopId, String displayProgramCode, String displayProgramName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT distinct(st.DISPLAY_PROGRAM_ID), st.display_program_code, st.display_program_name, st.media_item_id, st.from_date, st.to_date, st.status, st.create_user, st.create_date, st.update_user, st.update_date, st.type ");
		sql.append("FROM display_program st ");
		sql.append("JOIN display_shop_map sm ON st.display_program_id = sm.display_program_id ");
		sql.append("WHERE 1=1 ");
		sql.append("AND st.status = 1 ");
		sql.append("AND sm.status = 1 ");
		sql.append("AND ((from_date >= add_months(trunc(sysdate,'MM'),-2) ");
		sql.append("AND from_date <= trunc(SYSDATE)) ");
		sql.append("OR (from_date <= add_months(trunc(sysdate,'MM'),-2) ");
		sql.append("AND to_date >= add_months(trunc(sysdate,'MM'),-2)) ");
		sql.append("OR (from_date <= trunc(SYSDATE) ");
		sql.append("AND to_date IS NULL)) ");
		if(!StringUtility.isNullOrEmpty(displayProgramCode)) {
			sql.append("AND st.display_program_code = ? ");
			params.add(displayProgramCode);
		}
		if(!StringUtility.isNullOrEmpty(displayProgramName)) {
			sql.append("AND st.display_program_name like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(displayProgramName));
		}
		sql.append("AND sm.shop_id IN ");
		sql.append("(SELECT shop_id ");
		sql.append("FROM shop ");
		//Tablet: sql.append("WHERE shop_type = 1 START WITH shop_id = ? ");
		sql.append("START WITH shop_id = ? ");
		params.add(shopId);
		sql.append("CONNECT BY ");
		sql.append("PRIOR shop_id = parent_shop_id) ");
		sql.append("ORDER BY st.display_program_code ASC ");
		
		if (kPaging == null)
			return repo.getListBySQL(StDisplayProgram.class, sql.toString(),
					params);
		else
			return repo.getListBySQLPaginated(StDisplayProgram.class,
					sql.toString(), params, kPaging);
	}
	//@author: hunglm16; @since: 18, 2014; @description: Lay danh sach CTTB theo nam
	@Override
	public List<StDisplayProgram> getListDisplayProgramByYear(KPaging<StDisplayProgram> kPaging, Long shopId, Integer year, String code, String name) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String endDate = "31/12/"+year;
		String beginDate = "01/01/"+year;
		sql.append(" select dp.* from display_program dp join display_shop_map dm on dp.display_program_id = dm.display_program_id  ");
		sql.append(" where dm.status = 1 and dp.status = 1 and dp.from_date is not null ");
		if(!StringUtility.isNullOrEmpty(code)) {
			sql.append("and lower(dp.display_program_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(code.toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(name)) {
			sql.append("and lower(dp.display_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(name.toLowerCase()));
		}
		//sql.append(" and (dp.to_date is null OR to_char(dp.to_date, 'yyyy')>?) ");
		//params.add(year - 1);
		 sql.append(" and trunc(dp.from_date) <= trunc(to_date(?,'dd/mm/yyyy'))");
		 params.add(endDate);
		 sql.append(" and (trunc(dp.to_date) >= trunc(to_date(?,'dd/mm/yyyy')) or dp.to_date is null)");
		 params.add(beginDate);
		sql.append(" and (dm.shop_id = ? OR dm.shop_id in(select shop_id from shop where status =1 start with shop_id = ? connect by prior shop_id = parent_shop_id)) ");
		sql.append(" order by dp.display_program_code ");
		params.add(shopId);
		params.add(shopId);
		if (kPaging == null)
			return repo.getListBySQL(StDisplayProgram.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StDisplayProgram.class, sql.toString(), params, kPaging);
	}
	
	//@author: hunglm16; @since: 18, 2014; @description: Lay danh sach CTTB theo ds Ma CTTB
	@Override
	public List<StDisplayProgram> getListDisplayProgramByListId(Long shopId, List<Long> lstId, Boolean flag, Integer year, String code, String name) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String endDate = "31/12/"+year;
		String beginDate = "01/01/"+year;
		sql.append(" select dp.* from display_program dp join display_shop_map dm on dp.display_program_id = dm.display_program_id ");
		sql.append(" where dm.status = 1 and dp.status = 1 and dp.from_date is not null ");
		if(!StringUtility.isNullOrEmpty(code)) {
			sql.append("and lower(dp.display_program_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(code.toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(name)) {
			sql.append("and lower(dp.display_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(name.toLowerCase()));
		}
		//sql.append(" and (dp.to_date is null OR to_char(dp.to_date, 'yyyy')>?) ");
		//params.add(year - 1);
		 sql.append(" and trunc(dp.from_date) <= trunc(to_date(?,'dd/mm/yyyy'))");
		 params.add(endDate);
		 sql.append(" and (trunc(dp.to_date) >= trunc(to_date(?,'dd/mm/yyyy')) or dp.to_date is null)");
		 params.add(beginDate);
		if(flag){
			if(lstId!=null && lstId.size()>0){
				sql.append(" and dp.display_program_id not in (-1 ");
				for(Long id:lstId){
					sql.append(" ,? ");
					params.add(id);
				}
				sql.append(" ) ");
			}
		}else{
			sql.append(" and dp.display_program_id in (-1 ");
			if(lstId!=null && lstId.size()>0){
				for(Long id:lstId){
					sql.append(" ,? ");
					params.add(id);
				}
				sql.append(" ) ");
			}
		}
		sql.append(" and (dm.shop_id = ? OR dm.shop_id in(select shop_id from shop where status =1 start with shop_id = ? connect by prior shop_id = parent_shop_id)) ");
		params.add(shopId);
		params.add(shopId);
		
		return repo.getListBySQL(StDisplayProgram.class, sql.toString(), params);
	}

	@Override
	public List<StDisplayProgramVNM> getListDisplayProgramInTowDate(
			KPaging<StDisplayProgramVNM> kPaging, Long shopId, Date fDate,
			Date tDate, String code, String name) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from (SELECT distinct dp.* FROM display_program_vnm dp JOIN display_pro_shop_map_vnm dm ON dp.display_program_vnm_id = dm.display_program_id ");
		sql.append(" where dm.status in (1, 0) and dp.status in (1, 0) and dp.from_date is not null ");
		
		if(!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and lower(dp.display_program_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(code.toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(name)) {
			sql.append(" and lower(dp.display_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(name.toLowerCase()));
		}
		 sql.append(" and dp.from_date < trunc(?)+1");
		 params.add(tDate);
		 sql.append(" and (dp.to_date >= trunc(?) or dp.to_date is null)");
		 params.add(fDate);
		sql.append(" and (dm.shop_id = ? OR dm.shop_id in(select shop_id from shop where status =1 start with shop_id = ? connect by prior shop_id = parent_shop_id)) ");
		sql.append(" order by dp.display_program_code ");
		params.add(shopId);
		params.add(shopId);
		sql.append(")");
		if (kPaging == null)
			return repo.getListBySQL(StDisplayProgramVNM.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StDisplayProgramVNM.class, sql.toString(), params, kPaging);
	}

	@Override
	public StDisplayProgramVNM getDisplayProgramByCode(String code)
			throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from display_program_vnm where display_program_code=? and status!=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		return repo.getEntityBySQL(StDisplayProgramVNM.class, sql, params);
	}
}

package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ParamsDAOGetListCustomer;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.CustomerFilter;
import ths.dms.core.entities.vo.Customer4SaleVO;
import ths.dms.core.entities.vo.CustomerExportVO;
import ths.dms.core.entities.vo.CustomerGTVO;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.CycleCountVO;
import ths.dms.core.entities.vo.DuplicatedCustomerVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.core.entities.vo.rpt.RptCustomerByProductCatDataVO;
import ths.core.entities.vo.rpt.RptCustomerByRoutingDataVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class CustomerDAOImpl implements CustomerDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	ShopLockDAO shopLockDAO;

	public Customer createCustomer(Customer customer, LogInfoVO logInfo) throws DataAccessException, BusinessException {
		if (customer == null) {
			throw new IllegalArgumentException("customer");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}

		// set random code
		customer.setCustomerCode(customer.getShortCode() + "_" + customer.getShop().getShopCode());
		String nameText = null;
		if (!StringUtility.isNullOrEmpty(customer.getCustomerName())) {
			nameText = customer.getCustomerName().toUpperCase();
		}
		if (!StringUtility.isNullOrEmpty(customer.getAddress())) {
			nameText = nameText + " " + customer.getAddress().toUpperCase();
		}
		if (!StringUtility.isNullOrEmpty(nameText)) {
			nameText = Unicode2English.codau2khongdau(nameText);
			customer.setNameText(nameText);
		}
		//customer.setShortCode("SCTemp");

		customer.setCreateUser(logInfo.getStaffCode());
		customer.setCreateDate(commonDAO.getSysDate());
		customer = repo.create(customer);
		customer = repo.getEntityById(Customer.class, customer.getId());

		if (StringUtility.isNullOrEmpty(customer.getShortCode())) {

			//String code = this.generateCodeFromId(this.getNextCode(customer.getId(), customer.getShop().getId()));
			//			int byNext = 0;
			if (customer.getArea() == null || StringUtility.isNullOrEmpty(customer.getArea().getProvince())) {
				throw new IllegalArgumentException("area is null");
			}
			String code = this.generateCodeFromShortCodeMax(this.getMaxShortCode(customer.getArea().getProvince()));
			if (StringUtility.isNullOrEmpty(code)) {
				throw new IllegalArgumentException("shortCode Max");
			}
			if (customer.getArea().getProvince().length() > 3) {
				code = customer.getArea().getProvince().substring(0, 3).toUpperCase() + code;
			} else {
				code = customer.getArea().getProvince().toUpperCase() + code;
			}
			customer.setShortCode(code);

			customer.setCustomerCode(code + "_" + customer.getShop().getShopCode());
			nameText = null;
			if (!StringUtility.isNullOrEmpty(customer.getCustomerName())) {
				nameText = customer.getCustomerName().toUpperCase();
			}
			if (!StringUtility.isNullOrEmpty(customer.getAddress())) {
				nameText = nameText + " " + customer.getAddress().toUpperCase();
			}
			if (!StringUtility.isNullOrEmpty(nameText)) {
				nameText = Unicode2English.codau2khongdau(nameText);
				customer.setNameText(nameText);
			}
			repo.update(customer);
		}

		try {
			actionGeneralLogDAO.createActionGeneralLog(null, customer, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(Customer.class, customer.getId());
	}

	/**
	 * @author hunglm16
	 * @since MAY 31,2014
	 * @description check shortCode by Shop_id
	 **/
	private Integer countByShortCode(String shortCode, Long shopId) throws DataAccessException {
		String sql = "select count(1) as count from customer where short_code= ? and shop_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(shortCode.trim());
		params.add(shopId);
		return repo.countBySQL(sql, params);
	}

	/**
	 * @author hunglm16
	 * @since MAY 31,2014
	 * @description get Max Short Code by ShopId
	 **/
	private String getMaxShopCode(Long shopId) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "select nvl(max(short_code),'000') from customer  where shop_id = ? and LENGTH(customer_code) = 11 and LENGTH(short_code) = 3";//phuongvm tao shop moi shortcode la 000
		//		String sql = "select max(short_code) from customer  where shop_id = ? and LENGTH(customer_code) = 11 and LENGTH(short_code) = 3";
		params.add(shopId);
		return (String) repo.getObjectByQuery(sql, params);
	}

	/**
	 * @author TungMT
	 * @since 17/06/2015
	 * @description get Max Short Code by ShopId
	 **/
	private String getMaxShortCode(String maTinh) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "select to_char(nvl(max(cast(SUBSTR(short_code,4,14) as number)),0) + 1) from customer c ";
		sql += " join area a on c.area_id = a.area_id where a.province = ?";
		params.add(maTinh);
		return (String) repo.getObjectByQuery(sql, params);
	}

	/**
	 * @author Tungmt
	 * @since 17/6/2015
	 * @description general
	 **/
	private String generateCodeFromShortCodeMax(String maxShortCode) throws DataAccessException {
		int max = 8;
		int length = maxShortCode.length();
		if (length < max) {
			int n = max - length;
			for (int i = 0; i < n; i++) {
				maxShortCode = "0" + maxShortCode;
			}
		}
		return maxShortCode;
	}

	/**
	 * @author hunglm16
	 * @since MAY 31,2014
	 * @description general
	 **/
	private String generateCodeFromShopCodeMax(String maxShopCode, int next) throws DataAccessException {
		if (maxShopCode.length() != 3) {
			return "";
		}
		if (next > 5) {
			return "";
		} else if (next < 0) {
			next = 0;
		}
		String arrStr = "0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
		String[] arr = arrStr.split(",");

		String kyTuR = maxShopCode.substring(2, 3);
		String kyTuRNew = "";
		int byNext = 0;
		if (!"Z".equals(kyTuR)) {
			for (int i = 0; i < arr.length; i++) {
				if (kyTuR.equals(arr[i])) {
					kyTuRNew = arr[i + 1];
					break;
				}
			}
			return maxShopCode.substring(0, 2) + kyTuRNew;
		} else {
			kyTuR = maxShopCode.substring(1, 2);
			if (!"Z".equals(kyTuR)) {
				for (int i = 0; i < arr.length; i++) {
					byNext = i + 1 + next;
					if (kyTuR.equals(arr[i]) && byNext < arr.length) {
						kyTuRNew = arr[i + 1];
						break;
					}
				}
				return maxShopCode.substring(0, 1) + kyTuRNew + "0";
			} else {
				kyTuR = maxShopCode.substring(0, 1);
				if (!"Z".equals(kyTuR)) {
					for (int i = 0; i < arr.length; i++) {
						byNext = i + 1 + next;
						if (kyTuR.equals(arr[i]) && byNext < arr.length) {
							kyTuRNew = arr[i + 1 + next];
							break;
						}
					}
					return kyTuRNew + "00";
				} else {
					return "";
				}
			}
		}
	}

	@Override
	public void deleteCustomer(Customer customer, LogInfoVO logInfo) throws DataAccessException {
		if (customer == null) {
			throw new IllegalArgumentException("customer");
		}
		repo.delete(customer);
		try {
			actionGeneralLogDAO.createActionGeneralLog(customer, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Customer getCustomerById(long id) throws DataAccessException {
		return repo.getEntityById(Customer.class, id);
	}

	@Override
	public Customer getCustomerForUpdate(Long id) throws DataAccessException {
		String sql = "select * from CUSTOMER where customer_id=? for update";
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		Customer rs = repo.getEntityBySQL(Customer.class, sql, params);
		return rs;
	}

	@Override
	public void updateCustomer(Customer customer, LogInfoVO logInfo) throws DataAccessException {
		if (customer == null) {
			throw new IllegalArgumentException("customer");
		}
		if (logInfo != null) {
			customer.setUpdateDate(commonDAO.getSysDate());
			customer.setUpdateUser(logInfo.getStaffCode());
		}
		if (customer.getCustomerCode() != null) {
			customer.setCustomerCode(customer.getCustomerCode().toUpperCase());
		}
		Customer temp = this.getCustomerById(customer.getId());
		String nameText = null;
		if (!StringUtility.isNullOrEmpty(customer.getCustomerName())) {
			nameText = customer.getCustomerName().toUpperCase();
		}
		if (!StringUtility.isNullOrEmpty(customer.getAddress())) {
			nameText = nameText + " " + customer.getAddress().toUpperCase();
		}
		if (!StringUtility.isNullOrEmpty(nameText)) {
			nameText = Unicode2English.codau2khongdau(nameText);
			customer.setNameText(nameText);
		}
		// source bi do , phuocdh2 common lai
		//temp = temp.clone();
		repo.update(customer);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, customer, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Customer getCustomerByCode(String code) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from CUSTOMER where customer_code=? and status != ?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		Customer rs = repo.getEntityBySQL(Customer.class, sql, params);
		return rs;
	}

	/**
	 * @author sangtn
	 * @since 08-05-2014
	 * @description Get customer by full code and status
	 * @note Extend from getCustomerByCode
	 */
	@Override
	public Customer getCustomerByCodeEx(String code, ActiveType status) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from CUSTOMER where customer_code=? and status = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(status.getValue());
		Customer rs = repo.getEntityBySQL(Customer.class, sql, params);
		return rs;
	}

	@Override
	public Customer getCustomerByCodeAndShop(String shortCode, Long shopId) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "select * from CUSTOMER where short_code=? and status != ? and shop_id = ?";
		params.add(shortCode.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		params.add(shopId);
		Customer rs = repo.getEntityBySQL(Customer.class, sql, params);
		return rs;
	}

	@Override
	public List<Customer> getListCustomer(ParamsDAOGetListCustomer funcParams) throws DataAccessException {
		final String ORDER_BY_CUSTOMER_ROUTES = "routes";
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select c.* ");
		sql.append(" from customer c  ");
		sql.append(" join shop s on c.shop_id = s.shop_id  ");
		sql.append(" left join channel_type ct on ct.channel_type_id = c.CHANNEL_TYPE_ID ");
		if (ORDER_BY_CUSTOMER_ROUTES.equalsIgnoreCase(funcParams.getSort())) {
			sql.append(" left join ( ");
			sql.append("	SELECT rc.customer_id, LISTAGG (r.routing_code, ', ') WITHIN GROUP (ORDER BY r.routing_code) routes ");
			sql.append("	FROM routing_customer rc ");
			sql.append("		 JOIN routing r ON rc.routing_id = r.routing_id ");
			sql.append("	WHERE rc.status = ? and r.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			params.add(ActiveType.RUNNING.getValue());
			sql.append("	AND rc.start_date <= sysdate ");
			sql.append("	AND rc.end_date is null ");
			sql.append("	OR rc.end_date + 1 > sysdate ");
			sql.append("	GROUP BY rc.customer_id ");
			sql.append(" ) cus_route on c.customer_id = cus_route.customer_id ");
		}
		sql.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(funcParams.getShortCode())) {
			sql.append(" and (c.short_code like ? ESCAPE '/' or c.short_code like ? ESCAPE '/') ");
			String s = funcParams.getShortCode().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
			s = funcParams.getShortCode().toLowerCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getCustomerName())) {
			//sql.append(" and lower(name_text) like ? ESCAPE '/' ");
			sql.append(" and unicode2english(c.customer_name) like ? ESCAPE '/' ");
			String s = funcParams.getCustomerName().trim().toLowerCase();
			s = Unicode2English.codau2khongdau(s);
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (funcParams.getLstAreaCode() != null && funcParams.getLstAreaCode().size() > 0) {
			String str = " and (";
			for (String areaCode : funcParams.getLstAreaCode()) {
				str += " exists (select 1 from area a where a.area_id=c.area_id and area_code=?) or ";
				params.add(areaCode);
			}
			str = str.substring(0, str.length() - 4);
			str += ")";
			sql.append(str);
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getPhone())) {
			sql.append(" and (lower(c.phone) like ? ESCAPE '/' or  lower(c.mobiphone) like ? ESCAPE '/' )");
			String s = funcParams.getPhone().toLowerCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getMobiPhone())) {
			sql.append(" and (lower(c.phone) like ? ESCAPE '/' or  lower(c.mobiphone) like ? ESCAPE '/' ) ");
			String s = funcParams.getMobiPhone().toLowerCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
			params.add(s);
		}

		if (funcParams.getStatus() != null) {
			sql.append(" and c.status=?");
			params.add(funcParams.getStatus().getValue());
		} else if (funcParams.getStatus() == null) {
			sql.append(" and c.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (funcParams.getCustomerTypeCode() != null) {
			sql.append(" and ct.channel_type_code=? ");
			params.add(funcParams.getCustomerTypeCode());
		} else {
			//TUNGTT
			sql.append(" and (c.CHANNEL_TYPE_ID in (select channel_type_id from channel_type) or c.CHANNEL_TYPE_ID is null) ");
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getRegion())) {
			sql.append(" and lower(c.region) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(funcParams.getRegion().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getLoyalty())) {
			sql.append(" and lower(c.loyalty) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(funcParams.getLoyalty().toLowerCase()));
		}
		if (funcParams.getDeliverId() != null) {
			sql.append(" and c.DELIVER_ID=?");
			params.add(funcParams.getDeliverId());
		}
		if (funcParams.getCashierId() != null) {
			sql.append(" and c.CASHIER_STAFF_ID=?");
			params.add(funcParams.getCashierId());
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getAddress())) {
			sql.append(" and lower(c.address) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(funcParams.getAddress().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getStreet())) {
			sql.append(" and lower(c.street) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(funcParams.getStreet().toLowerCase()));
		}
		if (funcParams.getShopId() != null) {
			if (funcParams.isAllSubShop()) {
				sql.append(" and (c.shop_id in ( SELECT shop_id FROM shop START WITH  status=1 and shop_id = ? CONNECT BY prior shop_id = parent_shop_id))");
			} else {
				sql.append(" and (c.shop_id = ?)"); //in (select shop_id from shop ");
			}
			params.add(funcParams.getShopId());

		} else {
			sql.append(" and (c.shop_id in (select shop_id from shop ");
			sql.append(" 	start with parent_shop_id is null and status=1");
			sql.append(" 	connect by prior shop_id=parent_shop_id))");
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getStrShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(funcParams.getStrShopId(), "c.shop_id");
			sql.append(paramsFix);
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getDirectShopCode())) {
			sql.append(" and s.shop_code = ?");
			params.add(funcParams.getDirectShopCode().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getShopCodeLike())) {
			sql.append(" and s.shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(funcParams.getShopCodeLike().toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getDisplay())) {
			sql.append(" and upper(c.DISPLAY) = ?");
			params.add(funcParams.getDisplay().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getLocation())) {
			sql.append(" and upper(c.location) = ?");
			params.add(funcParams.getLocation().toUpperCase());
		}

		if (funcParams.getNotInDeliverId() != null) {
			sql.append(" and (c.deliver_id != ? or c.deliver_id is null)");
			params.add(funcParams.getNotInDeliverId());
		}

		if (funcParams.getCustomerTypeId() != null) {
			sql.append(" and c.channel_type_id = ?");
			params.add(funcParams.getCustomerTypeId());
		}

		if (funcParams.getTansuat() != null) {
			sql.append(" and c.FREQUENCY = ?");
			params.add(funcParams.getTansuat());
		}
		if (ORDER_BY_CUSTOMER_ROUTES.equalsIgnoreCase(funcParams.getSort())) {
			sql.append(" order by cus_route.routes ").append(funcParams.getOrder());
		} else if (!StringUtility.isNullOrEmpty(funcParams.getSort())) {
			sql.append(" order by ").append(funcParams.getSort()).append(" ").append(funcParams.getOrder());
		} else {
			sql.append(" order by s.shop_name, c.customer_code");
		}
		if (funcParams.getkPaging() == null) {
			return repo.getListBySQL(Customer.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(Customer.class, sql.toString(), params, funcParams.getkPaging());
	}
	
	@Override
	public List<Customer> getListCustomerByListID(List<Long> listId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from CUSTOMER c where 1 = 1");
		sql.append("  and customer_id in (-1   ");
		for (Long id : listId) {
			sql.append(" ,? ");
			params.add(id);
		}
		sql.append(" ) ");
		return repo.getListBySQL(Customer.class, sql.toString(), params);
	}

	@Override
	public List<Customer4SaleVO> getListCustomer4CreateOrder(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select CUSTOMER_ID customerId, SHORT_CODE shortCode, CUSTOMER_CODE customerCode, CUSTOMER_NAME customerName, ADDRESS address, PHONE phone from CUSTOMER c where 1 = 1");
		sql.append(" and status=?");
		params.add(ActiveType.RUNNING.getValue());
		if (shopId != null) {
			sql.append(" and (shop_id in (select shop_id from shop s1 where status = 1 and exists (select 1 from channel_type ct where ct.channel_type_id = s1.shop_type_id and ct.object_type = ?)");
			params.add(ShopObjectType.NPP.getValue());
			sql.append(" 	start with shop_id = ?");
			params.add(shopId);
			sql.append(" 	connect by prior shop_id=parent_shop_id))");
		}
		sql.append(" order by customer_code");

		String[] fieldNames = { "customerId",//0
				"shortCode",// 1
				"customerCode",// 2
				"customerName",// 3
				"address",// 4
				"phone"// 5
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING //5
		};

		return repo.getListByQueryAndScalar(Customer4SaleVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<Customer> getListCustomerByShopId(Long shopId, ParamsDAOGetListCustomer funcParams) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from CUSTOMER c where 1 = 1");

		if (!StringUtility.isNullOrEmpty(funcParams.getShortCode())) {
			sql.append(" and short_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(funcParams.getShortCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getCustomerName())) {
			sql.append(" and upper(customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(funcParams.getCustomerName().toUpperCase()));
		}
		if (funcParams.getLstAreaCode() != null && funcParams.getLstAreaCode().size() > 0) {
			String str = " and (";
			for (String areaCode : funcParams.getLstAreaCode()) {
				str += " exists (select 1 from area a where a.area_id=c.area_id and area_code=?) or ";
				params.add(areaCode);
			}
			str = str.substring(0, str.length() - 4);
			str += ")";
			sql.append(str);
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getPhone())) {
			sql.append(" and lower(phone) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(funcParams.getPhone().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getMobiPhone())) {
			sql.append(" and lower(mobiphone) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(funcParams.getMobiPhone().toLowerCase()));
		}
		if (funcParams.getStatus() != null) {
			sql.append(" and status=?");
			params.add(funcParams.getStatus().getValue());
		} else {
			sql.append(" and status<>?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (funcParams.getCustomerTypeId() != null) {
			sql.append(" and CHANNEL_TYPE_ID=?");
			params.add(funcParams.getCustomerTypeId());
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getRegion())) {
			sql.append(" and lower(region) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(funcParams.getRegion().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getLoyalty())) {
			sql.append(" and lower(loyalty) like ?");
			params.add(StringUtility.toOracleSearchLike(funcParams.getLoyalty().toLowerCase()));
		}
		if (funcParams.getDeliverId() != null) {
			sql.append(" and DELIVER_ID=?");
			params.add(funcParams.getDeliverId());
		}
		if (funcParams.getCashierId() != null) {
			sql.append(" and CASHIER_STAFF_ID=?");
			params.add(funcParams.getCashierId());
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getAddress())) {
			sql.append(" and lower(address) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(funcParams.getAddress().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(funcParams.getStreet())) {
			sql.append(" and lower(street) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(funcParams.getStreet().toLowerCase()));
		}
		/*
		 * if (funcParams.getShopId() != null) {
		 * sql.append(" and (shop_id in (select shop_id from shop ");
		 * sql.append(" 	start with parent_shop_id = ? and status=1");
		 * sql.append
		 * (" 	connect by prior shop_id=parent_shop_id) or shop_id=?)");
		 * params.add(funcParams.getShopId());
		 * params.add(funcParams.getShopId()); } else {
		 * sql.append(" and (shop_id in (select shop_id from shop ");
		 * sql.append(" 	start with parent_shop_id is null and status=1");
		 * sql.append(" 	connect by prior shop_id=parent_shop_id))"); }
		 */

		if (shopId != null) {
			sql.append(" and (shop_id in (select shop_id from shop s1 where status = 1 and exists (select 1 from channel_type ct where ct.channel_type_id = s1.shop_type_id and ct.object_type = ?)");
			params.add(ShopObjectType.NPP.getValue());
			sql.append(" 	start with shop_id = ?");
			params.add(shopId);
			sql.append(" 	connect by prior shop_id=parent_shop_id))");
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getDirectShopCode())) {
			sql.append(" and exists (select 1 from shop s where c.shop_id = s.shop_id and upper(s.shop_code) = ?)");
			params.add(funcParams.getDirectShopCode().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getShopCodeLike())) {
			sql.append(" and exists (select 1 from shop s where c.shop_id = s.shop_id and s.shop_code like ?)");
			params.add(StringUtility.toOracleSearchLikeSuffix(funcParams.getShopCodeLike().toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getDisplay())) {
			sql.append(" and upper(c.DISPLAY) = ?");
			params.add(funcParams.getDisplay().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(funcParams.getLocation())) {
			sql.append(" and upper(location) = ?");
			params.add(funcParams.getLocation().toUpperCase());
		}

		if (funcParams.getNotInDeliverId() != null) {
			sql.append(" and (deliver_id != ? or deliver_id is null)");
			params.add(funcParams.getNotInDeliverId());
		}

		sql.append(" order by customer_code");
		if (funcParams.getkPaging() == null)
			return repo.getListBySQL(Customer.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Customer.class, sql.toString(), params, funcParams.getkPaging());
	}

	@Override
	public List<CustomerVO> getListCustomerByShopFilter(CustomerFilter<CustomerVO> filter) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct c.customer_id id, s.shop_code shopCode, c.short_code shortCode, c.customer_name customerName, c.address,  ksc.reward_type rewardType ");
		sql.append(" from CUSTOMER c");
		sql.append(" inner join shop s on (c.shop_id=s.shop_id and s.status=1)");
		sql.append(" Left join ks_customer ksc on (ksc.customer_id=c.customer_id and ksc.status=1");
		if (filter.getCycleId() != null) {
			sql.append(" and ksc.cycle_id = ? ");
			params.add(filter.getCycleId());
		}
		if (filter.getKsId() != null) {
			sql.append(" AND ksc.ks_id = ? ");
			params.add(filter.getKsId());
		}
		sql.append(" )");
		sql.append(" where 1 = 1");
		if (filter.getStatus() != null) {
			sql.append(" AND c.status = ?");
			params.add(filter.getStatus());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and short_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShortCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
			sql.append(" and upper(customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getCustomerName().toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(filter.getAddress())) {
			sql.append(" and lower(address) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getAddress().toLowerCase()));
		}

		if (!StringUtility.isNullOrEmpty(filter.getStrShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrShopId(), "c.shop_id");
			sql.append(paramsFix);
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" AND c.shop_id IN (select shop_id from shop  where status=1 start with upper(shop_code) = ? connect by prior shop_id=parent_shop_id)");
			params.add(filter.getShopCode().toUpperCase());
		}
		if (filter.getKsId() != null) {
			sql.append(" AND c.shop_id IN ( ");
			sql.append("  SELECT shop_id    FROM shop    WHERE status   =1");
			sql.append("  START WITH shop_id IN (SELECT shop_id  FROM ks");
			sql.append(" INNER JOIN ks_shop_map kss  ON (ks.ks_id  =kss.ks_id)  WHERE ");
			sql.append("  ks.ks_id = ? and kss.status = 1) CONNECT BY prior shop_id    =parent_shop_id  )");
			params.add(filter.getKsId());
		}

		//Count total
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");

		//order sau countSql
		sql.append(" order by shop_code, short_code");

		final String[] fieldNames = new String[] { "id", "shopCode", "shortCode", "customerName", "address", "rewardType" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };

		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}

	@Override
	public Boolean isUsingByOthers(long customerId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) as count from customer ");
		sql.append("	where exists(select 1 from Promotion_Customer_Map where CUSTOMER_ID=? and status != ?)");
		params.add(customerId);
		params.add(ActiveType.DELETED.getValue());
		sql.append("		or exists(select 1 from display_customer_score where CUSTOMER_ID=?)");
		params.add(customerId);
		sql.append("		or exists(select 1 from display_customer_map where CUSTOMER_ID=? and status != ?)");
		params.add(customerId);
		params.add(ActiveType.DELETED.getValue());
		sql.append("		or exists(select 1 from stock_total where OBJECT_ID=? and OBJECT_TYPE=?)");
		params.add(customerId);
		params.add(StockObjectType.CUSTOMER.getValue());
		sql.append("		or exists(select 1 from product_lot where OBJECT_ID=? and OBJECT_TYPE=? and status != ?)");
		params.add(customerId);
		params.add(StockObjectType.CUSTOMER.getValue());
		params.add(ActiveType.DELETED.getValue());
		sql.append("		or exists(select 1 from customer_cat_level where CUSTOMER_ID=? )");
		params.add(customerId);
		sql.append("		or exists(select 1 from pay_received where CUSTOMER_ID=? )");
		params.add(customerId);
		sql.append("		or exists(select 1 from DEBIT_DETAIL where CUSTOMER_ID=? )");
		params.add(customerId);
		sql.append("		or exists(select 1 from SALE_ORDER where CUSTOMER_ID=? )");
		params.add(customerId);
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public String generateCodeFromId(Integer id) throws BusinessException {
		String code = "";
		Integer firstNum = id / (36 * 36);
		Integer secondNum = (id - firstNum * 36 * 36) / 36;
		Integer thirdNum = (id - firstNum * 36 * 36 - secondNum * 36);
		code = convertNumToCode(firstNum) + convertNumToCode(secondNum) + convertNumToCode(thirdNum);
		return code;
	}

	private String convertNumToCode(Integer num) throws BusinessException {
		if (num < 10)
			return String.valueOf(num);
		if (num > 35)
			throw new BusinessException("Customer code is overload");
		return String.valueOf(Character.toChars((int) (65 + (num - 10))));
	}

	@Override
	public Boolean checkIfCustomerExists(String email, String mobiphone, String phone, String numberIdentity, Long id, String fax) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(*) as count from customer where 1=1");
		if (!StringUtility.isNullOrEmpty(email)) {
			sql.append(" and lower(email)=?");
			params.add(email.toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(mobiphone)) {
			sql.append(" and lower(MOBIPHONE)=?");
			params.add(mobiphone.toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(fax)) {
			sql.append(" and lower(FAX)=?");
			params.add(fax.toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(phone)) {
			sql.append(" and lower(phone)=?");
			params.add(phone.toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(numberIdentity)) {
			sql.append(" and lower(IDNO)=?");
			params.add(numberIdentity.toLowerCase());
		}
		if (id != null) {
			sql.append(" and customer_id != ?");
			params.add(id);
		}

		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public List<RptCustomerByProductCatDataVO> getDataForCusByProCatReport(Long shopId, List<Long> cusIds, Date fromDate, Date toDate) throws DataAccessException {
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptCustomerByProductCatDataVO>();
		if (null == shopId) {
			throw new IllegalArgumentException("Shop is invalidate");
		}

		if (null == fromDate) {
			throw new IllegalArgumentException("From date is invalidate");
		}

		if (null == toDate) {
			throw new IllegalArgumentException("To date is invalidate");
		}

		if (fromDate.compareTo(toDate) > 0) {
			throw new IllegalArgumentException("From date can't after to date");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT CUS.CUSTOMER_ID AS customerId, CUS.CUSTOMER_CODE AS customerCode, CUS.CUSTOMER_NAME AS customerName, ");
		sql.append("     PRO.PRODUCT_CODE AS productCode, PRO.PRODUCT_NAME AS productName, PRO_I.PRODUCT_INFO_NAME AS productCat, PRO_I.DESCRIPTION AS productCatDes, ");
		sql.append("     SO_D.PRICE AS price, SO_D.QUANTITY AS quantity, SO_D.AMOUNT AS amount ");
		sql.append(" FROM CUSTOMER CUS, SALE_ORDER SO, SALE_ORDER_DETAIL SO_D, PRODUCT PRO,  ");
		sql.append("     PRODUCT_LEVEL PRO_L, PRODUCT_INFO PRO_I ");
		sql.append(" WHERE SO.CUSTOMER_ID = CUS.CUSTOMER_ID ");
		sql.append("     AND SO.SALE_ORDER_ID = SO_D.SALE_ORDER_ID ");
		sql.append("     AND SO_D.PRODUCT_ID = PRO.PRODUCT_ID ");
		sql.append("     AND PRO.PRODUCT_LEVEL_ID = PRO_L.PRODUCT_LEVEL_ID ");
		sql.append("     AND PRO_L.CAT_ID = PRO_I.PRODUCT_INFO_ID ");
		sql.append("     AND PRO_I.TYPE = ? ");
		params.add(ProductType.CAT.getValue());
		sql.append("     AND SO.SHOP_ID = ? ");

		params.add(shopId);
		sql.append("     AND SO.APPROVED = ? ");
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append("     AND SO_D.IS_FREE_ITEM = 0 ");
		sql.append("     AND SO.ORDER_DATE >= trunc(?) AND SO.ORDER_DATE < trunc(?) + 1 ");

		params.add(fromDate);
		params.add(toDate);

		if (null != cusIds && cusIds.size() > 0) {
			sql.append("     AND CUS.CUSTOMER_ID IN (");

			for (int i = 0; i < cusIds.size(); i++) {
				if (i < cusIds.size() - 1) {
					sql.append(" ?,");
				} else {
					sql.append(" ?");
				}

				params.add(cusIds.get(i));
			}

			sql.append(") ");
		}

		sql.append(" ORDER BY customerCode ASC, productCat ASC, productCode ASC ");

		String[] fieldNames = { "customerId",// 1
				"customerCode",// 2
				"customerName",// 3
				"productCatDes",// 3'
				"productCode",// 4
				"productName",// 5
				"productCat",// 6
				"price",// 7
				"quantity",// 8
				"amount"// 9
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 3'
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.FLOAT, // 7
				StandardBasicTypes.LONG, // 8
				StandardBasicTypes.BIG_DECIMAL // 9
		};

		return repo.getListByQueryAndScalar(RptCustomerByProductCatDataVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public CustomerVO getCustomerVO(Long customerId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select c.customer_code as customerCode,c.shop_id as shopId,");
		sql.append("       c.short_code as shortCode,");
		sql.append("       c.customer_name as customerName,");
		sql.append("       c.address as address,");
		sql.append("       c.phone as phone,");
		sql.append("       c.mobiphone as mobiphone,");
		sql.append("       ct.channel_type_name as shopTypeName,");
		sql.append("       c.loyalty as loyalty,");
		sql.append("       c.contact_name as contactName");

		sql.append(" from customer c left join channel_type ct on c.channel_type_id = ct.channel_type_id");
		sql.append(" where c.customer_id = ?");
		params.add(customerId);

		String[] fieldNames = { "customerCode", "shopId", // 0
				"shortCode", "customerName", // 1
				"address", // 2
				"phone", // 3
				"mobiphone", // 4
				"shopTypeName", // 5
				"loyalty", // 6
				"contactName", // 7
		};
		//dh2 them shopid
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.STRING, // 7
		};
		List<CustomerVO> lstVO = repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		CustomerVO vo = lstVO.get(0);

		Customer c = getCustomerById(customerId);
		Date lockDate = shopLockDAO.getApplicationDate(c.getShop().getId());

		sql = new StringBuilder();
		params = new ArrayList<Object>();

		sql.append(" select count(prd) as count");
		sql.append("   from (");
		sql.append("   select sod.product_id as prd");
		sql.append("   from sale_order so ");
		sql.append("           join sale_order_detail sod on so.sale_order_id = sod.sale_order_id");
		sql.append("               and so.from_sale_order_id is null");
		sql.append("               and so.ACCOUNT_DATE >= trunc(?) and so.ACCOUNT_DATE < trunc(?) + 1");
		params.add(lockDate);
		params.add(lockDate);
		sql.append("               and so.customer_id = ?");
		params.add(customerId);
		sql.append("           left join sale_order so1 on so1.from_sale_order_id is null");
		sql.append("               and so1.ACCOUNT_DATE >= trunc(?) and so1.ACCOUNT_DATE < trunc(?) + 1");
		params.add(lockDate);
		params.add(lockDate);
		sql.append("               and so1.customer_id = ?");
		params.add(customerId);
		sql.append("           left join sale_order_detail sod1 on sod1.sale_order_id = so1.sale_order_id ");
		sql.append("               and sod.product_id = sod1.product_id");
		sql.append("	where so.approved = ?");
		params.add(ApprovalStatus.APPROVED.getValue());
		sql.append("   group by sod.product_id");
		sql.append("   having (sum(sod.quantity) - sum(case when sod1.quantity is null then 0 else sod1.quantity end)) > 0");
		sql.append(" )");

		vo.setNumSkuInDate(repo.countBySQL(sql.toString(), params));

		sql = new StringBuilder();
		params = new ArrayList<Object>();

		sql.append(" select sum(so.amount)");
		sql.append(" from sale_order so ");
		sql.append(" where so.customer_id = ? and so.approved = ? and so.type = ? and so.amount > 0 ");
		params.add(customerId);
		params.add(ApprovalStatus.APPROVED.getValue());
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		sql.append("   and ACCOUNT_DATE >= trunc(?) and ACCOUNT_DATE < trunc(?) + 1");
		params.add(lockDate);
		params.add(lockDate);

		vo.setTotalInDate((BigDecimal) repo.getObjectByQuery(sql.toString(), params));

		sql = new StringBuilder();
		params = new ArrayList<Object>();

		sql.append(" select sum(so.quantity)");
		sql.append(" from sale_order so ");
		sql.append(" where so.customer_id = ? and so.approved = ? and so.type = ? and so.amount > 0 ");
		params.add(customerId);
		params.add(ApprovalStatus.APPROVED.getValue());
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		sql.append("   and ACCOUNT_DATE >= trunc(?) and ACCOUNT_DATE < trunc(?) + 1");
		params.add(lockDate);
		params.add(lockDate);

		vo.setQuantityInDate((BigDecimal) repo.getObjectByQuery(sql.toString(), params));

		sql = new StringBuilder();
		params = new ArrayList<Object>();

		sql.append(" select count (*) as count from ( ");
		sql.append(" select to_char(ACCOUNT_DATE,'dd/mm/yyyy'), count (sale_order_id) ");
		sql.append(" from sale_order so ");
		sql.append(" where so.customer_id = ? and so.approved = ? and so.type = ? and so.amount > 0 ");
		params.add(customerId);
		params.add(ApprovalStatus.APPROVED.getValue());
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		sql.append(" AND ACCOUNT_DATE >= TRUNC(?, 'MONTH') AND ACCOUNT_DATE  < (TRUNC(?, 'MONTH') + interval '1' MONTH)");
		params.add(lockDate);
		params.add(lockDate);
		sql.append(" group by to_char(ACCOUNT_DATE,'dd/mm/yyyy')) ");
		vo.setNumOrderInMonth(repo.countBySQL(sql.toString(), params));

		sql = new StringBuilder();
		params = new ArrayList<Object>();

		sql.append(" select  sum(so.amount) ");
		sql.append(" from sale_order so ");
		sql.append(" where so.customer_id = ? and so.approved = ? and so.type = ? and so.amount > 0 ");
		params.add(customerId);
		params.add(ApprovalStatus.APPROVED.getValue());
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		sql.append(" and so.ACCOUNT_DATE >= trunc(?, 'MONTH') and so.ACCOUNT_DATE < (trunc(?, 'MONTH') + interval '1' month) ");
		params.add(lockDate);
		params.add(lockDate);

		vo.setTotalInMonth((BigDecimal) repo.getObjectByQuery(sql.toString(), params));

		sql = new StringBuilder();
		params = new ArrayList<Object>();
		sql.append(" select  sum(so.quantity)");
		sql.append(" from sale_order so ");
		sql.append(" where so.customer_id = ? and so.approved = ? and so.type = ? and so.amount > 0 ");
		params.add(customerId);
		params.add(ApprovalStatus.APPROVED.getValue());
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		sql.append(" and so.ACCOUNT_DATE >= trunc(?, 'MONTH') and so.ACCOUNT_DATE < (trunc(?, 'MONTH') + interval '1' month)");
		params.add(lockDate);
		params.add(lockDate);

		vo.setQuantityInMonth((BigDecimal) repo.getObjectByQuery(sql.toString(), params));
		sql = new StringBuilder();
		params = new ArrayList<Object>();

		sql.append(" select  round(sum(so.amount)/2)");
		sql.append(" from sale_order so ");
		sql.append(" where so.customer_id = ? and so.approved = ? and so.type = ? and so.amount > 0 ");
		params.add(customerId);
		params.add(ApprovalStatus.APPROVED.getValue());
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		sql.append(" and so.ACCOUNT_DATE >= trunc(?, 'MONTH') - interval '2' month and so.order_date < trunc(?, 'MONTH')");
		params.add(lockDate);
		params.add(lockDate);
		vo.setAvgTotalInLastTwoMonth((BigDecimal) repo.getObjectByQuery(sql.toString(), params));

		//dh2 avgQuantityInTwoMonth
		sql = new StringBuilder();
		params = new ArrayList<Object>();
		sql.append(" select  round(sum(quantity)/2)");
		sql.append(" from sale_order so ");
		sql.append(" where so.customer_id = ? and so.approved = ? and so.type = ? and so.amount > 0 ");
		params.add(customerId);
		params.add(ApprovalStatus.APPROVED.getValue());
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		sql.append(" and so.ACCOUNT_DATE >= trunc(?, 'MONTH') - interval '2' month and so.order_date < trunc(?, 'MONTH') ");
		params.add(lockDate);
		params.add(lockDate);
		vo.setAvgQuantityInLastTwoMonth((BigDecimal) repo.getObjectByQuery(sql.toString(), params));
		return vo;
	}

	@Override
	public List<Customer> getListCustomerNotInDisplayProgram(Long parentShopId, Long displayStaffMapId, String customerCode, String customerName, Integer limit) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select *  ");
		sql.append(" from customer s ");
		sql.append(" where s.customer_id not in (select customer_id from display_customer_map ds where ds.display_staff_map_id = ?)  ");
		params.add(displayStaffMapId);
		sql.append("   and shop_id in (select shop_id from shop ");
		if (parentShopId == null)
			sql.append("                   start with parent_shop_id is null ");
		else {
			sql.append("                   start with shop_id=? ");
			params.add(parentShopId);
		}
		sql.append("                   connect by prior shop_id=parent_shop_id) ");

		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append(" and customer_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append(" and upper(customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(customerName.toUpperCase()));
		}

		sql.append(" and s.status=?");
		params.add(ActiveType.RUNNING.getValue());

		if (limit != null) {
			sql.append(" and rownum <= ?");
			params.add(limit);
		}

		sql.append(" order by customer_code asc");
		return repo.getListBySQL(Customer.class, sql.toString(), params);
	}

	@Override
	public Customer getCustomerMultiChoine(Long customerId, Long shopId, String shortCode, Integer status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from customer ");
		sql.append(" where 1 = 1 ");
		if (customerId != null) {
			sql.append(" and customer_id = ? ");
			params.add(customerId);
		}
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		if (!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append(" and upper(short_code) = ? ");
			params.add(shortCode.trim().toUpperCase());
		}
		if (status != null) {
			sql.append(" and status = 1 ");
			params.add(status);
		}
		return repo.getEntityBySQL(Customer.class, sql.toString(), params);
	}

	@Override
	public List<Customer> getListCustomer(Long parentShopId, String customerCode, String customerName, Integer limit) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select *  ");
		sql.append(" from customer s where 1=1");
		if (parentShopId != null)
			sql.append("   and shop_id is null");
		else {
			sql.append("   and shop_id = ?");
			params.add(parentShopId);
		}

		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append(" and customer_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append(" and upper(customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(customerName.toUpperCase()));
		}

		sql.append(" and s.status=?");
		params.add(ActiveType.RUNNING.getValue());

		if (limit != null) {
			sql.append(" and rownum <= ?");
			params.add(limit);
		}

		sql.append(" order by customer_code asc");
		return repo.getListBySQL(Customer.class, sql.toString(), params);
	}

	@Override
	public List<Customer> getListCustomerEx(Long parentShopId, String customerCode, String customerName, Integer limit) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select *  ");
		sql.append(" from customer s where 1=1");
		if (parentShopId != null)
			sql.append("   and shop_id in (select shop_id from shop start with parent_shop_id is null connect by prior shop_id = parent_shop_id)");
		else {
			sql.append("   and shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
			params.add(parentShopId);
		}

		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append(" and customer_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append(" and upper(customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(customerName.toUpperCase()));
		}

		sql.append(" and s.status=?");
		params.add(ActiveType.RUNNING.getValue());

		if (limit != null) {
			sql.append(" and rownum <= ?");
			params.add(limit);
		}

		sql.append(" order by customer_code asc");
		return repo.getListBySQL(Customer.class, sql.toString(), params);
	}

	@Override
	public List<Customer> getListCustomerNotInPromotionProgram(KPaging<Customer> kPaging, Long parentShopId, Long promotionShopMapId, String shortCode, String customerName, String address) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select *  ");
		sql.append(" from customer s ");
		sql.append(" where s.customer_id not in (select customer_id from promotion_customer_map ds where ds.promotion_shop_map_id = ? and ds.status = 1)  ");
		params.add(promotionShopMapId);
		if (parentShopId != null) {
			sql.append(" and shop_id in (?)");
			params.add(parentShopId);
		}

		sql.append(" and s.status=?");
		params.add(ActiveType.RUNNING.getValue());

		if (!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append(" and s.short_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append(" and upper(customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(customerName.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(address)) {
			sql.append(" and upper(address) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(address.toUpperCase()));
		}

		sql.append(" order by short_code asc");
		if (kPaging == null)
			return repo.getListBySQL(Customer.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Customer.class, sql.toString(), params, kPaging);
	}

	@Override
	public String test() throws DataAccessException {
		String sql = "SELECT LISTAGG(customer_name, ', ') WITHIN GROUP (ORDER BY null) as cycleCountCode FROM customer";
		List<Object> params = new ArrayList<Object>();
		String[] fieldNames = { "cycleCountCode" };
		Type[] fieldTypes = { StandardBasicTypes.STRING };
		List<CycleCountVO> lstVO = repo.getListByQueryAndScalar(CycleCountVO.class, fieldNames, fieldTypes, sql, params);
		if (lstVO.size() != 0)
			return lstVO.get(0).getCycleCountCode();
		else
			return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.CustomerDAO#getListCustomerForOnlyShop(com.viettel
	 * .core.entities.enumtype.KPaging, java.lang.String, java.lang.String,
	 * java.lang.Long)
	 */
	@Override
	public List<Customer> getListCustomerForOnlyShop(KPaging<Customer> kPaging, String shortCode, String customerName, String shopCode, List<Long> lstExceptionCus) throws DataAccessException {
		if (null == shopCode) {
			throw new IllegalArgumentException("shopCode is invalidate");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select c.*  ");
		sql.append(" from customer c, shop sh ");
		sql.append(" where c.shop_id = sh.shop_id and lower(sh.shop_code) = ? ");
		params.add(shopCode.toLowerCase());
		sql.append(" and c.status=?");
		params.add(ActiveType.RUNNING.getValue());
		if (lstExceptionCus != null && lstExceptionCus.size() > 0) {
			sql.append(" and c.customer_id not in(-1 ");
			for (Long id : lstExceptionCus) {
				sql.append(", ?");
				params.add(id);
			}
			sql.append(") ");
		}
		if (!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append(" and c.short_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append(" and upper(UNICODE2ENGLISH(c.customer_name)) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(customerName).trim().toUpperCase()));
		}
		sql.append(" order by c.status desc, c.customer_code asc, c.customer_name asc");
		if (kPaging == null)
			return repo.getListBySQL(Customer.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Customer.class, sql.toString(), params, kPaging);
	}

	private Integer getNextCode(Long id, Long shopId) throws DataAccessException {
		String sql = "select max(CUSTOMER_ID) as count from customer where customer_id <= ? and shop_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		params.add(shopId);
		return repo.countBySQL(sql, params) + 1;
	}

	@Override
	public List<Customer> getListCustomerByShopCode(KPaging<Customer> kPaging, String shopCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *");
		sql.append(" FROM customer s JOIN shop sh on s.shop_id = sh.shop_id");
		sql.append(" WHERE sh.shop_code = ?");
		params.add(shopCode.toUpperCase());
		sql.append(" AND s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (kPaging == null)
			return repo.getListBySQL(Customer.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Customer.class, sql.toString(), params, kPaging);
	}

	@Override
	public void updateDb() throws DataAccessException {
		String sql = "select * from customer";
		List<Object> params = new ArrayList<Object>();
		List<Customer> lst = repo.getListBySQL(Customer.class, sql, params);
		for (Customer c : lst) {
			if (c.getCustomerName() != null) {
				c.setNameText(Unicode2English.codau2khongdau(c.getCustomerName().toUpperCase()));
				// source bi do , phuocdh2 common lai

				this.updateCustomer(c, null);
			}
		}
	}

	@Override
	public List<CustomerVO> getListCustomerVO(Long shopId, String staffCode, String customerCode, String customerName, boolean isGroupByStaff, Integer status, Long parentStaffId, boolean checkMap) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		if (parentStaffId != null) {
			sql.append("with lstStaff as (");
			sql.append("select * from staff s");
			sql.append(" where 1=1");
			sql.append(" and exists (select 1 from channel_type where channel_type_id = s.staff_type_id and object_type in (?, ?) and type = ?)");
			params.add(StaffObjectType.NVBH.getValue());
			params.add(StaffObjectType.NVVS.getValue());
			params.add(ChannelTypeType.STAFF.getValue());
			if (checkMap) {
				sql.append(" and s.staff_id in (");
				sql.append(" select psm.staff_id as staffId from parent_staff_map psm");
				sql.append(" where psm.status = 1");
				sql.append(" and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id)");
				sql.append(" start with psm.parent_staff_id = ? AND PSM.STATUS = 1 connect by prior psm.staff_id = psm.parent_staff_id");
				params.add(parentStaffId);
				sql.append(" )");
			}
			sql.append(" and not exists (select 1 from exception_user_access where status = 1 and parent_staff_id = ? and staff_id = s.staff_id)");
			params.add(parentStaffId);
			sql.append(")");
		}

		sql.append("select * from( ");
		if (isGroupByStaff) {
			//NHOM THEO NHAN VIEN
			sql.append("SELECT cu.SHORT_CODE shortCode, ");
			sql.append("  cu.customer_name customerName, ");
			sql.append("  cu.status status, ");
			sql.append("  cu.address address, ");
			sql.append("  cu.street street, ");
			sql.append("  ar.precinct_name wardName, ");
			sql.append("  ar.district_name districtName, ");
			sql.append("  ar.province_name provinceName, ");
			//sql.append("  (ar.province_name || '--' || ar.district_name) areaName, ");
			sql.append("  nvl((case when (ar.province_name is not null and ar.district_name is not null) then (ar.province_name || '--' || ar.district_name) else (case when ar.province_name is not null then ar.province_name end) end), '-1') as areaName,   ");
			sql.append("  cu.mobiphone mobiphone, ");
			sql.append("  cu.phone phone, ");
			sql.append("  cu.housenumber housenumber, ");
			sql.append("  ct.channel_type_code channelTypeCode,    ");
			sql.append("  cu.max_debit_date maxDebitDate, ");
			sql.append("  ct.sale_amount saleAmount,    ");
			sql.append("  to_char(cu.create_date,'dd/mm/yyyy') createDate, ");
			sql.append("  to_char(cu.update_date,'dd/mm/yyyy') updateDate, ");
			//sql.append("  to_char(cu.last_approve_order,'dd/mm/yyyy') lastApproveOrder, ");
			sql.append("  to_char(sc.last_approve_order,'dd/mm/yyyy') lastApproveOrder, ");
			sql.append("  cu.invoice_tax invoiceTax, ");
			sql.append("  cu.invoice_number_account invoiceNumberAccount, ");
			//sql.append("  to_char(s.staff_code || ' -- ' ||s.staff_name) staffName, ");
			sql.append("  (case when s.staff_id is not null then to_char(s.staff_code|| ' -- ' ||s.staff_name) else '-1' end) as staffName,");
			sql.append("  sh.shop_name shopName, ");
			//sql.append("  1 isRouting ");
			sql.append(" (case when s.staff_id is not null then 1 else 0 end) as isRouting");
			sql.append(" FROM customer cu");
			sql.append(" JOIN shop sh ON sh.shop_id = cu.shop_id");
			sql.append(" LEFT JOIN area ar ON ar.area_id    = cu.area_id");
			sql.append(" LEFT JOIN channel_type ct on cu.channel_type_id = ct.channel_type_id");

			sql.append(" LEFT JOIN routing_customer rc ON rc.customer_id = cu.customer_id");
			sql.append(" LEFT JOIN routing rt ON rt.routing_id = rc.routing_id");
			sql.append(" LEFT JOIN visit_plan vp ON vp.routing_id = rt.routing_id");

			if (parentStaffId != null) {
				sql.append(" left join staff s on s.staff_id = vp.staff_id ");
			} else {
				sql.append(" LEFT JOIN staff s ON s.staff_id = vp.staff_id ");

				sql.append(" and exists (select 1 from channel_type where channel_type_id = s.staff_type_id and object_type in (?, ?) and type = ?)");
				params.add(StaffObjectType.NVBH.getValue());
				params.add(StaffObjectType.NVVS.getValue());
				params.add(ChannelTypeType.STAFF.getValue());
			}

			sql.append(" LEFT JOIN staff_customer sc ON cu.customer_id = sc.customer_id  and s.staff_id = sc.staff_id ");

			sql.append(" WHERE cu.status in (0,1) and rc.status = 1  and rt.status = 1 and vp.status = 1");
			sql.append(" and vp.from_date <= trunc(sysdate) and (vp.to_date >= trunc(sysdate) or vp.to_date is null)");

			sql.append(" and (nvl(rc.week1, 0) + nvl(rc.week2, 0) + nvl(rc.week3, 0) + nvl(rc.week4, 0)) > 0");
			sql.append(" and (");
			sql.append(" ((mod(floor((trunc(sysdate) - trunc(rc.start_date, 'iw')) / 7), 4) + 1) = 1 and rc.week1 = 1)");
			sql.append(" or ((mod(floor((trunc(sysdate) - trunc(rc.start_date, 'iw')) / 7), 4) + 1) = 2 and rc.week2 = 1)");
			sql.append(" or ((mod(floor((trunc(sysdate) - trunc(rc.start_date, 'iw')) / 7), 4) + 1) = 3 and rc.week3 = 1)");
			sql.append(" or ((mod(floor((trunc(sysdate) - trunc(rc.start_date, 'iw')) / 7), 4) + 1) = 4 and rc.week4 = 1)");
			sql.append(" )");
			sql.append(" AND rc.start_date  <= TRUNC(sysdate)");
			sql.append(" AND (rc.end_date   >= TRUNC(sysdate) OR rc.end_date     IS NULL)");

			if (shopId != null) {
				sql.append("   and sh.shop_id = ? ");
				params.add(shopId);
			}

			if (!StringUtility.isNullOrEmpty(staffCode)) {
				sql.append("   and s.staff_code like ? ESCAPE '/'");
				params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
			}
			if (!StringUtility.isNullOrEmpty(staffCode)) {
				String[] arrStaffCode = staffCode.split(",");
				sql.append("   and lower(s.staff_code) in ('-1' ");
				for (String value : arrStaffCode) {
					sql.append(" , ? ");
					params.add(value.toLowerCase().trim());
				}
				sql.append(" ) ");
			}

			if (!StringUtility.isNullOrEmpty(customerCode)) {
				String[] arrShortCode = customerCode.split(",");
				sql.append("   and lower(cu.short_code) in ('-1' ");
				for (String value : arrShortCode) {
					sql.append(" , ? ");
					params.add(value.toLowerCase().trim());
				}
				sql.append("   ) ");
			}
			if (!StringUtility.isNullOrEmpty(customerName)) {
				sql.append("   and upper(cu.customer_name) like ? ESCAPE '/'");
				params.add(StringUtility.toOracleSearchLikeSuffix(customerName.toUpperCase()));
			}
			if (status != null && status >= 0) {
				sql.append("   and cu.status = ? ");
				params.add(status);
			}
		}
		if (StringUtility.isNullOrEmpty(staffCode)) {//Neu ma nhan vien chuyen vao la null hoac rong
			if (isGroupByStaff) {
				sql.append("union ");
			}
			//KHONG NHOM THEO NHAN VIEN
			sql.append("SELECT cu.SHORT_CODE shortCode, ");
			sql.append("  cu.customer_name customerName, ");
			sql.append("  cu.status status, ");
			sql.append("  cu.address address, ");
			sql.append("  cu.street street, ");
			sql.append("  ar.precinct_name wardName, ");
			sql.append("  ar.district_name districtName, ");
			sql.append("  ar.province_name provinceName, ");
			//sql.append("  (ar.province_name || '--' || ar.district_name) areaName, ");
			sql.append("  nvl((case when (ar.province_name is not null and ar.district_name is not null) then (ar.province_name || '--' || ar.district_name) else (case when ar.province_name is not null then ar.province_name end) end), '-1') as areaName,   ");
			sql.append("  cu.mobiphone mobiphone, ");
			sql.append("  cu.phone phone, ");
			sql.append("  cu.housenumber housenumber, ");
			sql.append("  ct.channel_type_code channelTypeCode,    ");
			sql.append("  cu.max_debit_date maxDebitDate, ");
			sql.append("  ct.sale_amount saleAmount,    ");
			sql.append("  to_char(cu.create_date,'dd/mm/yyyy') createDate, ");
			sql.append("  to_char(cu.update_date,'dd/mm/yyyy') updateDate, ");
			sql.append("  (select TO_CHAR(MAX(last_approve_order),'dd/mm/yyyy') from staff_customer where customer_id = cu.customer_id) lastApproveOrder, ");
			//sql.append("  to_char(sc.last_approve_order,'dd/mm/yyyy') lastApproveOrder, "); --TUNGTT FIX BUG #0011519
			sql.append("  cu.invoice_tax invoiceTax, ");
			sql.append("  cu.invoice_number_account invoiceNumberAccount, ");
			sql.append("  to_char(-1) staffName, ");
			sql.append("  sh.shop_name shopName, ");
			sql.append("  0 isRouting ");
			sql.append("FROM customer cu ");
			sql.append("JOIN shop sh ");
			sql.append("ON sh.shop_id = cu.shop_id ");
			sql.append("LEFT JOIN area ar ");
			sql.append("ON ar.area_id    = cu.area_id ");
			sql.append("LEFT JOIN channel_type ct on cu.channel_type_id = ct.channel_type_id ");
			//sql.append("LEFT JOIN staff_customer sc ON cu.customer_id = sc.customer_id "); --TUNGTT FIX BUG #0011519
			sql.append("WHERE cu.status in (0,1) ");

			if (shopId != null) {
				sql.append("   and sh.shop_id = ? ");
				params.add(shopId);
			}
			if (!StringUtility.isNullOrEmpty(customerCode)) {
				String[] arrShortCode = customerCode.split(",");
				sql.append("   and lower(cu.short_code) in ('-1' ");
				for (String value : arrShortCode) {
					sql.append(" , ? ");
					params.add(value.toLowerCase().trim());
				}
				sql.append("   ) ");
			}
			if (!StringUtility.isNullOrEmpty(customerName)) {
				sql.append("   and upper(cu.customer_name) like ? ESCAPE '/'");
				params.add(StringUtility.toOracleSearchLikeSuffix(customerName.toUpperCase()));
			}
			if (status != null && status >= 0) {
				sql.append("   and cu.status = ? ");
				params.add(status);
			}
			if (isGroupByStaff) {
				sql.append("AND not EXISTS ");
				sql.append("  (SELECT 1 ");
				sql.append("  FROM routing_customer rc ");
				sql.append("  JOIN routing rt ");
				sql.append("  ON rt.routing_id = rc.routing_id ");
				sql.append("  JOIN visit_plan vp ");
				sql.append("  ON vp.routing_id     = rc.routing_id ");
				sql.append("  WHERE rc.status = 1 and rt.status = 1 and vp.status = 1 and rc.customer_id = cu.customer_id ");
				sql.append("  and vp.from_date <= trunc(sysdate) and (vp.to_date >= trunc(sysdate) or vp.to_date is null) ");

				sql.append(" and (nvl(rc.week1, 0) + nvl(rc.week2, 0) + nvl(rc.week3, 0) + nvl(rc.week4, 0)) > 0");
				sql.append(" and (");
				sql.append(" ((mod(floor((trunc(sysdate) - trunc(rc.start_date, 'iw')) / 7), 4) + 1) = 1 and rc.week1 = 1)");
				sql.append(" or ((mod(floor((trunc(sysdate) - trunc(rc.start_date, 'iw')) / 7), 4) + 1) = 2 and rc.week2 = 1)");
				sql.append(" or ((mod(floor((trunc(sysdate) - trunc(rc.start_date, 'iw')) / 7), 4) + 1) = 3 and rc.week3 = 1)");
				sql.append(" or ((mod(floor((trunc(sysdate) - trunc(rc.start_date, 'iw')) / 7), 4) + 1) = 4 and rc.week4 = 1)");
				sql.append(" )");
				sql.append(" AND rc.start_date  <= TRUNC(sysdate)");
				sql.append(" AND (rc.end_date   >= TRUNC(sysdate) OR rc.end_date     IS NULL)");

				sql.append("  )");
			}
		}
		sql.append("  )");
		if (isGroupByStaff) {
			sql.append(" order by isRouting desc,staffName,UNICODE2ENGLISH(provinceName),UNICODE2ENGLISH(districtName),shortCode,customerName");
		} else {
			sql.append(" order by UNICODE2ENGLISH(provinceName),UNICODE2ENGLISH(districtName),shortCode,customerName");
		}
		String[] fieldNames = { "shortCode", "customerName", "status", "address", "street", "wardName", "districtName", "provinceName", "areaName", "mobiphone", "channelTypeCode", "maxDebitDate", "saleAmount", "createDate", "updateDate",
				"lastApproveOrder", "invoiceTax", "invoiceNumberAccount", "staffName", "shopName", "isRouting", "phone", "housenumber" };

		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<Customer> searchCustomerVOForDSKH(KPaging<Customer> kPaging, String shopCode, String staffCode, String customerCode, String customerName) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from( ");
		sql.append("SELECT cu.* ");
		sql.append("FROM customer cu ");
		sql.append("JOIN area ar ");
		sql.append("ON ar.area_id    = cu.area_id ");
		sql.append("JOIN routing_customer rc ");
		sql.append("ON rc.customer_id = cu.customer_id ");
		sql.append("JOIN routing rt ");
		sql.append("ON rt.routing_id = rc.routing_id ");
		sql.append("JOIN visit_plan vp ");
		sql.append("ON vp.routing_id = rt.routing_id ");
		sql.append("JOIN staff s ");
		sql.append("ON s.staff_id = vp.staff_id ");
		sql.append("JOIN shop sh ");
		sql.append("ON sh.shop_id = cu.shop_id ");
		sql.append("WHERE cu.status in (0,1) and rc.status = 1 and vp.status = 1");
		sql.append(" and vp.from_date <= trunc(sysdate) and (vp.to_date >= trunc(sysdate) or vp.to_date is null)");
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append("   and upper(sh.shop_code) = ? ");
			params.add(shopCode.toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append("   and s.staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append("   and cu.customer_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append("   and cu.customer_name like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(customerName.toUpperCase()));
		}
		sql.append("  )");
		sql.append(" order by short_code, customer_name ");

		//String countSql = "select count(1) count from (" + sql.toString() + ")";

		if (kPaging == null)
			return repo.getListBySQL(Customer.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Customer.class, sql.toString(), params, kPaging);

	}

	@Override
	public List<RptCustomerByRoutingDataVO> getRptCustomerByRouting(Long shopId, List<Long> listStaffId, List<Long> listCustomerId, String customerName) throws DataAccessException {
		StringBuilder findStaffId = new StringBuilder(" ( ");
		StringBuilder findCustomerId = new StringBuilder(" ( ");

		if (null != listStaffId && listStaffId.size() > 0) {
			for (int i = 0; i < listStaffId.size(); i++) {
				if (i < listStaffId.size() - 1) {
					findStaffId.append("?, ");
				} else {
					findStaffId.append("?) ");
				}
			}
		}

		if (null != listCustomerId && listCustomerId.size() > 0) {
			for (int i = 0; i < listCustomerId.size(); i++) {
				if (i < listCustomerId.size() - 1) {
					findCustomerId.append("?, ");
				} else {
					findCustomerId.append("?) ");
				}
			}
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select staff_id as staffId, staff_name as staffName, staff_code as staffCode, ");
		sql.append(" 	 DECODE(staff_status, 1, 'Hoạt động', 'Không hoạt động') as staffStatus, ");
		sql.append("     short_code as customerCode, customer_address customerAddress, mobiphone, customer_status customerStatus, ");
		sql.append("     week_interval as interval, ");
		sql.append("     channel_type_code as customerType, ");
		sql.append("     CASE WHEN day_name = 1 THEN seq8 WHEN day_name = 2 THEN seq2 WHEN day_name = 3 THEN seq3 WHEN day_name = 4 THEN seq4 WHEN day_name = 5 THEN seq5 WHEN day_name = 6 THEN seq6 WHEN day_name = 7 THEN seq7 ELSE 0 END as seq, ");
		sql.append("     seq2, seq3, seq4, seq5, seq6, seq7, seq8, ");
		sql.append("     '' as displayPosition, '' as displayName, ");
		sql.append("      case when day_name = 1 then 'CN' ");
		sql.append("             when day_name = 2 then 'Thu 2' ");
		sql.append("             when day_name = 3 then 'Thu 3' ");
		sql.append("             when day_name = 4 then 'Thu 4' ");
		sql.append("             when day_name = 5 then 'Thu 5' ");
		sql.append("             when day_name = 6 then 'Thu 6' ");
		sql.append("             when day_name = 7 then 'Thu 7' ");
		sql.append("             else '' ");
		sql.append("      end dateName ");
		sql.append(" from ( ");
		sql.append("     select st.staff_id, st.staff_name, st.staff_code, st.status staff_status, ");
		sql.append("         c.short_code, ");
		sql.append("         c.customer_name || ' - ' || c.address || ' - ' || c.street as customer_address, ");
		sql.append("         c.mobiphone, ");
		sql.append("         c.status as customer_status, ");
		sql.append("         rc.seq, rc.seq2, rc.seq3, rc.seq4, rc.seq5, rc.seq6, rc.seq7, rc.seq8, ");
		sql.append("         rc.week_interval, ");
		sql.append("         ct.channel_type_code, ");
		sql.append("         case when rowno = 1 and rc.sunday = 1 then 1 ");
		sql.append("             when rowno = 2 and rc.monday = 1 then 2 ");
		sql.append("             when rowno = 3 and rc.tuesday = 1 then 3 ");
		sql.append("             when rowno = 4 and rc.wednesday = 1 then 4 ");
		sql.append("             when rowno = 5 and rc.thursday = 1 then 5 ");
		sql.append("             when rowno = 6 and rc.friday = 1 then 6 ");
		sql.append("             when rowno = 7 and rc.saturday = 1 then 7 ");
		sql.append("             else null  ");
		sql.append("         end day_name ");
		sql.append("     from staff st, visit_plan vp, routing r, routing_customer rc, customer c, channel_type ct, (select rownum rowno from dual connect by level <= 7) ");
		sql.append("     where st.staff_id = vp.staff_id ");
		sql.append("         and vp.routing_id = r.routing_id ");
		sql.append("         and r.routing_id = rc.routing_id ");
		sql.append("         and rc.customer_id = c.customer_id ");
		sql.append("         and c.channel_type_id = ct.channel_type_id ");
		sql.append("         and (nvl(rc.monday, 0) + nvl(rc.tuesday, 0) + nvl(rc.wednesday, 0) + nvl(rc.thursday, 0) + nvl(rc.friday, 0) + nvl(rc.saturday, 0) + nvl(rc.sunday, 0)) > 1 ");
		sql.append("         and vp.status = 1 ");
		sql.append("         and vp.from_date <= trunc(sysdate) ");
		sql.append("         and (vp.to_date >= trunc(sysdate) or vp.to_date is null) ");
		sql.append("         and r.status = ? ");
		params.add(ActiveType.RUNNING.getValue());

		sql.append("         and rc.status = ? ");
		params.add(ActiveType.RUNNING.getValue());

		sql.append("         and c.status <> ? ");
		params.add(ActiveType.DELETED.getValue());

		sql.append("         and st.shop_id = ? ");
		params.add(shopId);

		sql.append("         and vp.shop_id = ? ");
		params.add(shopId);

		sql.append("         and r.shop_id = ? ");
		params.add(shopId);

		sql.append("         and c.shop_id = ? ");
		params.add(shopId);

		if (null != listStaffId && listStaffId.size() > 0) {
			sql.append("         and st.staff_id in ");
			sql.append(findStaffId);

			params.addAll(listStaffId);
		}

		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append("         and upper(c.customer_name) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(customerName.toUpperCase()));
		}

		if (null != listCustomerId && listCustomerId.size() > 0) {
			sql.append("         and c.customer_id in ");
			sql.append(findCustomerId);

			params.addAll(listCustomerId);
		} else {
			sql.append("         and exists(select 1 from staff st, channel_type ct where st.staff_type_id = ct.channel_type_id ");
			sql.append("				and st.shop_id = ? ");
			sql.append("				and ct.object_type in (?, ?) ");
			sql.append("		 )");

			params.add(shopId);
			params.add(StaffObjectType.NVBH.getValue());
			params.add(StaffObjectType.NVVS.getValue());
		}

		sql.append(") ");
		sql.append(" where day_name is not null ");
		sql.append(" order by staff_code, day_name, seq2, seq3, seq4, seq5, seq6, seq7, seq8 ");

		final String[] fieldNames = new String[] { "staffId", "staffCode", "staffName", "staffStatus", "customerCode", "seq", "mobiphone", "dateName", "customerType", "customerStatus", "displayPosition", "displayName", "interval", "customerAddress" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };

		//String fullSql = TestUtils.addParamToQuery(sql.toString(), params.toArray());
		//System.out.println(fullSql);

		return repo.getListByQueryAndScalar(RptCustomerByRoutingDataVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<Customer> getListCustomerByStaffRouting(KPaging<Customer> kPaging, Long shopId, String customerCode, String customerName, Long staffId, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		if (null == shopId) {
			throw new IllegalArgumentException(ExceptionCode.SHOP_ID_IS_NULL);
		}

		if (null == staffId) {
			throw new IllegalArgumentException(ExceptionCode.STAFF_ID_IS_NULL);
		}

		sql.append(" SELECT * ");
		sql.append(" FROM ");
		sql.append("   (SELECT distinct(rc.customer_id) ");
		sql.append("    FROM staff st, visit_plan vp, ");
		sql.append("         routing_customer rc, ");
		sql.append("         routing r ");
		sql.append("    WHERE st.staff_id = vp.staff_id ");
		sql.append("      AND st.shop_id = ? ");
		params.add(shopId);

		sql.append("      AND st.staff_id = ? ");
		params.add(staffId);

		sql.append("      AND vp.routing_id = r.routing_id ");
		sql.append("      AND r.routing_id = rc.routing_id ");
		sql.append("      AND r.status = 1 ");
		sql.append("      AND rc.status = 1 ");
		sql.append("      AND vp.status = 1) tb1 ");
		sql.append(" JOIN customer c ON tb1.customer_id = c.customer_id ");
		if (null != status) {
			sql.append(" AND c.status = ? ");
			params.add(status.getValue());
		} else {
			sql.append(" AND c.status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}

		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append(" and c.customer_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append(" and upper(c.customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(customerName.toUpperCase()));
		}

		//		System.out.println(TestUtils.addParamToQuery(sql.toString(),
		//				params.toArray()));
		if (kPaging == null)
			return repo.getListBySQL(Customer.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Customer.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<CustomerGTVO> getListCustomerGTVOByGroupTransfer(KPaging<CustomerGTVO> kPaging, Long shopId, String groupTransferCode, String groupTransferName, String staffTransferCode, String shortCode, ActiveType status, String shopCode,
			Boolean check, String customerName) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT g.group_transfer_code groupTransferCode");
		sql.append("	, g.group_transfer_name groupTransferName");
		sql.append("	, c.short_code shortCode");
		sql.append("	, c.customer_name customerName");
		sql.append("	, sh.shop_code shopCode");
		sql.append(" FROM GROUP_TRANSFER g join shop sh on sh.shop_id = g.shop_id ");
		sql.append(" 	join customer c on c.group_transfer_id = g.group_transfer_id");
		sql.append("	where 1 = 1");

		if (shopId != null) {
			if (Boolean.TRUE.equals(check)) {
				sql.append(" AND EXISTS");
				sql.append("   (SELECT 1");
				sql.append("   FROM shop s");
				sql.append("   WHERE s.shop_id            = g.shop_id");
				sql.append("     START WITH shop_id       = ?");
				params.add(shopId);
				sql.append("     CONNECT BY prior shop_id = parent_shop_id");
				sql.append("   )");
			} else {
				sql.append(" AND EXISTS");
				sql.append("   (SELECT 1");
				sql.append("   FROM shop s");
				sql.append("   WHERE s.shop_id     = g.shop_id");
				sql.append("   AND (s.shop_id      = ?");
				params.add(shopId);
				sql.append("   OR s.parent_shop_id = ?)");
				params.add(shopId);
				sql.append("   )");
			}
		}

		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and sh.shop_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(groupTransferCode)) {
			sql.append(" and g.group_transfer_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(groupTransferCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(groupTransferName)) {
			sql.append(" and lower(g.group_transfer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(groupTransferName.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(staffTransferCode)) {
			sql.append(" and exists (select 1 from STAFF s where s.staff_id=g.staff_id and staff_code like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffTransferCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append(" and  c.short_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append(" and  upper(c.customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(customerName.toUpperCase()));
		}
		if (status != null) {
			sql.append(" and g.status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and g.status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" order by sh.shop_code, g.group_transfer_code");

		String countSql = "select count(1) count from (" + sql.toString() + ")";

		String[] fieldNames = { "groupTransferCode", "groupTransferName", "shortCode", "customerName", "shopCode" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (kPaging == null)
			return repo.getListByQueryAndScalar(CustomerGTVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(CustomerGTVO.class, fieldNames, fieldTypes, sql.toString(), countSql, params, params, kPaging);
	}

	public static void main(String[] args) {
		System.out.println(Integer.MAX_VALUE);
	}

	@Override
	public CustomerVO getCustomerVisitting(Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		//sql.append(" SELECT SUBSTR(c.customer_code, 0, 3) AS shortCode, CASE when c.housenumber is null and c.street is null then to_nchar('') WHEN c.housenumber IS NULL and c.street IS not NULL THEN TO_NCHAR (c.street) when c.housenumber is not null and c.street is null then TO_NCHAR(c.housenumber) ELSE TO_NCHAR (c.housenumber || ',' || c.street) END AS address, c.customer_name as customerName");
		sql.append(" SELECT c.short_code AS shortCode, CASE when c.housenumber is null and c.street is null then to_nchar('') WHEN c.housenumber IS NULL and c.street IS not NULL THEN TO_NCHAR (c.street) when c.housenumber is not null and c.street is null then TO_NCHAR(c.housenumber) ELSE TO_NCHAR (c.housenumber || ',' || c.street) END AS address, c.customer_name as customerName");
		sql.append(" FROM customer c ");
		sql.append(" JOIN action_log al ");
		sql.append(" ON c.customer_id =al.customer_id ");
		sql.append(" WHERE al.staff_id = ? ");
		params.add(staffId);
		sql.append(" AND TRUNC(al.start_time)=TRUNC(sysdate) ");
		sql.append(" AND al.end_time IS NULL ");
		sql.append(" AND al.object_type = 0 ");
		final String[] fieldNames = new String[] { "shortCode", "address", "customerName" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		List<CustomerVO> vos = repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return vos != null && vos.size() > 0 ? vos.get(0) : null;
	}

	@Override
	public List<CustomerVO> getListCustomerBySaleStaffHasVisitPlan(Long staffId, Date checkDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT DISTINCT c.customer_id AS id, ");
		sql.append(" c.customer_code AS customerCode, ");
		sql.append(" c.customer_name AS customerName, ");
		sql.append(" c.short_code AS shortCode, ");
		sql.append(" c.short_code  || '-'  || c.customer_name AS customerCodeName, ");
		sql.append(" CASE when c.housenumber is null and c.street is null then to_nchar('') WHEN c.housenumber IS NULL and c.street IS not NULL THEN TO_NCHAR (c.street) when c.housenumber is not null and c.street is null then TO_NCHAR(c.housenumber) ELSE TO_NCHAR (c.housenumber || ',' || c.street) END AS address,  c.mobiphone as mobiphone, ");
		//		sql.append(" c.phone,  NVL(ROUND(rssd.amount_plan), 0) AS amountPlan,  NVL(ROUND(rssd.amount), 0) as amount,NVL(ROUND(rssd.amount_approved), 0) as dsDuyet,  ");
		sql.append(" c.phone,  NVL(ROUND(rssd.day_amount_plan), 0) AS amountPlan,  NVL(ROUND(rssd.day_amount), 0) as amount,NVL(ROUND(rssd.day_amount_approved), 0) as dsDuyet,  ");
		sql.append(" (CASE TO_CHAR((?),'D') ");
		params.add(checkDate);
		sql.append(" WHEN '2'      THEN rc.SEQ2 ");
		sql.append(" WHEN '3'      THEN rc.SEQ3 ");
		sql.append(" WHEN '4'      THEN rc.SEQ4 ");
		sql.append(" WHEN '5'      THEN rc.SEQ5 ");
		sql.append(" WHEN '6'      THEN rc.SEQ6 ");
		sql.append(" WHEN '7'      THEN rc.SEQ7 ");
		sql.append(" ELSE NULL ");
		sql.append(" END )  AS ordinalVisit,  al.object_type as objectType, al.start_time as startTime, al.end_time as endTime,  c.lat,  c.lng,al.is_or as isOr, ");
		sql.append(" (select count(customer_id) as count from action_log al where TRUNC(START_TIME) = TRUNC(?) ");
		params.add(checkDate);
		sql.append(" and (object_type=4) and customer_id = rc.customer_id and staff_id = vp.staff_id )AS count ");
		sql.append(" FROM customer c ");
		sql.append(" JOIN ROUTING_CUSTOMER rc ON rc.customer_id = c.customer_id and rc.status = 1 inner join ROUTING r on r.ROUTING_ID = rc.ROUTING_ID and r.status = 1 inner join VISIT_PLAN vp on vp.ROUTING_ID = r.ROUTING_ID ");
		sql.append(" left join action_log al on vp.staff_id  = al.staff_id and al.customer_id  = rc.customer_id  AND TRUNC(al.start_time) = TRUNC(?) and al.OBJECT_TYPE in (0, 1) ");
		params.add(checkDate);
		sql.append(" left join RPT_STAFF_SALE_DETAIL rssd on rssd.customer_id= c.customer_id and trunc(rssd.SALE_DATE) = trunc(?) and rssd.shop_id = c.shop_id ");
		params.add(checkDate);
		/**
		 * SangTN 26-05-2014 Update thay doi phan tuyen
		 */
		//		sql.append(" WHERE c.status=1 and vp.status =1 "
		//				+ "AND ((trunc(?,'iw') - trunc(rc.start_date, 'iw'))               >= 0) "
		//params.add(checkDate);
		//				+ "AND (mod((TRUNC(?,'iw')- TRUNC(rc.start_date, 'iw'))/7, week_interval) = 0) ");
		//params.add(checkDate);
		sql.append(" WHERE c.status=1 and vp.status =1 ");
		sql.append(" 	and (nvl(rc.week1, 0) + nvl(rc.week2, 0) + nvl(rc.week3, 0) + nvl(rc.week4, 0)) > 0 ");
		sql.append("    and ( ");
		sql.append("        ((mod(floor((trunc(?, 'iw') - trunc(rc.start_date, 'iw')) / 7), 4) + 1) = 1 and rc.week1 = 1) ");
		params.add(checkDate);
		sql.append("        or ((mod(floor((trunc(?, 'iw') - trunc(rc.start_date, 'iw')) / 7), 4) + 1) = 2 and rc.week2 = 1) ");
		params.add(checkDate);
		sql.append("        or ((mod(floor((trunc(?, 'iw') - trunc(rc.start_date, 'iw')) / 7), 4) + 1) = 3 and rc.week3 = 1) ");
		params.add(checkDate);
		sql.append("        or ((mod(floor((trunc(?, 'iw') - trunc(rc.start_date, 'iw')) / 7), 4) + 1) = 4 and rc.week4 = 1) ");
		params.add(checkDate);
		sql.append("    ) ");
		/** End SangTN */

		sql.append(" and vp.staff_id = ? ");
		params.add(staffId);
		sql.append(" and trunc(vp.from_date) <= trunc(?) ");
		params.add(checkDate);
		sql.append(" and ( trunc(vp.to_date) >= trunc(?) or vp.to_date is null) ");
		params.add(checkDate);
		sql.append(" AND TRUNC(rc.start_date)                                                 <= TRUNC(?) ");
		params.add(checkDate);
		sql.append(" AND ( TRUNC(rc.end_date)                                                 >= TRUNC(?) ");
		params.add(checkDate);
		sql.append(" OR rc.end_date                                                           IS NULL) ");
		sql.append(" AND ((TO_CHAR((?),'D')=1  AND SUNDAY                   =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D')   =2 AND MONDAY                   =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D')   =3 AND TUESDAY                  =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D')   =4 AND WEDNESDAY                =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D')   =5 AND THURSDAY                 =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D')   =6 AND FRIDAY                   =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D')   =7 AND SATURDAY                 =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D')=1 AND SUNDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =2 AND MONDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =3 AND TUESDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =4 AND WEDNESDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =5 AND THURSDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =6 AND FRIDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =7 AND SATURDAY =0 AND al.is_or=1)) ");
		params.add(checkDate);
		sql.append("order by al.start_time");
		final String[] fieldNames = new String[] { "id", //1
				"customerCode",//2
				"customerName",//3
				"shortCode",//4
				"customerCodeName",//5
				"address",//6				
				"mobiphone",//7
				"phone",//8
				"amountPlan",//9
				"amount",//10
				"dsDuyet",//10.1
				"ordinalVisit",//11
				"objectType",//12
				"startTime", "endTime",//13
				"lat",//14
				"lng",//15
				"isOr",//16
				"count" };//17
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.STRING, //6		
				StandardBasicTypes.STRING,//7
				StandardBasicTypes.STRING,//8
				StandardBasicTypes.BIG_DECIMAL,//9
				StandardBasicTypes.BIG_DECIMAL,//10
				StandardBasicTypes.BIG_DECIMAL,//10.1
				StandardBasicTypes.INTEGER, //11
				StandardBasicTypes.INTEGER, //12
				StandardBasicTypes.TIMESTAMP,//13 
				StandardBasicTypes.TIMESTAMP,//13 
				StandardBasicTypes.STRING, //14
				StandardBasicTypes.STRING,//15
				StandardBasicTypes.INTEGER,//16
				StandardBasicTypes.INTEGER };//17
		List<CustomerVO> vos = repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return vos;
	}

	@Override
	public List<Customer> getListCustomerByShortCode(Long shopId, String shortCode) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(shortCode)) {
			throw new IllegalArgumentException("invalid code");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		String[] arrShortCode = shortCode.split(",");
		sql.append("select * from customer where lower(short_code) in (?");
		params.add(arrShortCode[0].trim().toLowerCase());
		if (arrShortCode.length > 1) {
			for (int i = 1; i < arrShortCode.length; i++) {
				sql.append(", ?");
				params.add(arrShortCode[i].trim().toLowerCase());
			}
		}
		sql.append(" ) ");

		if (shopId != null) {
			sql.append(" and shop_id = ?");
			params.add(shopId);
		}

		return repo.getListBySQL(Customer.class, sql.toString(), params);
	}

	@Override
	public void removeTypeOfCustomer(long typeId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("update customer");
		sql.append(" set channel_type_id = null");
		sql.append(" where channel_type_id = ?");
		params.add(typeId);

		repo.executeSQLQuery(sql.toString(), params);
	}

	@Override
//	public List<CustomerVO> searchListCustomerVOByFilter(CustomerFilter<CustomerVO> filter) throws DataAccessException {
//		if (filter.getLstShopId() == null || filter.getLstShopId().size() == 0) {
//			throw new IllegalArgumentException("filter.getLstShopId()");
//		}
//		StringBuilder sql = new StringBuilder();
//		StringBuilder countSql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		StringBuilder fromSql = new StringBuilder();
//
//		sql.append(" select  ");
//		sql.append(" customer_id as id  ");
//		sql.append(" , short_code as shortCode  ");
//		sql.append(" , customer_code as customerCode  ");
//		sql.append(" , customer_name as customerName  ");
//		sql.append(" , address as address  ");
//		sql.append(" , (select channel_type_name from channel_type where channel_type_id = cus.channel_type_id) as channelTypeName  ");
//		fromSql.append(" from customer cus ");
//		fromSql.append(" where 1 = 1  ");
//		if (filter.getStatus() != null) {
//			fromSql.append(" and cus.status = ?  ");
//			params.add(filter.getStatus());
//		} else {
//			fromSql.append(" and cus.status != ? ");
//			params.add(ActiveType.DELETED.getValue());
//		}
//		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
//			fromSql.append(" and lower(cus.short_code) like ? ESCAPE '/' ");
//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShortCode().trim().toLowerCase()));
//		}
//		if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
//			fromSql.append(" and lower(cus.name_text) like ? ESCAPE '/' ");
//			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getCustomerName().trim()).toLowerCase()));
//
//		}
//		if (!StringUtility.isNullOrEmpty(filter.getAddress())) {
//			fromSql.append(" and lower(cus.address) like ? ESCAPE '/' ");
//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getAddress().trim().toLowerCase()));
//		}
//		if (filter.getObjectType() != null) {
//			fromSql.append(" and cus.channel_type_id in (select channel_type_id from channel_type where type = ? and object_type = ?) ");
//			params.add(ChannelTypeType.CUSTOMER.getValue());
//			params.add(filter.getObjectType());
//		}
//
//		fromSql.append(" and cus.shop_id in (  ");
//		fromSql.append("  select sh.shop_id from shop sh where status = 1 start with sh.shop_id in (-1  ");
//		for (Long value : filter.getLstShopId()) {
//			fromSql.append("  , ? ");
//			params.add(value);
//		}
//		fromSql.append(" ) connect by prior sh.shop_id = sh.parent_shop_id )  ");
//
//		///Co the bo sung them dieu kien tim kiem con thieu
//		sql.append(fromSql.toString());
//		//Append Chuoi From and Where
//		sql.append(" order by customerCode ");
//		//Count total
//		countSql.append(" select count(1) count ");
//		countSql.append(fromSql.toString());
//		final String[] fieldNames = new String[] { "id", "shortCode", "customerCode", "customerName", "address", "channelTypeName" };
//
//		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
//		if (filter.getkPaging() == null)
//			return repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
//		else
//			return repo.getListByQueryAndScalarPaginated(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
//	}
	
	public List<CustomerVO> searchListCustomerVOByFilter(CustomerFilter<CustomerVO> filter) throws DataAccessException {
		if (filter.getLstShopId() == null || filter.getLstShopId().size() == 0) {
			throw new IllegalArgumentException("filter.getLstShopId()");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		List<Object> countParams = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" select distinct  ");
		sql.append(" customer_id as id  ");
		sql.append(" , short_code as shortCode  ");
		sql.append(" , customer_code as customerCode ");
		sql.append(" , customer_name as customerName  ");
		sql.append(" , address as address ");
		sql.append(" , (select channel_type_name from channel_type where channel_type_id = cus.channel_type_id) as channelTypeName  ");
		sql.append(" , phone as phone ");
		sql.append(" , mobiphone as mobiphone ");
		sql.append(" , street");
		sql.append(" , housenumber ");
		sql.append(" , cus.area_id as areaId ");
		sql.append(" , phuong.area_code as wardCode ");
		sql.append(" , quan.area_code as districtCode ");
		sql.append(" , tinh.area_code as provinceCode ");
		sql.append(" , (select shop_code from shop sh where sh.shop_id = cus.shop_id) shopCode ");
		if (!StringUtility.isNullOrEmpty(filter.getNganhHangParam()) && filter.getSothangParam() != null) {
			sql.append(" ,(select NVL(sum(rptm.amount),0)");			
			sql.append(" from rpt_sale_in_month rptm ");
			sql.append(" join product pd on pd.product_id = rptm.product_id ");
			sql.append(" join product_info pi on pd.cat_id = pi.product_info_id ");
			sql.append(" where 1= 1 ");
			sql.append(" and rptm.status = 1 ");
			sql.append(" and rptm.customer_id = cus.customer_id ");
			sql.append(" AND (? is null ");
			sql.append(" or pi.product_info_code in (SELECT trim(regexp_substr(?,'[^,]+', 1, level)) ");
			sql.append(" 				from dual CONNECT BY regexp_substr(?, '[^,]+', 1, level) IS NOT NULL ");
			sql.append(" 					) ");
			sql.append(" ) ");
			params.add(filter.getNganhHangParam());
			params.add(filter.getNganhHangParam());
			params.add(filter.getNganhHangParam());
			sql.append(" and EXTRACT(MONTH from rptm.month) >= (select EXTRACT( MONTH FROM sysdate) - ? from dual) ");
			params.add(filter.getSothangParam());
			sql.append(" and EXTRACT(MONTH from rptm.month) < (select EXTRACT( MONTH FROM sysdate) from dual)) ");
			if (!BigDecimal.ZERO.equals(filter.getSothangParam())) {
				sql.append("/? ");
				params.add(filter.getSothangParam());
			}
			sql.append(" as amountEquip ");
		} else {
			sql.append(" , 0 as amountEquip ");
		}
		fromSql.append(" from customer cus ");
		fromSql.append(" join area phuong on cus.area_id = phuong.area_id ");
		fromSql.append(" join area quan on phuong.parent_area_id = quan.area_id ");
		fromSql.append(" join area tinh on quan.parent_area_id = tinh.area_id ");
		fromSql.append(" where 1 = 1  ");
		if (filter.getStatus() != null) {
			fromSql.append(" and cus.status = ?  ");
			params.add(filter.getStatus());
			countParams.add(filter.getStatus());
		} else {
			fromSql.append(" and cus.status != ? ");
			params.add(ActiveType.DELETED.getValue());
			countParams.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			fromSql.append(" and (cus.short_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getShortCode().trim()).toUpperCase()));
			countParams.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getShortCode().trim()).toUpperCase()));
			
			fromSql.append(" or upper(cus.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getShortCode().trim()).toUpperCase()));
			countParams.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getShortCode().trim()).toUpperCase()));
			
			fromSql.append(" ) ");
		}
//		if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
//			fromSql.append(" and lower(cus.name_text) like ? ESCAPE '/' ");
//			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getCustomerName().trim()).toLowerCase()));
//			countParams.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getCustomerName().trim()).toLowerCase()));
//		}
//		if (!StringUtility.isNullOrEmpty(filter.getAddress())) {
//			fromSql.append(" and lower(cus.name_text) like ? ESCAPE '/' ");
//			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getAddress()).trim().toLowerCase()));
//			countParams.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getAddress()).trim().toLowerCase()));
//		}
		if (filter.getObjectType() != null) {
			fromSql.append(" and cus.channel_type_id in (select channel_type_id from channel_type where type = ? and object_type = ?) ");
			params.add(ChannelTypeType.CUSTOMER.getValue());
			params.add(filter.getObjectType());
			countParams.add(ChannelTypeType.CUSTOMER.getValue());
			countParams.add(filter.getObjectType());
		}

		if (filter.getLstShopId() != null && !filter.getLstShopId().isEmpty()) {
			fromSql.append(" and cus.shop_id in (  ");
			fromSql.append("  select sh.shop_id from shop sh where status = 1 start with sh.shop_id in (?  ");
			params.add(filter.getLstShopId().get(0));
			countParams.add(filter.getLstShopId().get(0));
			for (int i = 1, size = filter.getLstShopId().size(); i < size; i++) {
				fromSql.append("  , ? ");
				params.add(filter.getLstShopId().get(i));
				countParams.add(filter.getLstShopId().get(i));
			}
			fromSql.append(" ) connect by prior sh.shop_id = sh.parent_shop_id )  ");
		}

		///Co the bo sung them dieu kien tim kiem con thieu
		sql.append(fromSql.toString());
		//Append Chuoi From and Where
		sql.append(" order by shopCode, customerCode ");
		//Count total
		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());
		final String[] fieldNames = new String[] { "id", "shortCode", "customerCode", "customerName", "address", "channelTypeName", "phone", "mobiphone", "street", "housenumber", "wardCode", "districtCode", "provinceCode", "shopCode", "amountEquip",
				"areaId" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
												StandardBasicTypes.STRING,
												StandardBasicTypes.STRING,
												StandardBasicTypes.STRING,
												StandardBasicTypes.STRING,
												StandardBasicTypes.STRING,
												StandardBasicTypes.STRING,
												StandardBasicTypes.BIG_DECIMAL,
												StandardBasicTypes.LONG
												};
		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, countParams, filter.getkPaging());
	}

	@Override
	public List<CustomerVO> searchListCustomerVOForRoutingByFilter(CustomerFilter<CustomerVO> filter) throws DataAccessException {
		if (filter.getLstShopId() == null || filter.getLstShopId().size() == 0) {
			throw new IllegalArgumentException("filter.getLstShopId() is null");
		}
		if (filter.getStaffRootId() == null) {
			throw new IllegalArgumentException("getStaffRootId() is null");
		}

		if (StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			filter.setStaffCode("-1");
		} else {
			filter.setStaffCode(filter.getStaffCode().trim().replace(",", "esc"));
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with rstaffcode as (  ");
		sql.append(" 		select trim(regexp_substr(trim(?), '[^esc]+', 1, level)) from dual connect by trim(regexp_substr(trim(?), '[^esc]+', 1, level)) is not null   ");
		params.add(filter.getStaffCode());
		params.add(filter.getStaffCode());
		sql.append(" 		),  ");
		sql.append(" 		Rparentstaff as (");
		sql.append(" 		    select psm.staff_id as staff_id from parent_staff_map psm where psm.status = 1  ");
		sql.append(" 		    and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and psm.staff_id = parent_staff_id)  ");
		sql.append(" 			    start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id  ");
		params.add(filter.getStaffRootId());
		sql.append(" 			),  ");
		sql.append(" 		tb1 as (SELECT distinct(rc.customer_id)  ");
		sql.append(" 	    from staff st left join Rparentstaff rs on rs.staff_id = st.staff_id join visit_plan vp on st.staff_id = vp.staff_id  ");
		if (filter.getpDate() != null) {
			sql.append(" and from_date < trunc(?)+1 and (to_date is null or to_date >= trunc(?))");
			params.add(filter.getpDate());
			params.add(filter.getpDate());
		}
		sql.append(" 	    join routing r on vp.routing_id = r.routing_id join routing_customer rc on r.routing_id = rc.routing_id  ");
		sql.append(" 	    where 1 = 1  ");
		if (filter.getStaffCode().equals("-1")) {
			sql.append(" and lower(st.staff_code) not in (select * from rstaffcode)  ");
		} else {
			sql.append(" and lower(st.staff_code) in (select * from rstaffcode)  ");
		}
		if (filter.getIsFlagCMS() != null && filter.getIsFlagCMS()) {
			sql.append(" and rs.staff_id is not null  ");
		}
		sql.append(" 	    and st.staff_id not in (select staff_id from exception_user_access where status = 1  and st.staff_id = parent_staff_id)  ");
		sql.append(" 	    and st.shop_id in ( select shop_id from shop WHERE status = 1 start with shop_id in (-1  ");
		for (Long value : filter.getLstShopId()) {
			sql.append("  , ? ");
			params.add(value);
		}
		sql.append(" 	 ) connect by prior shop_id = parent_shop_id)  ");
		sql.append(" 	    AND r.status = 1 AND rc.status = 1 AND vp.status = 1)  ");
		sql.append(" 	SELECT   ");
		sql.append(" 	 cus.customer_id as id   ");
		sql.append(" 			 , cus.short_code as shortCode   ");
		sql.append(" 			 , cus.customer_code as customerCode   ");
		sql.append(" 			 , cus.customer_name as customerName   ");
		sql.append(" 			 , cus.address as address   ");
		sql.append(" 			 , (select channel_type_name from channel_type where channel_type_id = cus.channel_type_id) as channelTypeName   ");
		sql.append(" 	FROM tb1 JOIN customer cus ON tb1.customer_id = cus.customer_id   ");

		sql.append(" where 1 = 1  ");
		if (filter.getStatus() != null) {
			sql.append(" and cus.status = ?  ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and cus.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and lower(cus.short_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShortCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
			sql.append(" and lower(cus.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerName().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getAddress())) {
			sql.append(" and lower(cus.address) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getAddress().trim().toLowerCase()));
		}
		if (filter.getObjectType() != null) {
			sql.append(" and cus.channel_type_id in (select channel_type_id from channel_type where type = ? and object_type = ?) ");
			params.add(ChannelTypeType.CUSTOMER.getValue());
			params.add(filter.getObjectType());
		}

		sql.append(" and cus.shop_id in (  ");
		sql.append("  select sh.shop_id from shop sh where status = 1 start with sh.shop_id in (-1  ");
		for (Long value : filter.getLstShopId()) {
			sql.append("  , ? ");
			params.add(value);
		}
		sql.append(" ) connect by prior sh.shop_id = sh.parent_shop_id )  ");

		//Append Chuoi From and Where
		sql.append(" order by customerCode ");
		//Count total
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		final String[] fieldNames = new String[] { "id", "shortCode", "customerCode", "customerName", "address", "channelTypeName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}

	@Override
	public Customer getCustomerByShortCode(String code) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from CUSTOMER where short_code=? and status != ?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		Customer rs = repo.getEntityBySQL(Customer.class, sql, params);
		return rs;
	}

	@Override
	public boolean existsCode(Long cusID, Long shopid, String code) throws DataAccessException {
		String sql = "select count(1) count from CUSTOMER where short_code=? and shop_id = ? and CUSTOMER_id<>? ";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(shopid == null ? 0L : shopid);
		params.add(cusID == null ? 0L : cusID);
		return repo.countBySQL(sql, params) > 0;

	}

	@Override
	public List<DuplicatedCustomerVO> getDuplicatedCustomers(Long customerId) throws DataAccessException {
		if (customerId == null) {
			throw new IllegalArgumentException("customerId is null");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select c.*, s.shop_code shopCode, s.shop_name shopName ");
		sql.append(" 	, area_shop.shop_code areaShopCode, area_shop.shop_name areaShopName ");
		sql.append("     , region_shop.shop_code regionShopCode, region_shop.shop_name regionShopName ");
		sql.append(" from ( ");
		sql.append("     select customer_code customerCode, short_code shortCode, customer_name customername, address, status, lat, lng, shop_id, ");
		sql.append("    	case ");
		sql.append("    	when upper(address) = (select upper(address) from customer where customer_id = ?) then 1 ");
		params.add(customerId);
		sql.append("    	else null ");
		sql.append("    	end no, ");
		sql.append("    	case ");
		sql.append("        when lat is not null and lng is not null ");
		sql.append("        then ");
		sql.append("    	F_CALCULATE_GPS_DISTANCE(to_number(lat), to_number(lng),  ");
		sql.append("            		(select to_number(lat) from customer where customer_id = ?),  ");
		sql.append("                    (select to_number(lng) from customer where customer_id = ?)) ");
		params.add(customerId);
		params.add(customerId);
		sql.append("        else -9999 ");
		sql.append("        end distanceFromApprovingCustomer ");
		sql.append("     from customer ");
		sql.append("     where 1=1 ");
		sql.append("     and status in (0, 1) ");
		sql.append("     and customer_id <> ? ");
		params.add(customerId);
		sql.append(" ) c ");
		sql.append(" join shop s on c.shop_id = s.shop_id ");
		sql.append(" join shop area_shop on s.parent_shop_id= area_shop.shop_id ");
		sql.append(" join shop region_shop on area_shop.parent_shop_id = region_shop.shop_id ");
		sql.append(" where c.no = 1 ");
		sql.append(" or (c.distanceFromApprovingCustomer >= 0 and c.distanceFromApprovingCustomer <= (select to_number(value) from ap_param where ap_param_code = 'SYS_CHECK_DUPLICATE_CUSTOMER_DISTANCE' and status = 1)) ");
		sql.append(" order by c.no nulls last, c.distanceFromApprovingCustomer ");
		
		final String[] fieldNames = new String[] {
			"customerCode", "shortCode", "customerName", 
			"address", "status", "no", 
			"lat", "lng", "distanceFromApprovingCustomer",
			"shopCode", "shopName",
			"areaShopCode", "areaShopName",
			"regionShopCode", "regionShopName"
		};

		final Type[] fieldTypes = new Type[] {
			StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
			StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, 
			StandardBasicTypes.DOUBLE, StandardBasicTypes.DOUBLE, StandardBasicTypes.DOUBLE,
			StandardBasicTypes.STRING, StandardBasicTypes.STRING,
			StandardBasicTypes.STRING, StandardBasicTypes.STRING,
			StandardBasicTypes.STRING, StandardBasicTypes.STRING
		};
		
		return repo.getListByQueryAndScalar(DuplicatedCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<CustomerExportVO> getCustomerWithDynamicAttributeInfo(ParamsDAOGetListCustomer param) throws DataAccessException {
		if (param == null) {
			throw new IllegalArgumentException("search param is null.");
		}
		StringBuilder sql = new StringBuilder();
		sql.append("call TUANND.CUSTOMER_DYNAMIC_ATTRIBUTE(?, :shopId , :withInShops , :isGetInChildShop , :customerShortCode , :customerName , :mobiphone , :customerStatus , :customerAddress , :customerTypeCode , :customerFrequency , :sortField , :sortOrder)");
		
		List<Object> params = new ArrayList<Object>();
		int paramIndex = 2;
		params.add(paramIndex++);
		params.add(param.getShopId());
		params.add(java.sql.Types.NUMERIC);
		
		params.add(paramIndex++);
		String listShopIdConcat = StringUtility.getParamsIdFixBugTooLongParams(param.getStrShopId(), "c.shop_id");
		params.add(listShopIdConcat);
		params.add(java.sql.Types.VARCHAR);
		
		params.add(paramIndex++);
		params.add(param.isAllSubShop() ? 1 : 0);
		params.add(java.sql.Types.INTEGER);
		
		params.add(paramIndex++);
		params.add(StringUtility.isNullOrEmpty(param.getShortCode()) ? null : param.getShortCode().toUpperCase());
		params.add(java.sql.Types.VARCHAR);
		
		params.add(paramIndex++);
		params.add(StringUtility.isNullOrEmpty(param.getCustomerName()) ? null : Unicode2English.codau2khongdau(param.getCustomerName().toUpperCase()));
		params.add(java.sql.Types.VARCHAR);
		
		params.add(paramIndex++);
		params.add(param.getMobiPhone());
		params.add(java.sql.Types.VARCHAR);
		
		params.add(paramIndex++);
		params.add(param.getStatus() != null ? param.getStatus().getValue() : null);
		params.add(java.sql.Types.INTEGER);
		
		params.add(paramIndex++);
		params.add(StringUtility.isNullOrEmpty(param.getAddress()) ? null : param.getAddress().toUpperCase());
		params.add(java.sql.Types.VARCHAR);
		
		params.add(paramIndex++);
		params.add(param.getCustomerTypeCode());
		params.add(java.sql.Types.VARCHAR);
		
		params.add(paramIndex++);
		params.add(param.getTansuat());
		params.add(java.sql.Types.INTEGER);
		
		params.add(paramIndex++);
		params.add(param.getSort());
		params.add(java.sql.Types.VARCHAR);
		
		params.add(paramIndex++);
		params.add(param.getOrder());
		params.add(java.sql.Types.VARCHAR);
		
		return repo.getListByQueryDynamicFromPackage(CustomerExportVO.class, sql.toString(), params, null);
	}
	
	@Override
	public List<CustomerVO> getListCustomerBySaleStaffHasVisitPlanWithFirstSaleOrder(Long shopId, Long staffId, Integer cfIsCustomerWaitingToApproved,Date checkDate)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();		
		sql.append(" with saleOrder as (                ");
		sql.append("  	select so.*               ");
		sql.append("    from sale_order so             ");
		sql.append("    where so.order_source = ? and  so.staff_id = ?             ");/**Select saleorder from tablet*/
		params.add(SaleOrderSource.TABLET.getValue());
		params.add(staffId);
		sql.append("          and so.shop_id = ?       ");
		params.add(shopId);
		sql.append("          and so.order_date >= trunc(?) and so.order_date < trunc(?) + 1       ");
		params.add(checkDate);
		params.add(checkDate);
		sql.append("          and so.order_date = ( ");
		sql.append("  				select min(s.order_date)               ");
		sql.append("    			from sale_order s             ");
		sql.append("    			where  s.order_source = ? and s.staff_id = so.staff_id             ");
		params.add(SaleOrderSource.TABLET.getValue());
		sql.append("          			and s.shop_id = so.shop_id and s.customer_id = so.customer_id       ");
		sql.append("          			and s.order_date >= trunc(?) and s.order_date < trunc(?) + 1       ");
		params.add(checkDate);
		params.add(checkDate);
		sql.append(" 		)                ");
		sql.append(" )                ");
		sql.append(" select * from ( ");
		sql.append(" SELECT distinct (c.customer_id) AS id, ");
		sql.append(" c.customer_code AS customerCode, ");
		sql.append(" c.customer_name AS customerName, ");
		sql.append(" c.ADDRESS AS address, ");
		sql.append(" c.MOBIPHONE AS mobiphone, ");
		//sql.append(" c.short_code AS shortCode, ");
		//sql.append(" c.short_code || '-' || c.customer_name AS customerCodeName, ");
		//sql.append(" CASE when c.housenumber is null and c.street is null then to_nchar('') WHEN c.housenumber IS NULL and c.street IS not NULL THEN TO_NCHAR (c.street) when c.housenumber is not null and c.street is null then TO_NCHAR(c.housenumber) ELSE TO_NCHAR (c.housenumber || ', ' || c.street) END AS address, c.mobiphone as mobiphone, c.phone, ");
	
		// anhhpt
		//	sql.append(" NVL(floor(rsd.amount/1000), 0) as amount, ");
		/*sql.append(" rsd.amount as amount, ");*/
		sql.append(" so.total as amount, ");
		sql.append(" (CASE TO_CHAR((?),'D') ");
		params.add(checkDate);
		sql.append(" WHEN '2' THEN rc.SEQ2 ");
		sql.append(" WHEN '3' THEN rc.SEQ3 ");
		sql.append(" WHEN '4' THEN rc.SEQ4 ");
		sql.append(" WHEN '5' THEN rc.SEQ5 ");
		sql.append(" WHEN '6' THEN rc.SEQ6 ");
		sql.append(" WHEN '7' THEN rc.SEQ7 ");
		sql.append(" WHEN '1' THEN rc.SEQ8 ");
		sql.append(" ELSE NULL ");
		sql.append(" END ) AS ordinalVisit, al.object_type as objectType, al.start_time as startTime, al.end_time as endTime, c.lat, c.lng,al.is_or as isOr, ");
		sql.append(" (select count(customer_id) as count from action_log al ");
		sql.append(" where START_TIME >= TRUNC(?) ");
		params.add(checkDate);
		sql.append(" and START_TIME < TRUNC(?) + 1 ");
		params.add(checkDate);
		sql.append(" and (object_type=4) and customer_id = rc.customer_id and staff_id = vp.staff_id )AS count ");
		sql.append(" FROM customer c ");
		sql.append(" JOIN ROUTING_CUSTOMER rc ON rc.customer_id = c.customer_id ");
		sql.append(" join ROUTING r on r.ROUTING_ID = rc.ROUTING_ID and r.status in (0,1) ");
		sql.append(" join VISIT_PLAN vp on vp.ROUTING_ID = r.ROUTING_ID ");
		sql.append(" left join action_log al on vp.staff_id = al.staff_id ");
		sql.append(" and al.customer_id = rc.customer_id AND al.start_time >= TRUNC(?) ");
		params.add(checkDate);
		sql.append(" and al.start_time < TRUNC(?) + 1 ");
		params.add(checkDate);
		sql.append(" left join saleOrder so on so.shop_id = c.shop_id and so.staff_id = vp.staff_id and so.customer_id = c.customer_id             ");
		sql.append(" WHERE 1=1 ");
		/**
		 * lochp
		 * fix bug vẽ lộ trình của KH đã bị off
		 */
		if (cfIsCustomerWaitingToApproved == null){
			sql.append(" and c.status in (1, 0)  ");
		} else {
			if (cfIsCustomerWaitingToApproved <= 0){
				sql.append(" and c.status in (0, 1, 2)  ");
			} else {
				sql.append(" and c.status in (0, 1)  ");
			}
		}		
		sql.append(" AND vp.status in (0,1) AND rc.status in (0,1) ");
		sql.append(" and c.shop_id = ? and rc.shop_id= ? and r.shop_id = ? and vp.shop_id = ? and (al.shop_id is null or al.shop_id = ?)  ");
		params.add(shopId);
		params.add(shopId);
		params.add(shopId);
		params.add(shopId);
		params.add(shopId);
		sql.append(" AND( ( rc.WEEK1 IS NULL ");
		sql.append(" AND rc.WEEK2 IS NULL ");
		sql.append(" AND rc.WEEK3 IS NULL ");
		sql.append(" AND rc.WEEK4 IS NULL) ");
		sql.append(" OR ( ( MOD( ( ( floor((TRUNC(?,'dd') - TRUNC(rc.start_date,'dd'))/7)) + ( ");
		params.add(checkDate);
		sql.append(" CASE ");
		sql.append(" WHEN (mod(TRUNC(?,'dd')- TRUNC(rc.start_date,'dd'),7) > 0) ");
		params.add(checkDate);
		sql.append(" AND ( ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR(?,'D') = '1' ");
		params.add(checkDate);
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR(?, 'D')) ");
		params.add(checkDate);
		sql.append(" END ) < ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR( rc.start_date,'D') = '1' ");
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR( rc.start_date,'D')) ");
		sql.append(" END ) ) ");
		sql.append(" THEN 1 ");
		sql.append(" ELSE 0 ");
		sql.append(" END) ),4) + 1 ) = 1 ");
		sql.append(" AND week1 =1 ) ");
		sql.append(" OR ( ( MOD( ( ( floor((TRUNC(?,'dd') - TRUNC(rc.start_date,'dd'))/7)) + ( ");
		params.add(checkDate);
		sql.append(" CASE ");
		sql.append(" WHEN (mod(TRUNC(?,'dd')- TRUNC(rc.start_date,'dd'),7) > 0) ");
		params.add(checkDate);
		sql.append(" AND ( ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR(?,'D') = '1' ");
		params.add(checkDate);
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR(?, 'D')) ");
		params.add(checkDate);
		sql.append(" END ) < ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR( rc.start_date,'D') = '1' ");
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR( rc.start_date,'D')) ");
		sql.append(" END ) ) ");
		sql.append(" THEN 1 ");
		sql.append(" ELSE 0 ");
		sql.append(" END) ),4) + 1 ) = 2 ");
		sql.append(" AND week2 =1) ");
		sql.append(" OR ( ( MOD( ( ( floor((TRUNC(?,'dd') - TRUNC(rc.start_date,'dd'))/7)) + ( ");
		params.add(checkDate);
		sql.append(" CASE ");
		sql.append(" WHEN (mod(TRUNC(?,'dd')- TRUNC(rc.start_date,'dd'),7) > 0) ");
		params.add(checkDate);
		sql.append(" AND ( ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR(?,'D') = '1' ");
		params.add(checkDate);
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR(?, 'D')) ");
		params.add(checkDate);
		sql.append(" END ) < ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR( rc.start_date,'D') = '1' ");
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR( rc.start_date,'D')) ");
		sql.append(" END ) ) ");
		sql.append(" THEN 1 ");
		sql.append(" ELSE 0 ");
		sql.append(" END) ),4) + 1 ) = 3 ");
		sql.append(" AND week3 =1) ");
		sql.append(" OR ( ( MOD( ( ( floor((TRUNC(?,'dd') - TRUNC(rc.start_date,'dd'))/7)) + ( ");
		params.add(checkDate);
		sql.append(" CASE ");
		sql.append(" WHEN (mod(TRUNC(?,'dd')- TRUNC(rc.start_date,'dd'),7) > 0) ");
		params.add(checkDate);
		sql.append(" AND ( ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR(?,'D') = '1' ");
		params.add(checkDate);
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR(?, 'D')) ");
		params.add(checkDate);
		sql.append(" END ) < ( ");
		sql.append(" CASE ");
		sql.append(" WHEN TO_CHAR( rc.start_date,'D') = '1' ");
		sql.append(" THEN 8 ");
		sql.append(" ELSE TRUNC(TO_CHAR( rc.start_date,'D')) ");
		sql.append(" END ) ) ");
		sql.append(" THEN 1 ");
		sql.append(" ELSE 0 ");
		sql.append(" END) ),4) + 1 ) = 4 ");
		sql.append(" AND week4 =1) ) ");
		sql.append(" and vp.staff_id = ? ");
		params.add(staffId);
		sql.append(" and trunc(vp.from_date) <= trunc(?) ");
		sql.append(" and ( trunc(vp.to_date) >= trunc(?) or vp.to_date is null) ");
		sql.append(" and trunc(rc.start_date) <= trunc(?) ");
		sql.append(" and ( trunc(rc.end_date) >= trunc(?) or rc.end_date is null) ");
		params.add(checkDate);
		params.add(checkDate);
		params.add(checkDate);
		params.add(checkDate);
		sql.append(" AND ( (TO_CHAR((?),'D')=1 AND SUNDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =2 AND MONDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =3 AND TUESDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =4 AND WEDNESDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =5 AND THURSDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =6 AND FRIDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =7 AND SATURDAY =1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D')=1 AND SUNDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =2 AND MONDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =3 AND TUESDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =4 AND WEDNESDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =5 AND THURSDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =6 AND FRIDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" OR (TO_CHAR((?),'D') =7 AND SATURDAY =0 AND al.is_or=1) ");
		params.add(checkDate);
		sql.append(" ) ");
		sql.append(" order by al.start_time ) ");
		sql.append(" where objecttype is null or objecttype <> 4 ");
		sql.append("  ");
		
		final String[] fieldNames = new String[] {
				"id", //1
				"customerCode",//2
				"customerName",//3
				//"shortCode",//4
				//"customerCodeName",//5
				"address",//6				
				"mobiphone",//7
				//"phone",//8
				//"amountPlan",//9
				"amount",//10
				"ordinalVisit",//11
				"objectType",//12
				"startTime",
				"endTime",//13
				"lat",//14
				"lng",//15
				"isOr",//16
				"count"};//17
		final Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.STRING, //5
				//StandardBasicTypes.STRING, 		//6		
				//StandardBasicTypes.STRING,//7
				//StandardBasicTypes.STRING,//8
				//StandardBasicTypes.BIG_DECIMAL,//9
				StandardBasicTypes.BIG_DECIMAL,//10
				StandardBasicTypes.INTEGER, //11
				StandardBasicTypes.INTEGER, //12
				StandardBasicTypes.TIMESTAMP,//13 
				StandardBasicTypes.TIMESTAMP,//13 
				StandardBasicTypes.STRING, //14
				StandardBasicTypes.STRING,//15
				StandardBasicTypes.INTEGER,//16
				StandardBasicTypes.INTEGER};//17
		List<CustomerVO> vos = repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
//		List<CustomerVO> vosEx = this.getListCustomerBySaleStaffNotVisit(staffId);
//		if(vosEx!=null && vosEx.size()>0){
//			if(vos==null){
//				vos = new ArrayList<CustomerVO>();
//			}
//			for(CustomerVO vo:vosEx){
//				vos.add(vo);
//			}
//		}
//		//Kiem tra thong tin khach hang ghe tham co don hang hay ko?
//		for(CustomerVO ctVO:vos){
//			ctVO.setCount(this.countCustomerInActionLog(staffId, ctVO.getId()));
//		}		
		return vos;
	}

	@Override
	public List<CustomerVO> getCustomerRoutingsInfo(List<String> customerCodes) throws DataAccessException {
		if (customerCodes == null || customerCodes.isEmpty()) {
			throw new IllegalArgumentException("customerCodes is null or empty");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT c.customerid                        id, ");
		sql.append("       c.customercode, ");
		sql.append("       c.routes, ");
		sql.append("       (SELECT staff_name ");
		sql.append("        FROM   staff ");
		sql.append("        WHERE  staff_code = c.create_user) createUserName ");
		sql.append("FROM   (SELECT rc.customer_id                           customerId, ");
		sql.append("               c.customer_code                          customerCode, ");
		sql.append("               Upper(c.create_user)                     create_user, ");
		sql.append("               Listagg(r.routing_code, ', ') ");
		sql.append("                 within GROUP (ORDER BY r.routing_code) routes ");
		sql.append("        FROM   (SELECT * ");
		sql.append("                FROM   customer c ");
		sql.append(" 			where c.customer_code in ('-1' ");
		for (String customerCode : customerCodes) {
			sql.append(",?");
			params.add(customerCode);
		}
		sql.append(" 			) ");
		sql.append(" 	) c ");
		sql.append("               left join routing_customer rc ");
		sql.append("                      ON rc.customer_id = c.customer_id ");
		sql.append("                         AND rc.status = 1 ");
		sql.append("                         AND rc.start_date <= SYSDATE ");
		sql.append("                         AND ( rc.end_date IS NULL ");
		sql.append("                                OR rc.end_date + 1 > SYSDATE ) ");
		sql.append("               left join routing r ");
		sql.append("                      ON rc.routing_id = r.routing_id ");
		sql.append("                         AND r.status = 1 ");
		sql.append("        GROUP  BY rc.customer_id, ");
		sql.append("                  c.customer_code, ");
		sql.append("                  c.create_user) c ");
		
		final String[] fieldNames = new String[] {
			"id", "customerCode", "routes", "createUserName"
		};
		final Type[] fieldTypes = new Type[] {
			StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING
		};
		return repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	
	/*** BEGIN FEEDBACK VUONGMQ */
	@Override
	public List<CustomerVO> getCustomerVOFeedback(CustomerFilter<CustomerVO> filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null or empty");
		}
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (filter.getStaffRootId() == null || filter.getRoleId() == null || filter.getShopRootId() == null) {
			throw new IllegalArgumentException("priviledge data is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select cu.customer_id as id, ");
		sql.append(" cu.short_code as shortCode, ");
		sql.append(" cu.customer_name as customerName, ");
		sql.append(" cu.address as address, ");
		sql.append(" sh.shop_id as shopId, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName ");
		sql.append(" from customer cu ");
		sql.append(" join shop sh on sh.shop_id = cu.shop_id ");
		sql.append(" join table (f_get_list_child_shop_inherit(?, sysdate, ?, ?)) tmp on tmp.number_key_id = cu.shop_id ");
		params.add(filter.getStaffRootId());
		params.add(filter.getRoleId());
		params.add(filter.getShopRootId());
		sql.append(" where cu.status = 1 ");
		if (filter.getShopId() != null) {
			sql.append(" and cu.shop_id in ( ");
			sql.append(" select shop_id ");
			sql.append(" from shop ");
			sql.append(" start with shop_id = ? ");
			sql.append(" connect by prior shop_id = parent_shop_id ");
			sql.append(" ) ");
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and (cu.short_code like ? escape '/' ");
			sql.append(" or lower(cu.name_text) like ? escape '/' ");
			sql.append(" or unicode2english(cu.address) like ? escape '/' ");
			sql.append(" ) ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCustomerCode()).toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCustomerCode()).toLowerCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCustomerCode()).toLowerCase()));
		}
		if (filter.getLstNotInId() != null && filter.getLstNotInId().size() > 0) {
			sql.append(" and cu.customer_id not in (-1 ");
			for (int i = 0, sz = filter.getLstNotInId().size(); i < sz; i++) {
				sql.append(", ? ");
				params.add(filter.getLstNotInId().get(i));
			}
			sql.append(" ) ");
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		sql.append(" order by shortCode ");
		
		final String[] fieldNames = new String[] {
			"id",  //1
			"shortCode", //2 
			"customerName", //3
			"address",//4
			"shopId", //5
			"shopCode",  //6
			"shopName" //7
		};
		final Type[] fieldTypes = new Type[] {
			StandardBasicTypes.LONG, //1 
			StandardBasicTypes.STRING, //2
			StandardBasicTypes.STRING, //3
			StandardBasicTypes.STRING, //4
			StandardBasicTypes.LONG,  //5
			StandardBasicTypes.STRING,  //6
			StandardBasicTypes.STRING,  //7
		};
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
		return repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<CustomerVO> getCustomerVORoutingFeedback(CustomerFilter<CustomerVO> filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null or empty");
		}
		if (filter.getStaffRootId() == null || filter.getRoleId() == null || filter.getShopRootId() == null) {
			throw new IllegalArgumentException("priviledge data is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select cu.customer_id as id, ");
		sql.append(" cu.short_code as shortCode, ");
		sql.append(" cu.customer_name as customerName, ");
		sql.append(" cu.address as address, ");
		sql.append(" sh.shop_id as shopId, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName ");
		sql.append(" from visit_plan vp ");
		sql.append(" join routing r on r.routing_id = vp.routing_id and r.status = 1 ");
		sql.append(" join routing_customer rc on rc.routing_id = vp.routing_id and rc.status = 1 ");
		sql.append(" join customer cu on cu.customer_id = rc.customer_id and cu.status = 1");
		sql.append(" join shop sh on sh.shop_id = cu.shop_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and vp.from_date < trunc(sysdate) + 1 ");
		sql.append(" and (vp.to_date >= trunc(sysdate) or vp.to_date is null) ");
		sql.append(" and vp.status = 1 ");
		if (filter.getStaffId() != null) {
			sql.append(" and vp.staff_id = ? ");
			params.add(filter.getStaffId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and (cu.short_code like ? escape '/' ");
			sql.append(" or lower(cu.name_text) like ? escape '/' ");
			sql.append(" or unicode2english(cu.address) like ? escape '/' ");
			sql.append(" ) ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCustomerCode()).toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCustomerCode()).toLowerCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCustomerCode()).toLowerCase()));
		}
		if (filter.getLstNotInId() != null && filter.getLstNotInId().size() > 0) {
			sql.append(" and cu.customer_id not in (-1 ");
			for (int i = 0, sz = filter.getLstNotInId().size(); i < sz; i++) {
				sql.append(", ? ");
				params.add(filter.getLstNotInId().get(i));
			}
			sql.append(" ) ");
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		sql.append(" order by shortCode ");
		
		final String[] fieldNames = new String[] {
			"id",  //1
			"shortCode", //2 
			"customerName", //3
			"address",//4
			"shopId", //5
			"shopCode",  //6
			"shopName" //7
		};
		final Type[] fieldTypes = new Type[] {
			StandardBasicTypes.LONG, //1 
			StandardBasicTypes.STRING, //2
			StandardBasicTypes.STRING, //3
			StandardBasicTypes.STRING, //4
			StandardBasicTypes.LONG,  //5
			StandardBasicTypes.STRING,  //6
			StandardBasicTypes.STRING,  //7
		};
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
		return repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		
	}
	/*** END FEEDBACK VUONGMQ */

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.CustomerDAO#getListCustomerFullVOByFilter(ths.dms.core.entities.filter.CustomerFilter)
	 */
	@Override
	public List<CustomerVO> getListCustomerFullVOByFilter(CustomerFilter<CustomerVO> filter) throws DataAccessException {
		if (filter.getLstShopId() == null || filter.getLstShopId().size() == 0) {
			throw new IllegalArgumentException("filter.getLstShopId()");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select distinct  ");
		sql.append(" cus.customer_id as id  ");
		sql.append(" , cus.short_code as shortCode  ");
		sql.append(" , cus.customer_code as customerCode ");
		sql.append(" , cus.customer_name as customerName  ");
		sql.append(" , cus.address as address ");
		sql.append(" , (select channel_type_name from channel_type where channel_type_id = cus.channel_type_id) as channelTypeName  ");
		sql.append(" , cus.phone ");
		sql.append(" , cus.mobiphone ");
		sql.append(" , cus.housenumber ");
		sql.append(" , cus.street ");
		sql.append(" , sh.shop_code as shopCode ");
		sql.append(" , sh.shop_name as shopName ");
		sql.append(" , vung.shop_name as shopVungName ");
		sql.append(" , mien.shop_name as shopMienName ");
		sql.append(" , kenh.shop_name as shopKenhName ");
		sql.append(" , vung.shop_code as shopVungCode ");
		sql.append(" , mien.shop_code as shopMienCode ");
		sql.append(" , kenh.shop_code as shopKenhCode ");
		sql.append(" from customer cus ");
		sql.append(" join shop sh on cus.shop_id = sh.shop_id ");
		sql.append(" join shop vung on vung.shop_id = sh.parent_shop_id ");
		sql.append(" left join shop mien on mien.shop_id = vung.parent_shop_id ");
		sql.append(" left join shop kenh on kenh.shop_id = mien.parent_shop_id ");
		sql.append(" where 1 = 1  ");
		if (filter.getIsCheckStatus() == null || filter.getIsCheckStatus()) {
			if (filter.getStatus() != null) {
				sql.append(" and cus.status = ?  ");
				params.add(filter.getStatus());
			} else {
				sql.append(" and cus.status != ? ");
				params.add(ActiveType.DELETED.getValue());
			}
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and cus.short_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShortCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
			sql.append(" and lower(cus.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getCustomerName().trim()).toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())){
			sql.append(" and sh.shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and lower(sh.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getShopName().trim()).toLowerCase()));
		}
		if (filter.getLstShopId() != null && !filter.getLstShopId().isEmpty()) {
			sql.append(" and cus.shop_id in (  ");
			sql.append(" select s1.shop_id from shop s1 ");
			if (filter.getIsCheckStatus() == null || filter.getIsCheckStatus()) {
				sql.append(" where s1.status = ? ");
				params.add(ActiveType.RUNNING.getValue());
			}
			sql.append(" start with s1.shop_id in (?  ");
			params.add(filter.getLstShopId().get(0));
			for (int i = 1, size = filter.getLstShopId().size(); i < size; i++) {
				sql.append("  , ? ");
				params.add(filter.getLstShopId().get(i));
			}
			sql.append(" ) connect by prior s1.shop_id = s1.parent_shop_id )  ");
		}

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from ( ");
		countSql.append(sql);
		countSql.append(" ) ");
		
		sql.append(" order by cus.customer_code ");
		String[] fieldNames = { "id", //1
								"shortCode", //2 
								"customerCode", //3
								"customerName", //4
								"address", //5
								"channelTypeName", //6 
								"phone", //7
								"mobiphone", //8 
								"housenumber", //9
								"street", //10
								"shopCode", //11
								"shopName", //12
								"shopVungName",
								"shopMienName", //13
								"shopKenhName", //14
								"shopVungCode",
								"shopMienCode", //13
								"shopKenhCode" //14
								};

		Type[] fieldTypes = { 	StandardBasicTypes.LONG, //1
								StandardBasicTypes.STRING, //2
								StandardBasicTypes.STRING, //3
								StandardBasicTypes.STRING, //4
								StandardBasicTypes.STRING, //5
								StandardBasicTypes.STRING, //6
								StandardBasicTypes.STRING, //7
								StandardBasicTypes.STRING, //8
								StandardBasicTypes.STRING, //9
								StandardBasicTypes.STRING, //10
								StandardBasicTypes.STRING, //11
								StandardBasicTypes.STRING, //12
								StandardBasicTypes.STRING,
								StandardBasicTypes.STRING, //13
								StandardBasicTypes.STRING, //14
								StandardBasicTypes.STRING,
								StandardBasicTypes.STRING, //13
								StandardBasicTypes.STRING //14
								};
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(CustomerVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	
}
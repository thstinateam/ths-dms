package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.StaffPrsmFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.DataAccessException;

/**
 * StaffDAO New
 * 
 * @author hunglm16
 * @since September 28,2014
 * **/
public interface StaffDAONew {

	/**
	 * Tao moi Staff
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * */
	Staff createStaff(Staff staff, LogInfoVO logInfo) throws DataAccessException;

	/**
	 * Xoa Staff
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * */
	void deleteStaff(Staff staff, LogInfoVO logInfo) throws DataAccessException;

	/**
	 * Lay Staff voi ma Id
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * */
	Staff getStaffById(long id) throws DataAccessException;

	/**
	 * Cap nhat thong tin Staff
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * */
	void updateStaff(Staff staff, LogInfoVO logInfo) throws DataAccessException;

	/**
	 * Lay Staff voi ma code
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * */
	Staff getStaffByCode(String code) throws DataAccessException;

	/**
	 * Login by Staff
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * */
	Staff getStaffLogin(String code, String password) throws DataAccessException;

	/**
	 * search Staff by Filter
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * @description Tim kiem dang Like '%%', in ()
	 * */
	List<StaffVO> searchListStaffVOByFilter(StaffPrsmFilter<StaffVO> filter) throws DataAccessException;
	
	/**
	 * search Staff by Filter
	 * 
	 * @author longnh15
	 * @since 25/05/2015
	 * @description Search nhan vien thuoc shop
	 * */
	List<StaffVO> searchListStaffVOByShop(StaffPrsmFilter<StaffVO> filter) throws DataAccessException;
	

	/**
	 * get Staff by Filter
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * @description Tim kiem dang Like, =
	 * */
	List<StaffVO> getListStaffVOByFilter(StaffPrsmFilter<StaffVO> filter) throws DataAccessException;

	/**
	 * get Staff by Filter
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * @description Tim kiem dang Like, =; ShopRoot
	 * */
	List<StaffVO> getListStaffVOByShopId(BasicFilter<StaffVO> filter) throws DataAccessException;

	/**
	 * get Staff by Filter
	 * 
	 * @author hunglm16
	 * @since September 28,2014
	 * @description Tim kiem dang Like, =; ShopRoot
	 * */
	List<StaffVO> getListStaffVOAndChilrentByShopId(BasicFilter<StaffVO> filter) throws DataAccessException;

	/**
	 * get Staff by Filter
	 * 
	 * @author hunglm16
	 * @since October 1, 2014
	 * @description Tim kiem dang Like, =; ShopRoot
	 * */
	Staff getStaffByFilterWithCMS(BasicFilter<Staff> filter) throws DataAccessException;

	/**
	 * Kiem tra tinh dung dan du lieu voi Staff
	 * 
	 * @author hunglm16
	 * @since October 6 ,2014
	 * */
	boolean checkStaffInCMSByFilterWithCMS(BasicFilter<BasicVO> filter) throws DataAccessException;

	/**
	 * Lay doi tuong la sanh sach nhan vien theo phan quyen CMS
	 * 
	 * @author hunglm16
	 * @since October 6 ,2014
	 * 
	 * @param filter
	 *            .getInheritUserPriv(), filter.getLstShopId(),
	 *            filter.getIsLstChildStaffRoot(): Nhung tham so bat buoc
	 * */
	List<StaffVO> getListStaffVOFullChilrentByCMS(StaffPrsmFilter<StaffVO> filter) throws DataAccessException;
	/**
	 * Kiem tra cha con trong parent staff map
	 * 
	 * @author hunglm16
	 * @since October 23 ,2014
	 * */
	boolean checkParentChildrentByDoubleStaffId(Long parentStaffId, Long childStaffId) throws DataAccessException;
	/**
	 * Lay danh sach giam sat theo NVBH voi phan quyen CMS
	 * 
	 * @author hunglm16
	 * @since October 25 ,2014
	 * */
	List<StaffVO> getListGSByStaffRootWithNVBH(StaffPrsmFilter<StaffVO> filter) throws DataAccessException;

	List<StaffVO> getListGSByStaffRootWithInParentStaffMap(StaffPrsmFilter<StaffVO> filter) throws DataAccessException;
	
	/**
	 * Lay danh sach giam sat Kenh KA
	 * 
	 * @author tamvnm
	 * @since 07/07/2015
	 * */
	List<StaffVO> getListGSByStaffRootKAMT(StaffPrsmFilter<StaffVO> filter) throws DataAccessException;

	/**
	 * Xu ly getListGSByFilter
	 * @author vuongmq
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @since Feb 24, 2016
	*/
	List<StaffVO> getListGSByFilter(StaffPrsmFilter<StaffVO> filter) throws DataAccessException;
}
package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.CycleCountResult;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.CycleCountResultVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.core.entities.vo.rpt.RptCycleCountDiff3VO;
import ths.dms.core.exceptions.DataAccessException;

public interface CycleCountResultDAO {

	void deleteCycleCountResult(CycleCountResult cycleCountResult) throws DataAccessException;

	CycleCountResult getCycleCountResultById(long id) throws DataAccessException;

	Boolean checkIfRecordExist(long cycleCountMapProductId, String lotCode, Long id)
			throws DataAccessException;

	List<CycleCountResult> getListCycleCount(KPaging<CycleCountResult> kPaging,
			Long cycleCountMapProductId) throws DataAccessException;

	CycleCountResult createCycleCountResult(CycleCountResult cycleCountResult) throws DataAccessException;
	
	void createListCycleCountResult(List<CycleCountResult> lstCycleCountResult) throws DataAccessException;

	void updateCycleCountResult(CycleCountResult cycleCountResult,
			Boolean isUpdateQuantityBfrCount) throws DataAccessException;

	void updateListCycleCountResult(
			List<CycleCountResultVO> lstCycleCountResultVO)
			throws DataAccessException;

	CycleCountResult getCycleCountResult(Long cycleCountId, Long productId,
			String lot) throws DataAccessException;

	List<RptCycleCountDiff3VO> getListRptCycleCountDiff3VO(Long shopId,
			Date fromDate, Date toDate, String cycleCountCode)
			throws DataAccessException;
	
	List<RptCycleCountDiff3VO> getListRptCycleCountDiff3VO(long cycleCountId) throws DataAccessException;

	List<CycleCountResult> getListCycleCountResultEx(
			KPaging<CycleCountResult> kPaging, Long cycleCountMapProductId,
			Long ownerId, StockObjectType ownerType, Long productId)
			throws DataAccessException;

	void createListCycleCountResult(List<CycleCountResult> lstCycleCountResult,LogInfoVO log, Boolean isNewProduct) throws DataAccessException;

	void deleteListCycleCountResult(List<CycleCountResult> lstCycleCountResult,LogInfoVO log) throws DataAccessException;

}

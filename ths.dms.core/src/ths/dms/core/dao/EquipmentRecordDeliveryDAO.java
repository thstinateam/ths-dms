package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.EquipDeliveryRecDtl;
import ths.dms.core.entities.EquipDeliveryRecord;
import ths.dms.core.entities.EquipStockTotal;
import ths.dms.core.entities.EquipStockTransFormDtl;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.EquipmentRecordDeliveryVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.DataAccessException;
/**
 * EquipmentRecordDeliveryDAO
 * 
 * @author nhutnn
 * @since 15/12/2014
 * @description: Lop DAO quan ly bien ban giao nhan thiet bi
 */
public interface EquipmentRecordDeliveryDAO {
	
	/**
	 * Them moi bien ban
	 * @author nhutnn
	 * @since 15/12/2014
	 * @param equipDeliveryRecord
	 * @throws DataAccessException
	 */
	EquipDeliveryRecord createEquipmentRecordDelivery(EquipDeliveryRecord equipDeliveryRecord) throws DataAccessException;
	
	/**
	 * Chinh sua bien ban 
	 * @author nhutnn
	 * @since 15/12/2014
	 * @param equipDeliveryRecord
	 * @return
	 * @throws DataAccessException
	 */
	void updateEquipmentRecordDelivery(EquipDeliveryRecord equipDeliveryRecord) throws DataAccessException;
		
	/**
	 * Xoa bien ban
	 * @author nhutnn
	 * @since 15/12/2014
	 * @param idEquipDeliveryRecord
	 * @throws DataAccessException
	 */
	//void deleteEquipmentRecordDelivery(Long idEquipDeliveryRecord) throws DataAccessException;
	
	/**
	 * Them moi thiet bi vao bien ban
	 * @author nhutnn
	 * @since 15/12/2014
	 * @param equipDeliveryRecDtl
	 * @throws DataAccessException
	 */
	EquipDeliveryRecDtl createEquipmentRecordDeliveryDetail(EquipDeliveryRecDtl equipDeliveryRecDtl) throws DataAccessException;
	
	/**
	 * Xoa thiet bi trong bien ban 
	 * @author nhutnn
	 * @since 15/12/2014
	 * @param idEquipDeliveryRecDtl
	 * @return
	 * @throws DataAccessException
	 */
	void deleteEquipmentRecordDeliveryDetail(EquipDeliveryRecDtl equipDeliveryRecDtl) throws DataAccessException;
	
	/**
	 * Lay danh sach bien ban giao nhan
	 * @author nhutnn
	 * @since 15/12/2014
	 * @param equipmentFilter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipmentRecordDeliveryVO> getListEquipmentRecordDeliveryVOByFilter(EquipmentFilter<EquipmentRecordDeliveryVO> filter) throws DataAccessException;
	
	/**
	 * Lay so luong bien ban de tao ma bien ban
	 * @author nhutnn
	 * @since 17/12/2014
	 * @return
	 * @throws DataAccessException
	 */
	String getCodeRecordDelivery() throws DataAccessException;
	/**
	 * Lay so luong bien ban de tao ma bien ban
	 * @author tamvnm
	 * @since 15/05/2015
	 * @return
	 * @throws DataAccessException
	 */
	String getCodeEviction() throws DataAccessException;
	
	/**
	 * Lay bien ban VO
	 * @author nhutnn
	 * @since 18/12/2014
	 * @param idRecord
	 * @return
	 * @throws DataAccessException
	 */
	EquipmentRecordDeliveryVO getEquipmentRecordDeliveryVOById(Long idRecord) throws DataAccessException;
	
	/**
	 * Lay bien ban
	 * @author nhutnn
	 * @since 17/12/2014
	 * @param idRecord
	 * @return
	 * @throws DataAccessException
	 */
	EquipDeliveryRecord getEquipDeliveryRecordById(Long idRecord) throws DataAccessException;
	
	/**
	 * Lay danh sach chi tiet bien ban
	 * @author nhutnn
	 * @since 18/12/2014
	 * @param idRecord
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipDeliveryRecDtl> getListEquipDeliveryRecDtlByEquipDeliveryRecId(Long idRecord) throws DataAccessException;
	
	/**
	 * Lay chi tiet bien ban
	 * @author nhutnn
	 * @since 22/12/2014
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	EquipDeliveryRecDtl getEquipDeliveryRecDtlByFilter(EquipmentFilter<EquipDeliveryRecDtl> filter) throws DataAccessException;
	/**
	 * Lay danh sach chi tiet bien ban chuyen kho
	 * @author hoanv25
	 * @since 25/12/2014
	 * @param id
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipStockTransFormDtl> getListEquipStockTransFormDtlById(Long id)throws DataAccessException;
	
	/**
	 * Lay danh sach cac bien ban huy, de bien ban de nghi muon xuat lai hop dong giao nhan
	 * @author nhutnn
	 * @since 10/07/2015
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<EquipmentRecordDeliveryVO> getListRecordDeliveryVOForEquipLendByFilter(EquipmentFilter<EquipmentRecordDeliveryVO> filter) throws DataAccessException;

	/**
	 * Lay giam sat trong bien ban giao nhan gan nhat
	 * 
	 * @author hunglm16
	 * @param [equipId]: id Thiet bi
	 * @param [customerId]: id Khach Hang
	 * @since August 19, 2015
	 * */
	Staff getStaffInEquipDelivRecordApproved(Long equipId, Long customerId) throws DataAccessException;
}

package ths.dms.core.dao;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.RptStockTotalDay;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class RptStockTotalDayDAOImpl implements RptStockTotalDayDAO {

	@Autowired
	private IRepository repo;

	@Override
	public RptStockTotalDay createRptStockTotalDay(RptStockTotalDay rptStockTotalDay) throws DataAccessException {
		if (rptStockTotalDay == null) {
			throw new IllegalArgumentException("rptStockTotalDay");
		}
		repo.create(rptStockTotalDay);
		return repo.getEntityById(RptStockTotalDay.class, rptStockTotalDay.getId());
	}

	@Override
	public void deleteRptStockTotalDay(RptStockTotalDay rptStockTotalDay) throws DataAccessException {
		if (rptStockTotalDay == null) {
			throw new IllegalArgumentException("rptStockTotalDay");
		}
		repo.delete(rptStockTotalDay);
	}

	@Override
	public RptStockTotalDay getRptStockTotalDayById(long id) throws DataAccessException {
		return repo.getEntityById(RptStockTotalDay.class, id);
	}
	
	@Override
	public void updateRptStockTotalDay(RptStockTotalDay rptStockTotalDay) throws DataAccessException {
		if (rptStockTotalDay == null) {
			throw new IllegalArgumentException("rptStockTotalDay");
		}
		repo.update(rptStockTotalDay);
	}
}

package ths.dms.core.dao;


import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.CompoundSaleOrder;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class CompoundSaleOrderDAOImpl implements CompoundSaleOrderDAO {
	@Autowired
	private IRepository repo;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public CompoundSaleOrder createCompoundSaleOrder(CompoundSaleOrder cso) throws DataAccessException {
		if (cso == null) {
			throw new IllegalArgumentException("compoundSaleOrder");
		}
		repo.create(cso);
		return repo.getEntityById(CompoundSaleOrder.class, cso.getId());
	}

	@Override
	public void deleteCompoundSaleOrder(CompoundSaleOrder cso) throws DataAccessException {
		if (cso == null) {
			throw new IllegalArgumentException("compoundSaleOrder");
		}
		repo.delete(cso);
	}

	@Override
	public CompoundSaleOrder getCompoundSaleOrderById(long id) throws DataAccessException {
		return repo.getEntityById(CompoundSaleOrder.class, id);
	}
	
	@Override
	public void updateCompoundSaleOrder(CompoundSaleOrder cso) throws DataAccessException {
		if (cso == null) {
			throw new IllegalArgumentException("compoundSaleOrder");
		}
		repo.update(cso);
	}
}

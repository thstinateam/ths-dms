package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.FocusChannelMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface FocusChannelMapDAO {

	FocusChannelMap createFocusChannelMap(FocusChannelMap focusChannelMap, LogInfoVO logInfo) throws DataAccessException;

	void deleteFocusChannelMap(FocusChannelMap focusChannelMap, LogInfoVO logInfo) throws DataAccessException;

	void updateFocusChannelMap(FocusChannelMap focusChannelMap, LogInfoVO logInfo) throws DataAccessException;
	
	FocusChannelMap getFocusChannelMapById(long id) throws DataAccessException;

	Boolean isUsingByOthers(long focusChannelMapId) throws DataAccessException;

	Boolean checkIfRecordExist(long focusProgramId, String saleTypeCode,
			Date fromDate, Date toDate, Long id) throws DataAccessException;

	FocusChannelMap getFocusChannelMapByProgramIdAndSaleTypeCode(
			long focusProgramId, String saleTypeCode)
			throws DataAccessException;

	List<FocusChannelMap> getListFocusChannelMapByFocusProgramId(
			KPaging<FocusChannelMap> kPaging, Long focusProgramId,
			String saleTypeCode, ActiveType status) throws DataAccessException;
	
	Boolean checkIfSaleTypeExistProduct(long focusProgramId, String saleTypeCode) throws DataAccessException;

	FocusChannelMap getFocusChannelMapByCode(String code) throws DataAccessException;
}

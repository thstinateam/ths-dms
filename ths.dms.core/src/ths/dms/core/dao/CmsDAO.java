package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.enumtype.CmsFilter;
import ths.dms.core.entities.vo.CmsVO;
import ths.dms.core.entities.vo.RoleVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.exceptions.DataAccessException;

public interface CmsDAO {
	List<CmsVO> getListUrl(CmsFilter filter) throws DataAccessException;
	
	List<ShopVO> getListOrgAccess(CmsFilter filter) throws DataAccessException;
	
	List<CmsVO> getListShopInRoleByUser(CmsFilter filter) throws DataAccessException;
	
	List<CmsVO> getListRoleByUser(CmsFilter filter) throws DataAccessException;
	
	List<ShopVO> getListShopByRole(CmsFilter filter) throws DataAccessException;
	
	CmsVO getApplicationVOByCode(String code) throws DataAccessException;
	
	List<CmsVO> getListFormControlByRole(CmsFilter filter) throws DataAccessException;
	
	List<ShopVO> getListShopByListParentId(CmsFilter filter) throws DataAccessException;
	
	List<ShopVO> getFullShopInOrgAccessByFilter(CmsFilter filter) throws DataAccessException;
	
	List<ShopVO> getListShopInOrgAccess(CmsFilter filter) throws DataAccessException;
	/**
	 * Lay danh sach nhan thoa man quyen du lieu theo User ID <Inherit User Priv>
	 * 
	 * @author hunglm16
	 * @since August 21,2014
	 * @return List ShopVO
	 * @description Ham lay tat ca danh sach tinh ca con chau cua no voi nhieu tham so 
	 * */
	List<StaffVO> getListStaffByUserWithRole(CmsFilter filter) throws DataAccessException;
	/**
	 * Lay danh sach nhan thoa man quyen du lieu theo User ID <Inherit User Priv>
	 * 
	 * @author hunglm16
	 * @since September 30,2014
	 * @return List Form
	 * @description Ham lay tat ca danh sach man hinh thuoc WEB 
	 * */
	List<CmsVO> getListFormInRoleByFilter(CmsFilter filter) throws DataAccessException;
	/**
	 * Lay danh sach nhan thoa man quyen du lieu theo User ID <Inherit User Priv>
	 * Tao du lieu cay bao cao
	 * 
	 * @author hunglm16
	 * @since September 30,2014
	 * @return List Form
	 * @description Ham lay tat ca danh sach man hinh thuoc WEB 
	 * */
	List<CmsVO> getListFormInRoleForTreeByFilter(CmsFilter filter) throws DataAccessException;

	/**
	 * Kiem tra tinh hop le cua Shop
	 * 
	 * @param shop_id - Đon vi tuong tac; Khong duoc trong
	 * @param staffRootId - Lay tu currentUser trong session co the hieu la Inherit User; truogn nay co the de trong
	 * @param shopRootId -  Lay tu currentUser trong session co the hieu la Don vi dang chon de tuong tac voi he thong; truogn nay co the de trong
	 * @description staffRootId & shopRootId khonphai co it nhat mot truong co gia tri
	 * @return 1 - Ton tai, 0 - Khong ton tai
	 * 
	 * @author hunglm16
	 * @since March 25,2015
	 * */
	int checkShopInCMSByFilter(CmsFilter filter) throws DataAccessException;

	List<RoleVO> getListOwnerRoleShopVOByStaff(Long staffId)
			throws DataAccessException;

	List<ShopVO> getListOwnerShopVOByRole(Long roleId, Long staffId)
			throws DataAccessException;

	List<ShopVO> getListShopVOByStaffVO(CmsFilter filter)
			throws DataAccessException;

	List<CmsVO> getListChildStaffInherit(CmsFilter filter)
			throws DataAccessException;

	List<CmsVO> getListChildShopInherit(CmsFilter filter)
			throws DataAccessException;

	List<ShopVO> getFullShopOwnerByFilter(CmsFilter filter)
			throws DataAccessException;

	/**
	 * Lay man hinh theo URL
	 * 
	 * @author hunglm16
	 * @param url
	 * @param permissionType
	 * @param roleId
	 * @param userId
	 * @param inheritStaffId
	 * @return
	 * @throws DataAccessException
	 * @since August 26, 2015
	 */
	CmsVO getFormCmsVoByUrl(String url, Integer permissionType, Long roleId, Long userId, Long inheritStaffId) throws DataAccessException;
}

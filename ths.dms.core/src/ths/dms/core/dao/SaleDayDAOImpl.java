package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SaleDays;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.dao.repo.IRepository;

public class SaleDayDAOImpl implements SaleDayDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Autowired
	CommonDAO commonDAO;

	@Override
	public SaleDays createSaleDay(SaleDays saleDay, LogInfoVO logInfo)
			throws DataAccessException {
		if (saleDay == null) {
			throw new IllegalArgumentException("saleDay");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		//saleDay.setCreateUser(logInfo.getStaffCode());
		//saleDay.setCreateDate(this.commonDAO.getSysDate());
		repo.create(saleDay);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, saleDay, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(SaleDays.class, saleDay.getId());
	}

	@Override
	public void deleteSaleDay(SaleDays saleDay, LogInfoVO logInfo)
			throws DataAccessException {
		if (saleDay == null) {
			throw new IllegalArgumentException("saleDay");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(saleDay);
		try {
			actionGeneralLogDAO.createActionGeneralLog(saleDay, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public SaleDays getSaleDayById(long id) throws DataAccessException {
		return repo.getEntityById(SaleDays.class, id);
	}

	@Override
	public void updateSaleDay(SaleDays saleDay, LogInfoVO logInfo)
			throws DataAccessException {
		if (saleDay == null) {
			throw new IllegalArgumentException("saleDay");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		SaleDays temp = this.getSaleDayById(saleDay.getId());
		repo.update(saleDay);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, saleDay, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public List<SaleDays> getListSaleDay(KPaging<SaleDays> kPaging,
			Integer fromYear, Integer toYear, ActiveType status)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from SALE_DAY where 1 = 1");
		if (fromYear != null) {
			sql.append(" and year >= ?");
			params.add(fromYear);
		}
		if (toYear != null) {
			sql.append(" and year <= ?");
			params.add(toYear);
		}
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		}
		sql.append(" order by sale_day_id desc");
		if (kPaging == null)
			return repo.getListBySQL(SaleDays.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(SaleDays.class, sql.toString(),
					params, kPaging);
	}

	@Override
	public Boolean isUsingByOthers(long saleDayId) throws DataAccessException {
		return false;
	}

	@Override
	public SaleDays getSaleDayByYear(Integer year) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from SALE_DAY where 1 = 1 ");
		if (year != null) {
			sql.append(" and year = ?");
			params.add(year);
		}
		return repo.getFirstBySQL(SaleDays.class, sql.toString(), params);
	}

	@Override
	public Integer getSaleDayByYear(int year, int month)
			throws DataAccessException {
		if (1 > month || 12 < month) {
			throw new IllegalArgumentException("month > 12 or month < 1");
		}
		if (1900 > year) {
			throw new IllegalArgumentException("year < 1900");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String getField = "T" + month;
		sql.append("select " + getField + " from SALE_DAY where status = 1 and year = ?");
		params.add(year);
		BigDecimal xx = (BigDecimal) repo.getObjectByQuery(sql.toString(),
				params);
		if (xx == null) {
			return null;
		}
		return (Integer) xx.intValue();
	}

	@Override
	public Integer getSaleDayByDate(Date date) throws DataAccessException {
		if (date == null) {
			throw new IllegalArgumentException("date is null");
		}
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return getSaleDayByYear(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1);
	}

	@Override
	public SaleDays getSaleDayForUpdate(int year) throws DataAccessException {
		if (1900 > year) {
			throw new IllegalArgumentException("year < 1900");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from SALE_DAY where year = ?");
		params.add(year);

		return repo.getEntityBySQL(SaleDays.class, sql.toString(), params);
	}

	@Override
	public void addSaleDay(int month, int year, int numDay, LogInfoVO logInfo)
			throws DataAccessException {
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}

		SaleDays oldSaleDay = getSaleDayByYear(year);

		if (null == oldSaleDay) {
			throw new IllegalArgumentException("can't get saleday of year, or saleday is invalidate");
		}

		List<Object> params = new ArrayList<Object>();
		StringBuffer sql = new StringBuffer();
		String getField = "T" + month;
		sql.append("update sale_day set " + getField + " = nvl(" + getField + ", 0) + ? where year = ?");
		//sql.append("update sale_day set ? = nvl(?  "); // set tham so nhu the nay thi T1 se thanh 'T1' -> loi
		//params.add(getField);
		//params.add(getField);
		//sql.append(", 0) + ? where year = ?");
		params.add(numDay);
		params.add(year);

		repo.executeSQLQuery(sql.toString(), params);
		SaleDays newSaleDay = getSaleDayByYear(year);

		try {
			actionGeneralLogDAO.createActionGeneralLog(oldSaleDay, newSaleDay, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SaleDayDAO#getSaleDayFromStartYearToDate(java.util.Date)
	 */
	@Override
	public Integer getSaleDayFromStartYearToDate(Date date)
			throws DataAccessException {
		try {
			//lay ra nam va thang hien tai
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH) + 1;
			Integer result = 0;
			for (int i = 1; i < month; i ++) {
				result = result + this.getSaleDayByYear(year, i);
			}
			return result;
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SaleDayDAO#getSumSaleDateOfYear(java.util.Date)
	 */
	@Override
	public Integer getSumSaleDateOfYear(Date date) throws DataAccessException {
		// TODO Auto-generated method stub
		try {
			Integer result = 0;
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			sql.append("select (t1 + t2 + t3 + t4 + t5 + t6 + ");
			sql.append("t7 + t8 + t9 + t10 + t11 + t12) as count from sale_day");
			sql.append(" where year = to_number(to_char(?,'YYYY')) and status = 1");
			params.add(date);
			result = repo.countBySQL(sql.toString(), params);
			return result;
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public Integer getSalesMonthByYear() throws DataAccessException {		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		int MM = DateUtility.getMonth(commonDAO.getSysDate());		
		sql.append("select T" + MM);
		//params.add(MM);
		sql.append(" from sale_day where year=?");
		params.add(DateUtility.getYear(commonDAO.getSysDate()));
		Object t = repo.getObjectByQuery(sql.toString(), params);
		return ((BigDecimal)t).intValue();
	}
	

	/***
	 * @author vuongmq
	 * @date 31/01/2015
	 * lay saleday theo nam va shop Id, 
	 * chua khai bao Mgr
	 */
	@Override
	public SaleDays getSaleDayByYearAndShopId(Long shopId, int year) throws DataAccessException {		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from sale_day ");
		sql.append("where 1 = 1 ");
		if(shopId != null){
			sql.append("and shop_id = ? ");
			params.add(shopId);
		}
		if(year != 0){
			sql.append("and year = ? ");
			params.add(year);
		}
		return repo.getEntityBySQL(SaleDays.class, sql.toString(), params);
	}
}

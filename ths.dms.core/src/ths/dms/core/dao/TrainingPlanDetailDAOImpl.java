package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.TrainingPlanDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class TrainingPlanDetailDAOImpl implements TrainingPlanDetailDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private CommonDAO commonDAO;

	@Override
	public TrainingPlanDetail createTrainingPlanDetail(TrainingPlanDetail trainingPlanDetail) throws DataAccessException {
		if (trainingPlanDetail == null) {
			throw new IllegalArgumentException("trainingPlanDetail");
		}
		trainingPlanDetail.setCreateDate(commonDAO.getSysDate());
		repo.create(trainingPlanDetail);
		return repo.getEntityById(TrainingPlanDetail.class, trainingPlanDetail.getId());
	}

	@Override
	public void deleteTrainingPlanDetail(TrainingPlanDetail trainingPlanDetail) throws DataAccessException {
		if (trainingPlanDetail == null) {
			throw new IllegalArgumentException("trainingPlanDetail");
		}
		repo.delete(trainingPlanDetail);
	}

	@Override
	public TrainingPlanDetail getTrainingPlanDetailById(long id) throws DataAccessException {
		return repo.getEntityById(TrainingPlanDetail.class, id);
	}
	
	@Override
	public void updateTrainingPlanDetail(TrainingPlanDetail trainingPlanDetail) throws DataAccessException {
		if (trainingPlanDetail == null) {
			throw new IllegalArgumentException("trainingPlanDetail");
		}
		repo.update(trainingPlanDetail);
	}
	
	@Override
	public TrainingPlanDetail getTrainingPlanDetailBy(String superCode,
			String staffCode, Date date) throws DataAccessException {
		if (superCode == null) {
			throw new IllegalArgumentException("superCode is null");
		}
//		if (staffCode == null) {
//			throw new IllegalArgumentException("staffCode is null");
//		}
		if (date == null) {
			throw new IllegalArgumentException("date is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *");
		sql.append(" FROM training_plan_detail tpd");
		sql.append(" WHERE 1 = 1");
		sql.append(" AND tpd.training_date >= TRUNC(?) AND tpd.training_date < TRUNC(?) + 1");
		params.add(date);
		params.add(date);
		sql.append(" AND EXISTS");
		sql.append(" (SELECT 1");
		sql.append(" FROM training_plan tp");
		sql.append(" WHERE tp.training_plan_id = tpd.training_plan_id");
		sql.append(" AND EXISTS (SELECT 1 FROM staff s where s.staff_id = tp.staff_id AND UPPER(s.staff_code) = ?)");
		params.add(superCode.toUpperCase());
		sql.append(" AND tp.month       >= TRUNC(?, 'MONTH')");
		params.add(date);
		sql.append(" AND tp.month       < TRUNC(?, 'MONTH') + interval '1' month");
		params.add(date);
		sql.append(" )");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" AND EXISTS (SELECT 1 FROM staff s where s.staff_id = tpd.staff_id AND UPPER(s.staff_code) = ?)");
			params.add(staffCode.toUpperCase());
		}

		return repo.getFirstBySQL(TrainingPlanDetail.class, sql.toString(), params);
	}
	
	@Override
	public List<TrainingPlanDetail> getListTrainingPlanDetailByStaffId(KPaging<TrainingPlanDetail> kPaging,Long staffId, Date date) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *");
		sql.append(" FROM training_plan_detail tpd");
		sql.append(" WHERE 1 = 1");
		sql.append(" AND EXISTS");
		sql.append(" (SELECT 1");
		sql.append(" FROM training_plan tp");
		sql.append(" WHERE tp.training_plan_id = tpd.training_plan_id");
//		sql.append(" WHERE tp.id = tpd.training_plan_id");
		sql.append(" AND tp.staff_id           = ?");
		params.add(staffId);
		sql.append(" AND tp.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND tp.month       >= TRUNC(?, 'MONTH')");
		params.add(date);
		sql.append(" AND tp.month       < TRUNC(?, 'MONTH') + interval '1' month");
		params.add(date);
		sql.append(" )");
		
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");
		if (kPaging == null)
			return repo.getListBySQL(TrainingPlanDetail.class, sql.toString(), params);
		else 
			return repo.getListBySQLPaginated(TrainingPlanDetail.class, sql.toString(), countSql.toString(), params, params, kPaging);
	}
	@Override
	public int countTrainingPlanDetail(Long trainingPlanId) throws DataAccessException {
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		countSql.append("select count(*) as count from training_plan_detail where training_plan_id = ? ");
		params.add(trainingPlanId);
		return repo.countBySQL(countSql.toString(), params);
	}

	@Override
	public void updateTrainingPlanDetailStatusToDelete(Staff staff, Long shopId) throws DataAccessException {
		/*StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (StaffObjectType.TBHV.getValue().equals(staff.getStaffType().getObjectType())) {	
			sql.append(" update training_plan_detail tpd set status_manager = -1 ");
			sql.append(" where training_date >= trunc(sysdate) and status_manager != -1 ");
			sql.append(" and area_manager_id = ? ");
			params.add(staff.getId());
			repo.executeSQLQuery(sql.toString(), params);
		}
		else if (StaffObjectType.NVGS.getValue().equals(staff.getStaffType().getObjectType())) {
			sql.append(" update training_plan_detail tpd set status = -1 ");
			sql.append(" where training_date >= trunc(sysdate) and status != -1 ");
			sql.append(" and exists ( select 1 from staff where staff_owner_id = ? and staff_id = tpd.staff_id and shop_id = ?) ");
			params.add(staff.getId());
			params.add(shopId);
			repo.executeSQLQuery(sql.toString(), params);
		}
		else if (StaffObjectType.NVBH.getValue().equals(staff.getStaffType().getObjectType())) {
			sql.append(" update training_plan_detail tpd set status = -1 ");
			sql.append(" where training_date >= trunc(sysdate) and status != -1 ");
			sql.append(" and exists ( select 1 from staff where staff_id = ? and staff_id = tpd.staff_id and shop_id = ?) ");
			params.add(staff.getId());
			params.add(shopId);
			repo.executeSQLQuery(sql.toString(), params);
		}else if (StaffObjectType.NVVS.getValue().equals(staff.getStaffType().getObjectType())) {
			sql.append(" update training_plan_detail tpd set status = -1 ");
			sql.append(" where training_date >= trunc(sysdate) and status != -1 ");
			sql.append(" and exists ( select 1 from staff where staff_id = ? and staff_id = tpd.staff_id and shop_id = ?) ");
			params.add(staff.getId());
			params.add(shopId);
			repo.executeSQLQuery(sql.toString(), params);
		}*/
		return ;
	}
}

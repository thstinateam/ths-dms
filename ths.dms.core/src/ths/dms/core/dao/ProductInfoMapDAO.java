package ths.dms.core.dao;

import ths.dms.core.entities.ProductInfoMap;
import ths.dms.core.entities.enumtype.ProductInfoMapType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface ProductInfoMapDAO {

	ProductInfoMap createProductInfoMap(ProductInfoMap productInfoMap,LogInfoVO logInfo) throws DataAccessException;

	void deleteProductInfoMap(ProductInfoMap productInfoMap, LogInfoVO logInfo) throws DataAccessException;

	ProductInfoMap getProductInfoMapById(long id) throws DataAccessException;

	void updateProductInfoMap(ProductInfoMap productInfoMap, LogInfoVO logInfo)throws DataAccessException;

	ProductInfoMap getProductInfoMap(Long catId, Long subCatId,ProductInfoMapType productInfoMapType) throws DataAccessException;

	/**
	 * lay ProductInfoMap theo nganh hang con
	 * @author trietptm
	 * @param subCatId
	 * @param productInfoMapType
	 * @return
	 * @throws DataAccessException
	 * @since Nov 17, 2015
	 */
	ProductInfoMap getProductInfoMapByToId(long toId, ProductInfoMapType productInfoMapType) throws DataAccessException;
}
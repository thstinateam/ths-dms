package ths.dms.core.dao;

/**
/**
 * Import Thu vien ho tro
 * */
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.EquipAttachFile;
import ths.dms.core.entities.EquipLiquidationFormDtl;
import ths.dms.core.entities.EquipLostMobileRec;
import ths.dms.core.entities.EquipLostRecord;
import ths.dms.core.entities.EquipStatisticRecDtl;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipTradeType;
import ths.dms.core.entities.enumtype.EquipUsageStatus;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.LinquidationStatus;
import ths.dms.core.entities.enumtype.StatusRecordsEquip;
import ths.dms.core.entities.enumtype.StatusType;
import ths.dms.core.entities.enumtype.TableName;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.EquipRecordFilter;
import ths.dms.core.entities.filter.EquipStatisticFilter;
import ths.dms.core.entities.filter.EquipStockFilter;
import ths.dms.core.entities.vo.EquipLostMobileRecVO;
import ths.dms.core.entities.vo.EquipRecordVO;
import ths.dms.core.entities.vo.EquipStatisticRecordDetailVO;
import ths.dms.core.entities.vo.EquipStockVO;
import ths.dms.core.entities.vo.EquipmentDeliveryVO;
import ths.dms.core.entities.vo.EquipmentEvictionVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.FileVO;
import ths.dms.core.entities.vo.StatisticCheckingVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

/**
 * Xu ly tuong tac DB
 * 
 * @author hunglm16
 * @since December 31,2014
 * @description Dung chung cho Bien ban Thiet Bi
 * */
public class EquipRecordDAOImpl implements EquipRecordDAO {
	@Autowired
	private IRepository repo;

	@Autowired
	private CommonDAO commonDAO;

	@Override
	public EquipLostRecord getEquipLostRecordById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipLostRecord.class, id);
	}
	
	@Override
	public EquipLostRecord getEquipLostRecordById(Long id, Long shopId) throws DataAccessException {
		if (id == null) {
			throw new IllegalArgumentException("EquipLostRecordId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select e.* ");
		sql.append(" from equip_lost_record e ");
		sql.append(" where e.equip_lost_record_id = ? ");
		params.add(id);
		if (shopId != null) {
			sql.append(" AND ( ");
			sql.append("  (e.stock_type = 1 and e.stock_id IN ");
			sql.append(" (SELECT equip_stock_id ");
			sql.append(" FROM equip_stock ");
			sql.append(" WHERE status = 1 ");
			sql.append(" and shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append(" ) ");
			params.add(shopId);
			/** end kho NPP */
			sql.append(" or  (e.stock_type = 2 and e.stock_id IN ");
			sql.append(" (SELECT cu.customer_id ");
			sql.append(" FROM customer cu ");
			sql.append(" WHERE  ");
			sql.append(" cu.shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append("  ");
			sql.append(" ) ");
			/** end kho KH */
			sql.append(" ) "); // end And
			params.add(shopId);
		}
		return repo.getEntityBySQL(EquipLostRecord.class, sql.toString(), params);
	}

	@Override
	public void deletEquipAttachFile(EquipAttachFile equipAttachFile) throws DataAccessException {
		repo.delete(equipAttachFile);
	}

	@Override
	public EquipLostMobileRec getEquipLostMobileRecById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipLostMobileRec.class, id);
	}
	
	@Override
	public EquipLostMobileRec getEquipLostMobileRecById(Long id, Long shopId) throws DataAccessException {
		if (id == null) {
			throw new IllegalArgumentException("EquipLostMobileRecid is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select e.* ");
		sql.append(" from equip_lost_mobile_rec e ");
		sql.append(" where e.equip_lost_mobile_rec_id = ? ");
		params.add(id);
		if (shopId != null) {
			sql.append(" and e.stock_id IN ");
			sql.append(" (SELECT cu.customer_id ");
			sql.append(" FROM customer cu ");
			sql.append(" WHERE  ");
			sql.append(" cu.shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			params.add(shopId);
		}
		return repo.getEntityBySQL(EquipLostMobileRec.class, sql.toString(), params);
	}

	@Override
	public Integer countEquipLostMobileRecByFilter(EquipRecordFilter<EquipRecordVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(*) as count from EQUIP_LOST_MOBILE_REC ");
		sql.append(" where 1 = 1 ");
		if (filter.getEquipId() != null) {
			sql.append(" and equip_id = ? ");
			params.add(filter.getEquipId());

		}
		if (filter.getEqLostRecId() != null) {
			sql.append(" and equip_lost_rec_id = ? ");
			params.add(filter.getEqLostRecId());
		}
		if (filter.getStockId() != null) {
			sql.append(" and stock_id = ? ");
			params.add(filter.getStockId());
		}
		return repo.countBySQL(sql.toString(), params);

	}

	@Override
	public EquipAttachFile createEquipAttachFile(EquipAttachFile equipAttFile) throws DataAccessException {
		if (equipAttFile == null) {
			throw new IllegalArgumentException("equipAttFile is null");
		}
		return repo.create(equipAttFile);
	}

	@Override
	public EquipLostRecord createEquipLostRecord(EquipLostRecord equipLostRecord) throws DataAccessException {
		if (equipLostRecord == null) {
			throw new IllegalArgumentException("equipLostRecord is null");
		}
		return repo.create(equipLostRecord);
	}

	@Override
	public EquipStatisticRecDtl getEquipStatisticRecDtlById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipStatisticRecDtl.class, id);
	}

	@Override
	public String getMaxCodeEquipLostRecord(String perfix) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select max(code) as maxCode from equip_lost_record where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(perfix)) {
			sql.append(" and lower(code) like ? ");
			params.add(StringUtility.toOracleSearchLikeSuffix(perfix.toLowerCase().trim()));
		}
		String code = (String) repo.getObjectByQuery(sql.toString(), params);
		if (code == null) {
			return "BM00000001";
		}
		return code;
	}

	@Override
	public void updateEquipLostRecord(EquipLostRecord equipLostRecord) throws DataAccessException {
		if (equipLostRecord == null) {
			throw new IllegalArgumentException("equipLostRecord");
		}
		equipLostRecord.setUpdateDate(commonDAO.getSysDate());
		repo.update(equipLostRecord);
	}

	@Override
	public void updateEquipLostMobileRec(EquipLostMobileRec equipLostMobileRec) throws DataAccessException {
		if (equipLostMobileRec == null) {
			throw new IllegalArgumentException("equipLostMobileRec");
		}
		equipLostMobileRec.setUpdateDate(commonDAO.getSysDate());
		repo.update(equipLostMobileRec);
	}
	
	/**
	 * list detail
	 * @author phut
	 */
	@Override
	public List<StatisticCheckingVO> getListDetailGroupSoLan(KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId, Long equipGroupId, String equipCode, String seri, Long stockId, Integer stepCheck) throws DataAccessException {
		StringBuilder SQL = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		SQL.append("WITH listUnit AS \n");
		SQL.append("  ( SELECT sh.shop_id shopId, sh.shop_code shopCode, sh.shop_name shopName FROM shop sh WHERE sh.shop_id IN \n");
		SQL.append("    (SELECT unit_id FROM equip_statistic_unit WHERE EQUIP_STATISTIC_RECORD_ID = ? \n");
		params.add(recordId);
		SQL.append("    ) \n");
		SQL.append("  ), \n");
		SQL.append("  listEquip AS \n");
		SQL.append("  ( SELECT equipment.EQUIP_ID equipId, equipment.CODE equipCode, equip_group.EQUIP_GROUP_ID equipGroupId,equipment.stock_id, equip_group.CODE equipCodeGroup, equip_group.NAME equipNameGroup, equipment.SERIAL seri FROM equipment INNER JOIN equip_group ON equipment.EQUIP_GROUP_ID = equip_group.EQUIP_GROUP_ID WHERE 1=1 and (equip_group.EQUIP_GROUP_ID IN \n");
		SQL.append("    (SELECT OBJECT_ID FROM equip_statistic_group WHERE STATUS = 1 AND OBJECT_TYPE = 1  and EQUIP_STATISTIC_RECORD_ID = ?\n");
		params.add(recordId);
		SQL.append("    ) ");
		SQL.append("     or  equipment.EQUIP_id in ");
		SQL.append("    (SELECT OBJECT_ID FROM equip_statistic_group WHERE STATUS = 1 AND OBJECT_TYPE = 3  and EQUIP_STATISTIC_RECORD_ID = ?\n");
		params.add(recordId);
		SQL.append("    ) ");
		SQL.append("     )");
		
		
		SQL.append("   and equipment.status = ? ");
		params.add(StatusType.ACTIVE.getValue());
//		SQL.append("   and equipment.trade_status = ? ");
//		params.add(EquipTradeStatus.NOT_TRADE.getValue());
//		SQL.append("   and equipment.trade_type is null ");
		SQL.append("   and equipment.stock_type = ? ");
		params.add(EquipStockTotalType.KHO.getValue());
		SQL.append("   and equipment.usage_status in (?,?) ");
		params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
		params.add(EquipUsageStatus.SHOWING_REPAIR.getValue());
		SQL.append("  ), \n");
		SQL.append("  listShopEquip AS \n");
		SQL.append("  ( SELECT shopId, shopCode, shopName, equip_stock.EQUIP_STOCK_ID stockId, equip_stock_total.EQUIP_STOCK_TOTAL_ID stockTotalId, equip_stock.CODE stockCode, equip_stock.NAME stockName, listEquip.equipId, listEquip.equipCode, listEquip.equipGroupId, listEquip.equipCodeGroup, listEquip.equipNameGroup, listEquip.seri FROM listUnit INNER JOIN equip_stock ON listUnit.shopId = equip_stock.SHOP_ID INNER JOIN equip_stock_total ON equip_stock_total.STOCK_TYPE = 1 AND equip_stock_total.STOCK_ID = equip_stock.EQUIP_STOCK_ID INNER JOIN listEquip ON equip_stock_total.EQUIP_GROUP_ID = listEquip.equipGroupId and equip_stock.equip_stock_id = listEquip.stock_id \n");
		SQL.append("  ) \n");
		SQL.append(" SELECT listShopEquip.shopId, \n");
		SQL.append("  listShopEquip.shopCode, \n");
		SQL.append("  listShopEquip.shopName, \n");
		SQL.append("  listShopEquip.stockId, \n");
		SQL.append("  listShopEquip.stockTotalId, \n");
		SQL.append("  listShopEquip.stockCode, \n");
		SQL.append("  listShopEquip.stockName, \n");
		SQL.append("  listShopEquip.equipId, \n");
		SQL.append("  listShopEquip.equipCode, \n");
		SQL.append("  listShopEquip.equipGroupId, \n");
		SQL.append("  listShopEquip.equipCodeGroup, \n");
		SQL.append("  listShopEquip.equipNameGroup, \n");
		SQL.append("  listShopEquip.seri, \n");
		SQL.append("  equip_statistic_rec_dtl.STATISTIC_TIME stepCheck, \n");
		SQL.append("  ( \n");
		SQL.append("  CASE equip_statistic_rec_dtl.STATUS \n");
		SQL.append("    WHEN 2 \n");
		SQL.append("    THEN 1 \n");
		SQL.append("    WHEN 3 \n");
		SQL.append("    THEN 0 \n");
		SQL.append("    ELSE NULL \n");
		SQL.append("  END) isBelong, \n");
		SQL.append("  equip_statistic_rec_dtl.EQUIP_STATISTIC_REC_DTL_ID detailId, \n");
		SQL.append(" (select count(1) from EQUIP_ATTACH_FILE where EQUIP_ATTACH_FILE.OBJECT_ID = equip_statistic_rec_dtl.EQUIP_STATISTIC_REC_DTL_ID and EQUIP_ATTACH_FILE.OBJECT_TYPE = ?) numImage ");
		params.add(EquipTradeType.INVENTORY.getValue());
		SQL.append(" FROM listShopEquip \n");
		SQL.append(" LEFT JOIN equip_statistic_rec_dtl \n");
		SQL.append(" ON listShopEquip.shopId = equip_statistic_rec_dtl.SHOP_ID \n");
		SQL.append(" AND listShopEquip.equipId = equip_statistic_rec_dtl.OBJECT_ID \n");
		SQL.append(" AND listShopEquip.stockId = equip_statistic_rec_dtl.OBJECT_STOCK_ID \n");
		SQL.append(" AND equip_statistic_rec_dtl.EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
		if (stepCheck != null) {
			SQL.append(" AND equip_statistic_rec_dtl.STATISTIC_TIME = ? ");
			params.add(stepCheck);
		}
		SQL.append(" WHERE 1 = 1 ");
		if (shopId != null && shopId > -1) {
			SQL.append(" AND listShopEquip.shopId = ?");
			params.add(shopId);
		}
		if (equipGroupId != null && equipGroupId > -1) {
			SQL.append(" AND listShopEquip.equipGroupId = ? ");
			params.add(equipGroupId);
		}
		if (!StringUtility.isNullOrEmpty(equipCode)) {
			SQL.append(" AND listShopEquip.equipCode like  ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipCode.trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(seri)) {
			SQL.append(" AND listShopEquip.seri like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(seri.trim().toUpperCase()));
		}
		if (stockId != null && stockId > -1) {
			SQL.append(" AND listShopEquip.stockId = ? ");
			params.add(stockId);
		}

		SQL.append(" order by listShopEquip.shopCode,listShopEquip.equipCodeGroup,listShopEquip.equipCode ");

		String[] fieldNames = { "shopId",//1
			"shopCode",//2
			"shopName",//3
			"stockId",//4
			"stockTotalId",//5
			"stockCode",//6
			"stockName",//7
			"equipId",//8
			"equipCode",//9
			"equipGroupId",//10
			"equipCodeGroup",//11
			"equipNameGroup",//12
			"seri",//13
			"stepCheck",//14
			"isBelong",//15
			"detailId",//16
			"numImage"
		};
		Type[] fieldTypes = {
			StandardBasicTypes.LONG,//1
			StandardBasicTypes.STRING,//2
			StandardBasicTypes.STRING,//3
			StandardBasicTypes.LONG,//4
			StandardBasicTypes.LONG,//5
			StandardBasicTypes.STRING,//6
			StandardBasicTypes.STRING,//7
			StandardBasicTypes.LONG,//8
			StandardBasicTypes.STRING,//9
			StandardBasicTypes.LONG,//10
			StandardBasicTypes.STRING,//11
			StandardBasicTypes.STRING,//12
			StandardBasicTypes.STRING,//13
			StandardBasicTypes.INTEGER,//14
			StandardBasicTypes.INTEGER,//15
			StandardBasicTypes.LONG,//16
			StandardBasicTypes.INTEGER
		};
		
		if(kPaging == null) {
			return repo.getListByQueryAndScalar(StatisticCheckingVO.class, fieldNames, fieldTypes, SQL.toString(), params);
		} else {
			String countSQL = "select count(1) as count from (" + SQL.toString() + ")";
			return repo.getListByQueryAndScalarPaginated(StatisticCheckingVO.class, fieldNames, fieldTypes, SQL.toString(), countSQL, params, params, kPaging);
		}
	}
	
	/**
	 * list detail
	 * @author phut
	 */
	@Override
	public List<StatisticCheckingVO> getListDetailGroupTuyen(KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId, Long equipGroupId, String equipCode, String seri, Long stockId, Date checkDate) throws DataAccessException {
		StringBuilder SQL = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		SQL.append("WITH listUnit AS \n");
		SQL.append("  ( SELECT sh.shop_id shopId, sh.shop_code shopCode, sh.shop_name shopName FROM shop sh WHERE sh.shop_id IN \n");
		SQL.append("    (SELECT unit_id FROM equip_statistic_unit WHERE EQUIP_STATISTIC_RECORD_ID = ? \n");
		params.add(recordId);
		SQL.append("    ) \n");
		SQL.append("  ), \n");
		SQL.append("  listEquip AS \n");
		SQL.append("  ( SELECT equipment.EQUIP_ID equipId, equipment.CODE equipCode, equip_group.EQUIP_GROUP_ID equipGroupId,equipment.stock_id, equip_group.CODE equipCodeGroup, equip_group.NAME equipNameGroup, equipment.SERIAL seri FROM equipment INNER JOIN equip_group ON equipment.EQUIP_GROUP_ID = equip_group.EQUIP_GROUP_ID WHERE 1=1 and (equip_group.EQUIP_GROUP_ID IN \n");
		SQL.append("    (SELECT OBJECT_ID FROM equip_statistic_group WHERE STATUS = 1 AND OBJECT_TYPE = 1 and EQUIP_STATISTIC_RECORD_ID = ? \n");
		params.add(recordId);
		SQL.append("    ) \n");
		SQL.append("   OR ");
		SQL.append("    equipment.EQUIP_id in  ");
		SQL.append("   (SELECT OBJECT_ID FROM equip_statistic_group WHERE STATUS = 1 AND OBJECT_TYPE = 3 and EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(recordId);
		SQL.append("    ) )");
		SQL.append("   and equipment.status = ? ");
		params.add(StatusType.ACTIVE.getValue());
//		SQL.append("   and equipment.trade_status = ? ");
//		params.add(EquipTradeStatus.NOT_TRADE.getValue());
//		SQL.append("   and equipment.trade_type is null ");
		SQL.append("   and equipment.stock_type = ? ");
		params.add(EquipStockTotalType.KHO.getValue());
		SQL.append("   and equipment.usage_status in (?,?) ");
		params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
		params.add(EquipUsageStatus.SHOWING_REPAIR.getValue());
		SQL.append("  ), \n");
		SQL.append("  listShopEquip AS \n");
		SQL.append("  ( SELECT shopId, shopCode, shopName, equip_stock.EQUIP_STOCK_ID stockId, equip_stock_total.EQUIP_STOCK_TOTAL_ID stockTotalId, equip_stock.CODE stockCode, equip_stock.NAME stockName, listEquip.equipId, listEquip.equipCode, listEquip.equipGroupId, listEquip.equipCodeGroup, listEquip.equipNameGroup, listEquip.seri FROM listUnit INNER JOIN equip_stock ON listUnit.shopId = equip_stock.SHOP_ID INNER JOIN equip_stock_total ON equip_stock_total.STOCK_TYPE = 1 AND equip_stock_total.STOCK_ID = equip_stock.EQUIP_STOCK_ID INNER JOIN listEquip ON equip_stock_total.EQUIP_GROUP_ID = listEquip.equipGroupId and equip_stock.equip_stock_id = listEquip.stock_id \n");
		SQL.append("  ) \n");
		SQL.append("SELECT listShopEquip.shopId, \n");
		SQL.append("  listShopEquip.shopCode, \n");
		SQL.append("  listShopEquip.shopName, \n");
		SQL.append("  listShopEquip.stockId, \n");
		SQL.append("  listShopEquip.stockTotalId, \n");
		SQL.append("  listShopEquip.stockCode, \n");
		SQL.append("  listShopEquip.stockName, \n");
		SQL.append("  listShopEquip.equipId, \n");
		SQL.append("  listShopEquip.equipCode, \n");
		SQL.append("  listShopEquip.equipGroupId, \n");
		SQL.append("  listShopEquip.equipCodeGroup, \n");
		SQL.append("  listShopEquip.equipNameGroup, \n");
		SQL.append("  listShopEquip.seri, \n");
		SQL.append("  to_char(equip_statistic_rec_dtl.STATISTIC_DATE, 'dd/mm/yyyy') checkDate, \n");
		SQL.append("  ( \n");
		SQL.append("  CASE equip_statistic_rec_dtl.STATUS \n");
		SQL.append("    WHEN 2 \n");
		SQL.append("    THEN 1 \n");
		SQL.append("    WHEN 3 \n");
		SQL.append("    THEN 0 \n");
		SQL.append("    ELSE NULL \n");
		SQL.append("  END) isBelong, \n");
		SQL.append("  equip_statistic_rec_dtl.EQUIP_STATISTIC_REC_DTL_ID detailId, \n");
		SQL.append(" (select count(1) from EQUIP_ATTACH_FILE where EQUIP_ATTACH_FILE.OBJECT_ID = equip_statistic_rec_dtl.EQUIP_STATISTIC_REC_DTL_ID and EQUIP_ATTACH_FILE.OBJECT_TYPE = ?) numImage ");
		params.add(EquipTradeType.INVENTORY.getValue());
		SQL.append("FROM listShopEquip \n");
		SQL.append("LEFT JOIN equip_statistic_rec_dtl \n");
		SQL.append("ON listShopEquip.shopId = equip_statistic_rec_dtl.SHOP_ID \n");
		SQL.append("AND listShopEquip.equipId = equip_statistic_rec_dtl.OBJECT_ID \n");
		SQL.append("AND listShopEquip.stockId = equip_statistic_rec_dtl.OBJECT_STOCK_ID \n");
		SQL.append("AND equip_statistic_rec_dtl.EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(recordId);
		if (checkDate != null) {
			SQL.append(" AND equip_statistic_rec_dtl.STATISTIC_DATE >= trunc(?) and equip_statistic_rec_dtl.STATISTIC_DATE < trunc(?) + 1 ");
			params.add(checkDate);
			params.add(checkDate);
		}
		SQL.append(" WHERE 1 = 1 ");
		if (shopId != null && shopId > -1) {
			SQL.append(" AND listShopEquip.shopId = ?");
			params.add(shopId);
		}
		if (equipGroupId != null && equipGroupId > -1) {
			SQL.append(" AND listShopEquip.equipGroupId = ? ");
			params.add(equipGroupId);
		}
		if (!StringUtility.isNullOrEmpty(equipCode)) {
			SQL.append(" AND listShopEquip.equipCode like  ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(equipCode.trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(seri)) {
			SQL.append(" AND listShopEquip.seri like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(seri.trim().toUpperCase()));
		}
		if (stockId != null && stockId > -1) {
			SQL.append(" AND listShopEquip.stockId = ? ");
			params.add(stockId);
		}
		
		SQL.append(" order by listShopEquip.shopCode,listShopEquip.equipCodeGroup,listShopEquip.equipCode ");
		
		String[] fieldNames = {
			"shopId",//1
			"shopCode",//2
			"shopName",//3
			"stockId",//4
			"stockTotalId",//5
			"stockCode",//6
			"stockName",//7
			"equipId",//8
			"equipCode",//9
			"equipGroupId",//10
			"equipCodeGroup",//11
			"equipNameGroup",//12
			"seri",//13
			"checkDate",//14
			"isBelong",//15
			"detailId",//16
			"numImage"
		};
		Type[] fieldTypes = {
			StandardBasicTypes.LONG,//1
			StandardBasicTypes.STRING,//2
			StandardBasicTypes.STRING,//3
			StandardBasicTypes.LONG,//4
			StandardBasicTypes.LONG,//5
			StandardBasicTypes.STRING,//6
			StandardBasicTypes.STRING,//7
			StandardBasicTypes.LONG,//8
			StandardBasicTypes.STRING,//9
			StandardBasicTypes.LONG,//10
			StandardBasicTypes.STRING,//11
			StandardBasicTypes.STRING,//12
			StandardBasicTypes.STRING,//13
			StandardBasicTypes.STRING,//14
			StandardBasicTypes.INTEGER,//15
			StandardBasicTypes.LONG,//16
			StandardBasicTypes.INTEGER
		};
		
		if(kPaging == null) {
			return repo.getListByQueryAndScalar(StatisticCheckingVO.class, fieldNames, fieldTypes, SQL.toString(), params);
		} else {
			String countSQL = "select count(1) as count from (" + SQL.toString() + ")";
			return repo.getListByQueryAndScalarPaginated(StatisticCheckingVO.class, fieldNames, fieldTypes, SQL.toString(), countSQL, params, params, kPaging);
		}
	}
	
	/**
	 * get list detail to check STATISTIC_TIME
	 * @author phut
	 */
	@Override
	public List<EquipStatisticRecDtl> getListEquipStatisticRecDetail(KPaging<EquipStatisticRecDtl> kPaging, Long recordId, Long shopId, Long stockId, Long objectId) throws DataAccessException {
		StringBuilder SQL = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		SQL.append("select * from EQUIP_STATISTIC_REC_DTL where 1 = 1 ");
		if(recordId != null) {
			SQL.append(" and EQUIP_STATISTIC_RECORD_ID = ? ");
			params.add(recordId);
		}
		
		if(shopId != null) {
			SQL.append(" and SHOP_ID = ? ");
			params.add(shopId);
		}
		
		if(stockId != null) {
			SQL.append(" and OBJECT_STOCK_ID = ? ");
			params.add(stockId);
		}
		
		if(objectId != null) {
			SQL.append(" and OBJECT_ID = ? ");
			params.add(objectId);
		}
		
		SQL.append(" order by STATISTIC_TIME desc");
		
		if(kPaging != null) {
			return repo.getListBySQLPaginated(EquipStatisticRecDtl.class, SQL.toString(), params, kPaging);
		} else {
			return repo.getListBySQL(EquipStatisticRecDtl.class, SQL.toString(), params);
		}
	}
	
	@Override
	public List<EquipStatisticRecordDetailVO> getListStatisticTime(EquipStatisticFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT to_char(dt.STATISTIC_DATE, 'dd/mm/yyyy') statisticDate, dt.STATISTIC_TIME statisticTime, dt.status ");
		sql.append(" FROM EQUIP_STATISTIC_REC_DTL dt ");
		sql.append(" JOIN equipment e ON e.equip_id = dt.object_id ");
		sql.append(" JOIN equip_group eg ON eg.equip_group_id = e.equip_group_id ");
		sql.append(" JOIN equip_category ec ON eg.equip_category_id = ec.equip_category_id ");
		sql.append(" JOIN customer c ON c.customer_id = dt.object_stock_id AND dt.object_stock_type = ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append(" JOIN shop sh ON sh.shop_id = c.shop_id ");
		sql.append(" LEFT JOIN staff st ON st.staff_id = dt.staff_id ");
		sql.append(" WHERE 1 = 1 ");
		if (filter.getRecordId() != null) {
			sql.append(" and dt.EQUIP_STATISTIC_RECORD_ID = ? ");
			params.add(filter.getRecordId());
		}
		if (filter.getEquipId() != null) {
			sql.append(" and dt.object_id = ? ");
			params.add(filter.getEquipId());
		}
		if (filter.getNotStatus() != null) {
			sql.append(" and dt.status != ? ");
			params.add(filter.getNotStatus());
		}
		if(filter.getStatus() != null && filter.getStatus() > -1) {
			sql.append(" and dt.status = ? ");
			params.add(filter.getStatus());
		}
		if(filter.getStepCheck() != null) {
			sql.append(" and dt.STATISTIC_TIME = ? ");
			params.add(filter.getStepCheck());
		}
		if(filter.getShopId() != null) {
			sql.append(" and dt.shop_id = ? ");
			params.add(filter.getShopId());
		}
		if(!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" AND lower(sh.shop_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			sql.append(" AND lower(e.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipCode().trim().toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getSeri())) {
			sql.append(" AND lower(e.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeri().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and (upper(c.short_code) like ? ESCAPE '/' or c.name_text like ? ESCAPE '/')");
			String customerCode = Unicode2English.codau2khongdau(filter.getCustomerCode().trim().toUpperCase());
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
		}
		if(!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" AND (upper(st.staff_code) like  ? ESCAPE '/' OR st.name_text like ? ESCAPE '/' )");
			String staffCode = Unicode2English.codau2khongdau(filter.getStaffCode().trim().toUpperCase());
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode));
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode));
		}
		if (filter.getFromDate() != null) {
			sql.append(" and dt.STATISTIC_DATE >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and dt.STATISTIC_DATE < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		String[] fieldNames = { 
				"statisticTime",//1
				"status",		//2
				"statisticDate",//3
		};
		Type[] fieldTypes = { 
				StandardBasicTypes.INTEGER,//1
				StandardBasicTypes.INTEGER,//2
				StandardBasicTypes.STRING,//3
		};

		if (filter.getkPagingESRD() == null) {
			sql.append(" order by dt.STATISTIC_TIME desc");
			return repo.getListByQueryAndScalar(EquipStatisticRecordDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			String countSQL = "select count(1) as count from (" + sql.toString() + ")";
			sql.append(" order by dt.STATISTIC_TIME desc");
			return repo.getListByQueryAndScalarPaginated(EquipStatisticRecordDetailVO.class, fieldNames, fieldTypes, sql.toString(), countSQL, params, params, filter.getkPagingESRD());
		}
	}

	/**
	 * list detail
	 * @author phut
	 */
	@Override
	public List<StatisticCheckingVO> getListDetailShelfSoLan(KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId, Long productId, Long stockId, Integer stepCheck) throws DataAccessException {
		StringBuilder SQL = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		SQL.append("WITH listUnit AS \n");
		SQL.append("  ( SELECT sh.shop_id shopId, sh.shop_code shopCode, sh.shop_name shopName FROM shop sh WHERE sh.shop_id IN \n");
		SQL.append("    (SELECT unit_id FROM equip_statistic_unit WHERE EQUIP_STATISTIC_RECORD_ID = ? \n");
		params.add(recordId);
		SQL.append("    ) \n");
		SQL.append("  ), \n");
		SQL.append("  listEquip AS \n");
		SQL.append("  ( SELECT stock_total.shop_id shopId, product.PRODUCT_ID equipId, product.PRODUCT_CODE equipCode, product.PRODUCT_NAME equipName, warehouse.warehouse_ID stockId, stock_total.STOCK_TOTAL_ID stockTotalId, warehouse.warehouse_CODE stockCode, warehouse.warehouse_NAME stockName FROM product INNER JOIN stock_total ON product.product_id = stock_total.product_id AND stock_total.OBJECT_TYPE = 1 INNER JOIN warehouse ON warehouse.shop_id = stock_total.shop_id and warehouse.warehouse_id = stock_total.warehouse_id WHERE product.PRODUCT_ID IN \n");
		SQL.append("    (SELECT OBJECT_ID FROM equip_statistic_group WHERE STATUS = 1 AND OBJECT_TYPE = 2 and EQUIP_STATISTIC_RECORD_ID = ? \n");
		params.add(recordId);
		SQL.append("    ) AND stock_total.shop_id IN \n");
		SQL.append("    (SELECT shopId FROM listUnit \n");
		SQL.append("    ) \n");
		SQL.append("  ), \n");
		SQL.append("  listShopEquip AS \n");
		SQL.append("  ( SELECT listUnit.shopId, listUnit.shopCode, listUnit.shopName, listEquip.equipId, listEquip.equipCode, listEquip.equipName, listEquip.stockId, listEquip.stockTotalId, listEquip.stockCode, listEquip.stockName FROM listUnit INNER JOIN listEquip ON listUnit.shopId = listEquip.shopId \n");
		SQL.append("  ) \n");
		SQL.append("SELECT listShopEquip.shopId, listShopEquip.shopCode, listShopEquip.shopName, listShopEquip.equipId, listShopEquip.equipCode, listShopEquip.equipName, listShopEquip.stockId, listShopEquip.stockTotalId, listShopEquip.stockCode, listShopEquip.stockName, equip_statistic_rec_dtl.STATISTIC_TIME stepCheck, equip_statistic_rec_dtl.QUANTITY quantity, equip_statistic_rec_dtl.QUANTITY_ACTUALLY actualQuantity, equip_statistic_rec_dtl.EQUIP_STATISTIC_REC_DTL_ID detailId, ");
		SQL.append(" (select count(1) from EQUIP_ATTACH_FILE where EQUIP_ATTACH_FILE.OBJECT_ID = equip_statistic_rec_dtl.EQUIP_STATISTIC_REC_DTL_ID and EQUIP_ATTACH_FILE.OBJECT_TYPE = ?) numImage ");
		params.add(EquipTradeType.INVENTORY.getValue());
		SQL.append(" FROM listShopEquip LEFT JOIN equip_statistic_rec_dtl ON listShopEquip.shopId = equip_statistic_rec_dtl.shop_id AND listShopEquip.stockId = equip_statistic_rec_dtl.OBJECT_STOCK_ID AND listShopEquip.equipId = equip_statistic_rec_dtl.OBJECT_ID AND equip_statistic_rec_dtl.EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(recordId);
		if (stepCheck != null) {
			SQL.append(" and equip_statistic_rec_dtl.STATISTIC_TIME = ? ");
			params.add(stepCheck);
		}
		SQL.append(" where 1 = 1 ");
		if (shopId != null && shopId > -1) {
			SQL.append(" and listShopEquip.shopId = ? ");
			params.add(shopId);
		}
		if (productId != null && productId > -1) {
			SQL.append(" and listShopEquip.equipId = ? ");
			params.add(productId);
		}
		if (stockId != null && stockId > -1) {
			SQL.append(" and listShopEquip.stockId = ? ");
			params.add(stockId);
		}
		
		SQL.append(" order by listShopEquip.shopCode,listShopEquip.equipCode ");
		
		String[] fieldNames = {
			"shopId",//1
			"shopCode",//2
			"shopName",//3
			"equipId",//4
			"equipCode",//5
			"equipName",//6
			"stockId",//7
			"stockTotalId",//8
			"stockCode",//9
			"stockName",//10
			"stepCheck",//11
			"quantity",//12
			"actualQuantity",//13
			"detailId",//14
			"numImage"
		};
		Type[] fieldTypes = {
			StandardBasicTypes.LONG,//1
			StandardBasicTypes.STRING,//2
			StandardBasicTypes.STRING,//3
			StandardBasicTypes.LONG,//4
			StandardBasicTypes.STRING,//5
			StandardBasicTypes.STRING,//6
			StandardBasicTypes.LONG,//7
			StandardBasicTypes.LONG,//8
			StandardBasicTypes.STRING,//9
			StandardBasicTypes.STRING,//10
			StandardBasicTypes.INTEGER,//11
			StandardBasicTypes.INTEGER,//12
			StandardBasicTypes.INTEGER,//13
			StandardBasicTypes.LONG,//14
			StandardBasicTypes.INTEGER
		};
		
		if(kPaging == null) {
			return repo.getListByQueryAndScalar(StatisticCheckingVO.class, fieldNames, fieldTypes, SQL.toString(), params);
		} else {
			String countSQL = "select count(1) as count from (" + SQL.toString() + ")";
			return repo.getListByQueryAndScalarPaginated(StatisticCheckingVO.class, fieldNames, fieldTypes, SQL.toString(), countSQL, params, params, kPaging);
		}
	}
	
	/**
	 * list detail
	 */
	@Override
	public List<StatisticCheckingVO> getListDetailShelfTuyen(KPaging<StatisticCheckingVO> kPaging, Long recordId, Long shopId, Long productId, Long stockId, Date checkDate) throws DataAccessException {
		StringBuilder SQL = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		SQL.append("WITH listUnit AS \n");
		SQL.append("  ( SELECT sh.shop_id shopId, sh.shop_code shopCode, sh.shop_name shopName FROM shop sh WHERE sh.shop_id IN \n");
		SQL.append("    (SELECT unit_id FROM equip_statistic_unit WHERE EQUIP_STATISTIC_RECORD_ID = ? \n");
		params.add(recordId);
		SQL.append("    ) \n");
		SQL.append("  ), \n");
		SQL.append("  listEquip AS \n");
		SQL.append("  ( SELECT stock_total.shop_id shopId, product.PRODUCT_ID equipId, product.PRODUCT_CODE equipCode, product.PRODUCT_NAME equipName, warehouse.warehouse_ID stockId, stock_total.STOCK_TOTAL_ID stockTotalId, warehouse.warehouse_CODE stockCode, warehouse.warehouse_NAME stockName FROM product INNER JOIN stock_total ON product.product_id = stock_total.product_id AND stock_total.OBJECT_TYPE = 1 INNER JOIN warehouse ON warehouse.shop_id = stock_total.shop_id and warehouse.warehouse_id = stock_total.warehouse_id WHERE product.PRODUCT_ID IN \n");
		SQL.append("    (SELECT OBJECT_ID FROM equip_statistic_group WHERE STATUS = 1 AND OBJECT_TYPE = 2 and EQUIP_STATISTIC_RECORD_ID = ? \n");
		params.add(recordId);
		SQL.append("    ) AND stock_total.shop_id IN \n");
		SQL.append("    (SELECT shopId FROM listUnit \n");
		SQL.append("    ) \n");
		SQL.append("  ), \n");
		SQL.append("  listShopEquip AS \n");
		SQL.append("  ( SELECT listUnit.shopId, listUnit.shopCode, listUnit.shopName, listEquip.equipId, listEquip.equipCode, listEquip.equipName, listEquip.stockId, listEquip.stockTotalId, listEquip.stockCode, listEquip.stockName FROM listUnit INNER JOIN listEquip ON listUnit.shopId = listEquip.shopId \n");
		SQL.append("  ) \n");
		SQL.append(" SELECT listShopEquip.shopId, listShopEquip.shopCode, listShopEquip.shopName, listShopEquip.equipId, listShopEquip.equipCode, listShopEquip.equipName, listShopEquip.stockId, listShopEquip.stockTotalId, listShopEquip.stockCode, listShopEquip.stockName, to_char(equip_statistic_rec_dtl.STATISTIC_DATE, 'dd/mm/yyyy') checkDate, equip_statistic_rec_dtl.QUANTITY quantity, equip_statistic_rec_dtl.QUANTITY_ACTUALLY actualQuantity, equip_statistic_rec_dtl.EQUIP_STATISTIC_REC_DTL_ID detailId, ");
		SQL.append(" (select count(1) from EQUIP_ATTACH_FILE where EQUIP_ATTACH_FILE.OBJECT_ID = equip_statistic_rec_dtl.EQUIP_STATISTIC_REC_DTL_ID and EQUIP_ATTACH_FILE.OBJECT_TYPE = ?) numImage ");
		params.add(EquipTradeType.INVENTORY.getValue());
		SQL.append(" FROM listShopEquip LEFT JOIN equip_statistic_rec_dtl ON listShopEquip.shopId = equip_statistic_rec_dtl.shop_id AND listShopEquip.stockId = equip_statistic_rec_dtl.OBJECT_STOCK_ID AND listShopEquip.equipId = equip_statistic_rec_dtl.OBJECT_ID AND equip_statistic_rec_dtl.EQUIP_STATISTIC_RECORD_ID = ? ");
		params.add(recordId);
		if(checkDate != null) {
			SQL.append(" and equip_statistic_rec_dtl.STATISTIC_DATE >= ? and equip_statistic_rec_dtl.STATISTIC_DATE < trunc(?) + 1 ");
			params.add(checkDate);
			params.add(checkDate);
		}
		SQL.append(" where 1 = 1 ");
		if (shopId != null && shopId > -1) {
			SQL.append(" and listShopEquip.shopId = ? ");
			params.add(shopId);
		}
		if (productId != null && productId > -1) {
			SQL.append(" and listShopEquip.equipId = ? ");
			params.add(productId);
		}
		if (stockId != null && stockId > -1) {
			SQL.append(" and listShopEquip.stockId = ? ");
			params.add(stockId);
		}

		SQL.append(" order by listShopEquip.shopCode,listShopEquip.equipCode ");
		
		String[] fieldNames = {
			"shopId",//1
			"shopCode",//2
			"shopName",//3
			"equipId",//4
			"equipCode",//5
			"equipName",//6
			"stockId",//7
			"stockTotalId",//8
			"stockCode",//9
			"stockName",//10
			"checkDate",//11
			"quantity",//12
			"actualQuantity",//13
			"detailId",//14
			"numImage"
		};
		Type[] fieldTypes = {
			StandardBasicTypes.LONG,//1
			StandardBasicTypes.STRING,//2
			StandardBasicTypes.STRING,//3
			StandardBasicTypes.LONG,//4
			StandardBasicTypes.STRING,//5
			StandardBasicTypes.STRING,//6
			StandardBasicTypes.LONG,//7
			StandardBasicTypes.LONG,//8
			StandardBasicTypes.STRING,//9
			StandardBasicTypes.STRING,//10
			StandardBasicTypes.STRING,//11
			StandardBasicTypes.INTEGER,//12
			StandardBasicTypes.INTEGER,//13
			StandardBasicTypes.LONG,//14
			StandardBasicTypes.INTEGER
		};
		
		if(kPaging == null) {
			return repo.getListByQueryAndScalar(StatisticCheckingVO.class, fieldNames, fieldTypes, SQL.toString(), params);
		} else {
			String countSQL = "select count(1) as count from (" + SQL.toString() + ")";
			return repo.getListByQueryAndScalarPaginated(StatisticCheckingVO.class, fieldNames, fieldTypes, SQL.toString(), countSQL, params, params, kPaging);
		}
	}
	
	/***
	 * @author vuongmq
	 * @date 25/04/2015
	 * cap nhat: lay danh sach bao mat
	 * search danh sach ben QL bao mat; search danh sach ben phe duuey bao mat
	 */
	@Override
	public List<EquipRecordVO> searchListEquipLostRecordVOByFilter(EquipRecordFilter<EquipRecordVO> filter) throws DataAccessException {
		if (filter.getStaffRoot() == null || filter.getLstShopId() == null) {
			throw new IllegalArgumentException("staffRoot or LstShopId is null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/** * staffRoot và shopCode; LstShopId luôn luôn có*/
		sql.append(" select ");
		sql.append(" elr.equip_lost_record_id as id,  ");
		sql.append(" elr.equip_id as  equipId,  ");
		sql.append(" elr.code as code,  ");
		sql.append(" elr.note as note,  ");
		sql.append(" elr.stock_code as stockCode,  ");
		sql.append(" elr.stock_id as stockId,  ");
		sql.append(" elr.record_status as recordStatus,  ");
		/*sql.append(" to_char(elr.lost_date, 'dd/MM/yyyy') as lostDateStr,  "); 
		sql.append(" elr.lost_date as lostDate,  ");*/
//		sql.append(" to_char(elr.create_date, 'dd/MM/yyyy') as lostDateStr,  "); // vuongmq; 18/05/2015 lay gia tri nay la createDate
		sql.append(" to_char(elr.CREATE_FORM_DATE, 'dd/MM/yyyy') as lostDateStr,  "); // tamvnm; 02/07/2015 lay gia tri nay la createFormDate
		sql.append(" elr.create_date as lostDate,  ");
		sql.append(" to_char(elr.lost_date , 'dd/MM/yyyy') as equipLostDate,  ");// Datpv4; 26/06/2015 ngay mat tu
		/*sql.append(" cus.customer_id as customerId,  ");
		sql.append(" cus.short_code as shortCode,  ");
		sql.append(" cus.customer_name as customerName,  ");
		sql.append(" cus.address as address,  ");*/
		sql.append(" (case when elr.stock_type = 2 ");
		sql.append(" then elr.stock_id ");
		sql.append(" else null ");
		sql.append(" end) as customerId, ");
		sql.append(" (case when elr.stock_type = 2 ");
		sql.append(" then (select cus.short_code from customer cus where cus.customer_id = elr.stock_id) ");
		sql.append(" else null ");
		sql.append(" end) as shortCode, ");
		sql.append(" (case when elr.stock_type = 2 ");
		sql.append(" then (select cus.customer_name from customer cus where cus.customer_id = elr.stock_id) ");
		sql.append(" else null ");
		sql.append(" end) as customerName, ");
		sql.append(" (case when elr.stock_type = 2 ");
		sql.append(" then (select cus.short_code || ' - ' || cus.customer_name from customer cus where cus.customer_id = elr.stock_id) ");
		sql.append(" else null ");
		sql.append(" end) as shortCodeName, ");
		sql.append(" (case when elr.stock_type = 2 ");
		sql.append(" then (select cus.address from customer cus where cus.customer_id = elr.stock_id) ");
		sql.append(" else  ");
		sql.append("   (SELECT sh.address FROM shop sh WHERE sh.shop_id = ");
		sql.append("   		(SELECT shop_id FROM equip_stock WHERE equip_stock_id = elr.stock_id) ) ");
		sql.append(" end) as address, ");
		/** end customer */
		/*sql.append(" sh.shop_id as shopId,  ");
		sql.append(" sh.shop_code as shopCode,  ");
		sql.append(" sh.shop_name as shopName,  ");
		sql.append(" (sh.shop_code ||' - '||sh.shop_name) as shopCodeName,  ");*/
		sql.append(" (case when elr.stock_type = 1 ");
		sql.append(" then (select sh.shop_id from shop sh where sh.shop_id = (select shop_id from equip_stock where equip_stock_id = elr.stock_id)) ");
		sql.append(" else ");
		sql.append(" (SELECT cus.shop_id FROM customer cus WHERE cus.customer_id = elr.stock_id) ");
		sql.append(" end) as shopId, ");
		sql.append(" (case when elr.stock_type = 1 ");
		sql.append(" then (select sh.shop_code from shop sh where sh.shop_id = (select shop_id from equip_stock where equip_stock_id = elr.stock_id)) ");
		sql.append(" else ");
		sql.append("   (SELECT sh.shop_code ");
		sql.append("   FROM shop sh ");
		sql.append("   WHERE sh.shop_id = ");
		sql.append("     (SELECT cus.shop_id FROM customer cus WHERE cus.customer_id = elr.stock_id) ) ");
		sql.append(" end) as shopCode, ");
		sql.append(" (case when elr.stock_type = 1 ");
		sql.append(" then (select sh.shop_name from shop sh where sh.shop_id = (select shop_id from equip_stock where equip_stock_id = elr.stock_id)) ");
		sql.append(" else ");
		sql.append("   (SELECT sh.shop_name ");
		sql.append("   FROM shop sh ");
		sql.append("   WHERE sh.shop_id = ");
		sql.append("     (SELECT cus.shop_id FROM customer cus WHERE cus.customer_id = elr.stock_id) ) ");
		sql.append(" end) as shopName, ");
		sql.append(" (case when elr.stock_type = 1 ");
		sql.append(" then (select (sh.shop_code ||' - '|| sh.shop_name) from shop sh where sh.shop_id = (select shop_id from equip_stock where equip_stock_id = elr.stock_id)) ");
		sql.append(" else ");
		sql.append("   (SELECT (sh.shop_code ||' - '|| sh.shop_name) ");
		sql.append("   FROM shop sh ");
		sql.append("   WHERE sh.shop_id = ");
		sql.append("     (SELECT cus.shop_id FROM customer cus WHERE cus.customer_id = elr.stock_id) ) ");
		sql.append(" end) as shopCodeName, ");
		/** end Shop*/
		sql.append(" eq.serial as serial,  ");
		sql.append(" eq.code as equipCode,  ");
		sql.append(" elr.price_actually as priceActually, "); // vuongmq; 12/05/2015; export
		sql.append(" elr.representor as representor,  ");
		sql.append(" to_char(elr.last_arising_sales_date, 'dd/MM/yyyy') as lastArisingSalesDateStr,  ");
		sql.append(" elr.conclusion as conclusion,  ");
		sql.append(" (case when elr.tracing_result = 0 then 'Tìm được khách hàng nhưng không thấy tủ' when elr.tracing_result = 1 then 'Không tìm thấy khách hàng' else '' end) as tracingResultStr,  ");
		sql.append(" (case when elr.tracing_place = 0 then 'Tại địa chỉ kinh doanh' when elr.tracing_place = 1 then 'Tại địa chỉ thường trú' else '' end) as tracingPlaceStr,  ");
		sql.append(" elr.recommended_treatment as recommendedTreatment,  ");
		sql.append(" elr.stock_type as stockType,  ");
		sql.append(" elr.delivery_status as deliveryStatus, ");
		sql.append("(select url from equip_attach_file where ");
		sql.append("	object_id = elr.equip_lost_record_id and rownum = 1 and object_type = ?) as url "); // lay them Url de kiem tra tap tin
		params.add(EquipTradeType.NOTICE_LOST.getValue());
		/**end select; */
		fromSql.append(" from equip_lost_record elr   ");
		//fromSql.append(" join customer cus on elr.stock_id = cus.customer_id  ");
//		fromSql.append(" join equip_period ep on ep.equip_period_id = elr.equip_period_id  ");
		fromSql.append(" join equipment eq on eq.equip_id = elr.equip_id  ");
		//fromSql.append(" join shop sh on cus.shop_id = sh.shop_id  ");
		fromSql.append(" where 1 = 1   ");
		//Bo sung phan quyen
		/*fromSql.append(" and sh.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? and status = 1 connect by prior shop_id = parent_shop_id)  ");
		params.add(filter.getShopRootId());*/

		if (filter.getStockType() != null && filter.getStockType() > ActiveType.DELETED.getValue()) {
			fromSql.append(" and elr.stock_type = ? ");
			params.add(filter.getStockType());
			/** vuongmq; 11/05/2015; lay theo kho cua shop_id; hay customer_id ; khong theo phan quyen */
			if (EquipStockTotalType.KHO.getValue().equals(filter.getStockType())) {
				fromSql.append(" AND elr.stock_id IN (SELECT equip_stock_id from equip_stock ");
				fromSql.append(" where 1 = 1 ");
				fromSql.append(" and status = 1 ");
				fromSql.append(" and shop_id in (  ");
				fromSql.append(" SELECT shop_id ");
				fromSql.append(" FROM shop ");
				fromSql.append(" WHERE status = 1 ");
				fromSql.append(" START WITH shop_id IN (? ");
				params.add(filter.getLstShopId().get(0));
				for (int i = 1, size = filter.getLstShopId().size(); i < size; i++) {
					fromSql.append(" ,?  ");
					params.add(filter.getLstShopId().get(i));
				}
				fromSql.append("  ) AND status = 1 ");
				fromSql.append(" CONNECT BY prior shop_id = parent_shop_id) ");
				fromSql.append(" ) ");
			} else {
				//fromSql.append(" and elr.stock_id IN ( select cus.customer_id from customer cus where cus.shop_id = ?) ");
				//params.add(filter.getShopRootId());
				fromSql.append(" AND elr.stock_id IN (SELECT cus.customer_id ");
				fromSql.append(" FROM customer cus ");
				fromSql.append(" WHERE 1 = 1 ");
//				fromSql.append(" and cus.status = 1 ");
				fromSql.append(" and cus.shop_id IN ");
				fromSql.append(" (SELECT shop_id ");
				fromSql.append(" FROM shop ");
				fromSql.append(" WHERE status = 1 ");
				fromSql.append(" START WITH shop_id IN (? ");
				params.add(filter.getLstShopId().get(0));
				for (int i = 1, size = filter.getLstShopId().size(); i < size; i++) {
					fromSql.append(" ,?  ");
					params.add(filter.getLstShopId().get(i));
				}
				fromSql.append("  ) AND status = 1 ");
				fromSql.append(" CONNECT BY prior shop_id = parent_shop_id) ");
				fromSql.append(" ) ");
			}
		} else {
			// tim kiem tat ca stock_type
			/**bao mat:  su dung kho NPP: 1; kho KH: 2; du la kho NPP hay KH  Ko theo phan quyen */
			fromSql.append(" AND ( (elr.stock_id IN (SELECT equip_stock_id from equip_stock ");
			fromSql.append(" where 1 = 1 ");
			fromSql.append(" and status = 1 ");
			fromSql.append(" and shop_id in (  ");
			fromSql.append(" SELECT shop_id ");
			fromSql.append(" FROM shop ");
			fromSql.append(" WHERE status = 1 ");
			fromSql.append(" START WITH shop_id IN (? ");
			params.add(filter.getLstShopId().get(0));
			for (int i = 1, size = filter.getLstShopId().size(); i < size; i++) {
				fromSql.append(" ,?  ");
				params.add(filter.getLstShopId().get(i));
			}
			fromSql.append("  ) AND status = 1 ");
			fromSql.append(" CONNECT BY prior shop_id = parent_shop_id) ");
			fromSql.append(" ) ");
			fromSql.append(" AND ELR.STOCK_TYPE = ?) ");
			params.add(EquipStockTotalType.KHO.getValue());

			/*** or trong tiep lay theo KH: 2 */
			fromSql.append(" or ( elr.stock_id IN (SELECT cus.customer_id ");
			fromSql.append(" FROM customer cus ");
			fromSql.append(" WHERE 1 = 1 ");
//			fromSql.append(" and cus.status = 1 ");
			fromSql.append(" and cus.shop_id IN ");
			fromSql.append(" (SELECT shop_id ");
			fromSql.append(" FROM shop ");
			fromSql.append(" WHERE status = 1 ");
			fromSql.append(" START WITH shop_id IN (? ");
			params.add(filter.getLstShopId().get(0));
			for (int i = 1, size = filter.getLstShopId().size(); i < size; i++) {
				fromSql.append(" ,?  ");
				params.add(filter.getLstShopId().get(i));
			}
			fromSql.append("  ) AND status = 1 ");
			fromSql.append(" CONNECT BY prior shop_id = parent_shop_id) ");
			fromSql.append(" ) ");
			fromSql.append(" AND ELR.STOCK_TYPE = ?) ");
			params.add(EquipStockTotalType.KHO_KH.getValue());
			fromSql.append(" ) ");
		}

		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			fromSql.append(" and upper(elr.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toUpperCase()));
		}
		/*if (filter.getShopId() != null) {
			//fromSql.append(" and sh.shop_id = ?  ");
			*//** vuongmq; 11/05/2015; lay theo kho cua shop_id*//*
			fromSql.append(" AND elr.stock_id IN ");
			fromSql.append(" (select equip_stock_id from equip_stock where shop_id in (?)) ");
			params.add(filter.getShopId());
		}*/
		/*if (filter.getLstShopId() != null && !filter.getLstShopId().isEmpty()) {
			fromSql.append(" and sh.shop_id in (select shop_id from shop where status = 1 start with shop_id in ( ?  ");
			params.add(filter.getLstShopId().get(0));
			for (int i = 1, size = filter.getLstShopId().size(); i < size; i++) {
				sql.append(" ,?  ");
				params.add(filter.getLstShopId().get(i));
			}
			fromSql.append(" ) and status = 1 connect by prior shop_id = parent_shop_id) ");
		}*/
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			//fromSql.append(" and cus.short_code like ? ESCAPE '/' ");
			fromSql.append(" and elr.stock_id IN ( select cus.customer_id from customer cus where (cus.short_code like ? ESCAPE '/' or upper(cus.name_text) like ? ESCAPE '/')) ");
			String customerCode = Unicode2English.codau2khongdau(filter.getShortCode().trim().toUpperCase());
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSerial())) {
			fromSql.append(" and lower(eq.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSerial().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			fromSql.append(" and lower(eq.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipCode().trim().toLowerCase()));
		}
//		if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
//			//fromSql.append(" and lower(cus.name_text) like ? ESCAPE '/' ");
//			fromSql.append(" and elr.stock_id IN ( select cus.customer_id from customer cus where lower(cus.name_text) like ? ESCAPE '/') ");
//			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getCustomerName().trim()).toLowerCase()));
//
//		}
//		if (!StringUtility.isNullOrEmpty(filter.getAddress())) {
//			//fromSql.append(" and lower(cus.name_text) like ? ESCAPE '/' ");
//			fromSql.append(" and elr.stock_id IN ( select cus.customer_id from customer cus where lower(cus.name_text) like ? ESCAPE '/') ");
//			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getAddress().trim().toLowerCase())));
//		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerSearch())) {
			String customerSearch = Unicode2English.codau2khongdau(filter.getCustomerSearch().toUpperCase());
			fromSql.append(" and elr.stock_id IN (select customer_id from customer where short_code like ? ESCAPE '/' or upper(name_text) like ? ESCAPE '/') ");
			params.add(StringUtility.toOracleSearchLikeSuffix(customerSearch));
			params.add(StringUtility.toOracleSearchLikeSuffix(customerSearch));
		}
		// vuongmq; 18/05/2015 lay gia tri nay la createDate
		if (filter.getFromDate() != null) {
			//fromSql.append(" and trunc(elr.lost_date) >= trunc(?) ");
//			fromSql.append(" and trunc(elr.create_date) >= trunc(?) ");
			//tamvnm: 13/07/2015 - Thay doi tim kiem theo ngay lap
			fromSql.append(" and trunc(elr.create_form_date) >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			//fromSql.append(" and trunc(elr.lost_date) < trunc(?) + 1 ");
//			fromSql.append(" and trunc(elr.create_date) < trunc(?) + 1 ");
			//tamvnm: 13/07/2015 - Thay doi tim kiem theo ngay lap
			fromSql.append(" and trunc(elr.create_form_date) < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if (filter.getRecordStatus() != null) {
			fromSql.append(" and elr.record_status = ?  ");
			params.add(filter.getRecordStatus());
			if (StatusRecordsEquip.WAITING_APPROVAL.getValue().equals(filter.getFlagRecordStatus())) { // equal voi filter.getFlagRecordStatus() ben MH phe duyet de lay thiet bi
				fromSql.append(" and eq.liquidation_status in (?, ?)  ");
				params.add(LinquidationStatus.NO_LIQUIDATION.getValue());
				params.add(LinquidationStatus.FINISH_LIQUIDATION.getValue());
			}
		}
		/*if (filter.getIsLevel() != null && filter.getIsLevel().equals(ShopDecentralizationSTT.VNM.getValue())) {
			fromSql.append(" and elr.record_status not in (0, 4)  ");
		}*/
		if (filter.getDeliveryStatus() != null) {
			fromSql.append(" and elr.delivery_status = ?  ");
			params.add(filter.getDeliveryStatus());
		}
		sql.append(fromSql.toString());
		//sql.append(" order by elr.lost_date desc, elr.code  ");
		sql.append(" order by shopCode, lostDate desc, code desc ");
		

		String[] fieldNames = { "id", "equipId", "code", "note", "stockCode", "stockId", //1 
							"recordStatus", "lostDateStr",  //2
							"customerId", "shortCode", "customerName", "shortCodeName", "address", //3 
							"shopId", "shopCode", "shopName", "shopCodeName",  //4
							"serial", "equipCode", "priceActually",//5
							"representor", "lastArisingSalesDateStr", //6 
							"conclusion", "tracingResultStr", "tracingPlaceStr", //7 
							"recommendedTreatment", "stockType",  //8
							"deliveryStatus", "lostDate", //9 
							"url", //10
							"equipLostDate"//11
							};
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, //1
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, //2
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, //3 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,  //4
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, //5
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,  //6
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, //7 
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, //8
				StandardBasicTypes.INTEGER, StandardBasicTypes.DATE,  //9
				StandardBasicTypes.STRING, //10
				StandardBasicTypes.STRING//11
				};
		/*StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count ");
		countSql.append(fromSql.toString());*/
		if (filter.getkPaging() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count  from (");
			countSql.append(sql).append(" ) ");
			return repo.getListByQueryAndScalarPaginated(EquipRecordVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipRecordVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<FileVO> searchListFileVOByFilter(EquipRecordFilter<FileVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select   ");
		sql.append(" equip_attach_file_id as fileId,  ");
		sql.append(" file_name as fileName,  ");
		sql.append(" url,  ");
		sql.append(" thumb_url as thumbUrl  ");
		sql.append(" from equip_attach_file  ");
		sql.append(" where 1 = 1  ");
		if (filter.getObjectId() != null) {
			sql.append(" and object_id = ?  ");
			params.add(filter.getObjectId());

		}
		if (filter.getObjectType() != null) {
			sql.append(" and object_type = ?  ");
			params.add(filter.getObjectType());

		}
		String[] fieldNames = { "fileId", "fileName", "url", "thumbUrl" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count ");
		countSql.append(fromSql.toString());
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(FileVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(FileVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	///////////////////////////////// BEGIN VUONGMQ; 04/05/2015///////////////////
	/**
	 * Tim kiem danh sach thiet bi bien ban sua chua, 
	 * lay theo phan quyen; popup thiet bi
	 * @author vuongmq
	 * @date 04/05/2015
	 */
	@Override
	public List<EquipmentDeliveryVO> getListEquipmentLostPopUp(EquipmentFilter<EquipmentDeliveryVO> filter) throws DataAccessException {
		if (filter.getStaffRoot() == null || filter.getShopId() == null) {
			throw new IllegalArgumentException("staffRoot or shopId is null");
		}
		StringBuilder sqlKhoNPP = new StringBuilder();
		StringBuilder sql = new StringBuilder();
		StringBuilder whereSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/** * staffRoot và shopCode; shopId luôn luôn có*/
		sqlKhoNPP.append(" with t_kho_under as ");
		//sqlKhoNPP.append(" (SELECT es.shop_id ");
		sqlKhoNPP.append(" (SELECT es.equip_stock_id AS id, "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" es.shop_id AS shopId, "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" es.code AS code, "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" es.name AS name "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" FROM equip_role_user eru ");
		sqlKhoNPP.append(" JOIN equip_role_detail erd ");
		sqlKhoNPP.append(" ON eru.equip_role_id = erd.equip_role_id ");
		sqlKhoNPP.append(" JOIN equip_stock es ");
		sqlKhoNPP.append(" ON es.equip_stock_id = erd.equip_stock_id ");
		sqlKhoNPP.append(" WHERE 1 = 1 ");
		sqlKhoNPP.append(" AND es.status = 1 ");
		sqlKhoNPP.append(" AND erd.is_under = 1 ");
		sqlKhoNPP.append(" AND eru.status = 1 ");
		sqlKhoNPP.append(" AND eru.user_id = ? ");
		sqlKhoNPP.append(" ), ");
		params.add(filter.getStaffRoot());
		sqlKhoNPP.append(" stock_under AS ");
		sqlKhoNPP.append(" (SELECT equip_stock_id AS id, ");
		sqlKhoNPP.append(" es.shop_id as shopId, ");
		sqlKhoNPP.append(" es.code AS code, ");
		sqlKhoNPP.append(" es.name AS name ");
		sqlKhoNPP.append(" FROM equip_stock es ");
		sqlKhoNPP.append(" WHERE 1 =1 ");
		sqlKhoNPP.append(" AND es.status = 1 ");
		sqlKhoNPP.append(" AND es.shop_id IN ");
		sqlKhoNPP.append(" (SELECT sh.shop_id ");
		sqlKhoNPP.append(" FROM shop sh ");
		sqlKhoNPP.append(" WHERE sh.status = 1 ");
		sqlKhoNPP.append(" and sh.shop_id not in (SELECT shopId FROM t_kho_under) "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" START WITH sh.shop_id IN (SELECT shopId FROM t_kho_under ) ");
		sqlKhoNPP.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sqlKhoNPP.append(" ) ");
		sqlKhoNPP.append(" union "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" (SELECT * FROM t_kho_under) "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sqlKhoNPP.append(" ), ");
		sqlKhoNPP.append(" t_kho_nounder AS ");
		sqlKhoNPP.append(" (SELECT es.equip_stock_id AS id, ");
		sqlKhoNPP.append(" es.shop_id AS shopId, ");
		sqlKhoNPP.append(" es.code AS code, ");
		sqlKhoNPP.append(" es.name AS name ");
		sqlKhoNPP.append(" FROM equip_role_user eru ");
		sqlKhoNPP.append(" JOIN equip_role_detail erd ");
		sqlKhoNPP.append(" ON eru.equip_role_id = erd.equip_role_id ");
		sqlKhoNPP.append(" JOIN equip_stock es ");
		sqlKhoNPP.append(" ON es.equip_stock_id = erd.equip_stock_id ");
		sqlKhoNPP.append(" WHERE 1 = 1 ");
		sqlKhoNPP.append(" AND es.status = 1 ");
		sqlKhoNPP.append(" AND erd.is_under = 0 ");
		sqlKhoNPP.append(" AND eru.status = 1 ");
		sqlKhoNPP.append(" AND eru.user_id = ? ");
		sqlKhoNPP.append(" ) , ");
		params.add(filter.getStaffRoot());
		sqlKhoNPP.append(" lstStockShop AS ");
		sqlKhoNPP.append(" (SELECT id, code, name, shopId FROM stock_under ");
		sqlKhoNPP.append(" UNION ");
		sqlKhoNPP.append(" SELECT id, code, name, shopId FROM t_kho_nounder ");
		sqlKhoNPP.append(" ) , ");
		sqlKhoNPP.append(" lstStockShopView as ");
		sqlKhoNPP.append(" (select * from lstStockShop where shopId in ");
		sqlKhoNPP.append("    (select s.shop_id from Shop s where s.shop_id in (?) ) ");
		sqlKhoNPP.append(" ) ");
		params.add(filter.getShopId());
		/** end danh sách kho cho NPP */
		/**bao mat:  su dung kho NPP: 1; kho KH: 2; truong hop 1:NPP thi lay kho in trong shop Chon shopId nua */
		if (filter.getStockType() != null) {
			if (EquipStockTotalType.KHO.getValue().equals(filter.getStockType())) {
				sql.append(sqlKhoNPP);
			} else {
				// kho KH khong xai sqlKhoNPP
				params = new ArrayList<Object>();
			}
		}
		/**** select VO ***/
		sql.append(" SELECT DISTINCT eq.equip_id AS equipmentId, ");
		sql.append(" eq.code AS equipmentCode, ");
		sql.append(" eq.serial AS seriNumber, ");
		sql.append(" ( ");
		sql.append(" CASE ");
		sql.append(" WHEN eq.health_status IS NOT NULL ");
		sql.append(" THEN ");
		sql.append(" (SELECT MIN(value) ");
		sql.append(" FROM ap_param ap ");
		sql.append(" WHERE ap.status = 1 ");
		sql.append(" AND ap.ap_param_code LIKE eq.health_status ");
		sql.append(" AND type LIKE 'EQUIP_CONDITION' ");
		sql.append(" ) ");
		sql.append(" ELSE null ");
		sql.append(" END) AS healthStatus, ");
		sql.append(" eq.stock_code AS stockCode, ");
		sql.append(" eq.stock_name   AS stockName, ");
		/*** end select stockName */
		sql.append(" ec.name AS typeEquipment, ");
		sql.append(" eg.code AS groupEquipmentCode, ");
		sql.append(" eg.name AS groupEquipmentName, ");
		sql.append(" ( ");
		sql.append(" CASE ");
		sql.append(" WHEN eg.capacity_from IS NULL ");
		sql.append(" AND eg.capacity_to IS NULL ");
		sql.append(" THEN '' ");
		sql.append(" WHEN eg.capacity_from IS NULL ");
		sql.append(" THEN '<' ");
		sql.append(" ||TO_CHAR(eg.capacity_to) ");
		sql.append(" WHEN eg.capacity_to IS NULL ");
		sql.append(" THEN '>' ");
		sql.append(" ||TO_CHAR(eg.capacity_from) ");
		sql.append(" WHEN eg.capacity_to <> eg.capacity_from ");
		sql.append(" THEN TO_CHAR(eg.capacity_from) ");
		sql.append(" || ' - ' ");
		sql.append(" || TO_CHAR(eg.capacity_to) ");
		sql.append(" ELSE TO_CHAR(eg.capacity_from) ");
		sql.append(" END) AS capacity, ");
		sql.append(" eg.brand_name AS equipmentBrand, ");
		sql.append(" ep.name AS equipmentProvider, ");
		sql.append(" eq.manufacturing_year AS yearManufacture, ");
		sql.append(" eq.liquidation_status as liquidationStatus, ");
		sql.append(" TO_CHAR(eq.warranty_Expired_Date,'dd/mm/yyyy') AS warrantyExpiredDate, ");
		sql.append(" (SELECT COUNT(1)+1 ");
		sql.append(" FROM equip_repair_form erf ");
		sql.append(" WHERE erf.equip_id = eq.equip_id ");
		sql.append(" AND erf.status = ? ");
		params.add(StatusRecordsEquip.COMPLETION_REPAIRS.getValue());
		sql.append(" ) repairCount, ");
		if (filter.getIdRecordDelivery() != null) {
			sql.append(" edrdt.content AS contentDelivery ");
		} else {
			sql.append(" null AS contentDelivery ");
		}
		/*** from cac table*/
		sql.append(" FROM equipment eq ");
		sql.append(" JOIN equip_group eg ON eg.equip_group_id = eq.equip_group_id ");
		sql.append(" JOIN equip_category ec ON eg.equip_category_id = ec.equip_category_id ");
		//sql.append(" join equip_brand eb on eb.equip_brand_id = eg.equip_brand_id ");
		sql.append(" join equip_provider ep on ep.equip_provider_id = eq.equip_provider_id ");
		if (filter.getIdRecordDelivery() != null) {
			sql.append(" join equip_delivery_rec_dtl edrdt on edrdt.equip_id = eq.equip_id ");
		}
		/** begin cac dieu kien where macdinh */
		sql.append(" WHERE 1 =1 ");
		sql.append(" AND ec.status = 1 ");
		sql.append(" AND eg.status = 1 ");
		sql.append(" AND ep.status = 1 ");
		sql.append(" AND eq.trade_type IS NULL ");
		/*sql.append(" and ( (eq.stock_id IN (select t.id from lstStockShop t) and eq.stock_type = ?) ");
		params.add(EquipStockTotalType.KHO.getValue()); *//** kho cua don vi; id equip_stock*//*
		sql.append(" or (eq.stock_id IN (select tmp.id from t_kho_customer tmp) and eq.stock_type = ?) ");
		params.add(EquipStockTotalType.KHO_KH.getValue()); *//** kho cua kh*//*
		sql.append(" ) ");*/
		/** end cac dieu kien where macdinh */
		if (filter.getLstEquipAdd() != null) {
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
				code += ",?";
			}
			sql.append(" and eq.code not in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
				params.add(filter.getLstEquipAdd().get(i));
			}
		}
		if (filter.getFlagUpdate() != null && filter.getFlagUpdate() == 0) {
			whereSql.append(" and eq.liquidation_status is null ");
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			whereSql.append(" and eq.code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
			whereSql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCategoryCode())) {
			whereSql.append(" and upper(ec.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCategoryCode().toUpperCase()));
		}
		if (filter.getEquipCategoryId() != null) {
			whereSql.append(" and ec.equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getGroupCode())) {
			whereSql.append(" and upper(eg.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getGroupCode().toUpperCase()));
		}
		if (filter.getEquipGroupId() != null) {
			whereSql.append(" and eg.equip_group_id = ? ");
			params.add(filter.getEquipGroupId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getProviderCode())) {
			whereSql.append(" and upper(ep.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProviderCode().toUpperCase()));
		}
		if (filter.getEquipProviderId() != null) {
			whereSql.append(" and ep.equip_provider_id = ? ");
			params.add(filter.getEquipProviderId());
		}
		if (filter.getYearManufacture() != null) {
			whereSql.append(" and eq.manufacturing_year = ? ");
			params.add(filter.getYearManufacture());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			whereSql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
		}
		/**bao mat:  su dung kho NPP: 1; kho KH: 2 */
		if (filter.getStockType() != null) {
			whereSql.append(" and eq.stock_type = ? ");
			params.add(filter.getStockType());
			/** vuongmq; 08/05/2015; chi lay TB dang o kho: 1 voi loai kho: NPP; dang su dung: 2 voi loai kho la KH
			* va giao dich Binh thuong: 0
			*/
			if (EquipStockTotalType.KHO.getValue().equals(filter.getStockType())) {
				// bao mat NPP
				/*whereSql.append(" and eq.stock_id IN (select t.id from lstStockShop t) ");
				whereSql.append(" and eq.stock_id IN (select es.equip_stock_id from equip_stock es where es.shop_id = ?) ");
				params.add(filter.getShopId());*/
				whereSql.append(" and eq.stock_id IN (select id from lstStockShopView)  ");
				
				whereSql.append(" and eq.usage_status in (?) ");
				params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
			} else {
				// bao mat KH
				//whereSql.append(" and eq.stock_id IN (select tmp.id from t_kho_customer tmp) ");
				whereSql.append(" and eq.stock_id IN (?) ");
				params.add(filter.getStockId());
				
				whereSql.append(" and eq.usage_status in (?) ");
				params.add(EquipUsageStatus.IS_USED.getValue()); 
			}
			
		} else if (filter.getIdRecordDelivery() == null) {
			/*whereSql.append(" and eq.stock_type != ? ");
			params.add(EquipStockTotalType.KHO_KH.getValue());*/
		}
		if (filter.getStatusEquip() != null) {
			whereSql.append(" and eq.status = ? ");
			params.add(filter.getStatusEquip());
		} else {
			whereSql.append(" and eq.status != ? ");
			params.add(StatusType.DELETED.getValue());
		}
		if (filter.getTradeStatus() != null) {
			whereSql.append(" and eq.trade_status = ?");
			params.add(filter.getTradeStatus());
		}
		//if (filter.getUsageStatus() != null) {
		/*whereSql.append(" and eq.usage_status in (?)");
		params.add(EquipUsageStatus.IS_USED.getValue()); 
		params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());*/
		//}
		if (filter.getIdRecordDelivery() != null) {
			whereSql.append(" and edrdt.equip_delivery_rec_dtl_id in ");
			whereSql.append(" (select edrd.equip_delivery_rec_dtl_id from equip_delivery_rec_dtl edrd ");
			whereSql.append(" join equip_delivery_record edr on edr.equip_delivery_record_id = edrd.equip_delivery_record_id ");
			whereSql.append(" where 1=1 ");
			whereSql.append(" and edr.equip_delivery_record_id = ?) ");
			params.add(filter.getIdRecordDelivery());
		}
		String sqlTemp = sql.toString();
		sql.append(whereSql);
		//////////**** them cac thiet bi xoa tam tren danh sach ********//////
		if (filter.getLstEquipDelete() != null) {
			sql.append(" union ");
			sql.append(sqlTemp);
			if (filter.getLstEquipAdd() != null) {
				for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
					params.add(filter.getLstEquipAdd().get(i));
				}
			}
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipDelete().size(); i++) {
				code += ",?";
			}
			sql.append(" and eq.code in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipDelete().size(); i++) {
				params.add(filter.getLstEquipDelete().get(i));
			}
			if (!StringUtility.isNullOrEmpty(filter.getCode())) {
				sql.append(" and upper(eq.code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
				sql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getCategoryCode())) {
				sql.append(" and upper(ec.code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCategoryCode().toUpperCase()));
			}
			if (filter.getEquipCategoryId() != null) {
				sql.append(" and ec.equip_category_id = ? ");
				params.add(filter.getEquipCategoryId());
			}
			if (!StringUtility.isNullOrEmpty(filter.getGroupCode())) {
				sql.append(" and upper(eg.code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getGroupCode().toUpperCase()));
			}
			if (filter.getEquipGroupId() != null) {
				sql.append(" and eg.equip_group_id = ? ");
				params.add(filter.getEquipGroupId());
			}
			if (!StringUtility.isNullOrEmpty(filter.getProviderCode())) {
				sql.append(" and upper(ep.code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProviderCode().toUpperCase()));
			}
			if (filter.getEquipProviderId() != null) {
				sql.append(" and ep.equip_provider_id = ? ");
				params.add(filter.getEquipProviderId());
			}
			if (filter.getYearManufacture() != null) {
				sql.append(" and eq.manufacturing_year = ? ");
				params.add(filter.getYearManufacture());
			}
			if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
				sql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
			}
			if (filter.getStatusEquip() != null) {
				sql.append(" and eq.status = ? ");
				params.add(filter.getStatusEquip());
			} else {
				sql.append(" and eq.status != ? ");
				params.add(StatusType.DELETED.getValue());
			}
		}
		/**vuongmq; 16/04/2015; lay danh sach thiet bi het han*/
		//sql.append(" and eq.warranty_Expired_Date < sysdate ");
		sql.append(" ORDER BY equipmentCode ASC  ");
		String[] fieldNames = { "equipmentId",// 0
				"equipmentCode",// 1
				"seriNumber",// 2
				"healthStatus",// 3
				"stockCode",// 4
				"stockName",// 5
				"typeEquipment",// 6
				"groupEquipmentCode",// 7
				"groupEquipmentName",// 8
				"capacity",// 9
				"equipmentBrand",// 10
				"equipmentProvider",// 11
				"yearManufacture",// 12
				"warrantyExpiredDate", // 12.1 /** 06/04/2015; vuongmq; ngay het han bh*/
				"repairCount",// 13
				"contentDelivery",// 14
				"liquidationStatus"
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.STRING,// 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.STRING, // 12.1
				StandardBasicTypes.INTEGER,//13
				StandardBasicTypes.INTEGER, // 14
				StandardBasicTypes.INTEGER // 15
		};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*) as count from ( ").append(sql).append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	/***
	 * @author vuongmq
	 * @date 04/05/2015
	 * cap nhat  lay kho popup cua Thiet bi, theo phan quyen
	 */
	@Override
	public List<EquipStockVO> getListStockLostByFilter(EquipStockFilter filter) throws DataAccessException {
		if (filter.getStaffRoot() == null || filter.getShopId() == null) {
			throw new IllegalArgumentException("staffRoot or shopId not id");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH t_kho_under AS ");
		//sql.append(" (SELECT es.shop_id ");
		sql.append(" (SELECT es.equip_stock_id AS id, "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sql.append(" es.shop_id AS shopId, "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sql.append(" sh.shop_code as shopCode, "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sql.append(" sh.shop_name as shopName,  "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sql.append(" es.code as stockCode "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sql.append(" FROM equip_role_user eru ");
		sql.append(" JOIN equip_role_detail erd ON eru.equip_role_id = erd.equip_role_id ");
		sql.append(" JOIN equip_stock es ON es.equip_stock_id = erd.equip_stock_id ");
		sql.append(" JOIN shop sh ON sh.shop_id = es.shop_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND es.status = 1 ");
		sql.append(" AND eru.status = 1 ");
		sql.append(" AND erd.is_under = 1  "); /** co quan ly don vi cap duoi: 1*/
		sql.append(" AND eru.user_id = ? ");
		params.add(filter.getStaffRoot());
		sql.append(" ), ");
		sql.append(" stock_under as ( ");
		sql.append(" select equip_stock_id as id, ");
		sql.append(" sh.shop_id as shopId, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName, ");
		sql.append(" es.code as stockCode ");
		sql.append(" from equip_stock es ");
		sql.append(" join shop sh on sh.shop_id = es.shop_id ");
		sql.append(" where 1=1 ");
		sql.append(" and es.status = 1 ");
		sql.append(" and sh.status = 1 ");
		sql.append(" and es.shop_id in ( ");
		sql.append(" select sh.shop_id ");
		sql.append(" FROM shop sh ");
		sql.append(" WHERE sh.status = 1 ");
		sql.append(" and sh.shop_id not in (SELECT shopId FROM t_kho_under) "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sql.append(" START WITH sh.shop_id IN (SELECT shopId FROM t_kho_under) ");
		sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ");
		sql.append(" ) ");
		sql.append(" union "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sql.append(" (SELECT * FROM t_kho_under) "); // vuongmq; 18/05/2015; fix lay kho cua isUnder phan quyen
		sql.append(" ), ");
		sql.append(" t_kho_nounder as ");
		sql.append(" (SELECT es.equip_stock_id as id, ");
		sql.append(" sh.shop_id as shopId, ");
		sql.append(" sh.shop_code as shopCode, ");
		sql.append(" sh.shop_name as shopName, ");
		sql.append(" es.code as stockCode ");
		sql.append(" FROM equip_role_user eru ");
		sql.append(" JOIN equip_role_detail erd ON eru.equip_role_id = erd.equip_role_id ");
		sql.append(" JOIN equip_stock es ON es.equip_stock_id = erd.equip_stock_id ");
		sql.append(" join shop sh on sh.shop_id = es. shop_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND es.status = 1 ");
		sql.append(" AND eru.status = 1 ");
		sql.append(" AND erd.is_under = 0  ");/** khong quan ly don vi cap duoi: 0*/
		sql.append(" AND eru.user_id = ? ");
		params.add(filter.getStaffRoot());
		sql.append(" ), ");
		/** end danh sach kho cua shop*/
		sql.append(" lstStockShop as ");
		sql.append("   (select id, shopId, shopCode, shopName, stockCode from stock_under ");
		sql.append("  union ");
		sql.append("   select id, shopId, shopCode, shopName, stockCode from t_kho_nounder ");
		sql.append(" ), ");
		sql.append(" lstStockShopView as ");
		sql.append(" (select * from lstStockShop where shopId in ");
		sql.append("    (select s.shop_id from Shop s where s.shop_id in (?) ) ");
		sql.append(" ) ");
		params.add(filter.getShopId());
		/** select VO */
		sql.append(" select * from lstStockShopView t ");
		sql.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("        and t.shopCode like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			// ham trong DB function: unicode2english(customer_name) -> khong dau lower
			sql.append("        and unicode2english(t.shopName) like ? ESCAPE '/' ");
			//params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopName().toLowerCase()));
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getShopName()).toLowerCase()));
		}
		/** 	Nếu là báo mất NPP: thì phải đang thuộc kho NPP mà người dùng được phân quyền và phải thuộc kho NPP đăng nhập. */
		/*sql.append("        and t.id in (select es.equip_stock_id from equip_stock es where es.shop_id = ?) ");
		params.add(filter.getShopRoot());*/
		sql.append(" order by shopCode");
		String[] fieldNames = { "shopCode",// 0
				"shopName",// 1
				"stockCode"// 5
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING // 5
		};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*) as count from ( ").append(sql).append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipStockVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipStockVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	/////////////////////////////// END VUONGMQ; 04/05/2015///////////////////
	
	@Override
	public List<EquipRecordVO> printfEquipLostRecordVOByFilter(EquipRecordFilter<EquipRecordVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct  ");
		sql.append("  elr.equip_lost_record_id as id,    ");
		sql.append("  elr.code as code,    ");
		sql.append("  elr.record_status as recordStatus,   ");
		sql.append("  ('Ngày '||to_char(elr.lost_date, 'dd') ||' Tháng '||to_char(elr.lost_date, 'MM')|| ' Năm '||to_char(elr.lost_date, 'yyyy'))  as lostDateStr,  ");
		/*sql.append("  cus.short_code as shortCode,    ");
		sql.append("  cus.customer_name as customerName,    ");
		sql.append("  cus.address as customerAddress,  ");
		sql.append("  (case when cus.phone is not null and cus.mobiphone is not null then to_char(cus.phone || ' - ' || cus.mobiphone)  ");
		sql.append("    when cus.phone is not null then to_char(cus.phone) when cus.mobiphone is not null then to_char(cus.mobiphone) else '' end) AS customerPhone,  ");*/
		/*** begin vuongmq; 12/05/2015 lay lai cac gia tri: shortCode; customerName; customerAddress ; customerPhone */
		sql.append(" (CASE ");
		sql.append(" WHEN elr.stock_type = 2 ");
		sql.append(" THEN ");
		sql.append(" (SELECT cus.short_code FROM customer cus WHERE cus.customer_id = elr.stock_id ");
		sql.append(" ) ");
		sql.append(" ELSE NULL ");
		sql.append(" END) AS shortCode, ");
		/** end shortCode */
		sql.append(" ( CASE ");
		sql.append(" WHEN elr.stock_type = 2 ");
		sql.append(" THEN ");
		sql.append(" (SELECT cus.customer_name ");
		sql.append(" FROM customer cus ");
		sql.append(" WHERE cus.customer_id = elr.stock_id ) ");
		sql.append(" ELSE NULL ");
		sql.append(" END) AS customerName, ");
		/** end customerName */
		sql.append(" (CASE ");
		sql.append(" WHEN elr.stock_type = 2 ");
		sql.append(" THEN ");
//		sql.append(" (SELECT cus.address FROM customer cus WHERE cus.customer_id = elr.stock_id) ");
		sql.append("edr.TO_OBJECT_ADDRES");
		sql.append(" ELSE ( SELECT sh.address FROM shop sh WHERE sh.shop_id = ");
		sql.append(" (SELECT shop_id FROM equip_stock WHERE equip_stock_id = elr.stock_id) ) ");
		sql.append(" END) AS customerAddress, ");
		/** end customerAddress */
		sql.append(" (CASE ");
		sql.append(" WHEN elr.stock_type = 2 ");
		sql.append(" then ");
		sql.append(" (CASE ");
		sql.append(" WHEN (SELECT cus.phone FROM customer cus WHERE cus.customer_id = elr.stock_id) IS NOT NULL ");
		sql.append(" AND (SELECT cus.mobiphone FROM customer cus WHERE cus.customer_id = elr.stock_id) IS NOT NULL ");
		sql.append(" THEN TO_CHAR( (SELECT cus.phone FROM customer cus WHERE cus.customer_id = elr.stock_id) ");
		sql.append(" || ' - ' ");
		sql.append(" || (SELECT cus.mobiphone FROM customer cus WHERE cus.customer_id = elr.stock_id) ) ");
		sql.append(" WHEN (SELECT cus.phone FROM customer cus WHERE cus.customer_id = elr.stock_id) IS NOT NULL ");
		sql.append(" THEN TO_CHAR( (SELECT cus.phone FROM customer cus WHERE cus.customer_id = elr.stock_id) ) ");
		sql.append(" WHEN (SELECT cus.mobiphone FROM customer cus WHERE cus.customer_id = elr.stock_id) IS NOT NULL ");
		sql.append(" THEN TO_CHAR( (SELECT cus.mobiphone FROM customer cus WHERE cus.customer_id = elr.stock_id) ) ");
		sql.append(" ELSE '' ");
		sql.append(" END) ");
		sql.append(" else ");
		sql.append(" (CASE ");
		sql.append(" WHEN (SELECT sh.phone FROM shop sh WHERE sh.shop_id = (SELECT shop_id FROM equip_stock WHERE equip_stock_id = elr.stock_id )) IS NOT NULL ");
		sql.append(" AND (SELECT sh.mobiphone FROM shop sh WHERE sh.shop_id = (SELECT shop_id FROM equip_stock WHERE equip_stock_id = elr.stock_id )) IS NOT NULL ");
		sql.append(" THEN TO_CHAR( (SELECT sh.phone FROM shop sh WHERE sh.shop_id = (SELECT shop_id FROM equip_stock WHERE equip_stock_id = elr.stock_id )) ");
		sql.append(" || ' - ' ");
		sql.append(" || (SELECT sh.mobiphone FROM shop sh WHERE sh.shop_id = (SELECT shop_id FROM equip_stock WHERE equip_stock_id = elr.stock_id )) ) ");
		sql.append(" WHEN (SELECT sh.phone FROM shop sh WHERE sh.shop_id = (SELECT shop_id FROM equip_stock WHERE equip_stock_id = elr.stock_id )) IS NOT NULL ");
		sql.append(" THEN TO_CHAR( (SELECT sh.phone FROM shop sh WHERE sh.shop_id = (SELECT shop_id FROM equip_stock WHERE equip_stock_id = elr.stock_id )) ) ");
		sql.append(" WHEN (SELECT sh.mobiphone FROM shop sh WHERE sh.shop_id = (SELECT shop_id FROM equip_stock WHERE equip_stock_id = elr.stock_id )) IS NOT NULL ");
		sql.append(" THEN TO_CHAR( (SELECT sh.mobiphone FROM shop sh WHERE sh.shop_id = (SELECT shop_id FROM equip_stock WHERE equip_stock_id = elr.stock_id )) ) ");
		sql.append(" ELSE '' ");
		sql.append(" END) ");
		sql.append(" end ) AS customerPhone, ");
		/** end customerPhone */
		/*** end vuongmq; 12/05/2015 lay lai cac gia tri: shortCode; customerName; customerAddress ; customerPhone*/
		sql.append(" (CASE WHEN elr.stock_type = 2 ");
		sql.append(" THEN ( select sh.shop_name from shop sh ");
		sql.append(" where sh.status = 1 ");
		sql.append(" and sh.shop_type_id = (select shop_type_id from shop_type where status = 1 and specific_type = 1) ");
		sql.append(" AND sh.shop_id = (SELECT cus.shop_id FROM customer cus WHERE cus.customer_id = elr.stock_id) ");
		sql.append(" ) ");
		sql.append(" ELSE ( select sh.shop_name from shop sh ");
		sql.append(" where sh.status = 1 ");
		sql.append(" and sh.shop_type_id = (select shop_type_id from shop_type where status = 1 and specific_type = 1) ");
		sql.append(" AND sh.shop_id = (SELECT shop_id FROM equip_stock WHERE equip_stock_id = elr.stock_id) ");
		sql.append(" ) ");
		sql.append(" END) as shopIsLevel5Code, ");
		/**end lay them kenh*/ /** -> end lay 5: cap nhat NPP*/
		sql.append(" (CASE WHEN elr.stock_type = 2 ");
		sql.append(" THEN ( select sh.shop_name from shop sh ");
		sql.append(" where sh.status = 1 ");
		sql.append(" and sh.shop_type_id = (select shop_type_id from shop_type where status = 1 and specific_type = 1) ");
		sql.append(" START WITH sh.shop_id = (SELECT cus.shop_id FROM customer cus WHERE cus.customer_id = elr.stock_id) ");
		sql.append(" CONNECT BY PRIOR sh.parent_shop_id = sh.shop_id) ");
		sql.append(" ELSE ( select sh.shop_name from shop sh ");
		sql.append(" where sh.status = 1 ");
		sql.append(" and sh.shop_type_id = (select shop_type_id from shop_type where status = 1 and specific_type = 1) ");
		sql.append(" START WITH sh.shop_id = (SELECT shop_id FROM equip_stock WHERE equip_stock_id = elr.stock_id ) ");
		sql.append(" CONNECT BY PRIOR sh.parent_shop_id = sh.shop_id) ");
		sql.append(" END) as shopIsLevel3Code, ");
		/**end lay them mien*/
		sql.append(" 	 elr.representor as representor,    ");
		sql.append("  ('Ngày '||to_char(elr.last_arising_sales_date, 'dd') ||' Tháng '||to_char(elr.last_arising_sales_date, 'MM')|| ' Năm '||to_char(elr.last_arising_sales_date, 'yyyy')) as lastArisingSalesDateStr,  ");
		sql.append(" 	 elr.conclusion as conclusion,    ");
		sql.append("   elr.tracing_result as tracingResult,  ");
		sql.append("   elr.tracing_place as tracingPlace,  ");
		sql.append(" 	 elr.recommended_treatment as recommendedTreatment,    ");
		sql.append("  elr.delivery_status as deliveryStatus,  ");
		sql.append("  edr.contract_number as contractNumber,  ");
		sql.append("  edr.to_position as chucVuBenMuon,  "); //// vuongmq; 29/05/2015; print chuc vu ben ben; bao mat
		sql.append("  eq.manufacturing_year as manufacturingYear,  ");
		sql.append(" to_char(eq.first_date_in_use, 'yyyy') as firstDateInUseStr, ");
		sql.append("  eg.name as equipGroupName,  ");
		sql.append("  ec.name as equipCategoryName  ");
		fromSql.append(" 	from equip_lost_record elr     ");
		//fromSql.append(" 	 join customer cus on elr.stock_id = cus.customer_id    "); // vuongmq; 12/05/2015; khong xai
//		fromSql.append(" 	 join equip_period ep on ep.equip_period_id = elr.equip_period_id    ");
		fromSql.append(" 	 join equipment eq on eq.equip_id = elr.equip_id   ");
		fromSql.append("  join equip_group eg on eq.equip_group_id = eg.equip_group_id  ");
		fromSql.append("  join equip_category ec on eg.equip_category_id = ec.equip_category_id  ");
		//fromSql.append(" 	 join shop sh on cus.shop_id = sh.shop_id    "); // vuongmq; 12/05/2015; khong xai
		fromSql.append("  left join equip_delivery_record edr on elr.equip_delivery_record_id = edr.equip_delivery_record_id   ");
		fromSql.append(" where 1 = 1   ");
		if (filter.getLstId() != null && !filter.getLstId().isEmpty()) {
			fromSql.append(" and elr.equip_lost_record_id  in ( ? ");
			params.add(filter.getLstId().get(0));
			for (int i = 1, size = filter.getLstId().size(); i < size; i++) {
				fromSql.append(" ,? ");
				params.add(filter.getLstId().get(i));
			}
			fromSql.append(" ) ");
		}

		sql.append(fromSql.toString());

		sql.append(" order by elr.code  ");

		String[] fieldNames = { "id", "code", "recordStatus", "lostDateStr", "shortCode", "customerName", "customerAddress", "customerPhone", 
				"shopIsLevel5Code", "shopIsLevel3Code",
				"representor", "lastArisingSalesDateStr", "conclusion", "tracingResult", "tracingPlace",
				"recommendedTreatment", "deliveryStatus", "contractNumber", 
				"chucVuBenMuon",
				"manufacturingYear", "equipGroupName", "equipCategoryName", "firstDateInUseStr" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, //  chucVuBenMuon
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count ");
		countSql.append(fromSql.toString());
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipRecordVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipRecordVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<EquipAttachFile> searchEquipAttachFileByFilter(BasicFilter<EquipAttachFile> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_attach_file  ");
		sql.append("	where 1 = 1   ");
		if (filter.getLstId() != null && !filter.getLstId().isEmpty()) {
			sql.append("	and equip_attach_file_id in ( ? ");
			params.add(filter.getLstId().get(0));
			for (int i = 1, size = filter.getLstId().size(); i < size; i++) {
				sql.append(" ,? ");
				params.add(filter.getLstId().get(i));
			}
			sql.append(" ) ");
		}
		if (filter.getId() != null) {
			sql.append("	and equip_attach_file_id = ? ");
			params.add(filter.getId());
		}
		if (filter.getObjectId() != null) {
			sql.append("	and object_id = ? ");
			params.add(filter.getObjectId());
		}
		if (filter.getObjectType() != null) {
			sql.append("	and object_type = ?  ");
			params.add(filter.getObjectType());
		}
		if (filter.getkPaging() != null) {
			return repo.getListBySQLPaginated(EquipAttachFile.class, sql.toString(), params, filter.getkPaging());
		}
		return repo.getListBySQL(EquipAttachFile.class, sql.toString(), params);
	}

	@Override
	public EquipAttachFile getEquipAttachFileById(Long id) throws DataAccessException {
		return repo.getEntityById(EquipAttachFile.class, id);
	}

	@Override
	public List<EquipLostRecord> searchEquipLostRecordByFilter(EquipRecordFilter<EquipLostRecord> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_lost_record e");
		sql.append("where 1 = 1 ");
		if (filter.getStockType() != null) {
			sql.append("and e.stock_type = ? ");
			params.add(filter.getStockType());
		}
		if (filter.getLstId() != null && !filter.getLstId().isEmpty()) {
			sql.append("and e.equip_lost_record_id in (? ");
			params.add(filter.getLstId().get(0));
			for (int i = 1, size = filter.getLstId().size(); i < size; i++) {
				sql.append(" ,? ");
				params.add(filter.getLstId().get(i));
			}
			sql.append(" ) ");
		}

		if (filter.getId() != null) {
			sql.append(" and e.equip_lost_record_id = ? ");
			params.add(filter.getId());
		}
		
		if(filter.getShopId() != null) {
			sql.append(" AND ( ");
			sql.append("  (e.stock_type = 1 and e.stock_id IN ");
			sql.append(" (SELECT equip_stock_id ");
			sql.append(" FROM equip_stock ");
			sql.append(" WHERE status = 1 ");
			sql.append(" and shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append(" ) ");
			params.add(filter.getShopId());
			/** end kho NPP */
			sql.append(" or  (e.stock_type = 2 and e.stock_id IN ");
			sql.append(" (SELECT cu.customer_id ");
			sql.append(" FROM customer cu ");
			sql.append(" WHERE  ");
			sql.append(" cu.shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append("  ");
			sql.append(" ) ");
			/** end kho KH */
			sql.append(" ) "); // end And
			params.add(filter.getShopId());
		}
		
		if (filter.getkPaging() != null) {
			return repo.getListBySQLPaginated(EquipLostRecord.class, sql.toString(), params, filter.getkPaging());
		}
		return repo.getListBySQL(EquipLostRecord.class, sql.toString(), params);
	}

	/////////////////////////////// BEGIN THANH LY TAI SAN; VuongMQ; 08/07/2015 //////////////////////////////
	@Override
	public List<EquipmentDeliveryVO> getListEquipmentLiquidationVOByFilter(EquipmentFilter<EquipmentDeliveryVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder whereSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select DISTINCT eq.equip_id as equipmentId, ");
		sql.append(" eq.code as equipmentCode, ");
		sql.append(" eq.serial as seriNumber, ");
		sql.append(" (case when eq.health_status is not null then (select min(value) from ap_param ap where ap.status = 1 and ap.ap_param_code like eq.health_status and type like 'EQUIP_CONDITION')else NULL end) as healthStatus, ");
		sql.append(" eq.stock_type as stockType, ");
		sql.append(" eq.stock_id as stockId, ");
		sql.append(" (case ");
		sql.append(" when eq.stock_type = 2 then (select cus.customer_code from customer cus where cus.customer_id = eq.stock_id and cus.status = 1) ");
		sql.append(" else (select sh.shop_code from shop sh where sh.shop_code = eq.stock_code and sh.status = 1) end)as stockCode, ");
		sql.append(" (case ");
		sql.append(" when eq.stock_type = 2 then (select cus.customer_name from customer cus where cus.customer_id = eq.stock_id and cus.status = 1) ");
		sql.append(" else (select sh.shop_name from shop sh where sh.shop_code = eq.stock_code and sh.status = 1) end)as stockName, ");
		sql.append(" ec.name as typeEquipment, ");
		sql.append(" eg.code as groupEquipmentCode, ");
		sql.append(" eg.name as groupEquipmentName, ");
		sql.append(" ( CASE ");
		sql.append(" WHEN eg.capacity_from IS NULL ");
		sql.append(" AND eg.capacity_to IS NULL ");
		sql.append(" THEN '' ");
		sql.append(" WHEN eg.capacity_from IS NULL ");
		sql.append(" THEN ' <= ' ");
		sql.append(" ||TO_CHAR(eg.capacity_to) ");
		sql.append(" WHEN eg.capacity_to IS NULL ");
		sql.append(" THEN ' >= ' ");
		sql.append(" ||TO_CHAR(eg.capacity_from) ");
		sql.append(" WHEN eg.capacity_to <> eg.capacity_from ");
		sql.append(" THEN TO_CHAR(eg.capacity_from) ");
		sql.append(" || ' - ' ");
		sql.append(" || TO_CHAR(eg.capacity_to) ");
		sql.append(" ELSE TO_CHAR(eg.capacity_from) ");
		sql.append(" END) AS capacity, ");
		sql.append(" eg.brand_name as equipmentBrand, ");
		sql.append(" ep.name as equipmentProvider, ");
		sql.append(" eq.manufacturing_year as yearManufacture, ");
		sql.append(" eq.price as price, ");
		/*if (filter.getIdLiquidation() != null) {
			sql.append(" edrdt.equip_value as valueEquip ");
		} else {
			sql.append(" null as valueEquip ");
		}*/
		if (filter.getIdLiquidation() != null) {
			sql.append(" edrdt.liquidation_value as liquidationValue, ");
			sql.append(" edrdt.eviction_value as evictionValue ");
		} else {
			sql.append(" null as liquidationValue, ");
			sql.append(" null as evictionValue ");
		}
		/** From Table*/
		sql.append(" from equipment eq ");
		sql.append(" join equip_group eg on eg.equip_group_id = eq.equip_group_id ");
		sql.append(" join equip_category ec on eg.equip_category_id = ec.equip_category_id ");
		sql.append(" join equip_provider ep on ep.equip_provider_id = eq.equip_provider_id ");
		if (filter.getIdLiquidation() != null) {
			sql.append(" join equip_liquidation_form_dtl edrdt on edrdt.equip_id = eq.equip_id ");
		}
		/** Lay theo dieu kien where */
		sql.append(" where 1=1 ");
		if (filter.getLstEquipAdd() != null && filter.getLstEquipAdd().size() > 0) {
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
				code += ",?";
			}
			sql.append(" and eq.code not in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
				params.add(filter.getLstEquipAdd().get(i));
			}
		}
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			whereSql.append(" and upper(eq.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
			whereSql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
		} /*else {
			whereSql.append(" and eq.serial is not null ");
		}*/
		if (filter.getEquipCategoryId() != null) {
			whereSql.append(" and ec.equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		if (filter.getEquipGroupId() != null) {
			whereSql.append(" and eg.equip_group_id = ? ");
			params.add(filter.getEquipGroupId());
		}
		if (filter.getEquipProviderId() != null) {
			whereSql.append(" and ep.equip_provider_id = ? ");
			params.add(filter.getEquipProviderId());
		}
		if (filter.getYearManufacture() != null) {
			whereSql.append(" and eq.manufacturing_year = ? ");
			params.add(filter.getYearManufacture());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			whereSql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
		}
		if (filter.getStockType() != null) {
			whereSql.append(" and eq.stock_type = ? ");
			params.add(filter.getStockType());
		} else if (filter.getIdRecordDelivery() == null) {
			// xai tat ca ca kho
			/*whereSql.append(" and eq.stock_type != ? ");
			params.add(EquipStockTotalType.KHO_KH.getValue());*/
		}
		if (filter.getStatusEquip() != null) {
			whereSql.append(" and eq.status = ? ");
			params.add(filter.getStatusEquip());
		} else {
			whereSql.append(" and eq.status != ? ");
			params.add(StatusType.DELETED.getValue());
		}
		if (filter.getTradeStatus() != null) {
			whereSql.append(" and eq.trade_status = ?");
			params.add(filter.getTradeStatus());
		}
		if (filter.getUsageStatus() != null) {
			whereSql.append(" and eq.usage_status = ?");
			params.add(filter.getUsageStatus());
		}
		if (filter.getIdLiquidation() != null) {
			whereSql.append(" and edrdt.equip_liquidation_form_dtl_id in ");
			whereSql.append(" (select edrd.equip_liquidation_form_dtl_id from equip_liquidation_form_dtl edrd ");
			whereSql.append(" join equip_liquidation_form edr on edr.equip_liquidation_form_id = edrd.equip_liquidation_form_id ");
			whereSql.append(" where 1=1 ");
			whereSql.append(" and edr.equip_liquidation_form_id = ?) ");
			params.add(filter.getIdLiquidation());
		}
		String sqlTemp = sql.toString();
		sql.append(whereSql);
		// them cac thiet bi xoa tam tren danh sach
		if (filter.getLstEquipDelete() != null && filter.getLstEquipDelete().size() > 0) {
			sql.append(" union ");
			sql.append(sqlTemp);
			if (filter.getLstEquipAdd() != null && filter.getLstEquipAdd().size() > 0) {
				for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
					params.add(filter.getLstEquipAdd().get(i));
				}
			}
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipDelete().size(); i++) {
				code += ",?";
			}
			sql.append(" and eq.code in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipDelete().size(); i++) {
				params.add(filter.getLstEquipDelete().get(i));
			}
			if (!StringUtility.isNullOrEmpty(filter.getCode())) {
				sql.append(" and upper(eq.code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
				sql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
			} else {
				sql.append(" and eq.serial is not null ");
			}
			if (filter.getEquipCategoryId() != null) {
				sql.append(" and ec.equip_category_id = ? ");
				params.add(filter.getEquipCategoryId());
			}
			if (filter.getEquipGroupId() != null) {
				sql.append(" and eg.equip_group_id = ? ");
				params.add(filter.getEquipGroupId());
			}
			if (filter.getEquipProviderId() != null) {
				sql.append(" and ep.equip_provider_id = ? ");
				params.add(filter.getEquipProviderId());
			}
			if (filter.getYearManufacture() != null) {
				sql.append(" and eq.manufacturing_year = ? ");
				params.add(filter.getYearManufacture());
			}
			if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
				sql.append(" and upper(eq.stock_code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
			}
			if (filter.getStatusEquip() != null) {
				sql.append(" and eq.status = ? ");
				params.add(filter.getStatusEquip());
			} else {
				sql.append(" and eq.status != ? ");
				params.add(StatusType.DELETED.getValue());
			}
			if (filter.getUsageStatus() != null) {
				sql.append(" and eq.usage_status = ?");
				params.add(filter.getUsageStatus());
			}
		}
		sql.append(" ORDER BY equipmentCode ASC  ");

		String[] fieldNames = { "equipmentId",// 0
				"equipmentCode",// 1
				"seriNumber",// 2
				"healthStatus",// 3
				"stockType",// 4
				"stockId",// 5
				"stockCode",// 4
				"stockName",// 5
				"typeEquipment",// 6
				"groupEquipmentCode",// 7
				"groupEquipmentName",// 8
				"capacity",// 9
				"equipmentBrand",// 10
				"equipmentProvider",// 11
				"yearManufacture",// 12
				"price",// 13
				//"valueEquip",// 14
				"liquidationValue", //14
				"evictionValue" //15
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.INTEGER,// 4
				StandardBasicTypes.LONG,// 5
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.STRING,// 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.BIG_DECIMAL, // 13
				//StandardBasicTypes.BIG_DECIMAL // 14
				StandardBasicTypes.BIG_DECIMAL, // 14
				StandardBasicTypes.BIG_DECIMAL // 15
		};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	/***
	 * @author vuongmq
	 * @date 08/07/2015
	 * Lay danh sach thiet bi pop up cua thanh ly tai san
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public List<EquipmentDeliveryVO> getListEquipmentLiquidationPopup(EquipmentFilter<EquipmentDeliveryVO> filter) throws DataAccessException {
		if (filter.getStaffRoot() == null || filter.getShopRoot() == null) {
			throw new IllegalArgumentException("staffRoot or shopRoot not id");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder whereSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH rdta ");
		sql.append(" AS (SELECT es.equip_stock_id AS equipStockId, ");
		sql.append(" es.code AS equipStockCode, ");
		sql.append(" es.name AS equipStockName, ");
		sql.append(" es.shop_id AS shopId, ");
		sql.append(" sh.shop_code AS shopCode, ");
		sql.append(" sh.shop_name AS shopName, ");
		sql.append(" erd.is_under AS isUnder ");
		sql.append(" FROM equip_role_user eru ");
		sql.append(" join equip_role er ");
		sql.append(" ON eru.equip_role_id = er.equip_role_id ");
		sql.append(" join equip_role_detail erd ");
		sql.append(" ON er.equip_role_id = erd.equip_role_id ");
		sql.append(" join equip_stock es ");
		sql.append(" ON erd.equip_stock_id = es.equip_stock_id ");
		sql.append(" join shop sh ");
		sql.append(" ON es.shop_id = sh.shop_id ");
		sql.append(" WHERE eru.status = 1 ");
		sql.append(" AND er.status = 1 ");
		sql.append(" AND es.status = 1 ");
		sql.append(" AND eru.user_id = ? ");
		params.add(filter.getStaffRoot()); // DK phan quyen Kho 
		sql.append(" and sh.shop_id in ( ");
		sql.append("  select shop_id from shop ");
		sql.append("  start with shop_id in (?) ");
		sql.append("  connect by prior shop_id = parent_shop_id) ");
		sql.append(" ), ");
		params.add(filter.getShopRoot()); // DK CMS, --shop ma user chon tuong tac khi dang nhap. Truong hop user dươc phan quyen nhieu shop, thi luc dang nhap chi chon 1 shop nao do de tuong tac. 
		sql.append(" rstockfull ");
		sql.append(" AS (SELECT DISTINCT equipstockid, ");
		sql.append(" equipstockcode, ");
		sql.append(" equipstockname, ");
		sql.append(" shopcode, ");
		sql.append(" shopname, ");
		sql.append(" shopid ");
		sql.append(" FROM ((SELECT es.equip_stock_id AS equipStockId, ");
		sql.append(" es.code AS equipStockCode, ");
		sql.append(" es.name AS equipStockName, ");
		sql.append(" es.shop_id AS shopId, ");
		sql.append(" sh.shop_code AS shopCode, ");
		sql.append(" sh.shop_name AS shopName, ");
		sql.append(" 1 AS isUnder ");
		sql.append(" FROM (SELECT * ");
		sql.append(" FROM shop ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" START WITH shop_id IN (SELECT shopid ");
		sql.append(" FROM rdta ");
		sql.append(" WHERE isunder = 1) ");
		sql.append(" AND status = 1 ");
		sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id) sh ");
		sql.append(" join equip_stock es ");
		sql.append(" ON sh.shop_id = es.shop_id ");
		sql.append(" WHERE es.status = 1 ");
		sql.append(" MINUS ");
		sql.append(" (SELECT es.equip_stock_id AS equipStockId, ");
		sql.append(" es.code AS equipStockCode, ");
		sql.append(" es.name AS equipStockName, ");
		sql.append(" es.shop_id AS shopId, ");
		sql.append(" sh.shop_code AS shopCode, ");
		sql.append(" sh.shop_name AS shopName, ");
		sql.append(" 1 AS isUnder ");
		sql.append(" FROM shop sh ");
		sql.append(" join equip_stock es ON sh.shop_id = es.shop_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND sh.shop_id IN (SELECT shopid ");
		sql.append(" FROM rdta ");
		sql.append(" WHERE isunder = 1) ");
		sql.append(" AND es.equip_stock_id NOT IN (SELECT equipstockid ");
		sql.append(" FROM rdta))) ");
		sql.append(" UNION ");
		sql.append(" (SELECT * ");
		sql.append(" FROM rdta ");
		sql.append(" WHERE isunder = 0)) ");
		sql.append(" ORDER BY equipstockcode), ");
		/** begin kho KH */
		sql.append(" stockKH as ( ");
		sql.append(" SELECT c.customer_id AS equipStockId, ");
		sql.append(" sh.shop_code||c.short_code AS equipStockCode, ");
		sql.append(" c.customer_name AS equipStockName, ");
		sql.append(" sh.shop_id AS shopId, ");
		sql.append(" sh.shop_code AS shopCode, ");
		sql.append(" sh.shop_name AS shopName, ");
		sql.append(" 0 AS isUnder, ");
		sql.append(" c.short_code as shortCode ");
		sql.append(" from customer c ");
		sql.append(" join shop sh on sh.shop_id = c.shop_id ");
		sql.append(" where c.shop_id in (select distinct r.shopId from rstockfull r) ");
		//sql.append(" join shop sh on sh.shop_id = r.shopId ");
		//sql.append(" where sh.shop_type_id = (select channel_type_id from channel_type where type = 1 and object_type = 3 and status = 1)) ");
		sql.append(" ) ");
		/*** select VO -----------------------------------------------------------------------------------------------------------------------**/
		sql.append(" ,lstEquips as ( ");
		sql.append(" SELECT DISTINCT eq.equip_id AS equipmentId, ");
		sql.append(" eq.code AS equipmentCode, ");
		sql.append(" eq.serial AS seriNumber, ");
		/** begin healthStatus */
		sql.append(" ( CASE ");
		sql.append(" WHEN eq.health_status IS NOT NULL THEN ");
		sql.append(" (SELECT Min(value) ");
		sql.append(" FROM ap_param ap ");
		sql.append(" WHERE ap.status = 1 ");
		sql.append(" AND ap.ap_param_code LIKE ");
		sql.append(" eq.health_status ");
		sql.append(" AND ");
		sql.append(" TYPE LIKE 'EQUIP_CONDITION' ");
		sql.append(" ) ");
		sql.append(" ELSE NULL ");
		sql.append(" END ) AS healthStatus, ");
		/** begin healthCode */
		sql.append(" ap.ap_param_code AS healthCode, ");
		/** Mong muon tester lay ma kho ben bang equipment */
		sql.append(" eq.stock_code AS stockCode, ");
		/** Mong muon tester lay ten kho ben bang equipment */
		sql.append(" eq.stock_name AS stockName, ");
		/** begin typeEquipment */
		sql.append(" ec.name AS typeEquipment, ");
		sql.append(" eg.code AS groupEquipmentCode, ");
		sql.append(" eg.name AS groupEquipmentName, ");
		/** begin capacity */
		sql.append(" ( CASE ");
		sql.append(" WHEN eg.capacity_from IS NULL ");
		sql.append(" AND eg.capacity_to IS NULL THEN '' ");
		sql.append(" WHEN eg.capacity_from IS NULL THEN ' <= ' ");
		sql.append(" ||To_char(eg.capacity_to) ");
		sql.append(" WHEN eg.capacity_to IS NULL THEN ' >= ' ");
		sql.append(" ||To_char(eg.capacity_from) ");
		sql.append(" WHEN eg.capacity_to <> eg.capacity_from THEN ");
		sql.append(" To_char(eg.capacity_from) ");
		sql.append(" || ' - ' ");
		sql.append(" || To_char(eg.capacity_to) ");
		sql.append(" ELSE To_char(eg.capacity_from) ");
		sql.append(" END ) AS capacity, ");
		/** begin equipmentBrand */
		sql.append(" eg.brand_name AS equipmentBrand, ");
		sql.append(" ep.name AS equipmentProvider, ");
		sql.append(" ep.code AS equipmentProviderCode, ");
		sql.append(" eq.manufacturing_year AS yearManufacture, ");
		sql.append(" To_char(eq.first_date_in_use, 'yyyy')  AS firstYearInUse, ");
		sql.append(" eq.price AS price, ");
		sql.append(" NULL AS contentDelivery, ");
		sql.append(" NULL AS depreciation, ");
		/** begin shopCode */
		sql.append(" (case when eq.stock_type = 1 then sh.shop_code ");
		sql.append(" else shc.shop_code ");
		sql.append(" end) AS shopCode, ");
		/** begin shopName */
		sql.append(" (case when eq.stock_type = 1 then sh.shop_name ");
		sql.append(" else shc.shop_name");
		sql.append(" end) AS shopName, ");
		/** begin shortCode */
		sql.append(" cu.short_code AS shortCode, ");
		sql.append(" cu.name_text AS nameText, ");
		/** begin customerName */
		sql.append(" cu.customer_name AS customerName, ");
		/** begin address */
		sql.append(" (case when eq.stock_type = 1 then sh.address ");
		sql.append(" else cu.address ");
		sql.append(" end) AS address, ");
		/** begin stockType */
		sql.append(" eq.stock_type as stockType, ");
		sql.append(" eq.trade_type as tradeType, ");
		sql.append(" eq.trade_status as tradeStatus, ");
		sql.append(" eq.usage_status as usageStatus, ");
		sql.append(" eq.status as status, ");
		sql.append(" eq.liquidation_status as liquidationStatus ");
		
		/** from table */
		sql.append(" FROM equipment eq ");
		sql.append(" join equip_group eg ON eg.equip_group_id = eq.equip_group_id ");
		sql.append(" AND eg.status = 1 ");
		sql.append(" join equip_category ec ON eg.equip_category_id = ec.equip_category_id ");
		sql.append(" AND ec.status = 1 ");
		sql.append(" join equip_provider ep ON ep.equip_provider_id = eq.equip_provider_id ");
		sql.append(" AND ep.status = 1 ");
		
		sql.append(" left join ap_param ap on ap.status = 1 AND ap.ap_param_code LIKE eq.health_status AND TYPE LIKE 'EQUIP_CONDITION' ");
		sql.append(" left join rstockfull npp on eq.stock_id = npp.equipStockId and eq.stock_type = 1 ");
		sql.append(" left join shop sh on sh.shop_id = npp.shopId and eq.stock_type = 1 ");
		sql.append(" left join stockKH kh on eq.stock_id = kh.equipStockId and eq.stock_type = 2 ");
		sql.append(" left join customer cu on cu.customer_id = eq.stock_id and eq.stock_type = 2 ");
		sql.append(" left join shop shc on shc.shop_id = cu.shop_id ");
		
		if (!StringUtility.isNullOrEmpty(filter.getEquipLendCode())) {
			sql.append(" JOIN equip_lend_detail eld ");
			sql.append(" ON eld.EQUIP_CATEGORY_ID = eg.EQUIP_CATEGORY_ID ");
			sql.append(" JOIN equip_lend el ");
			sql.append(" ON eld.equip_lend_id = el.equip_lend_id AND el.code = ? ");
			sql.append(" AND el.status = 1 AND eld.status = 1 ");
			params.add(filter.getEquipLendCode());
		}
		sql.append("WHERE  1 = 1 ");
		sql.append(" AND ((eq.stock_type = 1 and eq.stock_id in (SELECT npp.equipStockId FROM rstockfull npp))  ");
		sql.append(" or (eq.stock_type = 2 and cu.customer_id in (SELECT kh.equipStockId FROM stockKH kh))) ");
		
		/*** BEgin quyen CMS, quyen KHo cua TB dang o kho va su dung **/
//		sql.append(" AND ( ");
//		/** Begin Dieu kien NPP */
//		sql.append(" (eq.stock_type = 1 ");
//		sql.append(" and eq.stock_id in (select npp.equipStockId from rstockfull npp ");
//		sql.append(" where 1 =1 ");
//		// tim kiem don vi
//		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
//			sql.append(" and (npp.shopCode like ? escape '/' ");
//			sql.append("    or unicode2English(npp.shopName) like ? escape '/') ");
//			params.add(StringUtility.toOracleSearchLike(filter.getShopCode().toUpperCase()));
//			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getShopCode()).toLowerCase()));
//		}
//		// tim kiem kho
//		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
//			sql.append(" and npp.equipStockCode like ? escape '/' ");
//			params.add(StringUtility.toOracleSearchLike(filter.getStockCode().toUpperCase()));
//		}
//		// tim kiem dia chi
//		if (!StringUtility.isNullOrEmpty(filter.getAddress())) {
//			sql.append(" and npp.shopId in (select sh.shop_id from shop sh where unicode2English(sh.address) like ? escape '/') ");
//			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getAddress()).toLowerCase()));
//		}
//		sql.append("    ) ");
//		sql.append(" ) ");
//		/** End Dieu kien NPP */
//		sql.append(" or ");
//		sql.append(" (eq.stock_type = 2 ");
//		sql.append(" and eq.stock_id in (select kh.equipStockId from stockKH kh ");
//		sql.append(" where 1 =1 ");
//		// tim kiem don vi
//		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
//			sql.append(" and (kh.shopCode like ? escape '/' ");
//			sql.append("      or unicode2English(kh.shopName) like ? escape '/') ");
//			params.add(StringUtility.toOracleSearchLike(filter.getShopCode().toUpperCase()));
//			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getShopCode()).toLowerCase()));
//		}
//		// tim kiem kho
//		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
//			sql.append(" and kh.equipStockCode like ? escape '/' ");
//			params.add(StringUtility.toOracleSearchLike(filter.getStockCode().toUpperCase()));
//		}
//		// tim kiem dia chi
//		if (!StringUtility.isNullOrEmpty(filter.getAddress())) {
//			sql.append(" and kh.equipStockId in (select cu.customer_id from customer cu where unicode2English(cu.address) like ? escape '/') ");
//			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getAddress()).toLowerCase()));
//		}
//		sql.append(" 		) ");
//		sql.append("  ) ");
//		/** End Dieu kien KH */
//		sql.append(" ) ");
		/*** END quyen CMS, quyen KHo cua TB dang o kho va su dung **/
//		// Neu tim co ma KH va ten KH
//		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode()) || !StringUtility.isNullOrEmpty(filter.getCustomerName())) {
//			StringBuilder whereKH = new StringBuilder();
//			whereKH.append(" and (eq.stock_type = 2 ");
//			whereKH.append(" and eq.stock_id in (select kh.equipStockId from stockKH kh ");
//			whereKH.append(" where 1 =1 ");
//			// tim kiem maKH
//			if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
//				whereKH.append(" and kh.shortCode like ? escape '/' ");
//				params.add(StringUtility.toOracleSearchLike(filter.getCustomerCode().toUpperCase()));
//			}
//			// tim kiem ten KH
//			if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
//				whereKH.append(" and unicode2English(kh.equipStockName) like ? escape '/' ");
//				params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCustomerName()).toLowerCase()));
//			}
//			whereKH.append(" 		) ");
//			whereKH.append("  ) ");
//			/** End Dieu kien KH  neu tim kiem co Ma KH hoac ten KH */
//			sql.append(whereKH); // add sql vao whereSql
//		}
		// tiep theo la dieu kien gom: dang su dung, dang o kho va bao mat
		/*if (filter.getTradeStatus() != null) {
			whereSql.append(" and eq.trade_status = ?");
			params.add(filter.getTradeStatus());
		}*/
		if (Boolean.TRUE.equals(filter.isFlagNotUsageStatus())) {
			// vuongmq; 03/07/2015; truong hop import excel phieu sua chua thi: True, khong can lay trang thai su dung va o kho
			// cac truong hop khac lay thiet bi trang thai su dung va o kho
			whereSql.append(" and (eq.TRADE_TYPE in (?,?)  "); // True: lay TB ben Bao mat, ko lay trang thai thanh ly
			params.add(EquipTradeType.NOTICE_LOST.getValue());
			params.add(EquipTradeType.NOTICE_LOSS_MOBILE.getValue());
			whereSql.append(" ) ");
		} else {
			// Begin DK ca TB Thanh Ly va TB bao mat
			whereSql.append(" AND ( ");
			// Begin DK cho TB ben thanh ly
			whereSql.append(" ( 1 = 1 ");
			if (filter.getTradeStatus() != null) {
				whereSql.append(" and eq.trade_status = ?");
				params.add(filter.getTradeStatus());
			}
			if (filter.getTradeType() != null) {
				whereSql.append(" AND eq.trade_type = ? ");
				params.add(filter.getTradeType());
			} else {
				whereSql.append(" AND eq.trade_type IS NULL ");
			}
			whereSql.append(" )"); // Begin DK cho TB ben thanh ly
			// Begin DK cho TB ben bao mat
			whereSql.append(" or (eq.TRADE_TYPE in (?,?)  ");
			params.add(EquipTradeType.NOTICE_LOST.getValue());
			params.add(EquipTradeType.NOTICE_LOSS_MOBILE.getValue());
			whereSql.append(" and eq.LIQUIDATION_STATUS = ?  ");
			params.add(LinquidationStatus.WAITING_LIQUIDATION.getValue());
			whereSql.append("      )"); // End DK cho TB ben bao mat
			whereSql.append(" ) "); // End DK ca TB Thanh Ly va TB bao mat
			// vuongmq 06/08/2015; chung cho TB thanh ly va TB bao mat
			whereSql.append(" and eq.usage_status in (?,?) ");
			params.add(EquipUsageStatus.IS_USED.getValue());
			params.add(EquipUsageStatus.SHOWING_WAREHOUSE.getValue());
		}
		if (filter.getStatusEquip() != null) {
			whereSql.append(" and eq.status = ? ");
			params.add(filter.getStatusEquip());
		} else {
			whereSql.append(" and eq.status != ? ");
			params.add(StatusType.DELETED.getValue());
		}
		if (filter.getLstEquipAdd() != null && filter.getLstEquipAdd().size() > 0) {
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
				code += ",?";
			}
			whereSql.append(" and eq.code not in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipAdd().size(); i++) {
				params.add(filter.getLstEquipAdd().get(i));
			}
		}
		
		if (filter.getLstEquipmentCode() != null && filter.getLstEquipmentCode().size() > 0) {
			String code = "'-1'";
			for (int i = 0; i < filter.getLstEquipmentCode().size(); i++) {
				code += ",?";
			}
			whereSql.append(" and eq.code not in ( " + code + " ) ");
			for (int i = 0; i < filter.getLstEquipmentCode().size(); i++) {
				params.add(filter.getLstEquipmentCode().get(i));
			}
		}
		
		//tim kiem bang trong dieu kien so sanh
		if (!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			whereSql.append(" and eq.code = ? ");
			params.add(filter.getEquipCode());
		}
		
		//tim kiem like code tren dialog
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			whereSql.append(" and eq.code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeriNumber())) {
			whereSql.append(" and upper(eq.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeriNumber().toUpperCase()));
		}
		if (filter.getEquipCategoryId() != null) {
			whereSql.append(" and ec.equip_category_id = ? ");
			params.add(filter.getEquipCategoryId());
		}
		if (filter.getEquipGroupId() != null) {
			whereSql.append(" and eg.equip_group_id = ? ");
			params.add(filter.getEquipGroupId());
		}
		if (filter.getEquipProviderId() != null) {
			whereSql.append(" and ep.equip_provider_id = ? ");
			params.add(filter.getEquipProviderId());
		}
		if (filter.getYearManufacture() != null) {
			whereSql.append(" and eq.manufacturing_year = ? ");
			params.add(filter.getYearManufacture());
		}
		/*if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			whereSql.append(" and s.equipStockCode like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStockCode().toUpperCase()));
		}*/
		if (filter.getStockType() != null) {
			whereSql.append(" and eq.stock_type = ? ");
			params.add(filter.getStockType());
		} else if (filter.getIdRecordDelivery() == null) {
			// xai tat ca kho
			/*whereSql.append(" and eq.stock_type != ? ");
			params.add(EquipStockTotalType.KHO_KH.getValue());*/
		}
		/** end Dk thiet bi lay dang o kho va dang su dung*/
		
		sql.append(whereSql);
		sql.append(" ) SELECT * FROM lstEquips ");
		sql.append(" where 1=1  ");
		// tim kiem don vi
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("and ( ");
			sql.append("  shopCode like ? escape '/' ");
			sql.append("  	or unicode2English(shopName) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getShopCode().toUpperCase()));
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getShopCode()).toLowerCase()));
			sql.append(" ) ");
		}
		// tim kiem kho
		if (!StringUtility.isNullOrEmpty(filter.getStockCode())) {
			sql.append(" and stockCode like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getStockCode().toUpperCase()));
		}
		// tim kiem dia chi
		if (!StringUtility.isNullOrEmpty(filter.getAddress())) {
			sql.append(" and unicode2English(address) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getAddress()).toLowerCase()));
		}
		// tim kiem maKH
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and (upper(shortCode) like ? ESCAPE '/' or nameText like ? ESCAPE '/')");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().toUpperCase()));
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().toUpperCase()));
		}

		// tim kiem ten KH
		if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
			sql.append(" and unicode2English(customerName) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getCustomerName()).toLowerCase()));
		}

		String[] fieldNames = { "equipmentId",// 0
				"equipmentCode",// 1
				"seriNumber",// 2
				"healthStatus",// 3
				"healthCode",	//3.5
				"stockCode",// 4
				"stockName",// 5
				"typeEquipment",// 6
				"groupEquipmentCode",// 7
				"groupEquipmentName",// 8
				"capacity",// 9
				"equipmentBrand",// 10
				"equipmentProvider",// 11
				"equipmentProviderCode",
				"yearManufacture",// 12
				"contentDelivery",// 13
				"firstYearInUse",	//14
				"price",				//15
				"depreciation", //16,
				"shopCode",  //17
				"shopName", //18
				"shortCode", //19
				"customerName",  //20
				"address", // 20.1
				"stockType", //21
				"tradeType", //22
				"tradeStatus", // 23
				"usageStatus", // 24
				"status", // 25
				"liquidationStatus", // 26
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,//3.5
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.STRING,// 10
				StandardBasicTypes.STRING,// 11
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.INTEGER, // 13
				StandardBasicTypes.STRING,	//14
				StandardBasicTypes.BIG_DECIMAL, //15
				StandardBasicTypes.INTEGER, //16
				StandardBasicTypes.STRING,	//17
				StandardBasicTypes.STRING,	//18
				StandardBasicTypes.STRING,	//19
				StandardBasicTypes.STRING,	//20
				StandardBasicTypes.STRING,	//20.1
				StandardBasicTypes.INTEGER,	//21
				StandardBasicTypes.INTEGER,	//22
				StandardBasicTypes.INTEGER, // 23
				StandardBasicTypes.INTEGER, // 24
				StandardBasicTypes.INTEGER, // 25
				StandardBasicTypes.INTEGER, // 26
		};

		StringBuilder countSql = new StringBuilder();
		countSql.append(" Select count(*)as count from ( ").append(sql).append(" ) ");
		
		sql.append(" ORDER BY equipmentCode ASC  ");
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipmentDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	////////////////////////////// END THANH LY TAI SAN; VuongMQ; 08/07/2015 //////////////////////////////
	@Override
	public List<EquipmentVO> getListEquipmentVOByEquiId(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select DISTINCT eq.equip_id as id ");
		sql.append("  ,eq.code as code ");
		sql.append("  ,eq.serial as serial  ");
		sql.append("  ,(case when eq.health_status is not null then (select min(value) from ap_param ap where ap.status = 1 and ap.ap_param_code like eq.health_status and type like 'EQUIP_CONDITION')else null end) as healthStatus ");
		sql.append("  ,eg.name as equipGroupName ");
		sql.append("  ,ec.name as equipCategoryName ");
		sql.append("  from equipment eq  ");
		sql.append("  join equip_group eg on eg.equip_group_id = eq.equip_group_id  ");
		sql.append("   join equip_category ec on eg.equip_category_id = ec.equip_category_id  ");
		sql.append(" where eq.equip_id = ? ");
		params.add(id);
		String[] fieldNames = { "id", "code", "serial", "healthStatus", "equipGroupName", "equipCategoryName" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(EquipmentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<EquipLostMobileRecVO> getListEquipLostMobileRecByFilter(EquipmentFilter<EquipLostMobileRecVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select elmr.equip_lost_mobile_rec_id as id, ");
		sql.append(" elmr.lost_date as lostDate, ");
		sql.append(" elmr.last_arising_sales_date as lastArisingSalesDate, ");
		sql.append(" (select s.shop_code from shop s where s.shop_id = c.shop_id) as shopCode, ");
		sql.append(" (select s.shop_name from shop s where s.shop_id = c.shop_id) as shopName, ");
		sql.append(" c.short_code || '-' || c.customer_name as stockCodeName, ");
		sql.append(" c.short_code as stockCode, c.customer_name as stockName,c.address as stockAddress, ");
		sql.append(" st.staff_code || '-' || st.staff_name as staffCodeName,elmr.equip_lost_rec_id as equipLostId, ");
		sql.append(" elmr.record_status as recordStatus, ");
		sql.append(" c.customer_id as customerId,  elmr.equip_id as equipId ");
		sql.append(" from equip_lost_mobile_rec elmr ");
		sql.append(" join customer c on c.customer_id = elmr.stock_id");
		sql.append(" join staff st on st.staff_id = elmr.report_staff_id and st.status = 1 ");
		sql.append(" where 1 = 1 ");
		if (filter.getShopId() != null) {
			sql.append(" and c.shop_id = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getShopRoot() != null) {
			sql.append(" and c.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? and status = 1 connect by prior shop_id = parent_shop_id) ");
			params.add(filter.getShopRoot());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and (upper(c.short_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().toUpperCase()));
			
			sql.append(" or upper(c.customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getCustomerCode().toUpperCase()));
			
			sql.append(" or upper(c.address) like ? ESCAPE '/') ");
			params.add(StringUtility.toOracleSearchLike(filter.getCustomerCode().toUpperCase()));
		}

//		if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
//			sql.append(" and upper(c.customer_name) like ? ESCAPE '/' ");
//			params.add(StringUtility.toOracleSearchLike(filter.getCustomerName().toUpperCase()));
//		}
//
//		if (!StringUtility.isNullOrEmpty(filter.getCustomerAddress())) {
//			sql.append(" and lower(c.address) like ? ESCAPE '/' ");
//			params.add(StringUtility.toOracleSearchLike(filter.getCustomerAddress().toLowerCase()));
//		}

		if (filter.getFromDate() != null) {
			sql.append(" and lost_date >= trunc(?) ");
			params.add(filter.getFromDate());
		}

		if (filter.getToDate() != null) {
			sql.append(" and lost_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}

		if (filter.getRecordStatus() != null) {
			sql.append(" and elmr.record_status = ? ");
			params.add(filter.getRecordStatus());
		}

		if (filter.getEquipLostId() != null && filter.getEquipLostId() > 1) {
			sql.append(" and elmr.equip_lost_rec_id = -1 ");

		} else if (filter.getEquipLostId() != null && filter.getEquipLostId() == 1) {
			sql.append(" and elmr.equip_lost_rec_id > -1 ");
		}
		sql.append(" order by shopCode asc, elmr.lost_date desc, stockCode asc, staffCodeName asc ");

		String[] fieldNames = { "id",// 0
				"lostDate",// 1
				"lastArisingSalesDate",// 1
				"shopCode", // 1.1
				"shopName", // 1.2
				"stockCodeName", // 1.2
				"stockCode",// 2
				"stockName",// 3
				"stockAddress",// 4
				"staffCodeName",// 5
				"equipLostId",// 6
				"recordStatus", "customerId", "equipId" };
		Type[] fieldTypes = { StandardBasicTypes.LONG,// 0
				StandardBasicTypes.TIMESTAMP,// 1
				StandardBasicTypes.TIMESTAMP,// 1
				StandardBasicTypes.STRING,// 1.1
				StandardBasicTypes.STRING,// 1.2
				StandardBasicTypes.STRING,// 1.2
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.LONG,// 6
				StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.LONG };
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) as count from ( ").append(sql).append(" ) ");

		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(EquipLostMobileRecVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(EquipLostMobileRecVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<EquipLostMobileRec> searchEquipRecordFilter(EquipRecordFilter<EquipLostMobileRec> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_lost_mobile_rec e ");
		sql.append("	where 1 = 1   ");
		if (filter.getLstId() != null && !filter.getLstId().isEmpty()) {
			sql.append("	and e.equip_lost_mobile_rec_id in (?   ");
			params.add(filter.getLstId().get(0));
			for (int i = 1, size = filter.getLstId().size(); i < size; i++) {
				sql.append(" ,? ");
				params.add(filter.getLstId().get(i));
			}
			sql.append(" )  ");
		}
		
		if (filter.getShopId() != null) {
			sql.append(" AND e.stock_id IN ");
			sql.append(" (SELECT cu.customer_id ");
			sql.append(" FROM customer cu ");
			sql.append(" WHERE  ");
			sql.append(" cu.shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			params.add(filter.getShopId());
		}
		
		if (filter.getkPaging() != null) {
			return repo.getListBySQLPaginated(EquipLostMobileRec.class, sql.toString(), params, filter.getkPaging());
		}
		return repo.getListBySQL(EquipLostMobileRec.class, sql.toString(), params);
	}
	
	/**
	 * list detail
	 * @author phuongvm u ke
	 */
	@Override
	public List<StatisticCheckingVO> getListDetailShelf(EquipStatisticFilter filter) throws DataAccessException {
		StringBuilder SQL = new StringBuilder();
		List<Object> params = new ArrayList<Object>();	
		SQL.append("WITH listUnit AS");
		SQL.append("  ( SELECT sh.shop_id shopId, sh.shop_code shopCode, sh.shop_name shopName FROM shop sh WHERE sh.shop_id IN \n");
		SQL.append("    (SELECT unit_id FROM equip_statistic_unit WHERE EQUIP_STATISTIC_RECORD_ID = ? \n");
		params.add(filter.getRecordId());// truong nay chac chan co
		SQL.append("    ) \n");
		SQL.append("  ), \n");
		SQL.append("  listEquip AS \n");
		SQL.append("  ( SELECT stock_total.shop_id shopId, product.PRODUCT_ID equipId, product.PRODUCT_CODE equipCode, product.PRODUCT_NAME equipName, warehouse.warehouse_ID stockId, stock_total.STOCK_TOTAL_ID stockTotalId, warehouse.warehouse_CODE stockCode, warehouse.warehouse_NAME stockName FROM product INNER JOIN stock_total ON product.product_id = stock_total.product_id AND stock_total.OBJECT_TYPE = 1 INNER JOIN warehouse ON warehouse.shop_id = stock_total.shop_id and warehouse.warehouse_id = stock_total.warehouse_id WHERE product.PRODUCT_ID IN \n");
		SQL.append("    (SELECT OBJECT_ID FROM equip_statistic_group WHERE STATUS = 1 AND OBJECT_TYPE = 2 \n");
		SQL.append("    ) AND stock_total.shop_id IN \n");
		SQL.append("    (SELECT shopId FROM listUnit \n");
		SQL.append("    ) \n");
		SQL.append("  ), \n");
		SQL.append("  listShopEquip AS \n");
		SQL.append("  ( SELECT listUnit.shopId, listUnit.shopCode, listUnit.shopName, listEquip.equipId, listEquip.equipCode, listEquip.equipName, listEquip.stockId, listEquip.stockTotalId, listEquip.stockCode, listEquip.stockName FROM listUnit INNER JOIN listEquip ON listUnit.shopId = listEquip.shopId \n");
		SQL.append("  ), \n");
		
		SQL.append(" equipStatisticRecDtl AS (SELECT dt.STATISTIC_TIME,");
		SQL.append("  dt.QUANTITY, ");
		SQL.append("  dt.QUANTITY_ACTUALLY,");
		SQL.append("  dt.EQUIP_STATISTIC_REC_DTL_ID,");
		SQL.append("  dt.OBJECT_ID,");
		SQL.append("  dt.OBJECT_STOCK_ID,");
		SQL.append("  dt.shop_id,");
		SQL.append("  dt.date_in_week,");
		SQL.append("  dt.staff_id,");
		SQL.append("   dt.STATISTIC_Date,");
		SQL.append("  c.customer_id,");
		SQL.append("   c.customer_name,");
		SQL.append("   c.address,");
		SQL.append("   c.name_text,");
		SQL.append("   c.short_code");
		SQL.append("   FROM equip_statistic_rec_dtl dt ");
		SQL.append("   left join customer c on c.customer_id = dt.OBJECT_STOCK_ID and dt.object_stock_type = ?");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		SQL.append("   where dt.equip_statistic_record_id =  ?");
		params.add(filter.getRecordId());// truong nay chac chan co
		if (filter.getNotStatus() != null) {
			SQL.append(" and dt.status != ? ");
			params.add(filter.getNotStatus());
		}
		SQL.append("    order by dt.shop_id,dt.OBJECT_ID");
		SQL.append("    )");
		
		SQL.append("SELECT listShopEquip.shopId, listShopEquip.shopCode, listShopEquip.shopName, equip_statistic_rec_dtl.short_code customerCode,equip_statistic_rec_dtl.customer_name customerName, listShopEquip.equipId, listShopEquip.equipCode,equip_statistic_rec_dtl.address, listShopEquip.equipName, listShopEquip.stockId, listShopEquip.stockTotalId, listShopEquip.stockCode, listShopEquip.stockName, equip_statistic_rec_dtl.STATISTIC_TIME stepCheck, equip_statistic_rec_dtl.QUANTITY quantity, equip_statistic_rec_dtl.QUANTITY_ACTUALLY actualQuantity,equip_statistic_rec_dtl.date_in_week  dateInWeek, to_char(equip_statistic_rec_dtl.STATISTIC_Date,'dd/mm/yyyy') statisticDate, equip_statistic_rec_dtl.EQUIP_STATISTIC_REC_DTL_ID detailId, ");
		SQL.append(" (select count(1) from EQUIP_ATTACH_FILE where EQUIP_ATTACH_FILE.OBJECT_ID = equip_statistic_rec_dtl.EQUIP_STATISTIC_REC_DTL_ID and EQUIP_ATTACH_FILE.OBJECT_TYPE = ?) numImage ");
		params.add(EquipTradeType.INVENTORY.getValue());
		SQL.append("   ,st.staff_code staffCode,st.staff_name staffName");
		SQL.append(" FROM listShopEquip JOIN equipStatisticRecDtl equip_statistic_rec_dtl ON listShopEquip.shopId = equip_statistic_rec_dtl.shop_id AND listShopEquip.equipId = equip_statistic_rec_dtl.OBJECT_ID");
		SQL.append(" left join staff st on st.staff_id = equip_statistic_rec_dtl.staff_id");
		if(filter.getStepCheck() != null) {
			SQL.append(" and equip_statistic_rec_dtl.STATISTIC_TIME = ? ");
			params.add(filter.getStepCheck());
		}
		SQL.append(" where 1 = 1 ");
		if(filter.getShopId() != null) {
			SQL.append(" and listShopEquip.shopId = ? ");
			params.add(filter.getShopId());
		}
		
		if(filter.getStepCheck() != null) {
			SQL.append(" and equip_statistic_rec_dtl.STATISTIC_TIME = ? ");
			params.add(filter.getStepCheck());
		}
		
		if(filter.getProductId() != null) {
			SQL.append(" and listShopEquip.equipId = ? ");
			params.add(filter.getProductId());
		}
		if(filter.getStockId() != null) {
			SQL.append(" and listShopEquip.stockId = ? ");
			params.add(filter.getStockId());
		}
		if(!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			SQL.append(" AND listShopEquip.shopId in (select shop_id from shop where shop_code like  ? ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			SQL.append(" AND equip_statistic_rec_dtl.short_code like  ? ESCAPE '/'  ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().trim().toUpperCase()));
		}
//		if(!StringUtility.isNullOrEmpty(filter.getCustomerNameAddress())) {
//			SQL.append(" AND (equip_statistic_rec_dtl.customer_name like  ? ESCAPE '/' or equip_statistic_rec_dtl.address like  ? ESCAPE '/' )");
//			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getCustomerNameAddress().trim())));
//			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getCustomerNameAddress().trim())));
//		}
		//tamvnm: 03/09/2015 sua lai dieu kien tim kiem
		if(!StringUtility.isNullOrEmpty(filter.getCustomerNameAddress())) {
			SQL.append(" AND equip_statistic_rec_dtl.name_text like  ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getCustomerNameAddress().trim())).toUpperCase());
		}
//		if(!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
//			SQL.append(" AND lower(st.staff_code) like  ? ESCAPE '/'  ");
//			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().trim().toLowerCase()));
//		}
		//tamvnm: 03/09/2015 sua lai dieu kien tim kiem
		if(!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			SQL.append(" AND st.staff_code like  ? ESCAPE '/'  ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().trim().toUpperCase()));
		}
//		if(!StringUtility.isNullOrEmpty(filter.getStaffName())) {
//			SQL.append(" AND (lower(UNICODE2ENGLISH(st.staff_name)) like  ? ESCAPE '/' or lower(UNICODE2ENGLISH(st.name_text)) like  ? ESCAPE '/' )");
//			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getStaffName().trim().toLowerCase())));
//			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getStaffName().trim().toLowerCase())));
//		}
		//tamvnm: 03/09/2015 sua lai dieu kien tim kiem
		if(!StringUtility.isNullOrEmpty(filter.getStaffName())) {
			SQL.append(" AND st.name_text like  ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getStaffName().trim().toUpperCase())));
		}
		if(!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			SQL.append(" AND listShopEquip.equipCode like  ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipCode().trim().toUpperCase()));
		}
		if (filter.getFromDate() != null) {
			SQL.append(" and trunc(equip_statistic_rec_dtl.STATISTIC_Date) >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			SQL.append(" and trunc(equip_statistic_rec_dtl.STATISTIC_Date) < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if(!StringUtility.isNullOrEmpty(filter.getDateInWeek())) {
			SQL.append(" AND equip_statistic_rec_dtl.date_in_week = ? ");
			params.add(filter.getDateInWeek());
		}
		SQL.append(" order by  listShopEquip.shopCode,equip_statistic_rec_dtl.short_code,listShopEquip.equipCode ");
		
		String[] fieldNames = {
			"shopId",//1
			"shopCode",//2
			"shopName",//3
			"equipId",//4
			"equipCode",//5
			"equipName",//6
			"stockId",//7
			"stockTotalId",//8
			"stockCode",//9
			"stockName",//10
			"stepCheck",//11
			"quantity",//12
			"actualQuantity",//13
			"detailId",//14
			"numImage",
			"customerCode",
			"customerName",
			"dateInWeek",
			"statisticDate",
			"address",
			"staffCode",
			"staffName",
		};
		Type[] fieldTypes = {
			StandardBasicTypes.LONG,//1
			StandardBasicTypes.STRING,//2
			StandardBasicTypes.STRING,//3
			StandardBasicTypes.LONG,//4
			StandardBasicTypes.STRING,//5
			StandardBasicTypes.STRING,//6
			StandardBasicTypes.LONG,//7
			StandardBasicTypes.LONG,//8
			StandardBasicTypes.STRING,//9
			StandardBasicTypes.STRING,//10
			StandardBasicTypes.INTEGER,//11
			StandardBasicTypes.INTEGER,//12
			StandardBasicTypes.INTEGER,//13
			StandardBasicTypes.LONG,//14
			StandardBasicTypes.INTEGER,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
		};
		
		if(filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StatisticCheckingVO.class, fieldNames, fieldTypes, SQL.toString(), params);
		} else {
			String countSQL = "select count(1) as count from (" + SQL.toString() + ")";
			return repo.getListByQueryAndScalarPaginated(StatisticCheckingVO.class, fieldNames, fieldTypes, SQL.toString(), countSQL, params, params, filter.getkPaging());
		}
	}
	
	/**
	 * list detail
	 * @author phuongvm  thiet bi
	 *//*
	@Override
	public List<StatisticCheckingVO> getListDetailGroup(EquipStatisticFilter filter) throws DataAccessException {
		StringBuilder SQL = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		SQL.append("WITH listUnit AS");
		SQL.append("  ( SELECT sh.shop_id shopId, sh.shop_code shopCode, sh.shop_name shopName FROM shop sh WHERE sh.shop_id IN \n");
		SQL.append("    (SELECT unit_id FROM equip_statistic_unit WHERE EQUIP_STATISTIC_RECORD_ID = ? \n");
		params.add(filter.getRecordId());// truong nay chac chan co
		SQL.append("    ) \n");
		SQL.append("  ), \n");
		SQL.append(" listEquip AS");
		SQL.append("  (SELECT equipment.EQUIP_ID equipId,");
		SQL.append("   equipment.CODE equipCode,");
		SQL.append("   equipment.stock_id,");
		SQL.append("   equipment.stock_type,");
		SQL.append("   equip_group.EQUIP_GROUP_ID equipGroupId,");
		SQL.append("  equip_group.CODE equipCodeGroup,");
		SQL.append("  equip_group.NAME equipNameGroup,");
		SQL.append("   equipment.SERIAL seri");
		SQL.append("  FROM equipment");
		SQL.append("  INNER JOIN equip_group");
		SQL.append("  ON equipment.EQUIP_GROUP_ID       = equip_group.EQUIP_GROUP_ID");
		SQL.append("  WHERE equip_group.EQUIP_GROUP_ID IN");
		SQL.append("    (SELECT OBJECT_ID");
		SQL.append("   FROM equip_statistic_group");
		SQL.append("   WHERE STATUS    = 1");
		SQL.append("  AND OBJECT_TYPE = 1");
		SQL.append("  AND  EQUIP_STATISTIC_RECORD_ID = ?");
		params.add(filter.getRecordId());// truong nay chac chan co
		SQL.append("   )");
		SQL.append("   ),");
		
		SQL.append(" listShopEquip AS");
		SQL.append("  (SELECT s.shop_id shopId,");
		SQL.append("   s.shop_code shopCode,");
		SQL.append("  s.shop_name shopName,");
		SQL.append("  listEquip.STOCK_ID stockId,");
		SQL.append("  equip_stock.CODE stockCode,");
		SQL.append("  equip_stock.NAME stockName,");
		SQL.append("  listEquip.equipId,");
		SQL.append(" listEquip.equipCode,");
		SQL.append("  listEquip.equipGroupId,");
		SQL.append("  listEquip.equipCodeGroup,");
		SQL.append("  listEquip.equipNameGroup,");
		SQL.append(" listEquip.seri");
		SQL.append("  FROM listEquip");
		SQL.append("  join equip_stock equip_stock on equip_stock.equip_stock_id = listEquip.STOCK_ID and listEquip.STOCK_type = ?");
		params.add(EquipStockTotalType.KHO.getValue());
		SQL.append("  join shop s on s.shop_id = equip_stock.shop_id");
		
		SQL.append("   union");
		SQL.append("   SELECT s.shop_id shopId,");
		SQL.append("   s.shop_code shopCode,");
		SQL.append("   s.shop_name shopName,");
		SQL.append("   listEquip.STOCK_ID stockId,");
		SQL.append("   null stockCode,");
		SQL.append("   null stockName,");
		SQL.append("   listEquip.equipId,");
		SQL.append("   listEquip.equipCode,");
		SQL.append("   listEquip.equipGroupId,");
		SQL.append("   listEquip.equipCodeGroup,");
		SQL.append("   listEquip.equipNameGroup, ");
		SQL.append("   listEquip.seri");
		SQL.append("  FROM listEquip ");
		SQL.append("  join customer c on c.customer_id = listEquip.STOCK_ID and listEquip.STOCK_type = ?");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		SQL.append("  join shop s on s.shop_id = c.shop_id ");
		SQL.append("   )");
		SQL.append("   ,");
		SQL.append(" equipStatisticRecDtl AS (SELECT dt.STATISTIC_TIME,");
		SQL.append("  dt.QUANTITY, ");
		SQL.append("  dt.QUANTITY_ACTUALLY,");
		SQL.append("  dt.EQUIP_STATISTIC_REC_DTL_ID,");
		SQL.append("  dt.OBJECT_ID,");
		SQL.append("  dt.OBJECT_STOCK_ID,");
		SQL.append("  dt.shop_id,");
		SQL.append("  dt.date_in_week,");
		SQL.append("   dt.STATISTIC_Date,");
		SQL.append("   dt.status,");
		SQL.append("  c.customer_id,");
		SQL.append("   c.customer_name,");
		SQL.append("   c.address,");
		SQL.append("   c.short_code");
		SQL.append("   FROM equip_statistic_rec_dtl dt ");
//		SQL.append("   INNER JOIN equip_statistic_customer etc on etc.equip_statistic_record_id = dt.equip_statistic_record_id");
		SQL.append("   left join customer c on c.customer_id = dt.OBJECT_STOCK_ID and dt.object_stock_type =?");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		SQL.append("   where dt.equip_statistic_record_id =  ? and dt.object_id in (select equipId from listShopEquip)");
		params.add(filter.getRecordId());// truong nay chac chan co
		SQL.append("    order by dt.shop_id,dt.OBJECT_ID");
		SQL.append("    )");
		
		SQL.append("SELECT listShopEquip.shopId, listShopEquip.shopCode, listShopEquip.shopName, equip_statistic_rec_dtl.short_code customerCode,equip_statistic_rec_dtl.customer_name customerName,equip_statistic_rec_dtl.status, listShopEquip.equipId,listShopEquip.seri, listShopEquip.equipCode,listShopEquip.equipCodeGroup,listShopEquip.equipNameGroup,equip_statistic_rec_dtl.address, listShopEquip.stockId, listShopEquip.stockCode, listShopEquip.stockName, equip_statistic_rec_dtl.STATISTIC_TIME stepCheck, equip_statistic_rec_dtl.QUANTITY quantity, equip_statistic_rec_dtl.QUANTITY_ACTUALLY actualQuantity,equip_statistic_rec_dtl.date_in_week  dateInWeek, to_char(equip_statistic_rec_dtl.STATISTIC_Date,'dd/mm/yyyy') statisticDate, equip_statistic_rec_dtl.EQUIP_STATISTIC_REC_DTL_ID detailId, ");
		SQL.append(" (select count(1) from EQUIP_ATTACH_FILE where EQUIP_ATTACH_FILE.OBJECT_ID = equip_statistic_rec_dtl.EQUIP_STATISTIC_REC_DTL_ID and EQUIP_ATTACH_FILE.OBJECT_TYPE = ?) numImage ");
		params.add(EquipTradeType.INVENTORY.getValue());
		SQL.append(" FROM listShopEquip JOIN equipStatisticRecDtl equip_statistic_rec_dtl ON listShopEquip.shopId = equip_statistic_rec_dtl.shop_id AND listShopEquip.stockId = equip_statistic_rec_dtl.OBJECT_STOCK_ID AND listShopEquip.equipId = equip_statistic_rec_dtl.OBJECT_ID");

		if(filter.getStepCheck() != null) {
			SQL.append(" and equip_statistic_rec_dtl.STATISTIC_TIME = ? ");
			params.add(filter.getStepCheck());
		}
		SQL.append(" where 1 = 1 ");
		if(filter.getShopId() != null) {
			SQL.append(" and listShopEquip.shopId = ? ");
			params.add(filter.getShopId());
		}
		if(filter.getEquipGroupId() != null) {
			SQL.append(" and listShopEquip.equipGroupId = ? ");
			params.add(filter.getEquipGroupId());
		}
		if(!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			SQL.append(" AND listShopEquip.equipCode like  ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipCode().trim().toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getSeri())) {
			SQL.append(" AND listShopEquip.seri like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeri().trim().toUpperCase()));
		}
		if(filter.getStockId() != null) {
			SQL.append(" and listShopEquip.stockId = ? ");
			params.add(filter.getStockId());
		}
		if(!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			SQL.append(" AND listShopEquip.shopId in (select shop_id from shop where shop_code like  ? ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			SQL.append(" AND equip_statistic_rec_dtl.short_code like  ? ESCAPE '/'  ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().trim().toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getCustomerNameAddress())) {
			SQL.append(" AND (equip_statistic_rec_dtl.customer_name like  ? ESCAPE '/' or equip_statistic_rec_dtl.address like  ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getCustomerNameAddress().trim())));
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getCustomerNameAddress().trim())));
		}
		if(filter.getStatus() != null && filter.getStatus() > -1) {
			SQL.append(" and equip_statistic_rec_dtl.status = ? ");
			params.add(filter.getStatus());
		}
		if (filter.getFromDate() != null) {
			SQL.append(" and trunc(equip_statistic_rec_dtl.STATISTIC_Date) >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			SQL.append(" and trunc(equip_statistic_rec_dtl.STATISTIC_Date) < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if(!StringUtility.isNullOrEmpty(filter.getDateInWeek())) {
			SQL.append(" AND equip_statistic_rec_dtl.date_in_week = ? ");
			params.add(filter.getDateInWeek());
		}
		SQL.append(" order by  listShopEquip.shopCode,equip_statistic_rec_dtl.short_code,listShopEquip.equipCode ");
		
		String[] fieldNames = {
			"shopId",//1
			"shopCode",//2
			"shopName",//3
			"equipId",//4
			"equipCode",//5
//			"equipName",//6
			"stockId",//7
//			"stockTotalId",//8
			"stockCode",//9
			"stockName",//10
			"stepCheck",//11
			"quantity",//12
			"actualQuantity",//13
			"detailId",//14
			"numImage",
			"customerCode",
			"customerName",
			"dateInWeek",
			"statisticDate",
			"address",
			"status",
			"seri",
			"equipNameGroup",
			"equipCodeGroup",
		};
		Type[] fieldTypes = {
			StandardBasicTypes.LONG,//1
			StandardBasicTypes.STRING,//2
			StandardBasicTypes.STRING,//3
			StandardBasicTypes.LONG,//4
			StandardBasicTypes.STRING,//5
//			StandardBasicTypes.STRING,//6
			StandardBasicTypes.LONG,//7
//			StandardBasicTypes.LONG,//8
			StandardBasicTypes.STRING,//9
			StandardBasicTypes.STRING,//10
			StandardBasicTypes.INTEGER,//11
			StandardBasicTypes.INTEGER,//12
			StandardBasicTypes.INTEGER,//13
			StandardBasicTypes.LONG,//14
			StandardBasicTypes.INTEGER,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.INTEGER,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
		};
		
		if(filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(StatisticCheckingVO.class, fieldNames, fieldTypes, SQL.toString(), params);
		} else {
			String countSQL = "select count(1) as count from (" + SQL.toString() + ")";
			return repo.getListByQueryAndScalarPaginated(StatisticCheckingVO.class, fieldNames, fieldTypes, SQL.toString(), countSQL, params, params, filter.getkPaging());
		}
	}*/
	
	/**
	 * list detail
	 * @author phuongvm  thiet bi
	 */
	@Override
	public List<StatisticCheckingVO> getListDetailGroup(EquipStatisticFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH had_record AS ");
		sql.append(" (SELECT sh.shop_id shopId, ");
		sql.append(" sh.shop_code shopCode, ");
		sql.append(" sh.shop_name shopName, ");
		sql.append(" c.short_code customerCode, ");
		sql.append(" c.customer_name customerName, ");
		sql.append(" c.name_text customerNameText, ");
		sql.append(" c.address, ");
		sql.append(" st.staff_code AS staffCode, ");
		sql.append(" st.staff_name AS staffName, ");
		sql.append(" st.name_text AS staffNameText, ");
		sql.append(" dt.status, ");
		//sql.append(" dt.STATISTIC_TIME stepCheck, ");
		sql.append(" dt.date_in_week dateInWeek, ");
		sql.append(" dt.STATISTIC_DATE statisticDate, ");
		sql.append(" dt.EQUIP_STATISTIC_REC_DTL_ID detailId, ");
		sql.append(" e.equip_id equipId, ");
		sql.append(" e.serial seri, ");
		sql.append(" e.code equipCode, ");
		sql.append(" eg.code equipCodeGroup, ");
		sql.append(" eg.name equipNameGroup, ");
		sql.append(" ec.name equipCategoryName, ");
		sql.append(" (SELECT COUNT(1) ");
		sql.append(" FROM EQUIP_ATTACH_FILE ");
		sql.append(" WHERE EQUIP_ATTACH_FILE.OBJECT_ID = dt.EQUIP_STATISTIC_REC_DTL_ID ");
		sql.append(" AND EQUIP_ATTACH_FILE.OBJECT_TYPE = 7 ");
		sql.append(" ) numImage ");
		sql.append(" FROM equip_statistic_rec_dtl dt ");
		sql.append(" JOIN equipment e ON e.equip_id = dt.object_id ");
		sql.append(" JOIN equip_group eg ON eg.equip_group_id = e.equip_group_id ");
		sql.append(" JOIN equip_category ec ON eg.equip_category_id = ec.equip_category_id ");
		sql.append(" JOIN customer c ON c.customer_id = dt.object_stock_id AND dt.object_stock_type = ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append(" JOIN shop sh ON sh.shop_id = c.shop_id ");
		sql.append(" LEFT JOIN staff st ON st.staff_id = dt.staff_id ");
		sql.append(" WHERE 1 = 1  ");
		if (filter.getRecordId() != null) {
			sql.append(" and dt.equip_statistic_record_id = ? ");
			params.add(filter.getRecordId());
		}
		if (filter.getNotStatus() != null) {
			sql.append(" and dt.status != ? ");
			params.add(filter.getNotStatus());
		}
		if(filter.getStatus() != null && filter.getStatus() > -1) {
			sql.append(" and dt.status = ? ");
			params.add(filter.getStatus());
		}
		if(filter.getStepCheck() != null) {
			sql.append(" and dt.STATISTIC_TIME = ? ");
			params.add(filter.getStepCheck());
		}
		if(filter.getShopId() != null) {
			sql.append(" and sh.shopId = ? ");
			params.add(filter.getShopId());
		}
		if(!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" AND lower(sh.shopCode) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			sql.append(" AND lower(e.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipCode().trim().toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getSeri())) {
			sql.append(" AND lower(e.serial) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeri().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and (upper(c.short_code) like ? ESCAPE '/' or c.name_text like ? ESCAPE '/')");
			String customerCode = Unicode2English.codau2khongdau(filter.getCustomerCode().trim().toUpperCase());
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
		}
		if(!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" AND (upper(st.staff_code) like  ? ESCAPE '/' OR st.name_text like ? ESCAPE '/' )");
			String staffCode = Unicode2English.codau2khongdau(filter.getStaffCode().trim().toUpperCase());
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode));
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode));
		}
		if (filter.getFromDate() != null) {
			sql.append(" and dt.STATISTIC_DATE >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and dt.STATISTIC_DATE < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		sql.append(" ), ");
		sql.append(" no_record AS ");
		sql.append(" (SELECT sh.shop_id shopId, ");
		sql.append(" sh.shop_code shopCode, ");
		sql.append(" sh.shop_name shopName, ");
		sql.append(" c.short_code customerCode, ");
		sql.append(" c.customer_name customerName, ");
		sql.append(" c.name_text customerNameText, ");
		sql.append(" c.address, ");
		sql.append(" NULL AS staffCode, ");
		sql.append(" NULL AS staffName, ");
		sql.append(" NULL AS staffNameText, ");
		sql.append(" NULL status, ");
		sql.append(" NULL stepCheck, ");
		sql.append(" NULL dateInWeek, ");
		sql.append(" NULL statisticDate, ");
		sql.append(" NULL detailId, ");
		sql.append(" e.equip_id equipId, ");
		sql.append(" e.serial seri, ");
		sql.append(" e.code equipCode, ");
		sql.append(" eg.code equipCodeGroup, ");
		sql.append(" eg.name equipNameGroup, ");
		sql.append(" ec.name equipCategoryName, ");
		sql.append(" NULL numImage ");
		sql.append(" FROM equipment e ");
		sql.append(" JOIN equip_group eg ON eg.equip_group_id = e.equip_group_id ");
		sql.append(" JOIN equip_category ec ON eg.equip_category_id = ec.equip_category_id ");
		sql.append(" JOIN customer c ON c.customer_id = e.stock_id AND e.STOCK_TYPE = ? ");
		params.add(EquipStockTotalType.KHO_KH.getValue());
		sql.append(" JOIN shop sh ON sh.shop_id = c.shop_id ");
		sql.append(" WHERE e.equip_id NOT IN (SELECT equipId FROM had_record) ");
		sql.append(" ) ");
		
		sql.append(" SELECT * ");
		sql.append(" FROM ");
		sql.append(" (SELECT shopId, shopCode, shopName, customerCode, customerName, customerNameText, address, staffCode, staffName, staffNameText, status, ");
		sql.append(" 	(SELECT COUNT(1) FROM had_record where equipId = hr.equipId) stepCheck, ");
		sql.append(" 	TO_CHAR(statisticDate,'dd/mm/yyyy') statisticDate, ");
		sql.append(" 	dateInWeek, detailId, equipId, seri, equipCode, equipCodeGroup, equipNameGroup, equipCategoryName, numImage ");
		sql.append(" FROM had_record hr ");
		sql.append(" WHERE statisticDate = (SELECT MAX(statisticDate) FROM had_record) ");
		sql.append(" UNION ");
		sql.append(" SELECT shopId, shopCode, shopName, customerCode, customerName, customerNameText, address, staffCode, staffName, staffNameText, status, stepCheck, ");
		sql.append(" 	TO_CHAR(statisticDate,'dd/mm/yyyy') statisticDate, ");
		sql.append(" 	dateInWeek, detailId, equipId, seri, equipCode, equipCodeGroup, equipNameGroup, equipCategoryName, numImage ");
		sql.append(" FROM no_record ");
		sql.append(" ) ");
		sql.append(" where 1 = 1 ");
		
		if (filter.getNotStatus() != null) {
			sql.append(" and status != ? ");
			params.add(filter.getNotStatus());
		}
		if(filter.getStatus() != null && filter.getStatus() > -1) {
			sql.append(" and status = ? ");
			params.add(filter.getStatus());
		}
		if(filter.getStepCheck() != null) {
			sql.append(" and stepCheck = ? ");
			params.add(filter.getStepCheck());
		}
		if(filter.getShopId() != null) {
			sql.append(" and shopId = ? ");
			params.add(filter.getShopId());
		}
		if(!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" AND lower(shopCode) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getEquipCode())) {
			sql.append(" AND lower(equipCode) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEquipCode().trim().toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getSeri())) {
			sql.append(" AND lower(seri) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeri().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append(" and (upper(customerCode) like ? ESCAPE '/' or customerNameText like ? ESCAPE '/')");
			String customerCode = Unicode2English.codau2khongdau(filter.getCustomerCode().trim().toUpperCase());
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode));
		}
		if(!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" AND (upper(staffCode) like  ? ESCAPE '/' OR staffNameText like ? ESCAPE '/' )");
			String staffCode = Unicode2English.codau2khongdau(filter.getStaffCode().trim().toUpperCase());
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode));
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode));
		}
		if (filter.getFromDate() != null) {
			sql.append(" and statisticDate >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and statisticDate < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		String order = " ORDER BY shopcode, equipCategoryName, equipNameGroup, customerCode, equipCode, statisticDate DESC ";
		
		String[] fieldNames = {
			"shopId",		//1
			"shopCode",		//2
			"shopName",		//3
			"equipId",		//4
			"equipCode",	//5
//			"equipName",	//6
			"seri", 		//7
			"stepCheck",	//8
			"detailId",		//9
			"numImage",		//10
			"customerCode",	//11
			"customerName",	//12
			"dateInWeek",	//13
			"statisticDate",//14
			"address",		//15
			"status",		//16
			"equipNameGroup",//17
			"equipCodeGroup",//18
			"equipCategoryName",//19
			"staffCode",//20
			"staffName",//21
		};
		Type[] fieldTypes = {
			StandardBasicTypes.LONG,//1
			StandardBasicTypes.STRING,//2
			StandardBasicTypes.STRING,//3
			StandardBasicTypes.LONG,//4
			StandardBasicTypes.STRING,//5
//			StandardBasicTypes.STRING,//6
			StandardBasicTypes.STRING,//7
			StandardBasicTypes.INTEGER,//8
			StandardBasicTypes.LONG,//9
			StandardBasicTypes.INTEGER,//10
			StandardBasicTypes.STRING,//11
			StandardBasicTypes.STRING,//12
			StandardBasicTypes.STRING,//13
			StandardBasicTypes.STRING,//14
			StandardBasicTypes.STRING,//15
			StandardBasicTypes.INTEGER,//16
			StandardBasicTypes.STRING,//17
			StandardBasicTypes.STRING,//18
			StandardBasicTypes.STRING,//19
			StandardBasicTypes.STRING,//20
			StandardBasicTypes.STRING,//21
		};
		
		if(filter.getkPaging() == null) {
			sql.append(order);
			return repo.getListByQueryAndScalar(StatisticCheckingVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			String countSQL = "select count(1) as count from (" + sql.toString() + ")";
			sql.append(order);
			return repo.getListByQueryAndScalarPaginated(StatisticCheckingVO.class, fieldNames, fieldTypes, sql.toString(), countSQL, params, params, filter.getkPaging());
		}
	}
	
	@Override
	public EquipStatisticRecDtl geEquipStatisticRecDtlByFilter(EquipStatisticFilter filter) throws DataAccessException {
		StringBuilder SQL = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		SQL.append("select * from EQUIP_STATISTIC_REC_DTL where 1 = 1 and OBJECT_STOCK_TYPE = 1");

		if (filter.getEquipId() != null) {
			SQL.append(" and OBJECT_ID = ? ");
			params.add(filter.getEquipId());
		}
		if (filter.getRecordId() != null) {
			SQL.append(" and EQUIP_STATISTIC_RECORD_ID = ? ");
			params.add(filter.getRecordId());
		}
		if (filter.getStepCheck() != null) {
			SQL.append(" and STATISTIC_TIME = ? ");
			params.add(filter.getStepCheck());
		}
		if (filter.getShopId() != null) {
			SQL.append(" and shop_id = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getStatisticDate() != null) {
			SQL.append(" and statistic_date >= trunc(?) and statistic_date < trunc(?) +1 ");
			params.add(filter.getStatisticDate());
			params.add(filter.getStatisticDate());
		}

		return repo.getFirstBySQL(EquipStatisticRecDtl.class, SQL.toString(), params);
	}
	
	@Override
	public List<EquipmentEvictionVO> getListFormEvictionVOForEquipSuggestEvictionByFilter(EquipmentFilter<EquipmentEvictionVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		if (filter == null) {
			throw new IllegalArgumentException("filter == null");
		}
		if (StringUtility.isNullOrEmpty(filter.getCurShopCode())) {
			throw new IllegalArgumentException("filter.getCurShopCode() == null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" WITH lstEvictionRecord AS ");
		sql.append(" (SELECT edr.EQUIP_EVICTION_FORM_ID AS id, ");
		sql.append(" lstShop.shop_code AS maNPP, ");
		sql.append(" edr.code AS maPhieu, ");		
		sql.append(" cus.short_code AS maKH, ");
		sql.append(" cus.customer_code AS customerCode, ");
		sql.append(" cus.customer_id AS customerId, ");		
		sql.append(" TO_CHAR(edr.create_date, 'dd/mm/yyyy') AS createDate, ");
		sql.append(" edr.FORM_STATUS AS recordStatus, ");
		sql.append(" edr.ACTION_STATUS AS deliveryStatus, ");
		sql.append(" el.code AS equipSugEvictionCode, ");
		sql.append(" Row_Number () Over (Partition BY cus.short_code, edr.FORM_STATUS Order By edr.create_date DESC) AS stt, ");
		sql.append(" DECODE(edr.FORM_STATUS, ?,0 ,1) num ");
		params.add(StatusRecordsEquip.CANCELLATION.getValue());
		sql.append(" FROM EQUIP_EVICTION_FORM edr ");
		sql.append(" JOIN customer cus ");
		sql.append(" ON cus.customer_id = edr.customer_id ");
		sql.append(" AND cus.STATUS = 1 ");
		sql.append(" JOIN ");
		sql.append(" (SELECT * ");
		sql.append(" FROM shop ");
		sql.append(" WHERE status = 1 ");
		sql.append(" START WITH shop_code = ? ");
		params.add(filter.getCurShopCode());
		sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id ");
		sql.append(" ) lstShop ");
		sql.append(" ON lstShop.shop_id = cus.shop_id ");
		sql.append(" LEFT JOIN equip_suggest_eviction el ");
		sql.append(" ON el.equip_suggest_eviction_ID = edr.equip_suggest_eviction_ID ");
		sql.append(" WHERE 1 = 1 ");
		if(filter.getEquipSuggestEvictionId() != null){
			sql.append(" AND edr.equip_suggest_eviction_ID = ? ");
			params.add(filter.getEquipSuggestEvictionId());
		}
		sql.append(" ) ");
		sql.append(" SELECT * ");
		sql.append(" FROM lstEvictionRecord ");
		sql.append(" WHERE customerId in (SELECT customerId FROM lstEvictionRecord ldr ");
		sql.append(" WHERE ldr.stt = 1 ");
		sql.append(" GROUP BY customerId ");
		sql.append(" HAVING SUM(ldr.num) = 0) ");
		sql.append(" and stt = 1 ");
		sql.append(" ORDER BY maPhieu, customerCode ");

		String[] fieldNames = { 
				"id",			// 0
				"maNPP",
				"maPhieu",		// 1
				"maKH",		// 2
				"customerCode",		// 3
				"customerId",		// 4
				"recordStatus",		// 5
				"deliveryStatus",		// 6
		};

		Type[] fieldTypes = { 
				StandardBasicTypes.LONG,	// 0
				StandardBasicTypes.STRING,	
				StandardBasicTypes.STRING,	// 1
				StandardBasicTypes.STRING,	// 2
				StandardBasicTypes.STRING,	// 3
				StandardBasicTypes.LONG,	// 4
				StandardBasicTypes.INTEGER,	// 5
				StandardBasicTypes.INTEGER,	// 6
		};

		return repo.getListByQueryAndScalar(EquipmentEvictionVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public EquipRecordVO getLostRecordNewestByEquipId(Long equipId) throws DataAccessException {
		if (equipId == null) {
			throw new IllegalArgumentException("equipId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select EQUIP_LOST_RECORD_ID id,code   ");
		sql.append(" from EQUIP_LOST_RECORD  ");
		sql.append(" where EQUIP_LOST_RECORD_ID in (  ");
		sql.append(" select form_id from EQUIP_HISTORY where equip_id = ? and CREATE_DATE = (select max(CREATE_DATE) from EQUIP_HISTORY where equip_id = ? and TABLE_NAME = ?) )  ");
		params.add(equipId);
		params.add(equipId);
		params.add(TableName.EQUIP_LOST_RECORD.getValue());
		String[] fieldNames = { "id", "code" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalarFirst(EquipRecordVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<EquipLiquidationFormDtl> getListEquipLiquidationFormDtlByRecordId(Long recordId) throws DataAccessException {
		if (recordId == null) {
			throw new IllegalArgumentException("recordId is null");
		}
		StringBuilder SQL = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		SQL.append("select * from EQUIP_LIQUIDATION_FORM_DTL where EQUIP_LIQUIDATION_FORM_ID = ? ");
		params.add(recordId);
		return repo.getListBySQL(EquipLiquidationFormDtl.class, SQL.toString(), params);
	}
	
	@Override
	public Boolean isExistContractNumInDeliveryRecord(String contractNumber) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(contractNumber)) {
			throw new IllegalArgumentException("contractNumber is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from EQUIP_DELIVERY_RECORD");
		sql.append(" where record_status != ? and CONTRACT_NUMBER = ?");
		params.add(StatusRecordsEquip.CANCELLATION.getValue());
		params.add(contractNumber);
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}
}

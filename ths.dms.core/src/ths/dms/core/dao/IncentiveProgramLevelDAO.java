package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.IncentiveProgramLevel;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.IncentiveProgramLevelFilter;
import ths.dms.core.entities.enumtype.IncentiveProgramLevelVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface IncentiveProgramLevelDAO {

	IncentiveProgramLevel createIncentiveProgramLevel(IncentiveProgramLevel incentiveProgramLevel, LogInfoVO logInfo) throws DataAccessException;

	void deleteIncentiveProgramLevel(IncentiveProgramLevel incentiveProgramLevel, LogInfoVO logInfo) throws DataAccessException;

	void updateIncentiveProgramLevel(IncentiveProgramLevel incentiveProgramLevel, LogInfoVO logInfo) throws DataAccessException;
	
	IncentiveProgramLevel getIncentiveProgramLevelById(long id) throws DataAccessException;

	List<IncentiveProgramLevel> getListIncentiveProgramLevel(Long incentiveProgramId, ActiveType status,
			KPaging<IncentiveProgramLevel> kPaging)
			throws DataAccessException;

	List<IncentiveProgramLevelVO> getListIncentiveProgramLevelVO(
			IncentiveProgramLevelFilter filter,
			KPaging<IncentiveProgramLevelVO> kPaging)
			throws DataAccessException;

	/**
	 * Lay level theo IncentiveProCode 
	 * @author thuattq
	 */
	IncentiveProgramLevel getIncentiveProgramLevelByIncentiveProCode(Long incentiveProgramId,
			String incentiveProCode, String levelCode, ActiveType activeType)
			throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.StockTransLot;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTransLotOwnerType;
import ths.dms.core.entities.vo.StockTransLotVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class StockTransLotDAOImpl implements StockTransLotDAO {

	@Autowired
	private IRepository repo;

	@Override
	public StockTransLot createStockTransLot(StockTransLot stockTransLot) throws DataAccessException {
		if (stockTransLot == null) {
			throw new IllegalArgumentException("stockTransLot");
		}
		StockTransDetail d = stockTransLot.getStockTransDetail(); 
		if (d != null)
			stockTransLot.setStockTrans(d.getStockTrans());
		repo.create(stockTransLot);
		return repo.getEntityById(StockTransLot.class, stockTransLot.getId());
	}

	@Override
	public void deleteStockTransLot(StockTransLot stockTransLot) throws DataAccessException {
		if (stockTransLot == null) {
			throw new IllegalArgumentException("stockTransLot");
		}
		repo.delete(stockTransLot);
	}

	@Override
	public StockTransLot getStockTransLotById(long id) throws DataAccessException {
		return repo.getEntityById(StockTransLot.class, id);
	}
	
	
	@Override
	public void updateStockTransLot(StockTransLot stockTransLot) throws DataAccessException {
		if (stockTransLot == null) {
			throw new IllegalArgumentException("stockTransLot");
		}
		StockTransDetail d = stockTransLot.getStockTransDetail(); 
		if (d != null)
			stockTransLot.setStockTrans(d.getStockTrans());
		repo.update(stockTransLot);
	}
	
	@Override
	public List<StockTransLot> getListStockTransLotByStockTransId(KPaging<StockTransLot> kPaging, long stockTransId) 
			throws DataAccessException {
		String sql = "select * from stock_trans_lot where stock_trans_id=? order by STOCK_TRANS_LOT_ID desc";
		List<Object> params = new ArrayList<Object>();
		params.add(stockTransId);
		if (kPaging != null)
			return repo.getListBySQLPaginated(StockTransLot.class, sql, params, kPaging);
		else 
			return repo.getListBySQL(StockTransLot.class, sql, params);
	}
	
	/**
	 * Ham su dung de tim kiem danh sach chi tiet giao dich kho
	 * @author hoanv25
	 * @since  19 - 11 - 2014
	 */
	@Override
	public List<StockTransLotVO> getListStockTransLotVO(KPaging<StockTransLotVO> kPaging, long stockTransId)
		throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSQL = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		fromSQL.append(" from (");
		/*fromSQL.append(" select prd.product_code productCode,  ");
		fromSQL.append("         prd.product_name productName,  ");
		fromSQL.append("         std.quantity, prd.convfact,  ");
		fromSQL.append("         '' as lot, price, (price*quantity) as total ");
		fromSQL.append(", '' as fromWarehouse");
		fromSQL.append(", '' as toWarehouse");
		fromSQL.append(" from stock_trans_detail std join product prd  ");
		fromSQL.append("     on prd.check_lot=0 and std.product_id=prd.product_id ");
		fromSQL.append(" where stock_trans_id=? ");
		fromSQL.append(" union  ");*/
		fromSQL.append(" select prd.product_code productCode,  ");
		fromSQL.append("         prd.product_name productName,  ");
		fromSQL.append("         stl.quantity, prd.convfact,  ");
		fromSQL.append(" trunc(std.quantity/prd.convfact)  || '/' ||  mod(std.quantity,prd.convfact)  as thungLe,");
		fromSQL.append("         lot, price, (price*stl.quantity) as total ");
		fromSQL.append(", (case stl.from_owner_type when 1 then (select warehouse_code from warehouse where warehouse_id = stl.from_owner_id)");
		fromSQL.append(" when 2 then (select staff_code from staff where staff_id = stl.from_owner_id)");
		fromSQL.append(" when 3 then (select warehouse_code from warehouse where warehouse_id = stl.from_owner_id)");
		fromSQL.append(" else '' end) as fromWarehouse");
		fromSQL.append(", (case stl.to_owner_type when 1 then (select warehouse_code from warehouse where warehouse_id = stl.to_owner_id)");
		fromSQL.append(" when 2 then (select staff_code from staff where staff_id = stl.to_owner_id)");
		fromSQL.append(" when 3 then (select warehouse_code from warehouse where warehouse_id = stl.to_owner_id)");
		fromSQL.append(" else '' end) as toWarehouse");
		fromSQL.append(" from stock_trans_lot stl join product prd ");
		//fromSQL.append("     on prd.check_lot=1 and stl.product_id=prd.product_id ");
		fromSQL.append("     on stl.product_id=prd.product_id ");
		fromSQL.append("     join stock_trans_detail std on std.stock_trans_detail_id=stl.stock_trans_detail_id ");
		fromSQL.append(" where stl.stock_trans_id=? )");
		params.add(stockTransId);
		//params.add(stockTransId);
		sql.append(fromSQL);
		sql.append(" order by productCode, lot");		
		countSql.append(" select count(1) as count ");
		countSql.append(fromSQL);		
		String[] fieldNames = {"productCode", "productName", "quantity", 
								"convfact", "lot", "price", "total",
								"fromWarehouse", "toWarehouse"};
		Type[] fieldTypes = {StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING};
		
		if (kPaging == null)
			return repo.getListByQueryAndScalar(StockTransLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else 
			return repo.getListByQueryAndScalarPaginated(StockTransLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}
	
	@Override
	public List<StockTransLotVO> getListStockTransLotVOForSearchSale(KPaging<StockTransLotVO> kPaging, long stockTransId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select prd.product_code productCode,  ");
		sql.append("         prd.product_name productName,  ");
		sql.append("         stl.quantity, prd.convfact,  ");
		sql.append("         lot, std.price, (price*stl.quantity) as total, ");
//		sql.append("         stl.from_owner_type , ");
		sql.append("         stl.from_owner_id fromOwnerId, ");
//		sql.append("         stl.to_owner_type , ");
		sql.append("         stl.to_owner_id toOwnerId ");
		sql.append(" from stock_trans_lot stl ");
		sql.append("     join product prd on stl.product_id = prd.product_id ");
		sql.append("     join stock_trans_detail std on std.stock_trans_detail_id = stl.stock_trans_detail_id ");
		sql.append(" where stl.stock_trans_id=? ");
		params.add(stockTransId);

		countSql.append(" select count(1) as count from (");
		countSql.append(sql);
		countSql.append(")");

		sql.append(" order by productCode, lot");
		String[] fieldNames = { "productCode", "productName", "quantity", 
								"convfact", "lot", "price", "total", 
								"fromOwnerId", "toOwnerId" 
								};
		Type[] fieldTypes = {StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, 
							StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
							StandardBasicTypes.LONG, StandardBasicTypes.LONG 
							};

		if (kPaging == null)
			return repo.getListByQueryAndScalar(StockTransLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(StockTransLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}
	
	@Override
	public List<StockTransLotVO> getListStockTransLotVOForStockUpdate(KPaging<StockTransLotVO> kPaging, long stockTransId)
		throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select  productCode, productName, quantity, convfact, lot, price, total, ");
		sql.append(" (case when from_owner_type = ? then fromWarehouse when to_owner_type = ? then toWarehouse else null end) as fromWarehouse ");
		params.add(StockTransLotOwnerType.WAREHOUSE.getValue());
		params.add(StockTransLotOwnerType.WAREHOUSE.getValue());
		sql.append(" from (");
		sql.append(" select prd.product_code productCode,  ");
		sql.append("         prd.product_name productName,  ");
		sql.append("         stl.quantity, prd.convfact,  ");
		sql.append("         lot, price, (price*stl.quantity) as total, ");
		sql.append("         stl.from_owner_type , ");
		sql.append("         (select warehouse_name from warehouse where warehouse_id=stl.from_owner_id) fromWarehouse, ");
		sql.append("         stl.to_owner_type , ");
		sql.append("         (select warehouse_name from warehouse where warehouse_id=stl.to_owner_id) toWarehouse ");
		sql.append(" from stock_trans_lot stl join product prd ");
		sql.append("     on stl.product_id=prd.product_id ");
		sql.append("     join stock_trans_detail std on std.stock_trans_detail_id=stl.stock_trans_detail_id ");
		sql.append(" where stl.stock_trans_id=? )");
		params.add(stockTransId);
		sql.append(" order by productCode, lot");
		
		
		countSql.append(" select count(1) as count from (");
		countSql.append(sql);
		countSql.append(")");
		String[] fieldNames = {"productCode", "productName", "quantity", 
								"convfact", "lot", "price",
								"total", "fromWarehouse"
								};
		Type[] fieldTypes = {StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING};
		
		if (kPaging == null)
			return repo.getListByQueryAndScalar(StockTransLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else 
			return repo.getListByQueryAndScalarPaginated(StockTransLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.StockTransLotDAO#getListLot(java.lang.Long, ths.dms.core.entities.enumtype.StockObjectType, java.lang.Long, ths.dms.core.entities.enumtype.StockObjectType, java.util.Date)
	 */
	@Override
	public List<StockTransLotVO> getListLot(Long fromOwnerId,
			StockObjectType fromOwnerType, Long toOwnerId,
			StockObjectType toOwnerType, Date stockTransDate)
			throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct(stl.lot) as lot, p.product_code as productCode from stock_trans st, stock_trans_lot stl, product p where st.stock_trans_id = stl.stock_trans_id");
		sql.append(" and stl.product_id = p.product_id");
		if (fromOwnerId != null) {
			sql.append(" and st.from_owner_id = ? and st.from_owner_type = ?");
			params.add(fromOwnerId);
			params.add(fromOwnerType.getValue());
		}
		if (toOwnerId != null) {
			sql.append(" and st.to_owner_id = ? and st.to_owner_type = ?");
			params.add(toOwnerId);
			params.add(toOwnerType.getValue());
		}
		sql.append(" and st.stock_trans_date >= trunc(?) and st.stock_trans_date < trunc(? + 1)");
		sql.append(" order by stl.lot");
		params.add(stockTransDate);
		params.add(stockTransDate);
		String[] fieldNames = { "productCode", "lot" };
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(StockTransLotVO.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.AttributeValue;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class AttributeValueDAOImpl implements AttributeValueDAO {
	@Autowired
	private IRepository repo;
	
	@Override
	public AttributeValue getValueOfObject(Long objId, Long attributeId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM ATTRIBUTE_VALUE WHERE ATTRIBUTE_ID = ? AND TABLE_ID = ?");
		params.add(attributeId);
		params.add(objId);
		return repo.getEntityBySQL(AttributeValue.class, sql.toString(), params);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public AttributeValue createAttributeValue(AttributeValue attributeValue) throws DataAccessException {
		return repo.create(attributeValue);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateAttributeValue(AttributeValue attributeValue) throws DataAccessException {
		repo.update(attributeValue);
	}
}

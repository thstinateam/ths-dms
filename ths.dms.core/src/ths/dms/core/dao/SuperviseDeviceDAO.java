package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ToolFilter;
import ths.dms.core.entities.vo.ToolVO;
import ths.dms.core.exceptions.DataAccessException;
/**
 * 
 * @author tungmt
 * @since 13/3/2014 
 * @description: quan ly tu lanh cho vnm
 */
public interface SuperviseDeviceDAO {
	/**
	 * 
	 * @author tungmt
	 * @since 15/3/2014 
	 * @description: lay danh sach shop va count tu lanh
	 */
	List<ToolVO> getListShopTool(Long parentShopId, Integer typeSup) throws DataAccessException;
	List<ToolVO> getListTool(Long parentShopId, Integer typeSup) throws DataAccessException;
	List<ToolVO> getListShopToolOneNode(Long parentShopId, Integer typeSup) throws DataAccessException;
	List<Shop> getListAncestor(Long parentShopId,String shopCode,String shopName) throws DataAccessException;
	List<ToolVO> getListToolKP(ToolFilter filter) throws DataAccessException;
	List<ToolVO> getListChip(ToolFilter filter) throws DataAccessException;
	List<ToolVO> getListChipForCustomer(ToolFilter filter) throws DataAccessException;
	void updateChip(Long chipId,Float minTemp,Float maxTemp) throws DataAccessException;
	void updateConfig(int tempConfig, int positionConfig) throws DataAccessException;
	void updateConfigDistance(int distanceConfig) throws DataAccessException;
	List<ToolVO> getBCTemperature(ToolFilter filter) throws DataAccessException;
	List<ToolVO> getBCPosition(ToolFilter filter) throws DataAccessException;
}

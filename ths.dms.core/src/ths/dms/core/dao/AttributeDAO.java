package ths.dms.core.dao;

import java.util.List;

import org.apache.poi.hssf.record.formula.functions.T;

import ths.dms.core.entities.Attribute;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AttributeColumnType;
import ths.dms.core.entities.enumtype.AttributeFilter;
import ths.dms.core.entities.enumtype.AttributeVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.TableHasAttribute;
import ths.dms.core.entities.vo.DynamicAttributeVO;
import ths.dms.core.exceptions.DataAccessException;

public interface AttributeDAO {

	Attribute createAttribute(Attribute attribute) throws DataAccessException;

	void deleteAttribute(Attribute attribute) throws DataAccessException;

	void updateAttribute(Attribute attribute) throws DataAccessException;
	
	Attribute getAttributeById(long id) throws DataAccessException;

	Attribute getAttributeByCode(String table, String code)
			throws DataAccessException;

	List<AttributeVO> getListAttributeVO(
			AttributeFilter filter, KPaging<AttributeVO> kPaging)
			throws DataAccessException;
	
	Boolean isUsingByOther(String attributeCode) throws DataAccessException;

	List<Attribute> getListAttribute(KPaging<Attribute> kPaging,
			TableHasAttribute table, String attributeCode,
			String attributeName, AttributeColumnType columnType,
			ActiveType status) throws DataAccessException;
	
	/**
	 * Lay cac thuoc tinh dong theo loai entity
	 * @author tuannd20
	 * @param clazz Loai doi tuong can lay thuoc tinh dong
	 * @param status Trang thai cua thuoc tinh dong
	 * @return Danh sach thuoc tinh dong dang phang (thuoc tinh, gia tri thuoc tinh)
	 * @throws DataAccessException
	 * @since 03/09/2015
	 */
	<T> List<DynamicAttributeVO> getDynamicAttribute(Class<T> clazz, ActiveType status) throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.PoAutoGroupDetail;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailVO;
import ths.dms.core.exceptions.DataAccessException;

public interface PoAutoGroupDetailDAO {

	PoAutoGroupDetail createPoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail) throws DataAccessException;

	void deletePoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail) throws DataAccessException;

	void updatePoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail) throws DataAccessException;

	PoAutoGroupDetail getPoAutoGroupDetailById(Long id) throws DataAccessException;
	
	List<PoAutoGroupDetail> getListPoAutoGroupDetail(KPaging<PoAutoGroupDetail> kPaging,
			Long poAutoGroupId) throws DataAccessException;
	
	List<PoAutoGroupDetailVO> getListPoAutoGroupDetailVO(PoAutoGroupDetailFilter filter, KPaging<PoAutoGroupDetailVO> kPaging) throws DataAccessException;

	List<PoAutoGroupDetailVO> getPoAutoGroupDetailVOByShop(Long shopId)
			throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Kpi;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.CommonFilter;

public interface KpiDAO {
	List<Kpi> getListKpiByFilter(KPaging<Kpi> kPaging, CommonFilter filter) throws DataAccessException;
}

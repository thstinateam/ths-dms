package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.IncentiveProgram;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.IncentiveProgramFilter;
import ths.dms.core.entities.enumtype.IncentiveProgramVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class IncentiveProgramDAOImpl implements IncentiveProgramDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Autowired
	CommonDAO commonDAO;

	@Override
	public IncentiveProgram createIncentiveProgram(IncentiveProgram incentiveProgram, LogInfoVO logInfo) throws DataAccessException {
		if (incentiveProgram == null) {
			throw new IllegalArgumentException("incentiveProgram");
		}
		if (incentiveProgram.getIncentiveProgramCode() != null)
			incentiveProgram.setIncentiveProgramCode(incentiveProgram.getIncentiveProgramCode().toUpperCase());
		repo.create(incentiveProgram);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, incentiveProgram, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(IncentiveProgram.class, incentiveProgram.getId());
	}

	@Override
	public void deleteIncentiveProgram(IncentiveProgram incentiveProgram, LogInfoVO logInfo) throws DataAccessException {
		if (incentiveProgram == null) {
			throw new IllegalArgumentException("incentiveProgram");
		}
		repo.delete(incentiveProgram);
		try {
			actionGeneralLogDAO.createActionGeneralLog(incentiveProgram, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public IncentiveProgram getIncentiveProgramById(long id) throws DataAccessException {
		return repo.getEntityById(IncentiveProgram.class, id);
	}
	
	@Override
	public void updateIncentiveProgram(IncentiveProgram incentiveProgram,
			LogInfoVO logInfo) throws DataAccessException {
		if (incentiveProgram == null) {
			throw new IllegalArgumentException("incentiveProgram");
		}

		if (incentiveProgram.getIncentiveProgramCode() != null) {
			incentiveProgram.setIncentiveProgramCode(incentiveProgram
					.getIncentiveProgramCode().toUpperCase());
		}

		IncentiveProgram temp = this.getIncentiveProgramById(incentiveProgram
				.getId());

		temp = temp.clone();
		incentiveProgram.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			incentiveProgram.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(incentiveProgram);

		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, incentiveProgram,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public ObjectVO<IncentiveProgramVO> getListIncentiveProgramVO(IncentiveProgramFilter filter,
			KPaging<IncentiveProgramVO> kPaging)
			throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate())){
			ObjectVO<IncentiveProgramVO> res = new ObjectVO<IncentiveProgramVO>();
			res.setkPaging(null);
			res.setLstObject(new ArrayList<IncentiveProgramVO>());
			return res;
		}
		
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		selectSql.append("SELECT pr.incentive_program_id id, ");
		selectSql.append("  pr.incentive_program_code programCode, ");
		selectSql.append("  pr.incentive_program_name programName, ");
		selectSql.append("  pr.object_type incentiveType, ");
		selectSql.append("  pr.type type, ");
		selectSql.append("  pr.from_date fromDate, ");
		selectSql.append("  pr.to_date toDate, ");
		selectSql.append("  pr.status status ");
		fromSql.append("FROM incentive_program pr ");
		fromSql.append("WHERE 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(filter.getProgramCode())) {
			fromSql.append(" and upper(pr.INCENTIVE_PROGRAM_CODE) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProgramCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getProgramName())) {
			fromSql.append(" and lower(pr.INCENTIVE_PROGRAM_NAME) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getProgramName().toLowerCase()));
		}
		if(filter.getIncentiveType() != null){
			fromSql.append(" and pr.object_type=?");
			params.add(filter.getIncentiveType().getValue());
		}
		if(filter.getShopId() != null){
			fromSql.append(" and exists(select 1 from incentive_shop_map sm where sm.incentive_program_id = pr.incentive_program_id and sm.SHOP_ID = ? and sm.STATUS = 1)");
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			fromSql.append(" and exists(select 1 from incentive_shop_map sm, shop s where sm.incentive_program_id = pr.incentive_program_id and sm.SHOP_ID = s.shop_id and sm.STATUS = ?");
			params.add(ActiveType.RUNNING.getValue());
			fromSql.append(" and upper(s.shop_code) like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().toUpperCase()));
		}
		if(filter.getApplyType() != null){
			fromSql.append(" and pr.type=?");
			params.add(filter.getApplyType().getValue());
		}
		if (filter.getStatus() != null) {
			fromSql.append(" and pr.status=?");
			params.add(filter.getStatus().getValue());
		}else {
			fromSql.append(" and pr.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		if (filter.getFromDate() != null) {
			fromSql.append(" and ((to_date is not null and to_date >= trunc(?)) or to_date is null)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			fromSql.append(" and ((from_date is not null and from_date < trunc(?) + 1) or from_date is null)");
			params.add(filter.getToDate());
		}
		selectSql.append(fromSql.toString());
		selectSql.append(" order by pr.from_date desc, pr.to_date desc, pr.incentive_program_code");
		
		countSql.append("select count(pr.incentive_program_id) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"id", "programCode", "programName", "incentiveType",
				"type", 
				"fromDate","toDate", "status"
				};

		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, 
				StandardBasicTypes.DATE, StandardBasicTypes.DATE, StandardBasicTypes.INTEGER
		};	
		ObjectVO<IncentiveProgramVO> res = new ObjectVO<IncentiveProgramVO>();
		if (kPaging == null){
			List<IncentiveProgramVO> lst =  repo.getListByQueryAndScalar(
					IncentiveProgramVO.class, fieldNames,
					fieldTypes, selectSql.toString(), params);
			res.setLstObject(lst);
		}else{
			List<IncentiveProgramVO> lst = repo.getListByQueryAndScalarPaginated(IncentiveProgramVO.class, fieldNames, fieldTypes,
					selectSql.toString(), countSql.toString(), params, params, kPaging);
			res.setkPaging(kPaging);
			res.setLstObject(lst);
		}
		/*System.out.println(TestUtil.addParamToQuery(selectSql.toString(), params));*/
		return res;
	
	}

	@Override
	public IncentiveProgram getIncentiveProgramByCode(String code)
			throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from incentive_program where incentive_program_code=? and status!=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		return repo.getEntityBySQL(IncentiveProgram.class, sql, params);
	}

	@Override
	public Boolean isDuplicateProductByIncentiveProgram(IncentiveProgram incentiveProgram) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("SELECT count(1) as count ");
		sql.append("FROM incentive_program_detail dt ");
		sql.append("JOIN incentive_program pr ");
		sql.append("ON pr.incentive_program_id = dt.incentive_program_Id ");
		sql.append("WHERE 1                    = 1 ");
		sql.append(" AND pr.incentive_program_id != ? ");
		params.add(incentiveProgram.getId());
			sql.append(" AND dt.product_id in (select ipdt.product_id from incentive_program ip join incentive_program_detail ipdt on ip.incentive_program_id = ipdt.incentive_program_id where ip.incentive_program_id =?) ");
			params.add(incentiveProgram.getId());
		if (incentiveProgram.getFromDate() != null) {
			sql.append(" and ((pr.to_date is not null and pr.to_date >= trunc(?)) or pr.to_date is null)");
			params.add(incentiveProgram.getFromDate());
		}
		if (incentiveProgram.getToDate() != null) {
			sql.append(" and ((pr.from_date is not null and pr.from_date < trunc(?) + 1) or pr.from_date is null)");
			params.add(incentiveProgram.getToDate());
		}
		sql.append(" and dt.status = ? and pr.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}
}

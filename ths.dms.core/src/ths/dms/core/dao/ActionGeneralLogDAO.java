package ths.dms.core.dao;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.ActionAudit;
import ths.dms.core.entities.enumtype.ActionAuditType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface ActionGeneralLogDAO {

	ActionAudit createActionGeneralLog(ActionAudit actionGeneralLog) throws DataAccessException;

	void deleteActionGeneralLog(ActionAudit actionGeneralLog) throws DataAccessException;

	void updateActionGeneralLog(ActionAudit actionGeneralLog) throws DataAccessException;
	
	ActionAudit getActionGeneralLogById(long id) throws DataAccessException;

	void createActionGeneralLog(Object oldEntity, Object newEntity,
			LogInfoVO logInfo) throws DataAccessException, SecurityException,
			NoSuchMethodException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException;

	/**
	 * @author hungnm
	 * @param kPaging
	 * @param staffCode: nhan vien thuc hien thao tac
	 * @param customerId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<ActionAudit> getListActionGeneralLog(
			KPaging<ActionAudit> kPaging, String staffCode,
			Long customerId, Long staffId, Date fromDate, Date toDate)
			throws DataAccessException;
	
	
	List<ActionAudit> getListActionAudit(
			KPaging<ActionAudit> kPaging, String staffCode, Long objectId, ActionAuditType type, Date fromDate, Date toDate)
					throws DataAccessException;
}

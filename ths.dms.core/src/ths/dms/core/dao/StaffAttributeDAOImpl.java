package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ProductAttributeDetail;
import ths.dms.core.entities.StaffAttributeDetail;
import ths.dms.core.entities.StaffAttribute;
import ths.dms.core.entities.StaffAttributeEnum;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumFilter;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.StaffAttributeDetailVO;
import ths.dms.core.entities.filter.ProductAttributeFilter;
import ths.dms.core.entities.filter.StaffAttributeFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProductAttributeDetailVO;
import ths.dms.core.entities.vo.ProductAttributeEnumVO;
import ths.dms.core.entities.vo.ProductAttributeVO;
import ths.dms.core.entities.vo.StaffAttributeEnumVO;
import ths.dms.core.entities.vo.StaffAttributeVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;
/**
 * @author liemtpt
 * @since 21/01/2015
 */
public class StaffAttributeDAOImpl implements StaffAttributeDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public StaffAttribute createStaffAttribute(StaffAttribute staffAttribute,LogInfoVO logInfo)
			throws DataAccessException {
		if (staffAttribute == null) {
			throw new IllegalArgumentException("staffAttribute is null");
		}
		staffAttribute.setCreateDate(commonDAO.getSysDate());
		if (logInfo != null){
			staffAttribute.setCreateUser(logInfo.getStaffCode());
		}
		repo.create(staffAttribute);
		return repo.getEntityById(StaffAttribute.class, staffAttribute.getId());
	}

	@Override
	public void deleteStaffAttribute(StaffAttribute staffAttribute)
			throws DataAccessException {
		if (staffAttribute == null) {
			throw new IllegalArgumentException("staffAttribute is null");
		}
		repo.delete(staffAttribute);
	}

	@Override
	public void deleteStaffAttributeEnum(StaffAttributeEnum staffAttribute)
			throws DataAccessException {
		if (staffAttribute == null) {
			throw new IllegalArgumentException("staffAttributeEnum is null");
		}
		repo.delete(staffAttribute);
	}
	
	@Override
	public StaffAttribute getStaffAttributeById(long id) throws DataAccessException {
		return repo.getEntityById(StaffAttribute.class, id);
	}
	
	@Override
	public StaffAttributeEnum getStaffAttributeEnumById(long id) throws DataAccessException {
		return repo.getEntityById(StaffAttributeEnum.class, id);
	}
	
	@Override
	public StaffAttribute getStaffAttributeByCode(String code)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from staff_attribute ");
		sql.append(" where 1 = 1 and (status = 1 or status = 0)");
		sql.append(" and code = ? ");
		params.add(code.toUpperCase());
		return repo.getEntityBySQL(StaffAttribute.class, sql.toString(), params);
	}

	@Override
	public StaffAttributeEnum getStaffAttributeEnumByCode(String code)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from staff_attribute_enum ");
		sql.append(" where 1 = 1 and (status = 1 or status = 0) ");
		sql.append(" and code = ? ");
		params.add(code.toUpperCase());
		return repo.getEntityBySQL(StaffAttributeEnum.class, sql.toString(), params);
	}
	
	@Override
	public StaffAttributeEnum createStaffAttributeEnum(StaffAttributeEnum staffAttributeEnum,LogInfoVO logInfo)
			throws DataAccessException {
		if (staffAttributeEnum == null) {
			throw new IllegalArgumentException("staffAttributeEnum is null");
		}
		staffAttributeEnum.setCreateDate(commonDAO.getSysDate());
		if (logInfo != null){
			staffAttributeEnum.setCreateUser(logInfo.getStaffCode());
		}
		repo.create(staffAttributeEnum);
		return repo.getEntityById(StaffAttributeEnum.class, staffAttributeEnum.getId());
	}	
	
	@Override
	public void updateStaffAttributeEnum(StaffAttributeEnum staffAttributeEnum, LogInfoVO logInfo) throws DataAccessException {
		if (staffAttributeEnum == null) {
			throw new IllegalArgumentException("staffAttributeEnum is null");
		}
		staffAttributeEnum.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			staffAttributeEnum.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(staffAttributeEnum);
	}
	
	@Override
	public void updateStaffAttribute(StaffAttribute staffAttribute, LogInfoVO logInfo) throws DataAccessException {
		if (staffAttribute == null) {
			throw new IllegalArgumentException("staffAttribute is null");
		}
		staffAttribute.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			staffAttribute.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(staffAttribute);
	}
	
	@Override
	public void updateStaffAttributeDetail(StaffAttributeDetail staffAttributeDetail, LogInfoVO logInfo) throws DataAccessException {
		if (staffAttributeDetail == null) {
			throw new IllegalArgumentException("staffAttributeDetail is null");
		}
		staffAttributeDetail.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			staffAttributeDetail.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(staffAttributeDetail);
	}
	
	@Override
	public List<AttributeDynamicVO> getListStaffAttributeVO(AttributeDynamicFilter filter, KPaging<AttributeDynamicVO> kPaging) throws DataAccessException {
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		selectSql.append("select sa.staff_attribute_id attributeId ");
		selectSql.append(" ,sa.code attributeCode ");
		selectSql.append(" ,sa.name attributeName ");
		selectSql.append(" ,sa.type type ");
		selectSql.append(" ,sa.mandatory mandatory ");
		selectSql.append(" ,sa.display_order displayOrder ");
		selectSql.append(" ,sa.status status ");
		selectSql.append(" ,sa.description description ");
		selectSql.append(" ,sa.data_length dataLength ");
		selectSql.append(" , sa.min_value minValue ");
		selectSql.append(" , sa.max_value maxValue ");
		fromSql.append(" from staff_attribute sa  ");
		fromSql.append("WHERE 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(filter.getAttributeCode())) {
			fromSql.append(" and upper(sa.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getAttributeCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getAttributeName())) {
			fromSql.append(" and upper(sa.name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getAttributeName().toUpperCase()));
		}
		if (filter.getType()!=null) {
			fromSql.append(" and sa.type = ? ");
			params.add(filter.getType());
		}
		if(filter.getStatus() != null){
			fromSql.append(" and sa.status =?");
			params.add(filter.getStatus());
		}
		selectSql.append(fromSql.toString());

		countSql.append("select count(sa.staff_attribute_id) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"attributeId", "attributeCode", "attributeName" 
				,"type","mandatory","displayOrder", "status","description"
				,"dataLength","minValue","maxValue"
		};

		String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			selectSql.append(" order by ").append(sort).append(" ");
			String order = StringUtility.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				selectSql.append(order);
			}
		} else {
			selectSql.append(" order by sa.code, sa.name");
		}
		
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING
				,StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER
				,StandardBasicTypes.STRING,StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER
		};	
		List<AttributeDynamicVO> lst =  null;
		if (kPaging == null){
			lst =  repo.getListByQueryAndScalar(AttributeDynamicVO.class, fieldNames,fieldTypes, selectSql.toString(), params);
		}else{
			lst = repo.getListByQueryAndScalarPaginated(AttributeDynamicVO.class, fieldNames, fieldTypes,
					selectSql.toString(), countSql.toString(), params, params, kPaging);
		}
		return lst;
	}
	
	@Override
	public List<AttributeEnumVO> getListStaffAttributeEnumVO(AttributeEnumFilter filter,
			KPaging<AttributeEnumVO> kPaging) throws DataAccessException {
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		selectSql.append("select sa.staff_attribute_id attributeId ");
		selectSql.append(" ,sae.staff_attribute_enum_id enumId ");
		selectSql.append(" ,sae.code enumCode ");
		selectSql.append(" ,sae.value enumValue ");
		selectSql.append(" ,sae.status status");
		fromSql.append(" from staff_attribute sa ");
		fromSql.append(" join staff_attribute_enum sae on sa.staff_attribute_id = sae.staff_attribute_id ");
		fromSql.append(" WHERE 1 = 1 and sae.status = 1 ");
		List<Object> params = new ArrayList<Object>();
		if (filter.getAttributeId() != null) {
			fromSql.append(" and sa.staff_attribute_id = ? ");
			params.add(filter.getAttributeId());
		}
		
		if (filter.getStatus() != null) {
			fromSql.append(" and sa.status = ? ");
			params.add(filter.getStatus().getValue());
		}
		
		selectSql.append(fromSql.toString());
		
		countSql.append("select count(sae.staff_attribute_enum_id) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"attributeId", "enumId", "enumCode", "enumValue", "status"
				};

		String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			selectSql.append(" order by ").append(sort).append(" ");
			String order = StringUtility.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				selectSql.append(order);
			}
		} else {
			selectSql.append(" order by sae.code, sae.value");
		}
		
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER
		};	
		List<AttributeEnumVO> lst =  null;
		if (kPaging == null){
			lst =  repo.getListByQueryAndScalar(AttributeEnumVO.class, fieldNames,fieldTypes, selectSql.toString(), params);
		}else{
			lst = repo.getListByQueryAndScalarPaginated(AttributeEnumVO.class, fieldNames, fieldTypes,
					selectSql.toString(), countSql.toString(), params, params, kPaging);
		}
		return lst;
	}
	@Override
	public List<StaffAttributeDetail> getListStaffAttributeDetailByEnumId(Long id) throws DataAccessException {

		if (id == null)
			return new ArrayList<StaffAttributeDetail>();
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from staff_attribute_detail where 1 = 1");
		sql.append(" and status = 1 and staff_attribute_enum_id = ? ");
		params.add(id);
		return repo.getListBySQL(StaffAttributeDetail.class, sql.toString(), params);
	}
	/****VUONGMQ; 26/02/2014; danh sach thuoc tinh dong */
	@Override
	public List<StaffAttributeVO> getListStaffAttributeVOFilter(StaffAttributeFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select staff_attribute_id id,code,name,type,mandatory,data_length dataLength,min_value minValue,max_value maxValue ");		
		sql.append(" from staff_attribute sta ");
		sql.append(" where 1 = 1 and sta.status = 1 ");
		
		sql.append(" order by display_order ");
		
		final String[] fieldNames = new String[] { "id", "code", 
				"name" , 
				"type",
				"mandatory",
				"dataLength",
				"minValue",
				"maxValue"
		};
		
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER
		};
		
		return repo.getListByQueryAndScalar(StaffAttributeVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<StaffAttributeEnumVO> getListStaffAttributeEnumVOFilter(StaffAttributeFilter filter) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select staff_attribute_enum_id id,staff_attribute_id staffAttId,code,value ");
		
		sql.append(" from staff_attribute_enum stae ");		
		sql.append(" where 1 = 1 and stae.status = 1 ");
		
		if(filter.getStaffAttId() != null){
			sql.append(" and stae.staff_attribute_id = ? ");
			params.add(filter.getStaffAttId());
		}
		
		final String[] fieldNames = new String[] { "id", "staffAttId","code", "value"};

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING};
		
		return repo.getListByQueryAndScalar(StaffAttributeEnumVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<StaffAttributeDetailVO> getListStaffAttributeDetailVOFilter(StaffAttributeFilter filter) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select staff_attribute_detail_id id,staff_attribute_id staffAttId,staff_id staffId,value,staff_attribute_enum_id staffAttEnumId");
		
		sql.append(" from staff_attribute_detail stad ");		
		sql.append(" where 1 = 1 and stad.status = 1 ");
		
		if(filter.getStaffAttId() != null){
			sql.append(" and stad.staff_attribute_id = ? ");
			params.add(filter.getStaffAttId());
		}
		
		if(filter.getStaffId() != null){
			sql.append(" and stad.staff_id = ? ");
			params.add(filter.getStaffId());
		}
		
		final String[] fieldNames = new String[] { "id", "staffAttId","staffId", 
				"value",
				"staffAttEnumId"
		};

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,StandardBasicTypes.LONG, StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.LONG
		};
		
		return repo.getListByQueryAndScalar(StaffAttributeDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/****VUONGMQ; 27/02/2014; lay de cap nhat thong tin thuoc tinh dong */
	@Override
	public StaffAttributeDetail getStaffAttributeDetailByFilter(Long staffAtt, Long staffId, Long staffAttEnum)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from staff_attribute_detail ");
		sql.append(" where status = 1 ");
		if(staffAtt != null){
			sql.append(" and staff_attribute_id = ? ");
			params.add(staffAtt);
		}
		
		if(staffId != null){
			sql.append(" and staff_id = ? ");
			params.add(staffId);
		}
		if(staffAttEnum != null){
			//sql.append(" and staff_attribute_enum_id = 1 ");
			sql.append(" and staff_attribute_enum_id = ? ");
			params.add(staffAttEnum);
		}
		return repo.getEntityBySQL(StaffAttributeDetail.class, sql.toString(), params);
	}

	@Override
	public List<StaffAttributeDetail> getStaffAttributeDetailByFilter(Long staffAtt, Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from staff_attribute_detail ");
		sql.append(" where status = 1 ");
		if(staffAtt != null){
			sql.append(" and staff_attribute_id = ? ");
			params.add(staffAtt);
		}
		
		if(staffId != null){
			sql.append(" and staff_id = ? ");
			params.add(staffId);
		}		
		return repo.getListBySQL(StaffAttributeDetail.class, sql.toString(), params);
	}
}

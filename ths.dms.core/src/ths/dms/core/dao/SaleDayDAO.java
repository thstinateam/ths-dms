package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.SaleDays;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface SaleDayDAO {

	SaleDays createSaleDay(SaleDays saleDay, LogInfoVO logInfo)
			throws DataAccessException;

	void deleteSaleDay(SaleDays saleDay, LogInfoVO logInfo)
			throws DataAccessException;

	void updateSaleDay(SaleDays saleDay, LogInfoVO logInfo)
			throws DataAccessException;

	SaleDays getSaleDayById(long id) throws DataAccessException;

	List<SaleDays> getListSaleDay(KPaging<SaleDays> kPaging, Integer fromYear,
			Integer toYear, ActiveType status) throws DataAccessException;

	Boolean isUsingByOthers(long saleDayId) throws DataAccessException;

	SaleDays getSaleDayByYear(Integer year) throws DataAccessException;

	Integer getSaleDayByYear(int year, int month) throws DataAccessException;

	/**
	 * Get SALE_DAY for update
	 * 
	 * @param year
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	SaleDays getSaleDayForUpdate(int year) throws DataAccessException;

	/**
	 * 
	 * @param month
	 * @param year
	 * @param logInfo
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	void addSaleDay(int month, int year, int numDay, LogInfoVO logInfo)
			throws DataAccessException;

	/**
	 * getSaleDayByDate
	 * 
	 * @author hieunq1
	 * @param date
	 * @return
	 * @throws DataAccessException
	 */
	Integer getSaleDayByDate(Date date) throws DataAccessException;
	
	/**
	 * 
	*  lay ngay ban hang tu dau nam den ngay hien tai
	*  @author: thanhnn
	*  @param date
	*  @return
	*  @throws DataAccessException
	*  @return: Integer
	*  @throws:
	 */
	Integer getSaleDayFromStartYearToDate(Date date) throws DataAccessException;
	
	/**
	 * 
	*  Lay tong so ngay ban hang cua nam ung voi ngay truyen vao
	*  @author: thanhnn
	*  @param date
	*  @return
	*  @throws DataAccessException
	*  @return: Integer
	*  @throws:
	 */
	Integer getSumSaleDateOfYear(Date date) throws DataAccessException;

	Integer getSalesMonthByYear() throws DataAccessException;

	/***
	 * @author vuongmq
	 * @date 31/01/2015
	 * lay saleday theo nam va shop Id, 
	 * chua khai bao Mgr
	 */
	SaleDays getSaleDayByYearAndShopId(Long shopId, int year) throws DataAccessException;
}

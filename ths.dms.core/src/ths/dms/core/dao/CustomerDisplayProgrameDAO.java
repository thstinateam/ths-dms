package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.CustomerDisplayPrograme;
import ths.dms.core.entities.StDisplayProgramLevel;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.CustomerDisplayProgrameVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface CustomerDisplayProgrameDAO {

	CustomerDisplayPrograme createCustomerDisplayPrograme(CustomerDisplayPrograme customerDisplayPrograme) throws DataAccessException;

	void deleteCustomerDisplayPrograme(CustomerDisplayPrograme customerDisplayPrograme) throws DataAccessException;

	void updateCustomerDisplayPrograme(CustomerDisplayPrograme customerDisplayPrograme) throws DataAccessException;
	
	CustomerDisplayPrograme getCustomerDisplayProgrameById(long id) throws DataAccessException;
	
	CustomerDisplayPrograme getCustomerDisplayPrograme(CustomerDisplayPrograme cd,Boolean checkAll) throws DataAccessException;
	
	List<CustomerDisplayProgrameVO> getListCustomerDisplayPrograme(
			KPaging<CustomerDisplayProgrameVO> kPaging, Long shopId,
			Long id, String month)
			throws DataAccessException; 
	void deleteListCustomerDisplayPrograme(List<Long> lstCustomerDisplayPrograme)throws DataAccessException;
	
	int countShopConnectBy(Long shopId)throws DataAccessException;

	List<Customer> getListCustomerInLevel(KPaging<Customer> kPaging,
			String displayProgramCode, String levelCode)
			throws DataAccessException;
	
	StDisplayProgramLevel checkLevelCodeExists(Long id,String levelCode)throws DataAccessException;
	
	CustomerDisplayPrograme checkExistDisplayCustomer(Long id,Long customerId,Long staffId,String levelCode,String month)throws DataAccessException ;
	
	List<CustomerDisplayProgrameVO> getListCustomerDisplayProgrameEX(
			KPaging<CustomerDisplayProgrameVO> kPaging, List<Long> shopId,
			Long id, String month, boolean checkMap, long parentStaffId)
			throws DataAccessException;
}

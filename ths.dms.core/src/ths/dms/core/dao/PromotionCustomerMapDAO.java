package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.PromotionCustomerMap;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PromotionShopMapFilter;
import ths.dms.core.entities.filter.PromotionCustomerFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PromotionCustomerVO;
import ths.dms.core.entities.vo.PromotionShopMapVO;
import ths.dms.core.exceptions.DataAccessException;

public interface PromotionCustomerMapDAO {

	PromotionCustomerMap createPromotionCustomerMap(PromotionCustomerMap promotionCustomerMap, LogInfoVO logInfo) throws DataAccessException;

	void deletePromotionCustomerMap(PromotionCustomerMap promotionCustomerMap, LogInfoVO logInfo) throws DataAccessException;

	void updatePromotionCustomerMap(PromotionCustomerMap promotionCustomerMap, LogInfoVO logInfo) throws DataAccessException;
	
	PromotionCustomerMap getPromotionCustomerMapById(long id) throws DataAccessException;

	void createPromotionCustomerMap(
			List<PromotionCustomerMap> lstPromotionCustomerMap, LogInfoVO logInfo)
			throws DataAccessException;

	Boolean checkIfRecordExist(long promotionProgramId, String customerCode,
			Long id) throws DataAccessException;

	Boolean checkPromotionShopMapInCustomerMap(Long promotionShopMapId)
			throws DataAccessException;

	void deleteRelevantCustomerMap(Long promotionShopMapId)
			throws DataAccessException;

	List<PromotionCustomerMap> getListPromotionCustomerMap(
			KPaging<PromotionCustomerMap> kPaging, Long promotionProgramId,
			String shopCode, Long channelTypeId, String shortCode,
			Integer quantityMax, ActiveType status, String channelTypeName,
			Integer quantityReceived, String customerName,
			String channelTypeCode) throws DataAccessException;

	Boolean checkIfRecordExistEx(long promotionProgramId,
			String channelTypeCode, String shopCode, Long id)
			throws DataAccessException;

	PromotionCustomerMap getPromotionCustomerMap(Long promotionShopMapId,
			Long customerId) throws DataAccessException;

	PromotionCustomerMap checkPromotionCustomerMap(Long promotionProgramId,
			String shopCode, String shortCode, String channelTypeCode)
			throws DataAccessException;

	/**
	*  Mo ta chuc nang cua ham
	*  @author: thanhnn
	*  @param listPromotionProgramId
	*  @return
	*  @return: List<PromotionCustomerMap>
	*  @throws:
	*/
	List<PromotionCustomerMap> getListPromotionCustomerMapWithListPromotionId(
			List<Long> listPromotionProgramId) throws DataAccessException;

	List<PromotionCustomerMap> getListPromotionCustomerMapByShop(
			Long promotionShopMapId) throws DataAccessException;

	List<PromotionCustomerMap> getListPromotionCustomerMapByShopAndPP(
			KPaging<PromotionCustomerMap> paging, Long shopId,
			Long promotionProgramId, String customerCode, String customerName, String address) throws DataAccessException;

	/**
	*  Lay PromotionCustomerMap cho KH
	*/
	List<PromotionCustomerMap> getListPromotionCustomerMapForUpdate(Long promotionProgramId, Long customerId, ActiveType status)throws DataAccessException;
	/**
	*  Lay PromotionCustomerMap cho don vi
	*/
	List<PromotionCustomerMap> getListPromotionCustomerMapWithShopAndPromotionProgram(Long shopId, Long promotionProgramId, Boolean exceptShopCheck)throws DataAccessException;
	
	Boolean checkExistCustomerMap(Long id)throws DataAccessException;
	
	Integer getSumQuantityMaxOfShopMap(long shopMapId, List<Long> lstexceptCusMapId) throws DataAccessException;

	/**
	 * 
	 * @param filter
	 * @return Lay danh sach khach hang cho CTKM
	 * @throws DataAccessException
	 * @author trietptm
	 * @since Aug 03, 2015
	 */
	List<PromotionCustomerVO> getCustomerInPromotionProgram(PromotionCustomerFilter filter) throws DataAccessException;
	
	/**
	 * Kiem tra xem KH co ton tai trong CTKM chua
	 * 
	 * @author lacnv1
	 * @since Aug 19, 2014
	 */
	List<PromotionCustomerVO> getListCustomerInPromotion(long promotionId, List<Long> lstCustId) throws DataAccessException;

	List<PromotionShopMapVO> getListPromotionShopMapVO(
			PromotionShopMapFilter filter) throws DataAccessException;
	/**
	 * 
	 * @param filter
	 * @return Lay danh sach khach hang popup them KH vao CTKM
	 * @throws DataAccessException
	 * @author trietptm
	 * @since @since Aug 03, 2015
	 */
	List<PromotionCustomerVO> searchCustomerForPromotionProgram(PromotionCustomerFilter filter) throws DataAccessException;
	
	List<PromotionCustomerMap> getListPromotionCustomerMapEntity(Long listCustomerID, PromotionShopMap listPromotionShopMap) throws DataAccessException;
}
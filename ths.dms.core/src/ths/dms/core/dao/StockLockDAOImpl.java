package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StockLock;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class StockLockDAOImpl implements StockLockDAO{
	
	@Autowired
	private IRepository repo;
	
	@Override
	public StockLock getStockLock(Long shopId, Long staffId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from stock_lock where 1=1");
		if(shopId != null){
			sql.append(" and shop_id = ?");
			params.add(shopId);
		}
		if(staffId != null){
			sql.append(" and staff_id = ?");
			params.add(staffId);
		}
		return repo.getEntityBySQL(StockLock.class, sql.toString(), params);
	}
	
	@Override
	public List<StockLock> getListStockLockByListStaffId(List<Long> lstStaffId, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from stock_lock where 1=1");
		if(shopId != null){
			sql.append(" and shop_id = ?");
			params.add(shopId);
		}
		if(lstStaffId != null && lstStaffId.size()>0){
			String paramFix = StringUtility.getParamsIdFixBugTooLongParams(lstStaffId, "staff_id");
			sql.append(paramFix);
		}
		return repo.getListBySQL(StockLock.class, sql.toString(), params);
	}
	
	@Override
	public StockLock getStockLockByStaffCode(String staffCode, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select * from stock_lock ");
		sql.append(" where shop_id = ? ");
		params.add(shopId);
		sql.append(" and staff_id in (select staff_id from staff where staff_code = ? and shop_id = ?) ");
		params.add(staffCode);
		params.add(shopId);
		return repo.getEntityBySQL(StockLock.class, sql.toString(), params);
	}

	@Override
	public StockLock getStockLockByFilter(StockLock filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from stock_lock where 1=1");
		if(filter.getShop() != null){
			sql.append(" and shop_id = ?");
			params.add(filter.getShop());
		}
		if(filter.getStaff() != null){
			sql.append(" and staff_id = ?");
			params.add(filter.getStaff());
		}
		if (filter.getCreateDate() != null) {
			sql.append(" and create_date >=trunc(?) ");
			params.add(filter.getCreateDate());
			sql.append(" and create_date < trunc(?) +1 ");
			params.add(filter.getCreateDate());
		}
		/*if (filter.getCreateDate() != null) {
			sql.append(" and create_date >= trunc(?) ");
			params.add(filter.getCreateDate());
		}
		if (filter.getCreateDate() != null) {
			sql.append(" and create_date < trunc(?) + 1 ");
			params.add(filter.getCreateDate());
		}*/
		return repo.getEntityBySQL(StockLock.class, sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.KpiImplement;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.CommonFilter;

import ths.dms.core.dao.repo.IRepository;

public class KpiImplementDAOImpl implements KpiImplementDAO{

	@Autowired
	private IRepository repo;
	
	@Override
	public List<KpiImplement> getListKpiImplByFilter(
			KPaging<KpiImplement> kPaging, CommonFilter filter)
			throws DataAccessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public KpiImplement getKpiImplement(Long staffId, Long kpiId, Long cycleId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from kpi_implement ");
		sql.append(" where staff_id = ? ");
		sql.append(" and kpi_id = ? ");
		sql.append(" and cycle_id = ? ");
		params.add(staffId);
		params.add(kpiId);
		params.add(cycleId);
		return repo.getFirstBySQL(KpiImplement.class, sql.toString(), params);
	}

}

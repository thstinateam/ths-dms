package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.VatType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface VatTypeDAO {

	VatType createVatType(VatType vatType, LogInfoVO logInfo) throws DataAccessException;

	void deleteVatType(VatType vatType, LogInfoVO logInfo) throws DataAccessException;

	void updateVatType(VatType vatType, LogInfoVO logInfo) throws DataAccessException;
	
	VatType getVatTypeById(long id) throws DataAccessException;

	VatType getVatTypeByCode(String code) throws DataAccessException;

	List<VatType> getListVatType(KPaging<VatType> kPaging, String vatTypeCode,
			String vatTypeName, String desc, ActiveType status, Float value)
			throws DataAccessException;

	Boolean isUsingByOthers(long vatTypeId) throws DataAccessException;
	
}

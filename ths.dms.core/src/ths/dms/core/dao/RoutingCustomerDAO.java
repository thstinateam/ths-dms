package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.RoutingCustomer;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.vo.CustomerPositionVO;
import ths.dms.core.entities.vo.RoutingCustomerVO;
import ths.dms.core.exceptions.DataAccessException;

public interface RoutingCustomerDAO {

	RoutingCustomer createRoutingCustomer(RoutingCustomer routingCustomer) throws DataAccessException;

	void deleteRoutingCustomer(RoutingCustomer routingCustomer) throws DataAccessException;

	void updateRoutingCustomer(RoutingCustomer routingCustomer) throws DataAccessException;
	
	
	void updateRoutingCustomer(List<RoutingCustomer> lstRoutingCustomer) throws DataAccessException;
	
	RoutingCustomer getRoutingCustomerById(Long id) throws DataAccessException;
	
	void createRoutingCustomer(List<RoutingCustomer> listRoutingCustomer) throws DataAccessException;
	
	List<RoutingCustomer> getListRoutingCustomer(
			KPaging<RoutingCustomer> kPaging, Long routingId,
			ActiveType status, Integer saleDate, Boolean hasOrderBySeq,
			Long shopId, Boolean hasChildShop, Long superId) throws DataAccessException;
	
	Boolean checkExit(Long routingId, String listCustomerId) throws DataAccessException;

	RoutingCustomer getRoutingCustomerByCustomerInCurrent(Long customerId)
			throws DataAccessException;

	RoutingCustomer getRoutingCustomerByDate(Long customerId, Long shopId,
			Date orderDate) throws DataAccessException;

	RoutingCustomer getRoutingCustomerByDateAndStaff(Long customerId, Long shopId,
			Date orderDate, Long staffId) throws DataAccessException;
	
	
	/**
	*  Mo ta chuc nang cua ham
	*  @author: thanhnn
	*  @param staffId
	*  @return
	*  @return: List<Customer>
	*  @throws:
	*/
	List<CustomerPositionVO> getListCustomerForStaff(Long staffId) throws DataAccessException;

	/**
	 * Cap nhat thu tu ghe tham cua khach khang khi khach hang off
	 * 
	 * @author thuattq
	 */
	void updateSeqOfDisableCustomer(Long customerId) throws DataAccessException;
	
	/**
	 * 
	*  off routing customer khi off routing
	*  @author: thanhnn
	*  @param routingId
	*  @throws DataAccessException
	*  @return: void
	*  @throws:
	 */
	void offRoutingCustomerWithRoutingId(Long routingId, String updateUser, Date updateDate) throws DataAccessException;

	RoutingCustomer getRoutingCustomerByRoutingIdAndCustomerId(Long routingId, Long customerId) throws DataAccessException;

	List<RoutingCustomer> getRoutingCustomerByFilterAndListID(List<Long> lstCustomerID, List<Long> lstRoutingID, RoutingCustomerFilter filter) throws DataAccessException;

	List<RoutingCustomerVO> getListRouCusWitFDateMinByListCustomerID(List<Long> lstCusId, Long routeId) throws DataAccessException;

	/**
	 * Lay danh sach Routing Customer hien tai va tuong lai
	 * 
	 * @author hunglm16
	 * @since October 6,2014
	 * @description Bo sung phan quyen CMS
	 * */
	List<RoutingCustomerVO> getListRusCusByEndateNextSysDate() throws DataAccessException;

	List<RoutingCustomerVO> getListRouCusWitFDateMinByFlag(List<Long> lstCusId, Long routeId, Long shopId, Boolean flag) throws DataAccessException;

	List<RoutingCustomer> getListRoutingCustomerListRouCusID(List<Long> lstID) throws DataAccessException;

	List<RoutingCustomer> getListRouCusErrByStartDate(Long customerId, Long routingId, Date fDate, Long shopId, Long rouCusId) throws DataAccessException;

	List<RoutingCustomerVO> getListRoutingCustomerNew(KPaging<RoutingCustomer> kPaging, Long routingId, ActiveType status, Integer saleDate, Boolean hasOrderBySeq,
			Long shopId, Boolean hasChildShop, Long superId) throws DataAccessException;

	List<RoutingCustomer> getListRouCusByShortCodeAndShopId(String shortCode, Long shopId, Integer saleDate) throws DataAccessException;

	int countSEQBySeqAndRoutingId(Long routingId, Integer seq) throws DataAccessException;

	Boolean countCustomerInRouCusValidImport(String shortCode, String routingCode, Long shopId) throws DataAccessException;

	List<RoutingCustomerVO> getListRoutingCustomerByCusOrRouting(Long routingId, Long shopId, Long customerId, String shortCode,
			String customerName, String address, Date startDate, Date endDate, Integer status) throws DataAccessException;

	List<RoutingCustomer> getListRoutingCustomersBySeq(Long routingId, Integer seq) throws DataAccessException;

	List<RoutingCustomer> getListRouCusByListShortCodeAndShopId(
			List<String> lstShortCode, Long shopId, Integer saleDate,
			Long routingId) throws DataAccessException;

	List<RoutingCustomer> getListRoutingCustomerByImport(RoutingCustomer roucus) throws DataAccessException;

	/**
	 * Lay danh sach cac Routing Customer
	 * 
	 * @author hunglm16
	 * @since OCtober 13,2014
	 * @description Co the bo sung them cac tham so lien quan
	 * */
	List<RoutingCustomer> getRoutingCustomerByFilter(RoutingCustomerFilter filter) throws DataAccessException;

}

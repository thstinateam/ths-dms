package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SalesPlan;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class SalesPlanDAOImpl implements SalesPlanDAO {

	@Autowired
	private IRepository repo;
	
	@Override
	public SalesPlan createSalesPlan(SalesPlan salesPlan) throws DataAccessException {
		repo.create(salesPlan);
		return repo.getEntityById(SalesPlan.class, salesPlan.getId());
	}

	@Override
	public void deleteSalesPlan(SalesPlan salesPlan) throws DataAccessException {
		if (salesPlan == null) {
			throw new IllegalArgumentException("salesPlan");
		}
		repo.delete(salesPlan);
	}

	@Override
	public SalesPlan getSalesPlanById(long id) throws DataAccessException {
		return repo.getEntityById(SalesPlan.class, id);
	}
	
	@Override
	public void updateSalesPlan(SalesPlan salesPlan) throws DataAccessException {
		repo.update(salesPlan);
	}
	

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SalePlanDAO#getListSalePlanForManagerPlan(ths.dms.core.entities.enumtype.KPaging, java.lang.Long, java.lang.Long, java.lang.Long, java.util.Date)
	 */
	@Override
	public List<SalesPlan> getListSalePlanForManagerPlan(
			KPaging<SalesPlan> kPaging, Long shopId, Long staffId,
			Long productId, Date date) throws DataAccessException {
		// TODO Auto-generated method stub
		if (staffId == null) {
			throw new IllegalArgumentException("staffId is null");
		}
		if (date == null) {
			throw new IllegalArgumentException("date is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sp.* from sales_plan sp, staff s, product p");
		sql.append(" where sp.staff_id = ?");
		params.add(staffId);
		sql.append(" and sp.staff_id = s.staff_id and s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and sp.product_id = p.product_id");
		sql.append(" and p.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (shopId != null) {
			sql.append(" and sp.shop_id = ?");
			params.add(shopId);
		}
		if (productId != null) {
			sql.append(" and sp.product_id = ?");
			params.add(productId);
		}
		sql.append(" and trunc(?) + 1 >= trunc(sp.from_date, 'month')");
		sql.append(" and (trunc(?) <= sp.to_date or sp.to_date is null)");
		params.add(date);
		params.add(date);
		sql.append(" and NVL(sp.quantity, 0) > 0");
		sql.append(" order by upper(p.product_code)");
		if (kPaging != null)
			return repo.getListBySQLPaginated(SalesPlan.class, sql.toString(), params, kPaging);
		else
			return repo.getListBySQL(SalesPlan.class, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SalesPlanDAO#getListSalePlanForManagerPlanSearchLike(ths.dms.core.entities.enumtype.KPaging, java.lang.Long, java.lang.Long, java.lang.String, java.util.Date)
	 */
	@Override
	public List<SalesPlan> getListSalePlanForManagerPlanSearchLike(
			KPaging<SalesPlan> kPaging, Long shopId, Long staffId,
			String productCode, Date date) throws DataAccessException {
		// TODO Auto-generated method stub
		if (staffId == null) {
			throw new IllegalArgumentException("staffId is null");
		}
		if (date == null) {
			throw new IllegalArgumentException("date is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sp.* from sales_plan sp, staff s, product p");
		sql.append(" where sp.staff_id = ?");
		params.add(staffId);
		sql.append(" and sp.staff_id = s.staff_id and s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and sp.product_id = p.product_id");
		sql.append(" and p.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (shopId != null) {
			sql.append(" and sp.shop_id = ?");
			params.add(shopId);
		}
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and lower(p.product_code) like ?");
			params.add(StringUtility.toOracleSearchLike(productCode.toLowerCase()));
		}
		sql.append(" and trunc(?) + 1 >= trunc(sp.from_date, 'month')");
		sql.append(" and (trunc(?) <= sp.to_date or sp.to_date is null)");
		params.add(date);
		params.add(date);
		sql.append(" and NVL(sp.quantity, 0) > 0");
		sql.append(" order by upper(p.product_code)");
		if (kPaging != null)
			return repo.getListBySQLPaginated(SalesPlan.class, sql.toString(), params, kPaging);
		else
			return repo.getListBySQL(SalesPlan.class, sql.toString(), params);
	}
	
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.FocusShopMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.exceptions.DataAccessException;

public interface FocusShopMapDAO {

	FocusShopMap createFocusShopMap(FocusShopMap focusShopMap, LogInfoVO logInfo) throws DataAccessException;

	void deleteFocusShopMap(FocusShopMap focusShopMap, LogInfoVO logInfo) throws DataAccessException;

	void updateFocusShopMap(FocusShopMap focusShopMap, LogInfoVO logInfo) throws DataAccessException;
	
	FocusShopMap getFocusShopMapById(long id) throws DataAccessException;

	Boolean isUsingByOthers(long focusShopMapId) throws DataAccessException;

	Shop getShopMapArea(long focusShopMapId) throws DataAccessException;

	Boolean checkIfRecordExist(long focusProgramId, String shopCode, Long id)
			throws DataAccessException;

	Boolean createListFocusShopMap(Long focusProgramId, List<Long> lstShop, ActiveType status,
			LogInfoVO logInfo) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param focusProgramId
	 * @param shopCode
	 * @param id
	 * @return
	 * @throws DataAccessException
	 */
	FocusShopMap getFocusShopMap(long focusProgramId, Long shopId, Long id) throws DataAccessException;

	Boolean checkShopFamilyInFocusProgram(Long shopId, Long focusProgramId) throws DataAccessException;

	Boolean checkShopAncestorInFocusProgram(Long focusProgramId, Long shopId) throws DataAccessException;

	List<FocusShopMap> getListFocusShopMapByFocusProgramId(
			KPaging<FocusShopMap> kPaging, Long focusProgramId,
			String shopCode, String shopName, ActiveType status,
			Long parentShopId) throws DataAccessException;

	Boolean checkIfAncestorInFp(Long shopId, Long focusProgramId)
			throws DataAccessException;

	Boolean checkExitsFocusProgramWithParentShop(Long focusProgramid,
			String shopCode) throws DataAccessException;

	List<Shop> getListShopInFocusProgram(Long focusProgramId, String shopCode,
			String shopName, Long parentShopId, Boolean isJoin) throws DataAccessException;

	List<ProgrameExtentVO> getListProgrameExtentVOEx(List<Shop> lstShop,
			Long focusProgramId) throws DataAccessException;

	List<ProgrameExtentVO> getListProgrameExtentVO(List<Shop> lstShop, Long focusProgramId)
			throws DataAccessException;

	List<FocusShopMap> getListFocusShopMapWithShopAndFocusProgram(Long shopId,
			Long focusProgramId) throws DataAccessException;

	List<FocusShopMap> getListFocusChildShopMapWithShopAndFocusProgram(
			Long shopId, Long focusProgramId) throws DataAccessException;

	Boolean isExistChildJoinProgram(String shopCode, Long focusProgramId)
			throws DataAccessException;
}

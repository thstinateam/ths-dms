package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.EquipStockTotal;
import ths.dms.core.entities.EquipStockTransForm;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.vo.EquipmentStockDeliveryVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class EquipmentStockTotalDAOImpl implements EquipmentStockTotalDAO{
	@Autowired
	private IRepository repo;
	
	@Override
	public EquipStockTotal getEquipStockTotalById(Long stockId, EquipStockTotalType stockType, Long equipGroupId) throws DataAccessException{
		// TODO Auto-generated method stub
		if (stockId == null) {
			throw new IllegalArgumentException("stockId is null");
		}
		if (stockType == null) {
			throw new IllegalArgumentException("stockType is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select * ");
		sql.append(" from equip_stock_total est ");
		sql.append(" where 1=1 ");
		sql.append(" and est.stock_id = ? ");
		params.add(stockId);
		sql.append(" and est.stock_type = ? ");
		params.add(stockType.getValue());
		sql.append(" and est.equip_group_id = ? ");
		params.add(equipGroupId);
		
		return repo.getEntityBySQL(EquipStockTotal.class, sql.toString(), params);
	}

	@Override
	public void updateEquipmentStockTotal(EquipStockTotal eqDeliveryRecord) throws DataAccessException {
		// TODO Auto-generated method stub
		if (eqDeliveryRecord == null) {
			throw new IllegalArgumentException("eqDeliveryRecord is null");
		}
		repo.update(eqDeliveryRecord);
	}
	
	@Override
	public List<EquipStockTotal> getListEquipStockTotal(Long stockId, EquipStockTotalType stockType, Long equipGroupId) throws DataAccessException{
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select * ");
		sql.append(" from equip_stock_total est ");
		sql.append(" where 1=1 ");
		if(stockId!=null){
			sql.append(" and est.stock_id = ? ");
			params.add(stockId);
		}
		if(stockType!=null){
			sql.append(" and est.stock_type = ? ");
			params.add(stockType.getValue());
		}
		if(equipGroupId!=null){
			sql.append(" and est.equip_group_id = ? ");
			params.add(equipGroupId);
		}
		
		return repo.getListBySQL(EquipStockTotal.class, sql.toString(), params);
	}

	@Override
	public EquipStockTotal createEquipStockTotal(EquipStockTotal equipStockTotal) throws DataAccessException {		
		// TODO Auto-generated method stub
		if (equipStockTotal == null) {
			throw new IllegalArgumentException("equipStockTotal is null");
		}
		return repo.create(equipStockTotal);
	}

	@Override
	public List<EquipmentStockDeliveryVO> getListEquipStockTotalVOByfilter(EquipmentFilter<EquipmentStockDeliveryVO> filter) throws DataAccessException {
		// TODO Auto-generated method stub
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		
		sql.append(" select  ");
		sql.append(" shop_id as shopId, ");
		sql.append(" shop_code as shopCode, ");
		sql.append(" shop_name as shopName, ");
		sql.append(" status as status, ");
		sql.append(" parent_shop_id as parentId ");
		sql.append(" from shop sh ");
		sql.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(shop_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			sql.append(" and lower(name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getName().trim().toLowerCase())));
		}
		if (filter.getStatus() != null) {
			sql.append(" and status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append(" and status != -1 ");
		}
		if(filter.getStockType() == null ){
			sql.append(" and sh.shop_type_id in (select channel_type_id as shop_type_id from channel_type where status = 1 and type = ? and (object_type = ? or object_type = ? or object_type = ?)) ");
			params.add(ChannelTypeType.SHOP.getValue());
			params.add(ShopObjectType.NPP.getValue());
			params.add(ShopObjectType.NPP_KA.getValue());
			params.add(ShopObjectType.VNM.getValue());
		}
		
		if(filter.getShopParentId()!= null ){
			sql.append(" and sh.shop_id in (select s1.shop_id from shop s1 where s1.status != -1 start with s1.shop_id = ? connect by prior s1.shop_id = s1.parent_shop_id ) ");
			params.add(filter.getShopParentId());
		}		
		sql.append(" order by sh.shop_code asc ");		

		countSql.append("  select count(*) as count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		
		final String[] fieldNames = new String[] { 
				"shopId", 
				"shopCode", 
				"shopName"};
		
		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING
				};
		
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipmentStockDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(EquipmentStockDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}

	@Override
	public EquipStockTransForm getEquipStockTransFormById(Long idRecord) throws DataAccessException {
		return repo.getEntityById(EquipStockTransForm.class, idRecord);
	}
	
	@Override
	public EquipStockTransForm getEquipStockTransFormById(Long idRecord, Long shopId) throws DataAccessException {
		if (idRecord == null) {
			throw new IllegalArgumentException("idRecord is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct estf.* ");
		sql.append(" from equip_stock_trans_form estf, equip_stock_trans_form_dtl estfd  ");
		sql.append(" where estf.equip_stock_trans_form_id = ? ");
		params.add(idRecord);
		sql.append(" and estf.equip_stock_trans_form_id = estfd.equip_stock_trans_form_id ");
		if (shopId != null) {
			sql.append(" AND  ");
			sql.append("  (estfd.from_stock_id IN ");
			sql.append(" (SELECT equip_stock_id ");
			sql.append(" FROM equip_stock ");
			sql.append(" WHERE status = 1 ");
			sql.append(" and shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append(" ) ");
			params.add(shopId);
			sql.append(" and  (estfd.to_stock_id IN ");
			sql.append(" (SELECT cu.customer_id ");
			sql.append(" FROM customer cu ");
			sql.append(" WHERE  ");
			sql.append(" cu.shop_id IN ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ) ");
			sql.append("  ");
			sql.append(" ) ");
			params.add(shopId);
		}
		return repo.getEntityBySQL(EquipStockTransForm.class, sql.toString(), params);
	}	
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.ApParamEquip;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamEquipType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.vo.ApParamEquipVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface ApParamEquipDAO {

	ApParamEquip createApParamEquip(ApParamEquip ApParamEquip, LogInfoVO logInfo) throws DataAccessException;

	void deleteApParamEquip(ApParamEquip ApParamEquip, LogInfoVO logInfo) throws DataAccessException;

	void updateApParamEquip(ApParamEquip ApParamEquip, LogInfoVO logInfo) throws DataAccessException;
	
	ApParamEquip getApParamEquipById(long id) throws DataAccessException;

	ApParamEquip getApParamEquipByCode(String code, ApParamEquipType type) throws DataAccessException;
	
	ApParamEquip getApParamEquipByCodeX(String code, ApParamEquipType type, ActiveType status) throws DataAccessException;


	ApParamEquip getApParamEquipByCodeActive(String code, ActiveType status) 	throws DataAccessException;

	List<ApParamEquip> getListApParamEquipByType(ApParamEquipType type, ActiveType status)	throws DataAccessException;
	
	List<ApParamEquip> getListApParamEquipByFilter(ApParamFilter filter)  throws DataAccessException;

	List<ApParamEquipVO> getListEmailToRepairByFilter(ApParamFilter filter) throws DataAccessException;
	/**
	 * Lay danh sach shop cap cha va Equip-Apparam neu co tuong ung
	 * 
	 * @author hunglm16
	 * @since 01/07/2015
	 */
	List<ApParamEquipVO> getListEmailToParentShopByFilter(ApParamFilter filter) throws DataAccessException;

}

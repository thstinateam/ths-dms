package ths.dms.core.dao;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.RptStockTotalMonth;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class RptStockTotalMonthDAOImpl implements RptStockTotalMonthDAO {

	@Autowired
	private IRepository repo;
	

	@Override
	public RptStockTotalMonth createRptStockTotalMonth(RptStockTotalMonth rptStockTotalMonth) throws DataAccessException {
		if (rptStockTotalMonth == null) {
			throw new IllegalArgumentException("rptStockTotalMonth");
		}
		repo.create(rptStockTotalMonth);
		return repo.getEntityById(RptStockTotalMonth.class, rptStockTotalMonth.getId());
	}

	@Override
	public void deleteRptStockTotalMonth(RptStockTotalMonth rptStockTotalMonth) throws DataAccessException {
		if (rptStockTotalMonth == null) {
			throw new IllegalArgumentException("rptStockTotalMonth");
		}
		repo.delete(rptStockTotalMonth);
	}

	@Override
	public RptStockTotalMonth getRptStockTotalMonthById(long id) throws DataAccessException {
		return repo.getEntityById(RptStockTotalMonth.class, id);
	}
	
	@Override
	public void updateRptStockTotalMonth(RptStockTotalMonth rptStockTotalMonth) throws DataAccessException {
		if (rptStockTotalMonth == null) {
			throw new IllegalArgumentException("rptStockTotalMonth");
		}
		repo.update(rptStockTotalMonth);
	}
}

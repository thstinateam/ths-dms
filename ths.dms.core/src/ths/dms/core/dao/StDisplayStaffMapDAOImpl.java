package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ChannelType;
import ths.dms.core.entities.StDisplayProgram;
import ths.dms.core.entities.StDisplayStaffMap;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.StaffFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class StDisplayStaffMapDAOImpl implements StDisplayStaffMapDAO {
	@Autowired
	private IRepository repo;
	
	@Autowired 
	private StaffDAO staffDAO;
	
	@Autowired
	DisplayProgrameDAO displayProgramDAO;
	
	
	@Autowired
	CommonDAO commonDAO;
	
	
	@Autowired
	ShopDAO shopDAO;
	
	@Override
	public StDisplayStaffMap createDisplayStaffMap(StDisplayStaffMap displayStaffMap, LogInfoVO logInfo) throws DataAccessException {
		if (displayStaffMap == null) {
			throw new IllegalArgumentException("displayStaffMap");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo");
		}	
		repo.create(displayStaffMap);		
		return repo.getEntityById(StDisplayStaffMap.class, displayStaffMap.getId());
	}
	
	@Override
	public StDisplayStaffMap getDisplayStaffMap(Long dpId,Long staffId,Date month) throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		sql.append("select * from display_staff_map dsm");		
		sql.append(" where status=1 and staff_id = ? and display_program_id = ?");
		sql.append(" and to_char(month,'MM/YYYY') = ? ");		
		List<Object> params = new ArrayList<Object>();		
		params.add(staffId);
		params.add(dpId);
		params.add(DateUtility.toMonthYearString(month));		
		return repo.getEntityBySQL(StDisplayStaffMap.class, sql.toString(), params);
	}

	@Override
	public void deleteDisplayStaffMap(StDisplayStaffMap displayStaffMap, LogInfoVO logInfo) throws DataAccessException {
		if (displayStaffMap == null) {
			throw new IllegalArgumentException("displayStaffMap");
		}
		repo.delete(displayStaffMap);		
	}

	@Override
	public StDisplayStaffMap getDisplayStaffMapById(long id) throws DataAccessException {
		return repo.getEntityById(StDisplayStaffMap.class, id);
	}
	
	@Override
	public StDisplayStaffMap getDisplayStaffMapByIdForUpdate(long id) throws DataAccessException {
		String sql = "select * from display_staff_map dsm where dsm.display_staff_map_id = ? for update";
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		return repo.getEntityBySQL(StDisplayStaffMap.class, sql, params);
	}
	
	@Override
	public void updateDisplayStaffMap(StDisplayStaffMap displayStaffMap, LogInfoVO logInfo) throws DataAccessException {
		if (displayStaffMap == null) {
			throw new IllegalArgumentException("displayStaffMap");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo");
		}
//		if (displayStaffMap.getStaff() != null && displayStaffMap.getStatus() == ActiveType.RUNNING)
//			if (checkIfRecordExist(displayStaffMap.getDisplayProgram().getId(), displayStaffMap.getStaff().getStaffId(), displayStaffMap.getId()))
//				return ;
//		StDisplayStaffMap temp = this.getDisplayStaffMapById(displayStaffMap.getId());
//		temp = temp.clone();
		
//		displayStaffMap.setUpdateDate(commonDAO.getSysDate());
//		displayStaffMap.setUpdateUser(logInfo.getStaffCode());
		repo.update(displayStaffMap);	
	}
	
	@Override
	public Boolean isUsingByOthers(long displayStaffMapId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		
		sql.append(" select count(1) as count from display_staff_map  ");
		sql.append(" where exists (select 1 from display_customer_map where display_program_level_id=? and status != ?) ");
		
		List<Object> params = new ArrayList<Object>();
		params.add(displayStaffMapId);
		params.add(ActiveType.DELETED.getValue());
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}
	
	@Override
	public Boolean checkIfRecordExist(long displayProgramId, long staffId, Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(1) as count from display_staff_map p ");
		sql.append(" where display_program_id=? and staff_id=? and status != ?");
		
		List<Object> params = new ArrayList<Object>();
		params.add(displayProgramId);
		params.add(staffId);
		params.add(ActiveType.DELETED.getValue());
		
		if (id != null) {
			sql.append(" and display_staff_map_id != ?");
			params.add(id);
		}
		
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true; 
	}
	
	@Override
	public List<StDisplayStaffMap> getListDisplayStaffMap(KPaging<StDisplayStaffMap> kPaging, Long displayProgramId,
			ActiveType status,  List<Long> lstShopId,String month, boolean checkMap, long parentStaffId)
			throws DataAccessException {		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select dsm.* from display_staff_map dsm ,display_program sdp,shop s ,staff st where 1=1 ");
		if(displayProgramId!=null && displayProgramId>0){
			sql.append(" and dsm.display_program_id=? ");
			params.add(displayProgramId);
		}
		sql.append(" and dsm.display_program_id = sdp.display_program_id ");
		sql.append(" and dsm.staff_id = st.staff_id ");		
		sql.append(" and dsm.quantity_max IS NOT NULL ");		
		if (status != null) {
			sql.append(" and dsm.status=?");
			params.add(status.getValue());
		}
		if(!StringUtility.isNullOrEmpty(month)){
			sql.append(" and to_char(month,'MM/YYYY') = ? ");	
			params.add(month);
		}
		if (lstShopId != null && lstShopId.size()>0) {
			sql.append(" and  st.shop_id in (SELECT shop_id FROM shop START WITH shop_id in(-1 ");
			for (Long shopId : lstShopId) {
				sql.append(",?");
				params.add(shopId);
			}
			sql.append(")");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id)");
		}
		sql.append(" and st.shop_id = s.shop_id ");
		
		if (checkMap) {
			sql.append("and st.staff_id in (");
			sql.append(" select psm.staff_id from parent_staff_map psm");
			sql.append(" where psm.status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" start with psm.parent_staff_id = ? and psm.status = ? connect by prior psm.staff_id = psm.parent_staff_id");
			params.add(parentStaffId);
			params.add(ActiveType.RUNNING.getValue());
			sql.append(")");
		}
		sql.append(" and not exists (select 1 from exception_user_access where status = ? and parent_staff_id = ? and staff_id = st.staff_id)");
		params.add(ActiveType.RUNNING.getValue());
		params.add(parentStaffId);
		
		sql.append(" order by sdp.display_program_code,s.shop_code,st.staff_code");
		if (kPaging == null)
			return repo.getListBySQL(StDisplayStaffMap.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StDisplayStaffMap.class, sql.toString(), params, kPaging);
	}
	public ChannelType getChannelTypeById(long id) throws DataAccessException {
		return repo.getEntityById(ChannelType.class, id);
	}
	
	@Override
	public Integer checkShopOfStaffJoinDisplayProgram(Long displayProgramId,Long shopId) throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(*) as count ");
		sql.append(" from shop s where s.shop_id=? and s.shop_id ");
		params.add(shopId);
		sql.append(" in (select dsm.shop_id from display_shop_map  dsm");
		sql.append(" where display_program_id=? and dsm.status = 1 ) ");
		params.add(displayProgramId);
		return repo.countBySQL(sql.toString(), params);
	}
	
	public List<Staff> getListStaff(StaffFilter filter) throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select st.* from STAFF st join shop s on st.shop_id = s.shop_id where (1=1 ");
		
		if(filter.getShopId() != null){
			sql.append(" and st.shop_id = ? ");
			params.add(filter.getShopId());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and st.staff_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffName())) {
			String temp = StringUtility.toOracleSearchLike(filter.getStaffName().toLowerCase());
			sql.append(" and lower(st.staff_name) like ? ESCAPE '/'");
			params.add(temp);
		}
		
		if (filter.getStatus() != null) {
			sql.append(" and st.status=?");
			params.add(filter.getStatus().getValue());
		}
		if (filter.getStaffTypeCode() != null) {
			sql.append(" and exists (select 1 from channel_type ct where ct.status != ? and st.STAFF_TYPE_ID=ct.channel_type_id and ct.channel_type_code=?)");
			params.add(ActiveType.DELETED.getValue());
			params.add(filter.getStaffTypeCode());
		}
		
		
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and ( st.shop_id in (select shop_id from shop start with shop_code=?  connect by prior shop_id=parent_shop_id )");
			params.add(filter.getShopCode().toUpperCase());
			sql.append(" 	or STAFF_ID IN (select STAFF_ID from staff_group_detail sgd join staff_group sg on sgd.staff_group_id = sg.staff_group_id where sg.status = 1 and sgd.status = 1 and sg.shop_id in (select shop_id from shop start with shop_id = (select shop_id from shop where shop_code =? ) connect by prior shop_id = parent_shop_id)))");
			params.add(filter.getShopCode().toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getSaleTypeCode())) {
			sql.append(" and st.sale_type_code =? ");
			params.add(filter.getSaleTypeCode().toUpperCase());
		}
		
		sql.append(")");
		sql.append(" order by staff_code asc");
		if (filter.getkPaging() == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, filter.getkPaging());
	}
	@Override
	public Boolean createDisplayStaffMap(long displayProgramId, String shopCode, Long staffTypeId, String staffCode, LogInfoVO logInfo)
			throws DataAccessException {
		StDisplayProgram dp = displayProgramDAO.getStDisplayProgramById(displayProgramId);
		if (dp != null) {
			if (staffTypeId != null) {
				ChannelType staffType = this.getChannelTypeById(staffTypeId);
				StaffFilter filter = new StaffFilter();
				filter.setStaffTypeCode(staffType != null ? staffType.getChannelTypeCode() : null);
				filter.setStatus(ActiveType.RUNNING);
				filter.setShopCode(shopCode);
				List<Staff> lstStaff = this.getListStaff(filter);
				List<StDisplayStaffMap> lstDisplayStaff = new ArrayList<StDisplayStaffMap>();
				for (Staff s: lstStaff) {
					if (!this.checkIfRecordExist(displayProgramId, s.getId(), null)){
						StDisplayStaffMap m = new StDisplayStaffMap();
						m.setDisplayProgram(dp);
						m.setStaff(s);
						m.setCreateUser(logInfo.getStaffCode());
						lstDisplayStaff.add(m);
					}
				}
				repo.create(lstDisplayStaff);
				if (lstDisplayStaff.size() != 0)
					return true;
				else
					return false;
			}
			else if (!StringUtility.isNullOrEmpty(staffCode)) {
				Staff s = staffDAO.getStaffByCode(staffCode);
				if (s != null) {
					StDisplayStaffMap m = new StDisplayStaffMap();
					m.setDisplayProgram(dp);
					m.setCreateUser(logInfo.getStaffCode());
					m.setStaff(s);
					repo.create(m);
					return true;
				}
				return false;
			}
		}
		return false;
	}
	
}

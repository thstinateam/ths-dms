package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.List;

import ths.dms.core.entities.SaleLevelCat;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.SaleCatLevelVO;
import ths.dms.core.exceptions.DataAccessException;

public interface SaleLevelCatDAO {

	SaleLevelCat createSaleLevelCat(SaleLevelCat saleLevelCat, LogInfoVO logInfo) throws DataAccessException;

	void deleteSaleLevelCat(SaleLevelCat saleLevelCat, LogInfoVO logInfo) throws DataAccessException;

	void updateSaleLevelCat(SaleLevelCat saleLevelCat, LogInfoVO logInfo) throws DataAccessException;
	
	SaleLevelCat getSaleLevelCatById(long id) throws DataAccessException;

	List<SaleLevelCat> getListSaleLevelCat(KPaging<SaleLevelCat> kPaging,
			Long catId, String catName, String saleLevel, ActiveType status)
			throws DataAccessException;
	
	/**
	 * Lay danh sach muc doanh so theo nghanh hang
	 * 
	 * @param kPaging
	 * @param listProInfoCode
	 * @param catCode
	 * @param catName
	 * @param fromAmount
	 * @param toAmount
	 * @param status
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleLevelCat> getListSaleLevelCatByProductInfo(
			KPaging<SaleLevelCat> kPaging, List<String> listProInfoCode,
			String saleLevelCode, String saleLevelName, BigDecimal fromAmount,
			BigDecimal toAmount, ActiveType status,String sort,String order) throws DataAccessException;

	Boolean checkIfRecordExist(long catId, String saleLevel, Long id)
			throws DataAccessException;

	SaleLevelCat getSaleLevelCatByCatAndSaleLevel(Long catId, String saleLevel)
			throws DataAccessException;

	List<String> getListSaleLevel(Long catId, String catName,
			String saleLevel, ActiveType status) throws DataAccessException;
	
	List<SaleCatLevelVO> getListSaleCatLevelVOByIdPro(Long id) throws DataAccessException;
	
	List<SaleCatLevelVO> getListSaleCatLevelVOByIdProAlreadySet(KPaging<SaleCatLevelVO> kPaging, long promotionProgramId) throws DataAccessException;
	List<SaleCatLevelVO> getListSaleCatLevelVOByIdProAlreadySetSO(KPaging<SaleCatLevelVO> kPaging, long promotionProgramId) throws DataAccessException;
}
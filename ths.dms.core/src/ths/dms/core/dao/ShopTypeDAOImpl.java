package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ShopType;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;



public class ShopTypeDAOImpl implements ShopTypeDAO {


	@Autowired
	private IRepository repo;
	
	@Override
	public ShopType getShopTypeByCode(String code) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(code)) {
			throw new IllegalArgumentException("shoptype");
		}
		code = code.toUpperCase();
		String sql = "select * from shop_type where upper(name) = upper(?)";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		ShopType rs = repo.getEntityBySQL(ShopType.class, sql, params);
		return rs;
	}

	@Override
	public List<ShopType> getListShopTypeByType(List<Integer> status)
			throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from shop_type where 1 = 1 ");
		
		if (status != null && status.size() > 0) {
			sql.append(" and STATUS in ( ");
			String subSql = "";
			for (Integer stt : status) {
				subSql += ", ?";
				params.add(stt);
			}
			sql.append(subSql.replaceFirst(", ", ""));
			sql.append(" ) ");
		}
		
		return repo.getListBySQL(ShopType.class, sql.toString(), params);
	}	
}
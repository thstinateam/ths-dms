package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.ProductLotVO;
import ths.core.entities.vo.rpt.RptStockTransDetailVO;
import ths.core.entities.vo.rpt.RptStockTransInMoveVO;
import ths.dms.core.exceptions.DataAccessException;

public interface StockTransDetailDAO {

	StockTransDetail createStockTransDetail(StockTransDetail stockTransDetail) throws DataAccessException;

	void deleteStockTransDetail(StockTransDetail stockTransDetail) throws DataAccessException;

	void updateStockTransDetail(StockTransDetail stockTransDetail) throws DataAccessException;

	StockTransDetail getStockTransDetailById(long id) throws DataAccessException;

	Boolean isUsingByOthers(long stockTransDetailId) throws DataAccessException;

	List<StockTransDetail> getListStockTransDetailByStockTransId(long stockTransId) throws DataAccessException;

	List<RptStockTransDetailVO> getListReportStockTransDetail(Long shopId, Date fromDate, Date toDate) throws DataAccessException;

	StockTransDetail getStockTransDetailForUpdate(Long stockTransDetailId, Long productId) throws DataAccessException;

	List<ProductLotVO> getListStockTransDetailForVanSale(Long shopId, StockObjectType shopType, Long ownerId, StockObjectType ownerType, Integer productId) throws DataAccessException;

	List<RptStockTransInMoveVO> getListRptStrockTransInMove(Long stockTransId) throws DataAccessException;

}

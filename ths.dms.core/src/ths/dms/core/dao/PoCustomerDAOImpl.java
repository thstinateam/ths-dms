package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PoCustomer;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class PoCustomerDAOImpl implements PoCustomerDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	CommonDAO commonDAO;

	@Override
	public void deletePoCustomer(PoCustomer poCustomer)
			throws DataAccessException {
		if (poCustomer == null) {
			throw new IllegalArgumentException("poCustomer");
		}
		repo.delete(poCustomer);
	}

	@Override
	public void updatePoCustomer(PoCustomer poCustomer)
			throws DataAccessException {
		if (poCustomer == null) {
			throw new IllegalArgumentException("poCustomer");
		}
		poCustomer.setUpdateDate(commonDAO.getSysDate());
		repo.update(poCustomer);
	}

	@Override
	public PoCustomer getPoCustomerById(long id) throws DataAccessException {
		return repo.getEntityById(PoCustomer.class, id);
	}

	@Override
	public PoCustomer getPoCustomerByOrderNumberAndShopId(String orderNumber,
			Long shopId, Date orderDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.* from po_customer so");
		sql.append(" where 1 = 1");
		sql.append(" and so.order_number = ? ");
		sql.append(" and so.shop_id = ? ");
		sql.append(" and so.order_date >= trunc(?)");
		sql.append(" and so.order_date < trunc(?) + 1");
		params.add(orderNumber);
		params.add(shopId);
		params.add(orderDate);
		params.add(orderDate);
		
		return repo.getEntityBySQL(PoCustomer.class, sql.toString(), params);
	}

	@Override
	public PoCustomer getPoCustomerByFromSaleorderId(Long saleOrderId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.* from po_customer so");
		sql.append(" where 1 = 1");
		sql.append(" and so.from_sale_order_id = ? ");
		params.add(saleOrderId);
		
		return repo.getEntityBySQL(PoCustomer.class, sql.toString(), params);
	}

	@Override
	public PoCustomer getPoCustomerById(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.* from po_customer so");
		sql.append(" where 1 = 1");
		sql.append(" and so.po_customer_id = ? ");
		params.add(id);
		
		return repo.getEntityBySQL(PoCustomer.class, sql.toString(), params);
	}
	

}

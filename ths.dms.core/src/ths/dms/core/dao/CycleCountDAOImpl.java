package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.Cycle;
import ths.dms.core.entities.CycleCount;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.CycleCountFilter;
import ths.dms.core.entities.enumtype.CycleCountSearchFilter;
import ths.dms.core.entities.enumtype.CycleCountType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.vo.CycleCountSearchVO;
import ths.dms.core.entities.vo.CycleCountVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class CycleCountDAOImpl implements CycleCountDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	CommonDAO commonDAO;
	
	@Autowired
	CycleDAO cycleDAO;
	
	@Override
	public CycleCount createCycleCount(CycleCount cycleCount) throws DataAccessException {
		if (cycleCount == null) {
			throw new IllegalArgumentException("cycleCount");
		}
		if (cycleCount.getCycleCountCode() != null)
			cycleCount.setCycleCountCode(cycleCount.getCycleCountCode().toUpperCase());
		
		if (cycleCount.getShop() != null)
			if (checkOngoingCycleCount(cycleCount.getShop().getId(), cycleCount.getWarehouse().getId()))
				return null;
		
		repo.create(cycleCount);//1//2
		
		if (cycleCount.getShop() != null)
			if (!checkOneOngoingCycleCount(cycleCount.getShop().getId(), cycleCount.getWarehouse().getId())) {
				this.deleteCycleCount(cycleCount);
				return null;
			}
				
		
		return repo.getEntityById(CycleCount.class, cycleCount.getId());
	}

	@Transactional(rollbackFor = Exception.class)
	@Override
	public void deleteCycleCount(CycleCount cycleCount) throws DataAccessException {
		if (cycleCount == null) {
			throw new IllegalArgumentException("cycleCount");
		}
		String sql1 = "delete from cycle_count_result where cycle_count_id=?";
		String sql2 = "delete from cycle_count_map_product where cycle_count_id=?";
		String sql3 = "delete from cycle_count where cycle_count_id=?";
		List<Object> params = new ArrayList<Object>();
		params.add(cycleCount.getId());
		repo.executeSQLQuery(sql1, params);
		repo.executeSQLQuery(sql2, params);
		repo.executeSQLQuery(sql3, params);
	}

	@Override
	public CycleCount getCycleCountById(long id) throws DataAccessException {
		return repo.getEntityById(CycleCount.class, id);
	}
	
	/*@Override
	public CycleCount getCycleCountByCode(String code) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from cycle_count where cycle_count_code=? and status!=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code);
		params.add(ActiveType.DELETED.getValue());
		return repo.getEntityBySQL(CycleCount.class, sql, params);
	}*/
	
	@Override
	public CycleCount getCycleCountByCodeAndShop(String code, Long shopId) throws DataAccessException {		
		code = code.toUpperCase();
		String sql = "select * from cycle_count where cycle_count_code=? and status!=? and shop_id = ? ";
		List<Object> params = new ArrayList<Object>();
		params.add(code);
		params.add(ActiveType.DELETED.getValue());
		params.add(shopId);
		return repo.getEntityBySQL(CycleCount.class, sql, params);
	}
	
	@Transactional
	@Override
	public void updateCycleCount(CycleCount cycleCount) throws DataAccessException {
		if (cycleCount == null) {
			throw new IllegalArgumentException("cycleCount");
		}
		if (cycleCount.getCycleCountCode() != null)
			cycleCount.setCycleCountCode(cycleCount.getCycleCountCode().toUpperCase());
		cycleCount.setUpdateDate(commonDAO.getSysDate());
		repo.update(cycleCount);
	}
	
	@Override
	public List<CycleCount> getListCycleCount(KPaging<CycleCount> kPaging, String cycleCountCode,
			CycleCountType cycleCountStatus, String desc, Long shopId, Date fromDate, Date toDate, Boolean isCompleteOrOngoing, Long wareHouseId, String strListShopId) 
			throws DataAccessException {
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<CycleCount>();
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from cycle_count where 1=1");
		if (!StringUtility.isNullOrEmpty(cycleCountCode)) {
			sql.append(" and cycle_count_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(cycleCountCode.toUpperCase()));
		}
		if (cycleCountStatus != null) {
			sql.append(" and status=?");
			params.add(cycleCountStatus.getValue());
		}
		else if (Boolean.TRUE.equals(isCompleteOrOngoing)) {
			sql.append(" and (status = ? or status = ?)");
			params.add(CycleCountType.COMPLETED.getValue());
			params.add(CycleCountType.ONGOING.getValue());
		}
		
		if (!StringUtility.isNullOrEmpty(desc)) {
			sql.append(" and lower(description) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(desc.toLowerCase()));
		}
		if (shopId != null) {
			sql.append(" and shop_id=?");
			params.add(shopId);
		}
		if (!StringUtility.isNullOrEmpty(strListShopId)) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(strListShopId, "shop_id");
			sql.append(paramsFix);
		}
		if (wareHouseId != null) {
			sql.append(" and wareHouse_id=?");
			params.add(wareHouseId);
		}
		if (fromDate != null) {
			sql.append(" and START_DATE >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and START_DATE < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append(" order by start_date desc, cycle_count_id");
		if (kPaging != null)
			return repo.getListBySQLPaginated(CycleCount.class, sql.toString(), params, kPaging);
		else 
			return repo.getListBySQL(CycleCount.class, sql.toString(), params);
	}
	
	@Override
	public List<CycleCount> getListCycleCount(CycleCountSearchFilter filter) throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate()))
			return new ArrayList<CycleCount>();
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from cycle_count where 1=1");
		if (!StringUtility.isNullOrEmpty(filter.getCycleCountCode())) {
			sql.append(" and cycle_count_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCycleCountCode().toUpperCase()));
		}
		if (filter.getStatus() != null) {
			sql.append(" and status=?");
			params.add(filter.getStatus().getValue());
		}
		
		if (filter.getStatusLst() != null && filter.getStatusLst().size() > 0) {
			sql.append(" and status in (? ");
			params.add(filter.getStatusLst().get(0).getValue());
			for (int i = 1, n = filter.getStatusLst().size(); i < n; i++) {
				sql.append(", ? ");
				params.add(filter.getStatusLst().get(i).getValue());
			}
			sql.append(" ) ");
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getDescription())) {
			sql.append(" and lower(description) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getDescription().toLowerCase()));
		}
		if (filter.getShopId() != null) {
			sql.append(" and shop_id=?");
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "shop_id");
			sql.append(paramsFix);
		}
		if (filter.getWareHouseId() != null) {
			sql.append(" and wareHouse_id=?");
			params.add(filter.getWareHouseId());
		}
		if (filter.getFromDate() != null) {
			sql.append(" and START_DATE >= trunc(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and START_DATE < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		sql.append(" order by start_date desc, cycle_count_id");
		if (filter.getkPaging() != null)
			return repo.getListBySQLPaginated(CycleCount.class, sql.toString(), params, filter.getkPaging());
		else 
			return repo.getListBySQL(CycleCount.class, sql.toString(), params);
	}
	
	@Override
	public List<CycleCountVO> getListCycleCountVO(KPaging<CycleCountVO> kPaging, String cycleCountCode,
			CycleCountType cycleCountStatus, String desc, Long shopId, Date fromDate, Date toDate,Date fromCreate,Date toCreate, Long wareHouseId, String strListShopId)
			throws DataAccessException {
		
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<CycleCountVO>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" select c.cycle_count_id as id, c.create_date as createDate,c.START_DATE as startDate,");
		sql.append("       c.cycle_count_code as cycleCountCode,");
		sql.append("       c.description as description,");
		sql.append("       c.status as status,");
		sql.append("       wh.warehouse_code as wareHouseCode,");
		sql.append("       1 as canUpdate");
//		sql.append("       (case when count(ccm.cycle_count_id) > 0 then 0 else 1 end) as canUpdate");

		sql.append(" from cycle_count c left join cycle_count_map_product ccm on c.cycle_count_id = ccm.cycle_count_id");
		sql.append(" JOIN warehouse wh ON wh.warehouse_id = c.warehouse_id ");
		fromSql.append(" where 1 = 1");
		if (!StringUtility.isNullOrEmpty(cycleCountCode)) {
			fromSql.append(" and c.cycle_count_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(cycleCountCode.toUpperCase()));
		}
		if (cycleCountStatus != null) {
			fromSql.append(" and c.status=?");
			params.add(cycleCountStatus.getValue());
		}
		if (!StringUtility.isNullOrEmpty(desc)) {
			fromSql.append(" and lower(c.description) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(desc.toLowerCase()));
		}
		if (shopId != null) {
			fromSql.append(" and c.shop_id=?");
			params.add(shopId);
		}
		if (!StringUtility.isNullOrEmpty(strListShopId)) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(strListShopId, "c.shop_id");
			fromSql.append(paramsFix);
		}
		if (wareHouseId != null) {
			fromSql.append(" and c.warehouse_id=?");
			params.add(wareHouseId);
		}
		if (fromDate != null) {
			fromSql.append(" and c.START_DATE >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			fromSql.append(" and c.START_DATE < trunc(?) + 1");
			params.add(toDate);
		}
		
		if (fromCreate != null) {
			fromSql.append(" and c.CREATE_DATE >= trunc(?)");
			params.add(fromCreate);
		}
		if (toCreate != null) {
			fromSql.append(" and c.CREATE_DATE < trunc(?) + 1 ");
			params.add(toCreate);
		}
		fromSql.append(" and c.warehouse_id in(select w.warehouse_id from warehouse w where 1=1 ");
		if (shopId != null) {
			fromSql.append(" and w.shop_id=? ");
			params.add(shopId);
		}
		fromSql.append(")");
		sql.append(fromSql);
		sql.append(" group by c.cycle_count_id, c.create_date,c.start_date, c.cycle_count_code, c.description, c.status, wh.warehouse_code");
		sql.append(" order by c.create_date desc");
		
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from (");
		countSql.append(sql);
		countSql.append(" )");
		String[] fieldNames = { "id", 
				"createDate", // 0
				"startDate", // 0
				"cycleCountCode", // 1
				"description", // 2
				"status", // 3
				"wareHouseCode", // 5
				"canUpdate", // 4
				
		};

		Type[] fieldTypes = {StandardBasicTypes.LONG, 
				StandardBasicTypes.DATE, // 0
				StandardBasicTypes.DATE, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.INTEGER, // 4				
		};

		if (kPaging == null)
			return repo.getListByQueryAndScalar(CycleCountVO.class, fieldNames,
					fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(CycleCountVO.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, kPaging);
	}
	
	@Override
	public List<CycleCountSearchVO> getListCycleCountSearchVO(CycleCountFilter filter) throws DataAccessException {
		
		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate()))
			return new ArrayList<CycleCountSearchVO>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" SELECT c.CYCLE_COUNT_ID id, ");
		sql.append("   s2.SHOP_NAME vungshopName, ");
		sql.append("   s1.SHOP_NAME areaShopName,");
		sql.append("   s.SHOP_NAME shopName, ");
		sql.append("   s.PHONE shopPhone, ");
		sql.append("   s.ADDRESS shopAddress, ");
		sql.append("   w.WAREHOUSE_NAME wareHouseName,");
		sql.append("   c.CYCLE_COUNT_CODE CycleCountCode, ");
		sql.append("   c.START_DATE startDate, ");
		sql.append("   c.CREATE_USER createUser, ");
		sql.append("   CASE WHEN c.STATUS = 1 THEN c.APPROVED_DATE END approvedDate, ");
		sql.append("   CASE WHEN c.STATUS = 1 THEN c.UPDATE_USER END approvedUser, ");
		sql.append("   NVL(c.STATUS, 5) status");
		
		fromSql.append(" FROM SHOP s ");
		fromSql.append(" JOIN SHOP_TYPE st on s.SHOP_TYPE_ID = st.SHOP_TYPE_ID ");
		fromSql.append(" LEFT JOIN CYCLE_COUNT c ON c.SHOP_ID   = s.SHOP_ID ");
//		fromSql.append(" and c.status != ? ");
//		params.add(CycleCountType.CANCELLED.getValue());
		if (filter.getStatus() != null && CycleCountType.NOT_COUNTING.equals(filter.getStatus())) {
			fromSql.append(" and c.status = ? ");
			params.add(filter.getStatus().getValue());
		}
		if (filter.getYearPeriod() != null && filter.getNumPeriod() != null) {
			Cycle cycle = cycleDAO.getCycleByYearAndNum(filter.getYearPeriod(), filter.getNumPeriod());
			fromSql.append(" and c.CREATE_DATE >= trunc(?)");
			fromSql.append(" and c.CREATE_DATE < trunc(?) + 1 ");
			params.add(cycle.getBeginDate());
			params.add(cycle.getEndDate());
		}
		fromSql.append(" LEFT JOIN WAREHOUSE w ON c.WAREHOUSE_ID = w.WAREHOUSE_ID ");
		fromSql.append(" LEFT JOIN SHOP s1 on s.PARENT_SHOP_ID = s1.SHOP_ID ");
		fromSql.append(" LEFT JOIN SHOP s2 on s1.PARENT_SHOP_ID = s2.SHOP_ID ");
		fromSql.append(" WHERE 1 = 1 ");
		fromSql.append(" AND st.SPECIFIC_TYPE = ? ");
		params.add(ShopSpecificType.NPP.getValue());
		if (filter.getShopId() != null) {
			fromSql.append(" AND s.SHOP_ID IN ( ");
			fromSql.append("   SELECT SHOP_ID  ");
			fromSql.append("   FROM SHOP  ");
			fromSql.append("   START WITH shop_id = ? ");
			params.add(filter.getShopId());
			fromSql.append("   CONNECT BY PRIOR shop_id = parent_shop_id )");
		}
		if (!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "s.shop_id");
			fromSql.append(paramsFix);
		}		
		if (!StringUtility.isNullOrEmpty(filter.getCycleCountCode())) {
			fromSql.append(" and c.cycle_count_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCycleCountCode().toUpperCase()));
		}
		if (filter.getStatus() != null && !CycleCountType.NOT_COUNTING.equals(filter.getStatus())) {
			fromSql.append(" and c.status = ?");
			params.add(filter.getStatus().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getDescription())) {
			fromSql.append(" and lower(c.description) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getDescription().toLowerCase()));
		}
		if (filter.getWareHouseId() != null) {
			fromSql.append(" and c.warehouse_id=?");
			params.add(filter.getWareHouseId());
		}
		if (filter.getFromDate() != null) {
			fromSql.append(" and c.START_DATE >= trunc(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			fromSql.append(" and c.START_DATE < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		
		if (filter.getFromCreate() != null) {
			fromSql.append(" and c.CREATE_DATE >= trunc(?)");
			params.add(filter.getFromCreate());
		}
		if (filter.getToCreate() != null) {
			fromSql.append(" and c.CREATE_DATE < trunc(?) + 1 ");
			params.add(filter.getToCreate());
		}
		
		sql.append(fromSql);
		sql.append(" order by vungshopName, areaShopName, shopName, c.start_date, c.cycle_count_code ");
		
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count ");
		countSql.append(fromSql);
		
		String[] fieldNames = { "id", //1
				"vungshopName", 	// 2
				"areaShopName", 	// 3
				"shopName", 		// 4
				"shopPhone", 		// 5
				"shopAddress", 		// 6
				"wareHouseName", 	// 7
				"cycleCountCode", 	// 8
				"startDate", 		// 9
				"createUser", 		// 10
				"approvedDate", 	// 11
				"approvedUser", 	// 12
				"status", 			// 13
		};

		Type[] fieldTypes = {StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.STRING, // 7
				StandardBasicTypes.STRING, // 8
				StandardBasicTypes.DATE,   // 9
				StandardBasicTypes.STRING, // 10
				StandardBasicTypes.DATE,   // 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.STRING, // 13
		};

		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(CycleCountSearchVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(CycleCountSearchVO.class, fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, filter.getkPaging());
	}

	

	/** check if it has any cycle count is ongoing 
	 * @throws DataAccessException */
	
	
	@Override
	public Boolean checkOngoingCycleCount(long shopId, long warehouseId) throws DataAccessException {
		String sql = "select count(1) as count from CYCLE_COUNT where STATUS in (?, ?) and warehouse_Id = ? and shop_id = ? ";
		List<Object> params = new ArrayList<Object>();
		params.add(CycleCountType.ONGOING.getValue());
		params.add(CycleCountType.WAIT_APPROVED.getValue());
		params.add(warehouseId);
		params.add(shopId);
		int c = repo.countBySQL(sql, params);
		return c==0?false:true;
	}
	
	private Boolean checkOneOngoingCycleCount(long shopId, long warehouseId) throws DataAccessException {
		String sql = "select * from CYCLE_COUNT where STATUS in (?, ?) and warehouse_Id = ? and shop_id = ? for update";
		List<Object> params = new ArrayList<Object>();
		params.add(CycleCountType.ONGOING.getValue());
		params.add(CycleCountType.WAIT_APPROVED.getValue());
		params.add(warehouseId);
		params.add(shopId);
		List<CycleCount> lst = repo.getListBySQL(CycleCount.class, sql, params);
		return lst.size()==1?true:false;
	}
	
	@Override
	public Boolean checkCompleteCheckStock(long cycleCounting) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT count(1) as count ");
		sql.append(" FROM CYCLE_COUNT cc ");
		sql.append(" JOIN CYCLE_COUNT_MAP_PRODUCT ccmp on cc.CYCLE_COUNT_ID = ccmp.CYCLE_COUNT_ID ");
		sql.append(" JOIN STOCK_TOTAL tt on cc.WAREHOUSE_ID = tt.WAREHOUSE_ID AND ccmp.PRODUCT_ID = tt.PRODUCT_ID AND cc.SHOP_ID = tt.SHOP_ID ");
		sql.append(" WHERE cc.CYCLE_COUNT_ID = ? ");
		params.add(cycleCounting);
		sql.append(" AND NVL(tt.QUANTITY,0) <> NVL(ccmp.quantity_Counted,0) ");
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}
}

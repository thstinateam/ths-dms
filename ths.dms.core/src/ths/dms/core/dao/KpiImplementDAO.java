package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.KpiImplement;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.CommonFilter;

public interface KpiImplementDAO {
	KpiImplement getKpiImplement(Long staffId, Long kpiId, Long cycleId) throws DataAccessException;
	
	List<KpiImplement> getListKpiImplByFilter(KPaging<KpiImplement> kPaging, CommonFilter filter) throws DataAccessException;
}

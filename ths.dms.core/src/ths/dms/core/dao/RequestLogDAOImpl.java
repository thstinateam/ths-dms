package ths.dms.core.dao;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.RequestLog;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class RequestLogDAOImpl implements RequestLogDAO {

	@Autowired
	private IRepository repo;

	@Override
	public RequestLog createRequestLog(RequestLog requestLog) throws DataAccessException {
		if (requestLog == null) {
			throw new IllegalArgumentException("requestLog");
		}
		repo.create(requestLog);
		return repo.getEntityById(RequestLog.class, requestLog.getId());
	}

	@Override
	public void deleteRequestLog(RequestLog requestLog) throws DataAccessException {
		if (requestLog == null) {
			throw new IllegalArgumentException("requestLog");
		}
		repo.delete(requestLog);
	}

	@Override
	public RequestLog getRequestLogById(long id) throws DataAccessException {
		return repo.getEntityById(RequestLog.class, id);
	}
	
	@Override
	public void updateRequestLog(RequestLog requestLog) throws DataAccessException {
		if (requestLog == null) {
			throw new IllegalArgumentException("requestLog");
		}
		repo.update(requestLog);
	}
	
}

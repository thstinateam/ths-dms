package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PoAutoDetail;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.core.entities.vo.rpt.RptBookProductVinamilkVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class PoAutoDetailDAOImpl implements PoAutoDetailDAO {

	@Autowired
	private IRepository repo;

	@Override
	public PoAutoDetail createPoAutoDetail(PoAutoDetail poAutoDetail)
			throws DataAccessException {
		if (poAutoDetail == null) {
			throw new IllegalArgumentException("poAutoDetail");
		}
		repo.create(poAutoDetail);
		return repo.getEntityById(PoAutoDetail.class, poAutoDetail.getId());
	}

	@Override
	public void deletePoAutoDetail(PoAutoDetail poAutoDetail)
			throws DataAccessException {
		if (poAutoDetail == null) {
			throw new IllegalArgumentException("poAutoDetail");
		}
		repo.delete(poAutoDetail);
	}

	@Override
	public PoAutoDetail getPoAutoDetailById(int id) throws DataAccessException {
		return repo.getEntityById(PoAutoDetail.class, id);
	}

	@Override
	public void updatePoAutoDetail(PoAutoDetail poAutoDetail)
			throws DataAccessException {
		if (poAutoDetail == null) {
			throw new IllegalArgumentException("poAutoDetail");
		}
		repo.update(poAutoDetail);
	}

	@Override
	public List<PoAutoDetail> getListPoAutoDetail(KPaging<PoAutoDetail> kPaging, Long poAutoId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select pod.*");
		sql.append(" from po_auto_detail pod");
		sql.append(" left join product pr on pod.product_id = pr.product_id");
		sql.append(" where 1 = 1");

		if (poAutoId != null) {
			sql.append(" and pod.po_auto_id = ?");
			params.add(poAutoId);
		}

		sql.append(" order by pr.product_code asc");

		if (kPaging == null) {
			return repo.getListBySQL(PoAutoDetail.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(PoAutoDetail.class, sql.toString(), params, kPaging);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.PoAutoDetailDAO#getListRptBookProduct(ths.dms.core.entities.enumtype.KPaging, java.lang.Long, java.util.Date, java.util.Date)
	 */
	@Override
	public List<RptBookProductVinamilkVO> getListRptBookProduct( Long shopId, Date fromDate,
			Date toDate) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptBookProductVinamilkVO>();
		
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		if (toDate == null) {
			throw new IllegalArgumentException("toDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT pa.po_auto_date AS poAutoDate,");
		sql.append(" pa.po_auto_number AS poAutoNumber,");
		sql.append(" pa.status      AS status,");
		sql.append(" pad.quantity   AS quantity,");
		sql.append(" pad.price      AS priceValue,");
		sql.append(" pad.amount     AS amount,");
		sql.append(" p.product_code AS productCode,");
		sql.append(" p.product_name AS productName,");
		sql.append(" p.uom1         AS uom1,");
		sql.append(" p.gross_weight AS grossWeight");
		sql.append(" FROM po_auto pa,");
		sql.append(" po_auto_detail pad,");
		sql.append(" product p");
		sql.append(" WHERE pa.po_auto_id = pad.po_auto_id");
		sql.append(" AND pad.product_id  = p.product_id");
		sql.append(" AND (pa.status      = ?");
		sql.append(" OR pa.status        = ?)");
		params.add(ApprovalStatus.NOT_YET.getValue());
		params.add(ApprovalStatus.APPROVED.getValue());
		sql.append(" and pa.shop_id = ?");
		params.add(shopId);
		if (fromDate != null) {
			sql.append(" and pa.po_auto_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and pa.po_auto_date < (trunc(?) + 1)");
			params.add(toDate);
		}
		sql.append(" order by pa.po_auto_number, p.product_code");
		final String[] fieldNames = new String[] { //
				"poAutoDate",// 1
				"poAutoNumber", // 2
				"status", // 3
				"quantity", // 3
				"priceValue",//4
				"amount", // 5
				"productCode", // 6
				"productName", // 7
				"uom1", // 8
				"grossWeight", // 9
				};

		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.TIMESTAMP,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.INTEGER,// 3
				StandardBasicTypes.INTEGER,// 3
				StandardBasicTypes.FLOAT,// 4
				StandardBasicTypes.BIG_DECIMAL,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.FLOAT,// 9
		};
		return repo.getListByQueryAndScalar(RptBookProductVinamilkVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
}

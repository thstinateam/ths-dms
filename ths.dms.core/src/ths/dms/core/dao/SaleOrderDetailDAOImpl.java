package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.SalePromoMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.PromotionType;
import ths.dms.core.entities.enumtype.SaleOrderDetailFilter;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.SaleOrderPromoDetailFilter;
import ths.dms.core.entities.vo.PrintDeliveryCustomerGroupVO1;
import ths.dms.core.entities.vo.PrintDeliveryGroupExportVO2;
import ths.dms.core.entities.vo.PrintDeliveryGroupVO1;
import ths.dms.core.entities.vo.SaleOrderDetailGroupVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO2;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderLotVO;
import ths.dms.core.entities.vo.SaleOrderLotVOEx;
import ths.dms.core.entities.vo.SaleOrderPromotionDetailVO;
import ths.dms.core.entities.vo.TaxInvoiceDetailVO;
import ths.core.entities.vo.rpt.RptCustomerProductSaleOrder1VO;
import ths.core.entities.vo.rpt.RptExSaleOrderVO;
import ths.core.entities.vo.rpt.RptOrderDetailProduct4VO;
import ths.core.entities.vo.rpt.RptPayDetailDisplayProgr1VO;
import ths.core.entities.vo.rpt.RptProductDistrResult2VO;
import ths.core.entities.vo.rpt.RptPromotionDetailStaff2VO;
import ths.core.entities.vo.rpt.RptReturnSaleOrderAllowDeliveryVO;
import ths.core.entities.vo.rpt.RptReturnSaleOrderVO;
import ths.core.entities.vo.rpt.RptSaleOrderDetail2VO;
import ths.core.entities.vo.rpt.RptSaleOrderDetailVO;
import ths.core.entities.vo.rpt.RptSaleOrderLotVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class SaleOrderDetailDAOImpl implements SaleOrderDetailDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	CommonDAO commonDAO;

	@Override
	public SaleOrderDetail createSaleOrderDetail(SaleOrderDetail salesOrderDetail) throws DataAccessException {
		if (salesOrderDetail == null) {
			throw new IllegalArgumentException("salesOrderDetail");
		}
		//Lay gia tren giao dien
		/*if (salesOrderDetail.getPrice() != null)
			salesOrderDetail.setPriceValue(salesOrderDetail.getPrice().getPrice());*/
		repo.create(salesOrderDetail);
		return repo.getEntityById(SaleOrderDetail.class, salesOrderDetail.getId());
	}

	@Override
	public void deleteSaleOrderDetail(List<SaleOrderDetail> lstSalesOrderDetail) throws DataAccessException {
		if (lstSalesOrderDetail.isEmpty() || lstSalesOrderDetail == null) {
			throw new IllegalArgumentException("lstSalesOrderDetail");
		}
		for(SaleOrderDetail sod:lstSalesOrderDetail){
			repo.delete(sod);
		}
	}
	
	@Override
	public void deleteSaleOrderDetail(SaleOrderDetail salesOrderDetail) throws DataAccessException {
		if (salesOrderDetail == null) {
			throw new IllegalArgumentException("salesOrderDetail");
		}
		repo.delete(salesOrderDetail);
	}

	@Override
	public SaleOrderDetail getSaleOrderDetailById(long id) throws DataAccessException {
		return repo.getEntityById(SaleOrderDetail.class, id);
	}
	
	@Override
	public void updateSaleOrderDetail(SaleOrderDetail salesOrderDetail) throws DataAccessException {
		if (salesOrderDetail == null) {
			throw new IllegalArgumentException("salesOrderDetail");
		}
		if (salesOrderDetail.getPrice() != null)
			salesOrderDetail.setPriceValue(salesOrderDetail.getPrice().getPrice());
		salesOrderDetail.setUpdateDate(commonDAO.getSysDate());
		repo.update(salesOrderDetail);
	}
	
	
	@Override
	public void updateSaleOrderDetail(List<SaleOrderDetail> lstSalesOrderDetail) throws DataAccessException {
		if (lstSalesOrderDetail.isEmpty() || lstSalesOrderDetail == null) {
			throw new IllegalArgumentException("lstSalesOrderDetail");
		}
		for(SaleOrderDetail sod:lstSalesOrderDetail){
			sod.setUpdateDate(commonDAO.getSysDate());
			repo.update(sod);
		}
	}
	
	@Override
	public void createSaleOrderDetail(List<SaleOrderDetail> lstSaleOrderDetail)
			throws DataAccessException {
		if (lstSaleOrderDetail == null) {
			return;
		}
		repo.create(lstSaleOrderDetail);
	}
	
	@Override
	public void createSalePromoMap(List<SalePromoMap> lstSalePromoMaps) throws DataAccessException {
		if (lstSalePromoMaps == null) {
			return;
		}
		repo.create(lstSalePromoMaps);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SaleOrderDetailDAO#getListSaleOrderDetailBySaleOrderId(java.lang.Long)
	 */
	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailBySaleOrderId(
			long saleOrderId, KPaging<SaleOrderDetail> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sod.* from sale_order_detail sod");
		sql.append(" left join product p on (p.product_id = sod.product_id)");
		sql.append(" where sod.sale_order_id = ?");
		params.add(saleOrderId);
		sql.append(" order by sod.is_free_item, p.product_code");
		if (kPaging == null) {
			return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(SaleOrderDetail.class, sql.toString(), params, kPaging);
	}
	
	/**
	 * @author trietptm
	 * @param filter
	 * @return danh sach SaleOrderDetail theo filter
	 * @throws DataAccessException
	 */
	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailBySaleOrderId(SaleOrderDetailFilter<SaleOrderDetail> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sod.* from sale_order_detail sod");
		sql.append(" left join product p on (p.product_id = sod.product_id)");
		sql.append(" where 1 = 1");
		if (filter.getSaleOrderId() != null) {
			sql.append(" and sod.sale_order_id = ? ");
			params.add(filter.getSaleOrderId());
		}
		if (Boolean.TRUE.equals(filter.getIsNotKeyShop())) {
			sql.append(" AND (sod.program_type is null OR sod.program_type <> ?) ");
			params.add(ProgramType.KEY_SHOP.getValue());
		}
		sql.append(" order by sod.is_free_item, p.product_code");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(SaleOrderDetail.class, sql.toString(), params, filter.getkPaging());
	}
	
	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailBySaleOrderIdAndType(long saleOrderId, Boolean isAutoPromotion)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_detail");
		sql.append(" where sale_order_id = ?");
		params.add(saleOrderId);
		if (Boolean.TRUE.equals(isAutoPromotion)) {
			sql.append(" and is_free_item = 1 and program_type = ? ");
			params.add(ProgramType.AUTO_PROM.getValue());
		} else if (Boolean.FALSE.equals(isAutoPromotion)) {
			sql.append(" and ((is_free_item = 1 and program_type != ?) OR (is_free_item = 0))");
			params.add(ProgramType.AUTO_PROM.getValue());
		}
		sql.append(" order by my_time asc, product_id desc");
		//sql.append(" order by is_free_item, product_id");
		return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
	}
	
	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailBySaleOrderId(
			Long saleOrderId, Long invoiceId, KPaging<SaleOrderDetail> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_detail");
		sql.append(" where 1 = 1");
		if(saleOrderId!=null){
			sql.append(" and sale_order_id = ?");
			params.add(saleOrderId);
		}
		sql.append(" and invoice_id = ?");
		params.add(invoiceId);
		sql.append(" order by is_free_item, product_id");
		if (kPaging == null) {
			return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(SaleOrderDetail.class, sql.toString(), params, kPaging);
		}
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SaleOrderDetailDAO#getListSaleOrderDetailBySaleOrderId(java.lang.Long)
	 */
	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailByInvoiceId(
			Long invoiceId, KPaging<SaleOrderDetail> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_detail");
		sql.append(" where invoice_id = ?");
		params.add(invoiceId);
		if (kPaging == null) {
			return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(SaleOrderDetail.class, sql.toString(), params, kPaging);
		}
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SaleOrderDetailDAO#getListSaleOrderDetailBySOIDOrderByVat1(long, ths.dms.core.entities.enumtype.KPaging)
	 * 
	 * lay ra danh sach sale order ban
	 */
	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailBySOIDOrderByVat1(
			long saleOrderId, KPaging<SaleOrderDetail> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_detail");
		sql.append(" where sale_order_id = ? and invoice_id is null and is_free_item = 0 order by vat asc, sale_order_detail_id");
		params.add(saleOrderId);
		if (kPaging == null) {
			return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(SaleOrderDetail.class, sql.toString(), params, kPaging);
		}
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SaleOrderDetailDAO#getListSaleOrderDetailBySOIDOrderByVat2(long, ths.dms.core.entities.enumtype.KPaging)
	 * 
	 * lay ra danh sach sale order km hang
	 */
	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailBySOIDOrderByVat2(
			long saleOrderId, KPaging<SaleOrderDetail> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_detail");
		sql.append(" where sale_order_id = ? and invoice_id is null and quantity > 0 and is_free_item = 1 and program_type != 2 and program_type != 5 and program_type != 3 and program_type != 4 and discount_amount is null order by sale_order_detail_id");
		params.add(saleOrderId);
		if (kPaging == null) {
			return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(SaleOrderDetail.class, sql.toString(), params, kPaging);
		}
	}
	
	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SaleOrderDetailDAO#getListSaleOrderDetailBySOIDOrderByVat3(long, ths.dms.core.entities.enumtype.KPaging)
	 * 
	 * lay ra danh sach sale order km tien
	 */
	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailBySOIDOrderByVat3(
			long saleOrderId, KPaging<SaleOrderDetail> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_detail");
		sql.append(" where sale_order_id = ? and invoice_id is null and is_free_item = 1 and program_type != 2 and program_type != 5 and program_type != 3 and program_type != 4 and discount_amount is not null order by sale_order_detail_id");
		params.add(saleOrderId);
		if (kPaging == null) {
			return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(SaleOrderDetail.class, sql.toString(), params, kPaging);
		}
	}
	
	@Override
	public List<SaleOrderDetailVOEx> getListSaleOrderDetailVOEx(KPaging<SaleOrderDetailVOEx> kPaging, 
			Long saleOrderId, Integer isFreeItem, ProgramType programType,
			Boolean isAutoPromotion,Long customerTypeId,Long shopId,Date lockDay) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder withSql = new StringBuilder();
		if(saleOrderId != null){
			sql.append(" with lstSOPromo as ( ");
			sql.append(" 	    SELECT product_group_id, promotion_program_code,promotion_level ");
			sql.append(" 	    FROM sale_order_promotion  WHERE sale_order_id = ? ");
			params.add(saleOrderId);
			sql.append(" ), ");
			sql.append(" lstGL as ( ");
			sql.append(" 	select group_level_id,parent_group_level_id,order_number from group_level where status=1 ");
			sql.append(" ), ");
			sql.append(" lstGLD as ( ");
			sql.append("    select group_level_id from group_level_detail where status=1 and is_required=1 and group_level_id in (select group_level_id from lstGL) ");
			sql.append(" ) ");
		}
		/*
		 * using old price from sale_order_detail to load as the default price of sale order
		 * then inform user that the price is invalid , and reload when user payment again
		 * @modified by NhanLT6
		 * @date 24/20/2014
		 */
		sql.append(" SELECT sod.sale_order_detail_id as id, sol.sale_order_lot_id saleOrderLotId, prd.product_id as productId,");
		sql.append("         prd.product_code as productCode, prd.product_name as productName,  ");
		sql.append(" sod.price as price, ");
		sql.append(" sod.price_not_vat as priceNotVat, ");
		sql.append(" sod.PACKAGE_PRICE as pkPrice, ");
		sql.append(" sod.PACKAGE_PRICE_NOT_VAT as pkPriceNotVat, ");
		
		
		sql.append(" stt.quantity as stockQuantity, sol.warehouse_id warehouseId, w.warehouse_name warehouseName, ");
		sql.append(" stt.AVAILABLE_QUANTITY ");
		/*
		 * add sale order id to load subtracted quantity for sale product
		 * @modified by tuannd20
		 * @date 02/10/2014
		 */
		if (saleOrderId != null) {
			sql.append(" + (select sum(nvl(sol.quantity, 0)) ");
			sql.append(" 	from sale_order_lot sol ");
			sql.append(" 	where sol.sale_order_id = ? ");
			params.add(saleOrderId);
			sql.append(" 	and sol.warehouse_id = w.warehouse_id ");
			sql.append(" 	and sol.product_id = sod.product_id ");
			sql.append(" ) ");
		}
		sql.append(" as availableQuantity, ");		
		sql.append(" prd.convfact, ");
		sql.append(" (case when sol.quantity is null then sod.quantity else sol.quantity end) as quantity,");
		sql.append(" (case when sol.quantity_package is null then sod.quantity_package else sol.quantity_package end) as quantityPackage,");
		sql.append(" (case when sol.quantity_retail is null then sod.quantity_retail else sol.quantity_retail end) as quantityRetail,");
		sql.append(" (sod.package_price*sol.quantity_package + sod.price*sol.quantity_retail) as amount, ");
		sql.append(" sod.program_code as programCode, sol.discount_percent discountPercent, sol.discount_amount discountAmount, prd.uom1 as uom1, ");
		sql.append(" sod.join_program_code as joinProgramCode, ");
//		sql.append(" case sod.program_type ");
//		sql.append(" when ? then 'DISPLAY_SCORE' ");
//		params.add(ProgramType.DISPLAY_SCORE.getValue());
//		sql.append(" else pg.type ");
//		sql.append(" end ");
		sql.append(" sod.programe_type_code as promotionType, ");
		
		sql.append(" prd.gross_weight as grossWeight, prd.check_lot as checkLot, ");
		sql.append(" sod.MAX_QUANTITY_FREE maxQuantityFree, 0 maxQuantityFreeCk, 0 myTime, ");//sod.MAX_QUANTITY_FREE_CK, sod.my_time
		sql.append(" sod.is_free_item as isFreeItem, pg.is_edited as isEdited,");		
		sql.append(" sod.promotion_order_number levelPromo,  ");
		sql.append(" sod.PRODUCT_GROUP_ID productGroupId,  ");
		sql.append(" sod.GROUP_LEVEL_ID groupLevelId,  ");
		if (saleOrderId != null) {
			sql.append(" case  ");
			sql.append(" when exists ( ");
			sql.append(" 		select 1 ");
			sql.append(" 		from group_level gl ");
			sql.append(" 		where status = 1 ");
			sql.append(" 		and product_group_id = (	select product_group_id ");
			sql.append(" 									from lstSOPromo sop ");
			sql.append(" 									where 1=1 ");
			sql.append(" 									and sop.promotion_program_code = sod.program_code ");
			sql.append(" 									and sop.promotion_level = sod.promotion_order_number ");
			sql.append(" 									and sop.product_group_id = sod.product_group_id"); // lacnv1 - check them dieu kien cung nhom
			sql.append(" 									and rownum = 1 ");
			sql.append(" 								) ");
			sql.append(" 		and gl.order_number = sod.promotion_order_number ");
			sql.append(" 		and gl.parent_group_level_id is null"); // lacnv1 - check them dieu kien la group_level cha
			sql.append(" 		/*OR type*/ ");
			sql.append(" 		and not exists ( ");
			sql.append(" 				select 1 ");
			sql.append(" 				from lstGLD gld ");
			sql.append(" 				where gld.group_level_id in (select group_level_id from lstGL where parent_group_level_id = gl.group_level_id) ");
			sql.append(" 			) ");
			// TODO Begin vuongmq; 18/01/2015; lay link doi SPKM
			sql.append("	 and (select count(1) from group_level_detail gld ");
			sql.append(" 		where gld.is_required = 0 ");
			sql.append(" 		and gld.group_level_id IN ");
			sql.append(" 		(SELECT group_level_id ");
			sql.append(" 			FROM lstGL ");
			sql.append("			 where parent_group_level_id = gl.group_level_id ");
			sql.append(" 			)) > 1 ");
			//TODO End vuongmq; 18/01/2015; lay link doi SPKM
			sql.append(" 	) then 1 ");
			sql.append(" else 0 ");
			sql.append(" end as changeProduct ");
		} else {
			sql.append(" 0 changeProduct ");
		}
		
		sql.append(", (case when sod.programe_type_code = 'ZV24' then 1 else 0 end) as openPromo");
		sql.append(", (case when sod.programe_type_code = 'ZV23' then 1 else 0 end) as accumulation");
		sql.append(", (case when exists (");
		sql.append(" select 1 from group_level where group_level_id in (");
		sql.append("select sale_group_level_id from group_mapping where promo_group_level_id = sod.group_level_id and promo_group_id = sod.product_group_id");
		sql.append(") and has_product = 0");
		sql.append(") then 2 else null");
		sql.append(" end) as isOwner"); // lacnv1 - them truong ao nay de xac dinh no la khuyen mai mo moi hay khuyen mai mo moi don hang
		
		/*
		 * Xu ly cho truong hop xem chi tiet don hang tablet, lay danh sach san pham khuyen mai.
		 * Vi don tablet chua co du lieu trong [SALE_ORDER_LOT] nen join bang tu [SALE_ORDER_LOT] se khong co du lieu
		 * => doi lai, bat dau join bang tu [SALE_ORDER_DETAIL] de lay danh sach san pham khuyen mai cua don hang TABLET
		 * 
		 * @modified by tuannd20
		 * @date 06/10/2014
		 */
		sql.append(" FROM sale_order_detail sod ");
		sql.append(" left join sale_order_lot sol ON sol.sale_order_detail_id = sod.sale_order_detail_id ");
		sql.append(" left JOIN warehouse w ON w.warehouse_id = sol.warehouse_id ");
		sql.append(" left join product prd on sod.product_id=prd.product_id ");
		sql.append(" LEFT JOIN stock_total stt ON stt.object_id=sod.shop_id ");
		sql.append(" AND stt.object_type=1 and stt.status=1");
		sql.append(" AND stt.product_id=sod.product_id ");
		sql.append(" AND stt.warehouse_id = w.warehouse_id ");
		sql.append(" LEFT JOIN promotion_program pg ON pg.promotion_program_code = sod.program_code ");
		sql.append(" WHERE 1=1");
		
		StringBuilder countSql = new StringBuilder();
		countSql.append(withSql.toString());
		countSql.append(" select count(1) as count from sale_order_lot sol join sale_order_detail sod on sol.sale_order_detail_id = sod.sale_order_detail_id where 1=1");
		
		/*sql.append(" and nvl(sod.programe_type_code, '-1') not in (?)");
		params.add(PromotionType.ZV23.getValue());*/
		
		if (saleOrderId != null) {
			sql.append(" 	and sod.sale_order_id = ?");
			countSql.append(" 	and sod.sale_order_id = ?");
			params.add(saleOrderId);
		}
		if (isFreeItem != null) {
			sql.append(" and sod.is_free_item = ? ");
			countSql.append(" and sod.is_free_item = ? ");
			params.add(isFreeItem);
		}
		
		if (programType != null) {
			sql.append(" and program_type =  ?");
			countSql.append(" and sod.program_type = ?");
			params.add(programType.getValue());
		}
		
		if (Boolean.TRUE.equals(isAutoPromotion)) {
			sql.append(" and program_type in (?,?) and is_free_item = 1 ");
			countSql.append(" and sod.program_type in (?,?) ");
			params.add(ProgramType.AUTO_PROM.getValue());
			params.add(ProgramType.KEY_SHOP.getValue());//them keyshop
		} else if (Boolean.FALSE.equals(isAutoPromotion)) {
			sql.append(" and (sod.program_type not in (?,?) OR sod.is_free_item = 0)");
			countSql.append(" and (sod.program_type not in (?,?) OR sod.is_free_item = 0)");
			params.add(ProgramType.AUTO_PROM.getValue());
			params.add(ProgramType.KEY_SHOP.getValue());
		}
		
		sql.append(" order by productId desc, w.seq ");
//		sql.append(" order by myTime asc, productId desc, w.seq ");
		
		//bo  "myTime",
		String[] fieldNames = {
			"id", "saleOrderLotId", "productId", "productCode", "productName", 
			"price", "stockQuantity", "warehouseId","warehouseName", "convfact", 
			"quantity", "quantityPackage", "quantityRetail", "availableQuantity", "amount", "programCode", "discountPercent", 
			"discountAmount", "promotionType", "grossWeight", "checkLot", "uom1", 
			"joinProgramCode", "priceNotVat", "maxQuantityFree", "maxQuantityFreeCk",
			"isFreeItem", "isEdited", "levelPromo", "changeProduct",
			"productGroupId", "groupLevelId",
			"openPromo", "accumulation", "isOwner", "pkPrice", "pkPriceNotVat"
		};
		
		//bo myTime StandardBasicTypes.BIG_DECIMAL,
		Type[] fieldTypes = {
			StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
			StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER,StandardBasicTypes.LONG,StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, 
			StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.FLOAT, 
			StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.FLOAT, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,  
			StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
			StandardBasicTypes.LONG, StandardBasicTypes.LONG,
			StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
			StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL 
		};
		
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(SaleOrderDetailVOEx.class, fieldNames, fieldTypes, sql.toString().replaceAll("  ", " "), params);
		}
		return repo.getListByQueryAndScalarPaginated(SaleOrderDetailVOEx.class, 
					fieldNames, fieldTypes, sql.toString().replaceAll("  ", " "), countSql.toString().replaceAll("  ", " "), params, params, kPaging);
	}
	
	
	@Override
	public List<SaleOrderDetailVOEx> getListSaleOrderDetailVOExForTablet(KPaging<SaleOrderDetailVOEx> kPaging, 
			Long saleOrderId, Integer isFreeItem, ProgramType programType,
			Boolean isAutoPromotion,Long customerTypeId,Long shopId,Date lockDay) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT sod.sale_order_detail_id as id, prd.product_id as productId,");
		sql.append("         prd.product_code as productCode, prd.product_name as productName,  ");
		sql.append("         (select price from price pr where pr.product_id = sod.product_id and pr.status = ? ");
		sql.append("           and ( pr.customer_type_id = ?  or pr.shop_id = ? )  and pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?))) as price, ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(customerTypeId);
		params.add(shopId);
		params.add(lockDay);
		params.add(lockDay);
		sql.append("         (select PRICE_NOT_VAT from price pr where pr.product_id = sod.product_id and ( pr.customer_type_id = ?  or pr.shop_id = ? ) and pr.status = ? and pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?))) as priceNotVat, ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(customerTypeId);
		params.add(shopId);
		params.add(lockDay);
		params.add(lockDay);
		sql.append("         prd.convfact, sod.quantity as quantity, (sod.price*sod.quantity) as amount, ");
		sql.append("         sod.program_code as programCode, sod.program_type as programType, sod.discount_percent discountPercent, sod.discount_amount discountAmount, prd.uom1 as uom1, pg.type as promotionType, prd.gross_weight as grossWeight, prd.check_lot as checkLot, ");
		sql.append("		 sod.MAX_QUANTITY_FREE maxQuantityFree, sod.MAX_QUANTITY_FREE_CK maxQuantityFreeCk, sod.my_time myTime, ");
		sql.append("		 sod.is_free_item as isFreeItem");
		sql.append(" FROM sale_order_detail sod " );
		sql.append(	"left join product prd on sod.product_id=prd.product_id ");
		sql.append(" left join promotion_program pg on pg.promotion_program_code = sod.program_code");
		sql.append(" WHERE 1=1");
		
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from sale_order_detail sod where 1=1");
		
		if (saleOrderId != null) {
			sql.append(" 	and sod.sale_order_id = ?");
			countSql.append(" 	and sod.sale_order_id = ?");
			params.add(saleOrderId);
		}
		if (isFreeItem != null) {
			if (isFreeItem == 1) {
				sql.append(" and sod.is_free_item = 1 ");
			}
			else if (isFreeItem == 0) {
				sql.append(" and sod.is_free_item = ? or ");
				params.add(isFreeItem);
			}
		}
		
		if (programType != null) {
			sql.append(" and program_type =  ?");
			countSql.append(" and sod.program_type = ?");
			params.add(programType.getValue());
		}
		
		if (Boolean.TRUE.equals(isAutoPromotion)) {
			sql.append(" and program_type = ? and is_free_item = 1 ");
			countSql.append(" and sod.program_type = ?");
			params.add(ProgramType.AUTO_PROM.getValue());
		}
		else if (Boolean.FALSE.equals(isAutoPromotion)) {
			sql.append(" and (sod.program_type != ? OR sod.is_free_item = 0)");
			countSql.append(" and (sod.program_type != ? OR sod.is_free_item = 0)");
			params.add(ProgramType.AUTO_PROM.getValue());
//			sql.append(" and is_free_item = 0 ");
		}
		
		sql.append(" order by myTime asc, productId desc");
		
		String[] fieldNames = {"id", "productId", "productCode", 
				"productName", "price", 
				"convfact", "quantity","amount", 
				"programCode", "programType", "discountPercent", "discountAmount", 
				"promotionType", "grossWeight", "checkLot", "uom1", "priceNotVat", "maxQuantityFree", "maxQuantityFreeCk", "myTime",
				"isFreeItem"};
		
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING,
				 StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, 
				 StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,StandardBasicTypes.BIG_DECIMAL,
				 StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.FLOAT, StandardBasicTypes.BIG_DECIMAL,
				 StandardBasicTypes.STRING, StandardBasicTypes.FLOAT, StandardBasicTypes.INTEGER, 
				 StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL,
				 StandardBasicTypes.INTEGER};
		
		if (kPaging == null)
			return repo.getListByQueryAndScalar(SaleOrderDetailVOEx.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(SaleOrderDetailVOEx.class, 
					fieldNames, fieldTypes, 
					sql.toString(), countSql.toString(), 
					params, params, kPaging);
	}
	
	
	
	@Override
	public List<SaleOrderDetailGroupVO> getListSaleOrderDetailGroupVO(KPaging<SaleOrderDetailGroupVO> kPaging, 
			Long saleOrderId, Integer isFreeItem) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT prd.product_id as productId,");
		sql.append("         prd.product_code as productCode, prd.product_name as productName,  ");
		sql.append("         sod.price as price, stt.available_quantity as stockQuantity,  ");
		sql.append("         prd.convfact, sum(sod.quantity) as quantity, sum(sod.amount) as amount ");
		sql.append(" FROM sale_order_detail sod join product prd on sod.product_id=prd.product_id ");
		sql.append("       join stock_total stt on stt.object_id=sod.shop_id  ");
		sql.append("         and stt.object_type=1 and stt.product_id=sod.product_id ");
		sql.append(" WHERE 1=1");
		
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count from sale_order_detail where 1=1");
		
		if (saleOrderId != null) {
			sql.append(" 	and sale_order_id = ?");
			countSql.append(" 	and sale_order_id = ?");
			params.add(saleOrderId);
		}
		if (isFreeItem != null) {
			sql.append(" 	and is_free_item = ?");
			countSql.append(" 	and is_free_item = ?");
			params.add(isFreeItem);
		}
		sql.append(" GROUP BY prd.product_id, prd.product_code, prd.product_name, sod.price, stt.available_quantity, prd.convfact");
		countSql.append(" GROUP BY product_id");
		
		String[] fieldNames = {"productId", "productCode", 
				"productName", "price", "stockQuantity",
				"convfact", "quantity", "amount"
			};
		
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.STRING,
				 StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER,
				 StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL
				 };
		
		if (kPaging == null)
			return repo.getListByQueryAndScalar(SaleOrderDetailGroupVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(SaleOrderDetailGroupVO.class, 
					fieldNames, fieldTypes, 
					sql.toString(), countSql.toString(), 
					params, params, kPaging);
	}

	@Override
	public List<TaxInvoiceDetailVO> getListTaxInvoiceDetailVO(Long saleOrderId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select pro.product_code as productCode, ");
		sql.append(" pro.product_name as productName, ");
		sql.append(" pro.UOM1 as unit, ");
		sql.append(" nvl(sod.quantity, 0) as quantity, ");
		sql.append(" nvl(pri.price_not_vat, 0) as price, ");
		sql.append(" nvl((pri.price_not_vat * sod.quantity), 0) as amount ");
		sql.append(" from sale_order_detail sod ");
		sql.append(" left join product pro on sod.product_id = pro.product_id ");
		sql.append(" left join price pri on sod.price_id = pri.price_id ");
		sql.append(" where sod.is_free_item = 0 ");
		sql.append(" and sod.sale_order_id = ? ");

		params.add(saleOrderId);

		String[] fieldNames = { "productCode", // 1
				"productName", // 2
				"unit",// 3
				"quantity",// 4
				"price",// 5
				"amount"// 6
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.INTEGER,// 4
				StandardBasicTypes.BIG_DECIMAL,// 5
				StandardBasicTypes.BIG_DECIMAL // 6
		};

		return repo.getListByQueryAndScalar(TaxInvoiceDetailVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RptOrderDetailProduct4VO> getListRptOrderDetailProduct4VO(Long shopId, String staffCode,
			Date fromDate, Date toDate) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptOrderDetailProduct4VO>();
		
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		if (toDate == null) {
			throw new IllegalArgumentException("toDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT p.product_code AS productCode,");
		sql.append("   p.product_name      AS productName,");
		sql.append("   sod.price           AS price,");
		sql.append("   so.order_date       AS orderDate,");
		sql.append("   s.staff_code        AS staffCode,");
		sql.append("   s.staff_name        AS staffName,");
		sql.append("   c.customer_code     AS customerCode,");
		sql.append("   c.customer_name     AS customername,");
		sql.append("   c.address           AS address,");
		sql.append("   so.order_number     AS orderNumber,");
		sql.append("   inv.invoice_number  AS invoiceNumber,");
		sql.append("   sod.quantity        AS quantity,");
		sql.append("   sod.amount          AS amount");
		sql.append(" FROM sale_order_detail sod,");
		sql.append("   sale_order so,");
		sql.append("   invoice inv,");
		sql.append("   product p,");
		sql.append("   staff s,");
		sql.append("   customer c");
		sql.append(" WHERE 1                   =1");
		sql.append(" AND sod.sale_order_id     = so.sale_order_id");
		sql.append(" AND ((so.order_type       = 'IN'");
		sql.append(" AND so.approved           = 1");
		sql.append(" AND so.type               = 1)");
		sql.append(" OR so.order_type          = 'CM')");
		sql.append(" AND so.invoice_id         = inv.invoice_id");
		sql.append(" AND sod.product_id        = p.product_id");
		sql.append(" AND sod.is_free_item      = 0");
		sql.append(" AND so.staff_id           = s.staff_id");
		sql.append(" AND so.customer_id        = c.customer_id");
		sql.append(" AND so.order_date >= TRUNC(?)");
		params.add(fromDate);
		sql.append(" AND so.order_date < TRUNC(?) + 1");
		params.add(toDate);
		sql.append(" AND so.shop_id            = ?");
		params.add(shopId);
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			staffCode = staffCode.toUpperCase();
			sql.append(" AND UPPER(s.staff_code)           = ?");
			params.add(staffCode);
		}
		sql.append(" ORDER BY p.product_code,");
		sql.append("   sod.price,");
		sql.append("   so.order_date,");
		sql.append("   s.staff_code,");
		sql.append("   c.customer_code");

		String[] fieldNames = { "productCode",// 1
				"productName",// 2
				"price",// 3
				"orderDate",// 4
				"staffCode",// 5
				"staffName",// 6
				"customerCode",// 7
				"customername",// 8
				"address",// 9
				"orderNumber",// 10
				"invoiceNumber",// 11
				"quantity",// 12
				"amount"// 13
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.BIG_DECIMAL,// 3
				StandardBasicTypes.TIMESTAMP,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING, // 7
				StandardBasicTypes.STRING, // 8
				StandardBasicTypes.STRING, // 9
				StandardBasicTypes.STRING, // 10
				StandardBasicTypes.STRING, // 11
				StandardBasicTypes.INTEGER, // 12
				StandardBasicTypes.BIG_DECIMAL // 13
		};
		return repo.getListByQueryAndScalar(RptOrderDetailProduct4VO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RptCustomerProductSaleOrder1VO> getListRptCustomerProductSaleOrderVO(
			Long shopId, Long staffId, Date fromDate, Date toDate) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptCustomerProductSaleOrder1VO>();
		
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		if (toDate == null) {
			throw new IllegalArgumentException("toDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT tmp1.short_code   AS shortCode,");
		sql.append("   tmp1.customer_name     AS customerName,");
		sql.append("   tmp1.product_info_code AS catCode,");
		sql.append("   tmp1.product_code      AS productCode,");
		sql.append("   tmp1.product_name      AS productName,");
		sql.append("   NVL(tmp1.quantity, 0)  AS orderQty,");
		sql.append("   NVL(tmp1.amount, 0)    AS amount,");
		sql.append("   NVL(tmp2.quantity, 0)  AS returnQty");
		sql.append(" FROM");
		sql.append("   (SELECT c.short_code   AS short_code,");
		sql.append("     c.customer_name      AS customer_name,");
		sql.append("     pi.product_info_code AS product_info_code,");
		sql.append("     p.product_code       AS product_code,");
		sql.append("     p.product_name       AS product_name,");
		sql.append("     SUM(sod.quantity)    AS quantity,");
		sql.append("     SUM(sod.amount)      AS amount");
		sql.append("   FROM sale_order_detail sod,");
		sql.append("     sale_order so,");
		sql.append("    product p,");
		sql.append("     product_level pl,");
		sql.append("     product_info pi,");
		sql.append("     customer c");
		sql.append("   WHERE 1                   =1");
		sql.append("   AND sod.sale_order_id     = so.sale_order_id");
		sql.append("   AND so.order_type         = 'IN'");
		sql.append("   AND so.approved           = 1");
		sql.append("   AND sod.product_id        = p.product_id");
		sql.append("   AND sod.is_free_item      = 0");
		sql.append("   AND p.product_level_id    = pl.product_level_id");
		sql.append("   AND pl.cat_id             = pi.product_info_id");
		sql.append("   AND so.customer_id        = c.customer_id");
		sql.append("   AND so.order_date >= TRUNC(?)");
		params.add(fromDate);
		sql.append("   AND so.order_date < TRUNC(?) + 1");
		params.add(toDate);
		sql.append("   AND so.shop_id            = ?");
		params.add(shopId);
		if (staffId != null) {
			sql.append("   AND so.staff_id           = ?");
			params.add(staffId);
		}
		sql.append("   GROUP BY c.short_code,");
		sql.append("     c.customer_name,");
		sql.append("     pi.product_info_code,");
		sql.append("     p.product_code,");
		sql.append("     p.product_name");
		sql.append("   ORDER BY c.short_code,");
		sql.append("     pi.product_info_code,");
		sql.append("     p.product_code");
		sql.append("   ) tmp1");
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT c.short_code   AS short_code,");
		sql.append("     c.customer_name      AS customer_name,");
		sql.append("     pi.product_info_code AS product_info_code,");
		sql.append("     p.product_code       AS product_code,");
		sql.append("     p.product_name       AS product_name,");
		sql.append("     SUM(sod.quantity)    AS quantity,");
		sql.append("     SUM(sod.amount)      AS amount");
		sql.append("   FROM sale_order_detail sod,");
		sql.append("     sale_order so,");
		sql.append("     product p,");
		sql.append("     product_level pl,");
		sql.append("     product_info pi,");
		sql.append("     customer c");
		sql.append("   WHERE 1                   =1");
		sql.append("   AND sod.sale_order_id     = so.sale_order_id");
		sql.append("   AND so.order_type         = 'IN'");
		sql.append("   AND so.approved           = 1");
		sql.append("   AND sod.product_id        = p.product_id");
		sql.append("   AND sod.is_free_item      = 1");
		sql.append("   AND sod.program_type      = 2");
		sql.append("   AND p.product_level_id    = pl.product_level_id");
		sql.append("   AND pl.cat_id             = pi.product_info_id");
		sql.append("   AND so.customer_id        = c.customer_id");
		sql.append("   AND so.order_date >= TRUNC(?)");
		params.add(fromDate);
		sql.append("   AND so.order_date < TRUNC(?) + 1");
		params.add(toDate);
		sql.append("   AND so.shop_id            = ?");
		params.add(shopId);
		if (staffId != null) {
			sql.append("   AND so.staff_id           = ?");
			params.add(staffId);
		}
		sql.append("   GROUP BY c.short_code,");
		sql.append("     c.customer_name,");
		sql.append("     pi.product_info_code,");
		sql.append("     p.product_code,");
		sql.append("     p.product_name");
		sql.append("   ORDER BY c.short_code,");
		sql.append("     pi.product_info_code,");
		sql.append("     p.product_code");
		sql.append("   ) tmp2");
		sql.append(" ON (((tmp1.short_code     IS NULL");
		sql.append(" AND tmp2.short_code       IS NULL)");
		sql.append(" OR (tmp1.short_code        = tmp2.short_code))");
		sql.append(" AND tmp1.product_info_code = tmp2.product_info_code");
		sql.append(" AND tmp1.product_code      = tmp2.product_code)");

		String[] fieldNames = { "shortCode", // 1
				"customerName", // 2
				"catCode",// 3
				"productCode",// 4
				"productName",// 5
				"orderQty",// 6
				"amount",// 7
				"returnQty"// 8
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.INTEGER,// 6
				StandardBasicTypes.BIG_DECIMAL, // 7
				StandardBasicTypes.INTEGER // 8
		};
		return repo.getListByQueryAndScalar(RptCustomerProductSaleOrder1VO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<RptPayDetailDisplayProgr1VO> getListRptPayDetailDisplayProgrVO(
			Long shopId, String ppCode, Date fromDate, Date toDate) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptPayDetailDisplayProgr1VO>();
		
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (ppCode == null) {
			throw new IllegalArgumentException("ppCode is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT so.order_number    AS orderNumber,");
		sql.append("   c.short_code            AS shortCode,");
		sql.append("   c.customer_name         AS customerName,");
		sql.append("   p.product_code          AS productCode,");
		sql.append("   p.product_name          AS productName,");
		sql.append("   sod.quantity            AS quantity,");
		sql.append("   sod.price               AS price,");
		sql.append("   sod.amount              AS amount");
		sql.append(" FROM sale_order_detail sod,");
		sql.append("   sale_order so,");
		sql.append("   product p,");
		sql.append("   customer c");
		sql.append(" WHERE 1                   =1");
		sql.append(" AND sod.sale_order_id     = so.sale_order_id");
		sql.append(" AND so.order_type         = 'IN'");
		sql.append(" AND so.approved           = 1");
		sql.append(" AND sod.product_id        = p.product_id");
		sql.append(" AND sod.is_free_item      = 1");
		sql.append(" AND sod.program_type      = 2");
		sql.append(" AND so.customer_id        = c.customer_id");
		if (fromDate != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		sql.append(" AND UPPER(sod.program_code) = ?");
		params.add(ppCode.toUpperCase());
		sql.append(" AND so.shop_id            = ?");
		params.add(shopId);
		sql.append(" ORDER BY so.order_number,");
		sql.append("   c.customer_code,");
		sql.append("   p.product_code");
		String[] fieldNames = { "orderNumber", // 1
				"shortCode", // 2
				"customerName",// 3
				"productCode",// 4
				"productName",// 5
				"quantity",// 6
				"price",// 7
				"amount"// 8
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.INTEGER,// 6
				StandardBasicTypes.BIG_DECIMAL, // 7
				StandardBasicTypes.BIG_DECIMAL // 8
		};
		return repo.getListByQueryAndScalar(RptPayDetailDisplayProgr1VO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<RptPromotionDetailStaff2VO> getListRptPromotionDetailStaffV2O(
			Long shopId, String staffCode, Date fromDate, Date toDate, Boolean programe_staff) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptPromotionDetailStaff2VO>();
		
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		if (toDate == null) {
			throw new IllegalArgumentException("toDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT pp.promotion_program_code AS promotionProgramCode,");
		sql.append("   pp.promotion_program_name      AS promotionProgramName,");
		sql.append("   sod.staff_code	              AS staffCode,");
		sql.append("   sod.staff_name                 AS staffName,");
		sql.append("   sod.quantity                   AS quantity,");
		sql.append("   sod.amount                     AS amount");
		sql.append(" FROM");
		sql.append("   (SELECT pp.promotion_program_code AS promotion_program_code,");
		sql.append("     pp.promotion_program_name       AS promotion_program_name");
		sql.append("   FROM promotion_program pp");
		sql.append("   WHERE 1                  = 1");
		sql.append("   AND ((pp.from_date      IS NULL");
		sql.append("   AND pp.to_date          IS NULL)");
		sql.append("   OR (pp.to_date          IS NOT NULL");
		sql.append("   AND pp.to_date   >= TRUNC(?))");
		params.add(fromDate);
		sql.append("   OR (pp.from_date        IS NOT NULL");
		sql.append("   AND pp.from_date < TRUNC(?) + 1)");
		params.add(toDate);
		sql.append("   OR(pp.from_date  < TRUNC(?) + 1");
		params.add(fromDate);
		sql.append("   AND pp.to_date   >= TRUNC(?)))");
		params.add(toDate);
		sql.append("   AND pp.status            = 1");
		sql.append("   ) pp");
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT sod.program_code AS program_code,");
		sql.append("     s.staff_code           AS staff_code,");
		sql.append("     s.staff_name           AS staff_name,");
		sql.append("     SUM(sod.quantity)      AS quantity,");
		sql.append("     SUM(sod.amount)        AS amount");
		sql.append("   FROM sale_order_detail sod,");
		sql.append("     sale_order so,");
		sql.append("     staff s");
		sql.append("   WHERE 1                   =1");
		sql.append("   AND sod.sale_order_id     = so.sale_order_id");
		sql.append("   AND so.order_type         = 'IN'");
		sql.append("   AND so.approved           = 1");
		sql.append("   AND sod.is_free_item      = 1");
		sql.append("   AND sod.program_type     IN (0, 1)");
		sql.append("   AND so.staff_id           = s.staff_id");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append("   AND UPPER(s.staff_code)    = ?");
			params.add(staffCode.toUpperCase());
		}
		sql.append("   AND so.shop_id            = ?");
		params.add(shopId);
		sql.append("   GROUP BY sod.program_code,");
		sql.append("     s.staff_code,");
		sql.append("     s.staff_name");
		sql.append("   ) sod");
		sql.append(" ON (pp.promotion_program_code = sod.program_code)");
		if (programe_staff == true) {
			sql.append(" ORDER BY pp.promotion_program_code, sod.staff_code");
		} else {
			sql.append(" ORDER BY sod.staff_code, pp.promotion_program_code");
		}
		
		String[] fieldNames = { "promotionProgramCode", // 1
				"promotionProgramName", // 2
				"staffCode",// 3
				"staffName",// 4
				"quantity",// 5
				"amount"// 6
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.BIG_DECIMAL// 6
		};
		return repo.getListByQueryAndScalar(RptPromotionDetailStaff2VO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<RptExSaleOrderVO> getListExSaleOrderMoneyPromoVO(Long shopId, Date fromDate, Date toDate,
			Long deliveryStaffId, String lstOrderNumber) throws DataAccessException {
		List<String> __lstSO = new ArrayList<String>();
		if(!StringUtility.isNullOrEmpty(lstOrderNumber)) {
			String[] _lstOrderNumber = lstOrderNumber.split(",");
			for(int i = 0; i < _lstOrderNumber.length; i++) {
				__lstSO.add(_lstOrderNumber[i].trim());
			}
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select null productCode, null productName, null convfact, null checkLot, null lot, null price, null promoQuantity, null saleQuantity, null deliveryStaffInfo, null deliveryStaffId, nvl(sum(decode(type,0,sod.discount_amount,1,sod.discount_amount,2,-sod.discount_amount)), 0) discountAmount, null orderNumber ");
		sql.append("from sale_order_detail sod inner join sale_order so on sod.sale_order_id = so.sale_order_id ");
		sql.append("where sod.is_free_item = 1 and sod.discount_amount > 0 ");
		if(null != __lstSO && __lstSO.size() > 0) {
			sql.append("and so.order_number IN (?");
			params.add(__lstSO.get(0));
			for(int i = 1; i < __lstSO.size(); i++) {
				sql.append(", ?");
				params.add(__lstSO.get(i));
			}
			sql.append(")");
		}
		if(null != shopId) {
			sql.append("and so.shop_id = ? ");
			params.add(shopId);
		}
		if(null != fromDate) {
			sql.append("and TRUNC(so.order_date) >= trunc (?) ");
			params.add(fromDate);
		}
		if(null != toDate) {
			sql.append("and TRUNC(so.order_date) < trunc (?) + 1 ");
			params.add(toDate);
		}
		if(null != deliveryStaffId) {
			sql.append("and so.delivery_id = ? ");
			params.add(deliveryStaffId);
		}
		
		String[] fieldNames = { "productCode", 
				"productName", 
				"convfact", 
				"checkLot",
				"lot", 
				"price", 
				"promoQuantity", 
				"saleQuantity", "deliveryStaffInfo", "deliveryStaffId", "discountAmount", "orderNumber"};
		
		Type[] fieldTypes = { 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, 
				StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, 
				StandardBasicTypes.STRING, 
				StandardBasicTypes.LONG, 
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING};
		
		return repo.getListByQueryAndScalar(RptExSaleOrderVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<RptExSaleOrderVO> getListExSaleOrderVO(Long shopId, Date fromDate, Date toDate,
			Long deliveryStaffId) throws DataAccessException {
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptExSaleOrderVO>();
			
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select productCode, productName, convfact, checkLot, lot, price, sum(promoQuantity) promoQuantity, sum(saleQuantity) saleQuantity, deliveryStaffInfo, deliveryStaffId,sum(discountAmount) discountAmount, listagg(orderNumber, ', ') within group (order by orderNumber) orderNumber ");
		sql.append(" from (SELECT pr.product_code productCode,");
		sql.append("   pr.product_name productName,");
		sql.append("   pr.convfact,");
		sql.append("   pr.check_lot checkLot,");
		sql.append("   lot,");
		sql.append("   price,");
		sql.append("   promoQuantity,");
		sql.append("   saleQuantity,");
		sql.append("   (SELECT staff_name || ' - ' || staff_code");
		sql.append("   FROM staff");
		sql.append("   WHERE staff_id = temp.delivery_id");
		sql.append("   ) deliveryStaffInfo,");
		sql.append("   temp.delivery_id deliveryStaffId,");
		sql.append("   nvl(temp1.discountAmount, 0) discountAmount, TEMP.ORDER_NUMBER orderNumber ");
		sql.append(" FROM (");
		sql.append("   (SELECT order_number, delivery_id,");
		sql.append("     product_id,");
		sql.append("     lot,");
		sql.append("     price,");
		sql.append("     SUM(CASE WHEN freeQuanL IS NOT NULL THEN freeQuanL ELSE freeQuanD END) promoQuantity,");
		sql.append("     SUM(CASE WHEN saleQuanL IS NOT NULL THEN saleQuanL ELSE saleQuanD END) saleQuantity");
		sql.append("   FROM (");
		sql.append("     (SELECT so.order_number, sod.product_id,");
		sql.append("       sol.lot,");
		sql.append("       sod.price,");
		sql.append("       so.delivery_id,");
		sql.append("       sod.quantity saleQuanD,");
		sql.append("       sol.quantity saleQuanL,");
		sql.append("       0 freeQuanD,");
		sql.append("       0 freeQuanL");
		sql.append("     FROM sale_order so");
		sql.append("     JOIN sale_order_detail sod");
		sql.append("     ON so.sale_order_id = sod.sale_order_id");
		sql.append("     LEFT JOIN sale_order_lot sol");
		sql.append("     ON sol.sale_order_detail_id = sod.sale_order_detail_id");
		sql.append("     WHERE approved              = 1");
		sql.append("     AND type                    = 1");
		sql.append("     AND is_free_item            = 0");
		sql.append("     AND order_type             IN ('IN','TT')");
		if (shopId != null) {
			sql.append(" and so.shop_id = ? ");
			params.add(shopId);
		}
		if (deliveryStaffId != null) {
			sql.append(" and so.delivery_id = ? ");
			params.add(deliveryStaffId);
		}
		if (fromDate != null) {
			sql.append(" and trunc(so.order_date) >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and trunc(so.order_date) < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append("     )");
		sql.append("   UNION");
		sql.append("     (SELECT so.order_number, sod.product_id,");
		sql.append("       sol.lot,");
		sql.append("       sod.price,");
		sql.append("       so.delivery_id,");
		sql.append("       0 saleQuanD,");
		sql.append("       0 saleQuanL,");
		sql.append("       sod.quantity freeQuanD,");
		sql.append("       sol.quantity freeQuanL");
		sql.append("     FROM sale_order so");
		sql.append("     JOIN sale_order_detail sod");
		sql.append("     ON so.sale_order_id = sod.sale_order_id");
		sql.append("     LEFT JOIN sale_order_lot sol");
		sql.append("     ON sol.sale_order_detail_id = sod.sale_order_detail_id");
		sql.append("     WHERE approved              = 1");
		sql.append("     AND type                    = 1");
		sql.append("     AND is_free_item            = 1");
		sql.append("     AND order_type             IN ('IN', 'TT')");
		sql.append("     AND program_type NOT       IN (3,4,5)");
		sql.append("     AND sod.quantity            > 0");
		if (shopId != null) {
			sql.append(" and so.shop_id = ? ");
			params.add(shopId);
		}
		if (deliveryStaffId != null) {
			sql.append(" and so.delivery_id = ? ");
			params.add(deliveryStaffId);
		}
		if (fromDate != null) {
			sql.append(" and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append("     ) )");
		sql.append("   WHERE price IS NOT NULL and delivery_id is not null");
		sql.append("   GROUP BY order_number, delivery_id,");
		sql.append("     product_id,");
		sql.append("     lot,");
		sql.append("     price");
		sql.append("   ) temp");
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT sod.product_id,");
		sql.append("     so.delivery_id,");
		sql.append("     SUM(NVL(discount_amount, 0)) discountAmount");
		sql.append("   FROM sale_order so");
		sql.append("   JOIN sale_order_detail sod");
		sql.append("   ON so.sale_order_id   = sod.sale_order_id");
		sql.append("   WHERE approved        = 1");
		sql.append("   AND type              = 1");
		sql.append("   AND is_free_item      = 1");
		sql.append("   AND order_type       IN ('IN')");
		sql.append("   AND program_type NOT IN (3,4,5)");		
		sql.append("   AND sod.quantity      > 0");
		if (fromDate != null) {
			sql.append(" and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append("   GROUP BY so.delivery_id,");
		sql.append("     sod.product_id");
		sql.append("   ) temp1 ON temp.product_id = temp1.product_id AND temp.delivery_id         = temp1.delivery_id )");
		sql.append(" JOIN product pr");
		sql.append(" ON temp.product_id = pr.product_id");
		sql.append(" ORDER BY temp.delivery_id,");
		sql.append("   pr.product_code)");
		sql.append(" group by productCode, productName, convfact, checkLot, lot, price, deliveryStaffInfo, deliveryStaffId order by deliveryStaffId, productCode");
		
		/*System.out.print(TestUtil.addParamToQuery(sql.toString(), params));*/
		System.out.println(sql.toString());
		
		String[] fieldNames = { "productCode", 
				"productName", 
				"convfact", 
				"checkLot",
				"lot", 
				"price", 
				"promoQuantity", 
				"saleQuantity", "deliveryStaffInfo", "deliveryStaffId", "discountAmount", "orderNumber"};
		
		Type[] fieldTypes = { 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, 
				StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, 
				StandardBasicTypes.STRING, 
				StandardBasicTypes.LONG, 
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING};
		
		return repo.getListByQueryAndScalar(RptExSaleOrderVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public String getListOrderNumberForReport(Long shopId, Date fromDate, Date toDate,
			Long deliveryStaffId) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select LISTAGG(order_number, '') WITHIN GROUP (ORDER BY order_number)");
		sql.append(" from sale_order so ");
		sql.append(" where approved in (0,1) and type = 1 and order_type in ('IN','TT') ");
		sql.append("	and exists(select 1 from sale_order_detail sod where sod.sale_order_id = so.sale_order_id and sod.quantity > 0 and sod.product_id is not null and program_type not in (3,4,5))");
		sql.append("	and delivery_id is not null");
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		if (deliveryStaffId != null) {
			sql.append(" and delivery_id = ? ");
			params.add(deliveryStaffId);
		}
		if (fromDate != null) {
			sql.append(" and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		
		return (String) repo.getObjectByQuery(sql.toString(), params);
	}
	

	@Override
	public List<RptSaleOrderLotVO> getListSaleOrderLotVO(
			KPaging<RptSaleOrderLotVO> kPaging, Long shopId,
			Long deliveryStaffId, Date fromDate, Date toDate,
			Integer isFreeItem, Boolean getPromotion, Long saleOrderId)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptSaleOrderLotVO>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" SELECT p.product_code productCode, ");
		sql.append("   p.product_name productName, ");
		sql.append("   sol.lot, ");
		sql.append("   sod.price, ");
		sql.append("   case when sod.is_free_item = 0 then sol.quantity else 0 end lotQuantity, ");
		sql.append("   case when sod.is_free_item = 1 then sol.quantity else 0 end promotionLotQuantity, ");
		sql.append("   sod.quantity, ");
		sql.append("   p.convfact, ");
		sql.append("   sod.is_free_item isFreeItem, ");
		sql.append("   sod.discount_amount discountAmount, ");

		if (Boolean.TRUE.equals(getPromotion)) {
			sql.append("     sod.program_code as promotionCode, ");
			sql.append("	(case when program_type in(0,1) ");
			sql.append("	then (select promotion_program_name as promotionName from promotion_program pg where pg.promotion_program_code=sod.program_code and pg.status = 1)");
			sql.append("	when program_type in (2)");
			sql.append("	then (select display_program_name as promotionName from display_program pg where pg.display_program_code=sod.program_code and pg.status = 1)");
			sql.append(" 	end) as promotionName");
		} else {
			sql.append("     '' as promotionCode, '' as promotionName");
		}

		fromSql.append(" from sale_order so join sale_order_detail sod on so.sale_order_id=sod.sale_order_id");
		fromSql.append("   left join sale_order_lot sol on sol.sale_order_detail_id=sod.sale_order_detail_id");
		fromSql.append("   join product p on p.product_id=sod.product_id");
		fromSql.append(" where 1=1");

		if (saleOrderId != null) {
			fromSql.append("   and so.sale_order_id = ?");
			params.add(saleOrderId);
		}

		if (isFreeItem != null) {
			fromSql.append("   and sod.is_free_item = ? and (sod.program_type is null or sod.program_type not in (?, ?, ?)) and sod.quantity > 0 ");
			params.add(isFreeItem);
			
			params.add(ProgramType.PRODUCT_EXCHANGE.getValue());
			params.add(ProgramType.DESTROY.getValue());
			params.add(ProgramType.RETURN.getValue());
		}

		if (fromDate != null) {
			fromSql.append("   and so.order_date >= trunc(?)");
			params.add(fromDate);
		}

		if (toDate != null) {
			fromSql.append("   and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		if (deliveryStaffId != null) {
			fromSql.append("   and so.delivery_id = ?");
			params.add(deliveryStaffId);
		}
		fromSql.append("   and so.approved=? and so.type=?");
		params.add(ApprovalStatus.APPROVED.getValue());
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());

		fromSql.append("   and so.order_type=?");
		params.add(OrderType.IN.getValue());

		if (shopId != null) {
			fromSql.append(" 	and so.shop_id=?");
			params.add(shopId);
		}

		sql.append(fromSql);
		sql.append(" order by p.product_code, sol.lot");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count");
		countSql.append(fromSql);

		String[] fieldNames = { "productCode", // 1
				"productName", // 2
				"lot", // 3
				"price", // 4
				"lotQuantity", // 5
				"promotionLotQuantity",
				"quantity", // 6
				"convfact", // 7
				"isFreeItem", // 8
				"discountAmount",// 9
				"promotionCode", "promotionName" };
		
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		
		/*System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));*/
		if (kPaging == null)
			return repo.getListByQueryAndScalar(RptSaleOrderLotVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(
					RptSaleOrderLotVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params,
					kPaging);
	}
	
	
	@Override
	public List<RptSaleOrderLotVO> getListSaleOrderLotVOEx(
			KPaging<RptSaleOrderLotVO> kPaging, Long shopId,
			Long deliveryStaffId, Date fromDate, Date toDate,
			Integer isFreeItem, Boolean getPromotion, Long saleOrderId)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptSaleOrderLotVO>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" SELECT p.product_code productCode, ");
		sql.append("   p.product_name productName, ");
		//sql.append("   sol.lot, ");
		sql.append("   sod.price, ");
		//sql.append("   case when sod.is_free_item = 0 then sol.quantity else 0 end lotQuantity, ");
		//sql.append("   case when sod.is_free_item = 1 then sol.quantity else 0 end promotionLotQuantity, ");
		sql.append("   sod.quantity, ");
		sql.append("   p.convfact, ");
		sql.append("   sod.is_free_item isFreeItem, ");
		sql.append("   sod.discount_amount discountAmount, ");

		if (Boolean.TRUE.equals(getPromotion)) {
			sql.append("     sod.program_code as promotionCode, ");
			sql.append("	(case when program_type in(0,1) ");
			sql.append("	then (select promotion_program_name as promotionName from promotion_program pg where pg.promotion_program_code=sod.program_code and pg.status = 1)");
			sql.append("	when program_type in (2)");
			sql.append("	then (select display_program_name as promotionName from display_program pg where pg.display_program_code=sod.program_code and pg.status = 1)");
			sql.append(" 	end) as promotionName");
		} else {
			sql.append("     '' as promotionCode, '' as promotionName");
		}

		fromSql.append(" from sale_order so join sale_order_detail sod on so.sale_order_id=sod.sale_order_id");
		//fromSql.append("   left join sale_order_lot sol on sol.sale_order_detail_id=sod.sale_order_detail_id");
		fromSql.append("   join product p on p.product_id=sod.product_id");
		fromSql.append(" where 1=1");

		if (saleOrderId != null) {
			fromSql.append("   and so.sale_order_id = ?");
			params.add(saleOrderId);
		}

		if (isFreeItem != null) {
			fromSql.append("   and sod.is_free_item = ? and (sod.program_type is null or sod.program_type not in (?, ?, ?)) and sod.quantity > 0 ");
			params.add(isFreeItem);
			
			params.add(ProgramType.PRODUCT_EXCHANGE.getValue());
			params.add(ProgramType.DESTROY.getValue());
			params.add(ProgramType.RETURN.getValue());
		}

		if (fromDate != null) {
			fromSql.append("   and so.order_date >= trunc(?)");
			params.add(fromDate);
		}

		if (toDate != null) {
			fromSql.append("   and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		if (deliveryStaffId != null) {
			fromSql.append("   and so.delivery_id = ?");
			params.add(deliveryStaffId);
		}
		fromSql.append("   and so.approved=? and so.type=?");
		params.add(ApprovalStatus.APPROVED.getValue());
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());

		fromSql.append("   and so.order_type IN (?, ?)");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.TT.getValue());

		if (shopId != null) {
			fromSql.append(" 	and so.shop_id=?");
			params.add(shopId);
		}

		sql.append(fromSql);
		sql.append(" order by p.product_code");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count");
		countSql.append(fromSql);

		String[] fieldNames = { "productCode", // 1
				"productName", // 2
				"price", // 4
				"quantity", // 6
				"convfact", // 7
				"isFreeItem", // 8
				"discountAmount",// 9
				"promotionCode", "promotionName" };
		
		Type[] fieldTypes = { StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		
		/*System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));*/
		if (kPaging == null)
			return repo.getListByQueryAndScalar(RptSaleOrderLotVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(
					RptSaleOrderLotVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params,
					kPaging);
	}
	

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SaleOrderDetailDAO#getListRptReturnSaleOrder(java.lang.Long, java.util.Date, java.util.Date, java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<RptReturnSaleOrderVO> getListRptReturnSaleOrder(Long shopId,
			Date fromDate, Date toDate, Long customerId)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptReturnSaleOrderVO>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT distinct so.sale_order_id soId,");
		sql.append(" sod.sale_order_detail_id sodId,");
		sql.append(" so.order_date        AS orderDate,");
		sql.append(" so.order_number           AS orderNumber,");
		sql.append(" (select order_number from sale_order sor where sor.sale_order_id = so.from_sale_order_id) rootOrderNumber,");
		sql.append(" so.amount                 AS amount,");
		sql.append(" so.total                  AS total,");
		sql.append(" so.staff_id               AS staffId,");
		sql.append(" so.delivery_id            AS deliveryId,");
		sql.append(" s1.staff_code             AS staffCode,");
		sql.append(" s2.staff_code             AS deliveryCode,");
		sql.append(" s1.staff_name             AS staffName,");
		sql.append(" s2.staff_name             AS deliveryName,");
		sql.append(" sod.is_free_item          AS isFreeItem,");
		sql.append(" sod.quantity              AS quantity,");
		sql.append(" sod.amount                AS detailAmount,");
		sql.append(" nvl(so.discount,0)       AS discountAmount,");
		sql.append(" (case when sod.is_free_item = 1 then 0 else sod.price end) AS priceValue,");
		sql.append(" p.product_code            AS productCode,");
		sql.append(" p.product_name            AS productName,");
		sql.append(" p.convfact                AS convfact,");
		sql.append(" c.short_code           AS shortCode,");
		sql.append(" c.customer_code           AS customerCode,");
		sql.append(" c.customer_name           AS customerName,");
		sql.append(" c.address           AS customerAddress,");
		sql.append(" c.phone                   AS phone,");
		sql.append(" c.mobiphone               AS mobiphone");
		sql.append(" FROM sale_order so, staff s1, staff s2, sale_order_detail sod,");
		sql.append(" product p, customer c, promotion_program pp");
		sql.append(" WHERE so.staff_id         = s1.staff_id");
		sql.append(" AND so.delivery_id        = s2.staff_id");
		sql.append(" AND sod.sale_order_id     = so.sale_order_id");
		sql.append(" AND so.customer_id        = c.customer_id");
		sql.append(" AND so.order_type         = ?");
		params.add(OrderType.CM.getValue());
		sql.append(" AND sod.product_id        = p.product_id");
		//fix bug 0007293 - check quantity > 0
		sql.append(" AND sod.quantity 		   > 0");
		sql.append(" AND so.shop_id            = ?");
		params.add(shopId);
		if (fromDate != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		if (customerId != null) {
			sql.append(" AND c.customer_id = ?");
			params.add(customerId);
		}
		sql.append(" order by so.sale_order_id,sod.is_free_item DESC,sod.sale_order_detail_id");
		String[] fieldNames = {
				"soId", "sodId",
				"orderDate", "orderNumber", "rootOrderNumber", "amount", "total", 
				"staffCode", "deliveryCode", "staffName", "deliveryName", "isFreeItem", 
				"quantity", "detailAmount", "discountAmount", 
				"priceValue", "productCode", "productName", "convfact", "shortCode",
				"customerCode", "customerName", "customerAddress","phone", "mobiphone" 
				};
		Type[] fieldTypes = {
				StandardBasicTypes.LONG,StandardBasicTypes.LONG,
				StandardBasicTypes.TIMESTAMP, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				};
		System.out.println(sql.toString());
		return repo.getListByQueryAndScalar(RptReturnSaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<RptProductDistrResult2VO> getListRptProductDistrResult2VO(
			Long shopId, String staffCode, Date fromDate, Date toDate) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptProductDistrResult2VO>();
		
		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		if (toDate == null) {
			throw new IllegalArgumentException("toDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT s.staff_code     AS staffCode,");
		sql.append("   s.staff_name          AS staffName,");
		sql.append("   p.product_code        AS productCode,");
		sql.append("   so.customer_id        AS customerId,");
		sql.append("   COUNT(so.customer_id) AS custCount,");
		sql.append("   SUM(sod.quantity)     AS quantity,");
		sql.append("   SUM(sod.amount)       AS amount");
		sql.append(" FROM sale_order so,");
		sql.append("   sale_order_detail sod,");
		sql.append("   staff s,");
		sql.append("   product p");
		sql.append(" WHERE so.sale_order_id = sod.sale_order_id");
		sql.append(" AND so.staff_id        = s.staff_id");
		sql.append(" AND sod.product_id     = p.product_id");
		sql.append(" AND so.order_type      = 'IN'");
		sql.append(" AND so.approved        = 1");
		sql.append(" AND sod.is_free_item   = 0");
		sql.append(" AND so.order_date >= trunc(?)");
		params.add(fromDate);
		sql.append(" AND so.order_date < trunc(?) + 1");
		params.add(toDate);
		sql.append(" AND so.shop_id         = ?");
		params.add(shopId);
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			staffCode = staffCode.toUpperCase();
			sql.append(" AND UPPER(s.staff_code)        = ?");
			params.add(staffCode);
		}
		sql.append(" GROUP BY s.staff_code,");
		sql.append("   s.staff_name,");
		sql.append("   p.product_code,");
		sql.append("   so.customer_id");
		sql.append(" ORDER BY s.staff_code,");
		sql.append("   p.product_code");
		
		String[] fieldNames = { "staffCode", // 1
				"staffName", // 2
				"productCode",// 3
				"customerId",// 4
				"custCount",// 5
				"quantity",// 6
				"amount"// 6
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.LONG,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.INTEGER,// 6
				StandardBasicTypes.BIG_DECIMAL// 6
		};
		return repo.getListByQueryAndScalar(RptProductDistrResult2VO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	
//	@Override
//	public List<RptInvDetailStat2VO> getListRptInvDetailStat2VO(
//			Long shopId, String staffCode, String shortCode, Integer vat, Date fromDate, Date toDate, InvoiceStatus status) throws DataAccessException {
//
//		if (fromDate != null && toDate != null && fromDate.after(toDate))
//			return new ArrayList<RptInvDetailStat2VO>();
//		
//		if (shopId == null || shopId <= 0) {
//			throw new IllegalArgumentException("shopId is null or <= 0");
//		}
//		if (fromDate == null) {
//			throw new IllegalArgumentException("fromDate is null");
//		}
//		if (toDate == null) {
//			throw new IllegalArgumentException("toDate is null");
//		}
//		StringBuilder sql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		sql.append(" SELECT inv.invoice_number AS invoiceNumber, ");
//		sql.append("  inv.order_date        AS invoiceDate, ");
//		sql.append("  inv.customer_code            AS shortCode, ");
//		sql.append("  inv.cust_name         AS customerName, ");
//		sql.append("  inv.customer_addr               AS address, ");
//		sql.append("  inv.cust_tax_number     AS custTaxNumber, ");
//		sql.append("  p.product_code          AS productCode, ");
//		sql.append("  p.product_name          AS productName, ");
//		sql.append("  p.uom1                  AS uom1, ");
//		sql.append("  inv.vat                 AS vat, ");
//		sql.append("  ( ");
//		sql.append("  CASE ");
//		sql.append("    WHEN sod.is_free_item = 1 ");
//		sql.append("    THEN 0 ");
//		sql.append("    ELSE (sod.price_not_vat * sod.quantity) ");
//		sql.append("  END)                                             AS amount, ");
//		sql.append("  sod.amount                                       AS finalAmount, ");
//		sql.append("  ( ");
//		sql.append("  CASE ");
//		sql.append("    WHEN sod.is_free_item = 1 ");
//		sql.append("    THEN 0 ");
//		sql.append("    ELSE (sod.price_not_vat * sod.quantity) * inv.VAT/100 ");
//		sql.append("  END)                                             AS taxAmount, ");
//		sql.append("  inv.status                                       AS status, ");
//		sql.append("  so.order_number                                  AS orderNumber, ");
//		sql.append("  (select staff_code from staff where staff_id = inv.staff_id) AS staffCode, ");
//		sql.append("  sod.quantity AS sku,");
//		sql.append("  inv.discount as discount ");
//		sql.append("FROM sale_order so ");
//		sql.append("JOIN sale_order_detail sod ");
//		sql.append("ON so.sale_order_id = sod.sale_order_id ");
//		sql.append("JOIN invoice inv ");
//		sql.append("ON sod.invoice_id = inv.invoice_id ");
//		sql.append("JOIN product p ");
//		sql.append("ON p.product_id = sod.product_id ");
//		sql.append("WHERE 1          = 1 ");
//		sql.append(" AND so.shop_id               = ?");
//		params.add(shopId);
//		sql.append(" AND inv.order_date >= TRUNC(?)");
//		params.add(fromDate);
//		sql.append(" AND inv.order_date < TRUNC(?) + 1");
//		params.add(toDate);
//		if (!StringUtility.isNullOrEmpty(staffCode)) {
//			staffCode = staffCode.toUpperCase();
//			sql.append(" AND UPPER(s.staff_code)      = ?");
//			params.add(staffCode);
//		}
//		if (!StringUtility.isNullOrEmpty(shortCode)) {
//			shortCode = shortCode.toUpperCase();
//			sql.append(" AND UPPER(c.short_code)      = ?");
//			params.add(shortCode);
//		}
//		if (vat != null) {
//			sql.append(" AND inv.vat                  = ?");
//			params.add(vat);
//		}
//		if (status != null) {
//			sql.append(" AND inv.status                  = ?");
//			params.add(status.getValue());
//		}
//		sql.append(" ORDER BY inv.invoice_number,");
//		sql.append(" inv.customer_code,");
//		sql.append(" p.product_code");
//		
//		String[] fieldNames = { 
//				"invoiceNumber", "invoiceDate", "shortCode", "customerName",
//				"address", "custTaxNumber", "productCode", "productName",
//				"uom1", "vat", "amount",
//				"finalAmount", "taxAmount", "status", "orderNumber",
//				"staffCode", "sku", "discount"
//		};
//		Type[] fieldTypes = { 
//				StandardBasicTypes.STRING, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
//				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
//				StandardBasicTypes.STRING, StandardBasicTypes.FLOAT, StandardBasicTypes.BIG_DECIMAL,
//				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING,
//				StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL 
//		};
//		return repo.getListByQueryAndScalar(RptInvDetailStat2VO.class,
//				fieldNames, fieldTypes, sql.toString(), params);
//	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SaleOrderDetailDAO#getListChangeReturnSaleOrder(java.lang.Long, java.util.Date, java.util.Date, java.lang.Long)
	 */
	@Override
	public List<RptReturnSaleOrderVO> getListChangeReturnSaleOrder(Long shopId,
			Date fromDate, Date toDate, Long deliveryId)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptReturnSaleOrderVO>();
		
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT so.order_date      AS orderDate,");
		sql.append(" so.order_number           AS orderNumber,");
		sql.append(" nvbh.staff_code           AS staffCode,");
		sql.append(" nvbh.staff_name           AS staffName,");
		sql.append(" nvgh.staff_code           AS deliveryCode,");
		sql.append(" nvgh.staff_name           AS deliveryName,");
		sql.append(" sod.is_free_item          AS isFreeItem,");
		sql.append(" sod.quantity              AS quantity,");
		sql.append(" sod.amount                AS detailAmout,");
		sql.append(" sod.discount_amount       AS discountAmount,");
		sql.append(" sod.discount_percent      AS discountPercent,");
		sql.append(" sod.price                 AS priceValue,");
		sql.append(" p.product_code            AS productCode,");
		sql.append(" p.product_name            AS productName,");
		sql.append(" p.convfact                AS convfact,");
		sql.append(" c.customer_code           AS customerCode,");
		sql.append(" c.customer_name           AS customerName,");
		sql.append(" c.phone                   AS phone,");
		sql.append(" c.mobiphone               AS mobiphone,");
		sql.append(" pp.promotion_program_code AS promotionProgramCode,");
		sql.append(" pp.promotion_program_name AS promotionProgramname,");
		sql.append(" iv.invoice_number         AS invoiceNumber");
		sql.append(" FROM sale_order so, staff nvbh, staff nvgh, sale_order_detail sod,");
		sql.append(" product p, customer c, promotion_program pp, invoice iv");
		sql.append(" WHERE so.staff_id         = nvbh.staff_id");
		sql.append(" AND so.delivery_id        = nvgh.staff_id");
		sql.append(" AND sod.sale_order_id     = so.sale_order_id");
		sql.append(" AND so.customer_id        = c.customer_id");
		sql.append(" AND sod.program_code      = pp.promotion_program_code");
		sql.append(" AND so.order_type         = ?");
		params.add(OrderType.IN.getValue());
		sql.append(" AND so.invoice_id         IS NOT NULL");
		sql.append(" AND sod.program_type      = ?");
		params.add(ProgramType.PRODUCT_EXCHANGE.getValue());
		sql.append(" AND sod.product_id        = p.product_id");
		sql.append(" AND so.shop_id            = ?");
		params.add(shopId);
		if (fromDate != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		if (deliveryId != null) {
			sql.append(" AND so.delivery_id = ?");
			params.add(deliveryId);
		}
		sql.append(" ORDER BY so.order_number");
		String[] fieldNames = {
				"orderDate", //1
				"orderNumber", //2
				"staffCode", 			//3
				"staffName", 		//4
				"deliveryCode", 	//5
				"deliveryName", 	//6
				"isFreeItem", 	//7
				"quantity", 	//8
				"detailAmout", 	//9
				"discountAmount", 	//10
				"discountPercent", 	//11
				"priceValue", 	//12
				"productCode", 	//13
				"productName", 	//14
				"convfact", 	//15
				"customerCode", 	//16
				"customerName", 	//17
				"phone", 	//18
				"mobiphone", 	//19
				"promotionProgramCode", 	//20
				"promotionProgramCode", 	//21
				"invoiceNumber" //22
				};
		Type[] fieldTypes = {
				StandardBasicTypes.TIMESTAMP, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING, //4 
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.INTEGER, //7
				StandardBasicTypes.INTEGER, //8
				StandardBasicTypes.BIG_DECIMAL, //9
				StandardBasicTypes.BIG_DECIMAL, //10
				StandardBasicTypes.FLOAT, //11
				StandardBasicTypes.STRING, //12
				StandardBasicTypes.STRING, //13
				StandardBasicTypes.STRING, //14
				StandardBasicTypes.INTEGER, //15
				StandardBasicTypes.STRING, //16
				StandardBasicTypes.STRING, //17
				StandardBasicTypes.STRING, //18
				StandardBasicTypes.STRING, //19
				StandardBasicTypes.STRING, //20
				StandardBasicTypes.STRING, //21
				StandardBasicTypes.STRING}; //22
		return repo.getListByQueryAndScalar(RptReturnSaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SaleOrderDetailDAO#getSku(java.lang.Long)
	 */
	@Override
	public Long getSku(Long saleOrderId, Long invoiceId) throws DataAccessException {
		
		if (saleOrderId == null) {
			throw new IllegalArgumentException("saleOrderId is null");
		}
		String sql = "select count(distinct product_id) as count from sale_order_detail where sale_order_id = ? and is_free_item = 0";
		List<Object> params = new ArrayList<Object>();
		params.add(saleOrderId);
		if (invoiceId != null) {
			sql += " and invoice_id = ?";
			params.add(invoiceId);
		}
		Object o = repo.getObjectByQuery(sql, params);
		Long result = Long.valueOf(o.toString());
		return result;
	}

	@Override
	public List<PrintDeliveryGroupExportVO2> getListPrintDeliveryGroupExportVO2(
			List<Long> listSaleOrderId, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT om_tmp.orderNumber AS orderNumber,");
		sql.append("   tmp.deliveryCode AS deliveryCode,");
		sql.append("   tmp.deliveryName      AS deliveryName,");
		sql.append("   tmp.productCode       AS productCode,");
		sql.append("   tmp.productName       AS productName,");
		sql.append("   tmp.lot               AS lot,");
		sql.append("   NVL(tmp.convfact, 0)          AS convfact,");
		sql.append("   NVL(tmp.price, 0)             AS price,");
		sql.append("   NVL(tmp.quantity, 0)          AS quantity,");
		sql.append("   NVL(tmp.amount, 0)            AS amount,");
		sql.append("   NVL(tmp.proQuantity, 0)       AS proQuantity,");
		sql.append("   NVL(tmp.proAmount, 0)         AS proAmount,");
		sql.append("   NVL(tmp.proMoney, 0)          AS proMoney");
		sql.append("   FROM");
		sql.append("   ((SELECT NVL(so0.deliveryCode, so1.deliveryCode) AS deliveryCode,");
		sql.append("     NVL(so0.deliveryName, so1.deliveryName)       AS deliveryName,");
		sql.append("     NVL(so0.productCode, so1.productCode)         AS productCode,");
		sql.append("     NVL(so0.productName, so1.productName)         AS productName,");
		sql.append("     NVL(so0.lot, so1.lot)                         AS lot,");
		sql.append("     NVL(so0.convfact, so1.convfact)               AS convfact,");
		sql.append("     NVL(so0.price, so1.price)                     AS price,");
		sql.append("     NVL(so0.quantity, 0)                          AS quantity,");
		sql.append("     NVL(so0.amount, 0)                            AS amount,");
		sql.append("     NVL(so1.quantity, 0)                          AS proQuantity,");
		sql.append("     NVL(so1.amount, 0)                            AS proAmount,");
		sql.append("     NULL                                          AS proMoney");
		sql.append("   FROM (");
		sql.append("     (SELECT s.staff_code        AS deliveryCode,");
		sql.append("       s.staff_name              AS deliveryName,");
		sql.append("       p.product_code            AS productCode,");
		sql.append("       p.product_name            AS productName,");
		sql.append("       p.convfact                AS convfact,");
		sql.append("       SUM(NVL(sod.quantity, 0)) AS quantity,");
		sql.append("       NVL(sod.price, 0)         AS price,");
		sql.append("       SUM(NVL(sod.amount, 0))   AS amount,");
		sql.append("       ''                        AS lot");
		sql.append("     FROM sale_order_detail sod");
		sql.append("     JOIN sale_order so");
		sql.append("     ON (so.sale_order_id = sod.sale_order_id)");
		sql.append("     JOIN staff s");
		sql.append("     ON (s.staff_id = so.delivery_id)");
		sql.append("     JOIN product p");
		sql.append("     ON (p.product_id       = sod.product_id");
		sql.append("     AND p.check_lot        = 0)");
		sql.append("     WHERE sod.is_free_item = 0");
		sql.append("     AND (1                != 1");
		if (listSaleOrderId != null) {
			for (Long saleOrderId : listSaleOrderId) {
				if (saleOrderId != null) {
					sql.append(" OR so.sale_order_id = ?");
					params.add(saleOrderId);
				}
			}
		}
		sql.append("     )");
		if (fromDate != null) {
			sql.append("   AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("   AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		sql.append("     GROUP BY s.staff_code,");
		sql.append("       s.staff_name,");
		sql.append("       p.product_code,");
		sql.append("       p.product_name,");
		sql.append("       p.convfact,");
		sql.append("       NVL(sod.price, 0),");
		sql.append("       ''");
		sql.append("     )");
		sql.append("   UNION");
		sql.append("     (SELECT s.staff_code                            AS deliveryCode,");
		sql.append("       s.staff_name                                  AS deliveryName,");
		sql.append("       p.product_code                                AS productCode,");
		sql.append("       p.product_name                                AS productName,");
		sql.append("       p.convfact                                    AS convfact,");
		sql.append("       SUM(NVL(sol.quantity, 0))                     AS quantity,");
		sql.append("       NVL(sod.price, 0)                             AS price,");
		sql.append("       SUM(NVL(sol.quantity, 0) * NVL(sod.price, 0)) AS amount,");
		sql.append("       sol.lot                                       AS lot");
		sql.append("     FROM sale_order_lot sol");
		sql.append("     JOIN sale_order_detail sod");
		sql.append("     ON (sol.sale_order_detail_id = sod.sale_order_detail_id)");
		sql.append("     JOIN sale_order so");
		sql.append("     ON (so.sale_order_id = sod.sale_order_id)");
		sql.append("     JOIN staff s");
		sql.append("     ON (s.staff_id = so.delivery_id)");
		sql.append("     JOIN product p");
		sql.append("     ON (p.product_id       = sod.product_id");
		sql.append("     AND p.check_lot        = 1)");
		sql.append("     WHERE sod.is_free_item = 0");
		sql.append("     AND (1                != 1");
		if (listSaleOrderId != null) {
			for (Long saleOrderId : listSaleOrderId) {
				if (saleOrderId != null) {
					sql.append(" OR so.sale_order_id = ?");
					params.add(saleOrderId);
				}
			}
		}
		sql.append("     )");
		if (fromDate != null) {
			sql.append("   AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("   AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		sql.append("     GROUP BY s.staff_code,");
		sql.append("       s.staff_name,");
		sql.append("       p.product_code,");
		sql.append("       p.product_name,");
		sql.append("       p.convfact,");
		sql.append("       NVL(sod.price, 0),");
		sql.append("       sol.lot");
		sql.append("     )) so0");
		sql.append("   FULL JOIN (");
		sql.append("     (SELECT s.staff_code        AS deliveryCode,");
		sql.append("       s.staff_name              AS deliveryName,");
		sql.append("       p.product_code            AS productCode,");
		sql.append("       p.product_name            AS productName,");
		sql.append("       p.convfact                AS convfact,");
		sql.append("       SUM(NVL(sod.quantity, 0)) AS quantity,");
		sql.append("       NVL(sod.price, 0)         AS price,");
		sql.append("       SUM(NVL(sod.amount, 0))   AS amount,");
		sql.append("       ''                        AS lot");
		sql.append("     FROM sale_order_detail sod");
		sql.append("     JOIN sale_order so");
		sql.append("     ON (so.sale_order_id = sod.sale_order_id)");
		sql.append("     JOIN staff s");
		sql.append("     ON (s.staff_id = so.delivery_id)");
		sql.append("     JOIN product p");
		sql.append("     ON (p.product_id       = sod.product_id");
		sql.append("     AND p.check_lot        = 0)");
		sql.append("     WHERE sod.is_free_item = 1");
		sql.append("     AND (1                != 1");
		if (listSaleOrderId != null) {
			for (Long saleOrderId : listSaleOrderId) {
				if (saleOrderId != null) {
					sql.append(" OR so.sale_order_id = ?");
					params.add(saleOrderId);
				}
			}
		}
		sql.append("     )");
		if (fromDate != null) {
			sql.append("   AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("   AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		sql.append("     GROUP BY s.staff_code,");
		sql.append("       s.staff_name,");
		sql.append("       p.product_code,");
		sql.append("       p.product_name,");
		sql.append("       p.convfact,");
		sql.append("       NVL(sod.price, 0),");
		sql.append("       ''");
		sql.append("     )");
		sql.append("   UNION");
		sql.append("     (SELECT s.staff_code                            AS deliveryCode,");
		sql.append("       s.staff_name                                  AS deliveryName,");
		sql.append("       p.product_code                                AS productCode,");
		sql.append("       p.product_name                                AS productName,");
		sql.append("       p.convfact                                    AS convfact,");
		sql.append("       SUM(NVL(sol.quantity, 0))                     AS quantity,");
		sql.append("       NVL(sod.price, 0)                             AS price,");
		sql.append("       SUM(NVL(sol.quantity, 0) * NVL(sod.price, 0)) AS amount,");
		sql.append("       sol.lot                                       AS lot");
		sql.append("     FROM sale_order_lot sol");
		sql.append("     JOIN sale_order_detail sod");
		sql.append("     ON (sol.sale_order_detail_id = sod.sale_order_detail_id)");
		sql.append("     JOIN sale_order so");
		sql.append("     ON (so.sale_order_id = sod.sale_order_id)");
		sql.append("     JOIN staff s");
		sql.append("     ON (s.staff_id = so.delivery_id)");
		sql.append("     JOIN product p");
		sql.append("     ON (p.product_id       = sod.product_id");
		sql.append("     AND p.check_lot        = 1)");
		sql.append("     WHERE sod.is_free_item = 1 and (sod.program_type = 0 or sod.program_type = 1 or sod.program_type = 2)");
		sql.append("     AND (1                != 1");
		if (listSaleOrderId != null) {
			for (Long saleOrderId : listSaleOrderId) {
				if (saleOrderId != null) {
					sql.append(" OR so.sale_order_id = ?");
					params.add(saleOrderId);
				}
			}
		}
		sql.append("     )");
		if (fromDate != null) {
			sql.append("   AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("   AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		sql.append("     GROUP BY s.staff_code,");
		sql.append("       s.staff_name,");
		sql.append("       p.product_code,");
		sql.append("       p.product_name,");
		sql.append("       p.convfact,");
		sql.append("       NVL(sod.price, 0),");
		sql.append("       sol.lot");
		sql.append("     )) so1 ON (so0.deliveryCode = so1.deliveryCode");
		sql.append("   AND so0.productCode           = so1.productCode");
		sql.append("   AND so0.convfact              = so1.convfact");
		sql.append("   AND so0.price                 = so1.price");
		sql.append("   AND ((so0.lot                IS NULL");
		sql.append("   AND so1.lot                  IS NULL)");
		sql.append("   OR (so0.lot                   = so1.lot)))");
		sql.append("   )");
		sql.append(" UNION");
		sql.append("   (SELECT s.staff_code      AS deliveryCode,");
		sql.append("     s.staff_name            AS deliveryName,");
		sql.append("     NULL                    AS productCode,");
		sql.append("     NULL                    AS productName,");
		sql.append("     NULL                    AS lot,");
		sql.append("     NULL                    AS convfact,");
		sql.append("     NULL                    AS price,");
		sql.append("     NULL                    AS quantity,");
		sql.append("     NULL                    AS amount,");
		sql.append("     NULL                    AS proQuantity,");
		sql.append("     NULL                    AS proAmount,");
		sql.append("     SUM(NVL(sod.discount_amount, 0)) AS proMoney");
		sql.append("   FROM sale_order_detail sod");
		sql.append("   JOIN sale_order so");
		sql.append("   ON (so.sale_order_id = sod.sale_order_id)");
		sql.append("   JOIN staff s");
		sql.append("   ON (s.staff_id        = so.delivery_id)");
		sql.append("   WHERE sod.product_id IS NULL");
		sql.append("   AND (1               != 1");
		if (listSaleOrderId != null) {
			for (Long saleOrderId : listSaleOrderId) {
				if (saleOrderId != null) {
					sql.append(" OR so.sale_order_id = ?");
					params.add(saleOrderId);
				}
			}
		}
		sql.append("   )");
		if (fromDate != null) {
			sql.append("   AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("   AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		sql.append("   GROUP BY s.staff_code,");
		sql.append("     s.staff_name");
		sql.append(" )) tmp");
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT s.staff_code AS deliveryCode,");
		sql.append("     LISTAGG(order_number, ', ') WITHIN GROUP (");
		sql.append("   ORDER BY order_number) AS orderNumber");
		sql.append("   FROM sale_order so");
		sql.append("   JOIN staff s");
		sql.append("   ON (so.delivery_id = s.staff_id)");
		sql.append("   WHERE 1 = 1");
		sql.append("   AND (1               != 1");
		if (listSaleOrderId != null) {
			for (Long saleOrderId : listSaleOrderId) {
				if (saleOrderId != null) {
					sql.append(" OR so.sale_order_id = ?");
					params.add(saleOrderId);
				}
			}
		}
		sql.append("   )");
		if (fromDate != null) {
			sql.append("   AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("   AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		sql.append("   GROUP BY s.staff_code");
		sql.append("   ) om_tmp ON (om_tmp.deliveryCode = tmp.deliveryCode)");
		sql.append(" ORDER BY tmp.deliveryCode,");
		sql.append("   tmp.productCode");


		
		String[] fieldNames = { "deliveryCode", // 1
				"deliveryName", // 2
				"productCode", // 3
				"productName", // 4
				"lot", // 5
				"convfact", // 6
				"price", // 7
				"quantity", // 8
				"amount", // 9
				"proQuantity", // 10
				"proAmount", // 11
				"proMoney", // 12
				"orderNumber",
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.INTEGER, // 6
				StandardBasicTypes.BIG_DECIMAL, // 7
				StandardBasicTypes.INTEGER, // 8
				StandardBasicTypes.BIG_DECIMAL, // 9
				StandardBasicTypes.INTEGER, // 10
				StandardBasicTypes.BIG_DECIMAL, // 11
				StandardBasicTypes.BIG_DECIMAL, // 12
				StandardBasicTypes.STRING, // 3
		};
		return repo.getListByQueryAndScalar(PrintDeliveryGroupExportVO2.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SaleOrderDetailDAO#getListRptGenaralDetailDisplayProgram(java.lang.Long, java.lang.String)
	 */
	@Override
	public List<RptPayDetailDisplayProgr1VO> getListRptGenaralDetailDisplayProgram(
			Long shopId, String displayProgramCode) throws DataAccessException {
		
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT p.product_code     AS productCode,");
		sql.append(" p.product_name          AS productName,");
		sql.append(" temp.quantity           AS quantity,");
		sql.append(" sod.price               AS price");
		sql.append(" FROM product p,");
		sql.append(" sale_order_detail sod,");
		sql.append(" (SELECT sod.product_id     AS product_id,");
		sql.append(" sod.sale_order_detail_id AS sale_order_detail_id,");
		sql.append(" SUM(sod.quantity)        AS quantity");
		sql.append(" FROM sale_order_detail sod,");
		sql.append(" display_program dp,");
		sql.append(" sale_order so");
		sql.append(" WHERE so.sale_order_id        = sod.sale_order_id");
		sql.append(" AND so.approved             = 1");
		sql.append(" AND so.order_type           = ?");
		params.add(OrderType.IN.getValue());
		sql.append(" AND sod.is_free_item        = 1");
		sql.append(" AND sod.program_type        = ?");
		params.add(ProgramType.DISPLAY_SCORE.getValue());
		sql.append(" AND so.shop_id              = ?");
		params.add(shopId);
		sql.append(" AND sod.program_code        = ?");
		params.add(displayProgramCode);
		sql.append(" GROUP BY sod.product_id,");
		sql.append(" sod.sale_order_detail_id");
		sql.append(" ) temp");
		sql.append(" WHERE temp.product_id        = p.product_id");
		sql.append(" AND sod.sale_order_detail_id = temp.sale_order_detail_id");
		String[] fieldNames = {
				"productCode", //1
				"productName", //2
				"quantity", 			//3
				"price", 		//4
				};
		Type[] fieldTypes = {
				StandardBasicTypes.STRING, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.INTEGER, //3
				StandardBasicTypes.FLOAT, //4 
				};
		return repo.getListByQueryAndScalar(RptPayDetailDisplayProgr1VO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.SaleOrderDetailDAO#getListRptReturnSaleOrderAllowDelivery(java.lang.Long, java.util.Date, java.util.Date, java.lang.Long)
	 */
	@Override
	public List<RptReturnSaleOrderAllowDeliveryVO> getListRptReturnSaleOrderAllowDelivery(
			Long shopId, Date fromDate, Date toDate, Long deliveryId)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptReturnSaleOrderAllowDeliveryVO>();
		
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
    
		sql.append(" SELECT NVL(tb1.staff_code, tb2.staff_code) as deliveryCode,");
		sql.append(" NVL(tb1.staff_name, tb2.staff_name) as deliveryName,");
		sql.append(" NVL(tb1.product_code, tb2.product_code) as productCode,");
		sql.append(" NVL(tb1.product_name, tb2.product_name) as productName,");
		sql.append(" NVL(tb1.product_id, tb2.product_id) as productId,");
		sql.append(" NVL(tb1.convfact, tb2.convfact) as convfact,");
		sql.append(" NVL(tb1.lot, tb2.lot) as lot,");
		sql.append(" NVL(tb1.amount, 0) as amount,");
		sql.append(" NVL(tb1.discount_amount, tb2.discount_amount) as discountAmount,");
		sql.append(" NVL(tb1.quantity, 0) as quantity,");
		sql.append(" NVL(tb1.price, tb2.price) as price,");
		sql.append(" nvl(tb2.quantity,0) AS promotionQuantity,");
		sql.append(" nvl(tb3.sumDiscountAmount,0) as sumDiscountAmount");
		sql.append(" FROM");
		sql.append(" (SELECT staff_code, staff_name, product_code, product_name, product_id, convfact, lot, amount,");
		sql.append(" discount_amount, quantity, price");
		sql.append(" FROM (");
		sql.append(" (SELECT SUM(NVL(sod.amount,0))     AS amount,");
		sql.append(" SUM(NVL(sod.discount_amount,0))  AS discount_amount ,");
		sql.append(" SUM(NVL(sod.quantity,0))         AS quantity,");
		sql.append(" NVL(sod.price,0)                 AS price,");
		sql.append(" s.staff_code,");
		sql.append(" s.staff_name,");
		sql.append(" p.product_code,");
		sql.append(" p.product_name,");
		sql.append(" p.product_id,");
		sql.append(" p.convfact,");
		sql.append(" '' AS lot");
		sql.append(" FROM sale_order so,");
		sql.append(" sale_order_detail sod,");
		sql.append(" product p, staff s");
		sql.append(" WHERE so.sale_order_id = sod.sale_order_id");
		sql.append(" AND sod.product_id     = p.product_id");
		sql.append(" AND p.status 			= ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND p.check_lot 		= 0");
		sql.append(" AND so.shop_id         = ?");
		params.add(shopId);
		sql.append(" AND so.order_type = ?");
		params.add(OrderType.CM.getValue());
		sql.append(" AND so.order_date     >= TRUNC(?)");
		params.add(fromDate);
		sql.append(" AND so.order_date     < TRUNC(?) + 1");
		params.add(toDate);
		sql.append(" AND so.delivery_id = s.staff_id");
		if (deliveryId != null) {
			sql.append(" AND so.delivery_id     = ?");
			params.add(deliveryId);
		}
		sql.append(" AND sod.is_free_item   = 0");
		sql.append(" GROUP BY NVL(sod.price,0), s.staff_code, s.staff_name, p.product_code, p.product_name, p.product_id, p.convfact, '')");
		sql.append(" UNION");
		sql.append(" (SELECT SUM(NVL(sod.amount,0))     AS amount,");
		sql.append(" SUM(NVL(sod.discount_amount,0))  AS discount_amount ,");
		sql.append(" SUM(NVL(sol.quantity,0))         AS quantity,");
		sql.append(" NVL(sod.price,0)                 AS price,");
		sql.append(" s.staff_code,");
		sql.append(" s.staff_name,");
		sql.append(" p.product_code,");
		sql.append(" p.product_name,");
		sql.append(" p.product_id,");
		sql.append(" p.convfact,");
		sql.append(" sol.lot AS lot");
		sql.append(" FROM sale_order so,");
		sql.append(" sale_order_detail sod,");
		sql.append(" product p, staff s, sale_order_lot sol");
		sql.append(" WHERE so.sale_order_id = sod.sale_order_id");
		sql.append(" AND sod.product_id     = p.product_id");
		sql.append(" AND p.status 			= ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND p.check_lot 		= 1");
		sql.append(" AND so.shop_id         = ?");
		params.add(shopId);
		sql.append(" AND so.order_type = ?");
		params.add(OrderType.CM.getValue());
		sql.append(" AND sol.sale_order_detail_id = sod.sale_order_detail_id");
		sql.append(" AND sol.product_id           = p.product_id");
		sql.append(" AND so.order_date     >= TRUNC(?)");
		params.add(fromDate);
		sql.append(" AND so.order_date     < TRUNC(?) + 1");
		params.add(toDate);
		sql.append(" AND so.delivery_id = s.staff_id");
		if (deliveryId != null) {
			sql.append(" AND so.delivery_id     = ?");
			params.add(deliveryId);
		}
		sql.append(" AND sod.is_free_item   = 0");
		sql.append(" GROUP BY NVL(sod.price,0), s.staff_code, s.staff_name, p.product_code, p.product_name, p.product_id, p.convfact, sol.lot)");
		sql.append(" )) tb1");
		sql.append(" FULL JOIN");
		sql.append(" (SELECT staff_code, staff_name, product_code, product_name, product_id, convfact, lot,");
		sql.append(" amount, discount_amount, quantity, price");
		sql.append(" FROM (");
		sql.append(" (SELECT SUM(NVL(sod.amount,0))   AS amount,");
		sql.append(" SUM(NVL(sod.discount_amount,0))  AS discount_amount ,");
		sql.append(" SUM(NVL(sod.quantity,0))         AS quantity,");
		sql.append(" NVL(sod.price,0)                 AS price,");
		sql.append(" s.staff_code,");
		sql.append(" s.staff_name,");
		sql.append(" p.product_code,");
		sql.append(" p.product_name,");
		sql.append(" p.product_id,");
		sql.append(" p.convfact,");
		sql.append(" '' AS lot");
		sql.append(" FROM sale_order so,");
		sql.append(" sale_order_detail sod,");
		sql.append(" product p, staff s");
		sql.append(" WHERE so.sale_order_id = sod.sale_order_id");
		sql.append(" AND sod.product_id     = p.product_id");
		sql.append(" AND p.status 			= ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND p.check_lot 		= 0");
		sql.append(" AND so.shop_id         = ?");
		params.add(shopId);
		sql.append(" AND so.order_type = ?");
		params.add(OrderType.CM.getValue());
		sql.append(" AND so.order_date     >= TRUNC(?)");
		params.add(fromDate);
		sql.append(" AND so.order_date     < TRUNC(?) + 1");
		params.add(toDate);
		sql.append(" AND so.delivery_id = s.staff_id");
		if (deliveryId != null) {
			sql.append(" AND so.delivery_id     = ?");
			params.add(deliveryId);
		}
		sql.append(" AND sod.is_free_item   = 1");
		sql.append(" GROUP BY NVL(sod.price,0), s.staff_code, s.staff_name, p.product_code, p.product_name, p.product_id, p.convfact, '')");
		sql.append(" UNION");
		sql.append(" (SELECT SUM(NVL(sod.amount,0))     AS amount,");
		sql.append(" SUM(NVL(sod.discount_amount,0))  AS discount_amount ,");
		sql.append(" SUM(NVL(sol.quantity,0))         AS quantity,");
		sql.append(" NVL(sod.price,0)                 AS price,");
		sql.append(" s.staff_code,");
		sql.append(" s.staff_name,");
		sql.append(" p.product_code,");
		sql.append(" p.product_name,");
		sql.append(" p.product_id,");
		sql.append(" p.convfact,");
		sql.append(" sol.lot AS lot");
		sql.append(" FROM sale_order so,");
		sql.append(" sale_order_detail sod,");
		sql.append(" product p, staff s, sale_order_lot sol");
		sql.append(" WHERE so.sale_order_id = sod.sale_order_id");
		sql.append(" AND sod.product_id     = p.product_id");
		sql.append(" AND p.status 			= ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND p.check_lot 		= 1");
		sql.append(" AND so.shop_id         = ?");
		params.add(shopId);
		sql.append(" AND so.order_type = ?");
		params.add(OrderType.CM.getValue());
		sql.append(" AND sol.sale_order_detail_id = sod.sale_order_detail_id");
		sql.append(" AND sol.product_id           = p.product_id");
		sql.append(" AND so.order_date     >= TRUNC(?)");
		params.add(fromDate);
		sql.append(" AND so.order_date     < TRUNC(?) + 1");
		params.add(toDate);
		sql.append(" AND so.delivery_id = s.staff_id");
		if (deliveryId != null) {
			sql.append(" AND so.delivery_id     = ?");
			params.add(deliveryId);
		}
		sql.append(" AND sod.is_free_item   = 1");
		sql.append(" GROUP BY NVL(sod.price,0), s.staff_code, s.staff_name, p.product_code, p.product_name, p.product_id, p.convfact, sol.lot)");
		sql.append(" )) tb2");
		sql.append(" ON tb1.staff_code  = tb2.staff_code");
		sql.append(" AND tb1.product_id = tb2.product_id");
		sql.append(" AND ((tb1.lot     IS NULL");
		sql.append(" AND tb2.lot       IS NULL)");
		sql.append(" OR tb1.lot         = tb2.lot)");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT s.staff_code,");
		sql.append(" sum(sod.discount_amount) as sumDiscountAmount");
		sql.append(" FROM sale_order so");
		sql.append(" JOIN sale_order_detail sod");
		sql.append(" ON so.sale_order_id = sod.sale_order_id");
		sql.append(" JOIN staff s");
		sql.append(" ON (so.delivery_id = s.staff_id)");
		sql.append(" WHERE 1            = 1");
		sql.append(" AND so.shop_id     = ?");
		params.add(shopId);
		sql.append(" AND s.status 		= ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND so.order_type = ?");
		params.add(OrderType.CM.getValue());
		sql.append(" AND sod.product_id is null");
		sql.append(" AND so.order_date     >= TRUNC(?)");
		params.add(fromDate);
		sql.append(" AND so.order_date     < TRUNC(?) + 1");
		params.add(toDate);
		sql.append(" GROUP BY s.staff_code");
		sql.append(" ) tb3 ON (tb1.staff_code = tb3.staff_code");
		sql.append(" OR tb2.staff_code          = tb3.staff_code)");
		sql.append(" ORDER BY NVL(tb1.staff_code, tb2.staff_code),");
		sql.append(" NVL(tb1.product_code, tb2.product_code)");
		
		/*System.out.println(TestUtil.addParamToQuery(sql.toString(), params));*/
		
		String[] fieldNames = {
				"deliveryCode", //1
				"deliveryName", //2
				"productCode", 			//3
				"productName", 		//4
				"convfact", 		//5
				"lot", 		//6
				"amount", 		//7
				"discountAmount", 		//7
				"quantity", 		//8
				"price", 		//9
				"promotionQuantity", 		//10
				"sumDiscountAmount", 		//11
				};
		Type[] fieldTypes = {
				StandardBasicTypes.STRING, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING, //4 
				StandardBasicTypes.INTEGER, //5 
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.BIG_DECIMAL, //7
				StandardBasicTypes.BIG_DECIMAL, //7
				StandardBasicTypes.INTEGER, //8
				StandardBasicTypes.FLOAT, //9
				StandardBasicTypes.INTEGER, //10
				StandardBasicTypes.BIG_DECIMAL, //11
				};
		return repo.getListByQueryAndScalar(RptReturnSaleOrderAllowDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<PrintDeliveryGroupVO1> getListPrintDeliveryGroupVO(
			List<Long> listSaleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT s.staff_code         AS deliveryCode,");
		sql.append("   s.staff_name              AS deliveryName,");
		sql.append("   cu.short_code             AS customerShortCode,");
		sql.append("   cu.customer_code          AS customerCode,");
		sql.append("   cu.customer_name          AS customerName,");
		sql.append("   cu.address                AS customerAddress,");
		sql.append("   p.product_code            AS productCode,");
		sql.append("   p.product_name            AS productName,");
		sql.append("   sod.is_free_item          AS isFreeItem,");
		sql.append("   p.convfact                AS convfact,");
		sql.append("   SUM(NVL(sod.quantity, 0)) AS quantity,");
		sql.append("   NVL(sod.price, 0)         AS price,");
		sql.append("   SUM(NVL(sod.amount, 0))   AS amount,");
		sql.append("   SUM(NVL(sod.discount_amount, 0))   AS discountAmount,");
		sql.append("   pp.promotion_program_code AS promotionProgramCode,");
		sql.append("   pp.promotion_program_name AS promotionProgramName,");
		sql.append("   so.order_number           AS orderNumber");
//		sql.append("   car.car_number              AS carNumber");
		sql.append(" FROM sale_order_detail sod");
		sql.append(" JOIN sale_order so");
		sql.append(" ON so.sale_order_id = sod.sale_order_id");
		sql.append(" INNER JOIN car car ON so.car_id = car.car_id");
		sql.append(" AND (1 != 1");
		if (listSaleOrderId != null) {
			for (Long saleOrderId : listSaleOrderId) {
				if (saleOrderId != null) {
					sql.append(" OR so.sale_order_id = ?");
					params.add(saleOrderId);
				}
			}
		}
		sql.append(" )");
		sql.append(" JOIN customer cu");
		sql.append(" ON (cu.customer_id = so.customer_id)");
		sql.append(" JOIN staff s");
		sql.append(" ON (s.staff_id = so.delivery_id)");
		sql.append(" LEFT JOIN product p");
		sql.append(" ON (p.product_id = sod.product_id)");
		sql.append(" LEFT JOIN promotion_program pp");
		sql.append(" ON (sod.program_code  = pp.promotion_program_code");
		sql.append(" AND sod.is_free_item  = 1");
		sql.append(" AND sod.program_type IN (0, 1, 2))");
		sql.append(" where (sod.program_type IN (0, 1, 2) or sod.program_type is null)");
		sql.append(" GROUP BY s.staff_code,");
		sql.append("   s.staff_name,");
		sql.append("   cu.short_code, ");
		sql.append("   cu.customer_code,");
		sql.append("   cu.customer_name,");
		sql.append("   cu.address,");
		sql.append("   p.product_code,");
		sql.append("   p.product_name,");
		sql.append("   sod.is_free_item,");
		sql.append("   p.convfact,");
		sql.append("   sod.price,");
		sql.append("   pp.promotion_program_code,");
		sql.append("   pp.promotion_program_name,");
		sql.append("   so.order_number");
//		sql.append("   car.car_number");
		sql.append(" ORDER BY s.staff_code, cu.short_code, ");
		sql.append("   sod.is_free_item,");
		sql.append("   p.product_code");

		String[] fieldNames = { "deliveryCode", // 1
				"deliveryName", // 2
				"customerShortCode",//3
				"customerCode",//4
				"customerName",//5
				"customerAddress",//6
				"productCode", // 7
				"productName", // 8
				"isFreeItem", // 9
				"convfact", // 10
				"quantity", // 11
				"price", // 12
				"amount", // 13
				"discountAmount",//14
				"promotionProgramCode", // 15
				"promotionProgramName", // 16
				"orderNumber" //17
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.STRING, // 7
				StandardBasicTypes.STRING, // 8
				StandardBasicTypes.INTEGER, //9
				StandardBasicTypes.INTEGER, // 10
				StandardBasicTypes.INTEGER, // 11
				StandardBasicTypes.BIG_DECIMAL, // 12
				StandardBasicTypes.BIG_DECIMAL, // 13
				StandardBasicTypes.BIG_DECIMAL, // 14
				StandardBasicTypes.STRING, // 15
				StandardBasicTypes.STRING, // 16
				StandardBasicTypes.STRING//17
		};
		return repo.getListByQueryAndScalar(PrintDeliveryGroupVO1.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<RptSaleOrderDetailVO> getPDTHReport(Long shopId,
			Date fromDate, Date toDate, Long staffId, String deliveryCode)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptSaleOrderDetailVO>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT so.sale_order_id soId, dt.sale_order_detail_id sodId, ");
		sql.append("  p.product_code productCode, ");
		sql.append("  p.product_name productName, ");
		sql.append("  p.convfact convfact, ");
		sql.append("  dt.price price, ");
		sql.append("  dt.quantity quantity, ");
		sql.append("  (dt.price * dt.quantity) amount, ");
		sql.append("  (select promotion_program_name from promotion_program pg where pg.promotion_program_code = dt.program_code) promotionName, ");
		sql.append("  (SELECT promotion_program_code FROM promotion_program pg WHERE pg.promotion_program_code = dt.program_code) promotionCode, ");
		sql.append("  (SELECT car_number FROM car c WHERE c.car_id = so.car_id) carNumber, ");
		sql.append("  (select customer_name from customer cu where cu.customer_id = so.customer_id) customerName, ");
		sql.append("  (select MOBIPHONE from customer cu where cu.customer_id = so.customer_id) customerPhone, ");
		sql.append("  (select SHORT_CODE from customer cu where cu.customer_id = so.customer_id) customerShortCode, ");
		sql.append("  (select address from customer cu where cu.customer_id = so.customer_id) customerAddress, ");
		sql.append("  (select shop_name from shop s where s.shop_id = so.shop_id) shopName, ");
		sql.append("  (select phone from shop s where s.shop_id = so.shop_id) shopPhone, ");
		sql.append("  (select tax_num from shop s where s.shop_id = so.shop_id) shopTaxNum, ");
		sql.append("  (select address from shop s where s.shop_id = so.shop_id) shopAddress, ");
		sql.append("  (select sf.staff_name from staff sf where sf.staff_id = so.delivery_id) deliveryName, ");
		sql.append("  (select sf.staff_code from staff sf where sf.staff_id = so.delivery_id) deliveryCode, ");
		sql.append("  (select sf.staff_name from staff sf where sf.staff_id = so.staff_id) staffName, ");
		sql.append("  (select sf.staff_code from staff sf where sf.staff_id = so.staff_id) staffCode, ");
		sql.append("  dt.invoice_id invoiceId, ");
		sql.append("  so.order_number invoiceNumber, ");
		sql.append("  so.order_date orderDate, ");
		sql.append("  (case when p.convfact is not null and p.convfact > 0 then floor(dt.quantity/p.convfact) else 0 end) numThung, ");
		sql.append("  (case when p.convfact is not null and p.convfact > 0 then mod(dt.quantity,p.convfact) else 0 end) numLe ");
		sql.append("FROM sale_order so ");
		sql.append("JOIN sale_order_detail dt ");
		sql.append("ON dt.sale_order_id  = so.sale_order_id ");
		sql.append("JOIN product p ");
		sql.append("ON p.product_id = dt.product_id ");
		sql.append("WHERE so.approved    =1 ");
		sql.append("AND so.type          =1 ");
		sql.append("AND so.order_type    = 'IN' ");
		sql.append("AND dt.program_type IN (3,5) ");
		if (fromDate != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		if (staffId != null) {
			sql.append(" AND so.staff_id = ?");
			params.add(staffId);
		}
		if (!StringUtility.isNullOrEmpty(deliveryCode)) {
			sql.append(" and exists(select 1 from staff where staff_id = so.DELIVERY_ID and staff_code like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(deliveryCode));
		}
		if (shopId != null) {
			sql.append(" AND so.shop_id = ?");
			params.add(shopId);
		}
		sql.append(" order by so.sale_order_id, so.create_date, p.product_code, dt.sale_order_detail_id");
		
		String[] fieldNames = {
				"soId", "sodId","productCode", "productName", "convfact", 
				"price", "quantity", "amount", "promotionName", "promotionCode", "carNumber",
				"customerName", "customerPhone", "customerShortCode", "customerAddress", 
				"shopName", "shopPhone", "shopTaxNum", "shopAddress", 
				"deliveryName", "deliveryCode", "staffName", "staffCode", 
				"invoiceId", "invoiceNumber","orderDate", "numThung", "numLe"
				};
		Type[] fieldTypes = {
				StandardBasicTypes.LONG,StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.DATE, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER};
		return repo.getListByQueryAndScalar(RptSaleOrderDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	//vuonghn
	@Override
	public List<RptSaleOrderDetail2VO> getPDHCGReport(Long shopId,
			Date fromDate, Date toDate, String deliveryCode)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptSaleOrderDetail2VO>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select so.sale_order_id as soID, sh.shop_name as shopName, sh.address as shopAddress, sh.phone shopPhone,");
		 sql.append(" cu.customer_name as customerName, cu.address as customerAddress,");
		 sql.append(" s.staff_code as staffCode, s.staff_name as staffName,");
		 sql.append(" d.staff_code as deliveryCode, d.staff_name as deliveryName,");
		 sql.append(" so.order_number as orderNumber, so.order_date as orderDate, so.amount as total, so.discount as discount,");
		 sql.append(" p.product_code as productCode, p.product_name as productName,");
		 sql.append(" sod.quantity as quantity,");
		 sql.append(" (case when p.convfact is not null and p.convfact > 0 then floor(sod.quantity/p.convfact) else 0 end) as numThung,");
		 sql.append(" (case when p.convfact is not null and p.convfact > 0 then mod(sod.quantity,p.convfact) else 0 end) as numLe,");
		 sql.append(" sod.price as price,");
		 sql.append(" (sod.price * sod.quantity) as amount");
		 sql.append(" from sale_order so");
		 sql.append(" JOIN sale_order_detail sod");
		 sql.append(" ON so.sale_order_id = sod.sale_order_id");
		 sql.append(" JOIN customer cu");
		 sql.append(" ON cu.customer_id = so.customer_id");
		 sql.append(" JOIN product p");
		 sql.append(" ON p.product_id = sod.product_id");
		 sql.append(" JOIN staff s");
		 sql.append(" ON s.staff_id = so.staff_id");
		 sql.append(" join staff d");
		 sql.append(" on d.staff_id = so.delivery_id");
		 sql.append(" join shop sh");
		 sql.append(" on sh.shop_id = so.shop_id");
		 sql.append(" where ");
		 sql.append(" so.approved = 1");
		 sql.append(" and so.order_type = 'IN'");
		 sql.append(" and so.time_print is null");
		 sql.append(" and sod.is_free_item = 1");
		if (fromDate != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		if (!StringUtility.isNullOrEmpty(deliveryCode)) {
			sql.append(" and exists(select 1 from staff where staff_id = so.DELIVERY_ID and staff_code like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(deliveryCode));
		}
		if (shopId != null) {
			sql.append(" AND so.shop_id = ?");
			params.add(shopId);
		}
		sql.append(" order by p.product_code");
		
		String[] fieldNames = {
				"soID", "shopName","shopAddress", "shopPhone", "customerName", 
				"customerAddress", "staffCode", "staffName", "deliveryCode", "deliveryName",
				"orderNumber", "orderDate", "total", "discount", 
				"productCode", "productName","quantity", "numThung", "numLe", 
				"deliveryName", "deliveryCode", "staffName", "staffCode", 
				"amount","price"
				};
		Type[] fieldTypes = {
				StandardBasicTypes.LONG,StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.DATE, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.BIG_DECIMAL,StandardBasicTypes.BIG_DECIMAL};
		return repo.getListByQueryAndScalar(RptSaleOrderDetail2VO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<PrintDeliveryCustomerGroupVO1> getListPrintDeliveryCustomerGroupVO(
			List<Long> listSaleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT s.staff_code         AS deliveryCode,");
		sql.append("   s.staff_name              AS deliveryName,");
		sql.append("   c.customer_code           AS customerCode,");
		sql.append("   c.customer_name           AS customerName,");
		sql.append("   c.address                 AS customerAddress,");
		sql.append("   car.car_number            AS carNumber,");
		sql.append("   p.product_code            AS productCode,");
		sql.append("   p.product_name            AS productName,");
		sql.append("   sod.is_free_item          AS isFreeItem,");
		sql.append("   p.convfact                AS convfact,");
		sql.append("   SUM(NVL(sod.quantity, 0)) AS quantity,");
		sql.append("   NVL(sod.price, 0)         AS price,");
		sql.append("   SUM(NVL(sod.amount, 0))   AS amount,");
		sql.append("   pp.promotion_program_code AS promotionProgramCode,");
		sql.append("   pp.promotion_program_name AS promotionProgramName,");
		sql.append("   om_tmp.invoiceNumber      AS invoiceNumber");
		sql.append(" FROM sale_order_detail sod");
		sql.append(" JOIN sale_order so");
		sql.append(" ON (so.sale_order_id = sod.sale_order_id");
		sql.append(" AND (1 != 1");
		if (listSaleOrderId != null) {
			for (Long saleOrderId : listSaleOrderId) {
				if (saleOrderId != null) {
					sql.append(" OR so.sale_order_id = ?");
					params.add(saleOrderId);
				}
			}
		}
		sql.append(" ))");
		sql.append(" JOIN staff s");
		sql.append(" ON (s.staff_id = so.delivery_id)");
		sql.append(" JOIN customer c");
		sql.append(" ON (c.customer_id = so.customer_id)");
		sql.append(" JOIN product p");
		sql.append(" ON (p.product_id = sod.product_id)");
		sql.append(" JOIN car");
		sql.append(" ON (car.car_id = so.car_id)");
		sql.append(" LEFT JOIN promotion_program pp");
		sql.append(" ON (sod.program_code  = pp.promotion_program_code");
		sql.append(" AND sod.is_free_item  = 1");
		sql.append(" AND sod.program_type IN (0, 1))");
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT s.staff_code AS staff_code,");
		sql.append("     c.customer_code AS customer_code,");
		sql.append("     car.car_number AS car_number,");
		sql.append("     LISTAGG(inv.invoice_number, ', ') WITHIN GROUP (");
		sql.append("   ORDER BY inv.invoice_number) AS invoiceNumber");
		sql.append("   FROM sale_order so");
		sql.append("   JOIN staff s");
		sql.append("   ON (so.delivery_id = s.staff_id)");
		sql.append("   JOIN customer c");
		sql.append("   ON (so.customer_id = c.customer_id)");
		sql.append("   JOIN car");
		sql.append("   ON (so.car_id = car.car_id)");
		sql.append("   JOIN invoice inv");
		sql.append("   ON (so.invoice_id = inv.invoice_id)");
		sql.append("   WHERE 1 = 1");
		sql.append("   AND (1               != 1");
		if (listSaleOrderId != null) {
			for (Long saleOrderId : listSaleOrderId) {
				if (saleOrderId != null) {
					sql.append(" OR so.sale_order_id = ?");
					params.add(saleOrderId);
				}
			}
		}
		sql.append("   )");
		sql.append("   GROUP BY s.staff_code, c.customer_code, car.car_number");
		sql.append("   ) om_tmp ON (om_tmp.staff_code = s.staff_code AND om_tmp.customer_code = c.customer_code AND om_tmp.car_number = car.car_number)");
		sql.append(" GROUP BY s.staff_code,");
		sql.append("   s.staff_name,");
		sql.append("   p.product_code,");
		sql.append("   p.product_name,");
		sql.append("   c.customer_code,");
		sql.append("   c.customer_name,");
		sql.append("   c.address,");
		sql.append("   car.car_number,");
		sql.append("   sod.is_free_item,");
		sql.append("   p.convfact,");
		sql.append("   sod.price,");
		sql.append("   pp.promotion_program_code,");
		sql.append("   pp.promotion_program_name,");
		sql.append("   om_tmp.invoiceNumber");
		sql.append(" ORDER BY c.customer_code, s.staff_code,");
		sql.append("   sod.is_free_item,");
		sql.append("   p.product_code");

		String[] fieldNames = { "deliveryCode", // 1
				"deliveryName", // 2
				"customerCode", // 3
				"customerName", // 3
				"customerAddress", // 3
				"carNumber", // 3
				"productCode", // 3
				"productName", // 4
				"isFreeItem", // 5
				"convfact", // 6
				"quantity", // 7
				"price", // 8
				"amount", // 9
				"promotionProgramCode", // 10
				"promotionProgramName", // 11
				"invoiceNumber", // 11
		};
		Type[] fieldTypes = { StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.INTEGER, // 5
				StandardBasicTypes.INTEGER, // 6
				StandardBasicTypes.INTEGER, // 7
				StandardBasicTypes.BIG_DECIMAL, // 8
				StandardBasicTypes.BIG_DECIMAL, // 9
				StandardBasicTypes.STRING, // 10
				StandardBasicTypes.STRING, // 11
				StandardBasicTypes.STRING, // 11
		};
		return repo.getListByQueryAndScalar(PrintDeliveryCustomerGroupVO1.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RptExSaleOrderVO> getListSaleOrderByDeliveryStaffGroupByProductCode(
			List<Long> listSaleOrderId, Long shopId)throws DataAccessException{		
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();			
			sql.append(" SELECT pr.product_code productCode,");
			sql.append("   pr.product_name productName,");
			sql.append("   pr.convfact,");
			sql.append("   lot,");
			sql.append("   price,");
			sql.append("   promoQuantity,");
			sql.append("   saleQuantity,");
			sql.append("   (SELECT staff_code || '-' || staff_name");
			sql.append("   FROM staff");
			sql.append("   WHERE staff_id = temp.delivery_id");
			sql.append("   ) deliveryStaffInfo,");
			sql.append("   temp.delivery_id deliveryStaffId,");
			sql.append("   nvl(temp1.discountAmount, 0) discountAmount, temp.order_number orderNumber");
			sql.append(" FROM (");
			sql.append("   (SELECT order_number, delivery_id,");
			sql.append("     product_id,");
			sql.append("     lot,");
			sql.append("     price,");
			sql.append("     SUM(CASE WHEN freeQuanL IS NOT NULL THEN freeQuanL ELSE freeQuanD END) promoQuantity,");
			sql.append("     SUM(CASE WHEN saleQuanD IS NOT NULL THEN saleQuanD ELSE saleQuanD END) saleQuantity");
			sql.append("   FROM (");
			sql.append("     (SELECT so.order_number, sod.product_id,");
			sql.append("       sol.lot,");
			sql.append("       sod.price,");
			sql.append("       so.delivery_id,");
			sql.append("       sod.quantity saleQuanD,");
			sql.append("       sol.quantity saleQuanL,");
			sql.append("       0 freeQuanD,");
			sql.append("       0 freeQuanL");
			sql.append("     FROM sale_order so");
			sql.append("     JOIN sale_order_detail sod");
			sql.append("     ON so.sale_order_id = sod.sale_order_id");
			sql.append("     LEFT JOIN sale_order_lot sol");
			sql.append("     ON sol.sale_order_detail_id = sod.sale_order_detail_id");
			sql.append("     WHERE approved     = 1 ");
			sql.append("     AND type                    = 1");
			
			sql.append(" and so.sale_order_id in ( ");
			String strParams =""; 
			strParams  += "?";
			params.add(listSaleOrderId.get(0));
			for(int i = 1,size=listSaleOrderId.size(); i < size; i++){
				strParams += ",?";
				params.add(listSaleOrderId.get(i));
			}
			sql.append(strParams);
			sql.append(" ) ");
			
			sql.append("     )");
			sql.append("   UNION");
			sql.append("     (SELECT so.order_number, sod.product_id,");
			sql.append("       sol.lot,");
			sql.append("       sod.price,");
			sql.append("       so.delivery_id,");
			sql.append("       0 saleQuanD,");
			sql.append("       0 saleQuanL,");
			sql.append("       sod.quantity freeQuanD,");
			sql.append("       sol.quantity freeQuanL");
			sql.append("     FROM sale_order so");
			sql.append("     JOIN sale_order_detail sod");
			sql.append("     ON so.sale_order_id = sod.sale_order_id");
			sql.append("     LEFT JOIN sale_order_lot sol");
			sql.append("     ON sol.sale_order_detail_id = sod.sale_order_detail_id");
			sql.append("     WHERE approved    = 1 ");
			sql.append("     AND type                    = 1");
			sql.append("     AND is_free_item            = 1");
			sql.append(" and so.sale_order_id in ( ");
			strParams =""; 
			strParams  += "?";
			params.add(listSaleOrderId.get(0));
			for(int i = 1,size=listSaleOrderId.size(); i < size; i++){
				strParams += ",?";
				params.add(listSaleOrderId.get(i));
			}
			sql.append(strParams);
			sql.append(" ) ");
			
			sql.append("     AND order_type             IN ('IN', 'TT')");
			sql.append("     AND program_type NOT       IN (3,4,5)");
			sql.append("     AND sod.quantity            > 0");
			if (shopId != null) {
				sql.append(" and so.shop_id = ? ");
				params.add(shopId);
			}
			sql.append("     ) )");
			sql.append("   WHERE price IS NOT NULL and delivery_id is not null");
			sql.append("   GROUP BY order_number, delivery_id,");
			sql.append("     product_id,");
			sql.append("     lot,");
			sql.append("     price");
			sql.append("   ) temp");
			sql.append(" LEFT JOIN");
			sql.append("   (SELECT sod.product_id,");
			sql.append("     so.delivery_id,");
			sql.append("     SUM(NVL(discount_amount, 0)) discountAmount");
			sql.append("   FROM sale_order so");
			sql.append("   JOIN sale_order_detail sod");
			sql.append("   ON so.sale_order_id   = sod.sale_order_id");
			sql.append("   WHERE approved      = 1 ");
			sql.append("   AND type              = 1");
			sql.append(" and so.sale_order_id in ( ");
			strParams =""; 
			strParams  += "?";
			params.add(listSaleOrderId.get(0));
			for(int i = 1,size=listSaleOrderId.size(); i < size; i++){
				strParams += ",?";
				params.add(listSaleOrderId.get(i));
			}
			sql.append(strParams);
			sql.append(" ) ");
			sql.append("   AND is_free_item      = 1");
			sql.append("   AND order_type       IN ('IN', 'TT')");
			sql.append("   AND program_type NOT IN (3,4,5)");
			sql.append("   AND sod.quantity      > 0");
			sql.append("   GROUP BY so.delivery_id,");
			sql.append("     sod.product_id");
			sql.append("   ) temp1 ON temp.product_id = temp1.product_id AND temp.delivery_id         = temp1.delivery_id )");
			sql.append(" JOIN product pr");
			sql.append(" ON temp.product_id = pr.product_id");
			sql.append(" ORDER BY temp.delivery_id,");
			sql.append("   pr.product_code");
			
			
			String[] fieldNames = { "productCode", 
					"productName", 
					"convfact", 
					"lot", 
					"price", 
					"promoQuantity", 
					"saleQuantity", "deliveryStaffInfo", "deliveryStaffId", "discountAmount", "orderNumber"};
			
			Type[] fieldTypes = { 
					StandardBasicTypes.STRING,
					StandardBasicTypes.STRING, 
					StandardBasicTypes.INTEGER,
					StandardBasicTypes.STRING, 
					StandardBasicTypes.BIG_DECIMAL, 
					StandardBasicTypes.INTEGER,
					StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL,
					StandardBasicTypes.STRING};
			
			return repo.getListByQueryAndScalar(RptExSaleOrderVO.class,
					fieldNames, fieldTypes, sql.toString(), params);		
			
	}

	@Override
	public List<SaleOrderDetail> getListAllSaleOrderDetail(long saleOrderId,
			KPaging<SaleOrderDetail> kPaging, Date orderDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_detail");
		sql.append(" where sale_order_id = ? and invoice_id is null ");
		sql.append(" and product_id is not null");
		sql.append(" and order_date >= TRUNC(?)");
		sql.append(" and order_date < TRUNC(?) + 1");
		params.add(saleOrderId);
		params.add(orderDate);
		params.add(orderDate);
		sql.append(" order by vat asc,program_code, is_free_item desc, sale_order_detail_id");
		
		if (kPaging == null) {
			return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(SaleOrderDetail.class, sql.toString(), params, kPaging);
		}
	}

	@Override
	public List<SaleOrderDetail> getListSaleProductBySaleOrderId(
			long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_detail");
		sql.append(" where sale_order_id = ?");
		params.add(saleOrderId);
		sql.append(" and is_free_item = 0");
		sql.append(" order by product_id");
		return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
	}
	
	@Override
	public List<SaleOrderDetail> getListDetailByInvoiceAndInvoceDetail(
			Long invoiceId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from Sale_Order_Detail");
		sql.append(" where Sale_Order_Detail_Id in(");
		sql.append(" select Sale_Order_Detail_Id");
		sql.append(" from Invoice_Detail where Invoice_Id = ?)");
		params.add(invoiceId);
		return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
	}
	
	@Override
	public List<SaleOrderDetailVOEx> getListSaleOrderDetailByOrderId(long saleOrderId, Integer isFreeItem, ProgramType programType,
			Boolean isAutoPromotion, Boolean isGetCTTL) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select p.product_id as productId, p.product_code as productCode, p.product_name as productName,");
		sql.append(" (select price from price where price_id = sod.price_id) as price,");
		sql.append(" (select warehouse_name from warehouse where warehouse_id = sol.warehouse_id) as warehouseName,");
		sql.append(" sol.quantity, p.convfact,");
		//sql.append(" (select available_quantity from stock_total where stock_total_id = sol.stock_total_id) as availableQuantity,");
		//sql.append(" (select quantity from stock_total where stock_total_id = sol.stock_total_id) as stockQuantity,");
		sql.append(" (select sum(available_quantity) from stock_total");
		sql.append(" where product_id = sol.product_id and object_type = 1 and object_id = so.shop_id and status = 1) as availableQuantity,");
		sql.append(" (select sum(quantity) from stock_total");
		sql.append(" where product_id = sol.product_id and object_type = 1 and object_id = so.shop_id and status = 1) as stockQuantity,");
		sql.append(" sol.discount_amount as discountAmount,");
		sql.append(" sod.program_code as programCode, sod.program_type as ppType,");
		sql.append(" nvl(p.gross_weight, 0) as grossWeight,");
		sql.append(" nvl(sod.total_weight, 0) as totalWeight,");
		sql.append(" sod.is_free_item as isFreeItem, ");
		sql.append(" sod.programe_type_code as programTypeCode ");
		sql.append(" from sale_order_lot sol");
		sql.append(" join sale_order_detail sod on (sod.sale_order_detail_id = sol.sale_order_detail_id)");
		sql.append(" join sale_order so on (so.sale_order_id = sod.sale_order_id)");
		sql.append(" left join product p on (p.product_id = sol.product_id and p.status = 1)");
		sql.append(" where sod.sale_order_id = ?");
		params.add(saleOrderId);
		
		if (isFreeItem != null) {
			sql.append(" and sod.is_free_item = ?");
			params.add(isFreeItem);
		}
		
		if (programType != null) {
			sql.append(" and sod.program_type =  ?");
			params.add(programType.getValue());
		}
		
		if (Boolean.TRUE.equals(isAutoPromotion)) {
			sql.append(" and sod.program_type = ? and sod.is_free_item = 1 ");
			params.add(ProgramType.AUTO_PROM.getValue());
		} else if (Boolean.FALSE.equals(isAutoPromotion)) {
			sql.append(" and (sod.program_type <> ? or sod.is_free_item = 0)");
			params.add(ProgramType.AUTO_PROM.getValue());
		}
		
		if(Boolean.TRUE.equals(isGetCTTL)){
			sql.append(" union all ");
			sql.append(" select 0 AS productId, null AS productCode, null AS productName, 0 AS price, null as warehouseName,");
			sql.append("  round(sum(spd.discount_amount)) as quantity, 0 as convfact, 0 AS availableQuantity, 0 AS stockQuantity, sum(spd.discount_amount) AS discountAmount,");
			sql.append(" spd.program_code AS programCode, 0 AS ppType, 0 AS grossWeight, 0 as totalWeight, 1 AS isFreeItem, spd.PROGRAME_TYPE_CODE programTypeCode ");
			sql.append(" from sale_order_promo_detail spd where spd.sale_order_id = ? ");
			params.add(saleOrderId);
			sql.append(" group by spd.program_code, spd.PROGRAME_TYPE_CODE ");
		}
		
		sql.append(" order by productCode");
		
		String[] fieldNames = new String[] {
				"productId",
				"productCode", "productName",
				"price",
				"warehouseName",
				"quantity", "convfact",
				"availableQuantity", "stockQuantity",
				"discountAmount",
				"programCode",
				"ppType",
				"grossWeight",
				"totalWeight",
				"isFreeItem", "programTypeCode"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.FLOAT,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER,StandardBasicTypes.STRING
		};
		
		List<SaleOrderDetailVOEx> lst = repo.getListByQueryAndScalar(SaleOrderDetailVOEx.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	@Override
	public List<SaleOrderPromoDetail> getListSaleOrderPromoDetail(
			Long saleOrderDetailId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_promo_detail where sale_order_detail_id  = ? ");
		params.add(saleOrderDetailId);
		return repo.getListBySQL(SaleOrderPromoDetail.class, sql.toString(), params);
	}

	@Override
	public List<SaleOrderDetail> getSaleOrderDetails(SaleOrderDetailFilter<SaleOrderDetail> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/*
		 * sql.append(" select * ");
		 * sql.append(" from sale_order_promotion sop ");
		 * sql.append(" where 1 = 1 "); if (filter.getSaleOrderId() != null) {
		 * sql.append(" and sop.sale_order_id = ? ");
		 * params.add(filter.getSaleOrderId()); } if
		 * (!StringUtility.isNullOrEmpty(filter.getProgramCode())) {
		 * sql.append(" and sop.promotion_program_code = ? ");
		 * params.add(filter.getProgramCode()); } if (filter.getPromoLevel() !=
		 * null) { sql.append(" and sop.promotion_level = ? ");
		 * params.add(filter.getPromoLevel()); }
		 */
		sql.append(" select * ");
		sql.append(" from sale_order_detail sod ");
		sql.append(" where 1 = 1 ");
		if (filter.getSaleOrderId() != null) {
			sql.append(" and sod.sale_order_id = ? ");
			params.add(filter.getSaleOrderId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getProgramCode())) {
			sql.append(" and sod.program_code = ? ");
			params.add(filter.getProgramCode());
		}
		if (filter.getProgramType() != null) {
			sql.append(" and sod.PROGRAM_TYPE = ? ");
			params.add(filter.getProgramType().getValue());
		}
		if (filter.getPromoLevel() != null) {
			sql.append(" and sod.promotion_order_number = ? ");
			params.add(filter.getPromoLevel());
		}
		if (filter.getLevelId() != null) {
			sql.append(" and sod.group_level_id = ? ");
			params.add(filter.getLevelId());
		}
		return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrderLotVO> getSaleOrderLotVOOfOrder(long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select sol.sale_order_lot_id as id, sol.sale_order_detail_id as saleOrderDetailId,");
		sql.append(" sol.sale_order_id as saleOrderId, sol.warehouse_id as warehouseId,");
		sql.append(" (select warehouse_name from warehouse where warehouse_id = sol.warehouse_id) as warehouseName,");
		sql.append(" (select available_quantity from stock_total where object_type = ? and object_id = sol.shop_id");
		params.add(StockObjectType.SHOP.getValue());
		sql.append(" and warehouse_id = sol.warehouse_id and product_id = sol.product_id and status = ?) as availableQuantity,");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" sol.shop_id as shopId,");
		sql.append(" sol.product_id as productId, sol.quantity as quantity, sol.staff_id as staffId,");
		sql.append(" sol.price as price, sol.discount_amount as discountAmount, sol.discount_percent as discountPercent");
		sql.append(" from sale_order_lot sol");
		sql.append(" where sol.sale_order_id = ?");
		params.add(saleOrderId);
		
		String[] fieldNames = {
				"id",
				"saleOrderDetailId",
				"saleOrderId",
				"warehouseId",
				"warehouseName",
				"shopId",
				"productId",
				"quantity",
				"availableQuantity",
				"staffId",
				"price",
				"discountAmount",
				"discountPercent"	
		};
		
		Type[] fieldTypes = {
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.LONG,
				StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL
		};
		
		List<SaleOrderLotVO> lst = repo.getListByQueryAndScalar(SaleOrderLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrderPromoDetail> getListSaleOrderPromoDetailOfOrder(long saleOrderId, String programCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from sale_order_promo_detail");
		sql.append(" where sale_order_id = ?");
		params.add(saleOrderId);
		if (!StringUtility.isNullOrEmpty(programCode)) {
			sql.append(" and program_code = ?");
			params.add(programCode);
		}
		sql.append(" order by program_type");
		List<SaleOrderPromoDetail> lst = repo.getListBySQL(SaleOrderPromoDetail.class, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailZV23(long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from sale_order_detail");
		sql.append(" where sale_order_id = ? and is_free_item = 1");
		params.add(saleOrderId);
		sql.append(" and programe_type_code = ?");
		params.add(PromotionType.ZV23.getValue());
		
		sql.append(" order by program_code, paying_order");
		
		List<SaleOrderDetail> lst = repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
		return lst;
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrderLotVOEx> getListOrderDetailVOByOrderId(long orderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select sod.sale_order_detail_id saleOrderDetailId, p.product_code as productCode, p.product_name as productName,");
		sql.append(" p.convfact, sod.price,");
		sql.append(" sod.package_price packagePrice,");
		sql.append(" NVL(sod.quantity_package, 0) quantityPackage,");
		sql.append(" NVL(sod.quantity_retail, 0) quantityRetail,");
		sql.append(" (select warehouse_name from warehouse where warehouse_id = sol.warehouse_id) as warehouseName,");
		sql.append(" sum(sol.quantity) as quantity,");
		sql.append(" sum(sol.discount_amount) as discountAmount,");
		sql.append(" sod.program_code as programCode,");
		sql.append(" sod.is_free_item as isFreeItem, sod.program_type as programType,");
		sql.append(" sod.programe_type_code as programTypeCode,");
		sql.append(" sod.max_quantity_free as maxQuantityFree,");
		sql.append(" sod.promotion_order_number as levelPromo,");
		sql.append(" sod.join_program_code as joinProgramCode,");
		sql.append(" (select quantity_received from sale_order_promotion");
		sql.append(" where sale_order_id = sod.sale_order_id and group_level_id = sod.group_level_id");
		sql.append(" and sod.is_free_item = 1 and sod.program_type = 0 and rownum = 1");
		sql.append(" ) as quantityReceived,");
		sql.append(" (select quantity_received_max from sale_order_promotion");
		sql.append(" where sale_order_id = sod.sale_order_id and group_level_id = sod.group_level_id");
		sql.append(" and sod.is_free_item = 1 and sod.program_type = 0 and rownum = 1");
		sql.append(" ) as maxQuantityReceived,");
		sql.append(" sod.product_group_id as productGroupId,");
		sql.append(" sod.paying_order as payingOrder");
		sql.append(" from sale_order_detail sod");
		sql.append(" join sale_order_lot sol on (sol.sale_order_detail_id = sod.sale_order_detail_id)");
		sql.append(" join product p on (p.product_id = sod.product_id)");
		sql.append(" where sod.sale_order_id = ?");
		params.add(orderId);
		sql.append(" group by sod.sale_order_id, sod.sale_order_detail_id, p.product_code, p.product_name, p.convfact, sod.price, sod.package_price, sod.quantity_package, sod.quantity_retail, sol.warehouse_id,");
		sql.append(" sod.program_code, sod.is_free_item, sod.program_type, sod.programe_type_code,");
		sql.append(" sod.max_quantity_free, sod.product_group_id, sod.group_level_id, sod.promotion_order_number, sod.join_program_code, sod.paying_order");
		
		sql.append(" order by programCode, levelPromo, productGroupId, productCode, payingOrder");
		
		String[] fieldNames = {
				"saleOrderDetailId",
				"productCode",
				"productName",
				"convfact",
				"price",
				"packagePrice",
				"quantityPackage",
				"quantityRetail",
				"warehouseName",
				"quantity",
				"discountAmount",
				"programCode",
				"isFreeItem",
				"programType",
				"programTypeCode",
				"maxQuantityFree",
				"levelPromo",
				"joinProgramCode",
				"quantityReceived",
				"maxQuantityReceived",
				"productGroupId",
				"payingOrder"
		};
		
		Type[] fieldTypes = {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER
		};
		
		List<SaleOrderLotVOEx> lst = repo.getListByQueryAndScalar(SaleOrderLotVOEx.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
	
	/**
	 * @author trietptm
	 */
	@Override
	public List<SaleOrderDetailVO2> getListDiscountAmountByProgramCode(@SuppressWarnings("rawtypes") SaleOrderDetailFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select PROGRAM_CODE programCode, SUM(QUANTITY) quantity from sale_order_detail ");
		sql.append(" where 1 = 1 ");
		if (filter.getSaleOrderId() != null) {
			sql.append(" AND SALE_ORDER_ID = ? ");
			params.add(filter.getSaleOrderId());
		}
		if (filter.getShopId() != null ){
			sql.append(" AND SHOP_ID = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getProgramType() != null) {
			sql.append(" AND PROGRAM_TYPE = ? ");
			params.add(filter.getProgramType().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getProgrameTypeCode())) {
			sql.append(" AND PROGRAME_TYPE_CODE = ? ");
			params.add(filter.getProgrameTypeCode());
		}
		if (Boolean.TRUE.equals(filter.getIsNotKeyShop())) {
			sql.append(" AND PROGRAM_TYPE <> ? ");
			params.add(ProgramType.KEY_SHOP.getValue());
		}
		sql.append(" GROUP BY SALE_ORDER_ID, PROGRAM_CODE ");
		String[] fieldNames = {
				"programCode",
				"quantity"
		};
		
		Type[] fieldTypes = {
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER
		};
		
		List<SaleOrderDetailVO2> lst = repo.getListByQueryAndScalar(SaleOrderDetailVO2.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrderLotVOEx> getListOrderPromoDetailByOrderId(long orderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select sum(sopd.discount_amount) as discountAmount,");
		sql.append(" sopd.program_code as programCode,");
		sql.append(" sopd.program_type as programType,");
		sql.append(" sopd.programe_type_code as programTypeCode,");
		sql.append(" sum(sopd.max_amount_free) as maxAmountFree,");
		sql.append(" sopd.product_group_id as productGroupId,");
		sql.append(" (select order_number from group_level where group_level_id = sopd.group_level_id) as levelPromo");
		sql.append(" from sale_order_promo_detail sopd");
		sql.append(" where sopd.is_owner = 2");
		sql.append(" and sopd.sale_order_id = ?");
		params.add(orderId);
		sql.append(" group by sopd.program_code, sopd.program_type, sopd.programe_type_code,");
		sql.append(" sopd.product_group_id, sopd.group_level_id");
		
		sql.append(" order by programCode, levelPromo, productGroupId");
		
		String[] fieldNames = {
				"discountAmount",
				"programCode",
				"programType",
				"programTypeCode",
				"maxAmountFree",
				"productGroupId",
				"levelPromo"
		};
		
		Type[] fieldTypes = {
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER
		};
		
		List<SaleOrderLotVOEx> lst = repo.getListByQueryAndScalar(SaleOrderLotVOEx.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
}
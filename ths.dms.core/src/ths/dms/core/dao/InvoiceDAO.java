package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Invoice;
import ths.dms.core.entities.InvoiceDetail;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.InvoiceCustPayment;
import ths.dms.core.entities.enumtype.InvoiceFilter;
import ths.dms.core.entities.enumtype.InvoiceStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.core.entities.vo.rpt.RptAggegateVATInvoiceDataVO;
import ths.core.entities.vo.rpt.RptInvoiceComparingVO;
import ths.core.entities.vo.rpt.RptViewInvoiceVO;
import ths.dms.core.exceptions.DataAccessException;

public interface InvoiceDAO {

	Invoice createInvoice(Invoice invoice) throws DataAccessException;
	
	InvoiceDetail createInvoiceDetail(InvoiceDetail invoiceDetail) throws DataAccessException;

	void deleteInvoice(Invoice invoice) throws DataAccessException;

	void updateInvoice(Invoice invoice) throws DataAccessException;

	Invoice getInvoiceById(long id) throws DataAccessException;
	
	void updateInvoice(List<Invoice> lstInvoice)
			throws DataAccessException;

	/**
	 * Lay du lieu cho bao cao Lay ban ke hoa don GTGT
	 * 
	 * @param shopId
	 * @param customerId
	 * @param staffId
	 * @param VAT
	 * @param status
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<RptAggegateVATInvoiceDataVO> getDataForAggegateVATInvoiceReport(
			Long shopId, List<Long> customerId, Long staffId, Integer VAT,
			InvoiceStatus status, Date fromDate, Date toDate)
			throws DataAccessException;

	List<RptInvoiceComparingVO> getListRptInvoiceComparingVO(
			KPaging<RptInvoiceComparingVO> kPaging, Float vat,
			String customerCode, Long staffId, Date fromDate, Date toDate,
			Long shopId, ActiveType status) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param lstInvoiceNumber
	 * @return 
	 * @throws DataAccessException
	 */
	List<Invoice> getListInvoiceByLstInvoiceNumber(Long shopId, List<String> lstInvoiceNumber)
			throws DataAccessException;
	
	Invoice getInvoiceByInvoiceNumber(String invoiceNumber, Long shopId) throws DataAccessException;

	/**
	 * Check duplicate invoice number.
	 * @author phut
	 * @param shopId the shop id
	 * @param lstInvoiceNumber the lst invoice number
	 * @return the invoice
	 * @throws DataAccessException the data access exception
	 * @author phut
	 * @since Jan 7, 2013
	 */
	boolean checkDuplicateInvoiceNumber(Long shopId,
			List<String> lstInvoiceNumber) throws DataAccessException;
	
	List<RptViewInvoiceVO> getRptViewInvoice(KPaging<RptViewInvoiceVO> kPaging, Float vat, String customerCode, List<Long> listStaffId, Date fromDate,
			Date toDate, InvoiceCustPayment custPayment, String selName, String orderNumber, Long shopId, String invoiceNumber)
			throws DataAccessException;

	List<Invoice> getListInvoiceByCondition(KPaging<Invoice> kPaging, Float taxValue, String orderNumber, String redInvoiceNumber, Date fromDate, Date toDate, String staffCode, String deliveryCode, String shortCode,
			InvoiceCustPayment custPayment, Date lockDate, Long shopId, Integer isValueOrder,Boolean isVATGop, Integer numberValueOrder) throws DataAccessException;

	List<Invoice> getListInvoice(InvoiceFilter filter)
			throws DataAccessException;

	List<Invoice> getListInvoiceByLstInvoiceId(List<Long> lstInvoiceId)
			throws DataAccessException;

	List<Invoice> getListInvoiceByLstInvoiceIdEx(List<Long> lstInvoiceId)
			throws DataAccessException;

	List<Invoice> getListInvoiceBySaleOrderId(Long saleOrderId) throws DataAccessException;
	List<Invoice> getListInvoiceBySaleOrderId2(long saleOrderId, int invoiceType) throws DataAccessException;
	

	/**
	 * Lấy danh sách hóa đơn hủy của đơn hàng hủy hóa đơn 1 phần
	 * 
	 * @author tungtt21
	 * @param kPaging
	 * @param taxValue
	 * @param orderNumber
	 * @param redInvoiceNumber
	 * @param fromDate
	 * @param toDate
	 * @param staffCode
	 * @param deliveryCode
	 * @param shortCode
	 * @param custPayment
	 * @param lockDate
	 * @param shopId
	 * @param isValueOrder
	 * @return
	 * @throws DataAccessException
	 */
	List<Invoice> getListInvoiceCanceledByCondition(KPaging<Invoice> kPaging, Float taxValue, String orderNumber, String redInvoiceNumber, Date fromDate, Date toDate, String staffCode, String deliveryCode, String shortCode,
			InvoiceCustPayment custPayment, Date lockDate, Long shopId, Integer isValueOrder) throws DataAccessException;
	
	/**
	 * Kiem tra xem Don hang SaleOrder co hoa don nao huy khong.
	 * @author tungtt21
	 * @param saleOrderId
	 * @param lockDate
	 * @return
	 * @throws DataAccessException
	 */
	List<Invoice> checkHasInvoiceRemoveOfSaleOrder(Long saleOrderId) throws DataAccessException;
	
	/**
	 * Lấy danh sách hóa đơn hủy của đơn hàng hủy hóa đơn 1 phần
	 * 
	 * @author tungtt21
	 * @param kPaging
	 * @param shopId
	 * @param saleStaffCode
	 * @param deliveryStaffCode
	 * @param orderNumber
	 * @param fromDate
	 * @param toDate
	 * @param customerCode
	 * @param lockDate
	 * @param isValueOrder
	 * @return
	 * @throws DataAccessException
	 */
	List<Invoice> getListInvoiceCanceledByConditionEx1(KPaging<SaleOrder> kPaging, Long shopId,
			String saleStaffCode, String deliveryStaffCode,
			String orderNumber, Date fromDate, Date toDate, String customerCode, Date lockDate, Integer isValueOrder, Integer numberValueOrder)
			throws DataAccessException;
	
	/**
	 * 
	 * 
	 * @author phuongvm
	 * @param invoiceId
	 * @return
	 * @throws DataAccessException
	 */
	List<InvoiceDetail> getListInvoiceDetailByInvoiceId(Long invoiceId)
			throws DataAccessException;

	void updateInvoiceDetail(List<InvoiceDetail> lstInvoiceDetail)
			throws DataAccessException;

	List<Invoice> getListInvoiceByConditionByNumber(KPaging<Invoice> kPaging, Float taxValue, String orderNumber, String redInvoiceNumber, Date fromDate, Date toDate, String staffCode, String deliveryCode, String shortCode,
			InvoiceCustPayment custPayment, Date lockDate, Long shopId, Integer isValueOrder, Boolean isVATGop, Integer numberValueOrder)throws DataAccessException;
}

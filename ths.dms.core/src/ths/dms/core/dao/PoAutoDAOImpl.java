package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PoAuto;
import ths.dms.core.entities.PoAutoGroupShopMap;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.PoAuto01VO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class PoAutoDAOImpl implements PoAutoDAO {

	@Autowired
	private IRepository repo;

	@Override
	public PoAuto createPoAuto(PoAuto poAuto) throws DataAccessException {
		if (poAuto == null) {
			throw new IllegalArgumentException("poAuto");
		}
		repo.create(poAuto);
		return repo.getEntityById(PoAuto.class, poAuto.getId());
	}

	@Override
	public void deletePoAuto(PoAuto poAuto) throws DataAccessException {
		if (poAuto == null) {
			throw new IllegalArgumentException("poAuto");
		}
		repo.delete(poAuto);
	}

	@Override
	public PoAuto getPoAutoById(Long id) throws DataAccessException {
		return repo.getEntityById(PoAuto.class, id);
	}
	
	@Override
	public PoAutoGroupShopMap getPoAutoGroupShopMapById(Long id) throws DataAccessException {
		return repo.getEntityById(PoAutoGroupShopMap.class, id);
	}
	
	@Override
	public PoAuto getPoAutoByIdForUpdate(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_AUTO where po_auto_id = ?");
		params.add(id);
		return repo.getEntityBySQL(PoAuto.class, sql.toString(), params);
	}

	@Override
	public void updatePoAuto(PoAuto poAuto) throws DataAccessException {
		if (poAuto == null) {
			throw new IllegalArgumentException("poAuto");
		}
		repo.update(poAuto);
	}

	@Override
	public void updatePoAuto(List<PoAuto> listPoAuto)
			throws DataAccessException {
		if (listPoAuto == null) {
			throw new IllegalArgumentException("poAuto");
		}
		repo.update(listPoAuto);
	}

	@Override
	public List<PoAuto> getListPoVnm(KPaging<PoAuto> kPaging, Long shopId,
			String poNumber, Date fromDate, Date toDate, ApprovalStatus status)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<PoAuto>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PO_AUTO where 1 = 1");

		if (shopId != null) {
			sql.append(" and SHOP_ID = ?");
			params.add(shopId);
		}
		if (poNumber != null) {
			sql.append(" and PO_AUTO_NUMBER = ?");
			params.add(poNumber);
		}
		if (fromDate != null) {
			sql.append(" and PO_AUTO_DATE >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and PO_AUTO_DATE < (trunc(?) + 1)");
			params.add(toDate);
		}
		if (null != status) {
			sql.append(" and STATUS = ?");
			params.add(status.getValue());
		}

		sql.append(" order by PO_AUTO_ID desc");

		if (kPaging == null)
			return repo.getListBySQL(PoAuto.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(PoAuto.class, sql.toString(),
					params, kPaging);
	}

	@Override
	public List<PoAuto01VO> getListPoAuto01(Long shopId, String poAutoNumber,
			Date fromDate, Date toDate, ApprovalStatus status, boolean hasFindChildShop)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<PoAuto01VO>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT PO.PO_AUTO_ID AS poAutoId, PO.PO_AUTO_NUMBER AS poAutoNumber, PO.SHOP_ID AS shopId, S.SHOP_CODE AS shopCode, s.shop_name as shopName, PO.STATUS AS status, PO.PO_AUTO_DATE AS poAutoDate, PO.AMOUNT AS amount,(SELECT SUM (QUANTITY) FROM PO_AUTO_DETAIL WHERE PO_AUTO_ID = PO.PO_AUTO_ID) AS sumQuantity FROM PO_AUTO PO LEFT JOIN SHOP S ON PO.SHOP_ID = S.SHOP_ID where 1 = 1");

		if (shopId != null) {
			if(hasFindChildShop){
				sql.append(" and PO.SHOP_ID IN (SELECT SHOP_ID FROM SHOP START WITH SHOP_ID = ? CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID)");
			}
			else{
				sql.append(" and PO.SHOP_ID = ?");	
			}
			
			params.add(shopId);
		}
		if (poAutoNumber != null) {
			sql.append(" and upper(PO.PO_AUTO_NUMBER) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(poAutoNumber.toUpperCase()));
		}
		if (fromDate != null) {
			sql.append(" and PO.PO_AUTO_DATE >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and PO.PO_AUTO_DATE < (trunc(?) + 1)");
			params.add(toDate);
		}
		if (null != status) {
			sql.append(" and PO.STATUS = ?");
			params.add(status.getValue());
		}

		sql.append(" order by poAutoNumber desc");//status desc, shopCode,

		final String[] fieldNames = new String[] {//
		"poAutoId",// 1
				"poAutoNumber",// 2
				"shopId",// 3
				"shopCode",// 4
				"shopName",// 5
				"status",// 6
				"poAutoDate",// 7
				"sumQuantity",// 8
				"amount"// 9
		};

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.LONG,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.INTEGER,// 6
				StandardBasicTypes.TIMESTAMP,// 7
				StandardBasicTypes.LONG, // 8
				StandardBasicTypes.BIG_DECIMAL // 9
		};

		return repo.getListByQueryAndScalar(PoAuto01VO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.PoAutoDAO#getListPoAutoForNCC(ths.dms.core.entities.enumtype.KPaging, java.lang.Long, java.lang.String, java.util.Date, java.util.Date, java.util.List, boolean)
	 */
	@Override
	public List<PoAuto01VO> getListPoAutoForNCC(KPaging<PoAuto01VO> kPaging,
			Long shopId, String poAutoNumber, Date fromDate, Date toDate,
			List<ApprovalStatus> listStatus, boolean hasFindChildShop)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<PoAuto01VO>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT PO.PO_AUTO_ID AS poAutoId, PO.PO_AUTO_NUMBER AS poAutoNumber, PO.SHOP_ID AS shopId, S.SHOP_CODE AS shopCode, S.SHOP_NAME AS shopName, PO.STATUS AS status, PO.PO_AUTO_DATE AS poAutoDate, PO.AMOUNT AS amount,(SELECT SUM (QUANTITY) FROM PO_AUTO_DETAIL WHERE PO_AUTO_ID = PO.PO_AUTO_ID) AS sumQuantity FROM PO_AUTO PO LEFT JOIN SHOP S ON PO.SHOP_ID = S.SHOP_ID where 1 = 1");

		if (shopId != null) {
			if(hasFindChildShop){
				sql.append(" and PO.SHOP_ID IN (SELECT SHOP_ID FROM SHOP START WITH SHOP_ID = ? CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID)");
			}
			else{
				sql.append(" and PO.SHOP_ID = ?");	
			}
			
			params.add(shopId);
		}
		if (poAutoNumber != null) {
			sql.append(" and upper(PO.PO_AUTO_NUMBER) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(poAutoNumber.toUpperCase()));
		}
		if (fromDate != null) {
			sql.append(" and PO.PO_AUTO_DATE >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and PO.PO_AUTO_DATE < (trunc(?) + 1)");
			params.add(toDate);
		}
		if (null != listStatus && listStatus.size() > 0) {
			sql.append(" and (");
			for (ApprovalStatus status: listStatus) {
				sql.append(" PO.STATUS = ? or");
				params.add(status.getValue());
			}
			sql.append(" PO.STATUS = -9)");
		}
		sql.append(" order by status desc, shopCode, poAutoNumber desc");

		final String[] fieldNames = new String[] {//
		"poAutoId",// 1
				"poAutoNumber",// 2
				"shopId",// 3
				"shopCode",// 4
				"shopName",// 4'
				"status",// 5
				"poAutoDate",// 6
				"sumQuantity",// 7
				"amount"// 8
		};

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.LONG,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 4'
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.TIMESTAMP,// 6
				StandardBasicTypes.LONG, // 7
				StandardBasicTypes.BIG_DECIMAL // 8
		};
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(PoAuto01VO.class, fieldNames,
				fieldTypes, sql.toString(), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(PoAuto01VO.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, kPaging);
		}
	}
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StDisplayPdGroupDtl;
import ths.dms.core.entities.StDisplayPlDpDtl;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;


public class DisplayProductGroupDTLDAOImpl implements DisplayProductGroupDTLDAO{
	@Autowired
	private IRepository repo;
	
	@Autowired
	CommonDAO commonDAO;

	@Override
	public StDisplayPdGroupDtl createDisplayProductGroupDTL(
			StDisplayPdGroupDtl displayProductGroupDTL, LogInfoVO logInfo)
			throws DataAccessException {
		if (displayProductGroupDTL == null) {
			throw new IllegalArgumentException("displayProductGroupDTL is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		displayProductGroupDTL.setCreateUser(logInfo.getStaffCode());
		repo.create(displayProductGroupDTL);
		return repo.getEntityById(StDisplayPdGroupDtl.class, displayProductGroupDTL.getId());
	}

	@Override
	public void deleteDisplayProductGroupDTL(
			StDisplayPdGroupDtl displayProductGroupDTL, LogInfoVO logInfo)
			throws DataAccessException {
		if (displayProductGroupDTL == null) {
			throw new IllegalArgumentException("displayProductGroupDTL");
		}
		repo.delete(displayProductGroupDTL);		
		
	}

	@Override
	public void updateDisplayProductGroupDTL(
			StDisplayPdGroupDtl displayProductGroupDTL, LogInfoVO logInfo)
			throws DataAccessException {
		if (displayProductGroupDTL == null) {
			throw new IllegalArgumentException("displayProductGroupDTL is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}		
		displayProductGroupDTL.setUpdateDate(commonDAO.getSysDate());
		displayProductGroupDTL.setUpdateUser(logInfo.getStaffCode());
		repo.update(displayProductGroupDTL);
		
	}

	@Override
	public StDisplayPdGroupDtl getDisplayProductGroupDTLById(long id)
			throws DataAccessException {
		return repo.getEntityById(StDisplayPdGroupDtl.class, id);
	}

	@Override
	public List<StDisplayPdGroupDtl> getListDisplayProductGroupDTL(
			KPaging<StDisplayPdGroupDtl> kPaging,
			Long displayProductGroupDTLId,Long displayProductGroupId, ActiveType status)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select dpgd.* FROM display_product_group_dtl dpgd JOIN product p ON dpgd.product_id = p.product_id WHERE 1=1 ");
		if (status != null) {
			sql.append(" and dpgd.status=?");
			params.add(status.getValue());
		}
		if (displayProductGroupDTLId != null) {
			sql.append(" and dpgd.display_product_group_dtl_id=?");
			params.add(displayProductGroupDTLId);
		}
		if(displayProductGroupId!=null){
			sql.append(" and dpgd.display_product_group_id=?");
			params.add(displayProductGroupId);
		}
		sql.append(" order by p.product_code");
		if (kPaging == null)
			return repo.getListBySQL(StDisplayPdGroupDtl.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StDisplayPdGroupDtl.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public List<StDisplayPlDpDtl> getListDisplayPDGroupDTL(Long displayProductGroupDTLId, ActiveType status)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * FROM display_pl_dp_dtl WHERE 1=1 ");
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		}
		if (displayProductGroupDTLId != null) {
			sql.append(" and display_product_group_dtl_id = ?");
			params.add(displayProductGroupDTLId);
		}		
		return repo.getListBySQL(StDisplayPlDpDtl.class, sql.toString(), params);
	}

	@Override
	public StDisplayPdGroupDtl getDisplayProductGroupDTLByGroupId(
			Long displayProductGroupId, Long productId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM display_product_group_dtl dpgd WHERE 1=1 and dpgd.status != -1");
		if (displayProductGroupId != null) {
			sql.append(" and dpgd.display_product_group_dtl_id = ? ");
			params.add(displayProductGroupId);
		}
		if (displayProductGroupId != null) {
			sql.append(" and dpgd.product_id = ? ");
			params.add(productId);
		}
		return repo.getEntityBySQL(StDisplayPdGroupDtl.class, sql.toString(), params);
	}
	
	
	
	
}

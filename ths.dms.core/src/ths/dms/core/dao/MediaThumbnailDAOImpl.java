package ths.dms.core.dao;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.MediaThumbnail;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class MediaThumbnailDAOImpl implements MediaThumbnailDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Override
	public MediaThumbnail createMediaThumbnail(MediaThumbnail mediaThumbnail, LogInfoVO logInfo) throws DataAccessException {
		if (mediaThumbnail == null) {
			throw new IllegalArgumentException("mediaThumbnail");
		}
//		mediaThumbnail.setCreateUser(logInfo.getStaffCode());
		repo.create(mediaThumbnail);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, mediaThumbnail, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(MediaThumbnail.class, mediaThumbnail.getId());
	}

	@Override
	public void deleteMediaThumbnail(MediaThumbnail mediaThumbnail, LogInfoVO logInfo) throws DataAccessException {
		if (mediaThumbnail == null) {
			throw new IllegalArgumentException("mediaThumbnail");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(mediaThumbnail);
		try {
			actionGeneralLogDAO.createActionGeneralLog(mediaThumbnail, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public MediaThumbnail getMediaThumbnailById(long id) throws DataAccessException {
		return repo.getEntityById(MediaThumbnail.class, id);
	}
	
	@Override
	public void updateMediaThumbnail(MediaThumbnail mediaThumbnail, LogInfoVO logInfo) throws DataAccessException {
		if (mediaThumbnail == null) {
			throw new IllegalArgumentException("mediaThumbnail");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		MediaThumbnail temp = this.getMediaThumbnailById(mediaThumbnail.getId());

//		product.setUpdateDate(commonDAO.getSysDate());
//		product.setUpdateUser(logInfo.getStaffCode());
		repo.update(mediaThumbnail);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, mediaThumbnail, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
//
//	@Override
//	public List<MediaThumbnail> getListMediaThumbnailByObjectIdAndObjectType(KPaging<MediaThumbnail> kPaging, Long objectId, MediaObjectType objectType) throws DataAccessException {
//		if (objectId == null) {
//			throw new IllegalArgumentException("objectId is null");
//		}
//		StringBuilder sql = new StringBuilder();
//		ArrayList<Object> params = new ArrayList<Object>();
//		sql.append(" select * from media_thumbnail");
//		sql.append(" where object_id = ?");
//		params.add(objectId);
//		sql.append(" and object_type = ?");
//		params.add(objectType.getValue());		
//		sql.append(" order by id");
//		if (kPaging == null)
//			return repo.getListBySQL(MediaThumbnail.class, sql.toString(), params);
//		else
//			return repo.getListBySQLPaginated(MediaThumbnail.class, sql.toString(), params, kPaging);
//	}
//
//	@Override
//	public List<MediaThumbnail> getListMediaThumbnailByMediaType(MediaType mediaType) throws DataAccessException {
//		if (mediaType == null) {
//			throw new IllegalArgumentException("mediaType is null");
//		}
//		StringBuilder sql = new StringBuilder();
//		ArrayList<Object> params = new ArrayList<Object>();
//		sql.append(" select * from media_thumbnail");
//		sql.append(" where media_type = ?");
//		params.add(mediaType.getValue());		
//		sql.append(" order by id");
//		return repo.getListBySQL(MediaThumbnail.class, sql.toString(), params);
//	}
	
//	@Override
//	public List<MediaThumbnail> getListMediaThumbnailByProduct(Long productId) throws DataAccessException {
//		if (productId == null) {
//			throw new IllegalArgumentException("productId is null");
//		}
//		StringBuilder sql = new StringBuilder();
//		ArrayList<Object> params = new ArrayList<Object>();
//		sql.append(" select * from media_thumbnail");
//		sql.append(" where object_id = ?");
//		params.add(productId);
//		sql.append(" order by media_thumbnail_id");
//		return repo.getListBySQL(MediaThumbnail.class, sql.toString(), params);
//	}
}

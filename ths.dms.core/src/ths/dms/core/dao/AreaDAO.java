package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Area;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.AreaType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.AreaVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface AreaDAO {

	Area createArea(Area area, LogInfoVO logInfo) throws DataAccessException;

	void deleteArea(Area area, LogInfoVO logInfo) throws DataAccessException;

	void updateArea(Area area, LogInfoVO logInfo) throws DataAccessException;
	
	Area getAreaByCode(String code) throws DataAccessException;

	Boolean isUsingByOthers(long areaId) throws DataAccessException;

	List<Area> getListSubArea(Long parentId) throws DataAccessException;

//	void createArea(List<Area> lstArea, LogInfoVO logInfo) throws DataAccessException;

	Area getAreaById(long id) throws DataAccessException;

	Boolean checkIfAreaHasAnyChildStopped(Long areaId)
			throws DataAccessException;

	List<Area> getListArea(KPaging<Area> kPaging, String areaCode,
			String areaName, Long parentId, ActiveType status,
			String provinceCode, String provinceName, String districtCode,
			String districtName, String precinctCode, String precinctName,
			AreaType type, Boolean isGetOneChildLevel,String sort,String order)
			throws DataAccessException;

	Boolean checkIfAreaHasAllChildStopped(Long areaId)
			throws DataAccessException;

	Area getArea(String provinceCode, String districtCode, String precinctCode,
			AreaType type, ActiveType status) throws DataAccessException;

	Area getAreaEqual(String areaCode, String areaName, Long parentId,
			ActiveType status, String provinceCode, String provinceName,
			String districtCode, String districtName, String precinctCode,
			String precinctName, AreaType type, Boolean isGetOneChildLevel)
			throws DataAccessException;

	List<Area> getListSubArea(Long parentId, ActiveType activeType,
			Long exceptionalAreaId, Boolean isOrderByCode)
			throws DataAccessException;
	
	List<Area> getListAreaFull()throws DataAccessException;
	
	Area getAreaByPrecinct(String code) throws DataAccessException;
	
	/**
	 * Lay danh sach dia ban theo loai
	 * 
	 * @author lacnv1
	 * @since Apr 21, 2014
	 */
	List<AreaVO> getListAreaByType(Long parentId, Integer type, Integer status) throws DataAccessException;
	
	/**
	 * lay thong tin dia ban voi thong tin cac dia ban cha
	 * @author tuannd20
	 * @param status Trang thai dia ban can lay
	 * @return Danh sach dia ban & thong tin dia ban cha
	 * @throws BusinessException
	 * @since 28/03/2015
	 */
	List<AreaVO> retrieveAreaWithParentAreaInfo(ActiveType status) throws DataAccessException;
}

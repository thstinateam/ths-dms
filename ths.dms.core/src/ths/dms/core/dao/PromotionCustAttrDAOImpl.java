package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PromotionCustAttr;
import ths.dms.core.entities.PromotionCustAttrDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PromotionCustAttVO2;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.dao.repo.IRepository;

public class PromotionCustAttrDAOImpl implements PromotionCustAttrDAO {

	@Autowired
	private IRepository repo;
	@Autowired
	private PromotionCustAttrDetailDAO promotionCustAttrDetailDAO;
	@Override
	public PromotionCustAttr createPromotionCustAttr(
			PromotionCustAttr promotionCustAttr, LogInfoVO logInfo)
			throws DataAccessException {
		if (promotionCustAttr == null) {
			throw new IllegalArgumentException("poAuto");
		}
		repo.create(promotionCustAttr);
		return repo.getEntityById(PromotionCustAttr.class, promotionCustAttr.getId());
	}

	@Override
	public void deletePromotionCustAttr(PromotionCustAttr promotionCustAttr,
			LogInfoVO logInfo) throws DataAccessException {
		if (promotionCustAttr == null) {
			throw new IllegalArgumentException("promotionCustAttr");
		}
		repo.delete(promotionCustAttr);
	}

	@Override
	public void updatePromotionCustAttr(PromotionCustAttr promotionCustAttr,
			LogInfoVO logInfo) throws DataAccessException {
		if (promotionCustAttr == null) {
			throw new IllegalArgumentException("promotionCustAttr");
		}
		repo.update(promotionCustAttr);
	}

	@Override
	public PromotionCustAttr getPromotionCustAttrById(long id)
			throws DataAccessException {
		return repo.getEntityById(PromotionCustAttr.class, id);
	}

	@Override
	public List<PromotionCustAttVO2> getListPromotionCustAttVOValue(
			long promotionProgramId) throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT ");
		sql.append(" PRO.PROMOTION_CUST_ATTR_ID promotionCustAttrId, ");
		sql.append(" ATT.CUSTOMER_ATTRIBUTE_ID attributeId, ");
		sql.append(" PRO.FROM_VALUE fromValue, ");
		sql.append(" PRO.TO_VALUE toValue, ");
		sql.append(" ATT.TYPE valueType ");
		sql.append(" FROM PROMOTION_CUST_ATTR pro,CUSTOMER_ATTRIBUTE att ");
		sql.append(" WHERE PRO.OBJECT_TYPE=1 ");
		sql.append(" AND ATT.CUSTOMER_ATTRIBUTE_ID=PRO.OBJECT_ID ");
		sql.append(" AND ATT.TYPE in (1,2,3) ");
		sql.append(" AND ATT.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND PRO.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND PRO.PROMOTION_PROGRAM_ID=? ");
		params.add(promotionProgramId);
		
		String[] fieldNames = { "promotionCustAttrId", // 0
				"attributeId", // 1
				"fromValue", // 2,
				"toValue",//3
				"valueType"//4
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.INTEGER//4
		};
		
		return repo.getListByQueryAndScalar(PromotionCustAttVO2.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<PromotionCustAttVO2> getListPromotionCustAttVOValueDetail(
			long promotionProgramId) throws DataAccessException {
		
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" with lstTmp as( ");
		sql.append(" SELECT PRO.PROMOTION_CUST_ATTR_ID,ATT.CUSTOMER_ATTRIBUTE_ID, ");
		sql.append(" ATT.TYPE ");
		sql.append(" FROM ");
		sql.append(" PROMOTION_CUST_ATTR pro,CUSTOMER_ATTRIBUTE att ");
		sql.append(" WHERE PRO.OBJECT_ID=ATT.CUSTOMER_ATTRIBUTE_ID ");
		sql.append(" AND PRO.STATUS = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND ATT.STATUS = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND PRO.OBJECT_TYPE = 1 ");
		sql.append(" AND PRO.PROMOTION_PROGRAM_ID = ? ");
		params.add(promotionProgramId);
		sql.append(" ) ");
		sql.append(" SELECT ");
		sql.append("  t.PROMOTION_CUST_ATTR_ID promotionCustAttrId, ");
		sql.append("  t.CUSTOMER_ATTRIBUTE_ID attributeId, ");
		sql.append("  pcad.PROMOTION_CUST_ATTR_DETAIL_ID promotionCustAttrDetailId, ");
//		sql.append("  cad.CUSTOMER_ATTRIBUTE_DETAIL_ID attributeDetailId, ");
		sql.append("  cae.CUSTOMER_ATTRIBUTE_ENUM_ID attributeEnumId, ");
		sql.append("  cae.VALUE attributeDetailName, ");
		sql.append("  cae.CODE attributeDetailCode, ");
		sql.append("  t.TYPE valueType ");
		sql.append(" FROM lstTmp t ");
		sql.append(" JOIN PROMOTION_CUST_ATTR_DETAIL pcad on t.PROMOTION_CUST_ATTR_ID = pcad.PROMOTION_CUST_ATTR_ID ");
//		sql.append(" JOIN CUSTOMER_ATTRIBUTE_DETAIL cad on pcad.OBJECT_ID = cad.CUSTOMER_ATTRIBUTE_DETAIL_ID ");
		sql.append(" JOIN CUSTOMER_ATTRIBUTE_ENUM cae on pcad.OBJECT_ID = cae.CUSTOMER_ATTRIBUTE_ENUM_ID ");
		sql.append(" WHERE 1 = 1 ");
		sql.append("  AND pcad.STATUS=? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("  AND pcad.OBJECT_TYPE = 1");
//		sql.append("  AND CAD.STATUS=? ");
//		params.add(ActiveType.RUNNING.getValue());
		
		String[] fieldNames = { "promotionCustAttrId", // 0
				"attributeId", // 1
				"promotionCustAttrDetailId", // 2,
//				"attributeDetailId",//3
				"attributeEnumId",//3
				"attributeDetailName",//4
				"attributeDetailCode",//5
				"valueType"//6
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.LONG, // 1
				StandardBasicTypes.LONG, // 2
				StandardBasicTypes.LONG, // 3
				StandardBasicTypes.STRING,//4
				StandardBasicTypes.STRING,//5
				StandardBasicTypes.INTEGER//6
		};
		return repo.getListByQueryAndScalar(PromotionCustAttVO2.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public void createOrUpdatePromotionCustAttDynamicVO(PromotionCustAttr proCusAtt) throws DataAccessException {
		if(proCusAtt.getId()!=null){
			
			//cap nhat 
			proCusAtt.setUpdateDate(DateUtility.now());
			repo.update(proCusAtt);
		}else{
			//them moi
			proCusAtt.setCreateDate(DateUtility.now());
			repo.create(proCusAtt);
		}
	}

	@Override
	public void createOrUpdatePromotionCustAttDynamicVO(PromotionCustAttr proCusAtt,
			List<PromotionCustAttrDetail> lstproCusAttDetail,LogInfoVO logInfo) throws DataAccessException {
		String updateUser = logInfo.getStaffCode();
		if(proCusAtt.getId()!=null){
			//cap nhat
			proCusAtt.setUpdateDate(DateUtility.now());
			repo.update(proCusAtt);
			if(proCusAtt.getStatus()==ActiveType.DELETED){
				List<PromotionCustAttrDetail> lsTemp=new ArrayList<PromotionCustAttrDetail>();
				for(PromotionCustAttrDetail item: promotionCustAttrDetailDAO.getListPromotionCustAttrDetail(proCusAtt.getId())){
					item.setUpdateDate(DateUtility.now());
					item.setStatus(ActiveType.DELETED);
					item.setUpdateUser(updateUser);
					lsTemp.add(item);
				}
				//xoa het
				repo.update(lsTemp);
			}
			if(proCusAtt.getStatus()==ActiveType.RUNNING){
				List<PromotionCustAttrDetail> lsTempOld=promotionCustAttrDetailDAO.getListPromotionCustAttrDetail(proCusAtt.getId());
				if(lstproCusAttDetail!=null&&!lstproCusAttDetail.isEmpty()){
					for(PromotionCustAttrDetail item:lsTempOld){
						Boolean flag=false;
						for(PromotionCustAttrDetail itemChil:lstproCusAttDetail){
							if(itemChil.getObjectId().compareTo(item.getObjectId())==0
								&&itemChil.getObjectType()==item.getObjectType()){
								flag=true;
							}
						}
						if(!flag){
							item.setStatus(ActiveType.DELETED);
							item.setUpdateDate(DateUtility.now());
							item.setUpdateUser(updateUser);
							repo.update(item);
						}
					}
					for(PromotionCustAttrDetail item:lstproCusAttDetail){
						Boolean flag=false;
						for(PromotionCustAttrDetail itemChil:lsTempOld){
							if(itemChil.getObjectId().compareTo(item.getObjectId())==0
									&&itemChil.getObjectType()==item.getObjectType()){
									flag=true;
							}
						}
						if(!flag){
							item.setCreateDate(DateUtility.now());
							item.setPromotionCustAttr(proCusAtt);
							repo.create(item);
						}
					}
				}
			}
		}else{
			//them moi
			proCusAtt.setCreateDate(DateUtility.now());
			proCusAtt=repo.create(proCusAtt);
			for(int i=0;i<lstproCusAttDetail.size();i++){
				lstproCusAttDetail.get(i).setCreateDate(DateUtility.now());
				lstproCusAttDetail.get(i).setPromotionCustAttr(proCusAtt);
			}
			repo.create(lstproCusAttDetail);
		}
	}
	
	@Override
	public PromotionCustAttr getPromotionCustAttrByPromotion(long promotionId, Integer objectType, Long objectId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder("select * from PROMOTION_CUST_ATTR where rownum = 1 and status = 1 and PROMOTION_PROGRAM_ID=?");
		List<Object> params = new ArrayList<Object>();
		params.add(promotionId);
		if(objectType!= null){
			sql.append(" AND object_type = ?");
			params.add(objectType);
		}
		if(objectId!= null){
			sql.append(" AND object_id = ?");
			params.add(objectId);
		}
		return repo.getEntityBySQL(PromotionCustAttr.class, sql.toString(), params);
	}
	
	@Override
	public void deletePromotionCustAttrInfo(Long promotionProgramId, List<Long> lstExceptAttrId, LogInfoVO logInfo)
			throws DataAccessException {
		if (logInfo != null){
			deletePromotionCustAttrDetailInfo(promotionProgramId, lstExceptAttrId, logInfo);
		}else{
			deletePromotionCustAttrDetailInfo(promotionProgramId, lstExceptAttrId);
		}
		StringBuilder sql = new StringBuilder("update PROMOTION_CUST_ATTR set status = -1, UPDATE_USER = ?, UPDATE_DATE = sysdate where 1 = 1 and PROMOTION_PROGRAM_ID = ?");
		
		List<Object> params = new ArrayList<Object>();
		params.add(logInfo.getStaffCode());
		params.add(promotionProgramId);
		if(lstExceptAttrId != null && lstExceptAttrId.size() > 0){
			Boolean isFirst = true;
			sql.append(" AND PROMOTION_CUST_ATTR_ID not in(");
			for (Long child : lstExceptAttrId) {
				if(isFirst){
					isFirst = false;
					sql.append(child);
				}else{
					sql.append("," + child);
				}
			}
			sql.append(" )");
		}
		repo.executeSQLQuery(sql.toString(), params);
	}
	
	public void deletePromotionCustAttrDetailInfo(Long promotionProgramId, List<Long> lstExceptAttrId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder("update PROMOTION_CUST_ATTR_DETAIL dt set dt.status = -1 where 1 = 1 ");
		sql.append(" AND EXISTS(select 1 from PROMOTION_CUST_ATTR attr where attr.PROMOTION_PROGRAM_ID = ? and attr.PROMOTION_CUST_ATTR_ID = dt.PROMOTION_CUST_ATTR_ID)");
		List<Object> params = new ArrayList<Object>();
		params.add(promotionProgramId);
		if(lstExceptAttrId != null && lstExceptAttrId.size() > 0){
			Boolean isFirst = true;
			sql.append(" AND dt.PROMOTION_CUST_ATTR_ID not in(");
			for (Long child : lstExceptAttrId) {
				if(isFirst){
					isFirst = false;
					sql.append(child);
				}else{
					sql.append("," + child);
				}
			}
			sql.append(" )");
		}
		repo.executeSQLQuery(sql.toString(), params);
	}
	
	/*
	 * author: lochp
	 * fix loi insert updateUser khi delete promotionCusAttrDetail
	 * (non-Javadoc)
	 * @see ths.dms.core.dao.PromotionCustAttrDAO#getLstPromotionCustAttrByPP(java.lang.Long)
	 */
	
	public void deletePromotionCustAttrDetailInfo(Long promotionProgramId, List<Long> lstExceptAttrId, LogInfoVO logInfo)
			throws DataAccessException {
		String updateUser = logInfo.getStaffCode();
		StringBuilder sql = new StringBuilder("update PROMOTION_CUST_ATTR_DETAIL dt set dt.status = -1, update_user = ?   where 1 = 1 ");
		sql.append(" AND EXISTS(select 1 from PROMOTION_CUST_ATTR attr where attr.PROMOTION_PROGRAM_ID = ? and attr.PROMOTION_CUST_ATTR_ID = dt.PROMOTION_CUST_ATTR_ID)");
		List<Object> params = new ArrayList<Object>();
		params.add(updateUser);
		params.add(promotionProgramId);
		if(lstExceptAttrId != null && lstExceptAttrId.size() > 0){
			Boolean isFirst = true;
			sql.append(" AND dt.PROMOTION_CUST_ATTR_ID not in(");
			for (Long child : lstExceptAttrId) {
				if(isFirst){
					isFirst = false;
					sql.append(child);
				}else{
					sql.append("," + child);
				}
			}
			sql.append(" )");
		}
		repo.executeSQLQuery(sql.toString(), params);
	}
	
	@Override
	public List<PromotionCustAttr> getLstPromotionCustAttrByPP(Long promotionProgramId)
			throws DataAccessException {
		String sql = "select * from promotion_cust_attr where status != -1 and promotion_program_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(promotionProgramId);
		return repo.getListBySQL(PromotionCustAttr.class, sql, params);
	}
}

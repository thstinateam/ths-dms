package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.enumtype.ProductFilter;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.ProgramObjectType;
import ths.dms.core.entities.enumtype.TreeVOType;
import ths.dms.core.entities.filter.ProductGeneralFilter;
import ths.dms.core.entities.vo.GeneralProductVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PoAutoVO;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.entities.vo.ProductExportVO;
import ths.dms.core.entities.vo.ProductTreeVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.entities.vo.ProductVOEx;
import ths.dms.core.entities.vo.ProductsForPoAutoParams;
import ths.dms.core.entities.vo.SalePlanVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.core.entities.vo.rpt.RptCompareBookAndDeliveryProductVO;
import ths.core.entities.vo.rpt.RptF1DataVO;
import ths.core.entities.vo.rpt.Rpt_TDXNTCT_NganhVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface ProductDAO {

	Product createProduct(Product product, LogInfoVO logInfo) throws DataAccessException;

	void deleteProduct(Product product, LogInfoVO logInfo) throws DataAccessException;

	void updateProduct(Product product, LogInfoVO logInfo) throws DataAccessException;
	
	Product getProductById(long id) throws DataAccessException;

	Product getProductByCode(String code) throws DataAccessException;
	
	Product getProductByCode(String code, Integer status) throws DataAccessException;
	
	Product getProductByCodeExceptZ(String code, Long staffId) throws DataAccessException;

	Boolean isUsingByOthers(long productId) throws DataAccessException;

	/**
	 * lay danh sach po auto cho NPP
	 * @author hieunq1
	 * @param shopId
	 * @param checkAll
	 * @return
	 * @throws DataAccessException
	 */
	List<PoAutoVO> getListPoAuto(Long shopId, Boolean checkAll) throws DataAccessException;
	
	/**
	 * Lay du lieu phuc vu Bao cao so sanh PO AUTO
	 * 
	 * @param shopId
	 * @param subCat
	 * @param workDateInMonth
	 * @param realWorkDateInMonth
	 * @return
	 * @author ThuatTQ
	 */
	/*List<RptCompareImExStF1DataVO> getDataForComparePOAutoReport(Long shopId,
			Long poId, int workDateInMonth, int realWorkDateInMonth)
			throws DataAccessException;*/


	/**
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @param staffCode
	 * @param catCode
	 * @param productCode
	 * @param month
	 * @param year
	 * @return
	 * @throws DataAccessException
	 */
	List<SalePlanVO> getListSalePlanVOForProduct(KPaging<SalePlanVO> kPaging, Long shopId, String staffCode,
			String catCode,String productCode, Integer month, Integer year)
			throws DataAccessException;


	/**
	 * @author thanhnguyen
	 * @param kPaging
	 * @param productCode
	 * @param productName
	 * @return
	 */
	List<ProductVO> getListProductAddSkuSalePlan(KPaging<ProductVO> kPaging,
			String productCode, String productName, Integer month, Integer year) throws DataAccessException;

	/**
	 * lay du lieu cho bao cao xuat nhap ton chi tiet
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	/*List<RptImExStDataDetailVO> getDataForImExStReport(Long shopId, Date fromDate,
			Date toDate, Long catId) throws DataAccessException;*/

	/**
	 * lay san pham cho tao don hang
	 * @author hieunq1
	 * @param paging
	 * @param shopId
	 * @param staffId
	 * @param shopType 
	 * @return
	 * @throws DataAccessException
	 */
	List<OrderProductVO> getListOrderProductVO(KPaging<OrderProductVO> paging,
			Long shopId, Long staffId, Long custId, String pdCode, String pdName, String ppCode,Date lockDay,Long custTypeId, Long shopType) throws DataAccessException;
	
	/**
	 * 3.1.4.7 lay danh sach bao cao so sach so luong dat va giao hang
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptCompareBookAndDeliveryProductVO> getListCompareBookAndDeliveryProduct(
			Long shopId, Date fromDate, Date toDate) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param productCode
	 * @param productName
	 * @param dpId
	 * @return
	 * @throws DataAccessException
	 */
	/*List<Product> getListDisplayProductTreeVO(String productCode, String productName,
			Long exDpId, Integer limit) throws DataAccessException;*/

	/**
	 * 
	 * @author hieunq1
	 * @param productCode
	 * @param productName
	 * @param exDpId
	 * @return
	 * @throws DataAccessException
	 */
	List<Product> getListFocusProductTreeVO(String productCode,
			String productName, Long exDpId, Integer limit) throws DataAccessException;
	
	/**
	 * Lay du lieu cho report Xuat Nhap Ton F1 (sau khi thay doi dac ta yeu cau)
	 * 
	 * @param shopId
	 * @param workDateInMonth
	 * @param realWorkDateInMonth
	 * @param passWorkDateInMonth
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<RptF1DataVO> getDataForF1Report(Long shopId, Date fromDate,
			Date toDate, int workDateInMonth, int realWorkDateInMonth)
			throws DataAccessException;

	List<Product> getListProduct(ProductFilter filter)
			throws DataAccessException;

	void updateDb() throws DataAccessException;

	List<StockTotalVO> getListProductForPoAuto(KPaging<StockTotalVO> kPaging,
			ProductsForPoAutoParams filter) throws DataAccessException;

	List<PoVnmDetailLotVO> getListProductForPoConfirm(KPaging<PoVnmDetailLotVO> kPaging, 
			PoVnmFilter filter)
			throws DataAccessException;
	
	List<Product> getListProductHasInSaleOrder(KPaging<Product> kPaging, String productCode,
			String productName, ActiveType status) throws DataAccessException;

	List<Product> getListProductExceptZ(KPaging<Product> paging,
			String productCode, String productName, Long staffId) throws DataAccessException;

	Boolean checkProductInZ(String productCode) throws DataAccessException;

	Boolean isProductBelongEquipmentCat(Product product)
			throws DataAccessException;
	
	List<GeneralProductVO> getAllProducts(String productCode,KPaging<GeneralProductVO> kPaging,Boolean isExceptZCat) throws DataAccessException;
	
	List<Product> getListProductEquipmentZ() throws DataAccessException;

	List<ProductTreeVO> getListProductTreeVO(String productCode,String productName, 
			Long programObjectId, ProgramObjectType programObjectType, 
			Boolean exceptEquipmentCat,DisplayProductGroupType type)throws DataAccessException;

	List<ProductTreeVO> getListProductTreeVOByParentNode(Long programObjectId, ProgramObjectType programObjectType,
			Long parentIdNode, TreeVOType parentTypeNode,Boolean exceptEquipmentCat,DisplayProductGroupType type)
			throws DataAccessException;

	List<Product> getListProductInCatZ(KPaging<Product> paging,
			String productCode, String productName)throws DataAccessException;

	List<Product> getListProductEquipmentZOrderByCat() throws DataAccessException;

	Boolean checkExitsProductInEquipment(String productCode) throws DataAccessException;
	
	public List<ProductLot> getProductLotByProduct(Product product) throws DataAccessException;
	
	public List<Rpt_TDXNTCT_NganhVO> LayDanhSachXuatNhapKhoChiTiet(Long shopId, Date fromDate, Date toDate)throws DataAccessException;
	
	//CTTB 2013
	List<ProductVO> getListGroupProductVO(KPaging<ProductVO> kPaging,
			String productCode, String productName, Long displayId,
			DisplayProductGroupType type) throws DataAccessException;
	
	List<ProductVOEx> getListProductVOForVideo(ths.dms.core.entities.filter.ProductFilter filter, List<Product> lstProductExpect)
			throws DataAccessException;

	List<ProductVOEx> getListCategroy(String expectOne) throws DataAccessException;
	
	/*BEGIN invocation DisplayToolMgrImpl*/
	List<Product> getListProduct4ProductTool(KPaging<Product> kPaging,
			String productCode, String productName, String toolCode)
			throws DataAccessException;
	
	List<Product> getListProductToMTDisplayTeamplate(KPaging<Product> kPaging,
			String productCode, String productName, Long idMTDisplayTeamplate)
			throws DataAccessException;
	
	Product checkProductToMTDisplayTeamplateChange(String productCode, Long idMTDisplayTeamplate) throws DataAccessException;
	
	/*END invocation DisplayToolMgrImpl*/
	
	//vuongmq
	List<Product> getListProductByNameOrCode(KPaging<Product> kPaging,String name,String code,String categoryCode)throws DataAccessException;
	
	/**
	 * Lay danh sach san pham ban cua nhan vien ban hang vansale
	 * @author vuonghn
	 */
	List<OrderProductVO> getListOrderProductVansale(
			KPaging<OrderProductVO> kPaging, Long shopId, Long staffId,
			String pdCode, String pdName, Date lockDay) throws DataAccessException;
	
	List<OrderProductVO> getListOrderProductVOWithZ(
			KPaging<OrderProductVO> paging, Long shopId, Long staffId,
			Long custId, String pdCode, String pdName, String ppCode,
			Date lockDay) throws DataAccessException;

	List<Product> getAllListProduct(ProductFilter filter)
			throws DataAccessException;
	
	/**
	 * lay danh sach kho theo SP 
	 * @author nhanlt6
	 * @param paging
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 */
	List<OrderProductVO> getListOrderProductWarehouseVO(KPaging<OrderProductVO> paging, Long productId, Long shopId) throws DataAccessException;

	/**
	 * lay danh sach tong ton kho cua sp theo nhieu gia
	 * @author nhanlt6
	 * @return
	 * @throws DataAccessException
	 */
	List<StockTotalVO> getPromotionProductInfo(Long productId, Long shopId, Integer shopChannel, Long customerId, Integer objectType,Date orderDate) throws DataAccessException;

	/**
	 * 
	 * @param status
	 * @return
	 * @throws DataAccessException
	 */
	List<ProductVO> getListProductForWarehouseExport(Integer status)throws DataAccessException;

	/***************************BEGIN HAM MOI TU HABECO *************************/
	/**
	 * Lay danh sach san pham theo ten (tim bang) productName, tru san pham exceptProductId
	 * 
	 * @author lacnv1
	 * @since Dec 28, 2013
	 */
	List<Product> getListProductsByName(String productName, Long exceptProductId)
			throws DataAccessException;
	
	/**
	 * Lay danh sach san pham theo ten shortName (tim bang) shortName, tru san pham exceptProductId
	 * @author haupv3
	 * @since Feb 10, 2014
	 */
	List<Product> getListProductsByShortName(String productName, Long exceptProductId)
	throws DataAccessException;	
	
		/**
	 * LacNV
	 */
	List<ProductExportVO> getListProductExport(ProductFilter filter, KPaging<ProductExportVO> paging)
			throws DataAccessException;
	/***************************END HAM MOI TU HABECO *************************/

	void increaseOrderIndexProduct(int orderIndex) throws DataAccessException;

	Boolean checkDuplicateOrderIndex(Long productId, int orderIndex) throws DataAccessException;

	/**
	 * Ham lay danh sach san pham theo nganh hang 
	 * 
	 * @author hoanv25
	 * @since July 10, 2015
	 * @description creat
	 */
	List<ImageVO> getListParentCatByCat(Long id) throws DataAccessException;

	/**
	 * Ham lay danh sach san pham theo nganh hang
	 * 
	 * @author hoanv25
	 * @since July 10, 2015
	 * @description creat
	 */
	List<ImageVO> getListParentCatBySubCat(Long id) throws DataAccessException;

	/**
	 * Ham lay nganh hang theo id
	 * 
	 * @param Id
	 * @author hoanv25
	 * @since July 10, 2015
	 * @description creat
	 */
	ProductInfo getProductInfoById(Long id) throws DataAccessException;

	/**
	 * Lay ds san pham theo nganh hang va nganh hang con
	 * 
	 * @author hoanv25
	 * @since July 20/2015
	 * @param idParentCat , idParentSubCat
	 * @return
	 * @throws BusinessException
	 */
	List<ImageVO> getListParentCatByCatAndSubCat(Long idParentCat, Long idParentSubCat) throws DataAccessException;

	/**
	 * tao cay nganh hang
	 * 
	 * @author hoanv25
	 * @return cac node tren cay don vi
	 * @since Auggust 19,2015
	 */
	List<ProductInfo> getSubCatOnTree(Boolean b) throws DataAccessException;
	
	/**
	 * kiem tra san pham dang chon productInfo
	 * @author trietptm
	 * @param pdType
	 * @param productInfoId
	 * @return danh sach san pham dang chon productInfo
	 * @throws DataAccessException
	 * @since Nov 18, 2015
	 */
	List<Product> checkIfExistsProductByProductInfo(ProductType pdType, Long productInfoId) throws DataAccessException;
	
	/**
	 * Tim kiem san pham voi thong tin thuoc tinh dong
	 * @author tuannd20
	 * @param filter tham so tim kiem
	 * @return danh sach san pham kem theo thong tin thuoc tinh dong
	 * @throws BusinessException
	 * @since 18/09/2015
	 */
	List<ProductExportVO> getProductWithDynamicAttributeInfo(ProductFilter filter) throws DataAccessException;
	/**
	 * Lay san pham theo ma code khong so sanh tinh trang cua sp
	 * 
	 * @param code: ma san pham
	 * 
	 * @author hunglm16
	 * @since May 20,2015
	 * */
	Product getProductByCodeNotByStatus(String code) throws DataAccessException;
	
	/**
	 * Lay danh sach san pham
	 * 
	 * @author hunglm16
	 * @since May 20,2015
	 * */
	List<ProductVO> getListProductVOByFilter(ProductGeneralFilter<ProductVO> filter) throws DataAccessException;

	/**
	 * Gets the product by filter.
	 * @author trietptm
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @since Mar 2, 2016
	 */
	Product getProductByFilter(ProductFilter filter) throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StDisplayProgramLevel;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class DisplayProgrameLevelDAOImpl implements DisplayProgrameLevelDAO {

	@Autowired
	private IRepository repo;

	/**
	 * @author loctt
	 * @since 18sep2013
	 * @param kPaging
	 * @param displayProgramId
	 * @param levelCode
	 * @param status
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public List<StDisplayProgramLevel> getListDisplayProgramLevel(
			KPaging<StDisplayProgramLevel> kPaging, Long displayProgramId,
			String levelCode, ActiveType status) 
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from display_program_level where 1=1");
		
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		}
		else {
			sql.append(" and status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		if (displayProgramId != null) {
			sql.append(" and display_program_id=?");
			params.add(displayProgramId);
		}
		else
			sql.append(" and display_program_id is null");
		
		if (!StringUtility.isNullOrEmpty(levelCode)) {
			sql.append(" and level_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(levelCode.toUpperCase()));
		}
		sql.append(" order by level_code");
		if (kPaging == null)
			return repo.getListBySQL(StDisplayProgramLevel.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StDisplayProgramLevel.class, sql.toString(), params, kPaging);
	}
	/**
	 * @author loctt
	 * @since 19sep2013
	 * @param id
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public StDisplayProgramLevel getDisplayProgramLevelById(long id) throws DataAccessException {
		return repo.getEntityById(StDisplayProgramLevel.class, id);
	}	
	
	@Autowired
	private CommonDAO commonDAO;
	/**
	 * @author loctt
	 * @since 19sep2013
	 * @param displayProgramLevel
	 * @param logInfo
	 * @throws DataAccessException
	 */
	@Override
	public void updateDisplayProgramLevel(StDisplayProgramLevel displayProgramLevel, LogInfoVO logInfo) throws DataAccessException {
		if (displayProgramLevel == null) {
			throw new IllegalArgumentException("displayProgramLevel is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (displayProgramLevel.getLevelCode() != null)displayProgramLevel.setLevelCode(displayProgramLevel.getLevelCode().toUpperCase());
		StDisplayProgramLevel temp = this.getDisplayProgramLevelById(displayProgramLevel.getId());
		temp = temp.clone();
		
		displayProgramLevel.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null) displayProgramLevel.setUpdateUser(logInfo.getStaffCode());
		repo.update(displayProgramLevel);
//		try {
//			actionGeneralLogDAO.createActionGeneralLog(temp, displayProgramLevel, logInfo);
//		}
//		catch (Exception ex) {
//			throw new DataAccessException(ex);
//		}
	}
	/**
	 * @author loctt
	 * @since 19sep2013
	 */
	@Override
	public StDisplayProgramLevel getDisplayProgramLevelByLevelCode(Long displayProgramId, String levelCode, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from display_program_level where 1 = 1");
		if (displayProgramId != null) {
			sql.append(" and display_program_id=?");
			params.add(displayProgramId);
		}
		if (!StringUtility.isNullOrEmpty(levelCode)) {
			sql.append(" and upper(level_code)=?");
			params.add(levelCode.toUpperCase());
		}
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		}
		else {
			sql.append(" and status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		return repo.getEntityBySQL(StDisplayProgramLevel.class, sql.toString(), params);
	}
	/**
	 * @author loctt
	 * @since 19sep2013
	 * @param displayProgramLevel
	 * @param logInfo
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public StDisplayProgramLevel createDisplayProgramLevel(StDisplayProgramLevel displayProgramLevel, LogInfoVO logInfo) throws DataAccessException {
		if (displayProgramLevel == null) {
			throw new IllegalArgumentException("displayProgramLevel is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (displayProgramLevel.getLevelCode() != null) displayProgramLevel.setLevelCode(displayProgramLevel.getLevelCode().toUpperCase());
		displayProgramLevel.setCreateUser(logInfo.getStaffCode());
		displayProgramLevel = repo.create(displayProgramLevel);
		return displayProgramLevel;//repo.getEntityById(StDisplayProgramLevel.class, displayProgramLevel.getId());
	}		
	
}

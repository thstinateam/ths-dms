package ths.dms.core.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.EquipLender;
import ths.dms.core.entities.ImportFileData;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ArithmeticEnum;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

public interface CommonDAO {
	/**
	 * Lay ngay gio he thong
	 * 
	 * @return
	 * @throws DataAccessException
	 */
	Date getSysDate() throws DataAccessException;

	Date getFirstDateOfMonth() throws DataAccessException;
	
	Date getLastDate(Date date) throws DataAccessException;

	Date getYesterday() throws DataAccessException;

	Date getDateBySysdateForNumberDay(Integer numberDay, boolean trunc) throws DataAccessException;

	Date getDateByDateForNumberDay(Date date, Integer numberDay, boolean trunc) throws DataAccessException;

	Integer compareDateWithoutTime(Date date1, Date date2) throws DataAccessException;

	BigDecimal checkValidDateDate(String dateStr) throws DataAccessException;

	ShopParam getShopParamByType(Long shopId, String type) throws DataAccessException;

	Integer getIslevel(Long shopId) throws DataAccessException;
	
	/**
	 * Tao moi entity cho bat ky loai doi tuong nao
	 * @author tientv11
	 * @param <T>
	 * @param object
	 * @return
	 * @throws DataAccessException
	 */
	<T> T createEntity(T object) throws DataAccessException;
	
	/**
	 *  Tao moi List Entity cho bat ky loai doi tuong nao
	 *  @author tientv11
	 * @param <T>
	 * @param lstObjects
	 * @return
	 * @throws DataAccessException
	 */
	<T> List<T> creatListEntity(List<T> lstObjects ) throws DataAccessException;
	
	
	/**
	 * Update Entity bat ky loai doi tuong nao
	 * @author tientv11
	 * @param object
	 * @throws DataAccessException
	 */
	void updateEntity(Object object) throws DataAccessException;
	
	/**
	 * Update list entity bat ky loai doi tuong nao
	 * @author tientv11
	 * @param <T>
	 * @param lstObjects
	 * @return
	 * @throws DataAccessException
	 */
	<T> List<T> updateListEntity(List<T> lstObjects) throws DataAccessException;
	
	
	/**
	 * Lay doi tuong theo id
	 * @author tientv11
	 * @param <T>
	 * @param clazz
	 * @param id
	 * @return
	 * @throws DataAccessException
	 */
	<T> T getEntityById(Class<T> clazz, Serializable id) throws DataAccessException;
	
	/**
	 * Xoa doi tuong
	 * @author tientv11
	 * @param object
	 * @throws DataAccessException
	 */
	void deleteEntity(Object object) throws DataAccessException;

	List<ImportFileData> getListImportFileData(KPaging<ImportFileData> kPaging,
			Long staffId, List<Integer> listStatus, String serverId)
			throws DataAccessException;

	ImportFileData createImportFileData(ImportFileData importFileData)
			throws DataAccessException;

	ImportFileData getImportFileData(String fileName, Long staffId)
			throws DataAccessException;

	List<Staff> getListStaffImportFile(KPaging<Staff> kPaging,
			List<Integer> listStatus, String serverId)
			throws DataAccessException;


	/**
	 * Thuc hien phep tru hoac cong 2 ngay
	 * 
	 * @author hunglm16
	 * @param date1
	 * @param date2
	 * @param arithmetic cac phep tinh toan hoc
	 * @return gia tri theo phep toan
	 * @throws DataAccessException
	 * @since 29/10/2015 
	 */
	int arithmeticForDateByDateWithTrunc(Date date1, Date date2, ArithmeticEnum arithmetic) throws DataAccessException;
	
	/**
	 * Lay danh sach Equip_Lender
	 * 
	 * @author hunglm16
	 * @param [shopId]: id Don vi - Khong duoc de rong
	 * @since August 18, 2015
	 * */
	EquipLender getFirtInformationEquipLender(Long shopId) throws DataAccessException;
	
	/**
	 * Lay ky dong co ngay den lon nhat cua kenh thiet bi
	 * 
	 * @author hunglm16
	 * @since Jun 07,2015
	 * */
	Long getEquipPeriodIdByCloseForMaxToDate() throws DataAccessException;
}

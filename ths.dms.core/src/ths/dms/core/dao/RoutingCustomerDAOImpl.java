package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.RoutingCustomer;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.FlagTime;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.VisitPlanType;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.vo.CustomerPositionVO;
import ths.dms.core.entities.vo.RoutingCustomerVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class RoutingCustomerDAOImpl implements RoutingCustomerDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private CommonDAO cmmDAO;

	@Override
	public RoutingCustomer createRoutingCustomer(RoutingCustomer routingCustomer) throws DataAccessException {
		if (routingCustomer == null) {
			throw new IllegalArgumentException("routingCustomer");
		}
		routingCustomer.setShop(routingCustomer.getRouting().getShop());
		routingCustomer.setCreateDate(cmmDAO.getSysDate());
		routingCustomer.setFrequency(this.calcTansuat(routingCustomer));
		repo.create(routingCustomer);
		return repo.getEntityById(RoutingCustomer.class, routingCustomer.getId());
	}
	
	private Integer calcTansuat(RoutingCustomer rc) {
		int week = rc.getWeek1().getValue()+rc.getWeek2().getValue()+rc.getWeek3().getValue()+rc.getWeek4().getValue();
		int day = rc.getMonday().getValue()+rc.getTuesday().getValue()+rc.getWednesday().getValue()
				+rc.getThursday().getValue()+rc.getFriday().getValue()+rc.getSaturday().getValue()+rc.getSunday().getValue();
		return week * day;
	}

	@Override
	public void deleteRoutingCustomer(RoutingCustomer routingCustomer) throws DataAccessException {
		if (routingCustomer == null) {
			throw new IllegalArgumentException("routingCustomer");
		}
		repo.delete(routingCustomer);
	}

	@Override
	public RoutingCustomer getRoutingCustomerById(Long id) throws DataAccessException {
		return repo.getEntityById(RoutingCustomer.class, id);
	}

	@Override
	public void updateRoutingCustomer(RoutingCustomer routingCustomer) throws DataAccessException {
		if (routingCustomer == null) {
			throw new IllegalArgumentException("routingCustomer");
		}
		routingCustomer.setUpdateDate(cmmDAO.getSysDate());
		routingCustomer.setFrequency(this.calcTansuat(routingCustomer));
		repo.update(routingCustomer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.RoutingCustomerDAO#createRoutingCustomer(java.util
	 * .List)
	 */
	@Override
	public void createRoutingCustomer(List<RoutingCustomer> listRoutingCustomer) throws DataAccessException {
		// TODO Auto-generated method stub
		if (listRoutingCustomer == null) {
			throw new IllegalArgumentException("listRoutingCustomer is null");
		}
		for (RoutingCustomer rc : listRoutingCustomer) {
			rc.setFrequency(this.calcTansuat(rc));
		}
		repo.create(listRoutingCustomer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.RoutingCustomerDAO#getListRoutingCustomer(java.lang
	 * .Long)
	 */
	@Override
	public List<RoutingCustomer> getListRoutingCustomer(KPaging<RoutingCustomer> kPaging, Long routingId, ActiveType status, Integer saleDate, Boolean hasOrderBySeq, Long shopId, Boolean hasChildShop, Long superId) throws DataAccessException {
		if (routingId == null) {
			throw new IllegalArgumentException("routingId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select rc.* from routing_customer rc, routing r, customer c");
		sql.append(" where rc.routing_id = ?");
		params.add(routingId);
		sql.append(" and rc.routing_id = r.routing_id");
		sql.append(" and r.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (shopId != null) {
			sql.append(" and r.shop_id = ?");
			params.add(shopId);
		} else {
			if (superId != null) {
				sql.append(" and r.shop_id in (select shop_id from staff where staff_owner_id = ?)");
				params.add(superId);
			}
		}
		sql.append(" and rc.customer_id = c.customer_id");
		sql.append(" and c.status = ?");
		params.add(ActiveType.RUNNING.getValue());

		//		sql.append(" and mod(to_char(sysdate,'iw')- rc.start_week, rc.week_interval) = 0 ");
		//		sql.append(" and (to_char(sysdate,'iw')- rc.start_week) >= 0");
		if (status != null) {
			sql.append(" and rc.status = ?");
			params.add(status.getValue());
		} else {
			sql.append(" and rc.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (saleDate != null) {
			switch (saleDate) {
			case 1:
				sql.append(" and rc.sunday = ?");
				if (null != hasOrderBySeq && hasOrderBySeq == true) {
					sql.append(" order by nvl(rc.seq8, 0)");
				}
				break;
			case 2:
				sql.append(" and rc.monday = ?");
				if (null != hasOrderBySeq && hasOrderBySeq == true) {
					sql.append(" order by nvl(rc.seq2, 0)");
				}
				break;
			case 3:
				sql.append(" and rc.tuesday = ?");
				if (null != hasOrderBySeq && hasOrderBySeq == true) {
					sql.append(" order by nvl(rc.seq3, 0)");
				}
				break;
			case 4:
				sql.append(" and rc.wednesday = ?");
				if (null != hasOrderBySeq && hasOrderBySeq == true) {
					sql.append(" order by nvl(rc.seq4, 0)");
				}
				break;
			case 5:
				sql.append(" and rc.thursday = ?");
				if (null != hasOrderBySeq && hasOrderBySeq == true) {
					sql.append(" order by nvl(rc.seq5, 0)");
				}
				break;
			case 6:
				sql.append(" and rc.friday = ?");
				if (null != hasOrderBySeq && hasOrderBySeq == true) {
					sql.append(" order by nvl(rc.seq6, 0)");
				}
				break;
			case 7:
				sql.append(" and rc.saturday = ?");
				if (null != hasOrderBySeq && hasOrderBySeq == true) {
					sql.append(" order by nvl(rc.seq7, 0)");
				}
				break;
			default:
				break;
			}
			params.add(VisitPlanType.GO.getValue());
		}
		if (null == hasOrderBySeq || hasOrderBySeq == false) {
			sql.append(" order by upper(c.customer_code),  rc.status desc, rc.create_date desc");
		}
		if (kPaging != null)
			return repo.getListBySQLPaginated(RoutingCustomer.class, sql.toString(), params, kPaging);
		else
			return repo.getListBySQL(RoutingCustomer.class, sql.toString(), params);
	}

	@Override
	public List<RoutingCustomer> getListRoutingCustomerByImport(RoutingCustomer roucus) throws DataAccessException {
		//@flag[0, 1, 2]: qua khu, hien tai, tuong lai
		if (roucus == null) {
			throw new IllegalArgumentException("roucus is null");
		}
		if (roucus.getRouting() == null || roucus.getRouting().getId() == null) {
			throw new IllegalArgumentException("getRouting is null");
		}
		if (roucus.getCustomer() == null || roucus.getCustomer().getId() == null) {
			throw new IllegalArgumentException("getRouting is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT rc.* FROM routing_customer rc JOIN routing r ON rc.routing_id = r.routing_id JOIN customer cus ON rc.customer_id = cus.customer_id");
		sql.append(" where rc.routing_id = ?");
		params.add(roucus.getRouting().getId());
		sql.append(" and cus.customer_id = ?");
		params.add(roucus.getCustomer().getId());
		sql.append(" and r.shop_id = ?");
		params.add(roucus.getShop().getId());
		sql.append(" and r.status = 1 and rc.status = 1 and cus.status = 1");
		if (roucus.getEndDate() != null) {
			sql.append(" 	and ((rc.start_date <= TRUNC(?) and (rc.end_date is null or rc.end_date >= TRUNC(?)))");
			params.add(roucus.getEndDate());
			params.add(roucus.getEndDate());
			sql.append(" 		or (rc.start_date <= TRUNC(?) and (rc.end_date is null or rc.end_date >= TRUNC(?)))");
			params.add(roucus.getStartDate());
			params.add(roucus.getStartDate());
			sql.append(" 		or (rc.start_date <= TRUNC(?) and (rc.end_date is null or rc.end_date >= TRUNC(?)))");
			params.add(roucus.getStartDate());
			params.add(roucus.getEndDate());
			sql.append(" 		or (rc.start_date >= TRUNC(?) and rc.end_date <= TRUNC(?))");
			params.add(roucus.getStartDate());
			params.add(roucus.getEndDate());
			sql.append(" 		) ");
		} else {
			sql.append(" and (rc.end_date is null or rc.end_date >= TRUNC(?)) ");
			params.add(roucus.getStartDate());
		}
		return repo.getListBySQL(RoutingCustomer.class, sql.toString(), params);
	}

	@Override
	public List<RoutingCustomerVO> getListRoutingCustomerNew(KPaging<RoutingCustomer> kPaging, Long routingId, ActiveType status, Integer saleDate, Boolean hasOrderBySeq, Long shopId, Boolean hasChildShop, Long superId) throws DataAccessException {
		if (routingId == null) {
			throw new IllegalArgumentException("routingId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select ");
		sql.append("   c.customer_id as customerId, ");
		sql.append("   c.short_code as shortCode, ");
		sql.append("   c.customer_name as customerName, ");
		sql.append("   c.address as address, ");
		sql.append("   c.lat as lat, ");
		sql.append("   c.lng as lng, ");

		sql.append("   MIN(rc.seq2)       AS seq2, ");
		sql.append("     MIN(rc.seq3)       AS seq3, ");
		sql.append("     MIN(rc.seq4)       AS seq4, ");
		sql.append("     MIN(rc.seq5)       AS seq5, ");
		sql.append("     MIN(rc.seq6)       AS seq6, ");
		sql.append("     MIN(rc.seq7)       AS seq7, ");
		sql.append("     MIN(rc.seq8)       AS seq8, ");
		sql.append("     MIN(rc.seq)        AS seq ");

		sql.append(" from ");
		sql.append(" routing_customer rc join customer c on rc.customer_id = c.customer_id ");
		sql.append(" and c.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" join routing r on rc.routing_id = r.routing_id ");
		sql.append(" where ");
		sql.append(" rc.status = 1 ");
		sql.append(" and (rc.end_date is null or rc.end_date >= trunc(sysdate))  ");
		if (saleDate != null) {
			switch (saleDate) {
			case 1:
				sql.append(" and rc.sunday = ? ");
				break;
			case 2:
				sql.append(" and rc.monday = ? ");
				break;
			case 3:
				sql.append(" and rc.tuesday = ? ");
				break;
			case 4:
				sql.append(" and rc.wednesday = ? ");
				break;
			case 5:
				sql.append(" and rc.thursday = ? ");
				break;
			case 6:
				sql.append(" and rc.friday = ? ");
				break;
			case 7:
				sql.append(" and rc.saturday = ? ");
				break;
			default:
				sql.append(" and 1 = 1 ");
				break;
			}
			params.add(VisitPlanType.GO.getValue());
		}
		if (routingId != null) {
			sql.append(" and rc.routing_id = ? ");
			params.add(routingId);
		}
		if (shopId != null) {
			sql.append(" and r.shop_id = ?");
			params.add(shopId);
		} else {
			if (superId != null) {
				sql.append(" and r.shop_id in (select shop_id from staff where staff_id = ?)");
				params.add(superId);
			}
		}
		sql.append("  and r.status = 1  ");
		sql.append("  group by c.customer_id, c.short_code, c.customer_name,  c.address, c.lat, c.lng ");
		sql.append("  order by shortCode ");
		String[] fieldNames = { "customerId", "shortCode", "customerName", "address", "lat", "lng", "seq2", "seq3", "seq4", "seq5", "seq6", "seq7", "seq8", "seq" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.DOUBLE, StandardBasicTypes.DOUBLE, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(RoutingCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RoutingCustomerVO> getListRusCusByEndateNextSysDate() throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("  select distinct  ");
		sql.append("  (select short_code from customer where status = 1 and customer_id = rcus.customer_id) as shortCode,  ");
		sql.append("  (select routing_code from routing where status = 1 and routing_id = rcus.routing_id) as routingCode,  ");
		sql.append("  to_char(rcus.start_date, 'dd/MM/yyyy') as startDateStr,  ");
		sql.append("  to_char(rcus.end_date, 'dd/MM/yyyy') as endDateStr,  ");
		sql.append("  (case when rcus.start_date is not null and rcus.start_date < TRUNC(sysdate+1) then 0  ");
		sql.append("  when rcus.end_date is not null and rcus.end_date < TRUNC(sysdate+1) then 0 else 1 end) isDel  ");
		sql.append("  from routing_customer rcus  ");
		sql.append("  where 1 = 1  ");
		sql.append("  and (rcus.end_date   IS NULL OR (TRUNC(sysdate) < rcus.end_date + 1))  ");
		sql.append("  and rcus.status = 1  ");
		sql.append("  order by startDateStr, endDateStr asc  ");

		String[] fieldNames = { "shortCode", //1
				"routingCode", //2
				"startDateStr", //3
				"endDateStr", "isDel" };

		Type[] fieldTypes = { StandardBasicTypes.STRING, //16
				StandardBasicTypes.STRING, //17
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(RoutingCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RoutingCustomerVO> getListRoutingCustomerByCusOrRouting(Long routingId, Long shopId, Long customerId, String shortCode, String customerName, String address, Date startDate, Date endDate, Integer status) throws DataAccessException {
		if (routingId == null) {
			throw new IllegalArgumentException("routingId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT  ");
		sql.append(" rc.routing_customer_id as routingCustomerId,  ");
		sql.append(" rc.week_interval as weekInterval,  ");
		sql.append(" rc.start_week as startWeek,  ");
		sql.append(" rc. monday as monday,  ");
		sql.append(" rc.tuesday as tuesday,  ");
		sql.append(" rc.wednesday as wednesday,  ");
		sql.append(" rc.thursday as thursday,  ");
		sql.append(" rc.friday as friday,  ");
		sql.append(" rc.saturday as saturday,  ");
		sql.append(" rc.sunday as sunday,  ");
		sql.append(" rc.week1 as week1,  ");
		sql.append(" rc.week2 as week2,  ");
		sql.append(" rc.week3 as week3,  ");
		sql.append(" rc.week4 as week4,  ");
		sql.append("  to_char(rc.start_date, 'dd/MM/yyyy') AS startDateStr,  ");
		sql.append("  to_char(rc.end_date  , 'dd/MM/yyyy') AS endDateStr,  ");
		sql.append(" r.routing_id as routingId,  ");
		sql.append(" r.routing_name as routingName,  ");
		sql.append(" cus.customer_id as customerId,  ");
		sql.append(" cus.short_code as shortCode,  ");
		sql.append(" cus.customer_name as customerName,  ");
		sql.append(" cus.customer_code as customerCode,  ");
		sql.append(" cus.address as address,  ");
		sql.append(" cus.lat as lat,  ");
		sql.append(" cus.lng as lng,  ");
		sql.append(" (CASE WHEN rc.end_date IS NOT NULL AND rc.end_date < TRUNC(sysdate) THEN 0 ELSE 1 END) as status,  ");
		sql.append(" (CASE   ");
		sql.append("  WHEN rc.end_date IS NOT NULL AND rc.end_date   < TRUNC(sysdate) THEN 2   ");
		sql.append("  WHEN rc.start_date < TRUNC(sysdate) and (rc.end_date is null or rc.end_date >= TRUNC(sysdate)) THEN 0   ");
		sql.append("  ELSE 1   ");
		sql.append("  END) isDel,   ");
		sql.append(" rc.FREQUENCY tansuat ");
		
		sql.append(" FROM routing_customer rc join routing r on rc.routing_id = r.routing_id  ");
		sql.append(" join customer cus on rc.customer_id = cus.customer_id  ");
		sql.append(" WHERE 1 = 1  ");
		if(routingId != null){
			sql.append(" and  rc.routing_id = ?  ");
			params.add(routingId);
		}
		if(customerId != null){
			sql.append(" and cus.customer_id  = ?");
			params.add(customerId);
		}
//		sql.append(" and r.status        = 1  ");
		if(shopId!=null){
			sql.append(" and r.shop_id = ?  ");
			params.add(shopId);
		}
		
		if (!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append(" and cus.short_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append(" and unicode2english(cus.customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(customerName.toLowerCase())));
		}
		if (!StringUtility.isNullOrEmpty(address)) {
			sql.append(" and unicode2english(cus.address) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(address.toLowerCase())));
		}
		if(startDate!=null && endDate!=null){
			
			sql.append(" and ((rc.start_date <= trunc(?) and (rc.end_date is null or rc.end_date >= trunc(?)))  ");
			params.add(endDate);
			params.add(endDate);
			sql.append("	or (rc.start_date <= trunc(?) and (rc.end_date is null or rc.end_date >= trunc(?)))  ");
			params.add(startDate);
			params.add(startDate);
			sql.append("	or (rc.start_date <= trunc(?) and (rc.end_date is null or rc.end_date >= trunc(?)))  ");
			params.add(startDate);
			params.add(endDate);
			sql.append("	or (rc.start_date >= trunc(?) and rc.end_date <= trunc(?))  ");
			params.add(startDate);
			params.add(endDate);
			sql.append(" )  ");
			
		}else if(startDate!=null && endDate==null){
			sql.append(" and (rc.end_date is null or rc.end_date >= trunc(?)) ");
			params.add(startDate);
		}else if(startDate==null && endDate!=null){
			sql.append(" and rc.start_date <= trunc(?) ");
			params.add(endDate);
		}
		if(status != 1){
			sql.append(" AND (rc.end_date is not null and rc.end_date < TRUNC(sysdate)) ");
		}else{
			sql.append(" AND (rc.end_date is null or rc.end_date >= TRUNC(sysdate)) ");
		}
		sql.append(" and cus.status        = 1  ");
		sql.append(" and rc.status = 1 ");
		sql.append(" ORDER BY cus.short_code, startDateStr DESC, endDateStr DESC ");
		
		String[] fieldNames = { "routingCustomerId", //1
				"weekInterval", //2
				"startWeek", //3
				"monday", //4
				"tuesday", //5
				"wednesday", //6
				"thursday",//7
				"friday", //8
				"saturday", //9
				"sunday", //10
				"week1", //10
				"week2", //10
				"week3", //10
				"week4", //10
				"startDateStr", //11
				"endDateStr", //12
				"routingId", //13
				"routingName", //14
				"customerId", //15
				"shortCode",//16
				"customerName",//17
				"customerCode",//17
				"address",//18
				"lat", //19
				"lng", //20
				"status", //21
				"isDel",//22
				"tansuat"
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, //1
				StandardBasicTypes.INTEGER, //2
				StandardBasicTypes.INTEGER,//3
				StandardBasicTypes.INTEGER, //4
				StandardBasicTypes.INTEGER, //5
				StandardBasicTypes.INTEGER,//6
				StandardBasicTypes.INTEGER, //7
				StandardBasicTypes.INTEGER,//8
				StandardBasicTypes.INTEGER, //9
				StandardBasicTypes.INTEGER,//6
				StandardBasicTypes.INTEGER, //7
				StandardBasicTypes.INTEGER,//8
				StandardBasicTypes.INTEGER, //9
				StandardBasicTypes.INTEGER, //10
				StandardBasicTypes.STRING, //11
				StandardBasicTypes.STRING, //12
				StandardBasicTypes.LONG, //13
				StandardBasicTypes.STRING,//14
				StandardBasicTypes.LONG,  //15
				StandardBasicTypes.STRING, //16
				StandardBasicTypes.STRING,  //17
				StandardBasicTypes.STRING,  //17
				StandardBasicTypes.STRING, //18
				StandardBasicTypes.DOUBLE, //19
				StandardBasicTypes.DOUBLE, //20
				StandardBasicTypes.INTEGER, //21
				StandardBasicTypes.INTEGER, //22
				StandardBasicTypes.INTEGER
		};
		return repo.getListByQueryAndScalar(RoutingCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RoutingCustomerVO> getListRouCusWitFDateMinByListCustomerID(List<Long> lstCusId, Long routeId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select roucus.customer_id as customerId,  ");
		sql.append("  cus.short_code as shortCode,  ");
		sql.append("  roucus.routing_id as routingId,   ");
		sql.append(" to_char(min(roucus.start_date), 'dd/MM/yyyy') as startDateStr,  ");
		sql.append(" r.routing_code as routingCode  ");

		sql.append(" from routing_customer roucus join customer cus on roucus.customer_id = cus.customer_id ");
		sql.append(" join routing r on roucus.routing_id = r.routing_id ");
		sql.append(" WHERE 1 = 1  ");

		if (lstCusId != null && lstCusId.size() > 0) {
			sql.append(" and roucus.customer_id  in( -1");
			for (Long id : lstCusId) {
				sql.append(" ,?");
				params.add(id);
			}
			sql.append(" ) ");
		}

		if (routeId != null) {
			sql.append(" and roucus.routing_id <> ? ");
			params.add(routeId);
		}
		sql.append(" and roucus.status    > -1  ");
		sql.append(" and roucus.start_date > trunc(sysdate + 1) ");
		sql.append(" group by (roucus.routing_id, r.routing_code), (roucus.customer_id, cus.short_code)  ");
		String[] fieldNames = { "customerId", //2
				"shortCode", "routingId", //1
				"startDateStr", "routingCode", };

		Type[] fieldTypes = { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //1
				StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(RoutingCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RoutingCustomerVO> getListRouCusWitFDateMinByFlag(List<Long> lstCusId, Long routeId, Long shopId, Boolean flag) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select roucus.customer_id as customerId,  ");
		sql.append(" cus.short_code as shortCode,  ");
		sql.append(" min(roucus.start_date) as startDate  ");

		sql.append(" from routing_customer roucus join customer cus on roucus.customer_id = cus.customer_id ");
		sql.append(" join routing r on roucus.routing_id = r.routing_id ");
		sql.append(" WHERE 1 = 1  ");
		if (shopId != null) {
			sql.append(" and r.shop_id = ? ");
			params.add(shopId);
		}
		sql.append(" and roucus.start_date > trunc(sysdate + 1) ");
		sql.append(" and roucus.status    > -1  ");
		if (lstCusId != null && lstCusId.size() > 0) {
			sql.append(" and roucus.customer_id  in(-1");
			for (Long id : lstCusId) {
				sql.append(", ?");
				params.add(id);
			}
			sql.append(" ) ");
		}

		sql.append(" group by (roucus.customer_id, cus.short_code)  ");
		String[] fieldNames = { "customerId", //2
				"shortCode", "startDate" };

		Type[] fieldTypes = { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //1
				StandardBasicTypes.DATE };
		return repo.getListByQueryAndScalar(RoutingCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RoutingCustomer> getListRouCusErrByStartDate(Long customerId, Long routingId, Date fDate, Long shopId, Long rouCusId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select * from routing_customer  ");
		sql.append(" where status = 1  ");
		if (rouCusId != null) {
			sql.append(" and routing_customer_id <> ?   ");
			params.add(rouCusId);
		}
		if (shopId != null) {
			sql.append(" and routing_id in(select routing_id from routing where shop_id = ?) ");
			params.add(shopId);
			sql.append(" AND customer_id in (SELECT customer_id  FROM customer WHERE shop_id = ?) ");
			params.add(shopId);
		}
		if (customerId != null) {
			sql.append(" and customer_id = ?  ");
			params.add(customerId);
		}
		if (routingId != null) {
			sql.append(" and routing_id = ?  ");
			params.add(routingId);
		}
		if (fDate != null) {
			sql.append(" and start_date <= trunc(?) and (end_date is null or end_date > trunc(?) - 1)  ");
			params.add(fDate);
			params.add(fDate);
		} else {
			sql.append(" and start_date > trunc(sysdate) ");
		}
		sql.append(" order by start_date ");
		return repo.getListBySQL(RoutingCustomer.class, sql.toString(), params);
	}

	@Override
	public RoutingCustomer getRoutingCustomerByDate(Long customerId, Long shopId, Date orderDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select a.* ");
		sql.append(" from ROUTING_CUSTOMER a, visit_plan b, routing c ");
		sql.append(" where b.routing_id = a.routing_id ");
		sql.append(" and c.routing_id = a.routing_id ");
		sql.append(" and a.status = 1 ");
		sql.append(" and b.status = 1 ");
		sql.append(" and c.status = 1 ");
		sql.append(" and b.from_date < trunc(?)+1 and (b.to_date >= trunc(?) or b.to_date is null) ");
		params.add(orderDate);
		params.add(orderDate);
		sql.append(" and (  (to_char(?, 'D') = 1 and a.sunday = 1 ) ");
		params.add(orderDate);
		sql.append("     or (to_char(?, 'D') = 2 and a.monday = 1) ");
		params.add(orderDate);
		sql.append("     or (to_char(?, 'D') = 3 and a.tuesday = 1) ");
		params.add(orderDate);
		sql.append("     or (to_char(?, 'D') = 4 and a.wednesday = 1) ");
		params.add(orderDate);
		sql.append("     or (to_char(?, 'D') = 5 and a.thursday = 1) ");
		params.add(orderDate);
		sql.append("     or (to_char(?, 'D') = 6 and a.friday = 1) ");
		params.add(orderDate);
		sql.append("     or (to_char(?, 'D') = 7 and a.saturday = 1) ");
		params.add(orderDate);
		sql.append("     ) ");
		sql.append(" and a.customer_id = ? ");
		sql.append(" and c.shop_id = ? ");
		params.add(customerId);
		params.add(shopId);
		return repo.getFirstBySQL(RoutingCustomer.class, sql.toString(), params);
	}

	
	@Override
	public RoutingCustomer getRoutingCustomerByDateAndStaff(Long customerId, Long shopId, Date orderDate, Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select a.* ");
		sql.append(" from ROUTING_CUSTOMER a, visit_plan b, routing c ");
		sql.append(" where b.routing_id = a.routing_id ");
		sql.append(" and c.routing_id = a.routing_id ");
		sql.append(" and a.status = 1 ");
		sql.append(" and b.status = 1 ");
		sql.append(" and c.status = 1 ");
		sql.append(" and a.start_date < trunc(?)+1 and (a.end_date >= trunc(?) or a.end_date is null) ");
		params.add(orderDate);
		params.add(orderDate);
		sql.append(" and b.from_date < trunc(?)+1 and (b.to_date >= trunc(?) or b.to_date is null) ");
		params.add(orderDate);
		params.add(orderDate);
		/*sql.append(" and (  (to_char(?, 'D') = 1 and a.sunday = 1 ) ");
		params.add(orderDate);
		sql.append("     or (to_char(?, 'D') = 2 and a.monday = 1) ");
		params.add(orderDate);
		sql.append("     or (to_char(?, 'D') = 3 and a.tuesday = 1) ");
		params.add(orderDate);
		sql.append("     or (to_char(?, 'D') = 4 and a.wednesday = 1) ");
		params.add(orderDate);
		sql.append("     or (to_char(?, 'D') = 5 and a.thursday = 1) ");
		params.add(orderDate);
		sql.append("     or (to_char(?, 'D') = 6 and a.friday = 1) ");
		params.add(orderDate);
		sql.append("     or (to_char(?, 'D') = 7 and a.saturday = 1) ");
		params.add(orderDate);
		sql.append("     ) ");*/
		sql.append(" and a.customer_id = ? ");
		params.add(customerId);
		sql.append(" and c.shop_id = ? ");
		params.add(shopId);
		if (staffId != null) {
			sql.append(" and b.staff_id = ? ");
			params.add(staffId);
		}
		return repo.getFirstBySQL(RoutingCustomer.class, sql.toString(), params);
	}
	
	
	
	@Override
	public RoutingCustomer getRoutingCustomerByCustomerInCurrent(Long customerId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from routing_customer a where customer_id = ? and status = ?");
		params.add(customerId);
		params.add(ActiveType.RUNNING.getValue());

		sql.append(" and (  (to_char(sysdate, 'D') = 1 and a.sunday = 1 ) ");
		sql.append("     or (to_char(sysdate, 'D') = 2 and a.monday = 1) ");
		sql.append("     or (to_char(sysdate, 'D') = 3 and a.tuesday = 1) ");
		sql.append("     or (to_char(sysdate, 'D') = 4 and a.wednesday = 1) ");
		sql.append("     or (to_char(sysdate, 'D') = 5 and a.thursday = 1) ");
		sql.append("     or (to_char(sysdate, 'D') = 6 and a.friday = 1) ");
		sql.append("     or (to_char(sysdate, 'D') = 7 and a.saturday = 1) ");
		sql.append("     ) ");

		return repo.getFirstBySQL(RoutingCustomer.class, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.RoutingCustomerDAO#updateRoutingCustomer(java.util
	 * .List)
	 */
	@Override
	public void updateRoutingCustomer(List<RoutingCustomer> lstRoutingCustomer) throws DataAccessException {
		// TODO Auto-generated method stub
		if (lstRoutingCustomer == null) {
			throw new IllegalArgumentException("lstRoutingCustomer is null");
		} else {
			/*for (RoutingCustomer item : lstRoutingCustomer) {
			}*/
		}

		repo.update(lstRoutingCustomer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ths.dms.core.dao.RoutingCustomerDAO#checkExit(java.lang.Long,
	 * java.lang.String)
	 */
	@Override
	public Boolean checkExit(Long routingId, String listCustomerId) throws DataAccessException {
		// TODO Auto-generated method stub
		String sql = "select count(*) as count from routing_customer where routing_id = ? and status <> ? and customer_id in ";
		sql += listCustomerId;
		List<Object> params = new ArrayList<Object>();
		params.add(routingId);
		params.add(ActiveType.DELETED.getValue());
		Object o = repo.getObjectByQuery(sql, params);
		Long count = Long.valueOf(o.toString());
		if (count > 0) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.RoutingCustomerDAO#getListCustomerForStaff(java.
	 * lang.Long)
	 */
	@Override
	public List<CustomerPositionVO> getListCustomerForStaff(Long staffId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select c.customer_code as customerCode, c.customer_name as customerName, c.short_code as shortCode, c.lat as customerLat, c.lng as customerLng");
		sql.append(" from routing_customer rc, visit_plan vp, customer c, routing r");
		sql.append(" where rc.routing_id = vp.routing_id and rc.customer_id = c.customer_id ");
		sql.append(" and r.routing_id = vp.routing_id");
		sql.append(" and c.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and r.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and rc.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and vp.staff_id = ? and vp.from_date <= trunc(sysdate + 1) and (vp.to_date > trunc(sysdate) or vp.to_date is null) and vp.status = ? ");
		params.add(staffId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and mod ((to_char(sysdate,'iw') - rc.START_WEEK), rc.WEEK_INTERVAL) = 0");
		sql.append(" AND to_char(sysdate,'iw') - rc.START_WEEK >= 0");
		sql.append(" 	and (  (to_char(sysdate, 'D') = 1 and rc.sunday = 1 )");
		sql.append("     or (to_char(sysdate, 'D') = 2 and rc.monday = 1) ");
		sql.append("     or (to_char(sysdate, 'D') = 3 and rc.tuesday = 1) ");
		sql.append("     or (to_char(sysdate, 'D') = 4 and rc.wednesday = 1) ");
		sql.append("     or (to_char(sysdate, 'D') = 5 and rc.thursday = 1) ");
		sql.append("     or (to_char(sysdate, 'D') = 6 and rc.friday = 1) ");
		sql.append("     or (to_char(sysdate, 'D') = 7 and rc.saturday = 1) ");
		sql.append("     ) ");

		String[] fieldNames = { "customerCode", // 0
				"customerName", // 1
				"shortCode", // 1'
				"customerLat", // 2
				"customerLng", // 2
		};

		Type[] fieldTypes = { StandardBasicTypes.STRING, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.DOUBLE, // 2
				StandardBasicTypes.DOUBLE, // 2
		};

		return repo.getListByQueryAndScalar(CustomerPositionVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public void updateSeqOfDisableCustomer(Long customerId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" update routing_customer ");
		sql.append(" set seq2 = null, seq3 = null, seq4 = null, seq5 = null, seq6 = null, seq7 = null, seq8 = null ");
		sql.append(" where routing_customer_id in ( ");
		sql.append("     select routing_customer_id ");
		sql.append("     from visit_plan vp, routing r, routing_customer rc ");
		sql.append("     where vp.routing_id = r.routing_id  ");
		sql.append("         and r.routing_id = rc.routing_id ");
		sql.append("         and trunc(vp.from_date) <= trunc(sysdate) ");
		sql.append("         and (trunc(vp.to_date) > trunc(sysdate) or vp.to_date is null) ");
		sql.append("         and rc.customer_id = ? ");
		sql.append("          ) ");

		params.add(customerId);

		repo.executeSQLQuery(sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.RoutingCustomerDAO#offRoutingCustomerWithRoutingId
	 * (java.lang.Long)
	 */
	@Override
	public void offRoutingCustomerWithRoutingId(Long routingId, String updateUser, Date updateDate) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" update routing_customer set status = ?, update_date = ?, update_user = ? where routing_id = ? ");
		params.add(ActiveType.DELETED.getValue());
		params.add(updateDate);
		params.add(updateUser);
		params.add(routingId);
		repo.executeSQLQuery(sql.toString(), params);
	}

	@Override
	public RoutingCustomer getRoutingCustomerByRoutingIdAndCustomerId(Long routingId, Long customerId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from routing_customer ");
		sql.append(" where 1 = 1 ");
		if (customerId != null) {
			sql.append(" and customer_id = ? ");
			params.add(customerId);
		}
		if (routingId != null) {
			sql.append(" and routing_id = ? ");
			params.add(routingId);
		}
		sql.append(" and status = 1 ");
		return repo.getEntityBySQL(RoutingCustomer.class, sql.toString(), params);
	}

	@Override
	public List<RoutingCustomer> getRoutingCustomerByFilterAndListID(List<Long> lstCustomerID, List<Long> lstRoutingID, RoutingCustomerFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("  select * from routing_customer  ");
		sql.append("  where 1 = 1  ");
		sql.append("  and status = 1  ");
		if (lstCustomerID != null && lstCustomerID.size() > 0) {
			sql.append("  and customer_id in(-1  ");
			for (Long id : lstCustomerID) {
				sql.append("  , ?  ");
				params.add(id);
			}
			sql.append(" ) ");
			if (filter.getRoutingId() != null) {
				sql.append("  and routing_id = ?  ");
				params.add(filter.getRoutingId());
			}
		}
		if (lstRoutingID != null && lstRoutingID.size() > 0) {
			sql.append("  and routing_id in(-1  ");
			for (Long id : lstRoutingID) {
				sql.append("  , ?  ");
				params.add(id);
			}
			sql.append(" ) ");
			if (filter.getCustomerId() != null) {
				sql.append("  and customer_id = ?  ");
				params.add(filter.getCustomerId());
			}
		}
		if (filter.getFlag() != null && filter.getFlag()) {
			sql.append("  and start_date < trunc(sysdate) and (end_date is null or end_date > trunc(sysdate))  ");

		} else if (filter.getFlag() != null && !filter.getFlag()) {
			sql.append("  and start_date > trunc(sysdate) and (end_date is null or end_date >= start_date)  ");
		}

		return repo.getListBySQL(RoutingCustomer.class, sql.toString(), params);
	}

	@Override
	public List<RoutingCustomer> getRoutingCustomerByFilter(RoutingCustomerFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("  select * from routing_customer  ");
		sql.append("  where 1 = 1  ");
		sql.append("  and status = 1  ");
		if (filter.getRoutingId() != null) {
			sql.append("  and routing_id = ?  ");
			params.add(filter.getRoutingId());
		}
		if (filter.getCustomerId() != null) {
			sql.append("  and customer_id = ?  ");
			params.add(filter.getCustomerId());
		}
		if (filter.getLstRoutingId() != null && !filter.getLstRoutingId().isEmpty()) {
			sql.append("  and routing_id in(-1  ");
			for (Long id : filter.getLstRoutingId()) {
				sql.append("  , ?  ");
				params.add(id);
			}
			sql.append(" ) ");
		}
		if (filter.getLstCustomerId() != null && !filter.getLstCustomerId().isEmpty()) {
			sql.append("  and customer_id in(-1  ");
			for (Long id : filter.getLstCustomerId()) {
				sql.append("  , ?  ");
				params.add(id);
			}
			sql.append(" ) ");
		}
		if (filter.getFlagTime() != null && FlagTime.BACKTIME.getValue().equals(filter.getFlagTime())) {
			sql.append(" and end_date is null and end_date <= trunc(sysdate) ");

		} 
		if (filter.getFlagTime() != null && FlagTime.SYSTEM.getValue().equals(filter.getFlagTime())) {
			sql.append("  and start_date <= trunc(sysdate) and (end_date is null or end_date > trunc(sysdate)) ");

		}
		if (filter.getFlagTime() != null && FlagTime.NEXTTIME.getValue().equals(filter.getFlagTime())) {
			sql.append(" and start_date > trunc(sysdate) and (end_date is null or end_date >= start_date) ");

		}
		if (filter.getFlagTime() != null && FlagTime.SYSANDNEXTTIME.getValue().equals(filter.getFlagTime())) {
			sql.append(" and end_date is null or end_date >= start_date ");

		}
		//Bo sung them cac dieu kien cho cac nghiep vụ khac neu can thiet
		return repo.getListBySQL(RoutingCustomer.class, sql.toString(), params);
	}

	@Override
	public List<RoutingCustomer> getListRoutingCustomerListRouCusID(List<Long> lstID) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("  select * from routing_customer  ");
		sql.append("  where 1 = 1  ");
		if (lstID != null && lstID.size() > 0) {
			sql.append("  and routing_customer_id in(-1  ");
			for (Long id : lstID) {
				sql.append("  , ?  ");
				params.add(id);
			}
			sql.append(" ) ");
		}
		return repo.getListBySQL(RoutingCustomer.class, sql.toString(), params);
	}

	@Override
	public List<RoutingCustomer> getListRouCusByShortCodeAndShopId(String shortCode, Long shopId, Integer saleDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("  select rc.*   ");
		sql.append("  FROM routing_customer rc JOIN customer c ON rc.customer_id = c.customer_id   ");
		sql.append("  JOIN routing r ON rc.routing_id  = r.routing_id  ");
		sql.append("  WHERE rc.status   = 1  ");
		sql.append("  AND (rc.end_date is null or rc.end_date >= trunc(sysdate))  ");
		if (shopId != null) {
			sql.append("  AND r.shop_id     = ?  ");
			params.add(shopId);
		}
		if (!StringUtility.isNullOrEmpty(shortCode)) {
			sql.append("  AND c.short_code = ?  ");
			params.add(shortCode.trim().toUpperCase());
		}
		sql.append("  AND r.status      = 1  ");
		if (saleDate != null) {
			switch (saleDate) {
			case 1:
				sql.append(" and rc.sunday = ? ");
				break;
			case 2:
				sql.append(" and rc.monday = ? ");
				break;
			case 3:
				sql.append(" and rc.tuesday = ? ");
				break;
			case 4:
				sql.append(" and rc.wednesday = ? ");
				break;
			case 5:
				sql.append(" and rc.thursday = ? ");
				break;
			case 6:
				sql.append(" and rc.friday = ? ");
				break;
			case 7:
				sql.append(" and rc.saturday = ? ");
				break;
			default:
				sql.append(" and 1 = 1 ");
				break;
			}
			params.add(VisitPlanType.GO.getValue());
		}

		return repo.getListBySQL(RoutingCustomer.class, sql.toString(), params);
	}

	@Override
	public List<RoutingCustomer> getListRouCusByListShortCodeAndShopId(List<String> lstShortCode, Long shopId, Integer saleDate, Long routingId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String tmpFirt = "-1";
		sql.append("  select rc.*   ");
		sql.append("  FROM routing_customer rc JOIN customer c ON rc.customer_id = c.customer_id   ");
		sql.append("  JOIN routing r ON rc.routing_id  = r.routing_id  ");
		sql.append("  WHERE rc.status   = 1  ");
		sql.append("  AND (rc.end_date is null or rc.end_date >= trunc(sysdate))  ");
		if (shopId != null) {
			sql.append("  AND r.shop_id     = ?  ");
			params.add(shopId);
		}
		if (routingId != null) {
			sql.append("  and r.routing_id = ?  ");
			params.add(routingId);
		}
		if (lstShortCode != null && lstShortCode.size() > 0) {
			sql.append("  AND c.short_code in (?  ");
			params.add(tmpFirt.trim().toUpperCase());
			for (String code : lstShortCode) {
				sql.append(" , ? ");
				params.add(code.trim().toUpperCase());
			}
			sql.append(" ) ");
		}
		sql.append("  AND r.status      = 1  ");
		if (saleDate != null) {
			switch (saleDate) {
			case 1:
				sql.append(" and rc.sunday = ? ");
				break;
			case 2:
				sql.append(" and rc.monday = ? ");
				break;
			case 3:
				sql.append(" and rc.tuesday = ? ");
				break;
			case 4:
				sql.append(" and rc.wednesday = ? ");
				break;
			case 5:
				sql.append(" and rc.thursday = ? ");
				break;
			case 6:
				sql.append(" and rc.friday = ? ");
				break;
			case 7:
				sql.append(" and rc.saturday = ? ");
				break;
			default:
				sql.append(" and 1 = 1 ");
				break;
			}
			params.add(VisitPlanType.GO.getValue());
		}

		return repo.getListBySQL(RoutingCustomer.class, sql.toString(), params);
	}

	@Override
	public int countSEQBySeqAndRoutingId(Long routingId, Integer seq) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("  SELECT COUNT(1) as count FROM routing_customer   ");
		sql.append("  where status = 1   ");
		if (routingId != null) {
			sql.append("  and routing_id = ?   ");
			params.add(routingId);
		}
		if (seq != null) {
			switch (seq) {
			case 1:
				sql.append(" and seq2 = ? ");
				break;
			case 2:
				sql.append(" and seq3 = ? ");
				break;
			case 3:
				sql.append(" and seq3 = ? ");
				break;
			case 4:
				sql.append(" and seq4 = ? ");
				break;
			case 5:
				sql.append(" and seq5 = ? ");
				break;
			case 6:
				sql.append(" and seq6 = ? ");
				break;
			case 7:
				sql.append(" and seq7 = ? ");
				break;
			case 8:
				sql.append(" and seq8 = ? ");
				break;
			}
			params.add(seq);
		}

		return repo.countBySQL(sql.toString(), params);
	}

	@Override
	public List<RoutingCustomer> getListRoutingCustomersBySeq(Long routingId, Integer seq) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("  SELECT * FROM routing_customer rc   ");
		sql.append("  where rc.status = 1  AND (rc.end_date IS NULL OR rc.end_date   >= TRUNC(sysdate)) ");
		if (routingId != null) {
			sql.append("  and rc.routing_id = ?   ");
			params.add(routingId);
		}
		if (seq != null) {
			switch (seq) {
			case 1:
				sql.append(" and rc.sunday = ? ");
				break;
			case 2:
				sql.append(" and rc.monday = ? ");
				break;
			case 3:
				sql.append(" and rc.tuesday = ? ");
				break;
			case 4:
				sql.append(" and rc.wednesday = ? ");
				break;
			case 5:
				sql.append(" and rc.thursday = ? ");
				break;
			case 6:
				sql.append(" and rc.friday = ? ");
				break;
			case 7:
				sql.append(" and rc.saturday = ? ");
				break;
			default:
				sql.append(" and 1 = 1 ");
				break;
			}
			params.add(VisitPlanType.GO.getValue());
		}

		return repo.getListBySQL(RoutingCustomer.class, sql.toString(), params);
	}

	@Override
	public Boolean countCustomerInRouCusValidImport(String shortCode, String routingCode, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("  SELECT COUNT(1) as count FROM routing_customer   ");
		sql.append("  where status = 1   ");
		sql.append("  and routing_id in (select routing_id from routing where status = 1 and shop_id = ? and upper(routing_code) = ?)   ");
		params.add(shopId);
		params.add(routingCode.trim().toUpperCase());
		sql.append("  and customer_id in (select customer_id from customer where status = 1 and shop_id = ? and upper(short_code) = ?)   ");
		params.add(shopId);
		params.add(shortCode.trim().toUpperCase());
		sql.append("  and ((start_date < trunc(sysdate) and (end_date is null or end_date >= trunc(sysdate))) or start_date >= trunc(sysdate)) ");

		int kq = repo.countBySQL(sql.toString(), params);

		if (kq > 0) {
			return false;
		} else {
			return true;
		}
	}
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PoVnmLot;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class PoVnmLotDAOImpl implements PoVnmLotDAO {

	@Autowired
	private IRepository repo;

	@Override
	public PoVnmLot createPoVnmLot(PoVnmLot poVnmLot) throws DataAccessException {
		if (poVnmLot == null) {
			throw new IllegalArgumentException("poVnmLot");
		}
		repo.create(poVnmLot);
		return repo.getEntityById(PoVnmLot.class, poVnmLot.getId());
	}
	
	@Override
	public void createListPoVnmLot(List<PoVnmLot> listPoVnmLot) throws DataAccessException {
		if (listPoVnmLot == null) {
			throw new IllegalArgumentException("listPoVnmLot");
		}
		repo.create(listPoVnmLot);
	}

	@Override
	public void deletePoVnmLot(PoVnmLot poVnmLot) throws DataAccessException {
		if (poVnmLot == null) {
			throw new IllegalArgumentException("poVnmLot");
		}
		repo.delete(poVnmLot);
	}

	@Override
	public PoVnmLot getPoVnmLotById(long id) throws DataAccessException {
		return repo.getEntityById(PoVnmLot.class, id);
	}
	
	@Override
	public PoVnmLot getPoVnmLotByIdForUpdate(long id) throws DataAccessException {
		String sql = "select * from po_vnm_lot where po_vnm_lot_id = ? for update";
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		return repo.getFirstBySQL(PoVnmLot.class, sql, params);
	}
	
	@Override
	public void updatePoVnmLot(PoVnmLot poVnmLot) throws DataAccessException {
		if (poVnmLot == null) {
			throw new IllegalArgumentException("poVnmLot");
		}
		repo.update(poVnmLot);
	}
	
	@Override
	public void updateListPoVnmLot(List<PoVnmLot> listPoVnmLot) throws DataAccessException {
		if (listPoVnmLot == null) {
			throw new IllegalArgumentException("listPoVnmLot");
		}
		repo.update(listPoVnmLot);
	}

	@Override
	public List<PoVnmLot> getListPoVnmLotByPoVnmId(Long poVnmId)
			throws DataAccessException {
		if (poVnmId == null) {
			throw new IllegalArgumentException("poVnmId is null");
		}
		String sql = "select * from po_vnm_lot where po_vnm_id = ? order by po_vnm_lot_id desc";
		List<Object> params = new ArrayList<Object>();
		params.add(poVnmId);
		return repo.getListBySQL(PoVnmLot.class, sql, params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.PoVnmLotDAO#getPoVnmLotByPoVnmId(long)
	 */
	@Override
	public PoVnmLot getPoVnmLotByPoVnmIdandProductId(Long poVnmDetailId, Long productId)
			throws DataAccessException {
		if (poVnmDetailId == null) {
			throw new IllegalArgumentException("poVnmDetailId is null");
		}
		if (productId == null) {
			throw new IllegalArgumentException("productId is null");
		}
		String sql = "select * from po_vnm_lot where po_vnm_detail_id = ? and product_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(poVnmDetailId);
		params.add(productId);
		return repo.getEntityBySQL(PoVnmLot.class, sql.toString(), params);
	}

	@Override
	public List<PoVnmLot> getListPoVnmLotByPoVnmDetailId(Long poVnmDetailId, Long productId)
			throws DataAccessException {
		if (poVnmDetailId == null) {
			throw new IllegalArgumentException("poVnmDetailId is null");
		}
		String sql = "select * from po_vnm_lot where po_vnm_detail_id = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(poVnmDetailId);
		if (productId != null) {
			sql += " and product_id = ?";
			params.add(productId);
		}
		return repo.getListBySQL(PoVnmLot.class, sql.toString(), params);
	}
}

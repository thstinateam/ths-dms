package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Customer;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.ProductLot;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.DisplayProductGroupType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.enumtype.ProductFilter;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.ProgramObjectType;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.SalePlanOwnerType;
import ths.dms.core.entities.enumtype.ShopProductType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.TreeVOType;
import ths.dms.core.entities.filter.ProductGeneralFilter;
import ths.dms.core.entities.vo.GeneralProductVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PoAutoVO;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.entities.vo.ProductExportVO;
import ths.dms.core.entities.vo.ProductTreeVO;
import ths.dms.core.entities.vo.ProductVO;
import ths.dms.core.entities.vo.ProductVOEx;
import ths.dms.core.entities.vo.ProductsForPoAutoParams;
import ths.dms.core.entities.vo.SalePlanVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.core.entities.vo.rpt.RptCompareBookAndDeliveryProductVO;
import ths.core.entities.vo.rpt.RptF1DataVO;
import ths.core.entities.vo.rpt.Rpt_TDXNTCTVO;
import ths.core.entities.vo.rpt.Rpt_TDXNTCT_NganhVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class ProductDAOImpl implements ProductDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	ShopDAO shopDAO;

	@Override
	public Product createProduct(Product product, LogInfoVO logInfo) throws DataAccessException {
		if (product == null) {
			throw new IllegalArgumentException("product");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (!StringUtility.isNullOrEmpty(product.getProductCode())) {
			product.setProductCode(product.getProductCode().toUpperCase());
		}
		product.setCreateUser(logInfo.getStaffCode());
		product.setCreateDate(commonDAO.getSysDate());
		if (product.getProductName() != null) {
			product.setNameText(Unicode2English.codau2khongdau(product.getProductName()).toLowerCase());
		}
		repo.create(product);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, product, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(Product.class, product.getId());
	}

	@Override
	public void deleteProduct(Product product, LogInfoVO logInfo) throws DataAccessException {
		if (product == null) {
			throw new IllegalArgumentException("product");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(product);
		try {
			actionGeneralLogDAO.createActionGeneralLog(product, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Product getProductById(long id) throws DataAccessException {
		return repo.getEntityById(Product.class, id);
	}

	@Override
	public void updateProduct(Product product, LogInfoVO logInfo) throws DataAccessException {
		if (product == null) {
			throw new IllegalArgumentException("product");
		}
		if (!StringUtility.isNullOrEmpty(product.getProductName())) {
			product.setNameText(Unicode2English.codau2khongdau(product.getProductName()).toLowerCase());
		}
		Product temp = this.getProductById(product.getId());

		if (!StringUtility.isNullOrEmpty(product.getProductCode())) {
			product.setProductCode(product.getProductCode().toUpperCase());
		}
		product.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null) {
			product.setUpdateUser(logInfo.getStaffCode());
		}
		temp = temp.clone();
		repo.update(product);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, product, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Product getProductByCode(String code) throws DataAccessException {
		if (!StringUtility.isNullOrEmpty(code)) {
			code = code.toUpperCase().trim();
		} else {
			code = "";
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder("select * from PRODUCT where product_code = ? and status <> ?");
		params.add(code);
		params.add(ActiveType.DELETED.getValue());
		Product rs = repo.getEntityBySQL(Product.class, sql.toString(), params);
		return rs;
	}

	@Override
	public Product getProductByCode(String code, Integer status) throws DataAccessException {
		if (!StringUtility.isNullOrEmpty(code)) {
			code = code.toUpperCase().trim();
		}
		StringBuilder sql = new StringBuilder("select * from PRODUCT where product_code = ? ");
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		if (status != null) {
			sql.append("  and status = ? ");
			params.add(status);
		}
		Product rs = repo.getEntityBySQL(Product.class, sql.toString(), params);
		return rs;
	}
	
	@Override
	public Product getProductByFilter(ProductFilter filter) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql.append("select * from PRODUCT where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			sql.append(" and product_code=?");
			params.add(filter.getProductCode().toUpperCase());
		}
		if (filter.getStatus()!=null) {
			sql.append(" and status = ?");
			params.add(filter.getStatus().getValue());
		}else{
			sql.append(" and status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		Product rs = repo.getEntityBySQL(Product.class, sql.toString(), params);
		return rs;
	}

	@Override
	public Product getProductByCodeExceptZ(String code, Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from PRODUCT pr where product_code=? and status=? ");
		sql.append(" and exists (select 1 from product_info pi where pr.cat_id = pi.product_info_id ");
		sql.append(" and pi.product_info_code not in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.RUNNING.getValue());
		if (staffId != null) {
			sql.append(" and (not exists (select 1 from staff_sale_cat where staff_id = ?) or cat_id in (select cat_id from staff_sale_cat where staff_id = ?) )");
			params.add(staffId);
			params.add(staffId);
		}
		Product rs = repo.getEntityBySQL(Product.class, sql.toString(), params);
		return rs;
	}

	@Override
	public List<Product> getListProductExceptZ(KPaging<Product> paging, String productCode, String productName, Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from PRODUCT pr where status=? ");
		sql.append(" and exists (select 1 from product_info pi where pr.cat_id = pi.product_info_id ");
		sql.append(" and pi.product_info_code not in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append("  and product_code like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase().trim()));
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append("  and lower(name_text) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(productName.toLowerCase().trim())));
		}
		params.add(ActiveType.RUNNING.getValue());
		if (staffId != null) {
			sql.append(" and (not exists (select 1 from staff_sale_cat where staff_id = ?) or cat_id in (select cat_id from staff_sale_cat where staff_id = ?) )");
			params.add(staffId);
			params.add(staffId);
		}
		List<Product> rs = null;
		if (paging != null)
			rs = repo.getListBySQLPaginated(Product.class, sql.toString(), params, paging);
		else
			rs = repo.getListBySQL(Product.class, sql.toString(), params);
		return rs;
	}

	@Override
	public Boolean checkProductInZ(String productCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) count from PRODUCT p where status=1 ");
		sql.append(" 	and product_code = ?");
		params.add(productCode.toUpperCase());

		sql.append(" AND p.cat_id NOT  IN ");
		sql.append("     (SELECT product_info_id ");
		sql.append("     FROM product_info ");
		sql.append("     WHERE status           = 1 ");
		sql.append("     AND type               = 1 ");
		sql.append("     AND product_info_code IN ");
		sql.append("       (SELECT ap_param_code ");
		sql.append("       FROM ap_param ");
		sql.append("       WHERE status = 1 ");
		sql.append("       AND type     = 'EQUIPMENT_CAT' ");
		sql.append("       ) ");
		sql.append("     ) ");

		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;

	}

	@Override
	public Boolean checkDuplicateOrderIndex(Long productId, int orderIndex) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) count from PRODUCT p where p.order_index = ? ");
		params.add(orderIndex);
		if (productId != null && productId > 0L) {
			sql.append(" and product_id != ? ");
			params.add(productId);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public void increaseOrderIndexProduct(int orderIndex) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("Update product set order_index = order_index + 1 where order_index >= ? and order_index is not null ");
		params.add(orderIndex);
		repo.executeSQLQuery(sql.toString(), params);
	}

	@Override
	public List<Product> getListProduct(ProductFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from PRODUCT p where 1 = 1");
		if (filter.getLstProductCodeSelected() != null) {
			sql.append(" AND p.product_code not in (");
			int i = 0;
			for (String code : filter.getLstProductCodeSelected()) {
				i++;
				sql.append("?");
				params.add(code.toUpperCase());
				if (i != filter.getLstProductCodeSelected().size()) {
					sql.append(",");
				}
			}
			sql.append(")");
			sql.append(" AND p.cat_id not in ");
			sql.append(" (select product_info_id from product_info where status = 1 and product_info_code in (SELECT ap_param_code");
			sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and status = 1");
			sql.append(" ) )");
		}
		if (filter.getBrandId() != null) {
			sql.append(" and p.brand_id = ? ");
			params.add(filter.getBrandId());
		}
		if (Boolean.TRUE.equals(filter.getCheckBrand())) {
			sql.append(" and p.brand_id is null ");
		}
		if (Boolean.TRUE.equals(filter.getIsExceptZCat())) {
			sql.append(" AND p.cat_id not in ");
			sql.append(" (select product_info_id from product_info where status = 1 and product_info_code in (SELECT ap_param_code");
			sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and status = 1");
			sql.append(" ) )");
		}

		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			sql.append(" and product_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProductCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCategoryCode())) { // truong hop nay khong roi vao ko xai product_level_id
			sql.append(" and exists (select 1 from product_info i join product_level pl ");
			sql.append("	on pl.cat_id=i.product_info_id ");
			sql.append("		where pl.product_level_id=p.product_level_id ");
			sql.append("		and product_info_code like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCategoryCode().toUpperCase()));
		} else {
			List<String> catCodes = filter.getLstCategoryCodes();
			if (catCodes != null && catCodes.size() > 0) {
				sql.append(" and p.cat_id in ");
				sql.append(" (select product_info_id from product_info where status = 1");
				sql.append("		and product_info_code in (?");
				params.add(catCodes.get(0));
				for (int i = 1; i < catCodes.size(); i++) {
					sql.append(",?");
					params.add(catCodes.get(i));
				}
				sql.append(") )");
			}
		}

		if (!StringUtility.isNullOrEmpty(filter.getProductTypeCode())) {
			sql.append(" and product_type like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getProductTypeCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			sql.append(" and lower(name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getProductName()).toLowerCase()));
		}
		if (filter.getStatus() != null) {
			sql.append(" and status=?");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and status != ?");
			params.add(ActiveType.DELETED.getValue());
		}

		if (!StringUtility.isNullOrEmpty(filter.getSubCatCode())) {
			sql.append(" and exists (select 1 from product_info i join product_level pl ");
			sql.append("	on pl.cat_id=i.product_info_id ");
			sql.append("		where pl.product_level_id=p.product_level_id ");
			sql.append("		and product_info_code like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLike(filter.getSubCatCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getPromotionCode())) {
			sql.append(" and exists (select 1 from promotion_program_detail ppd where p.product_id=ppd.product_id and exists(select 1 from promotion_program pp where pp.promotion_program_id=ppd.promotion_program_id and pp.promotion_PROGRAM_code=?))");
			params.add(filter.getPromotionCode().toUpperCase());
		}

		if (filter.getCheckLot() != null) {
			sql.append(" and check_lot = ?");
			params.add(filter.getCheckLot());
		}

		if (Boolean.TRUE.equals(filter.getIsPriceValid())) {
			sql.append(" and exists (select 1 from price where status = 1 and product_id = p.product_id and from_date < trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null ) )");
		}

		if ("USER1".equals(filter.getUser())) {
			sql.append(" and user1 is not null");
		} else if ("USER2".equals(filter.getUser())) {
			sql.append(" and user2 is not null");
		} else if ("USER3".equals(filter.getUser())) {
			sql.append(" and user3 is not null");
		} else if ("USER4".equals(filter.getUser())) {
			sql.append(" and user4 is not null");
		} else if ("USER5".equals(filter.getUser())) {
			sql.append(" and user5 is not null");
		} else if ("USER6".equals(filter.getUser())) {
			sql.append(" and user6 is not null");
		} else if ("USER7".equals(filter.getUser())) {
			sql.append(" and user7 is not null");
		} else if ("USER8".equals(filter.getUser())) {
			sql.append(" and user8 is not null");
		} else if ("USER9".equals(filter.getUser())) {
			sql.append(" and user9 is not null");
		} else if ("USER10".equals(filter.getUser())) {
			sql.append(" and user10 is not null");
		} else if (!StringUtility.isNullOrEmpty(filter.getUser())) {
			sql.append(" and 1=0");
		}

		sql.append(" order by product_code");
		if (filter.getkPaging() == null)
			return repo.getListBySQL(Product.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Product.class, sql.toString(), params, filter.getkPaging());
	}

	@Override
	public List<Product> getAllListProduct(ProductFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product p where 1 = 1 ");
		if (filter.getStatusInt() != null) {
			sql.append(" and p.status   = ? ");
			params.add(filter.getStatusInt());
		} else {
			sql.append(" and p.status  <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getStatus() != null) {
			sql.append(" and p.status = ? ");
			params.add(filter.getStatus().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			sql.append(" 	and p.product_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getProductCode().trim().toUpperCase()));
		}
		if (filter.getIsExceptZCat() != null && filter.getIsExceptZCat()) {
			sql.append(" and NOT EXISTS (select 1 from product_info where product_info_id = p.cat_id and product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
		}
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			sql.append(" 	and unicode2english(p.product_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getProductName().trim().toLowerCase().trim())));
		}
		if (filter.getCatId() != null) {
			sql.append(" and p.cat_id   = ? ");
			params.add(filter.getCatId());
		}
		if (filter.getLstCategoryCodes() != null && !filter.getLstCategoryCodes().isEmpty()) {
			sql.append(" and p.cat_id in ");
			sql.append(" (select product_info_id from product_info where status = 1");
			sql.append("		and product_info_code in ('-1' ");
			for (String value : filter.getLstCategoryCodes()) {
				sql.append(",?");
				params.add(value);
			}
			sql.append(") ) ");
		}

		sql.append(" order by p.product_code asc ");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(Product.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(Product.class, sql.toString(), params, filter.getkPaging());
		}
	}

	@Override
	public Boolean isUsingByOthers(long productId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from product ");
		sql.append(" where ");

		sql.append("   exists (select 1 from cycle_count_map_product where product_id=?) ");
		params.add(productId);
		sql.append("   or exists (select 1 from cycle_count_result where product_id=?) ");
		params.add(productId);
		sql.append("   or exists (select 1 from display_program_detail where product_id=?) ");
		params.add(productId);
		sql.append("   or exists (select 1 from focus_channel_map_product where product_id=?) ");
		params.add(productId);
		sql.append("   or exists (select 1 from po_auto_detail where product_id=?) ");
		params.add(productId);
		sql.append("   or exists (select 1 from po_vnm_detail where product_id=?) ");
		params.add(productId);
		sql.append("   or exists (select 1 from po_vnm_lot where product_id=?) ");
		params.add(productId);
		sql.append("   or exists (select 1 from price where product_id=? and status != ?) ");
		params.add(productId);
		params.add(ActiveType.DELETED.getValue());
		sql.append("   or exists (select 1 from product_lot where product_id=? and status != ?) ");
		params.add(productId);
		params.add(ActiveType.DELETED.getValue());
		sql.append("   or exists (select 1 from product_map_level where product_id=? and status != ?) ");
		params.add(productId);
		params.add(ActiveType.DELETED.getValue());
		sql.append("   or exists (select 1 from promotion_program_detail where product_id=? or free_product_id=?) ");
		params.add(productId);
		params.add(productId);
		sql.append("   or exists (select 1 from focus_channel_map_product where product_id=?) ");
		params.add(productId);
		sql.append("   or exists (select 1 from sale_plan where product_id=?) ");
		params.add(productId);
		sql.append("   or exists (select 1 from sale_plan_version where product_id=?) ");
		params.add(productId);
		sql.append("   or exists (select 1 from shop_product where product_id=? and status != ?) ");
		params.add(productId);
		params.add(ActiveType.DELETED.getValue());

		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public List<PoAutoVO> getListPoAuto(Long shopId, Boolean checkAll) throws DataAccessException {
		if (shopId == null) {
			throw new DataAccessException("shopId is null");
		}
		if (checkAll == null) {
			throw new DataAccessException("checkAll is null");
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();

		sql.append(" SELECT pif.product_info_code                                                AS productInfoCode,");
		sql.append("   pd.product_code                                                           AS productCode,");
		sql.append("   pd.product_name                                                           AS productName,");
		sql.append("   NVL(rpt_stock_total_day_tmp.close_stock_total, 0)                         AS openStockTotal,");
		sql.append("   NVL(po_vnm_detail_tmp_1.pvd_quantity, 0)                                  AS impPoVnmQty,");
		sql.append("   NVL(sale_order_detail_tmp_1.sod_quantity, 0)                              AS impSaleOrderQty,");
		sql.append("   NVL(stock_trans_detail_tmp_1.std_quantity, 0)                             AS impStockTransQty,");
		sql.append("   NVL(po_vnm_detail_tmp_2.pvd_quantity, 0)                                  AS expPoVnmQty,");
		sql.append("   NVL(sale_order_detail_tmp_2.sod_quantity, 0)                              AS expSaleOrderQty,");
		sql.append("   NVL(std_tmp_exp.std_quantity, 0)                                          AS expStockTransQty,");
		sql.append("   NVL(sale_order_detail_tmp_3.sod_quantity, 0)                              AS expVansaleQty,");
		sql.append("   NVL(stock_total_tmp.st_quantity, 0)                                       AS stockQty,");
		sql.append("   NVL(sale_plan_tmp.sl_quantity, 0)                                         AS monthPlan,");
		sql.append("   NVL(sp_tmp_product.safety_stock_min, NVL(sp_tmp_cat.safety_stock_min, 0)) AS safetyStockMin,");
		sql.append("   NVL(sp_tmp_product.lead, NVL(sp_tmp_cat.lead, 0))                         AS lead,");
		sql.append("   NVL(sp_tmp_product.next, NVL(sp_tmp_cat.next, 0))                         AS NEXT,");
		sql.append("   NVL(sp_tmp_product.percentage, NVL(sp_tmp_cat.percentage, 0))             AS percentage,");
		//		sql.append("   NVL(pvd_tmp_3.pvd_quantity, 0)                                            AS qtyPoDvkh,");
		//		sql.append("   NVL(pvd_tmp_3.pvd_quantity_received, 0)                                   AS stockPoConfirm,");
		sql.append("   NVL(pvd_tmp_3.pvd_quantity, 0) - NVL(pvd_tmp_3.pvd_quantity_received, 0)  AS stockPoDvkh,");
		sql.append("   NVL(pvd_tmp_5.pvd_quantity, 0)                                            AS qtyGoPoConfirm,");
		sql.append("   NVL(pd.convfact, 0)                                                       AS convfact,");
		sql.append("   NVL(pr.price, 0)                                                          AS priceValue,");
		sql.append("   NVL(pr.price_id, 0)                                                       AS priceId");

		sql.append(" from product pd ");
		sql.append(" left join ");

		sql.append(" ( ");
		sql.append(" select pvd.product_id product_id, sum(pvd.quantity) pvd_quantity ");
		sql.append(" from po_vnm pv, po_vnm_detail pvd ");
		sql.append(" where pv.po_vnm_id = pvd.po_vnm_id and pv.type = ? and pv.object_type = 1 ");
		params.add(PoType.PO_CONFIRM.getValue());
		sql.append(" and pv.status = ? and pv.shop_id = ? ");
		params.add(PoVNMStatus.IMPORTED.getValue());
		params.add(shopId);
		sql.append(" and pv.import_date >= trunc(sysdate, 'MONTH') and pv.import_date < (trunc(sysdate) + 1) ");
		//		sql.append(" and pvd.po_vnm_date >= trunc(sysdate, 'MONTH') and pvd.po_vnm_date < (trunc(sysdate) + 1) ");
		sql.append(" group by pvd.product_id ");
		sql.append(" ) po_vnm_detail_tmp_1 on po_vnm_detail_tmp_1.product_id = pd.product_id ");

		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select sod.product_id, sum(sod.quantity) sod_quantity ");
		sql.append(" from sale_order so, sale_order_detail sod ");
		sql.append(" where so.sale_order_id = sod.sale_order_id ");
		sql.append(" and so.from_sale_order_id is not null and so.shop_id = ? ");
		params.add(shopId);
		sql.append(" and so.order_date >= trunc(sysdate, 'MONTH') and so.order_date < trunc(sysdate) + 1 ");
		sql.append(" and sod.order_date >= trunc(sysdate, 'MONTH') and sod.order_date < trunc(sysdate) + 1 ");
		sql.append(" group by sod.product_id ");
		sql.append(" ) sale_order_detail_tmp_1 on sale_order_detail_tmp_1.product_id = pd.product_id ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select std.product_id product_id, sum(std.quantity) std_quantity ");
		sql.append(" from stock_trans st join stock_trans_detail std on (st.stock_trans_id = std.stock_trans_id)");
		sql.append(" where 1 = 1 ");
		sql.append(" and st.to_owner_type = ? ");
		params.add(StockObjectType.SHOP.getValue());
		sql.append(" and (st.from_owner_type = ? or st.from_owner_type is null) ");
		params.add(StockObjectType.STAFF.getValue());
		sql.append(" and st.to_owner_id = ? ");
		params.add(shopId);
		sql.append(" and st.stock_trans_date >= trunc(sysdate, 'MONTH') and st.stock_trans_date < (trunc(sysdate) + 1) ");
		sql.append(" and std.stock_trans_date >= trunc(sysdate, 'MONTH') and std.stock_trans_date < (trunc(sysdate) + 1) ");
		sql.append(" group by std. product_id ");
		sql.append(" ) stock_trans_detail_tmp_1 on stock_trans_detail_tmp_1.product_id = pd.product_id ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select pvd.product_id product_id, sum(pvd.quantity) pvd_quantity ");
		sql.append(" from po_vnm pv, po_vnm_detail pvd ");
		sql.append(" where pv.po_vnm_id = pvd.po_vnm_id and pv.type = ? and pv.object_type = 1");
		params.add(PoType.RETURNED_SALES_ORDER.getValue());
		sql.append(" and pv.status = 2 and pv.shop_id = ? ");
		params.add(shopId);
		sql.append(" and pv.po_vnm_date >= trunc(sysdate, 'MONTH') and pv.po_vnm_date < (trunc(sysdate) + 1) ");
		sql.append(" and pvd.po_vnm_date >= trunc(sysdate, 'MONTH') and pvd.po_vnm_date < (trunc(sysdate) + 1) ");
		sql.append(" group by pvd.product_id ");
		sql.append(" ) po_vnm_detail_tmp_2 on po_vnm_detail_tmp_2.product_id = pd.product_id ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select sod.product_id product_id, sum(sod.quantity) sod_quantity ");
		sql.append(" from sale_order so, sale_order_detail sod ");
		sql.append(" where so.sale_order_id = sod.sale_order_id ");
		sql.append(" and so.from_sale_order_id is null and so.shop_id = ? ");
		params.add(shopId);
		sql.append(" and so.order_type = ? and so.approved = ? ");
		params.add(OrderType.IN.getValue());
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append(" and so.order_date >= trunc(sysdate, 'MONTH') and so.order_date < trunc(sysdate) + 1 ");
		sql.append(" and sod.order_date >= trunc(sysdate, 'MONTH') and sod.order_date < trunc(sysdate) + 1 ");
		sql.append(" group by sod.product_id ");
		sql.append(" ) sale_order_detail_tmp_2 on sale_order_detail_tmp_2.product_id = pd.product_id ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select std.product_id product_id, sum(std.quantity) std_quantity");
		sql.append(" from stock_trans st join stock_trans_detail std on (st.stock_trans_id = std.stock_trans_id)");
		sql.append(" where 1 =1 ");
		sql.append(" and st.from_owner_type = ? ");
		params.add(StockObjectType.SHOP.getValue());
		sql.append(" and (st.to_owner_type is null or st.to_owner_type = ?) ");
		params.add(StockObjectType.STAFF.getValue());
		sql.append(" and st.shop_id = ? ");
		params.add(shopId);

		//update: HUNGNM
		sql.append(" and st.from_owner_id = ? ");
		params.add(shopId);
		//end update

		sql.append(" and st.stock_trans_date >= trunc(sysdate, 'MONTH') and st.stock_trans_date < (trunc(sysdate) + 1) ");
		sql.append(" and std.stock_trans_date >= trunc(sysdate, 'MONTH') and std.stock_trans_date < (trunc(sysdate) + 1) ");
		sql.append(" group by std.product_id ");
		sql.append(" ) std_tmp_exp on std_tmp_exp.product_id = pd.product_id ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select sod.product_id product_id, sum(sod.quantity) sod_quantity ");
		sql.append(" from sale_order so, sale_order_detail sod ");
		sql.append(" where so.sale_order_id = sod.sale_order_id ");
		sql.append(" and so.from_sale_order_id is null and so.shop_id = ? ");
		params.add(shopId);
		sql.append(" and so.order_type = ? and so.approved = ? ");
		params.add(OrderType.SO.getValue());
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append(" and so.order_date >= trunc(sysdate, 'MONTH') and so.order_date < trunc(sysdate) + 1 ");
		sql.append(" and sod.order_date >= trunc(sysdate, 'MONTH') and sod.order_date < trunc(sysdate) +1 ");
		sql.append(" group by sod.product_id ");
		sql.append(" ) sale_order_detail_tmp_3 on sale_order_detail_tmp_3.product_id = pd.product_id ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select st.product_id, sum(st.quantity) st_quantity ");
		sql.append(" from stock_total st ");
		sql.append(" where  st.object_type = ? and st.object_id = ? ");
		params.add(StockObjectType.SHOP.getValue());
		params.add(shopId);
		sql.append(" group by st.product_id ");
		sql.append(" ) stock_total_tmp on stock_total_tmp.product_id = pd.product_id ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select sl.product_id product_id, sum(sl.quantity) sl_quantity ");
		sql.append(" from sale_plan sl ");
		sql.append(" where sl.object_type = ? and sl.object_id = ? ");
		params.add(SalePlanOwnerType.SHOP.getValue());
		params.add(shopId);
		sql.append(" and sl.month = to_number(to_char(sysdate, 'MM')) ");
		sql.append(" and sl.year = to_number(to_char(sysdate, 'YYYY')) ");
		sql.append(" group by sl.product_id ");
		sql.append(" ) sale_plan_tmp on sale_plan_tmp.product_id = pd.product_id ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select pvd.product_id product_id, sum(pvd.quantity) pvd_quantity, sum(pvd.quantity_received) pvd_quantity_received ");
		sql.append(" from po_vnm pv, po_vnm_detail pvd ");
		sql.append(" where pv.po_vnm_id = pvd.po_vnm_id and pv.type = ? and pv.object_type = 1 ");
		params.add(PoType.PO_CUSTOMER_SERVICE.getValue());
		sql.append(" and pv.status in (?, ?) and pv.shop_id = ? ");
		params.add(PoVNMStatus.NOT_IMPORT.getValue());
		params.add(PoVNMStatus.IMPORTING.getValue());
		params.add(shopId);
		sql.append(" group by pvd.product_id ");
		sql.append(" ) pvd_tmp_3 on pvd_tmp_3.product_id = pd.product_id ");
		//		//
		//		sql.append(" left join ");
		//		sql.append(" ( ");
		//		sql.append(" select pvd.product_id product_id, sum(pvd.quantity) pvd_quantity ");
		//		sql.append(" from po_vnm pv, po_vnm_detail pvd ");
		//		sql.append(" where pv.po_vnm_id = pvd.po_vnm_id and pv.type = 2 ");
		//		sql.append(" and pv.status = 2 and pv.shop_id = ? ");
		//		params.add(shopId);
		//		// po_auto_number = po_auto_number dang nhap
		//		sql.append(" AND EXISTS ");
		//		sql.append(" (SELECT 1 ");
		//		sql.append(" FROM ");
		//		sql.append(" (SELECT pv.po_auto_number po_auto_number ");
		//		sql.append(" FROM po_vnm pv, po_vnm_detail pvd ");
		//		sql.append(" WHERE pv.po_vnm_id = pvd.po_vnm_id ");
		//		sql.append(" AND pv.type = 1 ");
		//		sql.append(" AND pv.status = 1 ");
		//		sql.append(" AND pv.shop_id = ? ");
		//		params.add(shopId);
		//		sql.append(" )tmp ");
		//		sql.append(" WHERE tmp.po_auto_number = pv.po_auto_number ");
		//		sql.append(" ) ");
		//		sql.append(" group by pvd.product_id ");
		//		sql.append(" ) pvd_tmp_4 on pvd_tmp_4.product_id = pd.product_id ");
		//
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select pvd.product_id product_id, sum(pvd.quantity) pvd_quantity ");
		sql.append(" from po_vnm pv, po_vnm_detail pvd ");
		sql.append(" where pv.po_vnm_id = pvd.po_vnm_id and pv.type = ? and pv.object_type = 1 ");
		params.add(PoType.PO_CONFIRM.getValue());
		sql.append(" and pv.status = ? and pv.shop_id = ? ");
		params.add(PoVNMStatus.NOT_IMPORT.getValue());
		params.add(shopId);
		sql.append(" group by pvd.product_id ");
		sql.append(" ) pvd_tmp_5 on pvd_tmp_5.product_id = pd.product_id ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select sp.product_id, sp.minsf safety_stock_min, sp.lead lead, ");
		sql.append(" (case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 1, 7) + 1)) > 0) then 1 else ");
		sql.append("  case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 2, 7) + 1)) > 0) then 2 else ");
		sql.append("  case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 3, 7) + 1)) > 0) then 3 else ");
		sql.append("  case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 4, 7) + 1)) > 0) then 4 else ");
		sql.append("  case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 5, 7) + 1)) > 0) then 5 else ");
		sql.append("  case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 6, 7) + 1)) > 0) then 6 else ");
		sql.append("  case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 7, 7) + 1)) > 0) then 7 else ");
		sql.append("  0 end end end end end end end ) next, ");
		sql.append(" sp.percentage percentage ");
		sql.append(" from shop_product sp ");
		sql.append(" where sp.shop_id = ?  and sp.status = ? and sp.type = ? ");
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		params.add(ShopProductType.PRODUCT.getValue());
		//		sql.append(" and sp.year = to_number(to_char(sysdate, 'YYYY')) ");
		sql.append(" ) sp_tmp_product on sp_tmp_product.product_id = pd.product_id ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select sp.cat_id, sp.minsf safety_stock_min, sp.lead lead, ");
		sql.append(" (case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 1, 7) + 1)) > 0) then 1 else ");
		sql.append(" case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 2, 7) + 1)) > 0) then 2 else ");
		sql.append(" case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 3, 7) + 1)) > 0) then 3 else ");
		sql.append(" case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 4, 7) + 1)) > 0) then 4 else ");
		sql.append(" case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 5, 7) + 1)) > 0) then 5 else ");
		sql.append(" case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 6, 7) + 1)) > 0) then 6 else ");
		sql.append(" case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 7, 7) + 1)) > 0) then 7 else ");
		sql.append(" 0 end end end end end end end ) next, ");
		sql.append(" sp.percentage percentage ");
		sql.append(" from shop_product sp ");
		sql.append(" where sp.shop_id = ?  and sp.status = ? and sp.type = ? ");
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		params.add(ShopProductType.CAT.getValue());
		//		sql.append(" and sp.year = to_number(to_char(sysdate, 'YYYY')) ");
		sql.append(" ) sp_tmp_cat on sp_tmp_cat.cat_id = pd.cat_id ");
		sql.append(" left join ");
		sql.append(" ( ");
		sql.append(" select rpt_std.product_id product_id, sum(rpt_std.close_stock_total) close_stock_total ");
		sql.append(" from rpt_stock_total_day rpt_std ");
		sql.append(" where rpt_std.object_type = ? and rpt_std.object_id = ? ");
		params.add(StockObjectType.SHOP.getValue());
		params.add(shopId);
		sql.append(" and rpt_std.rpt_in_day >= trunc(trunc(sysdate, 'MONTH') - 1) and rpt_std.rpt_in_day < trunc(sysdate, 'MONTH')");
		sql.append(" group by rpt_std.product_id ");
		sql.append(" ) rpt_stock_total_day_tmp ");
		sql.append(" on rpt_stock_total_day_tmp.product_id = pd.product_id ");
		//		sql.append(" , product_info pif, product_level pl, price pr ");
		sql.append(" , product_info pif, price pr ");
		sql.append(" where pd.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		//		sql.append(" and pd.product_level_id = pl.product_level_id ");
		//		sql.append(" and pl.cat_id = pif.product_info_id and pif.type = ? and pif.status = ? ");
		sql.append(" and pd.cat_id = pif.product_info_id and pif.type = ? and pif.status = ? ");
		params.add(ProductType.CAT.getValue());
		params.add(ActiveType.RUNNING.getValue());
		if (checkAll) {
			sql.append(" and upper(pif.product_info_code) in ('A', 'B', 'C', 'D', 'E') ");
		} else {
			sql.append(" and upper(pif.product_info_code) in ('A', 'B', 'C', 'E') ");
		}
		sql.append(" and pr.product_id = pd.product_id and pr.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and pr.from_date < trunc(sysdate) + 1 and (pr.to_date is null or pr.to_date >= trunc(sysdate)) ");
		sql.append(" AND EXISTS");
		sql.append("   (SELECT 1");
		sql.append("   FROM shop_product sp");
		sql.append("   WHERE sp.shop_id  = ?");
		params.add(shopId);
		sql.append("   AND sp.status     = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   AND ((sp.type     = ?");
		params.add(ShopProductType.CAT.getValue());
		sql.append("   AND sp.cat_id     = pd.cat_id)");
		sql.append("   OR (sp.type       = ?");
		params.add(ShopProductType.PRODUCT.getValue());
		sql.append("   AND sp.product_id = pd.product_id))");
		sql.append("   )");
		sql.append(" order by pif.product_info_code, pd.product_code ");

		final String[] fieldNames = new String[] { //
		"productInfoCode",// 1
				"productCode", // 2
				"productName", // 3
				"openStockTotal", // 4
				"impPoVnmQty", // 5
				"impSaleOrderQty", // 6
				"impStockTransQty", // 7
				"expPoVnmQty", // 8
				"expSaleOrderQty", // 9
				"expStockTransQty", // 10
				"expVansaleQty", // 11
				"stockQty", // 12
				"monthPlan", // 13
				"safetyStockMin", // 15
				"lead", // 16
				"next", // 17
				"percentage", // 18
				//				"qtyPoDvkh", // 19
				//				"stockPoConfirm", // 20
				"stockPoDvkh",//19
				"qtyGoPoConfirm", // 21
				"convfact", // 22
				"priceValue", // 23
				"priceId", // 23
		};

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.INTEGER,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.INTEGER,// 6
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.INTEGER,// 8
				StandardBasicTypes.INTEGER,// 9
				StandardBasicTypes.INTEGER,// 12
				StandardBasicTypes.INTEGER,// 11
				StandardBasicTypes.INTEGER,// 12
				StandardBasicTypes.INTEGER,// 13
				StandardBasicTypes.INTEGER,// 15
				StandardBasicTypes.INTEGER,// 16
				StandardBasicTypes.INTEGER,// 17
				StandardBasicTypes.FLOAT,// 18
				//				StandardBasicTypes.INTEGER,// 19
				//				StandardBasicTypes.INTEGER,// 20
				StandardBasicTypes.INTEGER,// 19
				StandardBasicTypes.INTEGER,// 21
				StandardBasicTypes.INTEGER,// 22
				StandardBasicTypes.BIG_DECIMAL,// 23
				StandardBasicTypes.LONG,// 23
		};
		return repo.getListByQueryAndScalar(PoAutoVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<SalePlanVO> getListSalePlanVOForProduct(KPaging<SalePlanVO> kPaging, Long shopId, String staffCode, String catCode, String productCode, Integer month, Integer year) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (month == null) {
			throw new IllegalArgumentException("month is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}
		if (staffCode != null) {
			staffCode = staffCode.toUpperCase();
		}
		if (catCode != null) {
			catCode = catCode.toUpperCase();
		}
		if (productCode != null) {
			productCode = productCode.toUpperCase();
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlSelect = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		List<Object> countParams = new ArrayList<Object>();

		countSql.append(" SELECT count(1) as count ");

		sqlSelect.append(" SELECT sale_plan_staff_tmp.salePlanId      AS salePlanId,");
		sqlSelect.append("   sale_plan_staff_tmp.staffId              AS staffId,");
		sqlSelect.append("   sale_plan_staff_tmp.staffCode            AS staffCode,");
		sqlSelect.append("   pd.product_id                            AS productId,");
		sqlSelect.append("   pd.product_code                          AS productCode,");
		sqlSelect.append("   pd.product_name                          AS productName,");
		sqlSelect.append("   ?                                        AS month,");
		params.add(month);
		sqlSelect.append("   ?                                        AS year,");
		params.add(year);
		sqlSelect.append("   sale_plan_staff_tmp.quantity             AS staffQuantity,");
		sqlSelect.append("   NVL(sale_plan_staff_tmp.price, pr.price) AS price,");
		sqlSelect.append("   sale_plan_shop_tmp.quantity              AS shopQuantity,");
		sqlSelect.append("   sale_plan_shop_tmp.quantityAssign        AS shopQuantityAssign,");
		sqlSelect.append("   sale_plan_shop_tmp.leftQuantity          AS shopLeftQuantity");

		sql.append(" FROM product pd");
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT sl.sale_plan_id AS salePlanId,");
		sql.append("     s.staff_id            AS staffId,");
		sql.append("     s.staff_code          AS staffCode,");
		sql.append("     sl.product_id         AS productId,");
		sql.append("     sl.quantity           AS quantity,");
		sql.append("     sl.price              AS price");
		sql.append("   FROM sale_plan sl,");
		sql.append("     staff s");
		sql.append("   WHERE 1           = 1");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append("   AND s.staff_code     = ?");
			params.add(staffCode.trim().toUpperCase());
			countParams.add(staffCode.trim().toUpperCase());
		} else {
			sql.append("   AND 1 = 2");
		}
		sql.append("   AND s.status      = 1");
		sql.append("   AND sl.object_type = ?");
		params.add(SalePlanOwnerType.SALEMAN.getValue());
		sql.append("   AND sl.object_id   = s.staff_id");
		sql.append("   AND sl.month      = ?");
		params.add(month);
		countParams.add(month);
		sql.append("   AND sl.year       = ?");
		params.add(year);
		countParams.add(year);
		sql.append("   ) sale_plan_staff_tmp");
		sql.append(" ON (sale_plan_staff_tmp.productId = pd.product_id)");
		sql.append(" LEFT JOIN");
		sql.append("   (SELECT pd.product_id                              AS productId,");
		sql.append("     NVL(sl.quantity, 0)                              AS quantity,");
		sql.append("     NVL(sl.quantity_assign, 0)                       AS quantityAssign,");
		sql.append("     NVL(sl.quantity, 0) - NVL(sl.quantity_assign, 0) AS leftQuantity");
		sql.append("   FROM sale_plan sl,");
		sql.append("     product pd");
		sql.append("   WHERE 1           = 1");
		sql.append("   AND sl.object_type = ?");
		params.add(SalePlanOwnerType.SHOP.getValue());
		sql.append("   AND sl.object_id   = ?");
		params.add(shopId);
		countParams.add(shopId);
		sql.append("   AND sl.product_id = pd.product_id");
		sql.append("   AND sl.month      = ?");
		params.add(month);
		countParams.add(month);
		sql.append("   AND sl.year       = ?");
		params.add(year);
		countParams.add(year);
		sql.append("   ) sale_plan_shop_tmp");
		sql.append(" ON (sale_plan_shop_tmp.productId = pd.product_id)");
		sql.append(" JOIN price pr");
		sql.append(" ON (pd.product_id        = pr.product_id");
		sql.append(" AND pr.from_date < TRUNC(sysdate) + 1");
		sql.append(" AND (pr.to_date         IS NULL");
		sql.append(" OR pr.to_date    >= TRUNC(sysdate)))");
		sql.append(" JOIN product_level pl");
		sql.append(" ON (pd.product_level_id = pl.product_level_id)");
		sql.append(" JOIN product_info pif");
		if (!StringUtility.isNullOrEmpty(catCode)) {
			sql.append(" ON (pl.cat_id = pif.product_info_id AND pif.product_info_code = ?)");
			params.add(catCode);
			countParams.add(catCode);
		} else {
			sql.append(" ON (pl.cat_id = pif.product_info_id)");
		}
		sql.append(" WHERE pd.status = 1");
		sql.append(" ORDER BY pd.product_code");
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" AND pd.product_code = ?");
			params.add(productCode);
			countParams.add(productCode);
		}
		// count sql
		sqlSelect.append(sql);
		countSql.append(sql);

		String[] fieldNames = new String[] {//
		"salePlanId",// 1
				"staffId",// 3
				"staffCode",// 4
				"productId",// 5
				"productCode",// 6
				"productName",// 7
				"month",// 8
				"year",// 9
				"staffQuantity",// 10
				"price",// 11
				"shopQuantity",// 12
				"shopQuantityAssign",// 13
				"shopLeftQuantity"// 14
		};
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.LONG,// 1
				StandardBasicTypes.LONG,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.LONG,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.INTEGER,// 8
				StandardBasicTypes.INTEGER,// 9
				StandardBasicTypes.INTEGER,// 10
				StandardBasicTypes.BIG_DECIMAL,// 11
				StandardBasicTypes.INTEGER,// 12
				StandardBasicTypes.INTEGER,// 13
				StandardBasicTypes.INTEGER,// 14
		};
		if (kPaging != null)
			return repo.getListByQueryAndScalarPaginated(SalePlanVO.class, fieldNames, fieldTypes, sqlSelect.toString(), countSql.toString(), params, countParams, kPaging);
		else
			return repo.getListByQueryAndScalar(SalePlanVO.class, fieldNames, fieldTypes, sqlSelect.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.ProductDAO#getListProductAddSkuSalePlan(com.viettel
	 * .core.entities.enumtype.KPaging, java.lang.String, java.lang.String)
	 */
	@Override
	public List<ProductVO> getListProductAddSkuSalePlan(KPaging<ProductVO> kPaging, String productCode, String productName, Integer month, Integer year) throws DataAccessException {

		if (month == null) {
			throw new IllegalArgumentException("month is null");
		}
		if (year == null) {
			throw new IllegalArgumentException("year is null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		List<Object> countParams = new ArrayList<Object>();
		countSql.append(" select count(*) as count");
		countSql.append(" from product d, product_level pl, product_info pif where d.product_level_id = pl.product_level_id and pl.cat_id = pif.product_info_id ");
		countSql.append(" and pif.type = 1");
		sql.append(" select temp1.productId as productId, temp1.productCode as productCode, temp1.productName as productName, temp1.catelogyCode as catelogyCode, temp.quantity as quantity from");
		sql.append(" (select d.product_id as productId, d.product_code as productCode, d.product_name as productName, pif.product_info_code as catelogyCode");
		sql.append(" from product d, product_level pl, product_info pif where d.product_level_id = pl.product_level_id and pl.cat_id = pif.product_info_id ");
		sql.append(" and pif.type = 1");
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and d.product_code = ?");
			countSql.append(" and d.product_code = ?");
			params.add(productCode.toUpperCase());
			countParams.add(productCode.toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" and lower(d.name_text) like ? ESCAPE '/' ");
			countSql.append(" and lower(d.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(productName).trim().toLowerCase()));
			countParams.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(productName).trim().toLowerCase()));
		}
		sql.append(" ) temp1");
		sql.append(" left join (select product_id as productSalePlanId, quantity as quantity from sale_plan where month = ? and year = ?) temp");
		params.add(month);
		params.add(year);
		sql.append(" on temp.productSalePlanId = temp1.productId");
		String[] fieldNames = new String[] {//
		"productId",// 1
				"productCode",// 2
				"productName",// 3
				"catelogyCode",// 4
				"quantity",// 5
		};
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.INTEGER,// 5
		};
		if (kPaging != null)
			return repo.getListByQueryAndScalarPaginated(ProductVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, countParams, kPaging);
		else
			return repo.getListByQueryAndScalar(ProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/*
	 * @Override public List<RptImExStDataDetailVO> getDataForImExStReport(Long
	 * shopId, Date fromDate, Date toDate, Long catId) throws
	 * DataAccessException {
	 * 
	 * if (fromDate != null && toDate != null && fromDate.after(toDate)) return
	 * new ArrayList<RptImExStDataDetailVO>();
	 * 
	 * Date currentDate = commonDAO.getSysDate();
	 * 
	 * if (shopId == null) { throw new
	 * IllegalArgumentException("shopId is null"); } if (fromDate == null) {
	 * throw new IllegalArgumentException("fromDate is null"); } if (toDate ==
	 * null) { throw new IllegalArgumentException("toDate is null"); } if
	 * (fromDate.compareTo(toDate) > 0) { throw new
	 * IllegalArgumentException("From date can't after to date"); } if (null ==
	 * currentDate) { throw new
	 * IllegalArgumentException("Can't get current date."); }
	 * 
	 * // StringBuilder sql = new StringBuilder(); StringBuilder sqlSelect = new
	 * StringBuilder(); List<Object> params = new ArrayList<Object>();
	 * 
	 * //toDate < currentDate if (DateUtility.getOnlyDate(toDate).compareTo(
	 * DateUtility.getOnlyDate(currentDate)) < 0) {
	 * sqlSelect.append(" select pro_i.product_info_name as productCat,");
	 * sqlSelect.append(
	 * " 	pro.product_code as productCode, pro.product_name as productName, ");
	 * sqlSelect.append(
	 * " 	pro.convfact as productConvfact, nvl(pri.price, 0) as productPrice,");
	 * sqlSelect.append(
	 * "     nvl(rpt_std_out_b.close_stock_total, 0) as quantityCloseSTBegin,");
	 * sqlSelect
	 * .append(" 	nvl(rpt_std_out_e.close_stock_total, 0) as quantityCloseSTEnd,"
	 * ); sqlSelect.append(
	 * "     nvl(rpt_std_out.sale_total_pre, 0) + nvl(rpt_std_out.sale_total_van, 0) as quantitySaleToCus,"
	 * ); sqlSelect.append(
	 * "     nvl(rpt_std_out.sale_total_pre, 0) + nvl(rpt_std_out.sale_total_van, 0) + nvl(rpt_std_out.sale_promotion_d_pre, 0) + nvl(rpt_std_out.sale_promotion_d_van, 0) as quantitySaleOffCus,"
	 * ); sqlSelect.append(
	 * "     nvl(rpt_std_out.import_total_vnm, 0) as quantityBuyFromVNM, ");
	 * sqlSelect
	 * .append("     nvl(rpt_std_out.export_total_vnm, 0) as quantityReturnToVNM, "
	 * ); sqlSelect.append(
	 * "     nvl(rpt_std_out.import_total, 0) - nvl(rpt_std_out.import_total_vnm, 0) quantityImport, "
	 * ); sqlSelect.append(
	 * "     nvl(rpt_std_out.export_total, 0) - nvl(rpt_std_out.export_total_vnm, 0) quantityExport"
	 * ); sqlSelect.append(" from product pro");
	 * sqlSelect.append(" left join price pri on pro.product_id = pri.product_id "
	 * ); sqlSelect.append(
	 * "     and pri.from_date < trunc(?) + 1 and pri.to_date >= trunc(?)");
	 * params.add(toDate); params.add(toDate); sqlSelect.append(
	 * " left join (select rpt_std.product_id, sum(rpt_std.close_stock_total) as close_stock_total"
	 * ); sqlSelect.append("     from rpt_stock_total_day rpt_std");
	 * sqlSelect.append("     where rpt_std.object_id = ? ");
	 * params.add(shopId);
	 * sqlSelect.append("         and rpt_std.object_type = 1");
	 * sqlSelect.append(
	 * "         and rpt_std.rpt_in_day >= trunc(?) - 1 and rpt_std.rpt_in_day < trunc(?) "
	 * ); params.add(fromDate); params.add(fromDate); sqlSelect.append(
	 * "     group by rpt_std.product_id) rpt_std_out_b on pro.product_id = rpt_std_out_b.product_id"
	 * ); sqlSelect.append(
	 * " left join (select rpt_std.product_id, sum(rpt_std.close_stock_total) as close_stock_total"
	 * ); sqlSelect.append("     from rpt_stock_total_day rpt_std");
	 * sqlSelect.append("     where rpt_std.object_id = ? ");
	 * params.add(shopId);
	 * sqlSelect.append("         and rpt_std.object_type = 1");
	 * sqlSelect.append(
	 * "         and rpt_std.rpt_in_day >= trunc(?) and rpt_std.rpt_in_day < trunc(?) + 1"
	 * ); params.add(toDate); params.add(toDate); sqlSelect.append(
	 * "     group by rpt_std.product_id) rpt_std_out_e on pro.product_id = rpt_std_out_e.product_id"
	 * ); sqlSelect.append(" left join (select rpt_std.product_id, ");
	 * sqlSelect.append(
	 * "                 sum(rpt_std.sale_total_pre) sale_total_pre, sum(rpt_std.sale_total_van) sale_total_van,"
	 * ); sqlSelect.append(
	 * "                 sum(rpt_std.sale_promotion_d_pre) sale_promotion_d_pre, sum(rpt_std.sale_promotion_d_van) sale_promotion_d_van,"
	 * ); sqlSelect.append(
	 * "                 sum(rpt_std.import_total_vnm) import_total_vnm,");
	 * sqlSelect
	 * .append("                 sum(rpt_std.export_total_vnm) export_total_vnm,"
	 * );
	 * sqlSelect.append("                 sum(rpt_std.import_total) import_total,"
	 * );
	 * sqlSelect.append("                 sum(rpt_std.export_total) export_total"
	 * ); sqlSelect.append("     from rpt_stock_total_day rpt_std");
	 * sqlSelect.append("     where rpt_std.object_id = ? ");
	 * params.add(shopId);
	 * sqlSelect.append("         and rpt_std.object_type = 1");
	 * sqlSelect.append(
	 * "         and rpt_std.rpt_in_day between trunc(?) and trunc(?) + 1 - interval '0.001' second"
	 * ); params.add(fromDate); params.add(toDate); sqlSelect.append(
	 * "     group by rpt_std.product_id) rpt_std_out on pro.product_id = rpt_std_out.product_id"
	 * ); sqlSelect.append(
	 * " inner join product_level pro_l on pro.product_level_id = pro_l.product_level_id"
	 * ); sqlSelect.append(
	 * " inner join product_info pro_i on pro_l.cat_id = pro_i.product_info_id "
	 * );
	 * sqlSelect.append(" 	and pro_i.type = 1 and pro_i.product_info_id = ?");
	 * params.add(catId); } // toDate >= currentDate else {
	 * sqlSelect.append(" select pro_i.product_info_name as productCat,");
	 * sqlSelect.append(
	 * " 	pro.product_code as productCode, pro.product_name as productName, ");
	 * sqlSelect.append(
	 * " 	pro.convfact as productConvfact, nvl(pri.price, 0) as productPrice,");
	 * sqlSelect.append(
	 * "     nvl(rpt_std_out_b.close_stock_total, 0) as quantityCloseSTBegin, "
	 * ); sqlSelect.append(" 	nvl(rpt_std_out_b.close_stock_total, 0) ");
	 * sqlSelect.append(
	 * " 		- ((nvl(rpt_std_out.sale_total_pre, 0) + nvl(rpt_std_out.sale_total_van, 0)) + nvl(xuat_ban_sl.quantity, 0))"
	 * ); sqlSelect.append(
	 * " 		- ((nvl(rpt_std_out.sale_total_pre, 0) + nvl(rpt_std_out.sale_total_van, 0) + nvl(rpt_std_out.sale_promotion_d_pre, 0) + nvl(rpt_std_out.sale_promotion_d_van, 0)) + nvl(xuat_ban_km.quantity, 0))"
	 * ); sqlSelect.append(
	 * " 		+ ((nvl(rpt_std_out.import_total, 0) - nvl(rpt_std_out.import_total_vnm, 0)) + nvl(nhap_kho_dc.quantity, 0) + nvl(nhap_tra_pre.quantity, 0) + nvl(nhap_kho_van.quantity, 0))"
	 * ); sqlSelect.append(
	 * " 		+ (nvl(rpt_std_out.import_total_vnm, 0) + nvl(tong_nhap_vnm.quantity, 0))"
	 * ); sqlSelect.append(
	 * " 		- ((nvl(rpt_std_out.export_total, 0) - nvl(rpt_std_out.export_total_vnm, 0)) + nvl(xuat_kho_pre.quantity, 0) + nvl(xuat_kho_van.quantity, 0) + nvl(xuat_kho_dc.quantity, 0))"
	 * ); sqlSelect.append(
	 * " 		- (nvl(rpt_std_out.export_total_vnm, 0) + nvl(xuat_tra_vnm.quantity, 0)) as quantityCloseSTEnd, "
	 * ); sqlSelect.append(
	 * " 	(nvl(rpt_std_out.import_total, 0) - nvl(rpt_std_out.import_total_vnm, 0)) + nvl(nhap_kho_dc.quantity, 0) + nvl(nhap_tra_pre.quantity, 0) + nvl(nhap_kho_van.quantity, 0) as quantityImport, "
	 * ); sqlSelect.append(
	 * " 	nvl(rpt_std_out.import_total_vnm, 0) + nvl(tong_nhap_vnm.quantity, 0) as quantityBuyFromVNM, "
	 * ); sqlSelect.append(
	 * " 	(nvl(rpt_std_out.export_total, 0) - nvl(rpt_std_out.export_total_vnm, 0)) + nvl(xuat_kho_pre.quantity, 0) + nvl(xuat_kho_van.quantity, 0) + nvl(xuat_kho_dc.quantity, 0) as quantityExport, "
	 * ); sqlSelect.append(
	 * " 	nvl(rpt_std_out.export_total_vnm, 0) + nvl(xuat_tra_vnm.quantity, 0) as quantityReturnToVNM, "
	 * ); sqlSelect.append(
	 * " 	(nvl(rpt_std_out.sale_total_pre, 0) + nvl(rpt_std_out.sale_total_van, 0)) + nvl(xuat_ban_sl.quantity, 0) as quantitySaleToCus, "
	 * ); sqlSelect.append(
	 * " 	(nvl(rpt_std_out.sale_total_pre, 0) + nvl(rpt_std_out.sale_total_van, 0) + nvl(rpt_std_out.sale_promotion_d_pre, 0) + nvl(rpt_std_out.sale_promotion_d_van, 0)) + nvl(xuat_ban_km.quantity, 0) as quantitySaleOffCus "
	 * ); sqlSelect.append(" from product pro");
	 * sqlSelect.append(" left join price pri on pro.product_id = pri.product_id "
	 * ); sqlSelect.append(
	 * "     and pri.from_date < trunc(?) + 1 and pri.to_date >= trunc(?)");
	 * params.add(toDate); params.add(toDate);
	 * sqlSelect.append(" left join (select rpt_std.product_id, ");
	 * sqlSelect.append(
	 * "                 sum(rpt_std.sale_total_pre) sale_total_pre, sum(rpt_std.sale_total_van) sale_total_van,"
	 * ); sqlSelect.append(
	 * "                 sum(rpt_std.sale_promotion_d_pre) sale_promotion_d_pre, sum(rpt_std.sale_promotion_d_van) sale_promotion_d_van,"
	 * ); sqlSelect.append(
	 * "                 sum(rpt_std.import_total_vnm) import_total_vnm,");
	 * sqlSelect
	 * .append("                 sum(rpt_std.export_total_vnm) export_total_vnm,"
	 * );
	 * sqlSelect.append("                 sum(rpt_std.import_total) import_total,"
	 * );
	 * sqlSelect.append("                 sum(rpt_std.export_total) export_total"
	 * ); sqlSelect.append("     from rpt_stock_total_day rpt_std");
	 * sqlSelect.append("     where rpt_std.object_id = ? ");
	 * params.add(shopId);
	 * sqlSelect.append("         and rpt_std.object_type = 1");
	 * sqlSelect.append(
	 * "         and rpt_std.rpt_in_day between trunc(?) and trunc(?) + 1 - interval '0.001' second "
	 * ); params.add(fromDate); params.add(toDate); sqlSelect.append(
	 * "     group by rpt_std.product_id) rpt_std_out on pro.product_id = rpt_std_out.product_id"
	 * ); sqlSelect.append(
	 * " left join (select rpt_std.product_id, sum(rpt_std.close_stock_total) as close_stock_total"
	 * ); sqlSelect.append("     from rpt_stock_total_day rpt_std");
	 * sqlSelect.append("     where rpt_std.object_id = ? ");
	 * params.add(shopId);
	 * sqlSelect.append("         and rpt_std.object_type = 1");
	 * sqlSelect.append(
	 * "         and rpt_std.rpt_in_day >= trunc(?) - 1 and rpt_std.rpt_in_day < trunc(?)"
	 * ); params.add(fromDate); params.add(fromDate); sqlSelect.append(
	 * "     group by rpt_std.product_id) rpt_std_out_b on pro.product_id = rpt_std_out_b.product_id"
	 * ); sqlSelect.append(
	 * " left join (select po_d.product_id, sum(po_d.quantity) as quantity");
	 * sqlSelect.append(" 	from po_vnm po, po_vnm_detail po_d");
	 * sqlSelect.append(" 	where po.po_vnm_id = po_d.po_vnm_id");
	 * sqlSelect.append(" 		and po.type = 2 ");
	 * sqlSelect.append(" 		and po.status = 2");
	 * sqlSelect.append(" 		and po.shop_id = ? "); params.add(shopId);
	 * sqlSelect.append(
	 * " 		and po.import_date >= trunc(sysdate) and po.import_date < trunc(sysdate) + 1"
	 * ); sqlSelect.append(
	 * " 	group by po_d.product_id) tong_nhap_vnm on pro.product_id = tong_nhap_vnm.product_id"
	 * ); sqlSelect.append(
	 * " left join (select st_d.product_id, sum(st_d.quantity) as quantity");
	 * sqlSelect.append(" 	from stock_trans st, stock_trans_detail st_d");
	 * sqlSelect.append(" 	where st.stock_trans_id = st_d.stock_trans_id");
	 * sqlSelect.append(" 		and st.shop_id = ? "); params.add(shopId);
	 * sqlSelect.append(" 		and st.from_owner_type is null");
	 * sqlSelect.append(" 		and st.to_owner_type = 1"); sqlSelect.append(
	 * " 		and st.stock_trans_date >= trunc(sysdate) and st.stock_trans_date < (trunc(sysdate) + 1)"
	 * ); sqlSelect.append(
	 * " 	group by st_d.product_id) nhap_kho_dc on pro.product_id = nhap_kho_dc.product_id"
	 * ); sqlSelect.append(
	 * " left join (select so_d.product_id, sum(so_d.quantity) as quantity");
	 * sqlSelect.append(" 	from sale_order so, sale_order_detail so_d");
	 * sqlSelect.append(" 	where so.sale_order_id = so_d.sale_order_id");
	 * sqlSelect.append(" 		and so.from_sale_order_id is not null");
	 * sqlSelect.append(" 		and so.shop_id = ? "); params.add(shopId);
	 * sqlSelect.append(
	 * " 		and so.order_date >= trunc(sysdate) and so.order_date < trunc(sysdate) + 1"
	 * ); sqlSelect.append(
	 * " 	group by so_d.product_id) nhap_tra_pre on pro.product_id = nhap_tra_pre.product_id"
	 * ); sqlSelect.append(
	 * " left join (select st_d.product_id, sum(st_d.quantity) as quantity");
	 * sqlSelect.append(" 	from stock_trans st, stock_trans_detail st_d");
	 * sqlSelect.append(" 	where st.stock_trans_id = st_d.stock_trans_id");
	 * sqlSelect.append(" 		and st.shop_id = ? "); params.add(shopId);
	 * sqlSelect.append(" 		and st.from_owner_type = 2");
	 * sqlSelect.append(" 		and st.to_owner_type = 1"); sqlSelect.append(
	 * " 		and st.stock_trans_date >= trunc(sysdate) and st.stock_trans_date < (trunc(sysdate) + 1)"
	 * ); sqlSelect.append(
	 * " 	group by st_d.product_id) nhap_kho_van on pro.product_id = nhap_kho_van.product_id	"
	 * ); sqlSelect.append(
	 * " left join(select so_d.product_id, sum(so_d.quantity) as quantity");
	 * sqlSelect.append(" 	from sale_order so, sale_order_detail so_d");
	 * sqlSelect.append(" 	where so.sale_order_id = so_d.sale_order_id");
	 * sqlSelect.append(" 		and upper(so.order_type) = 'IN'");
	 * sqlSelect.append(" 		and so.approved = 1 ");
	 * sqlSelect.append(" 		and so.from_sale_order_id is null");
	 * sqlSelect.append(" 		and so.shop_id = ? "); params.add(shopId);
	 * sqlSelect.append(
	 * " 	group by so_d.product_id) xuat_kho_pre on pro.product_id = xuat_kho_pre.product_id"
	 * ); sqlSelect.append(
	 * " left join(select st_d.product_id, sum(st_d.quantity) as quantity");
	 * sqlSelect.append(" 	from stock_trans st, stock_trans_detail st_d");
	 * sqlSelect.append(" 	where st.stock_trans_id = st_d.stock_trans_id");
	 * sqlSelect.append(" 		and st.shop_id = ? "); params.add(shopId);
	 * sqlSelect.append(" 		and st.from_owner_type = 1");
	 * sqlSelect.append(" 		and st.to_owner_type = 2"); sqlSelect.append(
	 * " 		and st.stock_trans_date >= trunc(sysdate) and st.stock_trans_date < trunc(sysdate) + 1"
	 * ); sqlSelect.append(
	 * " 	group by st_d.product_id) xuat_kho_van on pro.product_id = xuat_kho_van.product_id"
	 * ); sqlSelect.append(
	 * " left join (select po_d.product_id, sum(po_d.quantity) as quantity");
	 * sqlSelect.append(" 	from po_vnm po, po_vnm_detail po_d");
	 * sqlSelect.append(" 	where po.po_vnm_id = po_d.po_vnm_id");
	 * sqlSelect.append(" 		and po.type = 3");
	 * sqlSelect.append(" 		and po.status = 2");
	 * sqlSelect.append(" 		and po.shop_id = ? "); params.add(shopId);
	 * sqlSelect.append(
	 * " 		and po.import_date >= trunc(sysdate) and po.import_date < trunc(sysdate) + 1"
	 * ); sqlSelect.append(
	 * " 	group by po_d.product_id) xuat_tra_vnm on pro.product_id = xuat_tra_vnm.product_id"
	 * ); sqlSelect.append(
	 * " left join (select st_d.product_id, sum(st_d.quantity) as quantity");
	 * sqlSelect.append(" 	from stock_trans st, stock_trans_detail st_d");
	 * sqlSelect.append(" 	where st.stock_trans_id = st_d.stock_trans_id");
	 * sqlSelect.append(" 		and st.shop_id = ? "); params.add(shopId);
	 * sqlSelect.append(" 		and st.from_owner_type = 1");
	 * sqlSelect.append(" 		and st.to_owner_type is null"); sqlSelect.append(
	 * " 		and st.stock_trans_date >= trunc(sysdate) and st.stock_trans_date < (trunc(sysdate) + 1)"
	 * ); sqlSelect.append(
	 * " 	group by st_d.product_id) xuat_kho_dc on pro.product_id = xuat_kho_dc.product_id"
	 * ); sqlSelect.append(
	 * " left join(select so_d.product_id, sum(so_d.quantity) as quantity");
	 * sqlSelect.append(" 	from sale_order so, sale_order_detail so_d");
	 * sqlSelect.append(" 	where so.sale_order_id = so_d.sale_order_id");
	 * sqlSelect.append(" 		and so.shop_id = ? "); params.add(shopId);
	 * sqlSelect.append(" 		and upper(so.order_type) = 'IN'");
	 * sqlSelect.append(" 		and so.approved = 1");
	 * sqlSelect.append(" 		and so.from_sale_order_id is null");
	 * sqlSelect.append(" 		and so_d.is_free_item = 0"); sqlSelect.append(
	 * " 	group by so_d.product_id) xuat_ban_sl on pro.product_id = xuat_ban_sl.product_id"
	 * ); sqlSelect.append(
	 * " left join(select so_d.product_id, sum(so_d.quantity) as quantity");
	 * sqlSelect.append(" 	from sale_order so, sale_order_detail so_d");
	 * sqlSelect.append(" 	where so.sale_order_id = so_d.sale_order_id");
	 * sqlSelect.append(" 		and so.shop_id = ? "); params.add(shopId);
	 * sqlSelect.append(" 		and upper(so.order_type) = 'IN'");
	 * sqlSelect.append(" 		and so.approved = 1");
	 * sqlSelect.append(" 		and so.from_sale_order_id is null");
	 * sqlSelect.append(" 		and so_d.is_free_item = 1"); sqlSelect.append(
	 * " 	group by so_d.product_id) xuat_ban_km on pro.product_id = xuat_ban_km.product_id"
	 * ); sqlSelect.append(
	 * " inner join product_level pro_l on pro.product_level_id = pro_l.product_level_id"
	 * ); sqlSelect.append(
	 * " inner join product_info pro_i on pro_l.cat_id = pro_i.product_info_id "
	 * );
	 * sqlSelect.append(" 	and pro_i.type = 1 and pro_i.product_info_id = ?");
	 * params.add(catId); }
	 * 
	 * String[] fieldNames = new String[] { "productCat", //1 "productCode",//2
	 * "productName", //3 "productPrice", //4 "quantityCloseSTBegin",//5
	 * "quantityCloseSTEnd", //6 "quantitySaleToCus",//7 "quantitySaleOffCus",
	 * //8 "quantityBuyFromVNM",//9 "quantityReturnToVNM", //10
	 * "quantityImport", //11 "quantityExport" //12 }; Type[] fieldTypes = new
	 * Type[] { StandardBasicTypes.STRING,//1 StandardBasicTypes.STRING,//2
	 * StandardBasicTypes.STRING,//3 StandardBasicTypes.BIG_DECIMAL,//4
	 * StandardBasicTypes.LONG,//5 StandardBasicTypes.LONG,//6
	 * StandardBasicTypes.LONG,//7 StandardBasicTypes.LONG,//8
	 * StandardBasicTypes.LONG,//9 StandardBasicTypes.LONG,//10
	 * StandardBasicTypes.LONG,//11 StandardBasicTypes.LONG//12 };
	 * 
	 * return repo.getListByQueryAndScalar(RptImExStDataDetailVO.class,
	 * fieldNames, fieldTypes, sqlSelect.toString(), params); }
	 */

	/*
	 * @Override public List<OrderProductVO>
	 * getListOrderProductVO(KPaging<OrderProductVO> paging, Long shopId, Long
	 * staffId, Long custId, String pdCode, String pdName, String ppCode, Date
	 * lockDay, Long custTypeId, Long shopTypeId) throws DataAccessException {
	 * //hardcode test // shopId = (long) 2807; // custId = (long) 168518; //
	 * staffId = (long) 805; if (shopId == null) { throw new
	 * IllegalArgumentException("shopId is null"); } if (staffId == null) {
	 * throw new IllegalArgumentException("staffId is null"); } if (custId ==
	 * null) { throw new IllegalArgumentException("custId is null"); }
	 * 
	 * StringBuilder sql = new StringBuilder(); StringBuilder selectSql = new
	 * StringBuilder(); StringBuilder countSql = new StringBuilder();
	 * List<Object> params = new ArrayList<Object>();
	 * 
	 * //selectSql.append("   pr.price as price, "); // lacnv1 - check lai gia
	 * // selectSql.append(" (select price from ("); //
	 * selectSql.append(" select price, product_id from (select price, product_id"
	 * ); // selectSql.append(" from price pr"); //
	 * selectSql.append(" join (select shop_id"); //
	 * selectSql.append(" from shop s"); //
	 * selectSql.append(" where s.status = ?"); //
	 * params.add(ActiveType.RUNNING.getValue()); // selectSql.append(
	 * " start with shop_id = ? connect by prior parent_shop_id = shop_id"); //
	 * params.add(shopId); //
	 * selectSql.append(" ) sh on (sh.shop_id = pr.shop_id)"); //
	 * selectSql.append(" where status = ?"); //
	 * params.add(ActiveType.RUNNING.getValue()); //
	 * selectSql.append(" and from_date < trunc(?) + 1"); //
	 * params.add(lockDay); //
	 * selectSql.append(" and (to_date >= trunc(?) or to_date is null)"); //
	 * params.add(lockDay); // selectSql.append(
	 * " and  (customer_type_id in (select channel_type_id from customer where customer_id = ?) or customer_type_id is null)"
	 * ); // params.add(custId); // selectSql.append(" union all"); //
	 * selectSql.append(" select price, product_id"); //// selectSql.append(
	 * " ((case when shop_channel is not null then 6 else 7 end) - (case when customer_type_id is null then 0 else 8 end)) as orderNo"
	 * ); // selectSql.append(" from price pr"); //
	 * selectSql.append(" where status = ?"); //
	 * params.add(ActiveType.RUNNING.getValue()); //
	 * selectSql.append(" and from_date < trunc(?) + 1"); //
	 * params.add(lockDay); //
	 * selectSql.append(" and (to_date >= trunc(?) or to_date is null)"); //
	 * params.add(lockDay); // selectSql.append(
	 * " and (customer_type_id in (select channel_type_id from customer where customer_id = ?) or customer_type_id is null)"
	 * ); // params.add(custId); // selectSql.append(" and shop_id is null");
	 * //// selectSql.append(" and "); //// params.add(shopId); //
	 * selectSql.append(" ) )"); //
	 * selectSql.append(" where product_id = p.product_id and rownum = 1"); //
	 * selectSql.append(" ) as price,"); // // //gia thung //
	 * selectSql.append(" (select package_price from ("); // selectSql.append(
	 * " select package_price, product_id from (select package_price, product_id"
	 * ); // selectSql.append(" from price pr"); //
	 * selectSql.append(" join (select shop_id"); //
	 * selectSql.append(" from shop s"); //
	 * selectSql.append(" where s.status = ?"); //
	 * params.add(ActiveType.RUNNING.getValue()); // selectSql.append(
	 * " start with shop_id = ? connect by prior parent_shop_id = shop_id"); //
	 * params.add(shopId); //
	 * selectSql.append(" ) sh on (sh.shop_id = pr.shop_id)"); //
	 * selectSql.append(" where status = ?"); //
	 * params.add(ActiveType.RUNNING.getValue()); //
	 * selectSql.append(" and from_date < trunc(?) + 1"); //
	 * params.add(lockDay); //
	 * selectSql.append(" and (to_date >= trunc(?) or to_date is null)"); //
	 * params.add(lockDay); // selectSql.append(
	 * " and  (customer_type_id in (select channel_type_id from customer where customer_id = ?) or customer_type_id is null)"
	 * ); // params.add(custId); // selectSql.append(" union all"); //
	 * selectSql.append(" select package_price, product_id"); ////
	 * selectSql.append(
	 * " ((case when shop_channel is not null then 6 else 7 end) - (case when customer_type_id is null then 0 else 8 end)) as orderNo"
	 * ); // selectSql.append(" from price pr"); //
	 * selectSql.append(" where status = ?"); //
	 * params.add(ActiveType.RUNNING.getValue()); //
	 * selectSql.append(" and from_date < trunc(?) + 1"); //
	 * params.add(lockDay); //
	 * selectSql.append(" and (to_date >= trunc(?) or to_date is null)"); //
	 * params.add(lockDay); // selectSql.append(
	 * " and (customer_type_id in (select channel_type_id from customer where customer_id = ?) or customer_type_id is null)"
	 * ); // params.add(custId); // selectSql.append(" and shop_id is null");
	 * //// selectSql.append(" and "); //// params.add(shopId); //
	 * selectSql.append(" ) )"); //
	 * selectSql.append(" where product_id = p.product_id and rownum = 1"); //
	 * selectSql.append(" ) as packagePrice,");
	 * 
	 * 
	 * //lay gia theo don vi selectSql.append("with ");
	 * selectSql.append("lstShop as ( ");
	 * selectSql.append("    select shop_id ,level as lv ");
	 * selectSql.append("    from shop s1 ");
	 * selectSql.append("    where status = 1 ");
	 * selectSql.append("    start with s1.shop_id           = ?");
	 * params.add(shopId);
	 * selectSql.append("    connect by prior parent_shop_id = shop_id ");
	 * selectSql.append(") "); selectSql.append(",prc as ( "); selectSql.append(
	 * "    SELECT	product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id "
	 * ); selectSql.append("    FROM	 price	prc ");
	 * selectSql.append("    WHERE	1 = 1 ");
	 * selectSql.append("    AND prc.status = 1 ");
	 * selectSql.append("    and prc.from_date <= trunc(?) ");
	 * params.add(lockDay);
	 * selectSql.append("    and (prc.to_date >= trunc(?) or prc.to_date is null) "
	 * ); params.add(lockDay); selectSql.append(
	 * "    and (shop_id is null or shop_id in (select shop_id from lstShop)) "
	 * ); selectSql.append(") "); selectSql.append(",prcX as ( ");
	 * selectSql.append("    select "); selectSql.append(
	 * "        product_id,customer_id,customer_type_id,prc.shop_id,shop_type_id, from_date, to_date ,price_id "
	 * ); selectSql.append(
	 * "        ,lstShop.lv, rank() over(PARTITION BY product_id ORDER BY lstShop.lv) rank_last "
	 * ); selectSql.append(
	 * "    from prc join lstShop on prc.shop_id = lstShop.shop_id ");
	 * selectSql.append("    where prc.customer_type_id is null  ");
	 * selectSql.append("    union all "); selectSql.append("    select ");
	 * selectSql.append(
	 * "         product_id,customer_id,customer_type_id,prc.shop_id,shop_type_id, from_date, to_date ,price_id "
	 * ); selectSql.append(
	 * "        ,lstShop.lv, rank() over(PARTITION BY product_id ORDER BY lstShop.lv) rank_last "
	 * ); selectSql.append(
	 * "    from prc join lstShop on prc.shop_id = lstShop.shop_id ");
	 * selectSql.append("    where prc.customer_type_id = ?  ");
	 * params.add(custTypeId); selectSql.append(") ");
	 * selectSql.append(",prc_rec as( "); selectSql.append(
	 * "    select  product_id,shop_id,lv, rank_last, from_date, to_date ,price_id,customer_id,customer_type_id,shop_type_id "
	 * ); selectSql.append("    from prcX ");
	 * selectSql.append("    where rank_last = 1 "); selectSql.append(") ");
	 * selectSql.append(",result as ( "); selectSql.append(
	 * "    select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id "
	 * ); selectSql.append("    from prc_rec ");
	 * selectSql.append("    union all "); selectSql.append(
	 * "    select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id "
	 * ); selectSql.append("    from prc where customer_id = ?  ");
	 * params.add(custId); selectSql.append("    union all "); selectSql.append(
	 * "    select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id "
	 * ); selectSql.append(
	 * "    from prc where customer_type_id = ? and shop_id is null and (shop_type_id is null or shop_type_id = ?)  "
	 * ); params.add(custTypeId); params.add(shopTypeId);
	 * selectSql.append("    union all "); selectSql.append(
	 * "    select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id "
	 * ); selectSql.append(
	 * "    from prc where customer_type_id is null and shop_id is null and shop_type_id = ?  "
	 * ); params.add(shopTypeId); selectSql.append(") ");
	 * selectSql.append(",resultAll as ( "); selectSql.append("    SELECT ");
	 * selectSql.append("        result.product_id, "); selectSql.append(
	 * "        max(case when result.customer_id = ? then price_id end) pr_cus, "
	 * ); params.add(custId); selectSql.append(
	 * "        max(case when result.customer_id is null and result.customer_type_id = ? "
	 * ); params.add(custTypeId); selectSql.append(
	 * "          and result.shop_id is not null and result.shop_type_id is null  then price_id	end "
	 * ); selectSql.append("        ) pr_cus_type_and_shop_id, ");
	 * selectSql.append("        max( "); selectSql.append(
	 * "          case when result.customer_id is null and result.customer_type_id = ? "
	 * ); params.add(custTypeId); selectSql.append(
	 * "          and result.shop_id is null and result.shop_type_id = ? then price_id end "
	 * ); params.add(shopTypeId);
	 * selectSql.append("        ) pr_cus_type_and_shop_type, ");
	 * selectSql.append("        max( "); selectSql.append(
	 * "          case when result.customer_id is null and result.customer_type_id = ? "
	 * ); params.add(custTypeId); selectSql.append(
	 * "          and result.shop_id is null and result.shop_type_id is null then price_id end "
	 * ); selectSql.append("        ) pr_cus_type, ");
	 * selectSql.append("        max( "); selectSql.append(
	 * "          case when result.customer_id is null and result.customer_type_id is null "
	 * ); selectSql.append(
	 * "          and result.shop_id is not null and result.shop_type_id is null then price_id end "
	 * ); selectSql.append("        ) pr_shop_id, ");
	 * selectSql.append("        max( "); selectSql.append(
	 * "          case when result.customer_id is null  and result.customer_type_id is null "
	 * ); selectSql.append(
	 * "          and result.shop_id  is null and result.shop_type_id =  ? then price_id "
	 * ); params.add(shopTypeId); selectSql.append("          end ");
	 * selectSql.append("        ) pr_shop_type ");
	 * selectSql.append("    FROM	result ");
	 * selectSql.append("    group by product_id "); selectSql.append(") ");
	 * 
	 * //--lay danh sach product selectSql.append(" , data as ( ");
	 * selectSql.append("select * from (");
	 * selectSql.append(" SELECT distinct(p.product_id) as productId, ");
	 * selectSql.append("   p.product_code as productCode, ");
	 * selectSql.append("   p.product_name as productName, ");
	 * selectSql.append("   pp.promotion_program_code as promotionProgramCode, "
	 * ); selectSql.append("   p.convfact                as convfact, ");
	 * selectSql.append("   0   as isFocus, ");
	 * selectSql.append("   p.gross_weight as grossWeight, ");
	 * selectSql.append("   p.net_weight as netWeight, ");
	 * selectSql.append("   p.check_lot as checkLot ");
	 * 
	 * countSql.append(" SELECT count(1) as count");
	 * 
	 * sql.append(" FROM product p"); sql.append(" JOIN stock_total st");
	 * sql.append(" ON (st.product_id = p.product_id ");
	 * sql.append(" AND st.object_id   = ?"); params.add(shopId);
	 * sql.append(" AND st.object_type = ? and st.status = ?)");
	 * params.add(StockObjectType.SHOP.getValue());
	 * params.add(ActiveType.RUNNING.getValue()); sql.append(
	 * "  AND ( (p.cat_id IN (SELECT cat_id FROM staff_sale_cat WHERE staff_id = ?) "
	 * ); sql.append(
	 * "       AND (SELECT COUNT(1) FROM staff_sale_cat WHERE staff_id = ?) > 0) "
	 * ); sql.append(
	 * " 	 or (SELECT COUNT(1) FROM staff_sale_cat WHERE staff_id = ?) = 0 ");
	 * sql.append(" 	 ) "); params.add(staffId); params.add(staffId);
	 * params.add(staffId); // //Khong lay trong nganh hang Z // sql.append(
	 * " and exists (select 1 from product_info pi where p.cat_id = pi.product_info_id "
	 * ); // sql.append(
	 * " and pi.product_info_code not in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))"
	 * );
	 * 
	 * sql.append(" LEFT JOIN"); sql.append(
	 * " (SELECT DISTINCT gld.product_id AS product_id, pp.promotion_program_code AS promotion_program_code"
	 * ); sql.append(" FROM promotion_program pp "); sql.append(
	 * " join product_group pg on pp.promotion_program_id = pg.promotion_program_id and pg.group_type = 1 "
	 * ); sql.append(
	 * " join group_level gl on gl.product_group_id = pg.product_group_id ");
	 * sql.append(
	 * " join group_level_detail gld on gld.group_level_id = gl.group_level_id "
	 * ); sql.append(
	 * " WHERE pp.status   = 1 and pp.type not in ('ZM', 'ZD', 'ZH', 'ZT')");
	 * sql.append(" AND pp.from_date < TRUNC(?) + 1 "); params.add(lockDay);
	 * sql.append(" AND (pp.to_date    >= TRUNC(?) or pp.to_date is null)");
	 * params.add(lockDay); sql.append(" and pg.status = 1");
	 * sql.append(" and gl.status = 1 "); sql.append(" and gld.status = 1 ");
	 * sql.append(" AND EXISTS "); sql.append("   (SELECT 1 ");
	 * sql.append("   FROM promotion_shop_map psm "); sql.append(
	 * "   WHERE psm.promotion_program_id = pp.promotion_program_id and status = 1"
	 * ); sql.append(
	 * "   AND (psm.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id)) "
	 * ); params.add(shopId); //NhanLT 12/03/2014 - Bo sung check fromDate,
	 * toDate cua promotion_shop_map
	 * sql.append("   AND psm.from_date < TRUNC(?) + 1  "); params.add(lockDay);
	 * sql
	 * .append("   AND (psm.to_date    >= TRUNC(?) or psm.to_date is null) ");
	 * params.add(lockDay); //end sql.append(
	 * " AND ( (quantity_max IS NULL OR NVL(quantity_received, 0) < quantity_max) ) "
	 * ); //Kiem tra KM ap dung cho KH sql.append(" AND (  ");
	 * 
	 * sql.append(
	 * "         (not exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1) and ((psm.quantity_max is null or NVL(psm.quantity_received, 0) < psm.quantity_max)) )"
	 * ); sql.append("         or "); sql.append(
	 * "         exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and customer_id = ? and ((quantity_max is null or NVL(quantity_received, 0) < quantity_max))) "
	 * ); params.add(custId);
	 * 
	 * sql.append(" 	 ) "); //Kiem tra KM ap dung cho NVBH
	 * sql.append(" AND (  ");
	 * 
	 * sql.append(
	 * "         (not exists (select 1 from promotion_staff_map where promotion_shop_map_id = psm.promotion_shop_map_id and shop_id = ? and status = 1) and ((psm.quantity_max is null or NVL(psm.quantity_received, 0) < psm.quantity_max)) )"
	 * ); sql.append("         or "); sql.append(
	 * "         exists (select 1 from promotion_staff_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and staff_id = ? and shop_id = ? and ((quantity_max is null or NVL(quantity_received, 0) < quantity_max))) "
	 * ); params.add(shopId); params.add(staffId); params.add(shopId);
	 * 
	 * sql.append(" 	 ) "); //end Kiem tra KM ap dung cho NVBH
	 * sql.append("   ) "); sql.append(" AND  "); sql.append("   ( ");
	 * sql.append(
	 * "     NOT EXISTS (SELECT 1 FROM PROMOTION_CUST_ATTR pa WHERE pa.promotion_program_id = pp.promotion_program_id and status = 1) "
	 * ); sql.append("     OR  ");
	 * 
	 * //Kiem tra thuoc tinh KH sql.append(" ( "); sql.append(
	 * " EXISTS (SELECT * FROM PROMOTION_CUST_ATTR pa WHERE pa.promotion_program_id = pp.promotion_program_id AND status = 1)"
	 * ); sql.append(" AND ( "); sql.append(" NOT EXISTS "); sql.append(" ( ");
	 * sql.append(" (SELECT * "); sql.append(" FROM PROMOTION_CUST_ATTR pa ");
	 * sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
	 * sql.append(" AND status = 1) "); sql.append(" minus ");
	 * sql.append(" (SELECT * "); sql.append(" FROM PROMOTION_CUST_ATTR pa ");
	 * sql.append(" WHERE pa.promotion_program_id = pp.promotion_program_id ");
	 * sql.append(" AND status = 1 "); sql.append(" AND ( (object_type = 1 ");
	 * sql.append(" AND ( ( EXISTS "); sql.append(" (SELECT 1 ");
	 * sql.append(" FROM attribute "); sql.append(" WHERE value_type = 1 ");
	 * sql.append(" AND attribute_id = pa.object_id ");
	 * sql.append(" AND status = 1 "); sql.append(" ) ");
	 * sql.append(" AND EXISTS "); sql.append(" (SELECT * ");
	 * sql.append(" FROM attribute_value ");
	 * sql.append(" WHERE attribute_id = pa.object_id ");
	 * sql.append(" AND status = 1 ");
	 * sql.append(" AND table_name = 'CUSTOMER' ");
	 * sql.append(" AND TABLE_ID = ? "); params.add(custId);
	 * sql.append(" AND STATUS = 1 ");
	 * sql.append(" AND upper(VALUE) = upper(PA.FROM_VALUE) ");
	 * sql.append(" ) ) "); sql.append(" OR ( EXISTS ");
	 * sql.append(" (SELECT 1 "); sql.append(" FROM attribute ");
	 * sql.append(" WHERE value_type = 2 ");
	 * sql.append(" AND attribute_id = pa.object_id ");
	 * sql.append(" AND status = 1 "); sql.append(" ) ");
	 * sql.append(" AND EXISTS "); sql.append(" (SELECT * ");
	 * sql.append(" FROM attribute_value ");
	 * sql.append(" WHERE attribute_id = pa.object_id ");
	 * sql.append(" AND table_name = 'CUSTOMER' ");
	 * sql.append(" AND TABLE_ID = ? "); params.add(custId);
	 * sql.append(" AND STATUS = 1 ");
	 * sql.append(" AND to_number(VALUE) >= to_number(PA.FROM_VALUE) ");
	 * sql.append(" AND to_number(value) <= to_number(pa.to_value) ");
	 * sql.append(" ) ) "); sql.append(" OR ( EXISTS ");
	 * sql.append(" (SELECT 1 "); sql.append(" FROM attribute ");
	 * sql.append(" WHERE value_type = 3 ");
	 * sql.append(" AND attribute_id = pa.object_id ");
	 * sql.append(" AND status = 1 "); sql.append(" ) ");
	 * sql.append(" AND EXISTS "); sql.append(" (SELECT * ");
	 * sql.append(" FROM attribute_value ");
	 * sql.append(" WHERE attribute_id = pa.object_id ");
	 * sql.append(" AND table_name = 'CUSTOMER' ");
	 * sql.append(" AND TABLE_ID = ? "); params.add(custId);
	 * sql.append(" AND STATUS = 1 "); sql.append(
	 * " AND (to_date(VALUE, 'yyyy-MM-dd') >= to_date(PA.FROM_VALUE, 'yyyy-MM-dd') "
	 * ); sql.append(" OR pa.from_value IS NULL) "); sql.append(
	 * " AND (to_date(value, 'yyyy-MM-dd') <= to_date(pa.to_value, 'yyyy-MM-dd') "
	 * ); sql.append(" OR pa.to_value IS NULL) "); sql.append(" ) ) ");
	 * sql.append(" OR ( EXISTS "); sql.append(" (SELECT 1 ");
	 * sql.append(" FROM attribute ");
	 * sql.append(" WHERE value_type IN (4,5) ");
	 * sql.append(" AND attribute_id = pa.object_id ");
	 * sql.append(" AND status = 1 "); sql.append(" ) ");
	 * sql.append(" AND EXISTS "); sql.append(" (SELECT * ");
	 * sql.append(" FROM attribute_value av ");
	 * sql.append(" WHERE attribute_id = pa.object_id ");
	 * sql.append(" AND table_name = 'CUSTOMER' ");
	 * sql.append(" AND TABLE_ID = ? "); params.add(custId);
	 * sql.append(" AND STATUS = 1 ");
	 * 
	 * //Kiem tra thuoc tinh mutil select sql.append(
	 * " and exists (select 1 from attribute_value_detail avd join promotion_cust_attr_detail pcd on avd.attribute_detail_id = pcd.object_id "
	 * ); sql.append(
	 * "             where avd.attribute_value_id = av.attribute_value_id and pcd.promotion_cust_attr_id = pa.promotion_cust_attr_id and pcd.status = 1 and avd.status = 1)"
	 * );
	 * 
	 * //Kiem tra thuoc tinh mutil select
	 * 
	 * sql.append(" ) ) ) ) "); sql.append(" OR (object_type = 2 ");
	 * sql.append(" AND EXISTS "); sql.append(" (SELECT 1 ");
	 * sql.append(" FROM PROMOTION_CUST_ATTR_DETAIL ");
	 * sql.append(" WHERE status = 1 ");
	 * sql.append(" AND PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
	 * sql.append(" AND OBJECT_ID = "); sql.append(
	 * " (SELECT CHANNEL_TYPE_ID FROM CUSTOMER WHERE CUSTOMER_ID = ? AND status = 1 "
	 * ); params.add(custId); sql.append(" ) "); sql.append(" ) ) ");
	 * sql.append(" OR (object_type = 3 "); sql.append(" AND EXISTS ");
	 * sql.append(" (SELECT 1 ");
	 * sql.append(" FROM PROMOTION_CUST_ATTR_DETAIL ");
	 * sql.append(" WHERE status = 1 ");
	 * sql.append(" AND PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
	 * sql.append(" AND OBJECT_ID IN "); sql.append(
	 * " (SELECT SALE_LEVEL_CAT_ID FROM CUSTOMER_CAT_LEVEL WHERE CUSTOMER_ID = ? "
	 * ); params.add(custId); sql.append(" AND status = 1 "); sql.append(" ) ");
	 * sql.append(" ) ) ) "); sql.append(" ) "); sql.append(" ) ");
	 * sql.append(" ) "); sql.append(" ) "); //Kiem tra thuoc tinh KH
	 * sql.append("   ) ");
	 * sql.append("   ) pp ON (p.product_id = pp.product_id)");
	 * 
	 * // sql.append(" LEFT JOIN"); //
	 * sql.append("  (SELECT DISTINCT fcmd.product_id AS product_id"); //
	 * sql.append("   FROM focus_program fp"); //
	 * sql.append("   JOIN focus_shop_map fsm"); //
	 * sql.append("   ON (fp.focus_program_id = fsm.focus_program_id )"); //
	 * sql.append("   JOIN focus_channel_map fcm"); //
	 * sql.append("   ON (fp.focus_program_id = fcm.focus_program_id)"); //
	 * sql.append("   JOIN focus_channel_map_product fcmd"); //
	 * sql.append("   ON (fcm.focus_channel_map_id = fcmd.focus_channel_map_id )"
	 * ); // sql.append("   JOIN staff s"); //
	 * sql.append("   ON (fcm.sale_type_code = s.sale_type_code)"); //
	 * sql.append("   WHERE 1               = 1"); //
	 * sql.append("   AND fsm.shop_id       = ?"); // params.add(shopId); //
	 * sql.append("   AND fp.status         = ?"); //
	 * params.add(ActiveType.RUNNING.getValue()); //
	 * sql.append("   AND fp.from_date      < TRUNC(?) + 1"); //
	 * params.add(lockDay); // sql.append("   AND (fp.to_date      IS NULL"); //
	 * sql.append("   OR (fp.to_date       >= TRUNC(?)))"); //
	 * params.add(lockDay); // sql.append("   AND fsm.status        = ?"); //
	 * params.add(ActiveType.RUNNING.getValue()); //
	 * sql.append("   AND fcm.status        = ?"); //
	 * params.add(ActiveType.RUNNING.getValue()); //
	 * sql.append("   AND s.staff_id   = ?"); // params.add(staffId); //
	 * sql.append("   ) fcmd ON (fcmd.product_id   = p.product_id)");
	 * sql.append(" WHERE p.status                 = ?");
	 * params.add(ActiveType.RUNNING.getValue()); //Khong lay sp co lo
	 * sql.append(" and p.check_lot = 0 "); if
	 * (!StringUtility.isNullOrEmpty(pdCode)) {
	 * sql.append(" AND UPPER(p.product_code) like ? ESCAPE '/' ");
	 * params.add(StringUtility.toOracleSearchLikeSuffix(pdCode.toUpperCase()));
	 * } if (!StringUtility.isNullOrEmpty(pdName)) {
	 * sql.append(" AND upper(product_name) like ? ESCAPE '/' ");
	 * params.add(StringUtility.toOracleSearchLike(pdName.toUpperCase())); } if
	 * (!StringUtility.isNullOrEmpty(ppCode)) {
	 * sql.append(" AND UPPER(pp.promotion_program_code) like ? ESCAPE '/' ");
	 * params.add(StringUtility.toOracleSearchLikeSuffix(ppCode.toUpperCase()));
	 * }
	 * 
	 * selectSql.append(sql);
	 * countSql.append(selectSql.toString().replaceFirst("select \\*", ""));
	 * 
	 * selectSql.append(" order by p.product_code ");
	 * selectSql.append(") where 1 = 1) ");
	 * selectSql.append(", data_final as ( "); selectSql.append("select ");
	 * selectSql.append("    data.*, ");
	 * selectSql.append("    (case when pr_cus is not null then pr_cus ");
	 * selectSql.append(
	 * "    when pr_cus_type_and_shop_id is not null then pr_cus_type_and_shop_id "
	 * ); selectSql.append(
	 * "    when pr_cus_type_and_shop_type is not null then pr_cus_type_and_shop_type "
	 * );
	 * selectSql.append("    when pr_cus_type is not null then pr_cus_type ");
	 * selectSql.append("    when pr_shop_id is not null then pr_shop_id ");
	 * selectSql.append("    when pr_shop_type is not null then pr_shop_type ");
	 * selectSql.append("    else -1 ");
	 * selectSql.append("    end ) price_id ");
	 * selectSql.append(" from resultALL rsa, data  "); //
	 * selectSql.append(" where 1=1  ) ");
	 * selectSql.append(" where rsa.product_id = data.productid  ) ");
	 * selectSql.append(
	 * " select data_final.*, price.price, price.package_price as packagePrice from data_final, price where price.price_id = data_final.price_id "
	 * ); String[] fieldNames = new String[] { "productId", // 1
	 * "productCode",// 2 "productName", // 3 "price", // 4 "packagePrice",//5
	 * "promotionProgramCode", // 6 "convfact", //7 "isFocus", //8
	 * "grossWeight", //9 "netWeight", //10 "checkLot", }; Type[] fieldTypes =
	 * new Type[] { StandardBasicTypes.LONG,// 1 StandardBasicTypes.STRING,// 2
	 * StandardBasicTypes.STRING,// 3 StandardBasicTypes.BIG_DECIMAL,// 4
	 * StandardBasicTypes.BIG_DECIMAL,// 5 StandardBasicTypes.STRING,// 6
	 * StandardBasicTypes.INTEGER,// 7 StandardBasicTypes.LONG,// 8
	 * StandardBasicTypes.FLOAT,// 9 StandardBasicTypes.FLOAT,// 10
	 * StandardBasicTypes.INTEGER };
	 * 
	 * if (paging != null) { return
	 * repo.getListByQueryAndScalarPaginated(OrderProductVO.class, fieldNames,
	 * fieldTypes, selectSql.toString(), countSql.toString(), params, params,
	 * paging); } else { return
	 * repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames,
	 * fieldTypes, selectSql.toString(), params); } }
	 */

	@Override
	public List<OrderProductVO> getListOrderProductVO(KPaging<OrderProductVO> paging, Long shopId, Long staffId, Long custId, String pdCode, String pdName, String ppCode, Date lockDay, Long custTypeId, Long shopTypeId) throws DataAccessException {
		//hardcode test
		//		shopId = (long) 2807;
		//		custId = (long) 168518;
		//		staffId = (long) 805;
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (staffId == null) {
			throw new IllegalArgumentException("staffId is null");
		}
		if (custId == null) {
			throw new IllegalArgumentException("custId is null");
		}

		StringBuilder sql = new StringBuilder();
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		//lay gia theo don vi
		selectSql.append("with ");
		selectSql.append("lstShop as ( ");
		selectSql.append("    select shop_id ,level as lv ");
		selectSql.append("    from shop s1 ");
		selectSql.append("    where status = 1 ");
		selectSql.append("    start with s1.shop_id           = ?");
		params.add(shopId);
		selectSql.append("    connect by prior parent_shop_id = shop_id ");
		selectSql.append(") ");
		selectSql.append(",lstFocus as ( ");
		selectSql.append(" 		    select distinct fcmp.product_id productId, MIN(nvl((select cast(value as number) from ap_param where ap_param_code = fcmp.type),1000)) orderIndex "); // vuongmq; 19/01/2016; distinct CT trong tam, HuongNT128 nho
		selectSql.append(" 		    from focus_channel_map_product fcmp ");
		selectSql.append(" 		    join focus_channel_map fcm on fcmp.focus_channel_map_id = fcm.focus_channel_map_id ");
		selectSql.append(" 		    JOIN FOCUS_SHOP_MAP fsm on fcm.FOCUS_PROGRAM_ID = fsm.FOCUS_PROGRAM_ID ");
		selectSql.append(" 		    where fcm.sale_type_code in (select sale_type_code from staff where staff_id = ?) ");
		selectSql.append(" 		    and fsm.shop_id in (select shop_id from lstShop) ");
		params.add(staffId);
		selectSql.append(" 			group by fcmp.product_id ");
		selectSql.append(") ");
		//		selectSql.append(",prc as ( ");
		//		selectSql.append("    SELECT	product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		//		selectSql.append("    FROM	 price	prc ");
		//		selectSql.append("    WHERE	1 = 1 ");
		//		selectSql.append("    AND prc.status = 1 ");
		//		selectSql.append("    and prc.from_date <= trunc(?) ");
		//		params.add(lockDay);
		//		selectSql.append("    and (prc.to_date >= trunc(?) or prc.to_date is null) ");
		//		params.add(lockDay);
		//		selectSql.append("    and (shop_id is null or shop_id in (select shop_id from lstShop)) ");
		//		selectSql.append(") ");
		//		selectSql.append(",prcX as ( ");
		//		selectSql.append("    select ");
		//		selectSql.append("        product_id,customer_id,customer_type_id,prc.shop_id,shop_type_id, from_date, to_date ,price_id ");
		//		selectSql.append("        ,lstShop.lv, rank() over(PARTITION BY product_id ORDER BY lstShop.lv) rank_last ");
		//		selectSql.append("    from prc join lstShop on prc.shop_id = lstShop.shop_id ");
		//		selectSql.append("    where prc.customer_type_id is null  ");
		//		selectSql.append("    union all ");
		//		selectSql.append("    select ");
		//		selectSql.append("         product_id,customer_id,customer_type_id,prc.shop_id,shop_type_id, from_date, to_date ,price_id ");
		//		selectSql.append("        ,lstShop.lv, rank() over(PARTITION BY product_id ORDER BY lstShop.lv) rank_last ");
		//		selectSql.append("    from prc join lstShop on prc.shop_id = lstShop.shop_id ");
		//		selectSql.append("    where prc.customer_type_id = ?  ");
		//		params.add(custTypeId);
		//		selectSql.append(") ");
		//		selectSql.append(",prc_rec as( ");
		//		selectSql.append("    select  product_id,shop_id,lv, rank_last, from_date, to_date ,price_id,customer_id,customer_type_id,shop_type_id ");
		//		selectSql.append("    from prcX ");
		//		selectSql.append("    where rank_last = 1 ");
		//		selectSql.append(") ");
		//		selectSql.append(",result as ( ");
		//		selectSql.append("    select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		//		selectSql.append("    from prc_rec ");
		//		selectSql.append("    union all ");
		//		selectSql.append("    select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		//		selectSql.append("    from prc where customer_id = ?  ");
		//		params.add(custId);
		//		selectSql.append("    union all ");
		//		selectSql.append("    select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		//		selectSql.append("    from prc where customer_type_id = ? and shop_id is null and (shop_type_id is null or shop_type_id = ?)  ");
		//		params.add(custTypeId);
		//		params.add(shopTypeId);
		//		selectSql.append("    union all ");
		//		selectSql.append("    select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		//		selectSql.append("    from prc where customer_type_id is null and shop_id is null and shop_type_id = ?  ");
		//		params.add(shopTypeId);
		//		selectSql.append(") ");
		//		selectSql.append(",resultAll as ( ");
		//		selectSql.append("    SELECT ");
		//		selectSql.append("        result.product_id, ");
		//		selectSql.append("        max(case when result.customer_id = ? then price_id end) pr_cus, ");
		//		params.add(custId);
		//		selectSql.append("        max(case when result.customer_id is null and result.customer_type_id = ? ");
		//		params.add(custTypeId);
		//		selectSql.append("          and result.shop_id is not null and result.shop_type_id is null  then price_id	end ");
		//		selectSql.append("        ) pr_cus_type_and_shop_id, ");
		//		selectSql.append("        max( ");
		//		selectSql.append("          case when result.customer_id is null and result.customer_type_id = ? ");
		//		params.add(custTypeId);
		//		selectSql.append("          and result.shop_id is null and result.shop_type_id = ? then price_id end ");
		//		params.add(shopTypeId);
		//		selectSql.append("        ) pr_cus_type_and_shop_type, ");
		//		selectSql.append("        max( ");
		//		selectSql.append("          case when result.customer_id is null and result.customer_type_id = ? ");
		//		params.add(custTypeId);
		//		selectSql.append("          and result.shop_id is null and result.shop_type_id is null then price_id end ");
		//		selectSql.append("        ) pr_cus_type, ");
		//		selectSql.append("        max( ");
		//		selectSql.append("          case when result.customer_id is null and result.customer_type_id is null ");
		//		selectSql.append("          and result.shop_id is not null and result.shop_type_id is null then price_id end ");
		//		selectSql.append("        ) pr_shop_id, ");
		//		selectSql.append("        max( ");
		//		selectSql.append("          case when result.customer_id is null  and result.customer_type_id is null ");
		//		selectSql.append("          and result.shop_id  is null and result.shop_type_id =  ? then price_id ");
		//		params.add(shopTypeId);
		//		selectSql.append("          end ");
		//		selectSql.append("        ) pr_shop_type ");
		//		selectSql.append("    FROM	result ");
		//		selectSql.append("    group by product_id ");
		//		selectSql.append(") ");	

		//--lay danh sach gia
		selectSql.append(",resultAll as (select * from PRICE_CUSTOMER_DEDUCED where customer_id = ? and shop_id = ? and status = ? ");
		params.add(custId);
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		selectSql.append(" and from_date < trunc(?)+1 and (to_date is null or to_date >= trunc(?)) ");
		params.add(lockDay);
		params.add(lockDay);
		selectSql.append(" ) ");
		//--lay danh sach product
		selectSql.append(" , data as ( ");
		selectSql.append("select * from (");
		selectSql.append(" SELECT distinct(p.product_id) as productId, ");
		selectSql.append("   p.product_code as productCode, ");
		selectSql.append("   p.product_name as productName, ");
		selectSql.append("   pp.promotion_program_code as promotionProgramCode, ");
		selectSql.append("   p.convfact                as convfact, ");
		selectSql.append("   0   as isFocus, ");
		selectSql.append("   p.gross_weight as grossWeight, ");
		selectSql.append("   p.net_weight as netWeight, ");
		selectSql.append("   p.check_lot as checkLot, ");
		selectSql.append("  nvl((select orderIndex from lstFocus where productId = p.product_id),1000) orderIndex1, ");
		selectSql.append("   p.order_index orderIndex2 ");
		countSql.append(" SELECT count(1) as count");

		sql.append(" FROM product p");
		sql.append(" JOIN stock_total st");
		sql.append(" ON (st.product_id = p.product_id ");
		sql.append(" AND st.object_id   = ?");
		params.add(shopId);
		sql.append(" AND st.object_type = ? and st.status = ?)");
		params.add(StockObjectType.SHOP.getValue());
		params.add(ActiveType.RUNNING.getValue());
		sql.append("  AND ( (p.sub_cat_id IN (SELECT cat_id FROM staff_sale_cat WHERE staff_id = ?) ");
		sql.append("       AND (SELECT COUNT(1) FROM staff_sale_cat WHERE staff_id = ?) > 0) ");
		sql.append(" 	 or (SELECT COUNT(1) FROM staff_sale_cat WHERE staff_id = ?) = 0 ");
		sql.append(" 	 ) ");
		params.add(staffId);
		params.add(staffId);
		params.add(staffId);
		//		//Khong lay trong nganh hang Z
		//		sql.append(" and exists (select 1 from product_info pi where p.cat_id = pi.product_info_id ");
		//		sql.append(" and pi.product_info_code not in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");

		sql.append(" LEFT JOIN");
		sql.append(" (SELECT DISTINCT gld.product_id AS product_id, pp.promotion_program_code AS promotion_program_code");
		sql.append(" FROM promotion_program pp ");
		sql.append(" join product_group pg on pp.promotion_program_id = pg.promotion_program_id and pg.group_type = 1 ");
		sql.append(" join group_level gl on gl.product_group_id = pg.product_group_id ");
		sql.append(" join group_level_detail gld on gld.group_level_id = gl.group_level_id ");
		sql.append(" WHERE pp.status   = 1 and pp.type not in ('ZM', 'ZD', 'ZH', 'ZT')");
		sql.append(" AND pp.from_date < TRUNC(?) + 1 ");
		params.add(lockDay);
		sql.append(" AND (pp.to_date    >= TRUNC(?) or pp.to_date is null)");
		params.add(lockDay);
		sql.append(" and pg.status = 1");
		sql.append(" and gl.status = 1 ");
		sql.append(" and gld.status = 1 ");
		sql.append(" AND EXISTS ");
		sql.append("   (SELECT 1 ");
		sql.append("   FROM promotion_shop_map psm ");
		sql.append("   WHERE psm.promotion_program_id = pp.promotion_program_id and status = 1");
		sql.append("   AND (psm.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id)) ");
		params.add(shopId);
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("   AND psm.from_date < TRUNC(?) + 1  ");
		params.add(lockDay);
		sql.append("   AND (psm.to_date    >= TRUNC(?) or psm.to_date is null) ");
		params.add(lockDay);
		//end

		//TODO vuongmq 05/02/2016; comment duoc huong CTKM la hien thi len
		//sql.append(" AND ( (quantity_max IS NULL OR NVL(quantity_received_total, 0) < quantity_max) ) ");
		
		//TODO vuongmq 13/01/2016; comment khong kiem tra CTKM cho NVBH, KH
		//Kiem tra KM ap dung cho KH
		/*sql.append(" AND (  ");

		sql.append("         (not exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1) and ((psm.quantity_max is null or NVL(psm.quantity_received_total, 0) < psm.quantity_max)) )");
		sql.append("         or ");
		sql.append("         exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and customer_id = ? and ((quantity_max is null or NVL(quantity_received_total, 0) < quantity_max))) ");
		params.add(custId);

		sql.append(" 	 ) ");
		//Kiem tra KM ap dung cho NVBH
		sql.append(" AND (  ");

		sql.append("         (not exists (select 1 from promotion_staff_map where promotion_shop_map_id = psm.promotion_shop_map_id and shop_id = ? and status = 1) and ((psm.quantity_max is null or NVL(psm.quantity_received_total, 0) < psm.quantity_max)) )");
		sql.append("         or ");
		sql.append("         exists (select 1 from promotion_staff_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and staff_id = ? and shop_id = ? and ((quantity_max is null or NVL(quantity_received_total, 0) < quantity_max))) ");
		params.add(shopId);
		params.add(staffId);
		params.add(shopId);

		sql.append(" 	 ) ");*/
		//end Kiem tra KM ap dung cho NVBH
		sql.append("   ) ");
		//==============
		sql.append(" and (not exists ");
		sql.append(" 	(select 1 ");
		sql.append(" 	 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 	 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 	  and status = 1) ");
		sql.append("    or (exists ");
		sql.append(" 		(select 1 ");
		sql.append(" 		 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 		 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 		  and status = 1) ");
		sql.append(" 	   and (not exists ( ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						  and status = 1) ");
		sql.append(" 						 minus ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						   and status = 1 ");
		sql.append(" 						   and ( ");
		sql.append(" 						   		(object_type = 1 ");
		sql.append(" 								 and ( ");
		sql.append(" 								 	(exists ");
		sql.append(" 										(select 1 ");
		sql.append(" 										 from customer_attribute ");
		sql.append(" 										 where type = 1 ");
		sql.append(" 										  and customer_attribute_id = pa.object_id ");
		sql.append(" 										  and status = 1) ");
		sql.append(" 										and exists ( ");
		sql.append(" 											select 1 ");
		sql.append(" 											from customer_attribute_detail ");
		sql.append(" 											where customer_id = ? ");
		params.add(custId);
		sql.append(" 											and status = 1 ");
		sql.append(" 											and customer_attribute_id = pa.object_id ");
		sql.append(" 											and upper(value) = upper(pa.from_value) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 2 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(custId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and to_number(value) >= to_number(pa.from_value) ");
		sql.append(" 												and to_number(value) <= to_number(pa.to_value) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 3 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(custId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') >= to_date(pa.from_value, 'yyyy-mm-dd') or pa.from_value is null) ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') <= to_date(pa.to_value, 'yyyy-mm-dd') or pa.to_value is null) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type in (4, 5) ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail cad ");
		sql.append(" 												join promotion_cust_attr_detail pcad  ");
		sql.append(" 													on pcad.object_type = 1  ");
		sql.append(" 													and pcad.status = 1 ");
		sql.append(" 													and cad.customer_attribute_enum_id = pcad.object_id ");
		sql.append(" 												where cad.customer_id = ? ");
		params.add(custId);
		sql.append(" 												and cad.status = 1 ");
		sql.append(" 												and pcad.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
		sql.append(" 												and cad.customer_attribute_id = pa.object_id ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 								) ");
		sql.append(" 								or (object_type = 2 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID = ");
		sql.append(" 										(select CHANNEL_TYPE_ID ");
		sql.append(" 										 from CUSTOMER ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(custId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 								or (object_type = 3 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID in ");
		sql.append(" 										(select SALE_LEVEL_CAT_ID ");
		sql.append(" 										 from CUSTOMER_CAT_LEVEL ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(custId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 							) ");
		sql.append(" 						) ");
		sql.append(" 				) ");
		sql.append(" 			) ");
		sql.append(" 	) ");
		sql.append(" ) ");

		sql.append("   ) pp ON (p.product_id = pp.product_id)");

		sql.append(" WHERE p.status                 = ?");
		params.add(ActiveType.RUNNING.getValue());
		//Khong lay sp co lo
		sql.append(" and p.check_lot = 0 ");
		if (!StringUtility.isNullOrEmpty(pdCode)) {
			sql.append(" AND p.product_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(pdCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(pdName)) {
			sql.append(" AND lower(p.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(pdName).trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(ppCode)) {
			sql.append(" AND pp.promotion_program_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(ppCode.toUpperCase()));
		}

		selectSql.append(sql);
		countSql.append(selectSql.toString().replaceFirst("select \\*", ""));

		selectSql.append(") where 1 = 1) ");
		//		selectSql.append(", data_final as ( ");
		selectSql.append("select ");
		selectSql.append("    data.*, nvl(rsa.price,0) price, nvl(rsa.package_price,0) packagePrice ");
		//		selectSql.append("    (case when pr_cus is not null then pr_cus ");
		//		selectSql.append("    when pr_cus_type_and_shop_id is not null then pr_cus_type_and_shop_id ");
		//		selectSql.append("    when pr_cus_type_and_shop_type is not null then pr_cus_type_and_shop_type ");
		//		selectSql.append("    when pr_cus_type is not null then pr_cus_type ");
		//		selectSql.append("    when pr_shop_id is not null then pr_shop_id ");
		//		selectSql.append("    when pr_shop_type is not null then pr_shop_type ");
		//		selectSql.append("    else -1 ");
		//		selectSql.append("    end ) price_id ");
		selectSql.append(" from data left join resultALL rsa on  rsa.product_id = data.productid");
		//		selectSql.append(" where 1=1  ) ");
		selectSql.append(" where 1=1  ");
		//		selectSql.append(" select data_final.*, price.price, price.package_price as packagePrice from data_final, price where price.price_id = data_final.price_id ");
		selectSql.append(" order by orderIndex1, orderIndex2, productCode ");
		String[] fieldNames = new String[] { "productId", // 1
				"productCode",// 2
				"productName", // 3
				"price", // 4
				"packagePrice",//5
				"promotionProgramCode", // 6
				"convfact", //7
				"isFocus", //8
				"grossWeight", //9
				"netWeight", //10
				"checkLot", };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.BIG_DECIMAL,// 4
				StandardBasicTypes.BIG_DECIMAL,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.LONG,// 8
				StandardBasicTypes.FLOAT,// 9
				StandardBasicTypes.FLOAT,// 10
				StandardBasicTypes.INTEGER };

		if (paging != null) {
			return repo.getListByQueryAndScalarPaginated(OrderProductVO.class, fieldNames, fieldTypes, selectSql.toString().replaceAll("  ", " "), countSql.toString().replaceAll("  ", " "), params, params, paging);
		} else {
			return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, selectSql.toString().replaceAll("  ", " "), params);
		}
	}

	@Override
	public List<OrderProductVO> getListOrderProductVOWithZ(KPaging<OrderProductVO> paging, Long shopId, Long staffId, Long custId, String pdCode, String pdName, String ppCode, Date lockDay) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (staffId == null) {
			throw new IllegalArgumentException("staffId is null");
		}
		if (custId == null) {
			throw new IllegalArgumentException("custId is null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		selectSql.append(" SELECT p.product_id as productId,");
		selectSql.append("   p.product_code as productCode,");
		selectSql.append("   p.product_name as productName,");
		selectSql.append("   st.available_quantity as availableQuantity,");
		selectSql.append("   st.quantity as stock,");
		selectSql.append("   pr.price as price,");
		selectSql.append("   pp.promotion_program_code as promotionProgramCode,");
		selectSql.append("   p.convfact                as convfact,");
		selectSql.append("   NVL(fcmd.product_id, 0)   as isFocus,");
		selectSql.append("   p.gross_weight as grossWeight,");
		selectSql.append("   p.net_weight as netWeight,");
		selectSql.append("   p.check_lot as checkLot");

		countSql.append(" SELECT count(1) as count");

		sql.append(" FROM product p");
		sql.append(" LEFT JOIN stock_total st");
		sql.append(" ON (st.product_id = p.product_id ");
		sql.append(" AND st.object_id   = ?");
		params.add(shopId);
		sql.append(" AND st.object_type = ?)");
		params.add(StockObjectType.SHOP.getValue());
		sql.append(" JOIN price pr");
		sql.append(" ON (p.product_id         = pr.product_id");
		sql.append(" AND pr.from_date < TRUNC(?) + 1");
		params.add(lockDay);
		sql.append(" AND (pr.to_date         IS NULL");
		sql.append(" OR pr.to_date    >= TRUNC(?)) and pr.status = ?)");
		params.add(lockDay);
		params.add(ActiveType.RUNNING.getValue());
		//		sql.append(" AND p.cat_id NOT  IN ");
		//		sql.append("     (SELECT product_info_id ");
		//		sql.append("     FROM product_info ");
		//		sql.append("     WHERE status           = 1 ");
		//		sql.append("     AND type               = 1 ");
		//		sql.append("     AND product_info_code IN ");
		//		sql.append("       (SELECT ap_param_code ");
		//		sql.append("       FROM ap_param ");
		//		sql.append("       WHERE status = 1 ");
		//		sql.append("       AND type     = 'EQUIPMENT_CAT' ");
		//		sql.append("       ) ");
		//		sql.append("     ) ");
		//		sql.append("  AND ( (p.cat_id IN (SELECT cat_id FROM staff_sale_cat WHERE staff_id = ?) ");
		//		sql.append("       AND (SELECT COUNT(1) FROM staff_sale_cat WHERE staff_id = ?) > 0) ");
		//		sql.append(" 	 or (SELECT COUNT(1) FROM staff_sale_cat WHERE staff_id = ?) = 0 ");
		//		sql.append(" 	 ) ");
		//		params.add(staffId);
		//		params.add(staffId);
		//		params.add(staffId);

		sql.append(" LEFT JOIN");
		sql.append(" (SELECT DISTINCT ppd.product_id AS product_id, pp.promotion_program_code AS promotion_program_code");
		sql.append(" FROM promotion_program pp join promotion_program_detail ppd on pp.promotion_program_id = ppd.promotion_program_id");
		sql.append(" WHERE pp.status   = 1 and pp.type not in ('ZM', 'ZD', 'ZH', 'ZT')");
		sql.append(" AND pp.from_date < TRUNC(?) + 1 ");
		params.add(lockDay);
		sql.append(" AND (pp.to_date    >= TRUNC(?) or pp.to_date is null)");
		params.add(lockDay);
		sql.append(" AND EXISTS ");
		sql.append("   (SELECT 1 ");
		sql.append("   FROM promotion_shop_map psm ");
		sql.append("   WHERE psm.promotion_program_id = pp.promotion_program_id and status = 1");
		sql.append("   AND (psm.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id)) ");
		params.add(shopId);
		//NhanLT 12/03/2014 - Bo sung check fromDate, toDate cua promotion_shop_map
		sql.append("   AND psm.from_date < TRUNC(?) + 1  ");
		params.add(lockDay);
		sql.append("   AND (psm.to_date    >= TRUNC(?) or psm.to_date is null) ");
		params.add(lockDay);
		//end
		sql.append(" AND ( (quantity_max IS NULL OR NVL(quantity_received, 0) < quantity_max) ) ");
		sql.append(" AND (  ");

		sql.append("         (not exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1) and ((psm.quantity_max is null or NVL(psm.quantity_received, 0) < psm.quantity_max)) )");
		sql.append("         or ");
		sql.append("         exists (select 1 from promotion_customer_map where promotion_shop_map_id = psm.promotion_shop_map_id and status = 1 and customer_id = ? and ((quantity_max is null or NVL(quantity_received, 0) < quantity_max))) ");
		params.add(custId);

		sql.append(" 	 ) ");
		sql.append("   ) ");

		sql.append(" and (not exists ");
		sql.append(" 	(select 1 ");
		sql.append(" 	 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 	 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 	  and status = 1) ");
		sql.append("    or (exists ");
		sql.append(" 		(select 1 ");
		sql.append(" 		 from PROMOTION_CUST_ATTR pa ");
		sql.append(" 		 where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 		  and status = 1) ");
		sql.append(" 	   and (not exists ( ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						  and status = 1) ");
		sql.append(" 						 minus ");
		sql.append(" 						 (select * ");
		sql.append(" 						  from PROMOTION_CUST_ATTR pa ");
		sql.append(" 						  where pa.promotion_program_id = pp.promotion_program_id ");
		sql.append(" 						   and status = 1 ");
		sql.append(" 						   and ( ");
		sql.append(" 						   		(object_type = 1 ");
		sql.append(" 								 and ( ");
		sql.append(" 								 	(exists ");
		sql.append(" 										(select 1 ");
		sql.append(" 										 from customer_attribute ");
		sql.append(" 										 where type = 1 ");
		sql.append(" 										  and customer_attribute_id = pa.object_id ");
		sql.append(" 										  and status = 1) ");
		sql.append(" 										and exists ( ");
		sql.append(" 											select 1 ");
		sql.append(" 											from customer_attribute_detail ");
		sql.append(" 											where customer_id = ? ");
		params.add(custId);
		sql.append(" 											and status = 1 ");
		sql.append(" 											and customer_attribute_id = pa.object_id ");
		sql.append(" 											and upper(value) = upper(pa.from_value) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 2 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(custId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and to_number(value) >= to_number(pa.from_value) ");
		sql.append(" 												and to_number(value) <= to_number(pa.to_value) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type = 3 ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail ");
		sql.append(" 												where customer_id = ? ");
		params.add(custId);
		sql.append(" 												and status = 1 ");
		sql.append(" 												and customer_attribute_id = pa.object_id ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') >= to_date(pa.from_value, 'yyyy-mm-dd') or pa.from_value is null) ");
		sql.append(" 												and (to_date(value, 'yyyy-mm-dd') <= to_date(pa.to_value, 'yyyy-mm-dd') or pa.to_value is null) ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									  or (exists ");
		sql.append(" 										   (select 1 ");
		sql.append(" 										    from customer_attribute ");
		sql.append(" 										    where type in (4, 5) ");
		sql.append(" 										     and customer_attribute_id = pa.object_id ");
		sql.append(" 										     and status = 1) ");
		sql.append(" 									  		and exists ( ");
		sql.append(" 									  			select 1 ");
		sql.append(" 												from customer_attribute_detail cad ");
		sql.append(" 												join promotion_cust_attr_detail pcad  ");
		sql.append(" 													on pcad.object_type = 1  ");
		sql.append(" 													and pcad.status = 1 ");
		sql.append(" 													and cad.customer_attribute_enum_id = pcad.object_id ");
		sql.append(" 												where cad.customer_id = ? ");
		params.add(custId);
		sql.append(" 												and cad.status = 1 ");
		sql.append(" 												and pcad.promotion_cust_attr_id = pa.promotion_cust_attr_id ");
		sql.append(" 												and cad.customer_attribute_id = pa.object_id ");
		sql.append(" 									  		) ");
		sql.append(" 										) ");
		sql.append(" 									) ");
		sql.append(" 								) ");
		sql.append(" 								or (object_type = 2 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID = ");
		sql.append(" 										(select CHANNEL_TYPE_ID ");
		sql.append(" 										 from CUSTOMER ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(custId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 								or (object_type = 3 ");
		sql.append(" 									and exists ");
		sql.append(" 									 (select 1 ");
		sql.append(" 									  from PROMOTION_CUST_ATTR_DETAIL ");
		sql.append(" 									  where status = 1 ");
		sql.append(" 									   and PROMOTION_CUST_ATTR_ID = PA.PROMOTION_CUST_ATTR_ID ");
		sql.append(" 									   and OBJECT_ID in ");
		sql.append(" 										(select SALE_LEVEL_CAT_ID ");
		sql.append(" 										 from CUSTOMER_CAT_LEVEL ");
		sql.append(" 										 where CUSTOMER_ID = ? ");
		params.add(custId);
		sql.append(" 										  and status = 1))) ");
		sql.append(" 							) ");
		sql.append(" 						) ");
		sql.append(" 				) ");
		sql.append(" 			) ");
		sql.append(" 	) ");
		sql.append(" ) ");

		sql.append("   ) pp ON (p.product_id = pp.product_id)");

		sql.append(" LEFT JOIN");
		sql.append("  (SELECT DISTINCT fcmd.product_id AS product_id");
		sql.append("   FROM focus_program fp");
		sql.append("   JOIN focus_shop_map fsm");
		sql.append("   ON (fp.focus_program_id = fsm.focus_program_id )");
		sql.append("   JOIN focus_channel_map fcm");
		sql.append("   ON (fp.focus_program_id = fcm.focus_program_id)");
		sql.append("   JOIN focus_channel_map_product fcmd");
		sql.append("   ON (fcm.focus_channel_map_id = fcmd.focus_channel_map_id )");
		sql.append("   JOIN staff s");
		sql.append("   ON (fcm.sale_type_code = s.sale_type_code)");
		sql.append("   WHERE 1               = 1");
		sql.append("   AND fsm.shop_id       = ?");
		params.add(shopId);
		sql.append("   AND fp.status         = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   AND fp.from_date      < TRUNC(?) + 1");
		params.add(lockDay);
		sql.append("   AND (fp.to_date      IS NULL");
		sql.append("   OR (fp.to_date       >= TRUNC(?)))");
		params.add(lockDay);
		sql.append("   AND fsm.status        = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   AND fcm.status        = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   AND s.staff_id   = ?");
		params.add(staffId);
		sql.append("   ) fcmd ON (fcmd.product_id   = p.product_id)");
		sql.append(" WHERE p.status                 = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (!StringUtility.isNullOrEmpty(pdCode)) {
			sql.append(" AND p.product_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(pdCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(pdName)) {
			sql.append(" AND lower(p.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(pdName).trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(ppCode)) {
			sql.append(" AND pp.promotion_program_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(ppCode.toUpperCase()));
		}

		selectSql.append(sql);
		countSql.append(sql);

		selectSql.append(" order by p.product_code");
		String[] fieldNames = new String[] { "productId", // 1
				"productCode",// 2
				"productName", // 3
				"price", // 4
				"availableQuantity",// 5
				"stock",// 5
				"promotionProgramCode", // 6
				"convfact", //7
				"isFocus", //8
				"grossWeight", //9
				"netWeight", //10
				"checkLot" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.BIG_DECIMAL,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.LONG,// 8
				StandardBasicTypes.FLOAT,// 9
				StandardBasicTypes.FLOAT,// 10
				StandardBasicTypes.INTEGER };
		if (paging != null) {
			return repo.getListByQueryAndScalarPaginated(OrderProductVO.class, fieldNames, fieldTypes, selectSql.toString().replaceAll("  ", " "), countSql.toString().replaceAll("  ", " "), params, params, paging);
		} else {
			return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, selectSql.toString().replaceAll("  ", " "), params);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.ProductDAO#getListCompareBookAndDeliveryProduct(
	 * java.lang.Long, java.util.Date, java.util.Date)
	 */
	@Override
	public List<RptCompareBookAndDeliveryProductVO> getListCompareBookAndDeliveryProduct(Long shopId, Date fromDate, Date toDate) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptCompareBookAndDeliveryProductVO>();

		if (shopId == null || shopId <= 0) {
			throw new IllegalArgumentException("shopId is null or <= 0");
		}
		if (fromDate == null) {
			throw new IllegalArgumentException("fromDate is null");
		}
		if (toDate == null) {
			throw new IllegalArgumentException("toDate is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT tb1.po_auto_number AS poAutoNumber,");
		sql.append(" tb1.product_id          AS productId,");
		sql.append(" tb1.quantity            AS bookQuantity,");
		sql.append(" tb1.product_code        AS productCode,");
		sql.append(" tb1.product_name        AS productName,");
		sql.append(" tb1.convfact            AS bookConvfact,");
		sql.append(" tb2.quantity            AS saleQuantity,");
		sql.append(" tb2.convfact            AS saleConvfact");
		sql.append(" FROM");
		sql.append(" (SELECT po.po_auto_number po_auto_number, po.shop_id, po.po_vnm_date,");
		sql.append(" po_d.quantity, po_d.product_id product_id, p.convfact, p.product_code, p.product_name");
		sql.append(" FROM po_vnm po, po_vnm_detail po_d, product p");
		sql.append(" WHERE po.po_vnm_id = po_d.po_vnm_id AND po.type = ? AND p.product_id = po_d.product_id");
		params.add(PoType.PO_CUSTOMER_SERVICE.getValue());
		sql.append(" ORDER BY po.po_auto_number, p.product_code) tb1");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT po.po_auto_number po_auto_number, po_d.quantity, po_d.product_id product_id, p.convfact");
		sql.append(" FROM (SELECT *  FROM po_vnm WHERE po_auto_number IN (SELECT po_auto_number FROM po_vnm WHERE type = 1)) po,");
		sql.append(" po_vnm_detail po_d, product p");
		sql.append(" WHERE po.po_vnm_id = po_d.po_vnm_id AND po.type = ? AND po.status = ?");
		params.add(PoType.PO_CONFIRM.getValue());
		params.add(PoVNMStatus.IMPORTED.getValue());
		sql.append(" AND p.product_id   = po_d.product_id");
		sql.append(" ORDER BY po.po_auto_number ) tb2");
		sql.append(" ON (tb1.po_auto_number = tb2.po_auto_number");
		sql.append(" AND tb1.product_id     = tb2.product_id)");
		sql.append(" and tb1.shop_id = ?");
		params.add(shopId);
		if (fromDate != null) {
			sql.append(" and tb1.po_vnm_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and tb1.po_vnm_date < (trunc(?) + 1)");
			params.add(toDate);
		}
		sql.append(" order by tb1.product_code");
		final String[] fieldNames = new String[] { //
		"poAutoNumber",// 1
				"productId", // 2
				"bookQuantity", // 3
				"productCode", // 3
				"productName",//4
				"bookConvfact", // 5
				"saleQuantity", // 6
				"saleConvfact", // 7
		};

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.STRING,// 1
				StandardBasicTypes.LONG,// 2
				StandardBasicTypes.INTEGER,// 3
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.INTEGER,// 6
				StandardBasicTypes.INTEGER,// 7
		};
		return repo.getListByQueryAndScalar(RptCompareBookAndDeliveryProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/*
	 * @Override public List<RptCompareImExStF1DataVO>
	 * getDataForComparePOAutoReport( Long shopId, Long poId, int
	 * workDateInMonth, int realWorkDateInMonth) throws DataAccessException { if
	 * (shopId == null) { throw new DataAccessException("shopId is null"); }
	 * else if (null == poId) { throw new
	 * DataAccessException("Po auto id is null"); } else if (workDateInMonth <=
	 * 0) { throw new DataAccessException("workDateInMonth <= 0"); } else if
	 * (realWorkDateInMonth <= 0) { throw new
	 * DataAccessException("realWorkDateInMonth <= 0"); }
	 * 
	 * List<Object> params = new ArrayList<Object>(); StringBuilder sql = new
	 * StringBuilder();
	 * 
	 * sql.append(" SELECT po.productInfoCode productInfoCode, ");
	 * sql.append(" 		po.productCode productCode, ");
	 * sql.append(" 		po.productName productName, ");
	 * sql.append(" 		po.openStockTotal openStockTotal, "); sql.append(
	 * " 		po.impPoVnmQty + po.impSaleOrderQty + po.impStockTransQty importQty, "
	 * ); sql.append(
	 * " 		po.expSaleOrderQty + po.expPoVnmQty + po.expVanStockTransQty + po.expEditStockTransQty exportQty, "
	 * ); sql.append(" 		po.stockQty stockQty, ");
	 * sql.append(" 		po.expSaleOrderQty + po.expVansaleQty monthCumulate, ");
	 * sql.append(" 		po.monthPlan monthPlan, ");
	 * sql.append(" 		round(po.monthPlan / ?) dayPlan, "); sql.append(
	 * " 		decode(round(po.monthPlan / ?), 0, 0, round(po.stockQty / round(po.monthPlan / ?), 2)) dayReservePlan, "
	 * ); sql.append(
	 * " 		decode(po.expSaleOrderQty + po.expVansaleQty, 0, 0, round(po.stockQty / ((po.expSaleOrderQty + po.expVansaleQty) / ?), 2)) dayReserveReal, "
	 * ); sql.append(" 		po.safetyStockMin min, ");
	 * sql.append(" 		po.lead lead, "); sql.append(" 		po.next next, ");
	 * sql.append(
	 * " 		(po.safetyStockMin + po.lead + po.next) * round(po.monthPlan / ?) requimentStock, "
	 * ); sql.append(" 		po.poConfirmQty poConfirmQty, ");
	 * sql.append(" 		po.qtyPoDvkh - po.qtyRecPoDvkh stockPoDvkh, ");
	 * sql.append(" 		po.convfact convfact, ");
	 * sql.append(" 		po.percentage percentage, ");
	 * sql.append(" 		po.price price, ");
	 * sql.append(" 		po.poImport poImport, ");
	 * sql.append(" 		po.poExport poExport, ");
	 * sql.append(" 		po.poStock poStock, ");
	 * sql.append(" 		po.poMonthCumulate poMonthCumulate, ");
	 * sql.append(" 		po.poQuantity poQuantity, ");
	 * sql.append(" 		po.poConvfact poConvfact, ");
	 * sql.append(" 		? as realSaleDateInMonth ");
	 * 
	 * params.add(workDateInMonth);// tong so ngay ban hang
	 * params.add(workDateInMonth);// tong so ngay ban hang
	 * params.add(workDateInMonth);// tong so ngay ban hang
	 * params.add(realWorkDateInMonth);// so ngay ban hang thuc te
	 * params.add(workDateInMonth);// tong so ngay ban hang
	 * params.add(realWorkDateInMonth);// so ngay ban hang thuc te
	 * 
	 * sql.append(" FROM ");
	 * sql.append("   (SELECT pif.product_info_code productInfoCode, ");
	 * sql.append("           pd.product_code productCode, ");
	 * sql.append("           pd.product_name productName, "); sql.append(
	 * "           nvl(rpt_stock_total_day_tmp.close_stock_total, 0) openStockTotal, "
	 * ); sql.append(
	 * "           nvl(po_vnm_detail_tmp_1.pvd_quantity, 0) impPoVnmQty, ");
	 * sql.append(
	 * "           nvl(sale_order_detail_tmp_1.sod_quantity, 0) impSaleOrderQty, "
	 * ); sql.append(
	 * "           nvl(stock_trans_detail_tmp_1.std_quantity, 0) impStockTransQty, "
	 * ); sql.append(
	 * "           nvl(sale_order_detail_tmp_2.sod_quantity, 0) expSaleOrderQty, "
	 * ); sql.append(
	 * "           nvl(po_vnm_detail_tmp_2.pvd_quantity, 0) expPoVnmQty, ");
	 * sql.
	 * append("           nvl(std_exp_van.std_quantity, 0) expVanStockTransQty, "
	 * ); sql.append(
	 * "           nvl(std_exp_edit.std_quantity, 0) expEditStockTransQty, ");
	 * sql.append(
	 * "           nvl(sale_order_detail_tmp_3.sod_quantity, 0) expVansaleQty, "
	 * );
	 * sql.append("           nvl(stock_total_tmp.st_quantity, 0) stockQty, ");
	 * sql.append("           nvl(sale_plan_tmp.sl_quantity, 0) monthPlan, ");
	 * sql.append(
	 * "           nvl(sp_tmp.safety_stock_min, nvl(sp_tmp_cat.safety_stock_min, 0)) safetyStockMin, "
	 * );
	 * sql.append("           nvl(sp_tmp.lead, nvl(sp_tmp_cat.lead, 0)) lead, "
	 * );
	 * sql.append("           nvl(sp_tmp.next, nvl(sp_tmp_cat.next, 0)) next, "
	 * ); sql.append(
	 * "           nvl(sp_tmp.percentage, nvl(sp_tmp_cat.percentage, 0)) percentage, "
	 * ); sql.append(
	 * "           nvl(po_vnm_detail_tmp_3.pvd_quantity, 0) poConfirmQty, ");
	 * sql.append("           nvl(pvd_tmp_3.pvd_quantity, 0) qtyPoDvkh, ");
	 * sql.append
	 * ("           nvl(pvd_tmp_3.pvd_quantity_received, 0) qtyRecPoDvkh, ");
	 * sql.append("           nvl(pd.convfact, 0) convfact, ");
	 * sql.append("           nvl(pr.price, 0) price, ");
	 * sql.append("           nvl(poAuto.poImport, 0) poImport, ");
	 * sql.append("           nvl(poAuto.poExport, 0) poExport, ");
	 * sql.append("           nvl(poAuto.poStock, 0) poStock, ");
	 * sql.append("           nvl(poAuto.poMonthCumulate, 0) poMonthCumulate, "
	 * ); sql.append("           nvl(poAuto.poQuantity, 0) poQuantity, ");
	 * sql.append("           nvl(poAuto.poConvfact, 0) poConvfact ");
	 * sql.append(
	 * "    FROM (SELECT distinct pro.status as status, pro.product_level_id as product_level_id, pro.product_id as product_id, pro.product_code as product_code,  "
	 * ); sql.append(
	 * "                             pro.convfact as convfact, pro.product_name as product_name, pro.cat_id as cat_id "
	 * );
	 * sql.append("         FROM po_auto po, po_auto_detail po_d, product pro "
	 * ); sql.append("         where po.po_auto_id = po_d.po_auto_id ");
	 * sql.append(" 			and pro.product_id = po_d.product_id ");
	 * sql.append("             and po.po_auto_id = ?) pd ");
	 * 
	 * params.add(poId);
	 * 
	 * // Nhap tu VNM ve sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT pvd.product_id product_id, ");
	 * sql.append("              sum(pvd.quantity) pvd_quantity ");
	 * sql.append("       FROM po_vnm pv, ");
	 * sql.append("            po_vnm_detail pvd "); sql.append(
	 * "       WHERE pv.po_vnm_id = pvd.po_vnm_id and pv.object_type = 1 ");
	 * sql.append("         AND pv.type = ? ");
	 * sql.append("         AND pv.status = ? ");
	 * sql.append("         AND pv.shop_id = ? "); sql.append(
	 * "         AND pv.import_date >= trunc(sysdate, 'MONTH') AND pv.import_date < (trunc(sysdate) + 1) "
	 * ); sql.append(
	 * "       GROUP BY pvd.product_id) po_vnm_detail_tmp_1 ON po_vnm_detail_tmp_1.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(PoType.PO_CONFIRM.getValue());
	 * params.add(PoVNMStatus.IMPORTED.getValue()); params.add(shopId);
	 * 
	 * // Nhap tra hang tu khach hang sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT sod.product_id, ");
	 * sql.append("              sum(sod.quantity) sod_quantity ");
	 * sql.append("       FROM sale_order so, ");
	 * sql.append("            sale_order_detail sod ");
	 * sql.append("       WHERE so.sale_order_id = sod.sale_order_id ");
	 * sql.append("         AND so.from_sale_order_id IS NOT NULL ");
	 * sql.append("         AND so.shop_id = ? "); sql.append(
	 * "         AND so.order_date >= trunc(sysdate, 'MONTH') AND so.order_date < (trunc(sysdate) + 1) "
	 * ); sql.append(
	 * "       GROUP BY sod.product_id) sale_order_detail_tmp_1 ON sale_order_detail_tmp_1.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(shopId);
	 * 
	 * // Tong nhap tra Vansales + tong nhap dieu chinh
	 * sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT std.product_id product_id, ");
	 * sql.append("              sum(std.quantity) std_quantity ");
	 * sql.append("       FROM stock_trans st, ");
	 * sql.append("            stock_trans_detail std ");
	 * sql.append("       WHERE st.stock_trans_id = std.stock_trans_id ");
	 * sql.append("         AND ((st.from_owner_type IS NULL ");
	 * sql.append("               AND st.to_owner_type = ?) ");
	 * sql.append("              OR (st.from_owner_type = ? ");
	 * sql.append("                  AND st.to_owner_type = ?)) ");
	 * sql.append("         AND st.shop_id = ? ");
	 * sql.append("         AND st.to_owner_type = ? AND st.to_owner_id = ?");
	 * sql.append(
	 * "         AND st.stock_trans_date >= trunc(sysdate, 'MONTH') AND st.stock_trans_date < (trunc(sysdate) + 1) "
	 * ); sql.append(
	 * "       GROUP BY std. product_id) stock_trans_detail_tmp_1 ON stock_trans_detail_tmp_1.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(StockObjectType.SHOP.getValue());
	 * params.add(StockObjectType.STAFF.getValue());
	 * params.add(StockObjectType.SHOP.getValue()); params.add(shopId);
	 * params.add(StockObjectType.SHOP.getValue()); params.add(shopId);
	 * 
	 * // Xuat ban cho khach hang sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT sod.product_id product_id, ");
	 * sql.append("              sum(sod.quantity) sod_quantity ");
	 * sql.append("       FROM sale_order so, ");
	 * sql.append("            sale_order_detail sod ");
	 * sql.append("       WHERE so.sale_order_id = sod.sale_order_id ");
	 * sql.append("         AND so.shop_id = ? ");
	 * sql.append("         AND so.order_type = ? ");
	 * sql.append("         AND so.approved = ? ");
	 * sql.append("         AND so.from_sale_order_id is NULL "); sql.append(
	 * "         AND so.order_date >= trunc(sysdate, 'MONTH') AND so.order_date < trunc(sysdate) + 1 "
	 * ); sql.append(
	 * "       GROUP BY sod.product_id) sale_order_detail_tmp_2 ON sale_order_detail_tmp_2.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(shopId); params.add(OrderType.IN.getValue());
	 * params.add(SaleOrderStatus.APPROVED.getValue());
	 * 
	 * // Xuat tra VNM sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT pvd.product_id product_id, ");
	 * sql.append("              sum(pvd.quantity) pvd_quantity ");
	 * sql.append("       FROM po_vnm pv, ");
	 * sql.append("            po_vnm_detail pvd "); sql.append(
	 * "       WHERE pv.po_vnm_id = pvd.po_vnm_id and pv.object_type = 1 ");
	 * sql.append("         AND pv.type = ? ");
	 * sql.append("         AND pv.status = ? ");
	 * sql.append("         AND pv.shop_id = ? "); sql.append(
	 * "         AND pv.po_vnm_date >= trunc(sysdate, 'MONTH') AND pv.po_vnm_date < (trunc(sysdate) + 1) "
	 * ); sql.append(
	 * "       GROUP BY pvd.product_id) po_vnm_detail_tmp_2 ON po_vnm_detail_tmp_2.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(PoType.RETURNED_SALES_ORDER.getValue());
	 * params.add(PoVNMStatus.IMPORTED.getValue()); params.add(shopId);
	 * 
	 * // Xuat tra vansales sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT std.product_id product_id, ");
	 * sql.append("              sum(std.quantity) std_quantity ");
	 * sql.append("       FROM stock_trans st, ");
	 * sql.append("            stock_trans_detail std ");
	 * sql.append("       WHERE st.stock_trans_id = std.stock_trans_id ");
	 * sql.append("         AND ((st.from_owner_type = ? ");
	 * sql.append("               AND st.to_owner_type is NULL) ");
	 * sql.append("              OR (st.from_owner_type = ? ");
	 * sql.append("                  AND st.to_owner_type = ?)) ");
	 * sql.append("         AND st.shop_id = ? ");
	 * sql.append("         AND st.from_owner_id = ? and st.to_owner_type = ? "
	 * ); sql.append(
	 * "         AND st.stock_trans_date >= trunc(sysdate, 'MONTH') AND st.stock_trans_date < (trunc(sysdate) + 1) "
	 * ); sql.append(
	 * "       GROUP BY std.product_id) std_exp_van ON std_exp_van.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(StockObjectType.SHOP.getValue());
	 * params.add(StockObjectType.SHOP.getValue());
	 * params.add(StockObjectType.STAFF.getValue()); params.add(shopId);
	 * params.add(shopId); params.add(StockObjectType.STAFF.getValue());
	 * 
	 * // Xuat dieu chinh sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT std.product_id product_id, ");
	 * sql.append("              sum(std.quantity) std_quantity ");
	 * sql.append("       FROM stock_trans st, ");
	 * sql.append("            stock_trans_detail std ");
	 * sql.append("       WHERE st.stock_trans_id = std.stock_trans_id ");
	 * sql.append("         AND ((st.from_owner_type = ? ");
	 * sql.append("               AND st.to_owner_type is NULL) ");
	 * sql.append("              OR (st.from_owner_type = ? ");
	 * sql.append("                  AND st.to_owner_type = ?)) ");
	 * sql.append("         AND st.shop_id = ? ");
	 * sql.append("         AND st.from_owner_id = ? and st.to_owner_type is NULL "
	 * ); sql.append(
	 * "         AND st.stock_trans_date >= trunc(sysdate, 'MONTH') AND st.stock_trans_date < (trunc(sysdate) + 1) "
	 * ); sql.append(
	 * "       GROUP BY std.product_id) std_exp_edit ON std_exp_edit.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(StockObjectType.SHOP.getValue());
	 * params.add(StockObjectType.SHOP.getValue());
	 * params.add(StockObjectType.STAFF.getValue()); params.add(shopId);
	 * params.add(shopId);
	 * 
	 * // Tong xuat ban/ khuyen mai/ huy, tra, doi KH vansale
	 * sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT sod.product_id product_id, ");
	 * sql.append("              sum(sod.quantity) sod_quantity ");
	 * sql.append("       FROM sale_order so, ");
	 * sql.append("            sale_order_detail sod ");
	 * sql.append("       WHERE so.sale_order_id = sod.sale_order_id ");
	 * sql.append("         AND so.from_sale_order_id is NULL ");
	 * sql.append("         AND so.shop_id = ? ");
	 * sql.append("         AND so.order_type = ? ");
	 * sql.append("         AND so.approved = ? "); sql.append(
	 * "         AND so.order_date >= trunc(sysdate, 'MONTH') AND so.order_date < (trunc(sysdate) + 1) "
	 * ); sql.append(
	 * "       GROUP BY sod.product_id) sale_order_detail_tmp_3 ON sale_order_detail_tmp_3.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(shopId); params.add(OrderType.SO.getValue());
	 * params.add(SaleOrderStatus.APPROVED.getValue());
	 * 
	 * // Ton tai thoi diem hien tai sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT st.product_id, sum(st.quantity) st_quantity ");
	 * sql.append("       FROM stock_total st ");
	 * sql.append("       WHERE st.object_type = ? ");
	 * sql.append("         AND st.object_id = ? "); sql.append(
	 * "       GROUP BY st.product_id) stock_total_tmp ON stock_total_tmp.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(StockObjectType.SHOP.getValue()); params.add(shopId);
	 * 
	 * // Ke hoach tieu thu thang sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT sl.product_id product_id, ");
	 * sql.append("              sum(sl.quantity) sl_quantity ");
	 * sql.append("       FROM sale_plan sl ");
	 * sql.append("       WHERE sl.object_type = ? ");
	 * sql.append("         AND sl.object_id = ? ");
	 * sql.append("         AND sl.month = to_number(to_char(sysdate, 'MM')) ");
	 * sql
	 * .append("         AND sl.year = to_number(to_char(sysdate, 'YYYY')) ");
	 * sql.append(
	 * "       GROUP BY sl.product_id) sale_plan_tmp ON sale_plan_tmp.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(SalePlanOwnerType.SHOP.getValue()); params.add(shopId);
	 * 
	 * // So luong PO confirm sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT pvd.product_id product_id, ");
	 * sql.append("              sum(pvd.quantity) pvd_quantity ");
	 * sql.append("       FROM po_vnm pv, ");
	 * sql.append("            po_vnm_detail pvd "); sql.append(
	 * "       WHERE pv.po_vnm_id = pvd.po_vnm_id and pv.object_type = 1 ");
	 * sql.append("         AND pv.type = ? ");
	 * sql.append("         AND pv.status = ? ");
	 * sql.append("         AND pv.shop_id = ? "); sql.append(
	 * "       GROUP BY pvd.product_id) po_vnm_detail_tmp_3 ON po_vnm_detail_tmp_3.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(PoType.PO_CONFIRM.getValue());
	 * params.add(PoVNMStatus.NOT_IMPORT.getValue()); params.add(shopId);
	 * 
	 * // Tinh so luong PO DVKH sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT pvd.product_id product_id, "); sql.append(
	 * "              sum(pvd.quantity) pvd_quantity, sum(pvd.quantity_received) pvd_quantity_received "
	 * ); sql.append("       FROM po_vnm pv, ");
	 * sql.append("            po_vnm_detail pvd "); sql.append(
	 * "       WHERE pv.po_vnm_id = pvd.po_vnm_id and pv.object_type = 1 ");
	 * sql.append("         AND pv.type = ? ");
	 * sql.append("         AND pv.status IN (?, ?) ");
	 * sql.append("         AND pv.shop_id = ? "); sql.append(
	 * "       GROUP BY pvd.product_id) pvd_tmp_3 ON pvd_tmp_3.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(PoType.PO_CUSTOMER_SERVICE.getValue());
	 * params.add(PoVNMStatus.NOT_IMPORT.getValue());
	 * params.add(PoVNMStatus.IMPORTING.getValue()); params.add(shopId);
	 * 
	 * // Tinh ton kho an toan min, lead, next Tinh theo Product
	 * sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT sp.product_id, ");
	 * sql.append("              sp.minsf safety_stock_min, ");
	 * sql.append("              sp.lead next, "); sql.append(
	 * "              (CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 1, 7) + 1)) > 0) THEN 1 ELSE CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 2, 7) + 1)) > 0) THEN 2 ELSE CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 3, 7) + 1)) > 0) THEN 3 ELSE CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 4, 7) + 1)) > 0) THEN 4 ELSE CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 5, 7) + 1)) > 0) THEN 5 ELSE CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 6, 7) + 1)) > 0) THEN 6 ELSE CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 7, 7) + 1)) > 0) THEN 7 ELSE 0 END END END END END END END) lead, "
	 * ); sql.append("              sp.percentage percentage ");
	 * sql.append("       FROM shop_product sp ");
	 * sql.append("       WHERE sp.shop_id = ? ");
	 * sql.append("         AND sp.status = ? ");
	 * sql.append("         AND sp.type = ? "); //
	 * sql.append("         AND sp.year = to_number(to_char(sysdate, 'YYYY'))");
	 * sql.append("       ) sp_tmp ON sp_tmp.product_id = pd.product_id ");
	 * 
	 * params.add(shopId); params.add(ActiveType.RUNNING.getValue());
	 * params.add(ShopProductType.PRODUCT.getValue());
	 * 
	 * // Tinh ton kho an toan min, lead, next Tinh theo Cat
	 * sql.append("    LEFT JOIN "); sql.append("      (SELECT sp.cat_id, ");
	 * sql.append("              sp.minsf safety_stock_min, ");
	 * sql.append("      	      sp.lead lead,  "); sql.append(
	 * "      	      (CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 1, 7) + 1)) > 0) THEN 1 ELSE CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 2, 7) + 1)) > 0) THEN 2 ELSE CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 3, 7) + 1)) > 0) THEN 3 ELSE CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 4, 7) + 1)) > 0) THEN 4 ELSE CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 5, 7) + 1)) > 0) THEN 5 ELSE CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 6, 7) + 1)) > 0) THEN 6 ELSE CASE WHEN (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'D')) - 1 + 7, 7) + 1)) > 0) THEN 7 ELSE 0 END END END END END END END) next, "
	 * ); sql.append("      		  sp.percentage percentage ");
	 * sql.append("      FROM shop_product sp ");
	 * sql.append("      WHERE sp.shop_id = ? ");
	 * sql.append("        AND sp.status = ? ");
	 * sql.append("        AND sp.type = ? "); //
	 * sql.append("        AND sp.year = to_number(to_char(sysdate, 'YYYY'))");
	 * sql.append("      ) sp_tmp_cat on sp_tmp_cat.cat_id = pd.cat_id  ");
	 * 
	 * params.add(shopId); params.add(ActiveType.RUNNING.getValue());
	 * params.add(ShopProductType.CAT.getValue());
	 * 
	 * // Nhap Po, xuat Po, ton Po, luy ke tieu thu thang po, slt po
	 * sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT poAutoD.product_id, ");
	 * sql.append("              sum(poAutoD.IMPORT) AS poImport, ");
	 * sql.append("              sum(poAutoD.EXPORT) AS poExport, ");
	 * sql.append("              sum(poAutoD.STOCK) AS poStock, ");
	 * sql.append("              sum(poAutoD.MONTH_CUMULATE) AS poMonthCumulate, "
	 * ); sql.append("              sum(poAutoD.QUANTITY) AS poQuantity, ");
	 * sql.append("              poAutoD.convfact poConvfact ");
	 * sql.append("       FROM PO_AUTO poAuto, ");
	 * sql.append("            PO_AUTO_DETAIL poAutoD ");
	 * sql.append("       WHERE poAuto.po_auto_id = poAutoD.po_auto_id ");
	 * sql.append("         AND poAuto.shop_id = ? ");
	 * sql.append("         AND poAuto.po_auto_id = ? ");
	 * sql.append("       GROUP BY poAutoD.product_id, "); sql.append(
	 * "                poAutoD.convfact) poAuto ON poAuto.product_id = pd.product_id "
	 * );
	 * 
	 * params.add(shopId); params.add(poId);
	 * 
	 * // Ton dau ky sql.append("    LEFT JOIN ");
	 * sql.append("      (SELECT rpt_std.product_id product_id, ");
	 * sql.append("              sum(rpt_std.close_stock_total) close_stock_total "
	 * ); sql.append("       FROM rpt_stock_total_day rpt_std ");
	 * sql.append("       WHERE rpt_std.object_type = ? ");
	 * sql.append("         AND rpt_std.object_id = ? "); sql.append(
	 * "         AND rpt_std.rpt_in_day >= (trunc(sysdate, 'MONTH') - 1) AND rpt_std.rpt_in_day < trunc(sysdate, 'MONTH') "
	 * ); sql.append(
	 * "       GROUP BY rpt_std.product_id) rpt_stock_total_day_tmp ON rpt_stock_total_day_tmp.product_id = pd.product_id, "
	 * ); sql.append(" 		product_info pif, ");
	 * sql.append(" 		product_level pl, "); sql.append(" 		price pr ");
	 * sql.append("    WHERE pd.status = ? ");
	 * sql.append("      AND pd.product_level_id = pl.product_level_id ");
	 * sql.append("      AND pl.cat_id = pif.product_info_id ");
	 * sql.append("      AND pif.type = ? ");
	 * sql.append("      AND pd.product_id = pr.product_id ");
	 * sql.append("      AND pr.status = ? ");
	 * sql.append("      AND pr.from_date < (trunc(sysdate) + 1) ");
	 * sql.append("      AND (pr.to_date IS NULL ");
	 * sql.append("           OR pr.to_date >= trunc(sysdate)) ");
	 * sql.append("    ORDER BY pd.product_code) po ");
	 * sql.append(" ORDER BY po.productInfoCode, po.productCode ");
	 * 
	 * params.add(StockObjectType.SHOP.getValue()); params.add(shopId);
	 * params.add(ActiveType.RUNNING.getValue());
	 * params.add(ProductType.CAT.getValue());
	 * params.add(ActiveType.RUNNING.getValue());
	 * 
	 * final String[] fieldNames = new String[] { // "productInfoCode",// 1
	 * "productCode",// 2 "productName",// 3 "openStockTotal",// 4
	 * "importQty",// 5 "exportQty",// 6 "stockQty",// 7 "monthCumulate",// 8
	 * "monthPlan",// 9 "dayPlan",// 10 "dayReservePlan",// 11
	 * "dayReserveReal",// 12 "min",// 13 "lead",// 14 "next",// 15
	 * "requimentStock",// 16 "poConfirmQty",// 17 "stockPoDvkh",// 18
	 * "convfact",// 19 "percentage",// 20 "price",// 21 "poImport",// 22
	 * "poExport",// 23 "poStock",// 24 "poMonthCumulate",// 25 "poQuantity",//
	 * 26 "poConvfact",// 27 "realSaleDateInMonth"// 28 };
	 * 
	 * final Type[] fieldTypes = new Type[] { // StandardBasicTypes.STRING,// 1
	 * StandardBasicTypes.STRING,// 2 StandardBasicTypes.STRING,// 3
	 * StandardBasicTypes.LONG,// 4 StandardBasicTypes.LONG,// 5
	 * StandardBasicTypes.LONG,// 6 StandardBasicTypes.LONG,// 7
	 * StandardBasicTypes.LONG,// 8 StandardBasicTypes.LONG,// 9
	 * StandardBasicTypes.LONG,// 10 StandardBasicTypes.DOUBLE,// 11
	 * StandardBasicTypes.DOUBLE,// 12 StandardBasicTypes.LONG,// 13
	 * StandardBasicTypes.LONG,// 14 StandardBasicTypes.LONG,// 15
	 * StandardBasicTypes.LONG,// 16 StandardBasicTypes.LONG,// 17
	 * StandardBasicTypes.LONG,// 18 StandardBasicTypes.LONG,// 19
	 * StandardBasicTypes.DOUBLE,// 20 StandardBasicTypes.BIG_DECIMAL,// 21
	 * StandardBasicTypes.LONG,// 22 StandardBasicTypes.LONG,// 23
	 * StandardBasicTypes.LONG,// 24 StandardBasicTypes.LONG,// 25
	 * StandardBasicTypes.LONG,// 26 StandardBasicTypes.LONG,// 27
	 * StandardBasicTypes.INTEGER// 28 };
	 * 
	 * return repo.getListByQueryAndScalar(RptCompareImExStF1DataVO.class,
	 * fieldNames, fieldTypes, sql.toString(), params); }
	 */

	/*
	 * @Override public List<Product> getListDisplayProductTreeVO(String
	 * productCode, String productName, Long exDpId, Integer limit) throws
	 * DataAccessException { StringBuilder sql = new StringBuilder();
	 * List<Object> params = new ArrayList<Object>();
	 * sql.append(" SELECT * FROM ("); sql.append(" SELECT p.*");
	 * sql.append(" FROM product p"); sql.append(" JOIN product_level pl");
	 * sql.append(" ON (p.product_level_id = pl.product_level_id)");
	 * sql.append(" JOIN product_info pic");
	 * sql.append(" ON (pl.cat_id = pic.product_info_id)");
	 * sql.append(" JOIN product_info pisc");
	 * sql.append(" ON (pl.sub_cat_id   = pisc.product_info_id)");
	 * sql.append(" WHERE p.status = ?");
	 * params.add(ActiveType.RUNNING.getValue()); if
	 * (!StringUtility.isNullOrEmpty(productCode)) {
	 * sql.append(" AND UPPER(p.product_code) LIKE ? ESCAPE '/' ");
	 * params.add(StringUtility
	 * .toOracleSearchLikeSuffix(productCode).toUpperCase()); } if
	 * (!StringUtility.isNullOrEmpty(productName)) {
	 * sql.append(" AND upper(p.product_name) LIKE ? ESCAPE '/' ");
	 * params.add(StringUtility.toOracleSearchLike(productName.toUpperCase()));
	 * } if (exDpId != null) { sql.append(
	 * " AND p.product_id NOT IN (SELECT product_id FROM display_program_detail WHERE display_program_id = ?)"
	 * ); params.add(exDpId); } sql.append(" ORDER BY pic.product_info_code,");
	 * sql.append("   pic.product_info_id,");
	 * sql.append("   pisc.product_info_code,");
	 * sql.append("   pisc.product_info_id,"); sql.append("   p.product_code");
	 * sql.append(" )"); if (limit != null) { sql.append(" WHERE rownum <= ?");
	 * params.add(limit); }
	 * 
	 * return repo.getListBySQL(Product.class, sql.toString(), params); }
	 */

	@Override
	public List<Product> getListFocusProductTreeVO(String productCode, String productName, Long exFpId, Integer limit) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM (");
		sql.append(" SELECT p.*");
		sql.append(" FROM product p");
		//		sql.append(" JOIN product_level pl");
		//		sql.append(" ON (p.product_level_id = pl.product_level_id)");
		sql.append(" JOIN product_info pic");
		sql.append(" ON (p.cat_id = pic.product_info_id)");
		sql.append(" JOIN product_info pisc");
		sql.append(" ON (p.sub_cat_id   = pisc.product_info_id)");
		sql.append(" WHERE p.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND pic.status != ?");
		params.add(ActiveType.DELETED.getValue());
		sql.append(" AND pisc.status != ?");
		params.add(ActiveType.DELETED.getValue());
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" AND p.product_code LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" AND lower(p.name_text) LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(productName).trim().toLowerCase()));
		}
		if (exFpId != null) {
			sql.append(" AND p.product_id NOT IN");
			sql.append(" (");
			sql.append(" SELECT fpmp.product_id");
			sql.append(" FROM focus_channel_map fpm");
			sql.append(" JOIN focus_channel_map_product fpmp");
			sql.append(" ON (fpm.focus_channel_map_id = fpmp.focus_channel_map_id)");
			sql.append(" WHERE fpm.focus_program_id   = ?");
			params.add(exFpId);
			sql.append(" )");
		}
		sql.append(" ORDER BY pic.product_info_code,");
		sql.append("   pic.product_info_id,");
		sql.append("   pisc.product_info_code,");
		sql.append("   pisc.product_info_id,");
		sql.append("   p.product_code");
		sql.append(" )");
		if (limit != null) {
			sql.append(" WHERE rownum <= ?");
			params.add(limit);
		}

		return repo.getListBySQL(Product.class, sql.toString(), params);
	}

	@Override
	public List<RptF1DataVO> getDataForF1Report(Long shopId, Date fromDate, Date toDate, int workDateInMonth, int realWorkDateInMonth) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptF1DataVO>();

		if (shopId == null) {
			throw new DataAccessException("shopId is null");
		} else if (workDateInMonth <= 0) {
			throw new DataAccessException("workDateInMonth <= 0");
		} else if (realWorkDateInMonth <= 0) {
			throw new DataAccessException("realWorkDateInMonth <= 0");
		} else if (null == fromDate) {
			throw new DataAccessException("fromDate is null");
		} else if (null == toDate) {
			throw new DataAccessException("toDate is null");
		}

		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();

		if (fromDate.compareTo(toDate) < 0) {
			sql.append(" select pro.product_code as productCode, pro.product_name as productName, pro.convfact, pro.product_info_name as productInfoCode, pro.price, ");
			sql.append("             nvl(stk_b.close_stock_total, 0) as openStockTotal, ");
			sql.append("             nvl(tran.import_total, 0) importQty, ");
			sql.append("             nvl(tran.export_total, 0) exportQty, ");
			sql.append("             nvl(stk_e.stockQty, 0) as closeStockTotal, ");
			sql.append("             nvl(export_pre, 0) + nvl(export_van, 0) as monthCumulate, ");
			sql.append("             nvl(sal_p.quantity, 0) as monthPlan, ");
			sql.append("             nvl(sp_tmp.min, nvl(sp_tmp_cat.min, 0)) min, ");
			sql.append("             nvl(sp_tmp.lead, nvl(sp_tmp_cat.lead, 0)) lead, ");
			sql.append("             nvl(sp_tmp.next, nvl(sp_tmp_cat.next, 0)) next, ");
			sql.append("             nvl(sp_tmp.percentage, nvl(sp_tmp_cat.percentage, 0)) percentage, ");
			sql.append("             nvl(po_cf.quantity, 0) as poCfQty, ");
			sql.append("             nvl(po_cs.quantity, 0) as poCsQty, ");
			sql.append("             ? as saleDateInMonth, ");
			sql.append("             ? as realSaleDateInMonth ");

			params.add(workDateInMonth);
			params.add(realWorkDateInMonth);

			// san pham
			sql.append("     from (select pro_.product_id as product_id, pro_.product_code, pro_.product_name as product_name, pro_.convfact as convfact, pro_.cat_id, ");
			sql.append("                 pro_i.product_info_name as product_info_name, ");
			sql.append("                 pri.price as price ");
			sql.append("         from product pro_, product_level pro_l, product_info pro_i, price pri ");
			sql.append("         where pro_.product_level_id = pro_l.product_level_id ");
			sql.append("             and pro_l.cat_id = pro_i.product_info_id ");
			sql.append("             and pro_i.type = ? ");
			sql.append("             and pro_.product_id = pri.product_id ");
			sql.append("             and pri.status = ? ");
			sql.append("             and pro_i.product_info_code in ('A', 'B', 'C', 'D', 'E') ");
			sql.append("             and pri.from_date <= trunc(?) ");
			sql.append("             and (pri.to_date >= trunc(?) or pri.to_date is null) ");
			sql.append("     ) pro  ");

			params.add(ProductType.CAT.getValue());
			params.add(ActiveType.RUNNING.getValue());
			params.add(fromDate);
			params.add(toDate);

			// ton dau ky
			sql.append("     left join(select product_id as product_id, close_stock_total as close_stock_total  ");
			sql.append("         from rpt_stock_total_day ");
			sql.append("         where object_type = ? ");
			sql.append("             and object_id = ? ");
			sql.append("             and rpt_in_day >= (trunc(?) - 1) and rpt_in_day < trunc(?) ");
			sql.append("     ) stk_b on pro.product_id = stk_b.product_id  ");

			params.add(StockObjectType.SHOP.getValue());
			params.add(shopId);
			params.add(fromDate);
			params.add(fromDate);

			// xuat nhap
			sql.append("     left join(select product_id, sum(import_total) import_total, sum(export_total) export_total, ");
			sql.append("             sum(sale_total_pre) + sum(sale_promotion_d_pre) + sum(sale_promotion_pre) export_pre, ");
			sql.append("             sum(sale_total_van) + sum(sale_promotion_d_van) + sum(sale_promotion_van) export_van ");
			sql.append("         from rpt_stock_total_day ");
			sql.append("         where rpt_in_day >= trunc(?) and rpt_in_day < (trunc(?) + 1) ");
			sql.append("             and object_type = ? ");
			sql.append(" 			and object_id = ? ");
			sql.append("         group by product_id) tran on tran.product_id = pro.product_id ");

			params.add(fromDate);
			params.add(toDate);
			params.add(StockObjectType.SHOP.getValue());
			params.add(shopId);

			// ton cuoi ky
			sql.append("     left join(select product_id, close_stock_total as stockQty ");
			sql.append("         from rpt_stock_total_day ");
			sql.append("         where rpt_in_day >= trunc(?) and rpt_in_day < (trunc(?) + 1) ");
			sql.append("             and object_id = ? ");
			sql.append("             and object_type = ? ");
			sql.append("     )stk_e on pro.product_id = stk_e.product_id  ");

			params.add(fromDate);
			params.add(toDate);
			params.add(shopId);
			params.add(StockObjectType.SHOP.getValue());

			// ke hoach tieu thu thang
			sql.append("     left join(select product_id, quantity ");
			sql.append("         from sale_plan ");
			sql.append("         where object_type = ? ");
			sql.append("             and object_id = ?  ");
			sql.append("             and month = extract( month from ?) ");
			sql.append("             and year = extract( year from ?) ");
			sql.append("     )sal_p on pro.product_id = sal_p.product_id  ");

			params.add(SalePlanOwnerType.SHOP.getValue());
			params.add(shopId);
			params.add(toDate);
			params.add(toDate);

			sql.append("     left join(select sp.product_id, ");
			sql.append("             sp.minsf min, ");
			sql.append("             sp.lead next, ");
			sql.append("             (case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 1, 7) + 1)) > 0) then 1 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 2, 7) + 1)) > 0) then 2 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 3, 7) + 1)) > 0) then 3 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 4, 7) + 1)) > 0) then 4 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 5, 7) + 1)) > 0) then 5 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 6, 7) + 1)) > 0) then 6 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 7, 7) + 1)) > 0) then 7 else 0 end end end end end end end) lead, ");
			sql.append("             sp.percentage percentage  ");
			sql.append("         from shop_product sp  ");
			sql.append("         where sp.status = ?  ");
			sql.append("             and sp.shop_id = ? ");
			sql.append("             and sp.type = ? ");
			sql.append("     ) sp_tmp on sp_tmp.product_id = pro.product_id ");

			params.add(ActiveType.RUNNING.getValue());
			params.add(shopId);
			params.add(ShopProductType.PRODUCT.getValue());

			sql.append("     left join(select sp.cat_id, ");
			sql.append("             sp.minsf min, ");
			sql.append("             sp.lead lead, ");
			sql.append("             (case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 1, 7) + 1)) > 0) then 1 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 2, 7) + 1)) > 0) then 2 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 3, 7) + 1)) > 0) then 3 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 4, 7) + 1)) > 0) then 4 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 5, 7) + 1)) > 0) then 5 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 6, 7) + 1)) > 0) then 6 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 7, 7) + 1)) > 0) then 7 else 0 end end end end end end end) next, ");
			sql.append("             sp.percentage percentage ");
			sql.append("     from shop_product sp ");
			sql.append("     where sp.status = ? ");
			sql.append("         and sp.shop_id = ? ");
			sql.append("         and sp.type = ? ");
			sql.append("     ) sp_tmp_cat on sp_tmp_cat.cat_id = pro.cat_id ");

			params.add(ActiveType.RUNNING.getValue());
			params.add(shopId);
			params.add(ShopProductType.CAT.getValue());

			// so luong po confirm
			sql.append("     left join(select pvd.product_id product_id, sum(pvd.quantity) quantity ");
			sql.append("        from po_vnm pv, po_vnm_detail pvd ");
			sql.append("        where pv.po_vnm_id = pvd.po_vnm_id ");
			sql.append("          and pv.type = ? ");
			sql.append("          and pv.status = ? ");
			sql.append("          and pv.shop_id = ? ");
			sql.append("        group by pvd.product_id) po_cf on po_cf.product_id = pro.product_id  ");

			params.add(PoType.PO_CONFIRM.getValue());
			params.add(PoVNMStatus.NOT_IMPORT.getValue());
			params.add(shopId);

			// tinh so luong po dvkh
			sql.append("     left join(select pvd.product_id product_id, sum(pvd.quantity) - sum(pvd.quantity_received) quantity ");
			sql.append("        from po_vnm pv, po_vnm_detail pvd ");
			sql.append("        where pv.po_vnm_id = pvd.po_vnm_id ");
			sql.append("          and pv.type = ? ");
			sql.append("          and pv.status in (?, ?) ");
			sql.append("          and pv.shop_id = ? ");
			sql.append("        group by pvd.product_id) po_cs on po_cs.product_id = pro.product_id  ");
			sql.append(" order by NLSSORT(pro.product_info_name,'NLS_SORT=vietnamese'), NLSSORT(pro.product_name,'NLS_SORT=vietnamese') ");

			params.add(PoType.PO_CUSTOMER_SERVICE.getValue());
			params.add(PoVNMStatus.NOT_IMPORT.getValue());
			params.add(PoVNMStatus.IMPORTED.getValue());
			params.add(shopId);
		} else {
			sql.append(" select pro.product_code, pro.product_name, pro.convfact, pro.product_info_name, pro.price, ");
			sql.append("             nvl(stk_b.close_stock_total, 0) as openStockTotal, ");
			sql.append("             nvl(tran.import_total + im_vnm.quantity + im_e.quantity + im_r_p.quantity + im_v.quantity, 0) importQty, ");
			sql.append("             nvl(tran.export_total + ex_p.quantity + ex_v.quantity + ex_vnm.quantity + ex_e.quantity, 0) exportQty, ");
			sql.append("             nvl(stk_e.quantity, 0) as closeStockTotal, ");
			sql.append("             nvl(tran.export_pre + tran.export_van + ex_p.quantity + ex_e_v.quantity, 0) as monthCumulate, ");
			sql.append("             nvl(sal_p.quantity, 0) as monthPlan, ");
			sql.append("             nvl(sp_tmp.min, nvl(sp_tmp_cat.min, 0)) min, ");
			sql.append("             nvl(sp_tmp.lead, nvl(sp_tmp_cat.lead, 0)) lead, ");
			sql.append("             nvl(sp_tmp.next, nvl(sp_tmp_cat.next, 0)) next, ");
			sql.append("             nvl(sp_tmp.percentage, nvl(sp_tmp_cat.percentage, 0)) percentage, ");
			sql.append("             nvl(po_cf.quantity, 0) as poCfQty, ");
			sql.append("             nvl(po_cs.quantity, 0) as poCsQty, ");
			sql.append("             ? as saleDateInMonth, ");
			sql.append("             ? as realSaleDateInMonth ");

			params.add(workDateInMonth);
			params.add(realWorkDateInMonth);

			// san pham
			sql.append("     from (select pro_.product_id as product_id, pro_.product_code as product_code, pro_.product_name as product_name, pro_.convfact as convfact, pro_.cat_id, ");
			sql.append("                 pro_i.product_info_name as product_info_name, ");
			sql.append("                 pri.price as price ");
			sql.append("         from product pro_, product_level pro_l, product_info pro_i, price pri ");
			sql.append("         where pro_.product_level_id = pro_l.product_level_id ");
			sql.append("             and pro_l.cat_id = pro_i.product_info_id ");
			sql.append("             and pro_i.type = ? ");
			sql.append("             and pro_.product_id = pri.product_id ");
			sql.append("             and pri.status = ? ");
			sql.append("             and pri.from_date <= trunc(?) ");
			sql.append("             and (pri.to_date >= trunc(?) or pri.to_date is null)) pro ");

			params.add(ProductType.CAT.getValue());
			params.add(ActiveType.RUNNING.getValue());
			params.add(fromDate);
			params.add(toDate);

			// ton dau ky
			sql.append("     left join(select close_stock_total as close_stock_total, product_id as product_id ");
			sql.append("         from rpt_stock_total_day ");
			sql.append("         where object_type = ? ");
			sql.append("             and object_id = ? ");
			sql.append("             and rpt_in_day >= trunc(? -1) and rpt_in_day < trunc(?)) stk_b on pro.product_id = stk_b.product_id ");

			params.add(StockObjectType.SHOP.getValue());
			params.add(shopId);
			params.add(fromDate);
			params.add(fromDate);

			// nhap xuat
			sql.append("     left join(select product_id, sum(import_total) as import_total, sum(export_total) as export_total, ");
			sql.append("             sum(sale_total_pre) + sum(sale_promotion_d_pre) + sum(sale_promotion_pre) export_pre, ");
			sql.append("             sum(sale_total_van) + sum(sale_promotion_d_van) + sum(sale_promotion_van) export_van ");
			sql.append("         from rpt_stock_total_day ");
			sql.append("         where object_type = ? ");
			sql.append("             and object_id = ? ");
			sql.append("             and rpt_in_day >= trunc(?) and rpt_to_date < trunc(? + 1) ");
			sql.append("         group by product_id) tran on tran.product_id = pro.product_id ");

			params.add(StockObjectType.SHOP.getValue());
			params.add(shopId);
			params.add(fromDate);
			params.add(toDate);

			// nhap vnm ngay hien tai
			sql.append("     left join(select po_d.product_id, sum(po_d.quantity) as quantity ");
			sql.append("         from po_vnm po, po_vnm_detail po_d ");
			sql.append("         where po.po_vnm_id = po_d.po_vnm_id ");
			sql.append("             and po.shop_id = ? ");
			sql.append("             and po.type = ? and po.status = ? ");
			sql.append("             and po.import_date >= trunc(sysdate) and po.import_date < (trunc(sysdate) + 1) ");
			sql.append("         group by po_d.product_id) im_vnm on im_vnm.product_id = pro.product_id ");

			params.add(shopId);
			params.add(PoType.PO_CONFIRM.getValue());
			params.add(PoVNMStatus.IMPORTED.getValue());

			// tong nhap dieu chinh ngay hien tai
			sql.append("     left join(select sto_d.product_id, sum(sto_d.quantity) as quantity ");
			sql.append("         from stock_trans sto, stock_trans_detail sto_d ");
			sql.append("         where sto.stock_trans_id = sto_d.stock_trans_id ");
			sql.append("             and sto.shop_id = ? ");
			sql.append("             and sto.from_owner_type is null ");
			sql.append("             and sto.to_owner_type = ? and sto.to_owner_id = ?");
			sql.append("             and sto.stock_trans_date >= trunc(sysdate) and sto.stock_trans_date <(trunc(sysdate) + 1) ");
			sql.append("         group by sto_d.product_id) im_e on im_e.product_id = pro.product_id ");

			params.add(shopId);
			params.add(StockObjectType.SHOP.getValue());
			params.add(shopId);

			// tong nhap tra hang pre ngay hien tai
			sql.append("     left join(select so_d.product_id, sum(so_d.quantity) as quantity ");
			sql.append("         from sale_order so, sale_order_detail so_d ");
			sql.append("         where so.sale_order_id = so_d.sale_order_id ");
			sql.append("             and so.shop_id = ? ");
			sql.append("             and so.from_sale_order_id is not null ");
			sql.append("             and so.order_date >= trunc(sysdate) and so.order_date < (trunc(sysdate) + 1) ");
			sql.append("         group by so_d.product_id) im_r_p on im_r_p.product_id = pro.product_id ");

			params.add(shopId);

			// tong nhap vansale ngay hien tai
			sql.append("     left join(select sto_d.product_id, sum(sto_d.quantity) as quantity ");
			sql.append("         from stock_trans sto, stock_trans_detail sto_d ");
			sql.append("         where sto.stock_trans_id = sto_d.stock_trans_id     ");
			sql.append("             and sto.shop_id = ? ");
			sql.append("             and sto.from_owner_type = ? ");
			sql.append("             and sto.to_owner_type = ? and sto.to_owner_id = ?");
			sql.append("             and sto.stock_trans_date >= trunc(sysdate) and sto.stock_trans_date <(trunc(sysdate) + 1) ");
			sql.append("         group by sto_d.product_id) im_v on im_v.product_id = pro.product_id ");

			params.add(shopId);
			params.add(StockObjectType.STAFF.getValue());
			params.add(StockObjectType.SHOP.getValue());
			params.add(shopId);

			// tong xuat ban/khuyen mai/huy, tra doi kh presale ngay hien tai
			sql.append("     left join(select so_d.product_id, sum(so_d.quantity) as quantity ");
			sql.append("         from sale_order so, sale_order_detail so_d ");
			sql.append("         where so.sale_order_id = so_d.sale_order_id ");
			sql.append("             and so.shop_id = ? ");
			sql.append("             and so.order_type = ? ");
			sql.append("             and so.approved = ? ");
			sql.append("             and so.from_sale_order_id is null ");
			sql.append("             and so.order_date >= trunc(sysdate) and so.order_date < (trunc(sysdate) + 1) ");
			sql.append("         group by so_d.product_id) ex_p on ex_p.product_id = pro.product_id ");

			params.add(shopId);
			params.add(OrderType.IN.getValue());
			params.add(SaleOrderStatus.APPROVED.getValue());

			// tong xuat vansale ngay hien tai
			sql.append("     left join(select sto_d.product_id, sum(sto_d.quantity) as quantity ");
			sql.append("         from stock_trans sto, stock_trans_detail sto_d ");
			sql.append("         where sto.stock_trans_id = sto_d.stock_trans_id ");
			sql.append("             and sto.shop_id = ? ");
			sql.append("             and sto.from_owner_type = ? ");
			sql.append("             and sto.to_owner_type = ? and sto.from_owner_id = ?");
			sql.append("             and sto.stock_trans_date >= trunc(sysdate) and sto.stock_trans_date < (trunc(sysdate)  + 1) ");
			sql.append("         group by sto_d.product_id) ex_v on ex_v.product_id = pro.product_id ");

			params.add(shopId);
			params.add(StockObjectType.SHOP.getValue());
			params.add(StockObjectType.STAFF.getValue());
			params.add(shopId);

			// xuat tra vnm ngay hien tai
			sql.append("     left join(select po_d.product_id, sum(po_d.quantity) as quantity ");
			sql.append("         from po_vnm po, po_vnm_detail po_d ");
			sql.append("         where po.po_vnm_id = po_d.po_vnm_id ");
			sql.append("             and po.shop_id = ? ");
			sql.append("             and po.type = ? and po.status = ? ");
			sql.append("             and po.import_date >= trunc(sysdate) and po.import_date < (trunc(sysdate) + 1) ");
			sql.append("         group by po_d.product_id) ex_vnm on ex_vnm.product_id = pro.product_id ");

			params.add(shopId);
			params.add(PoType.RETURNED_SALES_ORDER.getValue());
			params.add(PoVNMStatus.IMPORTED.getValue());

			// tong xuat dieu chinh ngay hien tai
			sql.append("     left join(select sto_d.product_id, sum(sto_d.quantity) as quantity ");
			sql.append("         from stock_trans sto, stock_trans_detail sto_d ");
			sql.append("         where sto.stock_trans_id = sto_d.stock_trans_id     ");
			sql.append("             and sto.shop_id = ? ");
			sql.append("             and sto.from_owner_type = ? ");
			sql.append("             and sto.to_owner_type is null and sto.from_owner_id = ?");
			sql.append("             and sto.stock_trans_date >= trunc(sysdate) and sto.stock_trans_date < (trunc(sysdate) + 1) ");
			sql.append("         group by sto_d.product_id) ex_e on ex_e.product_id = pro.product_id ");

			params.add(shopId);
			params.add(StockObjectType.SHOP.getValue());
			params.add(shopId);

			// ton cuoi ky
			sql.append("     left join(select product_id, quantity ");
			sql.append("         from stock_total ");
			sql.append("         where object_type = ? ");
			sql.append("             and object_id = ?) stk_e on stk_e.product_id = pro.product_id ");

			params.add(StockObjectType.SHOP.getValue());
			params.add(shopId);

			// tong xuat ban/khuyen mai/huy, tra doi kh presale ngay hien tai
			sql.append("     left join(select so_d.product_id, sum(so_d.quantity) as quantity ");
			sql.append("         from sale_order so, sale_order_detail so_d ");
			sql.append("         where so.sale_order_id = so_d.sale_order_id ");
			sql.append("             and so.shop_id = ? ");
			sql.append("             and so.order_type = ? ");
			sql.append("             and so.approved = ? ");
			sql.append("             and so.from_sale_order_id is null ");
			sql.append("             and so.order_date >= trunc(sysdate) and so.order_date < (trunc(sysdate) + 1) ");
			sql.append("         group by so_d.product_id) ex_e_v on ex_e_v.product_id = pro.product_id ");

			params.add(shopId);
			params.add(OrderType.SO.getValue());
			params.add(SaleOrderStatus.APPROVED.getValue());

			// ke hoach tieu thu thang
			sql.append("     left join(select product_id, quantity ");
			sql.append("         from sale_plan ");
			sql.append("         where object_type = ? ");
			sql.append("             and object_id = ? ");
			sql.append("             and month = extract( month from ?) ");
			sql.append("             and year = extract( year from ?) ");
			sql.append("     )sal_p on pro.product_id = sal_p.product_id ");

			params.add(SalePlanOwnerType.SHOP.getValue());
			params.add(shopId);
			params.add(toDate);
			params.add(toDate);

			sql.append("     left join(select sp.product_id,  ");
			sql.append("                       sp.minsf min,  ");
			sql.append("                       sp.lead next,  ");
			sql.append("                       (case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 1, 7) + 1)) > 0) then 1 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 2, 7) + 1)) > 0) then 2 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 3, 7) + 1)) > 0) then 3 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 4, 7) + 1)) > 0) then 4 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 5, 7) + 1)) > 0) then 5 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 6, 7) + 1)) > 0) then 6 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 7, 7) + 1)) > 0) then 7 else 0 end end end end end end end) lead,  ");
			sql.append("                       sp.percentage percentage  ");
			sql.append("         from shop_product sp  ");
			sql.append("         where sp.status = ?  ");
			sql.append("             and sp.shop_id = ? ");
			sql.append("             and sp.type = ? ");

			params.add(ActiveType.RUNNING.getValue());
			params.add(shopId);
			params.add(ShopProductType.PRODUCT.getValue());

			sql.append("     ) sp_tmp on sp_tmp.product_id = pro.product_id ");
			sql.append("     left join(select sp.cat_id, ");
			sql.append("                     sp.minsf min, ");
			sql.append("                     sp.lead lead, ");
			sql.append("                     (case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 1, 7) + 1)) > 0) then 1 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 2, 7) + 1)) > 0) then 2 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 3, 7) + 1)) > 0) then 3 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 4, 7) + 1)) > 0) then 4 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 5, 7) + 1)) > 0) then 5 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 6, 7) + 1)) > 0) then 6 else case when (instr(sp.calendar_d, to_char(mod(to_number(to_char(sysdate, 'd')) - 1 + 7, 7) + 1)) > 0) then 7 else 0 end end end end end end end) next, ");
			sql.append("                     sp.percentage percentage ");
			sql.append("     from shop_product sp ");
			sql.append("     where  sp.status = ? ");
			sql.append("         and sp.shop_id = ? ");
			sql.append("         and sp.type = ? ");
			sql.append("     ) sp_tmp_cat on sp_tmp_cat.cat_id = pro.cat_id ");

			params.add(ActiveType.RUNNING.getValue());
			params.add(shopId);
			params.add(ShopProductType.CAT.getValue());

			// so luong po confirm
			sql.append("     left join(select pvd.product_id product_id, sum(pvd.quantity) quantity ");
			sql.append("        from po_vnm pv, po_vnm_detail pvd ");
			sql.append("        where pv.po_vnm_id = pvd.po_vnm_id ");
			sql.append("          and pv.type = ? ");
			sql.append("          and pv.status = ? ");
			sql.append("          and pv.shop_id = ? ");
			sql.append("        group by pvd.product_id) po_cf on po_cf.product_id = pro.product_id ");

			params.add(PoType.PO_CONFIRM.getValue());
			params.add(PoVNMStatus.NOT_IMPORT.getValue());
			params.add(shopId);

			// tinh so luong po dvkh
			sql.append("     left join(select pvd.product_id product_id, sum(pvd.quantity) - sum(pvd.quantity_received) quantity ");
			sql.append("        from po_vnm pv, po_vnm_detail pvd ");
			sql.append("        where pv.po_vnm_id = pvd.po_vnm_id ");
			sql.append("          and pv.type = ? ");
			sql.append("          and pv.status in (?, ?) ");
			sql.append("          and pv.shop_id = ? ");
			sql.append("        group by pvd.product_id) po_cs on po_cs.product_id = pro.product_id  ");

			params.add(PoType.PO_CUSTOMER_SERVICE.getValue());
			params.add(PoVNMStatus.NOT_IMPORT.getValue());
			params.add(PoVNMStatus.IMPORTING.getValue());
			params.add(shopId);

			sql.append(" order by NLSSORT(pro.product_info_name,'NLS_SORT=vietnamese'), NLSSORT(pro.product_name,'NLS_SORT=vietnamese') ");
		}

		final String[] fieldNames = new String[] { //
		"productCode",// 1
				"productName",// 2
				"convfact",// 3
				"productInfoCode",// 4
				"price",// 5
				"openStockTotal",// 6
				"importQty",// 7
				"exportQty",// 8
				"closeStockTotal",// 9
				"monthCumulate",// 10
				"monthPlan",// 11
				"min",// 12
				"lead",// 13
				"next",// 14
				"percentage",// 15
				"poCfQty",// 16
				"poCsQty",// 17
				"saleDateInMonth",// 18
				"realSaleDateInMonth"// 19
		};

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.LONG,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.BIG_DECIMAL,// 5
				StandardBasicTypes.LONG,// 6
				StandardBasicTypes.LONG,// 7
				StandardBasicTypes.LONG,// 8
				StandardBasicTypes.LONG,// 9
				StandardBasicTypes.LONG,// 10
				StandardBasicTypes.LONG,// 11
				StandardBasicTypes.LONG,// 12
				StandardBasicTypes.LONG,// 13
				StandardBasicTypes.LONG,// 14
				StandardBasicTypes.INTEGER,// 15
				StandardBasicTypes.LONG,// 16
				StandardBasicTypes.LONG,// 17
				StandardBasicTypes.INTEGER,// 18
				StandardBasicTypes.INTEGER // 19
		};

		return repo.getListByQueryAndScalar(RptF1DataVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public void updateDb() throws DataAccessException {
		String sql = "select * from product";
		List<Object> params = new ArrayList<Object>();
		List<Product> lst = repo.getListBySQL(Product.class, sql, params);
		for (Product c : lst) {
			if (c.getProductName() != null) {
				c.setNameText(Unicode2English.codau2khongdau(c.getProductName().toUpperCase()));
				if (c.getStatus() == null)
					c.setStatus(ActiveType.DELETED);
				this.updateProduct(c, null);
			}
		}
	}

	@Override
	public List<PoVnmDetailLotVO> getListProductForPoConfirm(KPaging<PoVnmDetailLotVO> kPaging, PoVnmFilter filter) throws DataAccessException {
		if (filter == null || filter.getPoVnmId() == null) {
			return null;
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		sql.append(" WITH poconfirm AS");
		sql.append("   (SELECT pr.product_id, sum(pod.quantity) quantity");
		sql.append("   FROM po_vnm po");
		sql.append("   JOIN po_vnm_detail pod ");
		sql.append("   ON po.po_vnm_id = pod.po_vnm_id");
		sql.append("   JOIN product pr ");
		sql.append("   ON pr.product_id = pod.product_id");
		sql.append("   WHERE po.type            = 2 and po.object_type = 0");
		if (filter.getPoConfirmId() != null) {
			sql.append("   AND po.po_vnm_id != ? ");
			params.add(filter.getPoConfirmId());
		}
		sql.append("   AND po.SALE_ORDER_NUMBER =");
		sql.append("     (SELECT SALE_ORDER_NUMBER FROM po_vnm WHERE po_vnm_id = ?");
		params.add(filter.getPoVnmId());
		sql.append("     )");
		sql.append("   group by pr.product_id");
		sql.append("   )");
		sql.append(" select productId,");
		sql.append("   productCode ,");
		sql.append("   productName,");
		sql.append(" checkLot,");
		sql.append("   convfact,");
		sql.append("   price,");
		sql.append("   quantityStockTotal,");
		sql.append("   (quantityStockTotal - usedQuantity) quantityStock");
		fromSql.append("   from (");
		fromSql.append(" SELECT distinct pr.product_id productId,");
		fromSql.append("   pr.product_code productCode ,");
		fromSql.append("   pr.product_name productName,");
		fromSql.append("   pr.check_lot checkLot,");
		fromSql.append("   pr.convfact,");
		fromSql.append("   prc.price,");
		fromSql.append("   pod.quantity quantityStockTotal,");
		fromSql.append("   nvl((select quantity from poconfirm where product_id = pr.product_id),0) usedQuantity");
		fromSql.append(" FROM product pr");
		fromSql.append(" JOIN price prc");
		fromSql.append(" ON pr.product_id = prc.product_id");
		fromSql.append(" JOIN po_vnm_detail pod ");
		fromSql.append(" ON pr.product_id = pod.product_id");
		fromSql.append(" JOIN po_vnm po ");
		fromSql.append(" ON po.po_vnm_id = pod.po_vnm_id");
		fromSql.append(" WHERE pr.status  = 1");
		fromSql.append(" AND prc.status   = 1");

		fromSql.append(" 	and prc.from_date < trunc(sysdate) + 1 and prc.to_date >= trunc(sysdate)");
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			fromSql.append(" and pr.product_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getProductCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			fromSql.append(" and lower(pr.name_text) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getProductName()).trim().toLowerCase()));
		}
		if (filter.getPoVnmId() != null) {
			fromSql.append(" and pod.po_vnm_id = ? )");
			params.add(filter.getPoVnmId());
		}

		fromSql.append(" where quantityStockTotal - usedQuantity > 0");

		sql.append(fromSql.toString());
		sql.append(" order by productCode");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(*) count from (");
		countSql.append(sql.toString());
		countSql.append(")");
		String[] fieldNames = { "productId", "productCode", "productName", "checkLot", "convfact", "price", "quantityStock", "quantityStockTotal" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER };

		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(PoVnmDetailLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<StockTotalVO> getListProductForPoAuto(KPaging<StockTotalVO> kPaging, ProductsForPoAutoParams filter) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" select pr.product_id productId, pr.product_code productCode");
		sql.append("	, pr.product_name productName, pr.convfact, prc.price");
		sql.append(" from product pr join price prc on pr.product_id = prc.product_id");
		sql.append(" where pr.status = ? and prc.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" 	and (prc.from_date < trunc(sysdate) + 1 or prc.from_date is null) and (prc.to_date >= trunc(sysdate) or prc.to_date is null)");
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			sql.append(" and pr.product_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getProductCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			sql.append(" and lower(pr.name_text) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getProductName()).trim().toLowerCase()));
		}
		if (filter.getPoAutoId() != null) {
			sql.append(" and pr.product_id not in (select product_id from po_auto_detail where po_auto_id = ?)");
			params.add(filter.getPoAutoId());
		}
		if (filter.getLstExceptProductId() != null) {
			sql.append(" and pr.product_id not in (");
			for (Long productId : filter.getLstExceptProductId()) {
				if (productId != null) {
					sql.append(productId);
					sql.append(",");
				}
			}
			sql.append(" -1)");
		}

		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");

		sql.append(" order by pr.product_code");

		String[] fieldNames = { "productId", "productCode", "productName", "convfact", "price" };
		Type[] fieldTypes = { StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL };

		if (kPaging != null)
			return repo.getListByQueryAndScalarPaginated(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		else
			return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.ProductDAO#getListProductHasInSaleOrder(com.viettel
	 * .core.entities.enumtype.KPaging, java.lang.String, java.lang.String,
	 * ths.dms.core.entities.enumtype.ActiveType)
	 */
	@Override
	public List<Product> getListProductHasInSaleOrder(KPaging<Product> kPaging, String productCode, String productName, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * FROM product WHERE 1 = 1 ");
		sql.append(" AND product_id IN");
		sql.append(" ( SELECT DISTINCT(sod.product_id) ");
		sql.append(" FROM sale_order_detail sod ");
		sql.append(" JOIN sale_order so ");
		sql.append(" ON sod.sale_order_id = so.sale_order_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND so.order_type   IN (?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		sql.append(" AND so.type = ? ");
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		sql.append(" AND so.approved = ? ");
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append(" AND so.order_date   >= TRUNC(sysdate, 'MM') ");
		sql.append(" AND so.order_date    < TRUNC(sysdate + 1)) ");
		if (status != null) {
			sql.append(" AND status = ? ");
			params.add(status.getValue());
		}
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and product_code like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(productCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" and lower(name_text) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(productName).trim().toLowerCase()));
		}
		sql.append(" order by product_code ");
		if (kPaging == null)
			return repo.getListBySQL(Product.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Product.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<ProductTreeVO> getListProductTreeVO(String productCode, String productName, Long programObjectId, ProgramObjectType programObjectType, Boolean exceptEquipmentCat, DisplayProductGroupType type) throws DataAccessException {

		StringBuilder prdSql = new StringBuilder();
		StringBuilder catSql = new StringBuilder();
		StringBuilder subSql = new StringBuilder();

		List<Object> params = new ArrayList<Object>();

		prdSql.append(" select product_id id, (product_code || '-' || product_name) name, sub_cat_id parentId, convfact convfact from product p where p.status = 1");
		catSql.append(" select distinct cat_id id, (select product_info_code || '-' || product_info_name from product_info where product_info_id = p.cat_id) name, null parentId, null convfact from product p where status = 1 and cat_id is not null ");
		subSql.append(" select distinct sub_cat_id id, (select product_info_code || '-' || product_info_name from product_info where product_info_id = p.sub_cat_id) name, cat_id parentId, null convfact from product p where status = 1 and sub_cat_id is not null ");

		if (!StringUtility.isNullOrEmpty(productCode)) {
			prdSql.append(" and product_code like ? escape '/'");
			catSql.append(" and product_code like ? escape '/'");
			subSql.append(" and product_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLike(productCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(productName)) {
			prdSql.append(" and lower(name_text) like ? escape '/' ");
			catSql.append(" and lower(name_text) like ? escape '/' ");
			subSql.append(" and lower(name_text) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(productName.toLowerCase())));
		}
		if (programObjectId != null) {
			if (programObjectType == null) {
				throw new IllegalArgumentException("programObjectType is null");
			}
			if (ProgramObjectType.EQUIPMENT.equals(programObjectType)) {
				prdSql.append(" and NOT EXISTS (select 1 from equipment_product pdt where pdt.equip_product_id = ? and pdt.product_id = p.product_id)");
				catSql.append(" and NOT EXISTS (select 1 from equipment_product pdt where pdt.equip_product_id = ? and pdt.product_id = p.product_id)");
				subSql.append(" and NOT EXISTS (select 1 from equipment_product pdt where pdt.equip_product_id = ? and pdt.product_id = p.product_id)");
			}
			if (ProgramObjectType.GROUP_PO.equals(programObjectType)) {//   
				//				prdSql.append(" and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.po_auto_group_id = ? and pdt.object_id = p.product_id)"); 
				//				catSql.append(" and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.po_auto_group_id = ? and pdt.object_id = p.product_id)");
				//				subSql.append(" and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.po_auto_group_id = ? and pdt.object_id = p.product_id)"); 

				prdSql.append(" and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.status = 1  and pdt.object_id = p.product_id)");
				catSql.append(" and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.status = 1  and pdt.object_id = p.product_id)");
				subSql.append(" and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.status = 1  and pdt.object_id = p.product_id)");
			}
			if (ProgramObjectType.GROUP_PO_NPP.equals(programObjectType)) {//   
				//				prdSql.append(" and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.po_auto_group_id = ? and pdt.object_id = p.product_id)"); 
				//				catSql.append(" and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.po_auto_group_id = ? and pdt.object_id = p.product_id)");
				//				subSql.append(" and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.po_auto_group_id = ? and pdt.object_id = p.product_id)"); 

				/* fix bug http://10.30.174.211/mantis/view.php?id=2326 */
				/*
				 * prdSql.append(
				 * " and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.status = 1  and pdt.object_id = p.product_id)"
				 * ); catSql.append(
				 * " and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.status = 1  and pdt.object_id = p.product_id)"
				 * ); subSql.append(
				 * " and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.status = 1  and pdt.object_id = p.product_id)"
				 * );
				 */
			}
			if (ProgramObjectType.INCENTIVE_PROGRAM.equals(programObjectType)) {
				prdSql.append(" and NOT EXISTS (select 1 from incentive_program_detail pdt where status = 1 and incentive_program_id = ? and pdt.product_id = p.product_id)");
				catSql.append(" and NOT EXISTS (select 1 from incentive_program_detail pdt where status = 1 and incentive_program_id = ? and pdt.product_id = p.product_id)");
				subSql.append(" and NOT EXISTS (select 1 from incentive_program_detail pdt where status = 1 and incentive_program_id = ? and pdt.product_id = p.product_id)");

				prdSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				catSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				subSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
			}
			if (ProgramObjectType.FOCUS_PROGRAM.equals(programObjectType)) {
				prdSql.append(" and NOT EXISTS (select 1 from focus_channel_map_product pdt where status = 1 and pdt.product_id = p.product_id and exists (select 1 from focus_channel_map where status = 1 and focus_channel_map_id = pdt.focus_channel_map_id and focus_program_id = ?))");
				catSql.append(" and NOT EXISTS (select 1 from focus_channel_map_product pdt where status = 1 and pdt.product_id = p.product_id and exists (select 1 from focus_channel_map where status = 1 and focus_channel_map_id = pdt.focus_channel_map_id and focus_program_id = ?))");
				subSql.append(" and NOT EXISTS (select 1 from focus_channel_map_product pdt where status = 1 and pdt.product_id = p.product_id and exists (select 1 from focus_channel_map where status = 1 and focus_channel_map_id = pdt.focus_channel_map_id and focus_program_id = ?))");

				prdSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				catSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				subSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");

			}
			if (ProgramObjectType.DISPLAY_PROGRAM.equals(programObjectType)) {
				prdSql.append(" and NOT EXISTS (select 1 from display_product_group_dtl pdt where status = 1 and pdt.product_id = p.product_id and exists (select 1 from display_product_group where status = 1 and display_product_group_id = pdt.display_product_group_id and type = ? and display_program_id = ?))");
				catSql.append(" and NOT EXISTS (select 1 from display_product_group_dtl pdt where status = 1 and pdt.product_id = p.product_id and exists (select 1 from display_product_group where status = 1 and display_product_group_id = pdt.display_product_group_id and type = ? and display_program_id = ?))");
				subSql.append(" and NOT EXISTS (select 1 from display_product_group_dtl pdt where status = 1 and pdt.product_id = p.product_id and exists (select 1 from display_product_group where status = 1 and display_product_group_id = pdt.display_product_group_id and type = ? and display_program_id = ?))");
				params.add(type.getValue());
				prdSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				catSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				subSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
			}
			if (ProgramObjectType.PROMOTION_PROGRAM.equals(programObjectType)) {
				prdSql.append(" and NOT EXISTS (select 1 from promotion_program_detail pdt where status = 1 and promotion_program_id = ? and pdt.product_id = p.product_id)");
				catSql.append(" and NOT EXISTS (select 1 from promotion_program_detail pdt where status = 1 and promotion_program_id = ? and pdt.product_id = p.product_id)");
				subSql.append(" and NOT EXISTS (select 1 from promotion_program_detail pdt where status = 1 and promotion_program_id = ? and pdt.product_id = p.product_id)");

				prdSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				catSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				subSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
			}
			if (!ProgramObjectType.GROUP_PO.equals(programObjectType) && !ProgramObjectType.GROUP_PO_NPP.equals(programObjectType)) {
				params.add(programObjectId);
			}
		}
		if (Boolean.TRUE.equals(exceptEquipmentCat)) {
			prdSql.append(" and NOT EXISTS (select 1 from product_info where product_info_id = p.cat_id and product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
			catSql.append(" and NOT EXISTS (select 1 from product_info where product_info_id = p.cat_id and product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
			subSql.append(" and NOT EXISTS (select 1 from product_info where product_info_id = p.cat_id and product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
		}

		prdSql.append(" order by name");
		catSql.append(" order by name");
		subSql.append(" order by name");

		String[] fieldNames = { "id", "name", "parentId", "convfact" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		List<ProductTreeVO> products = repo.getListByQueryAndScalar(ProductTreeVO.class, fieldNames, fieldTypes, prdSql.toString(), params);
		List<ProductTreeVO> cats = repo.getListByQueryAndScalar(ProductTreeVO.class, fieldNames, fieldTypes, catSql.toString(), params);
		List<ProductTreeVO> subs = repo.getListByQueryAndScalar(ProductTreeVO.class, fieldNames, fieldTypes, subSql.toString(), params);

		for (ProductTreeVO sub : subs) {
			for (ProductTreeVO prd : products) {
				if (sub.getId().equals(prd.getParentId())) {
					prd.setType(TreeVOType.PRODUCT);
					sub.getListChildren().add(prd);
				}
			}
		}

		for (ProductTreeVO cat : cats) {
			cat.setType(TreeVOType.CATEGORY);
			for (ProductTreeVO sub : subs) {
				if (cat.getId().equals(sub.getParentId())) {
					sub.setType(TreeVOType.SUB_CATEGORY);
					cat.getListChildren().add(sub);
				}
			}
		}

		return cats;
	}

	@Override
	public List<ProductTreeVO> getListProductTreeVOByParentNode(Long programObjectId, ProgramObjectType programObjectType, Long parentIdNode, TreeVOType parentTypeNode, Boolean exceptEquipmentCat, DisplayProductGroupType type)
			throws DataAccessException {
		StringBuilder prdSql = new StringBuilder();
		StringBuilder catSql = new StringBuilder();
		StringBuilder subSql = new StringBuilder();

		List<Object> params = new ArrayList<Object>();

		prdSql.append(" select product_id id, (product_code || '-' || product_name) name, sub_cat_id parentId, convfact convfact from product p where p.status = 1");
		catSql.append(" select distinct cat_id id, (select product_info_code || '-' || product_info_name from product_info where product_info_id = p.cat_id) name, null parentId, null convfact from product p where status = 1");
		subSql.append(" select distinct sub_cat_id id, (select product_info_code || '-' || product_info_name from product_info where product_info_id = p.sub_cat_id) name, cat_id parentId, null convfact from product p where status = 1");

		if (programObjectId != null) {
			if (programObjectType == null) {
				throw new IllegalArgumentException("programObjectType is null");
			}
			if (ProgramObjectType.EQUIPMENT.equals(programObjectType)) {
				prdSql.append(" and NOT EXISTS (select 1 from equipment_product pdt where pdt.equip_product_id = ? and pdt.product_id = p.product_id)");
				catSql.append(" and NOT EXISTS (select 1 from equipment_product pdt where pdt.equip_product_id = ? and pdt.product_id = p.product_id)");
				subSql.append(" and NOT EXISTS (select 1 from equipment_product pdt where pdt.equip_product_id = ? and pdt.product_id = p.product_id)");
			}
			if (ProgramObjectType.GROUP_PO.equals(programObjectType)) {
				prdSql.append(" and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.po_auto_group_id = ? and pdt.object_id = p.product_id)");
				catSql.append(" and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.po_auto_group_id = ? and pdt.object_id = p.product_id)");
				subSql.append(" and NOT EXISTS (select 1 from po_auto_group_detail pdt join po_auto_group p on p.po_auto_group_id = pdt.po_auto_group_id where p.status = 1 and pdt.po_auto_group_id = ? and pdt.object_id = p.product_id)");
			}
			if (ProgramObjectType.INCENTIVE_PROGRAM.equals(programObjectType)) {
				prdSql.append(" and NOT EXISTS (select 1 from incentive_program_detail pdt where status = 1 and incentive_program_id = ? and pdt.product_id = p.product_id)");
				catSql.append(" and NOT EXISTS (select 1 from incentive_program_detail pdt where status = 1 and incentive_program_id = ? and pdt.product_id = p.product_id)");
				subSql.append(" and NOT EXISTS (select 1 from incentive_program_detail pdt where status = 1 and incentive_program_id = ? and pdt.product_id = p.product_id)");

				prdSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				catSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				subSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
			}
			if (ProgramObjectType.FOCUS_PROGRAM.equals(programObjectType)) {
				prdSql.append(" and NOT EXISTS (select 1 from focus_channel_map_product pdt where status = 1 and pdt.product_id = p.product_id and exists (select 1 from focus_channel_map where status = 1 and focus_channel_map_id = pdt.focus_channel_map_id and focus_program_id = ?))");
				catSql.append(" and NOT EXISTS (select 1 from focus_channel_map_product pdt where status = 1 and pdt.product_id = p.product_id and exists (select 1 from focus_channel_map where status = 1 and focus_channel_map_id = pdt.focus_channel_map_id and focus_program_id = ?))");
				subSql.append(" and NOT EXISTS (select 1 from focus_channel_map_product pdt where status = 1 and pdt.product_id = p.product_id and exists (select 1 from focus_channel_map where status = 1 and focus_channel_map_id = pdt.focus_channel_map_id and focus_program_id = ?))");

				prdSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				catSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				subSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");

			}
			if (ProgramObjectType.DISPLAY_PROGRAM.equals(programObjectType)) {
				prdSql.append(" and NOT EXISTS (select 1 from display_product_group_dtl pdt where status = 1 and pdt.product_id = p.product_id and exists (select 1 from display_product_group where status = 1 and display_product_group_id = pdt.display_product_group_id and type = ? and display_program_id = ?))");
				catSql.append(" and NOT EXISTS (select 1 from display_product_group_dtl pdt where status = 1 and pdt.product_id = p.product_id and exists (select 1 from display_product_group where status = 1 and display_product_group_id = pdt.display_product_group_id and type = ? and display_program_id = ?))");
				subSql.append(" and NOT EXISTS (select 1 from display_product_group_dtl pdt where status = 1 and pdt.product_id = p.product_id and exists (select 1 from display_product_group where status = 1 and display_product_group_id = pdt.display_product_group_id and type = ? and display_program_id = ?))");
				params.add(type.getValue());
				prdSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				catSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				subSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
			}
			if (ProgramObjectType.PROMOTION_PROGRAM.equals(programObjectType)) {
				prdSql.append(" and NOT EXISTS (select 1 from promotion_program_detail pdt where status = 1 and promotion_program_id = ? and pdt.product_id = p.product_id)");
				catSql.append(" and NOT EXISTS (select 1 from promotion_program_detail pdt where status = 1 and promotion_program_id = ? and pdt.product_id = p.product_id)");
				subSql.append(" and NOT EXISTS (select 1 from promotion_program_detail pdt where status = 1 and promotion_program_id = ? and pdt.product_id = p.product_id)");

				prdSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				catSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
				subSql.append(" and EXISTS (SELECT 1 from price where product_id = p.product_id and status = 1 and from_date <= trunc(sysdate) + 1 and (to_date >= trunc(sysdate) or to_date is null))");
			}
			params.add(programObjectId);
		}
		if (Boolean.TRUE.equals(exceptEquipmentCat)) {
			prdSql.append(" and NOT EXISTS (select 1 from product_info where product_info_id = p.cat_id and product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
			catSql.append(" and NOT EXISTS (select 1 from product_info where product_info_id = p.cat_id and product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
			subSql.append(" and NOT EXISTS (select 1 from product_info where product_info_id = p.cat_id and product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
		}
		if (parentTypeNode == null) {

		} else if (TreeVOType.CATEGORY.equals(parentTypeNode)) {
			subSql.append(" and p.cat_id = ?");
			params.add(parentIdNode);
		} else if (TreeVOType.SUB_CATEGORY.equals(parentTypeNode)) {
			prdSql.append(" and p.sub_cat_id = ?");
			params.add(parentIdNode);
		}
		prdSql.append(" order by name");
		catSql.append(" order by name");
		subSql.append(" order by name");

		String[] fieldNames = { "id", "name", "parentId", "convfact" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };

		List<ProductTreeVO> lstProductTreeVO = new ArrayList<ProductTreeVO>();
		if (parentTypeNode == null) {
			lstProductTreeVO = repo.getListByQueryAndScalar(ProductTreeVO.class, fieldNames, fieldTypes, catSql.toString(), params);
			for (ProductTreeVO cat : lstProductTreeVO) {
				cat.setType(TreeVOType.CATEGORY);
			}
		} else if (TreeVOType.CATEGORY.equals(parentTypeNode)) {
			lstProductTreeVO = repo.getListByQueryAndScalar(ProductTreeVO.class, fieldNames, fieldTypes, subSql.toString(), params);
			for (ProductTreeVO subcat : lstProductTreeVO) {
				subcat.setType(TreeVOType.SUB_CATEGORY);
			}
		} else if (TreeVOType.SUB_CATEGORY.equals(parentTypeNode)) {
			lstProductTreeVO = repo.getListByQueryAndScalar(ProductTreeVO.class, fieldNames, fieldTypes, prdSql.toString(), params);
			for (ProductTreeVO prd : lstProductTreeVO) {
				prd.setType(TreeVOType.PRODUCT);
			}
		} else if (TreeVOType.PRODUCT.equals(parentTypeNode)) {

		}
		return lstProductTreeVO;
	}

	@Override
	public Boolean isProductBelongEquipmentCat(Product product) throws DataAccessException {
		if (product == null || product.getCat() == null) {
			return false;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT count(1) as count ");
		sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT'  and ap.status = 1");
		sql.append(" AND upper(ap.ap_param_code)  = ?");
		params.add(product.getCat().getProductInfoCode().toUpperCase());
		int res = repo.countBySQL(sql.toString(), params);
		return res == 1 ? true : false;
	}

	@Override
	public List<GeneralProductVO> getAllProducts(String productCode, KPaging<GeneralProductVO> kPaging, Boolean isExceptZCat) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder sqlCount = new StringBuilder();
		StringBuilder sqlSelect = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sqlCount.append("select count(1) as count ");
		sqlSelect.append("select p.product_id as productId, ");
		sqlSelect.append("p.product_code as productCode, ");
		sqlSelect.append("p.product_name as productName ");
		sql.append("FROM PRODUCT p where 1 = 1");
		if (isExceptZCat) {
			sql.append(" and NOT EXISTS (select 1 from product_info where product_info_id = p.cat_id and product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
		}
		sql.append(" and exists (select 1 from price where product_id = p.product_id and status = 1 and from_date   <= TRUNC(sysdate) + 1  and (to_date    >= TRUNC(sysdate) or to_date is null ))");
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and p.product_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase()));
		}
		sql.append(" and p.status = ? ");
		sqlSelect.append(sql.toString());
		sqlCount.append(sql.toString());
		sqlSelect.append(" order by p.product_code");
		params.add(ActiveType.RUNNING.getValue());
		String[] fieldNames = { "productId", "productCode", "productName" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(GeneralProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(GeneralProductVO.class, fieldNames, fieldTypes, sqlSelect.toString(), sqlCount.toString(), params, params, kPaging);
		}
	}

	@Override
	public List<Product> getListProductEquipmentZ() throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product  p ");
		sql.append(" where status=1 AND p.cat_id in ");
		sql.append(" (select product_info_id from product_info where status = 1 and product_info_code in (SELECT ap_param_code ");
		sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and status = 1 )) ");
		return repo.getListBySQL(Product.class, sql.toString(), params);
	}

	@Override
	public List<Product> getListProductEquipmentZOrderByCat() throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select p.*");
		sql.append(" from product p, product_info pi, product_info pif where p.cat_id = pi.product_info_id and p.sub_cat_id = pif.product_info_id");
		sql.append(" and pi.product_info_code = (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT') and p.status = ? and pi.type = ? and pif.type = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ProductType.CAT.getValue());
		params.add(ProductType.SUB_CAT.getValue());
		sql.append(" order by pif.product_info_code, p.product_code ");
		return repo.getListBySQL(Product.class, sql.toString(), params);
	}

	@Override
	public Boolean checkExitsProductInEquipment(String productCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from product p, product_info pi where upper(p.product_code) = ?");
		params.add(productCode.toUpperCase());
		sql.append(" and p.cat_id = pi.product_info_id and pi.product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT')");
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public List<Product> getListProductInCatZ(KPaging<Product> paging, String productCode, String productName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from PRODUCT pr where status=? ");
		sql.append(" and exists (select 1 from product_info pi where pr.cat_id = pi.product_info_id ");
		sql.append(" and pi.product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT'))");
		List<Object> params = new ArrayList<Object>();
		params.add(ActiveType.RUNNING.getValue());
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append("  and product_code like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append("  and lower(name_text) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(productName).toLowerCase()));
		}
		sql.append("  order by product_code");
		List<Product> rs = null;
		if (paging != null)
			rs = repo.getListBySQLPaginated(Product.class, sql.toString(), params, paging);
		else
			rs = repo.getListBySQL(Product.class, sql.toString(), params);
		return rs;
	}

	@Override
	public List<ProductLot> getProductLotByProduct(Product product) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select * from PRODUCT_LOT pl, PRODUCT p ");
		sql.append(" where p.product_id = pl.product_id and p.product_id = ? and pl.quantity > 0");
		List<Object> params = new ArrayList<Object>();
		params.add(product.getId());
		return repo.getListBySQL(ProductLot.class, sql.toString(), params);

	}

	@Override
	public List<Rpt_TDXNTCT_NganhVO> LayDanhSachXuatNhapKhoChiTiet(Long shopId, Date fromDate, Date toDate) throws DataAccessException {
		ArrayList<Rpt_TDXNTCT_NganhVO> lstResult = new ArrayList<Rpt_TDXNTCT_NganhVO>();
		try {
			List<Object> inParams = new ArrayList<Object>();
			inParams.add("SHOPID");
			inParams.add(shopId);
			inParams.add(StandardBasicTypes.LONG);
			inParams.add("FROMDATE");
			inParams.add(fromDate);
			inParams.add(StandardBasicTypes.DATE);
			inParams.add("TODATE");
			inParams.add(toDate);
			inParams.add(StandardBasicTypes.DATE);
			List<Rpt_TDXNTCTVO> lstResultSimple = repo.getListByNamedQuery(Rpt_TDXNTCTVO.class, "PKG_SHOP_REPORT.PRO_TDXNTCT", inParams);

			int iCount = lstResultSimple.size();
			for (int i = 0; i < iCount; i++) {
				Rpt_TDXNTCTVO itemI = lstResultSimple.get(i);

				List<Rpt_TDXNTCTVO> lstSimpleNganh = new ArrayList<Rpt_TDXNTCTVO>();
				lstSimpleNganh.add(itemI);

				int iSave = i;

				for (int j = i + 1; j < iCount; j++) {
					iSave = j;

					Rpt_TDXNTCTVO itemJ = lstResultSimple.get(j);
					if (itemJ.getMa_nganh().equals(itemI.getMa_nganh())) {
						lstSimpleNganh.add(itemJ);
					} else {
						break;
					}

				}

				Rpt_TDXNTCT_NganhVO itemNganh = new Rpt_TDXNTCT_NganhVO(lstSimpleNganh);

				lstResult.add(itemNganh);

				if (i + 1 < iCount) {
					i = iSave - 1;
				}
			}
			//
			//return result;
		} catch (DataAccessException e) {

			System.out.println(e.getMessage());
		}
		return lstResult;
	}

	//CTTB 2013
	@Override
	public List<ProductVO> getListGroupProductVO(KPaging<ProductVO> kPaging, String productCode, String productName, Long displayId, DisplayProductGroupType type) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();

		sql.append("select pr.product_id as productId, pr.product_code as productCode, pr.product_name as productName, pi.product_info_code as catCode, pr.convfact as convfact ");
		sql.append("from product pr JOIN price price ON pr.product_id=price.product_id AND price.status=1 AND trunc(sysdate)>=trunc(price.from_date) AND (trunc(sysdate)<=trunc(price.to_date) OR price.to_date IS NULL) ");
		sql.append("join product_info pi on pi.product_info_id = pr.cat_id ");
		sql.append("where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and pr.product_code LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode.trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append("and lower(pr.name_text) LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(productName).trim().toUpperCase()));
		}

		sql.append("and pr.status = 1 ");
		sql.append("and pr.product_id not in ( ");
		sql.append("  select dpgd.product_id  ");
		sql.append("  from display_product_group_dtl dpgd  ");
		sql.append("    inner join display_product_group dpg on dpgd.display_product_group_id = dpg.display_product_group_id and dpg.status = 1 and dpgd.status = 1 ");
		sql.append("  where dpg.display_program_id = ? ");
		params.add(displayId);
		sql.append("    and dpg.type = ? ");
		params.add(type.getValue());
		sql.append(")  ");
		sql.append("order by pi.product_info_code, pr.product_code");
		final String[] fieldNames = new String[] { "productId", "productCode", "productName", "catCode", "convfact" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };

		if (kPaging == null) {
			return repo.getListByQueryAndScalar(ProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" SELECT COUNT(*) AS COUNT from ( ");
			countSql.append(sql);
			countSql.append(" ) ");
			return repo.getListByQueryAndScalarPaginated(ProductVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}

	@Override
	public List<ProductVOEx> getListProductVOForVideo(ths.dms.core.entities.filter.ProductFilter filter, List<Product> lstProductExcept) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT DISTINCT PS.PRODUCT_ID as id, ");
		sql.append(" PS.PRODUCT_CODE as productCode, ");
		sql.append(" PS.PRODUCT_NAME as productName, ");

		sql.append(" picat.product_info_code    AS productCat ");
		sql.append(" FROM PRODUCT PS JOIN product_info piCat on ps.cat_id = picat.product_info_id ");
		//		sql.append(" RIGHT JOIN ");
		//		sql.append(" ( ");
		//		sql.append(" SELECT * FROM PRICE PRI WHERE TRUNC(PRI.FROM_DATE)<=TRUNC(SYSDATE) AND (TRUNC(SYSDATE)<=TRUNC(PRI.TO_DATE) OR PRI.TO_DATE is null) ");
		//		sql.append(" AND PRI.STATUS = 1 ");
		//		sql.append(" ) TB ");
		//		sql.append(" ON TB.PRODUCT_ID=PS.PRODUCT_ID ");
		sql.append(" WHERE PS.STATUS=1 ");

		//Khong lay nghanh hang Z
		sql.append(" AND lower(picat.product_info_code)    <> 'z' ");

		if (!StringUtility.isNullOrEmpty(filter.getNamePro())) {
			sql.append(" AND (lower(PS.PRODUCT_NAME) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getNamePro().trim().toLowerCase()));
			sql.append(" OR lower(PS.NAME_TEXT) like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getNamePro()).trim().toLowerCase())); // Tulv2 update search khong dau
		}
		if (!StringUtility.isNullOrEmpty(filter.getCodePro())) {
			sql.append(" AND PS.PRODUCT_CODE like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getCodePro().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCatNamePro())) {
			sql.append(" AND picat.product_info_code like ? ");
			params.add(filter.getCatNamePro().trim().toUpperCase());
		}

		if (null != lstProductExcept && lstProductExcept.size() > 0) {
			sql.append(" AND PS.PRODUCT_CODE NOT IN (");
			for (int i = 0; i < lstProductExcept.size() - 1; i++) {
				sql.append("?,");
				params.add(lstProductExcept.get(i).getProductCode().trim().toUpperCase());
			}
			sql.append("?)");
			params.add(lstProductExcept.get(lstProductExcept.size() - 1).getProductCode().trim().toUpperCase());

		}
		sql.append(" ORDER BY PS.PRODUCT_CODE ");

		StringBuilder countSql = new StringBuilder();
		countSql.append(" SELECT COUNT(*) AS COUNT FROM ( ");
		countSql.append(sql.toString().trim());
		countSql.append(" ) ");
		String[] fieldNames = { "id",//1
				"productCode",//2
				"productName",//3
				"productCat"// 4
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING,//3
				StandardBasicTypes.STRING,// 4
		};
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(ProductVOEx.class, fieldNames, fieldTypes, sql.toString().trim(), countSql.toString().trim(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(ProductVOEx.class, fieldNames, fieldTypes, sql.toString().trim(), params);
		}
	}

	@Override
	public List<ProductVOEx> getListCategroy(String expectOne) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select distinct picat.product_info_code as productCat FROM PRODUCT PS JOIN product_info piCat on ps.cat_id = picat.product_info_id ");
		sql.append(" where picat.product_info_code != ? order by picat.product_info_code");
		params.add(expectOne);

		String[] fieldNames = { "productCat" };

		Type[] fieldTypes = { StandardBasicTypes.STRING,// 4
		};

		return repo.getListByQueryAndScalar(ProductVOEx.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<Product> getListProduct4ProductTool(KPaging<Product> kPaging, String productCode, String productName, String toolCode) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder("");
		sql.append("SELECT P.* FROM PRODUCT P join product_info pi on p.cat_id = pi.product_info_id");
		sql.append(" WHERE P.STATUS = ? AND pi.product_info_code   <> ? ");
		params.add(ActiveType.RUNNING.getValue());
		params.add('Z');
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" AND P.PRODUCT_CODE LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(productCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" and lower(P.NAME_TEXT) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(productName).trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(toolCode)) {
			sql.append(" and p.product_id not in (");
			sql.append(" SELECT PP.product_id FROM PRODUCT PP INNER JOIN DISPLAY_TOOLS_PRODUCT DTP ON PP.PRODUCT_ID = DTP.PRODUCT_ID");
			sql.append(" WHERE DTP.TOOL_ID IN (select product_id from product where upper(product_code) = ?) )");
			params.add(toolCode.toUpperCase());
		}
		sql.append(" order by P.product_code");
		if (kPaging == null) {
			return repo.getListBySQL(Product.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(Product.class, sql.toString(), params, kPaging);
		}
	}

	@Override
	public List<Product> getListProductToMTDisplayTeamplate(KPaging<Product> kPaging, String productCode, String productName, Long idMTDisplayTeamplate) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder("");
		sql.append(" select p.* from product p ");
		sql.append(" where 1=1 ");
		sql.append(" and p.status = 1 ");
		sql.append(" and p.category_code <>'Z' ");
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and p.product_code LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(productCode.trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" and lower(p.name_text) LIKE ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(productName).trim().toLowerCase()));
		}
		sql.append(" and p.product_id not in ( select nvl(object_id,0) from mt_display_template_detail ");
		sql.append(" where 1=1 ");
		sql.append(" and display_template_id = ? ");
		params.add(idMTDisplayTeamplate);
		sql.append(" and object_type = 2 ");
		sql.append(" and type = 2 ");
		sql.append(" and status = 1) ");
		sql.append(" order by p.product_code");
		if (kPaging == null) {
			return repo.getListBySQL(Product.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(Product.class, sql.toString(), params, kPaging);
		}
	}

	@Override
	public Product checkProductToMTDisplayTeamplateChange(String productCode, Long idMTDisplayTeamplate) throws DataAccessException {
		if (!StringUtility.isNullOrEmpty(productCode)) {
			productCode = productCode.trim();
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder("");
		sql.append(" select p.* from product p ");
		sql.append(" where 1=1 and upper(p.product_code) = ? ");
		params.add(productCode);
		sql.append(" and p.status = 1 ");
		sql.append(" and p.category_code <>'Z' ");
		sql.append(" and p.product_id not in ( select nvl(object_id,0) from mt_display_template_detail ");
		sql.append(" where 1=1 ");
		sql.append(" and display_template_id = ? ");
		params.add(idMTDisplayTeamplate);
		sql.append(" and object_type = 2 ");
		sql.append(" and type = 2 ");
		sql.append(" and status = 1) ");
		sql.append(" order by p.product_code");
		return repo.getEntityBySQL(Product.class, sql.toString(), params);
	}

	//hien tai IDP chua su dung; co the modify lai
	@Override
	public List<Product> getListProductByNameOrCode(KPaging<Product> kPaging, String name, String code, String categoryCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" select p.* ");
		sql.append(" from product p join price pr on p.product_id=pr.product_id ");
		sql.append(" join product_category pc on p.category_code=pc.category_code ");
		sql.append(" where p.status=1 and pr.status=1 and pc.type=0 ");
		sql.append(" and trunc(pr.from_date)<=trunc(sysdate) ");
		sql.append(" and (trunc(pr.to_date)>=trunc(sysdate) ");
		sql.append(" or pr.to_date is null) ");

		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" AND p.PRODUCT_CODE like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(code.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(name)) {
			sql.append(" and lower(p.NAME_TEXT) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(name).toLowerCase()));
		}

		if (!StringUtility.isNullOrEmpty(categoryCode)) {
			sql.append(" AND upper(p.CATEGORY_CODE) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(categoryCode.toUpperCase()));
		}
		sql.append(" order by p.product_code,p.product_name,p.category_code ");
		if (kPaging != null) {
			return repo.getListBySQLPaginated(Product.class, sql.toString(), params, kPaging);
		} else {
			return repo.getListBySQL(Product.class, sql.toString(), params);
		}
	}

	@Override
	public List<OrderProductVO> getListOrderProductVansale(KPaging<OrderProductVO> kPaging, Long shopId, Long staffId, String pdCode, String pdName, Date lockDay) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (staffId == null) {
			throw new IllegalArgumentException("staffId is null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		selectSql.append(" SELECT p.product_id as productId,");
		selectSql.append("   p.product_code as productCode,");
		selectSql.append("   p.product_name as productName,");
		selectSql.append("   st.available_quantity as availableQuantity,");
		selectSql.append("   st.quantity as stock,");
		selectSql.append("   pr.price as price,");
		selectSql.append("   p.convfact                as convfact,");
		selectSql.append("   p.gross_weight as grossWeight,");
		selectSql.append("   p.net_weight as netWeight,");
		selectSql.append("   p.check_lot as checkLot");

		countSql.append(" SELECT count(1) as count");

		sql.append(" FROM product p");
		sql.append(" LEFT JOIN stock_total st");
		sql.append(" ON (st.product_id = p.product_id ");
		sql.append(" AND st.object_id   = ?");
		params.add(shopId);
		sql.append(" AND st.object_type = ?)");
		params.add(StockObjectType.SHOP.getValue());
		sql.append(" JOIN price pr");
		sql.append(" ON (p.product_id         = pr.product_id");
		sql.append(" AND pr.from_date < TRUNC(?) + 1");
		params.add(lockDay);
		sql.append(" AND (pr.to_date         IS NULL");
		sql.append(" OR pr.to_date    >= TRUNC(?)) and pr.status = ?)");
		params.add(lockDay);
		params.add(ActiveType.RUNNING.getValue());
		sql.append("  AND ( (p.cat_id IN (SELECT cat_id FROM staff_sale_cat WHERE staff_id = ?) ");
		sql.append("       AND (SELECT COUNT(1) FROM staff_sale_cat WHERE staff_id = ?) > 0) ");
		sql.append(" 	 or (SELECT COUNT(1) FROM staff_sale_cat WHERE staff_id = ?) = 0 ");
		sql.append(" 	 ) ");
		params.add(staffId);
		params.add(staffId);
		params.add(staffId);
		sql.append(" WHERE p.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		if (!StringUtility.isNullOrEmpty(pdCode)) {
			sql.append(" AND p.product_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(pdCode.toUpperCase().trim()));
		}
		if (!StringUtility.isNullOrEmpty(pdName)) {
			sql.append(" AND lower(p.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(pdName).toLowerCase().trim()));
		}
		selectSql.append(sql);
		countSql.append(sql);

		selectSql.append(" order by p.product_code");
		String[] fieldNames = new String[] { "productId", // 1
				"productCode",// 2
				"productName", // 3
				"price", // 4
				"availableQuantity",// 5
				"stock",// 5
				"convfact", //7
				"grossWeight", //9
				"netWeight", //10
				"checkLot" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.BIG_DECIMAL,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.FLOAT,// 9
				StandardBasicTypes.FLOAT,// 10
				StandardBasicTypes.INTEGER };
		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(OrderProductVO.class, fieldNames, fieldTypes, selectSql.toString(), countSql.toString(), params, params, kPaging);
		} else {
			return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, selectSql.toString(), params);
		}
	}

	/**
	 * lay danh sach kho theo SP
	 * 
	 * @author nhanlt6
	 * @param paging
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public List<OrderProductVO> getListOrderProductWarehouseVO(KPaging<OrderProductVO> paging, Long productId, Long shopId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (productId == null) {
			throw new IllegalArgumentException("productId is null");
		}
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select p.product_id as productId ,wh.warehouse_code as warehouseCode, wh.warehouse_name as warehouseName, wh.warehouse_id as warehouseId, ");
		sql.append("       st.quantity as stock, p.convfact as convfact, st.available_quantity as availableQuantity ");
		sql.append("from product p ");
		sql.append("join stock_total st on p.product_id = st.product_id ");
		sql.append("join warehouse wh on st.warehouse_id = wh.warehouse_id ");
		sql.append("where p.product_id = ? and p.status = 1 and st.object_type = 1 and st.object_id = ? ");
		params.add(productId);
		params.add(shopId);

		countSql.append(" SELECT count(1) as count from ( ");
		countSql.append(sql);
		countSql.append("  )  ");

		String[] fieldNames = new String[] { "productId", "availableQuantity", "stock", "convfact", "warehouseCode", "warehouseName", "warehouseId" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG };
		if (paging != null) {
			return repo.getListByQueryAndScalarPaginated(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, paging);
		} else {
			return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<StockTotalVO> getPromotionProductInfo(Long productId, Long shopId, Integer shopChannel, Long customerId, Integer objectType, Date orderDate) throws DataAccessException {
		Shop shop = commonDAO.getEntityById(Shop.class, shopId);
		Long shopTypeId = shop != null && shop.getType() != null ? shop.getType().getId() : 0;

		Customer customer = commonDAO.getEntityById(Customer.class, customerId);
		Long customerTypeId = customer != null && customer.getChannelType() != null ? customer.getChannelType().getId() : 0;

		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();

		sql.append(" with  ");
		sql.append(" lstShop as ( ");
		sql.append("     select shop_id ,level as lv ");
		sql.append("     from shop s1 ");
		sql.append("     where status = 1 ");
		sql.append("     start with s1.shop_id           = ? ");
		params.add(shopId);
		sql.append("     connect by prior parent_shop_id = shop_id ");
		sql.append(" ) ");
		sql.append(" ,prc as ( ");
		sql.append("     SELECT    product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append("     FROM     price    prc ");
		sql.append("     WHERE    1 = 1     ");
		sql.append("     AND prc.status = 1     ");
		sql.append("     and prc.from_date <= ? ");
		params.add(orderDate);
		sql.append("     and (prc.to_date >= ? or prc.to_date is null) ");
		params.add(orderDate);
		sql.append("     and (shop_id is null or shop_id in (select shop_id from lstShop)) ");
		sql.append(" ) ");
		sql.append(" ,prcX as ( ");
		sql.append("     select  ");
		sql.append("         product_id,customer_id,customer_type_id,prc.shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append("         ,lstShop.lv, rank() over(PARTITION BY product_id ORDER BY lstShop.lv) rank_last ");
		sql.append("     from prc join lstShop on prc.shop_id = lstShop.shop_id ");
		sql.append("     where prc.customer_type_id is null  "); //(Loại 5)
		sql.append("     union all ");
		sql.append("     select  ");
		sql.append("          product_id,customer_id,customer_type_id,prc.shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append("         ,lstShop.lv, rank() over(PARTITION BY product_id ORDER BY lstShop.lv) rank_last ");
		sql.append("     from prc join lstShop on prc.shop_id = lstShop.shop_id ");
		sql.append("     where prc.customer_type_id = ?  ");//--(Loại 2)
		params.add(customerTypeId);
		sql.append(" ) ");
		sql.append(" ,prc_rec as( ");
		sql.append("     select  product_id,shop_id,lv, rank_last, from_date, to_date ,price_id,customer_id,customer_type_id,shop_type_id ");
		sql.append("     from prcX ");
		sql.append("     where rank_last = 1 ");
		sql.append(" ) ");
		sql.append(" ,result as ( ");
		sql.append("     select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append("     from prc_rec ");
		sql.append("     union all ");
		sql.append("     select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append("     from prc where customer_id = ? "); //--(Loại 1)
		params.add(customerId);
		sql.append("     union all ");
		sql.append("     select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append("     from prc where customer_type_id = ? and shop_id is null and (shop_type_id is null or shop_type_id = ?) ");//--(Loại 3 v?� 4)
		params.add(customerTypeId);
		params.add(shopTypeId);
		sql.append("     union all ");
		sql.append("     select  product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append("     from prc where customer_type_id is null and shop_id is null and shop_type_id = ? ");//--(Loại 6)
		params.add(shopTypeId);
		sql.append(" ) ");
		sql.append(" ,resultAll as ( ");
		sql.append("     SELECT     ");
		sql.append("         result.product_id, ");
		sql.append("         max(case when result.customer_id = ? then price_id end) pr_cus, ");
		params.add(customerId);
		sql.append("         max(case when result.customer_id is null and result.customer_type_id = ? ");
		params.add(customerTypeId);
		sql.append("           and result.shop_id is not null and result.shop_type_id is null  then price_id    end ");
		sql.append("         ) pr_cus_type_and_shop_id, ");
		sql.append("         max( ");
		sql.append("           case when result.customer_id is null and result.customer_type_id = ? ");
		params.add(customerTypeId);
		sql.append("           and result.shop_id is null and result.shop_type_id = 11 then price_id end ");
		sql.append("         ) pr_cus_type_and_shop_type, ");
		sql.append("         max( ");
		sql.append("           case when result.customer_id is null and result.customer_type_id = ?  ");
		params.add(customerTypeId);
		sql.append("           and result.shop_id is null and result.shop_type_id is null then price_id end ");
		sql.append("         ) pr_cus_type, ");
		sql.append("         max( ");
		sql.append("           case when result.customer_id is null and result.customer_type_id is null ");
		sql.append("           and result.shop_id is not null and result.shop_type_id is null then price_id end ");
		sql.append("         ) pr_shop_id, ");
		sql.append("         max( ");
		sql.append("           case when result.customer_id is null  and result.customer_type_id is null ");
		sql.append("           and result.shop_id  is null and result.shop_type_id =  ? then price_id     ");
		params.add(shopTypeId);
		sql.append("           end ");
		sql.append("         ) pr_shop_type ");
		sql.append("     FROM    result ");
		sql.append("     group by product_id ");
		sql.append(" ), ");
		sql.append(" priceTemp as ( ");
		sql.append("     select  ");
		sql.append("         product_id, ");
		sql.append("         (case when pr_cus is not null then pr_cus ");
		sql.append("         when pr_cus_type_and_shop_id is not null then pr_cus_type_and_shop_id ");
		sql.append("         when pr_cus_type_and_shop_type is not null then pr_cus_type_and_shop_type ");
		sql.append("         when pr_cus_type is not null then pr_cus_type ");
		sql.append("         when pr_shop_id is not null then pr_shop_id ");
		sql.append("         when pr_shop_type is not null then pr_shop_type ");
		sql.append("         else -1 ");
		sql.append("         end ) price_id ");
		sql.append("     from resultALL ");
		sql.append(" ) ");

		sql.append("SELECT p.product_id as productId, p.product_code as productCode, p.GROSS_WEIGHT as grossWeight, p.product_name as productName, ");
		sql.append(" p.convfact AS convfact , pr.price as price, pr.price_id as priceId, pr.vat as vat, ");
		sql.append(" st.quantity as stockQuantity, st.available_quantity as availableQuantity, pr.price_not_vat as priceNotVat  ");
		sql.append(" FROM product p left outer join ( ");
		//begin lay tong khoi luong sp theo nhieu kho
		sql.append("  SELECT SUM(st.quantity) as QUANTITY, SUM(st.available_quantity) available_quantity, st.product_id product_id ");
		sql.append("  FROM stock_total st ");
		sql.append("  LEFT JOIN warehouse wh ON (st.object_type = 1 AND wh.warehouse_id = st.warehouse_id AND wh.status = 1) ");
		sql.append("  WHERE 1=1 ");
		sql.append("    AND (st.object_type = 2 OR (st.object_type = 1 AND wh.warehouse_id is not null )) ");
		sql.append("  	AND st.object_id = ? AND st.status = 1 ");
		params.add(shopId);
		sql.append("    AND st.object_type = ? ");
		params.add(objectType);
		sql.append("  	AND st.product_id = ? ");
		params.add(productId);
		sql.append("  	group by st.product_id ");
		//end lay tong khoi luong sp theo nhieu kho

		sql.append(" ) st on st.product_id = p.product_id ");
		sql.append(" left outer join (");

		//begin lay gia theo nhieu gia
		/*
		 * sql.append("	SELECT pr.* FROM 	"); sql.append(
		 * "	(SELECT NVL(NVL(NVL(NVL(NVL(price_id1, price_id2), price_id3), price_id4), price_id5), price_id6) price_id	"
		 * ); sql.append("	FROM (	"); sql.append("		SELECT 	"); sql.append(
		 * "			MAX(CASE WHEN pr.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID AND pr.shop_id = ? THEN price_id END) price_id1, 	"
		 * ); params.add(shopId); // sql.append(
		 * "			MAX(CASE WHEN pr.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID AND pr.SHOP_CHANNEL = ? AND NVL(pr.shop_id, '') = '' THEN price_id END) price_id2,	"
		 * ); sql.append(
		 * "			MAX(CASE WHEN pr.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID  AND NVL(pr.shop_id, '') = '' THEN price_id END) price_id2,	"
		 * ); // params.add(shopChannel); // sql.append(
		 * "			MAX(CASE WHEN pr.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID AND NVL(pr.SHOP_CHANNEL, '') = '' AND NVL(pr.shop_id, '') = '' THEN price_id END) price_id3,	"
		 * ); sql.append(
		 * "			MAX(CASE WHEN pr.CUSTOMER_TYPE_ID = c.CUSTOMER_TYPE_ID  AND NVL(pr.shop_id, '') = '' THEN price_id END) price_id3,	"
		 * ); // sql.append(
		 * "			MAX(CASE WHEN NVL(pr.CUSTOMER_TYPE_ID, '') = '' AND NVL(pr.SHOP_CHANNEL, '') = '' AND pr.shop_id = ? THEN price_id END) price_id4, 	"
		 * ); sql.append(
		 * "			MAX(CASE WHEN NVL(pr.CUSTOMER_TYPE_ID, '') = ''  AND pr.shop_id = ? THEN price_id END) price_id4, 	"
		 * ); params.add(shopId); // sql.append(
		 * "			MAX(CASE WHEN NVL(pr.CUSTOMER_TYPE_ID, '') = '' AND pr.SHOP_CHANNEL = ? AND NVL(pr.shop_id, '') = '' THEN price_id END) price_id5, 	"
		 * ); sql.append(
		 * "			MAX(CASE WHEN NVL(pr.CUSTOMER_TYPE_ID, '') = '' AND NVL(pr.shop_id, '') = '' THEN price_id END) price_id5, 	"
		 * ); // params.add(shopChannel); // sql.append(
		 * "			MAX(CASE WHEN NVL(pr.CUSTOMER_TYPE_ID, '') = '' AND NVL(pr.SHOP_CHANNEL, '') = '' AND NVL(pr.shop_id, '') = '' THEN price_id END) price_id6	"
		 * ); sql.append(
		 * "			MAX(CASE WHEN NVL(pr.CUSTOMER_TYPE_ID, '') = ''  AND NVL(pr.shop_id, '') = '' THEN price_id END) price_id6	"
		 * ); sql.append(
		 * "		FROM PRICE pr JOIN (SELECT CHANNEL_TYPE_ID as CUSTOMER_TYPE_ID FROM CUSTOMER WHERE CUSTOMER_ID = ?) c ON NVL(c.CUSTOMER_TYPE_ID, '') <> ''	"
		 * ); params.add(customerId); sql.append("		WHERE 1 = 1	");
		 * sql.append("			AND pr.STATUS = 1	");
		 * sql.append("			AND (TRUNC(pr.from_date) <= TRUNC(?))	");
		 * params.add(orderDate);
		 * sql.append("			AND (TRUNC(pr.to_date) >= TRUNC(?))	");
		 * params.add(orderDate); sql.append("		GROUP BY pr.PRODUCT_ID	");
		 * sql.append("	)) pri_id, PRICE pr	"); sql.append("	WHERE 1=1	");
		 * sql.append("	and pri_id.price_id = pr.price_id	");
		 */

		sql.append("	select prTmp.*	");
		sql.append("	from priceTemp temp	");
		sql.append("	join price prTmp on temp.price_id = prTmp.price_id	");
		//end lay gia theo nhieu gia	

		sql.append(" ) pr ");
		sql.append(" on pr.product_id = p.product_id  ");
		sql.append(" WHERE p.product_id = ? ");
		params.add(productId);
		String[] fieldNames = new String[] { "productId", "productCode",// 1
				"grossWeight", // 3
				"productName", // 4
				"convfact",// 5
				"price",// 5
				"priceId", // 6
				"vat", //7
				"stockQuantity", //8
				"availableQuantity", //9
				"priceNotVat", //10
		};
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.FLOAT,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.BIG_DECIMAL,// 5
				StandardBasicTypes.LONG,// 6
				StandardBasicTypes.INTEGER,// 7
				StandardBasicTypes.INTEGER,// 8
				StandardBasicTypes.INTEGER,// 9
				StandardBasicTypes.BIG_DECIMAL,// 10
		};
		return repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ProductVO> getListProductForWarehouseExport(Integer status) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select ");
		sql.append(" pd.product_id as productId, ");
		sql.append(" case when (pd.convfact > 1) then ");
		sql.append(" (select ap_param_name from ap_param where ap_param_code = pd.uom2) ");
		sql.append(" when (pd.convfact <=1) then (select ap_param_name from ap_param where ap_param_code = pd.uom1) ");
		sql.append(" end as donvitinh, ");
		sql.append(" pd.product_name as productName, ");
		sql.append(" pd.product_code as productCode, ");
		sql.append(" pd.convfact as convfact, ");
		sql.append(" pr.price_not_vat as price ");
		sql.append(" from product pd ");
		sql.append(" left join mt_price pr on pr.product_id = pd.product_id ");
		sql.append(" where ");
		sql.append(" pd.status = 1 ");
		sql.append(" and pr.status = 1 ");
		sql.append(" and pr.price > 0 ");
		sql.append(" and pr.price_type = ? ");
		params.add(status);
		sql.append(" AND trunc(from_Date) =  (select trunc(max(from_date)) as maxdate from MT_PRICE where from_date <= trunc(sysdate) ");
		sql.append(" AND ( TO_DATE >= trunc(sysdate) or to_date is null) ");
		sql.append(" AND status = 1 AND PRICE_TYPE = ? AND product_id = pr.product_id group by product_id) ");
		params.add(status);

		sql.append(" order by productCode, productName ");

		//select count cho phan trang tren grid
		countSql.append(" select count(*) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");

		String[] fieldNames = { "productId", "productCode", "productName", "convfact", "price", "donvitinh" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(ProductVO.class, fieldNames, fieldTypes, sql.toString(), params);

	}

	/*************************** BEGIN HAM MOI TU HABECO *************************/
	/**
	 * Lay danh sach san pham theo ten (tim bang) productName, tru san pham
	 * exceptProductId
	 * 
	 * @author lacnv1
	 * @since Dec 28, 2013
	 */
	@Override
	public List<Product> getListProductsByName(String productName, Long exceptProductId) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(productName)) {
			throw new IllegalArgumentException("invalid name");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from product where lower(product_name)=?");
		params.add(productName.toLowerCase());

		if (exceptProductId != null) {
			sql.append(" and product_id <> ?");
			params.add(exceptProductId);
		}

		return repo.getListBySQL(Product.class, sql.toString(), params);
	}

	/**
	 * Lay danh sach san pham theo shortName(tim bang) shortName, tru san pham
	 * exceptProductId
	 * 
	 * @author haupv3
	 * @since Feb 10, 2014
	 */
	@Override
	public List<Product> getListProductsByShortName(String shortName, Long exceptProductId) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(shortName)) {
			throw new IllegalArgumentException("invalid name");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from product where lower(short_name)=?");
		params.add(shortName.toLowerCase());

		if (exceptProductId != null) {
			sql.append(" and product_id <> ?");
			params.add(exceptProductId);
		}

		return repo.getListBySQL(Product.class, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<ProductExportVO> getListProductExport(ProductFilter filter, KPaging<ProductExportVO> paging) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("invalid filter");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT p.product_id AS productId, ");
		sql.append(" p.product_code AS productCode, ");
		sql.append(" p.product_name AS productName, ");
		//sql.append(" p.short_name AS shortName, ");
		sql.append(" p.barcode AS barcode, ");
		sql.append(" cat.product_info_name AS categoryName, ");
		sql.append(" cat.product_info_code AS categoryCode, ");
		sql.append(" subcat.product_info_name AS subcatName,");
		sql.append(" subcat.product_info_code AS subcatCode,");
		sql.append(" brand.product_info_name AS brandName, ");
		sql.append(" brand.product_info_code AS brandCode, ");
		sql.append(" flavour.product_info_name AS flavourName,");
		sql.append(" flavour.product_info_code AS flavourCode,");
		sql.append(" packing.product_info_name AS packingName,");
		sql.append(" packing.product_info_code AS packingCode,");
		sql.append(" p.order_index as orderIndex,");
		sql.append(" ap1.ap_param_name AS uom1, ");
		sql.append(" ap2.ap_param_name AS uom2, ");
		sql.append(" p.convfact AS convfact, ");
		sql.append(" p.net_weight AS netWeight, ");
		sql.append(" p.gross_weight AS grossWeight, ");
		sql.append(" p.volumn AS volume, ");

		//		sql.append(" ( ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN p.expiry_type= 0 ");
		//		sql.append(" THEN 'Ng?�y' ");
		//		sql.append(" ELSE ( ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN p.expiry_type= 1 ");
		//		sql.append(" THEN 'Th?�ng' ");
		//		sql.append(" ELSE ( ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN p.expiry_type= 3 ");
		//		sql.append(" THEN 'Năm' ");
		//		sql.append(" ELSE '' ");
		//		sql.append(" END) ");
		//		sql.append(" END) ");
		//		sql.append(" END) AS expiryType, ");

		sql.append(" p.expiry_type AS expiryType, ");
		sql.append(" p.expiry_date AS expiryDate, ");
		sql.append(" ( ");
		sql.append(" CASE ");
		sql.append(" WHEN p.status= 1 ");
		sql.append(" THEN 'Hoạt động' ");
		sql.append(" ELSE 'Tạm ngưng' ");
		sql.append(" END) AS status, ");
		sql.append(" p.cat_id AS catId, ");
		sql.append(" p.sub_cat_id AS subCatId, ");
		sql.append(" p.brand_id AS brandId, ");
		sql.append(" p.check_Lot AS checkLot ");
		sql.append(" FROM product p ");
		sql.append(" left JOIN product_info cat ON (p.cat_id=cat.product_info_id and cat.status = 1 and cat.type = ?)");
		params.add(ProductType.CAT.getValue());
		sql.append(" LEFT JOIN product_info subcat ON (p.sub_cat_id=subcat.product_info_id and subcat.status = 1 and subcat.type = ?)");
		params.add(ProductType.SUB_CAT.getValue());
		sql.append(" LEFT JOIN product_info brand ON (p.brand_id=brand.product_info_id and brand.status = 1 and brand.type = ?) ");
		params.add(ProductType.BRAND.getValue());
		sql.append(" LEFT JOIN product_info flavour ON (p.flavour_id=flavour.product_info_id and flavour.status = 1 and flavour.type = ?)");
		params.add(ProductType.FLAVOUR.getValue());
		sql.append(" LEFT JOIN product_info packing ON (p.packing_id=packing.product_info_id and packing.status = 1 and packing.type = ?)");
		params.add(ProductType.PACKING.getValue());
		sql.append(" LEFT JOIN ap_param ap1 ");
		sql.append(" ON (p.uom1 = ap1.ap_param_code ");
		sql.append(" AND ap1.type = ?) ");
		params.add(ApParamType.UOM.getValue());
		sql.append(" LEFT JOIN ap_param ap2 ");
		sql.append(" ON (p.uom2 = ap2.ap_param_code ");
		sql.append(" AND ap2.type = ?) ");
		params.add(ApParamType.UOM.getValue());
		sql.append(" WHERE 1 =1 ");

		String productCode = filter.getProductCode();
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and p.product_code like ? escape '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase()));
		}

		String productName = filter.getProductName();
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" and (unicode2english(p.product_name) like ? escape '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(productName).toLowerCase()));
			sql.append(" or unicode2english(p.short_name) like ? escape '/') ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(productName).toLowerCase()));
		}

		List<Long> catIds = filter.getLstCategoryId();
		if (catIds != null && catIds.size() > 0) {
			Long id = catIds.get(0);
			sql.append(" and p.cat_id in (?");
			params.add(id);
			int n = catIds.size();
			for (int i = 1; i < n; i++) {
				id = catIds.get(i);
				sql.append(",?");
				params.add(id);
			}
			sql.append(")");
		}

		List<Long> subCatIds = filter.getLstSubCategoryId();
		if (subCatIds != null && subCatIds.size() > 0) {
			Long id = subCatIds.get(0);
			sql.append(" and p.SUB_CAT_ID in (?");
			params.add(id);
			int n = subCatIds.size();
			for (int i = 1; i < n; i++) {
				id = subCatIds.get(i);
				sql.append(",?");
				params.add(id);
			}
			sql.append(")");
		}

		Long brandId = filter.getBrandId();
		if (brandId != null) {
			sql.append(" and p.BRAND_ID = ?");
			params.add(brandId);
		}

		Long flavourId = filter.getFlavourId();
		if (flavourId != null) {
			sql.append(" and p.FLAVOUR_ID = ?");
			params.add(flavourId);
		}

		Long packingId = filter.getPackingId();
		if (packingId != null) {
			sql.append(" and p.PACKING_ID = ?");
			params.add(packingId);
		}

		ActiveType status = filter.getStatus();
		if (status != null) {
			sql.append(" and p.status= ? ");
			params.add(status.getValue().intValue());
		}

		String uom1 = filter.getUom1();
		if (!StringUtility.isNullOrEmpty(uom1)) {
			sql.append(" and upper(p.uom1) = ?");
			params.add(uom1.toUpperCase());
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) count from (");
		countSql.append(sql.toString());
		countSql.append(")");
		
		String fieldNames[] = new String[] { "productId", // 1
				"productCode", "productName",
				//"shortName",
				"barcode", // 5
				"categoryName", "categoryCode", "subcatName", "subcatCode", "brandName", "brandCode", "flavourName", "flavourCode", "packingName", //10
				"packingCode", "orderIndex", "uom1", "uom2", "convfact", "netWeight", // 15
				"grossWeight", "volume", "expiryType", "expiryDate", "status", // 20
				"catId", "subCatId", "brandId", "checkLot" // 24

		};

		String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			sql.append(" order by ").append(sort).append(" ");
			String order = StringUtility.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				sql.append(order);
			}
		} else {
			sql.append(" order by p.product_code ");
		}
		
		Type fieldTypes[] = new Type[] {
				StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				//StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, //10
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, //15
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.DOUBLE, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, //20
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER //24
		};

		if (paging != null) {
			return repo.getListByQueryAndScalarPaginated(ProductExportVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, paging);
		}
		return repo.getListByQueryAndScalar(ProductExportVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/*************************** BEGIN HAM MOI TU HABECO *************************/

	@Override
	public List<ImageVO> getListParentCatByCat(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct p.product_id as nvbhId , p.product_code as nvbhCode, p.product_name as nvbhName from product p ");
		sql.append(" where 1 = 1 ");
		sql.append(" and p.status = 1 ");
		sql.append(" and cat_id in ");
		sql.append(" ( ");
		sql.append(" select product_info_id ");
		sql.append(" from product_info ");
		sql.append(" where type = 1 and status = 1 ");
		sql.append(" ) ");
		sql.append(" and sub_cat_id in ");
		sql.append(" ( ");
		sql.append(" select product_info_id ");
		sql.append(" from product_info ");
		sql.append(" where type = 2 and status = 1 ");
		sql.append(" ) ");
		if (id != null && id > 0) {
			sql.append(" and p.cat_id = ? ");
			params.add(id);
		}
		sql.append(" order by nvbhCode, nvbhName ");

		String[] fieldNames = { "nvbhId",//1
				"nvbhCode",//2
				"nvbhName"//3
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING //2
		};
		return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ImageVO> getListParentCatBySubCat(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct p.product_id as nvbhId , p.product_code as nvbhCode, p.product_name as nvbhName from product p ");
		sql.append(" where 1 = 1 ");
		sql.append(" and p.status = 1 ");
		sql.append(" and cat_id in ");
		sql.append(" ( ");
		sql.append(" select product_info_id ");
		sql.append(" from product_info ");
		sql.append(" where type = 1 and status = 1 ");
		sql.append(" ) ");
		sql.append(" and sub_cat_id in ");
		sql.append(" ( ");
		sql.append(" select product_info_id ");
		sql.append(" from product_info ");
		sql.append(" where type = 2 and status = 1 ");
		sql.append(" ) ");
		if (id != null && id > 0) {
			sql.append(" and p.sub_cat_id = ? ");
			params.add(id);
		}
		sql.append(" order by nvbhId, nvbhCode, nvbhName ");

		String[] fieldNames = { "nvbhId",//1
				"nvbhCode",//2
				"nvbhName"//3
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING //2
		};
		return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public ProductInfo getProductInfoById(Long id) throws DataAccessException {
		return repo.getEntityById(ProductInfo.class, id);
	}

	@Override
	public List<ImageVO> getListParentCatByCatAndSubCat(Long idParentCat, Long idParentSubCat) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct p.product_id as nvbhId , p.product_code as nvbhCode, p.product_name as nvbhName from product p ");
		sql.append(" where 1 = 1 ");
		sql.append(" and p.status = 1 ");
		sql.append(" and cat_id in ");
		sql.append(" ( ");
		sql.append(" select product_info_id ");
		sql.append(" from product_info ");
		sql.append(" where type = 1 and status = 1 ");
		sql.append(" ) ");
		sql.append(" and sub_cat_id in ");
		sql.append(" ( ");
		sql.append(" select product_info_id ");
		sql.append(" from product_info ");
		sql.append(" where type = 2 and status = 1 ");
		sql.append(" ) ");
		if (idParentCat != null && idParentCat > 0) {
			sql.append(" and p.cat_id = ? ");
			params.add(idParentCat);
		}
		if (idParentSubCat != null && idParentSubCat > 0) {
			sql.append(" and p.SUB_CAT_ID = ? ");
			params.add(idParentSubCat);
		}
		sql.append(" order by nvbhCode, nvbhName ");

		String[] fieldNames = { "nvbhId",//1
				"nvbhCode",//2
				"nvbhName"//3
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING //2
		};
		return repo.getListByQueryAndScalar(ImageVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ProductInfo> getSubCatOnTree(Boolean b) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from product_info where status = 1 and type = 2 ");
		sql.append(" order by product_info_code ");
		return repo.getListBySQL(ProductInfo.class, sql.toString(), params);
	}
	
	@Override
	public List<Product> checkIfExistsProductByProductInfo(ProductType pdType, Long productInfoId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from product where 1=1 ");
		if(pdType.equals(ProductType.CAT)){
			sql.append(" and CAT_ID = ? ");
			params.add(productInfoId);
		}else if(pdType.equals(ProductType.SUB_CAT)){
			sql.append(" and SUB_CAT_ID = ?");
			params.add(productInfoId);
		}else if(pdType.equals(ProductType.FLAVOUR)){
			sql.append(" and FLAVOUR_ID = ? ");
			params.add(productInfoId);
		}else if(pdType.equals(ProductType.PACKING)){
			sql.append(" and PACKING_ID = ? ");
			params.add(productInfoId);
		}else if(pdType.equals(ProductType.BRAND)){
			sql.append(" and BRAND_ID = ? ");
			params.add(productInfoId);
		}		
		return repo.getListBySQL(Product.class, sql.toString(), params);

	}

	@Override
	public List<ProductExportVO> getProductWithDynamicAttributeInfo(ProductFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("search filter is null.");
		}
		StringBuilder sql = new StringBuilder();
		sql.append("call TUANND.PRODUCT_DYNAMIC_ATTRIBUTE(?, :productCode, :productName, :productStatus, :categoryIds, :subCategoryIds, :brandId, :flavourId, :packingId, :uom1, :sortField, :sortOrder)");

		List<Object> params = new ArrayList<Object>();
		int paramIndex = 2;
		params.add(paramIndex++);
		params.add(filter.getProductCode());
		params.add(java.sql.Types.VARCHAR);

		params.add(paramIndex++);
		params.add(filter.getProductName());
		params.add(java.sql.Types.VARCHAR);

		params.add(paramIndex++);
		params.add(filter.getStatus() != null ? filter.getStatus().getValue() : null);
		params.add(java.sql.Types.INTEGER);

		List<Long> categoryIds = filter.getLstCategoryId();
		StringBuilder categoryId = new StringBuilder();
		if (categoryIds != null && !categoryIds.isEmpty()) {
			for (Long catId : categoryIds) {
				categoryId.append(catId).append(",");
			}
			categoryId.append("-1");
		}
		params.add(paramIndex++);
		params.add(categoryId.toString());
		params.add(java.sql.Types.VARCHAR);

		List<Long> subCategoryIds = filter.getLstSubCategoryId();
		StringBuilder subCategoryId = new StringBuilder();
		if (subCategoryIds != null && !subCategoryIds.isEmpty()) {
			for (Long subCatId : subCategoryIds) {
				subCategoryId.append(subCatId).append(",");
			}
			subCategoryId.append("-1");
		}
		params.add(paramIndex++);
		params.add(subCategoryId.toString());
		params.add(java.sql.Types.VARCHAR);

		params.add(paramIndex++);
		params.add(filter.getBrandId());
		params.add(java.sql.Types.INTEGER);

		params.add(paramIndex++);
		params.add(filter.getFlavourId());
		params.add(java.sql.Types.INTEGER);

		params.add(paramIndex++);
		params.add(filter.getPackingId());
		params.add(java.sql.Types.INTEGER);

		params.add(paramIndex++);
		params.add(filter.getUom1());
		params.add(java.sql.Types.VARCHAR);

		params.add(paramIndex++);
		params.add(filter.getSort());
		params.add(java.sql.Types.VARCHAR);

		params.add(paramIndex++);
		params.add(filter.getOrder());
		params.add(java.sql.Types.VARCHAR);
		return repo.getListByQueryDynamicFromPackage(ProductExportVO.class, sql.toString(), params, null);
	}
	
	@Override
	public Product getProductByCodeNotByStatus(String code) throws DataAccessException {
		if (!StringUtility.isNullOrEmpty(code)) {
			code = code.toUpperCase().trim();
		}
		String sql = "select * from PRODUCT where product_code = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		Product rs = repo.getEntityBySQL(Product.class, sql, params);
		return rs;
	}
	
	@Override
	public List<ProductVO> getListProductVOByFilter(ProductGeneralFilter<ProductVO> filter)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select pd.product_id as productId ");
		sql.append(" , pdi.product_info_code as productInfoCode ");
		sql.append(" , pd.product_code as productCode ");
		sql.append(" , pd.product_name as productName ");
		sql.append(" , pd.status as productStatus ");
		sql.append(" , to_char(sysdate, 'dd/MM/yyyy') as dateSysStr ");
		sql.append(" from product pd  ");
		sql.append(" join product_info pdi on pdi.product_info_id = pd.cat_id ");
		sql.append(" where 1 = 1 ");
		if (filter.getStatus() != null) {
			sql.append(" and pd.status = ? ");
			params.add(filter.getStatus());
		} else if (filter.getArrStatus() != null && !filter.getArrStatus().isEmpty()) {
			sql.append(" and pd.status in (? ");
			params.add(filter.getArrStatus().get(0));
			for (int i = 1, size = filter.getArrStatus().size(); i < size; i++) {
				sql.append(" ,? ");
				params.add(filter.getArrStatus().get(i));
			}
			sql.append(" ) ");
		} else {
			sql.append(" and pd.status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getProductInforStatus() != null) {
			sql.append(" and pdi.status = ? ");
			params.add(filter.getProductInforStatus());
		}
		if (filter.getProductInforTypes() != null && !filter.getProductInforTypes().isEmpty()) {
			sql.append(" and pdi.type in (? ");
			params.add(filter.getProductInforTypes().get(0));
			for (int i = 1, size = filter.getProductInforTypes().size(); i < size; i++) {
				sql.append(" ,? ");
				params.add(filter.getArrStatus().get(i));
			}
			sql.append(" ) ");
		}
		if (filter.getProductCodes() != null && !filter.getProductCodes().isEmpty()) {
			sql.append(" and pd.product_code in (? ");
			params.add(filter.getProductCodes().get(0).trim().toUpperCase());
			for (int i = 1, size = filter.getProductCodes().size(); i < size; i++) {
				sql.append(" ,? ");
				params.add(filter.getProductCodes().get(i).trim().toUpperCase());	
			}
			sql.append(" ) ");
		}
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			sql.append(" and pd.product_code like ? ESCAPE '/' ");
			String nameTmp = Unicode2English.codau2khongdau(filter.getProductCode().trim());
			params.add(StringUtility.toOracleSearchLikeSuffix(nameTmp.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			sql.append(" and lower(pd.name_text) like ? ESCAPE '/' ");
			String nameTmp = Unicode2English.codau2khongdau(filter.getProductName().trim());
			params.add(StringUtility.toOracleSearchLikeSuffix(nameTmp.toLowerCase()));
		}
		if (filter.getProductIds() != null && !filter.getProductIds().isEmpty()) {
			sql.append(" and pd.product_id in (? ");
			params.add(filter.getProductIds().get(0));
			for (int i = 1, size = filter.getProductIds().size(); i < size; i++) {
				sql.append(" ,? ");
				params.add(filter.getProductIds().get(i));	
			}
			sql.append(" ) ");
		}
		if (filter.getProductInforIds() != null && !filter.getProductInforIds().isEmpty()) {
			sql.append(" and pdi.product_info_id in (? ");
			params.add(filter.getProductInforIds().get(0));
			for (int i = 1, size = filter.getProductInforIds().size(); i < size; i++) {
				sql.append(" ,? ");
				params.add(filter.getProductInforIds().get(i));	
			}
			sql.append(" ) ");
		}
		sql.append(" order by productInfoCode, productCode ");
		String[] fieldNames = {"productId", "productInfoCode", "productCode", "productName", "productStatus", "dateSysStr"};
		Type[] fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.STRING,StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING};
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(ProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		return repo.getListByQueryAndScalarPaginated(ProductVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.AttributeValueDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class AttributeValueDetailDAOImpl implements AttributeValueDetailDAO {
	@Autowired
	private IRepository repo;
	
	@Override
	public List<AttributeValueDetail> getListAttrValueDetailFromObj(Long objId, Long attributeId, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM ATTRIBUTE_VALUE_DETAIL ");
		if(null != status) {
			sql.append("WHERE STATUS = ? AND ATTRIBUTE_VALUE_ID IN (SELECT ATTRIBUTE_VALUE_ID FROM ATTRIBUTE_VALUE WHERE TABLE_ID = ? AND ATTRIBUTE_ID = ?)");
			params.add(status.getValue());
		} else {
			sql.append("WHERE ATTRIBUTE_VALUE_ID IN (SELECT ATTRIBUTE_VALUE_ID FROM ATTRIBUTE_VALUE WHERE TABLE_ID = ? AND ATTRIBUTE_ID = ?)");
		}
		params.add(objId);
		params.add(attributeId);
		return repo.getListBySQL(AttributeValueDetail.class, sql.toString(), params);
	}
	
	@Override
	public AttributeValueDetail getAttributeValueDetailByDetailAndValue(Long attrDetailId, Long attrValueId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from ATTRIBUTE_VALUE_DETAIL where ATTRIBUTE_DETAIL_ID = ? and ATTRIBUTE_VALUE_ID = ?");
		params.add(attrDetailId);
		params.add(attrValueId);
		return repo.getEntityBySQL(AttributeValueDetail.class, sql.toString(), params);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public AttributeValueDetail createAttributeValueDetail(AttributeValueDetail attributeValueDetail) throws DataAccessException {
		return repo.create(attributeValueDetail);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateAttributeValueDetail(AttributeValueDetail attributeValueDetail) throws DataAccessException {
		repo.update(attributeValueDetail);
	}
}

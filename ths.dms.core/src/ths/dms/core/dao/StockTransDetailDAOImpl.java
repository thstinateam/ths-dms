package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StockTransDetail;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.vo.ProductLotVO;
import ths.core.entities.vo.rpt.RptStockTransDetailVO;
import ths.core.entities.vo.rpt.RptStockTransInMoveVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class StockTransDetailDAOImpl implements StockTransDetailDAO {

	@Autowired
	private IRepository repo;

	@Override
	public StockTransDetail createStockTransDetail(StockTransDetail stockTransDetail) throws DataAccessException {
		if (stockTransDetail == null) {
			throw new IllegalArgumentException("stockTransDetail");
		}
		repo.create(stockTransDetail);
		return repo.getEntityById(StockTransDetail.class, stockTransDetail.getId());
	}

	@Override
	public void deleteStockTransDetail(StockTransDetail stockTransDetail) throws DataAccessException {
		if (stockTransDetail == null) {
			throw new IllegalArgumentException("stockTransDetail");
		}
		repo.delete(stockTransDetail);
	}

	@Override
	public StockTransDetail getStockTransDetailById(long id) throws DataAccessException {
		return repo.getEntityById(StockTransDetail.class, id);
	}

	@Override
	public void updateStockTransDetail(StockTransDetail stockTransDetail) throws DataAccessException {
		if (stockTransDetail == null) {
			throw new IllegalArgumentException("stockTransDetail");
		}
		repo.update(stockTransDetail);
	}

	@Override
	public Boolean isUsingByOthers(long stockTransDetailId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select count(1) as count from stock_trans_detail  ");
		sql.append(" where  ");
		sql.append("   exists (select 1 from stock_trans_lot where stock_trans_detail_id=?) ");

		List<Object> params = new ArrayList<Object>();
		params.add(stockTransDetailId);
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public List<StockTransDetail> getListStockTransDetailByStockTransId(long stockTransId) throws DataAccessException {
		String sql = "select * from stock_trans_detail where stock_trans_id=? order by STOCK_TRANS_DETAIL_ID desc";
		List<Object> params = new ArrayList<Object>();
		params.add(stockTransId);
		return repo.getListBySQL(StockTransDetail.class, sql, params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.StockTransDetailDAO#getListReportStockTransDetail
	 * (java.lang.Long, java.util.Date, java.util.Date)
	 */
	@Override
	public List<RptStockTransDetailVO> getListReportStockTransDetail(Long shopId, Date fromDate, Date toDate) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptStockTransDetailVO>();

		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.shop_code as shopCode,");
		sql.append(" s.shop_name as shopName,");
		sql.append(" s.address as address,");
		sql.append(" st.stock_trans_date as stockTransDate,");
		sql.append(" st.from_owner_type as fromOwnerType,");
		sql.append(" st.to_owner_type as toOwnerType,");
		sql.append(" p.product_code as productCode,");
		sql.append(" p.product_name as productName,");
		sql.append(" stl.lot as lot,");
		sql.append(" stl.quantity as quantity");
		sql.append(" from shop s, stock_trans st, stock_trans_detail std, product p, stock_trans_lot stl");
		sql.append(" where s.shop_id = st.shop_id ");
		sql.append(" and st.stock_trans_id = std.stock_trans_id");
		sql.append(" and std.product_id = p.product_id");
		sql.append(" and stl.stock_trans_detail_id = std.stock_trans_detail_id");
		sql.append(" and s.shop_id = ?");
		params.add(shopId);
		if (fromDate != null) {
			sql.append(" and st.stock_trans_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and st.stock_trans_date < (trunc(?) + 1)");
			params.add(toDate);
		}
		sql.append(" and (st.from_owner_type is null or st.to_owner_type is null)");
		String[] fieldNames = new String[] {//
		"shopCode",//1
				"shopName",//2
				"address",//3
				"stockTransDate",//4
				"fromOwnerType",//5
				"toOwnerType",//6
				"productCode",//7
				"productName",//8
				"lot",//9
				"quantity"//10
		};
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.TIMESTAMP,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.INTEGER,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 8
				StandardBasicTypes.STRING,// 9
				StandardBasicTypes.INTEGER // 10
		};
		return repo.getListByQueryAndScalar(RptStockTransDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public StockTransDetail getStockTransDetailForUpdate(Long stockTransDetailId, Long productId) throws DataAccessException {
		String sql = "select * from stock_trans_detail std where std.stock_trans_id = ? and std.product_id = ? for update";
		List<Object> params = new ArrayList<Object>();
		params.add(stockTransDetailId);
		params.add(productId);
		List<StockTransDetail> lst = repo.getListBySQL(StockTransDetail.class, sql, params);
		if (lst == null || lst.size() == 0)
			return null;
		else
			return lst.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.StockTransDetailDAO#getListStockTransDetailForVanSale
	 * (java.lang.Long, ths.dms.core.entities.enumtype.StockObjectType,
	 * java.lang.Long, ths.dms.core.entities.enumtype.StockObjectType,
	 * java.lang.Long)
	 */
	@Override
	public List<ProductLotVO> getListStockTransDetailForVanSale(Long shopId, StockObjectType shopType, Long ownerId, StockObjectType ownerType, Integer productId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select product_id as productId, lot as lot, sum(quantity) as quantity, expiry_date from (");
		sql.append(" select stl.product_id, stl.lot, stl.quantity, pl.expiry_date from stock_trans_lot stl, stock_trans st, product_lot pl");
		sql.append(" where st.stock_trans_id = stl.stock_trans_id and pl.product_id = stl.product_id and pl.lot = stl.lot and stl.product_id = ?");
		params.add(productId);
		sql.append(" and stl.quantity > 0 and st.from_owner_id = ? and st.from_owner_type = ? and st.to_owner_id = ? and st.to_owner_type = ?)");
		params.add(shopId);
		params.add(shopType.getValue());
		params.add(ownerId);
		params.add(ownerType.getValue());
		sql.append(" group by product_id, lot, expiry_date");
		sql.append(" order by expiry_date desc");
		String[] fieldNames = new String[] {//
		"productId",//1
				"lot",//2
				"quantity",//3
		};
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.INTEGER,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.INTEGER,// 3
		};
		return repo.getListByQueryAndScalar(ProductLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.StockTransDetailDAO#getListRptStrockTransInMove(
	 * java.lang.Long)
	 */
	@Override
	public List<RptStockTransInMoveVO> getListRptStrockTransInMove(Long stockTransId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT std.price price,");
		sql.append(" p.product_code productCode,");
		sql.append(" p.product_name productName,");
		sql.append(" p.convfact convfact,");
		sql.append(" p.uom2 uom2,");
		sql.append(" std.quantity quantity,");
		sql.append(" std.quantity * std.price AS toMoney");
		sql.append(" FROM stock_trans_detail std");
		sql.append(" JOIN product p");
		sql.append(" ON std.product_id        = p.product_id");
		sql.append(" WHERE std.stock_trans_id = ?");
		params.add(stockTransId);
		sql.append(" order by p.product_code");
		String[] fieldNames = new String[] {//
		"price",//1
				"productCode",//2
				"productName",//3
				"convfact",//4
				"uom2",//5
				"quantity",//6
				"toMoney",//7
		};
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.BIG_DECIMAL,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING,// 3
				StandardBasicTypes.INTEGER,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.INTEGER,// 6
				StandardBasicTypes.BIG_DECIMAL,// 7
		};
		return repo.getListByQueryAndScalar(RptStockTransInMoveVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

}

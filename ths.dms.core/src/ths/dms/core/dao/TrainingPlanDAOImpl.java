package ths.dms.core.dao;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

//import oracle.net.aso.p;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.TrainingPlan;
import ths.dms.core.entities.TrainingPlanDetail;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.TrainingPlanVO;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class TrainingPlanDAOImpl implements TrainingPlanDAO {

	@Autowired
	private IRepository repo;

	@Override
	public TrainingPlan createTrainingPlan(TrainingPlan trainingPlan) throws DataAccessException {
		if (trainingPlan == null) {
			throw new IllegalArgumentException("trainingPlan");
		}
		repo.create(trainingPlan);
		return repo.getEntityById(TrainingPlan.class, trainingPlan.getId());
	}

	@Override
	public void deleteTrainingPlan(TrainingPlan trainingPlan) throws DataAccessException {
		if (trainingPlan == null) {
			throw new IllegalArgumentException("trainingPlan");
		}
		repo.delete(trainingPlan);
	}

	@Override
	public TrainingPlan getTrainingPlanById(long id) throws DataAccessException {
		return repo.getEntityById(TrainingPlan.class, id);
	}

	@Override
	public void updateTrainingPlan(TrainingPlan trainingPlan) throws DataAccessException {
		if (trainingPlan == null) {
			throw new IllegalArgumentException("trainingPlan");
		}
		repo.update(trainingPlan);
	}

	@Override
	public List<TrainingPlanVO> getListTrainingPlan(KPaging<TrainingPlanVO> kPaging, Long shopId, Long nvgsId, Date date, Long shopRootId, Long staffRootId, boolean flagChildCMS) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId can be not null");
		}
		if (nvgsId == null) {
			throw new IllegalArgumentException("nvgsId can be not null");
		}
		StringBuilder sql = new StringBuilder();
		//StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();

		List<Object> params = new ArrayList<Object>();
		if (!flagChildCMS) {
			sql.append(" with  Rparentstaff as ( ");
			sql.append("  select psm.staff_id as staff_id from parent_staff_map psm where psm.status = 1 ");
			sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and psm.staff_id = parent_staff_id) ");
			sql.append("  start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id), ");
			params.add(staffRootId);
			sql.append("  MapCMSWithStaffNVBH as ( ");
			sql.append("  select st.staff_id from staff st join channel_type cn on st.staff_type_id = cn.channel_type_id left join Rparentstaff rs on rs.staff_id = st.staff_id ");
			sql.append(" where 1 = 1 ");
			sql.append("  and st.shop_id in (select shop_id from shop WHERE status = 1 start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
			params.add(shopRootId);
			sql.append("  and cn.status = 1 and cn.type = 2 and cn.object_type in (1, 2) ");
			sql.append("  ), ");
			sql.append("  MapCMSWithStaffGS as ( ");
			sql.append("  select distinct psm.parent_staff_id as staff_id from parent_staff_map psm join ");
			sql.append("  staff st on psm.parent_staff_id = st.staff_id join channel_type cn on st.staff_type_id = cn.channel_type_id ");
			sql.append("  where psm.status = 1 and cn.status = 1 and cn.type = 2 and cn.object_type in (5) ");
			sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and staff_id = parent_staff_id) ");
			sql.append("  and psm.staff_id in (select * from MapCMSWithStaffNVBH)) ");
		} else {
			sql.append(" with Rparentstaff as ( ");
			sql.append("  (select psm.staff_id as staff_id from parent_staff_map psm where psm.status = 1 ");
			sql.append("  and psm.staff_id not in (select staff_id from exception_user_access where status = 1  and psm.staff_id = parent_staff_id) ");
			sql.append("  start with psm.parent_staff_id = ? connect by prior psm.staff_id = psm.parent_staff_id) union (select psm.staff_id as staff_id from parent_staff_map psm where psm.staff_id = ?)), ");
			params.add(staffRootId);
			params.add(staffRootId);
			sql.append(" 	MapCMSWithStaffGS as ( ");
			sql.append(" 	  select st.staff_id from staff st join channel_type cn on st.staff_type_id = cn.channel_type_id join Rparentstaff rs on rs.staff_id = st.staff_id "); 
			sql.append(" 	  where 1 = 1 ");
			sql.append(" 	  and cn.status = 1 and cn.type = 2 and cn.object_type in (5)) ");
		}
		
		sql.append(" SELECT T_DT.TRAINING_PLAN_DETAIL_ID as id, ");
		sql.append(" (SELECT f.STAFF_CODE FROM STAFF f WHERE f.STAFF_ID=tp.STAFF_ID) as codeGSNPP, ");
		sql.append(" (SELECT f.STAFF_NAME FROM STAFF f WHERE f.STAFF_ID=tp.STAFF_ID) as nameGSNPP, ");
		sql.append(" (SELECT SP.SHOP_CODE FROM SHOP sp WHERE SP.SHOP_ID = T_DT.SHOP_ID) AS codeNPP, ");
		sql.append(" (SELECT f.STAFF_CODE FROM STAFF f WHERE f.STAFF_ID=T_DT.STAFF_ID) as codeNVBH, ");
		sql.append(" (SELECT f.STAFF_NAME FROM STAFF f WHERE f.STAFF_ID=T_DT.STAFF_ID) as nameNVBH, ");
		sql.append(" T_DT.TRAINING_DATE AS trainingDate, ");
		sql.append(" T_DT.TRAINING_PLAN_ID AS trainingPlanId ");
		
		sql.append(" FROM ");
		sql.append(" TRAINING_PLAN_DETAIL t_dt, TRAINING_PLAN tp , STAFF st ");
		sql.append(" WHERE ");
		sql.append(" T_DT.TRAINING_PLAN_ID=tp.TRAINING_PLAN_ID AND tp.STATUS=1 ");
		sql.append(" AND st.STAFF_ID=tp.STAFF_ID ");
		sql.append(" AND T_DT.STATUS in (1,0) ");
		sql.append(" and tp.STAFF_ID in (select * from MapCMSWithStaffGS) ");
		sql.append(" 	  and T_DT.SHOP_ID in (select shop_id from shop WHERE status = 1 start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
		params.add(shopRootId);
		
		if(shopId!=null && shopId>0){
			sql.append("and  T_DT.staff_id in ( select staff_id from staff where shop_id in (SELECT sh.SHOP_ID FROM SHOP sh ");
			sql.append("START WITH Sh.shop_id       = ? ");
			sql.append("CONNECT BY PRIOR Sh.SHOP_ID = Sh.PARENT_SHOP_ID");
			sql.append(" )) ");
			params.add(shopId);
			sql.append(" AND T_DT.SHOP_ID IN (SELECT sh.SHOP_ID FROM SHOP sh ");
			sql.append(" START WITH Sh.shop_id =? ");
			sql.append(" CONNECT BY PRIOR Sh.SHOP_ID = Sh.PARENT_SHOP_ID ");
			sql.append(" ) ");
			params.add(shopId);
		}
		if(nvgsId!=null && nvgsId>0){
			sql.append(" AND tp.STAFF_ID=? ");
			params.add(nvgsId);
		}
		if(date!=null){
			sql.append(" AND TRUNC(TP.MONTH,'YYYY')=TRUNC(?, 'YYYY') AND TRUNC(TP.MONTH,'MM')=TRUNC(?, 'MM') ");
			params.add(date);
			params.add(date);
		}
		
		sql.append(" ORDER BY codeNPP,codeGSNPP,codeNVBH,trainingDate ");
		countSql.append(" Select count(*) count from ( ").append(sql);
		countSql.append(" ) ");
		final String[] fieldNames = new String[] { "id", "codeGSNPP", //1
				"nameGSNPP", //2
				"codeNPP", //3
				"codeNVBH",//4
				"nameNVBH",//5
				"trainingDate",//6
				"trainingPlanId"//7
		};

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3
				StandardBasicTypes.STRING, //4
				StandardBasicTypes.STRING, //5
				StandardBasicTypes.DATE, //6
				StandardBasicTypes.LONG //7
		};
		List<Class<?>> synchronizedClass = new ArrayList<Class<?>>();
		synchronizedClass.add(TrainingPlan.class);
		synchronizedClass.add(TrainingPlanDetail.class);
		synchronizedClass.add(Shop.class);
		synchronizedClass.add(Staff.class);

		List<TrainingPlanVO> list = null;
		if (kPaging != null) {
			list = repo.getListByQueryAndScalarPaginated(TrainingPlanVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging, synchronizedClass);
		} else {
			list = repo.getListByQueryAndScalar(TrainingPlanVO.class, fieldNames, fieldTypes, sql.toString(), params, synchronizedClass);
		}
		return list;
	}

	@Override
	public TrainingPlan getTrainingPlanByOwnerIdAndDate(Long ownerId, Date date) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *");
		sql.append(" FROM training_plan tp");
		sql.append(" WHERE tp.staff_id = ?");
		params.add(ownerId);
		sql.append(" AND tp.month     >= TRUNC(?, 'MONTH')");
		params.add(date);
		sql.append(" AND tp.month      < TRUNC(?, 'MONTH') + interval '1' MONTH");
		params.add(date);
		sql.append(" AND tp.status     = ?");
		params.add(ActiveType.RUNNING.getValue());
		return repo.getFirstBySQL(TrainingPlan.class, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.TrainingPlanDAO#getTrainingPlanByOwnerCodeAndDate
	 * (java.lang.String, java.util.Date)
	 */
	@Override
	public TrainingPlan getTrainingPlanByOwnerCodeAndDate(String ownerCode, Long shopId, Date date) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(ownerCode)) {
			throw new IllegalArgumentException("ownerCode is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT tp.*");
		sql.append(" FROM training_plan tp, staff s");
		sql.append(" WHERE tp.staff_id = s.staff_id");
		/*
		 * sql.append(" AND tp.shop_id = ?"); params.add(shopId);
		 */
		sql.append(" AND s.staff_code = ?");
		params.add(ownerCode);
		sql.append(" AND tp.month     >= TRUNC(?, 'MONTH')");
		params.add(date);
		sql.append(" AND tp.month      < TRUNC(?, 'MONTH') + interval '1' MONTH");
		params.add(date);
		//		sql.append(" AND tp.status     = ?");
		//		params.add(ActiveType.RUNNING.getValue());
		return repo.getFirstBySQL(TrainingPlan.class, sql.toString(), params);
	}

	@Override
	public Integer getCountTrainingPlanDetailById(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		sql.append(" SELECT COUNT(*) count ");
		sql.append(" FROM TRAINING_PLAN_DETAIL df ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND df.TRAINING_PLAN_ID in (SELECT DFC.TRAINING_PLAN_ID FROM TRAINING_PLAN_DETAIL dfc WHERE DFC.TRAINING_PLAN_DETAIL_ID=?) ");
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		return repo.countBySQL(sql.toString(), params);
	}

	@Override
	public void deleteTrainingPlanByIdDetail(Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		sql.append(" DELETE FROM TRAINING_PLAN ");
		sql.append(" WHERE TRAINING_PLAN_ID=(SELECT DF.TRAINING_PLAN_ID ");
		sql.append(" FROM TRAINING_PLAN_DETAIL df ");
		sql.append(" WHERE DF.TRAINING_PLAN_DETAIL_ID=?) ");
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		repo.executeSQLQuery(sql.toString(), params);
	}
}

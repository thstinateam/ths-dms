package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockIssue;
import ths.dms.core.entities.StockIssueDetail;
import ths.dms.core.entities.StockLock;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTransType;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransVO;
import ths.core.entities.vo.rpt.RptBCXNKNVBHVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_Stock;
import ths.dms.core.exceptions.DataAccessException;

public interface StockTransDAO {

	StockTrans createStockTrans(StockTrans stockTrans) throws DataAccessException;

	void deleteStockTrans(StockTrans stockTrans) throws DataAccessException;

	void updateStockTrans(StockTrans stockTrans) throws DataAccessException;

	StockTrans getStockTransById(long id) throws DataAccessException;
	List<StockTrans> getListStockTransByFromId(Long fromId) throws DataAccessException;

	//String generateStockTransCode() throws DataAccessException;

	Boolean isUsingByOthers(long stockTransId) throws DataAccessException;

	StockTrans getStockTransByCode(String code) throws DataAccessException;
	
	StockTrans getStockTransByCodeAndShop(String code,Long shopId) throws DataAccessException;

	List<StockTrans> getListStockTrans(KPaging<StockTrans> kPaging, Date fromDate, Date toDate, Long shopId, StockTransType stockTransType) throws DataAccessException;

	List<StockTransVO> getListStockTransVO(KPaging<StockTransVO> kPaging, Date fromDate, Date toDate, Long shopId, StockTransType stockTransType) throws DataAccessException;

	List<StockTrans> getListStockTransWithCondition(KPaging<StockTrans> kPaging, StockObjectType fromOwnerType, StockObjectType toOwnerType, Long staffId, Date stockTransDate, Long fromOwnerId, Long toOwnerId) throws DataAccessException;

	List<RptBCXNKNVBHVO> getBCXNKNVBH(Long shopId, List<Long> lstStaffId, Date fromDate, Date toDate) throws DataAccessException;

	List<StockTrans> getListStockTrans(String staffCode, Long shopID);

	List<Staff> getListStaffTrans(Long shopId);

	String generateStockTransCode(Long shopId) throws DataAccessException;

	String generateStockTransCodeVansale(Long shopId, String transType) throws DataAccessException;

	int checkStockTrans(Long shopId, Date lockDate) throws DataAccessException;

	int checkStockTransVansale(Long shopId) throws DataAccessException;

	String getStockTransNotYetReturn(Long shopId) throws DataAccessException;

	/**
	 * phieu xuat kho kiem van chuyen noi bo
	 * 
	 * @author vuonghn
	 */
	RptPXKKVCNB_Stock getPXKKVCNB(Long shopId, String staffCode, Long stockTransId, String transacName, String transacContent) throws DataAccessException;

	List<StockTrans> getListStockTrans(List<Long> lstId) throws DataAccessException;

	/**
	 * Lay ds NV co giao dich xuat kho nhan vien trong ngay
	 * 
	 * @author lacnv1
	 * @since Sep 12, 2014
	 */
	String getListStockTransStaffId(long shopId, Date date) throws DataAccessException;

	/**
	 * Lay ds NV chua chot ngay
	 * 
	 * @author lacnv1
	 * @since Sep 12, 2014
	 */
	String getListStaffLockStockYet(String lstStaffId, Date date) throws DataAccessException;

	/**
	 * Lay ds NV co ton kho goc nhin ke toan khac 0
	 * 
	 * @author lacnv1
	 * @since Sep 12, 2014
	 */
	String getListStaffStockReturnYet(String lstStaffId) throws DataAccessException;

	/**
	 * Lay ds sp theo don vansale DP cua NV
	 * 
	 * @author lacnv1
	 * @since Sep 25, 2014
	 */
	List<StockTotalVO> getListProductDPByStaff(long shopId, long staffId) throws DataAccessException;
	
	/**
	 * Lay ds don vansale DP cua NV
	 * 
	 * @author tungmt
	 * @since 19/01/2015
	 */
	List<StockTransVO> getListDPByStaff(StockStransFilter filter) throws DataAccessException;

	/**
	 * Lay ds NV co ton kho goc nhin ke toan khac 0 (theo npp)
	 * 
	 * @author lacnv1
	 * @since Oct 15, 2014
	 */
	String getListStaffStockReturnYet(long shopId) throws DataAccessException;

	/**
	 * Kiem tra NV da co don GO trong ngay chua
	 * 
	 * @author lacnv1
	 * @since Nov 06, 2014
	 */
	boolean checkExistsGOStockTrans(long staffId, Date pDate) throws DataAccessException;
	
	/**
	 * Kiem tra NV co don GO chua duyet
	 * 
	 * @author lacnv1
	 * @since Nov 06, 2014
	 */
	boolean checkExistsGOStockTransNotApproves(long staffId, String type) throws DataAccessException;


	/**
	 * 
	 * @param type
	 * @return
	 * @throws DataAccessException
	 */
	String getMaxMTStockTranCode(String type) throws DataAccessException;


	/**
	 * Lay ds quan ly kho
	 * 
	 * @author hoanv25
	 * @since Dec 11, 2014
	 */
	List<StockTransVO> getListStockTransVOFilter(StockStransFilter filter) throws DataAccessException;

	/**
	 * Tao moi Stock Issue
	 * 
	 * @author hunglm16
	 * @since December 15,2014
	 * */
	StockIssue createStockIssue(StockIssue stockIssue) throws DataAccessException;

	/**
	 * Tao moi Stock Issue Detail
	 * 
	 * @author hunglm16
	 * @since December 15,2014
	 * */
	StockIssueDetail createStockIssueDetail(StockIssueDetail stockIssueDetail) throws DataAccessException;
	
	/**
	 *Tao moi kho nhan vien ban hang vansale
	 * @author hoanv25
	 * @sine 28/12/2014
	 */
	StockLock createStockTransLock(StockLock stl) throws DataAccessException;
}

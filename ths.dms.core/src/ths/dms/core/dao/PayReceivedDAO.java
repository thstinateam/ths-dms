package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.PayReceived;
import ths.dms.core.entities.PayReceivedTemp;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.PayReceivedFilter;
import ths.dms.core.entities.vo.CustomerDebitVO;
import ths.dms.core.entities.vo.PayReceivedVO;
import ths.core.entities.vo.rpt.RptCustomerPayReceivedVO;
import ths.dms.core.exceptions.DataAccessException;

public interface PayReceivedDAO {
	void deletePayReceived(PayReceived payReceived) throws DataAccessException;

	void updatePayReceived(PayReceived payReceived) throws DataAccessException;
	
	PayReceived getPayReceivedById(long id) throws DataAccessException;

	PayReceived createPayReceived(PayReceived payReceived) throws DataAccessException;

	List<RptCustomerPayReceivedVO> getListRptCustomerPayReceivedVO(
			KPaging<RptCustomerPayReceivedVO> kPaging, Long shopId,
			Date fromPayDate, Date toPayDate, Long customerId)
			throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @return
	 * @throws DataAccessException
	 */
	Integer getCountOfPayReceivedInSysdate() throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @throws DataAccessException 
	 */
	String getPayReceivedStringSugguest(Date lockDate) throws DataAccessException;

	void updatePayReceivedWithoutUpdateDate(PayReceived payReceived)
			throws DataAccessException;

	/**
	 * Lay phieu thanh toan pay_received theo so chung tu
	 * 
	 * @param payreceivedNumber - so chung tu
	 * @param shopId - id npp
	 * 
	 * @author lacnv1
	 * @since May 20, 2015
	 */
	PayReceived getPayReceivedByNumber(String payreceivedNumber, Long shopId) throws DataAccessException;
	
	/**
	 * copy pay_received sang pay_received_temp
	 * 
	 * @author lacnv1
	 * @since Nov 20, 2014
	 */
	PayReceivedTemp clonePayReceivedToTemp(PayReceived payReceived) throws DataAccessException;
	
	/**
	 * Lay danh sach pay_received theo filter
	 * 
	 * @author lacnv1
	 * @since Mar 19, 2015
	 */
	List<PayReceivedVO> getListPayReceivedVO(PayReceivedFilter<PayReceivedVO> filter) throws DataAccessException;
	
	/**
	 * Lay thong tin pay_received theo pay_received_number
	 * 
	 * @author lacnv1
	 * @since Mar 20, 2015
	 */
	PayReceivedVO getPayReceivedVO(PayReceivedFilter<PayReceivedVO> filter) throws DataAccessException;

	/**
	 * lay payReceiveTemp
	 * @author nhutnn
	 * @since 24/03/2015
	 */
	PayReceivedTemp getPayReceivedTempByNumber(String code, Long shopId) throws DataAccessException;
	void updatePayReceivedTemp(PayReceivedTemp payReceivedTemp) throws DataAccessException;
	
	/**
	 * Lay thong tin pay_received theo id de xuat phieu
	 * 
	 * @author lacnv1
	 * @since Mar 20, 2015
	 */
	PayReceivedVO getPayReceivedVOForExport(long payId) throws DataAccessException;
	
	/**
	 * Lay thong tin chi tiet phieu xuat
	 * 
	 * @author lacnv1
	 * @since Mar 24, 2015
	 */
	List<CustomerDebitVO> getCustomerDebitOfPayReceived(long shopId, long payId) throws DataAccessException;
	
	/**
	 * check thanh toan co phai cua KH khong
	 * @author nhutnn
	 * @since 25/03/2015
	 */
	Integer checkOwnerTypeCustomerPayReceived(Long payReceivedId) throws DataAccessException;
	
	/**
	 * lay pay_received 
	 * @author nhutnn
	 * @since 27/03/2015
	 */
	PayReceived getPayReceivedByFilter(PayReceivedFilter<PayReceivedVO> filter) throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.EquipRepairPayForm;
import ths.dms.core.entities.EquipRepairPayFormDtl;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.filter.EquipRepairFilter;
import ths.dms.core.entities.vo.EquipRepairFormVO;
import ths.dms.core.entities.vo.EquipRepairPayFormVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class EquipmentRepairPayFormDAOImpl implements EquipmentRepairPayFormDAO {
	@Autowired
	private IRepository repo;

	/**
	 * @author vuongmq
	 * @date 22/06/2015
	 * @description Danh sach search Quan ly thanh toan
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public List<EquipRepairPayFormVO> getListEquipRepairPayForm(EquipRepairFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT erp.equip_repair_pay_form_id as id, ");
		sql.append(" erp.code as maPhieu, ");
		sql.append(" to_char(erp.pay_date, 'dd/mm/yyyy') as ngayLap, ");
		sql.append(" erp.status as trangThai, ");
		sql.append(" erp.total_amount as tongTien ");
		sql.append(" FROM equip_repair_pay_form erp ");
		sql.append(" where 1 = 1 ");
		if (filter.getShopRoot() != null) {
			sql.append(" and erp.shop_id in ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ");
			params.add(filter.getShopRoot());
		}
		if (StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "erp.shop_id");
			sql.append(paramsFix);
		}
		if (!StringUtility.isNullOrEmpty(filter.getFormCode())) {
			sql.append(" and erp.code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getFormCode().trim().toUpperCase()));
		}
		if (filter.getStatus() != null && filter.getStatus() > ActiveType.DELETED.getValue()) {
			sql.append("  and erp.status = ? ");
			params.add(filter.getStatus());
		}
		if (filter.getFromDate() != null) {
			sql.append("  and erp.pay_date = trunc(?) ");
			params.add(filter.getFromDate());
		}
		// vuongmq; 02/07/2015; not in khong lay status is null; de du lieu chinh xac
		sql.append("  and erp.status is not null ");
		
		sql.append("  order by erp.code desc ");
		String[] fieldNames = { "id", 
								"maPhieu",
								"ngayLap",
								"trangThai",
								"tongTien"};
		Type[] fieldTypes = { StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL};
		if (filter.getkPagingPayFormVO() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from ( ");
			countSql.append(sql);
			countSql.append(" ) ");
			return repo.getListByQueryAndScalarPaginated(EquipRepairPayFormVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingPayFormVO());
		} else {
			return repo.getListByQueryAndScalar(EquipRepairPayFormVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	/**
	 * @author vuongmq
	 * @date 22/06/2015
	 * @description Danh sach search Quan ly thanh toan Detail
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public List<EquipRepairPayFormVO> getListEquipRepairPayFormDtlById(EquipRepairFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT r.equip_repair_form_id as id, ");
		sql.append(" r.equip_repair_form_code as maPhieu, ");
		sql.append(" to_char(r.create_date,'dd/mm/yyyy') as ngayLap, ");
		sql.append(" pdt.total_actual_amount as tongTien ");
		sql.append(" from equip_repair_pay_form_dtl pdt ");
		sql.append(" join equip_repair_form r on r.equip_repair_form_id = pdt.equip_repair_form_id ");
		sql.append(" where 1 = 1 ");
		if (filter.getId() != null) {
			sql.append(" and pdt.equip_repair_pay_form_id = ? ");
			params.add(filter.getId());
		}
		sql.append("  order by maPhieu desc ");
		// id luc nay la: id phieu sua chua
		String[] fieldNames = { "id", 
								"maPhieu",
								"ngayLap",
								//"trangThai",
								"tongTien"};
		Type[] fieldTypes = { StandardBasicTypes.LONG, 
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				//StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL};
		if (filter.getkPagingPayFormVO() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from ( ");
			countSql.append(sql);
			countSql.append(" ) ");
			return repo.getListByQueryAndScalarPaginated(EquipRepairPayFormVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingPayFormVO());
		} else {
			return repo.getListByQueryAndScalar(EquipRepairPayFormVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
	
	/**
	 * Lay danh sach chi tiet cua phieu thanh toan;
	 * @author vuongmq
	 * @since 24/06/2015
	 * @return
	 */
	@Override
	public List<EquipRepairPayFormDtl> getEquipmentRepairPaymentDtlFilter(EquipRepairFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_repair_pay_form_dtl erpfdl ");
		sql.append(" where 1=1 ");
		if (filter.getId() != null) {
			sql.append(" and erpfdl.equip_repair_pay_form_id = ? ");
			params.add(filter.getId());
		}
		if (filter.getEquipRepairId() != null) {
			sql.append(" and erpfdl.equip_repair_form_id = ? ");
			params.add(filter.getEquipRepairId());
		}
		return repo.getListBySQL(EquipRepairPayFormDtl.class, sql.toString(), params);
	}
	
	/***
	 * @author vuongmq
	 * @date 23/06/2015
	 * phieu thanh toan theo entities Id
	 */
	@Override
	public EquipRepairPayForm retrieveEquipmentRepairPayment(Long equipmentRepairPayFormId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from equip_repair_pay_form where equip_repair_pay_form_id = ? ");
		params.add(equipmentRepairPayFormId);
		return repo.getEntityBySQL(EquipRepairPayForm.class, sql.toString(), params);
	}
	
	/***
	 * @author vuongmq
	 * @date 23/06/2015
	 * danh sach cua phieu thanh toan theo entities
	 */
	@Override
	public List<EquipRepairPayForm> retrieveListEquipmentRepairPayment(EquipRepairFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from equip_repair_pay_form f ");
		sql.append(" where 1=1 ");
		if (filter.getId() != null) {
			sql.append(" and f.equip_repair_pay_form_id = ? ");
			params.add(filter.getId());
		}
		if (filter.getLstId() != null) {
			sql.append(" and f.equip_repair_pay_form_id in (-1 ");
			for (Long id : filter.getLstId()) {
				if (id != null) {
					sql.append(" ,?");
					params.add(id);
				}
			}
			sql.append(" )");
		}
		return repo.getListBySQL(EquipRepairPayForm.class, sql.toString(), params);
	}
	
	/***
	 * @author vuongmq
	 * @date 23/06/2015
	 * danh sach phieu sua chua tren popup
	 */
	@Override
	public List<EquipRepairFormVO> getListEquipRepairPopup(EquipRepairFilter filter) throws DataAccessException {
		if (filter.getShopRoot() == null) {
			throw new IllegalArgumentException("shopRoot is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/** select VO **/
		sql.append(" SELECT f.equip_repair_form_id id, ");
		sql.append(" (CASE WHEN f.stock_type = 1 ");
		sql.append(" THEN ");
		sql.append(" (SELECT sh.shop_code || '-' || sh.shop_name ");
		sql.append(" FROM equip_stock es ");
		sql.append(" JOIN shop sh ON sh.shop_id = es.shop_id ");
		sql.append(" WHERE es.equip_stock_id = f.stock_id ");
		sql.append(" ) ");
		sql.append(" ELSE ");
		sql.append(" (SELECT sh.shop_code || '-' || sh.shop_name ");
		sql.append(" FROM customer c ");
		sql.append(" JOIN shop sh ON sh.shop_id = c.shop_id ");
		sql.append(" WHERE c.customer_id = f.stock_id ) ");
		sql.append(" END ) AS donVi, ");
		sql.append(" f.equip_repair_form_code AS maPhieu, ");
		sql.append(" ec.name AS loaiThietBi, ");
		sql.append(" (eg.code ||'-' ||eg.name) AS nhomThietBi, ");
		sql.append(" e.code AS maThietBi, ");
		sql.append(" e.serial AS soSeri, ");
		sql.append(" TO_CHAR(f.create_date,'dd/mm/yyyy') ngayTao, ");
		sql.append(" ( CASE WHEN f.stock_type = 2 ");
		sql.append(" THEN ");
		sql.append(" (SELECT (cus.short_code || '-' || cus.customer_name) ");
		sql.append(" FROM customer cus ");
		sql.append(" WHERE cus.customer_id = f.stock_id ");
		sql.append(" ) ");
		sql.append(" ELSE NULL ");
		sql.append(" END ) khachHang, ");
		sql.append(" ( CASE WHEN f.stock_type = 2 ");
		sql.append(" THEN ");
		sql.append(" (SELECT address ");
		sql.append(" FROM customer ");
		sql.append(" WHERE customer.customer_id = f.stock_id ) ");
		sql.append(" ELSE ");
		sql.append(" (SELECT address ");
		sql.append(" FROM shop sh ");
		sql.append(" WHERE sh.shop_id = (SELECT es.shop_id ");
		sql.append(" FROM equip_stock es ");
		sql.append(" WHERE es.equip_stock_id = f.stock_id) ");
		sql.append(" ) ");
		sql.append(" END ) diaChi, ");
		sql.append(" f.condition AS tinhTrangHuHong, ");
		sql.append(" f.reason lyDoDeNghi, ");
		sql.append(" f.total_amount tongTien, ");
		sql.append(" f.reject_reason lyDoTuChoi, ");
		sql.append(" e.manufacturing_year namSanXuat ");
		/** lay table from **/
		sql.append(" from  equip_repair_form f");
		sql.append(" join equipment e on f.equip_id = e.equip_id ");
		sql.append(" join equip_group eg on eg.equip_group_id = e.equip_group_id");
		sql.append(" join equip_category ec on ec.equip_category_id = eg.equip_category_id");
		sql.append(" where 1=1 "); 
		
		if (!StringUtility.isNullOrEmpty(filter.getFormCode())) {
			sql.append(" and f.equip_repair_form_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getFormCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getEuqipCode())) {
			sql.append(" and upper(e.code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getEuqipCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getSeri())) {
			sql.append(" and upper(e.serial) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSeri().toUpperCase()));
		}
		if (filter.getFromDate() != null) {
			sql.append(" and f.create_date >=trunc(?)  ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and f.create_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		/** vuongmq; 23/06/2015; cap nhat lay trang thai va trang thai thanh toan; */
		if (filter.getStatus() != null) {
			sql.append(" and f.status = ? ");
			params.add(filter.getStatus());
		}
		if (filter.getStatusPayment() != null) {
			sql.append(" and f.payment_status = ? ");
			params.add(filter.getStatusPayment());
		}
		// lay danh sach phieu sua chua not in ds getLstId duoi gridRepairItem
		if (filter.getLstId() != null) {
			sql.append(" and f.equip_repair_form_id not in (-1 ");
			for (Long id : filter.getLstId()) {
				if (id != null) {
					sql.append(" ,?");
					params.add(id);
				}
			}
			sql.append(" )");
		}
		/** lay stock_type all**/
		if (filter.getShopRoot() != null) {
			sql.append(" AND ( (f.stock_id IN ");
			sql.append(" (SELECT equip_stock_id ");
			sql.append(" FROM equip_stock ");
			sql.append(" WHERE shop_id IN ");
			sql.append(" (SELECT sh.shop_id ");
			sql.append(" FROM shop sh ");
			sql.append(" where sh.status = 1 ");
			if (StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
				String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "sh.shop_id");
				sql.append(paramsFix);
			}
			sql.append(" START WITH sh.shop_id = ? ");
			params.add(filter.getShopRoot());
			sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ) ");
			// vuongmq; 23/06/2015; tim kiem popup theo shopCode
			if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
				sql.append(" and shop_id in (select sh.shop_id from shop sh where sh.shop_code like ? ESCAPE '/' ) ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().toUpperCase()));
			}
			sql.append(" ) ");
			
			sql.append(" and f.stock_type = 1 ");
			sql.append(" ) ");
			/*** end kho NPP */
			sql.append(" OR (f.stock_id IN ");
			sql.append(" (SELECT cus.customer_id ");
			sql.append(" FROM customer cus ");
			sql.append(" WHERE cus.shop_id IN ");
			sql.append(" (SELECT sh.shop_id ");
			sql.append(" FROM shop sh ");
			sql.append(" where sh.status = 1 ");
			if (StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
				String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "sh.shop_id");
				sql.append(paramsFix);
			}
			sql.append(" START WITH sh.shop_id = ? ");
			params.add(filter.getShopRoot());
			sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ) ");
			// vuongmq; 23/06/2015; tim kiem popup theo shopCode
			if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
				sql.append(" and cus.shop_id in (select sh.shop_id from shop sh where sh.shop_code like ? ESCAPE '/' ) ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().toUpperCase()));
			}
			sql.append(" ) ");
			
			sql.append(" AND f.stock_type = 2) ");
			sql.append(" ) ");
			/*** end kho KH */
		}
		/** seach danh sach kho cua KH theo shop Id; connect by lay theo shop*/
		if (filter.getShopRoot() != null
				&& (!StringUtility.isNullOrEmpty(filter.getCustomerCode()) || !StringUtility.isNullOrEmpty(filter.getCustomerName()) ) ) {
			/** --------- tim kiem MAKH va ten KH */
			sql.append(" AND (f.stock_id IN ");
				sql.append(" (SELECT cus.customer_id ");
				sql.append(" FROM customer cus ");
				sql.append(" WHERE cus.shop_id IN ");
				sql.append(" (SELECT sh.shop_id ");
				sql.append(" FROM shop sh ");
				sql.append(" where sh.status = 1 ");
				if (StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
					String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "sh.shop_id");
					sql.append(paramsFix);
				}
				sql.append(" START WITH sh.shop_id = ? ");
				params.add(filter.getShopRoot());
				sql.append(" CONNECT BY prior sh.shop_id = sh.parent_shop_id ) ");
				if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
					sql.append(" and upper(cus.short_code) like ? ESCAPE '/' ");
					params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().toUpperCase()));
				}
				if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
					sql.append(" and upper(cus.name_text) like ? ESCAPE '/' ");
					params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.removeDau(filter.getCustomerName().toUpperCase())));
				}
				sql.append(" ) ");
			sql.append(" AND f.stock_type = 2) ");
		}
		sql.append(" order by donVi, maPhieu desc ");

		String[] fieldNames = new String[] { "id", //1
				"donVi", "maPhieu", "loaiThietBi", "nhomThietBi", "maThietBi", "soSeri", "ngayTao",//8 
				"khachHang", "diaChi", "tinhTrangHuHong", "lyDoDeNghi", "tongTien", //12
				"lyDoTuChoi", "namSanXuat" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,//8 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, //12
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };
		if (filter.getkPaging() == null) {
			return repo.getListByQueryAndScalar(EquipRepairFormVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(EquipRepairFormVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	
	/**
	 * @author vuongmq
	 * @date 29/06/2015
	 * @description Danh sach Quan ly thanh toan Xuat file excel theo dieu kien check chon danh sach lstId
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public List<EquipRepairPayFormVO> exportRepairPayForm(EquipRepairFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT erp.equip_repair_pay_form_id as id, ");
		sql.append(" erp.code as maPhieu, ");
		sql.append(" to_char(erp.pay_date, 'dd/mm/yyyy') as ngayLap, ");
		sql.append(" erp.status as trangThai, ");
		sql.append(" erp.total_amount as tongTien, ");
		sql.append(" r.equip_repair_form_code as maPhieuSuaChua, ");
		sql.append(" to_char(r.create_date,'dd/mm/yyyy') as ngayTao, ");
		sql.append(" pdt.total_actual_amount as tongTienSuaChua ");
		sql.append(" FROM equip_repair_pay_form erp ");
		sql.append(" JOIN equip_repair_pay_form_dtl pdt on pdt.equip_repair_pay_form_id = erp.equip_repair_pay_form_id ");
		sql.append(" JOIN equip_repair_form r on r.equip_repair_form_id = pdt.equip_repair_form_id ");
		sql.append(" where 1 = 1 ");
		// lay danh sach theo phan quyen thanh toan tu shopRoot tro xuong
		if (filter.getShopRoot() != null) {
			sql.append(" and erp.shop_id in ");
			sql.append(" (SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" WHERE status = 1 ");
			sql.append(" START WITH shop_id IN (?) ");
			sql.append(" AND status = 1 ");
			sql.append(" CONNECT BY prior shop_id = parent_shop_id) ");
			params.add(filter.getShopRoot());
		}
		// lay danh sach phieu sua chua not in ds getLstId duoi gridRepairItem
		if (filter.getLstId() != null) {
			sql.append(" and erp.equip_repair_pay_form_id in (-1 ");
			for (Long id : filter.getLstId()) {
				if (id != null) {
					sql.append(" ,?");
					params.add(id);
				}
			}
			sql.append(" )");
		}
		sql.append("  order by erp.code desc ");
		String[] fieldNames = { "id",   //1 
								"maPhieu",  //2
								"ngayLap",   //3
								"trangThai",  //4
								"tongTien",    //5
								"maPhieuSuaChua",  //6
								"ngayTao",   //7
								"tongTienSuaChua"  //8
								};
		Type[] fieldTypes = { StandardBasicTypes.LONG, //1 
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.STRING, //3 
				StandardBasicTypes.INTEGER,  //4
				StandardBasicTypes.BIG_DECIMAL,  //5
				StandardBasicTypes.STRING,  //6
				StandardBasicTypes.STRING,  //7
				StandardBasicTypes.BIG_DECIMAL //8
				};
		if (filter.getkPagingPayFormVO() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from ( ");
			countSql.append(sql);
			countSql.append(" ) ");
			return repo.getListByQueryAndScalarPaginated(EquipRepairPayFormVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingPayFormVO());
		} else {
			return repo.getListByQueryAndScalar(EquipRepairPayFormVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ths.dms.core.entities.DisplayToolsProduct;
import ths.dms.core.entities.Product;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class DisplayToolsProductDAOImpl implements DisplayToolsProductDAO {

	@Autowired
	private IRepository repo;

	@Override
	public DisplayToolsProduct createDisplayToolsProduct(DisplayToolsProduct displayToolsProduct) throws DataAccessException {
		if (displayToolsProduct == null) {
			throw new IllegalArgumentException("displayToolsProduct");
		}
		repo.create(displayToolsProduct);
		return repo.getEntityById(DisplayToolsProduct.class, displayToolsProduct.getId());
	}

	@Override
	public void deleteDisplayToolsProduct(DisplayToolsProduct displayToolsProduct) throws DataAccessException {
		if (displayToolsProduct == null) {
			throw new IllegalArgumentException("displayToolsProduct");
		}
		repo.delete(displayToolsProduct);
	}

	@Override
	public DisplayToolsProduct getDisplayToolsProductById(long id) throws DataAccessException {
		return repo.getEntityById(DisplayToolsProduct.class, id);
	}
	
	@Override
	public void updateDisplayToolsProduct(DisplayToolsProduct displayToolsProduct) throws DataAccessException {
		if (displayToolsProduct == null) {
			throw new IllegalArgumentException("displayToolsProduct");
		}
		repo.update(displayToolsProduct);
	}
	
	@Override
	public void deleteListDisplayToolsProduct(List<DisplayToolsProduct> lstDisplayToolsProduct) throws DataAccessException {
		if (lstDisplayToolsProduct == null) {
			throw new IllegalArgumentException("lstDisplayToolsProduct");
		}
		repo.delete(lstDisplayToolsProduct);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void createDisplayToolListProducts(Long id, List<Long> _list)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if(id==null){
			throw new IllegalArgumentException("id can not null");
		}
		if(_list==null){
			throw new IllegalArgumentException("_list can not null");
		}
		List<DisplayToolsProduct> listCreate=new ArrayList<DisplayToolsProduct>();
		for(Long item:_list){
			Product pTool=new Product();
			//pTool.setProductId(id);
			pTool.setId(id);
			Product pProduct=new Product();
			//pProduct.setProductId(item);
			pProduct.setId(item);
			DisplayToolsProduct ds=new DisplayToolsProduct();
			ds.setProduct(pProduct);
			ds.setTool(pTool);
			
			listCreate.add(ds);
		}
		repo.create(listCreate);
	}

	@Override
	public void deleteDisplayToolListProducts(Long id, List<Long> _list)
			throws DataAccessException {
		// TODO Auto-generated method stub
		if(id==null){
			throw new IllegalArgumentException("id can not null");
		}
		StringBuilder sql=new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" DELETE FROM DISPLAY_TOOLS_PRODUCT dp ");
		sql.append(" WHERE DP.TOOL_ID=? ");
		params.add(id);
		
		Integer i=0;
		if(_list!=null&&!_list.isEmpty())
		{
			sql.append(" AND dp.PRODUCT_ID IN ( ");
			for(Long item:_list){
				if(i==0){
					sql.append("?");
				}else{
					sql.append(",?");
				}
				params.add(item);
				i++;
			}
			sql.append(" )");
		}
		repo.executeSQLQuery(sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import test.ths.dms.common.TestUtils;
import ths.dms.core.common.utils.DateUtility;
import ths.dms.core.common.utils.LogUtility;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

import ths.dms.core.entities.ApParam;
import ths.dms.core.entities.Area;
import ths.dms.core.entities.Customer;
import ths.dms.core.entities.DebitDetailTemp;
import ths.dms.core.entities.PayReceivedTemp;
import ths.dms.core.entities.PaymentDetailTemp;
import ths.dms.core.entities.ProcessHistory;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.PromotionCustomerMap;
import ths.dms.core.entities.PromotionMapDelta;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.PromotionShopMap;
import ths.dms.core.entities.PromotionStaffMap;
import ths.dms.core.entities.RptAccumulativePromotionProgram;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.SaleOrderPromotion;
import ths.dms.core.entities.SalePromoMap;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.ActionSaleOrder;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.AreaType;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.CycleCountType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.InvoiceStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.OrderTypeGroup;
import ths.dms.core.entities.enumtype.PrintOrderFilter;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.enumtype.PromotionType;
import ths.dms.core.entities.enumtype.ReceiptType;
import ths.dms.core.entities.enumtype.RewardType;
import ths.dms.core.entities.enumtype.SaleOrderDetailFilter;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SaleOrderSource;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.SalePlanOwnerType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.StaffSpecificType;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTransLotOwnerType;
import ths.dms.core.entities.filter.PromotionMapBasicFilter;
import ths.dms.core.entities.filter.SaleOrderDetailVATFilter;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PrintOrderVO;
import ths.dms.core.entities.vo.ProductStockTotal;
import ths.dms.core.entities.vo.ProgramCodeVO;
import ths.dms.core.entities.vo.PromotionMapDeltaVO;
import ths.dms.core.entities.vo.RptDSKHTMHRecordProductVO;
import ths.dms.core.entities.vo.RptDTBHTNTHVBHRecordOrderVO;
import ths.dms.core.entities.vo.RptTDBHTMHProductDateStaffFollowCustomerVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO2;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderNumberVO;
import ths.dms.core.entities.vo.SaleOrderPromotionDetailVO;
import ths.dms.core.entities.vo.SaleOrderPromotionVO;
import ths.dms.core.entities.vo.SaleOrderStockDetailVO;
import ths.dms.core.entities.vo.SaleOrderStockVO;
import ths.dms.core.entities.vo.SaleOrderVO;
import ths.dms.core.entities.vo.SaleOrderVOEx2;
import ths.dms.core.entities.vo.SaleProductVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.core.entities.vo.rpt.RptBHTSP_DSNhanVienBH;
import ths.core.entities.vo.rpt.RptBHTSP_ThongTinTVVO;
import ths.core.entities.vo.rpt.RptDTTHNVRecordVO;
import ths.core.entities.vo.rpt.RptOrderByPromotionDataVO;
import ths.core.entities.vo.rpt.RptPGHDetailVO;
import ths.core.entities.vo.rpt.RptPGHLotVO;
import ths.core.entities.vo.rpt.RptPGHVO;
import ths.core.entities.vo.rpt.RptPGNVTTGDetailVO;
import ths.core.entities.vo.rpt.RptPGNVTTGVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_DataConvert;
import ths.core.entities.vo.rpt.RptPayDisplayProgrameRecordProductVO;
import ths.core.entities.vo.rpt.RptProductExchangeVO;
import ths.core.entities.vo.rpt.RptReturnSaleOrderByDeliveryDataVO;
import ths.core.entities.vo.rpt.RptRevenueInDateVO;
import ths.core.entities.vo.rpt.RptSaleOrderByProductDataVO;
import ths.core.entities.vo.rpt.RptSaleOrderInDateVO;
import ths.core.entities.vo.rpt.RptSaleOrderOfDeliveryVO;
import ths.core.entities.vo.rpt.RptSaleOrderVO;
import ths.core.entities.vo.rpt.RptTotalAmountVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public class SaleOrderDAOImpl implements SaleOrderDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private SaleOrderDetailDAO salesOrderDetailDAO;

	@Autowired
	private SaleOrderLotDAO salesOrderLotDAO;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	CustomerDAO customerDAO;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	PromotionProgramDAO promotionProgramDAO;
	
	@Autowired
	PromotionShopMapDAO promotionShopMapDAO;
	
	@Autowired
	PromotionStaffMapDAO promotionStaffMapDAO;
	
	@Autowired
	PromotionCustomerMapDAO promotionCustomerMapDAO;

	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	ApParamDAO apParamDAO;

	@Override
	public SaleOrder createSaleOrder(SaleOrder salesOrder) throws DataAccessException {
		if (salesOrder == null) {
			throw new IllegalArgumentException("salesOrder");
		}
		if (salesOrder.getAccountDate() != null) {
			salesOrder.setAccountDate(DateUtility.getOnlyDate(salesOrder.getAccountDate()));
		}
		salesOrder = repo.create(salesOrder);
		String id = "";
		if (salesOrder.getFromSaleOrder() == null) {
			id = "0000000000000" + salesOrder.getId().toString();
			id = id.substring(id.length() - 13);
		} else {
			id = "000000" + salesOrder.getId().toString();
			id = id.substring(id.length() - 6);
		}
		String code = null;
		if (salesOrder.getFromSaleOrder() == null) {
			code = "M" + id;
		} else if (OrderType.AI.equals(salesOrder.getOrderType())) {
			code = "AI" + salesOrder.getId().toString();
		} else if (OrderType.AD.equals(salesOrder.getOrderType())) {
			code = "AD" + salesOrder.getId().toString();
		} else if (OrderType.CM.equals(salesOrder.getOrderType()) || OrderType.CO.equals(salesOrder.getOrderType())) {
			id = salesOrder.getId().toString();
			code = "C" + id;
		} else {
			String sql = "select code from shop_param where shop_id = ? and type = ?";
			List<Object> params = new ArrayList<Object>();
			params.add(salesOrder.getShop().getId());
			params.add("MAX_SO_NUMBER");
			String lastOrder = (String) repo.getObjectByQuery(sql, params);
			try {
				Long __lastOrder = Long.parseLong(lastOrder);
				__lastOrder = __lastOrder + 1;
				lastOrder = "" + __lastOrder;
			} catch (Exception e) {
				return null;
			}
			/*if (lastOrder == null) {
				String number = "1";
				Shop shop = shopDAO.getShopById(salesOrder.getShop().getId());
				ShopParam shopParam = new ShopParam();
				shopParam.setShop(shop);
				shopParam.setStatus(ActiveType.RUNNING);
				shopParam.setType("MAX_SO_NUMBER");
				shopParam.setCode("1");
				shopParam.setCreateDate(commonDAO.getSysDate());
				shopParam.setCreateUser("VNM_ADMIN");
				shopParam = repo.create(shopParam);
				id = "00000" + "1";
				id = id.substring(id.length() - 6);
			} else {*/
				id = "00000" + lastOrder;
				id = id.substring(id.length() - 6);
			//}
			if (!OrderType.CM.equals(salesOrder.getOrderType()) && !OrderType.CO.equals(salesOrder.getOrderType())) { // lacnv1 : don CO, CM khong tang hoa don noi bo
				this.updateLastestOrderNumberByShop(salesOrder.getShop().getId());
			}
			if (OrderType.CM.equals(salesOrder.getOrderType())) { // phuongvm
				code = "CM" + id;
			} else {
				code = "CO" + id;
			}
			//salesOrder.setRefOrderNumber(code);
		}
		//1/ Ä�Æ¡n bÃ¡n: PO + [SALE_ORDER_ID]: cÃ³ chiá»�u dÃ i lÃ  14 Ã  PO +  láº¥y 12 kÃ½ tá»± Ä‘áº§u tiÃªn cá»§a SALE_ORDER_ID tá»« trÃ¡i sang.
		//2/ Ä�Æ¡n tráº£: CM + [SALE_ORDER_ID]: cÃ³ chiá»�u dÃ i lÃ  14 Ã  CM +  láº¥y 12 kÃ½ tá»± Ä‘áº§u tiÃªn cá»§a SALE_ORDER_ID tá»« trÃ¡i sang.

		if (StringUtility.isNullOrEmpty(code)) {
			throw new DataAccessException("GENERATE_SALE_ORDER_NUMBER_FAIL");
		}
		salesOrder = repo.getEntityById(SaleOrder.class, salesOrder.getId());
		salesOrder.setOrderNumber(code);
		if (OrderType.CM.equals(salesOrder.getOrderType()) || OrderType.CO.equals(salesOrder.getOrderType())) { // don tra gan so ref bang so don hang goc
			if (salesOrder.getFromSaleOrder() != null) {
				salesOrder.setRefOrderNumber(salesOrder.getFromSaleOrder().getOrderNumber());
			}
		} else {
			salesOrder.setRefOrderNumber(code);
		}

		repo.update(salesOrder);
		return salesOrder;
	}

	@Override
	public void updateLastestOrderNumberByShop(Long shopId) throws DataAccessException {
		String sql = "select * from shop_param where shop_id = ? and type = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);
		params.add("MAX_SO_NUMBER");
		ShopParam shopParam = repo.getEntityBySQL(ShopParam.class, sql, params);
		String lastOrder = shopParam.getCode();
		try {
			Long __lastOrder = Long.parseLong(lastOrder);
			__lastOrder = __lastOrder + 1;
			lastOrder = "" + __lastOrder;
			shopParam.setCode(lastOrder);
			repo.update(shopParam);
		} catch (Exception e) {
			throw new DataAccessException("loi convert number");
		}
	}

	@Override
	public SaleOrder getLastestOrderWithException(Long shopId, Long customerId, Long exceptionId, List<OrderType> orderTypes) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from sale_order so ");
		sql.append("where shop_id = ? and customer_id = ? and sale_order_id != ? and approved = ? ");
		params.add(shopId);
		params.add(customerId);
		params.add(exceptionId);
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append(" and so.amount > 0 ");
		if (orderTypes != null && orderTypes.size() > 0) {
			sql.append(" and order_type = ( ");
			String paramPlaceholderStr = "";
			for (OrderType orderType : orderTypes) {
				paramPlaceholderStr += ", ?";
				params.add(orderType.getValue());
			}
			sql.append(paramPlaceholderStr.replaceFirst(", ", ""));
			sql.append(" ) ");
		}
		sql.append("order by order_date desc");
		return repo.getEntityBySQL(SaleOrder.class, sql.toString(), params);
	}

	/**
	 * Kiem tra loai khuyen mai cho don hang
	 * 
	 * @author tientv11
	 */
	public Integer countProductInPromotion(String code) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with tmp as( ");
		sql.append(" 	select group_level_id ");
		sql.append(" 	from group_level gl ");
		sql.append(" 	join product_group pg on pg.product_group_id = gl.product_group_id ");
		sql.append(" 	join promotion_program pp on pp.promotion_program_id = pg.promotion_program_id ");
		sql.append(" 	where promotion_program_code = ? and group_type = 1 and rownum = 1 ");
		params.add(code);
		sql.append(" ) ");
		sql.append(" select count(distinct product_id) count ");
		sql.append(" 	from group_level_detail ");
		sql.append(" 	where group_level_id in ( ");
		sql.append(" 		select group_level_id ");
		sql.append(" 		from tmp ");
		sql.append(" ) ");
		Integer count = repo.countBySQL(sql.toString(), params);
		if (count == null || count == 0) {
			return 2;
		} else if (count > 1) {
			return 0;
		}
		return 1;
	}

	@Override
	public String getLastestOrderNumberByShop(Long shopId) throws DataAccessException {
		String sql = "select * from shop_param where shop_id = ? and type = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);
		params.add("MAX_SO_NUMBER");
		ShopParam __shopParam = repo.getEntityBySQL(ShopParam.class, sql, params);
		String lastOrder = null;
		if (__shopParam != null) {
			lastOrder = __shopParam.getCode();
		}

		if (lastOrder == null) {
			//String number = "1";
			Shop shop = shopDAO.getShopById(shopId);
			ShopParam shopParam = new ShopParam();
			shopParam.setShop(shop);
			shopParam.setStatus(ActiveType.RUNNING);
			shopParam.setType("MAX_SO_NUMBER");
			shopParam.setCode("1");
			shopParam.setCreateDate(commonDAO.getSysDate());
			shopParam.setCreateUser("VNM_ADMIN");
			shopParam = repo.create(shopParam);
			String orderNumber = "000000" + "1";
			orderNumber = orderNumber.substring(orderNumber.length() - 6);
			orderNumber = "IN" + orderNumber;
			return orderNumber;
		} else {
			try {
				Long __lastOrder = Long.parseLong(lastOrder);
				__lastOrder = __lastOrder + 1;
				lastOrder = "" + __lastOrder;
			} catch (Exception e) {
				return null;
			}
			
			String orderNumber = "000000" + lastOrder;
			orderNumber = orderNumber.substring(orderNumber.length() - 6);
			orderNumber = "IN" + orderNumber;
			return orderNumber;
		}
	}
	
	@Override
	public String getLastestOrderNumberAndUpdateByShop(Long shopId,OrderType orderType) throws DataAccessException {
		if(orderType == null){
			throw new IllegalArgumentException("orderType is not null");
		}
		String sql = "select * from shop_param where shop_id = ? and type = ? for update";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);
		params.add("MAX_SO_NUMBER");
		ShopParam __shopParam = repo.getEntityBySQL(ShopParam.class, sql, params);
		String lastOrder = null;
		if (__shopParam != null) {
			lastOrder = __shopParam.getCode();
		}

		if (lastOrder == null) {
			//String number = "1";
			Shop shop = shopDAO.getShopById(shopId);
			ShopParam shopParam = new ShopParam();
			shopParam.setShop(shop);
			shopParam.setStatus(ActiveType.RUNNING);
			shopParam.setType("MAX_SO_NUMBER");
			shopParam.setCode("1");
			shopParam.setCreateDate(commonDAO.getSysDate());
			shopParam.setCreateUser("VNM_ADMIN");
			shopParam = repo.create(shopParam);
			String orderNumber = "000000" + "1";
			orderNumber = orderNumber.substring(orderNumber.length() - 6);
			orderNumber = orderType.getValue() + orderNumber;
			return orderNumber;
		} else {
			try {
				Long __lastOrder = Long.parseLong(lastOrder);
				__lastOrder = __lastOrder + 1;
				lastOrder = "" + __lastOrder;
			} catch (Exception e) {
				return null;
			}
			__shopParam.setCode(lastOrder);
			repo.update(__shopParam);
			String orderNumber = "000000" + lastOrder;
			orderNumber = orderNumber.substring(orderNumber.length() - 6);
			orderNumber = orderType.getValue() + orderNumber;
			return orderNumber;
		}
	}

	@Override
	public String getLastestCOOrderNumberByShop(Long shopId) throws DataAccessException {
		String sql = "select * from shop_param where shop_id = ? and type = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);
		params.add("MAX_SO_NUMBER");
		String lastOrder = null;//(String) repo.getObjectByQuery(sql, params);
		ShopParam shopParam = repo.getEntityBySQL(ShopParam.class, sql, params);
		if (shopParam != null) {
			lastOrder = shopParam.getCode();
		}
		
		if (lastOrder == null) {
			//String number = "1";
			Shop shop = shopDAO.getShopById(shopId);
			shopParam = new ShopParam();
			shopParam.setShop(shop);
			shopParam.setStatus(ActiveType.RUNNING);
			shopParam.setType("MAX_SO_NUMBER");
			shopParam.setCode("1");
			shopParam.setCreateDate(commonDAO.getSysDate());
			shopParam.setCreateUser("VNM_ADMIN");
			shopParam = repo.create(shopParam);
			String orderNumber = "000000" + "1";
			orderNumber = orderNumber.substring(orderNumber.length() - 6);
			orderNumber = "CO" + orderNumber;
			return orderNumber;
		} else {
			try {
				Long __lastOrder = Long.parseLong(lastOrder);
				__lastOrder = __lastOrder + 1;
				lastOrder = "" + __lastOrder;
			} catch (Exception e) {
				return null;
			}
			String orderNumber = "000000" + lastOrder;
			orderNumber = orderNumber.substring(orderNumber.length() - 6);
			orderNumber = "CO" + orderNumber;
			return orderNumber;
		}
	}

	@Override
	public String getLastestSOOrderNumberByShop(Long shopId) throws DataAccessException {
		String sql = "select * from shop_param where shop_id = ? and type = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);
		params.add("MAX_SO_NUMBER");
		String lastOrder = null;//(String) repo.getObjectByQuery(sql, params);
		ShopParam shopParam = repo.getEntityBySQL(ShopParam.class, sql, params);
		if (shopParam != null) {
			lastOrder = shopParam.getCode();
		}
		
		if (lastOrder == null) {
			//String number = "1";
			Shop shop = shopDAO.getShopById(shopId);
			shopParam = new ShopParam();
			shopParam.setShop(shop);
			shopParam.setStatus(ActiveType.RUNNING);
			shopParam.setType("MAX_SO_NUMBER");
			shopParam.setCode("1");
			shopParam.setCreateDate(commonDAO.getSysDate());
			shopParam.setCreateUser("VNM_ADMIN");
			shopParam = repo.create(shopParam);
			String orderNumber = "000000" + "1";
			orderNumber = orderNumber.substring(orderNumber.length() - 6);
			orderNumber = "SO" + orderNumber;
			return orderNumber;
		} else {
			try {
				Long __lastOrder = Long.parseLong(lastOrder);
				__lastOrder = __lastOrder + 1;
				lastOrder = "" + __lastOrder;
			} catch (Exception e) {
				return null;
			}
			String orderNumber = "000000" + lastOrder;
			orderNumber = orderNumber.substring(orderNumber.length() - 6);
			orderNumber = "SO" + orderNumber;
			return orderNumber;
		}
	}

	@Override
	public String getLastestCMOrderNumberByShop(Long shopId) throws DataAccessException {
		String sql = "select * from shop_param where shop_id = ? and type = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);
		params.add("MAX_SO_NUMBER");
		String lastOrder = null;//(String) repo.getObjectByQuery(sql, params);
		ShopParam shopParam = repo.getEntityBySQL(ShopParam.class, sql, params);
		if (shopParam != null) {
			lastOrder = shopParam.getCode();
		}
		
		if (lastOrder == null) {
			//String number = "1";
			Shop shop = shopDAO.getShopById(shopId);
			shopParam = new ShopParam();
			shopParam.setShop(shop);
			shopParam.setStatus(ActiveType.RUNNING);
			shopParam.setType("MAX_SO_NUMBER");
			shopParam.setCode("1");
			shopParam.setCreateDate(commonDAO.getSysDate());
			shopParam.setCreateUser("VNM_ADMIN");
			shopParam = repo.create(shopParam);
			String orderNumber = "000000" + "1";
			orderNumber = orderNumber.substring(orderNumber.length() - 6);
			orderNumber = "CM" + orderNumber;
			return orderNumber;
		} else {
			try {
				Long __lastOrder = Long.parseLong(lastOrder);
				__lastOrder = __lastOrder + 1;
				lastOrder = "" + __lastOrder;
			} catch (Exception e) {
				return null;
			}
			String orderNumber = "000000" + lastOrder;
			orderNumber = orderNumber.substring(orderNumber.length() - 6);
			orderNumber = "CM" + orderNumber;
			return orderNumber;
		}
	}

	@Override
	public void deleteSaleOrder(SaleOrder salesOrder) throws DataAccessException {
		if (salesOrder == null) {
			throw new IllegalArgumentException("salesOrder");
		}
		repo.delete(salesOrder);
	}

	@Override
	public void deleteSaleOrderDetail(SaleOrderDetail saleOrderDetail) throws DataAccessException {
		if (saleOrderDetail == null) {
			throw new IllegalArgumentException("saleOrderDetail");
		}
		repo.delete(saleOrderDetail);
	}

	@Override
	public void deleteSaleOrderDetail(List<SaleOrderDetail> lstSaleOrderDetail) throws DataAccessException {
		if (lstSaleOrderDetail == null || lstSaleOrderDetail.size() == 0) {
			throw new IllegalArgumentException("lstSaleOrderDetail");
		}
		for (SaleOrderDetail sod : lstSaleOrderDetail) {
			repo.delete(sod);
		}
	}
	
	@Override
	public void deleteSaleOrderPromoLot(long saleOrderId) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "delete from sale_order_promo_lot where sale_order_id = ? ";
		params.add(saleOrderId);
		repo.executeSQLQuery(sql, params);
	}
	
	@Override
	public void deleteSaleOrderPromotion(long saleOrderId) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "delete from sale_order_promotion where sale_order_id = ? ";
		params.add(saleOrderId);
		repo.executeSQLQuery(sql, params);
	}
	
	@Override
	public void deleteSalePromoMap(long saleOrderId) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "delete from sale_promo_map where sale_order_id = ? ";
		params.add(saleOrderId);
		repo.executeSQLQuery(sql, params);
	}

	@Override
	public SaleOrder getSaleOrderById(long id) throws DataAccessException {
		return repo.getEntityById(SaleOrder.class, id);
	}

	@Override
	public SaleOrder getSaleOrderByIdForUpdate(long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order where sale_order_id = ?");
		params.add(id);
		return repo.getFirstBySQL(SaleOrder.class, sql.toString(), params);
	}

	@Override
	public SaleOrder getSaleOrderByOrderNumber(String orderNumber, Long shopId) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(orderNumber)) {
			throw new IllegalArgumentException("orderNumber");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order where lower(order_number) like ? ESCAPE '/'");
		params.add(orderNumber.trim().toLowerCase());
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		return repo.getFirstBySQL(SaleOrder.class, sql.toString(), params);
	}

	@Override
	public void updateSaleOrder(SaleOrder salesOrder) throws DataAccessException {
		if (salesOrder == null) {
			throw new IllegalArgumentException("salesOrder");
		}
		salesOrder.setUpdateDate(commonDAO.getSysDate());
		if (salesOrder.getAccountDate() != null) {
			salesOrder.setAccountDate(DateUtility.getOnlyDate(salesOrder.getAccountDate()));
		}
		repo.update(salesOrder);
	}

	@Override
	public void updateSaleOrder(List<SaleOrder> lstSalesOrder) throws DataAccessException {
		if (lstSalesOrder == null) {
			throw new IllegalArgumentException("lstSalesOrder");
		}
		Date sysDate = commonDAO.getSysDate();
		for (SaleOrder salesOrder : lstSalesOrder) {
			salesOrder.setUpdateDate(sysDate);
			if (salesOrder.getAccountDate() != null) {
				salesOrder.setAccountDate(DateUtility.getOnlyDate(salesOrder.getAccountDate()));
			}
			repo.update(salesOrder);
		}
	}

	@Override
	public void updateSaleOrderDetail(List<SaleOrderDetail> lstSalesOrder) throws DataAccessException {
		if (lstSalesOrder == null) {
			throw new IllegalArgumentException("salesOrder");
		}
		for (SaleOrderDetail salesOrder : lstSalesOrder) {
			salesOrder.setUpdateDate(commonDAO.getSysDate());
		}

		repo.update(lstSalesOrder);
	}

	@Override
	public List<SaleOrder> getListSaleOrder(KPaging<SaleOrder> kPaging, String shopCode, Long customerId, Long staffId, Date fromDate, Date toDate, SaleOrderStatus approved, OrderType orderType, Integer state) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<SaleOrder>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from SALE_ORDER sc where 1 = 1");
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and exists(select 1 from shop s where s.shop_id=sc.shop_id and shop_code like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}
		if (customerId != null) {
			sql.append(" and customer_id=?");
			params.add(customerId);
		}
		if (staffId != null) {
			sql.append(" and staff_id=?");
			params.add(staffId);
		}
		if (fromDate != null) {
			sql.append(" and ORDER_DATE >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and ORDER_DATE < trunc(?) + 1");
			params.add(toDate);
		}
		if (approved != null) {
			sql.append(" and approved = ?");
			params.add(approved.getValue());
		}
		if (state != null) {
			sql.append(" and ORDER_SOURCE = ?");
			params.add(state);
		}
		if (orderType != null) {
			sql.append(" and order_type = ?");
			params.add(orderType.getValue());
		}
		sql.append(" order by order_date desc");
		if (kPaging == null)
			return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<SaleOrder> getListSaleOrderByLstOrderNumber(KPaging<SaleOrder> kPaging, List<String> orderNumber) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM SALE_ORDER ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND (ORDER_NUMBER = ? ");
		params.add(orderNumber.get(0));
		for (int i = 1; i < orderNumber.size(); i++) {
			sql.append("  OR ORDER_NUMBER = ? ");
			params.add(orderNumber.get(i));
		}
		sql.append(" )");
		sql.append(" order by ORDER_DATE ASC");
		if (kPaging == null)
			return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<SaleOrder> getListSaleOrder(SaleOrderFilter filter, KPaging<SaleOrder> kPaging) throws DataAccessException {

		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate()))
			return new ArrayList<SaleOrder>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select sc.* from SALE_ORDER sc where 1 = 1");
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			if (filter.getIsCompareEqual() != null && filter.getIsCompareEqual()) {
				sql.append(" and exists(select 1 from shop s where s.shop_id=sc.shop_id and upper(shop_code) = ? )");
				params.add(filter.getShopCode().toUpperCase());
			} else {
				sql.append(" and exists(select 1 from shop s where s.shop_id=sc.shop_id and upper(shop_code) like ? ESCAPE '/' )");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().toUpperCase()));
			}
		}
		if (filter.getCustomerId() != null) {
			sql.append(" and sc.customer_id=?");
			params.add(filter.getCustomerId());
		}
		if (filter.getStaffId() != null) {
			sql.append(" and sc.staff_id=?");
			params.add(filter.getStaffId());
		}
		if (filter.getFromDate() != null) {
			sql.append(" and sc.ORDER_DATE >= trunc(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and sc.ORDER_DATE < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		if (filter.getApproved() != null) {
			sql.append(" and sc.approved = ?");
			params.add(filter.getApproved().getValue());
		}

		if (filter.getType() != null) {
			sql.append(" and sc.type = ? ");
			params.add(filter.getType());
		}

		if (filter.getLstApprovedStatus() != null && filter.getLstApprovedStatus().size() > 0) {
			sql.append(" and sc.approved in ( ");
			String subParamStr = "";
			for (Object saleOrderStatus : filter.getLstApprovedStatus()) {
				subParamStr += "?,";
				params.add(((SaleOrderStatus) saleOrderStatus).getValue());
			}
			sql.append(subParamStr.replaceAll("(?=,$),$", ""));
			sql.append(" ) ");
		}

		if (filter.getDiffDaysFromDate() != null && filter.getAnchorDateDiff() != null) {
			sql.append(" and sc.order_date <= trunc(?) + 1 ");
			params.add(filter.getAnchorDateDiff());
			sql.append(" and sc.order_date > trunc(? - ?) ");
			params.add(filter.getAnchorDateDiff());
			params.add(filter.getDiffDaysFromDate());
		}

		if (filter.getIsPositiveAmountSaleOrder() != null && filter.getIsPositiveAmountSaleOrder().equals(Boolean.TRUE)) {
			sql.append(" and sc.amount > 0 ");
		}

		if (filter.getShopId() != null) {
			sql.append(" and sc.shop_id = ? ");
			params.add(filter.getShopId());
		}

		if (filter.getState() != null) {
			sql.append(" and sc.ORDER_SOURCE = ?");
			params.add(filter.getState());
		}
		if (filter.getOrderType() != null) {
			sql.append(" and sc.order_type = ?");
			params.add(filter.getOrderType().getValue());
		}
		if (Boolean.TRUE.equals(filter.getGetBothINandSO())) {
			sql.append(" and (sc.order_type = ? or sc.order_type = ?)");
			params.add(OrderType.IN.getValue());
			params.add(OrderType.SO.getValue());
		}
		if (filter.getListSaleOrderId() != null && !filter.getListSaleOrderId().isEmpty()) {
			sql.append(" AND sc.sale_order_id in (-1");
			for (int i = 0; i < filter.getListSaleOrderId().size(); i++) {
				sql.append(",?");
				params.add(filter.getListSaleOrderId().get(i));
			}
			sql.append(")");
		}
		if (filter.getLstFromSaleOrderId() != null && filter.getLstFromSaleOrderId().size() > 0) {
			sql.append(" and sc.from_sale_order_id in (-1");
			for (int i = 0; i < filter.getLstFromSaleOrderId().size() ; i++){
				sql.append(",?");
				params.add(filter.getLstFromSaleOrderId().get(i));
			}
			sql.append(")");
		}
		if (filter.getOrderByType() != null) {
			sql.append(" order by sc.order_date asc ");
		} else {
			sql.append(" order by sc.order_date desc");
		}
		if (kPaging == null)
			return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, kPaging);
	}

	@Override
	public Boolean checkSaleOrderByStaff(Long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) count from SALE_ORDER sc where 1 = 1");
		if (staffId != null) {
			sql.append(" and staff_id=?");
			params.add(staffId);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public List<SaleOrder> getListSaleOrderByDate(Long customerId, Date orderDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from SALE_ORDER sc where (approved = ? or approved = ?) and ORDER_DATE >= trunc(?)");
		params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
		params.add(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM.getValue());
		params.add(orderDate);
		if (customerId != null) {
			sql.append(" and customer_id=?");
			params.add(customerId);
		}

		return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
	}

	@Override
	public Boolean checkSaleOrderByDate(Long customerId, Long saleOrderId, Date orderDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) as count from SALE_ORDER sc where (approved = ? or approved = ?)");
		params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
		params.add(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM.getValue());
		if (customerId != null) {
			sql.append(" and customer_id=?");
			params.add(customerId);
		}
		if (saleOrderId != null) {
			sql.append(" and sale_order_id != ?");
			params.add(saleOrderId);
		}
		if (orderDate != null) {
			sql.append("  and ORDER_DATE >= trunc(?) and order_date < trunc(?)+1");
			params.add(orderDate);
			params.add(orderDate);
		}

		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public SaleOrder createSaleOrder(SaleOrder salesOrder, List<SaleProductVO> lstSaleProductVO, List<SaleProductVO> lstPromoProductVO, Boolean isUpdate, List<SaleOrderPromotionVO> lstOrderPromotion,
			List<SaleOrderPromotionDetailVO> lstOrderPromoDetail,LogInfoVO logInfoVO) throws DataAccessException {
		if (salesOrder == null) {
			throw new IllegalArgumentException("salesOrder");
		}
		if (isUpdate == true) {
			this.updateSaleOrder(salesOrder);
		} else {
			salesOrder = this.createSaleOrder(salesOrder);
		}
		List<ApParam> lstTypeCode = apParamDAO.getListApParamByType(ApParamType.PROMOTION_MANUAL, ActiveType.RUNNING);
		List<String> lstType = new ArrayList<String>();
		for (ApParam apParam : lstTypeCode) {
			lstType.add(apParam.getApParamCode());
		}
		/** OLD Thong tin lien quan den so suat */
		List<SaleOrderDetail> lstSalesProduct = new ArrayList<SaleOrderDetail>();
		if (lstPromoProductVO != null && lstPromoProductVO.size() > 0) {
			lstSaleProductVO.addAll(lstPromoProductVO);
		}
		//Luu num va Amount promotion;
		HashMap<String, SaleOrderPromotion> mapPromotionGroup = new HashMap<>();
		String keyPromotionGroup = null;
		for (SaleProductVO vo : lstSaleProductVO) {
			SaleOrderDetail detailTmp = vo.getSaleOrderDetail();
			detailTmp.setSaleOrder(salesOrder);
			if (!StringUtility.isNullOrEmpty(detailTmp.getProgramCode())) {
				PromotionProgram pp = promotionProgramDAO.getPromotionProgramByCode(detailTmp.getProgramCode());
				if (pp != null) {
					detailTmp.setProgrameTypeCode(pp.getType());
				}
			}
			
			SaleOrderDetail detail = salesOrderDetailDAO.createSaleOrderDetail(detailTmp);
			
			//Duyet qua cac ctkm co the huong va loc ra cac chuong trinh dat or khong dat
			//lstOrderPromotion luu cac dong co programcode dat. 
			List<SalePromoMap> lstPromoMap = new ArrayList<SalePromoMap>();
			//Begin vuongmq; 15/01/2016; lay lai danh sach KM ung voi san pham mua; neu co thay doi so suat duoi DB cua SP mua detail
			Shop shopOrderTmp = salesOrder.getShop();
			Staff staffOrderTmp = salesOrder.getStaff();
			Customer customerOrderTmp = salesOrder.getCustomer();
			Product productOrderDetailTmp = detailTmp.getProduct();
			if (detailTmp.getIsFreeItem() != null && detailTmp.getIsFreeItem().equals(0)) {
				if (shopOrderTmp != null && staffOrderTmp != null && customerOrderTmp != null && productOrderDetailTmp != null) {
					List<OrderProductVO> listOrderProductTmp = productDAO.getListOrderProductVO(null, shopOrderTmp.getId(), staffOrderTmp.getId(), customerOrderTmp.getId(), productOrderDetailTmp.getProductCode(), null, null, salesOrder.getOrderDate(), (customerOrderTmp.getChannelType() == null ? 0L : customerOrderTmp.getChannelType().getId()), shopOrderTmp.getType().getId());
					if (listOrderProductTmp != null && !listOrderProductTmp.isEmpty()) {
						StringBuilder promotionCodeTmp = new StringBuilder();
						for (OrderProductVO orderProductVO : listOrderProductTmp) {
							if (orderProductVO != null && !StringUtility.isNullOrEmpty(orderProductVO.getPromotionProgramCode())) {
								promotionCodeTmp.append(",").append(orderProductVO.getPromotionProgramCode());
							}
						}
						if (!StringUtility.isNullOrEmpty(promotionCodeTmp.toString())) {
							detailTmp.setLstProgramCode(promotionCodeTmp.toString().replaceFirst(",", ""));
						}
					}
				}
				//End vuongmq; 15/01/2016; lay lai danh sach KM ung voi san pham mua; neu co thay doi so suat duoi DB cua SP mua detail
				if (!StringUtility.isNullOrEmpty(detailTmp.getLstProgramCode())) {
					String[] lstProgramCode = detailTmp.getLstProgramCode().split(",");
					for (int i = 0 ; i < lstProgramCode.length ; i++) {
						lstProgramCode[i] = lstProgramCode[i].trim();
						ActiveType status = ActiveType.KHONG_DAT;
						for (SaleProductVO temp : lstSaleProductVO) {
							if (temp.getLstProgramDat() != null && temp.getLstProgramDat().indexOf(lstProgramCode[i]) != -1
								&& temp.getSaleOrderDetail().getProduct().getId().equals(detail.getProduct().getId())) {
								status = ActiveType.DAT;
								break;
							}
						}
						SalePromoMap som = new SalePromoMap();
						som.setProgramCode(lstProgramCode[i]);
						som.setSaleOrder(salesOrder);
						som.setStaff(salesOrder.getStaff());
						som.setStatus(status);
						lstPromoMap.add(som);
					}
					//Luu thong tin promo_map lstSaleProductVO,lstOrderPromotion
					for (SalePromoMap som : lstPromoMap) {
						som.setSaleOrderDetail(detail);
						commonDAO.createEntity(som);
					}
				}
			} else {
				// vuongmq; 19/01/2015; lay numReceived
				if (lstType.contains(detailTmp.getProgrameTypeCode())) {
					if (detailTmp.getProduct() != null) {					
						keyPromotionGroup = detailTmp.getProgramCode() + "_" + detailTmp.getProduct().getId();
					}
				} else {
					keyPromotionGroup = detailTmp.getProgramCode() + "_" + detailTmp.getProductGroupId() + "_" + detailTmp.getGroupLevelId();
				}
				SaleOrderPromotion saleOrderPromotionTmp = mapPromotionGroup.get(keyPromotionGroup);
				if (saleOrderPromotionTmp != null) {
					if (saleOrderPromotionTmp.getNumReceived() != null) {
						if (detailTmp.getQuantity() != null) {
							saleOrderPromotionTmp.setNumReceived(saleOrderPromotionTmp.getNumReceived().add(new BigDecimal(detailTmp.getQuantity())));
						}
					} else {
						saleOrderPromotionTmp.setNumReceived(detailTmp.getQuantity() != null ? new BigDecimal(detailTmp.getQuantity()) : null);
					}
					if (saleOrderPromotionTmp.getMaxNumReceived() != null) {
						if (detailTmp.getMaxQuantityFreeTotal() != null) {
							saleOrderPromotionTmp.setMaxNumReceived(saleOrderPromotionTmp.getMaxNumReceived().add(new BigDecimal(detailTmp.getMaxQuantityFreeTotal())));
						}
					} else {
						saleOrderPromotionTmp.setMaxNumReceived(detailTmp.getMaxQuantityFreeTotal() != null ? new BigDecimal(detailTmp.getMaxQuantityFreeTotal()) : null);
					}
				} else {
					saleOrderPromotionTmp = new SaleOrderPromotion();
					saleOrderPromotionTmp.setNumReceived(detailTmp.getQuantity() != null ? new BigDecimal(detailTmp.getQuantity()) : null);
					saleOrderPromotionTmp.setMaxNumReceived(detailTmp.getMaxQuantityFreeTotal() != null ? new BigDecimal(detailTmp.getMaxQuantityFreeTotal()) : null);
					mapPromotionGroup.put(keyPromotionGroup, saleOrderPromotionTmp);
				}
			}
			//String isUsingZVOrder = lstSaleProductVO.get(0).getIsUsingZVOrder();
			String isUsingZVCode = lstSaleProductVO.get(0).getIsUsingZVCode();
			/**
			 * Tao moi SALE_ORDER_PROMO_DETAIL luu thong tin CKMH cua cac SP ban
			 * theo KM SP hoac KM don hang
			 */
			if (lstOrderPromoDetail != null && lstOrderPromoDetail.size() > 0) {
				SaleOrderPromoDetail promoDetail = null;
				for (SaleOrderPromotionDetailVO tmp : lstOrderPromoDetail) {
					if (tmp.getProductId() != null && vo.getSaleOrderDetail().getProduct() != null && tmp.getProductId().equals(vo.getSaleOrderDetail().getProduct().getId()) && vo.getSaleOrderDetail().getIsFreeItem() == 0) {
						promoDetail = new SaleOrderPromoDetail();
						promoDetail.setSaleOrder(salesOrder);
						promoDetail.setShop(salesOrder.getShop());
						promoDetail.setStaff(salesOrder.getStaff());
						promoDetail.setOrderDate(salesOrder.getOrderDate());
						promoDetail.setSaleOrderDetail(detail);
						promoDetail.setProgramCode(tmp.getProgramCode());
						promoDetail.setProgrameTypeCode(tmp.getProgramTypeCode());
						//if (tmp.getProgramTypeCode() != null && "ZV24".equalsIgnoreCase(tmp.getProgramTypeCode().trim())) { // cheat
							//promoDetail.setProgramType(ProgramType.OPEN_PRODUCT);
						//} else {
							promoDetail.setProgramType(tmp.getProgramType());
						//}
						promoDetail.setIsOwner(tmp.getIsOwner());
						promoDetail.setDiscountAmount(tmp.getDiscountAmount());
						promoDetail.setDiscountPercent(tmp.getDiscountPercent() != null ? tmp.getDiscountPercent().floatValue() : null);
						promoDetail.setMaxAmountFree(tmp.getMaxAmountFree());
						//promoDetail.setProductGroup(commonDAO.getEntityById(ProductGroup.class, tmp.getProductGroupId()));
						promoDetail.setProductGroupId(tmp.getProductGroupId());
						//promoDetail.setGroupLevel(commonDAO.getEntityById(GroupLevel.class, tmp.getGroupLevelId()));
						promoDetail.setGroupLevelId(tmp.getGroupLevelId());
						if (promoDetail.getIsOwner().equals(1) || promoDetail.getIsOwner().equals(0)) {
							promoDetail = commonDAO.createEntity(promoDetail);
						} else if (promoDetail.getIsOwner().equals(2)) {
							/*if (PromotionType.ZV24.getValue().equalsIgnoreCase(promoDetail.getProgrameTypeCode()) && promoDetail.getProgramCode().equals(isUsingZVCode)) {
								promoDetail = commonDAO.createEntity(promoDetail);
							} else if (promoDetail.getProgrameTypeCode().equals(isUsingZVOrder) && promoDetail.getProgramCode().equals(isUsingZVCode)) {
								promoDetail = commonDAO.createEntity(promoDetail);
							}*/
							if (promoDetail.getProgramCode().equals(isUsingZVCode)
									|| PromotionType.ZV24.getValue().equalsIgnoreCase(promoDetail.getProgrameTypeCode())) {
								promoDetail = commonDAO.createEntity(promoDetail);
							}
						}
						// vuongmq; begin 19/01/2015; lay amountReceived
						if (lstType.contains(tmp.getProgramTypeCode())) {				
							keyPromotionGroup = tmp.getProgramCode() + "_" + tmp.getProductId();
						} else {
							keyPromotionGroup = tmp.getProgramCode() + "_" + tmp.getProductGroupId() + "_" + tmp.getGroupLevelId();
						}
						SaleOrderPromotion saleOrderPromotionTmp = mapPromotionGroup.get(keyPromotionGroup);
						if (saleOrderPromotionTmp != null) {
							if (saleOrderPromotionTmp.getAmountReceived() != null) {
								if (tmp.getDiscountAmount() != null) {
									saleOrderPromotionTmp.setAmountReceived(saleOrderPromotionTmp.getAmountReceived().add(tmp.getDiscountAmount()));
								}
							} else {
								saleOrderPromotionTmp.setAmountReceived(tmp.getDiscountAmount());
							}
							if (saleOrderPromotionTmp.getMaxAmountReceived() != null) {
								if (tmp.getMaxAmountFree() != null) {
									saleOrderPromotionTmp.setMaxAmountReceived(saleOrderPromotionTmp.getMaxAmountReceived().add(tmp.getMaxAmountFree()));
								}
							} else {
								saleOrderPromotionTmp.setMaxAmountReceived(tmp.getMaxAmountFree());
							}
						} else {
							saleOrderPromotionTmp = new SaleOrderPromotion();
							saleOrderPromotionTmp.setAmountReceived(tmp.getDiscountAmount());
							saleOrderPromotionTmp.setMaxAmountReceived(tmp.getMaxAmountFree());
							mapPromotionGroup.put(keyPromotionGroup, saleOrderPromotionTmp);
						}
						// vuongmq; begin 19/01/2015; lay amountReceived
					}
				}
			}

			List<SaleOrderLot> lstLot = new ArrayList<SaleOrderLot>();
			if (vo.getLstSaleOrderLot() != null) {
				for (SaleOrderLot lot : vo.getLstSaleOrderLot()) {
					lot.setSaleOrderDetail(detail);
					lstLot.add(lot);
				}
				salesOrderLotDAO.createSaleOrderLot(lstLot);
			}
			lstSalesProduct.add(detail);
		}
		/** Thong tin lien quan den so suat SALE_ORDER_PROMOTION */
		if (lstOrderPromotion == null) {
			lstOrderPromotion = new ArrayList<SaleOrderPromotionVO>();
		}
		boolean isPromotionManual = true;
		SaleOrderPromotion saleOrderPromotion = null;
		for (SaleOrderPromotionVO vo : lstOrderPromotion) {
			saleOrderPromotion = new SaleOrderPromotion();
			saleOrderPromotion.setCreateDate(salesOrder.getCreateDate());
			saleOrderPromotion.setCreateUser(salesOrder.getCreateUser());
			//saleOrderPromotion.setGroupLevel(commonDAO.getEntityById(GroupLevel.class, vo.getLevelId()));
			saleOrderPromotion.setGroupLevelId(vo.getLevelId());
			//saleOrderPromotion.setProductGroup(commonDAO.getEntityById(ProductGroup.class, vo.getGroupId()));
			saleOrderPromotion.setProductGroupId(vo.getGroupId());
			PromotionProgram pp = commonDAO.getEntityById(PromotionProgram.class, vo.getPromotionId());
			saleOrderPromotion.setPromotionProgram(pp);
			saleOrderPromotion.setPromotionCode(pp.getPromotionProgramCode());
			saleOrderPromotion.setQuantityReceived(vo.getQuantityReceived());
			//saleOrderPromotion.setMaxQuantityReceived(vo.getMaxQuantityReceived());
			//begin vuongmq; 11/01/2015; them cac bien theo xu ly save: sale_order_promotion
			saleOrderPromotion.setMaxQuantityReceived(vo.getMaxQuantityReceivedTotal());
			isPromotionManual = lstType.contains(pp.getType());
			if (isPromotionManual) {
				if (vo.getProduct() != null) {					
					keyPromotionGroup = pp.getPromotionProgramCode() + "_" + vo.getProduct().getId();
				}
			} else {
				keyPromotionGroup = pp.getPromotionProgramCode() + "_" + vo.getGroupId() + "_" + vo.getLevelId();
			}
			SaleOrderPromotion saleOrderPromotionTmp = mapPromotionGroup.get(keyPromotionGroup);
			if (saleOrderPromotionTmp != null) {
				saleOrderPromotion.setNumReceived(saleOrderPromotionTmp.getNumReceived());
				if (isPromotionManual) {
					saleOrderPromotion.setMaxNumReceived(saleOrderPromotion.getNumReceived());					
				} else {
					saleOrderPromotion.setMaxNumReceived(saleOrderPromotionTmp.getMaxNumReceived());
				}
				saleOrderPromotion.setAmountReceived(saleOrderPromotionTmp.getAmountReceived());
				saleOrderPromotion.setMaxAmountReceived(saleOrderPromotionTmp.getMaxAmountReceived());
			}
			saleOrderPromotion.setExceedObject(vo.getExceedObject());
			saleOrderPromotion.setExceedUnit(vo.getExceedUnit());
			//end vuongmq; 11/01/2015; them cac bien theo xu ly save: sale_order_promotion
			saleOrderPromotion.setSaleOrder(salesOrder);
			saleOrderPromotion.setShop(salesOrder.getShop());
			saleOrderPromotion.setStaff(salesOrder.getStaff());
			saleOrderPromotion.setPromotionLevel(vo.getPromotionLevel());
			saleOrderPromotion.setOrderDate(salesOrder.getOrderDate());
			saleOrderPromotion = commonDAO.createEntity(saleOrderPromotion);
			//begin vuongmq; 11/01/2015; them dong bang: promotion_map_delta
			saleOrderPromotion.setPromotionDetail(genPromotionDetail(saleOrderPromotion, salesOrder));
			this.createPromotionMapDelta(salesOrder, saleOrderPromotion, ActionSaleOrder.INSERT, isUpdate);
			//end vuongmq; 11/01/2015; them dong bang: promotion_map_delta
		}
		/** Ghi log */
		try {
			logInfoVO.setIdObject(salesOrder.getId().toString());
			LogUtility.logInfoWs(logInfoVO);
		} catch (Exception e) {}	
		
		return salesOrder;
	}
	
	private String genPromotionDetail(SaleOrderPromotion saleOrderPromotion, SaleOrder salesOrder) throws DataAccessException {
		StringBuilder promotionDetail = new StringBuilder();
		Double[] number = new Double[3];
		final int QUANTITY = 1;
		final int NUM = 2;
		final int AMOUNT = 3;
		final int SHOP = 1;
		final int STAFF = 2;
		final int CUSTOMER = 3;
		final int TOTAL = 1;
		final int REMAIN = 2;
		PromotionShopMap promotionShopMap = null;
		PromotionStaffMap promotionStaffMap = null;
		PromotionCustomerMap promotionCustomerMap = null;
		if (salesOrder.getShop() != null && saleOrderPromotion.getPromotionProgram() != null) {
			List<PromotionShopMap> lstPromotionShopMap = promotionShopMapDAO.getPromotionShopMapIncludeParentShop(saleOrderPromotion.getPromotionProgram().getId(), saleOrderPromotion.getShop().getId(), ActiveType.RUNNING.getValue());
			if (lstPromotionShopMap != null && lstPromotionShopMap.size() > 0) {
				promotionShopMap = lstPromotionShopMap.get(0);
				if (salesOrder.getStaff() != null) {
					promotionStaffMap = promotionStaffMapDAO.getPromotionStaffMapByShopMapAndStaff(promotionShopMap.getId(), salesOrder.getStaff().getId());
				}
				if (salesOrder.getCustomer() != null) {
					promotionCustomerMap = promotionCustomerMapDAO.getPromotionCustomerMap(promotionShopMap.getId(), salesOrder.getCustomer().getId());
				}
			}
		}
		for (int a = QUANTITY; a <= AMOUNT; a++) {
			number[TOTAL] = null;
			number[REMAIN] = null;
			for (int b = SHOP; b <= CUSTOMER; b++) {
				if (b == SHOP && promotionShopMap != null) {
					if (a == QUANTITY) {						
						number[TOTAL] = promotionShopMap.getQuantityReceivedTotal() == null ? 0 : promotionShopMap.getQuantityReceivedTotal().doubleValue();
						number[REMAIN] = promotionShopMap.getQuantityMax() == null ? null : (promotionShopMap.getQuantityMax().doubleValue() - number[TOTAL]);
					}
					if (a == NUM) {						
						number[TOTAL] = promotionShopMap.getNumReceivedTotal() == null ? 0 : promotionShopMap.getNumReceivedTotal().doubleValue();
						number[REMAIN] = promotionShopMap.getNumMax() == null ? null : (promotionShopMap.getNumMax().doubleValue() - number[TOTAL]);
					}
					if (a == AMOUNT) {						
						number[TOTAL] = promotionShopMap.getAmountReceivedTotal() == null ? 0 : promotionShopMap.getAmountReceivedTotal().doubleValue();
						number[REMAIN] = promotionShopMap.getAmountMax() == null ? null : (promotionShopMap.getAmountMax().doubleValue() - number[TOTAL]);
					}
				} else if (b == STAFF && promotionStaffMap != null) {
					if (a == QUANTITY) {						
						number[TOTAL] = promotionStaffMap.getQuantityReceivedTotal() == null ? 0 : promotionStaffMap.getQuantityReceivedTotal().doubleValue();
						number[REMAIN] = promotionStaffMap.getQuantityMax() == null ? null : (promotionStaffMap.getQuantityMax().doubleValue() - number[TOTAL]);
					}
					if (a == NUM) {						
						number[TOTAL] = promotionStaffMap.getNumReceivedTotal() == null ? 0 : promotionStaffMap.getNumReceivedTotal().doubleValue();
						number[REMAIN] = promotionStaffMap.getNumMax() == null ? null : (promotionStaffMap.getNumMax().doubleValue() - number[TOTAL]);
					}
					if (a == AMOUNT) {						
						number[TOTAL] = promotionStaffMap.getAmountReceivedTotal() == null ? 0 : promotionStaffMap.getAmountReceivedTotal().doubleValue();
						number[REMAIN] = promotionStaffMap.getAmountMax() == null ? null : (promotionStaffMap.getAmountMax().doubleValue() - number[TOTAL]);
					}
				} else if (b == CUSTOMER && promotionCustomerMap != null) {
					if (a == QUANTITY) {						
						number[TOTAL] = promotionCustomerMap.getQuantityReceivedTotal() == null ? 0 : promotionCustomerMap.getQuantityReceivedTotal().doubleValue();
						number[REMAIN] = promotionCustomerMap.getQuantityMax() == null ? null : (promotionCustomerMap.getQuantityMax().doubleValue() - number[TOTAL]);
					}
					if (a == NUM) {						
						number[TOTAL] = promotionCustomerMap.getNumReceivedTotal() == null ? 0 : promotionCustomerMap.getNumReceivedTotal().doubleValue();
						number[REMAIN] = promotionCustomerMap.getNumMax() == null ? null : (promotionCustomerMap.getNumMax().doubleValue() - number[TOTAL]);
					}
					if (a == AMOUNT) {						
						number[TOTAL] = promotionCustomerMap.getAmountReceivedTotal() == null ? 0 : promotionCustomerMap.getAmountReceivedTotal().doubleValue();
						number[REMAIN] = promotionCustomerMap.getAmountMax() == null ? null : (promotionCustomerMap.getAmountMax().doubleValue() - number[TOTAL]);
					}
				}
				for (int c = TOTAL; c <= REMAIN; c++) {					
					promotionDetail.append(";").append("(").append("" + a + b + c).append(": ").append(number[c] == null ? "" : number[c]).append(")");
				}
			}
		}
		if (promotionDetail.length() > 0) {
			return promotionDetail.toString().replaceFirst(";", "");
		}
		return promotionDetail.toString();
	}

	/**
	 * Xu ly createPromotionMapDelta
	 * @author vuongmq
	 * @param salesOrder
	 * @param saleOrderPromotion
	 * @param actionSaleOrder
	 * @param isUpdate
	 * @throws DataAccessException
	 * @since Jan 11, 2016
	*/
	@Override
	public PromotionMapDelta createPromotionMapDelta(SaleOrder salesOrder, SaleOrderPromotion saleOrderPromotion, ActionSaleOrder actionSaleOrder, Boolean isUpdate) throws DataAccessException {
		PromotionMapDelta promotionMapDelta = null;
		if (salesOrder != null && saleOrderPromotion != null) {
			promotionMapDelta = new PromotionMapDelta();
			PromotionProgram pp = saleOrderPromotion.getPromotionProgram();
			if (pp != null && pp.getId() != null && salesOrder.getShop() != null && salesOrder.getShop().getId() != null) {
				PromotionMapBasicFilter<PromotionShopMap> filterShop = new PromotionMapBasicFilter<PromotionShopMap>();
				filterShop.setId(pp.getId());
				filterShop.setOrderDate(salesOrder.getOrderDate());
				filterShop.setShopId(salesOrder.getShop().getId());
				PromotionShopMap promotionShopMap = promotionProgramDAO.getPromotionShopMapFilter(filterShop);
				promotionMapDelta.setPromotionShopMap(promotionShopMap);
				if (salesOrder.getStaff() != null && salesOrder.getStaff().getId() != null) {
					PromotionMapBasicFilter<PromotionStaffMap> filterStaff = new PromotionMapBasicFilter<PromotionStaffMap>();
					filterStaff.setId(pp.getId());
					filterStaff.setOrderDate(salesOrder.getOrderDate());
					filterStaff.setShopId(salesOrder.getShop().getId());
					filterStaff.setStaffId(salesOrder.getStaff().getId());
					PromotionStaffMap promotionStaffMap = promotionProgramDAO.getPromotionStaffMapFilter(filterStaff);
					promotionMapDelta.setPromotionStaffMap(promotionStaffMap);
				}
				if (salesOrder.getCustomer() != null && salesOrder.getCustomer().getId() != null) {
					PromotionMapBasicFilter<PromotionCustomerMap> filterCus = new PromotionMapBasicFilter<PromotionCustomerMap>();
					filterCus.setId(pp.getId());
					filterCus.setOrderDate(salesOrder.getOrderDate());
					filterCus.setShopId(salesOrder.getShop().getId());
					filterCus.setCustomerId(salesOrder.getCustomer().getId());
					PromotionCustomerMap promotionCustomerMap = promotionProgramDAO.getPromotionCustomerMapFilter(filterCus);
					promotionMapDelta.setPromotionCustomerMap(promotionCustomerMap);
				}
			}
			if (ActionSaleOrder.INSERT.equals(actionSaleOrder)) {
				promotionMapDelta.setQuantityDelta(saleOrderPromotion.getQuantityReceived());
				promotionMapDelta.setNumDelta(saleOrderPromotion.getNumReceived());
				promotionMapDelta.setAmountDelta(saleOrderPromotion.getAmountReceived());
				if (isUpdate) {
					promotionMapDelta.setAction(ActionSaleOrder.UPDATE);
				} else {
					promotionMapDelta.setAction(ActionSaleOrder.INSERT);
				}
			} else {
				promotionMapDelta.setQuantityDelta(saleOrderPromotion.getQuantityReceived() != null ? saleOrderPromotion.getQuantityReceived() * (-1) : null);
				promotionMapDelta.setNumDelta(saleOrderPromotion.getNumReceived() != null ? saleOrderPromotion.getNumReceived().negate() : null);
				promotionMapDelta.setAmountDelta(saleOrderPromotion.getAmountReceived() != null ? saleOrderPromotion.getAmountReceived().negate() : null);
				promotionMapDelta.setAction(ActionSaleOrder.UPDATE);
			}
			promotionMapDelta.setSource(SaleOrderSource.WEB);
			promotionMapDelta.setFromObjectId(salesOrder.getId());
			promotionMapDelta.setPromotionProgram(pp);
			promotionMapDelta.setShop(salesOrder.getShop());
			promotionMapDelta.setStaff(salesOrder.getStaff());
			promotionMapDelta.setCustomer(salesOrder.getCustomer());
			promotionMapDelta.setCreateDate(salesOrder.getCreateDate());
			promotionMapDelta.setCreateUser(salesOrder.getCreateUser());
			promotionMapDelta = commonDAO.createEntity(promotionMapDelta);
		}
		return promotionMapDelta;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SaleOrderDAO#getListSaleOrder(ths.dms.core.entities
	 * .enumtype.KPaging, java.lang.String, java.lang.String, java.lang.String,
	 * ths.dms.core.entities.enumtype.ApprovalStatus,
	 * ths.dms.core.entities.enumtype.OrderType, java.util.Date,
	 * java.util.Date, java.lang.String, java.lang.String)
	 */
	@Override
	public List<SaleOrderVO> getListSaleOrderVO(KPaging<SaleOrderVO> kPaging, String shortCode, String customeName, String orderNumber, ApprovalStatus approval, OrderType orderType, Date fromDate, Date toDate, String staffCode, String deliveryCode)
			throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<SaleOrderVO>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.sale_order_id as saleOrderId, so.order_date as orderDate, so.customer_id as customerId, c.customer_code as customerCode, c.customer_name as customerName, so.order_number as orderNumber, so.staff_id as staffId, s.staff_code as staffCode, so.delivery_id as deliveryId, s2.staff_code as deliveryCode,");
		sql.append(" so.approved as approved, so.order_type as orderType,");
		sql.append(" (select ORDER_NUMBER from sale_order so1 where so1.sale_order_id=so.from_sale_order_id) as fromSaleOrderCode, so.total");
		sql.append(" from sale_order so, staff s, customer c, staff s2");
		sql.append(" where so.customer_id = c.customer_id and s.staff_id = so.staff_id and so.delivery_id = s2.staff_id");
		sql.append(" and so.type = ? and so.from_sale_order_id is null");
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		if (shortCode != null) {
			sql.append(" and c.short_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shortCode.toUpperCase()));
		}
		if (customeName != null) {
			sql.append(" and lower(c.customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(customeName));
		}
		if (orderNumber != null) {
			sql.append(" and lower(so.order_number) = ?");
			params.add(orderNumber.toLowerCase());
		}
		if (approval != null) {
			sql.append(" and so.approved = ?");
			params.add(approval.getValue());
		} else {
			sql.append(" and so.approved = ?");
			params.add(SaleOrderStatus.APPROVED.getValue());
		}
		if (orderType != null) {
			sql.append(" and so.order_type = ?");
			params.add(orderType.getValue());
		} else {
			sql.append(" and so.order_type not in (?,?)");
			params.add(OrderType.SO.getValue());
			params.add(OrderType.CO.getValue());
		}
		if (fromDate != null) {
			sql.append(" and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		if (staffCode != null) {
			sql.append(" and s.staff_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toUpperCase()));
		}
		if (deliveryCode != null) {
			sql.append(" and s2.staff_code like ? ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(deliveryCode.toUpperCase()));
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql.toString());
		countSql.append(")");
		final String[] fieldNames = new String[] { //
		"saleOrderId",// 1
				"orderDate", // 2
				"customerId", // 3
				"customerCode", // 4
				"customerName", // 5
				"orderNumber", // 6
				"staffId", // 7
				"staffCode", // 8
				"deliveryId", // 9
				"deliveryCode", // 10
				"orderType", //11	
				"fromSaleOrderCode", //12
				"approved", //13
				"total" //14
		};

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.LONG, // 1
				StandardBasicTypes.TIMESTAMP, // 2
				StandardBasicTypes.LONG, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.LONG, // 7
				StandardBasicTypes.STRING, // 8
				StandardBasicTypes.LONG, // 9
				StandardBasicTypes.STRING, // 10
				StandardBasicTypes.STRING, // 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.INTEGER, // 13
				StandardBasicTypes.BIG_DECIMAL // 14
		};
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(SaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(SaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}

	@Override
	public List<SaleOrderVO> getListSaleOrderVOByDelivery(KPaging<SaleOrderVO> kPaging, Long shopId, List<OrderType> orderType, SaleOrderStatus status, SaleOrderType type, Date fromDate, Date toDate, Long deliveryId) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<SaleOrderVO>();

		StringBuilder sql = new StringBuilder();
		StringBuilder conditionSQL = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select so.amount as amount, ");
		sql.append("     so.sale_order_id as saleOrderId, ");
		sql.append("     so.order_date as orderDate, ");
		sql.append("     so.order_number as orderNumber, ");
		sql.append("     so.total as total, ");
		sql.append("     so.customer_id as customerId, ");
		sql.append("     (select short_code from customer where customer_id = so.customer_id) as shortCode, ");
		sql.append("     (select customer_code from customer where customer_id = so.customer_id) as customerCode, ");
		sql.append("     (select customer_name from customer where customer_id = so.customer_id) as customerName, ");
		sql.append("     (select address from customer where customer_id = so.customer_id) as customerAddress, ");
		sql.append("     (select phone from customer where customer_id = so.customer_id) as customerPhone, ");
		sql.append("     so.staff_id as staffId, ");
		sql.append("     (select staff_code from staff where staff_id = so.staff_id) as staffCode, ");
		sql.append("     (select staff_name from staff where staff_id = so.staff_id) as staffName, ");
		sql.append("     delivery.staff_id as deliveryId, ");
		sql.append("     delivery.staff_code as deliveryCode, ");
		sql.append("     delivery.staff_name as deliveryName,     ");
		sql.append("     (select sum(discount_amount) from sale_order_detail where sale_order_id = so.sale_order_id) as discount, ");
		sql.append("     decode(sign((select count(*) from sale_order_detail where sale_order_id = so.sale_order_id and invoice_id is not null)), 1, 1, 0) as isInvoice ");

		conditionSQL.append(" from sale_order so, staff delivery ");
		conditionSQL.append(" where so.delivery_id = delivery.staff_id ");

		if (shopId != null) {
			conditionSQL.append(" and so.shop_id = ?");
			params.add(shopId);
		}
		if (deliveryId != null) {
			conditionSQL.append(" and so.delivery_id = ?");
			params.add(deliveryId);
		}
		if (orderType != null && orderType.size() > 0) {
			conditionSQL.append(" and so.order_type in (");

			for (int i = 0; i < orderType.size(); i++) {
				if (i < orderType.size() - 1) {
					conditionSQL.append(" ?, ");
				} else {
					conditionSQL.append(" ?)");
				}

				params.add(orderType.get(i).getValue());
			}
		}
		if (null != status) {
			conditionSQL.append(" and so.approved = ?");
			params.add(status.getValue());
		}
		if (null != type) {
			conditionSQL.append(" and so.type = ?");
			params.add(type.getValue());
		}
		if (fromDate != null) {
			conditionSQL.append(" and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			conditionSQL.append(" and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}

		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count ");
		countSql.append(conditionSQL.toString());

		sql.append(conditionSQL.toString());
		sql.append(" order by so.sale_order_id desc");

		final String[] fieldNames = new String[] { //
		"amount", "orderDate", "orderNumber", "total", "customerId", "shortCode", "customerCode", "customerName", "customerAddress", "customerPhone", "staffId", "staffCode", "staffName", "deliveryId", "deliveryCode", "deliveryName", "discount",
				"isInvoice", "saleOrderId" };

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.DATE, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG };

		//		System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));
		System.out.println(sql.toString());
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(SaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(SaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SaleOrderDAO#getListSaleOrderWithCondition(java.
	 * lang.Float, java.lang.String, java.util.Date, java.util.Date,
	 * java.lang.String, java.lang.String, int)
	 */

	@Override
	public List<SaleOrder> getListSaleOrderWithCondition(KPaging<SaleOrder> kPaging, Float vat, String orderNumber, Date fromDate, Date toDate, String customerCode, String staffCode, Boolean hasInvoice) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<SaleOrder>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.* from sale_order so, customer c, staff s, invoice iv");
		sql.append(" where 1 = 1");
		sql.append(" and so.customer_id = c.customer_id");
		sql.append(" and so.staff_id = s.staff_id");
		if (hasInvoice == null || hasInvoice == false) {
			sql.append(" and so.invoice_id is null");
			sql.append(" and so.order_type in (?,?)");
			params.add(OrderType.IN.getValue());
			params.add(OrderType.SO.getValue());
			sql.append(" and so.approved = ?");
			params.add(SaleOrderStatus.APPROVED.getValue());
			sql.append(" and so.type = ?");
			params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		} else {
			sql.append(" and so.invoice_id = iv.invoice_id");
			sql.append(" and iv.status in (?,?)");
			params.add(InvoiceStatus.USING.getValue());
			params.add(InvoiceStatus.MODIFIED.getValue());
		}
		if (vat != null) {
			sql.append(" and so.vat = ?");
			params.add(vat);
		}
		if (!StringUtility.isNullOrEmpty(orderNumber)) {
			sql.append(" and lower(so.order_number) = ?");
			params.add(orderNumber.toLowerCase());
		}
		if (fromDate != null) {
			sql.append(" and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append(" and lower(c.customer_code) = ?");
			params.add(customerCode.toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and lower(s.staff_code) = ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode.toLowerCase()));
		}
		if (kPaging == null) {
			return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, kPaging);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SaleOrderDAO#getListSaleOrder(ths.dms.core.entities
	 * .enumtype.KPaging, java.lang.String, java.lang.String, java.lang.String,
	 * ths.dms.core.entities.enumtype.ApprovalStatus,
	 * ths.dms.core.entities.enumtype.OrderType, java.util.Date,
	 * java.util.Date, java.lang.String, java.lang.String)
	 */

	@Override
	public List<SaleOrder> getListSaleOrder(SoFilter filter) throws DataAccessException {

		if (filter.getFromDate() != null && filter.getToDate() != null && DateUtility.compareTwoDate(filter.getFromDate(), filter.getToDate()) > 0) {
			return new ArrayList<SaleOrder>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.* from sale_order so ");
		if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
			sql.append(" JOIN staff s2 ON s2.staff_id = so.delivery_id ");
		}
		sql.append(" , staff s1");
		if (!StringUtility.isNullOrEmpty(filter.getPriorityCode())) {
			sql.append(", ap_param pr");
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomeName()) || !StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(", customer c");
		}
		sql.append(" where so.staff_id = s1.staff_id ");
		if (!StringUtility.isNullOrEmpty(filter.getPriorityCode())) {
			sql.append(" and so.priority = pr.ap_param_id AND pr.ap_param_code = ?");
			params.add(filter.getPriorityCode());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomeName()) || !StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and so.customer_id = c.customer_id and c.status in (0,1)");
		}
		if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "so.staff_id");
			sql.append(paramsFix);
		}
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and lower(c.short_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShortCode().toLowerCase()));
		}

		if (filter.getCustomerId() != null) {
			sql.append(" and so.customer_id = ?");
			params.add(filter.getCustomerId());
		}

		if (filter.getShopId() != null) {
			sql.append(" and so.shop_id = ?");
			params.add(filter.getShopId());
		}

		if (Boolean.TRUE.equals(filter.getIsFromSaleOrderNull())) {
			sql.append(" and so.from_sale_order_id is null");
		} else if (Boolean.FALSE.equals(filter.getIsFromSaleOrderNull())) {
			sql.append(" and so.from_sale_order_id is not null");
		}

		if (!StringUtility.isNullOrEmpty(filter.getCustomeName())) {
			sql.append(" and lower(c.customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getCustomeName().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and lower(so.order_number) like ? escape'/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber().toLowerCase()));
		}
		if (filter.getApproval() != null) {
			sql.append(" and so.approved = ?");
			params.add(filter.getApproval().getValue());
		}
		if (filter.getApprovedStep() != null) {
			sql.append(" and so.approved_step = ?");
			params.add(filter.getApprovedStep().getValue());
		}
		if (filter.getApproveVan() != null) {
			sql.append(" and so.approved_van = ?");
			params.add(filter.getApproveVan());
		}
		if (filter.getListOrderType() != null) {
			sql.append(" and (");
			int i = 0;
			for (OrderType orderType : filter.getListOrderType()) {
				if (i == 0) {
					sql.append(" so.order_type = ?");
				} else {
					sql.append(" or so.order_type = ?");
				}
				params.add(orderType.getValue());
				i++;
			}
			sql.append(")");
		}
		if (filter.getCheckDefault() != null && filter.getCheckDefault() == true) {
			sql.append(" and so.order_type not in ('CO')");
		}
		if (Boolean.TRUE.equals(filter.getCheckDueDate())) {
			sql.append(" and (not exists (select value from ap_param where status = 1 and ap_param_code = 'NTKH' and type = 'TH' and value > 0) or trunc(sysdate) - trunc(order_date) <= (select value from ap_param where status = 1 and ap_param_code = 'NTKH' and type = 'TH'))");
		}
		/** @tientv11 order_date=> create_date */
		if (filter.getFromDate() != null) {
			sql.append(" and so.order_date >= trunc(?)");
			params.add(filter.getFromDate());
		}

		if (filter.getToDate() != null) {
			sql.append(" and so.order_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		if (filter.getDeliveryDate() != null) {
			sql.append(" and so.delivery_date >= trunc(?) and so.delivery_date < trunc(?) + 1");
			params.add(filter.getDeliveryDate());
			params.add(filter.getDeliveryDate());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and lower(s1.staff_code) like ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
			sql.append(" and so.delivery_id = s2.staff_id");
			sql.append(" and lower(s2.staff_code) like ?"); //tungmt : sua tim kiem like
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDeliveryCode().toLowerCase()));
		}
		if (filter.getSaleOrderType() != null) {
			sql.append(" and so.type = ?");
			params.add(filter.getSaleOrderType().getValue());
		}
		if (filter.getOrderSource() != null) {
			sql.append(" and so.ORDER_SOURCE = ?");
			params.add(filter.getOrderSource().getValue());
		}
		if (filter.getApprovedStep() != null) {
			sql.append(" and so.approved_step= ? ");
			params.add(filter.getApprovedStep().getValue());
		}
		sql.append(" and so.order_type not in (?,?) ");
		params.add(OrderType.AD.getValue());
		params.add(OrderType.AI.getValue());

		if (Boolean.TRUE.equals(filter.getIsCheckOrderDateDesc())) {
			sql.append(" order by (so.order_date) desc, so.order_number");
		} else {
			sql.append(" order by trunc(so.order_date), so.order_number ");
		}
		//sql.append(" order by so.order_date, so.order_number ");//loctt-Oct4, 2013- sua order_date thanh create_date
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, filter.getkPaging());
		}
	}

	/**
	 * @author sangtn
	 * @since 11-06-2014
	 * @description clone from getListSaleOrder, change result from Entity to VO
	 * @note for search sale order, sale order manager screen:
	 *       SearchSaleTransactionAction->searchOrder()
	 */
	@Override
	public List<SaleOrderVOEx2> getListSaleOrderEx(KPaging<SaleOrderVOEx2> kPaging, SoFilter filter) throws DataAccessException {

		if (filter.getFromDate() != null && filter.getToDate() != null && DateUtility.compareTwoDate(filter.getFromDate(), filter.getToDate()) > 0)
			return new ArrayList<SaleOrderVOEx2>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.sale_order_id id ");
		sql.append(" , so.ref_order_number refOrderNumber ");
		sql.append(" , so.approved  ");
		sql.append(" , so.order_number orderNumber ");
		sql.append(" , so.priority ");
		sql.append(" , so.staff_id staffId ");
		sql.append(" , (select staff_code from staff s where s.staff_id = so.staff_id) staffCode ");
		sql.append(" , (select staff_name from staff s where s.staff_id = so.staff_id) staffName ");

		sql.append(" , so.customer_id customerId ");
		sql.append(" , (select cu.short_code from customer cu where cu.customer_id = so.customer_id) shortCode ");
		sql.append(" , (select cu.customer_name from customer cu where cu.customer_id = so.customer_id) customerName ");
		sql.append(" , (select cu.address from customer cu where cu.customer_id = so.customer_id) customerAddress ");

		sql.append(" , so.total  ");
		sql.append(" , so.order_date orderDate ");
		sql.append(" , so.delivery_date deliveryDate ");

		sql.append(" , so.type ");
		sql.append(" , so.order_type orderType");
		sql.append(" , (select soTmp.order_number from sale_order soTmp where soTmp.sale_order_id = so.from_sale_order_id) fromSaleOrderCode ");

		sql.append(" , so.delivery_id deliveryId ");
		sql.append(" , (select staff_code from staff s where s.staff_id = so.delivery_id) deliveryCode ");
		sql.append(" , (select staff_name from staff s where s.staff_id = so.delivery_id) deliveryName ");
		//sql.append(" select so.*  ");		
		sql.append(" from sale_order so ");
		if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
			sql.append(" JOIN staff s2 ON s2.staff_id = so.delivery_id ");
		}
		sql.append(" , staff s1");
		if (!StringUtility.isNullOrEmpty(filter.getPriorityCode())) {
			sql.append(", ap_param pr");
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomeName()) || !StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(", customer c");
		}
		sql.append(" where so.staff_id = s1.staff_id ");
		if (!StringUtility.isNullOrEmpty(filter.getPriorityCode())) {
			sql.append(" and so.priority = pr.ap_param_id AND pr.ap_param_code = ?");
			params.add(filter.getPriorityCode());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomeName()) || !StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and so.customer_id = c.customer_id");
		}
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and lower(c.short_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShortCode().toLowerCase()));
		}

		if (filter.getCustomerId() != null) {
			sql.append(" and so.customer_id = ?");
			params.add(filter.getCustomerId());
		}

		if (filter.getShopId() != null) {
			sql.append(" and so.shop_id = ?");
			params.add(filter.getShopId());
		}

		if (Boolean.TRUE.equals(filter.getIsFromSaleOrderNull())) {
			sql.append(" and so.from_sale_order_id is null");
		} else if (Boolean.FALSE.equals(filter.getIsFromSaleOrderNull())) {
			sql.append(" and so.from_sale_order_id is not null");
		}

		if (!StringUtility.isNullOrEmpty(filter.getCustomeName())) {
			sql.append(" and lower(c.customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getCustomeName().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and lower(so.order_number) like ? escape'/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber().toLowerCase()));
		}
		if (filter.getApproval() != null) {
			sql.append(" and so.approved = ?");
			params.add(filter.getApproval().getValue());
		}
		if (filter.getApproveVan() != null) {
			sql.append(" and so.approved_van = ?");
			params.add(filter.getApproveVan());
		}
		if (filter.getListOrderType() != null) {
			sql.append(" and (");
			int i = 0;
			for (OrderType orderType : filter.getListOrderType()) {
				if (i == 0) {
					sql.append(" so.order_type = ?");
				} else {
					sql.append(" or so.order_type = ?");
				}
				params.add(orderType.getValue());
				i++;
			}
			sql.append(")");
		}
		if (filter.getCheckDefault() != null && filter.getCheckDefault() == true) {
			sql.append(" and so.order_type not in ('CO')");
		}
		if (Boolean.TRUE.equals(filter.getCheckDueDate())) {
			sql.append(" and (not exists (select value from ap_param where status = 1 and ap_param_code = 'NTKH' and type = 'TH' and value > 0) or trunc(sysdate) - trunc(order_date) <= (select value from ap_param where status = 1 and ap_param_code = 'NTKH' and type = 'TH'))");
		}
		/** @tientv11 order_date=> create_date */
		if (filter.getFromDate() != null) {
			sql.append(" and so.order_date >= trunc(?)");
			params.add(filter.getFromDate());
		}

		if (filter.getToDate() != null) {
			sql.append(" and so.order_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		if (filter.getDeliveryDate() != null) {
			sql.append(" and so.delivery_date >= trunc(?) and so.delivery_date < trunc(?) + 1");
			params.add(filter.getDeliveryDate());
			params.add(filter.getDeliveryDate());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and lower(s1.staff_code) like ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
			sql.append(" and so.delivery_id = s2.staff_id");
			sql.append(" and lower(s2.staff_code) like ?"); //tungmt : sua tim kiem like
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDeliveryCode().toLowerCase()));
		}
		if (filter.getSaleOrderType() != null) {
			sql.append(" and so.type = ?");
			params.add(filter.getSaleOrderType().getValue());
		}
		if (filter.getOrderSource() != null) {
			sql.append(" and so.ORDER_SOURCE = ?");
			params.add(filter.getOrderSource().getValue());
		}

		sql.append(" order by so.order_date, so.order_number ");

		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql.toString());
		countSql.append(")");

		System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));

		final String[] fieldNames = new String[] { "id", "refOrderNumber", "approved", "orderNumber", "priority", "staffId", "staffCode", "staffName", "customerId", "shortCode", "customerName", "customerAddress", "total", "orderDate",
				"deliveryDate", "type", "orderType", "fromSaleOrderCode", "deliveryId", "deliveryCode", "deliveryName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.TIMESTAMP,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(SaleOrderVOEx2.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(SaleOrderVOEx2.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}

	/**
	 * Danh sach don hang can xac nhan
	 * 
	 * @author tungmt
	 */
	@Override
	public List<SaleOrder> getListSaleOrderConfirm(SoFilter filter) throws DataAccessException {

		if (filter.getLockDay() == null) {
			filter.setLockDay(commonDAO.getSysDate());
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.*,(select nvl(sum(discount_amount),0) from sale_order_promo_detail where sale_order_id=so.sale_order_id and is_owner=2) saleOrderDiscount, ");
		sql.append(" (select nvl(sum(discount_amount),0) from sale_order_promo_detail where sale_order_id=so.sale_order_id and is_owner in (0,1)) totalDiscount ");
		sql.append(" from sale_order so join ap_param ap on ap.ap_param_id = so.priority ");
		sql.append(" where 1=1 ");
		sql.append(" and so.approved = 0 ");
		if (filter.getApprovedStep() != null) {
			sql.append(" and so.approved_step= ? ");
			params.add(filter.getApprovedStep().getValue());
		}
		if (filter.getLstSaleOrderId() != null) {
			sql.append(" and so.sale_order_id in (-1");
			for (Long orderId : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(orderId);
			}
			sql.append(") ");
		}
		if (filter.getLockDay() != null) {
//			sql.append(" and so.order_date>=trunc(?) ");
//			params.add(filter.getLockDay());
			sql.append(" and so.order_date<trunc(?)+1 ");
			params.add(filter.getLockDay());
		}
		if (filter.getStaffId() != null) {
			sql.append(" and so.staff_id=? ");
			params.add(filter.getStaffId());
		}
		if (filter.getShopId() != null) {
			sql.append(" and so.shop_id = ?");
			params.add(filter.getShopId());
		}

		if (filter.getListOrderType() != null && filter.getListOrderType().size() > 0) {
			sql.append(" and so.order_type in (");
			int i = 0;
			for (OrderType orderType : filter.getListOrderType()) {
				if (i == 0) {
					sql.append("?");
				} else {
					sql.append(",?");
				}
				params.add(orderType.getValue());
				i++;
			}
			sql.append(") ");
		}
		if (filter.getOrderSource() != null) {
			sql.append(" and so.ORDER_SOURCE = ?");
			params.add(filter.getOrderSource().getValue());
		}

		// lacnv1 - kiem tra neu don vansale chi xac nhan (huy, tu choi) don cua nhan vien da chot kho
//		sql.append(" and (so.order_type not in (?, ?) or exists (select 1 from stock_lock where staff_id = so.staff_id and van_lock = 1");
//		params.add(OrderType.SO.getValue());
//		params.add(OrderType.CO.getValue());
//		sql.append(" and create_date >= trunc(?) ");
//		params.add(filter.getLockDay());
//		sql.append(" and create_date < trunc(?)+1)");
//		params.add(filter.getLockDay());
//		sql.append(")");
		// --

		sql.append(" order by so.order_date, so.order_number ");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, filter.getkPaging());
		}
	}

	/**
	 * Danh sach don hang can xac nhan
	 * 
	 * @author tungmt
	 */
	@Override
	public List<SaleOrderVOEx2> getListSaleOrderVOConfirm(SoFilter filter) throws DataAccessException {

		if (filter.getLockDay() == null) {
			filter.setLockDay(commonDAO.getSysDate());
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.sale_order_id id, so.ref_Order_Number refOrderNumber, so.order_Date orderDate, so.delivery_Date deliveryDate,  ");
		sql.append(" so.total totalValue,so.order_number orderNumber, so.customer_id customerId, ");
		sql.append(" (select short_code from customer where customer_id=so.customer_id) customerCode, ");
		sql.append(" (select customer_name from customer where customer_id=so.customer_id) customerName, ");
		sql.append(" (select address from customer where customer_id=so.customer_id) address, ");
		sql.append(" (select staff_code from staff where staff_id = so.staff_id) staffCode, ");
		sql.append(" (select staff_name from staff where staff_id = so.staff_id) staffName, ");
		sql.append(" (select ap_param_name from ap_param where ap_param_id = so.priority) priorityStr, ");
		sql.append(" (select nvl(sum(discount_amount),0) from sale_order_promo_detail where sale_order_id=so.sale_order_id and is_owner=2) saleOrderDiscount, ");
		sql.append(" (select nvl(sum(discount_amount),0) from sale_order_promo_detail where sale_order_id=so.sale_order_id and is_owner in (0,1)) totalDiscount,");
		sql.append(" (case when so.order_type in (?, ?) then (select 1 from stock_lock where staff_id = so.staff_id and van_lock = 1");
		params.add(OrderType.SO.getValue());
		params.add(OrderType.CO.getValue());
//		params.add(OrderType.IN.getValue());
//		sql.append(" and create_date >= trunc(?) and create_date < trunc(?) + 1");
//		params.add(filter.getFromDate() != null ? filter.getFromDate() : filter.getLockDay());
//		params.add(filter.getLockDay());
		sql.append(" ) else 1 end) as lockedStock");
		sql.append(", (select staff_code from staff where staff_id = so.delivery_id) deliveryCode, ");
		sql.append(" (select staff_name from staff where staff_id = so.delivery_id) deliveryName ");
		sql.append(" from sale_order so join ap_param ap on ap.ap_param_id = so.priority ");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "so.staff_id");
			sql.append(paramsFix);
		}
		sql.append(" and so.approved = 0 ");
		if (filter.getApprovedStep() != null) {
			sql.append(" and so.approved_step= ? ");
			params.add(filter.getApprovedStep().getValue());
		}
		if (filter.getLstSaleOrderId() != null) {
			sql.append(" and so.sale_order_id in (-1");
			for (Long orderId : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(orderId);
			}
			sql.append(") ");
		}
		if (filter.getFromDate() != null) {
			sql.append(" and so.order_date >= trunc(?) ");
			params.add(filter.getFromDate() != null ? filter.getFromDate() : filter.getLockDay());
		}
		if (filter.getLockDay() != null) {
			sql.append(" and so.order_date < trunc(?) + 1  ");
			params.add(filter.getLockDay());
		}
		if (filter.getStaffId() != null) {
			sql.append(" and so.staff_id=? ");
			params.add(filter.getStaffId());
		}
		if (filter.getShopId() != null) {
			sql.append(" and so.shop_id = ?");
			params.add(filter.getShopId());
		}

		if (filter.getListOrderType() != null && filter.getListOrderType().size() > 0) {
			sql.append(" and so.order_type in (");
			int i = 0;
			//boolean isVansale = false;
			for (OrderType orderType : filter.getListOrderType()) {
				if (i == 0) {
					sql.append("?");
				} else {
					sql.append(",?");
				}
				/*
				 * if (OrderType.SO.getValue().equals(orderType.getValue()) ||
				 * OrderType.CO.getValue().equals(orderType.getValue())) {
				 * isVansale = true; }
				 */
				params.add(orderType.getValue());
				i++;
			}
			sql.append(") ");
			// Modified by NhanLT6 - 14/11/2014 - Bo sung check dieu kien voi don vansale ( SO,CO ) 
			// thi check them NVBH da chot ngay trong bang stock_lock hay chua ?
			/*
			 * if (isVansale) { sql.append(
			 * " and exists (select 1 from stock_lock where staff_id = so.staff_id and van_lock = 1  "
			 * ); sql.append(
			 * " and create_date >= TRUNC(?) AND create_date < TRUNC(?)+1 ) ");
			 * params.add(filter.getLockDay()); params.add(filter.getLockDay());
			 * }
			 */
		}
		if (filter.getOrderSource() != null) {
			sql.append(" and so.ORDER_SOURCE = ?");
			params.add(filter.getOrderSource().getValue());
		}

		if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
			sql.append(" and so.delivery_id in (select staff_id from staff where upper(staff_code) like ? escape '/' ) ");
			String deliveryStaffCode = filter.getDeliveryCode().toUpperCase();
			deliveryStaffCode = StringUtility.toOracleSearchLike(deliveryStaffCode);
			params.add(deliveryStaffCode);
		}
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and so.customer_id in (select customer_id from customer where upper(short_code) like ? escape '/' ) ");
			String customerShortCode = filter.getShortCode().toUpperCase();
			customerShortCode = StringUtility.toOracleSearchLike(customerShortCode);
			params.add(customerShortCode);
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomeName())) {
			sql.append(" and so.customer_id in (select customer_id from customer where upper(name_text) like ? escape '/' ) ");
			String customerName = filter.getCustomeName().toUpperCase();
			customerName = Unicode2English.codau2khongdau(customerName);
			customerName = StringUtility.toOracleSearchLike(customerName);
			params.add(customerName);
		}
		if (!StringUtility.isNullOrEmpty(filter.getPriorityCode())) {
			sql.append(" and so.priority = (select ap_param_id from ap_param where status = 1 and ap_param_code = ? and type = 'ORDER_PIRITY' and rownum = 1) ");
			params.add(filter.getPriorityCode());
		}
		if(filter.getIsValueOrder()!=null && filter.getNumberValueOrder() != null){
			if( filter.getIsValueOrder() == 0 ){
				sql.append(" and so.amount < ? ");
				params.add(filter.getNumberValueOrder());
			}
			if( filter.getIsValueOrder() == 1 ){
				sql.append(" and so.amount >= ? ");
				params.add(filter.getNumberValueOrder());
			}
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql);
		countSql.append(")");
		
		
		sql.append(" order by so.order_date, so.order_number ");
		final String[] fieldNames = new String[] { "id", "refOrderNumber", "orderDate", "deliveryDate", "totalValue", "orderNumber", "customerId", "customerCode", "customerName", "address", "staffCode", "staffName", "priorityStr",
				"saleOrderDiscount", "totalDiscount", "lockedStock", "deliveryCode", "deliveryName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (filter.getkPagingVOEx2() != null) {
			return repo.getListByQueryAndScalarPaginated(SaleOrderVOEx2.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVOEx2());
		}
		return repo.getListByQueryAndScalar(SaleOrderVOEx2.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<SaleOrderStockVO> getListSaleOrderStock(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws DataAccessException {

		if (filter.getFromDate() != null && filter.getToDate() != null && DateUtility.compareTwoDate(filter.getFromDate(), filter.getToDate()) > 0) {
			return new ArrayList<SaleOrderStockVO>();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select so.sale_order_id as id ");
		sql.append(" , (select shop_id from shop s where s.shop_id = so.shop_id) as shopId ");
		sql.append(" , so.staff_id as staffId ");
		sql.append(" , (select shop_code from shop s where s.shop_id = so.shop_id) as shopCode ");
		sql.append(" , so.order_number as orderNumber ");
		sql.append(" , so.order_date as orderDate ");
		sql.append(" , to_char(so.order_date, 'dd/mm/yyyy hh24:mi:ss') as strOrderDate ");
		sql.append(" , so.ref_order_number as refOrderNumber ");
		sql.append(" , cu.short_code as shortCode ");
		sql.append(" , cu.customer_name as customerName ");
		sql.append(" , cu.address as customerAddress ");
		sql.append(" , so.total as total ");
		sql.append(" , so.quantity as quantity ");
		sql.append(" , so.order_type orderType ");
		sql.append(" , (select staff_code from staff s where s.staff_id = so.staff_id) as staffCode ");
		sql.append(" , (select staff_name from staff s where s.staff_id = so.staff_id) as staffName ");
		sql.append(" , (select staff_code from staff s where s.staff_id = so.delivery_id) as deliveryCode ");
		sql.append(" , (select staff_name from staff s where s.staff_id = so.delivery_id) as deliveryName ");
		sql.append(" , so.order_source as orderSource ");
		sql.append(" from sale_order so ");
		sql.append(" join customer cu on cu.customer_id = so.customer_id ");
//		if (!StringUtility.isNullOrEmpty(filter.getShortCode()) || !StringUtility.isNullOrEmpty(filter.getCustomeName())) {
//			sql.append(" join customer cu on cu.customer_id = so.customer_id ");
//		}
		sql.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "so.staff_id");
			sql.append(paramsFix);
		}
		sql.append(" and so.order_type not in (?,?) ");
		params.add(OrderType.AI.getValue());
		/**
		 * khong lay AI, AD ; AI Dieu chinh tang - update cho Dieu chinh doanh thu
		 */
		params.add(OrderType.AD.getValue());
		/**
		 * khong lay AI, AD ; AD Dieu chinh giam - update cho Dieu chinh doanh thu
		 */
		/** order_date=> create_date */
		if (filter.getFromDate() != null) {
			sql.append(" and so.order_date >= trunc(?)");
			params.add(filter.getFromDate());
		}

		if (filter.getToDate() != null) {
			sql.append(" and so.order_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		sql.append(" and so.approved = 1 ");
		sql.append(" and so.approved_step = 2 ");
		if (filter.getShopId() != null) {
			sql.append(" and so.shop_id = ?");
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and so.order_number like ? escape'/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber().toUpperCase()));
		}
		if (filter.getOrderSource() != null) {
			sql.append(" and so.order_source = ?");
			params.add(filter.getOrderSource().getValue());
		}
		if (filter.getOrderType() != null) {
			sql.append(" and so.order_type in (?) ");
			params.add(filter.getOrderType().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and cu.short_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShortCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomeName())) {
			sql.append(" and unicode2english(cu.customer_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getCustomeName()).toLowerCase()));
		}
		if (filter.getStaffId() != null) {
			sql.append(" and so.staff_id = ? ");
			params.add(filter.getStaffId());
		}
		if (filter.getDeliveryId() != null) {
			sql.append(" and so.delivery_id = ? ");
			params.add(filter.getDeliveryId());
		}
		if (filter.getIsValueOrder() != null && filter.getNumberValueOrder() != null) {
			if (filter.getIsValueOrder() == 0) {
				sql.append(" and so.amount < ? ");
				params.add(filter.getNumberValueOrder());
			}
			if (filter.getIsValueOrder() == 1) {
				sql.append(" and so.amount >= ? ");
				params.add(filter.getNumberValueOrder());
			}
		}
		StringBuilder sqlTrans = new StringBuilder();
		List<Object> paramsTrans = new ArrayList<Object>();
		sqlTrans.append(" SELECT tmp.id, tmp.shopId, tmp.staffId, tmp.shopCode, tmp.orderNumber, tmp.orderDate,");
		sqlTrans.append("	to_char(tmp.orderDate, 'dd/mm/yyyy hh24:mi:ss') as strOrderDate, ");
		sqlTrans.append("   NULL AS refOrderNumber,");
		sqlTrans.append("   NULL AS shortCode,");
		sqlTrans.append("   NULL AS customerName,");
		sqlTrans.append("   NULL AS customerAddress,");
		sqlTrans.append("   tmp.total, SUM(std.quantity) quantity, tmp.orderType, tmp.staffCode,");
		sqlTrans.append("   NULL AS staffName,");
		sqlTrans.append("   NULL AS deliveryCode,");
		sqlTrans.append("   NULL AS deliveryName,");
		sqlTrans.append("   NULL AS orderSource  ");
		sqlTrans.append(" FROM ( ");
		sqlTrans.append(" select DISTINCT st.stock_trans_id as id ");
		sqlTrans.append(" , (select shop_id from shop s where s.shop_id = st.shop_id) as shopId ");
		sqlTrans.append(" , st.staff_id as staffId ");
		sqlTrans.append(" , (select shop_code from shop s where s.shop_id = st.shop_id) as shopCode ");
		sqlTrans.append(" , st.stock_trans_code as orderNumber ");
		sqlTrans.append(" , st.stock_trans_date as orderDate ");
		sqlTrans.append(" , st.total_amount as total ");
		sqlTrans.append(" , st.trans_type orderType ");
		sqlTrans.append(" , to_char(case when stl.from_owner_type = ? ");
		paramsTrans.add(StockTransLotOwnerType.STAFF.getValue());
		sqlTrans.append("  	then (select staff_code ||'-'|| staff_name from staff where staff_id = stl.from_owner_id)  ");
		sqlTrans.append(" 	when stl.to_owner_type = ? then");
		paramsTrans.add(StockTransLotOwnerType.STAFF.getValue());
		sqlTrans.append(" 	 (select staff_code ||'-'|| staff_name from staff where staff_id = stl.to_owner_id) else null end) ");
		sqlTrans.append("  staffCode ");
		sqlTrans.append(" , case when stl.from_owner_type = ? ");
		paramsTrans.add(StockTransLotOwnerType.STAFF.getValue());
		sqlTrans.append("  	then stl.from_owner_id  ");
		sqlTrans.append(" 	when stl.to_owner_type = ? ");
		paramsTrans.add(StockTransLotOwnerType.STAFF.getValue());
		sqlTrans.append(" 	then stl.to_owner_id else st.staff_id end ");
		sqlTrans.append("  staffId2 ");
		sqlTrans.append(" from stock_trans st ");
		sqlTrans.append(" join stock_trans_lot stl on stl.stock_trans_id = st.stock_trans_id ");
		sqlTrans.append(" where 1 = 1 ");
		/** order_date=> create_date */
		if (filter.getFromDate() != null) {
			sqlTrans.append(" and st.stock_trans_date >= trunc(?)");
			paramsTrans.add(filter.getFromDate());
		}

		if (filter.getToDate() != null) {
			sqlTrans.append(" and st.stock_trans_date < trunc(?) + 1");
			paramsTrans.add(filter.getToDate());
		}
		sqlTrans.append(" and st.approved = 1 ");
		sqlTrans.append(" and st.approved_step = 2 ");
		if (filter.getShopId() != null) {
			sqlTrans.append(" and st.shop_id = ?");
			paramsTrans.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sqlTrans.append(" and st.stock_trans_code like ? escape'/'");
			paramsTrans.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber().toUpperCase()));
		}
		if (filter.getOrderType() != null) {
			sqlTrans.append(" and st.trans_type in (?) ");
			paramsTrans.add(filter.getOrderType().getValue());
		} else {
			//sqlTrans.append(" and st.trans_type not in ('DC','DCT','DCG') ");
			sqlTrans.append(" and st.trans_type in ('DP','GO') ");
		}
		if (filter.getIsValueOrder() != null && filter.getNumberValueOrder() != null) {
			if (filter.getIsValueOrder() == 0) {
				sqlTrans.append(" and st.total_amount < ? ");
				paramsTrans.add(filter.getNumberValueOrder());
			}
			if (filter.getIsValueOrder() == 1) {
				sqlTrans.append(" and st.total_amount >= ? ");
				paramsTrans.add(filter.getNumberValueOrder());
			}
		}	
		if (filter.getStaffId() != null) {
			sqlTrans.append(" and ( ");
			sqlTrans.append(" (stl.to_owner_type = ? and stl.to_owner_id = ?) ");
			sqlTrans.append("  or (stl.from_owner_type = ? and stl.from_owner_id = ?) ");
			sqlTrans.append("  ) ");
			paramsTrans.add(StockTransLotOwnerType.STAFF.getValue());
			paramsTrans.add(filter.getStaffId());
			paramsTrans.add(StockTransLotOwnerType.STAFF.getValue());
			paramsTrans.add(filter.getStaffId());
		}
		
		sqlTrans.append(" ) tmp");
		sqlTrans.append(" JOIN stock_trans_detail std ON tmp.id = std.stock_trans_id");
		sqlTrans.append(" WHERE 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "tmp.staffId2");
			sqlTrans.append(paramsFix);
		}
		sqlTrans.append(" GROUP BY tmp.id, tmp.shopId, tmp.staffId, tmp.shopCode, tmp.orderNumber, tmp.orderDate, tmp.total, tmp.orderType, tmp.staffCode");
		
		final String[] fieldNames = new String[] { "id", "shopId", "staffId", "shopCode", "orderNumber", "orderDate", "strOrderDate", "refOrderNumber", "shortCode", "customerName", "customerAddress", "total", "quantity", "orderType", "staffCode", "staffName", "deliveryCode",
				"deliveryName", "orderSource" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };
		if (!StringUtility.isNullOrEmpty(filter.getShortCode()) || !StringUtility.isNullOrEmpty(filter.getCustomeName()) || filter.getDeliveryId() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			sql.append(" order by shopCode, orderNumber ");
			if (kPaging == null) {
				return repo.getListByQueryAndScalar(SaleOrderStockVO.class, fieldNames, fieldTypes, sql.toString(), params);
			} else {
				return repo.getListByQueryAndScalarPaginated(SaleOrderStockVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
			}
		} else if (filter.getOrderType() != null) {
			if (filter.getOrderType().equals(OrderType.IN) || filter.getOrderType().equals(OrderType.CM) || filter.getOrderType().equals(OrderType.SO) || filter.getOrderType().equals(OrderType.CO)) {
				StringBuilder countSql = new StringBuilder();
				countSql.append("select count(1) as count from (");
				countSql.append(sql.toString());
				countSql.append(")");
				sql.append(" order by shopCode, orderNumber ");
				if (kPaging == null) {
					return repo.getListByQueryAndScalar(SaleOrderStockVO.class, fieldNames, fieldTypes, sql.toString(), params);
				} else {
					return repo.getListByQueryAndScalarPaginated(SaleOrderStockVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
				}
			} else {
				/*
				 * if (filter.getOrderType().equals(OrderType.GO) ||
				 * filter.getOrderType().equals(OrderType.DP) ||
				 * filter.getOrderType().equals(OrderType.DC) ||
				 * filter.getOrderType().equals(OrderType.DCT) ||
				 * filter.getOrderType().equals(OrderType.DCG) ){
				 */
				StringBuilder countSql = new StringBuilder();
				countSql.append("select count(1) as count from (");
				countSql.append(sqlTrans.toString());
				countSql.append(")");
				sqlTrans.append(" order by shopCode, orderNumber ");
				if (kPaging == null) {
					return repo.getListByQueryAndScalar(SaleOrderStockVO.class, fieldNames, fieldTypes, sqlTrans.toString(), paramsTrans);
				} else {
					return repo.getListByQueryAndScalarPaginated(SaleOrderStockVO.class, fieldNames, fieldTypes, sqlTrans.toString(), countSql.toString(), paramsTrans, paramsTrans, kPaging);
				}
			}
		} else {
			// tat ca order type = null
			StringBuilder sqlData = new StringBuilder();
			//List<Object> paramsData = new ArrayList<Object>();
			sqlData.append(" with saleOrder as( ");
			sqlData.append(sql.toString());
			sqlData.append(" ),  ");
			sqlData.append(" stockTrans as( ");
			sqlData.append(sqlTrans);
			sqlData.append(" ),  ");
			sqlData.append(" result as ( ");
			sqlData.append(" select * from saleOrder ");
			sqlData.append(" UNION ");
			sqlData.append(" select * from stockTrans ");
			sqlData.append(" ) ");
			StringBuilder countSql = new StringBuilder();
			countSql.append(sqlData);
			countSql.append(" select count(1) as count from result ");
			sqlData.append(" select * from result order by shopCode, orderNumber ");
			params.addAll(paramsTrans);
			if (kPaging == null) {
				return repo.getListByQueryAndScalar(SaleOrderStockVO.class, fieldNames, fieldTypes, sqlData.toString(), params);
			} else {
				return repo.getListByQueryAndScalarPaginated(SaleOrderStockVO.class, fieldNames, fieldTypes, sqlData.toString(), countSql.toString(), params, params, kPaging);
			}
		}
	}
	
	@Override
	public List<SaleOrderStockVO> getListSaleOrderStockUpdate(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null && DateUtility.compareTwoDate(filter.getFromDate(), filter.getToDate()) > 0) {
			return new ArrayList<SaleOrderStockVO>();
		}
		StringBuilder sqlTrans = new StringBuilder();
		List<Object> paramsTrans = new ArrayList<Object>();
		sqlTrans.append(" WITH ");
		if(filter.getShopId() != null) {
			sqlTrans.append(" lstShop AS ");
			sqlTrans.append("   (SELECT shop_id ");
			sqlTrans.append("   FROM shop ");
			sqlTrans.append("   WHERE status               = ? ");
			paramsTrans.add(ActiveType.RUNNING.getValue());
			if(!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
				String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "shop_id");
				sqlTrans.append(paramsFix);
			}
			sqlTrans.append("     START WITH shop_id       = ? ");
			paramsTrans.add(filter.getShopId());
			sqlTrans.append("     CONNECT BY prior shop_id = parent_shop_id ");
			sqlTrans.append("   ) ");
		}
		sqlTrans.append(" SELECT st.stock_trans_id id, ");
		sqlTrans.append(" 	st.shop_id shopId, ");
		sqlTrans.append(" 	NVL(s1.shop_code, s2.shop_code) shopCode, ");
		sqlTrans.append("   st.STOCK_TRANS_CODE orderNumber, ");
		sqlTrans.append("   st.trans_type orderType, ");
		sqlTrans.append("   st.STOCK_TRANS_DATE orderDate, ");
		sqlTrans.append("   SUM(stl.quantity) quantity ");
		
//		sqlTrans.append("   , ");
//		sqlTrans.append("   p.convfact convfact, ");
//		sqlTrans.append("   w1.WAREHOUSE_NAME fromWarehouse, ");
//		sqlTrans.append("   s1.SHOP_CODE fromShop, ");		
//		sqlTrans.append("   w2.WAREHOUSE_NAME toWarehouse, ");
//		sqlTrans.append("   s2.SHOP_CODE toShop");
		
		sqlTrans.append(" FROM stock_trans st ");
		sqlTrans.append(" JOIN STOCK_TRANS_LOT stl ON st.STOCK_TRANS_ID = stl.STOCK_TRANS_ID ");
		sqlTrans.append(" JOIN product p ON stl.product_id = p.product_id ");
		sqlTrans.append(" LEFT JOIN WAREHOUSE w1 ON stl.FROM_OWNER_ID = w1.WAREHOUSE_ID AND stl.FROM_OWNER_TYPE = 1 ");
		sqlTrans.append(" LEFT JOIN SHOP      s1 ON w1.SHOP_ID        = s1.SHOP_ID ");
		sqlTrans.append(" LEFT JOIN WAREHOUSE w2 ON stl.TO_OWNER_ID   = w2.WAREHOUSE_ID AND stl.TO_OWNER_TYPE = 1 ");
		sqlTrans.append(" LEFT JOIN SHOP      s2 ON w2.SHOP_ID        = s2.SHOP_ID ");
		sqlTrans.append(" where 1 = 1 ");
		if (filter.getFromDate() != null) {
			sqlTrans.append(" and st.stock_trans_date >= trunc(?)");
			paramsTrans.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sqlTrans.append(" and st.stock_trans_date < trunc(?) + 1");
			paramsTrans.add(filter.getToDate());
		}
		sqlTrans.append(" and st.approved = ? ");
		paramsTrans.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
		sqlTrans.append(" and st.approved_step = ? ");
		paramsTrans.add(SaleOrderStep.CONFIRMED.getValue());
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sqlTrans.append(" and lower(st.stock_trans_code) like ? escape'/'");
			paramsTrans.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber().toLowerCase()));
		}
		if (filter.getOrderType() != null) {
			sqlTrans.append(" and st.trans_type = ? ");
			paramsTrans.add(filter.getOrderType().getValue());
		}
		if (filter.getListOrderType() != null && filter.getListOrderType().size() > 0) {
			sqlTrans.append(" and st.trans_type in ('' ");
			for (OrderType type : filter.getListOrderType()) {
				sqlTrans.append(", ? ");
				paramsTrans.add(type.getValue());
			}
			sqlTrans.append(" ) ");
		}
		
		if (filter.getShopId() != null) {
			sqlTrans.append(" and ( 1 != 1 ");
			if ((filter.getOrderType() != null && OrderType.DCT.equals(filter.getOrderType()))
					|| (filter.getListOrderType() != null && filter.getListOrderType().size() > 0 && filter.getListOrderType().contains(OrderType.DCT)) ) {
				sqlTrans.append(" or (st.shop_id in (select shop_id from lstShop) and st.trans_type = ?) ");
				paramsTrans.add(OrderType.DCT.getValue());
			}
			if ((filter.getOrderType() != null && OrderType.DCG.equals(filter.getOrderType()))
					|| (filter.getListOrderType() != null && filter.getListOrderType().size() > 0 && filter.getListOrderType().contains(OrderType.DCG)) ) {
				sqlTrans.append(" or (st.shop_id in (select shop_id from lstShop) and st.trans_type = ?) ");
				paramsTrans.add(OrderType.DCG.getValue());
			}
			if ((filter.getOrderType() != null && OrderType.DC.equals(filter.getOrderType()))
					|| (filter.getListOrderType() != null && filter.getListOrderType().size() > 0 && filter.getListOrderType().contains(OrderType.DC)) ) { 
				sqlTrans.append(" or ((w1.shop_id in (select shop_id from lstShop) OR w2.shop_id in (select * from lstShop)) and st.trans_type = ? ) ");
				paramsTrans.add(OrderType.DC.getValue());
			}
			sqlTrans.append(" ) ");
		}
		sqlTrans.append(" GROUP BY st.stock_trans_id, ");
		sqlTrans.append(" 	st.shop_id, ");
		sqlTrans.append("   st.STOCK_TRANS_CODE, ");
		sqlTrans.append("   st.trans_type, ");
		sqlTrans.append("   st.STOCK_TRANS_DATE, ");
		
//		sqlTrans.append("   p.convfact, ");
//		sqlTrans.append("   w1.WAREHOUSE_NAME, ");
		sqlTrans.append("   s1.SHOP_CODE, ");
//		sqlTrans.append("   w2.WAREHOUSE_NAME, ");
		sqlTrans.append("   s2.SHOP_CODE ");
		
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sqlTrans.toString());
		countSql.append(")");
		
		sqlTrans.append(" ORDER BY st.stock_trans_code, st.trans_type ");
		
		final String[] fieldNames = new String[] {
				"id", "shopId", "shopCode", "orderNumber", 
				"orderType", "orderDate", "quantity"
//				, "convfact", "fromWarehouse", "fromShop", "toWarehouse", "toShop"
				};

		final Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.BIG_DECIMAL
//				, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				};
				
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(SaleOrderStockVO.class, fieldNames, fieldTypes, sqlTrans.toString(), paramsTrans);
		} else {
			return repo.getListByQueryAndScalarPaginated(SaleOrderStockVO.class, fieldNames, fieldTypes, sqlTrans.toString(), countSql.toString(), paramsTrans, paramsTrans, kPaging);
		}
	}
	
	@Override
	public List<SaleOrderStockVO> getListSaleOrderStockWarehouseUpdate(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws DataAccessException {
		if (filter.getFromDate() != null && filter.getToDate() != null && DateUtility.compareTwoDate(filter.getFromDate(), filter.getToDate()) > 0) {
			return new ArrayList<SaleOrderStockVO>();
		}
		StringBuilder sqlTrans = new StringBuilder();
		List<Object> paramsTrans = new ArrayList<Object>();
		sqlTrans.append(" WITH ");
		if(filter.getShopId() != null) {
			sqlTrans.append(" lstShop AS ");
			sqlTrans.append("   (SELECT shop_id ");
			sqlTrans.append("   FROM shop ");
			sqlTrans.append("   WHERE status               = ? ");
			paramsTrans.add(ActiveType.RUNNING.getValue());
			if(!StringUtility.isNullOrEmpty(filter.getStrListShopId())) {
				String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListShopId(), "shop_id");
				sqlTrans.append(paramsFix);
			}
			sqlTrans.append("     START WITH shop_id       = ? ");
			paramsTrans.add(filter.getShopId());
			sqlTrans.append("     CONNECT BY prior shop_id = parent_shop_id ");
			sqlTrans.append("   ) ");
		}
		sqlTrans.append(" SELECT st.stock_trans_id id, ");
		sqlTrans.append("   w1.WAREHOUSE_NAME fromWarehouse, ");
		sqlTrans.append("   s1.SHOP_NAME fromShop, ");		
		sqlTrans.append("   w2.WAREHOUSE_NAME toWarehouse, ");
		sqlTrans.append("   s2.SHOP_NAME toShop");
		
		sqlTrans.append(" FROM stock_trans st ");
		sqlTrans.append(" JOIN STOCK_TRANS_LOT stl ON st.STOCK_TRANS_ID = stl.STOCK_TRANS_ID ");
		sqlTrans.append(" JOIN product p ON stl.product_id = p.product_id ");
		sqlTrans.append(" LEFT JOIN WAREHOUSE w1 ON stl.FROM_OWNER_ID = w1.WAREHOUSE_ID AND stl.FROM_OWNER_TYPE = 1 ");
		sqlTrans.append(" LEFT JOIN SHOP      s1 ON w1.SHOP_ID        = s1.SHOP_ID ");
		sqlTrans.append(" LEFT JOIN WAREHOUSE w2 ON stl.TO_OWNER_ID   = w2.WAREHOUSE_ID AND stl.TO_OWNER_TYPE = 1 ");
		sqlTrans.append(" LEFT JOIN SHOP      s2 ON w2.SHOP_ID        = s2.SHOP_ID ");
		sqlTrans.append(" where 1 = 1 ");
		if (filter.getFromDate() != null) {
			sqlTrans.append(" and st.stock_trans_date >= trunc(?)");
			paramsTrans.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sqlTrans.append(" and st.stock_trans_date < trunc(?) + 1");
			paramsTrans.add(filter.getToDate());
		}
		sqlTrans.append(" and st.approved = ? ");
		paramsTrans.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
		sqlTrans.append(" and st.approved_step = ? ");
		paramsTrans.add(SaleOrderStep.CONFIRMED.getValue());
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sqlTrans.append(" and lower(st.stock_trans_code) like ? escape'/'");
			paramsTrans.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber().toLowerCase()));
		}
		if (filter.getOrderType() != null) {
			sqlTrans.append(" and st.trans_type = ? ");
			paramsTrans.add(filter.getOrderType().getValue());
		}
		if (filter.getListOrderType() != null && filter.getListOrderType().size() > 0) {
			sqlTrans.append(" and st.trans_type in ('' ");
			for (OrderType type : filter.getListOrderType()) {
				sqlTrans.append(", ? ");
				paramsTrans.add(type.getValue());
			}
			sqlTrans.append(" ) ");
		}
		
		if (filter.getShopId() != null) {
			sqlTrans.append(" and ( 1 != 1 ");
			if ((filter.getOrderType() != null && OrderType.DCT.equals(filter.getOrderType()))
					|| (filter.getListOrderType() != null && filter.getListOrderType().size() > 0 && filter.getListOrderType().contains(OrderType.DCT)) ) {
				sqlTrans.append(" or (st.shop_id in (select shop_id from lstShop) and st.trans_type = ?) ");
				paramsTrans.add(OrderType.DCT.getValue());
			}
			if ((filter.getOrderType() != null && OrderType.DCG.equals(filter.getOrderType()))
					|| (filter.getListOrderType() != null && filter.getListOrderType().size() > 0 && filter.getListOrderType().contains(OrderType.DCG)) ) {
				sqlTrans.append(" or (st.shop_id in (select shop_id from lstShop) and st.trans_type = ?) ");
				paramsTrans.add(OrderType.DCG.getValue());
			}
			if ((filter.getOrderType() != null && OrderType.DC.equals(filter.getOrderType()))
					|| (filter.getListOrderType() != null && filter.getListOrderType().size() > 0 && filter.getListOrderType().contains(OrderType.DC)) ) { 
				sqlTrans.append(" or ((w1.shop_id in (select shop_id from lstShop) OR w2.shop_id in (select * from lstShop)) and st.trans_type = ? ) ");
				paramsTrans.add(OrderType.DC.getValue());
			}
			sqlTrans.append(" ) ");
		}
		sqlTrans.append(" GROUP BY st.stock_trans_id, ");
		sqlTrans.append("   st.stock_trans_code, ");
		sqlTrans.append("   st.trans_type, ");
		sqlTrans.append("   w1.WAREHOUSE_NAME, ");
		sqlTrans.append("   s1.SHOP_NAME, ");
		sqlTrans.append("   w2.WAREHOUSE_NAME, ");
		sqlTrans.append("   s2.SHOP_NAME ");
		
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sqlTrans.toString());
		countSql.append(")");
		
		sqlTrans.append(" ORDER BY st.stock_trans_code, st.trans_type ");
		
		final String[] fieldNames = new String[] { "id", "fromWarehouse", "fromShop", "toWarehouse", "toShop" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, };
				
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(SaleOrderStockVO.class, fieldNames, fieldTypes, sqlTrans.toString(), paramsTrans);
		} else {
			return repo.getListByQueryAndScalarPaginated(SaleOrderStockVO.class, fieldNames, fieldTypes, sqlTrans.toString(), countSql.toString(), paramsTrans, paramsTrans, kPaging);
		}
	}

	@Override
	public List<SaleOrderStockVO> getListSaleOrderStockTrans(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws DataAccessException {

		if (filter.getFromDate() != null && filter.getToDate() != null && DateUtility.compareTwoDate(filter.getFromDate(), filter.getToDate()) > 0) {
			return new ArrayList<SaleOrderStockVO>();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select st.stock_trans_id as id ");
		sql.append(" , (select shop_code from shop s where s.shop_id = st.shop_id) as shopCode ");
		sql.append(" , st.stock_trans_code as orderNumber ");
		sql.append(" , st.stock_trans_date as orderDate ");
		sql.append(" , st.total_amount as total ");
		sql.append(" , st.trans_type orderType ");
		sql.append(" , (select staff_code from staff s where s.staff_id = st.staff_id) as staffCode ");
		sql.append(" , (select staff_name from staff s where s.staff_id = st.staff_id) as staffName ");
		sql.append("  ");
		sql.append(" from stock_trans st ");
		sql.append(" where 1 = 1 ");
		/** order_date=> create_date */
		if (filter.getFromDate() != null) {
			sql.append(" and st.stock_trans_date >= trunc(?)");
			params.add(filter.getFromDate());
		}

		if (filter.getToDate() != null) {
			sql.append(" and st.stock_trans_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		sql.append(" and st.approved = 1 ");
		sql.append(" and st.approved_step = 2 ");
		if (filter.getShopId() != null) {
			sql.append(" and st.shop_id = ?");
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and lower(st.stock_trans_code) like ? escape'/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber().toLowerCase()));
		}
		if (filter.getOrderType() != null) {
			sql.append(" and st.trans_type in (?) ");
			params.add(filter.getOrderType().getValue());
		}
		if (filter.getStaffId() != null) {
			sql.append(" and st.staff_id = ? ");
		}
		sql.append(" order by so.order_number, so.order_date");

		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql.toString());
		countSql.append(")");
		System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));
		final String[] fieldNames = new String[] { "id", "shopCode", "orderNumber", "orderDate", "total", "orderType", "staffCode", "staffName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING };

		if (kPaging == null) {
			return repo.getListByQueryAndScalar(SaleOrderStockVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(SaleOrderStockVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}

	/**
	 * Danh sach chi tiet don hang can xac nhan
	 * 
	 * @author tungmt
	 */
	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailConfirm(SoFilter filter) throws DataAccessException {

		if (filter.getLockDay() == null) {
			filter.setLockDay(commonDAO.getSysDate());
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sod.* from sale_order so ");
		sql.append(" join sale_order_detail sod on sod.sale_order_id=so.sale_order_id ");
		sql.append(" where 1=1 ");
		sql.append(" and so.approved=0 ");
		sql.append(" and so.approved_step=0 ");
		if (filter.getIsFreeItem() != null) {
			sql.append(" and sod.is_free_item=? ");
			params.add(filter.getIsFreeItem());
		}
		if (filter.getLstSaleOrderId() != null) {
			sql.append(" and so.sale_order_id in (-1");
			for (Long orderId : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(orderId);
			}
			sql.append(") ");
		}
		if (filter.getLockDay() != null) {
			sql.append(" and so.order_date>=trunc(?) and so.order_date<trunc(?)+1 ");
			params.add(filter.getLockDay());
			params.add(filter.getLockDay());
		}
		if (filter.getStaffId() != null) {
			sql.append(" and so.staff_id=? ");
			params.add(filter.getStaffId());
		}
		if (filter.getShopId() != null) {
			sql.append(" and so.shop_id = ?");
			params.add(filter.getShopId());
		}

		if (filter.getListOrderType() != null && filter.getListOrderType().size() > 0) {
			sql.append(" and so.order_type in (");
			int i = 0;
			for (OrderType orderType : filter.getListOrderType()) {
				if (i == 0) {
					sql.append("?");
				} else {
					sql.append(",?");
				}
				params.add(orderType.getValue());
				i++;
			}
			sql.append(") ");
		}
		if (filter.getOrderSource() != null) {
			sql.append(" and so.ORDER_SOURCE = ?");
			params.add(filter.getOrderSource().getValue());
		}
		//		sql.append(" order by so.order_number, so.order_date");
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(SaleOrderDetail.class, sql.toString(), params, filter.getkPagingDetail());
		}
	}

	/**
	 * @author sangtn
	 * @since 10-06-2014
	 * @description clone from getListSaleOrder2, change result from Entity to
	 *              VO
	 */
	@Override
	public List<SaleOrderVOEx2> getListSaleOrder2Ex(KPaging<SaleOrderVOEx2> kPaging, SoFilter filter) throws DataAccessException {

		if (filter.getFromDate() != null && filter.getToDate() != null && DateUtility.compareTwoDate(filter.getFromDate(), filter.getToDate()) > 0)
			return new ArrayList<SaleOrderVOEx2>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select so.sale_order_id id ");
		sql.append(" , so.ref_order_number refOrderNumber ");
		//sql.append(" , so.approved  ");
		sql.append(" , case when so.order_type in ('SO', 'CO') then nvl(so.approved_van,0) else so.approved end as  approved ");
		sql.append(" , so.order_number orderNumber ");
		sql.append(" , so.priority ");
		sql.append(" , so.staff_id staffId ");
		sql.append(" , (select staff_code from staff s where s.staff_id = so.staff_id) staffCode ");
		sql.append(" , (select staff_name from staff s where s.staff_id = so.staff_id) staffName ");

		sql.append(" , so.customer_id customerId ");
		sql.append(" , (select cu.short_code from customer cu where cu.customer_id = so.customer_id) shortCode ");
		sql.append(" , (select cu.customer_name from customer cu where cu.customer_id = so.customer_id) customerName ");
		sql.append(" , (select cu.address from customer cu where cu.customer_id = so.customer_id) customerAddress ");

		sql.append(" , so.total  ");
		sql.append(" , so.order_date orderDate ");
		sql.append(" , so.delivery_date deliveryDate ");

		sql.append(" , so.type ");
		sql.append(" , so.order_type orderType");
		sql.append(" , (select soTmp.order_number from sale_order soTmp where soTmp.sale_order_id = so.from_sale_order_id) fromSaleOrderCode ");

		sql.append(" , so.delivery_id deliveryId ");
		sql.append(" , (select staff_code from staff s where s.staff_id = so.delivery_id) deliveryCode ");
		sql.append(" , (select staff_name from staff s where s.staff_id = so.delivery_id) deliveryName ");
		//sql.append(" select so.*  ");		
		sql.append(" from sale_order so ");
		if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
			sql.append(" JOIN staff s2 ON s2.staff_id = so.delivery_id ");
		}
		sql.append(" , staff s1");
		if (!StringUtility.isNullOrEmpty(filter.getPriorityCode())) {
			sql.append(", ap_param pr");
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomeName()) || !StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(", customer c");
		}
		sql.append(" where so.staff_id = s1.staff_id ");
		if (!StringUtility.isNullOrEmpty(filter.getPriorityCode())) {
			sql.append(" and so.priority = pr.ap_param_id AND pr.ap_param_code = ?");
			params.add(filter.getPriorityCode());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomeName()) || !StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and so.customer_id = c.customer_id");
		}
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and lower(c.short_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShortCode().toLowerCase()));
		}

		if (filter.getCustomerId() != null) {
			sql.append(" and so.customer_id = ?");
			params.add(filter.getCustomerId());
		}

		if (filter.getShopId() != null) {
			sql.append(" and so.shop_id = ?");
			params.add(filter.getShopId());
		}

		if (Boolean.TRUE.equals(filter.getIsFromSaleOrderNull())) {
			sql.append(" and so.from_sale_order_id is null");
		} else if (Boolean.FALSE.equals(filter.getIsFromSaleOrderNull())) {
			sql.append(" and so.from_sale_order_id is not null");
		}

		if (!StringUtility.isNullOrEmpty(filter.getCustomeName())) {
			sql.append(" and lower(c.customer_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getCustomeName().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and lower(so.order_number) like ? escape'/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber().toLowerCase()));
		}
		if (filter.getApproval() != null) {
			if (filter.getApproveVan() != null) {
				sql.append(" and so.approved = 1");
				if (filter.getApproveVan() != 1) {
					sql.append(" and (so.approved_van is null or so.approved_van = ?)");
					params.add(filter.getApproveVan());
				} else {
					sql.append(" and so.approved_van = ?");
					params.add(filter.getApproveVan());
				}
			} else {
				if (filter.getListOrderType() == null || filter.getListOrderType().isEmpty()) {
					if (filter.getApproval().getValue().intValue() == 1) {
						sql.append(" and so.approved = 1 and((so.order_type not in('SO','CO')) or  (so.order_type in('SO','CO') and so.approved_van = 1) )");
					} else if (filter.getApproval().getValue().intValue() == 0) {
						sql.append(" and((so.order_type not in('SO','CO') and so.approved = 0) or  (so.order_type in('SO','CO') and so.approved = 1 and (so.approved_van = 0 or so.approved_van is null)) )");
					}
				} else {
					sql.append(" and so.approved = ?");
					params.add(filter.getApproval().getValue());
				}
			}
		}
		if (filter.getListOrderType() != null) {
			sql.append(" and (");
			int i = 0;
			for (OrderType orderType : filter.getListOrderType()) {
				if (i == 0) {
					sql.append(" so.order_type = ?");
				} else {
					sql.append(" or so.order_type = ?");
				}
				params.add(orderType.getValue());
				i++;
			}
			sql.append(")");
		}
		if (filter.getCheckDefault() != null && filter.getCheckDefault() == true) {
			sql.append(" and so.order_type not in ('SO','CO')");
		}
		if (Boolean.TRUE.equals(filter.getCheckDueDate())) {
			sql.append(" and (not exists (select value from ap_param where status = 1 and ap_param_code = 'NTKH' and type = 'TH' and value > 0) or trunc(sysdate) - trunc(order_date) <= (select value from ap_param where status = 1 and ap_param_code = 'NTKH' and type = 'TH'))");
		}
		/** @tientv11 order_date=> create_date */
		if (filter.getFromDate() != null) {
			sql.append(" and so.order_date >= trunc(?)");
			params.add(filter.getFromDate());
		}

		if (filter.getToDate() != null) {
			sql.append(" and so.order_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		if (filter.getDeliveryDate() != null) {
			sql.append(" and so.delivery_date >= trunc(?) and so.delivery_date < trunc(?) + 1");
			params.add(filter.getDeliveryDate());
			params.add(filter.getDeliveryDate());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and lower(s1.staff_code) like ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
			sql.append(" and so.delivery_id = s2.staff_id");
			sql.append(" and lower(s2.staff_code) like ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDeliveryCode().toLowerCase()));
		}
		//if (filter.getSaleOrderType() != null) {
		sql.append(" and so.type != ?");
		params.add(SaleOrderType.RETURNED.getValue());
		//params.add(filter.getSaleOrderType().getValue());
		//}
		if (filter.getOrderSource() != null) {
			sql.append(" and so.ORDER_SOURCE = ?");
			params.add(filter.getOrderSource().getValue());
		}

		sql.append(" order by so.order_number, so.order_date");

		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql.toString());
		countSql.append(")");

		System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));

		final String[] fieldNames = new String[] { "id", "refOrderNumber", "approved", "orderNumber", "priority", "staffId", "staffCode", "staffName", "customerId", "shortCode", "customerName", "customerAddress", "total", "orderDate",
				"deliveryDate", "type", "orderType", "fromSaleOrderCode", "deliveryId", "deliveryCode", "deliveryName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.TIMESTAMP,
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(SaleOrderVOEx2.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(SaleOrderVOEx2.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
	}

	@Override
	public Boolean checkSaleOrderBeforeDueDate(Long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select count(1) count from sale_order where sale_order_id = ?");
		params.add(saleOrderId);
		sql.append(" and not exists (select 1 from ap_param where status = 1 and ap_param_code = 'NTKH' and type = 'TH' and to_number(value) > 0)");
		sql.append(" or trunc(sysdate) - trunc(order_date) <= (select to_number(value) from ap_param where status = 1 and ap_param_code = 'NTKH' and type = 'TH')");

		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	//trungtm6 comment on 24/07/2015 replace by function from VNM below
	/*@Override
	public List<SaleOrder> getListSaleOrderForDelivery(KPaging<SaleOrder> paging, SoFilter filter) throws DataAccessException {

		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate()))
			return new ArrayList<SaleOrder>();

		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * FROM sale_order so ");
		sql.append(" WHERE 1 = 1");
		if (filter.getValueOrder() != null && filter.getValueOrder() == 0) {
			sql.append(" and (so.amount is null or so.amount <= 0 )");
		} else if (filter.getValueOrder() != null && filter.getValueOrder() == 1) {
			sql.append(" and so.amount > 0 ");
		}

		//sql.append(" AND so.delivery_id is not null and so.shop_id = ?  ");
		sql.append(" AND  so.shop_id = ?  ");
		params.add(filter.getShopId());

		if (filter.getApproval() != null) {
			sql.append(" AND so.approved  = ?");
			params.add(filter.getApproval().getValue());
		}
		if (filter.getApprovedStep() != null) {
			sql.append(" AND so.approved_step  = ?");
			params.add(filter.getApprovedStep().getValue());
		}

		if (filter.getListOrderType() != null && filter.getListOrderType().size() > 0) {
			sql.append(" AND (so.order_type = ?");
			params.add(filter.getListOrderType().get(0).getValue());
			for (int i = 1; i < filter.getListOrderType().size(); i++) {
				sql.append(" OR so.order_type = ?");
				params.add(filter.getListOrderType().get(i).getValue());
			}
			sql.append(")");
		}

		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" AND UPPER(so.order_number) LIKE ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber()).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" AND EXISTS");
			sql.append("   (SELECT 1");
			sql.append("   FROM customer ct");
			sql.append("   WHERE ct.customer_id = so.customer_id");
			sql.append("   AND UPPER(ct.short_code) LIKE ? ESCAPE '/'");
			sql.append("   )");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShortCode()).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" AND EXISTS");
			sql.append("   (SELECT 1");
			sql.append("   FROM staff s1");
			sql.append("   WHERE s1.staff_id = so.staff_id");
			sql.append("   AND UPPER(s1.staff_code) LIKE ? ESCAPE '/'");
			sql.append("   )");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode()).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
			sql.append(" AND EXISTS");
			sql.append("   (SELECT 1");
			sql.append("   FROM staff s2");
			sql.append("   WHERE s2.staff_id = so.delivery_id");
			sql.append("   AND UPPER(s2.staff_code) LIKE ? ESCAPE '/'");
			sql.append("   )");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDeliveryCode()).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getCashierCode())) {
			sql.append(" AND EXISTS ");
			sql.append(" (SELECT 1 ");
			sql.append(" FROM staff s3 ");
			sql.append(" WHERE s3.staff_id = so.cashier_id ");
			sql.append(" AND UPPER(s3.staff_code) LIKE ? ESCAPE '/' ");
			sql.append(" ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCashierCode()).toUpperCase());
		}

		if (filter.getCarId() != null) {
			sql.append(" AND EXISTS");
			sql.append("   (SELECT 1 FROM car c WHERE c.car_id = so.car_id AND c.car_id = ?");
			sql.append("   )");
			params.add(filter.getCarId());
		}

		//		if(null != filter && null != filter.getApproval() && filter.getApproval().equals(SaleOrderStatus.NOT_YET_APPROVE)){
		//			sql.append(" AND so.approved  = ?");
		//			params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
		//		}else{
		//			sql.append(" AND so.approved  = 1");
		//		}

		if (filter.getFromDate() != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" AND so.order_date < TRUNC(?) + 1");
			params.add(filter.getToDate());
		}

		if (filter.getPrintTime() != null) {
			sql.append(" AND so.TIME_PRINT >= TRUNC(?)");
			sql.append(" AND so.TIME_PRINT < TRUNC(?) + 1");
			params.add(filter.getPrintTime());
			params.add(filter.getPrintTime());
		}

		if (filter.getPrintBatch() != null) {
			if (filter.getPrintBatch() != 0) {
				sql.append(" AND so.PRINT_BATCH = ?");
				params.add(filter.getPrintBatch());
			} else {
				sql.append(" AND so.PRINT_BATCH is null");
			}
		}

		if (null != filter.getSaleOrderType()) {
			sql.append(" AND so.type = ?");
			params.add(filter.getSaleOrderType().getValue());
		}
		if (null != filter.getIsPrint() && Boolean.TRUE == filter.getIsPrint()) {
			sql.append(" AND so.time_print is not null");
		} else if (null != filter.getIsPrint() && Boolean.FALSE == filter.getIsPrint()) {
			sql.append(" AND so.time_print is null");
		}
		
		 * // khong su dung if(null != filter.getOrderSource()){
		 * sql.append(" AND so.order_source = ?");
		 * params.add(filter.getOrderSource().getValue()); }
		 *
		 * 
		 if(!StringUtility.isNullOrEmpty(filter.getPriorityCode())){
			 sql.append(" and so.priority in (select ap_param_id from ap_param where ap_param_code = ? and type = 'ORDER_PIRITY')");
			 params.add(filter.getPriorityCode()); 
		 }
		 

		//sql.append(" ORDER BY so.sale_order_id");
		sql.append(" ORDER BY so.order_number ");
		if (paging == null) {
			List<SaleOrder> list = repo.getListBySQL(SaleOrder.class, sql.toString(), params);
			return list;
		}
		List<SaleOrder> list = repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, paging);
		return list;
	}*/
	
	@Override
	public List<SaleOrder> getListSaleOrderForDelivery(KPaging<SaleOrder> paging, SoFilter filter) throws DataAccessException {

		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate()))
			return new ArrayList<SaleOrder>();

		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * FROM sale_order so ");
		
		if (StringUtility.isNullOrEmpty(filter.getStrListUserId()) && filter.getShopId() != null && filter.getRoleId() != null && filter.getUserId() != null) {
			sql.append(" JOIN table (F_GET_LIST_CHILD_STAFF_INHERIT(?, sysdate, ?, ?, ?)) on so.staff_id = number_key_id and so.shop_id = number_shop_id ");
			params.add(filter.getUserId());
			params.add(filter.getRoleId());
			params.add(filter.getShopId());
			params.add(StaffSpecificType.STAFF.getValue());
		}
		
		//Datpv4 16/06/2015 tim theo trang thai hoa don VAT, chua co hoa don VAT
//		if(filter.getPrintVATStatus()!=null && filter.getPrintVATStatus()!=-1 && filter.getPrintVATStatus()==0){
//			sql.append("left join invoice invo on invo.sale_order_id = so.sale_order_id ");
//		}
		
		sql.append(" WHERE 1 = 1");
		if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "so.staff_id");
			sql.append(paramsFix);
		}
		/*if (filter.getValueOrder() != null && filter.getValueOrder() == 0) {
			sql.append(" and (so.amount is null or so.amount <= 0 )");
		} else if (filter.getValueOrder() != null && filter.getValueOrder() == 1) {
			sql.append(" and so.amount > 0 ");
		}*/
		/** //vuongmq; 04/06/2015; tim kiem theo Gia tri DH*/
		if (filter.getValueOrder() != null && filter.getNumberValueOrder() != null){
			if( filter.getValueOrder() == 0 ){
				sql.append(" and so.amount < ? ");
				params.add(filter.getNumberValueOrder());
			}
			if( filter.getValueOrder() == 1 ){
				sql.append(" and so.amount >= ? ");
				params.add(filter.getNumberValueOrder());
			}
		}

		//sql.append(" AND so.delivery_id is not null and so.shop_id = ?  ");
		sql.append(" AND  so.shop_id = ?  ");
		params.add(filter.getShopId());
		if (filter.getApproval() != null) {
			sql.append(" AND so.approved  = ?");
			params.add(filter.getApproval().getValue());
		}
		if (filter.getApprovedStep() != null) {
			sql.append(" AND so.approved_step  = ?");
			params.add(filter.getApprovedStep().getValue());
		}
		//trungtm6 add
		if (Boolean.TRUE.equals(filter.getApprovedAndStep1())) {
			sql.append(" AND ( so.approved  = ? ");
			params.add(SaleOrderStatus.APPROVED.getValue());
			
			
			sql.append(" OR ( so.approved  = ? and so.approved_step  = ? ) ");
			params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
			params.add(SaleOrderStep.CONFIRMED.getValue());
			
			sql.append(" ) ");
		}

		if (filter.getListOrderType() != null && filter.getListOrderType().size() > 0) {
			int i = 0;
			int size = filter.getListOrderType().size();
			while (i < size && filter.getListOrderType().get(i) == null) {
				i++;
			}
			if (i < size) {
				sql.append(" AND (so.order_type = ?");
				params.add(filter.getListOrderType().get(i).getValue());
				for (i++; i < filter.getListOrderType().size(); i++) {
					if (filter.getListOrderType().get(i) != null) {
						sql.append(" OR so.order_type = ?");
						params.add(filter.getListOrderType().get(i).getValue());					
					}
				}
				sql.append(")");
			}
		}

		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" AND UPPER(so.order_number) LIKE ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber()).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" AND EXISTS");
			sql.append("   (SELECT 1");
			sql.append("   FROM customer ct");
			sql.append("   WHERE ct.customer_id = so.customer_id");
			sql.append("   AND UPPER(ct.short_code) LIKE ? ESCAPE '/'");
			sql.append("   )");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShortCode()).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" AND EXISTS");
			sql.append("   (SELECT 1");
			sql.append("   FROM staff s1");
			sql.append("   WHERE s1.staff_id = so.staff_id");
			if (filter.getExactStaffSearching()) {
				sql.append(" and s1.staff_code = ?");
				params.add(filter.getStaffCode().trim());
			} else {
				sql.append("   AND UPPER(s1.staff_code) LIKE ? ESCAPE '/'");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode()).toUpperCase());
			}
			sql.append("   )");
		}
		if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
			sql.append(" AND EXISTS");
			sql.append("   (SELECT 1");
			sql.append("   FROM staff s2");
			sql.append("   WHERE s2.staff_id = so.delivery_id");
			if (filter.getExactStaffSearching()) {
				sql.append(" and s2.staff_code = ?");
				params.add(filter.getDeliveryCode().trim());
			} else {
				sql.append("   AND UPPER(s2.staff_code) LIKE ? ESCAPE '/'");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDeliveryCode()).toUpperCase());
			}
			sql.append("   )");
		}
		if (!StringUtility.isNullOrEmpty(filter.getCashierCode())) {
			sql.append(" AND EXISTS ");
			sql.append(" (SELECT 1 ");
			sql.append(" FROM staff s3 ");
			sql.append(" WHERE s3.staff_id = so.cashier_id ");
			if (filter.getExactStaffSearching()) {
				sql.append(" and s3.staff_code = ?");
				params.add(filter.getCashierCode().trim());
			} else {
				sql.append(" AND UPPER(s3.staff_code) LIKE ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCashierCode()).toUpperCase());
			}
			sql.append(" ) ");
		}

		if (filter.getCarId() != null) {
			sql.append(" AND EXISTS");
			sql.append("   (SELECT 1 FROM car c WHERE c.car_id = so.car_id AND c.car_id = ?");
			sql.append("   )");
			params.add(filter.getCarId());
		}

		//		if(null != filter && null != filter.getApproval() && filter.getApproval().equals(SaleOrderStatus.NOT_YET_APPROVE)){
		//			sql.append(" AND so.approved  = ?");
		//			params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
		//		}else{
		//			sql.append(" AND so.approved  = 1");
		//		}

		if (filter.getFromDate() != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" AND so.order_date < TRUNC(?) + 1");
			params.add(filter.getToDate());
		}
		if (filter.getOrderDate() != null) {
			sql.append(" AND so.order_date >= trunc(?) and so.order_date < TRUNC(?) + 1");
			params.add(filter.getOrderDate());
			params.add(filter.getOrderDate());
		}

		if (filter.getPrintTime() != null) {
			sql.append(" AND so.TIME_PRINT >= TRUNC(?)");
			sql.append(" AND so.TIME_PRINT < TRUNC(?) + 1");
			params.add(filter.getPrintTime());
			params.add(filter.getPrintTime());
		}

		if (filter.getPrintBatch() != null) {
			if (filter.getPrintBatch() != 0) {
				sql.append(" AND so.PRINT_BATCH = ?");
				params.add(filter.getPrintBatch());
			} else {
				sql.append(" AND so.PRINT_BATCH is null");
			}
		}

		if (null != filter.getSaleOrderType()) {
			sql.append(" AND so.type = ?");
			params.add(filter.getSaleOrderType().getValue());
		}
		if (null != filter.getIsPrint() && Boolean.TRUE == filter.getIsPrint()) {
			sql.append(" AND so.time_print is not null");
		} else if (null != filter.getIsPrint() && Boolean.FALSE == filter.getIsPrint()) {
			sql.append(" AND so.time_print is null");
		}
		/*
		 * // khong su dung if(null != filter.getOrderSource()){
		 * sql.append(" AND so.order_source = ?");
		 * params.add(filter.getOrderSource().getValue()); }
		 *
		 **/ 
		 if(!StringUtility.isNullOrEmpty(filter.getPriorityCode())){
			 sql.append(" and so.priority in (select ap_param_id from ap_param where ap_param_code = ? and type = 'ORDER_PIRITY')");
			 params.add(filter.getPriorityCode()); 
		 }
		 
		//Datpv4 16/06/2015 tim theo trang thai hoa don VAT
		
		if (filter.getPrintVATStatus() != null && filter.getPrintVATStatus() == 0) { // chua co VAT
			sql.append(" and not exists (select 1 from sale_order_detail sod");
			sql.append(" join invoice iv on (iv.invoice_id = sod.invoice_id");
			sql.append(" and iv.status <> ?");
			params.add(InvoiceStatus.CANCELED.getValue());
			sql.append(") where sod.sale_order_id = so.sale_order_id)");
			//sql.append(" and invo.invoice_id is null ");
		} else if (filter.getPrintVATStatus() != null && filter.getPrintVATStatus() == 1) {// Co VAT va da in
			sql.append(" and exists (select 1 from sale_order_detail sod");
			sql.append(" join invoice iv on (iv.invoice_id = sod.invoice_id");
			sql.append(" and iv.invoice_date is not null and iv.status <> ?");
			params.add(InvoiceStatus.CANCELED.getValue());
			sql.append(") where sod.sale_order_id = so.sale_order_id)");
//			sql.append("  and so.SALE_ORDER_ID in (select invo.SALE_ORDER_ID from invoice invo where invo.invoice_date is not null) ");
		} else if (filter.getPrintVATStatus() != null && filter.getPrintVATStatus() == 2) {// Co VAT chua in
			sql.append(" and exists (select 1 from sale_order_detail sod");
			sql.append(" join invoice iv on (iv.invoice_id = sod.invoice_id");
			sql.append(" and iv.invoice_date is null and iv.status <> ?");
			params.add(InvoiceStatus.CANCELED.getValue());
			sql.append(") where sod.sale_order_id = so.sale_order_id)");
			//sql.append(" and so.SALE_ORDER_ID in (select invo.SALE_ORDER_ID from invoice invo where invo.invoice_date is null) ");
		}
		 
		//sql.append(" ORDER BY so.sale_order_id");
		sql.append(" ORDER BY so.order_number ");
		if (paging == null) {
			List<SaleOrder> list = repo.getListBySQL(SaleOrder.class, sql.toString(), params);
			return list;
		}
		List<SaleOrder> list = repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, paging);
		return list;
	}

	@Override
	public List<SaleOrder> getListSaleOrderFormTableByCondition(String orderNumber, SaleOrderStatus approved, String customerCode, String customerName, Long staffId, Integer priority, Date fromDate, Date toDate) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<SaleOrder>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *");
		sql.append(" FROM sale_order so");
		sql.append(" WHERE 1 = 1");

		if (null != orderNumber) {
			sql.append(" AND ORDER_NUMBER = ?");
			params.add(orderNumber);
		}

		if (null != approved) {
			sql.append(" AND APPROVED = ?");
			params.add(approved.getValue());
		} else {
			sql.append(" AND (APPROVED = ? OR APPROVED = ?)");

			params.add(SaleOrderStatus.TABLET_CREATED_AND_NOT_REQUIRED_CONFIRM);
			params.add(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM);
		}

		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append(" AND EXISTS");
			sql.append("   (SELECT 1");
			sql.append("   FROM customer ct");
			sql.append("   WHERE ct.customer_id = customer_id");
			sql.append("   AND ct.customer_code = ?");
			sql.append("   )");
			params.add(customerCode.toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append(" AND EXISTS");
			sql.append("   (SELECT 1");
			sql.append("   FROM customer ct");
			sql.append("   WHERE ct.customer_id = customer_id");
			sql.append("   AND lower(ct.customer_name) like ? ESCAPE '/' ");
			sql.append("   )");
			params.add(StringUtility.toOracleSearchLike(customerName.toLowerCase()));
		}

		if (null != staffId) {
			sql.append(" AND STAFF_ID = ?");
			params.add(staffId);
		}

		if (null != priority) {
			sql.append(" AND PRIORITY = ?");
			params.add(priority);
		}

		if (null != fromDate) {
			sql.append(" and ORDER_DATE >= trunc(?)");
			params.add(fromDate);
		}

		if (null != toDate) {
			sql.append(" and ORDER_DATE < trunc(?) + 1");
			params.add(toDate);
		}

		sql.append(" ORDER BY ORDER_NUMBER");
		return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
	}

	//sontt: BAO CAO TONG HOP CHI TRA HANG TRUNG BAY----------------------------
	//vuonghn update
	@Override
	public List<RptPayDisplayProgrameRecordProductVO> getListRptPayDisplayProgrameRecordProductVO(Long shopId, Integer year, Long displayProgrameId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String endDate = "31/12/" + year;
		String beginDate = "01/01/" + year;
		sql.append(" select s.shop_code as shopCode, s.shop_name as shopName, s.address shopAddress,");
		sql.append(" dp.display_program_code as displayProgrameCode, dp.display_program_name as displayProgrameName, to_char(dp.from_date ,'dd/mm/yyyy') as fdate, to_char(dp.to_date, 'dd/mm/yyyy') as tdate,");
		sql.append(" p.product_code as productCode, p.product_name as productName,");
		sql.append(" sum(sod.quantity) as promotionQuantity, sod.price as price");
		sql.append(" from sale_order_detail sod");
		sql.append(" inner join sale_order so");
		sql.append(" on so.sale_order_id = sod.sale_order_id");
		sql.append(" inner join product p");
		sql.append(" on p.product_id = sod.product_id");
		sql.append(" inner join display_program dp");
		sql.append(" on dp.display_program_code = sod.program_code");
		sql.append(" inner join shop s");
		sql.append(" on s.shop_id = sod.shop_id");
		sql.append(" where");
		sql.append(" trunc(dp.from_date) <= trunc(to_date(?,'dd/mm/yyyy'))");
		params.add(endDate);
		sql.append(" and (trunc(dp.to_date) >= trunc(to_date(?,'dd/mm/yyyy')) or dp.to_date is null)");
		params.add(beginDate);
		sql.append(" and sod.shop_id in(select shop_id from shop where status = 1 start with shop_id = ? connect by prior PARENT_SHOP_ID = shop_id)");
		params.add(shopId);
		if (displayProgrameId != null) {
			sql.append(" and dp.display_program_id = ?");
			params.add(displayProgrameId);
		}
		sql.append(" and so.order_type in('IN','SO')");
		sql.append(" and sod.is_free_item = 1");
		sql.append(" group by s.shop_id, s.shop_code, s.shop_name, s.address,");
		sql.append(" dp.display_program_code, dp.display_program_name, dp.from_date, dp.to_date,");
		sql.append(" p.product_code, p.product_name,");
		sql.append(" sod.price");
		sql.append(" order by s.shop_id, s.shop_code, s.shop_name, s.address,");
		sql.append(" dp.display_program_code, dp.display_program_name, dp.from_date, dp.to_date,");
		sql.append(" p.product_code, p.product_name");
		String[] fieldNames = new String[] {//
		"shopCode", "shopName", "shopAddress", "displayProgrameCode",//1
				"displayProgrameName",//2
				"fDate", "tDate", "productCode",//3
				"productName",//4
				"promotionQuantity",//5
				"price",//6
		};
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.BIG_DECIMAL,// 6
		};
		System.out.println(sql.toString());
		return repo.getListByQueryAndScalar(RptPayDisplayProgrameRecordProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	//sontt: BAO CAO DANH SACH KHACH HANG THEO MAT HANG-------------------------------------------------------------------------------
	@Override
	public List<RptDSKHTMHRecordProductVO> getListRptDSKHTMHRecordProductVO(Long shopId, Long customerId, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		//		sql.append(" with infoSaleAndDisplay as ");
		//		sql.append(" ( ");
		//		sql.append(" select ");
		//		sql.append(" so.shop_id , cu.customer_id , cu.short_code, cu.customer_name, pi.product_info_code AS cat_code, pi.product_info_name as cat_name, ");
		//		sql.append(" p.product_code , p.product_name, sod.quantity, sod.price, sod.amount, sod.is_free_item, sod.program_type ");
		//		sql.append(" FROM sale_order so ");
		//		sql.append(" JOIN sale_order_detail sod ");
		//		sql.append(" ON so.sale_order_id = sod.sale_order_id ");
		//		sql.append(" JOIN customer cu ");
		//		sql.append(" ON cu.customer_id = so.customer_id ");
		//		sql.append(" JOIN product p ");
		//		sql.append(" ON p.product_id = sod.product_id ");
		//		sql.append(" JOIN product_info pi ");
		//		sql.append(" ON pi.product_info_id = p.cat_id ");
		//		sql.append(" WHERE so.approved     = 1 ");
		//		sql.append(" AND so.type           = 1 ");
		//		if(fromDate != null){
		//			sql.append(" AND so.order_date >= trunc(?) ");
		//			params.add(fromDate);
		//		}
		//		if (toDate != null) {
		//			sql.append(" AND so.order_date < trunc(?) + 1  ");
		//			params.add(toDate);
		//		}
		//		if (customerId != null) {
		//			sql.append(" AND so.customer_id = ? ");
		//			params.add(customerId);
		//		}
		//		sql.append(" AND so.shop_id     = ? ");
		//		params.add(shopId);
		//		sql.append(" AND so.order_type     = 'IN' ");
		//		sql.append(" ) ");
		//		sql.append(" , infoSale as ");
		//		sql.append(" ( ");
		//		sql.append(" SELECT ");
		//		sql.append(" infoSaleAndDisplay.shop_id , ");
		//		sql.append(" infoSaleAndDisplay.customer_id, ");
		//		sql.append(" infoSaleAndDisplay.short_code, ");
		//		sql.append(" infoSaleAndDisplay.customer_name, ");
		//		sql.append(" infoSaleAndDisplay.cat_code, ");
		//		sql.append(" infoSaleAndDisplay.cat_name, ");
		//		sql.append(" infoSaleAndDisplay.product_code , ");
		//		sql.append(" infoSaleAndDisplay.product_name , ");
		//		sql.append(" sum(infoSaleAndDisplay.quantity) as SL_Ban, ");
		//		sql.append(" SUM(infoSaleAndDisplay.amount) AS Tien_Ban ");
		//		sql.append(" from infoSaleAndDisplay ");
		//		sql.append(" where infoSaleAndDisplay.is_free_item  = 0 ");
		//		sql.append(" group by infoSaleAndDisplay.shop_id, infoSaleAndDisplay.customer_id, infoSaleAndDisplay.short_code, infoSaleAndDisplay.customer_name, ");
		//		sql.append(" infoSaleAndDisplay.cat_code, infoSaleAndDisplay.cat_name, infoSaleAndDisplay.product_code, infoSaleAndDisplay.product_name ");
		//		sql.append("  ");
		//		sql.append(" ), ");
		//		sql.append(" infoDisplay as ");
		//		sql.append(" ( ");
		//		sql.append(" SELECT ");
		//		sql.append(" infoSaleAndDisplay.shop_id , ");
		//		sql.append(" infoSaleAndDisplay.customer_id, ");
		//		sql.append(" infoSaleAndDisplay.short_code, ");
		//		sql.append(" infoSaleAndDisplay.customer_name, ");
		//		sql.append(" infoSaleAndDisplay.cat_code, ");
		//		sql.append(" infoSaleAndDisplay.cat_name, ");
		//		sql.append(" infoSaleAndDisplay.product_code , ");
		//		sql.append(" infoSaleAndDisplay.product_name , ");
		//		sql.append(" sum(infoSaleAndDisplay.quantity) AS SL_TB, ");
		//		sql.append(" sum((infoSaleAndDisplay.quantity*infoSaleAndDisplay.price)) Tien_TB ");
		//		sql.append(" from infoSaleAndDisplay ");
		//		sql.append(" where infoSaleAndDisplay.is_free_item  = 1 ");
		//		sql.append(" AND infoSaleAndDisplay.program_type  =2 ");
		//		sql.append(" group by infoSaleAndDisplay.shop_id, infoSaleAndDisplay.customer_id, infoSaleAndDisplay.short_code, infoSaleAndDisplay.customer_name, ");
		//		sql.append(" infoSaleAndDisplay.cat_code, infoSaleAndDisplay.cat_name, infoSaleAndDisplay.product_code, infoSaleAndDisplay.product_name ");
		//		sql.append(" ), ");
		//		sql.append(" infoAnd_SLB_SLTB_TBan_TTB as ");
		//		sql.append(" ( ");
		//		sql.append(" SELECT ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN infoSale.shop_id IS NULL ");
		//		sql.append(" THEN infoDisplay.shop_id ");
		//		sql.append(" WHEN infoSale.shop_id IS NOT NULL ");
		//		sql.append(" THEN infoSale.shop_id ");
		//		sql.append(" END AS shop_id, ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN infoSale.customer_id IS NULL ");
		//		sql.append(" THEN infoDisplay.customer_id ");
		//		sql.append(" WHEN infoSale.customer_id IS NOT NULL ");
		//		sql.append(" THEN infoSale.customer_id ");
		//		sql.append(" END AS customer_id, ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN infoSale.short_code IS NULL ");
		//		sql.append(" THEN infoDisplay.short_code ");
		//		sql.append(" WHEN infoSale.short_code IS NOT NULL ");
		//		sql.append(" THEN infoSale.short_code ");
		//		sql.append(" END AS short_code, ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN infoSale.customer_name IS NULL ");
		//		sql.append(" THEN infoDisplay.customer_name ");
		//		sql.append(" WHEN infoSale.customer_name IS NOT NULL ");
		//		sql.append(" THEN infoSale.customer_name ");
		//		sql.append(" END AS customer_name, ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN infoSale.cat_code IS NULL ");
		//		sql.append(" THEN infoDisplay.cat_code ");
		//		sql.append(" WHEN infoSale.cat_code IS NOT NULL ");
		//		sql.append(" THEN infoSale.cat_code ");
		//		sql.append(" END AS cat_code, ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN infoSale.cat_name IS NULL ");
		//		sql.append(" THEN infoDisplay.cat_name ");
		//		sql.append(" WHEN infoSale.cat_name IS NOT NULL ");
		//		sql.append(" THEN infoSale.cat_name ");
		//		sql.append(" END AS cat_name, ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN infoSale.product_code IS NULL ");
		//		sql.append(" THEN infoDisplay.product_code ");
		//		sql.append(" WHEN infoSale.product_code IS NOT NULL ");
		//		sql.append(" THEN infoSale.product_code ");
		//		sql.append(" END AS product_code, ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN infoSale.product_name IS NULL ");
		//		sql.append(" THEN infoDisplay.product_name ");
		//		sql.append(" WHEN infoSale.product_name IS NOT NULL ");
		//		sql.append(" THEN infoSale.product_name ");
		//		sql.append(" END AS product_name, ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN infoSale.SL_Ban IS NULL ");
		//		sql.append(" THEN 0 ");
		//		sql.append(" WHEN infoSale.SL_Ban IS NOT NULL ");
		//		sql.append(" THEN infoSale.SL_Ban ");
		//		sql.append(" END AS SL_Ban, ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN infoDisplay.SL_TB IS NULL ");
		//		sql.append(" THEN 0 ");
		//		sql.append(" WHEN infoDisplay.SL_TB IS NOT NULL ");
		//		sql.append(" THEN infoDisplay.SL_TB ");
		//		sql.append(" END AS SL_TB, ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN infoSale.Tien_Ban IS NULL ");
		//		sql.append(" THEN 0 ");
		//		sql.append(" WHEN infoSale.Tien_Ban IS NOT NULL ");
		//		sql.append(" THEN infoSale.Tien_Ban ");
		//		sql.append(" END AS Tien_Ban, ");
		//		sql.append(" CASE ");
		//		sql.append(" WHEN infoDisplay.Tien_TB IS NULL ");
		//		sql.append(" THEN 0 ");
		//		sql.append(" WHEN infoDisplay.Tien_TB IS NOT NULL ");
		//		sql.append(" THEN infoDisplay.Tien_TB ");
		//		sql.append(" END AS Tien_TB ");
		//		sql.append(" from infoSale ");
		//		sql.append(" full join infoDisplay ");
		//		sql.append(" ON ( ");
		//		sql.append(" infoSale.customer_id = infoDisplay.customer_id ");
		//		sql.append(" and infoSale.cat_code = infoDisplay.cat_code ");
		//		sql.append(" and  infoSale.product_code = infoDisplay.product_code ");
		//		sql.append(" ) ");
		//		sql.append(" ), ");
		//		sql.append(" infoAnd_SumCat as ");
		//		sql.append(" ( ");
		//		sql.append(" SELECT SHOP_ID, CUSTOMER_ID, short_code, customer_name, CAT_CODE, cat_name, ");
		//		sql.append(" SUM(SL_BAN) AS SUM_SL_BAN_FOR_THIS_CAT, SUM(SL_TB) AS SUM_SL_TB_FOR_THIS_CAT, ");
		//		sql.append(" SUM(Tien_Ban) AS SUM_TIEN_BAN_FOR_THIS_CAT, SUM(Tien_TB) AS SUM_TIEN_TB_FOR_THIS_CAT ");
		//		sql.append(" FROM infoAnd_SLB_SLTB_TBan_TTB ");
		//		sql.append(" group by SHOP_ID, CUSTOMER_ID, short_code, customer_name, CAT_CODE, cat_name ");
		//		sql.append(" ), ");
		//		sql.append(" infoAnd_SumCustomer as ");
		//		sql.append(" ( ");
		//		sql.append(" SELECT SHOP_ID, CUSTOMER_ID, short_code, customer_name, ");
		//		sql.append(" SUM(SL_BAN) AS SUM_SL_BAN_FOR_THIS_CUSTOMER, SUM(SL_TB) AS SUM_SL_TB_FOR_THIS_CUSTOMER, ");
		//		sql.append(" SUM(Tien_Ban) AS SUM_TIEN_BAN_FOR_THIS_CUSTOMER, SUM(Tien_TB) AS SUM_TIEN_TB_FOR_THIS_CUSTOMER ");
		//		sql.append(" FROM infoAnd_SLB_SLTB_TBan_TTB ");
		//		sql.append(" group by SHOP_ID, CUSTOMER_ID, short_code, customer_name ");
		//		sql.append(" ) ");
		//		sql.append("  ");
		//		sql.append(" SELECT  infoAnd_SLB_SLTB_TBan_TTB.SHOP_ID, infoAnd_SLB_SLTB_TBan_TTB.CUSTOMER_ID as customerId, infoAnd_SLB_SLTB_TBan_TTB.SHORT_CODE as customerCode, infoAnd_SLB_SLTB_TBan_TTB.CUSTOMER_NAME as customerName, ");
		//		sql.append(" infoAnd_SLB_SLTB_TBan_TTB.CAT_CODE as catCode,infoAnd_SLB_SLTB_TBan_TTB.CAT_NAME as catName,infoAnd_SLB_SLTB_TBan_TTB.product_code as productCode,infoAnd_SLB_SLTB_TBan_TTB.product_name as productName , ");
		//		sql.append(" infoAnd_SLB_SLTB_TBan_TTB.SL_Ban as quantitySale,infoAnd_SLB_SLTB_TBan_TTB.SL_TB  as quantityDisplay, ");
		//		sql.append(" infoAnd_SLB_SLTB_TBan_TTB.Tien_Ban, infoAnd_SLB_SLTB_TBan_TTB.Tien_TB, (infoAnd_SLB_SLTB_TBan_TTB.Tien_Ban+infoAnd_SLB_SLTB_TBan_TTB.Tien_TB) AS money, ");
		//		sql.append(" infoAnd_SumCat.SUM_SL_BAN_FOR_THIS_CAT as sumQuantitySaleForCategory,infoAnd_SumCat.SUM_SL_TB_FOR_THIS_CAT as sumQuantityDisplayForCategory, ");
		//		sql.append(" infoAnd_SumCat.SUM_TIEN_BAN_FOR_THIS_CAT, infoAnd_SumCat.SUM_TIEN_TB_FOR_THIS_CAT,(infoAnd_SumCat.SUM_TIEN_BAN_FOR_THIS_CAT+infoAnd_SumCat.SUM_TIEN_TB_FOR_THIS_CAT) AS sumMoneyForCategory, ");
		//		sql.append(" infoAnd_SumCustomer.SUM_SL_BAN_FOR_THIS_CUSTOMER as sumQuantitySaleForCustomer, infoAnd_SumCustomer.SUM_SL_TB_FOR_THIS_CUSTOMER as sumQuantityDisplayForCustomer, ");
		//		sql.append(" infoAnd_SumCustomer.SUM_TIEN_BAN_FOR_THIS_CUSTOMER, infoAnd_SumCustomer.SUM_TIEN_TB_FOR_THIS_CUSTOMER,(infoAnd_SumCustomer.SUM_TIEN_BAN_FOR_THIS_CUSTOMER+infoAnd_SumCustomer.SUM_TIEN_TB_FOR_THIS_CUSTOMER) AS sumMoneyForCustomer ");
		//		sql.append(" from ");
		//		sql.append(" infoAnd_SLB_SLTB_TBan_TTB, infoAnd_SumCat, infoAnd_SumCustomer ");
		//		sql.append(" WHERE ");
		//		sql.append(" infoAnd_SLB_SLTB_TBan_TTB.shop_id = infoAnd_SumCat.shop_id ");
		//		sql.append(" and infoAnd_SLB_SLTB_TBan_TTB.customer_id = infoAnd_SumCat.customer_id ");
		//		sql.append(" and infoAnd_SLB_SLTB_TBan_TTB.cat_code = infoAnd_SumCat.cat_code ");
		//		sql.append("  ");
		//		sql.append(" and infoAnd_SLB_SLTB_TBan_TTB.shop_id = infoAnd_SumCustomer.shop_id ");
		//		sql.append(" and infoAnd_SLB_SLTB_TBan_TTB.customer_id = infoAnd_SumCustomer.customer_id ");
		//		sql.append(" order by customerId, catCode, productCode ");

		//		String[] fieldNames = { 
		//				"customerCode","customerName","catCode","catName","productCode","productName",
		//				"quantitySale","quantityDisplay","money",
		//				"sumQuantitySaleForCategory","sumQuantityDisplayForCategory","sumMoneyForCategory",
		//				"sumQuantitySaleForCustomer","sumQuantityDisplayForCustomer","sumMoneyForCustomer"
		//			};
		//
		//		Type[] fieldTypes = { 
		//				StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING, 
		//				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
		//				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
		//				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL
		//			};

		sql.append("  SELECT cu.short_code   AS customerCode,");
		sql.append("  cu.customer_name     AS customerName,");
		sql.append("  pi.product_info_code AS catCode,");
		sql.append("  pi.product_info_name AS catName,");
		sql.append("  p.product_code       AS productCode,");
		sql.append("  p.product_name       AS productName,");
		sql.append("  ");
		sql.append("    (SELECT ");
		sql.append("  CASE");
		sql.append("      WHEN SUM(sod1.quantity) IS NULL");
		sql.append("      THEN 0");
		sql.append("      WHEN SUM(sod1.quantity) IS NOT NULL");
		sql.append("      THEN SUM(sod1.quantity)");
		sql.append("    END");
		sql.append("  FROM sale_order_detail sod1");
		sql.append("  JOIN sale_order so1");
		sql.append("  ON so1.sale_order_id            = sod1.sale_order_id");
		sql.append("  JOIN customer cu1");
		sql.append("  ON so1.customer_id            = cu1.customer_id");
		sql.append("  JOIN product p1");
		sql.append("  ON p1.product_id = sod1.product_id");
		sql.append("  WHERE cu1.customer_id = cu.customer_id");
		sql.append("  AND p1.product_id = p.product_id");
		sql.append("  AND sod1.is_free_item           = 0");
		sql.append("  AND so1.order_type              = 'IN'");
		if (fromDate != null) {
			sql.append(" AND sod1.order_date >= trunc(?) ");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND sod1.order_date < trunc(?) + 1  ");
			params.add(toDate);
		}
		sql.append("  AND so1.approved     = 1");
		sql.append("  AND so1.type           IN (1, 0)");
		sql.append("  ) AS quantitySale,");
		sql.append("  ");
		sql.append("  (SELECT");
		sql.append("    CASE");
		sql.append("      WHEN SUM(sod1.quantity) IS NULL");
		sql.append("      THEN 0");
		sql.append("      WHEN SUM(sod1.quantity) IS NOT NULL");
		sql.append("      THEN SUM(sod1.quantity * sod1.price)");
		sql.append("    END");
		sql.append("  FROM sale_order_detail sod1");
		sql.append("  JOIN sale_order so1");
		sql.append("  ON so1.sale_order_id            = sod1.sale_order_id");
		sql.append("  JOIN customer cu1");
		sql.append("  ON so1.customer_id            = cu1.customer_id");
		sql.append("  JOIN product p1");
		sql.append("  ON p1.product_id = sod1.product_id");
		sql.append("  WHERE cu1.customer_id = cu.customer_id");
		sql.append("  AND p1.product_id = p.product_id");
		sql.append("  AND sod1.is_free_item           = 1");
		sql.append("  AND so1.order_type              = 'TT'");
		if (fromDate != null) {
			sql.append(" AND sod1.order_date >= trunc(?) ");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND sod1.order_date < trunc(?) + 1  ");
			params.add(toDate);
		}
		sql.append("  AND so1.approved     = 1");
		sql.append("  AND so1.type           IN (1, 0) ");
		sql.append("  ) AS quantityDisplay,");
		sql.append("  ");
		sql.append("  (");
		sql.append("  (SELECT ");
		sql.append("  CASE");
		sql.append("      WHEN SUM(sod1.amount) IS NULL");
		sql.append("      THEN 0");
		sql.append("      WHEN SUM(sod1.amount) IS NOT NULL");
		sql.append("      THEN SUM(sod1.amount)");
		sql.append("    END");
		sql.append("  FROM sale_order_detail sod1");
		sql.append("  JOIN sale_order so1");
		sql.append("  ON so1.sale_order_id            = sod1.sale_order_id");
		sql.append("  JOIN customer cu1");
		sql.append("  ON so1.customer_id            = cu1.customer_id");
		sql.append("  JOIN product p1");
		sql.append("  ON p1.product_id = sod1.product_id");
		sql.append("  WHERE cu1.customer_id = cu.customer_id");
		sql.append("  AND p1.product_id = p.product_id");
		sql.append("  AND sod1.is_free_item           = 0");
		sql.append("  AND so1.order_type              = 'IN'");
		if (fromDate != null) {
			sql.append(" AND sod1.order_date >= trunc(?) ");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND sod1.order_date < trunc(?) + 1  ");
			params.add(toDate);
		}
		sql.append("  AND so1.approved     = 1");
		sql.append("  AND so1.type           IN (1, 0) ");
		sql.append("  ) +");
		sql.append("  (SELECT");
		sql.append("    CASE");
		sql.append("      WHEN SUM(sod1.quantity) IS NULL");
		sql.append("      THEN 0");
		sql.append("      WHEN SUM(sod1.quantity) IS NOT NULL");
		sql.append("      THEN SUM(sod1.quantity * sod1.price)");
		sql.append("    END AS SUM");
		sql.append("  FROM sale_order_detail sod1");
		sql.append("  JOIN sale_order so1");
		sql.append("  ON so1.sale_order_id            = sod1.sale_order_id");
		sql.append("  JOIN customer cu1");
		sql.append("  ON so1.customer_id            = cu1.customer_id");
		sql.append("  JOIN product p1");
		sql.append("  ON p1.product_id = sod1.product_id");
		sql.append("  WHERE cu1.customer_id = cu.customer_id");
		sql.append("  AND p1.product_id = p.product_id");
		sql.append("  AND sod1.is_free_item           = 1");
		sql.append("  AND so1.order_type              = 'TT'");
		if (fromDate != null) {
			sql.append(" AND sod1.order_date >= trunc(?) ");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND sod1.order_date < trunc(?) + 1  ");
			params.add(toDate);
		}
		sql.append("  AND so1.approved     = 1");
		sql.append("  AND so1.type           IN (1, 0) ");
		sql.append("  )) AS money ");
		sql.append(" FROM sale_order so");
		sql.append(" JOIN sale_order_detail sod");
		sql.append(" ON so.sale_order_id = sod.sale_order_id");
		sql.append(" JOIN customer cu");
		sql.append(" ON cu.customer_id = so.customer_id");
		sql.append(" JOIN product p");
		sql.append(" ON p.product_id = sod.product_id");
		sql.append(" JOIN product_info pi");
		sql.append(" ON pi.product_info_id = p.cat_id");
		sql.append(" WHERE so.approved     = 1");
		sql.append(" AND so.type           IN (1, 0) ");
		if (fromDate != null) {
			sql.append(" AND so.order_date >= trunc(?) ");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND so.order_date < trunc(?) + 1  ");
			params.add(toDate);
		}
		if (customerId != null) {
			sql.append(" AND so.customer_id = ? ");
			params.add(customerId);
		}
		sql.append(" AND so.shop_id     = ? ");
		params.add(shopId);
		sql.append(" AND so.order_type    IN ('IN', 'TT')");
		sql.append(" GROUP BY cu.customer_id,");
		sql.append("  cu.customer_name,");
		sql.append("  cu.short_code,");
		sql.append("  p.cat_id,");
		sql.append("  p.product_id,");
		sql.append("  p.product_code,");
		sql.append("  p.product_name,");
		sql.append("  pi.product_info_code,");
		sql.append("  pi.product_info_name");
		sql.append(" ORDER BY cu.customer_name,");
		sql.append("  p.product_code,");
		sql.append("  p.cat_id");

		String[] fieldNames = { "customerCode", "customerName", "catCode", "catName", "productCode", "productName", "quantitySale", "quantityDisplay", "money" };

		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL };

		System.out.println(sql.toString());
		return repo.getListByQueryAndScalar(RptDSKHTMHRecordProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	//endsontt------------------------------------------------------------------------------------------------------------------------------

	//SangTN: Bao cao theo doi ban hang theo mat hang
	public List<RptTDBHTMHProductDateStaffFollowCustomerVO> getListRptTDBHTMHProductDateStaffFollowCustomerVO(Long shopId, List<Long> listStaffId, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH normalInfo AS");
		sql.append("  (SELECT so.shop_id AS shopId,");
		sql.append("    p.product_id     AS productId,");
		sql.append("    p.product_code   AS productCode,");
		sql.append("    p.product_name   AS productName,");
		sql.append("    sod.price        AS price,");
		sql.append("    to_char(so.create_date)   AS createDate,");
		sql.append("    s.staff_id       AS staffId,");
		sql.append("    s.staff_code     AS staffCode,");
		sql.append("    s.staff_name     AS staffName,");
		sql.append("    cu.short_code    AS customerCode,");
		sql.append("    cu.customer_name AS customerName,");
		sql.append("    cu.address       AS customerAddress,");
		sql.append("    cu.customer_id   AS customerId,");
		sql.append("    sod.quantity     AS quantity,");
		sql.append("    so.amount        AS amount,");
		sql.append("    so.order_number  AS orderNumber");
		sql.append("  FROM sale_order so");
		sql.append("  JOIN sale_order_detail sod");
		sql.append("  ON so.sale_order_id = sod.sale_order_id");
		sql.append("  JOIN customer cu");
		sql.append("  ON cu.customer_id = so.customer_id");
		sql.append("  JOIN product p");
		sql.append("  ON p.product_id = sod.product_id");
		sql.append("  JOIN staff s");
		sql.append("  ON s.staff_id     = so.staff_id");
		sql.append("  WHERE so.shop_id  = 16");
		sql.append("  AND so.approved   = 1");
		sql.append("  AND so.type       = 1");
		sql.append("  AND so.order_type = 'IN'");
		if (toDate != null) {
			sql.append(" and so.create_date < trunc(?) + 1 ");
			params.add(toDate);
		}

		if (fromDate != null) {
			sql.append(" and so.create_date >= trunc(?) ");
			params.add(fromDate);
		}

		if (listStaffId != null && listStaffId.size() > 0) {
			sql.append(" and (");
			for (int i = 0; i < listStaffId.size(); i++) {
				if (i == 0) {
					sql.append(" so.staff_id = ? ");
				} else {
					sql.append(" or so.staff_id = ? ");
				}
				params.add(listStaffId.get(i));
			}
			sql.append(") ");
		}
		sql.append("  ORDER BY p.product_id,");
		sql.append("    sod.price,");
		sql.append("     to_char(so.create_date),");
		sql.append("    s.staff_id");
		sql.append("  ),");
		sql.append("  sumStaffInfo AS");
		sql.append("  (SELECT normalInfo.shopId  AS shopId,");
		sql.append("    normalInfo.productId     AS productId,");
		sql.append("    normalInfo.price         AS price,");
		sql.append("    to_char(normalInfo.createDate)    AS createDate,");
		sql.append("    normalInfo.staffId       AS staffId,");
		sql.append("    SUM(normalInfo.quantity) AS sumStaffQuantity,");
		sql.append("    SUM(normalInfo.amount)   AS sumStaffAmount");
		sql.append("  FROM normalInfo");
		sql.append("  GROUP BY normalInfo.shopId,");
		sql.append("    normalInfo.productId,");
		sql.append("    normalInfo.price,");
		sql.append("    to_char(normalInfo.createDate),");
		sql.append("    normalInfo.staffId");
		sql.append("  ),");
		sql.append("  sumDayInfo AS");
		sql.append("  (SELECT normalInfo.shopId         AS shopId,");
		sql.append("    normalInfo.productId            AS productId,");
		sql.append("    normalInfo.price                AS price, ");
		sql.append("    TO_CHAR( normalInfo.createDate) AS createDate,    ");
		sql.append("    SUM(normalInfo.quantity)        AS sumDayQuantity,");
		sql.append("    SUM(normalInfo.amount)          AS sumDayAmount");
		sql.append("  FROM normalInfo");
		sql.append("  GROUP BY normalInfo.shopId,");
		sql.append("    normalInfo.productId,");
		sql.append("    normalInfo.price,");
		sql.append("    TO_CHAR( normalInfo.createDate)");
		sql.append("  ),");
		sql.append("  sumProuctAndPriceInfo AS");
		sql.append("  (SELECT normalInfo.shopId  AS shopId,");
		sql.append("    normalInfo.productId     AS productId,");
		sql.append("    normalInfo.price         AS price,");
		sql.append("    SUM(normalInfo.quantity) AS sumProductAndPriceQuantity,");
		sql.append("    SUM(normalInfo.amount)   AS sumProductAndPriceAmount");
		sql.append("  FROM normalInfo");
		sql.append("  GROUP BY normalInfo.shopId,");
		sql.append("    normalInfo.productId,");
		sql.append("    normalInfo.price");
		sql.append("  )");
		sql.append(" SELECT normalInfo.shopId,");
		sql.append("  normalInfo.productId,");
		sql.append("  normalInfo.productCode,");
		sql.append("  normalInfo.productName,");
		sql.append("  normalInfo.price,");
		sql.append("  to_char(normalInfo.createDate) as createDate,");
		sql.append("  normalInfo.staffId,");
		sql.append("  normalInfo.staffCode,");
		sql.append("  normalInfo.staffName,");
		sql.append("  normalInfo.customerCode,");
		sql.append("  normalInfo.customerName,");
		sql.append("  normalInfo.customerAddress,");
		sql.append("  normalInfo.customerId,");
		sql.append("  normalInfo.orderNumber,");
		sql.append("  normalInfo.quantity,");
		sql.append("  normalInfo.amount,");
		sql.append("  sumStaffInfo.sumStaffQuantity,");
		sql.append("  sumStaffInfo.sumStaffAmount,");
		sql.append("  sumDayInfo.sumDayQuantity,");
		sql.append("  sumDayInfo.sumDayAmount,");
		sql.append("  sumProuctAndPriceInfo.sumProductAndPriceQuantity,");
		sql.append("  sumProuctAndPriceInfo.sumProductAndPriceAmount");
		sql.append(" FROM sumStaffInfo");
		sql.append(" JOIN normalInfo");
		sql.append(" ON (sumStaffInfo.shopId   = normalInfo.shopId");
		sql.append(" AND normalInfo.productId  = sumStaffInfo.productId");
		sql.append(" AND normalInfo.price      = sumStaffInfo.price");
		sql.append(" AND to_char(normalInfo.createDate) = to_char(sumStaffInfo.createDate)");
		sql.append(" AND normalInfo.staffId    = sumStaffInfo.staffId)");
		sql.append(" JOIN sumDayInfo");
		sql.append(" ON (sumDayInfo.shopId     = normalInfo.shopId");
		sql.append(" AND normalInfo.productId  = sumDayInfo.productId");
		sql.append(" AND normalInfo.price      = sumDayInfo.price");
		sql.append(" AND to_char(normalInfo.createDate) = to_char(sumDayInfo.createDate))");
		sql.append(" JOIN sumProuctAndPriceInfo");
		sql.append(" ON (sumProuctAndPriceInfo.shopId = normalInfo.shopId");
		sql.append(" AND normalInfo.productId         = sumProuctAndPriceInfo.productId");
		sql.append(" AND normalInfo.price             = sumProuctAndPriceInfo.price)");

		String[] fieldNames = { "shopId", "productId", "productCode", "productName", "price", "createDate", "staffId", "staffCode", "staffName", "customerCode", "customerName", "customerAddress", "customerId", "quantity", "amount", "orderNumber",
				"sumStaffQuantity", "sumStaffAmount", "sumDayQuantity", "sumDayAmount", "sumProductAndPriceQuantity", "sumProductAndPriceAmount"

		//				"customerCode","customerName","catCode","catName","productCode","productName",
		//				"quantitySale","quantityDisplay","money",
		//				"sumQuantitySaleForCategory","sumQuantityDisplayForCategory","sumMoneyForCategory",
		//				"sumQuantitySaleForCustomer","sumQuantityDisplayForCustomer","sumMoneyForCustomer"
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				//StandardBasicTypes.DATE,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG,
				StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG,
				StandardBasicTypes.BIG_DECIMAL };
		System.out.println(sql.toString());
		return repo.getListByQueryAndScalar(RptTDBHTMHProductDateStaffFollowCustomerVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	//namlb: DOANH THU BAN HANG TRONG NGAY THEO NVBH-------------------------------------------------------------------------------
	@Override
	public List<RptDTBHTNTHVBHRecordOrderVO> getListRptDTBHTNTHVBHRecordOrderVO(Long shopId, Long staffId, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH sumOrderDate AS (");
		sql.append(" SELECT");
		sql.append("   sum(so.AMOUNT)                  AS sumRevenueDate,");
		sql.append("   sum(so.DISCOUNT)                AS sumMoneyDiscountDate,");
		sql.append("   sum(sod.QUANTITY * sod.PRICE)   AS sumProductDiscountDate,");
		sql.append("   sum(so.AMOUNT    - so.DISCOUNT) AS sumMoneyDate,");
		sql.append("   sod.CREATE_DATE            AS dateOrder,");
		//sql.append("   st.staff_id           AS staffID, ");
		sql.append("   sum(0)                          AS sumDiscountDate,");
		sql.append("   sum(0)                          AS sumSKUDate,");
		sql.append("   sum(0)                          AS sumDebitDate,");
		sql.append("   sum(0)                          AS sumCashDate");
		sql.append(" FROM sale_order so");
		sql.append(" JOIN sale_order_detail sod");
		sql.append(" ON so.sale_order_id = sod.sale_order_id");
		sql.append(" JOIN product p");
		sql.append(" ON p.product_id = sod.product_id");
		sql.append(" JOIN customer cu");
		sql.append(" ON cu.customer_id = so.customer_id");
		sql.append(" JOIN staff st");
		sql.append(" ON sod.staff_id  = st.staff_id");
		sql.append(" WHERE so.shop_id = 16");
		sql.append(" AND so.APPROVED      = 1");
		sql.append(" AND so.TYPE          = 1");
		sql.append(" AND so.ORDER_TYPE    = 'IN'");
		sql.append(" AND sod.IS_FREE_ITEM = 1");
		sql.append(" group by sod.CREATE_DATE ");
		sql.append(" ), ");
		sql.append(" sumStaffCode AS (");
		sql.append(" SELECT");
		sql.append("   sum(so.AMOUNT)                  AS sumRevenueStaff,");
		sql.append("   sum(so.DISCOUNT)                AS sumMoneyDiscountStaff,");
		sql.append("   sum(sod.QUANTITY * sod.PRICE)   AS sumProductDiscountStaff,");
		sql.append("   sum(so.AMOUNT    - so.DISCOUNT) AS sumMoneyStaff,");
		sql.append("   st.staff_code              AS staffCode,");
		sql.append("   st.staff_id           AS staffID, ");
		sql.append("   sum(0)                          AS sumDiscountStaff,");
		sql.append("   sum(0)                          AS sumSKUStaff,");
		sql.append("   sum(0)                          AS sumDebitStaff,");
		sql.append("   sum(0)                          AS sumCashStaff");
		sql.append(" FROM sale_order so");
		sql.append(" JOIN sale_order_detail sod");
		sql.append(" ON so.sale_order_id = sod.sale_order_id");
		sql.append(" JOIN product p");
		sql.append(" ON p.product_id = sod.product_id");
		sql.append(" JOIN customer cu");
		sql.append(" ON cu.customer_id = so.customer_id");
		sql.append(" JOIN staff st");
		sql.append(" ON sod.staff_id  = st.staff_id");
		sql.append(" WHERE so.shop_id = 16");
		sql.append(" AND so.APPROVED      = 1");
		sql.append(" AND so.TYPE          = 1");
		sql.append(" AND so.ORDER_TYPE    = 'IN'");
		sql.append(" AND sod.IS_FREE_ITEM = 1");
		sql.append(" group by st.staff_code, st.staff_id");
		sql.append(" )");
		sql.append(" ");
		sql.append(" SELECT DISTINCT cu.SHORT_CODE         AS customerCode,");
		sql.append("   cu.CUSTOMER_NAME           AS customerName,");
		sql.append("   cu.ADDRESS                 AS customerAddress,");
		sql.append("   so.AMOUNT                  AS revenue,");
		sql.append("   so.DISCOUNT                AS moneyDiscount,");
		sql.append("   sod.QUANTITY * sod.PRICE   AS productDiscount,");
		sql.append("   so.AMOUNT    - so.DISCOUNT AS money,");
		sql.append("   st.STAFF_NAME              AS staffName,");
		sql.append("   st.staff_code              AS staffCode,");
		sql.append("   sod.CREATE_DATE            AS dateOrder,");
		sql.append("   0                          AS discount,");
		sql.append("   0                          AS SKU,");
		sql.append("   0                          AS debit,");
		sql.append("   0                          AS cash, ");
		sql.append("   sdate.sumRevenueDate AS sumRevenueDate,");
		sql.append("   sdate.sumMoneyDiscountDate AS sumMoneyDiscountDate,");
		sql.append("   sdate.sumProductDiscountDate AS sumProductDiscountDate,");
		sql.append("   sdate.sumMoneyDate AS sumMoneyDate,");
		sql.append("   sdate.sumDiscountDate AS sumDiscountDate,");
		sql.append("   sdate.sumSKUDate AS sumSKUDate,");
		sql.append("   sdate.sumDebitDate AS sumDebitDate,");
		sql.append("   sdate.sumCashDate AS sumCashDate,");
		sql.append("   sstaff.sumRevenueStaff AS sumRevenueStaff,");
		sql.append("   sstaff.sumMoneyDiscountStaff AS sumMoneyDiscountStaff,");
		sql.append("   sstaff.sumProductDiscountStaff AS sumProductDiscountStaff,");
		sql.append("   sstaff.sumMoneyStaff AS sumMoneyStaff,");
		sql.append("   sstaff.sumDiscountStaff AS sumDiscountStaff,");
		sql.append("   sstaff.sumSKUStaff AS sumSKUStaff,");
		sql.append("   sstaff.sumDebitStaff AS sumDebitStaff,");
		sql.append("   sstaff.sumCashStaff AS sumCashStaff");
		sql.append(" FROM ");
		sql.append(" sale_order so");
		sql.append(" JOIN sale_order_detail sod");
		sql.append(" ON so.sale_order_id = sod.sale_order_id");
		sql.append(" JOIN product p");
		sql.append(" ON p.product_id = sod.product_id");
		sql.append(" JOIN customer cu");
		sql.append(" ON cu.customer_id = so.customer_id");
		sql.append(" JOIN staff st");
		sql.append(" ON sod.staff_id  = st.staff_id");
		sql.append(" JOIN sumOrderDate sdate ON sdate.dateOrder = sod.CREATE_DATE ");
		sql.append(" JOIN sumStaffCode sstaff ON sstaff.staffID = st.staff_id ");
		sql.append(" WHERE ");
		//sql.append(" sstaff.staffID = sdate.staffID AND ");
		if (fromDate != null) {
			sql.append(" so.order_date >= trunc(?) AND");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" so.order_date < trunc(?) + 1 AND ");
			params.add(toDate);
		}
		if (staffId != null) {
			sql.append(" so.customer_id = ? AND ");
			params.add(staffId);
		}
		sql.append(" so.shop_id     = ? ");
		params.add(shopId);
		sql.append(" AND so.APPROVED = 1  ");
		sql.append(" AND so.TYPE = 1 ");
		sql.append(" AND so.ORDER_TYPE = 'IN' ");
		sql.append(" AND sod.IS_FREE_ITEM = 1  ");
		String[] fieldNames = { "customerCode", "customerName", "customerAddress", "revenue", "staffName", "staffCode", "moneyDiscount", "productDiscount", "money",
				"dateOrder", //, "SKU",
				"discount", "SKU", "debit", "cash", "sumRevenueDate", "sumMoneyDiscountDate", "sumProductDiscountDate", "sumMoneyDate", "sumDiscountDate", "sumSKUDate", "sumDebitDate", "sumCashDate", "sumRevenueStaff", "sumMoneyDiscountStaff",
				"sumProductDiscountStaff", "sumMoneyStaff", "sumDiscountStaff", "sumSKUStaff", "sumDebitStaff", "sumCashStaff" };

		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL

		//, StandardBasicTypes.LONG,  
		//StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
		//StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL,
		};
		System.out.println(sql.toString());
		return repo.getListByQueryAndScalar(RptDTBHTNTHVBHRecordOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	//SangTN
	@Override
	public List<RptSaleOrderOfDeliveryVO> getListRptSaleOrderOfDelivery(Long shopId, Date fromDate, Date toDate, String lstDeliveryId) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptSaleOrderOfDeliveryVO>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT so.delivery_id as deliveryId,");
		sql.append(" so.sale_order_id as saleOrderId,");
		sql.append(" so.order_date as orderDate,");
		sql.append(" so.order_number as orderNumber,");
		sql.append(" so.total as total,");
		sql.append(" so.description as note,");
		sql.append(" sf.staff_code as deliveryCode,");
		sql.append(" sf.staff_name as deliveryName,");
		sql.append(" c.short_code as customerCode,");
		sql.append(" c.customer_name as customerName,");
		sql.append(" c.address as customerAddress");
		sql.append(" FROM sale_order so,");
		sql.append(" staff sf,");
		sql.append(" customer c");
		sql.append(" WHERE sf.staff_id  = so.delivery_id");
		sql.append(" AND so.customer_id = c.customer_id");
		sql.append(" AND so.approved    = ?");
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append(" AND so.type        = ?");
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		sql.append(" AND (so.order_type  = ? or so.order_type = ?)");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.TT.getValue());
		sql.append(" AND so.shop_id     = ?");
		params.add(shopId);
		//		if (deliveryId != null) {
		//			sql.append(" AND so.delivery_id = ?");
		//			params.add(deliveryId);
		//		}

		if (!StringUtility.isNullOrEmpty(lstDeliveryId)) {
			String[] temp = lstDeliveryId.split(",");
			sql.append(" and (");
			//for (String staffCode: lstDeliveryId) {
			for (int i = 0; i < temp.length; i++) {
				String staffCode = temp[i];
				if (staffCode != null) {
					sql.append(" sf.staff_code = ? or ");
					params.add(staffCode);
				}
			}
			sql.append(" 1=0)");
		}

		if (fromDate != null) {
			sql.append(" AND so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append(" ORDER BY so.delivery_id, so.order_date DESC");
		String[] fieldNames = new String[] {//
		"deliveryCode",//1
				"deliveryName",//2
				"orderDate",//3
				"orderNumber",//4
				"customerCode",//5
				"customerName",//6
				"customerAddress",//7
				"note",//7'
				"total"//8
		};
		Type[] fieldTypes = new Type[] {//
		StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 2
				StandardBasicTypes.TIMESTAMP,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.STRING,// 6
				StandardBasicTypes.STRING,// 7
				StandardBasicTypes.STRING,// 7'
				StandardBasicTypes.BIG_DECIMAL // 8
		};
		return repo.getListByQueryAndScalar(RptSaleOrderOfDeliveryVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RptSaleOrderVO> getListRptSaleOrderVO(KPaging<RptSaleOrderVO> kPaging, Long shopId, Long deliveryStaffId, Date fromDate, Date toDate, Boolean getDeliveryStaff) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptSaleOrderVO>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" select so.sale_order_id as saleOrderId,");
		sql.append("   so.order_number as orderNumber, ");
		sql.append("   so.order_date as orderDate,");
		sql.append("   so.amount as amount,");
		sql.append("   so.total as total,");
		sql.append("   c.customer_code as customerCode,");
		sql.append("   c.customer_name as customerName,");
		sql.append("   c.phone as phoneNumber,");
		sql.append("   c.delivery_address as deliveryAddress,");
		sql.append("   s.staff_code as saleStaffCode,");
		sql.append("   s.staff_name as saleStaffName,");
		if (Boolean.TRUE.equals(getDeliveryStaff)) {
			sql.append("   ds.staff_code as deliveryStaffCode,");
			sql.append("   ds.staff_name as deliveryStaffName,");
		} else {
			sql.append("   '' as deliveryStaffCode,");
			sql.append("   '' as deliveryStaffName,");
		}
		sql.append("   (select invoice_number from invoice i where i.order_number=so.order_number)");
		sql.append("         as invoiceNumber");
		fromSql.append(" from sale_order so join customer c on so.customer_id=c.customer_id");
		fromSql.append("     join staff s on so.staff_id=s.staff_id");
		if (Boolean.TRUE.equals(getDeliveryStaff)) {
			fromSql.append("   join staff ds on ds.staff_id=so.delivery_id");
		}
		fromSql.append(" where 1=1");
		if (shopId != null) {
			fromSql.append(" and so.shop_id=? ");
			params.add(shopId);
		}
		if (deliveryStaffId != null) {
			fromSql.append(" and so.delivery_id=?");
			params.add(deliveryStaffId);
		}
		if (fromDate != null) {
			fromSql.append(" and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			fromSql.append(" and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}

		sql.append(fromSql);
		sql.append(" order by so.order_number");
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count");
		countSql.append(fromSql);

		String[] fieldNames = { "saleOrderId", //1
				"orderNumber", //2
				"orderDate", //3
				"amount", //4
				"total", //5
				"customerCode", //6
				"customerName", //7
				"phoneNumber", //8
				"deliveryAddress",//9
				"saleStaffCode", //10
				"saleStaffName", //11
				"invoiceNumber", //12
				"deliveryStaffCode", //10
				"deliveryStaffName", //11
		};
		Type[] fieldTypes = { StandardBasicTypes.LONG, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.DATE, //3
				StandardBasicTypes.BIG_DECIMAL,//4 
				StandardBasicTypes.BIG_DECIMAL, //5
				StandardBasicTypes.STRING, //6 
				StandardBasicTypes.STRING, //7
				StandardBasicTypes.STRING, //8
				StandardBasicTypes.STRING, //9
				StandardBasicTypes.STRING, //10
				StandardBasicTypes.STRING, //11
				StandardBasicTypes.STRING, //12
				StandardBasicTypes.STRING, //11
				StandardBasicTypes.STRING, //12
		};
		if (kPaging == null)
			return repo.getListByQueryAndScalar(RptSaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(RptSaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	@Override
	public List<Product> checkEnoughProductQuantityInOrder(Long saleOrderId, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select *");
		sql.append(" from product ");
		sql.append(" where product_id in (select so_d.product_id");
		sql.append("     from (select so_d.product_id, so_d.shop_id, sum(so_d.quantity) as quantity");
		sql.append("         from sale_order_detail so_d, product pro");
		sql.append("         where so_d.sale_order_id = ?");
		params.add(saleOrderId);
		sql.append("             and so_d.shop_id = ?");
		params.add(shopId);
		sql.append("             and so_d.product_id = pro.product_id");
		sql.append("         group by so_d.product_id, so_d.shop_id) so_d");
		sql.append("     left join (select * ");
		sql.append("         from stock_total ");
		sql.append("         where object_id = ? ");
		params.add(shopId);
		sql.append("         and object_type = ?) st on so_d.product_id = st.product_id");
		params.add(StockObjectType.SHOP.getValue());
		sql.append("     where so_d.quantity > st.available_quantity)");

		return repo.getListBySQL(Product.class, sql.toString(), params);
	}

	@Override
	public List<Product> checkEnoughProductQuantityInListOrder(Long shopId, List<Long> listOrderId) throws DataAccessException {
		if (null == shopId) {
			throw new IllegalArgumentException("shop is null");
		}

		if (null == listOrderId || listOrderId.size() <= 0) {
			throw new IllegalArgumentException("list SaleOrder is null or empty");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT PRO.* ");
		sql.append(" FROM PRODUCT PRO ");
		sql.append(" JOIN (  SELECT SO_D.PRODUCT_ID, SO_D.SHOP_ID, SUM(SO_D.QUANTITY) AS QUANTITY ");
		sql.append("             FROM PRODUCT PRO, SALE_ORDER_DETAIL SO_D ");
		sql.append("             WHERE 1 = 1 ");
		sql.append("                 AND PRO.PRODUCT_ID = SO_D.PRODUCT_ID ");
		sql.append("                 AND SO_D.SHOP_ID = ? ");
		params.add(shopId);

		sql.append("                 AND SO_D.SALE_ORDER_ID IN ( ");
		for (int i = 0; i < listOrderId.size(); i++) {
			if (i < listOrderId.size() - 1) {
				sql.append("?, ");
			} else {
				sql.append("?");
			}

			params.add(listOrderId.get(i));
		}
		sql.append(" ) ");

		sql.append("             GROUP BY SO_D.PRODUCT_ID, SO_D.SHOP_ID) SO ON PRO.PRODUCT_ID = SO.PRODUCT_ID ");
		sql.append(" JOIN(   SELECT PRODUCT_ID, AVAILABLE_QUANTITY, OBJECT_ID ");
		sql.append("     FROM STOCK_TOTAL ");
		sql.append("     WHERE 1 = 1 ");
		sql.append("         AND OBJECT_ID = ? ");
		params.add(shopId);

		sql.append("         AND OWNER_TYPE = 1) STO ON SO.SHOP_ID = STO.OBJECT_ID AND STO.PRODUCT_ID = SO.PRODUCT_ID  ");
		sql.append(" WHERE SO.QUANTITY > STO.AVAILABLE_QUANTITY ");

		return repo.getListBySQL(Product.class, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SaleOrderDAO#getListReturnSaleOrder(ths.dms.
	 * core.entities.enumtype.KPaging, java.lang.Long,
	 * ths.dms.core.entities.enumtype.OrderType, java.util.Date,
	 * java.util.Date, java.lang.Long)
	 */
	@Override
	public List<SaleOrder> getListRptReturnSaleOrder(KPaging<SaleOrder> kPaging, Long shopId, OrderType orderType, SaleOrderStatus status, SaleOrderType type, Date fromDate, Date toDate, Long deliveryId) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<SaleOrder>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from SALE_ORDER sc where 1 = 1");
		if (shopId != null) {
			sql.append(" and shop_id = ?");
			params.add(shopId);
		}
		if (deliveryId != null) {
			sql.append(" and delivery_id = ?");
			params.add(deliveryId);
		}
		if (orderType != null) {
			sql.append(" and order_type = ?");
			params.add(orderType.getValue());
		}
		if (null != status) {
			sql.append(" and APPROVED = ?");
			params.add(status.getValue());
		}
		if (null != type) {
			sql.append(" and TYPE = ?");
			params.add(type.getValue());
		}
		if (fromDate != null) {
			sql.append(" and ORDER_DATE >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and ORDER_DATE < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append(" order by sale_order_id desc");
		if (kPaging == null)
			return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<SaleOrder> getListSaleOrder(KPaging<SaleOrder> kPaging, Long shopId, String shortCode, String staffCode, Float vat, InvoiceStatus invSatus, Date fromDate, Date toDate) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<SaleOrder>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from SALE_ORDER sc where 1 = 1");
		sql.append(" SELECT so.*");
		sql.append(" FROM sale_order so");
		sql.append(" JOIN invoice inv");
		sql.append(" ON (so.invoice_id = inv.invoice_id)");
		sql.append(" WHERE so.shop_id  = ?");
		params.add(shopId);
		if (vat != null) {
			sql.append(" AND inv.vat    = ?");
			params.add(vat);
		}
		if (shortCode != null) {
			sql.append(" AND EXISTS (SELECT 1 FROM customer c WHERE so.customer_id = c.customer_id AND UPPER(c.short_code)  = ?)");
			params.add(shortCode.toUpperCase());
		}
		if (staffCode != null) {
			sql.append(" AND EXISTS (SELECT 1 FROM staff s WHERE so.staff_id = s.staff_id AND UPPER(s.staff_code)  = ?)");
			params.add(staffCode.toUpperCase());
		}
		if (fromDate != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		if (invSatus != null) {
			sql.append(" AND inv.status = ?");
			params.add(invSatus.getValue());
		}
		if (kPaging == null)
			return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<RptSaleOrderInDateVO> getListRptSaleOrderInDateVO(KPaging<RptSaleOrderInDateVO> kPaging, Date fromDate, Date toDate, List<Long> lstStaffId, Long shopId) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptSaleOrderInDateVO>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		sql.append(" select * from (");
		sql.append(" SELECT ");
		sql.append("   (SELECT short_code || ' - ' || customer_name || ' - ' || address ");
		sql.append("   FROM customer ");
		sql.append("   WHERE customer_id = so.customer_id ");
		sql.append("   ) customerInfo, ");
		sql.append("   order_number orderNumber, ");
		sql.append("   (select LISTAGG(invoice_number, ', ') WITHIN GROUP (ORDER BY invoice_number)");
		sql.append("	from invoice where invoice_id in (select invoice_id from sale_order_detail sod ");
		sql.append("	where sod.sale_order_id = so.sale_order_id)) invoiceNumber, ");
		sql.append("   order_source orderSource, ");
		sql.append("   total, ");
		sql.append("   0 discount, amount, ");
		sql.append("   discount totalPromotion, ");
		sql.append("   (SELECT COUNT(DISTINCT product_id) ");
		sql.append("   FROM sale_order_detail ");
		sql.append("   WHERE sale_order_id   = so.sale_order_id ");
		sql.append("   and is_free_item = 0 ");
		sql.append("   ) numSku, ");
		sql.append("   (select SUM(nvl(sod.quantity,0) * nvl(sod.price,0)) from sale_order_detail sod where sod.sale_order_id = so.sale_order_id");
		sql.append("    	and sod.is_free_item = 1 and program_type not in (3,4,5)) kmHang,");
		sql.append("   order_date orderDate,");
		sql.append("	(select staff_name || ' - ' || staff_code from staff where staff_id = so.staff_id) staffInfo");
		fromSql.append(" FROM sale_order so where 1=1 ");
		if (fromDate != null) {
			fromSql.append(" and order_date >= trunc(?) ");
			params.add(fromDate);
		}
		if (toDate != null) {
			fromSql.append(" AND order_date   < trunc(?) + 1 ");
			params.add(toDate);
		}
		fromSql.append(" AND approved      = 1 ");
		fromSql.append(" AND type          = 1 ");
		fromSql.append(" AND order_type    = 'IN' ");

		if (lstStaffId != null && lstStaffId.size() != 0 && lstStaffId.get(0) != null) {
			fromSql.append(" and (");
			for (Long staffId : lstStaffId) {
				if (staffId != null) {
					fromSql.append(" staff_id = ? or ");
					params.add(staffId);
				}
			}
			fromSql.append(" 1=0)");
		}

		if (shopId != null) {
			fromSql.append(" and shop_id = ?");
			params.add(shopId);
		}

		sql.append(fromSql);
		sql.append(" ) order by staffInfo, orderDate, customerInfo");
		countSql.append(" select count(1) as count");
		countSql.append(fromSql);

		String[] fieldNames = { "customerInfo", "orderNumber", "invoiceNumber", "orderSource",

		"total", "discount", "totalPromotion",

		"numSku", "orderDate", "staffInfo", "amount", "kmHang" };

		Type[] fieldTypes = { StandardBasicTypes.STRING, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.INTEGER, // 3

				StandardBasicTypes.BIG_DECIMAL, // 4
				StandardBasicTypes.BIG_DECIMAL, // 5
				StandardBasicTypes.BIG_DECIMAL, // 6

				StandardBasicTypes.INTEGER, StandardBasicTypes.DATE, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL };

		if (kPaging == null)
			return repo.getListByQueryAndScalar(RptSaleOrderInDateVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(RptSaleOrderInDateVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	@Override
	public List<RptRevenueInDateVO> getListRptRevenueInDateVO(KPaging<RptRevenueInDateVO> kPaging, Date fromDate, Date toDate, Long staffId, Long shopId) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptRevenueInDateVO>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" select s.shop_name as shopName,");
		sql.append(" s.address as shopAddress,");
		sql.append(" so.order_number as orderNumber,");
		sql.append(" so.order_date as orderDate,");
		sql.append(" so.ORDER_SOURCE as orderState,");
		sql.append(" so.total as total,");
		sql.append(" so.discount as discount,");
		sql.append(" c.customer_code as customerCode,");
		sql.append(" c.customer_name as customerName,");
		sql.append(" c.address customerAddress,");
		sql.append(" count(d.product_id) as numSku,");
		sql.append(" st.staff_name as staffName,");
		sql.append(" st.staff_code as staffCode");
		sql.append(" db.total_pay as totalPay");
		sql.append(" db.remain as remain");

		fromSql.append(" from sale_order so join shop s on so.shop_id = s.shop_id");
		fromSql.append(" 	join staff st on st.staff_id = so.staff_id");
		fromSql.append(" 	join customer c on so.customer_id = c.customer_id");
		fromSql.append(" 	join debit_detail db on db.from_object_id = so.sale_order_id");
		fromSql.append(" 	join debit dg on dg.debit_id = db.debit_id and dg.object_type = ?");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		fromSql.append("    left join sale_order_detail d on d.sale_order_id = so.sale_order_id and d.program_type not in (?,?,?)");

		params.add(ProgramType.AUTO_PROM.getValue());
		params.add(ProgramType.MANUAL_PROM.getValue());
		params.add(ProgramType.DISPLAY_SCORE.getValue());

		fromSql.append(" where 1 = 1");
		if (fromDate != null) {
			fromSql.append("   and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			fromSql.append(" 	and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		if (staffId != null) {
			fromSql.append(" 	and so.staff_id = ?");
			params.add(staffId);
		}
		if (shopId != null) {
			fromSql.append(" 	and so.shop_id = ?");
			params.add(shopId);
		}
		fromSql.append(" 	and so.approved = ?");
		params.add(SaleOrderStatus.APPROVED.getValue());

		fromSql.append(" 	and so.type = ?");
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());

		fromSql.append(" 	and so.order_type = ? ");
		params.add(OrderType.IN.getValue());

		sql.append(fromSql);
		sql.append("   group by s.shop_name, s.address, so.order_number, so.order_date, so.ORDER_SOURCE, so.total, so.discount, i.invoice_number, c.customer_code, c.customer_name, c.address, st.staff_name, st.staff_code");
		sql.append("   order by staffCode, orderDate");
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count");
		countSql.append(fromSql);

		String[] fieldNames = { "shopName", // 0
				"shopAddress", // 1
				"orderNumber", // 2
				"orderDate", // 3
				"orderState", // 4
				"total", // 5
				"discount", // 6
				"invoiceNumber", // 7
				"customerCode", // 8
				"customerName", // 9
				"customerAddress", // 10
				"numSku", // 11
				"staffName", // 12
				"staffCode", // 13
				"totalPay", "remain" };

		Type[] fieldTypes = { StandardBasicTypes.STRING, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.DATE, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.BIG_DECIMAL, // 5
				StandardBasicTypes.BIG_DECIMAL, // 6
				StandardBasicTypes.STRING, // 7
				StandardBasicTypes.STRING, // 8
				StandardBasicTypes.STRING, // 9
				StandardBasicTypes.STRING, // 10
				StandardBasicTypes.STRING, // 11
				StandardBasicTypes.STRING, // 12
				StandardBasicTypes.STRING, // 13
				StandardBasicTypes.BIG_DECIMAL, // 14
				StandardBasicTypes.BIG_DECIMAL, // 15
		};

		if (kPaging == null)
			return repo.getListByQueryAndScalar(RptRevenueInDateVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(RptRevenueInDateVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	/*
	 * @Override public List<SaleOrder> getListSaleOrderWithInvoice(Long shopId,
	 * String saleStaffCode, String deliveryStaffCode, Float vat, String
	 * orderNumber, Date fromDate, Date toDate, String customerCode,
	 * InvoiceCustPayment custPayment) throws DataAccessException {
	 * 
	 * if (fromDate != null && toDate != null && fromDate.after(toDate)) return
	 * new ArrayList<SaleOrder>();
	 * 
	 * StringBuilder sql = new StringBuilder(); List<Object> params = new
	 * ArrayList<Object>(); sql.append(" SELECT so.*"); sql.append(
	 * " FROM sale_order so INNER JOIN invoice i ON so.invoice_id = i.invoice_id "
	 * );
	 * sql.append(" INNER JOIN customer c ON so.customer_id = c.customer_id");
	 * sql.append(" INNER JOIN staff ss ON so.staff_id = ss.staff_id");
	 * sql.append(" INNER JOIN staff ds ON so.delivery_id = ds.staff_id");
	 * sql.append(" WHERE so.order_type in (?,?)");
	 * params.add(OrderType.IN.getValue()); params.add(OrderType.SO.getValue());
	 * if(shopId != null) { sql.append(" AND so.shop_id = ?");
	 * params.add(shopId); } if(customerCode != null) {
	 * sql.append(" AND lower(c.short_code) LIKE ?");
	 * params.add(StringUtility.toOracleSearchLikeSuffix
	 * (customerCode.toLowerCase())); } if(saleStaffCode != null) {
	 * sql.append(" AND lower(ss.staff_code) LIKE ?");
	 * params.add(StringUtility.toOracleSearchLikeSuffix
	 * (saleStaffCode.toLowerCase())); } if(deliveryStaffCode != null) {
	 * sql.append(" AND lower(ds.staff_code) LIKE ?");
	 * params.add(StringUtility.toOracleSearchLikeSuffix
	 * (deliveryStaffCode.toLowerCase())); } sql.append(" AND EXISTS ");
	 * sql.append(" (SELECT 1"); sql.append(" FROM invoice i");
	 * sql.append(" WHERE i.invoice_id = invoice_id");
	 * sql.append(" AND i.status IN (?, ?)");
	 * params.add(InvoiceStatus.USING.getValue());
	 * params.add(InvoiceStatus.MODIFIED.getValue()); if(custPayment != null) {
	 * sql.append(" AND i.cust_payment = ?");
	 * params.add(custPayment.getValue()); } sql.append(" )");
	 * sql.append(" AND so.invoice_id is not null");
	 * 
	 * sql.append(" AND i.status IN (?, ?) ");
	 * params.add(InvoiceStatus.USING.getValue());
	 * params.add(InvoiceStatus.MODIFIED.getValue());
	 * 
	 * if(custPayment != null) { sql.append(" AND i.cust_payment = ?");
	 * params.add(custPayment.getValue()); }
	 * 
	 * if(vat != null) { sql.append(" AND so.vat = ?"); params.add(vat); }
	 * if(orderNumber != null) {
	 * sql.append(" AND lower(so.order_number) LIKE ?");
	 * params.add(StringUtility
	 * .toOracleSearchLikeSuffix(orderNumber.toLowerCase())); } if(fromDate !=
	 * null) { sql.append(" AND so.order_date >= TRUNC(?)");
	 * params.add(fromDate); } if(toDate != null) {
	 * sql.append(" AND so.order_date < TRUNC(?) + 1"); params.add(toDate); }
	 * sql.append(" ORDER BY so.order_number desc");
	 * 
	 * return repo.getListBySQL(SaleOrder.class, sql.toString(), params); }
	 */

	/*
	 * @Override public List<SaleOrder> getListSaleOrderWithInvoiceEx1(Long
	 * shopId, String saleStaffCode, String deliveryStaffCode, Float vat, String
	 * orderNumber, Date fromDate, Date toDate, String customerCode,
	 * InvoiceCustPayment custPayment) throws DataAccessException {
	 * 
	 * if (fromDate != null && toDate != null && fromDate.after(toDate)) return
	 * new ArrayList<SaleOrder>();
	 * 
	 * StringBuilder sql = new StringBuilder(); List<Object> params = new
	 * ArrayList<Object>(); sql.append(" SELECT distinct so.*"); sql.append(
	 * " FROM sale_order so INNER JOIN customer c ON so.customer_id = c.customer_id"
	 * ); sql.append(" LEFT JOIN staff ss ON so.staff_id = ss.staff_id");
	 * sql.append(" LEFT JOIN staff ds ON so.delivery_id = ds.staff_id");
	 * sql.append
	 * (" INNER JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id"
	 * ); sql.append(" INNER JOIN invoice i ON sod.invoice_id = i.invoice_id");
	 * sql.append(" WHERE so.order_type in (?,?)");
	 * params.add(OrderType.IN.getValue()); params.add(OrderType.SO.getValue());
	 * if(shopId != null) { sql.append(" AND so.shop_id = ?");
	 * params.add(shopId); } if(customerCode != null) {
	 * sql.append(" AND lower(c.short_code) LIKE ?");
	 * params.add(StringUtility.toOracleSearchLikeSuffix
	 * (customerCode.toLowerCase())); } if(saleStaffCode != null) {
	 * sql.append(" AND lower(ss.staff_code) LIKE ?");
	 * params.add(StringUtility.toOracleSearchLikeSuffix
	 * (saleStaffCode.toLowerCase())); } if(deliveryStaffCode != null) {
	 * sql.append(" AND lower(ds.staff_code) LIKE ?");
	 * params.add(StringUtility.toOracleSearchLikeSuffix
	 * (deliveryStaffCode.toLowerCase())); } sql.append(" AND EXISTS ");
	 * sql.append(" (SELECT 1"); sql.append(" FROM invoice i");
	 * sql.append(" WHERE i.invoice_id = invoice_id");
	 * sql.append(" AND i.status IN (?, ?)");
	 * params.add(InvoiceStatus.USING.getValue());
	 * params.add(InvoiceStatus.MODIFIED.getValue()); if(custPayment != null) {
	 * sql.append(" AND i.cust_payment = ?");
	 * params.add(custPayment.getValue()); } sql.append(" )");
	 * sql.append(" AND sod.invoice_id is not null");
	 * 
	 * sql.append(" AND i.status IN (?) ");
	 * params.add(InvoiceStatus.USING.getValue());
	 * //params.add(InvoiceStatus.MODIFIED.getValue());
	 * 
	 * if(custPayment != null) { sql.append(" AND i.cust_payment = ?");
	 * params.add(custPayment.getValue()); }
	 * 
	 * if(vat != null) { sql.append(" AND so.vat = ?"); params.add(vat); }
	 * if(orderNumber != null) {
	 * sql.append(" AND lower(so.order_number) LIKE ?");
	 * params.add(StringUtility
	 * .toOracleSearchLikeSuffix(orderNumber.toLowerCase())); } if(fromDate !=
	 * null) { sql.append(" AND so.order_date >= TRUNC(?)");
	 * params.add(fromDate); } if(toDate != null) {
	 * sql.append(" AND so.order_date < TRUNC(?) + 1"); params.add(toDate); }
	 * sql.append(" ORDER BY so.order_number desc");
	 * 
	 * return repo.getListBySQL(SaleOrder.class, sql.toString(), params); }
	 */

	/*
	 * @Override public List<SaleOrder> getListSaleOrderWithNoInvoice(Long
	 * shopId, String saleStaffCode, String deliveryStaffCode, Float vat, String
	 * orderNumber, Date fromDate, Date toDate, String customerCode) throws
	 * DataAccessException {
	 * 
	 * if (fromDate != null && toDate != null && fromDate.after(toDate)) return
	 * new ArrayList<SaleOrder>();
	 * 
	 * StringBuilder sql = new StringBuilder(); List<Object> params = new
	 * ArrayList<Object>(); sql.append(" SELECT so.*");
	 * sql.append(" FROM sale_order so");
	 * sql.append(" INNER JOIN customer c ON so.customer_id = c.customer_id");
	 * sql.append(" INNER JOIN staff ss ON so.staff_id = ss.staff_id");
	 * sql.append(" INNER JOIN staff ds ON so.delivery_id = ds.staff_id");
	 * sql.append(" WHERE 1          = 1"); if(shopId != null) {
	 * sql.append(" AND so.shop_id = ?"); params.add(shopId); } if(customerCode
	 * != null) { sql.append(" AND lower(c.short_code) LIKE ?");
	 * params.add(StringUtility
	 * .toOracleSearchLikeSuffix(customerCode.toLowerCase())); }
	 * if(saleStaffCode != null) {
	 * sql.append(" AND lower(ss.staff_code) LIKE ?");
	 * params.add(StringUtility.toOracleSearchLikeSuffix
	 * (saleStaffCode.toLowerCase())); } if(deliveryStaffCode != null) {
	 * sql.append(" AND lower(ds.staff_code) LIKE ?");
	 * params.add(StringUtility.toOracleSearchLikeSuffix
	 * (deliveryStaffCode.toLowerCase())); }
	 * sql.append(" AND sod.invoice_id is null");
	 * sql.append(" AND so.order_type IN (?, ?)");
	 * params.add(OrderType.IN.getValue()); params.add(OrderType.SO.getValue());
	 * sql.append(" AND so.approved = ?");
	 * params.add(SaleOrderStatus.APPROVED.getValue());
	 * sql.append(" AND so.type = ?");
	 * params.add(SaleOrderType.NOT_YET_RETURNED.getValue()); if(vat != null) {
	 * sql.append(" AND so.vat = ?"); params.add(vat); } if(orderNumber != null)
	 * { sql.append(" AND lower(so.order_number) LIKE ?");
	 * params.add(StringUtility
	 * .toOracleSearchLikeSuffix(orderNumber.toLowerCase())); } if(fromDate !=
	 * null) { sql.append(" AND so.order_date >= TRUNC(?)");
	 * params.add(fromDate); } if(toDate != null) {
	 * sql.append(" AND so.order_date < TRUNC(?) + 1"); params.add(toDate); }
	 * sql.append(" ORDER BY so.order_number desc");
	 * 
	 * return repo.getListBySQL(SaleOrder.class, sql.toString(), params); }
	 */

	@Override
	public List<SaleOrder> getListSaleOrderWithNoInvoiceEx1(KPaging<SaleOrder> kPaging, Long shopId, String saleStaffCode, String deliveryStaffCode, String orderNumber, Date fromDate, Date toDate, String customerCode, Date lockDate,
			Integer isValueOrder, Integer isVAT, Integer numberValueOrder) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<SaleOrder>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT distinct so.*");
		sql.append(" FROM sale_order so");
		sql.append(" INNER JOIN customer c ON so.customer_id = c.customer_id");
		sql.append(" LEFT JOIN staff ss ON so.staff_id = ss.staff_id");
		sql.append(" LEFT JOIN staff ds ON so.delivery_id = ds.staff_id");
		sql.append(" INNER JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id");
		sql.append(" WHERE 1          = 1");
		if (isVAT != null) { // phuongvm
			sql.append(" and c.is_vat = ? "); //don le thuoc tinh is_vat duoc kich hoat (isvat=1)
			params.add(isVAT);
			if (isVAT.equals(0)) {
				sql.append(" and so.parent_cp_sale_order_id  is null ");
			}
		}
		if (isValueOrder != null && isValueOrder == 0 && numberValueOrder!= null) {
			sql.append(" and so.amount < ? ");
			params.add(numberValueOrder);
		}
		if (isValueOrder != null && isValueOrder == 1 && numberValueOrder != null) {
			sql.append(" and so.amount >= ? ");
			params.add(numberValueOrder);
		}
		if (shopId != null) {
			sql.append(" AND so.shop_id = ?");
			params.add(shopId);
		}
		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append(" AND lower(c.short_code) LIKE ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(saleStaffCode)) {
			sql.append(" AND (ss.staff_code is null or lower(ss.staff_code) LIKE ?)");
			params.add(StringUtility.toOracleSearchLikeSuffix(saleStaffCode.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(deliveryStaffCode)) {
			sql.append(" AND (ds.staff_code is null or lower(ds.staff_code) LIKE ?) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(deliveryStaffCode.toLowerCase()));
		}
		sql.append(" AND sod.invoice_id is null and (sod.program_type is null or sod.program_type = 0 or sod.program_type = 1 or sod.program_type = 2)");
		sql.append(" AND so.order_type IN (?, ?)");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		sql.append(" AND so.approved = ?");
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append(" AND so.approved_step in (2,3)");
		sql.append(" AND so.type = ?");
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		if (orderNumber != null) {
			sql.append(" AND lower(so.order_number) LIKE ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(orderNumber.toLowerCase()));
		}
		if (fromDate != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			if (DateUtility.compareDateWithoutTime(toDate, lockDate) == 1) {
				sql.append(" AND so.order_date < TRUNC(?) + 1");
				params.add(lockDate);
			} else {
				sql.append(" AND so.order_date < TRUNC(?) + 1");
				params.add(toDate);
			}

		}
		sql.append(" AND not exists (");
		sql.append(" select inv.sale_order_id ");
		sql.append(" from invoice inv ");
		sql.append(" where inv.status = 0");
		if (shopId != null) {
			sql.append(" AND inv.shop_id = ?");
			params.add(shopId);
		}
		if (fromDate != null) {
			sql.append(" AND inv.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			if (DateUtility.compareDateWithoutTime(toDate, lockDate) == 1) {
				sql.append(" AND inv.order_date < TRUNC(?) + 1");
				params.add(lockDate);
			} else {
				sql.append(" AND inv.order_date < TRUNC(?) + 1");
				params.add(toDate);
			}
		}
		sql.append(" and so.sale_order_id = inv.sale_order_id");
		sql.append(" )");
		sql.append(" ORDER BY so.order_number");

		if (kPaging != null) {
			return repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, kPaging);
		} else {
			return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
		}
	}

	@Override
	public List<SaleOrderDetail> getListSaleOrderWithNoInvoiceEx1SOD(KPaging<SaleOrder> kPaging, Long shopId, String saleStaffCode, String deliveryStaffCode, String orderNumber, Date fromDate, Date toDate, String customerCode, Date lockDate,
			Integer isValueOrder, Integer numberValueOrder) throws DataAccessException {
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<SaleOrderDetail>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from sale_order_detail ");
		sql.append(" where sale_order_id in( ");
		sql.append(" SELECT DISTINCT so.sale_order_id");
		sql.append(" FROM sale_order so");
		sql.append(" INNER JOIN customer c ON so.customer_id = c.customer_id");
		sql.append(" LEFT JOIN staff ss ON so.staff_id = ss.staff_id");
		sql.append(" LEFT JOIN staff ds ON so.delivery_id = ds.staff_id");
		sql.append(" INNER JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id");
		sql.append(" WHERE 1          = 1");
		sql.append(" and c.is_vat = 1 "); //don le thuoc tinh is_vat duoc kich hoat
		if (isValueOrder != null && isValueOrder == 0 && numberValueOrder != null) {
			sql.append(" and so.amount < ? ");
			params.add(numberValueOrder);
		}
		if (isValueOrder != null && isValueOrder == 1 && numberValueOrder != null) {
			sql.append(" and so.amount >= ? ");
			params.add(numberValueOrder);
		}
		if (shopId != null) {
			sql.append(" AND so.shop_id = ?");
			params.add(shopId);
		}
		if (customerCode != null) {
			sql.append(" AND lower(c.short_code) LIKE ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(customerCode.toLowerCase()));
		}
		if (saleStaffCode != null) {
			sql.append(" AND (ss.staff_code is null or lower(ss.staff_code) LIKE ?) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(saleStaffCode.toLowerCase()));
		}
		if (deliveryStaffCode != null) {
			sql.append(" AND (ds.staff_code is null or lower(ds.staff_code) LIKE ?) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(deliveryStaffCode.toLowerCase()));
		}
		sql.append(" AND sod.invoice_id is null and (sod.program_type is null or sod.program_type = 0 or sod.program_type = 1 or sod.program_type = 2)");
		sql.append(" AND so.order_type IN (?, ?)");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		sql.append(" AND so.approved = ?");
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append(" AND so.approved_step in (2,3)");
		sql.append(" AND so.type = ?");
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		if (orderNumber != null) {
			sql.append(" AND lower(so.order_number) LIKE ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(orderNumber.toLowerCase()));
		}
		if (fromDate != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			if (DateUtility.compareDateWithoutTime(toDate, lockDate) == 1) {
				sql.append(" AND so.order_date < TRUNC(?) + 1");
				params.add(lockDate);
			} else {
				sql.append(" AND so.order_date < TRUNC(?) + 1");
				params.add(toDate);
			}
		}
		sql.append(" AND not exists (");
		sql.append(" select inv.sale_order_id ");
		sql.append(" from invoice inv ");
		sql.append(" where inv.status = 0");
		if (shopId != null) {
			sql.append(" AND inv.shop_id = ?");
			params.add(shopId);
		}
		if (fromDate != null) {
			sql.append(" AND inv.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			if (DateUtility.compareDateWithoutTime(toDate, lockDate) == 1) {
				sql.append(" AND inv.order_date < TRUNC(?) + 1");
				params.add(lockDate);
			} else {
				sql.append(" AND inv.order_date < TRUNC(?) + 1");
				params.add(toDate);
			}
		}
		sql.append(" and so.sale_order_id = inv.sale_order_id ");
		sql.append(" )");
		sql.append(" ) ");

		//		if(fromDate != null) {
		//			sql.append(" AND order_date >= TRUNC(?)");
		//			params.add(fromDate);
		//		}
		//		if(toDate != null) {
		//			if(DateUtility.compareDateWithoutTime(toDate, lockDate) == 1){
		//				sql.append(" AND order_date < TRUNC(?) + 1");
		//				params.add(lockDate);
		//			}else{
		//				sql.append(" AND order_date < TRUNC(?) + 1");
		//				params.add(toDate);
		//			}
		//		}
		sql.append(" and product_id is not null and invoice_id is null");
		sql.append(" order by sale_order_id, vat desc, program_code, is_free_item desc, sale_order_detail_id");
		return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
	}

	@Override
	public List<RptSaleOrderByProductDataVO> getDataForSaleOrderByProductReport(Long shopId, List<Long> staffIds, Date fromDate, Date toDate) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptSaleOrderByProductDataVO>();

		if (null == fromDate) {
			throw new IllegalArgumentException("From date is invalidate");
		}

		if (null == toDate) {
			throw new IllegalArgumentException("To date is invalidate");
		}

		if (fromDate.compareTo(toDate) > 0) {
			throw new IllegalArgumentException("From date can't after to date");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT STA.STAFF_ID AS staffId, STA.STAFF_CODE AS staffCode, STA.STAFF_NAME AS staffName,");
		sql.append(" 	NVL(SO_D.QUANTITY, 0) AS quantity, NVL(SO_D.AMOUNT, 0) AS amount, NVL(SO_D.PRICE, 0) AS price, ");
		sql.append("    PRO.PRODUCT_ID AS productId, PRO.PRODUCT_CODE AS productCode, PRO.PRODUCT_NAME AS productName, PRO_I.PRODUCT_INFO_NAME AS productInfo ");
		sql.append(" FROM SALE_ORDER SO, SALE_ORDER_DETAIL SO_D, STAFF STA, PRODUCT PRO, PRODUCT_LEVEL PRO_L, PRODUCT_INFO PRO_I ");
		sql.append(" WHERE SO.SALE_ORDER_ID = SO_D.SALE_ORDER_ID ");
		sql.append("     AND SO.STAFF_ID = STA.STAFF_ID ");
		sql.append("     AND SO.SHOP_ID = ? ");

		params.add(shopId);

		if (null != staffIds && staffIds.size() > 0) {
			sql.append("     AND STA.STAFF_ID IN (");

			for (int i = 0; i < staffIds.size(); i++) {
				if (i < staffIds.size() - 1) {
					sql.append(" ?,");
				} else {
					sql.append(" ?");
				}

				params.add(staffIds.get(i));
			}

			sql.append(") ");
		}

		sql.append("     AND SO.ORDER_DATE >= trunc(?) and  SO.ORDER_DATE < trunc(?) + 1");

		params.add(fromDate);
		params.add(toDate);

		sql.append("     AND SO.APPROVED = ? ");
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append("     AND SO.ORDER_TYPE = ? ");
		params.add(OrderType.IN.getValue());
		sql.append("     AND SO_D.IS_FREE_ITEM = 0 ");
		sql.append("     AND PRO.PRODUCT_ID = SO_D.PRODUCT_ID ");
		sql.append("     AND PRO.PRODUCT_LEVEL_ID = PRO_L.PRODUCT_LEVEL_ID ");
		sql.append("     AND PRO_L.CAT_ID = PRO_I.PRODUCT_INFO_ID ");
		sql.append("     AND PRO_I.TYPE = ? ");
		params.add(ProductType.CAT.getValue());
		sql.append(" ORDER BY STA.STAFF_CODE ASC, PRO_I.PRODUCT_INFO_NAME ASC, PRO.PRODUCT_CODE ASC ");

		String[] fieldNames = { "staffId", // 0
				"staffCode", // 1
				"staffName", // 1'
				"quantity", // 2
				"amount", // 3
				"price", // 4
				"productId", // 5
				"productCode", // 6
				"productName", // 7
				"productInfo" // 8
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 1'
				StandardBasicTypes.LONG, // 2
				StandardBasicTypes.BIG_DECIMAL, // 3
				StandardBasicTypes.FLOAT, // 4
				StandardBasicTypes.LONG, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.STRING, // 7
				StandardBasicTypes.STRING, // 8
		};

		return repo.getListByQueryAndScalar(RptSaleOrderByProductDataVO.class, fieldNames, fieldTypes, sql.toString(), params);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SaleOrderDAO#getListRptTotalAmount(java.lang.Long,
	 * java.util.Date, java.util.Date, java.lang.Long)
	 */
	@Override
	public List<RptTotalAmountVO> getListRptTotalAmount(Long shopId, Date fromDate, Date toDate, String staffCode) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptTotalAmountVO>();

		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT so.order_date AS orderDate,");
		sql.append(" so.order_number    AS orderNumber,");
		sql.append(" so.total           AS total,");
		sql.append(" so.discount        AS discount,");
		sql.append(" sod.is_free_item   AS isFreeItem,");
		sql.append(" sod.price   		AS priceValue,");
		sql.append(" sod.quantity   	AS quantity,");
		sql.append(" s.staff_code       AS staffCode,");
		sql.append(" s.staff_name       AS staffName,");
		sql.append(" d.total_pay        AS totalPay,");
		sql.append(" d.remain           AS remain");
		sql.append(" FROM sale_order so,");
		sql.append(" staff s,");
		sql.append(" sale_order_detail sod,");
		sql.append(" debit_detail d");
		sql.append(" WHERE so.sale_order_id = sod.sale_order_id");
		sql.append(" AND so.staff_id        = s.staff_id");
		sql.append(" AND d.from_object_id   = so.sale_order_id");
		sql.append(" AND so.approved        = ?");
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append(" AND so.type            = ?");
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		sql.append(" AND so.order_type      = ?");
		params.add(OrderType.IN.getValue());
		sql.append(" AND so.shop_id            = ?");
		params.add(shopId);
		if (fromDate != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		if (staffCode != null) {
			sql.append(" AND s.staff_code = ?");
			params.add(staffCode);
		}
		sql.append(" ORDER BY s.staff_code, so.order_date");
		String[] fieldNames = { "orderDate", //1
				"orderNumber", //2
				"total", //3
				"discount", //4
				"isFreeItem", //5
				"priceValue", //5
				"quantity", //5
				"staffCode", //6
				"staffName", //7
				"totalPay", //8
				"remain", //9
		};
		Type[] fieldTypes = { StandardBasicTypes.TIMESTAMP, //1
				StandardBasicTypes.STRING, //2
				StandardBasicTypes.BIG_DECIMAL, //3
				StandardBasicTypes.BIG_DECIMAL, //4 
				StandardBasicTypes.INTEGER, //5
				StandardBasicTypes.FLOAT, //5
				StandardBasicTypes.INTEGER, //5
				StandardBasicTypes.STRING, //6
				StandardBasicTypes.STRING, //7
				StandardBasicTypes.BIG_DECIMAL, //8
				StandardBasicTypes.BIG_DECIMAL, //9
		};
		return repo.getListByQueryAndScalar(RptTotalAmountVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RptOrderByPromotionDataVO> getDataForOrderByPromotionReport(Long shopId, List<Long> staffIds, Date fromDate, Date toDate) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptOrderByPromotionDataVO>();

		if (null == shopId) {
			throw new IllegalArgumentException("Shop id is invalidate");
		}

		if (null == fromDate) {
			throw new IllegalArgumentException("From date is invalidate");
		}

		if (null == toDate) {
			throw new IllegalArgumentException("To date is invalidate");
		}

		if (fromDate.compareTo(toDate) > 0) {
			throw new IllegalArgumentException("From date can't after to date");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT PRO_P.PROMOTION_PROGRAM_CODE AS promotionCode, PRO_P.PROMOTION_PROGRAM_NAME AS promotionName,  ");
		sql.append("     SO.ORDER_DATE AS orderDate, ");
		sql.append("     STA.STAFF_CODE AS staffCode, STA.STAFF_NAME AS staffName, ");
		sql.append("     CUS.CUSTOMER_CODE AS customerCode, CUS.CUSTOMER_NAME AS customerName, ");
		sql.append("     PRO.PRODUCT_CODE AS productCode, PRO.PRODUCT_NAME AS productName, ");
		sql.append("     NVL(SO_D.PRICE, 0) AS price, NVL(SO_D.QUANTITY, 0) AS quantity, NVL(SO_D.AMOUNT, 0) AS amount ");
		sql.append(" FROM PROMOTION_PROGRAM PRO_P, CUSTOMER CUS, ");
		sql.append("     STAFF STA, PRODUCT PRO, SALE_ORDER SO, SALE_ORDER_DETAIL SO_D ");
		sql.append(" WHERE SO.SALE_ORDER_ID = SO_D.SALE_ORDER_ID ");
		sql.append("     AND SO_D.PRODUCT_ID = PRO.PRODUCT_ID ");
		sql.append("     AND SO.CUSTOMER_ID = CUS.CUSTOMER_ID ");
		sql.append("     AND SO.STAFF_ID = STA.STAFF_ID ");
		sql.append("     AND SO_D.PROGRAM_CODE = PRO_P.PROMOTION_PROGRAM_CODE ");
		sql.append("     AND SO_D.PROGRAM_TYPE IN (?, ?) ");
		params.add(ProgramType.AUTO_PROM.getValue());
		params.add(ProgramType.MANUAL_PROM.getValue());
		sql.append("     AND SO.APPROVED = ? ");
		params.add(SaleOrderStatus.APPROVED.getValue());
		sql.append("     AND SO.ORDER_TYPE = ? ");
		params.add(OrderType.IN.getValue());
		sql.append("     AND SO_D.IS_FREE_ITEM = 1 ");
		sql.append("     AND SO.SHOP_ID = ? ");
		sql.append("     AND PRO_P.FROM_DATE >= TRUNC(?) ");
		sql.append("     AND PRO_P.TO_DATE < TRUNC(?) + 1");
		sql.append("     AND SO.ORDER_DATE >= trunc(?) AND SO.ORDER_DATE < trunc(?) + 1");

		params.add(shopId);
		params.add(fromDate);
		params.add(toDate);
		params.add(fromDate);
		params.add(toDate);

		if (null != staffIds && staffIds.size() > 0) {
			sql.append("     AND STA.STAFF_ID IN (");

			for (int i = 0; i < staffIds.size(); i++) {
				if (i < staffIds.size() - 1) {
					sql.append(" ?,");
				} else {
					sql.append(" ?");
				}

				params.add(staffIds.get(i));
			}

			sql.append(") ");
		}

		sql.append(" ORDER BY PRO_P.PROMOTION_PROGRAM_CODE ASC, SO.ORDER_DATE ASC, STA.STAFF_CODE ASC, CUS.CUSTOMER_CODE ASC, PRO.PRODUCT_CODE ASC ");

		String[] fieldNames = { //
		"promotionCode",// 0
				"promotionName",// 1
				"orderDate",// 2
				"staffCode",// 3
				"staffName",// 4
				"customerCode",// 5
				"customerName",// 6
				"productCode",// 7
				"productName",// 8
				"price",// 9
				"quantity",// 10
				"amount"//11
		};

		Type[] fieldTypes = { //
		StandardBasicTypes.STRING, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.DATE, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.STRING, // 7
				StandardBasicTypes.STRING, // 8
				StandardBasicTypes.FLOAT, // 9
				StandardBasicTypes.LONG, // 10
				StandardBasicTypes.BIG_DECIMAL, // 11
		};

		return repo.getListByQueryAndScalar(RptOrderByPromotionDataVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<RptProductExchangeVO> getListRptProductExchangeVO(KPaging<RptProductExchangeVO> kPaging, Date fromDate, Date toDate, Long shopId, String programCode, Long catId) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<RptProductExchangeVO>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" select s.shop_name shopName,");
		sql.append("       s.address address,");
		sql.append("       c.customer_code customerCode,");
		sql.append("       c.housenumber houseNumber,");
		sql.append("       c.street street,");
		sql.append("       a.district_name districtName,");
		sql.append("       p.product_code productCode,");
		sql.append("       so.order_number orderNumber,");
		sql.append("       so.order_date orderDate,");
		sql.append("       sod.amount amount,");
		sql.append("       sod.discount_amount discountAmount");

		fromSql.append(" from sale_order so join sale_order_detail sod on so.sale_ordeR_id = sod.sale_order_id");
		fromSql.append("     join product p on p.product_id = sod.product_id");
		fromSql.append("     join product_level pl on p.product_level_id = pl.product_level_id");
		fromSql.append("     join shop s on s.shop_id = so.shop_id");
		fromSql.append("     join customer c on c.customer_id = so.customer_id");
		fromSql.append("     join area a on a.area_id = c.area_id");
		fromSql.append(" where 1 = 1");
		if (fromDate != null) {
			fromSql.append("   and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			fromSql.append("   and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		if (shopId != null) {
			fromSql.append("   and so.shop_id = ?");
			params.add(shopId);
		}
		if (!StringUtility.isNullOrEmpty(programCode)) {
			fromSql.append("   and sod.program_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(programCode.toUpperCase()));
		}
		if (catId != null) {
			fromSql.append("   and pl.cat_id = ?");
			params.add(catId);
		}
		fromSql.append("   and sod.is_free_item = 1");
		fromSql.append("   and sod.program_type in (?,?,?)");

		params.add(ProgramType.PRODUCT_EXCHANGE.getValue());
		params.add(ProgramType.DESTROY.getValue());
		params.add(ProgramType.RETURN.getValue());

		fromSql.append("   and so.approved = ?");
		params.add(SaleOrderStatus.APPROVED.getValue());
		fromSql.append("   and so.order_type = ?");
		params.add(OrderType.IN.getValue());

		sql.append(fromSql);
		sql.append(" order by orderDate");
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count");
		countSql.append(fromSql);

		String[] fieldNames = { "shopName", // 0
				"address", // 1
				"customerCode", // 2
				"houseNumber", // 3
				"street", // 4
				"districtName", // 5
				"productCode", // 6
				"orderNumber", // 7
				"orderDate", // 8
				"amount", // 9
				"discountAmount", // 10
		};

		Type[] fieldTypes = { StandardBasicTypes.STRING, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.STRING, // 3
				StandardBasicTypes.STRING, // 4
				StandardBasicTypes.STRING, // 5
				StandardBasicTypes.STRING, // 6
				StandardBasicTypes.STRING, // 7
				StandardBasicTypes.DATE, // 8
				StandardBasicTypes.BIG_DECIMAL, // 9
				StandardBasicTypes.BIG_DECIMAL, // 10
		};

		if (kPaging == null)
			return repo.getListByQueryAndScalar(RptProductExchangeVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(RptProductExchangeVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}

	@Override
	public List<RptReturnSaleOrderByDeliveryDataVO> getListRptReturnSaleOrderByDelivery(Long shopId, OrderType orderType, Date fromDate, Date toDate, Long deliveryId) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate)) {
			return new ArrayList<RptReturnSaleOrderByDeliveryDataVO>();
		}
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		/*
		 * if (deliveryId == null) { throw new
		 * IllegalArgumentException("deliveryId is null"); }
		 */
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT so.order_date AS orderDate,");
		sql.append(" so.order_number    AS orderNumber,");
		sql.append(" so.total           AS total,");
		sql.append(" s.staff_code       AS deliveryCode,");
		sql.append(" s.staff_name       AS deliveryName,");
		sql.append(" s.staff_id         AS deliveryId,");
		sql.append(" c.customer_code    AS customerCode,");
		sql.append(" c.short_code       AS shortCode,");
		sql.append(" c.customer_name    AS customerName,");
		sql.append(" c.phone            AS phone,");
		sql.append(" c.mobiphone        AS mobiphone,");
		sql.append(" c.address    AS customerAddress");
		sql.append(" FROM sale_order so, staff s, customer c");
		sql.append(" WHERE so.delivery_id      = s.staff_id");
		sql.append(" AND so.customer_id        = c.customer_id");
		sql.append(" AND s.status 			   = 1 ");
		sql.append(" AND so.shop_id            = ?");
		params.add(shopId);

		if (null != deliveryId) {
			sql.append(" AND so.delivery_id        = ?");
			params.add(deliveryId);
		} else {
			sql.append(" AND exists (select 1 from staff st, channel_type ct where st.staff_type_id = ct.channel_type_id ");
			sql.append(" 	and st.staff_id = s.staff_id ");
			sql.append(" 	and st.shop_id = ? ");
			sql.append(" 	and ct.object_type = ? )");

			params.add(shopId);
			params.add(StaffObjectType.NVGH.getValue());
		}

		if (orderType != null) {
			sql.append(" AND so.order_type      = ?");
			params.add(orderType.getValue());
		}
		if (fromDate != null) {
			sql.append(" AND so.order_date >= TRUNC(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" AND so.order_date < TRUNC(?) + 1");
			params.add(toDate);
		}
		sql.append(" ORDER BY s.staff_code, so.order_number, so.order_date");
		String[] fieldNames = { "orderDate" //1
				, "orderNumber" //2
				, "total" //3
				, "deliveryCode" //4
				, "deliveryName" //5
				, "deliveryId" //6
				, "customerCode" //7
				, "shortCode" //8
				, "customerName" //9
				, "phone" //10
				, "mobiphone" //11
				, "customerAddress" //12
		};
		Type[] fieldTypes = { StandardBasicTypes.TIMESTAMP //1
				, StandardBasicTypes.STRING //2
				, StandardBasicTypes.BIG_DECIMAL //3
				, StandardBasicTypes.STRING //4 
				, StandardBasicTypes.STRING //5
				, StandardBasicTypes.LONG //6
				, StandardBasicTypes.STRING //7
				, StandardBasicTypes.STRING //8
				, StandardBasicTypes.STRING //9
				, StandardBasicTypes.STRING //10
				, StandardBasicTypes.STRING //11
				, StandardBasicTypes.STRING //12
		};
		return repo.getListByQueryAndScalar(RptReturnSaleOrderByDeliveryDataVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<SaleOrder> getListSaleOrder(List<Long> lstSaleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from sale_order where 1 != 1");
		for (Long saleOrderId : lstSaleOrderId) {
			sql.append(" or sale_order_id = ?");
			params.add(saleOrderId);
		}
		return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SaleOrderDAO#checkWithoutSaleOrderOfShop(java.lang
	 * .Long, java.util.List)
	 */
	@Override
	public Boolean checkWithoutSaleOrderOfShop(Long shopId, List<Long> listSaleOrderId) throws DataAccessException {

		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (listSaleOrderId == null || listSaleOrderId.size() < 1) {
			throw new IllegalArgumentException("listSaleOrderId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(*) as count from sale_order where shop_id <> ?");
		params.add(shopId);
		sql.append(" and sale_order_id in (");
		int length = listSaleOrderId.size();
		int i = 0;
		for (Long saleOrderId : listSaleOrderId) {
			i++;
			if (i == length) {
				sql.append(" ?");
			} else {
				sql.append(" ?,");
			}
			params.add(saleOrderId);
		}
		sql.append(")");
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public List<SaleOrderDetail> getListVansaleSaleOrderDetail(Long staffId, Long productId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select sod.* from sale_order so join sale_order_detail sod on so.sale_order_id = sod.sale_order_id ");
		sql.append("   where so.ORDER_TYPE = ? and so.staff_id = ? and sod.product_id = ?");
		params.add(OrderType.SO.getValue());
		params.add(staffId);
		params.add(productId);

		sql.append(" and exists (select 1 from product pr where pr.product_id = sod.product_id and pr.check_lot = 1)");
		sql.append(" and not exists (select 1 from sale_order_lot sol where sol.sale_order_detail_id = sod.sale_order_detail_id)");
		sql.append(" order by so.order_date");
		return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
	}

	@Override
	public Date checkSaleOrderInLast60days(Long customerId, Long staffId, Long saleOrderId, Date lockDay) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from (");
		sql.append(" select order_date from sale_order so");
		sql.append("	where approved in (0,1) and order_type = 'IN'");
		sql.append("		and order_date >= trunc(?) - 60 and sale_order_id != ?");
		params.add(lockDay);
		params.add(saleOrderId);
		if (customerId != null) {
			sql.append("		and customer_id = ?");
			params.add(customerId);
		}
		if (staffId != null) {
			sql.append(" and staff_id = ?");
			params.add(staffId);
		}
		sql.append(" order by order_date desc");
		sql.append(" ) where rownum = 1");
		return (Date) repo.getObjectByQuery(sql.toString(), params);
	}

	/***************************** MUA HANG -IN PHIEU GIAO HANG ****************************************/
	/**
	 * In phieu giao hang gop theo don hang chon
	 * 
	 * @author thongnm
	 */
	@Override
	public List<RptPGHVO> getPGHGroupByDH(List<Long> listSaleOrderId, String strListShopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with lst1 as ( ");
		sql.append(" SELECT ");
		sql.append(" SUM(NVL(so.amount, 0)) amount, ");
		sql.append(" SUM(NVL(so.total, 0)) total, ");
		sql.append(" SUM ( ");
		sql.append(" (SELECT SUM(NVL(discount_amount, 0)) ");
		sql.append(" FROM sale_order_detail sod ");
		sql.append(" WHERE sod.sale_order_id = so.sale_order_id ");
		sql.append(" ) ");
		sql.append(" )moneyDiscount ");
		sql.append(" FROM sale_order so ");
		sql.append(" WHERE so.approved = 1  ");
		sql.append(" AND so.type = 1 ");
		sql.append(" AND so.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		if (listSaleOrderId != null) {
			sql.append(" AND so.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		if (!StringUtility.isNullOrEmpty(strListShopId)) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(strListShopId, "so.shop_id");
			sql.append(paramsFix);
		}
		sql.append(" ), ");
		sql.append(" lst2 as ( ");
		sql.append(" SELECT LISTAGG(order_number, ', ') WITHIN GROUP (ORDER BY order_number) lstOrderNumber ");
		sql.append(" FROM sale_order so ");
		sql.append(" WHERE 1 =1 ");
		sql.append(" AND so.approved = 1  ");
		sql.append(" AND so.type = 1 ");
		sql.append(" AND so.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		if (listSaleOrderId != null) {
			sql.append(" AND so.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		if (!StringUtility.isNullOrEmpty(strListShopId)) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(strListShopId, "so.shop_id");
			sql.append(paramsFix);
		}
		sql.append(" AND rownum < 100 ");
		sql.append(" ) ");
		sql.append(" select * from lst1, lst2");

		String[] fieldNames = { "amount", "total", "moneyDiscount", "lstOrderNumber" };
		Type[] fieldTypes = { StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING };

		List<RptPGHVO> rpt = repo.getListByQueryAndScalar(RptPGHVO.class, fieldNames, fieldTypes, sql.toString(), params);

		for (RptPGHVO vo : rpt) {
			vo.setLstSaleProduct(this.getPGHGroupByDHProduct(listSaleOrderId, true));
			vo.setLstPromoProduct(this.getPGHGroupByDHProduct(listSaleOrderId, false));
			for (RptPGHDetailVO item : vo.getLstSaleProduct()) {
				vo.setSumThung(vo.getSumThung() + item.getThung());
				vo.setSumLe(vo.getSumLe() + item.getLe());
			}
		}
		return rpt;
	}

	private List<RptPGHDetailVO> getPGHGroupByDHProduct(List<Long> listSaleOrderId, Boolean isSaleProduct) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with result as (SELECT ");
		sql.append(" sod.product_id productId, ");
		sql.append(" NVL(sod.price, 0) price, ");
		sql.append(" NVL(sod.package_price, 0) packagePrice, ");
		sql.append(" sol.lot lot, ");
		sql.append(" case ");
		sql.append(" when sol.lot is null ");
		sql.append(" then SUM(NVL(sod.amount, 0)) ");
		sql.append(" else SUM(NVL(sol.quantity *sod.price, 0)) ");
		sql.append(" end as amount, ");
		sql.append(" case ");
		sql.append(" when sol.lot is null ");
		sql.append(" then SUM(NVL(sod.quantity, 0)) ");
		sql.append(" else SUM(NVL(sol.quantity, 0)) ");
		sql.append(" end as quantity ");
		sql.append(" FROM sale_order so JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id ");
		sql.append(" LEFT JOIN sale_order_lot sol ON sol.sale_order_detail_id = sod.sale_order_detail_id ");
		sql.append(" WHERE 1=1 ");
		if (Boolean.TRUE.equals(isSaleProduct)) {
			sql.append(" AND sod.is_free_item = 0");
		} else {
			sql.append(" AND sod.is_free_item = 1");
		}
		sql.append(" AND so.approved = 1 ");
		sql.append(" AND so.type = 1 ");
		sql.append(" AND so.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		if (listSaleOrderId != null) {
			sql.append(" AND so.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		sql.append(" GROUP BY sod.product_id,sod.price, sol.lot, sod.package_price ) ");
		sql.append(" select ");
		sql.append(" p.product_code productCode, ");
		sql.append(" p.product_name productName, ");
		sql.append(" p.convfact convfact, ");
		sql.append(" result.price price, ");
		sql.append(" result.packagePrice packagePrice, ");
		sql.append(" result.lot lot, ");
		sql.append(" result.amount amount, ");
		sql.append(" result.quantity quantity ");
		sql.append(" from result JOIN product p ON p.product_id = result.productId ");
		if (!Boolean.TRUE.equals(isSaleProduct)) {
			sql.append(" where result.quantity > 0");
		}
		sql.append(" order by productCode, price, packagePrice ");

		String[] fieldNames = { "productCode", "productName", "convfact", "lot", "price", "packagePrice", "amount", "quantity" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER };

		List<RptPGHLotVO> lstProductLot = repo.getListByQueryAndScalar(RptPGHLotVO.class, fieldNames, fieldTypes, sql.toString(), params);
		List<RptPGHDetailVO> rs = new ArrayList<RptPGHDetailVO>();
		String productCode = "";
		BigDecimal price = BigDecimal.ZERO;
		BigDecimal packagePrice = BigDecimal.ZERO;
		RptPGHDetailVO rptDetailVO = null;
		for (RptPGHLotVO rptLotVO : lstProductLot) {
			if (!rptLotVO.getProductCode().equals(productCode) || rptLotVO.getPrice().compareTo(price) != 0 || rptLotVO.getPackagePrice().compareTo(packagePrice) != 0) {
				productCode = rptLotVO.getProductCode();
				price = rptLotVO.getPrice();
				packagePrice = rptLotVO.getPackagePrice();
				rptDetailVO = new RptPGHDetailVO();
				rptDetailVO.setProductCode(rptLotVO.getProductCode());
				rptDetailVO.setProductName(rptLotVO.getProductName());
				rptDetailVO.setConvfact(rptLotVO.getConvfact());
				rptDetailVO.setPrice(rptLotVO.getPrice());
				rptDetailVO.setPackagePrice(rptLotVO.getPackagePrice());
				if (Boolean.FALSE.equals(isSaleProduct)) {
					rptDetailVO.setPromotionName(getPromotionCodeGroupByDH(listSaleOrderId, rptLotVO.getProductCode()));
				}
				rs.add(rptDetailVO);
			}
			rptDetailVO.getLstProductLot().add(rptLotVO);
			rptDetailVO.setQuantity(rptDetailVO.getQuantity() + rptLotVO.getQuantity());
			rptDetailVO.setAmount(rptDetailVO.getAmount().add(rptLotVO.getAmount()));
		}
		return rs;
	}

	private String getPromotionCodeGroupByDH(List<Long> listSaleOrderId, String productCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select nvl(listagg(program_code, ', ') within group (order by program_code), ' ') from ( ");
		sql.append("SELECT distinct(sod.program_code) as program_code");
		sql.append(" from sale_order_detail sod ");
		sql.append(" inner join sale_order so on sod.sale_order_id = so.sale_order_id ");
		sql.append(" inner join product p on sod.product_id = p.product_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and so.sale_order_id IN (-1");
		if (listSaleOrderId != null && listSaleOrderId.size() > 0) {
			for (int i = 0; i < listSaleOrderId.size(); i++) {
				sql.append(",?");
				params.add(listSaleOrderId.get(i));
			}
		}
		sql.append(")");
		sql.append(" and p.product_code = ? ");
		params.add(productCode);
		sql.append(")");
		return repo.getObjectByQuery(sql.toString(), params).toString();
	}
	
	/**
	 * lay thong tin dia chi cua KH bao gom quan, huyen, xa
	 * @author tuannd20
	 * @param customer
	 * @return dia chi cua KH voi thong tin quan, huyen
	 */
	private String getCustomerFullAddress(Customer customer) {
		StringBuilder customerFullAddress = new StringBuilder(customer.getAddress());
		try {
			Area area = customer.getArea();
			if (area != null && area.getType() == AreaType.WARD) {
				if (!StringUtility.isNullOrEmpty(customerFullAddress.toString())) {
					customerFullAddress.append(", ");					
				}
				customerFullAddress.append(area.getAreaName());
				area = area.getParentArea();
			}
			if (area != null && area.getType() == AreaType.DISTRICT) {
				if (!StringUtility.isNullOrEmpty(customerFullAddress.toString())) {
					customerFullAddress.append(", ");					
				}
				customerFullAddress.append(area.getAreaName());
				area = area.getParentArea();
			}
			/*if (area != null && area.getType() == AreaType.PROVINCE) {
				customerFullAddress.append(", ");
				customerFullAddress.append(area.getAreaName());
				area = area.getParentArea();
			}*/
		} catch (Exception e) {
			// pass through
		}
		return customerFullAddress.toString();
	}

	/**
	 * In phieu giao hang gop theo KH
	 * 
	 * @author thongnm
	 */
	@Override
	public List<RptPGHVO> getPGHGroupByKH(List<Long> listSaleOrderId, String strListShopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT ");
		sql.append(" so.customer_id customerId, ");
		sql.append(" (SELECT short_code|| ' - ' || CUSTOMER_NAME");
		sql.append(" FROM customer ");
		sql.append(" WHERE customer_id = so.customer_id ");
		sql.append(" ) customerInfo, ");
		sql.append(" SUM(NVL(so.amount, 0)) amount, ");
		sql.append(" SUM(NVL(so.total, 0)) total, ");
		sql.append(" (SELECT SUM(NVL(discount_amount, 0)) ");
		sql.append(" FROM sale_order_detail sod ");
		sql.append(" JOIN sale_order so1 ON sod.sale_order_id = so1.sale_order_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND so1.customer_id = so.customer_id ");
		sql.append(" AND so1.approved = 1 ");
		sql.append(" AND so1.type = 1 ");
		sql.append(" AND so1.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		if (listSaleOrderId != null) {
			sql.append(" AND so1.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		sql.append(" ) moneyDiscount, ");

		//listOrderNumber
		sql.append(" (SELECT LISTAGG(order_number, ', ') WITHIN GROUP (ORDER BY order_number) ");
		sql.append(" FROM sale_order so1 ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND so1.customer_id = so.customer_id ");
		sql.append(" AND so1.approved = 1 ");
		sql.append(" AND so1.type = 1 ");
		sql.append(" AND so1.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		if (listSaleOrderId != null) {
			sql.append(" AND so1.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		sql.append(" AND rownum < 100 ");
		sql.append(" ) lstOrderNumber ,");

		//listOrderDate
		sql.append(" (SELECT LISTAGG(to_char(ddd, 'dd/mm/yyyy'), ', ') WITHIN GROUP (ORDER BY ddd) ");
		sql.append(" FROM (select distinct trunc(order_date) as ddd, customer_id from sale_order so1 ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND so1.approved = 1 ");
		sql.append(" AND so1.type = 1 ");
		sql.append(" AND so1.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		if (listSaleOrderId != null) {
			sql.append(" AND so1.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		sql.append(" AND rownum < 100 )");
		sql.append(" where customer_id = so.customer_id ");
		sql.append(" group by customer_id ");
		sql.append(" ) lstOrderDate ,");

		//listDelivery
		sql.append(" (SELECT LISTAGG(to_char(ddd), ', ') WITHIN GROUP (ORDER BY ddd) ");
		sql.append(" FROM (SELECT DISTINCT st.staff_Name ||  ");
		sql.append("       	(case  ");
		sql.append("         when st.mobilephone is not null then ('(' || st.mobilephone || ')') ");
		sql.append("         when st.phone is not null then ('(' || st.phone || ')') ");
		sql.append("         else '' end) AS ddd, ");
		sql.append(" 		customer_id ");
		sql.append(" from sale_order so1 join staff st on so1.delivery_id = st.staff_id ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND so1.approved = 1 ");
		sql.append(" AND so1.type = 1 ");
		sql.append(" AND so1.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		if (listSaleOrderId != null) {
			sql.append(" AND so1.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		sql.append(" AND rownum < 100 )");
		sql.append(" where customer_id = so.customer_id ");
		sql.append(" group by customer_id ");
		sql.append(" ) lstDelivery ");

		sql.append(" FROM sale_order so ");
		sql.append(" WHERE so.approved = 1 ");
		sql.append(" AND so.type = 1 ");
		sql.append(" AND so.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		if (listSaleOrderId != null) {
			sql.append(" AND so.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		//sql.append(" AND so.time_print is null");
		if (!StringUtility.isNullOrEmpty(strListShopId)) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(strListShopId, "so.shop_id");
			sql.append(paramsFix);
		}
		sql.append(" GROUP BY so.customer_id ");

		String[] fieldNames = { "customerId", "customerInfo", "amount", "total", "moneyDiscount", "lstOrderNumber", "lstOrderDate", "lstDelivery" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING };

		List<RptPGHVO> rpt = repo.getListByQueryAndScalar(RptPGHVO.class, fieldNames, fieldTypes, sql.toString(), params);

		for (RptPGHVO vo : rpt) {
			vo.setCustomer(customerDAO.getCustomerById(vo.getCustomerId()));
			vo.setLstSaleProduct(this.getPGHGroupByKHProduct(listSaleOrderId, vo.getCustomerId(), true));
			vo.setLstPromoProduct(this.getPGHGroupByKHProduct(listSaleOrderId, vo.getCustomerId(), false));
			for (RptPGHDetailVO item : vo.getLstSaleProduct()) {
				vo.setSumThung(vo.getSumThung() + item.getThung());
				vo.setSumLe(vo.getSumLe() + item.getLe());
			}
		}
		return rpt;
	}

	private List<RptPGHDetailVO> getPGHGroupByKHProduct(List<Long> listSaleOrderId, Long customerId, Boolean isSaleProduct) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with result as (SELECT ");
		sql.append(" sod.product_id productId, ");
		sql.append(" NVL(sod.price, 0) price, ");
		sql.append(" NVL(sod.PACKAGE_PRICE, 0) packagePrice,");
		sql.append(" sol.lot lot, ");
		sql.append(" case ");
		sql.append(" when sol.lot is null ");
		sql.append(" then SUM(NVL(sod.amount, 0)) ");
		sql.append(" else SUM(NVL(sol.quantity *sod.price, 0)) ");
		sql.append(" end as amount, ");
		sql.append(" case ");
		sql.append(" when sol.lot is null ");
		sql.append(" then SUM(NVL(sod.quantity, 0)) ");
		sql.append(" else SUM(NVL(sol.quantity, 0)) ");
		sql.append(" end as quantity ");
		sql.append(" FROM sale_order so JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id ");
		sql.append(" LEFT JOIN sale_order_lot sol ON sol.sale_order_detail_id = sod.sale_order_detail_id ");
		sql.append(" WHERE 1=1 ");
		if (Boolean.TRUE.equals(isSaleProduct)) {
			sql.append(" AND sod.is_free_item = 0");
		} else {
			sql.append(" AND sod.is_free_item = 1");
		}
		sql.append(" AND so.approved = 1 ");
		sql.append(" AND so.type = 1 ");
		sql.append(" AND so.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		sql.append(" AND so.customer_id = ? ");
		params.add(customerId);
		if (listSaleOrderId != null) {
			sql.append(" AND so.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		//sql.append(" AND so.time_print is null");
		sql.append(" GROUP BY sod.product_id,sod.price, sol.lot, sod.PACKAGE_PRICE ) ");
		sql.append(" select ");
		sql.append(" p.product_code productCode, ");
		sql.append(" p.product_name productName, ");
		sql.append(" p.convfact convfact, ");
		sql.append(" result.price price, ");
		sql.append(" result.packagePrice packagePrice,");
		sql.append(" result.lot lot, ");
		sql.append(" result.amount amount, ");
		sql.append(" result.quantity quantity ");
		sql.append(" from result JOIN product p ON p.product_id = result.productId ");
		if (!Boolean.TRUE.equals(isSaleProduct)) {
			sql.append(" where result.quantity > 0");
		}
		sql.append(" order by productCode, price, packagePrice ");

		String[] fieldNames = { "productCode", "productName", "convfact", "lot", "price", "packagePrice", "amount", "quantity" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER };

		List<RptPGHLotVO> lstProductLot = repo.getListByQueryAndScalar(RptPGHLotVO.class, fieldNames, fieldTypes, sql.toString(), params);

		List<RptPGHDetailVO> rs = new ArrayList<RptPGHDetailVO>();
		String productCode = "";
		BigDecimal price = BigDecimal.ZERO;
		BigDecimal packagePrice = BigDecimal.ZERO;
		RptPGHDetailVO rptDetailVO = null;
		for (RptPGHLotVO rptLotVO : lstProductLot) {
			if (!rptLotVO.getProductCode().equals(productCode) || rptLotVO.getPrice().compareTo(price) != 0 || rptLotVO.getPackagePrice().compareTo(packagePrice) != 0) {
				productCode = rptLotVO.getProductCode();
				price = rptLotVO.getPrice();
				packagePrice = rptLotVO.getPackagePrice();
				rptDetailVO = new RptPGHDetailVO();
				rptDetailVO.setProductCode(rptLotVO.getProductCode());
				rptDetailVO.setProductName(rptLotVO.getProductName());
				rptDetailVO.setConvfact(rptLotVO.getConvfact());
				rptDetailVO.setPrice(rptLotVO.getPrice());
				rptDetailVO.setPackagePrice(rptLotVO.getPackagePrice());
				if (Boolean.FALSE.equals(isSaleProduct)) {
					rptDetailVO.setPromotionName(getPromotionCodeGroupByKH(listSaleOrderId, customerId, rptLotVO.getProductCode()));
				}
				rs.add(rptDetailVO);
			}
			rptDetailVO.getLstProductLot().add(rptLotVO);
			rptDetailVO.setQuantity(rptDetailVO.getQuantity() + rptLotVO.getQuantity());
			rptDetailVO.setAmount(rptDetailVO.getAmount().add(rptLotVO.getAmount()));
		}
		return rs;
	}

	private String getPromotionCodeGroupByKH(List<Long> listSaleOrderId, Long customerId, String productCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select nvl(listagg(program_code, ', ') within group (order by program_code), ' ') from ( ");
		sql.append("select distinct(sod.program_code) as program_code");
		sql.append(" from sale_order_detail sod ");
		sql.append(" inner join sale_order so on sod.sale_order_id = so.sale_order_id ");
		sql.append(" inner join product p on sod.product_id = p.product_id ");
		sql.append(" inner join customer c on so.customer_id = c.customer_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and so.sale_order_id IN (-1");
		if (listSaleOrderId != null && listSaleOrderId.size() > 0) {
			for (int i = 0; i < listSaleOrderId.size(); i++) {
				sql.append(",?");
				params.add(listSaleOrderId.get(i));
			}
		}
		sql.append(")");
		sql.append(" and so.customer_id = ?");
		params.add(customerId);
		sql.append(" and p.product_code = ? ");
		params.add(productCode);
		sql.append(")");
		return repo.getObjectByQuery(sql.toString(), params).toString();
	}

	private List<RptPGHDetailVO> getPGHGroupByNGVHProduct(List<Long> listSaleOrderId, Long staffId, Boolean isSaleProduct) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with result as (SELECT ");
		sql.append(" sod.product_id productId, ");
		sql.append(" NVL(sod.price, 0) price, ");
		sql.append(" NVL(sod.package_price, 0) packagePrice, ");
		sql.append(" sol.lot lot, ");
		sql.append(" case ");
		sql.append(" when sol.lot is null ");
		sql.append(" then SUM(NVL(sod.amount, 0)) ");
		sql.append(" else SUM(NVL(sol.quantity *sod.price, 0)) ");
		sql.append(" end as amount, ");
		sql.append(" case ");
		sql.append(" when sol.lot is null ");
		sql.append(" then SUM(NVL(sod.quantity, 0)) ");
		sql.append(" else SUM(NVL(sol.quantity, 0)) ");
		sql.append(" end as quantity ");
		sql.append(" FROM sale_order so JOIN sale_order_detail sod ON so.sale_order_id = sod.sale_order_id ");
		sql.append(" LEFT JOIN sale_order_lot sol ON sol.sale_order_detail_id = sod.sale_order_detail_id ");
		sql.append(" WHERE 1=1 ");
		if (Boolean.TRUE.equals(isSaleProduct)) {
			sql.append(" AND sod.is_free_item = 0");
		} else {
			sql.append(" AND sod.is_free_item = 1");
		}
		sql.append(" AND so.approved = 1 ");
		sql.append(" AND so.type = 1 ");
		sql.append(" AND so.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		sql.append(" AND so.delivery_id = ? ");
		params.add(staffId);
		if (listSaleOrderId != null) {
			sql.append(" AND so.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		//sql.append(" AND so.time_print is null");
		sql.append(" GROUP BY sod.product_id,sod.price, sol.lot, sod.package_price ) ");
		sql.append(" select ");
		sql.append(" p.product_code productCode, ");
		sql.append(" p.product_name productName, ");
		sql.append(" p.convfact convfact, ");
		sql.append(" result.price price, ");
		sql.append(" result.packagePrice packagePrice, ");
		sql.append(" result.lot lot, ");
		sql.append(" result.amount amount, ");
		sql.append(" result.quantity quantity ");
		sql.append(" from result JOIN product p ON p.product_id = result.productId ");
		if (!Boolean.TRUE.equals(isSaleProduct)) {
			sql.append(" where result.quantity > 0");
		}
		sql.append(" order by productCode, price, packagePrice");

		String[] fieldNames = { "productCode", "productName", "convfact", "lot", "price", "packagePrice","amount", "quantity" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER };

		List<RptPGHLotVO> lstProductLot = repo.getListByQueryAndScalar(RptPGHLotVO.class, fieldNames, fieldTypes, sql.toString(), params);

		List<RptPGHDetailVO> rs = new ArrayList<RptPGHDetailVO>();
		String productCode = "";
		BigDecimal price = BigDecimal.ZERO;
		BigDecimal packagePrice = BigDecimal.ZERO;
		RptPGHDetailVO rptDetailVO = null;
		for (RptPGHLotVO rptLotVO : lstProductLot) {
			if (!rptLotVO.getProductCode().equals(productCode) || rptLotVO.getPrice().compareTo(price) != 0 || rptLotVO.getPackagePrice().compareTo(packagePrice) != 0) {
				productCode = rptLotVO.getProductCode();
				price = rptLotVO.getPrice();
				packagePrice = rptLotVO.getPackagePrice();
				rptDetailVO = new RptPGHDetailVO();
				rptDetailVO.setProductCode(rptLotVO.getProductCode());
				rptDetailVO.setProductName(rptLotVO.getProductName());
				rptDetailVO.setConvfact(rptLotVO.getConvfact());
				rptDetailVO.setPrice(rptLotVO.getPrice());
				rptDetailVO.setPackagePrice(rptLotVO.getPackagePrice());
				if (Boolean.FALSE.equals(isSaleProduct)) {
					rptDetailVO.setPromotionName(getPromotionCodeGroupByNGVH(listSaleOrderId, staffId, rptLotVO.getProductCode()));
				}
				rs.add(rptDetailVO);
			}
			rptDetailVO.getLstProductLot().add(rptLotVO);
			rptDetailVO.setQuantity(rptDetailVO.getQuantity() + rptLotVO.getQuantity());
			rptDetailVO.setAmount(rptDetailVO.getAmount().add(rptLotVO.getAmount()));
		}
		return rs;
	}

	private String getPromotionCodeGroupByNGVH(List<Long> listSaleOrderId, Long staffId, String productCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select nvl(listagg(program_code, ', ') within group (order by program_code), ' ') from ( ");
		sql.append("select distinct(sod.program_code) as program_code");
		sql.append(" from sale_order_detail sod ");
		sql.append(" inner join sale_order so on sod.sale_order_id = so.sale_order_id ");
		sql.append(" inner join product p on sod.product_id = p.product_id ");
		sql.append(" inner join customer c on so.customer_id = c.customer_id ");
		sql.append(" where 1 = 1 ");
		sql.append(" and so.sale_order_id IN (-1");
		if (listSaleOrderId != null && listSaleOrderId.size() > 0) {
			for (int i = 0; i < listSaleOrderId.size(); i++) {
				sql.append(",?");
				params.add(listSaleOrderId.get(i));
			}
		}
		sql.append(")");
		sql.append(" and so.delivery_id = ? ");
		params.add(staffId);
		sql.append(" and p.product_code = ? ");
		params.add(productCode);
		sql.append(")");
		return repo.getObjectByQuery(sql.toString(), params).toString();
	}
	
	/**
	 * @author trietptm
	 */
	@Override
	public List<ProgramCodeVO> getListProgramCodeVO(SaleOrderFilter<SaleOrder> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT saleOrderId, saleOrderDetailId, productId, NVL(listagg(program_code, ', ') within GROUP (ORDER BY program_code), ' ') strProgramCode ");
		sql.append(" FROM ( ");
		sql.append(" SELECT DISTINCT(spm.program_code) AS program_code, sod.sale_order_id saleOrderId, sod.sale_order_detail_id saleOrderDetailId, sod.product_id productId ");
		sql.append("  FROM SALE_PROMO_MAP spm  ");
		sql.append("  INNER JOIN sale_order_detail sod on spm.sale_order_detail_id = sod.sale_order_detail_id ");
		sql.append(" where 1 = 1 ");
		if (filter.getSaleOrderId() != null) {
			sql.append(" and sod.sale_order_id = ? ");
			params.add(filter.getSaleOrderId());
		}
		if (filter.getListSaleOrderId() != null && filter.getListSaleOrderId().size() > 0) {
			sql.append(" and sod.sale_order_id IN (-1");
			for (int i = 0; i < filter.getListSaleOrderId().size(); i++) {
				sql.append(", ? ");
				params.add(filter.getListSaleOrderId().get(i));
			}
			sql.append(" )");
		}
		sql.append(" ) ");
		sql.append(" group by saleOrderId, saleOrderDetailId, productId ");

		String[] fieldNames = { "saleOrderId", "saleOrderDetailId", "productId", "strProgramCode" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING };
		List<ProgramCodeVO> rs = repo.getListByQueryAndScalar(ProgramCodeVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return rs;
	}

	@Override
	public List<RptPGNVTTGVO> getPGNVTTG(Long shopId, Long deliveryStaffId, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select ((select staff_name || ' - ' from staff where staff_id = so.delivery_id) ");
		sql.append("			|| (select LISTAGG(car_number, ', ') WITHIN GROUP (ORDER BY car_number) from car c join sale_order so1 on c.car_id = so1.car_id");
		sql.append("			where so1.delivery_id = so.delivery_id and so1.customer_id = so.customer_id and so1.approved = 1 and so1.type = 1 and so1.order_type in ('IN', 'TT') and rownum = 1 ");
		if (fromDate != null) {
			sql.append(" 	and so1.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("	and so1.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append("	 )) deliveryStaffInfo,");
		//		sql.append(" 		(select staff_name || '-' || staff_code from staff where staff_id = so.staff_id) staffInfo,");
		sql.append(" 		(select short_code || ' - ' || CUSTOMER_NAME || ' - ' || 'SDT:' || phone from customer where customer_id = so.customer_id) customerInfo,");
		sql.append(" 		(select address from customer where customer_id = so.customer_id) deliveryAddress,");
		sql.append("		sum(nvl(so.amount, 0)) amount,");
		sql.append("		sum(nvl(so.total, 0)) total,");
		sql.append("		(select sum(nvl(discount_amount, 0)) ");
		sql.append("			from sale_order_detail sod join sale_order so1 on sod.sale_order_id = so1.sale_order_id");
		sql.append(" 			where so1.delivery_id = so.delivery_id and so1.customer_id = so.customer_id  and so1.approved = 1 and so1.type = 1 and so1.order_type in ('IN', 'TT')");
		if (fromDate != null) {
			sql.append(" 	and so1.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("	and so1.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append(" ) moneyDiscount, ");

		sql.append("		(select LISTAGG(order_number, ', ') WITHIN GROUP (ORDER BY order_number) ");
		sql.append("			from sale_order so1");
		sql.append(" 			where so1.delivery_id = so.delivery_id and so1.customer_id = so.customer_id and so1.approved = 1 and so1.type = 1 and so1.order_type in ('IN', 'TT')");
		if (fromDate != null) {
			sql.append(" 	and so1.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("	and so1.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append("  and rownum < 100) lstOrderNumber,");

		sql.append("	so.delivery_id deliveryId, so.customer_id customerId");

		sql.append(" from sale_order so join staff st on so.delivery_id = st.staff_id");
		sql.append(" where so.approved = 1 and so.type = 1 and so.order_type in ('IN', 'TT')");
		sql.append("	and so.shop_id = ?");
		params.add(shopId);
		if (deliveryStaffId != null) {
			sql.append("	and so.delivery_id = ?");
			params.add(deliveryStaffId);
		}
		if (fromDate != null) {
			sql.append(" 	and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("	and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}

		sql.append(" group by so.delivery_id, st.staff_code, so.customer_id");
		sql.append(" order by st.staff_code");

		String[] fieldNames = { "deliveryStaffInfo", "customerInfo", "deliveryAddress", "amount", "total", "moneyDiscount", "lstOrderNumber", "deliveryId", "customerId" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING,
				StandardBasicTypes.LONG, StandardBasicTypes.LONG };

		/* System.out.println(TestUtil.addParamToQuery(sql.toString(), params)); */
		System.out.println(sql.toString());
		List<RptPGNVTTGVO> rpt = repo.getListByQueryAndScalar(RptPGNVTTGVO.class, fieldNames, fieldTypes, sql.toString(), params);

		for (RptPGNVTTGVO vo : rpt) {
			vo.setLstSaleProduct(this.getRptPGNVTTGSaleProduct(vo.getDeliveryId(), vo.getCustomerId(), fromDate, toDate));
			vo.setLstPromoProduct(this.getRptPGNVTTGPromoProduct(vo.getDeliveryId(), vo.getCustomerId(), fromDate, toDate));
		}
		return rpt;
	}

	private List<RptPGNVTTGDetailVO> getRptPGNVTTGSaleProduct(Long deliveryId, Long customerId, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select pr.product_code productCode,");
		sql.append(" 	    pr.product_name productName,");
		sql.append(" 	    pr.convfact convfact,");
		sql.append("		nvl(sod.price, 0) price,");
		sql.append("		sum(nvl(sod.amount, 0)) amount,");
		sql.append("		sum(nvl(sod.quantity, 0)) quantity");
		sql.append(" 	from sale_order so join sale_order_detail sod on so.sale_order_id = sod.sale_order_id");
		sql.append("		join product pr on pr.product_id = sod.product_id");
		sql.append("	where sod.is_free_item = 0 and so.delivery_id = ? and so.approved = 1 and so.type = 1");
		params.add(deliveryId);
		sql.append("		and so.customer_id = ?");
		params.add(customerId);
		if (fromDate != null) {
			sql.append(" 	and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("	and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append(" group by pr.product_code, pr.product_name, pr.convfact, sod.price");
		sql.append(" order by pr.product_code, sod.price");

		String[] fieldNames = { "productCode", "productName", "convfact", "price", "amount", "quantity" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER };

		//		System.out.println(TestUtil.addParamToQuery(sql.toString(), params));
		List<RptPGNVTTGDetailVO> rs = repo.getListByQueryAndScalar(RptPGNVTTGDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return rs;
	}

	private List<RptPGNVTTGDetailVO> getRptPGNVTTGPromoProduct(Long deliveryId, Long customerId, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select pr.product_code productCode,");
		sql.append(" 	    pr.product_name productName,");
		sql.append(" 	    pr.convfact convfact,");
		sql.append("		sum(nvl(sod.quantity, 0)) quantity,");
		sql.append("		case when sod.program_type=2");
		sql.append("		then (select display_program_code || '-' || display_program_name from  display_program where display_program_code = sod.program_code)");
		sql.append("		else");
		sql.append("		(select promotion_program_code || '-' ");
		sql.append("			|| promotion_program_name from promotion_program ");
		sql.append("			where promotion_program_code = sod.program_code ");
		sql.append("				and status = 1) end programInfo");
		sql.append(" 	from sale_order so join sale_order_detail sod on so.sale_order_id = sod.sale_order_id");
		sql.append("		join product pr on pr.product_id = sod.product_id");
		sql.append("	where sod.is_free_item = 1 and sod.program_type not in (3,4,5) and so.approved = 1 and so.type = 1");
		sql.append("		and sod.quantity > 0 and so.delivery_id = ?");
		params.add(deliveryId);
		sql.append("		and so.customer_id = ?");
		params.add(customerId);
		if (fromDate != null) {
			sql.append(" 	and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("	and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append(" group by pr.product_code, pr.product_name, pr.convfact, sod.program_code, sod.program_type");
		sql.append(" order by pr.product_code, sod.program_code");

		String[] fieldNames = { "productCode", "productName", "convfact", "quantity", "programInfo" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING };

		/* System.out.println(TestUtil.addParamToQuery(sql.toString(), params)); */
		List<RptPGNVTTGDetailVO> rs = repo.getListByQueryAndScalar(RptPGNVTTGDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return rs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.SaleOrderDAO#getListSaleOrderNumberVOForDelivery
	 * (java.lang.Long, java.lang.String, java.util.List, java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public List<SaleOrderNumberVO> getListSaleOrderNumberVOForDelivery(Long shopId, String deliveryCode, List<OrderType> listOrderType, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select distinct(so.order_number) as orderNumber from sale_order so, staff s where 1 = 1");
		sql.append(" and so.shop_id = ?");
		params.add(shopId);
		sql.append(" and so.delivery_id = s.staff_id and s.staff_code = ?");
		params.add(deliveryCode);
		if (listOrderType != null) {
			sql.append(" and (");
			int i = 0;
			for (OrderType orderType : listOrderType) {
				if (i == 0) {
					sql.append(" so.order_type = ?");
				} else {
					sql.append(" or so.order_type = ?");
				}
				params.add(orderType.getValue());
				i++;
			}
			sql.append(")");
		}
		if (fromDate != null) {
			sql.append(" and so.order_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and so.order_date < trunc(?) + 1");
			params.add(toDate);
		}
		String[] fieldNames = { "orderNumber" };
		Type[] fieldTypes = { StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(SaleOrderNumberVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public Boolean checkOrderInDelay(Long staffId, int delayTime) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) count from sale_order where Order_type='SO'");
		sql.append(" and staff_id=?");
		params.add(staffId);
		sql.append(" and order_source=2");
		sql.append(" and  order_date > sysdate - ?/24");
		params.add(delayTime);

		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	
	@Override
	public Boolean checkProductLotInSO(Long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) count from sale_order_detail sod where sod.sale_order_id = ? ");
		params.add(saleOrderId);
		sql.append(" and exists (select 1 from product where check_lot = 1 and product_id = sod.product_id)");
		sql.append(" and not exists (select 1 from sale_order_lot where sale_order_detail_id = sod.sale_order_detail_id)");

		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? false : true;
	}

	@Override
	public List<SaleOrderLot> getListSaleOrderLotBySaleOrderId(Long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from sale_order_lot where sale_order_id = ?");
		params.add(saleOrderId);
		return repo.getListBySQL(SaleOrderLot.class, sql.toString(), params);
	}

	/**
	 * Bao cao doanh thu tong hop cua nhan vien
	 * 
	 * @author tungtt
	 */
	@Override
	public List<RptDTTHNVRecordVO> getDTTHNVReport(Long shopId, List<Long> listStaffId, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select ");
		sql.append("s.staff_code AS staffCode, ");
		sql.append("s.staff_name AS staffName, ");
		sql.append("TO_CHAR(so.order_date, 'dd/mm/yyyy') AS orderDate, ");
		sql.append("sum(so.amount) AS revenueForDay, ");
		sql.append("0 AS moneyForDay, ");
		sql.append("sum(so.amount) - 0 AS remainForDay, ");
		sql.append("0 AS discountForDay, ");
		sql.append("sum(so.discount) AS promotionMoneyForDay, ");
		sql.append("0 AS promotionStockForDay, ");
		sql.append("sum(so.amount) - 0 - sum(so.discount) AS salesForDay ");

		sql.append("from sale_order so ");
		sql.append("inner join staff s on so.staff_id = s.staff_id ");
		sql.append("inner join sale_order_detail sod on so.sale_order_id = sod.sale_order_id ");
		sql.append("where ");
		sql.append("approved = 1 and type = 1 and order_type = 'IN' ");
		sql.append("and s.shop_id = ? ");
		params.add(shopId);

		if (toDate != null) {
			sql.append(" and so.order_date < trunc(?) + 1 ");
			params.add(toDate);
		}

		if (fromDate != null) {
			sql.append(" and so.order_date >= trunc(?) ");
			params.add(fromDate);
		}

		if (listStaffId != null && listStaffId.size() > 0) {
			sql.append(" and (");
			for (int i = 0; i < listStaffId.size(); i++) {
				if (i == 0) {
					sql.append(" so.staff_id = ? ");
				} else {
					sql.append(" or so.staff_id = ? ");
				}
				params.add(listStaffId.get(i));
			}
			sql.append(") ");
		}

		sql.append("group by s.staff_id, s.staff_code, s.staff_name, ");
		sql.append("TO_CHAR(so.order_date, 'dd/mm/yyyy') ");
		sql.append("order by s.staff_id, TO_CHAR(so.order_date, 'dd/mm/yyyy')");

		String[] fieldNames = { "staffCode", "staffName", "orderDate", "moneyForDay", "remainForDay", "discountForDay", "promotionMoneyForDay", "promotionStockForDay", "revenueForDay", "salesForDay" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL };

		List<RptDTTHNVRecordVO> rs = repo.getListByQueryAndScalar(RptDTTHNVRecordVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return rs;
	}

	@Override
	public List<RptBHTSP_DSNhanVienBH> getBHTSP_DSNhanVienVO(String shopCode, String dt_tu_ngay, String dt_den_ngay, String nhan_vien_ban_hang) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>(0);

		sql.append(" SELECT  so.shop_id as shop_id, stff.STAFF_ID as staff_id, 'NVBH:' || stff.STAFF_CODE || ' - ' || stff.staff_name as staff_name, 'NgÃ nh-' || pri.PRODUCT_INFO_CODE as product_info_code, prd.CAT_ID as cat_id,prd.PRODUCT_CODE as product_code, prd.PRODUCT_NAME as product_name, sod.QUANTITY as quantity, pr.PRICE as price ");
		sql.append("   FROM SALE_ORDER so,SALE_ORDER_DETAIL sod,STAFF stff,PRODUCT prd, PRICE pr, PRODUCT_INFO pri");

		sql.append(" WHERE stff.shop_id = ? ");
		params.add((long) (Long.parseLong(shopCode)));

		sql.append(" AND so.SHOP_ID = stff.SHOP_ID ");
		sql.append(" AND so.SALE_ORDER_ID = sod.SALE_ORDER_ID ");
		sql.append(" AND stff.STAFF_ID = so.STAFF_ID  ");

		sql.append(" AND sod.PRODUCT_ID = prd.PRODUCT_ID ");
		sql.append(" AND sod.IS_FREE_ITEM = 0 ");
		sql.append(" AND pr.PRICE_ID = sod.PRICE_ID ");
		sql.append(" AND pr.STATUS = 1 ");

		sql.append(" AND trunc(so.ORDER_DATE) >= trunc(to_date(?, 'dd/mm/yyyy')) ");
		sql.append(" AND trunc(so.ORDER_DATE) <= trunc(to_date(?, 'dd/mm/yyyy')) ");

		params.add(dt_tu_ngay);
		params.add(dt_den_ngay);

		sql.append(" AND  so.APPROVED = '1' ");
		sql.append(" AND  so.TYPE = '1'  ");

		//sql.append(" AND so.ORDER_TYPE = 'IN' ");
		sql.append(" AND pri.PRODUCT_INFO_ID = prD.CAT_ID ");

		if (nhan_vien_ban_hang != null && !nhan_vien_ban_hang.trim().equals("")) {
			/*
			 * String test =
			 * StringUtility.toOracleSearchLikeSuffix(nhan_vien_ban_hang);
			 * sql.append(" AND stff.STAFF_CODE like ?   ");
			 * params.add(StringUtility
			 * .toOracleSearchLikeSuffix(nhan_vien_ban_hang));
			 */
			String[] sSplits = nhan_vien_ban_hang.split(",");
			String sFillParamater = "";
			int len = sSplits.length;

			sFillParamater += "?";
			for (int i = 1; i < len; i++) {
				sFillParamater += ",?";
				params.add(sSplits[i]);
			}

			sql.append(" AND stff.STAFF_CODE in (" + sFillParamater + ") ");

			//params.add("0000010908");
			//params.add(StringUtility.to//(nhan_vien_ban_hang));
		}

		sql.append(" ORDER BY   stff.STAFF_CODE,   pri.PRODUCT_INFO_CODE, prd.PRODUCT_CODE ");

		String[] fieldNames = { "shop_id", "staff_id", "staff_name", "product_info_code", "cat_id", "product_code", "product_name", "quantity", "price" };
		Type[] fieldTypes = { StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,
				StandardBasicTypes.FLOAT };

		System.out.println(sql.toString());

		List<RptBHTSP_ThongTinTVVO> rs = repo.getListByQueryAndScalar(RptBHTSP_ThongTinTVVO.class, fieldNames, fieldTypes, sql.toString(), params);

		List<RptBHTSP_DSNhanVienBH> dsNhanVien = new ArrayList<RptBHTSP_DSNhanVienBH>();

		List<String> lstCheck = new ArrayList<String>();
		int iCountEntity = rs.size();
		for (int i = 0; i < iCountEntity; i++) {
			if (lstCheck.contains(String.valueOf(i)))
				continue;

			lstCheck.add(String.valueOf(i));

			List<RptBHTSP_ThongTinTVVO> groupItem = new ArrayList<RptBHTSP_ThongTinTVVO>();

			RptBHTSP_ThongTinTVVO rsItem = rs.get(i);
			groupItem.add(rsItem);

			if (i == iCountEntity - 1) {

				RptBHTSP_DSNhanVienBH item_dsNhanVien = new RptBHTSP_DSNhanVienBH(groupItem);
				dsNhanVien.add(item_dsNhanVien);
				break;
			}

			int id_staff = rsItem.getStaff_id();
			for (int j = i + 1; j < iCountEntity; j++) {
				if (lstCheck.contains(String.valueOf(j)))
					continue;

				RptBHTSP_ThongTinTVVO rsItem2 = rs.get(j);
				int id_staff2 = rsItem2.getStaff_id();
				if (id_staff == id_staff2) {
					lstCheck.add(String.valueOf(j));
					groupItem.add(rsItem2);

					if (j == iCountEntity - 1) {
						RptBHTSP_DSNhanVienBH item_dsNhanVien = new RptBHTSP_DSNhanVienBH(groupItem);
						dsNhanVien.add(item_dsNhanVien);
					}
				} else {

					RptBHTSP_DSNhanVienBH item_dsNhanVien = new RptBHTSP_DSNhanVienBH(groupItem);
					dsNhanVien.add(item_dsNhanVien);

					//i = j;
					break;
				}

			}
		}

		return dsNhanVien;
	}

	@Override
	public Integer getOrderDebitMaxCustomer(SaleOrder saleOrder, Date lockDay) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>(0);
		sql.append(" select ");
		sql.append(" nvl(trunc (?) - trunc((select min(s.order_date) from debit d, debit_detail dd, sale_order s ");
		params.add(lockDay);
		sql.append(" where d.debit_id = dd.debit_id ");
		sql.append(" and dd.from_object_id = s.sale_order_id ");
		sql.append(" and d.object_id = ? and d.object_type= ? ");
		params.add(saleOrder.getCustomer().getId());
		params.add(DebitOwnerType.CUSTOMER.getValue());
		sql.append(" and dd.remain > 0 ");
		sql.append(" and s.customer_id = ? )),0) as count from dual ");
		params.add(saleOrder.getCustomer().getId());
		return repo.countBySQL(sql.toString(), params);
	}

	@Override
	public List<RptPGHVO> getPGHGroupByNVGH(List<Long> listSaleOrderId, String strListShopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT so.DELIVERY_ID deliveryId, ");
		sql.append(" (SELECT staff_name || ");
		sql.append("       	(case  ");
		sql.append("         when mobilephone is not null then ('(' || mobilephone || ')') ");
		sql.append("         when phone is not null then ('(' || phone || ')') ");
		sql.append("         else '' end) ");
		sql.append(" FROM staff ");
		sql.append(" WHERE staff_id = so.DELIVERY_ID ");
		sql.append(" ) lstDelivery, ");
		sql.append(" SUM(NVL(so.amount, 0)) amount, ");
		sql.append(" SUM(NVL(so.total, 0)) total, ");
		sql.append(" (SELECT SUM(NVL(discount_amount, 0)) ");
		sql.append(" FROM sale_order_detail sod ");
		sql.append(" JOIN sale_order so1 ");
		sql.append(" ON sod.sale_order_id = so1.sale_order_id ");
		sql.append(" WHERE 1 = 1 ");
		sql.append(" AND so1.DELIVERY_ID = so.DELIVERY_ID ");
		sql.append(" AND so1.approved = 1 ");
		sql.append(" AND so1.type = 1 ");
		sql.append(" AND so1.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		if (listSaleOrderId != null) {
			sql.append(" AND so1.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}

		//sql.append(" AND so1.time_print IS NULL ");
		sql.append(" ) moneyDiscount, ");
		sql.append(" (SELECT LISTAGG(order_number, ', ') WITHIN GROUP ( ");
		sql.append(" ORDER BY order_number) ");
		sql.append(" FROM sale_order so1 ");
		sql.append(" WHERE 1 =1 ");
		sql.append(" AND so1.DELIVERY_ID = so.DELIVERY_ID ");
		sql.append(" AND so1.approved = 1 ");
		sql.append(" AND so1.type = 1 ");
		sql.append(" AND so1.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		if (listSaleOrderId != null) {
			sql.append(" AND so1.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		//sql.append(" AND so1.time_print IS NULL ");
		sql.append(" AND rownum < 100 ");
		sql.append(" ) lstOrderNumber ");
		sql.append(" FROM sale_order so ");
		sql.append(" WHERE so.approved = 1 ");
		sql.append(" AND so.type = 1 ");
		sql.append(" AND so.order_type IN (?, ?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		params.add(OrderType.TT.getValue());
		if (listSaleOrderId != null) {
			sql.append(" AND so.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		//sql.append(" AND so.time_print IS NULL ");
		if (!StringUtility.isNullOrEmpty(strListShopId)) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(strListShopId, "so.shop_id");
			sql.append(paramsFix);
		}
		sql.append(" GROUP BY so.DELIVERY_ID ");

		String[] fieldNames = { "deliveryId", "lstDelivery", "amount", "total", "moneyDiscount", "lstOrderNumber" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING };

		List<RptPGHVO> rpt = repo.getListByQueryAndScalar(RptPGHVO.class, fieldNames, fieldTypes, sql.toString(), params);

		for (RptPGHVO vo : rpt) {
			vo.setLstSaleProduct(this.getPGHGroupByNGVHProduct(listSaleOrderId, vo.getDeliveryId(), true));
			vo.setLstPromoProduct(this.getPGHGroupByNGVHProduct(listSaleOrderId, vo.getDeliveryId(), false));
			for (RptPGHDetailVO item : vo.getLstSaleProduct()) {
				vo.setSumThung(vo.getSumThung() + item.getThung());
				vo.setSumLe(vo.getSumLe() + item.getLe());
			}
		}
		return rpt;
	}

	@Override
	public int countSaleOrderNotApprovedOfShopInDay(Long shopId, Date lockDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("SELECT count(1) as count ");
		sql.append("FROM sale_order ");
		sql.append("WHERE shop_id   = ? ");
		params.add(shopId);
		sql.append("AND order_type in ('SO', 'IN') ");
		sql.append("AND order_date >= TRUNC(?) ");
		sql.append("AND order_date  < TRUNC(?) + 1 ");
		params.add(lockDate);
		params.add(lockDate);
		sql.append("and (approved = 0 or approved_van = 0)");
		return repo.countBySQL(sql.toString(), params);
	}

	@Override
	public List<SaleOrder> getListSaleOrderById(List<Long> listSaleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.* from sale_order so");
		sql.append(" where 1 = 1");
		if (listSaleOrderId != null) {
			sql.append(" AND so.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		sql.append(" ORDER BY so.staff_id, so.customer_id ");
		return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
	}
	
	@Override
	public List<SaleOrder> getListSaleOrderById2(List<Long> listSaleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.* from sale_order so");
		sql.append(" where 1 = 1");
		if (listSaleOrderId != null) {
			sql.append(" AND so.sale_order_id in (-1");
			for (Long saleOrderId : listSaleOrderId) {
				sql.append(",?");
				params.add(saleOrderId);
			}
			sql.append(")");
		}
		sql.append(" ORDER BY so.order_number");
		return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
	}

	/**
	 * ham nay phuongvm cco su dung bÃªn AdjustmentTaxAction.save() va
	 * saveVATGop()
	 */
	@Override
	public SaleOrder getSaleOrderByOrderNumberAndShopId(String orderNumber, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.* from sale_order so");
		sql.append(" where 1 = 1");
		sql.append(" and so.order_number = ? ");
		sql.append(" and so.shop_id = ? ");
		params.add(orderNumber);
		params.add(shopId);
		return repo.getEntityBySQL(SaleOrder.class, sql.toString(), params);
	}

	/**
	 * @author vuongmq
	 * @since Oct 15, 2014 su dung bÃªn
	 *        AssignTransferStaffAction.importExcelFile()
	 * @description Lay them dieu kien approved = 0 vÃ  approved_step =1
	 */
	@Override
	public SaleOrder getSaleOrderByOrderNumberAndShopIdApproved(String orderNumber, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.* from sale_order so");
		sql.append(" where 1 = 1");
		sql.append(" and upper(so.order_number) = ? ");
		sql.append(" and so.shop_id = ? ");
		params.add(orderNumber.toUpperCase());
		params.add(shopId);
		sql.append(" and approved = ? "); // 0
		sql.append(" and approved_step = ? "); //1
		params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue()); // chua duyet
		params.add(SaleOrderStep.CONFIRMED.getValue()); // don Ä‘Ã£ xÃ¡c nháº­n)
		return repo.getEntityBySQL(SaleOrder.class, sql.toString(), params);
	}
	
	@Override
	public SaleOrder getSaleOrderByOrderNumberAndListShop(String orderNumber, String lstShopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.* from sale_order so");
		sql.append(" where 1 = 1");
		sql.append(" and upper(so.order_number) = ? ");
		params.add(orderNumber.toUpperCase());
		//fix loi nhieu shop, nv
		String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(lstShopId, "so.shop_id");
		sql.append(paramsFix);
		//sql.append(" and approved = ? "); // 0
		//sql.append(" and approved_step = ? "); //1
		//params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue()); // chua duyet
		//params.add(SaleOrderStep.CONFIRMED.getValue()); // don Ä‘Ã£ xÃ¡c nháº­n)
		sql.append("AND ((approved = 0 AND approved_step = 1 ) OR approved = 1) ");		
		return repo.getEntityBySQL(SaleOrder.class, sql.toString(), params);
	}

	@Override
	public String generatePayNumber(Long shopId, ReceiptType type, Date dateNow) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		String code = "";
		sql.append(" select ");
		sql.append(" case  ");
		sql.append(" when count(*)<10 then to_char(''|| (count(*)+1))  ");
		sql.append(" when count(*)<100 then to_char(''|| (count(*)+1)) ");
		sql.append(" when count(*)<1000 then to_char(count(*)+1) end aaaa ");
		sql.append(" from pay_received pc ");
		sql.append(" where trunc(pc.create_date)=trunc(sysdate)  ");
		sql.append(" and pc.shop_id = ?");
		params.add(shopId);
		if (ReceiptType.RECEIVED.equals(type)) {
			//Phieu thu
			code = "T-" + DateUtility.toDateString(dateNow, "ddMMyy");
			sql.append(" and pc.receipt_type= ? ");
			params.add(ReceiptType.RECEIVED.getValue());
		} else if (ReceiptType.PAID.equals(type)) {
			//Phieu chi
			code = "C-" + DateUtility.toDateString(dateNow, "ddMMyy");
			sql.append(" and pc.receipt_type= ? ");
			params.add(ReceiptType.PAID.getValue());
		}
		String res = (String) repo.getObjectByQuery(sql.toString(), params);
		if (res != null)
			return code + '-' + res;
		else
			return code + "-1";
	}

	@Override
	public List<SaleOrder> getListSaleOrderById(List<Long> listSaleOrderId, Integer orderType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (orderType != null) {
			if (orderType == 1) { //order theo NVGH
				sql.append(" select so.* from sale_order so");
				sql.append(" left join staff st on so.delivery_id = st.staff_id");// Nutifood not require delivery staff
				sql.append(" where 1 = 1");
				if (listSaleOrderId != null) {
					sql.append(" AND so.sale_order_id in (-1");
					for (Long saleOrderId : listSaleOrderId) {
						sql.append(",?");
						params.add(saleOrderId);
					}
					sql.append(")");
				}
				sql.append(" ORDER BY st.staff_code, so.order_number");
			} else { //order theo NVBH
				sql.append(" select so.* from sale_order so");
				sql.append(" join staff st on so.staff_id = st.staff_id");
				sql.append(" where 1 = 1");
				if (listSaleOrderId != null) {
					sql.append(" AND so.sale_order_id in (-1");
					for (Long saleOrderId : listSaleOrderId) {
						sql.append(",?");
						params.add(saleOrderId);
					}
					sql.append(")");
				}
				sql.append(" ORDER BY st.staff_code, so.order_number");
			}
		}

		return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
	}

	@Override
	public List<SaleOrder> getListSaleOrderByIterator(Iterator<Long> soIterator) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select so.* from sale_order so");
		sql.append(" where 1 = 1");
		if (soIterator != null) {
			sql.append(" AND so.sale_order_id in (-1");
			while (soIterator.hasNext()) {
				Long soId = soIterator.next();
				sql.append(",?");
				params.add(soId);
			}
			sql.append(")");
		}
		sql.append(" ORDER BY so.sale_order_id");
		return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
	}

	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailByIterator(Iterator<Long> soIterator) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_detail");
		sql.append(" where sale_order_id in (");
		sql.append(" select so.sale_order_id from sale_order so");
		sql.append(" where 1 = 1");
		if (soIterator != null) {
			sql.append(" AND so.sale_order_id in (-1");
			while (soIterator.hasNext()) {
				Long soId = soIterator.next();
				sql.append(",?");
				params.add(soId);
			}
			sql.append(")");
		}
		sql.append(" )");
		sql.append(" and product_id is not null and invoice_id is null");
		sql.append(" order by sale_order_id, vat desc, program_code, is_free_item desc, sale_order_detail_id");
		return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
	}

	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailByListSaleOrderId(List<Long> lstSaleOrderId, Integer isFreeItem, boolean isOrder) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_detail");
		sql.append(" where 1 = 1");
		if (isFreeItem != null) {
			sql.append(" and is_free_item = ? ");
			params.add(isFreeItem);
		}
		sql.append(" and sale_order_id in ( ");
		sql.append(" select so.sale_order_id as sale_order_id from sale_order so");
		sql.append(" where 1 = 1");
		if (lstSaleOrderId != null && !lstSaleOrderId.isEmpty()) {
			sql.append(" AND so.sale_order_id in (-1");
			for (Long soId : lstSaleOrderId) {
				sql.append(",?");
				params.add(soId);
			}
			sql.append(")");
		}
		sql.append(" )");
		//sql.append(" and product_id is not null and invoice_id is null");
		if (isOrder) {
			sql.append(" order by sale_order_id, vat desc, program_code, is_free_item desc, sale_order_detail_id");
		}
		return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<RptPXKKVCNB_DataConvert> getPhieuXKVCNB(List<Long> lstsoId) throws DataAccessException {
		if (lstsoId == null) {
			throw new IllegalArgumentException("invalid parameter");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select p.product_code as ma_san_pham, p.product_name as ten_san_pham,");
		sql.append(" sum(trunc(sod.quantity / p.convfact)) as so_thung,");
		sql.append(" sum(mod(sod.quantity, p.convfact)) as so_le,");
		sql.append(" sod.price as don_gia, sum(nvl(sod.price * sod.quantity, 0)) as thanh_tien");
		sql.append(" from sale_order_detail sod");
		sql.append(" join sale_order so on (so.sale_order_id = sod.sale_order_id)");
		sql.append(" join product p on (p.product_id = sod.product_id)");
		sql.append(" where so.sale_order_id in (-1");
		for (int i = 0, sz = lstsoId.size(); i < sz; i++) {
			sql.append(",?");
			params.add(lstsoId.get(i));
		}
		sql.append(" ) group by p.product_id, p.product_code, p.product_name, sod.price, sod.price_id");
		sql.append(" order by ten_san_pham");

		String[] fieldNames = new String[] { "ma_san_pham", "ten_san_pham", "so_thung", "so_le", "don_gia", "thanh_tien" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL };

		return repo.getListByQueryAndScalar(RptPXKKVCNB_DataConvert.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/**
	 * Lay danh sach sp theo don hang
	 * 
	 * @author tungmt
	 */
	@Override
	public List<OrderProductVO> getListSOProductQuantityByList(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select sod.sale_order_id saleOrderId,sod.product_id productId, sum(sod.quantity) as quantity, ");
		sql.append(" (select order_date from sale_order where sale_order_id = sod.sale_order_id) orderDate ");
		sql.append(" from sale_order_detail sod  ");
		sql.append(" where 1=1 ");
		if(filter.getLstProductId() != null && filter.getLstProductId().size()>0){
			sql.append("and sod.product_id in (?");
			params.add(filter.getLstProductId().get(0));
			for(int i = 1 ; i < filter.getLstProductId().size() ; i++){
				sql.append(",?");
				params.add(filter.getLstProductId().get(i));
			}
			sql.append(") ");
		}
		if(filter.getLstSaleOrderId() != null && filter.getLstSaleOrderId().size()>0){
			sql.append("and sod.sale_order_id in (?");
			params.add(filter.getLstSaleOrderId().get(0));
			for(int i = 1 ; i < filter.getLstSaleOrderId().size() ; i++){
				sql.append(",?");
				params.add(filter.getLstSaleOrderId().get(i));
			}
			sql.append(") ");
		}
		sql.append(" group by sod.sale_order_id, sod.product_id");
		sql.append(" order by orderDate ");
		
		String[] fieldNames = new String[] {"saleOrderId", "productId", "quantity"};

		Type[] fieldTypes = new Type[] {StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };

		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/**
	 * Lay danh sach sp vuot ton kho
	 * 
	 * @author tungmt
	 */
	@Override
	public List<OrderProductVO> getListProductChangeQuantityWarning(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with ");
		sql.append(" lstSOD as ( ");
		sql.append(" select so.sale_order_id,sod.product_id,sod.quantity quantity,so.order_date  ");
		sql.append(" from sale_order so ");
		sql.append(" join sale_order_detail sod on sod.sale_order_id=so.sale_order_id ");
		sql.append(" where 1=1 ");
		sql.append(" and so.order_date>=trunc(?) and so.order_date<trunc(?)+1 ");
		params.add(filter.getFromDate() != null ? filter.getFromDate() : filter.getLockDay());
		params.add(filter.getLockDay());
		sql.append(" and so.order_source =? ");
		params.add(SaleOrderSource.TABLET.getValue());
		sql.append(" and so.shop_id=? ");
		params.add(filter.getShopId());
		sql.append(" and so.approved=0 ");
		sql.append(" and  nvl(sod.product_id,0)!=0 ");
		sql.append(" and so.approved_step=0 ");
		//chi tinh cac don hang trong lst
		if (filter.getLstSaleOrderId() != null) {// tinh tong so luong cho 1 lst don hang
			sql.append(" and so.sale_order_id in (-1");
			for (Long orderId : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(orderId);
			}
			sql.append(") ");
		}
		sql.append(" and sod.is_free_item = 1 and sod.program_type = 0 and sod.max_quantity_free != sod.quantity");
		sql.append(" ) ");
		sql.append(" select lstSOD.sale_order_id saleOrderId,lstSOD.product_id productId ");
		sql.append(" from lstSOD where 1=1 ");
		if (filter.getSaleOrderId() != null) {//filter cac san pham cua 1 don hang
			sql.append(" and sale_order_id=?");
			params.add(filter.getSaleOrderId());
		}
		sql.append(" order by lstSOD.order_date");
		String[] fieldNames = new String[] { "saleOrderId", "productId" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG };

		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/**
	 * Lay danh sach sp vuot ton kho
	 * 
	 * @author tungmt
	 */
	@Override
	public List<OrderProductVO> getListProductChangeRationWarning(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with ");
		sql.append(" lstSO as ( ");
		sql.append(" select so.*  ");
		sql.append(" from sale_order so ");
		sql.append(" where 1=1 ");
		sql.append(" and so.order_date>=trunc(?) and so.order_date<trunc(?)+1 ");
		params.add(filter.getFromDate() != null ? filter.getFromDate() : filter.getLockDay());
		params.add(filter.getLockDay());
		sql.append(" and so.order_source =? ");
		params.add(SaleOrderSource.TABLET.getValue());
		sql.append(" and so.shop_id=? ");
		params.add(filter.getShopId());
		sql.append(" and so.approved=0 ");
		sql.append(" and so.approved_step=0 ");
		//chi tinh cac don hang trong lst
		if (filter.getLstSaleOrderId() != null) {// tinh tong so luong cho 1 lst don hang
			sql.append(" and so.sale_order_id in (-1");
			for (Long orderId : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(orderId);
			}
			sql.append(") ");
		}
		sql.append(" ) ");
		sql.append(" select sop.sale_order_id saleOrderId,sop.PROMOTION_PROGRAM_CODE promotionProgramCode  ");
		sql.append(" from Sale_order_promotion sop where 1=1 ");
		sql.append(" and sop.sale_order_id in (select sale_order_id from lstSO) ");
		//sql.append(" and sop.quantity_received != sop.quantity_received_max ");
		sql.append(" and sop.quantity_received > sop.quantity_received_max ");
		if (filter.getSaleOrderId() != null) {//filter cac san pham cua 1 don hang
			sql.append(" and sop.sale_order_id=?");
			params.add(filter.getSaleOrderId());
		}
		String[] fieldNames = new String[] { "saleOrderId", "promotionProgramCode" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/**
	 * Lay danh sach sp vuot ton kho
	 * 
	 * @author tungmt
	 */
	@Override
	public List<OrderProductVO> getListProductQuantityWarning(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with ");
		sql.append(" lstSOExcep as (");
		sql.append(" 	SELECT distinct sale_order_id from sale_order_lot where order_date>=trunc(?) and order_date<trunc(?)+1 and sale_order_id is not null");
		params.add(filter.getFromDate() != null ? filter.getFromDate() : filter.getLockDay());
		params.add(filter.getLockDay());
		sql.append(" ),");
		sql.append(" lstSOD as ( ");
		sql.append(" select so.sale_order_id,sod.product_id,sod.quantity quantity,so.order_date, sod.is_free_item  ");
		sql.append(" from sale_order so ");
		sql.append(" join sale_order_detail sod on sod.sale_order_id=so.sale_order_id ");
		sql.append(" where 1=1 ");
		sql.append(" and so.order_date>=trunc(?) and so.order_date<trunc(?)+1 ");
		params.add(filter.getFromDate() != null ? filter.getFromDate() : filter.getLockDay());
		params.add(filter.getLockDay());
		sql.append(" and so.order_type = ? and so.order_source =? ");
		params.add(OrderType.IN.getValue());
		params.add(SaleOrderSource.TABLET.getValue());
		sql.append(" and so.shop_id=? ");
		params.add(filter.getShopId());
		sql.append(" and so.approved=0 ");
		sql.append(" and  nvl(sod.product_id,0)!=0 ");
		sql.append(" and so.approved_step=0 ");
		sql.append(" and so.sale_order_id not in (select sale_order_id from lstSOExcep) ");
		//chi tinh cac don hang trong lst
		if (filter.getLstSaleOrderId() != null) {// tinh tong so luong cho 1 lst don hang
			sql.append(" and so.sale_order_id in (-1");
			for (Long orderId : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(orderId);
			}
			sql.append(") ");
		}
		sql.append(" ), ");
		sql.append(" lstSP as ( ");
		sql.append(" select sod.sale_order_id, sod.order_date, sod.product_id, ");
		if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
			sql.append(" sod.is_free_item, ");
		}
		sql.append(" sum(sod.quantity) quantity ");
		sql.append(" from lstSOD sod ");
		sql.append(" group by sod.sale_order_id, sod.order_date, sod.product_id ");
		if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
			sql.append(" , sod.is_free_item ");
		}
		sql.append(" ), ");
		sql.append(" lstStock as ( ");
		sql.append(" select st.product_id, nvl(sum(st.available_quantity),0) quantity ");
		if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
			sql.append(" , wh.warehouse_type ");
		}
		sql.append(" from stock_total st ");
		sql.append(" join warehouse wh on st.warehouse_id = wh.warehouse_id and st.shop_id = wh.shop_id ");
		sql.append(" where st.status = 1 and st.product_id in (select product_id from lstSP) ");
		sql.append(" and st.object_type=1 and st.object_id=? ");
		params.add(filter.getShopId());
		sql.append(" group by st.product_id ");
		if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
			sql.append(" , wh.warehouse_type ");
		}
		sql.append(" ) ");
		sql.append(" select lstSP.sale_order_id saleOrderId, lstSP.product_id productId, (select product_code from product where product_id=lstSP.product_id) productCode ");
		sql.append(" ,nvl(lstStock.quantity,0) quantity ");
		if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
			sql.append(" , lstSP.is_free_item isFreeItem ");
		}
		sql.append(" from lstSP left join lstStock on lstSP.product_id=lstStock.product_id ");
		if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
			sql.append(" and lstStock.warehouse_type = (case when lstSP.is_free_item = 0 then 0 else 1 end) ");
		}		
		sql.append(" where lstStock.quantity is null or lstSP.quantity > lstStock.quantity ");
		if (filter.getSaleOrderId() != null) {//filter cac san pham cua 1 don hang
			sql.append(" and sale_order_id=?");
			params.add(filter.getSaleOrderId());
		}

		String[] fieldNames;
		if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
			fieldNames = new String[] { "saleOrderId", "productId", "quantity", "productCode", "isFreeItem" };
		} else {
			fieldNames = new String[] { "saleOrderId", "productId", "quantity", "productCode" };
		}

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };

		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/**
	 * Lay danh sach sp khac gia
	 * 
	 * @author tungmt
	 */
	@Override
	public List<OrderProductVO> getListProductPriceWarning(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with ");
		sql.append(" lstShop as ( ");
		sql.append("     select shop_id, level as orderNo from shop where status = 1 start with shop_id = ? connect by prior parent_shop_id = shop_id and status = 1 ");
		params.add(filter.getShopId());
		sql.append(" ), ");
		sql.append(" lstSOD as (");
		sql.append(" select sod.*,so.customer_id ");
		sql.append(" from sale_order so ");
		sql.append(" join sale_order_detail sod on sod.sale_order_id=so.sale_order_id ");
		sql.append(" where 1=1 ");
		if (filter.getIsFreeItem() != null) {
			sql.append(" and sod.is_free_item = ?");
			params.add(filter.getIsFreeItem());
		}
		if (filter.getLstSaleOrderId() != null) {// tinh tong so luong cho 1 lst don hang
			sql.append(" and so.sale_order_id in (-1");
			for (Long orderId : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(orderId);
			}
			sql.append(") ");
		}
		sql.append(" and so.order_date>=trunc(?) and so.order_date<trunc(?)+1 ");
		params.add(filter.getFromDate() != null ? filter.getFromDate() : filter.getLockDay());
		params.add(filter.getLockDay());
		if (filter.getOrderSource() != null) {
			sql.append(" and so.order_source =? ");
			params.add(filter.getOrderSource().getValue());
		}
		if (filter.getOrderType() != null) {
			sql.append(" and so.order_type =? ");
			params.add(filter.getOrderType().getValue());
		}
		sql.append(" and so.shop_id=? ");
		params.add(filter.getShopId());
		sql.append(" and so.approved=0 ");
		sql.append(" and nvl(sod.product_id,0)!=0 ");
		if (filter.getApprovedStep() != null) {
			sql.append(" and so.approved_step=? ");
			params.add(filter.getApprovedStep().getValue());
		}
		sql.append(" ) ");
		sql.append(" select saleOrderId, productId, prPrice price, priceId, prPackagePrice packagePrice, vat, productCode ");
		sql.append(" from ( ");
		sql.append(" 		SELECT sod.sale_order_id saleOrderId, sod.sale_order_detail_id saleOrderDetailId, sod.product_id productId, pr.price prPrice,");
		sql.append(" 		  sod.price sodPrice, pr.package_price prPackagePrice, sod.package_price sodPackagePrice, pr.price_id priceId,");
		sql.append(" 		  pr.vat, p.product_code productCode ");
		sql.append(" 		FROM lstSOD sod JOIN product p ON p.product_id=sod.product_id ");
		sql.append(" 		JOIN product_info pi ON pi.product_info_id=p.cat_id ");
		sql.append(" 		JOIN customer c ON c.customer_id=sod.customer_id ");
		sql.append(" 		LEFT JOIN price_customer_deduced pr ON pr.product_id = sod.product_id and sod.customer_id = pr.customer_id and sod.shop_id = pr.shop_id ");
		sql.append(" 			and pr.from_date < trunc(sod.order_date) + 1 and (pr.to_date is null or pr.to_date >= trunc(sod.order_date)) and pr.status=1 ");
		sql.append(" 		) r ");
		sql.append(" where 1=1 ");
		sql.append(" AND (r.prPrice IS NULL OR r.prPrice != r.sodPrice OR r.prPackagePrice IS NULL OR r.prPackagePrice != r.sodPackagePrice) ");
		
		String[] fieldNames = new String[] { "saleOrderId", "productId", "price", "priceId", "packagePrice", "vat", "productCode" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG, 
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.FLOAT, StandardBasicTypes.STRING };
		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/**
	 * Lay danh sach sp co CTKM con han
	 * 
	 * @author tungmt
	 */
	@Override
	public List<OrderProductVO> getListProductPromotionWarning(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with lstShopParent as ( ");
		sql.append(" 	select shop_id from shop where status=1 start with shop_id=? connect by prior parent_shop_id=shop_id ");
		params.add(filter.getShopId());
		sql.append(" ), ");
		sql.append(" lstSO as ( ");
		sql.append(" 	SELECT so.* FROM sale_order so");
		sql.append(" 	WHERE 1 =1");
		//chi tinh cac don hang trong lst
		if (filter.getLstSaleOrderId() != null) {// tinh tong so luong cho 1 lst don hang
			sql.append(" and so.sale_order_id in (-1");
			for (Long orderId : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(orderId);
			}
			sql.append(") ");
		}
		sql.append(" and so.order_date>=trunc(?) and so.order_date<trunc(?)+1 ");
		params.add(filter.getFromDate() != null ? filter.getFromDate() : filter.getLockDay());
		params.add(filter.getLockDay());
		sql.append(" and so.order_type =? and so.order_source =? ");
		params.add(OrderType.IN.getValue());
		params.add(SaleOrderSource.TABLET.getValue());
		sql.append(" and so.shop_id=? ");
		params.add(filter.getShopId());
		sql.append(" and so.approved=0 ");
		sql.append(" and so.approved_step=0 ");
		sql.append(" ), ");
		sql.append(" lstSOD as ( ");
		sql.append(" select so.sale_order_id,so.shop_id, sod.product_id, sod.program_code, so.order_date ");
		sql.append(" from lstSO so ");
		sql.append(" join sale_order_detail sod on sod.sale_order_id=so.sale_order_id ");
		sql.append(" where 1=1 ");
		sql.append(" and sod.program_type not in (?,?) ");
		params.add(ProgramType.DISPLAY_SCORE.getValue());
		params.add(ProgramType.KEY_SHOP.getValue());
		
		sql.append(" union all  ");
		
		sql.append(" SELECT distinct so.sale_order_id, so.shop_id, -1 product_id, spd.program_code, so.order_date ");
		sql.append(" FROM lstSO so ");
		sql.append(" join Sale_order_promo_detail spd on spd.sale_order_id = so.sale_order_id ");
		sql.append(" and spd.program_type not in (?,?) ");
		params.add(ProgramType.DISPLAY_SCORE.getValue());
		params.add(ProgramType.KEY_SHOP.getValue());
		
		sql.append(" ) ");
		sql.append(" select distinct sod.sale_order_id saleOrderId,sod.program_code promotionProgramCode ");
		sql.append(" from lstSOD sod ");
		sql.append(" where 1=1 ");
		sql.append(" And sod.program_code is not null ");
		sql.append(" and not exists (select 1 from promotion_program pp");
		sql.append(" join promotion_shop_map psm on (psm.promotion_program_id = pp.promotion_program_id");
		sql.append("    and psm.shop_id in (select shop_id from lstShopParent)");
		sql.append(" 	and psm.status = 1 ) ");
		sql.append(" where pp.promotion_program_code = sod.program_code");
		sql.append(" 	and pp.status = 1 and pp.from_date < trunc(sod.order_date)+1 ");
		sql.append(" 	and (pp.to_date is null or pp.to_date >= trunc(sod.order_date)) ");
		sql.append(" 	and psm.from_date < trunc(sod.order_date)+1 and (psm.to_date is null or psm.to_date >= trunc(sod.order_date)) ");
		sql.append(")");

		String[] fieldNames = new String[] { "saleOrderId", "promotionProgramCode" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<OrderProductVO> getListWarningPromotionOfOrder(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("with lstShopParent as (");
		sql.append(" 	select shop_id from shop where status=1 start with shop_id=? connect by prior parent_shop_id=shop_id");
		params.add(filter.getShopId());
		sql.append(") ");
		
		sql.append(", lstSOD as (");
		sql.append(" select sod.sale_order_id, sod.program_code, sod.order_date ");
		sql.append(" from sale_order_detail sod");
		sql.append(" where 1=1 ");
		sql.append(" and sod.sale_order_id = ?");
		params.add(filter.getSaleOrderId());
		sql.append(" and sod.program_type not in (?,?)");
		params.add(ProgramType.DISPLAY_SCORE.getValue());
		params.add(ProgramType.KEY_SHOP.getValue());
		
		sql.append(" union all");
		
		sql.append(" select sopd.sale_order_id, sopd.program_code, sopd.order_date ");
		sql.append(" from sale_order_promo_detail sopd");
		sql.append(" where 1=1 ");
		sql.append(" and sopd.sale_order_id = ?");
		params.add(filter.getSaleOrderId());
		sql.append(" and sopd.program_type not in (?,?)");
		params.add(ProgramType.DISPLAY_SCORE.getValue());
		params.add(ProgramType.KEY_SHOP.getValue());
		
		sql.append(")");
		
		sql.append(" select distinct sod.sale_order_id saleOrderId,sod.program_code promotionProgramCode ");
		sql.append(" from lstSOD sod ");
		sql.append(" where 1=1 ");
		sql.append(" And sod.program_code is not null ");
		sql.append(" and not exists (select 1 from promotion_program pp");
		sql.append(" 	join promotion_shop_map psm on (psm.promotion_program_id = pp.promotion_program_id");
		sql.append("    	and psm.shop_id in (select shop_id from lstShopParent)");
		sql.append(" 		and psm.status = 1 ) ");
		sql.append(" 	where pp.promotion_program_code = sod.program_code");
		sql.append(" 		and pp.status = 1 and pp.from_date < trunc(sod.order_date)+1 ");
		sql.append(" 		and (pp.to_date is null or pp.to_date >= trunc(sod.order_date)) ");
		sql.append(" 		and psm.from_date < trunc(sod.order_date)+1 and (psm.to_date is null or psm.to_date >= trunc(sod.order_date)) ");
		sql.append(")");

		String[] fieldNames = new String[] { "saleOrderId", "promotionProgramCode" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/**
	 * Lay danh sach don hang sai amount (rot chi tiet)
	 * 
	 * @author tungmt
	 */
	@Override
	public List<OrderProductVO> getListProductAmountWarning(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select so.sale_order_id saleOrderId,nvl(so.total_detail,0) totalDetail, ");
		sql.append(" (select count(1) from sale_order_detail where sale_order_id = so.sale_order_id) countSOD ");
		sql.append(" from sale_order so ");
		sql.append(" where 1=1 ");
		//chi tinh cac don hang trong lst
		if (filter.getLstSaleOrderId() != null) {// tinh tong so luong cho 1 lst don hang
			sql.append(" and so.sale_order_id in (-1");
			for (Long orderId : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(orderId);
			}
			sql.append(") ");
		}
		sql.append(" and so.order_date>=trunc(?) and so.order_date<trunc(?)+1 ");
		params.add(filter.getFromDate() != null ? filter.getFromDate() : filter.getLockDay());
		params.add(filter.getLockDay());
		sql.append(" and so.order_type = ? and so.order_source =? ");
		params.add(OrderType.IN.getValue());
		params.add(SaleOrderSource.TABLET.getValue());
		sql.append(" and so.shop_id=? ");
		params.add(filter.getShopId());
		sql.append(" and so.approved=0 ");
		sql.append(" and so.approved_step=0 ");
		sql.append(" group by so.sale_order_id,so.total_detail ");

		String[] fieldNames = new String[] { "saleOrderId", "totalDetail", "countSOD" };

		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };

		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/**
	 * Lay danh sach tra thuong keyshop vuot qua muc tra
	 * 
	 * @author tungmt
	 */
	@Override
	public List<OrderProductVO> getListKeyShopWarning(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with lstSOExcep as ( ");
		sql.append("    SELECT distinct sale_order_id from sale_order_lot where order_date>=trunc(?) and order_date<trunc(?)+1 and sale_order_id is not null ");
		params.add(filter.getFromDate() != null ? filter.getFromDate() : filter.getLockDay());
		params.add(filter.getLockDay());
		sql.append(" ) ");
		sql.append(" ,lstSO as ( "); 
		sql.append("		 select so.* ");
		sql.append("		 from sale_order so "); 
		sql.append("		 where 1=1  ");
		sql.append("		 and so.order_date>=trunc(?) and so.order_date<trunc(?)+1 ");
		params.add(filter.getFromDate() != null ? filter.getFromDate() : filter.getLockDay());
		params.add(filter.getLockDay());
		sql.append("		 and so.order_type = ? and so.order_source =? ");
		params.add(OrderType.IN.getValue());
		params.add(SaleOrderSource.TABLET.getValue());
		sql.append("		 and so.shop_id = ? ");
		params.add(filter.getShopId());
		sql.append("		 and so.approved=0 and so.approved_step=0 ");
		if (filter.getLstSaleOrderId() != null) {
			sql.append(" 		and so.sale_order_id in (-1");
			for (Long orderId : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(orderId);
			}
			sql.append(") ");
		}
		sql.append("		 and so.sale_order_id not in (select sale_order_id from lstSOExcep) ");
		sql.append(" ) ");
		sql.append(" ,lstProduct as ( "); 
		sql.append("     select (select ks_code from ks where ks_id = kc.ks_id) ksCode ,kc.customer_id ,p.product_id "); 
		sql.append("     ,(nvl(sum(product_num),0) - nvl(sum(product_num_done),0)) quantityAmount ");
		sql.append("     from ks_customer kc  ");
		sql.append("     join ks_cus_product_reward kcpr on kcpr.ks_customer_id = kc.ks_customer_id "); 
		sql.append("     join product p on kcpr.product_id = p.product_id  ");
		sql.append("     where kc.status = 1 and kc.reward_type in (?,?)  ");
		params.add(RewardType.MO_KHOA_CHUA_TRA.getValue());
		params.add(RewardType.DA_TRA_MOT_PHAN.getValue());
		sql.append("     group by kc.ks_id,kc.customer_id, p.product_id ");
		sql.append(" ) ");
		sql.append(" ,lstMoney as ( "); 
		sql.append("     select (select ks_code from ks where ks_id = kc.ks_id) ksCode, kc.customer_id "); 
		sql.append("     ,(nvl(sum(total_reward_money),0) - nvl(sum(total_reward_money_done),0)) quantityAmount "); 
		sql.append("     from ks_customer kc  ");
		sql.append("     where kc.status = 1 and kc.reward_type in (?,?) ");
		params.add(RewardType.MO_KHOA_CHUA_TRA.getValue());
		params.add(RewardType.DA_TRA_MOT_PHAN.getValue());
		sql.append("     group by kc.ks_id, kc.customer_id ");
		sql.append(" ) ");
		sql.append(" ,lstSODProduct as ( "); 
		sql.append("		 select so.sale_order_id,so.customer_id, sod.program_code ksCode, sod.product_id, nvl(sum(sod.quantity),0) quantityAmount ");
		sql.append("		 from lstSO so  ");
		sql.append("		 join sale_order_detail sod on sod.sale_order_id=so.sale_order_id "); 
		sql.append("		 where sod.is_free_item = 1 and sod.program_type = ? ");
		params.add(ProgramType.KEY_SHOP.getValue());
		sql.append("     group by so.sale_order_id, so.customer_id, sod.program_code, sod.product_id ");
		sql.append(" ) ");
		sql.append(" ,lstSODMoney as ( ");
		sql.append("		 SELECT so.sale_order_id, so.customer_id, spd.program_code ksCode, nvl(sum(discount_amount),0) quantityAmount ");
		sql.append("		 FROM lstSO so  ");
		sql.append("		 join Sale_order_promo_detail spd on spd.sale_order_id = so.sale_order_id "); 
		sql.append("     where 1=1 and spd.program_type = ? ");
		params.add(ProgramType.KEY_SHOP.getValue());
		sql.append("     group by so.sale_order_id, so.customer_id, spd.program_code ");
		sql.append(" ) ");
		sql.append(" ,resultProduct as ( ");
		sql.append("     select sod.sale_order_id saleOrderId, sod.ksCode, sod.product_id productId, (select product_code from product where product_id = sod.product_id) productCode ");
		sql.append("     from lstSODProduct sod ");
		sql.append("     where 1=1 "); 
		sql.append("     and ( ");
		sql.append("        not exists (select 1 from lstProduct where product_id = sod.product_id and ksCode = sod.ksCode and customer_id = sod.customer_id ) ");
		sql.append("        or ");
		sql.append("        exists (select 1 from lstProduct where product_id = sod.product_id and ksCode = sod.ksCode and customer_id = sod.customer_id and sod.quantityAmount > quantityAmount) ");
		sql.append("     ) ");
		sql.append(" ) ");
		sql.append(" ,resultMoney as ( ");
		sql.append("     select sod.sale_order_id saleOrderId, sod.ksCode, null as productId, null as productCode ");
		sql.append("     from lstSODMoney sod ");
		sql.append("     where 1=1 "); 
		sql.append("     and ( ");
		sql.append("        not exists (select 1 from lstMoney where 1=1 and ksCode = sod.ksCode and customer_id = sod.customer_id) ");
		sql.append("        or ");
		sql.append("        exists (select 1 from lstMoney where 1=1 and ksCode = sod.ksCode and customer_id = sod.customer_id and sod.quantityAmount > quantityAmount) ");
		sql.append("     ) ");
		sql.append(" ) ");
		sql.append(" select * from resultProduct ");
		sql.append(" union all ");
		sql.append(" select * from resultMoney ");

		String[] fieldNames = new String[] { "saleOrderId", "ksCode", "productId", "productCode" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/**
	 * Lay danh sach don hang sai amount (rot chi tiet)
	 * 
	 * @author tungmt
	 */
	@Override
	public List<SaleOrderDetailVOEx> getListSaleOrderDetailForConfirm(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (filter.getLockDay() == null) {
			filter.setLockDay(commonDAO.getSysDate());
		}

		SaleOrder so = this.getSaleOrderById(filter.getSaleOrderId());

		sql.append(" with lstShopParent as ( ");
		sql.append(" 	select shop_id from shop where status=1 start with shop_id=? connect by prior parent_shop_id=shop_id ");
		params.add(filter.getShopId());
		sql.append(" ), ");
		sql.append(" lstSOD as ( ");
		sql.append(" 	select sod.product_id productId,sod.sale_order_id, ");
		sql.append(" 	p.product_code productCode, ");
		sql.append(" 	p.product_name productName, ");
		sql.append(" 	p.convfact convfact, ");
		sql.append("	pi.product_info_code productInfoCode,");
		sql.append(" 	sod.quantity,sod.is_free_item isFreeItem, ");
		sql.append(" 	sod.program_code programCode, sod.program_type programType, sod.price, so.staff_id, ");
		sql.append(" 	(select pp.promotion_program_code from promotion_program pp ");
		sql.append(" 	join promotion_shop_map psm on pp.promotion_program_id=psm.promotion_program_id ");
		sql.append(" 	where 1=1 ");
		sql.append(" 	and psm.shop_id in (select shop_id from lstShopParent) ");
		sql.append(" 	and psm.status = 1 and psm.from_date < trunc(so.order_date)+1 ");
		sql.append(" 	and (psm.to_date is null or psm.to_date >= trunc(so.order_date)) ");
		sql.append(" 	and pp.status = 1 and pp.from_date < trunc(so.order_date)+1 ");
		sql.append(" 	and (pp.to_date is null or pp.to_date >= trunc(so.order_date)) ");
		sql.append(" 	and pp.promotion_program_code=sod.program_code and rownum=1");
		sql.append(" 	) promotionProgramCode, ");
		sql.append(" (select short_code from customer where customer_id in ");
		sql.append(" 	(select customer_id from sale_order where sale_order_id=sod.sale_order_id)) customerCode");
		if (so != null && (OrderType.SO.equals(so.getOrderType())) || OrderType.CO.equals(so.getOrderType())) {
			sql.append(",1 as isVansale");
		} else {
			sql.append(",0 as isVansale");
		}
		sql.append(" 	from sale_order_detail sod ");
		sql.append(" 	join sale_order so on so.sale_order_id=sod.sale_order_id ");
		sql.append(" 	join customer c on so.customer_id=c.customer_id ");
		sql.append(" 	join product p on p.product_id=sod.product_id ");
		sql.append(" 	join product_info pi on pi.product_info_id=p.cat_id ");
		sql.append(" 	where 1=1 ");
		sql.append(" 	and sod.sale_order_id=? ");
		params.add(filter.getSaleOrderId());
		sql.append(" ), ");
		sql.append(" lstStock as ( ");
		sql.append(" select st.product_id, sum(st.available_quantity) availableQuantity ");
		if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
			sql.append(" , wh.warehouse_type ");
		}
		sql.append(" from stock_total st ");
		sql.append(" join warehouse wh on wh.warehouse_id = st.warehouse_id ");
		sql.append(" where 1=1 ");
		sql.append(" and st.object_type=1 and st.object_id=?  ");
		params.add(filter.getShopId());
		sql.append(" and st.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" group by st.product_id ");
		if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
			sql.append(" , wh.warehouse_type ");
		}
		sql.append(" ) ");

		sql.append(" select productId, productCode, productName ");
		if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
			sql.append(" , warehouse_type warehouseType ");
		} else {
			sql.append(" , -1 warehouseType ");
		}
		sql.append(" , programCode, programType, promotionProgramCode, isFreeItem, price, convfact, customerCode, productInfoCode, isVansale, ");
		// lacnv1 - ton kho don vansale lay theo ton kho nhan vien
		if (so != null && (OrderType.SO.equals(so.getOrderType())) || OrderType.CO.equals(so.getOrderType())) {
			sql.append(" (select sum(approved_quantity) from stock_total where object_type = ? and object_id = ? and product_id = lstSOD.productId and status = ?) as availableQuantity,");
			params.add(StockObjectType.STAFF.getValue());
			params.add(so.getStaff().getId());
			params.add(ActiveType.RUNNING.getValue());
		} else {
			sql.append(" lstStock.availableQuantity,");
		}
		sql.append(" sum(quantity) as quantity,");
		// --
		sql.append(" (select staff_code from staff where staff_id = lstSOD.staff_id) staffCode ");
		
		sql.append(" from lstSOD");
		sql.append(" left join lstStock on lstSOD.productId = lstStock.product_id ");
		if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
			sql.append(" and lstStock.warehouse_type = (case when isFreeItem = 0 then 0 else 1 end) ");
		}
		if (so != null && (OrderType.SO.equals(so.getOrderType())) || OrderType.CO.equals(so.getOrderType())) {
			sql.append(" group by lstSOD.sale_order_id, productId, productCode, productName, programCode, programType, promotionProgramCode, isFreeItem, price, convfact, customerCode, productInfoCode, isVansale, lstSOD.staff_id ");
			if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
				sql.append(" , warehouse_type ");
			}
		} else {
			sql.append(" group by lstSOD.sale_order_id, productId, productCode, productName, programCode, programType, promotionProgramCode, isFreeItem, price, convfact, customerCode, productInfoCode, isVansale, availableQuantity, lstSOD.staff_id");
			if (filter.getIsDividualWarehouse() == null || filter.getIsDividualWarehouse()) {
				sql.append(" , warehouse_type ");
			}
		}

		sql.append(" order by isFreeItem , productCode");

		String[] fieldNames = new String[] { 
				"productId", "productCode", "productName", "warehouseType", 
				"programCode", "programType", "promotionProgramCode", "isFreeItem", 
				"price", "convfact", "customerCode", "productInfoCode",
				"isVansale", "availableQuantity", "quantity", "staffCode" };

		Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, 
				StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER,  
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING};

		return repo.getListByQueryAndScalar(SaleOrderDetailVOEx.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public void updatePOCustomer(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("update PO_CUSTOMER set APPROVED=?, update_date=?, update_user=? ");
		params.add(filter.getApproval().getValue());
		params.add(commonDAO.getSysDate());
		params.add(filter.getStaffCode());
		sql.append(" where PO_CUSTOMER_ID in (-1");
		for (Long id : filter.getLstPOCustomerId()) {
			sql.append(",?");
			params.add(id);
		}
		sql.append(")");

		repo.executeSQLQuery(sql.toString(), params);
	}

	@Override
	public List<SaleOrderStockDetailVO> getListSaleOrderStockDetail(long saleOrderId, String orderType, boolean isPrintStep) throws DataAccessException {
		if (saleOrderId <= 0) {
			throw new IllegalArgumentException("invalid saleOrderId");
		}
		if (orderType.length() <= 0) {
			throw new IllegalArgumentException("invalid orderType");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (orderType.equals(OrderType.IN.getValue()) || orderType.equals(OrderType.CM.getValue()) || orderType.equals(OrderType.SO.getValue()) || orderType.equals(OrderType.CO.getValue())) {
			sql.append(" select so.sale_order_id as id ");
			sql.append(" , so.shop_id as shopId ");
			sql.append(" , so.staff_id as staffId ");
			sql.append(" , sod.sale_order_detail_id as detailId ");
			sql.append(" , sol.product_id as productId ");
			sql.append(" , sol.quantity as quatity ");
			sql.append(" , sol.stock_total_id as stockId ");
			sql.append(" , sol. warehouse_id as warehouseId ");
			sql.append(" , so.order_type as orderType ");
			sql.append(" from sale_order so ");
			sql.append(" join sale_order_detail sod on sod.sale_order_id = so.sale_order_id ");
			sql.append(" join sale_order_lot sol on sol.sale_order_detail_id = sod.sale_order_detail_id ");
			sql.append(" where 1 = 1 ");
			sql.append(" and so.sale_order_id = ? ");
			params.add(saleOrderId);
			if (!isPrintStep) {
				sql.append(" and so.approved = ? and approved_step = ? ");
				params.add(SaleOrderStatus.APPROVED.getValue());
				params.add(SaleOrderStep.PRINT_CONFIRMED.getValue());
			}
			/*
			 * sql.append(" and so.order_type = ? "); params.add(orderType);
			 */
			String[] fieldNames = new String[] { "id", "shopId", "staffId", "detailId", "productId", "quatity", "stockId", "warehouseId", };
			Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.LONG };
			return repo.getListByQueryAndScalar(SaleOrderStockDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else if (orderType.equals(OrderType.GO.getValue()) || orderType.equals(OrderType.DP.getValue()) || orderType.equals(OrderType.DC.getValue()) || orderType.equals(OrderType.DCT.getValue()) || orderType.equals(OrderType.DCG.getValue())) {
			sql.append(" select str.stock_trans_id as id ");
			sql.append(" , str.shop_id as shopId ");
			sql.append(" , str.staff_id as staffId ");
			sql.append(" , strd.stock_trans_detail_id as detailId ");
			sql.append(" , strl.product_id as productId ");
			sql.append(" , strl.quantity as quatity ");
			sql.append(" , strl.from_owner_id as fromOwnerId ");
			sql.append(" , strl.from_owner_type as fromOwnerType ");
			sql.append(" , strl.to_owner_id as toOwnerId ");
			sql.append(" , strl.to_owner_type as toOwnerType ");
			sql.append(" from stock_trans str ");
			sql.append(" join stock_trans_detail strd on strd.stock_trans_id = str.stock_trans_id ");
			sql.append(" join stock_trans_lot strl on strl.stock_trans_detail_id = strd.stock_trans_detail_id ");
			sql.append(" where 1 = 1 ");
			sql.append(" and str.stock_trans_id = ? ");
			params.add(saleOrderId);
			if (!isPrintStep) {
				sql.append(" and str.approved = ? and approved_step = ? ");
				params.add(SaleOrderStatus.APPROVED.getValue());
				params.add(SaleOrderStep.PRINT_CONFIRMED.getValue());
			}
			/*
			 * sql.append(" str.TRANS_TYPE = ? "); params.add(orderType);
			 */
		}
		String[] fieldNames = new String[] { "id", "shopId", "staffId", "detailId", "productId", "quatity", "fromOwnerId", "fromOwnerType", "toOwnerId", "toOwnerType" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(SaleOrderStockDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<SaleOrderStockDetailVO> getListSaleOrderStockDetailUpdate(long saleOrderId, String orderType) throws DataAccessException {
		if (saleOrderId <= 0) {
			throw new IllegalArgumentException("invalid saleOrderId");
		}
		if (orderType.length() <= 0) {
			throw new IllegalArgumentException("invalid orderType");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (orderType.equals(OrderType.IN.getValue()) || orderType.equals(OrderType.CM.getValue()) || orderType.equals(OrderType.SO.getValue()) || orderType.equals(OrderType.CO.getValue())) {
			sql.append(" select so.sale_order_id as id ");
			sql.append(" , so.shop_id as shopId ");
			sql.append(" , so.staff_id as staffId ");
			sql.append(" , sod.sale_order_detail_id as detailId ");
			sql.append(" , sol.product_id as productId ");
			sql.append(" , sol.quantity as quatity ");
			sql.append(" , sol.stock_total_id as stockId ");
			sql.append(" , sol. warehouse_id as warehouseId ");
			sql.append(" , so.order_type as orderType ");
			sql.append(" from sale_order so ");
			sql.append(" join sale_order_detail sod on sod.sale_order_id = so.sale_order_id ");
			sql.append(" join sale_order_lot sol on sol.sale_order_detail_id = sod.sale_order_detail_id ");
			sql.append(" where 1 = 1 ");
			sql.append(" and so.sale_order_id = ? ");
			params.add(saleOrderId);
			sql.append(" and so.approved = ? and approved_step = ? ");
			params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
			params.add(SaleOrderStep.CONFIRMED.getValue());
			/*
			 * sql.append(" and so.order_type = ? "); params.add(orderType);
			 */
			String[] fieldNames = new String[] { "id", "shopId", "staffId", "detailId", "productId", "quatity", "stockId", "warehouseId", };
			Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.LONG };
			return repo.getListByQueryAndScalar(SaleOrderStockDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else if (orderType.equals(OrderType.GO.getValue()) || orderType.equals(OrderType.DP.getValue()) || orderType.equals(OrderType.DC.getValue()) || orderType.equals(OrderType.DCT.getValue()) || orderType.equals(OrderType.DCG.getValue())) {
			sql.append(" select str.stock_trans_id as id ");
			sql.append(" , str.shop_id as shopId ");
			sql.append(" , str.staff_id as staffId ");
			sql.append(" , strd.stock_trans_detail_id as detailId ");
			sql.append(" , strl.product_id as productId ");
			sql.append(" , strl.quantity as quatity ");
			sql.append(" , strl.from_owner_id as fromOwnerId ");
			sql.append(" , strl.from_owner_type as fromOwnerType ");
			sql.append(" , strl.to_owner_id as toOwnerId ");
			sql.append(" , strl.to_owner_type as toOwnerType ");
			sql.append(" from stock_trans str ");
			sql.append(" join stock_trans_detail strd on strd.stock_trans_id = str.stock_trans_id ");
			sql.append(" join stock_trans_lot strl on strl.stock_trans_detail_id = strd.stock_trans_detail_id ");
			sql.append(" where 1 = 1 ");
			sql.append(" and str.stock_trans_id = ? ");
			params.add(saleOrderId);
			sql.append(" and str.approved = ? and approved_step = ? ");
			params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
			params.add(SaleOrderStep.CONFIRMED.getValue());
			/*
			 * sql.append(" str.TRANS_TYPE = ? "); params.add(orderType);
			 */
		}
		String[] fieldNames = new String[] { "id", "shopId", "staffId", "detailId", "productId", "quatity", "fromOwnerId", "fromOwnerType", "toOwnerId", "toOwnerType" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(SaleOrderStockDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/**
	 * Lay danh sach don hang pay
	 * 
	 * @author tungmt
	 */
	@Override
	public List<SaleOrderVO> getListSaleOrderForPay(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (filter.getLockDay() == null) {
			filter.setLockDay(commonDAO.getSysDate());
		}
		sql.append(" with ");
		sql.append(" lstPR as ( ");
		sql.append(" select pdt.pay_received_id , prt.pay_received_number,nvl(pdt.discount,0) discount, ");
		sql.append(" nvl(pdt.amount,0) amount, nvl(so.total, 0) total,so.order_number, pdt.pay_date, ");
		sql.append(" so.ref_order_number,so.staff_id, ");
		sql.append(" (select staff_code||' - '||staff_name from staff where staff_id=so.staff_id) staffCode, ");
		sql.append(" (select shop_code from shop where shop_id=so.shop_id) shopCode, ");
		sql.append(" (select customer_id||'.-'||short_code||'-'||customer_name from customer where customer_id= so.customer_id) customerCode, ");
		sql.append(" (select address from customer where customer_id= so.customer_id) address");
		// lacnv1 - debit_detail
		sql.append(", nvl((select sum(remain) from debit_detail where type in (4, 5) and from_object_id = so.sale_order_id), 0) as remain");
		// --
		sql.append(", (case when so.delivery_id is not null then (select staff_code||' - '||staff_name from staff where staff_id=so.delivery_id) else null end) as deliveryCode ");
		sql.append(" from pay_received_temp prt ");
		sql.append(" join payment_detail_temp pdt on pdt.pay_received_id=prt.pay_received_id ");
		//sql.append(" join debit_detail dd on dd.debit_detail_id=pdt.debit_detail_id ");
		//sql.append(" join sale_order so on so.sale_order_id=dd.from_object_id ");
		sql.append(" join sale_order so on so.sale_order_id=pdt.for_object_id ");
		sql.append(" and so.approved_step in (2,3) and so.order_type in ('IN','SO','CO') ");
		if ( filter.getShopId() != null ) {
			sql.append(" where prt.shop_id=? ");
			params.add(filter.getShopId());
		}
		sql.append(" and pdt.pay_date<trunc(?)+1 ");
		params.add(filter.getLockDay());
		sql.append(" AND nvl(prt.approved,-1) not in (1,2) ");
		sql.append(" and not exists (select 1 from pay_received where prt.pay_received_id=pay_received_id) ");
		if (filter.getLstPayReceivedId() != null) {
			sql.append(" and prt.pay_received_id in (-1");
			for (Long id : filter.getLstPayReceivedId()) {
				sql.append(",?");
				params.add(id);
			}
			sql.append(" ) ");
		}
		//trietptm - bá»� TH cho phÃ©p duyá»‡t thanh toÃ¡n phiáº¿u chi khi Ä‘Æ¡n hÃ ng cá»§a Ä‘Æ¡n vansale CO chÆ°a qua bÆ°á»›c in hÃ³a Ä‘Æ¡n
		sql.append(" AND NOT EXISTS (SELECT 1 FROM sale_order ");
		sql.append("                 WHERE from_sale_order_id = pdt.for_object_id");
		sql.append("                 AND approved_step < 2 ");
		sql.append("                 AND order_type = 'CO')");
		
		sql.append(" ), ");
		sql.append(" lstPRStaff as ( ");
		sql.append(" select pay_received_id from lstPR ");
		sql.append(" where 1=1 ");
		if (filter.getStaffId() != null) {
			sql.append(" and staff_id=? ");//chi can co don hang co nvbh nay la lay payreceived do luon
			params.add(filter.getStaffId());
		}
		sql.append(" ) ");
		sql.append(" SELECT pr.pay_received_id payReceivedId,pr.pay_received_number payReceivedNumber, ");
		sql.append(" sum(pr.amount) amount, sum(pr.discount) discount, sum(pr.total) total,pr.pay_date payDate, ");
		sql.append(" LISTAGG(pr.order_number, ',\n') WITHIN GROUP (ORDER BY pr.order_number) orderNumber, ");
		sql.append(" LISTAGG(pr.ref_order_number, ',\n') WITHIN GROUP (ORDER BY pr.ref_order_number) refOrderNumber, ");
		sql.append(" LISTAGG(to_char(pr.staffCode), ',\n') WITHIN GROUP (ORDER BY pr.staffCode) staffCode, ");
		sql.append(" LISTAGG(pr.shopCode, ',\n') WITHIN GROUP (ORDER BY pr.shopCode) shopCode, ");
		sql.append(" LISTAGG(to_char(pr.customerCode), ',\n') WITHIN GROUP (ORDER BY pr.customerCode) customerCode, ");
		sql.append(" LISTAGG(to_char(pr.address), ',\n') WITHIN GROUP (ORDER BY pr.address) address ");
		// lacnv1 - debit_detail
		sql.append(", sum(remain) as remain");
		// --
		sql.append(", LISTAGG(to_char(pr.deliveryCode), ',\n') WITHIN GROUP (ORDER BY pr.deliveryCode) deliveryCode ");
		sql.append(" FROM lstPR pr where pr.pay_received_id in (select pay_received_id from lstPRStaff) ");
		sql.append(" GROUP BY pr.pay_received_id,pr.pay_received_number,pr.pay_date ");
		
		countSql.append("select count(1) count from (");
		countSql.append(sql);
		countSql.append(")");
		
		String[] fieldNames = new String[] { "orderNumber", "payDate", "refOrderNumber", "staffCode", "shopCode", "customerCode", "address", "total", "amount", "discount", "payReceivedNumber", "payReceivedId",
				// lacnv1 - debit_detail
				"remain",
				// --
				"deliveryCode"
		};
		
		String[] arrColumnAction = new String[] { "nvl(refOrderNumber,'')", "nvl(orderNumber,'')", "nvl(payReceivedNumber,'')", "nvl(staffCode,'')",
										"nvl(customerCode,'')", "nvl(total,0)", "nvl(discount,'')", "nvl(amount,'')", 
										"payDate", "nvl(address,'')", "nvl(deliveryCode,'')", "shopCode",
										"remain", "payReceivedId"
		};
		String sort = StringUtility.getColumnSort(filter.getSort(), arrColumnAction);
		if (!StringUtility.isNullOrEmpty(sort)) {
			sql.append(" order by ").append(sort).append(" ");
			String order = StringUtility.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				sql.append(order);
			}
		} else {
			sql.append(" order by shopCode, staffCode, payDate ");
		}
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.STRING, StandardBasicTypes.DATE,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING, StandardBasicTypes.LONG,
				// lacnv1 - debit_detail
				StandardBasicTypes.BIG_DECIMAL,
				// --
				StandardBasicTypes.STRING
		};

		if (filter.getkPagingVO() == null) {
			//sql.append(" order by shopCode,staffCode,payDate ");
			return repo.getListByQueryAndScalar(SaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(SaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVO());
		}
	}

	@Override
	public void cancelPay(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (filter.getLockDay() == null) {
			filter.setLockDay(commonDAO.getSysDate());
		}
		sql.append("select *");
		sql.append(" from payment_detail_temp pdt");
		sql.append(" where pdt.pay_received_id in (-1");
		if (filter.getLstPayReceivedId() != null) {
			for (Long id : filter.getLstPayReceivedId()) {
				sql.append(",?");
				params.add(id);
			}
		}
		sql.append(")");
		List<PaymentDetailTemp> lst = repo.getListBySQL(PaymentDetailTemp.class, sql.toString(), params);
		if (lst != null && lst.size() > 0) {
			Date sysdate = commonDAO.getSysDate();
			PayReceivedTemp pTmp = null;
			SaleOrder so = null;
			BigDecimal am = null;
			DebitDetailTemp ddTmp = null;
			sql = new StringBuilder();
			sql.append("select * from debit_detail_temp where type in (4, 5) and from_object_id = ?");
			for (PaymentDetailTemp pdTmp : lst) {
				// cap nhat trang thai cua pay_received_temp
				pTmp = repo.getEntityById(PayReceivedTemp.class, pdTmp.getPayReceived().getId());
				pTmp.setApproved(2);
				pTmp.setUpdateDate(sysdate);
				pTmp.setUpdateUser(filter.getStaffCode());
				repo.update(pTmp);
				
				//
				so = repo.getEntityById(SaleOrder.class, pdTmp.getForObjectId());
				so.setDescription(filter.getDescription());
				so.setUpdateDate(sysdate);
				so.setUpdateUser(filter.getStaffCode());
				repo.update(so);
				
				// lacnv1 - debit_detail
				// cap nhat debit_detail_temp
				if (pdTmp.getForObjectId() != null) {
					params = new ArrayList<Object>();
					params.add(pdTmp.getForObjectId());
					
					ddTmp = repo.getEntityBySQL(DebitDetailTemp.class, sql.toString(), params);
					
					if (ddTmp != null) {
						if (pdTmp.getAmount() == null) {
							pdTmp.setAmount(BigDecimal.ZERO);
						}
						if (pdTmp.getDiscount() == null) {
							pdTmp.setDiscount(BigDecimal.ZERO);
						}
						am = pdTmp.getAmount().add(pdTmp.getDiscount());
						
						ddTmp.setUpdateDate(sysdate);
						ddTmp.setUpdateUser(pTmp.getUpdateUser());
						if (ddTmp.getRemain() == null) {
							ddTmp.setRemain(BigDecimal.ZERO);
						}
						ddTmp.setRemain(ddTmp.getRemain().add(am));
						if (ddTmp.getTotalPay() == null) {
							ddTmp.setTotalPay(BigDecimal.ZERO);
						}
						ddTmp.setTotalPay(ddTmp.getTotalPay().subtract(pdTmp.getAmount()));
						if (pdTmp.getDiscount() != null) {
							if (ddTmp.getDiscountAmount() == null) {
								ddTmp.setDiscountAmount(BigDecimal.ZERO);
							}
							ddTmp.setDiscountAmount(ddTmp.getDiscountAmount().subtract(pdTmp.getDiscount()));
						}
						repo.update(ddTmp);
					}
				}
			}
		}
	}

	@Override
	public void coppyPayment(List<Long> lstPayReceived, LogInfoVO loginfo) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" insert into PAY_RECEIVED (PAY_RECEIVED_ID,PAY_RECEIVED_NUMBER,AMOUNT,CREATE_DATE,PAYMENT_TYPE,SHOP_ID, payment_status,payer_name,payer_address,payment_reason,");
		sql.append(" RECEIPT_TYPE,UPDATE_DATE,CREATE_USER,UPDATE_USER,TYPE,CREATE_DATE_SYS,BANK_ID,STAFF_ID,APPROVED) ");
		sql.append(" select pr.PAY_RECEIVED_ID,pr.PAY_RECEIVED_NUMBER,pr.AMOUNT,pr.CREATE_DATE,pr.PAYMENT_TYPE,pr.SHOP_ID, 1,pr.payer_name,pr.payer_address,pr.payment_reason,");
		sql.append(" pr.RECEIPT_TYPE,pr.UPDATE_DATE,pr.CREATE_USER,pr.UPDATE_USER,pr.TYPE,pr.CREATE_DATE_SYS,pr.BANK_ID,pr.STAFF_ID,pr.APPROVED ");
		sql.append(" from PAY_RECEIVED_TEMP pr ");
		sql.append(" where pr.pay_received_id in (-1 ");
		if (lstPayReceived != null) {
			for (Long id : lstPayReceived) {
				sql.append(",?");
				params.add(id);
			}
		}
		sql.append(" ) ");
		sql.append(" and not exists (select 1 from PAY_RECEIVED where pay_received_id=pr.pay_received_id) ");
		repo.executeSQLQuery(sql.toString(), params);

		sql = new StringBuilder();
		params = new ArrayList<Object>();
		sql.append(" insert into payment_detail (PAYMENT_DETAIL_ID,PAY_RECEIVED_ID,AMOUNT,PAYMENT_TYPE,STATUS,PAY_DATE,customer_id, shop_id, payment_status,");
		sql.append(" CREATE_USER,UPDATE_USER,CREATE_DATE,UPDATE_DATE,DEBIT_DETAIL_ID,DISCOUNT,TIME,STAFF_ID) ");
		sql.append(" select pdt.PAYMENT_DETAIL_ID,pdt.PAY_RECEIVED_ID,pdt.AMOUNT,pdt.PAYMENT_TYPE,pdt.STATUS,pdt.PAY_DATE,pdt.customer_id, pdt.shop_id, 1,");
		sql.append(" pdt.CREATE_USER,pdt.UPDATE_USER,pdt.CREATE_DATE,pdt.UPDATE_DATE,");
		sql.append(" dd.debit_detail_id,");
		sql.append(" pdt.DISCOUNT,pdt.TIME,pdt.STAFF_ID");
		sql.append(" from payment_detail_temp pdt ");
		sql.append(" join debit_detail dd on (dd.from_object_id = pdt.FOR_OBJECT_ID)");
		sql.append(" where pdt.pay_received_id in (-1 ");
		if (lstPayReceived != null) {
			for (Long id : lstPayReceived) {
				sql.append(",?");
				params.add(id);
			}
		}
		sql.append(" ) ");
		sql.append(" and not exists (select 1 from payment_detail where PAYMENT_DETAIL_ID=pdt.PAYMENT_DETAIL_ID) ");
		repo.executeSQLQuery(sql.toString(), params);
	}

	@Override
	public List<SaleOrderVO> getListDiscountAndAmount(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		//sql.append(" select pdt.customer_id customerId,dd.from_object_id saleOrderId,");
		sql.append(" select pdt.customer_id customerId, pdt.for_object_id saleOrderId,");
		sql.append(" sum(nvl(pdt.discount,0)) discount, sum(nvl(pdt.amount,0)) amount,");
		sql.append(" prt.shop_id as shopId, prt.staff_id as staffId,");
		sql.append(" (select order_number from sale_order where sale_order_id = pdt.for_object_id) as orderNumber");
		sql.append(" from payment_detail_temp pdt ");
		//sql.append(" join debit_detail dd on dd.debit_detail_id=pdt.debit_detail_id ");
		sql.append(" join pay_received_temp prt on (prt.pay_received_id = pdt.pay_received_id)");
		sql.append(" where 1=1 ");
		if (filter.getLstPayReceivedId() != null && filter.getLstPayReceivedId().size() > 0) {
			sql.append(" and pdt.pay_received_id in (-1");
			for (Long payId : filter.getLstPayReceivedId()) {
				sql.append(",?");
				params.add(payId);
			}
			sql.append(")");
		}
		if (filter.getLstSaleOrderId() != null) {
			//sql.append(" and dd.from_object_id in (-1");
			sql.append(" and pdt.for_object_id in (-1");
			for (Long id : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(id);
			}
			sql.append(")");
		}
		sql.append(" and not exists (select 1 from payment_detail where PAYMENT_DETAIL_ID=pdt.PAYMENT_DETAIL_ID) ");
		sql.append(" group by pdt.customer_id, pdt.for_object_id, prt.shop_id, prt.staff_id ");

		String[] fieldNames = new String[] { "customerId", "saleOrderId", "discount", "amount", "shopId", "staffId", "orderNumber" };
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING };

		return repo.getListByQueryAndScalar(SaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<SaleOrder> getListSaleOrderForPrint(SoFilter filter) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from SALE_ORDER so where 1 = 1");
		sql.append(" and so.approved = 0 and so.approved_step = 2 ");

		sql.append(" order by order_date desc");
		if (filter.getkPaging() == null)
			return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, filter.getkPaging());
	}

	@Override
	public List<SaleOrderVO> getListSaleOrderAIADVOByFilter(SaleOrderFilter<SaleOrderVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append("   select  ");
		sql.append("   so.sale_order_id as saleOrderId ");
		sql.append("   ,so.order_type as orderType ");
		sql.append("   ,so.order_number as orderNumber ");
		sql.append("   ,so.approved as approved ");
		sql.append("   ,so.amount as amount ");
		sql.append("   ,to_char(so.order_date, 'dd/MM/yyyy') as orderDateStr ");
		sql.append("   ,(select so1.order_number from sale_order so1 where so.from_sale_order_id = so1.sale_order_id) as isJoinOderNumber ");
		sql.append("   ,(select shop_code from shop s2 where s2.shop_id =  so.shop_id) as shopCode ");
		fromSql.append(" from sale_order so ");
		fromSql.append("  where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getSaleOderNumber())) {
			fromSql.append(" and lower(so.order_number) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getSaleOderNumber().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getIsJoinSaleOderNumber())) {
			fromSql.append("  and so.from_sale_order_id in (select so1.sale_order_id as from_sale_order_id from sale_order so1 where lower(so1.order_number) like ? ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getIsJoinSaleOderNumber().trim().toLowerCase()));
		}
		if (filter.getStatus() != null) {
			fromSql.append(" and so.approved = ? ");
			params.add(filter.getStatus());
		} else {
			fromSql.append(" and so.approved in (0, 1) ");
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderTypeStr())) {
			fromSql.append(" and lower(so.order_type) like ? ESCAPE '/' ");
			params.add(filter.getOrderTypeStr().trim().toLowerCase());
		} else {
			fromSql.append(" and (lower(so.order_type) like ? or lower(so.order_type) like ? ESCAPE '/') ");
			params.add(OrderType.AI.getValue().toLowerCase());
			params.add(OrderType.AD.getValue().toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			fromSql.append("  and so.shop_id in (select shop_id from shop s1 where s1.status = 1 and lower(s1.shop_code) like ? ESCAPE '/' ) ");
			params.add(filter.getShopCode().trim().toLowerCase());
		}
		if (filter.getFromDate() != null) {
			fromSql.append(" and so.order_date >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			fromSql.append(" and so.order_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		//Append Chuoi From and Where
		sql.append(fromSql.toString());
		sql.append(" order by orderNumber asc, orderType desc ");
		//Count total
		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());
		final String[] fieldNames = new String[] { "saleOrderId", "orderType", "orderNumber", "approved", "amount", "orderDateStr", "isJoinOderNumber", "shopCode" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.STRING };
		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(SaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(SaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}

	@Override
	public List<SaleOrderPromotionVO> getListSOPromotionBySOId(Long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select sop.PROMOTION_PROGRAM_ID promotionId, sop.PROMOTION_PROGRAM_CODE promotionCode, sum(sop.QUANTITY_RECEIVED) quantity ");
		sql.append("from sale_order_promotion sop ");
		sql.append("where 1 = 1 ");
		sql.append("and sop.SALE_ORDER_ID = ? ");
		sql.append("group by sop.PROMOTION_PROGRAM_ID, sop.PROMOTION_PROGRAM_CODE");
		params.add(saleOrderId);

		final String[] fieldNames = new String[] { "promotionId", "promotionCode", "quantity" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };

		return repo.getListByQueryAndScalar(SaleOrderPromotionVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public PromotionProgram checkQuantityReceived(Long promotionId, Integer quantity, Long shopId, Long staffId, Long customerId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT pp.* ");
		sql.append("FROM promotion_program pp ");
		sql.append("WHERE 1 = 1 ");
		sql.append("AND pp.promotion_program_id = ? ");
		params.add(promotionId);
		sql.append("AND pp.promotion_program_id IN ");
		sql.append("(SELECT promotion_program_id ");
		sql.append("FROM promotion_shop_map psm ");
		sql.append("WHERE 1 = 1 ");
		sql.append("and psm.shop_id IN (select shop_id from shop start with shop_id = ? connect by prior parent_shop_id = shop_id) ");
		params.add(shopId);
		sql.append("AND pp.promotion_program_id = psm.promotion_program_id ");
		sql.append("and ( ");
		sql.append("(psm.quantity_max IS NOT NULL AND nvl(psm.quantity_received, 0) + ? > psm.quantity_max) ");
		params.add(quantity);
		sql.append("OR ");
		sql.append("(psm.promotion_shop_map_id IN ( ");
		sql.append("select pcm.promotion_shop_map_id from promotion_customer_map pcm ");
		sql.append("where pcm.customer_id = ? and pcm.promotion_shop_map_id = psm.promotion_shop_map_id ");
		params.add(customerId);
		sql.append("and pcm.quantity_max is not null and nvl(pcm.quantity_received, 0) + ? > pcm.quantity_max ");
		params.add(quantity);
		sql.append(") ");
		sql.append(") ");
		sql.append("OR ");
		sql.append("(psm.promotion_shop_map_id IN ( ");
		sql.append("select pstm.promotion_shop_map_id from promotion_staff_map pstm ");
		sql.append("where pstm.staff_id = ? and pstm.promotion_shop_map_id = psm.promotion_shop_map_id ");
		params.add(staffId);
		sql.append("and pstm.quantity_max is not null and nvl(pstm.quantity_received, 0) + ? > pstm.quantity_max ");
		params.add(quantity);
		sql.append(") ");
		sql.append(") ");
		sql.append(") ");
		sql.append(")");
		return repo.getEntityBySQL(PromotionProgram.class, sql.toString(), params);
	}

	@Override
	public List<PrintOrderVO> getListPrintOrderVO(PrintOrderFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (OrderType.GO.equals(filter.getOrderType()) || OrderType.DP.equals(filter.getOrderType())) {
			sql.append(" SELECT * FROM ( ");
			sql.append(" select stt.STOCK_TRANS_ID id, stt.SHOP_ID shopId, sh.shop_code shopCode, to_char(stt.stock_trans_date, 'dd/mm/yyyy hh24:mi:ss') createDate, ");
			sql.append(" stt.TOTAL_AMOUNT amount, stt.STOCK_TRANS_CODE refOrder, ");
			sql.append(" (select (case when stl.from_owner_type = ? ");
			params.add(StockTransLotOwnerType.STAFF.getValue());
			sql.append("  	then (select staff_code ||'-'|| staff_name from staff where staff_id = stl.from_owner_id)  ");
			sql.append(" 	else (select staff_code ||'-'|| staff_name from staff where staff_id = stl.to_owner_id)  end)  ");
			sql.append(" from stock_trans_lot  stl where stl.stock_trans_id=stt.stock_trans_id and rownum=1) staffCode, ");
			sql.append(" (SELECT CASE ");
			sql.append("     WHEN stl.from_owner_type = ? THEN stl.from_owner_id ");
			params.add(StockTransLotOwnerType.STAFF.getValue());
			sql.append("     WHEN stl.to_owner_type = ? THEN stl.to_owner_id ");
			params.add(StockTransLotOwnerType.STAFF.getValue());
			sql.append("     ELSE stt.staff_id ");
			sql.append("   END ");
			sql.append(" FROM stock_trans_lot stl WHERE stl.stock_trans_id = stt.stock_trans_id AND rownum =1 ");
			sql.append(" ) staffId, ");
			sql.append(" ? as orderType ");
			params.add(filter.getOrderType().getValue());
			sql.append(" from stock_trans stt inner join shop sh on stt.shop_id = sh.shop_id where 1 = 1 ");
			if (filter.getShopId() != null) {
				sql.append("and stt.shop_id = ? ");
				params.add(filter.getShopId());
			}
			if (filter.getFromDate() != null) {
				sql.append("and stt.stock_trans_date >= trunc(?) ");
				params.add(filter.getFromDate());
			}
			if (filter.getToDate() != null) {
				sql.append("and stt.stock_trans_date < trunc(?) + 1 ");
				params.add(filter.getToDate());
			}
			if (filter.getOrderType() != null) {
				sql.append("and stt.TRANS_TYPE = ? ");
				params.add(filter.getOrderType().getValue());
			}
			sql.append("and stt.APPROVED = ? ");
			params.add(0);
			sql.append("and stt.APPROVED_STEP = ? ");
			params.add(1);
			if(filter.getIsValueOrder()!=null && filter.getNumberValueOrder() != null){
				if( filter.getIsValueOrder() == 0 ){
					sql.append(" and stt.total_amount < ? ");
					params.add(filter.getNumberValueOrder());
				}
				if( filter.getIsValueOrder() == 1 ){
					sql.append(" and stt.total_amount >= ? ");
					params.add(filter.getNumberValueOrder());
				}
			}
			sql.append(" ) ");
			sql.append(" WHERE 1 = 1 ");
			if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
				String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "staffId");
				sql.append(paramsFix);
			}
			
			final String[] fieldNames = new String[] { "id", "shopId", "shopCode", "createDate", "amount", "refOrder","staffCode", "orderType"};

			final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING,  StandardBasicTypes.STRING, StandardBasicTypes.STRING };
			if (filter.getkPaging() == null) {
				sql.append(" order by refOrder");
				return repo.getListByQueryAndScalar(PrintOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
			} else {
				String sqlCount = "select count(1) as count from (" + sql.toString() + ")";
				sql.append(" order by refOrder");
				return repo.getListByQueryAndScalarPaginated(PrintOrderVO.class, fieldNames, fieldTypes, sql.toString(), sqlCount, params, params, filter.getkPaging());
			}
		} else {
			sql.append("select so.SALE_ORDER_ID id, so.shop_id shopId, sh.shop_code shopCode, so.order_type orderType, to_char(so.ORDER_DATE, 'dd/mm/yyyy hh24:mi:ss') createDate, to_char(so.DELIVERY_DATE, 'dd/mm/yyyy') deliveryDate, ");
			sql.append("app.AP_PARAM_NAME priority, c.short_code shortCode, c.customer_name customerName, c.ADDRESS address, so.total amount, so.quantity quantity, (select sum(discount_amount) from sale_order_promo_detail where is_owner = 2 and sale_order_id = so.sale_order_id) discount, (select sum(discount_amount) from sale_order_promo_detail where is_owner in (0,1) and sale_order_id = so.sale_order_id) discountAmount, ");
			sql.append("so.REF_ORDER_NUMBER refOrder, nvbh.staff_code staffCode, nvbh.staff_name staffName, nvgh.staff_code deliveryCode, nvgh.staff_name deliveryName ");
			sql.append("from sale_order so inner join shop sh on so.shop_id = sh.shop_id left join customer c on so.customer_id = c.customer_id left join staff nvbh on so.staff_id = nvbh.staff_id left join staff nvgh on so.DELIVERY_ID = nvgh.staff_id ");
			sql.append("left join ap_param app on so.PRIORITY = app.AP_PARAM_ID  where 1 = 1 ");
			if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
				String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "so.staff_id");
				sql.append(paramsFix);
			}
			if (filter.getShopId() != null) {
				sql.append("and so.shop_id = ? ");
				params.add(filter.getShopId());
			}
			if (filter.getFromDate() != null) {
				sql.append("and so.order_date >= trunc(?) ");
				params.add(filter.getFromDate());
			}
			if (filter.getToDate() != null) {
				sql.append("and so.order_date < trunc(?) + 1 ");
				params.add(filter.getToDate());
			}
			if (filter.getOrderType() != null) {
				sql.append("and so.ORDER_TYPE = ? ");
				params.add(filter.getOrderType().getValue());
			}
			if (!StringUtility.isNullOrEmpty(filter.getRefOrderNumber())) {
				sql.append("and lower(so.REF_ORDER_NUMBER) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getRefOrderNumber().trim().toLowerCase()));
			}
			if (filter.getOrderSource() != null) {
				sql.append("and so.ORDER_SOURCE = ? ");
				params.add(filter.getOrderSource().getValue());
			}
			if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
				sql.append("and lower(c.short_code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().trim().toLowerCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getCustomeName())) {
				sql.append("and lower(c.customer_name) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomeName().trim().toLowerCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
				sql.append("and lower(nvbh.staff_code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().trim().toLowerCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
				sql.append("and lower(nvgh.staff_code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDeliveryCode().trim().toLowerCase()));
			}
			sql.append("and so.APPROVED = ? ");
			params.add(0);
			sql.append("and so.APPROVED_STEP = ? ");
			params.add(1);
			if(filter.getIsValueOrder()!=null && filter.getNumberValueOrder() != null){
				if( filter.getIsValueOrder() == 0 ){
					sql.append(" and so.amount < ? ");
					params.add(filter.getNumberValueOrder());
				}
				if( filter.getIsValueOrder() == 1 ){
					sql.append(" and so.amount >= ? ");
					params.add(filter.getNumberValueOrder());
				}
			}
			final String[] fieldNames = new String[] { "id", "shopId", "shopCode", "orderType", "createDate", "deliveryDate", "priority", "shortCode", "customerName", "address", "amount", "quantity", "discount", "discountAmount", "refOrder", "staffCode", "staffName",
					"deliveryCode", "deliveryName" };

			final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
					StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
					StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
			if (filter.getkPaging() == null) {
				sql.append(" order by refOrder");
				return repo.getListByQueryAndScalar(PrintOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
			} else {
				String sqlCount = "select count(1) as count from (" + sql.toString() + ")";
				sql.append(" order by refOrder");
				return repo.getListByQueryAndScalarPaginated(PrintOrderVO.class, fieldNames, fieldTypes, sql.toString(), sqlCount, params, params, filter.getkPaging());
			}
		}
	}

	@Override
	public List<SaleOrderDetailVOEx> getListSaleOrderDetailVOByFilter(SaleOrderDetailFilter<SaleOrderDetailVOEx> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append("   select sod.sale_order_detail_id as id ");
		sql.append("   ,sod.price as price ");
		sql.append("   ,sod.quantity as quantity ");
		sql.append("   ,sod.amount as amount ");
		sql.append("   , p.product_id as productId ");
		sql.append("   , p.product_code as productCode ");
		sql.append("   , p.product_name as productName ");
		sql.append("   , 1 as isAttr ");
		fromSql.append(" from sale_order_detail sod join product p on sod.product_id  = p.product_id ");
		fromSql.append(" where 1 = 1 ");
		if (filter.getSaleOrderId() != null) {
			fromSql.append(" and sod.sale_order_id = ? ");
			params.add(filter.getSaleOrderId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			fromSql.append(" and lower(p.product_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProductCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			fromSql.append(" and lower(p.name_text) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getProductName()).toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			fromSql.append(" and sod.sale_order_id = (select max(sale_order_id) as sale_order_id from sale_order where lower(order_number) like ? ESCAPE '/'  ");
			params.add(filter.getOrderNumber().trim().toLowerCase());
			if (filter.getStatus() != null) {
				fromSql.append(" and approved = ?  ");
				params.add(filter.getStatus());
			}
			if (filter.getType() != null) {
				fromSql.append(" and type = ?  ");
				params.add(filter.getStatus());
			}
			//			if(filter.getIsAttr()!=null && SaleOrderIsAttr.AIORAD.getValue().equals(filter.getIsAttr())){
			//				fromSql.append(" and order_type in ('SO', 'IN')  ");
			//			}
			fromSql.append(" ) ");
		}
		if (filter.getIsFreeItem() != null) {
			fromSql.append(" and is_free_item = ?  ");
			params.add(filter.getIsFreeItem());
		}
		if (filter.getArrStr() != null && !filter.getArrStr().isEmpty() && filter.getIsArrLongId()) {
			fromSql.append(" and sod.product_id in (select product_id from product where product_code in('-1' ");
			for (String value : filter.getArrStr()) {
				fromSql.append(" ,? ");
				params.add(value.trim());
			}
			fromSql.append(" )) ");
		} else if (filter.getArrStr() != null && !filter.getArrStr().isEmpty()) {
			fromSql.append(" and sod.product_id not in (select product_id from product where product_code in('-1' ");
			for (String value : filter.getArrStr()) {
				fromSql.append(" ,? ");
				params.add(value.trim());
			}
			fromSql.append(" )) ");
		}

		//Append Chuoi From and Where
		sql.append(fromSql.toString());
		sql.append(" order by productCode  ");
		//Count total
		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());
		final String[] fieldNames = new String[] { "id", "price", "quantity", "amount", "productId", "productCode", "productName", "isAttr" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER };
		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(SaleOrderDetailVOEx.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(SaleOrderDetailVOEx.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}

	@Override
	public SaleOrderVO getSaleOrderVOSingle(SaleOrderFilter<SaleOrderVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("   select  ");
		sql.append("   so.sale_order_id as saleOrderId ");
		sql.append("   ,so.order_type as orderType ");
		sql.append("   ,so.order_number as orderNumber ");
		sql.append("   ,so.approved as approved ");
		sql.append("   ,so.amount as amount ");
		sql.append("   ,to_char(so.order_date, 'dd/MM/yyyy') as orderDateStr ");
		sql.append("   ,(select so1.order_number from sale_order so1 where so.from_sale_order_id = so1.sale_order_id) as isJoinOderNumber ");
		/**
		 * sql.append(
		 * "   ,(select shop_code from shop s2 where s2.shop_id =  so.shop_id) as shopCode "
		 * ); @author hunglm16 @description Tam thoi de an
		 **/
		sql.append(" from sale_order so ");
		sql.append("  where 1 = 1 ");
		if (filter.getSaleOrderId() != null) {
			sql.append(" so.sale_order_id = ? ");
			params.add(filter.getSaleOrderId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderTypeStr())) {
			sql.append(" and lower(so.order_number) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderTypeStr().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getIsJoinSaleOderNumber())) {
			sql.append("  and so.from_sale_order_id in (select so1.sale_order_id as from_sale_order_id from sale_order so1 where lower(so1.order_number) like ? ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getIsJoinSaleOderNumber().trim().toLowerCase()));
		}
		if (filter.getStatus() != null) {
			sql.append(" and so.approved = ? ");
			params.add(filter.getStatus());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderTypeStr())) {
			sql.append(" and lower(so.order_type) like ? ");
			params.add(filter.getOrderTypeStr().trim().toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("  and so.shop_id in (select shop_id from shop s1 where s1.status = 1 and lower(s1.shop_code) like ? ) ");
			params.add(filter.getShopCode().trim().toLowerCase());
		}
		if (filter.getFromDate() != null) {
			sql.append(" and so.order_date >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and so.order_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		final String[] fieldNames = new String[] { "saleOrderId", "orderType", "orderNumber", "approved", "amount", "orderDateStr", "isJoinOderNumber" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.STRING };
		List<SaleOrderVO> lstData = new ArrayList<SaleOrderVO>();
		lstData = repo.getListByQueryAndScalar(SaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (lstData != null && !lstData.isEmpty()) {
			return lstData.get(0);
		}
		return new SaleOrderVO();

	}

	/**
	 * @author lacnv1 orderTypeGroup = 1 ->(order_type = IN, SO, CM, CO;
	 *         approved = 1, approved_step = 3) orderTypeGroup = 2 ->(order_type
	 *         = AI, AD; approved = 1, approved_step = 2) orderTypeGroup = 3
	 *         ->(trans_type = DCT, DCG; approved = 1, approved_step = 3)
	 */
	@Override
	public Integer countSaleOrderApprovedYet(long shopId, Date date, int orderTypeGroup) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		if (orderTypeGroup < 3) {
			sql.append("select count(1) as count");
			sql.append(" from sale_order");
			sql.append(" where shop_id = ?");
			params.add(shopId);
			if (1 == orderTypeGroup) {
				sql.append(" and order_type in ('IN', 'SO', 'CM', 'CO')");
				//sql.append(" and (approved <> 1 or approved_step <> 3)");
				sql.append(" and (approved = 0 or (approved = 1 and approved_step = 2))");
			} else {
				sql.append(" and order_type in ('AI', 'AD')");
				sql.append(" and (approved = 0 or (approved = 1 and approved_step < 2))");
			}
			if (date != null) {
				sql.append(" and order_date >= trunc(?)");
				params.add(date);
				sql.append(" and order_date < trunc(?) + 1");
				params.add(date);
			}
		} else {
			sql.append("select count(1) as count");
			sql.append(" from stock_trans");
			sql.append(" where shop_id = ?");
			params.add(shopId);
			sql.append(" and trans_type in ('DCT', 'DCG', 'DC')");
			sql.append(" and (approved = 0 or (approved = 1 and approved_step < 3))");
			if (date != null) {
				sql.append(" and stock_trans_date >= trunc(?)");
				params.add(date);
				sql.append(" and stock_trans_date < trunc(?) + 1");
				params.add(date);
			}
		}

		return repo.countBySQL(sql.toString(), params);
	}
	
	@Override
	public List<BasicVO> countAndSumSaleOrderApprovedYet(long shopId, Date date, int orderTypeGroup) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		if (orderTypeGroup < OrderTypeGroup.B3.getValue()) {
			sql.append("select count(1) as count, sum (amount) as amount");
			sql.append(" from sale_order");
			sql.append(" where shop_id = ?");
			params.add(shopId);
			if (OrderTypeGroup.B1.getValue() == orderTypeGroup) {
				sql.append(" and order_type in (?, ?, ?, ?)");
				params.add(OrderType.IN.getValue());
				params.add(OrderType.SO.getValue());
				params.add(OrderType.CM.getValue());
				params.add(OrderType.CO.getValue());
				//sql.append(" and (approved <> 1 or approved_step <> 3)");
				sql.append(" and approved in (?, ?) and approved_step < ?");
				params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
				params.add(SaleOrderStatus.APPROVED.getValue());
				params.add(SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT.getValue());
			} else {
				sql.append(" and order_type in (?, ?)");
				params.add(OrderType.AI.getValue());
				params.add(OrderType.AD.getValue());
//				sql.append(" and (approved = 0 or (approved = 1 and approved_step < 2))");
				sql.append(" and approved = ?");
				params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
//				sql.append(SaleOrderStatus.APPROVED.getValue());
//				sql.append(SaleOrderStep.PRINT_CONFIRMED.getValue());
			}
			if (date != null) {
				sql.append(" and order_date >= trunc(?)");
				params.add(date);
				sql.append(" and order_date < trunc(?) + 1");
				params.add(date);
			}
		} else {
			sql.append("select count(1) as count, sum (total_amount) as amount ");
			sql.append(" from stock_trans");
			sql.append(" where shop_id = ?");
			params.add(shopId);
			sql.append(" and trans_type in (?, ?, ?, ?, ?)");
			params.add(OrderType.DCT.getValue());
			params.add(OrderType.DCG.getValue());
			params.add(OrderType.DC.getValue());
			params.add(OrderType.DP.getValue());
			params.add(OrderType.GO.getValue());
			sql.append(" and approved in (?, ?) and approved_step < ?");
			params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
			params.add(SaleOrderStatus.APPROVED.getValue());
			params.add(SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT.getValue());
			if (date != null) {
				sql.append(" and stock_trans_date >= trunc(?)");
				params.add(date);
				sql.append(" and stock_trans_date < trunc(?) + 1");
				params.add(date);
			}
		}
		/*
		if (orderTypeGroup < OrderTypeGroup.B3.getValue()) {
			sql.append("select count(1) as count, sum (amount) as amount ");
			sql.append(" from sale_order");
			sql.append(" where shop_id = ?");
			params.add(shopId);
			if (OrderTypeGroup.B1.getValue() == orderTypeGroup) {
				sql.append(" and order_type in ('IN', 'SO', 'CM', 'CO')");
				sql.append(" and (approved = 0 or (approved = 1 and approved_step = 2))");
			} else {
				sql.append(" and order_type in ('AI', 'AD')");
				sql.append(" and (approved = 0 or (approved = 1 and approved_step < 2))");
			}
			if (date != null) {
				sql.append(" and order_date >= trunc(?)");
				params.add(date);
				sql.append(" and order_date < trunc(?) + 1");
				params.add(date);
			}
		} else {
			sql.append("select count(1) as count, sum (total_amount) as amount ");
			sql.append(" from stock_trans");
			sql.append(" where shop_id = ?");
			params.add(shopId);
			sql.append(" and trans_type in ('DCT', 'DCG', 'DC')");
			sql.append(" and (approved = 0 or (approved = 1 and approved_step < 3))");
			if (date != null) {
				sql.append(" and stock_trans_date >= trunc(?)");
				params.add(date);
				sql.append(" and stock_trans_date < trunc(?) + 1");
				params.add(date);
			}
		}
		*/
		final String[] fieldNames = new String[] { "count", "amount" };
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL };
		return repo.getListByQueryAndScalar(BasicVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<SaleOrder> getListSaleOrderExByFilter(SaleOrderFilter<SaleOrder> filter, String orderBy) throws DataAccessException {
		/** Cai tien bang truy van long @author hunglm16 **/
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("  select * from sale_order so ");
		sql.append("  where 1 = 1 ");
		if (filter.getSaleOrderId() != null) {
			sql.append(" and so.sale_order_id = ? ");
			params.add(filter.getSaleOrderId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and lower(so.order_number) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getIsJoinSaleOderNumber())) {
			sql.append("  and so.from_sale_order_id in (select so1.sale_order_id as from_sale_order_id from sale_order so1 where lower(so1.order_number) like ? ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getIsJoinSaleOderNumber().trim().toLowerCase()));
		}
		if (filter.getStatus() != null) {
			sql.append(" and so.approved = ? ");
			params.add(filter.getStatus());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderTypeStr())) {
			sql.append(" and lower(so.order_type) like ? ");
			params.add(filter.getOrderTypeStr().trim().toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and so.staff_id in (select staff_id from staff where status = 1 and lower(staff_code) like ? ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
			sql.append(" and so.delivery_id in (select staff_id as delivery_id from staff where status = 1 and lower(staff_code) like ? ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getDeliveryCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomerName()) || !StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and so.customer_id in (select customer_id from customer where status = 1   ");
			if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
				sql.append("and lower(short_code) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShortCode().trim().toLowerCase()));
			}
			if (!StringUtility.isNullOrEmpty(filter.getCustomerName())) {
				sql.append(" and lower(customer_name) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerName().trim().toLowerCase()));
			}
			sql.append(" ) ");
		}
		if (filter.getType() != null) {
			sql.append(" and so.type = ? ");
			params.add(filter.getType());
		}
		if (filter.getOrderType() != null) {
			sql.append(" and so.order_type like ? ESCAPE '/' ");
			params.add(filter.getOrderType().getValue());
		}
		if (filter.getShopId() != null) {
			sql.append(" and so.shop_id = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getApproved() != null) {
			sql.append(" and so.approved = ? ");
			params.add(filter.getApproved().getValue());
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("  and so.shop_id in (select shop_id from shop s1 where s1.status = 1 and lower(s1.shop_code) like ?  ESCAPE '/' ) ");
			params.add(filter.getShopCode().trim().toLowerCase());
		}
		if (filter.getFromDate() != null) {
			sql.append(" and so.order_date >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and so.order_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if (!StringUtility.isNullOrEmpty(orderBy)) {
			sql.append(orderBy.trim());
		} else {
			sql.append(" order by so.order_date desc ");
		}
		if (filter.getkPaging() == null) {
			return repo.getListBySQL(SaleOrder.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(SaleOrder.class, sql.toString(), params, filter.getkPaging());
		}
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrder> getListReturnOrderByOrderId(long soId, SaleOrderStatus approved, SaleOrderStep approvedStep) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select so.* from sale_order so");
		sql.append(" where type = ?");
		params.add(SaleOrderType.RETURNED_ORDER.getValue());
		if (approved != null) {
			sql.append(" and approved = ?");
			params.add(approved.getValue());
		}
		if (approvedStep != null) {
			sql.append(" and approved_step = ?");
			params.add(approvedStep.getValue());
		}
		sql.append(" and from_sale_order_id = ?");
		params.add(soId);

		List<SaleOrder> lst = repo.getListBySQL(SaleOrder.class, sql.toString(), params);
		return lst;
	}

	@Override
	public List<SaleOrderPromotion> getListSaleOrderPromotionBySaleOrder(Long orderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from sale_order_promotion where sale_order_id = ? ");
		params.add(orderId);
		return repo.getListBySQL(SaleOrderPromotion.class, sql.toString(), params);
	}
	
	@Override
	public List<SaleOrderPromoDetail> getListSaleOrderPromoDetailBySaleOrder(Long orderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from sale_order_promo_detail where sale_order_id = ? ");
		params.add(orderId);
		return repo.getListBySQL(SaleOrderPromoDetail.class, sql.toString(), params);
	}

	@Override
	public List<SaleOrderDetail> getListSaleOrderDetailByFilter(SaleOrderDetailVATFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from sale_order_detail");
		sql.append(" where sale_order_id in (");
		sql.append(" select so.sale_order_id from sale_order so");
		sql.append(" where 1 = 1");
		if (filter.getLstSOId() != null && !filter.getLstSOId().isEmpty()) {
			List<Long> lst = filter.getLstSOId();
			sql.append(" AND so.sale_order_id in (-1");
			for (int i = 0; i < lst.size(); i++) {
				Long soId = lst.get(i);
				sql.append(",?");
				params.add(soId);
			}
			sql.append(")");
		}
		sql.append(" )");
		if (filter.getProductId() != null) {
			sql.append(" and product_id = ?");
			params.add(filter.getProductId());
		}
		if (filter.getVat() != null) {
			sql.append(" and vat = ?");
			params.add(filter.getVat());
		}
		if (filter.getIsFreeItem() != null) {
			sql.append(" and is_free_item = ?");
			params.add(filter.getIsFreeItem());
		}
		return repo.getListBySQL(SaleOrderDetail.class, sql.toString(), params);
	}

	/**
	 * Search list sale order
	 * 
	 * @author nhanlt6
	 * @param SoFilter
	 * @throws BusinessException
	 * @since Nov 18, 2014
	 * @modify_by lacnv1
	 * @modifiy_date Nov 20, 2014
	 */
	@Override
	public List<SaleOrderVOEx2> searchSaleOrder(SoFilter filter) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select id, refOrderNumber, orderDate, deliveryDate,");
		sql.append(" totalValue, orderNumber, customerId,");
		sql.append(" (select short_code from customer where customer_id=soTmp.customerId) as customerCode, ");
		sql.append(" (select customer_name from customer where customer_id=soTmp.customerId) as customerName, ");
		sql.append(" (select address from customer where customer_id=soTmp.customerId) as address, ");
		sql.append(" (select staff_code from staff where staff_id = soTmp.staff_id) as staffCode, ");
		sql.append(" (select staff_name from staff where staff_id = soTmp.staff_id) as staffName, ");
		sql.append(" (select staff_code from staff where staff_id = soTmp.delivery_id) as deliveryCode, ");
		sql.append(" (select staff_name from staff where staff_id = soTmp.delivery_id) as deliveryName, ");
		sql.append(" saleOrderDiscount, totalDiscount,");
		sql.append(" fromSaleOrderNumber, orderType, orderStatus, saleOrderType,priorityStr");
		
		sql.append(" from (");

		// IN, SO, CM, CO
		/*boolean hasSaleOrder = false;
		if (filter.getOrderType() == null
				|| (!OrderType.DP.equals(filter.getOrderType()) && !OrderType.GO.equals(filter.getOrderType()))) {
			hasSaleOrder = true;*/
			
			sql.append(" select so.sale_order_id as id, so.ref_Order_Number as refOrderNumber, so.order_date as orderDate, so.delivery_Date as deliveryDate,  ");
			sql.append(" so.total as totalValue,so.order_number as orderNumber, so.customer_id as customerId, ");
			/*sql.append(" (select short_code from customer where customer_id=so.customer_id) customerCode, ");
			sql.append(" (select customer_name from customer where customer_id=so.customer_id) customerName, ");
			sql.append(" (select address from customer where customer_id=so.customer_id) address, ");*/
			/*sql.append(" (select staff_code from staff where staff_id = so.staff_id) staffCode, ");
			sql.append(" (select staff_name from staff where staff_id = so.staff_id) staffName, ");
			sql.append(" (select staff_code from staff where staff_id = so.delivery_id) deliveryCode, ");
			sql.append(" (select staff_name from staff where staff_id = so.delivery_id) deliveryName, ");*/
			sql.append(" so.staff_id, so.delivery_id,");
			sql.append(" (select nvl(sum(discount_amount),0) from sale_order_promo_detail where sale_order_id=so.sale_order_id and is_owner=2) as saleOrderDiscount, ");
			sql.append(" (select nvl(sum(discount_amount),0) from sale_order_promo_detail where sale_order_id=so.sale_order_id and is_owner in (0,1)) as totalDiscount,");
			sql.append(" (select order_number from sale_order where sale_order_id = so.from_sale_order_id) as fromSaleOrderNumber,");
			sql.append(" so.order_type as orderType, so.approved as orderStatus, so.type as saleOrderType ");
			sql.append(", (select ap_param_name from ap_param where ap_param_id = so.priority) as priorityStr");
			sql.append(" from sale_order so ");
			//sql.append(" left join sale_order so2 on so.from_sale_order_id = so2.sale_order_id");
			sql.append(" where 1=1 ");
			
			if (filter.getApproval() != null) {
				sql.append(" and so.approved = ? ");
				params.add(filter.getApproval().getValue());
			}
			String s = null;
			if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
				sql.append(" and upper(so.order_number) like ? escape '/' ");
				s = filter.getOrderNumber().toUpperCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (!StringUtility.isNullOrEmpty(filter.getRefOrderNumber())) {
				sql.append(" and upper(so.ref_order_number) like ? escape '/' ");
				s = filter.getRefOrderNumber().toUpperCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (filter.getFromDate() != null) {
				sql.append(" and so.order_date >=trunc(?) ");
				params.add(filter.getFromDate());
			}
			if (filter.getToDate() != null) {
				sql.append(" and so.order_date < trunc(?) + 1 ");
				params.add(filter.getToDate());
			}
			if (filter.getDeliveryDate() != null) {
				sql.append(" and so.delivery_date >= trunc(?) and so.delivery_date < trunc(?) + 1 ");
				params.add(filter.getDeliveryDate());
				params.add(filter.getDeliveryDate());
			}
			if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
				sql.append(" and so.staff_id in (select staff_id from staff where upper(staff_code) like ? escape '/' ) ");
				s = filter.getStaffCode().toUpperCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
				sql.append(" and so.delivery_id in (select staff_id from staff where upper(staff_code) like ? escape '/' ) ");
				s = filter.getDeliveryCode().toUpperCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
				sql.append(" and so.customer_id in (select customer_id from customer where upper(short_code) like ? escape '/' ) ");
				s = filter.getShortCode().toUpperCase();
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (!StringUtility.isNullOrEmpty(filter.getCustomeName())) {
				sql.append(" and so.customer_id in (select customer_id from customer where upper(name_text) like ? escape '/' ) ");
				s = filter.getCustomeName().toUpperCase();
				s = Unicode2English.codau2khongdau(s);
				s = StringUtility.toOracleSearchLike(s);
				params.add(s);
			}
			if (filter.getShopId() != null) {
				sql.append(" and so.shop_id = ?");
				params.add(filter.getShopId());
			}
			if (filter.getOrderType() != null) {
				sql.append(" and so.order_type = ? ");
				params.add(filter.getOrderType().getValue());
			} else {
				sql.append(" and so.order_type not in (?, ?) ");
				params.add(OrderType.AI.getValue());
				params.add(OrderType.AD.getValue());
			}
			if (filter.getSaleOrderType() != null) {
				sql.append(" and so.type = ? ");
				params.add(filter.getSaleOrderType().getValue());
			} else {
				sql.append(" and so.type IN (?,?) ");
				params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
				params.add(SaleOrderType.RETURNED_ORDER.getValue());
			}
			if (filter.getOrderSource() != null) {
				sql.append(" and so.ORDER_SOURCE = ? ");
				params.add(filter.getOrderSource().getValue());
			}
			if (!StringUtility.isNullOrEmpty(filter.getPriorityCode())) {
				sql.append(" and so.priority = (select ap_param_id from ap_param where status = 1 and ap_param_code = ? and type = 'ORDER_PIRITY' and rownum = 1) ");
				params.add(filter.getPriorityCode());
			}
			
		//}

		// DP, GO
		/*
		 * if (filter.getOrderType() == null ||
		 * OrderType.DP.equals(filter.getOrderType()) ||
		 * OrderType.GO.equals(filter.getOrderType())) { if (hasSaleOrder) {
		 * sql.append(" union all"); } sql.append(
		 * " select so.stock_trans_id as id, null as refOrderNumber, so.stock_trans_date as orderDate, null as deliveryDate,"
		 * ); sql.append(
		 * " so.total_amount as totalValue, so.stock_trans_code as orderNumber, 0 as customerId,"
		 * ); sql.append(" so.staff_id, 0 as delivery_id,");
		 * sql.append(" null as saleOrderDiscount,");
		 * sql.append(" null as totalDiscount,"); sql.append(
		 * " null as fromSaleOrderNumber, so.trans_type as orderType, so.approved as orderStatus"
		 * ); sql.append(" from stock_trans so"); sql.append(" where 1=1 ");
		 * 
		 * if (filter.getApproval() != null) {
		 * sql.append(" and so.approved = ? ");
		 * params.add(filter.getApproval().getValue()); } String s = null; if
		 * (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
		 * sql.append(" and so.stock_trans_code like ? escape '/' "); s =
		 * filter.getOrderNumber().toUpperCase(); s =
		 * StringUtility.toOracleSearchLike(s); params.add(s); } if
		 * (!StringUtility.isNullOrEmpty(filter.getRefOrderNumber())) {
		 * sql.append(" and 1=0"); } if (filter.getFromDate() != null) {
		 * sql.append(" and so.stock_trans_date >=trunc(?) ");
		 * params.add(filter.getFromDate()); } if (filter.getToDate() != null) {
		 * sql.append(" and so.stock_trans_date < trunc(?) + 1 ");
		 * params.add(filter.getToDate()); } if
		 * (!StringUtility.isNullOrEmpty(filter.getStaffCode())) { sql.append(
		 * " and so.staff_id in (select staff_id from staff where upper(staff_code) like ? escape '/' ) "
		 * ); s = filter.getStaffCode().toUpperCase(); s =
		 * StringUtility.toOracleSearchLike(s); params.add(s); } if
		 * (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
		 * sql.append(
		 * " and so.delivery_id in (select staff_id from staff where upper(staff_code) like ? escape '/' ) "
		 * ); s = filter.getDeliveryCode().toUpperCase(); s =
		 * StringUtility.toOracleSearchLike(s); params.add(s); } if
		 * (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
		 * sql.append(" and 1=0"); } if
		 * (!StringUtility.isNullOrEmpty(filter.getCustomeName())) {
		 * sql.append(" and 1=0"); } if (filter.getShopId() != null) {
		 * sql.append(" and so.shop_id = ?"); params.add(filter.getShopId()); }
		 * if (filter.getOrderType() != null) {
		 * sql.append(" and so.trans_type = ? ");
		 * params.add(filter.getOrderType().getValue()); } if
		 * (filter.getOrderSource() != null &&
		 * !SaleOrderSource.WEB.equals(filter.getOrderSource())) {
		 * sql.append(" and 1=0"); } }
		 */

		sql.append(") soTmp ");
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql.toString());
		countSql.append(")");
		sql.append(" order by orderNumber, orderDate");

		final String[] fieldNames = new String[] { "id", "refOrderNumber", "orderDate", "deliveryDate", "totalValue", "orderNumber", "customerId", "customerCode", "customerName", "address", "staffCode", "staffName", "deliveryCode", "deliveryName",
				"orderType", "orderStatus", "saleOrderDiscount", "totalDiscount", "fromSaleOrderNumber", "saleOrderType", "priorityStr" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.LONG,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING };

		if (filter.getkPagingVOEx2() != null) {
			return repo.getListByQueryAndScalarPaginated(SaleOrderVOEx2.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVOEx2());
		}
		return repo.getListByQueryAndScalar(SaleOrderVOEx2.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	/**
	 * Search list sale order
	 * 
	 * @author nhanlt6
	 * @param SoFilter
	 * @throws BusinessException
	 * @since Nov 18, 2014
	 * @modify_by lacnv1
	 * @modifiy_date Nov 20, 2014
	 */
	@Override
	public List<SaleOrderVOEx2> searchStockTrans(SoFilter filter) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder sqlTrans = new StringBuilder();
		List<Object> paramsTrans = new ArrayList<Object>();
		sql.append("select id, refOrderNumber, orderDate, deliveryDate,");
		sql.append(" totalValue, orderNumber, customerId,");
		sql.append(" (select short_code from customer where customer_id=soTmp.customerId) as customerCode, ");
		sql.append(" (select customer_name from customer where customer_id=soTmp.customerId) as customerName, ");
		sql.append(" (select address from customer where customer_id=soTmp.customerId) as address, ");
		sql.append(" (select staff_code from staff where staff_id = soTmp.staff_id) as staffCode, ");
		sql.append(" (select staff_name from staff where staff_id = soTmp.staff_id) as staffName, ");
		sql.append(" (select staff_code from staff where staff_id = soTmp.delivery_id) as deliveryCode, ");
		sql.append(" (select staff_name from staff where staff_id = soTmp.delivery_id) as deliveryName, ");
		sql.append(" saleOrderDiscount, totalDiscount, fromOrderId, ");
		sql.append(" fromSaleOrderNumber, orderType, orderStatus, approvedStep, saleOrderType,priorityStr");
		
		sql.append(" from (");
		sql.append(" select so.sale_order_id as id, so.ref_Order_Number as refOrderNumber, so.order_date as orderDate, so.delivery_Date as deliveryDate,  ");
		sql.append(" so.total as totalValue,so.order_number as orderNumber, so.customer_id as customerId, ");
		sql.append(" so.staff_id, so.delivery_id,");
		sql.append(" (select nvl(sum(discount_amount),0) from sale_order_promo_detail where sale_order_id=so.sale_order_id and is_owner=2) as saleOrderDiscount, ");
		sql.append(" (select nvl(sum(discount_amount),0) from sale_order_promo_detail where sale_order_id=so.sale_order_id and is_owner in (0,1)) as totalDiscount,");
		sql.append(" so.from_sale_order_id fromOrderId, ");
		sql.append(" (select order_number from sale_order where sale_order_id = so.from_sale_order_id) as fromSaleOrderNumber,");
		sql.append(" so.order_type as orderType, so.approved as orderStatus, so.approved_step as approvedStep, so.type as saleOrderType, ");
		sql.append(" (select ap_param_name from ap_param where ap_param_id = so.priority) as priorityStr");
		sql.append(" from sale_order so ");
		sql.append(" where 1=1 ");
		
		if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "so.staff_id");
			sql.append(paramsFix);
		}
		if (filter.getApproval() != null) {
			sql.append(" and so.approved = ? ");
			params.add(filter.getApproval().getValue());
		}
		if(filter.getApprovedStep() != null){
			sql.append(" and so.approved_step = ? ");
			params.add(filter.getApprovedStep().getValue());
		}
		String s = null;
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and upper(so.order_number) like ? escape '/' ");
			s = filter.getOrderNumber().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(filter.getRefOrderNumber())) {
			sql.append(" and upper(so.ref_order_number) like ? escape '/' ");
			s = filter.getRefOrderNumber().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (filter.getFromDate() != null) {
			sql.append(" and so.order_date >=trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and so.order_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		if (filter.getDeliveryDate() != null) {
			sql.append(" and so.delivery_date >= trunc(?) and so.delivery_date < trunc(?) + 1 ");
			params.add(filter.getDeliveryDate());
			params.add(filter.getDeliveryDate());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and so.staff_id in (select staff_id from staff where upper(staff_code) like ? escape '/' ) ");
			s = filter.getStaffCode().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
			sql.append(" and so.delivery_id in (select staff_id from staff where upper(staff_code) like ? escape '/' ) ");
			s = filter.getDeliveryCode().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and so.customer_id in (select customer_id from customer where upper(short_code) like ? escape '/' ) ");
			s = filter.getShortCode().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomeName())) {
			sql.append(" and so.customer_id in (select customer_id from customer where upper(name_text) like ? escape '/' ) ");
			s = filter.getCustomeName().toUpperCase();
			s = Unicode2English.codau2khongdau(s);
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (filter.getShopId() != null) {
			sql.append(" and so.shop_id = ?");
			params.add(filter.getShopId());
		}
		if (filter.getOrderType() != null) {
			sql.append(" and so.order_type = ? ");
			params.add(filter.getOrderType().getValue());
		} else if (filter.getLstOrderTypeStr() != null && filter.getLstOrderTypeStr().size() > 0) {
			sql.append(" and so.order_type in ('-1'");
			for (String ot : filter.getLstOrderTypeStr()) {
				sql.append(",?");
				params.add(ot.trim());
			}
			sql.append(")");
		}
		if (filter.getSaleOrderType() != null) {
			sql.append(" and so.type = ? ");
			params.add(filter.getSaleOrderType().getValue());
		}
		if (filter.getOrderSource() != null) {
			sql.append(" and so.ORDER_SOURCE = ? ");
			params.add(filter.getOrderSource().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getPriorityCode())) {
			sql.append(" and so.priority = (select ap_param_id from ap_param where status = 1 and ap_param_code = ? and type = 'ORDER_PIRITY' and rownum = 1) ");
			params.add(filter.getPriorityCode());
		}
		sql.append(") soTmp ");
		
		
		sqlTrans.append("select id, refOrderNumber, orderDate, deliveryDate,");
		sqlTrans.append(" totalValue, orderNumber, customerId,");
		sqlTrans.append(" customerCode, customerName, address, ");
		sqlTrans.append(" staffCode, staffName,");
		sqlTrans.append(" deliveryCode, deliveryName,");
		sqlTrans.append(" saleOrderDiscount, totalDiscount, fromOrderId, ");
		sqlTrans.append(" fromSaleOrderNumber, orderType, orderStatus, approvedStep, saleOrderType,priorityStr ");
		sqlTrans.append(" from (");
		sqlTrans.append(" select st.stock_trans_id as id ");
		sqlTrans.append(" , st.stock_trans_code as orderNumber ");
		sqlTrans.append(" , st.stock_trans_date as orderDate ");
		sqlTrans.append(" , null as deliveryDate, st.total_amount as totalValue, null as customerId ");
		sqlTrans.append(" ,null as refOrderNumber ");
		sqlTrans.append(" ,null as customerCode ");
		sqlTrans.append(" ,null as customerName ");
		sqlTrans.append(" ,null as address ");
		sqlTrans.append(" , st.trans_type orderType ");
		sqlTrans.append(" , (select to_char(case when stl.from_owner_type = ? ");
		paramsTrans.add(StockTransLotOwnerType.STAFF.getValue());
		sqlTrans.append("  	then (select staff_code ||'-'|| staff_name from staff where staff_id = stl.from_owner_id)  ");
		sqlTrans.append(" 	else (case when stl.to_owner_type = ? then");
		paramsTrans.add(StockTransLotOwnerType.STAFF.getValue());
		sqlTrans.append(" (select staff_code ||'-'|| staff_name from staff where staff_id = stl.to_owner_id) else null end)  end)");
		sqlTrans.append(" from stock_trans_lot  stl where stl.stock_trans_id=st.stock_trans_id and rownum=1) staffCode ");
		sqlTrans.append(" , (SELECT CASE ");
		sqlTrans.append("     WHEN stl.from_owner_type = ? THEN stl.from_owner_id ");
		paramsTrans.add(StockTransLotOwnerType.STAFF.getValue());
		sqlTrans.append("     WHEN stl.to_owner_type = ? THEN stl.to_owner_id ");
		paramsTrans.add(StockTransLotOwnerType.STAFF.getValue());
		sqlTrans.append("     ELSE st.staff_id ");
		sqlTrans.append("   END ");
		sqlTrans.append(" FROM stock_trans_lot stl WHERE stl.stock_trans_id=st.stock_trans_id AND rownum =1 ");
		sqlTrans.append(" ) staffId ");
		sqlTrans.append(" , null as staffName ");
		sqlTrans.append(" , null as deliveryCode ");
		sqlTrans.append(" , null as deliveryName ");
		sqlTrans.append(" , null as saleOrderDiscount ");
		sqlTrans.append(" , null as totalDiscount ");
		sqlTrans.append(" , st.from_stock_trans_id fromOrderId ");
//		sqlTrans.append(" , (select stock_trans_code from stock_trans where stock_trans_id = st.from_stock_trans_id) as fromSaleOrderNumber ");
		sqlTrans.append(" , (select listagg(stock_trans_code,', ')  WITHIN GROUP (order by stock_trans_code)  ");
		sqlTrans.append(" 			from stock_trans where from_stock_trans_id = st.stock_trans_id) as fromSaleOrderNumber");
		sqlTrans.append(" , st.approved as orderStatus ");
		sqlTrans.append(" , st.approved_step as approvedStep ");
		sqlTrans.append(" , null as saleOrderType ");
		sqlTrans.append(" , null as priorityStr");
		sqlTrans.append(" from stock_trans st ");
		sqlTrans.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getRefOrderNumber()) || !StringUtility.isNullOrEmpty(filter.getShortCode()) || !StringUtility.isNullOrEmpty(filter.getCustomeName())
				|| !StringUtility.isNullOrEmpty(filter.getDeliveryCode()) || !StringUtility.isNullOrEmpty(filter.getPriorityCode()) || filter.getOrderSource() != null
				|| filter.getDeliveryDate() != null){
			sqlTrans.append(" and 1=0 ");
		}
		if (filter.getFromDate() != null) {
			sqlTrans.append(" and st.stock_trans_date >= trunc(?)");
			paramsTrans.add(filter.getFromDate());
		}

		if (filter.getToDate() != null) {
			sqlTrans.append(" and st.stock_trans_date < trunc(?) + 1");
			paramsTrans.add(filter.getToDate());
		}
		if(filter.getApproval() != null){
			sqlTrans.append(" and st.approved = ? ");
			paramsTrans.add(filter.getApproval().getValue());
		}
		if(filter.getApprovedStep() != null){
			sqlTrans.append(" and st.approved_step = ? ");
			paramsTrans.add(filter.getApprovedStep().getValue());
		}
		if (filter.getShopId() != null) {
			sqlTrans.append(" and st.shop_id = ?");
			paramsTrans.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sqlTrans.append(" and lower(st.stock_trans_code) like ? escape '/'");
			paramsTrans.add(StringUtility.toOracleSearchLikeSuffix(filter.getOrderNumber().toLowerCase()));
		}
		if (filter.getOrderType() != null) {
			sqlTrans.append(" and st.trans_type in (?) ");
			paramsTrans.add(filter.getOrderType().getValue());
		} else if (filter.getLstOrderTypeStr() != null && filter.getLstOrderTypeStr().size() > 0) {
			sqlTrans.append(" and st.trans_type in ('-1'");
			for (String ot : filter.getLstOrderTypeStr()) {
				sqlTrans.append(",?");
				paramsTrans.add(ot.trim());
			}
			sqlTrans.append(")");
		}
		sqlTrans.append(" ) where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getStrListUserId())) {
			sqlTrans.append(" and ( (orderType not in ('DC','DCT','DCG') ");
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrListUserId(), "staffId");
			sqlTrans.append(paramsFix);
			sqlTrans.append(" ) or orderType in ('DC','DCT','DCG') ) ");
		}
		if(!StringUtility.isNullOrEmpty(filter.getStaffCode())){
			sqlTrans.append(" and lower(staffCode) like ? escape '/' ");
			paramsTrans.add(StringUtility.toOracleSearchLikeSuffix(filter.getStaffCode().toLowerCase()));
		}
		if(filter.getSaleOrderType() != null && SaleOrderType.RETURNED.equals(filter.getSaleOrderType())){
			sqlTrans.append(" and orderType = ? and fromSaleOrderNumber is not null ");
			paramsTrans.add(OrderType.DP.getValue());
		}

		final String[] fieldNames = new String[] { 
				"id", "refOrderNumber", "orderDate", 
				"deliveryDate", "totalValue", "orderNumber", 
				"customerId", "customerCode", "customerName", 
				"address", "staffCode", "staffName", 
				"deliveryCode", "deliveryName", "saleOrderDiscount",
				"totalDiscount", "fromSaleOrderNumber", "fromOrderId" ,"orderType",
				"orderStatus","approvedStep", "saleOrderType", "priorityStr" };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.TIMESTAMP, 
				StandardBasicTypes.TIMESTAMP, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER,StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.STRING };

		if (filter.getOrderType() != null) {
			if (filter.getOrderType().equals(OrderType.GO) || filter.getOrderType().equals(OrderType.DP) || filter.getOrderType().equals(OrderType.DCT) || filter.getOrderType().equals(OrderType.DCG)
					 || filter.getOrderType().equals(OrderType.DC)) {
				sqlTrans.append(" order by orderDate,orderNumber ");
				StringBuilder countSql = new StringBuilder();
				countSql.append("select count(1) as count from (");
				countSql.append(sqlTrans.toString());
				countSql.append(")");
				if (filter.getkPagingVOEx2() == null) {
					return repo.getListByQueryAndScalar(SaleOrderVOEx2.class, fieldNames, fieldTypes, sqlTrans.toString(), paramsTrans);
				} else {
					return repo.getListByQueryAndScalarPaginated(SaleOrderVOEx2.class, fieldNames, fieldTypes, sqlTrans.toString(), countSql.toString(), paramsTrans, paramsTrans, filter.getkPagingVOEx2());
				}
			} else {
				sql.append(" order by orderDate,orderNumber ");
				StringBuilder countSql = new StringBuilder();
				countSql.append("select count(1) as count from (");
				countSql.append(sql.toString());
				countSql.append(")");
				if (filter.getkPagingVOEx2() == null) {
					return repo.getListByQueryAndScalar(SaleOrderVOEx2.class, fieldNames, fieldTypes, sql.toString(), params);
				} else {
					return repo.getListByQueryAndScalarPaginated(SaleOrderVOEx2.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingVOEx2());
				}
			}
		} else {
			// tat ca order type = null
			StringBuilder sqlData = new StringBuilder();
			sqlData.append(" with saleOrder as( ");
			sqlData.append(sql.toString());
			sqlData.append(" ),  ");
			sqlData.append(" stockTrans as( ");
			sqlData.append(sqlTrans);
			sqlData.append(" ),  ");
			sqlData.append(" result as ( ");
			sqlData.append(" select * from saleOrder ");
			sqlData.append(" UNION ");
			sqlData.append(" select * from stockTrans ");
			sqlData.append(" ) ");
			StringBuilder countSql = new StringBuilder();
			countSql.append(sqlData);
			countSql.append(" select count(1) as count from result ");
			sqlData.append(" select * from result order by orderDate,orderNumber ");
			params.addAll(paramsTrans);
			if (filter.getkPagingVOEx2() == null) {
				return repo.getListByQueryAndScalar(SaleOrderVOEx2.class, fieldNames, fieldTypes, sqlData.toString(), params);
			} else {
				return repo.getListByQueryAndScalarPaginated(SaleOrderVOEx2.class, fieldNames, fieldTypes, sqlData.toString(), countSql.toString(), params, params, filter.getkPagingVOEx2());
			}
		}
	} 

	/**
	 * @author lacnv1
	 */
	@Override
	public boolean checkPromotionOpenOfSaleOrder(long saleOrderId, long customerId, Date lockDate, List<Long> lstPassOrderId) throws DataAccessException {
		if (lockDate == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select count(distinct sop.promotion_program_id) as count");
		sql.append(" from sale_order_promotion sop");
		sql.append(" where exists (select 1 from promotion_program");
		sql.append(" where promotion_program_id = sop.promotion_program_id");
		sql.append(" and type = ?");
		params.add(PromotionType.ZV24.getValue());
		sql.append(" )");
		sql.append(" and quantity_received > 0");
		sql.append(" and sale_order_id = ?");
		params.add(saleOrderId);
		
		Integer c = repo.countBySQL(sql.toString(), params);
		if (c == null || c == 0) {
			return true;
		}
		
		sql = new StringBuilder();
		params = new ArrayList<Object>();
		sql.append("with lstPromotion as (");
		sql.append(" select sop.promotion_program_id");
		sql.append(" from sale_order_promotion sop");
		sql.append(" where exists (select 1 from promotion_program");
		sql.append(" where promotion_program_id = sop.promotion_program_id");
		sql.append(" and type = ?");
		params.add(PromotionType.ZV24.getValue());
		sql.append(" )");
		sql.append(" and quantity_received > 0");
		sql.append(" and sale_order_id = ?");
		params.add(saleOrderId);
		sql.append(" group by sop.promotion_program_id");
		sql.append(" )");
		sql.append(" select count(1) as count");
		sql.append(" from promotion_product_open ppo");
		sql.append(" join sale_order_detail p on (p.sale_order_id = ? and is_free_item = 0 and p.product_id = ppo.product_id)");
		params.add(saleOrderId);
		sql.append(" where promotion_program_id in (select promotion_program_id from lstPromotion)");
		sql.append(" and not exists (select 1");
		sql.append(" from sale_order_detail sod");
		sql.append(" where order_date >= (select add_months(trunc(?), -1* quanti_month_new_open)");
		params.add(lockDate);
		sql.append(" from promotion_program where promotion_program_id = ppo.promotion_program_id");
		sql.append(" )");
		sql.append(" and is_free_item = 0 and product_id = ppo.product_id");
		sql.append(" and sale_order_id <> ?");
		params.add(saleOrderId);
		sql.append(" and exists (select 1 from sale_order so1");
		sql.append(" where sale_order_id = sod.sale_order_id and customer_id = ?");
		params.add(customerId);
		sql.append(" and type = ? and (approved = ? or (approved = ? and (approved_step > ?");
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		params.add(SaleOrderStatus.APPROVED.getValue());
		params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
		params.add(SaleOrderStep.NOT_YET_CONFIRM.getValue());
		if (lstPassOrderId != null && lstPassOrderId.size() > 0) {
			sql.append(" or sale_order_id in (-1");
			for (Long idt : lstPassOrderId) {
				sql.append(",?");
				params.add(idt);
			}
			sql.append("))");
		} else {
			sql.append(" or 1=0)");
		}
		sql.append(" and exists (select 1 from sale_order_promotion where sale_order_id = so1.sale_order_id");
		sql.append(" and promotion_program_id = ppo.promotion_program_id and quantity_received > 0)");
		sql.append("))");
		sql.append(" )");
		sql.append(" )");
		sql.append(" and (ppo.quantity is null or p.quantity >= ppo.quantity)");
		sql.append(" and (ppo.amount is null or p.amount >= ppo.amount)");
		
		c = repo.countBySQL(sql.toString(), params);
		
		return (c != null && c > 0);
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public boolean checkApprovedPromotionOpenOfSaleOrder(long saleOrderId, long customerId, Date lockDate, List<Long> lstPassOrderId) throws DataAccessException {
		if (lockDate == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select count(distinct sop.promotion_program_id) as count");
		sql.append(" from sale_order_promotion sop");
		sql.append(" where exists (select 1 from promotion_program");
		sql.append(" where promotion_program_id = sop.promotion_program_id");
		sql.append(" and type = ?");
		params.add(PromotionType.ZV24.getValue());
		sql.append(" )");
		sql.append(" and quantity_received > 0");
		sql.append(" and sale_order_id = ?");
		params.add(saleOrderId);
		
		Integer c = repo.countBySQL(sql.toString(), params);
		if (c == null || c == 0) {
			return true;
		}
		
		sql = new StringBuilder();
		params = new ArrayList<Object>();
		sql.append("with lstPromotion as (");
		sql.append(" select sop.promotion_program_id");
		sql.append(" from sale_order_promotion sop");
		sql.append(" where exists (select 1 from promotion_program");
		sql.append(" where promotion_program_id = sop.promotion_program_id");
		sql.append(" and type = ?");
		params.add(PromotionType.ZV24.getValue());
		sql.append(" )");
		sql.append(" and quantity_received > 0");
		sql.append(" and sale_order_id = ?");
		params.add(saleOrderId);
		sql.append(" group by sop.promotion_program_id");
		sql.append(" )");
		sql.append(" select count(1) as count");
		sql.append(" from promotion_product_open ppo");
		sql.append(" join sale_order_detail p on (p.sale_order_id = ? and is_free_item = 0 and p.product_id = ppo.product_id)");
		params.add(saleOrderId);
		sql.append(" where promotion_program_id in (select promotion_program_id from lstPromotion)");
		sql.append(" and not exists (select 1");
		sql.append(" from sale_order_detail sod");
		sql.append(" where order_date >= (select add_months(trunc(?), -1* quanti_month_new_open)");
		params.add(lockDate);
		sql.append(" from promotion_program where promotion_program_id = ppo.promotion_program_id");
		sql.append(" )");
		sql.append(" and is_free_item = 0 and product_id = ppo.product_id");
		sql.append(" and sale_order_id <> ?");
		params.add(saleOrderId);
		sql.append(" and exists (select 1 from sale_order so1");
		sql.append(" where sale_order_id = sod.sale_order_id and customer_id = ?");
		params.add(customerId);
		sql.append(" and type = ?");
		params.add(SaleOrderType.NOT_YET_RETURNED.getValue());
		if (lstPassOrderId != null && lstPassOrderId.size() > 0) {
			sql.append(" and (approved = ?");
			params.add(SaleOrderStatus.APPROVED.getValue());
			sql.append(" or sale_order_id in (-1");
			for (Long idt : lstPassOrderId) {
				sql.append(",?");
				params.add(idt);
			}
			sql.append("))");
		} else {
			sql.append(" and approved = ?");
			params.add(SaleOrderStatus.APPROVED.getValue());
		}
		sql.append(")");
		sql.append(" )");
		sql.append(" and (ppo.quantity is null or p.quantity >= ppo.quantity)");
		sql.append(" and (ppo.amount is null or p.amount >= ppo.amount)");
		
		c = repo.countBySQL(sql.toString(), params);
		
		return (c != null && c > 0);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<PromotionProgram> getAccumulativeProgramOfOrder(long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select pp.*");
		sql.append(" from promotion_program pp");
		sql.append(" where pp.promotion_program_code in (");
		
		sql.append(" select sod.program_code");
		sql.append(" from sale_order_detail sod");
		sql.append(" where sod.sale_order_id = ?");
		params.add(saleOrderId);
		sql.append(" and sod.programe_type_code = ?");
		params.add(PromotionType.ZV23.getValue());
		sql.append(" union all");
		sql.append(" select sopd.program_code");
		sql.append(" from sale_order_promo_detail sopd");
		sql.append(" where sopd.sale_order_id = ?");
		params.add(saleOrderId);
		sql.append(" and sopd.programe_type_code = ?");
		params.add(PromotionType.ZV23.getValue());
		
		sql.append(")");
		
		List<PromotionProgram> lst = repo.getListBySQL(PromotionProgram.class, sql.toString(), params);
		
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public RptAccumulativePromotionProgram getRptAccumulativePPOfOrder(Long shopId, Long promotionProgramId, Long customerId) throws DataAccessException {
		if (shopId == null || promotionProgramId == null || customerId == null) {
			throw new DataAccessException("Invalid arguments (shop_id, promotion_program_id, customer_id must not be null.)");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select rpt.* ");
		sql.append(" from rpt_cttl rpt ");
		sql.append(" where 1 = 1 ");
		sql.append(" and rpt.shop_id = ? ");
		params.add(shopId);
		sql.append(" and rpt.customer_id = ? ");
		params.add(customerId);
		sql.append(" and rpt.promotion_program_id = ? ");
		params.add(promotionProgramId);
		sql.append(" and exists (select 1 from promotion_program where promotion_program_id = ?) ");
		params.add(promotionProgramId);

		return repo.getEntityBySQL(RptAccumulativePromotionProgram.class, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public boolean checkAccumulativePromotionOfSaleOrder(long saleOrderId, Date orderDate, List<Long> lstPassOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select count(1) as count");
		sql.append(" from rpt_cttl tl");
		sql.append(" join rpt_cttl_detail tld on (tld.rpt_cttl_id = tl.rpt_cttl_id)"); //
		sql.append(" where exists (select 1 from rpt_cttl_pay p2");
		sql.append(" join rpt_cttl_detail_pay pd2 on (pd2.rpt_cttl_pay_id = p2.rpt_cttl_pay_id)"); //
		sql.append(" where p2.promotion_program_id = tl.promotion_program_id and p2.customer_id = tl.customer_id");
		sql.append(" and pd2.product_id = tld.product_id"); //
		sql.append(" and p2.sale_order_id = ?");
		params.add(saleOrderId);
		sql.append(" )");
		//sql.append(" and tl.total_amount < (select nvl(sum(total_amount_pay_promotion), 0) from rpt_cttl_pay p1");
		sql.append(" and exists (select 1 from rpt_cttl_pay p1"); //
		sql.append(" join rpt_cttl_detail_pay pd on (pd.rpt_cttl_pay_id = p1.rpt_cttl_pay_id)"); //
		sql.append(" where p1.promotion_program_id = tl.promotion_program_id and p1.customer_id = tl.customer_id");
		sql.append(" and pd.product_id = tld.product_id"); //
		sql.append(" and (p1.approved = 1");
		sql.append(" or (p1.approved = 0");
		sql.append(" and exists (select 1 from sale_order where sale_order_id = p1.sale_order_id and type = 1 and approved_step > 0)");
		sql.append(")");
		sql.append(" or p1.sale_order_id in (?");
		params.add(saleOrderId);
		if (lstPassOrderId != null && lstPassOrderId.size() > 0) {
			for (Long idt : lstPassOrderId) {
				sql.append(",?");
				params.add(idt);
			}
		}
		sql.append("))");
		if (orderDate != null) {
			sql.append(" and p1.create_date >= trunc(?) and p1.create_date < trunc(?) + 1");
			params.add(orderDate);
			params.add(orderDate);
		}
		sql.append(" having nvl(sum(pd.amount_promotion), 0) > tld.amount or nvl(sum(pd.quantity_promotion), 0) > tld.quantity"); //
		sql.append(" )");
		
		Integer c = repo.countBySQL(sql.toString(), params);
		
		return (c <= 0);
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public boolean checkApprovedAccumulativePromotionOfSaleOrder(long saleOrderId, Date orderDate, List<Long> lstPassOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select count(1) as count");
		sql.append(" from rpt_cttl tl");
		sql.append(" join rpt_cttl_detail tld on (tld.rpt_cttl_id = tl.rpt_cttl_id)"); //
		sql.append(" where exists (select 1 from rpt_cttl_pay p2");
		sql.append(" join rpt_cttl_detail_pay pd2 on (pd2.rpt_cttl_pay_id = p2.rpt_cttl_pay_id)"); //
		sql.append(" where promotion_program_id = tl.promotion_program_id and customer_id = tl.customer_id");
		sql.append(" and pd2.product_id = tld.product_id"); //
		sql.append(" and p2.sale_order_id = ?");
		params.add(saleOrderId);
		sql.append(")");
		//sql.append(" and tl.total_amount < (select nvl(sum(total_amount_pay_promotion), 0) from rpt_cttl_pay");
		sql.append(" and exists (select 1 from rpt_cttl_pay p1"); //
		sql.append(" join rpt_cttl_detail_pay pd on (pd.rpt_cttl_pay_id = p1.rpt_cttl_pay_id)"); //
		sql.append(" where p1.promotion_program_id = tl.promotion_program_id and p1.customer_id = tl.customer_id");
		sql.append(" and pd.product_id = tld.product_id"); //
		sql.append(" and (p1.approved = 1 or p1.sale_order_id in (?");
		params.add(saleOrderId);
		if (lstPassOrderId != null && lstPassOrderId.size() > 0) {
			for (Long idt : lstPassOrderId) {
				sql.append(",?");
				params.add(idt);
			}
		}
		sql.append("))");
		if (orderDate != null) {
			sql.append(" and p1.create_date >= trunc(?) and p1.create_date < trunc(?) + 1");
			params.add(orderDate);
			params.add(orderDate);
		}
		sql.append(" having nvl(sum(pd.amount_promotion), 0) > tld.amount or nvl(sum(pd.quantity_promotion), 0) > tld.quantity"); //
		sql.append(")");
		
		Integer c = repo.countBySQL(sql.toString(), params);
		
		return (c <= 0);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<StockTotalVO> getListWaringAccumulativeProductQuantity(long shopId, long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		SaleOrder so = this.getSaleOrderById(saleOrderId);
		if (so == null || (!OrderType.SO.equals(so.getOrderType()) && !OrderType.IN.equals(so.getOrderType()))) {
			return null;
		}
		
		sql.append("with products as (");
		sql.append(" select product_id, decode(programe_type_code, 'ZV23', 'ZV23', '-1') as programe_type_code, sum(quantity) as quantity");
		sql.append(" from sale_order_detail sod");
		sql.append(" where sale_order_id = ?");
		params.add(saleOrderId);
		sql.append(" and exists (");
		sql.append(" select 1 from sale_order_detail");
		sql.append(" where sale_order_id = ? and product_id = sod.product_id");
		params.add(saleOrderId);
		sql.append(" and is_free_item = 1 and programe_type_code = ?");
		params.add(PromotionType.ZV23.getValue());
		sql.append(" )");
		sql.append(" group by product_id, decode(programe_type_code, 'ZV23', 'ZV23', '-1')");
		sql.append("),");
		sql.append("stocktmp as (");
		sql.append(" select stt.product_id,");
		sql.append(" sum(available_quantity) - nvl(p.quantity, 0)  as available_quantity");
		sql.append(" from stock_total stt");
		sql.append(" left join products p");
		sql.append(" on (p.product_id = stt.product_id and p.programe_type_code <> ?)");
		params.add(PromotionType.ZV23.getValue());
		sql.append(" where stt.status = ? and stt.object_type = ? and stt.object_id   = ?");
		params.add(ActiveType.RUNNING.getValue());
		params.add(StockObjectType.SHOP.getValue());
		params.add(shopId);
		sql.append(" and stt.product_id in (select product_id from products)");
		sql.append(" group by stt.product_id, p.product_id, p.quantity");
		sql.append("),");
		sql.append(" stocks as (");
		sql.append(" select stt.product_id, sum(available_quantity) as available_quantity, p.quantity");
		sql.append(" from stocktmp stt");
		sql.append(" join products p on (p.product_id = stt.product_id and p.programe_type_code = ?)");
		params.add(PromotionType.ZV23.getValue());
		sql.append(" where not exists (select 1 from stocktmp where available_quantity < 0)");
		sql.append(" group by stt.product_id, p.product_id, p.quantity");
		sql.append(")");
		sql.append(" select st.product_id as productId,");
		sql.append(" (select product_code from product where product_id = st.product_id) as productCode,");
		sql.append(" st.available_quantity as availableQuantity, st.quantity as quantity");
		sql.append(" from stocks st");
		sql.append(" where available_quantity >= 0 and available_quantity < quantity");
		
		String[] fieldNames = {
				"productId",
				"productCode",
				"availableQuantity",
				"quantity"
		};
		
		Type[] fieldTypes = {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER
		};
		
		List<StockTotalVO> lst = repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<OrderProductVO> getCONotHaveApprovedSO(SoFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("invalid filter");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select co.sale_order_id as saleOrderId, co.order_number as orderNumber");
		sql.append(" from sale_order co");
		sql.append(" where order_type = ?");
		params.add(OrderType.CO.getValue());
		sql.append(" and not exists (");
		sql.append(" select 1 from sale_order");
		sql.append(" where sale_order_id = co.from_sale_order_id");
		sql.append(" and approved = 1 and approved_step > 1");
		sql.append(" )");
		if (filter.getLstSaleOrderId() != null) {
			sql.append(" and sale_order_id in (-1");
			for (Long idt : filter.getLstSaleOrderId()) {
				sql.append(",?");
				params.add(idt);
			}
			sql.append(")");
		}
		
		String[] fieldNames = { "saleOrderId", "orderNumber" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING };
		
		List<OrderProductVO> lst = repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	@Override
	public void updateEquipment(ShopParam numberValueOrderParam) throws DataAccessException {
		if (numberValueOrderParam == null) {
			throw new IllegalArgumentException("numberValueOrderParam is null");
		}
	/*	if (!StringUtility.isNullOrEmpty(numberValueOrderParam.getCode())) {
			numberValueOrderParam.setCode(numberValueOrderParam.getCode().trim().toUpperCase());
		}*/
		repo.update(numberValueOrderParam);
	}

	@Override
	public ShopParam createShopParamByInvoice(ShopParam numberValueOrderParam) throws DataAccessException {
		if (numberValueOrderParam == null) {
			throw new IllegalArgumentException("numberValueOrderParam is null");
		}
		if (!StringUtility.isNullOrEmpty(numberValueOrderParam.getCode())) {
			numberValueOrderParam.setCode(numberValueOrderParam.getCode().trim().toUpperCase());
		}
		numberValueOrderParam.setCreateDate(commonDAO.getSysDate());
		return repo.create(numberValueOrderParam);
	}
	
	@Override
	public boolean checkExistsProcessHistoryForOrder(long objectId, int objectType) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "select * from process_history where type = 1 and object_type=? and object_type = ? ";
		params.add(objectId);
		params.add(objectType);
		ProcessHistory rs = repo.getEntityBySQL(ProcessHistory.class, sql, params);
		if (rs != null){
			return true;
		}
		return false;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrderVO> getListOrderVOForXml(SoFilter filter) throws DataAccessException {
		if (filter == null || filter.getShopId() == null) {
			throw new IllegalArgumentException("invalid paramters");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select so.sale_order_id as saleOrderId,");
		sql.append(" so.amount,");
		sql.append(" (select staff_code from staff where staff_id = so.delivery_id) as deliveryCode,");
		//sql.append(" (select listagg(program_code, ', ') within group (order by program_code)");
		sql.append(" (select wm_concat(distinct program_code)");
		sql.append(" from sale_order_promo_detail where sale_order_id = so.sale_order_id and programe_type_code in ('ZV19', 'ZV20', 'ZV21')) as promoCode,");
		//sql.append(" nvl(so.discount, 0) as discount,");
		sql.append(" (select nvl(sum(discount_amount), 0)");
		sql.append(" from sale_order_promo_detail where sale_order_id = so.sale_order_id and programe_type_code in ('ZV19', 'ZV20', 'ZV21')) as discount,");
		sql.append(" so.ref_order_number as invoiceNumber,");
		//sql.append(" (to_char(so.order_date, 'yyyy-mm-dd') || 'T' || to_char(so.order_date, 'hh24:mi:ss') || sessiontimezone) as orderDateStr,");
		sql.append(" (to_char(so.order_date, 'yyyy-mm-dd') || 'T' || to_char(so.order_date, 'hh24:mi:ss') || '+07:00') as orderDateStr,");
		sql.append(" so.order_date as orderDate,");
		sql.append(" so.order_number as orderNumber, so.order_type as orderType,");
		sql.append(" so.total,");
		sql.append(" (select shop_code from shop where shop_id = so.shop_id) as shopCode,");
		sql.append(" (select short_code from customer where customer_id = so.customer_id) as customerCode,");
		sql.append(" (select staff_code from staff where staff_id = so.staff_id) as staffCode,");
		sql.append(" (select ap_param_name from ap_param where status = 1 and ap_param_id = so.priority and type = 'ORDER_PIRITY' and rownum = 1) as priorityStr,");
		sql.append(" (case when so.delivery_date is null then null");
		sql.append(" else to_char(so.delivery_date, 'yyyy-mm-dd') end) as deliveryDateStr");
		sql.append(" from sale_order so");
		
		sql.append(" where 1=1");
		
		if (filter.getApproval() != null) {
			sql.append(" and so.approved = ?");
			params.add(filter.getApproval().getValue());
		}
		if(filter.getApprovedStep() != null){
			sql.append(" and so.approved_step = ?");
			params.add(filter.getApprovedStep().getValue());
		}
		String s = null;
		if (!StringUtility.isNullOrEmpty(filter.getOrderNumber())) {
			sql.append(" and upper(so.order_number) like ? escape '/'");
			s = filter.getOrderNumber().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(filter.getRefOrderNumber())) {
			sql.append(" and upper(so.ref_order_number) like ? escape '/'");
			s = filter.getRefOrderNumber().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (filter.getFromDate() != null) {
			sql.append(" and so.order_date >=trunc(?)");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and so.order_date < trunc(?) + 1");
			params.add(filter.getToDate());
		}
		if (filter.getDeliveryDate() != null) {
			sql.append(" and so.delivery_date >= trunc(?) and so.delivery_date < trunc(?) + 1");
			params.add(filter.getDeliveryDate());
			params.add(filter.getDeliveryDate());
		}
		if (!StringUtility.isNullOrEmpty(filter.getStaffCode())) {
			sql.append(" and so.staff_id in (select staff_id from staff where upper(staff_code) like ? escape '/' )");
			s = filter.getStaffCode().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(filter.getDeliveryCode())) {
			sql.append(" and so.delivery_id in (select staff_id from staff where upper(staff_code) like ? escape '/' )");
			s = filter.getDeliveryCode().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append(" and so.customer_id in (select customer_id from customer where upper(short_code) like ? escape '/' )");
			s = filter.getShortCode().toUpperCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(filter.getCustomeName())) {
			sql.append(" and so.customer_id in (select customer_id from customer where upper(name_text) like ? escape '/' )");
			s = filter.getCustomeName().toUpperCase();
			s = Unicode2English.codau2khongdau(s);
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (filter.getShopId() != null) {
			sql.append(" and so.shop_id = ?");
			params.add(filter.getShopId());
		}
		if (filter.getOrderType() != null) {
			sql.append(" and so.order_type = ?");
			params.add(filter.getOrderType().getValue());
		} else if (filter.getLstOrderTypeStr() != null && filter.getLstOrderTypeStr().size() > 0) {
			sql.append(" and so.order_type in ('-1'");
			for (String ot : filter.getLstOrderTypeStr()) {
				sql.append(",?");
				params.add(ot.trim());
			}
			sql.append(")");
		} else {
			sql.append(" and so.order_type in ('IN', 'SO', 'CM', 'CO')");
		}
		if (filter.getSaleOrderType() != null) {
			sql.append(" and so.type = ?");
			params.add(filter.getSaleOrderType().getValue());
		}
		if (filter.getOrderSource() != null) {
			sql.append(" and so.order_source = ?");
			params.add(filter.getOrderSource().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getPriorityCode())) {
			sql.append(" and so.priority in (select ap_param_id from ap_param where status = 1 and ap_param_code = ? and type = 'ORDER_PIRITY' and rownum = 1)");
			params.add(filter.getPriorityCode());
		}
		
		String[] fieldNames = {
				"saleOrderId",
				"amount",
				"deliveryCode",
				"promoCode", "discount",
				"invoiceNumber",
				"orderDateStr",
				"orderDate",
				"orderNumber", "orderType",
				"total",
				"shopCode",
				"customerCode",
				"staffCode",
				"priorityStr",
				"deliveryDateStr"
		};
		
		Type[] fieldTypes = {
				StandardBasicTypes.LONG,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.DATE,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING
		};
		
		List<SaleOrderVO> lst = repo.getListByQueryAndScalar(SaleOrderVO.class, fieldNames, fieldTypes, sql.toString(), params);
		
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrderDetailVO2> getListOrderDetailVOForXml(long orderId, Date orderDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		/*sql.append("select nvl(sod.discount_amount, 0) as discount,");
		sql.append(" nvl(sod.discount_percent, 0) as discountPercent,");*/
		sql.append("select (select nvl(sum(discount_amount), 0)");
		sql.append(" from sale_order_promo_detail where sale_order_detail_id = sod.sale_order_detail_id and programe_type_code not in ('ZV19', 'ZV20', 'ZV21')) as discount,");
		sql.append(" 0 as discountPercent,");
		sql.append(" sod.is_free_item as isFreeItem, nvl(sod.amount, 0) as amount,");
		sql.append(" decode(sod.is_free_item, 0, null, sod.program_code) as programCode,");
		sql.append(" sod.quantity, null as reasonCode,");
		sql.append(" sod.price,");
		sql.append(" p.product_code as productCode, p.uom1, p.uom2");
		sql.append(" from sale_order_detail sod");
		sql.append(" join product p on (p.product_id = sod.product_id)");
		sql.append(" where sod.sale_order_id = ?");
		params.add(orderId);
		if (orderDate != null) {
			sql.append(" and order_date >= trunc(?) and order_date < trunc(?) + 1");
			params.add(orderDate);
			params.add(orderDate);
		}
		
		String[] fieldNames = {
				"discount",
				"discountPercent",
				"isFreeItem",
				"amount",
				"programCode",
				"quantity",
				"reasonCode",
				"price",
				"productCode",
				"uom1",
				"uom2"
		};
		
		Type[] fieldTypes = {
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING
		};
		
		List<SaleOrderDetailVO2> lst = repo.getListByQueryAndScalar(SaleOrderDetailVO2.class, fieldNames, fieldTypes, sql.toString(), params);
		
		return lst;
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public int countVansaleOrderNotUpdatedStock(long shopId, long staffId, Date pDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select count(1) as count");
		sql.append(" from sale_order so");
		sql.append(" where so.shop_id = ?");
		params.add(shopId);
		sql.append(" and so.staff_id = ?");
		params.add(staffId);
		
		sql.append(" and so.order_type in (?, ?)");
		params.add(OrderType.SO.getValue());
		params.add(OrderType.CO.getValue());
		sql.append(" and so.approved in (?, ?) and so.approved_step in (?, ?, ?)");
		params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
		params.add(SaleOrderStatus.APPROVED.getValue());
		params.add(SaleOrderStep.NOT_YET_CONFIRM.getValue());
		params.add(SaleOrderStep.CONFIRMED.getValue());
		params.add(SaleOrderStep.PRINT_CONFIRMED.getValue());
		
		if (pDate != null) {
			sql.append(" and so.order_date >= trunc(?) and so.order_date < trunc(?)+1");
			params.add(pDate);
			params.add(pDate);
		}
		
		Integer c = repo.countBySQL(sql.toString(), params);
		
		if (c != null) {
			return c.intValue();
		}
		
		return 0;
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public int getMaxPrintBatch(long shopId, Date pDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select max(print_batch) as printBatch");
		sql.append(" from sale_order so");
		sql.append(" where so.shop_id = ?");
		params.add(shopId);
		
		if (pDate != null) {
			sql.append(" and so.time_print >= trunc(?) and so.time_print < trunc(?)+1");
			params.add(pDate);
			params.add(pDate);
		}
		
		Object obj = repo.getObjectByQuery(sql.toString(), params);
		
		if (obj != null) {
			if (obj instanceof BigDecimal) {
				return ((BigDecimal) obj).intValue();
			} else {
				return Integer.valueOf(obj.toString());
			}
		}

		return 0;
	}
	
	@Override
	public List<ProductStockTotal> lstProductStockTotal(SaleOrder tempOrder, StockTrans stockTrans, Integer type) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if(tempOrder != null && tempOrder.getShop() != null && type == 0){
			sql.append(" select p.product_id as productId, p.product_code as productCode, p.product_name as productName, sum(st.quantity) as quantity from stock_total st,product p ");
			sql.append(" where p.product_id = st.product_id and st.object_type = 1  ");
			sql.append(" and p.product_id in(select sod.product_id from sale_order so,sale_order_detail sod where sod.sale_order_id = so.sale_order_id and so.sale_order_id = ? ) ");
			sql.append(" and exists(select wh.* from warehouse wh,shop s where s.shop_id = wh.shop_id and st.warehouse_id = wh.warehouse_id and s.shop_id = ?) ");
			sql.append(" group by p.product_id,p.product_code,p.product_name ");
			sql.append(" order by p.product_id ");
			params.add(tempOrder.getId());
			params.add(tempOrder.getShop().getId());
		}
		
		if(stockTrans != null && stockTrans.getShop() != null && type == 1){
			sql.append(" select p.product_id as productId, p.product_code as productCode, p.product_name as productName, sum(st.quantity) as quantity from stock_total st,product p ");
			sql.append(" where p.product_id = st.product_id  ");
			sql.append(" and p.product_id in(select sod.product_id from stock_trans so,stock_trans_detail sod where sod.stock_trans_id = so.stock_trans_id and so.stock_trans_id = ? ) ");
			sql.append(" and exists(select wh.* from warehouse wh,shop s where s.shop_id = wh.shop_id and st.warehouse_id = wh.warehouse_id and s.shop_id = ?) ");
			sql.append(" group by p.product_id,p.product_code,p.product_name ");
			sql.append(" order by p.product_id ");
			params.add(stockTrans.getId());
			params.add(stockTrans.getShop().getId());

		}
//		sql.append(" select p.product_id as productId, p.product_code as productCode, p.product_name as productName, sum(st.quantity) as quantity from stock_total st,product p");
//		sql.append(" where p.product_id = st.product_id and p.product_id in(-1");
//		for(Long id: lstProductId){
//			sql.append(",?");
//			params.add(id);
//		}
//		sql.append(")");
//		sql.append(" and exists(select wh.* from warehouse wh,shop s where s.shop_id = wh.shop_id and st.warehouse_id = wh.warehouse_id and s.shop_id in(-1,");
//		for(Long id: lstShopId){
//			sql.append(",?");
//			params.add(id);
//		}
//		sql.append("))");
//		sql.append(" group by p.product_id");
//		sql.append(" order by p.product_id ");
		final String[] fieldNames = new String[] { "productId", "quantity", "productCode", "productName" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.STRING, StandardBasicTypes.STRING};
		
		return repo.getListByQueryAndScalar(ProductStockTotal.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<ProductStockTotal> lstProductSalePlan(SaleOrder tempOrder, StockTrans stockTrans, Integer type) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if(tempOrder != null && tempOrder.getShop() != null && type == 0){
			sql.append(" with cycleTemp as (select * from cycle cl where trunc(cl.begin_date) <= trunc(sysdate) and trunc(cl.end_date) >= trunc(sysdate) and cl.status = 1) ");
			sql.append(" select sp.shop_id as shopId,sp.product_id as productId, ");
			sql.append(" ceil((sp.quantity * (select psm.nessesary_stock_day from product_shop_map psm where psm.shop_id = sp.shop_id and psm.product_id = sp.product_id))/(select (select cl.end_Date - cl.begin_date + 1 - count(*) from exception_day ex where ex.shop_id = 230 and trunc(ex.day_off) >= trunc(cl.begin_date) and trunc(ex.day_off) <= trunc(cl.end_date)) as datediff from cycleTemp cl)) as nesStock ");   
			sql.append(" from sale_plan sp, cycleTemp cl ");
			sql.append(" where 1=1  ");
			sql.append(" and sp.product_id in(select sod.product_id from sale_order so,sale_order_detail sod where sod.sale_order_id = so.sale_order_id and so.sale_order_id = ?) ");
			sql.append(" and sp.object_id = ? ");
			sql.append(" and sp.object_type = ? and cl.cycle_id = sp.cycle_id ");
			sql.append(" order by sp.product_id ");
			params.add(tempOrder.getId());
			params.add(tempOrder.getShop().getId());
			params.add(SalePlanOwnerType.SHOP.getValue());
			
		}
		
		if(stockTrans != null && stockTrans.getShop() != null && type == 1){
			sql.append(" with cycleTemp as (select * from cycle cl where trunc(cl.begin_date) <= trunc(sysdate) and trunc(cl.end_date) >= trunc(sysdate) and cl.status = 1) ");
			sql.append(" select sp.shop_id as shopId,sp.product_id as productId, ");
			sql.append(" ceil((sp.quantity * (select psm.nessesary_stock_day from product_shop_map psm where psm.shop_id = sp.shop_id and psm.product_id = sp.product_id))/(select (select cl.end_Date - cl.begin_date + 1 - count(*) from exception_day ex where ex.shop_id = 230 and trunc(ex.day_off) >= trunc(cl.begin_date) and trunc(ex.day_off) <= trunc(cl.end_date)) as datediff from cycleTemp cl)) as nesStock ");   
			sql.append(" from sale_plan sp, cycleTemp cl ");
			sql.append(" where 1=1  ");
			sql.append(" and sp.product_id in(select sod.product_id from stock_trans so,stock_trans_detail sod where sod.stock_trans_id = so.stock_trans_id and so.stock_trans_id = ?) ");
			sql.append(" and sp.object_id = ? ");
			sql.append(" and sp.object_type = ? and cl.cycle_id = sp.cycle_id ");
			sql.append(" order by sp.product_id ");
			params.add(stockTrans.getId());
			params.add(stockTrans.getShop().getId());
			params.add(SalePlanOwnerType.SHOP.getValue());
		}
		
		
//		sql.append(" with cycleTemp as (select * from cycle cl where trunc(cl.begin_date) <= trunc(sysdate) and trunc(cl.end_date) >= trunc(sysdate) and cl.status = 1) ");
//		sql.append(" select sp.shop_id as shopId,sp.product_id as productId,");
//		sql.append(" ceil((sp.quantity * (select psm.nessesary_stock_day from product_shop_map psm where psm.shop_id = sp.shop_id))/(select cl.end_Date - cl.begin_date  as datediff from cycleTemp cl)) as nesStock "); 
//		sql.append(" from sale_plan sp, cycleTemp cl");
//		sql.append(" where cl.cycle_id = sp.cycle_id and sp.product_id in(-1 ");
//		for(Long id: lstProductId){
//			sql.append(",?");
//			params.add(id);
//		}
//		sql.append(")");
//		sql.append(" and sp.object_id in(-1 ");
//		for(Long id: lstShopId){
//			sql.append(",?");
//			params.add(id);
//		}
//		sql.append(")");
//		sql.append(" and sp.object_type = 3 ");
//		sql.append(" order by sp.product_id ");
		final String[] fieldNames = new String[] { "productId", "nesStock"};

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.LONG };
		
		return repo.getListByQueryAndScalar(ProductStockTotal.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<SalePromoMap> getListSalePromoMapBySaleOrder(Long orderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from sale_promo_map where sale_order_id = ? ");
		params.add(orderId);
		return repo.getListBySQL(SalePromoMap.class, sql.toString(), params);
	}
	
	/**
	 * kiem tra dang thuc hien kiem kho
	 * @author trietptm
	 * @param saleOrderId
	 * @return
	 * @since Aug 26, 2015
	 */
	@Override
	public boolean checkStockCounting(long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT COUNT(1) COUNT ");
		sql.append(" FROM SALE_ORDER_LOT sol ");
		sql.append(" JOIN CYCLE_COUNT cc ON sol.WAREHOUSE_ID = cc.WAREHOUSE_ID AND sol.SHOP_ID = cc.SHOP_ID ");
		sql.append(" WHERE cc.STATUS IN (?) ");
		params.add(CycleCountType.ONGOING.getValue());
		sql.append(" AND sol.SALE_ORDER_ID = ? ");
		params.add(saleOrderId);
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	
	@Override
	public boolean checkStockCountingForStockTrans(StockStransFilter filter) throws DataAccessException {
		if (filter == null ) {
			throw new IllegalArgumentException("StockStransFilter filter is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT COUNT(1) as count ");
		sql.append(" FROM stock_trans st ");
		sql.append(" JOIN stock_trans_lot stl ON st.STOCK_TRANS_ID = stl.STOCK_TRANS_ID ");
		sql.append(" JOIN cycle_count cc ON (stl.FROM_OWNER_ID = cc.warehouse_id AND st.TRANS_TYPE = 'DP' AND stl.FROM_OWNER_TYPE = ?)  ");
		params.add(StockTransLotOwnerType.WAREHOUSE.getValue());
		sql.append("                     OR (stl.TO_OWNER_ID = cc.warehouse_id AND st.TRANS_TYPE = 'GO' AND stl.TO_OWNER_TYPE = ?) ");
		params.add(StockTransLotOwnerType.WAREHOUSE.getValue());
		sql.append(" WHERE cc.STATUS = ? ");
		params.add(CycleCountType.ONGOING.getValue());
		if (filter.getStockTransId() != null) {
			sql.append(" AND st.STOCK_TRANS_ID = ? ");
			params.add(filter.getStockTransId());
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	
	@Override
	public boolean checkStockTransCycleCount(StockStransFilter filter) throws DataAccessException {
		if (filter == null ) {
			throw new IllegalArgumentException("StockStransFilter filter is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count ");
		sql.append(" from stock_trans_lot stl ");
		if (filter.getFromOwnerId() != null) {
			sql.append(" join cycle_count cc ");
			sql.append(" on stl.from_owner_id = cc.warehouse_id ");
		} else if (filter.getToOwnerID() != null) {
			sql.append(" join cycle_count cc ");
			sql.append(" on stl.to_owner_id = cc.warehouse_id ");
		}
		sql.append(" where cc.status in (?) ");
		params.add(CycleCountType.ONGOING.getValue());
		if (filter.getStockTransId() != null) {
			sql.append(" and stl.stock_trans_id = ? ");
			params.add(filter.getStockTransId());
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public List<PromotionMapDeltaVO> getSaleOrderDelta(List<Long> lstID) throws DataAccessException {
		// TODO Auto-generated method stub
		List<Object> params = new ArrayList<Object>();
		
		StringBuilder varname1 = new StringBuilder();
		varname1.append("SELECT DISTINCT pshm.promotion_shop_map_id    promotionShopMapID, ");
		varname1.append("                pstm.promotion_staff_map_id   promotionStaffMapID, ");
		varname1.append("                pcm.promotion_customer_map_id promotionCustomerMapID, ");
		varname1.append("                so.sale_order_id              fromObjectID, ");
		varname1.append("                so.shop_id                    shopID, ");
		varname1.append("                so.staff_id                   staffID, ");
		varname1.append("                so.customer_id                customerID, ");
		varname1.append("                so.order_type                 orderType, ");
		varname1.append("                sop.quantity                  quantity, ");
		varname1.append("                sod.num                       num, ");
		varname1.append("                SUM(sopd.amount)              amount, ");
		varname1.append("                sop.promotion_program_id      promotionProgramID ");
		varname1.append("FROM   sale_order so ");
		varname1.append("       left join (SELECT SUM(sop.quantity_received) quantity, ");
		varname1.append("                         sop.sale_order_id, ");
		varname1.append("                         sop.promotion_program_id, ");
		varname1.append("                         sop.promotion_program_code ");
		varname1.append("                  FROM   sale_order_promotion sop ");
		varname1.append("                  WHERE  1 = 1 ");
		varname1.append("                  GROUP  BY sop.sale_order_id, ");
		varname1.append("                            sop.promotion_program_id, ");
		varname1.append("                            sop.promotion_program_code) sop ");
		varname1.append("              ON sop.sale_order_id = so.sale_order_id ");
		varname1.append("       left join (SELECT SUM(sod.quantity) num, ");
		varname1.append("                         sod.sale_order_id, ");
		varname1.append("                         sod.program_code ");
		varname1.append("                  FROM   sale_order_detail sod ");
		varname1.append("                  WHERE  1 = 1 ");
		varname1.append("                         AND is_free_item = 1 ");
		varname1.append("                  GROUP  BY sod.sale_order_id, sod.program_code) sod ");
		varname1.append("              ON sod.sale_order_id = so.sale_order_id ");
		varname1.append("              and sod.program_code = sop.promotion_program_code ");
		varname1.append("       left join (SELECT sopd.discount_amount amount, ");
		varname1.append("                         sopd.sale_order_id, ");
		varname1.append("                         sopd.program_code ");
		varname1.append("                  FROM   sale_order_promo_detail sopd ");
		varname1.append("                  WHERE  1 = 1) sopd ");
		varname1.append("              ON sopd.sale_order_id = so.sale_order_id ");
		varname1.append("                 AND sopd.program_code = sop.promotion_program_code ");
		varname1.append("       left join (SELECT m.promotion_shop_map_id, ");
		varname1.append("                         m.promotion_program_id ");
		varname1.append("                  FROM   promotion_shop_map m ");
		varname1.append("                  WHERE  1 = 1 ");
		varname1.append("                         AND m.status = 1 ");
		varname1.append("                         AND m.shop_id IN (SELECT shop_id ");
		varname1.append("                                           FROM   shop ");
		varname1.append("                                           WHERE  status = 1 ");
		varname1.append("                                           START WITH shop_id IN (SELECT shop_id ");
		varname1.append("                                                                  FROM ");
		varname1.append("                                                      sale_order ");
		varname1.append("                                                                  WHERE ");
		varname1.append("                                                      sale_order_id IN ( -1 ");
		for (Long soID : lstID) {
			varname1.append(", ?");
			params.add(soID);
		}
		varname1.append("                                                     ) ) ");
		varname1.append("                                           CONNECT BY PRIOR ");
		varname1.append("                                           shop_id = parent_shop_id ");
		varname1.append("                                           UNION ALL ");
		varname1.append("                                           SELECT shop_id ");
		varname1.append("                                           FROM   shop ");
		varname1.append("                                           WHERE  status = 1 ");
		varname1.append("                                           START WITH shop_id IN (SELECT shop_id ");
		varname1.append("                                                                  FROM ");
		varname1.append("                                                      sale_order ");
		varname1.append("                                                                  WHERE ");
		varname1.append("                                                      sale_order_id IN ( -1 ");
		for (Long soID : lstID) {
			varname1.append(", ?");
			params.add(soID);
		}
		varname1.append(" ) ) ");
		varname1.append("                                           CONNECT BY PRIOR parent_shop_id = ");
		varname1.append("                                                            shop_id)) ");
		varname1.append("       pshm ");
		varname1.append("              ON pshm.promotion_program_id = sop.promotion_program_id ");
		varname1.append("       left join promotion_staff_map pstm ");
		varname1.append("              ON pstm.shop_id = so.shop_id ");
		varname1.append("                 AND so.staff_id = pstm.staff_id ");
		varname1.append("                 AND pstm.promotion_shop_map_id = pshm.promotion_shop_map_id ");
		varname1.append("                 AND pstm.status = 1 ");
		varname1.append("       left join promotion_customer_map pcm ");
		varname1.append("              ON pcm.shop_id = so.shop_id ");
		varname1.append("                 AND so.customer_id = pcm.customer_id ");
		varname1.append("                 AND pcm.promotion_shop_map_id = pshm.promotion_shop_map_id ");
		varname1.append("                 AND pcm.status = 1 ");
		varname1.append("WHERE  1 = 1 ");
		varname1.append("       AND so.sale_order_id IN ( -1 ");
		for (Long soID : lstID) {
			varname1.append(", ?");
			params.add(soID);
		}
		varname1.append(" ) ");
		varname1.append(" and sop.promotion_program_id is not null ");
		varname1.append("GROUP  BY sop.promotion_program_id  , ");
		varname1.append("pshm.promotion_shop_map_id, ");
		varname1.append("                pstm.promotion_staff_map_id , ");
		varname1.append("                pcm.promotion_customer_map_id, ");
		varname1.append("                so.sale_order_id, ");
		varname1.append("                so.shop_id , ");
		varname1.append("                so.staff_id , ");
		varname1.append("                so.customer_id , ");
		varname1.append("                so.order_type , ");
		varname1.append("                sop.quantity , ");
		varname1.append("                sod.num");
		
		final String[] fieldNames = new String[] { //
				"fromObjectID",// 1
				"quantity", // 2
				"amount", // 3
				"num", // 4
				"promotionShopMapID", // 5
				"promotionStaffMapID", // 6
				"promotionCustomerMapID", // 7
				"promotionProgramID"
				, "shopID"
				, "customerID"
				, "staffID"
				, "orderType"
		};

		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.LONG, // 1
				StandardBasicTypes.BIG_DECIMAL, // 2
				StandardBasicTypes.BIG_DECIMAL, // 3
				StandardBasicTypes.BIG_DECIMAL, // 4
				StandardBasicTypes.LONG, // 5
				StandardBasicTypes.LONG, // 6
				StandardBasicTypes.LONG, // 7
				StandardBasicTypes.LONG, // 8
				StandardBasicTypes.LONG, // 9
				StandardBasicTypes.LONG, // 10
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING
		};
		/*StringBuilder countSql = new StringBuilder("select count(1) as count from (");
		countSql.append(varname1);
		countSql.append(")");*/
		return repo.getListByQueryAndScalar(PromotionMapDeltaVO.class, fieldNames, fieldTypes, varname1.toString(), params);
//		repo.executeSQLQuery(sql, params)
	}

	@Override
	public List<SaleOrder> getListCustomerNotApproved(Long customerID) throws DataAccessException {
		// TODO Auto-generated method stub
		List<Object> params = new ArrayList<Object>();
		StringBuilder  varname1 = new StringBuilder();
		varname1.append("select * from sale_order where 1 = 1 ");
		varname1.append("and order_type in ('IN', 'SO') ");
		varname1.append("and customer_id = ? ");
		params.add(customerID);
		varname1.append("and approved = 0");
		return repo.getListBySQL(SaleOrder.class, varname1.toString(), params);
	}

	@Override
	public List<OrderProductVO> getListSaleOrderID(List<Long> listSaleOrderID) throws DataAccessException {
		// TODO Auto-generated method stub
		List<Object> params = new ArrayList<Object>();
		StringBuilder  varname1 = new StringBuilder();
		varname1.append("SELECT sale_order_id saleOrderId, order_number orderNumber ");
		varname1.append("FROM   sale_order so ");
		varname1.append("       join customer cus ");
		varname1.append("         ON cus.customer_id = so.customer_id ");
		varname1.append("            AND cus.status = 0 ");
		varname1.append("WHERE  1 = 1 ");
		varname1.append("       AND sale_order_id IN ( -1 ");
		for (Long saleOrderID : listSaleOrderID) {
			varname1.append(", ?");
			params.add(saleOrderID);
		}
		varname1.append(") ");
		
		String[] fieldNames = new String[] {"saleOrderId", "orderNumber"};

		Type[] fieldTypes = new Type[] {StandardBasicTypes.LONG, StandardBasicTypes.STRING};
		
		return repo.getListByQueryAndScalar(OrderProductVO.class, fieldNames, fieldTypes, varname1.toString(), params);
	}
}

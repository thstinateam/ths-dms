package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.ShopProduct;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopProductType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface ShopProductDAO {

	ShopProduct createShopProduct(ShopProduct shopProduct, LogInfoVO logInfo) throws DataAccessException;

	void deleteShopProduct(ShopProduct shopProduct, LogInfoVO logInfo) throws DataAccessException;

	void updateShopProduct(ShopProduct shopProduct, LogInfoVO logInfo) throws DataAccessException;

	Boolean isUsingByOthers(long shopProductId) throws DataAccessException;

	ShopProduct getShopProductById(long id) throws DataAccessException;

	ShopProduct getShopProductByCat(Long shopId, Long catId)
			throws DataAccessException;

	/**
	 * Gets the shop product by product.
	 *
	 * @author phut
	 *
	 * @param shopId the shop id
	 * @param productId the product id
	 * @return the shop product by product
	 * @throws DataAccessException the data access exception
	 */
	ShopProduct getShopProductByProduct(Long shopId, Long productId)
			throws DataAccessException;

	ShopProduct getShopProductByImport(String shopCode, String codeFlag, String calendarD, int flag) throws DataAccessException;

	List<ShopProduct> getLitsShopProductByListId(List<Long> lst)
			throws DataAccessException;

	List<ShopProduct> getListShopProduct(KPaging<ShopProduct> kPaging,
			String shopCode, String shopName, List<String> listProInfoCode,
			String productCode, ActiveType status, ShopProductType type,
			List<Long> lstParentShopId, Boolean flag) throws DataAccessException;

	List<ShopProduct> getListShopProduct(KPaging<ShopProduct> kPaging,
			String shopCode, String shopName, Long catId, String productCode,
			ActiveType status, ShopProductType type, List<Long> listOrgAccess)
			throws DataAccessException;

}

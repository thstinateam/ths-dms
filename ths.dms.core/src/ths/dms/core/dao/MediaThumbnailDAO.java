package ths.dms.core.dao;

import ths.dms.core.entities.MediaThumbnail;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface MediaThumbnailDAO {

	MediaThumbnail createMediaThumbnail(MediaThumbnail mediaThumbnail, LogInfoVO logInfo) throws DataAccessException;

	void deleteMediaThumbnail(MediaThumbnail mediaThumbnail, LogInfoVO logInfo) throws DataAccessException;

	void updateMediaThumbnail(MediaThumbnail mediaThumbnail, LogInfoVO logInfo) throws DataAccessException;
	
	MediaThumbnail getMediaThumbnailById(long id) throws DataAccessException;

//	List<MediaThumbnail> getListMediaThumbnailByMediaType(MediaType mediaType) throws DataAccessException;
//
//	List<MediaThumbnail> getListMediaThumbnailByObjectIdAndObjectType(
//			KPaging<MediaThumbnail> kPaging, Long objectId,
//			MediaObjectType objectType) throws DataAccessException;

//	List<MediaThumbnail> getListMediaThumbnailByProduct(Long productId)
//			throws DataAccessException;
}

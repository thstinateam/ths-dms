package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.filter.DebitFilter;
import ths.dms.core.entities.vo.CustomerDebitVO;
import ths.dms.core.entities.vo.DebitShopVO;
import ths.core.entities.vo.rpt.RptDebtOfCustomerDataVO;
import ths.core.entities.vo.rpt.RptLimitDebitVO;
import ths.dms.core.exceptions.DataAccessException;

public interface DebitDetailDAO {

	DebitDetail createDebitDetail(DebitDetail debitDetail) throws DataAccessException;

	void deleteDebitDetail(DebitDetail debitDetail) throws DataAccessException;

	void updateDebitDetail(DebitDetail debitDetail) throws DataAccessException;
	
	void updateDebitDetail(List<DebitDetail> debitDetail) throws DataAccessException;
	
	DebitDetail getDebitDetailById(long id) throws DataAccessException;

//	BigDecimal getDebitOfShop(Long shopId, PoType poType) throws DataAccessException;

	boolean updateDebtForShop(long debitId, BigDecimal amount);

	/**
	 * lay debit de update khi update nhap hang
	 * @author thanhnguyen
	 * @param PoVnmId
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 */
	DebitDetail getDebitDetailForUpdatePo(Long PoVnmId, Long shopId, Long debitDetailId) throws DataAccessException;

	/**
	 * Lay debit detail cua PO confirm
	 * Chua khai bao Mgr
	 * @author vuongmq
	 * @param PoVnmId
	 * @param debitId
	 * @return
	 * @throws DataAccessException
	 * @since 12/08/2015 
	 */
	DebitDetail getDebitDetailForUpdatePoVNM(Long PoVnmId, Long debitId) throws DataAccessException;
	
	
	List<DebitShopVO> getListDebitShopVO(Long shopId, PoType poType)
			throws DataAccessException;

	/**
	 * getDebitByIdForUpdate
	 * @author hieunq1
	 * @param id
	 * @return
	 * @throws DataAccessException
	 */
	DebitDetail getDebitDetailByIdForUpdate(long id) throws DataAccessException;

	List<DebitDetail> getListPositiveDebitDetail(KPaging<DebitDetail> kPaging, String shortCode,
			Long fromObjectId, DebitOwnerType type, BigDecimal fromAmount,
			BigDecimal toAmount, Long shopId) throws DataAccessException;

//	/**
//	 * 
//	 * @author hieunq1
//	 * @param kPaging
//	 * @param shortCode
//	 * @param fromObjectId
//	 * @param type
//	 * @param fromAmount
//	 * @param toAmount
//	 * @param shopId
//	 * @return
//	 * @throws DataAccessException
//	 */
//	List<Debit> getListDiffZeroDebitDetail(KPaging<Debit> kPaging, String shortCode,
//			Long fromObjectId, DebitGeneralOwnerType type, BigDecimal fromAmount,
//			BigDecimal toAmount, Long shopId) throws DataAccessException;
	/**
	 * Lay du lieu cho Bao cao Tong cong no khach hang
	 * 
	 * @param shopId
	 * @param customerIds
	 * @param reportDate
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<RptDebtOfCustomerDataVO> getDataForReportDebtOfCustomer(Long shopId,
			List<Long> customerIds, Date reportDate) throws DataAccessException;
	
	List<RptLimitDebitVO> getListLimitDebitDetail(Long shopId, Date date, String customerCode) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @param staffCode
	 * @param custName
	 * @param shortCode
	 * @param orderNumber
	 * @param fromAmount
	 * @param toAmount
	 * @param isReveived
	 * @return
	 * @throws DataAccessException
	 */
	BigDecimal getCustomerTotalDebitDetail(Long shopId, String staffCode,
			String custName, String shortCode, String orderNumber,
			BigDecimal fromAmount, BigDecimal toAmount, Boolean isReveived, Long deliveryId, Long cashierId, Date fromDate, Date toDate)
			throws DataAccessException;

	/**
	 * Lay cong no KH
	 * 
	 * @author hieunq1
	 * @modify by lacnv1 - them id NVBH
	 * @modify date Mar 26, 2014
	 */

	List<CustomerDebitVO> getListCustomerDebitVO(Long shopId, String shortCode,
			String orderNumber, BigDecimal fromAmount, BigDecimal toAmount,
			Boolean isReveived, Date fromDate, Date toDate, Long nvghId,
			Long nvttId, Long nvbhId) throws DataAccessException;
	
	/**
	 * Lay danh sach cong no KH theo filter
	 * 
	 * @param filter - doi tuong chua dieu kien loc du lieu
	 * 
	 * @author lacnv1
	 * @since Apr 08, 2015
	 * 
	 * @see getListCustomerDebitVO
	 */
	List<CustomerDebitVO> getListCustomerDebitVO(DebitFilter<CustomerDebitVO> filter) throws DataAccessException;


	/**
	 * Lay no cua NPP voi VNM
	 * 
	 * @author lacnv1
	 * @since Apr 07, 2014
	 */
	List<DebitShopVO> getDebitVNMOfShop(Long shopId, Long typeInvoice) throws DataAccessException;
	List<DebitShopVO> getDebitVNMOfShopAndId(Long shopId, List<Long> listDebitDetailId) throws DataAccessException;
	/**
	 * Lay debit detail bang ma cua don hang
	 * @param id
	 * @return
	 * @author tungtt21
	 * @throws DataAccessException
	 */
	DebitDetail getDebitDetailBySaleOrderId(long id) throws DataAccessException;

	DebitDetail getDebitDetailByFromObjectId(long fromObjectId)
			throws DataAccessException;

	List<SaleOrder> getListSaleOderByListDebitdetailId(
			List<Long> lstDebitdetailId) throws DataAccessException;

	List<CustomerDebitVO> getListCustomerDebitVOEx(Long shopId,
			String shortCode, String orderNumber, BigDecimal fromAmount,
			BigDecimal toAmount, Boolean isReveived, Date fromDate,
			Date toDate, Long nvghId, Long nvttId, Long nvbhId,
			List<Long> lstDebitId) throws DataAccessException;

	List<DebitDetail> getListDebitDetailByFilter(SoFilter filter) throws DataAccessException;
	
	/**
	 * Check so phieu dieu chinh cong no da ton tai chua
	 * 
	 * @author lacnv1
	 * @since Sep 04, 2014
	 */
	boolean existsDebitNumber(long shopId, String number) throws DataAccessException;
	
	/**
	 * Tao debit_detail_temp tu debit_detail
	 * 
	 * @author lacnv1
	 * @since Nov 20, 2014
	 */
	void cloneDebitDetailToTemp(DebitDetail debitDetail) throws DataAccessException;
	
	/**
	 * Cap nhat lai so son hang cho bang debit_detail_temp
	 * 
	 * @author lacnv1
	 * @since Dec 23, 2014
	 */
	void updateOrderNumberForDebitDetailTemp(SaleOrder so) throws DataAccessException;
	
	/**
	 * Lay debit_detail dua theo from_object_number, shop_id
	 * 
	 * @author lacnv1
	 * @since Mar 25, 2015
	 */
	DebitDetail getDebitDetailByObjectNumber(long shopId, String fromObjectNumber, Date pDate) throws DataAccessException;
}

package ths.dms.core.dao;

import ths.dms.core.entities.RptStockTotalMonth;
import ths.dms.core.exceptions.DataAccessException;

public interface RptStockTotalMonthDAO {

	RptStockTotalMonth createRptStockTotalMonth(RptStockTotalMonth rptStockTotalMonth) throws DataAccessException;

	void deleteRptStockTotalMonth(RptStockTotalMonth rptStockTotalMonth) throws DataAccessException;

	void updateRptStockTotalMonth(RptStockTotalMonth rptStockTotalMonth) throws DataAccessException;
	
	RptStockTotalMonth getRptStockTotalMonthById(long id) throws DataAccessException;
}

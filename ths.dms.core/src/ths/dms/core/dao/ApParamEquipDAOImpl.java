package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ApParamEquip;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamEquipType;
import ths.dms.core.entities.enumtype.ApParamFilter;
import ths.dms.core.entities.vo.ApParamEquipVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.entities.vo.StatisticCheckingVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class ApParamEquipDAOImpl implements ApParamEquipDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * tao moi ApParamEquip
	 */
	@Override
	public ApParamEquip createApParamEquip(ApParamEquip apParam, LogInfoVO logInfo) throws DataAccessException {
		if (apParam == null) {
			throw new IllegalArgumentException("apParam");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (apParam.getApParamEquipCode() != null)
			apParam.setApParamEquipCode(apParam.getApParamEquipCode().toUpperCase());
		repo.create(apParam);
		apParam = repo.getEntityById(ApParamEquip.class, apParam.getId());
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, apParam, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return apParam;
	}

	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * delete ApParamEquip
	 */
	@Override
	public void deleteApParamEquip(ApParamEquip apParam, LogInfoVO logInfo) throws DataAccessException {
		if (apParam == null) {
			throw new IllegalArgumentException("apParam");
		}
		repo.delete(apParam);
		try {
			actionGeneralLogDAO.createActionGeneralLog(apParam, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * update ApParamEquip
	 */
	@Override
	public void updateApParamEquip(ApParamEquip apParam, LogInfoVO logInfo) throws DataAccessException {
		if (apParam == null) {
			throw new IllegalArgumentException("apParam");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (apParam.getApParamEquipCode() != null)
			apParam.setApParamEquipCode(apParam.getApParamEquipCode().toUpperCase());
		ApParamEquip temp = this.getApParamEquipById(apParam.getId());
		repo.update(apParam);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, apParam, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * lay ApParamEquip theo id
	 */
	@Override
	public ApParamEquip getApParamEquipById(long id) throws DataAccessException {
		return repo.getEntityById(ApParamEquip.class, id);
	}
	
	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * lay ApParamEquip theo code va type
	 */
	@Override
	public ApParamEquip getApParamEquipByCode(String code, ApParamEquipType type)	throws DataAccessException {
		code = code.toUpperCase();
		StringBuilder sql = new StringBuilder();
		sql.append("select * from equip_param where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and equip_param_code=? ");
			params.add(code.toUpperCase());
		}
		if (type != null) {
			sql.append(" and type=? ");
			params.add(type.getValue());
		}
		sql.append(" and status = ?");
		params.add(ActiveType.RUNNING.getValue());
		return repo.getEntityBySQL(ApParamEquip.class, sql.toString(), params);
	}
	
	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * lay ApParamEquip theo code, type va status
	 */
	@Override
	public ApParamEquip getApParamEquipByCodeX(String code, ApParamEquipType type, ActiveType status) throws DataAccessException {
		code = code.toUpperCase();
		StringBuilder sql = new StringBuilder();
		sql.append("select * from equip_param where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and equip_param_code=? ");
			params.add(code.toUpperCase());
		}
		if (type != null) {
			sql.append(" and type=? ");
			params.add(type.getValue());
		}
		if (status != null) {
			sql.append(" and status = ?");
			params.add(status.getValue());
		} else {
			sql.append(" and status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		return repo.getEntityBySQL(ApParamEquip.class, sql.toString(), params);
	}

	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * lay ApParamEquip theo code va status
	 */
	@Override
	public ApParamEquip getApParamEquipByCodeActive(String code, ActiveType status) throws DataAccessException {
		code = code.toUpperCase();
		StringBuilder sql = new StringBuilder();

		sql.append("select * from equip_param where 1=1");
		List<Object> params = new ArrayList<Object>();

		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and lower(equip_param_code) = ?");
			params.add(code.trim().toLowerCase());
		}

		if (status != null) {
			sql.append(" and STATUS = ?");
			params.add(status.getValue());
		}
		return repo.getEntityBySQL(ApParamEquip.class, sql.toString(), params);
	}
	
	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * lay danh sach ApParamEquip theo type va status
	 */
	@Override
	public List<ApParamEquip> getListApParamEquipByType(ApParamEquipType type, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_param where 1=1");
		if (type != null) {
			sql.append(" and type=?");
			params.add(type.getValue());
		}
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and STATUS <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		/*if (type != null 
			&& (ApParamType.PRODUCT_SUBCAT_MAP.equals(type)
			|| ApParamType.ACTIVE_TYPE_STATUS.equals(type))) {
			
			sql.append(" order by equip_param_id");
		} else {
			sql.append(" order by equip_param_code");
		}*/
		sql.append(" order by equip_param_code");
		return repo.getListBySQL(ApParamEquip.class, sql.toString(), params);
	}
	
	/**
	 * @author vuongmq
	 * @date 15/04/2015
	 * lay danh sach ApParamEquip theo filter
	 */
	@Override
	public List<ApParamEquip> getListApParamEquipByFilter(ApParamFilter filter)  throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from equip_param where 1=1");

		if (!StringUtility.isNullOrEmpty(filter.getApParamCode())) {
			sql.append(" and upper(equip_param_code) = ?");
			params.add(filter.getApParamCode().toUpperCase());
		}

		if (!StringUtility.isNullOrEmpty(filter.getDescription())) {
			sql.append(" and upper(description) = ?");
			params.add(filter.getDescription().toUpperCase());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getTypeStr())) {
			sql.append(" and upper(type) = ?");
			params.add(filter.getTypeStr().toUpperCase());
		}

		if (null != filter.getStatus()) {
			sql.append(" and status = ?");
			params.add(filter.getStatus().getValue());
		}
		
		if (null != filter.getType()) {
			sql.append(" and type = ?");
			params.add(filter.getType().getValue());
		}
		
		if(null != filter.getValue()){
			sql.append(" and upper(value) =  ?");
			params.add(filter.getValue().toUpperCase());
		}
		
		sql.append(" order by equip_param_code ");
		if(filter.getkPaging()!=null){
			return repo.getListBySQLPaginated(ApParamEquip.class, sql.toString(), params,filter.getKPagingParamEquip());
		}else{
			return repo.getListBySQL(ApParamEquip.class, sql.toString(), params);
		}
	}
	
	/**
	 * @author vuongmq
	 * @date 22/05/2015
	 * lay danh sach email param repair to (o trang thai phieu sua chua cho duyet gui mail)
	 */
	@Override
	public List<ApParamEquipVO> getListEmailToRepairByFilter(ApParamFilter filter)  throws DataAccessException {
		if (filter.getShopId() == null || StringUtility.isNullOrEmpty(filter.getTypeStr())) {
			throw new IllegalArgumentException(" shopId or paramTypeStr is null ");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT shop_id as shopId, shop_name as shopName, name as apParamName ");
		sql.append(" FROM ");
		sql.append(" ( ");
		sql.append(" SELECT sh.shop_code, sh.shop_name, sh.shop_id, sh.parent_shop_id, ep.equip_param_name as name ");
		sql.append(" FROM shop sh left join equip_param ep on sh.shop_id = ep.shop_id and ep.status = 1 ");
		if (filter.getTypeStr() != null) {
			sql.append(" and ep.type = ? ");
			params.add(filter.getTypeStr());
		}
		sql.append(" where 1=1 ");
		sql.append(" and sh.status = 1 ");
		sql.append(" ORDER BY sh.shop_code ");
		sql.append(" ) ");
		if (filter.getShopId() != null) {
			sql.append(" START WITH shop_id = ? ");
			params.add(filter.getShopId());
			sql.append(" CONNECT BY PRIOR parent_shop_id = shop_id ");
		}
		String[] fieldNames = {
				"shopId",//1
				"shopName",//2
				"apParamName" //3
			};
		Type[] fieldTypes = {
			StandardBasicTypes.LONG,//1
			StandardBasicTypes.STRING,//2
			StandardBasicTypes.STRING,//3
		};
		
		if(filter.getPagingVOEquip() == null) {
			return repo.getListByQueryAndScalar(ApParamEquipVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			String countSQL = "select count(1) as count from (" + sql.toString() + ")";
			return repo.getListByQueryAndScalarPaginated(ApParamEquipVO.class, fieldNames, fieldTypes, sql.toString(), countSQL, params, params, filter.getPagingVOEquip());
		}
	}
	
	/**
	 * Lay danh sach shop cap cha va Equip-Apparam neu co tuong ung
	 * 
	 * @author hunglm16
	 * @since 01/07/2015
	 */
	@Override
	public List<ApParamEquipVO> getListEmailToParentShopByFilter(ApParamFilter filter)  throws DataAccessException {
		if (filter.getShopId() == null || StringUtility.isNullOrEmpty(filter.getTypeStr())) {
			throw new IllegalArgumentException(" shopId or paramTypeStr is null ");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT sh.shop_name as shopName, sh.shop_id as shopId, ep.equip_param_name as apParamName ");
		sql.append(" FROM (select * from shop  ");
		if (filter.getShopId() != null) {
			sql.append(" START WITH shop_id = ? ");
			params.add(filter.getShopId());
			sql.append(" CONNECT BY PRIOR parent_shop_id = shop_id ");
		}
		sql.append(" ) sh left join equip_param ep on sh.shop_id = ep.shop_id and ep.status = 1 "); 
		if (filter.getTypeStr() != null) {
			sql.append(" and ep.type = ? ");
			params.add(filter.getTypeStr());
		}
		sql.append(" where 1 = 1  ");
		sql.append(" and sh.status = 1  ");
		sql.append(" ORDER BY sh.shop_code ");
		
		String[] fieldNames = {
				"shopId",//1
				"shopName",//2
				"apParamName" //3
			};
		Type[] fieldTypes = {
			StandardBasicTypes.LONG,//1
			StandardBasicTypes.STRING,//2
			StandardBasicTypes.STRING,//3
		};
		
		if(filter.getPagingVOEquip() == null) {
			return repo.getListByQueryAndScalar(ApParamEquipVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			String countSQL = "select count(1) as count from (" + sql.toString() + ")";
			return repo.getListByQueryAndScalarPaginated(ApParamEquipVO.class, fieldNames, fieldTypes, sql.toString(), countSQL, params, params, filter.getPagingVOEquip());
		}
	}
	
}

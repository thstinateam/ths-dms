package ths.dms.core.dao;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import ths.dms.core.entities.Product;
import ths.dms.core.entities.PromotionMapDelta;
import ths.dms.core.entities.PromotionProgram;
import ths.dms.core.entities.RptAccumulativePromotionProgram;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.SaleOrderDetail;
import ths.dms.core.entities.SaleOrderLot;
import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.SaleOrderPromotion;
import ths.dms.core.entities.SalePromoMap;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.ActionSaleOrder;
import ths.dms.core.entities.enumtype.ApprovalStatus;
import ths.dms.core.entities.enumtype.BasicVO;
import ths.dms.core.entities.enumtype.InvoiceStatus;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.PrintOrderFilter;
import ths.dms.core.entities.enumtype.ReceiptType;
import ths.dms.core.entities.enumtype.SaleOrderDetailFilter;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.SaleOrderType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.filter.SaleOrderDetailVATFilter;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PrintOrderVO;
import ths.dms.core.entities.vo.ProductStockTotal;
import ths.dms.core.entities.vo.ProgramCodeVO;
import ths.dms.core.entities.vo.PromotionMapDeltaVO;
import ths.dms.core.entities.vo.RptDSKHTMHRecordProductVO;
import ths.dms.core.entities.vo.RptDTBHTNTHVBHRecordOrderVO;
import ths.dms.core.entities.vo.RptTDBHTMHProductDateStaffFollowCustomerVO;
import ths.dms.core.entities.vo.SaleOrderDetailVO2;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderNumberVO;
import ths.dms.core.entities.vo.SaleOrderPromotionDetailVO;
import ths.dms.core.entities.vo.SaleOrderPromotionVO;
import ths.dms.core.entities.vo.SaleOrderStockDetailVO;
import ths.dms.core.entities.vo.SaleOrderStockVO;
import ths.dms.core.entities.vo.SaleOrderVO;
import ths.dms.core.entities.vo.SaleOrderVOEx2;
import ths.dms.core.entities.vo.SaleProductVO;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.core.entities.vo.rpt.RptBHTSP_DSNhanVienBH;
import ths.core.entities.vo.rpt.RptDTTHNVRecordVO;
import ths.core.entities.vo.rpt.RptOrderByPromotionDataVO;
import ths.core.entities.vo.rpt.RptPGHVO;
import ths.core.entities.vo.rpt.RptPGNVTTGVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_DataConvert;
import ths.core.entities.vo.rpt.RptPayDisplayProgrameRecordProductVO;
import ths.core.entities.vo.rpt.RptProductExchangeVO;
import ths.core.entities.vo.rpt.RptReturnSaleOrderByDeliveryDataVO;
import ths.core.entities.vo.rpt.RptRevenueInDateVO;
import ths.core.entities.vo.rpt.RptSaleOrderByProductDataVO;
import ths.core.entities.vo.rpt.RptSaleOrderInDateVO;
import ths.core.entities.vo.rpt.RptSaleOrderOfDeliveryVO;
import ths.core.entities.vo.rpt.RptSaleOrderVO;
import ths.core.entities.vo.rpt.RptTotalAmountVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface SaleOrderDAO {

	SaleOrder createSaleOrder(SaleOrder salesOrder) throws DataAccessException;

	void deleteSaleOrder(SaleOrder salesOrder) throws DataAccessException;

	void updateSaleOrder(SaleOrder salesOrder) throws DataAccessException;

	SaleOrder getSaleOrderById(long id) throws DataAccessException;
	
	SaleOrder getSaleOrderByOrderNumberAndListShop(String orderNumber, String lstShopId) throws DataAccessException;

	/**
	 * search danh sach don hang trong module tra hang
	 * 
	 * @author thanhnguyen
	 * @param kPaging
	 * @param customerCode
	 * @param customeName
	 * @param orderNumber
	 * @param approval
	 * @param orderType
	 * @param fromDate
	 * @param toDate
	 * @param staffCode
	 * @param deliveryCode
	 * @return
	 * @throws BusinessException
	 */
	List<SaleOrderVO> getListSaleOrderVO(KPaging<SaleOrderVO> kPaging, String shortCode, String customeName, String orderNumber, ApprovalStatus approval, OrderType orderType, Date fromDate, Date toDate, String staffCode, String deliveryCode)
			throws DataAccessException;

	/**
	 * lay danh sach order theo dieu kien tim kiem o module cap nhat hoa don
	 * GTGT
	 * 
	 * @author thanhnguyen
	 * @param vat
	 * @param orderNumber
	 * @param fromDate
	 * @param toDate
	 * @param customerCode
	 * @param staffCode
	 * @param status
	 * @return
	 */
	List<SaleOrder> getListSaleOrderWithCondition(KPaging<SaleOrder> kPaging, Float vat, String orderNumber, Date fromDate, Date toDate, String customerCode, String staffCode, Boolean hasInvoice) throws DataAccessException;

	/**
	 * getSaleOrderByIdForUpdate
	 * 
	 * @author hieunq1
	 * @param id
	 * @return
	 * @throws DataAccessException
	 */
	SaleOrder getSaleOrderByIdForUpdate(long id) throws DataAccessException;

	List<SaleOrder> getListSaleOrderFormTableByCondition(String orderNumber, SaleOrderStatus approved, String customerCode, String customerName, Long staffId, Integer priority, Date fromDate, Date toDate) throws DataAccessException;

	//sontt: Tong hop chi tra hang trung bay:
	List<RptPayDisplayProgrameRecordProductVO> getListRptPayDisplayProgrameRecordProductVO(Long shopId, Integer year, Long displayProgrameId) throws DataAccessException;

	//Danh sach khach hang theo mat hang
	List<RptDSKHTMHRecordProductVO> getListRptDSKHTMHRecordProductVO(Long shopId, Long customerId, Date fromDate, Date toDate) throws DataAccessException;

	// SangTN Theo doi ban hang theo mat hang
	List<RptTDBHTMHProductDateStaffFollowCustomerVO> getListRptTDBHTMHProductDateStaffFollowCustomerVO(Long shopId, List<Long> listStaffId, Date fromDate, Date toDate) throws DataAccessException;

	// end

	//Doanh thu ban hanh trong ngay theo NVBH
	List<RptDTBHTNTHVBHRecordOrderVO> getListRptDTBHTNTHVBHRecordOrderVO(Long shopId, Long staffId, Date fromDate, Date toDate) throws DataAccessException;

	//end

	/**
	 * bao cao doanh thu tong hop cua nhan vien
	 * 
	 * @author tungtt
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param listStaffID
	 * 
	 * */
	List<RptDTTHNVRecordVO> getDTTHNVReport(Long shopId, List<Long> listStaffId, Date fromDate, Date toDate) throws DataAccessException;

	/**
	 * lay danh sach bang ke cac don hang theo nhan vien giao hang
	 * 
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param deliveryId
	 * @return
	 */
	//	List<RptSaleOrderOfDeliveryVO> getListRptSaleOrderOfDelivery(Long shopId,
	//			Date fromDate, Date toDate, Long deliveryId) throws DataAccessException;

	//SangTN
	List<RptSaleOrderOfDeliveryVO> getListRptSaleOrderOfDelivery(Long shopId, Date fromDate, Date toDate, String lstDeliveryId) throws DataAccessException;

	/**
	 * @author hungnm
	 * @param kPaging
	 * @param shopId
	 * @param deliveryStaffId
	 * @param fromDate
	 * @param toDate
	 * @param getDeliveryStaff
	 * @return
	 * @throws DataAccessException
	 */
	List<RptSaleOrderVO> getListRptSaleOrderVO(KPaging<RptSaleOrderVO> kPaging, Long shopId, Long deliveryStaffId, Date fromDate, Date toDate, Boolean getDeliveryStaff) throws DataAccessException;

	/**
	 * * Kiem tra cac mat hang trong hoa don co du xuat khong
	 * 
	 * @return null: tat cac cac mat hang deu du, not null: danh sach cac mat
	 *         hang con thieu.
	 * @param saleOrderId
	 * @param shopId
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<Product> checkEnoughProductQuantityInOrder(Long saleOrderId, Long shopId) throws DataAccessException;

	/**
	 * Kiem tra mat hang trong tat ca cac don hang truyen vao co du so luong ton
	 * kho dap ung ko
	 * 
	 * @param shopId
	 * @param listOrderId
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<Product> checkEnoughProductQuantityInListOrder(Long shopId, List<Long> listOrderId) throws DataAccessException;

	/**
	 * lay danh sach bao cao phieu tra hang theo nvgh
	 * 
	 * @author thanhnguyen -> thuattq edit
	 * @param shopId
	 * @param orderType
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleOrder> getListRptReturnSaleOrder(KPaging<SaleOrder> kPaging, Long shopId, OrderType orderType, SaleOrderStatus status, SaleOrderType type, Date fromDate, Date toDate, Long deliveryId) throws DataAccessException;

	List<SaleOrder> getListSaleOrder(KPaging<SaleOrder> kPaging, Long shopId, String shortCode, String shortCode2, Float vat, InvoiceStatus invSatus, Date fromDate, Date toDate) throws DataAccessException;

	void updateSaleOrder(List<SaleOrder> lstSalesOrder) throws DataAccessException;

	void updateSaleOrderDetail(List<SaleOrderDetail> lstSalesOrder) throws DataAccessException;

	/**
	 * 3.1.5.1 Danh sách đơn hàng trong ngày theo NVBH
	 * 
	 * @author hungnm
	 * @param kPaging
	 * @param fromDate
	 * @param toDate
	 * @param staffId
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 */
	List<RptSaleOrderInDateVO> getListRptSaleOrderInDateVO(KPaging<RptSaleOrderInDateVO> kPaging, Date fromDate, Date toDate, List<Long> lstStaffId, Long shopId) throws DataAccessException;

	List<RptRevenueInDateVO> getListRptRevenueInDateVO(KPaging<RptRevenueInDateVO> kPaging, Date fromDate, Date toDate, Long staffId, Long shopId) throws DataAccessException;

	Integer getOrderDebitMaxCustomer(SaleOrder saleOrder, Date lockDay) throws DataAccessException;

	/**
	 * Gets the list sale order with invoice.
	 * 
	 * @author phut
	 * 
	 * @param saleStaffCode
	 *            the sale staff code
	 * @param deliveryStaffCode
	 *            the delivery staff code
	 * @param vat
	 *            the vat
	 * @param orderNumber
	 *            the order number
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param customerCode
	 *            the customer code
	 * @return the list sale order with invoice
	 * @throws DataAccessException
	 *             the data access exception
	 */
	/*
	 * List<SaleOrder> getListSaleOrderWithInvoice(Long shopId, String
	 * saleStaffCode, String deliveryStaffCode, Float vat, String orderNumber,
	 * Date fromDate, Date toDate, String customerCode, InvoiceCustPayment
	 * custPayment) throws DataAccessException;
	 * 
	 * List<SaleOrder> getListSaleOrderWithNoInvoice(Long shopId, String
	 * saleStaffCode, String deliveryStaffCode, Float vat, String orderNumber,
	 * Date fromDate, Date toDate, String customerCode) throws
	 * DataAccessException;
	 */

	/**
	 * Lay du lieu cho Bao cao ban hang theo san pham
	 * 
	 * @param shopId
	 * @param staffId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<RptSaleOrderByProductDataVO> getDataForSaleOrderByProductReport(Long shopId, List<Long> staffId, Date fromDate, Date toDate) throws DataAccessException;

	/**
	 * bao cao tong hop doanh so theo nvbh
	 * 
	 * @author thanhnguyen
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @param staffId
	 * @return
	 * @throws DataAccessException
	 */
	List<RptTotalAmountVO> getListRptTotalAmount(Long shopId, Date fromDate, Date toDate, String staffCode) throws DataAccessException;

	/**
	 * Lay du lieu cho Bao cao chi tiet khuyen mai theo chuong trinh
	 * 
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<RptOrderByPromotionDataVO> getDataForOrderByPromotionReport(Long shopId, List<Long> staffId, Date fromDate, Date toDate) throws DataAccessException;

	/**
	 * @author hungnm
	 * @param kPaging
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @param programCode
	 * @param catId
	 * @return
	 * @throws DataAccessException
	 */
	List<RptProductExchangeVO> getListRptProductExchangeVO(KPaging<RptProductExchangeVO> kPaging, Date fromDate, Date toDate, Long shopId, String programCode, Long catId) throws DataAccessException;

	/**
	 * @author thanhnguyen
	 * @param shopId
	 * @param orderType
	 * @param fromDate
	 * @param toDate
	 * @param deliveryId
	 * @return
	 */
	List<RptReturnSaleOrderByDeliveryDataVO> getListRptReturnSaleOrderByDelivery(Long shopId, OrderType orderType, Date fromDate, Date toDate, Long deliveryId) throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param lstSaleOrderId
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleOrder> getListSaleOrder(List<Long> lstSaleOrderId) throws DataAccessException;

	List<SaleOrder> getListSaleOrder(KPaging<SaleOrder> kPaging, String shopCode, Long customerId, Long staffId, Date fromDate, Date toDate, SaleOrderStatus approval, OrderType orderType, Integer state) throws DataAccessException;

	/**
	 * kiem tra xem co sale order nao truyen len khong thuoc shop hay khong?
	 * 
	 * @author: thanhnn8
	 * @param shopId
	 * @param listSaleOrderId
	 * @return
	 * @throws DataAccessException
	 * @return: Boolean
	 * @throws:
	 */
	Boolean checkWithoutSaleOrderOfShop(Long shopId, List<Long> listSaleOrderId) throws DataAccessException;

	/**
	 * Gets the list sale order with no invoice ex1.
	 * 
	 * @author phut
	 * 
	 * @param shopId
	 *            the shop id
	 * @param saleStaffCode
	 *            the sale staff code
	 * @param deliveryStaffCode
	 *            the delivery staff code
	 * @param vat
	 *            the vat
	 * @param orderNumber
	 *            the order number
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @param customerCode
	 *            the customer code
	 * @return the list sale order with no invoice ex1
	 * @throws DataAccessException
	 *             the data access exception
	 */
	List<SaleOrder> getListSaleOrderWithNoInvoiceEx1(KPaging<SaleOrder> kPaging, Long shopId, String saleStaffCode, String deliveryStaffCode, String orderNumber, Date fromDate, Date toDate, String customerCode, Date lockDate, Integer isValueOrder,
			Integer isVAT, Integer numberValueOrder) throws DataAccessException;

	List<SaleOrderDetail> getListSaleOrderWithNoInvoiceEx1SOD(KPaging<SaleOrder> kPaging, Long shopId, String saleStaffCode, String deliveryStaffCode, String orderNumber, Date fromDate, Date toDate, String customerCode, Date lockDate,
			Integer isValueOrder, Integer numberValueOrder) throws DataAccessException;

	List<SaleOrder> getListSaleOrderByDate(Long customerId, Date orderDate) throws DataAccessException;

	Boolean checkSaleOrderByDate(Long customerId, Long saleOrderId, Date orderDate) throws DataAccessException;

	List<SaleOrder> getListSaleOrder(SoFilter filter) throws DataAccessException;

	/**
	 * @author sangtn
	 * @since 11-06-2014
	 * @description clone from getListSaleOrder, change result from Entity to VO
	 * @note for search sale order, sale order manager screen:
	 *       SearchSaleTransactionAction->searchOrder()
	 */
	List<SaleOrderVOEx2> getListSaleOrderEx(KPaging<SaleOrderVOEx2> kPaging, SoFilter filter) throws DataAccessException;

	List<SaleOrder> getListSaleOrderConfirm(SoFilter filter) throws DataAccessException;

	List<SaleOrderVOEx2> getListSaleOrderVOConfirm(SoFilter filter) throws DataAccessException;

	List<SaleOrderDetail> getListSaleOrderDetailConfirm(SoFilter filter) throws DataAccessException;

	List<SaleOrderStockVO> getListSaleOrderStock(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws DataAccessException;

	List<SaleOrderStockVO> getListSaleOrderStockTrans(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws DataAccessException;

	/**
	 * @author sangtn
	 * @since 10-06-2014
	 * @description clone from getListSaleOrder2, change result from Entity to
	 *              VO
	 * @note for search sale order, sale order manager screen:
	 *       SearchSaleTransactionAction->searchOrder()
	 */
	List<SaleOrderVOEx2> getListSaleOrder2Ex(KPaging<SaleOrderVOEx2> kPaging, SoFilter filter) throws DataAccessException;

	SaleOrder createSaleOrder(SaleOrder salesOrder, List<SaleProductVO> lstSaleProductVO, List<SaleProductVO> lstPromoProductVO, Boolean isUpdate, List<SaleOrderPromotionVO> lstOrderPromotion, List<SaleOrderPromotionDetailVO> lstOrderPromoDetail,LogInfoVO logInfoVO)
			throws DataAccessException;

	List<SaleOrderDetail> getListVansaleSaleOrderDetail(Long staffId, Long productId) throws DataAccessException;

	Boolean checkSaleOrderByStaff(Long staffId) throws DataAccessException;

	List<SaleOrderVO> getListSaleOrderVOByDelivery(KPaging<SaleOrderVO> kPaging, Long shopId, List<OrderType> orderType, SaleOrderStatus status, SaleOrderType type, Date fromDate, Date toDate, Long deliveryId) throws DataAccessException;

	Date checkSaleOrderInLast60days(Long customerId, Long staffId, Long saleOrderId, Date lockDay) throws DataAccessException;

	List<RptPGNVTTGVO> getPGNVTTG(Long shopId, Long deliveryStaffId, Date fromDate, Date toDate) throws DataAccessException;

	/**
	 * Mo ta chuc nang cua ham
	 * 
	 * @author: thanhnn
	 * @param shopId
	 * @param deliveryCode
	 * @param listOrderType
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @return: List<SaleOrderNumberVO>
	 * @throws:
	 */
	List<SaleOrderNumberVO> getListSaleOrderNumberVOForDelivery(Long shopId, String deliveryCode, List<OrderType> listOrderType, Date fromDate, Date toDate) throws DataAccessException;

	List<SaleOrder> getListSaleOrder(SaleOrderFilter filter, KPaging<SaleOrder> kPaging) throws DataAccessException;

	List<SaleOrder> getListSaleOrderByLstOrderNumber(KPaging<SaleOrder> kPaging, List<String> orderNumber) throws DataAccessException;

	Boolean checkSaleOrderBeforeDueDate(Long saleOrderId) throws DataAccessException;

	List<SaleOrder> getListSaleOrderForDelivery(KPaging<SaleOrder> paging, SoFilter filter) throws DataAccessException;

	Boolean checkProductLotInSO(Long saleOrderId) throws DataAccessException;
	
	Boolean checkOrderInDelay(Long staffId, int delayTime) throws DataAccessException;

	List<SaleOrderLot> getListSaleOrderLotBySaleOrderId(Long saleOrderId) throws DataAccessException;

	/**
	 * In phieu giao hang gop theo KH
	 * 
	 * @author thongnm
	 */
	List<RptPGHVO> getPGHGroupByKH(List<Long> listSaleOrderId, String strListShopId) throws DataAccessException;

	/**
	 * In phieu giao hang gop theo KH
	 * 
	 * @author thongnm
	 */
	List<RptPGHVO> getPGHGroupByNVGH(List<Long> listSaleOrderId, String strListShopId) throws DataAccessException;

	/**
	 * In phieu giao hang gop theo KH
	 * 
	 * @author thongnm
	 */
	List<RptPGHVO> getPGHGroupByDH(List<Long> listSaleOrderId, String strListShopId) throws DataAccessException;

	/**
	 * 
	 * @param shopId
	 * @param nha_phan_phoi
	 * @param dt_tu_ngay
	 * @param dt_den_ngay
	 * @param nhan_vien_ban_hang
	 * @author cangnd
	 * @return
	 * @throws DataAccessException
	 */
	List<RptBHTSP_DSNhanVienBH> getBHTSP_DSNhanVienVO(String shopCode, String dt_tu_ngay, String dt_den_ngay, String nhan_vien_ban_hang) throws DataAccessException;

	/**
	 * Đếm số lượng hóa đơn chưa được duyệt trong ngày của cửa hàng
	 * 
	 * @author tungtt21
	 * @param shopId
	 * @param now
	 * @return
	 * @throws DataAccessException
	 */
	int countSaleOrderNotApprovedOfShopInDay(Long shopId, Date lockDate) throws DataAccessException;

	/**
	 * @author tungtt21
	 * @param listSaleOrderId
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleOrder> getListSaleOrderById(List<Long> listSaleOrderId) throws DataAccessException;
	List<SaleOrder> getListSaleOrderById2(List<Long> listSaleOrderId) throws DataAccessException;

	List<SaleOrder> getListSaleOrderById(List<Long> listSaleOrderId, Integer orderType) throws DataAccessException;

	SaleOrder getSaleOrderByOrderNumberAndShopId(String orderNumber, Long shopId) throws DataAccessException;

	SaleOrder getSaleOrderByOrderNumberAndShopIdApproved(String orderNumber, Long shopId) throws DataAccessException;

	/**
	 * Generate ma cua phieu thu/chi theo ngay
	 * 
	 * @author tungtt21
	 */
	String generatePayNumber(Long shopId, ReceiptType type, Date dateNow) throws DataAccessException;

	String getLastestOrderNumberByShop(Long shopId) throws DataAccessException;

	void updateLastestOrderNumberByShop(Long shopId) throws DataAccessException;

	/**
	 * Lay danh sach sale_order dua vao Iterator
	 * 
	 * @author tungtt21
	 * @param soIterator
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleOrder> getListSaleOrderByIterator(Iterator<Long> soIterator) throws DataAccessException;

	List<SaleOrderDetail> getListSaleOrderDetailByIterator(Iterator<Long> soIterator) throws DataAccessException;

	/**
	 * lay san pham don hang co quantity
	 * @author vuongmq
	 * @param saleOrderId
	 * @param orderType
	 * @param isPrintStep dang o buoc duyet don hang
	 * @return
	 * @throws DataAccessException
	 * @since 29/08/2014
	 */
	List<SaleOrderStockDetailVO> getListSaleOrderStockDetail(long saleOrderId, String orderType, boolean isPrintStep) throws DataAccessException;
	
	/**
	 * @author vuongmq
	 * @since 29/08/2014
	 * @description lay san pham don hang co quantity cho Duyet dieu chinh kho
	 */
	List<SaleOrderStockDetailVO> getListSaleOrderStockDetailUpdate(long saleOrderId, String orderType) throws DataAccessException;

	/**
	 * Lay phieu xuat kho kiem van chuyen noi bo tu don hang
	 * 
	 * @author lacnv1
	 * @since Jun 20, 2014
	 */
	List<RptPXKKVCNB_DataConvert> getPhieuXKVCNB(List<Long> lstsoId) throws DataAccessException;

	/**
	 * Lay danh sach don hang bi doi so luong
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 */
	List<OrderProductVO> getListProductChangeQuantityWarning(SoFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach don hang bi doi so suat
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 */
	List<OrderProductVO> getListProductChangeRationWarning(SoFilter filter) throws DataAccessException;
	
	/**
	 * Lay danh sach SP vuot ton kho
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 */
	List<OrderProductVO> getListProductQuantityWarning(SoFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach SP khac gia
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 */
	List<OrderProductVO> getListProductPriceWarning(SoFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach SP co CTKM het han
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 */
	List<OrderProductVO> getListProductPromotionWarning(SoFilter filter) throws DataAccessException;
	
	/**
	 * Lay danh sach SP co CTKM het han
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 */
	List<OrderProductVO> getListWarningPromotionOfOrder(SoFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach don hang khac amount(rot chi tiet)
	 * 
	 * @author tungmt
	 * @since 21/08/2014
	 */
	List<OrderProductVO> getListProductAmountWarning(SoFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach chi tiet don hang confirm
	 * 
	 * @author tungmt
	 * @since 25/08/2014
	 */
	List<SaleOrderDetailVOEx> getListSaleOrderDetailForConfirm(SoFilter filter) throws DataAccessException;

	/**
	 * Cap nhat approved cho po customer
	 * 
	 * @author tungmt
	 * @since 27/08/2014
	 */
	void updatePOCustomer(SoFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach don hang pay
	 * 
	 * @author tungmt
	 * @since 03/09/2014
	 */
	List<SaleOrderVO> getListSaleOrderForPay(SoFilter filter) throws DataAccessException;

	/**
	 * cancel thanh toan
	 * 
	 * @author tungmt
	 * @since 03/09/2014
	 */
	void cancelPay(SoFilter filter) throws DataAccessException;

	/**
	 * coppy du lieu tu bang tam payreceived va payment_detail sang bang chinh
	 * 
	 * @author tungmt
	 * @since 03/09/2014
	 */
	void coppyPayment(List<Long> lstOrder, LogInfoVO loginfo) throws DataAccessException;

	/**
	 * coppy du lieu tu bang tam payreceived va payment_detail sang bang chinh
	 * 
	 * @author tungmt
	 * @since 03/09/2014
	 */
	List<SaleOrderVO> getListDiscountAndAmount(SoFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach don hang de in
	 * 
	 * @author tungmt
	 * @since 08/09/2014
	 */
	List<SaleOrder> getListSaleOrderForPrint(SoFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach don hang dieu chinh tang, giam
	 * 
	 * @author hunglm16
	 * @since September 9,2014
	 * */
	List<SaleOrderVO> getListSaleOrderAIADVOByFilter(SaleOrderFilter<SaleOrderVO> filter) throws DataAccessException;

	List<PrintOrderVO> getListPrintOrderVO(PrintOrderFilter filter) throws DataAccessException;

	List<SaleOrderDetailVOEx> getListSaleOrderDetailVOByFilter(SaleOrderDetailFilter<SaleOrderDetailVOEx> filter) throws DataAccessException;

	SaleOrder getLastestOrderWithException(Long shopId, Long customerId, Long exceptionId, List<OrderType> orderTypes) throws DataAccessException;

	String getLastestSOOrderNumberByShop(Long shopId) throws DataAccessException;

	String getLastestCOOrderNumberByShop(Long shopId) throws DataAccessException;

	List<SaleOrderPromotionVO> getListSOPromotionBySOId(Long saleOrderId) throws DataAccessException;

	PromotionProgram checkQuantityReceived(Long promotionId, Integer quantity, Long shopId, Long staffId, Long customerId) throws DataAccessException;

	SaleOrderVO getSaleOrderVOSingle(SaleOrderFilter<SaleOrderVO> filter) throws DataAccessException;

	/**
	 * Dem so luong don hang trong ngay chua duyet
	 * 
	 * @author lacnv1
	 * @since Sep 12, 2014
	 * 
	 * @param orderTypeGroup
	 *            = 1 ->(order_type = IN, SO, CM, CO; approved = 1,
	 *            approved_step = 3) orderTypeGroup = 2 ->(order_type = AI, AD;
	 *            approved = 1, approved_step = 2) orderTypeGroup = 3
	 *            ->(trans_type = DCT, DCG; approved = 1, approved_step = 3)
	 */
	Integer countSaleOrderApprovedYet(long shopId, Date date, int orderTypeGroup) throws DataAccessException;

	void deleteSaleOrderDetail(SaleOrderDetail saleOrderDetail) throws DataAccessException;

	void deleteSaleOrderDetail(List<SaleOrderDetail> lstSaleOrderDetail) throws DataAccessException;
	
	/**
	 * xoa saleOrderPromolot theo saleOrderId
	 * @author trietptm
	 * @param id
	 * @throws DataAccessException
	 * @since Aug 29, 2015
	 */
	void deleteSaleOrderPromoLot(long saleOrderId) throws DataAccessException;
	
	/**
	 * xoa saleOrderPromotion theo saleOrderId
	 * @author trietptm
	 * @param id
	 * @throws DataAccessException
	 * @since Aug 29, 2015
	 */
	void deleteSaleOrderPromotion(long saleOrderId) throws DataAccessException;
	
	/**
	 * xoa salePromoMap theo saleOrderId
	 * @author trietptm
	 * @param id
	 * @throws DataAccessException
	 * @since Aug 29, 2015
	 */
	void deleteSalePromoMap(long saleOrderId) throws DataAccessException;

	List<SaleOrder> getListSaleOrderExByFilter(SaleOrderFilter<SaleOrder> filter, String orderBy) throws DataAccessException;

	/**
	 * Lay ds don hang tra cua don hang
	 * 
	 * @author lacnv1
	 * @since Sep 30, 2014
	 * 
	 * @param soId
	 *            : id don hang goc
	 */
	List<SaleOrder> getListReturnOrderByOrderId(long soId, SaleOrderStatus approved, SaleOrderStep approvedStep) throws DataAccessException;

	/**
	 * Lay ds Sale order promotion theo Id sale order
	 * 
	 * @author nhanlt6
	 * @since Oct 6, 2014
	 * @param soId
	 *            : id don hang
	 */
	List<SaleOrderPromotion> getListSaleOrderPromotionBySaleOrder(Long orderId) throws DataAccessException;
	
	/**
	 * @author tungmt
	 * @version 1.0
	 * @param filter
	 * @return Lay danh sach promoLot theo orderId
	 * @exception BusinessException
	 * @see
	 * @since 18/8/2015
	 * @serial
	 */
	List<SaleOrderPromoDetail> getListSaleOrderPromoDetailBySaleOrder(Long orderId) throws DataAccessException;

	String getLastestCMOrderNumberByShop(Long shopId) throws DataAccessException;

	/**
	 * Lay ds SaleOrderDetail theo Id sale order dung cho cap nhat vat gop
	 * 
	 * @author phuongvm
	 * @since 21/10/2014
	 * @param lstSOId
	 *            ,vat,product_id
	 */
	List<SaleOrderDetail> getListSaleOrderDetailByFilter(SaleOrderDetailVATFilter filter) throws DataAccessException;

	List<SaleOrderDetail> getListSaleOrderDetailByListSaleOrderId(List<Long> lstSaleOrderId, Integer isFreeItem, boolean isOrder) throws DataAccessException;

	SaleOrder getSaleOrderByOrderNumber(String orderNumber, Long shopId) throws DataAccessException;

	/**
	 * Search list sale order
	 * @author nhanlt6
	 * @param SoFilter
	 * @throws BusinessException
	 * @date 18/11/2014
	 */
	List<SaleOrderVOEx2> searchSaleOrder(SoFilter filter) throws DataAccessException;

	/**
	 * Kiem tra xem co tra thuong khuyen mai mo moi (neu co) hay khong
	 * Tra ve: true neu duoc huong, false neu khong
	 * 
	 * @author lacnv1
	 * @since Dec 17, 2014
	 */
	boolean checkPromotionOpenOfSaleOrder(long saleOrderId, long customerId, Date lockDate, List<Long> lstPassOrderId) throws DataAccessException;
	/**
	 * Kiem tra xem co tra thuong khuyen mai mo moi (neu co) hay khong (khi duyet don)
	 * Tra ve: true neu duoc huong, false neu khong
	 * 
	 * @author lacnv1
	 * @since Jan 10, 2015
	 */
	boolean checkApprovedPromotionOpenOfSaleOrder(long saleOrderId, long customerId, Date lockDate, List<Long> lstPassOrderId) throws DataAccessException;
	
	/**
	 * Lay danh sach chuong trinh khuyen mai tich luy dang co trong don hang
	 * 
	 * @author lacnv1
	 * @since Dec 31, 2014
	 */
	List<PromotionProgram> getAccumulativeProgramOfOrder(long saleOrderId) throws DataAccessException;
	
	/**
	 * Lay du lieu tich luy cua KH trong rpt
	 * 
	 * @author lacnv1
	 * @since Dec 31, 2014
	 */
	RptAccumulativePromotionProgram getRptAccumulativePPOfOrder(Long shopId, Long promotionProgramId, Long customerId) throws DataAccessException;
	
	/**
	 * Kiem tra co duoc huong khuyen mai tich luy (neu co) hay khong
	 * Tra ve: true neu duoc huong, false neu khong
	 * 
	 * @author lacnv1
	 * @since Jan 06, 2015
	 */
	boolean checkAccumulativePromotionOfSaleOrder(long saleOrderId, Date orderDate, List<Long> lstPassOrderId) throws DataAccessException;
	
	/**
	 * Kiem tra co duoc huong khuyen mai tich luy (neu co) hay khong (cho don da duyet)
	 * Tra ve: true neu duoc huong, false neu khong
	 * 
	 * @author lacnv1
	 * @since Jan 08, 2015
	 */
	boolean checkApprovedAccumulativePromotionOfSaleOrder(long saleOrderId, Date orderDate, List<Long> lstPassOrderId) throws DataAccessException;
	
	/**
	 * Kiem tra xem co thieu hang cua khuyen mai tich luy hay khong
	 * Tra ve: true neu chi thieu hang o khuyen mai tich luy (khong bao gom thieu hang ban hoac khuyen mai khac), nguoc lai: false
	 * 
	 * @author lacnv1
	 * @since Jan 07, 2015
	 */
	List<StockTotalVO> getListWaringAccumulativeProductQuantity(long shopId, long saleOrderId) throws DataAccessException;
	
	/**
	 * Lay danh sach don CO ma don SO cua no chua qua buoc xac nhan IN
	 * 
	 * @author lacnv1
	 * @since Jan 21, 2015
	 * 
	 * @param filter.lstSaleOrderId: danh sach id cua don hang can kiem tra
	 */
	List<OrderProductVO> getCONotHaveApprovedSO(SoFilter filter) throws DataAccessException;

	List<SaleOrderVOEx2> searchStockTrans(SoFilter filter) throws DataAccessException;

	List<OrderProductVO> getListSOProductQuantityByList(SoFilter filter) throws DataAccessException;
	
	/**
	 * Luu gia tri HD khi thay doi tim kiem
	 * 
	 * @author hoanv25
	 * @since 14/2/2015
	 * 
	 */
	void updateEquipment(ShopParam numberValueOrderParam)throws DataAccessException;
	
	/**
	 * Tao moi gia tri HD khi thay doi tim kiem
	 * 
	 * @author hoanv25
	 * @since 14/2/2015
	 * 
	 */
	ShopParam createShopParamByInvoice(ShopParam numberValueOrderParam)throws DataAccessException;
	
	/**
	 * lay history theo objectId, objectType
	 * 
	 * @author tungmt
	 * @since 10/3/2015
	 * 
	 */
	boolean checkExistsProcessHistoryForOrder(long objectId, int objectType) throws DataAccessException;
	
	/**
	 * Lay danh sach don hang xuat xml
	 * 
	 * @author lacnv1
	 * @since Mar 14, 2015
	 */
	List<SaleOrderVO> getListOrderVOForXml(SoFilter filter) throws DataAccessException;
	
	/**
	 * Lay chi tiet don hang xuat xml
	 * 
	 * @author lacnv1
	 * @since Mar 14, 2015
	 */
	List<SaleOrderDetailVO2> getListOrderDetailVOForXml(long orderId, Date orderDate) throws DataAccessException;

	/**
	 * Lay so don cuoi cung va update +1 theo shopId
	 * @param shopId shop can lay theo shop_param
	 * @param orderType dung de gen order_number
	 * @return order_number tang dan theo
	 * @author tungmt
	 * @since 19/03/2015
	 * @throws DataAccessException
	 */
	String getLastestOrderNumberAndUpdateByShop(Long shopId, OrderType orderType) throws DataAccessException;
	
	/**
	 * Kiem tra nhan vien con don SO, CO chua cap nhat phai thu va kho hay khong?
	 *
	 * @param shopId - id npp cua nhan vien
	 * @param staffId - id nhan vien
	 * @param pDate - ngay can kiem tra
	 * 
	 * @return integer = so don (SO, CO) chua cap nhat phai thu va kho
	 * 
	 * @author lacnv1
	 * @since Apr 21, 2015
	 */
	int countVansaleOrderNotUpdatedStock(long shopId, long staffId, Date pDate) throws DataAccessException;
	
	/**
	 * Lay so luot in trong ngay cua npp
	 * 
	 * @param shopId - id npp
	 * @param pDate - ngay can lay so luot in
	 * 
	 * @author lacnv1
	 * @since Apr 21, 2015
	 */
	int getMaxPrintBatch(long shopId, Date pDate) throws DataAccessException;

	/**
	 * Lay ds don DC, DCT, DCG cho duyet dieu chinh kho (chua co kho)
	 * 
	 * @param kPaging
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<SaleOrderStockVO> getListSaleOrderStockUpdate(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws DataAccessException;

	/**
	 * @author trietptm
	 * @param kPaging
	 * @param filter
	 * @return Lay ds don DC, DCT, DCG cho duyet dieu chinh kho (co kho)
	 * @throws DataAccessException
	 * @since Aug 22, 2015
	 */
	List<SaleOrderStockVO> getListSaleOrderStockWarehouseUpdate(KPaging<SaleOrderStockVO> kPaging, SoFilter filter) throws DataAccessException;
	
	/**
	 * @author trietptm
	 * @param filter
	 * @return lay danh sach programCodeVO
	 * @throws DataAccessException
	 * @since Aug 17, 2015
	 */
	List<ProgramCodeVO> getListProgramCodeVO(SaleOrderFilter<SaleOrder> filter) throws DataAccessException;

	/**
	 * @author tungmt
	 * @version 1.0
	 * @param filter
	 * @return Danh sach keyshop tra thuong vuot muc cho phep
	 * @exception BusinessException
	 * @see
	 * @since 17/8/2015
	 * @serial
	 */
	List<OrderProductVO> getListKeyShopWarning(SoFilter filter) throws DataAccessException;
	
	List<ProductStockTotal> lstProductStockTotal(SaleOrder tempOrder, StockTrans stockTrans, Integer type) throws DataAccessException;
	
	List<ProductStockTotal> lstProductSalePlan(SaleOrder tempOrder, StockTrans stockTrans, Integer type) throws DataAccessException;

	/**
	 * @author tungmt
	 * @version 1.0
	 * @param orderId
	 * @return Lay danh sach promoMap theo orderId
	 * @exception BusinessException
	 * @see
	 * @since 26/8/2015
	 * @serial
	 */
	List<SalePromoMap> getListSalePromoMapBySaleOrder(Long orderId) throws DataAccessException;
	
	/**
	 * kiem tra dang thuc hien kiem kho
	 * @author trietptm
	 * @param saleOrderId
	 * @return
	 * @throws DataAccessException
	 * @since Aug 26, 2015
	 */
	boolean checkStockCounting(long saleOrderId) throws DataAccessException;

	/**
	 * Dem va Tong cac don loi buoc 1, 2, 3
	 * 
	 * @author hunglm16
	 * @param shopId
	 * @param date
	 * @param orderTypeGroup
	 * @return
	 * @throws DataAccessException
	 * @since 03/09/2015
	 */
	List<BasicVO> countAndSumSaleOrderApprovedYet(long shopId, Date date, int orderTypeGroup) throws DataAccessException;

	/**
	 * Check kiem ke kho hien tai cua don hang dang thuc hien(0): thi khong cho duyet don hang 
	 * @author vuongmq
	 * @param filter
	 * @return boolean
	 * @throws DataAccessException
	 * @since 28/09/2015
	 */
	boolean checkStockTransCycleCount(StockStransFilter filter) throws DataAccessException;

	/**
	 * kiem tra dang thuc hien kiem kho cho don DP GO
	 * @author trietptm
	 * @param filter
	 * @return boolean
	 * @throws DataAccessException
	 * @since Dec 2, 2015
	*/
	boolean checkStockCountingForStockTrans(StockStransFilter filter) throws DataAccessException;
	
	
	List<PromotionMapDeltaVO> getSaleOrderDelta(List<Long> lstID) throws DataAccessException;
	
	List<SaleOrder> getListCustomerNotApproved(Long customerID) throws DataAccessException;
	
	List<OrderProductVO> getListSaleOrderID(List<Long> listSaleOrderID) throws DataAccessException;

	/**
	 * Xu ly createPromotionMapDelta
	 * @author vuongmq
	 * @param salesOrder
	 * @param saleOrderPromotion
	 * @param actionSaleOrder
	 * @param isUpdate
	 * @return PromotionMapDelta
	 * @throws DataAccessException
	 * @since Jan 12, 2016
	*/
	PromotionMapDelta createPromotionMapDelta(SaleOrder salesOrder, SaleOrderPromotion saleOrderPromotion, ActionSaleOrder actionSaleOrder, Boolean isUpdate) throws DataAccessException;
}
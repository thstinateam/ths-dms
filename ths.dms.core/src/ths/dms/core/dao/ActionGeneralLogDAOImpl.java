package ths.dms.core.dao;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ActionAudit;
import ths.dms.core.entities.ActionAuditDetail;
import ths.dms.core.entities.CustomerCatLevel;
import ths.dms.core.entities.StaffSaleCat;
import ths.dms.core.entities.enumtype.ActionAuditType;
import ths.dms.core.entities.enumtype.ActionType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.KThreadPoolExecutor;
import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class ActionGeneralLogDAOImpl implements ActionGeneralLogDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDetailDAO actionGeneralLogDetailDAO;

	@Autowired
	private CommonDAO commonDAO;

	@Override
	public void createActionGeneralLog(final Object oldEntity, final Object newEntity, final LogInfoVO logInfo) throws DataAccessException, SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException,
			InvocationTargetException {

		if (logInfo == null)
			return;
		KThreadPoolExecutor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					String createDateClmName = "CREATE_DATE";
					ActionAudit log = new ActionAudit();
					ActionType type = ActionType.INSERT;

					Method getId = null;
					Table table = null;
					Field[] fields = null;
					Object oldEntity2 = oldEntity;
					Object newEntity2 = newEntity;

					if (newEntity2 != null) {
						try {
							Method getStatus = newEntity2.getClass().getMethod("getStatus");
							ActiveType status = (ActiveType) getStatus.invoke(newEntity2);
							if (ActiveType.DELETED.equals(status)) {
								newEntity2 = null;
							}
						} catch (Exception ex) {
							//do nothing
						}
					}

					if (newEntity2 == null && oldEntity2 == null)
						return;

					if (newEntity2 == null) {
						type = ActionType.DELETE;
						getId = oldEntity2.getClass().getMethod("getId");
						table = oldEntity2.getClass().getAnnotation(Table.class);
						fields = oldEntity2.getClass().getDeclaredFields();
					} else {
						getId = newEntity2.getClass().getMethod("getId");
						table = newEntity2.getClass().getAnnotation(Table.class);
						fields = newEntity2.getClass().getDeclaredFields();
					}
					if (oldEntity2 != null && newEntity2 != null)
						type = ActionType.UPDATE;

					String tableName = table.name();

					Long objectId = newEntity2 != null ? (Long) getId.invoke(newEntity2) : (Long) getId.invoke(oldEntity2);

					log.setActionType(type);
					log.setActionUser(logInfo.getStaffCode());
					log.setActionIp(logInfo.getIp());
					log.setTableName(tableName);
					log.setColumnName(tableName + "_ID");

					if (tableName.equals("CUSTOMER_CAT_LEVEL")) {
						CustomerCatLevel c = (CustomerCatLevel) (oldEntity2 == null ? newEntity2 : oldEntity2);
						objectId = c.getCustomer().getId();
					}
					if (tableName.equals("STAFF_SALE_CAT")) {
						StaffSaleCat c = (StaffSaleCat) (oldEntity2 == null ? newEntity2 : oldEntity2);
						objectId = c.getStaff().getId();
					}

					log.setObjectId(objectId);
					Boolean flag = false;

					for (Field field : fields) {
						String fieldName = field.getName();
						if (fieldName.equals("serialVersionUID"))
							continue;
						String fieldNamePascal = fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
						String columnName = null;
						Object oldValue = null;
						Object newValue = null;
						Method getMethod = null;

						if (oldEntity2 != null) {
							getMethod = oldEntity2.getClass().getMethod("get" + fieldNamePascal);
						} else {
							getMethod = newEntity2.getClass().getMethod("get" + fieldNamePascal);
						}
						Column c = field.getAnnotation(Column.class);
						if (c != null) {
							columnName = c.name();
							oldValue = oldEntity2 != null ? getMethod.invoke(oldEntity2) : null;
							newValue = newEntity2 != null ? getMethod.invoke(newEntity2) : null;
						} else {
							JoinColumn anotation = field.getAnnotation(JoinColumn.class);
							if (anotation != null) {
								columnName = field.getAnnotation(JoinColumn.class).name();
							} else {
								columnName = "";
							}
							Object oldObject = oldEntity2 != null ? getMethod.invoke(oldEntity2) : null;
							Object newObject = newEntity2 != null ? getMethod.invoke(newEntity2) : null;
							if (oldObject != null) {
								getId = oldObject.getClass().getMethod("getId");
								oldValue = getId.invoke(oldObject);
							}
							if (newObject != null) {
								getId = newObject.getClass().getMethod("getId");
								newValue = getId.invoke(newObject);
							}
						}
						if (oldValue != null) {
							if (oldValue.getClass().equals(Timestamp.class))
								oldValue = new Date(((Date) oldValue).getTime());
							if (oldValue.toString().equals(""))
								oldValue = null;
						}

						if (newValue != null) {
							if (newValue.getClass().equals(Timestamp.class))
								newValue = new Date(((Date) newValue).getTime());
							if (newValue.toString().equals(""))
								newValue = null;
						}
						if (type.equals(ActionType.INSERT) && createDateClmName.equals(columnName)) {
							newValue = commonDAO.getSysDate();
						}
						if ((oldValue != null && !oldValue.equals(newValue)) || (newValue != null && !newValue.equals(oldValue))) {
							//insert into log table (if not yet insert)
							if (flag == false) {
								log = createActionGeneralLog(log);
								flag = true;
							}
							//insert into detail table
							ActionAuditDetail detail = new ActionAuditDetail();
							detail.setAction(log);
							detail.setColumnName(columnName);
							detail.setTableName(tableName);
							detail.setOldValue(oldValue != null ? oldValue.toString() : null);
							detail.setNewValue(newValue != null ? newValue.toString() : null);
							actionGeneralLogDetailDAO.createActionGeneralLogDetail(detail);
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});

	}

	@Override
	public List<ActionAudit> getListActionGeneralLog(KPaging<ActionAudit> kPaging, String staffCode, Long customerId, Long staffId, Date fromDate, Date toDate) throws DataAccessException {
		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<ActionAudit>();
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select * from action_audit where 1=1");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and action_user = ?");
			params.add(staffCode.toUpperCase());
		}

		if (fromDate != null) {
			sql.append(" and action_date >= trunc(?)");
			params.add(fromDate);
		}

		if (toDate != null) {
			sql.append(" and action_date < trunc(?) + 1");
			params.add(toDate);
		}

		if (customerId != null) {
			sql.append(" and object_id=? and (lower(table_name)='customer' or lower(table_name)='customer_cat_level')");
			params.add(customerId);
		}
		if (staffId != null) {
			sql.append(" and object_id=? and (lower(table_name)='staff' or lower(table_name)='staff_sale_cat')");
			params.add(staffId);
		}
		sql.append(" order by action_date desc");
		if (kPaging != null)
			return repo.getListBySQLPaginated(ActionAudit.class, sql.toString(), params, kPaging);
		else
			return repo.getListBySQL(ActionAudit.class, sql.toString(), params);
	}

	@Override
	public ActionAudit createActionGeneralLog(ActionAudit actionGeneralLog) throws DataAccessException {
		if (actionGeneralLog == null) {
			throw new IllegalArgumentException("actionGeneralLog");
		}
		actionGeneralLog.setActionUser(actionGeneralLog.getActionUser().toUpperCase());
		repo.create(actionGeneralLog);
		return repo.getEntityById(ActionAudit.class, actionGeneralLog.getId());
	}

	@Override
	public void deleteActionGeneralLog(ActionAudit actionGeneralLog) throws DataAccessException {
		if (actionGeneralLog == null) {
			throw new IllegalArgumentException("actionGeneralLog");
		}
		repo.delete(actionGeneralLog);
	}

	@Override
	public ActionAudit getActionGeneralLogById(long id) throws DataAccessException {
		return repo.getEntityById(ActionAudit.class, id);
	}

	@Override
	public void updateActionGeneralLog(ActionAudit actionGeneralLog) throws DataAccessException {
		if (actionGeneralLog == null) {
			throw new IllegalArgumentException("actionGeneralLog");
		}
		repo.update(actionGeneralLog);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ths.dms.core.dao.ActionGeneralLogDAO#getListActionAudit(com.viettel
	 * .core.entities.enumtype.KPaging,
	 * ths.dms.core.entities.enumtype.ActionAuditType, java.util.Date,
	 * java.util.Date)
	 */
	@Override
	public List<ActionAudit> getListActionAudit(KPaging<ActionAudit> kPaging, String staffCode, Long objectId, ActionAuditType type, Date fromDate, Date toDate) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select * from action_audit where 1=1");
		if (!StringUtility.isNullOrEmpty(staffCode)) {
			sql.append(" and action_user = ?");
			params.add(staffCode.toUpperCase());
		}

		if (fromDate != null) {
			sql.append(" and action_date >= trunc(?)");
			params.add(fromDate);
		}

		if (toDate != null) {
			sql.append(" and action_date < trunc(?) + 1");
			params.add(toDate);
		}

		if (type != null) {
			sql.append(" and upper(table_name) = ?");
			params.add(type.getValue());
		}

		if (objectId != null) {
			sql.append(" and object_id = ?");
			params.add(objectId);
		}

		sql.append(" order by action_date desc");
		if (kPaging != null)
			return repo.getListBySQLPaginated(ActionAudit.class, sql.toString(), params, kPaging);
		else
			return repo.getListBySQL(ActionAudit.class, sql.toString(), params);
	}
}

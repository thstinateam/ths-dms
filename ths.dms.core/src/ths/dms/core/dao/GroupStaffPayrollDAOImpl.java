package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.GroupStaffPayroll;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.GroupStaffPayrollObjApply;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.GroupStaffPayrollVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.exceptions.ExceptionCode;
//import test.ths.dms.common.TestUtils;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class GroupStaffPayrollDAOImpl implements GroupStaffPayrollDAO {
	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public GroupStaffPayroll createGroupStaffPayroll(
			GroupStaffPayroll groupStaffPayroll, LogInfoVO logInfo)
			throws DataAccessException {
		if (groupStaffPayroll == null) {
			throw new IllegalArgumentException(
					ExceptionCode.GROUP_STAFF_PAYROLL_INSTANCE_IS_NULL);
		}
		if (logInfo == null) {
			throw new IllegalArgumentException(ExceptionCode.LOG_INFO_IS_NULL);
		}
		if (groupStaffPayroll.getGroupStaffPrCode() != null){
			groupStaffPayroll.setGroupStaffPrCode(groupStaffPayroll.getGroupStaffPrCode().toUpperCase());
		}
		groupStaffPayroll.setCreateUser(logInfo.getStaffCode());
		repo.create(groupStaffPayroll);

		groupStaffPayroll = repo.getEntityById(GroupStaffPayroll.class,
				groupStaffPayroll.getId());

		try {
			actionGeneralLogDAO.createActionGeneralLog(null, groupStaffPayroll,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}

		return groupStaffPayroll;
	}

	@Override
	public void deleteGroupStaffPayroll(GroupStaffPayroll groupStaffPayroll,
			LogInfoVO logInfo) throws DataAccessException {
		if (groupStaffPayroll == null) {
			throw new IllegalArgumentException(
					ExceptionCode.GROUP_STAFF_PAYROLL_INSTANCE_IS_NULL);
		}
		deleteGroupStaffPayrollDetail(groupStaffPayroll.getId());
		repo.delete(groupStaffPayroll);

		try {
			actionGeneralLogDAO.createActionGeneralLog(groupStaffPayroll, null,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public void updateGroupStaffPayroll(GroupStaffPayroll groupStaffPayroll,
			LogInfoVO logInfo) throws DataAccessException {
		if (groupStaffPayroll == null) {
			throw new IllegalArgumentException(
					ExceptionCode.GROUP_STAFF_PAYROLL_INSTANCE_IS_NULL);
		}
		if (logInfo == null) {
			throw new IllegalArgumentException(ExceptionCode.LOG_INFO_IS_NULL);
		}

		GroupStaffPayroll temp = this
				.getGroupStaffPayrollById(groupStaffPayroll.getId());

		repo.update(groupStaffPayroll);

		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, groupStaffPayroll,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public GroupStaffPayroll getGroupStaffPayrollById(long id)
			throws DataAccessException {
		return repo.getEntityById(GroupStaffPayroll.class, id);
	}
	
	@Override
	public GroupStaffPayroll getGroupStaffPayrollByCode(String groupStraffPrCode)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM GROUP_STAFF_PAYROLL WHERE lower(GROUP_STAFF_PAYROLL_CODE) = lower(?)");
		params.add(groupStraffPrCode);
		return repo.getEntityBySQL(GroupStaffPayroll.class, sql.toString(), params);
	}

	@Override
	public void deleteGroupStaffPayrollDetail(long groupId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("delete GROUP_STAFF_PAYROLL_DETAIL where GROUP_STAFF_PAYROLL_ID = ?");
		params.add(groupId);
		repo.executeSQLQuery(sql.toString(), params);
	}

	
	@Override
	public List<Staff> getStaffOfGroup(KPaging<Staff> kPaging,
			long groupStaffPayRollId) throws DataAccessException {
		GroupStaffPayroll group = this
				.getGroupStaffPayrollById(groupStaffPayRollId);

		if (null == group) {
			throw new DataAccessException(
					ExceptionCode.GROUP_STAFF_PAYROLL_INSTANCE_IS_NULL);
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		// lay tat ca trong shop
		if (group.getObjectApply().getValue()
				.equals(GroupStaffPayrollObjApply.SHOP.getValue())) {
			sql.append(" select s.* ");
			sql.append(" from group_staff_payroll gspr, staff s, channel_type ct ");
			sql.append(" where s.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id and status = ?) ");
			sql.append("     and s.staff_type_id = ct.channel_type_id ");
			sql.append("     and s.status = ? ");
			sql.append("     and ct.status = ? ");
			sql.append("     and ct.type = ? ");
			sql.append("     and gspr.group_staff_payroll_id = ? ");
			
			params.add(group.getShop().getId());
			params.add(ActiveType.RUNNING.getValue());
			params.add(ActiveType.RUNNING.getValue());
			params.add(ActiveType.RUNNING.getValue());
			params.add(ChannelTypeType.STAFF.getValue());
			params.add(group.getId());
			
			if (null != group.getStaffType()
					&& null != group.getStaffType().getObjectType()) {
				sql.append("     and ct.channel_type_id = ? ");
				params.add(group.getStaffType().getId());
			}
			
			sql.append(" order by lower(s.staff_name), s.staff_id ");
		} else if (group.getObjectApply().getValue()
				.equals(GroupStaffPayrollObjApply.STAFF.getValue())) {
			// chi lay staff

			sql.append(" select s.* ");
			sql.append(" from GROUP_STAFF_PAYROLL gspr, GROUP_STAFF_PAYROLL_DETAIL gspd, STAFF s, CHANNEL_TYPE ct ");
			sql.append(" where gspr.GROUP_STAFF_PAYROLL_ID = gspd.GROUP_STAFF_PAYROLL_ID ");
			sql.append(" 	and gspd.STAFF_ID = s.STAFF_ID ");
			sql.append("    and s.STAFF_TYPE_ID = ct.CHANNEL_TYPE_ID ");
			sql.append(" 	and gspr.GROUP_STAFF_PAYROLL_ID = ? ");
			sql.append("     and s.STATUS = ? ");
			sql.append("     and ct.STATUS = ? ");
			sql.append(" order by lower(s.staff_name), s.staff_id ");
			params.add(group.getId());
			params.add(ActiveType.RUNNING.getValue());
			params.add(ActiveType.RUNNING.getValue());
		} else {
			throw new DataAccessException(
					ExceptionCode.GROUP_STAFF_PAYROLL_OBJECT_APPLY_INVALIDATE);
		}

		// System.out.println(TestUtils.addParamToQuery(sql.toString(), params.toArray()));
		
		if (null == kPaging) {
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(Staff.class, sql.toString(),
					params, kPaging);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.GroupStaffPayrollDAO#getListGroupStaffPayroll(java.lang.String, java.lang.String, java.lang.Long, ths.dms.core.entities.enumtype.GroupStaffPayrollObjApply, java.lang.Long)
	 */
	@Override
	public List<GroupStaffPayrollVO> getListGroupStaffPayroll(
			KPaging<GroupStaffPayrollVO> kPaging, String groupStaffPayrollCode,
			String groupStaffPayrollName, Long shopId,
			GroupStaffPayrollObjApply objectApply, Long staffTypeId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT gsp.GROUP_STAFF_PAYROLL_CODE groupStaffPrCode,");
		sql.append(" gsp.GROUP_STAFF_PAYROLL_NAME groupStaffPrName,");
		sql.append(" gsp.OBJECT_APPLY objectApply,");
		sql.append(" (SELECT COUNT(1) AS COUNT");
		sql.append(" FROM payroll");
		sql.append(" WHERE group_staff_payroll_id = gsp.group_staff_payroll_id) AS isHasUsed,");
		sql.append(" nvl((SELECT channel_type_code");
		sql.append(" FROM channel_type");
		sql.append(" WHERE channel_type_id = gsp.staff_type_id");
		sql.append(" ), 'ALL') AS channelTypeCode,");
		sql.append(" nvl((SELECT channel_type_name");
		sql.append(" FROM channel_type");
		sql.append(" WHERE channel_type_id = gsp.staff_type_id");
		sql.append(" ), 'Tất cả') AS channelTypeName,");
		sql.append(" gsp.staff_type_id AS staffTypeId,");
		sql.append(" gsp.group_staff_payroll_id AS id");
		sql.append(" from GROUP_STAFF_PAYROLL gsp where 1 = 1");
		if (!StringUtility.isNullOrEmpty(groupStaffPayrollCode)) {
			sql.append(" and upper(gsp.GROUP_STAFF_PAYROLL_CODE) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(groupStaffPayrollCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(groupStaffPayrollName)) {
			sql.append(" and upper(gsp.GROUP_STAFF_PAYROLL_NAME) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(groupStaffPayrollName.toUpperCase()));
		}
		if (objectApply != null) {
			sql.append(" and gsp.OBJECT_APPLY = ?");
			params.add(objectApply.getValue());
		}
		if (shopId != null) {
			sql.append(" and gsp.SHOP_ID = ?");
			params.add(shopId);
		}
		if (staffTypeId != null) {
			sql.append(" and gsp.STAFF_TYPE_ID = ?");
			params.add(staffTypeId);
		}
		sql.append(" order by GROUP_STAFF_PAYROLL_CODE");
		
		String[] fieldNames = new String[] {//
				"groupStaffPrCode",// 1
				"groupStaffPrName",// 1
				"objectApply",// 2
				"isHasUsed",// 3
				"channelTypeCode",// 4
				"channelTypeName",// 5
				"staffTypeId",// 6
				"id",// 6
		};
		Type[] fieldTypes = new Type[] {//
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.STRING,// 1
				StandardBasicTypes.INTEGER,// 2
				StandardBasicTypes.INTEGER,// 3
				StandardBasicTypes.STRING,// 4
				StandardBasicTypes.STRING,// 5
				StandardBasicTypes.INTEGER,// 5
				StandardBasicTypes.LONG,// 5
		};
		if (kPaging != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append("select count(1) as count from (");
			countSql.append(sql.toString());
			countSql.append(")");
			return repo.getListByQueryAndScalarPaginated(GroupStaffPayrollVO.class,
					fieldNames, fieldTypes, sql.toString(),
					countSql.toString(), params, params, kPaging);
		}
		else {
			return repo.getListByQueryAndScalar(GroupStaffPayrollVO.class, fieldNames,
					fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public boolean checkStaffExitInGoupStaff(long groupStaffPayRollId,
			long staffId) throws DataAccessException {
		GroupStaffPayroll group = this
				.getGroupStaffPayrollById(groupStaffPayRollId);

		if (null == group) {
			throw new DataAccessException(
					ExceptionCode.GROUP_STAFF_PAYROLL_INSTANCE_IS_NULL);
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		// lay tat ca trong shop
		if (group.getObjectApply().getValue()
				.equals(GroupStaffPayrollObjApply.SHOP.getValue())) {
			sql.append(" select count(*) as count ");
			sql.append(" from GROUP_STAFF_PAYROLL gspr, STAFF s, CHANNEL_TYPE ct ");
			sql.append(" where gspr.SHOP_ID = s.SHOP_ID ");
			sql.append("     and s.STAFF_TYPE_ID = ct.CHANNEL_TYPE_ID ");
			sql.append("     and s.STATUS = ? ");
			sql.append("     and ct.STATUS = ? ");
			sql.append("     and ct.TYPE = ? ");
			sql.append("     and gspr.GROUP_STAFF_PAYROLL_ID = ? ");
			sql.append("     and s.STAFF_ID = ? ");

			params.add(ActiveType.RUNNING.getValue());
			params.add(ActiveType.RUNNING.getValue());
			params.add(ChannelTypeType.STAFF.getValue());
			params.add(group.getId());
			params.add(staffId);
		} else if (group.getObjectApply().getValue()
				.equals(GroupStaffPayrollObjApply.STAFF.getValue())) {
			// chi lay staff

			sql.append(" select count(*) as count ");
			sql.append(" from GROUP_STAFF_PAYROLL gspr, GROUP_STAFF_PAYROLL_DETAIL gspd, STAFF s ");
			sql.append(" where gspr.GROUP_STAFF_PAYROLL_ID = gspd.GROUP_STAFF_PAYROLL_ID ");
			sql.append(" 	and gspd.STAFF_ID = s.STAFF_ID ");
			sql.append(" 	and gspr.GROUP_STAFF_PAYROLL_ID = ? ");
			sql.append(" 	and s.STAFF_ID = ? ");

			params.add(group.getId());
			params.add(staffId);
		} else {
			throw new DataAccessException(
					ExceptionCode.GROUP_STAFF_PAYROLL_OBJECT_APPLY_INVALIDATE);
		}

		return this.repo.countBySQL(sql.toString(), params) > 0 ? true : false;
	}
}

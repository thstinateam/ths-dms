package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.FocusChannelMap;
import ths.dms.core.entities.FocusChannelMapProduct;
import ths.dms.core.entities.FocusProgram;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.ProductInfo;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProductType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class FocusChannelMapProductDAOImpl implements FocusChannelMapProductDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ProductDAO productDAO;
	
	@Autowired
	private FocusProgramDAO focusProgramDAO;
	
	@Autowired
	private FocusChannelMapDAO focusChannelMapDAO;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;	
	
	@Autowired
	ProductInfoDAO productInfoDAO;
	
	@Override
	public FocusChannelMapProduct createFocusChannelMapProduct(FocusChannelMapProduct focusChannelMapProduct, LogInfoVO logInfo) throws DataAccessException {
		if (focusChannelMapProduct == null) {
			throw new IllegalArgumentException("focusChannelMapProduct is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		focusChannelMapProduct.setCreateUser(logInfo.getStaffCode());
		repo.create(focusChannelMapProduct);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, focusChannelMapProduct, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(FocusChannelMapProduct.class, focusChannelMapProduct.getId());
	}

	@Override
	public void deleteFocusChannelMapProduct(FocusChannelMapProduct focusChannelMapProduct, LogInfoVO logInfo) throws DataAccessException {
		if (focusChannelMapProduct == null) {
			throw new IllegalArgumentException("focusChannelMapProduct");
		}
		repo.delete(focusChannelMapProduct);
		try {
			actionGeneralLogDAO.createActionGeneralLog(focusChannelMapProduct, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public FocusChannelMapProduct getFocusChannelMapProductById(long id) throws DataAccessException {
		return repo.getEntityById(FocusChannelMapProduct.class, id);
	}
	
	@Override
	public void updateFocusChannelMapProduct(FocusChannelMapProduct focusChannelMapProduct, LogInfoVO logInfo) throws DataAccessException {
		if (focusChannelMapProduct == null) {
			throw new IllegalArgumentException("focusChannelMapProduct");
		}
		FocusChannelMapProduct temp = this.getFocusChannelMapProductById(focusChannelMapProduct.getId());
		FocusChannelMapProduct m = new FocusChannelMapProduct();
		m.setCreateDate(temp.getCreateDate());
		m.setCreateUser(temp.getCreateUser());
		m.setFocusChannelMap(temp.getFocusChannelMap());
		m.setId(temp.getId());
		m.setProduct(temp.getProduct());
		m.setUpdateDate(temp.getUpdateDate());
		m.setUpdateUser(temp.getUpdateUser());
		temp = m;
		focusChannelMapProduct.setUpdateDate(commonDAO.getSysDate());
		repo.update(focusChannelMapProduct);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, focusChannelMapProduct, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public List<FocusChannelMapProduct> getListFocusChannelMapProductByFocusProgramId(KPaging<FocusChannelMapProduct> kPaging, String productCode, String productName, Long focusProgramId, String type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select p.* from focus_channel_map_product p join product prd on p.product_id = prd.product_id where 1 = 1 ");
		sql.append(" and prd.STATUS <> ?  ");
		params.add(ActiveType.DELETED.getValue());
		sql.append(" and exists (select 1 from focus_channel_map m join focus_program f on m.focus_program_id = f.focus_program_id ");
		sql.append(" where 1 = 1  ");
		if (focusProgramId != null) {
			sql.append(" and m.focus_channel_map_id = p.focus_channel_map_id and f.focus_program_id=? ");
			params.add(focusProgramId);
		} else {
			sql.append(" and m.focus_channel_map_id = p.focus_channel_map_id and f.focus_program_id is null ");
		}
		sql.append(" and m.status <> ? and f.STATUS <> ? ) ");
		params.add(ActiveType.DELETED.getValue());
		params.add(ActiveType.DELETED.getValue());
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and prd.product_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" and lower(prd.name_text) like ? ESCAPE '/' ");
			String s = productName.toLowerCase();
			s = Unicode2English.codau2khongdau(s);
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		if (!StringUtility.isNullOrEmpty(type)) {
			sql.append(" and lower(type) like ? ");
			params.add(type.toLowerCase().trim());
		}
		sql.append(" order by prd.product_code ");
		if (kPaging == null) {
			return repo.getListBySQL(FocusChannelMapProduct.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(FocusChannelMapProduct.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public List<FocusChannelMapProduct> getListFocusChannelMapProductByFocusProgramId(
			KPaging<FocusChannelMapProduct> kPaging, String productCode, String productName, 
			Long focusProgramId, Long focusChannelMapId, String type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select p.* from focus_channel_map_product p join product prd on p.product_id = prd.product_id where 1=1 ");

		sql.append(" and exists (select 1 from focus_channel_map m, focus_program f where");
		sql.append(" m.focus_program_id=f.focus_program_id ");
		
		if (focusProgramId != null) {
			sql.append(" and m.focus_channel_map_id=p.focus_channel_map_id and f.focus_program_id=?)");
			params.add(focusProgramId);
		}
		else {
			sql.append(" and m.focus_channel_map_id=p.focus_channel_map_id and f.focus_program_id is null)");
		}
		
		if(focusChannelMapId != null) {
			sql.append(" and p.focus_channel_map_id=?");
			params.add(focusChannelMapId);
		}
		
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and prd.product_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase()));
		}
		
		if (!StringUtility.isNullOrEmpty(productName)) {
			sql.append(" and lower(prd.product_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(productName.toLowerCase()));
		}
		
		if (type != null) {
			sql.append(" and type = ?");
			params.add(type);
		}
		
		sql.append(" order by prd.product_code");
		if (kPaging == null)
			return repo.getListBySQL(FocusChannelMapProduct.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(FocusChannelMapProduct.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public Boolean isUsingByOthers(long focusProgramId) throws DataAccessException {
		return false;
	}
	
	@Override
	public Boolean checkIfRecordExists(long focusProgramId, String saleTypeCode, String productCode, Long id)
				throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select count(*) as count from focus_channel_map_product mp, focus_channel_map m, focus_program fp ");
		sql.append(" where mp.focus_channel_map_id=m.focus_channel_map_id and m.focus_program_id=fp.focus_program_id and fp.status=?  ");
		sql.append("     and exists (select * from product p where p.product_id=mp.product_id and p.product_code=?) ");
		sql.append("     and m.sale_type_code=? and m.focus_program_id=? ");
		sql.append("		and fp.to_date < trunc(?) + 1");
		
		List<Object> params = new ArrayList<Object>();
		params.add(ActiveType.RUNNING.getValue());
		params.add(productCode.toUpperCase());
		params.add(saleTypeCode.toUpperCase());
		params.add(focusProgramId);
		params.add(new Date());
		
		sql.append(" and fp.to_date >= (select trunc(from_date) from promotion_program where promotion_program_id=?)");
		params.add(focusProgramId);
		sql.append(" and fp.from_date < (select trunc(to_date) from promotion_program where promotion_program_id=?) + 1");
		params.add(focusProgramId);
		
		if (id != null) {
			sql.append("	and focus_channel_map_product_id!=?");
			params.add(id);
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}
	
	@Override
	public Boolean checkIfProductInFocusProgram(Long focusProgramId, String productCode)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select count(1) as count from focus_channel_map_product mp, focus_channel_map m ");
		sql.append(" where mp.focus_channel_map_id=m.focus_channel_map_id   ");
		
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append("     and exists (select * from product p where p.product_id=mp.product_id and p.product_code=?) ");
			params.add(productCode);
		}
		
		if (focusProgramId != null) {
			sql.append("     and m.focus_program_id=? ");
			params.add(focusProgramId);
		}
		
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}
	

	@Override
	public Boolean createFocusChannelMapProduct(long focusProgramId, String saleTypeCode,
			String code, ProductType type, LogInfoVO logInfo) throws DataAccessException {
		FocusProgram dp = focusProgramDAO.getFocusProgramById(focusProgramId);
		FocusChannelMap m = focusChannelMapDAO.getFocusChannelMapByProgramIdAndSaleTypeCode(focusProgramId, saleTypeCode);
		//TODO: sua db
		if (m == null || dp == null)
			return false;
		
		if (dp != null) {
			if (type == ProductType.PRODUCT) {
				Product product = productDAO.getProductByCode(code);
				FocusChannelMapProduct d = new FocusChannelMapProduct();
				d.setProduct(product);
				d.setFocusChannelMap(m);
				d.setCreateUser(logInfo.getStaffCode());
				FocusChannelMapProduct dpd = this.createFocusChannelMapProduct(d, logInfo);
				if (dpd==null)
					return false;
				else 
					return true;
			}
			if (type == ProductType.CAT) {
				List<FocusChannelMapProduct> lstDetail = new ArrayList<FocusChannelMapProduct>();
				ProductInfo cat = productInfoDAO.getProductInfoByCode(code, type,null, true);
				if (cat == null)
					return false;
//				List<ProductLevel> lstProductLevel = productLevelDAO.getListProductLevel(null, cat.getId(), null, ActiveType.RUNNING, null, null);
//				if (lstProductLevel == null || lstProductLevel.size() == 0)
//					return false;
//				//List<Product> lstProduct = productDAO.getListProduct(null, null, null, code, null, ActiveType.RUNNING, null);
//				List<Product> lstProduct = productDAO.getListProductByProductLevelCondition(lstProductLevel);
//				for (Product prd: lstProduct) {
//					if (!checkIfRecordExists(focusProgramId, saleTypeCode, prd.getProductCode(), null)) {
//						FocusChannelMapProduct d = new FocusChannelMapProduct();
//						d.setProduct(prd);
//						d.setFocusChannelMap(m);
//						d.setCreateUser(logInfo.getStaffCode());
//						lstDetail.add(d);
//					}
//				}
				if (lstDetail.size() == 0)
					return false;
				for (FocusChannelMapProduct fcm: lstDetail) {
					try {
						actionGeneralLogDAO.createActionGeneralLog(null, fcm, logInfo);
					}
					catch (Exception ex) {
						throw new DataAccessException(ex);
					}
				}
				repo.create(lstDetail);
				
				return true;
			}
			if (type == ProductType.SUB_CAT) {
				List<FocusChannelMapProduct> lstDetail = new ArrayList<FocusChannelMapProduct>();
				ProductInfo subCat = productInfoDAO.getProductInfoByCode(code, type,null,true);
				if (subCat == null)
					return false;
//				List<ProductLevel> lstProductLevel = productLevelDAO.getListProductLevel(null, cat.getId(), null, ActiveType.RUNNING, null, null);
//				if (lstProductLevel == null || lstProductLevel.size() == 0)
//					return false;
//				//List<Product> lstProduct = productDAO.getListProduct(null, null, null, code, null, ActiveType.RUNNING, null);
//				List<Product> lstProduct = productDAO.getListProductByProductLevelCondition(lstProductLevel);
//				for (Product prd: lstProduct) {
//					if (!checkIfRecordExists(focusProgramId, saleTypeCode, prd.getProductCode(), null)) {
//						FocusChannelMapProduct d = new FocusChannelMapProduct();
//						d.setProduct(prd);
//						d.setFocusChannelMap(m);
//						d.setCreateUser(logInfo.getStaffCode());
//						lstDetail.add(d);
//					}
//				}
				if (lstDetail.size() == 0)
					return false;
				for (FocusChannelMapProduct fcm: lstDetail) {
					try {
						actionGeneralLogDAO.createActionGeneralLog(null, fcm, logInfo);
					}
					catch (Exception ex) {
						throw new DataAccessException(ex);
					}
				}
				repo.create(lstDetail);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public FocusChannelMapProduct getFocusChannelMapProduct(Long focusProgramId, Long productId)
			throws DataAccessException {
		if (focusProgramId == null) {
			throw new IllegalArgumentException("focusProgramId is null");
		}
		if (productId == null) {
			throw new IllegalArgumentException("productId is null");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT mp.* FROM focus_channel_map_product mp, focus_channel_map m ");
		sql.append(" WHERE mp.focus_channel_map_id = m.focus_channel_map_id   ");

		sql.append("     AND mp.product_id = ?");
		params.add(productId);

		sql.append("     AND m.focus_program_id = ?");
		params.add(focusProgramId);
		
		return repo.getFirstBySQL(FocusChannelMapProduct.class, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public boolean checkExistsProductMap(long focusId, String saleTypeCode, String prodCode, String type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select count(1) as count");
		sql.append(" from focus_channel_map fcm");
		sql.append(" join focus_channel_map_product fcp on (fcp.focus_channel_map_id = fcm.focus_channel_map_id)");
		sql.append(" join product p on (p.product_id = fcp.product_id)");
		sql.append(" where fcm.focus_program_id = ? and fcm.status = ?");
		params.add(focusId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and lower(p.product_code) = ? and lower(fcp.type) = ? and lower(fcm.sale_type_code) = ?");
		params.add(prodCode.toLowerCase());
		params.add(type.toLowerCase());
		params.add(saleTypeCode.toLowerCase());
		
		return (repo.countBySQL(sql.toString(), params) > 0);
	}
}
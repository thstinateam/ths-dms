package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.MediaItem;
import ths.dms.core.entities.OfficeDocShopMap;
import ths.dms.core.entities.OfficeDocument;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.enumtype.MediaType;
import ths.dms.core.entities.filter.FeedbackFilter;
import ths.dms.core.entities.filter.ImageFilter;
import ths.dms.core.entities.filter.OfficeDocumentFilter;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.MediaItemDetailVO;
import ths.dms.core.entities.vo.MediaItemVO;
import ths.dms.core.exceptions.DataAccessException;

public interface MediaItemDAO {

	MediaItem createMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws DataAccessException;

	void deleteMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws DataAccessException;

//	void updateMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws DataAccessException;
	
	MediaItem updateMediaItem(MediaItem mediaItem, LogInfoVO logInfo) throws DataAccessException;
	
	MediaItem getMediaItemById(long id) throws DataAccessException;

	List<MediaItem> getListMediaItemByMediaType(MediaType mediaType) throws DataAccessException;

	List<MediaItem> getListMediaItemByObjectIdAndObjectType(
			KPaging<MediaItem> kPaging, Long objectId,
			MediaObjectType objectType) throws DataAccessException;

//	List<MediaItem> getListMediaItemByProduct(Long productId)
//			throws DataAccessException;
	
	MediaItem checkIsExistsVideoOfProduct(Long productId) throws DataAccessException;
	
	//Image SangTN
	MediaItemVO getMediaItemVOById(Long id) throws DataAccessException;
	List<ImageVO> getListImageVO(ImageFilter filter) throws DataAccessException;
	List<ImageVO> getListImageVOMT(ImageFilter filter) throws DataAccessException;
	List<ImageVO> getListImageVOGroup(ImageFilter filter) throws DataAccessException;
	List<ImageVO> getListImageVOGroupMT(ImageFilter filter)	throws DataAccessException;
	List<MediaItemDetailVO> getListMediaItemShopVO(List<Long> lstCustomerId, Date fromDate, Date toDate, String displayProgramCode)
			throws DataAccessException;
	MediaItem updateMediaItem(MediaItem mediaItem) throws DataAccessException;
	
	
	/** ----------------Begin Quan ly cong van----------------- **/
	List<OfficeDocument> getListOfficeDoc(KPaging<OfficeDocument> kPaging, OfficeDocumentFilter filter) throws DataAccessException;
	OfficeDocument getOfficeDocumentById(Long id) throws DataAccessException;
	Integer insertOffDocShopMap(OfficeDocShopMap docShopMap) throws DataAccessException;	
	List<OfficeDocShopMap> getListOffShopMapToDocumentId(Long documentID) throws DataAccessException;
	OfficeDocShopMap getListOffShopMapToDocIdShopID(Long documentID, Long shopId) throws DataAccessException;
	Integer updateOffDocShopMap(OfficeDocShopMap docShopMap) throws DataAccessException;
	OfficeDocument getOfficeDocumentByCode(String documentCode) throws DataAccessException;
	/**
	 * @author tientv
	 * @des: Tao cong van
	 */
	OfficeDocument createDocument(OfficeDocument document)throws DataAccessException;
	MediaItem createMediaItem(MediaItem mediaItem) throws DataAccessException;
	MediaItem getMediaItemByDocumentID (Long id) throws DataAccessException;
	MediaItem getMediaItemByDocumentID (Long id, Integer object_type) throws DataAccessException;
	Integer deleteOfficeDocument (OfficeDocument officeDocument) throws DataAccessException;

	/**
	 * get danh sach item feedback
	 * @author vuongmq
	 * @param filter
	 * @return List<MediaItem>
	 * @throws DataAccessException
	 * @since 17/11/2015
	 */
	List<MediaItem> getListMediaItemFeedbackFilter(FeedbackFilter filter) throws DataAccessException;
}

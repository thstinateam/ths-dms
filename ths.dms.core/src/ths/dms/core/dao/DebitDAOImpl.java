package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Debit;
import ths.dms.core.entities.DebitDetail;
import ths.dms.core.entities.SaleOrder;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.DebitOwnerType;
import ths.dms.core.entities.enumtype.DebitPaymentType;
import ths.dms.core.entities.enumtype.DebitType;
import ths.dms.core.entities.enumtype.OrderType;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.filter.DebitFilter;
import ths.dms.core.entities.vo.DebitCustomerVO;
import ths.core.entities.vo.rpt.RptBCTCNDetailVO;
import ths.core.entities.vo.rpt.RptDebitPaymentVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class DebitDAOImpl implements DebitDAO {

	@Autowired
	private IRepository repo;

	@Override
	public Debit createDebit(Debit debit) throws DataAccessException {
		if (debit == null) {
			throw new IllegalArgumentException("debit");
		}
		repo.create(debit);
		return repo.getEntityById(Debit.class, debit.getId());
	}

	@Override
	public void deleteDebit(Debit debit) throws DataAccessException {
		if (debit == null) {
			throw new IllegalArgumentException("debit");
		}
		repo.delete(debit);
	}

	@Override
	public Debit getDebitById(long id) throws DataAccessException {
		return repo.getEntityById(Debit.class, id);
	}

	@Override
	public void updateDebit(Debit debit) throws DataAccessException {
		if (debit == null) {
			throw new IllegalArgumentException("debit");
		}
		repo.update(debit);
	}
	
	@Override
	public void updateDebit(List<Debit> debit) throws DataAccessException {
		if (debit == null) {
			throw new IllegalArgumentException("debit");
		}
		for(Debit d:debit){
			repo.update(d);
		}
		repo.update(debit);
	}

	@Override
	public Debit getDebit(Long ownerId, DebitOwnerType ownerType)
			throws DataAccessException {
		if (ownerId == null) {
			throw new IllegalArgumentException("ownerId is null");
		}
		if (ownerType == null) {
			throw new IllegalArgumentException("ownerType is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM debit WHERE object_id = ? AND object_type = ?");
		params.add(ownerId);
		params.add(ownerType.getValue());
		Debit dg = repo.getFirstBySQL(Debit.class, sql.toString(), params);
		if (dg == null) {
			dg = new Debit();
			dg.setMaxDebitAmount(BigDecimal.valueOf(0.0));
			dg.setMaxDebitDate(0);
			dg.setObjectId(ownerId);
			dg.setObjectType(ownerType);
			dg.setTotalAmount(BigDecimal.valueOf(0.0));
			dg.setTotalDebit(BigDecimal.valueOf(0.0));
			dg.setTotalPay(BigDecimal.valueOf(0.0));
			dg = this.createDebit(dg);
		}
		return dg;
	}

	@Override
	public Debit getDebitForUpdate(Long ownerId, DebitOwnerType ownerType)
			throws DataAccessException {
		if (ownerId == null) {
			throw new IllegalArgumentException("ownerId is null");
		}
		if (ownerType == null) {
			throw new IllegalArgumentException("ownerType is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT * FROM debit WHERE object_id = ? AND object_type = ? for update");
		params.add(ownerId);
		params.add(ownerType.getValue());
		Debit dg = repo.getFirstBySQL(Debit.class, sql.toString(), params);
		if (dg == null) {
			dg = new Debit();
			dg.setMaxDebitAmount(BigDecimal.valueOf(0.0));
			// dg.setMaxDebitDate(0);
			dg.setObjectId(ownerId);
			dg.setObjectType(ownerType);
			dg.setTotalAmount(BigDecimal.valueOf(0.0));
			dg.setTotalDebit(BigDecimal.valueOf(0.0));
			dg.setTotalPay(BigDecimal.valueOf(0.0));
			dg = this.createDebit(dg);
		}
		return repo.getFirstBySQL(Debit.class, sql.toString(), params);
	}

	@Override
	public List<DebitCustomerVO> getDebitCustomer(Long shopId, Long customerId,
			String customerCode, String customerName, Date fromDate, Date toDate, DebitPaymentType type)
			throws DataAccessException {

		if (null == shopId) {
			throw new DataAccessException("shopId is null");
		}

		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();

		sql.append(" SELECT DD.CREATE_DATE AS createDate,");
		sql.append(" (case when C.MAX_DEBIT_DATE is null then null else SO.ORDER_DATE + NVL(C.MAX_DEBIT_DATE, 0) end) AS payDate, ");
		sql.append("     SO.ORDER_NUMBER AS orderNumber, ");
		sql.append("     S.STAFF_NAME AS staffName, ");
		sql.append("     NVL(DD.TOTAL, 0) AS totalAmount, ");
		sql.append("     NVL(DD.TOTAL_PAY, 0) AS totalAmountPay, ");
		sql.append("     s.staff_code as staffCode, ");
		sql.append("     c.short_code as shortCode, ");
		sql.append("     c.customer_name as customerName, ");
		sql.append("     c.address as address, ");
		sql.append("     NVL(DD.REMAIN, 0) AS totalAmountRemain, ");
		sql.append("     NVL(dd.remain, 0) AS amountRemain ");
		//sql.append("     NVL(d.total_debit, 0) AS amountRemain ");
		
		sql.append(" FROM SALE_ORDER SO, DEBIT D, DEBIT_DETAIL DD, STAFF S, CUSTOMER C ");
		sql.append(" WHERE D.DEBIT_ID = DD.DEBIT_ID and d.object_id = c.customer_id ");
		sql.append("     AND D.OBJECT_TYPE = ? ");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		
		sql.append("     AND SO.ORDER_TYPE in (?, ?) ");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());

		sql.append("     AND DD.FROM_OBJECT_ID = SO.SALE_ORDER_ID ");
		sql.append("     AND SO.STAFF_ID = S.STAFF_ID ");
		sql.append("     AND SO.CUSTOMER_ID = C.CUSTOMER_ID ");
		sql.append("     AND DD.REMAIN > 0 ");
		sql.append("     AND C.SHOP_ID = ? ");

		params.add(shopId);

		if (null != fromDate) {
			sql.append("     AND TRUNC(DD.CREATE_DATE) >= TRUNC(?) ");
			params.add(fromDate);
		}

		if (null != toDate) {
			sql.append("     AND TRUNC(DD.CREATE_DATE) <= TRUNC(?) ");
			params.add(toDate);
		}

		if (null != customerId) {
			sql.append("     AND C.CUSTOMER_ID = ? ");
			params.add(customerId);
		}

		if (!StringUtility.isNullOrEmpty(customerCode)) {
			sql.append("     AND C.SHORT_CODE like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(customerCode
					.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append("     AND C.NAME_TEXT like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English
					.codau2khongdau(customerName.toUpperCase())));
		}	
		
		if(type != null){
			switch (type) {
			case CHUA_TOI_HAN:
				sql.append(" and (C.MAX_DEBIT_DATE is null or trunc(SO.ORDER_DATE + NVL(C.MAX_DEBIT_DATE, 0)) > trunc(sysdate))");
				break;

			case TOI_HAN:
				sql.append(" and (C.MAX_DEBIT_DATE is not null and trunc(SO.ORDER_DATE + NVL(C.MAX_DEBIT_DATE, 0)) = trunc(sysdate))");
				break;
				
			case QUA_HAN:
				sql.append(" and (C.MAX_DEBIT_DATE is not null and trunc(SO.ORDER_DATE + NVL(C.MAX_DEBIT_DATE, 0)) < trunc(sysdate))");
				break;
				
			default:
				break;
			}
		}
		sql.append(" ORDER BY c.CUSTOMER_CODE, DD.CREATE_DATE ASC ");		
		final String[] fieldNames = new String[] { 
				"createDate",
				"payDate",
				"orderNumber",
				"staffName",
				"totalAmount",
				"totalAmountPay",
				"staffCode",
				"shortCode",
				"customerName",
				"address",
				"totalAmountRemain",
				"amountRemain",
		};

		final Type[] fieldTypes = new Type[] {
				StandardBasicTypes.DATE,
				StandardBasicTypes.DATE,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
		};

		return repo.getListByQueryAndScalar(DebitCustomerVO.class, fieldNames,
				fieldTypes, sql.toString(), params);
	}

	@Override
	public Date getLongestDebtSaleOrder(Long ownerId, DebitOwnerType ownerType) 
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT s.*");
		sql.append(" FROM sale_order s join debit_detail dd on s.sale_order_id = dd.from_object_id and dd.remain > 0");
		sql.append("   join debit d on dd.debit_id = d.debit_id and d.object_id = ? and d.object_type = ?");
		sql.append(" ORDER BY s.order_date");
		params.add(ownerId);
		params.add(ownerType.getValue());
		SaleOrder so = repo.getFirstBySQL(SaleOrder.class, sql.toString(), params);
		return so == null ? null : so.getOrderDate();
	}

	@Override
	public DebitDetail getLastDebitDetailHaveRemain(Long ownerId,
			DebitOwnerType ownerType) throws DataAccessException {
		if (ownerId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		if (ownerType == null) {
			throw new IllegalArgumentException("customerId is null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select dd.* ");
		sql.append(" from DEBIT_DETAIL dd, DEBIT d ");
		sql.append(" where dd.DEBIT_ID = d.DEBIT_ID ");
		sql.append("     and d.OBJECT_TYPE = ? ");
		sql.append("     and d.OBJECT_ID = ?  ");
		sql.append("     and dd.REMAIN > 0 ");
		sql.append(" order by dd.CREATE_DATE asc ");

		params.add(ownerType.getValue());
		params.add(ownerId);

		return repo.getFirstBySQL(DebitDetail.class, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.DebitDAO#getListDebitPayment(java.util.Date, java.util.Date, java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<RptDebitPaymentVO> getListDebitPayment(Date fromDate,
			Date toDate, Long shopId, List<Long> staffIds, List<Long> customerIds, Integer type)
			throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT so.order_date     AS orderDate,");
		sql.append(" so.order_number        AS orderNumber,");
		sql.append(" so.total               AS total,");
		sql.append(" c.short_code           AS shortCode,");
		sql.append(" c.customer_code        AS customerCode,");
		sql.append(" c.customer_name        AS customerName,");
		sql.append(" c.max_debit_date       AS debitDate,");
		sql.append(" pd.pay_date            AS payDate,");
		sql.append(" pd.payment_type        AS paymentType,");
		sql.append(" pr.pay_received_number AS payRecievedNumber,");
//		sql.append(" dd.remain              AS remain,");
		sql.append(" pd.amount              AS totalPay,");
		sql.append(" (SELECT staff_code FROM staff WHERE staff_id = so.cashier_id) AS cashierCode,");
		sql.append(" (SELECT staff_name FROM staff WHERE staff_id = so.cashier_id) AS cashierName,");
		sql.append(" s.staff_code AS staffCode,");
		sql.append(" s.staff_name AS staffName,");
		sql.append(" trunc(sysdate) - (trunc(so.order_date) + NVL(c.max_debit_date,0)) AS typeOrder");
		sql.append(" FROM sale_order so");
		sql.append(" JOIN debit_detail dd");
		sql.append(" ON so.sale_order_id = dd.from_object_id");
		sql.append(" JOIN debit d");
		sql.append(" ON dd.debit_id = d.debit_id");
		sql.append(" JOIN customer c");
		sql.append(" ON so.customer_id = c.customer_id AND c.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" JOIN staff s");
		sql.append(" ON s.staff_id = so.staff_id AND s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" LEFT JOIN payment_detail pd");
		sql.append(" ON pd.debit_detail_id = dd.debit_detail_id");
		sql.append(" LEFT JOIN pay_received pr");
		sql.append(" ON pr.pay_received_id = pd.pay_received_id");
		sql.append(" WHERE (so.order_type  = ?");
		params.add(OrderType.IN.getValue());
		sql.append(" OR so.order_type      = ?)");
		params.add(OrderType.SO.getValue());
		if (shopId != null) {
			sql.append(" and so.shop_id = ?");
			params.add(shopId);
		}
		if (fromDate != null) {
			sql.append(" and so.ORDER_DATE >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and so.ORDER_DATE < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append(" AND d.object_type     = ?");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		sql.append(" AND dd.total_pay      > 0");
		if (customerIds != null) {
			sql.append(" AND d.object_id       in (");
			for (Long customerId: customerIds) {
				sql.append("?, ");
				params.add(customerId);
			}
			sql.append("-1)");
		} else {
			if (type != null && type == 1 ) {
				sql.append(" AND d.object_id       in (select customer_id from customer where shop_id = ? and status = ?)");
				params.add(shopId);
				params.add(ActiveType.RUNNING.getValue());
			}
		}
		if (staffIds != null) {
			sql.append(" AND so.staff_id 	  in (");
			for (Long staffId: staffIds) {
				sql.append("?, ");
				params.add(staffId);
			}
			sql.append("-1)");
		} else {
			if (type != null && type != 1 ) {
				sql.append(" AND so.staff_id 	  in (select staff_id from staff where shop_id = ? and status = ?)");
				params.add(shopId);
				params.add(ActiveType.RUNNING.getValue());
			}
		}
		if (type != null && type == 1) {//tim theo khach hang
			sql.append(" ORDER BY c.customer_code");
		} else {
			sql.append(" ORDER BY s.staff_code");
		}
//		System.out.println(TestUtil.addParamToQuery(sql.toString(), params));
		final String[] fieldNames = new String[] { //
						"orderDate",// 1
						"orderNumber",// 2
						"total",// 3
						"shortCode",// 4
						"customerCode",// 5
						"customerName",// 6
						"debitDate",// 7
						"payDate",// 8
						"paymentType",// 9
						"payRecievedNumber",// 10
//						"remain",// 11
						"totalPay",// 11'
						"cashierCode",// 12
						"cashierName",// 13
						"staffCode",// 14
						"staffName",// 15
						"typeOrder",// 16
				};

				final Type[] fieldTypes = new Type[] { //
						StandardBasicTypes.DATE,// 1
						StandardBasicTypes.STRING,// 2
						StandardBasicTypes.BIG_DECIMAL,// 3
						StandardBasicTypes.STRING,// 4
						StandardBasicTypes.STRING,// 5
						StandardBasicTypes.STRING,// 6
						StandardBasicTypes.INTEGER,// 7
						StandardBasicTypes.DATE,// 8
						StandardBasicTypes.INTEGER,// 9
						StandardBasicTypes.STRING,// 10
//						StandardBasicTypes.BIG_DECIMAL,// 11
						StandardBasicTypes.BIG_DECIMAL,// 11'
						StandardBasicTypes.STRING,// 12
						StandardBasicTypes.STRING,// 13
						StandardBasicTypes.STRING,// 14
						StandardBasicTypes.STRING,// 15
						StandardBasicTypes.BIG_DECIMAL,// 16
				};

				return repo.getListByQueryAndScalar(RptDebitPaymentVO.class, fieldNames,
						fieldTypes, sql.toString(), params);
	}
	
	//SangTN: Chuyen qua su dung procedure
//	@Override
//	public List<RptBCTCNDetailVO> getRptBCTCNDetailVO(Long shopId, List<Long> lstCustomerId,
//			List<Long> lstStaffId, StockObjectType objectType) throws DataAccessException {
//		StringBuilder sql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		
//		sql.append(" select so.order_date orderDate,");
//		sql.append("	so.order_number orderNumber,");
//		sql.append("	so.order_type orderType,");
//		sql.append("	(trunc(dd.create_date) + c.max_debit_date) paidDate,");
////		sql.append("	case when trunc(dd.create_date) + c.max_debit_date > trunc(sysdate) then dd.remain else 0 end overdueDebt,");
////		sql.append("	case when trunc(dd.create_date) + c.max_debit_date = trunc(sysdate) then dd.remain else 0 end nextDebt,");
////		sql.append("	case when trunc(dd.create_date) + c.max_debit_date < trunc(sysdate) then dd.remain else 0 end availableDebt,");
//		sql.append("	CASE    WHEN TRUNC(dd.create_date + nvl(c.max_debit_date, 0)) > TRUNC(sysdate)    THEN dd.remain    ELSE 0  END overdueDebt,");
//		sql.append("	CASE    WHEN TRUNC(dd.create_date + nvl(c.max_debit_date, 0)) = TRUNC(sysdate)    THEN dd.remain    ELSE 0  END nextDebt,");
//		sql.append("	CASE    WHEN TRUNC(dd.create_date + nvl(c.max_debit_date, 0)) < TRUNC(sysdate)    THEN dd.remain    ELSE 0  END availableDebt,");
//		sql.append("	(c.short_code || '-' || c.customer_name) customerInfo,");
//		sql.append("	(staff_code || '-' || staff_name) staffInfo,");
//		sql.append("	(select staff_code || '-' || staff_name from staff where staff_id = so.delivery_id) deliveryStaffInfo");
//		
//		sql.append(" from sale_order so ");
//		
//		sql.append(" join customer c on so.customer_id = c.customer_id");
//		sql.append(" 	and so.order_type in ('IN', 'SO')");
//		if (shopId != null) {
//			sql.append("	and so.shop_id = ?");
//			params.add(shopId);
//		}
//		if (lstStaffId != null) {
//			sql.append("	and (1=0 ");
//			for (Long staffId: lstStaffId) {
//				if (staffId != null) {
//					sql.append("	or so.staff_id = ?");
//					params.add(staffId);
//				}
//			}
//			sql.append(" )");
//		}
//		if (lstCustomerId != null) {
//			sql.append("	and (1=0 ");
//			for (Long customerId: lstCustomerId) {
//				if (customerId != null) {
//					sql.append("	or so.customer_id = ?");
//					params.add(customerId);
//				}
//			}
//			sql.append(" )");
//		}
//		
//		
//		
//		sql.append(" join debit_detail dd on so.sale_order_id = dd.from_object_id and dd.remain > 0");
//		
//		sql.append(" join debit d on dd.debit_id = d.debit_id and d.object_type = ?");
//		params.add(DebitOwnerType.CUSTOMER.getValue());
//		
//		//SangTN
//		//Them dieu kien: •	DEBIT. OBJECT_ID = ID KH nhập vào hoặc tất cả của NPP
//		if (lstCustomerId != null) {
//			sql.append("	and (1=0 ");
//			for (Long customerId: lstCustomerId) {
//				if (customerId != null) {
//					sql.append("	or d.object_id = ?");
//					params.add(customerId);
//				}
//			}
//			sql.append(" )");
//		}
//		
//		sql.append(" join staff st on so.staff_id = st.staff_id");
//		if (StockObjectType.CUSTOMER.equals(objectType))
//			sql.append(" order by c.short_code, dd.create_date");
//		else if (StockObjectType.STAFF.equals(objectType))
//			sql.append(" order by st.staff_code, dd.create_date");
//		
//		
//		
//		String[] fieldNames = {"orderDate", "orderNumber", "orderType", "paidDate",
//								"overdueDebt", "nextDebt", "availableDebt", 
//								"customerInfo", "staffInfo", "deliveryStaffInfo"};
//		
//		Type[] fieldTypes = {StandardBasicTypes.DATE, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.DATE, 
//				StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.BIG_DECIMAL,
//				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING};
//		//SangTN
//		System.out.println(TestUtil.addParamToQuery(sql.toString(), params));
//		return repo.getListByQueryAndScalar(RptBCTCNDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
//	}
	
	public List<RptBCTCNDetailVO> getRptBCTCNDetailVO(Long shopId, List<Long> lstCustomerId,
			List<Long> lstStaffId, StockObjectType objectType) throws DataAccessException {
		
		StringBuilder strLstCustomerId = null;
		if(null != lstCustomerId && lstCustomerId.size() >0)
		{
			strLstCustomerId = new StringBuilder();
			for(int i = 0; i<lstCustomerId.size() - 1; i++)
			{
				strLstCustomerId.append(lstCustomerId.get(i).toString() + ",");
			}
			strLstCustomerId.append(lstCustomerId.get(lstCustomerId.size()-1));
		}		
		
		StringBuilder strLstStaffId = null;
		if(null != lstStaffId && lstStaffId.size() > 0){
			strLstStaffId = new StringBuilder();
			for(int i = 0; i<lstStaffId.size() - 1; i++)
			{
				strLstStaffId.append(lstStaffId.get(i).toString() + ",");
			}
			strLstStaffId.append(lstStaffId.get(lstStaffId.size()-1));
		}
		
		List<Object> inParams = new ArrayList<Object>();
		inParams.add("shopId");
		inParams.add(shopId);		
		inParams.add(StandardBasicTypes.LONG);
		
		inParams.add("listCustomerId");
		inParams.add(strLstCustomerId==null?"":strLstCustomerId.toString());		
		inParams.add(StandardBasicTypes.STRING);
		
		
		inParams.add("listStaffId");
		inParams.add(strLstStaffId==null?"":strLstStaffId.toString());		
		inParams.add(StandardBasicTypes.STRING);
		
		//objectType = 3 KH, = 2 NV
		inParams.add("objectType");
		inParams.add(objectType.getValue());		
		inParams.add(StandardBasicTypes.INTEGER);
		
		return repo.getListByNamedQuery(RptBCTCNDetailVO.class, "PKG_SHOP_REPORT.BAOCAO_TCN", inParams);		
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public BigDecimal getTotalDebit(Long ownerId, DebitOwnerType ownerType)
			throws DataAccessException {
		if (ownerType == null) {
			throw new IllegalArgumentException("ownerType is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select sum(total_debit) from debit where object_type = ?");
		params.add(ownerType.getValue());
		if (ownerId != null) {
			sql.append(" and object_id = ?");
			params.add(ownerId);
		}
		Object obj = repo.getObjectByQuery(sql.toString(), params);
		return new BigDecimal(obj.toString());
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<DebitCustomerVO> getListDebitCustomerVO(DebitFilter<DebitCustomerVO> filter) throws DataAccessException {
		if (null == filter.getShopId()) {
			throw new DataAccessException("shopId is null");
		}

		final Integer NOT_PAYMENT = 0;
		final Integer PAYMENT_LESS = 1;
		final Integer PAYMENT_ALL = 2;
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();

		sql.append(" SELECT DD.debit_date AS createDate,");
		sql.append(" (case when C.MAX_DEBIT_DATE is null then null else dd.debit_date + NVL(C.MAX_DEBIT_DATE, 0) end) AS payDate, ");
		sql.append("     (case when dd.from_object_number is null then SO.ORDER_NUMBER else dd.from_object_number end) AS orderNumber, ");
		sql.append("     (select STAFF_NAME from staff where staff_id = so.staff_id) AS staffName, ");
		sql.append("     NVL(DD.TOTAL, 0) AS totalAmount, ");
		sql.append("     NVL(DD.TOTAL_PAY, 0) AS totalAmountPay, ");
		sql.append("     NVL(DD.discount_amount, 0) AS discountAmount, ");
		sql.append("     (select staff_code from staff where staff_id = so.staff_id) as staffCode,");
		sql.append("     (select staff_name from staff where staff_id = so.delivery_id) as deliveryName,");
		sql.append("     (select staff_name from staff where staff_id = so.cashier_id) as cashierName,");
		sql.append("     c.short_code as shortCode, ");
		sql.append("     c.customer_name as customerName, ");
		sql.append("     c.address as address, ");
		sql.append("     NVL(DD.REMAIN, 0) AS totalAmountRemain, ");
		sql.append("     NVL(dd.remain, 0) AS amountRemain ");
		//sql.append("     NVL(d.total_debit, 0) AS amountRemain ");
		
		//sql.append(" FROM SALE_ORDER SO, DEBIT D, DEBIT_DETAIL DD, STAFF S, CUSTOMER C ");
		sql.append(" from debit_detail dd");
		sql.append(" join debit d on (d.debit_id = dd.debit_id and d.object_type = ?)");
		params.add(DebitOwnerType.CUSTOMER.getValue());
		sql.append(" join customer c on (c.customer_id = d.object_id and c.shop_id = ?)");
		params.add(filter.getShopId());
		sql.append(" left join sale_order so on (so.sale_order_id = dd.from_object_id and so.order_type in (?, ?))");
		params.add(OrderType.IN.getValue());
		params.add(OrderType.SO.getValue());
		
		sql.append(" where 1 = 1");

		if (null != filter.getFromDate()) {
			sql.append("     AND DD.debit_date >= TRUNC(?)");
			params.add(filter.getFromDate());
		}

		if (null != filter.getToDate()) {
			sql.append("     AND DD.debit_date < TRUNC(?) + 1");
			params.add(filter.getToDate());
		}

		/*if (null != customerId) {
			sql.append("     AND C.CUSTOMER_ID = ? ");
			params.add(customerId);
		}*/

		if (!StringUtility.isNullOrEmpty(filter.getShortCode())) {
			sql.append("     AND C.SHORT_CODE like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getShortCode().toUpperCase()));
		}

		/*if (!StringUtility.isNullOrEmpty(customerName)) {
			sql.append("     AND C.NAME_TEXT like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English
					.codau2khongdau(customerName.toUpperCase())));
		}*/
		
		if (filter.getDeliveryId() != null) {
			sql.append(" and so.delivery_id=?");
			params.add(filter.getDeliveryId());
		}
		
		if (filter.getCashierId() != null) {
			sql.append(" and so.cashier_id=?");
			params.add(filter.getCashierId());
		}

		if (filter.getStaffId() != null) {
			sql.append(" and so.staff_id=?");
			params.add(filter.getStaffId());
		}
		
		if(filter.getDebitPaymentType() != null){
			switch (filter.getDebitPaymentType()) {
			case CHUA_TOI_HAN:
				sql.append("     AND DD.REMAIN <> 0 ");
				sql.append(" and (C.MAX_DEBIT_DATE is null or trunc(dd.debit_date + NVL(C.MAX_DEBIT_DATE, 0)) > trunc(?))");
				params.add(filter.getLockDate());
				break;

			case TOI_HAN:
				sql.append("     AND DD.REMAIN <> 0 ");
				sql.append(" and (C.MAX_DEBIT_DATE is not null and trunc(dd.debit_date + NVL(C.MAX_DEBIT_DATE, 0)) = trunc(?))");
				params.add(filter.getLockDate());
				break;
				
			case QUA_HAN:
				sql.append("     AND DD.REMAIN <> 0 ");
				sql.append(" and (C.MAX_DEBIT_DATE is not null and trunc(dd.debit_date + NVL(C.MAX_DEBIT_DATE, 0)) < trunc(?))");
				params.add(filter.getLockDate());
				break;
				
			case DA_TAT_TOAN:
				sql.append("     AND DD.REMAIN = 0 ");
				break;
				
			default:
				break;
			}
		}
		if (filter.getStatusPayment() != null && filter.getStatusPayment().size() > 0) {
			sql.append(" and (1=0 ");
			for(int i = 0, sz = filter.getStatusPayment().size(); i < sz; i++) {
				if (filter.getStatusPayment().get(i) == NOT_PAYMENT) {
					sql.append(" or (DD.REMAIN = DD.TOTAL and DD.REMAIN <> 0) ");
				} else if (filter.getStatusPayment().get(i) == PAYMENT_ALL) {
					sql.append(" or DD.REMAIN = 0 ");
				} else if (filter.getStatusPayment().get(i) == PAYMENT_LESS) {
					sql.append(" or (DD.REMAIN <> DD.TOTAL and DD.REMAIN <> 0) ");
				} 
			}
			sql.append(")");
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getLstOrderNumbers())) {
			String s = filter.getLstOrderNumbers();
			s = s.replaceAll(";", ",");
			String[] str = s.split(",");
			sql.append(" AND ( ");
			sql.append(" (dd.from_object_number IN ('-1'");
			for (String s1 : str) {
				sql.append(",?");
				params.add(s1.trim().toUpperCase());
			}
			sql.append(") )");
			sql.append(" OR (SO.ORDER_NUMBER    IN ('-1'");
			for (String s1 : str) {
				sql.append(",?");
				params.add(s1.trim().toUpperCase());
			}
			sql.append(") )");
			sql.append(")"); // end And
		}
		sql.append(" ORDER BY c.CUSTOMER_CODE, DD.debit_date ASC ");		
		final String[] fieldNames = new String[] { 
				"createDate",
				"payDate",
				"orderNumber",
				"staffName",
				"totalAmount",
				"totalAmountPay",
				"discountAmount",
				"staffCode",
				"deliveryName",
				"cashierName",
				"shortCode",
				"customerName",
				"address",
				"totalAmountRemain",
				"amountRemain",
		};

		final Type[] fieldTypes = new Type[] {
				StandardBasicTypes.DATE,
				StandardBasicTypes.DATE,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL,
		};

		if (filter.getPaging() == null) {
			return repo.getListByQueryAndScalar(DebitCustomerVO.class, fieldNames,
					fieldTypes, sql.toString(), params);
		}
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from (");
		countSql.append(sql.toString());
		countSql.append(")");
		return repo.getListByQueryAndScalarPaginated(DebitCustomerVO.class, fieldNames, fieldTypes,
				sql.toString(), countSql.toString(), params, params, filter.getPaging());
	}

	/**
	 * @author tungtt21
	 * Lay no NPP voi VNM hien tai
	 */
	@Override
	public BigDecimal getCurrentDebit(Long objectId, DebitOwnerType ownerType, DebitType debitType)
			throws DataAccessException {
		if (objectId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select sum(dbd.remain) from debit db join debit_detail dbd on db.debit_id = dbd.debit_id where db.object_type = ?");
		params.add(ownerType.getValue());
		if (objectId != null) {
			sql.append(" and db.object_id = ?");
			params.add(objectId);
			if (DebitOwnerType.SHOP.equals(ownerType)) {
				sql.append(" and dbd.shop_id = ? "); // check them debit_detail(shop_id)
				params.add(objectId);
			}
		}
		if (debitType != null) {
			if(DebitType.THU.equals(debitType)){
				sql.append(" and dbd.remain > 0");
			}else{
				sql.append(" and dbd.remain < 0");
			}
		}
		Object obj = repo.getObjectByQuery(sql.toString(), params);
		if(obj == null){
			return BigDecimal.ZERO;
		}
		return new BigDecimal(obj.toString());
	}
	
	@Override
	public List<Debit> getListDebitByFilter(SoFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from debit d where 1 = 1");
		if(filter.getLstCustomerId()!=null){
			sql.append(" and d.object_id in (-1");
			for(Long id:filter.getLstCustomerId()){
				sql.append(",?");
				params.add(id);
			}
			sql.append(")");
		}
		if(filter.getObjectType()!=null){
			sql.append(" and d.object_type=? ");
			params.add(filter.getObjectType());
		}
		return repo.getListBySQL(Debit.class, sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Price;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.PriceFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PriceVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class PriceDAOImpl implements PriceDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;

	@Autowired
	ShopDAO shopDAO;

	@Autowired
	ShopLockDAO shopLockDAO;

	@Override
	public Price createPrice(Price price, LogInfoVO logInfo) throws DataAccessException {
		if (price == null) {
			throw new IllegalArgumentException("price");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		try {
			Date sysDate = commonDAO.getSysDate();
			if (price.getToDate() != null) {
				price.setToDate(sysDate);
			}
			price.setCreateDate(sysDate);
			price.setCreateUser(logInfo.getStaffCode());
			repo.create(price);
			actionGeneralLogDAO.createActionGeneralLog(null, price, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		Price x = repo.getEntityById(Price.class, price.getId());
		return x;
	}

	@Override
	public void deletePrice(Price price, LogInfoVO logInfo) throws DataAccessException {
		if (price == null) {
			throw new IllegalArgumentException("price");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}

		repo.delete(price);
		try {
			actionGeneralLogDAO.createActionGeneralLog(price, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Price getPriceById(long id) throws DataAccessException {
		return repo.getEntityById(Price.class, id);
	}

	@Override
	public void updatePrice(Price price, LogInfoVO logInfo) throws DataAccessException {
		if (price == null) {
			throw new IllegalArgumentException("price");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		Price temp = this.getPriceById(price.getId());
		price.setUpdateDate(commonDAO.getSysDate());
		price.setUpdateUser(logInfo.getStaffCode());
		repo.update(price);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, price, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public List<Price> getListPriceByShopId(Long shopId, Date lockDay, Long productId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("WITH listPrice AS ");
		sql.append("(SELECT pr.*, ");
		sql.append("orderNo ");
		sql.append("FROM price pr ");
		sql.append("JOIN ");
		sql.append("(SELECT shop_id, ");
		sql.append("( ");
		sql.append("CASE ct.object_type ");
		sql.append("WHEN 3 ");
		sql.append("THEN 1 ");
		sql.append("WHEN 11 ");
		sql.append("THEN 1 ");
		sql.append("WHEN 2 ");
		sql.append("THEN 2 ");
		sql.append("WHEN 10 ");
		sql.append("THEN 2 ");
		sql.append("WHEN 1 ");
		sql.append("THEN 3 ");
		sql.append("WHEN 9 ");
		sql.append("THEN 3 ");
		sql.append("WHEN 13 ");
		sql.append("THEN 3 ");
		sql.append("WHEN 0 ");
		sql.append("THEN 4 ");
		sql.append("WHEN 8 ");
		sql.append("THEN 4 ");
		sql.append("WHEN 12 ");
		sql.append("THEN 4 ");
		sql.append("WHEN 14 ");
		sql.append("THEN 5 ");
		sql.append("ELSE 8 ");
		sql.append("END)AS orderNo ");
		sql.append("FROM shop s ");
		sql.append("JOIN channel_type ct ");
		sql.append("ON (ct.channel_type_id            = s.shop_type_id) ");
		sql.append("WHERE s.status                    = 1 ");
		sql.append("AND ct.type                       = 1 ");
		sql.append("START WITH shop_id              = ? ");
		params.add(shopId);
		sql.append("CONNECT BY prior parent_shop_id = shop_id ");
		sql.append(") sh ON (sh.shop_id               = pr.shop_id) ");
		sql.append("WHERE status                        = 1 ");
		sql.append("AND pr.customer_type_id            IS NULL ");
		sql.append("and pr.product_id = ? ");
		params.add(productId);
		sql.append("AND from_date                       < TRUNC(?) + 1 ");
		sql.append("AND (to_date                       >= TRUNC(?) ");
		params.add(lockDay);
		params.add(lockDay);
		sql.append("OR to_date                         IS NULL) ");
		sql.append("UNION ALL ");
		sql.append("SELECT pr.*, ");
		sql.append("( ");
		sql.append("CASE ");
		sql.append("WHEN shop_channel IS NOT NULL ");
		sql.append("THEN 6 ");
		sql.append("ELSE 7 ");
		sql.append("END) AS orderNo ");
		sql.append("FROM price pr ");
		sql.append("WHERE status          = 1 ");
		sql.append("and pr.product_id = ? ");
		params.add(productId);
		sql.append("AND from_date         < TRUNC(?) + 1 ");
		sql.append("AND (to_date         >= TRUNC(?) ");
		params.add(lockDay);
		params.add(lockDay);
		sql.append("OR to_date           IS NULL) ");
		sql.append("AND customer_type_id IS NULL ");
		sql.append("AND shop_id          IS NULL ");
		sql.append("AND (shop_channel    IN ");
		sql.append("(SELECT shop_channel FROM shop WHERE shop_id = ? ");
		params.add(shopId);
		sql.append(") ");
		sql.append("OR shop_channel IS NULL) ");
		sql.append(")");
		sql.append("SELECT * ");
		sql.append("FROM listPrice tmp ");
		sql.append("WHERE orderNo <= ");
		sql.append("(SELECT MIN(orderNo) FROM listPrice WHERE product_id = tmp.product_id ");
		sql.append(")");
		return repo.getListBySQL(Price.class, sql.toString(), params);
	}

	@Override
	public List<Price> getListPrice(KPaging<Price> kPaging, String productCode, BigDecimal price, Date fromDate, Date toDate, ActiveType status, BigDecimal priceNotVat, BigDecimal vat, boolean isExport) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<Price>();

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/*
		 * sql.append("select * from PRICE pr where 1 = 1");
		 * 
		 * if (!StringUtility.isNullOrEmpty(productCode)) { sql.append(
		 * " and exists (select 1 from product p where pr.product_id = p.product_id and p.product_code = ? )"
		 * ); params.add(productCode.toUpperCase()); }
		 */

		sql.append(" select pr.* ");
		sql.append(" from price pr, product p ");
		sql.append(" where pr.product_id = p.product_id ");
		if (!StringUtility.isNullOrEmpty(productCode)) {
			if (isExport) {
				sql.append(" and p.product_code like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase()));
			} else {
				sql.append(" and p.product_code = ? ");
				params.add(productCode.toUpperCase());
			}
		}
		if (price != null) {
			sql.append(" and pr.price=?");
			params.add(price);
		}
		if (fromDate != null) {
			sql.append(" and ((pr.to_date is not null and pr.to_date >= trunc(?)) or pr.to_date is null)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and ((pr.from_date is not null and pr.from_date < trunc(?) + 1) or pr.from_date is null)");
			params.add(toDate);
		}
		if (status != null) {
			sql.append(" and pr.status = ?");
			params.add(status.getValue());
		} else {
			//Khong hien thi gia da xoa 
			sql.append(" and pr.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (priceNotVat != null) {
			sql.append(" and pr.price_not_vat = ?");
			params.add(priceNotVat);
		}
		if (vat != null) {
			sql.append(" and pr.vat = ?");
			params.add(vat);
		}

		sql.append(" order by p.product_code asc, pr.from_date desc, pr.to_date desc");

		if (kPaging == null)
			return repo.getListBySQL(Price.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Price.class, sql.toString(), params, kPaging);
	}

	@Override
	public Boolean isUsingByOthers(long priceId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select count(1) as count from PRICE");
		sql.append(" 	where exists (select 1 from PO_AUTO_DETAIL where PRICE_ID = ?)");
		params.add(priceId);
		sql.append(" 	or exists (select 1 from PO_VNM_DETAIL where PRICE_ID = ?)");
		params.add(priceId);
		sql.append(" 	or exists (select 1 from PO_VNM_LOT where PRICE_ID = ?)");
		params.add(priceId);
		sql.append(" 	or exists (select 1 from SALE_ORDER_DETAIL where PRICE_ID = ?)");
		params.add(priceId);
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public Boolean checkIfDateRangeInUse(Long productId, Date fromDate, Date toDate, Long priceId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT count(1) as count ");
		sql.append(" FROM price ");
		sql.append(" WHERE product_id=? and status = ?");
		params.add(productId);
		params.add(ActiveType.RUNNING.getValue());

		if (fromDate != null) {
			sql.append(" and ((to_date is not null and to_date >= trunc(?)) or to_date is null)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and from_date < trunc(?) + 1");
			params.add(toDate);
		}
		if (priceId != null) {
			sql.append(" and price_id != ?");
			params.add(priceId);
		}

		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public Price getPriceByProductId(Long productId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * ");
		sql.append(" FROM price ");
		sql.append(" WHERE product_id = ? AND trunc(sysdate) + 1 > from_date");
		sql.append(" AND (to_date is null OR trunc(sysdate) <= to_date) and status = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(productId);
		params.add(ActiveType.RUNNING.getValue());
		return repo.getFirstBySQL(Price.class, sql.toString(), params);
	}

	@Override
	public Price getPriceByProductId(Long shopId, Long productId) throws DataAccessException {
		Date appDate = commonDAO.getSysDate();
		if (shopId != null) {
			appDate = shopLockDAO.getApplicationDate(shopId);
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * ");
		sql.append(" FROM price ");
		sql.append(" WHERE product_id = ? AND trunc(?) + 1 > from_date");
		params.add(productId);
		params.add(appDate);
		sql.append(" AND (to_date is null OR trunc(?) <= to_date) AND status = ?");
		params.add(appDate);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND shop_id = ? ");
		params.add(shopId);
		return repo.getFirstBySQL(Price.class, sql.toString(), params);
	}

	/**
	 * @modify by lacnv1
	 * @modify date Sep 17, 2014
	 */
	@Override
	public Price getPriceByProductAndShopId(Long productId, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		/*
		 * sql.append(" SELECT * "); sql.append(" FROM price "); sql.append(
		 * " WHERE product_id = ? and shop_id = ? AND trunc(sysdate) + 1 > from_date"
		 * ); sql.append(
		 * " AND (to_date is null OR trunc(sysdate) <= to_date) and status = ?"
		 * ); params.add(productId); params.add(shopId);
		 * params.add(ActiveType.RUNNING.getValue());
		 */

		Date appDate = shopLockDAO.getApplicationDate(shopId);

		sql.append("with priceTmp1 as (");
		sql.append(" select price_id, customer_type_id, orderNo");
		sql.append(" from price pr");
		sql.append(" join (select shop_id, (case ct.object_type when ? then 1 when ? then 1");
		params.add(ShopObjectType.NPP.getValue());
		params.add(ShopObjectType.NPP_KA.getValue());
		sql.append(" when ? then 2 when ? then 2");
		params.add(ShopObjectType.VUNG.getValue());
		params.add(ShopObjectType.VUNG_KA.getValue());
		sql.append(" when ? then 3 when ? then 3 when ? then 3");
		params.add(ShopObjectType.MIEN.getValue());
		params.add(ShopObjectType.MIEN_ST.getValue());
		params.add(ShopObjectType.MIEN_KA.getValue());
		sql.append(" when ? then 4 when ? then 4 when ? then 4");
		params.add(ShopObjectType.GT.getValue());
		params.add(ShopObjectType.ST.getValue());
		params.add(ShopObjectType.KA.getValue());
		sql.append(" when ? then 5 else 8 end)as orderNo");
		params.add(ShopObjectType.VNM.getValue());
		sql.append(" from shop s");
		sql.append(" join channel_type ct on (ct.channel_type_id = s.shop_type_id)");
		sql.append(" where s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" start with shop_id = ? connect by prior parent_shop_id = shop_id");
		params.add(shopId);
		sql.append(" ) sh on (sh.shop_id = pr.shop_id)");
		sql.append(" where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and product_id = ?");
		params.add(productId);
		sql.append(" and from_date < trunc(?) + 1");
		params.add(appDate);
		sql.append(" and (to_date >= trunc(?) or to_date is null)");
		params.add(appDate);
		//sql.append(" and (customer_type_id = ? or customer_type_id is null)");
		sql.append(" and customer_type_id is null");
		sql.append(" union all");
		sql.append(" select price_id, customer_type_id,");
		sql.append(" (case when shop_channel is not null then 6 else 7 end) as orderNo");
		sql.append(" from price pr");
		sql.append(" where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and product_id = ?");
		params.add(productId);
		sql.append(" and from_date < trunc(?) + 1");
		params.add(appDate);
		sql.append(" and (to_date >= trunc(?) or to_date is null)");
		params.add(appDate);
		//sql.append(" and (customer_type_id = 611 or customer_type_id is null)");
		sql.append(" and customer_type_id is null");
		sql.append(" and shop_id is null");
		sql.append(" and (shop_channel in (select shop_channel from shop where shop_id = ?) or shop_channel is null)");
		params.add(shopId);
		sql.append(" ),");
		sql.append(" priceTmp as (");
		sql.append(" select price_id");
		sql.append(" from priceTmp1 pr1");
		sql.append(" order by customer_type_id nulls last, orderNo");
		sql.append(" )");
		sql.append(" select * from price where price_id in (select price_id from priceTmp where rownum = 1)");

		//return repo.getFirstBySQL(Price.class, sql.toString(), params);
		return repo.getEntityBySQL(Price.class, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public Price getProductPriceForCustomer(long productId, long shopId, long customerTypeId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		Date appDate = shopLockDAO.getApplicationDate(shopId);

		sql.append("with priceTmp1 as (");
		sql.append(" select price_id, customer_type_id, orderNo");
		sql.append(" from price pr");
		sql.append(" join (select shop_id, 1 as orderNo");
		sql.append(" from shop s");
		sql.append(" join organization org on org.organization_id = s.organization_id and org.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" join shop_type st on s.SHOP_TYPE_ID = st.SHOP_TYPE_ID and st.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
//		sql.append(" join channel_type ct on (ct.channel_type_id = s.shop_type_id)");
		sql.append(" where s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" start with shop_id = ? connect by prior parent_shop_id = shop_id");
		params.add(shopId);
		sql.append(" ) sh on (sh.shop_id = pr.shop_id)");
		sql.append(" where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and product_id = ?");
		params.add(productId);
		sql.append(" and from_date < trunc(?) + 1");
		params.add(appDate);
		sql.append(" and (to_date >= trunc(?) or to_date is null)");
		params.add(appDate);
		sql.append(" and (customer_type_id = ? or customer_type_id is null)");
		params.add(customerTypeId);
		sql.append(" union all");
		sql.append(" select price_id, customer_type_id,");
		sql.append(" 7  as orderNo");
		sql.append(" from price pr");
		sql.append(" where status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and product_id = ?");
		params.add(productId);
		sql.append(" and from_date < trunc(?) + 1");
		params.add(appDate);
		sql.append(" and (to_date >= trunc(?) or to_date is null)");
		params.add(appDate);
		sql.append(" and (customer_type_id = ? or customer_type_id is null)");
		params.add(customerTypeId);
		sql.append(" and shop_id is null");
//		sql.append(" and (shop_channel in (select shop_channel from shop where shop_id = ?) or shop_channel is null)");
//		params.add(shopId);
		sql.append(" ),");
		sql.append(" priceTmp as (");
		sql.append(" select price_id");
		sql.append(" from priceTmp1 pr1");
		sql.append(" order by customer_type_id nulls last, orderNo");
		sql.append(" )");
		sql.append(" select * from price where price_id in (select price_id from priceTmp where rownum = 1)");

		return repo.getEntityBySQL(Price.class, sql.toString(), params);
	}

	@Override
	public Price getPricetByFilter(PriceFilter<Price> filter) throws DataAccessException {
		/**
		 * @param BasicFilter
		 *            <Price> intG = 1: shop_id is null, customer_id is null,
		 *            shop_channel is null - Gia tren VNM intG = 2: shop_id is
		 *            null, customer_id is null, shop_channel is not null - Gia
		 *            tren kenh intG = 3: shop_id is null, customer_id is not
		 *            null, shop_channel is not null - Gia cua loai KH tren kenh
		 *            intG = 4: shop_id is not null, customer_id is null,
		 *            shop_channel is null - Gia cua don vi (Vung, Mien, NPP)
		 *            intG = 5: shop_id is not null, customer_id is not null,
		 *            shop_channel is null - Gia cua Loai KH tren Don vi (Vung,
		 *            Mien, NPP) intG = 6: shop_id is null, customer_id is not
		 *            null, shop_channel is null - Gia cua Loai KH tren VNM
		 * 
		 * @author hunglm16
		 * @description Bo sung cho cac truong hop cos the co ve gia tren VNM
		 * **/
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from price ");
		sql.append(" where 1 = 1  ");
		if (filter.getStatus() != null) {
			sql.append("  and status = ? ");
			params.add(filter.getStatus());
		} else {
			if (filter.getArrIntG() != null && !filter.getArrIntG().isEmpty()) {
				sql.append("  and status in (-10 ");
				for (Integer status : filter.getArrIntG()) {
					sql.append("  ,? ");
					params.add(status);
				}
				sql.append("  ) ");
			} else {
				sql.append("  and status != -1 ");
			}
		}
		if (filter.getId() != null) {
			sql.append(" and price_id = ?  ");
			params.add(filter.getId());
		}
		if (filter.getShopId() != null) {
			sql.append(" and shop_id = ?  ");
			params.add(filter.getShopId());
		} else {
			sql.append(" and shop_id is null  ");
		}
		if (filter.getShopChannel() != null) {
			sql.append(" and shop_channel = ?  ");
			params.add(filter.getShopChannel());
		} else {
			sql.append(" and shop_channel is null  ");
		}
		if (filter.getCustomerTypeId() != null) {
			sql.append(" and customer_type_id = ?  ");
			params.add(filter.getCustomerTypeId());
		} else {
			sql.append(" and customer_type_id is null  ");
		}
		if (filter.getProductId() != null) {
			sql.append(" and product_id = ?  ");
			params.add(filter.getProductId());
		}
		if (filter.getFromDate() != null) {
			sql.append("  and trunc(from_date) >= trunc(?) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append("  and trunc(?) < trunc(to_date) + 1 ");
			params.add(filter.getToDate());
		}
		Price rs = repo.getEntityBySQL(Price.class, sql.toString(), params);
		return rs;
	}

	@Override
	public Boolean checkPricetByFilter(PriceFilter<Price> filter) throws DataAccessException {
		/**
		 * @param BasicFilter
		 *            <Price> intG = 1: shop_id is null, customer_id is null,
		 *            shop_channel is null - Gia tren VNM intG = 2: shop_id is
		 *            null, customer_id is null, shop_channel is not null - Gia
		 *            tren kenh intG = 3: shop_id is null, customer_id is not
		 *            null, shop_channel is not null - Gia cua loai KH tren kenh
		 *            intG = 4: shop_id is not null, customer_id is null,
		 *            shop_channel is null - Gia cua don vi (Vung, Mien, NPP)
		 *            intG = 5: shop_id is not null, customer_id is not null,
		 *            shop_channel is null - Gia cua Loai KH tren Don vi (Vung,
		 *            Mien, NPP) intG = 6: shop_id is null, customer_id is not
		 *            null, shop_channel is null - Gia cua Loai KH tren VNM
		 * 
		 * @author hunglm16
		 * @description Bo sung cho cac truong hop cos the co ve gia tren VNM
		 * **/
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		String sqlPrior = "";

		sql.append(" select count(1) as count from price pr ");
		sql.append(" where 1 = 1  ");
		if (filter.getStatus() != null) {
			sql.append("  and pr.status = ? ");
			params.add(filter.getStatus());
		} else {
			if (filter.getArrIntG() != null && filter.getArrIntG().isEmpty()) {
				sql.append("  and pr.status in (-10 ");
				for (Integer status : filter.getArrIntG()) {
					sql.append("  ,? ");
					params.add(status);
				}
				sql.append("  ) ");
			} else {
				sql.append("  and pr.status != -1 ");
			}
		}
		if (filter.getId() != null) {
			if (filter.getIsArr() != null && !filter.getIsArr()) {
				sql.append(" and pr.price_id != ?  ");
			} else {
				sql.append(" and pr.price_id = ?  ");
			}
			params.add(filter.getId());
		}

		if (filter.getShopId() != null) {
			if (filter.getIsConnectBy() != null && !filter.getIsConnectBy()) {
				sqlPrior = "  start with shop_id = ? connect by shop_id = prior parent_shop_id ";
			} else {
				sql.append(" and pr.shop_id != ? ");
				params.add(filter.getShopId());
				sqlPrior = "  start with shop_id = ? connect by prior shop_id = parent_shop_id ";
			}
		} else {
			if (filter.getShopChannel() != null) {
				sql.append(" and pr.shop_channel != ? ");
				params.add(filter.getShopChannel());
			}
			if (filter.getIsConnectBy() != null && !filter.getIsConnectBy()) {
				sqlPrior = " start with shop_id = pr.shop_id_temp connect by shop_id = prior parent_shop_id";
			} else {
				sqlPrior = " start with shop_id = pr.shop_id_temp connect by prior shop_id = parent_shop_id";
			}
		}

		if (filter.getIsLevel() != null) {
			sql.append(" and (pr.shop_id in (select shop_id from shop where status = 1 " + sqlPrior.trim() + ")) ");
		} else {
			sql.append(" and (pr.shop_id in (select shop_id from shop where status = 1 " + sqlPrior.trim() + " ) ");
			if (filter.getShopId() != null) {
				params.add(filter.getShopId());
			} else {
				sql.append(" or (pr.shop_id is null) ");
			}
			sql.append(" ) ");
		}

		if (filter.getCustomerTypeId() != null) {
			sql.append(" and pr.customer_type_id = ?  ");
			params.add(filter.getCustomerTypeId());
		}

		if (filter.getProductId() != null) {
			sql.append(" and pr.product_id = ?  ");
			params.add(filter.getProductId());
		}

		int c = repo.countBySQL(sql.toString(), params);
		if (filter.getCount() != null && filter.getCount() > 0) {
			return c < filter.getCount() ? false : true;
		}
		return c == 0 ? false : true;
	}

	@Override
	public Boolean checkPricetForStop(PriceFilter<Price> filter) throws DataAccessException {

		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from price pr ");
		sql.append(" where 1 = 1  ");
		sql.append(" and pr.status = 1 ");
		//Xet truong hop lay cap cao nhat
		if (filter.getIsLevel() != null && filter.getIsLevel().equals(filter.isLevelCom)) {
			sql.append(" and shop_channel is null and shop_id is null ");
		} else if (filter.getIsLevel() != null && filter.getIsLevel().equals(filter.isLevelParent)) {
			if (filter.getShopChannel() != null) {
				//Chi dung trong truong hop ban build thang 10, 2014
				sql.append(" and ((shop_channel = ? and shop_id is null) or (shop_channel is null and shop_id in (select shop_id from shop where status = 1  START WITH shop_channel  = 1 CONNECT BY shop_id = prior parent_shop_id )))  ");
				params.add(filter.getShopChannel());
			} else if (filter.getShopId() != null) {
				sql.append(" AND (shop_channel IS NULL or shop_channel = ?) AND (shop_id is null or shop_id IN (SELECT shop_id  FROM shop WHERE status = 1 and shop_channel = ? START WITH shop_id = ? CONNECT BY shop_id = prior parent_shop_id ))  ");
				if (filter.getShopChannelByShop() == null) {
					params.add(ActiveType.DELETED.getValue());
					params.add(ActiveType.DELETED.getValue());
				} else {
					params.add(filter.getShopChannelByShop());
					params.add(filter.getShopChannelByShop());
				}
				params.add(filter.getShopId());
			}
			//Tham so la ma loai khach hang
			if (filter.getCustomerTypeId() != null) {
				sql.append(" and pr.customer_type_id = ?  ");
				params.add(filter.getCustomerTypeId());
			} else {
				sql.append(" and pr.customer_type_id is null ");
			}

		} else if (filter.getIsLevel() != null && filter.getIsLevel().equals(filter.isLevelChid)) {
			if (filter.getShopChannel() != null) {
				//Chi dung trong truong hop ban build thang 10, 2014
				sql.append(" and ((shop_channel = ? and shop_id is null) or (shop_channel is null and shop_id in (select shop_id from shop where status = 1  START WITH shop_channel  = 1 CONNECT BY prior shop_id = parent_shop_id )))  ");
				params.add(filter.getShopChannel());
			} else if (filter.getShopId() != null) {
				sql.append(" and (shop_channel is null and shop_id in (select shop_id from shop where status = 1  START WITH shop_id  = ? CONNECT BY prior shop_id = parent_shop_id ))  ");
				params.add(filter.getShopId());
			}
			//Tham so la ma loai khach hang
			if (filter.getCustomerTypeId() != null) {
				sql.append(" and pr.customer_type_id = ?  ");
				params.add(filter.getCustomerTypeId());
			} else {
				sql.append(" and pr.customer_type_id is null ");
			}
		}
		//Tham so la ma ID Price
		if (filter.getId() != null) {
			if (filter.getIsArr() != null && !filter.getIsArr()) {
				sql.append(" and pr.price_id != ?  ");
			} else {
				sql.append(" and pr.price_id = ?  ");
			}
			params.add(filter.getId());
		}

		//Tham so la ma San pham
		if (filter.getProductId() != null) {
			sql.append(" and pr.product_id = ?  ");
			params.add(filter.getProductId());
		}

		int c = repo.countBySQL(sql.toString(), params);
		return c > 1 ? true : false;
	}

	@Override
	public Boolean checkPricetByFilterEx(PriceFilter<Price> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from price pr ");
		sql.append(" where 1 = 1  ");
		if (filter.getStatus() != null) {
			sql.append("  and pr.status = ? ");
			params.add(filter.getStatus());
		} else {
			if (filter.getArrIntG() != null && filter.getArrIntG().isEmpty()) {
				sql.append("  and pr.status in (-10 ");
				for (Integer status : filter.getArrIntG()) {
					sql.append("  ,? ");
					params.add(status);
				}
				sql.append("  ) ");
			} else {
				sql.append("  and pr.status != -1 ");
			}
		}
		if (filter.getId() != null) {
			if (filter.getIsArr() != null && !filter.getIsArr()) {
				sql.append(" and pr.price_id != ?  ");
			} else {
				sql.append(" and pr.price_id = ?  ");
			}
			params.add(filter.getId());
		}

		if (filter.getIsConnectBy() != null && !filter.getIsConnectBy()) {
			//Xet truong hop kiem tra cha
			if (filter.getShopId() != null) {
				sql.append(" and (pr.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by shop_id = prior parent_shop_id) or (pr.shop_id is null)) ");
				params.add(filter.getShopId());
			} else {
//				if (filter.getShopIdTemp() != null) {
//					sql.append(" and pr.shop_id_temp in (select shop_id from shop where status = 1 start with shop_id = ? connect by  shop_id = prior parent_shop_id) ");
//					params.add(filter.getShopIdTemp());
//				} else {
//					sql.append(" and pr.shop_id in (select shop_id from shop where status = 1 start with shop_id = pr.shop_id_temp connect by  shop_id = prior parent_shop_id) ");
//				}
			}
		} else {
			//Xet truong hop kiem tra con chau
			if (filter.getShopId() != null) {
				sql.append(" and pr.shop_id in (select shop_id from shop where status = 1 and shop_id != ? start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
				params.add(filter.getShopId());
				params.add(filter.getShopId());
			} else {
//				if (filter.getShopIdTemp() != null) {
//					sql.append(" and pr.shop_id in (select shop_id from shop where status = 1 start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
//					params.add(filter.getShopIdTemp());
//				} else {
//					sql.append(" and pr.shop_id in (select shop_id from shop where status = 1 start with shop_id = pr.shop_id_temp connect by prior shop_id = parent_shop_id) ");
//				}
			}
		}

		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" AND (pr.shop_id IN (select shop_id from shop  START WITH shop_id  in ( SELECT shop_id FROM shop WHERE lower(shop_code)  like ? escape '/' and status = 1) CONNECT BY prior shop_id = parent_shop_id) ");
			sql.append(" or (pr.shop_id_temp IN (select shop_id from shop  START WITH shop_id  in ( SELECT shop_id FROM shop WHERE lower(shop_code)  like ? escape '/' and status = 1) CONNECT BY prior shop_id = parent_shop_id)) ");
			sql.append(" ) ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toLowerCase()));
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toLowerCase()));
		}

		if (filter.getCustomerTypeId() != null) {
			sql.append(" and pr.customer_type_id = ?  ");
			params.add(filter.getCustomerTypeId());
		} else {
			sql.append(" and pr.customer_type_id is null ");
		}

		if (filter.getProductId() != null) {
			sql.append(" and pr.product_id = ?  ");
			params.add(filter.getProductId());
		}

		int c = repo.countBySQL(sql.toString(), params);
		if (filter.getCount() != null && filter.getCount() > 0) {
			return c < filter.getCount() ? false : true;
		}
		return c == 0 ? false : true;
	}

	@Override
	public Boolean checkPricetByFilterInStockTotal(BasicFilter<Price> filter) throws DataAccessException {
		/**
		 * @param BasicFilter
		 *            <Price> intG = 1: shop_id is null, customer_id is null,
		 *            shop_channel is null - Gia tren VNM intG = 2: shop_id is
		 *            null, customer_id is null, shop_channel is not null - Gia
		 *            tren kenh intG = 3: shop_id is null, customer_id is not
		 *            null, shop_channel is not null - Gia cua loai KH tren kenh
		 *            intG = 4: shop_id is not null, customer_id is null,
		 *            shop_channel is null - Gia cua don vi (Vung, Mien, NPP)
		 *            intG = 5: shop_id is not null, customer_id is not null,
		 *            shop_channel is null - Gia cua Loai KH tren Don vi (Vung,
		 *            Mien, NPP) intG = 6: shop_id is null, customer_id is not
		 *            null, shop_channel is null - Gia cua Loai KH tren VNM
		 * 
		 * @author hunglm16
		 * @description Bo sung cho cac truong hop cos the co ve gia tren VNM
		 * **/
		StringBuilder sql = new StringBuilder("");
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from stock_total ");
		sql.append(" where 1 = 1  ");
		if (filter.getStatus() != null) {
			sql.append("  and status = ? ");
			params.add(filter.getStatus());
		} else {
			if (filter.getArrIntG() != null && filter.getArrIntG().isEmpty()) {
				sql.append("  and status in (-10 ");
				for (Integer status : filter.getArrIntG()) {
					sql.append("  ,? ");
					params.add(status);
				}
				sql.append("  ) ");
			} else {
				sql.append("  and status != -1 ");
			}
		}
		if (filter.getLongType() != null) {
			sql.append(" and product_id = ? ");
			params.add(filter.getLongType());
		}
		if (filter.getLongG() != null) {
			sql.append(" and warehouse_id in ( ");
			sql.append(" select warehouse_id from warehouse where status = 1 and shop_id in(select shop_id from shop where status = 1 start with shop_id = ? connect by prior shop_id = parent_shop_id))");
			params.add(filter.getLongG());
		}
		int c = repo.countBySQL(sql.toString(), params);
		if (filter.getIntFlag() != null && filter.getIntFlag() > 0) {
			return c < filter.getIntFlag() ? false : true;
		}
		return c == 0 ? false : true;
	}

	@Override
	public List<PriceVO> searchPriceVOByFilter(PriceFilter<PriceVO> filter) throws DataAccessException {				
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder(); 
		List<Object> params = new ArrayList<Object>();
		
		sql.append("SELECT shType.shop_type_id shopTypeId,");
		sql.append(" shType.name shopTypeName,");
		sql.append(" sh.shop_id shopId,");
		sql.append(" sh.name_text shopName,");
		sql.append(" custype.channel_type_id  customerTypeId,");
		sql.append(" custype.channel_type_name customerTypeName,");
		sql.append(" cus.customer_id  customerId,");
		sql.append(" cus.customer_name customerName,");
		sql.append(" pr.product_id productId,");
		sql.append(" pd.product_code productCode,");
		sql.append(" pd.product_name productName, ");
		sql.append(" TO_CHAR(pr.from_date, 'dd/MM/yyyy') fromDateStr,");
		sql.append(" TO_CHAR(pr.to_date, 'dd/MM/yyyy') toDateStr,");
		sql.append(" pr.price_id id, ");
		sql.append(" pr.vat vat,");
		sql.append(" pr.package_price pricePackage,");
		sql.append(" pr.package_price_not_vat pricePackageNotVAT,");
		sql.append(" pr.price price,");
		sql.append(" pr.price_not_vat priceNotVAT,");
		sql.append(" pr.status status");
		sql.append(" FROM price pr");
		sql.append(" join product pd on pr.product_id = pd.product_id");
		sql.append(" left join shop sh on pr.shop_id = sh.shop_id and sh.status = 1");
		sql.append(" left join shop_type shType on pr.shop_type_id = shType.shop_type_id");
		sql.append(" left join customer cus on pr.customer_id = cus.customer_id");
		sql.append(" left join channel_type custype on pr.customer_type_id = custype.channel_type_id and custype.type = 3");
		sql.append(" where 1 = 1 and pd.status = 1");
		
		//Ma san pham
		if (!StringUtility.isNullOrEmpty(filter.getProductCode())) {
			sql.append("  and pr.product_id in (select product_id from product where status = 1 and lower(product_code) like ? escape '/') ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getProductCode().trim().toLowerCase()));
		}
		
		//Ten san pham
		if (!StringUtility.isNullOrEmpty(filter.getProductName())) {
			sql.append("  and pr.product_id in (select product_id from product where status = 1 and unicode2english(product_name) like ? escape '/') ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getProductName().trim().toLowerCase().trim())));
		}
		
		//Ngach hang
		if (filter.getCatId() != null && filter.getCatId() > 0) {
			sql.append("  and pr.product_id in (select product_id from product where status = 1 and cat_id = ?) ");
			params.add(filter.getCatId());
		}
	
		//Loai don vi
		if (filter.getShopTypeId() != null && filter.getShopTypeId() > 0) {
			sql.append("  and pr.shop_type_id in (select shop_type_id from shop_type where status = 1 and shop_type_id = ?) ");
			params.add(filter.getShopTypeId());
		}
		
		//Ma Don vi
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("  and pr.shop_id in (select shop_id from shop where status = 1 and lower(shop_code) like ? escape '/') ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode().trim().toLowerCase()));
		}

		//Loai khach hang
		if (filter.getCustomerTypeId() != null && filter.getCustomerTypeId() > 0) {
			sql.append("  and pr.customer_type_id in (select channel_type_id from channel_type where status = 1 and type=3 and channel_type_id = ?) ");
			params.add(filter.getCustomerTypeId());
		}
		
		//Ma Khach hang
		if (!StringUtility.isNullOrEmpty(filter.getCustomerCode())) {
			sql.append("  and pr.customer_id in (select customer_id from customer where status = 1 and lower(short_code) like ? escape '/') ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().trim().toLowerCase()));
		}
		
		//Tu ngay- den ngay
		if (filter.getFromDate() != null && filter.getToDate() != null) {
			sql.append(" and ((pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?)))  ");
			params.add(filter.getToDate());
			params.add(filter.getToDate());
			sql.append("	or (pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?)))  ");
			params.add(filter.getFromDate());
			params.add(filter.getFromDate());
			sql.append("	or (pr.from_date < trunc(?) + 1 and (pr.to_date is null or pr.to_date >= trunc(?)))  ");
			params.add(filter.getFromDate());
			params.add(filter.getToDate());
			sql.append("	or (pr.from_date >= trunc(?) and pr.to_date < trunc(?) + 1)  ");
			params.add(filter.getFromDate());
			params.add(filter.getToDate());
			sql.append(" )  ");

		} else if (filter.getFromDate() != null && filter.getToDate() == null) {
			sql.append(" and (pr.to_date is null or pr.to_date >= trunc(?)) ");
			params.add(filter.getFromDate());
		} else if (filter.getFromDate() == null && filter.getToDate() != null) {
			sql.append(" and pr.from_date < trunc(?) + 1 ");
			params.add(filter.getToDate());
		}
		
		if (filter.getStatus() != null) {
			sql.append("  and pr.status = ? ");
			params.add(filter.getStatus());
		} else {
			sql.append("  and pr.status != -1 ");
		}
//		if (filter.getLongType() != null) {
//			sql.append("  and pr.customer_type_id = ? ");
//			params.add(filter.getLongType());
//		}
//
//		sql.append(" and (pr.shop_channel in (select shop_channel from rshoproot) or pr.shop_id in (select shop_id from rshoproot) or  pr.shop_channel is null or pr.shop_id is null)	 ");
//
//		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
//			if (filter.getIsLevelShop() != null && filter.getIsLevelShop() > ShopDecentralizationSTT.KENH.getValue()) {
//				sql.append("   and pr.shop_id in (select shop_id from rshop) ");
//			} else {
//				sql.append(" and (pr.shop_channel in (select shop_channel from rshop) or pr.shop_id in (select shop_id from rshop) or  pr.shop_channel is null or pr.shop_id is null) ");
//			}
//		}
//		
//
		

		countSql.append(" SELECT count(1) as count from ( ");
		countSql.append(sql.toString());
		countSql.append("  )  ");

		sql.append("  order by shType.name, sh.name_text,custype.channel_type_name,cus.customer_name,pd.product_code, TO_CHAR(pr.from_date, 'dd/MM/yyyy') desc,TO_CHAR(pr.to_date, 'dd/MM/yyyy') desc");
		
	
		String[] fieldNames = new String[] { "shopTypeId", "shopTypeName", "shopId", "shopName", 
											 "customerTypeId", "customerTypeName", "customerId", "customerName", 
											 "productId", "productCode", "productName", "fromDateStr", 
											 "toDateStr", "id" ,"vat" , "pricePackage", 
											 "pricePackageNotVAT", "price" , "priceNotVAT" ,"status"};
		Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING, 
										 StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.STRING,
										 StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
										 StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.FLOAT,	StandardBasicTypes.BIG_DECIMAL,
										 StandardBasicTypes.BIG_DECIMAL,StandardBasicTypes.BIG_DECIMAL,StandardBasicTypes.BIG_DECIMAL, StandardBasicTypes.INTEGER};
		if (filter.getkPaging() != null) {
			return repo.getListByQueryAndScalarPaginated(PriceVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		} else {
			return repo.getListByQueryAndScalar(PriceVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
	}

	@Override
	public List<Price> getListPriceByFilter(PriceFilter<Price> filter) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select * from price pr ");
		sql.append(" where 1 = 1 ");
		if (filter.getArrlongG() != null && !filter.getArrlongG().isEmpty()) {
			sql.append(" and pr.price_id in (-1 ");
			for (Long priceId : filter.getArrlongG()) {
				sql.append(" ,? ");
				params.add(priceId);
			}
			sql.append(" ) ");
		}
		if (filter.getStatus() != null) {
			sql.append(" and pr.status = ?  ");
			params.add(filter.getStatus());
		}
		if (filter.getProductId()!=null) {
			sql.append(" and pr.product_id = ?  ");
			params.add(filter.getProductId());
		}
		if (filter.getIsLevel() != null) {
			//Se lay doi ngau cac gia
			if (filter.getIsLevel().equals(filter.isLevelCom)) {
				sql.append(" and pr.customer_type_id is null and pr.shop_id is null and pr.shop_channel is null ");
			}
			if (filter.getShopChannel() != null) {
				sql.append(" and pr.shop_channel = ?  ");
				params.add(filter.getShopChannel());
			} else {
				sql.append(" and pr.shop_channel is null  ");
			}
			if (filter.getShopId() != null) {
				sql.append(" and (pr.shop_channel is null and pr.shop_id in (select shop_id from shop where status = 1   ");
				//TODO Xet theo truong hop lay theo Shop Channel
				if (filter.getShopChannelByShop() != null) {
					sql.append(" 		and shop_channel = ?   ");
					params.add(filter.getShopChannelByShop());
				}
				sql.append(" 		start with shop_id  = ? and  status = 1   ");
				params.add(filter.getShopId());
				if (filter.getShopChannelByShop() != null) {
					sql.append(" 		and shop_channel = ?   ");
					params.add(filter.getShopChannelByShop());
				}
				sql.append(" 		connect by prior shop_id = parent_shop_id))  ");
			} else {
				sql.append(" and pr.shop_id is null  ");
			}
			if (filter.getCustomerTypeId() != null) {
				sql.append(" and pr.customer_type_id = ?  ");
				params.add(filter.getCustomerTypeId());
			} else {
				sql.append(" and pr.customer_type_id is null ");
			}
		} else {
			if (filter.getShopChannel() != null) {
				sql.append(" and pr.shop_channel = ?  ");
				params.add(filter.getShopChannel());
			}
			if (filter.getShopId() != null) {
				sql.append(" and (pr.shop_channel is null and pr.shop_id in (select shop_id from shop where status = 1   ");
				//TODO Xet theo truong hop lay theo Shop Channel
				if (filter.getShopChannelByShop() != null) {
					sql.append(" and shop_channel = ?   ");
					params.add(filter.getShopChannelByShop());
				}
				sql.append(" start with shop_id  = ? and  status = 1   ");
				params.add(filter.getShopId());
				if (filter.getShopChannelByShop() != null) {
					sql.append(" and shop_channel = ?   ");
					params.add(filter.getShopChannelByShop());
				}
				sql.append(" connect by prior shop_id = parent_shop_id))  ");
			} else if (filter.getShopChannelByShop() != null) {
				sql.append(" and (pr.shop_channel is null and pr.shop_id in (select shop_id from shop where status = 1   ");
				//TODO Xet theo truong hop lay theo Shop Channel
				sql.append(" and shop_channel = ?   ");
				params.add(filter.getShopChannelByShop());
				sql.append(" start with status = 1  and shop_channel = ?   ");
				params.add(filter.getShopChannelByShop());
				sql.append(" connect by prior shop_id = parent_shop_id))  ");
			}

			if (filter.getCustomerTypeId() != null) {
				sql.append(" and pr.customer_type_id = ?  ");
				params.add(filter.getCustomerTypeId());
			}
		}

		if (filter.getkPaging() == null) {
			return repo.getListBySQL(Price.class, sql.toString(), params);
		} else {
			return repo.getListBySQLPaginated(Price.class, sql.toString(), params, filter.getkPaging());
		}
	}

	@Override
	public List<Product> getInvalidPriceProducts(List<String> productCodes, Long customerId, Long customerShopId, Date lockDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from product p ");
		sql.append(" where 1=1 ");
		if (productCodes != null && productCodes.size() > 0) {
			sql.append(" and p.product_code in ( ");
			String subSql = "";
			for (String productCode : productCodes) {
				subSql += ", ?";
				params.add(productCode);
			}
			sql.append(subSql.replaceFirst(", ", ""));
			sql.append(" ) ");
		}
		/*
		 * yeu cau nghiep vu moi: chi can san pham co gia la co the ban, ko quan tam san pham thuoc nganh hang Z hay co gia khac 0 hay ko
		 * @modified by tuannd20
		 * @date 17/12/2014
		 */
		// san pham ko thuoc nganh hang Z
		/*sql.append(" and not exists ( ");
		sql.append("     select 1 ");
		sql.append("     from product_info pi ");
		sql.append("     where 1=1 ");
		sql.append("     and pi.type = 1 and pi.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("     and pi.product_info_code in (select ap_param_code from ap_param where type = 'EQUIPMENT_CAT' and status = ?) ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("     and pi.product_info_id = p.cat_id ");
		sql.append(" ) ");*/
		sql.append(" and not exists ( ");
		sql.append("     select 1 ");
		sql.append("     from price pr ");
		sql.append("     where 1 = 1 ");
		sql.append("     and pr.product_id = p.product_id ");
		/*
		 * yeu cau nghiep vu moi: chi can san pham co gia la co the ban, ko quan tam san pham thuoc nganh hang Z hay co gia khac 0 hay ko
		 * @modified by tuannd20
		 * @date 17/12/2014
		 */
		//sql.append("     and pr.price != 0 ");
		sql.append("     and pr.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		if (lockDate != null) {
			sql.append("     and pr.from_date < trunc(?) + 1 ");
			params.add(lockDate);
			sql.append("     and (pr.to_date >= trunc(?) or pr.to_date is null) ");
			params.add(lockDate);
		}
		if (customerId != null) {
			sql.append("     and (pr.customer_type_id is null or pr.customer_type_id = (select channel_type_id from customer where customer_id = ?)) ");
			params.add(customerId);
		}
		if (customerShopId != null) {
			sql.append("     and ( ");
			sql.append("         ( pr.shop_id is not null and pr.shop_channel is null ");
			sql.append("         and exists ( ");
			sql.append("         		select * ");
			sql.append(" 				from shop ");
			sql.append(" 				where status = ? and shop_id = ? /*customer_shop_id*/ ");
			params.add(ActiveType.RUNNING.getValue());
			params.add(customerShopId);
			sql.append(" 				start with shop_id = pr.shop_id ");
			sql.append(" 				connect by prior shop_id = parent_shop_id ");
			sql.append("         	)) or ");
			sql.append("         ( pr.shop_id is null and pr.shop_channel is not null ");
			sql.append("         and pr.shop_channel = (select shop_channel from shop where shop_id = ?) /*customer_shop_id*/ ");
			params.add(customerShopId);
			sql.append("         ) ");
			sql.append("         or (pr.shop_id is null and pr.shop_channel is null) ");
			sql.append("     ) ");
			sql.append(" ) ");
		}
		return repo.getListBySQL(Product.class, sql.toString(), params);
	}
	
	@Override
	public List<Price> getPriceByFullCondition(Long proId, Long shopId, Date date,
			Long shopTypeId, Long cusId, Long cusTypeId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();	
		sql.append(" with ");
		sql.append(" lstShop as ( ");
		sql.append(" select shop_id ,level as lv ");
		sql.append(" from shop s1 ");
		sql.append(" where status = 1 ");
		sql.append(" start with s1.shop_id = ? ");
		params.add(shopId);
		sql.append(" connect by prior parent_shop_id = shop_id ");
		sql.append(" ) ");
		sql.append(" ,prc as ( ");
		sql.append(" SELECT	product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append(" FROM	 price	prc ");
		sql.append(" WHERE	prc.product_id = ? ");
		params.add(proId);
		sql.append(" AND prc.status = 1 ");
		sql.append(" and prc.from_date < trunc(?) + 1 ");
		sql.append(" and (prc.to_date is null or prc.to_date >= trunc(?) ) ");
		params.add(date);
		params.add(date);
		sql.append(" and (shop_id is null or shop_id in (select shop_id from lstShop)) ");
		sql.append(" ) ");
		sql.append(" ,prcX as ( ");
		sql.append(" select ");
		sql.append(" product_id,customer_id,customer_type_id,prc.shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append(" ,lstShop.lv, rank() over(PARTITION BY product_id ORDER BY lstShop.lv) rank_last ");
		sql.append(" from prc join lstShop on prc.shop_id = lstShop.shop_id ");
		sql.append(" where prc.customer_type_id is null ");// --(Loại 5)
		sql.append(" union all ");
		sql.append(" select ");
		sql.append(" product_id,customer_id,customer_type_id,prc.shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append(" ,lstShop.lv, rank() over(PARTITION BY product_id ORDER BY lstShop.lv) rank_last ");
		sql.append(" from prc join lstShop on prc.shop_id = lstShop.shop_id ");
		sql.append(" where prc.customer_type_id = ?  ");//--(Loại 2)
		params.add(cusTypeId != null? cusTypeId: 0);
		sql.append(" ) ");
		sql.append(" ,prc_rec as( ");
		sql.append(" select product_id,shop_id,lv, rank_last, from_date, to_date ,price_id,customer_id,customer_type_id,shop_type_id ");
		sql.append(" from prcX ");
		sql.append(" where rank_last = 1 ");
		sql.append(" ) ");
		sql.append(" ,result as ( ");
		sql.append(" select product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append(" from prc_rec ");
		sql.append(" union all ");
		sql.append(" select product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append(" from prc where customer_id = ? ");//--(Loại 1) 
		params.add(cusId);
		sql.append(" union all ");
		sql.append(" select product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append(" from prc where customer_type_id = ? and shop_id is null and (shop_type_id is null or shop_type_id = ? ) ");//--(Loại 3 và 4)
		params.add(cusTypeId != null? cusTypeId: 0);
		params.add(shopTypeId);
		sql.append(" union all ");
		sql.append(" select product_id,customer_id,customer_type_id,shop_id,shop_type_id, from_date, to_date ,price_id ");
		sql.append(" from prc where customer_type_id is null and shop_id is null and shop_type_id = ?  ");//--(Loại 6)
		params.add(shopTypeId);
		sql.append(" ) ");
		sql.append(" ,resultAll as ( ");
		sql.append(" SELECT ");
		sql.append(" result.product_id, ");
		sql.append(" max(case when result.customer_id = ? then price_id end) pr_cus, ");
		params.add(cusId);
		sql.append(" max(case when result.customer_id is null and result.customer_type_id = ? ");
		params.add(cusTypeId != null? cusTypeId: 0);
		sql.append(" and result.shop_id is not null and result.shop_type_id is null then price_id	end ");
		sql.append(" ) pr_cus_type_and_shop_id, ");
		sql.append(" max( ");
		sql.append(" case when result.customer_id is null and result.customer_type_id = ? ");
		params.add(cusTypeId != null? cusTypeId: 0);
		sql.append(" and result.shop_id is null and result.shop_type_id = ? then price_id end ");
		params.add(shopTypeId);
		sql.append(" ) pr_cus_type_and_shop_type, ");
		sql.append(" max( ");
		sql.append(" case when result.customer_id is null and result.customer_type_id = ? ");
		params.add(cusTypeId != null? cusTypeId: 0);
		sql.append(" and result.shop_id is null and result.shop_type_id is null then price_id end ");
		sql.append(" ) pr_cus_type, ");
		sql.append(" max( ");
		sql.append(" case when result.customer_id is null and result.customer_type_id is null ");
		sql.append(" and result.shop_id is not null and result.shop_type_id is null then price_id end ");
		sql.append(" ) pr_shop_id, ");
		sql.append(" max( ");
		sql.append(" case when result.customer_id is null and result.customer_type_id is null ");
		sql.append(" and result.shop_id is null and result.shop_type_id = ? then price_id ");
		params.add(shopTypeId);
		sql.append(" end ");
		sql.append(" ) pr_shop_type ");
		sql.append(" FROM	result ");
		sql.append(" group by product_id ");
		sql.append(" ) ");
		sql.append(" ,priceTemp as ( ");
		sql.append(" select ");
		sql.append(" product_id, ");
		sql.append(" (case when pr_cus is not null then pr_cus ");
		sql.append(" when pr_cus_type_and_shop_id is not null then pr_cus_type_and_shop_id ");
		sql.append(" when pr_cus_type_and_shop_type is not null then pr_cus_type_and_shop_type ");
		sql.append(" when pr_cus_type is not null then pr_cus_type ");
		sql.append(" when pr_shop_id is not null then pr_shop_id ");
		sql.append(" when pr_shop_type is not null then pr_shop_type ");
		sql.append(" else -1 ");
		sql.append(" end ) price_id ");
		sql.append(" from resultALL ");
		sql.append(" ) ");
		sql.append(" select pr.* ");
		sql.append(" from priceTemp temp ");
		sql.append(" join price pr on temp.price_id = pr.price_id ");
		return repo.getListBySQL(Price.class, sql.toString(), params);
	}
	
	@Override
	public List<Price> getPriceByRpt(SaleOrderFilter<Price> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();	
		sql.append(" select p.* from price p where p.price_id in ( ");
		sql.append(" select pcd.price_id from PRICE_CUSTOMER_DEDUCED pcd ");
		sql.append(" where pcd.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		if (filter.getProductId() != null) {
			sql.append(" and pcd.product_id = ? ");
			params.add(filter.getProductId());
		}
		if (filter.getCustomerId() != null) {
			sql.append(" and pcd.customer_id = ? ");
			params.add(filter.getCustomerId());
		}
		if (filter.getShopId() != null) {
			sql.append(" and pcd.shop_id = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getOrderDate() != null) {
			sql.append(" and pcd.from_date < trunc(?)+1 and (pcd.to_date is null or pcd.to_date >= trunc(?)) ");
			params.add(filter.getOrderDate());
			params.add(filter.getOrderDate());
		}
		sql.append(" ) ");
		return repo.getListBySQL(Price.class, sql.toString(), params);
	}

	@Override
	public Price getPriceByProductAndCustomer(Long productId, Long customerId, List<Integer> status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from price where CUSTOMER_ID = ? and PRODUCT_ID = ? ");
		params.add(customerId);
		params.add(productId);
		if (status != null && status.size() > 0) {
			sql.append(" and STATUS in (? ");
			params.add(status.get(0));
			for (int i = 1, sz = status.size(); i < sz; i++) {
				sql.append(", ?");
				params.add(status.get(i));
			}
			sql.append(" ) ");
		}
		return repo.getEntityBySQL(Price.class, sql.toString(), params);
	}

	@Override
	public Price getPriceByProductChannelTypeShopAndCustomerNull(
			Long productId, Long channelTypeId, Long shopId, List<Integer> status)
			throws DataAccessException {
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from price where CUSTOMER_TYPE_ID = ? and PRODUCT_ID = ? and SHOP_ID = ? and CUSTOMER_ID is null and SHOP_TYPE_ID is null ");
		params.add(channelTypeId);
		params.add(productId);
		params.add(shopId);		
		if (status != null && status.size() > 0) {
			sql.append(" and STATUS in (? ");
			params.add(status.get(0));
			for (int i = 1, sz = status.size(); i < sz; i++) {
				sql.append(", ?");
				params.add(status.get(i));
			}
			sql.append(" ) ");
		}
		return repo.getEntityBySQL(Price.class, sql.toString(), params);
	}

	@Override
	public Price getPriceByProductChannelTypeShopType(Long customerTypeId,
			Long shopTypeId, Long productId, List<Integer> status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();		
		
		if (shopTypeId > 0) {
			sql.append("select * from price where CUSTOMER_TYPE_ID = ? and PRODUCT_ID = ? and SHOP_TYPE_ID = ? and CUSTOMER_ID is null and SHOP_ID is null ");
			params.add(customerTypeId);
			params.add(productId);
			params.add(shopTypeId);	
			if (status != null && status.size() > 0) {
				sql.append(" and STATUS in (? ");
				params.add(status.get(0));
				for (int i = 1, sz = status.size(); i < sz; i++) {
					sql.append(", ?");
					params.add(status.get(i));
				}
				sql.append(" ) ");
			}
		} else {
			sql.append("select * from price where CUSTOMER_TYPE_ID = ? and PRODUCT_ID = ? and SHOP_TYPE_ID is null and CUSTOMER_ID is null and SHOP_ID is null ");
			params.add(customerTypeId);
			params.add(productId);
			if (status != null && status.size() > 0) {
				sql.append(" and STATUS in (? ");
				params.add(status.get(0));
				for (int i = 1, sz = status.size(); i < sz; i++) {
					sql.append(", ?");
					params.add(status.get(i));
				}
				sql.append(" ) ");
			}
		}
		return repo.getEntityBySQL(Price.class, sql.toString(), params);		
	}

	@Override
	public Price getPriceByProductShop(Long shopId, Long productId, List<Integer> status)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();		
		
		sql.append("select * from price where SHOP_ID = ? and PRODUCT_ID = ? and SHOP_TYPE_ID is null and CUSTOMER_ID is null and CUSTOMER_TYPE_ID is null ");
		params.add(shopId);
		params.add(productId);
		if (status != null && status.size() > 0) {
			sql.append(" and STATUS in (? ");
			params.add(status.get(0));
			for (int i = 1, sz = status.size(); i < sz; i++) {
				sql.append(", ?");
				params.add(status.get(i));
			}
			sql.append(" ) ");
		}
		return repo.getEntityBySQL(Price.class, sql.toString(), params);	
	}

	@Override
	public Price getPriceByProductShopType(Long shopTypeId, Long productId, List<Integer> status)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();		
		
		sql.append("select * from price where SHOP_TYPE_ID = ? and PRODUCT_ID = ? and SHOP_ID is null and CUSTOMER_ID is null and CUSTOMER_TYPE_ID is null ");
		params.add(shopTypeId);
		params.add(productId);
		if (status != null && status.size() > 0) {
			sql.append(" and STATUS in (? ");
			params.add(status.get(0));
			for (int i = 1, sz = status.size(); i < sz; i++) {
				sql.append(", ?");
				params.add(status.get(i));
			}
			sql.append(" ) ");
		}
		return repo.getEntityBySQL(Price.class, sql.toString(), params);
	}
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.PromotionStaffMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.PromotionShopMapFilter;
import ths.dms.core.entities.filter.PromotionStaffFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PromotionShopMapVO;
import ths.dms.core.entities.vo.PromotionStaffVO;
import ths.dms.core.exceptions.DataAccessException;

public interface PromotionStaffMapDAO {

	/**
	 * Lay promotion_staff_map theo id
	 * 
	 * @author lacnv1
	 * @since Aug 26, 2014
	 */
	PromotionStaffMap getPromotionStaffMapById(long id) throws DataAccessException;
	
	/**
	 * Lay danh sach so suat NVBH cho CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 16, 2014
	 */
	List<PromotionStaffVO> getSalerInPromotionProgram(long promotionId, String shopCode, String shopName, Integer quantity) throws DataAccessException;
	
	/**
	 * Cap nhat promotion_staff_map
	 * 
	 * @author lacnv1
	 * @since Aug 26, 2014
	 */
	void updatePromotionStaffMap(PromotionStaffMap promotionStaffMap, LogInfoVO logInfo) throws DataAccessException;
	
	/**
	 * Cap nhat promotion_staff_map
	 * 
	 * @author lacnv1
	 * @since Aug 26, 2014
	 */
	PromotionStaffMap createPromotionStaffMap(PromotionStaffMap promotionStaffMap, LogInfoVO logInfo) throws DataAccessException;
	
	/**
	 * Lay ds promotion_staff_map theo CTKM va don vi
	 * 
	 * @author lacnv1
	 * @since Aug 26, 2014
	 */
	List<PromotionStaffMap> getListPromotionStaffMap(long promotionId, long shopId, Integer status) throws DataAccessException;
	
	/**
	 * Tim kiem NVBH them vao CTKM
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	List<PromotionStaffVO> searchStaffForPromotion(PromotionStaffFilter filter) throws DataAccessException;
	
	/**
	 * Kiem tra xem NVBH co ton tai trong CTKM chua
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	List<PromotionStaffVO> getListStaffInPromotion(long promotionId, List<Long> lstStaffId) throws DataAccessException;
	
	/**
	 * Tao promotion_staff_map
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	void createListPromotionStaffMap(List<PromotionStaffMap> lst, LogInfoVO logInfo) throws DataAccessException;
	
	/**
	 * Lay ds promotion_staff_map theo promotion_shop_map_id
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	List<PromotionStaffMap> getListPromotionStaffMapByShopMap(long shopMapId, Integer status) throws DataAccessException;
	
	/**
	 * Lay danh sach so suat NVBH
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	List<PromotionShopMapVO> getListPromotionShopMapVO(PromotionShopMapFilter filter) throws DataAccessException;
	
	/**
	 * Lay promotion_staff_map theo promotion_shop_map_id
	 * 
	 * @author lacnv1
	 * @since Aug 27, 2014
	 */
	PromotionStaffMap getPromotionStaffMapByShopMapAndStaff(long shopMapId, long staffId) throws DataAccessException;
	
	/**
	*  Lay list promotion staff map cho NVBH
	*  @author nhanlt6
	 * @since Oct 5, 2014
	*/
	List<PromotionStaffMap> getListPromotionStaffMapForCreateOrder(Long promotionProgramId, Long staffId, Long shopId, ActiveType status)throws DataAccessException;
	List<PromotionStaffMap> getListPromotionStaffMapAddPromotionStaff(Long promotionShopMapId, Long staffId, Long shopId)throws DataAccessException;
	
}
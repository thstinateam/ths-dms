package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.EquipGroup;
import ths.dms.core.entities.EquipStatisticGroup;
import ths.dms.core.entities.Equipment;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.EquipStockTotalType;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.EquipStatisticFilter;
import ths.dms.core.entities.vo.EquipmentRecordDetailVO;
import ths.dms.core.entities.vo.EquipmentVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.StatisticCheckingVO;
import ths.dms.core.exceptions.DataAccessException;

public interface EquipStatisticGroupDAO {

	List<EquipStatisticGroup> getListEquipStatisticGroup(Long recordId)
			throws DataAccessException;

	List<EquipGroup> getListEquipGroupByRecordId(Long recordId)
			throws DataAccessException;

	List<EquipGroup> getListEquipGroupExceptInRecord(KPaging<EquipGroup> kPaging, Long recordId, String code, Long catId, Float fromCapacity, Float toCapacity, String brand)
			throws DataAccessException;

	List<EquipmentRecordDetailVO> getListEquipmentRecordDetailVOByRecordId(
			Long recordId) throws DataAccessException;

	List<StatisticCheckingVO> getListStatisticCheckingVOByRecordId(
			KPaging<StatisticCheckingVO> kPaging, Long recordId,
			String shopCode, String shortCode, String address,
			String equipCode, String seri, Integer status)
			throws DataAccessException;

	/**
	 * Lay danh sach hinh anh kiem ke thiet bi
	 * @author trietptm
	 * @param filter
	 * @return List<ImageVO>
	 * @throws DataAccessException
	 * @since Mar 21, 2016
	 */
	List<ImageVO> getListImageVO(EquipStatisticFilter filter) throws DataAccessException;
	
	/**
	 * lay danh sach thiet bi trong nhom thiet bi
	 * @author tuannd20
	 * @param equipmentGroupId id cua nhom thiet bi
	 * @return danh sach thiet bi trong nhom thiet bi
	 * @throws DataAccessException
	 */
	List<Equipment> getEquipmentsInEquipmentGroup(Long equipmentGroupId, Long objectId, EquipStockTotalType stockType) throws DataAccessException;

	/**
	 * list u ke in record
	 * @author phut
	 */
	List<Product> getListShelfByRecordId(Long recordId)
			throws DataAccessException;

	/**
	 * list u, ke not in record
	 * @author phut
	 */
	List<Product> getListShelfExceptInRecord(KPaging<Product> kPaging,
			Long recordId, String code, String name) throws DataAccessException;
	/**
	 * lay danh sach thiet bi trong kiem ke
	 * @author phuongvm
	 * @param filter
	 * @return danh sach thiet bi trong nhom thiet bi
	 * @throws DataAccessException
	 */
	List<EquipmentVO> getListEquipmentByStatisticFilter(
			EquipmentFilter<EquipmentVO> filter) throws DataAccessException;
	/**
	 * lay danh sach thiet bi trong kiem ke
	 * @author phuongvm
	 * @param filter
	 * @return danh sach thiet bi trong nhom thiet bi
	 * @throws DataAccessException
	 */
	List<Equipment> getListEquipByRecordId(Long recordId)
			throws DataAccessException;
	/**
	 * lay danh sach thiet bi nvbh de xuat excel tab khach hang
	 * @author phuongvm
	 * @param filter
	 * @return danh sach thiet bi trong nhom thiet bi
	 * @throws DataAccessException
	 */
	List<EquipmentVO> getListEquipmentStaffByStatisticFilter(
			EquipmentFilter<EquipmentVO> filter) throws DataAccessException;
}

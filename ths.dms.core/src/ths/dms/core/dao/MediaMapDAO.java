package ths.dms.core.dao;

import ths.dms.core.entities.MediaMap;
import ths.dms.core.exceptions.DataAccessException;

public interface MediaMapDAO {

	MediaMap createMediaMap(MediaMap mediaMap) throws DataAccessException;

	void deleteMediaMap(MediaMap mediaMap) throws DataAccessException;

	MediaMap getMediaMapById(long id) throws DataAccessException;

	MediaMap updateMediaMap(MediaMap mediaMap) throws DataAccessException;

	MediaMap getMediaMapByMediaAndObject(Long mediaId, Long objectId,
			Long objectType) throws DataAccessException;	
}

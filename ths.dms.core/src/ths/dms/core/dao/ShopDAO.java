package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ProgramObjectType;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.WorkingDateType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.RptStaffSaleFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.UnitFilter;

public interface ShopDAO {

	Shop createShop(Shop shop, LogInfoVO logInfo) throws DataAccessException;

	List<Shop> getListShopSaleMTByShopCode(String shopCode, Long shopId) throws DataAccessException;

	List<Shop> getListShopSaleMTByShopName(String shopCode, Long shopId) throws DataAccessException;

	List<Shop> getListSubShopSaleMTIncludeStopped(Long parentId, Boolean oneLevel, Boolean includeStopped) throws DataAccessException;

	void deleteShop(Shop shop, LogInfoVO logInfo) throws DataAccessException;

	List<Shop> getListSubShopSaleMT(Long parentId, Boolean oneLevel) throws DataAccessException;

	List<Shop> getSearchShopTreeVO(Long shopId, List<Long> lstExceptionalShopId, String shopCode, String shopName, ActiveType status, Long programId, ProgramObjectType programObjectType, Boolean isOrderByName) throws DataAccessException;

	List<Shop> getSearchShopTreeVO(Long shopId, List<Long> lstExceptionalShopId, String shopCode, String shopName, ActiveType status, Long programId, ProgramObjectType programObjectType, Long staffShopId, Boolean isGetChildOnly, Boolean isOrderByName, String strListShopId)
			throws DataAccessException;

	List<Shop> getListShopExport(Long shopId) throws DataAccessException;

	Shop getShopById(long id) throws DataAccessException;

	List<Shop> getAllShopChild(Long parentId) throws DataAccessException;

	List<Shop> getShopByUserSaleMT(Long userId) throws DataAccessException;

	void updateShop(Shop shop, LogInfoVO logInfo) throws DataAccessException;

	Shop getShopByCode(String code) throws DataAccessException;

	List<ShopVO> getListShopVOBasicByFilter(BasicFilter<ShopVO> filter) throws DataAccessException;
	List<ShopVO> getListShopVOBasicByFilter_GTMT(BasicFilter<ShopVO> filter) throws DataAccessException;

	Shop getShopRunningByCode(String code) throws DataAccessException;

	Shop getShopByParentIdNull() throws DataAccessException;

	Shop getShopRoot(Long shopId) throws DataAccessException;

	List<ShopVO> getListShopVOByFilter(BasicFilter<ShopVO> filter) throws DataAccessException;

	List<Shop> getListShopByFilter(BasicFilter<Shop> filter) throws DataAccessException;

	List<Shop> getAllShopChildrenByLstId(List<Long> lstId) throws DataAccessException;

	List<ShopVO> getListChildShopVOByFilter(BasicFilter<ShopVO> filter) throws DataAccessException;

	List<Shop> getListSubShop(Long parentId, String shopCode, String shopName, ActiveType status, Long programId, ProgramObjectType programObjectType, Long exceptionalShopId) throws DataAccessException;

	Shop getShopByIdAndIslevel(Long id, Integer isLevel) throws DataAccessException;

	Shop getShopByIdChannelIslevel(Long id, Integer isLevel, Integer shopChannel) throws DataAccessException;

	List<Shop> getListShopNotInPoGroup(ShopFilter filter, long shopId) throws DataAccessException;

	/**
	 * Lay danh muc cho bao cao loai HMDHT
	 * 
	 * @author hunglm16
	 * @since September 27,2014
	 * @description ke thua lai ham cu, khong chinh sua
	 * */
	Object getDMDHT(Long shopId) throws DataAccessException;

	/**
	 * Lay danh sach don vi theo shopFilter
	 * 
	 * @author tungmt
	 * @since S02/102014
	 * @description
	 * */
	List<Shop> getListShop(ShopFilter filter) throws DataAccessException;

	Boolean checkIfShopExists(ShopFilter filter) throws DataAccessException;

	/**
	 * Huy ke hoach huan luyen cua NVBH
	 * 
	 * @author lacnv1
	 * @since Jun 11, 2014
	 */
	void cancelTrainingPlanOfSaler(long shopId) throws DataAccessException;

	List<Shop> getListShopOfUnitTree(KPaging<Shop> kPaging, Long curShopId, String shopCode, String shopName, ActiveType status) throws DataAccessException;

	List<Shop> getSearchShopTreeDO(Long userId, Long shopID, Long offDocuID, String shopCode, String shopName) throws DataAccessException;

	List<ProgrameExtentVO> getListProgrameExtentVOExOD(List<Shop> lstShop, Long officeDocId) throws DataAccessException;

	Shop getShopArea(Long shopId) throws DataAccessException;

	Shop getShopByCodeSaleMT(String code) throws DataAccessException;

	Boolean checkAncestor(String parentCode, String shopCode) throws DataAccessException;

	List<Shop> getListShopOfUnitTreeSaleMT(KPaging<Shop> kPaging, String shopCode, String shopName, ActiveType status) throws DataAccessException;

	/**
	 * Lay so luong shop con
	 * 
	 * @author tungmt
	 * @since 10/07/14
	 */
	int getCountChildByShop(ShopFilter filter) throws DataAccessException;

	List<Shop> getListChildShopByFilterEx(BasicFilter<ShopVO> filter) throws DataAccessException;

	/**
	 * Lay shop thep shopchannel
	 * 
	 * @author hunglm16
	 * @since October 20,2014
	 * @description chi ap dung cho dot thang 10
	 * */
	Shop getShopByShopChannelWithObjectType(Integer shopChannel, Integer objectType) throws DataAccessException;

	ShopVO getShopVOByShopId(Long shopId) throws DataAccessException;

	List<ShopVO> getListChildShopVOByShopRoot(Long shopRootId, Integer status) throws DataAccessException;
	/**
	 * Lay shop thep shopchannel
	 * 
	 * @author hunglm16
	 * @since October 20,2014
	 * @description chi ap dung cho dot thang 10
	 * */
	List<ShopVO> searchListChildShopVOByFilter(BasicFilter<ShopVO> filter) throws DataAccessException;
	/**
	 * get shop parent working date config
	 * 
	 * @author liemtpt
	 * @since Feb 02,2014
	 * @description Lay shop cha theo dk shop chon 
	 * */
	Shop getShopParentWorkingDateConfig(Long id, WorkingDateType type)
			throws DataAccessException;

	/**
	 * @author vuongmq
	 * @date 13/02/2015
	 * @description: lay list Shop voi id con Organization
	 */
	List<Shop> getListShopOrganization(ShopFilter filter) throws DataAccessException;

	/**
	 * @author vuongmq
	 * @date 30/01/2015
	 * @description: lay list ShopVO parent voi id con
	 * chua khai bao o Mgr
	 */
	/*List<ShopVO> getListShopParentByShopChildren(Long shopId) throws DataAccessException;*/
	
	/**
	 * tim kiem don vi
	 * @author tuannd20
	 * @param kPaging thong tin phan trang
	 * @param unitFilter tieu chi tim kiem
	 * @return danh sach don vi
	 * @throws DataAccessException
	 * @since 26/02/2015
	 */
	List<ShopVO> findShopVOBy(KPaging<ShopVO> kPaging, UnitFilter unitFilter) throws DataAccessException;
	
	/**
	 * lay danh sach don vi dang co tren cay don vi
	 * @author tuannd20
	 * @param startFromShops Danh sach cac shop dung lam node goc cua cay. Neu truyen vao Null, mac dinh lay shop cao nhat (shop co parent = null) lam node goc
	 * @param includeStoppedShop true: lay ca nhung don vi tam ngung. false: ko lay don vi tam ngung
	 * @return danh sach don vi dang co tren cay don vi
	 * @throws DataAccessException
	 * @since 27/02/2015
	 */
	List<Shop> getShopsOnTree(List<Long> startFromShops, Boolean includeStoppedShop) throws DataAccessException;
	/**
	 * checkShopHaveActiveSubShop: kiem tra shop co shop con hoat dong
	 * @author phuocdh2
	 * @description: Nhan vao shopId va kiem tra shop con co hoat dong
	 * @return boolean
	 * @since 02/03/2015
	 */
	Boolean checkShopHaveActiveSubShop(ActiveType status, Long shopId) throws DataAccessException;
	/**
	 * checkShopHaveActiveStaff lay so luong nhan vien truc thuoc shop nhap vao va 
	 * @author phuocdh2
	 * @return true: co nhan vien dang hoat dong false : shop khong co nhan vien dang hoat dong
	 * @throws DataAccessException
	 * @since 02/03/2015
	 */
	Boolean checkShopHaveActiveStaff(ActiveType status,Long shopId) throws DataAccessException ;
	
	/**
	 * checkShopHaveActiveCustomer lay so luong khach hang hoat dong truc thuoc shop nhap vao va 
	 * @author phuocdh2
	 * @return true : shop co khach hang hoat dong false : shop co khach hang het hoat dong
	 * @throws DataAccessException
	 * @since 02/03/2015
	 */
	Boolean checkShopHaveActiveCustomer(ActiveType status,Long shopId) throws DataAccessException ;
	/**
	 * tra ve danh sach don vi de export ra file excel
	 * @param kPaging thong tin phan trang
	 * @param unitFilter tieu chi tim kiem
	 * @author phuocdh2
	 * @return danh sach don vi de export
	 * @since 24 Mar 2015
	 */
	 List<ShopVO> findShopVOExport(KPaging<ShopVO> kPaging, UnitFilter unitFilter) throws DataAccessException;

		/**
		 * lay danh sach shop_id theo level co cau hinh hien gia, an gia theo level tu 1-> 4... 
		 * shop con -> shop cha .. uu tien muc con, muc co level thap nhat
		 * @author phuocdh2
		 * @return List<ShopVO> danh sach shop_param
		 * @throws DataAccessException
		 * @since 08/04/2015
		 */
	List<ShopVO> getListShopVOConfigShowPrice(Long shopId, Integer status) throws DataAccessException ;
		

	 
	 /**lay danh sach don vi la NPP
	  * @author cuonglt3
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<Shop> getListShopSpectific(ShopFilter filter) throws DataAccessException;

	/**
	 * Lay cau hinh shop_param de qui len shop cha
	 * @author cuonglt3
	 * @param shopId
	 * @param sysModifyPrice
	 * @return
	 * @throws DataAccessException
	 */
	List<ShopVO> getListShopVOConfigShowPrice(Long shopId, ApParamType sysModifyPrice) throws DataAccessException; 
	
	/**
	 * Lay cau hinh shop_param theo paramCode lay de quy va order by
	 * @author trungtm6
	 * @since 21/04/2015
	 * @param shopId
	 * @param paramCode
	 * @return
	 * @throws DataAccessException
	 */
	List<ShopParam> getConfigShopParam (Long shopId, String paramCode) throws DataAccessException;

	/**
	 * @author tungmt
	 * @since 11/05/2015
	 * @param filter
	 * @return danh sach shop con
	 * @throws DataAccessException
	 */
	List<Shop> getListChildShop(ShopFilter filter) throws DataAccessException;
	
	/**
	 * lay danh sach don vi dang co tren cay don vi
	 * @author tuannd20
	 * @param shopFilter dieu kien tim kiem
	 * @return danh sach don vi dang co tren cay don vi
	 * @throws DataAccessException
	 * @since 27/02/2015
	 */
	List<Shop> getShopsOnTree(ShopFilter shopFilter) throws DataAccessException;
	
	/**
	 * @author tuannd20
	 * @param shopFilter
	 * @param keyshopId
	 * @return
	 * @throws DataAccessException
	 * @since 12/03/2015
	 */
	List<Shop> getShopsInAccessOnTree(ShopFilter shopFilter, Long keyshopId) throws DataAccessException;
	
	/**
	 * lay danh sach cac shop da duoc phan cho quyen du lieu
	 * @author tuannd20
	 * @param permissionId id quyen du lieu can lay
	 * @return danh sach cac shop da duoc phan cho quyen du lieu
	 * @throws DataAccessException
	 * @since 10/03/2015
	 */
	List<Shop> retriveShopsForKeyShop(Long keyshopId) throws DataAccessException;

	boolean isChild(Long parent, Long child) throws DataAccessException;
	
	/**
	 * Lay danh sach shop con ke can
	 * @author trungtm6
	 * @since 18/08/2015
	 * @param kPaging
	 * @param fitler
	 * @return
	 * @throws DataAccessException
	 */
	List<Shop> getListNextChildShop(ShopFilter filter) throws DataAccessException;

	/**
	 * @author vuongmq
	 * @date 21/08/2015
	 * @descriptip Lay danh sach shop cua NVGS quan ly
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<Shop> getListShopOfSupervise(RptStaffSaleFilter filter) throws DataAccessException;
	
	/**
	 * Lay cac shop cha & anh em cua shop <b>shopId<b/>
	 * @author tuannd20
	 * @param shopId id cua shop bat dau
	 * @return Danh sach shop cha & shop anh em cua shop shopId
	 * @throws DataAccessException
	 * @since 07/08/2015
	 */
	List<Shop> getAncestorAndSiblingShops(Long shopId) throws DataAccessException;

	/**
	 * Tim kiem don vi voi chuong trinh HTTM
	 * @author hunglm16
	 * @param shopFilter
	 * @param keyshopId
	 * @return
	 * @throws DataAccessException
	 * @since 17/11/2015
	 */
	List<ShopVO> searchShopsInAccessOnTreeForKeyShop(ShopFilter shopFilter, Long keyshopId) throws DataAccessException;

	/**
	 * Tim kiem don vi voi so suat CTHTTM
	 * @author hunglm16
	 * @param shopFilter
	 * @param keyshopId
	 * @return
	 * @throws DataAccessException
	 * @since 17/11/2015
	 */
	List<ShopVO> searchShopsOnTreeForKeyShop(ShopFilter shopFilter, Long keyShopId) throws DataAccessException;

	/**
	 * Lay danh sach don vi duoc check & con chau cua no
	 * @author hunglm16
	 * @param keyShopId
	 * @param status
	 * @return
	 * @throws DataAccessException
	 * @since 17/11/2015
	 */
	List<Shop> getShopForConnectChildrenByKeyShop(Long keyShopId, Integer status) throws DataAccessException;
	
	/**
	 * Lay danh sach don vi duoc check & con chau cua no
	 * @author hunglm16
	 * @param keyShopId
	 * @param cycleId
	 * @param status
	 * @param staffRootId
	 * @param roleId
	 * @param shopRootId
	 * @return
	 * @throws DataAccessException
	 * @since 19/11/2015
	 */
	List<ShopVO> getShopForConnectChildrenByKeyShopVO(Long keyShopId, Long cycleId, Integer status, Long staffRootId, Long roleId, Long shopRootId) throws DataAccessException;

	/**
	 * Lay danh sach con chau, khong lay chinh no
	 * @author hunglm16
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 * @since 19/11/2015
	 */
	List<Shop> getListChildShopId(Integer status, Long[] arrShopId) throws DataAccessException;
	
	/**
	 * Lay danh sach cha, khong lay chinh no
	 * @author hunglm16
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 * @since 19/11/2015
	 */
	List<Shop> getListParentShopId(Integer status, Long[] arrShopId) throws DataAccessException;
	
	/**
	 * lay tat ca cac shop NPP
	 * @author tuannd20
	 * @return
	 * @throws DataAccessException
	 * @since 13/01/2016
	 */
	List<Shop> getAllDistributorShop() throws DataAccessException;
	
	/**
	 * get all shop belong to record
	 * @author phut
	 */
	List<Shop> getAllShopByLstId(Long recordId, List<Long> lstId) throws DataAccessException;
	
	/**
	 * lay danh sach shop cha (long id)
	 * @author vuongmq
	 * @date March 24, 2015
	 */
	List<Long> getListLongShopParentByShopId(Long shopId) throws DataAccessException;
	
	/**
	 * lay danh sach shop con (long id)
	 * @author vuongmq
	 * @date March 24, 2015
	 */
	List<Long> getListLongShopChildByShopId(Long shopId) throws DataAccessException;
	
}
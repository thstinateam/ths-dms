package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.ShopParam;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ApParamType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.OrganizationNodeType;
import ths.dms.core.entities.enumtype.ProgramObjectType;
import ths.dms.core.entities.enumtype.ShopDecentralizationSTT;
import ths.dms.core.entities.enumtype.ShopFilter;
import ths.dms.core.entities.enumtype.ShopObjectType;
import ths.dms.core.entities.enumtype.ShopSpecificType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.TrainningPlanDetailStatus;
import ths.dms.core.entities.enumtype.WorkingDateType;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.RptStaffSaleFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.ProgrameExtentVO;
import ths.dms.core.entities.vo.ShopVO;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.UnitFilter;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class ShopDAOImpl implements ShopDAO {

	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;

	@Override
	public List<Shop> getSearchShopTreeVO(Long shopId, List<Long> lstExceptionalShopId, String shopCode, String shopName, ActiveType status, Long programId, ProgramObjectType programObjectType, Boolean isOrderByName) throws DataAccessException {
		return this.getSearchShopTreeVO(shopId, lstExceptionalShopId, shopCode, shopName, status, programId, programObjectType, null, null, isOrderByName, null);
	}

	@Override
	public List<Shop> getSearchShopTreeVO(Long shopId, List<Long> lstExceptionalShopId, String shopCode, String shopName, ActiveType status, Long programId, ProgramObjectType programObjectType, Long staffShopId, Boolean isGetChildOnly,
			Boolean isOrderByName, String strListShopId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT *");
		sql.append(" FROM shop s1");

		if (status != null) {
			sql.append(" where s1.status = ?");
			params.add(status.getValue());
		} else {
			sql.append("   where s1.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		if (!StringUtility.isNullOrEmpty(strListShopId)) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(strListShopId, "shop_id");
			sql.append(paramsFix);
		}

		sql.append(" AND EXISTS");
		sql.append("   (SELECT 1");
		sql.append("   FROM shop s2");
		sql.append("   WHERE 1 = 1 ");

		if (status != null) {
			sql.append(" and s2.status = ? ");
			params.add(status.getValue());
		} else {
			sql.append("   and s2.status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}

		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append("   AND UPPER(s2.shop_code) LIKE ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and lower(s2.NAME_TEXT) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(shopName.toLowerCase())).trim());
		}
		if (staffShopId != null) {
			sql.append(" and s2.shop_id = ?");
			params.add(staffShopId);
		}
		if (programObjectType != null && programId != null) {
			switch (programObjectType) {
			case FOCUS_PROGRAM:
				//				sql.append("   AND s2.shop_id NOT IN");
				//				sql.append("     ( SELECT sh.shop_id from shop sh start with shop_id in (SELECT fsm.shop_id");
				//				sql.append("     FROM focus_shop_map fsm");
				//				sql.append("     WHERE fsm.focus_program_id = ?");
				//				params.add(programId);
				//				sql.append("     AND fsm.status            != ?");
				//				params.add(ActiveType.DELETED.getValue());
				//				sql.append("     ) connect by prior shop_id = parent_shop_id )");
				break;
			case PROMOTION_PROGRAM:
				//				sql.append("   AND s2.shop_id NOT IN");
				//				sql.append("     ( SELECT sh.shop_id from shop sh start with shop_id in (SELECT fsm.shop_id");
				//				sql.append("     FROM promotion_shop_map fsm");
				//				sql.append("     WHERE fsm.promotion_program_id = ?");
				//				params.add(programId);
				//				sql.append("     AND fsm.status            != ?");
				//				params.add(ActiveType.DELETED.getValue());
				//				sql.append("     ) connect by prior shop_id = parent_shop_id )");
				break;
			case INCENTIVE_PROGRAM:
				//				sql.append("   AND s2.shop_id NOT IN");
				//				sql.append("     ( SELECT sh.shop_id from shop sh start with shop_id in (SELECT fsm.shop_id");
				//				sql.append("     FROM incentive_shop_map fsm");
				//				sql.append("     WHERE fsm.incentive_program_id = ?");
				//				params.add(programId);
				//				sql.append("     AND fsm.status            = ?");
				//				params.add(ActiveType.RUNNING.getValue());
				//				sql.append("     ) connect by prior shop_id = parent_shop_id )");
				break;
			case DISPLAY_PROGRAM:
				//				sql.append("   AND s2.shop_id not IN");
				//				sql.append("     ( SELECT sh.shop_id from shop sh start with shop_id in (SELECT fsm.shop_id");
				//				sql.append("     FROM display_shop_map fsm");
				//				sql.append("     WHERE fsm.display_program_id = ?");
				//				params.add(programId);
				//				sql.append("     AND fsm.status            = ?");
				//				params.add(ActiveType.RUNNING.getValue());
				//				sql.append("     ) connect by prior shop_id = parent_shop_id )");
				break;
			default:
				break;
			}
		}
		sql.append("     START WITH s2.shop_id       = s1.shop_id");
		sql.append("     CONNECT BY prior s2.shop_id = s2.parent_shop_id");
		sql.append("   )");

		if (Boolean.TRUE.equals(isGetChildOnly)) {
			sql.append(" and (s1.parent_shop_id = ? or s1.shop_id = ?)");
			params.add(shopId);
			params.add(shopId);
			if (!StringUtility.isNullOrEmpty(shopCode)) {
				sql.append("   AND UPPER(s1.shop_code) LIKE ?");
				params.add(StringUtility.toOracleSearchLikeSuffix(shopCode).toUpperCase());
			}
			if (!StringUtility.isNullOrEmpty(shopName)) {
				sql.append(" and upper(s1.shop_name) like ? ESCAPE '/' ");
				params.add(StringUtility.toOracleSearchLike(shopName.toUpperCase()));
			}
		} else {
			sql.append("   START WITH s1.shop_id       = ?");
			params.add(shopId);
			sql.append("   CONNECT BY prior s1.shop_id = s1.parent_shop_id");
		}

		if (lstExceptionalShopId != null) {
			for (Long exceptionalShopId : lstExceptionalShopId) {
				if (exceptionalShopId != null) {
					sql.append(" AND s1.shop_id != ?");
					params.add(exceptionalShopId);
				}
			}
		}
		if (Boolean.TRUE.equals(isOrderByName))
			sql.append(" order by NLSSORT(s1.shop_name,'NLS_SORT=vietnamese')");
		else
			sql.append(" order by s1.shop_code");
		/* System.out.println(TestUtil.addParamToQuery(sql.toString(), params)); */
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public Shop createShop(Shop shop, LogInfoVO logInfo) throws DataAccessException {
		if (shop == null) {
			throw new IllegalArgumentException("shop");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (shop.getShopCode() != null) {
			shop.setShopCode(shop.getShopCode().toUpperCase());
		}
		shop.setCreateUser(logInfo.getStaffCode());
		shop.setCreateDate(commonDAO.getSysDate());
		//shop.setUpdateDate(commonDAO.getSysDate());
		if (shop.getShopName() != null) {
			shop.setNameText(Unicode2English.codau2khongdau(shop.getShopName()).toUpperCase());
		}
		repo.create(shop);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, shop, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(Shop.class, shop.getId());
	}

	@Override
	public void deleteShop(Shop shop, LogInfoVO logInfo) throws DataAccessException {
		if (shop == null) {
			throw new IllegalArgumentException("shop");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(shop);
		try {
			actionGeneralLogDAO.createActionGeneralLog(shop, null, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Shop getShopById(long id) throws DataAccessException {
		return repo.getEntityById(Shop.class, id);
	}

	@Override
	public void updateShop(Shop shop, LogInfoVO logInfo) throws DataAccessException {
		if (shop == null) {
			throw new IllegalArgumentException("shop");
		}
		if (shop.getShopCode() != null)
			shop.setShopCode(shop.getShopCode().toUpperCase());
		Shop temp = this.getShopById(shop.getId());
		shop.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null) {
			shop.setUpdateUser(logInfo.getStaffCode());
		}
		if (shop.getShopName() != null) {
			shop.setNameText(Unicode2English.codau2khongdau(shop.getShopName()).toUpperCase());
		}
		repo.update(shop);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, shop, logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Shop getShopByCode(String code) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(code)) {
			throw new IllegalArgumentException("shop");
		}
		code = code.toUpperCase();
		String sql = "select * from SHOP where UPPER(shop_code)=? and status!=? ";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		Shop rs = repo.getEntityBySQL(Shop.class, sql, params);
		return rs;
	}

	@Override
	public Shop getShopByIdAndIslevel(Long id, Integer isLevel) throws DataAccessException {
		if (isLevel != null) {
			throw new IllegalArgumentException("isLevel is null");
		}
		List<Object> params = new ArrayList<Object>();
		String sql = "select * from shop where 1 = 1 and decode_islevel_vnm((select cn.object_type from channel_type cn where cn.status = 1 and cn.channel_type_id = shop_type_id)) = ? ";
		params.add(isLevel);
		if (id != null) {
			sql = sql + " start with shop_id = ? connect by shop_id = prior parent_shop_id ";
			params.add(id);
		}
		Shop rs = repo.getEntityBySQL(Shop.class, sql, params);
		return rs;
	}

	@Override
	public Shop getShopByIdChannelIslevel(Long id, Integer shopChannel, Integer isLevel) throws DataAccessException {
		if (isLevel == null) {
			throw new IllegalArgumentException("isLevel is null");
		}
		List<Object> params = new ArrayList<Object>();
		String sql = "select * from shop where 1 = 1 and decode_islevel_vnm((select cn.object_type from channel_type cn where cn.status = 1 and cn.channel_type_id = shop_type_id)) = ? ";
		params.add(isLevel);
		if (shopChannel != null) {
			sql = sql + " and shop_channel = ? ";
			params.add(shopChannel);
		}
		if (id != null) {
			sql = sql + " start with shop_id = ? connect by shop_id = prior parent_shop_id ";
			params.add(id);
		}
		Shop rs = repo.getEntityBySQL(Shop.class, sql, params);
		return rs;
	}

	/**
	 * Lay shop thep shopchannel
	 * 
	 * @author hunglm16
	 * @since October 20,2014
	 * @description chi ap dung cho dot thang 10
	 * */
	@Override
	public Shop getShopByShopChannelWithObjectType(Integer shopChannel, Integer objectType) throws DataAccessException {
		if (shopChannel == null || objectType == null) {
			throw new IllegalArgumentException("objectType is null Or shopChannel is null");
		}
		if (!shopChannel.equals(0) && !shopChannel.equals(1) && !shopChannel.equals(2) && !shopChannel.equals(3)) {
			throw new IllegalArgumentException("shopChannel not in (1,2,3)");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("with rcom as ( ");
		sql.append(" select shop_id from shop sh1 join channel_type cn1 on sh1.shop_type_id = cn1.channel_type_id where sh1.status = 1 and cn1.status = 1 and cn1.type = 1 ");
		sql.append("  and cn1.object_type = ?) ");
		params.add(ShopObjectType.VNM.getValue());
		sql.append(" select * from shop sh join channel_type cn on sh.shop_type_id = cn.channel_type_id ");
		sql.append(" where sh.status = 1 and cn.status = 1 and cn.type = 1 ");
		sql.append(" and cn.object_type = ?  ");
		params.add(objectType);
		if (!objectType.equals(ShopObjectType.VNM.getValue())) {
			sql.append(" and sh.parent_shop_id in (SELECT * FROM rcom) ");
		}
		Shop rs = repo.getEntityBySQL(Shop.class, sql.toString(), params);
		return rs;
	}

	@Override
	public List<ShopVO> getListShopVOBasicByFilter(BasicFilter<ShopVO> filter) throws DataAccessException { 
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" select  ");
		sql.append(" s.shop_id as shopId, ");
		sql.append(" s.shop_code as shopCode, ");
		sql.append(" s.shop_name as shopName, ");
		sql.append(" s.status as status, ");
		sql.append(" s.parent_shop_id as parentId ");
		fromSql.append(" from shop s ");
		if (filter != null && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			fromSql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s.shop_id = ogr.number_key_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		fromSql.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			fromSql.append(" and s.shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			fromSql.append(" and lower(s.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getName().trim().toLowerCase())));
		}
		if (filter.getStatus() != null) {
			fromSql.append(" and s.status = ? ");
			params.add(filter.getStatus());
		} else {
			fromSql.append(" and s.status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getShopType() != null) {
			fromSql.append(" and s.shop_type_id in (select shop_type_id from shop_type where status = 1 and specific_type = ?) ");
			params.add(filter.getShopType().getValue());
		}
		if (filter.getLongG() != null) {
			fromSql.append(" and s.shop_id in (select s1.shop_id from shop s1 where s1.status != -1 start with s1.shop_id = ?  connect by prior s1.shop_id = s1.parent_shop_id) ");
			params.add(filter.getLongG());
		}
		sql.append(fromSql.toString());
		sql.append(" order by s.shop_code ");

		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());
		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "status", "parentId" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG };
		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());

	}
	
	@Override
	public List<ShopVO> getListShopVOBasicByFilter_GTMT(BasicFilter<ShopVO> filter) throws DataAccessException { 
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder fromSql = new StringBuilder();
		
		sql.append(" select  ");
		sql.append(" s.shop_id as shopId, ");
		sql.append(" s.shop_code as shopCode, ");
		sql.append(" s.shop_name as shopName, ");
		sql.append(" s.status as status, ");
		sql.append(" s.parent_shop_id as parentId ");
		fromSql.append(" from shop s ");
		if (filter != null && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			fromSql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s.shop_id = ogr.number_key_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		fromSql.append(" where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			fromSql.append(" and s.shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			fromSql.append(" and lower(s.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getName().trim().toLowerCase())));
		}
		if (filter.getStatus() != null) {
			fromSql.append(" and s.status = ? ");
			params.add(filter.getStatus());
		} else {
			fromSql.append(" and s.status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
//		if (filter.getShopType() != null) {
//			fromSql.append(" and s.shop_type_id in (select shop_type_id from shop_type where status = 1 and specific_type = ?) ");
//			params.add(filter.getShopType().getValue());
//		}
		if (filter.getIntFlag() != null && ShopDecentralizationSTT.NPP.getValue().equals(filter.getIntFlag())) {
			fromSql.append(" and s.shop_type_id in (select shop_type_id from shop_type where status = 1 and (specific_type = ? or specific_type = ?)) ");
			params.add(ShopSpecificType.NPP.getValue());
			params.add(ShopSpecificType.NPP_MT.getValue());
//			params.add(filter.getShopType().getValue());
//			fromSql.append(" and shop_type_id in (select channel_type_id as shop_type_id from channel_type where status = 1 and type = ? and (object_type =? or object_type =? or object_type =? ");
//			params.add(ChannelTypeType.SHOP.getValue());
//			params.add(ShopObjectType.NPP.getValue());
//			params.add(ShopObjectType.NPP_KA.getValue());
//			params.add(ShopObjectType.MIEN_ST.getValue());
//			if (filter.getIsUnionShopRoot() != null && filter.getIsUnionShopRoot()) {
//				fromSql.append(" or object_type = ? ");
//				params.add(ShopObjectType.VNM.getValue());
//			}
//			fromSql.append(" )) ");
		}
		
		if (filter.getLongG() != null) {
			fromSql.append(" and s.shop_id in (select s1.shop_id from shop s1 where s1.status != -1 start with s1.shop_id = ?  connect by prior s1.shop_id = s1.parent_shop_id) ");
			params.add(filter.getLongG());
		}
		sql.append(fromSql.toString());
		sql.append(" order by s.shop_code ");
		
		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());
		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "status", "parentId" };
		
		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.LONG };
		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}

	@Override
	public Shop getShopRunningByCode(String code) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(code)) {
			throw new IllegalArgumentException("shop");
		}
		code = code.toUpperCase();
		String sql = "select * from SHOP where shop_code=? and status=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.RUNNING.getValue());
		Shop rs = repo.getEntityBySQL(Shop.class, sql, params);
		return rs;
	}

	@Override
	public Shop getShopByParentIdNull() throws DataAccessException {
		String sql = "select * from SHOP where parent_shop_id is null and status = 1";
		List<Object> params = new ArrayList<Object>();
		Shop rs = repo.getFirstBySQL(Shop.class, sql, params);
		return rs;
	}

	@Override
	public Shop getShopRoot(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from shop ");
		sql.append(" where parent_shop_id is null ");
		sql.append(" start with shop_id = ? ");
		params.add(shopId);
		sql.append(" connect by prior parent_shop_id = shop_id ");
		return repo.getEntityBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<ShopVO> getListShopVOByFilter(BasicFilter<ShopVO> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();

		sql.append(" select s.shop_id as shopId, s.shop_code as shopCode, s.shop_name as shopName, s.status as status,  ");
		sql.append(" decode_islevel_vnm((select cn.object_type from channel_type cn where cn.status = 1 and cn.channel_type_id = s.shop_type_id)) as isLevel  ");
		sql.append(" from shop s  ");
		fromSql.append(" where 1 = 1  ");
		//Dieu kien lay theo danh sach shop_id
		if (filter.getArrlongG() != null && !filter.getArrlongG().isEmpty()) {
			String paramFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getArrlongG(), "s.shop_id");
			fromSql.append(paramFix);
		}
		//Dieu kien tinh trang
		if (filter.getStatus() != null) {
			fromSql.append(" and s.status = ? ");
			params.add(filter.getStatus());
		} else {
			fromSql.append(" and s.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		//Dieu kien lay theo loai Don vi
		if (filter.getObjectType() != null) {
			fromSql.append(" and s.shop_type_id in (select cn.channel_type_id as shop_type_id from channel_type cn where cn.type = ? and cn.object_type = ?) ");
			params.add(ChannelTypeType.SHOP.getValue());
			params.add(filter.getObjectType());
		}
		///Them tiep cac dieu kien cho phu hop voi tung chuc nang
		//Append Chuoi From and Where
		sql.append(fromSql.toString());
		if (filter.getIntFlag() != null && filter.getIntFlag().equals(0)) {
			sql.append(" order by s.shop_id ");
		} else if (filter.getIntFlag() != null && filter.getIntFlag().equals(1)) {
			sql.append(" order by s.shop_code ");
		} else if (filter.getIntFlag() != null && filter.getIntFlag().equals(2)) {
			sql.append(" order by isLevel, shopCode ");
		}

		//Count total
		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());
		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "status", "isLevel" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		if (filter.getkPaging() == null)
			return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(ShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}

	@Override
	public List<Shop> getListShopNotInPoGroup(ShopFilter filter, long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select sh.* ");
		sql.append(" from shop sh join channel_type ct on sh.shop_type_id = ct.channel_type_id");
		sql.append(" where 1 = 1 ");
		sql.append("     and not exists (select 1 ");
		//sql.append("         from po_auto_group_shop_map ");
		sql.append("         from po_auto_group_shop_map ");
		sql.append("         where 1 = 1 ");
		sql.append("             and status = ? ");
		params.add(ActiveType.RUNNING.getValue());

		sql.append("             and sh.shop_id = shop_id) ");

		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("     and sh.shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode()).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append("     and upper(sh.name_text) like ? ESCAPE '/' ");
			String shopNameText = Unicode2English.codau2khongdau(filter.getShopName());
			params.add(StringUtility.toOracleSearchLike(shopNameText).toUpperCase());
		}

		sql.append("     and sh.status = ? and ct.status = sh.status ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("     and ct.type = 1 and ct.object_type = 3 ");
		sql.append("  start with sh.shop_id = ? connect by prior sh.shop_id = sh.parent_shop_id");
		params.add(shopId);
		sql.append("     order by sh.shop_code asc ");
		if (filter.getkPaging() == null)
			return repo.getListBySQL(Shop.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Shop.class, sql.toString(), params, filter.getkPaging());
	}

	@Override
	public List<Shop> getListShopByFilter(BasicFilter<Shop> filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select *");
		sql.append(" from shop s  ");
		fromSql.append(" where 1 = 1  ");
		//Dieu kien lay theo danh sach shop_id
		if (filter.getArrlongG() != null && !filter.getArrlongG().isEmpty()) {
			fromSql.append(" and s.shop_id in (-1 ");
			for (Long shopId : filter.getArrlongG()) {
				fromSql.append(" ,? ");
				params.add(shopId);
			}
			fromSql.append(" ) ");
		}
		//Dieu kien tinh trang
		if (filter.getStatus() != null) {
			fromSql.append(" and s.status = ? ");
			params.add(filter.getStatus());
		} else {
			fromSql.append(" and s.status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		//Dieu kien lay theo loai Don vi
		if (filter.getObjectType() != null) {
			fromSql.append(" and s.shop_type_id in (select cn.channel_type_id as shop_type_id from channel_type cn where cn.type = ? and cn.object_type = ?) ");
			params.add(ChannelTypeType.SHOP.getValue());
			params.add(filter.getObjectType());
		}
		///Them tiep cac dieu kien cho phu hop voi tung chuc nang

		sql.append(fromSql.toString());

		//Append Chuoi From and Where
		if (filter.getIntFlag() != null && filter.getIntFlag().equals(0)) {
			fromSql.append(" order by s.shop_id ");
		} else if (filter.getIntFlag() != null && filter.getIntFlag().equals(1)) {
			fromSql.append(" order by s.shop_code ");
		} else if (filter.getIntFlag() != null && filter.getIntFlag().equals(2)) {
			fromSql.append(" order by decode_islevel_vnm((select cn.object_type from channel_type cn where cn.status = 1 and cn.channel_type_id = s.shop_type_id)) asc  ");
		}
		//Count total
		countSql.append(" select count(1) count ");
		countSql.append(fromSql.toString());

		if (filter.getkPaging() == null) {
			return repo.getListBySQL(Shop.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(Shop.class, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
	}

	@Override
	public List<Shop> getAllShopChildrenByLstId(List<Long> lstId) throws DataAccessException {
		if (lstId == null || lstId.isEmpty()) {
			throw new IllegalArgumentException("lstId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * ");
		sql.append(" FROM shop WHERE status = 1 ");
		sql.append(" START WITH shop_id       in(-1 ");
		for (Long parentId : lstId) {
			sql.append(" ,?");
			params.add(parentId);
		}
		sql.append(" ) ");
		sql.append(" CONNECT BY prior shop_id = parent_shop_id");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<Shop> getListChildShopByFilterEx(BasicFilter<ShopVO> filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null");
		}
		if (filter.getId() == null) {
			throw new IllegalArgumentException("rootId is null");
		}
		if (filter.getUserId() == null) {
			throw new IllegalArgumentException("getUserId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with r1 as ( SELECT DISTINCT s1.shop_id ");
		sql.append(" 		  FROM shop s1 ");
		sql.append(" 		  WHERE 1         = 1 ");
		sql.append(" 		  AND s1.status   = 1 ");
		sql.append(" 		  AND s1.shop_id IN (SELECT org.shop_id  FROM org_access org WHERE org.status = 1 AND org.permission_id IN ( ");
		sql.append(" 		     SELECT prm.permission_id FROM role_permission_map prm WHERE prm.status = 1 AND prm.role_id IN ( ");
		sql.append(" 		              SELECT ru.role_id FROM role_user ru WHERE ru.status = 1  ");
		sql.append(" 		              AND ru.user_id = ?  ");
		params.add(filter.getUserId());
		if (filter.getLongG() != null) {
			sql.append(" 		              AND ru.role_id = ? ");
			params.add(filter.getLongG());
		}
		sql.append(" 		      ))) ");
		sql.append(" 		  AND s1.shop_id IN ( SELECT DISTINCT s2.shop_id ");
		sql.append(" 		    FROM shop s2 ");
		sql.append(" 		    WHERE 1                       = 1 ");
		sql.append(" 		    AND s2.status                 = 1 ");
		sql.append(" 		      START WITH s2.shop_id       = ? CONNECT BY prior s2.shop_id = s2.parent_shop_id  ");
		params.add(filter.getId());
		sql.append(" 		   )), ");
		sql.append(" 		r2 as ( SELECT DISTINCT s.shop_id FROM shop s WHERE 1 =1 AND s.status = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and lower(s.shop_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getCode().trim().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			sql.append(" and lower(s.shop_name) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getName().trim().toLowerCase()));
		}
		sql.append("		START WITH s.shop_id IN (select shop_id from r1) CONNECT BY prior s.shop_id =  s.parent_shop_id ) ");
		sql.append("SELECT * ");
		sql.append("FROM((SELECT * FROM SHOP s WHERE s.shop_id IN (SELECT shop_id FROM r2)) ");
		sql.append("UNION (SELECT * FROM SHOP s START WITH s.shop_id IN (SELECT shop_id FROM r2) CONNECT BY s.shop_id = prior s.parent_shop_id)) ");
		sql.append("order by shop_code");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<ShopVO> getListChildShopVOByFilter(BasicFilter<ShopVO> filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with r1 as ( SELECT DISTINCT s1.shop_id ");
		sql.append(" 		  FROM shop s1 ");
		sql.append(" 		  WHERE 1         = 1 ");
		sql.append(" 		  AND s1.status   = 1 ");
		if (filter.getUserId() != null) {
			sql.append(" 		  AND s1.shop_id IN (SELECT org.shop_id  FROM org_access org WHERE org.status = 1 AND org.permission_id IN ( ");
			sql.append(" 		     SELECT prm.permission_id FROM role_permission_map prm WHERE prm.status = 1 AND prm.role_id IN ( ");
			sql.append(" 		              SELECT ru.role_id FROM role_user ru WHERE ru.status = 1  ");
			sql.append(" 		              AND ru.user_id = ?  ");
			params.add(filter.getUserId());
			if (filter.getLongG() != null) {
				sql.append(" 		              AND ru.role_id = ? ");
				params.add(filter.getLongG());
			}
			sql.append(" 		      ))) ");
		}
		sql.append(" 		  AND s1.shop_id IN ( SELECT DISTINCT s2.shop_id ");
		sql.append(" 		    FROM shop s2 ");
		sql.append(" 		    WHERE 1                       = 1 ");
		sql.append(" 		    AND s2.status                 = 1 ");
		sql.append(" 		      START WITH s2.shop_id       = ? CONNECT BY prior s2.shop_id = s2.parent_shop_id  ");
		params.add(filter.getId());
		sql.append(" 		   )), ");
		sql.append(" 		r2 as ( SELECT DISTINCT s.shop_id FROM shop s WHERE 1 =1 AND s.status = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and s.shop_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			/** lay theo name text*/
			sql.append(" and lower(s.name_text) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getName().trim()).toLowerCase()));
		}
		sql.append("		START WITH s.shop_id IN (select shop_id from r1) CONNECT BY prior s.shop_id =  s.parent_shop_id ) ");
		sql.append(" select shop_id as shopId, shop_code as shopCode, shop_name as shopName, parent_shop_id as parentId, ((select  cn.node_ordinal from organization cn where cn.organization_id = r.organization_id)) as isLevel ");
		sql.append(" from( ");
		sql.append("		(SELECT shop_id, shop_code, shop_name, parent_shop_id, organization_id FROM SHOP s where s.shop_id IN (select shop_id from r2))  ");
		sql.append("		union (SELECT shop_id, shop_code, shop_name, parent_shop_id, organization_id FROM SHOP s ");
		sql.append("		START WITH s.shop_id IN (select shop_id from r2) ");
		sql.append("		CONNECT BY s.shop_id = prior s.parent_shop_id)) r ");
		if (StringUtility.isNullOrEmpty(filter.getStrShopId()) && filter.getUserId() != null && filter.getRoleId() != null && filter.getId() != null) {
			sql.append(" join table (f_get_list_child_shop_inherit(?, sysdate, ?, ?)) tmp on tmp.number_key_id = r.shop_id ");
			params.add(filter.getUserId());
			params.add(filter.getRoleId());
			params.add(filter.getId());
		}
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getStrShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrShopId(), "shop_id");
			sql.append(paramsFix);
		}
		sql.append("		ORDER BY isLevel, shopCode ASC ");

		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "parentId", "isLevel" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<ShopVO> getListChildShopVOByShopRoot(Long shopRootId, Integer status) throws DataAccessException {
		if (shopRootId == null) {
			throw new IllegalArgumentException("shopRootId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select shop_id as shopId, shop_code as shopCode, shop_name as shopName, parent_shop_id as parentId, decode_islevel_vnm(cn.object_type) as isLevel");
		sql.append("  FROM SHOP s join channel_type cn on cn.channel_type_id = shop_type_id ");
		if (status != null) {
			sql.append("	and s.status = ?  ");
			params.add(status);
		} else {
			sql.append("	and s.status in (0, 1)  ");
		}
		sql.append("	and cn.status = 1 ");
		sql.append("	START WITH s.shop_id = ? CONNECT BY prior s.shop_id =  s.parent_shop_id ");
		params.add(shopRootId);
		sql.append(" ORDER BY isLevel, shopCode ASC");

		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "parentId", "isLevel" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<ShopVO> searchListChildShopVOByFilter(BasicFilter<ShopVO> filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("filter is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with r1 as ( SELECT DISTINCT s1.shop_id ");
		sql.append(" 		  FROM shop s1 ");
		sql.append(" 		  WHERE 1         = 1 ");
		sql.append(" 		  AND s1.status   = 1 ");
		sql.append(" 		  AND s1.shop_id IN ( SELECT DISTINCT s2.shop_id ");
		sql.append(" 		    FROM shop s2 ");
		sql.append(" 		    WHERE 1                       = 1 ");
		sql.append(" 		    AND s2.status                 = 1 ");
		sql.append(" 		      START WITH s2.shop_id       = ? CONNECT BY prior s2.shop_id = s2.parent_shop_id  ");
		params.add(filter.getId());
		sql.append(" 		   )), ");
		sql.append(" 		r2 as ( SELECT DISTINCT s.shop_id FROM shop s WHERE 1 =1 AND s.status = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			sql.append(" and s.shop_code like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			/** lay theo name text */
			sql.append(" and lower(s.name_text) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getName().trim()).toLowerCase()));
		}
		sql.append("		START WITH s.shop_id IN (select shop_id from r1) CONNECT BY prior s.shop_id =  s.parent_shop_id ) ");
		sql.append("		select shop_id as shopId, shop_code as shopCode, shop_name as shopName, parent_shop_id as parentId, ");
		sql.append("		(select  cn.node_ordinal from organization cn where cn.status = 1 and cn.node_type = 1 and cn.organization_id = s.organization_id and rownum=1) as isLevel ");
		sql.append("		from ((select shop_id, shop_code, shop_name, parent_shop_id, shop_type_id, organization_id  from shop s where s.shop_id in (select shop_id from r2))  ");
		sql.append("				union ");
		sql.append("				(select shop_id, shop_code, shop_name, parent_shop_id, shop_type_id, organization_id from shop s ");
		sql.append("				START WITH s.shop_id IN (select shop_id from r2) ");
		sql.append("				CONNECT BY s.shop_id = prior s.parent_shop_id)) s ");
		if (StringUtility.isNullOrEmpty(filter.getStrShopId()) && filter.getUserId() != null && filter.getRoleId() != null && filter.getId() != null) {
			sql.append(" join table (f_get_list_child_shop_inherit(?, sysdate, ?, ?)) tmp on tmp.number_key_id = s.shop_id ");
			params.add(filter.getUserId());
			params.add(filter.getRoleId());
			params.add(filter.getId());
		}
		sql.append("		WHERE 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(filter.getStrShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrShopId(), "shop_id");
			sql.append(paramsFix);

		}
		sql.append("		ORDER BY isLevel, shopCode ASC ");

		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "parentId", "isLevel" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public ShopVO getShopVOByShopId(Long shopId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		/*sql.append(" SELECT shop_id AS shopId, shop_code AS shopCode, shop_name AS shopName, parent_shop_id AS parentId, decode_islevel_vnm(cn.object_type) AS isLevel ");
		sql.append(" FROM shop sh join channel_type cn on  cn.channel_type_id = shop_type_id ");
		sql.append(" where cn.status = 1 and shop_id = ? ");*/
		sql.append(" SELECT shop_id AS shopId, ");
		sql.append(" shop_code AS shopCode, ");
		sql.append(" shop_name AS shopName, ");
		sql.append(" parent_shop_id as parentId, ");
		sql.append(" st.specific_type AS isLevel ");
		sql.append(" FROM shop sh ");
		sql.append(" join shop_type st on st.shop_type_id = sh.shop_type_id ");
		sql.append(" WHERE st.status = 1 ");
		sql.append(" and shop_id = ? ");
		params.add(shopId);
		final String[] fieldNames = new String[] { "shopId", "shopCode", "shopName", "parentId", "isLevel" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER };
		List<ShopVO> lstData = repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
		if (lstData != null && !lstData.isEmpty()) {
			return lstData.get(0);
		}
		return null;
	}

	@Override
	public List<Shop> getListSubShop(Long parentId, String shopCode, String shopName, ActiveType status, Long programId, ProgramObjectType programObjectType, Long exceptionalShopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (parentId != null) {
			sql.append("select * from SHOP s where s.parent_shop_id=?");
			params.add(parentId);
			if (status != null) {
				sql.append(" and s.status = ?");
				params.add(status.getValue());
			} else {
				sql.append(" and s.status != ?");
				params.add(ActiveType.DELETED.getValue());
			}
		} else {
			sql.append("select * from SHOP s where s.parent_shop_id is null and s.status !=?");
			params.add(ActiveType.DELETED.getValue());
		}

		if (exceptionalShopId != null) {
			sql.append(" and s.shop_id != ?");
			params.add(exceptionalShopId);
		}

		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and s.shop_code like ?");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and upper(s.shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shopName.toUpperCase()));
		}

		if (programObjectType != null && programId != null) {
			switch (programObjectType) {
			case FOCUS_PROGRAM:
				sql.append("   AND s.shop_id NOT IN");
				sql.append("     ( SELECT sh.shop_id from shop sh start with shop_id in (SELECT fsm.shop_id");
				sql.append("     FROM focus_shop_map fsm");
				sql.append("     WHERE fsm.focus_program_id = ?");
				params.add(programId);
				sql.append("     AND fsm.status            != ?");
				params.add(ActiveType.DELETED.getValue());
				sql.append("     ) connect by prior shop_id = parent_shop_id )");
				break;
			case PROMOTION_PROGRAM:
				sql.append("   AND s.shop_id NOT IN");
				sql.append("     ( SELECT sh.shop_id from shop sh start with shop_id in (SELECT fsm.shop_id");
				sql.append("     FROM promotion_shop_map fsm");
				sql.append("     WHERE fsm.promotion_program_id = ?");
				params.add(programId);
				sql.append("     AND fsm.status            != ?");
				params.add(ActiveType.DELETED.getValue());
				sql.append("     ) connect by prior shop_id = parent_shop_id )");
				break;
			case DISPLAY_PROGRAM:
				sql.append("   AND s.shop_id NOT IN");
				sql.append("     ( SELECT sh.shop_id from shop sh start with shop_id in (SELECT fsm.shop_id");
				sql.append("     FROM display_shop_map fsm");
				sql.append("     WHERE fsm.display_program_id = ?");
				params.add(programId);
				sql.append("     AND fsm.status            != ?");
				params.add(ActiveType.DELETED.getValue());
				sql.append("     ) connect by prior shop_id = parent_shop_id )");
				break;
			case INCENTIVE_PROGRAM:
				sql.append("   AND s.shop_id NOT IN");
				sql.append("     ( SELECT sh.shop_id from shop sh start with shop_id in (SELECT fsm.shop_id");
				sql.append("     FROM incentive_shop_map fsm");
				sql.append("     WHERE fsm.incentive_program_id = ?");
				params.add(programId);
				sql.append("     AND fsm.status            != ?");
				params.add(ActiveType.DELETED.getValue());
				sql.append("     ) connect by prior shop_id = parent_shop_id )");
				break;
			default:
				break;
			}
		}
		sql.append(" order by SHOP_CODE");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public Object getDMDHT(Long shopId) throws DataAccessException {
		String sql = "select code from shop_param where type='HMDHT' and shop_id=? and status=1";
		List<Object> params = new ArrayList<Object>();
		params.add(shopId);

		Object obj = repo.getObjectByQuery(sql, params);
		if (obj != null) {
			return obj;
		}
		return 0.0;
	}

	@Override
	public List<Shop> getListShop(ShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.* from shop s ");
		if (filter != null && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) tmp on s.shop_id = tmp.number_key_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
		}
		sql.append(" join organization org on org.organization_id = s.organization_id and org.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" join shop_type st on s.SHOP_TYPE_ID = st.SHOP_TYPE_ID and st.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" where 1 = 1 ");
		if (filter.getShopId() != null) {
			sql.append(" and shop_id = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getSpecType() != null) {
			sql.append(" and st.SPECIFIC_TYPE = ? ");
			params.add(filter.getSpecType().getValue());
		}
		if (Boolean.TRUE.equals(filter.getIsGetOneChildLevel())) {
			sql.append(" and s.shop_id in (select shop_id from shop   ");
			sql.append(" where parent_shop_id in (-1");
			if (filter.getLstParentId() != null) {
				for (Long id : filter.getLstParentId()) {
					sql.append(",?");
					params.add(id);
				}
			}
			sql.append(" )) ");
		}
		if (filter.getStatus() != null) {
			sql.append(" and s.status=? ");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and s.status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append("     and s.shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getShopCode()).toUpperCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append("     and upper(s.shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getShopName()).toUpperCase());
		}
		if (Boolean.TRUE.equals(filter.getIsShopTypeId())) {
			sql.append(" order by org.node_ordinal,s.shop_name ");
		} else {
			sql.append(" order by s.shop_code,s.shop_name ");
		}
		if (filter.getkPaging() != null) {
			return repo.getListBySQLPaginated(Shop.class, sql.toString(), params, filter.getkPaging());
		}
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	@Override
	public List<Shop> getListChildShop(ShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		Long shopId = filter.getShopId();
		if (shopId == null) {
			shopId = -1l;
		}
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShop as (");
		sql.append(" select s.*, level lv from shop s");
		sql.append(" where 1=1");
		if (filter.getStatus() != null) {
			sql.append(" and status = ? ");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" start with shop_id = ?");
		params.add(shopId);
		sql.append(" connect by prior shop_id = parent_shop_id)");
		sql.append(" select s.* from lstShop s ");
		if (filter != null && StringUtility.isNullOrEmpty(filter.getStrShopId()) && filter.getStaffRootId() != null && filter.getRoleId() != null && filter.getShopRootId() != null) {
			sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) r_ogr on s.shop_id = r_ogr.number_key_id ");
			params.add(filter.getStaffRootId());
			params.add(filter.getRoleId());
			params.add(filter.getShopRootId());
	    }
		sql.append(" join organization org on org.organization_id = s.organization_id and org.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" join shop_type st on s.SHOP_TYPE_ID = st.SHOP_TYPE_ID and st.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" where 1=1 ");
		if (filter.getSpecType() != null) {
			sql.append(" and st.specific_type = ? ");
			params.add(filter.getSpecType().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopCode())) {
			sql.append(" and upper(s.shop_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(filter.getShopCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getShopName())) {
			sql.append(" and lower(s.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getShopName().trim().toLowerCase())));
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getStrShopId())) {
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(filter.getStrShopId(), "s.shop_id");
			sql.append(paramsFix);

		}
		sql.append(" order by s.lv, s.shop_code ");
		if (filter.getkPaging() != null) {
			return repo.getListBySQLPaginated(Shop.class, sql.toString(), params, filter.getkPaging());
		}
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public Boolean checkIfShopExists(ShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(*) as count from shop where 1=1");
		if (!StringUtility.isNullOrEmpty(filter.getEmail())) {
			sql.append(" and lower(email)=?");
			params.add(filter.getEmail().toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getMobilePhone())) {
			sql.append(" and lower(mobiphone)=?");
			params.add(filter.getMobilePhone().toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getPhone())) {
			sql.append(" and lower(phone)=?");
			params.add(filter.getPhone().toLowerCase());
		}
		if (!StringUtility.isNullOrEmpty(filter.getTaxNum())) {
			sql.append(" and lower(TAX_NUM)=?");
			params.add(filter.getTaxNum().toLowerCase());
		}
		if (filter.getShopId() != null) {
			sql.append(" and shop_id != ?");
			params.add(filter.getShopId());
		}

		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}

	@Override
	public void cancelTrainingPlanOfSaler(long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("update training_plan_detail set status = ?");
		params.add(TrainningPlanDetailStatus.CANCLED.getValue());
		sql.append(" where staff_id in (select staff_id from staff st where shop_id = ? and status = ?");
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and exists (select 1 from channel_type ct where ct.channel_type_id = st.staff_type_id");
		sql.append(" and ct.type = ? and ct.status = ? and ct.object_type in (?, ?)");
		params.add(ChannelTypeType.STAFF.getValue());
		params.add(ActiveType.RUNNING.getValue());
		params.add(StaffObjectType.NVBH.getValue());
		params.add(StaffObjectType.NVVS.getValue());
		sql.append(" )");
		sql.append(" ) and status = 0");

		repo.executeSQLQuery(sql.toString(), params);
	}

	@Override
	public List<Shop> getListShopOfUnitTree(KPaging<Shop> kPaging, Long curShopId, String shopCode, String shopName, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("with lstShopId as (");
		sql.append(" select shop_id from shop");
		sql.append(" where status <> ?");
		params.add(ActiveType.DELETED.getValue());
		sql.append(" start with shop_id = ?");
		params.add(curShopId);
		sql.append(" connect by prior shop_id = parent_shop_id)");
		sql.append("select * from shop s where 1 = 1");
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and upper(s.shop_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLike(shopCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and lower(s.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(shopName.trim().toLowerCase())));
		}
		if (status != null) {
			sql.append(" and s.status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and s.status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" and s.shop_id in (select shop_id from lstShopId)");
		sql.append(" and exists (select 1 from channel_type ct where ct.channel_type_id = s.shop_type_id and ct.status = ? and ct.type = ? and ct.object_type not in (4, 5, 6, 7))");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ChannelTypeType.SHOP.getValue());
		sql.append(" order by s.shop_code, s.shop_name");
		if (kPaging == null) {
			return repo.getListBySQL(Shop.class, sql.toString(), params);
		}

		return repo.getListBySQLPaginated(Shop.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<Shop> getSearchShopTreeDO(Long userId, Long shopID, Long offDocuID, String shopCode, String shopName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("WITH r1 AS ");
		sql.append("( SELECT DISTINCT s1.shop_id ");
		sql.append("FROM shop s1 ");
		sql.append("WHERE 1         = 1 ");
		sql.append("AND s1.status   = ? ");
		params.add(ActiveType.RUNNING.getValue());
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append("AND upper(s1.shop_code) LIKE ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(shopCode).toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append("and upper(s1.SHOP_name) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(shopName).toUpperCase()));
		}
		sql.append("AND s1.shop_id IN ");
		sql.append("(SELECT org.shop_id ");
		sql.append("FROM org_access org ");
		sql.append("WHERE org.status       = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("AND org.permission_id IN ");
		sql.append("(SELECT prm.permission_id ");
		sql.append("FROM role_permission_map prm ");
		sql.append("WHERE prm.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("AND prm.role_id IN ");
		sql.append("( SELECT ru.role_id FROM role_user ru WHERE ru.status = ? AND ru.user_id = ? ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(userId);
		sql.append(") ");
		sql.append(") ");
		sql.append(") ");
		sql.append("AND s1.shop_id IN ");
		sql.append("( SELECT DISTINCT s2.shop_id ");
		sql.append("FROM shop s2 ");
		sql.append("WHERE 1                       = 1 ");
		sql.append("    AND s2.status                 = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("START WITH s2.shop_id       = ? ");
		params.add(shopID);
		sql.append("CONNECT BY prior s2.shop_id = s2.parent_shop_id ");
		sql.append(") ");
		sql.append("), ");
		sql.append("r2 AS ");
		sql.append("( SELECT DISTINCT s.shop_id ");
		sql.append("FROM shop s ");
		sql.append("WHERE 1                 = 1 ");
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append("AND upper(s.shop_name) LIKE ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(shopCode).toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append("and upper(s.SHOP_CODE) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(shopName).toUpperCase()));
		}
		sql.append("AND s.status            = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("START WITH s.shop_id IN ");
		sql.append("(SELECT shop_id FROM r1 ");
		sql.append(") ");
		sql.append("CONNECT BY prior s.shop_id = s.parent_shop_id ");
		sql.append(") ");
		sql.append("SELECT * ");
		sql.append("FROM ( ");
		sql.append("(SELECT * ");
		sql.append("FROM SHOP s ");
		sql.append("WHERE 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append("AND upper(s.shop_name) LIKE ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(shopCode).toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append("and upper(s.SHOP_CODE) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(shopName).toUpperCase()));
		}
		sql.append("and s.shop_id IN ");
		sql.append("(SELECT shop_id FROM r2 ");
		sql.append(") ");
		sql.append(") ");
		sql.append("UNION ");
		sql.append("(SELECT * ");
		sql.append("FROM SHOP s ");
		sql.append("where 1 = 1 ");
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append("AND upper(s.shop_name) LIKE ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(shopCode).toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append("and upper(s.SHOP_CODE) like ? escape '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(shopName).toUpperCase()));
		}
		sql.append("START WITH s.shop_id IN ");
		sql.append("(SELECT shop_id FROM r2 ");
		sql.append(") ");
		sql.append("CONNECT BY s.shop_id = prior s.parent_shop_id ");
		sql.append("))");

		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<ProgrameExtentVO> getListProgrameExtentVOExOD(List<Shop> lstShop, Long officeDocId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT s.shop_id as shopId, ct.object_type as isNPP ,");
		sql.append(" (");
		sql.append("    SELECT COUNT(1) ");
		sql.append("    FROM shop ss ");
		sql.append("    WHERE EXISTS ");
		sql.append("      (SELECT 1 ");
		sql.append(" FROM office_doc_shop_map dsm ");
		sql.append(" WHERE dsm.shop_id = ss.shop_id ");
		sql.append(" AND dsm.status = 1 ");
		sql.append(" AND office_document_id = ? )");
		params.add(officeDocId);
		sql.append("    START WITH ss.shop_id = s.shop_id CONNECT BY PRIOR ss.shop_id = ss.parent_shop_id");
		sql.append(" ) isJoin, ");
		sql.append(" case when (select count(1) ");
		sql.append(" FROM shop ss ");
		sql.append(" WHERE NOT EXISTS ");
		sql.append(" (SELECT 1 ");
		sql.append(" FROM office_doc_shop_map dsm ");
		sql.append(" WHERE dsm.shop_id      = ss.shop_id ");
		sql.append(" AND dsm.status         = 1 ");
		sql.append(" AND office_document_id = ? ");
		params.add(officeDocId);
		sql.append(" ) AND ss.shop_id != s.shop_id ");
		sql.append(" and ss.status = 1 ");
		sql.append(" START WITH ss.shop_id = s.shop_id ");
		sql.append(" CONNECT BY PRIOR ss.shop_id = ss.parent_shop_id ) > 0 ");
		sql.append(" then 0 ");
		sql.append(" else 1 ");
		sql.append(" end isChildJoin ");
		sql.append(" FROM shop s ");
		sql.append(" join channel_type ct on ct.channel_type_id = s.shop_type_id ");
		sql.append(" WHERE 1=1 ");
		
		String paramFix = StringUtility.getParamsIdFixBugTooLongParamsLstShop(lstShop, "shop_id");
		sql.append(paramFix);

		String[] fieldNames = { "shopId", "isNPP", "isJoin", "isChildJoin" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ProgrameExtentVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	/** BEGIN Don Vi QL cong van **/
	@Override
	public List<Shop> getListChildShopId(Integer status, Long... arrShopId) throws DataAccessException {
		if (arrShopId == null || arrShopId.length == 0) {
			throw new IllegalArgumentException("shopId is null");
		}

		int i = 0, size = arrShopId.length;
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT * ");
		sql.append(" FROM shop ");
		sql.append(" WHERE 1 = 1  ");
		sql.append(" and shop_id not in (? ");
		params.add(arrShopId[0]);
		for (i = 1; i < size; i++) {
			sql.append(" ,? ");
			params.add(arrShopId[i]);
		}
		sql.append(" ) ");
		sql.append(" START WITH parent_shop_id in (? ");
		params.add(arrShopId[0]);
		for (i = 1; i < size; i++) {
			sql.append(" ,? ");
			params.add(arrShopId[i]);
		}
		sql.append(" ) ");
		if (status != null) {
			sql.append(" and status = ?  ");
			params.add(status);
		} else {
			sql.append(" and status <> ?  ");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id");

		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	@Override
	public List<Shop> getListParentShopId(Integer status, Long... arrShopId) throws DataAccessException {
		if (arrShopId == null || arrShopId.length == 0) {
			throw new IllegalArgumentException("shopId is null");
		}
		
		int i = 0, size = arrShopId.length;
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" SELECT * ");
		sql.append(" FROM shop ");
		sql.append(" WHERE 1 = 1  ");
		sql.append(" and shop_id not in (? ");
		params.add(arrShopId[0]);
		for (i = 1; i < size; i++) {
			sql.append(" ,? ");
			params.add(arrShopId[i]);
		}
		sql.append(" ) ");
		sql.append(" START WITH shop_id in (? ");
		params.add(arrShopId[0]);
		for (i = 1; i < size; i++) {
			sql.append(" ,? ");
			params.add(arrShopId[i]);
		}
		sql.append(" ) ");
		if (status != null) {
			sql.append(" and status = ?  ");
			params.add(status);
		} else {
			sql.append(" and status <> ?  ");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" CONNECT BY PRIOR parent_shop_id = shop_id ");
		
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	/** BEGIN Don Vi QL cong van **/
	@Override
	public boolean isChild(Long parent,Long child) throws DataAccessException {
		if (parent == null || child==null || parent==child) {
			return false;
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" SELECT  sum(decode(shop_id,?,1,0)) count ");
		sql.append(" FROM shop ");
		params.add(child);
		sql.append(" START WITH shop_id = ? ");
		params.add(parent);
		sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id");

		return repo.countBySQL( sql.toString(), params)>0;
	}

	@Override
	public Shop getShopArea(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from shop where level = 3 start with shop_id = ? ");
		params.add(shopId);
		sql.append(" connect by prior parent_shop_id = shop_id ");
		return repo.getEntityBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public Shop getShopByCodeSaleMT(String code) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(code)) {
			throw new IllegalArgumentException("shop");
		}
		code = code.toUpperCase();
		String sql = "select s.*  from SHOP s join channel_type ct on s.shop_type_id = ct.channel_type_id where s.shop_code=? and s.status!=? and ct.object_type > 3";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		Shop rs = repo.getEntityBySQL(Shop.class, sql, params);
		return rs;
	}

	@Override
	public Boolean checkAncestor(String parentCode, String shopCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select count(1) as count from shop where shop_code=?");
		params.add(shopCode.toUpperCase());
		sql.append(" start with shop_code=?");
		params.add(parentCode.toUpperCase());

		sql.append(" connect by prior shop_id=parent_shop_id");

		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	@Override
	public List<Shop> getShopByUserSaleMT(Long userId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select s.*  from shop s inner join MT_USER_SHOP us ");
		sql.append(" on s.shop_id = us.shop_id and s.shop_type_id=1929  and us.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and us.user_id=? ");
		params.add(userId);

		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<Shop> getAllShopChild(Long parentId) throws DataAccessException {
		//		List<Shop> rs = new ArrayList<Shop>();
		//		rs.add(this.getShopById(parentId));
		//		List<Shop> children = this.getListSubShop(parentId);
		//		rs.addAll(children);
		//		for (Shop child: children) {
		//			List<Shop> sub = this.getAllShopChild(child.getId());
		//			rs.addAll(sub);
		//		}
		//		return rs;
		if (parentId == null) {
			throw new IllegalArgumentException("parentId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *");
		sql.append(" 	FROM shop");
		sql.append("   START WITH shop_id       = ?");
		params.add(parentId);
		sql.append("   CONNECT BY prior shop_id = parent_shop_id");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<Shop> getListShopExport(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *  ");
		sql.append(" FROM SHOP ");
		sql.append(" START WITH SHOP_ID = ? ");
		params.add(shopId);
		sql.append(" CONNECT BY PRIOR SHOP_ID = PARENT_SHOP_ID ");
		sql.append(" order by SHOP_TYPE_ID ");

		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<Shop> getListShopOfUnitTreeSaleMT(KPaging<Shop> kPaging, String shopCode, String shopName, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from SHOP where 1 = 1");
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and upper(shop_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shopCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and upper(shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shopName.toUpperCase()));
		}
		if (status != null) {
			sql.append(" and status=?");
			params.add(status.getValue());
		} else {
			sql.append(" and status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" AND shop_type_id in (select channel_type_id from channel_type where type = 1 and object_type in (7,6,5,4)) ");
		sql.append(" order by SHOP_CODE asc");
		if (kPaging == null)
			return repo.getListBySQL(Shop.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Shop.class, sql.toString(), params, kPaging);
	}

	@Override
	public int getCountChildByShop(ShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count from shop s ");
		sql.append(" where 1=1 ");
		if (filter.getShopId() != null) {
			sql.append(" and parent_shop_id=?");
			params.add(filter.getShopId());
		}
		int c = repo.countBySQL(sql.toString(), params);
		return c;
	}

	@Override
	public List<Shop> getListShopSaleMTByShopCode(String shopCode, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("with tmp as (");
		sql.append(" SELECT sh.shop_id, ");
		sql.append(" sh.shop_code ");
		sql.append(" FROM shop sh, ");
		sql.append(" CHANNEL_TYPE ct ");
		sql.append(" WHERE sh.SHOP_TYPE_ID      = ct.CHANNEL_TYPE_ID ");
		sql.append(" AND ct.OBJECT_TYPE         in (4,5,6) and ct.status = 1  and ct.type = 1 ");
		sql.append(" START WITH sh.shop_id    = ? ");
		params.add(shopId);
		sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id ");
		sql.append(" ) ");
		sql.append("select s.* from SHOP s join channel_type ct on s.shop_type_id=ct.channel_type_id and ct.type=1 and ct.object_type in (4,5,6) and ct.status = 1 ");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and s.shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}

		sql.append(" and s.status = 1");
		sql.append(" order by s.SHOP_CODE");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<Shop> getListShopSaleMTByShopName(String shopName, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("with tmp as (");
		sql.append(" SELECT sh.shop_id, ");
		sql.append(" sh.shop_code ");
		sql.append(" FROM shop sh, ");
		sql.append(" CHANNEL_TYPE ct ");
		sql.append(" WHERE sh.SHOP_TYPE_ID      = ct.CHANNEL_TYPE_ID ");
		sql.append(" AND ct.OBJECT_TYPE         in (4,5,6) and ct.status = 1 and ct.type = 1 ");
		sql.append(" START WITH sh.shop_id    = ? ");
		params.add(shopId);
		sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id ");
		sql.append(" ) ");
		sql.append("select s.* from SHOP s join channel_type ct on s.shop_type_id=ct.channel_type_id and ct.type=1 and ct.object_type in (4,5,6) and ct.status=1 ");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and s.shop_name like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopName.toUpperCase()));
		}

		sql.append(" and s.status = 1");
		sql.append(" order by s.SHOP_CODE");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<Shop> getListSubShopSaleMTIncludeStopped(Long parentId, java.lang.Boolean oneLevel, java.lang.Boolean includeStopped) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select s.* from SHOP s join channel_type ct on s.shop_type_id=ct.channel_type_id and ct.type=1 and ct.object_type in (4,5,6) and ct.status=1 ");
		sql.append(" where 1=1 ");
		if (Boolean.TRUE.equals(oneLevel)) {
			sql.append(" and s.parent_shop_id=?");
			params.add(parentId);
		} else {
			sql.append(" and shop_id in ( select shop_id from shop where 1 = 1 ");
			if (!includeStopped)
				sql.append(" and status=1 ");
			sql.append(" start with shop_id = ? connect by prior shop_id=parent_shop_id) ");
			params.add(parentId);
		}
		if (!includeStopped)
			sql.append(" and s.status = 1");
		sql.append(" order by s.SHOP_CODE");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<Shop> getListSubShopSaleMT(Long parentId, Boolean oneLevel) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select s.* from SHOP s join channel_type ct on s.shop_type_id=ct.channel_type_id and ct.type=1 and ct.object_type in (4,5,6) and ct.status=1 ");
		sql.append(" where 1=1 ");
		if (Boolean.TRUE.equals(oneLevel)) {
			sql.append(" and s.parent_shop_id=?");
			params.add(parentId);
		} else {
			sql.append(" and shop_id in ( select shop_id from shop where status=1  start with shop_id = ? connect by prior shop_id=parent_shop_id) ");
			params.add(parentId);
		}
		sql.append(" and s.status = 1");
		sql.append(" order by s.SHOP_CODE");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	
	@Override
	public Shop getShopParentWorkingDateConfig(Long id,WorkingDateType type) throws DataAccessException {
		if (id == null) {
			throw new IllegalArgumentException("id is null");
		}
		List<Object> params = new ArrayList<Object>();
		String sql = "select *  from shop where 1 = 1 and rownum <= 1 and shop_id != ?";
		params.add(id);
		sql += " and shop_id in (select shop_id from working_date_config where 1 = 1 and status = 1 and type = ?) ";
		params.add(type.getValue());
		sql += " start with shop_id = ? " ;
		params.add(id);
		sql += " connect by parent_shop_id = shop_id";
		Shop rs = repo.getEntityBySQL(Shop.class, sql, params);
		return rs;
	}
	/*** BEGIN VUONGMQ ****/
	/*@Override
	public List<ShopVO> getListShopParentByShopChildren(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.shop_id as shopId   ");
		sql.append(" from shop s ");
		sql.append(" where s.status = 1 ");
		sql.append(" start with s.shop_id = ? ");
		params.add(shopId);
		sql.append("  connect by prior s.parent_shop_id = s.shop_id ");
		String[] fieldNames = {"shopId"};
		Type[] fieldTypes = {StandardBasicTypes.LONG };
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}*/
	@Override
	public List<Shop> getListShopOrganization(ShopFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select sh.* ");
		sql.append(" from shop sh ");
		sql.append(" where 1 = 1 ");
		if(filter.getStatus() != null){
			sql.append(" and sh.status =? ");
			params.add(filter.getStatus().getValue());
		}
		if(filter.getOrgId() != null){
			sql.append(" and sh.organization_id =? ");
			params.add(filter.getOrgId());
		}
		if (filter.getkPaging() == null)
			return repo.getListBySQL(Shop.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Shop.class, sql.toString(), params, filter.getkPaging());
	}
	/*** END VUONGMQ ****/
	/**
	 * tra ve danh sach don vi de export ra file excel
	 * @param kPaging thong tin phan trang
	 * @param unitFilter tieu chi tim kiem
	 * @author phuocdh2
	 * @return danh sach don vi de export
	 * @since 24 Mar 2015
	 */
	@Override
	public List<ShopVO> findShopVOExport(KPaging<ShopVO> kPaging, UnitFilter unitFilter) throws DataAccessException {
		if (unitFilter == null) {
			throw new IllegalArgumentException("unitFilter must not null");
		}
		if (unitFilter.getStaffRootId() == null || unitFilter.getRoleId() == null || unitFilter.getShopRootId() == null) {
			throw new IllegalArgumentException("priviledge data is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select ");
		sql.append(" 	s.shop_id shopId, ");
		sql.append(" 	s.shop_code shopCode, ");
		sql.append(" 	s.shop_name shopName, ");
		sql.append(" 	s.ABBREVIATION abbreviation, ");
		sql.append(" 	(select o.node_type_name from organization o where o.organization_id = s.organization_id) as orgName, ");
		//sql.append(" 	s.parent_shop_id  as parentShopId, ");
		sql.append(" 	(Select shop_code from shop where shop_id=s.parent_shop_id)  as parentShopCode, "); //longnh15: doi parent_id thanh 
		sql.append(" 	(select shop_name from shop where shop_id = s.parent_shop_id) as parentShopName, ");
		sql.append(" 	s.phone, s.mobiphone,s.fax,s.email,s.tax_num taxNum,s.contact_name contactName,s.bill_to billTo,s.ship_to shipTo, ");
		sql.append(" 	(select area_code from area  where area.area_id = s.area_id) as precinctName, "); //longnh 15: doi precin -> areacode
		sql.append(" 	replace((select province_name from area where area.area_id = s.area_id) || ' - ' || ");
		sql.append(" 	(select district_name from area where area.area_id = s.area_id) || ' - ' ||  ");
		sql.append(" 	(select precinct_name from area where area.area_id = s.area_id),' -  - ','') as areaName,  ");
		sql.append(" 	s.address,  ");
		//sql.append(" 	s.status status, ");
		sql.append(" 	(select name from shop_type where shop_type_id = s.shop_type_id) shopType,  ");
		sql.append(" 	 replace('(' || s.lat || ',' || s.lng || ')','(,)','') as latlng,  ");
		sql.append("	parent_shop_id parentId, ");
		sql.append("	shop_type_id shopTypeId ");
		sql.append(" from shop s ");
		if (ActiveType.RUNNING == unitFilter.getStatus()) {
			sql.append(" join ( ");
		} else {
			sql.append(" left join ( ");
		}
		sql.append("     select number_key_id as shop_id from table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ");
		sql.append(" ) privShop on s.shop_id = privShop.shop_id ");
		params.add(unitFilter.getStaffRootId());
		params.add(unitFilter.getRoleId());
		params.add(unitFilter.getShopRootId());
		sql.append(" where 1=1 ");
		if (unitFilter.getStatus() != null) {
			sql.append(" and s.status = ? ");
			params.add(unitFilter.getStatus().getValue());
		} else {
			sql.append(" and ((s.status = 1 and privShop.shop_id is not null) or s.status = 0)");
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getUnitCode())) {
			sql.append(" and s.shop_code like ? ");
			params.add(StringUtility.toOracleSearchLike(unitFilter.getUnitCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getUnitName())) {
			sql.append(" and( (upper(s.name_text) like ? ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(unitFilter.getUnitName().trim().toUpperCase())));
			sql.append(" or (upper(s.shop_name) like ? ESCAPE '/' ) )");
			params.add(StringUtility.toOracleSearchLike(unitFilter.getUnitName().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getManageUnitCode())) {
			sql.append(" and exists ( ");
			sql.append(" 	select 1 ");
			sql.append(" 	from shop ");
			sql.append(" 	where 1=1 ");
			sql.append(" 	and shop_id = s.shop_id ");
			//longnh15 add: search status =0 va 1
			if (unitFilter.getStatus()==ActiveType.RUNNING)
			{
				sql.append(" 	and status = ? ");
				params.add(ActiveType.RUNNING.getValue());
			}
			sql.append(" 	start with shop_code = ? ");
			params.add(unitFilter.getManageUnitCode().trim().toUpperCase());
			sql.append(" 	connect by prior shop_id = parent_shop_id ");
			sql.append(" ) ");
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getUnitType())) {
			sql.append(" and exists ( ");
			sql.append(" 	select 1 ");
			sql.append(" 	from shop_type ste ");
			sql.append(" 	where 1=1 ");
			sql.append(" 	and s.shop_type_id = ste.shop_type_id ");
			sql.append(" 	and ste.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" 	and ste.name = ? ");
			params.add(unitFilter.getUnitType());
			sql.append(" ) ");
		}
		
		StringBuilder countSql = new StringBuilder("select count(1) as count from (");
		countSql.append(sql);
		countSql.append(")");
		
		if (!StringUtility.isNullOrEmpty(unitFilter.getSortField()) && !StringUtility.isNullOrEmpty(unitFilter.getSortOrder())) {
			if (unitFilter.getSortField().toLowerCase().equals("parentshopname")) {
				sql.append(" order by (select shop_name from shop where shop_id = s.parent_shop_id) " + " " + unitFilter.getSortOrder());
			} else {
				sql.append(" order by " + unitFilter.transformSortFieldToDBTableColumn() + " " + unitFilter.getSortOrder());				
			}
		}
		
		final String[] fieldNames = new String[] {
			"shopId", 
			"shopCode", 
			"shopName",
			"abbreviation",
			"orgName","parentShopCode","parentShopName","phone",
			"mobiphone","fax","email","taxNum","contactName",
			"billTo","shipTo","precinctName","areaName","address",
			"shopType",
			"latlng",
			"parentId",
			"shopTypeId"
		};

		final Type[] fieldTypes = new Type[] { 
			StandardBasicTypes.LONG, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.LONG,
			StandardBasicTypes.LONG
		};
		
		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(ShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
		return repo.getListBySQL(ShopVO.class, sql.toString(), params);
	}

	@Override
	public List<ShopVO> findShopVOBy(KPaging<ShopVO> kPaging, UnitFilter unitFilter) throws DataAccessException {
		if (unitFilter == null) {
			throw new IllegalArgumentException("unitFilter is null");
		}
		if (unitFilter.getStaffRootId() == null || unitFilter.getRoleId() == null || unitFilter.getShopRootId() == null) {
			throw new IllegalArgumentException("priviledge data is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select ");
		sql.append(" 	s.shop_id shopId, ");
		sql.append(" 	s.shop_code shopCode, ");
		sql.append(" 	s.shop_name shopName, ");
		sql.append(" 	s.status status, ");
		sql.append("	(select shop_name from shop where shop_id = s.parent_shop_id) parentShopName, ");
		sql.append(" 	(select name from shop_type where shop_type_id = s.shop_type_id) shopType, ");
		sql.append("	parent_shop_id parentId, ");
		sql.append("	shop_type_id shopTypeId ");
		sql.append(" from shop s ");
		if (ActiveType.RUNNING == unitFilter.getStatus()) {
			sql.append(" join ( ");
		} else {
			sql.append(" left join ( ");
		}
		if (StringUtility.isNullOrEmpty(unitFilter.getStrShopId()) && unitFilter.getStaffRootId() != null && unitFilter.getRoleId() != null && unitFilter.getShopRootId() != null) {
			sql.append("     select number_key_id as shop_id from table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ");
			params.add(unitFilter.getStaffRootId());
			params.add(unitFilter.getRoleId());
			params.add(unitFilter.getShopRootId());
		}
		
		if (!StringUtility.isNullOrEmpty(unitFilter.getStrShopId())) {
			sql.append("     select sh.shop_id shop_id from shop sh where 1=1  ");
			String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(unitFilter.getStrShopId(), "sh.shop_id");
			sql.append(paramsFix);
		}
		
		sql.append(" ) privShop on s.shop_id = privShop.shop_id ");
		sql.append(" where 1=1 ");
		if (unitFilter.getStatus() != null) {
			sql.append(" and s.status = ? ");
			params.add(unitFilter.getStatus().getValue());
		} else {
			sql.append(" and ((s.status = 1 and privShop.shop_id is not null) or s.status = 0)");
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getUnitCode())) {
			sql.append(" and s.shop_code like ? ");
			params.add(StringUtility.toOracleSearchLike(unitFilter.getUnitCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getUnitName())) {
			sql.append(" and( (upper(s.name_text) like ? ESCAPE '/' ) ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(unitFilter.getUnitName().trim().toUpperCase())));
			sql.append(" or (upper(s.shop_name) like ? ESCAPE '/' ) )");
			params.add(StringUtility.toOracleSearchLike(unitFilter.getUnitName().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getManageUnitCode())) {
			sql.append(" and exists ( ");
			sql.append(" 	select 1 ");
			sql.append(" 	from shop ");
			sql.append(" 	where 1=1 ");
			sql.append(" 	and shop_id = s.shop_id ");
			//longnh15 add: search status 0 va 1
			if (unitFilter.getStatus()==ActiveType.RUNNING)
			{
				sql.append(" 	and status = ? ");
				params.add(ActiveType.RUNNING.getValue());
			}
			sql.append(" 	start with shop_code = ? ");
			params.add(unitFilter.getManageUnitCode().trim().toUpperCase());
			sql.append(" 	connect by prior shop_id = parent_shop_id ");
			sql.append(" ) ");
		}
		if (!StringUtility.isNullOrEmpty(unitFilter.getUnitType())) {
			sql.append(" and exists ( ");
			sql.append(" 	select 1 ");
			sql.append(" 	from shop_type ste ");
			sql.append(" 	where 1=1 ");
			sql.append(" 	and s.shop_type_id = ste.shop_type_id ");
			sql.append(" 	and ste.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" 	and ste.name = ? ");
			params.add(unitFilter.getUnitType());
			sql.append(" ) ");
		}
		
		StringBuilder countSql = new StringBuilder("select count(1) as count from (");
		countSql.append(sql);
		countSql.append(")");
		
		if (!StringUtility.isNullOrEmpty(unitFilter.getSortField()) && !StringUtility.isNullOrEmpty(unitFilter.getSortOrder())) {
			if (unitFilter.getSortField().toLowerCase().equals("parentshopname")) {
				sql.append(" order by (select shop_name from shop where shop_id = s.parent_shop_id) " + " " + unitFilter.getSortOrder());
			} else {
				sql.append(" order by " + unitFilter.transformSortFieldToDBTableColumn() + " " + unitFilter.getSortOrder());				
			}
		}
		
		final String[] fieldNames = new String[] {
			"shopId", 
			"shopCode", 
			"shopName", 
			"status",
			"parentShopName",
			"shopType",
			"parentId",
			"shopTypeId"
		};

		final Type[] fieldTypes = new Type[] { 
			StandardBasicTypes.LONG, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.STRING, 
			StandardBasicTypes.INTEGER, 
			StandardBasicTypes.STRING,
			StandardBasicTypes.STRING,
			StandardBasicTypes.LONG,
			StandardBasicTypes.LONG
		};
		
		if (kPaging != null) {
			return repo.getListByQueryAndScalarPaginated(ShopVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
		}
		return repo.getListBySQL(ShopVO.class, sql.toString(), params);
	}

	@Override
	public List<Shop> getShopsOnTree(List<Long> startFromShops, Boolean includeStoppedShop) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select * ");
		sql.append(" from shop ");
		sql.append(" where 1=1 ");
		if (includeStoppedShop == null || !includeStoppedShop) {
			sql.append(" and status = ? ");
			params.add(ActiveType.RUNNING.getValue());
		}
		if (startFromShops == null || startFromShops.isEmpty()) {
			sql.append(" start with shop_id in ( ");
			sql.append("     select s.shop_id ");
			sql.append("     from shop s ");
			sql.append("     where 1=1 ");
			sql.append("     and s.parent_shop_id is null ");
			sql.append("     and s.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("     and exists ( ");
			sql.append("         select 1 ");
			sql.append("         from organization o ");
			sql.append("         where 1=1 ");
			sql.append("         and o.status = ? ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("         and o.parent_org_id is null ");
			sql.append("         and o.node_type = ? ");
			params.add(OrganizationNodeType.SHOP.getValue());
			sql.append("         and exists (select 1 from shop_type where shop_type_id = o.node_type_id and status = ?) ");
			params.add(ActiveType.RUNNING.getValue());
			sql.append("         and o.node_type_id = s.shop_type_id ");
			sql.append("     ) ");
			sql.append(" ) ");
		} else {
			sql.append(" start with shop_id in (-1 ");
			for (Long shopId : startFromShops) {
				sql.append(" ,? ");
				params.add(shopId);
			}
			sql.append(" ) ");
		}
		sql.append(" connect by prior shop_id = parent_shop_id and status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" order by level, shop_code ");
		
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	/**
	 * checkShopHaveActiveSubShop: kiem tra shop co shop con hoat dong
	 * @return boolean
	 * @author phuocdh2
	 * @description: Nhan vao shopId va kiem tra shop con co hoat dong
	 * @createDate 02/03/2015
	 */
	@Override
	public Boolean checkShopHaveActiveSubShop(ActiveType status, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select count(1) as count from shop sh start with sh.shop_id = ? ");
		params.add(shopId);
		sql.append(" connect by prior sh.shop_id = sh.parent_shop_id ");
		sql.append(" and status = ? ");
		params.add(status.getValue());
		int c = repo.countBySQL(sql.toString(), params);
		return c > 1 ? true : false;
	}
	/**
	 * checkShopHaveActiveStaff lay so luong nhan vien truc thuoc shop nhap vao va 
	 * @author phuocdh2
	 * @return true: co nhan vien dang hoat dong false : shop khong co nhan vien dang hoat dong
	 * @throws DataAccessException
	 * @since 02/03/2015
	 */
	@Override
	public Boolean checkShopHaveActiveStaff(ActiveType status,Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count  from staff st ");
		sql.append(" where st.shop_id = ? ");
		params.add(shopId);
		sql.append(" and st.status = ?");
		params.add(status.getValue());
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}
	/**
	 * checkShopHaveActiveCustomer lay so luong khach hang hoat dong truc thuoc shop nhap vao va 
	 * @author phuocdh2
	 * @return true : shop co khach hang hoat dong false : shop co khach hang het hoat dong
	 * @throws DataAccessException
	 * @since 02/03/2015
	 */
	@Override
	public Boolean checkShopHaveActiveCustomer(ActiveType status,Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(1) as count  from customer ct ");
		sql.append(" where ct.shop_id = ? ");
		params.add(shopId);
		sql.append(" and ct.status = ?");
		params.add(status.getValue());
		int c = repo.countBySQL(sql.toString(), params);
		return c > 0 ? true : false;
	}

	/**
	 * lay danh sach shop_id theo level co cau hinh hien gia, an gia theo level tu 1-> 4... 
	 * shop con -> shop cha .. uu tien muc con, muc co level thap nhat
	 * @author phuocdh2
	 * @return List<ShopVO> danh sach shop_param
	 * @throws DataAccessException
	 * @since 08/04/2015
	 */
	@Override
	public List<ShopVO> getListShopVOConfigShowPrice(Long shopId, Integer status) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select sh.shopid as shopId , sh.lev as levelShowPrice ,shp.value as value ");
		sql.append(" from (  ");
		sql.append("        select s.shop_id as shopid, level  as lev  ");
		sql.append("         from shop s start with s.shop_id = ? ");
		sql.append("           and s.status = 1 ");
		sql.append("         connect by prior  s.parent_shop_id = s.shop_id ");
		sql.append("  ) sh join shop_param shp on shp.shop_id = sh.shopid ");
		sql.append(" where  shp.code= 'SYS_SHOW_PRICE' and shp.status = 1 ");   
		sql.append(" order by levelShowPrice ");
		if (shopId != null) {
			params.add(shopId);
		}
		
		final String[] fieldNames = new String[] { "shopId", "levelShowPrice", "value" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	

	
	/**
	 * @author cuonglt3
	 * 
	 */
	@Override
	public List<Shop> getListShopSpectific(ShopFilter filter) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();

		StringBuffer  varname1 = new StringBuffer();
		varname1.append("SELECT * ");
		varname1.append("FROM   shop S, ");
		varname1.append("       organization ORG, ");
		varname1.append("       shop_type ST ");
		varname1.append("WHERE  1 = 1 ");
		varname1.append("       AND S.organization_id = ORG.organization_id ");
		varname1.append("       AND ORG.node_type = 1 ");
		varname1.append("       AND S.shop_type_id = ST.shop_type_id ");
		varname1.append("       AND ST.specific_type = 1");
		varname1.append("       AND S.status = 1");
		if (filter.getkPaging() == null)
			return repo.getListBySQL(Shop.class, varname1.toString(), params);
		else
			return repo.getListBySQLPaginated(Shop.class, varname1.toString(), params, filter.getkPaging());
	}

	@Override
	public List<ShopVO> getListShopVOConfigShowPrice(Long shopId,
			ApParamType sysModifyPrice) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select sh.shopid as shopId , sh.lev as levelShowPrice ,shp.value as value ");
		sql.append(" from (  ");
		sql.append("        select s.shop_id as shopid, level  as lev  ");
		sql.append("         from shop s start with s.shop_id = ? ");
		sql.append("           and s.status = 1 ");
		sql.append("         connect by prior  s.parent_shop_id = s.shop_id ");
		sql.append("  ) sh join shop_param shp on shp.shop_id = sh.shopid ");
		sql.append(" where  shp.code= ? and shp.status = 1 ");   
		sql.append(" order by levelShowPrice ");
		if (shopId != null) {
			params.add(shopId);
			params.add(sysModifyPrice.toString());
		}
		
		final String[] fieldNames = new String[] { "shopId", "levelShowPrice", "value" };

		final Type[] fieldTypes = new Type[] { StandardBasicTypes.LONG, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}


	@Override
	public List<ShopParam> getConfigShopParam(Long shopId, String paramCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShop as( ");
		sql.append(" select sh.shop_id, level lv ");
		sql.append(" from shop sh ");
		sql.append(" connect by prior parent_shop_id = shop_id and status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" start with shop_id = ? and status = ? ");
		params.add(shopId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ) ");
		sql.append(" select sp.* ");
		sql.append(" from lstShop sh ");
		sql.append(" join shop_param sp on sh.shop_id = sp.shop_id ");
		sql.append(" where sp.code = upper(?) ");
		params.add(paramCode);
		sql.append(" and sp.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" order by sh.lv ");
		sql.append("  ");
		return repo.getListBySQL(ShopParam.class, sql.toString(), params);
	}
	@Override
	public List<Shop> getShopsOnTree(ShopFilter shopFilter) throws DataAccessException {
		Boolean isGetAllShops = shopFilter == null || 
							(shopFilter != null && StringUtility.isNullOrEmpty(shopFilter.getShopCode()) && StringUtility.isNullOrEmpty(shopFilter.getShopName()));
		if (isGetAllShops) {
			return getAllShopsOnTree(shopFilter);
		}
		return getFilterShopsOnTree(shopFilter);
	}
	@Override
	public List<Shop> getShopsInAccessOnTree(ShopFilter shopFilter, Long keyshopId) throws DataAccessException {
		Boolean isGetAllShops = shopFilter == null || 
				(shopFilter != null && StringUtility.isNullOrEmpty(shopFilter.getShopCode()) && StringUtility.isNullOrEmpty(shopFilter.getShopName()));
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select dv.* from (");
		sql.append(" select * from shop ");
		sql.append("  WHERE 1 = 1 ");
		sql.append("  and status in (?, ?) ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.STOPPED.getValue());
		sql.append(" start with shop_id in ( ");
		sql.append("     select shop_id ");
		sql.append("     from ").append("ks_shop_map");
		sql.append("     where ks_id = ? ");
		params.add(keyshopId);
		sql.append("     and status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		if (!isGetAllShops && shopFilter != null) {
			if (!StringUtility.isNullOrEmpty(shopFilter.getShopCode().trim())) {
				sql.append("     and exists (select 1 from shop where shop_id = ").append("ks").append(".shop_id and shop_code like ? ESCAPE '/') ");
				params.add(StringUtility.toOracleSearchLike(shopFilter.getShopCode().trim().toUpperCase()));
			}
			if (!StringUtility.isNullOrEmpty(shopFilter.getShopName().trim())) {
				sql.append("     and exists (select 1 from shop where shop_id = ").append("ks").append(".shop_id and upper(name_text) like ? ESCAPE '/') ");
				String englishName = Unicode2English.codau2khongdau(shopFilter.getShopName().trim().toUpperCase());
				params.add(StringUtility.toOracleSearchLike(englishName));
			}			
		}
		sql.append(" ) ");
		sql.append(" connect by prior parent_shop_id = shop_id ");
		sql.append(" union ");
		sql.append(" SELECT * ");
		sql.append(" from shop ");
		sql.append("  WHERE 1 = 1 ");
		sql.append("  and status in (?, ?) ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.STOPPED.getValue());
		sql.append(" start with shop_id in ( ");
		sql.append("     select shop_id ");
		sql.append("     from ").append("ks_shop_map");
		sql.append("     where ks_id = ? ");
		params.add(keyshopId);
		sql.append("     and status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ) ");
		sql.append(" connect by parent_shop_id = prior shop_id ");
		sql.append(" ) dv ");
		sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on dv.shop_id = ogr.number_key_id ");
		params.add(shopFilter.getStaffRootId());
		params.add(shopFilter.getRoleId());
		params.add(shopFilter.getShopId());
		sql.append(" order by dv.shop_code ");
		
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	private List<Shop> getAllShopsOnTree(ShopFilter shopFilter) throws DataAccessException {
		if (shopFilter.getStaffRootId() == null && shopFilter.getRoleId() == null && shopFilter.getShopRootId() == null) {
			throw new IllegalArgumentException("cms paramater is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sh.* from ( ");
		sql.append(" select *  ");
		sql.append(" from shop ");
		sql.append(" where 1=1 ");
		if (shopFilter != null && shopFilter.getStatus() != null) {
			sql.append(" and status = ? ");
			params.add(shopFilter.getStatus().getValue());
		}
		sql.append(" start with shop_id in ( ");
		sql.append("     select s.shop_id ");
		sql.append("     from shop s ");
		sql.append("     where 1=1 ");
		sql.append("     and s.parent_shop_id is null ");
		sql.append("     and s.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("     and exists ( ");
		sql.append("         select 1 ");
		sql.append("         from organization o ");
		sql.append("         where 1=1 ");
		sql.append("         and o.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("         and o.parent_org_id is null ");
		sql.append("         and o.node_type = ? ");
		params.add(OrganizationNodeType.SHOP.getValue());
		sql.append("         and exists (select 1 from shop_type where shop_type_id = o.node_type_id and status = ?) ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("         and o.node_type_id = s.shop_type_id ");
		sql.append("     ) ");
		sql.append(" ) ");
		sql.append(" connect by prior shop_id = parent_shop_id ");
		sql.append(" order by level, shop_code ");
		sql.append(" ) sh ");
		sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on sh.shop_id = ogr.number_key_id ");
		params.add(shopFilter.getStaffRootId());
		params.add(shopFilter.getRoleId());
		params.add(shopFilter.getShopRootId());
		
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	private List<Shop> getFilterShopsOnTree(ShopFilter shopFilter) throws DataAccessException {
		if (shopFilter.getStaffRootId() == null && shopFilter.getRoleId() == null && shopFilter.getShopRootId() == null) {
			throw new IllegalArgumentException("cms paramater is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" with ");
		sql.append(" tmp as ( ");
		sql.append("     select *  ");
		sql.append("     from shop  ");
		sql.append("     where 1=1  ");
		sql.append("     and status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		if (shopFilter != null && !StringUtility.isNullOrEmpty(shopFilter.getShopCode().trim())) {
			sql.append("     and shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shopFilter.getShopCode().trim().toUpperCase()));
		}
		if (shopFilter != null && !StringUtility.isNullOrEmpty(shopFilter.getShopName().trim())) {
			sql.append("     and upper(name_text) like ? ESCAPE '/' ");
			String englishName = Unicode2English.codau2khongdau(shopFilter.getShopName().trim().toUpperCase());
			params.add(StringUtility.toOracleSearchLike(englishName));
		}
		sql.append("     start with shop_id in (  ");
		sql.append("         select s.shop_id  ");
		sql.append("         from shop s  ");
		sql.append("         where 1=1  ");
		sql.append("         and s.parent_shop_id is null  ");
		sql.append("         and s.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("         and exists (  ");
		sql.append("             select 1  ");
		sql.append("             from organization o  ");
		sql.append("             where 1=1  ");
		sql.append("             and o.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("             and o.parent_org_id is null  ");
		sql.append("             and o.node_type = ? ");
		params.add(OrganizationNodeType.SHOP.getValue());
		sql.append("             and exists (select 1 from shop_type where shop_type_id = o.node_type_id and status = ?)  ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("             and o.node_type_id = s.shop_type_id  ");
		sql.append("         )  ");
		sql.append("     )  ");
		sql.append("     connect by prior shop_id = parent_shop_id ");
		sql.append(" ) ");
		sql.append(" select distinct * from ( ");
		sql.append(" select distinct * ");
		sql.append(" from ( ");
		sql.append("     select *   ");
		sql.append("     from shop ");
		sql.append("     where status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("     start with shop_id in (select shop_id from tmp) ");
		sql.append("     connect by prior parent_shop_id = shop_id ");
		sql.append(" ) ");
		sql.append(" ) rp join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on rp.shop_id = ogr.number_key_id ");
		params.add(shopFilter.getStaffRootId());
		params.add(shopFilter.getRoleId());
		params.add(shopFilter.getShopRootId());
		sql.append(" order by shop_code ");
		
		return repo.getListBySQL(Shop.class, sql.toString().replaceAll("  ", " "), params);
	}
	@Override
	public List<Shop> retriveShopsForKeyShop(Long keyshopId) throws DataAccessException {
		if (keyshopId == null) {
			throw new IllegalArgumentException("keyshopId must not be null.");
		}
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select s.* ");
		sql.append(" from ks_shop_map ks ");
		sql.append(" join shop s on ks.shop_id = s.shop_id ");
		sql.append(" where 1=1 ");
		sql.append(" and ks.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and ks.ks_id = ? ");
		params.add(keyshopId);		
		
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<Shop> getListNextChildShop(ShopFilter filter) throws DataAccessException {
		if (filter.getStaffRootId() == null || filter.getRoleId() == null || filter.getShopRootId() == null) {
			throw new IllegalArgumentException("cms paramater is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (filter.getShopId() == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		
		sql.append(" select * ");
		sql.append(" from shop ");
		sql.append(" where level <= 2 ");//next child
		sql.append(" start with shop_id = ? ");
		params.add(filter.getShopId());
		if (filter.getStatus() != null) {
			sql.append(" and status = ? ");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and status = ? ");
			params.add(ActiveType.RUNNING.getValue());
		}
		sql.append(" connect by prior shop_id = parent_shop_id ");
		if (filter.getStatus() != null) {
			sql.append(" and status = ? ");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and status = ? ");
			params.add(ActiveType.RUNNING.getValue());
		}
		StringBuilder newSql = new StringBuilder();
		newSql.append(" with lstShop as ( ");
		newSql.append("select rp.* from ( ");
		newSql.append(sql.toString());
		newSql.append("		) rp  ");
		newSql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on rp.shop_id = ogr.number_key_id ");
		params.add(filter.getStaffRootId());
		params.add(filter.getRoleId());
		params.add(filter.getShopRootId());
		newSql.append(" ) ");
		newSql.append(" select * from lstShop s ");
		newSql.append(" where 1 = 1 ");
		newSql.append(" and s.shop_id <> ? ");
		params.add(filter.getShopId());//not add root parent
		return repo.getListBySQL(Shop.class, newSql.toString(), params);
		
		
	}
	
	/**
	 * @author vuongmq
	 * @date 21/08/2015
	 * @descriptip Lay danh sach shop cua NVGS quan ly
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	@Override
	public List<Shop> getListShopOfSupervise(RptStaffSaleFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		if (filter == null) {
			throw new IllegalArgumentException("RptStaffSaleFilter is null");
		}
		sql.append(" select distinct sh.* from map_user_staff mus ");
		sql.append(" join shop sh on sh.shop_id = mus.shop_id ");
		sql.append(" where 1 = 1 ");
		if (filter.getStaffId() != null) {
			sql.append(" and user_id = ? ");
			params.add(filter.getStaffId());
		}
		sql.append(" and mus.from_date < trunc(sysdate) + 1 ");
		sql.append(" and (mus.to_date is null or mus.to_date >= trunc(sysdate)) ");
		sql.append(" order by sh.shop_code, sh.shop_name ");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}

	@Override
	public List<Shop> getAncestorAndSiblingShops(Long shopId) throws DataAccessException {
		if (shopId == null) {
			throw new IllegalArgumentException("shopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.* ");
		sql.append(" from ( ");
		sql.append("     select * ");
		sql.append("     from shop ");
		sql.append("     where 1=1 ");
		sql.append("     start with shop_id = ? ");
		params.add(shopId);
		sql.append("     connect by prior parent_shop_id = shop_id ");
		sql.append("     union ");
		sql.append("     select * ");
		sql.append("     from shop ");
		sql.append("     where parent_shop_id = (select parent_shop_id from shop where shop_id = ?) ");
		params.add(shopId);
		sql.append(" ) s ");
		sql.append(" join organization o on s.organization_id = o.organization_id ");
		sql.append(" order by s.parent_shop_id nulls first, o.node_ordinal, s.shop_name ");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	@Override
	public List<ShopVO> searchShopsInAccessOnTreeForKeyShop(ShopFilter shopFilter, Long keyShopId) throws DataAccessException {
		Boolean isGetAllShops = shopFilter == null ||  (shopFilter != null && StringUtility.isNullOrEmpty(shopFilter.getShopCode()) && StringUtility.isNullOrEmpty(shopFilter.getShopName()));
		if (shopFilter.getStaffRootId() == null || shopFilter.getRoleId() == null || shopFilter.getShopId() == null) {
			throw new IllegalArgumentException("CMS Paramater is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT dv.shop_id as id ");
		sql.append("   , dv.parent_shop_id as parentShopId ");
		sql.append(" , dv.shop_code as shopCode ");
		sql.append(" , dv.shop_name as shopName ");
		sql.append(" , dv.status ");
		sql.append(" , k_st.QUANTITY as quantityTarget ");
		sql.append(" , k_st.KS_SHOP_TARGET_ID ksShopTagetId ");
		sql.append(" FROM ");
		sql.append("   (SELECT shop_id ");
		sql.append("   , parent_shop_id ");
		sql.append("   , shop_code ");
		sql.append("   , shop_name ");
		sql.append("   , status ");
		sql.append("  FROM shop ");
		sql.append("  WHERE 1 = 1 ");
		sql.append("  and status in (?, ?) ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.STOPPED.getValue());
		sql.append(" start with shop_id in ( ");
		sql.append("     select shop_id ");
		sql.append("     from ").append("ks_shop_map");
		sql.append("     where ks_id = ? ");
		params.add(keyShopId);
		sql.append("     and status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		if (!isGetAllShops && shopFilter != null) {
			if (!StringUtility.isNullOrEmpty(shopFilter.getShopCode())) {
				sql.append("     and exists (select 1 from shop where shop_id = ").append("ks").append(".shop_id and shop_code like ? ESCAPE '/') ");
				params.add(StringUtility.toOracleSearchLike(shopFilter.getShopCode().trim().toUpperCase()));
			}
			if (!StringUtility.isNullOrEmpty(shopFilter.getShopName())) {
				sql.append("     and exists (select 1 from shop where shop_id = ").append("ks").append(".shop_id and upper(name_text) like ? ESCAPE '/') ");
				String englishName = Unicode2English.codau2khongdau(shopFilter.getShopName().trim().toUpperCase());
				params.add(StringUtility.toOracleSearchLike(englishName));
			}			
		}
		sql.append(" ) ");
		sql.append(" connect by prior parent_shop_id = shop_id ");
		sql.append(" union ");
		sql.append(" SELECT shop_id ");
		sql.append("   , parent_shop_id ");
		sql.append("   , shop_code ");
		sql.append("  , shop_name ");
		sql.append("  , status ");
		sql.append(" from shop ");
		sql.append("  WHERE 1 = 1 ");
		sql.append("  and status in (?, ?) ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.STOPPED.getValue());
		sql.append(" start with shop_id in ( ");
		sql.append("     select shop_id ");
		sql.append("     from ").append("ks_shop_map");
		sql.append("     where ks_id = ? ");
		params.add(keyShopId);
		sql.append("     and status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ) ");
		sql.append(" connect by parent_shop_id = prior shop_id ");
		sql.append(" ) dv ");
		sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on dv.shop_id = ogr.number_key_id ");
		params.add(shopFilter.getStaffRootId());
		params.add(shopFilter.getRoleId());
		params.add(shopFilter.getShopId());
		sql.append(" left join KS_SHOP_TARGET k_st on dv.shop_id = k_st.shop_id and k_st.KS_ID = ? and k_st.status = ? ");
		params.add(keyShopId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" where 1 = 1 ");
		if (shopFilter.getStatus() != null) {
			sql.append(" and dv.status = ? ");
			params.add(shopFilter.getStatus().getValue());
		} else {
			sql.append(" and dv.status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" ORDER BY dv.shop_code ");
		
		final String[] fieldNames = new String[] { 
				"id" //0
				, "parentShopId" //1
				,"shopCode" //2
				,"shopName" //3
				,"status" //4
				,"quantityTarget" //5
				,"ksShopTagetId" //6
				};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG //0
				, StandardBasicTypes.LONG //1
				, StandardBasicTypes.STRING //2
				, StandardBasicTypes.STRING //3
				, StandardBasicTypes.INTEGER //4
				, StandardBasicTypes.INTEGER //5
				, StandardBasicTypes.LONG //6
				};
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<ShopVO> searchShopsOnTreeForKeyShop(ShopFilter shopFilter, Long keyShopId) throws DataAccessException {
		if (shopFilter.getStaffRootId() == null || shopFilter.getRoleId() == null || shopFilter.getShopId() == null) {
			throw new IllegalArgumentException("CMS Paramater is null");
		}
		if (keyShopId == null) {
			throw new IllegalArgumentException("KeyshopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT dv.shop_id as id ");
		sql.append("   , dv.parent_shop_id as parentShopId ");
		sql.append(" , dv.shop_code as shopCode ");
		sql.append(" , dv.shop_name as shopName ");
		sql.append(" , dv.status ");
		sql.append(" , k_st.QUANTITY as quantityTarget ");
		sql.append(" , k_st.KS_SHOP_TARGET_ID ksShopTagetId ");
		sql.append(" FROM shop dv ");
		sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on dv.shop_id = ogr.number_key_id ");
		params.add(shopFilter.getStaffRootId());
		params.add(shopFilter.getRoleId());
		params.add(shopFilter.getShopId());
		sql.append(" left join KS_SHOP_TARGET k_st on dv.shop_id = k_st.shop_id and k_st.KS_ID = ?  and k_st.status = ? ");
		params.add(keyShopId);
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" where 1 = 1 ");
		if (shopFilter.getStatus() != null) {
			sql.append(" and dv.status = ? ");
			params.add(shopFilter.getStatus().getValue());
		} else {
			sql.append(" and dv.status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(shopFilter.getShopCode())) {
			sql.append("  and shop_code like ? ESCAPE '/'  ");
			params.add(StringUtility.toOracleSearchLike(shopFilter.getShopCode().trim().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopFilter.getShopName())) {
			sql.append("  and lower(name_text) like ? ESCAPE '/' ");
			String englishName = Unicode2English.codau2khongdau(shopFilter.getShopName().trim().toLowerCase());
			params.add(StringUtility.toOracleSearchLike(englishName));
		}
		sql.append(" ORDER BY dv.shop_code ");
		
		final String[] fieldNames = new String[] { 
				"id" //0
				, "parentShopId" //1
				,"shopCode" //2
				,"shopName" //3
				,"status" //4
				,"quantityTarget" //5
				,"ksShopTagetId" //6
				};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG //0
				, StandardBasicTypes.LONG //1
				, StandardBasicTypes.STRING //2
				, StandardBasicTypes.STRING //3
				, StandardBasicTypes.INTEGER //4
				, StandardBasicTypes.INTEGER //5
				, StandardBasicTypes.LONG //6
				};
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<Shop> getShopForConnectChildrenByKeyShop(Long keyShopId, Integer status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select * from shop  ");
		sql.append("   WHERE 1 = 1 ");
		if (status != null) {
			sql.append("   and status = ? ");
			params.add(status);
		} else {
			sql.append("   and status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (keyShopId != null) {
			sql.append("   START WITH shop_id in ( ");
			sql.append("    select shop_id from KS_SHOP_MAP ");
			sql.append("    where status = ? and KS_ID = ? ");
			params.add(ActiveType.RUNNING.getValue());
			params.add(keyShopId);
			sql.append("    GROUP BY shop_id ");
			sql.append("  ) ");
			sql.append("  CONNECT BY PRIOR shop_id = PARENT_SHOP_ID ");
		}
		
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	@Override
	public List<ShopVO> getShopForConnectChildrenByKeyShopVO(Long keyShopId, Long cycleId, Integer status, Long staffRootId, Long roleId, Long shopRootId) throws DataAccessException {
		if (shopRootId == null || roleId == null || staffRootId == null) {
			throw new IllegalArgumentException("CMS param is null");
		}
		if (keyShopId == null) {
			throw new IllegalArgumentException("KeyshopId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sh.shop_id as id  ");
		sql.append(" , sh.parent_shop_id as parentShopId  ");
		sql.append(" , sh.shop_code as shopCode ");
		sql.append(" , sh.shop_name as shopName ");
		sql.append(" , sh.status ");
		sql.append(" , NVL(q_sh.quantity, 0) as quantity  ");
		sql.append(" , NVL(tg_sh.quantityTarget, 0) as quantityTarget  ");
		sql.append(" from ( ");
		sql.append(" select shop_id, parent_shop_id, shop_code, shop_name, status  ");
		sql.append(" from shop  ");
		sql.append(" WHERE 1 = 1");
		if (status != null) {
			sql.append("   and status = ? ");
			params.add(status);
		} else {
			sql.append("   and status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" START WITH shop_id in (?) ");
		params.add(shopRootId);
		sql.append(" CONNECT BY PRIOR shop_id = PARENT_SHOP_ID");
		sql.append(" ) sh ");
		sql.append(" join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on sh.shop_id = ogr.number_key_id ");
		params.add(staffRootId);
		params.add(roleId);
		params.add(keyShopId);
		sql.append(" left join (  ");
		sql.append("  select shop_id, max(demKH) as quantity ");
		sql.append("  from (");
		sql.append(" select shop_id, CYCLE_ID, COUNT(*) demKH ");
		sql.append(" from KS_CUSTOMER");
		sql.append(" where 1 = 1  ");
		sql.append(" and status = ?  ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and KS_ID = ?");
		params.add(keyShopId);
		if (cycleId != null) {
			sql.append(" and CYCLE_ID = ? ");
			params.add(cycleId);
		}
		sql.append(" GROUP BY shop_id, CYCLE_ID  ");
		sql.append("  ) ");
		sql.append("  GROUP BY shop_id ");
		sql.append(" ) q_sh on sh.shop_id = q_sh.shop_id ");
		sql.append(" left join (  ");
		sql.append(" select shop_id, max (quantity) as quantityTarget  ");
		sql.append(" from KS_SHOP_TARGET  ");
		sql.append(" WHERE status = ?  ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" group by shop_id  ");
		sql.append(" ) tg_sh on sh.shop_id = tg_sh.shop_id ");
		final String[] fieldNames = new String[] { 
				"id" //0
				,"parentShopId" //1
				,"shopCode" //2
				,"shopName" //3
				,"status" //4
				,"quantity" //5
				,"quantityTarget" //6
				};

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG //0
				, StandardBasicTypes.LONG //1
				, StandardBasicTypes.STRING //2
				, StandardBasicTypes.STRING //3
				, StandardBasicTypes.INTEGER //4
				, StandardBasicTypes.INTEGER //5
				, StandardBasicTypes.INTEGER //6
				};
		return repo.getListByQueryAndScalar(ShopVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<Shop> getAllDistributorShop() throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.* ");
		sql.append(" from shop s ");
		sql.append(" join organization o on s.organization_id = o.organization_id ");
		sql.append(" join shop_type st on st.shop_type_id = o.node_type_id ");
		sql.append(" where 1=1 ");
		sql.append(" and st.specific_type = ? ");
		params.add(ShopSpecificType.NPP.getValue());
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	/**
	 * get all shop belong to record
	 * @author phut
	 */
	@Override
	public List<Shop> getAllShopByLstId(Long recordId, List<Long> lstId) throws DataAccessException {
		if (lstId == null || lstId.isEmpty()) {
			throw new IllegalArgumentException("lstId is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * ");
		sql.append(" FROM shop WHERE status = 1 ");
		sql.append(" and shop_id in (-1 ");
		for (Long parentId : lstId) {
			sql.append(" ,?");
			params.add(parentId);
		}
		sql.append(" ) ");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	@Override
	public List<Long> getListLongShopParentByShopId(Long shopId) throws DataAccessException {
		if (shopId == null) {
			return null;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sh.shop_id from shop sh ");
		sql.append(" start with sh.shop_id = ? ");
		params.add(shopId);	
		sql.append(" connect by prior sh.parent_shop_id = sh.shop_id ");
		List<Object> lst = repo.getDataToListPaginated(sql.toString(), params, 0, 0);
		List<Long> lstLong = new ArrayList<Long>();
		for (Object object : lst) {
			lstLong.add(Long.valueOf(((Map<Object, Object>)object).get("SHOP_ID").toString()));
			/*lstLong.add(Long.valueOf(())
			((Map<Object, Object>)object).keySet().toArray()[0];
			*/
		}
		return lstLong;
		//return repo.getEntityBySQL(ShopHistory.class, sql.toString(), params);
	}
	
	@Override
	public List<Long> getListLongShopChildByShopId(Long shopId) throws DataAccessException {
		if (shopId == null) {
			return null;
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select sh.shop_id from shop sh ");
		sql.append(" start with sh.shop_id = ? ");
		params.add(shopId);	
		sql.append(" connect by prior sh.shop_id = sh.parent_shop_id ");
		List<Object> lst = repo.getDataToListPaginated(sql.toString(), params, 0, 0);
		List<Long> lstLong = new ArrayList<Long>();
		for (Object object : lst) {
			lstLong.add(Long.valueOf(((Map<Object, Object>)object).get("SHOP_ID").toString()));
			/*lstLong.add(Long.valueOf(())
			((Map<Object, Object>)object).keySet().toArray()[0];
			*/
		}
		return lstLong;
		//return repo.getEntityBySQL(ShopHistory.class, sql.toString(), params);
	}

}
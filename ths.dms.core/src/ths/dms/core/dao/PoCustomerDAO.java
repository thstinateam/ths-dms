package ths.dms.core.dao;

import java.util.Date;

import ths.dms.core.entities.PoCustomer;
import ths.dms.core.exceptions.DataAccessException;

public interface PoCustomerDAO {
	void deletePoCustomer(PoCustomer poCustomer) throws DataAccessException;
	void updatePoCustomer(PoCustomer poCustomer) throws DataAccessException;
	PoCustomer getPoCustomerById(long id) throws DataAccessException;
	PoCustomer getPoCustomerByOrderNumberAndShopId(String orderNumber, Long shopId, Date orderDate) throws DataAccessException;
	PoCustomer getPoCustomerByFromSaleorderId(Long saleOrderId) throws DataAccessException;
	PoCustomer getPoCustomerById(Long id) throws DataAccessException;
}

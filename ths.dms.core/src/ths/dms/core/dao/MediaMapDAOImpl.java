package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.MediaMap;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class MediaMapDAOImpl implements MediaMapDAO {
	@Autowired
	private IRepository repo;	
	
	@Override
	public MediaMap createMediaMap(MediaMap mediaMap) throws DataAccessException {
		if (mediaMap == null) {
			throw new IllegalArgumentException("MediaMap");
		}
		repo.create(mediaMap);
		return repo.getEntityById(MediaMap.class, mediaMap.getMediaMapId());
	}

	@Override
	public void deleteMediaMap(MediaMap mediaMap) throws DataAccessException {
		if (mediaMap == null) {
			throw new IllegalArgumentException("MediaMap");
		}
		repo.delete(mediaMap);
	}

	@Override
	public MediaMap getMediaMapById(long id) throws DataAccessException {
		return repo.getEntityById(MediaMap.class, id);
	}
	
	@Override
	public MediaMap getMediaMapByMediaAndObject(Long mediaId, Long objectId, Long objectType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("   select *   "); 
		sql.append("   from media_map mm   "); 
		sql.append("   where 1 = 1   "); 
		sql.append("   and media_id = ?   ");
		params.add(mediaId);
		sql.append("   and object_id = ?   "); 
		params.add(objectId);
		sql.append("   and object_type = ?   ");
		params.add(objectType);		
		sql.append("   and status = 1");
		sql.append("   and rownum = 1   ");
		return repo.getEntityBySQL(MediaMap.class, sql.toString(), params);
		
	}
	
	@Override
	public MediaMap updateMediaMap(MediaMap mediaMap) throws DataAccessException {
		if (mediaMap == null) {
			throw new IllegalArgumentException("MediaMap");
		}
		
		repo.update(mediaMap);
		return this.getMediaMapById(mediaMap.getMediaMapId());
	}
}

package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.PoVnmDetail;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.PoVnmDetailLotVO;
import ths.dms.core.entities.vo.PoVnmDetailVO;
import ths.core.entities.vo.rpt.RptInputProductVinamilkVO;
import ths.core.entities.vo.rpt.RptPoCfrmFromDvkh2VO;
import ths.core.entities.vo.rpt.RptProductPoVnmDetail2VO;
import ths.core.entities.vo.rpt.Rpt_PO_CTTTVO;
import ths.core.entities.vo.rpt.Rpt_PO_CTTT_MAPPINGVO;
import ths.dms.core.exceptions.DataAccessException;

public interface PoVnmDetailDAO {

	PoVnmDetail createPoVnmDetail(PoVnmDetail poVnmDetail) throws DataAccessException;

	void deletePoVnmDetail(PoVnmDetail poVnmDetail) throws DataAccessException;

	void updatePoVnmDetail(PoVnmDetail poVnmDetail) throws DataAccessException;
	
	PoVnmDetail getPoVnmDetailById(long id) throws DataAccessException;
	
	
	List<PoVnmDetailVO> getListPoVnmDetailVO(Long poVnmId, Long shopId) throws DataAccessException;

	/**
	 * getListPoVnmDetailLotVOByPoVnmId
	 * @author hieunq1
	 * @param poVnmId
	 * @return
	 * @throws DataAccessException
	 */
	public List<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmId(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId)
			throws DataAccessException;
	/**
	 * getListPoVnmDetailLotVOByPoVnmId
	 * @author vuongmq
	 * @param poVnmId
	 * @return
	 * @throws DataAccessException
	 */
	public List<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmIdStockIn(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId)
			throws DataAccessException;

	/**
	 * createListPoVnmDetail
	 * @author hieunq1
	 * @param listPoVnmDetail
	 * @return 
	 * @throws DataAccessException
	 */
	void createListPoVnmDetail(List<PoVnmDetail> listPoVnmDetail)
			throws DataAccessException;

	/**
	 * updateListPoVnmDetail
	 * @author hieunq1
	 * @param listPoVnmDetail
	 * @throws DataAccessException
	 */
	void updateListPoVnmDetail(List<PoVnmDetail> listPoVnmDetail)
			throws DataAccessException;

	/**
	 * getPoVnmDetailByIdForUpdate
	 * @author hieunq1
	 * @param id
	 * @return
	 * @throws DataAccessException
	 */
	PoVnmDetail getPoVnmDetailByIdForUpdate(long id) throws DataAccessException;

	/**
	 * getListProductRptPoVnmDetailVO
	 * @author hieunq1
	 * @return
	 * @throws DataAccessException 
	 */
	List<RptProductPoVnmDetail2VO> getListRptProductPoVnmDetailVO(Long shopId) throws DataAccessException;

	/**
	 * getListPoCfrmFromDvkhRptVO
	 * @author hieunq1
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptPoCfrmFromDvkh2VO> getListRptPoCfrmFromDvkh2VO(Long shopId,
			Date fromDate, Date toDate) throws DataAccessException;
	
	/**
	 * lay danh sach phieu nhap hang tu vinamilk
	 * @author thanhnguyen
	 * @param kPaging
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptInputProductVinamilkVO> getListRptImportProduct(
			Long shopId, Date fromDate, Date toDate) throws DataAccessException;
	
	PoVnmDetail getPoVnmDetail(Long poVnmId, Long productId, Integer ProductType) throws DataAccessException;

	List<PoVnmDetail> getListPoVnmDetailByPoVnmId(KPaging<PoVnmDetail> kPaging,
			Long poVnmId) throws DataAccessException;

	List<PoVnmDetail> getListPoVnmDetailByPoVnmId(Long poVnmId)
			throws DataAccessException;

	List<PoVnmDetailLotVO> getPoConfirmDetail(
			KPaging<PoVnmDetailLotVO> kPaging, Long poDVKHId, Long poConfirmId)
			throws DataAccessException;
	
	/**
	 * 
	 * @param shopId
	 * @param typePerson : 1-NVKH; 0-CUSTOMER
	 * @param codePerson
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<Rpt_PO_CTTTVO> getPOCTTT(Long shopId, int typePerson, String codePerson, String fromDate, String toDate)throws DataAccessException;

	List<PoVnmDetailLotVO> getListFullPoVnmDetailLotVOByPoVnmId(KPaging<PoVnmDetailLotVO> kPaging, List<Long> poVnmId)throws DataAccessException;
	/**
	 * getListPoVnmDetailByPoVnmId
	 * @author phuongvm
	 * @return
	 * @throws DataAccessException 
	 */
	List<PoVnmDetail> getListPoVnmDetailByPoVnmId(Long poVnmId, Long shopId,
			Long productId) throws DataAccessException;
	/**
	 * 
	 * @param kPaging
	 * @param poVnmId
	 * @param shopId
	 * @author phuongvm
	 * @return
	 * @throws DataAccessException
	 */
	List<PoVnmDetailLotVO> getListPoVnmDetailLotVOByPoVnmIdEx(KPaging<PoVnmDetailLotVO> kPaging, Long poVnmId, Long shopId,Date lockDate) throws DataAccessException;

	
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.CustomerAttribute;
import ths.dms.core.entities.CustomerAttributeDetail;
import ths.dms.core.entities.CustomerAttributeEnum;
import ths.dms.core.entities.ProductAttributeDetail;
import ths.dms.core.entities.PromotionCustAttr;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumFilter;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;
/**
 * @author liemtpt
 * @since 21/01/2015
 */
public class CustomerAttributeDAOImpl implements CustomerAttributeDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private CommonDAO commonDAO;

	@Override
	public CustomerAttribute createCustomerAttribute(CustomerAttribute customerAttribute,LogInfoVO logInfo)
			throws DataAccessException {
		if (customerAttribute == null) {
			throw new IllegalArgumentException("customerAttribute is null");
		}
		customerAttribute.setCreateDate(commonDAO.getSysDate());
		if (logInfo != null){
			customerAttribute.setCreateUser(logInfo.getStaffCode());
		}
		repo.create(customerAttribute);
		return repo.getEntityById(CustomerAttribute.class, customerAttribute.getId());
	}

	@Override
	public CustomerAttributeEnum createCustomerAttributeEnum(CustomerAttributeEnum customerAttributeEnum,LogInfoVO logInfo)
			throws DataAccessException {
		if (customerAttributeEnum == null) {
			throw new IllegalArgumentException("customerAttributeEnum is null");
		}
		customerAttributeEnum.setCreateDate(commonDAO.getSysDate());
		if (logInfo != null){
			customerAttributeEnum.setCreateUser(logInfo.getStaffCode());
		}
		repo.create(customerAttributeEnum);
		return repo.getEntityById(CustomerAttributeEnum.class, customerAttributeEnum.getId());
	}
	
	@Override
	public void deleteCustomerAttribute(CustomerAttribute customerAttribute)
			throws DataAccessException {
		if (customerAttribute == null) {
			throw new IllegalArgumentException("customerAttribute is null");
		}
		repo.delete(customerAttribute);
	}
	@Override
	public void deleteCustomerAttributeEnum(CustomerAttributeEnum customerAttributeEnum)
			throws DataAccessException {
		if (customerAttributeEnum == null) {
			throw new IllegalArgumentException("customerAttributeEnum is null");
		}
		repo.delete(customerAttributeEnum);
	}
	@Override
	public CustomerAttribute getCustomerAttributeById(long id) throws DataAccessException {
		return repo.getEntityById(CustomerAttribute.class, id);
	}

	@Override
	public CustomerAttributeEnum getCustomerAttributeEnumById(long id) throws DataAccessException {
		return repo.getEntityById(CustomerAttributeEnum.class, id);
	}
	
	@Override
	public void updateCustomerAttribute(CustomerAttribute customerAttribute, LogInfoVO logInfo) throws DataAccessException {
		if (customerAttribute == null) {
			throw new IllegalArgumentException("customerAttribute is null");
		}
		customerAttribute.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			customerAttribute.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(customerAttribute);
	}
	@Override
	public void updateCustomerAttributeDetail(CustomerAttributeDetail customerAttribute, LogInfoVO logInfo) throws DataAccessException {
		if (customerAttribute == null) {
			throw new IllegalArgumentException("customerAttributeDetail is null");
		}
		customerAttribute.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			customerAttribute.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(customerAttribute);
	}
	@Override
	public void updateCustomerAttributeEnum(CustomerAttributeEnum customerAttributeEnum, LogInfoVO logInfo) throws DataAccessException {
		if (customerAttributeEnum == null) {
			throw new IllegalArgumentException("customerAttributeEnum is null");
		}
		customerAttributeEnum.setUpdateDate(commonDAO.getSysDate());
		if (logInfo != null){
			customerAttributeEnum.setUpdateUser(logInfo.getStaffCode());
		}
		repo.update(customerAttributeEnum);
	}
	
	@Override
	public CustomerAttribute getCustomerAttributeByCode(String code)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from customer_attribute ");
		sql.append(" where 1 = 1 and (status = 1 or status = 0)");
		sql.append(" and code = ? ");
		params.add(code.toUpperCase());
		return repo.getEntityBySQL(CustomerAttribute.class, sql.toString(), params);
	}
	
//	@Override
//	public CustomerAttributeEnum getCustomerAttributeEnumByCode(String code)throws DataAccessException {
//		StringBuilder sql = new StringBuilder();
//		List<Object> params = new ArrayList<Object>();
//		sql.append(" select * from customer_attribute_enum ");
//		sql.append(" where 1 = 1 and status = 1");
//		sql.append(" and code = ? ");
//		params.add(code.toUpperCase());
//		return repo.getEntityBySQL(CustomerAttributeEnum.class, sql.toString(), params);
//	}
	
	@Override
	public CustomerAttributeEnum getCustomerAttributeEnumByCode(String code)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from customer_attribute_enum ");
		sql.append(" where 1 = 1 and (status = 1 or status = 0) ");
		sql.append(" and code = ? ");
		params.add(code.toUpperCase());
		return repo.getEntityBySQL(CustomerAttributeEnum.class, sql.toString(), params);
	}
	
	@Override
	public List<AttributeDynamicVO> getListCustomerAttributeVO(AttributeDynamicFilter filter, KPaging<AttributeDynamicVO> kPaging) throws DataAccessException {
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		selectSql.append("select ca.customer_attribute_id attributeId ");
		selectSql.append(" ,ca.code attributeCode ");
		selectSql.append(" ,ca.name attributeName ");
		selectSql.append(" ,ca.type type ");
		selectSql.append(" ,ca.mandatory mandatory ");
		selectSql.append(" ,ca.display_order displayOrder ");
		selectSql.append(" ,ca.status status ");
		selectSql.append(" ,ca.description description ");
		selectSql.append(" ,ca.data_length dataLength ");
		selectSql.append(" ,ca.min_value minValue ");
		selectSql.append(" ,ca.max_value maxValue ");
		fromSql.append(" from customer_attribute ca  ");
		fromSql.append("WHERE 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		if (!StringUtility.isNullOrEmpty(filter.getAttributeCode())) {
			fromSql.append(" and upper(ca.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getAttributeCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getAttributeName())) {
			fromSql.append(" and upper(ca.name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getAttributeName().toUpperCase()));
		}
		if (filter.getType()!=null) {
			fromSql.append(" and ca.type = ? ");
			params.add(filter.getType());
		}
		if(filter.getStatus() != null){
			fromSql.append(" and ca.status =?");
			params.add(filter.getStatus());
		}
		
		selectSql.append(fromSql.toString());
		
		countSql.append("select count(ca.customer_attribute_id) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"attributeId",  	// 1
				"attributeCode",	// 2
				"attributeName" ,	// 3 
				"type",				// 4
				"mandatory",		// 5
				"displayOrder",		// 6
				"status",			// 7
				"description",		// 8
				"dataLength",		// 9
				"minValue",			// 10
				"maxValue"			// 11
		};
		
		String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			selectSql.append(" order by ").append(sort).append(" ");
			String order = StringUtility.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				selectSql.append(order);
			}
		} else {
			selectSql.append(" order by ca.code, ca.name");
		}
		
		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, 	// 1
				StandardBasicTypes.STRING, 	// 2
				StandardBasicTypes.STRING,	// 3 
				StandardBasicTypes.INTEGER,	// 4
				StandardBasicTypes.INTEGER, // 5
				StandardBasicTypes.INTEGER, // 6
				StandardBasicTypes.INTEGER,	// 7 
				StandardBasicTypes.STRING,	// 8
				StandardBasicTypes.INTEGER,	// 9
				StandardBasicTypes.INTEGER,	// 10
				StandardBasicTypes.INTEGER	// 11
		};	
		List<AttributeDynamicVO> lst =  null;
		if (kPaging == null) {
			lst = repo.getListByQueryAndScalar(AttributeDynamicVO.class, fieldNames, fieldTypes, selectSql.toString(), params);
		} else {
			lst = repo.getListByQueryAndScalarPaginated(AttributeDynamicVO.class, fieldNames, fieldTypes, selectSql.toString(), countSql.toString(), params, params, kPaging);
		}
		return lst;
	}
	@Override
	public List<AttributeEnumVO> getListCustomerAttributeEnumVO(AttributeEnumFilter filter,
			KPaging<AttributeEnumVO> kPaging) throws DataAccessException {
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		selectSql.append("select ca.customer_attribute_id attributeId ");
		selectSql.append(" ,cae.customer_attribute_enum_id enumId ");
		selectSql.append(" ,cae.code enumCode ");
		selectSql.append(" ,cae.value enumValue ");
		selectSql.append(" ,cae.status status");
		fromSql.append(" from customer_attribute ca ");
		fromSql.append(" join customer_attribute_enum cae on ca.customer_attribute_id = cae.customer_attribute_id ");
		fromSql.append(" WHERE 1 = 1 and cae.status = 1 ");
		List<Object> params = new ArrayList<Object>();
		
		if (filter.getAttributeId() != null) {
			fromSql.append(" and ca.customer_attribute_id = ? ");
			params.add(filter.getAttributeId());
		}
		if(filter.getStatus() != null){
			fromSql.append(" and ca.status = ? ");
			params.add(filter.getStatus().getValue());
		}
		
		selectSql.append(fromSql.toString());
		
		countSql.append("select count(cae.customer_attribute_enum_id) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"attributeId", "enumId", "enumCode", "enumValue", "status"
				};
		String sort = StringUtility.getColumnSort(filter.getSort(), fieldNames);
		if (!StringUtility.isNullOrEmpty(sort)) {
			selectSql.append(" order by ").append(sort).append(" ");
			String order = StringUtility.getOrderBy(filter.getOrder());
			if (!StringUtility.isNullOrEmpty(order)) {
				selectSql.append(order);
			}
		} else {
			selectSql.append(" order by cae.code, cae.value");
		}
		Type[] fieldTypes = {
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER
		};	
		List<AttributeEnumVO> lst =  null;
		if (kPaging == null) {
			lst = repo.getListByQueryAndScalar(AttributeEnumVO.class, fieldNames, fieldTypes, selectSql.toString(), params);
		} else {
			lst = repo.getListByQueryAndScalarPaginated(AttributeEnumVO.class, fieldNames, fieldTypes, selectSql.toString(), countSql.toString(), params, params, kPaging);
		}
		return lst;
	}
	
	@Override
	public PromotionCustAttr getPromotionCustAttrByObjectId(Long objectId)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select pca.* from promotion_cust_attr pca ");
		sql.append(" join promotion_program pp on pp.promotion_program_id = pca.promotion_program_id ");
		sql.append(" where 1  = 1 ");
		sql.append(" and pca.status = 1 and pp.status = 1 ");
		sql.append(" and pca.object_type = 1 and (pp.to_date >= trunc(sysdate) or pp.to_date is null) ");
		sql.append(" and pca.object_id = ? ");
		params.add(objectId);
		return repo.getEntityBySQL(PromotionCustAttr.class, sql.toString(), params);
	}
	
	@Override
	public List<CustomerAttributeDetail> getListCustomerAttributeDetailByEnumId(Long id) throws DataAccessException {
		if (id == null)
			return new ArrayList<CustomerAttributeDetail>();
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from customer_attribute_detail where 1 = 1");
		sql.append(" and status = 1 and customer_attribute_enum_id = ? ");
		params.add(id);
		return repo.getListBySQL(CustomerAttributeDetail.class, sql.toString(), params);
	}
	
	@Override
	public CustomerAttributeDetail getCustomerAttributeDetailByFilter(
			Long customerAtt, Long customerId, Long customerAttEnum)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from customer_attribute_detail ");
		sql.append(" where status = 1 ");
		if(customerAtt != null){
			sql.append(" and customer_attribute_id = ? ");
			params.add(customerAtt);
		}
		
		if(customerId != null){
			sql.append(" and customer_id = ? ");
			params.add(customerId);
		}
		if(customerAttEnum != null){
			sql.append(" and customer_attribute_enum_id = 1 ");
			params.add(customerAttEnum);
		}
		return repo.getEntityBySQL(CustomerAttributeDetail.class, sql.toString(), params);
	}
	
	@Override
	public List<CustomerAttributeDetail> getCustomerAttributeDetailByFilter(
			Long customerAtt, Long customerId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from customer_attribute_detail ");
		sql.append(" where status = 1 ");
		if(customerAtt != null){
			sql.append(" and customer_attribute_id = ? ");
			params.add(customerAtt);
		}
		
		if(customerId != null){
			sql.append(" and customer_id = ? ");
			params.add(customerId);
		}		
		return repo.getListBySQL(CustomerAttributeDetail.class, sql.toString(), params);
	}
}

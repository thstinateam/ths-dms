package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.SaleOrderPromoDetail;
import ths.dms.core.entities.SaleOrderPromotion;
import ths.dms.core.entities.enumtype.ProgramType;
import ths.dms.core.entities.filter.SaleOrderPromoDetailFilter;
import ths.dms.core.entities.vo.SaleOrderDetailVOEx;
import ths.dms.core.entities.vo.SaleOrderPromotionDetailVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class SaleOrderPromoDetailDAOImpl implements SaleOrderPromoDetailDAO {

	@Autowired
	IRepository repo;
	
	/**
	 * @author lacnv1
	 */
	@Override
	public void createListSaleOrderPromoDetail(List<SaleOrderPromoDetail> lstPromo) throws DataAccessException {
		if (lstPromo == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		repo.create(lstPromo);
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrderPromoDetail> getListSaleOrderPromoByDetailId(long soDetailId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from sale_order_promo_detail");
		sql.append(" where sale_order_detail_id = ?");
		params.add(soDetailId);
		
		List<SaleOrderPromoDetail> lst = repo.getListBySQL(SaleOrderPromoDetail.class, sql.toString(), params);
		return lst;
	}
	
	/**
	 * @author trietptm
	 * @param filter
	 * @return danh sach SaleOrderPromoDetail theo filter
	 * @throws DataAccessException
	 * @since Aug 21, 2015
	 */
	@Override
	public List<SaleOrderPromoDetail> getListSaleOrderPromoDetailByFilter(SaleOrderPromoDetailFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from sale_order_promo_detail");
		sql.append(" where 1 = 1 ");
		if (filter.getSaleOrderDetailId() != null) {
			sql.append(" and sale_order_detail_id = ?");
			params.add(filter.getSaleOrderDetailId());
		}
		if (Boolean.TRUE.equals(filter.getIsNotKeyShop())) {
			sql.append(" and (program_type is null OR program_type <> ?) ");
			params.add(ProgramType.KEY_SHOP.getValue());
		}

		List<SaleOrderPromoDetail> lst = repo.getListBySQL(SaleOrderPromoDetail.class, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public void deleteSaleOrderPromoDetail(SaleOrderPromoDetail promo) throws DataAccessException {
		if (promo == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		repo.delete(promo);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<SaleOrderPromotion> getListSaleOrderPromotionByOrderId(long saleOrderId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select * from sale_order_promotion");
		sql.append(" where sale_order_id = ?");
		params.add(saleOrderId);
		
		List<SaleOrderPromotion> lst = repo.getListBySQL(SaleOrderPromotion.class, sql.toString(), params);
		return lst;
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public void createListSaleOrderPromotion(List<SaleOrderPromotion> lstPromo) throws DataAccessException {
		if (lstPromo == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		repo.create(lstPromo);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public void deleteSaleOrderPromotion(SaleOrderPromotion promo) throws DataAccessException {
		if (promo == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		repo.delete(promo);
	}
	
	
	@Override
	public List<SaleOrderDetailVOEx> getListSaleOrderPromoDetailForOrder(Long orderId, Integer promotionType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select NVL(sod.promotion_order_number,0) levelPromo, sum(s.discount_amount) discountAmount, ");
		sql.append(" s.program_code as programCode, s.programe_type_code as promotionType");
		sql.append(" from sale_order_promo_detail s ");
		sql.append(" join sale_order_detail sod on s.sale_order_detail_id = sod.sale_order_detail_id ");
		sql.append(" where s.is_owner = ? and s.sale_order_id = ? group by sod.promotion_order_number, s.program_code, s.programe_type_code ");
		params.add(promotionType);
		params.add(orderId);
		String[] fieldNames = {"levelPromo","discountAmount", "programCode", "promotionType"};
		Type[] fieldTypes = {StandardBasicTypes.INTEGER, StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.STRING,StandardBasicTypes.STRING};
		
		return repo.getListByQueryAndScalar(SaleOrderDetailVOEx.class, fieldNames, fieldTypes, sql.toString().replaceAll("  ", " "), params);
	}
	
	/**
	 * @author trietptm
	 */
	@Override
	public List<SaleOrderPromotionDetailVO> getListSaleOrderPromoByFilter(SaleOrderPromoDetailFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select PROGRAM_CODE programCode, SUM(DISCOUNT_AMOUNT) discountAmount from sale_order_promo_detail ");
		sql.append(" where 1 = 1 ");
		if (filter.getSaleOrderId() != null) {
			sql.append(" AND SALE_ORDER_ID = ? ");
			params.add(filter.getSaleOrderId());
		}
		if (filter.getShopId() != null ){
			sql.append(" AND SHOP_ID = ? ");
			params.add(filter.getShopId());
		}
		if (filter.getProgramType() != null) {
			sql.append(" AND PROGRAM_TYPE = ? ");
			params.add(filter.getProgramType().getValue());
		}
		if (!StringUtility.isNullOrEmpty(filter.getProgrameTypeCode())) {
			sql.append(" AND PROGRAME_TYPE_CODE = ? ");
			params.add(filter.getProgrameTypeCode());
		}
		if (Boolean.TRUE.equals(filter.getIsNotKeyShop())) {
			sql.append(" AND PROGRAM_TYPE <> ? ");
			params.add(ProgramType.KEY_SHOP.getValue());
		}
		sql.append(" GROUP BY SALE_ORDER_ID, PROGRAM_CODE ");
		String[] fieldNames = {
				"programCode",
				"discountAmount"
		};
		
		Type[] fieldTypes = {
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL
		};
		
		List<SaleOrderPromotionDetailVO> lst = repo.getListByQueryAndScalar(SaleOrderPromotionDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
}
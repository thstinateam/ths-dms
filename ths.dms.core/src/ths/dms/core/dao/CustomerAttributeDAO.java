package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.CustomerAttribute;
import ths.dms.core.entities.CustomerAttributeDetail;
import ths.dms.core.entities.CustomerAttributeEnum;
import ths.dms.core.entities.ProductAttributeDetail;
import ths.dms.core.entities.PromotionCustAttr;
import ths.dms.core.entities.enumtype.AttributeDynamicFilter;
import ths.dms.core.entities.enumtype.AttributeDynamicVO;
import ths.dms.core.entities.enumtype.AttributeEnumFilter;
import ths.dms.core.entities.enumtype.AttributeEnumVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;
// TODO: Auto-generated Javadoc

/**
 * The Interface CustomerAttributeDAO.
 *
 * @author liemtpt
 * @since 21/01/2015
 */
public interface CustomerAttributeDAO {

	/**
	 * Creates the customer attribute.
	 *
	 * @param customerAttribute the customer attribute
	 * @param logInfo the log info
	 * @return the customer attribute
	 * @throws DataAccessException the data access exception
	 */
	CustomerAttribute createCustomerAttribute(CustomerAttribute customerAttribute, LogInfoVO logInfo)
			throws DataAccessException;

	/**
	 * Delete customer attribute.
	 *
	 * @param customerAttribute the customer attribute
	 * @throws DataAccessException the data access exception
	 */
	void deleteCustomerAttribute(CustomerAttribute customerAttribute) throws DataAccessException;

	/**
	 * Update customer attribute.
	 *
	 * @param customerAttribute the customer attribute
	 * @param logInfo the log info
	 * @throws DataAccessException the data access exception
	 */
	void updateCustomerAttribute(CustomerAttribute customerAttribute , LogInfoVO logInfo) throws DataAccessException;

	/**
	 * Gets the customer attribute by code.
	 *
	 * @param code the code
	 * @return the customer attribute by code
	 * @throws DataAccessException the data access exception
	 */
	CustomerAttribute getCustomerAttributeByCode(String code)
	throws DataAccessException;
	
	/**
	 * Gets the customer attribute by id.
	 *
	 * @param id the id
	 * @return the customer attribute by id
	 * @throws DataAccessException the data access exception
	 */
	CustomerAttribute getCustomerAttributeById(long id) throws DataAccessException;
   
   /**
    * **
    * Danh sach thuoc tinh dong cho khach hang.
    *
    * @param filter the filter
    * @param kPaging the k paging
    * @return list customer attribute vo
    * @throws DataAccessException the data access exception
    * @author liemtpt
    */
	List<AttributeDynamicVO> getListCustomerAttributeVO(AttributeDynamicFilter filter,
			KPaging<AttributeDynamicVO> kPaging) throws DataAccessException;

	/**
	 * **
	 * Danh sach gia tri thuoc tinh khi chon mot va chon nhieu.
	 *
	 * @param filter the filter
	 * @param kPaging the k paging
	 * @return list customer attribute enum vo
	 * @throws DataAccessException the data access exception
	 * @author liemtpt
	 */
	List<AttributeEnumVO> getListCustomerAttributeEnumVO(
		AttributeEnumFilter filter, KPaging<AttributeEnumVO> kPaging)
		throws DataAccessException;

	/**
	 * Gets the customer attribute enum by code.
	 *
	 * @param code the code
	 * @return the customer attribute enum by code
	 * @throws DataAccessException the data access exception
	 */
	CustomerAttributeEnum getCustomerAttributeEnumByCode(String code)
			throws DataAccessException;

	/**
	 * Update customer attribute enum.
	 *
	 * @param customerAttributeEnum the customer attribute enum
	 * @param logInfo the log info
	 * @throws DataAccessException the data access exception
	 */
	void updateCustomerAttributeEnum(CustomerAttributeEnum customerAttributeEnum, LogInfoVO logInfo)
			throws DataAccessException;

	/**
	 * Creates the customer attribute enum.
	 *
	 * @param customerAttributeEnum the customer attribute enum
	 * @param logInfo the log info
	 * @return the customer attribute enum
	 * @throws DataAccessException the data access exception
	 */
	CustomerAttributeEnum createCustomerAttributeEnum(CustomerAttributeEnum customerAttributeEnum, LogInfoVO logInfo)
			throws DataAccessException;

	/**
	 * Gets the promotion cust attr by object id.
	 *
	 * @param objectId the object id
	 * @return the promotion cust attr by object id
	 * @throws DataAccessException the data access exception
	 */
	PromotionCustAttr getPromotionCustAttrByObjectId(Long objectId)
			throws DataAccessException;

	/**
	 * Delete customer attribute enum.
	 *
	 * @param customerAttributeEnum the customer attribute enum
	 * @throws DataAccessException the data access exception
	 */
	void deleteCustomerAttributeEnum(CustomerAttributeEnum customerAttributeEnum)
			throws DataAccessException;

	/**
	 * Gets the customer attribute enum by id.
	 *
	 * @param id the id
	 * @return the customer attribute enum by id
	 * @throws DataAccessException the data access exception
	 */
	CustomerAttributeEnum getCustomerAttributeEnumById(long id)
			throws DataAccessException;

	/**
	 * Gets the list customer attribute detail by enum id.
	 *
	 * @param id the id
	 * @return the list customer attribute detail by enum id
	 * @throws DataAccessException the data access exception
	 */
	List<CustomerAttributeDetail> getListCustomerAttributeDetailByEnumId(Long id)
			throws DataAccessException;

	/**
	 * Update customer attribute detail.
	 *
	 * @param customerAttribute the customer attribute
	 * @param logInfo the log info
	 * @throws DataAccessException the data access exception
	 */
	void updateCustomerAttributeDetail(
			CustomerAttributeDetail customerAttribute, LogInfoVO logInfo)
			throws DataAccessException;

	CustomerAttributeDetail getCustomerAttributeDetailByFilter(Long productAtt,
			Long productId, Long productAttEnum) throws DataAccessException;

	List<CustomerAttributeDetail> getCustomerAttributeDetailByFilter(
			Long customerAtt, Long customerId) throws DataAccessException;

}

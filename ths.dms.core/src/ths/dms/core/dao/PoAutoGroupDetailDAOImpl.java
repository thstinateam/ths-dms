package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PoAutoGroupDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupDetailVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class PoAutoGroupDetailDAOImpl implements PoAutoGroupDetailDAO {

	@Autowired
	private IRepository repo;

	@Override
	public PoAutoGroupDetail createPoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail)
			throws DataAccessException {
		if (poAutoGroupDetail == null) {
			throw new IllegalArgumentException("poAuto");
		}
		repo.create(poAutoGroupDetail);
		return repo.getEntityById(PoAutoGroupDetail.class, poAutoGroupDetail.getId());
	}

	@Override
	public void deletePoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail)
			throws DataAccessException {
		if (poAutoGroupDetail == null) {
			throw new IllegalArgumentException("poAutoGroupDetail");
		}
		repo.delete(poAutoGroupDetail);
		
	}

	@Override
	public void updatePoAutoGroupDetail(PoAutoGroupDetail poAutoGroupDetail)
			throws DataAccessException {
		if (poAutoGroupDetail == null) {
			throw new IllegalArgumentException("poAutoGroupDetail");
		}
		repo.update(poAutoGroupDetail);
		
	}

	@Override
	public PoAutoGroupDetail getPoAutoGroupDetailById(Long id) throws DataAccessException {
		return repo.getEntityById(PoAutoGroupDetail.class, id);
	}

	@Override
	public List<PoAutoGroupDetail> getListPoAutoGroupDetail(
			KPaging<PoAutoGroupDetail> kPaging, Long poAutoGroupId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select d.*");
		sql.append(" from PO_AUTO_GROUP_DETAIL d");
		sql.append(" where 1 = 1");

		if (poAutoGroupId != null) {
			sql.append(" and d.PO_AUTO_GROUP_ID = ?");
			params.add(poAutoGroupId);
		}

		sql.append(" order by d.PO_AUTO_GROUP_DETAIL_ID asc");

		if (kPaging == null)
			return repo
					.getListBySQL(PoAutoGroupDetail.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(PoAutoGroupDetail.class,
					sql.toString(), params, kPaging);
	}

	@Override
	public List<PoAutoGroupDetailVO> getListPoAutoGroupDetailVO(
			PoAutoGroupDetailFilter filter, KPaging<PoAutoGroupDetailVO> kPaging)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		sql.append("SELECT d.po_auto_group_detail_id id,");
		sql.append(" d.object_type objectType,");
		sql.append(" d.object_id objectId,");
		sql.append("  (");
		sql.append("  CASE");

		sql.append("    WHEN object_type = 0 ");

		sql.append("    THEN");
		sql.append("      (SELECT product_info_code");
		sql.append("      FROM product_info");
		sql.append("      WHERE type          = 1");
		sql.append("      AND status          = 1");
		sql.append("      AND product_info_id = d.object_id");
		sql.append("      )");

		sql.append("    WHEN object_type = 1");

		sql.append("    THEN");
		sql.append("      (SELECT product_info_code");
		sql.append("      FROM product_info");
		sql.append("      WHERE type          = 2");
		sql.append("      AND status          = 1");
		sql.append("      AND product_info_id = d.object_id");
		sql.append("      )");

		sql.append("    WHEN object_type = 2");

		sql.append("    THEN");
		sql.append("      (SELECT product_code");
		sql.append("      FROM product");
		sql.append("      WHERE status   = 1");
		sql.append("      AND product_id = d.object_id");
		sql.append("      )");
		sql.append("  END) code,");
		
		sql.append("  (");
		sql.append("  CASE");
		sql.append("    WHEN object_type = 1");
		sql.append("    THEN");
		sql.append("      (SELECT info.product_info_code ");
		sql.append("      FROM product_info_map m");
		sql.append("      JOIN product_info info ON info.product_info_id = m.FROM_OBJECT_ID");
		sql.append("      WHERE m.object_type     = 12 and m.status = 1 and M.TO_OBJECT_ID = d.object_id and rownum = 1 ");
		sql.append("      )");
		sql.append("    else");
		sql.append("      ''");
		sql.append("  END) parentCode,");
		
		sql.append("  (");
		sql.append("  CASE");
		sql.append("    WHEN object_type = 0 ");
		sql.append("    THEN");
		sql.append("      (SELECT product_info_name");
		sql.append("      FROM product_info");
		sql.append("      WHERE type          = 1");
		sql.append("      AND status          = 1");
		sql.append("      AND product_info_id = d.object_id");
		sql.append("      )");

		sql.append("    WHEN object_type = 1");

		sql.append("    THEN");
		sql.append("      (SELECT product_info_name");
		sql.append("      FROM product_info");
		sql.append("      WHERE type          = 2");
		sql.append("      AND status          = 1");
		sql.append("      AND product_info_id = d.object_id");
		sql.append("      )");

		sql.append("    WHEN object_type = 2");

		sql.append("    THEN");
		sql.append("      (SELECT product_name");
		sql.append("      FROM product");
		sql.append("      WHERE status   = 1");
		sql.append("      AND product_id = d.object_id");
		sql.append("      )");
		sql.append("  END) name");
		fromSql.append(" FROM po_auto_group_detail d");
		fromSql.append(" WHERE 1 = 1 and d.status = 1");
		List<Object> params = new ArrayList<Object>();
		if (filter.getPoAutoGroupId() != null) {
			fromSql.append(" and d.PO_AUTO_GROUP_ID = ? ");
			params.add(filter.getPoAutoGroupId());
		}
		if (filter.getObjectId() != null) {
			fromSql.append(" and d.object_id = ? ");
			params.add(filter.getObjectId());
		}
		if (filter.getObjectType() != null) {
			fromSql.append(" and d.object_type = ? ");
			params.add(filter.getObjectType());
		}
		
		sql.append(fromSql.toString());
		sql.append(" order by code");
		
		countSql.append("select count(d.po_auto_group_detail_id) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"id", "objectId", "objectType", 
				"name", "code", "parentCode"
				};

		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.INTEGER,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING  
		};	
		List<PoAutoGroupDetailVO> lst = null;
		if (kPaging == null){
			lst =  repo.getListByQueryAndScalar(
					PoAutoGroupDetailVO.class, fieldNames,
					fieldTypes, sql.toString(), params);
		}else{
			lst = repo.getListByQueryAndScalarPaginated(PoAutoGroupDetailVO.class, fieldNames, fieldTypes,
					sql.toString(), countSql.toString(), params, params, kPaging);
		}
		return lst;
	}

	@Override
	public List<PoAutoGroupDetailVO> getPoAutoGroupDetailVOByShop(Long shopId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select g.po_auto_group_id as groupId, g.group_code as groupCode, gd.po_auto_group_detail_id as id, gd.object_id as objectId, gd.object_type as objectType ");
		sql.append(" from po_auto_group g, po_auto_group_detail gd ");
		sql.append(" where g.po_auto_group_id = gd.po_auto_group_id ");
		sql.append("    and g.status = ? and gd.status =? ");
		params.add(ActiveType.RUNNING.getValue());
		params.add(ActiveType.RUNNING.getValue());
		if(null != shopId){
			sql.append("    and exists (select 1 from po_auto_group_shop_map where po_auto_group_shop_map_id = g.po_auto_group_shop_map_id and shop_id = ? and status = ?) ");
			params.add(shopId);
			params.add(ActiveType.RUNNING.getValue());
		} else{
			sql.append("    and g.po_auto_group_shop_map_id is null ");
		}

		String[] fieldNames = { "id", "objectId", "objectType", "groupId", "groupCode" };

		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.LONG,
				StandardBasicTypes.INTEGER, StandardBasicTypes.LONG,
				StandardBasicTypes.STRING };

		return this.repo.getListByQueryAndScalar(PoAutoGroupDetailVO.class,
				fieldNames, fieldTypes, sql.toString(), params);

	}
}

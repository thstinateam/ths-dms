package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Car;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.CarSOFilter;
import ths.dms.core.entities.vo.CarSOVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class CarDAOImpl implements CarDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Override
	public Car createCar(Car car, LogInfoVO logInfo) throws DataAccessException {
		if (car == null) {
			throw new IllegalArgumentException("car");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.create(car);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, car, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(Car.class, car.getId());
	}

	@Override
	public void deleteCar(Car car, LogInfoVO logInfo) throws DataAccessException {
		if (car == null) {
			throw new IllegalArgumentException("car");
		}
		repo.delete(car);
		try {
			actionGeneralLogDAO.createActionGeneralLog(car, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Car getCarById(long id) throws DataAccessException {
		return repo.getEntityById(Car.class, id);
	}
	
	@Override
	public void updateCar(Car car, LogInfoVO logInfo) throws DataAccessException {
		if (car == null) {
			throw new IllegalArgumentException("car");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		Car temp = this.getCarById(car.getId());
		repo.update(car);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, car, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public List<Car> getListCar(KPaging<Car> kPaging, Long shopId, String type,
			String carNumber, Long labelId, Float gross, String category, 
			String origin, ActiveType status, Boolean getCarInChildShop)
				throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		sql.append("select c.* from car c join shop s on c.shop_id = s.shop_id where 1 = 1");
		List<Object> params = new ArrayList<Object>();
		if (shopId != null) {
			if (Boolean.TRUE.equals(getCarInChildShop)) {
				sql.append(" and c.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
				params.add(shopId);
			}
			else {
				sql.append(" and c.shop_id=?");
				params.add(shopId);
			}
		}
		if (!StringUtility.isNullOrEmpty(type)) {
			sql.append(" and c.type=?");
			params.add(type);
		}
		if (!StringUtility.isNullOrEmpty(carNumber)) {
			sql.append(" and lower(c.car_number) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(carNumber.toLowerCase()));
		}
		if (labelId != null) {
			sql.append(" and c.label_id=?");
			params.add(labelId);
		}
		if (gross != null)  {
			sql.append("and c.gross=?");
			params.add(gross);
		}
		if (!StringUtility.isNullOrEmpty(category)) {
			sql.append("and c.category=?");
			params.add(category);
		}
		if (status != null) {
			sql.append(" and c.status=?");
			params.add(status.getValue());
		}
		else {
			sql.append(" and c.status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (!StringUtility.isNullOrEmpty(origin)) {
			sql.append(" and c.origin=?");
			params.add(origin);
		}
		sql.append(" order by s.shop_code, c.car_number"); 
		if (kPaging == null)
			return repo.getListBySQL(Car.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Car.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public Boolean isUsingByOthers(long carId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		
		sql.append(" select count(1) as count from car  ");
		sql.append(" where exists (select 1 from STOCK_TRANS where car_id=?) ");
		sql.append(" or exists (select 1 from SALE_ORDER where car_id=?) ");
		
		List<Object> params = new ArrayList<Object>();
		params.add(carId);
		params.add(carId);
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}
	
	@Override
	public Boolean checkIfRecordExists(String carNumber,
			Long id) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select count(*) as count from car where 1=1");
		if (!StringUtility.isNullOrEmpty(carNumber)) {
			sql.append(" and lower(car_number)=?");
			params.add(carNumber.toLowerCase());
		}
		if (id != null) {
			sql.append(" and car_id != ?");
			params.add(id);
		}
			
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}

	@Override
	public Car getCarByNumberAndShopId(String carNumber, Long shopId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from car where upper(car_number) = ? and shop_id = ?");
		params.add(carNumber.toUpperCase());
		params.add(shopId);
		return repo.getEntityBySQL(Car.class, sql.toString(), params);
	}
	
	@Override
	public Car getCarByNumberAndListShop(String carNumber, String lstShopId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from car where upper(car_number) = ? ");
		params.add(carNumber.toUpperCase());
		//fix loi nhieu nv, shop (lay ds car trong lst shop)
		String paramsFix = StringUtility.getParamsIdFixBugTooLongParams(lstShopId, "shop_id");
		sql.append(paramsFix);
		return repo.getEntityBySQL(Car.class, sql.toString(), params);
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<CarSOVO> getListCar(CarSOFilter filter) throws DataAccessException {
		if (filter == null) {
			throw new IllegalArgumentException("invalid parameters");
		}
		
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select c.car_id as id, c.car_number as carNumber");
		fromSql.append(" from car c");
		fromSql.append(" join shop s on (c.shop_id = s.shop_id)");
		fromSql.append(" where 1=1");
		
		if (filter.getShopId() != null) {
			if (Boolean.TRUE.equals(filter.getInChildShop())) {
				fromSql.append(" and c.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id)");
				params.add(filter.getShopId());
			} else {
				fromSql.append(" and c.shop_id = ?");
				params.add(filter.getShopId());
			}
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getType())) {
			fromSql.append(" and c.type = ?");
			params.add(filter.getType());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getCarNumber())) {
			fromSql.append(" and lower(c.car_number) like ? escape '/' ");
			String s = filter.getCarNumber().toLowerCase();
			s = StringUtility.toOracleSearchLike(s);
			params.add(s);
		}
		
		if (filter.getLabelId() != null) {
			fromSql.append(" and c.label_id = ?");
			params.add(filter.getLabelId());
		}
		
		if (filter.getGross() != null)  {
			fromSql.append(" and c.gross = ?");
			params.add(filter.getGross());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getCategory())) {
			fromSql.append(" and lower(c.category) = ?");
			String s = filter.getCategory().trim();
			s = s.toLowerCase();
			params.add(s);
		}
		
		if (filter.getStatus() != null) {
			fromSql.append(" and c.status = ?");
			params.add(filter.getStatus().getValue());
		} else {
			fromSql.append(" and c.status <> ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		if (!StringUtility.isNullOrEmpty(filter.getOrigin())) {
			fromSql.append(" and lower(c.origin) = ?");
			String s = filter.getOrigin().toLowerCase();
			s = s.trim();
			params.add(s);
		}
		
		sql.append(fromSql.toString());
		sql.append(" order by carNumber");
		
		String fieldNames[] = new String[] {
				"id", "carNumber"
		};
		
		Type fieldTypes[] = new Type[] {
				StandardBasicTypes.LONG, StandardBasicTypes.STRING
		};
		
		if (filter.getPaging() == null) {
			return repo.getListByQueryAndScalar(CarSOVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		StringBuilder countSql = new StringBuilder("select count(1) as count");
		countSql.append(fromSql.toString());
		return repo.getListByQueryAndScalarPaginated(CarSOVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getPaging());
	}
	
}

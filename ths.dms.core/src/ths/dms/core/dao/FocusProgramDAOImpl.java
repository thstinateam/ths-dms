package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.FocusProgram;
import ths.dms.core.entities.FocusShopMap;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.FocusProgramFilter;
import ths.dms.core.entities.enumtype.HTBHFilter;
import ths.dms.core.entities.enumtype.HTBHVO;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffRole;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class FocusProgramDAOImpl implements FocusProgramDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public FocusProgram createFocusProgram(FocusProgram focusProgram, LogInfoVO logInfo) throws DataAccessException {
		if (focusProgram == null) {
			throw new IllegalArgumentException("focusProgram");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (focusProgram.getFocusProgramCode() != null) {
			focusProgram.setFocusProgramCode(focusProgram.getFocusProgramCode().toUpperCase());
		}
		focusProgram.setCreateUser(logInfo.getStaffCode());
		focusProgram.setCreateDate(commonDAO.getSysDate());
		
		if (!StringUtility.isNullOrEmpty(focusProgram.getFocusProgramName())) {
			String s = focusProgram.getFocusProgramName().trim();
			s = s.toUpperCase();
			s = Unicode2English.codau2khongdau(s);
			focusProgram.setNameText(s);
		}
		
		repo.create(focusProgram);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, focusProgram, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(FocusProgram.class, focusProgram.getId());
	}

	@Override
	public void deleteFocusProgram(FocusProgram focusProgram, LogInfoVO logInfo) throws DataAccessException {
		if (focusProgram == null) {
			throw new IllegalArgumentException("focusProgram");
		}
		repo.delete(focusProgram);
		try {
			actionGeneralLogDAO.createActionGeneralLog(focusProgram, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public FocusProgram getFocusProgramById(long id) throws DataAccessException {
		return repo.getEntityById(FocusProgram.class, id);
	}
	
	@Override
	public void updateFocusProgram(FocusProgram focusProgram, LogInfoVO logInfo) throws DataAccessException {
		if (focusProgram == null) {
			throw new IllegalArgumentException("focusProgram");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		if (focusProgram.getFocusProgramCode() != null)
			focusProgram.setFocusProgramCode(focusProgram.getFocusProgramCode().toUpperCase());
		FocusProgram temp = this.getFocusProgramById(focusProgram.getId());
		focusProgram.setUpdateDate(commonDAO.getSysDate());
		focusProgram.setUpdateUser(logInfo.getStaffCode());
		
		if (!StringUtility.isNullOrEmpty(focusProgram.getFocusProgramName())) {
			String s = focusProgram.getFocusProgramName().trim();
			s = s.toUpperCase();
			s = Unicode2English.codau2khongdau(s);
			focusProgram.setNameText(s);
		}
		
		temp = temp.clone();
		repo.update(focusProgram);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, focusProgram, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public FocusProgram getFocusProgramByCode(String code) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from focus_program where focus_program_code = ? and status!=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		return repo.getEntityBySQL(FocusProgram.class, sql, params);
	}
	
	@Override
	public List<FocusProgram> getListFocusProgram(KPaging<FocusProgram> kPaging,FocusProgramFilter filter, StaffRole staffRole) throws DataAccessException {
		if (filter == null || filter.getShopRootId() == null || filter.getStaffRootId() == null || filter.getRollId() == null) {
			throw new DataAccessException("CMS paramanter is null");
		}
		if (filter.getFromDate() != null && filter.getToDate() != null && filter.getFromDate().after(filter.getToDate())) {
			return new ArrayList<FocusProgram>();
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("WITH rShop AS ");
		sql.append("   ( ");
		sql.append("     select shop_id from ( ");
		sql.append("       SELECT shop_id ");
		sql.append("       FROM shop ");
		sql.append("   where status in (?, ?) ");
				params.add(ActiveType.RUNNING.getValue());
				params.add(ActiveType.STOPPED.getValue());
		if (filter.getShopId() != null && filter.getShopId() > 0) {
			sql.append("       START WITH shop_id = ? ");
			params.add(filter.getShopId());
			sql.append("       CONNECT BY prior shop_id    = parent_shop_id ");
		}
		sql.append("   ) s1 join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s1.shop_id = ogr.number_key_id ");
		params.add(filter.getStaffRootId());
		params.add(filter.getRollId());
		params.add(filter.getShopRootId());
		sql.append("   ) ");
		sql.append(" select fp.* from ( ");
		
		sql.append(" SELECT fp.* ");
		sql.append("  FROM focus_program fp ");
		sql.append("   JOIN ");
		sql.append("     (SELECT focus_program_id ");
		sql.append("    FROM focus_shop_map fsm ");
		sql.append("    JOIN rShop sh ");
		sql.append("     ON fsm.shop_id              = sh.shop_id ");
		sql.append("    WHERE fsm.status <> ? ");
		params.add(ActiveType.DELETED.getValue());
		sql.append("     ) rm ON fp.focus_program_id = rm.focus_program_id ");
		sql.append("   WHERE 1                       =1 ");
		sql.append("   AND fp.status                 <> ? ");
		params.add(ActiveType.DELETED.getValue());
		if (filter.getFlagAll() != null && filter.getFlagAll()) {
			sql.append("  UNION ALL ");
			sql.append("  SELECT fp.* ");
			sql.append("  FROM focus_program fp ");
			sql.append("  WHERE fp.focus_program_id not in ( ");
			sql.append("       SELECT focus_program_id ");
			sql.append("      FROM focus_shop_map fsm ");
			sql.append("      WHERE fsm.status <> ? ");
			params.add(ActiveType.DELETED.getValue());
			sql.append("   ) ");
		}
		sql.append(" ) fp");
		sql.append(" where 1=1 ");
		if (!StringUtility.isNullOrEmpty(filter.getFocusProgramCode())) {
			sql.append(" and fp.focus_program_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getFocusProgramCode().toUpperCase().trim()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getFocusProgramName())) {
			sql.append(" and lower(fp.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getFocusProgramName().toLowerCase())).trim());
		}
		
		if (filter.getStatus() != null) {
			sql.append(" and fp.status = ? ");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and fp.status <> ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (filter.getFromDate() != null) {
			sql.append(" and ((fp.to_date is not null and fp.to_date >= trunc(?)) or fp.to_date is null) ");
			params.add(filter.getFromDate());
		}
		if (filter.getToDate() != null) {
			sql.append(" and ((fp.from_date is not null and fp.from_date < trunc(?) + 1) or fp.from_date is null) ");
			params.add(filter.getToDate());
		}
		sql.append(" order by fp.from_date desc, fp.to_date desc, fp.focus_program_code asc");
		if (kPaging == null) {
			return repo.getListBySQL(FocusProgram.class, sql.toString(), params);
		}
		return repo.getListBySQLPaginated(FocusProgram.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public int countFocusProgram(FocusProgramFilter filter) throws DataAccessException {
		if (filter == null || filter.getShopRootId() == null || filter.getStaffRootId() == null || filter.getRollId() == null) {
			throw new DataAccessException("CMS paramanter is null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("WITH rShop AS ");
		sql.append("   ( ");
		sql.append("     select shop_id from ( ");
		sql.append("       SELECT shop_id ");
		sql.append("       FROM shop ");
		sql.append("   where status in (?, ?) ");
				params.add(ActiveType.RUNNING.getValue());
				params.add(ActiveType.STOPPED.getValue());
		if (filter.getShopId() != null && filter.getShopId() > 0) {
			sql.append("       START WITH shop_id = ? ");
			params.add(filter.getShopId());
			sql.append("       CONNECT BY prior shop_id    = parent_shop_id ");
		}
		sql.append("   ) s1 join table (F_GET_LIST_CHILD_SHOP_INHERIT(?, sysdate, ?, ?)) ogr on s1.shop_id = ogr.number_key_id ");
		params.add(filter.getStaffRootId());
		params.add(filter.getRollId());
		params.add(filter.getShopRootId());
		sql.append("   ) ");
		sql.append(" select count(*) dem from ( ");
		sql.append(" SELECT fp.* ");
		sql.append("  FROM focus_program fp ");
		sql.append("  JOIN ");
		sql.append("    (SELECT focus_program_id ");
		sql.append("    FROM focus_shop_map fsm ");
		sql.append("    JOIN rShop sh ");
		sql.append("    ON fsm.shop_id              = sh.shop_id ");
		sql.append("    WHERE fsm.status <> ? ");
		params.add(ActiveType.DELETED.getValue());
		sql.append("    ) rm ON fp.focus_program_id = rm.focus_program_id ");
		sql.append("   WHERE 1                       = 1 ");
		sql.append("  AND fp.status                 <> ? ");
		params.add(ActiveType.DELETED.getValue());
		sql.append("  UNION ALL ");
		sql.append("   SELECT fp.* ");
		sql.append("  FROM focus_program fp ");
		sql.append("  WHERE fp.focus_program_id not in ( ");
		sql.append("  	SELECT focus_program_id ");
		sql.append("  	FROM focus_shop_map fsm ");
		sql.append("  	WHERE fsm.status <> ? ");
		params.add(ActiveType.DELETED.getValue());
		sql.append("   )) ");
		if (filter.getId() != null) {
			sql.append(" where focus_program_id = ? ");
			params.add(filter.getId());
		}
		BigDecimal kq = (BigDecimal) repo.getObjectByQuery(sql.toString(), params);
		if (kq != null) {
			return kq.intValue();
		}
		return 0;
	}
	
	@Override
	public List<FocusShopMap> getListFocusShopMapV2(KPaging<FocusShopMap> kPaging,
			String focusProgramCode, String focusProgramName, String shopCode,
			String shopName, Date fromDate, Date toDate, ActiveType status) throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<FocusShopMap>();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select fsm.* from focus_program fp join focus_shop_map fsm on fsm.focus_program_id = fp.focus_program_id");
		sql.append("	join shop s on s.shop_id = fsm.shop_id");
		sql.append(" 	where fsm.status != ?");
		params.add(ActiveType.DELETED.getValue());
		sql.append(" 	and fp.status != ?");
		params.add(ActiveType.DELETED.getValue());
		
		if (status != null) {
			sql.append(" and fp.status=?");
			params.add(status.getValue());
		}
		
		if (!StringUtility.isNullOrEmpty(focusProgramCode)) {
			sql.append(" and focus_program_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(focusProgramCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(focusProgramName)) {
			sql.append(" and lower(focus_program_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(focusProgramName.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and s.shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" and lower(s.shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shopName.toLowerCase()));
		}
		if (fromDate != null) {
			sql.append(" and ((fp.to_date is not null and fp.to_date >= trunc(?)) or fp.to_date is null)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and ((fp.from_date is not null and fp.from_date < trunc(?) + 1) or fp.from_date is null)");
			params.add(toDate);
		}
		sql.append(" order by fp.from_date desc, s.shop_code");
		if (kPaging == null)
			return repo.getListBySQL(FocusShopMap.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(FocusShopMap.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public Boolean isUsingByOthers(long focusProgramId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("select count(1) as count from focus_program");
		sql.append(" 	where exists (select 1 from focus_shop_map where focus_program_id=? and status != ?)");
		sql.append(" 		or exists (select 1 from focus_channel_map where focus_program_id=? and status != ?)");
		List<Object> params = new ArrayList<Object>();
		params.add(focusProgramId);
		params.add(ActiveType.DELETED.getValue());
		params.add(focusProgramId);
		params.add(ActiveType.DELETED.getValue());
		int c = repo.countBySQL(sql.toString(), params);
		return c==0?false:true;
	}

	@Override
	public List<HTBHVO> getListHTBHVO(HTBHFilter filter, KPaging<HTBHVO> kPaging) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT ap_param_id id,");
		sql.append("   ap_param_code code,");
		sql.append("   ap_param_name name,");
		sql.append("   NVL(");
		sql.append("   (SELECT 1");
		sql.append("   FROM focus_channel_map fc");
		sql.append("   WHERE fc.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append("   AND fc.sale_type_code   = ap_param_code");
		if (filter.getFocusProgramId() != null) {
			sql.append("   AND fc.focus_program_id =? ");
			params.add(filter.getFocusProgramId());
		}
		sql.append("   ),0) isExist");
		fromSql.append(" FROM ap_param");
		fromSql.append(" WHERE type = 'STAFF_SALE_TYPE'  ");
		if (!StringUtility.isNullOrEmpty(filter.getCode())) {
			fromSql.append(" and ap_param_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCode().toUpperCase().trim()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getName())) {
			fromSql.append(" and unicode2english(ap_param_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getName().toLowerCase()).trim()));
		}
		if (filter.getStatus() != null) {
			fromSql.append(" and status = ? ");
			params.add(filter.getStatus().getValue());
		} else {
			fromSql.append(" and status != ? ");
			params.add(ActiveType.DELETED.getValue());
		}
		if (Boolean.FALSE.equals(filter.getIsHO())) {
			fromSql.append(" and exists (SELECT 1 ");
			fromSql.append("   FROM focus_channel_map fc");
			fromSql.append("   WHERE fc.status = 1");
			fromSql.append("   AND fc.sale_type_code   = ap_param_code");
			if (filter.getFocusProgramId() != null) {
				fromSql.append("   AND fc.focus_program_id = ?");
				params.add(filter.getFocusProgramId());
			}
			fromSql.append(")");
		}
		sql.append(fromSql.toString());
		sql.append(" order by ap_param_code, ap_param_id");
		String[] fieldNames = { "id", "code", "name", "isExist" };
		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER };
		if (kPaging == null) {
			return repo.getListByQueryAndScalar(HTBHVO.class, fieldNames, fieldTypes, sql.toString(), params);
		}
		countSql.append("select count(*) as count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) ");
		return repo.getListByQueryAndScalarPaginated(HTBHVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}
	
	@Override
	public boolean hasChildShopJoinFocusProgram(Long focusProgramId, Long shopId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select count(1) as count ");
		sql.append(" from focus_program fp, focus_shop_map fs ");
		sql.append(" where fp.focus_program_id = fs.focus_program_id ");
		sql.append("     and fp.focus_program_id = ? ");
		params.add(focusProgramId);
		
		sql.append("     and fp.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		
		sql.append("     and fp.from_date < trunc(sysdate) + 1 ");
		sql.append("     and (fp.to_date >= trunc(sysdate) or fp.to_date is null) ");
		sql.append("     and fs.status = ? ");
		params.add(ActiveType.RUNNING.getValue());
		
		sql.append("     and fs.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
		params.add(shopId);

		return repo.countBySQLReturnBigDecimal(sql.toString(), params)
				.compareTo(new BigDecimal(0)) > 0 ? true : false;
	}
}

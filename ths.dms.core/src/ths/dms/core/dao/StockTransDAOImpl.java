package ths.dms.core.dao;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StockIssue;
import ths.dms.core.entities.StockIssueDetail;
import ths.dms.core.entities.StockLock;
import ths.dms.core.entities.StockTrans;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.SaleOrderStatus;
import ths.dms.core.entities.enumtype.SaleOrderStep;
import ths.dms.core.entities.enumtype.StockObjectType;
import ths.dms.core.entities.enumtype.StockTransLotOwnerType;
import ths.dms.core.entities.enumtype.StockTransType;
import ths.dms.core.entities.filter.StockStransFilter;
import ths.dms.core.entities.vo.StockTotalVO;
import ths.dms.core.entities.vo.StockTransVO;
import ths.core.entities.vo.rpt.RptBCXNKNVBHDetailVO;
import ths.core.entities.vo.rpt.RptBCXNKNVBHVO;
import ths.core.entities.vo.rpt.RptPXKKVCNB_DataConvert;
import ths.core.entities.vo.rpt.RptPXKKVCNB_Stock;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;


public class StockTransDAOImpl implements StockTransDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private CommonDAO commonDAO;
	
	@Autowired
	private ShopLockDAO shopLockDAO;

	@Override
	public StockTrans createStockTrans(StockTrans stockTrans) throws DataAccessException {
		if (stockTrans == null) {
			throw new IllegalArgumentException("stockTrans");
		}
		if (stockTrans.getStockTransCode() != null)
			stockTrans.setStockTransCode(stockTrans.getStockTransCode().toUpperCase());
		repo.create(stockTrans);
		return repo.getEntityById(StockTrans.class, stockTrans.getId());
	}
	
	@Override
	public StockIssue createStockIssue(StockIssue stockIssue) throws DataAccessException {
		if (stockIssue == null) {
			throw new IllegalArgumentException("stockIssue");
		}
		stockIssue.setCreateDate(commonDAO.getSysDate());
		repo.create(stockIssue);
		return repo.getEntityById(StockIssue.class, stockIssue.getId());
	}
	
	@Override
	public StockIssueDetail createStockIssueDetail(StockIssueDetail stockIssueDetail) throws DataAccessException {
		if (stockIssueDetail == null) {
			throw new IllegalArgumentException("stockIssueDetail");
		}
		stockIssueDetail.setCreateDate(commonDAO.getSysDate());
		repo.create(stockIssueDetail);
		return repo.getEntityById(StockIssueDetail.class, stockIssueDetail.getId());
	}

	@Override
	public void deleteStockTrans(StockTrans stockTrans) throws DataAccessException {
		if (stockTrans == null) {
			throw new IllegalArgumentException("stockTrans");
		}
		repo.delete(stockTrans);
	}

	@Override
	public StockTrans getStockTransById(long id) throws DataAccessException {
		return repo.getEntityById(StockTrans.class, id);
	}
	
	@Override
    public String generateStockTransCode(Long shopId) throws DataAccessException {
          List<Object> params = new ArrayList<Object>();
          Date lockDate  = shopLockDAO.getApplicationDate(shopId);
          String sql = "select count(stock_trans_id) as count from stock_trans where create_date >= TRUNC(?)";
          params.add(lockDate);
          Integer val = repo.countBySQL(sql, params);
          if (val == null)
                val = 1;
          else
                val++;
          DateFormat format = new SimpleDateFormat("ddMMyy");
          //return format.format(commonDAO.getSysDate()) + String.valueOf(val);
          return format.format(lockDate) + String.valueOf(val);
    }

	
	@Override
	public StockTrans getStockTransByCode(String code) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from stock_trans where stock_trans_code=?";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		return repo.getEntityBySQL(StockTrans.class, sql, params);
	}
	
	@Override
	public StockTrans getStockTransByCodeAndShop(String code, Long shopId) throws DataAccessException {
		code = code.toUpperCase();
		String sql = "select * from stock_trans where stock_trans_code=? and shop_id=? ";
		List<Object> params = new ArrayList<Object>();
		params.add(code.toUpperCase());
		params.add(shopId);
		return repo.getEntityBySQL(StockTrans.class, sql, params);
	}
	
	@Override
	public void updateStockTrans(StockTrans stockTrans) throws DataAccessException {
		if (stockTrans == null) {
			throw new IllegalArgumentException("stockTrans");
		}
		if (stockTrans.getStockTransCode() != null)
			stockTrans.setStockTransCode(stockTrans.getStockTransCode().toUpperCase());
		repo.update(stockTrans);
	}
	
	@Override
	public Boolean isUsingByOthers(long stockTransId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select count(1) as count from stock_trans  ");
		sql.append(" where  ");
		sql.append("   exists (select 1 from stock_trans_detail where stock_trans_id=?) ");
		
		List<Object> params = new ArrayList<Object>();
		params.add(stockTransId);
		int c = repo.countBySQL(sql.toString(), params);
		return c == 0 ? false : true;
	}
	
	@Override
	public List<StockTransVO> getListStockTransVO(KPaging<StockTransVO> kPaging, 
			Date fromDate, Date toDate, Long shopId, StockTransType stockTransType) 
					throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<StockTransVO>();
		
		StringBuilder sql = new StringBuilder();
		StringBuilder selectSql = new StringBuilder();
		selectSql.append(" select stock_trans_code as stockTransCode, from_owner_type as fromOwnerType, ");
		selectSql.append("	to_owner_type as toOwnerType, stock_trans_date as stockTransDate, ");
		selectSql.append("	case from_owner_type ");
		selectSql.append("		when 1 then (select shop_code from shop where shop_id=from_owner_id) ");
		selectSql.append("		when 2 then (select staff_code from staff where staff_id=from_owner_id) ");
		selectSql.append("		else null ");
		selectSql.append("	end as fromStockCode, ");
		selectSql.append("	case to_owner_type ");
		selectSql.append("		when 1 then (select shop_code from shop where shop_id=to_owner_id) ");
		selectSql.append("		when 2 then (select staff_code from staff where staff_id=to_owner_id) ");
		selectSql.append("		else null ");
		selectSql.append("	end as toStockCode, ");
		selectSql.append("  total_amount as amount");
		
		StringBuilder countSql = new StringBuilder();
		countSql.append(" select count(1) as count ");
		
		
		sql.append(" from stock_trans where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		if (fromDate != null) {
			sql.append(" and stock_trans_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and stock_trans_date < (trunc(?) + 1)");
			params.add(toDate);
		}
		if (shopId != null) {
			sql.append(" and shop_id = ?");
			params.add(shopId);
		}
		if (stockTransType != null) {
//			if (shopId != null) {
//				sql.append(" and ((from_owner_id=? and from_owner_type=?) or (to_owner_id=? and to_owner_type=?))");
//				params.add(shopId);
//				params.add(StockObjectType.SHOP.getValue());
//				params.add(shopId);
//				params.add(StockObjectType.SHOP.getValue());
//			}
			
			if (stockTransType == StockTransType.IMPORT_FROM_STAFF) {
				sql.append(" and from_owner_type=? and to_owner_type=?");
				params.add(StockObjectType.STAFF.getValue());
				params.add(StockObjectType.SHOP.getValue());
			}
			else if (stockTransType == StockTransType.EXPORT_TO_STAFF) {
				sql.append(" and from_owner_type=? and to_owner_type=?");
				params.add(StockObjectType.SHOP.getValue());
				params.add(StockObjectType.STAFF.getValue());
			}
			else if (stockTransType == StockTransType.IMPORT_MODIFIED) {	
				sql.append(" and (from_owner_type is null)");
			}
			else if (stockTransType == StockTransType.EXPORT_MODIFIED) {	
				sql.append(" and (to_owner_type is null)");
			}
		}
		
		selectSql.append(sql);
		countSql.append(sql);
		selectSql.append(" order by stock_trans_date desc");
		/*System.out.println(TestUtil.addParamToQuery(selectSql.toString(), params));*/
		String[] fieldNames = {"stockTransCode", "fromOwnerType", "toOwnerType", "stockTransDate", "fromStockCode", "toStockCode", "amount"};
		Type[] fieldTypes = {StandardBasicTypes.STRING, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.TIMESTAMP, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.BIG_DECIMAL};
		
		if (kPaging != null)
			return repo.getListByQueryAndScalarPaginated(StockTransVO.class, fieldNames, fieldTypes, selectSql.toString(), countSql.toString(), params, params, kPaging);
		else
			return repo.getListByQueryAndScalar(StockTransVO.class, fieldNames, fieldTypes, selectSql.toString(), params);
	}
	
	@Override
	public List<StockTrans> getListStockTrans(List<Long> lstId) throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from STOCK_TRANS where 1 != 1");
		for (Long saleOrderId : lstId) {
			sql.append(" or STOCK_TRANS_ID = ?");
			params.add(saleOrderId);
		}
		return repo.getListBySQL(StockTrans.class, sql.toString(), params);
	}
	
	/**
	 * @author
	 */
	@Override
	public List<StockTrans> getListStockTrans(KPaging<StockTrans> kPaging, 
			Date fromDate, Date toDate, Long shopId, StockTransType stockTransType) 
					throws DataAccessException {

		if (fromDate != null && toDate != null && fromDate.after(toDate))
			return new ArrayList<StockTrans>();
		
		StringBuilder sql = new StringBuilder();
		sql.append(" select st.*");
		sql.append(" from stock_trans st");
		sql.append(" join stock_trans_lot stl on (stl.stock_trans_id = st.stock_trans_id)");
		sql.append(" where 1=1");
		
		List<Object> params = new ArrayList<Object>();
		if (fromDate != null) {
			sql.append(" and st.stock_trans_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append(" and st.stock_trans_date < (trunc(?) + 1)");
			params.add(toDate);
		}
		if (shopId != null) {
			sql.append(" and ((stl.from_owner_id in (select warehouse_id from warehouse where shop_id = ?)");
			params.add(shopId);
			sql.append(" and stl.from_owner_type=?)");
			params.add(StockObjectType.SHOP.getValue());
			sql.append(" or (stl.to_owner_id in (select warehouse_id from warehouse where shop_id = ?)");
			params.add(shopId);
			sql.append(" and stl.to_owner_type=?))");
			params.add(StockObjectType.SHOP.getValue());
		}
		if (stockTransType != null) {
			if (stockTransType == StockTransType.IMPORT_FROM_STAFF) {
				sql.append(" and stl.from_owner_type=? and stl.to_owner_type=?");
				params.add(StockObjectType.STAFF.getValue());
				params.add(StockObjectType.SHOP.getValue());
			}
			else if (stockTransType == StockTransType.EXPORT_TO_STAFF) {
				sql.append(" and stl.from_owner_type=? and stl.to_owner_type=?");
				params.add(StockObjectType.SHOP.getValue());
				params.add(StockObjectType.STAFF.getValue());
			}
			else if (stockTransType == StockTransType.IMPORT_MODIFIED) {	
				sql.append(" and (stl.from_owner_type is null)");
			}
			else if (stockTransType == StockTransType.EXPORT_MODIFIED) {	
				sql.append(" and (stl.to_owner_type is null)");
			}
		}
		sql.append(" order by STOCK_TRANS_CODE asc");
		
		if (kPaging != null) {
			return repo.getListBySQLPaginated(StockTrans.class, sql.toString(), params, kPaging);
		}
		return repo.getListBySQL(StockTrans.class, sql.toString(), params);
	}
	
	@Override
	public List<StockTrans> getListStockTransByFromId(Long fromId)  throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select st.*");
		sql.append(" from stock_trans st");
		sql.append(" where 1=1");
		if(fromId != null){
			sql.append(" and from_stock_trans_id = ? ");
			params.add(fromId);
		}
		
		return repo.getListBySQL(StockTrans.class, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.StockTransDAO#getListStockTransWithCondition(ths.dms.core.entities.enumtype.StockObjectType, ths.dms.core.entities.enumtype.StockObjectType, java.lang.Long, java.util.Date)
	 */
	@Override
	public List<StockTrans> getListStockTransWithCondition(
			KPaging<StockTrans> kPaging, StockObjectType fromOwnerType,
			StockObjectType toOwnerType, Long staffId, Date stockTransDate,
			Long fromOwnerId, Long toOwnerId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append(" select * from stock_trans where 1=1 ");
		List<Object> params = new ArrayList<Object>();
		if (fromOwnerType != null) {
			sql.append(" and from_owner_type = ?");
			params.add(fromOwnerType.getValue());
		}
		if (toOwnerType != null) {
			sql.append(" and to_owner_type = ?");
			params.add(toOwnerType.getValue());
		}
		if (fromOwnerId != null) {
			sql.append(" and from_owner_id = ?");
			params.add(fromOwnerId);
		}
		if (toOwnerId != null) {
			sql.append(" and to_owner_id = ?");
			params.add(toOwnerId);
		}
		if (staffId != null) {
			sql.append(" and staff_id = ?");
			params.add(staffId);
		}
		if (stockTransDate != null) {
			sql.append(" and stock_trans_date >= trunc(?) and stock_trans_date < trunc(?) + 1");
			params.add(stockTransDate);
			params.add(stockTransDate);
		}
		sql.append(" order by stock_trans_code");
		if (kPaging != null)
			return repo.getListBySQLPaginated(StockTrans.class, sql.toString(), params, kPaging);
		else
			return repo.getListBySQL(StockTrans.class, sql.toString(), params);
	}
	
	@Override
	public List<RptBCXNKNVBHVO> getBCXNKNVBH(Long shopId, List<Long> lstStaffId, Date fromDate, Date toDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from (");
		sql.append(" select st.stock_trans_id stockTransId, ");
		sql.append("	(select nvl(staff_name,'') || '-' || nvl(staff_code,'') from staff ");
		sql.append("		where staff_id = case when st.to_owner_type = ? then st.TO_OWNER_ID");
		params.add(StockObjectType.STAFF.getValue());
		sql.append("							when st.from_owner_type = ? then st.from_OWNER_ID end");
		params.add(StockObjectType.STAFF.getValue());
		sql.append("	) staffInfo, ");
		
		sql.append("	st.stock_trans_code stockTransCode, ");
		sql.append("	st.stock_trans_date stockTransDate,");
		sql.append("	(select sum(nvl(quantity, 0) * nvl(price, 0)) from stock_trans_detail ");
		sql.append("		where stock_trans_id = st.stock_trans_id) amount");
		
		sql.append(" from stock_trans st where ((from_owner_type = 1 and to_owner_type = 2) or ");
		sql.append(" 	(from_owner_type = 2 and to_owner_type = 1))");
		
//		if (shopId != null) {
//			sql.append(" and st.shop_id = ?");
//			params.add(shopId);
//		}
		
		if (shopId != null) {
			sql.append(" AND ( (st.to_owner_type = ? and exists (select 1 from staff s1 where s1.staff_id = st.TO_OWNER_ID and s1.shop_id = ?)) ");
			params.add(StockObjectType.STAFF.getValue());
			params.add(shopId);
			sql.append(" OR (st.from_owner_type = ? and exists (select 1 from staff s1 where s1.staff_id = st.FROM_OWNER_ID and s1.shop_id = ?)) ) ");
			params.add(StockObjectType.STAFF.getValue());
			params.add(shopId);
			sql.append(" and st.shop_id = ?");
			params.add(shopId);
		}
		
		if (lstStaffId != null && lstStaffId.size() != 0 && lstStaffId.get(0) != null) {
			sql.append(" and (");
			for (Long staffId: lstStaffId) {
				if (staffId != null) {
					sql.append(" (st.to_owner_type = ? and st.TO_OWNER_ID = ?) ");
					params.add(StockObjectType.STAFF.getValue());
					params.add(staffId); 
					sql.append(" OR (st.from_owner_type = ? and st.FROM_OWNER_ID = ?) OR ");
					params.add(StockObjectType.STAFF.getValue());
					params.add(staffId);
				}
			}
			sql.append(" 0 = 1)");
		}
		if (fromDate != null) {
			sql.append("	and st.stock_trans_date >= trunc(?)");
			params.add(fromDate);
		}
		if (toDate != null) {
			sql.append("	and st.stock_trans_date < trunc(?) + 1");
			params.add(toDate);
		}
		sql.append(" ) order by stockTransDate, staffInfo, stockTransCode");
		
		
		String[] fieldNames = {"stockTransId", "staffInfo", "stockTransCode", "amount", "stockTransDate"};
		Type[] fieldTypes = {StandardBasicTypes.LONG,
							StandardBasicTypes.STRING,
							StandardBasicTypes.STRING,
							StandardBasicTypes.BIG_DECIMAL,
							StandardBasicTypes.DATE};
		List<RptBCXNKNVBHVO> rs = repo.getListByQueryAndScalar(RptBCXNKNVBHVO.class, fieldNames, fieldTypes, sql.toString(), params);
		
		//for (RptBCXNKNVBHVO vo: rs) {
		//	vo.setLstDetail(this.getRptBCXNKNVBHDetailVO(vo.getStockTransId()));
		//}
		
		return rs;
	}
	
	private List<RptBCXNKNVBHDetailVO> getRptBCXNKNVBHDetailVO(Long stockTransId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select ");
		sql.append(" pr.product_code productCode, ");
		sql.append(" pr.product_name productName, ");
		sql.append("  std.price price,");
		sql.append("  stl.lot lot,");
		sql.append(" pr.convfact convfact, ");
		sql.append("  sum(case when lot is not null then stl.quantity else std.quantity end) quantity");
		
		sql.append("	from stock_trans_detail std left join stock_trans_lot stl ");
		sql.append("	on std.stock_trans_detail_id = stl.stock_trans_detail_id");
		sql.append("	join product pr on pr.product_id = std.product_id");
		sql.append("	where std.stock_trans_id = ?");
		params.add(stockTransId);
		sql.append("	group by pr.product_code, pr.product_name, pr.convfact, std.price, stl.lot");
		sql.append("	order by pr.product_code, stl.lot");
		
		
		String[] fieldNames = {"productCode", "productName", "price", "lot", "convfact", "quantity"};
		Type[] fieldTypes = {StandardBasicTypes.STRING,
							StandardBasicTypes.STRING,
							StandardBasicTypes.BIG_DECIMAL,
							StandardBasicTypes.STRING,
							StandardBasicTypes.INTEGER,
							StandardBasicTypes.INTEGER};
//		System.out.println(TestUtil.addParamToQuery(sql.toString(), params));
		List<RptBCXNKNVBHDetailVO> rs = repo.getListByQueryAndScalar(RptBCXNKNVBHDetailVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return rs;
	}

	@Override
	public List<StockTrans> getListStockTrans(String staffCode, Long shopID)
	{
		List<StockTrans> lsStockTrans = null;
		try	{
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			
			sql.append(" SELECT st.* ");
			sql.append(" FROM STOCK_TRANS st, STAFF stff  ");
			sql.append(" WHERE st.TO_OWNER_ID = stff.STAFF_ID ");
			sql.append(" AND (st.FROM_OWNER_TYPE = 1 OR st.FROM_OWNER_TYPE = 2) ");
			sql.append(" AND stff.SHOP_ID = ? ");			
			params.add(shopID);
			
			sql.append(" AND stff.STAFF_CODE like ? ESCAPE '/' ");	
			params.add(StringUtility.toOracleSearchLikeSuffix(staffCode));
			List<StockTrans> ss = repo.getListBySQL(StockTrans.class, sql.toString(), params);
			return ss; 		
		} catch(Exception ex) {
			System.out.println("error: " + ex.getMessage());
		}
		return lsStockTrans;
	}

	@Override
	public List<Staff> getListStaffTrans(Long shopId) 
	{
		List<Staff> lstStaff = null;
		try
		{
			StringBuilder sql = new StringBuilder();
			List<Object> params = new ArrayList<Object>();
			
			sql.append(" SELECT stff.* FROM STAFF stff ");
			sql.append(" JOIN CHANNEL_TYPE ct ");
			sql.append(" ON stff.STAFF_TYPE_ID = ct.CHANNEL_TYPE_ID ");
			sql.append(" AND (ct.OBJECT_TYPE = 1 OR OBJECT_TYPE = 2) ");
			sql.append(" AND ct.TYPE = 2 ");
			
			sql.append(" AND stff.SHOP_ID = ? ");
			params.add(shopId);
			System.out.println(sql.toString());
			lstStaff = repo.getListBySQL(Staff.class, sql.toString(), params);
			
		} catch(Exception ex)		{
			System.out.println(ex.getMessage());
		}
		return lstStaff;
	}
	
	@Override
	public String generateStockTransCodeVansale(Long shopId, String transType) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		String sql = "select count(stock_trans_id) as count from stock_trans where trans_type = ? and shop_id = ?";
		params.add(transType);
		params.add(shopId);
		Integer val = repo.countBySQL(sql, params);
		if (val == null) {
			val = 1;
		} else {
			val++;
		}
		String result = val.toString();
		String temp = "";
		for (int i = 0; i < 6 - result.length(); i++) {
			temp += "0";
		}
		return transType + temp + result;
	}

	@Override
	public int checkStockTrans(Long shopId, Date lockDate)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("SELECT count(1) as count from stock_trans ");
		sql.append("where from_owner_type = 1 ");
		sql.append("and from_owner_id = ? ");
		params.add(shopId);
		sql.append("AND stock_trans_date >= TRUNC(?) ");
		sql.append("AND stock_trans_date  < TRUNC(?) + 1 ");
		params.add(lockDate);
		params.add(lockDate);
		return repo.countBySQL(sql.toString(),params);
	}

	@Override
	public int checkStockTransVansale(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" with tmp as (select st.object_id, st.product_id, sum(st.quantity) ");
		sql.append(" from stock_total st ");
		sql.append(" join staff sf on st.object_id = sf.staff_id ");
		sql.append(" join channel_type ct on sf.staff_type_id = ct.channel_type_id ");
		sql.append(" where 1=1 ");
		sql.append(" and ct.type = 2 ");
		sql.append(" and ct.status = 1 ");
		sql.append(" and ct.channel_type_code = 'NVBH_VAN' ");
		sql.append(" and sf.shop_id = ? ");
		params.add(shopId);
		sql.append(" and st.object_type in (2, 4) ");
		sql.append(" and st.quantity > 0 ");
		sql.append(" group by st.object_id, st.product_id) ");
		sql.append(" select count(*) as count from tmp ");
		
		return repo.countBySQL(sql.toString(),params);
	}

	@Override
	public String getStockTransNotYetReturn(Long shopId)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append(" select listagg(staff_code, ', ') within group(order by staff_code) as nv ");
		sql.append(" from (select distinct(sf.staff_code) ");
		sql.append(" from stock_total st ");
		sql.append(" join staff sf on st.object_id = sf.staff_id ");
		sql.append(" join channel_type ct on sf.staff_type_id = ct.channel_type_id ");
		sql.append(" where 1=1 ");
		sql.append(" and ct.type = 2 ");
		sql.append(" and ct.status = 1 ");
		sql.append(" and ct.channel_type_code = 'NVBH_VAN' ");
		sql.append(" and sf.shop_id = ? ");
		params.add(shopId);
		sql.append(" and st.object_type in (2, 4) ");
		sql.append(" and st.quantity > 0 ");
		sql.append(" group by st.object_id, sf.staff_code, st.product_id) ");
		
		return (String) repo.getObjectByQuery(sql.toString(), params);
	}

	@Override
	public RptPXKKVCNB_Stock getPXKKVCNB(Long shopId, String staffCode,
			Long stockTransId, String transacName, String transacContent)
			throws DataAccessException {
		// TODO Auto-generated method stub
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		
		sql.append(" SELECT ");
		sql.append(" st.CREATE_DATE AS ngay_hien_tai, ");
		sql.append(" stff.STAFF_NAME AS ten_nguoi_van_chuyen, ");
		sql.append(" stff.ADDRESS dia_chi_nhan_vien, ");
		sql.append(" shop.SHOP_NAME AS don_vi_ban_hang, ");
		sql.append(" shop.TAX_NUM AS ma_so_thue, ");
		sql.append(" shop.ADDRESS AS dia_chi_kho, ");
		sql.append(" shop.PHONE AS so_dien_thoai, ");
		sql.append(" shop.INVOICE_NUMBER_ACCOUNT ");
		sql.append(" || ' - ' ");
		sql.append(" || shop.INVOICE_BANK_NAME AS so_tai_khoan , ");
		sql.append(" car.CAR_NUMBER AS so_xe, ");
		sql.append(" pd.PRODUCT_CODE AS ma_san_pham, ");
		sql.append(" pd.PRODUCT_NAME ten_san_pham, ");
		sql.append(" TRUNC(std.QUANTITY / pd.CONVFACT) AS so_thung, ");
		sql.append(" std.QUANTITY - TRUNC(std.QUANTITY / pd.CONVFACT) * pd.CONVFACT AS so_le , ");
		sql.append(" std.PRICE AS don_gia, ");
		sql.append(" std.PRICE * std.QUANTITY AS thanh_tien ");
		sql.append(" FROM STOCK_TRANS st ");
		sql.append(" LEFT JOIN ( ");
		sql.append(" SELECT stff.staff_id, stff.staff_name, stff.address ");
		sql.append(" FROM STAFF stff ");
		sql.append(" JOIN CHANNEL_TYPE ct ");
		sql.append(" ON stff.STAFF_TYPE_ID = ct.CHANNEL_TYPE_ID ");
		sql.append(" AND (ct.OBJECT_TYPE = 1 OR ct.OBJECT_TYPE = 2) ");
		sql.append(" AND ct.TYPE = 2 ");
		sql.append(" ) stff ON st.TO_OWNER_ID = stff.STAFF_ID ");
		sql.append(" JOIN SHOP shop ");
		sql.append(" ON shop.SHOP_ID = st.SHOP_ID ");
		sql.append(" LEFT JOIN CAR car ");
		sql.append(" ON car.CAR_ID = st.CAR_ID ");
		sql.append(" JOIN STOCK_TRANS_DETAIL std ");
		sql.append(" ON std.STOCK_TRANS_ID = st.STOCK_TRANS_ID ");
		sql.append(" JOIN PRODUCT pd ");
		sql.append(" ON std.PRODUCT_ID = pd.PRODUCT_ID ");
		sql.append(" WHERE 1= 1 ");
		sql.append(" AND st.FROM_OWNER_TYPE = 1 ");
		sql.append(" AND (st.to_owner_type IS NULL OR st.TO_OWNER_TYPE = 2) ");
		sql.append(" AND st.STOCK_TRANS_ID = ? ");
		params.add(stockTransId);
		if(shopId != null){
			sql.append(" AND shop.SHOP_ID = ? ");
			params.add(shopId);
		}
		
		String[] fieldNames = new String[] {
				"ngay_hien_tai",
				"ten_nguoi_van_chuyen",
				"dia_chi_nhan_vien",
				"don_vi_ban_hang",
				"ma_so_thue",
				"dia_chi_kho",
				"so_dien_thoai",
				"so_tai_khoan",
				"so_xe",
				"ma_san_pham",
				"ten_san_pham",
				"so_thung",
				"so_le",
				"don_gia",
				"thanh_tien"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.DATE,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL, 
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.BIG_DECIMAL
		};
		
		List<RptPXKKVCNB_DataConvert> list =  repo.getListByQueryAndScalar(RptPXKKVCNB_DataConvert.class, fieldNames, fieldTypes, sql.toString(), params);
		if(list!=null && list.size() > 0) {
			RptPXKKVCNB_Stock result = new RptPXKKVCNB_Stock(list, transacName, transacContent);
			return result;
		}
		return null;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public String getListStockTransStaffId(long shopId, Date date) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select listagg(to_owner_id,';') within group (order by to_owner_id) as staffIds");
		sql.append(" from (");
		sql.append(" select stl.to_owner_id from stock_trans st");
		sql.append(" join stock_trans_lot stl on (stl.stock_trans_id = st.stock_trans_id)");
		sql.append(" where stl.from_owner_type = ?");
		params.add(StockObjectType.SHOP.getValue());
		sql.append(" and stl.from_owner_id in (select warehouse_id from warehouse where shop_id = ?)");
		params.add(shopId);
		
		if (date != null) {
			sql.append(" and st.stock_trans_date >= trunc(?)");
			params.add(date);
			sql.append(" and st.stock_trans_date < trunc(?) + 1");
			params.add(date);
		}
		
		sql.append(" and stl.to_owner_type = ?");
		params.add(StockObjectType.STAFF.getValue());
		sql.append(" group by stl.to_owner_id");
		sql.append(")");
		
		Object o = repo.getObjectByQuery(sql.toString(), params);
		if (o != null) {
			return o.toString();
		}
		return null;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public String getListStaffLockStockYet(String lstStaffId, Date date) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(lstStaffId)) {
			throw new IllegalArgumentException("invalid staff ids");
		}
		lstStaffId = lstStaffId.trim();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select listagg(staff_code,', ') within group (order by staff_code) as staffIds");
		sql.append(" from staff st");
		sql.append(" where 1=1");
		//sql.append(" --and shop_id = 6");
		sql.append(" and staff_id in (select regexp_substr(?,'[^;]+', 1, level)");
		params.add(lstStaffId);
		sql.append(" from dual");
		sql.append(" connect by regexp_substr(?, '[^;]+', 1, level) is not null)");
		params.add(lstStaffId);
		sql.append(" and exists (");
		sql.append(" select 1 from stock_lock");
		sql.append(" where van_lock = 0");
		//sql.append(" --and shop_id = st.shop_id");
		sql.append(" and staff_id = st.staff_id");
		
		if (date != null) {
			sql.append(" and create_date >= trunc(?)");
			params.add(date);
			sql.append(" and create_date < trunc(?) + 1");
			params.add(date);
		}
		sql.append(" )");
		
		Object o = repo.getObjectByQuery(sql.toString(), params);
		if (o != null) {
			return o.toString();
		}
		return null;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public String getListStaffStockReturnYet(String lstStaffId) throws DataAccessException {
		if (StringUtility.isNullOrEmpty(lstStaffId)) {
			throw new IllegalArgumentException("invalid staff ids");
		}
		lstStaffId = lstStaffId.trim();
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select listagg(staff_code,', ') within group (order by staff_code) as staffIds");
		sql.append(" from staff st");
		sql.append(" where 1=1");
		//sql.append(" --and shop_id = 6");
		sql.append(" and staff_id in (select regexp_substr(?,'[^;]+', 1, level)");
		params.add(lstStaffId);
		sql.append(" from dual");
		sql.append(" connect by regexp_substr(?, '[^;]+', 1, level) is not null)");
		params.add(lstStaffId);
		sql.append(" and exists (");
		sql.append(" select 1 from stock_total");
		sql.append(" where object_type = ? and object_id = st.staff_id");
		params.add(StockObjectType.STAFF.getValue());
		sql.append(" and approved_quantity <> 0");
		sql.append(" )");
		
		Object o = repo.getObjectByQuery(sql.toString(), params);
		if (o != null) {
			return o.toString();
		}
		return null;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public List<StockTotalVO> getListProductDPByStaff(long shopId, long staffId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		Date lockDate = shopLockDAO.getApplicationDate(shopId);
		
		/*sql.append("with priceTmp1 as (");
		sql.append(" select price_id, price, product_id, orderNo");
		sql.append(" from price pr");
		sql.append(" join (select shop_id, (case ct.object_type when ? then 1 when ? then 1");
		params.add(ShopObjectType.NPP.getValue());
		params.add(ShopObjectType.NPP_KA.getValue());
		sql.append(" when ? then 2 when ? then 2");
		params.add(ShopObjectType.VUNG.getValue());
		params.add(ShopObjectType.VUNG_KA.getValue());
		sql.append(" when ? then 3 when ? then 3 when ? then 3");
		params.add(ShopObjectType.MIEN.getValue());
		params.add(ShopObjectType.MIEN_ST.getValue());
		params.add(ShopObjectType.MIEN_KA.getValue());
		sql.append(" when ? then 4 when ? then 4 when ? then 4");
		params.add(ShopObjectType.GT.getValue());
		params.add(ShopObjectType.ST.getValue());
		params.add(ShopObjectType.KA.getValue());
		sql.append(" when ? then 5 else 8 end)as orderNo");
		params.add(ShopObjectType.VNM.getValue());
		sql.append(" from shop s");
		sql.append(" join channel_type ct on (ct.channel_type_id = s.shop_type_id)");
		sql.append(" where s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" start with shop_id = ? connect by prior parent_shop_id = shop_id");
		params.add(shopId);
		sql.append(" ) sh on (sh.shop_id = pr.shop_id)");
		sql.append(" where status in (?, ?)");
		params.add(ActiveType.STOPPED.getValue());
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and from_date < trunc(?) + 1");
		params.add(lockDate);
		sql.append(" and (to_date >= trunc(?) or to_date is null)");
		params.add(lockDate);
		sql.append(" and customer_type_id is null");
		sql.append(" union all");
		sql.append(" select price_id, price, product_id, (case when shop_channel is not null then 6 else 7 end) as orderNo");
		sql.append(" from price pr");
		sql.append(" where status in (?, ?)");
		params.add(ActiveType.STOPPED.getValue());
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" and from_date < trunc(?) + 1");
		params.add(lockDate);
		sql.append(" and (to_date >= trunc(?) or to_date is null)");
		params.add(lockDate);
		sql.append(" and customer_type_id is null");
		sql.append(" and shop_id is null");
		sql.append(" and shop_channel in (select shop_channel from shop where shop_id = ?)");
		params.add(shopId);
		sql.append(" ),");
		sql.append(" priceTmp as (");
		sql.append(" select price_id, price, product_id");
		sql.append(" from priceTmp1 pr1");
		sql.append(" where orderNo <= (select min(orderNo) from priceTmp1 where product_id = pr1.product_id)");
		sql.append(" )");*/

		sql.append(" select p.product_id as productId, p.product_code as productCode, p.product_name as productName,");
		sql.append(" p.gross_weight as grossWeight, p.uom1, p.convfact, stt.approved_quantity as quantity,");
		sql.append(" wh.warehouse_id as warehouseId, wh.warehouse_name as warehouseName,");
		sql.append(" nvl(pr.price, 0) as price, wh.seq");
		sql.append(" from product p");
//		sql.append(" join priceTmp pr on (pr.product_id = p.product_id)");//
		sql.append(" LEFT JOIN PRICE_SHOP_DEDUCED pr ON pr.product_id = p.product_id and pr.SHOP_ID = ? ");
		params.add(shopId);
		sql.append("                           and pr.FROM_DATE < TRUNC(?) + 1 ");
		params.add(lockDate);
		sql.append("                           AND (pr.TO_DATE >= TRUNC(?) OR pr.TO_DATE IS NULL) ");
		params.add(lockDate);
		sql.append(" join stock_total stt on (stt.object_type = ? and stt.product_id = p.product_id");
		params.add(StockObjectType.STAFF.getValue());
		sql.append(" and stt.object_id = ? and stt.approved_quantity > 0)");
		params.add(staffId);
		sql.append(" join stock_total s_stt on (s_stt.object_type = ? and s_stt.object_id = ? and s_stt.product_id = p.product_id)");
		params.add(StockObjectType.SHOP.getValue());
		params.add(shopId);
		sql.append(" join warehouse wh on (wh.warehouse_id = s_stt.warehouse_id)");		
		sql.append(" order by productCode, seq");
		
		String[] fieldNames = new String[] {
				"productId",
				"productCode",
				"productName",
				"grossWeight",
				"uom1",
				"convfact",
				"quantity",
				"warehouseId",
				"warehouseName",
				"price",
				"seq"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.FLOAT,
				StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER
		};
		
		List<StockTotalVO> lst = repo.getListByQueryAndScalar(StockTotalVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}
	
	/**
	 * @author tungmt
	 */
	@Override
	public List<StockTransVO> getListDPByStaff(StockStransFilter filter) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		Date lockDate = commonDAO.getSysDate();
		if(filter.getShopId()!=null){
			lockDate = shopLockDAO.getApplicationDate(filter.getShopId());
		}
		
		sql.append(" select distinct st.stock_trans_id stockTransId, st.stock_trans_code stockTransCode ");
		sql.append(" from stock_trans st ");
		sql.append(" join stock_trans_detail std on std.stock_trans_id = st.stock_trans_id ");
		sql.append(" join stock_trans_lot stl on stl.stock_trans_detail_id = std.stock_trans_detail_id ");
		sql.append(" where 1=1 ");
		if(filter.getToOwnerID() != null){
			sql.append(" and stl.to_owner_type = ? and stl.to_owner_id = ?");
			params.add(StockTransLotOwnerType.STAFF.getValue());
			params.add(filter.getToOwnerID());
		}
		sql.append(" and stl.from_owner_type = ? ");
		params.add(StockTransLotOwnerType.WAREHOUSE.getValue());
//		sql.append(" and std.stock_trans_date >= trunc(?) and std.stock_trans_date < trunc(?) + 1");
//		params.add(lockDate);
//		params.add(lockDate);
		if(filter.getApproved()!=null){
			sql.append(" and st.APPROVED = ? ");
			params.add(filter.getApproved().getValue());
		}
		if(filter.getApprovedStep() != null){
			sql.append(" and st.APPROVED_STEP = ? ");
			params.add(filter.getApprovedStep().getValue());
		}
		sql.append(" order by st.stock_trans_code ");
		
		String[] fieldNames = new String[] {
				"stockTransId","stockTransCode"
		};
		
		Type[] fieldTypes = new Type[] {
				StandardBasicTypes.LONG,StandardBasicTypes.STRING
		};
		
		List<StockTransVO> lst = repo.getListByQueryAndScalar(StockTransVO.class, fieldNames, fieldTypes, sql.toString(), params);
		return lst;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public String getListStaffStockReturnYet(long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select listagg(staff_code,', ') within group (order by staff_code) as staffIds");
		sql.append(" from staff st");
		sql.append(" where shop_id = ?");
		params.add(shopId);
		sql.append(" and exists (select 1 from stock_total");
		sql.append(" where object_type = ? and approved_quantity <> 0");
		params.add(StockObjectType.STAFF.getValue());
		sql.append(" and object_id = st.staff_id)");
		
		Object o = repo.getObjectByQuery(sql.toString(), params);
		if (o != null) {
			return o.toString();
		}
		return null;
	}

	/**
	 * @author lacnv1
	 */
	@Override
	public boolean checkExistsGOStockTrans(long staffId, Date pDate) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select count(1) as count");
		sql.append(" from stock_trans st");
		sql.append(" where trans_type = ?");
		params.add("GO");
		sql.append(" and exists (select 1 from stock_trans_lot");
		sql.append(" where stock_trans_id = st.stock_trans_id and from_owner_type = ? and from_owner_id = ?)");
		params.add(StockTransLotOwnerType.STAFF.getValue());
		params.add(staffId);
		if (pDate != null) {
			sql.append(" and stock_trans_date >= trunc(?) and stock_trans_date < trunc(?) + 1");
			params.add(pDate);
			params.add(pDate);
		}
		sql.append(" and st.APPROVED = ? and st.APPROVED_STEP = ? ");
		params.add(SaleOrderStatus.APPROVED.getValue());
		params.add(SaleOrderStep.PRINT_CONFIRMED.getValue());
		
		Integer c = repo.countBySQL(sql.toString(), params);
		return (c > 0);
	}
	
	/**
	 * @author lacnv1
	 */
	@Override
	public boolean checkExistsGOStockTransNotApproves(long staffId, String type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select count(1) as count");
		sql.append(" from stock_trans st");
		sql.append(" inner join stock_trans_lot stl");
		sql.append(" on st.stock_trans_id=stl.stock_trans_id");
		sql.append(" where st.trans_type IN (?)");		
		params.add(type);
		sql.append(" and st.APPROVED_STEP NOT IN (?)");
		params.add("3");
		if ("GO".equals(type)) {
			sql.append(" and stl.from_owner_id=?");
		} else if ("DP".equals(type)) { 
			sql.append(" and stl.to_owner_id=?");
		}
		params.add(staffId);
		
		Integer c = repo.countBySQL(sql.toString(), params);
		return (c > 0);
	}

	
	
	@Override
	public String getMaxMTStockTranCode(String type) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from( select mt_stock_tran_code from mt_stock_tran ");
		sql.append(" where 1=1 ");
		if(!StringUtility.isNullOrEmpty(type)){
			sql.append(" and type = ? ");
			params.add(type);
		}
		sql.append(" and mt_stock_tran_date >= trunc(sysdate) and mt_stock_tran_date < trunc(sysdate) + 1  order by mt_stock_tran_id desc ");
		sql.append(" ) where rownum = 1 ");
		
		
		Object st = repo.getObjectByQuery(sql.toString(), params);
		if(st!=null){
			return st.toString();
		}else{
			return "";
		}
	}
	
	/**
	 * @author hoanv25
	 */
	@Override
	public List<StockTransVO> getListStockTransVOFilter(StockStransFilter filter) throws DataAccessException {
		if (filter == null) {
			return null;
		}
		final int STOCK_APPROVED = 1; // da cap nhat kho
		final int STOCK_APPROVE_YET = 0; // chua cap nhat kho
		final int STOCK_CANCELLED = 3; // da huy
		
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		sql.append("select stockTransId, stockTransCode, shopCode, shopName,");
		sql.append(" fromOwnerType, ");
		sql.append(" toOwnerType, ");
		sql.append(" stockTransDate, ");
		sql.append(" fromStockCode, ");
		sql.append(" toStockCode, ");
		sql.append(" amount ");
		sql.append(",status");
		sql.append(" from ( ");
		sql.append(" SELECT str.stock_trans_id as stockTransId, str.stock_trans_code AS stockTransCode,");
		sql.append(" sh.shop_code as shopCode, sh.shop_name as shopName,");
		sql.append(" strl.from_owner_type AS fromOwnerType, ");
		sql.append(" strl.to_owner_type AS toOwnerType, ");
		sql.append(" str.stock_trans_date AS stockTransDate, ");
		sql.append(" CASE from_owner_type ");
		sql.append(" WHEN 1 ");
		sql.append(" THEN ");
		sql.append(" (SELECT warehouse_code FROM warehouse WHERE warehouse_id=from_owner_id)");
		sql.append(" WHEN 2 ");
		sql.append(" THEN ");
		sql.append(" (SELECT staff_code FROM staff WHERE staff_id=from_owner_id)");
		sql.append(" ELSE NULL ");
		sql.append(" END AS fromStockCode, ");
		sql.append(" CASE to_owner_type ");
		sql.append(" WHEN 1 ");
		sql.append(" THEN ");
		sql.append(" (SELECT warehouse_code FROM warehouse WHERE warehouse_id=to_owner_id)");
		sql.append(" WHEN 2 ");
		sql.append(" THEN ");
		sql.append(" (SELECT staff_code FROM staff WHERE staff_id=to_owner_id)");
		sql.append(" ELSE NULL ");
		sql.append(" END AS toStockCode, ");
		sql.append(" str.total_amount AS amount,");
		sql.append(" (case when str.approved = ? then ? when str.approved = ? and str.approved_step >= ? then ? else ? end) as status,");
		params.add(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM.getValue());
		params.add(STOCK_CANCELLED);
		params.add(SaleOrderStatus.APPROVED.getValue());
		params.add(SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT.getValue());
		params.add(STOCK_APPROVED);
		params.add(STOCK_APPROVE_YET);
		sql.append(" row_number() over (partition by str.stock_trans_id order by str.stock_trans_id) as row_num ");
		sql.append(" FROM stock_trans str ");
		sql.append(" JOIN stock_trans_lot strl ON strl.stock_trans_id = str.stock_trans_id ");
		sql.append(" join shop sh on (sh.shop_id = str.shop_id)");
		sql.append(" WHERE 1 = 1 ");		
		if (filter.getStartTemp() != null) {
			sql.append(" and str.stock_trans_date >= trunc(?)");
			params.add(filter.getStartTemp());
		}
		if (filter.getEndTemp() != null) {
			sql.append(" and str.stock_trans_date< (trunc(?) + 1)");
			params.add(filter.getEndTemp());
		}
		if(filter.getFromOwnerType() == null && filter.getToOwnerType() == null){
			// luc nay la lay tat ca
		} else {
			if(filter.getFromOwnerType() == null){
				sql.append(" AND from_owner_type is null ");
			/*	params.add(filter.getFromOwnerType());*/
			}else {
				sql.append(" AND from_owner_type = ? ");
				params.add(filter.getFromOwnerType());
			}
			if(filter.getToOwnerType() == null) {
				sql.append(" AND to_owner_type  is null ");
			/*	params.add(filter.getToOwnerType());*/
			} else {
				sql.append(" AND to_owner_type  =? ");
				params.add(filter.getToOwnerType());
			}
		}
		if (filter.getShopId() != null || filter.getLstShopId() != null) {
			sql.append(" and str.shop_id in (");
			sql.append("select distinct shop_id from shop");
			sql.append(" where status = ?");
			params.add(ActiveType.RUNNING.getValue());
			if (filter.getShopId() != null) {
				sql.append(" start with shop_id = ?");
				params.add(filter.getShopId());
			} else {
				sql.append(" start with shop_id in (-1");
				for (Long idT : filter.getLstShopId()) {
					sql.append(",?");
					params.add(idT);
				}
				sql.append(")");
			}
			sql.append(" and status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(" connect by prior shop_id = parent_shop_id and status = ?");
			params.add(ActiveType.RUNNING.getValue());
			sql.append(")");
		}
		
		if (filter.getStatus() != null) {
			if (STOCK_CANCELLED == filter.getStatus()) {
				sql.append(" and str.approved = ?");
				params.add(SaleOrderStatus.TABLET_CREATED_AND_REQUIRED_CONFIRM.getValue()); // huy
			} else if (STOCK_APPROVED == filter.getStatus()) {
				sql.append(" and str.approved = ? and str.approved_step >= ?");
				params.add(SaleOrderStatus.APPROVED.getValue());
				params.add(SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT.getValue());
			} else {
				sql.append(" and str.approved in (?, ?) and str.approved_step < ?");
				params.add(SaleOrderStatus.NOT_YET_APPROVE.getValue());
				params.add(SaleOrderStatus.APPROVED.getValue());
				params.add(SaleOrderStep.DEBIT_CONFIRMED_AND_STOCK_OUT.getValue());
			}
		}
		
		sql.append(" ) ");
		sql.append(" where row_num = 1 ");

		String[] fieldNames = {
				"stockTransId",
				"stockTransCode",
				"shopCode", "shopName",
				"fromOwnerType",
				"toOwnerType",
				"stockTransDate",
				"fromStockCode",
				"toStockCode",
				"amount",
				"status"
		};
		Type[] fieldTypes = {
				StandardBasicTypes.LONG,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING, StandardBasicTypes.STRING,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.INTEGER,
				StandardBasicTypes.TIMESTAMP,
				StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,
				StandardBasicTypes.BIG_DECIMAL,
				StandardBasicTypes.INTEGER
		};
		
		if (filter.getkPagingStockTransVO() != null) {
			StringBuilder countSql = new StringBuilder();
			countSql.append(" select count(1) as count from ( ");
			countSql.append(sql);
			countSql.append(" ) ");
			sql.append(" ORDER BY shopCode, stockTransDate DESC ");
			return repo.getListByQueryAndScalarPaginated(StockTransVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPagingStockTransVO());
		}
		sql.append(" ORDER BY shopCode, stockTransDate DESC ");
		return repo.getListByQueryAndScalar(StockTransVO.class, fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public StockLock createStockTransLock(StockLock stl) throws DataAccessException {
		if (stl == null) {
			throw new IllegalArgumentException("stl is null");
		}
		return repo.create(stl);
	}
}

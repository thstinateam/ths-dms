package ths.dms.core.dao;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.TrainingPlanDetail;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.exceptions.DataAccessException;

public interface TrainingPlanDetailDAO {

	TrainingPlanDetail createTrainingPlanDetail(TrainingPlanDetail trainingPlanDetail) throws DataAccessException;

	void deleteTrainingPlanDetail(TrainingPlanDetail trainingPlanDetail) throws DataAccessException;

	void updateTrainingPlanDetail(TrainingPlanDetail trainingPlanDetail) throws DataAccessException;
	
	TrainingPlanDetail getTrainingPlanDetailById(long id) throws DataAccessException;
	
	TrainingPlanDetail getTrainingPlanDetailBy(String superCode,
			String staffCode, Date date) throws DataAccessException;
	
	List<TrainingPlanDetail> getListTrainingPlanDetailByStaffId(KPaging<TrainingPlanDetail> kPaging, Long staffId, Date date)
	throws DataAccessException;
	
	int countTrainingPlanDetail(Long trainingPlanId) throws DataAccessException;
	
	/**
	 * Cap nhat TrainingPlanDetail khi xoa hoac sua nhan vien trong cay don vi.
	 */
	void updateTrainingPlanDetailStatusToDelete(Staff staff, Long shopId)throws DataAccessException;
}

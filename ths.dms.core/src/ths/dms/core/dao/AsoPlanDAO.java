/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.AsoPlan;
import ths.dms.core.entities.vo.AsoPlanVO;
import ths.dms.core.entities.vo.RoutingVO;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.CommonFilter;

/**
 * AsoPlanDAO phan bo Aso
 * @author vuongmq
 * @since 19/10/2015 
 */
public interface AsoPlanDAO {
	/**
	 * @author vuongmq
	 * @param kPaging
	 * @param filter
	 * @return List<AsoPlan>
	 * @throws DataAccessException
	 * @since 20/10/2015
	 */
	List<AsoPlan> getListAsoByFilter(CommonFilter filter) throws DataAccessException;

	/**
	 * @author vuongmq
	 * @param filter
	 * @return AsoPlanVO
	 * @throws DataAccessException
	 * @since 20/10/2015
	 */
	AsoPlanVO getAsoPlanVOByFilter(CommonFilter filter) throws DataAccessException;

	/**
	 * @author vuongmq
	 * @param filter
	 * @return List<AsoPlanVO>
	 * @throws DataAccessException
	 * @since 20/10/2015
	 */
	List<AsoPlanVO> getListAsoPlanVOByFilter(CommonFilter filter) throws DataAccessException;

	/**
	 * @author vuongmq
	 * @param filter
	 * @return List<RoutingVO>
	 * @throws DataAccessException
	 * @since 20/10/2015
	 */
	List<RoutingVO> getRoutingVOByFilter(CommonFilter filter) throws DataAccessException;
}

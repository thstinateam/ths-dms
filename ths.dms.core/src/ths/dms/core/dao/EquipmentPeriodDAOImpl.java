package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.enumtype.EquipPeriodType;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;
/**
 * EquipmentPeriodDAO
 * 
 * @author nhutnn
 * @since 15/12/2014
 * @description: Lop DAO quan ly ky
 */
public class EquipmentPeriodDAOImpl implements EquipmentPeriodDAO {
	@Autowired
	private IRepository repo;
	
	@Override
	public EquipPeriod getEquipPeriodCurrent() throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * ");
		sql.append(" from equip_period eq ");
		sql.append(" where 1=1 and status = ?");
		params.add(EquipPeriodType.OPENED.getValue());
		sql.append(" and trunc(eq.from_date) <= trunc(sysdate)");
		sql.append(" and trunc(sysdate) < trunc(eq.to_date) + 1 ");
		
		return repo.getEntityBySQL(EquipPeriod.class, sql.toString(), params);
	}
	
}

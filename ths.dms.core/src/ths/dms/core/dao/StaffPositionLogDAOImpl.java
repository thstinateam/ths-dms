package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.StaffPositionLog;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.enumtype.TrainningPlanDetailStatus;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.entities.vo.SuperVisorInfomationVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class StaffPositionLogDAOImpl implements StaffPositionLogDAO {
	@Autowired
	private IRepository repo;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public StaffPositionLog createStaffPositionLog(
			StaffPositionLog staffPositionLog, LogInfoVO logInfo)
			throws DataAccessException {
		if (staffPositionLog == null) {
			throw new IllegalArgumentException("staffPositionLog");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.create(staffPositionLog);
		staffPositionLog = repo.getEntityById(StaffPositionLog.class,
				staffPositionLog.getId());
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, staffPositionLog,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return staffPositionLog;
	}

	@Override
	public void deleteStaffPositionLog(StaffPositionLog staffPositionLog,
			LogInfoVO logInfo) throws DataAccessException {
		if (staffPositionLog == null) {
			throw new IllegalArgumentException("staffPositionLog");
		}
		repo.delete(staffPositionLog);
		try {
			actionGeneralLogDAO.createActionGeneralLog(staffPositionLog, null,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public StaffPositionLog getStaffPositionLogById(long id)
			throws DataAccessException {
		return repo.getEntityById(StaffPositionLog.class, id);
	}

	@Override
	public void updateStaffPositionLog(StaffPositionLog staffPositionLog,
			LogInfoVO logInfo) throws DataAccessException {
		if (staffPositionLog == null) {
			throw new IllegalArgumentException("staffPositionLog");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		StaffPositionLog temp = this.getStaffPositionLogById(staffPositionLog
				.getId());
		repo.update(staffPositionLog);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, staffPositionLog,
					logInfo);
		} catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.StaffPositionLogDAO#getInfomationOfParentSuper(java.lang.Long)
	 */
	@Override
	public List<SuperVisorInfomationVO> getInfomationOfParentSuper(
			Long parentStaffId) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT tb1.lng   AS lng,");
		sql.append(" tb1.lat        AS lat,");
		sql.append(" tb1.accuracy   AS accuracy,");
		sql.append(" tb1.create_date   AS lastDatePosition,");
		sql.append(" tb2.staff_id   AS staffId,");
		sql.append(" tb2.staff_code AS staffCode");
		sql.append(" FROM");
		sql.append(" (SELECT ? as area_manager_id, lng, lat, accuracy, create_date");
		params.add(parentStaffId);
		sql.append(" FROM staff_position_log");
		sql.append(" WHERE staff_id  = ?");
		params.add(parentStaffId);
		sql.append(" AND create_date = (SELECT MAX(create_date) FROM staff_position_log WHERE staff_id = ?)");
		params.add(parentStaffId);
		sql.append(" ) tb1");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT tp.staff_id, tpd.area_manager_id,");
		sql.append(" s.staff_code");
		sql.append(" FROM training_plan_detail tpd,");
		sql.append(" training_plan tp,");
		sql.append(" staff s");
		sql.append(" WHERE tpd.training_plan_id = tp.training_plan_id");
		sql.append(" AND tpd.area_manager_id    = ?");
		params.add(parentStaffId);
		sql.append(" AND tp.staff_id            = s.staff_id");
		sql.append(" AND tpd.training_date     >= TRUNC(sysdate)");
		sql.append(" AND tpd.training_date      < TRUNC(sysdate + 1)");
		sql.append(" AND tp.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND tpd.status <> ?");
		params.add(TrainningPlanDetailStatus.CANCLED.getValue());
		sql.append(" ) tb2");
		sql.append(" ON tb1.area_manager_id = tb2.area_manager_id");
		
		String[] fieldNames = { "lng", // 0
				"lat", // 1
				"accuracy", // 2
				"lastDatePosition", // 2
				"staffId", // 2
				"staffCode", // 3
		};

		Type[] fieldTypes = { StandardBasicTypes.BIG_DECIMAL, // 0
				StandardBasicTypes.BIG_DECIMAL, // 1
				StandardBasicTypes.BIG_DECIMAL, // 2
				StandardBasicTypes.TIMESTAMP, // 2
				StandardBasicTypes.LONG, // 2
				StandardBasicTypes.STRING, // 3
		};

		return repo.getListByQueryAndScalar(SuperVisorInfomationVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.StaffPositionLogDAO#getListStaffPosition(java.lang.Long, ths.dms.core.entities.enumtype.StaffObjectType)
	 */
	@Override
	public List<StaffPositionVO> getListStaffPosition(Long ownerId,
			StaffObjectType objectType) throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT tb1.staff_id AS staffId,");
		sql.append(" tb1.staff_code    AS staffCode,");
		sql.append(" tb1.staff_name    AS staffName,");
		sql.append(" tb2.lat           AS lat,");
		sql.append(" tb2.lng           AS lng,");
		sql.append(" tb2.accuracy      AS accuracy,");
		sql.append(" tb2.create_date      AS lastDatePosition");
		sql.append(" FROM");
		sql.append(" (SELECT s.staff_id,");
		sql.append(" s.staff_code,");
		sql.append(" s.staff_name");
		sql.append(" FROM staff s, channel_type ct");
		sql.append(" WHERE s.staff_type_id = ct.channel_type_id");
		sql.append(" AND s.staff_owner_id  = ?");
		params.add(ownerId);
		sql.append(" AND ct.object_type    = ?");
		params.add(objectType.getValue());
		sql.append(" AND ct.type           = ?");
		params.add(ChannelTypeType.STAFF.getValue());
		sql.append(" AND ct.status         = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND s.status          = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ) tb1");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT *");
		sql.append(" FROM staff_position_log");
		sql.append(" WHERE staff_position_log_id IN");
		sql.append(" (SELECT MAX(staff_position_log_id) as staff_position_log_id");
		sql.append(" FROM staff_position_log");
		sql.append(" GROUP BY staff_id");
		sql.append(" )) tb2");
		sql.append(" ON tb1.staff_id = tb2.staff_id");
		
		String[] fieldNames = { "staffId", // 0
				"staffCode", // 1
				"staffName", // 2
				"lat", // 2
				"lng", // 3
				"accuracy", // 4
				"lastDatePosition", // 5
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.BIG_DECIMAL, // 2
				StandardBasicTypes.BIG_DECIMAL, // 3
				StandardBasicTypes.BIG_DECIMAL, // 4
				StandardBasicTypes.TIMESTAMP, // 4
		};

		return repo.getListByQueryAndScalar(StaffPositionVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.StaffPositionLogDAO#getInfomationOfSuper(java.lang.Long)
	 */
	@Override
	public List<SuperVisorInfomationVO> getInfomationOfSuper(Long superStaffId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT tb1.lng   AS lng,");
		sql.append(" tb1.lat        AS lat,");
		sql.append(" tb1.accuracy   AS accuracy,");
		sql.append(" tb1.create_date   AS lastDatePosition,");
		sql.append(" tb2.staff_id   AS staffId,");
		sql.append(" tb2.staff_code AS staffCode");
		sql.append(" FROM");
		sql.append(" (SELECT staff_id, lng, lat, accuracy, create_date");
		sql.append(" FROM staff_position_log");
		sql.append(" WHERE staff_id  = ?");
		params.add(superStaffId);
		sql.append(" AND create_date = (SELECT MAX(create_date) FROM staff_position_log WHERE staff_id = ?)");
		params.add(superStaffId);
		sql.append(" ) tb1");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT tp.staff_id as super_id,");
		sql.append(" tpd.staff_id,");
		sql.append(" s.staff_code");
		sql.append(" FROM training_plan_detail tpd,");
		sql.append(" training_plan tp,");
		sql.append(" staff s");
		sql.append(" WHERE s.staff_id        = tpd.staff_id");
		sql.append(" AND tpd.training_date     >= TRUNC(sysdate)");
		sql.append(" AND tpd.training_date      < TRUNC(sysdate + 1)");
		sql.append(" AND tp.training_plan_id = tpd.training_plan_id");
		sql.append(" AND tp.staff_id         = ?");
		params.add(superStaffId);
		sql.append(" AND tp.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND s.status = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND tpd.status <> ?");
		params.add(TrainningPlanDetailStatus.CANCLED.getValue());
		sql.append(" ) tb2");
		sql.append(" ON tb1.staff_id = tb2.super_id");
		
		String[] fieldNames = { "lng", // 0
				"lat", // 1
				"accuracy", // 2
				"lastDatePosition", // 2
				"staffId", // 2
				"staffCode", // 3
		};

		Type[] fieldTypes = { StandardBasicTypes.BIG_DECIMAL, // 0
				StandardBasicTypes.BIG_DECIMAL, // 1
				StandardBasicTypes.BIG_DECIMAL, // 2
				StandardBasicTypes.TIMESTAMP, // 2
				StandardBasicTypes.LONG, // 2
				StandardBasicTypes.STRING, // 3
		};

		return repo.getListByQueryAndScalar(SuperVisorInfomationVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.StaffPositionLogDAO#getInfomationOfStaff(java.lang.Long)
	 */
	@Override
	public List<SuperVisorInfomationVO> getInfomationOfStaff(Long staffId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT tb1.staff_id as staffId, tb1.lat as lat, tb1.lng as lng, tb1.accuracy as accuracy, tb1.create_date as lastDatePosition,");
		sql.append(" tb3.day_amount_plan as plan, tb3.day_amount as amount, tb3.point1 as pointInRouting, tb3.point2 as pointAmount");
		sql.append(" FROM");
		sql.append(" (SELECT spl.staff_id,");
		sql.append(" spl.lng,");
		sql.append(" spl.lat,");
		sql.append(" spl.accuracy,");
		sql.append(" spl.create_date");
		sql.append(" FROM staff_position_log spl,");
		sql.append(" staff s");
		sql.append(" WHERE spl.staff_id  = ?");
		params.add(staffId);
		sql.append(" AND spl.staff_id    = s.staff_id");
		sql.append(" AND s.status        = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND spl.create_date =");
		sql.append(" (SELECT MAX(create_date) FROM staff_position_log WHERE staff_id = ?)");
		params.add(staffId);
		sql.append(" ) tb1");
		sql.append(" LEFT JOIN");
		sql.append(" ( SELECT point1, point2, staff_id, day_amount, day_amount_plan FROM rpt_staff_sale WHERE staff_id = ?");
		params.add(staffId);
		sql.append(" and trunc(create_date, 'month') = trunc(sysdate, 'month')");
		sql.append(" ) tb3");
		sql.append(" ON tb1.staff_id  = tb3.staff_id");
		
		String[] fieldNames = { "staffId", // 0
				"lat", // 1
				"lng", // 2
				"accuracy", // 3
				"lastDatePosition", // 3
				"plan", // 4
				"amount", // 5
				"pointInRouting", // 6
				"pointAmount", // 7
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.BIG_DECIMAL, // 1
				StandardBasicTypes.BIG_DECIMAL, // 2
				StandardBasicTypes.BIG_DECIMAL, // 3
				StandardBasicTypes.TIMESTAMP, // 3
				StandardBasicTypes.BIG_DECIMAL, // 4
				StandardBasicTypes.BIG_DECIMAL, // 5
				StandardBasicTypes.INTEGER, // 6
				StandardBasicTypes.INTEGER, // 7
		};
		
		return repo.getListByQueryAndScalar(SuperVisorInfomationVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.StaffPositionLogDAO#getListSuperPosition(java.lang.Long)
	 */
	@Override
	public List<StaffPositionVO> getListSuperPosition(Long staffId)
			throws DataAccessException {
		// TODO Auto-generated method stub
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("SELECT tb1.staff_id AS staffId,");
		sql.append(" tb1.staff_code    AS staffCode,");
		sql.append(" tb1.staff_name    AS staffName,");
		sql.append(" tb2.lat           AS lat,");
		sql.append(" tb2.lng           AS lng,");
		sql.append(" tb2.accuracy      AS accuracy,");
		sql.append(" tb2.create_date      AS lastDatePosition");
		sql.append(" FROM");
		sql.append(" (SELECT s.staff_id,");
		sql.append(" s.staff_code,");
		sql.append(" s.staff_name");
		sql.append(" FROM staff s, channel_type ct");
		sql.append(" WHERE s.staff_type_id = ct.channel_type_id");
		sql.append(" AND s.shop_id  in (select shop_id from shop start with shop_id = (");
		sql.append(" select shop_id from staff where staff_id = ?)");
		params.add(staffId);
		sql.append(	" connect by prior shop_id = parent_shop_id and status = ?) ");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND ct.object_type    = ?");
		params.add(StaffObjectType.NVGS.getValue());
		sql.append(" AND ct.type           = ?");
		params.add(ChannelTypeType.STAFF.getValue());
		sql.append(" AND ct.status         = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" AND s.status          = ?");
		params.add(ActiveType.RUNNING.getValue());
		sql.append(" ) tb1");
		sql.append(" LEFT JOIN");
		sql.append(" (SELECT *");
		sql.append(" FROM staff_position_log");
		sql.append(" WHERE staff_position_log_id IN");
		sql.append(" (SELECT MAX(staff_position_log_id) as staff_position_log_id");
		sql.append(" FROM staff_position_log");
		sql.append(" GROUP BY staff_id");
		sql.append(" )) tb2");
		sql.append(" ON tb1.staff_id = tb2.staff_id");
		
		String[] fieldNames = { "staffId", // 0
				"staffCode", // 1
				"staffName", // 2
				"lat", // 2
				"lng", // 3
				"accuracy", // 4
				"lastDatePosition", // 5
		};

		Type[] fieldTypes = { StandardBasicTypes.LONG, // 0
				StandardBasicTypes.STRING, // 1
				StandardBasicTypes.STRING, // 2
				StandardBasicTypes.BIG_DECIMAL, // 2
				StandardBasicTypes.BIG_DECIMAL, // 3
				StandardBasicTypes.BIG_DECIMAL, // 4
				StandardBasicTypes.TIMESTAMP, // 4
		};

		return repo.getListByQueryAndScalar(StaffPositionVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	/* (non-Javadoc)
	 * @see ths.dms.core.dao.StaffPositionLogDAO#getLastStaffPositionLog(java.lang.Long)
	 */
	@Override
	public StaffPositionLog getLastStaffPositionLog(Long staffId, Date date, String fromTime, String toTime) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT *");
		sql.append(" FROM staff_position_log");
		sql.append(" WHERE staff_position_log_id =");
		sql.append(" (SELECT MAX(staff_position_log_id) as staff_position_log_id");
		sql.append(" FROM staff_position_log");
		sql.append(" WHERE staff_id = ? and create_date >= trunc(?) and create_date < trunc(? + 1)");
		params.add(staffId);
		params.add(date);
		params.add(date);
		if (!StringUtility.isNullOrEmpty(fromTime) && !StringUtility.isNullOrEmpty(toTime)) {
			sql.append(" AND TO_CHAR(create_date, 'HH24:MI') >= ? ");
			params.add(fromTime);
			sql.append(" AND TO_CHAR(create_date, 'HH24:MI') <= ? ) ");
			params.add(toTime);
		} else {
			sql.append(" ) ");
		}
		return repo.getEntityBySQL(StaffPositionLog.class, sql.toString(), params);
	}
	
	@Override
	public StaffPositionLog getNewestStaffPositionLog(Long staffId, Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" SELECT * ");
		sql.append(" FROM staff_position_log ");
		sql.append(" WHERE 1 = 1 ");
		if (shopId != null) {
			sql.append(" and shop_id = ? ");
			params.add(shopId);
		}
		if (staffId != null) {
			sql.append(" AND staff_id = ? ");
			params.add(staffId);
			sql.append(" and staff_position_log_id = ");
			sql.append(" (select max(staff_position_log_id) from staff_position_log ");
			sql.append(" where staff_id = ? and create_date >= trunc(sysdate)) ");
			params.add(staffId);
		}
		sql.append(" AND rownum = 1 ");
		return repo.getEntityBySQL(StaffPositionLog.class, sql.toString(), params);
	}
}
package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.ShopProduct;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ShopProductType;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class ShopProductDAOImpl implements ShopProductDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	CommonDAO commonDAO;

	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Override
	public ShopProduct createShopProduct(ShopProduct shopProduct, LogInfoVO logInfo) throws DataAccessException {
		if (shopProduct == null) {
			throw new IllegalArgumentException("shopProduct");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		shopProduct.setCreateUser(logInfo.getStaffCode());
		repo.create(shopProduct);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, shopProduct, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(ShopProduct.class, shopProduct.getId());
	}

	@Override
	public void deleteShopProduct(ShopProduct shopProduct, LogInfoVO logInfo) throws DataAccessException {
		if (shopProduct == null) {
			throw new IllegalArgumentException("shopProduct");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.delete(shopProduct);
		try {
			actionGeneralLogDAO.createActionGeneralLog(shopProduct, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public ShopProduct getShopProductById(long id) throws DataAccessException {
		return repo.getEntityById(ShopProduct.class, id);
	}
	
	@Override
	public ShopProduct getShopProductByCat(Long shopId, Long catId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from SHOP_PRODUCT sc where 1=1");
		sql.append(" and sc.status !=?");
		params.add(ActiveType.DELETED.getValue());
		if (catId != null) {
			sql.append(" and sc.cat_id=?");
			params.add(catId);
		}
		if (shopId != null) {
			sql.append(" and sc.shop_id=?");
			params.add(shopId);
		}
		return repo.getFirstBySQL(ShopProduct.class, sql.toString(), params);
	}
	
	@Override
	public ShopProduct getShopProductByProduct(Long shopId, Long productId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("select * from SHOP_PRODUCT sc where 1=1");
		sql.append(" and sc.status !=?");
		params.add(ActiveType.DELETED.getValue());
		if (productId != null) {
			sql.append(" and sc.product_id=?");
			params.add(productId);
		}
		if (shopId != null) {
			sql.append(" and sc.shop_id=?");
			params.add(shopId);
		}
		return repo.getFirstBySQL(ShopProduct.class, sql.toString(), params);
	}
	
	@Override
	public void updateShopProduct(ShopProduct shopProduct, LogInfoVO logInfo) throws DataAccessException {
		if (shopProduct == null) {
			throw new IllegalArgumentException("shopProduct");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		ShopProduct temp = this.getShopProductById(shopProduct.getId());
		shopProduct.setUpdateDate(commonDAO.getSysDate());
		shopProduct.setUpdateUser(logInfo.getStaffCode());
		repo.update(shopProduct);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, shopProduct, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public List<ShopProduct> getListShopProduct(KPaging<ShopProduct> kPaging, String shopCode,
							String shopName, Long catId, String productCode, ActiveType status,
							ShopProductType type,  List<Long> listOrgAccess) 
									throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with listOrg as(");
		sql.append(" select shop_id from shop start with shop_id in (-1");
		if (listOrgAccess != null && listOrgAccess.size() != 0) {
			for (Long parentId: listOrgAccess) {
				sql.append(",?");
				params.add(parentId);
			}
		}
		sql.append(" ) connect by prior shop_id = parent_shop_id");
		sql.append(" )");
		
		sql.append("select sc.* from SHOP_PRODUCT sc ");
		if (ShopProductType.PRODUCT.equals(type)){
			sql.append("  join product p on sc.product_id = p.product_id ");
			sql.append("  join product_info pi on pi.product_info_id = p.cat_id  ");
			sql.append(" where 1 = 1 and p.status = 1");
			sql.append(" AND NOT EXISTS");
			sql.append(" (SELECT 1");
			sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and ap.status = 1");
			sql.append(" AND ap.ap_param_code  = pi.product_info_code");
			sql.append(" )");
		}else{
			sql.append("  join product_info pi on pi.product_info_id = sc.cat_id");
			sql.append(" where 1 = 1");
			sql.append(" AND NOT EXISTS");
			sql.append(" (SELECT 1");
			sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and ap.status = 1");
			sql.append(" AND ap.ap_param_code  = pi.product_info_code");
			sql.append(" )");
		}
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and exists (select 1 from shop s where s.shop_id=sc.shop_id and status =1 and s.shop_code like ?  ESCAPE '/' )");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}

		if (!StringUtility.isNullOrEmpty(shopName)) {
			String temp = StringUtility.toOracleSearchLike(shopName.toLowerCase());
			sql.append(" and exists (select 1 from shop s where s.shop_id=sc.shop_id and status =1 and lower(s.shop_name) like ? ESCAPE '/' )");
			params.add(temp);
		}
		
		if (catId != null) {
			sql.append(" and sc.cat_id=?");
			params.add(catId);
		}
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and  exists (select 1 from product s where s.product_id=sc.product_id and product_code like ?)");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase()));
		}
		if (status != null) {
			sql.append(" and sc.status=?");
			params.add(status.getValue());
		}else {
			sql.append(" and sc.status !=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (type != null) {
			sql.append(" and sc.type=?");
			params.add(type.getValue());
		}
		
		if (listOrgAccess != null && listOrgAccess.size() != 0) {
//			sql.append(" and sc.shop_id in (select shop_id from shop start with shop_id = ? connect by prior shop_id = parent_shop_id and status =1)");
//			params.add(parentShopId);
			sql.append(" and sc.shop_id in (select shop_id from listOrg)");
		}
		if (ShopProductType.PRODUCT.equals(type))
			sql.append(" order by p.product_code, sc.shop_product_id");
		else
			sql.append(" order by pi.product_info_code, sc.shop_product_id");
			
		if (kPaging == null)
			return repo.getListBySQL(ShopProduct.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ShopProduct.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public List<ShopProduct> getListShopProduct(KPaging<ShopProduct> kPaging, String shopCode,
							String shopName, List<String> listProInfoCode, String productCode, ActiveType status,
							ShopProductType type, List<Long> lstParentShopId, Boolean flag) 
									throws DataAccessException{
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" select sc.* from SHOP_PRODUCT sc ");
		sql.append("  join shop sh on sh.shop_id = sc.shop_id ");
		if(flag){
			sql.append(" JOIN channel_type cn  ");
			sql.append(" on cn.channel_type_id = sh.shop_type_id ");
		}
		if (ShopProductType.PRODUCT.equals(type)){
			sql.append("  join product p on sc.product_id = p.product_id ");
			sql.append("  join product_info pi on pi.product_info_id = p.cat_id  ");
			sql.append(" where 1 = 1 and p.status = 1");
			sql.append(" AND NOT EXISTS");
			sql.append(" (SELECT 1");
			sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and ap.status = 1");
			sql.append(" AND ap.ap_param_code  = pi.product_info_code");
			sql.append(" )");
		}else{
			sql.append("  join product_info pi on pi.product_info_id = sc.cat_id");
			sql.append(" where 1 = 1 and  pi.status = 1");
			sql.append(" AND NOT EXISTS");
			sql.append(" (SELECT 1");
			sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and ap.status = 1");
			sql.append(" AND ap.ap_param_code  = pi.product_info_code");
			sql.append(" )");
		}
		
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" and exists (select 1 from shop s where s.shop_id=sc.shop_id and s.shop_code like ?  ESCAPE '/' and s.status =1 )");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			String temp = StringUtility.toOracleSearchLike(shopName.toLowerCase());
			sql.append(" and exists (select 1 from shop s where s.shop_id=sc.shop_id and lower(s.shop_name) like ? ESCAPE '/' and s.status =1)");
			params.add(temp);
		}
		
		if (null != listProInfoCode && listProInfoCode.size() > 0) {
			sql.append(" and pi.product_info_id in (");
			for (int i = 0; i < listProInfoCode.size(); i++) {
				if (i < listProInfoCode.size() - 1) {
					sql.append("?, ");
				} else {
					sql.append(" ?) ");
				}
				params.add(listProInfoCode.get(i));
			}
		}
		if (!StringUtility.isNullOrEmpty(productCode)) {
			sql.append(" and  exists (select 1 from product s where s.product_id=sc.product_id and product_code like ?)");
			params.add(StringUtility.toOracleSearchLikeSuffix(productCode.toUpperCase()));
		}
		if (status != null) {
			sql.append(" and sc.status=?");
			params.add(status.getValue());
		}else {
			sql.append(" and sc.status !=?");
			params.add(ActiveType.DELETED.getValue());
		}
		if (type != null) {
			sql.append(" and sc.type=?");
			params.add(type.getValue());
		}
		if(flag) {
			sql.append(" and cn.type     = 1 ");
			sql.append(" and cn.object_type = 3 ");
			sql.append(" AND cn.status = 1 ");
			sql.append(" AND sh.status = 1 ");
		} else if(lstParentShopId != null) {
			sql.append(" and sc.shop_id in (select shop_id from shop start with shop_id IN (-1");
			for(int i = 0; i < lstParentShopId.size(); i++) {
				sql.append(", ?");
				params.add(lstParentShopId.get(i));
			}
			sql.append(") connect by prior shop_id = parent_shop_id and status =1) ");
		}
		if (ShopProductType.PRODUCT.equals(type))
			sql.append(" order by p.product_code, sc.shop_product_id");
		else
			sql.append(" order by pi.product_info_name, sh.shop_code ASC");
			
		if (kPaging == null)
			return repo.getListBySQL(ShopProduct.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(ShopProduct.class, sql.toString(), params, kPaging);
	}
	
	@Override
	public Boolean isUsingByOthers(long shopProductId) throws DataAccessException {
		return false;
	}
	
	@Override
	public ShopProduct getShopProductByImport(String shopCode, String codeFlag, String calendarD, int flag) throws DataAccessException {
		/*
		 * flag = 0: Theo san pham
		 * flag = 1: Theo nghanh hang
		 * */
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from shop_product ");
		sql.append(" where shop_id in (select shop_id from shop where lower(shop_code)=? and status = 1) ");
		params.add(shopCode.trim().toLowerCase());
//		sql.append(" and lower(calendar_d) = ? ");
//		params.add(calendarD.trim().toLowerCase());
		if(flag==0){
			sql.append(" and product_id in (select product_id as product_id from product where lower(product_code)=? and status = 1) ");
		}else{
			sql.append(" and cat_id in (select product_info_id as cat_id from product_info where lower(product_info_code)=? and status = 1) ");
		}
		params.add(codeFlag.trim().toLowerCase());
		
		sql.append(" and status in(0, 1) ");
		return repo.getFirstBySQL(ShopProduct.class, sql.toString(), params);
	}

	@Override
	public List<ShopProduct> getLitsShopProductByListId(List<Long> lst) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from shop_product ");
		sql.append(" where status = 1 ");
		sql.append(" and shop_product_id in (-1  ");
		for(Long id:lst){
			sql.append(" ,? ");
			params.add(id);
			
		}
		sql.append(" )");
		return repo.getListBySQL(ShopProduct.class, sql.toString(), params);
	}
	
}

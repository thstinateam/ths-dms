package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.ActionLog;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.RptStaffSaleFilter;
import ths.dms.core.entities.vo.ActionLogCustomerVO;
import ths.dms.core.entities.vo.ActionLogVO;
import ths.dms.core.entities.vo.CustomerPositionVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.TimeVisitCustomerVO;
import ths.dms.core.exceptions.DataAccessException;

public interface ActionLogDAO {

	ActionLog createActionLog(ActionLog actionLog, LogInfoVO logInfo)
			throws DataAccessException;

	void deleteActionLog(ActionLog actionLog, LogInfoVO logInfo)
			throws DataAccessException;

	void updateActionLog(ActionLog actionLog, LogInfoVO logInfo)
			throws DataAccessException;

	ActionLog getActionLogById(long id) throws DataAccessException;
	
	List<ActionLogVO> getListVisitPlanBySuperVisor(KPaging<ActionLogVO> kPaging, Long shopId,
			String staffCode, Date visitPlanDate, Boolean isHasChild) throws DataAccessException;
	
	List<CustomerPositionVO> getListCustomerPosition(
			KPaging<CustomerPositionVO> kPaging, Long staffId,
			Date visitPlanDate, Integer day) throws DataAccessException;
	
	List<TimeVisitCustomerVO> getListVisitCustomer(Date fromDate, Date toDate,
			Long shopId, Long superId, Long staffId,
			Boolean getShopOnly) throws DataAccessException;
	
	List<ActionLogVO> getRptVisitPlan(KPaging<ActionLogVO> kPaging, Long shopId,
			String staffCode, String superCode, Date visitPlanDate, Boolean isHasChild) throws DataAccessException;

	/**
	 * Lay thoi gian ghe tham cua NVBH voi KH
	 * Chua khai bao tren Mgr
	 * @author vuongmq
	 * @param filter
	 * @return List<ActionLogCustomerVO>
	 * @throws DataAccessException
	 * @since 19/08/2015
	 */
	List<ActionLogCustomerVO> getActionLogCustomerOfStaff(RptStaffSaleFilter filter) throws DataAccessException;
}

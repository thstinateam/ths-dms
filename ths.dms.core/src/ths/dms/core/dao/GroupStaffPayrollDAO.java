package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.GroupStaffPayroll;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.GroupStaffPayrollObjApply;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.GroupStaffPayrollVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface GroupStaffPayrollDAO {

	GroupStaffPayroll createGroupStaffPayroll(
			GroupStaffPayroll groupStaffPayroll, LogInfoVO logInfo)
			throws DataAccessException;

	void deleteGroupStaffPayroll(GroupStaffPayroll groupStaffPayroll,
			LogInfoVO logInfo) throws DataAccessException;

	void updateGroupStaffPayroll(GroupStaffPayroll groupStaffPayroll,
			LogInfoVO logInfo) throws DataAccessException;

	GroupStaffPayroll getGroupStaffPayrollById(long id)
			throws DataAccessException;
	
	List<Staff> getStaffOfGroup(KPaging<Staff> kPaging,
			long groupStaffPayRollId) throws DataAccessException;
	
	List<GroupStaffPayrollVO> getListGroupStaffPayroll(
			KPaging<GroupStaffPayrollVO> kPaging, String groupStaffPayrollCode,
			String groupStaffPayrollName, Long shopId,
			GroupStaffPayrollObjApply objectApply, Long staffTypeId)
			throws DataAccessException;

	GroupStaffPayroll getGroupStaffPayrollByCode(String groupStraffPrCode) throws DataAccessException;
	
	boolean checkStaffExitInGoupStaff(long groupStaffPayRollId, long staffId)
			throws DataAccessException;

	void deleteGroupStaffPayrollDetail(long groupId) throws DataAccessException;
}

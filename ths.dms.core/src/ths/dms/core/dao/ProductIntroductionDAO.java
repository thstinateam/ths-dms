package ths.dms.core.dao;

import ths.dms.core.entities.ProductIntroduction;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface ProductIntroductionDAO {

	ProductIntroduction createProductIntroduction(ProductIntroduction productIntroduction, LogInfoVO logInfo) throws DataAccessException;

	void deleteProductIntroduction(ProductIntroduction productIntroduction, LogInfoVO logInfo) throws DataAccessException;

	void updateProductIntroduction(ProductIntroduction productIntroduction, LogInfoVO logInfo) throws DataAccessException;
	
	ProductIntroduction getProductIntroductionById(long id) throws DataAccessException;

	ProductIntroduction getProductIntroductionByProduct(Long productId)
			throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.Date;
import java.util.List;
import java.util.SortedMap;

import ths.dms.core.entities.ActionLog;
import ths.dms.core.entities.ParentStaffMap;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffGroupDetail;
import ths.dms.core.entities.StaffPositionLog;
import ths.dms.core.entities.StaffSaleCat;
import ths.dms.core.entities.StaffType;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.StaffFilter;
import ths.dms.core.entities.enumtype.StaffFilterNew;
import ths.dms.core.entities.enumtype.StaffObjectType;
import ths.dms.core.entities.filter.ImageFilter;
import ths.dms.core.entities.filter.RoutingCustomerFilter;
import ths.dms.core.entities.filter.StaffTypeFilter;
import ths.dms.core.entities.filter.SupFilter;
import ths.dms.core.entities.vo.AmountPlanStaffVO;
import ths.dms.core.entities.vo.CustomerVO;
import ths.dms.core.entities.vo.ImageVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.NVBHSaleVO;
import ths.dms.core.entities.vo.ParentStaffMapVO;
import ths.dms.core.entities.vo.ShopTreeVO;
import ths.dms.core.entities.vo.StaffComboxVO;
import ths.dms.core.entities.vo.StaffExportVO;
import ths.dms.core.entities.vo.StaffGroupDetailVO;
import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.entities.vo.StaffPositionVOEx;
import ths.dms.core.entities.vo.StaffSimpleVO;
import ths.dms.core.entities.vo.StaffVO;
import ths.dms.core.entities.vo.SupervisorSaleVO;
import ths.dms.core.entities.vo.TreeVO;
import ths.core.entities.vo.rpt.RptCustomerNotVisitOfSalesVO;
import ths.core.entities.vo.rpt.RptExImStockOfStaffDataVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;
import ths.dms.core.filter.UnitFilter;

public interface StaffDAO {

	Staff createStaff(Staff staff, LogInfoVO logInfo) throws DataAccessException;

	void deleteStaff(Staff staff, LogInfoVO logInfo) throws DataAccessException;

	void updateStaff(Staff staff, LogInfoVO logInfo) throws DataAccessException;
	
	ParentStaffMap createParentStaffMap(ParentStaffMap psm, LogInfoVO logInfo) throws DataAccessException;

	void deleteParentStaffMap(ParentStaffMap psm, LogInfoVO logInfo) throws DataAccessException;

	void updateParentStaffMap(ParentStaffMap psm, LogInfoVO logInfo) throws DataAccessException;
	
	void deleteParentByStaff(Long staffId,List<Long> lstParentId, LogInfoVO logInfo) throws DataAccessException;
	
	Staff getStaffById(long id) throws DataAccessException;
	
	Staff getStaffByCodeAndListShop(String staffCode, String lstShopId) throws DataAccessException;
	
	ParentStaffMap getParentStaffMapById(long id) throws DataAccessException;

	Staff getStaffByCode(String code) throws DataAccessException;
	
	Staff getStaffLogin(String code,String password) throws DataAccessException;

	Boolean isUsingByOthers(long staffId) throws DataAccessException;

	List<Staff> getListStaffByOwnerId(Long ownerId) throws DataAccessException;

	/**
	 * Lay danh sach nhan vien theo shopId, tuy theo kieu nhan vien (ban hang,
	 * vansale ...)
	 * 
	 * @author -> thuattq
	 */
	List<Staff> getListStaffByShopId(KPaging<Staff> kPaging, Long shopId,
			ActiveType staffActiveType, List<StaffObjectType> objectType)
			throws DataAccessException;

//	List<Staff> getListStaff(KPaging<Staff> kPaging, String staffCode,
//			String staffTypeCode, String staffName, String mobilePhoneNum,
//			ActiveType status, String shopCode, StaffObjectType staffType)
//			throws DataAccessException;

	Staff getStaffByRoutingCustomer(Long customerId) throws DataAccessException;

	Boolean checkIfStaffExists(String email, String mobiphone, String idno, Long id)
			throws DataAccessException;

	List<Staff> getListSupervisorByShop(KPaging<Staff> kPaging,
			String staffCode, String staffName, String childShopCode)
			throws DataAccessException;

	/**
	 * @author hungnm
	 * @param staffId
	 * @return
	 * @throws DataAccessException
	 */
	Boolean checkIfStaffManageAnySaleMan(Long staffId)
			throws DataAccessException;

	/**
	 * @author hungnm
	 * @param staffOwnerId
	 * @throws DataAccessException
	 */
	void updateSaleManagerToNull(Long staffOwnerId)
			throws DataAccessException;

	/**
	 * @author hungnm
	 * @param staffOwnerId
	 * @param lstStaffId
	 * @throws DataAccessException
	 */
	void updateSaleManager(Long staffOwnerId, List<Long> lstStaffId)
			throws DataAccessException;

	/**
	 * @author hungnm
	 * @param ownerId
	 * @param shopCode
	 * @param shopName
	 * @param staffCode
	 * @param staffName
	 * @return
	 * @throws DataAccessException
	 */
	List<Staff> getListStaffByOwnerId(KPaging<Staff> kPaging, Long ownerId,
			String shopCode, String shopName, String staffCode, String staffName)
			throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param staffOwnerCode
	 * @param staffCode
	 * @param staffName
	 * @param staffObjectType
	 * @return
	 * @throws DataAccessException
	 */
	List<Staff> getListStaffByOwner(String staffOwnerCode, String staffCode,
			String staffName, StaffObjectType staffObjectType)
			throws DataAccessException;

	Boolean checkStaffInShopOldFamily(String staffCode, String shopCode)
			throws DataAccessException;

	List<Staff> getListStaffNotManagedByOwnerId(KPaging<Staff> kPaging,
			Long ownerId, String shopCode, String shopName, String staffCode,
			String staffName, Long parentShopId) throws DataAccessException;

	void updateOwnerIdToNull(Long staffOwnerId) throws DataAccessException;

	String generateStaffCode() throws DataAccessException;

	List<Staff> getListStaffByShopCode(KPaging<Staff> kPaging, String shopCode)
			throws DataAccessException;

	/**
	*  Mo ta chuc nang cua ham
	*  @author: thanhnn
	*  @param shopCode
	*  @param superCode
	*  @return
	*  @return: Boolean
	*  @throws:
	*/
	Boolean checkExitStaffManagerBy(String shopCode, String superCode) throws DataAccessException;

	Staff getStaffNotInDisplayProgram(Long parentShopId, Long displayProgramId,
			String staffCode, String staffName, Integer limit,
			Boolean isGetChildOnly) throws DataAccessException;

	List<Staff> getListStaffNotInDisplayProgram(Long parentShopId,
			Long displayProgramId, String staffCode, String staffName,
			Integer limit, Boolean isGetChildOnly) throws DataAccessException;

	List<Staff> getListCashierByShop(KPaging<Staff> kPaging, String shopCode,
			ActiveType status, Boolean isOrderByCode) throws DataAccessException;

	/**
	 * Lay ds nhan vien cung quan ly 1 tuyen tai cung mot thoi diem
	 * @author trietptm
	 * @param filter
	 * @return ds nhan vien cung quan ly 1 tuyen tai cung mot thoi diem
	 * @throws DataAccessException
	 * @since Nov 26, 2015
	 */
	List<Staff> getSaleStaffByDate(RoutingCustomerFilter filter) throws DataAccessException;
	
	Staff getSaleStaffByDateForCreateOrder(Long customerId, Long shopId, Date orderDate, Long staffId)
			throws DataAccessException;

//	List<Staff> getListStaff(KPaging<Staff> kPaging, String staffCode,
//			String staffTypeCode, String staffName, String mobilePhoneNum,
//			ActiveType status, String shopCode, StaffObjectType staffType,
//			ChannelTypeType channelTypeType, Integer channelObjectType,
//			String saleTypeCode, String shopName, Boolean isGetShopOnly,
//			String shopCodeStaff, String shopNameStaff, String staffOwnerCode)
//			throws DataAccessException;

	List<Staff> getListStaffByRoutingCustomer(Long customerId)
			throws DataAccessException;
	
	List<RptExImStockOfStaffDataVO> getRptExImStockOfStaff(Long shopId,
			List<Long> listStaffId, Date fromDate, Date toDate)
			throws DataAccessException;

	Boolean checkStaffPhoneExists(String phoneNum) throws DataAccessException;

	
	/**
	 * Bao cao khach hang khong ghe tham trong tuyen cua nhan vien ban hang
	 * 
	 * @author thuattq
	 */
	List<RptCustomerNotVisitOfSalesVO> getListRptKHKGTTT_NVBH(List<Long> listShopId,
			List<Long> listNVGSId, List<Long> listNVBHId, Date fromDate,
			Date toDate) throws DataAccessException;

	/**
	*  lay danh sach nhan vien o nghiep vu giam sat nha phan phoi
	*  @author: thanhnn
	*  @param kPaging
	*  @param staffCode
	*  @param staffName
	*  @param shopCode
	*  @param type
	*  @param listObjectType
	*  @param isHasChild
	*  @return
	*  @return: List<StaffVO>
	*  @throws:
	*/
	List<StaffVO> getListStaffForSuperVisorWithCondition(KPaging<StaffVO> kPaging,
			String staffCode, String staffName, String shopCode, ActiveType status, 
			ChannelTypeType type, List<StaffObjectType> listObjectType,
			Boolean isHasChild) throws DataAccessException;
	
	List<Staff> getListSupervisorAllowShop(KPaging<Staff> kPaging,
			String staffCode, String staffName, String shopCode, Boolean isHasChild)
			throws DataAccessException;

	/**
	*  Mo ta chuc nang cua ham
	*  @author: thanhnn
	*  @param kPaging
	*  @param staffOwnerCode
	*  @param staffCode
	*  @param staffName
	*  @param shopCode
	*  @param isHasChild
	*  @return
	*  @return: List<Staff>
	*  @throws:
	*/
	List<Staff> getListStaffAllowOwnerAndShop(KPaging<Staff> kPaging,
			String staffOwnerCode, String staffCode, String staffName,
			String shopCode, Boolean isHasChild) throws DataAccessException;

	/**
	 * @author hungnm
	 * @param kPaging
	 * @param staffCode
	 * @param staffName
	 * @param shopId
	 * @param staffActiveType
	 * @param channelType
	 * @return
	 * @throws DataAccessException
	 */
	List<Staff> getListStaffByShopId(KPaging<Staff> kPaging, String staffCode,
			String staffName, Long shopId, ActiveType staffActiveType,
			List<StaffObjectType> channelType, Boolean isHasChild,  List<Long> listStaffOwnerIds) throws DataAccessException;

	/**
	 * @author hungnm
	 * @param staffCode
	 * @param staffName
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 */
	List<Staff> getListNVGS(KPaging<Staff> kPaging, String staffCode, String staffName, Long shopId , List<String> listTBHVCode)
			throws DataAccessException;

	List<Staff> getListNVGS2(KPaging<Staff> kPaging, Long shopId, String lstStaffCode)
	throws DataAccessException;
	
	List<Staff> getListNVGS3(KPaging<Staff> kPaging, String strListShopId,String staffCode,String staffName, String lstStaffCode)
			throws DataAccessException;
	
	List<Staff> getListNVBH(KPaging<Staff> kPaging, String staffCode,
			String staffName, List<String> lstStaffOwnerCode, Long shopId, Boolean isGetChildShop,List<String> lstTBHVCode)
			throws DataAccessException;
	
	List<StaffComboxVO> getListNVBHisShop(KPaging<StaffComboxVO> kPaging, String staffCode,
			String staffName, List<String> lstStaffOwnerCode, Long shopId, Boolean isGetChildShop,List<String> lstTBHVCode)
			throws DataAccessException;
	
	List<Staff> getListNVBH2(KPaging<Staff> kPaging, String staffCode,
			String staffName, List<String> lstStaffOwnerCode, String strListShopId, Boolean isGetChildShop,List<String> lstTBHVCode)
			throws DataAccessException;
	
	List<Staff> getListStaffExceptListStaff(KPaging<Staff> kPaging, String staffCode, String staffName,
			ActiveType status, String shopCode, Long staffTypeId, List<Long> exceptStaffIds)
			throws DataAccessException;

	List<Staff> getListSupervisorByShop(KPaging<Staff> kPaging,
			String staffCode, String staffName, String childShopCode,
			Boolean isGetUpperShop) throws DataAccessException;

	List<Staff> getListNVGH(KPaging<Staff> kPaging, Long shopId,
			String customerShortCode) throws DataAccessException;

	List<Staff> getListStaffForAbsentReport(KPaging<Staff> kPaging,
			String staffCode, String staffName, Long shopId)
			throws DataAccessException;

	/**
	*  Mo ta chuc nang cua ham
	*  @author: thanhnn
	*  @param kPaging
	*  @param staffCode
	*  @param staffName
	*  @param shopIds
	*  @param staffActiveType
	*  @param channelType
	*  @param isHasChild
	*  @param listStaffOwnerIds
	*  @return
	*  @return: List<Staff>
	*  @throws:
	*/
	List<Staff> getListPreAndVanStaffHasChild(KPaging<Staff> kPaging,
			String staffCode, String staffName, List<Long> shopIds,
			ActiveType staffActiveType, List<StaffObjectType> channelType,
			Boolean isHasChild, List<Long> listStaffOwnerIds) throws DataAccessException;

	List<Staff> getListStaff(StaffFilter filter) throws DataAccessException;
	
	List<StaffVO> getListStaffVO(StaffFilter filter) throws DataAccessException;
	
	Staff getNVGSInGroup(Long staffGroupId) throws DataAccessException;

	List<Staff> getListNVBHInGroup(Long staffGroupId)
			throws DataAccessException;

	Staff getTBHVOfShop(Long vungId) throws DataAccessException;

	Staff getTBHMOfShop(Long mienId) throws DataAccessException;

	List<Staff> getListNVGSInVung(Long vungId) throws DataAccessException;

	List<Staff> getListTBHVInMien(Long mienId) throws DataAccessException;
	
	List<ShopTreeVO> getListShopManagedByStaff(Long staffId, KPaging<ShopTreeVO> kPaging) throws DataAccessException;

	List<Staff> getListNVBHInTuyen(KPaging<Staff> kPaging, Long gsnppId,Long shopId) throws DataAccessException;

	List<Staff> getListNVGSNew(KPaging<Staff> kPaging, String staffCode,
			String staffName, Long vungId, Long shopId, List<Long> lstExceptId,
			SortedMap<Long, List<Long>> deletedStaffByVung)
			throws DataAccessException;

	List<StaffGroupDetail> getListStaffGroupDetail(StaffFilter filter)throws DataAccessException;

	List<StaffPositionVO> getListStaffPosition(Long shopId, Long staffOwnerId, StaffObjectType roleType, Boolean oneNode) throws DataAccessException;
	
	List<StaffPositionVO> getListStaffPosition(SupFilter filter) throws DataAccessException;

	List<SupervisorSaleVO> getListSupervisorSaleVO(KPaging<SupervisorSaleVO> kPaging, Long tbhvId, Long shopId) throws DataAccessException;

	List<StaffPositionVO> getListStaffPositionWithTraining(Long shopId) throws DataAccessException;

	AmountPlanStaffVO getAmountPlanOfStaff(StaffObjectType type, Staff staff,Long shopId) throws DataAccessException;

	Staff getStaffOwnerOfShop(Long shopId) throws DataAccessException;

	List<StaffPositionVO> getListStaffPosition(Long shopId, Long staffId, Boolean isVNM, Boolean isMien, Boolean isVung, Boolean isNPP, Boolean oneNode) throws DataAccessException;

	List<StaffPositionLog> getListStaffPosition2(Long userId, Boolean getTbhv,
			Boolean getGsnpp, Boolean getNvbh) throws DataAccessException;

	StaffPositionLog getLatLngOfStaff(Long staffId) throws DataAccessException;

	TreeVO<StaffPositionVO> searchStaff(SupFilter filter) throws DataAccessException;

	List<Staff> getListNVGSByTBHVId(KPaging<Staff> kPaging, Long tbhvId) throws DataAccessException;

	List<NVBHSaleVO> getListTBHMSaleVO(KPaging<NVBHSaleVO> kPaging, Long gdmId, Long shopId) throws DataAccessException;
	/**
	 * Tim kiem nhan vien them moi trong NVPTKH (CTTB)
	 * @author thongnm
	 */
	List<Staff> getListNVBHForDisplayProgram(KPaging<Staff> kPaging,Long shopId, Long displayProgramId, String code, String name) throws DataAccessException;

	List<ParentStaffMapVO> getListParentStaffMapVO(SupFilter filter) throws DataAccessException;
	
	StaffPositionVO getStaffPosition(Long staffId) throws DataAccessException;
	
	/**
	 * Kiem tra shop co duoc quan ly boi NVGS
	 * @author thongnm
	 */
	Boolean checkShopManagedByNVGS(Long gsnppId, Long shopId) throws DataAccessException;

	List<Staff> getListStaffForExportExcel(StaffFilter filter)
			throws DataAccessException;
	
	/**
	 * @author thachnn
	 * @param code
	 * @param parentId
	 * des : kiem tra code staff co thuoc quan ly user dang nhap
	 */
	Staff getStaffByCodeOfUserLogin(String code, Long parentId) 
	throws DataAccessException;

	List<Staff> getListStaffByShop(List<Long> lstShopId,
			ActiveType staffActiveType, List<StaffObjectType> objectTypes)
			throws DataAccessException;

	List<Staff> getListStaffByShop(KPaging<Staff> kPaging,
			List<Long> lstShopId, String staffName, String staffCode,
			ActiveType staffActiveType, List<StaffObjectType> objectTypes)throws DataAccessException;

	/**SangTN - Ve lo trinh*/
	List<StaffPositionLog> showDirectionStaff(Long staffId)
			throws DataAccessException;
	Staff getStaffByCodeAndShopId(String staffCode, Long shopId) throws DataAccessException;
	Staff getStaffByCodeAndShopId1(String staffCode, Long shopId) throws DataAccessException;

	List<StaffPositionVO> getListStaffPositionOptionsDate(SupFilter filter) throws DataAccessException;

	List<StaffPositionVOEx> getStaffPositionEx(SupFilter filter) throws DataAccessException;

	TreeVO<StaffPositionVO> searchStaffEx(SupFilter filter) throws DataAccessException;

	List<Staff> getListStaffByShopIdWithRunning(Long shopId) throws DataAccessException;

	List<Staff> getStaffMultilChoice(Long staffId, String staffCode, Long shopId, Integer status) throws DataAccessException;

	/**
	 * @author sangtn
	 * @since 28-03-2014
	 * @description Get list staff for VO
	 * @note Expand from getListStaffByShop function
	 */
	List<StaffSimpleVO> getListStaffByShopCombo(List<Long> lstShopId,
			ActiveType staffActiveType, List<StaffObjectType> objectTypes)
			throws DataAccessException;

	List<Staff> getListNVBHInTuyenNew(KPaging<Staff> kPaging, Long gsnppId, Long shopId) throws DataAccessException;

	Staff getStaffByInfoBasic(String code, Long shopId, ActiveType status)
			throws DataAccessException;

	List<Staff> getListNVGS4(KPaging<Staff> kPaging, String strListShopId,
			String staffCode, String staffName) throws DataAccessException;
	
	/**
	 * Tam ngung tat ca cac visit_plan cua NVBH trong shop
	 * 
	 * @author lacnv1
	 * @since Jun 10, 2014
	 */
	void offVisitPlanOfStaff(long staffId, long shopId, Date updateToDate, String userCode) throws DataAccessException;
	
	/**
	 * Lay danh sach NV popup cay don vi
	 * 
	 * @author lacnv1
	 * @since Jun 19, 2014
	 */
	List<Staff> getListStaffPopup(StaffFilter filter) throws DataAccessException;
	
	/**
	 * Lay danh sach NV (cay don vi)
	 * 
	 * @author lacnv1
	 * @since Jun 19, 2014
	 */
	List<Staff> getListStaffOfShop(StaffFilter filter) throws DataAccessException;
	
	/**
	 * Lay danh sach NV (cay don vi - export)
	 * 
	 * @author lacnv1
	 * @since Jun 19, 2014
	 */
	List<StaffExportVO> getListExportStaffOfShop(StaffFilter filter) throws DataAccessException;
	
	/**
	 * Lay danh sach NV (cay don vi)
	 * 
	 * @author lacnv1
	 * @since Jun 19, 2014
	 */
	List<StaffGroupDetailVO> getListStaffGroupOfUnitTree(StaffFilter filter, KPaging<StaffGroupDetailVO> paging) throws DataAccessException;
	List<Staff> getListStaffYMamagerStaffX(Long staffIdY, Long staffIdX) throws DataAccessException;
	
	List<Staff> getListStaffByShop(StaffFilter filter) throws DataAccessException;

	List<Staff> getListStaffNew(StaffFilterNew filter)
			throws DataAccessException;
	
	/**
	 * tam ngung bang parentStaffmap theo nhan vien
	 * 
	 * @author tungmt
	 * @since 14/10/2014
	 */
	void stopParentStaffMap(Staff staff, LogInfoVO logInfo) throws DataAccessException;
	
	List<ParentStaffMap> getListParentStaffMap(StaffFilter filter) throws DataAccessException;
	
	/**
	 * Lay danh sach nhan vien da khoa kho trong ngay hien tai
	 * 
	 * @author lacnv1
	 * @since Nov 05, 2014
	 */
	List<Staff> getListStaffLockedStock(StaffFilter filter) throws DataAccessException;
	
	/**
	 * tim kiem nhan vien
	 * @author tuannd20
	 * @param kPaging Thong tin phan trang
	 * @param unitFilter Tieu chi tim kiem
	 * @return Danh sach nhan vien
	 * @throws DataAccessException
	 * @since 13/03/2015
	 */
	List<StaffVO> findStaffVOBy(KPaging<StaffVO> kPaging, UnitFilter unitFilter) throws DataAccessException;
	
	/**
	 * Lay danh sach giam sat
	 * 
	 * @author hoanv25
	 * @param shopId      Id cua shop can lay
	 * @return Ket qua tim kiem giam sat
	 * @throws BusinessException
	 * @since July 09/2015
	 */
	List<ImageVO> getListGSByNPP(Long shopId, ImageFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach NVBH
	 * 
	 * @author hoanv25
	 * @param shopId      Id cua shop can lay
	 * @return Ket qua tim kiem giam sat
	 * @throws BusinessException
	 * @since July 09/2015
	 */
	List<ImageVO> getListNVBHByNPP(Long shopId, ImageFilter filter) throws DataAccessException;
	
	/**
	 * Lay danh sach giam sat theo list id
	 * 
	 * @author hoanv25
	 * @param  lstGsId
	 * @return Ket qua tim kiem giam sat
	 * @throws BusinessException
	 * @since July 09/2015
	 */
	List<StaffVO> getListGsById(List<Long> lstGsId)  throws DataAccessException;

	/**
	 * Lay danh sach NVBH
	 * 
	 * @author hoanv25
	 * @param shopId
	 *            Id cua shop can lay
	 * @return Ket qua tim kiem giam sat
	 * @throws BusinessException
	 * @since July 09/2015
	 */
	List<ImageVO> getListNVBHByNPPShop(List<Long> lstShopId, ImageFilter filter) throws DataAccessException;
	
	/**
	 * Lay danh sach sub nvbh
	 * 
	 * @author hoanv25
	 * @param filter
	 * @return 
	 * @throws BusinessException
	 * @since Auggust 17/2015
	 */
	List<StaffVO> listEquipCategoryCode(StaffFilter filter) throws DataAccessException;

	/**
	 * Kiem tra danh sach staff theo nganh hang
	 * 
	 * @author hoanv25
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @since Auggust 19/2015
	 */
	List<StaffVO> listCheckSubStaff(StaffSaleCat filter) throws DataAccessException;

	/**
	 * lay danh sach staff theo nganh hang
	 * 
	 * @author hoanv25
	 * @param filter
	 * @return
	 * @throws BusinessException
	 * @since Auggust 19/2015
	 */
	List<StaffVO> listCheckDelSubStaff(Long id) throws DataAccessException;
	
	/**
	 * lay danh sach thong tin nhan vien cung voi thong tin thuoc tinh dong
	 * @author tuannd20
	 * @param filter dieu kien loc nhan vien
	 * @return danh sach nhan vien cung voi thong tin thuoc tinh dong
	 * @throws BusinessException
	 * @since 13/09/2015
	 */
	List<StaffExportVO> getListStaffWithDynamicAttribute(StaffFilter filter) throws DataAccessException;
	
	/**
	 * Lay ds khach hang ghe tham
	 * @author trietptm
	 * @param shopId
	 * @param staffId
	 * @param isCustomerWaiting
	 * @param checkDate
	 * @param fromTime
	 * @param toTime
	 * @return
	 * @throws DataAccessException
	 * @since Sep 19, 2015
	 */
	List<CustomerVO> getListCustomerHasVisitPlan(Long shopId, Long staffId, Integer isCustomerWaiting, Date checkDate, String fromTime, String toTime) throws DataAccessException;
	
	/**
	 * Lộ trình nhân viên bán hàng (DMS.LITE)
	 * @author trietptm
	 * @param staffId
	 * @param checkDate
	 * @param numPoint
	 * @param fromTime
	 * @param toTime
	 * @return
	 * @throws DataAccessException
	 * @since Sep 19, 2015
	 */
	List<StaffPositionLog> showDirectionStaff(Long staffId, Date checkDate, int numPoint, String fromTime, String toTime) throws DataAccessException;
	
	/**
	 * lay ds ActionLog theo dk
	 * @author trietptm
	 * @param staffId
	 * @param checkDate
	 * @param fromTime
	 * @param toTime
	 * @return
	 * @throws DataAccessException
	 * @since Sep 19, 2015
	 */
	List<ActionLog> getListActionLog(Long staffId, Date checkDate, String fromTime, String toTime) throws DataAccessException;

	/**
	 * Lay danh sach staffVO phan quyen theo shopRoot
	 * @author vuongmq
	 * @param kPaging
	 * @param unitFilter
	 * @return List<StaffVO>
	 * @throws DataAccessException
	 * @since 13/11/2015
	 */
	List<StaffVO> getListStaffVOInherit(KPaging<StaffVO> kPaging, UnitFilter unitFilter) throws DataAccessException;

	/**
	 * Lay danh sach staffType phan quyen theo shopRoot, don vi chon
	 * @author vuongmq
	 * @param unitFilter
	 * @return List<StaffType>
	 * @throws DataAccessException
	 * @since 18/11/2015
	 */
	List<StaffType> getListStaffType(UnitFilter unitFilter) throws DataAccessException;

	/**
	 * Lay danh sach staff them feedback
	 * @author vuongmq
	 * @param filter
	 * @return List<StaffVO>
	 * @throws DataAccessException
	 * @since 18/11/2015
	 */
	List<StaffVO> getListStaffVOFeedbackByFilter(StaffFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach StaffVO quan ly cua staffId
	 * @author vuongmq
	 * @param filter
	 * @return List<StaffVO> 
	 * @throws DataAccessException
	 * @since 18/11/2015
	 */
	List<StaffVO> getListStaffVOUserMapStaffByFilter(StaffFilter filter) throws DataAccessException;

	/**
	 * lay danh sach staffType theo filter
	 * @author trietptm
	 * @param filter
	 * @return danh sach staffType
	 * @throws DataAccessException
	 * @since Nov 27, 2015
	*/
	List<StaffType> getListAllStaffType(StaffTypeFilter filter) throws DataAccessException;
	
}
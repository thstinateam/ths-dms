package ths.dms.core.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ths.dms.core.entities.Price;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.SaleOrderFilter;
import ths.dms.core.entities.filter.BasicFilter;
import ths.dms.core.entities.filter.PriceFilter;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.entities.vo.PriceVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface PriceDAO {

	Price createPrice(Price price, LogInfoVO logInfo) throws DataAccessException;

	void deletePrice(Price price, LogInfoVO logInfo) throws DataAccessException;

	void updatePrice(Price price, LogInfoVO logInfo) throws DataAccessException;
	
	Price getPriceById(long id) throws DataAccessException;

	Boolean isUsingByOthers(long priceId) throws DataAccessException;

	Boolean checkIfDateRangeInUse(Long productId, Date fromDate, Date toDate, Long priceId)
			throws DataAccessException;

	/**
	 * 
	 * @author hieunq1
	 * @param productId
	 * @return
	 * @throws DataAccessException
	 */
	Price getPriceByProductId(Long productId) throws DataAccessException;
	Price getPriceByProductId(Long shopId,Long productId) throws DataAccessException;

	List<Price> getListPrice(KPaging<Price> kPaging, String productCode,
			BigDecimal price, Date fromDate, Date toDate, ActiveType status,
			BigDecimal priceNotVat, BigDecimal vat,boolean isExport) throws DataAccessException;
	
	Price getPriceByProductAndShopId(Long productId, Long shopId) throws DataAccessException ;
	
	/**
	 * Lay gia theo loai KH va don vi
	 * 
	 * @author lacnv1
	 * @since Sep 17, 2014
	 */
	Price getProductPriceForCustomer(long productId, long shopId, long customerTypeId) throws DataAccessException;

	/**
	 * Tim kiem gia
	 * 
	 * @author hunglm16
	 * @return PriceVO
	 * @since September 20,2014
	 * */
	List<PriceVO> searchPriceVOByFilter(PriceFilter<PriceVO> filter) throws DataAccessException;
	
	/**
	 * Lay gia theo 3 Key shopId, customerId, shopChennel, fromDate, toDate
	 * 
	 * @author hunglm16
	 * @return Price
	 * @since September 20,2014
	 * */
	Price getPricetByFilter(PriceFilter<Price> filter) throws DataAccessException;
	/**
	 * Kiem tra san pham cua gia da ton tai trong Stock Total hay chua
	 * 
	 * @author hunglm16
	 * @return Price
	 * @since September 20,2014
	 * */
	Boolean checkPricetByFilterInStockTotal(BasicFilter<Price> filter) throws DataAccessException;
	/**
	 * Kiem tra gia co ton tai va con chau cua no hoac cha cua no
	 * 
	 * @author hunglm16
	 * @return Price
	 * @since September 20,2014
	 * */
	Boolean checkPricetByFilter(PriceFilter<Price> filter) throws DataAccessException;

	List<Price> getListPriceByFilter(PriceFilter<Price> filter) throws DataAccessException;

	Boolean checkPricetByFilterEx(PriceFilter<Price> filter) throws DataAccessException;
	/**
	 * Kiem tra gia co ton tai va con chau cua no hoac cha cua no
	 * 
	 * @author hunglm16
	 * @return Price
	 * @since September 20,2014
	 * @description Chi dung trong dot trien khai thang 10,2014
	 * */
	Boolean checkPricetForStop(PriceFilter<Price> filter) throws DataAccessException;
	
	/**
	 * get products in 'productCodes' which don't have a valid price
	 * @author tuannd20
	 * @param productCodes		products's code to check
	 * @param customerId		customer id
	 * @param customerShopId	customer's shop
	 * @param lockDate			lock date
	 * @return
	 * @throws DataAccessException
	 * @date 23/10/2014
	 */
	List<Product> getInvalidPriceProducts(List<String> productCodes, Long customerId, Long customerShopId, Date lockDate) throws DataAccessException;

	/**
	 * 
	 * @param shopId
	 * @param lockDay
	 * @param productId
	 * @return
	 * @throws DataAccessException
	 */
	List<Price> getListPriceByShopId(Long shopId, Date lockDay, Long productId)throws DataAccessException;
	
	/**
	 * Lay gia theo tat ca dieu kien hien tai
	 * Ham hop le khi tra ra dung 1 dong gia. Neu co nhieu hon 1 dong gia thi sai du lieu
	 * @author trungtm6
	 * @since 21/04/2015 
	 * @param proId
	 * @param shopId
	 * @param date: ngay lay gia
	 * @param shopTypeId
	 * @param cusId
	 * @param cusTypeId: co the null
	 * @return
	 * @throws DataAccessException
	 */
	List<Price> getPriceByFullCondition(Long proId, Long shopId, Date date, Long shopTypeId, Long cusId, Long cusTypeId) throws DataAccessException;
	
	/**
	 * @author tungmt
	 * @since 21/7/2015
	 * @param filter
	 * @return Lay danh sach gia trong rpt theo filter
	 * @throws BusinessException
	 */
	List<Price> getPriceByRpt(SaleOrderFilter<Price> filter) throws DataAccessException;
	
	Price getPriceByProductAndCustomer(Long productId, Long customerId, List<Integer> status) throws DataAccessException;
	Price getPriceByProductChannelTypeShopAndCustomerNull(Long productId, Long channelTypeId, Long shopId, List<Integer> status)  throws DataAccessException;
	Price getPriceByProductChannelTypeShopType(Long customerTypeId, Long shopTypeId, Long productId, List<Integer> status) throws DataAccessException;
	Price getPriceByProductShop(Long shopId, Long productId, List<Integer> status) throws DataAccessException;
	Price getPriceByProductShopType(Long shopTypeId, Long productId, List<Integer> status) throws DataAccessException;

}

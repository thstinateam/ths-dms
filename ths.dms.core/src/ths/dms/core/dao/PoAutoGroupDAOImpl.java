package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.PoAutoGroup;
import ths.dms.core.entities.Product;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoAutoGroupFilter;
import ths.dms.core.entities.enumtype.PoAutoGroupVO;
import ths.dms.core.entities.vo.ProductInfoVOEx;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.dao.repo.IRepository;

public class PoAutoGroupDAOImpl implements PoAutoGroupDAO {

	@Autowired
	private IRepository repo;

	@Override
	public PoAutoGroup createPoAutoGroup(PoAutoGroup poAutoGroup)
			throws DataAccessException {
		if (poAutoGroup == null) {
			throw new IllegalArgumentException("poAuto");
		}
		repo.create(poAutoGroup);
		return repo.getEntityById(PoAutoGroup.class, poAutoGroup.getId());
	}

	@Override
	public void deletePoAutoGroup(PoAutoGroup poAutoGroup)
			throws DataAccessException {
		if (poAutoGroup == null) {
			throw new IllegalArgumentException("poAutoGroup");
		}
		repo.delete(poAutoGroup);
		
	}

	@Override
	public void updatePoAutoGroup(PoAutoGroup poAutoGroup)
			throws DataAccessException {
		if (poAutoGroup == null) {
			throw new IllegalArgumentException("poAutoGroup");
		}
		repo.update(poAutoGroup);
		
	}

	@Override
	public PoAutoGroup getPoAutoGroupById(Long id) throws DataAccessException {
		return repo.getEntityById(PoAutoGroup.class, id);
	}
	
	@Override
	public PoAutoGroup getPoAutoGroupByCode(String groupCode) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select * from po_auto_group where GROUP_CODE = ?");
		params.add(groupCode);
		return repo.getFirstBySQL(PoAutoGroup.class, sql.toString(), params);
	}
	
	
	@Override
	public List<PoAutoGroupVO> getPoAutoGroupVOEx(PoAutoGroupFilter filter,
			KPaging<PoAutoGroupVO> kPaging) throws DataAccessException {
		
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		selectSql.append("SELECT pr.PO_AUTO_GROUP_ID id, ");
		selectSql.append("  pr.group_name groupName, ");
		selectSql.append("  pr.group_code groupCode, ");
		selectSql.append("  pr.status status ");
		fromSql.append("FROM PO_AUTO_GROUP pr ");
		fromSql.append("WHERE 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		if (filter.getId() != null) {
			fromSql.append(" and pr.PO_AUTO_GROUP_ID = ? ");
			params.add(filter.getId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getGroupCode())) {
			fromSql.append(" and lower(pr.group_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getGroupCode().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getGroupName())) {
			fromSql.append(" and lower(pr.group_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getGroupName().toLowerCase()));
		}
		if(filter.getStatus() != null){
			fromSql.append(" and pr.status=?");
			params.add(filter.getStatus().getValue());
		}
		if(filter.getShopId()!=null){
			fromSql.append(" and PO_AUTO_GROUP_SHOP_MAP_ID in (select PO_AUTO_GROUP_SHOP_MAP_ID from PO_AUTO_GROUP_SHOP_MAP where shop_id = ? and status = ?) ");
			params.add(filter.getShopId());
		}
		//sontt
		if(filter.getPoAutoGroupShopMapId()!=null){
			fromSql.append(" and pr.PO_AUTO_GROUP_SHOP_MAP_ID=?");
			params.add(filter.getPoAutoGroupShopMapId());
		}
		//
		selectSql.append(fromSql.toString());
		selectSql.append(" and pr.po_auto_group_shop_map_id is null");
		selectSql.append(" order by pr.group_code");
		
		countSql.append("select count(pr.PO_AUTO_GROUP_ID) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"id", "groupName", "groupCode", "status"
				};

		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER 
		};	
		List<PoAutoGroupVO> lst = null;
		if (kPaging == null){
			lst =  repo.getListByQueryAndScalar(
					PoAutoGroupVO.class, fieldNames,
					fieldTypes, selectSql.toString(), params);
		}else{
			lst = repo.getListByQueryAndScalarPaginated(PoAutoGroupVO.class, fieldNames, fieldTypes,
					selectSql.toString(), countSql.toString(), params, params, kPaging);
		}
		return lst;
	}
	

	@Override
	public List<PoAutoGroupVO> getPoAutoGroupVO(PoAutoGroupFilter filter,
			KPaging<PoAutoGroupVO> kPaging) throws DataAccessException {
		
		StringBuilder selectSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		selectSql.append("SELECT pr.PO_AUTO_GROUP_ID id, ");
		selectSql.append("  pr.group_name groupName, ");
		selectSql.append("  pr.group_code groupCode, ");
		selectSql.append("  pr.status status ");
		fromSql.append("FROM PO_AUTO_GROUP pr ");
		fromSql.append("WHERE 1 = 1 ");
		List<Object> params = new ArrayList<Object>();
		if (filter.getId() != null) {
			fromSql.append(" and pr.PO_AUTO_GROUP_ID = ? ");
			params.add(filter.getId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getGroupCode())) {
			fromSql.append(" and lower(pr.group_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getGroupCode().toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getGroupName())) {
			fromSql.append(" and lower(pr.group_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(filter.getGroupName().toLowerCase()));
		}
		if(filter.getStatus() != null){
			fromSql.append(" and pr.status=?");
			params.add(filter.getStatus().getValue());
		}
		if(filter.getShopId()!=null){
			fromSql.append(" and PO_AUTO_GROUP_SHOP_MAP_ID in (select PO_AUTO_GROUP_SHOP_MAP_ID from PO_AUTO_GROUP_SHOP_MAP where shop_id = ? and status = ?) ");
			params.add(filter.getShopId());
		}
		//sontt
		if(filter.getPoAutoGroupShopMapId()!=null){
			fromSql.append(" and pr.PO_AUTO_GROUP_SHOP_MAP_ID=?");
			params.add(filter.getPoAutoGroupShopMapId());
		}
		//
		selectSql.append(fromSql.toString());
		selectSql.append(" order by pr.group_code");
		
		countSql.append("select count(pr.PO_AUTO_GROUP_ID) as count "); 
		countSql.append(fromSql.toString());
		
		String[] fieldNames = {
				"id", "groupName", "groupCode", "status"
				};

		Type[] fieldTypes = { 
				StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER 
		};	
		List<PoAutoGroupVO> lst = null;
		if (kPaging == null){
			lst =  repo.getListByQueryAndScalar(
					PoAutoGroupVO.class, fieldNames,
					fieldTypes, selectSql.toString(), params);
		}else{
			lst = repo.getListByQueryAndScalarPaginated(PoAutoGroupVO.class, fieldNames, fieldTypes,
					selectSql.toString(), countSql.toString(), params, params, kPaging);
		}
		return lst;
	}
	
	@Override
	public List<ProductInfoVOEx> getListProductInfoCanBeAddedNPP(
			int poAutoGroupId, String name, String code, boolean isSubCat, KPaging<ProductInfoVOEx> kPaging)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("SELECT info.PRODUCT_INFO_ID productInfoId, info.PRODUCT_INFO_NAME productInfoName, info.PRODUCT_INFO_CODE productInfoCode,");
		sql.append(" info.status status, info.description description,");
		sql.append(" info.type type,");
		sql.append("  (");
		sql.append("  CASE");
		sql.append("    WHEN info.type = 2");
		sql.append("    THEN");
		sql.append("      (SELECT pi.product_info_code ");
		sql.append("      FROM product_info_map m");
		sql.append("      JOIN product_info pi ON pi.product_info_id = m.FROM_OBJECT_ID");
		sql.append("      WHERE m.object_type     = 12 and m.status = 1 and M.TO_OBJECT_ID = info.PRODUCT_INFO_ID and rownum = 1 ");
		sql.append("      )");
		sql.append("    else");
		sql.append("      ''");
		sql.append("  END) parentCode");
		sql.append(" FROM product_info info ");
		sql.append(" WHERE 1 =1");
		if(!isSubCat){
			sql.append(" AND info.type =1");
		}else{
			sql.append(" AND info.type =2");
		}
		sql.append(" AND info.status = 1");
		sql.append(" AND NOT EXISTS");
		sql.append(" (SELECT 1");
		sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and ap.status = 1");
		sql.append(" AND ap.ap_param_code  = info.product_info_code");
		sql.append(" )");
		/*fix bug http://10.30.174.211/mantis/view.php?id=2326*/
		/*sql.append(" AND NOT EXISTS");
		sql.append("   (SELECT 1");
		sql.append("   FROM po_auto_group_detail de");
		sql.append("   JOIN po_auto_group g");
		sql.append("   ON de.po_auto_group_id = g.po_auto_group_id");
		sql.append("   WHERE g.status         = 1 and de.status    = 1");
		if(!isSubCat){
			sql.append("   AND object_type        =0");
		}else{
			sql.append("   AND object_type        =1");
		}
		sql.append("   AND object_id          = info.product_info_id");
		sql.append("   )");*/
		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and lower(info.PRODUCT_INFO_CODE) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(code.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(name)) {
			sql.append(" and lower(info.PRODUCT_INFO_NAME) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(name.toLowerCase()));
		}
		sql.append(" order by info.PRODUCT_INFO_CODE asc");

		String[] fieldNames = {
				"productInfoId", 
				"productInfoName",
				"productInfoCode",
				"status", 
				"description",
				"type",
				"parentCode"
				};

		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING,StandardBasicTypes.INTEGER, StandardBasicTypes.STRING
		};	
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from("); 
		countSql.append(sql.toString());
		countSql.append(")");
		
		if (kPaging == null){
			 return repo.getListByQueryAndScalar(ProductInfoVOEx.class, fieldNames,fieldTypes, sql.toString(), params);
		}else{
			return repo.getListByQueryAndScalarPaginated(ProductInfoVOEx.class, fieldNames, fieldTypes,sql.toString(), countSql.toString(), params, params, kPaging);
		}
		
	}

	@Override
	public List<ProductInfoVOEx> getListProductInfoCanBeAdded(
			int poAutoGroupId, String name, String code, boolean isSubCat, KPaging<ProductInfoVOEx> kPaging)throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("SELECT info.PRODUCT_INFO_ID productInfoId, info.PRODUCT_INFO_NAME productInfoName, info.PRODUCT_INFO_CODE productInfoCode,");
		sql.append(" info.status status, info.description description,");
		sql.append(" info.type type,");
		sql.append("  (");
		sql.append("  CASE");
		sql.append("    WHEN info.type = 2");
		sql.append("    THEN");
		sql.append("      (SELECT pi.product_info_code ");
		sql.append("      FROM product_info_map m");
		sql.append("      JOIN product_info pi ON pi.product_info_id = m.FROM_OBJECT_ID");
		sql.append("      WHERE m.object_type     = 12 and m.status = 1 and M.TO_OBJECT_ID = info.PRODUCT_INFO_ID and rownum = 1 ");
		sql.append("      )");
		sql.append("    else");
		sql.append("      ''");
		sql.append("  END) parentCode");
		sql.append(" FROM product_info info ");
		sql.append(" WHERE 1 =1");
		if(!isSubCat){
			sql.append(" AND info.type =1");
		}else{
			sql.append(" AND info.type =2");
		}
		sql.append(" AND info.status = 1");
		sql.append(" AND NOT EXISTS");
		sql.append(" (SELECT 1");
		sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and ap.status = 1");
		sql.append(" AND ap.ap_param_code  = info.product_info_code");
		sql.append(" )");
		/*fix bug http://10.30.174.211/mantis/view.php?id=2326*/
		sql.append(" AND NOT EXISTS");
		sql.append("   (SELECT 1");
		sql.append("   FROM po_auto_group_detail de");
		sql.append("   JOIN po_auto_group g");
		sql.append("   ON de.po_auto_group_id = g.po_auto_group_id");
		sql.append("   WHERE g.status         = 1 and de.status    = 1");
		if(!isSubCat){
			sql.append("   AND object_type        =0");
		}else{
			sql.append("   AND object_type        =1");
		}
		sql.append("   AND object_id          = info.product_info_id");
		sql.append("   )");
		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and lower(info.PRODUCT_INFO_CODE) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(code.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(name)) {
			sql.append(" and lower(info.PRODUCT_INFO_NAME) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(name.toLowerCase()));
		}
		sql.append(" order by info.PRODUCT_INFO_CODE asc");

		String[] fieldNames = {
				"productInfoId", 
				"productInfoName",
				"productInfoCode",
				"status", 
				"description",
				"type",
				"parentCode"
				};

		Type[] fieldTypes = { StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.STRING,StandardBasicTypes.INTEGER, StandardBasicTypes.STRING
		};	
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(1) as count from("); 
		countSql.append(sql.toString());
		countSql.append(")");
		
		if (kPaging == null){
			 return repo.getListByQueryAndScalar(ProductInfoVOEx.class, fieldNames,fieldTypes, sql.toString(), params);
		}else{
			return repo.getListByQueryAndScalarPaginated(ProductInfoVOEx.class, fieldNames, fieldTypes,sql.toString(), countSql.toString(), params, params, kPaging);
		}
		
	}

	@Override
	public List<Product> getListProductCanBeAdded(
			int poAutoGroupId, String name, String code, KPaging<Product> kPaging)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append("SELECT p.*");
		sql.append(" FROM product p");
		sql.append(" JOIN product_info info");
		sql.append(" ON info.product_info_id = p.cat_id");
		sql.append(" WHERE 1 =1");
		sql.append(" AND p.status =1");
		sql.append(" AND NOT EXISTS");
		sql.append("   (SELECT 1");
		sql.append("   FROM po_auto_group_detail de");
		sql.append("   JOIN po_auto_group g");
		sql.append("   ON de.po_auto_group_id = g.po_auto_group_id");
		sql.append("   WHERE g.status         = 1");
		sql.append("   AND object_type        =3");
		sql.append("   AND object_id          = p.product_id");
		sql.append("   )");
		sql.append(" AND NOT EXISTS");
		sql.append(" (SELECT 1");
		sql.append(" FROM ap_param ap where ap.type = 'EQUIPMENT_CAT' and ap.status = 1");
		sql.append(" AND ap.ap_param_code  = info.product_info_code");
		sql.append(" )");
		if (!StringUtility.isNullOrEmpty(code)) {
			sql.append(" and lower(p.PRODUCT_CODE) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(code.toLowerCase()));
		}
		if (!StringUtility.isNullOrEmpty(name)) {
			sql.append(" and lower(p.PRODUCT_NAME) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(name.toLowerCase()));
		}
		sql.append(" order by p.PRODUCT_CODE asc");

		if (kPaging == null)
			return repo.getListBySQL(Product.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Product.class,
					sql.toString(), params, kPaging);
	}
	
	@Override
	public Boolean isExistsInfoOfPoAutoGroup(PoAutoGroup poAutoGroup)
			throws DataAccessException {
		if (poAutoGroup == null || poAutoGroup.getId()==null) {
			throw new IllegalArgumentException("poAutoGroup");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" WITH lstInfo AS");
		sql.append("   (SELECT po_auto_group_detail_id,");
		sql.append("     po_auto_group_id,");
		sql.append("     object_type,");
		sql.append("     object_id");
		sql.append("   FROM po_auto_group_detail");
		sql.append("   WHERE po_auto_group_id = ?");
		params.add(poAutoGroup.getId());
		sql.append("  AND status = 1 )");
		sql.append(" SELECT count(1) as count");
		sql.append(" FROM po_auto_group po");
		sql.append(" JOIN po_auto_group_detail dt");
		sql.append(" ON po.po_auto_group_id   = dt.po_auto_group_id");
		sql.append(" WHERE po.status          = 1 AND dt.status = 1");
//		sql.append(" AND po.po_auto_group_id != ?");
//		params.add(poAutoGroup.getId());
		sql.append(" AND EXISTS");
		sql.append("   (SELECT 1");
		sql.append("   FROM lstInfo");
		sql.append("   WHERE lstInfo.object_type = dt.object_type");
		sql.append("   AND lstInfo.object_id     = dt.object_id");
		sql.append("   )");
		int res = repo.countBySQL(sql.toString(), params);
		return res>=1?true:false;
	}
}

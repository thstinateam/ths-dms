package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.EquipDeliveryRecDtl;
import ths.dms.core.entities.EquipDeliveryRecord;
import ths.dms.core.entities.EquipPeriod;
import ths.dms.core.entities.enumtype.EquipmentFilter;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.EquipmentRecordDeliveryVO;
import ths.dms.core.entities.vo.ObjectVO;
import ths.dms.core.exceptions.DataAccessException;
/**
 * EquipmentPeriodDAO
 * 
 * @author nhutnn
 * @since 15/12/2014
 * @description: Lop DAO quan ly ky
 */
public interface EquipmentPeriodDAO {
	
	/**
	 * Lay ky hien tai
	 * @author nhutnn
	 * @since 16/12/2014
	 * @return
	 * @throws DataAccessException
	 */
	EquipPeriod getEquipPeriodCurrent() throws DataAccessException;
}

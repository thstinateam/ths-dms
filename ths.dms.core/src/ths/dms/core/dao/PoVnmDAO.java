package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.PoObjectType;
import ths.dms.core.entities.enumtype.PoType;
import ths.dms.core.entities.enumtype.PoVNMStatus;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.vo.PoCSVO;
import ths.dms.core.entities.vo.PoConfirmVO;
import ths.dms.core.entities.vo.PoSecVO;
import ths.dms.core.entities.vo.PoVnmStockInVO;
import ths.core.entities.vo.rpt.RptPoAutoDataVO;
import ths.core.entities.vo.rpt.RptPoStatusTrackingVO;
import ths.core.entities.vo.rpt.RptPoVnmDebitComparison2VO;
import ths.core.entities.vo.rpt.RptPoVnmVO;
import ths.dms.core.exceptions.BusinessException;
import ths.dms.core.exceptions.DataAccessException;

public interface PoVnmDAO {

	/**
	 * Lay du lieu tu table PO_VNM
	 * @param kPaging
	 * @param poType
	 * @param shopId
	 * @param poVnmNumber
	 * @param fromDate
	 * @param toDate
	 * @param poStatus
	 * @param invoiceNumb
	 * @param hasFindChildShop true: tim trong tat ca cac shop con. False: chi tim trong shop truyen vao
	 * @param shortField
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
//	List<PoVnm> getListPoVnm(KPaging<PoVnm> kPaging, PoType poType,
//			Long shopId, String poVnmNumber, Date fromDate, Date toDate,
//			PoVNMStatus poStatus, String invoiceNumb, boolean hasFindChildShop,
//			String shortField, boolean isLikePoNumber) throws DataAccessException;

	PoVnm createPoVnm(PoVnm poVnm) throws DataAccessException;

	void deletePoVnm(PoVnm poVnm) throws DataAccessException;

	void updatePoVnm(PoVnm poVnm) throws DataAccessException;

	PoVnm getPoVnmById(Long poVnmId) throws DataAccessException;
	
	/**
	 * lay danh sach po_vnm theo status
	*  Mo ta chuc nang cua ham
	*  @author: ThanhNN8
	*  @param poAutoNumber
	*  @param status
	*  @return
	*  @throws DataAccessException
	*  @return: List<PoVnm>
	*  @throws:
	 */
	List<PoVnm> getListPoConfirmByPoAutoNumber(String poAutoNumber, PoVNMStatus status)
			throws DataAccessException;

	boolean checkExistedPoVnmByPoCoNumber(String invoiceNumber)
			throws DataAccessException;

	/**
	 * @author thanhnguyen
	 * @param poAutoNumber
	 * @return
	 * @throws DataAccessException
	 */
	PoVnm getPoDVKH(String poAutoNumber) throws DataAccessException;

	/**
	 * Lay povnm theo code
	 * 
	 * @param code
	 * @return
	 * @throws BusinessException
	 * @author ThuatTQ
	 */
	PoVnm getPoVnmByCode(String code) throws DataAccessException;


	/**
	 * Lay danh sach po confirm phu vu bao cao "Quan ly Po Confirm tu DVKH"
	 * @param shopId
	 * @param poDVKHId
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	/***
	 * @author vuongmq
	 * @date 04/08/2015
	 * @description: lay danh sach PO confirm cua PODV, customize lai truyen vao filter
	 * ham cu: getListPoConfirmForReport(Long shopId, Long poDVKHId)
	 */
	List<PoConfirmVO> getListPoConfirmForReport(PoVnmFilter filter) throws DataAccessException;
	
	/**
	 * 
	 * @author hieunq1
	 * @param poVnmId
	 * @return
	 * @throws DataAccessException
	 */
	PoVnm getPoVnmByIdForUpdate(Long poVnmId) throws DataAccessException;

	/**
	 * Lay list Po DVKH theo dieu kien
	 * @param shopId
	 * @param poAutoNumber
	 * @param fromDate
	 * @param toDate
	 * @param poStatus
	 * @param hasFindChildShop true: lay tat ca shop con, false: chi lay shop truyen vao
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<PoCSVO> getListPoCSVO(Long shopId, String orderNumber, String poCoNumber, Date fromDate,
			Date toDate, PoVNMStatus poStatus, boolean hasFindChildShop) throws DataAccessException;
	List<PoCSVO> getListPoCSVO(Long shopId, String orderNumber, String poCoNumber, Date fromDate,
			Date toDate, List<Integer> lstPoStatus, boolean hasFindChildShop) throws DataAccessException;

	/***
	 * @author vuongmq
	 * @date 03/08/2015
	 * @description lay danh sach POVNM don nhap, don tra
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 */
	List<PoCSVO> getListPoCSVOFilter(PoVnmFilter filter)  throws DataAccessException;
	
	
	/**
	 * 
	 * @author hieunq1
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 */
	List<RptPoVnmDebitComparison2VO> getListRptPoVnmDebitComparison2VO(Long shopId, Date fromDate, Date toDate)
			throws DataAccessException;

	/**Bang ke chung tu hang hoa mua vao
	 * @author hungnm
	 * @param kPaging
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 */
	List<RptPoVnmVO> getListRptPoVnmVO(KPaging<RptPoVnmVO> kPaging,
			Date fromDate, Date toDate, Long shopId) throws DataAccessException;

	/**
	 * @author hungnm
	 * @param fromDate
	 * @param toDate
	 * @param shopId
	 * @return
	 * @throws DataAccessException
	 */
	List<RptPoStatusTrackingVO> getListRptPoStatusTrackingVO(Date fromDate,
			Date toDate, Long shopId) throws DataAccessException;
	
	/**
	 * Lay du lieu cho bao cao POAuto cua nha phan phoi
	 * @param shopId : Danh sach cac nha phan phoi can xem.
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<RptPoAutoDataVO> getListPoAutoForReport(List<Long> shopId,
			Date fromDate, Date toDate) throws DataAccessException;

//	List<PoVnm> getListPoVnmLike(KPaging<PoVnm> kPaging, PoType poType,
//			Long shopId, String poAutoNumber, Date fromDate, Date toDate,
//			PoVNMStatus poStatus, String invoiceNumb, boolean hasFindChildShop,
//			String shortField) throws DataAccessException;

	/**
	 * lay danh sach po dvkh theo shop trong khoang thoi gian. Tat cac cac bien khong duoc null
	 * @param shopId
	 * @param fromDate
	 * @param toDate
	 * @return
	 * @throws DataAccessException
	 * @author ThuatTQ
	 */
	List<PoVnm> getListPoDVKHByShop(Long shopId, Date fromDate, Date toDate,
			Long poVnmId) throws DataAccessException;

	void updatePoVnm(List<PoVnm> listPoVnm) throws DataAccessException;

	List<PoVnm> getListPoVnm(PoVnmFilter filter, KPaging<PoVnm> kPaging)
			throws DataAccessException;

	/**
	 * @author vuongmq
	 * @date 07/08/2015
	 * @description lay danh sach po confirm theo filter
	 * Dung cho Lay danh sach Po confirm dieu chinh hoa don
	 * @param filter
	 * @return
	 * @throws BusinessException
	 */
	List<PoVnm> getListPoConfirmByFilter(PoVnmFilter filter) throws DataAccessException;
	
	void deletePoVnmConfirmDetail(PoVnm poConfirm) throws DataAccessException;

	Integer checkProductInPoDVKH(Long poDvkhId) throws DataAccessException;

	List<PoVnm> getListPoConfirmByPoCS(KPaging<PoVnm> kPaging,
			String poDVKHNumber, PoObjectType objectType, PoType poType)
			throws DataAccessException;
	
	/**
	 * Xu ly getListPoConfirmByPoCSFromPoId
	 * @author vuongmq
	 * @param kPaging
	 * @param poVnmId
	 * @param objectType
	 * @param poType
	 * @return List<PoVnm>
	 * @throws DataAccessException
	 * @since Jan 28, 2016
	*/
	List<PoVnm> getListPoConfirmByPoCSFromPoId(KPaging<PoVnm> kPaging, Long poVnmId, PoObjectType objectType, PoType poType) throws DataAccessException;
	
	Long getPoVnmSeq() throws DataAccessException;

	List<PoVnm> getListPoVnmDVKHForUpdate(Long shopId, String orderNumber,
			PoType objectType) throws DataAccessException;

	List<PoSecVO> getListPoSecVO(KPaging<PoSecVO> paging,Long shopId,
			String poConfirmNumber, Date fromDate, Date toDate, Integer type)
			throws DataAccessException;

	PoVnm getPoDVKHBySaleOrderNumber(PoVnmFilter filter)
			throws DataAccessException;

	List<PoVnm> getListPoConfirmBySaleOrderNumber(PoVnmFilter filter) throws DataAccessException;

	/**
	 * Xu ly getListPoConfirmPoVnmByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<PoVnmStockInVO>
	 * @throws DataAccessException
	 * @since Jan 27, 2016
	*/
	List<PoVnmStockInVO> getListPoConfirmPoVnmByFilter(PoVnmFilter filter) throws DataAccessException;

}

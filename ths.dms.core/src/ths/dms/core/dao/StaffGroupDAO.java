package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.StaffGroup;
import ths.dms.core.entities.StaffGroupDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface StaffGroupDAO {

	StaffGroup createStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws DataAccessException;

	void deleteStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws DataAccessException;

	void updateStaffGroup(StaffGroup staffGroup, LogInfoVO logInfo) throws DataAccessException;
	
	StaffGroup getStaffGroupById(long id) throws DataAccessException;

	List<StaffGroup> getListStaffGroup(KPaging<StaffGroup> kPaging,
			Long shopId, ActiveType activeType) throws DataAccessException;
	
	StaffGroupDetail checkIfStaffExistsInStaffGroup(Long staffId,
			Long staffGroupId, ActiveType activeType)
			throws DataAccessException;

	Boolean checkStaffExistsInShopWithStaffGroup(Long shopId) throws DataAccessException;
}

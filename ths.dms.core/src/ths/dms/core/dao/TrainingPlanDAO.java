package ths.dms.core.dao;

import java.util.Date;
import java.util.List;

import ths.dms.core.entities.TrainingPlan;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.TrainingPlanVO;
import ths.dms.core.exceptions.DataAccessException;

public interface TrainingPlanDAO {

	TrainingPlan createTrainingPlan(TrainingPlan trainingPlan) throws DataAccessException;

	void deleteTrainingPlan(TrainingPlan trainingPlan) throws DataAccessException;

	void updateTrainingPlan(TrainingPlan trainingPlan) throws DataAccessException;
	
	TrainingPlan getTrainingPlanById(long id) throws DataAccessException;
	
    TrainingPlan getTrainingPlanByOwnerIdAndDate(Long ownerId, Date date) throws DataAccessException;
	
	TrainingPlan getTrainingPlanByOwnerCodeAndDate(String ownerCode,Long shopId, Date date) throws DataAccessException;
	
	Integer getCountTrainingPlanDetailById(Long id) throws DataAccessException;
	
	void deleteTrainingPlanByIdDetail(Long id) throws DataAccessException;

	List<TrainingPlanVO> getListTrainingPlan(KPaging<TrainingPlanVO> kPaging, Long shopId, Long nvgsId, Date date, Long shopRootId, Long staffRootId, boolean flagChildCMS) throws DataAccessException;
	
}

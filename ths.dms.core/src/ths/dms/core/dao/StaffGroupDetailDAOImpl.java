package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Staff;
import ths.dms.core.entities.StaffGroupDetail;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.dao.repo.IRepository;

public class StaffGroupDetailDAOImpl implements StaffGroupDetailDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;

	@Autowired
	CommonDAO commonDAO;
	
	@Override
	public StaffGroupDetail createStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws DataAccessException {
		if (staffGroupDetail == null) {
			throw new IllegalArgumentException("staffGroupDetail is null");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		staffGroupDetail.setCreateUser(logInfo.getStaffCode());
		repo.create(staffGroupDetail);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, staffGroupDetail, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		
		
		return repo.getEntityById(StaffGroupDetail.class, staffGroupDetail.getId());
	}

	@Override
	public void deleteStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws DataAccessException {
		if (staffGroupDetail == null) {
			throw new IllegalArgumentException("staffGroupDetail");
		}
		repo.delete(staffGroupDetail);
		try {
			actionGeneralLogDAO.createActionGeneralLog(staffGroupDetail, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public StaffGroupDetail getStaffGroupDetailById(long id) throws DataAccessException {
		return repo.getEntityById(StaffGroupDetail.class, id);
	}
	
	@Override
	public void updateStaffGroupDetail(StaffGroupDetail staffGroupDetail, LogInfoVO logInfo) throws DataAccessException {
		if (staffGroupDetail == null) {
			throw new IllegalArgumentException("staffGroupDetail");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		
		StaffGroupDetail temp = this.getStaffGroupDetailById(staffGroupDetail.getId());
		staffGroupDetail.setUpdateDate(commonDAO.getSysDate());
		staffGroupDetail.setUpdateUser(logInfo.getStaffCode());
		temp = StaffGroupDetail.clone(temp);
		repo.update(staffGroupDetail);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, staffGroupDetail, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public List<StaffGroupDetail> getListStaffGroupDetail(KPaging<StaffGroupDetail> kPaging, Long staffGroupId, ActiveType activeType)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append("select * from staff_group_detail where 1 = 1 ");
		
		if (staffGroupId != null) {
			sql.append(" and staff_group_id = ?");
			params.add(staffGroupId);
		}
		
		if (activeType != null) {
			sql.append(" and status = ?");
			params.add(activeType.getValue());
		}
		else {
			sql.append(" and status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		sql.append(" order by staff_group_detail_id");
		if (kPaging == null)
			return repo.getListBySQL(StaffGroupDetail.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(StaffGroupDetail.class, sql.toString(), params, kPaging);
	}

	@Override
	public List<Staff> getListStaffInStaffGroup(KPaging<Staff> paging, Long staffGroupId, ActiveType activeType) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" select s.* from ");
		sql.append(" (select s.* from staff_group_detail d join staff s on d.staff_id = s.staff_id where 1 = 1 ");
		
		if (staffGroupId != null) {
			sql.append(" and d.staff_group_id = ?");
			params.add(staffGroupId);
		}
		
		if (activeType != null) {
			sql.append(" and d.status = ?");
			params.add(activeType.getValue());
		}
		else {
			sql.append(" and d.status != ?");
			params.add(ActiveType.DELETED.getValue());
		}
		
		sql.append(" ) s join (select channel_type_id, case when object_type=2 then 1 else object_type end object_type from channel_type) ct on s.staff_type_id = ct.channel_type_id  order by ct.object_type desc, s.staff_code");
		if (paging == null)
			return repo.getListBySQL(Staff.class, sql.toString(), params);
		else
			return repo.getListBySQLPaginated(Staff.class, sql.toString(), params, paging);
	}
	

	@Override
	public List<StaffGroupDetail> getListStaffGroupDetailByStaff(Long staffId) throws DataAccessException {
		String sql = "select * from staff_group_detail where staff_id = ? and status != -1";
		List<Object> params = new ArrayList<Object>();
		params.add(staffId);
		return repo.getListBySQL(StaffGroupDetail.class, sql, params);
	}
	
}
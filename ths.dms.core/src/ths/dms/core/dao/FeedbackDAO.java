/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Feedback;
import ths.dms.core.entities.FeedbackStaff;
import ths.dms.core.entities.filter.FeedbackFilter;
import ths.dms.core.entities.vo.FeedbackVO;
import ths.dms.core.exceptions.DataAccessException;

/**
 * FeedbackDAO
 * @author vuongmq
 * @since 11/11/2015 
 */
public interface FeedbackDAO {
	/**
	 * Danh sach Feedback
	 * @author vuongmq
	 * @param filter
	 * @return List<Feedback>
	 * @throws DataAccessException
	 * @since 11/11/2015
	 */
	List<Feedback> getListFeedbackByFilter(FeedbackFilter filter) throws DataAccessException;

	/**
	 * get DS FeedbackStaff
	 * @author vuongmq
	 * @param filter
	 * @return List<FeedbackStaff>
	 * @throws DataAccessException
	 * @since 17/11/2015
	 */
	List<FeedbackStaff> getListFeedbackStaffByFilter(FeedbackFilter filter) throws DataAccessException;
	
	/**
	 * Lay FeedbackVO
	 * @author vuongmq
	 * @param filter
	 * @return FeedbackVO
	 * @throws DataAccessException
	 * @since 11/11/2015
	 */
	FeedbackVO getFeedBackVOByFilter(FeedbackFilter filter) throws DataAccessException;

	/**
	 * Lay danh sach List<FeedbackVO>
	 * @author vuongmq
	 * @param filter
	 * @return List<FeedbackVO>
	 * @throws DataAccessException
	 * @since 11/11/2015
	 */
	List<FeedbackVO> getListFeedbackVOByFilter(FeedbackFilter filter) throws DataAccessException;

	/**
	 * Xu ly getListFeedbackIdCheckByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<FeedbackVO>
	 * @throws DataAccessException
	 * @since Dec 17, 2015
	*/
	List<FeedbackVO> getListFeedbackIdCheckByFilter(FeedbackFilter filter) throws DataAccessException;

}

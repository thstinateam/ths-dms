package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.DisplayToolsProduct;
import ths.dms.core.exceptions.DataAccessException;

public interface DisplayToolsProductDAO {

	DisplayToolsProduct createDisplayToolsProduct(DisplayToolsProduct displayToolsProduct) throws DataAccessException;

	void deleteDisplayToolsProduct(DisplayToolsProduct displayToolsProduct) throws DataAccessException;

	void updateDisplayToolsProduct(DisplayToolsProduct displayToolsProduct) throws DataAccessException;
	
	DisplayToolsProduct getDisplayToolsProductById(long id) throws DataAccessException;

	void deleteListDisplayToolsProduct(List<DisplayToolsProduct> lstDisplayToolsProduct) throws DataAccessException;
	
	void createDisplayToolListProducts(Long id, List<Long> _list)throws DataAccessException;
	
	void deleteDisplayToolListProducts(Long id, List<Long> _list)throws DataAccessException;
}

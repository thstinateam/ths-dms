/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.Po;
import ths.dms.core.entities.PoDetail;
import ths.dms.core.entities.PoVnm;
import ths.dms.core.entities.enumtype.PoVnmFilter;
import ths.dms.core.entities.filter.PoManualFilter;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.entities.vo.PoManualVO;
import ths.dms.core.entities.vo.PoManualVODetail;
import ths.dms.core.entities.vo.PoVnmManualVO;
import ths.dms.core.exceptions.DataAccessException;

/**
 * Mo ta class PoManualDAO.java
 * @author vuongmq
 * @since Dec 7, 2015
 */
public interface PoManualDAO {

	/**
	 * Xu ly createPo
	 * @author vuongmq
	 * @param po
	 * @return Po
	 * @throws DataAccessException
	 * @since Dec 7, 2015
	*/
	Po createPo(Po po) throws DataAccessException;

	/**
	 * Xu ly createListPo
	 * @author vuongmq
	 * @param listPo
	 * @throws DataAccessException
	 * @since Dec 7, 2015
	*/
	void createListPo(List<Po> listPo) throws DataAccessException;

	/**
	 * Xu ly deletePo
	 * @author vuongmq
	 * @param po
	 * @throws DataAccessException
	 * @since Dec 7, 2015
	*/
	void deletePo(Po po) throws DataAccessException;

	/**
	 * Xu ly getPoById
	 * @author vuongmq
	 * @param id
	 * @return Po
	 * @throws DataAccessException
	 * @since Dec 7, 2015
	*/
	Po getPoById(long id) throws DataAccessException;

	/**
	 * Xu ly updatePo
	 * @author vuongmq
	 * @param po
	 * @throws DataAccessException
	 * @since Dec 7, 2015
	*/
	void updatePo(Po po) throws DataAccessException;

	/**
	 * Xu ly updateListPo
	 * @author vuongmq
	 * @param listPo
	 * @throws DataAccessException
	 * @since Dec 7, 2015
	*/
	void updateListPo(List<Po> listPo) throws DataAccessException;

	/**
	 * Xu ly getPoByPoNumber
	 * @author vuongmq
	 * @param poNumber
	 * @return Po
	 * @throws DataAccessException
	 * @since Feb 2, 2016
	*/
	Po getPoByPoNumber(String poNumber) throws DataAccessException;
	
	/**
	 * Xu ly getListPoDetailByPoId
	 * @author vuongmq
	 * @param poId
	 * @return List<PoDetail>
	 * @throws DataAccessException
	 * @since Dec 7, 2015
	*/
	List<PoDetail> getListPoDetailByPoId(Long poId) throws DataAccessException;

	/**
	 * Xu ly getPoDetailByPoDetailIdAndProductId
	 * @author vuongmq
	 * @param poDetailId
	 * @return PoDetail
	 * @throws DataAccessException
	 * @since Dec 7, 2015
	*/
	PoDetail getPoDetailByPoDetailIdAndProductId(Long poDetailId) throws DataAccessException;

	/**
	 * Xu ly getListOrderProductVO
	 * @author vuongmq
	 * @param filter
	 * @return List<OrderProductVO>
	 * @throws DataAccessException
	 * @since Dec 7, 2015
	*/
	List<OrderProductVO> getListOrderProductVO(PoManualFilter<OrderProductVO> filter) throws DataAccessException;

	/**
	 * Xu ly getListPoVNMByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<PoVnm>
	 * @throws DataAccessException
	 * @since Dec 14, 2015
	*/
	List<PoVnm> getListPoVNMByFilter(PoVnmFilter filter) throws DataAccessException;

	/**
	 * Xu ly getListPoVNMManualByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<PoVnmManualVO>
	 * @throws DataAccessException
	 * @since Dec 15, 2015
	 */
	List<PoVnmManualVO> getListPoVNMManualByFilter(PoVnmFilter filter) throws DataAccessException;
	
	/**
	 * Xu ly getListPoVNMDetailByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<OrderProductVO>
	 * @throws DataAccessException
	 * @since Dec 14, 2015
	*/
	List<OrderProductVO> getListPoVNMDetailByFilter(PoVnmFilter filter) throws DataAccessException;

	/**
	 * Xu ly getListPoManualVOByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<PoManualVO>
	 * @throws DataAccessException
	 * @since Dec 18, 2015
	*/
	List<PoManualVO> getListPoManualVOByFilter(PoManualFilter<PoManualVO> filter) throws DataAccessException;

	/**
	 * Xu ly getListPoManualDetailVOByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<PoManualVODetail>
	 * @throws DataAccessException
	 * @since Dec 21, 2015
	*/
	List<PoManualVODetail> getListPoManualDetailVOByFilter(PoManualFilter<PoManualVODetail> filter) throws DataAccessException;

	/**
	 * Xu ly getListPoDetailEditByFilter
	 * @author vuongmq
	 * @param filter
	 * @return List<OrderProductVO>
	 * @throws DataAccessException
	 * @since Dec 28, 2015
	*/
	List<OrderProductVO> getListPoDetailEditByFilter(PoManualFilter<OrderProductVO> filter) throws DataAccessException;

	
}

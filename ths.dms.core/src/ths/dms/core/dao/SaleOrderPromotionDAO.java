/**
 * DAO thao tac voi sale_order_promotion
 * @author tuannd20
 * @version 1.0
 * @since 11/10/2014
 */
package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.SaleOrderPromotion;
import ths.dms.core.entities.enumtype.SoFilter;
import ths.dms.core.entities.filter.SaleOrderPromotionFilter;
import ths.dms.core.entities.vo.OrderProductVO;
import ths.dms.core.exceptions.DataAccessException;

public interface SaleOrderPromotionDAO {
	/**
	 * get list sale order promotion by filter
	 * @author tuannd20
	 * @param filter
	 * @return
	 * @throws DataAccessException
	 * @date 11/10/2014
	 */
	List<SaleOrderPromotion> getSaleOrderPromotions(SaleOrderPromotionFilter filter) throws DataAccessException;
	
	/**
	 * Lay nhung don hang co CTKM khong du so suat
	 * 
	 * @author lacnv1
	 * @date Feb, 09 2015
	 */
	List<OrderProductVO> getOrderNotEnoughPortion(SoFilter filter) throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.List;

import ths.dms.core.entities.CustomerCatLevel;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.enumtype.ParamDAOGetCustomerCatLevelImportVO;
import ths.dms.core.entities.vo.CustomerCatLevelImportVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

public interface CustomerCatLevelDAO {

	CustomerCatLevel createCustomerCatLevel(CustomerCatLevel customerCatLevel, LogInfoVO logInfo) throws DataAccessException;

	void deleteCustomerCatLevel(CustomerCatLevel customerCatLevel, LogInfoVO logInfo) throws DataAccessException;

	void updateCustomerCatLevel(CustomerCatLevel customerCatLevel, LogInfoVO logInfo) throws DataAccessException;
	
	CustomerCatLevel getCustomerCatLevelById(long id) throws DataAccessException;

	Boolean checkIfRecordExist(long customerId, long saleLevelCatId, Long id)
			throws DataAccessException;

	CustomerCatLevel getCustomerCatLevel(Long customerId, Long catId)
			throws DataAccessException;

	List<CustomerCatLevel> getListCustomerCatLevel(
			KPaging<CustomerCatLevel> kPaging, Long customerId,
			Long saleLevelCatId, Boolean isOrderByName)
			throws DataAccessException;
	
	/**
	 * Lay danh muc nghanh hang cua khach hang
	 * 
	 * @param funcParams
	 * @return
	 * @throws DataAccessException
	 * THUATTQ
	 */
	List<CustomerCatLevelImportVO> getCustomerCatLevelImportVO(
			ParamDAOGetCustomerCatLevelImportVO funcParams) throws DataAccessException;
}

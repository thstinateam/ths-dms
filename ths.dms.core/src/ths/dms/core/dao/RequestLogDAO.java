package ths.dms.core.dao;

import ths.dms.core.entities.RequestLog;
import ths.dms.core.exceptions.DataAccessException;

public interface RequestLogDAO {

	RequestLog createRequestLog(RequestLog requestLog) throws DataAccessException;

	void deleteRequestLog(RequestLog requestLog) throws DataAccessException;

	void updateRequestLog(RequestLog requestLog) throws DataAccessException;
	
	RequestLog getRequestLogById(long id) throws DataAccessException;
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Bank;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.KPaging;
import ths.dms.core.entities.filter.BankFilter;
import ths.dms.core.entities.vo.BankVO;
import ths.dms.core.entities.vo.LogInfoVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class BankDAOImpl implements BankDAO {

	@Autowired
	private IRepository repo;
	
	@Autowired
	private ActionGeneralLogDAO actionGeneralLogDAO;
	
	@Override
	public Bank createBank(Bank bank, LogInfoVO logInfo) throws DataAccessException {
		if (bank == null) {
			throw new IllegalArgumentException("bank");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		repo.create(bank);
		try {
			actionGeneralLogDAO.createActionGeneralLog(null, bank, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
		return repo.getEntityById(Bank.class, bank.getId());
	}

	@Override
	public void deleteBank(Bank bank, LogInfoVO logInfo) throws DataAccessException {
		if (bank == null) {
			throw new IllegalArgumentException("bank");
		}
		repo.delete(bank);
		try {
			actionGeneralLogDAO.createActionGeneralLog(bank, null, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}

	@Override
	public Bank getBankById(long id) throws DataAccessException {
		return repo.getEntityById(Bank.class, id);
	}
	
	@Override
	public void updateBank(Bank bank, LogInfoVO logInfo) throws DataAccessException {
		if (bank == null) {
			throw new IllegalArgumentException("bank");
		}
		if (logInfo == null) {
			throw new IllegalArgumentException("logInfo is null");
		}
		Bank temp = this.getBankById(bank.getId());
		repo.update(bank);
		try {
			actionGeneralLogDAO.createActionGeneralLog(temp, bank, logInfo);
		}
		catch (Exception ex) {
			throw new DataAccessException(ex);
		}
	}
	
	@Override
	public Bank getBankByCode(String codeBank) throws DataAccessException {
		codeBank = codeBank.toUpperCase();
		String sql = "select * from BANK where upper(bank_code)=? and status!=?";
		List<Object> params = new ArrayList<Object>();
		params.add(codeBank.toUpperCase());
		params.add(ActiveType.DELETED.getValue());
		Bank rs = repo.getEntityBySQL(Bank.class, sql, params);
		return rs;
	}
	@Override
	public List<Bank> getListBankByShop(Long shopId, ActiveType status) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();	
		sql.append(" select *");
		sql.append(" from bank  where shop_id = ?");
		params.add(shopId);
		if (status == null) {
			sql.append(" and status <> ?");
			params.add(ActiveType.DELETED.getValue());
		} else {
			sql.append(" and status = ?");
			params.add(status.getValue());
		}
		return repo.getListBySQL(Bank.class, sql.toString(), params);
	}
	/**
	 * @author vuongmq
	 * @param shopId
	 * @return order by theo vietnamese bank_name
	 * @throws DataAccessException
	 * 
	 * Lay danh sach ngan hang nhap sec so tay, ASC bank_name
	 */
	@Override
	public List<Bank> getListBankByShopAscBankName(Long shopId) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();	
		sql.append(" select * ");
		sql.append(" from bank  where shop_id = ?");
		params.add(shopId);
		//sql.append(" order by bank_name asc ");
		sql.append(" ORDER BY NLSSORT(bank_name,'NLS_SORT=vietnamese') asc "); // order by theo vietnamese
		return repo.getListBySQL(Bank.class, sql.toString(), params);
	}
	@Override
	public Bank getBankByCodeAndShop(Long shopId,String Code) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();	
		sql.append(" select * ");
		sql.append(" from bank  where shop_id = ?");
		params.add(shopId);
		sql.append(" and upper(bank_code) = ?");
		params.add(Code.toUpperCase());
		return repo.getFirstBySQL(Bank.class, sql.toString(), params);
	}
	
	@Override
	public List<BankVO> getListBank(KPaging<BankVO> kPaging, BankFilter filter)
			throws DataAccessException {
		StringBuilder sql = new StringBuilder();		
		List<Object> params = new ArrayList<Object>();
		sql.append(" select b.bank_id as id, shop.shop_id as shopId, shop.shop_code as shopCode, b.bank_code as bankCode, b.bank_name as bankName,  ");
		sql.append(" b.bank_phone as bankPhone, b.bank_address as bankAddress, b.bank_account as bankAccount, b.status as status  ");
		sql.append(" from bank b join shop on shop.shop_id = b.shop_id where 1 = 1 ");
		if (filter.getShopId() != null) {
			sql.append(" AND b.shop_id in ( SELECT shop_id ");
			sql.append(" FROM shop ");
			sql.append(" START WITH shop_id       = ? ");
			sql.append(" CONNECT BY PRIOR shop_id = parent_shop_id) ");
			params.add(filter.getShopId());
		}
		if (!StringUtility.isNullOrEmpty(filter.getBankCode() ) ) {
			sql.append(" and upper(b.bank_code) like ? ESCAPE '/'");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getBankCode().toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(filter.getBankName())) {
			String temp = StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getBankName().toLowerCase()));
			sql.append(" and lower(b.bank_name) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (!StringUtility.isNullOrEmpty(filter.getBankPhone())) {
			String temp = StringUtility.toOracleSearchLike(filter.getBankPhone().toLowerCase() );
			sql.append(" and lower(b.bank_phone) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (!StringUtility.isNullOrEmpty(filter.getBankAddress())) {
			String temp = StringUtility.toOracleSearchLike(Unicode2English.codau2khongdau(filter.getBankAddress().toLowerCase()));
			sql.append(" and lower(b.bank_address) like ? ESCAPE '/'");
			params.add(temp);
		}
		if (filter.getStatus() != null) {
			sql.append(" and b.status=?");
			params.add(filter.getStatus().getValue());
		} else {
			sql.append(" and b.status!=?");
			params.add(ActiveType.DELETED.getValue());
		}
		sql.append(" order by shop.shop_code, b.bank_code ");
		StringBuilder countSql = new StringBuilder();
		countSql.append("select count(*) as count from ( ");
		countSql.append(sql.toString());
		countSql.append(" ) "); 
		String []fieldNames = {"id","shopId", "shopCode", "bankCode", "bankName",
								"bankPhone", "bankAddress", "bankAccount", "status"};
		Type []fieldTypes = {StandardBasicTypes.LONG, StandardBasicTypes.LONG, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
							StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.INTEGER};
		if (kPaging == null)
			return repo.getListByQueryAndScalar(BankVO.class, fieldNames, fieldTypes, sql.toString(), params);
		else
			return repo.getListByQueryAndScalarPaginated(BankVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, kPaging);
	}
}

package ths.dms.core.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.enumtype.ToolFilter;
import ths.dms.core.entities.vo.ToolVO;
import ths.dms.core.exceptions.DataAccessException;

import ths.dms.core.common.utils.StringUtility;
import ths.dms.core.common.utils.Unicode2English;
import ths.dms.core.dao.repo.IRepository;

public class SuperviseDeviceDAOImpl implements SuperviseDeviceDAO {

	@Autowired
	private IRepository repo;

	@Override
	public List<ToolVO> getListShopTool(Long parentShopId, Integer typeSup)
			throws DataAccessException {
		if (null == parentShopId || null == typeSup) {
			return new ArrayList<ToolVO>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		if(typeSup==0){
			sql.append(" with lstMien as  (select shop_id from shop where status=1 and parent_shop_id=?), ");
			params.add(parentShopId);
			sql.append("  tInfo as (select m.shop_id mienId, v.shop_id vungId, s.shop_id nppId from ( ");
			sql.append("     select TOOL_position_LOG_ID, tool_id,  shop_id ");
			sql.append("     from ( SELECT TOOL_position_LOG_ID, tool_id, shop_id,distance,is_warning, ");
			sql.append("             ROW_NUMBER() OVER(PARTITION BY tool_id,shop_id ORDER BY TOOL_position_LOG_ID DESC) RN ");
			sql.append("             FROM tool_position_log ");
			sql.append("             WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date  ");
			sql.append("           ) where rn = 1 and is_warning=1 ");
			sql.append("       )t  ");
			sql.append(" join shop s on s.shop_id = t.shop_id ");
			sql.append(" join shop v on v.shop_id = s.parent_shop_id ");
			sql.append(" join shop m on m.shop_id = v.parent_shop_id) ");
			sql.append(" select shop_id as shopId,shop_code as shopCode,shop_name as shopName,lat,lng, ");
			sql.append(" (select object_type from channel_type where channel_type_id=s.shop_type_id) as shopType, ");
			sql.append(" (select count(1) from tool where  shop_id in (select shop_id from shop "); 
			sql.append("     where status=1 start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id )) as countTool, "); 
			sql.append(" (select count(1) from tInfo where vungId= s.shop_id or mienId = s.shop_id) countWarning ");  
			sql.append("  from shop s ");   
			sql.append("  where (s.parent_shop_id=? or s.parent_shop_id in (select shop_id from lstMien)) and s.status=1 ");
			params.add(parentShopId);
		}else{
			sql.append(" with lstMien as  (select shop_id from shop where status=1 and parent_shop_id=?), ");
			params.add(parentShopId);
			sql.append("  tInfo as (select m.shop_id mienId, v.shop_id vungId, s.shop_id nppId from ( ");
			sql.append("     select TOOL_temperature_LOG_ID, tool_id,  shop_id ");
			sql.append("     from ( SELECT TOOL_temperature_LOG_ID, tool_id, shop_id,is_warning, ");
			sql.append("             ROW_NUMBER() OVER(PARTITION BY tool_id,shop_id ORDER BY TOOL_temperature_LOG_ID DESC) RN ");
			sql.append("             FROM tool_temperature_log ");
			sql.append("             WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date ");
			sql.append("           ) where rn = 1 AND is_warning=1 ");
			sql.append("       )t  ");
			sql.append(" join shop s on s.shop_id = t.shop_id ");
			sql.append(" join shop v on v.shop_id = s.parent_shop_id ");
			sql.append(" join shop m on m.shop_id = v.parent_shop_id) ");
			sql.append(" select shop_id as shopId,shop_code as shopCode,shop_name as shopName,lat,lng, ");
			sql.append(" (select object_type from channel_type where channel_type_id=s.shop_type_id) as shopType, ");
			sql.append(" (select count(1) from tool where  shop_id in (select shop_id from shop "); 
			sql.append("     where status=1 start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id )) as countTool, "); 
			sql.append(" (select count(1) from tInfo where vungId= s.shop_id or mienId = s.shop_id) countWarning ");  
			sql.append("  from shop s ");   
			sql.append("  where (s.parent_shop_id=? or s.parent_shop_id in (select shop_id from lstMien)) and s.status=1 ");
			params.add(parentShopId);

		}
		final String[] fieldNames = new String[] {
				"shopId","shopCode","shopName", "lat", "lng", 
				"shopType","countTool", "countWarning"
				 };

		final Type[] fieldTypes = new Type[] { //
				StandardBasicTypes.LONG,StandardBasicTypes.STRING,StandardBasicTypes.STRING, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, 
				StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ToolVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<ToolVO> getListTool(Long parentShopId, Integer typeSup)
			throws DataAccessException {
		if (null == parentShopId || null == typeSup) {
			return new ArrayList<ToolVO>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		
		if(typeSup==0){
			sql.append(" with lstTool as  (select * from tool where shop_id=?),  ");
			params.add(parentShopId);
			sql.append("   tInfo as (  select * ");
			sql.append(" 		    from ( SELECT TOOL_position_LOG_ID, tool_id, shop_id,distance,create_date,accuracy,lat,lng,is_warning, ");
			sql.append(" 		            ROW_NUMBER() OVER(PARTITION BY tool_id,shop_id ORDER BY TOOL_position_LOG_ID DESC) RN ");
			sql.append(" 		            FROM tool_position_log ");
			sql.append(" 		            WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date and shop_id=? ");
			params.add(parentShopId);
			sql.append(" 		          ) where rn = 1 ) ");
			sql.append(" 		 select  t.shop_id as shopId,t.tool_id as toolId,i.is_warning as isWarning, ");
			sql.append(" 		 to_char(i.create_date,'HH24:MI') as createDate, ");
			sql.append(" 		 i.accuracy,i.distance,i.lat,i.lng, ");
			sql.append(" 		 t.code as toolCode,t.imei, ");
			sql.append(" 		 to_char(t.product_year,'dd/MM/yyyy')  as productYear, ");
			sql.append(" 		 to_char(t.warranty,'dd/MM/yyyy') as warranty, ");
			sql.append(" 		 (select lat from customer where customer_id=t.customer_id) as latCustomer, ");
			sql.append(" 		 (select lng from customer where customer_id=t.customer_id) as lngCustomer, ");
			sql.append(" 		 (select short_code from customer where customer_id=t.customer_id) as customerCode, ");
			sql.append(" 		 (select customer_name from customer where customer_id=t.customer_id) as customerName, ");
			sql.append(" 		 (select address from customer where customer_id=t.customer_id) as address ");
			sql.append(" 		 from lstTool t join tInfo i on t.tool_id=i.tool_id ");
					
			final String[] fieldNames = new String[] {
					"shopId","toolId", "isWarning",
					"createDate", "accuracy","distance",
					"lat", "lng", "toolCode","imei",
					"productYear","warranty", 
					"latCustomer","lngCustomer", "customerCode",
					"customerName","address"
					 };

			final Type[] fieldTypes = new Type[] { 
					StandardBasicTypes.LONG,StandardBasicTypes.LONG,StandardBasicTypes.INTEGER,
					StandardBasicTypes.STRING,StandardBasicTypes.FLOAT, StandardBasicTypes.LONG,
					StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
					StandardBasicTypes.STRING,StandardBasicTypes.STRING,
					StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT,StandardBasicTypes.STRING,
					StandardBasicTypes.STRING, StandardBasicTypes.STRING };
			return repo.getListByQueryAndScalar(ToolVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
		}else{
			sql.append(" with lstTool as  (select * from tool where shop_id=?), ");
			params.add(parentShopId);
			sql.append(" tInfo as (  select * ");
			sql.append(" 	    from ( SELECT TOOL_temperature_LOG_ID, tool_id, shop_id,temperature,create_date,lat,lng,is_warning, ");
			sql.append(" 	            ROW_NUMBER() OVER(PARTITION BY tool_id,shop_id ORDER BY TOOL_temperature_LOG_ID DESC) RN ");
			sql.append(" 	            FROM tool_temperature_log ");
			sql.append(" 	            WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date and shop_id=? ");
			params.add(parentShopId);
			sql.append(" 	          ) where rn = 1 ) ");
			sql.append(" 	 select  t.shop_id as shopId,t.tool_id as toolId, "); 
			sql.append(" 	 (select min_temp from tool_chip where t.tool_chip_id=tool_chip_id) as minTemp, "); 
			sql.append(" 	 (select max_temp from tool_chip where t.tool_chip_id=tool_chip_id) as maxTemp,  ");
			sql.append(" 	 to_char(i.create_date,'HH24:MI') as createDate,  ");
			sql.append(" 	 i.is_warning as isWarning,  ");
			sql.append(" 	 i.temperature,i.lat,i.lng,  ");
			sql.append(" 	 t.code as toolCode,t.imei,  ");
			sql.append(" 	 to_char(t.product_year,'dd/MM/yyyy')  as productYear, "); 
			sql.append(" 	 to_char(t.warranty,'dd/MM/yyyy') as warranty,  ");
			sql.append(" 	 (select short_code from customer where customer_id=t.customer_id) as customerCode, "); 
			sql.append(" 	 (select customer_name from customer where customer_id=t.customer_id) as customerName,  ");
			sql.append(" 	 (select address from customer where customer_id=t.customer_id) as address  ");
			sql.append(" 	 from lstTool t join tInfo i on t.tool_id=i.tool_id ");
			
			final String[] fieldNames = new String[] {
					"shopId","toolId", "minTemp", "maxTemp",
					"createDate","isWarning","temperature",
					"lat", "lng", "toolCode","imei",
					"productYear","warranty", 
					 "customerCode",
					"customerName","address"
					 };

			final Type[] fieldTypes = new Type[] { 
					StandardBasicTypes.LONG,StandardBasicTypes.LONG,StandardBasicTypes.FLOAT,StandardBasicTypes.FLOAT,
					StandardBasicTypes.STRING,StandardBasicTypes.INTEGER,StandardBasicTypes.FLOAT,
					StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
					StandardBasicTypes.STRING,StandardBasicTypes.STRING,
					StandardBasicTypes.STRING,
					StandardBasicTypes.STRING, StandardBasicTypes.STRING };
			return repo.getListByQueryAndScalar(ToolVO.class,
					fieldNames, fieldTypes, sql.toString(), params);
		}

		
	}
	
	@Override
	public List<ToolVO> getListShopToolOneNode(Long parentShopId, Integer typeSup)
			throws DataAccessException {
		if (null == parentShopId || null == typeSup) {
			return new ArrayList<ToolVO>();
		}

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		if(typeSup==0){//vi tri
			sql.append(" with   tInfo as (select m.shop_id mienId, v.shop_id vungId, s.shop_id nppId from ( ");
			sql.append(" 	    select TOOL_position_LOG_ID, tool_id,  shop_id ");
			sql.append(" 	    from ( SELECT TOOL_position_LOG_ID, tool_id, shop_id,distance,is_warning, ");
			sql.append(" 	            ROW_NUMBER() OVER(PARTITION BY tool_id,shop_id ORDER BY TOOL_position_LOG_ID DESC) RN ");
			sql.append(" 	            FROM tool_position_log ");
			sql.append(" 	            WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date ");
			sql.append(" 	          ) where rn = 1 AND is_warning=1 ");
			sql.append(" 	      )t  ");
			sql.append(" 	join shop s on s.shop_id = t.shop_id ");
			sql.append(" 	join shop v on v.shop_id = s.parent_shop_id ");
			sql.append(" 	join shop m on m.shop_id = v.parent_shop_id) ");
			sql.append(" 	select shop_id as shopId,shop_code as shopCode,shop_name as shopName,lat,lng, ");
			sql.append(" 	(select object_type from channel_type where channel_type_id=s.shop_type_id) as shopType, ");
			sql.append(" 	(select count(1) from tool where  shop_id in (select shop_id from shop "); 
			sql.append(" 	    where status=1 start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id )) as countTool, "); 
			sql.append(" 	(select count(1) from tInfo where  nppId = s.shop_id or vungId= s.shop_id or mienId = s.shop_id) countWarning ");  
			sql.append(" 	 from shop s ");   
			sql.append(" 	 where s.parent_shop_id=? and s.status=1 ");
			params.add(parentShopId);
		}else{//nhiet do
			sql.append(" with   tInfo as (select m.shop_id mienId, v.shop_id vungId, s.shop_id nppId from ( ");
			sql.append(" 	    select TOOL_temperature_LOG_ID, tool_id,  shop_id ");
			sql.append(" 	    from ( SELECT TOOL_temperature_LOG_ID, tool_id, shop_id,is_warning, ");
			sql.append(" 	            ROW_NUMBER() OVER(PARTITION BY tool_id,shop_id ORDER BY TOOL_temperature_LOG_ID DESC) RN ");
			sql.append(" 	            FROM tool_temperature_log ");
			sql.append(" 	            WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date ");
			sql.append(" 	          ) where rn = 1 AND is_warning=1 ");
			sql.append(" 	      )t  ");
			sql.append(" 	join shop s on s.shop_id = t.shop_id ");
			sql.append(" 	join shop v on v.shop_id = s.parent_shop_id ");
			sql.append(" 	join shop m on m.shop_id = v.parent_shop_id) ");
			sql.append(" 	select shop_id as shopId,shop_code as shopCode,shop_name as shopName,lat,lng, ");
			sql.append(" 	(select object_type from channel_type where channel_type_id=s.shop_type_id) as shopType, ");
			sql.append(" 	(select count(1) from tool where  shop_id in (select shop_id from shop "); 
			sql.append(" 	    where status=1 start with shop_id = s.shop_id connect by prior shop_id = parent_shop_id )) as countTool, "); 
			sql.append(" 	(select count(1) from tInfo where  nppId = s.shop_id or vungId= s.shop_id or mienId = s.shop_id) countWarning ");  
			sql.append(" 	 from shop s ");   
			sql.append(" 	 where s.parent_shop_id=? and s.status=1 ");
			params.add(parentShopId);
		}
		final String[] fieldNames = new String[] {
				"shopId","shopCode","shopName", "lat", "lng", 
				"shopType","countTool", "countWarning"
				 };

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.LONG,StandardBasicTypes.STRING,StandardBasicTypes.STRING, StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT, 
		StandardBasicTypes.INTEGER,	StandardBasicTypes.INTEGER, StandardBasicTypes.INTEGER };
		return repo.getListByQueryAndScalar(ToolVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}

	@Override
	public List<Shop> getListAncestor(Long parentShopId, String shopCode,
			String shopName) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" with lstShop as  ");
		sql.append(" ( select shop_id from shop start with shop_id =? connect by prior shop_id =parent_shop_id), ");
		params.add(parentShopId);
		sql.append(" lstSearch as( ");
		sql.append(" select * from shop start with shop_id = "); 
		sql.append("     (select shop_id from shop where status=1 and ( 0=1 ");
		if (!StringUtility.isNullOrEmpty(shopCode)) {
			sql.append(" or shop_code like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(shopCode.toUpperCase()));
		}
		if (!StringUtility.isNullOrEmpty(shopName)) {
			sql.append(" or upper(shop_name) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLike(shopName.toUpperCase()));
		}
		sql.append("   )  and shop_id in ( select shop_id from lstShop)  and rownum=1) ");
		sql.append("   connect by prior parent_shop_id= shop_id  ");
		sql.append(" ) ");
		sql.append(" select * from lstSearch lst1 join lstShop lst2 on lst1.shop_id=lst2.shop_id ");
		return repo.getListBySQL(Shop.class, sql.toString(), params);
	}
	
	@Override
	public List<ToolVO> getListToolKP(ToolFilter filter) throws DataAccessException {
		if (null == filter.getParentShopId() || null == filter.getTypeSup()) {
			return new ArrayList<ToolVO>();
		}
		List<Object> params = new ArrayList<Object>();
		StringBuilder withSql = new StringBuilder();
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		
		withSql.append(" with lstShop  as (select shop_id from shop start with shop_id = ? ");
		params.add(filter.getParentShopId());
		withSql.append("         connect by prior shop_id = parent_shop_id), ");
		withSql.append(" 	 lstTool as  ( ");
		withSql.append(" 	        select m.shop_code mien, v.shop_code vung, s.shop_code shopCode,t.* ");
		withSql.append(" 	      from ( ");
		withSql.append(" 	    	select t.*, c.lat as latCustomer,c.lng as lngCustomer,c.short_code,c.customer_name,c.address "); 
		withSql.append(" 	           from tool t join customer c on c.customer_id=t.customer_id   ");
		withSql.append(" 	           where c.status=1 and t.shop_id in (select * from lstShop) ");
		if(!StringUtility.isNullOrEmpty(filter.getCustomerCode())){
			withSql.append(" 	       and lower(c.short_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().toLowerCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getToolCode())){
			withSql.append(" 	       and lower(t.code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getToolCode().toLowerCase()));
		}
		withSql.append(" 	       ) t ");
		withSql.append(" 	       join shop s on s.shop_id = t.shop_id  ");
		withSql.append(" 	       join shop v on v.shop_id = s.parent_shop_id  ");
		withSql.append(" 	       join shop m on m.shop_id = v.parent_shop_id ");
		withSql.append(" 	       ), ");
		
		if(filter.getTypeSup()==0){
			withSql.append(" 	 tInfo as ( ");
			withSql.append(" 	       select *  from ( "); 
			withSql.append(" 	               SELECT TOOL_position_LOG_ID, tool_id, shop_id,distance,  accuracy,lat,lng,create_date,is_warning, ");
			withSql.append(" 	                ROW_NUMBER() OVER(PARTITION BY tool_id,shop_id ORDER BY TOOL_position_LOG_ID DESC) RN "); 
			withSql.append(" 	                FROM tool_position_log lg ");
			withSql.append(" 	                WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date ");  
			withSql.append(" 	                and shop_id in (select * from lstShop) ");
			if(Boolean.TRUE.equals(filter.getIsWarning())){
				withSql.append(" 	              ) where rn = 1 and is_warning=1 ");
			}else{
				withSql.append(" 	              ) where rn = 1 ");
			}
			withSql.append(" 	         )               ");
			sql.append(withSql.toString());
			sql.append(" 	 select  lst.shop_id as shopId,lst.tool_id as toolId,t.is_warning as isWarning,  ");
			sql.append(" 	 to_char(t.create_date,'HH24:MI') as createDate,  t.accuracy,t.distance,t.lat,t.lng, ");
			sql.append(" 	 lst.code as toolCode,lst.imei,  to_char(lst.product_year,'dd/MM/yyyy')  as productYear, ");
			sql.append(" 	 to_char(lst.warranty,'dd/MM/yyyy') as warranty,  lst.latCustomer as latCustomer,   ");
			sql.append(" 	 lst.lngCustomer as lngCustomer,  lst.short_code as customerCode,  lst.customer_name as customerName, "); 
			sql.append(" 	 lst.address as address, ");
			sql.append(" 	 lst.mien,lst.vung,lst.shopCode ");
			if(Boolean.TRUE.equals(filter.getIsWarning())){
				fromSql.append(" 	 from lstTool lst join tInfo t on lst.tool_id=t.tool_id ");
			}else{
				fromSql.append(" 	 from lstTool lst left join tInfo t on lst.tool_id=t.tool_id ");
			}
			sql.append(fromSql.toString());
			
			final String[] fieldNames = new String[] {
					"shopId","toolId","isWarning", 
					"createDate", "accuracy","distance",
					"lat", "lng", "toolCode","imei",
					"productYear","warranty", 
					"latCustomer","lngCustomer",
					"customerCode", "customerName","address",
					"shopCode","vung","mien"
					 };

			final Type[] fieldTypes = new Type[] { 
					StandardBasicTypes.LONG,StandardBasicTypes.LONG,StandardBasicTypes.INTEGER,
					StandardBasicTypes.STRING,StandardBasicTypes.FLOAT, StandardBasicTypes.LONG,
					StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
					StandardBasicTypes.STRING,StandardBasicTypes.STRING,
					StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT,
					StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
					StandardBasicTypes.STRING, StandardBasicTypes.STRING,StandardBasicTypes.STRING};
			
			countSql.append(withSql.toString());
			countSql.append(" SELECT COUNT(1) AS COUNT ");
			countSql.append(fromSql.toString());
			
			if(null == filter.getkPaging()) {
				return repo.getListByQueryAndScalar(ToolVO.class, fieldNames, fieldTypes, sql.toString(), params);
			} else {
				return repo.getListByQueryAndScalarPaginated(ToolVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
			}
		}else{
			withSql.append(" tInfo as ( ");
			withSql.append("         select *  from ( "); 
			withSql.append("                 SELECT TOOL_temperature_LOG_ID, tool_id, shop_id,is_warning,lat,lng,create_date,temperature, ");
			withSql.append("                  ROW_NUMBER() OVER(PARTITION BY tool_id,shop_id ORDER BY TOOL_temperature_LOG_ID DESC) RN  ");
			withSql.append("                  FROM tool_temperature_log lg ");
			withSql.append("                  WHERE TRUNC(sysdate)<=create_date AND TRUNC(sysdate)+1 >create_date ");  
			withSql.append("                  and shop_id in (select * from lstShop) ");
			if(Boolean.TRUE.equals(filter.getIsWarning())){
				withSql.append("                ) where rn = 1 and is_warning=1 ");
			}else{
				withSql.append("                ) where rn = 1 ");
			}
			withSql.append("           ) ");
			sql.append(withSql.toString());
			sql.append("   select  lst.shop_id as shopId,lst.tool_id as toolId, "); 
			sql.append("   (select min_temp from tool_chip where lst.tool_chip_id=tool_chip_id) as minTemp, ");
			sql.append("   (select max_temp from tool_chip where lst.tool_chip_id=tool_chip_id) as maxTemp, ");
			sql.append("   to_char(t.create_date,'HH24:MI') as createDate,t.temperature,t.is_Warning isWarning, ");
			sql.append("   t.lat,t.lng,lst.code as toolCode,lst.imei,   ");
			sql.append("   to_char(lst.product_year,'dd/MM/yyyy')  as productYear, ");
			sql.append("   to_char(lst.warranty,'dd/MM/yyyy') as warranty,  ");
			sql.append("   lst.short_code as customerCode,  lst.customer_name as customerName, "); 
			sql.append("   lst.address as address, ");
			sql.append("   lst.mien,lst.vung,lst.shopCode ");
			if(Boolean.TRUE.equals(filter.getIsWarning())){
				fromSql.append(" 	 from lstTool lst join tInfo t on lst.tool_id=t.tool_id ");
			}else{
				fromSql.append(" 	 from lstTool lst left join tInfo t on lst.tool_id=t.tool_id ");
			}
			sql.append(fromSql.toString());
			
			final String[] fieldNames = new String[] {
					"shopId","toolId", "minTemp", "maxTemp",
					"createDate","temperature","isWarning",
					"lat", "lng", "toolCode","imei",
					"productYear","warranty",
					 "customerCode","customerName","address",
					"shopCode","vung","mien"
					 };

			final Type[] fieldTypes = new Type[] { 
					StandardBasicTypes.LONG,StandardBasicTypes.LONG,StandardBasicTypes.FLOAT,StandardBasicTypes.FLOAT,
					StandardBasicTypes.STRING,StandardBasicTypes.FLOAT,StandardBasicTypes.INTEGER,
					StandardBasicTypes.FLOAT, StandardBasicTypes.FLOAT,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
					StandardBasicTypes.STRING,StandardBasicTypes.STRING,
					StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING,
					StandardBasicTypes.STRING, StandardBasicTypes.STRING,StandardBasicTypes.STRING};
			
			
			countSql.append(withSql.toString());
			countSql.append(" SELECT COUNT(1) AS COUNT ");
			countSql.append(fromSql.toString());
			
			if(null == filter.getkPaging()) {
				return repo.getListByQueryAndScalar(ToolVO.class, fieldNames, fieldTypes, sql.toString(), params);
			} else {
				return repo.getListByQueryAndScalarPaginated(ToolVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
			}
		}
	}
	
	@Override
	public List<ToolVO> getListChip(ToolFilter filter) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		
		sql.append(" select tool_chip_id as chipId,chip_code as chipCode,CHIP_VER as chipVer, min_temp as minTemp,max_temp as maxTemp ");
		fromSql.append(" from tool_chip where status=1 ");
		if(filter.getChipId()!=null){
			fromSql.append(" and tool_chip_id=? ");
			params.add(filter.getChipId());
		}
		if(!StringUtility.isNullOrEmpty(filter.getChipCode())){
			fromSql.append(" and upper(chip_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getChipCode().toUpperCase()));
		}
		if(filter.getMinTemp()!=null){
			fromSql.append(" and min_temp=? ");
			params.add(filter.getMinTemp());
		}
		if(filter.getMaxTemp()!=null){
			fromSql.append(" and max_temp=? ");
			params.add(filter.getMaxTemp());
		}
		
		final String[] fieldNames = new String[] {
				"chipId","chipCode","chipVer",
				"minTemp", "maxTemp"
				 };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.FLOAT,StandardBasicTypes.FLOAT};
		
		sql.append(fromSql.toString());
		sql.append("  order by chipCode ");
		countSql.append(" SELECT COUNT(1) AS COUNT ");
		countSql.append(fromSql.toString());
		
		if(null == filter.getkPaging()) {
			return repo.getListByQueryAndScalar(ToolVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(ToolVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	
	@Override
	public List<ToolVO> getListChipForCustomer(ToolFilter filter) throws DataAccessException {
		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		StringBuilder fromSql = new StringBuilder();
		StringBuilder countSql = new StringBuilder();
		
		sql.append(" select tc.tool_chip_id as chipId,tc.chip_code as chipCode,tc.CHIP_VER as chipVer, ");
		sql.append(" c.short_code as customerCode,c.customer_name as customerName ");
		fromSql.append(" from tool_chip tc left join tool t on t.tool_chip_id=tc.tool_chip_id ");
		fromSql.append(" left join customer c on c.customer_id=t.customer_id and c.status=1 ");
		fromSql.append(" where tc.status=1 ");
		if(filter.getChipId()!=null){
			fromSql.append(" and tc.tool_chip_id=? ");
			params.add(filter.getChipId());
		}
		if(!StringUtility.isNullOrEmpty(filter.getChipCode())){
			fromSql.append(" and upper(tc.chip_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getChipCode().toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getCustomerCode())){
			fromSql.append(" and upper(c.short_code) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(filter.getCustomerCode().toUpperCase()));
		}
		if(!StringUtility.isNullOrEmpty(filter.getCustomerName())){
			fromSql.append(" and upper(c.name_text) like ? ESCAPE '/' ");
			params.add(StringUtility.toOracleSearchLikeSuffix(Unicode2English.codau2khongdau(filter.getCustomerName()).toUpperCase()));
		}
		
		
		final String[] fieldNames = new String[] {
				"chipId","chipCode","chipVer",
				"customerCode", "customerName"
				 };

		final Type[] fieldTypes = new Type[] { 
				StandardBasicTypes.LONG,StandardBasicTypes.STRING,StandardBasicTypes.STRING,
				StandardBasicTypes.STRING,StandardBasicTypes.STRING};
		
		sql.append(fromSql.toString());
		sql.append("  order by chipCode ");
		countSql.append(" SELECT COUNT(1) AS COUNT ");
		countSql.append(fromSql.toString());
		
		if(null == filter.getkPaging()) {
			return repo.getListByQueryAndScalar(ToolVO.class, fieldNames, fieldTypes, sql.toString(), params);
		} else {
			return repo.getListByQueryAndScalarPaginated(ToolVO.class, fieldNames, fieldTypes, sql.toString(), countSql.toString(), params, params, filter.getkPaging());
		}
	}
	
	@Override
	public void updateChip(Long chipId,Float minTemp,Float maxTemp) throws DataAccessException {
		if(chipId==null || minTemp==null || maxTemp==null){
			throw new IllegalArgumentException("data is not null");
		}
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" update tool_chip set min_temp = ?,max_temp=? "); 
		sql.append(" where tool_chip_id = ?");
		params.add(minTemp);
		params.add(maxTemp);
		params.add(chipId);
		repo.executeSQLQuery(sql.toString(), params);
	}
	
	@Override
	public void updateConfig(int tempConfig, int positionConfig) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" update ap_param set value = ? "); 
		params.add(tempConfig);
		sql.append(" where ap_param_code = ?");
		params.add("TEMP_CONFIG");
		repo.executeSQLQuery(sql.toString(), params);
		sql = new StringBuilder();
		params = new ArrayList<Object>();
		sql.append(" update ap_param set value = ? "); 
		params.add(positionConfig);
		sql.append(" where ap_param_code = ?");
		params.add("POSITION_CONFIG");
		repo.executeSQLQuery(sql.toString(), params);
	}
	
	@Override
	public void updateConfigDistance(int distanceConfig) throws DataAccessException {
		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();
		sql.append(" update ap_param set value = ? "); 
		params.add(distanceConfig);
		sql.append(" where ap_param_code = ?");
		params.add("DISTANCE_CONFIG");
		repo.executeSQLQuery(sql.toString(), params);
	}
	
	@Override
	public List<ToolVO> getBCTemperature(ToolFilter filter) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with lstShop as (select shop_id from shop where status=1 start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
		params.add(filter.getParentShopId());
		sql.append(" select ");
		sql.append(" (select shop_code||'-'||shop_name from shop where shop_id=(select parent_shop_id from shop "); 
		sql.append("       where shop_id = (select parent_shop_id from shop where shop_id=tl.shop_id))) as mien, ");
		sql.append(" (select shop_code||'-'||shop_name from shop where shop_id=tl.shop_id) as shopCode, ");
		sql.append(" (select code from tool where tool_id=tl.tool_id) as toolCode, ");
		sql.append(" (select short_code ||' - '|| customer_name from customer where customer_id=tl.customer_id) as customerCode, ");
		sql.append(" (select address from customer where customer_id=tl.customer_id) as address, ");
		sql.append(" to_char(tl.create_date,'dd/MM/yyyy HH24:MI') as createDate, ");
		sql.append(" (select min_temp||'-'||max_temp||' độ C' from tool_chip where tool_chip_id=(select tool_chip_id from tool where tool_id=tl.tool_id)) as minMaxTemp, ");
		sql.append(" tl.temperature ");
		sql.append(" from tool_temperature_log tl ");
		sql.append(" where tl.shop_id in (select shop_id from lstShop) ");
		sql.append(" and trunc(?)<tl.create_date and tl.create_date<trunc(?)+1 ");
		params.add(filter.getFromDate());
		params.add(filter.getToDate());
		sql.append(" and tl.is_warning=1 ");
		sql.append(" order by mien,shopCode,toolCode ");
		final String[] fieldNames = new String[] {
				"mien","shopCode","toolCode", "customerCode", "address", 
				"createDate","minMaxTemp", "temperature"
				 };

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
		StandardBasicTypes.STRING,StandardBasicTypes.STRING, StandardBasicTypes.FLOAT };
		return repo.getListByQueryAndScalar(ToolVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
	
	@Override
	public List<ToolVO> getBCPosition(ToolFilter filter) throws DataAccessException {

		StringBuilder sql = new StringBuilder();
		List<Object> params = new ArrayList<Object>();

		sql.append(" with lstShop as (select shop_id from shop where status=1 start with shop_id = ? connect by prior shop_id = parent_shop_id) ");
		params.add(filter.getParentShopId());
		sql.append(" select  ");
		sql.append(" (select shop_code||'-'||shop_name from shop where shop_id=(select parent_shop_id from shop "); 
		sql.append("       where shop_id = (select parent_shop_id from shop where shop_id=tl.shop_id))) as mien, ");
		sql.append(" (select shop_code||'-'||shop_name from shop where shop_id=tl.shop_id) as shopCode, ");
		sql.append(" (select code from tool where tool_id=tl.tool_id) as toolCode, ");
		sql.append(" (select short_code ||' - '|| customer_name from customer where customer_id=tl.customer_id) as customerCode, ");
		sql.append(" (select address from customer where customer_id=tl.customer_id) as address, ");
		sql.append(" to_char(tl.create_date,'dd/MM/yyyy HH24:MI') as createDate, ");
		sql.append(" tl.distance ");
		sql.append(" from tool_position_log tl ");
		sql.append(" where tl.shop_id in (select shop_id from lstShop) ");
		sql.append(" and trunc(?)<tl.create_date and tl.create_date<trunc(?)+1 ");
		params.add(filter.getFromDate());
		params.add(filter.getToDate());
		sql.append(" and tl.is_warning=1 ");
		sql.append(" order by mien,shopCode,toolCode ");
		final String[] fieldNames = new String[] {
				"mien","shopCode","toolCode", "customerCode", "address", 
				"createDate","distance"
				 };

		final Type[] fieldTypes = new Type[] { //
		StandardBasicTypes.STRING,StandardBasicTypes.STRING,StandardBasicTypes.STRING, StandardBasicTypes.STRING, StandardBasicTypes.STRING, 
		StandardBasicTypes.STRING,StandardBasicTypes.LONG};
		return repo.getListByQueryAndScalar(ToolVO.class,
				fieldNames, fieldTypes, sql.toString(), params);
	}
}

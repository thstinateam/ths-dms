package ths.dms.context;

import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.util.StringTokenizer;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import org.apache.tomcat.jdbc.pool.DataSource;

import ths.dms.core.common.utils.EncryptionUtils;

public class EncryptedDataSource extends DataSource{

	
	private static final String ENC_KEY = "191-251-228-45-195-23-211-153";

	@Override
	public void setUrl(String url) {
		try {
			url = EncryptionUtils.decrypt(url, getKey());
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.setUrl(url);
	}

	@Override
	public void setUsername(String username) {
		try {
			username = EncryptionUtils.decrypt(username, getKey());
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.setUsername(username);
	}
	
	@Override
	public void setPassword(String password) {
		try {
			password = EncryptionUtils.decrypt(password, getKey());
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.setPassword(password);
	}
	public static void main(String[] args) throws Exception {		
		String url = "jdbc:oracle:thin:@192.168.1.212:1521:kunkun";
		String username = "so_release";
		String password = "so_release";
		System.out.println(EncryptionUtils.encrypt(url, getKey()));
		System.out.println(EncryptionUtils.encrypt(username, getKey()));
		System.out.println(EncryptionUtils.encrypt(password, getKey()));
	}
	private static Key getKey() {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			StringTokenizer st = new StringTokenizer(ENC_KEY, "-", false);
			while (st.hasMoreTokens()) {
				int i = Integer.parseInt(st.nextToken());
				bos.write((byte) i);
			}
			byte[] bytes = bos.toByteArray();
			DESKeySpec pass = new DESKeySpec(bytes);
			SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
			SecretKey s = skf.generateSecret(pass);
			return s;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}

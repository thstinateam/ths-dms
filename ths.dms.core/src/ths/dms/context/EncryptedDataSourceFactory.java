/**
 * 
 */
package ths.dms.context;

import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.RefAddr;
import javax.naming.Reference;
import javax.naming.StringRefAddr;

import org.apache.tomcat.jdbc.pool.DataSourceFactory;

import ths.dms.core.common.utils.EncryptionUtils;


/**
 * @author tudm4
 * @since 11/9/2012
 * @version 1.0
 * encrypt the decrypted authentication information in context.xml 
 *
 */

public class EncryptedDataSourceFactory extends DataSourceFactory {

	// common key for encryption and decryption
	private static final String ENC_KEY = "191-251-228-45-195-23-211-153";

	public EncryptedDataSourceFactory() {}

	public Object getObjectInstance(Object obj, Name name, Context nameCtx,
			Hashtable environment) throws Exception {
		if (obj instanceof Reference) {
			setUsername((Reference) obj);
			setPassword((Reference) obj);
			setUrl((Reference) obj);
		}
		return super.getObjectInstance(obj, name, nameCtx, environment);
	}

	private void setUsername(Reference ref) throws Exception {
		findDecryptAndReplace("username", ref);
	}

	private void setPassword(Reference ref) throws Exception {
		findDecryptAndReplace("password", ref);
	}

	private void setUrl(Reference ref) throws Exception {
		findDecryptAndReplace("url", ref);
	}

	
	private void findDecryptAndReplace(String refType, Reference ref)
			throws Exception {
		int idx = find(refType, ref);
		String decrypted = decrypt(idx, ref);
		replace(idx, refType, decrypted, ref);
	}

	private void replace(int idx, String refType, String newValue, Reference ref)
			throws Exception {
		ref.remove(idx);
		ref.add(idx, new StringRefAddr(refType, newValue));
	}

	private String decrypt(int idx, Reference ref) throws Exception {
		return EncryptionUtils.decrypt(ref.get(idx).getContent().toString(), getKey());
	}

	private int find(String addrType, Reference ref) throws Exception {
		Enumeration<RefAddr> enu = ref.getAll();
		for (int i = 0; enu.hasMoreElements(); i++) {
			RefAddr addr = (RefAddr) enu.nextElement();
			if (addr.getType().compareTo(addrType) == 0)
				return i;
		}

		throw new Exception("The \"" + addrType
				+ "\" name/value pair was not found"
				+ " in the Reference object.  The reference Object is" + " "
				+ ref.toString());
	}
	
	private static Key getKey() {
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			StringTokenizer st = new StringTokenizer(ENC_KEY, "-", false);
			while (st.hasMoreTokens()) {
				int i = Integer.parseInt(st.nextToken());
				bos.write((byte) i);
			}
			byte[] bytes = bos.toByteArray();
			DESKeySpec pass = new DESKeySpec(bytes);
			SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
			SecretKey s = skf.generateSecret(pass);
			return s;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		//kk0618bizt
		//127-134-19-179-99-218-95-20-70-76-163-191-35-6-181-224
		System.out.println("Encrypted Username: " + EncryptionUtils.encrypt("so_release", getKey()));
		System.out.println("Encrypted Password: " + EncryptionUtils.encrypt("so_release", getKey()));
		//jdbc:oracle:thin:@10.58.41.195:1521:kunkun
		//126-122-249-159-58-255-170-1-5-54-227-130-81-214-73-245-42-197-59-116-170-106-203-244-138-162-205-207-154-34-4-113-101-122-155-81-191-17-53-196-249-77-7-84-87-50-112-143
		System.out.println("Encrypted URL: " + EncryptionUtils.encrypt("jdbc:oracle:thin:@192.168.1.212:1521:kunkun", getKey()));				
	}
}


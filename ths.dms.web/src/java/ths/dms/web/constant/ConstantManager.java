/*
 * 
 */
package ths.dms.web.constant;


public class ConstantManager {
	
	public static final String JAVA_IO_TMPDIR = "java.io.tmpdir";
	
	public static final int ERR_EXIST_SPECIAL_CHAR_IN_NUMBER_CONTRACT = 81;
	public static final int ERR_EXIST_SPECIAL_CHAR_IN_SERIAL = 82;
	public static final int ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER = 83;
	public static final int ERR_CHANGE_PASS_NOT_EXIST_UPPER_LOWER = 84;
	public static final int ERR_NUMBER_DOT = 12047;
	public static final int ERR_MAX_LENGTH_EQUALS = 4503;
	
	public static final String SESSION_ACTION_TYPE = "SESSION_ACTION_TYPE"; /** Loai tac dong cua nguoi dung */
	
	/** The Constant FILE_NAME. */
	public static final String SESSION_LOG_ERROR_STANDARD = "SESSION_LOG_ERROR_STANDARD";
	
	public static final String sysMaxNumberDayReportShort = "SYS_MAX_NUMBER_DAY_REPORT_SHORT";
	public static final String sysMaxNumberDayReportMedium = "SYS_MAX_NUMBER_DAY_REPORT_MEDIUM";
	public static final String sysMaxNumberDayReportLong = "SYS_MAX_NUMBER_DAY_REPORT_LONG";
	/* SESSION */
	/** The Constant USER_SESSION_KEY. */
	public static final String USER_SESSION_KEY = "USER_SESSSION_HANDLE";
	
	/** The Constant TOKEN_NAME. */
	public static final String TOKEN_NAME = "token";
	
	/** The Constant SESSION_TOKEN. */
	public static final String SESSION_TOKEN = "SESSION_TOKEN";
	
	/** The Constant FILE_NAME. */
	public static final String SESSION_FILE_NAME = "SESSION_FILE_NAME";
	
	/** The Constant SESSION_FILE_CONTENT_TYPE. */
	public static final String SESSION_FILE_CONTENT_TYPE = "SESSION_FILE_CONTENT_TYPE";
	
	/** The Constant SESSION_HEADER. */
	public static final String SESSION_HEADER = "SESSION_HEADER";
	
	/** The Constant SESSION_SHOP_ID. */
	public static final String SESSION_SHOP_ID = "SESSION_SHOP_ID";
	
	/** The Constant SESSION_SHOP. */
	public static final String SESSION_SHOP = "SESSION_SHOP";
	
	public static final String SESSION_SHOP_CHOOSE = "SESSION_SHOP_CHOOSE";
	
	public static final String SESSION_SALE_ORDER_PROMOTION = "SESSION_SALE_ORDER_PROMOTION";
	
	public static final String SESSION_LOG = "SESSION_LOG"; /** Doi tuong ghi log */
	/* ERROR CODE */
	/** The Constant ERROR_500_CODE. */
	public static final String ERROR_500_CODE = "500";
	
	/** The Constant ERROR_400_CODE. */
	public static final String ERROR_400_CODE = "400";
	
	/** The Constant LOGIN_REDIRECT. */
	public static final String LOGIN_REDIRECT = "login";
	
	/** The Constant VI_LANGUAGE. */
	public static final String VI_LANGUAGE = "vi";
	
	public static final String ISSUED_STOCK_AP_PARAM_CODE = "FXV";	
	
	public static final String RECEIVED_STOCK_AP_PARAM_CODE = "FNV";	
	
	public static final String AP_PARAM_CODE_NUM_DAY_LOT_EXPIRE = "NUM_DAY_LOT_EXPIRE";
	
	/* REPORT APCHE POI */
	public static final int XSSF_MAX_DIGIT_WIDTH = 7;
	public static final double XSSF_CHARACTER_DEFAULT_PADDING = 1.5;
	
	/* CATALOG */
	/** The Constant ACTION_STATUS. */
	public static final long ACTION_STATUS = 1L;
	
	/** The Constant PAUSED_STATUS. */
	public static final long PAUSED_STATUS = 0L;
	
	/** The Constant GRID_NUM_ROW. */
	public static final int GRID_NUM_ROW = 10;
	
	/** The Constant GRID_MAX_NUM_ROW. */
	public static final int GRID_MAX_NUM_ROW = 100;
	
	/** The Constant EXCEL_XLS_TYPE. */
	public static final int EXCEL_XLS_TYPE = 1;
	
	
	/** The Constant EXCEL_XLSX_TYPE. */
	public static final int EXCEL_XLSX_TYPE = 2;
	
	public static final int SALE_ORDER_STATE = 1;
	/** The Constant UTF_8. */
	public static final String UTF_8 = "UTF-8";
	
	/** The Constant JSTREE_STATE_OPEN. */
	public static final String JSTREE_STATE_OPEN = "open";
	public static final String JSTREE_STATE_CLOSE = "closed";
	public static final String JSTREE_STATE_LEAF = "leaf";
	
	/** The Constant JSTree Cat SubCat Product*/
	public static final String JSTREE_NODE_TYPE_CAT = "cat";
	public static final String JSTREE_NODE_TYPE_SUBCAT = "sub-cat";
	public static final String JSTREE_NODE_TYPE_PRODUCT = "product";
	public static final String JSTREE_NODE_TYPE_DEVICE = "device";
	
	
	/** Ban hang - tinh khuyen mai */
	public static final int FREE_PRODUCT = 1;
    public static final int FREE_PRICE = 2;
    public static final int FREE_PERCENT = 3;
    public static final int PROMOTION_FOR_ORDER = 4;
    public static final int PROMOTION_FOR_ORDER_TYPE_PRODUCT = 5;
    public static final int PROMOTION_FOR_ZV21 = 6;
    
    
    public static final Long KEY_PROMTOION_ZV21 = -1L;
    public static final Long KEY_PROMOTION_ZV19_20 = -2L;
	
	/** The Constant ACTION_ADD. */
	public static final int ACTION_ADD = 1;
	
	/** The Constant ACTION_UPDATE. */
	public static final int ACTION_UPDATE = 2;
	
	public static final String FULL_DATE_FORMAT = "dd/MM/yyyy";
	
	public static final String MONTH_YEAR_FORMAT = "MM/yyyy";
	
	public static final String UID_COOKIE = "UTOKEN";
	
	public static final String EMPTY_STRING = "";
	
	public static final String SECRET_KEY_COOKIE = "STOKEN";
	
	public final static String EN_LANGUAGE = "en";
	
	public static final String UTF_8_ENCODING = "UTF-8";
	
	/** Cau hinh font-size phieu giao hang */
	public static final String ORDERNUMBER_FONT_SIZE = "ORDERNUMBER_FONT_SIZE";
	public static final int DEFAULT_FONT_SIZE = 14;
	
	/* MAP */
	/** The Constant MAP_ZOOM_DEFAULT. */
	public static final int MAP_ZOOM_DEFAULT = 16;
	
	/** The Constant MARKER_WIDTH_DEFAULT. */
	public static final int MARKER_WIDTH_DEFAULT = 438;
	
	/** The Constant MARKER_HEIGHT_DEFAULT. */
	public static final int MARKER_HEIGHT_DEFAULT = 198;
	
	/** The Constant LAT_VIETNAM. */
	public static final double LAT_VIETNAM = 14.8152328;
	
	/** The Constant LNG_VIETNAM. */
	public static final double LNG_VIETNAM =106.680505;
	
	/** The Constant MAP_VN_ZOOM_DEFAULT. */
	public static final int MAP_VN_ZOOM_DEFAULT =4;
	
	/** The LA t_ ln g_ empty. */
	public static double LAT_LNG_EMPTY = 0;
	
	/** The Constant CLIENT_NAME_WEB. */
	public static final String CLIENT_NAME_WEB = "WEB";
	
	/** The Constant INVOICE TYPE PAID*/
	public static final long INVOICE_TYPE_PAID = 2;
	
	/** The Constant INVOICE TYPE RECEIVED*/
	public static final long INVOICE_TYPE_RECEIVED = 3;
	
	public static final long NOT_STATUS = -2;
	
	public static final long DELETED = -1;
	
	/** Cycle */
	public static final int MIN_CYCLE_NUM = 1;
	public static final int MAX_CYCLE_NUM = 13;
	
	public static final String IMPORT_FAILED_SESSION = "IMPORT_FAILED_SESSION";
	
	public static final String EXPORT_FILE_EXTENSION = ".xls";
	public static final String EXPORT_FILE_EXTENSION_EX = ".xlsx";
	
	public static final String PP_FAILED_IMPORT = "PP_FAILED_IMPORT";
	
	public static final String PP_FAILED_IMPORT_TEMPLATE = "CTKM_IMPORT_ERROR.xls";
	
	public static final String TEMPLATE_SUPERVISESHOP_MANAGERROUTER_CRETE_FAIL = "Bieu-mau-import-tuyen-file-error.xls";
	/** The Constant TEMPLATE_CATALOG*/
	public static final String EXPORT_CATALOG_DISPLAY_TOOLS_PRODUCT_TEMPLATE = "Bieu_mau_export_quan_ly_tu_san_pham.xls";
//	public static final String EXPORT_CATALOG_DISPLAY_TOOLS_PRODUCT_TEMPLATE = "Bieu_mau_export_quan_ly_tu_san_pham.xlsx";
	public static final String EXPORT_CATALOG_DISPLAY_TOOLS_PRODUCT = "Bieu_mau_export_quan_ly_tu_san_pham";
	public static final String EXPORT_CATALOG_CATEGORY_TYPE_SHOP_CAT_TEMPLATE = "Bieu_mau_export_nganh_hang_ban_cho_donvi.xls";
	
//	public static final String EXPORT_CATALOG_CATEGORY_TYPE_SHOP_CAT_TEMPLATE = "Bieu_mau_export_nganh_hang_ban_cho_NPP.xlsx";
	public static final String EXPORT_CATALOG_CATEGORY_TYPE_SHOP_CAT = "Bieu_mau_export_nganh_hang_ban_cho_donvi";
	public static final String EXPORT_CATALOG_CATEGORY_TYPE_PRODUCT_CAT_TEMPLATE = "Bieu_mau_export_danh_muc_Ton_kho_chuan_theo_san_pham.xls";
	public static final String EXPORT_CATALOG_ACTION_LOG = "Bieu_mau_export_thong_tin_thay_doi.xls";
	public static final String EXPORT_CATALOG_ACTION_LOG_TEMPLATE = "Bieu_mau_export_thong_tin_thay_doi.xls";
	
	public static final String EXPORT_SKU_CUSTOMER_TYPE_TEMPLATE = "Bieu_mau_export_SKU_CUSTOMER_TYPE.xls";
	public static final String EXPORT_SKU_CUSTOMER_TYPE = "Bieu_mau_SKU_CUSTOMER_TYPE";
//	public static final String EXPORT_CATALOG_CATEGORY_TYPE_PRODUCT_CAT_TEMPLATE = "Bieu_mau_export_khai_bao_SP_ban_cho_NPP.xlsx";
	public static final String EXPORT_CATALOG_CATEGORY_TYPE_PRODUCT_CAT = "Bieu_mau_export_danh_muc_Ton_kho_chuan_theo_san_pham";
	public static final String TEMPLATE_CATALOG_PROGRAME_STAFF_MAP = "Bieu_mau_import_so_suat_nvbh_cttb-loi.xls";
	public static final String TEMPLATE_CUSTOMER_DISPLAY_PROGRAM_FAIL = "CTTB_Khachhang_loi.xls";
	
	/** The Constant TEMPLATE_PO. */
	public static final String EXPORT_POCONFIRM = "Doi_chieu_ASN_tu_Sale_order";
	public static final String EXPORT_POCONFIRM_TEMPLATE = "template_Doi chieu PO confirm tu DVKH_export.xls";
	public static final String EXPORT_POAUTO = "BC xuat nhap ton F1";
	public static final String EXPORT_POAUTO_TEMPLATE = "template_BC xuat nhap ton F1_export.xls";
	public static final String TEMPLATE_PO_XNTCT = "template_theo_doi_xuat_nhap_ton_chi_tiet.xls";
	public static final String EXPORT_PO_XNTCT = "theo_doi_xuat_nhap_ton_chi_tiet";
	
	public static final String EXPORT_RETURNPRODUCT_TEMPLATE = "template_BC_tra_hang_VNM.xls";
	public static final String EXPORT_RETURNPRODUCT = "Bao_cao_tra_hang.xls";
	public static final String EXPORT_CATALOG_DISTRIBUTOR_INVOLVE_PROMOTION_TEMPLATE = "Bieu_mau_danh_muc_npp_thamgiakhuyenmai_import.xls";
	public static final String EXPORT_CATALOG_DISTRIBUTOR_INVOLVE_PROMOTION = "Bieu_mau_danh_muc_CTKM_sanpham";
	
	/** The Constant TEMPLATE_STOCK. */
	public static final String EXPORT_ISSUEDSTOCK = "Phieu xuat kho nhan vien";
	public static final String EXPORT_ISSUEDSTOCK_TEMPLATE = "template_StockOutVanSale_export.xls";
	public static final String EXPORT_CATEGORY_TEMPLATE = "template_cycleCount_MapProduct.xls";
	public static final String EXPORT_CATEGORY_STOCK = "Danh sach kiem kho thuc te";
	public static final String EXPORT_ISSUED_STOCK = "Phieu xuat kho nhan vien";
	public static final String EXPORT_UPDATESTOCK_FAIL_TEMPLATE = "template_nhap_xuat_dieu_chinh_loi.xls";
	public static final String EXPORT_UPDATESTOCK_TEMPLATE = "template_nhap_xuat_dieu_chinh.xls";
	public static final String EXPORT_APPROVESTOCK_TEMPLATE = "template_bao_cao_chenh_lech.xls";
	public static final String EXPORT_APPROVESTOCK_TEMPLATE2 = "bao_cao_chenh_lech_kho.xls";
	public static final String EXPORT_APPROVESTOCK = "Bao_cao_chenh_lech";
	/** The Constant TEMPLATE_CUSTOMERDEBIT. */
	public static final String TEMPLATE_CUSTOMER_DEBIT_BATCH_FAIL = "Bieu_mau_cong_no_khach_hang_file_thanh_toan_loi.xls";
	public static final String TEMPLATE_CUSTOMER_DEBIT_TTCKH = "BaoCao_thanh_toan_cua_khach_hang.xls";
	public static final String EXPORT_PRINT_CUSTOMER_DEBIT_TTCKH = "Bao cao thanh toan cua khach hang";
	public static final String IMPORT_CUSTOMER_DEBIT_BATCH_TEMPLATE = "Bieu_mau_import_thanh_toan";
	public static final String IMPORT_CUSTOMER_DEBIT_BATCH_FAIL = "Bieu_mau_import_thanh_toan_loi.xls";
	/** The constant template_shopdebit*/
	public static final String TEMPLATE_SHOP_DEBIT = "Bieu_mau_cong_no_npp.xls";
	/** The Constant TEMPLATE_SALEPRODUCT. */
	public static final String EXPORT_PRINTTAX = "Hoa_don_GTGT";
	public static final String EXPORT_PRINTTAX_TEMPLATE = "Hoa_don_GTGT.xls";
	public static final String EXPORT_PRINT_DELIVERY_ORDER = "Phieu Giao Hang";
	public static final String EXPORT_PRINT_DELIVERY_ORDER_TEMPLATE = "BieuMau_Phieu_Giao_Nhan_Va_Thanh_Toan.xls";
	public static final String EXPORT_PRINT_DELIVERY_ORDER_TOTAL = "Phieu Giao Hang Gop";
	public static final String EXPORT_PRINT_DELIVERY_ORDER_TOTAL_TEMPLATE = "BieuMau_Phieu_Giao_Nhan_Va_Thanh_Toan_Gop.xls";
	public static final String EXPORT_PRINT_EXPORT_DELIVERY_ORDER_TOTAL = "Phieu xuat";
	public static final String EXPORT_PRINT_EXPORT_DELIVERY_ORDER_TOTAL_TEMPLATE = "BieuMau_Phieu_Xuat_Giao_Nhan_Va_Thanh_Toan_Gop_NVGH.xls";
	
	/** The Constant SUPERVISE CUSTOMER. */
	public static final String EXPORT_VISIT_CUSTOMER_TIME_TEMPLATE = "Bao_Cao_Thoi_Gian_Ghe_Tham_Khach_Hang_Cua_NVBH.xls";
	public static final String EXPORT_VISIT_CUSTOMER_TIME = "Bao_Cao_Thoi_Gian_Ghe_Tham_Khach_Hang_Cua_NVBH";
	
	public static final String TEMPLATE_SUPERVISE_CUSTOMER_TIME = "template_thoi_gian_ghe_tham_khach_hang_1.xls";
	public static final String EXPORT_SUPERVISE_CUSTOMER_TIME = "Bao cao thoi gian ghe tham khach hang 1";
	
	public static final String EXPORT_NOT_VISIT_CUSTOMER_TEMPLATE = "Bao_Cao_Khong_Ghe_Tham_Khach_Hang_Trong_Tuyen.xls";
	public static final String EXPORT_NOT_VISIT_CUSTOMER = "Bao_Cao_Khong_Ghe_Tham_Khach_Hang_Trong_Tuyen";
	
	public static final String EXPORT_TURN_OFF_TIME_TEMPLATE = "Bao_Cao_Thoi_Gian_Tat_May.xls";
	public static final String EXPORT_TURN_OFF_TIME = "Bao_Cao_Thoi_Gian_Tat_May";
	
	public static final String EXPORT_SALE_SKUS_DETAIL_CUSTOMER_TEMPLATE = "Bao_Cao_Doanh_So_SKUs_Chi_Tiet_KH.xls";
	public static final String EXPORT_SALE_SKUS_DETAIL_CUSTOMER = "Bao_Cao_Doanh_So_SKUs_Chi_Tiet_KH";
	
	public static final String EXPORT_VT3 = "BaoCao_Ghe_Tham_Khach_Hang_";
	public static final String EXPORT_DS1 = "BaoCao_Doanh_So_Skus_Chi_Tiet_Theo_NVBH";
	public static final String EXPORT_VT7 = "BaoCao_VT7_Ghe_Tham_Khach_Hang";
	public static final String EXPORT_VT7_1 = "BaoCao_VT7_1";
	public static final String EXPORT_VT9 = "VT9_Bao_cao_hinh_anh";
	public static final String EXPORT_VT10 = "BaoCao_VT10_Ket_Qua_Cham_Trung_Bay";
	public static final String EXPORT_VT11 = "VT11_Bao_cao_nhan_vien_mo_clip";
	
	/** The Constant DPPAYPERIOD. */
	public static final String EXPORT_DPPAYPERIOD_MANAGER = "BieuMauQuanLyTraThuongCTTB";
	public static final String EXPORT_DPPAYPERIOD_MANAGER_TEMPLATE = "BieuMauQuanLyTraThuongCTTB.xls";
	public static final String EXPORT_DPPAYERIOD_TEMPLATE_EXPORT = "Ket_qua_tra_thuong_CTTB_export.xls";
	public static final String EXPORT_DPPAYERIOD_TEMPLATE_EXPORT2 = "Bieu_mau_Ket_qua_tra_thuong_CTTB_export.xls";
	public static final String EXPORT_DPPAYERIOD_TEMPLATE_FILE_NAME = "Ket qua tra thuong CTTB";
	public static final String EXPORT_CUSTOMER_DEBIT_TEMPLATE_EXPORT = "Bao_cao_cong_no_KH.xls";
	public static final String EXPORT_USTOMER_DEBIT_TEMPLATE_FILE_NAME = "Bao_cao_cong_no_khach_hang_";
	public static final String EXPORT_CUSTOMER_DEBIT_PAY = "Bao_cao_thanh_toan_cua_khach_hang.xls";
	
	
	/** BAO CAO CRM */
	public static final String EXPORT_DK1_1 = "KD1.1_Doanh_so_phan_phoi_theo_SKUs_";
	public static final String EXPORT_DK1_2 = "KD1.2_Doanh_so_phan_phoi_theo_nhan_hang_";
	public static final String EXPORT_DK1_3 = "KD1.3_Doanh_so_phan_phoi_theo_nhom_hang_";
	public static final String EXPORT_KD8_1 = "KD8.1_Bao_cao_NVBH_diem_kem.xls";
	public static final String EXPORT_KD16_1 = "KD16.1_Thong_ke_so_xa_chua_psds.xls";
	public static final String EXPORT_KD9= "KD9_SKus_cham_ban_tren_30_ngay_";
	public static final String EXPORT_KD19_1 = "KD19.1_bao_cao_gsnpp_bi_diem_kem";
	
	/** BAO CAO THUE **/
	public static final String EXPORT_BKCTHDGTGT = "Bao_cao_bang_ke_chi_tiet_hoa_don_GTGT_";
	public static final String EXPORT_DCHDHTVAHDGTGT = "Bao_cao_doi_chieu_HD_he_thong_va_Hd_GTGT_";
	
	/** BAO CAO THUE **/
	public static final String TEMPLATE_BAO_CAO_PO_AUTO_NPP = "Bao_cao_PO_Auto_NPP.xls";
	
	/**BAO CAO DOANH THU**/
	public static final String EXPORT_BTHDH = "Bang_tong_hop_doi_hang_";
	public static final String EXPORT_BCTDCTDS = "Bao_cao_theo_doi_chi_tieu_doanh_so_";
	public static final String EXPORT_BCN = "Bao_cao_ngay_";
	public static final String EXPORT_PTHCKH = "Phieu_Tra_Hang_Cua_Khach_Hang_";
	public static final String EXPORT_BKCTCTMH = "Bang_ke_chi_tiet_chung_tu_mua_hang_";
	public static final String EXPORT_HDGNVTT = "Hoa_Don_Giao_Nhan_Va_Thanh_Toan";
	public static final String EXPORT_CTKMTCT = "Chi_tiet_KM_theo_chuong_trinh_";
	public static final String EXPORT_CTKMTCTNV = "Chi_tiet_KM_theo_chuong_trinh_nhan_vien_";
	public static final String EXPORT_CTCTHTB = "Chi_tiet_chi_tra_hang_trung_bay_";
	public static final String EXPORT_TDBHTMH = "Theo_doi_ban_hang_theo_mat_hang_";
	public static final String EXPORT_THCTHTB = "Tong_hop_chi_tra_hang_trung_bay_";
	public static final String EXPORT_TDTTCN_KH = "Theo_doi_thanh_toan_cong_no_theo_khach_hang_";
	public static final String EXPORT_7_2_12 = "Bao_cao_7_2_12_phan_tich_doanh_thu_ban_hang_theo_NPP_";
	public static final String EXPORT_7_2_13 = "Bao_cao_7_2_13_phan_tich_doanh_thu_ban_hang_theo_NVBH_";
	
	/**BAO CAO THEO DOI HANG NGAY**/
	public static final String EXPORT_PXHTNVGH = "Phieu_Xuat_Hang_Theo_Nhan_Vien_Giao_Hang";
	public static final String EXPORT_PXHTNVBH = "Phieu_Xuat_Hang_Theo_Nhan_Vien_Ban_Hang";
	
	public static final String EXPORT_VAT = "Vat_template";
	
	public static final String IMPORT_VAT = "template-vat.";
	
	public static final int PROMOTION_AUTO = 1;
	
	public static final int PROMOTION_MANUAL = 2;
	
	public static final int CUSTOMER_VALUE = 2;
	
	public static final int CUSTOMER_TYPE_VALUE = 1;
	
	public static final long TYPE_SHOP = 11L;
	
//    public static final String PATH_TO_GALLERY = "/media/image/";
//    
//    public static final String PATH_TO_GALLERY_THUMBNAIL = "/media/bigthumbnail/";
//    
//    public static final String PATH_TO_VIDEO = "/media/video/";
	
	public static final String PATH_TO_FORDER = "/ProductImage/";
	
    public static final String PATH_TO_GALLERY = "/images/";
    
    public static final String PATH_TO_GALLERY_THUMBNAIL = "/Thumb/";
    
    public static final String PATH_TO_GALLERY_BIGTHUMBNAIL = "/Images/";
    
    public static final String PATH_TO_VIDEO = "/Media/";
    
    public static final String PATH_TO_ATTACH_FILES = "/image/ATTACH_FILES/";
    
    public static final String PRE_PROMOTION_TYPE = "ZV";
		
	/** The Constant TEMPLATE_CATALOG_LEVEL_FAIL. */
    public static final String TEMPLATE_DISPLAY_SHOP_MAP_FAIL="Bieu_mau_danh_muc_CTTB_don_vi_export_fail.xls";
    public static final String TEMPLATE_DISPLAY_PROGRAM_FAIL="Bieu_mau_danh_muc_CTTB_export_fail.xls";
	public static final String TEMPLATE_CATALOG_LEVEL_FAIL = "Bieu_mau_danh_muc_muc_import-file_loi.xls";	
	public static final String TEMPLATE_CATALOG_CAR_FAIL = "Bieu_mau_danh_muc_quan_ly_xe_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_GROUP_TRANSFER_FAIL = "Bieu_mau_danh_muc_nhomgiaohang_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_GROUP_TRANSFER_CUSTOMER_FAIL = "Bieu_mau_danh_muc_khcuanhomgiaohang_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_DISPLAY_PROGRAM_FAIL = "Bieu_mau_danh_muc_cttb_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_DISPLAY_PROGRAM_CUSTOMER_FAIL = "Bieu_mau_danh_muc_cttb_khach_hang_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_DISPLAY_PROGRAM_LEVEL_FAIL = "Bieu_mau_danh_muc_cttb_muc_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_DISPLAY_PROGRAM_QUOTA_GROUP_FAIL = "Bieu_mau_danh_muc_cttb_nhom_chi_tieu_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_DISPLAY_PROGRAM_PRODUCT_FAIL = "Bieu_mau_danh_muc_cttb_Sanpham_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_DISPLAY_PROGRAM_CUSTOMER_JOIN_FAIL = "Bieu_mau_danh_muc_cttb_NV_phattrien_KH_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_FOCUS_PROGRAM_FAIL = "Bieu_mau_danh_muc_cttt_import-file_loi.xls";
//	public static final String TEMPLATE_CATALOG_FOCUS_PROGRAM_SHOP_FAIL = "Bieu_mau_danh_muc_cttt_donvi_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_FOCUS_PROGRAM_SHOP_FAIL = "Bieu_mau_danh_muc_NPPThamgiavaoCTTT_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_FOCUS_PROGRAM_STAFF_FAIL = "Bieu_mau_danh_muc_cttt_HTBH_TTSP_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_PP_INFO_FAIL = "Bieu_mau_danh_muc_CTKM_thongtinchung_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_PP_PRODUCT_FAIL = "Bieu_mau_danh_muc_CTKM_sanphamkhuyenmai_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_PP_SHOP_FAIL = "Bieu_mau_danh_muc_CTKM_Donvi_file_loi.xls";
	public static final String TEMPLATE_CATALOG_PP_MANUAL_SHOP_FAIL = "Bieu_mau_chuong_trinh_khuyen_mai_thu_cong_tab_don_vi_file_loi.xls";
	public static final String TEMPLATE_CATALOG_PP_STAFF_FAIL = "Bieu_mau_danh_muc_CTKM_NVBH_loi.xls";
	public static final String TEMPLATE_SALEMT_CATALOG_PP_SHOP_FAIL = "MT_Bieu_mau_danh_muc_CTKM_Donvithamgia_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_PP_CUSTOMER_FAIL = "Bieu_mau_danh_muc_CTKM_KHthamgia_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_PP_CUSTOMER_TYPE_FAIL = "Bieu_mau_danh_muc_CTKM_Loai_KHthamgia_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_AUTO_PP_INFO_FAIL = "Bieu_mau_danh_muc_HTTM_thongtinchung_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_AUTO_PP_SHOP_FAIL = "Bieu_mau_danh_muc_HTTM_Donvithamgia_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_AUTO_PP_CUSTOMER_FAIL = "Bieu_mau_danh_muc_HTTM_KHthamgia_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_AUTO_PP_CUSTOMER_TYPE_FAIL = "Bieu_mau_danh_muc_HTTM_Loai_KHthamgia_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_PRODUCT_INFO_FAIL = "Bieu_mau_danh_muc_SP_thongtinchung_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_PRODUCT_QUANTITY_FAIL = "Bieu_mau_danh_muc_SP_tonkhoantoan_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_PRODUCT_DEPARTMENT_FAIL = "Bieu_mau_danh_muc_SP_phongbansudung_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_PRODUCT_PRICE_FAIL = "Bieu_mau_danh_muc_SP_gia_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_STAFF_INFO_FAIL = "Bieu_mau_danh_muc_NV_thongtinnhanvien_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_STAFF_INFO_FAIL_NEW = "Bieu_mau_danh_muc_nhan_vien_don_vi_import_file_loi.xls";
	public static final String TEMPLATE_CATALOG_UNIT_INFO_FAIL_NEW = "Bieu_mau_danh_muc_don_vi_import_file_loi.xls";
	public static final String TEMPLATE_CATALOG_STAFF_FAIL_NEW = "Bieu_mau_danh_muc_nhan_vien_import_file_loi.xls";
	public static final String TEMPLATE_CATALOG_STAFF_FAIL_WITHOUT_DYNAMIC_ATTRIBUTE = "Bieu_mau_danh_muc_nhan_vien_import_loi.xls";
	public static final String TEMPLATE_CATALOG_STAFF_CATEGORY_FAIL = "Bieu_mau_danh_muc_NV_thongtinnganhhang_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_CATEGORY_FAIL = "Bieu_mau_danh_muc_muc_doanh_so_nganh_hang_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_PRODUCT_SHOP_FAIL = "Bieu_mau_danh_muc_Ton_kho_chuan_theo_san_pham_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_CATEGORY_SHOP_FAIL = "Bieu_mau_danh_muc_Ton_kho_chuan_theo_nganh_hang_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_SALE_DAY_FAIL = "Bieu_mau_danh_muc_so_ngay_lam_viec_import-file_loi.xls";
//	public static final String TEMPLATE_CATALOG_CUSTOMER_FAIL = "Bieu_mau_danh_muc_khach_hang_import-file_loi.xls";
	
	public static final String TEMPLATE_CATALOG_CUSTOMER_SUBCAT_FAIL = "Bieu_mau_danh_muc_khach_hang_ngang_hang_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_SKU_CUSTOMER_TYPR_FAIL = "Bieu_mau_danh_muc_muc_sku_loai_khach_hang_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_TOTAL_MONEY_QUOTA_FAIL = "Bieu_mau_danh_muc_chi_tieu_doanh_so_loai_khach_hang_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_REASON_FAIL = "Bieu_mau_danh_muc_lydo_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_AREA_TREE_FAIL = "Bieu_mau_danh_muc_cay_dia_ban_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_UNIT_TREE_FAIL = "Bieu_mau_danh_muc_cay_don_vi_import-file_loi.xls";
	public static final String TEMPLATE_SALE_PLAN_CREATE_FAIL = "Bieu_mau_KHTT_phan_bo_ke_hoach_tieu_thu_thang_import-file_loi.xls";
	public static final String TEMPLATE_SALE_PLAN_CREATE = "Bieu_mau_KHTT_phan_bo_ke_hoach_tieu_thu.xls";
	public static final String TEMPLATE_SALE_PLAN_UNIT_CREATE = "Bieu_mau_KHTT_phan_bo_chi_tieu_thang.xls";
	public static final String TEMPLATE_SALE_PRODUCT_CREATE_ORDER_IMPORT_FAIL = "template_import_sale_order_fail.xls";
	public static final String TEMPLATE_SALE_PRODUCT_CREATE_ORDER_DISPLAY_IMPORT_FAIL = "template_import_sale_order_display_fail.xls";
	public static final String TEMPLATE_SALE_PLAN_QUOTA_MONTH_FAIL = "Bieu_mau_KHTT_phan_bo_ke_hoach_tieu_thu_thang_NPP_import-file_loi.xls";
	public static final String TEMPLATE_SALE_PLAN_QUOTA_EXPORT = "phan_bo_chi_tieu_thang_export.xls";
	public static final String TEMPLATE_SALE_PLAN_CREATE_EXPORT = "lap_ke_hoach_tieu_thu_thang_export.xls";
	public static final String TEMPLATE_STOCK_COUNTINGINPUT_FAIL = "Bieu_mau_kho_nhap_kiem_ke_import-file_loi.xls";
	public static final String TEMPLATE_SALE_PLAN_QUOTA_YEAR_FAIL = "Bieu_mau_KHTT_phan_bo_ke_hoach_tieu_thu_nam_theo_nganh_hang_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_DISPLAY_TOOL_FAIL = "Bieu_mau_danh_muc_KH_su_dung_tu_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_DISPLAY_TOOL_PRODUCT_FAIL = "Bieu_mau_danh_muc_quan_ly_tu_san_pham_import-file_loi.xls";
	public static final String TEMPLATE_SUPERVISESHOP_MANAGE_TRAININGPLAN_GSNPP = "Bieu_mau_ke_hoach_huan_luyen_GSNPP_import-file_loi.xls";
	public static final String TEMPLATE_SUPERVISESHOP_MANAGE_TRAININGPLAN_TBHV = "Bieu_mau_ke_hoach_huan_luyen_TBHV_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_PROMOTION_MANUAL = "Bieu_mau_danh_muc_CTHTTM_export_fail.xlsx";
	public static final String TEMPLATE_CATALOG_PROMOTION_PRODUCT_EXPORT = "Du_lieu_CTKM_sanphamkhuyenmai.xls";
	public static final String TEMPLATE_PROMOTION_SHOP_MAP_EXPORT = "Bieu_mau_export_Don_vi_CTKM";
	public static final String TEMPLATE_PROMOTION_STAFF_MAP_EXPORT = "Bieu_mau_export_NVBH_CTKM";
	public static final String TEMPLATE_PROMOTION_CUSTOMER_MAP_EXPORT = "Bieu_mau_export_KH_CTKM";
	public static final String TEMPLATE_CATALOG_PROMOTION_IMPORT_EXCEL = "Bieu_mau_danh_muc_CTKM_import_fail.xls";
	public static final String TEMPLATE_CATALOG_LEVEL_EXPORT = "Du_lieu_Danh_muc_Muc_SP.xls";
	public static final String TEMPLATE_ATTRIBUTE_EXPORT = "template_attribute_export.xls";
	public static final String TEMPLATE_CATALOG_AREA_EXPORT = "Du_lieu_Danh_muc_Dia_ban.xls";
	public static final String TEMPLATE_CATALOG_AREA_IMPORT = "Bieu_mau_danh_muc_cay_dia_ban.xls";
	public static final String TEMPLATE_CATALOG_CAR_EXPORT = "Du_lieu_Danh_muc_Quan_ly_xe.xls";
	public static final String TEMPLATE_CATALOG_DELIVERY_EXPORT = "Du_lieu_Danh_muc_Nhom_giao_hang.xls";
	public static final String TEMPLATE_ATTRIBUTE_FAIL = "Bieu_mau_danh_muc_thuoc_tinh_import-file_loi.xls";
	public static final String TEMPLATE_CATALOG_REASON_EXPORT = "Du_lieu_Danh_muc_Ly_do.xls";
	public static final String TEMPLATE_CATALOG_PRODUCT_EXPORT = "Du_lieu_Danh_muc_San_pham.xls";
	public static final String TEMPLATE_CATALOG_FOCUS_EXPORT = "Du_lieu_Danh_muc_CTTT.xls";
	public static final String TEMPLATE_CATALOG_CUSTOMER_EXPORT = "Du_lieu_danh_muc_khachhang.xls";
//	public static final String TEMPLATE_CATALOG_CUSTOMER_EXPORT = "export_customer_list.xlsx";
	
	/**
	 * file jasper for very large data export
	 */
	public static final String TEMPLATE_CATALOG_CUSTOMER_EXPORT_JASPER = "Du_lieu_danh_muc_khachhang.jasper";
	public static final String TEMPLATE_CUSTOMERDEBIT_BATCH_EXPORT = "Danh_sach_don_hang_con_no.xls";
	public static final String TEMPLATE_CATALOG_CATOGORY_CUSTOMER_EXPORT = "Du_lieu_danh_muc_nganhhangcuakhachhang.xls";
	public static final String TEMPLATE_EXPORT_TUYEN = "template-export-tuyen.xls";
	public static final String TEMPLATE_INCENTIVE_PROGRAM_FAIL = "Bieu_mau_danh_muc_CTKT_thongtinchung_loi.xls";
	public static final String TEMPLATE_INCENTIVE_PROGRAM_SHOP_FAIL = "Bieu_mau_danh_muc_CTKT_donvi_loi.xls";
	public static final String TEMPLATE_LEVEL_INCOME_EXPORT = "Bieu_mau_danh_muc_muc_doanh_so_theo_nganh.xls";
	public static final String TEMPLATE_INCENTIVE_LEVEL_FAIL = "Bieu_mau_danh_muc_CTKT_muc_loi.xls";
	public static final String TEMPLATE_INCENTIVE_PRODUCT_FAIL = "Bieu_mau_danh_muc_CTKT_sanpham_loi.xls";
	public static final String TEMPLATE_INCENTIVE_PRODUCT_EXPORT = "Du_lieu_Danh_muc_CTKT_sanpham.xls";
	public static final String TEMPLATE_INCENTIVE_LEVEL_EXPORT = "Du_lieu_Danh_muc_CTKT_muc.xls";
	public static final String TEMPLATE_INCENTIVE_SHOP_EXPORT = "Du_lieu_Danh_muc_CTKT_donvi.xls";
	public static final String TEMPLATE_CATALOG_UNIT_FAIL = "Du_lieu_Danh_muc_Cay_don_vi.xls";
	public static final String TEMPLATE_STAFF_EXPORT = "Bieu_mau_danh_muc_NV_thongtinnhanvien.xls";
	public static final String TEMPLATE_STAFF_EXPORT_NEW = "Bieu_mau_danh_muc_nhan_vien_don_vi_export.xls";
	public static final String TEMPLATE_STAFF_EXPORT_FILE_NAME = "Bieu_mau_danh_sach_nhan_vien";
	public static final String TEMPLATE_UNITS_EXPORT_FILE_NAME = "Units";
	public static final String TEMPLATE_STAFF_EXPORT_SALE_CAT = "Bieu_mau_danh_muc_nghanh_hang_cuaNV.xls";
	public static final String TEMPLATE_DEVICE_MANAGER_FAIL = "Bieu_mau_Quan_ly_thiet_bi_import-file_loi.xls";
	public static final String TEMPLATE_CONTRACT_MANAGER_FAIL = "Bieu_mau_quan_ly_hop_dong_thiet_bi_import_fail.xls";
	public static final String TEMPLATE_INFO_PAYROLL_ELEMENT_EXPORT = "Bieu_Mau_Phan_Tu_Luong.xls";
	public static final String TEMPLATE_DPPAYPERIOD_CALCULATE_FAIL="Bieu_mau_danh_muc_ky_tra_thuong_CTTB_loi.xls";
	public static final String TEMPLATE_CATALOG_FOCUS_SHOP_EXPORT = "Du_lieu_Danh_muc_CTTT_Don_vi_tham_gia.xls";
	public static final String TEMPLATE_CATALOG_FOCUS_STAFF_EXPORT = "Du_lieu_Danh_muc_CTTT_HTBH_TTSP.xls";
	public static final String TEMPLATE_CATALOG_DELIVERY_CUSTOMER_EXPORT = "Du_lieu_Danh_muc_Nhom_giao_hang_Khach_hang.xls";
	public static final String TEMPLATE_CATALOG_PRODUCT_PRICE_EXPORT = "Du_lieu_Danh_muc_San_pham_Gia.xls";
	public static final String TEMPLATE_INFO_PAYROLL_ELEMENT_FAIL = "Bieu_Mau_Phan_Tu_Luong_Loi.xls";
	public static final String TEMPLATE_CALCULATE_PAYROLL_EXPORT = "template_calculate_payroll_export.xls";
	public static final String TEMPLATE_SALE_DAY_EXCEPTION_FAIL = "Bieu_mau_KHTT_thiet_lap_ngay_ban_hang_import-file_loi.xls";
	public static final String TEMPLATE_SALE_DAY_EXCEPTION_EXPORT = "Thiet_lap_ngay_ban_hang.xls";
	public static final String TEMPLATE_CATALOG_FOCUS_SHOP_MAP_EXPORT = "Du_lieu_Danh_muc_CTTT_Don_vi_tham_gia.xls";
	public static final String TEMPLATE_CATALOG_PP_SHOP_EXPORT = "Bieu_mau_danh_muc_CTKM_Donvithamgia_export.xls";
	public static final String TEMPLATE_SALEMT_CATALOG_PP_SHOP_EXPORT = "SaleMT_Bieu_mau_danh_muc_CTKM_Donvithamgia_export.xls";
	public static final String TEMPLATE_CATALOG_DISPLAY_SHOP_MAP_EXPORT = "Bieu_mau_danh_muc_CTTB_don_vi_export.xls";
	public static final String EXPORT_EXCEL_DISPLAY_PROGRAM_CUSTOMER = "Danh_sach_khach_hang_cttb";
	public static final String TEMPLATE_DTBH_DSKHTMH_EXPORT = "rpt_dtbh_dskhtmh.xls";
	
	public static final String TEMPLATE_CATALOG_CUSTOMMER = "Bieu_mau_danh_muc_khachhang_import_multi_language.xls";
	public static final String TEMPLATE_CATALOG_CUSTOMER_FAIL = "Bieu_mau_danh_muc_khachhang_import_multi_language_loi.xlsm";
	public static final String TEMPLATE_CATALOG_CUSTOMER_FAIL_WITHOUT_DYNAMIC_ATTRIBUTE = "Bieu_mau_danh_muc_khachhang_import_multi_language_file_loi.xlsm";
	
	//update follow new request from Nuti 
	//public static final String TEMPLATE_CATALOG_CUSTOMMER = "Import_customers_template.xls";
	//public static final String TEMPLATE_CATALOG_CUSTOMER_FAIL = "Import_customers_template_error.xlsx";
	
	public static final String TEMPLATE_CATALOG_CATEGORY = "Bieu_mau_danh_muc_nganhhangcuakhachhang_import.xls";
	public static final String TEMPLATE_OFFICE_DOCUMENT_EXPORT_EXCEL = "export_CongVan.xls";
	public static final String TEMPLATE_TOOLCUSTOMER = "ThietBi_KhachHang_Loi.xls";
	public static final String TEMPLATE_SUB_CAT_UNIT = "Bieu_mau_nganh_hang_con.xls";
	public static final String TEMPLATE_KEY_SHOP_KS = "Bieu_mau_key_shop_ks.xls";
	public static final String TEMPLATE_KEY_SHOP_KS_FAIL = "Bieu_mau_key_shop_ks_loi.xls";
	public static final String TEMPLATE_KEY_SHOP_PRODUCT = "Bieu_mau_key_shop_product.xls";
	public static final String TEMPLATE_KEY_SHOP_PRODUCT_FAIL = "Bieu_mau_key_shop_product_loi.xls";
	public static final String TEMPLATE_SUB_UNIT_FAIL = "Bieu_mau_nganh_hang_con_loi.xls";
	public static final String TEMPLATE_KEY_SHOP_CUSTOMER = "Bieu_mau_key_shop_customer.xls";
	public static final String TEMPLATE_KEY_SHOP_CUSTOMER_FAIL = "Bieu_mau_key_shop_customer_loi.xls";
	public static final String TEMPLATE_KEY_SHOP_REWARD = "Bieu_mau_key_shop_reward.xls";
	public static final String TEMPLATE_KEY_SHOP_REWARD_FAIL = "Bieu_mau_key_shop_reward_loi.xls";
	public static final String TEMPLATE_KEY_SHOP_SHOP = "Bieu_mau_key_shop_shop.xls";
	public static final String TEMPLATE_KEY_SHOP_SHOP_FAIL = "Bieu_mau_key_shop_shop_loi.xls";
	public static final String EXPORT_EXCEL_KEY_SHOP = "danh_sach_keyshop.xls";
	public static final String EXPORT_EXCEL_KEY_SHOP_CUSTOMER = "danh_sach_khach_hang_dang_ky_cthttm.xls";
	public static final String EXPORT_EXCEL_KEY_SHOP_REWARD = "danh_sach_thong_tin_tra_thuong_keyshop.xls";
	
	public static final String TEMPLATE_SALEPRODUCT = "Template_Import_Chi_Dinh_NVGH_Loi.xls";
	public static final String TEMPLATE_EXPORT_SALEPRODUCT = "Template_Export_Chi_Dinh_NVGH.xls";
	public static final String EXPORT_SALEPRODUCT = "Chi_dinh_NVGH_";
	
	
	public static final String TEMPLATE_SALEMT_CATALOG_CUSTOMMER = "SALEMT_IMPORT_KHACH_HANG.xls";
	public static final String TEMPLATE_SALEMT_REPORT_BCXNT = "template_BaoCaoXuatNhapTon.xls";
	public static final String TEMPLATE_SALEMT_REPORT_BCXH = "template_BaoCaoXuatHang.xls";
	public static final String TEMPLATE_SALEMT_REPORT_BCDSTCAT = "template_BaoCaoDoanhSoTheoCat.xls";
	public static final String TEMPLATE_SALEMT_REPORT_BCDSTHD = "template_BaoCaoDoanhSoTheoHoaDon.xls";
	public static final String ERR_ACTIVE_PROGRAM_OK = "ERR_ACTIVE_PROGRAM_OK";
	
	public static final String ERR_ACTIVE_DISPLAY_PROGRAM_DVTG = "ERR_ACTIVE_DISPLAY_PROGRAM_DVTG";
	public static final String ERR_ACTIVE_DISPLAY_PROGRAM_SPDS = "ERR_ACTIVE_DISPLAY_PROGRAM_SPDS";
	public static final String ERR_ACTIVE_DISPLAY_PROGRAM_MUC = "ERR_ACTIVE_DISPLAY_PROGRAM_MUC";
	
	public static final String ERR_ACTIVE_PROMOTION_PROGRAM_DVTG = "ERR_ACTIVE_PROMOTION_PROGRAM_DVTG";
	public static final String ERR_ACTIVE_PROMOTION_PROGRAM_SPKM = "ERR_ACTIVE_PROMOTION_PROGRAM_SPKM";
	
	public static final String ERR_ACTIVE_PROMOTION_PROGRAM_HTTM_DVTG = "ERR_ACTIVE_PROMOTION_PROGRAM_HTTM_DVTG";
	
	public static final String ERR_ACTIVE_INCENTIVE_PROGRAM_DVTG = "ERR_ACTIVE_INCENTIVE_PROGRAM_DVTG";
	public static final String ERR_ACTIVE_INCENTIVE_PROGRAM_SPKT = "ERR_ACTIVE_INCENTIVE_PROGRAM_SPKT";
	public static final String ERR_ACTIVE_INCENTIVE_PROGRAM_MUC = "ERR_ACTIVE_INCENTIVE_PROGRAM_MUC";
	
	public static final String ERR_ACTIVE_FOCUS_PROGRAM_DVTG = "ERR_ACTIVE_FOCUS_PROGRAM_DVTG";
	public static final String ERR_ACTIVE_FOCUS_PROGRAM_HTBH = "ERR_ACTIVE_FOCUS_PROGRAM_HTBH";
	public static final String ERR_ACTIVE_FOCUS_PROGRAM_SPTT = "ERR_ACTIVE_FOCUS_PROGRAM_SPTT";
	public static final String TEMPLATE_CATALOG_CONTRACT_MANAGER = "Bieu_mau_quan_ly_hop_dong_thiet_bi_export.xls";
	
	public static final String CRM_KD1_REPORT = "KD1- Bc Doanh so ban hang cua nhan vien ban hang theo SKUs";
	public static final String CRM_KD3_REPORT = "KD3- Bc Doanh so ban hang cua nhan vien ban hang theo nhom";
	public static final String CRM_KD10_REPORT = "KD10-Thuc_hien_ds_pp_skus_de_nghi_theo_npp";
	public static final String CRM_KD10_2_REPORT = "KD10.2-Diem le can phan phoi theo SKUs de nghi - GOLDSOY";
	public static final String CRM_KD15_REPORT = "KD15 - So diem ban le chua PSDS cua thang so voi 1,2,3...thang truoc";
	public static final String CRM_KD14_REPORT = "KD14-Bao_cao_nhom_SKUs_tren_diem_ban_thang";
	public static final String CRM_KD5_REPORT = "KD5-Luong_ban_hang_skus_theo_mien";
	
	//EXPORT EXCEL
	public static final String EXPORT_GSBH_TUYEN = "Export_Tuyen";
	
	//REPORT EXCEL
	public static final String BC_ASO_FILE_NAME = "Bc ASO";
	public static final String BC_GROWTH_SKUs_FILE_NAME = "Bc_tang_truong_SKus";
	
	public static final String DTBH_TDDSTKH_NH_MH = "3.9_Theo_doi_doanh_thu_ban_hang_theo_khachhang_nganhhang_mahang";
	public static final String BCTDTN_BKDHTNVGH = "Bc Bang ke don hang theo NVGH";
	public static final String BCTDHN_DTTHTNVBH_CT = "Bc_Doanh_thu_ban_hang_trong_ngay_theo_NVBH";
	public static final String BC_DSKHTTBH = "Bc Danh Sach Khach Hang Theo Tuyen BH";
	public static final String KKK_BCDKTT = "BaoCaoDemKhoThucTe";
	public static final String BCDTBH_DTBHTNTNVBH = "Bc Doanh thu trong ngay theo NVBH";
	public static final String BCDTBH_DSTHNVBHTN = "Bc_Doanh_So_Tong_Hop_NVBH_Theo_Ngay";
	public static final String BCDTBH_DSTHTSP = "Bc_Doanh_Thu_Ban_Hang_Theo_San_Pham.xls";
	public static final String GSKH_VT1_1TGGTNVBH = "Bc_Thoi_gian_ghe_tham_cua_NVBH_1";
	public static final String GS_4_2 = "Bc_ghe_tham_khach_hang";
	public static final String GS_1_2 = "Bc_thoi_gian_ghe_tham_nvbh";
	public static final String GS_1_4 = "Bc_mo_moi_khach_hang";
	public static final String EXPORT_BAO_CAO_KHTT = "Ke_hoach_tieu_thu_NVBH";
	public static final String DTBH_DT3_REPORT = "Bc danh sach don hang trong ngay cua NVBH";
	public static final String PHIEU_DTH = "Phieu_doi_tra_hang";
	public static final String DSKH = "Danh_sach_khach_hang";
	public static final String DSKH1 = "Danh_sach_khach_hang_theo_NVBH_";
	
	public static final String GSKH_DS3_REPORT = "Bc_thoi_gian_ban_hang_hieu_qua_cua_nvbh";
	public static final String GSKH_BCTGGTNVBH = "Bao_cao_thoi_gian_ghe_tham_nvbh";
	public static final String GSKH_VT5_BCNVKBM = "Bao_cao_VT5_nhan_vien_khong_bat_may";
	public static final String EXPORT_GS_1_1 = "Bao_cao_GS1_1_Thong_Tin_Tuyen";
	
	public static final String BC_XNTF1 = "Bao_cao_xuat_nhap_ton_f1";
	public static final String BC_BKCTHHMV = "Bao_cao_bang_ke_chung_tu_hang_hoa_mua_vao";
	public static final String BC_XNTCT = "Bao_cao_xuat_nhap_ton_chi_tiet";
	// EXPORT EXCEL VANSALE
	public static final String TDBH_BCPTDTBHSKU = "Bao_cao_ptdtbhsku_7_2_15";
	public static final String TDBH_BCDHT_THEOGIATRI = "Bao_cao_dht_theogiatri_7_2_16";
	public static final String TDBH_BCDHT_THEOTONGDONHANG = "Bao_cao_dht_theotongdonhang_7_2_17";
		
	// EXPORT EXCEL VANSALE
	public static final String VANSALES_BCXNKNVBH = "Bao_cao_xuat_nhap_kho_nvbh_vansale";
	public static final String VANSALES_BCXBTNV = "Bao_cao_xuat_ban_ton_nv_vansale";
	public static final String VANSALES_BCPXKKVCNB = "Bao_cao_pxkkvcnb_vansale";
	
	public static final int ERR_SYSTEM = 1;
	public static final int ERR_INVALID_SHOP = 2;
	public static final int ERR_NOT_EXIST_DB = 3;
	public static final int ERR_STATUS_INACTIVE = 4;
	public static final int ERR_REQUIRE = 5;
	public static final int ERR_EXIST = 6;	
	public static final int ERR_NOT_REQUIRE = 7;
	public static final int ERR_IS_USED = 8;
	public static final int ERR_NOT_PERMISSION = 9;
	public static final int ERR_NOT_LOGIN = 10;
	public static final int ERR_NOT_INT=11;
	public static final int ERR_NOT_BELONG_ANY_SHOP = 12;
	public static final int ERR_USER_INPUT = 13;
	public static final int ERR_FORMAT_INVALID = 14;
	public static final int ERR_NOT_NUMBER = 15;
	public static final int ERR_NOT_SHOP_CODE = 16;
	public static final int ERR_NOT_POSSITIVE_INT = 17;
	public static final int ERR_EXIST_SPECIAL_CHAR_IN_CODE = 18;
	public static final int ERR_EXIST_SPECIAL_CHAR_IN_CODE_CORE = 1000;
	public static final int ERR_EXIST_SPECIAL_CHAR_IN_CODE_NEW = 108;
	public static final int ERR_EXIST_SPECIAL_CHAR_IN_NAME = 19;
	public static final int ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS = 20;
	public static final int ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL = 99;
	public static final int ERR_INVALID_EMAIL = 21;
	public static final int ERR_NOT_EFFECT=22;
	public static final int ERR_FIELD = 23;
	public static final int ERR_MAX_LENGTH = 24;
	public static final int ERR_NO_SELECT_DATA = 25;
	public static final int ERR_INTEGER = 26;
	public static final int ERR_POSSITIVE_INTEGER = 27;
	public static final int ERR_QUANTITY_STOCK_TOTAL = 28;
	public static final int ERR_FDATE_TDATE = 29;
	public static final int ERR_STOP_PARENT = 30;
	public static final int ERR_NOT_UOM1 = 31;
	public static final int ERR_NOT_GREATER = 32;
	public static final int ERR_SHOP_TYPE_NOT_EXIST = 33;
	public static final int ERR_SHOP_TYPE_INVALID = 34;
	public static final int ERR_STATUS_IN_WAITING = 35;
	public static final int ERR_NOT_INVALID = 36;
	public static final int ERR_SHOP_NOT_BELONG_AREA = 37;
	public static final int ERR_NOT_POSSITIVE_NUMBER = 38;
	public static final int ERR_FROM_DATE_NOT_GREATER_TO_DATE = 39;
	public static final int ERR_NOT_PERCENT_NUMBER = 40;
	public static final int ERR_INVALID_FORMAT_CHARACTER=41;
	public static final int ERR_INVALID_LENGTH=42;
	public static final int ERR_FLOAT_FORMAT = 43;
	public static final int ERR_CHILD_SHOP_IS_RUNNING = 44;
	public static final int ERR_STAFF_NOT_BELONG_SHOP = 45;
	public static final int ERR_STAFF_HAS_SUPERVISE = 46;
	public static final int ERR_DUPLICATE_IS_RUNNING = 47;
	public static final int ERR_CANT_CHANGE_PARENT = 48;
	public static final int ERR_SHOP_NOT_BELONG_TO_USER = 49;
	public static final int ERR_SHOP_NOT_ACTIVE = 50;
	public static final int ERR_STAFF_NOT_ACTIVE = 51;
	public static final int ERR_CUSTOMER_NOT_ACTIVE = 52;
	public static final int ERR_NOT_SALE_STAFF = 53;
	public static final int ERR_DISPLAY_TOOL_PRODUCT_EXIST = 54;
	public static final int ERR_DISPLAY_TOOL_PRODUCT_DATA_EXIST = 55;
	public static final int ERR_REGION_IMPORT_EXCEL = 56;
	public static final int ERR_PRODUCT_NOT_BELONG_CATEGORY = 57;
	public static final int ERR_CANT_PARENT_MYSELF = 58;
	public static final int ERR_STATUS_NOT_IN_WAITING = 59;
	public static final int ERR_NOT_CORRECT = 60;
	public static final int ERR_DATA_NOT_FULL = 61;
	public static final int ERR_PARENT_AREA_IN_WARD_TYPE = 62;
	public static final int ERR_NOT_EXIST_GENERAL = 63;
	public static final int ERR_FORMAT_LOT = 64;
	public static final int ERR_INTEGER_MAXVALUE = 65;
	public static final int ERR_SALE_EXIST_INVOICE_OR_NOT_PERMISSION = 66;
	public static final int ERR_FORMAT_NUMBER_CONVFACT = 67;
	public static final int ERR_NOT_DATA_CORRECT = 68;
	public static final int ERR_CAR_NOT_BELONG_SHOP = 69;
	public static final int ERR_INVALID = 70;
	public static final int ERR_NOT_EXIST_AND_NOT_RUNNING = 71;
	public static final int ERR_NOT_BELONG_USERLOGIN = 72;
	public static final int ERR_EXIST_ECat = 73;
	public static final int ERR_EXIST_ECatImport = 74;
	public static final int ERR_SALE_ORDER_NOT_EXIST_NVGH = 75;
	public static final int ERR_LOT_QUANTITY_IS_NOT_ENOUGH = 76;
	public static final int ERR_RECEIVED_QUANTITY_LARGER_THAN_MAX_QUANTITY = 77;
	public static final int ERR_TOTAL_DEBIT_LARGER_THAN_MAX_DEBIT = 78;
	public static final int ERR_MAX_DAY_DEBIT = 79;
	public static final int ERR_INTEGER_ZERO_OR_ONE = 80;
	public static final int ERR_FORMAT_CYCLE_YEAR = 81;
	
	public static final int ERR_IMAGES_CHECK_DATE = 4502;
	/**///me
	public static final int ERR_DEBIT_SUPLIER_INVALID_MONEY = 1001;
	public static final int ERR_DEBIT_SUPLIER_MONEY_ERR = 1002;
	public static final int ERR_DEBIT_SUPLIER_RECEIVE_MONEY = 1003;
	public static final int ERR_DEBIT_SUPLIER_DONT_HAVE_RETURN_ORDER = 1004;
	public static final int ERR_DEBIT_SUPLIER_PAYMENT_MONEY = 1005;
	public static final int ERR_DEBIT_SUPLIER_DONT_HAVE_INCOME_ORDER = 1006;
	public static final int ERR_DEBIT_BATCH_NOT_IN_SHOP = 1007;
	public static final int ERR_DEBIT_BATCH_MONEY_NOT_GREATER_ZERO = 1008;
	
	public static final int ERR_INTEGER_ZERO = 1499;
	public static final int ERR_STOCK_INPUT_IMPORTEXCEL_LOT_REQUIRE = 1500;
	public static final int ERR_STOCK_INPUT_IMPORTEXCEL_LOT_NOTREQUIRE = 1501;
	public static final int ERR_STOCK_INPUT_EXIT_INVENTORY = 1502;
	public static final int ERR_STOCK_INPUT_EXIST_LIST = 1503;
	public static final int ERR_STOCK_INPUT_DUPLICATE = 1504;
	
	public static final int ERR_STOCKTRAIN = 2000;
	public static final int ERR_PO_STOCKIN = 2001;
	public static final int ERR_CUSTAUTO_UPDATE = 2002;
	public static final int ERR_PO_POAUTO_NOT_YET = 2003;
	public static final int ERR_PO_DVKH_ALREADY_SUSPEND = 2004;
	public static final int ERR_PO_STOCKIN_FINISH = 2005;
	public static final int ERR_PO_RETURNPRODUCT_CHECKSTOCK = 2006;
	public static final int ERR_PO_RETURNPRODUCT_CHECKSTOCKLOT = 2007;
	public static final int ERR_STAFF_SALEGROUP_EXITS = 2008;
	public static final int ERR_PO_STOCKIN_SUSPEND = 2009;
	public static final int ERR_PO_POAUTO_NOT_APPROVED = 2010;
	
	public static final int ERR_NO_SELECT = 3000;
	public static final int ERR_NOT_EXIST = 3001;
	public static final int ERR_LAW = 3002;
	public static final int ERR_NOT_FLOAT = 3003;
	public static final int ERR_NETGROSS_WEIGHT = 3004;
	public static final int ERR_MIN_MAX = 3005;
	public static final int ERR_MAX_LENGTH_INT = 3006;
	public static final int ERR_MAX_LENGTH_FLOAT = 3007;
	public static final int ERR_CATALOG_DAYGO = 3008;
	public static final int ERR_NOT_ZERO = 3009;
	public static final int ERR_NOT_INVALID_TYPE_SHOP = 3010;
	public static final int ERR_MAX_LENGTH_DAY_SALE = 3011 ;
	public static final int ERR_MAX_LENGTH_GROWTH = 3012 ;
	public static final int ERR_MAX_LENGTH_MINMAX = 3013 ;
	public static final int ERR_CUSTOMER_EXIST_SKU = 3014 ;
	public static final int ERR_FLOAT_FORMAT_PRE = 3015 ;
	public static final int ERR_FOCUS_PROGRAM_EXIST_PRODUCT = 3016;
	public static final int ERR_STATUS_IN_RUNNING = 3017;
	public static final int ERR_ORDER_NOT_IN_TODAY = 3018;
	public static final int ERR_ORDER_CHOOSE_LOT = 3019;
	public static final int ERR_INVALID_DATE = 3020;
	public static final int ERR_EXISTS_BEFORE = 3021;
	public static final int ERR_NOT_EXISTS_BEFORE = 3022;
	public static final int ERR_PRODUCT_NOT_IN_STOCK_TOTAL_PROMOTION = 3023;
	public static final int ERR_LOT_PROMOTION=3024;
	public static final int ERR_DEBIT_BATCH_NOT_EXIST_DEBIT = 3025;
	public static final int ERR_NOT_EXIST_TOGETHER_DB = 3026;
	
	public static final int ERR_NOT_EXIST_GSNPP = 3027;
	public static final int ERR_NOT_EXIST_NVBH = 3028;
	public static final int ERR_STATUS_INACTIVE_GSNPP = 3029;
	public static final int ERR_STATUS_INACTIVE_NVBH = 3030;
	
	public static final int ERR_NOT_EXIST_MIEN = 3031;
	public static final int ERR_NOT_EXIST_VUNG = 3032;
	public static final int ERR_NOT_EXIST_NPP = 3033;
	public static final int ERR_NOT_EXIST_SHOP_MIEN = 3034;
	public static final int ERR_NOT_EXIST_SHOP_VUNG = 3035;
	public static final int ERR_NOT_EXIST_SHOP_NPP = 3036;
	
	public static final int ERR_SALEORDER_RETURNED = 4000;
	
	public static final int ERR_CONFIRM_ORDER_TABLET = 5000;
	
	public static final int ERR_DEVICE_STATUS_NOT_GOOD = 5500;
	
	public static final int ERR_DEVICE_IN_USE = 5501;
	
	public static final int ERR_CONTRACT_NOT_DRAFT = 5800;
	public static final int ERR_CONTRACT_STARTDATE_NOT_GREATE_ENDDATE = 5801;
	public static final int ERR_CONTRACT_CONTRACTDATE_NOT_GREATE_HOCDATE = 5802;
	
	public static final int ERR_DATE_VISIT_PLANT_EXISTS = 6000;
	
	
	public static final String IMPORT_WAREHOUSE_FAIL_TEMPLATE = "Bieu_mau_nhap_xuat_dieu_chinh_loi.xls";
	
	public static final int ERR_SP_CREATE = 7000;
	public static final int ERR_SP_PROMOTION = 7001;
	public static final int ERR_SP_PROMOTION_SHOP=7002;
	public static final int ERR_SP_PROMOTION_SHOP_FULL=7003;
	public static final int ERR_SP_PROMOTION_CUS=7004;
	public static final int ERR_SP_PROMOTION_CUS_FULL=7005;
	public static final int ERR_STOCK_CC=7006;
	public static final int ERR_STOCK_NOT_LOT=7007;
	public static final int ERR_PAUSE_STATUS=7008;
	public static final int ERR_QUANTITY_STOCK_TOTAL2=7009;
	public static final int ERR_QUANTITY_STOCK_TT_PROMOTION=7010;
	public static final int ERR_QUANTITY_STOCK_TT_PROMOTION2=7011;
	public static final int ERR_STATUS_PAUSE=7012;
	public static final int ERR_INVALID_FORMAT=7013;
	public static final int ERR_LOT_DESTROY=7014;
	public static final int ERR_LOT_DESTROY2=7015;
	public static final int ERR_IS_USED_DB=7016;
	public static final int ERR_IS_USED2=7017;
	public static final int ERR_CUS_MAP=7018;
	public static final int ERR_PERMISSION_CHANGE=7020;
	public static final int ERR_IS_USED3 = 7019;
	public static final int ERR_NOT_BELONG_STAFF = 7021;
	public static final int ERR_QUANTIY_HAS_LOT=7022;
	public static final int ERR_QUANTIY_HAS=7023;
	public static final int ERR_PRODUCT_NOT_IN_STOCK_TOTAL = 7024;
	public static final int ERR_SP_STATUS_INVOICE_NOT_EXISTS = 7025;
	public static final int ERR_SP_CREATE_ORDER_CUSTOMER_EXCEED_MAX_DEBIT = 7026;
	public static final int ERR_SP_CREATE_ORDER_CUSTOMER_EXCEED_MAX_DAY = 7027;
	public static final int ERR_QUANTITY_STOCK_TT_PROMOTION_RE_PAYMENT = 7028;
	public static final int ERR_STOCK_RECEIVED_PRODUCT_VANSALES = 7029;
	public static final int ERR_STOCK_RECEIVED_LOT_VANSALES = 7030;
	public static final int ERR_STOCK_STQ_QUANTITY=7031;
	public static final int ERR_STOCK_UPDATE_LOT_EXPIRE=7032;
	public static final int ERR_INCENTIVE_PROGRAM_NOT_EXIST_OR_DELETE=7033;
	public static final int ERR_INCENTIVE_LEVEL_NOT_EXIST_OR_DELETE = 7034;
	public static final int ERR_LOT_SALE=7035;
	public static final int ERR_DPPAYPERIOD_DP=7036;
	public static final int ERR_STOCK_UPDATE_HAS_NOT_EN_LOT=7037;
	public static final int ERR_STOCK_UPDATE_HAS_NOT_EN=7038;
	public static final int ERR_DP_PERIOD_STOCK=7039;
	public static final int ERR_DP_PERIOD_STOCK_QUANTITY=7040;
	public static final int ERR_DP_PERIOD_STOCK_LOT=7041;
	public static final int ERR_ISS_STOCK_OUT = 7042;
	public static final int ERR_PRODUCT_MAP=7045;
	public static final int ERR_PRODUCT_GROUP=7046;
	public static final int STATUS_UPDATE = 7047;
	public static final int STATUS_CREATE = 7048;
	public static final int STATUS_DELETE = 7049;
	public static final int ERR_SP_LOT_QUANTITY = 7050;
	public static final int ERR_INVALID_PRODUCT_FOR_CUSTOMER_AND_STAFF = 7051;
	
	

	
	public static final int ERR_SP_EXIST_INVOICE_NUMBER = 8000;
	public static final int ERR_SP_NEW_INVOICE_EQUAL_EXIST_INVOICE = 8001;
	public static final int ERR_SP_SALE_ORDER_NEED_TO_UPDATE_DELIVERY = 8002;
	public static final int ERR_SP_DUPLICATE_INVOICE_NUMBER = 8003;
	
	public static final int ERR_SEQ_ROUTING_CUSTOMER = 9000;
	public static final int ERR_SEQ_DUPLICATE_ROUTING_CUSTOMER = 9001;
	public static final int ERR_SEQ_LESS_THAN_ZERO = 9002;
	public static final int ERR_SEQ_NULL = 9003;
	public static final int ERR_SEQ_MAX_CREATE_ROUTE = 9004;
	public static final int ERR_START_WEEK_MAX_CREATE_ROUTE = 9005;
	public static final int ERR_SEQ_START_WEEK_NOT_NULL = 9006;	
	public static final int ERR_DATA_NULL = 9007;
	public static final int ERR_SHOP_NO_DATA= 9008;
	
	public static final int ERR_SS_NOT_GENERAL = 9100;
	public static final int ERR_SS_NOT_BELONG_GENERAL = 9101;
	public static final int ERR_SS_TRANINGPLAN_NOT_SAME_SHOP_STAFFSALE = 9102;
	public static final int ERR_SS_TRANINGPLAN_NOT_SAME_USERLOGIN_GENERAL = 9103;
	public static final int ERR_SS_TRANINGPLAN_GSNPP_NOT_MANAGE_SALESTAFF = 9104;
	public static final int ERR_SS_TRANINGPLAN_GSNPP_NOT_MANAGE_SALESTAFF_SHOP = 9105;
	public static final int ERR_SS_TRANINGPLAN_NOT_TRAIN_DATE = 9106;
	public static final int ERR_SS_TRANINGPLAN_NOT_TRAIN_SHOP_DATE = 9107;
	public static final int ERR_SS_TRANINGPLAN_CANCEL_TRAINED_DATE = 9108;
	public static final int ERR_SS_NOT_BELONG_SHOPUSER = 9109;
	public static final int ERR_SS_GREATER_DATENOW_GENERAL = 9110;
	
	public static final int ERR_CATALOG_PROMOTION_EXITS_RUNNING = 10000;
	public static final int ERR_CATALOG_PRODUCT_PRICE_VAT_NOT_EQUAL = 10001;
	public static final int ERR_FLOAT_NUMBER = 500;
	public static final int ERR_NOT_IN_SHOP = 501;
	public static final int ERR_NUMBER = 502;
	
	public static final int ERR_MONTH_CREARE_SALE_PLAN = 503;
	
	public static final int ERR_STOCK_APPROVE_COUNTDATE_NOT_COMPELETE = 112001;
	public static final int SELECT_ALL = -2;
	
	public static final int MAZ_ITEM = 8;
	
	public static final int ERR_FOCUS_PROGRAM_EXIST_ADD = 12000;
	public static final int ERR_FOCUS_PROGRAM_EXIST_DELETE = 12001;
	
	public static final String SESSION_PROMOTION_PRODUCT = "SESSION_PROMOTION_PRODUCT";
	
	public static final int ERR_CATALOG_PROMOTION_QUANTITY_ERROR = 12002;
	
	public static final int ERR_CATALOG_PROMOTION_QUANTITY_MAX = 12003;
	
	public static final int ERR_IS_USED_1 = 12004;
	
	public static final int ERR_ORDER_DATE_INVALID = 12005;
	
	public static final int ERR_ROUTING_INVALID = 12006;
	public static final int ERR_ATTRIBUTE_NOT_EXIST=12007;
	
	public static final int ERR_INCENTIVE_MULTIPLE_RECURSIVE_INVALID = 12008;
	public static final int ERR_INCENTIVE_FREE_PRODUCT_NOT_RUNNING = 12009;
	public static final int ERR_INCENTIVE_PRODUCT_CAT_NULL = 12010;
	
	public static final int ERR_STAFF_OWNER = 12011;
	
	public static final int ERR_INCENTIVE_ERROR_FDATE_TODATE = 12012;
	public static final int ERR_INCENTIVE_INVALID_FDATE = 12013;
	public static final int ERR_INCENTIVE_INVALID_TDATE = 12014;
	public static final int ERR_INCENTIVE_INVALID_FDATE_TDATE = 12015;
	public static final int ERR_INCENTIVE_INVALID_PRODUCT = 12016;
	public static final int ERR_INCENTIVE_PROGRAM_EXISTS = 12017;
	public static final int ERR_INCENTIVE_PRODUCT_STOPPED = 12018;
	public static final int ERR_INCENTIVE_PRODUCT_LEVEL_INVALID = 12019;
	public static final int ERR_INCENTIVE_LEVEL_EXISTS = 12020;
	public static final int ERR_INCENTIVE_PROGRAM_NOT_WAITING = 12026;
	public static final int ERR_INCENTIVE_PROGRAM_ONLY_NUMBER = 12033;
	
	public static final int ERR_PAYROLL_NOT_ENOUGH = 12021;
	public static final int ERR_PAYROLLT_NOT_EXISTS = 12022;
	public static final int ERR_STORE_NAME_NOT_EXISTS = 12023;
	public static final int ERR_ELEMENT_PAYROLLT_IS_NUMBER = 12024;
	public static final int ERR_ELEMENT_PAYROLLT_IS_CHARACTER = 12025;
	public static final int ERR_EXIST_SPECIAL_CHAR_IN_PAYROLL_ELEMENT_INFO = 12027;
	public static final int ERR_STAFF_NOT_IN_PAYROLL = 12028;
	public static final int ERR_REVENUE_BONUS = 12029;
	public static final int ERR_DISPLAY_BONUS = 12030;
	public static final int ERR_REVENUE_DISPLAY_BONUS = 12031;
	public static final int ERR_DISPLAY_PROGRAM_INTEGER = 12032;
	public static final int ERR_PAYROLL_STOPPED = 12034;
	public static final int ERR_PAYROLLT_STOPPED = 12035;
	
	public static final String TEMPLATE_REPORT_FOLDER = "/resources/templates/";
	
	public static final String ERR_FAIL_TEMPLATE = "ERRR";
	public static final String NOT_ERROR_TEXT = "OK";
	
	public static final String SESSION_REPORT_DATA = "SESSION_REPORT_DATA";
	public static final String SESSION_REPORT_PARAM = "SESSION_REPORT_PARAM";
	public static final String SESSION_REPORT_NUM_ROW = "SESSION_REPORT_NUM_ROW";
	public static final String SESSION_REPORT_TOTAL_ROW = "SESSION_REPORT_TOTAL_ROW";
	public static final String SESSION_OBJECT_TYPE = "SESSION_OBJECT_TYPE";
	
	public static final String APPARAM_TYPE_INCENTIVE = "FC_INCENTIVE";
	
	public static final int ERR_PROMOTION_NOT_EXISTS = 12036;
	public static final int ERR_ATTRIBUTE_NOT_EXISTS = 12037;
	public static final int ERR_ATTRIBUTE_DETAIL_NOT_EXISTS = 12038;
	
	public static final int ERR_GROUP_PO_AUTO_NOT_EXISTS = 12039;
	public static final int ERR_PROMOTION_PROGRAM_EXISTS = 12040;
	public static final int ERR_GROUP_PO_AUTO_EXISTS = 12041;
	public static final int ERR_FOCUS_PROGRAM_EXISTS = 12042;
	public static final int ERR_COPY_INCENTIVE_PROGRAM_EXISTS = 12043;
	public static final int ERR_PROMOTION_MANUAL_PROGRAM_EXISTS = 12044;
	public static final int ERR_CYCLE_COUNT_NOT_EXISTS = 12045;
	public static final int PRINT_CUSTOMER = 0;
	public static final int PRINT_DELIVERY = 1;
	public static final int PRINT_ORDER = 2;
	public static final int ERR_SUB_CAT_SECOND_NOT_EXISTS = 12046;
	
	//SALE MT
	public static final String TEMPLATE_CATALOG_PRICE_PRODUCT = "SALE_MT_IMPORT_BANG GIA_LOI.xls";
	public static final String TEMPLATE_CATALOG_PRODUCT = "SALE_MT_IMPORT_BANG GIA_DAU_VAO_LOI.xls";
	public static final String TEMPLATE_CATALOG_PRODUCT_OUT = "SALE_MT_IMPORT_BANG GIA_DAU_RA_LOI.xls";
	public static final String TEMPLATE_CATALOG_CUSTOMER_OUT = "SALEMT_IMPORT_KHACH_HANG_LOI.xlsx";
	
	public static final String SALEMT_REPORT_BCBH_TEMPLATE = "Admin_BaoCaoBanHang_Excel.xls";
//	public static final String SALEMT_REPORT_BCBH = "BÃƒÂ¡o cÃƒÂ¡o bÃƒÂ¡n hÃƒÂ ng_";
	public static final String SALEMT_REPORT_BCBH = "bao-cao-ban-hang_";
	
	public static final String SALEMT_REPORT_BCCHNH_TEMPLATE = "Admin_BaoCaoNhapHang_Excel.xls";
//	public static final String SALEMT_REPORT_BCCHNH = "BÃƒÂ¡o cÃƒÂ¡o cÃ¡Â»Â­a hÃƒÂ ng nhÃ¡ÂºÂ­p hÃƒÂ ng_";
	public static final String SALEMT_REPORT_BCCHNH = "bao-cao-nhap-hang_";

	public static final String SALEMT_REPORT_BCTK_TEMPLATE = "Admin_BaoCaoTonKho_Excel.xls";
	public static final String SALEMT_REPORT_BCTK = "Bao Cao Ton Kho ";

//	public static final String TEMPLATE_SALEMT_REPORT_BCXNT = "template_BaoCaoXuatNhapTon.xlsx";
//	public static final String TEMPLATE_SALEMT_REPORT_BCXH = "template_BaoCaoXuatHang.xlsx";

	public static final String SALEMT_REPORT_BCKM_TEMPLATE = "Admin_BaoCaoHangKhuyenMai_Excel.xls";
	public static final String SALEMT_REPORT_BCKM = "Bao cao khuyen mai ";
	
	public static final String SALEMT_REPORT_BCDSKH_TEMPLATE = "Admin_BaoCaoDanhSachKhachHang_Excel.xls";
	public static final String SALEMT_REPORT_BCDSKH = "Bao cao danh sach khach hang";
	
	public static final String SALEMT_REPORT_BCDH_TEMPLATE = "Admin_BaoCaoDatHang_Excel.xls";
	public static final String SALEMT_REPORT_BCDH = "Bao cao dat hang ";
	
	public static final String SALEMT_REPORT_BCSN_TEMPLATE = "Bao_cao_sinh_nhat.xls";
	public static final String SALEMT_REPORT_BCKHKGD_TEMPLATE = "Bao_cao_khach_hang_khong_giao_dich.xls";
	public static final String SALEMT_REPORT_BCSN = "Bao cao sinh nhat ";
	public static final String SALEMT_REPORT_BCKHKGD = "Bao_cao_khach_hang_khong_giao_dich";
	
	public static final String EXPORT_1_3_KET_QUA_DI_TUYEN = "Bao_cao_1_3_ket_qua_di_tuyen";
	public static final String EXPORT_2_1_SLDS = "Bao_cao_san_luong_doanh_so_ban_hang";
	public static final String EXPORT_2_2_SLDS = "Doanh_so_san_luong_NVBH";
	public static final String EXPORT_2_3_SLDS = "Doanh_so_san_luong_KH";
	public static final String EXPORT_2_6_SLDS = "Bao_cao_danh_sach_don_hang";
	public static final String EXPORT_2_4_SLDS = "Bao_cao_theo_doi_KPI";
	public static final String EXPORT_3_1_XNT = "Bao_cao_xuat_nhap_ton";
	public static final String EXPORT_3_2_XNT_CT = "Bao_cao_xuat_nhap_ton_chi_tiet";
	public static final String EXPORT_3_3_TTDH = "Bao_cao_so_tinh_trang_nhap_hang";
	public static final String EXPORT_3_4_KK = "Bao_cao_kiem_ke";
	public static final String EXPORT_4_1_SLDS = "Bao_cao_doanh_so_Key_shop";
	public static final String EXPORT_4_2_TKTNVVS = "Bao_cao_ton_kho_theo_nhan_vien_vansale";
	public static final String EXPORT_2_7_SLDS = "Du_lieu_tho_don_hang";
	public static final String EXPORT_6_5_3_TIME_KEEPING = "Bao_cao_cham_cong";
	
	public static final int ERR_PROMOTION_CATALOG_IMPORT_DUPLICATE_TOTAL_MONEY_ZV192021 = 150010;	// chuong trinh khuyen mai - import tong tien mua trung lap
	public static final int ERR_PROMOTION_CATALOG_IMPORT_DUPLICATE_PROMOTION_PRODUCT_ZV0405 = 150011;	// #19819 - san pham khuyen mai trong 1 nhom khong duoc trung nhau - tuannd20
	public static final int ERR_PROMOTION_CATALOG_IMPORT_DUPLICATE_PROMOTION_PRODUCT_ZV06 = 150012;	// #19818 - san pham khuyen mai trong 1 nhom khong duoc trung nhau - tuannd20
	public static final int ERR_PRICE_IS_NOT_EXIST = 15005; 		// KhÃƒÂ´ng tÃ¡Â»â€œn tÃ¡ÂºÂ¡i giÃƒÂ¡ (datnt43)
	public static final int ERR_NOT_DISTRIBUTOR = 15006; 		// KhÃƒÂ´ng tÃ¡Â»â€œn tÃ¡ÂºÂ¡i giÃƒÂ¡ (datnt43)
	
	/** The Constant Delimiter For Import Sale Order */
	public static final String IMPORT_SALE_ORDER_DELIMITER = "END";
	
	/** Import Price */
	public static final String TEMPLATE_PRICE_PRODUCT_FAIL = "Bieu_mau_danh_muc_import_gia_san_pham_loi.xls";
	public static final String TEMPLATE_PRICE_PRODUCT_EXPORT = "Template_Export_Gia_San_Pham.xlsx";
	public static final String PRICE_EXPORT_EXCEL = "Thong_Tin_Gia_San_Pham_";
	
	/** Price Sale In*/
	public static final String PRICE_SALE_IN_IMPORT_TEMPLATE = "Bieu_mau_import_gia_sale_in_san_pham";
	public static final String PRICE_SALE_IN_IMPORT_FAIL_TEMPLATE = "Bieu_mau_import_gia_sale_in_san_pham_loi.xlsx";
	public static final String PRICE_SALE_IN_EXPORT_TEMPLATE = "Thong_tin_gia_sale_in_san_pham.xlsx";
	public static final String PRICE_SALE_IN_EXPORT_FILENAME = "Thong_tin_gia_sale_in_San_pham_";
	
	public static final String PAGE_FORMAT_A4 = "A4";
	public static final String PAGE_FORMAT_A5 = "A5";
	
	public static final String IMPORT_STAFF_TEMPATE_FILE_NAME = "Bieu_mau_danh_muc_import_nhan_vien";
	public static final String IMPORT_SHOP_TEMPATE_FILE_NAME = "Bieu_mau_danh_muc_import_don_vi";
	public static final String IMPORT_PRICE_TEMPATE_FILE_NAME = "Bieu_mau_danh_muc_import_gia_san_pham";
	public static final String IMPORT_STOCK_UPDATE_TEMPATE_FILE_NAME = "Bieu_mau_nhap_xuat_dieu_chinh";
	public static final String IMPORT_ROUTE_TEMPATE_FILE_NAME = "Bieu-mau-import-tuyen-file.xls";
		
	/** Product */
	public static final String TEMPLATE_PRODUCT_EXPORT = "Thong_Tin_San_Pham.xls";
	public static final String PRODUCT_EXPORT_EXCEL = "Thong_Tin_San_Pham_";
	public static final String IMPORT_PRODUCT_TEMPATE_FILE_NAME = "Bieu_mau_danh_muc_import_thong_tin_san_pham";
	public static final String TEMPLATE_PRODUCT_FAIL_WITHOUT_DYNAMIC_ATTRIBUTE = "Bieu_mau_danh_muc_import_thong_tin_san_pham_loi.xlsx";
	public static final String TEMPLATE_PRODUCT_FAIL = "Bieu_mau_danh_muc_import_thong_tin_san_pham_file_loi.xlsx";
	
	//KPI
	public static final String TEMPLATE_IMPORT_KPI_TARGET = "Import_KPI_template.xlsx";
	public static final String TEMPLATE_IMPORT_KPI_TARGET_ERR = "Import_KPI_template_error.xlsx";
	
	//ASO
	public static final String TEMPLATE_ASO_TARGET = "Aso_Plan_Export.xlsx";
	public static final String TEMPLATE_ASO_TARGET_ERR = "Aso_Plan_Import_Error.xlsx";
	
	//Cau hinh ap_param
	public static final String SYS_SALE_ROUTE = "SYS_SALE_ROUTE";
	public static final String SYS_CAL_DATE = "SYS_CAL_DATE";
	public static final String SYS_CURRENCY = "SYS_CURRENCY";
	public static final String SYS_CURRENCY_DIVIDE = "SYS_CURRENCY_DIVIDE";
	public static final String SYS_DIGIT_DECIMAL = "SYS_DIGIT_DECIMAL";
	public static final String SYS_DECIMAL_POINT = "SYS_DECIMAL_POINT";
	public static final String SYS_MAP_TYPE = "SYS_MAP_TYPE";
	public static final String SYS_DATE_FORMAT = "SYS_DATE_FORMAT";
	public static final String SYS_DEFAULT_PASSWORD = "SYS_DEFAULT_PASSWORD";
	
	//shop_param
	public static final String SYS_USE_LOCKDATE = "SYS_USE_LOCKDATE";
	public static final String SYS_RETURN_DATE = "SYS_RETURN_DATE";
	public static final String SYS_CAL_UNAPPROVED = "SYS_CAL_UNAPPROVED";
	public static final String SYS_MAXDAY_APPROVE = "SYS_MAXDAY_APPROVE";
	public static final String SYS_MAXDAY_RETURN = "SYS_MAXDAY_RETURN";
	public static final String SYS_SHOW_PRICE = "SYS_SHOW_PRICE";
	public static final String SYS_MODIFY_PRICE = "SYS_MODIFY_PRICE";
	public static final String SYS_CAL_PLAN = "SYS_CAL_PLAN";
	public static final String STAFF_VISITED_CUSTOMER_DAY = "STAFF_VISITED_CUSTOMER_DAY";
	public static final String STAFF_QUANTITY_DAY = "STAFF_QUANTITY_DAY";
	
	//value 
	public static final String ZERO_TEXT = "0";
	public static final String ONE_TEXT = "1";
	public static final String TWO_TEXT = "2";
	
	//
	public static final int MAX_DECIMAL_NUMBER_FOR_PRICE = 6;
	
	public static final int SYS_CONFIG_DEFAULT_REPORT_MEMCACHED_TIMEOUT_IN_SECOND = 30;
	
	public static final String MAX_RECORD_IMPORT = "MAX_RECORD_IMPORT";
	public static final int MAX_RECORDS_IMPORT = 5000;
	
	public static final int MAX_NUM_ROW_EXPORT = 65000;
	
	public static final String NEW_LINE_CHARACTER = "\n";
	
	/**
	 * Thiet bi
	 */
	public static final String TEMPLATE_IMPORT_EQUIP_GROUP_EQUIPMENT_EXPORT = "template_import_danh_muc_nhom_thiet_bi.xlsx";
	public static final String TEMPLATE_EQUIP_GROUP_EQUIPMENT_IMPORT = "Bieu_Mau_Danh_Muc_Nhom_Thiet_Bi.xlsx";
	public static final String TEMPLATE_EQUIP_GROUP_EQUIPMENT_FAIL = "Bieu_Mau_Danh_Muc_Nhom_Thiet_Bi_Loi.xlsx";
	public static final String TEMPLATE_EQUIP_LIST_LIQUIDATION_EXPORT = "Export_bien_ban_thanh_ly_tai_san"; // da noi duoi ".xlsx" o Action
	public static final String EQUIP_GROUP_EQUIPMENT_EXPORT = "Danh_Muc_Nhom_Thiet_Bi_";
	public static final String TEMPLATE_IMPORT_EQUIPMENT_CATALOG_FAIL = "Import_Danhmuc_Loi.xlsx";
	public static final String TEMPLATE_IMPORT_EQUIP_CATALOG = "Danh_sach_danh_muc_thiet_bi";
	public static final String TEMPLATE_IMPORT_EVICTION_FAIL = "Bieu_mau_import_bien_ban_thu_hoi_thiet_bi_LOI.xlsx";
	public static final String TEMPLATE_IMPORT_GROUP_PRODUCT_FAIL = "Bieu_mau_import_danh_sach_san_pham_thiet_bi_LOI.xlsx";
	public static final String TEMPLATE_IMPORT_LIQUIDATION_FAIL = "Bieu_mau_import_bien_ban_thanh_ly_tai_san_LOI.xlsx";
	public static final String TEMPLATE_EXPORT_GROUP_PRODUCT = "Danh_sach_san_pham_thiet_bi";
	public static final String TEMPLATE_EQUIP_LIST_EQUIPMENT_EXPORT = "template_danh_sach_thiet_bi.xlsx";
	public static final String EQUIP_LIST_EQUIPMENT_EXPORT = "Danh_Sach_Thiet_Bi_";
	public static final String TEMPLATE_IMPORT_EQUIP_LIST_EQUIPMENT_EXPORT = "template_import_danh_sach_thiet_bi.xlsx";
	public static final String TEMPLATE_EQUIP_LIST_EQUIPMENT_FAIL = "Bieu_Mau_Nhap_Danh_Sach_Thiet_Bi_Loi.xlsx";
	public static final String TEMPLATE_EQUIP_LIST_EVICTION_EXPORT_FILE_NAME = "Bieu_mau_export_bien_ban_thu_hoi_thanh_ly";
	public static final String TEMPLATE_EQUIP_LIST_EVICTION_EXPORT = "Bieu_mau_export_bien_ban_thu_hoi_thanh_ly.xlsx";
	public static final String TEMPLATE_EQUIP_LIST_REPAIR_PAYFORM_EXPORT = "Export_danh_sach_phieu_thanh_toan_sua_chua";
	public static final String EQUIP_STOCK_IMPORT_FAIL = "Bieu_mau_quan_ly_kho_thiet_bi_Loi.xlsx";
	public static final String EQUIP_STOCK_PERMISSION_IMPORT_FAIL = "Bieu_mau_quan_ly_quyen_kho_thiet_bi_Loi.xlsx";
	public static final String EQUIP_ROLE_ASSIGN_STAFF_IMPORT_FAIL = "Bieu_mau_gan_quyen_nguoi_dung_Loi.xlsx";
	public static final String TEMPLATE_IMPORT_RECORD_DELIVERY_EX = "Bieu_mau_import_bien_ban_giao_nhan_thiet_bi.xlsx";
	public static final String TEMPLATE_IMPORT_EVICTON_EX = "Bieu_mau_import_bien_ban_thu_hoi_thanh_ly.xlsx";
	public static final String TEMPLATE_IMPORT_RECORD_DELIVERY_FAIL = "Bieu_mau_import_bien_ban_giao_nhan_thiet_bi_LOI.xls";
	public static final String TEMPLATE_IMPORT_RECORD_DELIVERY = "Danh_sach_bien_ban_giao_nhan_thiet_bi";
	public static final String TEMPLATE_PRINT_RECORD_DELIVERY = "Bien_ban_giao_nhan";
	public static final String TEMPLATE_PRINT_RECORD_DELIVERY_PDA = "Danh_sach_de_nghi_giao_thiet_bi_ban_hang_PDA.xlsx";
	public static final String TEMPLATE_PRINT_RECORD_DELIVERY_NPP = "Danh_sach_de_nghi_giao_thiet_bi_ban_hang_NPP.xlsx";
	public static final String TEMPLATE_PRINT_RECORD_DELIVERY_NPP_ZIP = "Danh_sach_de_nghi_giao_thiet_bi_ban_hang_NPP";
	public static final String TEMPLATE_PRINT_RECORD_DELIVERY_ALL_ZIP = "Danh_sach_de_nghi_giao_thiet_bi_ban_hang_tat_ca";
	public static final String TEMPLATE_PRINT_RECORD_DELIVERY_XNKV = "Danh_sach_de_nghi_giao_thiet_bi_ban_hang_XNKV.xlsx";
	public static final String TEMPLATE_PRINT_RECORD_LEND = "Bien_ban_hop_dong_cho_muon";
	public static final String TEMPLATE_PRINT_RECORD_EVICTION = "bbthtl";
	public static final String TEMPLATE_CUSTOMER_CHECK_STATISTIC_LOI = "Bieu_mau_import_kh_kiem_ke_LOI.xlsx";
	public static final String TEMPLATE_STAFF_CHECK_STATISTIC_LOI = "Bieu_mau_import_TB_NVBH_kiem_ke_LOI.xlsx";

	public static final String EQUIP_CUSTOMER_RECORD_EXPORT = "Danh_Sach_Khach_Hang_kiem_ke_";
	public static final String EQUIP_EQUIP_RECORD_EXPORT = "Danh_Sach_Thietbi_kiem_ke_";
	public static final String EQUIP_EQUIP_STAFF_RECORD_EXPORT = "Danh_Sach_Thietbi_NVBH_kiem_ke_";
	public static final String TEMPLATE_EQUIP_CUSTOMER_RECORD_EXPORT = "template_export_cusotmer_checking.xlsx";
	public static final String TEMPLATE_EQUIP_RECORD_EXPORT = "template_export_equip_checking.xlsx";
	public static final String TEMPLATE_EQUIP_STAFF_RECORD_EXPORT = "template_export_equip_staff_checking.xlsx";
	
	public static final String TEMPLATE_EQUIP_LIST_STATISTIC_CHECKING_EXPORT = "Bieu_mau_export_bien_ban_kiem_ke";
	public static final String TEMPLATE_SHOP_CHECK_STATISTIC = "Danh_sach_don_vi_kiem_ke";
	public static final String TEMPLATE_SHOP_CHECK_STATISTIC_LOI = "Bieu_mau_import_don_vi_kiem_ke_LOI.xlsx";
	public static final String TEMPLATE_EQUIP_CHECK_STATISTIC_LOI = "Impt_Thietbi_loi.xlsx";
	
	public static final String TEMPLATE_EQUIP_STATISTIC_GROUP_TIME_FAIL = "Bieu_mau_import_kiem_ke_thiet_bi_so_lan_LOI.xlsx";
	public static final String TEMPLATE_EQUIP_STATISTIC_GROUP_ROUTE_FAIL = "Bieu_mau_import_kiem_ke_thiet_bi_tuyen_LOI.xlsx";
	public static final String TEMPLATE_EQUIP_STATISTIC_SHELF_TIME_FAIL = "Bieu_mau_import_kiem_ke_u_ke_so_lan_LOI.xlsx";
	public static final String TEMPLATE_EQUIP_STATISTIC_SHELF_ROUTE_FAIL = "Bieu_mau_import_kiem_ke_u_ke_tuyen_LOI.xlsx";
	
	/* Quan ly de nghi muon thiet bi */
	public static final String TEMPLATE_IMPORT_RECORD_LEND_FAIL = "Bieu_mau_import_quan_ly_de_nghi_muon_thiet_bi_LOI.xlsx";
	public static final String TEMPLATE_IMPORT_RECORD_LEND = "Bieu_mau_import_quan_ly_de_nghi_muon_thiet_bi.xls";
	public static final String TEMPLATE_RECORD_LEND_PRINT = "Bieu_mau_in_quan_ly_de_nghi_muon_thiet_bi";
	/* Quan ly de nghi thu hoi thiet bi */
	public static final String TEMPLATE_IMPORT_RECORD_SUGGEST_EVICTION_FAIL = "Bieu_mau_import_bien_ban_de_nghi_thu_hoi_thiet_bi_LOI.xlsx";
	public static final String TEMPLATE_IMPORT_RECORD_SUGGEST_EVICTION = "Bieu_mau_import_bien_ban_de_nghi_thu_hoi_thiet_bi.xlsx";
	public static final String TEMPLATE_RECORD_SUGGEST_EVICTION_PRINT = "Bieu_mau_in_bien_ban_de_nghi_thu_hoi_thiet_bi";
	/**Import Equip item config**/
	public static final String TEMPLATE_EQUIP_ITEM_CONFIG_FAIL = "Bieu_mau_import_dinh_muc_Loi.xlsx";
	public static final String TEMPLATE_EQUIP_ITEM_CONFIG_EXPORT = "Bieu_mau_import_dinh_muc.xlsx";
	public static final String EQUIP_ITEM_CONFIG_EXCEL = "Thiet_lap_dinh_muc_hang_muc";
	public static final String TEMPLATE_EQUIP_ITEM_CONFIG_EXPORT_NEW = "export-thiet-lap-dinh-muc-hang-muc.xlsx";
	public static final String EQUIP_ITEMCONFIG_EXPORT_EXCEL = "Thong_Tin_Dinh_Muc_";
	
	/**Quan ly sua chua*/
	public static final String TEMPLATE_IMPORT_REPAIR_ERR = "Bieu_mau_impot_phieu_sua_chua_loi.xlsx"; // cai nay khong co ngay het han bao hanh
	public static final String TEMPLATE_IMPORT_REPAIR_FAIL = "Bieu_mau_impot_phieu_sua_chua_fail.xlsx"; //// cai nay co ngay het han bao hanh
	public static final String TEMPLATE_REPAIR_RECORD_DELIVERY = "Danh_sach_phieu_sua_chua_thiet_bi";
	/**Quan ly chuyen kho*/
	public static final String TEMPLATE_STOCK_TRAN_FORM= "Danh_sach_phieu_chuyen_kho";
	public static final String TEMPLATE_IMPORT_STOCK_TRAN_FAIL = "Bieu_mau_import_phieu_chuyen_kho_LOI.xlsx";
	/**
	 * Khai bao cac template bao cao
	 * */
	public static final String RPT_KM_5_1_TKKM_TEMPLATE = "Report_Promotion.xlsx";
	public static final String RPT_KM_5_1_TKKM_FILE_NAME = "Bc_tong_ket_khuyen_mai_";
	public static final String RPT_XNT_4_1_TEMPLATE = "Report_Warehouse.xlsx";
	public static final String RPT_XNT_4_1_FILE_NAME = "BC_xuat_nhap_ton_";
	public static final String RPT_BC_NVBH_ACTIVE_TEMPLATE = "Report_NVBH_Active.xlsx";
	public static final String RPT_BC_NVBH_ACTIVE_FILE_NAME = "Bc_NVBH_";
	public static final String RPT_BC_DH_CHO_KH_TEMPLATE = "Report_DonHangChoKH.xlsx";
	public static final String RPT_BC_DH_CHO_KH_FILE_NAME = "Bc_Don_hang_thanh_cong_";
	
	public static final String RPT_1_1_THDHPSTN_TEMPLATE = "Report_1d1_Thdhpstn.xlsx";
	public static final String RPT_1_1_THDHPSTN_FILE_NAME = "Bc_Don_hang_phat_sinh_trong_ngay_";
	
	public static final String RPT_1_2_TKDHGTN_TEMPLATE = "Report_1d2_tkdhgtn.xlsx";
	public static final String RPT_1_2_TKDHGTN_FILE_NAME = "Bc_Thong_ke_don_hang_giao_trong_ngay_";
	
	public static final String RPT_1_3_GHTNVGH_TEMPLATE = "Report_1d3_ghtnvgh.xlsx";
	public static final String RPT_1_3_GHTNVGH_FILE_NAME = "Bc_Don_hang_theo_NVGH_";
	
	public static final String RPT_1_4_BCDHR_TEMPLATE = "Report_1d4_bcdhr.xlsx";
	public static final String RPT_1_4_BCDHR_FILE_NAME = "Bc_Don_hang_rot_";
	
	public static final String RPT_3_1_REPORT_REVEIVABLE_SUMMARY_TEMPLATE = "Bao_cao_cong_no_phai_thu_tong_hop.xlsx";
	public static final String RPT_3_1_REPORT_REVEIVABLE_SUMMARY_FILE_NAME = "Bao_cao_cong_no_phai_thu_tong_hop_";
	
	public static final String RPT_3_2_BCCNPTCT_TEMPLATE = "Report_Debit_Retrieve_3_2.xlsx";
	public static final String RPT_3_2_BCCNPTCT_FILE_NAME = "Bc_Cong_No_Phai_Thu_Chi_Tiet_";
	
	public static final String RPT_3_3_BCCNPTR_TEMPLATE = "Report_Debit_Return_3_3.xlsx";
	public static final String RPT_3_3_BCCNPTR_FILE_NAME = "Bc_Cong_No_Phai_Tra_Tong_Hop_";
	public static final String RPT_3_4_REPORT_REVEIVABLE_SUMMARY_FILE_NAME = "Bao_cao_cong_no_phai_tra_chi_tiet_";
	
	public static final String RPT_6_1_2_THBHCNVBH_TEMPLATE = "bc6.1.2.BcTGNVLV.xlsx";
	public static final String RPT_6_1_2_THBHCNVBH_FILE_NAME = "Bc_Thoi_gian_lam_viec_cua_NVBH_";
	
	public static final String EXPORT_SLDS_KH_TEMPLATE = "Bao_cao_san_luong_doanh_so_kh.xlsx";
	public static final String EXPORT_SLDS_KH_FILENAME = "Bao_cao_san_luong_doanh_so_kh_";
	
	public static final String EXPORT_DS_CT_KH_TEMPLATE = "Bao_cao_doanh_so_chuong_trinh_kh.xlsx";
	public static final String EXPORT_DS_CT_KH_FILENAME = "Bao_cao_doanh_so_chuong_trinh_kh_";
	
	public static final String RPT_5_5_REPORT_PROGRESS_PROGRAM_TEMPLATE = "Bao_cao_tien_do_thuc_hien_chuong_trinh.xlsx";
	public static final String RPT_5_5_REPORT_PROGRESS_PROGRAM_FILE_NAME = "Bao_cao_tien_do_thuc_hien_chuong_trinh_";

	public static final String EXPORT_ASO_INACTIVE_TEMPLATE = "Bao_cao_aso_khong_hoat_dong.xlsx";
	public static final String EXPORT_ASO_INACTIVE_FILENAME = "Bao_cao_aso_khong_hoat_dong_";
	
	public static final String EXPORT_TIME_VISIT_CUSTOMER_TEMPLATE = "Bao_cao_thoi_gian_ghe_tham_kh.xlsx";
	public static final String EXPORT_TIME_VISIT_CUSTOMER_FILENAME = "Bao_cao_thoi_gian_ghe_tham_kh_";

	public static final String EXPORT_NOT_VISIT_CUSTOMER_ROUTE_TEMPLATE = "Bao_cao_khong_ghe_tham_kh_trong_tuyen.xlsx";
	public static final String EXPORT_NOT_VISIT_CUSTOMER_ROUTE_FILENAME = "Bao_cao_khong_ghe_tham_kh_trong_tuyen_";

	public static final String EXPORT_PO_DETAIL_ORDER_TEMPLATE = "Bao_cao_chi_tiet_don_mua_hang.xlsx";
	public static final String EXPORT_PO_DETAIL_ORDER_FILENAME = "Bao_cao_chi_tiet_don_mua_hang_";
	
	public static final String EXPORT_PROGRAM_DETAIL_TEMPLATE = "Bao_cao_chi_tiet_khuyen_mai.xlsx";
	public static final String EXPORT_PROGRAM_DETAIL_FILENAME = "Bao_cao_chi_tiet_khuyen_mai_";
	
	public static final String EXPORT_BC_TGTM_TEMPLATE = "Bao_cao_thoi_gian_ngung_hoat_dong.xlsx";
	public static final String EXPORT_BC_TGTM_FILENAME = "Bao_cao_thoi_gian_ngung_hoat_dong_";
	
	public static final String RPT_6_4_4_REPORT_SALEORDER_DATA_TEMPLATE = "Bao_cao_du_lieu_don_hang.xlsx";
	public static final String RPT_6_4_4_REPORT_SALEORDER_DATA_FILE_NAME = "Bao_cao_du_lieu_don_hang_";
	
	public static final String RPT_6_4_5_REPORT_STOCKTRANS_DATA_TEMPLATE = "Bao_cao_du_lieu_don_dieu_chinh.xlsx";
	public static final String RPT_6_4_5_REPORT_STOCKTRANS_DATA_FILE_NAME = "Bao_cao_du_lieu_don_dieu_chinh_";
	
	public static final String RPT_2_1_CTHHMV_FILE_NAME = "Bang_ke_chung_tu_hang_hoa_mua_vao_";
	public static final String RPT_2_2_SSSLDHMH_FILE_NAME = "So_sanh_so_luong_dat_hang_va_giao_hang_theo_don_hang_mua_";
	public static final String TEMPLATE_EQUIP_STOCK_ADJUST_FORM = "Bieu_mau_import_nhap_kho_dieu_chinh.xlsx";
	
	/**Bien Ban Bao Mat*/
	public static final String EQUIP_LOST_RECORD_EXPORT = "Danh_Sach_Bien_Ban_Bao_Mat_";
	public static final String TEMPLATE_EQUIP_LOST_RECORD_EXPORT = "template_ds_bien_ban_bao_mat.xlsx";
	public static final String TEMPLATE_EQUIP_LOST_RECORD_FAIL = "template_import_ban_bao_mat_loi.xlsx";
	public static final String TEMPLATE_PRINT_LOST_RECORD = "Bien_Ban_Bao_Mat";
	public static final String TEMPLATE_PRINT_EQUIP_LOST_RECORD = "printfEquipLostRecord";
	
	public static final String RPT_EQ_IM_EX_TEMPLATE = "template_eq_im_ex.xlsx";
	public static final String RPT_EQ_IM_EX_FILE_NAME = "Bao_cao_xuat_nhap_ton_thiet_bi_";
	/**phieu sua chua chua thuc hien*/
	public static final String TEMPLATE_EQUIP_BCPSCCTH_EXPORT = "bc_psccth.xlsx";
	public static final String EXPORT_BCPSCCTH = "BC_phieu_sua_chua_chua_thuc_hien_";
	public static final String EQUIP_REPORT_BSDSMTTK= "bao_cao_danh_sach_muon_thiet_bi";
	// bao cao TL 1.1
	public static final String TEMPLATE_BCDSTMTDTK = "bcdstdtmtltk_tl11.xlsx";
	public static final String EXPORT_BCDSTDTMTK = "BAO_CAO_DANH_SACH_THIET_BI_THANH_LY_";
}

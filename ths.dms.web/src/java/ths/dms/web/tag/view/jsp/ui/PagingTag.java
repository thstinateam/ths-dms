/*
 * $Id: PagingTag.java 103 2009-05-30 05:46:27Z dung.nguyen $
 
 *  
 *  $Author: dung.nguyen $
 *  $Rev: 103 $
 */
package ths.dms.web.tag.view.jsp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ui.AbstractUITag;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.tag.component.Paging;
import ths.dms.web.utils.StringUtil;

/**
 * The Class PagingTag.
 */
public class PagingTag extends AbstractUITag {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5994377982302742898L;

    /** The size. */
    protected String size;

    /** The page. */
    protected String page;

    /** The length. */
    protected String length;

    /** The mode. */
    protected String mode;

    /** The source. */
    protected String source;

    /** The prev. */
    protected String prev;

    /** The next. */
    protected String next;

    /**
     * Sets the size.
     * 
     * @param size
     *            the new size
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * Sets the page.
     * 
     * @param page
     *            the new page
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * Sets the length.
     * 
     * @param length
     *            the new length
     */
    public void setLength(String length) {
        this.length = length;
    }

    /**
     * Sets the mode.
     * 
     * @param mode
     *            the new mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the new source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * Sets the prev.
     * 
     * @param prev
     *            the new prev
     */
    public void setPrev(String prev) {
        this.prev = prev;
    }

    /**
     * Sets the next.
     * 
     * @param next
     *            the new next
     */
    public void setNext(String next) {
        this.next = next;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.struts2.views.jsp.ComponentTagSupport#getBean(com.opensymphony
     * .xwork2.util.ValueStack, javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    public Component getBean(ValueStack valueStack, HttpServletRequest request,
            HttpServletResponse response) {
        return new Paging(valueStack, request, response);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.views.jsp.ui.AbstractUITag#populateParams()
     */
    @Override
    protected void populateParams() {
        Paging p = (Paging) component;
        if (StringUtil.isNullOrEmpty(page)) {
            p.setInititalPage("0");
        } else {
            p.setInititalPage(page);
        }
        p.setTotalPage(length);
        p.setPageSize(size);
        if (StringUtil.isNullOrEmpty(mode)) {
            mode = "NextPrev";
        }
        p.setMode(mode);
        p.setSource(source);
        p.setPrev(prev);
        p.setNext(next);
    }

}

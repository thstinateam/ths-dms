/*
 * $Id: NavigationTag.java 103 2009-05-30 05:46:27Z dung.nguyen $
 
 *  
 *  $Author: dung.nguyen $
 *  $Rev: 103 $
 */
package ths.dms.web.tag.view.jsp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ui.AbstractClosingTag;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.tag.component.Navigation;

/**
 * The Class NavigationTag.
 */
public class NavigationTag extends AbstractClosingTag {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2740319257729799751L;

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.struts2.views.jsp.ComponentTagSupport#getBean(com.opensymphony
     * .xwork2.util.ValueStack, javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    public Component getBean(ValueStack vs, HttpServletRequest request,
            HttpServletResponse response) {
        return new Navigation(vs, request, response);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.views.jsp.ui.AbstractClosingTag#populateParams()
     */
    @Override
    protected void populateParams() {
        super.populateParams();
        Navigation v = (Navigation) component;
        v.setId(id);
        v.setCssClass(cssClass);
        v.setTitle(title);
    }
}

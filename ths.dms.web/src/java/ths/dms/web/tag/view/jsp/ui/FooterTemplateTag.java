/*
 * $Id: FooterTemplateTag.java 103 2009-05-30 05:46:27Z dung.nguyen $
 
 *  
 *  $Author: dung.nguyen $
 *  $Rev: 103 $
 */
package ths.dms.web.tag.view.jsp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ContextBeanTag;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.tag.component.FooterTemplate;

/**
 * The Class FooterTemplateTag.
 */
public class FooterTemplateTag extends ContextBeanTag {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6166065723207788063L;

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.struts2.views.jsp.ComponentTagSupport#getBean(com.opensymphony
     * .xwork2.util.ValueStack, javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    public Component getBean(ValueStack valuestack,
            HttpServletRequest httpservletrequest,
            HttpServletResponse httpservletresponse) {
        return new FooterTemplate(valuestack);
    }

}

/*
 * $Id: ImageTag.java 103 2009-05-30 05:46:27Z dung.nguyen $
 
 *  
 *  $Author: dung.nguyen $
 *  $Rev: 103 $
 */
package ths.dms.web.tag.view.jsp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ui.AbstractUITag;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.tag.component.Image;

/**
 * The Class ImageTag.
 */
public class ImageTag extends AbstractUITag {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4943811245324272084L;

    /** The alt. */
    private String alt;

    /** The src. */
    private String src;

    /** The url. */
    private String url;

    /** The theme. */
    private String theme;

    /**
     * Sets the alt.
     * 
     * @param alt
     *            the new alt
     */
    public void setAlt(String alt) {
        this.alt = alt;
    }

    /**
     * Sets the src.
     * 
     * @param src
     *            the new src
     */
    public void setSrc(String src) {
        this.src = src;
    }

    /**
     * Sets the url.
     * 
     * @param url
     *            the new url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.struts2.views.jsp.ui.AbstractUITag#setTheme(java.lang.String)
     */
    public void setTheme(String theme) {
        this.theme = theme;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.struts2.views.jsp.ComponentTagSupport#getBean(com.opensymphony
     * .xwork2.util.ValueStack, javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    public Component getBean(ValueStack vs, HttpServletRequest request,
            HttpServletResponse response) {
        return new Image(vs, request, response);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.views.jsp.ui.AbstractUITag#populateParams()
     */
    @Override
    protected void populateParams() {
        super.populateParams();
        Image v = (Image) component;
        v.setId(id);
        v.setAlt(alt);
        v.setSrc(src);
        v.setTheme(theme);
        v.setUrl(url);
    }
}

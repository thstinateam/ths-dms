package ths.dms.web.tag.view.jsp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ui.AbstractClosingTag;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.tag.component.Anchor;

/**
 * The Class AnchorTag.
 */
public class AnchorTag extends AbstractClosingTag {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3039700669000290497L;

    /** The href. */
    private String href;

    /** The action. */
    private String action;

    /** The namespace. */
    private String namespace;

    /**
     * Sets the href.
     * 
     * @param href
     *            the new href
     */
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * Sets the action.
     * 
     * @param action
     *            the new action
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * Sets the namespace.
     * 
     * @param namespace
     *            the new namespace
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.struts2.views.jsp.ComponentTagSupport#getBean(com.opensymphony
     * .xwork2.util.ValueStack, javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    public Component getBean(ValueStack vs, HttpServletRequest request,
            HttpServletResponse response) {
        return new Anchor(vs, request, response);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.views.jsp.ui.AbstractClosingTag#populateParams()
     */
    @Override
    protected void populateParams() {
        super.populateParams();
        Anchor v = (Anchor) component;
        v.setId(id);
        v.setCssClass(cssClass);
        v.setTitle(title);
        v.setHref(href);
        v.setAction(action);
        v.setNamespace(namespace);
    }
}

/*
 * $Id: ItemTemplateTag.java 180 2009-07-07 15:01:44Z dung.nguyen $
 
 *  
 *  $Author: dung.nguyen $
 *  $Rev: 180 $
 */
package ths.dms.web.tag.view.jsp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.BodyContent;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ContextBeanTag;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.tag.component.ItemTemplate;

/**
 * The Class ItemTemplateTag.
 */
public class ItemTemplateTag extends ContextBeanTag {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8543779612662446820L;

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.struts2.views.jsp.ComponentTagSupport#getBean(com.opensymphony
     * .xwork2.util.ValueStack, javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    public Component getBean(ValueStack valuestack,
            HttpServletRequest httpservletrequest,
            HttpServletResponse httpservletresponse) {
        return new ItemTemplate(valuestack);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.views.jsp.ComponentTagSupport#doEndTag()
     */
    @Override
    public int doEndTag() throws JspException {
        component = null;
        return 6;
    }

    /*
     * (non-Javadoc)
     * 
     * @see javax.servlet.jsp.tagext.BodyTagSupport#doAfterBody()
     */
    @Override
    public int doAfterBody() throws JspException {
        BodyContent bodyContent = getBodyContent();
        String body = bodyContent != null ? bodyContent.getString() : "";

        if (bodyContent != null) {
            bodyContent.clearBody();
        }

        boolean again = component.end(pageContext.getOut(), body);
        if (again) {
            return EVAL_BODY_AGAIN;
        }

        return SKIP_BODY;
    }
}

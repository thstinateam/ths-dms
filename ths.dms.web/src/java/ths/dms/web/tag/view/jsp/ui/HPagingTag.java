package ths.dms.web.tag.view.jsp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ui.AbstractUITag;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.tag.component.HPaging;
import ths.dms.web.utils.StringUtil;

public class HPagingTag extends AbstractUITag {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2576951558201005827L;
	
	protected String id;
	protected String page;

	protected String mode;
	protected String source;

	protected String scriptFunctionName;
	protected String firstPageClass;
	protected String lastPageClass;

	protected String nextPageClass;
	protected String prePageClass;

	protected String currentPageClass;

	protected String itemClass;

	protected String listParam;

	protected boolean showPrevNext = true;
	protected boolean showFirstLast = true;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setScriptFunctionName(String scriptFunctionName) {
		this.scriptFunctionName = scriptFunctionName;
	}

	public void setFirstPageClass(String firstPageClass) {
		this.firstPageClass = firstPageClass;
	}

	public void setLastPageClass(String lastPageClass) {
		this.lastPageClass = lastPageClass;
	}

	public void setItemClass(String itemClass) {
		this.itemClass = itemClass;
	}

	public void setNextPageClass(String nextPageClass) {
		this.nextPageClass = nextPageClass;
	}

	public void setPrePageClass(String prePageClass) {
		this.prePageClass = prePageClass;
	}

	public void setCurrentPageClass(String currentPageClass) {
		this.currentPageClass = currentPageClass;
	}

	public void setListParam(String listParam) {
		this.listParam = listParam;
	}

	public void setShowPrevNext(boolean showPrevNext) {
		this.showPrevNext = showPrevNext;
	}

	public void setShowFirstLast(boolean showFirstLast) {
		this.showFirstLast = showFirstLast;
	}

	@Override
	public Component getBean(ValueStack valueStack, HttpServletRequest request,
			HttpServletResponse response) {
		return new HPaging(valueStack, request, response);
	}

	@Override
	protected void populateParams() {
		HPaging p = (HPaging) component;

		if (StringUtil.isNullOrEmpty(mode)) {
			mode = "NextPrev";
		}
		p.setId(id);
		p.setSource(source);

		p.setScriptFunctionName(scriptFunctionName);
		p.setItemClass(itemClass);
		p.setFirstPageClass(firstPageClass);
		p.setLastPageClass(lastPageClass);
		p.setNextPageClass(nextPageClass);
		p.setPrePageClass(prePageClass);
		p.setCurrentPageClass(currentPageClass);
		p.setListParam(listParam);

		p.setShowFirstLast(showFirstLast);
		p.setShowPrevNext(showPrevNext);

	}

}

package ths.dms.web.tag.view.jsp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ui.AbstractUITag;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.tag.component.MPaging;

public class MPagingTag extends AbstractUITag {
	private static final long serialVersionUID = -2576951558201005827L;
	
	protected String id;
	protected String source;
	protected String action;
	protected String listParam;
	protected boolean showTotalPage;
	protected String theme = "simple";
	
	protected boolean showPrevNext = true;
	protected boolean showFirstLast = true;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setShowTotalPage(boolean showTotalPage) {
		this.showTotalPage = showTotalPage;
	}

	public void setListParam(String listParam) {
		this.listParam = listParam;
	}

	public void setShowPrevNext(boolean showPrevNext) {
		this.showPrevNext = showPrevNext;
	}

	public void setShowFirstLast(boolean showFirstLast) {
		this.showFirstLast = showFirstLast;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getSource() {
		return source;
	}

	public String getListParam() {
		return listParam;
	}

	public boolean isShowTotalPage() {
		return showTotalPage;
	}

	public boolean isShowPrevNext() {
		return showPrevNext;
	}

	public boolean isShowFirstLast() {
		return showFirstLast;
	}

	@Override
	public Component getBean(ValueStack valueStack, HttpServletRequest request,
			HttpServletResponse response) {
		return new MPaging(valueStack, request, response);
	}

	@Override
	protected void populateParams() {
		MPaging p = (MPaging) component;

		p.setId(id);
		p.setSource(source);
		p.setAction(action);
		p.setListParam(listParam);
		p.setShowTotalPage(showTotalPage);
		p.setTheme(theme);
		p.setShowFirstLast(showFirstLast);
		p.setShowPrevNext(showPrevNext);
	}
}

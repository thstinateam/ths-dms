/*
 * $Id: RepeaterTag.java 103 2009-05-30 05:46:27Z dung.nguyen $
 
 *  
 *  $Author: dung.nguyen $
 *  $Rev: 103 $
 */
package ths.dms.web.tag.view.jsp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ContextBeanTag;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.tag.component.Repeater;

/**
 * The Class RepeaterTag.
 */
public class RepeaterTag extends ContextBeanTag {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1025226774037980230L;

    /** The status. */
    protected String status;

    /** The value. */
    protected String value;

    /** The from. */
    protected int from;

    /** The to. */
    protected int to;

    /**
     * Sets the from.
     * 
     * @param from
     *            the new from
     */
    public void setFrom(int from) {
        this.from = from;
    }

    /**
     * Sets the to.
     * 
     * @param to
     *            the new to
     */
    public void setTo(int to) {
        this.to = to;
    }

    /**
     * Sets the status.
     * 
     * @param status
     *            the new status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Sets the value.
     * 
     * @param value
     *            the new value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.struts2.views.jsp.ComponentTagSupport#getBean(com.opensymphony
     * .xwork2.util.ValueStack, javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    public Component getBean(ValueStack valueStack, HttpServletRequest request,
            HttpServletResponse response) {
        return new Repeater(valueStack);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.views.jsp.ContextBeanTag#populateParams()
     */
    @Override
    protected void populateParams() {
        super.populateParams();
        Repeater r = (Repeater) component;
        r.setFrom(from);
        r.setTo(to);
        r.setStatus(status);
        r.setValue(value);
    }
}

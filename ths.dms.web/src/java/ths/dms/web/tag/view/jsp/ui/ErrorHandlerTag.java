package ths.dms.web.tag.view.jsp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ui.AbstractUITag;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.tag.component.ErrorHandler;

/**
 * The Class ErrorHandlerTag.
 */
public class ErrorHandlerTag extends AbstractUITag {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7549916513154009997L;

    /** The show title. */
    private String showTitle;

    /**
     * Sets the show title.
     * 
     * @param showTitle
     *            the new show title
     */
    public void setShowTitle(String showTitle) {
        this.showTitle = showTitle;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.struts2.views.jsp.ComponentTagSupport#getBean(com.opensymphony
     * .xwork2.util.ValueStack, javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    public Component getBean(ValueStack stack, HttpServletRequest req,
            HttpServletResponse res) {
        return new ErrorHandler(stack, req, res);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.views.jsp.ui.AbstractUITag#populateParams()
     */
    @Override
    protected void populateParams() {
        super.populateParams();
        ErrorHandler v = (ErrorHandler) component;
        if (showTitle != null) {
            v.setShowTitle(Boolean.valueOf(showTitle).booleanValue());
        }
    }
}

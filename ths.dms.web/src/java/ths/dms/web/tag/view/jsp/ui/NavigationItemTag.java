/*
 * $Id: NavigationItemTag.java 103 2009-05-30 05:46:27Z dung.nguyen $
 
 *  
 *  $Author: dung.nguyen $
 *  $Rev: 103 $
 */
package ths.dms.web.tag.view.jsp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ui.AbstractUITag;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.tag.component.NavigationItem;

/**
 * The Class NavigationItemTag.
 */
public class NavigationItemTag extends AbstractUITag {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3707447441710055526L;

    /** The action. */
    protected String action;

    /** The sub action. */
    protected String subAction;

    /** The roles. */
    protected String roles;

    /** The namespace. */
    protected String namespace;

    /**
     * Sets the action.
     * 
     * @param action
     *            the new action
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * Sets the sub action.
     * 
     * @param subAction
     *            the new sub action
     */
    public void setSubAction(String subAction) {
        this.subAction = subAction;
    }

    /**
     * Sets the roles.
     * 
     * @param roles
     *            the new roles
     */
    public void setRoles(String roles) {
        this.roles = roles;
    }

    /**
     * Sets the namespace.
     * 
     * @param namespace
     *            the new namespace
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.struts2.views.jsp.ComponentTagSupport#getBean(com.opensymphony
     * .xwork2.util.ValueStack, javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    public Component getBean(ValueStack vs, HttpServletRequest request,
            HttpServletResponse response) {
        return new NavigationItem(vs, request, response);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.views.jsp.ui.AbstractUITag#populateParams()
     */
    @Override
    protected void populateParams() {
        super.populateParams();
        NavigationItem v = (NavigationItem) component;
        v.setId(id);
        v.setAction(action);
        v.setSubAction(subAction);
        v.setCssClass(cssClass);
        v.setTitle(title);
        v.setValue(value);
        v.setKey(key);
        v.setRoles(roles);
        v.setNamespace(namespace);
    }
}

/*
 * $Id: ResourceTag.java 180 2009-07-07 15:01:44Z dung.nguyen $
 
 *  
 *  $Author: dung.nguyen $
 *  $Rev: 180 $
 */
package ths.dms.web.tag.view.jsp.ui;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.Component;
import org.apache.struts2.views.jsp.ui.AbstractUITag;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.tag.component.Resource;

/**
 * The Class ResourceTag.
 */
public class ResourceTag extends AbstractUITag {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -5073012465676734638L;

    /** The type. */
    protected String type;

    /** The media. */
    protected String media;

    /**
     * Sets the type.
     * 
     * @param type
     *            the new type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Sets the media.
     * 
     * @param media
     *            the new media
     */
    public void setMedia(String media) {
        this.media = media;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.struts2.views.jsp.ComponentTagSupport#getBean(com.opensymphony
     * .xwork2.util.ValueStack, javax.servlet.http.HttpServletRequest,
     * javax.servlet.http.HttpServletResponse)
     */
    @Override
    public Component getBean(ValueStack vs, HttpServletRequest request,
            HttpServletResponse response) {
        return new Resource(vs, request, response);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.views.jsp.ui.AbstractUITag#populateParams()
     */
    @Override
    protected void populateParams() {
        super.populateParams();
        Resource v = (Resource) component;
        v.setId(id);
        v.setMedia(media);
        v.setName(name);
        v.setTheme(theme);
        v.setType(type);
    }
}

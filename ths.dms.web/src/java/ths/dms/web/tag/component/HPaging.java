package ths.dms.web.tag.component;

import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.UIBean;
import org.apache.struts2.views.annotations.StrutsTagAttribute;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.utils.StringUtil;

public class HPaging extends UIBean {

	public static final String TEMPLATE = "hpaging";

	protected String id;
	protected String pageSize;
	protected String totalPage;

	protected String source;

	protected String prev;
	protected String next;

	protected String scriptFunctionName;

	protected String firstPageClass;
	protected String lastPageClass;

	protected String nextPageClass;
	protected String prePageClass;

	protected String itemClass;

	protected String currentPageClass;

	protected KPaging<Object> paginated;

	protected List<String> listItem;

	protected String listParam;

	protected boolean showPrevNext;
	
	protected boolean showFirstLast;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public void setTotalPage(String totalPage) {
		this.totalPage = totalPage;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setPrev(String prev) {
		this.prev = prev;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public void setScriptFunctionName(String scriptFunctionName) {
		this.scriptFunctionName = scriptFunctionName;
	}

	public void setFirstPageClass(String firstPageClass) {
		this.firstPageClass = firstPageClass;
	}

	public void setLastPageClass(String lastPageClass) {
		this.lastPageClass = lastPageClass;
	}

	public void setItemClass(String itemClass) {
		this.itemClass = itemClass;
	}

	public void setNextPageClass(String nextPageClass) {
		this.nextPageClass = nextPageClass;
	}

	public void setPrePageClass(String prePageClass) {
		this.prePageClass = prePageClass;
	}

	public void setCurrentPageClass(String currentPageClass) {
		this.currentPageClass = currentPageClass;
	}

	@StrutsTagAttribute(description = "The URL.")
	public void setListParam(String listParam) {
		this.listParam = listParam;
	}

	public void setShowPrevNext(boolean showPrevNext) {
		this.showPrevNext = showPrevNext;
	}

	public void setShowFirstLast(boolean showFirstLast) {
		this.showFirstLast = showFirstLast;
	}

	public HPaging(ValueStack stack, HttpServletRequest request,
			HttpServletResponse response) {
		super(stack, request, response);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getDefaultTemplate() {
		return TEMPLATE;
	}

	@Override
	public void evaluateParams() {
		listItem = new ArrayList<String>();
		buildId();
		buildListParam();
		
		String prevUrl = buildUrlWithClass(paginated.getPage() - 1,
				prePageClass, "Prev");
		String nextUrl = buildUrlWithClass(paginated.getPage() + 1,
				nextPageClass, "Next");
		
		addParameter("id", id);
		
		if (isPreviousPage()) {
			addParameter("prev", prevUrl);
		}
		if (isNextPage()) {
			addParameter("next", nextUrl);
		}

		if (isPreviousPage() || isNextPage()) {
			addParameter("enabled", true);
		}

		if (paginated.getPages() != null && paginated.getPages().length > 0) {
			buildListItem(paginated.getPages(), paginated.getPage());
		} else {
			listItem = new ArrayList<String>();
		}

		addParameter("prevText", "Previous");
		addParameter("nextText", "Next");
		addParameter("paginated", listItem);
//		addParameter("currentPage", paginated.getPage());
		addParameter("showPrevNext", showPrevNext);
		addParameter("showFirstLast", showFirstLast);
//		addParameter("viewCurrentPageInput", paginated.getPage() + 1);
		if (paginated.getPage() < (paginated.getTotalPages() - 2)) {
			addParameter("last", buildUrlWithClass(paginated.getTotalPages() - 1,
				lastPageClass, "»"));			
		}
		if (paginated.getPage() > 1) {
			addParameter("first", buildUrlWithClass(0, firstPageClass, "«"));
		}
//		addParameter("totalPage", paginated.getTotalPages());
		addParameter("scriptFunctionName", scriptFunctionName);
//		addParameter("listParam", listParam);
	}

	private void buildId() {
		if (id != null) {
			id = ensureAttributeSafelyNotEscaped(findString(id));
		}
	}
	
	private void buildListParam() {
		if (listParam != null)
			listParam = ensureAttributeSafelyNotEscaped(findString(listParam));

	}
	
	private void buildListItem(Integer[] pages, int currentPage) {
		for (int i : pages) {
			if (currentPage == i) {
				listItem.add(String.format("<li class=\"PagingGeneralItem FloatLeft\"><a href=\"javascript:void(0);\" class='%s'>%d</a></li>",
						StringUtil.fromNullToEmtpyString(currentPageClass),
						i + 1));
			} else {
				listItem.add(buildUrl(i));
			}
		}

	}

	@Override
	public boolean start(Writer writer) {
		paginated = convert(findValue(source));
		return super.start(writer);
	}

	@SuppressWarnings("unchecked")
	private KPaging<Object> convert(Object value) {
		return (KPaging<Object>) value;
	}

	private String buildUrl(int page) {
		String onclickFunction;
		if (StringUtil.isNullOrEmpty(listParam)) {
			onclickFunction = "return " + scriptFunctionName + "("
					+ String.valueOf(page) + ");";
		} else {
			onclickFunction = "return " + scriptFunctionName + "("
					+ String.valueOf(page) + "," + listParam + ");";
		}

		String url = String.format(
				"<li class=\"PagingGeneralItem FloatLeft\"><a href=\"javascript:void(0);\" onclick=\"%s\" class=\"%s\">%d</a></li>",
				onclickFunction, itemClass, page + 1);

		return url;
	}

	private String buildUrlWithClass(int page, String cssClass, String symbol) {
		String onclickFunction;
		if (StringUtil.isNullOrEmpty(listParam)) {
			onclickFunction = "return " + scriptFunctionName + "("
					+ String.valueOf(page) + ");";
		} else {
			onclickFunction = "return " + scriptFunctionName + "("
					+ String.valueOf(page) + "," + listParam + ");";
		}
		String url;
		if (StringUtil.isNullOrEmpty(cssClass)) {
			url = String.format("<li class=\"PagingGeneralItem FloatLeft\"><a href=\"javascript:void(0);\" onclick=\"%s\">%s</a></li>",
					onclickFunction, symbol);

		} else {
			url = String.format("<li class=\"PagingGeneralItem FloatLeft\"><a href=\"javascript:void(0);\"  onclick=\"%s\" class=\"%s\">%s</a></li>",
					onclickFunction, cssClass, symbol);
		}

		return url;

	}

	public boolean isNextPage() {

		return paginated.getPage() < paginated.getTotalPages() - 1;
	}

	public boolean isPreviousPage() {

		return paginated.getPage() > 0;
	}

}

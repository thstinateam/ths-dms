/*
 * $Id: Resource.java 180 2009-07-07 15:01:44Z dinhthaiha $
 
 *  
 *  $Author: dinhthaiha $
 *  $Rev: 180 $
 */
package ths.dms.web.tag.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.UIBean;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.utils.StringUtil;

/**
 * The Class Resource.
 */
public class Resource extends UIBean {

    /** The Constant TEMPLATE. */
    public static final String TEMPLATE = "resource";

    /** The type. */
    protected String type;

    /** The media. */
    protected String media;

    /** The context path. */
    protected String contextPath;

    /**
     * Sets the type.
     * 
     * @param type
     *            the new type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Sets the media.
     * 
     * @param media
     *            the new media
     */
    public void setMedia(String media) {
        this.media = media;
    }

    /**
     * Instantiates a new resource.
     * 
     * @param stack
     *            the stack
     * @param request
     *            the request
     * @param response
     *            the response
     */
    public Resource(ValueStack stack, HttpServletRequest request,
            HttpServletResponse response) {
        super(stack, request, response);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#getDefaultTemplate()
     */
    @Override
    protected String getDefaultTemplate() {
        return TEMPLATE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#evaluateExtraParams()
     */
    @Override
    public void evaluateExtraParams() {
        super.evaluateExtraParams();

        if (type != null) {
            addParameter("type",
                    ensureAttributeSafelyNotEscaped(findString(type)));
        }

        if (media != null) {
            addParameter("media",
                    ensureAttributeSafelyNotEscaped(findString(media)));
        }

        String contextPath = request.getContextPath();
        if (StringUtil.isNullOrEmpty(type) || type.equals("stylesheet")) {
            if (StringUtil.isNullOrEmpty(theme)) {
                contextPath =
                        String.format("%s/themes/default/stylesheets/%s.css",
                                contextPath, name);
            } else {
                contextPath =
                        String.format("%s/themes/%s/stylesheets/%s.css",
                                contextPath, theme, name);
            }
        } else {
            if (StringUtil.isNullOrEmpty(theme)) {
                contextPath =
                        String.format("%s/themes/default/javascripts/%s.js",
                                contextPath, name);
            } else {
                contextPath =
                        String.format("%s/themes/%s/javascripts/%s.js",
                                contextPath, theme, name);
            }
        }

        addParameter("href",
                ensureAttributeSafelyNotEscaped(findString(contextPath)));
    }
}

/*
 * $Id: SeparatorTemplate.java 103 2009-05-30 05:46:27Z dinhthaiha $
 
 *  
 *  $Author: dinhthaiha $
 *  $Rev: 103 $
 */
package ths.dms.web.tag.component;

import java.io.Writer;

import javax.servlet.jsp.JspException;

import org.apache.struts2.components.ContextBean;

import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

/**
 * The Class SeparatorTemplate.
 */
public class SeparatorTemplate extends ContextBean {

    /**
     * Instantiates a new separator template.
     * 
     * @param stack
     *            the stack
     */
    public SeparatorTemplate(ValueStack stack) {
        super(stack);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.Component#end(java.io.Writer,
     * java.lang.String)
     */
    @Override
    public boolean end(Writer writer, String body) {
        Repeater repeater =
                (Repeater) findAncestor(Repeater.class);
        if (repeater != null) {
            repeater.setSeparatorTemplate(body);
        } else {
            LoggerFactory
                    .getLogger(getClass())
                    .error(
                            "separatorTemplate: could not find ancestor tag Repeater",
                            new JspException(
                                    "separatorTemplate: could not find ancestor tag Repeater"));
        }

        return super.end(writer, "");
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.Component#usesBody()
     */
    @Override
    public boolean usesBody() {
        return true;
    }
}

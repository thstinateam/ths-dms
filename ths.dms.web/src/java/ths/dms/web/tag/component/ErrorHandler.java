/*
 * $Id: ErrorHandler.java 103 2009-05-30 05:46:27Z dinhthaiha $
 
 *  
 *  $Author: dinhthaiha $
 *  $Rev: 103 $
 */
package ths.dms.web.tag.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.UIBean;

import com.opensymphony.xwork2.util.ValueStack;

/**
 * The Class ErrorHandler.
 */
public class ErrorHandler extends UIBean {

    /** The Constant TEMPLATE. */
    public static final String TEMPLATE = "errorhandler";

    /** The show title. */
    protected Boolean showTitle;

    /**
     * Sets the show title.
     * 
     * @param showTitle
     *            the new show title
     */
    public void setShowTitle(Boolean showTitle) {
        this.showTitle = showTitle;
    }

    /**
     * Instantiates a new error handler.
     * 
     * @param stack
     *            the stack
     * @param req
     *            the req
     * @param res
     *            the res
     */
    public ErrorHandler(ValueStack stack, HttpServletRequest req,
            HttpServletResponse res) {
        super(stack, req, res);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#getDefaultTemplate()
     */
    protected String getDefaultTemplate() {
        return TEMPLATE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#evaluateExtraParams()
     */
    public void evaluateExtraParams() {
        super.evaluateExtraParams();

        if (showTitle != null && showTitle) {
            addParameter("showTitle", showTitle);
        }

    }
}
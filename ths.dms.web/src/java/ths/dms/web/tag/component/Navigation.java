/*
 * $Id: Navigation.java 103 2009-05-30 05:46:27Z dinhthaiha $
 
 *  
 *  $Author: dinhthaiha $
 *  $Rev: 103 $
 */
package ths.dms.web.tag.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.ClosingUIBean;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.utils.StringUtil;

/**
 * The Class Navigation.
 */
public class Navigation extends ClosingUIBean {

    /** The Constant OPEN_TEMPLATE. */
    public static final String OPEN_TEMPLATE = "navigation";

    /** The Constant TEMPLATE. */
    public static final String TEMPLATE = OPEN_TEMPLATE + "-close";

    /**
     * Instantiates a new navigation.
     * 
     * @param stack
     *            the stack
     * @param req
     *            the req
     * @param res
     *            the res
     */
    public Navigation(ValueStack stack, HttpServletRequest req,
            HttpServletResponse res) {
        super(stack, req, res);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.ClosingUIBean#getDefaultOpenTemplate()
     */
    public String getDefaultOpenTemplate() {
        return OPEN_TEMPLATE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#getDefaultTemplate()
     */
    protected String getDefaultTemplate() {
        return TEMPLATE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#evaluateExtraParams()
     */
    public void evaluateExtraParams() {
        super.evaluateExtraParams();

        if (!StringUtil.isNullOrEmpty(id)) {
            addParameter("id", ensureAttributeSafelyNotEscaped(findString(id)));
        }

        if (!StringUtil.isNullOrEmpty(cssClass)) {
            addParameter("cssClass",
                    ensureAttributeSafelyNotEscaped(findString(cssClass)));
        }

        if (!StringUtil.isNullOrEmpty(title)) {
            addParameter("title",
                    ensureAttributeSafelyNotEscaped(findString(title)));
        }
    }
}

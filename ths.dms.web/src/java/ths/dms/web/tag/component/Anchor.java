package ths.dms.web.tag.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.ClosingUIBean;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.utils.StringUtil;

/**
 * The Class Anchor.
 */
public class Anchor extends ClosingUIBean {

    /** The Constant OPEN_TEMPLATE. */
    public static final String OPEN_TEMPLATE = "a";

    /** The Constant TEMPLATE. */
    public static final String TEMPLATE = OPEN_TEMPLATE + "-close";

    /** The href. */
    private String href;

    /** The action. */
    private String action;

    /** The namespace. */
    private String namespace;

    /**
     * Sets the href.
     * 
     * @param href
     *            the new href
     */
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * Sets the action.
     * 
     * @param action
     *            the new action
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * Sets the namespace.
     * 
     * @param namespace
     *            the new namespace
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    /**
     * Instantiates a new anchor.
     * 
     * @param stack
     *            the stack
     * @param request
     *            the request
     * @param response
     *            the response
     */
    public Anchor(ValueStack stack, HttpServletRequest request,
            HttpServletResponse response) {
        super(stack, request, response);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.ClosingUIBean#getDefaultOpenTemplate()
     */
    @Override
    public String getDefaultOpenTemplate() {
        return OPEN_TEMPLATE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#getDefaultTemplate()
     */
    @Override
    protected String getDefaultTemplate() {
        return TEMPLATE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#evaluateExtraParams()
     */
    @Override
    public void evaluateExtraParams() {
        super.evaluateExtraParams();

        if (!StringUtil.isNullOrEmpty(action)) {
            addParameter("href", buildUrlForAction(action));
        }

        if (!StringUtil.isNullOrEmpty(href)) {
            addParameter("href", href);
        }

        if (!StringUtil.isNullOrEmpty(id)) {
            addParameter("id", ensureAttributeSafelyNotEscaped(findString(id)));
        }

        if (!StringUtil.isNullOrEmpty(cssClass)) {
            addParameter("cssClass",
                    ensureAttributeSafelyNotEscaped(findString(cssClass)));
        }

        if (!StringUtil.isNullOrEmpty(title)) {
            addParameter("title",
                    ensureAttributeSafelyNotEscaped(findString(title)));
        }
    }

    /**
     * Builds the url for action.
     * 
     * @param action
     *            the action
     * @return the string
     */
    private String buildUrlForAction(String action) {
        String namespace =
                determineNamespace(this.namespace, getStack(), request);
        return determineActionURL(action, namespace, null, request, response,
                null, request.getScheme(), true, true, false, true);
    }
}

/*
 * $Id: ItemTemplate.java 103 2009-05-30 05:46:27Z dinhthaiha $
 
 *  
 *  $Author: dinhthaiha $
 *  $Rev: 103 $
 */
package ths.dms.web.tag.component;

import java.io.Writer;
import java.util.Iterator;

import javax.servlet.jsp.JspException;

import org.apache.log4j.Logger;
import org.apache.struts2.components.ContextBean;
import org.apache.struts2.util.MakeIterator;
import org.apache.struts2.views.jsp.IteratorStatus;
import org.apache.struts2.views.jsp.IteratorStatus.StatusState;

import com.opensymphony.xwork2.util.ValueStack;
import com.opensymphony.xwork2.util.logging.LoggerFactory;

/**
 * The Class ItemTemplate.
 */
public class ItemTemplate extends ContextBean {

    /** The log. */
    protected Logger log = Logger.getLogger(ItemTemplate.class);

    /** The iterator. */
    @SuppressWarnings("unchecked")
    protected Iterator iterator;

    /** The status. */
    protected IteratorStatus status;

    /** The old status. */
    protected Object oldStatus;

    /** The status state. */
    protected StatusState statusState;

    /** The status attr. */
    protected String statusAttr;

    /** The value. */
    protected String value;

    /** The parent. */
    protected Repeater parent;

    /** The string builder. */
    protected StringBuilder stringBuilder;

    /**
     * Sets the value.
     * 
     * @param value
     *            the new value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Sets the status.
     * 
     * @param statusAttr
     *            the new status
     */
    public void setStatus(String statusAttr) {
        this.statusAttr = statusAttr;
    }

    /**
     * Instantiates a new item template.
     * 
     * @param stack
     *            the stack
     */
    public ItemTemplate(ValueStack stack) {
        super(stack);
        parent =
                (Repeater) findAncestor(Repeater.class);
        if (parent == null) {
            LoggerFactory
                    .getLogger(getClass())
                    .error(
                            "itemTemplate: could not find ancestor tag Repeater",
                            new JspException(
                                    "itemTemplate: could not find ancestor tag Repeater"));
        } else {
            statusAttr = parent.getStatus();
            value = parent.getValue();

            stringBuilder = new StringBuilder();
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.Component#usesBody()
     */
    @Override
    public boolean usesBody() {
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.Component#start(java.io.Writer)
     */
    @Override
    public boolean start(Writer writer) {

        if (parent != null) {

            if (statusAttr != null) {
                statusState =
                        new org.apache.struts2.views.jsp.IteratorStatus.StatusState();
                status = new IteratorStatus(statusState);
            }

            ValueStack stack = getStack();
            if (value == null) {
                value = "top";
            }

            iterator = MakeIterator.convert(findValue(value));
            if (iterator != null && iterator.hasNext()) {
                Object currentValue = iterator.next();
                stack.push(currentValue);

                String var = getVar();
                if (var != null && currentValue != null) {
                    putInContext(currentValue);
                }

                if (statusAttr != null) {
                    statusState.setLast(!iterator.hasNext());
                    oldStatus = stack.getContext().get(statusAttr);
                    stack.getContext().put(statusAttr, status);
                }

                return true;
            }
        }

        super.end(writer, "");
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.Component#end(java.io.Writer,
     * java.lang.String)
     */
    @Override
    public boolean end(Writer writer, String body) {

        if (parent != null) {
            ValueStack stack = getStack();
            if (iterator != null) {
                stack.pop();
            }

            if (iterator != null && iterator.hasNext()) {
                Object currentValue = iterator.next();

                stack.push(currentValue);
                putInContext(currentValue);
                if (status != null) {
                    statusState.next();
                    statusState.setLast(!iterator.hasNext());
                }

                stringBuilder.append(body);
                if (parent.getSeparatorTemplate() != null) {
                    stringBuilder.append(parent.getSeparatorTemplate());
                }

                return true;
            }

            if (status != null) {
                if (oldStatus == null) {
                    stack.getContext().put(statusAttr, null);
                } else {
                    stack.getContext().put(statusAttr, oldStatus);
                }
            }

            stringBuilder.append(body);
            parent.setItemTemplate(stringBuilder.toString());
        }

        super.end(writer, "");
        return false;
    }
}

/*
 * $Id: Image.java 103 2009-05-30 05:46:27Z dinhthaiha $
 
 *  
 *  $Author: dinhthaiha $
 *  $Rev: 103 $
 */
package ths.dms.web.tag.component;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.UIBean;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.utils.StringUtil;

/**
 * The Class Image.
 */
public class Image extends UIBean {

    /** The Constant TEMPLATE. */
    public static final String TEMPLATE = "image";

    /** The alt. */
    private String alt;

    /** The src. */
    private String src;

    /** The url. */
    private String url;

    /** The theme. */
    private String theme;

    /**
     * Sets the alt.
     * 
     * @param alt
     *            the new alt
     */
    public void setAlt(String alt) {
        this.alt = alt;
    }

    /**
     * Sets the src.
     * 
     * @param src
     *            the new src
     */
    public void setSrc(String src) {
        this.src = src;
    }

    /**
     * Sets the url.
     * 
     * @param url
     *            the new url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#setTheme(java.lang.String)
     */
    public void setTheme(String theme) {
        this.theme = theme;
    }

    /**
     * Instantiates a new image.
     * 
     * @param stack
     *            the stack
     * @param request
     *            the request
     * @param response
     *            the response
     */
    public Image(ValueStack stack, HttpServletRequest request,
            HttpServletResponse response) {
        super(stack, request, response);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#getDefaultTemplate()
     */
    @Override
    protected String getDefaultTemplate() {
        return TEMPLATE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#evaluateExtraParams()
     */
    public void evaluateExtraParams() {
        super.evaluateExtraParams();

        if (StringUtil.isNullOrEmpty(alt)) {
            alt = src;
        }

        addParameter("alt", findString(alt));

        if (StringUtil.isNullOrEmpty(url)) {
            String contextPath = request.getContextPath();
            if (StringUtil.isNullOrEmpty(theme)) {
                contextPath = String.format("%s/themes/default", contextPath);
            } else {
                contextPath = String.format("%s/themes/%s", contextPath, theme);
            }

            File f = new File(String.format("%s/images/%s", contextPath, src));
            if (f.exists()) {
                src = String.format("%s/images/%s", contextPath, src);
            } else {
                src =
                        String.format("%s/stylesheets/images/%s", contextPath,
                                src);
            }

            addParameter("src", findString(src));
        } else {
            addParameter("src", findString(url));
        }
    }
}

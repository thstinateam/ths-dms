package ths.dms.web.tag.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.components.UIBean;
import org.apache.struts2.util.TextProviderHelper;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.constant.ConstantManager;
import ths.dms.web.utils.StringUtil;

/**
 * The Class NavigationItem.
 */
public class NavigationItem extends UIBean {

    /** The Constant TEMPLATE. */
    public static final String TEMPLATE = "navigation-item";

    /** The action. */
    protected String action;

    /** The sub action. */
    protected String subAction;

    /** The roles. */
    protected String roles;

    /** The namespace. */
    protected String namespace;

    /**
     * Sets the action.
     * 
     * @param action
     *            the new action
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * Sets the sub action.
     * 
     * @param subAction
     *            the new sub action
     */
    public void setSubAction(String subAction) {
        this.subAction = subAction;
    }

    /**
     * Sets the roles.
     * 
     * @param roles
     *            the new roles
     */
    public void setRoles(String roles) {
        this.roles = roles;
    }

    /**
     * Sets the namespace.
     * 
     * @param namespace
     *            the new namespace
     */
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    /**
     * Instantiates a new navigation item.
     * 
     * @param stack
     *            the stack
     * @param req
     *            the req
     * @param res
     *            the res
     */
    public NavigationItem(ValueStack stack, HttpServletRequest req,
            HttpServletResponse res) {
        super(stack, req, res);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#getDefaultTemplate()
     */
    protected String getDefaultTemplate() {
        return TEMPLATE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#evaluateExtraParams()
     */
    public void evaluateExtraParams() {
        super.evaluateExtraParams();

        if (isAuthenticatedWithRoles(request, roles)) {
            String href = buildUrlForAction(action);
            addParameter("href", href);

            addParameter("authenticated", true);

            if (!StringUtil.isNullOrEmpty(id)) {
                addParameter("id", findString(id));
            }

            if (!StringUtil.isNullOrEmpty(title)) {
                addParameter("title", findString(title));
            }

            if (key != null) {
                addParameter("value", TextProviderHelper.getText(key, key,
                        stack));
            } else {
                addParameter("value", findString(value));
            }

            if (equalsAction(getCurrentAction())) {
                addParameter("cssClass", "Current");
            }
        }
    }

    /**
     * Equals action.
     * 
     * @param currentAction
     *            the current action
     * @return true, if successful
     */
    private boolean equalsAction(String currentAction) {

        String subAction = this.subAction;
        if (StringUtil.isNullOrEmpty(subAction)) {
            subAction =
                    new StringBuilder().append(findString(action)).toString();
        }

        // System.out.println("--" + currentAction);
        if (!StringUtil.isNullOrEmpty(currentAction)) {
            String[] sub = subAction.split(",");
            for (int i = 0; i < sub.length; i++) {
                // System.out.println(findString(sub[i]));
                // if (currentAction.startsWith(findString(sub[i])) ||
                // currentAction.endsWith(sub[i])) {
                // if (subEquals(sub[i], currentAction)) {
                if (currentAction.equals(findString(sub[i]))) {
                    return true;
                }
            }
        }
        // System.out.println("--");

        return false;
    }

    /*
     * private boolean subEquals(String s1, String s2) { if
     * (StringUtil.isNullOrEmpty(s1) && StringUtil.isNullOrEmpty(s2)) { return
     * true; } else if (StringUtil.isNullOrEmpty(s1) ||
     * StringUtil.isNullOrEmpty(s2)) { return false; } String[] a1 =
     * s1.split("/"); String[] a2 = s2.split("/"); for (int i = 0; i <
     * Math.min(a1.length, a2.length); i++) { if (!a1[i].equals(a2[i])) { return
     * false; } } return true; }
     */

    /**
     * Builds the url for action.
     * 
     * @param action
     *            the action
     * @return the string
     */
    private String buildUrlForAction(String action) {
        String namespace =
                determineNamespace(this.namespace, getStack(), request);
        return determineActionURL(action, namespace, null, request, response,
                null, request.getScheme(), true, true, false, true);
    }

    /**
     * Gets the current action.
     * 
     * @return the current action
     */
    private String getCurrentAction() {
        ActionInvocation ai =
                (ActionInvocation) getStack()
                        .getContext()
                        .get(
                                "com.opensymphony.xwork2.ActionContext.actionInvocation");
        if (ai != null) {
            return ai.getProxy().getActionName();
        } else if (ActionContext.getContext() != null) {
            return ActionContext.getContext().getName();
        } else {
            String uri = request.getRequestURI();
            return uri.substring(uri.lastIndexOf('/'));
        }
    }
    
    public boolean isAuthenticatedWithRoles(HttpServletRequest request,
            String rolesList) {
    	//TODO update later!
        if (StringUtil.isNullOrEmpty(rolesList)) {
            return true;
        }
        if (!isAuthenticated(request)) {
            return false;
        }
        return false;
    }
    
    public static boolean isAuthenticated(HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        return session != null && session.getAttribute(ConstantManager.USER_SESSION_KEY) != null;
    }
}

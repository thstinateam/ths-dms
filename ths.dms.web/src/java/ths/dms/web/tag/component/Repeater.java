/*
 * $Id: Repeater.java 103 2009-05-30 05:46:27Z dinhthaiha $
 
 *  
 *  $Author: dinhthaiha $
 *  $Rev: 103 $
 */
package ths.dms.web.tag.component;

import java.io.IOException;
import java.io.Writer;

import org.apache.log4j.Logger;
import org.apache.struts2.components.ContextBean;

import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.utils.LogUtility;

/**
 * The Class Repeater.
 */
public class Repeater extends ContextBean {

    /** The log. */
    protected Logger log = Logger.getLogger(Repeater.class);

    /** The status. */
    protected String status;

    /** The value. */
    protected String value;

    /** The none template. */
    protected String noneTemplate;

    /** The header template. */
    protected String headerTemplate;

    /** The footer template. */
    protected String footerTemplate;

    /** The separator template. */
    protected String separatorTemplate;

    /** The item template. */
    protected String itemTemplate;

    /** The alternating item template. */
    protected String alternatingItemTemplate;

    /** The separator. */
    protected String separator;

    /** The is alternate. */
    protected boolean isAlternate;

    /** The alternate. */
    protected boolean alternate;

    /** The from. */
    protected int from;

    /** The to. */
    protected int to;

    /** The paging. */
    protected Paging paging;

    /**
     * Gets the paginated.
     * 
     * @return the paginated
     */
    public Paging getPaginated() {
        return paging;
    }

    /**
     * Sets the paginated.
     * 
     * @param paging
     *            the new paginated
     */
    public void setPaginated(Paging paging) {
        this.paging = paging;
    }

    /**
     * Sets the from.
     * 
     * @param from
     *            the new from
     */
    public void setFrom(int from) {
        this.from = from;
    }

    /**
     * Gets the from.
     * 
     * @return the from
     */
    public int getFrom() {
        return from;
    }

    /**
     * Sets the to.
     * 
     * @param to
     *            the new to
     */
    public void setTo(int to) {
        this.to = to;
    }

    /**
     * Gets the to.
     * 
     * @return the to
     */
    public int getTo() {
        return to;
    }

    /**
     * Gets the none template.
     * 
     * @return the none template
     */
    public String getNoneTemplate() {
        return noneTemplate;
    }

    /**
     * Sets the none template.
     * 
     * @param noneTemplate
     *            the new none template
     */
    public void setNoneTemplate(String noneTemplate) {
        this.noneTemplate = noneTemplate;
    }

    /**
     * Gets the header template.
     * 
     * @return the header template
     */
    public String getHeaderTemplate() {
        return headerTemplate;
    }

    /**
     * Sets the header template.
     * 
     * @param headerTemplate
     *            the new header template
     */
    public void setHeaderTemplate(String headerTemplate) {
        this.headerTemplate = headerTemplate;
    }

    /**
     * Gets the footer template.
     * 
     * @return the footer template
     */
    public String getFooterTemplate() {
        return footerTemplate;
    }

    /**
     * Sets the footer template.
     * 
     * @param footerTemplate
     *            the new footer template
     */
    public void setFooterTemplate(String footerTemplate) {
        this.footerTemplate = footerTemplate;
    }

    /**
     * Gets the separator template.
     * 
     * @return the separator template
     */
    public String getSeparatorTemplate() {
        return separatorTemplate;
    }

    /**
     * Sets the separator template.
     * 
     * @param separatorTemplate
     *            the new separator template
     */
    public void setSeparatorTemplate(String separatorTemplate) {
        this.separatorTemplate = separatorTemplate;
    }

    /**
     * Gets the item template.
     * 
     * @return the item template
     */
    public String getItemTemplate() {
        return itemTemplate;
    }

    /**
     * Sets the item template.
     * 
     * @param itemTemplate
     *            the new item template
     */
    public void setItemTemplate(String itemTemplate) {
        this.itemTemplate = itemTemplate;
    }

    /**
     * Gets the alternating item template.
     * 
     * @return the alternating item template
     */
    public String getAlternatingItemTemplate() {
        return alternatingItemTemplate;
    }

    /**
     * Sets the alternating item template.
     * 
     * @param alternatingItemTemplate
     *            the new alternating item template
     */
    public void setAlternatingItemTemplate(String alternatingItemTemplate) {
        this.alternatingItemTemplate = alternatingItemTemplate;
    }

    /**
     * Gets the separator.
     * 
     * @return the separator
     */
    public String getSeparator() {
        return separator;
    }

    /**
     * Sets the separator.
     * 
     * @param separator
     *            the new separator
     */
    public void setSeparator(String separator) {
        this.separator = separator;
    }

    /**
     * Gets the status.
     * 
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the status.
     * 
     * @param status
     *            the new status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets the value.
     * 
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value.
     * 
     * @param value
     *            the new value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Sets the alternate.
     * 
     * @param alternate
     *            the new alternate
     */
    public void setAlternate(boolean alternate) {
        this.alternate = alternate;
    }

    /**
     * Gets the alternate.
     * 
     * @return the alternate
     */
    public boolean getAlternate() {
        return this.alternate;
    }

    /**
     * Mark alternate.
     */
    public void markAlternate() {
        isAlternate = true;
    }

    /**
     * Was alternate.
     * 
     * @return true, if successful
     */
    public boolean wasAlternate() {
        return isAlternate;
    }

    /**
     * Instantiates a new repeater.
     * 
     * @param stack
     *            the stack
     */
    public Repeater(ValueStack stack) {
        super(stack);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.Component#start(java.io.Writer)
     */
    public boolean start(Writer writer) {
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.Component#end(java.io.Writer,
     * java.lang.String)
     */
    public boolean end(Writer writer, String body) {

        try {
            StringBuilder builder = new StringBuilder();

            if (itemTemplate != null) {
                builder.append(itemTemplate);

                if (getHeaderTemplate() != null) {
                    builder.insert(0, headerTemplate);
                }

                if (footerTemplate != null) {
                    builder.append(footerTemplate);
                }

            } else if (itemTemplate == null && noneTemplate != null) {
                builder.append(noneTemplate);
            }

            if (builder != null && builder.toString() != null) {
                writer.write(builder.toString());
            }
        } catch (IOException e) {
            LogUtility.logError(e, e.getMessage());
        }

        return false;
    }
}

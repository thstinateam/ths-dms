package ths.dms.web.tag.component;

import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.UIBean;
import org.apache.struts2.views.annotations.StrutsTagAttribute;

import com.opensymphony.xwork2.util.ValueStack;

public class MPaging extends UIBean {
	public static final String TEMPLATE = "mpaging";

	protected String id;
	protected String theme = "simple";

	protected String source;
	protected String action;

	protected int currentPage;

	protected boolean showTotalPage = false;

	protected KPaging<Object> paginated;

	protected List<Integer> listItem;
	protected String listParam;

	protected boolean showPrevNext = true;
	protected boolean showFirstLast =true;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public void setShowTotalPage(boolean showTotalPage) {
		this.showTotalPage = showTotalPage;
	}

	@StrutsTagAttribute(description = "The URL.")
	public void setListParam(String listParam) {
		this.listParam = listParam;
	}

	public void setShowPrevNext(boolean showPrevNext) {
		this.showPrevNext = showPrevNext;
	}

	public void setShowFirstLast(boolean showFirstLast) {
		this.showFirstLast = showFirstLast;
	}

	public String getTheme() {
		if (theme == null) {
			return "simple";
		}
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public KPaging<Object> getPaginated() {
		return paginated;
	}

	public void setPaginated(KPaging<Object> paginated) {
		this.paginated = paginated;
	}

	public List<Integer> getListItem() {
		return listItem;
	}

	public void setListItem(List<Integer> listItem) {
		this.listItem = listItem;
	}

	public String getSource() {
		return source;
	}

	public boolean isShowTotalPage() {
		return showTotalPage;
	}

	public String getListParam() {
		return listParam;
	}

	public boolean isShowPrevNext() {
		return showPrevNext;
	}

	public boolean isShowFirstLast() {
		return showFirstLast;
	}

	public MPaging(ValueStack stack, HttpServletRequest request,
			HttpServletResponse response) {
		super(stack, request, response);
	}

	@Override
	protected String getDefaultTemplate() {
		return TEMPLATE;
	}

	@Override
	public void evaluateParams() {
		if(paginated != null) {
			listItem = new ArrayList<Integer>();
			buildId();
			buildAction();
			buildListParam();
			
			addParameter("id", id);
			addParameter("action", action);
			addParameter("listParam", listParam);
			
			int max = paginated.getMaxDisplayPage();			
			int prev = ((int)Math.floor(paginated.getPage()/max))*max - 1;
			boolean isPrevious = paginated.getPage() > 0 && prev > 0;
			
			boolean isNext = ((int)Math.floor(paginated.getPage()/max))*max + max < paginated.getTotalPages();
//			int upper = ((int)Math.floor(paginated.getPage()/max))*max + max - 1;
			
			if(isPrevious) {					
				addParameter("prev", prev);
			}
			if (isNext) {
				int next = prev + max + 1;
				addParameter("next", next);
			}			
			if (paginated.getTotalPages() >= 2) {
				addParameter("enabled", true);
			}
			if (paginated.getPages() != null && paginated.getPages().length > 0) {
				buildListItem(paginated.getPages(), paginated.getPage());
			} else {
				listItem = new ArrayList<Integer>();
			}
			addParameter("paginated", listItem);
			addParameter("currentPage", paginated.getPage());
			
			addParameter("showTotalPage", showTotalPage);
			if (showTotalPage) {
				addParameter("totalPages", paginated.getTotalPages() + " trang");
			}
			addParameter("showPrevNext", showPrevNext);
			addParameter("showFirstLast", showFirstLast);
			addParameter("viewCurrentPageInput", paginated.getPage() + 1);
			if (paginated.getPage() < (paginated.getTotalPages() - 2)) {
				addParameter("last", paginated.getTotalPages() - 1);
			}
			if (paginated.getPage() > 1) {
				addParameter("first", 0);
			}
			addParameter("totalPage", paginated.getTotalPages());			
		} else {
			return;
		}
	}

	private void buildId() {
		if (!isNullOrEmpty(id)) {
			id = ensureAttributeSafelyNotEscaped(findString(id));
		}
	}
	private void buildAction() {
		if (!isNullOrEmpty(action)) {
			action = ensureAttributeSafelyNotEscaped(findString(action));
		}
	}
	private void buildListParam() {
		if (!isNullOrEmpty(listParam)) {
			listParam = ensureAttributeSafelyNotEscaped(findString(listParam));
		}
	}
	
	private void buildListItem(Integer[] pages, int currentPage) {
		for (int i : pages) {
			listItem.add(i);
		}
	}

	@Override
	public boolean start(Writer writer) {
		paginated = convert(findValue(source));
		return super.start(writer);
	}

	@SuppressWarnings("unchecked")
	private KPaging<Object> convert(Object value) {
		return (KPaging<Object>) value;
	}
	
	private boolean isNullOrEmpty(String strSource) {
		if (strSource == null || strSource.trim() == "") {
			return true;
		}
		return false;
	}

}

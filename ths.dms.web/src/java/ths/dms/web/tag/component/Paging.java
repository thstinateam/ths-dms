/*
 * $Id: Paging.java 119 2009-06-09 16:25:24Z dinhthaiha $
 
 *  
 *  $Author: dinhthaiha $
 *  $Rev: 119 $
 */
package ths.dms.web.tag.component;

import java.io.Writer;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.components.UIBean;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.util.ValueStack;

import ths.dms.web.utils.StringUtil;
import ths.dms.web.utils.ValidateUtil;

/**
 * The Class Paging.
 */
public class Paging extends UIBean {

    /** The Constant TEMPLATE. */
    public static final String TEMPLATE = "paging";

    /** The current page. */
    protected int currentPage;

    /** The page size. */
    protected String pageSize;

    /** The total page. */
    protected String totalPage;

    /** The initital page. */
    protected String inititalPage;

    /** The source. */
    protected String source;

    /** The mode. */
    protected String mode;

    /** The prev. */
    protected String prev;

    /** The next. */
    protected String next;

    /** The list. */
    protected List<Object> list;

    /**
     * Sets the page size.
     * 
     * @param pageSize
     *            the new page size
     */
    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * Sets the initital page.
     * 
     * @param inititalPage
     *            the new initital page
     */
    public void setInititalPage(String inititalPage) {
        this.inititalPage = inititalPage;
    }

    /**
     * Sets the total page.
     * 
     * @param totalPage
     *            the new total page
     */
    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    /**
     * Sets the mode.
     * 
     * @param mode
     *            the new mode
     */
    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * Sets the source.
     * 
     * @param source
     *            the new source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * Sets the prev.
     * 
     * @param prev
     *            the new prev
     */
    public void setPrev(String prev) {
        this.prev = prev;
    }

    /**
     * Sets the next.
     * 
     * @param next
     *            the new next
     */
    public void setNext(String next) {
        this.next = next;
    }

    /**
     * Instantiates a new paging.
     * 
     * @param stack
     *            the stack
     * @param request
     *            the request
     * @param response
     *            the response
     */
    public Paging(ValueStack stack, HttpServletRequest request,
            HttpServletResponse response) {
        super(stack, request, response);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.Component#start(java.io.Writer)
     */
    @SuppressWarnings("unchecked")
    @Override
    public boolean start(Writer writer) {
        list = convert(findValue(source));
        return super.start(writer);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#getDefaultTemplate()
     */
    @Override
    protected String getDefaultTemplate() {
        return TEMPLATE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.apache.struts2.components.UIBean#evaluateParams()
     */
    @Override
    public void evaluateParams() {

        findCurrentPage();

        String prevUrl = buildUrl(currentPage - 1);
        String nextUrl = buildUrl(currentPage + 1);

        if (isPreviousPage()) {
            addParameter("prev", prevUrl);
        }
        if (isNextPage()) {
            addParameter("next", nextUrl);
        }

        if (isPreviousPage() || isNextPage()) {
            addParameter("enabled", true);
        }

        if (StringUtil.isNullOrEmpty(prev)) {
            if (mode.equals("NextPrev")) {
                prev = "« Previous";
            } else if (mode.equals("NewerOlder")) {
                prev = "Newer";
            } else if (mode.equals("MoreFewer")) {
                prev = "Fewer";
            }
        }

        if (StringUtil.isNullOrEmpty(next)) {
            if (mode.equals("NextPrev")) {
                next = "Next »";
            } else if (mode.equals("NewerOlder")) {
                next = "Older";
            } else if (mode.equals("MoreFewer")) {
                next = "More";
            }
        }

        addParameter("prevText", findString(prev));
        addParameter("nextText", findString(next));
    }

    /**
     * Find current page.
     */
    private void findCurrentPage() {
        String page = request.getParameter("page");
        if (!StringUtil.isNullOrEmpty(page) && ValidateUtil.validateNumber(page)) {
            currentPage = Integer.parseInt(page);
        } else {
            currentPage = (Integer) findValue(inititalPage);
        }
    }

    /**
     * Builds the url.
     * 
     * @param page
     *            the page
     * @return the string
     */
    private String buildUrl(int page) {
        addParameter("page", page);
        String namespace = determineNamespace(null, getStack(), request);
        return determineActionURL(ActionContext.getContext().getName(),
                namespace, null, request, response, parameters, request
                        .getScheme(), true, true, false, true);
    }

    /**
     * Checks if is next page.
     * 
     * @return true, if is next page
     */
    public boolean isNextPage() {
        int pageSize = (Integer) findValue(this.pageSize);
        return list.size() > pageSize;
    }

    /**
     * Checks if is previous page.
     * 
     * @return true, if is previous page
     */
    public boolean isPreviousPage() {
        return currentPage > 0;
    }

    /**
     * Convert.
     * 
     * @param value
     *            the value
     * @return the list
     */
    @SuppressWarnings("unchecked")
    public List convert(Object value) {
        if (value instanceof Map)
            value = ((Map) value).entrySet();
        if (value == null)
            return null;
        if (value.getClass().isArray()) {
            List list = new ArrayList(Array.getLength(value));
            for (int j = 0; j < Array.getLength(value); j++)
                list.add(Array.get(value, j));

            return list;
        } else if (value instanceof Enumeration) {
            Enumeration enumeration = (Enumeration) value;
            List list = new ArrayList();
            for (; enumeration.hasMoreElements(); list.add(enumeration
                    .nextElement()))
                ;
            return list;
        } else {
            return (List) value;
        }
    }
}

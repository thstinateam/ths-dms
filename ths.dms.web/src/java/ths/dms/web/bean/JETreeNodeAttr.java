/**
 * 
 */
package ths.dms.web.bean;

import ths.dms.core.entities.vo.ShopTreeVO;
import ths.dms.core.entities.vo.StaffGroupTreeVO;
import ths.dms.core.entities.vo.StaffTreeVO;

/**
 * @author hungtx
 *
 */
public class JETreeNodeAttr {
	
	/** The id. */
	private String id;
	
	private ShopTreeVO shop;
	private StaffTreeVO staff;
	private StaffGroupTreeVO group;
	private Integer hasGroup;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	public StaffTreeVO getStaff() {
		return staff;
	}
	public void setStaff(StaffTreeVO staff) {
		this.staff = staff;
	}
	
	public Integer getHasGroup() {
		return hasGroup;
	}
	public void setHasGroup(Integer hasGroup) {
		this.hasGroup = hasGroup;
	}
	public ShopTreeVO getShop() {
		return shop;
	}
	public void setShop(ShopTreeVO shop) {
		this.shop = shop;
	}
	public StaffGroupTreeVO getGroup() {
		return group;
	}
	public void setGroup(StaffGroupTreeVO group) {
		this.group = group;
	}

	
}

/**
 * 
 */
package ths.dms.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import ths.dms.web.utils.LogUtility;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ths.core.entities.vo.rpt.RptBCKD5;

/**
 * @author hungtx
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/spring/applicationContext.xml" })
public class BeanTest implements Serializable {	
	@SuppressWarnings("unchecked")
	   public static void main(String[] args) {
	      String sourceFileName = "C://Documents and Settings/lamnh/Desktop/jasper_report ppt/demo_kd17.jasper";
	      String printFileName = null;
	      List<RptBCKD5> lstDataKD5 = new ArrayList<RptBCKD5>();
	      lstDataKD5.add(0,new RptBCKD5());

	      JRBeanCollectionDataSource beanColDataSource =
	      new JRBeanCollectionDataSource(lstDataKD5);

	      Map parameters = new HashMap();
	      try {
	    	 printFileName = JasperFillManager.fillReportToFile(sourceFileName,parameters,beanColDataSource);
	         if(printFileName != null){
	           JasperPrintManager.printReport(
	               printFileName, true);
	      }
	      } catch (JRException e) {
	         LogUtility.logError(e, e.getMessage());
	      }
	   }
}

/**
 * 
 */
package ths.dms.web.bean;

import java.util.List;

import ths.dms.core.entities.vo.ToolVO;

/**
 * @author hungtx
 *
 */
public class JETreeGridDeviceNode {
	
	private Long id;
	/** The data. */
	private String text;
	
	/** The attr. */
	private ToolVO attr;
	
	/** The children. */
	private List<JETreeGridDeviceNode> children;
	
	/** The state. */
	private String state;
	
	/** The icon. */
	private String iconCls;
	
	public JETreeGridDeviceNode(){
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}


	

	public ToolVO getAttr() {
		return attr;
	}

	public void setAttr(ToolVO attr) {
		this.attr = attr;
	}

	public List<JETreeGridDeviceNode> getChildren() {
		return children;
	}

	public void setChildren(List<JETreeGridDeviceNode> children) {
		this.children = children;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	
}

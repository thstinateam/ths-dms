/**
 * 
 */
package ths.dms.web.bean;

import ths.dms.helper.AppSetting;
import ths.dms.helper.Configuration;

/**
 * @author hungtx
 *
 */
public class ConfigBean {
	
	/** The service url. */
	private String serviceUrl;

	/**
	 * @return the serviceUrl
	 */
	public String getServiceUrl() {
		return  serviceUrl;
	}

	/**
	 * @param serviceUrl the serviceUrl to set
	 */
	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}
	
	public String getFullUrl(String url){
//		 AppSetting.getStringValue("xml-real-path")
		return Configuration.getCoreServerPath() + url;
	}
	
}

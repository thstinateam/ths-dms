package ths.dms.web.bean;

import java.io.Serializable;

public class LotBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4650407369342567804L;
	private String lotNumber;
	private int lotTotal;
	public String getLotNumber() {
		return lotNumber;
	}
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	public int getLotTotal() {
		return lotTotal;
	}
	public void setLotTotal(int lotTotal) {
		this.lotTotal = lotTotal;
	}
}

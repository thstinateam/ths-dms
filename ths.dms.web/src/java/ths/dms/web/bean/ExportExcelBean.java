/**
 * 
 */
package ths.dms.web.bean;

/**
 * @author hungtx
 *
 */
public class ExportExcelBean<T> {
	
	/** The row data. */
	private T rowData;
	
	/** The reason fail. */
	private String reasonFail;

	/**
	 * @return the rowData
	 */
	public T getRowData() {
		return rowData;
	}

	/**
	 * @param rowData the rowData to set
	 */
	public void setRowData(T rowData) {
		this.rowData = rowData;
	}

	/**
	 * @return the reasonFail
	 */
	public String getReasonFail() {
		return reasonFail;
	}

	/**
	 * @param reasonFail the reasonFail to set
	 */
	public void setReasonFail(String reasonFail) {
		this.reasonFail = reasonFail;
	}

}

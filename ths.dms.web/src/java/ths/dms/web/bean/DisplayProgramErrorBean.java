/**
 * 
 */
package ths.dms.web.bean;

/**
 * @author hungtx
 *
 */
public class DisplayProgramErrorBean{	
	
	/** The display program code. */
	private String displayProgramCode;
	
	/** The display program name. */
	private String displayProgramName;
	
	/** The relation. */
	private String relation;
	
	/** The from date. */
	private String fromDate;
	
	/** The to date. */
	private String toDate;
	
	/** The quota group code. */
	private String quotaGroupCode;
	
	/** The quota group name. */
	private String quotaGroupName;
	
	/** The percent money. */
	private String percentMoney;
	
	/** The list code. */
	private String listCode;
	
	/** The level code. */
	private String levelCode;
	
	/** The num level. */
	private String numLevel;
	
	/** The product code. */
	private String productCode;
	
	/** The staff code. */
	private String staffCode;
	
	/** The customer code. */
	private String customerCode;
	
	/** The err msg. */
	private String errMsg;

	/**
	 * @return the displayProgramCode
	 */
	public String getDisplayProgramCode() {
		return displayProgramCode;
	}

	/**
	 * @param displayProgramCode the displayProgramCode to set
	 */
	public void setDisplayProgramCode(String displayProgramCode) {
		this.displayProgramCode = displayProgramCode;
	}

	/**
	 * @return the displayProgramName
	 */
	public String getDisplayProgramName() {
		return displayProgramName;
	}

	/**
	 * @param displayProgramName the displayProgramName to set
	 */
	public void setDisplayProgramName(String displayProgramName) {
		this.displayProgramName = displayProgramName;
	}

	/**
	 * @return the relation
	 */
	public String getRelation() {
		return relation;
	}

	/**
	 * @param relation the relation to set
	 */
	public void setRelation(String relation) {
		this.relation = relation;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the quotaGroupCode
	 */
	public String getQuotaGroupCode() {
		return quotaGroupCode;
	}

	/**
	 * @param quotaGroupCode the quotaGroupCode to set
	 */
	public void setQuotaGroupCode(String quotaGroupCode) {
		this.quotaGroupCode = quotaGroupCode;
	}

	/**
	 * @return the quotaGroupName
	 */
	public String getQuotaGroupName() {
		return quotaGroupName;
	}

	/**
	 * @param quotaGroupName the quotaGroupName to set
	 */
	public void setQuotaGroupName(String quotaGroupName) {
		this.quotaGroupName = quotaGroupName;
	}

	/**
	 * @return the percentMoney
	 */
	public String getPercentMoney() {
		return percentMoney;
	}

	/**
	 * @param percentMoney the percentMoney to set
	 */
	public void setPercentMoney(String percentMoney) {
		this.percentMoney = percentMoney;
	}

	/**
	 * @return the listCode
	 */
	public String getListCode() {
		return listCode;
	}

	/**
	 * @param listCode the listCode to set
	 */
	public void setListCode(String listCode) {
		this.listCode = listCode;
	}

	/**
	 * @return the levelCode
	 */
	public String getLevelCode() {
		return levelCode;
	}

	/**
	 * @param levelCode the levelCode to set
	 */
	public void setLevelCode(String levelCode) {
		this.levelCode = levelCode;
	}

	/**
	 * @return the numLevel
	 */
	public String getNumLevel() {
		return numLevel;
	}

	/**
	 * @param numLevel the numLevel to set
	 */
	public void setNumLevel(String numLevel) {
		this.numLevel = numLevel;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return productCode;
	}

	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/**
	 * @return the errMsg
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/**
	 * @param errMsg the errMsg to set
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	/**
	 * @return the staffCode
	 */
	public String getStaffCode() {
		return staffCode;
	}

	/**
	 * @param staffCode the staffCode to set
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	/**
	 * @return the customerCode
	 */
	public String getCustomerCode() {
		return customerCode;
	}

	/**
	 * @param customerCode the customerCode to set
	 */
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	
}

/**
 * @(#)StatusBean.java   Jul 18, 2012
 * 
 * Copyright (c) 2012 Viettel telecom
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Viettel
 * Telecom. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Viettel.
 */
package ths.dms.web.bean;

/**
 * 
 * @author dandt
 *
 */
public class GenderBean {
    
    /** The value. */
    private String value;
    
    /** The name. */
    private String name;


    public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
     * getName
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * setName
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Instantiates a new status bean.
     */
    public GenderBean(){
    	
    }
    
    /**
     * Instantiates a new status bean.
     *
     * @param vl the vl
     * @param nm the nm
     */
    public GenderBean(String vl, String nm){
	value = vl;
	name = nm;
    }
}

package ths.dms.web.bean;

import java.util.List;


/**
 * Node cua tree-grid
 * 
 * @author lacnv1
 * @since Aug 14, 2014
 */
public class TreeGridNode<T> {

	private String nodeId;
	private String parentId;
	private String text;
	private T attr;
	private List<TreeGridNode<T>> children;
	private String state;
	
	public String getNodeId() {
		return nodeId;
	}
	
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	
	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public T getAttr() {
		return attr;
	}

	public void setAttr(T attr) {
		this.attr = attr;
	}

	public List<TreeGridNode<T>> getChildren() {
		return children;
	}

	public void setChildren(List<TreeGridNode<T>> children) {
		this.children = children;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
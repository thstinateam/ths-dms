/**
 * 
 */
package ths.dms.web.bean;

import java.util.List;

/**
 * @author hungtx
 *
 */
public class CellBean {
	
	/** The content1. */
	private String content1;
	
	/** The content2. */
	private String content2;
	
	/** The content3. */
	private String content3;
	
	/** The content4. */
	private String content4;
	
	/** The content5. */
	private String content5;
	
	/** The content6. */
	private String content6;
	
	/** The content7. */
	private String content7;
	
	/** The content8. */
	private String content8;
	
	/** The content9. */
	private String content9;
	
	/** The content10. */
	private String content10;
	
	/** The content11. */
	private String content11;
	
	/** The content12. */
	private String content12;
	
	/** The content13. */
	private String content13;
	
	/** The content14. */
	private String content14;
	
	/** The content15. */
	private String content15;
	
	/** The content16. */
	private String content16;
	
	/** The content17. */
	private String content17;
	
	/** The content18. */
	private String content18;
	
	/** The content19. */
	private String content19;
	
	/** The content20. */
	private String content20;
	
	/** The content21. */
	private String content21;
	
	/** The content22. */
	private String content22;
	
	/** The content23. */
	private String content23;
	
	/** The content24. */
	private String content24;
	
	/** The content25. */
	private String content25;
	
	/** The content26. */
	private String content26;
	
	/** The content27. */
	private String content27;
	
	/** The content28. */
	private String content28;	
	
	/** The content29. */
	private String content29;
	
	/** The content30. */
	private String content30;
	
	/** The content31. */
	private String content31;
	
	/** The content32. */
	private String content32;
	
	/** The content33. */
	private String content33;
	
	/** The content34. */
	private String content34;
	
	/** The content35. */
	private String content35;
	
	/** The content35. */
	private String content36;
	
	
	/** The content30. */
	private String content37;
	
	/** The content31. */
	private String content38;
	
	/** The content32. */
	private String content39;
	
	/** The content33. */
	private String content40;
	
	/** The content34. */
	private String content41;
	
	/** The content35. */
	private String content42;
	
	/** The content35. */
	private String content43;
	
	/** The content34. */
	private String content44;
	
	/** The content35. */
	private String content45;
	
	/** The content35. */
	private String content46;
	
	/** The content35. */
	private String content47;
	
	/** The content34. */
	private String content48;
	
	/** The content35. */
	private String content49;
	
	/** The content35. */
	private String content50;
	
	private List<String> lstDynamic;
	
	/** The err msg. */
	private String errMsg;
	
	/** The type row. */
	private int typeRow;

	/**
	 * @return the typeRow
	 */
	public int getTypeRow() {
		return typeRow;
	}

	/**
	 * @param typeRow the typeRow to set
	 */
	public void setTypeRow(int typeRow) {
		this.typeRow = typeRow;
	}

	/**
	 * @return the content1
	 */
	public String getContent1() {
		return content1;
	}

	/**
	 * @param content1 the content1 to set
	 */
	public void setContent1(String content1) {
		this.content1 = content1;
	}

	/**
	 * @return the content2
	 */
	public String getContent2() {
		return content2;
	}

	/**
	 * @param content2 the content2 to set
	 */
	public void setContent2(String content2) {
		this.content2 = content2;
	}

	/**
	 * @return the content3
	 */
	public String getContent3() {
		return content3;
	}

	/**
	 * @param content3 the content3 to set
	 */
	public void setContent3(String content3) {
		this.content3 = content3;
	}

	/**
	 * @return the content4
	 */
	public String getContent4() {
		return content4;
	}

	/**
	 * @param content4 the content4 to set
	 */
	public void setContent4(String content4) {
		this.content4 = content4;
	}

	/**
	 * @return the content5
	 */
	public String getContent5() {
		return content5;
	}

	/**
	 * @param content5 the content5 to set
	 */
	public void setContent5(String content5) {
		this.content5 = content5;
	}

	/**
	 * @return the content6
	 */
	public String getContent6() {
		return content6;
	}

	/**
	 * @param content6 the content6 to set
	 */
	public void setContent6(String content6) {
		this.content6 = content6;
	}

	/**
	 * @return the content7
	 */
	public String getContent7() {
		return content7;
	}

	/**
	 * @param content7 the content7 to set
	 */
	public void setContent7(String content7) {
		this.content7 = content7;
	}

	/**
	 * @return the content8
	 */
	public String getContent8() {
		return content8;
	}

	/**
	 * @param content8 the content8 to set
	 */
	public void setContent8(String content8) {
		this.content8 = content8;
	}

	/**
	 * @return the content9
	 */
	public String getContent9() {
		return content9;
	}

	/**
	 * @param content9 the content9 to set
	 */
	public void setContent9(String content9) {
		this.content9 = content9;
	}

	/**
	 * @return the content10
	 */
	public String getContent10() {
		return content10;
	}

	/**
	 * @param content10 the content10 to set
	 */
	public void setContent10(String content10) {
		this.content10 = content10;
	}

	/**
	 * @return the content11
	 */
	public String getContent11() {
		return content11;
	}

	/**
	 * @param content11 the content11 to set
	 */
	public void setContent11(String content11) {
		this.content11 = content11;
	}

	/**
	 * @return the content12
	 */
	public String getContent12() {
		return content12;
	}

	/**
	 * @param content12 the content12 to set
	 */
	public void setContent12(String content12) {
		this.content12 = content12;
	}

	/**
	 * @return the content13
	 */
	public String getContent13() {
		return content13;
	}

	/**
	 * @param content13 the content13 to set
	 */
	public void setContent13(String content13) {
		this.content13 = content13;
	}

	/**
	 * @return the content14
	 */
	public String getContent14() {
		return content14;
	}

	/**
	 * @param content14 the content14 to set
	 */
	public void setContent14(String content14) {
		this.content14 = content14;
	}

	/**
	 * @return the content15
	 */
	public String getContent15() {
		return content15;
	}

	/**
	 * @param content15 the content15 to set
	 */
	public void setContent15(String content15) {
		this.content15 = content15;
	}

	/**
	 * @return the content16
	 */
	public String getContent16() {
		return content16;
	}

	/**
	 * @param content16 the content16 to set
	 */
	public void setContent16(String content16) {
		this.content16 = content16;
	}

	/**
	 * @return the content17
	 */
	public String getContent17() {
		return content17;
	}

	/**
	 * @param content17 the content17 to set
	 */
	public void setContent17(String content17) {
		this.content17 = content17;
	}

	/**
	 * @return the content18
	 */
	public String getContent18() {
		return content18;
	}

	/**
	 * @param content18 the content18 to set
	 */
	public void setContent18(String content18) {
		this.content18 = content18;
	}

	/**
	 * @return the content19
	 */
	public String getContent19() {
		return content19;
	}

	/**
	 * @param content19 the content19 to set
	 */
	public void setContent19(String content19) {
		this.content19 = content19;
	}

	/**
	 * @return the content20
	 */
	public String getContent20() {
		return content20;
	}

	/**
	 * @param content20 the content20 to set
	 */
	public void setContent20(String content20) {
		this.content20 = content20;
	}

	/**
	 * @return the content21
	 */
	public String getContent21() {
		return content21;
	}

	/**
	 * @param content21 the content21 to set
	 */
	public void setContent21(String content21) {
		this.content21 = content21;
	}

	/**
	 * @return the content22
	 */
	public String getContent22() {
		return content22;
	}

	/**
	 * @param content22 the content22 to set
	 */
	public void setContent22(String content22) {
		this.content22 = content22;
	}

	/**
	 * @return the content23
	 */
	public String getContent23() {
		return content23;
	}

	/**
	 * @param content23 the content23 to set
	 */
	public void setContent23(String content23) {
		this.content23 = content23;
	}

	/**
	 * @return the content24
	 */
	public String getContent24() {
		return content24;
	}

	/**
	 * @param content24 the content24 to set
	 */
	public void setContent24(String content24) {
		this.content24 = content24;
	}

	/**
	 * @return the content25
	 */
	public String getContent25() {
		return content25;
	}

	/**
	 * @param content25 the content25 to set
	 */
	public void setContent25(String content25) {
		this.content25 = content25;
	}

	/**
	 * @return the content26
	 */
	public String getContent26() {
		return content26;
	}

	/**
	 * @param content26 the content26 to set
	 */
	public void setContent26(String content26) {
		this.content26 = content26;
	}

	/**
	 * @return the content27
	 */
	public String getContent27() {
		return content27;
	}

	/**
	 * @param content27 the content27 to set
	 */
	public void setContent27(String content27) {
		this.content27 = content27;
	}

	/**
	 * @return the content28
	 */
	public String getContent28() {
		return content28;
	}

	/**
	 * @param content28 the content28 to set
	 */
	public void setContent28(String content28) {
		this.content28 = content28;
	}

	/**
	 * @return the content29
	 */
	public String getContent29() {
		return content29;
	}

	/**
	 * @param content29 the content29 to set
	 */
	public void setContent29(String content29) {
		this.content29 = content29;
	}

	/**
	 * @return the content30
	 */
	public String getContent30() {
		return content30;
	}

	/**
	 * @param content30 the content30 to set
	 */
	public void setContent30(String content30) {
		this.content30 = content30;
	}

	/**
	 * @return the errMsg
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/**
	 * @param errMsg the errMsg to set
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public String getContent31() {
		return content31;
	}

	public void setContent31(String content31) {
		this.content31 = content31;
	}

	public String getContent32() {
		return content32;
	}

	public void setContent32(String content32) {
		this.content32 = content32;
	}

	public String getContent33() {
		return content33;
	}

	public void setContent33(String content33) {
		this.content33 = content33;
	}

	public String getContent34() {
		return content34;
	}

	public void setContent34(String content34) {
		this.content34 = content34;
	}

	public String getContent35() {
		return content35;
	}

	public void setContent35(String content35) {
		this.content35 = content35;
	}

	public List<String> getLstDynamic() {
		return lstDynamic;
	}

	public void setLstDynamic(List<String> lstDynamic) {
		this.lstDynamic = lstDynamic;
	}

	public String getContent36() {
		return content36;
	}

	public void setContent36(String content36) {
		this.content36 = content36;
	}

	public String getContent37() {
		return content37;
	}

	public void setContent37(String content37) {
		this.content37 = content37;
	}

	public String getContent38() {
		return content38;
	}

	public void setContent38(String content38) {
		this.content38 = content38;
	}

	public String getContent39() {
		return content39;
	}

	public void setContent39(String content39) {
		this.content39 = content39;
	}

	public String getContent40() {
		return content40;
	}

	public void setContent40(String content40) {
		this.content40 = content40;
	}

	public String getContent41() {
		return content41;
	}

	public void setContent41(String content41) {
		this.content41 = content41;
	}

	public String getContent42() {
		return content42;
	}

	public void setContent42(String content42) {
		this.content42 = content42;
	}

	public String getContent43() {
		return content43;
	}

	public void setContent43(String content43) {
		this.content43 = content43;
	}

	public String getContent44() {
		return content44;
	}

	public void setContent44(String content44) {
		this.content44 = content44;
	}

	public String getContent45() {
		return content45;
	}

	public void setContent45(String content45) {
		this.content45 = content45;
	}

	public String getContent46() {
		return content46;
	}

	public void setContent46(String content46) {
		this.content46 = content46;
	}

	public String getContent47() {
		return content47;
	}

	public void setContent47(String content47) {
		this.content47 = content47;
	}

	public String getContent48() {
		return content48;
	}

	public void setContent48(String content48) {
		this.content48 = content48;
	}

	public String getContent49() {
		return content49;
	}

	public void setContent49(String content49) {
		this.content49 = content49;
	}

	public String getContent50() {
		return content50;
	}

	public void setContent50(String content50) {
		this.content50 = content50;
	}
	
}

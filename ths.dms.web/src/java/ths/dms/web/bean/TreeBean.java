/**
 * 
 */
package ths.dms.web.bean;

import java.util.List;

import ths.dms.web.utils.StringUtil;

// TODO: Auto-generated Javadoc
/**
 * The Class ComboTreeBean.
 *
 * @author hungtx
 */
public class TreeBean {
	
	/** The id. */
	private String id;
	
	/** The text. */
	private String text;
	
	/** The icon cls. */
	private String iconCls;
	
	/** The checked. */
	private boolean checked;
	
	/** The state. */
	private String state = "open";
	
	/** The children. */
	private List<TreeBean> children;
	
	/** The attributes. */
	private TreeBeanAttr attributes;
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText() {
		return StringUtil.convertStringToHTMLCode(text);
	}

	/**
	 * Sets the text.
	 *
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the icon cls.
	 *
	 * @return the iconCls
	 */
	public String getIconCls() {
		return iconCls;
	}

	/**
	 * Sets the icon cls.
	 *
	 * @param iconCls the iconCls to set
	 */
	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	/**
	 * Checks if is checked.
	 *
	 * @return the checked
	 */
	public boolean isChecked() {
		return checked;
	}

	/**
	 * Sets the checked.
	 *
	 * @param checked the checked to set
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state.
	 *
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the children.
	 *
	 * @return the children
	 */
	public List<TreeBean> getChildren() {
		return children;
	}

	/**
	 * Sets the children.
	 *
	 * @param children the children to set
	 */
	public void setChildren(List<TreeBean> children) {
		this.children = children;
	}

	/**
	 * Gets the attributes.
	 *
	 * @return the attributes
	 */
	public TreeBeanAttr getAttributes() {
		return attributes;
	}

	/**
	 * Sets the attributes.
	 *
	 * @param attributes the new attributes
	 */
	public void setAttributes(TreeBeanAttr attributes) {
		this.attributes = attributes;
	}

	
}

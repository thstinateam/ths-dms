/**
 * 
 */
package ths.dms.web.bean;

import java.util.List;

/**
 * @author hungtx
 *
 */
public class JETreeNode {
	
	private String id;
	/** The data. */
	private String text;
	
	/** The attr. */
	private JETreeNodeAttr attributes;
	
	/** The children. */
	private List<JETreeNode> children;
	
	/** The state. */
	private String state;
	
	/** The icon. */
	private String iconCls;
	
	/** Ismanage */
	private Integer isManage;
	
	public JETreeNode(){
		
	}
	
	public JETreeNode(String text,String id){
		this.text = text;
		JETreeNodeAttr nodeAttr = new JETreeNodeAttr();
		nodeAttr.setId(id);
		this.attributes = nodeAttr;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public JETreeNodeAttr getAttributes() {
		return attributes;
	}

	public void setAttributes(JETreeNodeAttr attributes) {
		this.attributes = attributes;
	}

	public List<JETreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<JETreeNode> children) {
		this.children = children;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getIsManage() {
		return isManage;
	}

	public void setIsManage(Integer isManage) {
		this.isManage = isManage;
	}

	
	
}

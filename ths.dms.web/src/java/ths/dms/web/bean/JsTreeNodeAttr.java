/**
 * 
 */
package ths.dms.web.bean;

/**
 * @author hunglm16
 *
 */
public class JsTreeNodeAttr {
	
	/** The id. */
	private String id;
	
	/** The class style. */
	private String classStyle;
	
	private String url;
	
	/** The content item id. */
	private String contentItemId;
	
	/** The rel. */
	private String rel;
	
	/** The shop type : VNM, MIEN, VUNG, NPP. */
	private String shopType;
	
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the classStyle
	 */
	public String getClassStyle() {
		return classStyle;
	}

	/**
	 * @param classStyle the classStyle to set
	 */
	public void setClassStyle(String classStyle) {
		this.classStyle = classStyle;
	}

	/**
	 * @return the contentItemId
	 */
	public String getContentItemId() {
		return contentItemId;
	}

	/**
	 * @param contentItemId the contentItemId to set
	 */
	public void setContentItemId(String contentItemId) {
		this.contentItemId = contentItemId;
	}

	/**
	 * @return the rel
	 */
	public String getRel() {
		return rel;
	}

	/**
	 * @param rel the rel to set
	 */
	public void setRel(String rel) {
		this.rel = rel;
	}

	public String getShopType() {
		return shopType;
	}

	public void setShopType(String shopType) {
		this.shopType = shopType;
	}

}

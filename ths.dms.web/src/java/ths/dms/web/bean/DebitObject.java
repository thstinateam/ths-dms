package ths.dms.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class DebitObject implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer debitType;
	private String dbNumber;
	private BigDecimal amount;
	private String reason;
	private String shortCode;
	
	public Integer getDebitType() {
		return debitType;
	}
	
	public void setDebitType(Integer debitType) {
		this.debitType = debitType;
	}

	public String getDbNumber() {
		return dbNumber;
	}

	public void setDbNumber(String dbNumber) {
		this.dbNumber = dbNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getShortCode() {
		return shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
}
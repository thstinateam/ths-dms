/**
 * 
 */
package ths.dms.web.bean;

import java.util.List;

import ths.dms.core.entities.vo.StaffPositionVO;
import ths.dms.core.entities.vo.StaffVO;

/**
 * @author hungtx
 *
 */
public class JETreeGridNode {
	
	private Long id;
	/** The data. */
	private String text;
	
	/** The attr. */
	private StaffPositionVO attr;
	
	private List<StaffVO> lstAllStaff;
	
	/** The children. */
	private List<JETreeGridNode> children;
	
	/** The state. */
	private String state;
	
	private Integer nodeType;//1: SHOP, 2: STAFF
	
	private boolean root;
	
	private Integer checked;
	private Long parentID;
	private Integer status;
	
	/** The icon. */
	private String iconCls;
	
	public JETreeGridNode(){
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public StaffPositionVO getAttr() {
		return attr;
	}

	public void setAttr(StaffPositionVO attr) {
		this.attr = attr;
	}

	public List<JETreeGridNode> getChildren() {
		return children;
	}

	public void setChildren(List<JETreeGridNode> children) {
		this.children = children;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public Integer getNodeType() {
		return nodeType;
	}

	public void setNodeType(Integer nodeType) {
		this.nodeType = nodeType;
	}

	public boolean isRoot() {
		return root;
	}

	public void setRoot(boolean root) {
		this.root = root;
	}

	public List<StaffVO> getLstAllStaff() {
		return lstAllStaff;
	}

	public void setLstAllStaff(List<StaffVO> lstAllStaff) {
		this.lstAllStaff = lstAllStaff;
	}

	public Integer getChecked() {
		return checked;
	}

	public void setChecked(Integer checked) {
		this.checked = checked;
	}

	public Long getParentID() {
		return parentID;
	}

	public void setParentID(Long parentID) {
		this.parentID = parentID;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}


	
}

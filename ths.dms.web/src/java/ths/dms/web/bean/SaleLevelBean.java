package ths.dms.web.bean;

public class SaleLevelBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4650407369342567804L;
	private String saleLevel;

	public String getSaleLevel() {
		return saleLevel;
	}

	public void setSaleLevel(String saleLevel) {
		this.saleLevel = saleLevel;
	}
}

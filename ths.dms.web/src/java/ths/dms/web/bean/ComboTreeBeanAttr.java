/**
 * 
 */
package ths.dms.web.bean;

import ths.dms.core.entities.Area;
import ths.dms.core.entities.Shop;
import ths.dms.core.entities.vo.ProductTreeVO;

/**
 * @author thongnm
 *
 */
public class ComboTreeBeanAttr {
	
	/** The id. */
	
	private Shop shop;
	private Area area;
	private ProductTreeVO productTreeVO;

	public Shop getShop() {
		return shop;
	}
	public void setShop(Shop shop) {
		this.shop = shop;
	}
	public Area getArea() {
		return area;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public ProductTreeVO getProductTreeVO() {
		return productTreeVO;
	}
	public void setProductTreeVO(ProductTreeVO productTreeVO) {
		this.productTreeVO = productTreeVO;
	}
	
	
}

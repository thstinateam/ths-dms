/**
 * 
 */
package ths.dms.web.bean;

import java.util.List;

/**
 * @author hunglm16
 *
 */
public class JsTreeNode {
	
	/** The data. */
	private String data;
	
	/** The attr. */
	private JsTreeNodeAttr attr;
	
	/** The children. */
	private List<JsTreeNode> children;
	
	/** The state. */
	private String state;
	
	/** The icon. */
	private String icon;
	
	public JsTreeNode(){
		
	}
	/** Dung cho cay bao cao @author hunglm16 @since October 1,2014 */
	public JsTreeNode(String data, String id, String name, String url){
		this.data = data;
		JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
		nodeAttr.setId(id);
		nodeAttr.setName(name);
		nodeAttr.setUrl(url);
		this.attr = nodeAttr;
	}
	
	public JsTreeNode(String data, String id){
		this.data = data;
		JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
		nodeAttr.setId(id);
		this.attr = nodeAttr;
	}
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @return the attr
	 */
	public JsTreeNodeAttr getAttr() {
		return attr;
	}

	/**
	 * @param attr the attr to set
	 */
	public void setAttr(JsTreeNodeAttr attr) {
		this.attr = attr;
	}

	/**
	 * @return the children
	 */
	public List<JsTreeNode> getChildren() {
		return children;
	}

	/**
	 * @param children the children to set
	 */
	public void setChildren(List<JsTreeNode> children) {
		this.children = children;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @param icon the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
}

/**
 * @(#)JqTreeBean.java   Jul 18, 2012
 * 
 * Copyright (c) 2012 Viettel telecom
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Viettel
 * Telecom. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Viettel.
 */
package ths.dms.web.bean;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * JqTreeBean
 * @author hungtx
 *
 */
@XmlRootElement
@XmlSeeAlso({JqTreeBean.class})
public class JqTreeBean {		
	
	/** The id. */
	private Integer id;
	
	/** The label. */
	private String label;
	
	/** The children. */
	private List<JqTreeBean> children;

	/**
	 * @return the id
	 */
	public Integer getId() {
	    return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
	    this.id = id;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
	    return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
	    this.label = label;
	}

	/**
	 * @return the children
	 */
	public List<JqTreeBean> getChildren() {
	    return children;
	}

	/**
	 * @param children the children to set
	 */
	public void setChildren(List<JqTreeBean> children) {
	    this.children = children;
	}	
}

/**
 * 
 */
package ths.dms.web.bean;

import java.util.List;

import ths.dms.core.entities.vo.CarVO;
import ths.dms.core.entities.vo.ToolVO;

/**
 * @author hungtx
 *
 */
public class JETreeGridLogisticsNode {
	
	private Long id;
	/** The data. */
	private String text;
	
	/** The attr. */
	private CarVO attr;
	
	/** The children. */
	private List<JETreeGridLogisticsNode> children;
	
	/** The state. */
	private String state;
	
	/** The icon. */
	private String iconCls;
	
	public JETreeGridLogisticsNode(){
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}
	
	public CarVO getAttr() {
		return attr;
	}

	public void setAttr(CarVO attr) {
		this.attr = attr;
	}

	public List<JETreeGridLogisticsNode> getChildren() {
		return children;
	}

	public void setChildren(List<JETreeGridLogisticsNode> children) {
		this.children = children;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	
}

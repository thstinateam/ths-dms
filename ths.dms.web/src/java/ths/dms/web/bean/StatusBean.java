/**
 * @(#)StatusBean.java   Jul 18, 2012
 * 
 * Copyright (c) 2012 Viettel telecom
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Viettel
 * Telecom. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Viettel.
 */
package ths.dms.web.bean;

/**
 * 
 * @author hungtx
 *
 */
public class StatusBean {
    
    /** The value. */
    private Long value;
    
    /** The name. */
    private String name;


    public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}

	/**
     * getName
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * setName
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Instantiates a new status bean.
     */
    public StatusBean(){
    	
    }
    
    /**
     * Instantiates a new status bean.
     *
     * @param vl the vl
     * @param nm the nm
     */
    public StatusBean(Long vl, String nm){
	value = vl;
	name = nm;
    }
}

package ths.dms.web.bean;


/**
 * The Class SubCategoryBean.
 * @author lamnh
 * @since Oct 1,2012
 * 
 */
public class CategoryBean {
	private String code;
	private String name;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public CategoryBean(){
	    	
	}
	public CategoryBean(String a,String b){
		code = a;
		name = b;
	}
}

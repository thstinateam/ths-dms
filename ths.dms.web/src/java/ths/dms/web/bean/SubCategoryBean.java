package ths.dms.web.bean;

import java.util.List;

import ths.dms.core.entities.ProductInfo;

/**
 * The Class SubCategoryBean.
 * @author lamnh
 * @since Oct 1,2012
 * 
 */
public class SubCategoryBean {
	private String apParamCode;
	private String apParamName;
	private List<ProductInfo> listSubCat;
	public String getApParamCode() {
		return apParamCode;
	}
	public void setApParamCode(String apParamCode) {
		this.apParamCode = apParamCode;
	}
	public String getApParamName() {
		return apParamName;
	}
	public void setApParamName(String apParamName) {
		this.apParamName = apParamName;
	}
	public List<ProductInfo> getListSubCat() {
		return listSubCat;
	}
	public void setListSubCat(List<ProductInfo> listSubCat) {
		this.listSubCat = listSubCat;
	}
}

/**
 * 
 */
package ths.dms.web.bean;

/**
 * @author hungtx
 *
 */
public class ProductLevelErrorBean{
	
	/** The product level code. */
	private String productLevelCode;
	
	/** The category code. */
	private String categoryCode;
	
	/** The sub category code. */
	private String subCategoryCode;
	
	/** The brand code. */
	private String brandCode;
	
	/** The description. */
	private String description;
	
	/** The err msg. */
	private String errMsg;

	/**
	 * @return the productLevelCode
	 */
	public String getProductLevelCode() {
		return productLevelCode;
	}

	/**
	 * @param productLevelCode the productLevelCode to set
	 */
	public void setProductLevelCode(String productLevelCode) {
		this.productLevelCode = productLevelCode;
	}

	/**
	 * @return the categoryCode
	 */
	public String getCategoryCode() {
		return categoryCode;
	}

	/**
	 * @param categoryCode the categoryCode to set
	 */
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	/**
	 * @return the subCategoryCode
	 */
	public String getSubCategoryCode() {
		return subCategoryCode;
	}

	/**
	 * @param subCategoryCode the subCategoryCode to set
	 */
	public void setSubCategoryCode(String subCategoryCode) {
		this.subCategoryCode = subCategoryCode;
	}

	/**
	 * @return the brandCode
	 */
	public String getBrandCode() {
		return brandCode;
	}

	/**
	 * @param brandCode the brandCode to set
	 */
	public void setBrandCode(String brandCode) {
		this.brandCode = brandCode;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the errMsg
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/**
	 * @param errMsg the errMsg to set
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

}

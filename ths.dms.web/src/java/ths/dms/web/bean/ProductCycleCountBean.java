package ths.dms.web.bean;

public class ProductCycleCountBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4650407369342567804L;
	private String productCode;
	private String lot;
	private String quantity;
	private String countDate;
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getLot() {
		return lot;
	}
	public void setLot(String lot) {
		this.lot = lot;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getCountDate() {
		return countDate;
	}
	public void setCountDate(String countDate) {
		this.countDate = countDate;
	}
}
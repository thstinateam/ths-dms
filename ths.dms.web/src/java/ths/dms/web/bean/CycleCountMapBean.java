package ths.dms.web.bean;

import java.io.Serializable;

public class CycleCountMapBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4650407369342567804L;
	private Integer stockCardNumber;
	private String productCode;
	private String productName;
	private String description;
	private Integer quantityCounted;
	private String productLot;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getQuantityCounted() {
		return quantityCounted;
	}
	public void setQuantityCounted(Integer quantityCounted) {
		this.quantityCounted = quantityCounted;
	}
	public Integer getStockCardNumber() {
		return stockCardNumber;
	}
	public void setStockCardNumber(Integer stockCardNumber) {
		this.stockCardNumber = stockCardNumber;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductLot() {
		return productLot;
	}
	public void setProductLot(String productLot) {
		this.productLot = productLot;
	}
	
}

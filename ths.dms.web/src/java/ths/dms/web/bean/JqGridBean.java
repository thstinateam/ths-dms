/**
 * @(#)StatusBean.java   Jul 18, 2012
 * 
 * Copyright (c) 2012 Viettel telecom
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Viettel
 * Telecom. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Viettel.
 */
package ths.dms.web.bean;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * JqGridBean
 * @author hungtx
 *
 */
@XmlRootElement
@XmlSeeAlso({JqGridBean.class})
public class JqGridBean<T> {
		
	/** The page. */
	private int page;
	
	/** The max. */
	private int max;
	
	/** The total. */
	private int total;
	
	/** The rows. */
	private List<T> rows;

	/**
	 * Gets the page.
	 *
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * Sets the page.
	 *
	 * @param page the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * Gets the max.
	 *
	 * @return the max
	 */
	public int getMax() {
		return max;
	}

	/**
	 * Sets the max.
	 *
	 * @param max the max to set
	 */
	public void setMax(int max) {
		this.max = max;
	}

	/**
	 * Gets the total.
	 *
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * Sets the total.
	 *
	 * @param total the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * Gets the rows.
	 *
	 * @return the rows
	 */
	public List<T> getRows() {
		return rows;
	}

	/**
	 * Sets the rows.
	 *
	 * @param rows the rows to set
	 */
	public void setRows(List<T> rows) {
		this.rows = rows;
	}
}

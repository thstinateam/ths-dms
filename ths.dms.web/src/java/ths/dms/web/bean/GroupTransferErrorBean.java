/**
 * 
 */
package ths.dms.web.bean;

/**
 * @author hungtx
 *
 */
public class GroupTransferErrorBean extends Object{	
	
	/** The shop code. */
	private String shopCode;
	
	/** The group transfer code. */
	private String groupTransferCode;
	
	/** The group transfer name. */
	private String groupTransferName;
	
	/** The staff code. */
	private String staffCode;
	
	/** The customer code. */
	private String customerCode;
	
	/** The err msg. */
	private String errMsg;

	/**
	 * @return the shopCode
	 */
	public String getShopCode() {
		return shopCode;
	}

	/**
	 * @param shopCode the shopCode to set
	 */
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	/**
	 * @return the groupTransferCode
	 */
	public String getGroupTransferCode() {
		return groupTransferCode;
	}

	/**
	 * @param groupTransferCode the groupTransferCode to set
	 */
	public void setGroupTransferCode(String groupTransferCode) {
		this.groupTransferCode = groupTransferCode;
	}

	/**
	 * @return the groupTransferName
	 */
	public String getGroupTransferName() {
		return groupTransferName;
	}

	/**
	 * @param groupTransferName the groupTransferName to set
	 */
	public void setGroupTransferName(String groupTransferName) {
		this.groupTransferName = groupTransferName;
	}

	/**
	 * @return the staffCode
	 */
	public String getStaffCode() {
		return staffCode;
	}

	/**
	 * @param staffCode the staffCode to set
	 */
	public void setStaffCode(String staffCode) {
		this.staffCode = staffCode;
	}

	/**
	 * @return the errMsg
	 */
	public String getErrMsg() {
		return errMsg;
	}

	/**
	 * @param errMsg the errMsg to set
	 */
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	/**
	 * @return the customerCode
	 */
	public String getCustomerCode() {
		return customerCode;
	}

	/**
	 * @param customerCode the customerCode to set
	 */
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
}

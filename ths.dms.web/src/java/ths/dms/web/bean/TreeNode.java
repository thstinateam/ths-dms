/**
 * Copyright 2015 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * node tren cay
 * @author tuannd20
 * @since 02/03/2015
 */
public class TreeNode implements Serializable {
	private static final long serialVersionUID = -6457170895371354852L;

	private Object id;
	private String text;
	private String state;
	private Map<String, Object> attributes;
	private List<TreeNode> children;
	
	public Object getId() {
		return id;
	}
	public void setId(Object id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Map<String, Object> getAttributes() {
		return attributes;
	}
	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}
	public List<TreeNode> getChildren() {
		return children;
	}
	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}
	
}

/**
 * 
 */
package ths.dms.web.bean;

import java.util.List;

/**
 * @author hungtx
 *
 */
public class JsPromotionShopTreeNode {
	
	/** The data. */
	private String data;
	
	/** The attr. */
	private JsTreeNodeAttr attr;
	
	/** The children. */
	private List<JsPromotionShopTreeNode> children;
	
	/** The state. */
	private String state;
	
	/** The icon. */
	private String icon;
	
	private Integer soXuat;
	private Integer soXuatKM;
	private Integer soXuatDaKM;
	private Long id;
	private Integer isJoin;
	private Integer isShop;
	private Integer isChildJoin;
	private Integer countNode;
	private Long parentId;
	
	public JsPromotionShopTreeNode(){
		
	}
	
	public JsPromotionShopTreeNode(String data,String id){
		this.data = data;
		JsTreeNodeAttr nodeAttr = new JsTreeNodeAttr();
		nodeAttr.setId(id);
		this.attr = nodeAttr;
	}
	
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @return the attr
	 */
	public JsTreeNodeAttr getAttr() {
		return attr;
	}

	/**
	 * @param attr the attr to set
	 */
	public void setAttr(JsTreeNodeAttr attr) {
		this.attr = attr;
	}

	/**
	 * @return the children
	 */
	public List<JsPromotionShopTreeNode> getChildren() {
		return children;
	}

	/**
	 * @param children the children to set
	 */
	public void setChildren(List<JsPromotionShopTreeNode> children) {
		this.children = children;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @param icon the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getSoXuat() {
		return soXuat;
	}

	public void setSoXuat(Integer soXuat) {
		this.soXuat = soXuat;
	}

	public Integer getSoXuatKM() {
		return soXuatKM;
	}

	public void setSoXuatKM(Integer soXuatKM) {
		this.soXuatKM = soXuatKM;
	}

	public Integer getSoXuatDaKM() {
		return soXuatDaKM;
	}

	public void setSoXuatDaKM(Integer soXuatDaKM) {
		this.soXuatDaKM = soXuatDaKM;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getIsJoin() {
		return isJoin;
	}

	public void setIsJoin(Integer isJoin) {
		this.isJoin = isJoin;
	}

	public Integer getIsShop() {
		return isShop;
	}

	public void setIsShop(Integer isShop) {
		this.isShop = isShop;
	}

	public Integer getIsChildJoin() {
		return isChildJoin;
	}

	public void setIsChildJoin(Integer isChildJoin) {
		this.isChildJoin = isChildJoin;
	}

	public Integer getCountNode() {
		return countNode;
	}

	public void setCountNode(Integer countNode) {
		this.countNode = countNode;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
}

package ths.dms.web.bean;

import java.io.Serializable;
import java.util.List;

public class DataDemoReport implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4650407369342567804L;
	List<String> lstData;

	public List<String> getLstData() {
		return lstData;
	}

	public void setLstData(List<String> lstData) {
		this.lstData = lstData;
	}
}

package ths.dms.web.context;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import ths.dms.core.memcached.MemcachedClientManager;

import ths.dms.helper.AppSetting;

/**
 * Destroys Spring's ApplicationContext that is managed by
 * {@link ApplicationContextFactory} at shutdown, forcing bean defined
 * destroy-methods to be invoked.
 * 
 */
public class ApplicationServletContextListener extends ContextLoaderListener {
    /** The log. */
    protected static final Logger logger = Logger.getLogger(ApplicationServletContextListener.class);   
	
	
    /**
     * Initialize the context.
     * @param context
     *            the context
     */
    public void init(ServletContext context) {
        WebApplicationContext ctx =
            WebApplicationContextUtils
                    .getRequiredWebApplicationContext(context);
	    AutowireCapableBeanFactory bf = ctx.getAutowireCapableBeanFactory();
	    bf.autowireBean(this);
    }
    
    @Override
    public void contextInitialized(ServletContextEvent event) {
    	System.out.println("Initializing Application Context...");
		super.contextInitialized(event);
        ServletContext context = event.getServletContext();
        this.init(context);
        
        try {
			System.out.println("Initializing memcached ...");
			MemcachedClientManager.start(AppSetting.getStringValue("hostMemCached"));
		} catch (Exception e) {	
			System.out.println("Can't init memcached. Exception:");
		}
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
    	System.out.println("Destroying Application Context..."); 
		super.contextDestroyed(event);
    }
   
}

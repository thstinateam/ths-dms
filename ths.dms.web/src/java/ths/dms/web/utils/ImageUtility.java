package ths.dms.web.utils;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.imageio.ImageIO;

import vn.kunkun.rd.ImageInfo;
import vn.kunkun.rd.ImgMagickUtil;

import ths.dms.core.entities.enumtype.MediaObjectType;
import ths.dms.core.entities.vo.MapVO;

import ths.dms.helper.Configuration;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;

public class ImageUtility {
	public static final int INT_THUMB_SIZE_60 = 60;
	    
    public static final int INT_THUMB_SIZE_120 = 120;
    
    public static final int INT_THUMB_SIZE_154 = 154;
    
    public static final int INT_THUMB_SIZE_185 = 185;
    
    public static final int INT_THUMB_SIZE_200 = 200;
    
    public static final int INT_THUMB_SIZE_320 = 320;
    
    public static final int INT_DEFAULT_ENTRY_TOP_IMAGE_WIDTH = 260;
    
    public static final int INT_DEFAULT_ENTRY_TOP_IMAGE_HEIGHT = 195;
    
    public static final int INT_DEFAULT_PROMOTION_IMAGE_WIDTH = 514;
    
    public static final int INT_DEFAULT_PROMOTION_IMAGE_HEIGHT = 284;
    
    /** Big image detail on Web. */
    public static final int INT_DEFAULT_IMAGE_WIDTH = 960;
    public static final int INT_DEFAULT_IMAGE_HEIGHT = 720;
    
    /** Small thumbnail image feed/chat/list image on Web. */
    public static final int INT_THUMB_SIZE_128 = 128;
    public static final int INT_THUMB_SIZE_96 = 96;
    
    public static final int INT_THUMB_SIZE_580 = 580;
    public static final int INT_THUMB_SIZE_420 = 420;
    
    /** Big thumbnail image feed/chat on Web */
    public static final int INT_THUMB_SIZE_400 = 400;
    public static final int INT_THUMB_SIZE_300 = 300;
    
    public static final int INT_THUMB_SIZE_240 = 240;
    public static final int INT_THUMB_SIZE_180 = 180;
    
    /** Avatar big thumbnail image feed/chat on Web. */
    public static final int INT_THUMB_SIZE_150 = 150;
    
    /** Small thumbnail image feed/chat/list image in Mobile. */
    public static final int INT_THUMB_SIZE_70 = 70;
    
    /** Big thumbnail image feed/chat on Mobile */
    public static final int INT_THUMB_SIZE_640 = 640;
    public static final int INT_THUMB_SIZE_480 = 480;
    
    /** Avatar header and autocomplete chat*/
    public static final int INT_THUMB_SIZE_20 = 20;
    
    /** chat bar */
    public static final int INT_THUMB_SIZE_30 = 30;
    
    /** Avatar feed/chat/notify/home/profile*/
    public static final int INT_THUMB_SIZE_50 = 50;
    
    private static final String _SEPARATOR = "/"; 
    
    public static final int INT_THUMB_SIZE_1024 = 1024;
    
    public static final int INT_THUMB_SIZE_768 = 768;

    public static Boolean createThumbnail(String srcImage, String outPutFileName,
            Integer thumbWidth, Integer thumbHeight) {
    	Boolean createGif = checkSizeCreateGif(thumbWidth, thumbHeight);
    	Boolean res = ImgMagickUtil.resize(srcImage, outPutFileName,
    			thumbWidth, thumbHeight, null, createGif);
        if (res == null || !res ) {
            return false;
        }
        return true;

    }
    public static Boolean checkSizeCreateGif(int width, int height){
    	if((width == ImageUtility.INT_THUMB_SIZE_400 && height == ImageUtility.INT_THUMB_SIZE_300)
    			|| (width == ImageUtility.INT_DEFAULT_IMAGE_WIDTH && height == ImageUtility.INT_DEFAULT_IMAGE_HEIGHT)){
    		return true;
    	}
    	return false;
    }
//    public static String createDetailUploadPath(Long productId,Calendar calendar,Integer type) {
//        StringBuffer folder = new StringBuffer();
//        if(productId != null) {
//        	if(type == 0){
//        		folder.append(ConstantManager.PATH_TO_GALLERY);
//        	}else if(type == 1){
//        		folder.append(ConstantManager.PATH_TO_VIDEO);
//        	}else{
//        		folder.append(ConstantManager.PATH_TO_GALLERY_THUMBNAIL);
//        	}
//            folder.append(calendar.get(Calendar.YEAR));
//            folder.append(_SEPARATOR);
//            folder.append(calendar.get(Calendar.MONTH) + 1);
//            folder.append(_SEPARATOR);
//            folder.append(productId);
//            folder.append(_SEPARATOR);
//        } 
//        return folder.toString();
//    }
    
    /**
     * Xu ly createDetailUploadFilePath
     * @author vuongmq
     * @param calendar
     * @param type
     * @param categoryCode
     * @return String
     * @since Nov 26, 2015
     */
	public static String createDetailUploadFilePath(Calendar calendar, Integer type, String categoryCode) {
		StringBuffer folder = new StringBuffer();
		if (type == MediaObjectType.IMAGE_FEEDBACK.getValue()) {
			folder.append(ConstantManager.PATH_TO_ATTACH_FILES);
		}
		if (!StringUtil.isNullOrEmpty(categoryCode)) {
			folder.append(categoryCode);
		}
		folder.append(calendar.get(Calendar.YEAR));
		folder.append(_SEPARATOR);
		folder.append(calendar.get(Calendar.MONTH) + 1);
		folder.append(_SEPARATOR);
		return folder.toString();
	}
    
    public static String createDetailUploadPath(Long productId,Calendar calendar,Integer type,String categoryCode) {
        StringBuffer folder = new StringBuffer();
        if(productId != null) {
        	folder.append(calendar.get(Calendar.YEAR));
        	folder.append(_SEPARATOR);
        	folder.append(calendar.get(Calendar.MONTH) + 1);
        	folder.append(_SEPARATOR);
        	folder.append(categoryCode);
        	if(type == 0){
        		folder.append(ConstantManager.PATH_TO_GALLERY_THUMBNAIL);
        	}else if(type == 1){
        		folder.append(ConstantManager.PATH_TO_VIDEO);
        	}else if(type == 2){
        		folder.append(ConstantManager.PATH_TO_GALLERY);
        	}else if(type==4){
        		folder.append(ConstantManager.PATH_TO_VIDEO);
        	}else{
        		folder.append(ConstantManager.PATH_TO_GALLERY_BIGTHUMBNAIL);
        	}
        } 
        return folder.toString();
    }
    
    public static ImageInfo getImageInfo(File f) {
    	ImageInfo info = new ImageInfo();
		try {
			info = ImageInfo.getImageInfo(f);
		} catch (Exception e) {
			return null;
		}
		return info;
    }
    
    public static String rotateImage(String realPath) throws FileNotFoundException, IOException {
    	BufferedImage oldImage = ImageIO.read(new FileInputStream(realPath)); //realPath truyen vao duong dan vat ly.ok
		BufferedImage newImage = new BufferedImage(oldImage.getHeight(), oldImage.getWidth(), oldImage.getType());
		Graphics2D graphics = (Graphics2D) newImage.getGraphics();
		graphics.rotate(Math.toRadians(90), (0 + newImage.getWidth()) / 2, (0 + newImage.getHeight()) / 2);
		graphics.drawImage(oldImage, (oldImage.getHeight() - oldImage.getWidth())/2, (oldImage.getWidth() - oldImage.getHeight())/2, oldImage.getWidth(), oldImage.getHeight(), null);
		OutputStream out = new FileOutputStream(realPath);
		ImageIO.write(newImage, "JPG", out);
		out.close();
		return realPath;
    }
    
    public static String createDetailUploadPathForMT(String albumCode,Calendar calendar,Integer type) {
        StringBuffer folder = new StringBuffer();
        if(!StringUtil.isNullOrEmpty(albumCode)) {
        	folder.append(calendar.get(Calendar.YEAR));
        	folder.append(_SEPARATOR);
        	folder.append(calendar.get(Calendar.MONTH) + 1);
        	folder.append(_SEPARATOR);
        	folder.append(albumCode);
        	if(type == 0){
        		folder.append(ConstantManager.PATH_TO_GALLERY_THUMBNAIL);
        	}else if(type == 1){
        		folder.append(ConstantManager.PATH_TO_VIDEO);
        	}else if(type == 2){
        		folder.append(ConstantManager.PATH_TO_GALLERY);
        	}else if(type==4){
        		folder.append(ConstantManager.PATH_TO_VIDEO);
        	}else{
        		folder.append(ConstantManager.PATH_TO_GALLERY_BIGTHUMBNAIL);
        	}
        } 
        return folder.toString();
    }
    
    
	public static String getOutputFileZip(String outputFileName){
		StringBuilder sbFileName = new StringBuilder(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_FILE_EXPORT))
			.append("_").append(outputFileName).append(FileExtension.ZIP.getValue());
		return StringUtil.replaceSeparatorChar(sbFileName.toString());
	}
    
    public static String exportAlbum(List<String> lstUrl,String fileZipName) throws Exception {
 		String outputFile = getOutputFileZip(fileZipName);
 		String outputPath = Configuration.getStoreRealPath();
 		String outputDownload = Configuration.getExportExcelPath()+"/" + outputFile;
// 		String outputPath = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getStoreRealPath();
// 		String outputDownload = ServletActionContext.getServletContext().getContextPath() + Configuration.getExportExcelPath() + outputFile;
 		String imageRealPath = Configuration.getImageRealSOPath();
 		try{
 			List<File>  lstFileImage = new ArrayList<File>();
 			MapVO<String, String> map = new MapVO<String, String>();
 			for(int i = 0; i< lstUrl.size() ;++i) {
 				if(map.get(lstUrl.get(i))== null) {
 					map.put(lstUrl.get(i), lstUrl.get(i));
 				}
 			}
 			for(String url : map.keyList()) {
 				File file = new File(imageRealPath + map.get(url));
 				if (file.exists()) {
 					lstFileImage.add(file);
 				}
 			}
 			FileUtility.compress(lstFileImage, outputFile, outputPath);
 		}catch (Exception e) {
 			LogUtility.logError(e, e.getMessage());
 			System.out.print(e.getMessage());
 			throw new Exception(e);
 		}
 		return outputDownload;
 	}
    
    /**
     * For SalemT
     * @param lstUrl
     * @param fileZipName
     * @return
     * @throws Exception
     */
    public static String exportAlbumSaleMT(List<String> lstUrl,String fileZipName) throws Exception {
 		String outputFile = getOutputFileZip(fileZipName);
 		String outputPath = Configuration.getStoreRealPath();
 		String outputDownload = Configuration.getExportExcelPath()+"/" + outputFile;
// 		String outputPath = ServletActionContext.getServletContext().getRealPath("/") + Configuration.getStoreRealPath();
// 		String outputDownload = ServletActionContext.getServletContext().getContextPath() + Configuration.getExportExcelPath() + outputFile;
 		String imageRealPath = Configuration.getImageRealPath();
 		try{
 			List<File>  lstFileImage = new ArrayList<File>();
 			MapVO<String, String> map = new MapVO<String, String>();
 			for(int i = 0; i< lstUrl.size() ;++i) {
 				if(map.get(lstUrl.get(i))== null) {
 					map.put(lstUrl.get(i), lstUrl.get(i));
 				}
 			}
 			for(String url : map.keyList()) {
 				File file = new File(imageRealPath + map.get(url));
 				if (file.exists()) {
 					lstFileImage.add(file);
 				}
 			}
 			FileUtility.compress(lstFileImage, outputFile, outputPath);
 		}catch (Exception e) {
 			LogUtility.logError(e, e.getMessage());
 			System.out.print(e.getMessage());
 			throw new Exception(e);
 		}
 		return outputDownload;
 	}
}

package ths.dms.web.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import ths.dms.core.entities.RequestLog;

import jxl.Cell;
import ths.dms.helper.Configuration;
import ths.dms.web.bean.CellBean;
import ths.dms.web.constant.ConstantManager;
import viettel.passport.client.UserToken;

/**
 * The Class StringUtil.
 */
public final class StringUtil {	
	//Bo sung cho Ghi log
	public static final String APP_LOG_HOME = "/home";
	public static final String APP_LOG_LOGIN = "/login";
	public static final String APP_LOG_LOGOUT = "/commons/logout";
	public static final String LOG_LOGIN_SUCCESS = "LOGIN_SUCCESS";
	public static final String LOG_LOGIN_ERROR = "LOGIN_ERROR";
	public static final String LOG_LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
	public static final String LOG_LOGOUT_ERROR = "LOGOUT_ERROR";
	
	public static final int LOGIN_CAPCHAR_MAX = 5;
	public static final String TEMPLATE_XNB = "XNB";
	public static final String TEMPLATE_VAT = "VAT";
	private static final String EMAIL_REGEX ="^[a-zA-Z0-9\\._-]{1,64}@[A-Za-z0-9]{2,64}(\\.[A-Za-z0-9]{2,64})*(\\.[A-Za-z]{2,4})$";
	private static final String[] htmlChar = new String[] { "&", "<", ">", "'","\"" };
	private static final String[] htmlNameCode = new String[] { "&amp;","&lt;", "&gt;", "&apos;", "&quot;" };
	public static final String PRODUCT_INFOR_SUBTRACT_Z = "Z";
	
	public static boolean validateEmail(String emailAdress) {
		if (StringUtil.isNullOrEmpty(emailAdress))
			return false;
		return emailAdress.trim().matches(EMAIL_REGEX);
	}
	/**
	 * Checks if is null or empty.
	 * 
	 * @param s
	 *            the s
	 * 
	 * @return true, if is null or empty
	 */
	public static boolean isNullOrEmpty(final String s) {
		return (s == null || s.trim().length() == 0);
	}

	/**
	 * Checks if is null or empty.
	 * 
	 * @param s
	 *            the s
	 * 
	 * @return true, if is null or empty
	 */
	public static boolean isNullOrEmptyNotTrim(final String s) {
		return (s == null || s.length() == 0);
	}
	
	/**
	 * From null to emtpy string.
	 * 
	 * @param a
	 *            the a
	 * 
	 * @return the string
	 */
	public static String fromNullToEmtpyString(String a) {
		if (a == null) {
			return "";
		} else {
			return a;
		}
	}
	
	/**
	 * Gets the message from resource.
	 *
	 * @param params the params
	 * @return the message from resource
	 */
	public static String getMessageFromResource(List<Object> lstValue,Object... params){
	    String message = "";
	    if (params != null && params.length > 0) {
		List<String> lstParam = new ArrayList<String>();		
		for (int i = 1; i < params.length; i++) {
			if(params[i]!= null){
			    String msg = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,String.valueOf(params[i]));
			    lstParam.add(msg);
			}
		}
		message = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, String.valueOf(params[0]), lstParam.toArray(),lstValue.toArray());
	    }
	    return message;
	}
	
	/**
	 * Gets the map image url.
	 *
	 * @param lat the lat
	 * @param lng the lng
	 * @param zoom the zoom
	 * @return the map image url
	 */
	public static String getMapImageUrl(double lat, double lng,int zoom){
		if(zoom<=0){
			zoom = ConstantManager.MAP_ZOOM_DEFAULT;
		}
		String service="vbd.map.service";
		if(lat == ConstantManager.LAT_LNG_EMPTY || lng == ConstantManager.LAT_LNG_EMPTY){
			lat = ConstantManager.LAT_VIETNAM;
			lng = ConstantManager.LNG_VIETNAM;
			zoom = ConstantManager.MAP_VN_ZOOM_DEFAULT;
			service = "vbd.map.service.default";
		}
		return Configuration.getResourceString(
				ConstantManager.VI_LANGUAGE,service,
				lat, lng, zoom,
				ConstantManager.MARKER_WIDTH_DEFAULT,
				ConstantManager.MARKER_HEIGHT_DEFAULT, lat, lng);
	}
	
	/**
	 * Gets the request log.
	 *
	 * @return the request log
	 * @author hungtx
	 */
	public static RequestLog getRequestLog(){
		RequestLog vbdRequest =new RequestLog();
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	    HttpServletRequest request =  attr.getRequest();			
		vbdRequest.setClient(ConstantManager.CLIENT_NAME_WEB);
		if(request!= null){
			vbdRequest.setIp(request.getRemoteAddr());
			if(request.getSession()!= null && request.getSession().getAttribute("cmsUserToken")!= null){
				UserToken user= (UserToken)request.getSession().getAttribute("cmsUserToken");
				if(user!= null){
					vbdRequest.setUserName(user.getUserName());
				}				
			}
		}
				
		return vbdRequest;
	}
			
	
	/**
	 * Gets the full code.
	 *
	 * @param shopCode the shop code
	 * @param shortCode the short code
	 * @return the full code
	 */
	public static String getFullCode(String shopCode, String shortCode){
		String fullCode = "";
		if(!StringUtil.isNullOrEmpty(shortCode)){
			fullCode+=shortCode;
		} else {
			return "";
		}
		if(!StringUtil.isNullOrEmpty(shopCode)){
			fullCode+= "_" + shopCode;
		}
		return fullCode;
	}
	 public static CellBean addFailBean(Cell rowData[],String message){
		 CellBean beanFail = new CellBean();
		 if (rowData.length >= 1) {
			beanFail.setContent1(rowData[0].getContents().toString().trim());
		 }
		 if (rowData.length >= 2) {
			beanFail.setContent2(rowData[1].getContents().toString().trim());
		 }
		 if (rowData.length >= 3) {
			beanFail.setContent3(rowData[2].getContents().toString().trim());
		 }
		 if (rowData.length >= 4) {
			beanFail.setContent4(rowData[3].getContents().toString().trim());
		 }
		 if (rowData.length >= 5) {
			beanFail.setContent5(rowData[4].getContents().toString().trim());
		 }
		 if (rowData.length >= 6) {
			beanFail.setContent6(rowData[5].getContents().toString().trim());
		 }
		 if (rowData.length >= 7) {
			beanFail.setContent7(rowData[6].getContents().toString().trim());
		 }
		 if (rowData.length >= 8) {
			beanFail.setContent8(rowData[7].getContents().toString().trim());
		 }
		 if (rowData.length >= 9) {
			beanFail.setContent9(rowData[8].getContents().toString().trim());
		 }
		 if (rowData.length >= 10) {
			beanFail.setContent10(rowData[9].getContents().toString().trim());
		 }
		 if (rowData.length >= 11) {
			beanFail.setContent11(rowData[10].getContents().toString().trim());
		 }
		 if (rowData.length >= 12) {
			beanFail.setContent12(rowData[11].getContents().toString().trim());
		 }
		 if (rowData.length >= 13) {
			beanFail.setContent13(rowData[12].getContents().toString().trim());
		 }
		 if (rowData.length >= 14) {
			beanFail.setContent14(rowData[13].getContents().toString().trim());
		 }
		 if (rowData.length >= 15) {
			beanFail.setContent15(rowData[14].getContents().toString().trim());
		 }
		 if (rowData.length >= 16) {
			beanFail.setContent16(rowData[15].getContents().toString().trim());
		 }
		 if (rowData.length >= 17) {
			beanFail.setContent17(rowData[16].getContents().toString().trim());
		 }
		 if (rowData.length >= 18) {
			beanFail.setContent18(rowData[17].getContents().toString().trim());
		 }
		 if (rowData.length >= 19) {
			beanFail.setContent19(rowData[18].getContents().toString().trim());
		 }
		 if (rowData.length >= 20) {
			beanFail.setContent20(rowData[19].getContents().toString().trim());
	     }
		 if (rowData.length >= 21) {
			beanFail.setContent21(rowData[20].getContents().toString().trim());
	     }
		 if (rowData.length >= 22) {
			beanFail.setContent22(rowData[21].getContents().toString().trim());
		 }
		 if (rowData.length >= 23) {
			beanFail.setContent23(rowData[22].getContents().toString().trim());
		 }
		 if (rowData.length >= 24) {
			beanFail.setContent24(rowData[23].getContents().toString().trim());
		 }
		 if (rowData.length >= 25) {
			beanFail.setContent25(rowData[24].getContents().toString().trim());
		 }
		 if (rowData.length >= 26) {
				beanFail.setContent26(rowData[25].getContents().toString().trim());
			 }
		 if (rowData.length >= 27) {
				beanFail.setContent27(rowData[26].getContents().toString().trim());
			 }
		 if (rowData.length >= 28) {
				beanFail.setContent28(rowData[27].getContents().toString().trim());
			 }
		 if (rowData.length >= 29) {
				beanFail.setContent29(rowData[28].getContents().toString().trim());
			 }
		 beanFail.setErrMsg(message);
		 return beanFail;
	 }
	 
	 public static CellBean addFailBean(List<String> lstData,String message){
		 CellBean beanFail = new CellBean();
		 if (lstData.size() > 0) {
			beanFail.setContent1(lstData.get(0));
		 }
		 if (lstData.size() > 1) {
			beanFail.setContent2(lstData.get(1));
		 }
		 if (lstData.size() > 2) {
			beanFail.setContent3(lstData.get(2));
		 } 
		 if (lstData.size() > 3) {
			beanFail.setContent4(lstData.get(3));
		 }
		 if (lstData.size() > 4) {
			beanFail.setContent5(lstData.get(4));
		 }
		 if (lstData.size() > 5) {
			beanFail.setContent6(lstData.get(5));
		 }
		 if (lstData.size() > 6) {
			beanFail.setContent7(lstData.get(6));
		 }
		 if (lstData.size() > 7) {
			beanFail.setContent8(lstData.get(7));
		 }
		 if (lstData.size() > 8) {
			beanFail.setContent9(lstData.get(8));
		 }
		 if (lstData.size() > 9) {
			beanFail.setContent10(lstData.get(9));
		 }
		 if (lstData.size() > 10) {
			beanFail.setContent11(lstData.get(10));
		 }
		 if (lstData.size() > 11) {
			beanFail.setContent12(lstData.get(11));
		 }
		 if (lstData.size() > 12) {
			beanFail.setContent13(lstData.get(12));
		 }
		 if (lstData.size() > 13) {
			beanFail.setContent14(lstData.get(13));
		 }
		 if (lstData.size() > 14) {
			beanFail.setContent15(lstData.get(14));
		 }
		 if (lstData.size() > 15) {
			beanFail.setContent16(lstData.get(15));
		 }
		 if (lstData.size() > 16) {
			beanFail.setContent17(lstData.get(16));
		 }
		 if (lstData.size() > 17) {
			beanFail.setContent18(lstData.get(17));
		 }
		 if (lstData.size() > 18) {
			beanFail.setContent19(lstData.get(18));
		 }
		 if (lstData.size() > 19) {
			beanFail.setContent20(lstData.get(19));
		 }
		 if (lstData.size() > 20) {
			beanFail.setContent21(lstData.get(20));
		 }
		 if (lstData.size() > 21) {
			beanFail.setContent22(lstData.get(21));
		 }
		 if (lstData.size() > 22) {
			beanFail.setContent23(lstData.get(22));
		 }
		 if (lstData.size() > 23) {
			beanFail.setContent24(lstData.get(23));
		 }
		 if (lstData.size() > 24) {
			beanFail.setContent25(lstData.get(24));
		 }
		 if (lstData.size() > 25) {
			beanFail.setContent26(lstData.get(25));
		 }
		 if (lstData.size() > 26) {
			beanFail.setContent27(lstData.get(26));
		 }
		 if (lstData.size() > 27) {
			beanFail.setContent28(lstData.get(27));
		 }
		 if (lstData.size() > 28) {
			beanFail.setContent29(lstData.get(28));
		 }
		 if (lstData.size() > 29) {
			beanFail.setContent30(lstData.get(29));
		 }
		 if (lstData.size() > 30) {
				beanFail.setContent31(lstData.get(30));
			 }
		 if (lstData.size() > 31) {
				beanFail.setContent32(lstData.get(31));
			 }
		 if (lstData.size() > 32) {
				beanFail.setContent33(lstData.get(32));
			 }
		 if (lstData.size() > 33) {
				beanFail.setContent34(lstData.get(33));
			 }
		 if (lstData.size() > 34) {
				beanFail.setContent35(lstData.get(34));
			 }
		 beanFail.setErrMsg(message);
		 return beanFail;
	 }
	 
	 /***
	  * @author trungtm6
	  * @since 21/07/2015
	  * @param lstData
	  * @param beginCol: begin dynamic column
	  * @param message
	  * @return
	  */
	 public static CellBean addDynamicFailBean(List<String> lstData, int beginCol, String message){
		 CellBean beanFail = new CellBean();
		 if (lstData != null && beginCol < lstData.size()) {
			 List<String> lstDyn = new ArrayList<String>();
			//add list dynamic column
			 for (int i = beginCol - 1, n = lstData.size(); i < n; i++) {
				 lstDyn.add(lstData.get(i));
			 }
			 beanFail.setLstDynamic(lstDyn);
			 
			 if (lstData.size() > 0) {
				beanFail.setContent1(lstData.get(0));
			 }
			 if (lstData.size() > 1) {
				beanFail.setContent2(lstData.get(1));
			 }
			 if (lstData.size() > 2) {
				beanFail.setContent3(lstData.get(2));
			 } 
			 if (lstData.size() > 3) {
				beanFail.setContent4(lstData.get(3));
			 }
			 if (lstData.size() > 4) {
				beanFail.setContent5(lstData.get(4));
			 }
			 if (lstData.size() > 5) {
				beanFail.setContent6(lstData.get(5));
			 }
			 if (lstData.size() > 6) {
				beanFail.setContent7(lstData.get(6));
			 }
			 if (lstData.size() > 7) {
				beanFail.setContent8(lstData.get(7));
			 }
			 if (lstData.size() > 8) {
				beanFail.setContent9(lstData.get(8));
			 }
			 if (lstData.size() > 9) {
				beanFail.setContent10(lstData.get(9));
			 }
			 if (lstData.size() > 10) {
				beanFail.setContent11(lstData.get(10));
			 }
			 if (lstData.size() > 11) {
				beanFail.setContent12(lstData.get(11));
			 }
			 if (lstData.size() > 12) {
				beanFail.setContent13(lstData.get(12));
			 }
			 if (lstData.size() > 13) {
				beanFail.setContent14(lstData.get(13));
			 }
			 if (lstData.size() > 14) {
				beanFail.setContent15(lstData.get(14));
			 }
			 if (lstData.size() > 15) {
				beanFail.setContent16(lstData.get(15));
			 }
			 if (lstData.size() > 16) {
				beanFail.setContent17(lstData.get(16));
			 }
			 if (lstData.size() > 17) {
				beanFail.setContent18(lstData.get(17));
			 }
			 if (lstData.size() > 18) {
				beanFail.setContent19(lstData.get(18));
			 }
			 if (lstData.size() > 19) {
				beanFail.setContent20(lstData.get(19));
			 }
			 if (lstData.size() > 20) {
				beanFail.setContent21(lstData.get(20));
			 }
			 if (lstData.size() > 21) {
				beanFail.setContent22(lstData.get(21));
			 }
			 if (lstData.size() > 22) {
				beanFail.setContent23(lstData.get(22));
			 }
			 if (lstData.size() > 23) {
				beanFail.setContent24(lstData.get(23));
			 }
			 if (lstData.size() > 24) {
				beanFail.setContent25(lstData.get(24));
			 }
			 if (lstData.size() > 25) {
				beanFail.setContent26(lstData.get(25));
			 }
			 if (lstData.size() > 26) {
				beanFail.setContent27(lstData.get(26));
			 }
			 if (lstData.size() > 27) {
				beanFail.setContent28(lstData.get(27));
			 }
			 if (lstData.size() > 28) {
				beanFail.setContent29(lstData.get(28));
			 }
			 if (lstData.size() > 29) {
				beanFail.setContent30(lstData.get(29));
			 }
			 if (lstData.size() > 30) {
					beanFail.setContent31(lstData.get(30));
				 }
			 if (lstData.size() > 31) {
					beanFail.setContent32(lstData.get(31));
				 }
			 if (lstData.size() > 32) {
					beanFail.setContent33(lstData.get(32));
				 }
			 if (lstData.size() > 33) {
					beanFail.setContent34(lstData.get(33));
				 }
			 if (lstData.size() > 34) {
					beanFail.setContent35(lstData.get(34));
				 }
			 beanFail.setErrMsg(message);
			 return beanFail;
			 /*int remainCol = lstData.size() - beginCol + 1;
			 
			 switch (remainCol) {
			 case 0:
				 break;
			 case 1:
				 break;
			 case 2:
				 break;
			 case 3:
				 break;
			 case 4:
				 break;
			 case 5: 
				 break;
			 case 6:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 break;
			 case 7: 
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 break;
			 case 8: 
				 break;
			 case 9:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 break;
			 case 10:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 cb.setContent10(lstData.get(9));
				 break;
			 case 11:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 cb.setContent10(lstData.get(9));
				 cb.setContent11(lstData.get(10));
				 break;
			 case 12:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 cb.setContent10(lstData.get(9));
				 cb.setContent11(lstData.get(10));
				 cb.setContent12(lstData.get(11));
				 break;
			 case 13:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 cb.setContent10(lstData.get(9));
				 cb.setContent11(lstData.get(10));
				 cb.setContent12(lstData.get(11));
				 cb.setContent13(lstData.get(12));
				 break;
			 case 14:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 cb.setContent10(lstData.get(9));
				 cb.setContent11(lstData.get(10));
				 cb.setContent12(lstData.get(11));
				 cb.setContent13(lstData.get(12));
				 cb.setContent14(lstData.get(13));
				 break;
			 case 15:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 cb.setContent10(lstData.get(9));
				 cb.setContent11(lstData.get(10));
				 cb.setContent12(lstData.get(11));
				 cb.setContent13(lstData.get(12));
				 cb.setContent14(lstData.get(13));
				 cb.setContent15(lstData.get(14));
				 break;
			 case 16:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 cb.setContent10(lstData.get(9));
				 cb.setContent11(lstData.get(10));
				 cb.setContent12(lstData.get(11));
				 cb.setContent13(lstData.get(12));
				 cb.setContent14(lstData.get(13));
				 cb.setContent15(lstData.get(14));
				 cb.setContent16(lstData.get(15));
				 break;
			 case 17:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 cb.setContent10(lstData.get(9));
				 cb.setContent11(lstData.get(10));
				 cb.setContent12(lstData.get(11));
				 cb.setContent13(lstData.get(12));
				 cb.setContent14(lstData.get(13));
				 cb.setContent15(lstData.get(14));
				 cb.setContent16(lstData.get(15));
				 cb.setContent17(lstData.get(16));
				 break;
			 case 18:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 cb.setContent10(lstData.get(9));
				 cb.setContent11(lstData.get(10));
				 cb.setContent12(lstData.get(11));
				 cb.setContent13(lstData.get(12));
				 cb.setContent14(lstData.get(13));
				 cb.setContent15(lstData.get(14));
				 cb.setContent16(lstData.get(15));
				 cb.setContent17(lstData.get(16));
				 cb.setContent18(lstData.get(17));
				 break;
			 case 19:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 cb.setContent10(lstData.get(9));
				 cb.setContent11(lstData.get(10));
				 cb.setContent12(lstData.get(11));
				 cb.setContent13(lstData.get(12));
				 cb.setContent14(lstData.get(13));
				 cb.setContent15(lstData.get(14));
				 cb.setContent16(lstData.get(15));
				 cb.setContent17(lstData.get(16));
				 cb.setContent18(lstData.get(17));
				 cb.setContent19(lstData.get(18));
				 break;
			 case 20:
				 cb.setContent1(lstData.get(0));
				 cb.setContent2(lstData.get(1));
				 cb.setContent3(lstData.get(2));
				 cb.setContent4(lstData.get(3));
				 cb.setContent5(lstData.get(4));
				 cb.setContent6(lstData.get(5));
				 cb.setContent7(lstData.get(6));
				 cb.setContent8(lstData.get(7));
				 cb.setContent9(lstData.get(8));
				 cb.setContent10(lstData.get(9));
				 cb.setContent11(lstData.get(10));
				 cb.setContent12(lstData.get(11));
				 cb.setContent13(lstData.get(12));
				 cb.setContent14(lstData.get(13));
				 cb.setContent15(lstData.get(14));
				 cb.setContent16(lstData.get(15));
				 cb.setContent17(lstData.get(16));
				 cb.setContent18(lstData.get(17));
				 cb.setContent19(lstData.get(18));
				 cb.setContent20(lstData.get(19));
				 break;
				 default:
					 break;
				 
			 }*/
			 
		 }
		 beanFail.setErrMsg(message);
		 return beanFail;
	 }
	 
	 public static String convertConvfact(Integer quality, Integer convfact) {
			int first = quality / convfact;
			int last = quality % convfact;
			return first + "/" + last;
	}
	 public static String convertNumber(Object number) {
		 if(number instanceof BigDecimal) {
			 BigDecimal __number = (BigDecimal) number;
			 __number = __number.setScale(0);
			 return String.valueOf(__number);
		 } else if(number instanceof Integer || number instanceof Long) {
			 return String.valueOf(number);
		 } else if(number instanceof Float) {
			 Float __number = (Float) number;
			 if(__number % 1 != 0) {
				 return number.toString();
			 } else {
				 return String.valueOf(__number.intValue());
			 }
		 } else if(number instanceof Double) {
			 Double __number = (Double) number;
			 if(__number % 1 != 0) {
				 return number.toString();
			 } else {
				 return String.valueOf(__number.intValue());
			 }
		 }
		 return number.toString();
	 }
	 
	public static String CodeAddName(String code,String name){			
		if(StringUtil.isNullOrEmpty(code) && !StringUtil.isNullOrEmpty(name)){
			return name;
		}
		if(!StringUtil.isNullOrEmpty(code) && StringUtil.isNullOrEmpty(name)){
			return code;
		}
		if(!StringUtil.isNullOrEmpty(code) && !StringUtil.isNullOrEmpty(name)){
			return code + " - " + name;
		}
		return  "";
	}
	
	public static String CodeAddNameEx(String code,String name){			
		if(StringUtil.isNullOrEmpty(code) && !StringUtil.isNullOrEmpty(name)){
			return name;
		}
		if(!StringUtil.isNullOrEmpty(code) && StringUtil.isNullOrEmpty(name)){
			return code;
		}
		if(!StringUtil.isNullOrEmpty(code) && !StringUtil.isNullOrEmpty(name)){
			return code + " / " + name;
		}
		return  "";
	}
	
	public static String priceWithDecimal (Double price) {
	    DecimalFormat formatter = new DecimalFormat("###,###,###.00");
	    return formatter.format(price);
	}

	public static String priceWithoutDecimal (Double price) {
	    DecimalFormat formatter = new DecimalFormat("###,###,###.##");
	    return formatter.format(price);
	}

	public static String priceToString(Double price) {
	    String toShow = priceWithoutDecimal(price);
	    if (toShow.indexOf(".") > 0) {
	        return priceWithDecimal(price);
	    } else {
	        return priceWithoutDecimal(price);
	    }
	}
	
	public static String convertStringToHTMLCode(String strSource) {
		if (isNullOrEmpty(strSource)) {
			return "";
		}
		for (int i = 0; i < htmlChar.length; i++) {
			strSource = strSource.replaceAll(htmlChar[i], htmlNameCode[i]);
		}
		return strSource;
	}
	
	public static String replaceSeparatorChar(String fileName){
		return fileName.replace('/', File.separatorChar);
	}
	
	public static Integer getTypeDebitCustomer(Date date){
		if(date!=null){
			if(DateUtil.compareDateWithoutTime(DateUtil.now(), date)>0){
				return 1;
			}if(DateUtil.compareDateWithoutTime(DateUtil.now(), date)==0){
				return 2;
			}else {
				return 3;
			}
		}
		return -1;
	}
	public static String convertHTMLNewLine(String str){
		String result = "";
		String regex ="(\r\n|\n)";
		String replacement = "&newline;";
		result = str.replaceAll(regex, replacement);
		return result;
	}
	public static String convertHTMLBreakLine(String str){
		String result = "";
		String regex ="(\r\n|\n)";
		String replacement = "<br />";
		result = str.replaceAll(regex, replacement);
		return result;
	}
	
	/**
	 * Convert money to string.
	 *
	 * @param money the money
	 * @return the string
	 */
	public static String convertMoneyToString(BigDecimal money) {
		String result = "";
		String strMoney = "";
		String thousandText = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "thousand");
		String millionText = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "million");
		String billionText = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "billion");
		boolean isNegative = false;
		if (money == null) {
			return "";
		}
		if (money != null) {
			strMoney = money.toString();
		}
		if (money.compareTo(BigDecimal.ZERO) == 0) {
			char[] stringArray = getNumberText(0).toCharArray();
			stringArray[0] = Character.toUpperCase(stringArray[0]);
			result = new String(stringArray);
			return new StringBuilder(result).append(" ").append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "vnd")).toString();
		} else if (money.compareTo(BigDecimal.ONE) == -1) {
			isNegative = true;
			strMoney = strMoney.replaceAll("-", "");
		}
		if (strMoney.indexOf(".") > 0) {
			strMoney = strMoney.substring(0, strMoney.indexOf("."));
		}
		List<String> lstComp = new ArrayList<String>();
		int numSpliter = strMoney.length() / 3;
		int numMod = strMoney.length() % 3;
		int startSpliterIdx = 0;
		int endSpliterIdx = 0;
		if (numMod > 0) {
			endSpliterIdx = numMod;
			lstComp.add(strMoney.substring(startSpliterIdx, endSpliterIdx));
			startSpliterIdx = numMod;
		}
		if (numSpliter > 0) {
			for (int i = 1; i <= numSpliter; i++) {
				endSpliterIdx += 3;
				lstComp.add(strMoney.substring(startSpliterIdx, endSpliterIdx));
				startSpliterIdx = endSpliterIdx;
			}
		}
		String oNumber = "";
		int order = 0;
		for (int i = lstComp.size() - 1; i >= 0; i--) {
			String comp = lstComp.get(i);
			String numberText = convertHundredNumberToString(comp);
			String splitText = "";
			switch (order) {
			case 1:
				if (Integer.valueOf(comp) > 0) {
					splitText = thousandText;
				}
				break;
			case 2:
				if (Integer.valueOf(comp) > 0) {
					splitText = millionText;
				}
				break;
			case 3:
				if (Integer.valueOf(comp) > 0) {
					splitText = billionText;
				}
				break;
			default:
				break;
			}
			if (order == 0) {
				result = numberText;
			} else {
				StringBuilder sbResult = new StringBuilder(numberText).append(" ").append(splitText);
				if (Long.valueOf(oNumber) > 0) {
					sbResult.append(" ").append(result);
				}
				result = sbResult.toString();
			}
			if (order == 3) {
				order = 1;
			} else {
				order++;
			}
			oNumber = comp + oNumber;
		}
		if (result.length() > 0) {
			char[] stringArray = result.toCharArray();
			stringArray[0] = Character.toUpperCase(stringArray[0]);
			result = new String(stringArray);
		}
		if (isNegative) {
			return new StringBuilder(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "negative.sign")).append(" ").append(result.trim()).append(" ").append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "vnd")).toString();
		}
		return new StringBuilder(result.trim()).append(" ").append(Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "vnd")).toString();
	}

	private static String convertHundredNumberToString(String strNumber) {
		String result = "";
		String strHundred = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "hundred");
		int numLen = strNumber.length();
		if (Integer.valueOf(strNumber) == 0) {
			return "";
		}
		if (numLen == 1) {
			result = getNumberText(Integer.valueOf(strNumber));
		} else if (numLen == 2) {
			result = convertTwoNummericToString(strNumber);
		} else {
			int hundred = Integer.valueOf(strNumber.substring(0, 1));
			int ten = Integer.valueOf(strNumber.substring(1, 2));
			String strUnit = strNumber.substring(1, 3);
			StringBuilder sbResult = new StringBuilder(getNumberText(hundred)).append(" ").append(strHundred);
			if (Integer.valueOf(strUnit) > 0) {
				if (ten == 0) {
					String strSingle = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "single");
					sbResult.append(" ").append(strSingle).append(" ").append(getNumberText(Integer.valueOf(strNumber.substring(2, 3))));
				} else {
					sbResult.append(" ").append(convertTwoNummericToString(strUnit)).toString();
				}
			}
			result = sbResult.toString();
		}
		return result;
	}

	private static String convertTwoNummericToString(String strNumber) {
		String result = "";
		String strOnes = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "one.s");
		String strFives = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "five.s");
		String strTens = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ten.low");
		String strTen = Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "ten");
		if (Integer.valueOf(strNumber) == 0) {
			return "";
		}
		int ten = Integer.valueOf(strNumber.substring(0, 1));
		int unit = Integer.valueOf(strNumber.substring(1, 2));
		if (ten == 0) {
			result = getNumberText(ten);
		}
		if (ten == 1) {
			result = strTen;
		} else {
			result = new StringBuilder(getNumberText(ten)).append(" ").append(strTens).toString();
		}
		if (ten != 1 && unit == 1) {
			result = new StringBuilder(result).append(" ").append(strOnes).toString();
		} else if (ten != 0 && unit == 5) {
			result = new StringBuilder(result).append(" ").append(strFives).toString();
		} else if (unit > 0) {
			result = new StringBuilder(result).append(" ").append(getNumberText(unit)).toString();
		}
		return result;
	}

	private static String getNumberText(int number) {
		switch (number) {
		case 0:
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "zero");
		case 1:
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "one");
		case 2:
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "two");
		case 3:
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "three");
		case 4:
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "four");
		case 5:
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "five");
		case 6:
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "six");
		case 7:
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "seven");
		case 8:
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "eight");
		case 9:
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "nine");
		default:
			return "";
		}
	}

	public static String toStringNumberMMYYY(String str) {
		String strMM = str.split("/")[0];
		String strYYYY = str.split("/")[1];
		strMM = Integer.valueOf(strMM) < 10 ? "0" + strMM : strMM;
		return strMM + "/" + strYYYY;
	}
	 
	/**
	 * Convert money
	 * 
	 * @author hunglm16
	 * @param money
	 * @return 10000 -> 10,000
	 * @since 03/09/2015
	 */
	public static String convertMoney(BigDecimal money) {
		if (money == null) {
			return "0";
		}
		String result = "";
		String _money = money.toBigInteger() + "";
		int isDot = 1;
		for (int i = _money.length(); i > 0; i--) {
			char ch = _money.charAt(i - 1);
			if (isDot == 3 && i != 1) {
				if (_money.charAt(i - 2) == '-') {
					result = ch + result;
					isDot = 0;
				} else {
					result = "," + ch + result;
					isDot = 0;
				}
			} else {
				result = ch + result;
			}
			isDot++;
		}
		return result;
	}
	
	/**
	* Sinh chuoi ma hoa password (code mobile server)
	* @author : BangHN 
	* @params : mat khau. Salt: ten dang nhap da lower. 
	* @since  : 1.0
	*/
		
	public static String generateHash(String input, String salt) throws NoSuchAlgorithmException,
	    UnsupportedEncodingException {
	// SHA or MD5
		MessageDigest md = MessageDigest.getInstance("MD5");
		String hash = "";
		if (null == salt || "".equals(salt)) {
		    salt = "";
		}
		input += salt;
		byte[] data = input.getBytes("US-ASCII");
		
		md.update(data);
		byte[] digest = md.digest();
		for (int i = 0; i < digest.length; i++) {
		    String hex = Integer.toHexString(digest[i]);
		    if (hex.length() == 1)
		          hex = "0" + hex;
		    hex = hex.substring(hex.length() - 2);
		    hash += hex;
		}
		return hash;
	}
	
	/**
	 * FLOAT lam tron so thap phan
	 * 
	 * @author hunglm16
	 * @return float
	 * @param decimalPlace, value
	 * @description Truyen vao mot bien Float roi lam tron thap phan theo tham so decimalPlace
	 * */
	public static Float precisionFloat(int decimalPlace, Float value) {
		///Vi du
		//value = 29.294998;  decimalPlace = 2
		//return 29.30 
		BigDecimal bd = new BigDecimal(Float.toString(value));
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
		return bd.floatValue();
	}
	
	/**
	 *  BigDecimal Lam tron so thap phan 
	 * 
	 * @author hunglm16
	 * @return float
	 * @param decimalPlace, value
	 * @description Truyen vao mot bien BigDecimal roi lam tron thap phan theo tham so decimalPlace
	 * */
	public static BigDecimal precisionBigDecimal(int decimalPlace, BigDecimal value) {
		/// Vi du
		// value= 10.57125; decimalPlace = 0
		//return 11
		return value.setScale(decimalPlace, RoundingMode.HALF_UP);
	}
	
	public static boolean isFloat(String number){
	    try {
	       return !new Float(number).isNaN();
	    } catch (NumberFormatException e){
	        return false;
	    }
	}
	
	public static boolean isNumberInt(String number){
	    try {
	       return Long.valueOf(number.toString())!=null?true:false;
	    } catch (NumberFormatException e){
	        return false;
	    }
	}
	/**
	 * @author vuongmq
	 * @param formatImage
	 * @return
	 * Lay cac dinh dang duoi file support trong app-setting.xml; vd: Configuration.getVideoUploadFileSupport();
	 */
	public static String getFormatSupport(String formatImage) {
		String value = formatImage;
		int begin = -1;
		int end = -1;
		StringBuilder str = new StringBuilder();
		for(int i = 0; i< value.length(); i++){
			String gt =  Character.toString(value.charAt(i));
			if("/".equals(gt)){
				begin = i;
			}
			if("|".equals(gt)){
				end = i;
			}
			if(begin != -1 && end != -1){
				String view = value.substring(begin+ 1,end);
				str.append(" .").append(view.trim()).append(";");
				begin = -1;
				end = -1;
			}
		}
		if(begin != -1 && end == -1){
			String view = value.substring(begin+ 1,value.length());
			str.append(" .").append(view.trim());
		}
//		System.out.println("Ho tro upload images app-setting.xml:"+ str);
	    return str.toString();
	}
	
	/**
	 * Lay ten bao cao theo chuan CSV
	 * 
	 * @author hunglm16
	 * @param name
	 * @param fDate
	 * @param tDate
	 * @param fileExtension
	 * @return
	 * @since 26/09/2015
	 * @description Quy hoach code 
	 */
	public static String getReportNameFormat(String name, String fDate, String tDate, String fileExtension) {
		String nameRpt = "Viettel_";
		if (fDate.isEmpty() && tDate.isEmpty()) {
			nameRpt = nameRpt + name;
		} else {
			Date fD = DateUtil.parse(fDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			Date tD = DateUtil.parse(tDate, DateUtil.DATE_FORMAT_DDMMYYYY);
			fDate = DateUtil.toDateString(fD, DateUtil.DATE_FORMAT_CSV);
			tDate = DateUtil.toDateString(tD, DateUtil.DATE_FORMAT_CSV);
			nameRpt = nameRpt + name + "_" + fDate + "-" + tDate;
		}
		Random rand = new Random();
		Integer n = rand.nextInt(100) + 1;
		return nameRpt + "_" + n.toString() + fileExtension;
	}
	
	/**
	 * Kiem tra tinh hop le voi cac phan tu khac null
	 * 
	 * @author hunglm16
	 * @since May 04,2015
	 * @description thuong dung de check dong hop le cho file Excel
	 * */
	public static boolean isEmtyRow(List<String> row, Integer size) {
		if (row == null) {
			return true;
		}
		if (size == null) {
			for (String value : row) {
				if (!StringUtil.isNullOrEmpty(value)) {
					return false;
				}
			}
		} else {
			if (row.size() < size) {
				size = row.size(); 
			}
			for (int i = 0; i < size; i++) {
				if (!StringUtil.isNullOrEmpty(row.get(i))) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Kiem tra cac gia tri hop le doi voi dong
	 * 
	 * @author hunglm16
	 * @since May 04,2015
	 * */
	public static boolean isRowValidSingleIndexNotNull(List<String> row, Integer...indexs) {
		if (indexs == null) {
			return true;
		}
		for (Integer index:indexs) {
			if (index == null || index >= row.size()) {
				return true;
			}
			if (!StringUtil.isNullOrEmpty(row.get(index))) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Xu ly XSS cho cac kieu chuoi tra ve
	 * level 3
	 * @author hunglm16
	 * @param value
	 * @return
	 * @since 02/10/2015
	 */
	public static String escapeHtml3ByStringSingle (String value) {
		if (StringUtil.isNullOrEmpty(value)) {
			return "";
		}
		return StringEscapeUtils.escapeHtml3(value);
	}
	
	/**
	 * Xu ly XSS cho cac kieu chuoi tra ve
	 * level 3
	 * @param lstString
	 * @return
	 * @since 02/10/2015
	 */
	public static List<String> escapeHtml3ByListString(List<String> lstString) {
		List<String> res = new ArrayList<String>();
		if (lstString == null || lstString.isEmpty()) {
			return res;
		}
		for (String value : lstString) {
			if (StringUtil.isNullOrEmpty(value)) {
				res.add("");
			}
			res.add(StringEscapeUtils.escapeHtml3(value));
		}
		return res;
	}
	
	/**
	 * Xu ly XSS cho cac kieu chuoi tra ve
	 * level 4
	 * @author hunglm16
	 * @param value
	 * @return
	 * @since 02/10/2015
	 */
	public static String escapeHtml4ByStringSingle (String value) {
		if (StringUtil.isNullOrEmpty(value)) {
			return "";
		}
		return StringEscapeUtils.escapeHtml4(value);
	}
	
	/**
	 * Xu ly XSS cho cac kieu chuoi tra ve
	 * level 4
	 * @param lstString
	 * @return
	 * @since 02/10/2015
	 */
	public static List<String> escapeHtml4ByListString(List<String> lstString) {
		List<String> res = new ArrayList<String>();
		if (lstString == null || lstString.isEmpty()) {
			return res;
		}
		for (String value : lstString) {
			if (StringUtil.isNullOrEmpty(value)) {
				res.add("");
			}
			res.add(StringEscapeUtils.escapeHtml4(value));
		}
		return res;
	}
	
	/**
	 * Xu ly getDDMMYYYYStr
	 * @author vuongmq
	 * @param s
	 * @param index
	 * @return String
	 * @since Nov 30, 2015
	 */
	public static String getDDMMYYYYStr(final String s, Integer index) {
		String value = "";
		if (!isNullOrEmpty(s)) {
			String strTmp[] = s.split(";");
			if (strTmp != null && strTmp.length > 0) {
				if (index == null) {
					index = 0;
				}
				String valueDate = strTmp[index];
				if (!isNullOrEmpty(valueDate)) {
					Date date = DateUtil.parse(valueDate.trim(), DateUtil.DATE_FORMAT_ATTRIBUTE);
					value = DateUtil.toDateString(date, DateUtil.DATE_FORMAT_STR_PRO);
				}
			}
		}
		return value;
	}
	
	/**
	 * Tao ma khach hang
	 * 
	 * @author hunglm16
	 * @since August 17, 2015
	 */
	public static String createCustomerCode(String shortCode, String shopCode) {
		String customerCode = "";
		if (!StringUtil.isNullOrEmpty(shortCode) && !StringUtil.isNullOrEmpty(shopCode)) {
			customerCode = shortCode.trim().toUpperCase() + "_" + shopCode.trim().toUpperCase(); 
		} else if (!StringUtil.isNullOrEmpty(shortCode)) {
			customerCode = shortCode.trim().toUpperCase(); 
		} else if (!StringUtil.isNullOrEmpty(shopCode)) {
			customerCode = shopCode.trim().toUpperCase(); 
		}
		return customerCode.trim();
	}
}


package ths.dms.web.utils;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import ths.dms.core.entities.vo.LogInfoVO4J;
import ths.core.entities.vo.rpt.AppLogVO;
/*import ths.dms.core.entities.AppLog;
import ths.dms.core.entities.enumtype.AppLogTypeEnum;*/

import ths.dms.helper.Configuration;
import ths.dms.web.constant.ConstantManager;

public class LogUtility {
	private static Logger responseLog = Logger.getLogger("vinamilk.web.log.response");
	private static Logger serverErrorLog = Logger.getLogger("vinamilk.web.log.error.server");
	private static Logger clientErrorLog = Logger.getLogger("vinamilk.web.log.error.client");
	private static Logger logLogginAndLoguot = Logger.getLogger("vinamilk.web.log.login.and.loguot");
	private static Logger logAccess = Logger.getLogger("vinamilk.web.log.access");

	public static void logResponse(String info, String methodName, String className) {
		responseLog.trace(className + "," + methodName + "," + info);
	}

	/**
	 * Ghi log dang nhap (theo 20_CNTT_Quyet dinh ban hanh quy dinh ghi log full)
	 * 
	 * @author hunglm16
	 * @since August 06, 2015
	 * @description Dam bao day du 7 truong. vd: YYYY/MM/DD hh:mi:ss:ms| pp_code|username|ip_address|path|result|duration
	 * */
	public static void logLogginAndLoguot(Throwable e, String message, LogInfoVO4J logInfo) {
		if (logLogginAndLoguot.isEnabledFor(Level.INFO)) {
			StringBuilder sb = new StringBuilder();
			//Thoi gian dang nhap, dang xuat
			sb.append(logInfo.getStartTime()).append("||");
			/** strart_time */
			//Ma ung dung (CMS)
			sb.append(logInfo.getAppCode()).append("||");
			/** Ma ung dung */
			sb.append("N/A").append("||");
			/** thread_id */
			//Tai khoan dang nhap
			sb.append(logInfo.getLoginUserCode()).append("||");
			/** UserName */
			//Dia chi IP cua may tram
			sb.append(logInfo.getIpClientAddress()).append("||"); //getIpClientAddress
			//Duong danh truy cap
			sb.append(logInfo.getUri()).append("||");
			//Ket qua hoac hanh dong dang nhap thanh cong hoac khong thanh cong (result, action)
			sb.append(" ( ");
			sb.append("\"").append(toCSVCell(message)).append("\",");
			sb.append("\"");
			if (e != null) {
				sb.append(e.getMessage()).append("\n");
				StackTraceElement[] st = e.getStackTrace();
				for (int i = 0; i < st.length; i++) {
					sb.append(st[i]).append("\n");
				}
			}
			sb.append("\"");
			sb.append(" )");
			sb.append("||");
			//Khoang thoi gian thuc hien (Tu khi bat dau den ket thuc tinh theo mili-second)
			sb.append(logInfo.getStartForEndTimeMiliSecon());
			logLogginAndLoguot.info(sb.toString());
		}
	}
	
	/**
	 * Ghi Log ung dung he thong
	 * 
	 * @author hunglm16
	 * @param appLog
	 * @return
	 * @since August 27, 2015
	 */
	public static void logAccessStandard(AppLogVO appLog) {
		if (appLog != null && logAccess.isEnabledFor(Level.INFO)) {
			StringBuilder sb = new StringBuilder();
			//Ma Don Vi
			sb.append(appLog.getShopId()).append("||");
			//ID Nhan vien
			sb.append(appLog.getStaffId()).append("||");
			//ID Nhan vien thay the
			sb.append(appLog.getIngeritStaffId()).append("||");
			//Ngay ghi log
			sb.append(appLog.getLogDate()).append("||");
			//Dia chi IP
			sb.append(appLog.getIpAddress()).append("||");
			//Ma ung dung
			sb.append(Configuration.getDomainCode()).append("||");
			//URL Gui den server
			sb.append(appLog.getUrlRequest()).append("||");
			//Ngay Tao
			sb.append(appLog.getCreateDate()).append("||");
			//Nguoi tao
			sb.append(appLog.getCreateUser()).append("||");
			//Xu ly Log Home
			sb.append(appLog.getLogType()).append("||");
			//Bat dau giao dich (start_action)
			sb.append(DateUtil.convertDateByString(appLog.getLogDate(), DateUtil.DATE_FORMAT_NOW_MILLISECONDS)).append("||");
			//** start_time *//*
			//Ket thuc giao dich (end_action)
			sb.append(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_NOW_MILLISECONDS)).append("||");
			//** end_time *//*
			logAccess.error(sb.toString());
		}
	}
	
	/**
	 * Ghi log ngiep vu, chuc nang
	 * 
	 * @author hunglm16
	 * @since August 06, 2015
	 * */
	public static void logErrorStandard(Throwable e, String message, LogInfoVO4J logInfo) {
		if (serverErrorLog.isEnabledFor(Level.ERROR)) {
			StringBuilder sb = new StringBuilder();
			//Bat dau giao dich (start_action)
			sb.append(logInfo.getStartTime()).append("||");
			/** start_time */
			//Ket thuc giao dich (end_action)
			sb.append(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_NOW)).append("||");
			/** end_time */
			//Bat dau ket noi (Log ket noi den he thong khac nhu: DB, Core)
			//...
			//Ket thuc ket noi
			//...
			//Ma ung dung (app_code)
			sb.append(logInfo.getAppCode()).append("||");
			/** Ma ung dung */
			sb.append("N/A").append("||");
			/** thread_id */
			//Thoi gian bat dau thuc hien (start_time (YYYY/MM/DD hh:mi:ss:ms)
			sb.append(logInfo.getStartTime()).append("||");
			/** start_time */
			//Tai khoan tac dong (userName)
			sb.append(logInfo.getLoginUserCode()).append("||");
			/** id user login */
			//Dia chi Ip tac dong cua may tram (IP Address)
			sb.append(logInfo.getIpClientAddress()).append("||");
			/** ip_address */
			//Duong dan (path)
			//Chuc nang tac dong hoac URL_request trong giao dich (Function URL_request)
			sb.append(logInfo.getFunctionCode()).append("||");
			/** function_code */
			sb.append(logInfo.getFunctionType().getValue()).append("||");
			/** action_type */
			sb.append("N/A").append("||");
			/** object_id */
			//Cac tham so thuc thuc hien trong giao dich
			sb.append(logInfo.getDescription()).append("||");
			/** content */
			sb.append(" ( ");
			sb.append("\"").append(toCSVCell(message)).append("\",");
			sb.append("\"");
			if (e != null) {
				sb.append(e.getMessage()).append("\n");
				StackTraceElement[] st = e.getStackTrace();
				for (int i = 0, size = st.length; i < size; i++) {
					sb.append(st[i]).append("\n");
				}
			}
			sb.append("\"");
			sb.append(" )");
			serverErrorLog.error(sb.toString());
		}
	}
	
	/**
	 * Ham chong che cho ghi log loi he thong
	 * 
	 * @author hunglm16
	 * @since August 13, 2015
	 * @description Yeu cau thay the bang logErrorStandard
	 * */
	@Deprecated
	public static void logError(Throwable e, String message) {
		if (serverErrorLog.isEnabledFor(Level.ERROR)) {
			LogInfoVO4J logInfo = null;
			//Dam bao tinh du lieu dung dan cho tat ca cac file upload
			ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
			HttpServletRequest request = attr.getRequest();
			if (request != null && request.getSession() != null && request.getSession().getAttribute(ConstantManager.SESSION_LOG_ERROR_STANDARD) != null) {
				logInfo = (LogInfoVO4J) request.getSession().getAttribute(ConstantManager.SESSION_LOG_ERROR_STANDARD);
			}
			if (logInfo != null) {
				StringBuilder sb = new StringBuilder();
				//Bat dau giao dich (start_action)
				sb.append(logInfo.getStartTime()).append("||");
				/** start_time */
				//Ket thuc giao dich (end_action)
				sb.append(DateUtil.toDateString(DateUtil.now(), DateUtil.DATE_FORMAT_NOW)).append("||");
				/** end_time */
				//Bat dau ket noi (Log ket noi den he thong khac nhu: DB, Core)
				//...
				//Ket thuc ket noi
				//...
				//Ma ung dung (app_code)
				sb.append(logInfo.getAppCode()).append("||");
				/** Ma ung dung */
				sb.append("N/A").append("||");
				/** thread_id */
				//Thoi gian bat dau thuc hien (start_time (YYYY/MM/DD hh:mi:ss:ms)
				sb.append(logInfo.getStartTime()).append("||");
				/** start_time */
				//Tai khoan tac dong (userName)
				sb.append(logInfo.getLoginUserCode()).append("||");
				/** id user login */
				//Dia chi Ip tac dong cua may tram (IP Address)
				sb.append(logInfo.getIpClientAddress()).append("||");
				/** ip_address */
				//Duong dan (path)
				//Chuc nang tac dong hoac URL_request trong giao dich (Function URL_request)
				sb.append(logInfo.getFunctionCode()).append("||");
				/** function_code */
				sb.append(logInfo.getFunctionType().getValue()).append("||");
				/** action_type */
				sb.append("N/A").append("||");
				/** object_id */
				//Cac tham so thuc thuc hien trong giao dich
				sb.append(logInfo.getDescription()).append("||");
				/** content */
				sb.append(" ( ");
				sb.append("\"").append(toCSVCell(message)).append("\",");
				sb.append("\"");
				if (e != null) {
					sb.append(e.getMessage()).append("\n");
					StackTraceElement[] st = e.getStackTrace();
					for (int i = 0; i < st.length; i++) {
						sb.append(st[i]).append("\n");
					}
				}
				sb.append("\"");
				sb.append(" )");
				serverErrorLog.error(sb.toString());
			}
		}
	}

	public static void logMobileClientError(String platform, String model, String version, String errName, String description) {
		if (clientErrorLog.isEnabledFor(Level.ERROR)) {
			StringBuilder sb = new StringBuilder();
			sb.append("\"").append(platform).append("\",");
			sb.append("\"").append(model).append("\",");
			sb.append("\"").append(toCSVCell(errName)).append("\",");
			sb.append("\"").append(toCSVCell(description)).append("\",");
			sb.append("\"").append(new Date()).append("\",");
			sb.append("\"").append(version).append("\"");

			clientErrorLog.error(sb.toString());
		}
	}

	private static String toCSVCell(String input) {
		return input.replaceAll("\"", "\"\"");
	}
}

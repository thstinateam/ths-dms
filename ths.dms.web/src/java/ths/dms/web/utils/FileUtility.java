/*
 * Copyright 2015 Viettel ICT. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import ths.dms.helper.AppSetting;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.utils.report.excel.SXSSFReportHelper;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FileUtils;
import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.xml.sax.SAXException;

import ths.dms.core.entities.vo.FileVO;

/**
 * The Class FileUtility.
 */
public class FileUtility {

	public static final String _SEPARATOR = "/";
	public static final Integer portrait = 1;
	public static final Integer landscape = 2;
	public static final String validImageExtension = "valid_image_extension";
	public static final String validVideoExtension = "valid_video_extension";
	public static final String validExcelExtension = "valid_excel_extension";
	public static final String validDocumentExtension = "valid_document_extension";
	private static int MAX_FILE_EQUIP_UPLOAD_SIZE = Configuration.getMaxFileEquipUploadSize() * 1024 * 1024;

	/**
	 * Save buffered image to png file.
	 * 
	 * @param image
	 *            the image
	 * @param fileName
	 *            the file name
	 */
	public static void saveBufferedImageToPNGFile(BufferedImage image, String fileName) {
		File output = new File(fileName);
		if (!isFolderExist(getOnlyPathName(fileName))) {
			createDirectory(getOnlyPathName(fileName));
		}
		try {
			ImageIO.write(image, "png", output);
		} catch (IOException ex) {
			Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Save buffered image to gif file.
	 * 
	 * @param image
	 *            the image
	 * @param fileName
	 *            the file name
	 */
	public static void saveBufferedImageToGIFFile(BufferedImage image, String fileName) {
		File output = new File(fileName);
		try {
			ImageIO.write(image, "gif", output);
		} catch (IOException ex) {
			Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * Save buffered image to jpeg file.
	 * 
	 * @param image
	 *            the image
	 * @param fileName
	 *            the file name
	 */
	public static void saveBufferedImageToJPEGFile(BufferedImage image, String fileName) {
		File output = new File(fileName);
		try {
			ImageIO.write(image, "jpeg", output);
		} catch (IOException ex) {
			Logger.getLogger(FileUtility.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

    /**
     * Checks if is file exist.
     * 
     * @param fileName
     *            the file name
     * @return true, if is file exist
     */
    public static boolean isFileExist(String fileName) {
        File f = new File(fileName);
        return f.exists();
    }

    // get only path include last /
    /**
     * Gets the only path name.
     * 
     * @param path
     *            the path
     * @return the only path name
     */
    public static String getOnlyPathName(String path) {
        int l = Math.max(path.lastIndexOf("\\"), path.lastIndexOf("/") + 1);

        // String path
        return path.substring(0, l);
    }

    /**
     * Checks if is folder exist.
     * 
     * @param folderName
     *            the folder name
     * @return true, if is folder exist
     */
    public static boolean isFolderExist(String folderName) {
        File f = new File(folderName);
        if (f.exists()) {
            return true;
        } else {
            return false;
        }
    }

    // create Directory
    // return true if create successfully else return false

    /**
     * Creates the directory.
     * 
     * @param directoryName
     *            the directory name
     * @return true, if successful
     */
    public static boolean createDirectory(String directoryName) {

        // Create a directory; 
        File folder = new File(directoryName);
        boolean success = folder.mkdir();
        if (!success) {
            // Create directory and all ancestor directories
            success = folder.mkdirs();
        }
        return success;
    }

    /**
     * Delete file.
     * 
     * @param fileName the file name
     * @return true, if successful
     */
    public static boolean deleteFile(String fileName) {
        File f = new File(fileName);
        return f.delete();

    }

    /**
     * Copy file.
     * @param fromFileName the from file name
     * @param toFileName the to file name
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void copyFile(String fromFileName, String toFileName) throws IOException {
        File fromFile = new File(fromFileName);
        File toFile = new File(toFileName);

        if (!fromFile.exists()) {
            throw new IOException("FileCopy: " + "no such source file: " + fromFileName);
        }
        if (!fromFile.isFile()) {
            throw new IOException("FileCopy: " + "can't copy directory: " + fromFileName);
        }
        if (!fromFile.canRead()) {
            throw new IOException("FileCopy: " + "source file is unreadable: " + fromFileName);
        }
        if (toFile.isDirectory()) {
            toFile = new File(toFile, fromFile.getName());
        }
        if (toFile.exists()) {
            if (!toFile.canWrite()) {
                throw new IOException("FileCopy: " + "destination file is unwriteable: " + toFileName);
            }
            return;
        } else {
            String parent = toFile.getParent();
            if (parent == null) {
                parent = System.getProperty("user.dir");
            }
            File dir = new File(parent);
            if (!dir.exists()) {
            	dir.mkdir();
//                throw new IOException("FileCopy: "
//                        + "destination directory doesn't exist: " + parent);
            }
            if (dir.isFile()) {
                throw new IOException("FileCopy: " + "destination is not a directory: " + parent);
            }
            if (!dir.canWrite()) {
                throw new IOException("FileCopy: " + "destination directory is unwriteable: " + parent);
            }
        }

        FileInputStream from = null;
        FileOutputStream to = null;
        try {
            from = new FileInputStream(fromFile);
            to = new FileOutputStream(toFile);
            byte[] buffer = new byte[4096];
            int bytesRead;

            while ((bytesRead = from.read(buffer)) != -1) {
                to.write(buffer, 0, bytesRead); // write

            }
        } finally {
            if (from != null) {
                try {
                    from.close();
                } catch (IOException e) {
                    LogUtility.logError(e, e.getMessage());
                }
            }
            if (to != null) {
                try {
                    to.close();
                } catch (IOException e) {
                	LogUtility.logError(e, e.getMessage());
                }
            }
        }
    }

	/**
	 * Creates the file from data.
	 * 
	 * @param filename
	 *            the filename
	 * @param data
	 *            the data
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void createFileFromData(String filename, byte[] data) throws IOException {
		FileOutputStream fout = new FileOutputStream(filename);
		fout.write(data);
		fout.close();
	}

    /**
     * Gets the file name from path.
     * 
     * @param path
     *            the path
     * @return the file name from path
     */
    public static String getFileNameFromPath(String path) {

        String filename = path.substring(path.lastIndexOf("/") + 1);
        return filename;
    }

	/*
	 * Add number to filename when this file is existed
	 */
	/**
	 * Gets the non exist file name.
	 * 
	 * @param rootPath
	 *            the root path
	 * @param fileName
	 *            the file name
	 * @return the non exist file name
	 */
	public static String getNonExistFileName(String rootPath, String fileName) {
		int j = 0;
		String tmpFileName = fileName;
		String onlyName = tmpFileName.substring(0, tmpFileName.lastIndexOf("."));
		String ext = tmpFileName.substring(tmpFileName.lastIndexOf("."), tmpFileName.length());
		if (!isFileExist(rootPath + tmpFileName)) {
			return tmpFileName;
		}
		do {
			tmpFileName = onlyName + "(" + String.valueOf(j) + ")" + ext;
			j++;
		} while (isFileExist(rootPath + tmpFileName));
		return tmpFileName;
	}

	/**
	 * Gets the mime.
	 * 
	 * @param file
	 *            the file
	 * @return the mime
	 */
	public static String getMime(File file) {
		FileInputStream is = null;
		String mime = null;
		try {
			is = new FileInputStream(file);

			BodyContentHandler contenthandler = new BodyContentHandler();
			Metadata metadata = new Metadata();
			metadata.set(Metadata.RESOURCE_NAME_KEY, file.getName());
			AutoDetectParser parser = new AutoDetectParser();
			parser.parse(is, contenthandler, metadata);
			mime = metadata.get(Metadata.CONTENT_TYPE);
			return mime;
		} catch (IOException | SAXException | TikaException | AbstractMethodError e ) {
			return null;
		/*} catch (SAXException e) {
			return null;
		} catch (TikaException e) {
			return null;
		} catch (AbstractMethodError e) {
			e.printStackTrace();
			return null;*/
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e) {
				}
			}
		}
    }
    
    /**
     * Gets the safe file name.
     *
     * @param input the input
     * @return the safe file name
     */
    public static String getSafeFileName(String input) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c != '/' && c != '\\' && c != 0) {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    
    /**
     * Gets the excel type.
     *
     * @param excelContentType the excel content type
     * @return the excel type
     */
    public static int getExcelType(File excelFile){
	if(FileUtility.getMime(excelFile).startsWith("application/vnd.ms-excel")){
	    return ConstantManager.EXCEL_XLS_TYPE;
	} 
	return ConstantManager.EXCEL_XLSX_TYPE;
    }
    
    /**
     * Gets the excel data.
     *
     * @param excelFile the excel file
     * @param excelFileContentType the excel file content type
     * @return the excel data
     * @author hungtx
     * @since Oct 9, 2012
     */
    public static List<List<String>> getExcelData(File excelFile,Integer limitColumn, boolean hasHeader){
    	List<List<String>> resData = new ArrayList<List<String>>();    	
		try{
            Workbook myWorkBook = null;                 
            InputStream is = new FileInputStream(excelFile);
            if(!is.markSupported()) {
            	is = new PushbackInputStream(is, 8);
            }
            if(POIFSFileSystem.hasPOIFSHeader(is)) {
            	myWorkBook = new HSSFWorkbook(is);
            } else if(POIXMLDocument.hasOOXMLHeader(is)) {
            	myWorkBook = new XSSFWorkbook(OPCPackage.open(is));
            }
            if(myWorkBook!= null){
            	Sheet mySheet = null;
                mySheet = myWorkBook.getSheetAt(0);
                if(mySheet!= null){
                	Iterator<?> rowIter = mySheet.rowIterator();
                    while (rowIter.hasNext()) {
                        Row myRow = (Row)rowIter.next();
                        Iterator<Cell> cellIter = myRow.cellIterator();                    
                        List<String> lstRow = new ArrayList<String>();
                        Integer count = 0;
                        boolean hasEmptyRow = true;
                        int current = 0, next = 1;
                        while (cellIter.hasNext() && (limitColumn == null || (limitColumn!= null && count<limitColumn))) {
                            Cell myCell = cellIter.next();
                            current = myCell.getColumnIndex();                             
                            String value = "";
                            switch (myCell.getCellType()) {
							case Cell.CELL_TYPE_BLANK:
								value = "";
								break;
							case Cell.CELL_TYPE_NUMERIC:
								if(HSSFDateUtil.isCellDateFormatted(myCell)){
//									if(myCell.getCellStyle()!= null && DateUtil.HSSF_DATE_FORMAT_M_D_YY.equals(myCell.getCellStyle().getDataFormatString())){
//										value =  DateUtil.toDateString(myCell.getDateCellValue(), DateUtil.DATA_FORMAT_MM_DD_YYYY); 
//									} else{
										value = DateUtil.toDateString(myCell.getDateCellValue(), DateUtil.DATE_FORMAT_DDMMYYYY);
//									}									 
								} else if(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue())).compareTo(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue()).longValue())) == 0){
									value = String.valueOf(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue())).longValue());
								} else {
									value = String.valueOf(myCell.getNumericCellValue());
								}									
								break;
							case Cell.CELL_TYPE_STRING:
								value = myCell.getStringCellValue();
								break;								
							default:
								value = myCell.toString();
								break;
							}
							// add null value
							if (current >= next) {
								int loop = current - next;
								for (int k = 0; k < loop + 1; k++) {
									lstRow.add("");
									next = next + 1;
									count++;
								}
							}
							// add current cell
							lstRow.add(value.trim());
							next = next + 1;
							count++;
							if (!StringUtil.isNullOrEmpty(value.trim())) {
								hasEmptyRow = false;
							}

							// 
							//if(cellIter.hasNext()== false && (limitColumn == null || (limitColumn!= null && count<limitColumn))){
							if (cellIter.hasNext() == false && (limitColumn != null && count < limitColumn)) {
								for (int i = count; i < limitColumn; ++i) {
									lstRow.add("");
								}
							}
						}
						if (!hasEmptyRow) {
							resData.add(lstRow);
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		//remove header row
		if (resData.size() > 0 && !hasHeader) {
			resData.remove(0);
		}
		return resData;
	}

	/**
	 * lay du lieu bao gom dong trong
	 * 
	 * @param excelFile
	 *            the excel file
	 * @param excelFileContentType
	 *            the excel file content type
	 * @return the excel data
	 * @author phuongvm
	 * @since 18/12/2014
	 */
	public static List<List<String>> getExcelDataEx(File excelFile, Integer limitColumn, boolean hasHeader) {
		List<List<String>> resData = new ArrayList<List<String>>();
		try {
			Workbook myWorkBook = null;
			InputStream is = new FileInputStream(excelFile);
			if (!is.markSupported()) {
				is = new PushbackInputStream(is, 8);
			}
			if (POIFSFileSystem.hasPOIFSHeader(is)) {
				myWorkBook = new HSSFWorkbook(is);
			} else if (POIXMLDocument.hasOOXMLHeader(is)) {
				myWorkBook = new XSSFWorkbook(OPCPackage.open(is));
			}
			if (myWorkBook != null) {
				Sheet mySheet = null;
				mySheet = myWorkBook.getSheetAt(0);
				if (mySheet != null) {
					Iterator<?> rowIter = mySheet.rowIterator();
					int rownum = -1;
					while (rowIter.hasNext()) {
						Row myRow = (Row) rowIter.next();
						Iterator<Cell> cellIter = myRow.cellIterator();
						List<String> lstRow = new ArrayList<String>();
						if (myRow.getRowNum() != rownum) {
							if (rownum != -1) {
								List<String> lstColum = new ArrayList<String>();
								for (int i = 0; i < limitColumn; i++) {
									lstColum.add("");
								}
								int rowNum = myRow.getRowNum() - rownum;
								for (int j = 0; j < rowNum; j++) {
									resData.add(lstColum);
								}
							}
						}
						rownum = myRow.getRowNum() + 1;
						Integer count = 0;
						boolean hasEmptyRow = true;
						int current = 0, next = 1;
						while (cellIter.hasNext() && (limitColumn == null || (limitColumn != null && count < limitColumn))) {
							Cell myCell = cellIter.next();
							current = myCell.getColumnIndex();
							String value = "";
							switch (myCell.getCellType()) {
							case Cell.CELL_TYPE_BLANK:
								value = "";
								break;
							case Cell.CELL_TYPE_NUMERIC:
								if (HSSFDateUtil.isCellDateFormatted(myCell)) {
									if (myCell.getCellStyle() != null && DateUtil.HSSF_DATE_FORMAT_M_D_YY.equals(myCell.getCellStyle().getDataFormatString())) {
										value = DateUtil.toDateString(myCell.getDateCellValue(), DateUtil.DATA_FORMAT_MM_DD_YYYY);
									} else {
										value = DateUtil.toDateString(myCell.getDateCellValue(), DateUtil.DATE_FORMAT_DDMMYYYY);
									}
								} else if (BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue())).compareTo(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue()).longValue())) == 0) {
									value = String.valueOf(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue())).longValue());
								} else {
									value = String.valueOf(myCell.getNumericCellValue());
								}
								break;
							case Cell.CELL_TYPE_STRING:
								value = myCell.getStringCellValue();
								break;
							default:
								value = myCell.toString();
								break;
							}
							// add null value
							if (current >= next) {
								int loop = current - next;
								for (int k = 0; k < loop + 1; k++) {
									lstRow.add("");
									next = next + 1;
									count++;
								}
							}
							// add current cell
							lstRow.add(value.trim());
							next = next + 1;
							count++;
							if (!StringUtil.isNullOrEmpty(value.trim())) {
								hasEmptyRow = false;
							}

							// 
							if (cellIter.hasNext() == false && (limitColumn == null || (limitColumn != null && count < limitColumn))) {
								for (int i = count; i < limitColumn; ++i) {
									lstRow.add("");
								}
							}
						}
						//                        if(!hasEmptyRow){
						resData.add(lstRow);
						//                        }                            
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
		//remove header row
		if (resData.size() > 0 && !hasHeader) {
			resData.remove(0);
		}
		return resData;
	}

	/**
	 * Gets the excel data.
	 * 
	 * @param excelFile
	 *            the excel file
	 * @param excelFileContentType
	 *            the excel file content type
	 * @return the excel data
	 * @author nhanlt6
	 * @since Apr 25, 2014
	 */
	public static List<List<String>> getExcelDataForSOImport(File excelFile, Integer limitColumn, boolean hasHeader) {
		List<List<String>> resData = new ArrayList<List<String>>();
		try {
			Workbook myWorkBook = null;
			InputStream is = new FileInputStream(excelFile);
			if (!is.markSupported()) {
				is = new PushbackInputStream(is, 8);
			}
			if (POIFSFileSystem.hasPOIFSHeader(is)) {
				myWorkBook = new HSSFWorkbook(is);
			} else if (POIXMLDocument.hasOOXMLHeader(is)) {
				myWorkBook = new XSSFWorkbook(OPCPackage.open(is));
			}
			if (myWorkBook != null) {
				Sheet mySheet = null;
				mySheet = myWorkBook.getSheetAt(0);
				if (mySheet != null) {
					Iterator<?> rowIter = mySheet.rowIterator();
					while (rowIter.hasNext()) {
						Row myRow = (Row) rowIter.next();
						Iterator<Cell> cellIter = myRow.cellIterator();
						List<String> lstRow = new ArrayList<String>();
						Integer count = 0;
						boolean hasEmptyRow = true;
						int current = 0, next = 1;
						while (cellIter.hasNext() && (limitColumn == null || (limitColumn != null && count < limitColumn))) {
							Cell myCell = cellIter.next();
							current = myCell.getColumnIndex();
							String value = "";
							switch (myCell.getCellType()) {
							case Cell.CELL_TYPE_BLANK:
								value = "";
								break;
							case Cell.CELL_TYPE_NUMERIC:
								if (HSSFDateUtil.isCellDateFormatted(myCell)) {
									if (myCell.getCellStyle() != null && DateUtil.HSSF_DATE_FORMAT_M_D_YY.equals(myCell.getCellStyle().getDataFormatString())) {
										value = DateUtil.toDateString(myCell.getDateCellValue(), DateUtil.DATA_FORMAT_MM_DD_YYYY);
									} else {
										value = DateUtil.toDateString(myCell.getDateCellValue(), DateUtil.DATE_FORMAT_DDMMYYYY);
									}
								} else if (BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue())).compareTo(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue()).longValue())) == 0) {
									value = String.valueOf(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue())).longValue());
								} else {
									value = String.valueOf(myCell.getNumericCellValue());
								}
								break;
							case Cell.CELL_TYPE_STRING:
								value = myCell.getStringCellValue();
								break;
							default:
								value = myCell.toString();
								break;
							}
							// add null value
							if (current >= next) {
								int loop = current - next;
								for (int k = 0; k < loop + 1; k++) {
									lstRow.add("");
									next = next + 1;
									count++;
								}
							}
							// add current cell
							lstRow.add(value.trim());
							next = next + 1;
							count++;
							if (!StringUtil.isNullOrEmpty(value.trim())) {
								hasEmptyRow = false;
							}

							// 
							if (cellIter.hasNext() == false && (limitColumn == null || (limitColumn != null && count < limitColumn))) {
								for (int i = count; i < limitColumn; ++i) {
									lstRow.add("");
								}
							}
						}
						if (!hasEmptyRow) {
							resData.add(lstRow);
						}
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}    	
    	//remove header row
    	if(resData.size()>0 && !hasHeader){
    		resData.remove(0);
    		resData.remove(0);
    	}
    	return resData;
    }
    
    /**
     * Gets the excel data.
     *
     * @param excelFile the excel file
     * @param excelFileContentType the excel file content type
     * @return the excel data
     * @author hunglm16
     * @since April 14, 2012
     */
    public static List<List<String>> getExcelDataNew(File excelFile,Integer limitColumn, boolean hasHeader, int next, int remove){
    	if(next <=0){
    		next = 1;
    	}
    	List<List<String>> resData = new ArrayList<List<String>>();    	
		try{
            Workbook myWorkBook = null;                 
            InputStream is = new FileInputStream(excelFile);
            if(!is.markSupported()) {
            	is = new PushbackInputStream(is, 8);
            }
            if(POIFSFileSystem.hasPOIFSHeader(is)) {
            	myWorkBook = new HSSFWorkbook(is);
            } else if(POIXMLDocument.hasOOXMLHeader(is)) {
            	myWorkBook = new XSSFWorkbook(OPCPackage.open(is));
            }
            if(myWorkBook!= null){
            	Sheet mySheet = null;
                mySheet = myWorkBook.getSheetAt(0);
                if(mySheet!= null){
                	Iterator<?> rowIter = mySheet.rowIterator();
                    while (rowIter.hasNext()) {
                        Row myRow = (Row)rowIter.next();
                        Iterator<Cell> cellIter = myRow.cellIterator();                    
                        List<String> lstRow = new ArrayList<String>();
                        Integer count = 0;
                        boolean hasEmptyRow = true;
                        int current = 0;
                        while (cellIter.hasNext() && (limitColumn == null || (limitColumn!= null && count<limitColumn))) {
                            Cell myCell = cellIter.next();
                            current = myCell.getColumnIndex();                             
                            String value = "";
                            switch (myCell.getCellType()) {
							case Cell.CELL_TYPE_BLANK:
								value = "";
								break;
							case Cell.CELL_TYPE_NUMERIC:
								if(HSSFDateUtil.isCellDateFormatted(myCell)){
									if(myCell.getCellStyle()!= null && DateUtil.HSSF_DATE_FORMAT_M_D_YY.equals(myCell.getCellStyle().getDataFormatString())){
										value =  DateUtil.toDateString(myCell.getDateCellValue(), DateUtil.DATA_FORMAT_MM_DD_YYYY); 
									} else{
										value = DateUtil.toDateString(myCell.getDateCellValue(), DateUtil.DATE_FORMAT_DDMMYYYY);
									}									 
								} else if(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue())).compareTo(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue()).longValue())) == 0){
									value = String.valueOf(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue())).longValue());
								} else {
									value = String.valueOf(myCell.getNumericCellValue());
								}									
								break;
							case Cell.CELL_TYPE_STRING:
								value = myCell.getStringCellValue();
								break;								
							default:
								value = myCell.toString();
								break;
							}
                            // add null value
                            if(current >= next){
                            	int loop = current-next;
                            	for(int k=0;k<loop+1;k++){                                        
                                    lstRow.add("");
                                    next = next + 1;
                                    count++;
                                }
                            }
                            // add current cell
                        	lstRow.add(value.trim());
                            next = next + 1;
                            count++;                                                                                               
                            if(!StringUtil.isNullOrEmpty(value.trim())){
                            	hasEmptyRow = false;
                            }
                            
                            // 
                            if(cellIter.hasNext()== false && (limitColumn == null || (limitColumn!= null && count<limitColumn))){
                            	for(int i = count ; i< limitColumn ; ++i){
                            		lstRow.add("");
                            	}
                            }
                        }
                        if(!hasEmptyRow){
                        	resData.add(lstRow);
                        }                            
                    }
                }
            }    			
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}    	
    	//remove header row
    	if(resData.size()>0 && !hasHeader){
    		for(int i=0; i< remove; i++){
    			resData.remove(i);
    		}
    	}
    	return resData;
    } 
    
    public static List<List<String>> getExcelDataAndEmptyRow(File excelFile,Integer limitColumn){
    	List<List<String>> resData = new ArrayList<List<String>>();    	
		try{
            Workbook myWorkBook = null;                 
            InputStream is = new FileInputStream(excelFile);
            if(!is.markSupported()) {
            	is = new PushbackInputStream(is, 8);
            }
            if(POIFSFileSystem.hasPOIFSHeader(is)) {
            	myWorkBook = new HSSFWorkbook(is);
            } else if(POIXMLDocument.hasOOXMLHeader(is)) {
            	myWorkBook = new XSSFWorkbook(OPCPackage.open(is));
            }
            if(myWorkBook!= null){
            	Sheet mySheet = null;
                mySheet = myWorkBook.getSheetAt(0);
                if(mySheet!= null){
                	Iterator<?> rowIter = mySheet.rowIterator();
                    while (rowIter.hasNext()) {
                        Row myRow = (Row)rowIter.next();
                        Iterator<Cell> cellIter = myRow.cellIterator();                    
                        List<String> lstRow = new ArrayList<String>();
                        Integer count = 0;
                        int current = 0, next = 1;
                        while (cellIter.hasNext() && (limitColumn == null || (limitColumn!= null && count<limitColumn))) {
                            Cell myCell = cellIter.next();
                            current = myCell.getColumnIndex();                             
                            String value = "";
                            switch (myCell.getCellType()) {
							case Cell.CELL_TYPE_BLANK:
								value = "";
								break;
							case Cell.CELL_TYPE_NUMERIC:
								if(HSSFDateUtil.isCellDateFormatted(myCell)){
									if(myCell.getCellStyle()!= null && DateUtil.HSSF_DATE_FORMAT_M_D_YY.equals(myCell.getCellStyle().getDataFormatString())){
										value =  DateUtil.toDateString(myCell.getDateCellValue(), DateUtil.DATA_FORMAT_MM_DD_YYYY); 
									} else{
										value = DateUtil.toDateString(myCell.getDateCellValue(), DateUtil.DATE_FORMAT_DDMMYYYY);
									}									 
								} else if(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue())).compareTo(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue()).longValue())) == 0){
									value = String.valueOf(BigDecimal.valueOf(Double.valueOf(myCell.getNumericCellValue())).longValue());
								} else {
									value = String.valueOf(myCell.getNumericCellValue());
								}									
								break;
							case Cell.CELL_TYPE_STRING:
								value = myCell.getStringCellValue();
								break;								
							default:
								value = myCell.toString();
								break;
							}
                            // add null value
                            if(current >= next){
                            	int loop = current-next;
                            	for(int k=0;k<loop+1;k++){                                        
                                    lstRow.add("");
                                    next = next + 1;
                                    count++;
                                }
                            }
                            // add current cell
                        	lstRow.add(value.trim());
                            next = next + 1;
                            count++;
                            // 
                            if(cellIter.hasNext()== false && (limitColumn == null || (limitColumn!= null && count<limitColumn))){
                            	for(int i = count ; i< limitColumn ; ++i){
                            		lstRow.add("");
                            	}
                            }
                        }
                        resData.add(lstRow);                           
                    }
                }
            }    			
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}    	
    	//remove header row
    	if(resData.size()>0){
    		resData.remove(0);
    	}
    	if(resData.size()>0){
    		List<String> row = resData.get(0);
    		String header_column = "";
    		String cell = "";
    		String message = "";
    		if(row.size() >0){									
				cell = row.get(0).trim();
				header_column = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock.update.import.header.format.invalid.product.code").trim();
				if(cell.equalsIgnoreCase(header_column)){
					message = ConstantManager.ERR_FAIL_TEMPLATE;											
				}
			}
			if(row.size()  >1){									
				cell = row.get(1).trim();
				header_column = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock.update.import.header.format.invalid.lot").trim();
				if(cell.equalsIgnoreCase(header_column)){
					message = ConstantManager.ERR_FAIL_TEMPLATE;	
				}
			}
			if(row.size() >2){									
				cell = row.get(2).trim();
				header_column = Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"stock.update.import.header.format.invalid.quantity").trim();
				if(cell.equalsIgnoreCase(header_column)){
					message = ConstantManager.ERR_FAIL_TEMPLATE;	
				}
			}
			if(StringUtil.isNullOrEmpty(message)){
				resData.remove(1);
			}
    	}
    	return resData;
    } 
	public static void compress(List<File> sourceFiles, String zipFile,String zipDestination) throws Exception   {
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		try {
			String zipFilePath = zipDestination.concat("/").concat(zipFile);
			fos = new FileOutputStream(zipFilePath);
			zos = new ZipOutputStream(fos);

			byte[] buffer = new byte[1024];
			for (File f : sourceFiles) {
				FileInputStream fis = new FileInputStream(f);
				zos.putNextEntry(new ZipEntry(f.getName()));
				int len;
				while ((len = fis.read(buffer)) > 0) {
					zos.write(buffer, 0, len);
				}
				zos.closeEntry();
				fis.close();
			}
			
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			throw new Exception(e);
		}
		finally {
			IOUtils.closeQuietly(zos);
			IOUtils.closeQuietly(fos);
		}
	}
	
	public static List<File> decompress(File sourceFiles,  String outputFolder, String fileName) throws Exception{
		List<File> listFile = new ArrayList<File>();
		FileOutputStream fos = null;
		ZipInputStream zis = null;
		try{
			zis = new ZipInputStream(new FileInputStream(sourceFiles));
			byte[] buffer = new byte[1024];
			ZipEntry ze = zis.getNextEntry();
	    	while(ze!=null){
	    		File newFile = null;
	    		if(fileName == null){
	    			newFile = new File(outputFolder + File.separator + ze.getName());
	    		}else{
	    			String fileType = ze.getName().substring(ze.getName().lastIndexOf('.'));
	    			newFile = new File(outputFolder + File.separator + fileName+ fileType);
	    		}
	            new File(newFile.getParent()).mkdirs();
	            if(!newFile.createNewFile()){
	            	newFile.delete();
	            	newFile.createNewFile();
	            }
	            fos = new FileOutputStream(newFile);             
	 
	            int len;
	            while ((len = zis.read(buffer)) > 0) {
	            	fos.write(buffer, 0, len);
	            }
	            
	            listFile.add(newFile);
	            fos.close();   
	            ze = zis.getNextEntry();
	    	}
	    	zis.closeEntry();
	    	zis.close();
		} catch(Exception e) {
			throw e;
		}
		finally {
			if (fos != null) {
				fos.close();
			}
			if (zis != null) {
				zis.closeEntry();
		    	zis.close();
			}
		}
		return listFile;
	}
	/***
	 * @author vuongmq
	 * @param sourceFiles
	 * @param fTypeUser
	 * @return
	 * @throws Exception
	 * Giai nen file zip, va kiem tra co file nao khac file trong fTyperUser, Example fTyperUser = ".jrxml;.jasper;.xlsx" 
	 */
	public static Boolean decompressExistReport(File sourceFiles, String fTypeUser) throws Exception{
		ZipInputStream zis = null;
		Boolean flag = false;
		try {
			zis = new ZipInputStream(new FileInputStream(sourceFiles));
			ZipEntry ze = zis.getNextEntry();
			if(ze!=null) { // kiêm tra de lay ten
				String nameZe =	ze.getName().toString().trim().substring(ze.getName().length()-1);
				if (!nameZe.equals("/")) {
					while(ze!=null){
			    		// KIEM TRA DUOI  fileType co ton tai trong fTypeUser can su dung
			    		String fileType = ze.getName().substring(ze.getName().lastIndexOf('.'));
			    		//Boolean co = false;
			    		Boolean co = fTypeUser.contains(fileType); // neu fileType co trong fTypeUser thi true, ngc lai false
			    		if(co== false) {
			    			flag = true;
			    		}
			    		ze = zis.getNextEntry();
			    	}
			    	// khong ton tai fileType nao ngoai file can su dung nen giai nen ra
			    	// flase: khong ton tai file nao khac  fTypeUser can su dung, true: ton tai file khac fTypeUser
				} else {
					// la loi vi la thu muc
					return true;
				}
			} else {
				// la loi vi la thu muc
				return true;
			}
		} catch(Exception e) {
			throw e;
		}
		finally {
			if(zis != null){
				zis.closeEntry();
		    	zis.close();
			}
		}
		
		return flag;
	}
	/**
	 * @author vuongmq
	 * @param file
	 * @param outputFolder
	 * @param nameReport
	 * @return
	 * tao file .jrxml trên server
	 */
	public static String createFileReport(File file, String outputFolder, String nameReport) {
		try {
			File newFile = new File(outputFolder + File.separator + nameReport);
			new File(newFile.getParent()).mkdirs();
	        if(!newFile.createNewFile()){
	         	newFile.delete();
	         	newFile.createNewFile();
	        }
	        if (newFile.exists()) {
		        FileInputStream from = null;
		        FileOutputStream to = null;
		        from = new FileInputStream(file);
		        to = new FileOutputStream(newFile);
		        byte[] buffer = new byte[2048];
		        int bytesRead;
		
		        while ((bytesRead = from.read(buffer)) > 0) {
		            to.write(buffer, 0, bytesRead); // write
		        }
		        from.close();
		        to.close();
			}else {
				return "";
			}
		 } catch (Exception e) {
			 LogUtility.logError(e, "createFileReport - " + e.getMessage());
		 }
		 return nameReport; // ten file khong duoi
	}
	/**
	 * @author vuongmq
	 * @param strFileJrxml
	 * @param outputFolder
	 * @param nameJrxml
	 * @return
	 * compiler jasper report trong java: .jrxml -> .jasper
	 */
	public static Boolean compilerJasperReportInJava( String strFileJrxml, String outputFolder,  String nameJrxml) {
		try {
			File jasper = new File(strFileJrxml);
			if(jasper.exists()){
				int idx = nameJrxml.lastIndexOf(".jrxml");
				nameJrxml = nameJrxml.substring(0,idx);
				String nameJasper = nameJrxml + ".jasper";
				String strFileJasper = outputFolder + nameJasper;
				JasperCompileManager.compileReportToFile(strFileJrxml, strFileJasper);
			} else {
				return false;
			}
		} catch (JRException e) {
			LogUtility.logError(e, "compilerJasperReportInJava - " + e.getMessage());
		}
		return true;
	}
	public static void zipFolder(String srcFolder, String destZipFile) throws Exception {
		ZipOutputStream zip = null;
		FileOutputStream fileWriter = null;
		try{	
			fileWriter = new FileOutputStream(destZipFile);
			zip = new ZipOutputStream(fileWriter);
			addFolderToZip("", srcFolder, zip);
			zip.flush();
			zip.close();
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}
	
	private static  void addFolderToZip(String path, String srcFolder,ZipOutputStream zip) throws Exception {
		File folder = new File(srcFolder);
		for (String fileName : folder.list()) {
			if (path.equals("")) {
				addFileToZip(folder.getName(), srcFolder + "/" + fileName, zip);
			} else {
				addFileToZip(path + "/" + folder.getName(), srcFolder + "/"+ fileName, zip);
			}
		}
	}
	
	private static void addFileToZip(String path, String srcFile,ZipOutputStream zip) throws Exception {
		File folder = new File(srcFile);
		if (folder.isDirectory()) {
			addFolderToZip(path, srcFile, zip);
		} else {
			byte[] buf = new byte[1024];
			int len;
			FileInputStream in = new FileInputStream(srcFile);
			zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
			while ((len = in.read(buf)) > 0) {
				zip.write(buf, 0, len);
			}
			IOUtils.closeQuietly(in);
		}
	}
	
	public static String encodeFileToBase64Binary(String fileName) throws IOException {
		File file = new File(fileName);
		byte[] bytes = loadFile(file);
		byte[] encoded = Base64.encodeBase64(bytes);
		String encodedString = new String(encoded);

		return encodedString;
	}
	
	private static byte[] loadFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		long length = file.length();
		if (length > Integer.MAX_VALUE) {
			// File is too large
		}
		byte[] bytes = new byte[(int) length];
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
				&& (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		if (offset < bytes.length) {
			IOUtils.closeQuietly(is);
			throw new IOException("Could not completely read file " + file.getName());
		}

		is.close();
		return bytes;
	}
	/**
	 * store file to disk
	 * 
	 * @author hunglm16
	 * @param receivedFiles
	 *            received files
	 * @param filesName
	 *            received files name
	 * @return
	 * @date 15/11/2014
	 */
	public static List<FileVO> storeFileOnDisk(List<File> receivedFiles, List<String> lstFileFileName, List<String> lstFileContentType, String prefix, Integer type) {
		List<FileVO> fileItems = new ArrayList<FileVO>();
		try {
			if (receivedFiles != null && receivedFiles.size() > 0) {
				//Thu muc goc chua file upload cua thiet bi
				String rootPath = Configuration.getFileEquipServerDocPath();

				String folder = rootPath;
				Calendar cal = Calendar.getInstance();

				String url = FileUtility.createDetailUploadPathURL(prefix, cal, type);

				for (int i = 0, size = receivedFiles.size(); i < size; i++) {
					FileVO voFile = new FileVO();
					voFile.setFileName(lstFileFileName.get(i));
					//String fileType = lstFileContentType.get(i);
					fileItems.add(voFile);
					String urlFile = url + (folder.endsWith("/") ? "" : "/") + StringUtil.generateHash(Long.toString(cal.getTimeInMillis()), Configuration.getPrefixFileEquipServerDocPath()) + lstFileFileName.get(i);
					voFile.setUrl(urlFile);
					String newFilePath = folder + urlFile;
					File receivedFile = receivedFiles.get(i);
					File newFile = new File(newFilePath);
					try {
						FileUtils.copyFile(receivedFile, newFile);
					} catch (IOException ioe) {
						LogUtility.logError(ioe, "FileUtility.storeFileOnDisk()" + ioe.getMessage());
					}
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ImagesAction.storeImageOnDisk()" + e.getMessage());
		}
		return fileItems;
	}
	/**
	 * store file to disk
	 * 
	 * @author hunglm16
	 * @param receivedFiles
	 *            received files
	 * @param filesName
	 *            received files name
	 * @return
	 * @date 15/11/2014
	 */
	public static void deleteStoreFileOnDisk(List<String> lstFileFileName, String rootPath) {
		try {
			for (String pathFile : lstFileFileName) {
				pathFile = rootPath + pathFile;
				File newFile = new File(pathFile);
				if (!newFile.delete()) {
					LogUtility.logError(new Exception("deleteStoreFileOnDisk EquipAttachFile"), pathFile);
				}
			}
		} catch (Exception e) {
			LogUtility.logError(e, "ImagesAction.storeImageOnDisk()" + e.getMessage());
		}
	}

	/**
	 * Tao URL
	 * 
	 * @author hunglm16
	 * @since January 30,2015
	 * @param programId
	 * @param calendar
	 * @param type
	 * @param NDPId
	 * @return
	 */
	public static String createDetailUploadPathURL(String prefix, Calendar calendar, Integer type) {
		StringBuffer folder = new StringBuffer();
		folder.append(calendar.get(Calendar.YEAR));
		folder.append(FileUtility._SEPARATOR);
		folder.append(calendar.get(Calendar.MONTH) + 1);
		folder.append(FileUtility._SEPARATOR);
		folder.append(calendar.get(Calendar.DAY_OF_MONTH));
		folder.append(FileUtility._SEPARATOR);
		if (!StringUtil.isNullOrEmpty(prefix)) {
			folder.append(prefix);
		}
		if (type != null) {
			folder.append(type);
		}
		return folder.toString();
	}

	public static String createZipFile(List<String> sourceFiles, List<String> contentFiles, String zipFile, String password) throws Exception{
		net.lingala.zip4j.io.ZipOutputStream os = null;
		try {
			os = new net.lingala.zip4j.io.ZipOutputStream(new FileOutputStream(new File(zipFile)));
			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			
			//set level compress file
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_ULTRA);
			
			if(!StringUtil.isNullOrEmpty(password)) {
				parameters.setEncryptFiles(true);
				parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
				parameters.setPassword(password);
			}
			
			for(int i = 0; i < sourceFiles.size(); i++) {
				String sourceFile = sourceFiles.get(i);
				File f = File.createTempFile(sourceFile.split("\\.")[0], "." + sourceFile.split("\\.")[1]);
				os.putNextEntry(f, parameters);
				
				String contentFile = contentFiles.get(i);
				byte[] __contentFile = contentFile.getBytes();
				
				os.write(__contentFile);
				
				os.closeEntry();
			}
			
			os.finish();
		} catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			throw e;
		} finally {
			if(os != null) {
				os.close();
			}
		}
		return zipFile;
	}
	
	/**
	 * Xu ly getAddFileExtension
	 * @author vuongmq
	 * @param fileNameConfig
	 * @return String
	 * @since Nov 26, 2015
	 */
	public static String getAddFileExtension(String fileNameConfig) {
		StringBuilder strBuilder = new StringBuilder();
		String [] fileName = fileNameConfig.split(",");
		for (int i = 0, sz = fileName.length; i < sz; i++) {
			String docFile = fileName[i];
			if (!StringUtil.isNullOrEmpty(docFile)) {
				if (i != sz - 1) {
					strBuilder.append("." + docFile.trim() + ",");
				} else {
					strBuilder.append("." + docFile.trim());
				}
			}
		}
		return strBuilder.toString();
	}
	
	/**
	 * Xu ly checkFileExtensionInWhiteListAttach
	 * @author vuongmq
	 * @param fileName
	 * @param fileExConfig
	 * @return boolean
	 * @since Nov 26, 2015
	 */
	public static boolean checkFileExInWhiteListAttach(String fileName, String fileExConfig) {
		String fileEx = FileUtility.getFileItemExtensionByFileName(fileName);
		boolean res = true;
		if (!StringUtil.isNullOrEmpty(fileExConfig)) {
			res = false;
			String [] fileExs = fileExConfig.split(",");
			for (int i = 0, sz = fileExs.length; i < sz; i++) {
				String docFile = fileExs[i];
				if (!StringUtil.isNullOrEmpty(fileEx) && !StringUtil.isNullOrEmpty(docFile) && fileEx.trim().equals(docFile.trim())) {
					res = true;
					break;
				}
			}
		}
		return res;
	}
	
	
	/**
	 * Lay duoi file theo File
	 * 
	 * @author hunglm16
	 * @param file Tap tin
	 * @return duoi file
	 * @since August 12,2015
	 * @description vi du: fileName = abc.png -> return png
	 */
	public static String getFileExtension(File file) {
		String fileName = file.getName();
		int lastIndex = fileName.lastIndexOf(".");
		if (lastIndex > 0) {
			return fileName.substring(lastIndex + 1);
		}
		return "";
	}
	
	/**
	 * Lay duoi file theo File
	 * 
	 * @author hunglm16
	 * @param file Tap tin
	 * @return duoi file
	 * @since August 12,2015
	 * @description vi du: fileName = abc.png -> return png
	 */
	public static String getFileItemExtension(FileItem file) {
		String fileName = file.getName();
		int lastIndex = fileName.lastIndexOf(".");
		if (lastIndex > 0) {
			return fileName.substring(lastIndex + 1);
		}
		return "";
	}
	
	/**
	 * Lay duoi file theo File theo File Name
	 * 
	 * @author hunglm16
	 * @param fileName Ten tap tin
	 * @return duoi file
	 * @since August 12,2015
	 * @description vi du: fileName = abc.png -> return png
	 */
	public static String getFileItemExtensionByFileName(String fileName) {
		if (StringUtil.isNullOrEmpty(fileName)) {
			fileName = "";
		}
		int lastIndex = fileName.lastIndexOf(".");
		if (lastIndex > 0) {
			return fileName.substring(lastIndex + 1);
		}
		return "";
	}
	
	/**
	 * Lay duoi file theo duong dan
	 * @author hunglm16
	 * @param url: la duong danh den tap tin
	 * @return duoi file
	 * @since August 12, 2015
	 */
	public static String getFileExtensionFromUrl(String url) {
		if (!StringUtil.isNullOrEmpty(url)) {
			int fragment = url.lastIndexOf('#');
			if (fragment > 0) {
				url = url.substring(0, fragment);
			}
			int query = url.lastIndexOf('?');
			if (query > 0) {
				url = url.substring(0, query);
			}
			int filenamePos = url.lastIndexOf('/');
			String filename = 0 <= filenamePos ? url.substring(filenamePos + 1) : url;
			// if the filename contains special characters, we don't
			// consider it valid for our matching purposes:
			if (!filename.isEmpty() && Pattern.matches("[a-zA-Z_0-9\\.\\-\\(\\)\\%]+", filename)) {
				int dotPos = filename.lastIndexOf('.');
				if (0 <= dotPos) {
					return filename.substring(dotPos + 1);
				}
			}
		}
		return "";
	}

	/**
	 * Kiem tra duoi file voi cau hinh App-setting tuong ung
	 * 
	 * @author hunglm16
	 * @param extension chuoi can so sanh
	 * @param validExtension app-setting name tuong ung
	 * @return true Hop le
	 * @return false Khong hop le
	 * @since Ausgust 12, 2015
	 */
	public static boolean isValidExtension(String extension, String validExtension) {
		if (StringUtil.isNullOrEmpty(validExtension)) {
			return false;
		} 
		if (!FileUtility.validDocumentExtension.trim().equals(validExtension.trim()) && !FileUtility.validExcelExtension.trim().equals(validExtension.trim())
				&& !FileUtility.validImageExtension.trim().equals(validExtension.trim()) && !FileUtility.validVideoExtension.trim().equals(validExtension.trim())) {
			return false;
		}
		if (!StringUtil.isNullOrEmpty(extension)) {
			String validFileExtension = AppSetting.getStringValue(validExtension);
			extension = "-." + extension.toLowerCase() + "-";
			if (extension.contains("tmp")) {
				//Dam bao tinh du lieu dung dan cho tat ca cac file upload
				ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
				HttpServletRequest request = attr.getRequest();
				if (request != null) {
					if (request.getSession() != null && request.getSession().getAttribute(ConstantManager.SESSION_FILE_NAME) != null) {
						String[] fileNames = (String[]) request.getSession().getAttribute(ConstantManager.SESSION_FILE_NAME);
						if (fileNames != null && fileNames.length > 0) {
							for (String fileName: fileNames) {
								if (!StringUtil.isNullOrEmpty(validFileExtension) && !validFileExtension.toLowerCase().contains(FileUtility.getFileItemExtensionByFileName(fileName).toLowerCase().trim())) {
									return false;
								}
							}
							return true;
						}
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(validFileExtension) && validFileExtension.toLowerCase().contains(extension.toLowerCase().trim())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Kiem tra duoi file voi cau hinh App-setting tuong ung
	 * Chi can ton tai trong danh sach file la thoa man
	 * 
	 * @author hunglm16
	 * @param extension chuoi can so sanh
	 * @param validExtension app-setting name tuong ung
	 * @return true Hop le
	 * @return fala Khong hop le
	 * @since Ausgust 12, 2015
	 */
	public static boolean isValidExtensionInListName(String extension, String validExtension) {
		if (StringUtil.isNullOrEmpty(validExtension)) {
			return false;
		} 
		if (!FileUtility.validDocumentExtension.trim().equals(validExtension.trim()) && !FileUtility.validExcelExtension.trim().equals(validExtension.trim())
				&& !FileUtility.validImageExtension.trim().equals(validExtension.trim()) && !FileUtility.validVideoExtension.trim().equals(validExtension.trim())) {
			return false;
		}
		if (!StringUtil.isNullOrEmpty(extension)) {
			String validFileExtension = AppSetting.getStringValue(validExtension);
			extension = "-." + extension.toLowerCase() + "-";
			if (extension.contains("tmp")) {
				//Dam bao tinh du lieu dung dan cho tat ca cac file upload
				ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
				HttpServletRequest request = attr.getRequest();
				if (request != null) {
					if (request.getSession() != null && request.getSession().getAttribute(ConstantManager.SESSION_FILE_NAME) != null) {
						String[] fileNames = (String[]) request.getSession().getAttribute(ConstantManager.SESSION_FILE_NAME);
						if (fileNames != null && fileNames.length > 0) {
							for (String fileName: fileNames) {
								if (!StringUtil.isNullOrEmpty(validFileExtension) && validFileExtension.toLowerCase().contains(FileUtility.getFileItemExtensionByFileName(fileName).toLowerCase().trim())) {
									return true;
								}
							}
							return false;
						}
					}
				}
			}
			if (!StringUtil.isNullOrEmpty(validFileExtension) && validFileExtension.toLowerCase().contains(extension.toLowerCase().trim())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Ham fix thu muc tao file tam bao cao
	 * @author hunglm16
	 * @since Aug 10, 2015
	 */
	public static void createTempDir() {
		try {
			final String POI_TMP_FOLDER = "poifiles";
			String poiAbsolutePath = System.getProperty(ConstantManager.JAVA_IO_TMPDIR);
			if (poiAbsolutePath == null) {
				return;
			}
			Integer index = poiAbsolutePath.indexOf(POI_TMP_FOLDER);
			File dir = null;
			if (index != null && index > -1) {
				dir = new File(poiAbsolutePath);
			} else {
				dir = new File(poiAbsolutePath, POI_TMP_FOLDER);
			}
			if (dir != null && !dir.exists()) {
				dir.mkdir();
			}
		} catch (Exception e) {
			LogUtility.logError(e, "Error while creating tmp folder : " + e.getMessage());
		}
	}
	
	/**
	 * Kiem tra file hop le
	 * 
	 * @author hunglm16
	 * @since Feb 5, 2015
	 * */
	public static String checkEquipAttachFileReturnMsg(List<File> lstFile, List<String> lstFileName, List<String> lstFileContentType, Integer countFileInDB) {
		String msg = "";
		if (countFileInDB == null) {
			countFileInDB = 0;
		}
		Integer maxlength = Configuration.getMaxLengthListFileEquipUpload();
		if (maxlength == null) {
			maxlength = 5;
		}
		Integer maxCount = maxlength - countFileInDB;
		if (maxCount < 0) {
			maxCount = 0;
		}
		if (lstFile != null && !lstFile.isEmpty()) {
			if (lstFileContentType == null || lstFileContentType.size() == 0) {
				return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "file.upload.err.type");
			}
			if (countFileInDB == maxlength) {
				return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "file.upload.err.sum.size", String.valueOf(maxlength));
			}
			if (lstFile.size() > maxCount) {
				return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "file.upload.err.insert.size", String.valueOf(maxCount));
			}
			String fileTypeApp = Configuration.getEquipUploadFileSupport();
			if (StringUtil.isNullOrEmpty(fileTypeApp)) {
				return msg;
			}
			String[] arrfileType = fileTypeApp.toString().split(",");
			for (int i = 0, size = lstFile.size(); i < size; i++) {
				boolean flag = false;
				for (int j = 0, size1 = arrfileType.length; j < size1; j++) {
					String fileType = arrfileType[j].trim().toString();
					if (!StringUtil.isNullOrEmpty(lstFileContentType.get(i)) && fileType.trim().equals(lstFileContentType.get(i))) {
						if (!StringUtil.isNullOrEmpty(lstFileName.get(i))) {
							flag = true;
							break;
						}
					}
				}
				if (!flag) {
					if (!StringUtil.isNullOrEmpty(lstFileName.get(i))) {
						msg = lstFileName.get(i);
					}
					msg = "Định dạng tập tin " + msg + " không hợp lệ";
					return msg;
				}
				if (lstFile.get(i).length() > MAX_FILE_EQUIP_UPLOAD_SIZE) {
					if (!StringUtil.isNullOrEmpty(lstFileName.get(i))) {
						msg = lstFileName.get(i);
					}
					msg = "Tập tin " + msg + " lớn hơn " + Configuration.getMaxFileEquipUploadSize() + "Mb";
					return msg;
				}
			}
		}
		return msg;
	}
	
	/**
	 * Tao file bao cao zip gom nhieu file co san
	 * 
	 * @param lstPaths - duong dan den file can dua vao file nen
	 * @param fileName - ten file bao cao (khi xuat ra se gan them thoi gian vao phia sau + .zip)
	 * @param fileNameRes - chuoi key trong file resource, neu fileName null thi su dung de lay ten file bao cao trong resource
	 * 
	 * @author lacnv1
	 * @since Apr 07, 2015
	 */
	public static String createZipFromListFile(List<String> lstPaths, String fileName, String fileNameRes)
		throws Exception {
		List<File> lstFile = null;
		try {
			if (lstPaths == null || lstPaths.size() == 0) {
				throw new IllegalArgumentException("lstPaths: null or empty");
			}
			if (StringUtil.isNullOrEmpty(fileName) && StringUtil.isNullOrEmpty(fileNameRes)) {
				throw new IllegalArgumentException("fileName + fileNameRes: null");
			}
			if (StringUtil.isNullOrEmpty(fileName)) {
				fileName = R.getResource(fileNameRes);
			}
			fileName = SXSSFReportHelper.getReportFileName(fileName, FileExtension.ZIP.getValue());
			
			lstFile = new ArrayList<File>();
			File f = null;
			for (int i = 0; i < lstPaths.size(); i++) {
				f = new File(lstPaths.get(i));
				lstFile.add(f);
			}
			FileUtility.compress(lstFile, fileName, Configuration.getStoreRealPath());
			return Configuration.getExportExcelPath() + fileName;
		} catch (Exception ex) {
			throw ex;
		} finally {
			if (lstFile != null) {
				for (int i = 0; i < lstFile.size(); i++) {
					if (lstFile.get(i) != null && lstFile.get(i).exists()) {
						lstFile.get(i).delete();
					}
				}
			}
		}
	}
}

package ths.dms.web.utils;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import ths.dms.helper.Configuration;



/**
 * @author DuongMinhTu
 * 
 */
public class DateUtil {

	public static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";

	public static final String DATETIME_FORMAT_STR = "dd/MM/yyyy HH:mm";
	
	public static final String DATE_FORMAT_STR_PRO = "dd/MM/yyyy";
	
	public static final String DATETIME_FORMAT_STR_PRO = "HH:mm dd/MM/yyyy";
	
	public static final String DATETIME_FORMAT_STR_PRO1 = "HH:mm:ss";
	
	public static final String TIME_FORMAT_STR = "HH:mm";

	public static final String ENDTIME = "endTime";

	public static final String BEGINTIME = "beginTime";
	
	public static final String DATE_FORMAT_STOCKTRAN_EX = "yyMMdd";

	public static final String DEFAULT_DATE = "01/01/1980";
	
	public static final String DATE_FORMAT_NOW = "dd/MM/yyyy HH:mm:ss";
	public static final String DATE_FORMAT_NOW_MILLISECONDS = "dd/MM/yyyy HH:mm:ss.SSS";
	public static final String DATE_FORMAT_EXCEL_FILE = "yyyyMMddHHmmss";
	public static final String DATE_FORMAT_EXCEL_FILE_MILISC = "yyyyMMddHHmmssSSS";
	
	public static final String DATE_DD_MM = "dd/MM";
	public static final String DATE_DD_MM_YY = "dd/MM/yy";
	public static final String DATE_D_M = "d/M";
	public static final String DATE_M_Y = "MM/yyyy";
	public static final String HSSF_DATE_FORMAT_M_D_YY = "m/d/yy";
	public static final String DATA_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
	public static final String DATE_FORMAT_FILE_EXPORT = "yyyyMMddHHmmss";
	
	public static final String DATE_FORMAT_ATTRIBUTE = "yyyy-MM-dd";
	
	public static final String DATE_FORMAT_VISIT = "dd-MM-yyyy";
	public static final String DATE_FORMAT_CSV = "ddMMyyyy";
	
	public static final Integer DATE_YEAR = 2;
	public static final Integer DATE_MONTH = 1;
	
	public static final String DATE_FORMAT_STOCKTRAN = "yyyyMMdd";
	
	public static final int[] arrDays = {31,28,31,30,31,30,31,31,30,31,30,31};
	
	public static String toYearString(Date date) {
		String dateStr = "";
		try {
			dateStr = new SimpleDateFormat("yyyy").format(date);
		} catch (Exception e) {
		}
		return dateStr;
	}

	public static String toMonthDayString(Date date) {
		String dateStr = "";
		try {
			dateStr = new SimpleDateFormat("dd/MM").format(date);
		} catch (Exception e) {
		}
		return dateStr;
	}


	public static Date getCurrentGMTDate() {
		Date now = new Date();
		Calendar calendar = new GregorianCalendar();
		int offset = calendar.getTimeZone().getOffset(now.getTime());
		Date gmtDate = new Date(now.getTime() - offset);
		return gmtDate;
	}

	

	public static String toDateString(Date date, String format) {
		String dateString = "";
		if (date == null)
			return dateString;
		Object[] params = new Object[] { date };

		try {
			dateString = MessageFormat
					.format("{0,date," + format + "}", params);
		} catch (Exception e) {

		}
		return dateString;
	}

	/**
	 * In case hour section of date object is ending in the form of "00:00", we
	 * don't display it by using Date_format string instead. 17/01/2008 00:00
	 * --> 17/01/2008
	 * 
	 * @param date
	 *            : Date
	 * @return string form of date
	 */
	public static String toDateString(Date date) {
		String dateString = "";
		String format = null;
		if (date == null)
			return dateString;
		Object[] params = new Object[] { date };
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		if (cal.get(Calendar.HOUR_OF_DAY) == 0 && cal.get(Calendar.MINUTE) == 0) {
			format = DateUtil.DATE_FORMAT_DDMMYYYY;
		} else {
			format = DateUtil.DATETIME_FORMAT_STR_PRO;
		}
		try {
			dateString = MessageFormat
					.format("{0,date," + format + "}", params);
		} catch (Exception e) {

		}
		return dateString;
	}
	
	/**
	 * @author tungtt21
	 * @param date
	 * @return
	 */
	public static String toDateSimpleFormatString(Date date){
		String dateString = "";
		String format = null;
		if (date == null)
			return dateString;
		Object[] params = new Object[] { date };
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		format = DateUtil.DATE_FORMAT_DDMMYYYY;

		try {
			dateString = MessageFormat
					.format("{0,date," + format + "}", params);
		} catch (Exception e) {

		}
		return dateString;
	}

	public static Date toDate(String dateStr, String format) {
		Date date = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat(format);
			date = (Date) dateFormat.parse(dateStr);
		} catch (Exception e) {
		}
		return date;
	}

	public static Date toAnotherDate(String dateStr) {
		Date date = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat(
					DateUtil.DATETIME_FORMAT_STR);
			date = (Date) dateFormat.parse("31/01/2008 " + dateStr);
		} catch (Exception e) {
		}
		return date;
	}

	public static Date toDate(String dateStr, String format, String time) {
		Date date = null;
		Calendar calendar = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat(format);
			date = (Date) dateFormat.parse(dateStr);
			calendar = new GregorianCalendar();
			calendar.setTime(date);
			if (ENDTIME.equalsIgnoreCase(time)) {
				calendar.set(Calendar.HOUR_OF_DAY, 23);
				calendar.set(Calendar.MINUTE, 59);
				calendar.set(Calendar.SECOND, 59);
				calendar.set(Calendar.MILLISECOND, 999);

			} else if (BEGINTIME.equalsIgnoreCase(time)) {
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
			}
		} catch (Exception e) {
		}
		if (calendar == null) {
			return null;
		}
		return calendar.getTime();
	}

	public static Date toGMTDate(String dateStr, String format, String time,
			int timezoneInMin) {
		Date date = null;
		Calendar calendar = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat(format);
			date = (Date) dateFormat.parse(dateStr);
			calendar = new GregorianCalendar();
			calendar.setTime(date);
			if (ENDTIME.equalsIgnoreCase(time)) {
				calendar.set(Calendar.HOUR_OF_DAY, 23);
				calendar.set(Calendar.MINUTE, 59);
				calendar.set(Calendar.SECOND, 59);
				calendar.set(Calendar.MILLISECOND, 999);

			} else if (BEGINTIME.equalsIgnoreCase(time)) {
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				calendar.set(Calendar.SECOND, 0);
				calendar.set(Calendar.MILLISECOND, 0);
			}
		} catch (Exception e) {
		}
		if (calendar == null) {
			return null;
		}
		return convertToGMTDate(calendar.getTime(), timezoneInMin);
	}

	public static Date toGMTDate(String dateStr, int timezoneInMin) {
		Date date = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
			date = (Date) dateFormat.parse(dateStr);
			date = convertToGMTDate(date, timezoneInMin);
		} catch (Exception e) {
		}
		return date;
	}

	public static Date toGMTDate(String dateStr, int timezoneInMin,
			boolean isFullDate) {
		Date date = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
			date = (Date) dateFormat.parse(dateStr);
			if (isFullDate) {
				Date endOfDate = new Date(date.getTime() + 1000 * 60 * 60 * 24
						- 1);
				date = endOfDate;
			}

			date = convertToGMTDate(date, timezoneInMin);
		} catch (Exception e) {
		}
		return date;
	}	
	private static int toCalendarMonth(String month) {
		int monthReturn = Calendar.JANUARY;
		if (month == null) {
			return monthReturn;
		}
		if (month.equals("january")) {
			monthReturn = Calendar.JANUARY;
		} else if (month.equals("february")) {
			monthReturn = Calendar.FEBRUARY;
		} else if (month.equals("march")) {
			monthReturn = Calendar.MARCH;
		} else if (month.equals("april")) {
			monthReturn = Calendar.APRIL;
		} else if (month.equals("may")) {
			monthReturn = Calendar.MAY;
		} else if (month.equals("june")) {
			monthReturn = Calendar.JUNE;
		} else if (month.equals("july")) {
			monthReturn = Calendar.JULY;
		} else if (month.equals("august")) {
			monthReturn = Calendar.AUGUST;
		} else if (month.equals("september")) {
			monthReturn = Calendar.SEPTEMBER;
		} else if (month.equals("october")) {
			monthReturn = Calendar.OCTOBER;
		} else if (month.equals("november")) {
			monthReturn = Calendar.NOVEMBER;
		} else if (month.equals("december")) {
			monthReturn = Calendar.DECEMBER;
		}
		return monthReturn;
	}

	public static Date toGMTDateTime(String dateStr, int timezoneInMin) {
		Date date = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat(DATETIME_FORMAT_STR);
			date = (Date) dateFormat.parse(dateStr);
			date = convertToGMTDate(date, timezoneInMin);
		} catch (Exception e) {
			date = null;
		}
		return date;
	}

	public static String toServerDateString(Date date, int timezoneInMin) {
		String dateStr = "";
		try {
			date = convertToServerDate(date, timezoneInMin);
			dateStr = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY).format(date);
		} catch (Exception e) {
		}
		return dateStr;
	}

	public static String toServerDateTimeString(Date date, int timezoneInMin) {
		String dateStr = "";
		try {
			date = convertToServerDate(date, timezoneInMin);
			dateStr = new SimpleDateFormat(DATETIME_FORMAT_STR).format(date);
		} catch (Exception e) {
			dateStr = "";
		}
		return dateStr;
	}

	public static Date getDateFromDateHour(Date currentDate, int hours,
			int minutes, int second) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(currentDate);
		calendar.add(Calendar.HOUR_OF_DAY, hours);
		calendar.add(Calendar.MINUTE, minutes);
		calendar.add(Calendar.SECOND, second);
		return calendar.getTime();
	}
	
	public static Date getDateFromMonths(Date currentDate, int months) {
		// if week = 0, do nothing
		if (months == 0) {
			return currentDate;
		}
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(currentDate);
		calendar.add(Calendar.MONTH, months);
		return calendar.getTime();

	}
	
	public static Date getDateFromDates(Date date, int dayNumber) {

		if (dayNumber == 0) {
			return date;
		}
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, dayNumber);
		return calendar.getTime();
	}

	public static Date getShortDateFromDates(Date date, int dayNumber) {

		if (dayNumber == 0) {
			return date;
		}
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, dayNumber);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);

		return calendar.getTime();
	}
	
	public static Date getShortDate(Date date) {
		if (date == null) return null;
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);

		return calendar.getTime();
	}

	public static Date getDateFromWeeks(Date currentDate, int weeks) {
		// if week = 0, do nothing
		if (weeks == 0) {
			return currentDate;
		}
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(currentDate);
		calendar.add(Calendar.WEEK_OF_YEAR, weeks);
		return calendar.getTime();
	}

	public static Date convertToGMTDate(Date localDate, int timezoneInMin) {
		return convertDate(localDate, -timezoneInMin);
	}

	public static Date convertToServerDate(Date gmtDate, int timezoneInMin) {
		return convertDate(gmtDate, timezoneInMin);
	}

	/**
	 * Xu ly compareDateWithoutTimeFull date hh:mm:ss
	 * Return 0 if equals, 1 if date1 > date2, -1 if date1 < date2
	 * @author vuongmq
	 * @param date1
	 * @param date2
	 * @return int 
	 * @since Nov 26, 2015
	 */
	public static int compareDateWithoutTimeFull(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		if (cal1.equals(cal2)) {
			return 0;
		} else if (cal1.after(cal2)) {
			return 1;
		} else {
			return -1;
		}
	}
	
	/**
	 * Return 0 if equals, 1 if date1 > date2, -1 if date1 < date2
	 * 
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int compareDateWithoutTime(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		cal1.set(Calendar.MINUTE, 0);
		cal1.set(Calendar.HOUR_OF_DAY, 0);
		cal1.set(Calendar.SECOND, 0);
		cal1.set(Calendar.MILLISECOND, 0);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		cal2.set(Calendar.MINUTE, 0);
		cal2.set(Calendar.HOUR_OF_DAY, 0);
		cal2.set(Calendar.SECOND, 0);
		cal2.set(Calendar.MILLISECOND, 0);
		if (cal1.equals(cal2)) {
			return 0;
		} else if (cal1.after(cal2)) {
			return 1;
		} else {
			return -1;
		}
	}

	/**
	 * Cong ngay
	 * @param input
	 * @param days
	 * @return
	 */
	public static Date addDate(Date input, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(input);
		cal.add(Calendar.DATE, days);

		return cal.getTime();
	}
	
	
	/**
	 * Return 0 if equals, 1 if date1 > date2, -1 if date1 < date2
	 * @author hunglm16
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int checkByBetweenTwoDate(Date date1, Date date2) {
		// Định dạng thời gian
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
			Calendar c1 = Calendar.getInstance();
			Calendar c2 = Calendar.getInstance();
			// Định nghĩa 2 mốc thời gian ban đầu
			Date date1Teamp = sdf.parse(sdf.format(date1.getTime()));
			Date date2Teamp = sdf.parse(sdf.format(date2.getTime()));
			c1.setTime(date1Teamp);
			c2.setTime(date2Teamp);
			long kq = (c1.getTime().getTime() - c2.getTime().getTime())
					/ (24 * 3600 * 1000);
			if (kq > 0) {
				return 1;
			}
			if (kq < 0) {
				return -1;
			}
			return 0;
		} catch (Exception e) {
			return -2;
		}
	}
	
	private static Date convertDate(Date srcDate, int timezoneInMin) {
		if (srcDate == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(srcDate);
		calendar.add(Calendar.MINUTE, timezoneInMin);
		Date destDate = calendar.getTime();

		return destDate;
	}

	public static Date now() {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		try {
			return sdf.parse(sdf.format(cal.getTime()));
		} catch (ParseException e) {
			return new Date();
		}
	}
	
	public static Date parseLockDate(Date lockDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(lockDate);
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		try {
			return sdf.parse(sdf.format(cal.getTime()));
		} catch (ParseException e) {
			return new Date();
		}
	}
	
	public static Date now(String format) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.parse(sdf.format(cal.getTime()));
		} catch(ParseException e) {
			return new Date();
		}
	}

	/**
	 * Gets the month.
	 * 
	 * @param date
	 *            the date
	 * @return the month
	 */
	public static int getMonth(Date date) {
		String month = null;
		DateFormat f = new SimpleDateFormat("MM");
		try {
			month = f.format(date);
			return Integer.parseInt(month);
		} catch (Exception e) {
			return -1;
		}
	}
	
	public static int getSecond(Date date) {
		String month = null;
		DateFormat f = new SimpleDateFormat("ss");
		try {
			month = f.format(date);
			return Integer.parseInt(month);
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * Gets the year.
	 * 
	 * @param date
	 *            the date
	 * @return the year
	 */
	public static int getYear(Date date) {
		String year = null;
		DateFormat f = new SimpleDateFormat("yyyy");
		try {
			year = f.format(date);
			return Integer.parseInt(year);
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * Gets the day.
	 * 
	 * @param date
	 *            the date
	 * @return the day
	 */
	public static int getDay(Date date) {
		String day = null;
		DateFormat f = new SimpleDateFormat("dd");
		try {
			day = f.format(date);
			return Integer.parseInt(day);
		} catch (Exception e) {
			return -1;
		}
	}
	
	public static int getHour(Date date) {
		String hour = null;
		DateFormat f = new SimpleDateFormat("hh");
		try {
			hour = f.format(date);
			return Integer.parseInt(hour);
		} catch (Exception e) {
			return -1;
		}
	}
	
	public static int getHour24(Date date) {
		String hour = null;
		DateFormat f = new SimpleDateFormat("HH");
		try {
			hour = f.format(date);
			return Integer.parseInt(hour);
		} catch (Exception e) {
			return -1;
		}
	}
	
	public static int getMinute(Date date) {
		String minute = null;
		DateFormat f = new SimpleDateFormat("mm");
		try {
			minute = f.format(date);
			return Integer.parseInt(minute);
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * Parses the.
	 * 
	 * @param str
	 *            the str
	 * @param format
	 *            the format
	 * @return the date
	 */
	public static Date parse(String str, String format) {
		DateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.parse(str);
		} catch (Exception e) {
			return null;
		}
	}

	

	
	/**
	 * @param date
	 * @return
	 * @author tu.duong
	 */
	private static String getLocalizedWeekDay(Date date, Locale currentLocale) {
		SimpleDateFormat dtf = new SimpleDateFormat("EEEE", currentLocale);
		return dtf.format(date);
	}

	/**
	 * @param date
	 * @return
	 * @author tu.duong
	 */
	private static String getLocalizedMonth(Date date, Locale currentLocale) {
		SimpleDateFormat dtf = new SimpleDateFormat("MMMM", currentLocale);
		return dtf.format(date);
	}

	static public void displayPattern(String pattern, Locale currentLocale) {
		Date today;
		SimpleDateFormat formatter;
		String output;

		formatter = new SimpleDateFormat(pattern, currentLocale);
		today = new Date();
		output = formatter.format(today);

		System.out.println(pattern + "   " + output);
	}

	public static long dateDiff(Date from, Date to) {
	    Calendar cFrom = Calendar.getInstance();
	    Calendar cTo = Calendar.getInstance();
	    cFrom.setTime(from);
	    cTo.setTime(to);
	    long diff = Math.abs(cFrom.getTimeInMillis() - cTo.getTimeInMillis());
	    return diff / (24 * 60 * 60 * 1000);
    }
	
	public static long dateDiffMinute(Date from, Date to) {
	    Calendar cFrom = Calendar.getInstance();
	    Calendar cTo = Calendar.getInstance();
	    cFrom.setTime(from);
	    cTo.setTime(to);
	    long diff = Math.abs(cFrom.getTimeInMillis() - cTo.getTimeInMillis());
	    return diff / (60 * 1000);
    }
	
	static public void displayDate(Locale currentLocale) {

		Date today;
		String result;
		SimpleDateFormat formatter;

		formatter = new SimpleDateFormat("EEEE", currentLocale);
		today = new Date();
		result = formatter.format(today);

		System.out.println("Locale: " + currentLocale.toString());
		System.out.println("Result: " + result);
	}

	public static String addDays(int n){
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar date = new GregorianCalendar();  
		date.add(Calendar.DATE, n);
        return dateFormat.format(date.getTime());
	}
	/**
	 * Check invalid format date.
	 * @param value 
	 * @return boolean
	 */
	public static Boolean checkInvalidFormatDate(String value){
		boolean error = false;
		if(!StringUtil.isNullOrEmpty(value)){
			String[] arrDate = value.split("/");
			int day = 0;
			int month = 0;
			int year = 0;
			try{
				day = Integer.valueOf(arrDate[0]);
				month = Integer.valueOf(arrDate[1]);
				year = Integer.valueOf(arrDate[2]);
			}catch (Exception e) {
				error = true;
			}
			if(year <1000 || year >9999){
				error = true;
			}
			if(month >0 && month <=12){
				if(month == 2){
					if(day > 29 || (!(year%4 == 0 || (year%400 == 0 && year%100!=0)) && day>28)){
						error = true;
					}
				} else if(day > DateUtil.arrDays[month-1]){
					error = true;
				}
			} else {
				error = true;
			}
			if(day <= 0){
				error = true;
			}
			if(error == false && DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY) == null){
				error = true;
			}
		}
		return error;
	}
	public static Date moveDate(Date date,Integer val,Integer type){ //type = 1 Date,2: Month
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if(type==1){
			cal.add(Calendar.DATE, val);
		}else if(type==2){
			cal.add(Calendar.MONTH, val);
		}		
		Date d = cal.getTime();
		return d;
	}
	public static Date converLotToDate(String lot,String format){
		Integer nowYear = DateUtil.getYear(DateUtil.now());
		String dd = lot.substring(0,2);
		String MM = lot.substring(2,4);
		String yy = lot.substring(4);
		return DateUtil.parse(dd +"/"+MM +"/" + String.valueOf(nowYear).substring(0,2)+yy, format);
	}
	
	public static String convertDateByString(Date date, String format){
		SimpleDateFormat dt = new SimpleDateFormat(format);
		return dt.format(date);
	}
	public static boolean isDateFull(String date){
		String checkMessage = ValidateUtil.getErrorMsgForInvalidFormatDate(date, "Date", null);
		if(!StringUtil.isNullOrEmpty(checkMessage)){
			return false;
		}
		return true;
	}
	/**
	 * @author hunglm16
	 * */
	public static boolean isThisDateValidate(String dateToValidate, String dateFromat){
		if(dateToValidate == null){
			return false;
		}
		String dem[] =dateToValidate.split("/"); 
		if(dem.length!=3){
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
		Date date = null;
		try {
			//if not valid, it will throw ParseException
			date = sdf.parse(dateToValidate);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	public static boolean compareDate(Date fDate, Date tDate){
		String checkMessage = ValidateUtil.getErrorMsgForInvalidToDate(fDate, tDate);
		if (!StringUtil.isNullOrEmpty(checkMessage)) {
			return false;
		}
		return true;
	}		
	
	public static int getWorkingDateInMonth(Date date) {
		int numDate = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		Date fDate = DateUtil.parse(new StringBuilder().append(1).append("/").append(month+1).append("/").append(year).toString(), "d/M/yyyy");
		Calendar calFD = Calendar.getInstance();
		calFD.setTime(fDate);
		for(int i=1;i<=cal.get(Calendar.DAY_OF_MONTH);i++){			
			if(calFD.get(Calendar.DAY_OF_WEEK) != 1){
				numDate++;
			}
			calFD.add(Calendar.DAY_OF_MONTH, 1);
		}
		return numDate;
	}
	
	public static Date getLastDateInMonth(Date date){		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);		
		int lDay = arrDays[cal.get(Calendar.MONTH)];
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH); 
		if(month == 1 && (year%4==0)&&((year%100!=0)||(year%400==0))){
			lDay = 29;
		}
		return DateUtil.parse(new StringBuilder().append(lDay).append("/").append(month+1).append("/").append(year).toString(), "d/M/yyyy");
	}
	
	public static Date getFirstDateInMonth(Date date){		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);		
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH); 
		return DateUtil.parse(new StringBuilder().append("01").append("/").append(month+1).append("/").append(year).toString(), "d/M/yyyy");
	}

	public static Date get7AfterDate(Date date){
		Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(date.getTime() - 24L * 60 * 60 * 1000*7);
        Date dateTmp = calendar.getTime();
        return dateTmp;
	}
	
	public static Date getYesterday(Date date){
		Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(date.getTime() - 24L * 60 * 60 * 1000);
        Date dateTmp = calendar.getTime();
        return dateTmp;
	}
	
	public static Date getTomorrow(Date date){
		Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(date.getTime() + 24L * 60 * 60 * 1000);
        Date dateTmp = calendar.getTime();
        return dateTmp;
	}
	
	public static String getFirstDayOfWeek(int week, int year){
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.WEEK_OF_YEAR, week);
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		Date date = calendar.getTime();
		return toDateString(date, DateUtil.DATE_FORMAT_DDMMYYYY);
	}
	public static int getWeekOfYear(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.setTime(date);
		int week = calendar.get(Calendar.WEEK_OF_YEAR);
		return week;
	}
	
	public static String convertFormatAttFromStr (String value) {
		if(!StringUtil.isNullOrEmpty(value)) {
			Date dateTmp = DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY)	;
			return DateUtil.toDateString(dateTmp,DateUtil.DATE_FORMAT_ATTRIBUTE);
		}
		return "";
	}
	public static String convertFormatStrFromAtt (String value) {
		if(!StringUtil.isNullOrEmpty(value)) {
			Date dateTmp = DateUtil.parse(value, DateUtil.DATE_FORMAT_ATTRIBUTE)	;
			return DateUtil.toDateString(dateTmp,DateUtil.DATE_FORMAT_DDMMYYYY);
		}
		return "";
	}
	
	public static String convertDate2StringddMMMyy(Date convertDate){
		SimpleDateFormat ddMMMMyyFormat = new SimpleDateFormat("dd-MMM-yy", Locale.ENGLISH);
		String result = ddMMMMyyFormat.format(convertDate);
		return result;
	}
	
	public static boolean CheckSunDayInWeek(Date date){
		boolean flag = true;	
		Calendar cal = Calendar.getInstance();
        cal.setTime(date);
	    if(cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
	    	flag = true;
	    else
	    	flag = false;
		return flag;
	}
	
	public static int getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {
	    Calendar startCal;
	    Calendar endCal;
	    startCal = Calendar.getInstance();
	    startCal.setTime(startDate);
	    endCal = Calendar.getInstance();
	    endCal.setTime(endDate);
	    int workDays = 0;
	    //Return 0 if start and end are the same
	    if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
	        return 0;
	    }
	    if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
	        startCal.setTime(endDate);
	        endCal.setTime(startDate);
	    }
	    do {
	        startCal.add(Calendar.DAY_OF_MONTH, 1);
	        if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
	            ++workDays;
	        }
	    } while (startCal.getTimeInMillis() < endCal.getTimeInMillis());

	    return workDays;
	}
	// ham main
	public static void main(String[] args) {
		String value = Configuration.getVideoUploadFileSupport();
		System.out.println("sun0:"+ value.indexOf("/"));
		System.out.println("sun1:"+ value.indexOf("|"));
		System.out.println("sun2:"+ value);
		int begin = -1;
		int end = -1;
		StringBuilder str = new StringBuilder();
		for(int i = 0; i< value.length(); i++){
			String gt =  Character.toString(value.charAt(i));
			if("/".equals(gt)){
				//System.out.println("s11:"+ i);
				begin = i;
			}
			if("|".equals(gt)){
				//System.out.println("s22:"+ i);
				end = i;
			}
			if(begin != -1 && end != -1){
				String view = value.substring(begin+ 1,end);
				//System.out.println("CCCCCCCCCCCCCCCC:"+ view+" - "+ view.length());
				str.append(" .").append(view.trim()).append(";");
				begin = -1;
				end = -1;
			}
			/*if( i == (value.length() - 1) && begin != -1 && end == -1){
				String view = value.substring(begin+ 1,end);
				System.out.println("CCCCCCCCCCCCCCCC:"+ view);
			}*/
			//System.out.println("svvv:"+ gt);
			/*System.out.println("CCCCCCCCCCCCCCCCBEGIN:"+ begin);
			System.out.println("CCCCCCCCCCCCCCCCBEGIN:"+ end);*/
		}
		if(begin != -1 && end == -1){
			String view = value.substring(begin+ 1,value.length());
			//System.out.println("CCCCCCCCCCCCCCCC:"+ view+" - "+ view.length());
			str.append(" .").append(view.trim());
		}
		System.out.println("CCCCCCCCCCCCCCCC:"+ str);
		/*CCCCCCCCCCCCCCCC:quicktime  - 10
		CCCCCCCCCCCCCCCC:x-flv  - 6
		CCCCCCCCCCCCCCCC:x-msvideo  - 10
		CCCCCCCCCCCCCCCC:mpeg  - 5
		CCCCCCCCCCCCCCCC:mp4 - 3*/
		/*Integer currentYear = DateUtil.getYear(DateUtil.getCurrentGMTDate()) + 10;
		System.out.println("sun:"+ currentYear);*/
		/*Date x = DateUtil.parse("01/02/2015", "dd/MM/yyyy");
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(x);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek == Calendar.SUNDAY) { 
			System.out.println("sun");
		}
		Integer a1 = 1;
		if(Calendar.SUNDAY == a1){
			System.out.println("ds: bang ne te");
		}*/
		/*int a = Calendar.MONDAY;
		System.out.println("MONDAY:"+a);
		int TUESDAY = Calendar.TUESDAY;
		System.out.println("TUESDAY:"+TUESDAY);
		int WEDNESDAY = Calendar.WEDNESDAY;
		System.out.println("WEDNESDAY:"+WEDNESDAY);
		int THURSDAY = Calendar.THURSDAY;
		System.out.println("THURSDAY:"+THURSDAY);
		int FRIDAY = Calendar.FRIDAY;
		System.out.println("FRIDAY:"+FRIDAY);
		int SATURDAY = Calendar.SATURDAY;
		System.out.println("SATURDAY:"+SATURDAY);
		int SUNDAY = Calendar.SUNDAY;
		System.out.println("SUNDAY:"+SUNDAY);*/
 	}
	
	public static int getNumOfWeekdays(Date startDate, Date endDate) {
	    Calendar startCal;
	    Calendar endCal;
	    startCal = Calendar.getInstance();
	    startCal.setTime(startDate);
	    endCal = Calendar.getInstance();
	    endCal.setTime(endDate);
	    int holiDays = 0;
	    //Return 0 if start and end are the same
	    if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
	    	if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
	    		return 1;
	        }
	    	return 0;
	    }
	    if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
	        startCal.setTime(endDate);
	        endCal.setTime(startDate);
	    }
	    do {
	    	startCal.add(Calendar.DAY_OF_MONTH, 1);
	        if (startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
	            ++holiDays;
	        }
	    } while (startCal.getTimeInMillis() < endCal.getTimeInMillis());

	    return holiDays;
	}
	
	public static Boolean check2MonthAfter(String fromDate) {
		Date fDate = DateUtil.parse(fromDate, DateUtil.DATE_FORMAT_DDMMYYYY);
		if(fDate == null) return false;
		Date checkDate = DateUtil.now();
		Calendar cal = Calendar.getInstance();
		cal.setTime(checkDate);	
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH); 
		if(month > 1) {
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.DATE, 1);
			cal.set(Calendar.MONTH, month - 2);
		} else {
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.DATE, 1);
			cal.set(Calendar.YEAR, year -1);
			cal.set(Calendar.MONTH, month +10);
		}
		checkDate = cal.getTime();
		if(compareTwoDate(fDate, checkDate) > -1) return true;
		else return false;
	}
	/** So sanh hai ngay: 0: bang nhau, 1: date1 > date2, -1: date1 < date2*/
	public static int compareTwoDate(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		cal1.set(Calendar.MINUTE, 0);
		cal1.set(Calendar.HOUR_OF_DAY, 0);
		cal1.set(Calendar.SECOND, 0);
		cal1.set(Calendar.MILLISECOND, 0);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		cal2.set(Calendar.MINUTE, 0);
		cal2.set(Calendar.HOUR_OF_DAY, 0);
		cal2.set(Calendar.SECOND, 0);
		cal2.set(Calendar.MILLISECOND, 0);
		if (cal1.equals(cal2)) {
			return 0;
		} else if (cal1.after(cal2)) {
			return 1;
		} else {
			return -1;
		}
	}
	
	public static String toMonthYearString(Date date) {
		String dateStr = "";
		try {
			dateStr = new SimpleDateFormat("MM/yyyy").format(date);
		} catch (Exception e) {
		}
		return dateStr;
	}
	public static Date getDateFirtMonthOfNow(){
		String nowStr = "01/" + DateUtil.toMonthYearString(DateUtil.now());
		return DateUtil.toDate(nowStr, DateUtil.DATE_FORMAT_DDMMYYYY);		
	}
	public static Date getFirstNextMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, 1);
		cal.set(Calendar.DATE, 1);
		Date nextMonth = cal.getTime();		
		return nextMonth;
	}
	/** So sanh hai tháng: 0: bang nhau, 1: date1 > date2, -1: date1 < date2*/
	public static int compareDateWithoutDay(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);
		cal1.set(Calendar.MINUTE, 0);
		cal1.set(Calendar.HOUR_OF_DAY, 0);
		cal1.set(Calendar.SECOND, 0);
		cal1.set(Calendar.MILLISECOND, 0);
		cal1.set(Calendar.DATE, 1);
		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);
		cal2.set(Calendar.MINUTE, 0);
		cal2.set(Calendar.HOUR_OF_DAY, 0);
		cal2.set(Calendar.SECOND, 0);
		cal2.set(Calendar.MILLISECOND, 0);
		cal2.set(Calendar.DATE, 1);
		if (cal1.equals(cal2)) {
			return 0;
		} else if (cal1.after(cal2)) {
			return 1;
		} else {
			return -1;
		}
	}
	
	/**
	 * @author tungmt
	 * @since 24/8/2015
	 * @param date - ngay can them thoi gian
	 * @return Date + time
	 */
	public static Date addTime(Date date) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(DateUtil.now());

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);

		cal1.set(Calendar.DATE, cal.get(Calendar.DATE));
		cal1.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
		cal1.set(Calendar.DAY_OF_WEEK_IN_MONTH, cal.get(Calendar.DAY_OF_WEEK_IN_MONTH));
		cal1.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR));
		cal1.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		cal1.set(Calendar.YEAR, cal.get(Calendar.YEAR));

		date = cal1.getTime();
		return date;
	}
	
	/**
	 * Lay ngay chot bo sung them thoi gian
	 * 
	 * @modify hunglm16 
	 * @param sDate - ngay he thong hien tai
	 * @param pDate - ngay can them thoi gian
	 * @return
	 * @description tach thanh ham chung
	 */
	public static Date getLockDatebyDate (Date sDate, Date pDate) {
		if (pDate == null) {
			return null;
		}
		if (sDate == null) {
			sDate = DateUtil.now();
		}
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(sDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(pDate);
		cal1.set(Calendar.DATE, cal.get(Calendar.DATE));
		cal1.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
		cal1.set(Calendar.DAY_OF_WEEK_IN_MONTH, cal.get(Calendar.DAY_OF_WEEK_IN_MONTH));
		cal1.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR));
		cal1.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		cal1.set(Calendar.YEAR, cal.get(Calendar.YEAR));
		return cal1.getTime();
	}
}

package ths.dms.web.utils;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import ths.dms.helper.Configuration;
public class SendMailBySite {
	
	/*public static void main(String[] args) {  
		  
	  String host="mail.javatpoint.com";  
	  final String user="1141486@student.hcmus.edu.vn";//change accordingly  
	  final String password="";//change accordingly  
	    
	  String to="vuongmq@viettel.com.vn";//change accordingly  
	  
	   //Get the session object  
	   Properties props = new Properties();  
	   props.put("mail.smtp.host",host);  
	   props.put("mail.smtp.auth", "true");  
	     
	   Session session = Session.getDefaultInstance(props,  
	    new javax.mail.Authenticator() {  
	      protected PasswordAuthentication getPasswordAuthentication() {  
	    return new PasswordAuthentication(user,password);  
	      }  
	    });  
	  
	   //Compose the message  
	    try {  
	     MimeMessage message = new MimeMessage(session);  
	     message.setFrom(new InternetAddress(user));  
	     message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));  
	     message.setSubject("javatpoint");  
	     message.setText("This is simple program of sending email using JavaMail API");  
	       
	    //send the message  
	     Transport.send(message);  
	  
	     System.out.println("message sent successfully...");  
	   
	     } catch (MessagingException e) {e.printStackTrace();}  
	 }  */
		
	public static void main(String[] args) {  
		  
		 String to="tungmt@viettel.com.vn";//change accordingly 
		 //Get the session object  
		 Properties props = new Properties();  
		 /** cau hinh: send email gmail
		  props.put("mail.smtp.host", "smtp.gmail.com");  
		  props.put("mail.smtp.socketFactory.port", "465");  
		  props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");  
		  props.put("mail.smtp.auth", "true");  
		  props.put("mail.smtp.port", "465");  */
		 /** cau hinh send email: viettel **/
		  props.put("mail.smtp.host", "smtp.viettel.com.vn");  
		  props.put("mail.smtp.socketFactory.port", "465");  
		  props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");  
		  props.put("mail.smtp.auth", "true");  
		  props.put("mail.smtp.port", "465");  
		   
		  Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {  
			   protected PasswordAuthentication getPasswordAuthentication() {  
				   return new PasswordAuthentication("vuongmq@viettel.com.vn","0copass@V");//change accordingly  
			   }  
		  });  
		  
		  //compose message  
		  try {  
			   MimeMessage message = new MimeMessage(session);  
			   message.setFrom(new InternetAddress("vuongmq@viettel.com.vn"));//change accordingly  
			   message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));  
			   message.setSubject("Hello");  
			   message.setText("Testing.......");  
			     
			   //send message  
			   Transport.send(message);  
			  
			   System.out.println("Message sent successfully");  
		   
		  } catch (MessagingException e) {
			  throw new RuntimeException("err Cannot send email " +e.getMessage());
		  }
	} 
		/**
	  * Send a single email.
	  */
	  public void sendEmail (final String aFromEmailAddr, final String aFromEmailAddrPassword, String aToEmailAddr, String aSubject, String aBody) {
			//Get the session object  
			Properties props = new Properties();  
			/** cau hinh: send email gmail */
			/*props.put("mail.smtp.host", "smtp.gmail.com");  
			props.put("mail.smtp.socketFactory.port", "465");  
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");  
			props.put("mail.smtp.auth", "true");  
			props.put("mail.smtp.port", "465");  */
			/** cau hinh send email: viettel **/
			/*props.put("mail.smtp.host", "smtp.viettel.com.vn");  
			props.put("mail.smtp.socketFactory.port", "465");  
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");  
			props.put("mail.smtp.auth", "true");  
			props.put("mail.smtp.port", "465");*/
			
			// Lay gia tri cac cu hinh
			String mail_smtp_host = Configuration.getMailSMTPHost();
			String mail_smtp_socket_port = Configuration.getMailSMTPSocketPort();
			String mail_smtp_socket_class = Configuration.getMailSMTPSocketClass();
			String mail_smtp_auth = Configuration.getMailSMTPAuth();
			String mail_smtp_port = Configuration.getMailSMTPPort();
			if (StringUtil.isNullOrEmpty(mail_smtp_host)
				|| StringUtil.isNullOrEmpty(mail_smtp_socket_port)
				|| StringUtil.isNullOrEmpty(mail_smtp_socket_class)
				|| StringUtil.isNullOrEmpty(mail_smtp_auth)
				|| StringUtil.isNullOrEmpty(mail_smtp_port)) {
				throw new RuntimeException("err Cannot send email because app-setting.xml Configuration not enough.");
			}
			
			props.put("mail.smtp.host", mail_smtp_host);  
			props.put("mail.smtp.socketFactory.port", mail_smtp_socket_port);  
			props.put("mail.smtp.socketFactory.class", mail_smtp_socket_class);  
			props.put("mail.smtp.auth", mail_smtp_auth);  
			props.put("mail.smtp.port", mail_smtp_port);
			
			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {  
			   protected PasswordAuthentication getPasswordAuthentication() {  
				   return new PasswordAuthentication(aFromEmailAddr,aFromEmailAddrPassword);//change accordingly  
			   }  
			});  
	
			//compose message  
		try {
			MimeMessage message = new MimeMessage(session);
			message.setHeader("Content-Type", "text/plain; charset=UTF-8");
			message.setFrom(new InternetAddress(aFromEmailAddr));//change accordingly  
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(aToEmailAddr));
			message.setSubject(aSubject, "utf-8");
			message.setText(aBody, "utf-8");

			//send message  
			Transport.send(message);

			System.out.println("Message sent successfully mail from: " + aFromEmailAddr + " mail to: " + aToEmailAddr);

		} catch (MessagingException e) {
			throw new RuntimeException("err Cannot send email: " + e.getMessage());
		}
	  }
}

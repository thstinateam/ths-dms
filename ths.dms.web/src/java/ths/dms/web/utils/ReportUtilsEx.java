/**
 * 
 */
package ths.dms.web.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jxls.transformer.XLSTransformer;
import ths.dms.helper.Configuration;
import ths.dms.web.constant.ConstantManager;
import ths.dms.web.enumtype.FileExtension;
import ths.dms.web.enumtype.ShopReportTemplate;
import ths.dms.web.utils.report.excel.ExcelProcessUtilsEx;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.struts2.ServletActionContext;

import ths.dms.core.exceptions.BusinessException;

/**
 * 
 * @author TIENTV11
 * @description Utils xu ly bao cao
 * @since 11/02/2014
 *
 */
public class ReportUtilsEx {
	public static String exportFromFormat(FileExtension ext,HashMap<String, Object> parameters, JRDataSource dataSource,ShopReportTemplate shopReport)throws BusinessException{ 
		String outputFile = shopReport.getOutputPath(ext);
		String outputPath = Configuration.getStoreRealPath() + outputFile;
		String outputDownload = Configuration.getExportExcelPath() + outputFile;
		String templatePath =shopReport.getTemplatePath(false, FileExtension.JASPER);
		try{			
			JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parameters, dataSource);
			JRAbstractExporter exporter = null;
			if(FileExtension.PDF.equals(ext)){		
				exporter = new JRPdfExporter();
			} else if(FileExtension.XLS.equals(ext)){
				exporter = new JExcelApiExporter();		
				//parameters.put(JRXlsAbstractExporterParameter.SHEET_NAMES, "ABC");
				parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			}else if(FileExtension.XLSX.equals(ext)){
				HttpServletResponse response = ServletActionContext.getResponse();
				response.setHeader("Content-disposition","attachment");
				response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
				response.setCharacterEncoding(ConstantManager.UTF_8);
				exporter = new JRXlsxExporter();	
				//parameters.put(JRXlsAbstractExporterParameter.SHEET_NAMES, "ABC");
				parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			} else if(FileExtension.DOC.equals(ext)){
				exporter = new JRRtfExporter();				
			} else if(FileExtension.CSV.equals(ext)){
				HttpServletResponse response = ServletActionContext.getResponse();
				response.setHeader("Content-disposition","attachment");
				response.setContentType("application/octet-stream");
				response.setCharacterEncoding(ConstantManager.UTF_8);
				exporter = new JRCsvExporter();
				parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			}
			if(exporter!= null){
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
				exporter.exportReport();
			}			
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			throw new BusinessException(e);
		}
		return outputDownload;
	}
	
	/**
	 * Export from format.
	 *
	 * Them tham so sheetName de dat ten sheet khi xuat file excel.
	 * @param ext the ext
	 * @param parameters the parameters
	 * @param dataSource the data source
	 * @param shopReport the shop report
	 * @return the string
	 * @author hungtt
	 * @since Feb 25, 2013
	 */
	public static String exportFromFormat(FileExtension ext,HashMap<String, Object> parameters, JRDataSource dataSource,ShopReportTemplate shopReport,String sheetName){
		String outputFile = shopReport.getOutputPath(ext);
		String outputPath = Configuration.getStoreRealPath() + outputFile;
		String outputDownload = Configuration.getExportExcelPath() + outputFile;
		String templatePath =shopReport.getTemplatePath(false, FileExtension.JASPER);
		try{			
			JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parameters, dataSource);
			JRAbstractExporter exporter = null;
			if(FileExtension.PDF.equals(ext)){		
				exporter = new JRPdfExporter();
			} else if(FileExtension.XLS.equals(ext)){
				exporter = new JExcelApiExporter();		
				//parameters.put(JRXlsAbstractExporterParameter.SHEET_NAMES, "ABC");
				parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			} else if(FileExtension.DOC.equals(ext)){
				exporter = new JRRtfExporter();				
			} else if(FileExtension.CSV.equals(ext)){
				HttpServletResponse response = ServletActionContext.getResponse();
				response.setHeader("Content-disposition","attachment");
				response.setContentType("application/octet-stream");
				response.setCharacterEncoding(ConstantManager.UTF_8);
				exporter = new JRCsvExporter();
				parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			}
			if(exporter!= null){
				exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
				exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
				if(!StringUtil.isNullOrEmpty(sheetName))
				{
					exporter.setParameter(JRXlsAbstractExporterParameter.SHEET_NAMES, new String[] {sheetName});
				}
				exporter.exportReport();
			}			
		}catch (Exception e) {
			System.out.print("Loi: "+e.getMessage());
			LogUtility.logError(e, e.getMessage());
		}
		return outputDownload;
	}
	
	/**
	 * Gets the max number row.
	 *
	 * @param lstObj the lst obj
	 * @param numCol the num col
	 * @return the max number row
	 */
	public static int getMaxNumberRow(int numCol){
		return (int)(Configuration.getMaxNumberCellReport()/numCol);
	}
	
	public static int getMaxNumRowCrosstabs(int sizeListAuto){
		return (int)(Configuration.getMaxNumberCellReport()/sizeListAuto);
	}
	
	/**
	 * Gets the max report in page.
	 *
	 * @return the max report in page
	 */
	public static int getMaxReportInPage(){
		return 50;
	}
	
	/**
	 * Export html.
	 *
	 * @param response the response
	 * @param parameters the parameters
	 * @param dataSource the data source
	 * @param shopReport the shop report
	 */
	public static void exportHtml(HttpServletResponse response,HashMap<String, Object> parameters,JRDataSource dataSource,ShopReportTemplate shopReport){
		String templatePath =shopReport.getTemplatePath(false, FileExtension.JASPER);
		ServletOutputStream ouputStream = null ;
		try{
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
			JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parameters, dataSource);
			ouputStream = response.getOutputStream();
			JRExporter exporter = null;
			exporter = new JRHtmlExporter();
			exporter.setParameter(JRHtmlExporterParameter.OUTPUT_STREAM,false);
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
			exporter.exportReport();
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
			System.out.print(e.getMessage());
		} finally{
			if(ouputStream!= null){
				try {
					ouputStream.close();
				} catch (IOException e) {
					LogUtility.logError(e, e.getMessage());
				}
			}
		}
	}	
	public static void exportCsvFromFormat(FileExtension ext,HashMap<String, Object> parameters, JRDataSource dataSource,ShopReportTemplate shopReport){
		String outputFile = shopReport.getOutputPath(ext);
		String outputPath = Configuration.getStoreRealPath() + outputFile;
		//String outputDownload = Configuration.getExportExcelPath() + outputFile;
		String templatePath =shopReport.getTemplatePath(false, FileExtension.JASPER);
		try{			
			JasperPrint jasperPrint = JasperFillManager.fillReport(templatePath, parameters, dataSource);
			HttpServletResponse response = ServletActionContext.getResponse();
			JRCsvExporter csvExporter = new JRCsvExporter();
			parameters.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);
		    csvExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
		    
		    
		    StringBuffer buffer = new StringBuffer();
		    csvExporter.setParameter(JRExporterParameter.OUTPUT_STRING_BUFFER, buffer);
		    csvExporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, outputPath);
		    csvExporter.exportReport();

		    response.setContentType("application/octet-stream");
		    response.setHeader("Content-Disposition", "attachment;filename="+outputFile);
		    response.setCharacterEncoding(ConstantManager.UTF_8_ENCODING);
		    response.getOutputStream().write(buffer.toString().getBytes());
		    
		    
		    StringBuffer sb = new StringBuffer();
		    InputStream inputStream = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
		    ServletOutputStream out = response.getOutputStream();
		    
		     
		    byte[] outputByte = new byte[4096];
		    //copy binary contect to output stream
		    while(inputStream.read(outputByte, 0, 4096) != -1)
		    {
		    	out.write(outputByte, 0, 4096);
		    }
		    inputStream.close();
		    //byte-order marker (BOM)
		    byte b[] = {(byte)0xEF, (byte)0xBB, (byte)0xBF};
		    //insert BOM byte array into outputStream
		    out.write(b);
		    out.flush();
		    out.close();
		}catch (Exception e) {
			LogUtility.logError(e, e.getMessage());
		}
	}
	
	
	/**
	 * Gets the vinamilk logo.
	 *
	 * @return the vinamilk logo
	 */
	public static InputStream getVinamilkLogoStream(){
		return ReportUtilsEx.class.getClassLoader().getResourceAsStream("vinamilk80.jpg");
	}
	
	/**
	 * Gets the vinamilk logo real path.
	 *
	 * @param request the request
	 * @return the vinamilk logo real path
	 */
	public static String getVinamilkLogoRealPath(HttpServletRequest request){
		return request.getServletContext().getRealPath("resources/images/vinamilk80.jpg");
	}
	
	/**
	 * Gets the vinamilk logo real path.
	 *
	 * @param request the request
	 * @return the icon uncheck
	 */
	public static String getVinamilkUnchecked(HttpServletRequest request){
		return request.getServletContext().getRealPath("resources/images/unchecked.png");
	}
	
	/**
	 * Gets the vinamilk logo real path.
	 *
	 * @param request the request
	 * @return the icon check
	 */
	public static String getVinamilkChecked(HttpServletRequest request){
		return request.getServletContext().getRealPath("resources/images/checked.png");
	}
	
	/**
	 * Export excel jxls.
	 * @param beans the beans
	 * @param tabletPortalReport the tablet portal report
	 * @return the string
	 * @author thongnm
	 * @since Apr 8, 2013
	 */
	public static String exportExcelJxls(HashMap<String, Object> beans,ShopReportTemplate tabletPortalReport, FileExtension ext) throws BusinessException {
		String outputFile = tabletPortalReport.getOutputPath(ext);
		String outputPath = Configuration.getStoreRealPath() + outputFile;
		String outputDownload = Configuration.getExportExcelPath() + outputFile;
		String templatePath =tabletPortalReport.getTemplatePath(false, FileExtension.XLS);
		
		try{
			//Configuration.getStoreRealPath();
			InputStream inputStream = new BufferedInputStream(new FileInputStream(templatePath));
			XLSTransformer transformer = new XLSTransformer();
			org.apache.poi.ss.usermodel.Workbook resultWorkbook = transformer.transformXLS(inputStream, beans);
			inputStream.close();
			OutputStream os = new BufferedOutputStream(new FileOutputStream(outputPath));
			resultWorkbook.write(os);
			os.flush();
			os.close();
			
		}catch (Exception e) {
			throw new BusinessException(e);
		}
		return outputDownload;
	}
	
	
	
	
	
	/**
	 * Begin JXL range
	 * @author datnt43
	 */
	public void addCell(WritableSheet sheet, int col, int row, Integer value, WritableCellFormat cellFormat) throws RowsExceededException, WriteException{		
		if(value != null){
			sheet.addCell(new Number(col, row, value.doubleValue(), cellFormat));
		}else{
			sheet.addCell(new Label(col, row, "", cellFormat));
		}
	}
	public void addCell(WritableSheet sheet, int col, int row, Float value, WritableCellFormat cellFormat) throws RowsExceededException, WriteException{		
		if(value != null){
			sheet.addCell(new Number(col, row, Math.round(value.doubleValue()*100.0)/100.0d, cellFormat));
		}else{
			sheet.addCell(new Label(col, row, "", cellFormat));
		}
	}
	public void addCell(WritableSheet sheet, int col, int row, BigDecimal value, WritableCellFormat cellFormat) throws RowsExceededException, WriteException{
		if(value != null){
			sheet.addCell(new Number(col, row, value.doubleValue(), cellFormat));
		}else{
			sheet.addCell(new Label(col, row, "", cellFormat));
		}
	}
	public void addCell(WritableSheet sheet, int col, int row, String value, WritableCellFormat cellFormat) throws RowsExceededException, WriteException{
		sheet.addCell(new Label(col, row, value, cellFormat));
	}
	public void addCells(WritableSheet sheet, int col, int row, int endCol, int endRow, String value, WritableCellFormat cellFormat) throws RowsExceededException, WriteException{
		sheet.mergeCells(col, row, endCol, endRow);
		sheet.addCell(new Label(col, row, value, cellFormat));
	}
	public void addCells(WritableSheet sheet, int col, int row, int endCol, int endRow, BigDecimal value, WritableCellFormat cellFormat) throws RowsExceededException, WriteException{
		sheet.mergeCells(col, row, endCol, endRow);
		addCell(sheet, col, row, value, cellFormat);
	}
	public void addCells(WritableSheet sheet, int col, int row, int endCol, int endRow, Integer value, WritableCellFormat cellFormat) throws RowsExceededException, WriteException{
		sheet.mergeCells(col, row, endCol, endRow);
		addCell(sheet, col, row, value, cellFormat);
	}
	/**	End JXL range	*/
	
	/**
	 * @author datnt43
	 * @since 23/12/2013
	 * @description add cell for Apache POI XSSF
	 * Begin XSSF Range
	 */
	public void addCell(XSSFSheet sheet, int colIndex, int rowIndex, Integer value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		Row row = sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex);
		Cell cell = row.getCell(colIndex)== null?row.createCell(colIndex):row.getCell(colIndex);
		if(value != null){
			cell.setCellValue(value.doubleValue());
		}else{
			cell.setCellValue(0);
		}
		cell.setCellStyle(cellFormat);
	}
	public void addCell(XSSFSheet sheet, int colIndex, int rowIndex, String value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		Row row = sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex);
		Cell cell = row.getCell(colIndex)== null?row.createCell(colIndex):row.getCell(colIndex);
		if(value != null){
			cell.setCellValue(value);
		}else{
			cell.setCellValue("");
		}
		cell.setCellStyle(cellFormat);
	}
	public void addCell(XSSFSheet sheet, int colIndex, int rowIndex, BigDecimal value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		Row row = sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex);
		Cell cell = row.getCell(colIndex)== null?row.createCell(colIndex):row.getCell(colIndex);
		if(value != null){
			cell.setCellValue(value.doubleValue());
		}else{
			cell.setCellValue("");
		}
		cell.setCellStyle(cellFormat);
	}
	
	public void addCell(XSSFSheet sheet, int colIndex, int rowIndex, Object value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		Row row = sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex);
		Cell cell = row.getCell(colIndex)== null?row.createCell(colIndex):row.getCell(colIndex);
		if(value != null){
			if(value instanceof BigDecimal){
				cell.setCellValue(((BigDecimal)value).doubleValue());
			} else if(value instanceof Integer){
				cell.setCellValue(((Integer)value).doubleValue());
			} else if(value instanceof String){
				cell.setCellValue((String)value);
			}
		}else{
			cell.setCellValue("");
		}
		cell.setCellStyle(cellFormat);
	}
	
	public void addCells(XSSFSheet sheet, int colIndex, int rowIndex, int endColIndex, int endRowIndex, String value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		for(int i = rowIndex; i <=endRowIndex;i++){
			Row row1 = sheet.getRow(i) == null?sheet.createRow(i):sheet.getRow(i);
			for(int j = colIndex; j <= endColIndex; j++){
				Cell cell1 = row1.getCell(j)== null?row1.createCell(j):row1.getCell(j);
				cell1.setCellStyle(cellFormat);
				if(i == rowIndex && j == colIndex){
					if(value != null){
						cell1.setCellValue(value);
					}else{
						cell1.setCellValue("");
					}
				}
			}
		}
		sheet.addMergedRegion(new CellRangeAddress(rowIndex,endRowIndex,colIndex,endColIndex));
		
	}
	public void addCells(XSSFSheet sheet, int colIndex, int rowIndex, int endColIndex, int endRowIndex, BigDecimal value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		for(int i = rowIndex; i <=endRowIndex;i++){
			Row row1 = sheet.getRow(i) == null?sheet.createRow(i):sheet.getRow(i);
			for(int j = colIndex; j <= endColIndex; j++){
				Cell cell1 = row1.getCell(j)== null?row1.createCell(j):row1.getCell(j);
				cell1.setCellStyle(cellFormat);
				if(i == rowIndex && j == colIndex){
					if(value != null){
						cell1.setCellValue(value.doubleValue());
					}else{
						cell1.setCellValue("");
					}
				}
			}
		}
		sheet.addMergedRegion(new CellRangeAddress(rowIndex,endRowIndex,colIndex,endColIndex));
//		cell.setCellStyle(cellFormat);
		
	}
	public void addCells(XSSFSheet sheet, int colIndex, int rowIndex, int endColIndex, int endRowIndex, Integer value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		for(int i = rowIndex; i <=endRowIndex;i++){
			Row row1 = sheet.getRow(i) == null?sheet.createRow(i):sheet.getRow(i);
			for(int j = colIndex; j <= endColIndex; j++){
				Cell cell1 = row1.getCell(j)== null?row1.createCell(j):row1.getCell(j);
				cell1.setCellStyle(cellFormat);
				if(i == rowIndex && j == colIndex){
					if(value != null){
						cell1.setCellValue(value.doubleValue());
					}else{
						cell1.setCellValue("");
					}
				}
			}
		}
		sheet.addMergedRegion(new CellRangeAddress(rowIndex,endRowIndex,colIndex,endColIndex));
	}
	/**
	 * Set RowHeight by height for row in index position (by point)
	 * Dat do cao height cho dong index (by pixel)
	 */
	public void setRowHeight(XSSFSheet sheet,int rowIndex, int height){
		Row row = sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex);
		row.setHeight((short) (height*20));
	}
	/**
	 * Set ColumnWidth by width for column in index position (by pixel)
	 * Dat do rong width cho cot index (by pixel) 
	 */
	public void setColumnWidth(XSSFSheet sheet,int colIndex, int width){
		sheet.setColumnWidth(colIndex, (int)((double)(width*256)/(double)(ConstantManager.XSSF_MAX_DIGIT_WIDTH + ConstantManager.XSSF_CHARACTER_DEFAULT_PADDING)));
	}
	
	/**
	 * Set ColumnWidthfor multiple Column, start from startIndex (by pixel)
	 * Dat do rong cho nhieu cot, bat dau tu cot thu startIndex (by pixel) 
	 */
	public void setColumnsWidth(XSSFSheet sheet,Integer startIndex, Integer...widths){
		for(int i =0,sizeTmp = widths.length;i < sizeTmp; i++){
			sheet.setColumnWidth(i+startIndex, (int)((double)(widths[i]*256)/(double)(ConstantManager.XSSF_MAX_DIGIT_WIDTH + ConstantManager.XSSF_CHARACTER_DEFAULT_PADDING)));
		}
	}
	/**
	 * Set RowHeight for multiple row, start from startIndex (by point)
	 * Dat do cao cho nhieu dong, bat dau tu cot thu startIndex (by point) 
	 */
	public void setRowsHeight(XSSFSheet sheet,Integer startIndex, Integer...heights){
		for(int i =0,sizeTmp = heights.length;i < sizeTmp; i++){
			setRowHeight(sheet,i+startIndex,heights[i]);
		}
	}
	
	
	/**
	 * Init Phase to set Border for an Area
	 * Qua trinh khoi tao de set Border cho mot Area (New CellStyle moi de tranh cac cell ko lien quan bi ghi de BorderStyle)
	 * @author datnt43
	 * @since 09/01/2014
	 * @param wb
	 * @param sheet
	 * @param colIndex
	 * @param rowIndex
	 * @param endColIndex
	 * @param endRowIndex
	 */
	public void InitSetBorderForArea(Workbook wb,SXSSFSheet sheet,int colIndex, int rowIndex, int endColIndex, int endRowIndex){
		SXSSFRow rowTop = (SXSSFRow) (sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex));
		SXSSFRow rowBottom = (SXSSFRow) (sheet.getRow(endRowIndex) == null?sheet.createRow(endRowIndex):sheet.getRow(endRowIndex));
		Map<CellStyle,CellStyle> mapTopCellStyle = new HashMap<CellStyle,CellStyle>();
		Map<CellStyle,CellStyle> mapBottomCellStyle = new HashMap<CellStyle,CellStyle>();
		Map<CellStyle,CellStyle> mapLeftCellStyle = new HashMap<CellStyle,CellStyle>();
		Map<CellStyle,CellStyle> mapRightCellStyle = new HashMap<CellStyle,CellStyle>();
		
		//4 conner----------------------------------
		SXSSFCell cellLeftTop = (SXSSFCell) (rowTop.getCell(colIndex)== null?rowTop.createCell(colIndex):rowTop.getCell(colIndex));
		SXSSFCell cellRightTop = (SXSSFCell) (rowTop.getCell(endColIndex)== null?rowTop.createCell(endColIndex):rowTop.getCell(endColIndex));
		SXSSFCell cellLeftBottom = (SXSSFCell) (rowBottom.getCell(colIndex)== null?rowBottom.createCell(colIndex):rowBottom.getCell(colIndex));
		SXSSFCell cellRightBottom = (SXSSFCell) (rowBottom.getCell(endColIndex)== null?rowBottom.createCell(endColIndex):rowBottom.getCell(endColIndex));
		
		CellStyle cLeftTop = wb.createCellStyle();
		CellStyle cRightTop = wb.createCellStyle();
		CellStyle cLeftBottom = wb.createCellStyle();
		CellStyle cRightBottom = wb.createCellStyle();
		
		cLeftTop.cloneStyleFrom(cellLeftTop.getCellStyle());
		cellLeftTop.setCellStyle(cLeftTop);
		
		
		cRightTop.cloneStyleFrom(cellRightTop.getCellStyle());
		cellRightTop.setCellStyle(cRightTop);
		
		cLeftBottom.cloneStyleFrom(cellLeftBottom.getCellStyle());
		cellLeftBottom.setCellStyle(cLeftBottom);
		
		cRightBottom.cloneStyleFrom(cellRightBottom.getCellStyle());
		cellRightBottom.setCellStyle(cRightBottom);
		
		
		
		//End 4 conner----------------------------------
		
		//not a standard Area-----------------------------
		if((colIndex == endColIndex) && (rowIndex == endRowIndex)){ // Area with 1 cell 
			SXSSFRow row = (SXSSFRow) (sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex));
			SXSSFCell cell = (SXSSFCell) (row.getCell(colIndex)== null?row.createCell(colIndex):row.getCell(colIndex));
			CellStyle c = wb.createCellStyle();
			c.cloneStyleFrom(cell.getCellStyle());
			cell.setCellStyle(c);
		}else if((colIndex == endColIndex) && (rowIndex != endRowIndex)) { // Area of Columns
			Map<CellStyle,CellStyle> mapCellStyle = new HashMap<CellStyle,CellStyle>();
			for(int i = rowIndex; i <=endRowIndex;i++){
				if(i == endRowIndex || i == rowIndex){
					continue;
				}
				SXSSFRow row = (SXSSFRow) (sheet.getRow(i) == null?sheet.createRow(i):sheet.getRow(i));
				SXSSFCell cell = (SXSSFCell) (row.getCell(colIndex)== null?row.createCell(colIndex):row.getCell(colIndex));
				if(mapCellStyle.get(cell.getCellStyle()) != null){
					cell.setCellStyle(mapCellStyle.get(cell.getCellStyle()));
					continue;
				} 
				CellStyle c = wb.createCellStyle();
				c.cloneStyleFrom(cell.getCellStyle());
				mapCellStyle.put(cell.getCellStyle(), c);
				cell.setCellStyle(c);
			}
			return;
		} else if((colIndex != endColIndex) && (rowIndex == endRowIndex)) { // Area of Rows
			Map<CellStyle,CellStyle> mapCellStyle = new HashMap<CellStyle,CellStyle>();
			SXSSFRow row = (SXSSFRow) (sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex));
			for(int i = colIndex; i <=endColIndex;i++){
				if(i == colIndex || i == endColIndex){
					continue;
				}
				SXSSFCell cell = (SXSSFCell) (row.getCell(i)== null?row.createCell(i):row.getCell(i));
				if(mapCellStyle.get(cell.getCellStyle()) != null){
					cell.setCellStyle(mapCellStyle.get(cell.getCellStyle()));
					continue;
				} 
				CellStyle c = wb.createCellStyle();
				c.cloneStyleFrom(cell.getCellStyle());
				mapCellStyle.put(cell.getCellStyle(), c);
				cell.setCellStyle(c);
			}
			return;
		}
		//End not a standard Area----------------------------
		
		
		//Standard Area-------------------------------------
		//Clone inside
		//Init Top Bottom
		Boolean flagContinueTop  = false;
		Boolean flagContinueBottom  = false;
		
		for(int j = colIndex; j <= endColIndex; j++){
			if(j == endColIndex || j == colIndex){
				continue;
			}
			flagContinueTop = false;
			flagContinueBottom = false;
			SXSSFCell cellTop = (SXSSFCell) (rowTop.getCell(j)== null?rowTop.createCell(j):rowTop.getCell(j));
			SXSSFCell cellBottom = (SXSSFCell) (rowBottom.getCell(j)== null?rowBottom.createCell(j):rowBottom.getCell(j));
			if(mapTopCellStyle.get(cellTop.getCellStyle()) != null){
				cellTop.setCellStyle(mapTopCellStyle.get(cellTop.getCellStyle()));
				flagContinueTop = true;
//				continue;
			} 
			if(mapBottomCellStyle.get(cellBottom.getCellStyle()) != null){
				cellBottom.setCellStyle(mapBottomCellStyle.get(cellBottom.getCellStyle()));
				flagContinueBottom = true;
			}
			if(!flagContinueTop){
//				if(j != colIndex && j != endColIndex){
					CellStyle c = wb.createCellStyle();
					c.cloneStyleFrom(cellTop.getCellStyle());
					mapTopCellStyle.put(cellTop.getCellStyle(), c);
					cellTop.setCellStyle(c);
//				}
			}
			if(!flagContinueBottom){
//				if(j != colIndex && j != endColIndex){
					CellStyle c = wb.createCellStyle();
					c.cloneStyleFrom(cellBottom.getCellStyle());
					mapBottomCellStyle.put(cellBottom.getCellStyle(), c);
					cellBottom.setCellStyle(c);
//				}
			}
		}
		
		//Init left Right
		Boolean flagContinueRight = false;
		Boolean flagContinueLeft = false;
		for(int i = rowIndex; i <=endRowIndex;i++){
			if(i == endRowIndex || i == rowIndex){
				continue;
			}
			SXSSFRow row = (SXSSFRow) (sheet.getRow(i) == null?sheet.createRow(i):sheet.getRow(i));
			flagContinueLeft = false;
			flagContinueRight = false;
			SXSSFCell cellLeft = (SXSSFCell) (row.getCell(colIndex)== null?row.createCell(colIndex):row.getCell(colIndex));
			SXSSFCell cellRight = (SXSSFCell) (row.getCell(endColIndex)== null?row.createCell(endColIndex):row.getCell(endColIndex));
			if(mapLeftCellStyle.get(cellLeft.getCellStyle()) != null){
				cellLeft.setCellStyle(mapLeftCellStyle.get(cellLeft.getCellStyle()));
				flagContinueLeft = true;
			} 
			if(mapRightCellStyle.get(cellRight.getCellStyle()) != null){
				cellRight.setCellStyle(mapRightCellStyle.get(cellRight.getCellStyle()));
				flagContinueRight = true;
			}
			if(!flagContinueLeft){
//				if(i != rowIndex && i != endRowIndex){
					CellStyle c = wb.createCellStyle();
					c.cloneStyleFrom(cellLeft.getCellStyle());
					mapLeftCellStyle.put(cellLeft.getCellStyle(), c);
					cellLeft.setCellStyle(c);
//				}
			}
			if(!flagContinueRight){
//				if(i != rowIndex && i != endRowIndex){
					CellStyle c = wb.createCellStyle();
					c.cloneStyleFrom(cellRight.getCellStyle());
					mapRightCellStyle.put(cellRight.getCellStyle(), c);
					cellRight.setCellStyle(c);
//				}
			}
		}
		//End Standard Area-------------------------------------		
		
	}
	
	/**
	 * Set border by borderStyle(Top,Left,Bottom,Right) for an Area
	 * Dat Boder Style cho mot area
	 * @author datnt43
	 * @since 09/01/2014
	 * @param wb: workbook dang su dung
	 * @param sheet: sheet hien hanh
	 * @param colIndex: column bat dau
	 * @param rowIndex: row bat dau
	 * @param endColIndex: column ket thuc
	 * @param endRowIndex: row ket thuc
	 * @param borderStyleTop
	 * @param borderStyleLeft
	 * @param borderStyleBottom
	 * @param borderStyleRight
	 * @note: So luong CellStyle trong mot Workbook ko vuot qua 32767 (Maximum of Short). Ham InitSetBorderForArea se lam tang so luong cellStyle len. CARE ! 
	 */
	public void setBorderForArea(Workbook wb,SXSSFSheet sheet,int colIndex, int rowIndex, int endColIndex, int endRowIndex, BorderStyle borderStyleTop,BorderStyle borderStyleLeft,BorderStyle borderStyleBottom,BorderStyle borderStyleRight){
		InitSetBorderForArea(wb,sheet,colIndex,rowIndex, endColIndex, endRowIndex);
		Short bT = borderStyleTop == null?-1:Short.valueOf(String.valueOf(borderStyleTop.ordinal()));
		Short bL = borderStyleLeft == null?-1:Short.valueOf(String.valueOf(borderStyleLeft.ordinal()));
		Short bB = borderStyleBottom == null?-1:Short.valueOf(String.valueOf(borderStyleBottom.ordinal()));
		Short bR = borderStyleRight == null?-1:Short.valueOf(String.valueOf(borderStyleRight.ordinal()));
		
		for(int i = rowIndex; i <=endRowIndex;i++){
			SXSSFRow row1 = (SXSSFRow) (sheet.getRow(i) == null?sheet.createRow(i):sheet.getRow(i));
			for(int j = colIndex; j <= endColIndex; j++){
				if((j > colIndex && j < endColIndex) && ( i > rowIndex  && i < endRowIndex)){
					continue;
				}
				SXSSFCell cell1 = (SXSSFCell) (row1.getCell(j)== null?row1.createCell(j):row1.getCell(j));
				if((colIndex == endColIndex) && (rowIndex == endRowIndex)){ // Area with 1 cell 
					ExcelProcessUtilsEx.setBorderForCell(cell1.getCellStyle(), null, ExcelProcessUtilsEx.poiBlack,borderStyleTop,borderStyleLeft,borderStyleBottom,borderStyleRight);
				} else if((colIndex == endColIndex) && (rowIndex != endRowIndex)) { // Area of Columns
					if(i ==rowIndex && j == colIndex){
						setBorderRightForCellStyle(cell1,bR);
						setBorderTopForCellStyle(cell1,bT);
						setBorderLeftForCellStyle(cell1,bL);
					} else if(i == endRowIndex && j == endColIndex){
						setBorderLeftForCellStyle(cell1,bL);
						setBorderRightForCellStyle(cell1,bR);
						setBorderBottomForCellStyle(cell1,bB);
					} else {
						setBorderLeftForCellStyle(cell1,bL);
						setBorderRightForCellStyle(cell1,bR);
					}
				} else if((colIndex != endColIndex) && (rowIndex == endRowIndex)) { // Area of Rows
					if(i ==rowIndex && j == colIndex){
						setBorderTopForCellStyle(cell1,bT);
						setBorderLeftForCellStyle(cell1,bL);
						setBorderBottomForCellStyle(cell1,bB);
					} else if(i == endRowIndex && j == endColIndex){
						setBorderTopForCellStyle(cell1,bT);
						setBorderRightForCellStyle(cell1,bR);
						setBorderBottomForCellStyle(cell1,bB);
					} else {
						setBorderTopForCellStyle(cell1,bT);
						setBorderBottomForCellStyle(cell1,bB);
					}
				} else{	// Area with colIndex != endColIndex && rowIndex != endRowIndex
					if(i ==rowIndex && j == colIndex){
						setBorderTopForCellStyle(cell1,bT);
						setBorderLeftForCellStyle(cell1,bL);
					} else if(i ==endRowIndex && j == colIndex){
						setBorderLeftForCellStyle(cell1,bL);
						setBorderBottomForCellStyle(cell1,bB);
					} else if(i == rowIndex && j == endColIndex){
						setBorderRightForCellStyle(cell1,bR);
						setBorderTopForCellStyle(cell1,bT);
					} else if(i == endRowIndex && j == endColIndex){
						setBorderRightForCellStyle(cell1,bR);
						setBorderBottomForCellStyle(cell1,bB);
					} else if( i == rowIndex){
						setBorderTopForCellStyle(cell1,bT);
					} else if( i == endRowIndex ){
						setBorderBottomForCellStyle(cell1,bB);
					} else if( j == colIndex ){
						setBorderLeftForCellStyle(cell1,bL);
					} else if( j == endColIndex ){
						setBorderRightForCellStyle(cell1,bR);
					}
				}
				
				
			}
		}
	}
	
	private void setBorderRightForCellStyle(SXSSFCell cell,Short borderStyleIndex){
		if(borderStyleIndex != -1){
			cell.getCellStyle().setBorderRight(borderStyleIndex);
		}
	}
	private void setBorderTopForCellStyle(SXSSFCell cell,Short borderStyleIndex){
		if(borderStyleIndex != -1){
			cell.getCellStyle().setBorderTop(borderStyleIndex);
		}
	}
	private void setBorderLeftForCellStyle(SXSSFCell cell,Short borderStyleIndex){
		if(borderStyleIndex != -1){
			cell.getCellStyle().setBorderLeft(borderStyleIndex);
		}
	}
	private void setBorderBottomForCellStyle(SXSSFCell cell,Short borderStyleIndex){
		if(borderStyleIndex != -1){
			cell.getCellStyle().setBorderBottom(borderStyleIndex);
		}
	}
	/**	End XSSF range	*/
	
	
	/**
	 * @author datnt43
	 * @since 30/12/2013
	 * @description add cell for Apache POI SXSSF
	 * Begin SXSSF Range
	 */
	public void addCell(SXSSFSheet sheet, int colIndex, int rowIndex, Integer value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		Row row = sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex);
		Cell cell = row.getCell(colIndex)== null?row.createCell(colIndex):row.getCell(colIndex);
		if(value != null){
			cell.setCellValue(value.doubleValue());
		}else{
			cell.setCellValue(0);
		}
		cell.setCellStyle(cellFormat);
	}
	public void addCell(SXSSFSheet sheet, int colIndex, int rowIndex, String value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		Row row = sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex);
		Cell cell = row.getCell(colIndex)== null?row.createCell(colIndex):row.getCell(colIndex);
		if(value != null){
			cell.setCellValue(value);
		}else{
			cell.setCellValue("");
		}
		cell.setCellStyle(cellFormat);
	}
	public void addCell(SXSSFSheet sheet, int colIndex, int rowIndex, BigDecimal value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		Row row = sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex);
		Cell cell = row.getCell(colIndex)== null?row.createCell(colIndex):row.getCell(colIndex);
		if(value != null){
			cell.setCellValue(value.doubleValue());
		}else{
			cell.setCellValue("");
		}
		cell.setCellStyle(cellFormat);
	}
	
	public void addCell(SXSSFSheet sheet, int colIndex, int rowIndex, Object value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		Row row = sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex);
		Cell cell = row.getCell(colIndex)== null?row.createCell(colIndex):row.getCell(colIndex);
		if(value != null){
			if(value instanceof BigDecimal){
				cell.setCellValue(((BigDecimal)value).doubleValue());
			} else if(value instanceof Integer){
				cell.setCellValue(((Integer)value).doubleValue());
			} else if(value instanceof String){
				cell.setCellValue((String)value);
			} else if (value instanceof Double) {
				cell.setCellValue((Double)value);
			} else if (value instanceof Long) {
				cell.setCellValue((Long)value);
			}
		}else{
			cell.setCellValue("");
		}
		cell.setCellStyle(cellFormat);
	}
	
	public void addCells(SXSSFSheet sheet, int colIndex, int rowIndex, int endColIndex, int endRowIndex, String value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		for(int i = rowIndex; i <=endRowIndex;i++){
			Row row1 = sheet.getRow(i) == null?sheet.createRow(i):sheet.getRow(i);
			for(int j = colIndex; j <= endColIndex; j++){
				Cell cell1 = row1.getCell(j)== null?row1.createCell(j):row1.getCell(j);
				cell1.setCellStyle(cellFormat);
				if(i == rowIndex && j == colIndex){
					if(value != null){
						cell1.setCellValue(value);
					}else{
						cell1.setCellValue("");
					}
				}
			}
		}
		sheet.addMergedRegion(new CellRangeAddress(rowIndex,endRowIndex,colIndex,endColIndex));
		
	}
	public void addCells(SXSSFSheet sheet, int colIndex, int rowIndex, int endColIndex, int endRowIndex, BigDecimal value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		for(int i = rowIndex; i <=endRowIndex;i++){
			Row row1 = sheet.getRow(i) == null?sheet.createRow(i):sheet.getRow(i);
			for(int j = colIndex; j <= endColIndex; j++){
				Cell cell1 = row1.getCell(j)== null?row1.createCell(j):row1.getCell(j);
				cell1.setCellStyle(cellFormat);
				if(i == rowIndex && j == colIndex){
					if(value != null){
						cell1.setCellValue(value.doubleValue());
					}else{
						cell1.setCellValue("");
					}
				}
			}
		}
		sheet.addMergedRegion(new CellRangeAddress(rowIndex,endRowIndex,colIndex,endColIndex));
		
	}
	public void addCells(SXSSFSheet sheet, int colIndex, int rowIndex, int endColIndex, int endRowIndex, Integer value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		for(int i = rowIndex; i <=endRowIndex;i++){
			Row row1 = sheet.getRow(i) == null?sheet.createRow(i):sheet.getRow(i);
			for(int j = colIndex; j <= endColIndex; j++){
				Cell cell1 = row1.getCell(j)== null?row1.createCell(j):row1.getCell(j);
				cell1.setCellStyle(cellFormat);
				if(i == rowIndex && j == colIndex){
					if(value != null){
						cell1.setCellValue(value.doubleValue());
					}else{
						cell1.setCellValue("");
					}
				}
			}
		}
		sheet.addMergedRegion(new CellRangeAddress(rowIndex,endRowIndex,colIndex,endColIndex));
	}
	public void addCells(SXSSFSheet sheet, int colIndex, int rowIndex, int endColIndex, int endRowIndex, Object value, XSSFCellStyle cellFormat) throws RowsExceededException, WriteException{
		for(int i = rowIndex; i <=endRowIndex;i++){
			Row row1 = sheet.getRow(i) == null?sheet.createRow(i):sheet.getRow(i);
			for(int j = colIndex; j <= endColIndex; j++){
				Cell cell = row1.getCell(j)== null?row1.createCell(j):row1.getCell(j);
				cell.setCellStyle(cellFormat);
				if(i == rowIndex && j == colIndex){
					if(value != null){
						if(value instanceof BigDecimal){
							cell.setCellValue(((BigDecimal)value).doubleValue());
						} else if(value instanceof Integer){
							cell.setCellValue(((Integer)value).doubleValue());
						} else if(value instanceof String){
							cell.setCellValue((String)value);
						} else if (value instanceof Long) {
							cell.setCellValue((Long)value);
						}
					}else{
						cell.setCellValue("");
					}
				}
			}
		}
		sheet.addMergedRegion(new CellRangeAddress(rowIndex,endRowIndex,colIndex,endColIndex));
	}
	/**
	 * Set RowHeight by height for row in index position (by point)
	 * Dat do cao height cho dong index (by point)
	 */
	public void setRowHeight(SXSSFSheet sheet,int rowIndex, int height){
		Row row = sheet.getRow(rowIndex) == null?sheet.createRow(rowIndex):sheet.getRow(rowIndex);
		row.setHeight((short) (height*20));	// 1/20 of a point
	}
	/**
	 * Set ColumnWidth by width for column in index position (by pixel)
	 * Dat do rong width cho cot index (by pixel) 
	 */
	public void setColumnWidth(SXSSFSheet sheet,int colIndex, int width){
		sheet.setColumnWidth(colIndex, (int)((double)(width*256)/(double)(ConstantManager.XSSF_MAX_DIGIT_WIDTH + ConstantManager.XSSF_CHARACTER_DEFAULT_PADDING)));
	}
	
	/**
	 * Set ColumnWidthfor multiple Column, start from startIndex (by pixel)
	 * Dat do rong cho nhieu cot, bat dau tu cot thu startIndex (by pixel) 
	 */
	public void setColumnsWidth(SXSSFSheet sheet,Integer startIndex, Integer...widths){
		for(int i =0,sizeTmp = widths.length;i < sizeTmp; i++){
			sheet.setColumnWidth(i+startIndex, (int)((double)(widths[i]*256)/(double)(ConstantManager.XSSF_MAX_DIGIT_WIDTH + ConstantManager.XSSF_CHARACTER_DEFAULT_PADDING)));
		}
	}
	/**
	 * Set RowHeight for multiple row, start from startIndex (by point)
	 * Dat do cao cho nhieu dong, bat dau tu cot thu startIndex (by point) 
	 */
	public void setRowsHeight(SXSSFSheet sheet,Integer startIndex, Integer...heights){
		for(int i =0,sizeTmp = heights.length;i < sizeTmp; i++){
			setRowHeight(sheet,i+startIndex,heights[i]);
		}
	}
	
	/**
	 * Ham Flush row xuong file tam, ho tro groupline cho SXSSFSheet.
	 * Flush row to Temp File, support Group Row.
	 * @author datnt43
	 * @param sheet
	 * @param lstStartIndex List start rowIndex of grouplines
	 * @param lstEndIndex	List end rowIndex of grouplines
	 * @param curEndIndex	Index of current Row
	 * @param rowNumInWindow Number of current row remain in window (not flush yet)
	 * @throws IOException
	 */
	public void flushAllRowAndGroupLines(SXSSFSheet sheet,List<Integer> lstStartIndex,List<Integer> lstEndIndex, int curEndIndex, int rowNumInWindow) throws IOException{
		//curIndex = index of first row in window. Incremental
		int curIndex = curEndIndex - rowNumInWindow + 1;
//		List<Integer> lstCurStartIndex =  new ArrayList<Integer>();
//		List<Integer> lstCurEndIndex =  new ArrayList<Integer>();
//		lstCurStartIndex.addAll(lstStartIndex);
//		lstCurEndIndex.addAll(lstEndIndex);
		
		if((lstStartIndex != null && lstStartIndex.size() >0) && (lstEndIndex != null && lstEndIndex.size() >0)){
			//Iterate throw all row in window
			for(;curIndex <= curEndIndex;curIndex++){
				for(int i =0,sizeStart = lstStartIndex.size(); i < sizeStart ;i++){
					//if curRow in groupRows list => group (infact it increase groupLevel of Row) 
					if(curIndex >= lstStartIndex.get(i) && curIndex < lstEndIndex.get(i)){
						sheet.groupRow(curIndex, curIndex);
					}
				}
			}
			//after group => flush
			sheet.flushRows();
			return;
		}else{
			sheet.flushRows();	//not groupline, just flush
			return;
		}
	}
	/**
	 * <Khong dung ham nay nua, ham flushAllRowAndGroupLines da ho tro group rows roi >
	 * Ham ho tro groupline cho SXSSFSheet.
	 * Group Row function. Support SXSSFSheet flushRow with groupline
	 * @author datnt43 
	 * @param sheet
	 * @param startIndex	Start rowIndex of current groupline
	 * @param endIndex	End rowIndex of current groupline
	 * @param rowNumInWindow Number of current row remain in window (not flush yet)
	 * @param lstStartIndex List start rowIndex of grouplines
	 * @param lstEndIndex	List end rowIndex of grouplines
	 * @throws IOException
	 */
	public void groupRows(SXSSFSheet sheet, int startIndex, int endIndex, Integer rowNumInWindow,List<Integer> lstStartIndex,List<Integer> lstEndIndex) throws IOException{
		
		if((lstStartIndex != null && lstStartIndex.size() >0) && (lstEndIndex != null && lstEndIndex.size() >0)){
			if((rowNumInWindow != null && rowNumInWindow >=0)){
				//kiem tra current group co namt rong current window ko
				if((endIndex - rowNumInWindow) > startIndex)
					return;
			}
			//Chi groupRow nhung Group nam trong current Window
			for(int i =0; i < lstStartIndex.size() ;i++){
				if(startIndex == lstStartIndex.get(i) && endIndex == lstEndIndex.get(i)){
					//Xoa grouprow hien tai ra khoi danh sach se duoc group. Tranh group lai boi ham flushAllRowAndGroupLines(..)
					lstStartIndex.remove(i);	
					lstEndIndex.remove(i);
				}
			}
			//group
			sheet.groupRow(startIndex, endIndex);
		} else{
			sheet.groupRow(startIndex, endIndex);	//neu khong define lstStart/EndIndex thi grouprow nhu binh thuong
		}
		return;
	}
	
	/**
	 * Tao Style chua du lieu kieu Text dua tren sourceStyle
	 * @author datnt43
	 * @param workbook
	 * @param fmt <=  duoc tao ra tu XSSFDataFormat fmt = (XSSFDataFormat) workbook.createDataFormat();
	 * @param sourceStyle
	 * @return
	 * @throws IOException
	 */
	public XSSFCellStyle createTextFormatStyleFromStyle(SXSSFWorkbook workbook, XSSFDataFormat fmt,XSSFCellStyle sourceStyle) throws IOException{
		XSSFCellStyle outputStyle = (XSSFCellStyle) workbook.createCellStyle();
		outputStyle.cloneStyleFrom(sourceStyle);
		outputStyle.setDataFormat(fmt.getFormat("@"));
		return outputStyle;
	}
	
	/**
	 * Tao Style chua du lieu kieu phan tram 0.0\%;-0.0\%;0.0\% dua tren sourceStyle
	 * @author datnt43
	 * @param workbook
	 * @param fmt <=  duoc tao ra tu XSSFDataFormat fmt = (XSSFDataFormat) workbook.createDataFormat();
	 * @param sourceStyle
	 * @return
	 * @throws IOException
	 */
	public XSSFCellStyle createPercentageFormatStyleFromStyle(SXSSFWorkbook workbook, XSSFDataFormat fmt,XSSFCellStyle sourceStyle) throws IOException{
		XSSFCellStyle outputStyle = (XSSFCellStyle) workbook.createCellStyle();
		outputStyle.cloneStyleFrom(sourceStyle);
		outputStyle.setDataFormat(fmt.getFormat("0.0\\%;-0.0\\%;0.0\\%"));
		return outputStyle;
	}
	
	/**
	 * Tao Style chua du lieu kieu phan tram 0.0\%;-0.0\%;"" dua tren sourceStyle
	 * @author datnt43
	 * @param workbook
	 * @param fmt <=  duoc tao ra tu XSSFDataFormat fmt = (XSSFDataFormat) workbook.createDataFormat();
	 * @param sourceStyle
	 * @return
	 * @throws IOException
	 */
	public XSSFCellStyle createPercentageHideZeroFormatStyleFromStyle(SXSSFWorkbook workbook, XSSFDataFormat fmt,XSSFCellStyle sourceStyle) throws IOException{
		XSSFCellStyle outputStyle = (XSSFCellStyle) workbook.createCellStyle();
		outputStyle.cloneStyleFrom(sourceStyle);
		outputStyle.setDataFormat(fmt.getFormat("0.0\\%;-0.0\\%;\"\""));
		return outputStyle;
	}
	
	/**	End SXSSF range	*/
}

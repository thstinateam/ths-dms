/**
 * Copyright 2011 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package ths.dms.web.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jxl.Cell;
import jxl.Sheet;
import ths.dms.helper.Configuration;
import ths.dms.helper.R;
import ths.dms.web.constant.ConstantManager;

import com.ibm.icu.text.SimpleDateFormat;

/**
 * Ham dung chung de kiem tra cac dinh dang
 * 
 * @author hunglm16
 * */
public class ValidateUtil {
	private static final Pattern PATTERN_VALID_CODE = Pattern.compile("^[0-9a-zA-Z._-]+$");
	private static final Pattern PATTERN_VALID_CODE_NEW = Pattern.compile("^[0-9a-zA-Z.-]+$");
	private static final Pattern PATTERN_VALID_CODE_A_Z = Pattern.compile("^[a-zA-Z]+$");
	private static final Pattern PATTERN_INVALID_NAME = Pattern.compile("[<>?\\~#&$%@*()^`\'\"]");
	private static final Pattern PATTERN_INVALID_ADDRESS = Pattern.compile("[<>\\~&\'\"]");
	private static final Pattern PATTERN_INVALID_SPECIAL = Pattern.compile("[<>\\&\'\"]");
	private static final Pattern PATTERN_VALID_EMAIL = Pattern.compile("^[a-zA-Z0-9._-]{1,64}@[A-Za-z0-9]{2,64}(\\.[A-Za-z0-9]{2,64})*(\\.[A-Za-z]{2,4})$");
	private static final Pattern PATTERN_VALID_CAR_NUMBER = Pattern.compile("^[0-9a-zA-Z.-]+$");
	private static final Pattern PATTERN_VALID_ID_NUMBER = Pattern.compile("^[0-9a-zA-Z_]+$");
	private static final Pattern PATTERN_VALID_PAYROLL_ELEMENT_INFO = Pattern.compile("^[0-9a-zA-Z.]+$");
	private static final Pattern PATTERN_SERIAL = Pattern.compile("^[0-9a-zA-Z-_.,^<|>|?|\\|\'|\"|&|~#|$|%|@|*|(|)|^|`]+$");
	private static final Pattern PATTERN_INVALID_NUMBER_CONTRACT = Pattern.compile("[<>?\\~#&$%@*^`\'\"!]");
	private static final Pattern PATTERN_VALID_PHONE_NUMBER = Pattern.compile("^[0-9-.() ]+$");
	private static final Pattern PATTERN_VALID_NUMBER_DOT = Pattern.compile("^[0-9.]+$");
	private static final Pattern PATTERN_VALID_CHAR_UPPER = Pattern.compile("^[A-Z]+$");
	private static final Pattern PATTERN_VALID_CHAR_LOWER = Pattern.compile("^[a-z]+$");

	
	/**
	 * Validate number.
	 * 
	 * @param value
	 *            the value
	 * @return true, if successful
	 */
	public static boolean validateNumber(String value) {
		Pattern p = Pattern.compile("^\\d+$");
		Matcher m = p.matcher(value);

		boolean matchFound = m.matches();
		return matchFound;
	}

	public static boolean validateFloatNumber(String value) {
		Pattern p = Pattern.compile("^[-+]?[0-9]*\\.?[0-9]+$");
		Matcher m = p.matcher(value);

		boolean matchFound = m.matches();
		return matchFound;
	}
	
	/**
     * Validate kieu version 10.3.2.20
     * @author tientv11
     * @param value
     * @return
     */
    public static boolean validateNumberDot(String value) {       
        Matcher m = PATTERN_VALID_NUMBER_DOT.matcher(value);

        boolean matchFound = m.matches();
        return matchFound;
    }

	/**
	 * Validate number Ex.
	 * 
	 * @param value
	 *            the value
	 * @param countPreDot
	 *            the count pre dot
	 * @param countPostDot
	 *            the count post dot
	 * @return true, if successful
	 * @author thongnm
	 * @since Oct 24, 2012
	 */
	public static boolean validateNumberEx(String value, Integer countPreDot, Integer countPostDot) {
		String pattern1 = "";
		String pattern2 = "";
		if (countPreDot == null) {
			if (countPostDot == null) {
				pattern1 = "[0-9]+";
				pattern2 = "[0-9]*\\.[0-9]+";
			} else {
				pattern1 = "[0-9]+";
				pattern2 = "[0-9]*\\.[0-9]{1," + countPostDot + "}";
			}
		} else {
			if (countPostDot == null) {
				pattern1 = "[0-9]{1," + countPreDot + "}";
				pattern2 = "[0-9]{1," + countPreDot + "}\\.[0-9]+";
			} else {
				pattern1 = "[0-9]{1," + countPreDot + "}";
				pattern2 = "[0-9]{1," + countPreDot + "}\\.[0-9]{1," + countPostDot + "}";
			}
		}
		Pattern p = Pattern.compile(pattern1 + "|" + pattern2);
		Matcher m = p.matcher(value);
		boolean matchFound = m.matches();
		return matchFound;
	}

	/**
	 * Validate excel file.
	 * 
	 * @param excelFile
	 *            the excel file
	 * @param excelFileContentType
	 *            the excel file content type
	 * @return the string
	 */
	public static String validateExcelFile(File excelFile, String excelFileContentType) {
		String message = "";
		//Kiem tra duoi file hop le
		if (!FileUtility.isValidExtension(FileUtility.getFileExtension(excelFile), FileUtility.validExcelExtension)) {
			return R.getResource("web.validate.file.error", excelFile.getName());
		}
		if (excelFile != null && (excelFile.length() > Configuration.getExcelUploadMaxSizeByte())) {
			return R.getResource("excel.invalid.size");
		}
//		if (excelFile == null || Configuration.getExcelHeaderFileSupprt().indexOf(excelFileContentType) == -1 || !ValidateUtil.isValidFileType(excelFile, Configuration.getExcelHeaderFileSupprt())) {
//			return R.getResource("excel.invalid.type");
//		}
		if (excelFile == null || Configuration.getExcelHeaderFileSupprt().indexOf(excelFileContentType) == -1 || (FileUtility.getMime(excelFile) != null && Configuration.getExcelHeaderFileSupprt().indexOf(FileUtility.getMime(excelFile)) == -1)) {
			return R.getResource("excel.invalid.type");
		}
		return message;
	}

	/**
	 * Check required data from excel.
	 * 
	 * @param sheet
	 *            the sheet
	 * @param pos
	 *            the pos
	 * @param msg
	 *            the msg
	 * @return the string
	 */
	public static String checkRequiredDataFromExcel(Sheet sheet, String pos, String msg) {
		Cell cell = sheet.getCell(pos);
		if (cell == null || StringUtil.isNullOrEmpty(cell.getContents())) {
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, msg);
		}
		return "";
	}

	/**
	 * Gets the error msg for invalid format date.
	 * 
	 * @param value
	 *            the value
	 * @return the error msg for invalid format date
	 */
	public static String getErrorMsgForInvalidFormatDateDisplayTool(String value, String objName, Boolean flag) {
		boolean error = false;
		if (!StringUtil.isNullOrEmpty(value)) {
			String[] arrDate = value.split("/");
			int day = 0;
			int month = 0;
			int year = 0;
			try {
				if (flag != null && flag) { // Phuongvm
					day = 1;
					month = "0".equals(arrDate[0]) || "00".equals(arrDate[0]) ? 0 : Integer.valueOf(arrDate[0]);
					year = "0".equals(arrDate[1]) || "00".equals(arrDate[1]) || "000".equals(arrDate[1]) || "0000".equals(arrDate[1]) ? 0 : Integer.valueOf(arrDate[1]);
				} else {
					day = "0".equals(arrDate[0]) || "00".equals(arrDate[0]) ? 0 : Integer.valueOf(arrDate[0]);
					month = "0".equals(arrDate[1]) || "00".equals(arrDate[1]) ? 0 : Integer.valueOf(arrDate[1]);
					year = "0".equals(arrDate[2]) || "00".equals(arrDate[2]) || "000".equals(arrDate[2]) || "0000".equals(arrDate[2]) ? 0 : Integer.valueOf(arrDate[2]);
				}

			} catch (Exception e) {
				error = true;
			}
			if (year <= 0 || year > 9999) {
				error = true;
			}
			if (month > 0 && month <= 12) {
				if (month == 2) {
					if (day > 29 || (!(year % 4 == 0 || (year % 400 == 0 && year % 100 != 0)) && day > 28)) {
						error = true;
					}
				} else if (day > DateUtil.arrDays[month - 1]) {
					error = true;
				}
			} else {
				error = true;
			}
			if (day <= 0) {
				error = true;
			}
			if (error == false) { // Phuong vm
				if (flag != null && flag) {
					if (DateUtil.parse(value, DateUtil.DATE_D_M) == null) {
						error = true;
					}
				} else {
					if (DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY) == null) {
						error = true;
					}
				}
			}
		}
		if (error) {
			if (flag != null && flag) {
				return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.month", objName) + "\n";
			} else {
				return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date", objName) + "\n";
			}
		}
		return "";
	}

	/**
	 * Gets the error msg for invalid format date.
	 * 
	 * @param value
	 *            the value
	 * @return the error msg for invalid format date
	 */
	public static String getErrorMsgForInvalidFormatDate(String value, String objName, Boolean flag) {
		boolean error = false;
		objName = R.getResource(objName);
		if (!StringUtil.isNullOrEmpty(value)) {
			String[] arrDate = value.split("/");
			int day = 0;
			int month = 0;
			int year = 0;
			try {
				day = Integer.valueOf(arrDate[0]);
				month = Integer.valueOf(arrDate[1]);
				year = Integer.valueOf(arrDate[2]);
			} catch (Exception e) {
				error = true;
			}
			if (year < 0 || year > 9999) {
				error = true;
			}
			if (month > 0 && month <= 12) {
				if (month == 2) {
					if (day > 29 || (!(year % 4 == 0 || (year % 400 == 0 && year % 100 != 0)) && day > 28)) {
						error = true;
					}
				} else if (day > DateUtil.arrDays[month - 1]) {
					error = true;
				}
			} else {
				error = true;
			}
			if (day <= 0) {
				error = true;
			}
			if (error == false && DateUtil.parse(value, DateUtil.DATE_FORMAT_DDMMYYYY) == null) {
				error = true;
			}
		}
		if (error) {
			if (flag != null && flag) {
				return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.month", objName) + "\n";
			} else {
				return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.date", objName) + "\n";
			}
		}
		return "";
	}

	/**
	 * Gets the error msg for invalid to date.
	 * 
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the error msg for invalid to date
	 */
	public static String getErrorMsgForInvalidToDate(Date fromDate, Date toDate) {
		if (fromDate != null && toDate != null && DateUtil.compareDateWithoutTime(fromDate, toDate) == 1) {
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.fromdate.greater.todate") + "\n";
		}
		return "";
	}

	/**
	 * Gets the error msg lot format.
	 * 
	 * @param value
	 *            the value
	 * @param objName
	 *            the obj name
	 * @return the error msg lot format
	 */
	public static String getErrorMsgLotFormat(String value, String objName, String format) {
		boolean error = false;
		if (!StringUtil.isNullOrEmpty(value)) {
			if (value.length() < 6) {
				return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.format.invalid", objName) + "\n";
			}
			int day = 0;
			int month = 0;

			try {
				day = Integer.valueOf(value.substring(0, 2));
				month = Integer.valueOf(value.substring(2, 4));
			} catch (Exception e) {
				error = true;
			}
			if (month > 0 && month <= 12) {
				if (day > DateUtil.arrDays[month - 1]) {
					error = true;
				}
			} else {
				error = true;
			}
			if (error == false && DateUtil.parse(value, "ddMMyy") == null) {
				error = true;
			}
		}
		if (error) {
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "stock.approve.lot.invalid.format", objName, format) + "\n";
		}
		return "";
	}

	/**
	 * Gets the error msg.
	 * 
	 * @param errorType
	 *            the error type
	 * @param params
	 *            the params
	 * @return the error msg *
	 */
	public static String getErrorMsg(int errorType, Object... params) {
		String key = "";
		// get template message name by error type
		switch (errorType) {
		// loi he thong
		case ConstantManager.ERR_SYSTEM:
			key = "system.error";
			break;
		// loi du lieu. khong ton tai don vi
		case ConstantManager.ERR_SHOP_NO_DATA:
			key = "common.no.data.shop.error";
			break;
		// loi npp khong thuoc quyen user
		case ConstantManager.ERR_INVALID_SHOP:
			key = "common.shop.invalid.user";
			break;
		case ConstantManager.ERR_SHOP_NOT_BELONG_TO_USER:
			key = "common.shop.not.belong.user";
			break;
		// loi khong co quyen
		case ConstantManager.ERR_NOT_PERMISSION:
			key = "common.permission";
			break;
		case ConstantManager.ERR_DISPLAY_TOOL_PRODUCT_DATA_EXIST:
			key = "catalog.display.tool.product.not.exist.not.del";
			break;
		case ConstantManager.ERR_INCENTIVE_PROGRAM_NOT_EXIST_OR_DELETE:
			key = "common.incentive.program.is.delete.or.not.exist";
			break;
		case ConstantManager.ERR_INVALID_PRODUCT_FOR_CUSTOMER_AND_STAFF:
			key = "sp.invalid.product.for.customer.and.staff";
			break;
		case ConstantManager.ERR_SP_LOT_QUANTITY:
			key = "sp.search.sale.order.approved.lot.quantity";
			break;
		case ConstantManager.ERR_INCENTIVE_LEVEL_NOT_EXIST_OR_DELETE:
			key = "common.incentive.level.is.delete.or.not.exist";
			break;
		case ConstantManager.ERR_PRODUCT_GROUP:
			key = "catalog.display.program.quota.group.sanpham.nhomchitieu";
			break;
		case ConstantManager.ERR_PARENT_AREA_IN_WARD_TYPE:
			key = "catalog.area.parent.in.ward.type";
			break;
		case ConstantManager.ERR_PRODUCT_NOT_IN_STOCK_TOTAL:
			key = "common.available.product.not.in.stock.total";
			break;
		case ConstantManager.ERR_PRODUCT_NOT_IN_STOCK_TOTAL_PROMOTION:
			key = "common.available.product.not.in.stock.total.promotion";
			break;
		case ConstantManager.ERR_NUMBER_DOT:
			key = "common.invalid.format.tf.num.dot";
			break;
		case ConstantManager.ERR_SP_CREATE_ORDER_CUSTOMER_EXCEED_MAX_DEBIT:
			key = "sp.create.order.customer.exceed.max.debit";
			break;
		// loi khong ton tai common.exist.in.ECat
		case ConstantManager.ERR_NOT_EXIST_DB:
			key = "common.not.exist.in.db";
			break;
		case ConstantManager.ERR_SALE_ORDER_NOT_EXIST_NVGH:
			key = "common.sale.order.not.exist.nvgh";
			break;
		// Ma GSNPP {0} khong ton tai
		case ConstantManager.ERR_NOT_EXIST_GSNPP:
			key = "common.not.correct.code.gsnpp";
			break;
		//Ma NVBH {0} khong ton tai
		case ConstantManager.ERR_NOT_EXIST_NVBH:
			key = "common.not.correct.code.nvbh";
			break;
		//Ma Mien {0} khong ton tai
		case ConstantManager.ERR_NOT_EXIST_MIEN:
			key = "common.not.correct.code.mien.not.db";
			break;
		//Ma Vung {0} khong ton tai
		case ConstantManager.ERR_NOT_EXIST_VUNG:
			key = "common.not.correct.code.vung.not.db";
			break;
		//Ma NVBH {0} khong ton tai
		case ConstantManager.ERR_NOT_EXIST_NPP:
			key = "common.not.correct.code.npp.not.db";
			break;
		//Ma Mien {0} khong phai lai don vi mien
		case ConstantManager.ERR_NOT_EXIST_SHOP_MIEN:
			key = "common.not.correct.shop.not.mien";
			break;
		//Ma vung {0} khong phai lai don vi vung
		case ConstantManager.ERR_NOT_EXIST_SHOP_VUNG:
			key = "common.not.correct.shop.not.vung";
			break;
		//Ma npp {0} khong phai lai don vi npp
		case ConstantManager.ERR_NOT_EXIST_SHOP_NPP:
			key = "common.not.correct.shop.not.npp";
			break;
		case ConstantManager.ERR_EXIST_ECat:
			key = "common.exist.in.ECat";
			break;
		case ConstantManager.ERR_EXIST_ECatImport:
			key = "common.exist.in.ECatImport";
			break;
		case ConstantManager.ERR_NOT_EXIST_TOGETHER_DB:
			key = "common.not.exist.together.in.db";
			break;
		case ConstantManager.ERR_NOT_EXIST_AND_NOT_RUNNING:
			key = "common.not.exist.and.not.running";
			break;
		case ConstantManager.ERR_NOT_EXIST_GENERAL:
			key = "common.not.exist.general";
			break;
		case ConstantManager.ERR_CANT_CHANGE_PARENT:
			key = "catalog.area.exist.child.code.parent.change";
			break;
		case ConstantManager.ERR_SP_CREATE_ORDER_CUSTOMER_EXCEED_MAX_DAY:
			key = "sp.create.order.customer.exceed.max.day";
			break;
		case ConstantManager.ERR_SP_STATUS_INVOICE_NOT_EXISTS:
			key = "sp.adjust.order.status.invoice.not.exist";
			break;
		case ConstantManager.ERR_CANT_PARENT_MYSELF:
			key = "catalog.area.parent.equat.myself";
			break;
		case ConstantManager.ERR_CHILD_SHOP_IS_RUNNING:
			key = "common.catalog.shop.can.not.update.child.shop.exist";
			break;
		case ConstantManager.ERR_DISPLAY_TOOL_PRODUCT_EXIST:
			key = "catalog.display.tool.product.exist";
			break;
		case ConstantManager.ERR_NOT_LOGIN:
			key = "common.not.login";
			break;
		case ConstantManager.ERR_NOT_EXIST:
			key = "common.not.exist";
			break;
		case ConstantManager.ERR_STAFF_HAS_SUPERVISE:
			key = "common.staff.has.supervise";
			break;
		case ConstantManager.ERR_NOT_INVALID:
			key = "common.not.shop.code";
			break;
		case ConstantManager.ERR_NOT_INVALID_TYPE_SHOP:
			key = "common.not.shop.type";
			break;
		case ConstantManager.ERR_REGION_IMPORT_EXCEL:
			key = "common.cargo.brand.distributer.import.region";
			break;
		case ConstantManager.ERR_FROM_DATE_NOT_GREATER_TO_DATE:
			key = "common.from.date.not.greater.to.date";
			break;
		case ConstantManager.ERR_STAFF_NOT_BELONG_SHOP:
			key = "common.staff.not.belong.shop";
			break;
		case ConstantManager.ERR_CAR_NOT_BELONG_SHOP:
			key = "sale.product.car.not.belong.to.shop";
			break;
		case ConstantManager.ERR_STATUS_NOT_IN_WAITING:
			key = "common.catalog.status.not.waiting";
			break;
		// loi ton tai
		case ConstantManager.ERR_EXIST:
			key = "common.exist.code";
			break;
		case ConstantManager.ERR_PO_DVKH_ALREADY_SUSPEND:
			key = "po.dvkh.already.suspend.error";
			break;
		case ConstantManager.ERR_PRODUCT_NOT_BELONG_CATEGORY:
			key = "common.product.category.not.belong.category";
			break;
		case ConstantManager.ERR_NO_SELECT_DATA:
			key = "common.no.select.data";
			break;
		case ConstantManager.ERR_SALE_EXIST_INVOICE_OR_NOT_PERMISSION:
			key = "sp.adjust.order.not.exist.invoice";
			break;
		case ConstantManager.ERR_NO_SELECT:
			key = "common.no.select.value";
			break;
		case ConstantManager.ERR_SHOP_TYPE_NOT_EXIST:
			key = "catalog.shop.type.not.exist";
			break;
		case ConstantManager.ERR_CUSTOMER_EXIST_SKU:
			key = "catalog.sale.customer.type.exist.sku";
			break;
		case ConstantManager.ERR_FOCUS_PROGRAM_EXIST_ADD:
			key = "catalog.focus.program.sale.type.exist.add";
			break;
		case ConstantManager.ERR_FOCUS_PROGRAM_EXIST_DELETE:
			key = "catalog.focus.program.sale.type.exist.delete";
			break;
		case ConstantManager.ERR_FOCUS_PROGRAM_EXIST_PRODUCT:
			key = "catalog.focus.program.product.exist";
			break;
		//Mã GSNPP: {0} đang ở trạng thái không hoạt động.
		case ConstantManager.ERR_STATUS_INACTIVE_GSNPP:
			key = "common.not.staffowner.code.status.not.active";
			break;
		//Mã NVBH: {0} đang ở trạng thái không hoạt động.
		case ConstantManager.ERR_STATUS_INACTIVE_NVBH:
			key = "common.not.staffsale.code.status.not.active";
			break;
		// loi trang thai bi tam ngung
		case ConstantManager.ERR_STATUS_INACTIVE:
			key = "common.catalog.status.in.active";
			break;
		case ConstantManager.ERR_SHOP_NOT_ACTIVE:
			key = "common.catalog.shop.status.not.active";
			break;
		case ConstantManager.ERR_STAFF_NOT_ACTIVE:
			key = "common.catalog.staff.status.not.active";
			break;
		case ConstantManager.ERR_CUSTOMER_NOT_ACTIVE:
			key = "common.catalog.customer.status.not.active";
			break;
		case ConstantManager.ERR_SHOP_TYPE_INVALID:
			key = "catelog.shop.type.invalid";
			break;
		// loi bat buoc nhap
		case ConstantManager.ERR_REQUIRE:
			key = "common.required.field";
			break;
		case ConstantManager.ERR_NOT_REQUIRE:
			key = "common.not.required";
			break;
		// dang duoc su dung
		case ConstantManager.ERR_IS_USED:
			key = "common.is.used";
			break;
		case ConstantManager.ERR_IS_USED2:
			key = "catalog.focus.program.is.running";
			break;
		case ConstantManager.ERR_IS_USED3:
			key = "catalog.focus.program.can.not.update";
			break;
		case ConstantManager.ERR_STOP_PARENT:
			key = "common.has.stop.parent";
			break;
		case ConstantManager.ERR_NOT_BELONG_ANY_SHOP:
			key = "common.not.belong.any.shop";
			break;
		case ConstantManager.ERR_SHOP_NOT_BELONG_AREA:
			key = "common.shop.not.belong.area";
			break;
		case ConstantManager.ERR_NOT_BELONG_USERLOGIN:
			key = "common.not.belong.userlogin";
			break;
		case ConstantManager.ERR_DEVICE_IN_USE:
			key = "device.in.use";
			break;
		//khong phai la so nguyen duong
		case ConstantManager.ERR_NOT_INT:
			key = "common.not.int";
			break;
		case ConstantManager.ERR_NOT_FLOAT:
			key = "common.not.float";
			break;
		case ConstantManager.ERR_INVALID_DATE:
			key = "common.invalid.date";
			break;
		case ConstantManager.ERR_EXISTS_BEFORE:
			key = "catalog.exists.before";
			break;
		case ConstantManager.ERR_NOT_EXISTS_BEFORE:
			key = "catalog.not.exists.before";
			break;
		case ConstantManager.ERR_USER_INPUT:
			key = "common.user.input";
			break;
		case ConstantManager.ERR_DEBIT_SUPLIER_INVALID_MONEY:
			key = "suplier.debit.manage.distribute.money.error";
			break;
		case ConstantManager.ERR_DEBIT_SUPLIER_RECEIVE_MONEY:
			key = "suplier.debit.manage.need.receive.money.error";
			break;
		case ConstantManager.ERR_DEBIT_SUPLIER_PAYMENT_MONEY:
			key = "suplier.debit.manage.need.payment.money.error";
			break;
		case ConstantManager.ERR_DEBIT_SUPLIER_DONT_HAVE_RETURN_ORDER:
			key = "suplier.debit.manage.dont.have.return.order";
			break;
		case ConstantManager.ERR_DEBIT_SUPLIER_DONT_HAVE_INCOME_ORDER:
			key = "suplier.debit.manage.dont.have.income.order";
			break;
		case ConstantManager.ERR_DEBIT_SUPLIER_MONEY_ERR:
			key = "suplier.debit.manage.need.pay.money.error";
			break;
		case ConstantManager.ERR_FORMAT_INVALID:
			key = "common.format.invalid";
			break;
		case ConstantManager.ERR_NOT_NUMBER:
			key = "common.not.number";
			break;
		case ConstantManager.ERR_NOT_POSSITIVE_NUMBER:
			key = "common.not.possitive.number";
			break;
		case ConstantManager.ERR_NOT_PERCENT_NUMBER:
			key = "common.not.percent.number";
			break;
		case ConstantManager.ERR_FLOAT_FORMAT:
			key = "common.float.format";
			break;
		case ConstantManager.ERR_NETGROSS_WEIGHT:
			key = "catalog.product.netgross";
			break;
		case ConstantManager.ERR_FLOAT_FORMAT_PRE:
			key = "common.float.format.pre";
			break;
		//het hieu luc
		case ConstantManager.ERR_NOT_EFFECT:
			key = "common.promotion.effect";
			break;
		case ConstantManager.ERR_NOT_CORRECT:
			key = "common.not.correct";
			break;
		case ConstantManager.ERR_NOT_DATA_CORRECT:
			key = "common.not.data.correct";
			break;
		case ConstantManager.ERR_FIELD:
			key = "common.promotion.field";
			break;
		case ConstantManager.ERR_NOT_SHOP_CODE:
			key = "common.not.shop.code";
			break;
		case ConstantManager.ERR_NOT_POSSITIVE_INT:
			key = "common.not.int0";
			break;
		case ConstantManager.ERR_NOT_UOM1:
			key = "catalog.product.uom1.uom2";
			break;
		case ConstantManager.ERR_NOT_GREATER:
			key = "catalog.product.greater.netweight";
			break;
		case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE:
			key = "common.invalid.format.code";
			break;
		case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE_NEW:
			key = "common.invalid.format.code.new";
			break;
		case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME:
			key = "common.invalid.format.name";
			break;
		case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS:
			key = "common.invalid.format.address";
			break;
		case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL:
			if (params == null || params.length == 0) {
				key = "common.invalid.g.special";
			} else {
				key = "common.invalid.format.special";
			}
			break;
		case ConstantManager.ERR_INVALID_EMAIL:
			key = "common.invalid.format.email";
			break;
		case ConstantManager.ERR_FORMAT_LOT:
			key = "common.invalid.format.lot";
			break;
		case ConstantManager.ERR_FORMAT_NUMBER_CONVFACT:
			key = "common.invalid.format.number.convfact";
			break;
		//common.maxlength = Giá trị {0} nhập vào phải nhỏ hơn {1} ký tự.
		case ConstantManager.ERR_MAX_LENGTH:
			key = "common.maxlength";
			break;
		//common.maxlength.equals = Giá trị {0} nhập vào phải nhỏ hơn bằng {1} ký tự.
		case ConstantManager.ERR_MAX_LENGTH_EQUALS:
			key = "common.maxlength.equals";
			break;
		case ConstantManager.ERR_INVALID_LENGTH:
			key = "common.invalid.length";
			break;
		case ConstantManager.ERR_INTEGER:
			key = "common.integer";
			break;
		case ConstantManager.ERR_INTEGER_MAXVALUE:
			key = "common.integer.maxvalue";
			break;
		case ConstantManager.ERR_INTEGER_ZERO:
			key = "common.integer.zero";
			break;
		case ConstantManager.ERR_INTEGER_ZERO_OR_ONE:
			key = "common.integer.zero.or.one";
			break;
		case ConstantManager.ERR_POSSITIVE_INTEGER:
			key = "common.possitive.integer";
			break;
		case ConstantManager.ERR_PO_STOCKIN:
			key = "po.stock.in.stockin";
			break;
		case ConstantManager.ERR_PO_STOCKIN_FINISH:
			key = "po.stockin.finish";
			break;
		case ConstantManager.ERR_PO_STOCKIN_SUSPEND:
			key = "po.stockin.suspend";
			break;
		case ConstantManager.ERR_PO_RETURNPRODUCT_CHECKSTOCKLOT:
			key = "po.returnproduct.return.checkstocklot";
			break;
		case ConstantManager.ERR_PO_RETURNPRODUCT_CHECKSTOCK:
			key = "po.returnproduct.return.checkstock";
			break;
		case ConstantManager.ERR_STOCKTRAIN:
			key = "catalog.stock.trans.null";
			break;
		case ConstantManager.ERR_CUSTAUTO_UPDATE:
			key = "customerdebit.auto.update.error";
			break;
		case ConstantManager.ERR_ORDER_NOT_IN_TODAY:
			key = "sp.search.sale.order.not.in.today.approved.error";
			break;
		case ConstantManager.ERR_ORDER_CHOOSE_LOT:
			key = "sp.search.sale.order.choose.lot.approved.error";
			break;
		//Loi don hang
		case ConstantManager.ERR_SALEORDER_RETURNED:
			key = "common.returned";
			break;
		//Loi quy dinh
		case ConstantManager.ERR_LAW:
			key = "common.error.law";
			break;

		case ConstantManager.ERR_CONFIRM_ORDER_TABLET:
			key = "sale.product.confirm.order.tablet.error";
			break;
		case ConstantManager.ERR_MIN_MAX:
			key = "catalog.compare.minSF.maxSF";
			break;
		case ConstantManager.ERR_MAX_LENGTH_INT:
			key = "common.not.int.maxlength";
			break;
		case ConstantManager.ERR_MAX_LENGTH_FLOAT:
			key = "common.not.float.maxlength";
			break;
		case ConstantManager.ERR_CATALOG_DAYGO:
			key = "catalog.daygo.not.format";
			break;
		case ConstantManager.ERR_CATALOG_PROMOTION_EXITS_RUNNING:
			key = "catalog.promotion.exist.running";
			break;
		case ConstantManager.ERR_CATALOG_PRODUCT_PRICE_VAT_NOT_EQUAL:
			key = "catalog.product.price.vat.not.equal";
			break;
		case ConstantManager.ERR_NOT_ZERO:
			key = "common.not.zero";
			break;

		case ConstantManager.ERR_MAX_LENGTH_DAY_SALE:
			key = "common.not.daysale.maxlength";
			break;
		case ConstantManager.ERR_MAX_LENGTH_GROWTH:
			key = "common.not.growth.maxlength";
			break;
		case ConstantManager.ERR_MAX_LENGTH_MINMAX:
			key = "common.not.minmax.maxlength";
			break;
		//Loi giao tuyen
		case ConstantManager.ERR_DATE_VISIT_PLANT_EXISTS:
			key = "ss.assign.route.add.staff.error.staff";
			break;

		case ConstantManager.ERR_FDATE_TDATE:
			key = "ss.display.program.todate.fromdate";
			break;
		case ConstantManager.ERR_SEQ_ROUTING_CUSTOMER:
			key = "ss.assign.route.set.order.error.seq";
			break;
		case ConstantManager.ERR_SEQ_DUPLICATE_ROUTING_CUSTOMER:
			key = "ss.assign.route.set.order.error.seq.duplicate";
			break;
		case ConstantManager.ERR_SEQ_LESS_THAN_ZERO:
			key = "ss.assign.route.set.order.error.seq.lessthan.zero";
			break;
		case ConstantManager.ERR_SEQ_NULL:
			key = "ss.assign.route.set.order.error.seq.null";
			break;
		case ConstantManager.ERR_SEQ_MAX_CREATE_ROUTE:
			key = "ss.assign.route.set.order.error.seq.max";
			break;
		case ConstantManager.ERR_START_WEEK_MAX_CREATE_ROUTE:
			key = "ss.assign.route.set.order.error.startweek.max";
			break;
		case ConstantManager.ERR_SEQ_START_WEEK_NOT_NULL:
			key = "ss.assign.route.set.order.error.seq.startweek.null";
			break;
		case ConstantManager.ERR_DATA_NULL:
			key = "ss.assign.route.set.order.error.data.null";
			break;
		case ConstantManager.ERR_SP_PROMOTION:
			key = "sp.create.order.promotion.effect";
			break;
		case ConstantManager.ERR_QUANTITY_STOCK_TOTAL:
			key = "common.available.quantity.and.stock.total";
			break;
		case ConstantManager.ERR_SP_PROMOTION_SHOP:
			key = "sp.create.order.promotion.map.shop.error1";
			break;
		case ConstantManager.ERR_SP_PROMOTION_SHOP_FULL:
			key = "sp.create.order.promotion.map.shop.error2";
			break;
		case ConstantManager.ERR_SP_PROMOTION_CUS:
			key = "sp.create.order.promotion.map.customer.error1";
			break;
		case ConstantManager.ERR_SP_PROMOTION_CUS_FULL:
			key = "sp.create.order.promotion.map.customer.error2";
			break;
		case ConstantManager.ERR_STOCK_CC:
			key = "stock.countinginput.map.product.is.exists";
			break;
		case ConstantManager.ERR_PO_POAUTO_NOT_YET:
			key = "po.manage.poauto.status.not.yet.error";
			break;
		case ConstantManager.ERR_SP_EXIST_INVOICE_NUMBER:
			key = "sale.product.adjust.tax.exist.invoice.number.error";
			break;
		case ConstantManager.ERR_SP_DUPLICATE_INVOICE_NUMBER:
			key = "sale.product.adjust.tax.duplicate.invoice.number";
			break;
		case ConstantManager.ERR_SP_SALE_ORDER_NEED_TO_UPDATE_DELIVERY:
			key = "sale.product.sale.need.to.update.delivery";
			break;
		case ConstantManager.ERR_SP_NEW_INVOICE_EQUAL_EXIST_INVOICE:
			key = "sale.product.adjust.tax.new.invoice.equal.exist.invoice";
			break;
		case ConstantManager.ERR_STOCK_NOT_LOT:
			key = "stock.update.stock.product.not.lot";
			break;
		case ConstantManager.ERR_PAUSE_STATUS:
			key = "pause.status.value";
			break;
		case ConstantManager.ERR_STATUS_IN_WAITING:
			key = "common.catalog.program.is.not.waiting.status";
			break;
		case ConstantManager.ERR_STATUS_IN_RUNNING:
			key = "common.catalog.program.is.not.running.status";
			break;
		case ConstantManager.ERR_QUANTITY_STOCK_TOTAL2:
			key = "common.available.quantity.and.stock.total2";
			break;
		case ConstantManager.ERR_QUANTITY_STOCK_TT_PROMOTION:
			key = "common.available.quantity.and.stock.total.promotion";
			break;
		case ConstantManager.ERR_QUANTITY_STOCK_TT_PROMOTION_RE_PAYMENT:
			key = "common.available.quantity.and.stock.total.promotion.re.payment";
			break;
		case ConstantManager.ERR_STATUS_PAUSE:
			key = "common.status.pause";
			break;
		case ConstantManager.ERR_QUANTITY_STOCK_TT_PROMOTION2:
			key = "common.available.quantity.and.stock.total.promotion2";
			break;
		case ConstantManager.ERR_INVALID_FORMAT:
			key = "common.invalid.format";
			break;
		case ConstantManager.ERR_INVALID_FORMAT_CHARACTER:
			key = "common.invalid.format.character";
			break;

		case ConstantManager.ERR_STOCK_RECEIVED_PRODUCT_VANSALES:
			key = "stock.received.vansale.stock.total";
			break;
		case ConstantManager.ERR_STOCK_RECEIVED_LOT_VANSALES:
			key = "stock.received.vansale.stock.total.lot";
			break;

		case ConstantManager.ERR_SS_GREATER_DATENOW_GENERAL:
			key = "ss.greater.datenow.general";
			break;
		case ConstantManager.ERR_SS_NOT_GENERAL:
			key = "ss.not.general";
			break;
		case ConstantManager.ERR_SS_NOT_BELONG_GENERAL:
			key = "ss.not.belong.general";
			break;
		case ConstantManager.ERR_SS_TRANINGPLAN_NOT_SAME_USERLOGIN_GENERAL:
			key = "ss.traningplan.not.same.userlogin.general";
			break;
		case ConstantManager.ERR_SS_TRANINGPLAN_NOT_SAME_SHOP_STAFFSALE:
			key = "ss.traningplan.not.same.shop.staffsale";
			break;

		case ConstantManager.ERR_SS_TRANINGPLAN_GSNPP_NOT_MANAGE_SALESTAFF:
			key = "ss.traningplan.gsnpp.not.manage.salestaff";
			break;
		case ConstantManager.ERR_SS_TRANINGPLAN_GSNPP_NOT_MANAGE_SALESTAFF_SHOP:
			key = "ss.traningplan.gsnpp.not.managestaffsale.shop";
			break;
		case ConstantManager.ERR_SS_TRANINGPLAN_NOT_TRAIN_DATE:
			//			key="ss.traningplan.gsnpp.not.train.date";
			key = "ss.traningplan.gsnpp.not.train.date.tbhv";
			break;
		case ConstantManager.ERR_SS_TRANINGPLAN_NOT_TRAIN_SHOP_DATE:
			key = "ss.traningplan.gsnpp.not.train.shop.date";
			break;
		case ConstantManager.ERR_SS_TRANINGPLAN_CANCEL_TRAINED_DATE:
			key = "ss.traningplan.gsnpp.cancel.train.date";
			break;
		// Chỉ phân bổ cho tháng hiện tại thực tế {0} và các tháng tiếp theo
		case ConstantManager.ERR_MONTH_CREARE_SALE_PLAN:
			key = "sale.plan.qm.month.create.sale.plan";
			break;
		//me
		case ConstantManager.ERR_DEBIT_BATCH_NOT_IN_SHOP:
			key = "customerdebit.batch.import.not.in.shop";
			break;
		case ConstantManager.ERR_DEBIT_BATCH_MONEY_NOT_GREATER_ZERO:
			key = "customerdebit.batch.import.money.not.greater.zero";
			break;
		case ConstantManager.ERR_DEBIT_BATCH_NOT_EXIST_DEBIT:
			key = "customerdebit.batch.not.exist.debit";
			break;
		case ConstantManager.ERR_ATTRIBUTE_NOT_EXISTS:
			key = "attribute.customer.code.not.exist";
			break;
		case ConstantManager.ERR_ATTRIBUTE_DETAIL_NOT_EXISTS:
			key = "attribute.customer.detail.code.not.exist";
			break;
		case ConstantManager.ERR_SS_NOT_BELONG_SHOPUSER:
			key = "ss.not.belong.shopuser";
			break;
		case ConstantManager.ERR_STOCK_APPROVE_COUNTDATE_NOT_COMPELETE:
			key = "stock.approve.cyclecount.countdate.not.complete";
			break;
		case ConstantManager.ERR_STOCK_INPUT_IMPORTEXCEL_LOT_REQUIRE:
			key = "stock.countinginput.importexcel.lot.require";
			break;
		case ConstantManager.ERR_STOCK_INPUT_IMPORTEXCEL_LOT_NOTREQUIRE:
			key = "stock.countinginput.importexcel.lot.notrequire";
			break;
		case ConstantManager.ERR_STOCK_INPUT_EXIT_INVENTORY:
			key = "stock.countinginput.exist.inventory";
			break;
		case ConstantManager.ERR_STOCK_INPUT_EXIST_LIST:
			key = "stock.countinginput.exist.list";
			break;
		case ConstantManager.ERR_STOCK_INPUT_DUPLICATE:
			key = "stock.countinginput.duplicate";
			break;
		case ConstantManager.ERR_FLOAT_NUMBER:
			key = "common.possitive.number";
			break;
		case ConstantManager.ERR_NOT_IN_SHOP:
			key = "common.not.exist.in.shop";
			break;
		case ConstantManager.ERR_NUMBER:
			key = "common.number";
			break;
		case ConstantManager.ERR_DUPLICATE_IS_RUNNING:
			key = "common.err.duplicate";
			break;
		case ConstantManager.ERR_LOT_DESTROY:
			key = "sp.create.order.product.lot.avaliable.quantity";
			break;
		case ConstantManager.ERR_LOT_SALE:
			key = "sp.create.order.product.sale.lot.avaliable.quantity";
			break;
		case ConstantManager.ERR_LOT_PROMOTION:
			key = "sp.create.order.product.promotion.lot.avaliable.quantity";
			break;
		case ConstantManager.ERR_IS_USED_DB:
			key = "common.is.used.db";
			break;
		case ConstantManager.ERR_NOT_SALE_STAFF:
			key = "catalog.not.nvbh";
			break;
		case ConstantManager.ERR_LOT_DESTROY2:
			key = "sp.create.order.product.lot.destroy.err";
			break;
		case ConstantManager.ERR_CATALOG_PROMOTION_QUANTITY_ERROR:
			key = "catalog.promotion.quantity.error";
			break;
		case ConstantManager.ERR_CATALOG_PROMOTION_QUANTITY_MAX:
			key = "catalog.promotion.quantity.max";
			break;
		case ConstantManager.ERR_CUS_MAP:
			key = "catalog.display.program.customer.display.map";
			break;
		case ConstantManager.ERR_PRODUCT_MAP:
			key = "catalog.display.program.customer.display.product";
			break;
		case ConstantManager.ERR_IS_USED_1:
			key = "common.is.used.1";
			break;
		case ConstantManager.ERR_DPPAYPERIOD_DP:
			key = "pay.period.code.checked.exists";
			break;
		case ConstantManager.ERR_PERMISSION_CHANGE:
			key = "common.permission.change";
			break;
		case ConstantManager.ERR_DATA_NOT_FULL:
			key = "common.data.not.full";
			break;
		case ConstantManager.ERR_NOT_BELONG_STAFF:
			key = "common.shop.not.belong.staff";
			break;

		case ConstantManager.ERR_QUANTIY_HAS:
			key = "has.not.enough.quantity";
			break;

		case ConstantManager.STATUS_UPDATE:
			key = "acton.type.status.update";
			break;

		case ConstantManager.STATUS_DELETE:
			key = "action.type.status.delete";
			break;

		case ConstantManager.STATUS_CREATE:
			key = "action.type.status.create";
			break;
		case ConstantManager.ERR_IMAGES_CHECK_DATE:
			key = "images.check.date";
			break;
		case ConstantManager.ERR_QUANTIY_HAS_LOT:
			key = "lot.has.not.enough.quantity";
			break;
		case ConstantManager.ERR_ROUTING_INVALID:
			key = "ss.set.order.routing.invalid";
			break;
		case ConstantManager.ERR_ORDER_DATE_INVALID:
			key = "sp.return.product.order.date.invalid";
			break;
		case ConstantManager.ERR_STOCK_STQ_QUANTITY:
			key = "stock.received.product.quantity.stock.total";
			break;
		case ConstantManager.ERR_STOCK_UPDATE_LOT_EXPIRE:
			key = "stock.update.validate.lot.expire";
			break;
		case ConstantManager.ERR_ATTRIBUTE_NOT_EXIST:
			key = "attribute.not.exist";
			break;
		case ConstantManager.ERR_INCENTIVE_MULTIPLE_RECURSIVE_INVALID:
			key = "incentive.program.error.multiple.recursive";
			break;
		case ConstantManager.ERR_INCENTIVE_FREE_PRODUCT_NOT_RUNNING:
			key = "incentive.program.free.product.not.running";
			break;
		case ConstantManager.ERR_STAFF_OWNER:
			key = "ss.assign.error.staff.owner";
			break;
		case ConstantManager.ERR_INCENTIVE_ERROR_FDATE_TODATE:
			key = "incentive.program.error.todate.fromdate";
			break;
		case ConstantManager.ERR_INCENTIVE_INVALID_FDATE_TDATE:
			key = "incentive.product.invalid.fdate.tdate";
			break;
		case ConstantManager.ERR_INCENTIVE_INVALID_FDATE:
			key = "incentive.product.invalid.fdate";
			break;
		case ConstantManager.ERR_INCENTIVE_INVALID_TDATE:
			key = "incentive.product.invalid.tdate";
			break;
		case ConstantManager.ERR_INCENTIVE_INVALID_PRODUCT:
			key = "incentive.product.invalid";
			break;
		case ConstantManager.ERR_INVALID:
			key = "contract.invalid";
			break;
		case ConstantManager.ERR_INCENTIVE_PROGRAM_EXISTS:
			key = "incentive.program.exists";
			break;
		case ConstantManager.ERR_INCENTIVE_PRODUCT_STOPPED:
			key = "incentive.product.stopped";
			break;
		case ConstantManager.ERR_INCENTIVE_PRODUCT_LEVEL_INVALID:
			key = "contract.invalid";
			break;
		case ConstantManager.ERR_STOCK_UPDATE_HAS_NOT_EN:
			key = "stock.received.quantity.has.not.en";
			break;
		case ConstantManager.ERR_STOCK_UPDATE_HAS_NOT_EN_LOT:
			key = "stock.received.quantity.has.not.en.lot";
			break;
		case ConstantManager.ERR_INCENTIVE_LEVEL_EXISTS:
			key = "incentive.level.exists";
			break;
		case ConstantManager.ERR_PAYROLL_NOT_ENOUGH:
			key = "payroll.not.enough";
			break;
		case ConstantManager.ERR_PAYROLLT_NOT_EXISTS:
			key = "payroll.payrollt.not.exists";
			break;
		case ConstantManager.ERR_STORE_NAME_NOT_EXISTS:
			key = "payroll.store.name.not.exists";
			break;
		case ConstantManager.ERR_DP_PERIOD_STOCK:
			key = "stock.received.vansale.stock.total.of.shop";
			break;
		case ConstantManager.ERR_DP_PERIOD_STOCK_QUANTITY:
			key = "stock.received.vansale.stock.total.quantity.of.shop";
			break;
		case ConstantManager.ERR_DP_PERIOD_STOCK_LOT:
			key = "stock.received.vansale.stock.total.quantity.lot.of.shop";
			break;
		case ConstantManager.ERR_ELEMENT_PAYROLLT_IS_NUMBER:
			key = "payroll.payroll.element.is.number";
			break;
		case ConstantManager.ERR_ELEMENT_PAYROLLT_IS_CHARACTER:
			key = "payroll.payroll.element.is.character";
			break;
		case ConstantManager.ERR_INCENTIVE_PROGRAM_NOT_WAITING:
			key = "incentive.program.not.waiting";
			break;
		case ConstantManager.ERR_DEVICE_STATUS_NOT_GOOD:
			key = "device.status.not.good";
			break;
		case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PAYROLL_ELEMENT_INFO:
			key = "payroll.value.in";
			break;
		case ConstantManager.ERR_STAFF_NOT_IN_PAYROLL:
			key = "payroll.staff.not.in.payroll";
			break;
		case ConstantManager.ERR_ISS_STOCK_OUT:
			key = "stock.issued.product.lot.lot.ex";
			break;
		case ConstantManager.ERR_CONTRACT_STARTDATE_NOT_GREATE_ENDDATE:
			key = "contract.startdate.not.great.enddate";
			break;
		case ConstantManager.ERR_CONTRACT_CONTRACTDATE_NOT_GREATE_HOCDATE:
			key = "contract.contractdate.not.great.hocdate";
			break;
		case ConstantManager.ERR_CONTRACT_NOT_DRAFT:
			key = "contract.not.draft";
			break;
		case ConstantManager.ERR_REVENUE_BONUS:
			key = "catalog.display.program.revenue.bonus.err";
			break;
		case ConstantManager.ERR_DISPLAY_BONUS:
			key = "catalog.display.program.display.bonus.err";
			break;
		case ConstantManager.ERR_REVENUE_DISPLAY_BONUS:
			key = "catalog.display.program.revenue.display.bonus.err";
			break;
		case ConstantManager.ERR_DISPLAY_PROGRAM_INTEGER:
			key = "catalog.display.program.revenue.display.bonus.err";
			break;
		case ConstantManager.ERR_INCENTIVE_PROGRAM_ONLY_NUMBER:
			key = "incentive.program.only.number.is.allow";
			break;
		case ConstantManager.ERR_PAYROLL_STOPPED:
			key = "payroll.payroll.stopped";
			break;
		case ConstantManager.ERR_PAYROLLT_STOPPED:
			key = "payroll.payrollt.stopped";
			break;
		case ConstantManager.ERR_PROMOTION_NOT_EXISTS:
			key = "promotion.not.exists";
			break;
		case ConstantManager.ERR_GROUP_PO_AUTO_NOT_EXISTS:
			key = "catalog.group.po.auto.not.exists";
			break;
		case ConstantManager.ERR_PROMOTION_PROGRAM_EXISTS:
			key = "catalog.promotion.program.exists";
			break;
		case ConstantManager.ERR_GROUP_PO_AUTO_EXISTS:
			key = "catalog.group.po.auto.exitst";
			break;
		case ConstantManager.ERR_FOCUS_PROGRAM_EXISTS:
			key = "catalog.focus.program.exists";
			break;
		case ConstantManager.ERR_COPY_INCENTIVE_PROGRAM_EXISTS:
			key = "catalog.incentive.program.exists";
			break;
		case ConstantManager.ERR_PROMOTION_MANUAL_PROGRAM_EXISTS:
			key = "catalog.promotion.manual.program.exists";
			break;
		case ConstantManager.ERR_CYCLE_COUNT_NOT_EXISTS:
			key = "cycle.count.not.exists";
			break;
		case ConstantManager.ERR_PROMOTION_CATALOG_IMPORT_DUPLICATE_TOTAL_MONEY_ZV192021: // tuannd20
			key = "salemt.promotion.catalog.import.duplicate.total.money.zv1920";
			break;
		case ConstantManager.ERR_PROMOTION_CATALOG_IMPORT_DUPLICATE_PROMOTION_PRODUCT_ZV0405: // tuannd20
			key = "salemt.promotion.catalog.import.duplicate.promotion.product.zv0405";
			break;
		case ConstantManager.ERR_PROMOTION_CATALOG_IMPORT_DUPLICATE_PROMOTION_PRODUCT_ZV06: // tuannd20
			key = "salemt.promotion.catalog.import.duplicate.promotion.product.zv06";
			break;
		case ConstantManager.ERR_LOT_QUANTITY_IS_NOT_ENOUGH: //tungtt
			key = "sp.search.sale.order.approved.quantity.not.enough";
			break;
		case ConstantManager.ERR_RECEIVED_QUANTITY_LARGER_THAN_MAX_QUANTITY: //tungtt
			key = "sp.adjust.order.received.larger.than.max.quantity";
			break;
		case ConstantManager.ERR_TOTAL_DEBIT_LARGER_THAN_MAX_DEBIT: //tungtt
			key = "sp.search.sale.order.total.larger.max.debit";
			break;
		case ConstantManager.ERR_MAX_DAY_DEBIT: //tungtt
			key = "sp.search.sale.order.max.debit.day";
			break;
		case ConstantManager.ERR_PO_POAUTO_NOT_APPROVED:
			key = "po.manage.poauto.status.not.approved.error";
			break;
		case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NUMBER_CONTRACT:
			// nhutnn
			key = "equipment.invalid.format.number.contract";
			break;
		case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SERIAL:
			// nhutnn
			key = "equipment.manager.equipment.seri.format";
			break;
		case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER:
			// nhutnn
			key = "equipment.manager.equipment.phone.number.format";
			break;
		case ConstantManager.ERR_CHANGE_PASS_NOT_EXIST_UPPER_LOWER:
			// vuongmq
			key = "common.password.new.password.err";
			break;
		default:
			break;
		}
		if (StringUtil.isNullOrEmpty(key)) {
			return key;
		}

		// get message content
		ResourceBundle rs = Configuration.getResourceBundle(ConstantManager.VI_LANGUAGE);
		String text = rs.getString(key);
		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				if (params[i] != null) {
					text = text.replace("{" + i + "}", params[i].toString());
				}
			}
		}
		return text;
	}

	/**
	 * Gets the error msg quickly.
	 * 
	 * 
	 * @param errorType the error type
	 * @param hasSuffix the has suffix
	 * @param params the params
	 * @return the error msg quickly
	 * 
	 * @modify hunglm16
	 * @since 16/15/2015
	 * @description Chi danh cho cac phuong thu lay message co params la cac key trong file .property 
	 */
	public static String getErrorMsgQuickly(int errorType, Boolean hasSuffix, Object... params) {
		String errMsg = "";
		List<String> lstParam = new ArrayList<String>();
		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				if (params[i] != null) {
					lstParam.add(R.getResource(params[i].toString()));
				}
			}
		}
		switch (lstParam.size()) {
		case 1:
			errMsg = ValidateUtil.getErrorMsg(errorType, lstParam.get(0));
			break;
		case 2:
			errMsg = ValidateUtil.getErrorMsg(errorType, lstParam.get(0), lstParam.get(1));
			break;
		case 3:
			errMsg = ValidateUtil.getErrorMsg(errorType, lstParam.get(0), lstParam.get(1), lstParam.get(2));
			break;
		case 4:
			errMsg = ValidateUtil.getErrorMsg(errorType, lstParam.get(0), lstParam.get(1), lstParam.get(2), lstParam.get(3));
			break;
		case 5:
			errMsg = ValidateUtil.getErrorMsg(errorType, lstParam.get(0), lstParam.get(1), lstParam.get(2), lstParam.get(3), lstParam.get(4));
			break;
		default:
			errMsg = ValidateUtil.getErrorMsg(errorType);
			break;
		}
		if (hasSuffix != null && hasSuffix) {
			errMsg += "\n";
		}
		return errMsg;
	}

	/**
	 * Validate format code.
	 * 
	 * @param value
	 *            the value
	 * @return true, if successful
	 * @author hungtx
	 * @since Sep 19, 2012
	 */
	public static boolean validateFormatCode(String value) {
		Matcher m = PATTERN_VALID_CODE.matcher(value);
		boolean matchFound = m.matches();
		return matchFound;
	}

	/**
	 * Validate format code.
	 * 
	 * @param value
	 *            the value
	 * @return true, if successful
	 * @author hunglm16
	 * @since January 06,2014
	 */
	public static boolean validateFormatSerial(String value) {
		Matcher m = PATTERN_SERIAL.matcher(value);
		boolean matchFound = m.matches();
		return matchFound;
	}

	/**
	 * Validate format code.
	 * 
	 * @param value
	 *            the value
	 * @return true, if successful
	 * @author hunglm16
	 * @since March 18, 2014
	 */
	public static boolean validateFormatCodeNew(String value) {
		Matcher m = PATTERN_VALID_CODE_NEW.matcher(value);
		boolean matchFound = m.matches();
		return matchFound;
	}

	/**
	 * Validate format code ato z.
	 * 
	 * @param value
	 *            the value
	 * @return true, if successful
	 * @author hungtx
	 * @since Oct 11, 2012
	 */
	public static boolean validateFormatCodeAtoZ(String value) {
		Matcher m = PATTERN_VALID_CODE_A_Z.matcher(value);
		boolean matchFound = m.matches();
		return matchFound;
	}

	/**
	 * Validate format name.
	 * 
	 * @param value
	 *            the value
	 * @return true, if successful
	 * @author hungtx
	 * @since Sep 19, 2012
	 */
	public static boolean validateFormatName(String value) {
		return PATTERN_INVALID_NAME.matcher(value).find();
	}

	/**
	 * Validate format numbercontract.
	 * 
	 * @param value
	 *            the value
	 * @return true, if successful
	 * @author nhutnn
	 * @since Jan 06, 2014
	 */
	public static boolean validateFormatNumberContract(String value) {
		return PATTERN_INVALID_NUMBER_CONTRACT.matcher(value).find();
	}

	/**
	 * Validate format numbercontract.
	 * 
	 * @param value
	 *            the value
	 * @return true, if successful
	 * @author nhutnn
	 * @since Jan 09, 2014
	 */
	public static boolean validateFormatInSerial(String value) {
		return PATTERN_SERIAL.matcher(value).find();
	}

	/**
	 * Validate format phone number.
	 * 
	 * @param value
	 *            the value
	 * @return true, if successful
	 * @author nhutnn
	 * @since Jan 09, 2014
	 */
	public static boolean validateFormatPhoneNumber(String value) {
		return PATTERN_VALID_PHONE_NUMBER.matcher(value).find();
	}
	/**
	 * Validate format address.
	 * 
	 * @param value
	 *            the value
	 * @return true, if successful
	 * @author hungtx
	 * @since Sep 19, 2012
	 */
	public static boolean validateFormatAddress(String value) {
		return PATTERN_INVALID_ADDRESS.matcher(value).find();
	}
	
	/**
	 * validate format special character
	 * @author trietptm
	 * @param value
	 * @return
	 * @since Oct 12, 2015
	 */
	public static boolean validateFormatSpecial(String value) {
		return PATTERN_INVALID_SPECIAL.matcher(value).find();
	}

	/**
	 * Validate email address.
	 * 
	 * @param value
	 *            the value
	 * @return true, if successful
	 * @author hungtx
	 * @since Sep 20, 2012
	 */
	public static boolean validateEmailAddress(String value) {
		Matcher m = PATTERN_VALID_EMAIL.matcher(value);
		boolean matchFound = m.matches();
		return matchFound;
	}

	public static boolean validateFormatLot(String value) {
		String yy = String.valueOf(DateUtil.getYear(DateUtil.now()) / 100);
		String dd = value.substring(0, 2);
		String mm = value.substring(2, 4);
		String format = dd + "/" + mm + "/" + yy + value.substring(4);
		String checkMessage = getErrorMsgForInvalidFormatDate(format, "Lô", false);
		if (!StringUtil.isNullOrEmpty(checkMessage)) {
			return false;
		}
		return true;
	}

	public static boolean validateFormatNumberConvfact(String value) {
		String pattern1 = "[0-9]*/[0-9]*";
		String pattern2 = "[0-9]*";
		Pattern p = Pattern.compile(pattern1 + "|" + pattern2);
		Matcher m = p.matcher(value);
		boolean matchFound = m.matches();
		return matchFound;
	}

	public static boolean validateCarNumber(String value) {
		Matcher m = PATTERN_VALID_CAR_NUMBER.matcher(value);
		boolean matchFound = m.matches();
		return matchFound;
	}

	public static boolean validateIdNumber(String value) {
		Matcher m = PATTERN_VALID_ID_NUMBER.matcher(value);
		boolean matchFound = m.matches();
		return matchFound;
	}

	/**
	 * Validate format code.
	 * 
	 * @param value
	 *            the value
	 * @return true, if successful
	 * @author huynp4
	 */
	public static boolean validateFormatPayrollElementInfo(String value) {
		Matcher m = PATTERN_VALID_PAYROLL_ELEMENT_INFO.matcher(value);
		boolean matchFound = m.matches();
		return matchFound;
	}
	
	/**
	 * Validate character upper
	 * @author vuongmq
	 * @param value
	 * @return boolean
	 * @since 30/10/2015
	 */
	public static boolean validateFormatCharUpper(String value) {
		Matcher m = PATTERN_VALID_CHAR_UPPER.matcher(value);
		boolean matchFound = m.matches();
		return matchFound;
	}
	
	/**
	 * Validate character lower
	 * @author vuongmq
	 * @param value
	 * @return boolean
	 * @since 30/10/2015
	 */
	public static boolean validateFormatCharLower(String value) {
		Matcher m = PATTERN_VALID_CHAR_LOWER.matcher(value);
		boolean matchFound = m.matches();
		return matchFound;
	}

	/**
	 * Gets the error msg of special char in code.
	 * 
	 * @param value
	 *            the value
	 * @param name
	 *            the name
	 * @return the error msg of special char in code
	 */
	public static String getErrorMsgOfSpecialCharInCode(String value, String name) {
		if (!ValidateUtil.validateFormatCode(value)) {
			return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE, true, name);
		}
		return "";
	}

	/**
	 * Gets the error msg of special char in code.
	 * 
	 * @param value
	 *            the value
	 * @param name
	 *            the name
	 * @return the error msg of special char in code
	 */
	public static String getErrorMsgOfSpecialCharInCodeNew(String value, String name) {
		if (!ValidateUtil.validateFormatCodeNew(value)) {
			return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE_NEW, true, name);
		}
		return "";
	}

	/**
	 * Gets the error msg of special char in code a to z.
	 * 
	 * @param value
	 *            the value
	 * @param name
	 *            the name
	 * @return the error msg of special char in code a to z
	 * @author hungtx
	 * @since Oct 11, 2012
	 */
	public static String getErrorMsgOfSpecialCharInCodeAToZ(String value, String name) {
		if (!ValidateUtil.validateFormatCodeAtoZ(value)) {
			return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "common.invalid.format.code.a.to.z", name);
		}
		return "";
	}

	/**
	 * Gets the error msg of special char in name.
	 * 
	 * @param value
	 *            the value
	 * @param name
	 *            the name
	 * @return the error msg of special char in name
	 */
	public static String getErrorMsgOfSpecialCharInName(String value, String name) {
		if (ValidateUtil.validateFormatName(value)) {
			return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME, true, name);
		}
		return "";
	}

	/**
	 * Gets the error msg of special char in address.
	 * 
	 * @param value
	 *            the value
	 * @param name
	 *            the name
	 * @return the error msg of special char in address
	 */
	public static String getErrorMsgOfSpecialCharInAddress(String value, String name) {
		if (ValidateUtil.validateFormatAddress(value)) {
			return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS, true, name);
		}
		return "";
	}
	
	/**
	 * kiem tra ky tu dac biet
	 * @author trietptm
	 * @param value
	 * @param name
	 * @return
	 * 
	 */
	public static String getErrorMsgOfSpecialCharInSpecial(String value, String name) {
		if (ValidateUtil.validateFormatSpecial(value)) {
			return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL, true, name);
		}
		return "";
	}

	/**
	 * Gets the error msg of special char in code.
	 * 
	 * @param value
	 *            the value
	 * @param name
	 *            the name
	 * @return the error msg of special char in code
	 */
	public static String getErrorMsgOfSpecialCharInPayrollElementInfo(String value, String name) {
		if (!ValidateUtil.validateFormatCode(value)) {
			return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PAYROLL_ELEMENT_INFO, true, name);
		}
		return "";
	}

	/**
	 * Gets the error msg of special char in number contract.
	 * 
	 * @param value
	 *            the value
	 * @param name
	 *            the name
	 * @return the error msg of special char in number contract
	 */
	public static String getErrorMsgOfSpecialCharInNumberContract(String value, String name) {
		if (ValidateUtil.validateFormatNumberContract(value)) {
			return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NUMBER_CONTRACT, true, name);
		}
		return "";
	}

	/**
	 * Gets the error msg of special char in serial.
	 * 
	 * @param value
	 *            the value
	 * @param name
	 *            the name
	 * @return the error msg of special char in serial
	 */
	public static String getErrorMsgOfSpecialCharInSerial(String value, String name) {
		if (!ValidateUtil.validateFormatInSerial(value)) {
			return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SERIAL, true, name);
		}
		return "";
	}
	/**
	 * Gets the error msg of special char in number contract.
	 * 
	 * @param value
	 *            the value
	 * @param name
	 *            the name
	 * @return the error msg of special char in number contract
	 */
	public static String getErrorMsgOfSpecialCharInPhoneNumber(String value, String name) {
		if (!ValidateUtil.validateFormatPhoneNumber(value)) {
			return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER, true, name);
		}
		return "";
	}
	
	/**
	 * Check change password
	 * @author vuongmq
	 * @param value
	 * @param name
	 * @return String
	 * @return maxlength
	 * @since 30/10/2015
	 */
	public static String getErrorMsgOfChangePass(String value, String name, Integer maxlength) {
		if ((maxlength != null && value.trim().length() < maxlength)
				|| ValidateUtil.validateFormatCharUpper(value)
				|| ValidateUtil.validateFormatCharLower(value)) {
			return ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_CHANGE_PASS_NOT_EXIST_UPPER_LOWER, false, name);
		}
		return "";
	}
	/**
	 * Validate field.
	 * 
	 * @param value the value
	 * @param name the name
	 * @param maxlength the maxlength
	 * @param params the params
	 * @return the string
	 * @author hungtx
	 * @since Sep 21, 2012
	 */
	public static String validateField(String value, String name, Integer maxlength, Object... params) {
		String errMsg = "";
		if (params != null) {
			for (int i = 0; i < params.length; i++) {
				if (params[i] != null) {
					int type = Integer.valueOf(params[i].toString()).intValue();
					switch (type) {
					case ConstantManager.ERR_REQUIRE:
						if (StringUtil.isNullOrEmpty(value)) {
							errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_REQUIRE, true, name);
						}
						break;
					case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE:
						if (!StringUtil.isNullOrEmpty(value)) {
							errMsg += ValidateUtil.getErrorMsgOfSpecialCharInCode(value, name);
						}
						break;
					case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_CODE_NEW:
						if (!StringUtil.isNullOrEmpty(value)) {
							errMsg += ValidateUtil.getErrorMsgOfSpecialCharInCodeNew(value, name);
						}
						break;
					case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NAME:
						if (!StringUtil.isNullOrEmpty(value)) {
							errMsg += ValidateUtil.getErrorMsgOfSpecialCharInName(value, name);
						}
						break;
					case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_ADDRESS:
						if (!StringUtil.isNullOrEmpty(value)) {
							errMsg += ValidateUtil.getErrorMsgOfSpecialCharInAddress(value, name);
						}
						break;
					case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SPECIAL:
						if (!StringUtil.isNullOrEmpty(value)) {
							errMsg += ValidateUtil.getErrorMsgOfSpecialCharInSpecial(value, name);
						}
						break;
					case ConstantManager.ERR_MAX_LENGTH:
						if (!StringUtil.isNullOrEmpty(value) && maxlength != null && value.trim().length() > maxlength) {
							errMsg += ValidateUtil.getErrorMsg(ConstantManager.ERR_MAX_LENGTH, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, name), maxlength) + "\n";
						}
						break;
					case ConstantManager.ERR_MAX_LENGTH_EQUALS:
						if (!StringUtil.isNullOrEmpty(value) && maxlength != null && value.trim().length() > maxlength) {
							errMsg += ValidateUtil.getErrorMsg(ConstantManager.ERR_MAX_LENGTH_EQUALS, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, name), maxlength) +"\n";
						}
						break;
					case ConstantManager.ERR_INTEGER:
						if (!StringUtil.isNullOrEmpty(value) && !ValidateUtil.validateNumber(value)) {
							errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_POSSITIVE_INTEGER, true, name);
						}
						break;
					case ConstantManager.ERR_FLOAT_NUMBER:
						if (!StringUtil.isNullOrEmpty(value) && !ValidateUtil.validateFloatNumber(value)) {
							errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_FLOAT_NUMBER, true, name);
						}
						break;
					case ConstantManager.ERR_NUMBER_DOT:
						if(!StringUtil.isNullOrEmpty(value) && !ValidateUtil.validateNumberDot(value)){
							errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NUMBER_DOT, true,name);
						}
						break;
					case ConstantManager.ERR_NOT_REQUIRE:
						if (!StringUtil.isNullOrEmpty(value)) {
							errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_NOT_REQUIRE, true, name);
						}
						break;
					case ConstantManager.ERR_INVALID_EMAIL:
						if (!ValidateUtil.validateEmailAddress(value)) {
							errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_INVALID_EMAIL, true, name);
						}
						break;
					case ConstantManager.ERR_INVALID_LENGTH:
						if (!StringUtil.isNullOrEmpty(value) && maxlength != null && value.trim().length() != maxlength) {
							errMsg += ValidateUtil.getErrorMsg(ConstantManager.ERR_INVALID_LENGTH, Configuration.getResourceString(ConstantManager.VI_LANGUAGE, name), maxlength) + "\n";
						}
						break;
					case ConstantManager.ERR_FORMAT_LOT:
						if (!ValidateUtil.validateFormatLot(value)) {
							errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_FORMAT_LOT, true, name);
						}
						break;
					case ConstantManager.ERR_FORMAT_NUMBER_CONVFACT:
						if (!StringUtil.isNullOrEmpty(value) && !ValidateUtil.validateFormatNumberConvfact(value)) {
							errMsg += ValidateUtil.getErrorMsgQuickly(ConstantManager.ERR_FORMAT_NUMBER_CONVFACT, true, name);
						}
						break;
					case ConstantManager.ERR_INVALID_DATE:
						if (!StringUtil.isNullOrEmpty(value)) {
							errMsg += ValidateUtil.getErrorMsgForInvalidFormatDate(value, name, false);
						}
						break;
					case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_NUMBER_CONTRACT:
						// nhutnn
						if (!StringUtil.isNullOrEmpty(value)) {
							errMsg += ValidateUtil.getErrorMsgOfSpecialCharInNumberContract(value, name);
						}
						break;
					case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_SERIAL:
						// nhutnn
						if (!StringUtil.isNullOrEmpty(value)) {
							errMsg += ValidateUtil.getErrorMsgOfSpecialCharInSerial(value, name);
						}
						break;
					case ConstantManager.ERR_EXIST_SPECIAL_CHAR_IN_PHONE_NUMBER:
						// nhutnn
						if (!StringUtil.isNullOrEmpty(value)) {
							errMsg += ValidateUtil.getErrorMsgOfSpecialCharInPhoneNumber(value, name);
						}
						break;
					case ConstantManager.ERR_CHANGE_PASS_NOT_EXIST_UPPER_LOWER:
						// vuongmq
						if (!StringUtil.isNullOrEmpty(value)) {
							errMsg += ValidateUtil.getErrorMsgOfChangePass(value, name, maxlength);
						}
						break;
					default:
						break;
					}
				}
			}
		}
		return errMsg;
	}
	/**
	 * Gets error msg for validate to time
	 * @author haupv3
	 * @since 17/10/2014
	 * **/
	public static String getErrorMsgForInvalidFormatTime(String timeStr, String objName){
		boolean error = false;
		if(!StringUtil.isNullOrEmpty(timeStr)){
			String[] arrTime = timeStr.split(":");
			int hour = 0;
			int minute = 0;
			int second = 0;
			
			try{
				hour = "0".equals(arrTime[0]) || "00".equals(arrTime[0]) ? 0 : Integer.valueOf(arrTime[0]);
				minute = "0".equals(arrTime[1]) || "00".equals(arrTime[1]) ? 0 :Integer.valueOf(arrTime[1]);
				second = "0".equals(arrTime[2]) || "00".equals(arrTime[2])  ? 0 : Integer.valueOf(arrTime[2]);
			}catch(Exception e){
				error = true;
			}
			
			if(hour > 24 || hour <0){
				error = true;
			}
			
			if(second > 60 || second <0){
				error = true;
			}
			
			if(minute >60 || minute <0){
				error = true;
			}
			
			if(error){
				return Configuration.getResourceString(ConstantManager.VI_LANGUAGE,"common.invalid.format.time",objName) + "\n";
			}
		}
		return "";
	}
	
	/**
     * Validate video file.
     *
     * @param videoFile the video file
     * @param videoFileContentType the video file content type
     * @return the string
     */
	public static String validateVideoFile(File videoFile, String videoFileContentType) {
		String message = "";
		//	if(videoFile != null && (videoFile.length() > Configuration.getVideoUploadMaxSizeByte())){
		//	    return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, "video.invalid.size");
		//	}
		if (videoFile == null || Configuration.getVideoUploadFileSupport().indexOf(videoFileContentType) == -1 || (FileUtility.getMime(videoFile) != null && Configuration.getVideoUploadFileSupport().indexOf(FileUtility.getMime(videoFile)) == -1)) {
			return R.getResource("video.invalid.type");
		}
		return message;
	}
    
    /**
	 * kiem tra ngay truyen vao co phai la ngay hop le
	 * @author tuannd20
	 * @param date chuoi gia tri ngay
	 * @return true: chuoi truyen vao la 1 ngay hop le. Nguoc lai, false
	 * @since 06/09/2015
	 */
	public static Boolean isValidDate(String date, String dateFormat) {
		if (StringUtil.isNullOrEmpty(date) || StringUtil.isNullOrEmpty(dateFormat)) {
			throw new IllegalArgumentException("invalid arguments");
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			sdf.setLenient(false);
			sdf.parse(date);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * kiem tra file co phan mo rong & mime hop le hay ko
	 * @author tuannd20
	 * @param file file can kiem tra
	 * @param validFileTypes cac file type & phan mo rong hop le trong cau hinh
	 * @return true: neu nhu file hop le. Nguoc lai, false
	 * @since 16/09/2015
	 */
	public static Boolean isValidFileType(File file, String validFileTypes) {
		if (validFileTypes == null || validFileTypes.isEmpty()) {
			return true;
		}
		boolean isValidFileType = false;
		if (file != null) {
			/*String fileExtension = FileUtility.getFileExtension(file);
			if (fileExtension != null && validFileTypes.contains(fileExtension)) {
				isValidFileType = true;
			}*/
			
			/*String fileMime = FileUtility.getMime(file);
			if (fileMime != null && validFileTypes.indexOf(fileMime) != -1) {
				isValidFileType = true;
			}*/
		}
		//return isValidFileType;
		return true;
	}
}

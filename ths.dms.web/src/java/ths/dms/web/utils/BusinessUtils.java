package ths.dms.web.utils;

import ths.dms.core.entities.Shop;
import ths.dms.core.entities.Staff;
import ths.dms.core.entities.enumtype.ActiveType;
import ths.dms.core.entities.enumtype.ChannelTypeType;
import ths.dms.core.entities.enumtype.ShopObjectType;
//import ths.dms.core.entities.enumtype.StaffGroupType;
import ths.dms.core.entities.enumtype.StaffObjectType;

import ths.dms.helper.Configuration;
import ths.dms.web.constant.ConstantManager;
/**
 * 
 * @author TIENTV11
 * @description Xu ly nghiep vu BSG cho check type
 * ho tro xuat bao cao
 * @since 11/02/2014
 *
 */
public class BusinessUtils {
	
	/** 
	 * @des Kiem tra loai don vi duoc chon
	 * @param sh
	 * @param shopType
	 * @param objectType
	 * @return
	 */
	public static Boolean checkShopType(Shop sh,ChannelTypeType shopType,ShopObjectType objectType){
		Boolean result = Boolean.FALSE;
		/*if(sh!=null && shopType.equals(sh.getType().getType())				 
				 && objectType.equals(ShopObjectType.parseValue(sh.getType().getObjectType()))){
			result = Boolean.TRUE;			
		}*/
		return result;
	}
	
	
	/** 
	 * @des Kiem tra loai don vi duoc chon
	 * @param sh	 
	 * @param objectType...
	 * @return
	 */
	public static Boolean checkShopType(Shop sh,ShopObjectType... objectTypes){
		Boolean result = Boolean.FALSE;
		/*for(ShopObjectType objectType : objectTypes) {
			if(sh!=null && ChannelTypeType.SHOP.equals(sh.getType().getType())				 
					 && objectType.equals(ShopObjectType.parseValue(sh.getType().getObjectType()))){
				result = Boolean.TRUE;		
				break;
			}
		}	*/	
		return result;
	}
	
	/** 
	 * @des Kiem tra loai nhan vien duoc chon
	 * @param sh
	 * @param staffType
	 * @param objectType
	 * @return
	 */
	public static Boolean checkStaffType(Staff staff,ChannelTypeType staffType,StaffObjectType... objectTypes){
		Boolean result = Boolean.FALSE;
		/*for(StaffObjectType objectType : objectTypes) {
			if(staff!=null && staffType.equals(staff.getStaffType().getType())				 
					 && objectType.equals(StaffObjectType.parseValue(staff.getStaffType().getObjectType()))){
				result = Boolean.TRUE;		
				break;
			}
		}*/		
		return result;
	}
	
	/** 
	 * @des Kiem tra loai nhan vien duoc chon
	 * @param sh
	 * @param staffType
	 * @param objectType
	 * @return
	 */
	public static Boolean checkStaffType(Integer staffType,StaffObjectType... objectTypes){
		Boolean result = Boolean.FALSE;
		for(StaffObjectType objectType : objectTypes) {
			if(staffType!=null && objectType.equals(StaffObjectType.parseValue(staffType))){
				result = Boolean.TRUE;		
				break;
			}
		}		
		return result;
	}
	
	/** 
	 * @des Kiem tra loai nhan vien duoc chon
	 * @param sh
	 * @param objectType...
	 * @return
	 */
	public static Boolean checkStaffType(Staff staff,StaffObjectType... objectTypes){
		Boolean result = Boolean.FALSE;
		/*for(StaffObjectType objectType : objectTypes) {
			if(staff!=null && ChannelTypeType.STAFF.equals(staff.getStaffType().getType())				 
					 && objectType.equals(StaffObjectType.parseValue(staff.getStaffType().getObjectType()))){
				result = Boolean.TRUE;		
				break;
			}
		}	*/	
		return result;
	}
	
	/** 
	 * @des Kiem tra loai nhan vien duoc chon
	 * @param sh
	 * @param objectType...
	 * @return
	 */
	/*public static Boolean checkGroupType(StaffGroup sg,StaffGroupType... objectTypes){
		Boolean result = Boolean.FALSE;
		for(StaffGroupType objectType : objectTypes) {
			if(sg!=null && objectType.equals(StaffGroupType.parseValue(sg.getGroupType()))){
				result = Boolean.TRUE;		
				break;
			}
		}		
		return result;
	}*/
	
	/**
	 * @author vuongmq
	 * Kiem tra loai status
	 * @param ActiveType
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public static Boolean checkActiveType(Integer status, ActiveType... params) throws Exception{
		if (params != null) {
			for (int i = 0, size = params.length; i < size; i++) {
				if (params[i] != null && params[i].getValue().equals(status)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Chuyen gia tri trang sang chuoi
	 * @author tuannd20
	 * @param statusValue Gia tri trang thai
	 * @return Y nghia cua trang thai
	 * @since 28/03/2015
	 */
	public static String decodeStatus(Integer statusValue) {
		String decodeValueKey = "common.status.invalid";
		if (statusValue != null && ActiveType.isValidValue(statusValue)) {
			ActiveType status = ActiveType.parseValue(statusValue);
			switch (status) {
			case RUNNING:
				decodeValueKey = "common.status.active";
				break;
			case STOPPED:
				decodeValueKey = "common.status.stopped";
				break;
			default:
				decodeValueKey = "common.status.stopped";
			}
		}
		return Configuration.getResourceString(ConstantManager.VI_LANGUAGE, decodeValueKey);
	}
}

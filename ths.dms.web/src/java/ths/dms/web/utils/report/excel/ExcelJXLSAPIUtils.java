/**
 * 
 */
package ths.dms.web.utils.report.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.NumberFormat;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 * The Class Excel JXLS API UTILS.
 *
 * @author hunglm16
 * @since January 3, 2014
 * 
 * update for VINAMILK
 * @since January 15, 2014
 */
public class ExcelJXLSAPIUtils {
	//Dung cho VINAMILK
	public final static String HEADER_GRAY_ALL_THIN = "header_gray_all_thin";//column static
	public final static String HEADER_GRAY01_ALL_THIN = "header_gray01_all_thin";//column static
	public final static String HEADER_GRAY01_ALL_THIN_LEFT = "header_gray01_all_thin_left";//column static
	public final static String HEADER_GRAY01_ALL_THIN_RIGHT = "header_gray01_all_thin_right";//column static
	public final static String HEADER_GRAY02_ALL_THIN = "header_gray02_all_thin";//column static
	public final static String HEADER_GRAY02_ALL_THIN_LEFT = "header_gray02_all_thin_left";//column static
	public final static String HEADER_GRAY02_ALL_THIN_RIGHT = "header_gray02_all_thin_right";//column static
	public final static String HEADER_INDIGO_ALL_THIN = "header_indigo_all_thin";
	public final static String TITLE_VNM_BLACK = "title_vnm_black";//row title
	public final static String ROW_ALL_THIN_LEFT = "row_all_thin_left";//row
	public final static String ROW_ALL_THIN_RIGHT = "row_all_thin_right";//row
	public final static String ROW_ALL_THIN_CENTRE = "row_all_thin_centre";//row
	public final static String NO_BORDER_THIN_LEFT = "no_border_thin_left";//row
	public final static String NO_BORDER_THIN_RIGHT = "no_border_thin_right";//row
	public final static String NO_BORDER_THIN_CENTRE = "no_border_thin_centre";//row
	public final static String NO_BORDER_THIN_CENTRE_NUMBER = "no_border_thin_centre_number";//row
	public final static String ROW_ALL_THIN_RIGHT_NUMBER_FORMAT_DECIMAL = "row_all_thin_right_number_format_decimal";//row
	public final static String ROW_ALL_THIN_RIGHT_NUMBER_FORMAT_THOUSAND = "row_all_thin_right_number_format_thousand";//rơ
	public final static String ROW_NO_BORDER_RIGHT_NUMBER_FORMAT_THOUSAND = "row_no_border_right_number_format_thousand";
	//Giao dien template to mau sac so (SABECO)
	public final static String GROUP_CENTER_INFOMATION_NAME = "group_center_infomation_name";//name company connect by... shop id
	public final static String GROUP_CENTER_INFOMATION_NAME_FORMAT_NUMBER = "group_center_infomation_name_format_number";//name company connect by... shop id
	public final static String GROUP_CENTER_INFOMATION_NAME_PERCEN = "group_center_infomation_name_percen";
	public final static String GROUP_LEFT_INFOMATION_NAME = "group_left_infomation_name";//name company connect by... shop id
	public final static String GROUP_LEFT_INFOMATION_NAME_FORMAT_NUMBER = "group_left_infomation_name_format_number";//name company connect by... shop id
	public final static String GROUP_LEFT_INFOMATION_NAME_PERCEN = "group_left_infomation_name_percen";
	public final static String GROUP_RIGHT_INFOMATION_NAME = "group_right_infomation_name";//name company connect by... shop id
	public final static String GROUP_RIGHT_INFOMATION_NAME_FORMAT_NUMBER = "group_right_infomation_name_format_number";//name company connect by... shop id
	public final static String GROUP_RIGHT_INFOMATION_NAME_PERCEN = "group_right_infomation_name_percen";
	public final static String TITLE = "title";//row title
	public final static String TITLE_LEFT = "title_left";//row title
	public final static String TIMES_TITLE = "times_title";//row start date - to date
	public final static String TIMES_TITLE_LEFT = "times_title_left";//row start date - to date
	public final static String COUNT_HORIZONTAL = "count_horizontal";//STT dem theo hang ngang kieu chuoi
	
	public final static String HEADER_BLUE_TOP_BOTTOM_MEDIUM = "header_blue_top_bottom_medium";//column static
	public final static String HEADER_BLUE_TOP_RIGHT_MEDIUM = "header_blue_top_right_medium";//column static
	public final static String HEADER_BLUE_ALL_THIN = "header_blue_all_thin";//column static
	public final static String HEADER_BLUE_ALL_THIN_NOBOLD = "header_blue_all_thin_nobold";//column static
	public final static String HEADER_BLUE_ALL_THIN_NOBOLD_LEFT_MEDIUM = "header_blue_all_thin_nobold_left_medium";//column static
	public final static String HEADER_BLUE_ALL_THIN_NOBOLD_RIGHT_MEDIUM = "header_blue_all_thin_nobold_right_medium";//column static
	public final static String HEADER_BLUE_ALL_THIN_TOP_MEDIUM = "header_blue_all_thin_top_medium";//column static
	public final static String HEADER_BLUE_R_L_THIN_TOP_MEDIUM = "header_blue_r_l_thin_top_medium";//column static
	public final static String HEADER_BLUE_RIGHT_THIN_TOP_MEDIUM = "header_blue_right_thin_top_medium";//column static
	public final static String HEADER_BLUE_ALL_THIN_BOTTOM_MEDIUM = "header_blue_all_thin_bottom_medium";//column static
	public final static String HEADER_BLUE_R_L_THIN_BOTTOM_MEDIUM = "header_blue_r_l_thin_bottom_medium";//column static
	public final static String HEADER_BLUE_ALL_THIN_LEFT_MEDIUM = "header_blue_all_thin_left_medium";//column mirror
	public final static String HEADER_BLUE_ALL_MEDIUM_LEFT_THIN = "header_blue_all_medium_left_thin";//column mirror
	public final static String HEADER_BLUE_ALL_THIN_RIGTH_MEDIUM = "header_blue_all_thin_right_medium";//column mirror
	
	public final static String HEADER_BLUESEE_ALL_THIN_BOTTOM_MEDIUM = "header_bluesee_all_thin_bottom_medium";//column mirror
	public final static String HEADER_BLUESEE_ALL_THIN_TOP_MEDIUM = "header_bluesee_all_thin_top_medium";//column mirror
	public final static String HEADER_BLUESEE_ALL_THIN_BOTTOM_MEDIUM_WRAP = "header_bluesee_all_thin_bottom_medium_wrap";//column mirror wrap
	public final static String HEADER_BLUESEE_ALL_THIN_TOP_MEDIUM_WRAP = "header_bluesee_all_thin_top_medium_wrap";//column mirror wrap
	public final static String HEADER_GREEN_ALL_THIN_BOTTOM_MEDIUM = "header_green_all_thin_bottom_medium";//column mirror
	public final static String HEADER_GREEN_ALL_THIN_TOP_MEDIUM = "header_green_all_thin_top_medium";//column mirror
	
	public final static String HEADER_BLUE_ALL_MEDIUM_BOTTOM_THIN = "header_blue_all_medium_bottom_thin";//column mirror
	public final static String HEADER_BLUE_ALL_MEDIUM_BOTTOM_THIN_WRAP = "header_blue_all_medium_bottom_thin_wrap";//column mirror
	
	public final static String ROW_COUNT_HOZITAL = "row_count_horizontal";//So TT dem ngang
	public final static String ROW_COUNT_HOZITAL_BODER_LEFT = "row_count_horizontal_boder_left";//So TT dem ngang
	public final static String ROW_COUNT_HOZITAL_BODER_RIGHT = "row_count_horizontal_boder_right";//So TT dem ngang
	
	public final static String ROW_DOTTED_LEFT = "row_dotted_left";//detail with text
	public final static String ROW_DOTTED_RIGTH = "row_dotted_right";//detail with number

	
	public final static String ROW_DOTTED_RIGTH_ORANGE01_DOUBLE_FORMATER_ZERO = "ROW_DOTTED_RIGTH_ORANGE01_double_formater_zero";//format ORANGE01 double show Zero
	public final static String ROW_DOTTED_RIGTH_DOUBLE_FORMATER_ZERO = "ROW_DOTTED_RIGTH_double_formater_zero";//format double show Zero  
	public final static String ROW_THIN_RIGTH_DOUBLE_FORMATER_ZERO = "ROW_THIN_RIGTH_double_formater_zero";//format double show Zero
	public final static String ROW_DOTTED_RIGTH_PERCENT = "row_dotted_right_percent";//format percent Ex: n.0i%
	public final static String ROW_DOTTED_CENTRE = "row_dotted_centre";//detail with text
	public final static String ROW_DOTTED_CENTRE_NUMBER = "row_dotted_centre_number";//detail with text
	
	public final static String ROW_DOTTED_LEFT_BLUELIGHT1 = "row_dotted_left_bluelight1";//detail with text
	public final static String ROW_DOTTED_RIGHT_BLUELIGHT1 = "row_dotted_right_bluelight1";//detail with number
	
	//detail with orange color font to 6
	public final static String ROW_DOTTED_LEFT_ORANGE01 = "row_dotted_left_orange_01";//detail with text
	public final static String ROW_DOTTED_LEFT_ORANGE01_FONT_RED = "row_dotted_left_orange_01_font_red";//detail with text
	public final static String ROW_DOTTED_LEFT_ORANGE02 = "row_dotted_left_orange_02";//detail with text
	public final static String ROW_DOTTED_LEFT_ORANGE03 = "row_dotted_left_orange_03";//detail with text
	
	public final static String ROW_DOTTED_LEFT_ORANGE04 = "row_dotted_left_orange_04";//detail with text
	public final static String ROW_DOTTED_LEFT_ORANGE05 = "row_dotted_left_orange_05";//detail with text
	public final static String ROW_DOTTED_LEFT_ORANGE06 = "row_dotted_left_orange_06";//detail with text
	
	public final static String ROW_DOTTED_CENTRE_ORANGE01 = "row_dotted_centre_orange_01";//detail with number sum
	public final static String ROW_DOTTED_CENTRE_ORANGE02 = "row_dotted_centre_orange_02";//detail with number sum
	public final static String ROW_DOTTED_CENTRE_ORANGE03 = "row_dotted_centre_orange_03";//detail with number sum
	public final static String ROW_DOTTED_CENTRE_ORANGE04 = "row_dotted_centre_orange_04";//detail with number sum
	public final static String ROW_DOTTED_CENTRE_ORANGE05 = "row_dotted_centre_orange_05";//detail with number sum
	public final static String ROW_DOTTED_CENTRE_ORANGE06 = "row_dotted_centre_orange_06";//detail with number sum
	
	public final static String ROW_DOTTED_RIGTH_ORANGE01 = "row_dotted_rigth_orange_01";//detail with number sum
	public final static String ROW_DOTTED_RIGTH_ORANGE01_PERCENT = "row_dotted_rigth_orange_01_percent";//detail with number sum
	public final static String ROW_DOTTED_RIGTH_ORANGE02 = "row_dotted_rigth_orange_02";//detail with number sum
	public final static String ROW_DOTTED_RIGTH_ORANGE03 = "row_dotted_rigth_orange_03";//detail with number sum
	public final static String ROW_DOTTED_RIGTH_ORANGE04 = "row_dotted_rigth_orange_04";//detail with number sum
	public final static String ROW_DOTTED_RIGTH_ORANGE05 = "row_dotted_rigth_orange_05";//detail with number sum
	public final static String ROW_DOTTED_RIGTH_ORANGE06 = "row_dotted_rigth_orange_06";//detail with number sum
	
	public final static String ROW_DOTTED_PERCENT_ORANGE01 = "row_dotted_percent_orange_01";//detail with number sum
	public final static String ROW_DOTTED_PERCENT_ORANGE02 = "row_dotted_percent_orange_02";//detail with number sum
	public final static String ROW_DOTTED_PERCENT_ORANGE03 = "row_dotted_percent_orange_03";//detail with number sum
	public final static String ROW_DOTTED_PERCENT_ORANGE04 = "row_dotted_percent_orange_04";//detail with number sum
	public final static String ROW_DOTTED_PERCENT_ORANGE05 = "row_dotted_percent_orange_05";//detail with number sum
	public final static String ROW_DOTTED_PERCENT_ORANGE06 = "row_dotted_percent_orange_06";//detail with number sum
	
	//phat trien cho cac bao cao xuat nhap ton
	public final static String ROW_DOTTED_LEFT_BORDER_LEFT = "row_dotted_left_border_left";//detail with text
	public final static String ROW_DOTTED_LEFT_BORDER_RIGHT = "row_dotted_left_border_right";//detail with text
	public final static String ROW_DOTTED_RIGTH_BORDER_LEFT = "row_dotted_rigth_border_left";//detail with text
	public final static String ROW_DOTTED_RIGTH_BORDER_RIGHT = "row_dotted_rigth_border_right";//detail with text
	
	public final static String ROW_DOTTED_LEFT_ORANGE01_BORDER_LEFT = "row_dotted_left_orange_01_border_left";//detail with text
	public final static String ROW_DOTTED_LEFT_ORANGE01_BORDER_RIGHT = "row_dotted_left_orange_01_border_right";//detail with text
	public final static String ROW_DOTTED_RIGTH_ORANGE01_BORDER_LEFT = "row_dotted_rigth_orange_01_border_left";//detail with text
	public final static String ROW_DOTTED_RIGTH_ORANGE01_BORDER_RIGHT = "row_dotted_rigth_orange_01_BORDER_RIGHT";//detail with text
	
	public final static String ROW_DOTTED_LEFT_ORANGE02_BORDER_LEFT = "row_dotted_left_orange_02_border_left";//detail with text
	public final static String ROW_DOTTED_LEFT_ORANGE02_BORDER_RIGHT = "row_dotted_left_orange_02_border_right";//detail with text
	public final static String ROW_DOTTED_RIGTH_ORANGE02_BORDER_RIGHT = "row_dotted_rigth_orange_02_border_right";//detail with text
	public final static String ROW_DOTTED_RIGTH_ORANGE02_BORDER_LEFT = "row_dotted_rigth_orange_02_border_left";//detail with text
	
	public final static String ROW_DOTTED_LEFT_ORANGE03_BORDER_TOP_BOTTOM = "row_dotted_left_orange_03_border_top_bottom";//detail with text
	public final static String ROW_DOTTED_RIGTH_ORANGE03_BORDER_TOP_BOTTOM = "row_dotted_rigth_orange_03_border_top_bottom";//detail with text
	public final static String ROW_DOTTED_LEFT_ORANGE03_BORDER_TOP_BOTTOM_LEFT = "row_dotted_left_orange_03_border_top_bottom_left";//detail with text
	public final static String ROW_DOTTED_LEFT_ORANGE03_BORDER_TOP_BOTTOM_RIGHT = "row_dotted_left_orange_03_border_top_bottom_right";//detail with text
	public final static String ROW_DOTTED_RIGTH_ORANGE03_BORDER_TOP_BOTTOM_LEFT = "row_dotted_rigth_orange_03_border_top_bottom_left";//detail with text
	public final static String ROW_DOTTED_RIGTH_ORANGE03_BORDER_TOP_BOTTOM_RIGHT = "row_dotted_rigth_orange_03_border_top_bottom_right";//detail with text
	
	//detail with red color font
	public final static String ROW_DOTTED_LEFT_RED = "row_dotted_left_red";//detail with text
	public final static String ROW_DOTTED_LEFT_FONT_RED = "row_dotted_left_font_red";//detail with text
	public final static String ROW_DOTTED_RIGTH_RED = "row_dotted_rigth_red";//detail with number sum
	public final static String ROW_DOTTED_PERCENT_RED = "row_dotted_percent_red";//detail with number sum
	//
	public final static String ROW_NOBODER_LEFT = "row_noboder_left";
	//
	public final static String ROW_NOBODER_BOLD_LEFT = "row_noboder_bold_left";//detail with text
	public final static String ROW_NOBODER_BOLD_RIGHT = "row_noboder_bold_right";
	
	//JXL color, index must be in 8-64
	public final static int JXL_BLACK = 8;//mau den
	public final static int JXL_WHITE = 9;//mau trang
	public final static int JXL_GRAY = 10;//mau xam
	public final static int JXL_RED = 11;//mau do
	public final static int JXL_BLUE = 12;//mau xanh da troi
	public final static int JXL_GREEN = 13;//mau xanh la cay
	public final static int JXL_BLUE_SKY = 14;//mau xanh da troi sang
	public final static int JXL_BLUE_SEE = 15;//mau xanh nuoc bien
	public final static int JXL_BLUE_LIGHT01 = 16;//mau xanh nhat
	
	public final static int JXL_ORANGE01 = 19;//mau cam 01
	public final static int JXL_ORANGE02 = 20;//mau cam 02
	public final static int JXL_ORANGE03 = 21;//mau cam 03
	public final static int JXL_ORANGE04 = 22;//mau cam 04
	public final static int JXL_ORANGE05 = 23;//mau cam 05
	public final static int JXL_ORANGE06 = 24;//mau cam 06
	
	public final static int JXL_GRAY01 = 26;//mau cham
	public final static int JXL_GRAY02 = 27;//mau cham
	
	public final static int JXL_INDIGO1 = 30;//mau cham
	
	/**
	 * Substitute.
	 *
	 * @param zipfile the zipfile
	 * @param tmpfile the tmpfile
	 * @param entry the entry
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hunglm16
	 * @since January 3, 2014
	 */
	public static void substitute(File zipfile, File tmpfile, String entry,
			OutputStream out) throws IOException {
		ZipFile zip = new ZipFile(zipfile);

		ZipOutputStream zos = new ZipOutputStream(out);

		@SuppressWarnings("unchecked")
		Enumeration<ZipEntry> en = (Enumeration<ZipEntry>) zip.entries();
		while (en.hasMoreElements()) {
			ZipEntry ze = en.nextElement();
			if (!ze.getName().equals(entry)) {
				zos.putNextEntry(new ZipEntry(ze.getName()));
				InputStream is = zip.getInputStream(ze);
				copyStream(is, zos);
				is.close();
			}
		}
		zos.putNextEntry(new ZipEntry(entry));
		InputStream is = new FileInputStream(tmpfile);
		copyStream(is, zos);
		is.close();

		zos.close();
		zip.close();
	}
	
	/**
	 * Copy stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hunglm16
	 * @since January 3,2014
	 */
	private static void copyStream(InputStream in, OutputStream out)
			throws IOException {
		byte[] chunk = new byte[1024];
		int count;
		while ((count = in.read(chunk)) >= 0) {
			out.write(chunk, 0, count);
		}
	}
	/**
	 * create styles writable word book
	 * @author hunglm16
	 * @since January 3,2014
	 * @description 
	 * */
	public static Map<String, WritableCellFormat> createStylesWritableWorkbook(WritableWorkbook wb) throws WriteException {
		/**Start: Dinh nghia ma mau (Colour)*/
		Map<String, WritableCellFormat> styles = new HashMap<String, WritableCellFormat>();
		wb.setColourRGB(Colour.getInternalColour(JXL_BLUE), 83, 141, 213);//mau xanh duong dam
		wb.setColourRGB(Colour.getInternalColour(JXL_BLUE_SKY), 0, 112, 192);//mau xanh duong dam (mau chu)
		wb.setColourRGB(Colour.getInternalColour(JXL_BLUE_SEE), 0, 176, 240);//mau xanh duong dam (cot dong)
		
		wb.setColourRGB(Colour.getInternalColour(JXL_WHITE), 255, 255, 255);//mau trang
		wb.setColourRGB(Colour.getInternalColour(JXL_BLACK), 0, 0, 0);//mau den
		wb.setColourRGB(Colour.getInternalColour(JXL_GREEN), 0, 176, 80);// mau xanh la cay
		
		wb.setColourRGB(Colour.getInternalColour(JXL_BLUE_LIGHT01), 221, 217, 196);// mau xanh da troi nhat
		//Mau cam tang dan theo cap do 01....0n (n>1)
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE01), 253, 233, 217);//mau cam nhat 01
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE02), 252, 213, 180);//mau cam nhat 02
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE03), 250, 191, 142);//mau cam nhat 03
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE04),  226, 107, 10);  //mau cam 04
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE05), 247, 150, 40); //mau cam dam 05
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE06), 151, 71, 6); //mau cam nau 06
		wb.setColourRGB(Colour.getInternalColour(JXL_GRAY), 192, 192, 192);//mau xam (GRAY Vinamilk)
		wb.setColourRGB(Colour.getInternalColour(JXL_GRAY01), 217, 217, 217);//mau xam pha cham (GRAY Vinamilk)
		wb.setColourRGB(Colour.getInternalColour(JXL_GRAY02), 242, 242, 242);//mau xam nhat (GRAY Vinamilk)
		wb.setColourRGB(Colour.getInternalColour(JXL_RED), 255, 0, 0);//mau do
		
		wb.setColourRGB(Colour.getInternalColour(JXL_INDIGO1), 148, 138, 84);//mau cham
		/**End: Colour*/
		
		/**Start: Dinh dang Font*/
		//Font mac dinh
		WritableFont generalFontBlack10 = new WritableFont(WritableFont.ARIAL, 9, WritableFont.NO_BOLD);//font BLACK
		generalFontBlack10.setColour(Colour.getInternalColour(JXL_BLACK));
	    //Font in dam mac dinh
	    WritableFont generalFontBoldBlack10 = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);//font BLACK
	    generalFontBoldBlack10.setColour(Colour.getInternalColour(JXL_BLACK));
	  //Font in dam mac dinh
	    WritableFont generalFontBoldBlack10Red = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);//font BLACK
	    generalFontBoldBlack10Red.setColour(Colour.getInternalColour(JXL_RED));
	    //Font title mac dinh
	    WritableFont tilteBoldBlue20 = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);//font Blue Sky
	    tilteBoldBlue20.setColour(Colour.getInternalColour(JXL_BLUE_SKY));
	    
	    WritableFont tilteBoldBlack = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);//font Blue Sky
	    tilteBoldBlack.setColour(Colour.getInternalColour(JXL_BLACK));
	    
	    //Font header mac dinh
	    WritableFont hedderBoldWhile = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);//font White
	    hedderBoldWhile.setColour(Colour.getInternalColour(JXL_WHITE));
	    
	    WritableFont hedderBoldWhileNoBold = new WritableFont(WritableFont.ARIAL, 9, WritableFont.NO_BOLD);//font White
	    hedderBoldWhileNoBold.setColour(Colour.getInternalColour(JXL_WHITE));
	    
	    //GROUP_LEFT_INFOMATION_NAME
		WritableCellFormat groupLeftInfomationName = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		groupLeftInfomationName.setBorder(Border.ALL, BorderLineStyle.NONE);
		groupLeftInfomationName.setAlignment(Alignment.LEFT);
		groupLeftInfomationName.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(GROUP_LEFT_INFOMATION_NAME, groupLeftInfomationName);
		
		//GROUP_LEFT_INFOMATION_NAME
		WritableCellFormat groupLeftInfomationNameFormatNumber = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		groupLeftInfomationNameFormatNumber.setBorder(Border.ALL, BorderLineStyle.NONE);
		groupLeftInfomationNameFormatNumber.setAlignment(Alignment.LEFT);
		groupLeftInfomationNameFormatNumber.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(GROUP_LEFT_INFOMATION_NAME_FORMAT_NUMBER, groupLeftInfomationNameFormatNumber);
		
		//GROUP_RIGHT_INFOMATION_NAME
		WritableCellFormat groupRightInfomationName = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		groupRightInfomationName.setBorder(Border.ALL, BorderLineStyle.NONE);
		groupRightInfomationName.setAlignment(Alignment.RIGHT);
		groupRightInfomationName.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(GROUP_RIGHT_INFOMATION_NAME, groupRightInfomationName);
		
		//GROUP_RIGHT_INFOMATION_NAME
		WritableCellFormat groupRightInfomationNameFormatNumber = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		groupRightInfomationNameFormatNumber.setBorder(Border.ALL, BorderLineStyle.NONE);
		groupRightInfomationNameFormatNumber.setAlignment(Alignment.RIGHT);
		groupRightInfomationNameFormatNumber.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(GROUP_RIGHT_INFOMATION_NAME_FORMAT_NUMBER, groupRightInfomationNameFormatNumber);
		
		//GROUP_CENTER_INFOMATION_NAME
		WritableCellFormat groupCenterInfomationName = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		groupCenterInfomationName.setBorder(Border.ALL, BorderLineStyle.NONE);
		groupCenterInfomationName.setAlignment(Alignment.CENTRE);
		groupCenterInfomationName.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(GROUP_CENTER_INFOMATION_NAME, groupCenterInfomationName);
		
		//GROUP_CENTER_INFOMATION_NAME_FORMAT_NUMBER
		WritableCellFormat groupCenterInfomationNameFormatNumber = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		groupCenterInfomationNameFormatNumber.setBorder(Border.ALL, BorderLineStyle.NONE);
		groupCenterInfomationNameFormatNumber.setAlignment(Alignment.CENTRE);
		groupCenterInfomationNameFormatNumber.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(GROUP_CENTER_INFOMATION_NAME_FORMAT_NUMBER, groupCenterInfomationNameFormatNumber);
	    
		//GROUP_LEFT_INFOMATION_NAME_PERCEN
		WritableCellFormat groupLeftInfomationNamePercen = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("0.0%"));//font BLACK
		groupLeftInfomationNamePercen.setBorder(Border.ALL, BorderLineStyle.NONE);
		groupLeftInfomationNamePercen.setAlignment(Alignment.LEFT);
		groupLeftInfomationNamePercen.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(GROUP_LEFT_INFOMATION_NAME_PERCEN, groupLeftInfomationNamePercen);
		
		//GROUP_RIGHT_INFOMATION_NAME_PERCEN
		WritableCellFormat groupRightInfomationNamePercen = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("0.0%"));//font BLACK
		groupRightInfomationNamePercen.setBorder(Border.ALL, BorderLineStyle.NONE);
		groupRightInfomationNamePercen.setAlignment(Alignment.RIGHT);
		groupRightInfomationNamePercen.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(GROUP_RIGHT_INFOMATION_NAME_PERCEN, groupRightInfomationNamePercen);
		
		//GROUP_CENTER_INFOMATION_NAME_PERCEN
		WritableCellFormat groupCenterInfomationNamePercen = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("0.0%"));//font BLACK
		groupCenterInfomationNamePercen.setBorder(Border.ALL, BorderLineStyle.NONE);
		groupCenterInfomationNamePercen.setAlignment(Alignment.CENTRE);
		groupCenterInfomationNamePercen.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(GROUP_CENTER_INFOMATION_NAME_PERCEN, groupCenterInfomationNamePercen);

		
		//TITLE
		WritableCellFormat title = new WritableCellFormat(tilteBoldBlue20);//font BlueSky
		title.setAlignment(Alignment.CENTRE);
		title.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(TITLE, title);
		
		//TITLE LEFT
		WritableCellFormat title_left = new WritableCellFormat(tilteBoldBlue20);//font BlueSky
		title_left.setAlignment(Alignment.LEFT);
		title_left.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(TITLE_LEFT, title_left);
		
		//TITLE_VNM_BLACK
		WritableCellFormat titleVnmBlack = new WritableCellFormat(tilteBoldBlack);//font Black
		titleVnmBlack.setAlignment(Alignment.CENTRE);
		titleVnmBlack.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(TITLE_VNM_BLACK, titleVnmBlack);
		
		//TIMES_TITLE
		WritableCellFormat timesTitle = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		timesTitle.setAlignment(Alignment.CENTRE);
		timesTitle.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(TIMES_TITLE, timesTitle);
		
		//TIMES_TITLE_LEFT
		WritableCellFormat timesTitleLeft = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		timesTitleLeft.setAlignment(Alignment.LEFT);
		timesTitleLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(TIMES_TITLE_LEFT, timesTitleLeft);
		
		
		//HEADER_BLUE_TOP_BOTTOM_MEDIUM
		WritableCellFormat headerBuleTopBottomThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBuleTopBottomThick.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBuleTopBottomThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBuleTopBottomThick.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		headerBuleTopBottomThick.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
		headerBuleTopBottomThick.setAlignment(Alignment.CENTRE);
		headerBuleTopBottomThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		//headerBuleTopBottomThick.setWrap(true);
		styles.put(HEADER_BLUE_TOP_BOTTOM_MEDIUM, headerBuleTopBottomThick);
		
		//HEADER_BLUE_TOP_RIGHT_MEDIUM
		WritableCellFormat headerBuleTopRightThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBuleTopRightThick.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBuleTopRightThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBuleTopRightThick.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		headerBuleTopRightThick.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
		headerBuleTopRightThick.setAlignment(Alignment.CENTRE);
		headerBuleTopRightThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		
		//HEADER_BLUE_ALL_THIN
		WritableCellFormat headerBlueAllThin = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueAllThin.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueAllThin.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueAllThin.setAlignment(Alignment.CENTRE);
		headerBlueAllThin.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_ALL_THIN, headerBlueAllThin);
		
		//HEADER_GRAY_ALL_THIN
		WritableCellFormat headerGrayAllThin = new WritableCellFormat(generalFontBoldBlack10);//font WHILE, backRound BLUE
		headerGrayAllThin.setBackground(Colour.getInternalColour(JXL_GRAY));
		headerGrayAllThin.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerGrayAllThin.setAlignment(Alignment.CENTRE);
		headerGrayAllThin.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_GRAY_ALL_THIN, headerGrayAllThin);
		
		//HEADER_GRAY01_ALL_THIN
		WritableCellFormat headerGray01AllThin = new WritableCellFormat(generalFontBoldBlack10);//font WHILE, backRound BLUE
		headerGray01AllThin.setBackground(Colour.getInternalColour(JXL_GRAY01));
		headerGray01AllThin.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerGray01AllThin.setAlignment(Alignment.CENTRE);
		headerGray01AllThin.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_GRAY01_ALL_THIN, headerGray01AllThin);
		
		//HEADER_GRAY01_ALL_THIN_LEFT
		WritableCellFormat headerGray01AllThinLeft = new WritableCellFormat(generalFontBoldBlack10);//font WHILE, backRound BLUE
		headerGray01AllThinLeft.setBackground(Colour.getInternalColour(JXL_GRAY01));
		headerGray01AllThinLeft.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerGray01AllThinLeft.setAlignment(Alignment.LEFT);
		headerGray01AllThinLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_GRAY01_ALL_THIN_LEFT, headerGray01AllThinLeft);
		
		//HEADER_GRAY01_ALL_THIN_RIGHT
		WritableCellFormat headerGray01AllThinRight = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font WHILE, backRound BLUE
		headerGray01AllThinRight.setBackground(Colour.getInternalColour(JXL_GRAY01));
		headerGray01AllThinRight.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerGray01AllThinRight.setAlignment(Alignment.RIGHT);
		headerGray01AllThinRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_GRAY01_ALL_THIN_RIGHT, headerGray01AllThinRight);
		
		//HEADER_GRAY02_ALL_THIN
		WritableCellFormat headerGray02AllThin = new WritableCellFormat(generalFontBoldBlack10);//font WHILE, backRound BLUE
		headerGray02AllThin.setBackground(Colour.getInternalColour(JXL_GRAY02));
		headerGray02AllThin.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerGray02AllThin.setAlignment(Alignment.CENTRE);
		headerGray02AllThin.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_GRAY02_ALL_THIN, headerGray02AllThin);
		
		//HEADER_GRAY02_ALL_THIN_LEFT
		WritableCellFormat headerGray02AllThinLeft = new WritableCellFormat(generalFontBoldBlack10);//font WHILE, backRound BLUE
		headerGray02AllThinLeft.setBackground(Colour.getInternalColour(JXL_GRAY02));
		headerGray02AllThinLeft.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerGray02AllThinLeft.setAlignment(Alignment.LEFT);
		headerGray02AllThinLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_GRAY02_ALL_THIN_LEFT, headerGray02AllThinLeft);
		
		//HEADER_GRAY02_ALL_THIN_RIGHT
		WritableCellFormat headerGray02AllThinRight = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font WHILE, backRound BLUE
		headerGray02AllThinRight.setBackground(Colour.getInternalColour(JXL_GRAY02));
		headerGray02AllThinRight.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerGray02AllThinRight.setAlignment(Alignment.RIGHT);
		headerGray02AllThinRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_GRAY02_ALL_THIN_RIGHT, headerGray02AllThinRight);
		
		//HEADER_INDIGO_ALL_THIN
		WritableCellFormat headerIndigoAllThin = new WritableCellFormat(generalFontBoldBlack10);//font WHILE, backRound BLUE
		headerIndigoAllThin.setBackground(Colour.getInternalColour(JXL_INDIGO1));
		headerIndigoAllThin.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerIndigoAllThin.setAlignment(Alignment.CENTRE);
		headerIndigoAllThin.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_INDIGO_ALL_THIN, headerIndigoAllThin);

		//HEADER_BLUE_ALL_THIN_NOBOLD
		WritableCellFormat headerBlueAllThinNoBold = new WritableCellFormat(hedderBoldWhileNoBold);//font WHILE, backRound BLUE
		headerBlueAllThinNoBold.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueAllThinNoBold.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueAllThinNoBold.setAlignment(Alignment.CENTRE);
		headerBlueAllThinNoBold.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_ALL_THIN_NOBOLD, headerBlueAllThinNoBold);
		
		//HEADER_BLUE_ALL_THIN_NOBOLD_LEFT_MEDIUM
		WritableCellFormat headerBlueAllThinNoBoldLeftThick = new WritableCellFormat(hedderBoldWhileNoBold);//font WHILE, backRound BLUE
		headerBlueAllThinNoBoldLeftThick.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueAllThinNoBoldLeftThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueAllThinNoBoldLeftThick.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
		headerBlueAllThinNoBoldLeftThick.setAlignment(Alignment.CENTRE);
		headerBlueAllThinNoBoldLeftThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_ALL_THIN_NOBOLD_LEFT_MEDIUM, headerBlueAllThinNoBoldLeftThick);
		//HEADER_BLUE_ALL_THIN_NOBOLD_RIGHT_MEDIUM
		WritableCellFormat headerBlueAllThinNoBoldRightThick = new WritableCellFormat(hedderBoldWhileNoBold);//font WHILE, backRound BLUE
		headerBlueAllThinNoBoldRightThick.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueAllThinNoBoldRightThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueAllThinNoBoldRightThick.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
		headerBlueAllThinNoBoldRightThick.setAlignment(Alignment.CENTRE);
		headerBlueAllThinNoBoldRightThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_ALL_THIN_NOBOLD_RIGHT_MEDIUM, headerBlueAllThinNoBoldRightThick);
		
		//HEADER_BLUE_ALL_THIN_TOP_MEDIUM
		WritableCellFormat headerBlueAllThinTopThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueAllThinTopThick.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueAllThinTopThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueAllThinTopThick.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		headerBlueAllThinTopThick.setAlignment(Alignment.CENTRE);
		headerBlueAllThinTopThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_ALL_THIN_TOP_MEDIUM, headerBlueAllThinTopThick);
		
		//HEADER_BLUE_R_L_THIN_TOP_MEDIUM
		WritableCellFormat headerBlueRLThinTopThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueRLThinTopThick.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueRLThinTopThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueRLThinTopThick.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		headerBlueRLThinTopThick.setBorder(Border.BOTTOM, BorderLineStyle.NONE);
		headerBlueRLThinTopThick.setAlignment(Alignment.CENTRE);
		headerBlueRLThinTopThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_R_L_THIN_TOP_MEDIUM, headerBlueRLThinTopThick);
		
		//HEADER_BLUE_RIGHT_THIN_TOP_MEDIUM
		WritableCellFormat headerBlueRightThinTopThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueRightThinTopThick.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueRightThinTopThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueRightThinTopThick.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		headerBlueRightThinTopThick.setBorder(Border.BOTTOM, BorderLineStyle.NONE);
		headerBlueRightThinTopThick.setBorder(Border.RIGHT, BorderLineStyle.NONE);
		headerBlueRightThinTopThick.setAlignment(Alignment.LEFT);
		headerBlueRightThinTopThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_RIGHT_THIN_TOP_MEDIUM, headerBlueRightThinTopThick);
		
		//HEADER_BLUE_ALL_THIN_BOTTOM_MEDIUM
		WritableCellFormat headerBlueAllThinBottomThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueAllThinBottomThick.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueAllThinBottomThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueAllThinBottomThick.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
		headerBlueAllThinBottomThick.setAlignment(Alignment.CENTRE);
		headerBlueAllThinBottomThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_ALL_THIN_BOTTOM_MEDIUM, headerBlueAllThinBottomThick);
		
		//HEADER_BLUE_R_L_THIN_BOTTOM_MEDIUM
		WritableCellFormat headerBlueRLThinBottomThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueRLThinBottomThick.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueRLThinBottomThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueRLThinBottomThick.setBorder(Border.TOP, BorderLineStyle.NONE);
		headerBlueRLThinBottomThick.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
		headerBlueRLThinBottomThick.setAlignment(Alignment.CENTRE);
		headerBlueRLThinBottomThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_R_L_THIN_BOTTOM_MEDIUM, headerBlueRLThinBottomThick);
		
		//HEADER_BLUESEE_ALL_THIN_BOTTOM_MEDIUM
		WritableCellFormat headerBlueSeeAllThinBottomThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueSeeAllThinBottomThick.setBackground(Colour.getInternalColour(JXL_BLUE_SEE));
		headerBlueSeeAllThinBottomThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueSeeAllThinBottomThick.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
		headerBlueSeeAllThinBottomThick.setAlignment(Alignment.CENTRE);
		headerBlueSeeAllThinBottomThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUESEE_ALL_THIN_BOTTOM_MEDIUM, headerBlueSeeAllThinBottomThick);
		
		//HEADER_BLUESEE_ALL_THIN_BOTTOM_MEDIUM_WRAP
		WritableCellFormat headerBlueSeeAllThinBottomThickWrap = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueSeeAllThinBottomThickWrap.setBackground(Colour.getInternalColour(JXL_BLUE_SEE));
		headerBlueSeeAllThinBottomThickWrap.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueSeeAllThinBottomThickWrap.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
		//headerBlueSeeAllThinBottomThickWrap.setWrap(true);
		headerBlueSeeAllThinBottomThickWrap.setAlignment(Alignment.CENTRE);
		headerBlueSeeAllThinBottomThickWrap.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUESEE_ALL_THIN_BOTTOM_MEDIUM_WRAP, headerBlueSeeAllThinBottomThickWrap);
		
		//HEADER_BLUESEE_ALL_THIN_TOP_MEDIUM
		WritableCellFormat headerBlueSeeAllThinTopThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueSeeAllThinTopThick.setBackground(Colour.getInternalColour(JXL_BLUE_SEE));
		headerBlueSeeAllThinTopThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueSeeAllThinTopThick.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		headerBlueSeeAllThinTopThick.setAlignment(Alignment.CENTRE);
		headerBlueSeeAllThinTopThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUESEE_ALL_THIN_TOP_MEDIUM, headerBlueSeeAllThinTopThick);
		
		//HEADER_BLUE_ALL_THIN_LEFT_MEDIUM
		WritableCellFormat headerBlueAllThinLeftThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueAllThinLeftThick.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueAllThinLeftThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueAllThinLeftThick.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
		headerBlueAllThinLeftThick.setAlignment(Alignment.CENTRE);
		headerBlueAllThinLeftThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_ALL_THIN_LEFT_MEDIUM, headerBlueAllThinLeftThick);
		
		//HEADER_BLUE_ALL_MEDIUM_LEFT_THIN
		WritableCellFormat headerBlueAllThickLeftThin = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueAllThickLeftThin.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueAllThickLeftThin.setBorder(Border.ALL, BorderLineStyle.MEDIUM);
		headerBlueAllThickLeftThin.setBorder(Border.LEFT, BorderLineStyle.THIN);
		headerBlueAllThickLeftThin.setAlignment(Alignment.CENTRE);
		headerBlueAllThickLeftThin.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_ALL_MEDIUM_LEFT_THIN, headerBlueAllThickLeftThin);
		
		//HEADER_BLUE_ALL_THIN_RIGTH_MEDIUM
		WritableCellFormat headerBlueAllThinRightThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueAllThinRightThick.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueAllThinRightThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueAllThinRightThick.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
		headerBlueAllThinRightThick.setAlignment(Alignment.CENTRE);
		headerBlueAllThinRightThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_ALL_THIN_RIGTH_MEDIUM, headerBlueAllThinRightThick);
		
		//HEADER_BLUESEE_ALL_THIN_TOP_MEDIUM_WRAP
		WritableCellFormat headerBlueSeeAllThinTopThickWrap = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueSeeAllThinTopThickWrap.setBackground(Colour.getInternalColour(JXL_BLUE_SEE));
		headerBlueSeeAllThinTopThickWrap.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerBlueSeeAllThinTopThickWrap.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		//headerBlueSeeAllThinTopThickWrap.setWrap(true);
		headerBlueSeeAllThinTopThickWrap.setAlignment(Alignment.CENTRE);
		headerBlueSeeAllThinTopThickWrap.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUESEE_ALL_THIN_TOP_MEDIUM_WRAP, headerBlueSeeAllThinTopThickWrap);
		
		//HEADER_GREEN_ALL_THIN_BOTTOM_MEDIUM
		WritableCellFormat headerGreenAllThinBottomThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerGreenAllThinBottomThick.setBackground(Colour.getInternalColour(JXL_GREEN));
		headerGreenAllThinBottomThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerGreenAllThinBottomThick.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
		headerGreenAllThinBottomThick.setAlignment(Alignment.CENTRE);
		headerGreenAllThinBottomThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_GREEN_ALL_THIN_BOTTOM_MEDIUM, headerGreenAllThinBottomThick);
		
		//HEADER_GREEN_ALL_THIN_TOP_MEDIUM
		WritableCellFormat headerGreenAllThinTopThick = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerGreenAllThinTopThick.setBackground(Colour.getInternalColour(JXL_GREEN));
		headerGreenAllThinTopThick.setBorder(Border.ALL, BorderLineStyle.THIN);
		headerGreenAllThinTopThick.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		headerGreenAllThinTopThick.setAlignment(Alignment.CENTRE);
		headerGreenAllThinTopThick.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_GREEN_ALL_THIN_TOP_MEDIUM, headerGreenAllThinTopThick);
		//HEADER_BLUE_ALL_MEDIUM_BOTTOM_THIN
		WritableCellFormat headerBlueAllThickBottomThin = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueAllThickBottomThin.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueAllThickBottomThin.setBorder(Border.ALL, BorderLineStyle.MEDIUM);
		headerBlueAllThickBottomThin.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
		headerBlueAllThickBottomThin.setAlignment(Alignment.CENTRE);
		headerBlueAllThickBottomThin.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(HEADER_BLUE_ALL_MEDIUM_BOTTOM_THIN, headerBlueAllThickBottomThin);
		//HEADER_BLUE_ALL_MEDIUM_BOTTOM_THIN_WRAP
		WritableCellFormat headerBlueAllThickBottomThinWrap = new WritableCellFormat(hedderBoldWhile);//font WHILE, backRound BLUE
		headerBlueAllThickBottomThinWrap.setBackground(Colour.getInternalColour(JXL_BLUE));
		headerBlueAllThickBottomThinWrap.setBorder(Border.ALL, BorderLineStyle.MEDIUM);
		headerBlueAllThickBottomThinWrap.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
		headerBlueAllThickBottomThinWrap.setAlignment(Alignment.CENTRE);
		headerBlueAllThickBottomThinWrap.setVerticalAlignment(VerticalAlignment.CENTRE);
		//headerBlueAllThickBottomThinWrap.setWrap(true);
		styles.put(HEADER_BLUE_ALL_MEDIUM_BOTTOM_THIN_WRAP, headerBlueAllThickBottomThinWrap);
		
		//ROW_ALL_THIN_CENTRE
		WritableCellFormat rowAllThinCentre = new WritableCellFormat(generalFontBlack10);//font BLACK
		rowAllThinCentre.setBorder(Border.ALL, BorderLineStyle.THIN);
		rowAllThinCentre.setAlignment(Alignment.CENTRE);
		rowAllThinCentre.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_ALL_THIN_CENTRE, rowAllThinCentre);
		
		//NO_BORDER_THIN_CENTRE
		WritableCellFormat noBorderThinCentre = new WritableCellFormat(generalFontBlack10);//font BLACK
		noBorderThinCentre.setBorder(Border.ALL, BorderLineStyle.NONE);
		noBorderThinCentre.setAlignment(Alignment.CENTRE);
		noBorderThinCentre.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(NO_BORDER_THIN_CENTRE, noBorderThinCentre);
		
		//NO_BORDER_THIN_CENTRE_NUMBER
		WritableCellFormat noBorderThinCentreNumber = new WritableCellFormat(generalFontBlack10, new NumberFormat("#,##0"));//font BLACK
		noBorderThinCentreNumber.setBorder(Border.ALL, BorderLineStyle.NONE);
		noBorderThinCentreNumber.setAlignment(Alignment.CENTRE);
		noBorderThinCentreNumber.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(NO_BORDER_THIN_CENTRE_NUMBER, noBorderThinCentreNumber);
		
		//ROW_ALL_THIN_LEFT
		WritableCellFormat rowAllThinLeft = new WritableCellFormat(generalFontBlack10, NumberFormats.TEXT);//font BLACK
		rowAllThinLeft.setBorder(Border.ALL, BorderLineStyle.THIN);
		rowAllThinLeft.setAlignment(Alignment.LEFT);
		rowAllThinLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		//rowAllThinLeft.setWrap(true);
		styles.put(ROW_ALL_THIN_LEFT, rowAllThinLeft);
		
		//ROW_ALL_THIN_LEFT
		WritableCellFormat noBorderThinLeft = new WritableCellFormat(generalFontBlack10, NumberFormats.TEXT);//font BLACK
		noBorderThinLeft.setBorder(Border.ALL, BorderLineStyle.NONE);
		noBorderThinLeft.setAlignment(Alignment.LEFT);
		noBorderThinLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(NO_BORDER_THIN_LEFT, noBorderThinLeft);
		
		//ROW_ALL_THIN_RIGHT
		WritableCellFormat rowAllThinRight = new WritableCellFormat(generalFontBlack10, new NumberFormat("#,##"));//font BLACK
		rowAllThinRight.setBorder(Border.ALL, BorderLineStyle.THIN);
		rowAllThinRight.setAlignment(Alignment.RIGHT);
		rowAllThinRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_ALL_THIN_RIGHT, rowAllThinRight);
		
		//ROW_ALL_THIN_RIGHT
		WritableCellFormat noBorderThinRight = new WritableCellFormat(generalFontBlack10, new NumberFormat("#,##"));//font BLACK
		noBorderThinRight.setBorder(Border.ALL, BorderLineStyle.NONE);
		noBorderThinRight.setAlignment(Alignment.RIGHT);
		noBorderThinRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(NO_BORDER_THIN_RIGHT, noBorderThinRight);
		
		
		//ROW_ALL_THIN_RIGHT_NUMBER_FORMAT_DECIMAL
		WritableCellFormat rowAllThinRightFormatDecimal = new WritableCellFormat(generalFontBlack10, new NumberFormat("#,###0.00"));//font BLACK
		rowAllThinRightFormatDecimal.setBorder(Border.ALL, BorderLineStyle.THIN);
		rowAllThinRightFormatDecimal.setAlignment(Alignment.RIGHT);
		rowAllThinRightFormatDecimal.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_ALL_THIN_RIGHT_NUMBER_FORMAT_DECIMAL, rowAllThinRightFormatDecimal);
		
		//ROW_ALL_THIN_RIGHT_NUMBER_FORMAT_THOUSAND
		WritableCellFormat rowAllThinRightFormatThousand = new WritableCellFormat(generalFontBlack10, new NumberFormat("#,###0"));//font BLACK
		rowAllThinRightFormatThousand.setBorder(Border.ALL, BorderLineStyle.THIN);
		rowAllThinRightFormatThousand.setAlignment(Alignment.RIGHT);
		rowAllThinRightFormatThousand.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_ALL_THIN_RIGHT_NUMBER_FORMAT_THOUSAND, rowAllThinRightFormatThousand);
		
		//ROW_NO_BORDER_RIGHT_NUMBER_FORMAT_THOUSAND
		WritableCellFormat rowNoBorderRightFormatThousand = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,###0"));//font BLACK
		rowNoBorderRightFormatThousand.setAlignment(Alignment.RIGHT);
		rowNoBorderRightFormatThousand.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_NO_BORDER_RIGHT_NUMBER_FORMAT_THOUSAND, rowNoBorderRightFormatThousand);
		
		
		//ROW_DOTTED_LEFT
		WritableCellFormat rowDottedLeft = new WritableCellFormat(generalFontBlack10, NumberFormats.TEXT);//font BLACK
		rowDottedLeft.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeft.setAlignment(Alignment.LEFT);
		rowDottedLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT, rowDottedLeft);
		
		//ROW_DOTTED_LEFT_BORDER_LEFT
		WritableCellFormat rowDottedLeftBorderLeft = new WritableCellFormat(generalFontBlack10);//font BLACK
		rowDottedLeftBorderLeft.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftBorderLeft.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
		rowDottedLeftBorderLeft.setAlignment(Alignment.LEFT);
		rowDottedLeftBorderLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_BORDER_LEFT, rowDottedLeftBorderLeft);
		
		//ROW_DOTTED_LEFT_BORDER_RIGHT
		WritableCellFormat rowDottedLeftBorderRight = new WritableCellFormat(generalFontBlack10);//font BLACK
		rowDottedLeftBorderRight.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftBorderRight.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
		rowDottedLeftBorderRight.setAlignment(Alignment.LEFT);
		rowDottedLeftBorderRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_BORDER_RIGHT, rowDottedLeftBorderRight);
		
		//ROW_DOTTED_RIGTH_BORDER_LEFT
		WritableCellFormat rowDottedRigthBorderLeft = new WritableCellFormat(generalFontBlack10,  new NumberFormat("#,##"));//font BLACK
		rowDottedRigthBorderLeft.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthBorderLeft.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
		rowDottedRigthBorderLeft.setAlignment(Alignment.RIGHT);
		rowDottedRigthBorderLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_BORDER_LEFT, rowDottedRigthBorderLeft);
		
		//ROW_DOTTED_RIGTH_BORDER_RIGHT
		WritableCellFormat rowDottedRigthBorderRight = new WritableCellFormat(generalFontBlack10,  new NumberFormat("#,##"));//font BLACK
		rowDottedRigthBorderRight.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthBorderRight.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
		rowDottedRigthBorderRight.setAlignment(Alignment.RIGHT);
		rowDottedRigthBorderRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_BORDER_RIGHT, rowDottedRigthBorderRight);
		
		//ROW_DOTTED_LEFT_BLUELIGHT1
		WritableCellFormat rowDottedLeftBlueLight1 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftBlueLight1.setBackground(Colour.getInternalColour(JXL_BLUE_LIGHT01));
		rowDottedLeftBlueLight1.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftBlueLight1.setAlignment(Alignment.LEFT);
		rowDottedLeftBlueLight1.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_BLUELIGHT1, rowDottedLeftBlueLight1);
		
		//ROW_DOTTED_RIGHT_BLUELIGHT1
		WritableCellFormat rowDottedRightBlueLight1 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRightBlueLight1.setBackground(Colour.getInternalColour(JXL_BLUE_LIGHT01));
		rowDottedRightBlueLight1.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRightBlueLight1.setAlignment(Alignment.RIGHT);
		rowDottedRightBlueLight1.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGHT_BLUELIGHT1, rowDottedRightBlueLight1);
		
		//ROW_COUNT_HOZITAL
		WritableCellFormat rowCountHozital = new WritableCellFormat(generalFontBlack10);//font BLACK
		rowCountHozital.setBackground(Colour.getInternalColour(JXL_BLUE_LIGHT01));
		rowCountHozital.setBorder(Border.ALL, BorderLineStyle.THIN);
		rowCountHozital.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		rowCountHozital.setAlignment(Alignment.CENTRE);
		rowCountHozital.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_COUNT_HOZITAL, rowCountHozital);
		
		//ROW_COUNT_HOZITAL_BODER_LEFT
		WritableCellFormat rowCountHozitalBoderLeft = new WritableCellFormat(generalFontBlack10);//font BLACK
		rowCountHozitalBoderLeft.setBackground(Colour.getInternalColour(JXL_BLUE_LIGHT01));
		rowCountHozitalBoderLeft.setBorder(Border.ALL, BorderLineStyle.THIN);
		rowCountHozitalBoderLeft.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		rowCountHozitalBoderLeft.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
		rowCountHozitalBoderLeft.setAlignment(Alignment.CENTRE);
		rowCountHozitalBoderLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_COUNT_HOZITAL_BODER_LEFT, rowCountHozitalBoderLeft);
		
		//ROW_COUNT_HOZITAL_BODER_RIGHT
		WritableCellFormat rowCountHozitalBoderRight = new WritableCellFormat(generalFontBlack10);//font BLACK
		rowCountHozitalBoderRight.setBackground(Colour.getInternalColour(JXL_BLUE_LIGHT01));
		rowCountHozitalBoderRight.setBorder(Border.ALL, BorderLineStyle.THIN);
		rowCountHozitalBoderRight.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		rowCountHozitalBoderRight.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
		rowCountHozitalBoderRight.setAlignment(Alignment.CENTRE);
		rowCountHozitalBoderRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_COUNT_HOZITAL_BODER_RIGHT, rowCountHozitalBoderRight);
		
		//ROW_DOTTED_RIGTH
		WritableCellFormat rowDottedRight = new WritableCellFormat(generalFontBlack10, new NumberFormat("#,##"));//font BLACK
		rowDottedRight.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRight.setAlignment(Alignment.RIGHT);
		rowDottedRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH, rowDottedRight);
		
		//vuongmq: kieu so - in dam - ORANGE01(mau cam) - canh phai
		//ROW_DOTTED_RIGTH_ORANGE01_DOUBLE_FORMATER_ZERO 
		WritableCellFormat rowDottedRightOrange1DoubleFormaterZero = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font Bold BLACK 
		rowDottedRightOrange1DoubleFormaterZero.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRightOrange1DoubleFormaterZero.setBackground(Colour.getInternalColour(JXL_ORANGE01));
		rowDottedRightOrange1DoubleFormaterZero.setAlignment(Alignment.RIGHT);
		rowDottedRightOrange1DoubleFormaterZero.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE01_DOUBLE_FORMATER_ZERO, rowDottedRightOrange1DoubleFormaterZero);
		
		
		//ROW_DOTTED_RIGTH_DOUBLE_FORMATER_ZERO
		WritableCellFormat rowDottedRightDoubleFormaterZero = new WritableCellFormat(generalFontBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRightDoubleFormaterZero.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRightDoubleFormaterZero.setAlignment(Alignment.RIGHT);
		rowDottedRightDoubleFormaterZero.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_DOUBLE_FORMATER_ZERO, rowDottedRightDoubleFormaterZero);
		
		//ROW_THIN_RIGTH_DOUBLE_FORMATER_ZERO
		WritableCellFormat rowThinRightDoubleFormaterZero = new WritableCellFormat(generalFontBlack10, new NumberFormat("#,##0"));//font BLACK
		rowThinRightDoubleFormaterZero.setBorder(Border.ALL, BorderLineStyle.THIN);
		rowThinRightDoubleFormaterZero.setAlignment(Alignment.RIGHT);
		rowThinRightDoubleFormaterZero.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_THIN_RIGTH_DOUBLE_FORMATER_ZERO, rowThinRightDoubleFormaterZero);
		
		//ROW_DOTTED_RIGTH_PERCENT
		WritableCellFormat rowDottedRighPercent = new WritableCellFormat(generalFontBlack10, new NumberFormat("0.0%"));//font BLACK
		rowDottedRighPercent.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRighPercent.setAlignment(Alignment.RIGHT);
		rowDottedRighPercent.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_PERCENT, rowDottedRighPercent);
		
		//ROW_DOTTED_CENTRE
		WritableCellFormat rowDottedCentre = new WritableCellFormat(generalFontBlack10);//font BLACK
		rowDottedCentre.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedCentre.setAlignment(Alignment.CENTRE);
		rowDottedCentre.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_CENTRE, rowDottedCentre);
		
		//ROW_DOTTED_CENTRE_NUMBER
		WritableCellFormat rowDottedCentreNumber = new WritableCellFormat(generalFontBlack10, new NumberFormat("#,##"));//font BLACK
		rowDottedCentreNumber.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedCentreNumber.setAlignment(Alignment.CENTRE);
		rowDottedCentreNumber.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_CENTRE_NUMBER, rowDottedCentreNumber);
		
		/*--Font 6 mau cam--*/
		//ROW_DOTTED_LEFT_ORANGE01
		WritableCellFormat rowDottedLeftOrange01 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange01.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftOrange01.setBackground(Colour.getInternalColour(JXL_ORANGE01));
		rowDottedLeftOrange01.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange01.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE01, rowDottedLeftOrange01);
		//ROW_DOTTED_LEFT_ORANGE01_FONT_RED
		WritableCellFormat rowDottedLeftOrange01Red = new WritableCellFormat(generalFontBoldBlack10Red);//font BLACK
		rowDottedLeftOrange01Red.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftOrange01Red.setBackground(Colour.getInternalColour(JXL_ORANGE01));
		rowDottedLeftOrange01Red.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange01Red.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE01_FONT_RED, rowDottedLeftOrange01Red);
		//ROW_DOTTED_LEFT_ORANGE01_BORDER_LEFT
		WritableCellFormat rowDottedLeftOrange01BorderLeft = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange01BorderLeft.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftOrange01BorderLeft.setBackground(Colour.getInternalColour(JXL_ORANGE01));
		rowDottedLeftOrange01BorderLeft.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
		rowDottedLeftOrange01BorderLeft.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange01BorderLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE01_BORDER_LEFT, rowDottedLeftOrange01BorderLeft);
		//ROW_DOTTED_LEFT_ORANGE01_BORDER_LEFT
		WritableCellFormat rowDottedLeftOrange01BorderRight = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange01BorderRight.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftOrange01BorderRight.setBackground(Colour.getInternalColour(JXL_ORANGE01));
		rowDottedLeftOrange01BorderRight.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
		rowDottedLeftOrange01BorderRight.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange01BorderRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE01_BORDER_RIGHT, rowDottedLeftOrange01BorderRight);
		//ROW_DOTTED_RIGTH_ORANGE01_BORDER_LEFT
		WritableCellFormat rowDottedRigthOrange01BorderLeft = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRigthOrange01BorderLeft.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthOrange01BorderLeft.setBackground(Colour.getInternalColour(JXL_ORANGE01));
		rowDottedRigthOrange01BorderLeft.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
		rowDottedRigthOrange01BorderLeft.setAlignment(Alignment.RIGHT);
		rowDottedRigthOrange01BorderLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE01_BORDER_LEFT, rowDottedRigthOrange01BorderLeft);
		//ROW_DOTTED_RIGTH_ORANGE01_BORDER_RIGHT
		WritableCellFormat rowDottedRigthOrange01BorderRight = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRigthOrange01BorderRight.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthOrange01BorderRight.setBackground(Colour.getInternalColour(JXL_ORANGE01));
		rowDottedRigthOrange01BorderRight.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
		rowDottedRigthOrange01BorderRight.setAlignment(Alignment.RIGHT);
		rowDottedRigthOrange01BorderRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE01_BORDER_RIGHT, rowDottedRigthOrange01BorderRight);
		//ROW_DOTTED_LEFT_ORANGE02
		WritableCellFormat rowDottedLeftOrange02 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange02.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftOrange02.setBackground(Colour.getInternalColour(JXL_ORANGE02));
		rowDottedLeftOrange02.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange02.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE02, rowDottedLeftOrange02);
		//ROW_DOTTED_LEFT_ORANGE02_BORDER_LEFT
		WritableCellFormat rowDottedLeftOrange02BorderLeft = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange02BorderLeft.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftOrange02BorderLeft.setBackground(Colour.getInternalColour(JXL_ORANGE02));
		rowDottedLeftOrange02BorderLeft.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
		rowDottedLeftOrange02BorderLeft.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange02BorderLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE02_BORDER_LEFT, rowDottedLeftOrange02BorderLeft);
		//ROW_DOTTED_LEFT_ORANGE02_BORDER_LEFT
		WritableCellFormat rowDottedLeftOrange02BorderRight = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange02BorderRight.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftOrange02BorderRight.setBackground(Colour.getInternalColour(JXL_ORANGE02));
		rowDottedLeftOrange02BorderRight.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
		rowDottedLeftOrange02BorderRight.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange02BorderRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE02_BORDER_RIGHT, rowDottedLeftOrange02BorderRight);
		//ROW_DOTTED_RIGTH_ORANGE02_BORDER_LEFT
		WritableCellFormat rowDottedRigthOrange02BorderLeft = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRigthOrange02BorderLeft.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthOrange02BorderLeft.setBackground(Colour.getInternalColour(JXL_ORANGE02));
		rowDottedRigthOrange02BorderLeft.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
		rowDottedRigthOrange02BorderLeft.setAlignment(Alignment.RIGHT);
		rowDottedRigthOrange02BorderLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE02_BORDER_LEFT, rowDottedRigthOrange02BorderLeft);
		//ROW_DOTTED_RIGTH_ORANGE02_BORDER_RIGHT
		WritableCellFormat rowDottedRigthOrange02BorderRigth = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRigthOrange02BorderRigth.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthOrange02BorderRigth.setBackground(Colour.getInternalColour(JXL_ORANGE02));
		rowDottedRigthOrange02BorderRigth.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
		rowDottedRigthOrange02BorderRigth.setAlignment(Alignment.RIGHT);
		rowDottedRigthOrange02BorderRigth.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE02_BORDER_RIGHT, rowDottedRigthOrange02BorderRigth);
		//ROW_DOTTED_LEFT_ORANGE03
		WritableCellFormat rowDottedLeftOrange03 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange03.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftOrange03.setBackground(Colour.getInternalColour(JXL_ORANGE03));
		rowDottedLeftOrange03.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange03.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE03, rowDottedLeftOrange03);
		//ROW_DOTTED_LEFT_ORANGE03_BORDER_TOP_BOTTOM
		WritableCellFormat rowDottedLeftOrange03BorderTopBottom = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange03BorderTopBottom.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftOrange03BorderTopBottom.setBackground(Colour.getInternalColour(JXL_ORANGE03));
		rowDottedLeftOrange03BorderTopBottom.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		rowDottedLeftOrange03BorderTopBottom.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
		rowDottedLeftOrange03BorderTopBottom.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange03BorderTopBottom.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE03_BORDER_TOP_BOTTOM, rowDottedLeftOrange03BorderTopBottom);
		//ROW_DOTTED_LEFT_ORANGE03_BORDER_TOP_BOTTOM_LEFT
		WritableCellFormat rowDottedLeftOrange03BorderTopBottomLeft = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange03BorderTopBottomLeft.setBorder(Border.ALL, BorderLineStyle.MEDIUM);
		rowDottedLeftOrange03BorderTopBottomLeft.setBackground(Colour.getInternalColour(JXL_ORANGE03));
		rowDottedLeftOrange03BorderTopBottomLeft.setBorder(Border.RIGHT, BorderLineStyle.DOTTED);
		rowDottedLeftOrange03BorderTopBottomLeft.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange03BorderTopBottomLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE03_BORDER_TOP_BOTTOM_LEFT, rowDottedLeftOrange03BorderTopBottomLeft);
		//ROW_DOTTED_LEFT_ORANGE03_BORDER_TOP_BOTTOM_RIGHT
		WritableCellFormat rowDottedLeftOrange03BorderTopBottomRight = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange03BorderTopBottomRight.setBorder(Border.ALL, BorderLineStyle.MEDIUM);
		rowDottedLeftOrange03BorderTopBottomRight.setBackground(Colour.getInternalColour(JXL_ORANGE03));
		rowDottedLeftOrange03BorderTopBottomRight.setBorder(Border.LEFT, BorderLineStyle.DOTTED);
		rowDottedLeftOrange03BorderTopBottomRight.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange03BorderTopBottomRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE03_BORDER_TOP_BOTTOM_RIGHT, rowDottedLeftOrange03BorderTopBottomRight);
		//ROW_DOTTED_RIGTH_ORANGE03_BORDER_TOP_BOTTOM
		WritableCellFormat rowDottedRightOrange03BorderTopBottom = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRightOrange03BorderTopBottom.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRightOrange03BorderTopBottom.setBackground(Colour.getInternalColour(JXL_ORANGE03));
		rowDottedRightOrange03BorderTopBottom.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
		rowDottedRightOrange03BorderTopBottom.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
		rowDottedRightOrange03BorderTopBottom.setAlignment(Alignment.RIGHT);
		rowDottedRightOrange03BorderTopBottom.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE03_BORDER_TOP_BOTTOM, rowDottedRightOrange03BorderTopBottom);
		//ROW_DOTTED_RIGTH_ORANGE03_BORDER_TOP_BOTTOM_LEFT
		WritableCellFormat rowDottedRightOrange03BorderTopBottomLeftt = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRightOrange03BorderTopBottomLeftt.setBorder(Border.ALL, BorderLineStyle.MEDIUM);
		rowDottedRightOrange03BorderTopBottomLeftt.setBackground(Colour.getInternalColour(JXL_ORANGE03));
		rowDottedRightOrange03BorderTopBottomLeftt.setBorder(Border.RIGHT, BorderLineStyle.DOTTED);
		rowDottedRightOrange03BorderTopBottomLeftt.setAlignment(Alignment.RIGHT);
		rowDottedRightOrange03BorderTopBottomLeftt.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE03_BORDER_TOP_BOTTOM_LEFT, rowDottedRightOrange03BorderTopBottomLeftt);
		//ROW_DOTTED_RIGTH_ORANGE03_BORDER_TOP_BOTTOM_RIGHT
		WritableCellFormat rowDottedRightOrange03BorderTopBottomRight = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRightOrange03BorderTopBottomRight.setBorder(Border.ALL, BorderLineStyle.MEDIUM);
		rowDottedRightOrange03BorderTopBottomRight.setBackground(Colour.getInternalColour(JXL_ORANGE03));
		rowDottedRightOrange03BorderTopBottomRight.setBorder(Border.LEFT, BorderLineStyle.DOTTED);
		rowDottedRightOrange03BorderTopBottomRight.setAlignment(Alignment.RIGHT);
		rowDottedRightOrange03BorderTopBottomRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE03_BORDER_TOP_BOTTOM_RIGHT, rowDottedRightOrange03BorderTopBottomRight);
		
		//ROW_DOTTED_LEFT_ORANGE04
		WritableCellFormat rowDottedLeftOrange04 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange04.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftOrange04.setBackground(Colour.getInternalColour(JXL_ORANGE04));
		rowDottedLeftOrange04.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange04.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE04, rowDottedLeftOrange04);
		//ROW_DOTTED_LEFT_ORANGE05
		WritableCellFormat rowDottedLeftOrange05 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange05.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftOrange05.setBackground(Colour.getInternalColour(JXL_ORANGE05));
		rowDottedLeftOrange05.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange05.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE05, rowDottedLeftOrange05);
		//ROW_DOTTED_LEFT_ORANGE06
		WritableCellFormat rowDottedLeftOrange06 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftOrange06.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftOrange06.setBackground(Colour.getInternalColour(JXL_ORANGE06));
		rowDottedLeftOrange06.setAlignment(Alignment.LEFT);
		rowDottedLeftOrange06.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_ORANGE06, rowDottedLeftOrange06);
		
		//ROW_DOTTED_CENTRE_ORANGE01
		WritableCellFormat rowDottedCentreOrange01 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedCentreOrange01.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedCentreOrange01.setBackground(Colour.getInternalColour(JXL_ORANGE01));
		rowDottedCentreOrange01.setAlignment(Alignment.CENTRE);
		rowDottedCentreOrange01.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_CENTRE_ORANGE01, rowDottedCentreOrange01);
		//ROW_DOTTED_CENTRE_ORANGE02
		WritableCellFormat rowDottedCentreOrange02 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedCentreOrange02.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedCentreOrange02.setBackground(Colour.getInternalColour(JXL_ORANGE02));
		rowDottedCentreOrange02.setAlignment(Alignment.CENTRE);
		rowDottedCentreOrange02.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_CENTRE_ORANGE02, rowDottedCentreOrange02);
		//ROW_DOTTED_CENTRE_ORANGE03
		WritableCellFormat rowDottedCentreOrange03 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedCentreOrange03.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedCentreOrange03.setBackground(Colour.getInternalColour(JXL_ORANGE03));
		rowDottedCentreOrange03.setAlignment(Alignment.CENTRE);
		rowDottedCentreOrange03.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_CENTRE_ORANGE03, rowDottedCentreOrange03);
		//ROW_DOTTED_CENTRE_ORANGE04
		WritableCellFormat rowDottedCentreOrange04 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedCentreOrange04.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedCentreOrange04.setBackground(Colour.getInternalColour(JXL_ORANGE04));
		rowDottedCentreOrange04.setAlignment(Alignment.CENTRE);
		rowDottedCentreOrange04.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_CENTRE_ORANGE04, rowDottedCentreOrange04);
		//ROW_DOTTED_CENTRE_ORANGE05
		WritableCellFormat rowDottedCentreOrange05 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedCentreOrange05.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedCentreOrange05.setBackground(Colour.getInternalColour(JXL_ORANGE05));
		rowDottedCentreOrange05.setAlignment(Alignment.CENTRE);
		rowDottedCentreOrange05.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_CENTRE_ORANGE05, rowDottedCentreOrange05);
		//ROW_DOTTED_CENTRE_ORANGE06
		WritableCellFormat rowDottedCentreOrange06 = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedCentreOrange06.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedCentreOrange06.setBackground(Colour.getInternalColour(JXL_ORANGE06));
		rowDottedCentreOrange06.setAlignment(Alignment.CENTRE);
		rowDottedCentreOrange06.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_CENTRE_ORANGE06, rowDottedCentreOrange06);
		
		//ROW_DOTTED_RIGTH_ORANGE01
		WritableCellFormat rowDottedRigthOrange01 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRigthOrange01.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthOrange01.setBackground(Colour.getInternalColour(JXL_ORANGE01));
		rowDottedRigthOrange01.setAlignment(Alignment.RIGHT);
		rowDottedRigthOrange01.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE01, rowDottedRigthOrange01);
		//ROW_DOTTED_RIGTH_ORANGE01_PERCENT
		WritableCellFormat rowDottedRigthOrange01Percent = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("0.0%"));//font BLACK
		rowDottedRigthOrange01Percent.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthOrange01Percent.setBackground(Colour.getInternalColour(JXL_ORANGE01));
		rowDottedRigthOrange01Percent.setAlignment(Alignment.RIGHT);
		rowDottedRigthOrange01Percent.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE01_PERCENT, rowDottedRigthOrange01Percent);
		//ROW_DOTTED_RIGTH_ORANGE02
		WritableCellFormat rowDottedRigthOrange02 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRigthOrange02.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthOrange02.setBackground(Colour.getInternalColour(JXL_ORANGE02));
		rowDottedRigthOrange02.setAlignment(Alignment.RIGHT);
		rowDottedRigthOrange02.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE02, rowDottedRigthOrange02);
		//ROW_DOTTED_LEFT_ORANGE03
		WritableCellFormat rowDottedRigthOrange03 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRigthOrange03.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthOrange03.setBackground(Colour.getInternalColour(JXL_ORANGE03));
		rowDottedRigthOrange03.setAlignment(Alignment.RIGHT);
		rowDottedRigthOrange03.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE03, rowDottedRigthOrange03);
		//ROW_DOTTED_RIGTH_ORANGE04
		WritableCellFormat rowDottedRigthOrange04 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRigthOrange04.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthOrange04.setBackground(Colour.getInternalColour(JXL_ORANGE04));
		rowDottedRigthOrange04.setAlignment(Alignment.RIGHT);
		rowDottedRigthOrange04.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE04, rowDottedRigthOrange04);
		//ROW_DOTTED_RIGTH_ORANGE05
		WritableCellFormat rowDottedRigthOrange05 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRigthOrange05.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthOrange05.setBackground(Colour.getInternalColour(JXL_ORANGE05));
		rowDottedRigthOrange05.setAlignment(Alignment.RIGHT);
		rowDottedRigthOrange05.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE05, rowDottedRigthOrange05);
		//ROW_DOTTED_RIGTH_ORANGE06
		WritableCellFormat rowDottedRigthOrange06 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowDottedRigthOrange06.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthOrange06.setBackground(Colour.getInternalColour(JXL_ORANGE06));
		rowDottedRigthOrange06.setAlignment(Alignment.RIGHT);
		rowDottedRigthOrange06.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_ORANGE06, rowDottedRigthOrange06);
		
		//ROW_DOTTED_PERCENT_ORANGE01
		WritableCellFormat rowDottedPercentOrange01 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("0.0%"));//font BLACK
		rowDottedPercentOrange01.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedPercentOrange01.setBackground(Colour.getInternalColour(JXL_ORANGE01));
		rowDottedPercentOrange01.setAlignment(Alignment.RIGHT);
		rowDottedPercentOrange01.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_PERCENT_ORANGE01, rowDottedPercentOrange01);
		//ROW_DOTTED_PERCENT_ORANGE02
		WritableCellFormat rowDottedPercentOrange02 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("0.0%"));//font BLACK
		rowDottedPercentOrange02.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedPercentOrange02.setBackground(Colour.getInternalColour(JXL_ORANGE02));
		rowDottedPercentOrange02.setAlignment(Alignment.RIGHT);
		rowDottedPercentOrange02.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_PERCENT_ORANGE02, rowDottedPercentOrange02);
		//ROW_DOTTED_PERCENT_ORANGE03
		WritableCellFormat rowDottedPercentOrange03 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("0.0%"));//font BLACK
		rowDottedPercentOrange03.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedPercentOrange03.setBackground(Colour.getInternalColour(JXL_ORANGE03));
		rowDottedPercentOrange03.setAlignment(Alignment.RIGHT);
		rowDottedPercentOrange03.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_PERCENT_ORANGE03, rowDottedPercentOrange03);
		//ROW_DOTTED_PERCENT_ORANGE04
		WritableCellFormat rowDottedPercentOrange04 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("0.0%"));//font BLACK
		rowDottedPercentOrange04.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedPercentOrange04.setBackground(Colour.getInternalColour(JXL_ORANGE04));
		rowDottedPercentOrange04.setAlignment(Alignment.RIGHT);
		rowDottedPercentOrange04.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_PERCENT_ORANGE04, rowDottedPercentOrange04);
		//ROW_DOTTED_PERCENT_ORANGE05
		WritableCellFormat rowDottedPercentOrange05 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("0.0%"));//font BLACK
		rowDottedPercentOrange05.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedPercentOrange05.setBackground(Colour.getInternalColour(JXL_ORANGE05));
		rowDottedPercentOrange05.setAlignment(Alignment.RIGHT);
		rowDottedPercentOrange05.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_PERCENT_ORANGE05, rowDottedRigthOrange05);
		//ROW_DOTTED_PERCENT_ORANGE06
		WritableCellFormat rowDottedPercentOrange06 = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("0.0%"));//font BLACK
		rowDottedPercentOrange06.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedPercentOrange06.setBackground(Colour.getInternalColour(JXL_ORANGE06));
		rowDottedPercentOrange06.setAlignment(Alignment.RIGHT);
		rowDottedPercentOrange06.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_PERCENT_ORANGE06, rowDottedRigthOrange06);
		
		//ROW_DOTTED_LEFT_RED
		WritableCellFormat rowDottedLeftRed = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowDottedLeftRed.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftRed.setBackground(Colour.getInternalColour(JXL_RED));
		rowDottedLeftRed.setAlignment(Alignment.LEFT);
		rowDottedLeftRed.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_RED, rowDottedLeftRed);
		//ROW_DOTTED_LEFT_FONT_RED
		WritableCellFormat rowDottedLeftFontRed = new WritableCellFormat(generalFontBoldBlack10Red);//font BLACK
		rowDottedLeftFontRed.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedLeftFontRed.setBackground(Colour.getInternalColour(JXL_RED));
		rowDottedLeftFontRed.setAlignment(Alignment.LEFT);
		rowDottedLeftFontRed.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_LEFT_FONT_RED, rowDottedLeftFontRed);
		//ROW_DOTTED_RIGTH_RED
		WritableCellFormat rowDottedRigthRed = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("0.0%"));//font BLACK
		rowDottedRigthRed.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedRigthRed.setBackground(Colour.getInternalColour(JXL_RED));
		rowDottedRigthRed.setAlignment(Alignment.RIGHT);
		rowDottedRigthRed.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_RIGTH_RED, rowDottedRigthRed);
		//ROW_DOTTED_PERCENT_RED
		WritableCellFormat rowDottedPercentRed = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("0.0%"));//font BLACK
		rowDottedPercentRed.setBorder(Border.ALL, BorderLineStyle.DOTTED);
		rowDottedPercentRed.setBackground(Colour.getInternalColour(JXL_RED));
		rowDottedPercentRed.setAlignment(Alignment.RIGHT);
		rowDottedPercentRed.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_DOTTED_PERCENT_RED, rowDottedPercentRed);
		
		//ROW_NOBODER_BOLD_LEFT
		WritableCellFormat rowNoBoderBoldLeft = new WritableCellFormat(generalFontBoldBlack10);//font BLACK
		rowNoBoderBoldLeft.setBorder(Border.ALL, BorderLineStyle.NONE);
		rowNoBoderBoldLeft.setAlignment(Alignment.LEFT);
		rowNoBoderBoldLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_NOBODER_BOLD_LEFT, rowNoBoderBoldLeft);
		
		//ROW_NOBODER_BOLD_RIGHT
		WritableCellFormat rowNoBoderRight = new WritableCellFormat(generalFontBoldBlack10, new NumberFormat("#,##0"));//font BLACK
		rowNoBoderRight.setBorder(Border.ALL, BorderLineStyle.NONE);
		rowNoBoderRight.setAlignment(Alignment.RIGHT);
		rowNoBoderRight.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_NOBODER_BOLD_RIGHT, rowNoBoderRight);
		
		//ROW_NOBODER_LEFT
		WritableCellFormat rowNoBoderLeft = new WritableCellFormat(generalFontBlack10);//font BLACK
		rowNoBoderLeft.setBorder(Border.ALL, BorderLineStyle.NONE);
		rowNoBoderLeft.setAlignment(Alignment.LEFT);
		rowNoBoderLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
		styles.put(ROW_NOBODER_LEFT, rowNoBoderLeft);
		
	    /**End: Font*/
		return styles;
	}
	
}

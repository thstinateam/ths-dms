/**
 * 
 */
package ths.dms.web.utils.report.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import jxl.biff.DisplayFormat;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.NumberFormat;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder.BorderSide;

/**
 * The Class ExcelProcessUtils.
 *
 * @author hungtx
 * @since Aug 19, 2013
 */
public class ExcelProcessUtilsEx {
//	public final static String STYLE_TITLE = "title";
//	public final static String STYLE_PERCENT = "percent";
//	public final static String STYLE_COEFF = "coeff";
//	public final static String STYLE_CURRENCY = "currency";
//	public final static String STYLE_DATE = "date";	
//	public final static String STYLE_ACCOUNTING = "accounting";	
//	public final static String STYLE_HEADER = "header";
//	public final static String STYLE_TEXT = "text";
//	public final static String STYLE_TEXT_ALIGN_LEFT = "text_left";
//	public final static String STYLE_TEXT_ALIGN_CENTER = "text_center";
//	public final static String STYLE_TEXT_ALIGN_RIGHT = "text_right";	
//	public final static String STYLE_HEADER_ORANGE_NO_BORDER = "header_orange_no_border";
//	public final static String STYLE_HEADER_ORANGE_NO_BORDER_HO_2_3 = "header_orange_no_border_ho_2_3";
//	public final static String STYLE_HEADER_ORANGE = "header_orange";
//	public final static String STYLE_HEADER_ORANGE_HO_2_3 = "header_orange_2_3";
	
	//writableworkbook jxl
	public final static String HEADER = "header";
	public final static String HEADER18HEIGHT = "header";
	public final static String HEADER_LEFT = "header_left";
	public final static String HEADER_BLACK = "header_black";
	public final static String MENU = "menu";
	public final static String MENUEX = "menuex";
	public final static String MENU_GRAY = "menu_gray";
	public final static String MENU_RED = "menu_red";
	public final static String BOLD = "bold";
	public final static String BOLD_ITALIC = "bold_italic";
	public final static String BOLD_CENTER = "bold_center";
	public final static String NORMAL_CENTER = "normal_center";
	public final static String NORMAL = "normal";
	public final static String DETAIL_NORMAL = "detail_normal";
	public final static String NORMAL_MEDIUM_TOP_BORDER = "detail_medium_top_border";
	public final static String NORMAL_MEDIUM_LEFT_BORDER = "detail_medium_left_border";
	public final static String NORMAL_MEDIUM_RIGHT_BORDER = "detail_medium_right_border";
	public final static String NORMAL_MEDIUM_BOTTOM_BORDER = "detail_medium_bottom_border";
	
	public final static String DETAIL_NORMAL_DOTTED_CENTER = "detail_normal_dotted_center";
	public final static String DETAIL_NORMAL_DOTTED_CENTER_BOLD = "detail_normal_dotted_center_bold";

	public final static String DETAIL_NORMAL_DOTTED_CENTER_EX = "detail_normal_dotted_center_ex";
	public final static String DETAIL_NORMAL_DOTTED_CENTER_RED = "detail_normal_dotted_center_red";
	public final static String DETAIL_NORMAL_DOTTED_LEFT = "detail_normal_dotted_left";
	public final static String DETAIL_NORMAL_DOTTED_RIGHT = "detail_normal_dotted_right";
	public final static String DETAIL_NORMAL_DOTTED_RIGHT_EX = "detail_normal_dotted_right_ex";
	public final static String DETAIL_NORMAL_CENTER = "detail_normal_center";
	public final static String DETAIL_NORMAL_LEFT = "detail_normal_left";
	public final static String DETAIL_NORMAL_RIGHT = "detail_normal_right";
	public final static String DETAIL_ORANGE_ACCENT6_DOTTED_LEFT = "detail_orange_accent6_dotted_left";
	public final static String DETAIL_ORANGE_LIGHT80_DOTTED_LEFT = "detail_orange_light80_dotted_left";
	public final static String DETAIL_ORANGE_LIGHT60_DOTTED_LEFT = "detail_orange_light60_dotted_left";
	public final static String DETAIL_ORANGE_LIGHT40_DOTTED_LEFT = "detail_orange_light40_dotted_left";
	public final static String DETAIL_ORANGE_DARK25_DOTTED_LEFT = "detail_orange_dark25_dotted_left";
	public final static String DETAIL_ORANGE_DARK50_DOTTED_LEFT = "detail_orange_dark50_dotted_left";
	public final static String DETAIL_ORANGE_ACCENT6_DOTTED_RIGHT = "detail_orange_accent6_dotted_right";
	public final static String DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT = "detail_orange_light80_dotted_right";
	public final static String DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT = "detail_orange_light60_dotted_right";
	public final static String DETAIL_ORANGE_LIGHT40_DOTTED_RIGHT = "detail_orange_light40_dotted_right";
	public final static String DETAIL_ORANGE_DARK25_DOTTED_RIGHT = "detail_orange_dark25_dotted_right";
	public final static String DETAIL_ORANGE_DARK50_DOTTED_RIGHT = "detail_orange_dark50_dotted_right";
	public final static String DETAIL_ORANGE_ACCENT6_DOTTED_CENTER = "detail_orange_accent6_dotted_center";
	public final static String DETAIL_ORANGE_LIGHT80_DOTTED_CENTER = "detail_orange_light80_dotted_center";
	public final static String DETAIL_ORANGE_LIGHT60_DOTTED_CENTER = "detail_orange_light60_dotted_center";
	public final static String DETAIL_ORANGE_LIGHT40_DOTTED_CENTER = "detail_orange_light40_dotted_center";
	public final static String DETAIL_ORANGE_DARK25_DOTTED_CENTER = "detail_orange_dark25_dotted_center";
	public final static String DETAIL_ORANGE_DARK50_DOTTED_CENTER = "detail_orange_dark50_dotted_center";
	
	public final static String DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_LEFT = "detail_normal_orange_accent6_dotted_left";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_LEFT = "detail_normal_orange_light80_dotted_left";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_LEFT = "detail_normal_orange_light60_dotted_left";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_LEFT = "detail_normal_orange_light40_dotted_left";
	public final static String DETAIL_NORMAL_ORANGE_DARK25_DOTTED_LEFT = "detail_normal_orange_dark25_dotted_left";
	public final static String DETAIL_NORMAL_ORANGE_DARK50_DOTTED_LEFT = "detail_normal_orange_dark50_dotted_left";
	
	public final static String DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_CENTER = "detail_normal_orange_accent6_dotted_center";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_CENTER = "detail_normal_orange_light80_dotted_center";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_CENTER = "detail_normal_orange_light60_dotted_center";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_CENTER = "detail_normal_orange_light40_dotted_center";
	public final static String DETAIL_NORMAL_ORANGE_DARK25_DOTTED_CENTER = "detail_normal_orange_dark25_dotted_center";
	public final static String DETAIL_NORMAL_ORANGE_DARK50_DOTTED_CENTER = "detail_normal_orange_dark50_dotted_center";
	
	public final static String DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_RIGHT = "detail_normal_orange_accent6_dotted_right";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_RIGHT = "detail_normal_orange_light80_dotted_right";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_RIGHT = "detail_normal_orange_light60_dotted_right";
	public final static String DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_RIGHT = "detail_normal_orange_light40_dotted_right";
	public final static String DETAIL_NORMAL_ORANGE_DARK25_DOTTED_RIGHT = "detail_normal_orange_dark25_dotted_right";
	public final static String DETAIL_NORMAL_ORANGE_DARK50_DOTTED_RIGHT = "detail_normal_orange_dark50_dotted_right";
	
	//detail with red color font
	public final static String DETAIL_ORANGE_LIGHT80_DOTTED_LEFT_RED = "detail_orange_light80_dotted_left_red";
	public final static String DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT_RED = "detail_orange_light80_dotted_right_red";
	public final static String DETAIL_ORANGE_LIGHT80_DOTTED_CENTER_RED = "detail_orange_light80_dotted_center_red";
	public final static String DETAIL_ORANGE_LIGHT60_DOTTED_LEFT_RED = "detail_orange_light60_dotted_left_red";
	public final static String DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT_RED = "detail_orange_light60_dotted_right_red";
	public final static String DETAIL_ORANGE_LIGHT60_DOTTED_CENTER_RED = "detail_orange_light60_dotted_center_red";
	
	
	//JXL color, index must be in 8-64
	public final static int JXL_BLUE = 8;
	public final static int JXL_WHITE = 9;
	public final static int JXL_BLACK = 10;
	public final static int JXL_GRAY = 11;
	public final static int JXL_RED = 12;
	
	public final static int JXL_ORANGE_ACCENT6 = 19;
	public final static int JXL_ORANGE_LIGHT80 = 20;
	public final static int JXL_ORANGE_LIGHT60 = 21;
	public final static int JXL_ORANGE_LIGHT40 = 22;
	public final static int JXL_ORANGE_DARK25 = 23;
	public final static int JXL_ORANGE_DARK50 = 24;
	public final static int JXL_ORANGE_DARK51 = 25;
	
	//XSSF color
	public final static XSSFColor poiBlue = new XSSFColor(new java.awt.Color(83, 142, 213));
	/*public final static XSSFColor poiWhite = new XSSFColor(new java.awt.Color(255, 255, 255));
	public final static XSSFColor poiBlack = new XSSFColor(new java.awt.Color(0, 0, 0));*/
	public final static XSSFColor poiWhite = new XSSFColor(java.awt.Color.WHITE);//Mau trang
	public final static XSSFColor poiBlack = new XSSFColor(java.awt.Color.BLACK);//Mau den
	public final static XSSFColor poiGreen = new XSSFColor(new java.awt.Color(0, 176, 80));
	public final static XSSFColor poiOrange_Accent6 = new XSSFColor(new java.awt.Color(247, 150,70));
	public final static XSSFColor poiOrange_light80 = new XSSFColor(new java.awt.Color(253, 233, 217));
	public final static XSSFColor poiOrange_light60 = new XSSFColor(new java.awt.Color(252, 213, 180));
	public final static XSSFColor poiOrange_light40 = new XSSFColor(new java.awt.Color(250, 192, 144));
	public final static XSSFColor poiOrange_dark25 = new XSSFColor(new java.awt.Color(228, 109, 10));
	public final static XSSFColor poiOrange_dark50 = new XSSFColor(new java.awt.Color(151, 72, 7));
	public final static XSSFColor poiGray = new XSSFColor(new java.awt.Color(216, 216, 216));
	public final static XSSFColor poiRed = new XSSFColor(new java.awt.Color(255, 0, 0));
	
	//XSSF Font
	public static XSSFFont headerFont;
	public static XSSFFont header18HeightFont;
	public static XSSFFont detailFont;
	public static XSSFFont detailRedFont;
	public static XSSFFont menuFont;
	public static XSSFFont menuFontGray;
	public static XSSFFont boldFont;
	public static XSSFFont normalFont;
	public static XSSFFont detailNormalFont;
	public static XSSFFont detailNormalRedFont ;
	public static XSSFFont detailBoldFont;
	public static XSSFFont detailBoldRedFont;
	
	/**
	 * Substitute.
	 *
	 * @param zipfile the zipfile
	 * @param tmpfile the tmpfile
	 * @param entry the entry
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	public static void substitute(File zipfile, File tmpfile, String entry,
			OutputStream out) throws IOException {
		ZipFile zip = new ZipFile(zipfile);

		ZipOutputStream zos = new ZipOutputStream(out);

		@SuppressWarnings("unchecked")
		Enumeration<ZipEntry> en = (Enumeration<ZipEntry>) zip.entries();
		while (en.hasMoreElements()) {
			ZipEntry ze = en.nextElement();
			if (!ze.getName().equals(entry)) {
				zos.putNextEntry(new ZipEntry(ze.getName()));
				InputStream is = zip.getInputStream(ze);
				copyStream(is, zos);
				is.close();
			}
		}
		zos.putNextEntry(new ZipEntry(entry));
		InputStream is = new FileInputStream(tmpfile);
		copyStream(is, zos);
		is.close();

		zos.close();
		zip.close();
	}
	
	/**
	 * Copy stream.
	 *
	 * @param in the in
	 * @param out the out
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @author hungtx
	 * @since Aug 19, 2013
	 */
	private static void copyStream(InputStream in, OutputStream out)
			throws IOException {
		byte[] chunk = new byte[1024];
		int count;
		while ((count = in.read(chunk)) >= 0) {
			out.write(chunk, 0, count);
		}
	}	
	
	
	
	public static Map<String, WritableCellFormat> createStylesWritableWorkbook(WritableWorkbook wb) throws WriteException {
		Map<String, WritableCellFormat> styles = new HashMap<String, WritableCellFormat>();
//		XSSFDataFormat fmt = wb.createDataFormat();
		wb.setColourRGB(Colour.getInternalColour(JXL_BLUE), 83, 142, 213);
		wb.setColourRGB(Colour.getInternalColour(JXL_WHITE), 255, 255, 255);
		wb.setColourRGB(Colour.getInternalColour(JXL_BLACK), 0, 0, 0);
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_LIGHT80), 253,	233, 217);//01
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_LIGHT60), 252, 213, 180);//02
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_LIGHT40), 250, 191, 143);//03
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_ACCENT6), 247, 150,	40); //04
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_DARK25),  226, 107, 10);  //05
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_DARK50), 151, 71, 6); //06
		wb.setColourRGB(Colour.getInternalColour(JXL_GRAY), 216, 216, 216);
		wb.setColourRGB(Colour.getInternalColour(JXL_RED), 255, 0, 0);
		wb.setColourRGB(Colour.getInternalColour(JXL_ORANGE_DARK51), 216, 216, 216);
		
		WritableFont headerFont = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD);
		headerFont.setColour(Colour.getInternalColour(JXL_BLUE));
	    WritableCellFormat headerCellFormat = new WritableCellFormat(headerFont);
	    headerCellFormat.setAlignment(Alignment.CENTRE);
	    headerCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
	    styles.put(HEADER, headerCellFormat);
	    
	    WritableFont headerFontEx = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD);
		headerFontEx.setColour(Colour.getInternalColour(JXL_BLACK));
	    WritableCellFormat headerCellFormatEx = new WritableCellFormat(headerFontEx);
	    headerCellFormatEx.setAlignment(Alignment.CENTRE);
	    headerCellFormatEx.setVerticalAlignment(VerticalAlignment.CENTRE);
	    styles.put(HEADER_BLACK, headerCellFormatEx);
	    
	    WritableFont headerFontLeft = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD);
	    headerFontLeft.setColour(Colour.getInternalColour(JXL_BLUE));
	    WritableCellFormat headerCellFormatLeft = new WritableCellFormat(headerFontLeft);
	    headerCellFormatLeft.setAlignment(Alignment.LEFT);
	    headerCellFormatLeft.setVerticalAlignment(VerticalAlignment.CENTRE);
	    styles.put(HEADER_LEFT, headerCellFormatLeft);
	    
	    WritableFont menuFont = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);
	    WritableCellFormat menuCellFormat = new WritableCellFormat(menuFont);
	    menuFont.setColour(Colour.getInternalColour(JXL_WHITE));
	    menuCellFormat.setAlignment(Alignment.CENTRE);
	    menuCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
	    menuCellFormat.setWrap(true);
	    menuCellFormat.setBorder(Border.ALL, BorderLineStyle.MEDIUM,Colour.getInternalColour(JXL_BLACK));
	    menuCellFormat.setBackground(Colour.getInternalColour(JXL_BLUE));
	    styles.put(MENU, menuCellFormat);
	    
	    WritableCellFormat menuCellFormatEx = new WritableCellFormat(menuFont);
	    WritableFont menuFontBlack = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);
	    menuFontBlack.setColour(Colour.getInternalColour(JXL_BLACK));
	    menuCellFormatEx.setAlignment(Alignment.CENTRE);
	    menuCellFormatEx.setVerticalAlignment(VerticalAlignment.CENTRE);
	    menuCellFormatEx.setWrap(true);
	    menuCellFormatEx.setBorder(Border.ALL, BorderLineStyle.MEDIUM,Colour.getInternalColour(JXL_BLACK));
	    menuCellFormatEx.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK51));
	    styles.put(MENUEX, menuCellFormatEx);
	    
	    WritableFont menuFontGray = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);
	    WritableCellFormat menuGrayCellFormat = new WritableCellFormat(menuFontGray);
	    menuFontGray.setColour(Colour.getInternalColour(JXL_GRAY));
	    menuGrayCellFormat.setAlignment(Alignment.CENTRE);
	    menuGrayCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
	    menuGrayCellFormat.setWrap(true);
	    menuGrayCellFormat.setBorder(Border.ALL, BorderLineStyle.MEDIUM,Colour.getInternalColour(JXL_BLACK));
	    menuGrayCellFormat.setBackground(Colour.getInternalColour(JXL_GRAY));
	    styles.put(MENU_GRAY, menuGrayCellFormat);
	    
	    
	    WritableFont boldFont = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);
	    WritableCellFormat boldNoBorderCellFormat = new WritableCellFormat(boldFont);
	    styles.put(BOLD, boldNoBorderCellFormat);
	    
	    WritableCellFormat boldCenterNoBorderCellFormat = new WritableCellFormat(boldFont);
	    boldCenterNoBorderCellFormat.setAlignment(Alignment.CENTRE);
	    styles.put(BOLD_CENTER, boldCenterNoBorderCellFormat);
	    
	   
	    
	    WritableFont normalFont = new WritableFont(WritableFont.ARIAL, 9, WritableFont.NO_BOLD);
	    WritableCellFormat normalCellFormat = new WritableCellFormat(normalFont);
	    styles.put(NORMAL, normalCellFormat);
	    
	    WritableCellFormat normalCenterNoBorderCellFormat = new WritableCellFormat(normalFont);
	    normalCenterNoBorderCellFormat.setAlignment(Alignment.CENTRE);
	    styles.put(NORMAL_CENTER, normalCenterNoBorderCellFormat);
	    
	    WritableFont detailNormalFont = new WritableFont(WritableFont.ARIAL, 9, WritableFont.NO_BOLD);
	    WritableFont detailNormalFontEX = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);
	    WritableCellFormat detailNormalCellFormat = new WritableCellFormat(detailNormalFont);
	    detailNormalCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN,Colour.getInternalColour(JXL_BLACK));
	    styles.put(DETAIL_NORMAL, detailNormalCellFormat);
	    
	    
	    WritableFont detailNormalFontEx = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);
	    WritableCellFormat detailNormalDottedCenterCellFormat = new WritableCellFormat(detailNormalFont);
	    detailNormalDottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailNormalDottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    styles.put(DETAIL_NORMAL_DOTTED_CENTER, detailNormalDottedCenterCellFormat);
	    
	   

	    WritableCellFormat detailNormalDottedCenterCellFormatBold = new WritableCellFormat(detailNormalFontEX);
	    detailNormalDottedCenterCellFormatBold.setBorder(Border.ALL, BorderLineStyle.THIN,Colour.getInternalColour(JXL_BLACK));
	    detailNormalDottedCenterCellFormatBold.setAlignment(Alignment.CENTRE);
	    styles.put(DETAIL_NORMAL_DOTTED_CENTER_BOLD, detailNormalDottedCenterCellFormatBold);
	    
	    WritableCellFormat detailNormalDottedCenterCellFormatEx = new WritableCellFormat(detailNormalFont);
	    detailNormalDottedCenterCellFormatEx.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailNormalDottedCenterCellFormatEx.setAlignment(Alignment.CENTRE);
	    styles.put(DETAIL_NORMAL_DOTTED_CENTER_EX, detailNormalDottedCenterCellFormatEx);
	    
	    DisplayFormat stringFormat = NumberFormats.TEXT; 
	    WritableCellFormat detailNormalDottedLeftCellFormat = new WritableCellFormat(stringFormat);
	    detailNormalDottedLeftCellFormat.setFont(detailNormalFont);
	    detailNormalDottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailNormalDottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    styles.put(DETAIL_NORMAL_DOTTED_LEFT, detailNormalDottedLeftCellFormat);
	    
	    NumberFormat currencyFormat = new NumberFormat("###,##0"); 
	    WritableCellFormat detailNormalDottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailNormalDottedRightCellFormat.setFont(detailNormalFont);
	    detailNormalDottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailNormalDottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    styles.put(DETAIL_NORMAL_DOTTED_RIGHT, detailNormalDottedRightCellFormat);
	    
	    WritableCellFormat detailNormalDottedRightCellFormatEx = new WritableCellFormat(currencyFormat);
	    detailNormalDottedRightCellFormatEx.setFont(detailNormalFontEX);
	    detailNormalDottedRightCellFormatEx.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailNormalDottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    styles.put(DETAIL_NORMAL_DOTTED_RIGHT_EX, detailNormalDottedRightCellFormatEx);
	    
	    WritableCellFormat detailNormalCenterCellFormat = new WritableCellFormat(detailNormalFont);
	    detailNormalCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN,Colour.getInternalColour(JXL_BLACK));
	    detailNormalCenterCellFormat.setAlignment(Alignment.CENTRE);
	    styles.put(DETAIL_NORMAL_CENTER, detailNormalCenterCellFormat);
	    
	    WritableCellFormat detailNormalLeftCellFormat = new WritableCellFormat(detailNormalFont);
	    detailNormalLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN,Colour.getInternalColour(JXL_BLACK));
	    detailNormalLeftCellFormat.setAlignment(Alignment.LEFT);
	    styles.put(DETAIL_NORMAL_LEFT, detailNormalLeftCellFormat);
	    
	    WritableCellFormat detailNormalRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailNormalRightCellFormat.setFont(detailNormalFont);
	    detailNormalRightCellFormat.setBorder(Border.ALL, BorderLineStyle.THIN,Colour.getInternalColour(JXL_BLACK));
	    detailNormalRightCellFormat.setAlignment(Alignment.RIGHT);
	    styles.put(DETAIL_NORMAL_RIGHT, detailNormalRightCellFormat);
	    

	    WritableFont detailBoldFont = new WritableFont(WritableFont.ARIAL, 9, WritableFont.BOLD);
	    WritableCellFormat detailOrangeAccent6DottedLeftCellFormat = new WritableCellFormat(detailBoldFont);
	    detailOrangeAccent6DottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeAccent6DottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    detailOrangeAccent6DottedLeftCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_ACCENT6));
	    styles.put(DETAIL_ORANGE_ACCENT6_DOTTED_LEFT, detailOrangeAccent6DottedLeftCellFormat);
	    
	    WritableCellFormat detailOrangeLight80DottedLeftCellFormat = new WritableCellFormat(detailBoldFont);
	    detailOrangeLight80DottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight80DottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    detailOrangeLight80DottedLeftCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT80));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_LEFT, detailOrangeLight80DottedLeftCellFormat);
	    
	    
	    WritableFont detailBoldFontRed = new WritableFont(WritableFont.ARIAL);
	    detailBoldFontRed.setColour(Colour.getInternalColour(JXL_RED));
	    detailBoldFontRed.setPointSize(9);
	    detailBoldFontRed.setBoldStyle(WritableFont.BOLD);
	    WritableCellFormat detailOrangeLight80DottedLeftCellFormatRed = new WritableCellFormat(detailBoldFontRed);
	    detailOrangeLight80DottedLeftCellFormatRed.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight80DottedLeftCellFormatRed.setAlignment(Alignment.LEFT);
	    detailOrangeLight80DottedLeftCellFormatRed.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT80));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_LEFT_RED, detailOrangeLight80DottedLeftCellFormatRed);
	    
	    WritableCellFormat detailOrangeLight60DottedLeftCellFormat = new WritableCellFormat(detailBoldFont);
	    detailOrangeLight60DottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight60DottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    detailOrangeLight60DottedLeftCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT60));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_LEFT, detailOrangeLight60DottedLeftCellFormat);
	    
	    WritableCellFormat detailOrangeLight40DottedLeftCellFormat = new WritableCellFormat(detailBoldFont);
	    detailOrangeLight40DottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight40DottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    detailOrangeLight40DottedLeftCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT40));
	    styles.put(DETAIL_ORANGE_LIGHT40_DOTTED_LEFT, detailOrangeLight40DottedLeftCellFormat);
	    
	    WritableCellFormat detailOrangeDark25DottedLeftCellFormat = new WritableCellFormat(detailBoldFont);
	    detailOrangeDark25DottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeDark25DottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    detailOrangeDark25DottedLeftCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK25));
	    styles.put(DETAIL_ORANGE_DARK25_DOTTED_LEFT, detailOrangeDark25DottedLeftCellFormat);
	    
	    WritableCellFormat detailOrangeDark50DottedLeftCellFormat = new WritableCellFormat(detailBoldFont);
	    detailOrangeDark50DottedLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeDark50DottedLeftCellFormat.setAlignment(Alignment.LEFT);
	    detailOrangeDark50DottedLeftCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK50));
	    styles.put(DETAIL_ORANGE_DARK50_DOTTED_LEFT, detailOrangeDark50DottedLeftCellFormat);
	    
	    WritableCellFormat detailOrangeAccent6DottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeAccent6DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeAccent6DottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeAccent6DottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    detailOrangeAccent6DottedRightCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_ACCENT6));
	    styles.put(DETAIL_ORANGE_ACCENT6_DOTTED_RIGHT, detailOrangeAccent6DottedRightCellFormat);
	    
	    WritableCellFormat detailOrangeLight80DottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeLight80DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeLight80DottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight80DottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    detailOrangeLight80DottedRightCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT80));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT, detailOrangeLight80DottedRightCellFormat);
	    
	    WritableCellFormat detailOrangeLight80DottedRightCellFormatRed = new WritableCellFormat(currencyFormat);
	    detailOrangeLight80DottedRightCellFormatRed.setFont(detailBoldFontRed);
	    detailOrangeLight80DottedRightCellFormatRed.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight80DottedRightCellFormatRed.setAlignment(Alignment.RIGHT);
	    detailOrangeLight80DottedRightCellFormatRed.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT80));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT_RED, detailOrangeLight80DottedRightCellFormatRed);
	    
	    WritableCellFormat detailOrangeLight60DottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeLight60DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeLight60DottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight60DottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    detailOrangeLight60DottedRightCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT60));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT, detailOrangeLight60DottedRightCellFormat);
	    
	    WritableCellFormat detailOrangeLight40DottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeLight40DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeLight40DottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight40DottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    detailOrangeLight40DottedRightCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT40));
	    styles.put(DETAIL_ORANGE_LIGHT40_DOTTED_RIGHT, detailOrangeLight40DottedRightCellFormat);
	    
	    WritableCellFormat detailOrangeDark25DottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeDark25DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeDark25DottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeDark25DottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    detailOrangeDark25DottedRightCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK25));
	    styles.put(DETAIL_ORANGE_DARK25_DOTTED_RIGHT, detailOrangeDark25DottedRightCellFormat);
	    
	    WritableCellFormat detailOrangeDark50DottedRightCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeDark50DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeDark50DottedRightCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeDark50DottedRightCellFormat.setAlignment(Alignment.RIGHT);
	    detailOrangeDark50DottedRightCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK50));
	    styles.put(DETAIL_ORANGE_DARK50_DOTTED_RIGHT, detailOrangeDark50DottedRightCellFormat);
	    
	    WritableCellFormat detailOrangeAccent6DottedCenterCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeAccent6DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeAccent6DottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeAccent6DottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    detailOrangeAccent6DottedCenterCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_ACCENT6));
	    styles.put(DETAIL_ORANGE_ACCENT6_DOTTED_CENTER, detailOrangeAccent6DottedCenterCellFormat);
	    
	    WritableCellFormat detailOrangeLight80DottedCenterCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeLight80DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeLight80DottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight80DottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    detailOrangeLight80DottedCenterCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT80));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_CENTER, detailOrangeLight80DottedCenterCellFormat);
	    
	    WritableCellFormat detailOrangeLight80DottedCenterCellFormatRed = new WritableCellFormat(currencyFormat);
	    detailOrangeLight80DottedCenterCellFormatRed.setFont(detailBoldFontRed);
	    detailOrangeLight80DottedCenterCellFormatRed.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight80DottedCenterCellFormatRed.setAlignment(Alignment.CENTRE);
	    detailOrangeLight80DottedCenterCellFormatRed.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT80));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_CENTER_RED, detailOrangeLight80DottedCenterCellFormatRed);
	    
	    WritableCellFormat detailOrangeLight60DottedCenterCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeLight60DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeLight60DottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight60DottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    detailOrangeLight60DottedCenterCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT60));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_CENTER, detailOrangeLight60DottedCenterCellFormat);
	    
	    WritableCellFormat detailOrangeLight40DottedCenterCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeLight40DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeLight40DottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeLight40DottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    detailOrangeLight40DottedCenterCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_LIGHT40));
	    styles.put(DETAIL_ORANGE_LIGHT40_DOTTED_CENTER, detailOrangeLight40DottedCenterCellFormat);
	    
	    WritableCellFormat detailOrangeDark25DottedCenterCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeDark25DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeDark25DottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeDark25DottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    detailOrangeDark25DottedCenterCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK25));
	    styles.put(DETAIL_ORANGE_DARK25_DOTTED_CENTER, detailOrangeDark25DottedCenterCellFormat);
	    
	    WritableCellFormat detailOrangeDark50DottedCenterCellFormat = new WritableCellFormat(currencyFormat);
	    detailOrangeDark50DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeDark50DottedCenterCellFormat.setBorder(Border.ALL, BorderLineStyle.HAIR,Colour.getInternalColour(JXL_BLACK));
	    detailOrangeDark50DottedCenterCellFormat.setAlignment(Alignment.CENTRE);
	    detailOrangeDark50DottedCenterCellFormat.setBackground(Colour.getInternalColour(JXL_ORANGE_DARK50));
	    styles.put(DETAIL_ORANGE_DARK50_DOTTED_CENTER, detailOrangeDark50DottedCenterCellFormat);
		return styles;
	}
	
	
	
	
	
	public static Map<String, XSSFCellStyle> createStylesPOI(Workbook wb) throws WriteException {
		Map<String, XSSFCellStyle> styles = new HashMap<String, XSSFCellStyle>();
		XSSFDataFormat fmt = (XSSFDataFormat) wb.createDataFormat();
		
		//Init Font
	    headerFont = (XSSFFont) wb.createFont();
	    detailFont = (XSSFFont) wb.createFont();
	    detailRedFont = (XSSFFont) wb.createFont();
	    menuFont = (XSSFFont) wb.createFont();
	    menuFontGray = (XSSFFont) wb.createFont();
	    boldFont = (XSSFFont) wb.createFont();
	    normalFont = (XSSFFont) wb.createFont();
	    detailNormalFont = (XSSFFont) wb.createFont();
	    detailNormalRedFont = (XSSFFont) wb.createFont();
	    detailBoldFont = (XSSFFont) wb.createFont();
	    detailBoldRedFont = (XSSFFont) wb.createFont();
	    header18HeightFont = (XSSFFont) wb.createFont();
	    
	    //set font
	    setFontPOI(headerFont,"Arial",20,true,poiBlue);
	    setFontPOI(header18HeightFont,"Arial",18,true,poiBlue);
	    setFontPOI(detailFont,"Arial",9,false,poiBlack);
	    setFontPOI(detailRedFont,"Arial",9,false,poiRed);
	    setFontPOI(menuFont,"Arial",9,true,poiWhite);
	    setFontPOI(menuFontGray,"Arial",9,true,poiBlack);
	    setFontPOI(boldFont,"Arial",9,true,poiBlack);
	    setFontPOI(normalFont,"Arial",9,false,poiBlack);
	    setFontPOI(detailBoldFont,"Arial",9,true,poiBlack);
	    setFontPOI(detailBoldRedFont,"Arial",9,true,poiRed);
	    setFontPOI(detailNormalFont,"Arial",9,false,poiBlack);
	    setFontPOI(detailNormalRedFont,"Arial",9,false,poiRed);
	    
	    
	    //Set Cell Styles
	    XSSFCellStyle headerCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    headerCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    headerCellFormat.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
	    headerCellFormat.setFont(headerFont);
	    styles.put(HEADER, headerCellFormat);
	    
	    XSSFCellStyle headerCellFormatLeft = (XSSFCellStyle) wb.createCellStyle();
	    headerCellFormatLeft.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    headerCellFormatLeft.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
	    headerCellFormatLeft.setFont(headerFont);
	    styles.put(HEADER_LEFT, headerCellFormatLeft);
	    
	    XSSFCellStyle header18HeightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    header18HeightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    header18HeightCellFormat.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
	    header18HeightCellFormat.setFont(header18HeightFont);
	    styles.put(HEADER18HEIGHT, header18HeightCellFormat);
	    
	    XSSFFont headerFontBlack = (XSSFFont)wb.createFont();
	    setFontPOI(headerFontBlack, "Arial", 20, true, poiBlack);
	    XSSFCellStyle headerCellFormatBlack = (XSSFCellStyle)wb.createCellStyle();
	    headerCellFormatBlack.setFont(headerFontBlack);
	    headerCellFormatBlack.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	    headerCellFormatBlack.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
	    styles.put(HEADER_BLACK, headerCellFormatBlack);
	    
	    XSSFCellStyle menuCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    menuCellFormat.setFillForegroundColor(poiBlue);
	    menuCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    //menuCellFormat.setWrapText(true);
	    menuCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    menuCellFormat.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
	    setBorderForCell(menuCellFormat,BorderStyle.MEDIUM,poiBlack);
	    menuCellFormat.setFont(menuFont);
	    styles.put(MENU, menuCellFormat);
	    
	    XSSFCellStyle menuGrayCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    menuGrayCellFormat.setFillForegroundColor(poiGray);
	    menuGrayCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    //menuGrayCellFormat.setWrapText(true);
	    menuGrayCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    menuGrayCellFormat.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
	    setBorderForCell(menuGrayCellFormat,BorderStyle.THIN,poiBlack);
	    menuGrayCellFormat.setFont(menuFontGray);
	    styles.put(MENU_GRAY, menuGrayCellFormat);
	    
	    XSSFCellStyle menuRedCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    menuRedCellFormat.setFillForegroundColor(poiRed);
	    menuRedCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    //menuRedCellFormat.setWrapText(true);
	    menuRedCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    menuRedCellFormat.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
	    setBorderForCell(menuRedCellFormat,BorderStyle.MEDIUM,poiBlack);
	    menuRedCellFormat.setFont(menuFont);
	    styles.put(MENU_RED, menuRedCellFormat);
	    
	    XSSFCellStyle boldNoBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    boldNoBorderCellFormat.setFont(boldFont);
	    styles.put(BOLD, boldNoBorderCellFormat);
	    
	    XSSFCellStyle boldCenterNoBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    boldCenterNoBorderCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    boldCenterNoBorderCellFormat.setFont(boldFont);
	    styles.put(BOLD_CENTER, boldCenterNoBorderCellFormat);
	    
	    XSSFCellStyle normalCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    normalCellFormat.setFont(normalFont);
	    styles.put(NORMAL, normalCellFormat);
	    
	    XSSFCellStyle normalCenterNoBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    normalCenterNoBorderCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    normalCenterNoBorderCellFormat.setFont(normalFont);
	    styles.put(NORMAL_CENTER, normalCenterNoBorderCellFormat);
	    
	    XSSFCellStyle normalMediumTopBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    normalMediumTopBorderCellFormat.setFont(normalFont);
	    normalMediumTopBorderCellFormat.setBorderTop(BorderStyle.MEDIUM);
	    normalMediumTopBorderCellFormat.setBorderColor(BorderSide.TOP, poiBlack);
	    styles.put(NORMAL_MEDIUM_TOP_BORDER, normalMediumTopBorderCellFormat);
	    
	    XSSFCellStyle normalMediumLeftBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    normalMediumLeftBorderCellFormat.setFont(normalFont);
	    normalMediumLeftBorderCellFormat.setBorderTop(BorderStyle.MEDIUM);
	    normalMediumLeftBorderCellFormat.setBorderColor(BorderSide.LEFT, poiBlack);
	    styles.put(NORMAL_MEDIUM_LEFT_BORDER, normalMediumLeftBorderCellFormat);
	    
	    XSSFCellStyle normalMediumRightBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    normalMediumRightBorderCellFormat.setFont(normalFont);
	    normalMediumRightBorderCellFormat.setBorderTop(BorderStyle.MEDIUM);
	    normalMediumRightBorderCellFormat.setBorderColor(BorderSide.RIGHT, poiBlack);
	    styles.put(NORMAL_MEDIUM_RIGHT_BORDER, normalMediumRightBorderCellFormat);
	    
	    XSSFCellStyle normalMediumBottomBorderCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    normalMediumBottomBorderCellFormat.setFont(normalFont);
	    normalMediumBottomBorderCellFormat.setBorderTop(BorderStyle.MEDIUM);
	    normalMediumBottomBorderCellFormat.setBorderColor(BorderSide.BOTTOM, poiBlack);
	    styles.put(NORMAL_MEDIUM_BOTTOM_BORDER, normalMediumRightBorderCellFormat);
	    
	    XSSFCellStyle detailNormalCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalCellFormat, BorderStyle.THIN, poiBlack);
	    detailNormalCellFormat.setFont(detailNormalFont);
	    styles.put(DETAIL_NORMAL, detailNormalCellFormat);
	    
	    XSSFCellStyle detailNormalDottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalDottedCenterCellFormat, BorderStyle.HAIR, poiBlack);
	    detailNormalDottedCenterCellFormat.setFont(detailNormalFont);
	    detailNormalDottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    detailNormalDottedCenterCellFormat.setDataFormat(fmt.getFormat("@"));
	    styles.put(DETAIL_NORMAL_DOTTED_CENTER, detailNormalDottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalDottedCenterBoldCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalDottedCenterBoldCellFormat, BorderStyle.THIN, poiBlack);
	    detailNormalDottedCenterBoldCellFormat.setFont(boldFont);
	    detailNormalDottedCenterBoldCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    detailNormalDottedCenterBoldCellFormat.setDataFormat(fmt.getFormat("@"));
	    styles.put(DETAIL_NORMAL_DOTTED_CENTER_BOLD, detailNormalDottedCenterBoldCellFormat);
	    
	    XSSFCellStyle detailNormalDottedCenterRedCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalDottedCenterRedCellFormat, BorderStyle.HAIR, poiBlack);
	    detailNormalDottedCenterRedCellFormat.setFont(detailNormalRedFont);
	    detailNormalDottedCenterRedCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    detailNormalDottedCenterRedCellFormat.setDataFormat(fmt.getFormat("@"));
	    styles.put(DETAIL_NORMAL_DOTTED_CENTER_RED, detailNormalDottedCenterRedCellFormat);
	    
	    XSSFCellStyle detailNormalDottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalDottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailNormalDottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailNormalDottedLeftCellFormat.setFont(detailNormalFont);
	    detailNormalDottedLeftCellFormat.setDataFormat(fmt.getFormat("@"));
	    styles.put(DETAIL_NORMAL_DOTTED_LEFT, detailNormalDottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalDottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalDottedRightCellFormat, BorderStyle.HAIR, poiBlack);
	    detailNormalDottedRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
//	    detailNormalDottedRightCellFormat.setDataFormat(fmt.getFormat("_-* #,##0.00;-* #,##0.00;_-* \"\"??;_-@_-"));
	    detailNormalDottedRightCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-#,##0;_-* \"\"??;_-@_-"));
	    detailNormalDottedRightCellFormat.setFont(detailNormalFont);
	    styles.put(DETAIL_NORMAL_DOTTED_RIGHT, detailNormalDottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalCenterCellFormat, BorderStyle.THIN, poiBlack);
	    detailNormalCenterCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailNormalCenterCellFormat.setFont(detailNormalFont);
	    detailNormalCenterCellFormat.setDataFormat(fmt.getFormat("@"));
	    styles.put(DETAIL_NORMAL_CENTER, detailNormalCenterCellFormat);
	    
	    XSSFCellStyle detailNormalDottedCenterCellFormatEx = (XSSFCellStyle)wb.createCellStyle();
	    detailNormalDottedCenterCellFormatEx.setFont(detailNormalFont);
	    setBorderForCell(detailNormalDottedCenterCellFormatEx, BorderStyle.THIN, poiBlack);
	    detailNormalDottedCenterCellFormatEx.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	    styles.put(DETAIL_NORMAL_DOTTED_CENTER_EX, detailNormalDottedCenterCellFormatEx);
	    
	    XSSFCellStyle detailNormalDottedRightCellFormatEx = (XSSFCellStyle)wb.createCellStyle();
	    detailNormalDottedRightCellFormatEx.setFont(boldFont);
	    detailNormalDottedRightCellFormatEx.setDataFormat(fmt.getFormat("#,##0"));
	    setBorderForCell(detailNormalDottedRightCellFormatEx, BorderStyle.HAIR, poiBlack);
	    detailNormalDottedRightCellFormatEx.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
	    styles.put(DETAIL_NORMAL_DOTTED_RIGHT_EX, detailNormalDottedRightCellFormatEx);
	    
	    XSSFCellStyle detailNormalLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalLeftCellFormat, BorderStyle.THIN, poiBlack);
	    detailNormalLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailNormalLeftCellFormat.setFont(detailNormalFont);
	    detailNormalLeftCellFormat.setDataFormat(fmt.getFormat("@"));
	    styles.put(DETAIL_NORMAL_LEFT, detailNormalLeftCellFormat);
	    
	    XSSFCellStyle detailNormalRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailNormalRightCellFormat, BorderStyle.THIN, poiBlack);
	    detailNormalRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailNormalRightCellFormat.setFont(detailNormalFont);
//	    detailNormalRightCellFormat.setDataFormat(fmt.getFormat("_-* #,##0;-* #,##0;_-* \"\"??;_-@_-"));
	    detailNormalRightCellFormat.setDataFormat(fmt.getFormat("#,##0;-#,##0;\"\";_-@_-"));
	    styles.put(DETAIL_NORMAL_RIGHT, detailNormalRightCellFormat);
	    
	    XSSFCellStyle detailOrangeAccent6DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeAccent6DottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeAccent6DottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeAccent6DottedLeftCellFormat.setFillForegroundColor(poiOrange_Accent6);
	    detailOrangeAccent6DottedLeftCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeAccent6DottedLeftCellFormat.setDataFormat(fmt.getFormat("@"));
	    detailOrangeAccent6DottedLeftCellFormat.setFont(detailBoldFont);
	    styles.put(DETAIL_ORANGE_ACCENT6_DOTTED_LEFT, detailOrangeAccent6DottedLeftCellFormat);
	    
	    XSSFCellStyle detailOrangeAccent6DottedCenterCellFormat = (XSSFCellStyle)detailOrangeAccent6DottedLeftCellFormat.clone();
	    detailOrangeAccent6DottedCenterCellFormat.setAlignment(XSSFCellStyle.ALIGN_CENTER);
	    styles.put(DETAIL_ORANGE_ACCENT6_DOTTED_CENTER, detailOrangeAccent6DottedCenterCellFormat);
	    
	    XSSFCellStyle detailOrangeAccent6DottedRightCellFormat = (XSSFCellStyle)detailOrangeAccent6DottedLeftCellFormat.clone();
	    detailOrangeAccent6DottedRightCellFormat.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
	    detailOrangeAccent6DottedRightCellFormat.setDataFormat(fmt.getFormat("#,##0"));
	    styles.put(DETAIL_ORANGE_ACCENT6_DOTTED_RIGHT, detailOrangeAccent6DottedRightCellFormat);
	    
	    XSSFCellStyle detailOrangeLight80DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight80DottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight80DottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeLight80DottedLeftCellFormat.setFillForegroundColor(poiOrange_light80);
	    detailOrangeLight80DottedLeftCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight80DottedLeftCellFormat.setDataFormat(fmt.getFormat("@"));
	    detailOrangeLight80DottedLeftCellFormat.setFont(detailBoldFont);
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_LEFT, detailOrangeLight80DottedLeftCellFormat);
	    
	    XSSFCellStyle detailOrangeLight60DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight60DottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight60DottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeLight60DottedLeftCellFormat.setFillForegroundColor(poiOrange_light60);
	    detailOrangeLight60DottedLeftCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight60DottedLeftCellFormat.setDataFormat(fmt.getFormat("@"));
	    detailOrangeLight60DottedLeftCellFormat.setFont(detailBoldFont);
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_LEFT, detailOrangeLight60DottedLeftCellFormat);
	    
	    XSSFCellStyle detailOrangeLight60DottedLeftRedCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight60DottedLeftRedCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight60DottedLeftRedCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeLight60DottedLeftRedCellFormat.setFillForegroundColor(poiOrange_light60);
	    detailOrangeLight60DottedLeftRedCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight60DottedLeftRedCellFormat.setDataFormat(fmt.getFormat("@"));
	    detailOrangeLight60DottedLeftRedCellFormat.setFont(detailBoldRedFont);
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_LEFT_RED, detailOrangeLight60DottedLeftRedCellFormat);
	    
	    XSSFCellStyle detailOrangeLight40DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight40DottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight40DottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeLight40DottedLeftCellFormat.setFillForegroundColor(poiOrange_light40);
	    detailOrangeLight40DottedLeftCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight40DottedLeftCellFormat.setDataFormat(fmt.getFormat("@"));
	    detailOrangeLight40DottedLeftCellFormat.setFont(detailBoldFont);
	    styles.put(DETAIL_ORANGE_LIGHT40_DOTTED_LEFT, detailOrangeLight40DottedLeftCellFormat);
	    
	    XSSFCellStyle detailOrangeDark25DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeDark25DottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeDark25DottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeDark25DottedLeftCellFormat.setFillForegroundColor(poiOrange_dark25);
	    detailOrangeDark25DottedLeftCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeDark25DottedLeftCellFormat.setDataFormat(fmt.getFormat("@"));
	    detailOrangeDark25DottedLeftCellFormat.setFont(detailBoldFont);
	    styles.put(DETAIL_ORANGE_DARK25_DOTTED_LEFT, detailOrangeDark25DottedLeftCellFormat);
	    
	    XSSFCellStyle detailOrangeDark50DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeDark50DottedLeftCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeDark50DottedLeftCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.LEFT);
	    detailOrangeDark50DottedLeftCellFormat.setFillForegroundColor(poiOrange_dark50);
	    detailOrangeDark50DottedLeftCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeDark50DottedLeftCellFormat.setDataFormat(fmt.getFormat("@"));
	    detailOrangeDark50DottedLeftCellFormat.setFont(detailBoldFont);
	    styles.put(DETAIL_ORANGE_DARK50_DOTTED_LEFT, detailOrangeDark50DottedLeftCellFormat);
	    
	    XSSFCellStyle detailOrangeLight80DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight80DottedRightCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight80DottedRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailOrangeLight80DottedRightCellFormat.setFillForegroundColor(poiOrange_light80);
	    detailOrangeLight80DottedRightCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight80DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeLight80DottedRightCellFormat.setDataFormat(fmt.getFormat("#,##0"));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT, detailOrangeLight80DottedRightCellFormat);
	    
	    XSSFCellStyle detailOrangeLight60DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight60DottedRightCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight60DottedRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailOrangeLight60DottedRightCellFormat.setFillForegroundColor(poiOrange_light60);
	    detailOrangeLight60DottedRightCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight60DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeLight60DottedRightCellFormat.setDataFormat(fmt.getFormat("#,##0"));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT, detailOrangeLight60DottedRightCellFormat);
	    
	    XSSFCellStyle detailOrangeLight60DottedRightRedCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight60DottedRightRedCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight60DottedRightRedCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailOrangeLight60DottedRightRedCellFormat.setFillForegroundColor(poiOrange_light60);
	    detailOrangeLight60DottedRightRedCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight60DottedRightRedCellFormat.setFont(detailBoldRedFont);
	    detailOrangeLight60DottedRightRedCellFormat.setDataFormat(fmt.getFormat("#,##0"));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT_RED, detailOrangeLight60DottedRightRedCellFormat);
	    
	    XSSFCellStyle detailOrangeLight40DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight40DottedRightCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight40DottedRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailOrangeLight40DottedRightCellFormat.setFillForegroundColor(poiOrange_light40);
	    detailOrangeLight40DottedRightCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight40DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeLight40DottedRightCellFormat.setDataFormat(fmt.getFormat("#,##0"));
	    styles.put(DETAIL_ORANGE_LIGHT40_DOTTED_RIGHT, detailOrangeLight40DottedRightCellFormat);
	    
	    XSSFCellStyle detailOrangeDark25DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeDark25DottedRightCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeDark25DottedRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailOrangeDark25DottedRightCellFormat.setFillForegroundColor(poiOrange_dark25);
	    detailOrangeDark25DottedRightCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeDark25DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeDark25DottedRightCellFormat.setDataFormat(fmt.getFormat("#,##0"));
	    styles.put(DETAIL_ORANGE_DARK25_DOTTED_RIGHT, detailOrangeDark25DottedRightCellFormat);
	    
	    
	    XSSFCellStyle detailOrangeDark50DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeDark50DottedRightCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeDark50DottedRightCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.RIGHT);
	    detailOrangeDark50DottedRightCellFormat.setFillForegroundColor(poiOrange_dark50);
	    detailOrangeDark50DottedRightCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeDark50DottedRightCellFormat.setFont(detailBoldFont);
	    detailOrangeDark50DottedRightCellFormat.setDataFormat(fmt.getFormat("#,##0"));
	    styles.put(DETAIL_ORANGE_DARK50_DOTTED_RIGHT, detailOrangeDark50DottedRightCellFormat);
	    
	    XSSFCellStyle detailOrangeLight80DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight80DottedCenterCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight80DottedCenterCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailOrangeLight80DottedCenterCellFormat.setFillForegroundColor(poiOrange_light80);
	    detailOrangeLight80DottedCenterCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight80DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeLight80DottedCenterCellFormat.setDataFormat(fmt.getFormat("@"));
	    styles.put(DETAIL_ORANGE_LIGHT80_DOTTED_CENTER, detailOrangeLight80DottedCenterCellFormat);
	    
	    XSSFCellStyle detailOrangeLight60DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight60DottedCenterCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight60DottedCenterCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailOrangeLight60DottedCenterCellFormat.setFillForegroundColor(poiOrange_light60);
	    detailOrangeLight60DottedCenterCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight60DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeLight60DottedCenterCellFormat.setDataFormat(fmt.getFormat("@"));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_CENTER, detailOrangeLight60DottedCenterCellFormat);
	    
	    XSSFCellStyle detailOrangeLight60DottedCenterRedCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight60DottedCenterRedCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight60DottedCenterRedCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailOrangeLight60DottedCenterRedCellFormat.setFillForegroundColor(poiOrange_light60);
	    detailOrangeLight60DottedCenterRedCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight60DottedCenterRedCellFormat.setFont(detailBoldRedFont);
	    detailOrangeLight60DottedCenterRedCellFormat.setDataFormat(fmt.getFormat("@"));
	    styles.put(DETAIL_ORANGE_LIGHT60_DOTTED_CENTER_RED, detailOrangeLight60DottedCenterRedCellFormat);
	    
	    XSSFCellStyle detailOrangeLight40DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeLight40DottedCenterCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeLight40DottedCenterCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailOrangeLight40DottedCenterCellFormat.setFillForegroundColor(poiOrange_light40);
	    detailOrangeLight40DottedCenterCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeLight40DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeLight40DottedCenterCellFormat.setDataFormat(fmt.getFormat("@"));
	    styles.put(DETAIL_ORANGE_LIGHT40_DOTTED_CENTER, detailOrangeLight40DottedCenterCellFormat);
	    
	    XSSFCellStyle detailOrangeDark25DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeDark25DottedCenterCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeDark25DottedCenterCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailOrangeDark25DottedCenterCellFormat.setFillForegroundColor(poiOrange_dark25);
	    detailOrangeDark25DottedCenterCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeDark25DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeDark25DottedCenterCellFormat.setDataFormat(fmt.getFormat("@"));
	    styles.put(DETAIL_ORANGE_DARK25_DOTTED_CENTER, detailOrangeDark25DottedCenterCellFormat);
	    
	    
	    XSSFCellStyle detailOrangeDark50DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    setBorderForCell(detailOrangeDark50DottedCenterCellFormat, BorderStyle.HAIR, poiBlack);
	    detailOrangeDark50DottedCenterCellFormat.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
	    detailOrangeDark50DottedCenterCellFormat.setFillForegroundColor(poiOrange_dark50);
	    detailOrangeDark50DottedCenterCellFormat.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    detailOrangeDark50DottedCenterCellFormat.setFont(detailBoldFont);
	    detailOrangeDark50DottedCenterCellFormat.setDataFormat(fmt.getFormat("@"));
	    styles.put(DETAIL_ORANGE_DARK50_DOTTED_CENTER, detailOrangeDark50DottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeAccent6DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeAccent6DottedLeftCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_ACCENT6_DOTTED_LEFT));
	    detailNormalOrangeAccent6DottedLeftCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_LEFT, detailNormalOrangeAccent6DottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight80DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight80DottedLeftCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT80_DOTTED_LEFT));
	    detailNormalOrangeLight80DottedLeftCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_LEFT, detailNormalOrangeLight80DottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight60DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight60DottedLeftCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT60_DOTTED_LEFT));
	    detailNormalOrangeLight60DottedLeftCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_LEFT, detailNormalOrangeLight60DottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight40DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight40DottedLeftCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT40_DOTTED_LEFT));
	    detailNormalOrangeLight40DottedLeftCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_LEFT, detailNormalOrangeLight40DottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeDark25DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeDark25DottedLeftCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_DARK25_DOTTED_LEFT));
	    detailNormalOrangeDark25DottedLeftCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_DARK25_DOTTED_LEFT, detailNormalOrangeDark25DottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeDark50DottedLeftCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeDark50DottedLeftCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_DARK50_DOTTED_LEFT));
	    detailNormalOrangeDark50DottedLeftCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_DARK50_DOTTED_LEFT, detailNormalOrangeDark50DottedLeftCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeAccent6DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeAccent6DottedRightCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_LEFT));
	    detailNormalOrangeAccent6DottedRightCellFormat.setAlignment(HorizontalAlignment.RIGHT);
	    styles.put(DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_RIGHT, detailNormalOrangeAccent6DottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight80DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight80DottedRightCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT80_DOTTED_RIGHT));
	    detailNormalOrangeLight80DottedRightCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_RIGHT, detailNormalOrangeLight80DottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight60DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight60DottedRightCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT60_DOTTED_RIGHT));
	    detailNormalOrangeLight60DottedRightCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_RIGHT, detailNormalOrangeLight60DottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight40DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight40DottedRightCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_LIGHT40_DOTTED_RIGHT));
	    detailNormalOrangeLight40DottedRightCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_RIGHT, detailNormalOrangeLight40DottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeDark25DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeDark25DottedRightCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_DARK25_DOTTED_RIGHT));
	    detailNormalOrangeDark25DottedRightCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_DARK25_DOTTED_RIGHT, detailNormalOrangeDark25DottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeDark50DottedRightCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeDark50DottedRightCellFormat.cloneStyleFrom(styles.get(DETAIL_ORANGE_DARK50_DOTTED_RIGHT));
	    detailNormalOrangeDark50DottedRightCellFormat.setFont(detailFont);
	    styles.put(DETAIL_NORMAL_ORANGE_DARK50_DOTTED_RIGHT, detailNormalOrangeDark50DottedRightCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeAccent6DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeAccent6DottedCenterCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_LEFT));
	    detailNormalOrangeAccent6DottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_ORANGE_ACCENT6_DOTTED_CENTER, detailNormalOrangeAccent6DottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight80DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight80DottedCenterCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_LEFT));
	    detailNormalOrangeLight80DottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT80_DOTTED_CENTER, detailNormalOrangeLight80DottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight60DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight60DottedCenterCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_LEFT));
	    detailNormalOrangeLight60DottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT60_DOTTED_CENTER, detailNormalOrangeLight60DottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeLight40DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeLight40DottedCenterCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_LEFT));
	    detailNormalOrangeLight40DottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_ORANGE_LIGHT40_DOTTED_CENTER, detailNormalOrangeLight40DottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeDark25DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeDark25DottedCenterCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_DARK25_DOTTED_LEFT));
	    detailNormalOrangeDark25DottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_ORANGE_DARK25_DOTTED_CENTER, detailNormalOrangeDark25DottedCenterCellFormat);
	    
	    XSSFCellStyle detailNormalOrangeDark50DottedCenterCellFormat = (XSSFCellStyle) wb.createCellStyle();
	    detailNormalOrangeDark50DottedCenterCellFormat.cloneStyleFrom(styles.get(DETAIL_NORMAL_ORANGE_DARK50_DOTTED_LEFT));
	    detailNormalOrangeDark50DottedCenterCellFormat.setAlignment(HorizontalAlignment.CENTER);
	    styles.put(DETAIL_NORMAL_ORANGE_DARK50_DOTTED_CENTER, detailNormalOrangeDark50DottedCenterCellFormat);
	    
	    
		return styles;
	}
	
	
	
	//params: border's Color tren, trai, duoi, phai cua cell	
//	public static XSSFCellStyle setBorderForCell(XSSFCellStyle cellStyle, BorderStyle borderStyle,XSSFColor borderColor,XSSFColor... params){
//		BorderStyle b = borderStyle == null?BorderStyle.THIN:borderStyle;
//		cellStyle.setBorderLeft(b);
//		cellStyle.setBorderTop(b);
//		cellStyle.setBorderRight(b);
//		cellStyle.setBorderBottom(b);
//		//set color
//		cellStyle.setBorderColor(BorderSide.TOP, borderColor);
//		cellStyle.setBorderColor(BorderSide.LEFT, borderColor);
//		cellStyle.setBorderColor(BorderSide.BOTTOM, borderColor);
//		cellStyle.setBorderColor(BorderSide.RIGHT, borderColor);
//		if(params.length <= 4){
//			int i = 0;
//			for(XSSFColor sideColor:params){
//				BorderSide bSide = null;
//				switch (i++){
//					case (0): bSide = BorderSide.TOP; break;
//					case (1): bSide = BorderSide.LEFT; break;
//					case (2): bSide = BorderSide.BOTTOM; break;
//					case (3): bSide = BorderSide.RIGHT; break;
//					default:bSide = BorderSide.TOP;
//				};
//				cellStyle.setBorderColor(bSide, sideColor);
//			}
//		}
//		return cellStyle;
//	}
	
	/**
	 * Set Border Style for a CellStyle
	 * Dat Border Style cho mot CellStyle
	 * @author Datnt43
	 * @since 09/01/2014
	 * @param cellStyle
	 * @param borderStyle : border style cho tat ca cac canh cua cell (neu chi set cho mot so canh thi de null)
	 * @param borderColor: mau canh
	 * @param params: border's Style tren, trai, duoi, phai cua cell	
	 */
	public static CellStyle setBorderForCell(CellStyle cellStyle, BorderStyle borderStyle,XSSFColor borderColor,BorderStyle... params){
//		BorderStyle a = borderStyle == null?BorderStyle.THIN:borderStyle;
		Short b = borderStyle == null?-1:Short.valueOf(String.valueOf(borderStyle.ordinal()));
		Short color = borderColor == null?-1:borderColor.getIndexed();
		
		if(b != -1){
			cellStyle.setBorderLeft(b);
			cellStyle.setBorderTop(b);
			cellStyle.setBorderRight(b);
			cellStyle.setBorderBottom(b);
		}
		//set color
		if(color != -1){
			cellStyle.setTopBorderColor(color);
			cellStyle.setBottomBorderColor(color);
			cellStyle.setLeftBorderColor(color);
			cellStyle.setRightBorderColor(color);
		}
		if(params.length <= 4){
			int i = 0;
			for(BorderStyle sideBorder:params){
				Short c = sideBorder == null?-1:Short.valueOf(String.valueOf(sideBorder.ordinal()));
				switch (i++){
					case (0): if(c!=-1){cellStyle.setBorderTop(c);};
							break;
					case (1): if(c!=-1){cellStyle.setBorderLeft(c);};
							break;
					case (2): if(c!=-1){cellStyle.setBorderBottom(c);};
							break;
					case (3): if(c!=-1){cellStyle.setBorderRight(c);};
						break;
					default:	break;
				};
			}
		}
		return cellStyle;
	}
	
	public static XSSFFont setFontPOI(XSSFFont fontStyle,String fontName,Integer fontHeight,Boolean isBold,XSSFColor fontColor){
		String fName = fontName == null?"Arial":fontName;
		Integer fHeight = fontHeight == null?9:fontHeight;
		Boolean fIsBold = isBold == null?false:isBold;
		XSSFColor fColor = fontColor == null?new XSSFColor(new java.awt.Color(0, 0, 0)):fontColor;
		fontStyle.setBold(fIsBold);
		fontStyle.setFontName(fName);
		fontStyle.setFontHeight(fHeight);
		/*if(fColor.getRgb()[0] == 0 && fColor.getRgb()[1] == 0 && fColor.getRgb()[2] == 0){
			fontStyle.setColor(HSSFColor.WHITE.index);
		} else if(fColor.getRgb()[0] == -1 && fColor.getRgb()[1] == -1 && fColor.getRgb()[2] == -1){
			fontStyle.setColor(HSSFColor.BLACK.index);
		} else{
			fontStyle.setColor(fColor);
		}*/
		fontStyle.setColor(fColor);
		return fontStyle;
	}
}

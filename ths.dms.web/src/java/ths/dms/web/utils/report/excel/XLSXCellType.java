/**
 * 
 */
package ths.dms.web.utils.report.excel;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hungtx
 *
 */
public enum XLSXCellType {
	BOOLEAN ("b","b"),
	DATE ("d","d"),
	ERROR ("e","e"),
	INLINESTR ("inlineStr","inlineStr"),
	NUMBER ("n","n"),
	SHAREDSTRING ("s","s"),
	FORMULA ("str","str");	

	/** The value. */
	private String name;
	
	/** The value. */
	private String value;

	/** The values. */
	private static Map<String, XLSXCellType> values = null;

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	/**
	 * Instantiates a new gender type.
	 * 
	 * @param value
	 *            the value
	 */
	XLSXCellType(String name,String value) {
		this.name = name;
		this.value = value;
	}

	/**
	 * Parses the value.
	 * 
	 * @param value
	 *            the value
	 * @return the gender type
	 */
	public static XLSXCellType parseValue(String value) {
		if (values == null) {
			values = new HashMap<String, XLSXCellType>(XLSXCellType.values().length);
			for (XLSXCellType e : XLSXCellType.values())
				values.put(e.getName(), e);
		}
		return values.get(value);
	}
}
